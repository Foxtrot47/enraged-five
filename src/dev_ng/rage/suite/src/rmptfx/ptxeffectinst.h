// 
// rmptfx/ptxeffectinst.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXEFFECTINST_H
#define RMPTFX_PTXEFFECTINST_H


// includes (rmptfx)
#include "rmptfx/ptxcollision.h"
#include "rmptfx/ptxeffectrule.h"
#include "rmptfx/ptxlist.h"
#include "rmptfx/ptxrendersetup.h"

// includes (rage)
#include "atl/ptr.h"
#include "atl/vector.h"
#include "system/bit.h"
#include "system/timer.h"
#include "vectormath/classes.h"


// namespaces
namespace rage 
{


// forward declarations
class ptxEffectInstDB;


// defines
#define PTXEFFECTINST_MAX_EVENT_INSTS							(7)


// typedefs
typedef ptxPool<ptxEffectInst, ptxEffectInstDB>					ptxEffectInstPool;
typedef ptxPool<ptxEffectInst, ptxEffectInstDB>::ptxListItem	ptxEffectInstItem;


// enumerations
enum ptxEffectInstPlayState
{
	PTXEFFECTINSTPLAYSTATE_WAITING_TO_ACTIVATE					= 0,					// set when initialising the effect inst
	PTXEFFECTINSTPLAYSTATE_ACTIVATED,													// set when starting the effect inst the first time
	PTXEFFECTINSTPLAYSTATE_PLAYING,														// set when starting or restarting the effect inst
	PTXEFFECTINSTPLAYSTATE_STOPPED_MANUALLY,											// set when an effect inst stops manually, or when finishing 
	PTXEFFECTINSTPLAYSTATE_STOPPED_NATURALLY,											// set when an effect inst stops naturally
	PTXEFFECTINSTPLAYSTATE_FINISHED,													// set when an effect inst finishes naturally, or when finishing
	PTXEFFECTINSTPLAYSTATE_DEACTIVATED,													// set when a finished effect inst gets deactivated

	PTXEFFECTINSTPLAYSTATE_NUM
};

enum ptxEffectFlags
{
	PTXEFFECTFLAG_KEEP_RESERVED									= BIT(0),  				// keeps the effect inst reserved so it can't be deactivated
	PTXEFFECTFLAG_MANUAL_DRAW									= BIT(1),  				// the effect inst gets drawn manually so it is skipped in the normal draw loop
	PTXEFFECTFLAG_USER1											= BIT(2),  				// user flags for game specific use
	PTXEFFECTFLAG_USER2											= BIT(3),
	PTXEFFECTFLAG_USER3											= BIT(4),
	PTXEFFECTFLAG_USER4											= BIT(5),
	PTXEFFECTFLAG_USER5											= BIT(6),
	PTXEFFECTFLAG_USER6											= BIT(7),
	PTXEFFECTFLAG_USER7											= BIT(8),
	PTXEFFECTFLAG_USER8											= BIT(9),
	PTXEFFECTFLAG_USER9											= BIT(10),
	PTXEFFECTFLAG_USER10										= BIT(11),
	PTXEFFECTFLAG_USER11										= BIT(12)		
};

enum ptxEffectInternalFlags
{
	PTXEFFECTFLAG_OVERRIDE_DISTSQR_TO_CAM						= 0,
	PTXEFFECTFLAG_OVERRIDE_DISTSQR_TO_CAM_ALWAYS,										// don't change the draw distance after a call to Draw()

	PTXEFFECTFLAG_NEEDS_RELOCATED,
	PTXEFFECTFLAG_CALLBACK_UPDATE,													// true if this effect can spawn child effects or call callbacks (so we can update all of these at a safe time)

	PTXEFFECTINTERNALFLAG_NUM
};

enum ptxEffectInvertAxis
{
	PTXEFFECTINVERTAXIS_X										= BIT(0),  
	PTXEFFECTINVERTAXIS_Y										= BIT(1),  
	PTXEFFECTINVERTAXIS_Z										= BIT(2)   
};

enum ptxEffectCullState
{
	CULLSTATE_NOT_CULLED										= 0,
	CULLSTATE_VIEWPORT_CULLED,
	CULLSTATE_OCCLUSION_CULLED,
	CULLSTATE_DISTANCE_CULLED,
};


// classes
class ptxDataVolume
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		void Clear() {m_type=0; m_vPosWldCentre=Vec3V(V_ZERO); m_vPosWldStart=Vec3V(V_ZERO); m_vPosWldStart=Vec3V(V_ZERO); m_length=0.0f; m_radius=0.0f;}

		float GetPointTValue(Vec3V_In vPoint);
		float GetPointDistSqr(Vec3V_In vPoint);
		float GetPointStrength(Vec3V_In vPoint, Vec3V_In vDir, bool useTaperedRadius, bool useDistFade, bool useLengthFade, bool useAngleFade);
		bool IsPointInside(Vec3V_In vPoint);

		// accessors
		void SetType(u8 val) {m_type = val;}
		u8 GetType() {return m_type;}
		void SetPosWldCentre(Vec3V_In vPos) {m_vPosWldCentre = vPos;}
		Vec3V_Out GetPosWldCentre() {return m_vPosWldCentre;}
		void SetPosWldStart(Vec3V_In vPos) {m_vPosWldStart = vPos;}
		Vec3V_Out GetPosWldStart() {return m_vPosWldStart;}
		void SetPosWldEnd(Vec3V_In vPos) {m_vPosWldEnd = vPos;}
		Vec3V_Out GetPosWldEnd() {return m_vPosWldEnd;}
		void SetLength(float val) {m_length = val;}
		float GetLength() {return m_length;}
		void SetRadius(float val) {m_radius = val;}
		float GetRadius() {return m_radius;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		u8 m_type;
		Vec3V m_vPosWldCentre;
		Vec3V m_vPosWldStart;
		Vec3V m_vPosWldEnd;
		float m_length;
		float m_radius;

};

class ptxEffectInstDB
{	
	friend class ptxEffectInst;

	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxEffectInstDB()
		: m_currLifeRatio(0.0f)
		, m_customFlags(0)
		, m_isDrawCulled(false)
		, m_distSqrToCamera(0.0f)
		{}

		// accessors
		float GetCurrLifeRatio() {return m_currLifeRatio;}
		ScalarV_Out GetCurrLifeRatioV() {return ScalarVFromF32(m_currLifeRatio);}
		void SetDistSqrToCamera(float val) {m_distSqrToCamera = val;}
		float GetDistSqrToCamera() const {return m_distSqrToCamera;}
		float& GetDistSqrToCameraRef() {return m_distSqrToCamera;}
		void SetIsDrawCulled(bool val) {m_isDrawCulled = val;}
		bool GetIsDrawCulled() {return m_isDrawCulled;}

		void SetCustomFlag(u8 flag) {m_customFlags |= flag;}
		bool GetCustomFlag(u8 flag) const {return (m_customFlags & flag) ? true : false;}
		void ClearCustomFlag(u8 flag) {m_customFlags &= ~flag;}
		void ClearCustomFlags() {m_customFlags = 0;}
		u8 GetCustomFlags() {return m_customFlags;}

	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		float m_currLifeRatio;												// the current ratio through the effect life
		float m_distSqrToCamera;											// squared distance from the effect to the camera (this get set in the update but read when sorting whilst drawing)
		u8 m_customFlags;
		bool m_isDrawCulled;												// whether this effect's point drawing is disabled due to culling

};


class ptxEffectInst
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxEffectInst();
		~ptxEffectInst();

		// main interface
		void Init(ptxEffectRule* pEffectRule, u32 flags);

		void Start();
		void Stop();
		void Finish(bool mustDeactivateNow);		
		void Relocate();
		void Deactivate();
		void UnReference();

		void UpdateSetup(float dt, const grcViewport* pViewport ASSERT_ONLY(, bool bPerformingPreUpdate = false));
		void UpdateParallel(float dt);
		void UpdateFinalize(float dt);
		void UpdateLODAlphaMultipler();

		void Draw(PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface));

		// memory
		void AllocateMemory();
		void DeallocateMemory();

		//rendering
		bool ShouldBeRendered(bool bSkipManualDraw);

		// culling
		Vec4V_Out GetBoundingSphere();
		ptxEffectCullState CalcCullState(const grcViewport* pViewport, bool bPerformOcclusionTest);

		// collision
		void AddCollisionPolygons(ptxCollisionPoly* pPolyArray, int numPolys);
		void ClearCollisionSet();
		void SetCollisionSet(ptxCollisionSet* pColnSet);
		void CopyCollisionSet(ptxCollisionSet* pColnSet);

		// data volumes
		bool GetUseDataVolume();
		void GetDataVolume(ptxDataVolume* pDataVolume);
		u8 GetDataVolumeType();

		// multi-buffering
		void CopyToUpdateBuffer();

		// accessors - rules
		void SetEffectRule(ptxEffectRule* pEffectRule) {m_pEffectRule = pEffectRule;}
		void SetEffectRuleRaw(ptxEffectRule* pEffectRule) {m_pEffectRule.ptr = pEffectRule;}
		ptxEffectRule* GetEffectRule() {return m_pEffectRule;}	
		
		// accessors - events
		int GetNumEvents() {return m_numEvents;}
		ptxEventInst& GetEventInst(int index) {return m_eventInsts[index];};
		ptxEventInst* GetEventInsts() {return m_eventInsts.GetElements();}
		
		// accessors - positional state
		Vec3V_Out SetBasePos(Vec3V_In vPos);
		Vec3V_Out SetBaseMtx(Mat34V_In vMtx);
		void SetOffsetMtx(Mat34V_In vMtx);
		void SetOffsetPos(Vec3V_In vPos);
		void SetOffsetRot(Vec3V_In vEulerAngles);
		void SetOffsetRot(Vec3V_In vAxis, float angle);
		void SetOffsetRot(QuatV_In vQuat);
		void SetOffsetRot(Mat34V_In vMtx);
		Mat34V_Out GetMatrix() const {return m_vMtxFinal;}
		__forceinline Mat34V_Out GetMatrixNoInvertedAxes() const;
		Vec3V_Out GetCurrPos() const {return m_vMtxFinal.GetCol3();}
		Vec3V_Out GetPrevPos() const {return m_vPrevPos;}
		void UpdateFinalMtx();

		// accessors - state
		int GetCurrLoop() const {return m_currLoop;}
		void SetOOLife(float val) {m_ooLife = val;}
		float GetOOLife() const {return m_ooLife;}
		float GetPrevLifeRatio() const {return m_prevLifeRatio;}
		float GetFinalZoom() const {return m_finalZoom;}
		int GetNumActivePoints() const {return m_numActivePoints;}
		void SetCullState(ptxEffectCullState cullState) {m_cullState = cullState;}
		ptxEffectCullState GetCullState() const {return m_cullState;}
		void SetIsEmitCulled(bool isCulled) {m_isEmitCulled = isCulled;}
		bool GetIsEmitCulled() const {return m_isEmitCulled;}
		void SetIsUpdateCulled(bool isCulled) {m_isUpdateCulled = isCulled;}
		bool GetIsUpdateCulled() const {return m_isUpdateCulled;}
		
		bool GetIsPlaying() const {return m_playState==PTXEFFECTINSTPLAYSTATE_PLAYING;}
		bool GetIsStopped() const {return m_playState==PTXEFFECTINSTPLAYSTATE_STOPPED_MANUALLY || m_playState==PTXEFFECTINSTPLAYSTATE_STOPPED_NATURALLY;}
		bool GetIsFinished() const {return m_playState==PTXEFFECTINSTPLAYSTATE_FINISHED;}
		bool GetIsDeactivated() const {return m_playState==PTXEFFECTINSTPLAYSTATE_DEACTIVATED;}
		ptxEffectInstPlayState GetPlayState() const {return m_playState;}
#if RMPTFX_BANK
		void SetPlayState(ptxEffectInstPlayState val);
#else
		void SetPlayState(ptxEffectInstPlayState val) {m_playState = val;}
#endif

		void GetAmbientScales(float& natAmbScale, float& artAmbScale) const {natAmbScale=m_ambientScale[0]*m_naturalAmbientMult; artAmbScale=m_ambientScale[1]*m_artificialAmbientMult;}
		void SetAmbientScales(float natAmbScale, float artAmbScale) {m_ambientScale[0]=natAmbScale; m_ambientScale[1]=artAmbScale;}

		bool GetAmbientScalesSetup() const {return m_ambientScaleSetup;}
		void SetAmbientScalesSetup(bool isSetup) {m_ambientScaleSetup = isSetup;}

		void SetArtificialAmbientMult(float val) {m_artificialAmbientMult = val;}
		void SetNaturalAmbientMult(float val) {m_naturalAmbientMult = val;}

		void SetInInterior(bool isInInterior) { m_isInInterior = isInInterior; }
		bool GetInInterior() const { return m_isInInterior; }

		// TODO: Figure out whether we can guarantee caching an interior location for every effect inst.
		//		 If that's possible, then we should get rid of the SetInInterior/GetInInterior api above.
		//		 Otherwise, when querying the effect inst for an interior location, we need to check
		//		 first whether one has been cached (ie: GetIsInteriorLocationCached).
		//		 If no interior location has been cached, we can then fallback to the previous logic
		//		 to figure out out an interior location based on the camera position or the attached entity.
		bool GetIsInteriorLocationCached() const { return m_interiorLocationCached; }
		void SetIsInteriorLocationCached(bool val) { m_interiorLocationCached = val; }
		void SetInteriorLocation(u32 intLocation) { m_interiorLocation = intLocation; }
		u32 GetInteriorLocation() const { return m_interiorLocation; }

		void SetIgnoreDataCapsuleTests(bool val) {m_ignoreDataCapsuleTests = val;}
		bool GetIgnoreDataCapsuleTests() {return m_ignoreDataCapsuleTests;}

		// accessors - settings
		void SetRenderSetup(ptxRenderSetup *pRenderSetup);
		const ptxRenderSetup* GetRenderSetup() const {return m_pRenderSetup;}
		ptxRenderSetup* GetRenderSetup() {return m_pRenderSetup;}
		void SetVelocityAdd(Vec3V_In vVel) {m_vVelocityAdd = vVel;}
		Vec3V_Out GetVelocityAdd() const {return m_vVelocityAdd;}
		void SetColourTint(Vec3V_In vRGB) {m_vRGBATint.SetXYZ(vRGB);}
		Vec3V_Out GetColourTint() const {return m_vRGBATint.GetXYZ();}
		void SetAlphaTint(float val) {m_vRGBATint.SetWf(val);}
		float GetAlphaTint() const {return m_vRGBATint.GetWf();}
		ScalarV_Out GetAlphaTintV() const {return m_vRGBATint.GetW();}
		void SetUserZoom(float val) {m_userZoom = val;}
		float GetUserZoom() const {return m_userZoom;}
		float GetPlaybackRateScalar() const {return m_playbackRateScalar;}
		void SetLODScalar(float val) {m_lodScalar = val;}
		float GetLODScalar() const {return m_lodScalar;}
		float GetLodAlphaMult() const {return m_lodAlphaMult;}
		s8 GetLodEvoIdx() const {return m_lodEvoIdx;}
		void SetDrawListIdx(int index) {m_drawListIdx = (u8)index;}
		u8 GetDrawListIdx() const {return m_drawListIdx;}
		u8 GetRGBARandIndex() const {return m_rgbaRndIdx;}
		u8 GetTrailTexInfoRandIndex() const {return m_trailTexInfoRndIdx;}
		void SetInvertAxes(u8 val) {m_invertAxes = val;}
		void SetCanBeCulled(bool val) {m_canBeCulled = val;}
		void SetDontEmitThisFrame(bool val) {m_dontEmitThisFrame = val;}
		bool GetDontEmitThisFrame() const {return m_dontEmitThisFrame;}
		void SetCanMoveVeryFast(bool val) {m_canMoveVeryFast = val;}
		bool GetCanMoveVeryFast() const {return m_canMoveVeryFast;}

		// accessors - overrides
		void SetOverrideNearClipDist(float val) {m_overrideNearClipDist = val;}
		float GetOverrideNearClipDist() const {return m_overrideNearClipDist;}
		void SetOverrideFarClipDist(float val) {m_overrideFarClipDist = val;}
		float GetOverrideFarClipDist() const {return m_overrideFarClipDist;}
		void SetOverrideDistTravelled(float val) {m_overrideDistTravelled = val;}
		float GetOverrideDistTravelled() const {return m_overrideDistTravelled;}
		void SetOverrideCreationDomainSize(Vec3V_In vSize) {m_overrideCreationDomainSize = vSize;}
		Vec3V_Out GetOverrideCreationDomainSize() const {return m_overrideCreationDomainSize;}

		// accessors - evolutions
		bool HasEvoFromHash(atHashValue hash);
		ptxEvoValueType* GetEvoValues() {return m_evoValues;}
		void SetEvoValueFromIndex(int evoIdx, float val);
		void SetEvoValueFromHash(atHashValue evoHashValue, float val);
		void SetEvoValueFromHash16(u16 evoHashValue, float val);
		float GetEvoValueFromIndex(int evoIdx);
		float GetEvoValueFromHash(atHashValue evoHashValue);
		ptxEvoValueType GetEvoValueRaw(int evoIdx);

		// accessors - collision data
		bool HasCollisionSet() const {return m_pCollisionSet && m_pCollisionSet->GetNumColnPolys()>0;}
		ptxCollisionSet* GetCollisionSet() {return HasCollisionSet() ? m_pCollisionSet : NULL;}
		ptxCollisionSet* GetInternalCollisionSet() {return &m_internalCollisionSet;}

		// accessors - flags
		void SetFlag(ptxEffectFlags flag) {m_flags |= flag;}
		bool GetFlag(ptxEffectFlags flag) const {return (m_flags & flag) ? true : false;}
		void ClearFlag(ptxEffectFlags flag) {m_flags &= ~flag;}
		void ClearFlags() {m_flags = 0;}
		u32 GetFlags() const {return m_flags;}

		void SetOverrideDistSqrToCamera(float distSqr, bool always=false);
		bool GetOverrideDistSqrToCamera() {return m_internalFlags.IsSet(PTXEFFECTFLAG_OVERRIDE_DISTSQR_TO_CAM);}
		bool GetOverrideDistSqrToCameraAlways() const {return m_internalFlags.IsSet(PTXEFFECTFLAG_OVERRIDE_DISTSQR_TO_CAM_ALWAYS);}

		void SetCameraBias(float bias) {m_cameraBias = bias;}
		float GetCameraBias() {return m_cameraBias;}

		void SetNeedsRelocated() {m_internalFlags.Set(PTXEFFECTFLAG_NEEDS_RELOCATED);}

		bool GetNeedsCallbackUpdate() const {return m_internalFlags.IsSet(PTXEFFECTFLAG_CALLBACK_UPDATE);}

		// accessors - spawned effects
		bool IsSpawnedEffect() const {return m_pSpawningPointItem!=NULL;}
		void SetSpawningPointItem(ptxPointItem* pPointItem) {m_pSpawningPointItem = pPointItem;}
		void SetSpawningEffectInst(ptxEffectInst* pEffectInst) {m_pSpawningEffectInst = pEffectInst;}
		ptxEffectInst* GetSpawningEffectInst() {return m_pSpawningEffectInst;}
		void SetEffectSpawner(ptxEffectSpawner* pEffectSpawner) {m_pEffectSpawner = pEffectSpawner;}
		void SetSpawnedEffectScalars(ptxSpawnedEffectScalars& scalarsMin, ptxSpawnedEffectScalars& scalarsMax) {m_spawnedEffectScalars.SetRandomly(scalarsMin, scalarsMax);}
		const ptxSpawnedEffectScalars& GetSpawnedEffectScalars() const {return m_spawnedEffectScalars;}

		// accessors - misc
		ScalarV_Out GetKeyTime() const {return Clamp(m_pEffectInstItem->GetDataDB()->GetCurrLifeRatioV(), ScalarV(V_ZERO), ScalarV(V_ONE));}
		ptxEffectInstDB* GetDataDB() {return m_pEffectInstItem->GetDataDB();}
		ptxEffectInstDB* GetDataDBMT(int bufferId) {return m_pEffectInstItem->GetDataDBMT(bufferId);}
		void SetEffectInstItem(ptxEffectInstItem* pEffectInstItem) {m_pEffectInstItem = pEffectInstItem;}
		ptxEffectInstItem* GetEffectInstItem() {return m_pEffectInstItem;}

		// debug
#if RMPTFX_BANK
		bool GetOutputPlayStateDebug() const {return m_outputPlayStateDebug;}
		const char* GetEvoName(int evoIdx);
		void DebugDraw();
#endif


	private: //////////////////////////

		void Loop();
		void UpdatePlayState(int numActiveEvents);
		void UpdateSpawnedEffect(float dt);
		int	CalcNumActivePoints();

		void ComputeCallbackUpdateFlag();

		static int EventSorter(const void* pA, const void* pB);

		// culling
		void UpdateCulling(const grcViewport* pViewport);

		// debug
#if RMPTFX_BANK
		void OutputUpdateProfileInfo(const char* pFunctionName, sysTimer& timer);
		bool ShouldOutputPlayStateInfo();
		void OutputPlayStateFunction(const char* pFunctionName);
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		// rules
		pgRef<ptxEffectRule> m_pEffectRule;										// pointer to the emitter rule

		// event data
		int m_numEvents;														// the number of events that this effect has
		atVector<ptxEventInst> m_eventInsts;									// array of event instance data

		// positional state
		Mat34V m_vMtxBase;														// effect's base matrix
		Mat34V m_vMtxOffset;													// effect's offset matrix
		Mat34V m_vMtxFinal;														// effect's final matrix
		Vec3V m_vPrevPos;														// previous position of the effect

		// state
		int m_currLoop;															// the current loop of the effect
		float m_duration;														// the duration of the effect	
		float m_ooLife;															// reciprocal of effect life
		float m_prevLifeRatio;													// the previous ratio through the effect life
		float m_finalZoom;														// the effect's final zoom value each frame (calculated from the rule zoom and inst user zoom)
		int m_numActivePoints;													// the number of active points across the effect's events
		ptxEffectCullState m_cullState;											// the cull state of the effect (none, viewport or distnace)
		bool m_isEmitCulled;													// whether this effect's point emission is disabled due to culling
		bool m_isUpdateCulled;													// whether this effect's point updating is disabled due to culling
		bool m_isInInterior;													// stored interior flag
		ptxEffectInstPlayState m_playState;										// the current play state (playing, finished etc)

		// settings
		ptxRenderSetup *m_pRenderSetup;											// pointer to the effect's render setup
		Vec3V m_vVelocityAdd;													// the velocity added to any points emitted from the effect
		Vec3V m_vRandOffsetPos;													// the random offset position added to the effect every loop
		Vec4V m_vRGBATint;														// rgba tint applied to the effect (not double buffered so should only be applied once before the effect starts)
		float m_userZoom;														// the user zoom set procedurally by the code
		float m_playbackRateScalar;												// effect playback rate scalar
		float m_lodScalar;														// scales the effects fade and cull distances 
		float m_lodAlphaMult;													// internally calculated alpha multiplier for automated lod fading between fade and cull distance
		s8 m_lodEvoIdx;															// the index into the evolution list of the lod evo
		s8 m_hqEvoIdx;															// the index into the evolution list of the high quality evo
		u8 m_drawListIdx;														// the index of the draw list this effect belongs to
		u8 m_zoomScalarRndIdx;													// the zoom scalar's index into the random table
		u8 m_rgbaRndIdx;														// the rgba index into the random table
		u8 m_trailTexInfoRndIdx;												// the trail texture info index into the random table
		u8 m_invertAxes;														// whether any effect axes should be inverted
		u8 m_skipOcclusionTestFrameCount;										// Remaining number of frames that occlusion test should be skipped
		bool m_canBeCulled;														// whether this effect can be culled (the game may want to override the culling)
		bool m_dontEmitThisFrame;												// whether this effect is allowed to emit points this frame
		bool m_canMoveVeryFast;													// whether this effect is expected to move very quickly - attachment relocation and spawn over distance asserts will need to be ignored for this effect

		// state (cont)
		bool m_ambientScaleSetup;
		float m_ambientScale[2];												// stored ambient scale values from the game lighting 
		float m_artificialAmbientMult;
		float m_naturalAmbientMult;

		// overrides
		float m_overrideNearClipDist;											// overrides the near clip distance set on the draw sprite behaviour (set to -1.0f to disable) (not implemented yet)
		float m_overrideFarClipDist;											// overrides the far clip distance set on the draw sprite behaviour (set to -1.0f to disable)
		float m_overrideDistTravelled;											// overrides the distance travelled by the effect in spawn over distance calculations (set to -1.0f to disable)
		Vec3V m_overrideCreationDomainSize;										// overrides the creation domain size (set to zero to disable)

		// evolutions
		ptxEvoValueType m_evoValues[PTXEVO_MAX_EVOLUTIONS];						// the current evolution values of this effect 

		// collision data
		ptxCollisionSet* m_pCollisionSet;										// pointer to the collision set to use (could be the internal one or some other shared one)
		ptxCollisionSet m_internalCollisionSet;									// the effect's internal collision set 

		// flags
		u32 m_flags;															// flags that can be accessed by the game code
		atFixedBitSet<PTXEFFECTINTERNALFLAG_NUM> m_internalFlags;				// flags that are internal to the rmptfx system

		// spawned effects
		ptxPointItem* m_pSpawningPointItem;										// pointer to the point that spawned this effect
		ptxEffectInst* m_pSpawningEffectInst;									// pointer to the effect inst that spawned this effect
		pgRef<ptxEffectSpawner> m_pEffectSpawner;								// pointer to the effect spawner that spawned this effect
		ptxSpawnedEffectScalars m_spawnedEffectScalars;							// scalars set internally if this effect has been spawned by another effect or point

		// misc
		ptxEffectInstItem* m_pEffectInstItem;									// pointer to the list item (can access the double buffered data from here)
		float				m_cameraBias;										// bias applied on the camera distance to affect sorting
		u32					m_interiorLocation;
		bool				m_interiorLocationCached;
		bool				m_ignoreDataCapsuleTests;

		// debug
#if RMPTFX_BANK
		bool m_outputPlayStateDebug;
		ATTR_UNUSED bool m_hasBeenActivated;
#endif

};


// code
__forceinline 
Mat34V_Out ptxEffectInst::GetMatrixNoInvertedAxes() const
{
	// the final matrix already has any inverted axes set in it
	// we need to un-invert the axes here

	ScalarV vNegOne(V_NEGONE);

	Mat34V vMtx = m_vMtxFinal;
	if (m_invertAxes & PTXEFFECTINVERTAXIS_X) 
	{
		vMtx.SetCol0(m_vMtxFinal.GetCol0()*vNegOne);
	}

	if (m_invertAxes & PTXEFFECTINVERTAXIS_Y) 
	{
		vMtx.SetCol1(m_vMtxFinal.GetCol1()*vNegOne);
	}

	if (m_invertAxes & PTXEFFECTINVERTAXIS_Z) 
	{
		vMtx.SetCol2(m_vMtxFinal.GetCol2()*vNegOne);
	}

	return vMtx;
}



} // namespace rage


#endif // RMPTFX_PTXEFFECTINST_H
