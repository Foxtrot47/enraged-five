// 
// rmptfx/ptxu_attractor.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// includes
#include "rmptfx/ptxu_attractor.h"
#include "rmptfx/ptxu_attractor_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{


//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_Attractor::sm_strengthDefn(				"Strength",					atHashValue("ptxu_Acceleration:m_strengthKFP"),				PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-500.0f,	500.0f,		0.01f),		"Min Stength",		"Max Stength");


// code
#if RMPTFX_XML_LOADING
ptxu_Attractor::ptxu_Attractor()
{
	// create keyframes
	m_strengthKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Acceleration:m_strengthKFP"));

	m_keyframePropList.AddKeyframeProp(&m_strengthKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();
}
#endif

void ptxu_Attractor::SetDefaultData()
{
	// default keyframes
	m_strengthKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// default other data
}

ptxu_Attractor::ptxu_Attractor(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_strengthKFP(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxu_Attractor_num++;
#endif
}

#if __DECLARESTRUCT
void ptxu_Attractor::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Attractor, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_Attractor, m_strengthKFP)
		//SSTRUCT_IGNORE(ptxu_Attractor, m_pad)
	SSTRUCT_END(ptxu_Attractor)
}
#endif

IMPLEMENT_PLACE(ptxu_Attractor);

void ptxu_Attractor::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointAttractor)
#endif
	{
		UpdatePoint(params);
	}
}

__forceinline Vec::V3Return128 FindClosestPointSegToOrigin(Vec::V3Param128 point1, Vec::V3Param128 point1To2Param)
{
	Vec3V v_point1(point1);
	Vec3V v_point1To2(point1To2Param);
	Vec3V v_closestPoint;
	const ScalarV fltVerySmall(V_FLT_SMALL_12);

	// Find the negative dot product of point 1 and the vector from point 2 to point 1.
	ScalarV v_oneDot = -Dot(v_point1To2, v_point1);
	if ( IsLessThanOrEqualAll(v_oneDot, fltVerySmall) != 0 )
	{
		// The dot product is not sufficiently positive, so point 1 is the closest point to the origin.
		v_closestPoint = v_point1;
	}
	else
	{
		// Find the dot product of point 2 with the vector from point 1 to point 2.
		ScalarV v_twoDot = MagSquared(v_point1To2) - v_oneDot;
		if ( IsLessThanOrEqualAll(v_twoDot, fltVerySmall) != 0 )
		{
			// The dot product is not sufficiently positive, so point 2 is the closest point to the origin.
			v_closestPoint = Add( v_point1, v_point1To2 );
		}
		else
		{
			// The closest point to the origin is on the segment between point 1 and point 2.
			ScalarV v_scale;
			v_scale = InvScale(v_oneDot, v_oneDot + v_twoDot);
			v_closestPoint = AddScaled(v_point1, v_point1To2, Vec3V(v_scale));
		}
	}

	return v_closestPoint.GetIntrin128();
}

__forceinline Vec::V3Return128 FindClosestPointSegToPoint(Vec::V3Param128 point1, Vec::V3Param128 point1To2, Vec::V3Param128 targetPoint)
{
	Vec3V v_targetPoint(targetPoint);

	// Transform the segment into the target point's coordinate system.
	Vec3V targetTo1;
	targetTo1 = Subtract(Vec3V(point1), v_targetPoint);

	// Now the problem is equivalent to finding the closest point on a segment to the origin.
	Vec3V v_closestPoint = Vec3V( FindClosestPointSegToOrigin(targetTo1.GetIntrin128(), point1To2) );

	// Transform the result back into the world coordinate system.
	v_closestPoint = Add(v_closestPoint, v_targetPoint);

	// Return 1 for point 1 closest, 2 for point 2 closest, 0 if another segment point is closest to the target.
	return v_closestPoint.GetIntrin128();
}

void ptxu_Attractor::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	ptxPointItem* pPointItem = params.pPointItem;	
	ScalarV	vDeltaTime = params.vDeltaTime;

	if (pEmitterInst->GetEmitterRule()->GetAttractorDomain()==NULL || pEmitterInst->GetEmitterRule()->GetAttractorDomain()->GetShape()!=PTXDOMAIN_SHAPE_ATTRACTOR)
	{
		return;
	}

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// get the particle and emitter life ratio key times
	ScalarV vKeyTimePoint = pPointDB->GetKeyTime();

	// cache some data that we'll need later
	const ScalarV vZero = ScalarV(V_ZERO);	
	const ScalarV vOne = ScalarV(V_ONE);	
	const ScalarV vSmall = ScalarV(V_FLT_SMALL_2);
	Vec3V vPointPos = pPointDB->GetCurrPos();
	ScalarV vAttractorRadiusNear = pEmitterInst->GetAttractorDomainInst()->GetCurrSizeInner().GetX();
	ScalarV vAttractorRadiusFar = pEmitterInst->GetAttractorDomainInst()->GetCurrSizeOuter().GetX();
	ScalarV vAttractorRadiusOrbit = pEmitterInst->GetAttractorDomainInst()->GetCurrSizeInner().GetY();
	ScalarV vAttractorLength = pEmitterInst->GetAttractorDomainInst()->GetCurrSizeOuter().GetY();
	Mat34V vAttractorMtx = pEmitterInst->GetAttractorDomainMtxWld(pEffectInst->GetMatrix());
	Vec3V vAttractorPos = vAttractorMtx.GetCol3();
	ptxAssertf(IsFiniteAll(vAttractorPos), "vAttractorPos is not finite");
	Vec3V vAttractorDir = vAttractorMtx.GetCol1();
	ptxAssertf(IsFiniteAll(vAttractorDir), "vAttractorDir is not finite");

	// make sure that the far fall off is always greater than near
	vAttractorRadiusFar = SelectFT(IsGreaterThan(vAttractorRadiusFar, vAttractorRadiusNear), vAttractorRadiusNear+vSmall, vAttractorRadiusFar);

	// get the keyframe data
	ptxEvolutionList* pEvolutionList = params.pEvolutionList;
	Vec4V vStrengthMinMax = m_strengthKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());

	// calculate the bias value
	ScalarV vBias = GetBiasValue(m_strengthKFP, pPointSB->GetRandIndex());

	// calculate the lerped data
	ScalarV vStrength = Lerp(vBias, SplatX(vStrengthMinMax), SplatY(vStrengthMinMax));
	ptxAssertf(IsFiniteAll(vStrength), "vStrength is not finite");

	// get closest point on attractor line to particle
	ptxAssertf(IsFiniteAll(vAttractorDir), "vAttractorDir is not finite");
	ptxAssertf(IsFiniteAll(vAttractorLength), "vAttractorLength is not finite");
	Vec3V vLineA = vAttractorPos + vAttractorDir*vAttractorLength;
	ptxAssertf(IsFiniteAll(vLineA), "vLineA is not finite");
	Vec3V vLineB = vAttractorPos - vAttractorDir*vAttractorLength;
	ptxAssertf(IsFiniteAll(vLineB), "vLineB is not finite");
	Vec3V vAtoB = vLineB - vLineA;
	ptxAssertf(IsFiniteAll(vAtoB), "vAtoB is not finite");
	Vec3V vClosestPtOnLine;
	vClosestPtOnLine.SetIntrin128(FindClosestPointSegToPoint(vLineA.GetIntrin128(), vAtoB.GetIntrin128(), vPointPos.GetIntrin128()));
	ptxAssertf(IsFiniteAll(vClosestPtOnLine), "vClosestPtOnLine is not finite");

	// calculate the attraction force
	Vec3V vPointToLine = vClosestPtOnLine - vPointPos;
	ptxAssertf(IsFiniteAll(vPointToLine), "vPointToLine is not finite (1)");
	ScalarV vPointToLineDist = Mag(vPointToLine);
	ptxAssertf(IsFiniteAll(vPointToLineDist), "vPointToLineDist is not finite");
	ScalarV vAttractionForce = (vPointToLineDist-vAttractorRadiusNear) / (vAttractorRadiusFar-vAttractorRadiusNear);
	ptxAssertf(IsFiniteAll(vAttractionForce), "vAttractionForce is not finite (1)");
	vAttractionForce = Clamp(vAttractionForce, vZero, vOne);
	vAttractionForce = vOne - vAttractionForce;
	vAttractionForce *= vAttractionForce;
	vAttractionForce *= vStrength;
	ptxAssertf(IsFiniteAll(vAttractionForce), "vAttractionForce is not finite (2)");

	// calculate the attraction dir
	// 	Vec3V vAttractionDir = Normalize(vPointToLine);

	// calculate the attraction dir (using a target position a distance 'near' units away from the closest point in the direction of the tangent of the point to line vector)
	vPointToLine = NormalizeSafe(vPointToLine, Vec3V(V_X_AXIS_WZERO));
	ptxAssertf(IsFiniteAll(vPointToLine), "vPointToLine is not finite (2)");
	Vec3V vTangent = Cross(vPointToLine, vAttractorDir);
	ptxAssertf(IsFiniteAll(vTangent), "vTangent is not finite");
	Vec3V vPointDir = NormalizeSafe(pPointDB->GetCurrVel(), Vec3V(V_X_AXIS_WZERO));
	ptxAssertf(IsFiniteAll(vPointDir), "vPointDir is not finite");
	ScalarV vDot = Dot(vPointDir, vTangent);
	ptxAssertf(IsFiniteAll(vDot), "vDot is not finite");
	Vec3V vTargetPos = SelectFT(IsGreaterThan(vDot, vZero), vClosestPtOnLine-vTangent*vAttractorRadiusOrbit, vClosestPtOnLine+vTangent*vAttractorRadiusOrbit);
	ptxAssertf(IsFiniteAll(vTargetPos), "vTargetPos is not finite");
	Vec3V vAttractionDir = vTargetPos - vPointPos;
	ptxAssertf(IsFiniteAll(vAttractionDir), "vAttractionDir is not finite (1)");
	vAttractionDir = NormalizeSafe(vAttractionDir, Vec3V(V_X_AXIS_WZERO));
	ptxAssertf(IsFiniteAll(vAttractionDir), "vAttractionDir is not finite (2)");

	//	grcDebugDraw::Line(vPointPos, vTargetPos, Color32(1.0f, 1.0f, 0.0f, 1.0f));
	//	grcDebugDraw::Line(vClosestPtOnLine, vTargetPos, Color32(0.0f, 1.0f, 0.0f, 1.0f));

	ptxAssertf(IsFiniteAll(pPointDB->GetCurrVel()), "pPointDB->GetCurrVel() is not finite");
	Vec3V vVel = pPointDB->GetCurrVel() + (vAttractionDir*vAttractionForce*vDeltaTime);
	ptxAssertf(IsFiniteAll(vVel), "vVel is not finite");

	if (IsFiniteAll(vVel))
	{
		if (!pPointSB->GetFlag(PTXPOINT_FLAG_AT_REST))
		{
			pPointDB->SetCurrVel(vVel);
		}
	}
}

void ptxu_Attractor::SetKeyframeDefns()
{
	m_strengthKFP.GetKeyframe().SetDefn(&sm_strengthDefn);
}

void ptxu_Attractor::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_strengthKFP, &sm_strengthDefn);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_Attractor::IsActive()
{
	ptxu_Attractor* pAttractor = static_cast<ptxu_Attractor*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_strengthKFP == pAttractor->m_strengthKFP;

	return !isEqual;
}

void ptxu_Attractor::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 1);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "Strength");

	// send non-keyframes
}

void ptxu_Attractor::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	// send keyframes
	SendKeyframeToEditor(buff, m_strengthKFP, pParticleRule);

	// send non-keyframes
}

void ptxu_Attractor::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_strengthKFP);

	// receive non-keyframes
}
#endif // RMPTFX_EDITOR


} // namespace rage
