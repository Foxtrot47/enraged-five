// 
// rmptfx/ptxu_wind.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_WIND_H 
#define PTXU_WIND_H 


// includes
#include "parser/macros.h"
#include "pheffects/spuwindeval.h"
#include "rmptfx/ptxbehaviour.h"


// namespaces
namespace rage
{


// enumerations
enum ptxu_Wind_DisturbanceMode
{
	PTXU_WIND_DISTURBANCE_NONE					= 0,
	PTXU_WIND_DISTURBANCE_FIELD,
	PTXU_WIND_DISTURBANCE_FIELD_AND_GLOBAL,

	PTXU_WIND_DISTURBANCE_NUM
};


// classes
class ptxu_Wind : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_Wind();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxu_Wind(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_Wind);

		// info
		const char* GetName() {return "ptxu_Wind";}														// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_WIND;}													// the ordering of the behaviour (see ptxconfig.h)

		bool NeedsToInit() {return true;}																// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}																// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return false;}													// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return false;}																// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}															// whether we call release when a particle dies
		bool UpdateOnMainThreadOnly() {return false;}													// whether the behaviour only updates on the main thread (as opposed to the spu)

		void InitPoint(ptxBehaviourParams& params);														// called for each newly created particle if NeedsToInit returns true
		void UpdatePoint(ptxBehaviourParams& params);													// called from InitPoint and UpdatePoint to update the particle accordingly 

		void UpdateEmitterInst(ptxBehaviourParams& params);

		void SetKeyframeDefns();																		// sets the keyframe definitions on the keyframe properties
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);									// sets the keyframe definitions on the evolved keyframe properties in the evolution list

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Wind";}													// name of the behaviour shown in the editor
		bool IsActive();																				// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& buff);														// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);				// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);										// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif



	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data
		ptxKeyframeProp m_influenceKFP;

		// non keyframe data
		phSpuWindEvalData* m_pGlobalData_NOTUSED;									// should always point to sm_globalData - so spu can load the global data
		phSpuWindEval* m_pWindEval_NOTUSED;											// we create a wind eval in the load fragment, and use it in the update fragment
		
		float m_highLodRange;
		float m_lowLodRange;

		int	m_highLodDisturbanceMode;
		int	m_lowLodDisturbanceMode;
		
		bool m_ignoreMtxWeight;

		datPadding<7> m_pad;

		// static data
		static ptxKeyframeDefn sm_influenceDefn;

};


} // namespace rage


#endif // PTXU_WIND_H 
