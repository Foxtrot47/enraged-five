// 
// rmptfx/ptxu_age.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "rmptfx/ptxu_age.h"
#include "rmptfx/ptxu_age_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxeffectinst.h"


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{


// code
#if RMPTFX_XML_LOADING
ptxu_Age::ptxu_Age()
{	
	// create keyframes

	// set default data
	SetDefaultData();
}
#endif

void ptxu_Age::SetDefaultData()
{
	// default keyframes

	// default other data
}

ptxu_Age::ptxu_Age(datResource& rsc)
	: ptxBehaviour(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxu_Age_num++;
#endif
}

#if __DECLARESTRUCT
void ptxu_Age::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Age, ptxBehaviour)
	SSTRUCT_END(ptxu_Age)
}
#endif

IMPLEMENT_PLACE(ptxu_Age);

void ptxu_Age::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointAge)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_Age::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxPointItem* pPointItem = params.pPointItem;	
	// convert vectorised variables to floating point
	float dt = params.vDeltaTime.Getf();

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	float effectSpawnerTriggerRatio = params.effectSpawnerTriggerRatio;

	// update the life ratio of the particle
	float prevLifeRatio = pPointDB->GetLifeRatio();
	float nextLifeRatio = prevLifeRatio + (dt * pPointSB->GetOOLife());

	// spawn effect
	float prevLifeRatioSpawn = prevLifeRatio;
	if (pPointSB->GetFlag(PTXPOINT_FLAG_IS_BEING_INITIALISED))
	{
		// if the point is being initialised then sub frame emission could mean that it's previous life ratio is not zero
		// override to zero for the spawning checks so we don't miss any spawning at the start of the point's life
		prevLifeRatioSpawn = 0.0f;
	}

	if (dt>0.0f && effectSpawnerTriggerRatio>=0.0f && effectSpawnerTriggerRatio>=prevLifeRatioSpawn && effectSpawnerTriggerRatio<nextLifeRatio)
	{
		pPointSB->SetFlag(PTXPOINT_FLAG_SPAWN_EFFECT);
	}

	// deal with particles with a life ratio above one
	if (nextLifeRatio>1.0f)
	{
		if (pPointSB->GetFlag(PTXPOINT_FLAG_LOOPED) && pEffectInst->GetIsPlaying())
		{
			// looped particles wrap their life ratio
			nextLifeRatio -= 1.0f;
		}
		else
		{
			// non looped particle are dead
			pPointDB->SetIsDead(true);
		}
	}

	// store on the particle
	pPointDB->SetLifeRatio(nextLifeRatio);
}


} // namespace rage
