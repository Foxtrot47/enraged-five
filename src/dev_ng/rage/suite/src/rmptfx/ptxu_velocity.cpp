// 
// rmptfx/ptxu_velocity.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "rmptfx/ptxu_velocity.h"
#include "rmptfx/ptxu_velocity_parser.h"

#include "rmptfx/ptxdebug.h"


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{


// code
#if RMPTFX_XML_LOADING
ptxu_Velocity::ptxu_Velocity()
{	
	// create keyframes

	// set default data
	SetDefaultData();
}
#endif

void ptxu_Velocity::SetDefaultData()
{
	// default keyframes

	// default other data
}

ptxu_Velocity::ptxu_Velocity(datResource& rsc)
	: ptxBehaviour(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxu_Velocity_num++;
#endif
}

#if __DECLARESTRUCT
void ptxu_Velocity::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Velocity, ptxBehaviour)
	SSTRUCT_END(ptxu_Velocity)
}
#endif

IMPLEMENT_PLACE(ptxu_Velocity);

void ptxu_Velocity::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointVelocity)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_Velocity::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxPointItem* pPointItem = params.pPointItem;
	ScalarV	vDeltaTime = params.vDeltaTime;

	// get the particle data (double buffered)
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();

	// cache data from the point
	ScalarV	vPlaybackRate = ScalarVFromF32(pPointSB->GetPlaybackRate());

	// set the new particle position 
	pPointDB->IncCurrPos(pPointDB->GetCurrVel() * Vec3V(vDeltaTime) * vPlaybackRate);
}


} // namespace rage
