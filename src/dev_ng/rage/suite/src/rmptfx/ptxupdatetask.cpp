// 
// rmptfx/ptxupdatetask.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 


// includes (us)
#include "rmptfx/ptxupdatetask.h"

// includes (rmptfx)
#include "rmptfx/ptxmanager.h"


// defines
#define MAX_UPDATE_ITEMS_PER_BATCH		1


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
using namespace rage;


// external declarations
namespace rage
{
	EXT_PF_TIMER(TimerUpdateParallel_WaitOnChildTasks);
}


// static variables
int ptxUpdateTask::sm_schedulerIndex = 0;
u32 ptxUpdateTask::sm_taskCount = 0;
ptxUpdateTaskInputData ptxUpdateTask::sm_updateInputData[RMPTFX_MAX_PARTICLE_UPDATE_TASKS];
sysTaskHandle ptxUpdateTask::sm_taskHandles[RMPTFX_MAX_PARTICLE_UPDATE_TASKS] = {};

//Place to store the thread Id's for each of the parallel update tasks
sysIpcCurrentThreadId ptxUpdateTask::sm_taskThreadIDs[RMPTFX_MAX_PARTICLE_UPDATE_TASKS] = 
{
	sysIpcCurrentThreadIdInvalid,
	sysIpcCurrentThreadIdInvalid,
	sysIpcCurrentThreadIdInvalid,
	sysIpcCurrentThreadIdInvalid
};


// task declaration
DECLARE_TASK_INTERFACE(ptxupdatetask);


// code
void ptxUpdateTask::Init(int scheduler)
{
	if (scheduler>=0)
	{
		sm_schedulerIndex = scheduler;
	}
	else
	{
		int cores = 0xff;

#if RSG_DURANGO || RSG_ORBIS
		cores = (1 << 1) | (1 << 2) | (1 << 3) | (1 << 5);
#elif RSG_PC
		if (sysTaskManager::GetNumThreads() <= 2)
		{	
			// for one and two hardware threads we're likely better off not threading, but for now...
			cores = 0x3;
		}
#endif

#if RMPTFX_BIGGER_UPDATE_THREAD_STACK_FOR_DEBUGGING
		sm_schedulerIndex = sysTaskManager::AddScheduler("ptxUpdate", 45*1024, 256, PRIO_HIGHEST, cores, 0);
#else
		sm_schedulerIndex = sysTaskManager::AddScheduler("ptxUpdate", 36*1024, 256, PRIO_HIGHEST, cores, 0);
#endif
	}
}

void ptxUpdateTask::Update(float dt, u32 lastUpdateItemIdx)
{
	// get the task index
	int taskIdx = sm_taskCount;
	sm_taskCount++;

	// check we have enough tasks
	Assert(sm_taskCount<=(u32)RMPTFX_MAX_PARTICLE_UPDATE_TASKS);
	if (sm_taskCount>(u32)RMPTFX_MAX_PARTICLE_UPDATE_TASKS) 
	{
		return;
	}

	// set up the task input data
	ptxUpdateTaskInputData& taskInputData = sm_updateInputData[taskIdx];
	taskInputData.m_deltaTime = dt;
	taskInputData.m_pCurrEmitterInstUpdateItemIdx = &RMPTFXMGR.m_currEmitterInstUpdateItemIdx;
	taskInputData.m_pEmitterInstUpdateItems = RMPTFXMGR.m_emitterInstUpdateItems.GetElements();
	taskInputData.m_numEmitterInstUpdateItems = lastUpdateItemIdx;
	taskInputData.m_ptxUpdateTaskIdx = (u8)taskIdx;

	// set up the task parameters
	sysTaskParameters taskParams;
	taskParams.Input.Data = &taskInputData;
	taskParams.Input.Size = sizeof(ptxUpdateTaskInputData);
	taskParams.Output.Size = 0;
	taskParams.Scratch.Size = 0;

	// start the job
	sysTaskHandle& task = sm_taskHandles[taskIdx];
	task = sysTaskManager::Create(TASK_INTERFACE(ptxupdatetask), taskParams, sm_schedulerIndex);

	ptxAssertf(task, "you have run out of task handles, try increasing 'MaxTasks' in task_psn.cpp");
}

void ptxUpdateTask::WaitForAll()
{
	PF_FUNC(TimerUpdateParallel_WaitOnChildTasks);
	if (sm_taskCount>0)
	{
		sysTaskManager::WaitMultiple(sm_taskCount, sm_taskHandles);
		sm_taskCount = 0;

		for (int i=0; i<RMPTFX_MAX_PARTICLE_UPDATE_TASKS; i++)
		{
			ptxUpdateTask::sm_taskThreadIDs[i] = sysIpcCurrentThreadIdInvalid;
		}
	}
}

void ptxupdatetask(sysTaskParameters& taskParams)
{
	PIXBegin(0, "ptxupdatetask");

	ptxUpdateTaskInputData& taskInputData = *(ptxUpdateTaskInputData*)taskParams.Input.Data;
	//Lets cache this threads ID for the task
	ptxUpdateTask::SetPtxUpdateTaskThreadID(taskInputData.m_ptxUpdateTaskIdx, sysIpcGetCurrentThreadId());

	//New task just start, lets setup the keyframe prop thread ID for this thread
	RMPTFXMGR.SetupPtxKeyFramePropThreadId();
	float dt = taskInputData.m_deltaTime;
	int loopCount = 0;

	while (true)
	{
		// get the start and end indices of the batch to process
		u32 startOfThisBatch = sysInterlockedAdd(taskInputData.m_pCurrEmitterInstUpdateItemIdx, MAX_UPDATE_ITEMS_PER_BATCH);
		u32 endOfThisBatch = startOfThisBatch+MAX_UPDATE_ITEMS_PER_BATCH;

		// check the back is valid
		if (startOfThisBatch>=taskInputData.m_numEmitterInstUpdateItems)
		{
			break;
		}

		// make sure we don't overrun the end of the item array
		if (endOfThisBatch>taskInputData.m_numEmitterInstUpdateItems)
		{
			endOfThisBatch = taskInputData.m_numEmitterInstUpdateItems;
		}

		// calculate the number of items to update
		u32 numUpdateItems = endOfThisBatch-startOfThisBatch;

		// get a pointer to the start of the items for this batch
		ptxEmitterInstUpdateItem* pEmitterInstUpdateItems = taskInputData.m_pEmitterInstUpdateItems+startOfThisBatch;

		// go through this batch's items
		for (u32 updateItemIdx=0; updateItemIdx<numUpdateItems; updateItemIdx++)
		{
			ptxEmitterInstUpdateItem& emitterInstUpdateItem = pEmitterInstUpdateItems[updateItemIdx];

			ptxEffectInst* pEffectInst = emitterInstUpdateItem.m_pEffectInstItem->GetDataSB();
			ptxEffectRule* pEffectRule = pEffectInst->GetEffectRule();
			ptxEmitterInst* pEmitterInst = pEffectInst->GetEventInst(emitterInstUpdateItem.m_eventIdx).GetEmitterInst();
			ptxEvolutionList* pEvoList = pEffectRule->GetTimeline().GetEvent(emitterInstUpdateItem.m_eventIdx)->GetEvolutionList();

			// skip any items that aren't emitter events, don't have any particles or should only be being processed on the main thread
			if (pEffectRule->GetTimeline().GetEvent(emitterInstUpdateItem.m_eventIdx)->GetType()!=PTXEVENT_TYPE_EMITTER ||
				pEmitterInst->GetFlag(PTXEMITINSTFLAG_ON_MAIN_THREAD_ONLY) 
				RMPTFX_BANK_ONLY(|| (!ptxDebug::sm_performParallelFinalize && pEmitterInst->GetNumActivePoints()==0)))
			{
				continue;
			}

			// skip any invalid emitters
			if (pEmitterInst->GetEmitterRule()==NULL)
			{
				continue;
			}

			//Make sure we prepare the evo data for this particle update task
			//Particle rules PrepareEvoData is called in the UpdateParallel.
			pEffectRule->PrepareEvoData();
			pEmitterInst->GetEmitterRule()->PrepareEvoData(pEvoList);

			// perform the update
			pEmitterInst->UpdateParallel(pEffectInst, pEvoList, dt);

			AssertMsg(loopCount<20000 , "infinite loop detected in ptxupdatetask");
			if (loopCount++>20000) 
			{
				break; 
			}
		}
	}

	PIXEnd();
}



