// 
// rmptfx/ptxkeyframeprop.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


// includes (us)
#include "rmptfx/ptxkeyframeprop.h"
#include "rmptfx/ptxkeyframeprop_parser.h"

// includes (rmptfx)
#include "rmptfx/ptxbiaslink.h"
#include "rmptfx/ptxbytebuff.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxinterface.h"
#include "rmptfx/ptxmanager.h"

// includes (rage)
#include "atl/array_struct.h"


// optimisations
RMPTFX_OPTIMISATIONS()

// namespaces
using namespace rage;

//Used for storing the thread ID in TLS and accessing the cached m_pEvolvedKeyframeProp
__THREAD s8 ptxKeyframeProp::ms_ptxKeyframePropThreadId = -1;

//Used for making sure that PrepareEvoData is called for that specific thread before it gets used
#if __ASSERT
__THREAD bool ptxKeyframeProp::ms_ptxKeyframePropEvoSetup = false;
#endif

// code
ptxKeyframeProp::ptxKeyframeProp()
{
	m_propertyId = 0;
	m_invertBiasLink = false;
	m_randIndex = ptxRandomTable::GetCurrIndex();
};

ptxKeyframeProp::~ptxKeyframeProp()
{
}

ptxKeyframeProp::ptxKeyframeProp(datResource& rsc)
: m_keyframe(rsc)
{
	if (!datResource_IsDefragmentation)
	{	
		m_randIndex = ptxRandomTable::GetCurrIndex();
	}

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxKeyframeProp_num++;
#endif
}

#if __DECLARESTRUCT
void ptxKeyframeProp::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxKeyframeProp)
		// There's no "ignore Array"....
		SSTRUCT_IGNORE(ptxKeyframeProp, m_pEvolvedKeyframeProp[0])
		SSTRUCT_IGNORE(ptxKeyframeProp, m_pEvolvedKeyframeProp[1])
		SSTRUCT_IGNORE(ptxKeyframeProp, m_pEvolvedKeyframeProp[2])
		SSTRUCT_IGNORE(ptxKeyframeProp, m_pEvolvedKeyframeProp[3])
		SSTRUCT_IGNORE(ptxKeyframeProp, m_pEvolvedKeyframeProp[4])
		SSTRUCT_IGNORE(ptxKeyframeProp, m_pEvolvedKeyframeProp[5])
		SSTRUCT_IGNORE(ptxKeyframeProp, m_pEvolvedKeyframeProp[6])
		SSTRUCT_IGNORE(ptxKeyframeProp, m_pEvolvedKeyframeProp[7])
		SSTRUCT_IGNORE(ptxKeyframeProp, m_pEvolvedKeyframeProp[8])
		SSTRUCT_IGNORE(ptxKeyframeProp, m_pEvolvedKeyframeProp[9])
		SSTRUCT_IGNORE(ptxKeyframeProp, m_pEvolvedKeyframeProp[10])
		SSTRUCT_IGNORE(ptxKeyframeProp, m_pEvolvedKeyframeProp[11])
		SSTRUCT_FIELD(ptxKeyframeProp, m_propertyId)
		SSTRUCT_FIELD(ptxKeyframeProp, m_invertBiasLink)
		SSTRUCT_FIELD(ptxKeyframeProp, m_randIndex)
		SSTRUCT_IGNORE(ptxKeyframeProp, m_pad)
		SSTRUCT_FIELD(ptxKeyframeProp, m_keyframe)
	SSTRUCT_END(ptxKeyframeProp)
}
#endif

//IMPLEMENT_PLACE(ptxKeyframeProp);

#if RMPTFX_EDITOR
bool ptxKeyframeProp::operator==(const ptxKeyframeProp& other)
{
	return m_keyframe == other.m_keyframe;
}
#endif

#if RMPTFX_EDITOR
void ptxKeyframeProp::UpdateBehaviourDefn(ptxKeyframeDefn* pKeyframeDefn, const ptxParticleRule* pParticleRule)
{
	if (m_keyframe.GetDefn()==pKeyframeDefn)
	{
		return;
	}

	m_keyframe.SetDefn(pKeyframeDefn);

	if (pKeyframeDefn)
	{
		RMPTFXMGR.SendUpdatedBehaviourKFPDefnToEditor(this, pParticleRule);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxKeyframeProp::SendToEditor(ptxByteBuff& buff, const char* pRuleName, ptxRuleType ruleType)
{
	buff.Write_u32(m_propertyId);
	buff.Write_bool(m_invertBiasLink);

	const char* pKeyframeDefnName = NULL;
	ptxKeyframeDefn* pKeyframeDefn = m_keyframe.GetDefn();
	if (pKeyframeDefn)
	{
		pKeyframeDefnName = pKeyframeDefn->GetName();
	}

	ptxKeyframeSpec keyframeSpec(pKeyframeDefnName, pRuleName, ruleType, m_propertyId);
	ptxInterface::SaveXmlToByteBuffer(keyframeSpec, buff);
}
#endif

#if RMPTFX_EDITOR
void ptxKeyframeProp::ReceiveFromEditor(const ptxByteBuff& buff)
{
#if __ASSERT
	u32 id = buff.Read_u32();
	ptxAssertf(id==m_propertyId, "id does not match sending object");
#else
	buff.Read_u32();
#endif
	
	m_invertBiasLink = buff.Read_bool();
}
#endif

ptxKeyframePropList::ptxKeyframePropList(datResource& rsc)
: m_pKeyframeProps(rsc, 1)
{
	// TODO: fixup all the keyframe prop pointers?

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxKeyframePropList_num++;
#endif
}

#if __DECLARESTRUCT
void ptxKeyframePropList::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxKeyframePropList)
		SSTRUCT_FIELD(ptxKeyframePropList, m_pKeyframeProps)
	SSTRUCT_END(ptxKeyframePropList)
}
#endif

IMPLEMENT_PLACE(ptxKeyframePropList);

void ptxKeyframePropList::Reserve(int numKeyframeProps)
{
	if (m_pKeyframeProps.GetCount()==0)
	{
		m_pKeyframeProps.Reserve(numKeyframeProps);
	}
}

void ptxKeyframePropList::AddKeyframeProp(ptxKeyframeProp* pKeyframeProp, ptxKeyframeDefn* pKeyframeDefn)
{
	pKeyframeProp->GetKeyframe().SetDefn(pKeyframeDefn);

	// add it to the array if it is not already there
	for (int i=0; i<m_pKeyframeProps.GetCount(); i++)
	{
		if (m_pKeyframeProps[i]->GetPropertyId()==pKeyframeProp->GetPropertyId())
		{
			return;
		}
	}

	m_pKeyframeProps.Grow() = pKeyframeProp;
}

void ptxKeyframePropList::PrepareEvoData(ptxEvolutionList* pEvolutionList)
{
	int numKeyframeProps = m_pKeyframeProps.GetCount();
	datRef<ptxKeyframeProp>* ppKeyframeProp = m_pKeyframeProps.GetElements();

	for (int i=0; i<numKeyframeProps; i++)
	{
		(*ppKeyframeProp)->PrepareEvoData(pEvolutionList);
		ppKeyframeProp++;
	}
}

void ptxKeyframePropList::SetBiasLinks(atArray<ptxBiasLink>& biasLinks)
{
	for (int i=0; i<biasLinks.GetCount(); i++)
	{
		for (int j=0; j<m_pKeyframeProps.GetCount(); j++)
		{
			ptxKeyframeProp* pKeyframeProp = m_pKeyframeProps[j];
			biasLinks[i].SetLink(*pKeyframeProp);
		}
	}
}

ptxKeyframeProp* ptxKeyframePropList::GetKeyframePropByPropId(u32 keyframePropId)
{
	for (int i=0; i<m_pKeyframeProps.GetCount(); i++)
	{
		if (m_pKeyframeProps[i]->GetPropertyId()==keyframePropId)
		{
			return m_pKeyframeProps[i];
		}
	}

	return NULL;
}

#if RMPTFX_EDITOR
void ptxKeyframePropList::SendToEditor(ptxByteBuff& buff, const char* pRuleName, ptxRuleType ruleType)
{
	buff.Write_s32(m_pKeyframeProps.GetCount());

	for (int i=0; i<m_pKeyframeProps.GetCount(); i++)
	{
		m_pKeyframeProps[i]->SendToEditor(buff, pRuleName, ruleType);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxKeyframePropList::ReceiveFromEditor(const ptxByteBuff& buff)
{
#if __ASSERT
	u32 numKeyframeProps = buff.Read_u32();
	ptxAssertf(numKeyframeProps==(u32)m_pKeyframeProps.GetCount(), "keyframe prop count mismatch");
#else
	buff.Read_u32();
#endif

	for (int i=0; i<m_pKeyframeProps.GetCount(); i++)
	{
		m_pKeyframeProps[i]->ReceiveFromEditor(buff);
	}
}
#endif
