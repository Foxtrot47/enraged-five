// 
// rmptfx/ptxtimeline.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxtimeline.h"
#include "rmptfx/ptxtimeline_parser.h"

// includes (rmptfx)
#include "rmptfx/ptxbytebuff.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxeffectrule.h"
#include "rmptfx/ptxevent.h"

// includes (rage)
#include "atl/array_struct.h"
#include "bank/msgbox.h"


// optimisations
RMPTFX_OPTIMISATIONS()

// namespaces
using namespace rage;


// code
ptxTimeLine::ptxTimeLine()
{
	m_events.Reserve(PTXTIMELINE_MAX_CHANNELS);
}

ptxTimeLine::~ptxTimeLine()
{
	for (int i=m_events.GetCount()-1; i>=0; i--)
	{
		delete m_events[i];
	}

	m_events.Reset();
}

ptxTimeLine::ptxTimeLine(datResource& rsc) 
: m_events(rsc, 1)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxTimeLine_num++;
#endif
}

#if __DECLARESTRUCT
void ptxTimeLine::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxTimeLine)
		SSTRUCT_FIELD(ptxTimeLine, m_events)
	SSTRUCT_END(ptxTimeLine)
}
#endif

IMPLEMENT_PLACE(ptxTimeLine);

#if RMPTFX_XML_LOADING
void ptxTimeLine::RepairAndUpgradeData(const char* pFilename)
{
	// repair and upgrade each event on the timeline
	for (int i=0; i<m_events.GetCount(); i++)
	{
		m_events[i]->RepairAndUpgradeData(pFilename, i);
	}
}
#endif

#if RMPTFX_XML_LOADING
void ptxTimeLine::ResolveReferences()
{
	// resolve references on each event on the timeline
	for (int i=0; i<m_events.GetCount(); i++)
	{
		m_events[i]->ResolveReferences();
	}
}
#endif

#if __RESOURCECOMPILER || RMPTFX_EDITOR
void ptxTimeLine::Save()
{
	// save each event on the timeline
	for (int i=0; i<m_events.GetCount(); i++)
	{
		m_events[i]->Save();
	}
}
#endif

#if RMPTFX_EDITOR
int ptxTimeLine::EventSorter(const void* pA, const void* pB)
{
	// get the events
	const ptxEvent& eventA = **(const ptxEvent**)pA;
	const ptxEvent& eventB = **(const ptxEvent**)pB;

	// sort on event index
	return (eventA.GetIndex()<=eventB.GetIndex()) ? -1 : 1;
}
#endif

#if RMPTFX_EDITOR
void ptxTimeLine::ReorderEvents()
{
	ptxEvent* pSortBuffer[PTXTIMELINE_MAX_CHANNELS];

	// copy the events into the sort buffer
	for (int i=0; i<m_events.GetCount(); i++)
	{
		pSortBuffer[i] = m_events[i];
	}

	// perform the sort
	qsort(pSortBuffer, m_events.GetCount(), sizeof(const ptxEvent*), EventSorter);

	// put the sorted events back into the array
	for (int i=0; i<m_events.GetCount(); i++)
	{
		m_events[i] = pSortBuffer[i];
	}
}
#endif

#if RMPTFX_EDITOR
void ptxTimeLine::CreateUiObjects()
{
	// create ui objects for each event on the timeline
	for (int i=0; i<m_events.GetCount(); i++)
	{
		m_events[i]->CreateUiObjects();
	}
}
#endif

#if RMPTFX_EDITOR
void ptxTimeLine::SendToEditor(ptxByteBuff& buff, const ptxEffectRule* pEffectRule)
{
	buff.Write_u32(m_events.GetCount());

	for (int i=0; i<m_events.GetCount(); i++)
	{
		m_events[i]->SendToEditor(buff, pEffectRule);
	}	
}
#endif

#if RMPTFX_EDITOR
void ptxTimeLine::ReceiveFromEditor(ptxByteBuff& buff)
{
	int numEvents = buff.Read_s32();

	if (ptxVerifyf(numEvents==m_events.GetCount(), "number of events doesn't match"))
	{
		for (int i=0; i<m_events.GetCount(); i++)
		{
			m_events[i]->ReceiveFromEditor(buff);
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxTimeLine::StartProfileUpdate()
{
	// start profile update on each event on the timeline
	for (int i=0; i<m_events.GetCount(); i++)
	{
		m_events[i]->StartProfileUpdate();
	}
}
#endif

#if RMPTFX_EDITOR
void ptxTimeLine::StartProfileDraw()
{
	// start profile draw on each event on the timeline
	for (int i=0; i<m_events.GetCount(); i++)
	{
		m_events[i]->StartProfileDraw();
	}
}
#endif

#if RMPTFX_EDITOR
void ptxTimeLine::AddEmitterEvent(ptxEffectRule* pEffectRule)
{
	// check we have space to add a new event
	if (m_events.GetCount()==m_events.GetCapacity())
	{
		char msg[512];
		formatf(msg, sizeof(msg), "unable to add event (max allowed is %d", m_events.GetCapacity());
		bkMessageBox("RMPTFX", msg, bkMsgOk, bkMsgInfo);
		return;
	}

	// create a new event and append it to the list
	ptxEventEmitter* pEmitterEvent = static_cast<ptxEventEmitter*>(m_events.Append() = rage_new ptxEventEmitter);

	// set up the event
	pEmitterEvent->CreateUiObjects();
	pEmitterEvent->SetIndex(m_events.GetCount()-1);

	// create evolutions on the event
	if (pEffectRule->GetEvolutionList())
	{
		for (int i=0; i<pEffectRule->GetEvolutionList()->GetNumEvolutions(); i++)
		{
			pEmitterEvent->CreateEvolution(pEffectRule->GetEvolutionList()->GetEvolutionName(i));
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxTimeLine::DeleteEvent(s32 eventIndex)
{
	if (ptxVerifyf(eventIndex>=0 && eventIndex<m_events.GetCount(), "event index is out of range"))
	{
		m_events.Delete(eventIndex);
	}
}
#endif


