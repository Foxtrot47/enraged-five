<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::ptxShaderVar" onPostSave="OnPostSaveBase" cond="RMPTFX_XML_LOADING" onPreLoad="OnPreLoadBase">
	<u32 name="m_type"/>
</structdef>

<structdef type="rage::ptxShaderVarVector" base="rage::ptxShaderVar" cond="RMPTFX_XML_LOADING">
	<Vector4 name="m_vector"/>
</structdef>

<structdef type="rage::ptxShaderVarTexture" base="rage::ptxShaderVar" onPostLoad="OnPostLoad" cond="RMPTFX_XML_LOADING" onPreLoad="OnPreLoad">
	<string name="m_textureName" type="ConstString"/>
</structdef>

<structdef type="rage::ptxShaderVarKeyframe" base="rage::ptxShaderVar" cond="RMPTFX_XML_LOADING">
	<struct name="m_keyframe" type="rage::ptxKeyframe"/>
</structdef>

</ParserSchema>