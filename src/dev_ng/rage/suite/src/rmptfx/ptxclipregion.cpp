// 
// rmptfx/ptxclipregion.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

// includes
#include "ptxclipregion.h"

#include "atl/hashstring.h"
#include "file/asset.h"


// namespaces
namespace rage
{

// static variables
const Vec4V ptxClipRegions::sm_defaultData = Vec4V(0.0f, 1.0f, 0.0f, 1.0f);
ptxClipRegionTable ptxClipRegions::sm_clipRegionTable ;
bool ptxClipRegions::sm_isInitialised = false;

// code
int ptxClipRegionData::Load(fiTokenizer& token, Vec4V*& pDataPool)
{
	m_numColumns = token.GetInt();
	m_numRows = token.GetInt();
	int numFrames = m_numColumns*m_numRows;

	m_pData = pDataPool;
	pDataPool += numFrames;

	for (int i=0; i<numFrames; i++)
	{
		float uMin = token.GetFloat();
		float uMax = token.GetFloat();
		float vMin = token.GetFloat();
		float vMax = token.GetFloat();
		m_pData[i] = Vec4V(uMin, uMax, vMin, vMax);
	}

	return numFrames;
}

Vec4V_Out ptxClipRegionData::Get(int column, int row) const 
{
	ptxAssertf(column>=0 && column<m_numColumns, "invalid index into clip region list");
	ptxAssertf(row>=0 && row<m_numRows, "invalid index into clip region list");
	return m_pData[(row*m_numColumns)+column];
}

Vec4V_Out ptxClipRegionData::operator[](int idx) const 
{
	ptxAssertf(idx>=0 && idx<m_numColumns*m_numRows, "invalid index into clip region list");
	return m_pData[idx];
}





ptxClipRegionTable::~ptxClipRegionTable()
{
	delete m_pClipRegionPool;
}

bool ptxClipRegionTable::Load(const char* pName)
{
	RAGE_TRACK(ClipRegionTable);

	fiStream* pStream = ASSET.Open(pName, "dat");
	if (pStream)
	{
		fiTokenizer token(pName, pStream);

		int numTextures = token.GetInt();
		m_hashNames.Resize(numTextures);
		m_clipRegions.Resize(numTextures);
#if RMPTFX_EDITOR
		m_textureNames.Resize(numTextures);
#endif

		m_numClipRegions = token.GetInt();
		m_pClipRegionPool = rage_new Vec4V[m_numClipRegions];

		int numClipRegionsLoaded = 0;
		for (int i=0; i<numTextures; i++)
		{
			char textureName[512];
			token.GetTokenToChar(textureName, 512, ' ');
			m_hashNames[i] = atHashValue(textureName);
			numClipRegionsLoaded += m_clipRegions[i].Load(token, m_pClipRegionPool);
#if RMPTFX_EDITOR
			m_textureNames[i] = textureName;
#endif
		}

		ptxAssertf(numClipRegionsLoaded==m_numClipRegions, "clip region data mismatch (%d %d)", numClipRegionsLoaded, m_numClipRegions);
		pStream->Close();

		return true;
	}
	else
	{
		ptxWarningf("could not load particle clip region data (%s)", pName);
		return false;
	}
}

int ptxClipRegionTable::GetIdx(u32 hashName) const 
{
	return m_hashNames.Find(hashName);
}

void ptxClipRegionTable::GetData(int idx, const Vec4V*& pvClipRegionData, int& numColumns, int& numRows)
{
	pvClipRegionData = m_clipRegions[idx].GetData();
	numColumns = m_clipRegions[idx].GetNumColumns();
	numRows = m_clipRegions[idx].GetNumRows();
}

#if RMPTFX_EDITOR
void ptxClipRegionTable::SendToEditor(ptxByteBuff& buff)
{
	ptxAssertf(m_hashNames.GetCount()==m_clipRegions.GetCount(), "hash name array and clip region array counts don't match");

	buff.Write_s32(m_hashNames.GetCount());

	for (s32 i=0; i<m_hashNames.GetCount(); i++)
	{
		buff.Write_const_char(m_textureNames[i]);
		buff.Write_s32(m_clipRegions[i].m_numColumns);
		buff.Write_s32(m_clipRegions[i].m_numRows);
	}
}
#endif

void ptxClipRegions::Load(const char* pName)
{
	sm_isInitialised = sm_clipRegionTable.Load(pName);
}

bool ptxClipRegions::GetData(u32 textureHashName, const Vec4V*& pvClipRegionData, int& numColumns, int& numRows)
{
	int textureIdx = sm_clipRegionTable.GetIdx(textureHashName);
	if (ptxVerifyf(textureIdx>-1, "cannot find texture in clip region table"))
	{
		sm_clipRegionTable.GetData(textureIdx, pvClipRegionData, numColumns, numRows);
		return true;
	}

	pvClipRegionData = &sm_defaultData;
	numColumns = 1;
	numRows = 1;
	return false;
}

} // namespace rage
