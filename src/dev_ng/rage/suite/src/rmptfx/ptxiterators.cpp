// 
// rmptfx/ptxiterators.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxiterators.h"

// includes (rmptfx)
#include "rmptfx/ptxdrawlist.h"
#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage {


// static variables
ptxEmitterInstIterator ptxEmitterInstIterator::sm_sharedIterator;


// code
template<>
int ptxRuleIterator<ptxEffectRule>::GetNumRules()
{
	return m_pCurrFxList->GetEffectRuleDictionary()->GetCount();
}

template<> ptxEffectRule* ptxRuleIterator<ptxEffectRule>::GetCurrRule()
{
	return m_pCurrFxList->GetEffectRuleDictionary()->GetEntry(m_currRuleIdx);
}

template<>
int ptxRuleIterator<ptxEmitterRule>::GetNumRules()
{
	return m_pCurrFxList->GetEmitterRuleDictionary()->GetCount();
}

template<>
ptxEmitterRule* ptxRuleIterator<ptxEmitterRule>::GetCurrRule()
{
	return m_pCurrFxList->GetEmitterRuleDictionary()->GetEntry(m_currRuleIdx);
}

template<>
int ptxRuleIterator<ptxParticleRule>::GetNumRules()
{
	return m_pCurrFxList->GetParticleRuleDictionary()->GetCount();
}

template<>
ptxParticleRule* ptxRuleIterator<ptxParticleRule>::GetCurrRule()
{
	return m_pCurrFxList->GetParticleRuleDictionary()->GetEntry(m_currRuleIdx);
}








ptxEmitterInstIterator::ptxEmitterInstIterator()
	: m_currDrawListIdx(-1)
	, m_pCurrEffectInstItem(NULL)
	, m_pNextEffectInstItem(NULL)
	, m_numEffectInstEvents(-1) 
	, m_currEffectInstEventIdx(-1)
	, m_pDrawLists(NULL)
{
}

ptxEmitterInstIterator::ptxEmitterInstIterator(ptxManager& manager)
	: m_currDrawListIdx(-1)
	, m_pCurrEffectInstItem(NULL)
	, m_pNextEffectInstItem(NULL)
	, m_numEffectInstEvents(-1) 
	, m_currEffectInstEventIdx(-1)
	, m_pDrawLists(&manager.GetDrawLists())
{
}

bool ptxEmitterInstIterator::Next()
{
	ptxEffectInstItem* pCurrEffectInstItem = NULL;
	atArray<ptxDrawList*>* pDrawLists = NULL;

	// get the next event
NextEvent:
	{
		// increment the event index
		m_currEffectInstEventIdx++;

		// check if this event exists
		if (m_currEffectInstEventIdx<m_numEffectInstEvents)
		{
			// it does
			return true;
		}
		else
		{
			// it doesn't - move to the next effect
			goto NextEffect;
		}
	}

	// get the next effect
NextEffect:
	{
		// move to the next effect inst
		m_pCurrEffectInstItem = m_pNextEffectInstItem;

		// check if this effect inst exists
		if (m_pCurrEffectInstItem)
		{
			// it does
			pCurrEffectInstItem = m_pCurrEffectInstItem;

			// store the next effect inst item
			m_pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNext();

			// get the number of events on the effect inst
			m_numEffectInstEvents = pCurrEffectInstItem->GetDataSB()->GetNumEvents();

			// get the first event 
			m_currEffectInstEventIdx = -1;
			goto NextEvent;
		}
		else
		{
			// it doesn't - move to the next draw list
			goto NextDrawList;
		}
	}

	// get the next draw list
NextDrawList:
	{
		pDrawLists = m_pDrawLists;

		// increment the draw list index
		m_currDrawListIdx++;

		// check if this draw list exists
		if (m_currDrawListIdx<pDrawLists->GetCount())
		{
			// it does - get the first effect inst
			m_pCurrEffectInstItem = NULL;

			ptxDrawList* pCullDrawList = (*pDrawLists)[m_currDrawListIdx];

			m_pNextEffectInstItem = (ptxEffectInstItem*)pCullDrawList->m_effectInstList.GetHead();
			goto NextEffect;
		}
		else
		{
			// it doesn't - we're finished
			return false;
		}
	}
}

ptxEmitterInst* ptxEmitterInstIterator::GetCurrEmitterInst()
{
	ptxEmitterInst* pEmitterInst = GetCurrEffectInst()->GetEventInst(m_currEffectInstEventIdx).GetEmitterInst();
	if (pEmitterInst->GetEmitterRule())
	{
		return pEmitterInst;
	}

	return NULL;
}


} // namespace rage
