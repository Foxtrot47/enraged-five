// 
// rmptfx/ptxiterators.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXITERATORS_H 
#define RMPTFX_PTXITERATORS_H 


// includes
#include "rmptfx/ptxeffectinst.h"


// namespaces
namespace rage {


// forward declarations
class ptxManager;
class ptxDrawList;


// structures
struct ptxFxListInfo
{
	ptxFxListInfo() : m_pFxList(NULL), m_flags(0) {}
	pgRef<ptxFxList> m_pFxList;
	u32 m_flags;
};


// typedefs
typedef atMap<atHashWithStringNotFinal, ptxFxListInfo> ptxFxListMap;


// classes
class ptxFxListIterator
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		inline ptxFxListIterator(ptxFxListMap::Iterator& fxListMapIterator);

		// 
		inline void Start(u32 fxListFlags);
		inline void Next();
		inline bool AtEnd();

		// accessors
		inline ptxFxList* GetCurrFxList();
		inline ptxFxListMap::Iterator& GetRawIter();


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		ptxFxListMap::Iterator m_fxListMapIterator;
		u32 m_fxListFlags;

};

ptxFxListIterator::ptxFxListIterator(ptxFxListMap::Iterator& fxListMapIterator) 
: m_fxListMapIterator(fxListMapIterator)
{
}

void ptxFxListIterator::Start(u32 fxListFlags)
{
	m_fxListFlags = fxListFlags; 
	m_fxListMapIterator.Start(); 
	while (!m_fxListMapIterator.AtEnd() && ((m_fxListMapIterator.GetData().m_flags & m_fxListFlags) == 0)) 
	{
		m_fxListMapIterator.Next();
	}
}

void ptxFxListIterator::Next()
{
	do 
	{
		m_fxListMapIterator.Next();
	} 
	while(!m_fxListMapIterator.AtEnd() && ((m_fxListMapIterator.GetData().m_flags & m_fxListFlags) == 0));
}

bool ptxFxListIterator::AtEnd()
{
	return m_fxListMapIterator.AtEnd();
}

ptxFxList* ptxFxListIterator::GetCurrFxList()
{						   
	ptxAssertf(!AtEnd(), "Off the end of the list!");
	return m_fxListMapIterator.GetData().m_pFxList;
}

ptxFxListMap::Iterator& ptxFxListIterator::GetRawIter()
{
	return m_fxListMapIterator;
}








template<class _T> 
class ptxRuleIterator
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxRuleIterator(ptxFxListIterator fxListIterator);

		void Start(u32 fxListFlags);
		void Next();
		bool AtEnd() {return m_fxListIterator.AtEnd();}

		_T* GetCurrRule();																// explicit specialisation required to get the rule


	private: //////////////////////////

		int GetNumRules();																// explicit specialisation required to get the number of rules
		void SkipToNextNonEmptyList();


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		int m_currRuleIdx;
		pgRef<ptxFxList> m_pCurrFxList; 

		ptxFxListIterator m_fxListIterator;

};

template<typename _T> 
ptxRuleIterator<_T>::ptxRuleIterator(ptxFxListIterator fxListIterator) 
: m_currRuleIdx(0)
, m_pCurrFxList(NULL)
, m_fxListIterator(fxListIterator) 
{

}

template<class _T>
void ptxRuleIterator<_T>::Start(u32 fxListFlags)
{
	m_currRuleIdx = 0;
	m_pCurrFxList = NULL;

	m_fxListIterator.Start(fxListFlags);
	m_pCurrFxList = m_fxListIterator.GetCurrFxList();

	SkipToNextNonEmptyList();
}

template<class _T>
void ptxRuleIterator<_T>::Next() 
{
	if (m_pCurrFxList)
	{
		// move to the next rule index
		m_currRuleIdx++;

		// check if we have moved past the last rule
		if (m_currRuleIdx == GetNumRules())
		{
			// we have - move to the next fx list
			m_currRuleIdx = 0;
			m_fxListIterator.Next();
			SkipToNextNonEmptyList();
		}
	}
}

template<class _T>
void ptxRuleIterator<_T>::SkipToNextNonEmptyList()
{
CheckForEmptyFxList:
	{
		if (m_fxListIterator.AtEnd())
		{
			// we're finished
			m_pCurrFxList = NULL;
			return;
		}
		else
		{
			// get the next fx list
			m_pCurrFxList = m_fxListIterator.GetCurrFxList();

			// check for fx lists with no rules
			if (GetNumRules() == 0)
			{
				// move onto the next fx list
				m_fxListIterator.Next();

				// could recurse here, this seems simpler
				goto CheckForEmptyFxList; 
			}
		}
	}
}

// typedefs
typedef ptxRuleIterator<ptxEffectRule> ptxEffectRuleIterator;
typedef ptxRuleIterator<ptxEmitterRule> ptxEmitterRuleIterator;
typedef ptxRuleIterator<ptxParticleRule> ptxParticleRuleIterator;

// explicit specialisations of the iterators
template<> int ptxRuleIterator<ptxEffectRule>::GetNumRules();
template<> ptxEffectRule* ptxRuleIterator<ptxEffectRule>::GetCurrRule();

template<> int ptxRuleIterator<ptxEmitterRule>::GetNumRules();
template<> ptxEmitterRule* ptxRuleIterator<ptxEmitterRule>::GetCurrRule();

template<> int ptxRuleIterator<ptxParticleRule>::GetNumRules();
template<> ptxParticleRule* ptxRuleIterator<ptxParticleRule>::GetCurrRule();








class ptxEmitterInstIterator
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxEmitterInstIterator();
		ptxEmitterInstIterator(ptxManager& manager);

		// main interface
		bool Next();

		// accessors
		ptxEffectInst* GetCurrEffectInst() {ptxAssert(m_pCurrEffectInstItem); return m_pCurrEffectInstItem ? m_pCurrEffectInstItem->GetDataSB() : NULL;}
		int GetCurrEffectInstEventIdx() {return m_currEffectInstEventIdx;}
		ptxEmitterInst* GetCurrEmitterInst();

		// static accessors
		static void SetSharedIterator(ptxEmitterInstIterator& emitterInstIterator) {sm_sharedIterator = emitterInstIterator;}
		static ptxEmitterInstIterator& GetSharedIterator() {return sm_sharedIterator;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		int m_currDrawListIdx;																	// the current draw list that we are iterating through
		ptxEffectInstItem* m_pCurrEffectInstItem;												// the current effect instance that we are visiting
		ptxEffectInstItem* m_pNextEffectInstItem;												// the next effect instance we are going to visit
		int m_numEffectInstEvents;																// the number of events on the current effect instance
		int m_currEffectInstEventIdx;															// the current event index on the current effect instance
		atArray<ptxDrawList*>* m_pDrawLists;													// pointer to the array of draw lists we are iterating through
		
		char m_pad[8];

		// static variables
		static ptxEmitterInstIterator sm_sharedIterator;										// the shared iterator we are currently using


} ;












} // namespace rage


#endif // RMPTFX_PTXITERATORS_H 
