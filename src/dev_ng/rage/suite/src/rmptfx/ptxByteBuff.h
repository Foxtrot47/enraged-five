
// 
// rmptfx/ptxbytebuff.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXBYTEBUFF_H
#define RMPTFX_PTXBYTEBUFF_H


// includes (rmptfx)
#include "rmptfx/ptxchannel.h"

// includes (rage)
#include "atl/map.h"


// namespaces
namespace rage {


// typedefs
typedef u32 ptxNetId;
typedef atMap<ptxNetId, void*> ptxIdToNetObjMap;
typedef atMap<void*, ptxNetId> ptxNetObjToIdMap;


// defines 
#define PTXBYTEBUFF_INVALID_ID		(0)


// classes
class ptxByteBuff 
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxByteBuff();
		~ptxByteBuff();
		
		void Create(u32 maxSize);
		void Clear() {m_currWritePos = 0; m_currReadPos = 0;}
		bool IsReady();

		void BeginTCPBuffer() {BeginWrite(); Write_u32(0);}
		void EndTCPBuffer() {Write_u32(0, m_currWritePos-4);}

		void BeginRead() {m_currReadPos = 0;}
		void EndRead() {m_currReadPos = m_currWritePos;}
		void SetReadPos(int pos) {m_currReadPos = pos;}

		void BeginWrite() {m_currWritePos = 0;}

		// read/write interface
		bool Read_bool() const;
		void Write_bool(bool data);
		void Write_bool(u32 writePos, bool data);

		u8 Read_u8() const;
		void Write_u8(u8 data);
		void Write_u8(u32 writePos, u8 data);

		s8 Read_s8() const;
		void Write_s8(s8 data);
		void Write_s8(u32 writePos, s8 data);

		u16 Read_u16() const;
		void Write_u16(u16 data);
		void Write_u16(u32 writePos, u16 data);

		s16 Read_s16() const;
		void Write_s16(s16 data);
		void Write_s16(u32 writePos, s16 data);

		u32 Read_u32() const;
		void Write_u32(u32 data);
		void Write_u32(u32 writePos, u32 data);

		s32 Read_s32() const;
		void Write_s32(s32 data);
		void Write_s32(u32 writePos,s32 data);

		float Read_float() const;
		void Write_float(float data);

		const char* Read_const_char() const;
		void Write_const_char(const char* pData);

		// accessors
		u8* GetBuffer() {return m_pBuffer;}
		int GetCurrWritePos() {return m_currWritePos;}
		void SetCurrWritePos(int pos) {m_currWritePos = pos;}
		int GetCurrReadPos() {return m_currReadPos;}
		void SetCurrReadPos(int pos) {m_currReadPos = pos;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		u32 m_size;
		mutable u32 m_currReadPos;
		u32 m_currWritePos;
		u8* m_pBuffer;
};	

} // namespace rage

#endif //RMPTFX_BYTEBUFF_H
