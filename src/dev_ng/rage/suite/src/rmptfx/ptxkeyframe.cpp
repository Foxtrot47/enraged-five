// 
// rmptfx/ptxkeyframe.cpp
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxkeyframe.h"
#include "rmptfx/ptxkeyframe_parser.h"

// includes (rmptfx)
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxkeyframewidget.h"
#include "rmptfx/ptxbytebuff.h"

// includes (rage)
#include "atl/array_struct.h"
#include "bank/bank.h"
#include "data/safestruct.h"
#include "parser/manager.h"
#include "system/param.h"


// optimisations
RMPTFX_OPTIMISATIONS()

// namespaces
using namespace rage;



// code
ptxKeyframeDefn* ptxKeyframeDefn::Clone()
{
	ptxKeyframeDefn* pNewDefn = rage_new ptxKeyframeDefn;
	pNewDefn->m_name = m_name;
	for (int i=0; i<PTXKEYFRAME_MAX_CHANNELS; i++)
	{
		pNewDefn->m_labels[i] = m_labels[i];
		pNewDefn->m_colours[i] = m_colours[i];
	}
	pNewDefn->m_vDefaultValue = m_vDefaultValue;
	pNewDefn->m_vMinMaxDeltaX = m_vMinMaxDeltaX;
	pNewDefn->m_vMinMaxDeltaY = m_vMinMaxDeltaY;
	pNewDefn->m_propertyId = m_propertyId;
	pNewDefn->m_type = m_type;
	pNewDefn->m_useColourGradient = m_useColourGradient;

	return pNewDefn;
}




#if PTXKEYFRAME_VERSION==0

ptxKeyframeEntry::ptxKeyframeEntry() 
{
	m_vTime = ScalarV(V_ZERO); 
	m_vValue = Vec4V(V_ZERO); 
	m_vDelta = Vec4V(V_ZERO);
}

ptxKeyframeEntry::ptxKeyframeEntry(datResource& rsc)
: m_vValue(rsc)
, m_vDelta(rsc)
{
	m_vTime = SplatX(Vec4V(m_vTime));

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxKeyframeEntry_num++;
#endif
}

#if __DECLARESTRUCT
void ptxKeyframeEntry::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(ptxKeyframeEntry);
		STRUCT_FIELD(m_vTime);
		STRUCT_FIELD(m_vValue);
		STRUCT_FIELD(m_vDelta);
	STRUCT_END();
}
#endif

IMPLEMENT_PLACE(ptxKeyframeEntry);

void ptxKeyframeEntry::ComputeDelta(const ptxKeyframeEntry& nextEntry) 
{
#if __ASSERT
	if (IsLessThanOrEqualAll(nextEntry.m_vTime, m_vTime))
	{
		ptxAssertf(0, "keyframe entry time is not later than the previous entry's time");
		m_vDelta = Vec4V(V_ZERO);
		return;
	}
#endif

	ScalarV vNorm = Invert(nextEntry.m_vTime-m_vTime);
	m_vDelta = Scale((nextEntry.m_vValue-m_vValue), vNorm);
}

void ptxKeyframeEntry::operator=(const ptxKeyframeEntry& other)
{
	m_vTime = other.m_vTime;
	m_vValue = other.m_vValue;
	m_vDelta = other.m_vDelta;
}

#if RMPTFX_EDITOR
bool ptxKeyframeEntry::operator==(const ptxKeyframeEntry& other)
{
	if (IsEqualAll(m_vTime, other.m_vTime) &&
		IsEqualAll(m_vValue, other.m_vValue) &&
		IsEqualAll(m_vDelta, other.m_vDelta))
	{
		return true;
	}

	return false;
}
#endif



ptxKeyframe::ptxKeyframe()
: m_pWidget(NULL)
, m_pDefn(NULL)
{
	Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
}

ptxKeyframe::~ptxKeyframe()
{
	m_subsequentEntries.Reset();
}

ptxKeyframe::ptxKeyframe(datResource& rsc) 
: m_initialEntry(rsc)
, m_subsequentEntries(rsc, 1)
{
#if RMPTFX_BANK
	// Could do this in a PostLoad function, but this is simpler and it saves us from traversing the /whole/ ptx object graph to find all the keyframes
	if (!datResource_IsDefragmentation)
	{
		m_pDefn = NULL;
		m_pWidget = NULL;
	}
#endif

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxKeyframe_num++;
#endif
}

#if __DECLARESTRUCT
void ptxKeyframe::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxKeyframe)
		SSTRUCT_FIELD(ptxKeyframe, m_initialEntry)
		SSTRUCT_FIELD(ptxKeyframe, m_subsequentEntries)
		SSTRUCT_FIELD_VP(ptxKeyframe, m_pWidget)
		SSTRUCT_FIELD_VP(ptxKeyframe, m_pDefn)
	SSTRUCT_END(ptxKeyframe)
}
#endif

IMPLEMENT_PLACE(ptxKeyframe);

void ptxKeyframe::PreLoadCallback(parTreeNode* pNode)
{
	// reset the keyframe entries
	Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// load the xml data into the keyframe xml data
	ptxKeyframeXmlData xmlData;
	{
		// use this instead of LoadObject so we don't assert that "keyData" isn't a known type
		sysMemAutoUseTempMemory mem;
		PARSER.LoadFromStructure(pNode->FindChildWithName("keyData"), *PARSER.GetStructure(&xmlData), &xmlData);
	}

	// check the loaded data is valid (4 channels plus a time for each entry)
	ptxAssert(xmlData.m_keyEntryData.GetCount() == xmlData.m_numKeyEntries*(PTXKEYFRAME_MAX_CHANNELS+1));

	// set the loaded data
	if (xmlData.m_numKeyEntries>0)
	{
		// resize the subsequent entries array to the correct size
		if (xmlData.m_numKeyEntries>1)
		{
			m_subsequentEntries.Resize(xmlData.m_numKeyEntries-1);
		}

		// read the key data into the entries
		int position = 0;
		for (int i=0; i<GetNumEntries(); i++)
		{
			// read the data from the xml key data
			float keyTime = xmlData.m_keyEntryData[position++];
			float keyDataX = xmlData.m_keyEntryData[position++];
			float keyDataY = xmlData.m_keyEntryData[position++];
			float keyDataZ = xmlData.m_keyEntryData[position++];
			float keyDataW = xmlData.m_keyEntryData[position++];

			// set the data on the entry
			ptxKeyframeEntry& kfEntry = GetEntry(i);
			kfEntry.Set(ScalarVFromF32(keyTime), Vec4V(keyDataX, keyDataY, keyDataZ, keyDataW));
		}

		// tidy up 
		{
			sysMemAutoUseTempMemory mem;
			xmlData.m_keyEntryData.Reset();
		}

		// compute the new keyframe deltas
		ComputeDeltas();
	}
}

void ptxKeyframe::PostSaveCallback(parTreeNode* RMPTFX_BANK_ONLY(pNode))
{
#if RMPTFX_BANK
	// create some xml data to fill out and save
	ptxKeyframeXmlData xmlData;
	xmlData.m_numKeyEntries = GetNumEntries();
	xmlData.m_keyEntryData.Reserve(xmlData.m_numKeyEntries*(PTXKEYFRAME_MAX_CHANNELS+1));

	// go through the entries adding the data to the xml data
	for (int i=0; i<GetNumEntries(); i++)
	{
		// read data from the entry
		float keyTime = GetEntry(i).GetTime().Getf();
		Vec4V vKeyValue = GetEntry(i).GetValue();

		// add to the xml data
		xmlData.m_keyEntryData.Append() = keyTime;
		xmlData.m_keyEntryData.Append() = vKeyValue[0];
		xmlData.m_keyEntryData.Append() = vKeyValue[1];
		xmlData.m_keyEntryData.Append() = vKeyValue[2];
		xmlData.m_keyEntryData.Append() = vKeyValue[3];
	}

	// add the xml data to the tree
	parTreeNode* pSaveNode = PARSER.BuildTreeNode(&xmlData);
	pSaveNode->GetElement().SetName("keyData", false);
	pSaveNode->InsertAsChildOf(pNode);
#else
	// FWIW we just need to make sure we preserve strings for the ptxKeyframeXmlData object to get
	// this to work in other builds. Will the keyframe editor need it?
	parAssertf(0, "Can't save keyframes in non-BANK builds (see Russ if you need to)");
#endif
}

void ptxKeyframe::Init(ScalarV_In vTime, Vec4V_In vValue)
{
	// set the initial entry with the input data and remove all subsequent entries
	m_initialEntry.Set(vTime, vValue);
	m_subsequentEntries.Reset();
}

void ptxKeyframe::AddEntry(ScalarV_In vTime, Vec4V_In vValue)
{
	// get the keyframe to set
	ptxKeyframeEntry* pEntry = NULL;

	// check if the new key time is the same as the initial entry
	if (IsEqualAll(m_initialEntry.GetTime(), vTime))
	{
		// use the initial entry to write the new data into
		pEntry = &m_initialEntry;
	}
	// check if the new key time is before the initial entry
	else if (IsGreaterThanAll(m_initialEntry.GetTime(), vTime))
	{
		// it is - expand the subsequent entries if required
		if (m_subsequentEntries.GetCount()==m_subsequentEntries.GetCapacity())
		{
			// grow the subsequent entries 
			// this will cause the count to increment as well so make sure we pop an entry so the count remains the same
			m_subsequentEntries.Grow(1);
			m_subsequentEntries.Pop();
		}

		// insert a new slot at the start of the subsequent entries and set it to the existing initial entry data
		m_subsequentEntries.Insert(0) = m_initialEntry;
		
		// use the current initial entry to write the new data into
		pEntry = &m_initialEntry;
	}
	else
	{
		// look for the correct place in the subsequent entries to add the new keyframe
		for (int i=0; i<m_subsequentEntries.GetCount(); i++)
		{
			// check if the new key time is the same as this entry
			if (IsEqualAll(m_subsequentEntries[i].GetTime(), vTime))
			{
				// use this entry to write the new data into
				pEntry = &m_subsequentEntries[i];
				break;
			}
			else
			{
				// check if the new key time is before this entry
				if (IsGreaterThanAll(m_subsequentEntries[i].GetTime(), vTime))
				{
					// it is - expand the subsequent entries if required
					if (m_subsequentEntries.GetCount()==m_subsequentEntries.GetCapacity())
					{
						// grow the subsequent entries 
						// this will cause the count to increment as well so make sure we pop an entry so the count remains the same
						m_subsequentEntries.Grow(1); 
						m_subsequentEntries.Pop();
					}

					// insert a new slot here and use it to write the new data into
					pEntry = &m_subsequentEntries.Insert(i);
					break;
				}
			}
		}

		// check if we've reached the end of the entries
		if (pEntry==NULL)
		{
			// we have - grow the subsequent entries and use it to write the new data into
			pEntry = &m_subsequentEntries.Grow(1);
		}
	}

	// check we found a valid entry
	ptxAssertf(pEntry, "couldn't find an inserion point at time %f", vTime.Getf());

	// set the data on the entry
	pEntry->Set(vTime, vValue);

	// recompute the deltas
	ComputeDeltas();
}

void ptxKeyframe::DeleteEntry(int entryId)
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	if (entryId==0)
	{
		if (GetNumEntries()>1)
		{
			m_initialEntry = m_subsequentEntries[0];
			m_subsequentEntries.Delete(0);
		}
		else
		{
			m_initialEntry.Set(ScalarV(V_ZERO), Vec4V(V_ZERO));
		}
	}
	else
	{
		m_subsequentEntries.Delete(entryId-1);
	}
}

ptxKeyframeEntry& ptxKeyframe::GetEntry(int entryId)
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	if (entryId==0)
	{
		return m_initialEntry;
	}
	else
	{
		return m_subsequentEntries[entryId-1];
	}
}

float ptxKeyframe::GetTime(int entryId) const
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	if (entryId==0)
	{
		return m_initialEntry.GetTime().Getf();
	}
	else
	{
		return m_subsequentEntries[entryId-1].GetTime().Getf();
	}
}

ScalarV_Out ptxKeyframe::GetTimeV(int entryId) const
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	if (entryId==0)
	{
		return m_initialEntry.GetTime();
	}
	else
	{
		return m_subsequentEntries[entryId-1].GetTime();
	}
}

Vec4V_Out ptxKeyframe::GetValue(int entryId) const
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	if (entryId==0)
	{
		return m_initialEntry.GetValue();
	}
	else
	{
		return m_subsequentEntries[entryId-1].GetValue();
	}
}

void ptxKeyframe::SetEntry(int entryId, float time, Vec4V_In vValue)
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	if (entryId==0)
	{
		return m_initialEntry.Set(ScalarVFromF32(time), vValue);
	}
	else
	{
		return m_subsequentEntries[entryId-1].Set(ScalarVFromF32(time), vValue);
	}
}

void ptxKeyframe::SetDefn(ptxKeyframeDefn* pKeyframeDefn) 
{
	if (m_pDefn!=pKeyframeDefn)
	{
#if !__RESOURCECOMPILER
		m_pDefn = pKeyframeDefn;
#endif
	}
}

ptxKeyframeDefn* ptxKeyframe::GetDefn() 
{
	return m_pDefn;
}

void ptxKeyframe::CopyEntriesFrom(const ptxKeyframe& other)
{
	// check we aren't copying from ourself
	if (this!=&other) 
	{  
		// copy the initial entry
		m_initialEntry = other.m_initialEntry;

		// reset the subsequent entries and resize
		m_subsequentEntries.Reset();
		m_subsequentEntries.Reserve(other.GetNumEntries()-1);

		// copy over the subsequent entries
		for (int i=0; i<other.m_subsequentEntries.GetCount(); i++)
		{
			ptxKeyframeEntry& data = m_subsequentEntries.Append();
			data = other.m_subsequentEntries[i];
		}

#if RMPTFX_BANK
		UpdateWidget();
#endif
	}
}

void ptxKeyframe::CopyDefnFrom(ptxKeyframe& other)
{
	// check we aren't copying from ourself
	if (this!=&other) 
	{  
		// copy the definition
#if !__RESOURCECOMPILER
		m_pDefn = other.m_pDefn;
#endif

#if RMPTFX_BANK
		UpdateWidget();
#endif
	}
}

#if RMPTFX_BANK || RMPTFX_EDITOR
bool ptxKeyframe::IsDefault()
{
	if (GetNumEntries()==1)
	{
		if (ptxVerifyf(m_pDefn, "keyframe doesn't have a valid definition"))
		{
			if (m_pDefn->GetType()==PTXKEYFRAMETYPE_FLOAT)
			{
				return GetValue(0).GetXf()==m_pDefn->GetDefaultValue().GetXf();
			}
			else if (m_pDefn->GetType()==PTXKEYFRAMETYPE_FLOAT2)
			{
				return GetValue(0).GetXf()==m_pDefn->GetDefaultValue().GetXf() && 
					   GetValue(0).GetYf()==m_pDefn->GetDefaultValue().GetYf();
			}
			else if (m_pDefn->GetType()==PTXKEYFRAMETYPE_FLOAT3)
			{
				return GetValue(0).GetXf()==m_pDefn->GetDefaultValue().GetXf() && 
					   GetValue(0).GetYf()==m_pDefn->GetDefaultValue().GetYf() && 
					   GetValue(0).GetZf()==m_pDefn->GetDefaultValue().GetZf();
			}
			else if (m_pDefn->GetType()==PTXKEYFRAMETYPE_FLOAT4)
			{
				return GetValue(0).GetXf()==m_pDefn->GetDefaultValue().GetXf() && 
					   GetValue(0).GetYf()==m_pDefn->GetDefaultValue().GetYf() && 
					   GetValue(0).GetZf()==m_pDefn->GetDefaultValue().GetZf() && 
					   GetValue(0).GetWf()==m_pDefn->GetDefaultValue().GetWf();
			}
		}
	}

	return false;
}
#endif

#if RMPTFX_BANK || RMPTFX_EDITOR
void ptxKeyframe::ScaleEntries(ScalarV_In vScale)
{
	// scale the initial entry
	m_initialEntry.ScaleValue(vScale);

	// scale the subsequent entries
	for (int i=0; i<m_subsequentEntries.GetCount(); i++)
	{
		m_subsequentEntries[i].ScaleValue(vScale);
	}

	// compute the new keyframe deltas
	ComputeDeltas();
}
#endif

ptxKeyframe& ptxKeyframe::operator=(const ptxKeyframe& other)
{
	CopyEntriesFrom(other);
	return *this;
}

#if RMPTFX_EDITOR
bool ptxKeyframe::operator==(const ptxKeyframe& other)
{
	// check if the number of entries is the same
	if (GetNumEntries()!=other.GetNumEntries())
	{
		// it's not - return not equal
		return false;
	}

	// check if the initial entry is the same
	if (!(m_initialEntry==other.m_initialEntry))
	{
		// it's not - return not equal
		return false;
	}

	// check if the subsequent entries are the same
	for (int i=0; i<m_subsequentEntries.GetCount(); i++)
	{
		if (!(m_subsequentEntries[i]==other.m_subsequentEntries[i]))
		{
			// this one isn't - return not equal
			return false;
		}
	}

	// everything is the same
	return true;
}
#endif

#if RMPTFX_BANK
void ptxKeyframe::Reset(int newSize)
{
	// resize the subsequent entries if required
	if (newSize!=GetNumEntries()) 
	{
		m_subsequentEntries.Reset();
		if (newSize>1)
		{
			m_subsequentEntries.Resize(newSize-1);
		}
	}

	// reset the data 
	m_initialEntry.Set(ScalarV(V_ZERO), Vec4V(V_ZERO));
	for (int i=0; i<newSize-1; i++) 
	{
		GetEntry(i).Set(ScalarV(V_ZERO), Vec4V(V_ZERO));
	}
}
#endif

#if RMPTFX_BANK
void ptxKeyframe::AddWidgets(bkBank& bank)
{
	m_pWidget = rage_new ptxKeyframeWidget(bank, *this, m_pDefn->GetName(), "");

	bank.AddWidget(*m_pWidget);
}
#endif

#if RMPTFX_BANK
void ptxKeyframe::UpdateWidget()
{
	if (m_pWidget) 
	{
		m_pWidget->UpdateUIData();
		m_pWidget->RemoteUpdate();
	}
}
#endif

void ptxKeyframe::ComputeDeltas()
{
	if (GetNumEntries()>1) 
	{
		// set the initial delta
		m_initialEntry.ComputeDelta(m_subsequentEntries[0]);

		// set the subsequent deltas
		for (int i=0; i<m_subsequentEntries.GetCount()-1; i++)
		{
			m_subsequentEntries[i].ComputeDelta(m_subsequentEntries[i+1]);
		}
	}
}

#elif PTXKEYFRAME_VERSION==1

ptxKeyframeEntry::ptxKeyframeEntry() 
{
	m_vTime = Vec4V(V_ZERO); 
	m_vValue = Vec4V(V_ZERO); 
}

ptxKeyframeEntry::ptxKeyframeEntry(datResource& UNUSED_PARAM(rsc))
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxKeyframeEntry_num++;
#endif
}

#if __DECLARESTRUCT
void ptxKeyframeEntry::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(ptxKeyframeEntry);
		STRUCT_FIELD(m_vTime);
		STRUCT_FIELD(m_vValue);
	STRUCT_END();
}
#endif

IMPLEMENT_PLACE(ptxKeyframeEntry);



ptxKeyframe::ptxKeyframe()
: m_pWidget(NULL)
, m_pDefn(NULL)
{
	Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
}

ptxKeyframe::~ptxKeyframe()
{
	m_entries.Reset();
}

ptxKeyframe::ptxKeyframe(datResource& rsc) 
: m_entries(rsc, 1)
{
#if RMPTFX_BANK
	// Could do this in a PostLoad function, but this is simpler and it saves us from traversing the /whole/ ptx object graph to find all the keyframes
	if (!datResource_IsDefragmentation)
	{
		m_pDefn = NULL;
		m_pWidget = NULL;
	}
#endif

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxKeyframe_num++;
#endif
}

#if __DECLARESTRUCT
void ptxKeyframe::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxKeyframe)
		SSTRUCT_FIELD(ptxKeyframe, m_entries)
		SSTRUCT_FIELD_VP(ptxKeyframe, m_pWidget)
		SSTRUCT_FIELD_VP(ptxKeyframe, m_pDefn)
	SSTRUCT_END(ptxKeyframe)
}
#endif

IMPLEMENT_PLACE(ptxKeyframe);

void ptxKeyframe::PreLoadCallback(parTreeNode* pNode)
{
	// reset the time and value arrays
	//Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// during the ragebuilder process we don't want to allocate memory by adding a default value to the array 
	// only for it to be reset and reallocated later during PreLoadCallback 
	// the memory allocations in the ragebuilder process get tracked and this ends up bloating our resourced assets
#if !__RESOURCECOMPILER
	m_entries.Reset();
#endif

	// load the xml data into the keyframe xml data
	ptxKeyframeXmlData xmlData;
	{
		// use this instead of LoadObject so we don't assert that "keyData" isn't a known type
		sysMemAutoUseTempMemory mem;
		PARSER.LoadFromStructure(pNode->FindChildWithName("keyData"), *PARSER.GetStructure(&xmlData), xmlData.parser_GetPointer());
	}

	// check the loaded data is valid (4 channels plus a time for each entry)
	ptxAssert(xmlData.m_keyEntryData.GetCount() == xmlData.m_numKeyEntries*(PTXKEYFRAME_MAX_CHANNELS+1));

	// set the loaded data
	if (xmlData.m_numKeyEntries>0)
	{
		// resize the subsequent entries array to the correct size
		m_entries.Reset();
		m_entries.Resize(xmlData.m_numKeyEntries);

		// read the key data into the entries
		int position = 0;
		for (int i=0; i<GetNumEntries(); i++)
		{
			// read the data from the xml key data
			float keyTime = xmlData.m_keyEntryData[position++];
			float keyDataX = xmlData.m_keyEntryData[position++];
			float keyDataY = xmlData.m_keyEntryData[position++];
			float keyDataZ = xmlData.m_keyEntryData[position++];
			float keyDataW = xmlData.m_keyEntryData[position++];

			// set the data 
			m_entries[i].m_vTime.SetXf(keyTime);
			m_entries[i].m_vValue = Vec4V(keyDataX, keyDataY, keyDataZ, keyDataW);
		}

		// tidy up 
		{
			sysMemAutoUseTempMemory mem;
			xmlData.m_keyEntryData.Reset();
		}

		// compute the new keyframe deltas
		ComputeDeltas();
	}
}

void ptxKeyframe::PostSaveCallback(parTreeNode* RMPTFX_BANK_ONLY(pNode))
{
#if RMPTFX_BANK
	// create some xml data to fill out and save
	ptxKeyframeXmlData xmlData;
	xmlData.m_numKeyEntries = GetNumEntries();
	xmlData.m_keyEntryData.Reserve(xmlData.m_numKeyEntries*(PTXKEYFRAME_MAX_CHANNELS+1));

	// go through the entries adding the data to the xml data
	for (int i=0; i<GetNumEntries(); i++)
	{
		// read data from the entry
		float keyTime = m_entries[i].m_vTime.GetXf();
		Vec4V vKeyValue = m_entries[i].m_vValue;

		// add to the xml data
		xmlData.m_keyEntryData.Append() = keyTime;
		xmlData.m_keyEntryData.Append() = vKeyValue[0];
		xmlData.m_keyEntryData.Append() = vKeyValue[1];
		xmlData.m_keyEntryData.Append() = vKeyValue[2];
		xmlData.m_keyEntryData.Append() = vKeyValue[3];
	}

	// add the xml data to the tree
	parTreeNode* pSaveNode = PARSER.BuildTreeNode(&xmlData);
	pSaveNode->GetElement().SetName("keyData", false);
	pSaveNode->InsertAsChildOf(pNode);
#else
	// FWIW we just need to make sure we preserve strings for the ptxKeyframeXmlData object to get
	// this to work in other builds. Will the keyframe editor need it?
	parAssertf(0, "Can't save keyframes in non-BANK builds (see Russ if you need to)");
#endif
}

void ptxKeyframe::Init(ScalarV_In vTime, Vec4V_In vValue)
{
	// during the ragebuilder process we don't want to allocate memory by adding a default value to the array 
	// only for it to be reset and reallocated later during PreLoadCallback 
	// the memory allocations in the ragebuilder process get tracked and this ends up bloating our resourced assets
#if !__RESOURCECOMPILER
	m_entries.Reset();
	AddEntry(vTime, vValue);
#else
	(void)vTime; (void)vValue;
#endif
}

void ptxKeyframe::AddEntry(ScalarV_In vTime, Vec4V_In vValue)
{
	float time = vTime.Getf();

	ptxKeyframeEntry* pEntry = NULL;

	// look for the correct place in the arrays to add the new entry
	for (int i=0; i<m_entries.GetCount(); i++)
	{
		// check if the new key time is the same as this entry
		if (time==m_entries[i].m_vTime.GetXf())
		{
			// it is - set the new value here
			pEntry = &m_entries[i];
			break;
		}
		else
		{
			// check if the new key time is before this entry
			if (time<=m_entries[i].m_vTime.GetXf())
			{
				// it is - expand the arrays, if required
				if (m_entries.GetCount()==m_entries.GetCapacity())
				{
					// grow the entries array 
					// this will cause the count to increment as well so make sure we pop an entry so the count remains the same
					m_entries.Grow(1); 
					m_entries.Pop();
				}

				// insert a new entry 
				pEntry = &m_entries.Insert(i);
				break;
			}
		}
	}

	// check if we've reached the end of the entries
	if (pEntry==NULL)
	{
		// we have - grow the arrays and use it to write the new data into
		pEntry = &m_entries.Grow(1);
	}

	ptxAssertf(pEntry, "couldn't find an insertion point at time %f", time);

	pEntry->m_vTime.SetXf(time);
	pEntry->m_vValue = vValue;

	// recompute the deltas
	ComputeDeltas();
}

void ptxKeyframe::DeleteEntry(int entryId)
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	m_entries.Delete(entryId);
}

float ptxKeyframe::GetTime(int entryId) const
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	return m_entries[entryId].m_vTime.GetXf();
}

ScalarV_Out ptxKeyframe::GetTimeV(int entryId) const
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	return m_entries[entryId].m_vTime.GetX();
}

Vec4V_Out ptxKeyframe::GetValue(int entryId) const
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	return m_entries[entryId].m_vValue;
}

void ptxKeyframe::SetEntry(int entryId, float time, Vec4V_In vValue)
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	m_entries[entryId].m_vTime.SetXf(time);
	m_entries[entryId].m_vValue = vValue;
}

void ptxKeyframe::SetDefn(ptxKeyframeDefn* pKeyframeDefn) 
{
	if (m_pDefn!=pKeyframeDefn)
	{
#if !__RESOURCECOMPILER
		m_pDefn = pKeyframeDefn;
#endif
	}
}

ptxKeyframeDefn* ptxKeyframe::GetDefn() 
{
	return m_pDefn;
}

void ptxKeyframe::CopyEntriesFrom(const ptxKeyframe& other)
{
	// check we aren't copying from ourself
	if (this!=&other) 
	{  
		// reset the arrays
		m_entries.Reset();
		m_entries.Reserve(other.GetNumEntries());

		// copy over the data
		for (int i=0; i<other.GetNumEntries(); i++)
		{
			ptxKeyframeEntry& entry = m_entries.Append();
			entry = other.m_entries[i];
		}

#if RMPTFX_BANK
		UpdateWidget();
#endif
	}
}

void ptxKeyframe::CopyDefnFrom(ptxKeyframe& other)
{
	// check we aren't copying from ourself
	if (this!=&other) 
	{  
		// copy the definition
#if !__RESOURCECOMPILER
		m_pDefn = other.m_pDefn;
#endif

#if RMPTFX_BANK
		UpdateWidget();
#endif
	}
}

#if RMPTFX_BANK || RMPTFX_EDITOR
bool ptxKeyframe::IsDefault() const
{
	if (GetNumEntries()==1)
	{
		if (ptxVerifyf(m_pDefn, "keyframe doesn't have a valid definition"))
		{
			if (m_pDefn->GetType()==PTXKEYFRAMETYPE_FLOAT)
			{
				return GetValue(0).GetXf()==m_pDefn->GetDefaultValue().GetXf();
			}
			else if (m_pDefn->GetType()==PTXKEYFRAMETYPE_FLOAT2)
			{
				return GetValue(0).GetXf()==m_pDefn->GetDefaultValue().GetXf() && 
					   GetValue(0).GetYf()==m_pDefn->GetDefaultValue().GetYf();
			}
			else if (m_pDefn->GetType()==PTXKEYFRAMETYPE_FLOAT3)
			{
				return GetValue(0).GetXf()==m_pDefn->GetDefaultValue().GetXf() && 
					   GetValue(0).GetYf()==m_pDefn->GetDefaultValue().GetYf() && 
					   GetValue(0).GetZf()==m_pDefn->GetDefaultValue().GetZf();
			}
			else if (m_pDefn->GetType()==PTXKEYFRAMETYPE_FLOAT4)
			{
				return GetValue(0).GetXf()==m_pDefn->GetDefaultValue().GetXf() && 
					   GetValue(0).GetYf()==m_pDefn->GetDefaultValue().GetYf() && 
					   GetValue(0).GetZf()==m_pDefn->GetDefaultValue().GetZf() && 
					   GetValue(0).GetWf()==m_pDefn->GetDefaultValue().GetWf();
			}
		}
	}

	return false;
}
#endif

#if RMPTFX_BANK || RMPTFX_EDITOR
void ptxKeyframe::ScaleEntries(ScalarV_In vScale)
{
	// scale the values
	for (int i=0; i<GetNumEntries(); i++)
	{
		m_entries[i].m_vValue *= vScale;
	}

	// compute the new keyframe deltas
	ComputeDeltas();
}
#endif

ptxKeyframe& ptxKeyframe::operator=(const ptxKeyframe& other)
{
	CopyEntriesFrom(other);
	return *this;
}

#if RMPTFX_EDITOR
bool ptxKeyframe::operator==(const ptxKeyframe& other)
{
	// default keyframes have no entries
	if ((GetNumEntries()==0 && other.IsDefault()) ||
		(IsDefault() && other.GetNumEntries()==0))
	{
		return true;
	}

	// check if the number of entries is the same
	if (GetNumEntries()!=other.GetNumEntries())
	{
		// it's not - return not equal
		return false;
	}

	// check if the times and values are the same
	for (int i=0; i<GetNumEntries(); i++)
	{
		if (!IsEqualAll(m_entries[i].m_vTime.GetX(), other.m_entries[i].m_vTime.GetX()))
		{
			// this one isn't - return not equal
			return false;
		}

		if (!IsEqualAll(m_entries[i].m_vValue, other.m_entries[i].m_vValue))
		{
			// this one isn't - return not equal
			return false;
		}
	}

	// everything is the same
	return true;
}
#endif

#if RMPTFX_BANK
void ptxKeyframe::Reset(int newSize)
{
	// resize the subsequent entries if required
	if (newSize!=GetNumEntries()) 
	{
		m_entries.Reset();
		m_entries.Resize(newSize);
	}

	// reset the data 
	for (int i=0; i<newSize; i++) 
	{
		m_entries[i].m_vTime = Vec4V(V_ZERO);
		m_entries[i].m_vValue = Vec4V(V_ZERO);
	}
}
#endif

#if RMPTFX_BANK
void ptxKeyframe::AddWidgets(bkBank& bank)
{
	m_pWidget = rage_new ptxKeyframeWidget(bank, *this, m_pDefn->GetName(), "");

	bank.AddWidget(*m_pWidget);
}
#endif

#if RMPTFX_BANK
void ptxKeyframe::UpdateWidget()
{
	if (m_pWidget) 
	{
		m_pWidget->UpdateUIData();
		m_pWidget->RemoteUpdate();
	}
}
#endif

void ptxKeyframe::ComputeDeltas()
{
	if (GetNumEntries()>1) 
	{
		// set the subsequent deltas
		for (int i=1; i<GetNumEntries(); i++)
		{
			m_entries[i].m_vTime.SetYf(1.0f / (m_entries[i].m_vTime.GetXf() - m_entries[i-1].m_vTime.GetXf()));
		}
	}
}

#elif PTXKEYFRAME_VERSION==2

ptxKeyframe::ptxKeyframe()
: m_pWidget(NULL)
, m_pDefn(NULL)
, m_entryStride(0)
{
	Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
}

ptxKeyframe::~ptxKeyframe()
{
	m_entries.Reset();
}

ptxKeyframe::ptxKeyframe(datResource& rsc) 
: m_entries(rsc)
{
#if RMPTFX_BANK
	// Could do this in a PostLoad function, but this is simpler and it saves us from traversing the /whole/ ptx object graph to find all the keyframes
	if (!datResource_IsDefragmentation)
	{
		m_pDefn = NULL;
		m_pWidget = NULL;
	}
#endif

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxKeyframe_num++;
#endif
}

#if __DECLARESTRUCT
void ptxKeyframe::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxKeyframe)
		SSTRUCT_FIELD(ptxKeyframe, m_entries)
		SSTRUCT_FIELD_VP(ptxKeyframe, m_pWidget)
		SSTRUCT_FIELD_VP(ptxKeyframe, m_pDefn)
		SSTRUCT_FIELD(ptxKeyframe, m_entryStride)
		SSTRUCT_IGNORE(ptxKeyframe, m_pad)
	SSTRUCT_END(ptxKeyframe)
}
#endif

IMPLEMENT_PLACE(ptxKeyframe);

void ptxKeyframe::PreLoadCallback(parTreeNode* pNode)
{
	// reset the time and value arrays
	//Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// during the ragebuilder process we don't want to allocate memory by adding a default value to the array 
	// only for it to be reset and reallocated later during PreLoadCallback 
	// the memory allocations in the ragebuilder process get tracked and this ends up bloating our resourced assets
#if !__RESOURCECOMPILER
	m_entries.Reset();
#endif

	// load the xml data into the keyframe xml data
	ptxKeyframeXmlData xmlData;
	{
		// use this instead of LoadObject so we don't assert that "keyData" isn't a known type
		sysMemAutoUseTempMemory mem;
		PARSER.LoadFromStructure(pNode->FindChildWithName("keyData"), *PARSER.GetStructure(&xmlData), &xmlData);
	}

	// check the loaded data is valid (4 channels plus a time for each entry)
	ptxAssert(xmlData.m_keyEntryData.GetCount() == xmlData.m_numKeyEntries*(PTXKEYFRAME_MAX_CHANNELS+1));

	// set the loaded data
	if (xmlData.m_numKeyEntries>0)
	{
		// resize the subsequent entries array to the correct size
		m_entries.Reset();
		m_entries.Resize(xmlData.m_numKeyEntries*m_entryStride);

		// read the key data into the entries
		int position = 0;
		for (int i=0; i<GetNumEntries(); i++)
		{
			// read the data from the xml key data
			float keyTime = xmlData.m_keyEntryData[position++];
			float keyData[4];
			keyData[0] = xmlData.m_keyEntryData[position++];
			keyData[1] = xmlData.m_keyEntryData[position++];
			keyData[2] = xmlData.m_keyEntryData[position++];
			keyData[3] = xmlData.m_keyEntryData[position++];

			// set the data 
			float* pEntry = _GetEntry(i);

			pEntry[PTXKEYFRAME_ENTRY_FIELD_TIME] = keyTime;

			for (int j=PTXKEYFRAME_ENTRY_FIELD_VALUE; j<m_entryStride; j++)
			{
				pEntry[j] = keyData[j-PTXKEYFRAME_ENTRY_FIELD_VALUE];
			}
		}

		// tidy up 
		{
			sysMemAutoUseTempMemory mem;
			xmlData.m_keyEntryData.Reset();
		}

		// compute the new keyframe deltas
		ComputeDeltas();
	}
}

void ptxKeyframe::PostSaveCallback(parTreeNode* RMPTFX_BANK_ONLY(pNode))
{
#if RMPTFX_BANK
	// create some xml data to fill out and save
	ptxKeyframeXmlData xmlData;
	xmlData.m_numKeyEntries = GetNumEntries();
	xmlData.m_keyEntryData.Reserve(xmlData.m_numKeyEntries*(PTXKEYFRAME_MAX_CHANNELS+1));

	// go through the entries adding the data to the xml data
	for (int i=0; i<GetNumEntries(); i++)
	{
		float* pEntry = _GetEntry(i);

		// add to the xml data
		xmlData.m_keyEntryData.Append() = pEntry[PTXKEYFRAME_ENTRY_FIELD_TIME];
		for (int j=PTXKEYFRAME_ENTRY_FIELD_VALUE; j<m_entryStride; j++)
		{
			xmlData.m_keyEntryData.Append() = pEntry[j];
		}
		for (int j=m_entryStride; j<PTXKEYFRAME_ENTRY_FIELD_VALUE+PTXKEYFRAME_MAX_CHANNELS; j++)
		{
			xmlData.m_keyEntryData.Append() = 0.0f;
		}
	}

	// add the xml data to the tree
	parTreeNode* pSaveNode = PARSER.BuildTreeNode(&xmlData);
	pSaveNode->GetElement().SetName("keyData", false);
	pSaveNode->InsertAsChildOf(pNode);
#else
	// FWIW we just need to make sure we preserve strings for the ptxKeyframeXmlData object to get
	// this to work in other builds. Will the keyframe editor need it?
	parAssertf(0, "Can't save keyframes in non-BANK builds (see Russ if you need to)");
#endif
}

void ptxKeyframe::Init(ScalarV_In vTime, Vec4V_In vValue)
{
	m_entryStride = 6;
	if (m_pDefn)
	{
		if		(m_pDefn->GetType() == PTXKEYFRAMETYPE_FLOAT)	m_entryStride = 3;
		else if (m_pDefn->GetType() == PTXKEYFRAMETYPE_FLOAT2)	m_entryStride = 4;
		else if (m_pDefn->GetType() == PTXKEYFRAMETYPE_FLOAT3)	m_entryStride = 5;
		else if (m_pDefn->GetType() == PTXKEYFRAMETYPE_FLOAT4)	m_entryStride = 6;
	}

	// during the ragebuilder process we don't want to allocate memory by adding a default value to the array 
	// only for it to be reset and reallocated later during PreLoadCallback 
	// the memory allocations in the ragebuilder process get tracked and this ends up bloating our resourced assets
#if !__RESOURCECOMPILER
	m_entries.Reset();
	AddEntry(vTime, vValue);
#else
	(void)vTime; (void)vValue;
#endif
}

void ptxKeyframe::AddEntry(ScalarV_In vTime, Vec4V_In vValue)
{
	float time = vTime.Getf();

	float* pEntry = NULL;

	// look for the correct place in the arrays to add the new entry
	for (int i=0; i<GetNumEntries(); i++)
	{
		pEntry = _GetEntry(i);

		// check if the new key time is the same as this entry
		if (time==pEntry[PTXKEYFRAME_ENTRY_FIELD_TIME])
		{
			// it is - set the new value here
			break;
		}
		else
		{
			// check if the new key time is before this entry
			if (time<=pEntry[PTXKEYFRAME_ENTRY_FIELD_TIME])
			{
				// it is - expand the arrays, if required
				if (m_entries.GetCount()==m_entries.GetCapacity())
				{
					// grow the entries array 
					// this will cause the count to increment as well so make sure we pop an entry so the count remains the same
					m_entries.Grow(m_entryStride); 
					m_entries.Pop();
				}

				// insert a new entry 
				for (int j=0; j<m_entryStride; j++)
				{
					pEntry = &m_entries.Insert(i*m_entryStride);
				}

				break;
			}
		}
	}

	// check if we've reached the end of the entries
	if (pEntry==NULL)
	{
		// we have - grow the arrays and use it to write the new data into
		pEntry = &m_entries.Grow(m_entryStride);
		for (int j=1; j<m_entryStride; j++)
		{
			m_entries.Append();
		}
	}

	ptxAssertf(pEntry, "couldn't find an insertion point at time %f", time);

	pEntry[PTXKEYFRAME_ENTRY_FIELD_TIME] = time;
	for (int j=PTXKEYFRAME_ENTRY_FIELD_VALUE; j<m_entryStride; j++)
	{
		pEntry[j] = vValue[j-PTXKEYFRAME_ENTRY_FIELD_VALUE];
	}

	// recompute the deltas
	ComputeDeltas();
}

void ptxKeyframe::DeleteEntry(int entryId)
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	for (int j=0; j<m_entryStride; j++)
	{
		m_entries.Delete(entryId*m_entryStride);
	}

	// recompute the deltas
	ComputeDeltas();
}

float ptxKeyframe::GetTime(int entryId) const
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	return _GetEntry(entryId)[PTXKEYFRAME_ENTRY_FIELD_TIME];
}

ScalarV_Out ptxKeyframe::GetTimeV(int entryId) const
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	return ScalarVFromF32(_GetEntry(entryId)[PTXKEYFRAME_ENTRY_FIELD_TIME]);
}

Vec4V_Out ptxKeyframe::GetValue(int entryId) const
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	const float* pReturn = &_GetEntry(entryId)[PTXKEYFRAME_ENTRY_FIELD_VALUE];
	Vec4V vReturn = Vec4V(pReturn[0], pReturn[1], pReturn[2], pReturn[3]);

	return vReturn;
}

void ptxKeyframe::SetEntry(int entryId, float time, Vec4V_In vValue)
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	_SetEntry(entryId, time, &vValue[0]);
}

void ptxKeyframe::SetDefn(ptxKeyframeDefn* pKeyframeDefn) 
{
	if (m_pDefn!=pKeyframeDefn)
	{
#if !__RESOURCECOMPILER
		m_pDefn = pKeyframeDefn;
#endif
	}
}

ptxKeyframeDefn* ptxKeyframe::GetDefn() 
{
	return m_pDefn;
}

void ptxKeyframe::CopyEntriesFrom(const ptxKeyframe& other)
{
	// check we aren't copying from ourself
	if (this!=&other) 
	{  
		// reset the arrays
		m_entries.Reset();
		m_entries.Reserve(other.GetNumEntries()*m_entryStride);

		// copy over the data
		for (int i=0; i<other.GetNumEntries(); i++)
		{
			const float* pOtherEntry = other._GetEntry(i);

			for (int j=0; j<m_entryStride; j++)
			{
				float& val = m_entries.Append();
				val = pOtherEntry[j];
			}
		}

#if RMPTFX_BANK
		UpdateWidget();
#endif
	}
}

void ptxKeyframe::CopyDefnFrom(ptxKeyframe& other)
{
	// check we aren't copying from ourself
	if (this!=&other) 
	{  
		// copy the definition
#if !__RESOURCECOMPILER
		m_pDefn = other.m_pDefn;
#endif

#if RMPTFX_BANK
		UpdateWidget();
#endif
	}
}

#if RMPTFX_BANK || RMPTFX_EDITOR
bool ptxKeyframe::IsDefault()
{
	if (GetNumEntries()==1)
	{
		if (ptxVerifyf(m_pDefn, "keyframe doesn't have a valid definition"))
		{
			if (m_pDefn->GetType()==PTXKEYFRAMETYPE_FLOAT)
			{
				return GetValue(0).GetXf()==m_pDefn->GetDefaultValue().GetXf();
			}
			else if (m_pDefn->GetType()==PTXKEYFRAMETYPE_FLOAT2)
			{
				return GetValue(0).GetXf()==m_pDefn->GetDefaultValue().GetXf() && 
					   GetValue(0).GetYf()==m_pDefn->GetDefaultValue().GetYf();
			}
			else if (m_pDefn->GetType()==PTXKEYFRAMETYPE_FLOAT3)
			{
				return GetValue(0).GetXf()==m_pDefn->GetDefaultValue().GetXf() && 
					   GetValue(0).GetYf()==m_pDefn->GetDefaultValue().GetYf() && 
					   GetValue(0).GetZf()==m_pDefn->GetDefaultValue().GetZf();
			}
			else if (m_pDefn->GetType()==PTXKEYFRAMETYPE_FLOAT4)
			{
				return GetValue(0).GetXf()==m_pDefn->GetDefaultValue().GetXf() && 
					   GetValue(0).GetYf()==m_pDefn->GetDefaultValue().GetYf() && 
					   GetValue(0).GetZf()==m_pDefn->GetDefaultValue().GetZf() && 
					   GetValue(0).GetWf()==m_pDefn->GetDefaultValue().GetWf();
			}
		}
	}

	return false;
}
#endif

#if RMPTFX_BANK || RMPTFX_EDITOR
void ptxKeyframe::ScaleEntries(ScalarV_In vScale)
{
	float scale = vScale.Getf();

	// scale the values
	for (int i=0; i<GetNumEntries(); i++)
	{
		float* pEntry = _GetEntry(i);
		
		for (int j=PTXKEYFRAME_ENTRY_FIELD_VALUE; j<m_entryStride; j++)
		{
			pEntry[j] *= scale;
		}
	}

	// compute the new keyframe deltas
	ComputeDeltas();
}
#endif

ptxKeyframe& ptxKeyframe::operator=(const ptxKeyframe& other)
{
	CopyEntriesFrom(other);
	return *this;
}

#if RMPTFX_EDITOR
bool ptxKeyframe::operator==(const ptxKeyframe& other)
{
	// check if the number of entries is the same
	if (GetNumEntries()!=other.GetNumEntries())
	{
		// it's not - return not equal
		return false;
	}

	// check if the times and values are the same
	for (int i=0; i<GetNumEntries(); i++)
	{
		float* pEntry = _GetEntry(i);
		const float* pOtherEntry = other._GetEntry(i);

		for (int j=0; j<m_entryStride; j++)
		{
			if (pEntry[j]!=pOtherEntry[j])
			{
				return false;
			}
		}	
	}

	// everything is the same
	return true;
}
#endif

#if RMPTFX_BANK
void ptxKeyframe::Reset(int newSize)
{
	// resize the subsequent entries if required
	if (newSize!=GetNumEntries()) 
	{
		m_entries.Reset();
		m_entries.Resize(newSize*m_entryStride);
	}

	// reset the data 
	for (int i=0; i<newSize; i++) 
	{
		float* pEntry = _GetEntry(i);

		for (int j=0; j<m_entryStride; j++)
		{
			pEntry[j] = 0.0f;
		}
	}
}
#endif

#if RMPTFX_BANK
void ptxKeyframe::AddWidgets(bkBank& bank)
{
	m_pWidget = rage_new ptxKeyframeWidget(bank, *this, m_pDefn->GetName(), "");

	bank.AddWidget(*m_pWidget);
}
#endif

#if RMPTFX_BANK
void ptxKeyframe::UpdateWidget()
{
	if (m_pWidget) 
	{
		m_pWidget->UpdateUIData();
		m_pWidget->RemoteUpdate();
	}
}
#endif

void ptxKeyframe::_SetEntry(int entryId, float time, const float* pValue)
{
	ptxAssertf(entryId<GetNumEntries(), "invalid entry id");

	float* pEntry = _GetEntry(entryId);

	pEntry[PTXKEYFRAME_ENTRY_FIELD_TIME] = time;

	for (int i=PTXKEYFRAME_ENTRY_FIELD_VALUE; i<m_entryStride; i++)
	{
		pEntry[i] = pValue[i-PTXKEYFRAME_ENTRY_FIELD_VALUE];
	}
}

void ptxKeyframe::ComputeDeltas()
{
	if (GetNumEntries()>1) 
	{
		// set the subsequent deltas
		for (int i=1; i<GetNumEntries(); i++)
		{
			float* pPrevEntry = _GetEntry(i-1);
			float* pCurrEntry = _GetEntry(i);

			pCurrEntry[PTXKEYFRAME_ENTRY_FIELD_INVDT] = 1.0f / (pCurrEntry[PTXKEYFRAME_ENTRY_FIELD_TIME]-pPrevEntry[PTXKEYFRAME_ENTRY_FIELD_TIME]);
		}
	}
}

#endif // PTXKEYFRAME_VERSION



