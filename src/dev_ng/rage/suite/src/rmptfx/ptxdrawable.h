// 
// rmptfx/ptxdrawable.h
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXDRAWABLE_H
#define RMPTFX_PTXDRAWABLE_H


// includes (rmptfx)
#include "rmcore/drawable.h"
#include "parser/macros.h"

// includes (rage)


// namespaces
namespace rage
{

// classes
// ptxDrawable
class ptxDrawable
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxDrawable();
		~ptxDrawable() {}

		DECLARE_PLACE(ptxDrawable);
		ptxDrawable(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		void PostLoad();

		void SetName(const char* pName) {m_name = pName; m_hashName = atHashValue(pName);}
		ConstString& GetName() {return m_name;}

		u32 GetHashName() {return m_hashName;}

		void SetDrawable(rmcDrawable* pDrawable) {m_pDrawable = pDrawable;}
		rmcDrawable* GetDrawable() {return m_pDrawable;}

		void SetInfo();
		Vec4V_Out GetBoundInfo() {return m_vBoundInfo;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		Vec4V m_vBoundInfo;							// bounding box width, height, depth and bounding sphere radius
		ConstString m_name;
		datRef<rmcDrawable> m_pDrawable;
		u32 m_hashName;
		datPadding<4> m_pad;

		PAR_SIMPLE_PARSABLE;

} ;

// ptxDrawableEntry
class ptxDrawableEntry
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxDrawableEntry() {m_ownedByRMPTFX=true; m_pDrawable=NULL;}
		~ptxDrawableEntry() {}

		DECLARE_PLACE(ptxDrawableEntry);
		ptxDrawableEntry(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif

		ptxDrawableEntry& operator=(const ptxDrawableEntry& other) 
		{
			m_pDrawable = other.m_pDrawable;
			m_ownedByRMPTFX = other.m_ownedByRMPTFX;
			return *this;
		}

		void SetDrawable(rmcDrawable* pDrawable) {m_pDrawable = pDrawable;}
		rmcDrawable* GetDrawable() {return m_pDrawable;}

		void SetIsOwnedByRMPTFX(bool val) {m_ownedByRMPTFX = val;}
		bool GetIsOwnedByRMPTFX() {return m_ownedByRMPTFX;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		rmcDrawable* m_pDrawable;
		bool m_ownedByRMPTFX;
		datPadding<3> m_pad;

		PAR_SIMPLE_PARSABLE;

};


} // namespace rage

#endif // RMPTFX_PTXDRAWABLE_H
