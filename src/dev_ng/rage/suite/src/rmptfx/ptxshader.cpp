// 
// rmptfx/ptxshader.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxshader.h"
#include "rmptfx/ptxshader_parser.h"

// includes (rmptfx)
#include "rmptfx/ptxbytebuff.h"
#include "rmptfx/ptxchannel.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"

// includes
#include "grcore/config.h"
#include "grcore/viewport.h"
#include "grmodel/model.h"
#include "parser/manager.h"
#include "system/nelem.h"
#include "system/timer.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// defines 
#define PTXSTR_EFFECTUI_NAME					"UIName"
#define PTXSTR_EFFECTUI_HINT					"UIHint"
#define PTXSTR_EFFECTUI_ASSETNAME				"UIAssetName"
#define PTXSTR_EFFECTUI_KEYFRAMEABLE			"Keyframeable"

#define PTXSTR_TECHNIQUENAME_DEFAULT			"draw"
#define PTXSTR_TECHNIQUENAME_UNKNOWN			"Unknown"

#define PTXSTR_SHADERNAME_SPRITE				"ptfx_sprite"
#define PTXSTR_SHADERNAME_TRAIL					"ptfx_trail"

#define PTXSTR_GLOBALSHADERVAR_ASPECTRATIO		"AspectRatio"
#define PTXSTR_GLOBALSHADERVAR_DEPTHMAP			"DepthMap"
#define PTXSTR_GLOBALSHADERVAR_CAMERAINVERSE	"CameraInverse"
#define PTXSTR_GLOBALSHADERVAR_NEARFARPLANE		"NearFarPlane"
#define PTXSTR_GLOBALSHADERVAR_INVSCREENSIZE	"InvScreenSize"

#define PTXSTR_TREENODE_SHADERTEMPLATENAME		"shaderTemplateName"
#define PTXSTR_TREENODE_INSTVARS				"instVars"
#define PTXSTR_TREENODE_VARNAME					"name"
#define PTXSTR_TREENODE_VARTYPE					"type"


// namespaces
namespace rage
{

// static variables
atStringMap<ptxShaderTemplate*>	ptxShaderTemplateList::sm_shaders;
atStringMap<grcEffectGlobalVar>	ptxShaderTemplateList::sm_globalVars;
const int ptxShaderTemplateList::sm_numShaderNames = PTXSHADER_MAX_SHADERS;
const char* ptxShaderTemplateList::sm_pShaderNames[PTXSHADER_MAX_SHADERS] =	
{	
	PTXSTR_SHADERNAME_SPRITE,
	PTXSTR_SHADERNAME_TRAIL,
};


// code
void ptxShaderTemplateList::Shutdown()
{
	sysMemAutoUseTempMemory temp;

	for (int i=0; i<sm_shaders.GetCount(); i++) 
	{
		ptxShaderTemplate** ppShaderTemplate = sm_shaders.GetItem(i);
		delete *ppShaderTemplate;
		*ppShaderTemplate = NULL;
	}

	sm_shaders.Reset();

	sm_globalVars.Reset();
}

ptxShaderTemplate* ptxShaderTemplateList::LoadShader(const char* pName)
{
	// Shader templates should all go into the temp heap
	sysMemAutoUseTempMemory temp;

	// check for invalid name
	if (strlen(pName)==0) 
	{
		return NULL;
	}

	// check if already loaded
	ptxShaderTemplate* pShaderTemplate = GetShader(pName);
	if (pShaderTemplate) 
	{
		// return the loaded shader template
		return pShaderTemplate;
	}

	// create the shader
	grmShader* pShader = NULL;

	if (grcEffect::Create((pName)))
	{
		pShader = grmShaderFactory::GetInstance().Create();
		pShader->Load(pName);

		static bool first = true;
		if (first)
		{
			LookUpGlobalVars();
			first = false;
		}

		// add to the template list
		pShaderTemplate = rage_new ptxShaderTemplate();
		pShaderTemplate->SetGrmShader(pShader);
		sm_shaders.InsertSorted(pName, pShaderTemplate);
	}

	return pShaderTemplate;
}

ptxShaderTemplate* ptxShaderTemplateList::GetShader(const char* pName)
{
	ptxShaderTemplate** ppShaderTemplate = sm_shaders.SafeGet(pName);
	if (ppShaderTemplate && *ppShaderTemplate)
	{
		return *ppShaderTemplate;
	}

	return NULL;
}

void ptxShaderTemplateList::SetGlobalVars()
{
	// inverse camera
	grcEffectGlobalVar* pGlobalVar = sm_globalVars.SafeGet(PTXSTR_GLOBALSHADERVAR_CAMERAINVERSE);
	Mat34V camInverse;
	InvertTransformOrtho(camInverse,grcViewport::GetCurrent()->GetCameraMtx());
	if (pGlobalVar && *pGlobalVar != grcegvNONE)
	{
		grcEffect::SetGlobalVar(*pGlobalVar, camInverse);
	}

	// aspect ratio
	pGlobalVar = sm_globalVars.SafeGet(PTXSTR_GLOBALSHADERVAR_ASPECTRATIO);
	Vec4V vRatio(grcViewport::GetCurrent()->GetProjection().GetCol0().GetX(),grcViewport::GetCurrent()->GetProjection().GetCol1().GetY(),Vec2V(V_ZERO));
	if (pGlobalVar && *pGlobalVar != grcegvNONE)
	{
		grcEffect::SetGlobalVar(*pGlobalVar, vRatio);
	}

	// near and far place
	pGlobalVar = sm_globalVars.SafeGet(PTXSTR_GLOBALSHADERVAR_NEARFARPLANE);
	if (pGlobalVar && *pGlobalVar != grcegvNONE)
	{
		// pre-compute sub expressions so the shader doesn't have to
		float nearClip = grcViewport::GetCurrent()->GetNearClip();
		float farClip = grcViewport::GetCurrent()->GetFarClip();
		float scale = -(farClip - nearClip);
		float add = -scale - farClip;
		float scale2 = 1.0f/-(nearClip * farClip);
		scale *= scale2;
		add *= scale2;

		Vec4V vNearFarPlane(grcViewport::GetCurrent()->GetNearClip(), grcViewport::GetCurrent()->GetFarClip(), scale, add);

		grcEffect::SetGlobalVar(*pGlobalVar, vNearFarPlane);
	}

	// depth map
	pGlobalVar = sm_globalVars.SafeGet(PTXSTR_GLOBALSHADERVAR_DEPTHMAP);
	const grcTexture* pDepthMap = RMPTFXMGR.GetDepthBuffer();
	if (pGlobalVar && *pGlobalVar != grcegvNONE)
	{
		if (pDepthMap)
		{
			grcEffect::SetGlobalVar(*pGlobalVar, pDepthMap);
		}
	}

	// inverse screen size
	pGlobalVar = sm_globalVars.SafeGet(PTXSTR_GLOBALSHADERVAR_INVSCREENSIZE);
	if (pGlobalVar && *pGlobalVar != grcegvNONE)
	{
		float width = static_cast<float>( GRCDEVICE.GetWidth() );
		float height = static_cast<float>( GRCDEVICE.GetHeight() );

		Vec4V vInvScreenSize(1.0f/width, 1.0f/height, 0.51f/width, 0.51f/height);
		grcEffect::SetGlobalVar(*pGlobalVar, vInvScreenSize);
	}
}

#if RMPTFX_EDITOR
void ptxShaderTemplateList::SendToEditor(ptxByteBuff& buff)
{
	int currBuffPos = buff.GetCurrWritePos();
	buff.Write_u8(1);

	int count = 0;
	for (int i=0; i<grcEffect::GetEffectCount(); i++)
	{
		grcEffect* pEffect = grcEffect::GetEffect(i);
		if (pEffect)
		{
			for (int j=0; j<sm_numShaderNames; j++)
			{
				if (strcmp(pEffect->GetEffectName(), sm_pShaderNames[j])==0)
				{
					buff.Write_const_char(pEffect->GetEffectName());
					count++;
				}
			}
		}
	}
	buff.Write_u8(currBuffPos, (u8)count);
}
#endif

void ptxShaderTemplateList::LookUpGlobalVars()
{
	sysMemAutoUseTempMemory mem;

	grcEffectGlobalVar globalVar = grcEffect::LookupGlobalVar(PTXSTR_GLOBALSHADERVAR_ASPECTRATIO, false);
	sm_globalVars.InsertSorted(PTXSTR_GLOBALSHADERVAR_ASPECTRATIO, globalVar);

	globalVar = grcEffect::LookupGlobalVar(PTXSTR_GLOBALSHADERVAR_DEPTHMAP, false);
	sm_globalVars.InsertSorted(PTXSTR_GLOBALSHADERVAR_DEPTHMAP, globalVar);

	globalVar = grcEffect::LookupGlobalVar(PTXSTR_GLOBALSHADERVAR_CAMERAINVERSE, false);
	sm_globalVars.InsertSorted(PTXSTR_GLOBALSHADERVAR_CAMERAINVERSE, globalVar);

	globalVar = grcEffect::LookupGlobalVar(PTXSTR_GLOBALSHADERVAR_NEARFARPLANE, false);
	sm_globalVars.InsertSorted(PTXSTR_GLOBALSHADERVAR_NEARFARPLANE, globalVar);

	globalVar = grcEffect::LookupGlobalVar(PTXSTR_GLOBALSHADERVAR_INVSCREENSIZE, false);
	sm_globalVars.InsertSorted(PTXSTR_GLOBALSHADERVAR_INVSCREENSIZE, globalVar);
}


#if RMPTFX_BANK
ptxShaderTemplateOverride ptxShaderTemplateOverride::sm_shaderDebugOverrideAlpha;
ptxShaderTemplate* ptxShaderTemplateOverride::sm_pSpriteShader = NULL;
#endif

#if RMPTFX_BANK
void ptxShaderTemplateOverride::InitDebug()
{
	// shaders
	sm_pSpriteShader = ptxShaderTemplateList::LoadShader("ptfx_sprite");

	if (sm_pSpriteShader != NULL)
	{
		grmShader* pShader = sm_pSpriteShader->GetGrmShader();
		grcEffectTechnique technique = pShader->LookupTechnique("DebugOverride_Alpha", false);

		if (technique != grcetNONE)
		{
			sm_shaderDebugOverrideAlpha.Init(pShader);
			sm_shaderDebugOverrideAlpha.OverrideAllTechniquesWith(technique);
		}
	}
}
#endif

#if RMPTFX_BANK
void ptxShaderTemplateOverride::BindDebugTechniqueOverride(ptxShaderDebugOverrideId id)
{
	// set shader overrides
	if (id == PTXSHADERDEBUG_ALPHA)
	{
		sm_pSpriteShader->SetShaderOverride(&sm_shaderDebugOverrideAlpha);
	}
}
#endif

#if RMPTFX_BANK
void ptxShaderTemplateOverride::UnbindDebugTechniqueOverride()
{
	sm_pSpriteShader->SetShaderOverride(NULL);
}
#endif

void ptxShaderTemplateOverride::Init(grmShader *pSrcShader)
{
	ptxAssert(pSrcShader);
	
	m_techniqueOverrides.Resize(pSrcShader->GetTechniqueCount()+1); // Technique indices start at 1, not 0.
	memset(m_techniqueOverrides.GetElements(),0,sizeof(techniqueOverride)*m_techniqueOverrides.GetCount());
}

void ptxShaderTemplateOverride::DisableAllTechniques()
{
	// m_techniqueOverrides only has a single copy used by all subrender
	// threads, so assert that it is never modified from a subrender thread.
	MULTIPLE_RENDER_THREADS_ONLY(ptxAssert(!g_IsSubRenderThread);)

	for (int i = 0; i<m_techniqueOverrides.GetCount(); i++)
	{
		m_techniqueOverrides[i].bDisable = true;
	}
}

void ptxShaderTemplateOverride::EnableAllTechniques()
{
	MULTIPLE_RENDER_THREADS_ONLY(ptxAssert(!g_IsSubRenderThread);)
	for (int i = 0; i<m_techniqueOverrides.GetCount(); i++)
	{
		m_techniqueOverrides[i].bDisable = false;
	}
}

void ptxShaderTemplateOverride::EnableTechnique(grcEffectTechnique enabledTechnique)
{
	MULTIPLE_RENDER_THREADS_ONLY(ptxAssert(!g_IsSubRenderThread);)
	ptxAssertf(enabledTechnique < m_techniqueOverrides.GetCount(), "Selected Technique is invalid");
	m_techniqueOverrides[enabledTechnique].bDisable = false;
}

void ptxShaderTemplateOverride::DisableTechnique(grcEffectTechnique enabledTechnique)
{
	MULTIPLE_RENDER_THREADS_ONLY(ptxAssert(!g_IsSubRenderThread);)
	ptxAssertf(enabledTechnique < m_techniqueOverrides.GetCount(), "Selected Technique is invalid");
	m_techniqueOverrides[enabledTechnique].bDisable = true;
}

void ptxShaderTemplateOverride::OverrideTechniqueWith(grcEffectTechnique overridenTechnique, grcEffectTechnique overrideTechnique)
{
	MULTIPLE_RENDER_THREADS_ONLY(ptxAssert(!g_IsSubRenderThread);)

	ptxAssertf(overridenTechnique < m_techniqueOverrides.GetCount(), "Overriden Technique is invalid");
	ptxAssertf(overrideTechnique < m_techniqueOverrides.GetCount(), "Override Technique is invalid");

	m_techniqueOverrides[overridenTechnique].bDisable = false;
	m_techniqueOverrides[overridenTechnique].bOverride = true;
	m_techniqueOverrides[overridenTechnique].techniqueId = (u16)overrideTechnique;
}

void ptxShaderTemplateOverride::OverrideAllTechniquesWith(grcEffectTechnique technique)
{
	MULTIPLE_RENDER_THREADS_ONLY(ptxAssert(!g_IsSubRenderThread);)
	for (int i = 0; i<m_techniqueOverrides.GetCount(); i++)
	{
		m_techniqueOverrides[i].bDisable = false;
		m_techniqueOverrides[i].bOverride = true;
		m_techniqueOverrides[i].techniqueId = (u16)technique;
	}
}

ptxShaderTemplate::ptxShaderTemplate()
{
	m_pShader = NULL;
	sysMemSet(m_pShaderOverride, 0, sizeof(m_pShaderOverride));
	for (unsigned i=0; i<NELEM(m_RenderPassToUse); ++i)
	{
		m_RenderPassToUse[i] = PTX_DEFAULT_PASS;
	}
}

ptxShaderTemplate::~ptxShaderTemplate()
{
	sysMemAutoUseTempMemory temp;
	for (int j=0; j<m_shaderVars.GetCount(); j++)
	{
		ptxShaderVar** ppShaderVar = m_shaderVars.GetItem(j);
		delete *ppShaderVar;
	}
	delete m_pShader;
}

void ptxShaderTemplate::SetGrmShader(grmShader* pShader)
{
	if (pShader==NULL)
	{
		return;
	}

	m_pShader = pShader;

	formatf(m_shaderName, sizeof(m_shaderName), "%s", pShader->GetName());

#if EFFECT_PRESERVE_STRINGS
	int numVars = pShader->GetInstancedVariableCount();
	for (int i=0; i<numVars; i++)
	{
		grmVariableInfo varInfo;
		pShader->GetInstancedVariableInfo(i, varInfo);

		ptxShaderVar* pShaderVar = CreateShaderVar(varInfo);
		grcEffectVar effectVar = pShader->LookupVar(varInfo.m_Name, true);

		if (pShaderVar)
		{
			pShaderVar->SetInfo(varInfo);
			pShaderVar->SetId(effectVar);
			pShaderVar->SetHashName(atFinalHashString(varInfo.m_Name).GetHash());
			pShaderVar->SetDefaultData(pShader);
			m_shaderVars.InsertSorted(varInfo.m_Name, pShaderVar);
		}
		else
		{
			ptxMsg::AssetErrorf("unsupported variable type in shader (%s)", pShader->GetName());
		}
	}
#else
	grcEffect& effect = pShader->GetInstanceData().GetBasis();
	int numVars = effect.GetVarCount();
	for (int i=0; i<numVars; i++)
	{
		grcEffectVar effectVar = effect.GetVarByIndex(i);

		if (!effect.HasAnnotation(effectVar, PTXSTR_EFFECTUI_NAME))
		{
			continue;
		}

		grmVariableInfo varInfo;
		u32 varHashName;
		effect.GetVarDesc(effectVar, varHashName, varInfo.m_Type);
		varInfo.m_UiHint = effect.GetAnnotationData(effectVar,ATSTRINGHASH(PTXSTR_EFFECTUI_HINT, 0xcddc9ea3),(char*)NULL);
		varInfo.m_AssetName = effect.GetAnnotationData(effectVar,ATSTRINGHASH(PTXSTR_EFFECTUI_ASSETNAME, 0xe6c32dcd),(char*)NULL);

		ptxShaderVar* pShaderVar = CreateShaderVar(varInfo);
		if (pShaderVar)
		{
			grmVariableInfo* pVarInfo = rage_new grmVariableInfo();
			pShaderVar->SetInfo(*pVarInfo);
			pShaderVar->SetId(effectVar);
			pShaderVar->SetHashName(varHashName);
			pShaderVar->SetDefaultData(pShader);
			m_shaderVars.InsertSorted(varHashName, pShaderVar);
		}
		else
		{
			ptxMsg::AssetErrorf("unsupported variable type in shader (%s)", pShader->GetName());
		}
	}
#endif

	InitProgVars(pShader);
}

void ptxShaderTemplate::ClearShaderVars()
{
	if (m_pShader==NULL) 
	{
		return;
	}

	if (m_progVars[PTXPROGVAR_FRAMEMAP]!=grcevNONE) 
	{
		m_pShader->SetVar(m_progVars[PTXPROGVAR_FRAMEMAP], (grcRenderTarget*)NULL);
	}
}

void ptxShaderTemplate::SetShaderVars(int blendSet, Vec4V_In vChannelMask)
{
	if (m_pShader==NULL) 
	{
		return;
	}

	if (m_progVars[PTXPROGVAR_FRAMEMAP]!=grcevNONE) 
	{
		m_pShader->SetVar(m_progVars[PTXPROGVAR_FRAMEMAP], RMPTFXMGR.GetFrameBufferSampler());
	}

	MULTIPLE_RENDER_THREADS_ONLY(ptxAssert(g_IsSubRenderThread);)
	const unsigned rti = g_RenderThreadIndex;

	if (m_progVars[PTXPROGVAR_BLENDMODE]!=grcevNONE) 
	{
		float blendModeVar = 0.0f;			// BLEND_MODE_NORMAL
		m_BlendSet[rti] = 0;

		if (blendSet==grcbsNormal)
		{
			blendModeVar = 1.0f;			// BLEND_MODE_NORMAL
			m_BlendSet[rti] = 0;
		}
		else if (blendSet==grcbsAdd)
		{
			blendModeVar = 2.0f;			// BLEND_MODE_ADD
			m_BlendSet[rti] = 1;
		}

		m_pShader->SetVar(m_progVars[PTXPROGVAR_BLENDMODE], blendModeVar);
	}
	else
	{
		// In case m_progVars[PTXPROGVAR_BLENDMODE] gets stripped.
		m_BlendSet[rti] = 0;

		if (blendSet==grcbsNormal)
		{
			m_BlendSet[rti] = 0; // BLEND_MODE_NORMAL
		}
		else if (blendSet==grcbsAdd)
		{
			m_BlendSet[rti] = 1; // BLEND_MODE_ADD
		}
	}
		
	if (m_progVars[PTXPROGVAR_CHANNELMASK]!=grcevNONE) 
	{
		m_pShader->SetVar(m_progVars[PTXPROGVAR_CHANNELMASK], vChannelMask);
	}

	if(RMPTFXMGR.GetShaderSetProgVarsFunctor())
	{
		RMPTFXMGR.GetShaderSetProgVarsFunctor()(this);
	}
}

bool ptxShaderTemplate::SyncShaderVars(ptxInstVars* pInstVars)
{
	sysMemAutoUseTempMemory temp;

	// default to being in sync
	bool isInSync = true;

	// check if inst shader var exists on the template
	ptxInstVars& instVars = *pInstVars;
	for (int i=instVars.GetCount()-1; i>=0; i--)
	{
		// try to get the template shader var with the same name as the inst shader var
		ptxShaderVar** ppTemplateShaderVar = NULL;
		if (instVars[i]->GetHashName())
		{
			ppTemplateShaderVar = m_shaderVars.SafeGet(instVars[i]->GetHashName());
		}

		// check the template shader var exists and is the same type
		if (ppTemplateShaderVar && *ppTemplateShaderVar && CompareShaderVarType(*ppTemplateShaderVar, instVars[i]))
		{
			// valid - populate the inst shader data with that of the template
			instVars[i]->CopyUiDataFromTemplate(*ppTemplateShaderVar);
		}
		else
		{
#if !__NO_OUTPUT
			if (!ppTemplateShaderVar)
			{
				ptxDebugf3("Mismatch when Synchronizing particle shader variables: Cound't find the template shader var with name (item didn't exist) %s", instVars[i]->GetInfo() ? instVars[i]->GetInfo()->m_Name : "no info");
			}
			else if (!(*ppTemplateShaderVar))
			{
				ptxDebugf3("Mismatch when Synchronizing particle shader variables: Cound't find the template shader var with name (entry exists but was null) %s", instVars[i]->GetInfo() ? instVars[i]->GetInfo()->m_Name : "no info");
			}
			else
			{
				ptxDebugf3("Mismatch when Synchronizing particle shader variables: Shader Var %s (type %u) and Inst Var %s (type %u) have different types",
					(*ppTemplateShaderVar)->GetInfo() ? (*ppTemplateShaderVar)->GetInfo()->m_Name : "no info",
					(*ppTemplateShaderVar)->GetType(),
					instVars[i]->GetInfo() ? instVars[i]->GetInfo()->m_Name : "no info",
					instVars[i]->GetType());
			}
#endif

			// not valid - delete the inst shader var
			if (!datResource::GetCurrent())			// this could be called from the resource constructor but we don't want to free resource memory
			{
				delete instVars[i];
			}
			instVars.Delete(i);

			// mark as not in sync
			isInSync = false;
		}
	}

	// we now have only shader vars that exists in the inst and the template
	// if the counts do not equal (because our template has more) then we need to append
	if (m_shaderVars.GetCount() == instVars.GetCount())
	{
		// the counts match - sort the inst vars and return
		instVars.QSort(0, -1, ptxShaderTemplate::SortByVarID);
		return isInSync;
	}

	// there are more template shader vars than inst shader vars 
	isInSync = false;

	// return out of sync if on template shader vars exist
	if (m_shaderVars.GetCount()==0)
	{
		ptxDebugf3("Mismatch when Synchronizing particle shader variables: There there is 0 shader vars and we have %d inst vars", instVars.GetCount());
		return isInSync;
	}

	// allow new rule creation to not cause an error
	if (instVars.GetCount()==0)
	{
		isInSync = true;
	}
#if !__NO_OUTPUT
	else
	{
		ptxDebugf3("Mismatch when Synchronizing particle shader variables - Count Shader Variables %d Instance Variables %d", m_shaderVars.GetCount(), instVars.GetCount());

		ptxDebugf3("Shader Variables");
		for (s32 iVariables = 0; iVariables < m_shaderVars.GetCount(); iVariables++)
		{
			ptxShaderVar* pTemplateShaderVar = *m_shaderVars.GetItem(iVariables);
			ptxDebugf3("Index %d - %u - %s", pTemplateShaderVar->GetId(), pTemplateShaderVar->GetHashName(), pTemplateShaderVar->GetInfo()->m_Name);
		}
		ptxDebugf3("Instance Variables");
		for (s32 iVariables = 0; iVariables < instVars.GetCount(); iVariables++)
		{
			ptxDebugf3("Index %d - %u - %s", instVars[iVariables]->GetId(), instVars[iVariables]->GetHashName(), instVars[iVariables]->GetInfo()->m_Name);
		}
	}
#endif // !__NO_OUTPUT

	// identify the template shader vars that need to be added to the inst shader vars
	int numTemplateVars = m_shaderVars.GetCount();

	atArray<bool> foundMatchFlags;
	foundMatchFlags.Resize(numTemplateVars);

	// go through the template shader vars and try to find a matching inst shader var
	for (int i=0; i<numTemplateVars; i++)
	{
		foundMatchFlags[i] = false;

		ptxShaderVar* pTemplateShaderVar = *m_shaderVars.GetItem(i);
		for (int s=0; s<instVars.GetCount(); s++)
		{
			if (pTemplateShaderVar->GetHashName() == instVars[s]->GetHashName())
			{
				// found the matching shader var
				foundMatchFlags[i] = true;
				break;
			}
		}
	}

	// find the max var id of the unmatched shader vars
	grcEffectVar maxVarId = (grcEffectVar)0;
	for (int i=0; i<m_shaderVars.GetCount(); i++)
	{
		if (foundMatchFlags[i]==false)
		{
			maxVarId = Max(maxVarId, (*m_shaderVars.GetItem(i))->GetId());
			OUTPUT_ONLY(ptxShaderVar* pTemplateShaderVar = *m_shaderVars.GetItem(i);)
			ptxWarningf("Missing Shader Index %d - Name %u - %s", pTemplateShaderVar->GetId(), pTemplateShaderVar->GetHashName(), pTemplateShaderVar->GetInfo()->m_Name);
		}
	}
	
	// go through the template shader vars and add any missing into the inst shader vars
	// - we want to add items to the shader in the order of the var ids
	//   not in the order they are in the atStringMap (which is based on the hash of the name)
	for (int id=0; id<=maxVarId; id++)
	{
		for (int var=0; var<m_shaderVars.GetCount(); var++)
		{
			if (foundMatchFlags[var]==false && id==(*m_shaderVars.GetItem(var))->GetId())
			{
				instVars.PushAndGrow((*m_shaderVars.GetItem(var))->Clone(), 1);
			}
		}
	}

	// sort by var id
	instVars.QSort(0, -1, ptxShaderTemplate::SortByVarID);

	// 
	return isInSync;
}

ptxShaderVar* ptxShaderTemplate::CreateShaderVar(grmVariableInfo& varInfo)
{
	// deal with keyframe shader vars
	if (varInfo.m_UiHint && strstr(varInfo.m_UiHint, PTXSTR_EFFECTUI_KEYFRAMEABLE))
	{
		ptxAssertf(varInfo.m_Type == grcEffect::VT_FLOAT	||
				varInfo.m_Type == grcEffect::VT_VECTOR2 ||
				varInfo.m_Type == grcEffect::VT_VECTOR3 ||
				varInfo.m_Type == grcEffect::VT_VECTOR4
				D3D11_ONLY(|| varInfo.m_Type == grcEffect::VT_UNUSED1
				|| varInfo.m_Type == grcEffect::VT_UNUSED2
				|| varInfo.m_Type == grcEffect::VT_UNUSED3
				|| varInfo.m_Type == grcEffect::VT_UNUSED4)
				ORBIS_ONLY(|| varInfo.m_Type == grcEffect::VT_UNUSED1
				|| varInfo.m_Type == grcEffect::VT_UNUSED2
				|| varInfo.m_Type == grcEffect::VT_UNUSED3
				|| varInfo.m_Type == grcEffect::VT_UNUSED4)
				, "RMPTFX: Keyframe Shader Var (%s) in shader (%s) type must be float1,2,3 or 4",varInfo.m_Name,m_shaderName);

		// return a new keyframe shader var
		ptxShaderVar* pShaderVarKeyframe = rage_new ptxShaderVarKeyframe();
		return pShaderVarKeyframe;
	}

	// deal with other shader vars
	switch(varInfo.m_Type)
	{
		case grcEffect::VT_TEXTURE:
		{
			ptxShaderVarTexture* pShaderVarTex = rage_new ptxShaderVarTexture();
			pShaderVarTex->SetTexture(varInfo.m_AssetName);
			return pShaderVarTex;
		}
		case grcEffect::VT_FLOAT:
		{
			return rage_new ptxShaderVarVector(PTXSHADERVAR_FLOAT);
		}
		case grcEffect::VT_VECTOR2:
		{
			return rage_new ptxShaderVarVector(PTXSHADERVAR_FLOAT2);
		}
		case grcEffect::VT_VECTOR3:
		{
			return rage_new ptxShaderVarVector(PTXSHADERVAR_FLOAT3);
		}
		case grcEffect::VT_VECTOR4:
		{
			return rage_new ptxShaderVarVector(PTXSHADERVAR_FLOAT4);
		}
		default:
		{
			ptxAssertf(0, "invalid shader variable type");
			return NULL;
		}
	}
}

void ptxShaderTemplate::InitProgVars(grmShader* pShader)
{
	for (int i=0; i<PTXPROGVAR_RMPTFX_COUNT; i++)
	{
		m_progVars[i] = pShader->LookupVar(ptxShaderVarUtils::GetProgrammedVarName(static_cast<ptxShaderProgVarType>(i)), false);
	}

	if(RMPTFXMGR.GetShaderInitProgVarsFunctor())
	{
		RMPTFXMGR.GetShaderInitProgVarsFunctor()(this);
	}
}

int ptxShaderTemplate::SortByVarID(const datOwner<ptxShaderVar>* ppVarA, const datOwner<ptxShaderVar>* ppVarB)
{
	if ((*ppVarA)->GetId() == (*ppVarB)->GetId())
	{
		return 0;
	}

	if ((*ppVarA)->GetId() < (*ppVarB)->GetId())
	{
		return 1;
	}

	return -1;
};

#if RMPTFX_EDITOR
void ptxShaderTemplate::SendShaderVarToEditor(u32 hashName, ptxByteBuff &buff)
{
	// get the shader var
	ptxShaderVar** ppShaderVar = m_shaderVars.SafeGet(hashName);
	ptxAssertf((ppShaderVar && *ppShaderVar), "tuneable shader var (%u) in shader (%s) must have ui info", hashName, m_shaderName);

	// send it to the editor
	ptxShaderVar* pShaderVar = *ppShaderVar;
	pShaderVar->SendDefnToEditor(buff);
}

// void ptxShaderTemplate::SendTechniqueListToEditor(ptxByteBuff & buff)
// {
// 	// send the number of techniques
// 	int numTechniques = m_pShader->GetTechniqueCount();
// 	buff.Write_s32(numTechniques);
// 
// 	// send the technique data
// 	for (int i=0; i<numTechniques; i++)
// 	{
// 		grcEffectTechnique effectTechnique = m_pShader->GetTechniqueByIndex(i);
// 		buff.Write_s32(effectTechnique);
// 
// #if EFFECT_PRESERVE_STRINGS
// 		buff.Write_const_char(m_pShader->GetTechniqueName(effectTechnique));
// #else
// 		buff.Write_const_char(PTXSTR_TECHNIQUENAME_UNKNOWN);
// #endif
// 	}
// }
#endif


#if RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES
bool ptxShaderTemplate::g_DebugRecordsHashed = false;
ptxShaderTemplate::RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES_FILTER *ptxShaderTemplate::g_pDebugPrintFilter = NULL;
#endif

#if RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES
u32 ptxShaderTemplate::BuildShaderStringHash(const char *pShaderName, const char *pTechniqueName)
{
	char Str[256];
	sprintf(Str, "ptxShaderTemplate::GetRenderPass() %s : %s\n", pShaderName, pTechniqueName);
	return atHashString(Str);
}
#endif

#if RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES
void ptxShaderTemplate::HashDebugRecords(void)
{
	RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES_FILTER *pRecord = g_pDebugPrintFilter;

	if(pRecord == NULL)
	{
		return;
	}

	while(pRecord->pShader != NULL)
	{
		pRecord->Hash = BuildShaderStringHash(pRecord->pShader, pRecord->pTechnique);
		pRecord++;
	}
}
#endif

#if RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES
bool ptxShaderTemplate::IsInADebugRecord(const char *pShaderName, const char *pTechniqueName, u32 blendMode)
{
	RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES_FILTER *pRecord = g_pDebugPrintFilter;

	if(pRecord == NULL)
	{
		return false;
	}

	u32 hashValue = BuildShaderStringHash(pShaderName, pTechniqueName);

	if(g_DebugRecordsHashed == false)
	{
		HashDebugRecords();
		g_DebugRecordsHashed = true;
	}
	while(pRecord->pShader != NULL)
	{
		if((pRecord->Hash == hashValue) && (pRecord->BlendModes[blendMode] != 0))
		{
			return true;
		}
		pRecord++;
	}
	return false;
}
#endif

#if RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES
void ptxShaderTemplate::SetDebugPrintFilter(RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES_FILTER *pFilter)
{
	g_pDebugPrintFilter = pFilter;
}
#endif

#if RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES
int ptxShaderTemplate::GetRenderPass(const char *pShaderName, const char *pTechniqueName)
#else
int ptxShaderTemplate::GetRenderPass()
#endif
{
	MULTIPLE_RENDER_THREADS_ONLY(ptxAssert(g_IsSubRenderThread);)
	const unsigned rti = g_RenderThreadIndex;

#if RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES
	(void)pShaderName;
	(void)pTechniqueName;

	if(IsInADebugRecord(pShaderName, pTechniqueName, m_BlendSet[rti]) == false)
	{
		Printf("ptxShaderTemplate::GetRenderPass() %s: %s : %d\n", pShaderName, pTechniqueName, m_BlendSet[rti]);
	}
#endif

#if IMPLEMENT_BLEND_MODES_AS_PASSES
#if !RMPTFX_USE_PARTICLE_SHADOWS
	return m_BlendSet[rti];
#else
#if GS_INSTANCED_SHADOWS
	if( RMPTFXMGR.IsUsingGeometryShader() )
		return 3*m_BlendSet[rti] + m_RenderPassToUse[rti];
	else
#endif
		return 2*m_BlendSet[rti] + m_RenderPassToUse[rti];

#endif

#else
	return 0;
#endif
}





ptxTechniqueDesc::ptxTechniqueDesc()
{
	m_diffuseMode = PTXDIFFUSEMODE_TEX1_RGBA;
	m_projMode = PTXPROJMODE_NONE;
	m_isLit = true;
	m_isSoft = true;
	m_isScreenSpace = false;
	m_isRefract = false;
	m_isNormalSpec = false;
}

ptxTechniqueDesc::ptxTechniqueDesc(datResource& UNUSED_PARAM(rsc)) 
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxRenderState_num++;
#endif
}

#if __DECLARESTRUCT
datSwapper_ENUM(ptxDiffuseMode);
datSwapper_ENUM(ptxProjectionMode);
void ptxTechniqueDesc::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxTechniqueDesc)
		SSTRUCT_FIELD(ptxTechniqueDesc, m_diffuseMode)
		SSTRUCT_FIELD(ptxTechniqueDesc, m_projMode)
		SSTRUCT_FIELD(ptxTechniqueDesc, m_isLit)
		SSTRUCT_FIELD(ptxTechniqueDesc, m_isSoft)
		SSTRUCT_FIELD(ptxTechniqueDesc, m_isScreenSpace)
		SSTRUCT_FIELD(ptxTechniqueDesc, m_isRefract)
		SSTRUCT_FIELD(ptxTechniqueDesc, m_isNormalSpec)
		SSTRUCT_IGNORE(ptxTechniqueDesc, m_pad)
	SSTRUCT_END(ptxTechniqueDesc)
}
#endif

IMPLEMENT_PLACE(ptxTechniqueDesc);

void ptxTechniqueDesc::GenerateName(ConstString& techniqueName)
{
	const char* diffuseName[PTXDIFFUSEMODE_NUM] = 
	{
		"RGBA",			// RGBA
		"XXXX",			// RRRR
		"XXXX",			// GGGG
		"XXXX",			// BBBB
		"RGB",			// RGB
		"RG_Blend"		// RG_BLEND
	};

	// check rules
	if (m_isRefract)
	{
		// can't have lit or non rgb diffuse mode
		ptxAssertf(m_isLit==false || m_isNormalSpec==true, "refract technique must not be lit unless is normal/specular");
		ptxAssertf(m_diffuseMode==PTXDIFFUSEMODE_TEX1_RGB, "refract technique must be rgb");
	}

	if (m_isScreenSpace)
	{
		// can't be soft 
		ptxAssertf(m_isSoft==false, "screen space technique must not be soft");
		// can't be RG_Blend
		ptxAssertf(m_diffuseMode!=PTXDIFFUSEMODE_TEX1_RG_BLEND, "screen space technique must not be RG_BLEND");
	}

	if (m_isNormalSpec)
	{
		// can't have non lit
		ptxAssertf(m_isLit, "normal spec technique must be lit");
	}

	// build string
	char txt[64];
	txt[0] = '\0';

	if (m_projMode!=PTXPROJMODE_NONE)
	{
		ptxAssertf(m_diffuseMode==PTXDIFFUSEMODE_TEX1_RGBA, "projection only supports RGBA diffuse mode");
		strcat(txt, "RGBA_proj");
	}
	else
	{
		ptxAssertf(m_diffuseMode>=PTXDIFFUSEMODE_FIRST && m_diffuseMode<PTXDIFFUSEMODE_NUM, "invalid diffuse mode");
		strcat(txt, diffuseName[m_diffuseMode]);

		if (m_isLit)
		{
			strcat(txt, "_lit");
		}

		if (m_isSoft)
		{
			strcat(txt, "_soft");
		}

		if (m_isScreenSpace)
		{
			strcat(txt, "_screen");
		}

		if (m_isRefract)
		{
			strcat(txt, "_refract");
		}

		if (m_isNormalSpec)
		{
			strcat(txt, "_normalspec");
		}
	}

	// copy string
	techniqueName = txt;
}








ptxShaderInst::ptxShaderInst()
{
	m_shaderTemplateTechniqueId = grcetNONE;
	m_pShaderTemplate = NULL;
	m_isDataInSync = true;
}

ptxShaderInst::~ptxShaderInst()
{
	for (int i=0; i<m_instVars.GetCount(); i++)
	{
		delete m_instVars[i];
	}
}

ptxShaderInst::ptxShaderInst(datResource& rsc)
: m_shaderTemplateName(rsc)
, m_shaderTemplateTechniqueName(rsc)
, m_techniqueDesc(rsc)
, m_instVars(rsc, 1)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxShaderInst_num++;
#endif
}

void ptxShaderInst::PostLoad()
{
	SetShaderTemplate(m_shaderTemplateName, m_shaderTemplateTechniqueName);

	for(int i = 0; i < m_instVars.GetCount(); i++)
	{
		m_instVars[i]->PostLoad();
	}
}

#if __DECLARESTRUCT
datSwapper_ENUM(grcEffectTechnique);
void ptxShaderInst::DeclareStruct(datTypeStruct& s)
{
	m_pShaderTemplate = NULL;

	SSTRUCT_BEGIN(ptxShaderInst)
		SSTRUCT_FIELD(ptxShaderInst, m_shaderTemplateName)
		SSTRUCT_FIELD(ptxShaderInst, m_shaderTemplateTechniqueName)
		SSTRUCT_FIELD_VP(ptxShaderInst, m_pShaderTemplate)
		SSTRUCT_FIELD(ptxShaderInst, m_shaderTemplateTechniqueId)
		SSTRUCT_FIELD(ptxShaderInst, m_techniqueDesc)
		SSTRUCT_FIELD(ptxShaderInst, m_instVars)
		SSTRUCT_FIELD(ptxShaderInst, m_isDataInSync)
		SSTRUCT_IGNORE(ptxShaderInst, m_pad)
		SSTRUCT_FIELD(ptxShaderInst, m_shaderTemplateHashName)
	SSTRUCT_END(ptxShaderInst)
}
#endif

IMPLEMENT_PLACE(ptxShaderInst);

ptxShaderInst& ptxShaderInst::operator=(const ptxShaderInst& rhs)
{
	m_shaderTemplateName			= rhs.m_shaderTemplateName;
	m_shaderTemplateTechniqueName	= rhs.m_shaderTemplateTechniqueName;
	m_pShaderTemplate				= rhs.m_pShaderTemplate;
	m_shaderTemplateHashName		= rhs.m_shaderTemplateHashName;
	m_shaderTemplateTechniqueId		= rhs.m_shaderTemplateTechniqueId;
	m_instVars						= rhs.m_instVars;
	m_isDataInSync					= rhs.m_isDataInSync;

	ptxAssertf(m_instVars.GetCount()==rhs.m_instVars.GetCount(), "array resize didn't work as expected");

	for (int i=0; i<rhs.m_instVars.GetCount(); i++)
	{
		m_instVars[i] = rhs.m_instVars[i]->Clone();
	}

	return *this;
}

#if RMPTFX_XML_LOADING
void ptxShaderInst::PreLoad(parTreeNode* pNode)
{
	m_instVars.Reset();

	sysMemStartTemp();
	SyncShaderVarsXml(pNode);
	sysMemEndTemp();
}

void ptxShaderInst::RepairAndUpgradeData()
{
	if (!m_shaderTemplateTechniqueName || strlen(m_shaderTemplateTechniqueName)<=0)
	{
		m_shaderTemplateTechniqueName = PTXSTR_TECHNIQUENAME_DEFAULT;
	}
}

void ptxShaderInst::ResolveReferences()
{
	for (int i=0; i<m_instVars.GetCount(); i++)
	{
		m_instVars[i]->SetData();
	}

	SetShaderTemplate(m_shaderTemplateName, m_shaderTemplateTechniqueName);
}
#endif

int ptxShaderInst::Begin()
{
	if (!m_pShaderTemplate) 
	{
		return 0;
	}

	if (m_pShaderTemplate->IsTechniqueDisabled(m_shaderTemplateTechniqueId))
	{
		return 0;
	}

	grcEffectTechnique technique = m_shaderTemplateTechniqueId;
	if( m_pShaderTemplate->IsTechniqueOverriden(m_shaderTemplateTechniqueId) )
	{
		technique = m_pShaderTemplate->GetOverrideTechniqueId(m_shaderTemplateTechniqueId);
	}
	
	int retVal = m_pShaderTemplate->GetGrmShader()->BeginDraw(grmShader::RMC_DRAW, true, technique);
	if (retVal)
	{
#if RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES
		m_pShaderTemplate->GetGrmShader()->Bind((int)m_pShaderTemplate->GetRenderPass(m_shaderTemplateName.c_str(), m_shaderTemplateTechniqueName.c_str()));
#else
		m_pShaderTemplate->GetGrmShader()->Bind((int)m_pShaderTemplate->GetRenderPass());
#endif
	}
	else
	{
		End(false);
		ptxErrorf("shader %s cannot being - maybe the techniques are not set correctly", m_pShaderTemplate->GetName());
	}

	return retVal;
}

void ptxShaderInst::End(bool isBound)
{
	if (isBound)
	{
		m_pShaderTemplate->GetGrmShader()->UnBind();
	}

	m_pShaderTemplate->GetGrmShader()->EndDraw();

	grmModel::SetForceShader(0);
}

bool ptxShaderInst::SetShaderTemplate(const char* pShaderName, const char* pTechniqueName)
{
	if (m_shaderTemplateName != pShaderName)
	{
		m_shaderTemplateName = pShaderName;
	}

	if (!pTechniqueName)
	{
		pTechniqueName = PTXSTR_TECHNIQUENAME_DEFAULT;
	}

	if (m_shaderTemplateTechniqueName != pTechniqueName)
	{
		m_shaderTemplateTechniqueName = pTechniqueName;
	}

	// Search for the shader
	m_pShaderTemplate = ptxShaderTemplateList::LoadShader(pShaderName);

	if (m_pShaderTemplate)
	{
		// If found, set up the shader vars...
		ptxInstVars* pInstVars = &m_instVars;
		m_isDataInSync = m_pShaderTemplate->SyncShaderVars(pInstVars);

		m_shaderTemplateHashName = atHashValue(m_shaderTemplateName);

		//... and make sure the technique exists.
		m_shaderTemplateTechniqueId = m_pShaderTemplate->GetGrmShader()->LookupTechnique(m_shaderTemplateTechniqueName, false);

		if (m_shaderTemplateTechniqueId == grcetNONE)
		{
			ptxMsg::AssetWarningf("technique [%s] was not found in shader [%s], defaulting to [%s]", m_shaderTemplateTechniqueName.c_str(), m_shaderTemplateName.c_str(), PTXSTR_TECHNIQUENAME_DEFAULT);

			m_shaderTemplateTechniqueName = PTXSTR_TECHNIQUENAME_DEFAULT;
			m_shaderTemplateTechniqueId = m_pShaderTemplate->GetGrmShader()->LookupTechnique(m_shaderTemplateTechniqueName, false);
		}
	}
	else
	{
		// Couldn't find the shader - fall back to the default shader & default technique
		// (also make sure that's not the one we just tried to use...)
		if (m_shaderTemplateName != ptxShaderTemplateList::GetDefaultShaderName())
		{
			ptxMsg::AssetWarningf("Shader (%s) is not preloaded - defaulting to %s", pShaderName, ptxShaderTemplateList::GetDefaultShaderName());	
			return SetShaderTemplate(ptxShaderTemplateList::GetDefaultShaderName(), PTXSTR_TECHNIQUENAME_DEFAULT);
		}
		else
		{
			ptxMsg::AssetWarningf("Couldn't find the default shader (%s)!", m_shaderTemplateName.c_str());
			return false;
		}
	}

	if (GetIsDataInSync()==false)
	{
		ptxAssertf(0, "particle rule is not in sync with its shader (%s) - this will crash when defragged - please resave via the Particle Editor\n", GetShaderTemplateName());
	}

	return (m_pShaderTemplate!=NULL);
}

void ptxShaderInst::ClearShaderVars() 
{
	if (m_pShaderTemplate) 
	{
		m_pShaderTemplate->ClearShaderVars(); 

		grmShader* pGrmShader = m_pShaderTemplate->GetGrmShader();
		for (int i=0; i<m_instVars.GetCount(); i++)
		{
			m_instVars[i]->ClearShaderVar(pGrmShader);
		}
	}
};

void ptxShaderInst::SetShaderVars(int blendSet) 
{
	if (m_pShaderTemplate) 
	{
		Vec4V vChannelMask = Vec4V(V_ONE);
		if (m_techniqueDesc.m_diffuseMode==PTXDIFFUSEMODE_TEX1_RRRR)
		{
			vChannelMask = Vec4V(V_X_AXIS_WZERO);
		}
		else if (m_techniqueDesc.m_diffuseMode==PTXDIFFUSEMODE_TEX1_GGGG)
		{
			vChannelMask = Vec4V(V_Y_AXIS_WZERO);
		}
		else if (m_techniqueDesc.m_diffuseMode==PTXDIFFUSEMODE_TEX1_BBBB)
		{
			vChannelMask = Vec4V(V_Z_AXIS_WZERO);
		}

		m_pShaderTemplate->SetShaderVars(blendSet, vChannelMask); 

		RMPTFX_BANK_ONLY(RunCustomOverrideFunc(&m_instVars));

		grmShader* pGrmShader = m_pShaderTemplate->GetGrmShader();
		for (int i=0; i<m_instVars.GetCount(); i++)
		{
			m_instVars[i]->SetShaderVar(pGrmShader);
		}
	}
};

const char* ptxShaderInst::GetTextureName(u32 samplerHashName) const
{
	for(int i=0; i<m_instVars.GetCount(); i++)
	{
		if (m_instVars[i]->GetHashName()==samplerHashName)
		{
			ptxShaderVar* pShaderVar = m_instVars[i];
			ptxShaderVarTexture* pShaderVarTexture = SafeCast(ptxShaderVarTexture, pShaderVar);
			return pShaderVarTexture->GetTextureName();
		}
	}
	return NULL;
}

const u32 ptxShaderInst::GetTextureHashName(u32 samplerHashName) const
{
	for(int i=0; i<m_instVars.GetCount(); i++)
	{
		if (m_instVars[i]->GetHashName()==samplerHashName)
		{
			ptxShaderVar* pShaderVar = m_instVars[i];
			ptxShaderVarTexture* pShaderVarTexture = SafeCast(ptxShaderVarTexture, pShaderVar);
			return pShaderVarTexture->GetTextureHashName();
		}
	}
	return (u32)-1;
}

void ptxShaderInst::SetKeyframeVars(ptxCustomVars& customVars, ScalarV_In vKeyTime) const
{
	float* pVarData = (float*)&customVars;

	Vec4V vKeyframeData;
	for (int i=0; i<m_instVars.GetCount(); i++)
	{
		if (m_instVars[i]->GetIsKeyframeable())
		{
			ptxShaderVar* pVar = m_instVars[i];
			ptxShaderVarKeyframe* pVarKeyframe = static_cast<ptxShaderVarKeyframe*>(pVar);

			grcEffect::VarType type = m_instVars[i]->GetGRCType();
			vKeyframeData = pVarKeyframe->m_keyframe.Query(vKeyTime);

			switch(type)
			{
			case grcEffect::VT_FLOAT:
#if __D3D11 || RSG_ORBIS
			case grcEffect::VT_UNUSED1:
#endif
				pVarData[0]=vKeyframeData.GetXf();
				pVarData += 1;
				break;
			case grcEffect::VT_VECTOR2:
#if __D3D11 || RSG_ORBIS
			case grcEffect::VT_UNUSED2:
#endif
				pVarData[0]=vKeyframeData.GetXf();
				pVarData[1]=vKeyframeData.GetYf();
				pVarData += 2;
				break;
			case grcEffect::VT_VECTOR3:
#if __D3D11 || RSG_ORBIS
			case grcEffect::VT_UNUSED3:
#endif
				pVarData[0]=vKeyframeData.GetXf();
				pVarData[1]=vKeyframeData.GetYf();
				pVarData[2]=vKeyframeData.GetZf();
				pVarData += 3;
				break;
			case grcEffect::VT_VECTOR4:
#if __D3D11 || RSG_ORBIS
			case grcEffect::VT_UNUSED4:
#endif
				pVarData[0]=vKeyframeData.GetXf();
				pVarData[1]=vKeyframeData.GetYf();
				pVarData[2]=vKeyframeData.GetZf();
				pVarData[3]=vKeyframeData.GetWf();
				pVarData += 4;
				break;
			default: ptxAssertf(0, "unsupported custom shader variable"); break;
			}

			ptxAssertf(((u32)((u8*)pVarData-(u8*)&customVars)) <= (u32)(sizeof(ptxCustomVars)), "too many custom shader variables");
		}
	}
}

#if RMPTFX_EDITOR
void ptxShaderInst::ReplaceTexture(const char* pName, grcTexture* pTexture)
{
	for (int i=0; i<m_instVars.GetCount(); i++)
	{
		if (m_instVars[i]->GetType()==PTXSHADERVAR_TEXTURE)
		{
			ptxShaderVarTexture* pShaderVarTex = static_cast<ptxShaderVarTexture*>(m_instVars[i].ptr);
			if (stricmp(pShaderVarTex->GetTextureName(), pName)==0)
			{
				pTexture->AddRef(); // increment the ref count because we're adding a new ref. to an external texture
				pShaderVarTex->SetTexture(pTexture, true); // The replacement is always external, because it's in the loose rules list
			}
		}
	}
}

void ptxShaderInst::SendToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	// send shader template name
	buff.Write_const_char(m_shaderTemplateName);

	// send number of shader template vars
	buff.Write_u8((u8)m_instVars.GetCount());

	// send the shader vars
	for(int i=0;i<m_instVars.GetCount();i++)
	{
		m_pShaderTemplate->SendShaderVarToEditor(m_instVars[i]->GetHashName(), buff);
		m_instVars[i]->SendToEditor(buff, pParticleRule);
	}

	// send technique list
//	m_pShaderTemplate->SendTechniqueListToEditor(buff);

	// send technique name
//	buff.Write_const_char(m_shaderTemplateTechniqueName);

	buff.Write_s32(m_techniqueDesc.m_diffuseMode);
	buff.Write_s32(m_techniqueDesc.m_projMode);
	buff.Write_bool(m_techniqueDesc.m_isLit);
	buff.Write_bool(m_techniqueDesc.m_isSoft);
	buff.Write_bool(m_techniqueDesc.m_isScreenSpace);
	buff.Write_bool(m_techniqueDesc.m_isRefract);
	buff.Write_bool(m_techniqueDesc.m_isNormalSpec);
}

void ptxShaderInst::ReceiveFromEditor(const ptxByteBuff& buff)
{
	// read the shader template name
	buff.Read_const_char(); 

	// read the number of template vars
#if __ASSERT
	int numVars = 
#endif
	buff.Read_u8();
	ptxAssertf(numVars == m_instVars.GetCount(), "(%s) Interface communication error, mismatched # of shader instance vars", GetShaderTemplateName());

	// send the shader vars
	for(int i=0;i<m_instVars.GetCount();i++)
	{
		m_instVars[i]->ReceiveFromEditor(buff);
	}

	// read the technique name
//	m_shaderTemplateTechniqueName = buff.Read_const_char();

	// read the technique id
//	m_shaderTemplateTechniqueId = m_pShaderTemplate->GetGrmShader()->LookupTechnique(m_shaderTemplateTechniqueName,false);

	m_techniqueDesc.m_diffuseMode = (ptxDiffuseMode)buff.Read_s32();
	m_techniqueDesc.m_projMode = (ptxProjectionMode)buff.Read_s32();
	m_techniqueDesc.m_isLit = buff.Read_bool();
	m_techniqueDesc.m_isSoft = buff.Read_bool();
	m_techniqueDesc.m_isScreenSpace = buff.Read_bool();
	m_techniqueDesc.m_isRefract = buff.Read_bool();
	m_techniqueDesc.m_isNormalSpec = buff.Read_bool();

	// generate the technique name
	m_techniqueDesc.GenerateName(m_shaderTemplateTechniqueName);

	// read the technique id
	m_shaderTemplateTechniqueId = m_pShaderTemplate->GetGrmShader()->LookupTechnique(m_shaderTemplateTechniqueName,false);

// 	// send back to editor (as technique desc rules may have updated the state)
// 	SendToEditor();
}
#endif // RMPTFX_EDITOR






#if RMPTFX_XML_LOADING
typedef std::pair<ptxShaderVar*, parTreeNode*> ptxVarAndNodePair;

bool ShaderInstSorter(const ptxVarAndNodePair& varNodePairA, const ptxVarAndNodePair& varNodePairB) 
{
	return varNodePairA.first->GetId() < varNodePairB.first->GetId();
}

bool ptxShaderInst::SyncShaderVarsXml(parTreeNode* pNode)
{
	// steps to sync shader vars:
	// 1. find the template
	// 2. classify each shader var in the tree node as either OK, need to delete, need to add, or need to change
	// 3. based on those lists, modify tree node in place

	const char* pShaderName = NULL;
	if (!pNode->FindValueFromPath(PTXSTR_TREENODE_SHADERTEMPLATENAME, pShaderName))
	{
		ptxWarningf("couldn't find shader");
		pNode->ClearChildrenAndData();
		return false;
	}

	ptxShaderTemplate* shaderTemplate = ptxShaderTemplateList::GetShader(pShaderName);

	if (!shaderTemplate)
	{
		ptxDebugf1("couldn't find the shader template \"%s\"... trying to load it instead", pShaderName);
		shaderTemplate = ptxShaderTemplateList::LoadShader(pShaderName);
	}

	if (!shaderTemplate)
	{
		ptxWarningf("couldn't find template for shader %s, using default instead", pShaderName);
		shaderTemplate = ptxShaderTemplateList::GetShader(ptxShaderTemplateList::GetDefaultShaderName());
	}

	if (!shaderTemplate)
	{
		ptxDebugf1("couldn't find the shader template \"%s\"... trying to load it instead", pShaderName);
		shaderTemplate = ptxShaderTemplateList::LoadShader(ptxShaderTemplateList::GetDefaultShaderName());
	}

	if (!shaderTemplate)
	{
		ptxErrorf("couldn't find any shader templates");
		pNode->ClearChildrenAndData();
		return false;
	}

	atArray<parTreeNode*> nodesToDelete;
	atArray<ptxShaderVar*> varsToAdd;
	atArray<ptxVarAndNodePair > nodesToUpdate;
	atArray<ptxVarAndNodePair > nodesThatAreOk;

	const ptxShaderTemplateVars& templateVars = shaderTemplate->GetShaderVars();

	parTreeNode* xmlInstVars = pNode->FindFromXPath(PTXSTR_TREENODE_INSTVARS);

	if (!xmlInstVars)
	{
		ptxDebugf1("couldn't find any shader instance variables for %s", pShaderName);
		for (int i=0; i<templateVars.GetCount(); i++)
		{
			varsToAdd.PushAndGrow(*templateVars.GetItem(i));
		}
	}
	else
	{
		for (parTreeNode::ChildNodeIterator xmlVarIter=xmlInstVars->BeginChildren(); xmlVarIter!=xmlInstVars->EndChildren(); ++xmlVarIter)
		{
			(*xmlVarIter)->GetElement().SetUserFlag(false);
		}

		for (int i=0; i<templateVars.GetCount(); i++)
		{
			ptxShaderVar* pTemplateVar = *templateVars.GetItem(i);
			parTreeNode* pFoundNode = NULL;

			for (parTreeNode::ChildNodeIterator xmlVarIter=xmlInstVars->BeginChildren(); xmlVarIter!=xmlInstVars->EndChildren(); ++xmlVarIter)
			{
				parTreeNode* xmlVar = *xmlVarIter;
				const char* xmlVarName = NULL;
				xmlVar->FindValueFromPath(PTXSTR_TREENODE_VARNAME, xmlVarName);

				if (xmlVarName && pTemplateVar->GetHashName() == atStringHash(xmlVarName))
				{
					if (pFoundNode) 
					{
						// We already found a shader variable with this name. Only take the first one!
						parWarningf("Duplicate shader variable \"%s\" found, just using the first one.", xmlVarName);
						nodesToDelete.PushAndGrow(xmlVar);
					}

					pFoundNode = xmlVar;
				}
			}

			if (!pFoundNode)
			{
				varsToAdd.PushAndGrow(pTemplateVar);
			}
			else
			{
				pFoundNode->GetElement().SetUserFlag(true);

				// make sure the types match too
				int typeVal = -1;
				if (pFoundNode->FindValueFromPath(PTXSTR_TREENODE_VARTYPE, typeVal))
				{
					if ((int)pTemplateVar->GetType()==typeVal)
					{
						// everything matches up
						nodesThatAreOk.PushAndGrow(ptxVarAndNodePair(pTemplateVar, pFoundNode));
					}
					else
					{
						// need to modify the variable type
						nodesToUpdate.PushAndGrow(ptxVarAndNodePair(pTemplateVar, pFoundNode));
					}
				}
				else
				{
					// couldn't find type - assume the worst
					nodesToUpdate.PushAndGrow(ptxVarAndNodePair(pTemplateVar, pFoundNode));
				}
			}
		}

		// now find all of the xml nodes that don't correspond to shader variables, and mark them for deletion
		for (parTreeNode::ChildNodeIterator xmlVarIter=xmlInstVars->BeginChildren(); xmlVarIter!=xmlInstVars->EndChildren(); ++xmlVarIter)
		{
			if ((*xmlVarIter)->GetElement().GetUserFlag()==false)
			{
				nodesToDelete.PushAndGrow(*xmlVarIter);
			}
		}
	}

	bool neededFixup = false;
	if (nodesToDelete.GetCount()>0 || nodesToUpdate.GetCount()>0 || varsToAdd.GetCount()>0)
	{
		ptxDebugf1("instance of shader \"%s\" needed fixup", pShaderName);
		neededFixup = true;
	}

	// all the nodes are classified now - remove them all from the <instVars> node and then add them back in as they get fixed up
	while (xmlInstVars->GetChild())
	{
		xmlInstVars->GetChild()->RemoveSelf();
	}

	// update any nodes whose types didn't match
	for (int i=0; i<nodesToUpdate.GetCount(); i++)
	{
		parWarningf("types for variable \"%s\" didn't match, write some better code to handle this?", atFinalHashString::TryGetString(nodesToUpdate[i].first->GetHashName()));
		varsToAdd.PushAndGrow(nodesToUpdate[i].first);
		nodesToDelete.PushAndGrow(nodesToUpdate[i].second);
	}
	nodesToUpdate.Reset();

	// delete any shader variables that are no longer relevant
	for (int i=0; i<nodesToDelete.GetCount(); i++)
	{
		delete nodesToDelete[i];
	}
	nodesToDelete.Reset();

	// add any new nodes
	for (int i=0; i<varsToAdd.GetCount(); i++)
	{
		parTreeNode* pNewNode = PARSER.BuildTreeNode(varsToAdd[i], "Item");
		nodesThatAreOk.PushAndGrow(ptxVarAndNodePair(varsToAdd[i], pNewNode));
	}
	varsToAdd.Reset();

	// sort the list of nodes that are OK by the ID (in reverse, so when we add them by pushing on the front of the list it'll be in order)
	std::sort(nodesThatAreOk.begin(), nodesThatAreOk.end(), ShaderInstSorter);

	for (int i=0; i<nodesThatAreOk.GetCount(); i++)
	{
		nodesThatAreOk[i].second->InsertAsChildOf(xmlInstVars);
	}

	return neededFixup;
}
#endif // RMPTFX_XML_LOADING

#if RMPTFX_BANK
ptxShaderInst::CustomOverrideFuncType ptxShaderInst::sm_customOverrideFunc = NULL;

void ptxShaderInst::SetCustomOverrideFunc(CustomOverrideFuncType func)
{
	sm_customOverrideFunc = func;
}

void ptxShaderInst::RunCustomOverrideFunc(ptxInstVars* instVars)
{
	if (sm_customOverrideFunc)
	{
		sm_customOverrideFunc(instVars);
	}
}
#endif

} // namespace rage
