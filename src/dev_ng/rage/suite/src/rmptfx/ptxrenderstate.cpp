// 
// rmptfx/ptxrenderstate.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxrenderstate.h"
#include "rmptfx/ptxrenderstate_parser.h"

// includes (rmptfx)
#include "rmptfx/ptxbytebuff.h"
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{


grcRasterizerStateHandle	ptxRenderState::ms_rasterizerStateCullModes[3];
grcDepthStencilStateHandle	ptxRenderState::ms_depthStateWriteDepthTestDepth[2][2][PTXPROJMODE_NUM];

grcBlendStateHandle			ptxRenderState::ms_blendNormal;
grcBlendStateHandle			ptxRenderState::ms_blendAdd;
grcBlendStateHandle			ptxRenderState::ms_blendSubtract;
grcBlendStateHandle			ptxRenderState::ms_blendCompositeAlpha;
grcBlendStateHandle			ptxRenderState::ms_blendCompositeAlphaSubtract;

// code
ptxRenderState::ptxRenderState() 
{
	m_cullMode = grccmBack; 
	m_blendSet = 0; 
	m_depthWrite = false; 
	m_lightingMode = true; 
	m_alphaBlend = true; 
	m_depthTest = true;
}

ptxRenderState::ptxRenderState(datResource& UNUSED_PARAM(rsc))
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxRenderState_num++;
#endif
}

IMPLEMENT_PLACE(ptxRenderState);

#if __DECLARESTRUCT
void ptxRenderState::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxRenderState)
		SSTRUCT_FIELD(ptxRenderState, m_cullMode)
		SSTRUCT_FIELD(ptxRenderState, m_blendSet)
		SSTRUCT_FIELD(ptxRenderState, m_lightingMode)
		SSTRUCT_FIELD(ptxRenderState, m_depthWrite)
		SSTRUCT_FIELD(ptxRenderState, m_depthTest)
		SSTRUCT_FIELD(ptxRenderState, m_alphaBlend)
		SSTRUCT_IGNORE(ptxRenderState, m_pad)
	SSTRUCT_END(ptxRenderState)
}
#endif

void ptxRenderState::InitClass() 
{
	InitRenderStateBlocks();
}

#if RMPTFX_EDITOR
void ptxRenderState::SendToEditor(ptxByteBuff& buff)
{
	buff.Write_u32(m_blendSet);
	buff.Write_u32(m_lightingMode);
	buff.Write_bool(m_depthWrite);
	buff.Write_bool(m_depthTest);
	buff.Write_bool(m_alphaBlend);
	buff.Write_u32(m_cullMode);
}
#endif

#if RMPTFX_EDITOR
void ptxRenderState::ReceiveFromEditor(const ptxByteBuff& buff)
{
	m_blendSet = buff.Read_u32();
	m_lightingMode = buff.Read_u32();
	m_depthWrite = buff.Read_bool();
	m_depthTest = buff.Read_bool();
	m_alphaBlend = buff.Read_bool();
	m_cullMode = buff.Read_u32();
}
#endif

void ptxRenderState::InitRenderStateBlocks()
{
	// rasterizer blocks
	grcRasterizerStateDesc rasterizerStateDesc;
	rasterizerStateDesc.CullMode = grcRSV::CULL_FRONT;

	// grccmNone
	ms_rasterizerStateCullModes[0] = grcStateBlock::RS_NoBackfaceCull;
	// grccmFront
	ms_rasterizerStateCullModes[1] = grcStateBlock::CreateRasterizerState(rasterizerStateDesc);
	// grccmBack
	ms_rasterizerStateCullModes[2] = grcStateBlock::RS_Default;

	// depth/stencil blocks
	grcDepthStencilStateDesc depthStencilDesc;
	depthStencilDesc.DepthFunc = rage::FixupDepthDirection(grcRSV::CMP_LESSEQUAL);

	//////////////////////////////////////////////////////////////////////////
	// Stencil Off
	// write off, test off
	depthStencilDesc.DepthWriteMask = 0;
	depthStencilDesc.DepthEnable = 0;
	ms_depthStateWriteDepthTestDepth[0][0][PTXPROJMODE_NONE] = grcStateBlock::CreateDepthStencilState(depthStencilDesc);
	ms_depthStateWriteDepthTestDepth[0][0][PTXPROJMODE_ALL] = grcStateBlock::CreateDepthStencilState(depthStencilDesc);

	// write off, test on
	depthStencilDesc.DepthWriteMask = 0;
	depthStencilDesc.DepthEnable = 1;


	ms_depthStateWriteDepthTestDepth[0][1][PTXPROJMODE_NONE] = grcStateBlock::CreateDepthStencilState(depthStencilDesc);
	ms_depthStateWriteDepthTestDepth[0][1][PTXPROJMODE_ALL] = grcStateBlock::CreateDepthStencilState(depthStencilDesc);

	// write on, test off
	depthStencilDesc.DepthWriteMask = 1;
	depthStencilDesc.DepthEnable = 0;
	ms_depthStateWriteDepthTestDepth[1][0][PTXPROJMODE_NONE] =  grcStateBlock::CreateDepthStencilState(depthStencilDesc);
	ms_depthStateWriteDepthTestDepth[1][0][PTXPROJMODE_ALL] =  grcStateBlock::CreateDepthStencilState(depthStencilDesc);

	// write on, test on
	depthStencilDesc.DepthWriteMask = 1;
	depthStencilDesc.DepthEnable = 1;
	ms_depthStateWriteDepthTestDepth[1][1][PTXPROJMODE_NONE] = grcStateBlock::CreateDepthStencilState(depthStencilDesc);
	ms_depthStateWriteDepthTestDepth[1][1][PTXPROJMODE_ALL] = grcStateBlock::CreateDepthStencilState(depthStencilDesc);

	//////////////////////////////////////////////////////////////////////////
	// Stencil water passes test

	// set up stencil
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0x10;
	depthStencilDesc.StencilWriteMask = 0xf0;
	depthStencilDesc.BackFace.StencilFunc = grcRSV::CMP_EQUAL;
	depthStencilDesc.BackFace.StencilDepthFailOp = grcRSV::STENCILOP_KEEP;
	depthStencilDesc.BackFace.StencilFailOp = grcRSV::STENCILOP_KEEP;
	depthStencilDesc.BackFace.StencilPassOp = grcRSV::STENCILOP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = grcRSV::CMP_EQUAL;
	depthStencilDesc.FrontFace.StencilDepthFailOp = grcRSV::STENCILOP_KEEP;
	depthStencilDesc.FrontFace.StencilFailOp = grcRSV::STENCILOP_KEEP;
	depthStencilDesc.FrontFace.StencilPassOp = grcRSV::STENCILOP_KEEP;

	// write off, test off
	depthStencilDesc.DepthWriteMask = 0;
	depthStencilDesc.DepthEnable = 0;
	ms_depthStateWriteDepthTestDepth[0][0][PTXPROJMODE_WATER] = grcStateBlock::CreateDepthStencilState(depthStencilDesc);

	// write off, test on
	depthStencilDesc.DepthWriteMask = 0;
	depthStencilDesc.DepthEnable = 1;


	ms_depthStateWriteDepthTestDepth[0][1][PTXPROJMODE_WATER] = grcStateBlock::CreateDepthStencilState(depthStencilDesc);

	// write on, test off
	depthStencilDesc.DepthWriteMask = 1;
	depthStencilDesc.DepthEnable = 0;
	ms_depthStateWriteDepthTestDepth[1][0][PTXPROJMODE_WATER] =  grcStateBlock::CreateDepthStencilState(depthStencilDesc);

	// write on, test on
	depthStencilDesc.DepthWriteMask = 1;
	depthStencilDesc.DepthEnable = 1;
	ms_depthStateWriteDepthTestDepth[1][1][PTXPROJMODE_WATER] = grcStateBlock::CreateDepthStencilState(depthStencilDesc);


	//////////////////////////////////////////////////////////////////////////
	// Stencil: water fails test

	// set up stencil
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0x10;
	depthStencilDesc.StencilWriteMask = 0xf0;
	depthStencilDesc.BackFace.StencilFunc = grcRSV::CMP_NOTEQUAL;
	depthStencilDesc.BackFace.StencilDepthFailOp = grcRSV::STENCILOP_KEEP;
	depthStencilDesc.BackFace.StencilFailOp = grcRSV::STENCILOP_KEEP;
	depthStencilDesc.BackFace.StencilPassOp = grcRSV::STENCILOP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = grcRSV::CMP_NOTEQUAL;
	depthStencilDesc.FrontFace.StencilDepthFailOp = grcRSV::STENCILOP_KEEP;
	depthStencilDesc.FrontFace.StencilFailOp = grcRSV::STENCILOP_KEEP;
	depthStencilDesc.FrontFace.StencilPassOp = grcRSV::STENCILOP_KEEP;

	// write off, test off
	depthStencilDesc.DepthWriteMask = 0;
	depthStencilDesc.DepthEnable = 0;
	ms_depthStateWriteDepthTestDepth[0][0][PTXPROJMODE_NON_WATER] = grcStateBlock::CreateDepthStencilState(depthStencilDesc);

	// write off, test on
	depthStencilDesc.DepthWriteMask = 0;
	depthStencilDesc.DepthEnable = 1;


	ms_depthStateWriteDepthTestDepth[0][1][PTXPROJMODE_NON_WATER] = grcStateBlock::CreateDepthStencilState(depthStencilDesc);

	// write on, test off
	depthStencilDesc.DepthWriteMask = 1;
	depthStencilDesc.DepthEnable = 0;
	ms_depthStateWriteDepthTestDepth[1][0][PTXPROJMODE_NON_WATER] =  grcStateBlock::CreateDepthStencilState(depthStencilDesc);

	// write on, test on
	depthStencilDesc.DepthWriteMask = 1;
	depthStencilDesc.DepthEnable = 1;
	ms_depthStateWriteDepthTestDepth[1][1][PTXPROJMODE_NON_WATER] = grcStateBlock::CreateDepthStencilState(depthStencilDesc);


	// blend state blocks
	grcBlendStateDesc blendStateDesc;

	blendStateDesc.IndependentBlendEnable = true;
	//Disable all other color writes
	blendStateDesc.BlendRTDesc[1].BlendEnable = 0;
	blendStateDesc.BlendRTDesc[1].RenderTargetWriteMask = grcRSV::COLORWRITEENABLE_NONE;
	for(int i=2; i<8; i++)
	{
		blendStateDesc.BlendRTDesc[i] = blendStateDesc.BlendRTDesc[1];
	}

	blendStateDesc.BlendRTDesc[0].BlendEnable = 1;
	blendStateDesc.BlendRTDesc[0].SrcBlendAlpha = grcRSV::BLEND_ONE;
	blendStateDesc.BlendRTDesc[0].DestBlendAlpha = grcRSV::BLEND_INVSRCALPHA;
	blendStateDesc.BlendRTDesc[0].BlendOpAlpha = grcRSV::BLENDOP_ADD;

	blendStateDesc.BlendRTDesc[0].DestBlend = grcRSV::BLEND_INVSRCALPHA;
	blendStateDesc.BlendRTDesc[0].SrcBlend = grcRSV::BLEND_SRCALPHA;
	blendStateDesc.BlendRTDesc[0].BlendOp = grcRSV::BLENDOP_ADD;

	ms_blendNormal = grcStateBlock::CreateBlendState(blendStateDesc);

	blendStateDesc.BlendRTDesc[0].DestBlend = grcRSV::BLEND_ONE;
	blendStateDesc.BlendRTDesc[0].SrcBlend = grcRSV::BLEND_ONE;
	blendStateDesc.BlendRTDesc[0].BlendOp = grcRSV::BLENDOP_ADD;

	ms_blendAdd = grcStateBlock::CreateBlendState(blendStateDesc);

	blendStateDesc.BlendRTDesc[0].DestBlend = grcRSV::BLEND_ONE;
	blendStateDesc.BlendRTDesc[0].SrcBlend = grcRSV::BLEND_ONE;
	blendStateDesc.BlendRTDesc[0].BlendOp = grcRSV::BLENDOP_SUBTRACT;

	ms_blendSubtract = grcStateBlock::CreateBlendState(blendStateDesc);

	blendStateDesc.BlendRTDesc[0].DestBlend = grcRSV::BLEND_INVSRCALPHA;
	blendStateDesc.BlendRTDesc[0].SrcBlend = grcRSV::BLEND_ONE;
	blendStateDesc.BlendRTDesc[0].BlendOp = grcRSV::BLENDOP_ADD;

	ms_blendCompositeAlpha = grcStateBlock::CreateBlendState(blendStateDesc);


	blendStateDesc.BlendRTDesc[0].DestBlend = grcRSV::BLEND_INVSRCALPHA;
	blendStateDesc.BlendRTDesc[0].SrcBlend = grcRSV::BLEND_ONE;
	blendStateDesc.BlendRTDesc[0].BlendOp = grcRSV::BLENDOP_REVSUBTRACT;

	ms_blendCompositeAlphaSubtract = grcStateBlock::CreateBlendState(blendStateDesc);

}

grcRasterizerStateHandle ptxRenderState::SelectRasterizerStateBlock(int cullMode)
{
	return ms_rasterizerStateCullModes[cullMode];
}

grcDepthStencilStateHandle ptxRenderState::SelectDepthStencilStateBlock(bool writeDepth, bool testDepth, ptxProjectionMode projMode)
{
	if(RMPTFXMGR.GetOverrideDepthStencilStateFunctor())
	{
		grcDepthStencilStateHandle bOverrideHandle = RMPTFXMGR.GetOverrideDepthStencilStateFunctor()(writeDepth, testDepth, projMode);
		if(bOverrideHandle != grcStateBlock::DSS_Invalid)
		{
			return bOverrideHandle;
		}
	}

	return ms_depthStateWriteDepthTestDepth[writeDepth][testDepth][projMode];
}

grcBlendStateHandle ptxRenderState::SelectBlendStateBlock(int blendMode)
{
	if(RMPTFXMGR.GetOverrideBlendStateFunctor())
	{
		grcBlendStateHandle bOverrideHandle = RMPTFXMGR.GetOverrideBlendStateFunctor()(blendMode);
		if(bOverrideHandle != grcStateBlock::BS_Invalid)
		{
			return bOverrideHandle;
		}
	}

	switch (blendMode)
	{
		case (grcbsNormal):
			return ms_blendNormal;
                 
		case (grcbsAdd):                
			return ms_blendAdd;

		case (grcbsSubtract):
			return ms_blendSubtract;

		case (grcbsCompositeAlpha):
			return ms_blendCompositeAlpha;

		case (grcbsCompositeAlphaSubtract):
			return ms_blendCompositeAlphaSubtract;

		default:
			return ms_blendNormal;
	}
}

} // namespace rage
