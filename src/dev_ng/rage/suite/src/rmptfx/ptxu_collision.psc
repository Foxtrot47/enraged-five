<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_Collision" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<struct name="m_bouncinessKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_bounceDirVarKFP" type="::rage::ptxKeyframeProp"/>
	<float name="m_radiusMult"/>
	<float name="m_restSpeed"/>
	<int name="m_colnChance"/>
	<int name="m_killChance"/>
</structdef>

</ParserSchema>