// 
// rmptfx/ptxevolution.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxevolution.h"
#include "rmptfx/ptxevolution_parser.h"

// includes (rmptfx)
#include "rmptfx/ptxbytebuff.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxkeyframeprop.h"
#include "rmptfx/ptxmanager.h"

// includes (rage)
#include "atl/binmap_struct.h"
#include "data/safestruct.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
using namespace rage;


// code
ptxEvolution::ptxEvolution()
{
	m_isOverriden = false; 
	m_overrideValue = 0.0f;
}

ptxEvolution::ptxEvolution(datResource& rsc)
: m_name(rsc)
{
	m_nameHash = m_name.c_str();
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxEvolution_num++;
#endif
}

#if __DECLARESTRUCT
void ptxEvolution::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxEvolution)
		SSTRUCT_FIELD(ptxEvolution, m_name)
		SSTRUCT_FIELD_AS(ptxEvolution, m_nameHash, u32)
		SSTRUCT_FIELD(ptxEvolution, m_overrideValue)
		SSTRUCT_FIELD(ptxEvolution, m_isOverriden)
		SSTRUCT_IGNORE(ptxEvolution, m_pad)
	SSTRUCT_END(ptxEvolution)
}
#endif

IMPLEMENT_PLACE(ptxEvolution);

void ptxEvolution::SetName(const char* pName) 
{
	if (m_name!=pName) 
	{
		m_name = pName;
		m_nameHash = pName;
	}
}

#if RMPTFX_EDITOR
void ptxEvolution::SendToEditor(ptxByteBuff& buff)
{
	buff.Write_const_char(m_name);
	buff.Write_bool(m_isOverriden);
	buff.Write_float(m_overrideValue);
}
#endif

#if RMPTFX_EDITOR
void ptxEvolution::ReceiveFromEditor(const ptxByteBuff& buff)
{
	SetName(buff.Read_const_char());
	m_isOverriden = buff.Read_bool();
	m_overrideValue = buff.Read_float();
}
#endif

	




ptxEvolvedKeyframe::ptxEvolvedKeyframe() 
{
	m_evolutionIdx = -1;
	m_isLodEvolution = false;
}

ptxEvolvedKeyframe::ptxEvolvedKeyframe(datResource& rsc)
: m_keyframe(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxEvolvedKeyframe_num++;
#endif
}

#if __DECLARESTRUCT
void ptxEvolvedKeyframe::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxEvolvedKeyframe)
		SSTRUCT_FIELD(ptxEvolvedKeyframe, m_keyframe)
		SSTRUCT_FIELD(ptxEvolvedKeyframe, m_evolutionIdx)
		SSTRUCT_FIELD(ptxEvolvedKeyframe, m_isLodEvolution)
		SSTRUCT_IGNORE(ptxEvolvedKeyframe, m_pad)
	SSTRUCT_END(ptxEvolvedKeyframe)
}
#endif

IMPLEMENT_PLACE(ptxEvolvedKeyframe);

#if RMPTFX_EDITOR
void ptxEvolvedKeyframe::SendToEditor(ptxByteBuff& buff, const ptxEffectRule* pEffectRule, s32 eventIdx, u32 propertyId)
{
	const char* pKeyframeDefnName = NULL;
	ptxKeyframeDefn* pKeyframeDefn = m_keyframe.GetDefn();
	if (pKeyframeDefn)
	{
		pKeyframeDefnName = pKeyframeDefn->GetName();
	}

	buff.Write_s32(m_evolutionIdx);
	buff.Write_const_char(pKeyframeDefnName);

	buff.Write_const_char(pEffectRule->GetName());
	buff.Write_s32(PTXRULETYPE_EFFECT);
	buff.Write_u32(propertyId);
	buff.Write_s32(eventIdx);
}
#endif

#if RMPTFX_EDITOR
void ptxEvolvedKeyframe::ReceiveFromEditor(const ptxByteBuff& buff)
{
	buff.Read_s32();
	buff.Read_const_char();
	buff.Read_const_char();
	buff.Read_s32();
	buff.Read_u32();
	buff.Read_s32();
}
#endif








ptxEvolvedKeyframeProp::ptxEvolvedKeyframeProp() 
{
	m_propertyId = 0;
	m_blendMode = PTXEVO_MODE_ADD; 
}

ptxEvolvedKeyframeProp::~ptxEvolvedKeyframeProp()
{
	for (int i=m_evolvedKeyframes.GetCount()-1; i>=0; i--)
	{
		m_evolvedKeyframes.Delete(i);
	}

	m_evolvedKeyframes.Reset();
}

ptxEvolvedKeyframeProp::ptxEvolvedKeyframeProp(datResource& rsc)
: m_evolvedKeyframes(rsc, 1)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxEvolvedKeyframeProp_num++;
#endif
}

#if __DECLARESTRUCT
void ptxEvolvedKeyframeProp::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxEvolvedKeyframeProp)
		SSTRUCT_FIELD(ptxEvolvedKeyframeProp, m_evolvedKeyframes)
		SSTRUCT_FIELD(ptxEvolvedKeyframeProp, m_propertyId)
		SSTRUCT_FIELD(ptxEvolvedKeyframeProp, m_blendMode)
		SSTRUCT_IGNORE(ptxEvolvedKeyframeProp, m_pad)
	SSTRUCT_END(ptxEvolvedKeyframeProp)
}
#endif

IMPLEMENT_PLACE(ptxEvolvedKeyframeProp);

#if RMPTFX_EDITOR
void ptxEvolvedKeyframeProp::ScaleEvolvedKeyframes(ScalarV_In vScale)
{
	for (int j=0; j<m_evolvedKeyframes.GetCount(); j++)
	{
		m_evolvedKeyframes[j].GetKeyframe().ScaleEntries(vScale);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEvolvedKeyframeProp::SendToEditor(ptxByteBuff& buff, const ptxEffectRule* pEffectRule, s32 eventIdx)
{
	buff.Write_u32(m_propertyId);
	buff.Write_u32(m_blendMode);
	buff.Write_u32(m_evolvedKeyframes.GetCount());

	for (int i=0; i<m_evolvedKeyframes.GetCount(); i++)
	{
		m_evolvedKeyframes[i].SendToEditor(buff, pEffectRule, eventIdx, m_propertyId);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEvolvedKeyframeProp::ReceiveFromEditor(const ptxByteBuff& buff)
{
#if __ASSERT
	u32 propertyId = 
#endif
		buff.Read_u32();

	ptxAssertf((m_propertyId==propertyId), "property id mismatch");

	m_blendMode = (u8) buff.Read_u32();

	int numEvolvedKeyframes = buff.Read_u32();
	ptxAssertf((numEvolvedKeyframes==m_evolvedKeyframes.GetCount()) , "num evolved keyframes mismatch");

	if (numEvolvedKeyframes==m_evolvedKeyframes.GetCount())
	{
		for (int i=0; i<numEvolvedKeyframes; i++)
		{
			m_evolvedKeyframes[i].ReceiveFromEditor(buff);
		}
	}
}
#endif
	





ptxEvolutionList::~ptxEvolutionList()
{
	// clean up the evolution array
	for (int i=m_evolutions.GetCount()-1; i>=0; i--)
	{
		m_evolutions.Delete(i);
	}

	// clean up the evolved keyframe property array
	for (int i=m_evolvedKeyframeProps.GetCount()-1; i>=0; i--)
	{
		m_evolvedKeyframeProps.Delete(i);
	}

	// reset all arrays and maps
	m_evolutions.Reset();
	m_evolvedKeyframeProps.Reset();
	m_evolvedKeyframePropMap.Reset();
}

ptxEvolutionList::ptxEvolutionList(datResource& rsc)
: m_evolutions(rsc, 1)
, m_evolvedKeyframeProps(rsc, 1)
, m_evolvedKeyframePropMap(rsc, atStringMap<datRef<ptxEvolvedKeyframeProp> >::PLACE_DATA)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxEvolutionList_num++;
#endif
}

#if __DECLARESTRUCT
void ptxEvolutionList::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxEvolutionList)
		SSTRUCT_FIELD(ptxEvolutionList, m_evolutions)
		SSTRUCT_FIELD(ptxEvolutionList, m_evolvedKeyframeProps)
		SSTRUCT_FIELD(ptxEvolutionList, m_evolvedKeyframePropMap)
	SSTRUCT_END(ptxEvolutionList)
}
#endif
	
IMPLEMENT_PLACE(ptxEvolutionList);

Vec4V_Out ptxEvolutionList::Query(ptxKeyframeProp* pKeyframeProp, ScalarV_In vKeyTime, const ptxEvoValueType* pEvoValues)
{
	// get the base value of the keyframed property
	Vec4V vBaseValue = pKeyframeProp->GetKeyframe().Query(vKeyTime);

	// get the property's evolved data
	ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = pKeyframeProp->GetEvolvedKeyframeProp();
	if (pEvolvedKeyframeProp)
	{
		// blend the base value with the evolved value and return it
		Vec4V vFinalValue = vBaseValue;
		switch (pEvolvedKeyframeProp->GetBlendMode())
		{
			case PTXEVO_MODE_AVERAGE: 
			{
				vFinalValue = ApplyBlendAverage(pEvolvedKeyframeProp, vBaseValue, vKeyTime, pEvoValues);
				break;
			}
			case PTXEVO_MODE_ADD: 
			{
				vFinalValue = ApplyBlendAdditive(pEvolvedKeyframeProp, vBaseValue, vKeyTime, pEvoValues);
				break;
			}
			case PTXEVO_MODE_MAX: 
			{
				vFinalValue = ApplyBlendMax(pEvolvedKeyframeProp, vBaseValue, vKeyTime, pEvoValues);
				break;
			}
			case PTXEVO_MODE_FULL_AVERAGE:
			{
				vFinalValue = ApplyBlendFullAverage(pEvolvedKeyframeProp, vBaseValue, vKeyTime, pEvoValues);
				break;
			}
			default:
			{
				ptxAssertf(0, "invalid evolution blend mode detected");
				break;
			}
		}

		return vFinalValue;
	}

	// no evolved data - just use the base value
	return vBaseValue;
}

ptxEvolvedKeyframeProp* ptxEvolutionList::GetEvolvedKeyframePropById(u32 propertyId) const
{
	// look up the evolved keyframed property in the map
	datRef<ptxEvolvedKeyframeProp> const* pEvolvedKeyframeProp = m_evolvedKeyframePropMap.SafeGet(propertyId);

	// check for valid values 
	if (pEvolvedKeyframeProp==NULL)
	{
		return NULL;
	}

	// return the evolved keyframed property
	return *pEvolvedKeyframeProp;
}
//
//int ptxEvolutionList::GetEvolutionIndex(const char* pEvoName) const
//{
//	// search for the evolution
//	for (int i=0; i<m_evolutions.GetCount(); i++)
//	{
//		if (stricmp(m_evolutions[i].GetName(), pEvoName)==0)
//		{
//			// found it - return the index
//			return i;
//		}
//	}
//
//	// didn't find the evolution
//	return -1;
//}

int ptxEvolutionList::GetEvolutionIndexFromHash(atHashValue evoHashValue) const
{
	// search for the evolution
	for (int i=0; i<m_evolutions.GetCount(); i++)
	{
		if (m_evolutions[i].GetNameHash()==evoHashValue)
		{
			// found it - return the index
			return i;
		}
	}

	// didn't find the evolution
	return -1;
}

int ptxEvolutionList::GetEvolutionIndexFromHash16(u16 evoHashValue) const
{
	// search for the evolution
	for (int i=0; i<m_evolutions.GetCount(); i++)
	{
		if (atHash16U(m_evolutions[i].GetNameString())==evoHashValue)
		{
			// found it - return the index
			return i;
		}
	}

	// didn't find the evolution
	return -1;
}

void ptxEvolutionList::SetKeyframeDefn(ptxKeyframeProp* pKeyframeProp, ptxKeyframeDefn* pKeyframeDefn)
{
	// look for an evolved keyframed property
	ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = GetEvolvedKeyframePropById(pKeyframeProp->GetPropertyId());
	if (pEvolvedKeyframeProp)
	{
		// go through the evolved keyframes
		for (int i=0; i<pEvolvedKeyframeProp->GetNumEvolvedKeyframes(); i++)
		{
			ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);

			// set the definition
			evolvedKeyframe.GetKeyframe().SetDefn(pKeyframeDefn);
		}
	}
}

#if RMPTFX_EDITOR
void ptxEvolutionList::GenerateUniqueName(char* pName, int maxLength)
{
	// start at the current count
	int numEvolutions = m_evolutions.GetCount()+1;
	
	// create names until we have a unique one
	do
	{
		formatf(pName, maxLength, "Evo %02d", numEvolutions++);
	}
	while (GetEvolutionIndexFromHash(atHashValue(pName))!=-1);
}
#endif

#if RMPTFX_EDITOR
int ptxEvolutionList::AddEvolution(const char* pEvoName)
{
	// check if the evolution exists already
	for (int i=0; i<m_evolutions.GetCount(); i++)
	{
		int evolutionIdx = GetEvolutionIndexFromHash(atHashValue(pEvoName));
		if (evolutionIdx>=0)
		{
			return evolutionIdx;
		}
	}

	// add a new evolution 
	ptxEvolution& newEvolution = m_evolutions.Grow();
	newEvolution.SetName(pEvoName);

	// return the new evolution id
	return m_evolutions.GetCount()-1;
}
#endif

#if RMPTFX_EDITOR
void ptxEvolutionList::DeleteEvolution(const char* pEvoName)
{
// 	// search for the evolution
// 	int evolutionIdx = GetEvolutionIndex(pEvoName);
// 	if (evolutionIdx>=0)
// 	{
// 		// delete the evolution
// 		m_evolutions.Delete(evolutionIdx);
// 
// 		// delete any evolved keyframed propeerties
// 		if (m_evolvedKeyframeProps.GetCount()>0)
// 		{
// 			// we've deleted an evolution so we need to update all of the evolution indices
// 			// first generate the new indices
// 			int newEvolutionIdx[PTXEVO_MAX_EVOLUTIONS];
// 			for (int i=0; i<PTXEVO_MAX_EVOLUTIONS; i++)
// 			{
// 				newEvolutionIdx[i] = i;
// 			}
// 
// 			// each evolution index above the one we've removed needs decremented
// 			for (int i=evolutionIdx+1; i<PTXEVO_MAX_EVOLUTIONS; i++)
// 			{
// 				newEvolutionIdx[i] -= 1;
// 			}
// 
// 			// the evolution we've removed no longer exists
// 			newEvolutionIdx[evolutionIdx] = -1;
// 
// 			// go through the evolved keyframed properties
// 			for (int i=m_evolvedKeyframeProps.GetCount()-1; i>=0; i--)
// 			{
// 				// go through the evolved keyframes of this evolved property
// 				for (int j=m_evolvedKeyframeProps[i].GetNumEvolvedKeyframes()-1; j>=0; j--)
// 				{
// 					ptxEvolvedKeyframe& evolvedKeyframe = m_evolvedKeyframeProps[i].GetEvolvedKeyframe(j);
// 
// 					// patch up this evolved keyframe with the new evolution index
// 					evolvedKeyframe.SetEvolutionIndex(newEvolutionIdx[evolvedKeyframe.GetEvolutionIndex()]);
// 					
// 					// delete any evolved keyframe using the evolution we've just deleted
// 					if (evolvedKeyframe.GetEvolutionIndex()==-1)
// 					{
// 						m_evolvedKeyframeProps[i].DeleteEvolvedKeyframe(j);
// 					}
// 				}
// 
// 				// delete any evolved keyframed properties that are now empty
// 				if (m_evolvedKeyframeProps[i].GetNumEvolvedKeyframes()==0)
// 				{
// 					m_evolvedKeyframeProps.Delete(i);
// 					GenerateKeyframePropMap();
// 				}
// 			}
// 		}
// 	}

	// search for the evolution
	int evolutionIdx = GetEvolutionIndexFromHash(atHashValue(pEvoName));
	if (evolutionIdx>=0)
	{
		// delete the evolution
		m_evolutions.Delete(evolutionIdx);

		// delete any evolved keyframed propeerties
		if (m_evolvedKeyframeProps.GetCount()>0)
		{
			// go through the evolved keyframed properties
			for (int i=m_evolvedKeyframeProps.GetCount()-1; i>=0; i--)
			{
				// go through the evolved keyframes of this evolved property
				for (int j=m_evolvedKeyframeProps[i].GetNumEvolvedKeyframes()-1; j>=0; j--)
				{
					ptxEvolvedKeyframe& evolvedKeyframe = m_evolvedKeyframeProps[i].GetEvolvedKeyframe(j);

					// update any evolution indices
					if (evolvedKeyframe.GetEvolutionIndex()==evolutionIdx)
					{
						// this evolution index has just been deleted 
						// delete the evolved keyframe as well
						m_evolvedKeyframeProps[i].DeleteEvolvedKeyframe(j);
					}
					else if (evolvedKeyframe.GetEvolutionIndex()>evolutionIdx)
					{
						// this evolution index is above the one we're just deleted
						// decrement the evolution index
						evolvedKeyframe.SetEvolutionIndex(evolvedKeyframe.GetEvolutionIndex()-1);
					}
				}

				// delete any evolved keyframed properties that are now empty
				if (m_evolvedKeyframeProps[i].GetNumEvolvedKeyframes()==0)
				{
					m_evolvedKeyframeProps.Delete(i);
					GenerateKeyframePropMap();
				}
			}
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEvolutionList::AddEvolvedKeyframeProp(ptxKeyframeProp* pKeyframeProp, const char* pEvoName)
{
	ptxAssertf(pKeyframeProp, "keyframed property pointer is invalid");

	// add the evolution
	int evolutionIdx = AddEvolution(pEvoName);

	// check if this keyframed property already has an evolved keyframe
	ptxEvolvedKeyframe* pEvolvedKeyframe = NULL;
	for (int i=0; i<m_evolvedKeyframeProps.GetCount(); i++)
	{
		// check if the property ids match
		if (m_evolvedKeyframeProps[i].GetPropertyId() == pKeyframeProp->GetPropertyId())
		{
			// they do - go through the evolved keyframes
			for (int j=0; j<m_evolvedKeyframeProps[i].GetNumEvolvedKeyframes(); j++)
			{
				ptxEvolvedKeyframe& evolvedKeyframe = m_evolvedKeyframeProps[i].GetEvolvedKeyframe(j);

				// check if the evolved keyframe evolves on this evolution
				if (evolvedKeyframe.GetEvolutionIndex() == evolutionIdx)
				{
					// it does - set the evolved keyframe the same as the base keyframe
					ptxAssertf(0, "we shouldn't be able to add an evolution to a keyframed property that already has the same evolution set on it");
					evolvedKeyframe.SetKeyframe(pKeyframeProp->GetKeyframe());
					return;
				}
			}

			// no evolved keyframe currently exists
			// add a new one and initialise to the evolved keyframe the same as the base keyframe
			pEvolvedKeyframe = &m_evolvedKeyframeProps[i].AddEvolvedKeyframe();
			pEvolvedKeyframe->SetEvolutionIndex(evolutionIdx);
			pEvolvedKeyframe->SetKeyframe(pKeyframeProp->GetKeyframe());
			pEvolvedKeyframe->GetKeyframe().CopyDefnFrom(pKeyframeProp->GetKeyframe());

			return;
		}
	}

	// the keyframed property didn't have an evolved keyframe - create one
	ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = &m_evolvedKeyframeProps.Grow();
	pEvolvedKeyframeProp->SetPropertyId(pKeyframeProp->GetPropertyId());

	// reset the evolved keyframes
	pEvolvedKeyframeProp->ResetEvolvedKeyframes();

	// add the evolved keyframe
	pEvolvedKeyframe = &pEvolvedKeyframeProp->AddEvolvedKeyframe();
	pEvolvedKeyframe->SetEvolutionIndex(evolutionIdx);
	pEvolvedKeyframe->SetKeyframe(pKeyframeProp->GetKeyframe());
	pEvolvedKeyframe->GetKeyframe().CopyDefnFrom(pKeyframeProp->GetKeyframe());

	// re-generate the map
	GenerateKeyframePropMap();
}
#endif

#if RMPTFX_EDITOR
void ptxEvolutionList::DeleteEvolvedKeyframeProp(ptxKeyframeProp* pKeyframeProp, const char* pEvoName)
{
	ptxAssertf(pKeyframeProp, "keyframed property pointer is invalid");

	// get the evolution index
	int evolutionIdx = GetEvolutionIndexFromHash(atHashValue(pEvoName));
	if (evolutionIdx>=0)
	{
		// go through the evolved keyframed properties
		for (int i=0; i<m_evolvedKeyframeProps.GetCount(); i++)
		{
			// check if the property ids match
			if (m_evolvedKeyframeProps[i].GetPropertyId() == pKeyframeProp->GetPropertyId())
			{
				// they do - go through the evolved keyframes
				for (int j=m_evolvedKeyframeProps[i].GetNumEvolvedKeyframes()-1; j>=0; j--)
				{
					ptxEvolvedKeyframe& evolvedKeyframe = m_evolvedKeyframeProps[i].GetEvolvedKeyframe(j);

					// check if the evolved keyframe evolves on this evolution
					if (evolvedKeyframe.GetEvolutionIndex() == evolutionIdx)
					{
						// it does - delete the evolved keyframe
						m_evolvedKeyframeProps[i].DeleteEvolvedKeyframeFast(j);	
					}
				}

				// delete any evolved keyframed properties that are now empty
				if (m_evolvedKeyframeProps[i].GetNumEvolvedKeyframes()==0)
				{
					m_evolvedKeyframeProps.Delete(i);
					GenerateKeyframePropMap();
				}

				// all done
				return;
			}
		}
	}
}
#endif

#if RMPTFX_EDITOR
#if RMPTFX_BANK
void ptxEvolutionList::UpdateUIData(ptxKeyframePropList* pKeyframePropList, const ptxEffectRule* pEffectRule, s32 eventIdx)
{
	ptxAssertf(pKeyframePropList, "keyframed property list pointer is invalid");

	// check that we have some evolved keyframed properties
	if (pKeyframePropList && m_evolvedKeyframeProps.GetCount()>0)
	{
		// go through the keyframed properties in the list
		for (int i=0; i<pKeyframePropList->GetNumKeyframeProps(); i++)
		{
			// get the keyframed property
			ptxKeyframeProp* pKeyframeProp = pKeyframePropList->GetKeyframePropByIndex(i);

			// update it's ui data
			UpdateUIData(pKeyframeProp, pEffectRule, eventIdx);
		}
	}
}
#else
void ptxEvolutionList::UpdateUIData(ptxKeyframePropList* UNUSED_PARAM(pKeyframePropList), const ptxEffectRule* UNUSED_PARAM(pEffectRule), s32 UNUSED_PARAM(eventIdx)) 
{
}
#endif
#endif

#if RMPTFX_EDITOR
#if RMPTFX_BANK
void ptxEvolutionList::UpdateUIData(ptxKeyframeProp* pKeyframeProp, const ptxEffectRule* pEffectRule, s32 eventIdx)
{
	// look for an evolved keyframed property
	ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = GetEvolvedKeyframePropById(pKeyframeProp->GetPropertyId());
	if (pEvolvedKeyframeProp)
	{
		// go through the evolved keyframes
		for (int i=0; i<pEvolvedKeyframeProp->GetNumEvolvedKeyframes(); i++)
		{
			ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);

			// set the definition
			evolvedKeyframe.GetKeyframe().CopyDefnFrom(pKeyframeProp->GetKeyframe());
			RMPTFXMGR.SendEvolvedKeyframeDefnToEditor(&evolvedKeyframe.GetKeyframe(), pEffectRule, pKeyframeProp->GetPropertyId(), eventIdx, evolvedKeyframe.GetEvolutionIndex());
		}
	}
}
#else 
void ptxEvolutionList::UpdateUIData(ptxKeyframeProp* UNUSED_PARAM(pKeyframeProp), const ptxEffectRule* UNUSED_PARAM(pEffectRule), s32 UNUSED_PARAM(eventIdx)) 
{
}
#endif
#endif

#if RMPTFX_EDITOR
void ptxEvolutionList::SendToEditor(ptxByteBuff& buff, const ptxEffectRule* pEffectRule, s32 eventIdx)
{
	// send evolution data
	buff.Write_u32(m_evolutions.GetCount());
	for (int i=0; i<m_evolutions.GetCount(); i++)
	{
		m_evolutions[i].SendToEditor(buff);
	}

	// send evolved keyframed property data
	buff.Write_u32(m_evolvedKeyframeProps.GetCount());
	for (int i=0; i<m_evolvedKeyframeProps.GetCount(); i++)
	{
		m_evolvedKeyframeProps[i].SendToEditor(buff, pEffectRule, eventIdx);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEvolutionList::ReceiveFromEditor(const ptxByteBuff& buff)
{
	// receive evolution data
#if __ASSERT
	int numEvolutions = 
#endif
		buff.Read_u32();

	ptxAssertf(numEvolutions == m_evolutions.GetCount(), "evolution count mismatch");

	for (int i=0; i<m_evolutions.GetCount(); i++)
	{
		m_evolutions[i].ReceiveFromEditor(buff);
	}

	// receive evolved keyframed property data
#if __ASSERT
	int numEvolvedKeyframeProps = 
#endif
		buff.Read_u32();

	ptxAssertf(numEvolvedKeyframeProps == m_evolvedKeyframeProps.GetCount(), "evolved keyframed property mismatch");

	for (int i=0; i<m_evolvedKeyframeProps.GetCount(); i++)
	{
		m_evolvedKeyframeProps[i].ReceiveFromEditor(buff);
	}

	// we need to flag any evolved keyframes that are on lod evolutions (as lod evolutions blend differently)
	int lodEvoIdx = GetEvolutionIndexFromHash(ATSTRINGHASH("LOD", 0x2FBE6388));
	for (int i=0; i<m_evolvedKeyframeProps.GetCount(); i++)
	{
		// flag any evolved lod keyframes
		for (int j=0; j<m_evolvedKeyframeProps[i].GetNumEvolvedKeyframes(); j++)
		{
			ptxEvolvedKeyframe& evolvedKeyframe = m_evolvedKeyframeProps[i].GetEvolvedKeyframe(j);
			if (evolvedKeyframe.GetEvolutionIndex()==lodEvoIdx)
			{
				evolvedKeyframe.SetIsLodEvolution(true);
			}
			else
			{
				evolvedKeyframe.SetIsLodEvolution(false);
			}
		}
	}
}
#endif

Vec4V_Out ptxEvolutionList::ApplyBlendAverage(ptxEvolvedKeyframeProp* pEvolvedKeyframeProp, Vec4V_In vBaseValue, ScalarV_In vKeyTime, const ptxEvoValueType* pEvoValues)
{
	// cache some values
	ScalarV vZero(V_ZERO);
	ScalarV vOne(V_ONE);
	ScalarV vNumEvolvedValues(V_ZERO);
	Vec4V vAccumValue(V_ZERO);

	// vectorise the evolution values
	ScalarV vEvoValues[PTXEVO_MAX_EVOLUTIONS];
	for (int i=0; i<PTXEVO_MAX_EVOLUTIONS; i++)
	{
		vEvoValues[i] = ScalarVFromF32(UnPackEvoValue(pEvoValues[i]));
	}

	// lod evo data
	int lodEvoIdx = -1;
	Vec4V vEvolvedValueLod = Vec4V(V_ZERO);

	// go through the property's evolved keyframes
	for (int i=pEvolvedKeyframeProp->GetNumEvolvedKeyframes()-1; i>=0; i--)
	{
		// get the evolved keyframe
		ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);

		// check that the evolution is active (it's value is above zero)
		int evoIdx = evolvedKeyframe.GetEvolutionIndex();
		if (IsGreaterThanAll(vEvoValues[evoIdx], vZero))
		{
			// don't blend if this is the lod evolution
			if (evolvedKeyframe.GetIsLodEvolution()==false)
			{
				// get the evolved value
				Vec4V vEvolvedValue = evolvedKeyframe.GetKeyframe().Query(vKeyTime);

				// interpolate between the base and evolved value and add to the accumulated value
				vAccumValue += Lerp(vEvoValues[evoIdx], vBaseValue, vEvolvedValue);

				// keep track of how many evolved values we've added to the final value
				vNumEvolvedValues += vOne;
			}
			else
			{
				// store the lod evolved value
				ptxAssertf(lodEvoIdx==-1, "evolution blending has already encountered an active lod evolution");
				lodEvoIdx = evoIdx;
				vEvolvedValueLod = evolvedKeyframe.GetKeyframe().Query(vKeyTime);
			}
		}
	}

	// the current value is the accumulated value divided by the number of accumulated values (i.e. the average)
	Vec4V vCurrValue = InvScaleSafe(vAccumValue, vNumEvolvedValues, vAccumValue);

	// blend in any lod evolution
	if (lodEvoIdx>-1)
	{
		vCurrValue = vCurrValue + (vEvolvedValueLod - vCurrValue) * vEvoValues[lodEvoIdx];
	}

	return vCurrValue;
}

Vec4V_Out ptxEvolutionList::ApplyBlendAdditive(ptxEvolvedKeyframeProp* pEvolvedKeyframeProp, Vec4V_In vBaseValue, ScalarV_In vKeyTime, const ptxEvoValueType* pEvoValues)
{
	// cache some values
	ScalarV vZero(V_ZERO);
	Vec4V vAccumValue(V_ZERO);

	// lod evo data
	int lodEvoIdx = -1;
	Vec4V vEvolvedValueLod = Vec4V(V_ZERO);

	// go through the property's evolved keyframes
	for (int i=pEvolvedKeyframeProp->GetNumEvolvedKeyframes()-1; i>=0; i--)
	{
		// get the evolved keyframe
		ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);

		// check that the evolution is active (it's value is above zero)
		int evoIdx = evolvedKeyframe.GetEvolutionIndex();
		const ScalarV vEvoValue = UnPackEvoValueV(pEvoValues[evoIdx]);
		if (IsGreaterThanAll(vEvoValue, vZero))
		{
			// don't blend if this is the lod evolution
			if (evolvedKeyframe.GetIsLodEvolution()==false)
			{
				// get the evolved value
				const Vec4V vEvolvedValue = evolvedKeyframe.GetKeyframe().Query(vKeyTime);

				// calculate the difference between the evolved and base values
				// accumulate the delta values
				vAccumValue = AddScaled(vAccumValue, vEvolvedValue - vBaseValue, vEvoValue);
			}
			else
			{
				// store the lod evolved value
				ptxAssertf(lodEvoIdx==-1, "evolution blending has already encountered an active lod evolution");
				lodEvoIdx = evoIdx;
				vEvolvedValueLod = evolvedKeyframe.GetKeyframe().Query(vKeyTime);
			}
		}
	}

	// the current value is the base value plus the accumulated value
	Vec4V vCurrValue = vBaseValue + vAccumValue;

	// blend in any lod evolution
	if (lodEvoIdx>-1)
	{
		vCurrValue = AddScaled(vCurrValue, (vEvolvedValueLod - vCurrValue), UnPackEvoValueV(pEvoValues[lodEvoIdx]));
	}

	return vCurrValue;
}

Vec4V_Out ptxEvolutionList::ApplyBlendMax(ptxEvolvedKeyframeProp* pEvolvedKeyframeProp, Vec4V_In vBaseValue, ScalarV_In vKeyTime, const ptxEvoValueType* pEvoValues)
{
	// cache some values
	ScalarV vZero(V_ZERO);
	Vec4V vCurrValue = vBaseValue;

	// vectorise the evolution times
	ScalarV vEvoValues[PTXEVO_MAX_EVOLUTIONS];
	for (int i=0; i<PTXEVO_MAX_EVOLUTIONS; i++)
	{
		vEvoValues[i] = ScalarVFromF32(UnPackEvoValue(pEvoValues[i]));
	}

	// lod evo data
	int lodEvoIdx = -1;
	Vec4V vEvolvedValueLod = Vec4V(V_ZERO);

	// go through the property's evolved keyframes
	for (int i=pEvolvedKeyframeProp->GetNumEvolvedKeyframes()-1; i>=0; i--)
	{
		// get the evolved keyframe
		ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);
		// check that the evolution is active (it's value is above zero)
		int evoIdx = evolvedKeyframe.GetEvolutionIndex();
		const ScalarV vEvoValue = UnPackEvoValueV(pEvoValues[evoIdx]);
		if (IsGreaterThanAll(vEvoValue, vZero))
		{
			// don't blend if this is the lod evolution
			if (evolvedKeyframe.GetIsLodEvolution()==false)
			{
				// get the evolved value
				Vec4V vEvolvedValue = evolvedKeyframe.GetKeyframe().Query(vKeyTime);

				// interpolate between the base and evolved value and store as the final value if it's larger than the current final value
				vCurrValue = Max(vCurrValue, Lerp(vEvoValue, vBaseValue, vEvolvedValue));
			}
			else
			{
				// store the lod evolved value
				ptxAssertf(lodEvoIdx==-1, "evolution blending has already encountered an active lod evolution");
				lodEvoIdx = evoIdx;
				vEvolvedValueLod = evolvedKeyframe.GetKeyframe().Query(vKeyTime);
			}
		}
	}

	// blend in any lod evolution
	if (lodEvoIdx>-1)
	{
		vCurrValue = AddScaled(vCurrValue, (vEvolvedValueLod - vCurrValue), UnPackEvoValueV(pEvoValues[lodEvoIdx]));
	}

	return vCurrValue;
}

Vec4V_Out ptxEvolutionList::ApplyBlendFullAverage(ptxEvolvedKeyframeProp* pEvolvedKeyframeProp, Vec4V_In vBaseValue, ScalarV_In vKeyTime, const ptxEvoValueType* pEvoValues)
{
	// cache some values
	ScalarV vZero(V_ZERO);
	ScalarV vOne(V_ONE);
	ScalarV vNumEvolvedValues(V_ZERO);
	Vec4V vAccumValue(V_ZERO);

	// vectorise the evolution times
	ScalarV vEvoValues[PTXEVO_MAX_EVOLUTIONS];
	for (int i=0; i<PTXEVO_MAX_EVOLUTIONS; i++)
	{
		vEvoValues[i] = ScalarVFromF32(UnPackEvoValue(pEvoValues[i]));
	}

	// lod evo data
	int lodEvoIdx = -1;
	Vec4V vEvolvedValueLod = Vec4V(V_ZERO);

	// go through the property's evolved keyframes
	for (int i=pEvolvedKeyframeProp->GetNumEvolvedKeyframes()-1; i>=0; i--)
	{
		// get the evolved keyframe
		ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);

		int evoIdx = evolvedKeyframe.GetEvolutionIndex();

		// don't blend if this is the lod evolution
		if (evolvedKeyframe.GetIsLodEvolution()==false)
		{
			// get the evolved value
			Vec4V vEvolvedValue = evolvedKeyframe.GetKeyframe().Query(vKeyTime);

			// interpolate between the base and evolved value and add to the accumulated value
			vAccumValue += Lerp(vEvoValues[evoIdx], vBaseValue, vEvolvedValue);

			// keep track of how many evolved values we've added to the final value
			vNumEvolvedValues += vOne;
		}
		else
		{
			// store the lod evolved value
			ptxAssertf(lodEvoIdx==-1, "evolution blending has already encountered an active lod evolution");
			lodEvoIdx = evoIdx;
			vEvolvedValueLod = evolvedKeyframe.GetKeyframe().Query(vKeyTime);
		}
	}

	// the current value is the accumulated value divided by the number of accumulated values (i.e. the average)
	Vec4V vCurrValue = InvScaleSafe(vAccumValue, vNumEvolvedValues, vAccumValue);

	// blend in any lod evolution
	if (lodEvoIdx>-1)
	{
		vCurrValue = vCurrValue + (vEvolvedValueLod - vCurrValue) * vEvoValues[lodEvoIdx];
	}

	return vCurrValue;
}

#if RMPTFX_XML_LOADING
void ptxEvolutionList::OnPostLoad()
{
	GenerateKeyframePropMap();
}
#endif

#if RMPTFX_XML_LOADING
void ptxEvolutionList::GenerateKeyframePropMap()
{
	// reserve the correct number of entries in the map
	m_evolvedKeyframePropMap.Reset();
	m_evolvedKeyframePropMap.Reserve(m_evolvedKeyframeProps.GetCount());

	// insert the keyframed properties into the map
	for (int i=0; i<m_evolvedKeyframeProps.GetCount(); i++)
	{
		m_evolvedKeyframePropMap.InsertSorted(m_evolvedKeyframeProps[i].GetPropertyId(), &m_evolvedKeyframeProps[i]);
	}

	// finalise the map
	m_evolvedKeyframePropMap.FinishInsertion();
}	
#endif
