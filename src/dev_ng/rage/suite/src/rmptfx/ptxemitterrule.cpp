// 
// rmptfx/ptxemitterrule.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 


// includes (us)
#include "rmptfx/ptxemitterrule.h"
#include "rmptfx/ptxemitterrule_parser.h"

// include (rmptfx)
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxinterface.h"
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxparticlerule.h"

// includes (rage)
#include "bank/msgbox.h"
#include "file/asset.h"
#include "parser/manager.h"
#include "grprofile/drawmanager.h"
#include "profile/element.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// params
#if !__FINAL
XPARAM(ptfxassetoverride);
#endif


// namespaces
using namespace rage;


// static variables
//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxEmitterRule::sm_spawnRateOverTimeDefn(		"Spawn Rate Over Time",		atHashValue("ptxEmitterRule:m_spawnRateOverTimeKFP"),		PTXKEYFRAMETYPE_FLOAT3,		Vec4V(1.0f, 1.0f, 0.0f, 0.0f),		Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		10000.0f,	0.1f),		"Rate Per Sec Min",		"Rate Per Sec Max",		"Particle Limit");
ptxKeyframeDefn ptxEmitterRule::sm_spawnRateOverDistDefn(		"Spawn Rate Over Distance",	atHashValue("ptxEmitterRule:m_spawnRateOverDistKFP"),		PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		100.0f,		0.1f),		"Rate Per Metre Min",	"Rate Per Metre Max");
ptxKeyframeDefn ptxEmitterRule::sm_particleLifeDefn(			"Particle Life",			atHashValue("ptxEmitterRule:m_particleLifeKFP"),			PTXKEYFRAMETYPE_FLOAT2,		Vec4V(1.0f, 1.0f, 0.0f, 0.0f),		Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.01f,	100.0f,		0.01f),		"Life Min",				"Life Max");
ptxKeyframeDefn ptxEmitterRule::sm_playbackRateScalarDefn(		"Playback Rate Scalar",		atHashValue("ptxEmitterRule:m_playbackRateScalarKFP"),		PTXKEYFRAMETYPE_FLOAT,		Vec4V(1.0f, 0.0f, 0.0f, 0.0f),		Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.01f,	100.0f,		0.01f),		"Playback Rate");
ptxKeyframeDefn ptxEmitterRule::sm_speedScalarDefn(				"Speed Scalar",				atHashValue("ptxEmitterRule:m_speedScalarKFP"),				PTXKEYFRAMETYPE_FLOAT2,		Vec4V(1.0f, 1.0f, 0.0f, 0.0f),		Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-1000.0f,	1000.0f,	0.01f),		"Speed Scalar Min",		"Speed Scalar Max");
ptxKeyframeDefn ptxEmitterRule::sm_sizeScalarDefn(				"Size Scalar",				atHashValue("ptxEmitterRule:m_sizeScalarKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(ScalarVFromF32(100.0f)),		Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1000.0f,	0.01f),		"Width %",				"Height %",				"Depth %");
ptxKeyframeDefn ptxEmitterRule::sm_accnScalarDefn(				"Acceleration Scalar",		atHashValue("ptxEmitterRule:m_accnScalarKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(ScalarVFromF32(100.0f)),		Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"X %", 					"Y %", 					"Z %");
ptxKeyframeDefn ptxEmitterRule::sm_dampeningScalarDefn(			"Dampening Scalar",			atHashValue("ptxEmitterRule:m_dampeningScalarKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(ScalarVFromF32(100.0f)),		Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"X %", 					"Y %", 					"Z %");
ptxKeyframeDefn ptxEmitterRule::sm_matrixWeightScalarDefn(		"Matrix Weight Scalar",		atHashValue("ptxEmitterRule:m_matrixWeightScalarKFP"),		PTXKEYFRAMETYPE_FLOAT,		Vec4V(ScalarVFromF32(100.0f)),		Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"Matrix Weight %");
ptxKeyframeDefn ptxEmitterRule::sm_inheritVelocityDefn(			"Inherit Velocity",			atHashValue("ptxEmitterRule:m_inheritVelocityKFP"),			PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-100.0f,	100.0f,		0.01f),		"Velocity % Min",		"Velocity % Max");


// code
#if RMPTFX_EDITOR
ptxEmitterRuleUIData::ptxEmitterRuleUIData()
: m_accumCPUUpdateTime(0.0f)
, m_accumCPUDrawTime(0.0f)
, m_accumCPUSortTime(0.0f)
, m_numPointsUpdated(0)
, m_numActiveInstances(0)
, m_showCreationDomain(false)
, m_showTargetDomain(false)
, m_showAttractorDomain(false)
{
}
#endif

#if RMPTFX_EDITOR
void ptxEmitterRuleUIData::StartProfileUpdate()
{
	m_accumCPUUpdateTime = 0.0f;
	m_numPointsUpdated = 0;
	m_numActiveInstances = 0;
}
#endif

#if RMPTFX_EDITOR
void ptxEmitterRuleUIData::StartProfileDraw()
{
	m_accumCPUDrawTime = 0.0f;
	m_accumCPUSortTime = 0.0f;
}
#endif

#if RMPTFX_EDITOR
void ptxEmitterRuleUIData::SendDebugDataToEditor(ptxByteBuff& buff)
{
	buff.Write_bool(m_showCreationDomain);
	buff.Write_bool(m_showTargetDomain);
	buff.Write_bool(m_showAttractorDomain);
}
#endif

#if RMPTFX_EDITOR
void ptxEmitterRuleUIData::ReceiveDebugDataFromEditor(ptxByteBuff& buff)
{
	m_showCreationDomain = buff.Read_bool();
	m_showTargetDomain = buff.Read_bool();
	m_showAttractorDomain = buff.Read_bool();
}
#endif

#if RMPTFX_EDITOR
void ptxEmitterRuleUIData::AccumulateFrom(ptxEmitterRuleUIData& uiData)
{
	m_accumCPUUpdateTime += uiData.m_accumCPUUpdateTime;
	m_accumCPUDrawTime += uiData.m_accumCPUDrawTime;
	m_accumCPUSortTime += uiData.m_accumCPUSortTime;
	m_numPointsUpdated += uiData.m_numPointsUpdated;
	m_numActiveInstances += uiData.m_numActiveInstances;
}
#endif

ptxEmitterRule::ptxEmitterRule() : pgBaseRefCounted(0)
{
	m_fileVersion = PTXEMITTERRULE_FILE_VERSION;

#if __RESOURCECOMPILER
	m_pLastEvoList_NOTUSED = NULL;
#else
	m_pLastEvoList_NOTUSED = (void*)0xFFFFFFFF;											// not even NULL, to make sure we get a "cache miss" the first time it runs
#endif

	m_pUIData = NULL;

	// keyframe properties
	Vec4V vDataOneX = Vec4V(1.0f, 0.0f, 0.0f, 0.0f);
	Vec4V vDataOneXY = Vec4V(1.0f, 1.0f, 0.0f, 0.0f);
	Vec4V vDataHundredXYZW = Vec4V(100.0f, 100.0f, 100.0f, 100.0f);

	m_spawnRateOverTimeKFP.Init(vDataOneXY, atHashValue("ptxEmitterRule:m_spawnRateOverTimeKFP"));
	m_spawnRateOverDistKFP.Init(Vec4V(V_ZERO), atHashValue("ptxEmitterRule:m_spawnRateOverDistKFP"));
	m_particleLifeKFP.Init(vDataOneXY, atHashValue("ptxEmitterRule:m_particleLifeKFP"));
	m_playbackRateScalarKFP.Init(vDataOneX, atHashValue("ptxEmitterRule:m_playbackRateScalarKFP"));
	m_speedScalarKFP.Init(vDataOneXY, atHashValue("ptxEmitterRule:m_speedScalarKFP"));
	m_sizeScalarKFP.Init(vDataHundredXYZW, atHashValue("ptxEmitterRule:m_sizeScalarKFP"));
	m_accnScalarKFP.Init(vDataHundredXYZW, atHashValue("ptxEmitterRule:m_accnScalarKFP"));
	m_dampeningScalarKFP.Init(vDataHundredXYZW, atHashValue("ptxEmitterRule:m_dampeningScalarKFP"));
	m_matrixWeightScalarKFP.Init(vDataHundredXYZW, atHashValue("ptxEmitterRule:m_matrixWeightScalarKFP"));
	m_inheritVelocityKFP.Init(Vec4V(V_ZERO), atHashValue("ptxEmitterRule:m_inheritVelocityKFP"));

	// property list
	m_keyframePropList.Reserve(10);
// #if RMPTFX_EDITOR
// 	m_keyframePropList.AddKeyframeProp(&m_spawnRateOverTimeKFP, &sm_spawnRateOverTimeDefn);
// 	m_keyframePropList.AddKeyframeProp(&m_spawnRateOverDistKFP, &sm_spawnRateOverDistDefn);
// 	m_keyframePropList.AddKeyframeProp(&m_particleLifeKFP, &sm_particleLifeDefn);
// 	m_keyframePropList.AddKeyframeProp(&m_playbackRateScalarKFP, &sm_playbackRateScalarDefn);
// 	m_keyframePropList.AddKeyframeProp(&m_speedScalarKFP, &sm_speedScalarDefn);
// 	m_keyframePropList.AddKeyframeProp(&m_sizeScalarKFP, &sm_sizeScalarDefn);
// 	m_keyframePropList.AddKeyframeProp(&m_accnScalarKFP, &sm_accnScalarDefn);
// 	m_keyframePropList.AddKeyframeProp(&m_dampeningScalarKFP, &sm_dampeningScalarDefn);
// 	m_keyframePropList.AddKeyframeProp(&m_matrixWeightScalarKFP, &sm_matrixWeightScalarDefn);
// 	m_keyframePropList.AddKeyframeProp(&m_inheritVelocityKFP, &sm_inheritVelocityDefn);
// 	CreateUiObjects();
// #else
	m_keyframePropList.AddKeyframeProp(&m_spawnRateOverTimeKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_spawnRateOverDistKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_particleLifeKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_playbackRateScalarKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_speedScalarKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_sizeScalarKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_accnScalarKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_dampeningScalarKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_matrixWeightScalarKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_inheritVelocityKFP, NULL);
//#endif

	SetKeyframeDefns();

#if RMPTFX_EDITOR
	CreateUiObjects();
#endif

	// flags
	m_isOneShot = false;
}

ptxEmitterRule::~ptxEmitterRule()
{
	delete[] (char*)m_pUIData;
	RemoveDependents(false);
}

ptxEmitterRule::ptxEmitterRule(datResource& rsc)
: pgBaseRefCounted(rsc)
, m_keyframePropList(rsc)
, m_name(rsc)
, m_creationDomainObj(rsc)
, m_targetDomainObj(rsc)
, m_attractorDomainObj(rsc)
, m_spawnRateOverTimeKFP(rsc)
, m_spawnRateOverDistKFP(rsc)
, m_particleLifeKFP(rsc)
, m_playbackRateScalarKFP(rsc)
, m_speedScalarKFP(rsc)
, m_sizeScalarKFP(rsc)
, m_accnScalarKFP(rsc)
, m_dampeningScalarKFP(rsc)
, m_matrixWeightScalarKFP(rsc)
, m_inheritVelocityKFP(rsc)
{
	m_pLastEvoList_NOTUSED = (void*)0xFFFFFFFF;											// not NULL - to make sure we get a "cache miss" the first time it runs

	SetKeyframeDefns();

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxEmitterRule_num++;
#endif
}

#if __DECLARESTRUCT
void ptxEmitterRule::DeclareStruct(datTypeStruct& s)
{
	pgBaseRefCounted::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxEmitterRule, pgBaseRefCounted)
		SSTRUCT_FIELD(ptxEmitterRule, m_fileVersion)
		SSTRUCT_FIELD(ptxEmitterRule, m_name)
		SSTRUCT_IGNORE(ptxEmitterRule, m_pLastEvoList_NOTUSED)
		SSTRUCT_FIELD_VP(ptxEmitterRule, m_pUIData)
		SSTRUCT_FIELD(ptxEmitterRule, m_creationDomainObj)
		SSTRUCT_FIELD(ptxEmitterRule, m_targetDomainObj)
		SSTRUCT_FIELD(ptxEmitterRule, m_attractorDomainObj)
		SSTRUCT_IGNORE(ptxEmitterRule, m_pad)
		SSTRUCT_FIELD(ptxEmitterRule, m_spawnRateOverTimeKFP)
		SSTRUCT_FIELD(ptxEmitterRule, m_spawnRateOverDistKFP)
		SSTRUCT_FIELD(ptxEmitterRule, m_particleLifeKFP)
		SSTRUCT_FIELD(ptxEmitterRule, m_playbackRateScalarKFP)
		SSTRUCT_FIELD(ptxEmitterRule, m_speedScalarKFP)
		SSTRUCT_FIELD(ptxEmitterRule, m_sizeScalarKFP)
		SSTRUCT_FIELD(ptxEmitterRule, m_accnScalarKFP)
		SSTRUCT_FIELD(ptxEmitterRule, m_dampeningScalarKFP)
		SSTRUCT_FIELD(ptxEmitterRule, m_matrixWeightScalarKFP)
		SSTRUCT_FIELD(ptxEmitterRule, m_inheritVelocityKFP)
		SSTRUCT_FIELD(ptxEmitterRule, m_keyframePropList)
		SSTRUCT_FIELD(ptxEmitterRule, m_isOneShot)
		SSTRUCT_IGNORE(ptxEmitterRule, m_pad2)
	SSTRUCT_END(ptxEmitterRule)
}
#endif

IMPLEMENT_PLACE(ptxEmitterRule);

void ptxEmitterRule::Init()
{
	// create default domains
	m_creationDomainObj.SetDomain(ptxDomain::CreateDomain(PTXDOMAIN_TYPE_CREATION, PTXDOMAIN_SHAPE_BOX));
	m_targetDomainObj.SetDomain(ptxDomain::CreateDomain(PTXDOMAIN_TYPE_TARGET, PTXDOMAIN_SHAPE_BOX));

	// we don't want to create a default attractor domain (this type is optional)
//	m_attractorDomainObj.SetDomain(ptxDomain::CreateDomain(PTXDOMAIN_TYPE_ATTRACTOR, PTXDOMAIN_SHAPE_ATTRACTOR));
}

void ptxEmitterRule::PrepareEvoData(ptxEvolutionList* pEvoList)
{
	//Keeping this commented to disable the evo list caching. Reason is we call PrepareEvoData on each thread separately.
	//And a single caching pointer is used for all the threads. It won't be worth it to actually expand this into a per thread
	//storage because we will hardly satisfy the condition (because the same rule can be updated by the same instance on any thread)
	/*
	if (m_pLastEvoList==pEvoList
#if RMPTFX_EDITOR
		&& !RMPTFXMGR.GetInterface().IsConnected()
#endif
		)
	{
		return;
	}
	*/

	m_keyframePropList.PrepareEvoData(pEvoList);
	m_creationDomainObj.PrepareEvoData(pEvoList);
	m_targetDomainObj.PrepareEvoData(pEvoList);
	m_attractorDomainObj.PrepareEvoData(pEvoList);

	//m_pLastEvoList = pEvoList;
}

void ptxEmitterRule::SetKeyframeDefns()
{
	m_spawnRateOverTimeKFP.GetKeyframe().SetDefn(&sm_spawnRateOverTimeDefn);
	m_spawnRateOverDistKFP.GetKeyframe().SetDefn(&sm_spawnRateOverDistDefn);
	m_particleLifeKFP.GetKeyframe().SetDefn(&sm_particleLifeDefn);
	m_playbackRateScalarKFP.GetKeyframe().SetDefn(&sm_playbackRateScalarDefn);
	m_speedScalarKFP.GetKeyframe().SetDefn(&sm_speedScalarDefn);
	m_sizeScalarKFP.GetKeyframe().SetDefn(&sm_sizeScalarDefn);
	m_accnScalarKFP.GetKeyframe().SetDefn(&sm_accnScalarDefn);
	m_dampeningScalarKFP.GetKeyframe().SetDefn(&sm_dampeningScalarDefn);
	m_matrixWeightScalarKFP.GetKeyframe().SetDefn(&sm_matrixWeightScalarDefn);
	m_inheritVelocityKFP.GetKeyframe().SetDefn(&sm_inheritVelocityDefn);
}

void ptxEmitterRule::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	m_creationDomainObj.GetDomain()->SetEvolvedKeyframeDefns(pEvolutionList);
	m_targetDomainObj.GetDomain()->SetEvolvedKeyframeDefns(pEvolutionList);
	if (m_attractorDomainObj.GetDomain())
	{
		m_attractorDomainObj.GetDomain()->SetEvolvedKeyframeDefns(pEvolutionList);
	}

	pEvolutionList->SetKeyframeDefn(&m_spawnRateOverTimeKFP, &sm_spawnRateOverTimeDefn);
	pEvolutionList->SetKeyframeDefn(&m_spawnRateOverDistKFP, &sm_spawnRateOverDistDefn);
	pEvolutionList->SetKeyframeDefn(&m_particleLifeKFP, &sm_particleLifeDefn);
	pEvolutionList->SetKeyframeDefn(&m_playbackRateScalarKFP, &sm_playbackRateScalarDefn);
	pEvolutionList->SetKeyframeDefn(&m_speedScalarKFP, &sm_speedScalarDefn);
	pEvolutionList->SetKeyframeDefn(&m_sizeScalarKFP, &sm_sizeScalarDefn);
	pEvolutionList->SetKeyframeDefn(&m_accnScalarKFP, &sm_accnScalarDefn);
	pEvolutionList->SetKeyframeDefn(&m_dampeningScalarKFP, &sm_dampeningScalarDefn);
	pEvolutionList->SetKeyframeDefn(&m_matrixWeightScalarKFP, &sm_matrixWeightScalarDefn);
	pEvolutionList->SetKeyframeDefn(&m_inheritVelocityKFP, &sm_inheritVelocityDefn);
}

void ptxEmitterRule::SetName(const char* pNewName)
{
	if (m_name==NULL || ((strcasecmp(m_name.c_str(), pNewName)!= 0)))
	{
		m_name = pNewName;
	}
}

#if RMPTFX_EDITOR
void ptxEmitterRule::RescaleZoomableComponents(float scalar, ptxEvolutionList* pEvolutionList)
{	
	ScalarV vScalar(scalar);

	// scale acceleration scalar and its evolutions
	m_accnScalarKFP.GetKeyframe().ScaleEntries(vScalar);

	ptxEvolvedKeyframeProp* pEvolvedKeframeProp = pEvolutionList->GetEvolvedKeyframePropById(m_accnScalarKFP.GetPropertyId());
	if (pEvolvedKeframeProp)
	{
		pEvolvedKeframeProp->ScaleEvolvedKeyframes(vScalar);
	}

	// scale size scalar and its evolutions
	m_sizeScalarKFP.GetKeyframe().ScaleEntries(vScalar);

	pEvolvedKeframeProp = pEvolutionList->GetEvolvedKeyframePropById(m_sizeScalarKFP.GetPropertyId());
	if (pEvolvedKeframeProp)
	{
		pEvolvedKeframeProp->ScaleEvolvedKeyframes(vScalar);
	}
	
	// scale creation domain
	if (m_creationDomainObj.GetDomain())
	{
		m_creationDomainObj.GetDomain()->RescaleZoomableComponents(scalar, pEvolutionList);
	}

	// scale target domain
	if (m_targetDomainObj.GetDomain())
	{
		m_targetDomainObj.GetDomain()->RescaleZoomableComponents(scalar, pEvolutionList);
	}

	// scale attractor domain
	if (m_attractorDomainObj.GetDomain())
	{
		m_attractorDomainObj.GetDomain()->RescaleZoomableComponents(scalar, pEvolutionList);
	}

	// send the updated emitter rule to the editor
#if RMPTFX_EDITOR
	RMPTFXMGR.GetInterface().SendEmitterRule(m_name);
#endif
}
#endif

#if RMPTFX_EDITOR
ptxKeyframeProp* ptxEmitterRule::GetKeyframeProp(u32 keyframePropId)
{
	ptxKeyframeProp* pKeyframeProp = m_keyframePropList.GetKeyframePropByPropId(keyframePropId);
	if (pKeyframeProp==NULL)
	{
		pKeyframeProp = m_creationDomainObj.GetKeyframeProp(keyframePropId);
		if (pKeyframeProp==NULL)
		{
			pKeyframeProp = m_targetDomainObj.GetKeyframeProp(keyframePropId);
			if (pKeyframeProp==NULL)
			{
				pKeyframeProp = m_attractorDomainObj.GetKeyframeProp(keyframePropId);
			}
		}
	}
	
	return pKeyframeProp;
}
#endif

#if RMPTFX_EDITOR
void ptxEmitterRule::UpdateUIData(ptxEvolutionList* pEvoList, const ptxEffectRule* pEffectRule, s32 eventIdx)
{
	if (pEvoList==NULL)
	{
		return;
	}

	pEvoList->UpdateUIData(&m_keyframePropList, pEffectRule, eventIdx);
	pEvoList->UpdateUIData(&m_creationDomainObj.GetDomain()->GetKeyframePropList(), pEffectRule, eventIdx);
	pEvoList->UpdateUIData(&m_targetDomainObj.GetDomain()->GetKeyframePropList(), pEffectRule, eventIdx);
	if (m_attractorDomainObj.GetDomain())
	{
		pEvoList->UpdateUIData(&m_attractorDomainObj.GetDomain()->GetKeyframePropList(), pEffectRule, eventIdx);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEmitterRule::ToggleAttractorDomain()
{
	if (m_attractorDomainObj.GetDomain())
	{
		m_attractorDomainObj.SetDomain(NULL);
	}
	else
	{
		m_attractorDomainObj.SetDomain(ptxDomain::CreateDomain(PTXDOMAIN_TYPE_ATTRACTOR, PTXDOMAIN_SHAPE_ATTRACTOR));
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEmitterRule::SendToEditor(ptxByteBuff& buff)
{
	buff.Write_float(PTXEMITTERRULE_FILE_VERSION);   
	buff.Write_const_char(GetName());  

	buff.Write_bool(m_isOneShot);

	m_creationDomainObj.SendToEditor(buff, this);
	m_targetDomainObj.SendToEditor(buff, this);
	m_attractorDomainObj.SendToEditor(buff, this);

	m_keyframePropList.SendToEditor(buff, GetName(), PTXRULETYPE_EMITTER);

	m_pUIData->SendDebugDataToEditor(buff);
}
#endif

#if RMPTFX_EDITOR
void ptxEmitterRule::ReceiveFromEditor(ptxByteBuff& buff)
{
	// skip version and name
	buff.Read_float();
	buff.Read_const_char();

	m_isOneShot = buff.Read_bool();

	m_creationDomainObj.ReceiveFromEditor(buff);
	m_targetDomainObj.ReceiveFromEditor(buff);
	m_attractorDomainObj.ReceiveFromEditor(buff);

	m_pUIData->ReceiveDebugDataFromEditor(buff);
}
#endif

#if __RESOURCECOMPILER || RMPTFX_EDITOR
void ptxEmitterRule::Save()
{
#if RMPTFX_EDITOR
	RemoveDefaultKeyframes();
#endif

	m_fileVersion = PTXEMITTERRULE_FILE_VERSION;

	ASSET.PushFolder(RMPTFXMGR.GetAssetPath());
	ASSET.PushFolder("emitrules");

#if !__FINAL
	bool folderOverrideUsed = false;
	const char* pPtfxAssetOverride = NULL;
	PARAM_ptfxassetoverride.Get(pPtfxAssetOverride);
	if (pPtfxAssetOverride)
	{
		ASSET.PushFolder(pPtfxAssetOverride);
		if (ASSET.Exists(GetName(), "emitrule"))
		{
			folderOverrideUsed = true;
		}
		else
		{
			ASSET.PopFolder();
		}
	}
#endif

	ASSET.CreateLeadingPath(GetName());
	bool res = PARSER.SaveObject(GetName(), "emitrule", this);

#if !__FINAL
	if (folderOverrideUsed)
	{
		ASSET.PopFolder();
	}
#endif

	ASSET.PopFolder();
	ASSET.PopFolder();

	if (res==false)
	{
		char msg[512];
		char path[512];
		formatf(path, sizeof(path), "%s\\%s", RMPTFXMGR.GetAssetPath(), "emitrules");
		formatf(msg, sizeof(msg), "Unable to save file: %s\\%s.emitrule, check permissions.", path,GetName());
		bkMessageBox("RMPTFX", msg, bkMsgOk, bkMsgInfo);
	}
}
#endif

#if RMPTFX_XML_LOADING
ptxEmitterRule * ptxEmitterRule::CreateFromFile(const char* pFilename)
{
	ptxEmitterRule* pEmitterRule = NULL;

	ASSET.PushFolder(RMPTFXMGR.GetAssetPath());
	ASSET.PushFolder("emitrules");

	PARSER.LoadObjectPtr(pFilename,"emitrule", pEmitterRule);

	ASSET.PopFolder();
	ASSET.PopFolder();

	if (pEmitterRule)
	{
		pEmitterRule->RepairAndUpgradeData(pFilename);
		pEmitterRule->ResolveReferences();
#if RMPTFX_EDITOR
		pEmitterRule->CreateUiObjects();
#endif
	}

	return pEmitterRule;
}
#endif

#if RMPTFX_EDITOR
void ptxEmitterRule::CreateUiObjects()
{
	if (m_pUIData==NULL)
	{
		m_pUIData = rage_new ptxEmitterRuleUIData;
	}
}
#endif

#if RMPTFX_XML_LOADING
void ptxEmitterRule::RepairAndUpgradeData(const char* pFilename)
{
	SetName(pFilename);

#if __DEV	
	if (m_fileVersion<PTXEMITTERRULE_FILE_VERSION)
	{
		ptxWarningf("(%s.emitrule) is outdated, please resave via the particle editor.", pFilename);
	}
#endif
}
#endif


#if RMPTFX_BANK || RMPTFX_EDITOR
void ptxEmitterRule::RemoveDefaultKeyframes()
{
	ptxDomain* pCreationDomain = GetCreationDomain();
	if (pCreationDomain)
	{
		pCreationDomain->RemoveDefaultKeyframes();
	}

	ptxDomain* pTargetDomain = GetTargetDomain();
	if (pTargetDomain)
	{
		pTargetDomain->RemoveDefaultKeyframes();
	}

	ptxDomain* pAttractorDomain = GetAttractorDomain();
	if (pAttractorDomain)
	{
		pAttractorDomain->RemoveDefaultKeyframes();
	}

	for (int i=0; i<m_keyframePropList.GetNumKeyframeProps(); i++)
	{
		ptxKeyframeProp* pKeyframeProp = m_keyframePropList.GetKeyframePropByIndex(i);
		if (pKeyframeProp)
		{
			ptxKeyframe& keyframe = pKeyframeProp->GetKeyframe();
			if (keyframe.IsDefault())
			{
				keyframe.DeleteEntry(0);
			}
		}
	}
}
#endif


#if RMPTFX_BANK
void ptxEmitterRule::GetKeyframeEntryInfo(int& num, int& mem, int& max, int& def)
{
	ptxDomain* pCreationDomain = GetCreationDomain();
	if (pCreationDomain)
	{
		pCreationDomain->GetKeyframeEntryInfo(num, mem, max, def);
	}

	ptxDomain* pTargetDomain = GetTargetDomain();
	if (pTargetDomain)
	{
		pTargetDomain->GetKeyframeEntryInfo(num, mem, max, def);
	}

	ptxDomain* pAttractorDomain = GetAttractorDomain();
	if (pAttractorDomain)
	{
		pAttractorDomain->GetKeyframeEntryInfo(num, mem, max, def);
	}

	for (int i=0; i<m_keyframePropList.GetNumKeyframeProps(); i++)
	{
		ptxKeyframeProp* pKeyframeProp = m_keyframePropList.GetKeyframePropByIndex(i);
		if (pKeyframeProp)
		{
			ptxKeyframe& keyframe = pKeyframeProp->GetKeyframe();

			int currNum = keyframe.GetNumEntries();

			num += currNum;
			mem += keyframe.GetEntryMem();

			if (currNum>max)
			{
				max = currNum;
			}

			if (keyframe.IsDefault())
			{
				def++;
			}
		}
	}
}
#endif


#if RMPTFX_BANK
void ptxEmitterRule::OutputKeyframeEntryInfo()
{
	ptxDomain* pCreationDomain = GetCreationDomain();
	if (pCreationDomain)
	{
		pCreationDomain->OutputKeyframeEntryInfo();
	}

	ptxDomain* pTargetDomain = GetTargetDomain();
	if (pTargetDomain)
	{
		pTargetDomain->OutputKeyframeEntryInfo();
	}

	ptxDomain* pAttractorDomain = GetAttractorDomain();
	if (pAttractorDomain)
	{
		pAttractorDomain->OutputKeyframeEntryInfo();
	}

	for (int i=0; i<m_keyframePropList.GetNumKeyframeProps(); i++)
	{
		ptxKeyframeProp* pKeyframeProp = m_keyframePropList.GetKeyframePropByIndex(i);
		if (pKeyframeProp)
		{
			ptxKeyframe& keyframe = pKeyframeProp->GetKeyframe();
			ptxKeyframeDefn* pKeyframeDefn = keyframe.GetDefn();
			if (ptxVerifyf(pKeyframeDefn, "keyframe doesn't have a definition"))
			{
				ptxAssertf(pKeyframeProp->GetPropertyId()==pKeyframeDefn->GetPropertyId(), "property ids don't match");
				ptxDebugf1("                  %d - %s (%u)", keyframe.GetNumEntries(), pKeyframeDefn->GetName(), pKeyframeDefn->GetPropertyId());
			}
		}
	}
}
#endif

