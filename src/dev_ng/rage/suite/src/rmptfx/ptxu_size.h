// 
// rmptfx/ptxu_size.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_SIZE_H 
#define PTXU_SIZE_H 


// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"


// namespaces
namespace rage
{


// enumerations
enum ptxu_Size_KeyframeMode
{
	PTXU_SIZE_KEYFRAMEMODE_POINT			= 0,
	PTXU_SIZE_KEYFRAMEMODE_EFFECT,
	PTXU_SIZE_KEYFRAMEMODE_EMITTER,

	PTXU_SIZE_KEYFRAMEMODE_NUM
};


// classes
class ptxu_Size : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_Size();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxu_Size(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_Size);

		// info
		const char* GetName() {return "ptxu_Size";}														// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_SIZE;}													// the ordering of the behaviour (see ptxconfig.h)

		bool NeedsToInit() {return true;}																// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}																// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return false;}													// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return false;}																// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}															// whether we call release when a particle dies

		void InitPoint(ptxBehaviourParams& params);														// called for each newly created particle if NeedsToInit returns true
		void UpdatePoint(ptxBehaviourParams& params);													// called from InitPoint and UpdatePoint to update the particle accordingly 

		void SetKeyframeDefns();																		// sets the keyframe definitions on the keyframe properties
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);									// sets the keyframe definitions on the evolved keyframe properties in the evolution list

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Size";}													// name of the behaviour shown in the editor
		bool IsActive();																				// whether this behaviour is active or not

		void UpdateKeyframeDefns(ptxDrawType drawType, const ptxParticleRule* pParticleRule);			// updates the keyframe definitions on the keyframe properties (when the draw type changes)

		void SendDefnToEditor(ptxByteBuff& buff);														// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);				// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);										// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif



	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data
		ptxKeyframeProp m_whdMinKFP;												// minimum width and height
		ptxKeyframeProp m_whdMaxKFP;												// maximum width and height
		ptxKeyframeProp m_tblrScalarKFP;											// top, bottom, left, right scalar
		ptxKeyframeProp m_tblrVelScalarKFP;											// top, bottom, left, right velocity scalar

		// non keyframe data
		int m_keyframeMode;
		bool m_isProportional;														// uses a single randomness across each size component

		datPadding<11> m_pad;

		// static data
		static ptxKeyframeDefn sm_whdMin2dDefn;
		static ptxKeyframeDefn sm_whdMax2dDefn;
		static ptxKeyframeDefn sm_whdMin3dDefn;
		static ptxKeyframeDefn sm_whdMax3dDefn;
		static ptxKeyframeDefn sm_tblrScalarDefn;
		static ptxKeyframeDefn sm_tblrVelScalarDefn;

};


} // namespace rage


#endif // PTXU_SIZE_H 
