<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_Colour" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<struct name="m_rgbaMinKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_rgbaMaxKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_emissiveIntensityKFP" type="::rage::ptxKeyframeProp"/>
	<int name="m_keyframeMode"/>
	<bool name="m_rgbaMaxEnable"/>
	<bool name="m_rgbaProportional"/>
	<bool name="m_rgbCanTint"/>
</structdef>

</ParserSchema>