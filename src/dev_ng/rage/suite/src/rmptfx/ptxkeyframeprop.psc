<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::ptxKeyframeProp" cond="RMPTFX_XML_LOADING">
	<struct name="m_keyframe" type="rage::ptxKeyframe"/>
	<bool name="m_invertBiasLink"/>
</structdef>

</ParserSchema>