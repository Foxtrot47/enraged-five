<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::ptxEmitterRule" cond="RMPTFX_XML_LOADING">
	<float name="m_fileVersion"/>
	<string name="m_name" type="ConstString"/>
	<bool name="m_isOneShot"/>
	<struct name="m_creationDomainObj" type="rage::ptxDomainObj"/>
	<struct name="m_targetDomainObj" type="rage::ptxDomainObj"/>
	<struct name="m_attractorDomainObj" type="rage::ptxDomainObj"/>
	<struct name="m_spawnRateOverTimeKFP" type="rage::ptxKeyframeProp"/>
	<struct name="m_spawnRateOverDistKFP" type="rage::ptxKeyframeProp"/>
	<struct name="m_particleLifeKFP" type="rage::ptxKeyframeProp"/>
	<struct name="m_playbackRateScalarKFP" type="rage::ptxKeyframeProp"/>
	<struct name="m_speedScalarKFP" type="rage::ptxKeyframeProp"/>
	<struct name="m_sizeScalarKFP" type="rage::ptxKeyframeProp"/>
	<struct name="m_accnScalarKFP" type="rage::ptxKeyframeProp"/>
	<struct name="m_dampeningScalarKFP" type="rage::ptxKeyframeProp"/>
	<struct name="m_matrixWeightScalarKFP" type="rage::ptxKeyframeProp"/>
	<struct name="m_inheritVelocityKFP" type="rage::ptxKeyframeProp"/>
</structdef>

</ParserSchema>