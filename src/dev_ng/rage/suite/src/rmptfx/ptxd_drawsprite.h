// 
// rmptfx/ptxd_drawsprite.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXD_DRAWSPRITE_H 
#define PTXD_DRAWSPRITE_H 


// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"


// forward declarations
namespace rage
{
	struct sysTaskParameters;
}


// namespaces
namespace rage
{


// enumerations
enum ptxd_Sprite_AlignmentMode
{
	PTXD_SPRITE_ALIGNMENTMODE_CAMERA		= 0,
	PTXD_SPRITE_ALIGNMENTMODE_VELOCITY,
	PTXD_SPRITE_ALIGNMENTMODE_WORLD,
	PTXD_SPRITE_ALIGNMENTMODE_EFFECT,
	PTXD_SPRITE_ALIGNMENTMODE_EMITTER,

	PTXD_SPRITE_ALIGNMENTMODE_NUM
};

// classes
class ptxd_Sprite : public ptxBehaviour
{
	friend class ptxParticleRule;
	friend class ptxDrawInterface;


	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxd_Sprite();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxd_Sprite(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxd_Sprite);

		// info
		const char* GetName() {return "ptxd_Sprite";}											// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_DRAWSPRITE;}									// the ordering of the behaviour (see ptxconfig.h)
		ptxDrawType GetTypeFilter() {return PTXPARTICLERULE_DRAWTYPE_SPRITE;}					// filter for deciding which type of particle this behaviour should be active

		bool NeedsToInit() {return false;}														// whether we call init when a particle is created
		bool NeedsToUpdate() {return false;}													// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return false;}											// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return true;}														// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}													// whether we call release when a particle dies

		__forceinline void DrawPoints(ptxEffectInst* RESTRICT pEffectInst,						// called for each active particle to draw them
			ptxEmitterInst* RESTRICT pEmitterInst,
			ptxParticleRule* RESTRICT pParticleRule,
			ptxEvolutionList* RESTRICT pEvoList
			PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface));

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Draw (Sprite)";}									// name of the behaviour shown in the editor
		bool IsActive() {return true;}															// whether this behaviour is active or not
																	
		void SendDefnToEditor(ptxByteBuff& buff);												// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);		// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);								// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif

	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data

		// non keyframe data
		Vec3V m_alignAxis;

		int m_alignmentMode;

		float m_flipChanceU;
		float m_flipChanceV;

		float m_nearClipDist;
		float m_farClipDist;

		float m_projectionDepth;
		float m_shadowCastIntensity;

		bool m_isScreenSpace;
		bool m_isHiRes;
		bool m_nearClip;
		bool m_farClip;
		bool m_uvClip;					// whether to clip the uvs when near or far clipping (or just leave them stretched)

		bool m_disableDraw;

		datPadding<14> m_pad;

};


} // namespace rage


#endif // PTXD_DRAWSPRITE_H 
