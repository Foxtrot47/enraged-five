// 
// rmptfx/ptxbiaslink.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 


// includes (us)
#include "rmptfx/ptxbiaslink.h"
#include "rmptfx/ptxbiaslink_parser.h"

// includes (rmptfx)
#include "rmptfx/ptxbytebuff.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxkeyframeprop.h"

// includes (rage)
#include "atl/array_struct.h"
#include "data/safestruct.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
using namespace rage;


// code
ptxBiasLink::ptxBiasLink() 
{
	m_randIndex = 0; 
	m_name[0] = 0;
}

ptxBiasLink::ptxBiasLink(const char* pName) 
{
	m_randIndex = 0;	
	formatf(m_name, sizeof(m_name), "%s", pName);
}

ptxBiasLink::ptxBiasLink(datResource& rsc)
: m_keyframePropIds(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxBiasLink_num++;
#endif
}

#if __DECLARESTRUCT
void ptxBiasLink::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxBiasLink)
		SSTRUCT_CONTAINED_ARRAY(ptxBiasLink, m_name)
		SSTRUCT_FIELD(ptxBiasLink, m_keyframePropIds)
		SSTRUCT_FIELD(ptxBiasLink, m_randIndex)
		SSTRUCT_IGNORE(ptxBiasLink, m_pad)
	SSTRUCT_END(ptxBiasLink)
}
#endif

IMPLEMENT_PLACE(ptxBiasLink);

void ptxBiasLink::SetLink(ptxKeyframeProp& keyframeProp)
{
	for (int i=0; i<m_keyframePropIds.GetCount(); i++)
	{
		if (m_keyframePropIds[i]==(int)keyframeProp.GetPropertyId())
		{
			keyframeProp.SetRandIndex(m_randIndex);
			return;
		}
	}
}

#if RMPTFX_EDITOR
void ptxBiasLink::SendToEditor(ptxByteBuff& buff)
{
	buff.Write_const_char(m_name);

	buff.Write_u32(m_keyframePropIds.GetCount());
	for (int i=0; i<m_keyframePropIds.GetCount(); i++)
	{
		buff.Write_u32(m_keyframePropIds[i]);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxBiasLink::ReceiveFromEditor(const ptxByteBuff& buff)
{
	formatf(m_name, sizeof(m_name), "%s", buff.Read_const_char());

	m_keyframePropIds.Reset();
	int numKeyframeProps = buff.Read_s32();
	for(int i=0;i<numKeyframeProps;i++)
	{
		Add(buff.Read_s32());
	}
}
#endif
	
