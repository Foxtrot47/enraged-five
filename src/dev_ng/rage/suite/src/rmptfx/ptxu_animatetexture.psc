<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_AnimateTexture" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<struct name="m_animRateKFP" type="::rage::ptxKeyframeProp"/>
	<int name="m_keyframeMode"/>
	<int name="m_lastFrameId"/>
	<int name="m_loopMode"/>
	<bool name="m_isRandomised"/>
	<bool name="m_isScaledOverParticleLife"/>
	<bool name="m_isHeldOnLastFrame"/>
	<bool name="m_doFrameBlending"/>
</structdef>

</ParserSchema>