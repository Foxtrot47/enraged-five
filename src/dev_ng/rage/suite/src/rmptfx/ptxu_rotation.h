// 
// rmptfx/ptxu_rotation.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_ROTATION_H 
#define PTXU_ROTATION_H 


// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"


// namespaces
namespace rage
{


// enumerations
enum ptxu_Rotation_Mode
{
	PTXU_ROTATION_ROTATEMODE_KEYFRAME			= 0,
	PTXU_ROTATION_ROTATEMODE_EFFECT,
	PTXU_ROTATION_ROTATEMODE_EMITTER,

	PTXU_ROTATION_ROTATEMODE_NUM
};


// classes
class ptxu_Rotation : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_Rotation();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxu_Rotation(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_Rotation);

		// info
		const char* GetName() {return "ptxu_Rotation";}													// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_ROTATION;}												// the ordering of the behaviour (see ptxconfig.h)

		bool NeedsToInit() {return true;}																// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}																// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return false;}													// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return false;}																// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}															// whether we call release when a particle dies

		void InitPoint(ptxBehaviourParams& params);														// called for each newly created particle if NeedsToInit returns true
		void UpdatePoint(ptxBehaviourParams& params);													// called from InitPoint and UpdatePoint to update the particle accordingly 

		void SetKeyframeDefns();																		// sets the keyframe definitions on the keyframe properties
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);									// sets the keyframe definitions on the evolved keyframe properties in the evolution list

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Rotation";}												// name of the behaviour shown in the editor
		bool IsActive();																				// whether this behaviour is active or not

		void UpdateKeyframeDefns(ptxDrawType drawType, const ptxParticleRule* pParticleRule);			// updates the keyframe definitions on the keyframe properties (when the draw type changes)

		void SendDefnToEditor(ptxByteBuff& buff);														// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);				// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);										// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif



	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data
		ptxKeyframeProp m_initialAngleMinKFP;
		ptxKeyframeProp m_initialAngleMaxKFP;
		ptxKeyframeProp m_angleMinKFP;
		ptxKeyframeProp m_angleMaxKFP;

		// non keyframe data
		int m_initRotationMode;
		int m_updateRotationMode;

		bool m_accumulateAngle;
		bool m_rotateAngleAxes;
		bool m_rotateInitAngleAxes;

		datPadding<1> m_pad;

		float m_speedFadeThresh;

		// static data
		static ptxKeyframeDefn sm_initialAngleMin2dDefn;
		static ptxKeyframeDefn sm_initialAngleMax2dDefn;
		static ptxKeyframeDefn sm_initialAngleMin3dDefn;
		static ptxKeyframeDefn sm_initialAngleMax3dDefn;
		static ptxKeyframeDefn sm_angleMin2dDefn;
		static ptxKeyframeDefn sm_angleMax2dDefn;
		static ptxKeyframeDefn sm_angleMin3dDefn;
		static ptxKeyframeDefn sm_angleMax3dDefn;

};


} // namespace rage


#endif // PTXU_ROTATION_H

