// 
// rmptfx/ptxdebug.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXDEBUG_H
#define RMPTFX_PTXDEBUG_H


// includes (rmptfx)
#include "rmptfx/ptxconfig.h"

// includes (rage)
#include "profile/profiler.h"


// namespaces
namespace rage 
{


// defines 
#define PTXDEBUG_MAX_DEBUG_TRAIL_POINTS		(4)


// forward declarations
class ptxFxList;


// profile stats - counters
EXT_PF_COUNTER(CounterPointsActive);
EXT_PF_COUNTER(CounterEffectInsts);
EXT_PF_COUNTER(CounterEffectInstsActive);
EXT_PF_COUNTER(CounterEffectInstsCulled);
EXT_PF_COUNTER(CounterEmitterInstsTotal);
EXT_PF_COUNTER(CounterEmitterInstsWontUpdate);
EXT_PF_COUNTER(CounterEmitterInstsEarlyWait);
EXT_PF_COUNTER(CounterEmitterInstsLateWait);


// profile stats - timers
EXT_PF_TIMER(TimerUpdate);
EXT_PF_TIMER(TimerUpdateSetup);
EXT_PF_TIMER(TimerUpdateSetupSort);
EXT_PF_TIMER(TimerUpdateParallel);
EXT_PF_TIMER(TimerUpdateFinalize);
EXT_PF_TIMER(TimerUpdateParallel_WaitOnChildTasks);
EXT_PF_TIMER(TimerUpdate_WaitOnCallbackEffects);
EXT_PF_TIMER(TimerExt_WaitForUpdateToFinish);
EXT_PF_TIMER(TimerUpdateLocal_ParallelAndFinalize);
EXT_PF_TIMER(TimerUpdateCallback_ParallelAndFinalize);
EXT_PF_TIMER(TimerUpdateLocal_WaitOnEffectUpdateComplete);
EXT_PF_TIMER(TimerDraw);


#if RMPTFX_BANK

// structures
struct ptxDebugTrailPoint
{
	Vec3V vPosition;
	Vec4V vColour;
	ScalarV vSize;
	ScalarV vLifeRatio;
};

struct ptxDebugFxListInfo
{
	void Clear();
	void Output(const ptxFxList* pFxList);

	int ptxFxList_num;
	int ptxEffectRule_num;
	int ptxTimeLine_num;
	int ptxEvent_num;
	int ptxEventEmitter_num;
	int ptxEvolutionList_num;
	int ptxEvolution_num;
	int ptxEvolvedKeyframeProp_num;
	int ptxEvolvedKeyframe_num;
	int ptxEffectSpawner_num;
	int ptxSpawnedEffectScalars_num;
	int ptxEmitterRule_num;
	int ptxDomainObj_num;
	int ptxDomain_num;
	int ptxDomainBox_num;
	int ptxDomainSphere_num;
	int ptxDomainCylinder_num;
	int ptxDomainAttractor_num;
	int ptxParticleRule_num;
	int ptxBehaviour_num;
	int ptxBiasLink_num;
	int ptxRenderState_num;
	int ptxTechniqueDesc_num;
	int ptxShaderInst_num;
	int ptxShaderVar_num;
	int ptxShaderVarVector_num;
	int ptxShaderVarKeyframe_num;
	int ptxShaderVarTexture_num;
	int ptxDrawable_num;
	int ptxDrawableEntry_num;
	int ptxKeyframePropList_num;
	int ptxKeyframeProp_num;
	int ptxKeyframe_num;
#if PTXKEYFRAME_VERSION==0 || PTXKEYFRAME_VERSION==1
	int ptxKeyframeEntry_num;
#endif
	int ptxd_Model_num;
	int ptxd_Sprite_num;
	int ptxd_Trail_num;
	int ptxu_Acceleration_num;
	int ptxu_Age_num;
	int ptxu_AnimateTexture_num;
	int ptxu_Attractor_num;
	int ptxu_Collision_num;
	int ptxu_Colour_num;
	int ptxu_Dampening_num;
	int ptxu_MatrixWeight_num;
	int ptxu_Noise_num;
	int ptxu_Pause_num;
	int ptxu_Rotation_num;
	int ptxu_Size_num;
	int ptxu_Velocity_num;
	int ptxu_Wind_num;
};

// classes
class ptxDebug
{
	friend class ptxDrawInterface;
	friend class ptxEffectInst;
	friend class ptxEmitterInst;
	friend class ptxInterface;
	friend class ptxManager;


	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		static void Init(bkBank* pVfxBank = NULL, bkGroup* pParticleGroup = NULL);
		static void Update();

		static void ToggleQuickDebug();


	private: //////////////////////////

		static void ResetProfilingData();
		static void InitWidgets(bkBank* pVfxBank = NULL, bkGroup* pParticleGroup = NULL);

		// global
		static void ToggleDisableInitPointMost();

		// asset reporting - overview

		// asset reporting - per fx list
		static void ReportFxList();

		// asset reporting - per effect rule
		static void ReportEffectRule();
		static void ReportEffectRuleKeyframeEntries();

		// asset reporting - per emitter rule
		static void ReportEmitterRule();
		static void ReportEmitterRuleKeyframeEntries();

		// asset reporting - per particle rule
		static void ReportParticleRule();
		static void ReportParticleRuleKeyframeEntries();

		// asset reporting - per texture
		static void ReportTexture();

		// asset reporting - per model
		static void ReportModel();


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////
	public: ///////////////////////////

		// widget data
		static bkBank* sm_pBank;

		// info
		static float sm_version;

		// fx lists
		static bool sm_drawFxlistReferences;

		// global
		static bool sm_enableDebugDrawing;

		static bool sm_enableExpensiveOneShotSubFrameCalcs;

		static bool sm_disableInitPointAccn;
		static bool sm_disableInitPointAge;
		static bool sm_disableInitPointAnimTex;
		static bool sm_disableInitPointAttractor;
		static bool sm_disableInitPointColn;
		static bool sm_disableInitPointColour;
		static bool sm_disableInitPointDampening;
		static bool sm_disableInitPointMtxWeight;
		static bool sm_disableInitPointNoise;
		static bool sm_disableInitPointPause;
		static bool sm_disableInitPointRotation;
		static bool sm_disableInitPointSize;
		static bool sm_disableInitPointVelocity;
		static bool sm_disableInitPointWind;
		static bool sm_disableInitPointDecal;
		static bool sm_disableInitPointDecalPool;
		static bool sm_disableInitPointDecalTrail;
		static bool sm_disableInitPointLight;
		static bool sm_disableInitPointLiquid;
		static bool sm_disableInitPointRiver;
		static bool sm_disableInitPointZCull;
		static float sm_overrideHighQualityEvo;

		static bool sm_preUpdateTimeLimitActive;
		static float sm_preUpdateTimeLimit;
		static bool sm_performParallelFinalize;
		static bool sm_disableShadowCullStateCalcs;

		// effect instances
		static char sm_effectInstNameFilter[64];
		static bool sm_drawEffectInstInfo;
		static bool sm_drawEffectInstNames;
		static bool sm_drawEffectInstAddress;
		static bool sm_drawEffectInstFxList;
		static bool sm_drawEffectInstLifeRatios;
		static bool sm_drawEffectInstNumPoints;
		static bool sm_drawEffectInstEvolutions;
		static bool sm_drawEffectInstDistToCam;
		static bool sm_drawEffectInstMaxDuration;
		static bool sm_drawEffectInstCameraBias;
		static bool sm_drawEffectInstColnPolys;
		static bool sm_drawEffectInstPositionTrace;
		static bool sm_drawEffectInstCallbacksHighlighted;
		static bool sm_drawEffectInstQualityHighlighted;
		static bool sm_outputEffectInstPlayStateDebug;
		static bool sm_outputEffectInstUpdateProfile;

		// emitter instances
		static char sm_emitterInstNameFilter[64];
		static int sm_emitterInstEventIdFilter;
		static bool sm_drawEmitterInstBoundBoxes;
		static bool sm_drawEmitterInstCreationDomain;
		static bool sm_drawEmitterInstTargetDomain;
		static bool sm_drawEmitterInstAttractorDomain;
		static bool sm_outputEmitterInstDrawStateDebug;
		static bool sm_outputEffectInstSpawningProfile;

		// trails
		static bool sm_drawTrails;
		static bool sm_drawTrailsWireframe;
		static bool sm_simpleTrails;
		static int sm_numDebugTrailPoints;
		static ptxDebugTrailPoint sm_debugTrailPoints[PTXDEBUG_MAX_DEBUG_TRAIL_POINTS];
		static bool sm_debugTrailTangentColours;
		static int sm_debugTrailTangentSteps;

		// points
		static float sm_pointAlphaCullThreshold;
		static bool sm_drawPoints;
		static bool sm_drawPointAlphas;
		static bool sm_drawPointCollisions;
		static int sm_numPointCollisionFrames;

		// misc
		static float sm_debugAxisScale;
		static bool sm_disableInvertedAxes;
		static bool sm_toggleQuickDebug;

#if RMPTFX_EDITOR
		//RMPTFX editor interface
		static bool sm_rmptfxListen;
#endif

		// profiling
		static int sm_profilingFrameCount;
		static float sm_accumKeyframeQueryTimeMs;
		static int sm_accumKeyframeQueryCount;
		static float sm_averageKeyframeQueryTimeMs;
		static int sm_averageKeyframeQueryCount;

		// fxlist info
		static ptxDebugFxListInfo sm_debugFxListInfo;

		// asset reporting - overview

		// asset reporting - per fx list
		static char sm_reportFxListName[64];

		// asset reporting - per effect rule
		static char sm_reportEffectRuleName[64];

		// asset reporting - per emitter rule
		static char sm_reportEmitterRuleName[64];

		// asset reporting - per particle rule
		static char sm_reportParticleRuleName[64];

		// asset reporting - per texture
		static char sm_reportTextureName[64];

		// asset reporting - per model
		static char sm_reportModelName[64];

		// shader debug
		static bool sm_bEnableShaderDebugOverrideAlpha;
};


#endif // RMPTFX_BANK


} // namespace rage


#endif // RMPTFX_PTXDEBUG_H

