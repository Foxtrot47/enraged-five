// 
// rmptfx/ptxinterface.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXINTERFACE_H
#define RMPTFX_PTXINTERFACE_H



// includes (rmptfx)
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxkeyframe.h"
#include "rmptfx/ptxtcpcomm.h"

// includes (rage)
#include "paging/ref.h"
#include "parser/manager.h"
#include "system/alloca.h"


// editor only file
#if RMPTFX_EDITOR


// namespaces
namespace rage {


// forward declarations
class ptxEffectRule;
class ptxEmitterRule;


// enumerations
enum ptxMessageId
{
	MSGID_NOTHING_TO_PROCESS						= 0x00000000,

	// fx lists
	MSGID_RECEIVE_LOADFXLIST						= 0x00000010,
	MSGID_RECEIVE_SAVEFXLIST						= 0x00000011,

	// rule lists
	MSGID_SEND_EFFECTRULELIST						= 0x00000020,
	MSGID_SEND_EMITTERRULELIST						= 0x00000021,
	MSGID_SEND_PARTICLERULELIST						= 0x00000022,

	// effect rules
	MSGID_SEND_EFFECTRULE							= 0x00000030,
	MSGID_RECEIVE_EFFECTRULE						= 0x00000031,
	MSGID_RECEIVE_LOADEFFECTRULE					= 0x00000032,
	MSGID_RECEIVE_LOADEFFECTRULES					= 0x00000033,
	MSGID_RECEIVE_CREATEEFFECTRULE					= 0x00000034,
	MSGID_RECEIVE_REMOVEEFFECTRULE					= 0x00000035,
	MSGID_RECEIVE_RENAMEEFFECTRULE					= 0x00000036,

	// emitter rules
	MSGID_SEND_EMITTERRULE							= 0x00000040,
	MSGID_RECEIVE_EMITTERRULE						= 0x00000041,
	MSGID_RECEIVE_LOADEMITTERRULE					= 0x00000042,
	MSGID_RECEIVE_CREATEEMITTERRULE					= 0x00000043,
	MSGID_RECEIVE_REMOVEEMITTERRULE					= 0x00000044, 
	MSGID_RECEIVE_RENAMEEMITTERRULE					= 0x00000045,

	// particle rules
	MSGID_SEND_PARTICLERULE							= 0x00000050,
	MSGID_RECEIVE_PARTICLERULE						= 0x00000051,
	MSGID_RECEIVE_LOADPARTICLERULE					= 0x00000052,
	MSGID_RECEIVE_CREATEPARTICLERULE				= 0x00000053,
	MSGID_RECEIVE_REMOVEPARTICLERULE				= 0x00000054,
	MSGID_RECEIVE_RENAMEPARTICLERULE				= 0x00000055,
	MSGID_RECEIVE_CLONEPARTICLERULE					= 0x00000056,

	// timeline events
	MSGID_RECEIVE_CREATETIMELINEEVENT				= 0x00000060,
	MSGID_RECEIVE_DELETETIMELINEEVENT				= 0x00000061,
	MSGID_RECEIVE_REORDERTIMELINEEVENTS				= 0x00000062,

	// domains
	MSGID_SEND_DOMAINSHAPELIST						= 0x00000070,
	MSGID_SEND_TOGGLEATTRACTORDOMAIN				= 0x00000071,

	// behaviours
	MSGID_SEND_BEHAVIOURLIST						= 0x00000080,
	MSGID_RECEIVE_ADDBEHAVIOUR						= 0x00000081,
	MSGID_RECEIVE_DELETEBEHAVIOUR					= 0x00000082,
	MSGID_RECEIVE_VALIDATEBEHAVIOUR					= 0x00000083,

	// keyframes
	MSGID_SEND_KEYFRAMEDEFN							= 0x00000090,
	MSGID_RECEIVE_KEYFRAMEREQUEST					= 0x00000091,
	MSGID_RECEIVE_KEYFRAME							= 0x00000092,
	MSGID_RECEIVE_KEYFRAMEEXISTS					= 0x00000093,

	// evolutions
	MSGID_RECEIVE_CREATEEVOLUTION					= 0x000000A0,
	MSGID_RECEIVE_DELETEEVOLUTION					= 0x000000A1,
	MSGID_RECEIVE_ADDEVOLVEDKEYFRAMEPROP			= 0x000000A2,
	MSGID_RECEIVE_DELETEEVOLVEDKEYFRAMEPROP			= 0x000000A3,
	MSGID_RECEIVE_LOADEVOLUTION						= 0x000000A4,
	MSGID_RECEIVE_SAVEEVOLUTION						= 0x000000A5,

	// graphics
	MSGID_SEND_CLIPREGIONDATA						= 0x000000B0,
	MSGID_SEND_BLENDSETLIST							= 0x000000B1,
	MSGID_SEND_SHADERTEMPLATELIST					= 0x000000B2,
	MSGID_RECEIVE_SETSHADERTEMPLATE					= 0x000000B3,

	// models
	MSGID_RECEIVE_ADDDELETEMODEL					= 0x000000C0,

	// saving
	MSGID_SEND_SAVEALLCOMPLETE						= 0x000000D0,
	MSGID_SEND_SAVEALLPROGRESS						= 0x000000D1,
	MSGID_RECEIVE_SAVE								= 0x000000D2,
	MSGID_RECEIVE_SAVEALL							= 0x000000D3,
	MSGID_RECEIVE_CANCELSAVEALL						= 0x000000D4,

	// misc
	MSGID_SEND_VERSION								= 0x000000E0,
	MSGID_SEND_DRAWLISTS							= 0x000000E1,
	MSGID_SEND_GAMESPECIFICDATA						= 0x000000E2,
	MSGID_RECEIVE_RESCALEZOOMABLECOMPONENTS			= 0x000000E3,

	// profile
	MSGID_SEND_GLOBALPROFILEDATA					= 0x000000F0,
	MSGID_SEND_EFFECTRULEPROFILEDATA				= 0x000000F1,
	MSGID_SEND_EMITTERRULEPROFILEDATA				= 0x000000F2,
	MSGID_SEND_PARTICLERULEPROFILEDATA				= 0x000000F3,

	// debug
	MSGID_RECEIVE_PLAYCONTROLDATA					= 0x00000100,
	MSGID_RECEIVE_STARTDEBUGEFFECT					= 0x00000101,
	MSGID_RECEIVE_SHOWDEBUGINFO						= 0x00000102,
	MSGID_REQUEST_RESET								= 0x00000103,
};


// classes
class ptxInterface : public ptxTcpComm
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxInterface();
		~ptxInterface();

		// virtual overrides
		void ProcessByteBuffer(ptxByteBuff& buff);

		// external sends to the editor
		void SendRulesLists();

		void SendKeyframeDefn(ptxKeyframe* pKeyframe, const char* pRuleName, ptxRuleType ruleType, u32 propertyId, s32 eventIdx, s32 evoIdx);

		void SendSaveAllCancelled(); 
		void SendSaveAllComplete(); 
		void SendSaveAllProgressUpdate(int progressBarId, int numItems, int currItem);

		// xml saving
		template<typename _Type>
		static void SaveXmlToByteBuffer(_Type& obj, ptxByteBuff& buff)
		{
			// allocate some memory
			int memorySize = 10*1024;
			char* pMemory = Alloca(char, memorySize);

			// create a filename for referencing the file in memory 
			char memoryFileName[RAGE_MAX_PATH];
			fiDevice::MakeMemoryFileName(memoryFileName, RAGE_MAX_PATH, pMemory, memorySize, false, "");

			// open the memory file
			fiStream* pStream = ASSET.Create(memoryFileName, "");

			// save the parser object to memory
			bool prevSetting = PARSER.Settings().SetFlag(parSettings::WRITE_XML_HEADER, false);
			PARSER.SaveObject(pStream, &obj);
			PARSER.Settings().SetFlag(parSettings::WRITE_XML_HEADER, prevSetting);
			
			// close the memory file
			char zero = '\0';
			pStream->WriteByte(&zero, 1);  
			pStream->Close();

			// write the memory to the buffer
			buff.Write_const_char(pMemory);
		}

		// accessors
		void SetNeedsReply(bool val) {m_needsReply = val;}
		ptxEffectRule* GetCurrEffectRule() {return m_pCurrEffectRule;}

		// play control accessors
		float GetTimeScalar() const {return m_timeScalar;}

		bool GetShouldRepeatPlayback() const {return m_shouldRepeatPlayback;}
		float GetRepeatPlaybackTime() const {return m_repeatPlaybackTime;}

		bool GetSpawnCamRelative() const {return m_spawnCamRelative;}
		float GetSpawnCamDist() const {return m_spawnCamDist;}
		Vec3V_Out GetSpawnWorldPos() const {return m_spawnWorldPos;}

		bool GetAnimEnabled() const {return m_animEnabled;}
		float GetAnimSpeed() const {return m_animSpeed;}
		float GetAnimRadius() const {return m_animRadius;}
		u8 GetAnimMode() const {return m_animMode;}


	private: //////////////////////////

		template<typename _Type>
		static void LoadXmlFromByteBuffer(_Type& obj, ptxByteBuff& buff)
		{
			// read the buffer into memory
			const char* pMemory = buff.Read_const_char();

			// create a filename for referencing the file in memory 
			int memorySize = (int)strlen(pMemory);
			char memoryFileName[RAGE_MAX_PATH];
			fiDevice::MakeMemoryFileName(memoryFileName, RAGE_MAX_PATH, pMemory, memorySize, false, "");
			
			// open the memory file
			fiStream* pStream = ASSET.Open(memoryFileName, "");
			
			// load the memory into the parser object
			PARSER.LoadObject(pStream, obj);
			
			// close the memory file
			pStream->Close();
		}

		void Connected();

		void SendBooleanReply(u32 replyId, bool val);

		// message responses - fx lists
		void ReceiveLoadFxList(ptxByteBuff& buff);
		void ReceiveSaveFxList(ptxByteBuff& buff);

		// message responses - rule lists
		void SendEffectRuleList();
		void SendEmitterRuleList();
		void SendParticleRuleList();

		// message responses - effect rules
public:
		void SendEffectRule(const char* pEffectRuleName);
private:
		void ReceiveEffectRule(ptxByteBuff& buff);
		void ReceiveLoadEffectRule(ptxByteBuff& buff);
		void ReceiveLoadEffectRules(ptxByteBuff& buff);
		void ReceiveCreateEffectRule(ptxByteBuff& buff);
		void ReceiveRemoveEffectRule(ptxByteBuff& buff);
		void ReceiveRenameEffectRule(ptxByteBuff& buff);

		// message responses - emitter rules
public:
		void SendEmitterRule(const char* pEmitterRuleName);
private:
		void ReceiveEmitterRule(ptxByteBuff& buff);
		void ReceiveLoadEmitterRule(ptxByteBuff& buff);
		void ReceiveCreateEmitterRule(ptxByteBuff& buff);
		void ReceiveRemoveEmitterRule(ptxByteBuff& buff);
		void ReceiveRenameEmitterRule(ptxByteBuff& buff);

		// message responses - particle rules
		void SendParticleRule(const char* pParticleRuleName);
		void ReceiveParticleRule(ptxByteBuff& buff);
		void ReceiveLoadParticleRule(ptxByteBuff& buff);
		void ReceiveCreateParticleRule(ptxByteBuff& buff);
		void ReceiveRemoveParticleRule(ptxByteBuff& buff);
		void ReceiveRenameParticleRule(ptxByteBuff& buff);
		void ReceiveCloneParticleRule(ptxByteBuff& buff);

		// message responses - timeline events
		void ReceiveCreateTimelineEvent(ptxByteBuff& buff);
		void ReceiveDeleteTimelineEvent(ptxByteBuff& buff);
		void ReceiveReorderTimelineEvents(ptxByteBuff& buff);

		// message responses - domains 
		void SendDomainShapeList();
		void ReceiveToggleAttractorDomain(ptxByteBuff& buff);

		// message responses - behaviours
		void SendBehaviourList();
		void ReceiveAddBehaviour(ptxByteBuff& buff);
		void ReceiveDeleteBehaviour(ptxByteBuff& buff);
		void ReceiveValidateBehaviour(ptxByteBuff& buff);

		// message responses - keyframes 
		void ReceiveKeyframeRequest(ptxByteBuff& buff);
		void ReceiveKeyframe(ptxByteBuff& buff);
		void ReceiveKeyframeExists(ptxByteBuff& buff);

		// message responses - evolutions
		void ReceiveCreateEvolution(ptxByteBuff& buff);
		void ReceiveDeleteEvolution(ptxByteBuff& buff);
		void ReceiveAddEvolvedKeyframeProp(ptxByteBuff& buff);
		void ReceiveDeleteEvolvedKeyframeProp(ptxByteBuff& buff);
		void ReceiveLoadEvolution(ptxByteBuff& buff);
		void ReceiveSaveEvolution(ptxByteBuff& buff);

		// message responses - graphics 
		void SendClipRegionData();
		void SendBlendSetList();
		void SendShaderTemplateList();
		void ReceiveSetShaderTemplate(ptxByteBuff& buff);

		// message responses - models 
		void ReceiveAddDeleteModel(ptxByteBuff& buff);

		// message responses - saving
		void ReceiveSave(ptxByteBuff& buff);
		void ReceiveSaveAll(ptxByteBuff& buff);
		void ReceiveCancelSaveAll(ptxByteBuff& buff);

		// message responses - misc 
		void SendVersion();
		void SendDrawLists();
		void SendGameSpecificData();
		void ReceiveRescaleZoomableComponents(ptxByteBuff& buff);
		void ReceiveCreateAutoLods(ptxByteBuff& buff);

		// message responses - profile 
		void SendGlobalProfileData();
		void SendEffectRuleProfileData(ptxByteBuff& buff);
		void SendEmitterRuleProfileData(ptxByteBuff& buff);
		void SendParticleRuleProfileData(ptxByteBuff& buff);

		// message responses - debug 
		void ReceivePlayControlData(ptxByteBuff& buff);
		void ReceiveStartDebugEffect(ptxByteBuff& buff);
		void ReceiveShowDebugInfo(ptxByteBuff& buff);
		void ReceiveReset(ptxByteBuff& buff);

		// test 
// 		void TestSendEffectRule(const char *name);

// 		template<typename _Type>
// 		void SendObject(_Type* obj, int messageID)
// 		{
// 			m_byteBuff.BeginTCPBuffer();
// 			m_byteBuff.Write_u32(messageID);
// 			if (obj)
// 			{
// 				SaveXmlToByteBuffer(*obj, m_byteBuff);
// 			}
// 			else
// 			{
// 				m_byteBuff.Write_const_char("");
// 			}
// 			m_byteBuff.EndTCPBuffer();
// 			SendByteBuffer(&m_byteBuff);
// 		}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		ptxByteBuff m_byteBuff;

		pgRef<ptxEffectRule> m_pCurrEffectRule;
		pgRef<ptxEmitterRule> m_pCurrEmitterRule;							// rsTODO: do we really need this to be defragmentable?
	
		bool m_needsReply;	

		// global controls
		float m_timeScalar;

		// debug effect controls
		bool m_shouldRepeatPlayback;
		float m_repeatPlaybackTime;

		// debug effect spawn info
		bool m_spawnCamRelative;
		float m_spawnCamDist;
		Vec3V m_spawnWorldPos;

		// debug effect anim info
		bool m_animEnabled;
		float m_animSpeed;
		float m_animRadius;
		u8 m_animMode;



};


} // namespace rage


#endif // RMPTFX_EDITOR


#endif // RMPTFX_PTXINTERFACE_H
