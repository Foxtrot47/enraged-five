// 
// rmptfx/ptxtcpcomm.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxtcpcomm.h"

// includes (rmptfx)
#include "rmptfx/ptxchannel.h"
#include "rmptfx/ptxdebug.h"
#include "system/param.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// params
PARAM(rmptfxlisten, "RMPTFX listens for Particle Editor communication");


// namespaces
using namespace rage;



#if RMPTFX_EDITOR


// defines
#define PTXTCPCOMM_DEBUG							(1)
#define PTXTCPCOMM_PING_COUNT_MAX					(500)					// if pings fail for this amount of frames in a row then we have lost the connection


// code
ptxTcpComm::ptxTcpComm()
{
	m_isConnected = false;
	m_isListening = false;

	m_listenSocket = fiHandleInvalid;
	m_tcpSocket = fiHandleInvalid;

	m_pingCount = 0;

	m_numBytesToRead = 0;
	m_inByteBuffer.Create(32*1024);
	m_pingBuffer.Create(8);
	m_pingBuffer.BeginWrite();
	m_pingBuffer.Write_u32(4);
	m_pingBuffer.Write_u32(0);

#if RMPTFX_BANK
	ptxDebug::sm_rmptfxListen = PARAM_rmptfxlisten.Get();
#endif
}

ptxTcpComm::~ptxTcpComm()
{
	Disconnect();
}

void ptxTcpComm::StartListening()
{
	m_isListening = true;

	// start a thread which listens for a connection
	// NOTE: core 1 isn't creating this thread on 360 for some reason - when -mt is on the sample_viewer cmd line and __RMPTFX_MULTITHREADED == 1
	sysIpcCreateThread(ptxTcpComm::ListenerThread, this, sysIpcMinThreadStackSize, PRIO_BELOW_NORMAL, "[RAGE] RMPTFXListenerThread", 2); 

#if PTXTCPCOMM_DEBUG
	ptxDisplayf("RMPTFX Listening...");
#endif
}

void ptxTcpComm::Update()
{
	// don't try to connect if the command line parameter isn't present
	if (!(PARAM_rmptfxlisten.Get() RMPTFX_BANK_ONLY( || ptxDebug::sm_rmptfxListen)))
	{
		return;
	}

	//Here we read packets and determine if we have lost a connection
	if (m_isConnected==false) 
	{
		if (m_isListening==false)	
		{
			StartListening();
		}

		return;
	}

	int numBytesAvailable = 0;
	do
	{
		numBytesAvailable = fiDeviceTcpIp::GetInstance().GetReadCount(m_tcpSocket);
		if (numBytesAvailable)
		{
			if (m_inByteBuffer.GetCurrWritePos()==0)
			{	
				BeginNewByteBuffer();
				ContinueByteBuffer();
			}
			else
			{
				// we are using all blocking code so, this is not used currently
				ContinueByteBuffer();
			}
		}
		else
		{
			// determine if we are still connected via ping
			m_pingCount++;
			if (m_pingCount>PTXTCPCOMM_PING_COUNT_MAX)
			{
#if PTXTCPCOMM_DEBUG
				ptxDisplayf("RMPTFX Ping");		
#endif

				if (SendByteBuffer(&m_pingBuffer)==false)
				{
					LostConnection();
				}
			}
		}

		// determine if we can process the packet
		if (m_inByteBuffer.IsReady())
		{
			ProcessByteBuffer(m_inByteBuffer);
			m_inByteBuffer.Clear();
			m_numBytesToRead = 0;
		}
	} 
	while(numBytesAvailable);
}

void ptxTcpComm::Disconnect()
{
#if PTXTCPCOMM_DEBUG
	bool wasConnected = m_isConnected || m_isListening;
#endif

	// end communication
	fiDeviceTcpIp::GetInstance().Close(m_tcpSocket);
	m_tcpSocket = fiHandleInvalid;

	fiDeviceTcpIp::GetInstance().Close(m_listenSocket);
	m_listenSocket = fiHandleInvalid;

	m_isConnected = false;
	m_isListening = false;

	m_inByteBuffer.BeginWrite();
	m_numBytesToRead = 0;
	m_pingCount = 0;

#if PTXTCPCOMM_DEBUG
	if (wasConnected)
	{
		ptxDisplayf("RMPTFX Disconnected");
	}
#endif
}

bool ptxTcpComm::SendByteBuffer(ptxByteBuff* pByteBuff)
{
	if (m_isConnected==false)
	{
		return false;
	}

	ptxAssertf(pByteBuff->IsReady(), "fFirst 4 bytes of buffer must represent its actual size");

	// SafeWrite is blocking
	bool res = fiDeviceTcpIp::GetInstance().SafeWrite(m_tcpSocket, pByteBuff->GetBuffer(), pByteBuff->GetCurrWritePos());
	if (res==false)
	{
		LostConnection();
	}
	else 
	{
		// successful send means we are still connected
		m_pingCount = 0;  
	}

	return res;
}

void ptxTcpComm::Connected()
{
	// called when a connection is established
	m_pingCount = 0;

	m_isConnected = true;
	m_isListening = false;

#if PTXTCPCOMM_DEBUG
	ptxDisplayf("RMPTFX Connected");
#endif
}

DECLARE_THREAD_FUNC(ptxTcpComm::ListenerThread)
{
	if (ptr)
	{
		((ptxTcpComm*)ptr)->ListenLoop();
	}
}

void ptxTcpComm::ListenLoop()
{
	while (m_isConnected==false && m_isListening)
	{
		if (fiIsValidHandle(m_listenSocket)==false)
		{
			m_listenSocket = fiDeviceTcpIp::Listen(3333, 1);
		}

		if (fiIsValidHandle(m_tcpSocket)==false)
		{
			m_tcpSocket = fiDeviceTcpIp::Pickup(m_listenSocket);
			if (fiIsValidHandle(m_tcpSocket)==false) 
			{
				// Don't spam the TTY every loop if Pickup() fails (Cable unplugged; url:bugstar:1917908)
				sysIpcSleep(250);
			}
		}

		if (fiIsValidHandle(m_tcpSocket)) 
		{
			Connected();
		}
	}
}

void ptxTcpComm::LostConnection()
{
#if PTXTCPCOMM_DEBUG
	ptxWarningf("Particle Editor connection lost");
#endif

	Disconnect();
}

void ptxTcpComm::ProcessByteBuffer(ptxByteBuff& byteBuff)
{
	// call an external function to parse the packet
	byteBuff.BeginRead();

	ptxDisplayf("RMPTFX Byte Buffer Processed - size: %d", byteBuff.Read_u32());
	ptxDisplayf("RMPTFX Byte Buffer Processed - id: %d", byteBuff.Read_u32());
}

void ptxTcpComm::BeginNewByteBuffer()
{
	if (m_isConnected==false)
	{
		return;
	}

	// SafeRead is blocking
	if (fiDeviceTcpIp::GetInstance().SafeRead(m_tcpSocket, m_inByteBuffer.GetBuffer(), 4))
	{
		m_inByteBuffer.BeginRead();
		m_inByteBuffer.SetCurrWritePos(4);
		m_numBytesToRead = m_inByteBuffer.Read_u32();
	}
	else
	{
		LostConnection();
	}
}

void ptxTcpComm::ContinueByteBuffer()
{
	if (m_isConnected==false || m_numBytesToRead==0)
	{
		return;
	}

	int numBytesAvailable = fiDeviceTcpIp::GetInstance().GetReadCount(m_tcpSocket);
	if (numBytesAvailable)
	{
		// SafeRead is blocking
		if (fiDeviceTcpIp::GetInstance().SafeRead(m_tcpSocket, m_inByteBuffer.GetBuffer()+m_inByteBuffer.GetCurrWritePos(), m_numBytesToRead))
		{
			m_inByteBuffer.SetCurrWritePos(m_inByteBuffer.GetCurrWritePos()+m_numBytesToRead);
		}
		else
		{
			LostConnection();
		}
	}
}


#endif // RMPTFX_EDITOR
