////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//  rmptfx_litsprite.fx
//
//  Adds normal-map based lighting to particles.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __RMPTFX_LITSPRITE_FX
#define __RMPTFX_LITSPRITE_FX

#include "../../../base/src/shaderlib/rmptfx_common.fxh"
#pragma dcl position normal diffuse texcoord0 texcoord1 texcoord2 texcoord3 texcoord4

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float4 ScreenRez:ScreenRez;


BeginSampler(sampler,NormalMapTex,NormalMapTexSampler,NormalMapTex)
string UIName = "Normal Map";
ContinueSampler(sampler,NormalMapTex,NormalMapTexSampler,NormalMapTex)
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = NONE;
    AddressU  = CLAMP;        
    AddressV  = CLAMP;
EndSampler;

// Frame map	
BeginSampler(sampler,FrameMap,FrameMapTexSampler,FrameMap)	// Frame map 
ContinueSampler(sampler,FrameMap,FrameMapTexSampler,FrameMap) 
   MinFilter = LINEAR;
   MagFilter = LINEAR;
   MipFilter = NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

BeginConstantBufferDX10(rmptfx_litsprite_locals)
// TEST LIGHTING			///////////////////////////////////////////////////////////////////////////////////////
float3 LightColor
<
	string UIName = "Light Color";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = float3(0.782f,0.7925f,0.06f);

float3 LightDir
<
	string UIName = "Light Dir";
	float UIMin = -1.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = float3(-0.5,-0.5,0);

float ghorizScale 
<
	string UIName = "Horizontal Warp Scale";
	float UIMin = 0.0;
	float UIMax = 2000.0;
	float UIStep = 0.01;
> = 0.0;
float gvertScale 
<
	string UIName = "Horizontal Warp Scale";
	float UIMin = 0.0;
	float UIMax = 2000.0;
	float UIStep = 0.01;
> = 0.220;

float gIntensity
<
	string UIName = "Horizontal Warp Scale";
	float UIMin = -1000.0;
	float UIMax = 2000.0;
	float UIStep = 0.01;
> = 1.0;
float gPower
<
	string UIName = "Horizontal Warp Scale";
	float UIMin = -1000.0;
	float UIMax = 2000.0;
	float UIStep = 0.01;
> = 1.0;

float gNormalScale 
<
	string UIName = "Horizontal Warp Scale";
	float UIMin = -1000.0;
	float UIMax = 2000.0;
	float UIStep = 0.01;
> = 1.0;

float gZdampen
<
	string UIName = "Horizontal Warp Scale";
	float UIMin = -1000.0;
	float UIMax = 2000.0;
	float UIStep = 0.01;
> = 0.0;

float gHeatHaze
<
	string UIName = "HeatHazeFactor";
	float UIMin = -1000.0;
	float UIMax = 2000.0;
	float UIStep = 0.01;
> = 0.0;

float gHeatPhase
<
	string UIName = "HeatPhaseFactor";
	float UIMin = -1000.0;
	float UIMax = 2000.0;
	float UIStep = 0.01;
> = 0.0;
EndConstantBufferDX10(rmptfx_litsprite_locals)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Output Structure
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct rmptfxPS {
	rmptfxPSBase BASE;
    float3 lightDir			: TEXCOORD5;					// lightdir in camspace, softparticle precalc
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float4 PS_Draw( rmptfxPS IN ) : COLOR
{
	// Color calculation
	float4 texColor;
	rage_rmptfx_ComputeDiffusePixelColor(texColor,IN.BASE);
	float4 color = texColor * IN.BASE.color;
	
	// Lighting 
	float3 normal = ((tex2D(NormalMapTexSampler, IN.BASE.normalTexCoord.xy)*2.0f)-1.0f).xyz;	// Scale range to -1 to 1.
	float intensity = saturate( dot(normal, IN.lightDir) );
	float3 lightCol = LightColor*intensity;
	color.xyz+=lightCol*texColor.xyz;
	
	POSTPROCESS_PS(color,IN.BASE);
	return color;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float4 PS_DrawWaterDrop( rmptfxPS IN ) : COLOR
{
	float4 texColor;
	texColor = tex2D(DiffuseTexSampler,IN.BASE.texCoord.xy);
	float3 normal = ((tex2D(NormalMapTexSampler, IN.BASE.normalTexCoord.xy)*2.0f)-1.0f).xyz;	// Scale range to -1 to 1.
	float3 lightnormal = normal;
	lightnormal.z *= gZdampen;

	normal *= gNormalScale;
	normal.z*=gZdampen;
	
	float sinTheta, cosTheta;	
	sincos( IN.BASE.internal.y, sinTheta, cosTheta);	
	float nnx = normal.x;
	float nny = normal.y;
	normal.x = nnx * cosTheta - nny * sinTheta;
	normal.y = nnx* sinTheta + nny* cosTheta;
	

	//float3 lightnormal = normal;
	lightnormal *= gIntensity;
	lightnormal.y *=-1.0f;
	
	// Lighting 
	//float intensity = (dot(lightnormal, IN.lightDir))/gIntensity;
	float intensity = (dot(lightnormal, IN.lightDir));
	intensity = ragePow(intensity,gPower);
	float3 lightCol = LightColor*(intensity);
	
	//scale the normal to sample pixels
	normal.x /= ghorizScale;
	normal.y /= gvertScale;
	
	//add in the normal's offset
	float texx = saturate(IN.BASE.internal.z);
	float texy = saturate(IN.BASE.internal.w);

	//float frametx = texx;
	//float framety = texy;
	
	float frametx = texx+normal.x;
	float framety = texy-normal.y;
	
	float4 frameColor  = tex2D(FrameMapTexSampler,float2(frametx,framety));
	float4 color = frameColor * IN.BASE.color;
	color.rgb += lightCol;
	color.w = texColor.w*IN.BASE.color.w;
	
	POSTPROCESS_PS(color,IN.BASE);
	return color;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float4 PS_DrawHeatHaze( rmptfxPS IN ) : COLOR
{
	float4 texColor;
	texColor = tex2D(DiffuseTexSampler,IN.BASE.texCoord.xy);

	float texx = (IN.BASE.internal.z);
	float texy = (IN.BASE.internal.w);
	
	float frametx = (texx+1.0f)*0.5f;;
	float framety = 1.0f-(texy+1.0f)*0.5f;
	
	//distort the image
	float sinTA, cosTA;
	sincos((IN.BASE.custom1.x+texx)*gHeatHaze, sinTA, cosTA);
	frametx+=sinTA/gHeatPhase;
	framety-=cosTA/gHeatPhase;
	//float sinTB, cosTB;
	//sincos((IN.BASE.custom1.x+texy)*gHeatHaze, sinTB, cosTB);
	//frametx+=sinTB/gHeatPhase;
	//framety-=cosTB/gHeatPhase;

	
	float4 frameColor  = tex2D(FrameMapTexSampler,float2(frametx,framety));
	
	//Sample points around the core
	//float2 uv = float2(0,0);
	
	//frameColor += tex2D(FrameMapTexSampler,float2(frametx,framety-gHeatHaze));
	//frameColor += tex2D(FrameMapTexSampler,float2(frametx+gHeatHaze,framety));
	//frameColor += tex2D(FrameMapTexSampler,float2(frametx-gHeatHaze,framety));
	
	//frameColor/=4.0f;
	

	float4 color = frameColor * IN.BASE.color;
	color.w = texColor.w*IN.BASE.color.w;
	
	POSTPROCESS_PS(color,IN.BASE);

	return color;

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rmptfxPS VS_TransformFlat(rmptfxVS IN)
{
	rmptfxPS OUT;
	OUT.BASE.color = IN.color;
	OUT.BASE.texCoord = IN.texCoord;

	//OUT.BASE.pos = mul(float4(IN.center.xyz,1), gWorldViewProj);
	OUT.BASE.pos = IN.center;
 	OUT.BASE.pos.z=0.0f;

	OUT.BASE.pos += rageComputeBillboardNoAspect(IN.billboard.xyz);
		
	// Lighting
	OUT.lightDir = float3(0,0,0);		
	// Calculate light direction in camera space
	float3 dir = -normalize(LightDir);
	OUT.lightDir = mul(float4(dir,0),gWorldView).xyz;
		
	// "unrotate" light direction to compensate for sprite rotation of normal map
	if(IN.billboard.z!=0)
	{
		float sinTheta, cosTheta;
		sincos(-IN.billboard.z, sinTheta, cosTheta);	
		OUT.lightDir.xy = float2(OUT.lightDir.x * cosTheta - OUT.lightDir.y * sinTheta, 
									OUT.lightDir.x * sinTheta + OUT.lightDir.y * cosTheta);
	}

	POSTPROCESS_VS(OUT.BASE,IN);
	
	OUT.BASE.internal.z = (OUT.BASE.pos.x+1.0f)/2.0f;
	OUT.BASE.internal.w = (OUT.BASE.pos.y+1.0f)/2.0f;
	
	return OUT;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rmptfxPS VS_Transform(rmptfxVS IN)
{
	rmptfxPS OUT;
	OUT.BASE.color = IN.color;
	OUT.BASE.texCoord = IN.texCoord;
	
	//Bias Towards the camera
	float3 cameraBias = gViewInverse[3].xyz - IN.center.xyz;
	normalize(cameraBias);
	IN.center.xyz += cameraBias*(gBiasToCamera*0.1f);
	
	OUT.BASE.pos = mul(float4(IN.center.xyz,1), gWorldViewProj);
	OUT.BASE.pos += rageComputeBillboard(IN.billboard.xyz);

	// Lighting
	OUT.lightDir = float3(0,0,0);		
	// Calculate light direction in camera space
	float3 dir = -normalize(LightDir);
	OUT.lightDir = mul(float4(dir,0),gWorldView).xyz;
		
	// "unrotate" light direction to compensate for sprite rotation of normal map
	if(IN.billboard.z!=0)
	{
		float sinTheta, cosTheta;
		sincos(-IN.billboard.z, sinTheta, cosTheta);	
		OUT.lightDir.xy = float2(OUT.lightDir.x * cosTheta - OUT.lightDir.y * sinTheta, 
									OUT.lightDir.x * sinTheta + OUT.lightDir.y * cosTheta);
	}

	POSTPROCESS_VS(OUT.BASE,IN);
	
	OUT.BASE.internal.z = (OUT.BASE.pos.x/OUT.BASE.pos.w);
	OUT.BASE.internal.w = (OUT.BASE.pos.y/OUT.BASE.pos.w);

	
	return OUT;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Techniques
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	technique screenTest
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_TransformFlat();
			PixelShader  = compile PIXELSHADER PS_DrawWaterDrop();
		}
	}
	
	technique heat_haze
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_Transform();
			PixelShader  = compile PIXELSHADER PS_DrawHeatHaze();
		}
	}

	technique draw
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_Transform();
			PixelShader  = compile PIXELSHADER PS_Draw();
		}
	}

	technique unlit_draw
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_Transform();
			PixelShader  = compile PIXELSHADER PS_Draw();
		}
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif 

