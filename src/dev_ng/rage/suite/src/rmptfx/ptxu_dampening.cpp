// 
// rmptfx/ptxu_dampening.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// includes
#include "rmptfx/ptxu_dampening.h"
#include "rmptfx/ptxu_dampening_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"

#if !__RESOURCECOMPILER && !RSG_TOOL
#include "vfx/ptfx/ptfxcallbacks.h"
#endif


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{


//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_Dampening::sm_xyzMinDefn(					"Dampening Min",			atHashValue("ptxu_Dampening:m_xyzMinKFP"),					PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		10000.0f,	0.01f),		"Min Damp X",	"Min Damp Y",	"Min Damp Z");
ptxKeyframeDefn ptxu_Dampening::sm_xyzMaxDefn(					"Dampening Max",			atHashValue("ptxu_Dampening:m_xyzMaxKFP"),					PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		10000.0f,	0.01f),		"Max Damp X",	"Max Damp Y",	"Max Damp Z");


// code
#if RMPTFX_XML_LOADING
ptxu_Dampening::ptxu_Dampening()
{
	// create keyframes
	m_xyzMinKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Dampening:m_xyzMinKFP"));
	m_xyzMaxKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Dampening:m_xyzMaxKFP"));

	m_keyframePropList.AddKeyframeProp(&m_xyzMinKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_xyzMaxKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();

	// init global data
	m_pGlobalData_NOTUSED = NULL;
}
#endif

void ptxu_Dampening::SetDefaultData()
{
	// default keyframes
	m_xyzMinKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_xyzMaxKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// default other data
	m_referenceSpace = PTXU_DAMPENING_REFERENCESPACE_WORLD;
	m_enableAirResistance = false;

}

ptxu_Dampening::ptxu_Dampening(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_xyzMinKFP(rsc)
	, m_xyzMaxKFP(rsc)
{
	m_pGlobalData_NOTUSED = NULL;

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxu_Dampening_num++;
#endif
}

#if __DECLARESTRUCT
void ptxu_Dampening::DeclareStruct(datTypeStruct& s)
{
	m_pGlobalData_NOTUSED = NULL; 

	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Dampening, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_Dampening, m_xyzMinKFP)
		SSTRUCT_FIELD(ptxu_Dampening, m_xyzMaxKFP)
		SSTRUCT_IGNORE(ptxu_Dampening, m_pGlobalData_NOTUSED)
		SSTRUCT_FIELD(ptxu_Dampening, m_referenceSpace)
		SSTRUCT_FIELD(ptxu_Dampening, m_enableAirResistance)
		SSTRUCT_IGNORE(ptxu_Dampening, m_pad)
	SSTRUCT_END(ptxu_Dampening)
}
#endif

IMPLEMENT_PLACE(ptxu_Dampening);

void ptxu_Dampening::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointDampening)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_Dampening::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	ptxPointItem* pPointItem = params.pPointItem;	
	ScalarV	vDeltaTime = params.vDeltaTime;

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// cache data from the point
	ScalarV	vPlaybackRate = ScalarVFromF32(pPointSB->GetPlaybackRate());

	// get the particle and emitter life ratio key times
	ScalarV vKeyTimePoint = pPointDB->GetKeyTime();
	ScalarV vKeyTimePointEmitter = ScalarVFromF32(pPointSB->GetEmitterLifeRatio());

	// cache some data that we'll need later
	ptxEmitterRule*	RESTRICT pEmitterRule = static_cast<ptxEmitterRule*>(pEmitterInst->GetEmitterRule());
	const ScalarV vOne(V_ONE);
	const ScalarV vDiv100 = ScalarV(V_FLT_SMALL_2);
	ScalarV vAirResistanceGame = vOne;
#if	!__RESOURCECOMPILER && !RSG_TOOL
	vAirResistanceGame = ScalarVFromF32(g_pPtfxCallbacks->GetAirResistance());
#endif
	ptxAssertf(IsFiniteAll(vAirResistanceGame), "vAirResistanceGame is not finite");

	// get the keyframe data
	ptxEvolutionList* pEvolutionList = params.pEvolutionList;
	Vec4V vPtxDampeningMin = m_xyzMinKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
	Vec4V vPtxDampeningMax = m_xyzMaxKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
	Vec4V vEmitDampening = pEmitterRule->GetDampeningScalarKFP().Query(vKeyTimePointEmitter, pEvolutionList, pPointSB->GetEvoValues()) * vDiv100;

	// calculate the bias value
	ScalarV vBias = GetBiasValue(m_xyzMinKFP, pPointSB->GetRandIndex());

	// calculate the lerped dampening between the min and max keyframe data
	Vec4V vDampening = Lerp(vBias, vPtxDampeningMin, vPtxDampeningMax);
	ptxAssertf(IsFiniteAll(vDampening), "vDampening is not finite (1)");

	// scale this by the emit dampening
	vDampening *= vEmitDampening;
	ptxAssertf(IsFiniteAll(vDampening), "vDampening is not finite (2)");

	// calc the dampening in effect space if required
	if (m_referenceSpace!=PTXU_DAMPENING_REFERENCESPACE_WORLD)
	{
		Vec3V vDampening3 = vDampening.GetXYZ();
		Mat34V vMtx = Mat34V(V_IDENTITY);
		if (m_referenceSpace==PTXU_DAMPENING_REFERENCESPACE_EFFECT)
		{
			vMtx = pEffectInst->GetMatrix();
		}
		else if (m_referenceSpace==PTXU_DAMPENING_REFERENCESPACE_EMITTER)
		{
			vMtx = pEmitterInst->GetCreationDomainMtxWld(pEffectInst->GetMatrix());
		}
		else
		{
			ptxAssertf(0, "unsupported reference space");
		}

		// scale the effect matrix by the dampening values
		Mat33V vDampMtx;
		vDampMtx.SetCol0(vMtx.GetCol0() * Vec3V(SplatX(vDampening3)));
		vDampMtx.SetCol1(vMtx.GetCol1() * Vec3V(SplatY(vDampening3)));
		vDampMtx.SetCol2(vMtx.GetCol2() * Vec3V(SplatZ(vDampening3)));

		// add up the lengths of each axis to get the world space lengths
		Abs(vDampMtx, vDampMtx);
		Vec3V vDampening3Final = vDampMtx.GetCol0() + vDampMtx.GetCol1() + vDampMtx.GetCol2();
		vDampening = Vec4V(vDampening3Final, vOne);
		ptxAssertf(IsFiniteAll(vDampening), "vDampening is not finite (3)");
	}

	// scale by the game air resistance
	if (m_enableAirResistance)
	{
		vDampening *= vAirResistanceGame;
		ptxAssertf(IsFiniteAll(vDampening), "vDampening is not finite (4)");
	}

	// calculate the velocity scale
	Vec4V vVelScale;
	vVelScale = Vec4V(vOne) - (vDampening * vDeltaTime * vPlaybackRate);
	vVelScale = Max(vVelScale, Vec4V(V_ZERO));
	ptxAssertf(IsFiniteAll(vVelScale), "vVelScale is not finite");

	// scale the particle's velocity
	if (IsFiniteAll(vVelScale))
	{
		pPointDB->ScaleCurrVel(vVelScale.GetXYZ());
	}
}

void ptxu_Dampening::SetKeyframeDefns()
{
	m_xyzMinKFP.GetKeyframe().SetDefn(&sm_xyzMinDefn);
	m_xyzMaxKFP.GetKeyframe().SetDefn(&sm_xyzMaxDefn);
}

void ptxu_Dampening::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_xyzMinKFP, &sm_xyzMinDefn);
	pEvolutionList->SetKeyframeDefn(&m_xyzMaxKFP, &sm_xyzMaxDefn);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_Dampening::IsActive()
{
	ptxu_Dampening* pDampening = static_cast<ptxu_Dampening*>(RMPTFXMGR.GetBehaviour(GetName()));
	
	bool isEqual = m_xyzMinKFP == pDampening->m_xyzMinKFP &&
		   		   m_xyzMaxKFP == pDampening->m_xyzMaxKFP && 
				   m_referenceSpace == pDampening->m_referenceSpace &&
				   m_enableAirResistance == pDampening->m_enableAirResistance;

	return !isEqual;
}

void ptxu_Dampening::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 4);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "Dampening Min");
	SendKeyframeDefnToEditor(buff, "Dampening Max");

	// send non-keyframes
	const char* pComboItemNames[PTXU_DAMPENING_REFERENCESPACE_NUM] = {"World", "Effect", "Emitter"};
	SendComboDefnToEditor(buff, "Reference Space", PTXU_DAMPENING_REFERENCESPACE_NUM, pComboItemNames);

	SendBoolDefnToEditor(buff, "Scale With Game Air Resistance");
}

void ptxu_Dampening::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	// send keyframes
	SendKeyframeToEditor(buff, m_xyzMinKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_xyzMaxKFP, pParticleRule);

	// send non-keyframes
	SendComboItemToEditor(buff, m_referenceSpace);
	SendBoolToEditor(buff, m_enableAirResistance);

}

void ptxu_Dampening::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_xyzMinKFP);
	ReceiveKeyframeFromEditor(buff, m_xyzMaxKFP);

	// receive non-keyframes
	ReceiveComboItemFromEditor(buff, m_referenceSpace);
	ReceiveBoolFromEditor(buff, m_enableAirResistance);

}
#endif // RMPTFX_EDITOR


} // namespace rage
