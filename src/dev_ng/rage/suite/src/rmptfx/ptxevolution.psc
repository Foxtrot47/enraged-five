<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::ptxEvolution" cond="RMPTFX_XML_LOADING">
	<string name="m_name" type="ConstString"/>
</structdef>

<structdef type="rage::ptxEvolvedKeyframe" cond="RMPTFX_XML_LOADING">
	<int name="m_evolutionIdx"/>
	<bool name="m_isLodEvolution"/>
	<struct name="m_keyframe" type="rage::ptxKeyframe"/>
</structdef>

<structdef type="rage::ptxEvolvedKeyframeProp" cond="RMPTFX_XML_LOADING">
	<u32 name="m_propertyId"/>
	<u8 name="m_blendMode"/>
	<array name="m_evolvedKeyframes" type="atArray">
		<struct type="rage::ptxEvolvedKeyframe"/>
	</array>
</structdef>

<structdef type="rage::ptxEvolutionList" onPostLoad="OnPostLoad" cond="RMPTFX_XML_LOADING">
	<array name="m_evolutions" type="atArray">
		<struct type="rage::ptxEvolution"/>
	</array>
	<array name="m_evolvedKeyframeProps" type="atArray">
		<struct type="rage::ptxEvolvedKeyframeProp"/>
	</array>
</structdef>

</ParserSchema>