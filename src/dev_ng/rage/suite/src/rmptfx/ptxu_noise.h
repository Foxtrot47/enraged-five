// 
// rmptfx/ptxu_noise.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_NOISE_H 
#define PTXU_NOISE_H 


// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"


// namespaces
namespace rage
{


// enumerations
enum ptxu_Noise_ReferenceSpace
{
	PTXU_NOISE_REFERENCESPACE_WORLD	= 0,
	PTXU_NOISE_REFERENCESPACE_EFFECT,
	PTXU_NOISE_REFERENCESPACE_EMITTER,

	PTXU_NOISE_REFERENCESPACE_NUM
};


// classes
class ptxu_Noise : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_Noise();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxu_Noise(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_Noise);

		// info
		const char* GetName() {return "ptxu_Noise";}													// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_NOISE;}												// the ordering of the behaviour (see ptxconfig.h)

		bool NeedsToInit() {return true;}																// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}																// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return false;}													// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return false;}																// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}															// whether we call release when a particle dies

		void InitPoint(ptxBehaviourParams& params);														// called for each newly created particle if NeedsToInit returns true
		void UpdatePoint(ptxBehaviourParams& params);													// called from InitPoint and UpdatePoint to update the particle accordingly 
		void ChangeSpace(Vec3V_InOut vNoise,															// changes the world space noise into effect or emitter space
			const ptxEffectInst* pEffectInst, 
			ptxEmitterInst* pEmitterInst);

		void SetKeyframeDefns();																		// sets the keyframe definitions on the keyframe properties
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);									// sets the keyframe definitions on the evolved keyframe properties in the evolution list

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Noise";}													// name of the behaviour shown in the editor
		bool IsActive();																				// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& buff);														// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);				// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);										// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif



	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data		
		ptxKeyframeProp m_posNoiseMinKFP;	
		ptxKeyframeProp m_posNoiseMaxKFP;			
		ptxKeyframeProp m_velNoiseMinKFP;		
		ptxKeyframeProp m_velNoiseMaxKFP;		

		// non keyframe data
		int m_referenceSpace;
		bool m_keepConstantSpeed;													// whether the velocity noise affects speed

		datPadding<11>	m_pad;

		// static data
		static ptxKeyframeDefn sm_posNoiseMinDefn;
		static ptxKeyframeDefn sm_posNoiseMaxDefn;
		static ptxKeyframeDefn sm_velNoiseMinDefn;
		static ptxKeyframeDefn sm_velNoiseMaxDefn;

};


} // namespace rage


#endif // PTXU_NOISE_H 
