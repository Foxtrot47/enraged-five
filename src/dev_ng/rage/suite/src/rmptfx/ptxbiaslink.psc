<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::ptxBiasLink" cond="RMPTFX_XML_LOADING">
	<string name="m_name" type="member" size="64"/>
	<array name="m_keyframePropIds" type="atArray">
		<int/>
	</array>
</structdef>

</ParserSchema>