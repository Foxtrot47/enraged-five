<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_Dampening" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<struct name="m_xyzMinKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_xyzMaxKFP" type="::rage::ptxKeyframeProp"/>
	<int name="m_referenceSpace"/>
	<bool name="m_enableAirResistance"/>
</structdef>

</ParserSchema>