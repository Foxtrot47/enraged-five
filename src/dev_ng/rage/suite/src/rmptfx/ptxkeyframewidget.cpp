// 
// rmptfx/ptxkeyframewidget.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxkeyframewidget.h"

// includes (rmptfx)
#include "rmptfx/ptxchannel.h"
#include "rmptfx/ptxkeyframe.h"

// includes (rage)
#include "bank/bank.h"
#include "bank/packet.h"
#include "bank/pane.h"
#include "vectormath/vec4v.h"
#if __WIN32PC
#include "system/xtl.h"
#endif


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
using namespace rage;


// enumerations
#if RMPTFX_BANK
enum
{
	KEYFRAME_FULL_UPDATE		= bkRemotePacket::USER+0,
	KEYFRAME_PARTIAL_KEYS_BEGIN	= bkRemotePacket::USER+1,
	KEYFRAME_PARTIAL_KEYS		= bkRemotePacket::USER+2,
	KEYFRAME_PARTIAL_KEYS_END	= bkRemotePacket::USER+3,
	UI_UPDATE_INFO				= bkRemotePacket::USER+4,
};
#endif


// code
#if RMPTFX_BANK
ptxKeyframeWidget::ptxKeyframeWidget(bkBank& bank, ptxKeyframe& keyframe, const char* pTitle, const char* pMemo, const char* pFillColor)
: bkWidget(NullCallback, pTitle, pMemo, pFillColor)
, m_keyframe(keyframe)
, m_bank(bank)
{
}

ptxKeyframeWidget::~ptxKeyframeWidget()
{
}

void ptxKeyframeWidget::InitClass()
{
	bkRemotePacket::AddType(BKGUID('r', 'k', 'e', 'y'), RemoteHandler);
	bkRemotePacket::AddType(BKGUID('r', 'c', 'g', 'e'), RemoteHandler);
}

int ptxKeyframeWidget::GetGuid() const 
{
	return m_keyframe.GetDefn()->GetUseColourGradient() ? BKGUID('r', 'c', 'g', 'e') : BKGUID('r', 'k', 'e', 'y');
}

void ptxKeyframeWidget::RemoteCreate()
{
	ptxKeyframeDefn* pKeyframeDefn;
	pKeyframeDefn = m_keyframe.GetDefn();

	bkWidget::RemoteCreate();

	bkRemotePacket packet;
	packet.Begin(bkRemotePacket::CREATE, GetGuid(), this);
	packet.WriteWidget(m_Parent);
	packet.Write_const_char(m_Title);
	packet.Write_const_char(GetTooltip());
	packet.Write_const_char(GetFillColor());
	packet.Write_bool(IsReadOnly());

	for (int i=0; i<4; i++)
	{
		if (pKeyframeDefn->m_labels[i]) 
		{
			packet.Write_const_char(pKeyframeDefn->m_labels[i]);
		}
		else 
		{
			packet.Write_const_char("");
		}
	}

	packet.Write_float(pKeyframeDefn->m_vMinMaxDeltaY.GetXf());
	packet.Write_float(pKeyframeDefn->m_vMinMaxDeltaY.GetYf());
	packet.Write_u8((u8)pKeyframeDefn->m_type);

	packet.Send();

	UpdateUIData();

	RemoteUpdate();
}

void ptxKeyframeWidget::RemoteUpdate()
{
	if (bkRemotePacket::IsConnected()==false) 
	{
		return;
	}

	u32 stepSize = 15;
	int numKeyframeEntries = m_keyframe.GetNumEntries();
	if (numKeyframeEntries < (int)stepSize) 
	{
		bkRemotePacket packet;
		packet.Begin(KEYFRAME_FULL_UPDATE, GetGuid(), this);
		packet.Write_u32(numKeyframeEntries);

		for (int i=0; i<numKeyframeEntries; i++)
		{
			float time = m_keyframe.GetTime(i);
			Vec4V vValue = m_keyframe.GetValue(i);
			packet.Write_float(time);
			packet.Write_float(vValue.GetXf());
			packet.Write_float(vValue.GetYf());
			packet.Write_float(vValue.GetZf());
			packet.Write_float(vValue.GetWf());
		}
		packet.Send();
	}
	else 
	{
		bkRemotePacket beginPacket;
		beginPacket.Begin(KEYFRAME_PARTIAL_KEYS_BEGIN, GetGuid(), this);
		beginPacket.Write_u32(numKeyframeEntries);
		beginPacket.Send();

		for (u32 group=0; group<(u32)numKeyframeEntries; group+=stepSize)
		{
			bkRemotePacket groupPacket;
			groupPacket.Begin(KEYFRAME_PARTIAL_KEYS, GetGuid(), this);
			u32 groupEnd = Min(numKeyframeEntries - group, stepSize)+group;
			groupPacket.Write_u32(group);
			groupPacket.Write_u32(groupEnd);

			for (int i=group; i<(int)groupEnd; i++) 
			{
				float time = m_keyframe.GetTime(i);
				Vec4V vValue = m_keyframe.GetValue(i);
				groupPacket.Write_float(time);
				groupPacket.Write_float(vValue.GetXf());
				groupPacket.Write_float(vValue.GetYf());
				groupPacket.Write_float(vValue.GetZf());
				groupPacket.Write_float(vValue.GetWf());
			}

			groupPacket.Send();
		}

		bkRemotePacket endPacket;
		endPacket.Begin(KEYFRAME_PARTIAL_KEYS_END, GetGuid(), this);
		endPacket.Send();
	}

}

void ptxKeyframeWidget::UpdateUIData()
{
	if (bkRemotePacket::IsConnected()==false)
	{
		return;
	}

	ptxKeyframeDefn* pKeyframeDefn = m_keyframe.GetDefn();

	bkRemotePacket packet;
	packet.Begin(UI_UPDATE_INFO, GetGuid(), this);

	for (int i=0; i<4; i++)
	{
		if (pKeyframeDefn->m_labels[i]) 
		{
			packet.Write_const_char(pKeyframeDefn->m_labels[i]);
		}
		else 
		{
			packet.Write_const_char("");
		}

		packet.Write_u8(pKeyframeDefn->m_colours[i].GetAlpha());
		packet.Write_u8(pKeyframeDefn->m_colours[i].GetRed());
		packet.Write_u8(pKeyframeDefn->m_colours[i].GetGreen());
		packet.Write_u8(pKeyframeDefn->m_colours[i].GetBlue());
	}

	packet.Write_float(pKeyframeDefn->m_vMinMaxDeltaY.GetXf());
	packet.Write_float(pKeyframeDefn->m_vMinMaxDeltaY.GetYf());
	packet.Write_float(pKeyframeDefn->m_vMinMaxDeltaX.GetXf());
	packet.Write_float(pKeyframeDefn->m_vMinMaxDeltaX.GetYf());
	packet.Write_float(pKeyframeDefn->m_vMinMaxDeltaX.GetZf());
	packet.Write_float(pKeyframeDefn->m_vMinMaxDeltaY.GetZf());
	
	packet.Send();
}

int ptxKeyframeWidget::DrawLocal(int x, int y) 
{
	Drawf(x, y, m_Title);
	return bkWidget::DrawLocal(x, y);
}

void ptxKeyframeWidget::RemoteHandler(const bkRemotePacket& packet)
{
	switch(packet.GetCommand())
	{
	case bkRemotePacket::CREATE:
		{
			Quitf("Remote creation not supported");
		}
		break;

	case KEYFRAME_FULL_UPDATE:
		{
			packet.Begin();
			ptxKeyframeWidget* pKeyframeWidget = packet.ReadWidget<ptxKeyframeWidget>();
			if (pKeyframeWidget==NULL) 
			{
				return;
			}

			int newSize = packet.Read_u32();
			pKeyframeWidget->m_keyframe.Reset(newSize);
			for (int i=0; i<newSize; i++) 
			{
				float time = packet.Read_float();

				Vec4V vec;
				vec.SetXf(packet.Read_float());
				vec.SetYf(packet.Read_float());
				vec.SetZf(packet.Read_float());
				vec.SetWf(packet.Read_float());

				pKeyframeWidget->m_keyframe.SetEntry(i, time, vec);
			}

			packet.End();
			pKeyframeWidget->m_keyframe.ComputeDeltas();
			pKeyframeWidget->Changed();
		}
		break;

	case KEYFRAME_PARTIAL_KEYS_BEGIN:
		{
			packet.Begin();
			ptxKeyframeWidget* pKeyframeWidget = packet.ReadWidget<ptxKeyframeWidget>();
			if (pKeyframeWidget==NULL) 
			{
				return;
			}

			int newSize = packet.Read_u32();
			pKeyframeWidget->m_keyframe.Reset(newSize);
			packet.End();
		}
		break;

	case KEYFRAME_PARTIAL_KEYS:
		{
			packet.Begin();
			ptxKeyframeWidget* pKeyframeWidget = packet.ReadWidget<ptxKeyframeWidget>();
			if (pKeyframeWidget==NULL) 
			{
				return;
			}

			int startKey = packet.Read_u32();
			int endKey = packet.Read_u32();

			ptxAssert(startKey<endKey);
			ptxAssert(endKey<=pKeyframeWidget->m_keyframe.GetNumEntries());

			for (int i=startKey; i<endKey; i++) 
			{
				float time = packet.Read_float();

				Vec4V vec;
				vec.SetXf(packet.Read_float());
				vec.SetYf(packet.Read_float());
				vec.SetZf(packet.Read_float());
				vec.SetWf(packet.Read_float());
				
				pKeyframeWidget->m_keyframe.SetEntry(i, time, vec);
			}

			packet.End();
		}
		break;

	case KEYFRAME_PARTIAL_KEYS_END:
		{
			packet.Begin();
			ptxKeyframeWidget* pKeyframeWidget = packet.ReadWidget<ptxKeyframeWidget>();
			if (pKeyframeWidget==NULL) 
			{
				return;
			}

			pKeyframeWidget->m_keyframe.ComputeDeltas();	
			packet.End();
			pKeyframeWidget->Changed();
		}
		break;
	}
}

#if __WIN32PC
void ptxKeyframeWidget::WindowCreate() 
{
	if (bkRemotePacket::IsConnectedToRag()==false)
	{
		GetPane()->AddLastWindow(this, "STATIC", 0, SS_LEFT|SS_NOPREFIX);
	}
}
#endif


#endif // RMPTFX_BANK

