// 
// rmptfx/ptxd_drawmodel.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "ptxd_drawmodel.h"
#include "ptxd_drawmodel_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxparticlerule.h"

#include "system/cache.h"
#if __OPENGL || __D3D
#include "grmodel/shaderfx.h"
#endif

#include "system/timer.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{


// code
#if RMPTFX_XML_LOADING
ptxd_Model::ptxd_Model()
{
	// create keyframes

	// set default data
	SetDefaultData();
}
#endif

void ptxd_Model::SetDefaultData()
{
	// default keyframes

	// default other data
	m_cameraShrink = 2.0f;
	m_disableDraw = false;
	m_shadowCastIntensity = 0.0f;
}

ptxd_Model::ptxd_Model(datResource& rsc)
	: ptxBehaviour(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxd_Model_num++;
#endif
}

#if __DECLARESTRUCT
void ptxd_Model::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxd_Model, ptxBehaviour)
		SSTRUCT_FIELD_AS(ptxd_Model, m_colourControlShaderId, int)
		SSTRUCT_FIELD(ptxd_Model, m_cameraShrink)
		SSTRUCT_FIELD(ptxd_Model, m_shadowCastIntensity)
		SSTRUCT_FIELD(ptxd_Model, m_disableDraw)
		SSTRUCT_IGNORE(ptxd_Model, m_pad)
	SSTRUCT_END(ptxd_Model)
}
#endif

IMPLEMENT_PLACE(ptxd_Model);

void ptxd_Model::DrawPoints(ptxEffectInst* RESTRICT pEffectInst, 
							ptxEmitterInst* RESTRICT pEmitterInst, 
							ptxParticleRule* RESTRICT pParticleRule, 
							ptxEvolutionList* RESTRICT UNUSED_PARAM(pEvoList)
							PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface))
{
	ptxAssertf(pEmitterInst, "missing emitter instance");
	//PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY(pMultipleDrawInterface);

#if RMPTFX_EDITOR
	sysTimer timer;
	timer.Reset();
#endif

	if(m_disableDraw)
	{
		return;
	}

#if RMPTFX_USE_PARTICLE_SHADOWS
	//If shadow cast intensity is really low, lets skip it
	if(RMPTFXMGR.IsShadowPass() && (m_shadowCastIntensity * RMPTFXMGR.GetGlobalShadowIntensity()) < RMPTFX_MIN_PARTICLE_SHADOW_INTENSITY)
	{
		return;
	}
#endif

	// return if there are no points to draw
	const unsigned drawBufferId = RMPTFXTHREADER.GetDrawBufferId();
	if (pEmitterInst->GetPointList().GetNumItemsMT(drawBufferId)==0)
	{
		return;
	}

	// sort the points
	unsigned numPoints = pEmitterInst->GetNumActivePointsMT(drawBufferId);
	ptxPointItem** ppPoints = Alloca(ptxPointItem*, numPoints);
	const unsigned numPoints2 = pEmitterInst->SortPoints(ppPoints);
	Assert(numPoints >= numPoints2);
	numPoints = numPoints2;

	// set the new render state

	grcRasterizerStateHandle RS_Prev = grcStateBlock::RS_Active;
	grcBlendStateHandle BS_Prev = grcStateBlock::BS_Active;
	grcDepthStencilStateHandle DSS_Prev = grcStateBlock::DSS_Active;
	pParticleRule->SetRenderState(pParticleRule->GetShaderInst().GetProjectionMode());

	grmModel::SetForceShader(0);
	grcViewport::SetCurrentWorldIdentity();
	

	// get the force colour shader var id
	m_colourControlShaderId = grcEffect::LookupGlobalVar(RMPTFXMGR.GetForcedColourShaderVarName(), false);

	// do any pre draw only if the fx instance is not sorted - i.e. no PTXEFFECTFLAG_MANUAL_DRAW flag,
	// as in that case pre draw setup has already been done during the regular render list building.
	if (!pEffectInst->GetFlag(PTXEFFECTFLAG_MANUAL_DRAW) && pEffectInst->GetRenderSetup()) 
	{ 
		Vec3V vBBMin = pEmitterInst->GetDataDBMT(drawBufferId)->GetCurrBoundBoxMin();
		Vec3V vBBMax = pEmitterInst->GetDataDBMT(drawBufferId)->GetCurrBoundBoxMax();

		if (ptxVerifyf(IsLessThanOrEqualAll(vBBMin, vBBMax), "%s - %s - min bound box is greater than max (min:%.3f, %.3f, %.3f max:%.3f, %.3f, %.3f", pEffectInst->GetEffectRule()->GetName(), pEmitterInst->GetEmitterRule()->GetName(), vBBMin.GetXf(), vBBMin.GetYf(), vBBMin.GetZf(), vBBMax.GetXf(), vBBMax.GetYf(), vBBMax.GetZf()))
		{
			if (!pEffectInst->GetRenderSetup()->PreDraw(pEffectInst, pEmitterInst, vBBMin, vBBMax, pParticleRule->GetShaderInst().GetShaderTemplateHashName(), pParticleRule->GetShaderInst().GetGrmShader()))
			{
				ptxWarningf("Error in Particle Setup for %s , aborting rendering particles", pParticleRule->GetShaderInst().GetShaderTemplateName());
				return;
			}
		}
	}

	// cache some data
	Vec3V vCamPos;
#if RMPTFX_USE_PARTICLE_SHADOWS
	if (RMPTFXMGR.IsShadowPass())
		vCamPos = RMPTFXMGR.GetShadowSourcePos();
	else
#endif
		vCamPos = grcViewport::GetCurrent()->GetCameraMtx().GetCol3();

	ScalarV vCameraShrink = ScalarVFromF32(m_cameraShrink);
	ScalarV vOne(V_ONE);

	// draw the particle
	for (unsigned i=0; i<numPoints; ++i)
	{
		ptxPointItem* pCurrPointItem = ppPoints[i];
		if (i+1 < numPoints)
		{
			PrefetchDC(ppPoints[i+1]);
		}

		// check that there is a model to draw
		if (pParticleRule->GetNumDrawables()>0)
		{
			// get the particle data
			ptxPointDB* pPointDB = pCurrPointItem->GetDataDBMT(drawBufferId);

			// get the drawable
			s32 drawableIdx = pPointDB->GetInitTexFrameId();
			if (ptxVerifyf(drawableIdx<pParticleRule->GetNumDrawables(), "particle has an out of range drawable index - skipping rendering (particle rule - %s, num drawables - %d, drawable idx - %d", pParticleRule->GetName(), pParticleRule->GetNumDrawables(), drawableIdx))
			{
				ptxDrawable* pPtxDrawable = &pParticleRule->GetDrawable(drawableIdx); 
				rmcDrawable* pDrawable = pPtxDrawable->GetDrawable();
				if (pDrawable)
				{
					// calc the camera shrink scale factor
					ScalarV vPointDistToCam = Dist(pPointDB->GetCurrPos(), vCamPos);
					ScalarV vCamShrinkScalar = SelectFT(IsLessThanOrEqual(vPointDistToCam, vCameraShrink), vOne, vPointDistToCam/vCameraShrink);

					// get the drawable dimensions
					Vec3V vBBMin, vBBMax;
					pDrawable->GetLodGroup().GetBoundingBox(RC_VECTOR3(vBBMin), RC_VECTOR3(vBBMax));
					Vec3V vBBSize = vBBMax-vBBMin;

					// calculate the particle matrix
					// note that we are using actual dimensions here instead of scaling the original model
					// this is necessary so any collision behaviour can determine the actual size of the model to do more accurate collision
					Mat34V vPtxMtx = Mat34V(V_IDENTITY);
					Scale(vPtxMtx, vPtxMtx, (pPointDB->GetDimensions().GetXYZ()/vBBSize)*vCamShrinkScalar);

					Mat34V vRotMtx;
					Mat34VFromQuatV(vRotMtx, pPointDB->GetCurrRot());
					Transform3x3(vPtxMtx, vRotMtx, vPtxMtx);

					vPtxMtx.SetCol3(pPointDB->GetCurrPos());

					// set the particle colour
					if (m_colourControlShaderId != grcegvNONE)
					{
						Vec4V finalCol = pPointDB->GetColour().GetRGBA();

#if RMPTFX_USE_PARTICLE_SHADOWS
						//if we're drawing the shadows, adjust the alpha based on the shadow cast intensity
						if (RMPTFXMGR.IsShadowPass())
						{
							finalCol.SetW(finalCol.GetW() * ScalarVFromF32(m_shadowCastIntensity * RMPTFXMGR.GetGlobalShadowIntensity()));
						}
#endif

						finalCol.SetW(finalCol.GetW()*vCamShrinkScalar);

						grcEffect::SetGlobalVar(m_colourControlShaderId, finalCol);
					}

					// draw the model

#if PTXDRAW_MULTIPLE_DRAWS_PER_BATCH
					if (pMultipleDrawInterface)
					{
#if GS_INSTANCED_SHADOWS
						if (pMultipleDrawInterface->IsUsingInstanced())
						{
							pMultipleDrawInterface->PreDrawInstanced();
							pParticleRule->GetDrawable(pPointDB->GetInitTexFrameId()).GetDrawable()->Draw(RCC_MATRIX34(vPtxMtx), 0xFFFFFFFF, 0, 0);
							pMultipleDrawInterface->PostDrawInstanced();
						}
						else
#endif
						{
							for(u32 i=0; i<pMultipleDrawInterface->NoOfDraws(); i++)
							{
								pMultipleDrawInterface->PreDraw(i);
								// draw the model
								pParticleRule->GetDrawable(pPointDB->GetInitTexFrameId()).GetDrawable()->Draw(RCC_MATRIX34(vPtxMtx), 0xFFFFFFFF, 0, 0);
								pMultipleDrawInterface->PostDraw(i);
							}
						}
					}
					else
						pParticleRule->GetDrawable(pPointDB->GetInitTexFrameId()).GetDrawable()->Draw(RCC_MATRIX34(vPtxMtx), 0xFFFFFFFF, 0, 0);
#else
					pParticleRule->GetDrawable(pPointDB->GetInitTexFrameId()).GetDrawable()->Draw(RCC_MATRIX34(vPtxMtx), 0xFFFFFFFF, 0, 0);
#endif
				}
			}
		}
	}


	// stop the shader being forced and reset the shader var
	grmModel::SetForceShader(0);
	if (m_colourControlShaderId != grcegvNONE)
	{
		grcEffect::SetGlobalVar(m_colourControlShaderId, Vec4V(1.0f, 1.0f, 1.0f, 1.0f));
	}

	grcStateBlock::SetRasterizerState(RS_Prev);
	grcStateBlock::SetBlendState(BS_Prev);
	grcStateBlock::SetDepthStencilState(DSS_Prev);

	// do any post draw only if the fx instance is not sorted - i.e. no PTXEFFECTFLAG_MANUAL_DRAW flag,
	// as in that case pre draw setup has already been done during the regular render list building.
	if (!pEffectInst->GetFlag(PTXEFFECTFLAG_MANUAL_DRAW) && pEffectInst->GetRenderSetup())
	{
		pEffectInst->GetRenderSetup()->PostDraw(pEffectInst, pEmitterInst);
	}

	// update the editor cpu draw time
#if RMPTFX_EDITOR
	pEmitterInst->GetEmitterRule()->GetUIData().IncAccumCPUDrawTime(timer.GetMsTime()); 
#endif
}

// editor code
#if RMPTFX_EDITOR
void ptxd_Model::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 3);

	//option For Disable
	SendBoolDefnToEditor(buff, "Disable Draw");

	// send keyframes

	// send non-keyframes
	SendFloatDefnToEditor(buff, "Camera Shrink", 0.0f, 1000.0f);
	SendFloatDefnToEditor(buff, "Shadow Cast Intensity", 0.0f, 1.0f);
}

void ptxd_Model::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	//option For Disable
	SendBoolToEditor(buff, m_disableDraw);

	// send keyframes

	// send non-keyframes
	SendFloatToEditor(buff, m_cameraShrink);
	SendFloatToEditor(buff, m_shadowCastIntensity);
}

void ptxd_Model::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	//option For Disable
	ReceiveBoolFromEditor(buff, m_disableDraw);

	// receive keyframes

	// receive non-keyframes
	ReceiveFloatFromEditor(buff, m_cameraShrink);
	ReceiveFloatFromEditor(buff, m_shadowCastIntensity);
}
#endif // RMPTFX_EDITOR


} // namespace rage
