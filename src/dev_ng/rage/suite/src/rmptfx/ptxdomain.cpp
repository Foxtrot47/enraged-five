// 
// rmptfx/ptxdomain.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxdomain.h"
#include "rmptfx/ptxdomain_parser.h"

// includes (rmptfx)
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxemitterrule.h"
#include "rmptfx/ptxinterface.h"
#include "rmptfx/ptxmanager.h"

// includes (rage)
#include "grcore/debugdraw.h"
#include "math/random.h"
#include "vector/colors.h"
#include "math/vecmath.h"

// optimisations
RMPTFX_OPTIMISATIONS()



// namespaces
using namespace rage;


// defines
#define PTXDOMAIN_FILE_VERSION				(2.1f)

#if RMPTFX_BANK
#define PTXDOMAIN_EMITTER_COL_OUTER			(Color32(255, 128,  0,  255))
#define PTXDOMAIN_EMITTER_COL_INNER			(Color32(128,  64,  0,  255))
#define PTXDOMAIN_VELOCITY_COL_OUTER		(Color32(  0, 255, 128, 255))
#define PTXDOMAIN_VELOCITY_COL_INNER		(Color32(  0, 128,  64, 255))
#define PTXDOMAIN_VELOCITY_COL_OUTER_PR		(Color32(128,   0, 255, 255))
#define PTXDOMAIN_VELOCITY_COL_INNER_PR		(Color32( 64,   0, 128, 255))
#define PTXDOMAIN_ATTRACTOR_COL_NEAR		(Color32(255,   0, 128, 255))
#define PTXDOMAIN_ATTRACTOR_COL_FAR			(Color32(128,   0,  64, 255))
#define PTXDOMAIN_ATTRACTOR_COL_ORBIT		(Color32(128, 255,   0, 255))
#endif


// static variables
ptxKeyframeDefn ptxDomain::sm_propertyDefns[PTXDOMAIN_TYPE_NUM][PTXDOMAIN_SHAPE_NUM][PTXDOMAIN_PROP_NUM];


// code
ptxDomainInst::ptxDomainInst()
{
	m_vCurrPos = Vec3V(V_ZERO);
	m_vCurrRot = Vec3V(V_ZERO);
	m_vCurrSizeOuter = Vec3V(V_ZERO);
	m_vCurrSizeInner = Vec3V(V_ZERO);
}

void ptxDomainInst::Update(ptxDomainType domainType, ptxDomain* pDomain, ScalarV_In vKeyTimeEmitter, Vec3V_In vScale, ptxEvolutionList* pEvolutionList, ptxEvoValueType* pEvoValues, Vec3V_In overrideSize)
{
	// get the keyframe data
	Vec4V vPosition = pDomain->GetPositionKFP().Query(vKeyTimeEmitter, pEvolutionList, pEvoValues);
	Vec4V vRotation = pDomain->GetRotationKFP().Query(vKeyTimeEmitter, pEvolutionList, pEvoValues);
	const Vec4V vSizeOuter = pDomain->GetSizeOuterKFP().Query(vKeyTimeEmitter, pEvolutionList, pEvoValues);
	Vec4V vSizeInner = pDomain->GetSizeInnerKFP().Query(vKeyTimeEmitter, pEvolutionList, pEvoValues);

	// make sure that the inner size is never larger than the outer size
	if (domainType!=PTXDOMAIN_TYPE_ATTRACTOR)
	{
		vSizeInner = Min(vSizeOuter, vSizeInner);
	}

	vPosition = vPosition * Vec4V(vScale, ScalarV(V_ONE));
	Vec3V vCurrSizeOuter = vSizeOuter.GetXYZ() * vScale;
	Vec3V vCurrSizeInner = vSizeInner.GetXYZ() * vScale;


	// rescale if size is overridden
	if (IsZeroAll(overrideSize)==false)
	{
		vCurrSizeOuter = Scale(overrideSize, vScale);

		// scale the inner size to be proportional to the size
		ptxAssertf(IsZeroAll(vSizeOuter)==false, "can't override the creation domain size when the outer size is zero");
		vCurrSizeInner = InvScale(vCurrSizeOuter, vSizeOuter.GetXYZ())*vSizeInner.GetXYZ();
	}

	// set the current state
	m_vCurrPos = vPosition.GetXYZ();
	m_vCurrRot = vRotation.GetXYZ();
	m_vCurrSizeOuter = vCurrSizeOuter;
	m_vCurrSizeInner = vCurrSizeInner;
}




ptxDomain::ptxDomain(ptxDomainType type, ptxDomainShape shape)
{	
	m_type = type;
	m_shape = shape;

	m_isWorldSpace = false;
	m_isPointRelative = false;
	m_isCreationRelative = false;
	m_isTargetRelative = false;

	m_fileVersion = PTXDOMAIN_FILE_VERSION;

	// init keyframes
	switch(m_type)
	{
	case PTXDOMAIN_TYPE_CREATION:
		m_positionKFP.Init(Vec4V(V_ZERO), atHashValue("ptxCreationDomain:m_positionKFP"));
		m_rotationKFP.Init(Vec4V(V_ZERO), atHashValue("ptxCreationDomain:m_rotationKFP"));
		m_sizeInnerKFP.Init(Vec4V(V_ZERO), atHashValue("ptxCreationDomain:m_sizeInnerKFP"));
		m_sizeOuterKFP.Init(Vec4V(V_ONE), atHashValue("ptxCreationDomain:m_sizeOuterKFP"));
		break;
	case PTXDOMAIN_TYPE_TARGET:
		m_positionKFP.Init(Vec4V(V_ZERO), atHashValue("ptxTargetDomain:m_positionKFP"));
		m_rotationKFP.Init(Vec4V(V_ZERO), atHashValue("ptxTargetDomain:m_rotationKFP"));
		m_sizeInnerKFP.Init(Vec4V(V_ZERO), atHashValue("ptxTargetDomain:m_sizeInnerKFP"));
		m_sizeOuterKFP.Init(Vec4V(V_ONE), atHashValue("ptxTargetDomain:m_sizeOuterKFP"));
		break;
	case PTXDOMAIN_TYPE_ATTRACTOR:
		m_positionKFP.Init(Vec4V(V_ZERO), atHashValue("ptxAttractorDomain:m_positionKFP"));
		m_rotationKFP.Init(Vec4V(V_ZERO), atHashValue("ptxAttractorDomain:m_rotationKFP"));
		m_sizeInnerKFP.Init(Vec4V(V_ZERO), atHashValue("ptxAttractorDomain:m_sizeInnerKFP"));
		m_sizeOuterKFP.Init(Vec4V(V_ONE), atHashValue("ptxAttractorDomain:m_sizeOuterKFP"));
		break;
	};

	// init keyframe property list
	m_keyframePropList.AddKeyframeProp(&m_positionKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_rotationKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_sizeInnerKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_sizeOuterKFP, NULL);
}

ptxDomain::ptxDomain(datResource& rsc)
: m_positionKFP(rsc)
, m_rotationKFP(rsc)
, m_sizeOuterKFP(rsc)
, m_sizeInnerKFP(rsc)
, m_keyframePropList(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxDomain_num++;
#endif
}

#if __DECLARESTRUCT
void ptxDomain::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN_BASE(ptxDomain, datBase)
		SSTRUCT_FIELD(ptxDomain, m_type)
		SSTRUCT_FIELD(ptxDomain, m_shape)
		SSTRUCT_FIELD(ptxDomain, m_isWorldSpace)
		SSTRUCT_FIELD(ptxDomain, m_isPointRelative)
		SSTRUCT_FIELD(ptxDomain, m_isCreationRelative)
		SSTRUCT_FIELD(ptxDomain, m_isTargetRelative)
		SSTRUCT_FIELD(ptxDomain, m_positionKFP)
		SSTRUCT_FIELD(ptxDomain, m_rotationKFP)
		SSTRUCT_FIELD(ptxDomain, m_sizeOuterKFP)
		SSTRUCT_FIELD(ptxDomain, m_sizeInnerKFP)
		SSTRUCT_FIELD(ptxDomain, m_fileVersion)
		SSTRUCT_FIELD(ptxDomain, m_keyframePropList)
		SSTRUCT_IGNORE(ptxDomain, m_pad2)
	SSTRUCT_END(ptxDomain)
}
#endif

void ptxDomain::Place(ptxDomain* pDomain, datResource& rsc)
{
	if (pDomain->m_shape==PTXDOMAIN_SHAPE_BOX)
	{
		ptxDomainBox* pDomainBox = reinterpret_cast<ptxDomainBox*>(pDomain);
		ptxDomainBox::Place(pDomainBox, rsc);
	}
	else if (pDomain->m_shape==PTXDOMAIN_SHAPE_SPHERE)
	{
		ptxDomainSphere* pDomainSphere = reinterpret_cast<ptxDomainSphere*>(pDomain);
		ptxDomainSphere::Place(pDomainSphere, rsc);
	}
	else if (pDomain->m_shape==PTXDOMAIN_SHAPE_CYLINDER)
	{
		ptxDomainCylinder* pDomainCylinder = reinterpret_cast<ptxDomainCylinder*>(pDomain);
		ptxDomainCylinder::Place(pDomainCylinder, rsc);
	}
	else if (pDomain->m_shape==PTXDOMAIN_SHAPE_ATTRACTOR)
	{
		ptxDomainAttractor* pDomainAttractor = reinterpret_cast<ptxDomainAttractor*>(pDomain);
		ptxDomainAttractor::Place(pDomainAttractor, rsc);
	}
	else
	{
		ptxErrorf("invalid domain type");
	}
}

void ptxDomain::InitKeyframeDefns()
{
	// box domains
	//																														name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_POSITION]				= ptxKeyframeDefn(	"Position",					atHashValue("ptxCreationDomain:m_positionKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-500.0f,	500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_ROTATION]				= ptxKeyframeDefn(	"Rotation",					atHashValue("ptxCreationDomain:m_rotationKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_SIZE_OUTER]			= ptxKeyframeDefn(	"Outer Size",				atHashValue("ptxCreationDomain:m_sizeOuterKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_SIZE_INNER]			= ptxKeyframeDefn(	"Inner Size",				atHashValue("ptxCreationDomain:m_sizeInnerKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ONE),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"X",			"Y",			"Z");

	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_POSITION]				= ptxKeyframeDefn(	"Position",					atHashValue("ptxTargetDomain:m_positionKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-500.0f,	500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_ROTATION]				= ptxKeyframeDefn(	"Rotation",					atHashValue("ptxTargetDomain:m_rotationKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_SIZE_OUTER]				= ptxKeyframeDefn(	"Outer Size",				atHashValue("ptxTargetDomain:m_sizeOuterKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_SIZE_INNER]				= ptxKeyframeDefn(	"Inner Size",				atHashValue("ptxTargetDomain:m_sizeInnerKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ONE),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"X",			"Y",			"Z");

	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_POSITION]			= ptxKeyframeDefn(	"Position",					atHashValue("ptxAttractorDomain:m_positionKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-500.0f,	500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_ROTATION]			= ptxKeyframeDefn(	"Rotation",					atHashValue("ptxAttractorDomain:m_rotationKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_SIZE_OUTER]			= ptxKeyframeDefn(	"Outer Size",				atHashValue("ptxAttractorDomain:m_sizeOuterKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_SIZE_INNER]			= ptxKeyframeDefn(	"Inner Size",				atHashValue("ptxAttractorDomain:m_sizeInnerKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ONE),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"X",			"Y",			"Z");

	// sphere domains
	//																														name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_POSITION] 			= ptxKeyframeDefn(	"Position",					atHashValue("ptxCreationDomain:m_positionKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-500.0f,	500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_ROTATION] 			= ptxKeyframeDefn(	"Rotation",					atHashValue("ptxCreationDomain:m_rotationKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_SIZE_OUTER] 		= ptxKeyframeDefn(	"Outer Size",				atHashValue("ptxCreationDomain:m_sizeOuterKFP"),			PTXKEYFRAMETYPE_FLOAT,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"Outer Radius");
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_SIZE_INNER] 		= ptxKeyframeDefn(	"Inner Size",				atHashValue("ptxCreationDomain:m_sizeInnerKFP"),			PTXKEYFRAMETYPE_FLOAT,		Vec4V(V_ONE),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"Inner Radius");

	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_POSITION] 			= ptxKeyframeDefn(	"Position",					atHashValue("ptxTargetDomain:m_positionKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-500.0f,	500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_ROTATION] 			= ptxKeyframeDefn(	"Rotation",					atHashValue("ptxTargetDomain:m_rotationKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_SIZE_OUTER] 			= ptxKeyframeDefn(	"Outer Size",				atHashValue("ptxTargetDomain:m_sizeOuterKFP"),				PTXKEYFRAMETYPE_FLOAT,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"Outer Radius");
	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_SIZE_INNER] 			= ptxKeyframeDefn(	"Inner Size",				atHashValue("ptxTargetDomain:m_sizeInnerKFP"),				PTXKEYFRAMETYPE_FLOAT,		Vec4V(V_ONE),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"Inner Radius");

	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_POSITION] 		= ptxKeyframeDefn(	"Position",					atHashValue("ptxAttractorDomain:m_positionKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-500.0f,	500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_ROTATION] 		= ptxKeyframeDefn(	"Rotation",					atHashValue("ptxAttractorDomain:m_rotationKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_SIZE_OUTER] 		= ptxKeyframeDefn(	"Outer Size",				atHashValue("ptxAttractorDomain:m_sizeOuterKFP"),			PTXKEYFRAMETYPE_FLOAT,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"Outer Radius");
	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_SIZE_INNER] 		= ptxKeyframeDefn(	"Inner Size",				atHashValue("ptxAttractorDomain:m_sizeInnerKFP"),			PTXKEYFRAMETYPE_FLOAT,		Vec4V(V_ONE),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"Inner Radius");

	// cylinder domains
	//																														name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_POSITION] 		= ptxKeyframeDefn(	"Position",					atHashValue("ptxCreationDomain:m_positionKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-500.0f,	500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_ROTATION] 		= ptxKeyframeDefn(	"Rotation",					atHashValue("ptxCreationDomain:m_rotationKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_SIZE_OUTER] 		= ptxKeyframeDefn(	"Outer Size",				atHashValue("ptxCreationDomain:m_sizeOuterKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"X",			"Length",		"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_SIZE_INNER] 		= ptxKeyframeDefn(	"Inner Size",				atHashValue("ptxCreationDomain:m_sizeInnerKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ONE),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"X",			"Length",		"Z");

	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_POSITION] 			= ptxKeyframeDefn(	"Position",					atHashValue("ptxTargetDomain:m_positionKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-500.0f,	500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_ROTATION] 			= ptxKeyframeDefn(	"Rotation",					atHashValue("ptxTargetDomain:m_rotationKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_SIZE_OUTER] 		= ptxKeyframeDefn(	"Outer Size",				atHashValue("ptxTargetDomain:m_sizeOuterKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"X",			"Length",		"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_SIZE_INNER] 		= ptxKeyframeDefn(	"Inner Size",				atHashValue("ptxTargetDomain:m_sizeInnerKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ONE),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"X",			"Length",		"Z");

	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_POSITION] 		= ptxKeyframeDefn(	"Position",					atHashValue("ptxAttractorDomain:m_positionKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-500.0f,	500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_ROTATION] 		= ptxKeyframeDefn(	"Rotation",					atHashValue("ptxAttractorDomain:m_rotationKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_SIZE_OUTER] 	= ptxKeyframeDefn(	"Outer Size",				atHashValue("ptxAttractorDomain:m_sizeOuterKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"X",			"Length",		"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_SIZE_INNER] 	= ptxKeyframeDefn(	"Inner Size",				atHashValue("ptxAttractorDomain:m_sizeInnerKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ONE),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"X",			"Length",		"Z");

	// attractor domains
	//																														name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_POSITION] 		= ptxKeyframeDefn(	"Position",					atHashValue("ptxCreationDomain:m_positionKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-500.0f,	500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_ROTATION] 		= ptxKeyframeDefn(	"Rotation",					atHashValue("ptxCreationDomain:m_rotationKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_SIZE_OUTER] 	= ptxKeyframeDefn(	"Outer Size",				atHashValue("ptxCreationDomain:m_sizeOuterKFP"),			PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"Radius",		"Length");
	sm_propertyDefns[PTXDOMAIN_TYPE_CREATION][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_SIZE_INNER] 	= ptxKeyframeDefn(	"Inner Size",				atHashValue("ptxCreationDomain:m_sizeInnerKFP"),			PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ONE),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"Radius",		"Orbit Radius");

	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_POSITION] 		= ptxKeyframeDefn(	"Position",					atHashValue("ptxTargetDomain:m_positionKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-500.0f,	500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_ROTATION] 		= ptxKeyframeDefn(	"Rotation",					atHashValue("ptxTargetDomain:m_rotationKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_SIZE_OUTER] 		= ptxKeyframeDefn(	"Outer Size",				atHashValue("ptxTargetDomain:m_sizeOuterKFP"),				PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"Radius",		"Length");
	sm_propertyDefns[PTXDOMAIN_TYPE_TARGET][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_SIZE_INNER] 		= ptxKeyframeDefn(	"Inner Size",				atHashValue("ptxTargetDomain:m_sizeInnerKFP"),				PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ONE),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"Radius",		"Orbit Radius");

	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_POSITION] 		= ptxKeyframeDefn(	"Position",					atHashValue("ptxAttractorDomain:m_positionKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-500.0f,	500.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_ROTATION] 		= ptxKeyframeDefn(	"Rotation",					atHashValue("ptxAttractorDomain:m_rotationKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X",			"Y",			"Z");
	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_SIZE_OUTER] 	= ptxKeyframeDefn(	"Outer Size",				atHashValue("ptxAttractorDomain:m_sizeOuterKFP"),			PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"Radius",		"Length");
	sm_propertyDefns[PTXDOMAIN_TYPE_ATTRACTOR][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_SIZE_INNER] 	= ptxKeyframeDefn(	"Inner Size",				atHashValue("ptxAttractorDomain:m_sizeInnerKFP"),			PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ONE),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		500.0f,		0.01f),		"Radius",		"Orbit Radius");
}

ptxDomain* ptxDomain::CreateDomain(ptxDomainType type, ptxDomainShape shape)
{
	ptxDomain* pDomain = NULL;

	switch (shape)
	{
	case PTXDOMAIN_SHAPE_BOX: 
		pDomain = rage_new ptxDomainBox(type);
		break;
	case PTXDOMAIN_SHAPE_SPHERE: 
		pDomain = rage_new ptxDomainSphere(type);
		break;
	case PTXDOMAIN_SHAPE_CYLINDER:
		pDomain = rage_new ptxDomainCylinder(type);
		break;
	case PTXDOMAIN_SHAPE_ATTRACTOR:
		pDomain = rage_new ptxDomainAttractor(type);
		break;
	default: 
		ptxAssertf(0, "invalid domain shape");
		break;
	}

	return pDomain;
}

void ptxDomain::OnPreLoad(parTreeNode* UNUSED_PARAM(pNode))
{
}

void ptxDomain::OnPostLoad()
{
	// this needs done here as when the domain is constructed from the parser, the type hasn't been set up yet so we don't know how to initialise the hash ids
	switch(m_type)
	{
	case PTXDOMAIN_TYPE_CREATION:
		m_positionKFP.SetPropertyId(atHashValue("ptxCreationDomain:m_positionKFP"));
		m_rotationKFP.SetPropertyId(atHashValue("ptxCreationDomain:m_rotationKFP"));
		m_sizeInnerKFP.SetPropertyId(atHashValue("ptxCreationDomain:m_sizeInnerKFP"));
		m_sizeOuterKFP.SetPropertyId(atHashValue("ptxCreationDomain:m_sizeOuterKFP"));
		break;						 
	case PTXDOMAIN_TYPE_TARGET:		 
		m_positionKFP.SetPropertyId(atHashValue("ptxTargetDomain:m_positionKFP"));
		m_rotationKFP.SetPropertyId(atHashValue("ptxTargetDomain:m_rotationKFP"));
		m_sizeInnerKFP.SetPropertyId(atHashValue("ptxTargetDomain:m_sizeInnerKFP"));
		m_sizeOuterKFP.SetPropertyId(atHashValue("ptxTargetDomain:m_sizeOuterKFP"));
		break;				 
	case PTXDOMAIN_TYPE_ATTRACTOR:		 
		m_positionKFP.SetPropertyId(atHashValue("ptxAttractorDomain:m_positionKFP"));
		m_rotationKFP.SetPropertyId(atHashValue("ptxAttractorDomain:m_rotationKFP"));
		m_sizeInnerKFP.SetPropertyId(atHashValue("ptxAttractorDomain:m_sizeInnerKFP"));
		m_sizeOuterKFP.SetPropertyId(atHashValue("ptxAttractorDomain:m_sizeOuterKFP"));
		break;
	};
}

#if RMPTFX_EDITOR
const char* ptxDomain::GetShapeName(ptxDomainShape shape) 
{
	const char* pShapeNames[PTXDOMAIN_SHAPE_NUM] = 
	{
		"Box", 
		"Sphere", 
		"Cylinder",
		"Attractor"
	};

	return pShapeNames[shape];
};
#endif

#if RMPTFX_EDITOR
const bool g_typeShapeCompatibilityTable[PTXDOMAIN_TYPE_NUM][PTXDOMAIN_SHAPE_NUM] =
{
	// box		sphere		cylinder	attractor
	{true,		true,		true,		false},			// creation
	{true,		true,		true,		false},			// target
	{false,		false,		false,		true}			// attractor
};
#endif

#if RMPTFX_EDITOR
u32 ptxDomain::GetTypeFilter(ptxDomainShape shape)
{
	u32 typeFilter = 0;
	for (int i=0; i<PTXDOMAIN_TYPE_NUM; i++)
	{
		typeFilter |= (((u32)g_typeShapeCompatibilityTable[i][shape])<<i);
	}

	return typeFilter;
}
#endif

#if RMPTFX_EDITOR
void ptxDomain::SendToEditor(ptxByteBuff& buff, const ptxEmitterRule* pEmitterRule)
{
	buff.Write_s32(m_shape);
	buff.Write_s32(m_type);
	buff.Write_bool(m_isWorldSpace);
	buff.Write_bool(m_isPointRelative);
	buff.Write_bool(m_isCreationRelative);
	buff.Write_bool(m_isTargetRelative);

	buff.Write_u32(4);
	m_positionKFP.SendToEditor(buff, pEmitterRule->GetName(), PTXRULETYPE_EMITTER);
	m_rotationKFP.SendToEditor(buff, pEmitterRule->GetName(), PTXRULETYPE_EMITTER);
	m_sizeOuterKFP.SendToEditor(buff, pEmitterRule->GetName(), PTXRULETYPE_EMITTER);
	m_sizeInnerKFP.SendToEditor(buff, pEmitterRule->GetName(), PTXRULETYPE_EMITTER);
}
#endif

#if RMPTFX_EDITOR
void ptxDomain::ReceiveFromEditor(ptxByteBuff& buff)
{
	m_shape = buff.Read_s32();
	buff.Read_s32();
	m_isWorldSpace = buff.Read_bool();
	m_isPointRelative = buff.Read_bool();
	m_isCreationRelative = buff.Read_bool();
	m_isTargetRelative = buff.Read_bool();
}
#endif

#if RMPTFX_EDITOR
void ptxDomain::RescaleZoomableComponents(float scalar, ptxEvolutionList* pEvolutionList)
{
	ScalarV vScalar(scalar);

	// scale outer size
	m_sizeOuterKFP.GetKeyframe().ScaleEntries(vScalar);

	ptxEvolvedKeyframeProp* pEvolvedKeframeProp = pEvolutionList->GetEvolvedKeyframePropById(m_sizeOuterKFP.GetPropertyId());
	if (pEvolvedKeframeProp)
	{
		pEvolvedKeframeProp->ScaleEvolvedKeyframes(vScalar);
	}

	// scale inner size
	m_sizeInnerKFP.GetKeyframe().ScaleEntries(vScalar);

	pEvolvedKeframeProp = pEvolutionList->GetEvolvedKeyframePropById(m_sizeInnerKFP.GetPropertyId());
	if (pEvolvedKeframeProp)
	{
		pEvolvedKeframeProp->ScaleEvolvedKeyframes(vScalar);
	}

	// scale position
	m_positionKFP.GetKeyframe().ScaleEntries(vScalar);

	pEvolvedKeframeProp = pEvolutionList->GetEvolvedKeyframePropById(m_positionKFP.GetPropertyId());
	if (pEvolvedKeframeProp)
	{
		pEvolvedKeframeProp->ScaleEvolvedKeyframes(vScalar);
	}

	// update the widgets
#if RMPTFX_BANK
	m_sizeOuterKFP.GetKeyframe().UpdateWidget();
	m_sizeInnerKFP.GetKeyframe().UpdateWidget();
	m_positionKFP.GetKeyframe().UpdateWidget();
#endif
}
#endif

#if RMPTFX_BANK || RMPTFX_EDITOR
void ptxDomain::RemoveDefaultKeyframes()
{
	for (int i=0; i<m_keyframePropList.GetNumKeyframeProps(); i++)
	{
		ptxKeyframeProp* pKeyframeProp = m_keyframePropList.GetKeyframePropByIndex(i);
		if (pKeyframeProp)
		{
			ptxKeyframe& keyframe = pKeyframeProp->GetKeyframe();
			if (keyframe.IsDefault())
			{
				keyframe.DeleteEntry(0);
			}
		}
	}
}
#endif

#if RMPTFX_BANK
void ptxDomain::GetKeyframeEntryInfo(int& num, int& mem, int& max, int& def)
{
	for (int i=0; i<m_keyframePropList.GetNumKeyframeProps(); i++)
	{
		ptxKeyframeProp* pKeyframeProp = m_keyframePropList.GetKeyframePropByIndex(i);
		if (pKeyframeProp)
		{
			ptxKeyframe& keyframe = pKeyframeProp->GetKeyframe();

			int currNum = keyframe.GetNumEntries();

			num += currNum;
			mem += keyframe.GetEntryMem();

			if (currNum>max)
			{
				max = currNum;
			}

			if (keyframe.IsDefault())
			{
				def++;
			}
		}
	}
}
#endif

#if RMPTFX_BANK
void ptxDomain::OutputKeyframeEntryInfo()
{
	for (int i=0; i<m_keyframePropList.GetNumKeyframeProps(); i++)
	{
		ptxKeyframeProp* pKeyframeProp = m_keyframePropList.GetKeyframePropByIndex(i);
		if (pKeyframeProp)
		{
			ptxKeyframe& keyframe = pKeyframeProp->GetKeyframe();
			ptxKeyframeDefn* pKeyframeDefn = keyframe.GetDefn();
			if (ptxVerifyf(pKeyframeDefn, "keyframe doesn't have a definition"))
			{
				ptxAssertf(pKeyframeProp->GetPropertyId()==pKeyframeDefn->GetPropertyId(), "property ids don't match");
				ptxDebugf1("                  %d - %s (%u)", keyframe.GetNumEntries(), pKeyframeDefn->GetName(), pKeyframeDefn->GetPropertyId());
			}
		}
	}
}
#endif







ptxDomainObj::ptxDomainObj(datResource& rsc)
: m_pDomain(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxDomainObj_num++;
#endif
}

#if __DECLARESTRUCT
void ptxDomainObj::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxDomainObj)
		SSTRUCT_FIELD(ptxDomainObj, m_pDomain)
		SSTRUCT_FIELD(ptxDomainObj, m_newDomainShape)
	SSTRUCT_END(ptxDomainObj)
}
#endif

IMPLEMENT_PLACE(ptxDomainObj);

#if RMPTFX_EDITOR
void ptxDomainObj::DomainShapeChangedCB(ptxDomainObj* pDomainObj)
{
	// store the state of the domain object we're changing from
	ptxKeyframe position;
	position.CopyEntriesFrom(pDomainObj->m_pDomain->GetPositionKFP().GetKeyframe());

	ptxKeyframe rotation;
	rotation.CopyEntriesFrom(pDomainObj->m_pDomain->GetRotationKFP().GetKeyframe());

	ptxKeyframe sizeOuter;
	sizeOuter.CopyEntriesFrom(pDomainObj->m_pDomain->GetSizeOuterKFP().GetKeyframe());

	ptxKeyframe sizeInner;
	sizeInner.CopyEntriesFrom(pDomainObj->m_pDomain->GetSizeInnerKFP().GetKeyframe());

	bool isWorldSpace = pDomainObj->m_pDomain->GetIsWorldSpace();
	bool isPointRelative = pDomainObj->m_pDomain->GetIsPointRelative();
	bool isCreationRelative = pDomainObj->m_pDomain->GetIsCreationRelative();
	bool isTargetRelative = pDomainObj->m_pDomain->GetIsTargetRelative();

	// create the new domain
	pDomainObj->m_pDomain = ptxDomain::CreateDomain(pDomainObj->m_pDomain->GetType(), (ptxDomainShape)(pDomainObj->m_newDomainShape));

	// set the new domain state
	pDomainObj->m_pDomain->GetPositionKFP().GetKeyframe().CopyEntriesFrom(position);
	pDomainObj->m_pDomain->GetRotationKFP().GetKeyframe().CopyEntriesFrom(rotation);
	pDomainObj->m_pDomain->GetSizeOuterKFP().GetKeyframe().CopyEntriesFrom(sizeOuter);
	pDomainObj->m_pDomain->GetSizeInnerKFP().GetKeyframe().CopyEntriesFrom(sizeInner);
	pDomainObj->m_pDomain->SetIsWorldSpace(isWorldSpace);
	pDomainObj->m_pDomain->SetIsPointRelative(isPointRelative);
	pDomainObj->m_pDomain->SetIsCreationRelative(isCreationRelative);
	pDomainObj->m_pDomain->SetIsTargetRelative(isTargetRelative);
}
#endif

#if RMPTFX_EDITOR
void ptxDomainObj::SendToEditor(ptxByteBuff& buff, const ptxEmitterRule* pEmitterRule)
{
	if (m_pDomain)
	{
		buff.Write_bool(true);
		m_pDomain->SendToEditor(buff, pEmitterRule);
	}
	else
	{
		buff.Write_bool(false);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxDomainObj::ReceiveFromEditor(ptxByteBuff& buff)
{
	bool exists = buff.Read_bool();
	if (exists)
	{
		ptxDomainShape oldShape = m_pDomain->GetShape();

		m_pDomain->ReceiveFromEditor(buff);

		m_newDomainShape = m_pDomain->GetShape();
		if (m_newDomainShape!=oldShape)
		{
			m_pDomain->SetShape(oldShape);
			DomainShapeChangedCB(this);
			RMPTFXMGR.GetInterface().SetNeedsReply(true);
		}
	}
}
#endif






ptxDomainBox::ptxDomainBox(ptxDomainType type) : ptxDomain(type, PTXDOMAIN_SHAPE_BOX)
{
	SetKeyframeDefns();
}

ptxDomainBox::ptxDomainBox(rage::datResource& rsc)
: ptxDomain(rsc)
{
	SetKeyframeDefns();

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxDomainBox_num++;
#endif
}

#if __DECLARESTRUCT
void ptxDomainBox::DeclareStruct(datTypeStruct& s)
{
	ptxDomain::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxDomainBox, ptxDomain)
	SSTRUCT_END(ptxDomainBox)
}
#endif

IMPLEMENT_PLACE(ptxDomainBox);

Vec3V_Out ptxDomainBox::GetRandomPoint(Mat34V_In vMtxLcl, Vec3V_In vSizeOuter, Vec3V_In vSizeInner)
{
	// default - in case min is greater than max
	Vec3V vRandomPos(V_ZERO);

	if (IsZeroAll(vSizeInner))
	{
		// no inner size specified - use outer size only
		vRandomPos = g_ptxVecRandom.GetSignedRangedVec3V(vSizeOuter);
	}
	else
	{
		// inner size is specified
		// take a single quadrant of the box and divide up into the 3 possible different box regions where points can be generated
		Vec2V regionData[3][3];
		int numRegions = 0;
		if (vSizeInner.GetXf()<vSizeOuter.GetXf())
		{
			regionData[numRegions][0].SetX(vSizeInner.GetX());
			regionData[numRegions][0].SetY(vSizeOuter.GetX());

			regionData[numRegions][1].SetX(ScalarV(V_ZERO));
			regionData[numRegions][1].SetY(vSizeOuter.GetY());

			regionData[numRegions][2].SetX(ScalarV(V_ZERO));
			regionData[numRegions][2].SetY(vSizeOuter.GetZ());	
			numRegions++;
		}

		if (vSizeInner.GetYf()<vSizeOuter.GetYf())
		{
			regionData[numRegions][0].SetX(ScalarV(V_ZERO));
			regionData[numRegions][0].SetY(vSizeOuter.GetX());

			regionData[numRegions][1].SetX(vSizeInner.GetY());	
			regionData[numRegions][1].SetY(vSizeOuter.GetY());	

			regionData[numRegions][2].SetX(ScalarV(V_ZERO));	
			regionData[numRegions][2].SetY(vSizeOuter.GetZ());	
			
			numRegions++;
		}

		if (vSizeInner.GetZf()<vSizeOuter.GetZf())
		{
			regionData[numRegions][0].SetX(ScalarV(V_ZERO));
			regionData[numRegions][0].SetY(vSizeOuter.GetX());

			regionData[numRegions][1].SetX(ScalarV(V_ZERO));
			regionData[numRegions][1].SetY(vSizeOuter.GetY());

			regionData[numRegions][2].SetX(vSizeInner.GetZ());	
			regionData[numRegions][2].SetY(vSizeOuter.GetZ());	

			numRegions++;
		}

		if (numRegions>0)
		{
			int randRegion = g_DrawRand.GetRanged(0, numRegions-1);

			vRandomPos = g_ptxVecRandom.GetRangedVec3V(Vec3V(regionData[randRegion][0].GetX(), regionData[randRegion][1].GetX(), regionData[randRegion][2].GetX()), 
													   Vec3V(regionData[randRegion][0].GetY(), regionData[randRegion][1].GetY(), regionData[randRegion][2].GetY()));

			// randomize the quadrant
			Vec3V vRand((float)((g_DrawRand.GetRanged(0, 1)*2)-1),
						(float)((g_DrawRand.GetRanged(0, 1)*2)-1),
						(float)((g_DrawRand.GetRanged(0, 1)*2)-1));

			vRandomPos *= vRand;
		}
	}

	// apply the current position and rotation keyframes
	vRandomPos = Transform(vMtxLcl, vRandomPos);

	// return the random point
	return vRandomPos;
}

void ptxDomainBox::SetKeyframeDefns()
{
	m_positionKFP.GetKeyframe().SetDefn(&ptxDomainBox::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_POSITION]);
	m_rotationKFP.GetKeyframe().SetDefn(&ptxDomainBox::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_ROTATION]);
	m_sizeOuterKFP.GetKeyframe().SetDefn(&ptxDomainBox::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_SIZE_OUTER]);
	m_sizeInnerKFP.GetKeyframe().SetDefn(&ptxDomainBox::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_SIZE_INNER]);
}

void ptxDomainBox::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_positionKFP, &ptxDomainBox::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_POSITION]);
	pEvolutionList->SetKeyframeDefn(&m_rotationKFP, &ptxDomainBox::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_ROTATION]);
	pEvolutionList->SetKeyframeDefn(&m_sizeOuterKFP, &ptxDomainBox::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_SIZE_OUTER]);
	pEvolutionList->SetKeyframeDefn(&m_sizeInnerKFP, &ptxDomainBox::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_BOX][PTXDOMAIN_PROP_SIZE_INNER]);
}

#if RMPTFX_BANK
void ptxDomainBox::Draw(Mat34V_In vMtxWld, Vec3V_In vSizeOuter, Vec3V_In vSizeInner)
{	
	// set up the colours
	Color32 outerCol, innerCol;
	if (m_type==PTXDOMAIN_TYPE_TARGET)
	{
		outerCol = PTXDOMAIN_VELOCITY_COL_OUTER;
		innerCol = PTXDOMAIN_VELOCITY_COL_INNER;

		if (m_isPointRelative)
		{
			outerCol = PTXDOMAIN_VELOCITY_COL_OUTER_PR;
			innerCol = PTXDOMAIN_VELOCITY_COL_INNER_PR;
		}
	}
	else
	{
		outerCol = PTXDOMAIN_EMITTER_COL_OUTER;
		innerCol = PTXDOMAIN_EMITTER_COL_INNER;
	}

	// draw the boxes
	grcDebugDraw::BoxOriented(-vSizeOuter, vSizeOuter, vMtxWld, outerCol, false);
	grcDebugDraw::BoxOriented(-vSizeInner, vSizeInner, vMtxWld, innerCol, false);

	grcDebugDraw::Axis(vMtxWld, ptxDebug::sm_debugAxisScale);
}
#endif






ptxDomainSphere::ptxDomainSphere(ptxDomainType type) : ptxDomain(type, PTXDOMAIN_SHAPE_SPHERE)
{
	SetKeyframeDefns();
}

ptxDomainSphere::ptxDomainSphere(datResource& rsc)
: ptxDomain(rsc)
{
	SetKeyframeDefns();

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxDomainSphere_num++;
#endif
}

#if __DECLARESTRUCT
void ptxDomainSphere::DeclareStruct(datTypeStruct& s)
{
	ptxDomain::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxDomainSphere, ptxDomain)
	SSTRUCT_END(ptxDomainSphere)
}
#endif

IMPLEMENT_PLACE(ptxDomainSphere);

Vec3V_Out ptxDomainSphere::GetRandomPoint(Mat34V_In vMtxLcl, Vec3V_In vSizeOuter, Vec3V_In vSizeInner)
{
	// get some random numbers
	Vec4V vRand = g_ptxVecRandom.GetVec4V();								// each element between 0 and 1

	// get random direction
	Vec3V vDir = vRand.GetXYZ()*Vec3V(V_TWO) - Vec3V(V_ONE);				// each element between -1 and 1

	// get random radius between inner and outer
	ScalarV vOuterRadius = vSizeOuter.GetX();
	ScalarV vInnerRadius = vSizeInner.GetX();
	ScalarV vRadius = ScalarV(V_ZERO);
	if (IsLessThanAll(vInnerRadius, vOuterRadius))
	{
		vRadius = vInnerRadius + ((vOuterRadius-vInnerRadius) * vRand.GetW());
	}

	// set random position
	Vec3V vRandomPos = Scale(vRadius, Normalize(vDir));

	// apply the current position and rotation keyframes
	vRandomPos = Transform(vMtxLcl, vRandomPos);

	// return the random point
	return vRandomPos;
}

void ptxDomainSphere::SetKeyframeDefns()
{
	m_positionKFP.GetKeyframe().SetDefn(&ptxDomainSphere::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_POSITION]);
	m_rotationKFP.GetKeyframe().SetDefn(&ptxDomainSphere::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_ROTATION]);
	m_sizeOuterKFP.GetKeyframe().SetDefn(&ptxDomainSphere::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_SIZE_OUTER]);
	m_sizeInnerKFP.GetKeyframe().SetDefn(&ptxDomainSphere::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_SIZE_INNER]);
}

void ptxDomainSphere::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_positionKFP, &ptxDomainSphere::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_POSITION]);
	pEvolutionList->SetKeyframeDefn(&m_rotationKFP, &ptxDomainSphere::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_ROTATION]);
	pEvolutionList->SetKeyframeDefn(&m_sizeOuterKFP, &ptxDomainSphere::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_SIZE_OUTER]);
	pEvolutionList->SetKeyframeDefn(&m_sizeInnerKFP, &ptxDomainSphere::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_SPHERE][PTXDOMAIN_PROP_SIZE_INNER]);
}

#if RMPTFX_BANK
void ptxDomainSphere::Draw(Mat34V_In vMtxWld, Vec3V_In vSizeOuter, Vec3V_In vSizeInner)
{
	// set up the colours
	Color32 outerCol, innerCol;
	if (m_type==PTXDOMAIN_TYPE_TARGET)
	{
		outerCol = PTXDOMAIN_VELOCITY_COL_OUTER;
		innerCol = PTXDOMAIN_VELOCITY_COL_INNER;

		if (m_isPointRelative)
		{
			outerCol = PTXDOMAIN_VELOCITY_COL_OUTER_PR;
			innerCol = PTXDOMAIN_VELOCITY_COL_INNER_PR;
		}
	}
	else
	{
		outerCol = PTXDOMAIN_EMITTER_COL_OUTER;
		innerCol = PTXDOMAIN_EMITTER_COL_INNER;
	}

	// draw the spheres
	Vec3V vPos = vMtxWld.GetCol3();
	grcDebugDraw::Sphere(vPos, vSizeOuter.GetXf(), outerCol, false);
	grcDebugDraw::Sphere(vPos, vSizeInner.GetXf(), innerCol, false);
	
	grcDebugDraw::Axis(vMtxWld, ptxDebug::sm_debugAxisScale);
}
#endif






ptxDomainCylinder::ptxDomainCylinder(ptxDomainType type) : ptxDomain(type, PTXDOMAIN_SHAPE_CYLINDER)
{
	SetKeyframeDefns();
}

ptxDomainCylinder::ptxDomainCylinder(datResource& rsc)
: ptxDomain(rsc)
{
	SetKeyframeDefns();

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxDomainCylinder_num++;
#endif
}

#if __DECLARESTRUCT
void ptxDomainCylinder::DeclareStruct(datTypeStruct& s)
{
	ptxDomain::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxDomainCylinder, ptxDomain)
	SSTRUCT_END(ptxDomainCylinder)
}
#endif

IMPLEMENT_PLACE(ptxDomainCylinder);

Vec3V_Out ptxDomainCylinder::GetRandomPoint(Mat34V_In vMtxLcl, Vec3V_In vSizeOuter, Vec3V_In vSizeInner)
{
	// default - in case min is greater than max
	Vec3V vRandomPos(V_ZERO);

	// make sure at least one of the inner sizes is less than the outer
	if (IsGreaterThanOrEqualAll(vSizeInner, vSizeOuter)==false)
	{
#if 1
		// get random angle
		ScalarV angle = g_ptxVecRandom.GetRangedScalarV(ScalarV(V_ZERO), ScalarV(V_TWO_PI));
		Vec2V sinCosAngle = SinCosApprox(angle); //Vec2V(sin(x), cos(x))

		// calculate the points on the cross sectional ellipse at the intersection of the angle and the inner and outer edges
		Vec3V vOuterPoint = Vec3V(vSizeOuter.GetX()*sinCosAngle.GetY(), ScalarV(V_ZERO), vSizeOuter.GetZ()*sinCosAngle.GetX());
		Vec3V vInnerPoint = Vec3V(vSizeInner.GetX()*sinCosAngle.GetY(), ScalarV(V_ZERO), vSizeInner.GetZ()*sinCosAngle.GetX());

		// get a lengths of the inner and outer radii
		ScalarV vOuterRadius = Mag(vOuterPoint);
		ScalarV vInnerRadius = Mag(vInnerPoint);

		// get random length
		ScalarV length = g_ptxVecRandom.GetRangedScalarV(-vSizeOuter.GetY(), vSizeOuter.GetY());
		
		// override the inner radius to zero if the random length is outside the inner length
		vInnerRadius = SelectFT(IsGreaterThan(Abs(length), vSizeInner.GetY()), vInnerRadius, ScalarV(V_ZERO)); 

		// check if the inner x and z sizes are both greater than the outer
		if (IsGreaterThanOrEqualAll(vSizeInner.GetX(), vSizeOuter.GetX()) && IsGreaterThanOrEqualAll(vSizeInner.GetZ(), vSizeOuter.GetZ()))
		{
			// the inner radii are outside the outer ones - must place in the length segments only
			vInnerRadius = ScalarV(V_ZERO);

			length = g_ptxVecRandom.GetRangedScalarV(vSizeInner.GetY(), vSizeOuter.GetY());
			length = SelectFT(IsGreaterThan(g_ptxVecRandom.GetScalarV(), ScalarV(V_HALF)), length, -length); 
		}

		// get a random radius
		ScalarV vRadius = g_DrawRand.GetRangedV(vInnerRadius, vOuterRadius);

		// set the random position 
		vRandomPos = NormalizeSafe(vOuterPoint, Vec3V(V_ZERO));
		vRandomPos *= vRadius;
		vRandomPos.SetY(length);

#else
		// MN - this is now out of date after changes to the above code - needs updated if uncommented
// 		// get some random numbers
// 		Vec3V vRand = g_ptxVecRandom.GetVec3V();										// each element between 0 and 1 
// 
// 		// get random angle
// 		ScalarV vAngle = Scale(vRand.GetX(), ScalarV(V_TWO_PI));
// 		ScalarV vSinAngle, vCosAngle;
// 		SinAndCosFast(vSinAngle, vCosAngle, vAngle);
// 		Vec3V vCosSin = Vec3V(vCosAngle, ScalarV(V_ZERO), vSinAngle);			// cos, 0, sin
// 
// 		Vec3V vInnerRadius = vCosSin * vSizeInner;
// 		Vec3V vOuterRadius = vCosSin * vSizeOuter;
// 		Vec3V vRadius = vInnerRadius + (vOuterRadius-vInnerRadius)*vRand.GetZ();
// 
// 		ScalarV vRandY = vRand.GetY()*ScalarV(V_TWO) - ScalarV(V_ONE);
// 		vRandomPos = Vec3V(Vec::V4PermuteTwo<Vec::X1, Vec::Y2, Vec::Z1, Vec::W1>(vRadius.GetIntrin128(), vRandY.GetIntrin128()));
#endif
	}

	// apply the current position and rotation keyframes
	vRandomPos = Transform(vMtxLcl, vRandomPos);

	// return the random point
	return vRandomPos;
}

void ptxDomainCylinder::SetKeyframeDefns()
{
	m_positionKFP.GetKeyframe().SetDefn(&ptxDomainCylinder::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_POSITION]);
	m_rotationKFP.GetKeyframe().SetDefn(&ptxDomainCylinder::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_ROTATION]);
	m_sizeOuterKFP.GetKeyframe().SetDefn(&ptxDomainCylinder::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_SIZE_OUTER]);
	m_sizeInnerKFP.GetKeyframe().SetDefn(&ptxDomainCylinder::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_SIZE_INNER]);
}

void ptxDomainCylinder::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_positionKFP, &ptxDomainCylinder::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_POSITION]);
	pEvolutionList->SetKeyframeDefn(&m_rotationKFP, &ptxDomainCylinder::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_ROTATION]);
	pEvolutionList->SetKeyframeDefn(&m_sizeOuterKFP, &ptxDomainCylinder::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_SIZE_OUTER]);
	pEvolutionList->SetKeyframeDefn(&m_sizeInnerKFP, &ptxDomainCylinder::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_CYLINDER][PTXDOMAIN_PROP_SIZE_INNER]);
}

#if RMPTFX_BANK
void ptxDomainCylinder::Draw(Mat34V_In vMtxWld, Vec3V_In vSizeOuter, Vec3V_In vSizeInner)
{
	// set up the colours
	Color32 outerCol, innerCol;
	if (m_type==PTXDOMAIN_TYPE_TARGET)
	{
		outerCol = PTXDOMAIN_VELOCITY_COL_OUTER;
		innerCol = PTXDOMAIN_VELOCITY_COL_INNER;

		if (m_isPointRelative)
		{
			outerCol = PTXDOMAIN_VELOCITY_COL_OUTER_PR;
			innerCol = PTXDOMAIN_VELOCITY_COL_INNER_PR;
		}
	}
	else
	{
		outerCol = PTXDOMAIN_EMITTER_COL_OUTER;
		innerCol = PTXDOMAIN_EMITTER_COL_INNER;
	}

	// draw the cylinders
	DrawCylinder(vSizeOuter.GetYf(), vSizeOuter.GetXf(), vSizeOuter.GetZf(), vMtxWld, outerCol, 10);
	DrawCylinder(vSizeInner.GetYf(), vSizeInner.GetXf(), vSizeInner.GetZf(), vMtxWld, innerCol, 10);

	grcDebugDraw::Axis(vMtxWld, ptxDebug::sm_debugAxisScale);
}
#endif

#if RMPTFX_BANK
void ptxDomainCylinder::DrawCylinder(float length, float sizeX, float sizeZ, Mat34V_In vMtxWld, Color32 colour, int steps)
{
	// set the colour
	grcColor(colour);

	// cache some values
	Vec3V vPos = vMtxWld.GetCol3();
	ScalarV vLength = ScalarVFromF32(length);

	// draw top
	Mat34V vTopMtx = vMtxWld;
	vTopMtx.SetCol3(vPos + vMtxWld.GetCol1()*vLength);

	Vec3V vTopVerts[32];
	for (int j=0; j<steps+1; j++) 
	{
		float angle = 2.0f*PI*float(j)/float(steps);
		vTopVerts[j] = Vec3V(sizeX*cosf(angle), 0.0f, sizeZ*sinf(angle));
		vTopVerts[j] = Transform(vTopMtx, vTopVerts[j]);
	}

	for (int j=0; j<steps; j++) 
	{
		grcDebugDraw::Line(vTopVerts[j], vTopVerts[j+1], colour, colour);
	}

	// draw bottom
	Mat34V vBottomMtx = vMtxWld;
	vBottomMtx.SetCol3(vPos - vMtxWld.GetCol1()*vLength);

	Vec3V vBottomVerts[32];
	for (int j=0; j<steps+1; j++) 
	{
		float angle = 2.0f*PI*float(j)/float(steps);
		vBottomVerts[j] = Vec3V(sizeX*cosf(angle), 0.0f, sizeZ*sinf(angle));
		vBottomVerts[j] = Transform(vBottomMtx, vBottomVerts[j]);
	}

	for (int j=0; j<steps; j++) 
	{
		grcDebugDraw::Line(vBottomVerts[j], vBottomVerts[j+1], colour, colour);
	}

	// draw length
	for (int j=0; j<=steps; j++) 
	{
		float angle = 2.0f*PI*float(j)/float(steps);
		Vec3V vStart = Vec3V(sizeX*cosf(angle), length, sizeZ*sinf(angle));
		vStart = Transform(vMtxWld, vStart);
		Vec3V vEnd = Vec3V(sizeX*cosf(angle), -length, sizeZ*sinf(angle));
		vEnd = Transform(vMtxWld, vEnd);
		grcDebugDraw::Line(vStart, vEnd, colour, colour);
	}
}
#endif






ptxDomainAttractor::ptxDomainAttractor(ptxDomainType type) : ptxDomain(type, PTXDOMAIN_SHAPE_ATTRACTOR)
{
	SetKeyframeDefns();
}

ptxDomainAttractor::ptxDomainAttractor(datResource& rsc)
: ptxDomain(rsc)
{
	SetKeyframeDefns();

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxDomainAttractor_num++;
#endif
}

#if __DECLARESTRUCT
void ptxDomainAttractor::DeclareStruct(datTypeStruct& s)
{
	ptxDomain::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxDomainAttractor, ptxDomain)
	SSTRUCT_END(ptxDomainAttractor)
}
#endif

IMPLEMENT_PLACE(ptxDomainAttractor);

void ptxDomainAttractor::SetKeyframeDefns()
{
	m_positionKFP.GetKeyframe().SetDefn(&ptxDomainAttractor::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_POSITION]);
	m_rotationKFP.GetKeyframe().SetDefn(&ptxDomainAttractor::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_ROTATION]);
	m_sizeOuterKFP.GetKeyframe().SetDefn(&ptxDomainAttractor::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_SIZE_OUTER]);
	m_sizeInnerKFP.GetKeyframe().SetDefn(&ptxDomainAttractor::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_SIZE_INNER]);
}

void ptxDomainAttractor::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_positionKFP, &ptxDomainAttractor::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_POSITION]);
	pEvolutionList->SetKeyframeDefn(&m_rotationKFP, &ptxDomainAttractor::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_ROTATION]);
	pEvolutionList->SetKeyframeDefn(&m_sizeOuterKFP, &ptxDomainAttractor::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_SIZE_OUTER]);
	pEvolutionList->SetKeyframeDefn(&m_sizeInnerKFP, &ptxDomainAttractor::sm_propertyDefns[m_type][PTXDOMAIN_SHAPE_ATTRACTOR][PTXDOMAIN_PROP_SIZE_INNER]);
}

#if RMPTFX_BANK
void ptxDomainAttractor::Draw(Mat34V_In vMtxWld, Vec3V_In vSizeOuter, Vec3V_In vSizeInner)
{
	// draw the attractors
	DrawAttractor(vSizeInner.GetXf(), vSizeOuter.GetYf(), vMtxWld, PTXDOMAIN_ATTRACTOR_COL_NEAR, 10);
	DrawAttractor(vSizeOuter.GetXf(), vSizeOuter.GetYf(), vMtxWld, PTXDOMAIN_ATTRACTOR_COL_FAR, 10);
	DrawAttractor(vSizeInner.GetYf(), vSizeOuter.GetYf(), vMtxWld, PTXDOMAIN_ATTRACTOR_COL_ORBIT, 10);

	grcDebugDraw::Axis(vMtxWld, ptxDebug::sm_debugAxisScale);
}
#endif

#if RMPTFX_BANK
void ptxDomainAttractor::DrawAttractor(float radius, float length, Mat34V_In vMtxWld, Color32 colour, int steps)
{
	// set the colour
	grcColor(colour);

	// cache some values
	Vec3V vPos = vMtxWld.GetCol3();
	ScalarV vLength = ScalarVFromF32(length);

	// draw top
	Mat34V vTopMtx = vMtxWld;
	vTopMtx.SetCol3(vPos + vMtxWld.GetCol1()*vLength);

	Vec3V vTopVerts[32];
	for (int j=0; j<steps+1; j++) 
	{
		float angle = 2.0f*PI*float(j)/float(steps);
		vTopVerts[j] = Vec3V(radius*cosf(angle), 0.0f, radius*sinf(angle));
		vTopVerts[j] = Transform(vTopMtx, vTopVerts[j]);
	}

	for (int j=0; j<steps; j++) 
	{
		grcDebugDraw::Line(vTopVerts[j], vTopVerts[j+1], colour, colour);
	}

	// draw bottom
	Mat34V vBottomMtx = vMtxWld;
	vBottomMtx.SetCol3(vPos - vMtxWld.GetCol1()*vLength);

	Vec3V vBottomVerts[32];
	for (int j=0; j<steps+1; j++) 
	{
		float angle = 2.0f*PI*float(j)/float(steps);
		vBottomVerts[j] = Vec3V(radius*cosf(angle), 0.0f, radius*sinf(angle));
		vBottomVerts[j] = Transform(vBottomMtx, vBottomVerts[j]);
	}

	for (int j=0; j<steps; j++) 
	{
		grcDebugDraw::Line(vBottomVerts[j], vBottomVerts[j+1], colour, colour);
	}

	// draw length
	for (int j=0; j<=steps; j++) 
	{
		float angle = 2.0f*PI*float(j)/float(steps);
		Vec3V vStart = Vec3V(radius*cosf(angle), length, radius*sinf(angle));
		vStart = Transform(vMtxWld, vStart);
		Vec3V vEnd = Vec3V(radius*cosf(angle), -length, radius*sinf(angle));
		vEnd = Transform(vMtxWld, vEnd);
		grcDebugDraw::Line(vStart, vEnd, colour, colour);
	}
}
#endif
