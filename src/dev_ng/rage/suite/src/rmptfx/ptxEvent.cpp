// 
// rmptfx/ptxevent.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxevent.h"
#include "rmptfx/ptxevent_parser.h"

// includes (rmptfx)
#include "rmptfx/ptxbytebuff.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxemitterinst.h"
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxparticlerule.h"
#include "rmptfx/ptxtimeline.h"

// includes (rage)
#include "data/safestruct.h"
#include "math/random.h"
#include "parser/treenode.h"
#include "grprofile/drawmanager.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
using namespace rage;


// code
#if RMPTFX_EDITOR
ptxEventUIData::ptxEventUIData()
{
	m_isActive = true;
}
#endif

#if RMPTFX_EDITOR
void ptxEventUIData::SendBaseToEditor(ptxByteBuff& buff)
{
	buff.Write_bool(m_isActive);
}
#endif

#if RMPTFX_EDITOR
void ptxEventUIData::ReceiveBaseFromEditor(ptxByteBuff& buff)
{
	m_isActive = buff.Read_bool();
}
#endif

#if RMPTFX_EDITOR
void ptxEventUIData::SendEffectToEditor(ptxByteBuff& buff)
{
	buff.Write_bool(m_pad1);
	buff.Write_bool(m_pad2);
}
#endif

#if RMPTFX_EDITOR
void ptxEventUIData::ReceiveEffectFromEditor(ptxByteBuff& buff)
{
	m_pad1 = buff.Read_bool();
	m_pad2 = buff.Read_bool();
}
#endif






ptxEvent::ptxEvent()
{
	m_index = -1;
	m_type = PTXEVENT_TYPE_NUM;
	m_startRatio = 0.0f;
	m_endRatio = 0.0f;

	m_pEvolutionList = NULL;

	m_distSqrToCamera = 0.0f;

	m_pUIData = NULL;
}

ptxEvent::~ptxEvent() 
{
	if (m_pEvolutionList)
	{
		delete m_pEvolutionList;
	}

	if (m_pUIData)
	{
		delete[] (char*)m_pUIData;
	}
}

ptxEvent::ptxEvent(datResource&rsc)
: m_pEvolutionList(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxEvent_num++;
#endif
}

#if __DECLARESTRUCT
namespace rage {datSwapper_ENUM(ptxEventType);}
void ptxEvent::DeclareStruct(datTypeStruct& s)
{
	m_pUIData = NULL;

	SSTRUCT_BEGIN(ptxEvent)
		SSTRUCT_FIELD(ptxEvent, m_index)
		SSTRUCT_FIELD(ptxEvent, m_type)
		SSTRUCT_FIELD(ptxEvent, m_startRatio)
		SSTRUCT_FIELD(ptxEvent, m_endRatio)
		SSTRUCT_FIELD(ptxEvent, m_pEvolutionList)
		SSTRUCT_FIELD(ptxEvent, m_distSqrToCamera)
		SSTRUCT_IGNORE(ptxEvent, m_pUIData)
	SSTRUCT_END(ptxEvent)
}
#endif

void ptxEvent::Place(ptxEvent* pEvent, datResource& rsc)
{
	if (pEvent->m_type == PTXEVENT_TYPE_EMITTER)
	{
		ptxEventEmitter* pEmitterEvent = reinterpret_cast<ptxEventEmitter*>(pEvent);
		ptxEventEmitter::Place(pEmitterEvent, rsc);
	}
}

#if RMPTFX_XML_LOADING
void ptxEvent::RepairAndUpgradeData(const char* UNUSED_PARAM(pFilename), int eventIdx)
{
	m_index = eventIdx;
}
#endif


#if RMPTFX_EDITOR
void ptxEvent::CreateUiData()
{
	m_pUIData = rage_new ptxEventUIData;
}
#endif

#if RMPTFX_EDITOR
void ptxEvent::SendToEditor(ptxByteBuff& buff, const ptxEffectRule* pEffectRule)
{
	buff.Write_s32(m_type);	
	buff.Write_s32(m_index);
	buff.Write_float(m_startRatio);
	buff.Write_float(m_endRatio);

	if (m_pEvolutionList)
	{
		buff.Write_bool(true);

		m_pEvolutionList->SendToEditor(buff, pEffectRule, m_index);
	}
	else
	{
		buff.Write_bool(false);
	}

	m_pUIData->SendBaseToEditor(buff);
}
#endif

#if RMPTFX_EDITOR
void ptxEvent::ReceiveFromEditor(ptxByteBuff& buff)
{
	s32 type = buff.Read_s32();
	if (ptxVerifyf(type==m_type, "event type mismatch"))
	{
		m_index = buff.Read_s32();
		m_startRatio = buff.Read_float();
		m_endRatio = buff.Read_float();

		bool hasEvoList = buff.Read_bool();
		if (hasEvoList)
		{
			if (ptxVerifyf(m_pEvolutionList, "event has no evo list"))
			{
				m_pEvolutionList->ReceiveFromEditor(buff);
			}
		}

		m_pUIData->ReceiveBaseFromEditor(buff);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEvent::CreateUiObjects()
{
	CreateUiData();
}
#endif








ptxEventEmitter::ptxEventEmitter()
{	
	m_type = PTXEVENT_TYPE_EMITTER;

	m_pEmitterRule = NULL;
	m_pParticleRule = NULL;

	m_playbackRateScalarMin = 1.0f;
	m_playbackRateScalarMax = 1.0f;
	m_zoomScalarMin = 1.0f;
	m_zoomScalarMax = 1.0f;
	m_colourTintMin.Set(255, 255, 255, 255);
	m_colourTintMax.Set(255, 255, 255, 255);
};

ptxEventEmitter::ptxEventEmitter(datResource& rsc)
: ptxEvent(rsc)
, m_emitterRuleName(rsc)
, m_particleRuleName(rsc)
, m_pEmitterRule(rsc)
, m_pParticleRule(rsc)
, m_colourTintMin(rsc)
, m_colourTintMax(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxEventEmitter_num++;
#endif
}

#if __DECLARESTRUCT
void ptxEventEmitter::DeclareStruct(datTypeStruct& s)
{
	ptxEvent::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxEventEmitter, ptxEvent)
		SSTRUCT_FIELD(ptxEventEmitter, m_emitterRuleName)
		SSTRUCT_FIELD(ptxEventEmitter, m_particleRuleName)
		SSTRUCT_FIELD(ptxEventEmitter, m_pEmitterRule)
		SSTRUCT_FIELD(ptxEventEmitter, m_pParticleRule)
		SSTRUCT_FIELD(ptxEventEmitter, m_playbackRateScalarMin)
		SSTRUCT_FIELD(ptxEventEmitter, m_playbackRateScalarMax)
		SSTRUCT_FIELD(ptxEventEmitter, m_zoomScalarMin)
		SSTRUCT_FIELD(ptxEventEmitter, m_zoomScalarMax)
		SSTRUCT_FIELD(ptxEventEmitter, m_colourTintMin)
		SSTRUCT_FIELD(ptxEventEmitter, m_colourTintMax)
	SSTRUCT_END(ptxEventEmitter)
}
#endif

IMPLEMENT_PLACE(ptxEventEmitter);

void ptxEventEmitter::Trigger(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst)
{
	ptxAssertf(pEffectInst, "RMPTFX: (%s) Missing effectinst", pEffectInst->GetEffectRule()->GetName());
	ptxAssertf(pEventInst, "RMPTFX: (%s) Missing event inst data", pEffectInst->GetEffectRule()->GetName());

	// deal with emitters not set up correctly
	if ((m_pParticleRule==NULL) || (m_pEmitterRule==NULL))
	{
		ptxMsg::AssetErrorf("(%s) Emitter Event: Missing emit and/or ptxrule. Auto Stopping Effect.", pEffectInst->GetEffectRule()->GetName());
		pEffectInst->Finish(false);
		return;
	}

	// check if the emitter is playing already
	if (pEventInst->GetEmitterInst()->GetPlayState()==PTXEMITINSTPLAYSTATE_PLAYING)
	{
		// already playing - loop the emitter
		pEventInst->GetEmitterInst()->Loop(pEffectInst);
	}
	else
	{
		// not playing yet - start it
		// get random emitter scalars
		float playbackRateScalar = g_DrawRand.GetRanged(m_playbackRateScalarMin, m_playbackRateScalarMax);
		float zoomScalar = g_DrawRand.GetRanged(m_zoomScalarMin, m_zoomScalarMax);

		// get random emitter colour tint
		float randRGB = g_DrawRand.GetFloat();
		float randA = g_DrawRand.GetFloat();

		Color32 colTint;
		colTint.SetRed(Lerp(randRGB, (int)m_colourTintMin.GetRed(), (int)m_colourTintMax.GetRed()));
		colTint.SetGreen(Lerp(randRGB, (int)m_colourTintMin.GetGreen(), (int)m_colourTintMax.GetGreen()));
		colTint.SetBlue(Lerp(randRGB, (int)m_colourTintMin.GetBlue(), (int)m_colourTintMax.GetBlue()));
		colTint.SetAlpha(Lerp(randA, (int)m_colourTintMin.GetAlpha(), (int)m_colourTintMax.GetAlpha()));

		// start the emitter
		pEventInst->GetEmitterInst()->Start(pEffectInst, m_pEmitterRule, m_pParticleRule, m_startRatio, m_endRatio, playbackRateScalar, zoomScalar, colTint);
	}
}

void ptxEventEmitter::Stop(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst)
{
	pEventInst->GetEmitterInst()->Stop(pEffectInst);
}

void ptxEventEmitter::Finish(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst)
{
	pEventInst->GetEmitterInst()->Finish(pEffectInst);
}

void ptxEventEmitter::Relocate(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst, Vec3V_In vDeltaPos)
{
	pEventInst->GetEmitterInst()->Relocate(pEffectInst, vDeltaPos);
}

void ptxEventEmitter::UpdateCulled(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst)
{
	pEventInst->GetEmitterInst()->UpdateCulled(pEffectInst, m_pEvolutionList);
}

void ptxEventEmitter::UpdateSetup(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst, float dt)
{
#if RMPTFX_EDITOR
	// check if we are active in the editor (solo or mute may be chosen)
	if (!m_pUIData->GetIsActive())
	{
		pEventInst->GetEmitterInst()->SetFlag(PTXEMITINSTFLAG_NOT_ACTIVE);
	}
	else
	{
		pEventInst->GetEmitterInst()->ClearFlag(PTXEMITINSTFLAG_NOT_ACTIVE);
	}
#endif

	pEventInst->GetEmitterInst()->UpdateSetup(pEffectInst, m_pEvolutionList, dt);
}

void ptxEventEmitter::UpdateParallel(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst, float dt)
{
	pEventInst->GetEmitterInst()->UpdateParallel(pEffectInst, m_pEvolutionList, dt);
}

bool ptxEventEmitter::UpdateFinalize(ptxEffectInst*  RMPTFX_BANK_ONLY(pEffectInst), ptxEventInst* pEventInst, float  RMPTFX_BANK_ONLY(dt))
{

	RMPTFX_BANK_ONLY(if(ptxDebug::sm_performParallelFinalize))
	{
		ptxEmitterInstPlayState emitterInstPlayState = pEventInst->GetEmitterInst()->GetPlayState();
		return (emitterInstPlayState == PTXEMITINSTPLAYSTATE_FINISHED || emitterInstPlayState == PTXEMITINSTPLAYSTATE_WAITING_TO_START);
	}
#if RMPTFX_BANK
	else
	{
		m_pEmitterRule->PrepareEvoData(m_pEvolutionList);
		m_pParticleRule->PrepareEvoData(m_pEvolutionList);
		return pEventInst->GetEmitterInst()->UpdateFinalize(pEffectInst, m_pEvolutionList, dt);
	}
#endif

	

}

void ptxEventEmitter::Draw(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface))
{
#if RMPTFX_EDITOR
	// check if we are active in the editor (solo or mute may be chosen)
	if (m_pUIData->GetIsActive())
#endif
	{
		m_pParticleRule->PrepareEvoData(m_pEvolutionList, false);

		if (pEventInst->GetEmitterInst()->GetNumActivePointsMT(RMPTFXTHREADER.GetDrawBufferId()))
		{
			pEventInst->GetEmitterInst()->Draw(pEffectInst, m_pEvolutionList  PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(pMultipleDrawInterface));
		}
	}
}

void ptxEventEmitter::PrepareEvoData()
{
	if (m_pEmitterRule) 
	{
		m_pEmitterRule->PrepareEvoData(m_pEvolutionList);
	}

	if (m_pParticleRule)
	{
		m_pParticleRule->PrepareEvoData(m_pEvolutionList);
	}
}

void ptxEventEmitter::SetEvolvedKeyframeDefns(const ptxEffectRule* ASSERT_ONLY(pEffectRule))
{
	if (m_pEvolutionList)
	{
		if (m_pEmitterRule)
		{
			m_pEmitterRule->SetEvolvedKeyframeDefns(m_pEvolutionList);
		}

		if (m_pParticleRule)
		{
			m_pParticleRule->SetEvolvedKeyframeDefns(m_pEvolutionList);
		}

		// check for any evolved keyframe property that has a keyframe with no definitions - these must be old and need removed
		for (int j=0; j<m_pEvolutionList->GetNumEvolvedKeyframeProps(); j++)
		{
			ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = m_pEvolutionList->GetEvolvedKeyframePropByIndex(j);
			if (pEvolvedKeyframeProp)
			{
				for (int i=0; i<pEvolvedKeyframeProp->GetNumEvolvedKeyframes(); i++)
				{
					ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);
					ptxKeyframe& keyframe = evolvedKeyframe.GetKeyframe();
					ptxKeyframeDefn* pKeyframeDefn = keyframe.GetDefn();
					if (pKeyframeDefn==NULL)
					{
						ptxAssertf(0, "effect rule %s has an evolved keyframe from no definition (%u)", pEffectRule->GetName(), pEvolvedKeyframeProp->GetPropertyId());
					}
				}
			}
		}
	}
}

#if RMPTFX_XML_LOADING
void ptxEventEmitter::OnPreSave()
{
	const char* pEmitterRuleName = m_pEmitterRule->GetName();
	if (pEmitterRuleName)
	{
		m_emitterRuleName = pEmitterRuleName;
	}
	else
	{
		m_emitterRuleName = NULL;
	}

	const char* pParticleRuleName = m_pParticleRule->GetName();
	if (pParticleRuleName)
	{
		m_particleRuleName = pParticleRuleName;
	}
	else
	{
		m_particleRuleName = NULL;
	}
}
#endif

#if RMPTFX_XML_LOADING
void ptxEventEmitter::ResolveReferences()
{
	// load in the emitter rule
	m_pEmitterRule = RMPTFXMGR.LoadEmitterRule(m_emitterRuleName);
	if (m_pEmitterRule)
	{
		m_pEmitterRule->AddRef();
	}
	else
	{
		ptxMsg::AssetErrorf("\"%s.emitrule\" is missing (file not found)", m_emitterRuleName.c_str());
	}

	// load in the particle rule
	m_pParticleRule = RMPTFXMGR.LoadParticleRule(m_particleRuleName);
	if (m_pParticleRule)
	{
		m_pParticleRule->AddRef();
	}
	else
	{
		ptxMsg::AssetErrorf("\"%s.ptxrule\" is missing (file not found)", m_particleRuleName.c_str());
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEventEmitter::SendToEditor(ptxByteBuff& buff, const ptxEffectRule* pEffectRule)
{
	// update the evolution domain ui data (in case types have changed)
	if (m_pEvolutionList)
	{
		if (m_pEmitterRule)
		{
			m_pEmitterRule->UpdateUIData(m_pEvolutionList, pEffectRule, m_index);
		}
		
		if (m_pParticleRule)
		{
			m_pParticleRule->UpdateUIData(m_pEvolutionList, pEffectRule, m_index);
		}
	}

	// send base data
	ptxEvent::SendToEditor(buff, pEffectRule);
	
	// send emitter rule name
	if (m_pEmitterRule)
	{
		buff.Write_const_char(m_pEmitterRule->GetName());
	}
	else
	{
		buff.Write_const_char(m_emitterRuleName);
	}

	// send particle rule name
	if (m_pParticleRule)
	{
		buff.Write_const_char(m_pParticleRule->GetName());
	}
	else
	{
		buff.Write_const_char(m_particleRuleName);
	}

	// send whether or not the emitter is a one shot
	// required for updating the timeline in the editor correctly
	if (m_pEmitterRule)
	{
		buff.Write_bool(m_pEmitterRule->GetIsOneShot());
	}
	else
	{
		buff.Write_bool(false);
	}

	// send scalars and tints
	buff.Write_float(m_playbackRateScalarMin);
	buff.Write_float(m_playbackRateScalarMax);

	buff.Write_float(m_zoomScalarMin*100.0f);
	buff.Write_float(m_zoomScalarMax*100.0f);

	buff.Write_u8(m_colourTintMin.GetRed());
	buff.Write_u8(m_colourTintMin.GetGreen());
	buff.Write_u8(m_colourTintMin.GetBlue());
	buff.Write_u8(m_colourTintMin.GetAlpha());

	buff.Write_u8(m_colourTintMax.GetRed());
	buff.Write_u8(m_colourTintMax.GetGreen());
	buff.Write_u8(m_colourTintMax.GetBlue());
	buff.Write_u8(m_colourTintMax.GetAlpha());
}
#endif

#if RMPTFX_EDITOR
void ptxEventEmitter::ReceiveFromEditor(ptxByteBuff& buff)
{
	// receive base data
	ptxEvent::ReceiveFromEditor(buff);

	// read emitter and particle names
	UpdateEmitterRule(buff.Read_const_char());
	UpdateParticleRule(buff.Read_const_char());

	// read scalars and tints
	m_playbackRateScalarMin = buff.Read_float();
	m_playbackRateScalarMax = buff.Read_float();

	m_zoomScalarMin = buff.Read_float()*0.01f;
	m_zoomScalarMax = buff.Read_float()*0.01f;

	u8 minR = buff.Read_u8();
	u8 minG = buff.Read_u8();
	u8 minB = buff.Read_u8();
	u8 minA = buff.Read_u8();
	m_colourTintMin.Set(minR, minG, minB, minA);

	u8 maxR = buff.Read_u8();
	u8 maxG = buff.Read_u8();
	u8 maxB = buff.Read_u8();
	u8 maxA = buff.Read_u8();
	m_colourTintMax.Set(maxR, maxG, maxB, maxA);
}
#endif

#if RMPTFX_EDITOR
void ptxEventEmitter::CreateEvolution(const char* pEvoName)
{
	if (strlen(pEvoName))
	{
		if (m_pEvolutionList==NULL)
		{
			m_pEvolutionList = rage_new ptxEvolutionList();
		}

		m_pEvolutionList->AddEvolution(pEvoName);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEventEmitter::DeleteEvolution(const char* pEvoName)
{
	m_pEvolutionList->DeleteEvolution(pEvoName);
}
#endif

#if RMPTFX_EDITOR
void ptxEventEmitter::AddEvolvedKeyframeProp(u32 keyframePropId, const char* pEvoName)
{
	if (m_pEvolutionList)
	{
		// search for the keyframe property in the emitter and particle rules
		ptxKeyframeProp* pKeyframeProp = NULL;
		if (m_pEmitterRule)
		{
			pKeyframeProp = m_pEmitterRule->GetKeyframeProp(keyframePropId);
		}
		
		if (pKeyframeProp==NULL && m_pParticleRule)
		{
			pKeyframeProp = m_pParticleRule->GetKeyframeProp(keyframePropId);
		}

		// if found add the evolution
		if (pKeyframeProp)
		{
			m_pEvolutionList->AddEvolvedKeyframeProp(pKeyframeProp, pEvoName);
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEventEmitter::DeleteEvolvedKeyframeProp(u32 keyframePropId, const char* pEvoName)
{
	if (m_pEvolutionList)
	{
		// search for the keyframe property in the emitter and particle rules
		ptxKeyframeProp* pKeyframeProp = NULL;
		if (m_pEmitterRule)
		{
			pKeyframeProp = m_pEmitterRule->GetKeyframeProp(keyframePropId);
		}

		if (pKeyframeProp==NULL && m_pParticleRule)
		{
			pKeyframeProp = m_pParticleRule->GetKeyframeProp(keyframePropId);
		}

		// if found delete the evolution
		if (pKeyframeProp)
		{
			m_pEvolutionList->DeleteEvolvedKeyframeProp(pKeyframeProp, pEvoName);
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEventEmitter::StartProfileUpdate()
{
	if (m_pEmitterRule)
	{
		m_pEmitterRule->GetUIData().StartProfileUpdate();
	}

	if (m_pParticleRule)
	{
		m_pParticleRule->StartProfileUpdate();
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEventEmitter::StartProfileDraw()
{
	if (m_pEmitterRule)
	{
		m_pEmitterRule->GetUIData().StartProfileDraw();
	}

	if (m_pParticleRule)
	{
		m_pParticleRule->StartProfileDraw();
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEventEmitter::RescaleZoomableComponents(float scalar)
{
	if (m_pEmitterRule)
	{
		m_pEmitterRule->RescaleZoomableComponents(scalar, m_pEvolutionList);
	}
}
#endif

#if RMPTFX_EDITOR
bool ptxEventEmitter::ContainsParticleRule(const char* pParticleRuleName)
{
	if (m_pParticleRule)
	{
		return stricmp(m_pParticleRule->GetName(), pParticleRuleName)==0;
	}

	return false;
}
#endif

#if RMPTFX_EDITOR || __RESOURCECOMPILER
void ptxEventEmitter::Save()
{
	// make sure the saved names match the actual names in the pointer
	if (m_pEmitterRule)
	{
		m_emitterRuleName = m_pEmitterRule->GetName();
		m_pEmitterRule->Save();
	}
	else
	{
		m_emitterRuleName = NULL;
	}

	if (m_pParticleRule)
	{
		m_particleRuleName = m_pParticleRule->GetName();
		m_pParticleRule->Save();
	}
	else
	{
		m_particleRuleName = NULL;
	}
}
#endif

#if RMPTFX_BANK
void ptxEventEmitter::DebugDraw(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst)
{
#if RMPTFX_EDITOR
	// check if we are active in the editor (solo or mute may be chosen)
	if (m_pUIData->GetIsActive())
#endif
	{
		if (m_pEmitterRule)
		{
			pEventInst->GetEmitterInst()->DebugDraw(pEffectInst, m_pEvolutionList, m_index);
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEventEmitter::UpdateEmitterRule(const char* pEmitterRuleName)
{
	// make sure a valid name is passed in
	if (pEmitterRuleName==NULL)
	{
		return;
	}

	// don't update if it's the same rule
	if (m_emitterRuleName && strcmp(m_emitterRuleName, pEmitterRuleName)==false)
	{
		return;
	}

	// load in the new rule
	ptxEmitterRule* pNewEmitterRule = RMPTFXMGR.LoadEmitterRule(pEmitterRuleName);

	// set up the new rule
	if (pNewEmitterRule)
	{
		// set the name
		m_emitterRuleName = pEmitterRuleName;

		// release the old rule
		if (m_pEmitterRule)
		{
			m_pEmitterRule->Release();
		}

		// add a reference to the new rule
		pNewEmitterRule->AddRef();

		// set the new rule
		m_pEmitterRule = pNewEmitterRule;
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEventEmitter::UpdateParticleRule(const char* pParticleRuleName)
{
	// make sure a valid name is passed in
	if (pParticleRuleName==NULL)
	{
		return;
	}

	// don't update if it's the same rule
	if (m_particleRuleName && strcmp(m_particleRuleName, pParticleRuleName)==false)
	{
		return;
	}

	ptxParticleRule* pNewParticleRule = RMPTFXMGR.LoadParticleRule(pParticleRuleName);

	if(pNewParticleRule)
	{
		m_particleRuleName = pParticleRuleName;
		if(m_pParticleRule)
		{
			m_pParticleRule->Release();
		}
		pNewParticleRule->AddRef();
		m_pParticleRule = pNewParticleRule;
	}
}
#endif


