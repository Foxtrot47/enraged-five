// 
// rmptfx/ptxevent.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXEVENT_H
#define RMPTFX_PTXEVENT_H


// includes (rmptfx)
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxemitterinst.h"

// include (rage)
#include "parser/macros.h"


// namespaces
namespace rage
{


// forward declarations
class ptxEffectInst;
class ptxEmitterRule;
class ptxEventInst;
class ptxParticleRule;
class ptxTimeLine;


// enumerations
enum ptxEventType
{
	PTXEVENT_TYPE_EMITTER			= 0,

	PTXEVENT_TYPE_NUM
};


// structures
#if RMPTFX_EDITOR
struct ptxEventUIData
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////
	
		ptxEventUIData();

		bool GetIsActive() {return m_isActive;}

		void SendBaseToEditor(ptxByteBuff& buff);
		void ReceiveBaseFromEditor(ptxByteBuff& buff);
		void SendEffectToEditor(ptxByteBuff& buff);
		void ReceiveEffectFromEditor(ptxByteBuff& buff);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		// generic
		bool m_isActive;														// whether the event is active (the editor can solo or mute events)

		// effect specific (no longer used)
		bool m_pad1;															// can be removed
		bool m_pad2;															// can be removed
};
#endif


// classes
class ptxEventInst
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxEventInst() {m_numTriggers = -1;}
		
		// 
		void Init(ptxEffectInst* pEffectInst) {m_emitterInst.Init(pEffectInst); m_numTriggers=0;}

		// accessors
		ptxEmitterInst* GetEmitterInst() {return &m_emitterInst;}
		void SetNumTriggers(int val) {m_numTriggers = val;}
		void IncNumTriggers() {m_numTriggers++;}
		int GetNumTriggers() {return m_numTriggers;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		ptxEmitterInst m_emitterInst;											// the event's emitter inst (if an emitter event)
		int m_numTriggers;														// the number of times the event has triggered (if an effect event)

};


class ptxEvent
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxEvent();
		virtual ~ptxEvent();
	
		// resourcing
		ptxEvent(datResource& rsc);
#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxEvent);

		// accessors
		ptxEventType GetType() {return m_type;}
		float GetStartRatio() {return m_startRatio;}
		float GetEndRatio() {return m_endRatio;}
		ptxEvolutionList* GetEvolutionList() {return m_pEvolutionList;}
		void SetDistSqrToCamera(float val) {m_distSqrToCamera = val;}
		float GetDistSqrToCamera() const {return m_distSqrToCamera;}
		void SetIndex(int val) {m_index = val;}
		int GetIndex() const {return m_index;}
		
		// virtual interface
		virtual void Trigger(ptxEffectInst* UNUSED_PARAM(pEffectInst), ptxEventInst* UNUSED_PARAM(pEventInst)) {}
		virtual void Stop(ptxEffectInst* UNUSED_PARAM(pEffectInst), ptxEventInst* UNUSED_PARAM(pEventInst)) {}
		virtual void Finish(ptxEffectInst* UNUSED_PARAM(pEffectInst), ptxEventInst* UNUSED_PARAM(pEventInst)) {}
		virtual void Relocate(ptxEffectInst* UNUSED_PARAM(pEffectInst), ptxEventInst* UNUSED_PARAM(pEventInst), Vec3V_In UNUSED_PARAM(vDeltaPos)) {}

		virtual void UpdateCulled(ptxEffectInst* UNUSED_PARAM(pEffectInst), ptxEventInst* UNUSED_PARAM(pEventInst)) {}
		virtual void UpdateSetup(ptxEffectInst* UNUSED_PARAM(pEffectInst), ptxEventInst* UNUSED_PARAM(pEventInst), float UNUSED_PARAM(dt)) {}
		virtual void UpdateParallel(ptxEffectInst* UNUSED_PARAM(pEffectInst), ptxEventInst* UNUSED_PARAM(pEventInst), float UNUSED_PARAM(dt)) {}
		virtual bool UpdateFinalize(ptxEffectInst* UNUSED_PARAM(pEffectInst), ptxEventInst* UNUSED_PARAM(pEventInst), float UNUSED_PARAM(dt)) {return true;}

		virtual void Draw(ptxEffectInst* UNUSED_PARAM(pEffectInst), ptxEventInst* UNUSED_PARAM(pEventInst)  PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface)) { PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY((void)pMultipleDrawInterface); }

		virtual void PrepareEvoData() {}

		// xml loading/saving
#if RMPTFX_XML_LOADING
		virtual void RepairAndUpgradeData(const char* pFilename, int eventIdx);
		virtual void ResolveReferences() {}
#endif

		// editor
#if RMPTFX_EDITOR
		virtual void SendToEditor(ptxByteBuff& buff, const ptxEffectRule* pEffectRule);
		virtual void ReceiveFromEditor(ptxByteBuff& buff);

		virtual void CreateEvolution(const char* UNUSED_PARAM(pEvoName)) {}
		virtual void DeleteEvolution(const char* UNUSED_PARAM(pEvoName)) {}
		virtual void AddEvolvedKeyframeProp(u32 UNUSED_PARAM(propertyID), const char* UNUSED_PARAM(pEvoName)) {}
		virtual void DeleteEvolvedKeyframeProp(u32 UNUSED_PARAM(propertyID), const char* UNUSED_PARAM(pEvoName)) {}

		virtual void StartProfileUpdate() {};
		virtual void StartProfileDraw() {};

		virtual void RescaleZoomableComponents(float UNUSED_PARAM(scalar)) {};
		virtual bool ContainsParticleRule(const char* UNUSED_PARAM(pParticleRuleName)) {return false;}

		// profiling
		virtual void CreateUiObjects();
		ptxEventUIData& GetUIData() {return *m_pUIData;}
#endif

#if RMPTFX_EDITOR || __RESOURCECOMPILER
		virtual void Save(){}
#endif

		// debug
#if RMPTFX_BANK
		virtual void DebugDraw(ptxEffectInst* UNUSED_PARAM(pEffectInst), ptxEventInst* UNUSED_PARAM(pEventInst)) {}
#endif


	private: //////////////////////////

#if RMPTFX_EDITOR
		void CreateUiData();
#else
		void CreateUiData() {m_pUIData = NULL;}
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	protected: ////////////////////////

		int m_index;															// the index of the event 
		ptxEventType m_type;													// the event type (emitter or effect)
		float m_startRatio;														// the ratio of the effect's current duration when this event starts
		float m_endRatio;														// the ratio of the effect's current duration when this event ends

		datOwner<ptxEvolutionList> m_pEvolutionList;							// pointer to the list of evolutions on the event

		float m_distSqrToCamera;												// space to store the distance from the event to the camera (temp storage for sorting when drawing)

#if RMPTFX_EDITOR
		ptxEventUIData* m_pUIData;												// profile and debug data used by the editor
#else
		void* m_pUIData;
#endif

};


class ptxEventEmitter : public ptxEvent
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxEventEmitter();

		// resourcing
		ptxEventEmitter(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxEventEmitter);

		// accessors
		void SetEmitterRule(ptxEmitterRule* pEmitterRule) {m_pEmitterRule = pEmitterRule;}
		ptxEmitterRule* GetEmitterRule() {return m_pEmitterRule;}
		void SetParticleRule(ptxParticleRule* pParticleRule) {m_pParticleRule = pParticleRule;}
		ptxParticleRule* GetParticleRule() {return m_pParticleRule;}
		float GetPlaybackRateScalarMin() const {return m_playbackRateScalarMin;}

		// virtual interface
		void Trigger(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst);
		void Stop(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst);
		void Finish(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst);
		void Relocate(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst, Vec3V_In vDeltaPos);

		void UpdateCulled(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst);
		void UpdateSetup(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst, float dt);
		void UpdateParallel(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst, float dt);
		bool UpdateFinalize(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst, float dt);

		void Draw(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface));

		void PrepareEvoData();
		void SetEvolvedKeyframeDefns(const ptxEffectRule* pEffectRule);

		// xml loading/saving
#if RMPTFX_XML_LOADING
		void OnPreSave();
		void ResolveReferences();
#endif

		// editor
#if RMPTFX_EDITOR
		void SendToEditor(ptxByteBuff& buff, const ptxEffectRule* pEffectRule);
		void ReceiveFromEditor(ptxByteBuff& buff);

		void CreateEvolution(const char* pEvoName);
		void DeleteEvolution(const char* pEvoName);
		void AddEvolvedKeyframeProp(u32 keyframePropId, const char* pEvoName);
		void DeleteEvolvedKeyframeProp(u32 keyframePropId, const char* pEvoName);

		void StartProfileUpdate();
		void StartProfileDraw();

		void RescaleZoomableComponents(float scale);
		bool ContainsParticleRule(const char* pParticleRuleName);
#endif

#if RMPTFX_EDITOR || __RESOURCECOMPILER
		void Save();
#endif

		// debug
#if RMPTFX_BANK
		void DebugDraw(ptxEffectInst* pEffectInst, ptxEventInst* pEventInst);
#endif


	private: //////////////////////////

#if RMPTFX_EDITOR
		void UpdateEmitterRule(const char* pEmitterRuleName);
		void UpdateParticleRule(const char* pParticleRuleName);
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		ConstString m_emitterRuleName;											// the name of the emitter rule
		ConstString m_particleRuleName;											// the name of the particle rule
		datRef<ptxEmitterRule> m_pEmitterRule;									// pointer to the emitter rule
		datRef<ptxParticleRule> m_pParticleRule;								// pointer to the effect rule

		float m_playbackRateScalarMin;											// scalars and tints that get applied to the emitter
		float m_playbackRateScalarMax;
		float m_zoomScalarMin;
		float m_zoomScalarMax;
		Color32 m_colourTintMin;
		Color32 m_colourTintMax;

};


} // namespace rage


#endif // RMPTFX_PTXEVENT_H
