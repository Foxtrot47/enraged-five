// 
// rmptfx/ptxkeyframe.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXKEYFRAME_H
#define RMPTFX_PTXKEYFRAME_H


// includes (us)
#include "rmptfx/ptxchannel.h"
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmisc.h"

// includes
#include "atl/array.h"
#include "file/token.h"
#include "parser/macros.h"
#include "parsercore/attribute.h"
#include "system/timer.h"
#include "vector/color32.h"
#include "vector/vector4.h"
#include "vectormath/classes.h"
#include "vectormath/legacyconvert.h"


//defines 
#define PTXKEYFRAME_MAX_CHANNELS			(4)

#define PTXKEYFRAMEDEFN_COL_0				Color32(255, 0,   0,   255)
#define PTXKEYFRAMEDEFN_COL_1				Color32(0,   128, 0,   255)
#define PTXKEYFRAMEDEFN_COL_2				Color32(0,   0,   255, 255)
#define PTXKEYFRAMEDEFN_COL_3				Color32(128, 0,   128, 255)


// namespaces
namespace rage {


// forward declarations
class bkBank;
class parTreeNode;
class ptxByteBuff;
class ptxKeyframeWidget;


// enumerations
enum ptxKeyframeType
{
	PTXKEYFRAMETYPE_FLOAT			= 0,
	PTXKEYFRAMETYPE_FLOAT2,
	PTXKEYFRAMETYPE_FLOAT3,
	PTXKEYFRAMETYPE_FLOAT4
};


// structures
struct ptxKeyframeXmlData
{
	int m_numKeyEntries;
	atArray<float> m_keyEntryData;
	PAR_SIMPLE_PARSABLE;
};

struct ptxKeyframeSpec
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxKeyframeSpec() : m_pDefnName(NULL), m_pRuleName(NULL), m_ruleType(PTXRULETYPE_INVALID), m_propertyId(0) {}
		ptxKeyframeSpec(const char* pDefnName, const char* pRuleName, ptxRuleType ruleType, u32 propertyId) : m_pDefnName(pDefnName), m_pRuleName(pRuleName), m_ruleType(ruleType), m_propertyId(propertyId) {}

		PAR_SIMPLE_PARSABLE;


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		const char* m_pDefnName;
		const char* m_pRuleName;
		ptxRuleType m_ruleType;
		u32 m_propertyId;
};

struct ptxKeyframeDefn
{
	friend class ptxKeyframeWidget;

	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxKeyframeDefn();
		ptxKeyframeDefn(const char* pName, u32 propertyId, 
			ptxKeyframeType type, Vec4V_In vDefaultValue,
			Vec3V_In vMinMaxDeltaX, Vec3V_In vMinMaxDeltaY,
			const char* pLabel0=NULL, const char* pLabel1=NULL, 
			const char* pLabel2=NULL, const char* pLabel3=NULL,
			bool useColourGradient=false);

		ptxKeyframeDefn* Clone();

		const char* GetName() {return m_name;}
		Vec4V_Out GetDefaultValue() {return m_vDefaultValue;}
		u32 GetPropertyId() {return m_propertyId;}
		ptxKeyframeType GetType() const {return m_type;}
		bool GetUseColourGradient() {return m_useColourGradient;}

		// parser
		PAR_SIMPLE_PARSABLE;


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		const char* m_name;
		const char* m_labels[PTXKEYFRAME_MAX_CHANNELS];
		Color32 m_colours[PTXKEYFRAME_MAX_CHANNELS];
		Vec4V m_vDefaultValue;
		Vec3V m_vMinMaxDeltaX;
		Vec3V m_vMinMaxDeltaY;
		u32 m_propertyId;
		ptxKeyframeType m_type;
		bool m_useColourGradient;

};

#if PTXKEYFRAME_VERSION==0
struct ptxKeyframeEntry
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor
		ptxKeyframeEntry();

		// resourcing
		ptxKeyframeEntry(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxKeyframeEntry);

		// functions
		__forceinline void Set(ScalarV_In vTime, Vec4V_In vValue) {m_vTime=vTime; m_vValue=vValue; m_vDelta=Vec4V(V_ZERO);}
		__forceinline void ScaleValue(ScalarV_In vScale) {m_vValue *= vScale;}
		void ComputeDelta(const ptxKeyframeEntry& nextEntry);

		// accessors
		__forceinline ScalarV_Out GetTime() const {return m_vTime;}
		__forceinline Vec4V_Out GetValue() const {return m_vValue;}
		__forceinline Vec4V_Out Query(ScalarV_In vTime) const {return m_vValue + m_vDelta*(vTime-m_vTime);}

		// operator overloads
		void operator=(const ptxKeyframeEntry& other);
#if RMPTFX_EDITOR
		bool operator==(const ptxKeyframeEntry& other);
#endif

		// parser
		PAR_SIMPLE_PARSABLE


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		ScalarV m_vTime;												// the time of the keyframe entry
		Vec4V m_vValue;													// the value of the keyframe entry
		Vec4V m_vDelta;													// the pre calculated delta value to minimise keyframe interpolation costs


} ;

#elif PTXKEYFRAME_VERSION==1

struct ptxKeyframeEntry
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor
		ptxKeyframeEntry();

		// resourcing
		ptxKeyframeEntry(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxKeyframeEntry);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////
	public: ///////////////////////////

		Vec4V m_vTime;
		Vec4V m_vValue;


} ;

#endif


// classes
#if PTXKEYFRAME_VERSION==0

class ptxKeyframe
{
	friend class ptxKeyframeWidget;

	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor / destructor
		ptxKeyframe();
		~ptxKeyframe();

		// resourcing
		ptxKeyframe(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxKeyframe);

		// parser
		void PreLoadCallback(parTreeNode* pNode);
		void PostSaveCallback(parTreeNode* pNode);

		// 
		void Init(ScalarV_In vTime, Vec4V_In vValue);

		void AddEntry(ScalarV_In vTime, Vec4V_In vValue);
		void DeleteEntry(int entryId); // NOTE: DeleteEntry() does not call UpdateWidget() to avoid calling it multiple times per frame

		Vec4V_Out Query(ScalarV_In vTime) const;

		// accessors
		__forceinline int GetNumEntries() const {return m_subsequentEntries.GetCount()+1;}
		int GetEntryMem() const {return GetNumEntries()*sizeof(ptxKeyframeEntry);}
		float GetTime(int entryId) const;
		ScalarV_Out GetTimeV(int entryId) const;
		Vec4V_Out GetValue(int entryId) const;
		void SetEntry(int entryId, float time, Vec4V_In vValue);
		void SetDefn(ptxKeyframeDefn* pKeyframeDefn);
		ptxKeyframeDefn* GetDefn();

		// data copiers
		void CopyEntriesFrom(const ptxKeyframe& keyframe);
		void CopyDefnFrom(ptxKeyframe& keyframe);

		// bank and editor
#if RMPTFX_BANK || RMPTFX_EDITOR
		bool IsDefault();
		void ScaleEntries(ScalarV_In vScale);

		void DeleteDefn() {delete m_pDefn;}
#endif

		// operator overloads
		ptxKeyframe& operator=(const ptxKeyframe& other);
#if RMPTFX_EDITOR
		bool operator==(const ptxKeyframe& other);
#endif

		// bank
#if RMPTFX_BANK
		void Reset(int newSize);
		void AddWidgets(bkBank& bank);
		void UpdateWidget();
		ptxKeyframeWidget* GetWidget() {return m_pWidget;}
#endif 

	private: //////////////////////////

		ptxKeyframeEntry& GetEntry(int entryId);
		void ComputeDeltas();

		// parser
		PAR_SIMPLE_PARSABLE;


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		ptxKeyframeEntry m_initialEntry;
		atArray<ptxKeyframeEntry> m_subsequentEntries;

		ptxKeyframeWidget* m_pWidget;
		ptxKeyframeDefn* m_pDefn;

};

#elif PTXKEYFRAME_VERSION==1

class ptxKeyframe
{
	friend class ptxKeyframeWidget;

	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor / destructor
		ptxKeyframe();
		~ptxKeyframe();

		// resourcing
		ptxKeyframe(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxKeyframe);

		// parser
		void PreLoadCallback(parTreeNode* pNode);
		void PostSaveCallback(parTreeNode* pNode);

		// 
		void Init(ScalarV_In vTime, Vec4V_In vValue);

		void AddEntry(ScalarV_In vTime, Vec4V_In vValue);
		void DeleteEntry(int entryId); // NOTE: DeleteEntry() does not call UpdateWidget() to avoid calling it multiple times per frame

		Vec4V_Out Query(ScalarV_In vTime) const;

		// accessors
		__forceinline int GetNumEntries() const {return m_entries.GetCount();}
		int GetEntryMem() const {return GetNumEntries()*sizeof(ptxKeyframeEntry);}
		float GetTime(int entryId) const;
		ScalarV_Out GetTimeV(int entryId) const;
		Vec4V_Out GetValue(int entryId) const;
		void SetEntry(int entryId, float time, Vec4V_In vValue);
		void SetDefn(ptxKeyframeDefn* pKeyframeDefn);
		ptxKeyframeDefn* GetDefn();

		// data copiers
		void CopyEntriesFrom(const ptxKeyframe& keyframe);
		void CopyDefnFrom(ptxKeyframe& keyframe);

		// bank and editor
#if RMPTFX_BANK || RMPTFX_EDITOR
		bool IsDefault() const;
		void ScaleEntries(ScalarV_In vScale);

		void DeleteDefn() {delete m_pDefn;}
#endif
		
		// operator overloads
		ptxKeyframe& operator=(const ptxKeyframe& other);
#if RMPTFX_EDITOR
		bool operator==(const ptxKeyframe& other);
#endif

		// bank
#if RMPTFX_BANK
		void Reset(int newSize);
		void AddWidgets(bkBank& bank);
		void UpdateWidget();
		ptxKeyframeWidget* GetWidget() {return m_pWidget;}
#endif 

	private: //////////////////////////

		void ComputeDeltas();

		// parser
		PAR_SIMPLE_PARSABLE;


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		atArray<ptxKeyframeEntry> m_entries;

		ptxKeyframeWidget* m_pWidget;
		ptxKeyframeDefn* m_pDefn;

};

#elif PTXKEYFRAME_VERSION==2

enum ptxKeyframeEntryField
{
	PTXKEYFRAME_ENTRY_FIELD_TIME		= 0,
	PTXKEYFRAME_ENTRY_FIELD_INVDT,
	PTXKEYFRAME_ENTRY_FIELD_VALUE,
};


class ptxKeyframe
{
	friend class ptxKeyframeWidget;

	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor / destructor
		ptxKeyframe();
		~ptxKeyframe();

		// resourcing
		ptxKeyframe(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxKeyframe);

		// parser
		void PreLoadCallback(parTreeNode* pNode);
		void PostSaveCallback(parTreeNode* pNode);

		// 
		void Init(ScalarV_In vTime, Vec4V_In vValue);

		void AddEntry(ScalarV_In vTime, Vec4V_In vValue);
		void DeleteEntry(int entryId); // NOTE: DeleteEntry() does not call UpdateWidget() to avoid calling it multiple times per frame

		Vec4V_Out Query(ScalarV_In vTime) const;

		// accessors
		__forceinline int GetNumEntries() const {return m_entries.GetCount()/m_entryStride;}
		int GetEntryMem() const {return m_entries.GetCount()*sizeof(float);}
		float GetTime(int entryId) const;
		ScalarV_Out GetTimeV(int entryId) const;
		Vec4V_Out GetValue(int entryId) const;
		void SetEntry(int entryId, float time, Vec4V_In vValue);
		void SetDefn(ptxKeyframeDefn* pKeyframeDefn);
		ptxKeyframeDefn* GetDefn();

		// data copiers
		void CopyEntriesFrom(const ptxKeyframe& keyframe);
		void CopyDefnFrom(ptxKeyframe& keyframe);

		// bank and editor
#if RMPTFX_BANK || RMPTFX_EDITOR
		bool IsDefault();
		void ScaleEntries(ScalarV_In vScale);

		void DeleteDefn() {delete m_pDefn;}
#endif
		
		// operator overloads
		ptxKeyframe& operator=(const ptxKeyframe& other);
#if RMPTFX_EDITOR
		bool operator==(const ptxKeyframe& other);
#endif

		// bank
#if RMPTFX_BANK
		void Reset(int newSize);
		void AddWidgets(bkBank& bank);
		void UpdateWidget();
		ptxKeyframeWidget* GetWidget() {return m_pWidget;}
#endif 

	private: //////////////////////////

		__forceinline const float* _GetEntry(int entryId) const {return &m_entries[(entryId*m_entryStride)+PTXKEYFRAME_ENTRY_FIELD_TIME];}
		__forceinline float* _GetEntry(int entryId) {return &m_entries[(entryId*m_entryStride)+PTXKEYFRAME_ENTRY_FIELD_TIME];}
		void _SetEntry(int entryId, float time, const float* pValue);

		void ComputeDeltas();

		// parser
		PAR_SIMPLE_PARSABLE;


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		atArray<float> m_entries;

		ptxKeyframeWidget* m_pWidget;
		ptxKeyframeDefn* m_pDefn;

		int m_entryStride;
		datPadding<12> m_pad;

};

#endif // PTXKEYFRAME_VERSION





// code
__forceinline 
ptxKeyframeDefn::ptxKeyframeDefn()
{
	m_name = NULL;
	m_labels[0] = NULL;
	m_labels[1] = NULL;
	m_labels[2] = NULL;
	m_labels[3] = NULL;
	m_colours[0] = PTXKEYFRAMEDEFN_COL_0;
	m_colours[1] = PTXKEYFRAMEDEFN_COL_1;
	m_colours[2] = PTXKEYFRAMEDEFN_COL_2;
	m_colours[3] = PTXKEYFRAMEDEFN_COL_3;
	m_vDefaultValue = Vec4V(V_ZERO);
	m_vMinMaxDeltaX = Vec3V(0.0f, 1.0f, 0.01f);
	m_vMinMaxDeltaY = Vec3V(-100.0f, 100.0f, 0.01f);
	m_propertyId = 0;
	m_type = PTXKEYFRAMETYPE_FLOAT;
	m_useColourGradient = false;
}

__forceinline 
ptxKeyframeDefn::ptxKeyframeDefn(const char* pName, u32 propertyId, 
								 ptxKeyframeType type, Vec4V_In vDefaultValue, 
								 Vec3V_In vMinMaxDeltaX, Vec3V_In vMinMaxDeltaY,
								 const char* pLabel0, const char* pLabel1, 
								 const char* pLabel2, const char* pLabel3,
								 bool useColourGradient)
{
	m_name = pName;
	m_labels[0] = pLabel0;
	m_labels[1] = pLabel1;
	m_labels[2] = pLabel2;
	m_labels[3] = pLabel3;
	m_colours[0] = PTXKEYFRAMEDEFN_COL_0;
	m_colours[1] = PTXKEYFRAMEDEFN_COL_1;
	m_colours[2] = PTXKEYFRAMEDEFN_COL_2;
	m_colours[3] = PTXKEYFRAMEDEFN_COL_3;
	m_vDefaultValue = vDefaultValue;
	m_vMinMaxDeltaX = vMinMaxDeltaX;
	m_vMinMaxDeltaY = vMinMaxDeltaY;
	m_propertyId = propertyId;
	m_type = type;
	m_useColourGradient = useColourGradient;
}


#if PTXKEYFRAME_VERSION==0

__forceinline 
Vec4V_Out ptxKeyframe::Query(ScalarV_In vTime) const
{
#if RMPTFX_TIMERS
	sysTimer timer;
	timer.Reset();
#endif

	if (GetNumEntries()==0)
	{
		return m_pDefn->GetDefaultValue();
	}

	// find the correct keyframe entry
	const ptxKeyframeEntry* pKeyframeEntry = NULL;

	// use the initial entry if there is no subsequent entries or if our query time is below the first subsequent entry's key time
	int numSubsequentEntries = m_subsequentEntries.GetCount();
	if (numSubsequentEntries==0 || IsLessThanAll(vTime, m_subsequentEntries[0].GetTime()))
	{
		pKeyframeEntry = &m_initialEntry;
	}
	else
	{
		// it's not the initial entry - go through the subsequent entries looking for the first entry whose key time is after the query time
		int i=1;
		for (i=1; i<numSubsequentEntries; i++)
		{
			if (IsLessThanAll(vTime, m_subsequentEntries[i].GetTime()))
			{
				break;
			}
		}

		// return the previous entry - the one whose key time is just before our query time
		pKeyframeEntry = &m_subsequentEntries[i-1];
	}

#if RMPTFX_TIMERS
	// query the entry for the actual data value at this time
	Vec4V vResult = pKeyframeEntry->Query(vTime);
	ptxDebug::sm_accumKeyframeQueryTimeMs += timer.GetMsTime();
	ptxDebug::sm_accumKeyframeQueryCount++;
	return vResult;
#else
	// query the entry for the actual data value at this time
	return pKeyframeEntry->Query(vTime);
#endif
}

#elif PTXKEYFRAME_VERSION==1

__forceinline 
Vec4V_Out ptxKeyframe::Query(ScalarV_In vTime) const
{;
#if RMPTFX_TIMERS
	sysTimer timer;
	timer.Reset();
#endif

	int numEntries = GetNumEntries();
	if (numEntries==0)
	{
		return m_pDefn->GetDefaultValue();
	}

	Vec4V vResult = m_entries[numEntries-1].m_vValue;

	const ptxKeyframeEntry* pEntry = &m_entries[0];
	Vec4V vPrevTime = pEntry->m_vTime;
	Vec4V vPrevValue = pEntry->m_vValue;
	Vec4V vCurrTime;
	Vec4V vCurrValue;
	ScalarV vRatio;
	for (int i=1; i<numEntries; i++)
	{
		pEntry = &m_entries[i];
		vCurrTime = pEntry->m_vTime;
		vCurrValue = pEntry->m_vValue;

		if (IsLessThanAll(vTime, vCurrTime.GetX()))
		{
			vRatio = (vTime-vPrevTime.GetX()) * vCurrTime.GetY();
			vResult = Lerp(vRatio, vPrevValue, vCurrValue);

			break;
		}

		vPrevTime = vCurrTime;
		vPrevValue = vCurrValue;
	}	

#if RMPTFX_TIMERS
	ptxDebug::sm_accumKeyframeQueryTimeMs += timer.GetMsTime();
	ptxDebug::sm_accumKeyframeQueryCount++;
#endif

	return vResult;
}

#elif PTXKEYFRAME_VERSION==2

__forceinline 
Vec4V_Out ptxKeyframe::Query(ScalarV_In vTime) const
{;
#if RMPTFX_TIMERS
	sysTimer timer;
	timer.Reset();
#endif

	float time = vTime.Getf();
	int numEntries = GetNumEntries();
	if (numEntries==0)
	{
		return m_pDefn->GetDefaultValue();
	}

	const float* pResult = &_GetEntry(numEntries-1)[PTXKEYFRAME_ENTRY_FIELD_VALUE];
	float interpResult[4];

	const float* pPrevEntry = _GetEntry(0);
	const float* pCurrEntry;

	for (int i=1; i<numEntries; i++)
	{
		pCurrEntry = _GetEntry(i);

		if (time<pCurrEntry[PTXKEYFRAME_ENTRY_FIELD_TIME])
		{
			const float ratio = (time-pPrevEntry[PTXKEYFRAME_ENTRY_FIELD_TIME]) * pCurrEntry[PTXKEYFRAME_ENTRY_FIELD_INVDT];

			interpResult[0] = pPrevEntry[PTXKEYFRAME_ENTRY_FIELD_VALUE+0] + ratio*(pCurrEntry[PTXKEYFRAME_ENTRY_FIELD_VALUE+0]-pPrevEntry[PTXKEYFRAME_ENTRY_FIELD_VALUE+0]);
			interpResult[1] = pPrevEntry[PTXKEYFRAME_ENTRY_FIELD_VALUE+1] + ratio*(pCurrEntry[PTXKEYFRAME_ENTRY_FIELD_VALUE+1]-pPrevEntry[PTXKEYFRAME_ENTRY_FIELD_VALUE+1]);
			interpResult[2] = pPrevEntry[PTXKEYFRAME_ENTRY_FIELD_VALUE+2] + ratio*(pCurrEntry[PTXKEYFRAME_ENTRY_FIELD_VALUE+2]-pPrevEntry[PTXKEYFRAME_ENTRY_FIELD_VALUE+2]);
			interpResult[3] = pPrevEntry[PTXKEYFRAME_ENTRY_FIELD_VALUE+3] + ratio*(pCurrEntry[PTXKEYFRAME_ENTRY_FIELD_VALUE+3]-pPrevEntry[PTXKEYFRAME_ENTRY_FIELD_VALUE+3]);

			pResult = &interpResult[0];

			break;
		}

		pPrevEntry = pCurrEntry;
	}

	Vec4V vResult = Vec4V(pResult[0], pResult[1], pResult[2], pResult[3]);

#if RMPTFX_TIMERS
	ptxDebug::sm_accumKeyframeQueryTimeMs += timer.GetMsTime();
	ptxDebug::sm_accumKeyframeQueryCount++;
#endif

	return vResult;
}

#endif // PTXKEYFRAME_VERSION


} //namespace rage


#endif // RMPTFX_PTXKEYFRAME_H

