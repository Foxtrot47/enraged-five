// 
// rmptfx/ptxrandomtable.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXRANDOMTABLE_H
#define RMPTFX_PTXRANDOMTABLE_H


// includes (rage)
#include "vectormath/classes.h"


// defines
#define PTXRANDTABLE_SIZE		(256)


// namespaces
namespace rage
{


// classes
class ptxRandomTable
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		static void Generate();

		// accessors
		static void SetCurrIndex(u8 idx) {sm_currIndex = idx;}
		static u8 GetCurrIndex() {CompileTimeAssert(sizeof(sm_currIndex)==1 && PTXRANDTABLE_SIZE <= 256); return sm_currIndex++;}
		static u8* GetCurrIndexPtr() {return &sm_currIndex;}

 		static float GetValue() {return GetValue(sm_currIndex);}
 		__forceinline static ScalarV_Out GetValueV() {return GetValueV(sm_currIndex);}

		static float GetValue(u8 idx) {return sm_data[idx];}
		__forceinline static ScalarV_Out GetValueV(u8 idx) {return ScalarVFromF32(sm_data[idx]);}
		static float* GetDataPtr() {return sm_data;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////
	
		static u8 sm_currIndex ;
		static float sm_data[PTXRANDTABLE_SIZE];

};


} // namespace rage


#endif // RMPTFX_PTXRANDOMTABLE_H
