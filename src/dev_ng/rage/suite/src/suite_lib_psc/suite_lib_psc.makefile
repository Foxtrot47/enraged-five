Project suite_lib_psc
ProjectType util
ForceLanguage Cpp

RootDirectory ..

ParserExtract

Directory {
	..\avchat
	..\bink
	..\breakableglass
	..\cloth
#	..\compappcomms
	..\crclip
	..\crextra
#	..\crik
	..\crmotiontree
	..\crparameterizedmotion
	..\cutfile
	..\cutscene
	..\event
	..\fragment
	..\gpuptfx
	..\grrope
	..\move
	..\pedsafezone
	..\phglass
	..\rmptfx
	..\snet
	..\softrasterizer
}
