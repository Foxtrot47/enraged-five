#define NO_SKINNING
#include "../../../base/src/shaderlib/rage_common.fxh"
#include "../../../base/src/shaderlib/rage_grass.fxh"

BeginSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
    string UIName = "Diffuse Texture Grass";
ContinueSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
		AddressU = Wrap;
		AddressV = Clamp;
		AddressW = Wrap;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR; 
		LOD_BIAS = 0;
EndSampler;

struct grassVertexOut
{
	float4 pos				: DX_POS;
	float4 worldPos			: TEXCOORD0;
	float2 uv				: TEXCOORD1;
	float2 colorMapUV		: TEXCOORD2;
	float3 center			: TEXCOORD3;
	float  fade				: TEXCOORD4;
	float3 ScreenPos		: TEXCOORD5;
	float4 color			: COLOR0;
};

// Control variables
float GrassTimer : GrassTimer;
float FramesPerSecond : FramesPerSecond = 20.0f;
static const int FrameFadeDelay = 3;
float3 MoveDirection: MoveDirection = float3( 0.0f, 1.0f, 0.0f );
float MoveStretch : MoveStretch = 2.0f;

float3 AnimateGrassPosition( float uvy, float colw)
{
	float fps = FramesPerSecond;
	float rate= (float)( (GrassTimer * fps)  + colw * g_GrassTexSize.x  );//
	
	float dist = ( 1.0f - frac( rate ) ) - MoveStretch * fps * ( uvy) / 30.0f;
	dist = max( dist, 0.0f );
	
	return  MoveDirection * ( dist );
}
float CalculateFrameFade( float uvx, float colw )
{
	float rate= (float)( (int)(GrassTimer * FramesPerSecond) );//
	return ( (int)(rate + colw * g_GrassTexSize.x * FrameFadeDelay ) % FrameFadeDelay ==  0 );
}
#if __XENON

#pragma dcl position		// Another dirty, dirty lie

grassVertexOut VS_Grass( int i : INDEX )
{
	grassVertexOut OUT =(grassVertexOut)0;
	
	float4 vWorldPos;
	float2 uvs;
	float thinFade;
	float4 col;
	float scale;
	
	vWorldPos.xyz = GrassDecodeDetailsSimpleQuad(i, uvs, thinFade, col, scale );
	vWorldPos.w = 1.0f;
	
	// Animate texture uvs
	float fps = FramesPerSecond;

	vWorldPos.xyz += AnimateGrassPosition( uvs.y, col.w);
	
	//thinFade *= CalculateFrameFade( uvs.x, col.w );
	
	
	OUT.uv = uvs;
	OUT.pos = (float4)mul(vWorldPos, gWorldViewProj);
	OUT.center = vWorldPos.xyz;
	OUT.worldPos = vWorldPos;
	OUT.colorMapUV = uvs;
	OUT.color = 1.0f;//float4(gLightColor[0].xyz * col.xyz, 1.0f);
	
	float3 cameraDistance =  gViewInverse[3].xyz - vWorldPos.xyz;
	OUT.fade = 1.0f;//thinFade * 0.5f; 
	
	return OUT;
}
#else
grassVertexOut VS_Grass(grassVertexIn IN)
{
	grassVertexOut OUT =(grassVertexOut)0;
	
    
	// Rotate to face the camera
	float4 vWorldPos = float4(IN.center.xyz, 0.0f);
	
	OUT.uv = IN.uv.xy;
	//OUT.colorMapUV = IN.pos.xz;
	float2 uv = float2((vWorldPos.x - g_vFieldExtents.x) / (g_vFieldExtents.y - g_vFieldExtents.x), 
							(vWorldPos.z - g_vFieldExtents.z) / (g_vFieldExtents.w - g_vFieldExtents.z));
	
	OUT.pos = mul(vWorldPos, gWorldViewProj);
	OUT.center = IN.center.xyz;
	OUT.worldPos = vWorldPos;
	OUT.colorMapUV = uv;
	OUT.color = float4(1.0f, 1.0f, 1.0f, 1.0f);		// to do: PC grass coloring
	
	float3 cameraDistance =  gViewInverse[3].xyz - vWorldPos.xyz;
	OUT.fade = 1.0f - saturate (  length(cameraDistance) * g_FadeScale.x - g_FadeScale.y );
	
	return OUT;
}
#endif

float4 PS_Grass(grassVertexOut IN) : COLOR0
{

	// Lookup the diffuse color
	float4 diffuse = tex2D(DiffuseSampler, IN.uv) * IN.color;
		
	// Compute the final color
	float4 finalColor = float4( diffuse.xyz, diffuse.w * IN.fade);

	return finalColor;
}

technique draw
{
	pass p0
	{
		AlphaTestEnable = true;
		AlphaRef = 2;
		AlphaFunc = Greater;
		AlphaBlendEnable = true;	// RaY
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Grass();
		PixelShader  = compile PIXELSHADER  PS_Grass();
	}
}
