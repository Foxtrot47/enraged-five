// 
// grass/manager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "bank/bkmgr.h"
#include "file/asset.h"
#include "grass/field.h"
#include "grass/manager.h"
#include "grcore/image.h"
#include "grcore/instancerendererspu.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shader.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "parser/treenode.h"
#include "parsercore/attribute.h"
#include "system/memory.h"
#include "system/performancetimer.h"
#include "grprofile/pix.h"

#include <algorithm>

#define DEFAULT_FADE		20.0f
#define DEFAULT_FADE_RANGE	80.0f


// Just for metrics
#define GRASS_COUNT_PATCHES (0)

#define GRASS_OUTPUT_METRICS (0 && !__FINAL)


namespace rage
{

	// Static variables
	float grassManager::m_time = 0.0f;
	Vector3 grassManager::m_windDirection = Vector3( 1.0f, 0.0f, 1.0f );  // y up by default
	float grassManager::m_windStrength = 0.0f;
	float grassManager::m_gstartFadeOut = DEFAULT_FADE;
	float grassManager::m_gfinishFadeOut = DEFAULT_FADE_RANGE;
	float grassManager::m_gfadePower = 2.0f;
	float grassManager::m_gstartFadeOut2 = 0.1f;
	float grassManager::m_gfinishFadeOut2 = 0.1f;
	float grassManager::m_gfadePower2 = 2.0f;

grassManager::grassManager()
				: m_cameraFacing( true ),
					m_fadePower( 2.0f ),
					m_extra( 0 ),
					m_sort( true ),
					m_drawBounds(false),
					m_CurrentStreamId(0),
					m_texturePath(NULL),
					m_useCylinderGrass(true)
{
	m_StaggeredLoader = 0;
	m_startFadeOut = DEFAULT_FADE;
	m_finishFadeOut = DEFAULT_FADE + DEFAULT_FADE_RANGE;
	m_renderInterceptor = NULL;

#if __PPU
	grcInstanceRenderer::Init(512*1024);
#endif // __PPU
}

grassManager::~grassManager()
{
	WaitForTasks();

#if __PPU
	grcInstanceRenderer::Shutdown();
#endif // __PPU

	if (this)
	{
		ClearData();
	}
}

void grassManager::SetRenderInterceptor(renderInterceptor* newInterceptor)
{
	m_renderInterceptor = newInterceptor;
}


void grassManager::Add( FieldList* field ,  u32 streamingId)
{
	Assert( field );
	field->SetID( streamingId );
	m_Fields.Grow() = field;

	// Validate all patches in this field list
	int amt = field->el.GetCount();
	int patchCount = 0;
	atString* patchList = Alloca(atString, amt);
	for (int cnt = 0; cnt < amt; cnt++)
	{
		::new(&patchList[cnt]) atString();
	}

	field->GetAllPatchNames(patchList, patchCount);
	for (int cnt = 0; cnt < patchCount; cnt++)
	{
		{
			atPtr<grassPatchBase> patch = m_Patches[patchList[cnt]];
			if (!patch)
			{
				Assertf(0, "grass ERROR: a field was loaded that has a bad patch: %s", patchList[cnt].c_str());
			}
			else
			{
				// valid patch!  inc texture reference count
				patch->AddReference();
			}
		}
	}
	FieldList& f = *field;
	for (int i = 0; i< f.GetCount(); i++)
	{
		atPtr<grassPatchBase> patch = m_Patches[f[i].GetType()];
		if ( patch )
		{
			patch->AddList( const_cast<grassField*>(&f[i]) );
		}
	}
	

// resize patch list
	for (int cnt = 0; cnt < amt; cnt++)
	{
		patchList[cnt].~atString();
	}

	sortOnType();
}

void grassManager::SetPatch( const char* textureName, int NoTextureFrames  )
{
	grassPatch* patch = rage_new grassPatch();
	patch->SetCardSize(1.0f, 1.0f );
	patch->SetCardSizeVariance( 1.0f, 1.0f );
	patch->SetWidthTextureVariantCount( NoTextureFrames);
	patch->SetHeightTextureVariantCount(1);
	patch->SetTextureNames(textureName);

	patch->LoadTextures();

	SetPatch( "grass", patch );
}

void grassManager::SetPatch( const char* name, grassPatchBase* patch )
{
	Assertf(m_Patches[atString(name)] == NULL, "GRASS ERROR: duplicate patch name %s!", name);
	m_Patches[atString(name)] = patch;
}

int grassManager::GetFieldCount() const
{
	return m_Fields.GetCount();
}

FieldList* grassManager::GetFieldList( u32 ID ) const
{
	for( int i = 0; i < m_Fields.GetCount(); i++ )
	{
		if( m_Fields[i] && m_Fields[i]->GetID() == ID )
		{
			return m_Fields[i];
		}
	}
	return 0;
}

void grassManager::DestroyFields( u32 ID )
{
	int count = 0;
	for( int i = 0; i < m_Fields.GetCount(); i++ )
	{
		if( m_Fields[i] && m_Fields[i]->GetID() == ID )
		{
			delete m_Fields[i];
			count++;
			m_Fields[i] =m_Fields[ Max( m_Fields.GetCount() - count, 0 ) ];  // swap with end
		}
	}
	// clean up
	m_Fields.Resize(m_Fields.GetCount() - count );
}

////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		This function drops a fieldlist from the internal list, without freeing
//	its memory.  Why would you do this?
//
//		Because a pgStreamable fieldlist that gets added here should not be
//	deleted: pgStreamable owns the data, and will delete it.  Only use this
//	function if you know what you're doing!
//
void grassManager::ForgetField( u32 ID )
{
	int count = 0;
	for( int i = 0; i < m_Fields.GetCount(); i++ )
	{
		if( m_Fields[i] && m_Fields[i]->GetID() == ID )
		{
			// Dec reference count for all patches that this fieldlist uses, for texture management
			FieldList* field = m_Fields[i];
			DecReferenceCounts(field);

			//delete m_Fields[i];		The only difference between this function and DestroyFields.

			count++;
			m_Fields[i] = m_Fields[Max(m_Fields.GetCount() - count, 0)];  // swap with end
		}
	}
	// clean up
	m_Fields.Resize(m_Fields.GetCount() - count );
}


//------------------------------------------------------------------------------
//	PURPOSE
//		Helper function that decs the reference count for all patches in this fieldlist.
//
void grassManager::DecReferenceCounts(FieldList* field)
{
	int amt = field->el.GetCount();
	int patchCount = 0;
	atString* patchList = Alloca(atString, amt);
	for (int cnt = 0; cnt < amt; cnt++)
	{
		::new(&patchList[cnt]) atString();
	}

	field->GetAllPatchNames(patchList, patchCount);
	for (int cnt = 0; cnt < patchCount; cnt++)
	{
//		if (patchList[cnt] != NULL)
		{
			atPtr<grassPatchBase> patch = m_Patches[patchList[cnt]];
			if (patch)
			{
				// dec texture reference count
				patch->RemoveReference();
			}
		}
	}

	FieldList& f = *field;
	for (int j = 0; j< f.GetCount(); j++)
	{
		atPtr<grassPatchBase> patch = m_Patches[f[j].GetType()];
		patch->RemoveList( const_cast<grassField*>(&f[j]) );
	}

	for (int cnt = 0; cnt < amt; cnt++)
	{
		patchList[cnt].~atString();
	}
}


void grassManager::Update()
{
}

template< class T1 >
struct SortPairByFirst
{
	bool operator()(const T1& x, const T1& y) const 
	{ 
		return x.first < y.first; 
	}
};

template< class T1 >
struct SortBackToFrontBackPairByFirst
{
	bool operator()(const T1& x, const T1& y) const 
	{ 
		return x.first > y.first; 
	}
};

void grassManager::DrawIntoScreenShadowMap( const spdShaft& /*shaft*/ ) 
{
	DrawVisList( 1 );
}

void grassManager::DrawIntoShadowCollector(  )
{
//	Draw( shaft, 6 );
	DrawVisList(  2 );
}
void grassManager::DrawAlphaZPass( const spdShaft& /*shaft*/ )
{
	//	Draw( shaft, 6 );
	DrawVisList( 1 );
}
void grassManager::Draw()
{
	DrawVisList(0);
}
void grassManager::DebugDraw() 
{
	for( int i = 0; i < m_Fields.GetCount(); i++ )
	{
		m_Fields[ i ]->DebugDraw();
	}
}
bool sortOnTypeFunction( const FieldList* a, const FieldList* b )
{
	return a->GetType() < b->GetType();
}
void grassManager::sortOnType()
{
	// sorts on type
	std::sort( m_Fields.begin(), m_Fields.end() , sortOnTypeFunction );
}

// Static
void grassManager::SetWind(const Vector3& windDirection, float windStrength)
{
	m_windDirection = windDirection;
	m_windStrength = windStrength;
}

// Static
float grassManager::GetWindStrenth()
{
	return m_windStrength;
}



void grassManager::Draw( const spdShaft& shaft , int passIndex  ) 
{
	if ( m_drawBounds )
	{
		DebugDraw();
	}
	
	int amt = 0;
	for( int i = 0; i < m_Fields.GetCount(); i++ )
	{
		amt += m_Fields[i]->el.GetCount();
	}

	// Create one sort list for the whole function (to be reused by the inner loop).  Do not
	// create this in the loop because Alloca doesn't free its memory until the function
	// quits, so putting it in a loop will just accumulate memory and blow the stack (happening
	// in RDR2 as of 4/28/08) - PSS
	typedef std::pair<float, grassField*> SortPair;
	SortPair* sortList = Alloca(SortPair, amt);


	float startFade = m_gstartFadeOut;
	float finishFade = m_gfinishFadeOut;
	float fadePower = m_gfadePower;
	PatchTable::Entry* patchTuple;
	PIXBegin(0, "Grass Fields" );
	// Don't fade out in the shadow pass, or else nearby patches will be thinned out!
	if (passIndex == 1)
	{
		startFade = 100.0f;
		finishFade = 10000.0f;
		fadePower = 0.0f;
	}
	else if (passIndex ==  2 || passIndex == 6)
	{
		startFade = m_gstartFadeOut2;
		finishFade = m_gfinishFadeOut2;
		fadePower = m_gfadePower2;
	}

	// Set the world matrix to identity since the cards are already in world space
	grcViewport::SetCurrentWorldMtx(M34_IDENTITY);

	grcSmallStateStack states;		// setup render states

	states.Set<grcsAlphaToMask>(true);  // leave it up to higher level code to select offsets
	states.Set<grcsAlphaToMaskOffsets>(grcamoSolid);

	WaitForTasks();

	// Loop through all patches, draw all fields that use that patch.
	for (int i = 0; i < m_Patches.GetNumSlots(); i++)
	{
		// Walk all nodes in this hash chain
		patchTuple = m_Patches.GetEntry(i);
		while (patchTuple)
		{
			GrassRefList& fields = patchTuple->data->GetList();
			int fieldCount = fields.GetCount();
			if ( fieldCount == 0 || !patchTuple->data->CanDraw() )
			{
				patchTuple = patchTuple->next;
				continue;
			}
			PIXBegin(0, "Grass patch");
			int renderCount = 0;
			FieldGetVisible( fields, &shaft, 1,sortList, renderCount, startFade, finishFade, amt);

			// Any fields on screen?
			if (renderCount)
			{
				AssertMsg(patchTuple->data, "Trying to draw a grass when you don't have the shader setup");

				// Only want to update these variables once per frame, and the shadow pass comes first.  MAKE SURE GRASS draws in the shadow pass, or else it will never blow in the wind.
				if (passIndex == 2 || passIndex == 6) // 9/4/07: note: we aren't casting shadows from grass right now, so animate from pass 2
				{
					grassField::ResetCachedPatch();
					patchTuple->data->SetTime(m_time);
					patchTuple->data->SetWind(m_windDirection, m_windStrength);
				}

				if (m_sort)
				{
					// Sort front to back
					std::sort(sortList, sortList + renderCount , SortPairByFirst<SortPair>());
				}

				for (int i = 0; i < renderCount; i++)
				{
					// now draw the patches
					if ((m_renderInterceptor) && (passIndex == 0))
					{
						Vector3 location;
						sortList[i].second->GetFieldCenter(location);
						m_renderInterceptor->Process(&location);
					}
					sortList[i].second->Draw(m_lodSettings, patchTuple->data, m_useCylinderGrass, startFade, finishFade, fadePower, m_TaskHandles, passIndex);
				}
			}
			PIXEnd();
			patchTuple = patchTuple->next;
		}
	}
	grassField::ResetCachedPatch();

	
	PIXEnd();
}
void grassManager::CalcVisibleList( const grcViewport& view )
{
	// No spdShaft any more!
//	spdShaft shaft;
//	shaft.Set( view, RCC_MATRIX34(view.GetCameraMtx()) );
//	CalcVisibleList( &shaft, 1 );
}
void grassManager::CalcVisibleList( const spdShaft* shafts, int amountShafts )
{
	SortPair* sortList = m_grassDrawList.begin();
	m_grassDrawList.Reset();
	float startFade = m_gstartFadeOut;
	float finishFade = m_gfinishFadeOut;

	PatchTable::Entry* patchTuple;
	PIX_AUTO_TAG(0, "Grass Fields" );
	// Don't fade out in the shadow pass, or else nearby patches will be thinned out!

	int idx = 0;

	m_patchIndexList.Reset();
	int cnt = m_Patches.GetNumSlots();

	// Loop through all patches, draw all fields that use that patch.
	for (int i = 0; i < cnt; i++)
	{
		// Walk all nodes in this hash chain
		patchTuple = m_Patches.GetEntry(i);

		while (patchTuple )
		{
			GrassRefList& fields = patchTuple->data->GetList();
			int fieldCount = fields.GetCount();
			if ( fieldCount == 0 || !patchTuple->data->CanDraw() )
			{
				patchTuple = patchTuple->next;
				continue;
			}
			int renderCount = 0;
			FieldGetVisible( fields, shafts, amountShafts, sortList, renderCount, startFade, finishFade, m_grassDrawList.GetMaxCount()-idx );

			// Any fields on screen?
			if (renderCount)
			{
				AssertMsg(patchTuple->data, "Trying to draw a grass when you don't have the shader setup");
				if ( (idx + renderCount) >= (m_grassDrawList.GetMaxCount()-1))
				{
					m_grassDrawList.Resize(idx);
					return;
				}

				if (m_sort)
				{
					// Sort front to back
					std::sort(sortList, sortList + renderCount , SortPairByFirst<SortPair>());
				}
				sortList += renderCount;
				idx += renderCount;
				
				
				m_patchIndexList.Append() = std::make_pair(idx, patchTuple->data);
				if ( m_patchIndexList.GetCount() >= m_patchIndexList.GetMaxCount())
				{
					m_grassDrawList.Resize(idx);
					return;
				}
			}
			
			patchTuple = patchTuple->next;
		}
	}
	m_grassDrawList.Resize(idx);
}


void grassManager::DrawVisList( int passIndex  ) 
{
	PIX_AUTO_TAG(0, "Grass Fields" );

	// Set the world matrix to identity since the cards are already in world space
	grcViewport::SetCurrentWorldMtx(M34_IDENTITY);

	grcSmallStateStack states;		// setup render states

	states.Set<grcsAlphaToMask>(true);  // leave it up to higher level code to select offsets
	states.Set<grcsAlphaToMaskOffsets>(grcamoSolid);
	float startFade = m_gstartFadeOut;
	float finishFade = m_gfinishFadeOut;
	float fadePower = m_gstartFadeOut;
	
	if (passIndex ==  2 || passIndex == 6)
	{
		startFade = m_gstartFadeOut2;
		finishFade = m_gfinishFadeOut2;
		 fadePower = m_gstartFadeOut2;
	}


	SortPair* sortList = m_grassDrawList.begin();
	//float maxDistance = passIndex == 2 ? 
	int s = 0;

	WaitForTasks();
	for ( int t = 0; t < m_patchIndexList.GetCount(); t++ )
	{			
		int e = m_patchIndexList[t].first;
		Assert( e > s );
		
		grassPatchBase* patchTuple =  m_patchIndexList[t].second;

		// Only want to update these variables once per frame, and the shadow pass comes first.  MAKE SURE GRASS draws in the shadow pass, or else it will never blow in the wind.
		if (passIndex == 2 || passIndex == 6) // 9/4/07: note: we aren't casting shadows from grass right now, so animate from pass 2
		{
			grassField::ResetCachedPatch();
			patchTuple->SetTime(m_time);
			patchTuple->SetWind(m_windDirection, m_windStrength);
		}

		for (int i = s; i < e; i++)
		{
			// now draw the patches
			if (passIndex == 2 &&  sortList[i].first > m_lodSettings.farShadowDistance  )
			{
				grassField::ResetCachedPatch();
				return;
			}
			sortList[i].second->Draw( m_lodSettings, patchTuple, m_useCylinderGrass, startFade, finishFade, fadePower, m_TaskHandles, passIndex);
		}
		grassField::ResetCachedPatch();
		s = e;
	}
}

void grassManager::SetFades(const float startFadeOut, const float finishFadeOut, const float fadePower)
{
	m_gstartFadeOut = startFadeOut;
	m_gfinishFadeOut = finishFadeOut;
	if (m_gfinishFadeOut < m_gstartFadeOut)
		m_gfinishFadeOut = m_gstartFadeOut;
	m_gfadePower = fadePower;
}

void grassManager::SetCollectorFades(const float startFadeOut, const float finishFadeOut, const float fadePower)
{
	m_gstartFadeOut2 = startFadeOut;
	m_gfinishFadeOut2 = finishFadeOut;
	if (m_gfinishFadeOut2 < m_gstartFadeOut)
		m_gfinishFadeOut2 = m_gstartFadeOut;
	m_gfadePower2 = fadePower;
}
//////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Destroys all members, wipes the class.  This is a helper function,
//	used by the destructor and the load function.
//
void grassManager::ClearData()
{
	// Destroy all data before doing a load.
	for (int i = 0; i < m_Fields.GetCount(); i++)
	{
		if (m_Fields[i])
		{
			delete m_Fields[i];
			m_Fields[i] = NULL;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Wait for all tasks to complete
//
void grassManager::WaitForTasks()
{
	int taskCount = m_TaskHandles.GetCount();
	if (taskCount > 0)
	{
		sysTaskManager::WaitMultiple(taskCount, m_TaskHandles.GetElements());
		m_TaskHandles.ResetCount();
	}
}

//////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Removes all grass from a specified area.
//
int grassManager::ClearAreaOfGrass(const spdSphere &area)
{
	int clearCount = 0;

	for (int i = 0; i < m_Fields.GetCount(); i++)
	{
		if (m_Fields[i])
		{
			clearCount += m_Fields[i]->ClearAreaOfGrass(area);
		}
	}
	return clearCount;
}

bool grassManager::IsInGrass( const spdSphere& area )
{
	for (int i = 0; i < m_Fields.GetCount(); i++)
	{
		if (m_Fields[i] && m_Fields[i]->Intersects( area ) )
		{
			return true;
		}
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Prepares the grassManager for a resource save.  To save space and
//	reduce stream time, cut all data that will be reconstructed on load.
//
void grassManager::SlimForResourcing()
{
}


//////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Deletes all patches.
//
void grassManager::DestroyPatches()
{
	m_Patches.Kill();
}


//////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Sets the global texture path for grass.
//
void grassManager::SetTexturePath(const char* filepath)
{
	m_texturePath = filepath;
}

const char* grassManager::GetTexturePath()
{
	return m_texturePath;
}

} // namespace rage
