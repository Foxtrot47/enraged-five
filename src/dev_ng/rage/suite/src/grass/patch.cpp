// 
// grass/patch.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "grass/patch.h"
#include "grass/manager.h"
#include "file/asset.h"
#include "bank/bkmgr.h"
#include "grcore/texture.h"
#include "vectormath/classes.h"

#if __XENON
#include "grass/embedded_rage_grass_fxl_final.h"
#include "grass/embedded_rage_grass_animation_fxl_final.h"
#include "grass/embedded_rage_grass_rain_fxl_final.h"
#include "grass/embedded_rage_grass_pushable_fxl_final.h"
#elif __PPU
#include "grass/embedded_rage_grass_psn.h"
#include "grass/embedded_rage_grass_animation_psn.h"
#include "grass/embedded_rage_grass_rain_psn.h"
#include "grass/embedded_rage_grass_pushable_psn.h"
#elif __WIN32PC
#include "grass/embedded_rage_grass_win32_30.h"
#include "grass/embedded_rage_grass_animation_win32_30.h"
#include "grass/embedded_rage_grass_rain_win32_30.h"
#include "grass/embedded_rage_grass_pushable_win32_30.h"
#endif

#include "patch_parser.h"

namespace rage
{
grmShader*			grassPatch::sm_GrassShader = 0;
grcEffectVar		grassPatch::sm_DiffuseTexture;
grcEffectVar		grassPatch::sm_ColorMapTexture;
grcEffectVar		grassPatch::sm_NormalMapTexture;
grcEffectVar		grassPatch::sm_WindTexture;
grcEffectVar		grassPatch::sm_UseWindTexture;
grcEffectVar		grassPatch::sm_UseNormalMap;
grcEffectVar		grassPatch::sm_FieldExtents;
grcEffectVar		grassPatch::sm_FadeOutConstant;
grcEffectVar		grassPatch::sm_RandomOffsetTable;
grcEffectVar		grassPatch::sm_RandomUVsTable;
grcEffectVar		grassPatch::sm_ThinAmount;
grcEffectVar		grassPatch::sm_ThinFade;
grcEffectVar		grassPatch::sm_CameraFacing;
grcEffectVar		grassPatch::sm_ExtraDataTable;
grcEffectVar		grassPatch::sm_WindDirection;
grcEffectVar		grassPatch::sm_WindStrength;
grcEffectVar		grassPatch::sm_Timer;
grcEffectVar		grassPatchTextured::sm_GrassDoubleFadeDistId;

// Tweaks
grcEffectVar		grassPatch::sm_sizeTweak;
grcEffectVar		grassPatch::sm_biasTweak;

grcEffectVar		grassPatch::sm_GrassTexSize;

//grcEffectVar		grassPatch::sm_FogStart;
//grcEffectVar		grassPatch::sm_FogEnd;

const char*			grassPatch::sm_GrassShaderName = "rage_grass";
bool				grassPatch::sm_Overriden = false;











void grassPatchBase::PreDraw()
{
	// This body exists so we don't have to make this base class method a pure virtual,
	// which would require all grassPatch variants to implement it.
}

void grassPatchBase::AddReference()
{
	// ditto
}

void grassPatchBase::RemoveReference()
{
	// ditto
}

int grassPatchBase::GetRefCount()
{
	return 0;
}

void grassPatchBase::GetPlayerPosition(Vector3& pos, float& radius) const
{
	pos = VEC3V_TO_VECTOR3(grcViewport::GetCurrentCameraPosition());
	radius = 1.0f; // 1 metre radius by default
}

grassPatch::grassPatch() :
	m_CardSize(0, 0),
	m_CardSizeVariance(0, 0),
	m_WidthTextureVariants(0),
	m_HeightTextureVariants(0),
	m_ColorMapTexture(0),
	m_NormalMapTexture(0),
	m_Zup(false)
{
	memset(m_DiffuseName, 0, MAPNAME_LEN);
	memset(m_ColorMapName, 0, MAPNAME_LEN);
	memset(m_NormalMapName, 0, MAPNAME_LEN);
}

IMPLEMENT_PLACE(grassPatch);

grassPatch::grassPatch(datResource &UNUSED_PARAM(rsc))
{
	Assert(sm_GrassShaderName);
	m_NormalMapTexture = NULL;
	m_ColorMapTexture = NULL;
}



bool Create( atPtr<grcTexture>& tex, const char* fName )
{
	grcTexture *texTmp = grcTextureFactory::GetInstance().Create( fName );
	if(texTmp)
	{
		tex = atCreateResourceAtPtr( texTmp );
	}
	else
	{
		Assertf(0, "Failed to load grass texture '%s', probably out of physical memory.", fName);
		tex = NULL;
	}
	return tex != 0;
}

grassPatch::~grassPatch()
{
	if (m_ColorMapTexture)
	{
		m_ColorMapTexture->Release();
		m_ColorMapTexture = NULL;
	}
	if (m_NormalMapTexture)
	{
		m_NormalMapTexture->Release();
		m_NormalMapTexture = NULL;
	}
}

void grassPatch::InitClass()
{
	if ( !sm_Overriden )
	{
		if( sm_GrassShader )
			ShutdownClass();

		sm_GrassShader = grmShaderFactory::GetInstance().Create();
		sm_GrassShader->Load(sm_GrassShaderName);
	}

	sm_DiffuseTexture = sm_GrassShader->LookupVar("DiffuseTex", false);
	sm_ColorMapTexture = sm_GrassShader->LookupVar("ColorMapTex", false);
	sm_NormalMapTexture = sm_GrassShader->LookupVar("NormalMapTexGrass", false);
	sm_WindTexture = sm_GrassShader->LookupVar("WindTexture", false);
	sm_UseWindTexture = sm_GrassShader->LookupVar("UseWindTexture", false);
	sm_UseNormalMap = sm_GrassShader->LookupVar("UseNormalMap", false);
	sm_FieldExtents = sm_GrassShader->LookupVar("FieldExtents", false);
	sm_FadeOutConstant = sm_GrassShader->LookupVar("Fade", false);

	sm_GrassTexSize = sm_GrassShader->LookupVar("GrassTexSize", false);

#if __XENON
	sm_GrassTexSize = sm_GrassShader->LookupVar("GrassTexSize", false);
	sm_RandomOffsetTable = sm_GrassShader->LookupVar("GrassRandomOffsetTable");
	sm_RandomUVsTable  = sm_GrassShader->LookupVar("GrassRandomUVsTable");
	//sm_ThinAmount = sm_GrassShader->LookupVar("GrassThinAmount");
	sm_ThinFade = sm_GrassShader->LookupVar("GrassThinFade");
	sm_CameraFacing  = sm_GrassShader->LookupVar("GrassCameraFacing");
	sm_ExtraDataTable = sm_GrassShader->LookupVar("GrassExtraVertData");
	sm_WindDirection = sm_GrassShader->LookupVar("GrassWindDirection", false);
	sm_WindStrength = sm_GrassShader->LookupVar("GrassWindStrength", false);
	sm_Timer = sm_GrassShader->LookupVar("GrassTimer", false);
	sm_sizeTweak = sm_GrassShader->LookupVar("GrassScale", false);
	sm_biasTweak = sm_GrassShader->LookupVar("GrassBias", false);
#endif
}

void grassPatch::ShutdownClass()
{
	if( sm_GrassShader )
		delete sm_GrassShader;
	sm_GrassShader = 0;
}

const Vector2& grassPatch::GetCardSize() const
{
	return m_CardSize;
}
void grassPatch::SetShader( const char* ShaderName )
{
	if (sm_GrassShader)
		delete sm_GrassShader;
	sm_GrassShader = 0;

	sm_GrassShaderName = ShaderName;
	sm_Overriden = true;

	sm_GrassShader = grmShaderFactory::GetInstance().Create();
	sm_GrassShader->Load(sm_GrassShaderName);
}
grmShader* grassPatch::GetShader()
{
	Assert( sm_GrassShader != 0 );
	return sm_GrassShader;
}
const Vector2& grassPatch::GetCardSizeVariance() const
{
	return m_CardSizeVariance;
}

void grassPatch::SetCardSize(float width, float height)
{
	m_CardSize.Set(width, height);
}

void grassPatch::SetCardSizeVariance(float widthVariance, float heightVariance)
{
	m_CardSizeVariance.Set(widthVariance, heightVariance);
}

int grassPatch::GetWidthTextureVariantCount() const
{
	return m_WidthTextureVariants;
}

int grassPatch::GetHeightTextureVariantCount() const
{
	return m_HeightTextureVariants;
}

void grassPatch::SetWidthTextureVariantCount(int count)
{
	m_WidthTextureVariants = count;
}

void grassPatch::SetHeightTextureVariantCount(int count)
{
	m_HeightTextureVariants = count;
}

const char* grassPatch::GetDiffuseName() const
{
	return m_DiffuseName;
}

const char* grassPatch::GetColorMapName() const
{
	return m_ColorMapName;
}


////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		This function will load all textures for this grassPatch, using the
//	internal texture names stored in the class.
//
void grassPatch::LoadTextures()
{
	// Need this one!
	if (!m_DiffuseName)
		return;

	LoadTextures(m_DiffuseName, NULL, m_NormalMapName);
}


void grassPatch::SetTextureNames(const char* diffuse)
{
	memcpy(m_DiffuseName, diffuse, sizeof(m_DiffuseName));
}


void grassPatch::LoadTextures(const char* diffuse, const char* colorMap, const char* normalMap)
{

	Create( m_DiffuseTexture, diffuse );

	if (colorMap)
		m_ColorMapTexture = grcTextureFactory::GetInstance().Create(colorMap);
	if( normalMap )
	{
#if !HACK_RDR2
		//MAK - disable normal maps. We're not using them for grass at this point.
		if (normalMap[0])
		{
			m_NormalMapTexture = grcTextureFactory::GetInstance().Create(normalMap);
			memcpy(m_NormalMapName, normalMap, sizeof(m_NormalMapName));
		}
#endif // !HACK_RDR2
	}

	memcpy(m_DiffuseName, diffuse, sizeof(m_DiffuseName));

	if (colorMap)
		memcpy(m_ColorMapName, colorMap, sizeof(m_ColorMapName));
}

void grassPatch::SetZup(bool useZup)
{
	m_Zup = useZup;
}

void grassPatch::SetDataTables( Vector3* offsets, Vector2* uvs, Vector4* extraData, int size ) const
{
	size = 1;
	sm_GrassShader->SetVar( sm_RandomOffsetTable, offsets, size );
	sm_GrassShader->SetVar( sm_RandomUVsTable, uvs, size );
	sm_GrassShader->SetVar( sm_ExtraDataTable, extraData, size );
}

void grassPatch::SetThinning( int ThinStart, int ThinEnd )const		// per sub field 
{
	float divisor = Max((float)( ThinEnd - ThinStart), 1.0f );
	Vector2 thin( 1.0f/divisor, (float) ThinStart );

	thin.x = 0.0f;

	sm_GrassShader->SetVar( sm_ThinFade, thin );
}

void grassPatch::SetCameraFacing( bool cameraFacing ) const
{
	sm_GrassShader->SetVar( sm_CameraFacing, cameraFacing );
}

void grassPatch::SetWind( const Vector3& windDirection, float windStrength)
{
	sm_GrassShader->SetVar(sm_WindDirection, windDirection);
	sm_GrassShader->SetVar(sm_WindStrength, windStrength);
}

void grassPatch::GetWind(Vector3& windDirection, float& windStrength) const
{
	sm_GrassShader->GetVar(sm_WindDirection, windDirection);
	sm_GrassShader->GetVar(sm_WindStrength, windStrength);
}

void grassPatch::SetTweakers(float sizeTweak, float biasTweak)
{
	sm_GrassShader->SetVar(sm_sizeTweak, sizeTweak);
	sm_GrassShader->SetVar(sm_biasTweak, biasTweak);
}


////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		The time variable is used as a pulse to drive the motion of the grass
//	in the vertex shader.  Generally, this should be updated once a frame.
//
void grassPatch::SetTime(float time)
{
	sm_GrassShader->SetVar(sm_Timer, time);
}

float grassPatch::GetTime() const
{
	float time;
	sm_GrassShader->GetVar(sm_Timer, time);
	return time;
}

void grassPatch::SetQuantisationScaleAndOffset( const Vector3& scale, const Vector3& offset )  // per sub field 
{
	static grcEffectGlobalVar	sm_GlobalScale = grcegvNONE;
	static grcEffectGlobalVar	sm_GlobalBias = grcegvNONE;

	if ( sm_GlobalScale == grcegvNONE)
	{
		sm_GlobalScale = grcEffect::LookupGlobalVar( "GlobalScale", false);
		sm_GlobalScale = grcEffect::LookupGlobalVar( "GlobalBias", false);

	}
	if ( sm_GlobalScale != grcegvNONE)
	{
		grcEffect::SetGlobalVar( sm_GlobalScale, VECTOR4_TO_VEC4V(Vector4(scale)));
		grcEffect::SetGlobalVar( sm_GlobalBias, VECTOR4_TO_VEC4V(Vector4(offset)));
	}
}

bool grassPatch::BeginDraw(const Vector4& extents,  int passIndex   ) const
{

	if (m_DiffuseTexture)
	{
		sm_GrassShader->SetVar(sm_DiffuseTexture, m_DiffuseTexture);
		sm_GrassShader->SetVar(sm_ColorMapTexture, m_DiffuseTexture);			// RAY swapped to get around wrapping bug
	}


	// Setup Normal Map
	if( m_NormalMapTexture )
	{
		sm_GrassShader->SetVar(sm_NormalMapTexture, m_NormalMapTexture);
		sm_GrassShader->SetVar(sm_UseNormalMap, 1.0f);
	}
	else
	{
		sm_GrassShader->SetVar(sm_UseNormalMap, 0.0f);
	}

	// Setup Wind Texture
	sm_GrassShader->SetVar(sm_UseWindTexture, 0.0f);

	// Set field extents
	sm_GrassShader->SetVar(sm_FieldExtents, extents);

	grcEffectTechnique technique = grcetNONE;


	// Bind the shader
	AssertVerify( sm_GrassShader->BeginDraw(grmShader::RMC_DRAW, true, technique ) );
	sm_GrassShader->Bind( passIndex);

	return true;
}


void grassPatch::EndDraw() const
{
	// Unbind the shader
	sm_GrassShader->UnBind();
	sm_GrassShader->EndDraw();
}



void grassPatchBase::AddList( grassField* field )
{
	m_fields.Grow() = field;
}
void grassPatchBase::RemoveList( grassField* field )
{
	for (int i =0; i < m_fields.GetCount(); i++ )
	{
		if ( m_fields[i] == field )
		{
			m_fields[i] = m_fields[m_fields.GetCount()-1];
			m_fields.Resize( m_fields.GetCount()-1);
			return;
		}
	}
	AssertMsg( 0, "Field not found in patch , strange...");
}

// Grass patch textured class -----------------------------
grassPatchTextured::~grassPatchTextured(){ delete m_Shader; }

void grassPatchTextured::SetTweakers(float sizeTweak, float biasTweak, float plant)
{
	m_Shader->SetVar(m_sizeTweakID, sizeTweak);
	m_Shader->SetVar(m_biasTweakID, biasTweak);
	m_Shader->SetVar(m_plantTweakID, plant);
}

void grassPatchTextured::SetAlphaFades(float offset, float slope)
{
	Vector2 fadeOffsetSlope = Vector2(offset, slope);
	m_Shader->SetVar(m_fadeOffsetSlopeID, fadeOffsetSlope);
}

void grassPatchTextured::GetAlphaFades(float& offset, float& slope) const
{
	Vector2 fadeOffsetSlope;
	m_Shader->GetVar(m_fadeOffsetSlopeID, fadeOffsetSlope);
	offset = fadeOffsetSlope.x;
	slope = fadeOffsetSlope.y;
}

void grassPatchTextured::SetQuantisationScaleAndOffset( const Vector3& scale, const Vector3& offset ) // per sub field 
{
	static grcEffectGlobalVar	sm_GlobalScale = grcegvNONE;
	static grcEffectGlobalVar	sm_GlobalBias = grcegvNONE;

	if ( sm_GlobalScale == grcegvNONE)
	{
		sm_GlobalScale = grcEffect::LookupGlobalVar( "GlobalScale", false);
		sm_GlobalBias = grcEffect::LookupGlobalVar( "GlobalBias", false);
	}
	if ( sm_GlobalScale != grcegvNONE)
	{
		grcEffect::SetGlobalVar( sm_GlobalScale, VECTOR4_TO_VEC4V(Vector4(scale)));
		grcEffect::SetGlobalVar( sm_GlobalBias, VECTOR4_TO_VEC4V(Vector4(offset)));
	}
}

void grassPatchTextured::SetThinning( int ThinStart, int ThinEnd )const		// per sub field 
{
	float divisor = Max((float)( ThinEnd - ThinStart), 1.0f );
	Vector2 thin( 1.0f/divisor, (float) ThinStart );
	thin.x = 0.0f;//
	m_Shader->SetVar( m_ThinFadeID , thin );
}

void grassPatchTextured::SetTime(float time)
{
	m_Shader->SetVar( m_TimerID , time);
}

float grassPatchTextured::GetTime() const
{
	float time;
	m_Shader->GetVar( m_TimerID, time);
	return time;
}

void grassPatchTextured::SetDoubleFade( float dist, float range )
{
	m_Shader->SetVar( sm_GrassDoubleFadeDistId, Vector2( dist, 1.0f/range));
}

void grassPatchTextured::GetDoubleFade( float& dist, float& invRange ) const
{
	Vector2 distInvRange;
	m_Shader->GetVar( sm_GrassDoubleFadeDistId, distInvRange);
	dist = distInvRange.x;
	invRange = distInvRange.y;
}

//------------------------------------------------------------------------------
//	PURPOSE
//		Incs the reference count for this patch.  Called when a field streams in
//	that uses it.  This is used to manage textures.
//
void grassPatchTextured::AddReference()
{
	m_DiffuseTexRefCount++;

	// First one?  Time to load the texture then
	if (m_DiffuseTexRefCount == 1)
	{
		Assert(!m_diffuseTexture); // shouldn't be loaded yet
		if (!m_diffuseTexture)
		{
			// TO DO: non-blocking textures.  Change the grass texture exporter to run
			// RORC at the end of the format conversion, and then async load them
			// as resources here.
			LoadTextures();
		}
	}
}


//------------------------------------------------------------------------------
//	PURPOSE
//		Decs the reference count for this patch.  Called when a field streams out.
//
void grassPatchTextured::RemoveReference()
{
	Assert(m_DiffuseTexRefCount);
	if (m_DiffuseTexRefCount > 0)
	{
		m_DiffuseTexRefCount--;

		// Time to go?
		if (m_DiffuseTexRefCount == 0)
		{
			/*Assert(m_diffuseTexture);*/
			if (m_diffuseTexture)
			{
				//m_diffuseTexture->Release();
				ReleaseTextures();
				m_diffuseTexture = NULL;
			}
		}
	}
}


int grassPatchTextured::GetRefCount()
{
	return m_DiffuseTexRefCount;
}


void grassPatchTextured::PreDraw()
{
	if (m_diffuseTexture)
	{
		m_Shader->SetVar(m_DiffuseTextureID, m_diffuseTexture);
	}
}


bool grassPatchTextured::BeginDraw( const Vector4& /*extents*/,  int passIndex   ) const
{
#if 0 && HACK_RDR2
	// We don't want to override the technique so the application
	// itself can decide what to use. /MAK
	grcEffectTechnique technique = grcetNONE;
#else
	grcEffectTechnique technique = m_drawTechnique;
#endif // HACK_RDR2

	// Bind the shader
	int iMaxPasses = m_Shader->BeginDraw(grmShader::RMC_DRAW, true, technique );
	Assert(iMaxPasses);
	Assert(passIndex < iMaxPasses);
	if (passIndex < iMaxPasses)
	{
		m_Shader->Bind( passIndex );
		return true;
	}
	return false;
}
void grassPatchTextured::EndDraw() const
{
	// Unbind the shader
	m_Shader->UnBind();
	m_Shader->EndDraw();

	m_Shader->SetVar(m_DiffuseTextureID, (grcTexture*)grcTexture::None);
}

void grassPatchTextured::LoadTextures()
{
	if (m_DiffuseTextureName[0] != '\0')
	{
		AssertVerify(Create(m_diffuseTexture, m_DiffuseTextureName));	}
#if !HACK_RDR2
	//MAK - disable normal maps. We're not using them for grass at this point.
	if (m_NormalMapName[0] != '\0')
	{

		AssertVerify( Create( m_normalMap, m_NormalMapName ) );
		m_Shader->SetVar( m_NormalMapID, m_normalMap );
	}
#endif // !HACK_RDR2
}

void grassPatchTextured::SetDiffuseTexture( const char* textureName , int NumFrames )
{	
	safecpy( m_DiffuseTextureName, textureName, MaxNumberCharactersInString );
	m_NumFrames = NumFrames;
//	LoadTextures();
}

void grassPatchTextured::SetNormalMap(const char* textureName)
{
	// Assume normal map has the same number of frames as the diffuse texture
	safecpy(m_NormalMapName, textureName, MaxNumberCharactersInString);
}


grassPatchTextured::grassPatchTextured( const char* shaderName ) : m_diffuseTexture(NULL), m_normalMap(NULL), m_NumFrames(1), m_DiffuseTexRefCount(0)
{
	m_Shader = grmShaderFactory::GetInstance().Create( );
	m_Shader->Load( shaderName );

	m_DiffuseTextureID = m_Shader->LookupVar("DiffuseTex", false);
	m_NormalMapID = m_Shader->LookupVar("NormalTex", false);
	m_FadeOutConstantID  = m_Shader->LookupVar("Fade", false);

	m_GrassTexSizeID  = m_Shader->LookupVar("GrassTexSize", false);
	m_ThinAmountID  = m_Shader->LookupVar("GrassThinAmount", false);
	m_ThinFadeID  = m_Shader->LookupVar("GrassThinFade", false);
	m_QuantScaleID  = m_Shader->LookupVar("GrassQuantScale", false);
	m_QuantOffsetID  = m_Shader->LookupVar("GrassQuantOffset", false);
	m_TimerID  = m_Shader->LookupVar("GrassTimer", false);
	m_sizeTweakID = m_Shader->LookupVar("GrassScale", false);
	m_biasTweakID = m_Shader->LookupVar("GrassBias", false);
	m_plantTweakID = m_Shader->LookupVar("GrassYPlant", false);


	sm_GrassDoubleFadeDistId = m_Shader->LookupVar("GrassDoubleFadeDist", true);


	m_DiffuseTextureName[0]='\0';
	m_NormalMapName[0]='\0';

	// Set tweaker defaults
	m_Shader->SetVar(m_sizeTweakID, 1.0f);
	m_Shader->SetVar(m_biasTweakID, 0.0f);
	m_Shader->SetVar(m_plantTweakID, 0.0f);

	m_drawTechnique = grcetNONE;
#if HACK_RDR2
	m_drawTechnique = m_Shader->LookupTechnique("grass_draw",true);
#else
	m_drawTechnique = m_Shader->LookupTechnique("draw");
#endif // HACK_RDR2

	// Fading out grass
	m_fadeOffsetSlopeID = m_Shader->LookupVar("FadeOffsetSlope");
}

} // namespace rage
