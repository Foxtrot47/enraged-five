// 
// grass/field.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "atl/ptr.h"
#include "bank/bank.h"
#include "data/safestruct.h"
#include "file/asset.h"
#include "grass/field.h"
#include "grass/manager.h"
#include "grass/patch.h"
#include "grass/rage_grasscommon.h"
#include "grass/staticparticles.h"
#include "grass/wind.h"
#include "grcore/im.h"
#include "grcore/indexbuffer.h"
#include "grcore/instancerendererspu.h"
#include "grcore/image.h"
#include "grcore/state.h"
#include "grcore/viewport.h"
#include "grmodel/shader.h"
#include "grmodel/modelfactory.h"
#include "math/float16.h"
#include "paging/rscbuilder.h"
#include "spatialdata/aabb.h"
#include "spatialdata/sphere.h"
#include "system/codefrag_spu.h"
#include "system/memory.h"
#include "system/performancetimer.h"
#include "system/platform.h"
#include "system/task.h"
#include "system/timemgr.h"
#include "mathext/noise.h"

#if __XENON
#include "system/xtl.h"
#include <d3d9.h>
#endif // __XENON

#include <algorithm>

#define MAX_PATCHES_PER_FIELD	(64000)
#define MINIMUM_PATCH_HEIGHT	(0.2f)
#define SPAWN_KIDS_HEIGHT		(0.1f)
#define SPAWN_KID_MULTIPLIER	(4.0f)

#if __XENON
	#define VERTEX_TO_INDEX_RATIO (4)
#else
	#define VERTEX_TO_INDEX_RATIO (1)
#endif
#define VERTEX_TO_INDEX_RATIO_HIGH (12)

#if GRASS_SPU_INSTANCE_RENDERER
#include "grcore/wrapper_gcm.h"
DECLARE_FRAG_INTERFACE(rage_grasshigh);
DECLARE_FRAG_INTERFACE(rage_grasslow);
#endif // GRASS_SPU_INSTANCE_RENDERER

//#pragma optimize("",off)

namespace rage
{

const float kFloat16_Min = 6.1035156e-5f;

// temporary until we can get the this placed somewhere nice
void ConvertFromU32( Vector4& v, u32 val )
{
	Color32 col( val );
	v = Vector4( col.GetRedf(), col.GetGreenf(), col.GetBluef(), col.GetAlphaf() );
}
void ConvertFromU32( float& v, u32 val )
{
	Color32 col( val );

	v = col.GetAlphaf();
}
void ConvertFromU32( Vector2& v, u32 val )
{
	/*u16 red = (u16)( interval.x * (float)( 1 << 16 ) );
	u16 green = (u16)( interval.y * (float)( 1 << 16) );
	u32 encodedValue = ((u32) green <<16 )| (u32)red; */

	u16 red = (u16)( val & (0xFFFF));
	u16 green = (u16)( val >> 16 );

	v.x = (float)red  * ( 1.0f/(float)(1<<16));
	v.y = (float)green  * ( 1.0f/(float)(1<<16));
}

template<class T>
	T BilinearFilterRead( const grcImage* image, float u, float v )
{
	float px =(float)image->GetWidth() * u;
	float py =(float)image->GetHeight() * v;

	int x = (int)px;		// TODO need fast int conversion
	int y = (int)py;		// need fast int conversion
	int x2 = x + 1;
	int y2 = y + 1;

	Assert(x < image->GetWidth());
	Assert(y < image->GetHeight());

	if (x2 > image->GetWidth()-1) x2 = x;
	if (y2 > image->GetHeight()-1) y2 = y;

	float fx = px - x;		//	Calculate the coordinates of the top left pixel
	float fy = py - y;

	Vector4 wieghts( (1.0f-fx) * (1.0f-fy),
		(fx)  * (1.0f -fy),
		(1.0f-fx) *  (fy) ,
		(fx)  *  (fy) );

	// now get four values
	T v1;
	T v2;
	T v3;
	T v4;

	ConvertFromU32( v1, image->GetPixel(x , y  ) );
	ConvertFromU32( v2, image->GetPixel(x2 , y  ) );
	ConvertFromU32( v3, image->GetPixel(x , y2  ) );
	ConvertFromU32( v4, image->GetPixel(x2, y2  ) );

	return v1 * wieghts.x + v2 * wieghts.y + v3 * wieghts.z + v4 * wieghts.w;
}

Vector3		grassField::sm_OffsetTable[ RandomTableSize];
Vector2		grassField::sm_UVsTable[ RandomTableSize ] ;
Vector4		grassField::sm_ExtraVertData[ RandomTableSize ] ;
bool			grassField::sm_built = false;
bool			grassField::sm_updated = false;

grcIndexBuffer*  grassField::sm_UnSortedIndexBuffer = 0;
grcVertexBuffer* grassField::sm_HighBladeVertexBuffer = 0;

static int sm_ClassCount = 0;


bool	grassField::sm_GrassTransition = true;
bool	grassField::sm_GrassDoubleDraw = true;


////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(grassField);

grassField::grassField(datResource &rsc)
	: m_Type(rsc), m_tp(0)
{
	m_TypeHash = atStringHash(m_Type);
	//m_extra = NULL;

	m_VertexDeclaration = grmModelFactory::BuildDeclarator(m_VertexBuffer->GetFvf(), 0, 0, 0 );
	m_aabbScale.w = 0.0f;
	m_aabbOffset.w = 16000.0f;

#if !HACK_RDR2
	PostPlace();
#endif // !HACK_RDR2
}

void grassField::PostPlace()
{
	SetupStaticIndexBuffer( VERTEX_TO_INDEX_RATIO_HIGH, 2048 );
	sm_ClassCount++;
}

grassField::grassField( atPtr<ParticleList> pl ) :
	m_Zup(false),
	m_useSortedBuffers( false ),
	m_tp(0)
{
	Create( pl );
	sm_ClassCount++;
}
grassField::grassField(  ParticleList& pl ) :
m_Zup(false),
m_useSortedBuffers( false ),
m_tp(0)
{
	sm_ClassCount++;
	Create( pl );
	sm_ClassCount++;
}

grassField::grassField(ParticleList& pl, const char* type) :
m_Zup(false),
m_useSortedBuffers( false ),
m_tp(0),
m_Type(type)
{
	Create( pl );
	sm_ClassCount++;
}

grassField::grassField() :
	m_Zup(false),
	m_VertexDeclaration(0),
	m_VertexBuffer(0),
	m_useSortedBuffers( false ),
	m_tp(0),
	m_Type("")
{
	sm_ClassCount++;

	for( int i = 0; i < gfevqCount; i++ )
	{
		m_IndexBuffers[i] = 0;
	}

	if (!sm_built)
	{
		for (int i = 0; i < RandomTableSize; ++i)
		{
			sm_OffsetTable[i].Set( 0.0f, 0.0f, 0.0f );
			sm_UVsTable[i].Set( 0.0f, 0.0f );
			sm_ExtraVertData[i].Set( 0.0f, 0.0f, 0.0f, 0.0f );
		}
    }
}

grassField::~grassField()
{
	grmModelFactory::FreeDeclarator( m_VertexDeclaration );

	if ( --sm_ClassCount == 0 && sm_UnSortedIndexBuffer )
	{
		//grassFields are no longer allowed to delete the static index buffer.  This totally fucks up resourcing.
		if (!sysMemAllocator::GetCurrent().IsBuildingResource()) 
		{
			delete sm_UnSortedIndexBuffer;
			sm_UnSortedIndexBuffer = 0;
		}
	}
	if (m_VertexBuffer)
	{
		delete m_VertexBuffer;
		m_VertexBuffer = NULL;
	}

	for (int i = 0; i < gfevqCount; i++)
	{
		if (m_IndexBuffers[i])
			delete m_IndexBuffers[i];
	}

	
}

#if __DECLARESTRUCT
void grassField::DeclareStruct(datTypeStruct& s)
{
	m_TypeHash = atStringHash(m_Type);

	SSTRUCT_BEGIN(grassField)
	SSTRUCT_FIELD(grassField, m_Extents)
	SSTRUCT_FIELD(grassField, m_aabbMax)
	SSTRUCT_FIELD(grassField,m_aabbMin)
	SSTRUCT_FIELD(grassField,m_aabbScale)
	SSTRUCT_FIELD(grassField,m_aabbOffset)
	SSTRUCT_FIELD_VP(grassField, m_tp)
	SSTRUCT_FIELD_VP(grassField,m_VertexDeclaration)
	SSTRUCT_FIELD(grassField,m_VertexBuffer)
	SSTRUCT_CONTAINED_ARRAY(grassField, m_IndexBuffers)
	SSTRUCT_FIELD(grassField,  m_Zup )
	SSTRUCT_FIELD(grassField, m_useSortedBuffers)
	SSTRUCT_CONTAINED_ARRAY(grassField, m_Pad0)
	SSTRUCT_FIELD(grassField, m_Type)
	SSTRUCT_FIELD(grassField, m_TypeHash)
	SSTRUCT_CONTAINED_ARRAY(grassField, m_Pad1)
	SSTRUCT_END(grassField)
}
#endif



//////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Creates the the list of blade locations and heights, given an input
//	color map.  This version of Create() uses a second extents parameter
//	to figure out where in the color map to center itself.  This is useful
//	if you are subdividing fields.
//
u32 grassField::Create(const grcImage* fieldColorMap, const grcImage* fieldColorMap2, const Vector4& boxExtents, const Vector4& maxExtents, float placementResolution, const Vector4& subImage, const Vector2& cardSize, int widthTextureVariantCount )
{
	bool toldEm = false;
	mthRandom rnd;
	int patchIndex = 0;
	u32 spawnedKids = 0;

	m_Extents = boxExtents;
	//m_Resolution = placementResolution;

	float fWidth = m_Extents.y - m_Extents.x;
	float fDepth = m_Extents.w - m_Extents.z;
	int nCellsW = (int)(fWidth / placementResolution);
	int nCellsD = (int)(fDepth / placementResolution);
	float maxWidth = maxExtents.y - maxExtents.x;
	float maxHeight = maxExtents.w - maxExtents.z;
	float stepX = (fWidth * 0.5f) / (float)(nCellsW);
	float stepZ = (fDepth * 0.5f) / (float)(nCellsD);


	// Make sure we have enough memory to work!
	u32 sizeNeeded = ((sizeof(Vector16) * MAX_PATCHES_PER_FIELD) + (sizeof(Float16) * MAX_PATCHES_PER_FIELD) + (sizeof(Color32) * MAX_PATCHES_PER_FIELD));
	u32 sizeGot = sysMemAllocator::GetCurrent().GetMemoryAvailable();
	if (sizeNeeded > sizeGot)	// x2 because of temporary memory
	{
		// This actually means we're out of memory.  But we're out of memory because the fields that were placed before this one took up too much.
		Printf("ERROR: There is too much grass on this field!\nTurn your GridResolution back up, or carve out more of your channel map.\n");
		return 0;
	}

	// Allocate some temp space for the maximum available patches
	static Vector3* tempPositions;
	static float* tempHeights;
	static Color32* tempColor;

	sysMemStartTemp();
	tempPositions = rage_new Vector3[MAX_PATCHES_PER_FIELD];
	tempHeights = rage_new float[MAX_PATCHES_PER_FIELD];
	tempColor = rage_new Color32[MAX_PATCHES_PER_FIELD];
	sysMemEndTemp();

	// Find the patches that actually exist in the color map
	for( int w = 0; w < nCellsW; w++ )
	{
		for( int d = 0; d < nCellsD; d++ )
		{
			bool placeIt = true;
			u32 spawnCount = 1;

			// Calculate the (u,v) pair for this position.  (fX,fY) stay in world space!
			float fX = m_Extents.x + ((float)w * placementResolution);
			float fY = m_Extents.z + ((float)d * placementResolution);
			float u = (fX - maxExtents.x) / maxWidth;
			float v = (fY - maxExtents.z) / maxHeight;
			u = Clamp(u, 0.0f, 1.0f);
			v = Clamp(v, 0.0f, 1.0f);

			u = u * (subImage.y - subImage.x) + subImage.x;
			v = v * (subImage.w - subImage.z) + subImage.z;

			// Get the color for this texel
			Vector4 color = BilinearFilterRead<Vector4>(fieldColorMap, u, v);
			float height = color.w;


			// Rule-based thinning
			if ((height * cardSize.y) < MINIMUM_PATCH_HEIGHT)
			{
				// Don't place grass if it's too small.
				placeIt = false;
			}
			else if ((height * cardSize.y) < SPAWN_KIDS_HEIGHT)
			{
				static float scalar = 1.0f / (SPAWN_KIDS_HEIGHT - MINIMUM_PATCH_HEIGHT);
				float kids;
				u32 kidCount;

				// We want to convert height to [0,1], inversely, so that the smallest height possible will generate the most child patches.
				kids = 1.0f - (SPAWN_KIDS_HEIGHT - MINIMUM_PATCH_HEIGHT) - ((height * cardSize.y) - MINIMUM_PATCH_HEIGHT) * scalar;

				// Booster
				kids += 0.5f;
				kids *= SPAWN_KID_MULTIPLIER * kids;

				kidCount = (u32)(rnd.GetRanged(0.0f, kids));
				spawnCount += kidCount;
				spawnedKids += kidCount;
			}

			// If everything succeeeded, place the grass
			if (placeIt)
			{
				for (u32 c = 0; c < spawnCount;c++)
				{
					if (patchIndex < MAX_PATCHES_PER_FIELD)
					{

						float fX2 = fX;
						float fY2 = fY;

						if (c == 0)
						{
							tempHeights[patchIndex] = height * cardSize.y;
							fX2 += rnd.GetRanged(-stepX, stepX);
							fY2 += rnd.GetRanged(-stepZ, stepZ);
						}
						else
						{
							float childHeight = rnd.GetRanged(height * 0.9f, height * 0.8f);
							childHeight = Max(kFloat16_Min, childHeight);
							tempHeights[patchIndex] = childHeight * cardSize.y;
							fX2 += rnd.GetRanged(-stepX * 0.75f, stepX * 0.75f);
							fY2 += rnd.GetRanged(-stepZ * 0.75f, stepZ * 0.75f);
						}

						int texNo = patchIndex;
						if ( fieldColorMap2 )
						{
							Vector4 color2 = BilinearFilterRead<Vector4>(fieldColorMap, u, v);
							texNo = (int)(color2.x * widthTextureVariantCount );
						}

						float wOffset = (float)(texNo % widthTextureVariantCount)/ (float)widthTextureVariantCount ;

						tempColor[patchIndex].Setf(color.x, color.y, color.z, wOffset );

						if (m_Zup)
						{
							tempPositions[patchIndex].Set(fX2, fY2, 0.0f);
						}
						else
						{
							tempPositions[patchIndex].Set(fX2, 0.0f, fY2);
						}

						patchIndex++;
					}
					else
					{
						if (!toldEm)
						{
							toldEm = true;
							Printf("ERROR: There is too much grass on this field!\nTurn your GridResolution back up, or carve out more of your channel map.\n");
						}
					}
				}
			}
		}
	}


	// Slim the lists down to their necessary size
	if (patchIndex != 0)
	{
		m_tp = rage_new texPlacementValues;
	
		m_tp->m_Patches.Resize(patchIndex);
		m_tp->m_HeightScales.Resize(patchIndex);
		m_tp->m_Color.Resize(patchIndex);
		memcpy(&m_tp->m_Patches[0], tempPositions, patchIndex * sizeof(Vector3));
		memcpy(&m_tp->m_HeightScales[0], tempHeights, patchIndex * sizeof(float));
		memcpy(&m_tp->m_Color[0], tempColor, patchIndex * sizeof(Color32));
	}

	sysMemStartTemp();
	delete[] tempPositions;
	delete[] tempHeights;
	delete[] tempColor;
	sysMemEndTemp();

	/*
	if (spawnedKids > 0)
	{
		Printf("Spawned %u kids\n", spawnedKids);
	}
	*/

	return patchIndex;
}

void grassField::GetGrassPatches(Vector3** outGrassPatchPointers, int& outGrassPatchCount)
{
	*outGrassPatchPointers = &(m_tp->m_Patches[0]);
	outGrassPatchCount = m_tp->m_Patches.GetCount();
}

const Vector4& grassField::GetExtents() const
{
	return m_Extents;
}



void grassField::GetExtents(Vector4 &fieldExtents)
{
	memcpy(&fieldExtents, &m_Extents, sizeof(Vector4));
}


void grassField::GetFieldCenter(Vector3& out)
{
	out = m_aabbMin + m_aabbMax;
	out.Scale(0.5f);
}


void grassField::SetZup(bool useZup)
{
	m_Zup = useZup;
}

int DistanceCompare(const float* a, const float* b)
{
	float fA = *a;
	float fB = *b;
	if( fA < fB )
		return -1;
	else if( fA > fB )
		return 1;
	
	return 0;
}

struct DirectionSort
{
	float	dist;
	int		idx;

	DirectionSort( float d, int i ) : dist( d ), idx( i )
	{}
	DirectionSort()
	{}
};
int DistanceCompareDirections( const DirectionSort* a, const DirectionSort* b )
{
	float fA = a->dist;
	float fB = b->dist;
	if( fA < fB )
		return -1;
	else if( fA > fB )
		return 1;

	return 0;
}

static void SetPrimitiveToIndexBuffer( u16 *lockPtr, int index, int patchIndex, int NoVertsPerPrimitive )
{
	for ( int i = 0; i<NoVertsPerPrimitive; i++ )
	{
		Assign(lockPtr[index + i], patchIndex + i);
	}
}

bool grassField::IsVisible( const spdShaft* UNUSED_PARAM(shafts), int UNUSED_PARAM(amtShafts),  float& UNUSED_PARAM(outZ), float& UNUSED_PARAM(endZ)  )
{
#if 0
	spdAABB	aabb;
	aabb.Set( m_aabbMin, m_aabbMax );

	for (int i = 0; i< amtShafts; i++ )
	{
		if ( shafts[i].IsVisible( aabb ) )
		{
			Vector3 centre = ( m_aabbMax + m_aabbMin ) * 0.5f;
			Vector3 radius = ( m_aabbMax - m_aabbMin ) * 0.5f;
			float size = radius.Mag();
			//Vector4 sphere = Vector4( centre.x, centre.y, centre.z, size );
			//grcViewport::GetCurrent()->IsSphereVisible( sphere,outZ  );
			Matrix34 camMtx = MAT34V_TO_MATRIX34(grcViewport::GetCurrent()->GetCameraMtx());

			Vector3  viewDir = centre - camMtx.d;
			outZ = viewDir.Dot( -camMtx.c);

			endZ = outZ + size;
			outZ -= size;	
			return true;
		}
	}
	return false;
#else
	return true;
#endif
}

void grassField::SetupRandomTables( const Vector2& cardSize,  const Vector2& cardSizeVariance, int cardWidthTexVariants,  int /*cardHeightTexVariants*/ )
{
	if ( sm_built )
	{
		return;
	}
	
	// Since our sm_UVsTable is static and applies to all fields, force our texture settings of 4x1.
//	cardWidthTexVariants = 4;
//	cardHeightTexVariants = 1;

	float widthTexVariantOffset = 1.0f / (float)cardWidthTexVariants;
	//float heightTexVariantOffset = 1.0f / (float)cardHeightTexVariants;
	mthRandom rnd;

	// setup the random tables for so as to reduce memory usage
	for ( int i = 0; i < RandomTableSize; i+= 4 )
	{
		Vector3 vTopLeftPos = VEC3_ZERO;
		Vector3 vTopRightPos = VEC3_ZERO;
		Vector3 vBottomLeftPos = VEC3_ZERO;
		Vector3 vBottomRightPos = VEC3_ZERO;

		// calculate random values
		float rndW = (rnd.GetRanged(-1.0f, 1.0f) * cardSizeVariance.x);
		//float rndH = (rnd.GetRanged(-1.0f, 1.0f) * cardSizeVariance.y);
		float angle = (rnd.GetRanged(0.0f, 1.0f) * PI * 2.0f );

		// Calculate positions
		float scale = 1.0f;
		float width =  scale * (cardSize.x + rndW) ;

		vTopLeftPos.z = width * cosf( angle );
		vTopRightPos.z = -width * cosf( angle );
		vBottomLeftPos.z = width * cosf( angle );
		vBottomRightPos.z = -width * cosf( angle );

		vTopLeftPos.x = width* sinf( angle );
		vTopRightPos.x = -width * sinf(angle );
		vBottomLeftPos.x = width * sinf(angle );
		vBottomRightPos.x = -width * sinf(angle );

		vTopLeftPos.y = 1.0f;//Max(0.0f, ((cardSize.y + rndH) * scale  ));
		vTopRightPos.y = 1.0f;//Max(0.0f, ((cardSize.y + rndH) *scale)  );

		// and put in random table
		sm_OffsetTable[i + 0 ] = vTopLeftPos;
		sm_OffsetTable[i + 1 ] = vTopRightPos;
		sm_OffsetTable[i + 2 ] = vBottomRightPos;
		sm_OffsetTable[i + 3 ] = vBottomLeftPos;




		// Just walk along the filmstrip for now.  Note: your grass texture must be a 4x1 now!
		Vector2 offset;
		offset.x = widthTexVariantOffset * (i / 4);
		offset.y = offset.x + widthTexVariantOffset;

		Vector2 vTopLeftUV(offset.x,  0.0f);
		Vector2 vTopRightUV(offset.y, 0.0f);
		Vector2 vBottomLeftUV(offset.x, 1.0f);
		Vector2 vBottomRightUV(offset.y, 1.0f);

		// and put in random table
		sm_UVsTable[i + 0 ] = vTopLeftUV;
		sm_UVsTable[i + 1 ] = vTopRightUV;
		sm_UVsTable[i + 2 ] = vBottomRightUV;
		sm_UVsTable[i + 3 ] = vBottomLeftUV;
	}

	sm_built = true;
}	
struct ConvertPositionAndBinder
{
	Vector3		m_aabbInvScale;
	Vector3		m_aabbOffset;

	ConvertPositionAndBinder( const Vector3& invScale, const Vector3& offset )
		: m_aabbOffset( offset ), m_aabbInvScale( invScale )
	{}
	Vector3  Convert( const Vector3& position, float /*scale*/ )
	{
		//Vector3 minFloat16V( kFloat16_Min, kFloat16_Min,  kFloat16_Min );
		Vector3 pos = position;

		pos = ( pos  -  m_aabbOffset) * m_aabbInvScale;

		// so we can compress the coordinateness
		Assert( pos.x >= 0.0f && pos.x <= 1.0f );
		Assert( pos.y >= 0.0f && pos.y <= 1.0f );
		Assert( pos.z >= 0.0f && pos.z <= 1.0f );

		//pos.Max( pos, minFloat16V );		// clamp to minium float value
		return pos;//Vector4( pos.x, pos.y, pos.z, scale );
	}
};


class  GrassVertexCreator
{
public:
	GrassVertexCreator( ParticleList& pl, const Vector3& invScale, const Vector3& offset )
		: m_posScaler( invScale, offset )
	{
		m_OneOverNumTexFrames = 1.0f/ (float)pl.GetNumberTextureFrames();
		pl.Randomize();
	}
	float operator()( int vi, const Particle& p, grcVertexBuffer* v )
	{
		Vector3 centre = m_posScaler.Convert( p.GetPosition(), p.Scale );

		grcVertexBufferEditor editor(v);
		editor.SetPosition(vi,centre);
		editor.SetCPV( vi, Vector4(p.Color.x, p.Color.y, p.Color.z,p.Scale) );

		return p.Scale;
	}
private:
	float		m_OneOverNumTexFrames;
	ConvertPositionAndBinder m_posScaler;
};

Vector3 grassField::SetBounds( const Vector3& min, const Vector3& max )
{
	m_aabbMin = min;
	m_aabbMax = max;

	m_aabbScale.SetVector3( m_aabbMax - m_aabbMin );
	m_aabbScale.w = 0.0f;
	m_aabbOffset.SetVector3(  m_aabbMin );
	m_aabbOffset.w = 16000.0f;


	Vector3		aabbInvScale = m_aabbScale.GetVector3();
	aabbInvScale.InvertSafe();
	return aabbInvScale;
}

void grassField::CreateVertexBuffer( grcFvf& fvf,  const Vector2& cardSize,  const Vector2& cardSizeVariance, int cardWidthTexVariants,  int cardHeightTexVariants  )
{

	SetupRandomTables( cardSize, cardSizeVariance, cardWidthTexVariants,  cardHeightTexVariants );

	const bool useInstancing = g_sysPlatform == platform::XENON || g_sysPlatform == platform::PS3;
	int quadVertCount = useInstancing ? 1 : 6;

	// Setup the vertex buffer
	mthRandom rnd;
	Assert( !m_VertexBuffer );
	if( !m_VertexBuffer )
		m_VertexBuffer = grcVertexBuffer::Create(m_tp->m_Patches.GetCount() * quadVertCount, fvf, g_sysPlatform == platform::PS3);

	// Randomly shuffle the positions
	int*  permutation = Alloca( int, m_tp->m_Patches.GetCount());
	for ( int i = 0; i < m_tp->m_Patches.GetCount(); i++ )
	{
		permutation[i] =i;
	}
	std::random_shuffle( permutation, permutation + m_tp->m_Patches.GetCount() );

	// calculate bounding box
	for( int i = 0; i < m_tp->m_Patches.GetCount(); i++ )
	{
		Vector3 pos;
		pos.Set(m_tp->m_Patches[permutation[i]].x, m_tp->m_Patches[permutation[i]].y, m_tp->m_Patches[permutation[i]].z);

		// update the bounds
		m_aabbMax.Max( m_aabbMax,  pos);   // could create one max / main
		m_aabbMin.Min( m_aabbMin,   pos);

	}

	Vector3		aabbInvScale = SetBounds( m_aabbMin, m_aabbMax );

	grcVertexBufferEditor editor(m_VertexBuffer);
	const float kFloat16_Min = 6.1035156e-5f;
	Vector3 minFloat16V( kFloat16_Min, kFloat16_Min,  kFloat16_Min );

	for ( int i = 0; i < m_tp->m_Patches.GetCount(); i++ )
	{
		Vector3 pos;
		pos.Set(m_tp->m_Patches[permutation[i]].x, m_tp->m_Patches[permutation[i]].y, m_tp->m_Patches[permutation[i]].z);
		
		// rescale to bounding box size
		pos = ( pos  -  m_aabbOffset.GetVector3()) * aabbInvScale;

		// so we can compress the coordinents
		Assert( pos.x >= 0.0f && pos.x <= 1.0f );
		Assert( pos.y >= 0.0f && pos.y <= 1.0f );
		Assert( pos.z >= 0.0f && pos.z <= 1.0f );

		pos.Max( pos, minFloat16V );		// clamp to minium float value
		float scale =  m_tp->m_HeightScales[permutation[i]];
		float rndH = rnd.GetRanged(0.9f, 1.0f);

		// the xennon version uses a lookup table so it onlys needs to store one position
		Vector4 centre = Vector4( pos.x, pos.y, pos.z, scale * rndH);


		if ( useInstancing )
		{
			// memory is currently 1/16th PC version
			editor.SetPositionAndBinding(i, centre);
			editor.SetCPV(i, m_tp->m_Color[permutation[i]]);
		}
		else
		{
			int j = i * 6;

			for ( int k= 0; k < 6; k++ )
			{
				editor.SetPositionAndBinding(j + k, centre);
				editor.SetCPV( j + k, m_tp->m_Color[permutation[i]]);
			}

			Vector2 tluv( 0.0f, 1.0f );
			Vector2 truv( 1.0f, 1.0f );
			Vector2 bluv( 0.0f, 0.0f );
			Vector2 bruv( 1.0f, 0.0f );

			editor.SetUV(j+ 0, 0, tluv);
			editor.SetUV(j+ 1, 0, truv);
			editor.SetUV(j+ 2, 0, bluv);
			editor.SetUV(j+ 3, 0, truv);
			editor.SetUV(j+ 4, 0, bruv);
			editor.SetUV(j+ 5, 0, bluv);

		}

	}
}

////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Removes all grass in this field that lies in the region defined.
//
int grassField::ClearAreaOfGrass(const spdSphere& area)
{
	int clearCount = 0;

	spdAABB aabb;
	aabb.Set( RCC_VEC3V(m_aabbMin), RCC_VEC3V(m_aabbMax) );
	if ( !aabb.IntersectsSphere( area ) )
	{
		return clearCount;
	}
	if (m_VertexBuffer)
	{
		int IndexCount = m_VertexBuffer->GetVertexCount();
		Vector3 clearLocation = VEC3V_TO_VECTOR3(area.GetCenter());
		grcVertexBufferEditor editor(m_VertexBuffer);
		Vector4 zero;
		zero.Zero();
		float radius = area.GetRadius().Getf() + 5.0f;
		float invRadius = 1.0f/radius;
		for (int cnt = 0; cnt < IndexCount; cnt++)
		{
			Vector3 posIn;
			Vector4 posOut;
			Vector3 actualLocation;
			Vector3 delta;

			posIn = editor.GetPosition(cnt);

			// rescale to bounding box size
			actualLocation = posIn;
			actualLocation = (actualLocation * m_aabbScale.GetVector3()) + m_aabbOffset.GetVector3();
			delta = actualLocation - clearLocation;
			Vector2 deltaXZ = Vector2(delta.x, delta.z);
			float deltaMag = deltaXZ.Mag();
			if (deltaMag < radius)
			{
				static float NoiseFreq = 4.0f;
				static float NoiseOffset = 26.0f;
				static float NoiseScale = 20.0f;

				deltaMag *= invRadius;
				deltaMag *= deltaMag;
				deltaMag *= deltaMag;

				Color32 col = editor.GetCPV(cnt);

				// This doesn't actually kill this patch of grass, but it does shrink it down to nothing.
				float scale = deltaMag *( PerlinNoise::RawNoise(actualLocation.x * NoiseFreq, actualLocation.z * NoiseFreq)* NoiseScale+NoiseOffset);
				if ( scale > 1.0f)
				{
					scale = 1.0f;
				}
				if ( scale < 0.5f)
				{
					scale = 0.0f;
				}
				editor.SetCPV( cnt, Vector4( col.GetRedf(), col.GetGreenf(), col.GetBluef(), col.GetAlphaf() * scale ));
				clearCount++;
			}
		}
	}

	return clearCount;
}

////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Frees the shared static index buffer.
//
void grassField::DestroyStaticIndexBuffer()
{
	if (sm_UnSortedIndexBuffer)
	{
		delete sm_UnSortedIndexBuffer;
		sm_UnSortedIndexBuffer = 0;
		}

}

Vec3V BentVerts[6] =  { 
	Vec3V( 0.0f,0.0f, 0.0f),		
	Vec3V( 0.0f, 0.0f, 1.0f),
	Vec3V( 0.0f, 1.0f, 1.0f ),			
	Vec3V( 0.0f, 1.0f, 0.0f ),			
	Vec3V( 1.0f, 1.0f, 0.0f ),
	Vec3V( 1.0f, 0.0f, 0.0f ) } ;

#define NUM_HIGH_GRASS_VERTS 12
#define NUM_RAND_ROTATIONS 16
#define HALF_ROTATIONS ((float)NUM_RAND_ROTATIONS * 0.5f)
#define RANDOM_BATCH_SIZE  ( NUM_HIGH_GRASS_VERTS*NUM_RAND_ROTATIONS )

Vec3V_Out GetRotationTangent( int vertex, float angleOffset)
{
	float angleDir =(((float) (vertex% NUM_RAND_ROTATIONS ) - HALF_ROTATIONS ) /HALF_ROTATIONS ) * 3.142f;
	angleDir+= angleOffset;
	Vec3V rotation = Vec3V( cosf( angleDir), 0.0f, sinf( angleDir) );
	return rotation;
}
Vec3V GetNormalDirection( int BladeId, Vec2V_In axiasScale, float angleOffset )
{
	Vec3V rot = GetRotationTangent( BladeId, angleOffset );
	return  Vec3V( rot.GetXf()* axiasScale.GetXf(),-axiasScale.GetYf() ,  rot.GetZf()* axiasScale.GetXf());
}

float RandRatio( int idx, int range )
{
	return (float)(idx%range)/((float)range);
}
Vector3 GrassDecodeClippedCylinderVertPos(int i, Vector3& uvs, float angleFadeIn )
{
	int vertex =(int) floor( (i + 0.5f) / (float)NUM_HIGH_GRASS_VERTS );  
	int v2 =((int)floor(( i + 0.5f) / 6.0f ) * 6);  
	int index = i - v2;
	int index2 = i - vertex * NUM_HIGH_GRASS_VERTS;

	Vec3V rotation =GetRotationTangent( vertex, 0.0f);

	Vec3V baseOffset = (index2 > 5) ? -rotation : rotation;

	const float angleTurn =0.5f *  PI * RandRatio((index2 > 5) ? vertex : vertex +NUM_RAND_ROTATIONS ,NUM_RAND_ROTATIONS*2);
	float rotAngle = angleTurn;//(index2 > 5) ?  angleTurn  : angleTurn;
	rotAngle *= angleFadeIn;

	Vec3V vert = BentVerts[ index ] ;
	uvs = Vector3( vert.GetXf(), vert.GetYf(), 0.0f);

	// calculate edge
	uvs.x +=0.5f * vert.GetZf();
	vert.SetZf( vert.GetZf() * angleFadeIn);

	//angleFadeIn = 0.0f;
	Vec2V axiasScale = (Vec2V(uvs.x, uvs.y) - Vec2V(0.5f, 1.0f)) *ScalarVFromF32(2.0f) * Vec2V( 0.9f,0.99f);
	axiasScale.SetX( vert.GetZf() * 0.5f + axiasScale.GetXf() );

	Vec3V dirScale = Vec3V(V_ONE)+ Vec3V( vert.GetZf(),0.0f, vert.GetZf() )*ScalarVFromF32(0.4f);

	Vec3V Normal =GetNormalDirection( vertex, axiasScale,  vert.GetZf() * 3.142f/2.0f + rotAngle) * dirScale;
	Normal +=baseOffset;
	return RCC_VECTOR3( Normal );
}


void grassField::SetupStaticIndexBuffer(int /*NoVertsPerPrimitive*/, int vertCount )
{
	// index buffer not required as draw primitive does the same thing
	vertCount = vertCount * (__PS3 ? 8 : 16);
	Assert(vertCount <= 65536);
#if __WIN32
	if ( !GRCDEVICE.IsCreated() )
	{
		return;
	}
#endif 
	if ( sm_UnSortedIndexBuffer == 0 || 
		( sm_UnSortedIndexBuffer->GetIndexCount() < (vertCount  )) )
	{
		if ( sm_UnSortedIndexBuffer )
		{
			Assertf(!HACK_RDR2,"This should not be in here");
			delete sm_UnSortedIndexBuffer;
		}
	

		sm_UnSortedIndexBuffer = grcIndexBuffer::Create(vertCount);
		u16 *lockPtr = sm_UnSortedIndexBuffer->LockRW();
		int indexTable[] = { 0,1,2,3,1,5,4, 2,
#if __PS3
		};

		for( int i = 0; i < vertCount; i++ )
		{
			int bladecnt = (int)floor((float)i / 8.0f);
			Assign(lockPtr[i],indexTable[i%8] + bladecnt * 6);
		}
#else
							 6,7,8,9,7,11,10,8};

		for( int i = 0; i < vertCount; i++ )
		{
			int bladecnt = (int)floor((float)i / 16.0f);
			Assign(lockPtr[i],indexTable[i%16] + bladecnt * 12);
		}
#endif // __PS3
		sm_UnSortedIndexBuffer->Unlock();
		
#if __XENON

		grcFvf fvf;
		//fvf.SetPosChannel( true, grcFvf::grcdsFloat4 );
		fvf.SetTextureChannel(1,true, grcFvf::grcdsFloat3);
		fvf.SetTextureChannel(2,true, grcFvf::grcdsFloat4);

		sm_HighBladeVertexBuffer = grcVertexBuffer::Create( RANDOM_BATCH_SIZE, fvf,false);
		grcVertexBufferEditor editor( sm_HighBladeVertexBuffer);

		// create the Vertex buffer for accessing a pre transfromed vert
		for (int i = 0; i < RANDOM_BATCH_SIZE; i+=6 )
		{
			Vector4 offset[6];
			Vector3 p1[6];
			Vector3 p2[6];
			Vector4 averageOffset;
			averageOffset.Zero();
			for( int j =0; j<6;j++ )
			{
				Vector3 uvs;
				p1[j] = GrassDecodeClippedCylinderVertPos( i +j, uvs, 1.0f);
				p2[j] = GrassDecodeClippedCylinderVertPos( i +j, uvs, 0.0f);
				offset[j] = Vector4( uvs.x, uvs.y, p1[j].x - p2[j].x, p1[j].z - p2[j].z);
				averageOffset += offset[j];
			}
			averageOffset.Scale(1.0f/6.0f);
			for( int j =0; j<6;j++ )
			{
				editor.SetUV(i+j,1, p2[j]);

				offset[j].z -= averageOffset.z;
				offset[j].w -= averageOffset.w;
				editor.SetUV( i+j,2,offset[j]);
			}
		}
#endif
	}
}
void grassField::CreateIndexBuffers( int /*NoVertsPerPrimitive*/, int /*vertCount */ )
{
	if (1 )  // just use one static sorted buffer instead to reduce memory cost
	{
		SetupStaticIndexBuffer( VERTEX_TO_INDEX_RATIO_HIGH, 2048);;
		return;
	}
	// CreateSortedIndexBuffers( NoVertsPerPrimitive , vertCount );
}	

void grassField::Create( const atPtr<ParticleList> p )
{
	Create( *p );
}
void grassField::Create( ParticleList& p )
{
	// convert the ParticleList to vertex and index buffers
	// Setup the vertex declaration
	grcFvf fvf;

	const bool useInstancing = g_sysPlatform == platform::XENON || g_sysPlatform == platform::PS3;
	int NoVertsPerPrimitive = useInstancing ? 4 : 6;

	if (useInstancing)
	{
		fvf.SetPosChannel( true, grcFvf::grcdsPackedNormal);
	}
	else
	{
		fvf.SetPosChannel( true, grcFvf::grcdsFloat3);
		fvf.SetTextureChannel(0, true, grcFvf::grcdsHalf2);
	}
	fvf.SetDiffuseChannel( true, grcFvf::grcdsUBYTE4);
	
#if __WIN32PC
	if (!GRCDEVICE.IsCreated()) 
		m_VertexDeclaration = NULL; 
	else
#endif
	m_VertexDeclaration = grmModelFactory::BuildDeclarator(&fvf, 0, 0, 0);

	
	Vector3		aabbInvScale = SetBounds( p.GetMin(), p.GetMax() );

	GrassVertexCreator	grassify( p,  aabbInvScale,  m_aabbOffset.GetVector3());
	m_VertexBuffer = p.CreateVertexBuffer( fvf, grassify );

	CreateIndexBuffers( NoVertsPerPrimitive , p.GetCount());
}

void grassField::SetupDrawBuffers( const Vector2& cardSize,  const Vector2& cardSizeVariance, int widthTextureVariantCount,  int heightTextureVariantCount  )
{

	int QuadsPerBlade =1;
	int NoVertsPerPrimitive = ( g_sysPlatform == platform::XENON || g_sysPlatform == platform::PS3 ) ? 4 : 6;

	NoVertsPerPrimitive *= QuadsPerBlade;

	m_aabbMax  = Vector3( -FLT_MAX, -FLT_MAX, -FLT_MAX );
	m_aabbMin = Vector3( FLT_MAX, FLT_MAX, FLT_MAX );

	// Setup the vertex declaration
	grcFvf fvf;


	fvf.SetPosChannel( true, grcFvf::grcdsHalf4);
	fvf.SetDiffuseChannel( true, grcFvf::grcdsUBYTE4);

	if ( g_sysPlatform != platform::XENON )
	{
		fvf.SetTextureChannel(0, true, grcFvf::grcdsHalf2);
	}

	/*if ( m_extra )
	{
		m_extra->RegisterExtraData( fvf );
	}*/

	m_VertexDeclaration = grmModelFactory::BuildDeclarator(&fvf, 0, 0, 0);

	// Setup the vertex buffer
	CreateVertexBuffer( fvf, cardSize ,cardSizeVariance ,widthTextureVariantCount ,heightTextureVariantCount );

	delete m_tp;
}
void grassField::CreateSortedIndexBuffers( int NoVertsPerPrimitive, int vertCount )
{
	// Create index buffer for the right side
	if( !m_IndexBuffers[gfevqRight] )
	{
		m_IndexBuffers[gfevqRight] = grcIndexBuffer::Create( vertCount );
		u16* lockPtr = m_IndexBuffers[gfevqRight]->LockRW();

		for( int i = 0; i < m_tp->m_Patches.GetCount() * NoVertsPerPrimitive; i++ )
		{
			Assign(lockPtr[i], i);
		}
		m_IndexBuffers[gfevqRight]->Unlock();
	}

	// Create index buffer for the left side
	if( !m_IndexBuffers[gfevqLeft] )
	{
		m_IndexBuffers[gfevqLeft] = grcIndexBuffer::Create( vertCount );
		u16 *lockPtr = m_IndexBuffers[gfevqLeft]->LockRW();
		for( int i = 0; i < vertCount; i++ )
		{
			Assign(lockPtr[i], vertCount- (i + 1));
		}
		m_IndexBuffers[gfevqLeft]->Unlock();
	}

	// Sort the other way
	if( !m_IndexBuffers[gfevqTop] || !m_IndexBuffers[gfevqBottom] )
	{
		sysMemStartTemp();
		int* sorted = rage_new int[m_tp->m_Patches.GetCount()];
		atArray<DirectionSort>		sdistances2;
		sdistances2.Resize(m_tp->m_Patches.GetCount());
		sysMemEndTemp();

		Vector3 vAnchor = Vector3(0.0f, 0.0f, m_Extents.w);
		if( m_Zup )
			vAnchor.Set(0.0f, m_Extents.w, 0.0f);

		for( int i = 0; i < m_tp->m_Patches.GetCount(); i++ )
		{
			Vector3 pos;
			
			pos.Set(m_tp->m_Patches[i].x, m_tp->m_Patches[i].y, m_tp->m_Patches[i].z);
			
			if( m_Zup )
			{
				pos.z = 0.0f;
				pos.x = 0.0f;
			}
			else
			{
				pos.y = 0.0f;
				pos.x = 0.0f;
			}

			float fDist = pos.Dist(vAnchor);;
			sdistances2[i] = DirectionSort( fDist, i );
		}
		sysPerformanceTimer tQSort("qsort");
		tQSort.Start();
		sdistances2.QSort( 0, -1, DistanceCompareDirections );

		for( int i = 0; i < m_tp->m_Patches.GetCount(); i++ )
		{
			int idx = sdistances2[i].idx;
			sorted[ m_tp->m_Patches.GetCount() - i - 1 ] = idx ;
		}
		tQSort.Stop();
		tQSort.PrintReset();

		// Create index buffer for the top side
		if( !m_IndexBuffers[gfevqTop] )
		{
			m_IndexBuffers[gfevqTop] = grcIndexBuffer::Create(m_tp->m_Patches.GetCount() * NoVertsPerPrimitive);
			u16 *lockPtr = m_IndexBuffers[gfevqTop]->LockRW();
			for( int i = 0; i < m_tp->m_Patches.GetCount(); i++ )
			{
				int index = i * NoVertsPerPrimitive;
				int patchIndex = sorted[i] * NoVertsPerPrimitive;

				SetPrimitiveToIndexBuffer( lockPtr, index, patchIndex, NoVertsPerPrimitive );
			}
			m_IndexBuffers[gfevqTop]->Unlock();
		}

		// Create index buffer for the bottom side
		if( !m_IndexBuffers[gfevqBottom] )
		{
			m_IndexBuffers[gfevqBottom] = grcIndexBuffer::Create(m_tp->m_Patches.GetCount() * NoVertsPerPrimitive);
			u16 *lockPtr = m_IndexBuffers[gfevqBottom]->LockRW();
			for( int i = 0; i < m_tp->m_Patches.GetCount(); i++ )
			{
				int index = i * NoVertsPerPrimitive;
				int patchIndex = sorted[m_tp->m_Patches.GetCount() - (i + 1)] * NoVertsPerPrimitive;

				SetPrimitiveToIndexBuffer( lockPtr, index, patchIndex, NoVertsPerPrimitive );
			}
			m_IndexBuffers[gfevqBottom]->Unlock();
		}

		sysMemStartTemp();
		delete[] sorted;
		sysMemEndTemp();
	}
}
grassField::gfeViewQuadrant grassField::GetSortedBufferToDraw()
{
	// Pick the index buffer to draw with
	Matrix34 camMtx = MAT34V_TO_MATRIX34(grcViewport::GetCurrent()->GetCameraMtx());
	Vector3 viewDir = -camMtx.c;

	gfeViewQuadrant indexBuffer = gfevqRight;
	if( m_Zup )
	{
	}
	else
	{
		viewDir.y = 0.0f;
		Vector3 vCorner1 = Vector3(0.7071068f, 0.0f, 0.7071068f);
		Vector3 vCorner2 = Vector3(0.7071068f, 0.0f, -0.7071068f);
		float dot1 = viewDir.Dot(vCorner1);
		float dot2 = viewDir.Dot(vCorner2);
		if( dot1 < 0.0f )
		{
			if( dot2 < 0.0f )
			{
				//indexBuffer = gfevqRight;
				indexBuffer = gfevqLeft;
			}
			else
			{
				//indexBuffer = gfevqTop;
				indexBuffer = gfevqBottom;
			}
		}
		else
		{
			if( dot2 < 0.0f )
			{
				//indexBuffer = gfevqBottom;
				indexBuffer = gfevqTop;
			}
			else
			{
				//indexBuffer = gfevqLeft;
				indexBuffer = gfevqRight;
			}
		}
	}
	return indexBuffer;
}

//
// PURPOSE
//	Transform bounding box into screen space so as to calculate the screen space extents
//
void GetScreenSpaceBound( Vector3& scMax, Vector3& scMin, const spdAABB& aabb )
{
	Matrix44 WorldToScreenMtx = MAT44V_TO_MATRIX44(grcViewport::GetCurrent()->GetFullCompositeMtx());

	Vector3 avec;

	Vector3 min = VEC3V_TO_VECTOR3(aabb.GetMin());
	Vector3 max = VEC3V_TO_VECTOR3(aabb.GetMax());
	Vector3 pts[8] = 
	{ 
		Vector3( min.x, min.y, min.z ),
			Vector3( max.x, min.y, min.z ),
			Vector3( min.x, max.y, min.z ),
			Vector3( min.x, min.y, max.z ),
			Vector3( max.x, max.y, min.z ),
			Vector3( min.x, max.y, max.z ),
			Vector3( max.x, min.y, max.z ),
			Vector3( max.x, max.y, max.z )
	};


	avec = WorldToScreenMtx.FullTransform( pts[0]);
	scMax = avec;
	scMin = avec;

	for ( int i = 1; i < 8; i++ )
	{
		avec= WorldToScreenMtx.FullTransform( pts[i] );
		scMin.Min( scMin, avec );
		scMax.Max( scMax, avec );
	}
}	

#if __XENON
void ResetPredication()
{
	GRCDEVICE.SetPredication( 0 );
}
void SetVisiblePredicatedTiles( const spdAABB& aabb )
{
	int tcount;
	const grcTileRect* tiles =  GRCDEVICE.GetTileRects( tcount );
	if ( tcount == 0)
	{
		return;
	}
	Assert( tcount == 3);
	
	
	// need to check y axias only and this is conserative as I don't care about clip volumes to screen

	Vector3 scMax;
	Vector3 scMin;
	GetScreenSpaceBound( scMax, scMin , aabb );
	if ( scMin.z <= 0.00000001f )		// clips z which causes error in caculation
	{
		return;
	}

	DWORD predicationFlag = 0;
	
	for ( int i = 0; i < 3 ; i++  )	
	{
		Assert( tiles[i].y1 < tiles[i].y2 );

		if  (!( scMax.y < (float)tiles[i].y1 || scMin.y > (float)tiles[i].y2 ) )  // on tile 
		{
			predicationFlag |= D3DPRED_TILE(i);
		}
	}
	GRCDEVICE.SetPredication( predicationFlag );

	
}
#endif

grassPatchBase*							grassField::sm_PrePatch= 0;
int										grassField::sm_passIndex = -1;
void grassField::ResetCachedPatch()
{
	if ( sm_PrePatch )
	{
		sm_PrePatch->EndDraw();
	}
	GRCDEVICE.ClearStreamSource(0);
	sm_PrePatch = 0;
	sm_passIndex = -1;
}

#if __XENON
#define GLOBAL_SCALE_IDX 78

__forceinline void SetGlobalScaleBiasFast( const Vector4* scale )
{
	GRCDEVICE.GetCurrent()->SetVertexShaderConstantF(GLOBAL_SCALE_IDX ,&scale->x, 2);
}
#endif

void grassField::Draw(  grassLod& lod, grassPatchBase* patch,  bool UNUSED_PARAM( useCylinderGrass), float UNUSED_PARAM( startFade), float UNUSED_PARAM( endFade ) , float UNUSED_PARAM( fadePower), atArray<sysTaskHandle>& PPU_ONLY(handles), int passIndex /*= 0*/ )
{
	int VertexToIndexRatio = VERTEX_TO_INDEX_RATIO;

	// Early return
	if (!patch)
		return;

	int ThinnedIndexCount = (m_VertexBuffer->GetVertexCount()) * VertexToIndexRatio;
	ThinnedIndexCount  =(m_VertexBuffer->GetVertexCount());
	int ThinnedStartFade = ThinnedIndexCount;

	grcSmallStateStack	states;
	if ( passIndex != 2)
	{
		XENON_ONLY( states.Set<grcsDepthBias>(0.00001f) );
	}
	

#if __XENON
	// setup the predication
	//
	bool usingTiling = false;
	if ( usingTiling )
	{
		spdAABB	aabb;
		aabb.Set( m_aabbMin, m_aabbMax );

		SetVisiblePredicatedTiles( aabb );
	}
	// removed as actually pretty slow due to vertx constant waterfalling
	patch->SetDataTables( sm_OffsetTable,	sm_UVsTable , sm_ExtraVertData, RandomTableSize );
	

	// use less on shadow pass
#endif
										
#if __XENON || __PS3
	grcDrawMode	DrawMode = drawQuads;
#else
	grcDrawMode	DrawMode = drawTris;
#endif
	
	int grassFilterAmt = 1;
	int	thinOffset = 0;
	bool doubleDraw = false;//sm_GrassDoubleDraw;  // high jacked this widget item (it did not work below anyway)
	float doubleDrawAmt = 0.0f;
	bool isHigh = false;
	/*if (passIndex == 2)
	{
		grassFilterAmt=2;
	}*/
	if ( 1 /*sm_GrassTransition*/ )
	{
		// If this is far enough away from the camera, use a cheaper technique.
		Vector3 center = (m_aabbMax + m_aabbMin);
		Vector3 radius = (m_aabbMax - m_aabbMin);
		center.Scale(0.5f);
		radius.Scale(0.5f);
		
		float dist = VEC3V_TO_VECTOR3(grcViewport::GetCurrent()->GetCameraPosition()).Dist(center);
		dist -=radius.Mag();
		
		if ( dist > lod.maxRenderDistance )
		{
			return;
		}
		float transDist = lod.transToLowDist;
		if (dist > transDist || !sm_GrassTransition)
		{
			if (passIndex == 2)
			{
				// Don't draw the collector too far.
				passIndex += lod.cheapPassOffset;
				//grassFilterAmt = lod.shadowReduceAmt;

				if ( dist > lod.farShadowDistance )  // greater than far shadow dist
				{
					return;
				}
				//return;
			}
			else
			{
				// Switch to cheapo.
				passIndex += lod.cheapPassOffset;
			}
			doubleDraw = false;
		}
		else
		{
			if ( lod.useCylinderGrass)
			{
				VertexToIndexRatio = 16;
				isHigh = true;
			}
		}
		int tIdx = 0;
		if (passIndex == 2)
		{
			tIdx++;
		}

		// calculate thinning
		float thinFactor = Clamp( (dist - lod.ThinStart[tIdx]) /(lod.ThinEnd[tIdx]-lod.ThinStart[tIdx]), 0.0f, 1.0f);
		thinFactor = powf( thinFactor, lod.ThinPower[tIdx] );
		thinOffset =(int)((float)m_VertexBuffer->GetVertexCount() * thinFactor);

		static float doubleFadeDist = 10.0f;
		float doubleDrawDist = lod.transToLowDist -doubleFadeDist;
		doubleDrawAmt = (dist - doubleDrawDist) /doubleFadeDist;
		m_aabbOffset.w = (!sm_GrassTransition) ? 0.0f : 1.0f - Clamp( doubleDrawAmt, 0.0f,1.0f);
	}	
	if (sm_PrePatch != patch || passIndex != sm_passIndex )
	{
		if ( sm_PrePatch ) 
		{
			sm_PrePatch->EndDraw();
		}
		// Setup the patch for drawing
		
		patch->SetThinning(ThinnedStartFade, ThinnedIndexCount);
		patch->SetDoubleFade( lod.doubleDrawDist, lod.doubleFadeDist);
	
		patch->PreDraw();
		ASSERT_ONLY( bool res = ) patch->BeginDraw(  m_Extents, passIndex );
		Assert( res );

#if __ASSERT
		if (!res)
		{
			return;
		}
#endif // __ASSERT

		sm_passIndex = passIndex;
		sm_PrePatch = patch;
	}

#if !__XENON
	patch->SetQuantisationScaleAndOffset( m_aabbScale.GetVector3(), m_aabbOffset.GetVector3() );
#else
	SetGlobalScaleBiasFast( &m_aabbScale);
#endif

#if !GRASS_SPU_INSTANCE_RENDERER
	// Draw all the patches
	
#endif // !GRASS_SPU_INSTANCE_RENDERER
	int vertexStride = m_VertexBuffer->GetVertexStride();
	int vertexCount = m_VertexBuffer->GetVertexCount();

#if __BANK
	vertexCount =  (int)((float)m_VertexBuffer->GetVertexCount() * lod.GlobalThinning );
#endif
	int indexCount =  (vertexCount/ grassFilterAmt ) * VertexToIndexRatio ;
	int indexStart = thinOffset* VertexToIndexRatio;
	indexCount -= indexStart;

	// Lod Routines
	//indexCount = ThinnedIndexCount;
	if ( indexCount >= VertexToIndexRatio )
	{
#if GRASS_SPU_INSTANCE_RENDERER
		grcInstanceRenderer::PpuContext ppuContext;
		grcInstanceRenderer::DmaListItem dmaList;
		AssertVerify(m_VertexBuffer->Lock());
		dmaList.Ea = (u32)m_VertexBuffer->GetLockPtr();
		AssertVerify(m_VertexBuffer->Unlock());
		dmaList.ElementSize = vertexStride;
		dmaList.Count = vertexCount / grassFilterAmt - thinOffset;
		ppuContext.DmaList = &dmaList;
		ppuContext.DmaListCount = 1;
		ppuContext.DrawMode = DrawMode;

		VS_Grass_TypeData typeData;
		typeData.CameraMat = RCC_MAT34V(grcViewport::GetCurrentCameraMtx());
		typeData.GlobalScale = RCC_VEC4V(m_aabbScale);
		typeData.GlobalBias = RCC_VEC4V(m_aabbOffset);
		typeData.DoubleFadeDist = Vec2V(lod.doubleDrawDist, 1.0f / lod.doubleFadeDist);
		Vector3 windDirection;
		float windStrength;
		grassManager::GetWind(windDirection, windStrength);
		typeData.GrassWindDirection = RCC_VEC3V(windDirection);
		typeData.GrassWindStrength = ScalarVFromF32(windStrength);
#if 1
		Vector3 playerPosition;
 		float playerRadius;
 		patch->GetPlayerPosition(playerPosition, playerRadius);
 		typeData.PersonPosition = Vec4V(RCC_VEC3V(playerPosition), ScalarVFromF32(playerRadius));
#else
		typeData.PersonPosition = Vec4V(RCC_VEC3V(grcViewport::GetCurrentCameraPosition()), ScalarVFromF32(1.0f));
#endif // 0
		typeData.GrassTimer = ScalarVFromF32(grassManager::GetTime());

		// Frustum planes (transposed)
		const grcViewport* currentViewport = grcViewport::GetCurrent();
		typeData.Plane0x = RCC_VEC4V(currentViewport->GetPlane0x());
		typeData.Plane0y = RCC_VEC4V(currentViewport->GetPlane0y());
		typeData.Plane0z = RCC_VEC4V(currentViewport->GetPlane0z());
		typeData.Plane0w = RCC_VEC4V(currentViewport->GetPlane0w());
		typeData.Plane1x = RCC_VEC4V(currentViewport->GetPlane1x());
		typeData.Plane1y = RCC_VEC4V(currentViewport->GetPlane1y());
		typeData.Plane1z = RCC_VEC4V(currentViewport->GetPlane1z());
		typeData.Plane1w = RCC_VEC4V(currentViewport->GetPlane1w());

		typeData.ClipDims = VECTOR4_TO_VEC4V(patch->GetClipRegion());

		ppuContext.TypeData = &typeData;
		ppuContext.TypeDataSize = sizeof(VS_Grass_TypeData);
		ppuContext.CacheTypeData = true;

		static const grcVertexElement vertexElements[] =
		{
			grcVertexElement(0, grcVertexElement::grcvetColor, 0, 4, grcFvf::grcdsColor),
			grcVertexElement(0, grcVertexElement::grcvetPosition, 0, 12, grcFvf::grcdsFloat3),
			grcVertexElement(0, grcVertexElement::grcvetTexture, 0, 4, grcFvf::grcdsHalf2)
		};
		static grcVertexDeclaration* outputVertexDecl = GRCDEVICE.CreateVertexDeclaration(vertexElements, NELEM(vertexElements));
		ppuContext.VertexDecl = outputVertexDecl;

		if (isHigh)
		{
			ppuContext.NumVertexesPerInstance = 6;
			ppuContext.NumIndexesPerInstance = 8;
			ppuContext.IndexBuffer = sm_UnSortedIndexBuffer;
			handles.Grow() = grcInstanceRenderer_Create(&ppuContext, rage_grasshigh);
		}
		else
		{
			ppuContext.NumVertexesPerInstance = 4;
			ppuContext.NumIndexesPerInstance = 0;
			ppuContext.IndexBuffer = NULL;
			handles.Grow() = grcInstanceRenderer_Create(&ppuContext, rage_grasslow);
		}
#else
		if (isHigh)
		{
			static const grcVertexElement vertexElements[] =
			{
				grcVertexElement(0, grcVertexElement::grcvetPosition, 0, 4, grcFvf::grcdsPackedNormal),
				grcVertexElement(0, grcVertexElement::grcvetColor, 0, 4, grcFvf::grcdsColor),
				grcVertexElement(1, grcVertexElement::grcvetTexture, 1, 12, grcFvf::grcdsFloat3),
				grcVertexElement(1, grcVertexElement::grcvetTexture, 2, 16, grcFvf::grcdsFloat4)
			};
			static grcVertexDeclaration* s_HighVertexDeclaration = GRCDEVICE.CreateVertexDeclaration(vertexElements, NELEM(vertexElements));

			GRCDEVICE.SetVertexDeclaration(s_HighVertexDeclaration);

			GRCDEVICE.SetStreamSource(0, *m_VertexBuffer, 0, vertexStride);
			GRCDEVICE.SetStreamSource(1, *sm_HighBladeVertexBuffer, 0, sm_HighBladeVertexBuffer->GetVertexStride() );

			GRCDEVICE.SetIndices(*sm_UnSortedIndexBuffer);
			GRCDEVICE.DrawIndexedPrimitive( DrawMode, indexStart, indexCount  );
		}
		else
		{
			GRCDEVICE.SetVertexDeclaration(m_VertexDeclaration);
			GRCDEVICE.SetStreamSource(0, *m_VertexBuffer, 0, vertexStride);
			GRCDEVICE.DrawPrimitive( DrawMode, indexStart, indexCount );
		}
#endif // GRASS_SPU_INSTANCE_RENDERER
		
		if (doubleDraw && 0 )
		{
			
			static Vector4 offset= Vector3(0.3f,0.0f,0.3f,0.0f);
			static float randomBladeOffset = 17.0f;
			Vector4 oldOffset = m_aabbOffset;

			if (sm_GrassTransition)	// just leave these off when testing no sliding textures.
			{	
				m_aabbOffset += offset;
				m_aabbScale.w = randomBladeOffset;// set to random number 
			}	
			float oldOffsetFade = m_aabbOffset.w;

#if !__XENON
			patch->SetQuantisationScaleAndOffset( m_aabbScale.GetVector3(), m_aabbOffset.GetVector3() );
#else
			SetGlobalScaleBiasFast( &m_aabbScale);
#endif

			Assert(isHigh);

#if GRASS_SPU_INSTANCE_RENDERER
			typeData.GlobalScale = RCC_VEC4V(m_aabbScale);
			typeData.GlobalBias = RCC_VEC4V(m_aabbOffset);

			handles.Grow() = grcInstanceRenderer_Create(&ppuContext, rage_grasshigh);
#else
			GRCDEVICE.SetIndices(*sm_UnSortedIndexBuffer);
			GRCDEVICE.DrawIndexedPrimitive( DrawMode, indexStart, indexCount );
#endif // GRASS_SPU_INSTANCE_RENDERER

			m_aabbOffset.w = oldOffsetFade;
			m_aabbScale.w = 0.0f;
			m_aabbOffset = oldOffset;
		}
#if !GRASS_SPU_INSTANCE_RENDERER
		GRCDEVICE.ClearStreamSource(1);
		GRCDEVICE.ClearStreamSource(0);
#endif // !GRASS_SPU_INSTANCE_RENDERER	
	}

#if __XENON
	
	if ( usingTiling )
	{
		ResetPredication();
	}
	sm_updated = false;

#endif
}

void grassField::DrawZPrePass( grassPatchBase* patch, float UNUSED_PARAM(lod ) )
{
	// Set the world matrix to identity since the cards are already in world space
	grcViewport::SetCurrentWorldMtx(M34_IDENTITY);


	// Setup the patch for drawing
	patch->BeginDraw(  m_Extents,  true);

	// Pick the index buffer to draw with
	Matrix34 camMtx = MAT34V_TO_MATRIX34(grcViewport::GetCurrent()->GetCameraMtx());
	Vector3 viewDir = -camMtx.c;
	gfeViewQuadrant indexBuffer = gfevqLeft;
	if( m_Zup )
	{
	}
	else
	{
		viewDir.y = 0.0f;
		Vector3 vCorner1 = Vector3(0.7071068f, 0.0f, 0.7071068f);
		Vector3 vCorner2 = Vector3(0.7071068f, 0.0f, -0.7071068f);
		float dot1 = viewDir.Dot(vCorner1);
		float dot2 = viewDir.Dot(vCorner2);
		if( dot1 < 0.0f )
		{
			if( dot2 < 0.0f )
			{
				indexBuffer = gfevqLeft;
			}
			else
			{
				indexBuffer = gfevqBottom;
			}
		}
		else
		{
			if( dot2 < 0.0f )
			{
				indexBuffer = gfevqTop;
			}
			else
			{
				indexBuffer = gfevqRight;
			}
		}
	}

#if __XENON
	grcDrawMode	DrawMode = drawQuads;
#else
	grcDrawMode	DrawMode = drawTris;
#endif

	GRCDEVICE.DrawIndexedPrimitive(DrawMode,m_VertexDeclaration,*m_VertexBuffer,*m_IndexBuffers[indexBuffer],m_IndexBuffers[indexBuffer]->GetIndexCount());

	// Finish drawing
	patch->EndDraw();
}


////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Clears all grass from the defined region.
//
int FieldList::ClearAreaOfGrass(const spdSphere& area)
{
	int clearCount = 0;

	
	for (int i = 0; i < GetCount(); i++)
	{
		if (el[i])
		{
			clearCount += el[i]->ClearAreaOfGrass(area);
		}
	}

	return clearCount;
}


FieldList*  LoadResourcedFieldList( const char* path )
{
	FieldList* 	fieldList;
	// load the resourced file
	pgRscBuilder::Load( fieldList, path, "#gs", FieldList::RORC_VERSION);
	AssertMsg( fieldList, "Error grass field did not load correctly ");
	return fieldList;
}

void  CreateFieldListImp( atArray<datOwner<grassField> >& fields,  atPtr<ParticleList> pl, int numPerBatch , atPtr<ParticleList> a )
{
	sysMemStartTemp();
	bool splitIt = pl->Split( numPerBatch, a );
	sysMemEndTemp();

	if ( splitIt )
	{
		// do some good old recursion

		atPtr<ParticleList> b = rage_new ParticleList;	
		CreateFieldListImp( fields, a, numPerBatch,b  );
		CreateFieldListImp( fields, pl, numPerBatch, a );
	}
	else
	{
		fields.Grow() =  rage_new grassField( pl );
	}
	
}

IMPLEMENT_PLACE( FieldList );

void FieldList::PostPlace()
{
	int n = el.GetCount();
	for(int i=0; i<n; ++i)
		el[i]->PostPlace();
}

struct LeafCreator
{
	FieldList* m_fields;
	ConstString	m_type;

	LeafCreator(FieldList* f , ConstString type )
		: m_fields( f ), m_type( type) {}

	void operator()( Particle* start, Particle* end , Vector3& , Vector3&)
	{
		sysMemStartTemp();
		ParticleList		pl = ParticleList( start, end);
		sysMemEndTemp();

		m_fields->el.Grow() = rage_new grassField( pl, m_type.m_String );

		sysMemStartTemp();
		pl.Clear();
		sysMemEndTemp();
	}
};


struct ResourceLeafCreator
{
	FieldList* m_fields;
	ConstString	m_type;

	ResourceLeafCreator(FieldList* f , const char * type ) : m_fields( f )
	{
		Assertf(m_fields, "warning: empty field data given to resource creator");
		sysMemStartTemp();
		m_type = type;
		sysMemEndTemp();
	}

	~ResourceLeafCreator()
	{
		sysMemStartTemp();
		m_type = NULL;
		sysMemEndTemp();
	}

	void operator()( Particle* start, Particle* end , Vector3& , Vector3&)
	{
		Assertf(m_fields, "warning: no grass data to construct!");
		if (m_fields)
		{
			if (m_fields->el.GetCapacity() > 0)
			{
				sysMemStartTemp();
				ParticleList		pl = ParticleList( start, end);
				sysMemEndTemp();

				// Don't grow, just append (fieldlist must be presized!)
				m_fields->el.Append() = rage_new grassField( pl, m_type.m_String );

				sysMemStartTemp();
				pl.Clear();
				sysMemEndTemp();
			}
			else
			{
				Assertf(false, "warning: cannot construct a field that has no grass");
			}
		}
	}
};


struct CountChildNodes
{
	int m_count;

	CountChildNodes() : m_count(0) {}
	
	void operator()( Particle* , Particle* , Vector3& , Vector3&)
	{
		m_count++;
	}
	int GetCount() const { return m_count; }
};

////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		This C-style function creates a field list from a particle list.
//
FieldList*  CreateFieldList( atPtr<ParticleList> pl, int numPerBatch )
{
	FieldList* fields = rage_new FieldList;
	fields->SetType( pl->GetType().m_String );
	fields->el.Reserve( (pl->GetCount() / numPerBatch) * 2);

	ResourceLeafCreator createFunction( fields, pl->GetType() );
	pl->CreateKdTree( numPerBatch, createFunction);
	return fields;
}

////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Just like CreateFieldList, but all fields are allocated up-front, instead
//	of "growing" the list as CreateKdTree does its sweep.  This is necessary
//	during resource generation, since incremental growing often destroys the
//	fieldlist and makes a new one, a no-no to pgRscBuilder.
//
FieldList*  CreateResourceFieldList( atPtr<ParticleList> pl, int numPerBatch, int requiredFields )
{
	FieldList* fl = rage_new FieldList;
	
	fl->el.Reserve(requiredFields);
	ResourceLeafCreator creator( fl, pl->GetType() );
	pl->CreateKdTree( numPerBatch, creator);
	return fl;
}

int CountNumFieldsRequired( atPtr<ParticleList> pl, int numPerBatch )
{
	CountChildNodes counter;
	pl->CreateKdTree( numPerBatch, counter );
	return counter.GetCount();
}

////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		This function adds more fields to an existing field list, such as one
//	created by CreateFieldList (see above).
//
void AppendResourceFieldList( FieldList* fieldlist, atPtr<ParticleList> pl, int numPerBatch )
{
	 ResourceLeafCreator  creator( fieldlist, pl->GetType() );
	pl->CreateKdTree( numPerBatch, creator);
}


void FieldGetVisible( GrassRefList& el,
					 const spdShaft* shafts, 
					 int			amtShafts,
					 std::pair<float, grassField*>* sortList, 
					 int& renderCount, float startFade, float finishFade, int maxRenderCount  )
{
	float recip =  1.0f  /( finishFade - startFade);
	maxRenderCount -=1;
	for ( int i = 0; i < el.GetCount(); i++ )
	{
		float outZ;
		float endZ;
		if ( el[i]->IsVisible( shafts, amtShafts, outZ, endZ  ) )
		{
			// calculate lod
			float lod =  ( outZ - startFade ) * recip ;
			if ( lod < 1.0f )
			{
				// add to list
				sortList[ renderCount++ ] = std::make_pair( lod, el[i]  );
				renderCount = Min(maxRenderCount,renderCount );
			}
		}
	}
}


} // namespace rage
#if 0 
/*
struct AABBHierarchyNode
{
	Vector3		m_min;
	u32			m_cIdx;

	Vector3		m_max;
	u16			m_NumberChildren;
	u8			m_orderdedTraversalSign;
	u8			m_orderdedTraversalAxias;

	AABBHierarchyNode() {}

	void Set( const Vector3 min, const Vector3 max  )
	{
		m_min = min;
		m_max = max;
	}
	void SetChild( int childIdx , int childEndIndex )
	{
		m_cIdx = childIdx;
		m_NumberChildren =  childEndIndex;
		m_orderdedTraversalSign = 1;
	}
	void SetNode( int childIdx , int childEndIndex )
	{
		m_cIdx = childIdx;
		m_NumberChildren =  childEndIndex;
		m_orderdedTraversalSign = 0;
	}

	bool IsNode() { return !m_orderdedTraversalSign; }
	bool IsChild() { return !m_orderdedTraversalSign; }


	bool Intersects( spdShaft& shaft )
	{
		spdAABB	bound;
		bound.Set( m_min, m_max );
		return shaft->Intersects( bound );
	}

};

template<class LEAFFUNCTION, class BVH, int STRIDE >
void TraverseSorted( AABBHierarchyNode* root, int stride, BVH& bound, LEAFFUNCTION& functor )
{
	AABBHierarchyNode* node = root;
	const int MaxStackSize = 64;
	Stack<int, MaxStackSize> stack;
	
	stack.push( NULL);

	while( node = stack.pop() )
	{
		int clip = node->Intersects( bound );

		if ( node->IsChild() || splitFunction( clip, distance, depth ) )
		{
			functor( node->m_cIdx, node->m_NumberChildren);
		}
		else
		{
			int firstChild = AABBHierarchyNode::TraversalOrder( node, ray);
			stack.push( node.GetChild(1-firstChild));
			node = node.GetChild(firstChild);
		}	
	}
}*/



#endif
