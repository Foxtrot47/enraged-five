// 
// grass/fieldfactory.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


//
//	PURPOSE
//		helper class to make passing parameters a bit easier
//
#ifndef FIELDFACTORY_H
#define FIELDFACTORY_H

#include <atl/string.h>

namespace rage
{
namespace fieldFactory
{


#if !__FINAL
	void SaveFieldParams(	const char* outFilename,
		const char* colorMapName, const char* patchTextureName, 
		const Vector4& extents, float gridRes, 
		const Vector2& cardSize, const Vector2& cardSizeVariance, 
		int widthTextureVariantCount, int heightTextureVariantCount );
#endif

struct PlacementFieldParams
{
	atString	m_colorMapName;
	atString	m_patchTextureName;
	atString	m_colorTexMapName2;
	Vector4 m_extents;
	float m_gridRes;
	Vector2 m_cardSize;
	Vector2 m_cardSizeVariance;
	int m_widthTextureVariantCount;
	int m_heightTextureVariantCount;
	const char* m_normalMapName;
	Matrix44	m_colorProjectionMtx;
	Vector4 m_subImage;

	PlacementFieldParams( char* colorMapName, char* patchTextureName,
		const Vector4& extents, float gridRes, 
		const Vector2& cardSize, const Vector2& cardSizeVariance, 
		int widthTextureVariantCount, int heightTextureVariantCount,
		Matrix44& colorProjectionMtx,
		const char* normalMapName = 0)
		:
	m_colorMapName( colorMapName ),
		m_patchTextureName( patchTextureName ),
		m_extents( extents),
		m_gridRes(gridRes ),
		m_cardSize( cardSize ),
		m_cardSizeVariance( cardSizeVariance ),
		m_widthTextureVariantCount( widthTextureVariantCount ),
		m_heightTextureVariantCount(heightTextureVariantCount ),
		m_normalMapName( normalMapName )	,
		m_colorProjectionMtx( colorProjectionMtx )
	{
		m_subImage.Set(0.0f, 1.0f, 0.0f, 1.0f);
	}
	PlacementFieldParams() : m_normalMapName( 0 ), m_subImage( 0.0f, 1.0f, 0.0f, 1.0f )		
	{}	

	PlacementFieldParams& SetColorPlacementMap( const char *colorMap ) { m_colorMapName = colorMap; return *this; }
	PlacementFieldParams& SetDiffuseMap( const char *diffuseMap) { m_patchTextureName = diffuseMap; return *this; }
	PlacementFieldParams& SetTextureIndexMap( const char *colorMap2) { m_colorTexMapName2 = colorMap2; return *this; }
	PlacementFieldParams& SetExtents( const Vector4& extents ) { m_extents = extents; return *this; }
	PlacementFieldParams& SetGridRes( float gridRes) { m_gridRes = gridRes; return *this; }
	PlacementFieldParams& SetCardSize( Vector2 cardSize ) { m_cardSize = cardSize; return *this; }
	PlacementFieldParams& SetCardSizeVariance( Vector2 cardSizeVariance ) { m_cardSizeVariance = cardSizeVariance; return *this; }
	PlacementFieldParams& SetNumFramesWidth( int widthTextureVariantCount) { m_widthTextureVariantCount = widthTextureVariantCount; return *this; }
	PlacementFieldParams& SetNumFramesHeight( int heightTextureVariantCount) { m_heightTextureVariantCount = heightTextureVariantCount; return *this; }
	PlacementFieldParams& SetNormalMap( const char* normalMapName) { m_normalMapName = normalMapName; return *this; }
	PlacementFieldParams& SetSubImage( const Vector4& subImage) {  m_subImage = subImage; return *this; }

	int Read( const char* fieldParamFile );

#if !__FINAL
	void Save( const char* outFileName )
	{
		SaveFieldParams( outFileName,
			m_colorMapName, m_patchTextureName, 
			m_extents, m_gridRes, 
			m_cardSize, m_cardSizeVariance, 
			m_widthTextureVariantCount, m_heightTextureVariantCount );
	}
#endif
};

void CreateSubdividedField( FieldList&  fields, int divisions, PlacementFieldParams& p, bool setupDraw = true);

void CreateSubdividedField( FieldList&  fields, int numDivisions,
						   const char* colorMapName, const char* patchTextureName,
						   const Vector4& extents, float gridRes, 
						   const Vector2& cardSize, const Vector2& cardSizeVariance, 
						   int widthTextureVariantCount, int heightTextureVariantCount,
						   Matrix44&	colorProjectionMtx,
						   const char* normalMapName = 0 , const char* fieldColorMap2 = 0 );

void CreateSubdividedField( FieldList&  fields, int numDivisions, const char* fieldParamFile );


};
};

#endif
