#define NO_SKINNING
#include "../../../base/src/shaderlib/rage_common.fxh"
#include "../../../base/src/shaderlib/rage_diffuse_sampler.fxh"
#include "../../../base/src/shaderlib/rage_grass.fxh"


struct grassVertexOut
{
	float4 pos				: DX_POS;
	float4 worldPos			: TEXCOORD0;
	float2 uv				: TEXCOORD1;
	float2 colorMapUV		: TEXCOORD2;
	float3 center			: TEXCOORD3;
	float  fade				: TEXCOORD4;
	float3 ScreenPos		: TEXCOORD5;
	float2 ExtraData		: TEXCOORD6;
	float4 color			: COLOR0;
};

// Control variables
float3 GrassWindDirection;
float GrassWindStrength;
float GrassTimer;

float3x3 GenerateBillboardMatrix(float3 centerPos)
{
	// Compute the view normal
	float3 viewNrm;
	if( g_fBillboardAgainstVP )
		viewNrm = gViewInverse[2].xyz;
	else
		viewNrm = normalize(gViewInverse[3].xyz - centerPos);
    
    // Compute the right vector
    float3 up = normalize(g_vUpVector);
    float3 viewRight = normalize(cross(viewNrm, up));
    
    // Compute the new view normal
    viewNrm = normalize(cross(up, viewRight));
    
    // Compute rotation basis matrix
    float3x3 mBillboardBasis;
	mBillboardBasis[0] = float3(viewNrm.x, up.x, viewRight.x);
	mBillboardBasis[1] = float3(viewNrm.y, up.y, viewRight.y);
	mBillboardBasis[2] = float3(viewNrm.z, up.z, viewRight.z);
	return mBillboardBasis;
}

#if __XENON

#pragma dcl position color0

grassVertexOut VS_Grass( int i : INDEX )
{
	grassVertexOut OUT = (grassVertexOut)0;
	
	float4 vWorldPos;
	float2 uvs;
	float thinFade;
	float4 col;
	float scale;
	
	vWorldPos.xyz = GrassDecodeClippedVert(false,i, uvs, thinFade, col, scale );
	vWorldPos.w = 1.0f;
	// Get quad color out of the VB
	
	// Animate grass
	float t = GrassTimer * 0.4f;
	float2 bias = t + vWorldPos.xz / 2.0f;
	float4 wave;
	sincos(bias.x * 0.5f, wave.x, wave.y);
	sincos(bias.x * 4.3f, wave.z, wave.w);
	float2 offset = (wave.xy + 0.1 * wave.wz) * (1.0f - uvs.y);
	offset *= GrassWindStrength;
	vWorldPos.xyz += offset.xyy * scale * GrassWindDirection; 

	OUT.uv = uvs;
	OUT.pos = (float4)mul(vWorldPos, gWorldViewProj);
	OUT.center = vWorldPos.xyz;
	OUT.worldPos = vWorldPos;
	OUT.colorMapUV = uvs;
	OUT.color = col;//float4(gLightColor[0].xyz * col.xyz, 1.0f);
	
	float3 cameraDistance =  gViewInverse[3].xyz - vWorldPos.xyz;
	OUT.fade = thinFade; //1.0f - saturate (  length(cameraDistance) * g_FadeScale.x - g_FadeScale.y );
	
	return OUT;
}
#else
grassVertexOut VS_Grass(grassVertexIn IN)
{
	grassVertexOut OUT = (grassVertexOut)0;
	
	float2 offset = (IN.uv.xy * IN.center.w) * 2.0f - 1.0f;
	IN.center.xyz += offset.x * gViewInverse[ 0 ].xyz + -offset.y * gViewInverse[ 1 ].xyz;

	// Rotate to face the camera
	float4 vWorldPos = float4( IN.center.xyz, 1.0f); 
		

	OUT.uv = IN.uv.xy;
	//OUT.colorMapUV = IN.pos.xz;
	OUT.pos = mul(vWorldPos, gWorldViewProj);
	OUT.center = IN.center.xyz;
	OUT.worldPos = vWorldPos;
	OUT.colorMapUV = IN.uv.xy;
	OUT.color = IN.color;
	
	float3 cameraDistance =  gViewInverse[3].xyz - vWorldPos.xyz;
	OUT.fade = 1.0f - saturate (  length(cameraDistance) * g_FadeScale.x - g_FadeScale.y );
	
	return OUT;
}
#endif

float4 PS_Grass(grassVertexOut IN) : COLOR0
{
	// Lookup the colormap tint color
	float4 tint = tex2D(ColorMapSampler, IN.colorMapUV);
	
	// Lookup the diffuse color
	float4 diffuse = tex2D(DiffuseSampler, IN.uv) * IN.color;
		
	// Compute the final color
	float4 finalColor = float4( diffuse.xyz, diffuse.w * IN.fade * 2.0f);
	
	return finalColor;
}

technique draw
{
	pass p0
	{
		ZEnable = true;
		ZWriteEnable = true;
		AlphaTestEnable = true;
		AlphaRef = 130;
		AlphaFunc = Greater;
		AlphaBlendEnable = false;	// RaY
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Grass();
		PixelShader  = compile PIXELSHADER  PS_Grass();
	}
}
