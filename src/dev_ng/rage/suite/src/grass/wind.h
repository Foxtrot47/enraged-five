// 
// grass/wind.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRASS_WIND_H
#define GRASS_WIND_H

#include "math/random.h"
#include "vector/vector3.h"

namespace rage
{

#define WIND_TEXTURE_COUNT		2

class phVelocityField;
class grcTexture;

class grassWind
{
public:
	enum gwType
	{
		gwtVelocityField,
		gwtSinWave,
	};

	grassWind(gwType type, int randomSeed = 0);
	~grassWind();

	void InitVelocityField(int resolution);

	void SetWindVector(const Vector3& windDirectionAndStrength);
	void SetGustChance(int gustPercentChance);
	void SetGustStrength(float minGustStrength, float maxGustStrength);
	void SetGustConeRaidus(float gustConeRadius);

	gwType GetType() const;
	void SetType(gwType newType);

	const grcTexture* GetVelocityTexture() const;

	void Update();

	static void SwapWindTextures();

private:
	void AddConstantWind();
	void AddWindGusts();
	void UpdateVelocityTexture();

protected:
	gwType				m_Type;
	phVelocityField*	m_VelocityField;
	grcTexture*			m_VelocityTexture[WIND_TEXTURE_COUNT];
	Vector3				m_WindDir;
	float				m_WindStr;
	int					m_GustChance;
	float				m_GustMin;
	float				m_GustMax;
	float				m_GustRadius;
	mthRandom			m_Random;

	static int			sm_BuildTexture;
	static int			sm_DrawTexture;
};


}

#endif // GRASS_WIND_H
