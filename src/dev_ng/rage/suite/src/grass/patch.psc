<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::Parsable" constructable="false" autoregister="true">
</structdef>

<structdef type="rage::grassPatchBase" constructable="false" autoregister="true">
</structdef>

<structdef type="rage::grassPatchTextured" base="rage::grassPatchBase" autoregister="true">
	<string name="m_DiffuseTextureName" type="member" size="128"/>
	<int name="m_NumFrames"/>
</structdef>

<structdef type="rage::grassPatchAnimation" base="rage::grassPatchTextured" autoregister="true">
	<float name="m_fps"/>
	<int name="m_delay"/>
</structdef>

<structdef type="rage::grassPatchMovingAnimation" base="rage::grassPatchAnimation" autoregister="true">
	<Vector3 name="m_offset"/>
	<float name="m_stretch"/>
</structdef>

<structdef type="rage::grassPatchPushable" base="rage::grassPatchTextured" autoregister="true">
	
	<bool name="m_drawSphere"/>
	<float name="m_WindStrength"/>
	<float name="m_windSpeed"/>
	<Vector4 name="m_bounds"/>
</structdef>

</ParserSchema>