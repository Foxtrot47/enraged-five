// 
// grass/rage_grasshigh.frag
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "grass/rage_grasscommon.h"
#include "grcore/grcorespu.h"

SPUFRAG_DECL(bool, rage_grasshigh, grcInstanceRenderer::InstanceContext*);
SPUFRAG_IMPL(bool, rage_grasshigh, grcInstanceRenderer::InstanceContext* context)
{
 	VS_Grass_Functor<true,true,true> functor(context);
 	return functor();
}
