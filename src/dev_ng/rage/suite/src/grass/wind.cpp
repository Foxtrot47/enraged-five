// 
// grass/wind.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "grass/wind.h"

#include "pheffects/velocity_field.h"
#include "grcore/image.h"
#include "grcore/texture.h"

namespace rage
{

int		grassWind::sm_BuildTexture = 0;
int		grassWind::sm_DrawTexture = 0;

grassWind::grassWind(gwType type, int randomSeed) :
	m_Type(type),
	m_VelocityField(0),
	m_WindDir(VEC3_ZERO),
	m_WindStr(0),
	m_GustChance(0),
	m_GustMin(0),
	m_GustMax(0),
	m_GustRadius(0),
	m_Random(randomSeed)
{
	for( int i = 0; i < WIND_TEXTURE_COUNT; i++ )
	{
		m_VelocityTexture[i] = 0;
	}
}

grassWind::~grassWind()
{
	for( int i = 0; i < WIND_TEXTURE_COUNT; i++ )
	{
		if( m_VelocityTexture[i] )
			m_VelocityTexture[i]->Release();
	}
	if( m_VelocityField )
		delete m_VelocityField;
}

void grassWind::InitVelocityField(int resolution)
{
	// Make sure this is velocity field wind
	Assert(m_Type == gwtVelocityField);

	// Setup the velocity field
	m_VelocityField = rage_new phVelocityField();
	m_VelocityField->Init(resolution, 0.0f, 0.1f, phVelocityField::phvfbZero);

	// Setup the texture
	grcImage* pImage = grcImage::Create(resolution, resolution, 1, grcImage::A32B32G32R32F, grcImage::STANDARD, 0, 0);
	grcTextureFactory::TextureCreateParams tcp(grcTextureFactory::TextureCreateParams::SYSTEM,grcTextureFactory::TextureCreateParams::LINEAR);
	for( int i = 0; i < WIND_TEXTURE_COUNT; i++ )
	{
		m_VelocityTexture[i] = grcTextureFactory::GetInstance().Create(pImage, &tcp);
	}
	pImage->Release();
}

void grassWind::SetWindVector(const Vector3& windDirectionAndStrength)
{
	m_WindStr = windDirectionAndStrength.Mag();
	m_WindDir = windDirectionAndStrength;
	m_WindDir.Normalize();
}

void grassWind::SetGustChance(int gustPercentChance)
{
	m_GustChance = Max(Min(gustPercentChance, 100), 0);
}

void grassWind::SetGustStrength(float minGustStrength, float maxGustStrength)
{
	m_GustMin = minGustStrength;
	m_GustMax = maxGustStrength;
}

void grassWind::SetGustConeRaidus(float gustConeRadius)
{
	m_GustRadius = gustConeRadius;
}

grassWind::gwType grassWind::GetType() const
{
	return m_Type;
}

void grassWind::SetType(gwType newType)
{
	m_Type = newType;
}

const grcTexture* grassWind::GetVelocityTexture() const
{
	return m_VelocityTexture[sm_DrawTexture];
}

void grassWind::Update()
{
	if( m_Type == gwtVelocityField )
	{
		// Add constant wind
		AddConstantWind();

		// Add gusts of wind
		AddWindGusts();

		// Update the velocity field
		m_VelocityField->Update();

		// Copy the data to the texture
		UpdateVelocityTexture();
	}
}

void grassWind::SwapWindTextures()
{
	// Draw texture is the one we just built
	sm_DrawTexture = sm_BuildTexture;

	// Move the build index to the next texture then wrap around if necessary
	sm_BuildTexture++;
	if( sm_BuildTexture >= WIND_TEXTURE_COUNT )
		sm_BuildTexture = 0;
}


void grassWind::AddConstantWind()
{
	int x = 0;
	int y = 0;
	int nRes = m_VelocityField->GetResolution();
	Vector3 vWindForce = m_WindDir * m_WindStr;

	// Top row of the field
	if( m_WindDir.z > 0.0f )
	{
		y = 0;
		for( x = 0; x < nRes; x++ )
		{
			float fStrength = sinf((float)x) * m_WindStr * m_Random.GetRanged(0.0f, 2.0f);
			vWindForce = m_WindDir * fStrength;
			m_VelocityField->AddForce(x, y, vWindForce);
		}
	}

	// Bottom row of the field
	if( m_WindDir.z < 0.0f )
	{
		y = nRes - 1;
		for( x = 0; x < nRes; x++ )
		{
			float fStrength = sinf((float)x) * m_WindStr * m_Random.GetRanged(0.0f, 2.0f);
			vWindForce = m_WindDir * fStrength;
			m_VelocityField->AddForce(x, y, vWindForce);
		}
	}

	// Left column of the field
	if( m_WindDir.x > 0.0f )
	{
		x = 0;
		for( y = 0; y < nRes; y++ )
		{
			float fStrength = sinf((float)y) * m_WindStr * m_Random.GetRanged(0.0f, 2.0f);
			vWindForce = m_WindDir * fStrength;
			m_VelocityField->AddForce(x, y, vWindForce);
		}
	}

	// Right column of the field
	if( m_WindDir.x < 0.0f )
	{
		x = nRes - 1;
		for( y = 0; y < nRes; y++ )
		{
			float fStrength = sinf((float)y) * m_WindStr * m_Random.GetRanged(0.0f, 2.0f);
			vWindForce = m_WindDir * fStrength;
			m_VelocityField->AddForce(x, y, vWindForce);
		}
	}
}

void grassWind::AddWindGusts()
{
	int randPercent = m_Random.GetRanged((int)1, (int)100);
	if( randPercent <= m_GustChance )
	{
		// Make the gust in the wind direction
		Vector3 vGust = m_WindDir;
		Vector3 vGustDir = Vector3(	m_Random.GetRanged(-m_GustRadius, m_GustRadius),
									m_Random.GetRanged(-m_GustRadius, m_GustRadius),
									m_Random.GetRanged(-m_GustRadius, m_GustRadius) );
		vGust += vGustDir;

		// Make the gust a random strength
		float fGustStrength = m_Random.GetRanged(m_GustMin, m_GustMax);
		vGust *= fGustStrength;

		// Pick a random place in the field to add the gust
		int nRes = m_VelocityField->GetResolution();
        int nGustX = m_Random.GetRanged(0, nRes - 1);
		int nGustY = m_Random.GetRanged(0, nRes - 1);

		// Add the gust of wind
		m_VelocityField->AddForce(nGustX, nGustY, vGust);
	}
}

void grassWind::UpdateVelocityTexture()
{
	// Lock the texture
	grcTextureLock lock;
	m_VelocityTexture[sm_BuildTexture]->LockRect(0, 0, lock);

	// Copy the data into the texture
	int nRes = m_VelocityField->GetResolution();
	Vector3* pTexturePtr = (Vector3*)lock.Base;
	for( int y = 0; y < nRes; y++ )
	{
		const Vector3* pRowPtr = &m_VelocityField->GetVelocity(0, y);
#if VECTORIZED_PADDING
		memcpy(pTexturePtr, pRowPtr, nRes * sizeof(Vector3));
		pTexturePtr += nRes;
#else
		for( int x = 0; x < nRes; x++ )
		{
			*pTexturePtr = pRowPtr[x];
			pTexturePtr = (Vector3*)(((u8*)pTexturePtr) + 16);
		}
#endif		
	}

	// Unlock the texture
	m_VelocityTexture[sm_BuildTexture]->UnlockRect(lock);
}

} // namespace rage

