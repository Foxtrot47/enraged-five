<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<autoregister allInFile="true"/>

<structdef type="Particle">
<Vector3 name="Position"/>
<Vector3 name="Color"/>
<float name="Scale"/>
<int name="TextureFrame"/>
</structdef>

<structdef type="ParticleList">
<string name="Type" type="ConstString"/>
<array name="Particles" type="atArray" indexBits="32">
	<struct type="Particle"/>
</array>
</structdef>

</ParserSchema>