// 
// grass/patch.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef GRASS_PATCH_H
#define GRASS_PATCH_H

#include "data/base.h"
#include "atl/array.h"
#include "file/token.h"
#include "vector/vector2.h"
#include "vector/matrix34.h"
#include "vector/color32.h"
#include "bank/bank.h"
#include "grcore/effect_typedefs.h"
#include "data/resource.h"
#include "paging/dictionary.h"

#include "grmodel/shader.h"
#include "atl/string.h"
#include "atl/ptr.h"
#include "grcore/im.h"

#include "parser/manager.h"
#include "parsercore/attribute.h"

namespace rage
{
#ifndef __GRASS_USE_STREAMABLE_TEXTURES
#define __GRASS_USE_STREAMABLE_TEXTURES 0
#endif


	//
	// linked_ptr - simple reference linked pointer
	// (like reference counting, just using a linked list of the references
	// instead of their count.)
	//
	// The implementation stores three pointers for every linked_ptr, but
	// does not allocate anything on the free store.
	//

template <class X> class LinkedPtr
{
public:
	typedef X element_type;

	explicit LinkedPtr(X* p = 0)
		: m_ptr(p) { m_prev = m_next = this;}

	~LinkedPtr()						{Release();}
	LinkedPtr(const LinkedPtr& r) 	{ Acquire(r);}

	LinkedPtr& operator=(const LinkedPtr& r)
	{
		if (this != &r) {
			Release();
			Acquire(r);
		}
		return *this;
	}
	
	X& operator*()  const	{return *m_ptr;}
	X* operator->() const	{return m_ptr;}
	X* Get()        const	{return m_ptr;}
	bool IsUnique()   const	{return m_prev ? m_prev == this : true;}

private:
	X*                          m_ptr;
	mutable const LinkedPtr*   m_prev;
	mutable const LinkedPtr*   m_next;

	// PURPOSE
	//	 erase this from the list, delete if unique
	//
	void Acquire(const LinkedPtr& r)
	{ 
		m_ptr = r.m_ptr;
		m_next = r.m_next;
		m_next->m_prev = this;
		m_prev = &r;
		r.m_next = this;
	}
	// PURPOSE
	//	 erase this from the list, delete if unique
	//
	void Release()
	{ 
		if (IsUnique()) delete m_ptr; // change to callback to say no used anymore
		else {
			m_prev->m_next = m_next;
			m_next->m_prev = m_prev;
			m_prev = m_next = 0;
		}
		m_ptr = 0;
	}
};
/*

template<class _Type>
class StreamableResourceManager
{
	std::list< pgStreamable< _Type >* >		m_streamables;
	std::list<LinkedPtr<_Type> > m_pointers;

public:
	StreamableResourceManager() 
	{}

	StrongResourcePtr<_Type> Create( const char* fName, const char *ext )
	{
		pgStreamable< _Type >* m_ptr = rage_new pgStreamable< _Type >;

		// Attempt to load resource file, if it exists.
		if ( m_ptr && m_ptr->Init( fName, ext, 0))
		{
			m_ptr->ScheduleAndPlace(0.5f);

			m_streamables.push_back( m_ptr );
			m_pointers.push_back( LinkedPtr<_Type>(m_ptr->GetObject()) );
			return true;
		}
		return false;
	}
	void SetPointerList(LinkedPtr<_Type>* head, _Type* value )
	{
		head->SetPtr( value );
		while ( head = head->Next()) 
		{
			head->SetPtr( value );
		}
	}

	void StreamedCallback( pgStreamable< _Type >* value )
	{
		SetPointerList( value->ClientList,)
	}
	~StreamableResourcePtr() 
	{
		m_ptr->Unschedule();

		if (m_ptr->GetState() != pgStreamableBase::PENDING)
		{
			m_ptr->DeleteSafe();
		}
	}
	bool operator()()
	{
		return ((m_ptr) && (m_ptr->GetState() == pgStreamableBase::RESIDENT));
	}
};
*/


class Parsable
{
public:
	virtual ~Parsable() {}

	bool SaveTreeNode( parTreeNode *pParentNode )
	{
#if !__SPU
		parTreeNode *pNode = PARSER.BuildTreeNode( this );
		if ( pNode )
		{
			pNode->AppendAsChildOf( pParentNode );
			return true;
		}
#endif
		return false;
	}

	bool LoadTreeNode( const parTreeNode *pRootNode )
	{
#if !__SPU
		parTreeNode *pNode = pRootNode->FindChildWithName( parser_GetStructure()->GetName() );
		if ( pNode )
		{
			return PARSER.LoadFromStructure( const_cast<parTreeNode *>(pNode), *parser_GetStructure(), this );
		}
#endif
		return false;
	}

	PAR_PARSABLE;
};
class objInStream
{	
#if !__SPU
	parTree*		m_tree;
	parTreeNode*	m_root;
#endif
	bool			m_isGood;
public:
	objInStream( const char* pFilename, const char* RootName = "root") 
	{
#if !__SPU
		m_isGood = true;
		char path[256];
		fiAssetManager::RemoveExtensionFromPath( path, 256, pFilename );

		m_tree = PARSER.LoadTree( path, "xml" );
		if ( m_tree == NULL )
		{
			Printf( "load tree failed: %s\n", pFilename );
			m_isGood = false;
			return;
		}
		m_root = m_tree->GetRoot();
		if ( strcmp( m_root->GetElement().GetName(),RootName) != 0 )
		{
			Printf( "Invalid file: %s\n", pFilename );
			m_isGood = false;
			return;
		}  
#endif
	}
	~objInStream() 
	{ 
#if !__SPU
		delete m_tree;
#endif
	}

	bool operator()() { return m_isGood; }
#if !__SPU
	parTreeNode*	GetRoot()	{ return m_root; }
#endif
	
};

inline objInStream& operator>>( objInStream& in, Parsable* par )
{
#if !__SPU
	if ( in() )
	{
		par->LoadTreeNode( in.GetRoot() );
	}
#endif
	return in;
}

class objOutStream
{	
	atString		m_name;
#if !__SPU
	parTreeNode*	m_root;
#endif
	bool			m_isGood;
#if !__SPU
	parTree*		m_tree;
#endif
public:
	objOutStream( const char* pFilename, const char* RootName = "root") : m_name( pFilename )
	{
#if !__SPU
		m_tree = rage_new parTree;
		m_root = m_tree->CreateRoot();
		m_root->GetElement().SetName( RootName );
#endif
		m_isGood = true;
	}
	~objOutStream()
	{
#if !__SPU
		char path[256];
		fiAssetManager::RemoveExtensionFromPath( path, 256, m_name );
		if ( !PARSER.SaveTree( path, "xml", m_tree, parManager::XML ) )
		{
			m_isGood = false;
		}
		delete m_tree;
#endif
	}
#if !__SPU
	parTreeNode*	GetRoot()	{ return m_root; }
#endif
};

inline objOutStream& operator<<( objOutStream& out, Parsable* par )
{
#if !__SPU
	par->SaveTreeNode( out.GetRoot() );
#endif
	return out;
}



class grcTexture;
class grmShader;



class grassField;

/*
	Totally abstract base class for use by the grass manager to setup rendering types
*/
typedef atArray<grassField*> GrassRefList;

class grassPatchBase : public Parsable, public atReferenceCounter
{
public:
	grassPatchBase() {}
	virtual ~grassPatchBase() {}


	virtual void LoadTextures() = 0;

	virtual void SetDataTables( Vector3* offsets, Vector2* uvs, Vector4* extraData,int size ) const  = 0;
	virtual void SetThinning( int val, int val2  ) const  = 0;
	virtual void SetQuantisationScaleAndOffset( const Vector3& scale, const Vector3& offset ) = 0;
	
	virtual void SetWind(const Vector3& /*windDirection*/, float /*windStrength*/) {}
	virtual void GetWind(Vector3& windDirection, float& windStrength) const { windDirection = Vector3(0,1,0); windStrength = 0.0f; }
	virtual void SetTime( float time ) = 0;
	virtual float GetTime() const { return 0.0f; }

	virtual void PreDraw();
	virtual bool BeginDraw( const Vector4& extents , int passIndex ) const = 0;
	virtual void EndDraw() const = 0;

	virtual void GetPlayerPosition( Vector3& position, float& radius ) const;

	virtual void GetAlphaFades(float& offset, float& slope) const { offset = 0.0f; slope = 0.0f; }

	// For reference counting textures (implemented by grassPatchTextured)
	virtual void AddReference();
	virtual void RemoveReference();
	virtual int GetRefCount();
	virtual bool CanDraw() { return true; }
	virtual void SetDoubleFade( float /*dist*/, float /*range*/ ) {}
	virtual void GetDoubleFade( float& dist, float& invRange ) const { dist = 0.0f; invRange = 0.0f; }

	virtual Vector4 GetClipRegion() const { return Vector4(1.0f, 1.0f, 0.0f, 0.0f); }

	void AddList( grassField* field );
	void RemoveList( grassField* field );

	GrassRefList& GetList() { return m_fields; }

#if __BANK
	virtual void AddWidgets( bkBank& /*bank*/) {};
#endif


	GrassRefList	m_fields;

	PAR_PARSABLE;
};


/*
	Class which sets up most of the values that you would want set.
*/
class grassPatchTextured : public grassPatchBase
{
public:
	grassPatchTextured() {}
	virtual ~grassPatchTextured();   
	grassPatchTextured( const char* shaderName);

	virtual void LoadTextures();
	virtual void ReleaseTextures() {};

	virtual void SetDiffuseTexture( const char* textureName , int noFrames = 1 );
	void SetNormalMap(const char* textureName);

	virtual void SetDataTables( Vector3* , Vector2*, Vector4* ,int  ) const  {}
	virtual void SetThinning( int val, int val2  )  const;
	virtual void SetQuantisationScaleAndOffset( const Vector3& scale, const Vector3& offset );

	virtual void SetTime( float time );
	virtual float GetTime() const;

	virtual void PreDraw();
	virtual bool BeginDraw( const Vector4& extents , int passIndex ) const ;
	virtual void EndDraw()  const;

	// Reference counting textures
	virtual void AddReference();
	virtual void RemoveReference();
	virtual int GetRefCount();

	void SetDoubleFade( float dist, float range );
	void GetDoubleFade( float& dist, float& invRange ) const;

	grmShader* GetShader() const	{ return m_Shader; }

	// Some external tweaking by overhead screenshot feature.
	void SetTweakers(float sizeTweak, float biasTweak, float plant);

	// Grass fading: set offset, slope
	void SetAlphaFades(float offset, float slope);

	virtual void GetAlphaFades(float& offset, float& slope) const;

	virtual bool CanDraw() { return m_diffuseTexture != 0; }

#if __BANK
	virtual void AddWidgets( bkBank& bank) 
	{
		bank.PushGroup("Texture");

		bank.AddText("Texture", m_DiffuseTextureName, MaxNumberCharactersInString);
		bank.AddSlider("NumFrames", &m_NumFrames, 0, 64, 1 );
		grassPatchBase::AddWidgets(bank );		// do our parent
		bank.PopGroup();
	}
#endif
	PAR_PARSABLE;
private:

	grmShader*						m_Shader;
	
protected:
	atPtr<grcTexture>				m_diffuseTexture;
	atPtr<grcTexture>				m_normalMap;

	
	//StreamableResourcePtr<grcTexture>				m_diffuseTexture;
	//StreamableResourcePtr<grcTexture>				m_normalMap;

	int								m_NumFrames;

	static const int				MaxNumberCharactersInString = 128;
	char							m_DiffuseTextureName[MaxNumberCharactersInString];
	char							m_NormalMapName[MaxNumberCharactersInString];
private:
	grcEffectVar	m_DiffuseTextureID;
	grcEffectVar	m_NormalMapID;
	grcEffectVar	m_FadeOutConstantID;
	grcEffectVar	m_GrassTexSizeID;
	grcEffectVar	m_ThinAmountID;
	grcEffectVar	m_ThinFadeID;
	grcEffectVar	m_QuantScaleID;
	grcEffectVar	m_QuantOffsetID;

	static grcEffectVar sm_GrassDoubleFadeDistId;	// what do sizeTweak and biasTweak do?  SANTOKI 6/4 migration

	// Santoki: these could be made static, since they will be the same for all passes of this shader per frame.
	grcEffectVar	m_TimerID;
	grcEffectVar	m_sizeTweakID;
	grcEffectVar	m_biasTweakID;
	grcEffectVar	m_plantTweakID;


	// Fading out grass (using alpha to mask)
	grcEffectVar	m_fadeOffsetSlopeID;

	grcEffectTechnique  m_drawTechnique;

	int				m_DiffuseTexRefCount;
};

/*
	PURPOSE
		Does a simple animation on the grass note I'm using teh base class to do all the boring setup
*/
class grassPatchAnimation : public grassPatchTextured
{
public:
	grassPatchAnimation() {}
	grassPatchAnimation( const char* shaderName, float fps = 30.0f , int frameDelay = 1 )
		: grassPatchTextured( shaderName ), m_fps( fps ), m_delay( static_cast<float>( frameDelay ) )
	{
		m_fpsID = GetShader()->LookupVar( "FramesPerSecond", false );
		m_FrameFadeDelayID = GetShader()->LookupVar( "FrameFadeDelay", false);
	}

	bool BeginDraw( const Vector4& extents , int passIndex ) const 
	{
		GetShader()->SetVar( m_fpsID, m_fps );
		GetShader()->SetVar( m_FrameFadeDelayID, m_delay );
		return grassPatchTextured::BeginDraw( extents, passIndex );
	}

	void SetFPS( float v) { m_fps = v; }
	void SetDelay( int delay ) { m_delay = static_cast<float>(delay ); }

#if __BANK
	virtual void AddWidgets( bkBank& bank) 
	{
		bank.PushGroup("Drops");

		bank.AddSlider("FramesPerSecond", &m_fps, 0.0f, 240.0f, 0.01f );
		bank.AddSlider("FrameDelay", &m_delay, 0, 10, 1 );

		grassPatchTextured::AddWidgets(bank );		// do our parent
		bank.PopGroup();
	}
#endif
	PAR_PARSABLE;
protected:
	float			m_fps;
	float			m_delay;
	grcEffectVar	m_fpsID ;
	grcEffectVar	m_FrameFadeDelayID;
};


/*
PURPOSE
	Moves the grass along a vector using the animation controls
*/
class grassPatchMovingAnimation : public grassPatchAnimation
{
public:
	grassPatchMovingAnimation() {}
	grassPatchMovingAnimation( const char* shaderName, float fps = 3.0f , int frameDelay = 1 )
		: grassPatchAnimation( shaderName, fps, frameDelay ), m_offset( 0.0f, 5.0f, 20.0f ), m_stretch(2.0f)
	{
		m_offsetID = GetShader()->LookupVar( "MoveDirection", false );
		m_stretchID = GetShader()->LookupVar( "MoveStretch", false );
	}

	bool BeginDraw( const Vector4& extents , int passIndex ) const 
	{
		GetShader()->SetVar( m_offsetID, m_offset );
		GetShader()->SetVar( m_stretchID, m_stretch );
		return grassPatchAnimation::BeginDraw( extents, passIndex );
	}

	void SetOffset( const Vector3& dir) { m_offset = dir; }
	void SetStretch( float v ) { m_stretch = v; }
	

#if __BANK
	virtual void AddWidgets( bkBank& bank) 
	{
		bank.PushGroup("Rain");
		bank.AddSlider("Move Direction", &m_offset, 0.0f, 100.0f, 0.01f );
		bank.AddSlider("Move Stretch", &m_stretch, 0.0f, 10.0f, 0.01f );
	
		grassPatchAnimation::AddWidgets(bank );		// do our parent
		bank.PopGroup();
	}
#endif
	PAR_PARSABLE;
protected:
	Vector3			m_offset;
	float			m_stretch;
	grcEffectVar	m_stretchID;
	grcEffectVar	m_offsetID ;
};





class grassPatch : public grassPatchBase
{
public:

	DECLARE_PLACE(grassPatch);

	grassPatch(datResource &rsc);
	grassPatch();
	~grassPatch();

	static void InitClass();
	static void ShutdownClass();

	const Vector2& GetCardSize() const;
	const Vector2& GetCardSizeVariance() const;
	void SetCardSize(float width, float height);
	void SetCardSizeVariance(float widthVariance, float heightVariance);

	int GetWidthTextureVariantCount() const;
	int GetHeightTextureVariantCount() const;
	void SetWidthTextureVariantCount(int count);
	void SetHeightTextureVariantCount(int count);

	const char* GetDiffuseName() const;
	const char* GetColorMapName() const;

	void LoadTextures(const char* diffuse, const char* colorMap, const char* normalMap = 0);
	void LoadTextures();
	void SetTextureNames(const char* diffuse);

	void SetZup(bool useZup);

	void SetDataTables( Vector3* offsets, Vector2* uvs, Vector4* extraData,int size ) const;
	virtual void SetThinning( int val, int val2  ) const;
	virtual void SetQuantisationScaleAndOffset( const Vector3& scale, const Vector3& offset );
	
	void SetGrassColor(const Vector3& color) const;
	void SetCameraFacing( bool cameraFacing ) const;
	void SetWind( const Vector3& windDirection, float windStrength);
	void GetWind(Vector3& windDirection, float& windStrength) const;
	virtual void SetTime(float time);
	virtual float GetTime() const;

	virtual bool BeginDraw( const Vector4& extents , int passIndex ) const;
	virtual void EndDraw() const;

	//
	//	PURPOSE
	//		Allows for overriding the shader for a game specific one
	//
	static void SetShader( const char* ShaderName );
	static grmShader* GetShader();



	// Some external tweaking by overhead screenshot feature.
	static void SetTweakers(float sizeTweak, float biasTweak);

protected:

#if __GRASS_USE_STREAMABLE_TEXTURES

	StreamableResourcePtr<grcTexture> m_DiffuseTexture;

#else

	atPtr<grcTexture>	m_DiffuseTexture;

#endif

	static const u32	MAPNAME_LEN = 64;

	char				m_DiffuseName[MAPNAME_LEN];
	char				m_ColorMapName[MAPNAME_LEN];
	char				m_NormalMapName[MAPNAME_LEN];

	Vector2				m_CardSize;
	Vector2				m_CardSizeVariance;

	int					m_WidthTextureVariants;
	int					m_HeightTextureVariants;

	grcTexture*				m_ColorMapTexture;
	grcTexture*				m_NormalMapTexture;



	bool					m_Zup;
	
	static grmShader*	sm_GrassShader;
	static grcEffectVar	sm_DiffuseTexture;
	static grcEffectVar	sm_ColorMapTexture;
	static grcEffectVar sm_NormalMapTexture;
	static grcEffectVar	sm_WindTexture;
	static grcEffectVar	sm_UseWindTexture;
	static grcEffectVar	sm_UseNormalMap;
	static grcEffectVar sm_FieldExtents;
	static grcEffectVar sm_FadeOutConstant;

	static grcEffectVar	sm_RandomOffsetTable;
	static grcEffectVar	sm_RandomUVsTable;
	static grcEffectVar sm_ThinAmount;
	static grcEffectVar sm_ThinFade;

	static grcEffectVar	sm_CameraFacing;

	static grcEffectVar	sm_WindDirection;
	static grcEffectVar	sm_WindStrength;
	static grcEffectVar	sm_Timer;

	static grcEffectVar	sm_ExtraDataTable;
	
	static const char*	sm_GrassShaderName;
	static bool			sm_Overriden;

	static grcEffectVar sm_sizeTweak;
	static grcEffectVar sm_biasTweak;
	static grcEffectVar sm_GrassTexSize;
};

class grassPatchPushable : public grassPatchTextured
{
	grcEffectVar m_PersonID;
	grcEffectVar m_WindDirectionID;
	grcEffectVar m_WindStrengthID;
	grcEffectVar m_pushAmountID;

protected:
	Vector4		 m_bounds;
	bool		 m_drawSphere;
	Vector3		 m_WindDirection;
	float		 m_WindStrength;
	float		m_windSpeed;
	float		m_pushAmount;

public:
	grassPatchPushable() {}
	grassPatchPushable( const char* shader )
		: grassPatchTextured( shader) ,m_drawSphere( false ), m_WindStrength(0.5f ), m_windSpeed( 2.0f )
	{
		m_PersonID = GetShader()->LookupVar("PersonPosition", false);
		m_WindDirectionID = GetShader()->LookupVar("GrassWindDirection", false);
		m_WindStrengthID = GetShader()->LookupVar("GrassWindStrength", false);
		m_pushAmountID = GetShader()->LookupVar("GrassPushAmount", false );

		m_bounds = Vector4( 0.0f, 0.0f, 0.0f, 1.0f );
	}
	void SetPlayer( const Vector3& pos, float radius, float pushAmount )
	{
		m_bounds = Vector4( pos.x, pos.y, pos.z, radius );
		m_pushAmount = pushAmount;
	}
	void GetPlayerPosition( Vector3& pos, float& radius ) const
	{
		pos = m_bounds.GetVector3();
		radius = m_bounds.GetW();
	}
	void SetWind( const Vector3& windDirection, float windStrength)
	{
		GetShader()->SetVar( m_WindDirectionID, windDirection);
		GetShader()->SetVar( m_WindStrengthID, windStrength);
	}
	void GetWind(Vector3& windDirection, float& windStrength) const
	{
		GetShader()->GetVar( m_WindDirectionID, windDirection);
		GetShader()->GetVar( m_WindStrengthID, windStrength);
	}
	void SetTime( float t )
	{
		grassPatchTextured::SetTime( t * m_windSpeed );
	}

	bool BeginDraw( const Vector4& extents , int passIndex ) const
	{
		Vector3	windDir( 1.0f, 1.0f, 0.0f );
		//SetWind( windDir, m_WindStrength);  grass manager sets this directly.  destroy m_Wind variables, they just fuck everything up!

		if ( m_drawSphere )
		{
			grcWorldIdentity();
			Vector3 centre( m_bounds.x, m_bounds.y, m_bounds.z );
			grcDrawSphere( m_bounds.w, centre, 8, true );
			grcWorldIdentity();
		}
		GetShader()->SetVar( m_PersonID, m_bounds );
		GetShader()->SetVar( m_pushAmountID, m_pushAmount );
		return grassPatchTextured::BeginDraw( extents, passIndex );
	}
#if __BANK
	virtual void AddWidgets( bkBank& bank) 
	{
		bank.PushGroup("Pushable Grass");
		bank.AddSlider("Player Position", &m_bounds, -100.0f, 100.0f, 0.01f );

		bank.AddSlider("Wind Strength", &m_WindStrength, 0.0f, 10.0f, 0.01f );
		bank.AddSlider("Wind Speed", &m_windSpeed, 0.0f, 10.0f, 0.01f );
		bank.AddToggle("Draw Sphere", &m_drawSphere );
		grassPatchTextured::AddWidgets(bank );		// do our parent
		bank.PopGroup();
	}
#endif
	PAR_PARSABLE;
};
	

} // namespace rage

#endif // GRASS_PATCH_H

