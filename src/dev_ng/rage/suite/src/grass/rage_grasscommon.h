// 
// grass/rage_grasscommon.h
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRASS_RAGE_GRASSCOMMON_H
#define GRASS_RAGE_GRASSCOMMON_H

#if __PS3
#include "grcore/instancerendererspu.h"
#include "grcore/vertexdecl.h"
#include "math/float16.h"
#include "system/alloca.h"
#include "system/codefrag_spu.h"
#include "system/spu_compress.h"
#include "system/spu_decompress.h"
#include "vector/color32.h"
#include "vectormath/classes.h"

using namespace rage;

struct VS_Grass_TypeData
{
	Mat34V CameraMat;
	Vec4V GlobalScale;
	Vec4V GlobalBias;
	Vec2V DoubleFadeDist; // float2(20.0f,1.0/5.0f);
	Vec3V GrassWindDirection;
	Vec4V PersonPosition;
	ScalarV GrassTimer;
	ScalarV GrassWindStrength;
	Vec4V Plane0x, Plane0y, Plane0z, Plane0w;
	Vec4V Plane1x, Plane1y, Plane1z, Plane1w;
	Vec4V ClipDims;
} ;

#if __SPU
template <bool _UseCylinder,bool _ApplyWind,bool _ApplyPush>
class VS_Grass_Functor
{
public:
	inline VS_Grass_Functor(grcInstanceRenderer::InstanceContext* context)
	: m_Context(context)
	, m_TypeData(reinterpret_cast<const VS_Grass_TypeData*>(context->TypeData))
	{
	}

	struct grassVertexOut
	{
		u32 Diffuse;
		float Position[3];
		u16 TexCoords[2];
	};
	struct grassVertexIn
	{
		u32 Position;
		u32 Diffuse;
	};

	bool operator()(void)
	{
		const ScalarV v_one(V_ONE);
		const ScalarV v_two(V_TWO);
		const ScalarV v_zeroPointFour = ScalarVFromF32(0.4f);
		const ScalarV v_fourPointThree = ScalarVFromF32(4.3f);
		const ScalarV v_oneTenth = ScalarVFromF32(0.1f);
		const ScalarV v_half(V_HALF);

		u8* vertexOutRaw = m_Context->OutputVertexData;
		const grassVertexIn* vertexIn = reinterpret_cast<const grassVertexIn*>(m_Context->InputInstanceData);

		Vec4V center;
		const vec_int4 packedPosition = { vertexIn->Position, 0, 0, 0 };
		pack11_11_10To3Floats(&center.GetIntrin128Ref(), &packedPosition, 1);

		// decompress position
		center.SetXYZ(center.GetXYZ() * m_TypeData->GlobalScale.GetXYZ() + m_TypeData->GlobalBias.GetXYZ());

 		Vec4V sphere(center.GetXYZ(), v_two); // Hard code sphere radius to 2.0 for now
 		if (!IsVisible(sphere))
 		{
 			return false;
 		}

		// RGB(A) <=> ARG(B)
		const ScalarV scale = Color32(vertexIn->Diffuse).GetARGB().GetW();
		//const ScalarV scale = ScalarVFromF32((float)(vertexIn->Diffuse & 0xff) / 255.0f);
	
		for (u32 i = 0; i < m_Context->NumVertexesPerInstance; ++i)
		{
			grassVertexOut* vertexOut = reinterpret_cast<grassVertexOut*>(vertexOutRaw);

			Vec2V uvs;

			const u32 vertexIdx = m_Context->InstanceIdx * m_Context->NumVertexesPerInstance + i;
			Vec3V position = GrassDecodeClippedVert(vertexIdx, center, scale, uvs);
			ScalarV scale = v_two;
			ScalarV isGrassTop = v_one - uvs.GetY();

			if (_ApplyWind)
			{
				// Animate grass
				ScalarV t = m_TypeData->GrassTimer * v_zeroPointFour;
				ScalarV bias = t + position.GetX() * v_half;
				ScalarV sinWave0;
				ScalarV cosWave0;
				ScalarV sinWave1;
				ScalarV cosWave1;
				SinAndCos(sinWave0, cosWave0, bias * v_half);
				SinAndCos(sinWave1, cosWave1, bias * v_fourPointThree);
				Vec2V offset(cosWave1, sinWave1);
				offset *= v_oneTenth;
				offset += Vec2V(sinWave0, cosWave0);
				offset *= isGrassTop;
				offset *= m_TypeData->GrassWindStrength;
				const Vec3V offsetXYY(offset.GetX(), offset.GetY(), offset.GetY()); // TODO: add Vec2V::Get accessor
				position += offsetXYY * m_TypeData->GrassWindDirection;
			}

			if (_ApplyPush)
			{
 				// if person is near push the grass out of the way  ( could do four at a time for cars? )
 				Vec3V v_pushForce = GetPersonPushForce(position, m_TypeData->PersonPosition, isGrassTop, scale);
 				position += v_pushForce;
			}

			vertexOut->Position[0] = position.GetXf();
			vertexOut->Position[1] = position.GetYf();
			vertexOut->Position[2] = position.GetZf();
			vertexOut->TexCoords[0] = Float16(uvs.GetXf()).GetBinaryData();
			vertexOut->TexCoords[1] = Float16(uvs.GetYf()).GetBinaryData();
			vertexOut->Diffuse = vertexIn->Diffuse;

			vertexOutRaw += m_Context->OutputVertexStride;
		}

		return true;
	}

private:
	grcInstanceRenderer::InstanceContext* m_Context;
	const VS_Grass_TypeData* m_TypeData;

	Vec3V_Out CalculateBentVerts(int index)
	{
		float y =(float)( index > 1  && index != 5);
		float x =(float)( index > 3 );
		float z = (float)( index==1 || index ==2 );

		return Vec3V( x, y, z );
	}

	// Stolen from grcore/viewport_inline.h
	bool IsVisible(Vec4V_In sphere) const
	{
		Vec4V localSph( sphere );
		Vec4V xxxx = Vec4V(SplatX(sphere));	// replicate .x through a vector
		Vec4V yyyy = Vec4V(SplatY(sphere));	// ...and y, etc
		Vec4V zzzz = Vec4V(SplatZ(sphere));
		Vec4V neg_wwww = Vec4V( -SplatW(sphere) );	// compute 0-w through a vector

		Vec4V sum0, sum1;
		sum0 = AddScaled( m_TypeData->Plane0w, zzzz, m_TypeData->Plane0z );
		sum0 = AddScaled( sum0, yyyy, m_TypeData->Plane0y );
		sum0 = AddScaled( sum0, xxxx, m_TypeData->Plane0x );
		sum1 = AddScaled( m_TypeData->Plane1w, zzzz, m_TypeData->Plane1z );
		sum1 = AddScaled( sum1, yyyy, m_TypeData->Plane1y );
		sum1 = AddScaled( sum1, xxxx, m_TypeData->Plane1x );

		VecBoolV cmp0 = IsGreaterThanOrEqual(sum0, neg_wwww);		// Planes that pass visibility will have 0xFFFFFFFF in their column.
		VecBoolV cmp1 = IsGreaterThanOrEqual(sum1, neg_wwww);
		VecBoolV final = And(cmp0,cmp1);				// Intersect the two sets of frustum clip planes.

		// Copy appropriate bit out of condition code which is nonzero if all four columns matched the test vector.
		return IsEqualIntAll( final, VecBoolV(V_T_T_T_T) ) != 0/*cullOutside*/;
	}

	Vec3V_Out GetPersonPushForce(Vec3V_In vWorldPos, Vec4V_In personPosition, ScalarV_In isGrassTop, ScalarV_In maxExtension)
	{
		const ScalarV v_zero(V_ZERO);
		const ScalarV v_personPositionW = personPosition.GetW();
		Vec3V delta = vWorldPos - personPosition.GetXYZ();
		ScalarV dist = Max(v_personPositionW - Mag(delta), v_zero);
		// reduce by strength 
		dist *= (dist * isGrassTop) / v_personPositionW;
		dist = Min(dist * isGrassTop, maxExtension); // clamp the distance to a maximum
		const Vec3V v3_one(V_ONE);
		return delta * dist * m_TypeData->GrassWindDirection + (v3_one - m_TypeData->GrassWindDirection) * -dist; // only do horizontally
	}

	Vec3V_Out GetNormalDirection(u32 BladeId, Vec2V_In axiasScale, ScalarV_In angleOffset)
	{
		const ScalarV v_bladeId_mod_30 = ScalarVFromF32((float)(BladeId % 30));
		const ScalarV v_15 = ScalarVFromF32(15.0f);
		const ScalarV v_pi(ScalarVConstant<U32_PI>());
		ScalarV angleDir = (v_bladeId_mod_30 - v_15) / v_15 * v_pi;
		angleDir += angleOffset;

		ScalarV v_cosAngleDir;
		ScalarV v_sinAngleDir;
		SinAndCos(v_sinAngleDir, v_cosAngleDir, angleDir);

		return Vec3V(v_cosAngleDir * axiasScale.GetX(), -axiasScale.GetY(), v_sinAngleDir * axiasScale.GetX());
	}

	Vec3V_Out GrassDecodeClippedQuad(u32 i, Vec4V_In position, ScalarV_In scale, Vec2V_InOut uvs)
	{
		const ScalarV v_zero(V_ZERO);
		const ScalarV v_one(V_ONE);

		Vec2V tluv( 0.0f, 1.0f );
		Vec2V truv( 1.0f, 1.0f );
		Vec2V bluv( 0.0f, 0.0f );
		Vec2V bruv( 1.0f, 0.0f );
		const Vec2V offsets[] =
		{
			tluv,
			truv,
			bruv,
			bluv
		};
		uvs = offsets[i % 4];

		// calculate edge
		const ScalarV v_half(V_HALF);
		const Vec2V uvs_offset(v_half, v_one);
		const ScalarV v_two(V_TWO);
		const Vec2V clipDimsXY = m_TypeData->ClipDims.Get<Vec::X, Vec::Y>();
		Vec2V axiasScale = (uvs - uvs_offset) * scale * v_two * clipDimsXY;
		const ScalarV ENLARGE_AMT = ScalarVFromF32(0.7f);

		axiasScale.SetX(axiasScale.GetX() * (v_one + ENLARGE_AMT)); // scale up to fill area.

		Vec3V Normal = GetNormalDirection(i / 4, axiasScale, v_zero);

		// rescale uv's to focus on the clip area
		uvs = uvs * clipDimsXY + m_TypeData->ClipDims.Get<Vec::Z, Vec::W>();
		uvs.SetX(uvs.GetX() * v_two);

		return position.GetXYZ() + Normal;
	}

	Vec3V GrassDecodeClippedCylinder(u32 i, Vec4V_In position, ScalarV_In scale, Vec2V_InOut uvs)
	{
		const u32 NUM_HIGH_GRASS_VERTS = 6;
		const u32 vertex = i / NUM_HIGH_GRASS_VERTS;
		const u32 index = i - vertex * NUM_HIGH_GRASS_VERTS;

		Vec3V vert = CalculateBentVerts(index);
		uvs = vert.Get<Vec::X, Vec::Y>();

		ScalarV distance(Mag(m_TypeData->CameraMat.GetCol3() - position.GetXYZ()));
		const ScalarV v_zero(V_ZERO);
		const ScalarV v_one(V_ONE);
		ScalarV distFade = Saturate(v_one - ((distance - m_TypeData->DoubleFadeDist.GetX()) * m_TypeData->DoubleFadeDist.GetY()));

		// calculate edge
		ScalarV angleFadeIn = v_one - Saturate(m_TypeData->GlobalBias.GetW());
		const ScalarV v_half(V_HALF);
		uvs.SetX(uvs.GetX() + v_half * vert.GetZ());

		const Vec2V v_uvsOffset(v_half, v_one);
		const ScalarV v_two(V_TWO);
		const Vec2V clipDimsXY = m_TypeData->ClipDims.Get<Vec::X, Vec::Y>();
		Vec2V axiasScale = (uvs - v_uvsOffset) * scale * v_two * clipDimsXY;
		const ScalarV v_threeQuarters = ScalarVFromF32(0.75f);
		axiasScale.SetX(axiasScale.GetX() + vert.GetZ() * scale * v_threeQuarters * m_TypeData->ClipDims.GetX() * angleFadeIn);

		// scale up in distance
		const ScalarV ENLARGE_AMT = ScalarVFromF32(0.7f);
		axiasScale.SetX(axiasScale.GetX() * ((v_one + ENLARGE_AMT) - ENLARGE_AMT * angleFadeIn));
		const ScalarV v_piOverTwo(Vec::V4VConstant(V_PI_OVER_TWO));
		Vec3V Normal = GetNormalDirection(vertex + (u32)m_TypeData->GlobalScale.GetWf(), axiasScale, vert.GetZ() * v_piOverTwo * angleFadeIn);

		uvs = uvs * clipDimsXY + m_TypeData->ClipDims.Get<Vec::Z, Vec::W>();
		uvs.SetX(uvs.GetX() * (v_two - angleFadeIn));

		return position.GetXYZ() + Normal;
	}

	Vec3V GrassDecodeClippedVert(u32 vertexIdx, Vec4V_In position, ScalarV_In scale, Vec2V_InOut uvs)
	{
		if (_UseCylinder)
		{
			return GrassDecodeClippedCylinder(vertexIdx, position, scale, uvs);
		}
		else
		{
			return GrassDecodeClippedQuad(vertexIdx, position, scale, uvs);
		}
	}
};
#endif // __SPU
#endif // __PS3

#endif // GRASS_RAGE_GRASSCOMMON_H
