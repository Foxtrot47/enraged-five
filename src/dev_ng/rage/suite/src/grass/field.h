// 
// grass/field.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRASS_FIELD_H
#define GRASS_FIELD_H

#include "atl/array.h"
#include "atl/functor.h"
#include "atl/string.h"
#include "data/base.h"
#include "data/resource.h"
#include "data/safestruct.h"
#include "file/token.h"
#include "grass/patch.h"
#include "grcore/texture.h"
#include "grcore/vertexbuffer.h"
#include "math/float16.h"
#include "paging/base.h"
#include "spatialdata/aabb.h"
#include "string/stringhash.h"
#include "system/task.h"
#include "vector/color32.h"
#include "vector/matrix34.h"
#include "spatialdata/sphere.h"

#define GRASS_STATS			0
#define GRASS_SPU_INSTANCE_RENDERER (__PPU && 1)

namespace rage
{

class grassPatch;
class grassWind;
class spdShaft;


#if __XENON	|| __PS3
#define NUM_VERTS_PER_PRIMITIVE		4
#else
#define NUM_VERTS_PER_PRIMITIVE		6
#endif




// needs to be kept in sync with the shaders
class grassExtraBladeData
{
public:
	virtual void RegisterExtraData( grcFvf& fvf  ) = 0;
	virtual void Begin()  {}
	virtual void SetExtraDataBlade( int vi, grcVertexBuffer* vb, const Vector3& position, float size ) = 0;
	virtual void End() {};
	virtual ~grassExtraBladeData() {}
};


////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		This structure represents a vector composed of Float16's.  It's a smaller
//	Vector3.  I made it to cut field data sizes in half.
//
struct Vector16 : public datBase
{
	Float16 x;
	Float16 y;
	Float16 z;

	// PURPOSE: Used by the rorc resourcing system
	void Place(class datResource&) { }

	Vector16() {}
	Vector16( const Vector3& other )
	{
		Set( other.x, other.y, other.z );
	}
	void Set(float a, float b, float c) {x.SetFloat16_FromFloat32(a); y.SetFloat16_FromFloat32(b); z.SetFloat16_FromFloat32(c);}


	const Vector16& operator=( const Vector3& other )
	{
		Set( other.x, other.y, other.z );
		return *this;
	}
};


class ParticleList;

struct grassLod
{
	float	transToLowDist; //25.0f
	int		shadowReduceAmt; // 4 
	int		cheapPassOffset;//3
	bool	useCylinderGrass;
	float	doubleDrawDist;
	float	doubleFadeDist;
	float	farShadowDistance;
	float	maxRenderDistance;
	float   ThinStart[2];
	float   ThinEnd[2];
	float	GlobalThinning;
	float	ThinPower[2];
	grassLod() :
	transToLowDist( 16.0f),
		shadowReduceAmt( 4),
		cheapPassOffset( 3),
		useCylinderGrass( true),
		doubleDrawDist( 13.0f ),
		doubleFadeDist( 3.0f),
		farShadowDistance(100.0f),
		maxRenderDistance(180.0f),
		GlobalThinning(1.0f)
	{	

		ThinStart[0] =40.0f;
		ThinEnd[0]=180.0f;
		ThinPower[0]=0.75f;

		
		ThinStart[1] =10.0f;
		ThinEnd[1]=120.0f;
		ThinPower[1]=0.4f;
	}

#if __BANK
	void AddWidgets( bkBank& bank )
	{
		bank.AddSlider( "Global Reduce %", &GlobalThinning, 0.0f, 1.0f, 0.0001f );
		bank.AddSlider( "Transition to Low", &transToLowDist, 0.0f, 100.0f, 0.01f );
		bank.AddSlider( "Transition Distance", &doubleDrawDist, 0.0f, 100.0f, 0.01f );

		bank.PushGroup("Thinning");
			bank.AddSlider("Start", &ThinStart[0], 1.0f,256.0f, 0.01f);
			bank.AddSlider("End", &ThinEnd[0],1.0f,256.0f, 0.01f);
			bank.AddSlider("Power", &ThinPower[0], 0.0f,1.0f, 0.01f);

			bank.AddSlider("Shadow Start", &ThinStart[1], 1.0f,256.0f, 0.01f);
			bank.AddSlider("Shadow End", &ThinEnd[1],1.0f,256.0f, 0.01f);
			bank.AddSlider("Shadow Power", &ThinPower[1], 0.0f,1.0f, 0.01f);

		bank.PopGroup();
	}
#endif
};

class grassField 
{
	struct texPlacementValues
	{
		atArray<Vector3, 0, unsigned int>  	m_Patches;
		atArray<float, 0, unsigned int>		m_HeightScales;
		atArray<Color32, 0, unsigned int>	m_Color;
		u8 pad;
	};
	enum gfeViewQuadrant
	{
		gfevqRight = 0,
		gfevqLeft,
		gfevqTop,
		gfevqBottom,
		gfevqCount
	};

	Vector4											m_Extents;
	Vector3											m_aabbMax;
	Vector3											m_aabbMin;
	Vector4											m_aabbScale;
	Vector4											m_aabbOffset;

	texPlacementValues*								m_tp;

	grcVertexDeclaration*							m_VertexDeclaration;
	datOwner<grcVertexBuffer>						m_VertexBuffer;
	datOwner<grcIndexBuffer>						m_IndexBuffers[gfevqCount];

	bool											m_Zup;
	bool											m_useSortedBuffers;
	u8												m_Pad0[2];

	atString										m_Type;
	u32												m_TypeHash;
	u8												m_Pad1[4];

	static const int RandomTableSize = 16;
	static Vector3		sm_OffsetTable[ RandomTableSize];
	static Vector2		sm_UVsTable[ RandomTableSize ] ;
	static Vector4		sm_ExtraVertData[ RandomTableSize ] ;
	static bool			sm_built;
	static bool			sm_updated;


	static grcIndexBuffer*							sm_UnSortedIndexBuffer;
	static grassPatchBase*							sm_PrePatch;
	static	int										sm_passIndex;

	static grcVertexBuffer*							sm_HighBladeVertexBuffer;
public:

	static const int RORC_VERSION = 7;
	DECLARE_PLACE(grassField);
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif

	grassField(datResource &rsc);
	grassField( atPtr<ParticleList> pl );
	grassField( ParticleList& pl );
	grassField( ParticleList& pl, const char* type);
	grassField();
	~grassField();

	void PostPlace();

	void SetType( const atString& str ) { m_Type = str; }
	const atString& GetType() const { return m_Type; }
	u32 GetHash() { return m_TypeHash; }


	// Uses maxExtents passed in.
	u32 Create(const grcImage* fieldColorMap, 
				const grcImage* fieldColorMap2,
				const Vector4& boxExtents, const Vector4& maxExtents, float placementResolution, const Vector4& subImage, const Vector2& cardSize, int amountPerTexture);

	void SetupDrawBuffers(  const Vector2& cardSize,  const Vector2& cardSizeVariance, int widthTextureVariantCount,  int heightTextureVariantCount  );
	void Create( const atPtr<ParticleList> p );
	void Create( ParticleList& p );
	void GetGrassPatches(Vector3** outGrassPatchPointers, int& outGrassPatchCount);

	const Vector4& GetExtents() const;
	float GetResolution() const;
	void GetExtents(Vector4 &fieldExtents);
	void SetZup(bool useZup);
	void GetFieldCenter(Vector3& out);

	//
	//	PURPOSE
	//		Draws the field as an index buffer
	// 
	void Draw( 	grassLod& lod,
				grassPatchBase* patch,
				 bool useCylinderGrass,
				float startFade,
				float endFade,
				float fadePower,
				atArray<sysTaskHandle>& handles,
				int passIndex = 0
				);

	void DrawZPrePass( grassPatchBase* patch, float lod = 0.0f );

	bool IsVisible( const spdShaft* shafts, int amtShafts,  float& outZ , float& endZ );
	
	// 
	// PURPOSE
	//		Allows fast calcution of random table data for use with grass blades
	//		this allows for greatly reducing the vertex / pixel shader cost
	//		by utlitizing the cpu
	//
	template<class _FUNCTOR >
	static void CalculateExtraShaderTable( _FUNCTOR& func )
	{
		for ( int i = 0; i < 2; i++ )
		{
			sm_ExtraVertData[i] = func( sm_OffsetTable[i], sm_UVsTable[i] );
		}
		sm_updated = true;
	}

	

	void DebugDraw()
	{
		// Disabled because spdAABB doesn't seem to have a Draw() function any more.
//		spdAABB aabb;
//		aabb.Set( RCC_VEC3V(m_aabbMin), RCC_VEC3V(m_aabbMax) );
//		aabb.Draw( Color32( 1.0f, 0.5f, 1.0f, 0.4f ));
	}

	int ClearAreaOfGrass(const spdSphere& area);

	static void SetupStaticIndexBuffer( int NoVertsPerPrimitive, int vertCount );
	static void DestroyStaticIndexBuffer();

	
	static void ResetCachedPatch();

	
	static bool* GetTransitionValue() { return &sm_GrassTransition ; }	
	static bool* GetDoubleDrawValue() { return &sm_GrassDoubleDraw ; }	

	Vec3V_Out GetMin() const { return RCC_VEC3V( m_aabbMin ); }
	Vec3V_Out GetMax() const { return RCC_VEC3V( m_aabbMax ); }
protected:


	static bool	sm_GrassTransition;
	static bool	sm_GrassDoubleDraw;

	void SetupRandomTables( const Vector2& cardSize,  const Vector2& cardSizeVariance, int widthTextureVariantCount,  int heightTextureVariantCount  );
	void CreateVertexBuffer( grcFvf& fvf , const Vector2& cardSize,  const Vector2& cardSizeVariance, int widthTextureVariantCount,  int heightTextureVariantCount  );  
	
	gfeViewQuadrant GetSortedBufferToDraw();

	void CreateSortedIndexBuffers( int NoVertsPerPrimitive , int vertCount );
	void CreateIndexBuffers( int NoVertsPerPrimitive, int vertCount );

	Vector3 SetBounds( const Vector3& min, const Vector3& max );
};

struct FieldList : public pgBase
{
public:
#if HACK_RDR2
	DECLARE_THREADED_PLACE();
#endif // HACK_RDR2

	atArray< datOwner<grassField> > el;
	u32 m_ID;
	atString										m_Type;

	static const int RORC_VERSION = grassField::RORC_VERSION + 1;
	FieldList(datResource &rsc) : el( rsc, 1 )
	{
	}

	void PostPlace();

	int Release() const {delete this; return 0;}


	FieldList()
	{
		el.Reset();
		m_ID = 0;
	}
	
	~FieldList()
	{
		int fieldCount = 0;
		for ( int i = 0; i < GetCount(); i++ )
		{
			if (el[i])
				delete el[i];
			else
				fieldCount++;
		}
		#ifdef GRASSRC_TOOL
		Displayf("note: field count in this FL: %i\n", fieldCount);
		#endif
	}
	FieldList( grassField* field ) 
	{
		el.Grow(1) = field;
	}
	DECLARE_PLACE(FieldList);
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s)
	{
		pgBase::DeclareStruct(s);

		SSTRUCT_BEGIN_BASE(FieldList, pgBase)
			SSTRUCT_FIELD( FieldList, el)
			SSTRUCT_FIELD( FieldList, m_ID)
			SSTRUCT_FIELD( FieldList, m_Type)
		SSTRUCT_END( FieldList )
	}
#endif
	void SetType( const char* str ) { m_Type = str; }
	void SetType( const atString& str ) { m_Type = str; }
	const atString& GetType() const { return m_Type; }
	void SetID( u32 Id ) { m_ID = Id ; }
	u32 GetID() const { return m_ID; }

	int GetCount() const { return el.GetCount(); }
	const grassField& operator[]( int i )	const { return *el[i]; }


	void DebugDraw()
	{
		for ( int i = 0; i < GetCount(); i++ )
		{
			el[i]->DebugDraw();
		}
	}

	void GetVisibleFields( const spdShaft& shaft, std::pair<float, grassField*>* sortList, int& renderCount, float startFade, float finishFade  )
	{
		float recip =  1.0f  /( finishFade - startFade);

		for ( int i = 0; i < GetCount(); i++ )
		{
			float outZ;
			float endZ;
			if ( el[i]->IsVisible( &shaft,1, outZ, endZ  ) )
			{
				// calculate lod
				float lod =  ( outZ - startFade ) * recip ;
				if ( lod < 1.0f )
				{
					// add to list
					sortList[ renderCount++ ] = std::make_pair( lod, el[i]  );
				}
			}
		}
	}




	////////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		Just like the other GetVisibleFields method, but this one only returns
	//	a list of fields that contain the patch type asked for by name.
	//
	void GetVisibleFields(const spdShaft& shaft, std::pair<float, grassField*>* sortList, int& renderCount, float startFade, float finishFade, const char *patchType)//const char *patchName)
	{
		u32 hashKey = atStringHash(patchType);
		GetVisibleFields(shaft, sortList, renderCount, startFade, finishFade, hashKey);
	}

	////////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		Just like the other GetVisibleFields method, but this one only returns
	//	a list of fields that are junctioned to a certain patch type.  Uses a hash key
	//
	void GetVisibleFields(const spdShaft& shaft, std::pair<float, grassField*>* sortList, int& renderCount, float startFade, float finishFade, u32 PatchHashKey)
	{
		float recip =  1.0f  /( finishFade - startFade);

		for ( int i = 0; i < GetCount(); i++ )
		{
			float outZ;
			float endZ;

			if ((el[i]->GetHash() == PatchHashKey) && (el[i]->IsVisible( &shaft, 1,outZ, endZ)))
			{
				// calculate lod
				float lod =  ( outZ - startFade ) * recip ;
				if ( lod < 1.0f )
				{
					// add to list
					sortList[ renderCount++ ] = std::make_pair( lod, el[i]  );
				}
			}
		}
	}

	// Returns a list of all patch names used by this fieldlist
	void GetAllPatchNames(atString* patchList, int& patchCount)
	{
		for (int i = 0; i < GetCount(); i++)
		{
			if (el[i])
			{
				patchList[patchCount++] = el[i]->GetType();
			}
		}
	}

	bool Intersects( const spdSphere& area)
	{
		for (int i = 0; i < GetCount(); i++)
		{
			spdAABB aabb;
			Vec3V vmin =el[i]->GetMin();
			Vec3V vmax = el[i]->GetMax();

			aabb.Set(  vmin,  vmax);
			if ( aabb.IntersectsSphere( area ) )
			{
				return true;
			}
		}
		return false;
	}
	// Removes all grass from this region
	int ClearAreaOfGrass(const spdSphere& area);
};
	
void FieldGetVisible( GrassRefList& el,
					 const spdShaft* shaft, int amtShafts,
					 std::pair<float, grassField*>* sortList, 
					 int& renderCount, float startFade, float finishFade , int maxRenderCount );




// C-style functions
FieldList*  CreateFieldList( atPtr<ParticleList> pl, int numPerBatch );
FieldList*  CreateResourceFieldList( atPtr<ParticleList> pl, int numPerBatch, int requiredFields );
int CountNumFieldsRequired( atPtr<ParticleList> pl, int numPerBatch );
void AppendResourceFieldList( FieldList* fieldlist, atPtr<ParticleList> pl, int numPerBatch );
void  LoadResourcedFieldList( atArray< datOwner<grassField> >& fields, const char* path );


} // namespace rage

#endif // GRASS_FIELD_H
