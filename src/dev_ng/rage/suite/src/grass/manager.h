// 
// grass/manager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRASS_MANAGER_H
#define GRASS_MANAGER_H

#include "atl/array.h"
#include "atl/singleton.h"
#include "data/base.h"
#include "data/resource.h"
#include "grcore/effect_typedefs.h"
#include "grcore/vertexdecl.h"
#include "math/random.h"
#include "grass/field.h"
#include "grass/patch.h"
#include "atl/string.h"
#include "atl/map.h"
#include "system/task.h"

namespace rage
{
class spdShaft;
class grassField;
class grassPatch;
class grcImage;
class grassExtraBladeData;


// This base class allows game code to run custom code in between field draws.
// This is useful, for example, if your game was to re-assign point lights for different fields based on their location.
class renderInterceptor
{
public:
	virtual void Process(Vector3* location) = 0;
	virtual ~renderInterceptor() {}
};



class grassManager : public datBase 
{ 
public:

	grassManager();
	~grassManager();

	static const int RORC_VERSION = 1;

	grassManager(datResource &rsc);
	DECLARE_PLACE(grassManager);

	int Release() const {delete this; return 0;}	// Not true reference counting yet!
	// pgStreamable end --------------------------------------------------------



	int GetFieldCount() const;
	FieldList* GetFieldList( u32 ID ) const;
	void DestroyFields( u32 ID );
	void ForgetField( u32 ID );			// Dangerous function that should be used with care.
	void DestroyPatches();

	void Update();

	void DebugDraw();
	void Draw();
	void Draw( const spdShaft& shaft ,int passIndex = 0   );
	void DrawIntoScreenShadowMap( const spdShaft& shaft );
	void DrawIntoShadowCollector( );
	void DrawAlphaZPass( const spdShaft& shaft );
	bool ReadyToDraw() {return (m_StaggeredLoader == 0);}

	
	void SetGrassFade( float start, float end )
	{
		FastAssert( start < end && start > 0.0f );
		m_startFadeOut = start;
		m_finishFadeOut = end;
	}

	void SetExtraDataGenerator( grassExtraBladeData* /*extra*/ )	
	{
	/*	FastAssert( m_extra == 0 );
		m_extra = extra;

		for ( int i = 0; i < m_Fields.GetCount(); i++ )
		{
			if ( m_Fields[i] )
			{
				m_Fields[i]->SetExtraDataGenerator( m_extra );
			}
		}*/
	}

	void  Add( grassField* field, u32 streamingId  )	{ Add( rage_new FieldList( field ), streamingId ); }
	void  Add(  FieldList* fields, u32 streamingId  );

// Global influencers
	void static SetTime(float time) {m_time = time;}
	void static SetWind( const Vector3& windDirection, float windStrength);
	void static SetFades(const float startFadeOut, const float finishFadeOut, const float fadePower);
	float static GetTime() { return m_time; }
	void static GetWind(Vector3& windDirection, float& windStrength) { windDirection = m_windDirection; windStrength = m_windStrength; }
	void static SetCollectorFades(const float startFadeOut, const float finishFadeOut, const float fadePower);
	void static GetFades(float& startFadeOut, float& finishFadeOut, float& fadePower) {startFadeOut = m_gstartFadeOut; finishFadeOut = m_gfinishFadeOut; fadePower = m_gfadePower;}
	float static GetWindStrenth();

	void SetPatch( const char* name, grassPatchBase* patch );
	void SetPatch( const char* textureName, int NoTextureFrames  );

	template<class T>
	atPtr<T> GetPatch( const atString& name )	
	{	
		grassPatchBase* base = m_Patches[name];
		if (!base)	{ return atPtr<T>(); }
		return dynamic_cast<T*>(base); 	// works as it's an embedded reference counter
	}

	void Save( const char* fName  )
	{
		objOutStream out( fName, "drawers" );

		PatchTable::Iterator itor = m_Patches.CreateIterator();
		for ( itor.Start(); !itor.AtEnd(); itor.Next() )
		{
			out << itor.GetData();
		}
	}
	void Load( const char* fName  )
	{
		objInStream in( fName, "drawers" );

		if ( !in() )
		{
			Errorf("Grass Patch File %s could not be found so grass will not be able to draw", fName );
			return;
		}
		PatchTable::Iterator itor = m_Patches.CreateIterator();
		for ( itor.Start(); !itor.AtEnd(); itor.Next() )
		{
			in >> itor.GetData();
		}

		LoadTextures();  // now get the assets it requires
	}
	void LoadTextures()
	{
		PatchTable::Iterator itor = m_Patches.CreateIterator();
		for ( itor.Start(); !itor.AtEnd(); itor.Next() )
		{
			itor.GetData()->LoadTextures();
		}
	}

	// If resourcing, whittle away redundant memory such as the carbon copy list of patches.
	void SlimForResourcing();
	void SetStreamID( u32 id ) { m_CurrentStreamId = id; }
	u32 GetStreamID() const { return m_CurrentStreamId; }
	void SetRenderInterceptor(renderInterceptor*);

#if __BANK
	virtual void AddWidgets( bkBank& bank )
	{
		bank.PushGroup("Grass Renderer" );
		//bank.AddSlider("Fade Out Start", &m_gstartFadeOut, 0.0f, 1000.0f, 0.1f);
		//bank.AddSlider("Fade Out Range", &m_gfinishFadeOut, 0.0f, 1000.0f, 0.1f);
		//bank.AddSlider("Fade Out Curve", &m_gfadePower, 0.0f, 16.0f, 0.1f);

		bank.AddToggle("Sort Grass Patches", &m_sort );
		bank.AddToggle("Use Cylinder Grass", &m_useCylinderGrass);
		bank.AddToggle("Draw Bounds", &m_drawBounds );
		m_lodSettings.AddWidgets( bank);

		bank.PopGroup();
		bank.PushGroup("Grass Drawers");

		bank.AddButton("Reload",datCallback(MFA(grassManager::LoadTextures ), this));

		PatchTable::Iterator itor = m_Patches.CreateIterator();
		for ( itor.Start(); !itor.AtEnd(); itor.Next() )
		{
			itor.GetData()->AddWidgets( bank );	
		}
		bank.PopGroup();
	}
#endif

	// Tell the manager where grass textures live
	void SetTexturePath(const char* filepath);
	const char* GetTexturePath();

	// Script
	int ClearAreaOfGrass(const spdSphere &area);

	grassLod&  GetLod() { return m_lodSettings; }

	bool IsInGrass( const spdSphere& area );
	
	void CalcVisibleList( const grcViewport& view );
	void CalcVisibleList( const spdShaft* shafts, int amtShafts );
	void DrawVisList(int passIndex = 0 );
protected:
	void sortOnType();
	void DecReferenceCounts(FieldList* field);	// Decs the reference count for all patches in this outbound field.  Add() incs the ref counts

	//void sortOnType();

	static float m_time;
	static Vector3 m_windDirection;
	static float m_windStrength;
	static float m_gstartFadeOut;
	static float m_gfinishFadeOut;
	static float m_gfadePower;
	static float m_gstartFadeOut2;		// For the shadow collector pass
	static float m_gfinishFadeOut2;
	static float m_gfadePower2;


	atArray< datOwner<FieldList> >						m_Fields;
	typedef atMap<atString, atPtr<grassPatchBase> >		PatchTable;
	PatchTable								m_Patches;
	atArray< sysTaskHandle >				m_TaskHandles;

	float					m_startFadeOut;		// Fade controls for fading in and out grass
	float					m_finishFadeOut;	// Fade controls for fading in and out grass.
	float					m_fadePower;
	bool					m_cameraFacing;
	bool					m_sort;
	grassExtraBladeData*	m_extra;
	u32						m_StaggeredLoader;

	bool					m_drawBounds;
	u32						m_CurrentStreamId;
	bool					m_useCylinderGrass;

	renderInterceptor*		m_renderInterceptor;
	grassLod				m_lodSettings;

	const char*					m_texturePath;


	static const int	MaxNumGrassFields = 1024;
	static const int	MaxNumGrassPatches = 128;


	typedef std::pair<float, grassField*>	SortPair;
	typedef std::pair<int, grassPatchBase*>	PatchRange;
	atFixedArray<SortPair,MaxNumGrassFields>				m_grassDrawList;
	atFixedArray<PatchRange,MaxNumGrassPatches> 			m_patchIndexList;

	void ClearData();
	void WaitForTasks();
};

typedef atSingleton<grassManager> grassManagerSingleton;

#define GRASS (grassManagerSingleton::InstanceRef())
#define GRASSLOADED (grassManagerSingleton::IsInstantiated())

} // namespace rage

#endif // GRASS_MANAGER_H
