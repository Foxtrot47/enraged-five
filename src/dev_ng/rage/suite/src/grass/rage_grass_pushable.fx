#define NO_SKINNING
#include "../../../base/src/shaderlib/rage_common.fxh"
#include "../../../base/src/shaderlib/rage_grass.fxh"

BeginSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
    string UIName = "Diffuse Texture Grass";
ContinueSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
		AddressU = Wrap;
		AddressV = Clamp;
		AddressW = Clamp;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR; 
		LOD_BIAS = 0;
EndSampler;

struct grassVertexOut
{
	float4 pos				: DX_POS;
	float3 uv				: TEXCOORD0;
	float3 color			: COLOR0;
};

// Control variables
float3 GrassWindDirection;
float GrassWindStrength;
float GrassTimer;


float4	PersonPosition;


#if __XENON
float3  GetPersonPushForce( float3 vWorldPos, float4 personPosition, float isGrassTop, float maxExtension )
{
	float3 delta = vWorldPos.xyz - personPosition.xyz;
	float dist = max( personPosition.w - length( delta ), 0 );
	// reduce by strength 
	dist *=( (dist * isGrassTop) / personPosition.w);
	dist = min( dist * isGrassTop, maxExtension );		// clamp the distance to a maximum
	
	return delta * dist * GrassWindDirection  + (float3(1.0,1.0,1.0) -GrassWindDirection) * -dist ;  // only do horizontally
}

#pragma dcl position color0		// Yet another lie

grassVertexOut VS_Grass( int i : INDEX, bool isCheap )
{
	grassVertexOut OUT=(grassVertexOut)0;
	
	float4 vWorldPos;
	float2 uvs;
	float thinFade;
	float4 col;
	float scale;
	
	vWorldPos.xyz = GrassDecodeClippedVert( !isCheap, i, uvs, thinFade, col, scale);
	vWorldPos.w = 1.0f;
	
	scale = 2.0f;

	float isGrassTop = uvs.y;

	// Animate grass
	float t = GrassTimer * 0.4f;
	float2 bias = t + vWorldPos.xz / 2.0f;
	float4 wave;
	sincos(bias.x * 0.5f, wave.x, wave.y);
	sincos(bias.x * 4.3f, wave.z, wave.w);
	float2 offset = (wave.xy + 0.1 * wave.wz) * isGrassTop;
	offset *= GrassWindStrength;
	vWorldPos.xyz += offset.xyy *  GrassWindDirection; 

	// if person is near push the grass out of the way  ( could do four at a time for cars? )
	vWorldPos.xyz += GetPersonPushForce( vWorldPos.xyz, PersonPosition, isGrassTop, scale );
			
	OUT.uv.xy = uvs.xy;
	OUT.pos = (float4)mul(vWorldPos, gWorldViewProj);
	
	OUT.uv.z  = thinFade;
	
	OUT.color = col;
	
	return OUT;
}
grassVertexOut VS_GrassHigh( int i : INDEX ) { return VS_Grass(i, false ); }
grassVertexOut VS_GrassLow( int i : INDEX ) { return VS_Grass(i, true ); }
#else
grassVertexOut VS_Grass(grassVertexIn IN)
{
	grassVertexOut OUT=(grassVertexOut)0;

	OUT.color = IN.color;

#if __PS3
	OUT.pos = mul(float4(IN.center.xyz, 1), gWorldViewProj);
	OUT.uv.xy = IN.uv;
	OUT.uv.z = 1.0f;
	return OUT;
#endif // __PS3

	float2 offset = (IN.uv.xy * IN.color.w) * 2.0f - 1.0f;
	IN.center.xyz = IN.center.xyz * GlobalScale.xyz + GlobalBias.xyz;

	IN.center.xyz += offset.x * gViewInverse[ 0 ].xyz + -offset.y * gViewInverse[ 1 ].xyz;

	// Rotate to face the camera
	float4 vWorldPos = float4( IN.center.xyz, 1.0f); 
		
	OUT.uv.xy = IN.uv.xy;
	OUT.pos = mul(vWorldPos, gWorldViewProj);
	
	float3 cameraDistance =  gViewInverse[3].xyz - vWorldPos.xyz;
	OUT.uv.z = 1.0f - saturate (  length(cameraDistance) * g_FadeScale.x - g_FadeScale.y );
	
	return OUT;
}

grassVertexOut VS_GrassHigh( grassVertexIn IN) 
 { 
	return VS_Grass(IN ); 
 }
grassVertexOut VS_GrassLow( grassVertexIn IN) 
{ 
	return VS_Grass(IN); 
}

#endif

float4 PS_Grass(grassVertexOut IN) : COLOR0
{
	// Lookup the diffuse color
	float4 diffuse = tex2D(DiffuseSampler, IN.uv.xy) ;
	
	float alpha = Grass_ComputeScaledAlpha(diffuse.w , DiffuseSampler, IN.uv.xy) ;

	// Compute the final color
	return float4( diffuse.xyz, diffuse.w  * 2.0* IN.uv.z);
}

technique draw
{
	pass p0
	{
		ZEnable = true;
		ZWriteEnable = true;
		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaBlendEnable = false;	// RaY
		CullMode = None;
#if __WIN32PC
		AlphaRef = 64;
#else
		AlphaRef = 0;
#endif // __WIN32PC
		VertexShader = compile VERTEXSHADER VS_GrassHigh();
		PixelShader  = compile PIXELSHADER  PS_Grass();
	}
	pass p1
	{
		ZEnable = true;
		ZWriteEnable = true;
		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaBlendEnable = false;	// RaY
		CullMode = None;
#if __WIN32PC
		AlphaRef = 64;
#else
		AlphaRef = 0;
#endif // __WIN32PC
		VertexShader = compile VERTEXSHADER VS_GrassLow();
		PixelShader  = compile PIXELSHADER  PS_Grass();
	}
}
