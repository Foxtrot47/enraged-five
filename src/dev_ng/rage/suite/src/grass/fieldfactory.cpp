// 
// grass/fieldfactory.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "grass/manager.h"
#include "grass/field.h"
#include "grmodel/shader.h"
#include "grmodel/modelfactory.h"
#include "file/asset.h"
#include "bank/bkmgr.h"
#include "grcore/image.h"
#include "system/memory.h"
#include "parser/tree.h"
#include "parser/treenode.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "grprofile/pix.h"
#include <algorithm>
#include "system/performancetimer.h"
#include "grass/fieldfactory.h"
#include "vector/vector4.h"
#include "vector/vector2.h"

namespace rage
{
	namespace fieldFactory
	{

void CreateSubdividedField( FieldList&  fields, int numDivisions,
										 const char* colorMapName, const char* patchTextureName,
										 const Vector4& extents, float gridRes, 
										 const Vector2& cardSize, const Vector2& cardSizeVariance, 
										 int widthTextureVariantCount, int heightTextureVariantCount,
										 Matrix44& colourProjectionMtx ,
										 const char* normalMapName,
										 const char* texChannelMap
										 )
{
	PlacementFieldParams	params;
	params.m_colorMapName = colorMapName;
	params.m_patchTextureName = patchTextureName;
	params.m_extents = extents;
	params.m_gridRes = gridRes;
	params.m_cardSize = cardSize;
	params.m_cardSizeVariance = cardSizeVariance;
	params.m_widthTextureVariantCount = widthTextureVariantCount;
	params.m_heightTextureVariantCount = heightTextureVariantCount;
	params.m_normalMapName = normalMapName;
	params.m_colorProjectionMtx = colourProjectionMtx;
	
	params.SetColorPlacementMap( colorMapName )
		.SetTextureIndexMap( texChannelMap );

	
	CreateSubdividedField( fields, numDivisions, params );

}
int PlacementFieldParams::Read( const char* fieldParamFile )
{
	parTree* pTree = PARSER.LoadTree(fieldParamFile, "");
	if( !pTree )
		return 0;

	parTreeNode *pRootNode = pTree->GetRoot();
	if(strcmp(pRootNode->GetElement().GetName(), "grass_field_params") != 0)
		return 0;

	parTreeNode *pNode;

	pNode = pRootNode->FindChildWithName("ColorMap");
	m_colorMapName = pNode->GetData();

	pNode = pRootNode->FindChildWithName("TexIndexMap");
	if ( pNode )
	{
		m_colorTexMapName2 = pNode->GetData();
	}

	pNode = pRootNode->FindChildWithName("PatchTexture");
	m_patchTextureName = pNode->GetData();

	pNode = pRootNode->FindChildWithName("Extents");
	pNode->GetElement().FindAttribute("x")->ConvertToFloat();
	pNode->GetElement().FindAttribute("y")->ConvertToFloat();
	pNode->GetElement().FindAttribute("z")->ConvertToFloat();
	pNode->GetElement().FindAttribute("w")->ConvertToFloat();
	m_extents.x = pNode->GetElement().FindAttribute("x")->GetFloatValue();
	m_extents.y = m_extents.x + pNode->GetElement().FindAttribute("y")->GetFloatValue();
	m_extents.z = pNode->GetElement().FindAttribute("z")->GetFloatValue();
	m_extents.w = m_extents.z + pNode->GetElement().FindAttribute("w")->GetFloatValue();

	pNode = pRootNode->FindChildWithName("GridResolution");
	pNode->GetElement().FindAttribute("value")->ConvertToFloat();
	m_gridRes = pNode->GetElement().FindAttribute("value")->GetFloatValue();

	pNode = pRootNode->FindChildWithName("CardSize");
	pNode->GetElement().FindAttribute("x")->ConvertToFloat();
	pNode->GetElement().FindAttribute("y")->ConvertToFloat();

	m_cardSize.x = pNode->GetElement().FindAttribute("x")->GetFloatValue();
	m_cardSize.y = pNode->GetElement().FindAttribute("y")->GetFloatValue();

	pNode = pRootNode->FindChildWithName("CardSizeVariance");
	pNode->GetElement().FindAttribute("x")->ConvertToFloat();
	pNode->GetElement().FindAttribute("y")->ConvertToFloat();
	Vector2 cardSizeVariance;
	m_cardSizeVariance.x = pNode->GetElement().FindAttribute("x")->GetFloatValue();
	m_cardSizeVariance.y = pNode->GetElement().FindAttribute("y")->GetFloatValue();

	pNode = pRootNode->FindChildWithName("HorizontalTextureVariantCount");
	pNode->GetElement().FindAttribute("value")->ConvertToInt();
	m_widthTextureVariantCount = pNode->GetElement().FindAttribute("value")->GetIntValue();

	pNode = pRootNode->FindChildWithName("VerticalTextureVariantCount");
	pNode->GetElement().FindAttribute("value")->ConvertToInt();
	m_heightTextureVariantCount = pNode->GetElement().FindAttribute("value")->GetIntValue();

	pNode = pRootNode->FindChildWithName("SubImage");
	if (pNode)
	{
		pNode->GetElement().FindAttribute("x")->ConvertToFloat();
		pNode->GetElement().FindAttribute("y")->ConvertToFloat();
		pNode->GetElement().FindAttribute("z")->ConvertToFloat();
		pNode->GetElement().FindAttribute("w")->ConvertToFloat();
		m_subImage.x = pNode->GetElement().FindAttribute("x")->GetFloatValue();
		m_subImage.y = pNode->GetElement().FindAttribute("y")->GetFloatValue();
		m_subImage.z = pNode->GetElement().FindAttribute("z")->GetFloatValue();
		m_subImage.w = pNode->GetElement().FindAttribute("w")->GetFloatValue();
	}
	else
	{
		m_subImage.Set(0.0f, 1.0f, 0.0f, 1.0f);
	}

	Vector4 colorProjectionA;
	Vector4 colorProjectionB;

	m_colorProjectionMtx.Identity();

	pNode = pRootNode->FindChildWithName("ColorProjectionA");
	if ( pNode )
	{
		pNode->GetElement().FindAttribute("x")->ConvertToFloat();
		pNode->GetElement().FindAttribute("y")->ConvertToFloat();
		pNode->GetElement().FindAttribute("z")->ConvertToFloat();
		pNode->GetElement().FindAttribute("w")->ConvertToFloat();

		m_colorProjectionMtx.a.x = pNode->GetElement().FindAttribute("x")->GetFloatValue();
		m_colorProjectionMtx.a.y = pNode->GetElement().FindAttribute("y")->GetFloatValue();
		m_colorProjectionMtx.a.z = pNode->GetElement().FindAttribute("z")->GetFloatValue();
		m_colorProjectionMtx.a.w = pNode->GetElement().FindAttribute("w")->GetFloatValue();
	}
	else
	{
		m_colorProjectionMtx.a.x = 0.000325567f;
		m_colorProjectionMtx.a.y = 0.0f;
		m_colorProjectionMtx.a.z = 0.0f;
		m_colorProjectionMtx.a.w = 0.5f;
	}

	pNode = pRootNode->FindChildWithName("ColorProjectionB");
	if ( pNode )
	{
		pNode->GetElement().FindAttribute("x")->ConvertToFloat();
		pNode->GetElement().FindAttribute("y")->ConvertToFloat();
		pNode->GetElement().FindAttribute("z")->ConvertToFloat();
		pNode->GetElement().FindAttribute("w")->ConvertToFloat();

		m_colorProjectionMtx.b.x = pNode->GetElement().FindAttribute("x")->GetFloatValue();
		m_colorProjectionMtx.b.y = pNode->GetElement().FindAttribute("y")->GetFloatValue();
		m_colorProjectionMtx.b.z = pNode->GetElement().FindAttribute("z")->GetFloatValue();
		m_colorProjectionMtx.b.w = pNode->GetElement().FindAttribute("w")->GetFloatValue();
	}
	else
	{
		m_colorProjectionMtx.b.x = 0.0f;
		m_colorProjectionMtx.b.y = -2.18557e-08f;
		m_colorProjectionMtx.b.z = -0.000546984f;
		m_colorProjectionMtx.b.w = 0.321029f;
	}

	delete pTree;
	return 1;
}
u32 CreateField(	FieldList&  fields,  grcImage* fieldColorMap, grcImage* fieldColorMap2, const char *UNUSED_PARAM(colourMapName), const char* UNUSED_PARAM(patchTextureName),
							  const Vector4& extents, float gridRes, 
							  const Vector2& cardSize, const Vector2& cardSizeVariance, 
							  int widthTextureVariantCount, int heightTextureVariantCount,
							  const Matrix44& UNUSED_PARAM(colourProjectionMtx),
							  const Vector4& subImage,
							  const char* UNUSED_PARAM(normalMapName),
							  const Vector4& completeExtents,
							  bool setupDraw )
{
	// Create the field
	grassField* field = rage_new grassField();

	Printf(".");

	int patchesCreated = field->Create( fieldColorMap, fieldColorMap2, extents, completeExtents , gridRes, subImage, cardSize, widthTextureVariantCount);
	bool required = (patchesCreated != 0);

	if ( required   )
	{
		fields.el.Grow() = field;

		if ( setupDraw )
		{
			field->SetupDrawBuffers( cardSize,  cardSizeVariance, widthTextureVariantCount, heightTextureVariantCount  );
		}
	}
	else
	{
		delete field;		// empty field so we don't want to waste time with it.	
	}

	return patchesCreated;
}
void  CreateSubdividedField( FieldList& fields, int numDivisions, const char* fieldParamFile )
{
	PlacementFieldParams params;
	params.Read( fieldParamFile );
	CreateSubdividedField( fields, numDivisions, params );
}

void CreateSubdividedField( FieldList& fields, int divisions, PlacementFieldParams& p, bool setupDraw)
{
	Assert( divisions > 0 && divisions < 128 );
	float divisionsf = (float) divisions;
	Vector4 extents = p.m_extents;
	float fWidthDiff = ( extents.y - extents.x ) / divisionsf;
	float fDepthDiff = ( extents.w - extents.z) / divisionsf;
	u32 patchesCreated = 0;

	grcImage*	channelMap = grcImage::Load( p.m_colorMapName  );
	grcImage*	texChannelMap2 = p.m_colorTexMapName2.GetLength() ? grcImage::Load( p.m_colorTexMapName2 ) : 0;

	if (!channelMap)
		return;

	Printf("Creating a field of grass");

	for ( float xf = extents.x ; xf < extents.y; xf += fWidthDiff )
	{
		for ( float zf = extents.z; zf < extents.w; zf += fDepthDiff )
		{
			Vector4 subFieldExtents( xf, xf + fWidthDiff, zf, zf + fDepthDiff );

			// now create a field with these extents and offset
			patchesCreated += CreateField( fields,
				channelMap,
				texChannelMap2,
				(const char*)p.m_colorMapName,
				(const char*)p.m_patchTextureName,
				subFieldExtents,
				p.m_gridRes,
				p.m_cardSize,
				p.m_cardSizeVariance, 
				p.m_widthTextureVariantCount, 
				p.m_heightTextureVariantCount,
				p.m_colorProjectionMtx,
				p.m_subImage,
				p.m_normalMapName,
				p.m_extents,
				setupDraw );
		}
	}

	Printf("Created %u patches of grass!\n", patchesCreated);

	channelMap->Release();
	if ( texChannelMap2)  texChannelMap2->Release();
}




#if !__FINAL
void SaveFieldParams(	const char* outFilename,
								   const char* colorMapName, const char* patchTextureName, 
								   const Vector4& extents, float gridRes, 
								   const Vector2& cardSize, const Vector2& cardSizeVariance, 
								   int widthTextureVariantCount, int heightTextureVariantCount )
{
	parTree* pTree = rage_new parTree();
	parTreeNode* pRootNode = pTree->CreateRoot();
	pTree->SetRoot(pRootNode);

	parElement& rootElm = pRootNode->GetElement();
	rootElm.SetName("grass_field_params");

	parTreeNode* pNode = parTreeNode::CreateStdLeaf("ColorMap", colorMapName);
	pNode->AppendAsChildOf(pRootNode);
	pNode = parTreeNode::CreateStdLeaf("PatchTexture", patchTextureName);
	pNode->AppendAsChildOf(pRootNode);
	pNode = parTreeNode::CreateStdLeaf("Extents", extents);
	pNode->AppendAsChildOf(pRootNode);
	pNode = parTreeNode::CreateStdLeaf("GridResolution", gridRes);
	pNode->AppendAsChildOf(pRootNode);
	pNode = parTreeNode::CreateStdLeaf("CardSize", cardSize);
	pNode->AppendAsChildOf(pRootNode);
	pNode = parTreeNode::CreateStdLeaf("CardSizeVariance", cardSizeVariance);
	pNode->AppendAsChildOf(pRootNode);
	pNode = parTreeNode::CreateStdLeaf("HorizontalTextureVariantCount", widthTextureVariantCount);
	pNode->AppendAsChildOf(pRootNode);
	pNode = parTreeNode::CreateStdLeaf("VerticalTextureVariantCount", heightTextureVariantCount);
	pNode->AppendAsChildOf(pRootNode);



	PARSER.SaveTree(outFilename, "" , pTree, parManager::XML);

	delete pTree;

}
#endif

}
}


