// 
// grass/rage_grasslow.frag
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "grass/rage_grasscommon.h"
#include "grcore/grcorespu.h"

SPUFRAG_DECL(bool, rage_grasslow, grcInstanceRenderer::InstanceContext*);
SPUFRAG_IMPL(bool, rage_grasslow, grcInstanceRenderer::InstanceContext* context)
{
 	VS_Grass_Functor<false,false,false> functor(context);
 	return functor();
}
