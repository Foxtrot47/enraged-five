// 
// grass/staticparticles.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//
#ifndef STATIC_PARTICLES_H
#define STATIC_PARTICLES_H

#include "parser/manager.h"
#include "atl/array.h"
#include "atl/ptr.h"
#include "data/base.h"
#include "vector/vector4.h"
#include "vector/vector3.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include <algorithm>
#include "system/platform.h"

namespace rage
{


namespace gtl
{
	struct SortOnAxias
	{
		int axias;
		SortOnAxias( int ax ) : axias( ax ) {}

		template<class OBJECT>
		bool operator()( const OBJECT& a, const OBJECT& b )
		{
			return a.GetPosition()[axias] < b.GetPosition()[axias];
		}
	};

	struct OnLeftSide
	{
		float centre;
		int  ax;
		OnLeftSide( int axias, float c ) : centre( c), ax( axias ) {}

		template<class OBJECT>
		bool operator()( const OBJECT& a )
		{
			return a.GetPosition()[ax] < centre;
		}
	};

	template<class T>
	void CreateBoundingBox( T start, T end, Vector3& min, Vector3& max )
	{
		max  = Vector3( -FLT_MAX, -FLT_MAX, -FLT_MAX );
		min = Vector3( FLT_MAX, FLT_MAX, FLT_MAX );

		for ( T ptr = start; ptr != end; ++ptr )
		{
			const Vector3& pos = ptr->GetPosition();
			float scale =ptr->GetRadius();
			// update the bounds
			max.Max( max,  pos + Vector3( scale, scale, scale) );   // could create one max / main
			min.Min( min,  pos - Vector3( scale, scale, scale) );
		}
	}
	inline int GetMaximumAxias( const Vector3& diff )
	{
		if ( diff.x > diff.y )
		{
			if ( diff.x > diff.z )
			{
				return 0;  //x
			}
			else
			{
				return 2;  // z
			}
		}
		else
		{
			if ( diff.y > diff.z )
			{
				return 1;  //x
			}
			else
			{
				return 2;  // z
			}
		}
	}

	struct MinCellPredicate
	{
		int	m_amt;
	public:
		MinCellPredicate( int amt ) : m_amt( amt ) {}

		template<class T>
		bool operator()( const T& start, const T& end, const Vector3& , const Vector3&   )
		{
			return ( ( end - start ) < m_amt );
		}
	};
	
	template<class T, class LeafFunctor, class Predicate>
	void KdTreeBuilder( T start, T end ,  Predicate predicate, LeafFunctor& leafFunction )
	{
		while( 1)
		{
			Vector3 min;
			Vector3 max;

			CreateBoundingBox( start, end, min, max );

			if (predicate( start, end, min, max  ))
			{
				leafFunction( start, end , min, max );
				return;
			}

			int axias = GetMaximumAxias( max - min );

			float centre =  ( min[ axias ] + max[ axias ] ) * 0.5f;

			T mid;
			mid = std::partition( start, end, OnLeftSide( axias, centre ));
			KdTreeBuilder( start, mid, predicate, leafFunction );

			start = mid;		// tail end recursion
		}
	}

};

	/*
		PURPOSE
			Fat Particle element which is read from xml file.
	*/
	class Particle : public datBase		
	{
	public:
		Vector3		Position;
		Vector3		Color;
		float		Scale;
		int			TextureFrame;

		const Vector3& GetPosition() const { return Position; }
		int				GetTextureFrame() const { return TextureFrame; }
		float			GetRadius() const { return Scale; }
		const Particle& operator=( const Particle& other ) 
		{
			Position = other.Position;
			Color = other.Color;
			Scale = other.Scale;
			TextureFrame = other.TextureFrame;
			return *this;
		}
		PAR_PARSABLE;
	};

	/*
		PURPOSE
		Simple list of static particles, with operations related to spatial data structures and culling
	*/
	class ParticleList: public atReferenceCounter
	{
	public:
		ParticleList() {}
		ParticleList( const Particle* start, const Particle* end  )
		{
			Particles.CopyFrom( start, static_cast<unsigned short>(end - start ) );
			Update();
		}

		static atPtr<ParticleList> Create( const char* filename, const char* ext = "xml" );
		void Save( const char* fileName );


		void Randomize()
		{
			std::random_shuffle( Particles.begin(), Particles.end() );
		}

		Particle* begin() { return Particles.begin(); }
		Particle* end() { return Particles.end(); }

		// calculate bounding box
		Vector3 GetMin() const { return m_min; }
		Vector3 GetMax() const { return m_max; }

		int		GetCount() const { return Particles.GetCount(); }
		bool Split( int maxSize, atPtr<ParticleList>& a );

		int GetNumberTextureFrames() const { return m_NumTexFrames; }

		template<class VERTCREATOR>
		grcVertexBuffer* CreateVertexBuffer( grcFvf& fvf, VERTCREATOR& verter ) const 
		{
			const bool useInstancing = g_sysPlatform == platform::XENON || g_sysPlatform == platform::PS3;
			grcVertexBuffer* vertexBuffer = grcVertexBuffer::Create( useInstancing ? Particles.GetCount() : Particles.GetCount() * 6, fvf, g_sysPlatform == platform::PS3 );
			grcVertexBufferEditor vertexBufferEditor(vertexBuffer);
			int idx = 0;
			for ( int i = 0; i < Particles.GetCount(); i++ )
			{
				if ( useInstancing )
				{
					verter( idx++, Particles[i], vertexBuffer );
				}
				else
				{
					// float scale;

					Vector2 tluv( 0.0f, 1.0f );
					Vector2 truv( 1.0f, 1.0f );
					Vector2 bluv( 0.0f, 0.0f );
					Vector2 bruv( 1.0f, 0.0f );

					Vector2 tl( -1.0f, 1.0f );
					Vector2 tr( 1.0f, 1.0f );
					Vector2 bl( -1.0f, -1.0f );
					Vector2 br( 1.0f, -1.0f );


					/*scale =*/ verter( idx, Particles[i], vertexBuffer );
					vertexBufferEditor.SetUV(idx,0,tluv);
					++idx;

					/*scale =*/ verter( idx, Particles[i], vertexBuffer );
					vertexBufferEditor.SetUV(idx, 0, truv);
					++idx;

					/*scale =*/ verter( idx, Particles[i], vertexBuffer );
					vertexBufferEditor.SetUV(idx, 0, bluv);
					++idx;


					/*scale =*/ verter( idx, Particles[i], vertexBuffer );
					vertexBufferEditor.SetUV(idx, 0, truv);
					++idx;

					/*scale =*/ verter( idx, Particles[i], vertexBuffer );
					vertexBufferEditor.SetUV(idx, 0, bruv);
					++idx;

					/*scale =*/ verter( idx, Particles[i], vertexBuffer );
					vertexBufferEditor.SetUV(idx, 0, bluv);
					++idx;
				}

			}
	
			return vertexBuffer;
		}
		virtual ~ParticleList() {}

		template<class LeafFunctor>
		void CreateKdTree( int maxSize, LeafFunctor& leaf )
		{
			gtl::KdTreeBuilder( Particles.begin(), Particles.end(),  gtl::MinCellPredicate( maxSize ) , leaf );
		}
		
		void Clear()
		{
			Particles.Reset();
		}

		ConstString&	GetType()	{ return Type; }
		void SetType( const char* str ) { Type = str; }

	private:
		atArray<Particle, 0, unsigned int>		Particles;
		ConstString				Type;

		Vector3					m_min;
		Vector3					m_max;
		int						m_NumTexFrames;

		void CreateBoundingBox();
		int GetMaxAxias() const;

		void Update()		{			CreateBoundingBox();		}


		PAR_PARSABLE;

	};

};

#endif
