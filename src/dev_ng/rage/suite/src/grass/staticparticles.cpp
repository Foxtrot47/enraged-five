// 
// grass/staticparticles.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#include "parser/manager.h"
#include "staticparticles.h"


using namespace rage;

#include "staticparticles_parser.h"

namespace rage
{



void ParticleList::CreateBoundingBox()
{
	gtl::CreateBoundingBox( Particles.begin(), Particles.end(), m_min, m_max );

	m_NumTexFrames = 1;

	for( int i = 0; i < Particles.GetCount(); i++ )
	{
		// update the bounds
		m_NumTexFrames = Max( Particles[i].GetTextureFrame(), m_NumTexFrames );
	}
}
int ParticleList::GetMaxAxias()  const
{
	Vector3 diff = m_max - m_min;
	return gtl::GetMaximumAxias( m_max - m_min );
}




bool ParticleList::Split( int maxSize, atPtr<ParticleList>& a )
{
	if ( Particles.GetCount() <= maxSize )
	{
		return false;
	}
	int axias = GetMaxAxias();
	
	if ( !a->Particles.GetCapacity() )	a->Particles.Reserve( Particles.GetCount() );
	a->Particles.Resize(0);
	
	//std::sort( Particles.begin(), Particles.end(), SortOnAxias( i ) );
	float centre = ( m_max[axias] + m_min[axias] ) * 0.5f;
	

	int startCount = 0;
	for (int i = 0; i < Particles.GetCount(); i++ )
	{
		if ( Particles[i].GetPosition()[axias] < centre )
		{
			a->Particles.Append() = Particles[i];
		}
		else
		{
			Particles[ startCount++ ] = Particles[i];
		}
	}
	Particles.Resize( startCount );
	Update();

	a->Update();
	return true;
}


atPtr<ParticleList> ParticleList::Create( const char* filename, const char* ext )
{
	ParticleList*		particles = rage_new ParticleList;
	if ( PARSER.LoadObject( filename , ext, *particles) )
	{
		particles->Update();
		return particles;
	}
	else
	{
		Warningf("ParticleList not found so empty one generated" );
		return atPtr<ParticleList>();
	}
	
}
void ParticleList::Save( const char* filename )
{
	PARSER.SaveObject( filename , "xml", this);
	
}
}
