<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<autoregister allInFile="true"/>

<structdef type="::rage::crPoseSample::Key">
	<int name="m_Hi"/>
	<int name="m_Lo"/>
</structdef>

<structdef type="::rage::crPoseSample">
	<string name="m_Filename" noInit="true" type="atString"/>
	<struct name="m_Key" type="::rage::crPoseSample::Key"/>
	<string name="m_clipSetId" type="atHashString"/>
	<string name="m_clipId" type="atHashString"/>
	<float init="0.0" name="m_Time"/>
</structdef>

<structdef base="::rage::crPoseSample" type="::rage::crAnimationPoseSample">
</structdef>

<structdef base="::rage::crPoseSample" type="::rage::crClipPoseSample">
</structdef>

<structdef type="::rage::crAnimWeight">
	<string name="m_BoneName" noInit="true" type="atString"/>
	<float init="1.0" name="m_Weight"/>
</structdef>

<structdef type="::rage::crPoseFile">
	<array name="m_Samples" type="atArray">
		<pointer policy="owner" type="::rage::crPoseSample"/>
	</array>
	<array name="m_Weights" type="atArray">
		<pointer policy="owner" type="::rage::crAnimWeight"/>
	</array>

	<int init="1" name="m_WindowSize"/>
	<float init="0.03333333F" name="m_SampleRate"/>
</structdef>

</ParserSchema>