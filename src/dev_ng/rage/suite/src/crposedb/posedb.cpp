#include "posedb.h"
#include "posedb_parser.h"

#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crextra/posematcher.h"

namespace rage {

#if CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS
/*static*/ atArray<atString> crPoseFile::ms_animSearchFolders;
#endif // CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS

u64 crPoseSample::GetKey() const
{
	return u64(m_Key.m_Hi)<<32 | m_Key.m_Lo;
}

crAnimationPoseSample::crAnimationPoseSample()
: m_Animation (NULL)
{
}

crAnimationPoseSample::~crAnimationPoseSample()
{
	if (m_Animation != NULL)
	{
		m_Animation->Release();
	}
}

bool crAnimationPoseSample::Load()
{
#if CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS
	const atString resolvedFilename = crPoseFile::ResolveFilenameUsingAnimSearchFolders(m_Filename);
#else // CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS
	const atString resolvedFilename = m_Filename;
#endif // CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS
	m_Animation = crAnimation::AllocateAndLoad(resolvedFilename);
	if (m_Animation) {
		return true;
	}

	Errorf("ERROR - Failed to load animation '%s'", resolvedFilename.c_str());

	return false;
}

void crAnimationPoseSample::AddSample(crPoseMatcher& m)
{
	m.AddSample(*m_Animation, GetKey(), m_Time);
}

crClipPoseSample::crClipPoseSample()
: m_Clip (NULL)
{
}

crClipPoseSample::~crClipPoseSample()
{
	if (m_Clip != NULL)
	{
		m_Clip->Release();
	}
}

bool crClipPoseSample::Load()
{
	m_Clip = crClip::AllocateAndLoad(m_Filename);
	if (m_Clip) {
		return true;
	}

	Errorf("ERROR - Failed to load clip '%s'", m_Filename.c_str());

	return false;
}

void crClipPoseSample::AddSample(crPoseMatcher& m)
{
	m.AddSample(*m_Clip, GetKey(), m_Time);
}

crPoseFile::~crPoseFile()
{
	Shutdown();
}

bool crPoseFile::Load() const
{
	bool ret = true;
	for (int i=0; i<m_Samples.GetCount(); ++i) 
	{
		crPoseSample* pSample = m_Samples[i];
		if (pSample)
		{
			bool loaded = pSample->Load();
			if (!loaded) 
			{ 
				Errorf("Failed to load animation %s\n", pSample->m_Filename.c_str());
				ret = false; 
			}

			if ( pSample->m_clipId.GetHash() != 0)
			{
				pSample->m_Key.m_Lo = pSample->m_clipId.GetHash();
			}
			if ( pSample->m_clipSetId.GetHash() != 0)
			{
				pSample->m_Key.m_Hi = pSample->m_clipSetId.GetHash();
			}
		}
	}

	return ret;
}

void crPoseFile::Shutdown()
{
	for (int i=0; i<m_Samples.GetCount(); ++i) {
		delete m_Samples[i];
	}

	m_Samples.Reset();

	for (int i=0; i<m_Weights.GetCount(); ++i) {
		delete m_Weights[i];
	}

	m_Weights.Reset();
}

#if CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS
atString crPoseFile::ResolveFilenameUsingAnimSearchFolders(const char* szFilename)
{
	if (!ASSET.IsAbsolutePath(szFilename))
	{
		char fullPath[512];

		// go through the search paths one by one, early out when file found
		for(int i = 0; i < ms_animSearchFolders.GetCount(); ++i)
		{
			ASSET.PushFolder(ms_animSearchFolders[i]);
			// expand the path and check whether file exists
			const bool fileFound = ASSET.FullReadPath(fullPath, NELEM(fullPath), szFilename, "anim");
			ASSET.PopFolder();
			if (fileFound)
			{
				return atString(fullPath);
			}
		}
	}

	return atString(szFilename);
}
#endif // CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS

} // rage