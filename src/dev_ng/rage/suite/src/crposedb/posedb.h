#ifndef CRPOSEDB_H
#define CRPOSEDB_H

#include "atl/array.h"
#include "parser/manager.h"

namespace rage
{

class crAnimation;
class crClip;
class crPoseMatcher;

#define CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS (RSG_TOOL && (GTA_VERSION > 0))

//-----------------------------------------------------------------------------
//PURPOSE: 
struct crPoseSample
{
	virtual ~crPoseSample() {}

	virtual bool Load() { return false; }

	virtual void AddSample(crPoseMatcher&) {};

	struct Key
	{
		u32 m_Hi, m_Lo;

		PAR_SIMPLE_PARSABLE;
	};

	u64 GetKey() const;

	atString m_Filename;
	Key m_Key;
	atHashString m_clipSetId;
	atHashString m_clipId;
	float m_Time;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: 
struct crAnimationPoseSample : public crPoseSample
{
	crAnimationPoseSample();
	virtual ~crAnimationPoseSample();

	virtual bool Load();

	virtual void AddSample(crPoseMatcher& m);

	crAnimation* m_Animation;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: 
struct crClipPoseSample : public crPoseSample
{
	crClipPoseSample();
	virtual ~crClipPoseSample();

	virtual bool Load();

	virtual void AddSample(crPoseMatcher& m);

	crClip* m_Clip;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: 
struct crAnimWeight
{
	atString m_BoneName;
	float m_Weight;

	PAR_SIMPLE_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: 
struct crPoseFile
{
	~crPoseFile();

	int GetSampleCount() const { return m_Samples.GetCount(); }
	crPoseSample* GetSample(int idx) const { return m_Samples[idx]; }

	bool Load() const;
	void Shutdown();

	atArray<crPoseSample*> m_Samples;
	atArray<crAnimWeight*> m_Weights;
	int m_WindowSize;
	float m_SampleRate;

#if CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS
	static atString ResolveFilenameUsingAnimSearchFolders(const char* szFilename);
	static void SetAnimSearchFolders(const atArray<atString>& animSearchFolders) { ms_animSearchFolders = animSearchFolders; }
	static atArray<atString> ms_animSearchFolders;
#endif // CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS

	PAR_SIMPLE_PARSABLE;
};

} // rage

#endif // CRPOSEDB_H