//
// crextra/posematcher.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "posematcher.h"

#include "atl/array_struct.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/weightset.h"
#include "crclip/clip.h"
#include "crmetadata/dumpoutput.h"
#include "crparameterizedmotion/pointcloud.h"
#include "crparameterizedmotion/pointcloudtransform.h"
#include "crskeleton/skeleton.h"
#include "file/asset.h"
#include "file/serialize.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crPoseMatcher::crPoseMatcher()
: m_SkeletonData(NULL)
, m_WeightSet(NULL)
, m_SampleRate(1.f/30.f)
, m_WindowSize(1)
{
}

////////////////////////////////////////////////////////////////////////////////

crPoseMatcher::crPoseMatcher(datResource& rsc)
: m_Samples(rsc, true)
{
}

////////////////////////////////////////////////////////////////////////////////

crPoseMatcher::~crPoseMatcher()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPoseMatcher::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crPoseMatcher);
	STRUCT_FIELD(m_Samples);
	STRUCT_FIELD(m_SkeletonData);
	STRUCT_FIELD(m_WeightSet);
	STRUCT_FIELD(m_SampleRate);
	STRUCT_FIELD(m_WindowSize);
	STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crPoseMatcher);

////////////////////////////////////////////////////////////////////////////////

void crPoseMatcher::Init(const crSkeletonData& skelData, crWeightSet* weightSet, int accelSize, u32 windowSize, float sampleRate)
{
	Assert(windowSize >= 1);
	Assert(sampleRate > 0.f);

	m_SkeletonData = &skelData;

	m_Signature = skelData.GetSignature();

	int numBones = skelData.GetNumBones();
	m_BoneIds.Resize(numBones);

	for (int i=0; i<numBones; ++i)
	{
		const crBoneData* boneData = skelData.GetBoneData(i);
		m_BoneIds[i] = boneData->GetBoneId();
	}

	if(weightSet)
	{
		m_WeightSet = rage_new crWeightSet(*weightSet);
	}

	m_WindowSize = windowSize;
	m_SampleRate = sampleRate;

	u32 heapSize = skelData.GetNumBones() * sizeof(f32);
	heapSize = (heapSize + SYS_TINYHEAP_STORAGE_ALIGNMENT - 1) & ~(SYS_TINYHEAP_STORAGE_ALIGNMENT - 1);
	m_WeightSetCache.Init(accelSize, heapSize);
}

////////////////////////////////////////////////////////////////////////////////

void crPoseMatcher::Shutdown()
{
	m_WeightSetCache.Shutdown();

	if(m_WeightSet)
	{
		delete m_WeightSet;
		m_WeightSet = NULL;
	}

	const int numSamples = m_Samples.GetCount();
	for(int i=0; i<numSamples; ++i)
	{
		delete m_Samples[i];
		m_Samples[i] = NULL;
	}
	m_Samples.Reset();

	m_SkeletonData = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crPoseMatcher::AddSample(const crAnimation& anim, u64 key, float time)
{
	Assert(m_SkeletonData);

	crFrame frame;
	frame.InitCreateBoneAndMoverDofs(*m_SkeletonData, false);
	frame.IdentityFromSkel(*m_SkeletonData);

	crpmPointCloud* pointCloud = rage_new crpmPointCloud;

	for(int i=m_WindowSize; i>0; --i)
	{
		anim.CompositeFrame(time - m_SampleRate * float(i-1), frame, false, false);
		pointCloud->AddFrame(frame, *m_SkeletonData, m_WeightSet);
	}

	AddPointCloud(pointCloud, key, time);
}

////////////////////////////////////////////////////////////////////////////////

void crPoseMatcher::AddSample(const crClip& clip, u64 key, float time)
{
	Assert(m_SkeletonData);

	crFrame frame;
	frame.InitCreateBoneAndMoverDofs(*m_SkeletonData, false);
	frame.IdentityFromSkel(*m_SkeletonData);

	crpmPointCloud* pointCloud = rage_new crpmPointCloud;

	for(int i=m_WindowSize; i>0; --i)
	{
		clip.Composite(frame, time - m_SampleRate * float(i-1));
		pointCloud->AddFrame(frame, *m_SkeletonData, m_WeightSet);
	}

	AddPointCloud(pointCloud, key, time);
}

////////////////////////////////////////////////////////////////////////////////

void crPoseMatcher::AddSamples(const crAnimation& anim, u64 key, float sampleInterval, float startTime, float endTime)
{
	Assert(m_SkeletonData);

	crFrame frame;
	frame.InitCreateBoneAndMoverDofs(*m_SkeletonData, false);
	frame.IdentityFromSkel(*m_SkeletonData);

	float time = startTime;
	while(time <= anim.GetDuration() && time <= endTime)
	{
		crpmPointCloud* pointCloud = rage_new crpmPointCloud;

		for(int i=m_WindowSize; i>0; --i)
		{
			anim.CompositeFrame(time - m_SampleRate * float(i-1), frame, false, false);
			pointCloud->AddFrame(frame, *m_SkeletonData, m_WeightSet);
		}

		AddPointCloud(pointCloud, key, time);

		time += sampleInterval;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crPoseMatcher::AddSamples(const crClip& clip, u64 key, float sampleInterval, float startTime, float endTime)
{
	Assert(m_SkeletonData);

	crFrame frame;
	frame.InitCreateBoneAndMoverDofs(*m_SkeletonData, false);
	frame.IdentityFromSkel(*m_SkeletonData);

	float time = startTime;
	while(time <= clip.GetDuration() && time <= endTime)
	{
		crpmPointCloud* pointCloud = rage_new crpmPointCloud;

		for(int i=m_WindowSize; i>0; --i)
		{
			clip.Composite(frame, time - m_SampleRate * float(i-1));
			pointCloud->AddFrame(frame, *m_SkeletonData, m_WeightSet);
		}

		AddPointCloud(pointCloud, key, time);

		time += sampleInterval;
	}
}

////////////////////////////////////////////////////////////////////////////////

struct WeightSetCacheCallbackData
{
	const crSkeletonData* m_MatchData;
	const atArray<u16>* m_BoneIds;
	const crWeightSet* m_WeightSet;
};

////////////////////////////////////////////////////////////////////////////////

u32 CalcWeightSetCallback(void* data, u8* buffer)
{
	WeightSetCacheCallbackData* inputs = reinterpret_cast<WeightSetCacheCallbackData*>(data);

	const crSkeletonData* matchData = inputs->m_MatchData;
	const atArray<u16>& boneIds = *inputs->m_BoneIds;
	const crWeightSet* sourceWeights = inputs->m_WeightSet;

	crWeightSet weightSet(*matchData);

	const int numBones = boneIds.GetCount();
	for(int i=0; i<numBones; ++i)
	{
		float weight = sourceWeights->GetAnimWeight(i);
		if (!(weight > 0.0F))
		{
			continue;
		}

		u16 boneId = boneIds[i];
		int boneIdx;
		if (matchData->ConvertBoneIdToIndex(boneId, boneIdx))
		{
			weightSet.SetAnimWeight(boneIdx, weight);
		}
	}

	f32* weights = reinterpret_cast<f32*>(buffer);
	u32 bufferSize = weightSet.GetNumAnimWeights()*sizeof(f32);
	sysMemCpy(weights, weightSet.GetAnimWeights(), bufferSize);
	return bufferSize;
}

////////////////////////////////////////////////////////////////////////////////

bool crPoseMatcher::FindBestMatch(const crSkeleton& skel, u64& outKey, float& outTime, Mat34V_InOut outTransform, crPoseMatcher::MatchFilter* filter)
{
	crpmPointCloud pointCloud;

	// if the skeletons don't match then translate the weightset to fit
	const crSkeletonData& skelData = skel.GetSkeletonData();
	if (skelData.GetSignature() != m_Signature)
	{
		crLock lock;
		WeightSetCacheCallbackData inputs;
		inputs.m_MatchData = &skelData;
		inputs.m_BoneIds = &m_BoneIds;
		inputs.m_WeightSet = m_WeightSet;
		m_WeightSetCache.GetEntry(skelData.GetSignature(), skelData.GetNumBones()*sizeof(f32), CalcWeightSetCallback, &inputs, lock);

		const f32* weights = reinterpret_cast<const f32*>(lock.Get());
		for(int i=0; i<m_WindowSize; ++i)
		{
			pointCloud.AddPose(skel, weights);
		}
	}
	else
	{
		for(int i=0; i<m_WindowSize; ++i)
		{
			pointCloud.AddPose(skel, m_WeightSet);
		}
	}

	return MatchPointCloud(pointCloud, outKey, outTime, outTransform, filter);
}

////////////////////////////////////////////////////////////////////////////////

bool crPoseMatcher::FindBestMatchWithHistory(int numSkels, const crSkeleton** skeletons, u64& outKey, float& outTime, Mat34V_InOut outTransform, crPoseMatcher::MatchFilter* filter)
{
	crpmPointCloud pointCloud;

	// if not enough skeletons provided, add first skeleton repeatedly to make up shortfall
	if(numSkels < m_WindowSize)
	{
		for(int i=m_WindowSize-numSkels; i>0; --i)
		{
			Assert(skeletons[0]);
			pointCloud.AddPose(*skeletons[0], m_WeightSet);
		}
	}

	// add skeletons provided, or most recent subset if surplice provided
	for(int i=Max(numSkels-m_WindowSize, 0); i<numSkels; ++i)
	{
		Assert(skeletons[i]);
		pointCloud.AddPose(*skeletons[i], m_WeightSet);
	}

	return MatchPointCloud(pointCloud, outKey, outTime, outTransform, filter);
}

////////////////////////////////////////////////////////////////////////////////

bool crPoseMatcher::Load(const char* filename)
{
	fiStream* s = ASSET.Open(filename, "pmdb");
	if (!s) {
		return false;
	}

	fiSerialize serializer (s, true, true);
	serializer << *this;

	Assert(m_WindowSize >= 1);
	Assert(m_SampleRate > 0.f);

	s->Close();

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crPoseMatcher::Save(const char* filename)
{
	fiStream* s = ASSET.Create(filename, "pmdb");
	if (!s) {
		return false;
	}

	fiSerialize serializer (s, false, true);
	serializer << *this;

	s->Close();

	return true;
}

////////////////////////////////////////////////////////////////////////////////

static int g_crPoseMatcherVersions[] =
{
	0, // DA 25-FEB-11 - initial version
};

void crPoseMatcher::Serialize(datSerialize& s)
{
	const int latest = g_crPoseMatcherVersions[0];

	int version = latest;
	s << datLabel("Version:") << version << datNewLine;

	if (version > latest)
	{
		Errorf("crPoseMatcher::Serialize - attempting to load version '%d' (only support up to '%d')", version, latest);
		return;
	}

	s << datLabel("Signature:") << m_Signature << datNewLine;

	int numSamples = m_Samples.GetCount();
	s << numSamples;

	if (s.IsRead())
	{
		m_Samples.Resize(numSamples);
	}

	for (int i=0; i<numSamples; ++i)
	{
		if (s.IsRead())
		{
			m_Samples[i] = rage_new MatchSample;
		}
		s << *m_Samples[i];
	}

	int numBones = m_BoneIds.GetCount();
	s << numBones;

	if (s.IsRead())
	{
		m_BoneIds.Resize(numBones);
	}

	for (int i=0; i<numBones; ++i)
	{
		s << m_BoneIds[i];
	}

	if (s.IsRead())
	{
		m_WeightSet = rage_new crWeightSet;
	}

	s << *m_WeightSet;

	s << m_SampleRate;
	s << m_WindowSize;

	s << m_Signature;
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crPoseMatcher::Dump(crDumpOutput& output) const
{
	output.Outputf(0, "sample rate", "%f", m_SampleRate);
	output.Outputf(0, "window size", "%d", m_WindowSize);

	const int numSamples = m_Samples.GetCount();
	output.Outputf(0, "num samples", "%d", numSamples);

	for (int i=0; i<numSamples; ++i) {
		output.Outputf(0, "sample", "%d", i);
		output.PushLevel(NULL);

		MatchSample* sample = m_Samples[i];

		output.Outputf(0, "key", "0x%08X%08X", sample->m_Key.m_Hi, sample->m_Key.m_Lo);
		output.Outputf(0, "time", "%f", sample->m_Time);

		sample->m_PointCloud->Dump(output);

		output.PopLevel();
	}

	m_WeightSet->Dump(output);
}
#endif // CR_DEV


////////////////////////////////////////////////////////////////////////////////

void crPoseMatcher::AddPointCloud(crpmPointCloud* pointCloud, u64 key, float time)
{
	Assert(pointCloud);

	m_Samples.Grow() = rage_new MatchSample;
	MatchSample& sample = *m_Samples.Top();

	sample.m_Key = key;
	sample.m_Time = time;

	sample.m_PointCloud = pointCloud;
}

////////////////////////////////////////////////////////////////////////////////

bool crPoseMatcher::MatchPointCloud(const crpmPointCloud& pointCloud, u64& outKey, float& outTime, Mat34V_InOut outTransform, crPoseMatcher::MatchFilter* filter)
{
	int bestMatchIdx = -1;
	float bestDistance = FLT_MAX;
	Mat34V bestTransformMtx;

	const int numSamples = m_Samples.GetCount();
	for(int i=0; i<numSamples; ++i)
	{
		const MatchSample& sample = *m_Samples[i];

		float weight = filter?filter->PreFilter(sample.m_Key, sample.m_Time):1.f;
		if(weight > SMALL_FLOAT)
		{
			crpmPointCloudTransform transform;

			float earlyOutDistance = (bestMatchIdx<0)?FLT_MAX:(bestDistance/weight);
			float distance = pointCloud.ComputeDistance(*sample.m_PointCloud, transform, earlyOutDistance) * weight;

#if DEBUG_POSE_MATCHER
			Printf("crPoseMatcher::MatchPointClound: [%d] dist %f weight %f key %s time %.3f (rot %.3f trans %.3f %.3f %.3f)\n", i, distance, weight, filter?filter->DebugName(sample.m_Key):"(null)", sample.m_Time, transform.GetAngle()*RtoD, transform.GetTranslation().GetXf(), transform.GetTranslation().GetYf(), transform.GetTranslation().GetZf());
#endif // DEBUG_POSE_MATCHER

			if(distance < bestDistance)
			{
				Mat34V transformMtx = transform.ToMatrix();

				if(!filter || filter->PostFilter(sample.m_Key, sample.m_Time, transformMtx, distance))
				{
					bestMatchIdx = i;
					bestDistance = distance;
					bestTransformMtx = transformMtx;
				}
			}
		}
	}

	if(bestMatchIdx >= 0)
	{
		outKey = m_Samples[bestMatchIdx]->m_Key;
		outTime = m_Samples[bestMatchIdx]->m_Time;
		outTransform = bestTransformMtx;
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

crPoseMatcher::MatchSample::MatchSample()
: m_Time(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crPoseMatcher::MatchSample::MatchSample(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crPoseMatcher::MatchSample::~MatchSample()
{
	if(m_PointCloud)
	{
		delete m_PointCloud;
		m_PointCloud = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crPoseMatcher::MatchSample::Serialize(datSerialize& s)
{
	s << m_Key;
	s << m_Time;

	if (s.IsRead())
	{
		m_PointCloud = rage_new crpmPointCloud;
	}

	s << *m_PointCloud;
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPoseMatcher::MatchSample::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(MatchSample);
	STRUCT_FIELD(m_Key);
	STRUCT_FIELD(m_Time);
	STRUCT_FIELD(m_PointCloud);
	STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crPoseMatcher::MatchSample::Key::Key()
{
}

////////////////////////////////////////////////////////////////////////////////

crPoseMatcher::MatchSample::Key::Key(u64 key)
{
	m_Hi = key >> 32;
	m_Lo = key & 0xFFFFFFFF;
}

////////////////////////////////////////////////////////////////////////////////

crPoseMatcher::MatchSample::Key::Key(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

void crPoseMatcher::MatchSample::Key::Serialize(datSerialize& s)
{
	s << m_Hi;
	s << m_Lo;
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPoseMatcher::MatchSample::Key::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(Key);
	STRUCT_FIELD(m_Hi);
	STRUCT_FIELD(m_Lo);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
