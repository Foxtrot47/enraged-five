//
// crextra/gesturerecognition.h
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#ifndef CREXTRA_GESTURERECOGNITION_H
#define CREXTRA_GESTURERECOGNITION_H

#include "grprofile/drawmanager.h"

namespace rage
{
#if 0

// PURPOSE:
class crGestureRecognition
{
public:

	// PURPOSE:
	crGestureRecognition();

	// PURPOSE:
	~crGestureRecognition();
	
	// PURPOSE:
	void Reset();

	// PURPOSE:
	void Update(float delta, float x, float y);
	
#if __PFDRAW
	void ProfileDraw(float x=0.f, float y=0.f);
#endif // __PFDRAW

private:
	
	struct Point
	{
		Point() : m_X(0.f), m_Y(0.f) {}
		Point(float x, float y) : m_X(x), m_Y(y) {}

		const Point& operator=(const Point& r) { m_X = r.m_X; m_Y = r.m_Y; return *this; }
		float m_X;
		float m_Y;
	};

	struct Polar
	{
		float m_Theta;
		float m_D;
	};

	Point m_CurrentPos;

	static const int sm_PreviousPosMaxCount = 100;

	atRangeArray<Point, sm_PreviousPosMaxCount> m_PreviousPos;
	int m_PreviousPosCount;
	int m_PreviousPosNext;

	Point m_CurrentVel, m_PreviousVel;
	Point m_CurrentAcc;
};
#endif //0

} // namespace rage

#endif // CREXTRA_GESTURERECOGNITION_H
