//
// crextra/gesturerecognition.cpp
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#include "gesturerecognition.h"

#include "math/simplemath.h"

#if __PFDRAW
#include "grcore/font.h"
#endif //__PFDRAW

namespace rage
{
#if 0
	
crGestureRecognition::crGestureRecognition()
: m_CurrentPos(0.f, 0.f)
, m_PreviousPosCount(0)
, m_PreviousPosNext(0)
{
	
}

crGestureRecognition::~crGestureRecognition()
{

}

void crGestureRecognition::Reset()
{

}

void crGestureRecognition::Update(float delta, float x, float y)
{
	m_CurrentPos.m_X = x;
	m_CurrentPos.m_Y = y;

	if(m_PreviousPosCount > 1)
	{
		const int previousPos = (m_PreviousPosNext+sm_PreviousPosMaxCount-1) % sm_PreviousPosMaxCount;
			
		m_PreviousVel = m_CurrentVel;

		m_CurrentVel.m_X = (m_CurrentPos.m_X - m_PreviousPos[previousPos].m_X) / delta;
		m_CurrentVel.m_Y = (m_CurrentPos.m_Y - m_PreviousPos[previousPos].m_Y) / delta;

		m_CurrentAcc.m_X = SameSign(m_CurrentVel.m_X, m_PreviousVel.m_X) ? ((m_CurrentVel.m_X - m_PreviousVel.m_X) / delta) : 0.f;
		m_CurrentAcc.m_Y = SameSign(m_CurrentVel.m_Y, m_PreviousVel.m_Y) ? ((m_CurrentVel.m_Y - m_PreviousVel.m_Y) / delta) : 0.f;
	}

	m_PreviousPos[m_PreviousPosNext].m_X = x;
	m_PreviousPos[m_PreviousPosNext].m_Y = y;

	m_PreviousPosNext = (m_PreviousPosNext+1) % sm_PreviousPosMaxCount;
	m_PreviousPosCount = Min(m_PreviousPosCount+1, sm_PreviousPosMaxCount);
}

#if __PFDRAW
PFD_DECLARE_GROUP(GestureRecognition);
PFD_DECLARE_ITEM(CurrentPos, Color32(255, 255, 255), GestureRecognition);
PFD_DECLARE_ITEM(PreviousPos, Color32(255, 0, 0), GestureRecognition);
PFD_DECLARE_ITEM(NextPos, Color32(0, 0, 255), GestureRecognition);

/*
PFD_DECLARE_ITEM(CoordinateSystems,Color32(255, 255, 255),Skeleton);
PFD_DECLARE_ITEM(BoneNames,Color32(255,255,255),Skeleton);
PFD_DECLARE_ITEM(Joints,Color32(255,255,255),Skeleton);
PFD_DECLARE_ITEM_SLIDER(JointLength,Color32(255,255,255),Skeleton,0.1f,10.0f,0.01f);
*/

void crGestureRecognition::ProfileDraw(float x, float y)
{
	if (PFDGROUP_GestureRecognition.Begin())
	{		
		char currentPosBuf[64];
		formatf(currentPosBuf, "x=%.3f y=%.3f", m_CurrentPos.m_X, m_CurrentPos.m_Y);

		PF_DRAW_SCREEN_TEXT(CurrentPos, x+100.f, y+100.f, currentPosBuf);

		PF_DRAW_SCREEN_TEXT(CurrentPos, x+200.f+(m_CurrentPos.m_X*50.f), y+200.f+(m_CurrentPos.m_Y*50.f), ".");

		for(int i=0; i<m_PreviousPosCount; ++i)
		{
			PF_DRAW_SCREEN_TEXT(PreviousPos, x+200.f+(m_PreviousPos[i].m_X*50.f), y+200.f+(m_PreviousPos[i].m_Y*50.f), ".");
		}

		for(int i=0; i<100; ++i)
		{
			float delta = (float(i)/100.f) * (1.f/30.f);

			Point NextPos;
			NextPos.m_X = m_CurrentPos.m_X + m_CurrentVel.m_X*delta + 0.5f*m_CurrentAcc.m_X*delta*delta;
			NextPos.m_Y = m_CurrentPos.m_Y + m_CurrentVel.m_Y*delta + 0.5f*m_CurrentAcc.m_Y*delta*delta;

			PF_DRAW_SCREEN_TEXT(NextPos, x+200.f+(NextPos.m_X*50.f), y+200.f+(NextPos.m_Y*50.f), ".");
		}

/*
		bool oldLighting = grcLighting(false);
		const int numBones = m_SkeletonData->GetNumBones();
		const int* parentIndices = m_SkeletonData->GetParentIndices();
		const Matrix34* renderMtxs = GetRenderBuffer();

		if (PFD_CoordinateSystems.Begin())
		{
			grcDrawAxis(axisSize, renderMtxs[0]);

			for (int i=1; i<numBones; i++)
			{
				grcDrawAxis(axisSize, renderMtxs[i]);	
			}

			PFD_CoordinateSystems.End();
		}
		*/
		PFDGROUP_GestureRecognition.End();
	}
}
#endif // __PFDRAW

#endif // 0
} // namespace rage
