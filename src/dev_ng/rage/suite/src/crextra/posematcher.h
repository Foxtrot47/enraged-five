//
// crextra/posematcher.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CREXTRA_POSEMATCHER_H
#define CREXTRA_POSEMATCHER_H

#define DEBUG_POSE_MATCHER (0)

#include "atl/array.h"
#include "data/resource.h"
#include "data/struct.h"
#include "vectormath/mat34v.h"

#include "cranimation/animation_config.h"
#include "cranimation/frameaccelerator.h"
#include <float.h>

namespace rage
{

class crAnimation;
class crClip;
class crFrame;
class crWeightSet;
class crSkeleton;
class crSkeletonData;
class crpmPointCloud;
class Matrix34;

#if CR_DEV
class crDumpOutput;
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Uses point clouds to find best matches between
// sampled animations and a current pose or history of poses.
// Useful for picking from a database of recovery animations
// when exiting some procedural movement.
class crPoseMatcher
{
public:

	// PURPOSE: Default constructor
	crPoseMatcher();

	// PURPOSE: Resource constructor
	crPoseMatcher(datResource&);

	// PURPOSE: Destructor
	~crPoseMatcher();

	// PURPOSE: Off line resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT

	// PURPOSE: Placement
	DECLARE_PLACE(crPoseMatcher);

	// PURPOSE: Resource version number
	static const int RORC_VERSION = 2;


	// PURPOSE: Initialization
	// PARAMS:
	// skelData - skeleton data (will retain internal pointer to this).
	// weightSet - weight set pointer (can be NULL, indicates no weight set).  Weight set will be copied.
	// windowSize - window size, number of frames/poses to add to cloud (>1 gives higher order matches, see notes)
	// sampleRate - sampling rate (if window >1, interval between frame samples)
	// NOTES:
	// These parameters are all integral in the way the database is calculated,
	// they can be only be set at initialization time.
	// Larger window sizes match higher order features, ie 2 == velocity, 3 == acceleration,
	// but they require a set of poses (rather than a single current pose) to be presented
	// at search time for this feature to function properly.
	void Init(const crSkeletonData& skelData, crWeightSet* weightSet=NULL, int accelSize=4, u32 windowSize=1, float sampleRate=1.f/30.f);

	// PURPOSE: Shutdown, free/release dynamic resources
	void Shutdown();

	
	// PURPOSE: Add a single sample
	// PARAMS:
	// anim - reference to animation to sample
	// key - animation identifier
	// time - time index to sample (in seconds)
	void AddSample(const crAnimation& anim, u64 key, float time=0.f);

	// PURPOSE: Add a single sample
	// PARAMS:
	// clip - reference to clip to sample
	// key - animation identifier
	// time - time index to sample (in seconds)
	void AddSample(const crClip& clip, u64 key, float time=0.f);

	// PURPOSE: Add multiple samples
	// PARAMS:
	// anim - reference to animation to sample
	// key - animation identifier
	// sampleInterval - interval period to use when obtaining samples (in seconds). Default 0.167 (ie six times a second)
	// startTime - defines start time index of window in which to obtain samples (in seconds). Default 0.0
	// endTime - defines end time index of window in which to obtain samples (in seconds).  Default end of animation.
	void AddSamples(const crAnimation& anim, u64 key, float sampleInterval=1.f/6.f, float startTime=0.f, float endTime=FLT_MAX);

	// PURPOSE: Add multiple samples
	// PARAMS:
	// clip - reference to clip to sample
	// key - animation identifier
	// sampleInterval - interval period to use when obtaining samples (in seconds). Default 0.167 (ie six times a second)
	// startTime - defines start time index of window in which to obtain samples (in seconds). Default 0.0
	// endTime - defines end time index of window in which to obtain samples (in seconds).  Default end of animation.
	void AddSamples(const crClip& clip, u64 key, float sampleInterval=1.f/6.f, float startTime=0.f, float endTime=FLT_MAX);


	// PURPOSE: Filtering object, base version (derive and customize)
	// Allows projects to filter individually filter samples to control
	// which is considered for the best match.
	class MatchFilter
	{
	public:

		// PURPOSE: Default constructor
		MatchFilter();

		// PURPOSE: Destructor
		virtual ~MatchFilter();

		// PURPOSE: Filter before transformation/distance calculation (derive and customize)
		// PARAMS:
		// key - animation identifier
		// time - time index used in sample
		// RETURNS:
		// weight for transformation/distance calculation (optionally prevent or bias sample use)
		// NOTES:
		// Use this type of filter if the filtering is only dependent on animation/time index,
		// it will cull out unwanted distance calculations, improving runtime performance
		// Cull with weight of 0.0, otherwise return 1.0 for normal use (or <1.0 or >1.0 to bias use)
		virtual float PreFilter(u64 key, float time);

		// PURPOSE: Filter after transformation/distance calculation (derive and customize)
		// PARAMS:
		// key - animation identifier
		// time - time index used in sample
		// transform - transformation (translation + up axis rotation) to align match and current pose
		// distance - result from the distance calculation metric (a measure of the similarity of poses, lower is better, zero is perfect)
		// RETURNS:
		// true - allow sample to be new become new best match, false - prevent this sample from being used.
		// NOTES:
		// Only use this type of filter if filtering is dependent on match transform or distance.
		// This filter will only be called if sample matched better than any existing best match
		// (ie it is not guaranteed to be called for every sample).
		virtual bool PostFilter(u64 key, float time, Mat34V_In transform, float distance);

#if DEBUG_POSE_MATCHER
		virtual const char* DebugName(u64 key);
#endif // DEBUG_POSE_MATCHER
	};


	// PURPOSE: Find best match (from a single pose)
	// PARAMS:
	// skel - skeleton containing pose (must have been updated already, so global matrices are posed).
	// outKey - key identifying animation that matched best
	// outTime - time index of best match
	// outTransform - transformation (translation + up axis rotation) to align match and current pose
	// filter - optional per sample filter (default NULL)
	// RETURNS:
	// true - if match found, false - no match found (output params undefined)
	// Can only fail to match if either there are no samples to match against,
	// or all samples are filtered out by the optional match filter.
	bool FindBestMatch(const crSkeleton& skel, u64& outKey, float& outTime, Mat34V_InOut outTransform, MatchFilter* filter=NULL);

	// PURPOSE: Find best match, given a set of poses (use this for improved matches with window sizes > 1)
	// PARAMS:
	// numSkeletons - count of number of skeletons being provided
	// skeletons - array of skeleton pointers, containing poses (ordered oldest to most recent)
	// outKey - key identifying animation that matched best
	// outTime - time index of best match
	// outTransform - transformation (translation + up axis rotation) to align match and current pose
	// filter - optional per sample filter (default NULL)
	// RETURNS:
	// true - if match found, false - no match found (output params undefined)
	// Can only fail to match if either there are no samples to match against,
	// or all samples are filtered out by the optional match filter.
	// NOTES:
	// Only beneficial to call this function if window size > 1 was defined when creating
	// database (larger window sizes match higher order features, ie 2 == velocity, 3 == acceleration...)
	// But in order to benefit from this higher order feature matching a set of poses must
	// be stored and presented on attempting to find a match.
	// The number of skeletons should match the window size (there is no benefit to providing more
	// and system will cope, but match less accurately if fewer are used).
	bool FindBestMatchWithHistory(int numSkeletons, const crSkeleton** skeletons, u64& outKey, float& outTime, Mat34V_InOut outTransform, MatchFilter* filter=NULL);

	// PURPOSE:
	bool Load(const char* filename);

	// PURPOSE:
	bool Save(const char* filename);

	// PUPOSE:
	void Serialize(datSerialize& s);

#if CR_DEV
	void Dump(crDumpOutput& output) const;
#endif // CR_DEV

//private:

	// PURPOSE: Internal function, adds a point cloud
	void AddPointCloud(crpmPointCloud* pointCloud, u64 key, float time);

	// PURPOSE: Internal function, matches a point cloud
	bool MatchPointCloud(const crpmPointCloud& pointCloud, u64& outKey, float& outTime, Mat34V_InOut outTransform, MatchFilter* filter);


	// PURPOSE: Defines entry in match database
	struct MatchSample
	{
		// PURPOSE: Default constructor
		MatchSample();

		// PURPOSE: Resource constructor
		MatchSample(datResource&);

		// PURPOSE: Destructor
		~MatchSample();

		// PURPOSE: Serialize
		void Serialize(datSerialize& s);

		// PURPOSE: Off line resourcing
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT

		// PURPOSE: Placement
		IMPLEMENT_PLACE_INLINE(MatchSample);

		// PUPOSE: 64 bit key.
		struct Key
		{
			//PUSPOSE: Default constructor
			Key();

			// PURPOSE:
			Key(u64 key);

			// PURPOSE:
			operator u64() const
			{
				return (u64(m_Hi)<<32)|m_Lo;
			}

			// PUPOSE: Resource constructor
			Key(datResource&);

			// PURPOSE: Serialize
			void Serialize(datSerialize& s);

			// PUPOSE: Off line resourcing
#if __DECLARESTRUCT
			void DeclareStruct(datTypeStruct&);
#endif // _DECLARESTRUCT

			// PUPOSE: Placement
			IMPLEMENT_PLACE_INLINE(Key);

			u32 m_Hi, m_Lo;
		};

		Key m_Key;
		float m_Time;

		datOwner< crpmPointCloud > m_PointCloud;
	};

	atArray< datOwner<MatchSample> > m_Samples;
	atArray< u16 > m_BoneIds;

	crAccelerator m_WeightSetCache;

	datRef<const crSkeletonData> m_SkeletonData;
	datOwner<crWeightSet> m_WeightSet;
	float m_SampleRate;
	int m_WindowSize;
	u32 m_Signature;
};

////////////////////////////////////////////////////////////////////////////////

inline crPoseMatcher::MatchFilter::MatchFilter()
{
}

////////////////////////////////////////////////////////////////////////////////

inline crPoseMatcher::MatchFilter::~MatchFilter()
{
}

////////////////////////////////////////////////////////////////////////////////

inline float crPoseMatcher::MatchFilter::PreFilter(u64, float)
{
	return 1.f;
}
	
////////////////////////////////////////////////////////////////////////////////

inline bool crPoseMatcher::MatchFilter::PostFilter(u64, float, Mat34V_In, float)
{
	return true;
}

////////////////////////////////////////////////////////////////////////////////

#if DEBUG_POSE_MATCHER
inline const char* crPoseMatcher::MatchFilter::DebugName(u64 key)
{
	static char s_DebugName[128];
	formatf(s_DebugName, sizeof(s_DebugName), "%lld", key);

	return s_DebugName;
}
#endif

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //CREXTRA_POSEMATCHER_H
