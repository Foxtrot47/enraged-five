//
// crextra/bonespring.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TODO --- better name than bone spring?  can drive blend shapes with them too...



/*

//
// pongfx/bonespringanim.h
//
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef PONGFX_BONESPRINGANIM_H
#define PONGFX_BONESPRINGANIM_H

#ifndef DATA_BASE_H
#include "data/base.h"
#endif

#ifndef PHEFFECTS_SPRINGANIMATEDATTACHMENT_H
#include "pheffects/springanimatedattachment.h"
#endif

#ifndef CRANIMATION_FRAME_H
#include "cranimation/frame.h"
#endif

#define BONE_SPRING_STR_LEN  32

namespace rage
{
	class bkBank;
	class fiTokenizer;
	class phArchetypeDamp;
	class phCollider;
	class phInst;
	class phBoundSphere;
	class Matrix34;
	class crSkeleton;
}
class pongPlayer;
class pongAnimationInfo;
class fxBoneSpringAnim;

class fxBoneSpringAnimSpring : public phSpringAninmatedAttachment
{
public:
	virtual void Update (float TimeStep);

	fxBoneSpringAnimSpring(datResource& rsc) : phSpringAninmatedAttachment(rsc), m_Frames(rsc), m_FramePtrs(rsc), m_FrameWeights(rsc), m_FrameIndices(rsc), m_FinalFrame(rsc) {	
		ObjectFixup(rsc, m_ColliderEnd);
		ObjectFixup(rsc, m_Archetype);
		rsc.PointerFixup(m_InstanceEnd);
		rsc.PointerFixup(m_BoundSphere);
		rsc.PointerFixup(m_BodyMatrix);
		rsc.PointerFixup(m_BoneNames);
		for(int i=0; i<m_Frames.GetCount(); i++)
		{
			ObjectFixup(rsc, m_FramePtrs[i]);
		}
	}

	// PURPOSE: Constructor, pass in a pointer to a position, it automagically keeps track of it. Also pass in spring parameters.
	fxBoneSpringAnimSpring (const Vector3* pos, float springConstant, float springLength, float springDampening, float motionScale, Vector3& systemDampening);
	~fxBoneSpringAnimSpring();
	virtual void Reset();
	phArchetypeDamp*						m_Archetype;
	phCollider*								m_ColliderEnd;
	phInst*									m_InstanceEnd;
	phBoundSphere*							m_BoundSphere;
	atArray<crFrame>					m_Frames;
	atArray<const crFrame*>				m_FramePtrs;
	atArray<float>							m_FrameWeights;
	atArray<int>							m_FrameIndices;
	crFrame								m_FinalFrame;
	Matrix34*								m_BodyMatrix;
	Vector3									m_Pos;
	Vector3									m_SpringAnimForce;
	Vector3									m_SpringOffset;
	float									m_SpringDampening;
	float									m_MotionScale;
	float									m_Weight;
	bool									m_SpringAnimForceEnableX;
	bool									m_SpringAnimForceEnableY;
	bool									m_SpringAnimForceEnableZ;
	char*									m_BoneNames;						// name of bone
#if __BANK
	void									Save(fiTokenizer& tok);
	void									AddWidgets(bkBank& bk);
#endif
};

class fxBoneSpringAnim : public datBase
{
public:
	enum eFrameIndices { kCenterFrame, kUpFrame, kRightFrame, kDownFrame, kLeftFrame, kFrontFrame, kBackFrame, kMaxNumBlendFrames };

	fxBoneSpringAnim();
	DECLARE_PLACE(fxBoneSpringAnim);
	fxBoneSpringAnim(datResource& rsc);
	~fxBoneSpringAnim();
	void									AddInstanceToLevel();
	void									RemoveInstanceFromLevel();
	void									Update();
	void									RequestReset() { m_ResetRequested = true; }
	void									Reset();

	bool									Load(fiTokenizer& tok, const char* name, crSkeleton& skel, const pongAnimationInfo* animInfo);
	void									Unload();								// releases stuff it doesn't need after it is no longer used
	const char*								GetName() const { return m_Name; }
	bool									GetActive() { return m_Active; }
	void									SetActive(bool active, float dTime);

#if __BANK
	static void								Save(pongPlayer* player);
	void									AddWidgets(bkBank& bk);
#endif

protected:
	void									UpdateWeight();
	float									GetWeight() const { return (m_AlwaysActive ? 1.0f : m_Weight); }

protected:
	atArray<fxBoneSpringAnimSpring*>		m_Spring;
	crSkeleton*								m_Skeleton;
	char									m_Name[BONE_SPRING_STR_LEN];						// name of spring
	float									m_Weight;
	bool									m_GoalActive;
	float									m_GoalDeltaTime;
	bool									m_Active;
	bool									m_AlwaysActive;										// if true, spring can't be deactivated.
	bool									m_ResetRequested;
};


#endif
 	
 */
