//
// crextra/mover.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CREXTRA_MOVER_H
#define CREXTRA_MOVER_H

#include "data/resource.h"
#include "data/struct.h"
#include "phbound/boundcapsule.h"
#include "phbound/boundcomposite.h"
#include "physics/archetype.h"
#include "physics/collider.h"
#include "physics/inst.h"
#include "vector/quaternion.h"

namespace rage
{

// TODO --- needs to support z up not just y up!


// PURPOSE: Physical mover.
// Example physical mover for basic character locomotion.
// Provides capsule bounds for collision, and single probe for ground plane detection.
class crMoverInst : public phInst
{
public:

	// PURPOSE: Default constructor
	crMoverInst();

	// PURPOSE: Resource constructor
	crMoverInst(datResource& rsc);

	// PURPOSE: Destructor
	virtual ~crMoverInst();

#if __DECLARESTRUCT
	// PURPOSE: Offline resourcing
	void DeclareStruct(datTypeStruct&);
#endif //!__FINAL

	// PURPOSE: Placement
	DECLARE_PLACE(crMoverInst);

	// PURPOSE: Initialize mover
	// PARAMS: radius - capsule radius
	// height - character height
	// floatHeight - buffer mover tries to create between bottom of the capsule and the ground
	// extraBoundOffset - any additional offset
	// mass - mass of capsule
	// ownInst - own instance to omit from all collisions/probes
	void Init(const float radius, const float height, const float floatHeight, Vector3::Vector3Param extraBoundOffset, const float mass, const phInst* ownInst);

	// PURPOSE: Free/release dynamic resources
	void Shutdown();

	// PURPOSE: Sets the total size of the character, taking into account the float height.
	// PARAMS: radius - capsule radius
	// height - character height
	// floatHeight - buffer mover tries to create between bottom of the capsule and the ground
	// extraBoundOffset - any additional offset
	// NOTES: It should be safe and cheap to keep calling this with the
	// same parameters over and over again.
	void SetCharacterSize(const float radius, const float height, const float floatHeight, Vector3::Vector3Param extraBoundOffset);

	// PURPOSE: Sets the mass of the capsule.
	// PARAMS: newMass - new mass of capsule
	// NOTES: Also recalculates the angular inertia of the capsule.
	void SetMass(const float newMass);

	// PURPOSE: Set max impulse allowed along local x axis
	// PARAMS: newLateralAccelCap - new max impulse
	void SetLateralAccelCap(const float newLateralAccelCap);

	// PURPOSE: Set max impulse allowed along local z axis
	// PARAMS: newLinearAccelCap - new max impulse
	void SetLinearAccelCap(const float newLinearAccelCap);

	// PURPOSE: Get max impulse allowed along local x axis
	// REUTRNS: max impulse
	float GetLateralAccelCap() const;

	// PURPOSE: Get max impulse allowed along local z axis
	// RETURNS: max impulse
	float GetLinearAccelCap() const;

	// PURPOSE: Add additional capsule bounds
	// PARAMS: boundIndex - new bound index
	// newBound - new bound pointer
	// boundMatrix - new bound matrix
	void AddExtraBound(const int boundIndex, phBound *newBound, const Matrix34 &boundMatrix);

	// PURPOSE: Exclude instance from collision
	// PARAMS: exclusionInst - instance to be excluded
	// NOTES: Only supports a single exclusion inst (at the moment).
	void AddCollisionExclusionInst(const phInst *exclusionInst);

	// PURPOSE: Remove instance from collision exclusion
	// PARAMS: exclusionInst - inst to be removed from exclusion
	// NOTES: Only supports a single exclusion inst (at the moment).
	void RemoveCollisionExclusionInst(const phInst *exclusionInst);

	// PURPOSE: Exclude instance from probe collision
	// PARAMS: exclusionInst - instance to be removed from exclusion
	// NOTES: Only supports a single exclusion inst (at the moment).
	void AddProbeExclusionInst(const phInst *exclusionInst);

	// PURPOSE: Remove instance from probe collision exclusion
	// PARAMS: exclusionInst - instance to be removed from exclusion
	// NOTES: Only supports a single exclusion inst (at the moment).
	void RemoveProbeExclusionInst(const phInst *exclusionInst);

	// PURPOSE: Set the mover's position and rotation directly (ie teleport)
	// PARAMS: mtx - new mover transform
	void SetMoverMtx(const Matrix34& mtx);

	// PURPOSE: Get the mover's current position and rotation
	// RETURNS: mtx - mover's current transform
	const Matrix34& GetMoverMtx() const;

	// PURPOSE: Get the desired capsule float height
	// RETURNS: Capsule float height
	float GetFloatHeight() const;

	// PURPOSE: Get the capsule bound
	// RETURNS: const capsule bound reference
	const phBoundCapsule &GetCapsuleBound() const;

	// PURPOSE: Get the collider
	// RETURNS: const collider reference
	const phCollider &GetCollider() const;

	// PURPOSE: Did the ground probe find the ground during the last update.
	// RETURNS: true - if ground was found
	bool IsOnGround() const;

	// PURPOSE: Returns normal of the ground that was last found.
	// RETURNS: Ground normal vector
	// NOTES: If ground has never been found, this will be whatever world up is.
	const Vector3 &GetLastGroundNormal() const;

	// PURPOSE: Returns the material ground probe found during last update
	// RETURNS: Pointer to material, can be NULL.
	// NOTES: If IsOnGround()==false => GetGoundMaterial()==NULL
	// while IsOnGround()==true => GetGroundMaterial()!=NULL
	const phMaterial *GetGroundMaterial() const;

	// PURPOSE: Has a jump been initiated (and no landing detected yet)
	// RETURNS: true - if jump initiated
	bool IsJumping() const;

	// PURPOSE: Is the mover suspended
	// RETURNS: true - mover is suspended
	// NOTES: Suspend mode indicates all physical aspects of mover are disabled.
	// Collisions are ignored, inputs are applied statically (physically relocating
	// object instead of applying impulses).
	bool IsSuspended() const;

	// PURPOSE: Is the mover recovering
	// RETURNS: true - mover is recovering
	// NOTES: Recovery mode is similar to suspend mode in that physical
	// aspects of the mover are disabled (athough the mover will collide
	// with fixed bounds - important in case a character gets spawned
	// penetrating the ground).
	// The main difference between recovery and suspend modes are that
	// recovery mode ends *automatically* when the mover finds a bound
	// within a certain distance.
	bool IsRecovering() const;

	// PURPOSE: Is the mover attached to an instance
	// RETURNS: true - if the mover is attached
	bool IsAttached() const;

	// PURPOSE: Has the mover collide with something
	// RETURNS: true - if collided with something since last update
	bool GetCollided() const;

	// PURPOSE: Has the mover been obstructed by something
	// RETURNS: true - if obstructed by something since last update
	bool GetObstructed() const;

	// PURPOSE: Get sliding along the local x axis (impluse required / max impulse allowed)
	// RETURNS: Lateral slide
	float GetLateralSlide() const;

	// PURPOSE: Get sliding along the local z axis (impluse required / max impulse allowed)
	// RETURNS: Linear slide
	float GetLinearSlide() const;

	// PURPOSE: Teleport the mover
	// PARAMS: newMatrix - new mover location
	// keepPreviousState - prevents state clearing for small teleports (default: false)
	void Teleport(const Matrix34 &newMatrix, bool keepPreviousState=false);

	// PURPOSE: Initiate a jump
	// PARAMS: height - jump height
	// duration - jump duration
	void StartJump(float height, float duration);

	// PURPOSE: End a jump
	// NOTES: Clients should not need to call this EVER
	void EndJump();

	// PURPOSE: Suspend the mover
	// NOTES: Requests to start suspend mode are queued.  For every StartSuspend()
	// that is issued there should be a corresponding EndSuspend().
	// The mover will remain in suspend mode until ALL outstanding requests are matched.
	// SEE ALSO: IsSuspended
	void StartSuspend();

	// PURPOSE: End suspend mode
	// NOTES: Suspend requests are queued, ALL outstanding requests to StartSuspend()
	// must be matched with EndSuspend() requests before suspend mode is exited.
	void EndSuspend();

	// PURPOSE: Start or end suspend mode
	// PARAMS: suspend - true to start suspend, false to end suspend
	// NOTES: See StartSuspend() for disucssion of how suspend mode is queued.
	void Suspend(bool suspend);

	// PURPOSE: Start mover recovery
	// PARAMS: recoveryHeightTolerance -  the distance below the mover within which a
	// bound needs to be found in order to exit recovery mode.
	// NOTES: Unlike suspend mode, calls to StartRecovery() are not queued.
	void StartRecovery(const float recoveryHeightTolerance);

	// PURPOSE: End recovery mode
	// NOTES: Clients should not need to call this EVER
	void EndRecovery();

	// PURPOSE: Set attachment
	// PARAMS: attachmentInst - instance mover is attached to
	// attachmentComponent - component on instance
	// attachmentMatrix - transformation to component attachement
	void SetAttached(const phInst *attachmentInst, const u16 attachmentComponent, const Matrix34 &attachmentMatrix);

	// PURPOSE: Update
	// PARAMS: deltaTime - time elapsed since last update
	// desiredTranslation - delta translation (world space)
	// desiredRotation - direction magnitude (world space, only supported about y axis)
	void Update(float deltaTime, const Vector3 &desiredTranslation, const Vector3 &desiredRotation);

	// PURPOSE: Update (matrix delta form)
	// PARAMS: deltaTime - time elapsed since last update
	// desiredTransform - delta transform matrix (world space)
	void Update(float deltaTime, const Matrix34& desiredTransform);

	// PURPOSE: Externaly controlled velocity override
	virtual Vec3V_Out GetExternallyControlledVelocity() const;

	// PURPOSE: Externally controlled angular velocity override
	virtual Vec3V_Out GetExternallyControlledAngVelocity() const;

	// PURPOSE: Should find impacts override
    virtual bool ShouldFindImpacts(const phInst* otherInst) const;

	// PURPOSE: Pre compute impacts override
    virtual void PreComputeImpacts(phContactIterator impacts);

	// PURPOSE: Prepare for activation override
	virtual phInst* PrepareForActivation(phCollider** colliderToUse, phInst* otherInst, const phConstraintBase * constraint);

	// PURPOSE: Prepare for deactivation override
	virtual bool PrepareForDeactivation(bool colliderIsManagedBySim, bool forceDeactivate);

	// PURPOSE: Set the suspend velocity
	// PARAMS: vel - suspend velocity
	// NOTES: Suspend velocity is the velocity reported as the externally
	// controlled velocity when in suspend mode.
	void SetSuspendVelocity(Vector3::Vector3Param vel);

	// PURPOSE: Get the suspend velocity
	// RETURNS: suspend velocity
	Vector3 GetSuspendVelocity() const;

	// PURPOSE: Set the gravity factor
	// PARAMS: factor - new gravity factor (using just 1.0 makes character seem floaty)
	void SetGravityFactor(float factor);

	// PURPOSE: Get the gravity factor
	// RETURNS: gravity factor
	float GetGravityFactor() const;

	// PURPOSE: Set the slope factor (controls slowdown/speedup on inclines)
	// PARAMS: factor - new slope factor (0.0 disables slope speed modification)
	void SetSlopeFactor(float factor);

	// PURPOSE: Get the slope factor
	// RETURNS: slope factor
	float GetSlopeFactor() const;

	// PURPOSE: Set the ground probe penetration
	// PARAMS: penetration - additional depth probe will travel below mover to find ground
	void SetGroundProbePenetration(float penetration);

	// PURPOSE: Get the ground probe penetration
	// RETURNS: ground probe penetration
	float GetGroundProbePenetration() const;

	// PURPOSE: Set the ground probe update delay
	// An optimization, the time the mover will wait before firing another
	// ground probe, if there is little movement.
	// PARAMS: delay - in seconds
	void SetGroundProbeUpdateDelay(float delay);

	// PURPOSE: Get the ground probe update delay
	// RETURNS: delay in seconds
	float GetGroundProbeUpdateDelay() const;

protected:

	// PURPOSE: Reset mover to its initial state.
	void Reset();

	// PURPOSE: Internal call, going back to being fully active again.
	void MakePhysical();

	// PURPOSE: Internal call, no longer physically active.
	void EndPhysical();


private:

	phArchetypeDamp m_ArchetypeDamped;
	phBoundCapsule m_CapsuleBound;
	phBoundComposite m_CompositeBound;
	phCollider m_Collider;

	Matrix34 m_AttachmentMatrix;

	Vector3 m_LastGroundNormal;
	Vector3 m_GroundInstancePos_WS;  // world space
	Vector3 m_GroundInstancePos_CS;  // collision space
	Vector3 m_RightVector_WS;  // right vector of character (previous frame)
	Vector3 m_RightVector_CS;
	Vector3 m_SuspendVelocity;

	datRef<const phInst> m_CollisionExclusionInst;
	datRef<const phInst> m_ProbeExclusionInst;
	datRef<const phInst> m_AttachmentInst; // instance attached to, NULL if none
	datRef<phInst> m_GroundInstance; // ground instance standing on, NULL if none
	datRef<const phMaterial> m_GroundMaterial;

	float m_GroundProbeDepth; // -1.f if we didn't find any ground last time
	float m_RecoveryHeightTolerance;
	float m_FloatHeight;
	float m_LateralSlide;
	float m_LinearSlide;
	float m_LateralAccelCap;
	float m_LinearAccelCap;
	float m_GravityFactor;
	float m_SlopeFactor;
	float m_GroundProbePenetration;
	float m_GroundProbeTimer;
	float m_GroundProbeUpdateDelay;

	u32 m_IsSuspendedCount; // number of queued start suspends

	u16 m_AttachmentComponent;
	u16 m_GroundInstanceComponent;

	bool m_IsOnGround;				
	bool m_IsJumping;
	bool m_IsRecovering;
	bool m_CollidedCached;
	bool m_Collided;
	bool m_ObstructedCached;
	bool m_Obstructed;


	static const float sm_DefaultCharacterRadius;
	static const float sm_DefaultCharacterHeight;
	static const float sm_DefaultCharacterFloatHeight;
	static const Vector3 sm_DefaultCharacterExtraBoundOffset;
	static const float sm_DefaultGravityFactor;
	static const float sm_DefaultCharacterMass;
	static const float sm_DefaultAccelCap;
	static const float sm_DefaultSlopeFactor;
	static const float sm_DefaultGroundProbePenetration;
	static const float sm_DefaultGroundProbeUpdateDelay;

	static const float sm_MaxAngSpeed;
	static const float sm_UprightTolerance;
	static const float sm_GroundNormalTolerance;
	static const float sm_GroundProbeHeightFactor;
	static const float sm_ObstructionTolerance;
	static const float sm_SpringConstant;
	static const float sm_EffectiveDepthFactor;
	static const float sm_DampeningConstant;
};


// inlines

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::SetLateralAccelCap(const float newLateralAccelCap)
{
	m_LateralAccelCap = newLateralAccelCap;
}

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::SetLinearAccelCap(const float newLinearAccelCap)
{
	m_LinearAccelCap = newLinearAccelCap;
}

////////////////////////////////////////////////////////////////////////////////

inline float crMoverInst::GetLateralAccelCap() const
{
	return m_LateralAccelCap;
}

////////////////////////////////////////////////////////////////////////////////

inline float crMoverInst::GetLinearAccelCap() const
{
	return m_LinearAccelCap;
}

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::AddCollisionExclusionInst(const phInst *exclusionInst)
{
	FastAssert(m_CollisionExclusionInst == NULL);

	m_CollisionExclusionInst = exclusionInst;
}

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::RemoveCollisionExclusionInst(const phInst *ASSERT_ONLY(exclusionInst))
{
	FastAssert(m_CollisionExclusionInst == exclusionInst);

	m_CollisionExclusionInst = NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::AddProbeExclusionInst(const phInst *exclusionInst)
{
	FastAssert(m_ProbeExclusionInst == NULL);

	m_ProbeExclusionInst = exclusionInst;
}

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::RemoveProbeExclusionInst(const phInst *ASSERT_ONLY(exclusionInst))
{
	FastAssert(m_ProbeExclusionInst == exclusionInst);

	m_ProbeExclusionInst = NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::SetMoverMtx(const Matrix34& mtx)
{
	Teleport(mtx);
}

////////////////////////////////////////////////////////////////////////////////

inline const Matrix34& crMoverInst::GetMoverMtx() const
{
	return RCC_MATRIX34(GetMatrix());
}

////////////////////////////////////////////////////////////////////////////////

inline float crMoverInst::GetFloatHeight() const
{
	return m_FloatHeight;
}

////////////////////////////////////////////////////////////////////////////////

inline const phBoundCapsule &crMoverInst::GetCapsuleBound() const
{
	return m_CapsuleBound;
}

////////////////////////////////////////////////////////////////////////////////

inline const phCollider &crMoverInst::GetCollider() const
{
	return m_Collider;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crMoverInst::IsOnGround() const
{
	return m_IsOnGround;
}

////////////////////////////////////////////////////////////////////////////////

inline const Vector3 &crMoverInst::GetLastGroundNormal() const
{
	return m_LastGroundNormal;
}

////////////////////////////////////////////////////////////////////////////////

inline const phMaterial *crMoverInst::GetGroundMaterial() const
{
	return m_GroundMaterial;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crMoverInst::IsJumping() const
{
	return m_IsJumping;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crMoverInst::IsSuspended() const
{
	return m_IsSuspendedCount > 0;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crMoverInst::IsRecovering() const
{
	return m_IsRecovering;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crMoverInst::IsAttached() const
{
	return m_AttachmentInst != NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crMoverInst::GetCollided() const
{
	return m_Collided;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crMoverInst::GetObstructed() const
{
	return m_Obstructed;
}

////////////////////////////////////////////////////////////////////////////////

inline float crMoverInst::GetLateralSlide() const
{
	return m_LateralSlide;
}

////////////////////////////////////////////////////////////////////////////////

inline float crMoverInst::GetLinearSlide() const
{
	return m_LinearSlide;
}

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::Suspend(bool suspend)
{
	if(suspend)
	{
		StartSuspend();
	}
	else
	{
		EndSuspend();
	}
}

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::SetAttached(const phInst *attachmentInst, const u16 attachmentComponent, const Matrix34 &attachmentMatrix)
{
	FastAssert(attachmentInst == NULL || IsSuspended());

	m_AttachmentInst = attachmentInst;
	m_AttachmentComponent = attachmentComponent;
	m_AttachmentMatrix.Set(attachmentMatrix);
}

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::Update(float deltaTime, const Matrix34& desiredTransform)
{
	Quaternion q;
	q.FromMatrix34(desiredTransform);

	Vector3 angVel;
	float ang;
	q.ToRotation(angVel, ang);

	angVel.Scale(ang);

	Update(deltaTime, desiredTransform.d, angVel);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crMoverInst::PrepareForDeactivation(bool UNUSED_PARAM(colliderIsManagedBySim), bool UNUSED_PARAM(forceDeactivate))
{
	return true;
}

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::SetSuspendVelocity(Vector3::Vector3Param vel)
{
	m_SuspendVelocity.Set(vel);	
}

////////////////////////////////////////////////////////////////////////////////

inline Vector3 crMoverInst::GetSuspendVelocity() const
{
	return m_SuspendVelocity;	
}

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::SetGravityFactor(float factor)
{
	m_GravityFactor = factor;
}

////////////////////////////////////////////////////////////////////////////////

inline float crMoverInst::GetGravityFactor() const
{
	return m_GravityFactor;
}

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::SetSlopeFactor(float factor)
{
	m_SlopeFactor = factor;
}

////////////////////////////////////////////////////////////////////////////////

inline float crMoverInst::GetSlopeFactor() const
{
	return m_SlopeFactor;
}

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::SetGroundProbePenetration(float penetration)
{
	m_GroundProbePenetration = penetration;
}

////////////////////////////////////////////////////////////////////////////////

inline float crMoverInst::GetGroundProbePenetration() const
{
	return m_GroundProbePenetration;
}

////////////////////////////////////////////////////////////////////////////////

inline void crMoverInst::SetGroundProbeUpdateDelay(float delay)
{
	m_GroundProbeUpdateDelay = delay;
}

////////////////////////////////////////////////////////////////////////////////

inline float crMoverInst::GetGroundProbeUpdateDelay() const
{
	return m_GroundProbeUpdateDelay;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CREXTRA_MOVER_H
