//
// crextra/componentevent.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "componentevent.h"

#include "creature/creature.h"
#include "crskeleton/skeleton.h"
#include "crclip/clip.h"
#include "crparameterizedmotion/parameterizedmotion.h"
#include "paging/rscbuilder.h"
#include "vector/vector3.h"

namespace rage
{

atPool<crEventData> crCreatureComponentEvent::sm_EventDataPool;
sysCriticalSectionToken crCreatureComponentEvent::sm_EventQueueLock;

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentEvent::InitClass(u16 eventPoolSize)
{
	sysCriticalSection lock(sm_EventQueueLock);
	sm_EventDataPool.Init(eventPoolSize);
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentEvent::ShutdownClass()
{

}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentEvent::crCreatureComponentEvent()
: crCreatureComponent(kCreatureComponentTypeEvent)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentEvent::crCreatureComponentEvent(crCreature& creature)
: crCreatureComponent(kCreatureComponentTypeEvent)
{
	Init(creature);
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentEvent::crCreatureComponentEvent(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crCreatureComponentEvent::~crCreatureComponentEvent()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_CREATURE_COMPONENT_TYPE(crCreatureComponentEvent, crCreatureComponent::kCreatureComponentTypeEvent);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crCreatureComponentEvent::DeclareStruct(datTypeStruct& s)
{
	crCreatureComponent::DeclareStruct(s);

	STRUCT_BEGIN(crCreatureComponentEvent);
	STRUCT_FIELD(m_Player);
	STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentEvent::Init(crCreature& creature)
{
	PreInit(creature);
	m_Player.CreateParameterList();
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentEvent::Shutdown()
{
	FlushPendingEvents();
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentEvent::FlushPendingEvents()
{
	{ // scope for lock
		sysCriticalSection lock(sm_EventQueueLock);

		while(!m_EventQueue.empty())
		{
			crEventData* data = m_EventQueue.back();

			if (data->m_Clip)
			{
				data->m_Clip->Release();	
			}
			if (data->m_PmData)
			{
				data->m_PmData->Release();	
			}

			m_EventQueue.pop_back();		
			sm_EventDataPool.Delete(data);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentEvent::AddEvent(const evtSet& eventSet, const crClip& clip, float rate)
{
	clip.AddRef();

	{	// scope for lock
		sysCriticalSection lock(sm_EventQueueLock);

		if (sm_EventDataPool.IsFull())
		{
			Errorf("The event queue is full. Can't add new events until existing ones get processed");
			return;
		}

		crEventData* data = sm_EventDataPool.New();

		data->Reset();

		data->m_EventSet = &eventSet;
		data->m_Clip = &clip;
		data->m_Rate = rate;

		m_EventQueue.push_front(data);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentEvent::AddEvent(const evtSet& eventSet, const crpmParameterizedMotionData& pmData, float rate)
{
	pmData.AddRef();

	{	// scope for lock
		sysCriticalSection lock(sm_EventQueueLock);
		if (sm_EventDataPool.IsFull())
		{
			Errorf("The event queue is full. Can't add new events until existing ones get processed");
			return;
		}

		crEventData* data = sm_EventDataPool.New();
		data->Reset();
		data->m_EventSet = &eventSet;
		data->m_PmData = &pmData;
		data->m_Rate = rate;

		m_EventQueue.push_front(data);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crCreatureComponentEvent::PostUpdate()
{
	Assertf(m_Creature, "You need a creature to run creature events");
	m_Player.GetPosition() = m_Creature->GetMoverMtx() ? m_Creature->GetMoverMtx()->GetCol3() : Vec3V(V_ZERO);
	m_Player.GetSkeleton() = const_cast<const crSkeleton*>(m_Creature->GetSkeleton());

	{	// scope for lock
		sysCriticalSection lock(sm_EventQueueLock);

		while(!m_EventQueue.empty())
		{
			crEventData* eventData = m_EventQueue.back();
			Assertf(eventData && eventData->m_EventSet, "Everything in the eventqueue needs an eventset");
			m_Player.SetSet(const_cast<evtSet&>(*eventData->m_EventSet));
			m_Player.GetAnimRate() = eventData->m_Rate;
			m_Player.StartUpdateStop();

			if (eventData->m_Clip)
			{
				eventData->m_Clip->Release();
			}
			if (eventData->m_PmData)
			{
				eventData->m_PmData->Release();
			}
			m_EventQueue.pop_back(); // remove eventData from the list
			sm_EventDataPool.Delete(eventData); // return it to the pool
		}
	}

}

////////////////////////////////////////////////////////////////////////////////

evtParamId crCreatureEventPlayer::s_AnimRate;
evtParamId crCreatureEventPlayer::s_Position;
evtParamId crCreatureEventPlayer::s_Skeleton;

IMPLEMENT_EVENT_PLAYER(crCreatureEventPlayer)

void crCreatureEventPlayer::RegisterPlayerClass()
{
	s_AnimRate = s_ParameterScheme.AddParam<float>("anim rate"); // PARAM_ANIM_SPEED
	s_Position = s_ParameterScheme.AddParam<Vector3>("position"); // PARAM_ANIM_ROOT_POSITION
	s_Skeleton = s_ParameterScheme.AddParam<const crSkeleton*>("skeleton pointer"); // PARAM_ANIM_SKELETON
}

void crCreatureEventPlayer::UnregisterPlayerClass()
{
	s_ParameterScheme.DeleteParams();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
