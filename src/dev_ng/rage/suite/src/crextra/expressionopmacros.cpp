//
// crextra/expressionopmacros.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "expressionopmacros.h"

#if CR_DEV
#include "cranimation/framedata.h"

namespace rage
{

namespace crMakeExpressionOp
{

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* Constant(const crExpressionOp::Value& val)
{
	crExpressionOpConstant* ret = rage_new crExpressionOpConstant;
	ret->SetValue(val);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ConstantFloat(float f)
{
	crExpressionOpConstantFloat* ret = rage_new crExpressionOpConstantFloat;
	ret->SetFloat(f);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* Get(u8 track, u16 id, u8 type)
{
	crExpressionOpGet* ret = rage_new crExpressionOpGet;
	ret->SetTrack(track);
	ret->SetId(id);
	ret->SetType(type);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* GetQuaternion(u8 track, u16 id)
{
	return Get(track, id, kFormatTypeQuaternion);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* GetVector3(u8 track, u16 id)
{
	return Get(track, id, kFormatTypeVector3);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* GetFloat(u8 track, u16 id)
{
	return Get(track, id, kFormatTypeFloat);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* GetBoneRotation(u16 id)
{
	return GetQuaternion(kTrackBoneRotation, id);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* GetBoneTranslation(u16 id)
{
	return GetVector3(kTrackBoneTranslation, id);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* GetBoneScale(u16 id)
{
	return GetVector3(kTrackBoneScale, id);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* GetDefaultBoneRotation(u16 id)
{
	crExpressionOpGet* op = static_cast<crExpressionOpGet*>(GetQuaternion(kTrackBoneRotation, id));
	op->SetForceDefault(true);
	return op;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* GetDefaultBoneTranslation(u16 id)
{
	crExpressionOpGet* op = static_cast<crExpressionOpGet*>(GetVector3(kTrackBoneTranslation, id));
	op->SetForceDefault(true);
	return op;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* GetDefaultBoneScale(u16 id)
{
	crExpressionOpGet* op = static_cast<crExpressionOpGet*>(GetVector3(kTrackBoneScale, id));
	op->SetForceDefault(true);
	return op;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* GetMoverRotation()
{
	return GetQuaternion(kTrackMoverRotation, 0);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* GetMoverTranslation()
{
	return GetVector3(kTrackMoverTranslation, 0);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* GetMoverScale()
{
	return GetVector3(kTrackMoverScale, 0);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* GetVariable(u32 hash)
{
	crExpressionOpVariableGet* ret = rage_new crExpressionOpVariableGet;
	ret->SetHash(hash);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* Set(u8 track, u16 id, u8 type, crExpressionOp* op)
{
	crExpressionOpSet* ret = rage_new crExpressionOpSet;
	ret->SetTrack(track);
	ret->SetId(id);
	ret->SetType(type);
	ret->SetSource(op);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SetQuaternion(u8 track, u16 id, crExpressionOp* op)
{
	return Set(track, id, kFormatTypeQuaternion, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SetVector3(u8 track, u16 id, crExpressionOp* op)
{
	return Set(track, id, kFormatTypeVector3, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SetFloat(u8 track, u16 id, crExpressionOp* op)
{
	return Set(track, id, kFormatTypeFloat, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SetBoneRotation(u16 id, crExpressionOp* op)
{
	return SetQuaternion(kTrackBoneRotation, id, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SetBoneTranslation(u16 id, crExpressionOp* op)
{
	return SetVector3(kTrackBoneTranslation, id, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SetBoneScale(u16 id, crExpressionOp* op)
{
	return SetVector3(kTrackBoneScale, id, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SetMoverRotation(crExpressionOp* op)
{
	return SetQuaternion(kTrackMoverRotation, 0, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SetMoverTranslation(crExpressionOp* op)
{
	return SetVector3(kTrackMoverTranslation, 0, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SetMoverScale(crExpressionOp* op)
{
	return SetVector3(kTrackMoverScale, 0, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SetVariable(u32 hash, crExpressionOp* op)
{
	crExpressionOpVariableSet* ret = rage_new crExpressionOpVariableSet;
	ret->SetHash(hash);
	ret->SetChild(0, *op);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* Valid(u8 track, u16 id, u8 type)
{
	crExpressionOpValid* ret = rage_new crExpressionOpValid;
	ret->SetTrack(track);
	ret->SetId(id);
	ret->SetType(type);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ValidBoneRotation(u16 id)
{
	return Valid(kTrackBoneRotation, id, kFormatTypeQuaternion);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ValidBoneTranslation(u16 id)
{
	return Valid(kTrackBoneTranslation, id, kFormatTypeVector3);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ValidBoneScale(u16 id)
{
	return Valid(kTrackBoneScale, id, kFormatTypeVector3);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ValidMoverRotation()
{
	return Valid(kTrackMoverRotation, 0, kFormatTypeQuaternion);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ValidMoverTranslation()
{
	return Valid(kTrackMoverTranslation, 0, kFormatTypeVector3);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ValidMoverScale()
{
	return Valid(kTrackMoverScale, 0, kFormatTypeVector3);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentGet(u8 track, u16 id, u8 type, u8 component, u8 order)
{
	crExpressionOpComponentGet* ret = rage_new crExpressionOpComponentGet;
	ret->SetTrack(track);
	ret->SetId(id);
	ret->SetType(type);
	ret->SetComponent(component);
	ret->SetOrder(order);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentGetEuler(u8 track, u16 id, u8 component, u8 order)
{
	return ComponentGet(track, id, kFormatTypeQuaternion, component, order);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentGetVector3(u8 track, u16 id, u8 component)
{
	return ComponentGet(track, id, kFormatTypeVector3, component, 0);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentGetBoneRotation(u16 id, u8 component, u8 order)
{
	return ComponentGet(kTrackBoneRotation, id, kFormatTypeQuaternion, component, order);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentGetBoneTranslation(u16 id, u8 component)
{
	return ComponentGet(kTrackBoneTranslation, id, kFormatTypeVector3, component, 0);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentGetBoneScale(u16 id, u8 component)
{
	return ComponentGet(kTrackBoneScale, id, kFormatTypeVector3, component, 0);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentGetDefaultBoneRotation(u16 id, u8 component, u8 order)
{
	crExpressionOpComponentGet* op = static_cast<crExpressionOpComponentGet*>(ComponentGet(kTrackBoneRotation, id, kFormatTypeQuaternion, component, order));
	op->SetForceDefault(true);
	return op;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentGetDefaultBoneTranslation(u16 id, u8 component)
{
	crExpressionOpComponentGet* op = static_cast<crExpressionOpComponentGet*>(ComponentGet(kTrackBoneTranslation, id, kFormatTypeVector3, component, 0));
	op->SetForceDefault(true);
	return op;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentGetDefaultBoneScale(u16 id, u8 component)
{
	crExpressionOpComponentGet* op = static_cast<crExpressionOpComponentGet*>(ComponentGet(kTrackBoneScale, id, kFormatTypeVector3, component, 0));
	op->SetForceDefault(true);
	return op;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentGetMoverRotation(u8 component, u8 order)
{
	return ComponentGet(kTrackMoverRotation, 0, kFormatTypeQuaternion, component, order);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentGetMoverTranslation(u8 component)
{
	return ComponentGet(kTrackMoverTranslation, 0, kFormatTypeVector3, component, 0);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentGetMoverScale(u8 component)
{
	return ComponentGet(kTrackMoverScale, 0, kFormatTypeVector3, component, 0);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentSet(u8 track, u16 id, u8 type, u8 component, u8 order, crExpressionOp* op)
{
	crExpressionOpComponentSet* ret = rage_new crExpressionOpComponentSet;
	ret->SetTrack(track);
	ret->SetId(id);
	ret->SetType(type);
	ret->SetComponent(component);
	ret->SetOrder(order);
	ret->SetSource(op);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentSetEuler(u8 track, u16 id, u8 component, u8 order, crExpressionOp* op)
{
	return ComponentSet(track, id, kFormatTypeQuaternion, component, order, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentSetVector3(u8 track, u16 id, u8 component, crExpressionOp* op)
{
	return ComponentSet(track, id, kFormatTypeVector3, component, 0, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentSetBoneRotation(u16 id, u8 component, u8 order, crExpressionOp* op)
{
	return ComponentSetEuler(kTrackBoneRotation, id, component, order, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentSetBoneTranslation(u16 id, u8 component, crExpressionOp* op)
{
	return ComponentSetVector3(kTrackBoneTranslation, id, component, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentSetBoneScale(u16 id, u8 component, crExpressionOp* op)
{
	return ComponentSetVector3(kTrackBoneScale, id, component, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentSetMoverRotation(u8 component, u8 order, crExpressionOp* op)
{
	return ComponentSetEuler(kTrackMoverRotation, 0, component, order, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentSetMoverTranslation(u8 component, crExpressionOp* op)
{
	return ComponentSetVector3(kTrackMoverTranslation, 0, component, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ComponentSetMoverScale(u8 component, crExpressionOp* op)
{
	return ComponentSetVector3(kTrackMoverScale, 0, component, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ObjectSpaceGet(u8 track, u16 id, u8 type)
{
	crExpressionOpObjectSpaceGet* ret = rage_new crExpressionOpObjectSpaceGet;
	ret->SetTrack(track);
	ret->SetId(id);
	ret->SetType(type);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ObjectSpaceGetBoneRotation(u16 id)
{
	return ObjectSpaceGet(kTrackBoneRotation, id, kFormatTypeQuaternion);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ObjectSpaceGetBoneTranslation(u16 id)
{
	return ObjectSpaceGet(kTrackBoneTranslation, id, kFormatTypeVector3);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ObjectSpaceSet(u8 track, u16 id, u8 type, crExpressionOp* op)
{
	crExpressionOpObjectSpaceSet* ret = rage_new crExpressionOpObjectSpaceSet;
	ret->SetTrack(track);
	ret->SetId(id);
	ret->SetType(type);
	ret->SetSource(op);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ObjectSpaceSetBoneRotation(u16 id, crExpressionOp* op)
{
	return ObjectSpaceSet(kTrackBoneRotation, id, kFormatTypeQuaternion, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ObjectSpaceSetBoneTranslation(u16 id, crExpressionOp* op)
{
	return ObjectSpaceSet(kTrackBoneTranslation, id, kFormatTypeVector3, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ObjectSpaceConvertTo(u8 track, u16 id, u8 type, crExpressionOp* op)
{
	crExpressionOpObjectSpaceConvertTo* ret = rage_new crExpressionOpObjectSpaceConvertTo;
	ret->SetTrack(track);
	ret->SetId(id);
	ret->SetType(type);
	ret->SetSource(op);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ObjectSpaceConvertToBoneRotation(u16 id, crExpressionOp* op)
{
	return ObjectSpaceConvertTo(kTrackBoneRotation, id, kFormatTypeQuaternion, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ObjectSpaceConvertToBoneTranslation(u16 id, crExpressionOp* op)
{
	return ObjectSpaceConvertTo(kTrackBoneTranslation, id, kFormatTypeVector3, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ObjectSpaceConvertFrom(u8 track, u16 id, u8 type, crExpressionOp* op)
{
	crExpressionOpObjectSpaceConvertFrom* ret = rage_new crExpressionOpObjectSpaceConvertFrom;
	ret->SetTrack(track);
	ret->SetId(id);
	ret->SetType(type);
	ret->SetSource(op);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ObjectSpaceConvertFromBoneRotation(u16 id, crExpressionOp* op)
{
	return ObjectSpaceConvertFrom(kTrackBoneRotation, id, kFormatTypeQuaternion, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* ObjectSpaceConvertFromBoneTranslation(u16 id, crExpressionOp* op)
{
	return ObjectSpaceConvertFrom(kTrackBoneTranslation, id, kFormatTypeVector3, op);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* Nullary(crExpressionOpNullary::eNullaryOpTypes nullaryOpType)
{
	crExpressionOpNullary* ret = rage_new crExpressionOpNullary;
	ret->SetNullaryOpType(nullaryOpType);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* Unary(crExpressionOpUnary::eUnaryOpTypes unaryOpType, crExpressionOp* op)
{
	crExpressionOpUnary* ret = rage_new crExpressionOpUnary;
	ret->SetUnaryOpType(unaryOpType);
	ret->SetSource(op);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* Binary(crExpressionOpBinary::eBinaryOpTypes binaryOpType, crExpressionOp* op0, crExpressionOp* op1)
{
	crExpressionOpBinary* ret = rage_new crExpressionOpBinary;
	ret->SetBinaryOpType(binaryOpType);
	ret->SetSource(0, op0);
	ret->SetSource(1, op1);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* Ternary(crExpressionOpTernary::eTernaryOpTypes ternaryOpType, crExpressionOp* op0, crExpressionOp* op1, crExpressionOp* op2)
{
	crExpressionOpTernary* ret = rage_new crExpressionOpTernary;
	ret->SetTernaryOpType(ternaryOpType);
	ret->SetSource(0, op0);
	ret->SetSource(1, op1);
	ret->SetSource(2, op2);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* Nary(crExpressionOpNary::eNaryOpTypes naryOpType, crExpressionOp* op0, crExpressionOp* op1, crExpressionOp* op2, crExpressionOp* op3, crExpressionOp* op4, crExpressionOp* op5, crExpressionOp* op6, crExpressionOp* op7)
{
	crExpressionOpNary* ret = rage_new crExpressionOpNary;
	ret->SetNaryOpType(naryOpType);
	ret->AppendSource(op0);
	if(op1)
	{
		ret->AppendSource(op1);
		if(op2)
		{
			ret->AppendSource(op2);
			if(op3)
			{
				ret->AppendSource(op3);
				if(op4)
				{
					ret->AppendSource(op4);
					if(op5)
					{
						ret->AppendSource(op5);
						if(op6)
						{
							ret->AppendSource(op6);
							if(op7)
							{
								ret->AppendSource(op7);
							}
						}
					}
				}
			}
		}
	}
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SpecialBlend(crExpressionOp* op0, float weight0, crExpressionOp* op1, float weight1, crExpressionOp* op2, float weight2, crExpressionOp* op3, float weight3, crExpressionOp* op4, float weight4, crExpressionOp* op5, float weight5, crExpressionOp* op6, float weight6, crExpressionOp* op7, float weight7)
{
	crExpressionOpSpecialBlend* ret = rage_new crExpressionOpSpecialBlend;
	ret->AppendSourceWeight(op0, weight0);
	if(op1)
	{
		ret->AppendSourceWeight(op1, weight1);
		if(op2)
		{
			ret->AppendSourceWeight(op2, weight2);
			if(op3)
			{
				ret->AppendSourceWeight(op3, weight3);
				if(op4)
				{
					ret->AppendSourceWeight(op4, weight4);
					if(op5)
					{
						ret->AppendSourceWeight(op5, weight5);
						if(op6)
						{
							ret->AppendSourceWeight(op6, weight6);
							if(op7)
							{
								ret->AppendSourceWeight(op7, weight7);
							}
						}
					}
				}
			}
		}
	}
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SpecialCurve(float in0, float out0, float in1, float out1, crExpressionOp* op)
{
	crExpressionOpSpecialCurve* ret = rage_new crExpressionOpSpecialCurve;
	ret->AppendKey(in0, out0);
	ret->AppendKey(in1, out1);
	ret->SetSource(op);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SpecialCurve(float in0, float out0, float in1, float out1, float in2, float out2, crExpressionOp* op)
{
	crExpressionOpSpecialCurve* ret = rage_new crExpressionOpSpecialCurve;
	ret->AppendKey(in0, out0);
	ret->AppendKey(in1, out1);
	ret->AppendKey(in2, out2);
	ret->SetSource(op);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SpecialLookAt(u8 axisLookAt, u8 axisLookAtUp, u8 axisOriginPos, QuatV_In offset, crExpressionOp* opLookAtPos, crExpressionOp* opLookAtRot, crExpressionOp* opOriginPos)
{
	crExpressionOpSpecialLookAt* ret = rage_new crExpressionOpSpecialLookAt;
	ret->SetLookAtAxis(axisLookAt);
	ret->SetLookAtUpAxis(axisLookAtUp);
	ret->SetOriginUpAxis(axisOriginPos);
	ret->SetOffset(offset);
	ret->SetLookAtPosSource(opLookAtPos);
	ret->SetLookAtRotSource(opLookAtRot);
	ret->SetOriginPosSource(opOriginPos);
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace crMakeExpressionOp

} // namespace rage

#endif // CR_DEV