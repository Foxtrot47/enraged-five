//
// crextra/expressionops.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CREXTRA_EXPRESSIONOPS_H
#define CREXTRA_EXPRESSIONOPS_H

#include "cranimation/animation_config.h"

#if CR_DEV
#include "atl/array.h"
#include "cranimation/framedata.h"
#include "creature/componentphysical.h"
#include "data/resource.h"
#include "vectormath/classes.h"

#define OPTIMIZE_CULL_SET_OPERATIONS (0)

namespace rage
{

class crCreature;
class crFrame;
class crFrameFilter;
class crSkeletonData;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Base expression operation class
class crExpressionOp
{
public:

	struct Value;
	struct ProcessHelper;
	struct SerializeHelper;
	struct DumpHelper;

	// PURPOSE: Enumeration of different types of expression operations
	enum eOpTypes
	{
		kOpTypeNone,
		
		kOpTypeConstant,
		kOpTypeConstantFloat,

		kOpTypeGet,
		kOpTypeSet,
		kOpTypeValid,
		kOpTypeComponentGet,
		kOpTypeComponentSet,
		kOpTypeObjectSpaceGet,
		kOpTypeObjectSpaceSet,
		kOpTypeObjectSpaceConvertTo,
		kOpTypeObjectSpaceConvertFrom,

		kOpTypeNullary,
		kOpTypeUnary,
		kOpTypeBinary,
		kOpTypeTernary,
		kOpTypeNary,

		kOpTypeSpecialBlend,
		kOpTypeDeprecated,
		kOpTypeSpecialCurve,
		kOpTypeSpecialLookAt,
		kOpTypeMotion,
		kOpTypeVariableGet,
		kOpTypeVariableSet,

		kOpTypeSpecialLinear,

		// must be last in list
		kOpTypeNum,
	};


	// PURPOSE: Default constructor
	crExpressionOp(eOpTypes opType=kOpTypeNone);

	// PURPOSE: Resource constructor
	crExpressionOp(datResource&);

	// PURPOSE: Clone expression op
	virtual crExpressionOp* Clone() const = 0;

	// PURPOSE: Allocate new expression op
	// PARAMS: opType - type of expression op to create
	static crExpressionOp* CreateExpressionOp(eOpTypes opType);

	// PURPOSE: Placement
	DECLARE_PLACE(crExpressionOp);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Fixes up derived type from pointer to base class only
	static void VirtualConstructFromPtr(datResource&, crExpressionOp* base);

	// PURPOSE: Destructor
	virtual ~crExpressionOp();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Get expression op type (see crExpressionOp::eOptypes for enumeration)
	eOpTypes GetOpType() const;


	// PURPOSE: Get number of children
	virtual u32 GetNumChildren() const;

	// PURPOSE: Get child by index (const)
	virtual const crExpressionOp* GetChild(u32) const;

	// PURPOSE: Get child by index (non-const)
	virtual crExpressionOp* GetChild(u32);

	// PURPOSE: Set child by index
	virtual void SetChild(u32, crExpressionOp&);


	// PURPOSE: Process the expression operation	
	virtual void Process(ProcessHelper&) const = 0;

	// PURPOSE: Serialize the expression operation
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump out the expression operation
	virtual void Dump(DumpHelper&) const;

	// PURPOSE: Initialize expression ops
	static void InitClass();

	// PURPOSE: Shutdown the expression op registry
	static void ShutdownClass();


	// PURPOSE: Definition of expression op create call
	typedef crExpressionOp* ExpressionOpCreateFn();

	// PURPOSE: Definition of expression op placement call
	typedef void ExpressionOpPlaceFn(crExpressionOp*);

	// PURPOSE: Definition of expression op resource placement call
	typedef void ExpressionOpPlaceRscFn(crExpressionOp*, datResource&);

	// PURPOSE: Expression op type info, used to register expression op types
	class ExpressionOpTypeInfo
	{
	public:

		// PURPOSE: Constructor
		// PARAMS: opType - expression op type identifier (see crExpressionOp::eOpTypes enumeration)
		// opName - expression op name string
		// createFn - expression op creation call
		// placeFn - expression op placement call
		// placeRscFn - expression op resource placement call
		// size - sizeof expression op
		ExpressionOpTypeInfo(eOpTypes opType, const char* opName, ExpressionOpCreateFn* createFn, ExpressionOpPlaceFn* placeFn, ExpressionOpPlaceRscFn* placeRscFn, size_t size);

		// PURPOSE: Destructor
		~ExpressionOpTypeInfo();

		// PURPOSE: Registration call
		void Register() const;

		// PURPOSE: Get expression op type identifier
		// RETURNS: Expression op type identifier (see crExpressionOp::eOpTypes enumeration)
		eOpTypes GetOpType() const;

		// PURPOSE: Get expression op name string
		// RETURNS: String of expression op's type name
		const char* GetOpName() const;

		// PURPOSE: Get expression op creation function
		// RETURNS: Expression op creation function pointer
		ExpressionOpCreateFn* GetCreateFn() const;

		// PURPOSE: Get expression op placement function
		// RETURNS: Expression op placement function pointer
		ExpressionOpPlaceFn* GetPlaceFn() const;

		// PURPOSE: Get expression op resource placement function
		// RETURNS: Expression op resource placement function pointer
		ExpressionOpPlaceRscFn* GetPlaceRscFn() const;

		// PURPOSE: Get expression op size (as returned by sizeof)
		// RETURNS: sizeof expression op
		size_t GetSize() const;

	private:
		eOpTypes m_OpType;
		const char* m_Name;
		ExpressionOpCreateFn* m_CreateFn;
		ExpressionOpPlaceFn* m_PlaceFn;
		ExpressionOpPlaceRscFn* m_PlaceRscFn;
		size_t m_Size;
	};

	// PURPOSE: Get info about a expression op type
	// PARAMS: opType - expression op type identifier (see crExpressionOp::eOpTypes enumeration)
	// RETURNS: const pointer to expression op type info structure (may be NULL if op type unknown)
	static const ExpressionOpTypeInfo* FindExpressionOpTypeInfo(eOpTypes opType);

	// PURPOSE: Get info about this expression op
	// RETURNS: const reference to expression op type info structure about this expression op
	virtual const ExpressionOpTypeInfo& GetExpressionOpTypeInfo() const = 0;


	// PURPOSE: Declare functions necessary to register an expression op type
	// Registers expression op in central expression op type registry
	#define CR_DECLARE_EXPRESSION_OP_TYPE(__typename) \
		static crExpressionOp* CreateExpressionOp(); \
		static void PlaceExpressionOp(rage::crExpressionOp* expressionOp); \
		static void PlaceRscExpressionOp(rage::crExpressionOp* expressionOp, rage::datResource& rsc); \
		static void InitClass(); \
		virtual crExpressionOp* Clone() const; \
		virtual const rage::crExpressionOp::ExpressionOpTypeInfo& GetExpressionOpTypeInfo() const; \
		static const rage::crExpressionOp::ExpressionOpTypeInfo sm_ExpressionOpTypeInfo;

	// PURPOSE: Implement functions necessary to register an expression op type
	#define CR_IMPLEMENT_EXPRESSION_OP_TYPE(__typename, __typeid) \
		rage::crExpressionOp* __typename::CreateExpressionOp() \
		{ \
			__typename* expressionOp = rage_new __typename; \
			return expressionOp; \
		} \
		void __typename::PlaceExpressionOp(rage::crExpressionOp* expressionOp) \
		{ \
			::new (expressionOp) __typename; \
		} \
		void __typename::PlaceRscExpressionOp(rage::crExpressionOp* expressionOp, rage::datResource& rsc) \
		{ \
			::new (expressionOp) __typename(rsc); \
		} \
		void __typename::InitClass() \
		{ \
			sm_ExpressionOpTypeInfo.Register(); \
		} \
		rage::crExpressionOp* __typename::Clone() const \
		{ \
			__typename* expressionOp = rage_new __typename(*this); \
			return expressionOp; \
		} \
		const rage::crExpressionOp::ExpressionOpTypeInfo& __typename::GetExpressionOpTypeInfo() const \
		{ \
			return sm_ExpressionOpTypeInfo; \
		} \
		const rage::crExpressionOp::ExpressionOpTypeInfo __typename::sm_ExpressionOpTypeInfo(rage::crExpressionOp::eOpTypes(__typeid), #__typename, CreateExpressionOp, PlaceExpressionOp, PlaceRscExpressionOp, sizeof(__typename));


	// PURPOSE: An expression value, used to pass values between expression operations
	struct ALIGNAS(16) Value
	{
		// PURPOSE: Default constructor
		Value();

		// PURPOSE: Initializing constructor
		Value(Vec3V_In);

		// PURPOSE: Initializing constructor
		Value(QuatV_In);

		// PURPOSE: Initializing constructor
		Value(float);

		// PURPOSE: Initializing constructor
		Value(s32);

		// PURPOSE: Resource constructor
		Value(datResource&);

		// PURPOSE: Placement
		IMPLEMENT_PLACE_INLINE(Value);

		// PURPOSE: Declare structure
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

		// PURPOSE: Destructor
		~Value();

		// PURPOSE: Set from vector
		void Set(Vec3V_In);

		// PURPOSE: Set from quaternion
		void Set(QuatV_In);

		// PURPOSE: Set from float
		void Set(float);

		// PURPOSE: Set from int
		void Set(s32);

		// PURPOSE: Get by return to vector (non-const)
		Vec3V_Ref GetVector();

		// PURPOSE: Get by return to quaternion (non-const)
		QuatV_Ref GetQuaternion();

		// PURPOSE: Get by return to float (non-const)
		float& GetFloat();

		// PURPOSE: Get by return to int (non-const)
		s32& GetInt();

		// PURPOSE: Assignment operator
		Value& operator=(const Value& src);

		// PURPOSE: Equality operator
		bool operator==(const Value& src) const;

		// PURPOSE: Non-equality operator
		bool operator!=(const Value& src) const;

		// PURPOSE: Dump output
		void Dump(DumpHelper&) const;

		// PURPOSE: Serialize
		void Serialize(SerializeHelper&);

		// PURPOSE: Zero
		void Zero();

	private:
		float m_Values[4];
		// TODO --- probably going to need sense of type here
	} ;


	// PURPOSE: Const iterator
	struct ConstIterator
	{
		// PURPOSE: Default constructor
		ConstIterator();

		// PURPOSE: Destructor
		virtual ~ConstIterator();

		// PURPOSE: Visit operation, override to implement specialized iteration
		// NOTE: Failure to call this base implementation will prevent visits to
		// any child operations.
		virtual void Visit(const crExpressionOp&);
	};


	// PURPOSE: Modifying iterator
	struct ModifyingIterator
	{
		// PURPOSE: Default constructor
		ModifyingIterator();

		// PURPOSE: Destructor
		virtual ~ModifyingIterator();

		// PURPOSE: Visit operation, override to implement specialized iteration
		// RETURNS: Current operation for no change, or new operation if modified
		// NOTE: Failure to call this base implementation will prevent visits to
		// any child operations.
		virtual crExpressionOp* Visit(crExpressionOp&);
	};


	// PURPOSE: Helps with processing the expression operations
	struct ProcessHelper
	{
		// PURPOSE: Constructor
		ProcessHelper();

		// PURPOSE: Push value onto stack
		Value& PushValue();

		// PURPOSE: Pop value off of stack
		Value& PopValue();

		// PURPOSE: Top value from the stack
		Value& TopValue();



		crFrame* m_Frame;
		crFrameFilter* m_Filter;
		float m_Time;
		float m_DeltaTime;
		crCreature* m_Creature;
		const crSkeletonData* m_SkeletonData;

		Value* m_Values;
		int m_NumValues;
		int m_Top;

		Value* m_Variables;
		u32 m_NumVariables;
	};

	// PURPOSE: Helps with serializing the expression operations
	struct SerializeHelper
	{
		// PURPOSE:
		SerializeHelper(datSerialize& s, int version);

		// PUPROSE:
		void operator<<(datOwner<crExpressionOp>& op);

		// PURPOSE:
		operator datSerialize&();

		// PURPOSE:
		bool IsRead() const;

		datSerialize* m_Serialize;
		int m_Version;
	};

	// PURPOSE: Helps with dumping the output of the expression operations
	struct DumpHelper : public ConstIterator
	{
		// PURPOSE: Default constructor
		DumpHelper(int verbosity=2, char* outputBuffer=NULL, u32 outputBufferSize=0);

		// PURPOSE: Destructor
		virtual ~DumpHelper();

		// PURPOSE: Output text (will be properly indented automatically)
		// PARAM: verbosity - verbosity level [0..3]
		void Outputf(int verbosity, const char* fmt, ...);

		// PURPOSE: Push indent
		void PushIndent();

		// PURPOSE: Pop indent
		void PopIndent();

		// PURPOSE: Visit expression operation override
		virtual void Visit(const crExpressionOp&);

	private:

		// PURPOSE: Internal call
		void InternalOutputf(const char* fmt, va_list& args);

		// PURPOSE: Internal call, route to direct output or internal buffer
		void InternalPrintf(const char* fmt, ...);

	private:
		int m_Verbosity;
		char* m_OutputBuffer;
		u32 m_OutputBufferSize;

		int m_Indent;
		bool m_FirstLine;	
	};
	
	// PURPOSE: Search for input and/or output operations
	template <bool input, bool output>
	struct InputOutputIterator : public ConstIterator
	{
		// PURPOSE: Default constructor
		InputOutputIterator();

		// PURPOSE: Destructor
		virtual ~InputOutputIterator();

		// PURPOSE: Visit expression operation override
		virtual void Visit(const crExpressionOp&);

		// PURPOSE: Input/output callback, override to implement, return true to abandon search
		virtual bool Callback(u8 track, u16 id, u8 type, bool isInput) = 0;

		// PURPOSE: Found target operation, abandons search early
		bool m_Found;
	};

	struct PhysicalIterator : public ConstIterator
	{
		// PURPOSE: Visit expression operation override
		virtual void Visit(const crExpressionOp&);
	};

protected:

	// PURPOSE: Get expression op sub type (use defined by derived op)
	u16 GetOpSubType() const;

	// PURPOSE: Get expression op sub type (use defined by derived op)
	void SetOpSubType(u16);

private:

	// PURPOSE: Register a new expression op type (only call from ExpressionOpTypeInfo::Register)
	// PARAMS: expression op type info (must be global/class static, persist for entire execution)
	static void RegisterExpressionOpTypeInfo(const ExpressionOpTypeInfo& info);

	// PURPOSE: Expression op type info registry
	static atArray<const ExpressionOpTypeInfo*> sm_ExpressionOpTypeInfos;

	// PURPOSE: Class initialization
	static bool sm_InitClassCalled;

private:
	u16 m_OpType;
	u16 m_OpSubType;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Constant operation (any type)
class crExpressionOpConstant : public crExpressionOp
{
public:

	// PURPOSE: Default constructor
	crExpressionOpConstant();

	// PURPOSE: Initializing constructor
	crExpressionOpConstant(const Value&);

	// PURPOSE: Resource constructor
	crExpressionOpConstant(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpConstant();

	// PURPOSE: Initializer
	void Init(const Value&);

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpConstant);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;

	// PURPOSE: Get constant value (const)
	const Value& GetValue() const;

	// PURPOSE: Get constant value (non-const)
	Value& GetValue();

	// PURPOSE: Set constant value
	void SetValue(const Value&);


private:
	datPadding<8> m_Padding;
	Value m_Value;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Constant operation, float only
class crExpressionOpConstantFloat : public crExpressionOp
{
public:

	// PURPOSE: Default constructor
	crExpressionOpConstantFloat();

	// PURPOSE: Initializing constructor
	crExpressionOpConstantFloat(float);

	// PURPOSE: Resource constructor
	crExpressionOpConstantFloat(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpConstantFloat();

	// PURPOSE: Initializer
	void Init(float);

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpConstantFloat);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;

	// PURPOSE: Get constant float
	float GetFloat() const;

	// PURPOSE: Set constant float
	void SetFloat(float);


private:
	float m_Float;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Base get/set operation
class crExpressionOpGetSet : public crExpressionOp
{
public:

	// PURPOSE: Constructor
	crExpressionOpGetSet(eOpTypes opType=kOpTypeNone);

	// PURPOSE: Copy constructor
	crExpressionOpGetSet(const crExpressionOpGetSet&);

	// PURPOSE: Resource constructor
	crExpressionOpGetSet(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpGetSet();

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;
	
	// PURPOSE: Get track
	u8 GetTrack() const;

	// PURPOSE: Set track
	void SetTrack(u8);

	// PURPOSE: Get type
	u8 GetType() const;

	// PURPOSE: Set type
	void SetType(u8);

	// PURPOSE: Get id
	u16 GetId() const;

	// PURPOSE: Set id
	void SetId(u16);

	// PURPOSE: Get accelerated index (used to increase dof get/set performance)
	int GetAcceleratedIndex() const;

	// PURPOSE: Set accelerated index (used to increase dof get/set performance)
	void SetAcceleratedIndex(int idx);

	// PURPOSE: Does operation get/set value relative to default (translation/rotation only)
	bool IsRelative() const;

	// PURPOSE: Does operation get/set value relative to default (translation/rotation only)
	void SetRelative(bool relative);

	// PURPOSE: Does operation return default rather than current value
	bool IsForceDefault() const;

	// PURPOSE: Force operation to return default rather than current value
	void SetForceDefault(bool force);


	// PURPOSE: Get component
	u8 GetComponent() const;

	// PURPOSE: Set component
	void SetComponent(u8);

	// PURPOSE: Get order
	EulerAngleOrder GetOrder() const;

	// PURPOSE: Set order
	void SetOrder(u8);

protected:

	// PURPOSE: Get the default value of this dof
	bool GetDefaultValue(ProcessHelper& ph, Value& outValue, bool relative=false) const;

	// PURPOSE: Apply or remove default rotation
	void ApplyDefaultQuaternion(QuatV_InOut inoutRotation, ProcessHelper& ph, bool stripNotApply=false) const;

	// PURPOSE: Apply or remove default translation
	void ApplyDefaultVector3(Vec3V_InOut inoutVector3, ProcessHelper& ph, bool subtractNotAdd=false) const;

protected:
	u8 m_Track;
	u8 m_Type;
	u16 m_Id;
	s32 m_AccIdx;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Get whole dof operation
class crExpressionOpGet : public crExpressionOpGetSet
{
public:

	// PURPOSE: Default constructor
	crExpressionOpGet();

	// PURPOSE: Constructor
	crExpressionOpGet(eOpTypes opType);

	// PURPOSE: Copy constructor
	crExpressionOpGet(const crExpressionOpGet&);

	// PURPOSE: Resource constructor
	crExpressionOpGet(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpGet();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpGet);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Set whole dof operation
class crExpressionOpSet : public crExpressionOpGetSet
{
public:

	// PURPOSE: Default constructor
	crExpressionOpSet();

	// PURPOSE: Copy constructor
	crExpressionOpSet(const crExpressionOpSet&);

	// PURPOSE: Resource constructor
	crExpressionOpSet(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpSet();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpSet);

	// PURPOSE: Get number of children
	virtual u32 GetNumChildren() const;

	// PURPOSE: Get child by index (const)
	virtual const crExpressionOp* GetChild(u32) const;

	// PURPOSE: Get child by index (non-const)
	virtual crExpressionOp* GetChild(u32);

	// PURPOSE: Set child by index
	virtual void SetChild(u32, crExpressionOp&);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Get source (can be NULL)
	crExpressionOp* GetSource() const;

	// PURPOSE: Set source (can be NULL)
	void SetSource(crExpressionOp*);


private:
	datOwner<crExpressionOp> m_Source;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Check if DOF is valid operation
class crExpressionOpValid : public crExpressionOpGetSet
{
public:

	// PURPOSE: Default constructor
	crExpressionOpValid();

	// PURPOSE: Constructor
	crExpressionOpValid(eOpTypes opType);

	// PURPOSE: Resource constructor
	crExpressionOpValid(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpValid();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpValid);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Base component get/set operation
class crExpressionOpComponentGetSet : public crExpressionOpGetSet
{
public:

	// PURPOSE: Default constructor
	crExpressionOpComponentGetSet(eOpTypes=kOpTypeNone);

	// PURPOSE: Resource constructor
	crExpressionOpComponentGetSet(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpComponentGetSet();

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;

protected:

	// PURPOSE: Get the default component value of this dof
	bool GetDefaultComponentValue(ProcessHelper& ph, Value& outValue, bool isRelative=false) const;

protected:
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Get dof component operation
class crExpressionOpComponentGet : public crExpressionOpComponentGetSet
{
public:

	// PURPOSE: Default constructor
	crExpressionOpComponentGet();

	// PURPOSE: Copy constructor
	crExpressionOpComponentGet(const crExpressionOpComponentGet&);

	// PURPOSE: Resource constructor
	crExpressionOpComponentGet(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpComponentGet();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpComponentGet);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Set dof component operation
class crExpressionOpComponentSet : public crExpressionOpComponentGetSet
{
public:

	// PURPOSE: Default constructor
	crExpressionOpComponentSet();

	// PURPOSE: Copy constructor
	crExpressionOpComponentSet(const crExpressionOpComponentSet&);

	// PURPOSE: Resource constructor
	crExpressionOpComponentSet(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpComponentSet();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpComponentSet);

	// PURPOSE: Get number of children
	virtual u32 GetNumChildren() const;

	// PURPOSE: Get child by index (const)
	virtual const crExpressionOp* GetChild(u32) const;

	// PURPOSE: Get child by index (non-const)
	virtual crExpressionOp* GetChild(u32);

	// PURPOSE: Set child by index
	virtual void SetChild(u32, crExpressionOp&);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Get source (can be NULL)
	crExpressionOp* GetSource() const;

	// PURPOSE: Set source (can be NULL)
	void SetSource(crExpressionOp*);

protected:

	// PURPOSE: Get the default value of this dof as a vector
	bool GetDefaultVector3(ProcessHelper& ph, Vec3V_InOut v) const;

	// PURPOSE: Get the default value of this dof as a quaternion
	bool GetDefaultQuaternion(ProcessHelper& ph, QuatV_InOut q) const;

private:
	datOwner<crExpressionOp> m_Source;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Get object space operation
class crExpressionOpObjectSpaceGet : public crExpressionOpGet
{
protected:

	// PURPOSE: Base constructor
	crExpressionOpObjectSpaceGet(eOpTypes);

public:

	// PURPOSE: Default constructor
	crExpressionOpObjectSpaceGet();

	// PURPOSE: Resource constructor
	crExpressionOpObjectSpaceGet(datResource&);

	// PURPOSE: Destructor
	virtual ~crExpressionOpObjectSpaceGet();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpObjectSpaceGet);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

protected:

	// PURPOSE: Calculate the object space matrix
	void CalcObjectSpaceMatrix(ProcessHelper& ph, TransformV_InOut) const;

	// PURPPOSE: Calculate the parent object space matrix
	void CalcParentObjectSpaceMatrix(ProcessHelper& ph, TransformV_InOut) const;

};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:  Set object space operation
class crExpressionOpObjectSpaceSet : public crExpressionOpObjectSpaceGet
{
protected:

	// PURPOSE: Base constructor
	crExpressionOpObjectSpaceSet(eOpTypes);

public:

	// PURPOSE: Default constructor
	crExpressionOpObjectSpaceSet();

	// PURPOSE: Copy constructor
	crExpressionOpObjectSpaceSet(const crExpressionOpObjectSpaceSet&);

	// PURPOSE: Resource constructor
	crExpressionOpObjectSpaceSet(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpObjectSpaceSet();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpObjectSpaceSet);


	// PURPOSE: Get number of children
	virtual u32 GetNumChildren() const;

	// PURPOSE: Get child by index (const)
	virtual const crExpressionOp* GetChild(u32) const;

	// PURPOSE: Get child by index (non-const)
	virtual crExpressionOp* GetChild(u32);

	// PURPOSE: Set child by index
	virtual void SetChild(u32, crExpressionOp&);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Get source (can be NULL)
	crExpressionOp* GetSource() const;

	// PURPOSE: Set source (can be NULL)
	void SetSource(crExpressionOp*);


protected:
	datOwner<crExpressionOp> m_Source;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Convert from local space to object space operation
class crExpressionOpObjectSpaceConvertTo : public crExpressionOpObjectSpaceSet
{
public:

	// PURPOSE: Default constructor
	crExpressionOpObjectSpaceConvertTo();

	// PURPOSE: Resource constructor
	crExpressionOpObjectSpaceConvertTo(datResource&);

	// PURPOSE: Destructor
	virtual ~crExpressionOpObjectSpaceConvertTo();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpObjectSpaceConvertTo);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Convert from object space to local space operation
class crExpressionOpObjectSpaceConvertFrom : public crExpressionOpObjectSpaceSet
{
public:

	// PURPOSE: Default constructor
	crExpressionOpObjectSpaceConvertFrom();

	// PURPOSE: Resource constructor
	crExpressionOpObjectSpaceConvertFrom(datResource&);

	// PURPOSE: Destructor
	virtual ~crExpressionOpObjectSpaceConvertFrom();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpObjectSpaceConvertFrom);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;
};

////////////////////////////////////////////////////////////////////////////////

class crExpressionOpMotion : public crExpressionOp
{
public:

	// PURPOSE: Default constructor
	crExpressionOpMotion();

	// PURPOSE: Resource constructor
	crExpressionOpMotion(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpMotion);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;

	int m_AccRotIdx;
	int m_AccTransIdx;
	crMotionDescription m_Descr;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Nullary operations
class crExpressionOpNullary : public crExpressionOp
{
public:

	// PURPOSE: Enumeration of nullary operations
	enum eNullaryOpTypes
	{
		kNullaryOpTypeNone,

		kNullaryOpTypeZero,
		kNullaryOpTypeOne,
		kNullaryOpTypePi,
		kNullaryOpTypeTime,
		kNullaryOpTypeRandom,

		kNullaryOpTypeVectorZero,
		kNullaryOpTypeVectorOne,

		kNullaryOpTypeDeltaTime, 

		kNullaryOpTypeQuatIdentity,

		// must be last in list
		kNullaryOpTypeNum,
	};

	// PURPOSE: Default constructor
	crExpressionOpNullary();

	// PURPOSE: Resource constructor
	crExpressionOpNullary(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpNullary();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpNullary);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;

	// PURPOSE: Get nullary operation type
	eNullaryOpTypes GetNullaryOpType() const;

	// PURPOSE: Set nullary operation type
	void SetNullaryOpType(eNullaryOpTypes);


private:
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Unary operations
class crExpressionOpUnary : public crExpressionOp
{
public:

	// PURPOSE: Enumeration of unary operations
	enum eUnaryOpTypes
	{
		kUnaryOpTypeNone,

		kUnaryOpTypeLogicalNot,

		// TODO - ADD BITWISE complement

		kUnaryOpTypeNegate,
		kUnaryOpTypeReciprocal,
		kUnaryOpTypeSquare,
		kUnaryOpTypeSqrt,
		kUnaryOpTypeAbsolute,
		kUnaryOpTypeFloor,
		kUnaryOpTypeCeil,
		kUnaryOpTypeLog,
		kUnaryOpTypeLn,
		kUnaryOpTypeExp,
		kUnaryOpTypeClamp01,

		kUnaryOpTypeCos,
		kUnaryOpTypeSin,
		kUnaryOpTypeTan,
		kUnaryOpTypeArcCos,
		kUnaryOpTypeArcSin,
		kUnaryOpTypeArcTan,
		kUnaryOpTypeCosH,
		kUnaryOpTypeSinH,
		kUnaryOpTypeTanH,

		kUnaryOpTypeDegreesToRadians,
		kUnaryOpTypeRadiansToDegrees,

		kUnaryOpTypeFromEuler,
		kUnaryOpTypeToEuler,

		kUnaryOpTypeSplat,

		kUnaryOpTypeVectorClamp01,

		kUnaryOpTypeQuatInverse,

		// TODO --- add vector ops - normalize, magnitude

		// must be last in list
		kUnaryOpTypeNum,
	};

	// PURPOSE: Default constructor
	crExpressionOpUnary();

	// PURPOSE: Copy constructor
	crExpressionOpUnary(const crExpressionOpUnary&);

	// PURPOSE: Resource constructor
	crExpressionOpUnary(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpUnary();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpUnary);

	// PURPOSE: Get number of children
	virtual u32 GetNumChildren() const;

	// PURPOSE: Get child by index (const)
	virtual const crExpressionOp* GetChild(u32) const;

	// PURPOSE: Get child by index (non-const)
	virtual crExpressionOp* GetChild(u32);

	// PURPOSE: Set child by index
	virtual void SetChild(u32, crExpressionOp&);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;

	// PURPOSE: Get unary operation type
	eUnaryOpTypes GetUnaryOpType() const;

	// PURPOSE: Set unary operation type
	void SetUnaryOpType(eUnaryOpTypes);

	// PURPOSE: Get source (can be NULL)
	crExpressionOp* GetSource() const;

	// PURPOSE: Set source (can be NULL)
	void SetSource(crExpressionOp*);


private:
	datOwner<crExpressionOp> m_Source;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Binary operations
class crExpressionOpBinary : public crExpressionOp
{
public:

	// PURPOSE: Enumeration of binary operations
	enum eBinaryOpTypes
	{
		kBinaryOpTypeNone,

		kBinaryOpTypeEqualTo,
		kBinaryOpTypeNotEqualTo,
		kBinaryOpTypeGreaterThan,
		kBinaryOpTypeLessThan,
		kBinaryOpTypeGreaterThanEqualTo,
		kBinaryOpTypeLessThanEqualTo,

		kBinaryOpTypeAdd,
		kBinaryOpTypeSubtract,
		kBinaryOpTypeMultiply,
		kBinaryOpTypeDivide,
		kBinaryOpTypeModulus,
		kBinaryOpTypeExponent,

		kBinaryOpTypeMax,
		kBinaryOpTypeMin,

		kBinaryOpTypeLogicalAnd,
		kBinaryOpTypeLogicalOr,
		kBinaryOpTypeLogicalXor,

		kBinaryOpTypeQuatMultiply,

		kBinaryOpTypeVectorAdd,
		kBinaryOpTypeVectorMultiply,
		kBinaryOpTypeVectorTransform,
		kBinaryOpTypeVectorSubtract,
		kBinaryOpTypeQuatScale,
		// kBinaryOpTypeVectorUnTransform ?

		// TODO - ADD BITWISE and, or, xor, bitshifts

		// TODO - ADD VECTOR OPS dotproduct, crossproduct VECTORIZED VERSIONS OF BINARY TYPES

		// must be last in list
		kBinaryOpTypeNum,
	};


	// PURPOSE: Default constructor
	crExpressionOpBinary();

	// PURPOSE: Copy constructor
	crExpressionOpBinary(const crExpressionOpBinary&);

	// PURPOSE: Resource constructor
	crExpressionOpBinary(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpBinary();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpBinary);

	// PURPOSE: Get number of children
	virtual u32 GetNumChildren() const;

	// PURPOSE: Get child by index (const)
	virtual const crExpressionOp* GetChild(u32) const;

	// PURPOSE: Get child by index (non-const)
	virtual crExpressionOp* GetChild(u32);

	// PURPOSE: Set child by index
	virtual void SetChild(u32, crExpressionOp&);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;

	// PURPOSE: Get binary operation type
	eBinaryOpTypes GetBinaryOpType() const;

	// PURPOSE: Set binary operation type
	void SetBinaryOpType(eBinaryOpTypes);

	// PURPOSE: Get source by index (can be NULL)
	crExpressionOp* GetSource(int) const;

	// PURPOSE: Set source by index (can be NULL)
	void SetSource(int, crExpressionOp*);


private:
	atRangeArray< datOwner<crExpressionOp>, 2> m_Sources;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Ternary operations
class crExpressionOpTernary : public crExpressionOp
{
public:

	// PURPOSE: Enumeration of ternary operations
	enum eTernaryOpTypes
	{
		kTernaryOpTypeNone,

		kTernaryOpTypeMultiplyAdd,

		kTernaryOpTypeClamp,
		kTernaryOpTypeLerp,

		kTernaryOpTypeToQuaternion,
		kTernaryOpTypeToVector,

		kTernaryOpTypeConditional,

		kTernaryOpTypeVectorMultiplyAdd,
		kTernaryOpTypeVectorLerpVector,

		kTernaryOpTypeQuaternionLerp,

		// must be last in list
		kTernaryOpTypeNum,
	};

	// PURPOSE: Default constructor
	crExpressionOpTernary();

	// PURPOSE: Copy constructor
	crExpressionOpTernary(const crExpressionOpTernary&);

	// PURPOSE: Resource constructor
	crExpressionOpTernary(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpTernary();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpTernary);

	// PURPOSE: Get number of children
	virtual u32 GetNumChildren() const;

	// PURPOSE: Get child by index (const)
	virtual const crExpressionOp* GetChild(u32) const;

	// PURPOSE: Get child by index (non-const)
	virtual crExpressionOp* GetChild(u32);

	// PURPOSE: Set child by index
	virtual void SetChild(u32, crExpressionOp&);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;

	// PURPOSE: Get ternary operation type
	eTernaryOpTypes GetTernaryOpType() const;

	// PURPOSE: Set ternary operation type
	void SetTernaryOpType(eTernaryOpTypes);

	// PURPOSE: Get source by index (can be NULL)
	crExpressionOp* GetSource(int) const;

	// PURPOSE: Set source by index (can be NULL)
	void SetSource(int, crExpressionOp*);


private:
	atRangeArray< datOwner<crExpressionOp>, 3> m_Sources;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: N-ary operations
class crExpressionOpNary : public crExpressionOp
{
public:

	// PURPOSE: Enumeration of nary operations
	enum eNaryOpTypes
	{
		kNaryOpTypeNone,

		kNaryOpTypeComma,
		kNaryOpTypeSum,
		kNaryOpTypeList,

		kNaryOpTypeLogicalAnd,
		kNaryOpTypeLogicalOr,

		// must be last in list
		kNaryOpTypeNum,
	};

	// PURPOSE: Default constructor
	crExpressionOpNary();

	// PURPOSE: Copy constructor
	crExpressionOpNary(const crExpressionOpNary&);

	// PURPOSE: Resource constructor
	crExpressionOpNary(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpNary();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpNary);

	// PURPOSE: Get number of children
	virtual u32 GetNumChildren() const;

	// PURPOSE: Get child by index (const)
	virtual const crExpressionOp* GetChild(u32) const;

	// PURPOSE: Get child by index (non-const)
	virtual crExpressionOp* GetChild(u32);

	// PURPOSE: Set child by index
	virtual void SetChild(u32, crExpressionOp&);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;

	// PURPOSE: Get nary operation type
	eNaryOpTypes GetNaryOpType() const;

	// PURPOSE: Set nary operation type
	void SetNaryOpType(eNaryOpTypes);

	// PURPOSE: Get number of sources
	int GetNumSources() const;

	// PURPOSE: Set number of sources
	void SetNumSources(int);

	// PURPOSE: Get source by index (can be NULL)
	crExpressionOp* GetSource(int) const;

	// PURPOSE: Set source by index (can be NULL)
	void SetSource(int, crExpressionOp*);

	// PURPOSE: Append a source (can be NULL)
	void AppendSource(crExpressionOp*);

	// PURPOSE: Remove a source
	void RemoveSource(int);

private:
	atArray< datOwner<crExpressionOp> > m_Sources;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Special blend operation
class crExpressionOpSpecialBlend : public crExpressionOp
{
public:

	// PURPOSE: Default constructor
	crExpressionOpSpecialBlend();

	// PURPOSE: Copy constructor
	crExpressionOpSpecialBlend(const crExpressionOpSpecialBlend&);

	// PURPOSE: Resource constructor
	crExpressionOpSpecialBlend(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpSpecialBlend();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpSpecialBlend);

	// PURPOSE: Get number of children
	virtual u32 GetNumChildren() const;

	// PURPOSE: Get child by index (const)
	virtual const crExpressionOp* GetChild(u32) const;

	// PURPOSE: Get child by index (non-const)
	virtual crExpressionOp* GetChild(u32);

	// PURPOSE: Set child by index
	virtual void SetChild(u32, crExpressionOp&);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;

	// PURPOSE: Blend mode
	enum eBlendMode
	{
		kBlendModeFloat = 0x0,
		kBlendModeVector,
		kBlendModeQuaternion,

		kBlendModeMask = 0x03,
	};

	// PURPOSE: Get blend mode
	// RETURNS: Current blend mode (float/vector/quaternion)
	eBlendMode GetBlendMode() const;

	// PURPOSE: Set blend mode
	// PARAMS: blendMode - float/vector/quaternion
	void SetBlendMode(eBlendMode);


	// PURPOSE: Get number of source weights
	int GetNumSourceWeights() const;

	// PURPOSE: Set number of source weights
	void SetNumSourceWeights(int);

	// PURPOSE: Get source by index (can be NULL)
	crExpressionOp* GetSource(int) const;

	// PURPOSE: Set source by index (can be NULL)
	void SetSource(int, crExpressionOp*);

	// PURPOSE: Get weight by index
	float GetWeight(int) const;

	// PURPOSE: Set weight by index
	void SetWeight(int, float);

	// PURPOSE: Append a source weight pair
	void AppendSourceWeight(crExpressionOp*, float);

	// PURPOSE: Remove a source weight pair
	void RemoveSourceWeight(int);

private:

	// PURPOSE: Source weight pair
	struct SourceWeight
	{
		// PURPOSE: Default constructor
		SourceWeight();

		// PURPOSE: Resource constructor
		SourceWeight(datResource&);

		// PURPOSE: Placement
		IMPLEMENT_PLACE_INLINE(SourceWeight);

		// PURPOSE: Declare structure
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

		// PURPOSE: Destructor
		~SourceWeight();

		// PURPOSE: Shutdown
		void Shutdown();

		datOwner<crExpressionOp> m_Source;
		float m_Weight;
	};

	atArray<SourceWeight> m_SourceWeights;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Special blend vector operation
class crExpressionOpSpecialLinear : public crExpressionOp
{
public:

	// PURPOSE: Default constructor
	crExpressionOpSpecialLinear();

	// PURPOSE: Copy constructor
	crExpressionOpSpecialLinear(const crExpressionOpSpecialLinear&);

	// PURPOSE: Resource constructor
	crExpressionOpSpecialLinear(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpSpecialLinear();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpSpecialLinear);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;

	// PURPOSE: Internal call to get the frame dof
	Vec3V_Out GetSourceValue(ProcessHelper& ph, u32 i) const;

	struct Source
	{
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif
		u32 m_Component;
		s32 m_AccIdx;
		u16 m_Id;
		u8 m_Track;
		u8 m_Type;
	};

	struct Interval
	{
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif
		Vec3V m_Begin;
		Vec3V m_Mult;
		Vec3V m_Add;
	};

	atArray<Source> m_Sources;
	atArray<Interval> m_Intervals;
	datOwner<crExpressionOp> m_Reference;
	u32 m_IntervalPerSource;
	u32 m_Mode;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Special curve operation
class crExpressionOpSpecialCurve : public crExpressionOp
{
public:

	// PURPOSE: Default constructor
	crExpressionOpSpecialCurve();

	// PURPOSE: Copy constructor
	crExpressionOpSpecialCurve(const crExpressionOpSpecialCurve&);

	// PURPOSE: Resource constructor
	crExpressionOpSpecialCurve(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpSpecialCurve();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpSpecialCurve);

	// PURPOSE: Get number of children
	virtual u32 GetNumChildren() const;

	// PURPOSE: Get child by index (const)
	virtual const crExpressionOp* GetChild(u32) const;

	// PURPOSE: Get child by index (non-const)
	virtual crExpressionOp* GetChild(u32);

	// PURPOSE: Set child by index
	virtual void SetChild(u32, crExpressionOp&);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;

	// PURPOSE: Get source (can be NULL)
	crExpressionOp* GetSource() const;

	// PURPOSE: Set source (can be NULL)
	void SetSource(crExpressionOp*);

	// PURPOSE: Get number of keys
	int GetNumKeys() const;

	// PURPOSE: Set number of keys
	void SetNumKeys(int);

	// PURPOSE: Get key by index
	void GetKey(int, float& in, float& out) const;

	// PURPOSE: Set key by index
	void SetKey(int, float in, float out);

	// PURPOSE: Append key
	void AppendKey(float in, float out);

	// PURPOSE: Remove key
	void RemoveKey(int);

private:

	datOwner<crExpressionOp> m_Source;

	// PURPOSE: Key
	struct Key
	{
		// PURPOSE: Default constructor
		Key();

		// PURPOSE: Resource constructor
		Key(datResource&);

		// PURPOSE: Placement
		IMPLEMENT_PLACE_INLINE(Key);

		// PURPOSE: Declare structure
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT


		float m_In;
		float m_Out;
	};

	atArray<Key> m_Keys;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Special look at operation
class crExpressionOpSpecialLookAt: public crExpressionOp
{
public:

	// PURPOSE: Default constructor
	crExpressionOpSpecialLookAt();

	// PURPOSE: Copy constructor
	crExpressionOpSpecialLookAt(const crExpressionOpSpecialLookAt&);

	// PURPOSE: Resource constructor
	crExpressionOpSpecialLookAt(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpSpecialLookAt();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpSpecialLookAt);

	// PURPOSE: Get number of children
	virtual u32 GetNumChildren() const;

	// PURPOSE: Get child by index (const)
	virtual const crExpressionOp* GetChild(u32) const;

	// PURPOSE: Get child by index (non-const)
	virtual crExpressionOp* GetChild(u32);

	// PURPOSE: Set child by index
	virtual void SetChild(u32, crExpressionOp&);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;

	// PURPOSE: Get look at position source (can be NULL)
	crExpressionOp* GetLookAtPosSource() const;

	// PURPOSE: Set look at position source (can be NULL)
	void SetLookAtPosSource(crExpressionOp*);

	// PURPOSE: Get look at rotation source (can be NULL)
	crExpressionOp* GetLookAtRotSource() const;

	// PURPOSE: Set look at rotation source (can be NULL)
	void SetLookAtRotSource(crExpressionOp*);

	// PURPOSE: Get origin position source (can be NULL)
	crExpressionOp* GetOriginPosSource() const;

	// PURPOSE: Set origin position source (can be NULL)
	void SetOriginPosSource(crExpressionOp*);

	// PURPOSE: Get rotational offset
	QuatV_Out GetOffset() const;

	// PURPOSE: Set rotational offset
	void SetOffset(QuatV_In);

	// PURPOSE:
	enum eAxis
	{
		kXAxis = 0,
		kYAxis,
		kZAxis,
		kXAxisNeg,
		kYAxisNeg,
		kZAxisNeg,
	};

	// PURPOSE: Get look at axis
	u8 GetLookAtAxis() const;

	// PURPOSE: Set look at axis
	void SetLookAtAxis(u8);

	// PURPOSE: Get up local axis
	u8 GetLookAtUpAxis() const;

	// PURPOSE: Set up local axis
	void SetLookAtUpAxis(u8);

	// PURPOSE: Get up source axis
	u8 GetOriginUpAxis() const;

	// PURPOSE: Set up source axis
	void SetOriginUpAxis(u8);


private:

	datOwner<crExpressionOp> m_LookAtPosSource;
	datOwner<crExpressionOp> m_LookAtRotSource;

	QuatV m_Offset;

	datOwner<crExpressionOp> m_OriginPosSource;

	u8 m_LookAtAxis;
	u8 m_LookAtUpAxis;
	u8 m_OriginUpAxis; // no neg
	datPadding<9> m_Padding;
};

////////////////////////////////////////////////////////////////////////////////

class crExpressionOpVariableGetSet : public crExpressionOp
{
public:

	// PURPOSE: Constructor
	crExpressionOpVariableGetSet(eOpTypes);

	// PURPOSE: Copy constructor
	crExpressionOpVariableGetSet(const crExpressionOpVariableGetSet&);

	// PURPOSE: Resource constructor
	crExpressionOpVariableGetSet(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crExpressionOpVariableGetSet();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

	// PURPOSE: Dump operation override
	virtual void Dump(DumpHelper&) const;

	// PURPOSE: Set variable hash value
	void SetHash(u32 hash);

	// PURPOSE: Get variable hash value
	u32 GetHash() const;

	// PURPOSE: Set variable index value (for fast lookup)
	void SetIndex(u16 idx);

	// PURPOSE: Get variable index value (for fast lookup)
	u16 GetIndex() const;


private:

	u32 m_Hash;
};

////////////////////////////////////////////////////////////////////////////////

class crExpressionOpVariableGet : public crExpressionOpVariableGetSet
{
public:

	// PURPOSE: Constructor
	crExpressionOpVariableGet();

	// PURPOSE: Copy constructor
	crExpressionOpVariableGet(const crExpressionOpVariableGet&);

	// PURPOSE: Resource constructor
	crExpressionOpVariableGet(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpVariableGet);

	// PURPOSE: Get number of children
	virtual u32 GetNumChildren() const;

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

};

////////////////////////////////////////////////////////////////////////////////

class crExpressionOpVariableSet : public crExpressionOpVariableGetSet
{
public:

	// PURPOSE: Constructor
	crExpressionOpVariableSet();

	// PURPOSE: Destructor
	~crExpressionOpVariableSet();

	// PURPOSE: Copy constructor
	crExpressionOpVariableSet(const crExpressionOpVariableSet&);

	// PURPOSE: Resource constructor
	crExpressionOpVariableSet(datResource&);

	// PURPOSE: Declare struct
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Type registration
	CR_DECLARE_EXPRESSION_OP_TYPE(crExpressionOpVariableSet);

	// PURPOSE: Get number of children
	virtual u32 GetNumChildren() const;

	// PURPOSE: Get child by index (const)
	virtual const crExpressionOp* GetChild(u32) const;

	// PURPOSE: Get child by index (non-const)
	virtual crExpressionOp* GetChild(u32);

	// PURPOSE: Set child by index
	virtual void SetChild(u32, crExpressionOp&);

	// PURPOSE: Process operation override
	virtual void Process(ProcessHelper&) const;

	// PURPOSE: Serialize operation override
	virtual void Serialize(SerializeHelper&);

private:

	datOwner<crExpressionOp> m_Source;
};

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::eOpTypes crExpressionOp::ExpressionOpTypeInfo::GetOpType() const
{
	return m_OpType;
}

////////////////////////////////////////////////////////////////////////////////

inline const char* crExpressionOp::ExpressionOpTypeInfo::GetOpName() const
{
	return m_Name;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::ExpressionOpCreateFn* crExpressionOp::ExpressionOpTypeInfo::GetCreateFn() const
{
	return m_CreateFn;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::ExpressionOpPlaceFn* crExpressionOp::ExpressionOpTypeInfo::GetPlaceFn() const
{
	return m_PlaceFn;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::ExpressionOpPlaceRscFn* crExpressionOp::ExpressionOpTypeInfo::GetPlaceRscFn() const
{
	return m_PlaceRscFn;
}

////////////////////////////////////////////////////////////////////////////////

inline size_t crExpressionOp::ExpressionOpTypeInfo::GetSize() const
{
	return m_Size;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::Value::Value()
{
	Zero();
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::Value::Value(Vec3V_In v)
{
	Zero();
	Set(v);
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::Value::Value(QuatV_In q)
{
	Zero();
	Set(q);
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::Value::Value(float f)
{
	Zero();
	Set(f);
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::Value::Value(s32 i)
{
	Zero();
	Set(i);
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::Value::Value(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::Value::~Value()
{
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOp::Value::Set(Vec3V_In v)
{
	*reinterpret_cast<Vec3V*>(m_Values) = v;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOp::Value::Set(QuatV_In q)
{
	*reinterpret_cast<QuatV*>(m_Values) = q;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOp::Value::Set(float f)
{
	*reinterpret_cast<float*>(m_Values) = f;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOp::Value::Set(s32 i)
{
	*reinterpret_cast<s32*>(m_Values) = i;
}

////////////////////////////////////////////////////////////////////////////////

inline Vec3V_Ref crExpressionOp::Value::GetVector()
{
	return *reinterpret_cast<Vec3V*>(m_Values);
}

////////////////////////////////////////////////////////////////////////////////

inline QuatV_Ref crExpressionOp::Value::GetQuaternion()
{
	return *reinterpret_cast<QuatV*>(m_Values);
}

////////////////////////////////////////////////////////////////////////////////

inline float& crExpressionOp::Value::GetFloat()
{
	return *reinterpret_cast<float*>(m_Values);
}

////////////////////////////////////////////////////////////////////////////////

inline s32& crExpressionOp::Value::GetInt()
{
	return *reinterpret_cast<s32*>(m_Values);
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::Value& crExpressionOp::Value::operator=(const Value& src)
{
	m_Values[0] = src.m_Values[0];
	m_Values[1] = src.m_Values[1];
	m_Values[2] = src.m_Values[2];
	m_Values[3] = src.m_Values[3];

	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crExpressionOp::Value::operator==(const Value& src) const
{
	return (m_Values[0] == src.m_Values[0]) && (m_Values[1] == src.m_Values[1]) && (m_Values[2] == src.m_Values[2]) && (m_Values[3] == src.m_Values[3]);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crExpressionOp::Value::operator!=(const Value& src) const
{
	return (m_Values[0] != src.m_Values[0]) || (m_Values[1] != src.m_Values[1]) || (m_Values[2] != src.m_Values[2]) || (m_Values[3] != src.m_Values[3]);
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOp::Value::Zero()
{
	m_Values[0] = m_Values[1] = m_Values[2] = m_Values[3] = 0.f;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::eOpTypes crExpressionOp::GetOpType() const
{
	return eOpTypes(m_OpType);
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::ProcessHelper::ProcessHelper()
: m_Frame(NULL)
, m_Filter(NULL)
, m_Time(0.f)
, m_DeltaTime(0.f)
, m_SkeletonData(NULL)
, m_Values(NULL)
, m_Top(-1)
, m_NumValues(0)
, m_Variables(NULL)
, m_NumVariables(0)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::Value& crExpressionOp::ProcessHelper::PushValue()
{
	Assert(m_Top < m_NumValues-1);
	return m_Values[++m_Top];
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::Value& crExpressionOp::ProcessHelper::PopValue()
{
	Assert(m_Top >= 0);
	return m_Values[m_Top--];
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::Value& crExpressionOp::ProcessHelper::TopValue()
{
	Assert(m_Top >= 0);
	return m_Values[m_Top];
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::SerializeHelper::SerializeHelper(datSerialize& s, int version)
{
	m_Serialize = &s;
	m_Version = version;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::SerializeHelper::operator datSerialize&()
{
	return *m_Serialize;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crExpressionOp::SerializeHelper::IsRead() const
{
	return m_Serialize->IsRead();
}

////////////////////////////////////////////////////////////////////////////////

template<bool input, bool output>
crExpressionOp::InputOutputIterator<input, output>::InputOutputIterator()
: m_Found(false)
{
}

////////////////////////////////////////////////////////////////////////////////

template<bool input, bool output>
crExpressionOp::InputOutputIterator<input, output>::~InputOutputIterator()
{
}

////////////////////////////////////////////////////////////////////////////////

template<bool input, bool output>
void crExpressionOp::InputOutputIterator<input, output>::Visit(const crExpressionOp& op)
{
	if(!m_Found)
	{
		switch(op.GetOpType())
		{
		case kOpTypeGet:
		case kOpTypeValid:
		case kOpTypeComponentGet:
		case kOpTypeObjectSpaceGet:
		case kOpTypeObjectSpaceConvertTo:
		case kOpTypeObjectSpaceConvertFrom:
			if(input)
			{
				const crExpressionOpGetSet& opGetSet = static_cast<const crExpressionOpGetSet&>(op);
				m_Found = Callback(opGetSet.GetTrack(), opGetSet.GetId(), opGetSet.GetType(), true);
			}
			break;

		case kOpTypeSet:
		case kOpTypeComponentSet:
		case kOpTypeObjectSpaceSet:
			if(output)
			{
				const crExpressionOpGetSet& opGetSet = static_cast<const crExpressionOpGetSet&>(op);
				m_Found = Callback(opGetSet.GetTrack(), opGetSet.GetId(), opGetSet.GetType(), false);
				if(!m_Found)
				{
					ConstIterator::Visit(op);
				}
			}
			else
			{
				ConstIterator::Visit(op);
			}
			break;

		case kOpTypeSpecialLinear:
			if(input)
			{
				const crExpressionOpSpecialLinear& opLinear = static_cast<const crExpressionOpSpecialLinear&>(op);

				const crExpressionOpSpecialLinear::Interval* interval = opLinear.m_Intervals.GetElements();
				for(int i=0; i<opLinear.m_Sources.GetCount(); ++i)
				{
					const crExpressionOpSpecialLinear::Source& src = opLinear.m_Sources[i];
					bool padding = src.m_Track == 0 && src.m_Id == 0 && src.m_Type == 0;
					for(u32 j=0; j<opLinear.m_IntervalPerSource; j++)
					{
						padding &= IsEqualAll(interval->m_Begin, Vec3V(V_NEG_FLT_MAX)) != 0;
						padding &= IsEqualAll(interval->m_Mult, Vec3V(V_ZERO)) != 0;
						interval++;
					}

					if(!padding)
					{
						m_Found = Callback(src.m_Track, src.m_Id, src.m_Type, true);
						if(m_Found)
						{
							break;
						}
					}
				}
			}
			break;

		case crExpressionOp::kOpTypeMotion:
			if (output)
			{
				const crExpressionOpMotion& opMotion = static_cast<const crExpressionOpMotion&>(op);
				m_Found = Callback(kTrackBoneRotation, opMotion.m_Descr.GetBoneId(), kFormatTypeQuaternion, false);
				m_Found && Callback(kTrackBoneTranslation, opMotion.m_Descr.GetBoneId(), kFormatTypeVector3, false);
			}

		default:
			ConstIterator::Visit(op);
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

inline u16 crExpressionOp::GetOpSubType() const
{
	return m_OpSubType;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOp::SetOpSubType(u16 opSubType)
{
	m_OpSubType = opSubType;
}

////////////////////////////////////////////////////////////////////////////////

inline const crExpressionOp::Value& crExpressionOpConstant::GetValue() const
{
	return m_Value;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp::Value& crExpressionOpConstant::GetValue()
{
	return m_Value;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpConstant::SetValue(const Value& val)
{
	m_Value = val;
}

////////////////////////////////////////////////////////////////////////////////

inline float crExpressionOpConstantFloat::GetFloat() const
{
	return m_Float;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpConstantFloat::SetFloat(float f)
{
	m_Float = f;
}

////////////////////////////////////////////////////////////////////////////////

inline u8 crExpressionOpGetSet::GetTrack() const
{
	return m_Track;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpGetSet::SetTrack(u8 track)
{
	m_Track = track;
}

////////////////////////////////////////////////////////////////////////////////

inline u8 crExpressionOpGetSet::GetType() const
{
	return m_Type;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpGetSet::SetType(u8 type)
{
	m_Type = type;
}

////////////////////////////////////////////////////////////////////////////////

inline u16 crExpressionOpGetSet::GetId() const
{
	return m_Id;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpGetSet::SetId(u16 id)
{
	m_Id = id;
}

////////////////////////////////////////////////////////////////////////////////

inline int crExpressionOpGetSet::GetAcceleratedIndex() const
{
	return m_AccIdx;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpGetSet::SetAcceleratedIndex(int idx)
{
	Assert(idx >= -1);
	m_AccIdx = idx;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crExpressionOpGetSet::IsRelative() const
{
	return (GetOpSubType()&0x4000) != 0;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpGetSet::SetRelative(bool relative)
{
	SetOpSubType(relative ? (GetOpSubType()|0x4000) : (GetOpSubType()&0xbfff));
}

////////////////////////////////////////////////////////////////////////////////

inline bool crExpressionOpGetSet::IsForceDefault() const
{
	return (GetOpSubType()&0x8000) != 0;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpGetSet::SetForceDefault(bool force)
{
	SetOpSubType(force ? (GetOpSubType()|0x8000) : (GetOpSubType()&0x7fff));
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp* crExpressionOpSet::GetSource() const
{
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSet::SetSource(crExpressionOp* source)
{
	// TODO --- destroy old source?
	m_Source = source;
}

////////////////////////////////////////////////////////////////////////////////

inline u8 crExpressionOpGetSet::GetComponent() const
{
	return u8((GetOpSubType()>>12)&0x0003);
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpGetSet::SetComponent(u8 component)
{
	SetOpSubType(((u16(component)&0x0003)<<12) | (GetOpSubType()&0xcfff));
}

////////////////////////////////////////////////////////////////////////////////

inline EulerAngleOrder crExpressionOpGetSet::GetOrder() const
{
	return EulerAngleOrder((GetOpSubType()>>9)&0x0007);
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpGetSet::SetOrder(u8 order)
{
	SetOpSubType(((u16(order)&0x0007)<<9) | (GetOpSubType()&0xf1ff));
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp* crExpressionOpComponentSet::GetSource() const
{
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpComponentSet::SetSource(crExpressionOp* source)
{
	// TODO --- destroy old source?
	m_Source = source;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp* crExpressionOpObjectSpaceSet::GetSource() const
{
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpObjectSpaceSet::SetSource(crExpressionOp* op)
{
	m_Source = op;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOpNullary::eNullaryOpTypes crExpressionOpNullary::GetNullaryOpType() const
{
	return eNullaryOpTypes(GetOpSubType());
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpNullary::SetNullaryOpType(eNullaryOpTypes opType)
{
	SetOpSubType(u16(opType));
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOpUnary::eUnaryOpTypes crExpressionOpUnary::GetUnaryOpType() const
{
	return eUnaryOpTypes(GetOpSubType());
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpUnary::SetUnaryOpType(eUnaryOpTypes opType)
{
	SetOpSubType(u16(opType));
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp* crExpressionOpUnary::GetSource() const
{
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpUnary::SetSource(crExpressionOp* source)
{
	// TODO --- destroy old source?
	m_Source = source;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOpBinary::eBinaryOpTypes crExpressionOpBinary::GetBinaryOpType() const
{
	return eBinaryOpTypes(GetOpSubType());
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpBinary::SetBinaryOpType(eBinaryOpTypes opType)
{
	SetOpSubType(u16(opType));
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp* crExpressionOpBinary::GetSource(int idx) const
{
	return m_Sources[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpBinary::SetSource(int idx, crExpressionOp* source)
{
	// TODO --- destroy old source?
	m_Sources[idx] = source;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOpTernary::eTernaryOpTypes crExpressionOpTernary::GetTernaryOpType() const
{
	return eTernaryOpTypes(GetOpSubType());
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpTernary::SetTernaryOpType(eTernaryOpTypes opType)
{
	SetOpSubType(u16(opType));
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp* crExpressionOpTernary::GetSource(int idx) const
{
	return m_Sources[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpTernary::SetSource(int idx, crExpressionOp* source)
{
	// TODO --- destroy old source?
	m_Sources[idx] = source;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOpNary::eNaryOpTypes crExpressionOpNary::GetNaryOpType() const
{
	return eNaryOpTypes(GetOpSubType());
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpNary::SetNaryOpType(eNaryOpTypes opType)
{
	SetOpSubType(u16(opType));
}

////////////////////////////////////////////////////////////////////////////////

inline int crExpressionOpNary::GetNumSources() const
{
	return m_Sources.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpNary::SetNumSources(int num)
{
//	m_Sources.Reset();
	m_Sources.Resize(num);
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp* crExpressionOpNary::GetSource(int idx) const
{
	return m_Sources[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpNary::SetSource(int idx, crExpressionOp* source)
{
	// TODO --- destroy old source?
	m_Sources[idx] = source;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpNary::AppendSource(crExpressionOp* source)
{
	m_Sources.Grow() = source;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpNary::RemoveSource(int idx)
{
	if(m_Sources[idx])
	{
		delete m_Sources[idx];
	}
	m_Sources.Delete(idx);
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOpSpecialBlend::eBlendMode crExpressionOpSpecialBlend::GetBlendMode() const
{
	return eBlendMode(GetOpSubType() & kBlendModeMask);
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialBlend::SetBlendMode(eBlendMode blendMode)
{
	SetOpSubType(u16((GetOpSubType()&~kBlendModeMask)|(blendMode&kBlendModeMask)));
}

////////////////////////////////////////////////////////////////////////////////

inline int crExpressionOpSpecialBlend::GetNumSourceWeights() const
{
	return m_SourceWeights.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialBlend::SetNumSourceWeights(int num)
{
//	m_SourceWeights.Reset();
	m_SourceWeights.Resize(num);
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp* crExpressionOpSpecialBlend::GetSource(int idx) const
{
	return m_SourceWeights[idx].m_Source;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialBlend::SetSource(int idx, crExpressionOp* source)
{
	m_SourceWeights[idx].m_Source = source;
}

////////////////////////////////////////////////////////////////////////////////

inline float crExpressionOpSpecialBlend::GetWeight(int idx) const
{
	return m_SourceWeights[idx].m_Weight;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialBlend::SetWeight(int idx, float weight)
{
	m_SourceWeights[idx].m_Weight = weight;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialBlend::AppendSourceWeight(crExpressionOp* source, float weight)
{
	SourceWeight& sourceWeight = m_SourceWeights.Grow();
	sourceWeight.m_Source = source;
	sourceWeight.m_Weight = weight;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialBlend::RemoveSourceWeight(int idx)
{
	m_SourceWeights[idx].Shutdown();
	m_SourceWeights.Delete(idx);
	m_SourceWeights.Append().m_Source = NULL;
	m_SourceWeights.Pop();
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp* crExpressionOpSpecialCurve::GetSource() const
{
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialCurve::SetSource(crExpressionOp* source)
{
	// TODO --- destroy old source?
	m_Source = source;
}

////////////////////////////////////////////////////////////////////////////////

inline int crExpressionOpSpecialCurve::GetNumKeys() const
{
	return m_Keys.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialCurve::SetNumKeys(int num)
{
	// m_Keys.Reset();
	m_Keys.Resize(num);
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialCurve::GetKey(int idx, float& in, float& out) const
{
	in = m_Keys[idx].m_In;
	out = m_Keys[idx].m_Out;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialCurve::SetKey(int idx, float in, float out)
{
	m_Keys[idx].m_In = in;
	m_Keys[idx].m_Out = out;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialCurve::AppendKey(float in, float out)
{
	Key& key = m_Keys.Grow();
	key.m_In = in;
	key.m_Out = out;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialCurve::RemoveKey(int idx)
{
	m_Keys.Delete(idx);
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp* crExpressionOpSpecialLookAt::GetLookAtPosSource() const
{
	return m_LookAtPosSource;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialLookAt::SetLookAtPosSource(crExpressionOp* source)
{
	m_LookAtPosSource = source;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp* crExpressionOpSpecialLookAt::GetLookAtRotSource() const
{
	return m_LookAtRotSource;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialLookAt::SetLookAtRotSource(crExpressionOp* source)
{
	m_LookAtRotSource = source;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp* crExpressionOpSpecialLookAt::GetOriginPosSource() const
{
	return m_OriginPosSource;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialLookAt::SetOriginPosSource(crExpressionOp* source)
{
	m_OriginPosSource = source;
}

////////////////////////////////////////////////////////////////////////////////

inline QuatV_Out crExpressionOpSpecialLookAt::GetOffset() const
{
	return m_Offset;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialLookAt::SetOffset(QuatV_In offset)
{
	m_Offset = offset;
}

////////////////////////////////////////////////////////////////////////////////

inline u8 crExpressionOpSpecialLookAt::GetLookAtAxis() const
{
	return m_LookAtAxis;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialLookAt::SetLookAtAxis(u8 axis)
{
	m_LookAtAxis = axis;
}

////////////////////////////////////////////////////////////////////////////////

inline u8 crExpressionOpSpecialLookAt::GetLookAtUpAxis() const
{
	return m_LookAtUpAxis;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialLookAt::SetLookAtUpAxis(u8 axis)
{
	m_LookAtUpAxis = axis;
}

////////////////////////////////////////////////////////////////////////////////

inline u8 crExpressionOpSpecialLookAt::GetOriginUpAxis() const
{
	return m_OriginUpAxis;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpSpecialLookAt::SetOriginUpAxis(u8 axis)
{
	m_OriginUpAxis = axis;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpVariableGetSet::SetHash(u32 hash)
{
	m_Hash = hash;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crExpressionOpVariableGetSet::GetHash() const
{
	return m_Hash;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionOpVariableGetSet::SetIndex(u16 idx)
{
	SetOpSubType(idx);
}

////////////////////////////////////////////////////////////////////////////////

inline u16 crExpressionOpVariableGetSet::GetIndex() const
{
	return GetOpSubType();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV

#endif // CREXTRA_EXPRESSIONOPS_H
