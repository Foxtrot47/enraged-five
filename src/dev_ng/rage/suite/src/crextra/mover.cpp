//
// crextra/mover.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "mover.h"

#include "fragment/instance.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundsphere.h"
#include "physics/colliderdispatch.h"
#include "physics/constraintcylindrical.h"
#include "physics/intersection.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "system/param.h"
#include "system/timemgr.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// defaults
const float crMoverInst::sm_DefaultCharacterRadius = 0.35f;
const float crMoverInst::sm_DefaultCharacterHeight = 2.f;
const float crMoverInst::sm_DefaultCharacterFloatHeight = 0.2f;
const Vector3 crMoverInst::sm_DefaultCharacterExtraBoundOffset = ORIGIN;
const float crMoverInst::sm_DefaultGravityFactor = 2.f;
const float crMoverInst::sm_DefaultCharacterMass = 97.f;
const float crMoverInst::sm_DefaultAccelCap = 60.f;
const float crMoverInst::sm_DefaultSlopeFactor = 0.8f;
const float crMoverInst::sm_DefaultGroundProbePenetration = 0.12f;
const float crMoverInst::sm_DefaultGroundProbeUpdateDelay = 3.f / 30.f;

// constants
const float crMoverInst::sm_MaxAngSpeed = 100.f*PI;
const float crMoverInst::sm_UprightTolerance = 0.9999f;
const float crMoverInst::sm_GroundNormalTolerance = 0.99f;
const float crMoverInst::sm_GroundProbeHeightFactor = 2.1f;
const float crMoverInst::sm_ObstructionTolerance = 0.7071f;
const float crMoverInst::sm_SpringConstant = -40.f;
const float crMoverInst::sm_EffectiveDepthFactor = 0.4f;
const float crMoverInst::sm_DampeningConstant = -40.f;

////////////////////////////////////////////////////////////////////////////////


crMoverInst::crMoverInst()
: m_AttachmentMatrix(M34_IDENTITY)

, m_LastGroundNormal(YAXIS) // TODO -- YUP
, m_GroundInstancePos_WS(ORIGIN)
, m_GroundInstancePos_CS(ORIGIN)
, m_RightVector_WS(ORIGIN)
, m_RightVector_CS(ORIGIN)
, m_SuspendVelocity(ORIGIN)

, m_CollisionExclusionInst(NULL)
, m_ProbeExclusionInst(NULL)
, m_AttachmentInst(NULL)
, m_GroundInstance(NULL)
, m_GroundMaterial(NULL)

, m_GroundProbeDepth(-1.f)
, m_RecoveryHeightTolerance(-1.f)
, m_FloatHeight(0.f)
, m_LateralSlide(0.f)
, m_LinearSlide(0.f)
, m_LateralAccelCap(sm_DefaultAccelCap)
, m_LinearAccelCap(sm_DefaultAccelCap)
, m_GravityFactor(sm_DefaultGravityFactor)
, m_SlopeFactor(sm_DefaultSlopeFactor)
, m_GroundProbePenetration(sm_DefaultGroundProbePenetration)
, m_GroundProbeTimer(0.f)
, m_GroundProbeUpdateDelay(sm_DefaultGroundProbeUpdateDelay)

, m_IsSuspendedCount(0)

, m_AttachmentComponent(u16(-1))
, m_GroundInstanceComponent(u16(-1))

, m_IsOnGround(false)
, m_IsJumping(false)
, m_IsRecovering(false)
, m_CollidedCached(false)
, m_Collided(false)
, m_ObstructedCached(false)
, m_Obstructed(false)
{
	m_CompositeBound.Init(2);

	// by default we don't want to be using the composite bound.
	m_CompositeBound.SetNumBounds(0);
	m_CompositeBound.SetBound(0, &m_CapsuleBound);
	m_CompositeBound.SetCurrentMatrix(0, Mat34V(V_IDENTITY));
	m_CompositeBound.SetLastMatrix(0, Mat34V(V_IDENTITY));

	Reset();
}

////////////////////////////////////////////////////////////////////////////////

crMoverInst::crMoverInst(datResource& UNUSED_PARAM(rsc))
{
}

////////////////////////////////////////////////////////////////////////////////

crMoverInst::~crMoverInst()
{
	Shutdown();

	// Release the composite bound, and remove the capsule bound so that its destructor doesn't try to destroy the capsule bound.
	m_CompositeBound.Release(false);
	m_CompositeBound.SetBound(0,NULL);

	// Remove the capsule bound from the archetype so that its destructor doesn't try to destroy the capsule bound.
	m_ArchetypeDamped.SetBound(NULL);

	// Remove the archetype from this instance, so that the destructor doesn't try to destroy the archetype.
	SetArchetype(NULL,false);

	// Release the capsule bound.
	m_CapsuleBound.Release(false);
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crMoverInst::DeclareStruct(datTypeStruct& s)
{
	phInst::DeclareStruct(s);

	STRUCT_BEGIN(crMoverInst);

	STRUCT_FIELD(m_ArchetypeDamped);
	STRUCT_FIELD(m_CapsuleBound);
	STRUCT_FIELD(m_CompositeBound);
	Assert(0); //STRUCT_FIELD(m_Collider);  // TODO - phConstrainedCollider doesn't support offline resourcing

	STRUCT_FIELD(m_AttachmentMatrix);

	STRUCT_FIELD(m_LastGroundNormal);
	STRUCT_FIELD(m_GroundInstancePos_WS);
	STRUCT_FIELD(m_GroundInstancePos_CS);
	STRUCT_FIELD(m_RightVector_WS);
	STRUCT_FIELD(m_RightVector_CS);
	STRUCT_FIELD(m_SuspendVelocity);

	STRUCT_FIELD(m_CollisionExclusionInst);
	STRUCT_FIELD(m_ProbeExclusionInst);
	STRUCT_FIELD(m_AttachmentInst);
	STRUCT_FIELD(m_GroundInstance);
	STRUCT_FIELD(m_GroundMaterial);

	STRUCT_FIELD(m_GroundProbeDepth);
	STRUCT_FIELD(m_RecoveryHeightTolerance);
	STRUCT_FIELD(m_FloatHeight);
	STRUCT_FIELD(m_LateralSlide);
	STRUCT_FIELD(m_LinearSlide);
	STRUCT_FIELD(m_LateralAccelCap);
	STRUCT_FIELD(m_LinearAccelCap);
	STRUCT_FIELD(m_GravityFactor);
	STRUCT_FIELD(m_SlopeFactor);
	STRUCT_FIELD(m_GroundProbePenetration);
	STRUCT_FIELD(m_GroundProbeTimer);
	STRUCT_FIELD(m_GroundProbeUpdateDelay);

	STRUCT_FIELD(m_IsSuspendedCount);

	STRUCT_FIELD(m_AttachmentComponent);
	STRUCT_FIELD(m_GroundInstanceComponent);

	STRUCT_FIELD(m_IsOnGround);				
	STRUCT_FIELD(m_IsJumping);
	STRUCT_FIELD(m_IsRecovering);
	STRUCT_FIELD(m_CollidedCached);
	STRUCT_FIELD(m_Collided);
	STRUCT_FIELD(m_ObstructedCached);
	STRUCT_FIELD(m_Obstructed);	
	
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crMoverInst);

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::Init(const float radius, const float height, const float floatHeight, Vector3::Vector3Param extraBoundOffset, const float mass, const phInst* ownInst)
{
	Reset();

	SetCharacterSize(radius, height, floatHeight, extraBoundOffset);
	SetMass(mass);

	if(ownInst)
	{
		AddCollisionExclusionInst(ownInst);
		AddProbeExclusionInst(ownInst);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::Shutdown()
{
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::SetCharacterSize(const float radius, const float height, const float floatHeight, Vector3::Vector3Param extraBoundOffset)
{
	const float newCapsuleHeight = height - 2.0f * radius - floatHeight;
	if( m_FloatHeight == floatHeight &&
		m_CapsuleBound.GetRadius() == radius &&
		m_CapsuleBound.GetLength() == newCapsuleHeight)
	{
		// already set up with these values, no need to do anything
		return;
	}

	m_FloatHeight = floatHeight;
	m_CapsuleBound.SetCapsuleSize(radius, newCapsuleHeight);

	Vector3 centroidOffset(0.0f, 0.5f * (height + floatHeight), 0.0f);
	centroidOffset.Add(extraBoundOffset);
	m_CapsuleBound.SetCentroidOffset(RCC_VEC3V(centroidOffset));

	Vector3 cgOffset(0.0f, 0.5f * height, 0.0f);
	cgOffset.Add(extraBoundOffset);
	m_CapsuleBound.SetCGOffset(RCC_VEC3V(cgOffset));

	// have this check here because we might not actually have set up our collider yet (we call this at init time, too).
	if(m_Collider.GetInstance() != NULL)
	{
		Assert(m_Collider.GetInstance() == this);
		m_Collider.SetColliderMatrixFromInstance();

		if(m_ArchetypeDamped.GetBound() == &m_CompositeBound)
		{
			m_CompositeBound.CalculateCompositeExtents();
			if(m_CompositeBound.HasBVHStructure())
			{
				if(IsInLevel())
				{
					PHLEVEL->RebuildCompositeBvh(GetLevelIndex());
				}
				else
				{
					m_CompositeBound.UpdateBvh(true);
				}
			}
		}
		if (IsInLevel())
		{
			PHLEVEL->UpdateObjectLocationAndRadius(GetLevelIndex(), (Mat34V_Ptr)(NULL));
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::SetMass(const float newMass)
{
	if(newMass != m_ArchetypeDamped.GetMass())
	{
		m_ArchetypeDamped.SetMass(newMass);
		m_ArchetypeDamped.CalcAngInertia();

		// now archetype updated, make sure collider processes those changes
		m_Collider.InitInertia();
	}
}
////////////////////////////////////////////////////////////////////////////////

void crMoverInst::AddExtraBound(const int boundIndex, phBound *newBound, const Matrix34 &boundMatrix)
{
	Assert(boundIndex > 0);
	Assert(boundIndex < m_CompositeBound.GetMaxNumBounds());

	m_CompositeBound.SetBound(boundIndex, newBound);
	m_CompositeBound.SetCurrentMatrix(boundIndex, RCC_MAT34V(boundMatrix));
	m_CompositeBound.SetLastMatrix(boundIndex, RCC_MAT34V(boundMatrix));
	m_CompositeBound.SetNumBounds(boundIndex + 1);
	m_CompositeBound.CalculateCompositeExtents();
	m_ArchetypeDamped.SetBound(&m_CompositeBound);

	if (IsInLevel())
	{
		if(m_CompositeBound.HasBVHStructure())
		{
			PHLEVEL->RebuildCompositeBvh(GetLevelIndex());
		}
		PHLEVEL->UpdateObjectLocationAndRadius(GetLevelIndex(), (Mat34V_Ptr)(NULL));
	}
	else
	{
		m_CompositeBound.UpdateBvh(true);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::Teleport(const Matrix34 &newMatrix, bool keepPreviousState)
{
	// We need to have an upright matrix unless we're in suspend mode.
	Matrix34 newInstanceMatrix(newMatrix);
	if(newInstanceMatrix.b.z < sm_UprightTolerance && !IsSuspended())
	{
		Warningf("crMoverInst::Teleport - trying to teleport mover with non-upright matrix");
		newInstanceMatrix.MakeUpright();
	}

	// under certain conditions we don't want to clear the previous state (ie small teleports)
	if (!keepPreviousState)
	{
		m_IsOnGround = false;
		m_GroundProbeDepth = -1.0f;
		m_GroundMaterial = NULL;
		m_GroundInstance = NULL;

		m_GroundProbeTimer = 0.f;
	}

	// Update the instance according to the new position.
	SetMatrix(RCC_MAT34V(newInstanceMatrix));
	Assert(IsInLevel());
	PHLEVEL->UpdateObjectLocation(GetLevelIndex());

	// Get the collider to reflect the new instance position.
	Assert(m_Collider.GetInstance() == this);
	m_Collider.SetColliderMatrixFromInstance();
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::StartJump(float height, float duration)
{
	// safety code to prevent double jumping..
	if (m_IsJumping)
	{
		return;
	}

	float newZVelocity = 4.0f * (height) / (duration);
	float newGravityFactor = (-8.0f * height) / (square(duration) * GRAVITY);

	m_ArchetypeDamped.SetGravityFactor(newGravityFactor);

	Assert(!IsSuspended());

	// We'll just directly add in the y-velocity we're getting from the jump.  That way the velocity will have already changed by the time we get to crMoverInst::Update().
	Vector3 colliderVelocity(RCC_VECTOR3(m_Collider.GetVelocity()));
	colliderVelocity.z += newZVelocity;  // TODO --- YUP
	m_Collider.SetVelocity(colliderVelocity);

	// TODO - should probably be applying an impulse to whatever we're standing on, too.
	m_IsJumping = true;
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::EndJump()
{
	m_ArchetypeDamped.SetGravityFactor(m_GravityFactor);
	m_IsJumping = false;
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::StartSuspend()
{
	if(!IsSuspended() && !IsRecovering())
	{
		EndPhysical();

		m_SuspendVelocity.Zero();
	}

	++m_IsSuspendedCount;
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::EndSuspend()
{
	AssertMsg(IsSuspended(), "crMoverInst::EndSuspend() not properly nested with crMoverInst::StartSuspend().");
	--m_IsSuspendedCount;
	if(!IsSuspended() && !IsRecovering())
	{
		MakePhysical();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::StartRecovery(const float recoveryHeightTolerance)
{
	if(!IsSuspended() && !IsRecovering())
	{
		EndPhysical();
	}

	m_RecoveryHeightTolerance = recoveryHeightTolerance;
	m_IsRecovering = true;
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::EndRecovery()
{
	Assert(IsRecovering());

	m_IsRecovering = false;
	m_RecoveryHeightTolerance = -1.0f;

	if(!IsSuspended())
	{
		MakePhysical();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::Update(float deltaTime, const Vector3 &desiredTranslation, const Vector3 &desiredRotation)
{
	// TODO -- should this be before or after ground probes?

	m_LateralSlide = 0.0f;
	m_LinearSlide = 0.0f;

	const float invDeltaTime = (deltaTime>0.f) ? (1.f/deltaTime) : 0.f;

	if(!IsSuspended())
	{
		// If we were standing on something, find its motion so that we can add that motion in to the motion of the mover.
		// We need to do this now, before we overwrite the data that we're using here.
		Vector3 platformTranslation_WS(ORIGIN);
		Vector3 platformRotation_WS(ORIGIN);

		// This is really asking if we were on the ground at the end of the last frame
		// (which is equivalent to saying "were we standing on something last frame?").
		if(m_GroundInstance != NULL && m_GroundInstance->IsInLevel() && !PHLEVEL->IsFixed(m_GroundInstance->GetLevelIndex()))
		{
			// Add in motion that comes from our platform (whatever we're standing on).
			Vector3 groundContactPosUpdated(m_GroundInstancePos_CS);
			Vector3 groundRightVectorUpdated(m_RightVector_CS);
			if(m_GroundInstance->GetArchetype()->GetBound()->GetType() == phBound::COMPOSITE)
			{
				const phBoundComposite *compositeBound = static_cast<const phBoundComposite *>(m_GroundInstance->GetArchetype()->GetBound());
				RCC_MATRIX34(compositeBound->GetCurrentMatrix(m_GroundInstanceComponent)).Transform(groundContactPosUpdated);
				RCC_MATRIX34(compositeBound->GetCurrentMatrix(m_GroundInstanceComponent)).Transform3x3(groundRightVectorUpdated);
			}
			RCC_MATRIX34(m_GroundInstance->GetMatrix()).Transform(groundContactPosUpdated);
			RCC_MATRIX34(m_GroundInstance->GetMatrix()).Transform3x3(groundRightVectorUpdated);

			platformTranslation_WS.Subtract(groundContactPosUpdated, m_GroundInstancePos_WS);
			// A more general method for determining the rotation would be to store the last bound component space -> world space matrix for the bound component
			//   in question, compare that with the current bound component space -> world space matrix for the bound component in question, and find the axis and
			//   angle of the rotation that moves between the two.
			// Since, ultimately, we're only going to be using the rotation of the platform about the world y-axis, the method below is adequate and saves us
			//   a lot of the math that would be involved in the more general method above.
			// Note also that, below, we are assuming that the angle of rotation is small and hence that sin x ~= x (this is to avoid an arc sine and a normalization).
			//   In the future we might want to put an if-check in here in case angle is large enough that we no longer consider the approximation good enough.
			platformRotation_WS.Cross(m_RightVector_WS, groundRightVectorUpdated);
		}

		Vector3 finalDesiredTranslation(desiredTranslation), finalDesiredRotation(desiredRotation);
		finalDesiredTranslation.Add(platformTranslation_WS);
		finalDesiredRotation.Add(platformRotation_WS);

		// We're not in suspended mode which means that our movement is going to be controlled by our collider.
		// So what we do here is calculate and apply the linear and angular impulses that would give us the desired linear
		//   and angular motion.  Note that this requires taking into account the current linear and angular momenta
		//   and that we *cannot* just skip parts of this based on whether xlat or turn are zero.
		{
			Vector3 desiredLinearMomentum(finalDesiredTranslation);
			desiredLinearMomentum.Scale(m_Collider.GetMass() * invDeltaTime);

			// If we're not on level ground let's rotate the impulse we apply such that we're exerting it in the plane on which we're standing.
			// This is what causes us (well, in conjunction with how everything else works) to slow down when going up hills and speed up when going down hills.
			if(IsOnGround() && GetLastGroundNormal().z < sm_GroundNormalTolerance && m_SlopeFactor > 0.f)
			{
				Vector3 rotationAxis;
				rotationAxis.Cross(GetLastGroundNormal(), YAXIS);  // TODO --- YUP
				Assert(rotationAxis.z == 0.0f);
				Matrix34 rotationMatrix(M34_IDENTITY);
				float angleToRotate = m_SlopeFactor * asinf(rotationAxis.Mag());
				rotationAxis.Normalize();
				rotationMatrix.MakeRotateUnitAxis(rotationAxis, angleToRotate);
				rotationMatrix.Transform(desiredLinearMomentum);
			}

			Vector3 linearImpulse(desiredLinearMomentum);
			linearImpulse.Subtract(VEC3V_TO_VECTOR3(m_Collider.CalculateMomentum()));

			// I suppose this might be kind of temporary.  Basically, we know that xlat is only going to be in the xz plane, so we don't want to try and
			//   affect our y-velocity or anything.
			linearImpulse.z = 0.0f;

			// Limit the maximum acceleration that our mover can exert in order to meet the 'desired translation' supplied by clients.
			// We do this because the client shouldn't have to deal with things like the friction on the ground we're standing on.
			const float kMaxAccel = IsOnGround() ? 60.0f * GetGroundMaterial()->GetFriction() : 4.0f;  // TODO --- MAGIC NUMBER
			if(linearImpulse.Mag2() > square(kMaxAccel * m_Collider.GetMass() * deltaTime))
			{
				linearImpulse.ClampMag(0.0f, kMaxAccel * m_Collider.GetMass() * deltaTime);
			}

			// Limit the amount of lateral acceleration that we can have.  This gives us a bit of drifting or skidding out as we make turns.
			const float kLateralImpulse = linearImpulse.Dot(RCC_MATRIX34(GetMatrix()).a);  // TODO --- YUP
			const float kMaxLateralAccel = IsOnGround() ? m_LateralAccelCap * GetGroundMaterial()->GetFriction() : 4.0f; // TODO --- MAGIC NUMBER
			const float kMaxLateralImpulse = kMaxLateralAccel * m_Collider.GetMass() * deltaTime; // TODO --- TIME
			if(fabs(kLateralImpulse) > kMaxLateralImpulse)
			{
				m_LateralSlide = (kLateralImpulse / kMaxLateralImpulse) - 1.0f;
				linearImpulse.SubtractScaled(RCC_MATRIX34(GetMatrix()).a, kLateralImpulse);  // TODO -- YUP
				linearImpulse.AddScaled(RCC_MATRIX34(GetMatrix()).a, (kLateralImpulse > 0.0f ? 1.0f : -1.0f) * kMaxLateralImpulse);  // TODO -- YUP
			}
			// Surely we would also want something like this (to keep things consistent)?
			// The thing I don't like about this is that, by limiting the acceleration independently in the forward and lateral directions it makes it so that
			//   it actually faster to move diagonally that straight ahead or sideways.
			const float kLinearImpulse = linearImpulse.Dot(RCC_MATRIX34(GetMatrix()).c);  // TODO -- YUP
			const float kMaxLinearAccel = IsOnGround() ? m_LinearAccelCap * GetGroundMaterial()->GetFriction() : 4.0f;  // TODO -- MAGIC NUMBER
			const float kMaxLinearImpulse = kMaxLinearAccel * m_Collider.GetMass() * deltaTime;
			if(fabs(kLinearImpulse) > kMaxLinearImpulse)
			{
				m_LinearSlide = (kLinearImpulse / kMaxLinearImpulse) - 1.0f;
				linearImpulse.SubtractScaled(RCC_MATRIX34(GetMatrix()).c, kLinearImpulse); // TODO -- YUP
				linearImpulse.AddScaled(RCC_MATRIX34(GetMatrix()).c, (kLinearImpulse > 0.0f ? 1.0f : -1.0f) * kMaxLinearImpulse);  // TODO -- YUP
			}

			m_Collider.ApplyImpulseCenterOfMass(linearImpulse);
		}

		Vector3 desiredAngularMomentum(finalDesiredRotation);
		desiredAngularMomentum.Multiply(RCC_VECTOR3(m_Collider.GetAngInertia()));
		desiredAngularMomentum.Scale(invDeltaTime);
		Assert(FPIsFinite(desiredAngularMomentum.z));  // TODO --- YUP

		Vector3 angularImpulse(desiredAngularMomentum);
		angularImpulse.Subtract(VEC3V_TO_VECTOR3(m_Collider.CalculateAngMomentum()));
		Assert(FPIsFinite(angularImpulse.z));

		// We only want to allow rotation about the y-axis.
		angularImpulse.x = 0.0f;
		angularImpulse.z = 0.0f;  // TODO -- YUP

		m_Collider.ApplyAngImpulse(angularImpulse);
	}
	else
	{
		Matrix34 newInstanceMatrix( RCC_MATRIX34(GetMatrix()) );
		if(!desiredRotation.IsZero())
		{
			Vector3 unitTurn(desiredRotation);
			float turnMag = desiredRotation.Mag();
			unitTurn.Normalize();
			newInstanceMatrix.Rotate(unitTurn, turnMag);
		}

		if(!desiredTranslation.IsZero())
		{
			newInstanceMatrix.d.Add(desiredTranslation);

			if(deltaTime >= SMALL_FLOAT)
			{
				// Extrapolate the translation into a velocity. My main
				// worry with this is that if there is a delay of a frame
				// between when the translation is set and when it's really
				// applied, an uneven framerate could have bad effects on
				// the velocity.
				Vector3 vel;
				vel.Scale(desiredTranslation, invDeltaTime);
				SetSuspendVelocity(vel);
			}
			// Note: if for some reason TIME.GetSeconds() is 0 or really small,
			// we just keep the old value for crMoverInst::m_SuspendVelocity,
			// since we can't do any reasonable extrapolation from the
			// translation.
		}
		else
		{
			// Make sure to clear the velocity in the mover instance,
			// since we're not moving.
			SetSuspendVelocity(VEC3_ZERO);
		}

		Teleport(newInstanceMatrix);
	}

	//Collider update
	m_Collided = m_CollidedCached;
	m_CollidedCached = false;

	m_Obstructed = m_ObstructedCached;
	m_ObstructedCached = false;

	phIntersection iSect;

	// Check about getting out of recovery mode.
	if(IsRecovering())
	{
		if(IsAttached())
		{
			// If we get attached to something we can leave recovery mode.
			EndRecovery();
		}
		else
		{
			// Check for ground underneath us.
			phSegment groundProbeSegment;
			groundProbeSegment.A.Set(RCC_VECTOR3(GetPosition()));
			groundProbeSegment.A.z += GetFloatHeight() + sm_GroundProbeHeightFactor * phSimulator::GetAllowedPenetration();
			groundProbeSegment.B.Set(RCC_VECTOR3(GetPosition()));
			groundProbeSegment.B.z -= m_RecoveryHeightTolerance;

			if(PHLEVEL->TestProbe(groundProbeSegment, &iSect, m_ProbeExclusionInst))
			{
				Assert(iSect.IsAHit());
				// should be starting from the inside of the capsule and hence should never hit the capsule
				Assert(iSect.GetInstance() != this);

				EndRecovery();
			}
			iSect.Zero();
		}
	}

	Assert(!IsAttached() || IsSuspended());
	if(!IsRecovering())
	{
		if(!IsSuspended())
		{
			Assert(PHLEVEL->IsActive(GetLevelIndex()));

			// Obtain ground data, either from the cached data if we're not moving much and
			//   it's not too old, or else from a probe.

			// If it hasn't been too long since our last probe and we're not moving too quickly, let's just use the results we got last time.
			// Be more sensitive to vertical movement if we're stationary.  The reason for this is that the very slight oscillations that you get from setting
			//   this threshold too high are noticeable when you're standing still are not at all noticeable when you're moving even slightly.
			const float zVelocityThreshold = RCC_VECTOR3(m_Collider.GetVelocity()).XYMag2() > square(0.1f) ? 0.10f : 0.01f;  // TODO --- MAGIC NUMBERS, maybe expose eventually
			const bool groundCanMove = m_GroundInstance != NULL && m_GroundInstance->IsInLevel() && !PHLEVEL->IsFixed(m_GroundInstance->GetLevelIndex());
			if(m_GroundProbeTimer < m_GroundProbeUpdateDelay && (fabs(m_Collider.GetVelocity().GetZf()) < zVelocityThreshold) && !groundCanMove)  // TODO --- YUP
			{
				m_GroundProbeTimer += deltaTime;
			}
			else
			{
				m_GroundProbeTimer = 0.f;

				// probe downward and test for a surface against which to exert a force.
				// Create and compute a segment for the probe that we will be doing.
				// NOTE: In the future, if the capsule can be oriented at all, we will need to take the up-vector of the instance matrix into account.
				// If iSect was used before by the recovery probe, it will have been cleared out afterward so we don't need to do that here.

				// Do a single probe downward to look for ground.
				phSegment groundProbeSegment;
				groundProbeSegment.A.Set(RCC_VECTOR3(GetPosition()));
				groundProbeSegment.A.z += GetFloatHeight() + sm_GroundProbeHeightFactor * phSimulator::GetAllowedPenetration();  // TODO --- YUP
				groundProbeSegment.B.Set(RCC_VECTOR3(GetPosition()));
				groundProbeSegment.B.z -= m_GroundProbePenetration;  // TODO --- YUP
				const bool probeHit = PHLEVEL->TestProbe(groundProbeSegment, &iSect, m_ProbeExclusionInst) != 0;

				// If our ground probe hit something, get the data from it.  Otherwise, fill in data that records that we didn't hit anything.
				if(probeHit)
				{
					m_GroundProbeDepth = iSect.GetDepth();
					m_LastGroundNormal.Set(RCC_VECTOR3(iSect.GetNormal()));
					const phMaterialMgr::Id materialId = iSect.GetMaterialId();
					m_GroundMaterial = &MATERIALMGR.GetMaterial(materialId);
					m_GroundInstance = iSect.GetInstance();
					m_GroundInstanceComponent = iSect.GetComponent();
					m_GroundInstancePos_WS.Set(RCC_VECTOR3(iSect.GetPosition()));

					// As an optimization, we won't record these values (m_GroundInstancePos_CS, m_RightVector_WS, m_RightVector_CS) if the instance on which we're
					//   standing is fixed, since we won't be trying to determine platform motion in that case (and which is the more common case).
					if(!PHLEVEL->IsFixed(m_GroundInstance->GetLevelIndex()))
					{
						m_RightVector_WS.Set(RCC_MATRIX34(GetMatrix()).a);

						// Get the position in the local space of the component that we found.
						RCC_MATRIX34(m_GroundInstance->GetMatrix()).UnTransform(m_GroundInstancePos_WS, m_GroundInstancePos_CS);
						RCC_MATRIX34(m_GroundInstance->GetMatrix()).UnTransform3x3(m_RightVector_WS, m_RightVector_CS);
						if(m_GroundInstance->GetArchetype()->GetBound()->GetType() == phBound::COMPOSITE)
						{
							Assert(m_GroundInstanceComponent != (u16)(-1));
							const phBoundComposite *compositeBound = static_cast<const phBoundComposite *>(m_GroundInstance->GetArchetype()->GetBound());
							RCC_MATRIX34(compositeBound->GetCurrentMatrix(m_GroundInstanceComponent)).UnTransform(m_GroundInstancePos_CS);
							RCC_MATRIX34(compositeBound->GetCurrentMatrix(m_GroundInstanceComponent)).UnTransform3x3(m_RightVector_CS);
						}
					}
				}
				else
				{
					m_GroundProbeDepth = -1.0f;
					m_GroundMaterial = NULL;
					m_GroundInstance = NULL;
				}
			}

			if(m_GroundProbeDepth > 0.0f)
			{
				// Apply a 'modified' dampened spring force.  See below for details on what 'modified' means.
				// There are two functions of this section of code.  One function is to keep the mover capsule floating a minimum height above the ground below it.  This occurs anytime
				//   we're not too far away from the ground, which is exactly when iSect.Depth >= kGroundProbePenetration.  The other function is to make the mover 'stick' to the ground
				//   a little bit for when heading 'down' from one ramp to the next.

				// This tells us whether we're coming toward or away from the ground ... it's a useful thing to know.
				bool isSeparating = RCC_VECTOR3(m_Collider.GetVelocity()).Dot(m_LastGroundNormal) >= 0.0f;

				// This tells us whether we'd be exerting a compressive or a suction force.  It seems like it would be a useful thing to know but maybe it's not so useful after all.
				//				bool isCompressing = iSect.GetDepth() > groundProbeGroundPenetration;

				// We only want to apply these forces if we're either not jumping or if we're jumping but on our way back down.
				if(!IsJumping() || !isSeparating)
				{
					Vector3 force(ORIGIN);

					// This is the spring part of the force.  It will either push us away from the ground or pull us toward it.
					const float kEffectiveDepth = Min(m_GroundProbeDepth - m_GroundProbePenetration, m_FloatHeight * sm_EffectiveDepthFactor);
					float fForce = sm_SpringConstant * m_Collider.GetMass() * GRAVITY * GetArchetype()->GetGravityFactor() * kEffectiveDepth;

					// This is the dampening part of the force.
					const float kVelocityAlongNormal = m_LastGroundNormal.Dot(RCC_VECTOR3(m_Collider.GetVelocity()));
					fForce += (sm_DampeningConstant * m_Collider.GetMass() * kVelocityAlongNormal);

					// This is the 'modified' portion of the force.  Basically, this force is here to allow the instance to be fully supported when the 'spring' is neither compressed
					//   nor stretched.
					fForce += -1.0f * m_Collider.GetMass() * GRAVITY * GetArchetype()->GetGravityFactor();

					force = m_LastGroundNormal * fForce;
					m_Collider.ApplyForceCenterOfMass(force, ScalarV(deltaTime).GetIntrin128ConstRef());

					// With whatever force we are pushing ourselves upward we should also be pushing what we're standing on downward.
					// However, we don't want to affect the other object if we're applying a 'suction' force to the character to keep him on the ground.
					// We have to check IsInLevel() here because we might be using a cached value for the instance and it's possible that it's not
					//   in the level any more.
					if(force.z > 0.0f && m_GroundInstance && m_GroundInstance->IsInLevel())  // TODO --- YUP
					{
						// NOTE: Here we are also assuming that force has no x or z components and that the normal of what we're standing on is straight up.  Otherwise we should really only
						//   be negating the component parallel to the normal of whatever we're standing on.  <-- Is this actually true?
						force.Negate();
						PHSIM->ApplyForce(ScalarV(deltaTime).GetIntrin128ConstRef(), m_GroundInstance->GetLevelIndex(), force);
					}
				}

				m_IsOnGround = true;
			}
			else
			{
				// Our ground probes missed.
				m_IsOnGround = false;
			}

			if(IsJumping() && m_Collider.GetVelocity().GetZf() < 0.0f && m_IsOnGround) // TODO --- YUP
			{
				EndJump();
			}
		}
		else
		{
			if(IsAttached())
			{
				Matrix34 newMoverMatrix(m_AttachmentMatrix);
				// Ignore the component for now and assume that we're attached directly to the instance.
				Assert(m_AttachmentComponent == (u16)(-1));
				newMoverMatrix.Dot(RCC_MATRIX34(m_AttachmentInst->GetMatrix()));

				// The product of two matrices that pass the IsOrthonormal() test might not, itself, pass the IsOrthonormal() test.
				// If, at the end of this, we're still not orthonormal, let's just fix that up the hard way.
				if(!newMoverMatrix.IsOrthonormal())
				{
					newMoverMatrix.Normalize();
				}

				Teleport(newMoverMatrix);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

Vec3V_Out crMoverInst::GetExternallyControlledVelocity() const
{
	// Nobody should be asking us about our velocity if we're active (they should be asking our collider instead).
	Assert(!IsInLevel() || !PHLEVEL->IsActive(GetLevelIndex()));

	Vector3 velocity;

	// Really it should be an error if the instance to which we're attached isn't in the level any more, but it seems to be
	//   happening sometimes (an could conceivably just be a timing thing).
	if(IsAttached() && m_AttachmentInst->IsInLevel())
	{
		if(PHLEVEL->IsActive(m_AttachmentInst->GetLevelIndex()))
		{
			// Return the velocity of the collider of the instance to which we're attached.
			const phCollider *attachmentCollider = PHSIM->GetCollider(m_AttachmentInst);
			Assert(attachmentCollider != NULL);
			velocity.Set(RCC_VECTOR3(attachmentCollider->GetVelocity()));
		}
		else
		{
			velocity = VEC3V_TO_VECTOR3(m_AttachmentInst->GetExternallyControlledVelocity());
		}
	}
	else
	{
		velocity.Set(m_SuspendVelocity);
	}

	return RCC_VEC3V(velocity);
}

////////////////////////////////////////////////////////////////////////////////

Vec3V_Out crMoverInst::GetExternallyControlledAngVelocity() const
{
	// Nobody should be asking us about our velocity if we're active (they should be asking our collider instead).
	Assert(!IsInLevel() || !PHLEVEL->IsActive(GetLevelIndex()));

	Vector3 angVelocity;

	// Really it should be an error if the instance to which we're attached isn't in the level any more, but it seems to be
	//   happening sometimes (an could conceivably just be a timing thing).
	if(IsAttached() && m_AttachmentInst->IsInLevel())
	{
		if(PHLEVEL->IsActive(m_AttachmentInst->GetLevelIndex()))
		{
			// Return the angular velocity of the collider of the instance to which we're attached.
			const phCollider *attachmentCollider = PHSIM->GetCollider(m_AttachmentInst);
			Assert(attachmentCollider != NULL);
			angVelocity.Set(RCC_VECTOR3(attachmentCollider->GetAngVelocity()));
		}
		else
		{
			angVelocity = VEC3V_TO_VECTOR3(m_AttachmentInst->GetExternallyControlledAngVelocity());
		}
	}
	else
	{
		// No better way to report our angular velocity at this time.
		angVelocity.Zero();
	}

	return RCC_VEC3V(angVelocity);
}

////////////////////////////////////////////////////////////////////////////////

bool crMoverInst::ShouldFindImpacts(const phInst* otherInst) const
{
	if(otherInst == m_CollisionExclusionInst)
	{
		// This is one of the instances that we've been specifically told not to collide with.
		return false;
	}

	if(IsSuspended())
	{
		// If we're suspended then we just don't want to collide with anything.
		return false;
	}

	if(IsRecovering())
	{
		// If we are recovering, then we're all right with colliding with fixed bounds, possibly as they're paging in.
		if(!PHLEVEL->IsFixed(otherInst->GetLevelIndex()))
		{
			return false;
		}
	}

	return phInst::ShouldFindImpacts(otherInst);
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::PreComputeImpacts(phContactIterator impacts)
{
	Vec3V myPos = GetCenterOfMass();

    phContactIterator deepestImpact = impacts;

	// Look through all of the impacts, making any changes and recording any info that we need.
    for(phContactIterator curImpact = impacts; !curImpact.AtEnd(); ++curImpact)
	{
		// First let's kill the friction and elasticity on each of the impacts!
		curImpact.SetFriction(0.0f);
		curImpact.SetElasticity(0.0f);

		// Find the deepest impact.
		if(curImpact.GetDepth() > deepestImpact.GetDepth())
		{
			deepestImpact = curImpact;
		}

		Vec3V normal;
		curImpact.GetMyNormal(normal);	

		Vec3V impactPos(curImpact.GetMyPosition());
		Vec3V moverToImpact(impactPos - myPos);

		// Vector from mover point to impact point should be in a direction
		// opposed to the impact normal.  Otherwise, we can ignore it for
		// determining whether it's a wall.
		if (IsLessThanAll(Dot(moverToImpact, normal), ScalarV(V_ZERO)))
		{
			// If the angle between the normal and the up vector is more than
			// ~45 degrees ( MAGIC NUM ) then we consider it an obstruction.
			if (normal.GetZf() <= sm_ObstructionTolerance)
			{
				m_ObstructedCached = true;
			}
		}
	}

	m_CollidedCached = true;
}

////////////////////////////////////////////////////////////////////////////////
phInst *crMoverInst::PrepareForActivation(phCollider **colliderToUse, phInst* UNUSED_PARAM(otherInst), const phConstraintBase * UNUSED_PARAM(constraint))
{
	if(IsSuspended() || IsRecovering())
	{
		return NULL;
	}
	Assert(colliderToUse != NULL);
	*colliderToUse = &m_Collider;
	return this;
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::MakePhysical()
{
	m_ArchetypeDamped.SetGravityFactor(m_GravityFactor);
	if(RCC_MATRIX34(GetMatrix()).b.z < sm_UprightTolerance)
	{
		Matrix34 newInstanceMatrix(RCC_MATRIX34(GetMatrix()));
		newInstanceMatrix.MakeUpright();
		Teleport(newInstanceMatrix);
	}
	PHSIM->ActivateObject(GetLevelIndex());
	phConstraintCylindrical::Params constraintParams;
	constraintParams.instanceA = this;
	constraintParams.worldAxis = Vec3V(V_Y_AXIS_WZERO);
	PHCONSTRAINT->Insert(constraintParams);
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::EndPhysical()
{
	// can fail if were not in level, which can be the case
	if(IsInLevel())
	{
		if(PHLEVEL->IsActive(GetLevelIndex()))
		{
			Assert(PHLEVEL->IsActive(GetLevelIndex()));
			PHSIM->DeactivateObject(GetLevelIndex());
		}
	}

	m_ArchetypeDamped.SetGravityFactor(0.0f);
	m_Collider.SetVelocity(ORIGIN);
	m_Collider.SetAngVelocity(ORIGIN);

	m_IsOnGround = false;
	m_GroundProbeDepth = -1.0f;
	m_GroundMaterial = NULL;
	m_GroundInstance = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crMoverInst::Reset()
{
	Assert(!IsInLevel());

	m_AttachmentMatrix = M34_IDENTITY;

	m_LastGroundNormal = YAXIS; // TODO -- YUP
	m_GroundInstancePos_WS = ORIGIN;
	m_GroundInstancePos_CS = ORIGIN;
	m_RightVector_WS = ORIGIN;
	m_RightVector_CS = ORIGIN;
	m_SuspendVelocity = ORIGIN;

	m_CollisionExclusionInst = NULL;
	m_ProbeExclusionInst = NULL;
	m_AttachmentInst = NULL;
	m_GroundInstance = NULL;
	m_GroundMaterial = NULL;

	m_GroundProbeDepth = -1.f;
	m_RecoveryHeightTolerance = -1.f;
	m_FloatHeight = 0.f;
	m_LateralSlide = 0.f;
	m_LinearSlide = 0.f;
	m_LateralAccelCap = sm_DefaultAccelCap;
	m_LinearAccelCap = sm_DefaultAccelCap;
	m_GravityFactor = sm_DefaultGravityFactor;
	m_SlopeFactor = sm_DefaultSlopeFactor;
	m_GroundProbePenetration = sm_DefaultGroundProbePenetration;
	m_GroundProbeTimer = 0.f;
	m_GroundProbeUpdateDelay = sm_DefaultGroundProbeUpdateDelay;

	m_IsSuspendedCount = 0;

	m_AttachmentComponent = u16(-1);
	m_GroundInstanceComponent = u16(-1);

	m_IsOnGround = false;
	m_IsJumping = false;
	m_IsRecovering = false;
	m_CollidedCached = false;
	m_Collided = false;
	m_ObstructedCached = false;
	m_Obstructed = false;

	// Initialize the character bound with some arbitrary but reasonable default values
	SetCharacterSize(sm_DefaultCharacterRadius, sm_DefaultCharacterHeight, sm_DefaultCharacterFloatHeight, sm_DefaultCharacterExtraBoundOffset);

	// Get our archetype set up
	m_ArchetypeDamped.SetBound(&m_CapsuleBound);
	m_ArchetypeDamped.SetGravityFactor(m_GravityFactor);
	m_ArchetypeDamped.SetIncludeFlags(INCLUDE_FLAGS_ALL);
	m_ArchetypeDamped.SetFilename("Mover");

	// Get our instance set up
	SetMatrix( Mat34V(V_IDENTITY) );
	SetArchetype(&m_ArchetypeDamped);

	// Get our collider set up
	m_Collider.SetInstanceAndReset(this);
	m_Collider.SetMaxAngSpeed(sm_MaxAngSpeed);

	SetMass(sm_DefaultCharacterMass);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
