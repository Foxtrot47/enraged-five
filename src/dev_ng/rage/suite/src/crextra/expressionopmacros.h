//
// crextra/expressionopmacros.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CREXTRA_EXPRESSIONOPMACROS_H
#define CREXTRA_EXPRESSIONOPMACROS_H

#include "expressionops.h"

#if CR_DEV
namespace rage
{

namespace crMakeExpressionOp
{

crExpressionOp* Constant(const crExpressionOp::Value& val);
crExpressionOp* ConstantFloat(float f);

crExpressionOp* Get(u8 track, u16 id, u8 type);
crExpressionOp* GetQuaternion(u8 track, u16 id);
crExpressionOp* GetVector3(u8 track, u16 id);
crExpressionOp* GetFloat(u8 track, u16 id);
crExpressionOp* GetInt(u8 track, u16 id);
crExpressionOp* GetBoneRotation(u16 id);
crExpressionOp* GetBoneTranslation(u16 id);
crExpressionOp* GetBoneScale(u16 id);
crExpressionOp* GetDefaultBoneRotation(u16 id);
crExpressionOp* GetDefaultBoneTranslation(u16 id);
crExpressionOp* GetDefaultBoneScale(u16 id);
crExpressionOp* GetMoverRotation();
crExpressionOp* GetMoverTranslation();
crExpressionOp* GetMoverScale();
crExpressionOp* GetVariable(u32 hash);

crExpressionOp* Set(u8 track, u16 id, u8 type, crExpressionOp* op);
crExpressionOp* SetQuaternion(u8 track, u16 id, crExpressionOp* op);
crExpressionOp* SetVector3(u8 track, u16 id, crExpressionOp* op);
crExpressionOp* SetFloat(u8 track, u16 id, crExpressionOp* op);
crExpressionOp* SetInt(u8 track, u16 id, crExpressionOp* op);
crExpressionOp* SetBoneRotation(u16 id, crExpressionOp* op);
crExpressionOp* SetBoneTranslation(u16 id, crExpressionOp* op);
crExpressionOp* SetBoneScale(u16 id, crExpressionOp* op);
crExpressionOp* SetMoverRotation(crExpressionOp* op);
crExpressionOp* SetMoverTranslation(crExpressionOp* op);
crExpressionOp* SetMoverScale(crExpressionOp* op);
crExpressionOp* SetVariable(u32 hash, crExpressionOp* op);

crExpressionOp* Valid(u8 track, u16 id, u8 type);
crExpressionOp* ValidBoneRotation(u16 id);
crExpressionOp* ValidBoneTranslation(u16 id);
crExpressionOp* ValidBoneScale(u16 id);
crExpressionOp* ValidMoverRotation();
crExpressionOp* ValidMoverTranslation();
crExpressionOp* ValidMoverScale();

crExpressionOp* ComponentGet(u8 track, u16 id, u8 type, u8 component, u8 order);
crExpressionOp* ComponentGetEuler(u8 track, u16 id, u8 component, u8 order);
crExpressionOp* ComponentGetVector3(u8 track, u16 id, u8 component);
crExpressionOp* ComponentGetBoneRotation(u16 id, u8 component, u8 order);
crExpressionOp* ComponentGetBoneTranslation(u16 id, u8 component);
crExpressionOp* ComponentGetBoneScale(u16 id, u8 component);
crExpressionOp* ComponentGetDefaultBoneRotation(u16 id, u8 component, u8 order);
crExpressionOp* ComponentGetDefaultBoneTranslation(u16 id, u8 component);
crExpressionOp* ComponentGetDefaultBoneScale(u16 id, u8 component);
crExpressionOp* ComponentGetMoverRotation(u8 component, u8 order);
crExpressionOp* ComponentGetMoverTranslation(u8 component);
crExpressionOp* ComponentGetMoverScale(u8 component);

crExpressionOp* ComponentSet(u8 track, u16 id, u8 type, u8 component, u8 order, crExpressionOp* op);
crExpressionOp* ComponentSetEuler(u8 track, u16 id, u8 component, u8 order, crExpressionOp* op);
crExpressionOp* ComponentSetVector3(u8 track, u16 id, u8 component, crExpressionOp* op);
crExpressionOp* ComponentSetBoneRotation(u16 id, u8 component, u8 order, crExpressionOp* op);
crExpressionOp* ComponentSetBoneTranslation(u16 id, u8 component, crExpressionOp* op);
crExpressionOp* ComponentSetBoneScale(u16 id, u8 component, crExpressionOp* op);
crExpressionOp* ComponentSetMoverRotation(u8 component, u8 order, crExpressionOp* op);
crExpressionOp* ComponentSetMoverTranslation(u8 component, crExpressionOp* op);
crExpressionOp* ComponentSetMoverScale(u8 component, crExpressionOp* op);

crExpressionOp* ObjectSpaceGet(u8 track, u16 id, u8 type);
crExpressionOp* ObjectSpaceGetBoneRotation(u16 id);
crExpressionOp* ObjectSpaceGetBoneTranslation(u16 id);

crExpressionOp* ObjectSpaceSet(u8 track, u16 id, u8 type, crExpressionOp* op);
crExpressionOp* ObjectSpaceSetBoneRotation(u16 id, crExpressionOp* op);
crExpressionOp* ObjectSpaceSetBoneTranslation(u16 id, crExpressionOp* op);

crExpressionOp* ObjectSpaceConvertTo(u8 track, u16 id, u8 type, crExpressionOp* op);
crExpressionOp* ObjectSpaceConvertToBoneRotation(u16 id, crExpressionOp* op);
crExpressionOp* ObjectSpaceConvertToBoneTranslation(u16 id, crExpressionOp* op);

crExpressionOp* ObjectSpaceConvertFrom(u8 track, u16 id, u8 type, crExpressionOp* op);
crExpressionOp* ObjectSpaceConvertFromBoneRotation(u16 id, crExpressionOp* op);
crExpressionOp* ObjectSpaceConvertFromBoneTranslation(u16 id, crExpressionOp* op);

crExpressionOp* Nullary(crExpressionOpNullary::eNullaryOpTypes nullaryOpType);
crExpressionOp* Unary(crExpressionOpUnary::eUnaryOpTypes unaryOpType, crExpressionOp* op);
crExpressionOp* Binary(crExpressionOpBinary::eBinaryOpTypes binaryOpType, crExpressionOp* op0, crExpressionOp* op1);
crExpressionOp* Ternary(crExpressionOpTernary::eTernaryOpTypes ternaryOpType, crExpressionOp* op0, crExpressionOp* op1, crExpressionOp* op2);
crExpressionOp* Nary(crExpressionOpNary::eNaryOpTypes naryOpType, crExpressionOp* op0, crExpressionOp* op1=NULL, crExpressionOp* op2=NULL, crExpressionOp* op3=NULL, crExpressionOp* op4=NULL, crExpressionOp* op5=NULL, crExpressionOp* op6=NULL, crExpressionOp* op7=NULL);

crExpressionOp* SpecialBlend(crExpressionOp* op0, float weight0, crExpressionOp* op1=NULL, float weight1=0.f, crExpressionOp* op2=NULL, float weight2=0.f, crExpressionOp* op3=NULL, float weight3=0.f, crExpressionOp* op4=NULL, float weight4=0.f, crExpressionOp* op5=NULL, float weight5=0.f, crExpressionOp* op6=NULL, float weight6=0.f, crExpressionOp* op7=NULL, float weight7=0.f);

crExpressionOp* SpecialCurve(float in0, float out0, float in1, float out1, crExpressionOp* op);
crExpressionOp* SpecialCurve(float in0, float out0, float in1, float out1, float in2, float out2, crExpressionOp* op);

crExpressionOp* SpecialLookAt(u8 axisLookAt, u8 axisLookAtUp, u8 axisOriginPos, QuatV_In offset, crExpressionOp* opLookAtPos, crExpressionOp* opLookAtRot, crExpressionOp* opOriginPos);

} // namespace crMakeExpressionOp

} // namespace rage

#endif // CR_DEV

#endif // CREXTRA_EXPRESSIONOPMACROS_H
