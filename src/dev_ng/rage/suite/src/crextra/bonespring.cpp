//
// crextra/bonespring.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


/*
//
// pongfx/bonespringanim.cpp
//
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#include "bonespringanim.h"

#include "ponggamedata/crdata.h"
#include "ponggamedata/chardata.h"
#include "ponggamedata/replay.h"
#include "ponggamedata/timemgr.h"

#include "pongcreature/animinfo.h"
#include "pongplayer/pongplayer.h"

#include "data/resourcehelpers.h"
#include "file/asset.h"
#include "vector/vector3.h"
#include "crskeleton/skeletondata.h"
#include "crskeleton/skeleton.h"
#include "cranimation/animation.h"
#include "cranimation/animtrack.h"
#include "phbound/boundsphere.h"
#include "physics/archetype.h"
#include "physics/collider.h"
#include "physics/inst.h"
#include "physics/simulator.h"
#include "physics/levelnew.h"
#include "system/memory.h"


#if __BANK
#include "bank/bank.h"
#endif


//////////////////////////////////////////////////////////////////////////
fxBoneSpringAnimSpring::fxBoneSpringAnimSpring (const Vector3* pos, float springConstant, float springLength, float springDampening, float motionScale, Vector3& systemDampening)
	:  m_SpringDampening(springDampening), m_MotionScale(motionScale) , m_SpringAnimForceEnableX(false), m_SpringAnimForceEnableY(false), m_SpringAnimForceEnableZ(false), phSpringAninmatedAttachment(Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), springConstant, springLength, springDampening) {
	if(pos)
		m_Pos.Set(*pos);
	m_BoundSphere = rage_new phBoundSphere();
	m_Archetype = rage_new phArchetypeDamp();
	m_Archetype->ActivateDamping(phArchetypeDamp::LINEAR_V, systemDampening);
	m_ColliderEnd = rage_new phCollider();
	m_InstanceEnd = rage_new phInst();
	m_BoundSphere->SetSphereRadius(0.005f);
	m_FrameIndices.Resize(fxBoneSpringAnim::kMaxNumBlendFrames);
	for(int i=0; i<fxBoneSpringAnim::kMaxNumBlendFrames;i++)
		m_FrameIndices[i] = -1;
}


fxBoneSpringAnimSpring::~fxBoneSpringAnimSpring()
{
	m_Frames.Reset();
	m_FramePtrs.Reset();
	m_FrameWeights.Reset();
	m_FrameIndices.Reset();
}

#if __BANK
void fxBoneSpringAnimSpring::Save(fiTokenizer& tok)
{
	tok.Put("SpringConstant:\t");
	tok.Put(GetSpringConstant());
	tok.Put("\nSpringLength:\t");
	tok.Put(GetSpringLength());
	tok.Put("\nSpringDampening:\t");
	tok.Put(m_SpringDampening);
	tok.Put("\nSystemDampening:\t");
	tok.Put(m_Archetype->GetDampingConstant(phArchetypeDamp::LINEAR_V));
	tok.Put("\nMotionScale:\t");
	tok.Put(m_MotionScale);
	tok.Put("\nSpringOffset:\t");
	tok.Put(m_SpringOffset);
	tok.Put("\nWeight:\t");
	tok.Put(m_Weight);
	tok.Put("\nNumBlendFrames:\t");
	int numBlendFrames = m_Frames.GetCount();
	tok.Put(numBlendFrames);
	for (int i=0; i<numBlendFrames; i++)
	{
		char frameName[64];
		tok.GetToken(frameName,64);
		if(m_FrameIndices[fxBoneSpringAnim::kCenterFrame] == i)
		{
			tok.Put("\nCenter");
		}
		else if(m_FrameIndices[fxBoneSpringAnim::kUpFrame] == i)
		{
			tok.Put("\nUp");
		}
		else if(m_FrameIndices[fxBoneSpringAnim::kDownFrame] == i)
		{
			tok.Put("\nDown");
		}
		else if(m_FrameIndices[fxBoneSpringAnim::kLeftFrame] == i)
		{
			tok.Put("\nLeft");
		}
		else if(m_FrameIndices[fxBoneSpringAnim::kRightFrame] == i)
		{
			tok.Put("\nRight");
		}
		else if(m_FrameIndices[fxBoneSpringAnim::kFrontFrame] == i)
		{
			tok.Put("\nFront");
		}
		else if(m_FrameIndices[fxBoneSpringAnim::kBackFrame] == i)
		{
			tok.Put("\nBack");
		}
#if __DEV
		else
		{
			Displayf("Hair blend frame order not specified properly, this is a code problem!");
			Assert(0);
		}
#endif
	}
	int numSpringBones = m_FinalFrame.GetNumDofs();
	tok.Put("\nNumAnimBones:\t");
	tok.Put(numSpringBones);
	tok.Put("\n");
	char* currentBoneName = m_BoneNames;
	for (int i=0; i<numSpringBones; ++i)
	{
		tok.Put(currentBoneName);
		tok.Put("\n");
		currentBoneName += strlen(currentBoneName) + 1;
	}
	tok.Put("\n");
	tok.Put("\n");
}

void fxBoneSpringAnimSpring::AddWidgets(bkBank& b)
{
	b.PushGroup(m_BoneNames,false);
	b.AddSlider("Spring Constant", &m_SpringConstant, 0.0f, 500.0f, 1.0f);
	b.AddSlider("Spring Length", &m_SpringLength, 0.0f, 3.0f, 0.01f);
	b.AddSlider("Spring Dampening", &m_SpringDampening, 0.0f, 20.0f, 0.1f);
	b.AddSlider("Spring Offset", &m_SpringOffset, -3.0f, 3.0f, 0.01f);
//	b.AddSlider("Weight", &sm_Spring.Weight, 0.0f, 300.0f, 0.1f);
	b.AddSlider("Motion Scale", &m_MotionScale, 0.0f, 500.0f, 1.0f);
	b.PopGroup();
}
#endif

void fxBoneSpringAnimSpring::Reset()
{
	Vector3 RestPosition(GetAttachedPositionOther());
	RestPosition.y -= m_SpringLength;
	m_Instance->SetPosition(RestPosition);
	if(m_Instance->IsInLevel())
		PHLEVEL->UpdateObjectLocation(m_Instance->GetLevelIndex());
	phSpringAninmatedAttachment::Reset();
	if(m_Instance->IsInLevel())
		PHSIM->GetCollider(m_Instance)->Reset();
}

//////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(fxBoneSpringAnim)

fxBoneSpringAnim::fxBoneSpringAnim() : m_Active(false), m_AlwaysActive(true), m_ResetRequested(false)
{
	m_Name[0] = 0;
}


fxBoneSpringAnim::~fxBoneSpringAnim()
{
	m_Spring.Reset();
}

fxBoneSpringAnim::fxBoneSpringAnim(datResource& rsc) : m_Spring(rsc)
{
	rsc.PointerFixup(m_Skeleton);
	for(int i=0; i<m_Spring.GetCount(); i++)
	{
		ObjectFixup(rsc, m_Spring[i]);
	}
}

void fxBoneSpringAnim::AddInstanceToLevel()
{
	for(int i=0; i<m_Spring.GetCount(); i++)
	{
		PHSIM->AddInstBehavior(*m_Spring[i]);
		PHSIM->AddActiveObject(m_Spring[i]->m_ColliderEnd);
	}
}

void fxBoneSpringAnim::RemoveInstanceFromLevel()
{
	for(int i=0; i<m_Spring.GetCount(); i++)
	{
		PHSIM->RemoveInstBehavior(*m_Spring[i]);
		PHSIM->DeleteObject(m_Spring[i]->m_ColliderEnd->GetLevelIndex());
	}
}

void fxBoneSpringAnimSpring::Update(float TimeStep)
{
	if(m_Archetype->GetDampingConstant(phArchetypeDamp::LINEAR_V).x != m_SpringDampening)
	{
		m_Archetype->ActivateDamping(phArchetypeDamp::LINEAR_V, Vector3(m_SpringDampening,m_SpringDampening,m_SpringDampening));
	}
	phSpringAninmatedAttachment::Update(TimeStep);
}

void fxBoneSpringAnim::Update()
{
	UpdateWeight();
	if((!m_Active && !m_GoalActive && m_Weight==0.0f && !m_AlwaysActive) || REPLAYMGR.IsPlayingBack()) return;

	// Save the current pose in the final frames, in case we're blending in/out.  This
	// allows us to blend the spring-influenced pose with the non-spring-influenced pose.
	for(int i=0; i<m_Spring.GetCount(); i++)
	{
		m_Spring[i]->m_FinalFrame.InversePose(*m_Skeleton);
	}
	for(int i=0; i<m_Spring.GetCount(); i++)
	{
		Vector3 WorldOffset;
		m_Spring[i]->m_BodyMatrix->Transform3x3(m_Spring[i]->m_SpringOffset,WorldOffset);
		m_Spring[i]->UpdateAttachmentPosition(m_Spring[i]->m_BodyMatrix->d + WorldOffset);

		if(m_ResetRequested) m_Spring[i]->Reset();
		m_Spring[i]->m_SpringAnimForce = m_Spring[i]->GetAttachedPositionOther() - m_Spring[i]->GetAttachedPositionThis();
		m_Spring[i]->m_SpringAnimForce.y += m_Spring[i]->GetSpringLength();

		Matrix34 TransformMtx(*m_Spring[i]->m_BodyMatrix);
		TransformMtx.FastInverse();
		TransformMtx.Transform3x3(m_Spring[i]->m_SpringAnimForce);
		m_Spring[i]->m_SpringAnimForce.Scale(m_Spring[i]->m_MotionScale);

		Vector3 ClampVec(m_Spring[i]->m_SpringAnimForce);
		
		ClampVec.Normalize();
		ClampVec.Abs();

		m_Spring[i]->m_SpringAnimForce.x = Clamp(m_Spring[i]->m_SpringAnimForce.x, -ClampVec.x, ClampVec.x);
		m_Spring[i]->m_SpringAnimForce.y = Clamp(m_Spring[i]->m_SpringAnimForce.y, -ClampVec.y, ClampVec.y);
		m_Spring[i]->m_SpringAnimForce.z = Clamp(m_Spring[i]->m_SpringAnimForce.z, -ClampVec.z, ClampVec.z);
		float totalWeight=0.0f;

		if(m_Spring[i]->m_SpringAnimForceEnableX)
		{
			float weight = GetWeight() * m_Spring[i]->m_SpringAnimForce.x * m_Spring[i]->m_SpringAnimForce.x;
			totalWeight += weight;
			if(m_Spring[i]->m_SpringAnimForce.x < 0)
			{
				m_Spring[i]->m_FrameWeights[m_Spring[i]->m_FrameIndices[kRightFrame]] = 0;
				m_Spring[i]->m_FrameWeights[m_Spring[i]->m_FrameIndices[kLeftFrame]] = weight;
			}
			else
			{
				m_Spring[i]->m_FrameWeights[m_Spring[i]->m_FrameIndices[kRightFrame]] = weight;
				m_Spring[i]->m_FrameWeights[m_Spring[i]->m_FrameIndices[kLeftFrame]] = 0;
			}
		}
		if(m_Spring[i]->m_SpringAnimForceEnableY)
		{
			float weight = GetWeight() * m_Spring[i]->m_SpringAnimForce.y * m_Spring[i]->m_SpringAnimForce.y;
			totalWeight += weight;
			if(m_Spring[i]->m_SpringAnimForce.y < 0)
			{
				m_Spring[i]->m_FrameWeights[m_Spring[i]->m_FrameIndices[kUpFrame]] = 0;
				m_Spring[i]->m_FrameWeights[m_Spring[i]->m_FrameIndices[kDownFrame]] = weight;
			}
			else
			{
				m_Spring[i]->m_FrameWeights[m_Spring[i]->m_FrameIndices[kUpFrame]] = weight;
				m_Spring[i]->m_FrameWeights[m_Spring[i]->m_FrameIndices[kDownFrame]] = 0;
			}
		}
		if(m_Spring[i]->m_SpringAnimForceEnableZ)
		{
			float weight = GetWeight() * m_Spring[i]->m_SpringAnimForce.z * m_Spring[i]->m_SpringAnimForce.z;
			totalWeight += weight;
			if(m_Spring[i]->m_SpringAnimForce.z < 0)
			{
				m_Spring[i]->m_FrameWeights[m_Spring[i]->m_FrameIndices[kBackFrame]] = 0;
				m_Spring[i]->m_FrameWeights[m_Spring[i]->m_FrameIndices[kFrontFrame]] = weight;
			}
			else
			{
				m_Spring[i]->m_FrameWeights[m_Spring[i]->m_FrameIndices[kBackFrame]] = weight;
				m_Spring[i]->m_FrameWeights[m_Spring[i]->m_FrameIndices[kFrontFrame]] = 0;
			}
		}
		m_Spring[i]->m_FrameWeights[m_Spring[i]->m_FrameIndices[kCenterFrame]] = GetWeight() * (1.0f - totalWeight >= 0.0f ? 1.0f - totalWeight : 0.0f);
		m_Spring[i]->m_FinalFrame.BlendN(m_Spring[i]->m_FrameWeights, m_Spring[i]->m_FramePtrs);
		m_Spring[i]->m_FinalFrame.Pose(*m_Skeleton);
		m_Skeleton->Update();
	}
	m_ResetRequested = false;
}

void fxBoneSpringAnim::UpdateWeight()
{
	if( !m_AlwaysActive && ((m_GoalActive != m_Active) || (m_Weight>0.0f && m_Weight<1.0f)) )
	{
		if( m_GoalDeltaTime>0.0f )
		{
			if( rage::Approach(m_Weight, (m_GoalActive ? 1.0f : 0.0f), (1.0f / m_GoalDeltaTime), PONGTIMEMGR.GetSeconds()) )
				m_Active = m_GoalActive;
		}
		else
		{
			m_Active = m_GoalActive;
			m_Weight = (m_Active ? 1.0f : 0.0f);
		}
	}
}

void fxBoneSpringAnim::Reset()
{
	m_Active = false;
	m_GoalActive = false;
	m_GoalDeltaTime = 0.0f;
	m_Weight = 0.0f;
	for(int i=0; i<m_Spring.GetCount(); i++)
	{
		m_Spring[i]->Reset();
	}
	m_ResetRequested = false;
}

void fxBoneSpringAnim::SetActive(bool active, float dTime)
{
	if( !m_AlwaysActive )
	{
		m_GoalActive = active;
		m_GoalDeltaTime = dTime;

		// If we're becoming active, reset all springs:
		if( !m_Active && m_GoalActive && m_Weight==0.0f )
		{
			for(int i=0; i<m_Spring.GetCount(); i++)
			{
				m_Spring[i]->Reset();
			}
		}
	}
}

bool fxBoneSpringAnim::Load(fiTokenizer& tok, const char* name, crSkeleton& skel, const pongAnimationInfo* animInfo)
{
	Assert(name);
	Assert(strlen(name) < BONE_SPRING_STR_LEN);
	safecpy(m_Name, name);

	int NumSprings = tok.MatchInt("NumSprings:");
	m_Spring.Resize(NumSprings);
	m_AlwaysActive = tok.MatchInt("AlwaysOn:") ? true : false;
	for(int springCount = 0; springCount < NumSprings; springCount++)
	{
		float SpringConstant = tok.MatchFloat("SpringConstant:");
		float SpringLength = tok.MatchFloat("SpringLength:");
		float SpringDampening = tok.MatchFloat("SpringDampening:");
		Vector3 SystemDampening;
		tok.MatchVector("SystemDampening:",SystemDampening);
		float MotionScale = tok.MatchFloat("MotionScale:") ;
		Vector3 Pos;
		m_Spring[springCount] = rage_new fxBoneSpringAnimSpring(	&Pos,	SpringConstant,	SpringLength, SpringDampening, MotionScale, SystemDampening);
		m_Spring[springCount]->m_Pos.Set(Pos);
		tok.MatchVector("SpringOffset:",m_Spring[springCount]->m_SpringOffset);	
		m_Spring[springCount]->m_Weight = tok.MatchFloat("Weight:");
	///////////////////////////////
		// hair tuning
		int numBlendFrames = tok.MatchInt("NumBlendFrames:");
		m_Spring[springCount]->m_Frames.Resize(numBlendFrames);
		m_Spring[springCount]->m_FramePtrs.Resize(numBlendFrames);
		m_Spring[springCount]->m_FrameWeights.Resize(numBlendFrames);
		for (int i=0; i<numBlendFrames; i++)
		{
			m_Spring[springCount]->m_FrameWeights[i] = 0.0f;
			char frameName[64];
			tok.GetToken(frameName,64);
			if(!stricmp(frameName, "Center"))
			{
				m_Spring[springCount]->m_FrameIndices[kCenterFrame] = i;
			}
			else if (!stricmp(frameName, "Up"))
			{
				m_Spring[springCount]->m_SpringAnimForceEnableY = true;
				m_Spring[springCount]->m_FrameIndices[kUpFrame] = i;
			}
			else if (!stricmp(frameName, "Down"))
			{
				m_Spring[springCount]->m_SpringAnimForceEnableY = true;
				m_Spring[springCount]->m_FrameIndices[kDownFrame] = i;
			}
			else if (!stricmp(frameName, "Left"))
			{
				m_Spring[springCount]->m_SpringAnimForceEnableX = true;
				m_Spring[springCount]->m_FrameIndices[kLeftFrame] = i;
			}
			else if (!stricmp(frameName, "Right"))
			{
				m_Spring[springCount]->m_SpringAnimForceEnableX = true;
				m_Spring[springCount]->m_FrameIndices[kRightFrame] = i;
			}
			else if (!stricmp(frameName, "Front"))
			{
				m_Spring[springCount]->m_SpringAnimForceEnableZ = true;
				m_Spring[springCount]->m_FrameIndices[kFrontFrame] = i;
			}
			else if (!stricmp(frameName, "Back"))
			{
				m_Spring[springCount]->m_SpringAnimForceEnableZ = true;
				m_Spring[springCount]->m_FrameIndices[kBackFrame] = i;
			}
	#if __DEV
			else
			{
				Displayf("Hair blend frame order not specified properly in %s", name);
				Assert(0);
			}
	#endif
			m_Spring[springCount]->m_FramePtrs[i] = &m_Spring[springCount]->m_Frames[i];
		}

		int numSpringBones = tok.MatchInt("NumAnimBones:");
		const crBoneData* boneData = 0;
		sysMemStartTemp();
		char** boneNames = rage_new char* [numSpringBones];
		sysMemEndTemp();

		int allBoneNamesStrLen = 0;
		for (int i=0; i<numSpringBones; ++i)
		{
			char boneName[64];
			tok.GetToken(boneName,64);
			int boneNameStrLen = strlen(boneName) + 1;
			sysMemStartTemp();
			boneNames[i] = rage_new char[boneNameStrLen];
			sysMemEndTemp();
			safecpy(boneNames[i], boneName);
			allBoneNamesStrLen += boneNameStrLen;
			boneData = skel.GetSkeletonData().FindBone(boneName);
			if (!boneData)
			{
				Quitf("fxBoneSpringAnim::Load() - cannot find bone '%s'!", boneName);
			}
			u16 boneID = boneData->GetBoneID();
			int k=0;
			for (int j=0; j<numBlendFrames; j++)
			{
				while(m_Spring[springCount]->m_FrameIndices[k] < 0) k++;									// skip frame indices for which we don't have frames.
				m_Spring[springCount]->m_Frames[m_Spring[springCount]->m_FrameIndices[k]].AddDof(kTrackBoneRotation, boneID, kFormatTypeQuaternion);
				k++;
			}
			m_Spring[springCount]->m_FinalFrame.AddDof(kTrackBoneRotation, boneID, kFormatTypeQuaternion);
		}
		m_Spring[springCount]->m_BoneNames = rage_new char[allBoneNamesStrLen];
		allBoneNamesStrLen = 0;
		for (int i=0; i<numSpringBones; ++i)
		{
			safecpy(&m_Spring[springCount]->m_BoneNames[allBoneNamesStrLen], boneNames[i]);
			allBoneNamesStrLen = strlen(boneNames[i]) + 1;
			sysMemStartTemp();
			delete boneNames[i];
			sysMemEndTemp();
		}

		sysMemStartTemp();
		delete boneNames;
		sysMemEndTemp();

		int j=0;
		for (int i=0; i<numBlendFrames; i++)
		{
			while(m_Spring[springCount]->m_FrameIndices[j] < 0) j++;									// skip frame indices for which we don't have frames.
			animInfo->Animation->CompositeFrame(animInfo->Animation->Convert30FrameToTime((float)i), m_Spring[springCount]->m_Frames[m_Spring[springCount]->m_FrameIndices[j]], true);
			j++;
		}
		///////////////////////////////////

		// assign matrix pointers
		m_Spring[springCount]->m_BodyMatrix = &skel.GetGlobalMtx(boneData);

		m_Spring[springCount]->m_Archetype->SetTypeFlags(0);
		m_Spring[springCount]->m_Archetype->SetBound(m_Spring[springCount]->m_BoundSphere);
		m_Spring[springCount]->m_Archetype->SetMass(m_Spring[springCount]->m_Weight);
		m_Spring[springCount]->m_InstanceEnd->SetArchetype(m_Spring[springCount]->m_Archetype);
		m_Spring[springCount]->m_ColliderEnd->Init(m_Spring[springCount]->m_InstanceEnd);
		m_Spring[springCount]->m_ColliderEnd->SetMatrix(M34_IDENTITY);
		m_Spring[springCount]->m_InstanceEnd->SetMatrix(M34_IDENTITY);
		if(m_Spring[springCount]->m_InstanceEnd->IsInLevel())
			PHLEVEL->UpdateObjectLocation(m_Spring[springCount]->m_InstanceEnd->GetLevelIndex());
		m_Spring[springCount]->SetInstance(*m_Spring[springCount]->m_InstanceEnd);
	}
	// m_BodyBound = &bodyBound;



	m_Skeleton = &skel;
	Reset();
	return true;
}

#if __BANK
void fxBoneSpringAnim::Save(pongPlayer* player)
{
	
	const char* modelname = CHARDATAMGR.GetCharDataByID(GAMEDATA.GetCharacterID(player->GetPlayerID()))->GetCrData().GetModelName();
	atArray<fxBoneSpringAnim*>& boneSpringAnim = player->GetBoneSpringAnimFx();
	
	atArray<char[64]> boneName;
	if (modelname[0] == 0) return;
	{
		ASSET.PushFolder("$/tune/character");
		fiStream* f = ASSET.Create(modelname, "bonespring");
		ASSET.PopFolder();
		if (!f) return;
		fiTokenizer tok("bonespring", f);
		tok.Put("NumAnims:\t");
		tok.Put(boneSpringAnim.GetCount());
		tok.Put("\n\n");
		for(int i=0; i<boneSpringAnim.GetCount(); i++)
		{
			tok.Put(boneSpringAnim[i]->m_Name);
			int NumSprings = boneSpringAnim[i]->m_Spring.GetCount();
			tok.Put("\nNumSprings:\t");
			tok.Put(NumSprings);
			tok.Put("\n\n");
			for(int springCount = 0; springCount < NumSprings; springCount++)
			{
				boneSpringAnim[i]->m_Spring[springCount]->Save(tok);
			}
		}
		f->Close();
	}
	boneName.Reset();
}

void fxBoneSpringAnim::AddWidgets(bkBank &b)
{
	b.PushGroup(m_Name, false);	
	for(int i=0; i<m_Spring.GetCount(); i++)
	{
		m_Spring[i]->AddWidgets(b);
	}
	b.PopGroup();
}
#endif
*/
