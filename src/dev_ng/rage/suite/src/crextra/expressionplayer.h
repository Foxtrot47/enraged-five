//
// crextra/expressionplayer.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CREXTRA_EXPRESSIONPLAYER_H
#define CREXTRA_EXPRESSIONPLAYER_H

#include "expressions.h"

#include "atl/array.h"
#include "atl/functor.h"
#include "data/struct.h"
#include "vectormath/vectormath.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

class crExpressions;
class crFrame;
class crSkeletonData;


// PURPOSE: Expression player
class crExpressionPlayer
{
public:

	// PURPOSE: Default constructor
	crExpressionPlayer();

	// PURPOSE: Initializing constructor
	// PARAMS:
	// expressions - expressions to play
	// skelData - optional skeleton data class, required by some types of expression ops
	crExpressionPlayer(crExpressions& expressions, const crSkeletonData* skelData=NULL);

	// PURPOSE: Destructor
	~crExpressionPlayer();

	// PURPOSE: Shutdown, free / release dynamic use
	void Shutdown();

	// PURPOSE: Initialization
	// PARAMS:
	// expressions - expressions to play
	// skelData - optional skeleton data class, required by some types of expression ops
	void Init(const crExpressions& expressions, const crSkeletonData* skelData=NULL);

	// PURPOSE: Update player
	// PARAMS: deltaTime - time elapsed (in seconds) since last update
	void Update(float deltaTime);

	// PURPOSE: Set expressions (can be NULL)
	void SetExpressions(const crExpressions* expressions);

	// PURPOSE: Get current expressions (can be NULL)
	const crExpressions* GetExpressions() const;

	// PURPOSE: Set skeleton data (can be NULL)
	void SetSkeletonData(const crSkeletonData* skelData);

	// PURPOSE: Get current skeleton data (can be NULL)
	const crSkeletonData* GetSkeletonData() const;

	// PURPOSE: Set current total time
	void SetTime(float time);

	// PURPOSE: Get current total time
	float GetTime() const;

	// PURPOSE: Set current delta time
	void SetDeltaTime(float deltaTime);

	// PURPOSE: Get current delta time
	float GetDeltaTime() const;

	// PURPOSE: Set expression processing enabled
	void SetEnabled(bool enabled);

	// PURPOSE: Is expression processing enabled
	bool IsEnabled() const;

	// PURPOSE: Set accelerator to lock indices
	void SetFunctor(const Functor0& functor);

	// PURPOSE: Set variable
	// PARAMS: hash - hash value identifying variable
	// val - new variable value
	// RETURNS: true - if variable successfully set
	bool SetVariable(u32 hash, Vec4V_In val);

	// PURPOSE: Set variable
	// PARAMS: hash - hash value identifying variable
	// val - existing variable value, if successfully retrieved (undefined otherwise)
	// RETURNS: true - if variable successfully retrieved
	bool GetVariable(u32 hash, Vec4V_InOut val) const;
		
	// PURPOSE: Get pointer to variable stack 
	// RETURNS: Pointer to variables (can return NULL)
	Vec4V_ConstPtr GetVariablesPtr() const;
	Vec4V_Ptr GetVariablesPtr();

	// PURPOSE: Get number of variables in stack
	u32 GetNumVariables() const;

private:
	datRef<const crExpressions> m_Expressions;
	datRef<const crSkeletonData> m_SkeletonData;
	Functor0 m_ExpressionsCallback;
	atArray<Vec4V> m_Variables;

	float m_Time;
	float m_DeltaTime;
	bool m_Enabled;
};

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionPlayer::Update(float deltaTime)
{
	m_Time += deltaTime;
	m_DeltaTime = deltaTime;
}

////////////////////////////////////////////////////////////////////////////////

inline const crExpressions* crExpressionPlayer::GetExpressions() const
{
	return m_Expressions;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionPlayer::SetSkeletonData(const crSkeletonData* skelData)
{
	m_SkeletonData = skelData;
}

////////////////////////////////////////////////////////////////////////////////

inline const crSkeletonData* crExpressionPlayer::GetSkeletonData() const
{
	return m_SkeletonData;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionPlayer::SetTime(float time)
{
	m_Time = time;
}

////////////////////////////////////////////////////////////////////////////////

inline float crExpressionPlayer::GetTime() const
{
	return m_Time;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionPlayer::SetDeltaTime(float deltaTime)
{
	m_DeltaTime = deltaTime;
}

////////////////////////////////////////////////////////////////////////////////

inline float crExpressionPlayer::GetDeltaTime() const
{
	return m_DeltaTime;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressionPlayer::SetEnabled(bool enabled)
{
	m_Enabled = enabled;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crExpressionPlayer::IsEnabled() const
{
	return m_Enabled;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crExpressionPlayer::SetVariable(u32 hash, Vec4V_In val)
{
	if(m_Expressions)
	{
		u32 idx;
		if(m_Expressions->ConvertVariableHashToIndex(hash, idx))
		{
			if(idx < u32(m_Variables.GetCount()))
			{
				m_Variables[idx] = val;
				return true;
			}
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crExpressionPlayer::GetVariable(u32 hash, Vec4V_InOut val) const
{
	if(m_Expressions)
	{
		u32 idx;
		if(m_Expressions->ConvertVariableHashToIndex(hash, idx))
		{
			if(idx < u32(m_Variables.GetCount()))
			{
				val = m_Variables[idx];
				return true;
			}
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline Vec4V_ConstPtr crExpressionPlayer::GetVariablesPtr() const
{
	return m_Variables.GetElements();
}

////////////////////////////////////////////////////////////////////////////////

inline Vec4V_Ptr crExpressionPlayer::GetVariablesPtr()
{
	return m_Variables.GetElements();
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crExpressionPlayer::GetNumVariables() const
{
	return u32(m_Variables.GetCount());
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CREXTRA_EXPRESSIONPLAYER_H
