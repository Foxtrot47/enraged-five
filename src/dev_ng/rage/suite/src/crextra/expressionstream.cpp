//
// crextra/expressionstream.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "expressionstream.h"

#include "system/cache.h"
#include "vectormath/classes.h"
#include "vectormath/layoutconvert.h"
#include "vectormath/scalarv_soa.h"
#include "vectormath/vec3v_soa.h"

#define USE_FIXED_STACK (1)

namespace rage
{

CompileTimeAssert16(sizeof(crExpressionStream));

////////////////////////////////////////////////////////////////////////////////

void crExpressionStream::Process(const ExprStreamHelper& ph) const
{
#if !__SPU // the stream is assumed to fit in data cache
	const u32 cacheLine = 128;
	u32 totalSize = GetSize();
	for(u32 i=0; i < totalSize; i+=cacheLine)
	{
		PrefetchDC2(this, i);
	}
#endif

#if USE_FIXED_STACK
	static const u32 stackDepth = 256;
	Vector_4V stackBuffer[stackDepth];
	Vector_4V* stack = stackBuffer;
#else
	u32 stackDepth = m_MaxStackDepth;
	Vector_4V* stack = Alloca(Vector_4V, stackDepth);
#endif
	Vector_4V* RESTRICT top = stack;
	const u8* RESTRICT alignParams = reinterpret_cast<const u8*>(this + 1);
	const u8* RESTRICT params = alignParams + m_AlignParamSize;
	const u8* RESTRICT ops = params + m_ParamSize;

	while(1)
	{
		FastAssert(top >= stack-1 && top < stack+stackDepth);

		ExprOp op = ExprOp(*ops++);
		switch(op)
		{
			// nullary
		case OpHalt:				return;
		case OpPop:					--top; break;
		case OpPush:				++top; top[0] = top[-1];  break;
		case OpZero:				++top; top[0] = V4VConstant(V_ZERO); break;
		case OpOne:					++top; top[0] = V4VConstant(V_ONE); break;
		case OpQuatIdentity:		++top; top[0] = V4VConstant(V_ZERO_WONE); break;
		case OpTime:				++top; top[0] = V4LoadScalar32IntoSplatted(ph.m_Time); break;
		case OpDeltaTime:			++top; top[0] = V4LoadScalar32IntoSplatted(ph.m_DeltaTime); break;
		case OpConstantFloat:		++top; top[0] = V4LoadScalar32IntoSplatted(*reinterpret_cast<const f32*>(params)); params += sizeof(f32); break;
		case OpGet:					++top; top[0] = reinterpret_cast<const crExprOpFrame*>(params)->Get(ph); params += sizeof(crExprOpFrame); break;
		case OpGetComp:				++top; top[0] = reinterpret_cast<const crExprOpFrame*>(params)->GetComp(ph); params += sizeof(crExprOpFrame); break;
		case OpGetRelative:			++top; top[0] = reinterpret_cast<const crExprOpFrame*>(params)->GetRelative(ph); params += sizeof(crExprOpFrame); break;
		case OpGetCompRelative:		++top; top[0] = reinterpret_cast<const crExprOpFrame*>(params)->GetCompRelative(ph); params += sizeof(crExprOpFrame); break;
		case OpValid:				++top; top[0] = reinterpret_cast<const crExprOpFrame*>(params)->IsValid(ph); params += sizeof(crExprOpFrame); break;
		case OpObjectGet:			++top; top[0] = reinterpret_cast<const crExprOpFrame*>(params)->GetObject(ph); params += sizeof(crExprOpFrame); break;
		case OpConstant:			++top; top[0] = *reinterpret_cast<const Vector_4V*>(alignParams); alignParams += sizeof(Vector_4V); break;
		case OpLinearVec:			++top; top[0] = reinterpret_cast<const crExprOpLinear*>(alignParams)->ProcessVectors(ph); alignParams += reinterpret_cast<const crExprOpLinear*>(alignParams)->m_Size; break;
		case OpLinearQuat:			++top; top[0] = reinterpret_cast<const crExprOpLinear*>(alignParams)->ProcessQuaternions(ph); alignParams += reinterpret_cast<const crExprOpLinear*>(alignParams)->m_Size; break;
		case OpGetVariable:			++top; top[0] = reinterpret_cast<const crExprOpVariable*>(params)->Get(ph); params += sizeof(crExprOpVariable); break;
		case OpMotion:				alignParams += sizeof(crExprOpMotion); break;

			// unary
		case OpAbs:					top[0] = V4Abs(top[0]); break;
		case OpNegate:				top[0] = V4Negate(top[0]); break;
		case OpInvert:				top[0] = V4InvertFast(top[0]); break;
		case OpSqrt:				top[0] = V4SqrtFast(top[0]); break;
		case OpLog:					top[0] = V4Log10(top[0]); break;
		case OpExp:					top[0] = V4Expt(top[0]); break;
		case OpCos:					top[0] = V4CosFast(top[0]); break;
		case OpSin:					top[0] = V4SinFast(top[0]); break;
		case OpTan:					top[0] = V4TanFast(top[0]); break;
		case OpArcCos:				top[0] = V4ArccosFast(top[0]); break;
		case OpArcSin:				top[0] = V4ArcsinFast(top[0]); break;
		case OpArcTan:				top[0] = V4ArctanFast(top[0]); break;
		case OpCosH:				top[0] = V4LoadScalar32IntoSplatted(cosh(GetX(top[0]))); break;
		case OpSinH:				top[0] = V4LoadScalar32IntoSplatted(sinh(GetX(top[0]))); break;
		case OpTanH:				top[0] = V4LoadScalar32IntoSplatted(tanh(GetX(top[0]))); break;
		case OpQuatInvert:			top[0] = V4QuatInvertNormInput(top[0]); break;
		case OpSquare:				top[0] = V4Scale(top[0], top[0]); break;
		case OpDegToRad:			top[0] = V4Scale(top[0], V4VConstant(V_TO_RADIANS)); break;
		case OpRadToDeg:			top[0] = V4Scale(top[0], V4VConstant(V_TO_DEGREES)); break;
		case OpClamp01:				top[0] = V4Clamp(top[0], V4VConstant(V_ZERO), V4VConstant(V_ONE)); break;
		case OpFromEuler:			top[0] = QuatVFromEulersXYZ(Vec3V(top[0])).GetIntrin128(); break;
		case OpToEuler:				top[0] = QuatVToEulersXYZ(QuatV(top[0])).GetIntrin128(); break;
		case OpObjectConvertFrom:	top[0] = reinterpret_cast<const crExprOpFrame*>(params)->ConvertFrom(ph, top[0]); params += sizeof(crExprOpFrame); break;
		case OpObjectConvertTo:		top[0] = reinterpret_cast<const crExprOpFrame*>(params)->ConvertTo(ph, top[0]); params += sizeof(crExprOpFrame); break;
		case OpCurve:				top[0] = reinterpret_cast<const crExprOpCurve*>(params)->Process(top[0]); params += reinterpret_cast<const crExprOpCurve*>(params)->m_Size; break;
		case OpSet:					--top; reinterpret_cast<const crExprOpFrame*>(params)->Set(ph, top[1]); params += sizeof(crExprOpFrame); break;
		case OpSetComp:				--top; reinterpret_cast<const crExprOpFrame*>(params)->SetComp(ph, top[1]); params += sizeof(crExprOpFrame); break;
		case OpSetRelative:			--top; reinterpret_cast<const crExprOpFrame*>(params)->SetRelative(ph, top[1]); params += sizeof(crExprOpFrame); break;
		case OpSetCompRelative:		--top; reinterpret_cast<const crExprOpFrame*>(params)->SetCompRelative(ph, top[1]); params += sizeof(crExprOpFrame); break;
		case OpObjectSet:			--top; reinterpret_cast<const crExprOpFrame*>(params)->SetObject(ph, top[1]); params += sizeof(crExprOpFrame); break;
		case OpSetVariable:			--top; reinterpret_cast<const crExprOpVariable*>(params)->Set(ph, top[1]); params += sizeof(crExprOpVariable); break;
		case OpBranch:				reinterpret_cast<const crExprOpBranch*>(params)->Branch(alignParams, params, ops); params += sizeof(crExprOpBranch); break;
		case OpBranchZero:			if(V4IsZeroAll(top[0])) reinterpret_cast<const crExprOpBranch*>(params)->Branch(alignParams, params, ops); params += sizeof(crExprOpBranch); break;
		case OpBranchNotZero:		if(!V4IsZeroAll(top[0])) reinterpret_cast<const crExprOpBranch*>(params)->Branch(alignParams, params, ops); params += sizeof(crExprOpBranch); break;

			// binary
		case OpAdd:					--top; top[0] = V4Add			(top[0], top[1]); break;
		case OpSubtract:			--top; top[0] = V4Subtract		(top[0], top[1]); break;
		case OpMultiply:			--top; top[0] = V4Scale			(top[0], top[1]); break;
		case OpExponent:			--top; top[0] = V4Pow			(top[0], top[1]); break;
		case OpMin:					--top; top[0] = V4Min			(top[0], top[1]); break;
		case OpMax:					--top; top[0] = V4Max			(top[0], top[1]); break;
		case OpXor:					--top; top[0] = V4Xor			(top[0], top[1]); break;
		case OpQuatMultiply:		--top; top[0] = V4QuatMultiply	(top[0], top[1]); break;
		case OpQuatScale:			--top; top[0] = V4QuatScaleAngle(top[0], top[1]); break;
		case OpTransform:			--top; top[0] = V3QuatRotate	(top[0], top[1]); break;
		case OpGreaterThan:			--top; top[0] = V4IsGreaterThanV(top[0], top[1]); break;
		case OpLessThan:			--top; top[0] = V4IsLessThanV	(top[0], top[1]); break;
		case OpGreaterThanEqual:	--top; top[0] = V4IsGreaterThanOrEqualV(top[0], top[1]); break;
		case OpLessThanEqual:		--top; top[0] = V4IsLessThanOrEqualV(top[0], top[1]); break;
		case OpEqual:				--top; top[0] = V4IsEqualV		(top[0], top[1]); break;
		case OpNotEqual:			--top; top[0] = V4InvertBits(V4IsEqualV(top[0], top[1])); break;

			// ternary
		case OpClamp:				----top; top[0] = V4Clamp		(top[0], top[1], top[2]); break;
		case OpLerp:				----top; top[0] = V4Lerp		(top[0], top[1], top[2]); break;
		case OpMultiplyAdd:			----top; top[0] = V4AddScaled	(top[2], top[0], top[1]); break;
		case OpQuatLerp:			----top; top[0] = V4QuatNlerp	(top[2], top[0], V4QuatPrepareSlerp(top[0], top[1])); break;
		case OpToVec:				----top; top[0] = V4MergeXY(V4MergeXY(top[0], top[2]), top[1]); break;
		case OpLookAt:				----top; top[0] = reinterpret_cast<const crExprOpLookAt*>(alignParams)->Process(top[0], top[1], top[2]); alignParams += sizeof(crExprOpLookAt); break;
		
			// never reach the default
		default:					NOT_REACHED; break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void crExprOpBranch::Branch(const u8* RESTRICT &alignParams, const u8* RESTRICT &params, const u8* RESTRICT &ops) const
{
	alignParams += m_AlignParamOffset;
	params += m_ParamOffset;
	ops += m_OperationOffset;
}

////////////////////////////////////////////////////////////////////////////////

__forceinline Vector_4V_Out crExprOpFrame::IsValid(const ExprStreamHelper& ph) const
{
	const u32* pSrc = reinterpret_cast<const u32*>(ph.m_Frame + ph.m_FrameIndices[m_AccIdx]);
	Vector_4V src = V4LoadScalar32IntoSplatted(pSrc[m_Component]);
	return V4InvertBits(V4IsEqualIntV(src, V4VConstant(V_MASKXYZW)));
}

////////////////////////////////////////////////////////////////////////////////

__forceinline Vector_4V_Out crExprOpFrame::Get(const ExprStreamHelper& ph) const
{
	const u32* pSrc = reinterpret_cast<const u32*>(ph.m_Frame + ph.m_FrameIndices[m_AccIdx]);
	if(!m_ForceDefault && *pSrc != UINT_MAX)
	{
		return *reinterpret_cast<const Vector_4V*>(pSrc);
	}
	else if(m_Track <= kTrackBoneScale)
	{
		u32 boneIdx = ph.m_SkelIndices[m_AccIdx];
		if(boneIdx != USHRT_MAX)
		{
			const crBoneData& bd = ph.m_Bones[boneIdx];
			switch(m_Track)
			{
			case kTrackBoneRotation:	return bd.GetDefaultRotation().GetIntrin128();
			case kTrackBoneTranslation: return bd.GetDefaultTranslation().GetIntrin128();
			case kTrackBoneScale:		return bd.GetDefaultScale().GetIntrin128();
			}
		}
	}
	else if(m_Track == kTrackFacialScale || m_Track == kTrackGenericScale)
	{
		return V4VConstant(V_ONE_WZERO);
	}

	return V4VConstant(V_ZERO_WONE);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline Vector_4V_Out crExprOpFrame::GetRelative(const ExprStreamHelper& ph) const
{
	const u32* pSrc = reinterpret_cast<const u32*>(ph.m_Frame + ph.m_FrameIndices[m_AccIdx]);
	if(!m_ForceDefault && *pSrc != UINT_MAX)
	{
		Vector_4V src = *reinterpret_cast<const Vector_4V*>(pSrc);

		u32 boneIdx = ph.m_SkelIndices[m_AccIdx];
		if(boneIdx != USHRT_MAX)
		{
			const crBoneData& bd = ph.m_Bones[boneIdx];
			switch(m_Track)
			{
			case kTrackBoneRotation:	
				return V4QuatMultiply(V4QuatInvertNormInput(bd.GetDefaultRotation().GetIntrin128()), src);

			case kTrackBoneTranslation:
				return V4Subtract(src, bd.GetDefaultTranslation().GetIntrin128());

			case kTrackBoneScale:
				return V4Subtract(src, bd.GetDefaultScale().GetIntrin128());
			}
		}
	}

	return V4VConstant(V_ZERO_WONE);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline Vector_4V_Out crExprOpFrame::GetComp(const ExprStreamHelper& ph) const
{
	const u32* pSrc = reinterpret_cast<const u32*>(ph.m_Frame + ph.m_FrameIndices[m_AccIdx]);
	const u32* pSrcComp = pSrc + m_Component;
	if(!m_ForceDefault && *pSrcComp != UINT_MAX)
	{
		if(m_Type == kFormatTypeQuaternion)
		{
			return V4LoadScalar32IntoSplatted(QuatVToEulersXYZ(*reinterpret_cast<const QuatV*>(pSrc))[m_Component]);
		}
		else
		{
			return V4LoadScalar32IntoSplatted(*reinterpret_cast<const f32*>(pSrcComp));
		}
	}
	else if(m_Track <= kTrackBoneScale)
	{
		u32 boneIdx = ph.m_SkelIndices[m_AccIdx];
		if(boneIdx != USHRT_MAX)
		{
			const crBoneData& bd = ph.m_Bones[boneIdx];
			switch(m_Track)
			{
			case kTrackBoneRotation:	return V4LoadScalar32IntoSplatted(QuatVToEulersXYZ(bd.GetDefaultRotation())[m_Component]);
			case kTrackBoneTranslation: return V4LoadScalar32IntoSplatted(bd.GetDefaultTranslation()[m_Component]);
			case kTrackBoneScale:		return V4LoadScalar32IntoSplatted(bd.GetDefaultScale()[m_Component]);
			}
		}
	}

	return V4VConstant(V_ZERO);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline Vector_4V_Out crExprOpFrame::GetCompRelative(const ExprStreamHelper& ph) const
{
	const u32* pSrc = reinterpret_cast<const u32*>(ph.m_Frame + ph.m_FrameIndices[m_AccIdx]);
	if(!m_ForceDefault && *pSrc != UINT_MAX)
	{
		Vector_4V src = *reinterpret_cast<const Vector_4V*>(pSrc);

		u32 boneIdx = ph.m_SkelIndices[m_AccIdx];
		if(boneIdx != USHRT_MAX)
		{
			const crBoneData& bd = ph.m_Bones[boneIdx];
			switch(m_Track)
			{
			case kTrackBoneRotation:
				return V4LoadScalar32IntoSplatted(QuatVToEulersXYZ(Multiply(InvertNormInput(bd.GetDefaultRotation()), QuatV(src)))[m_Component]);

			case kTrackBoneTranslation:
				return V4LoadScalar32IntoSplatted(Subtract(Vec3V(src), bd.GetDefaultTranslation())[m_Component]);

			case kTrackBoneScale:
				return V4LoadScalar32IntoSplatted(Subtract(Vec3V(src), bd.GetDefaultScale())[m_Component]);
			}
		}
	}

	return V4VConstant(V_ZERO);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void crExprOpFrame::Set(const ExprStreamHelper& ph, Vector_4V_In v) const
{
	FastAssert(m_AccIdx < ph.m_NumIndices && ph.m_FrameIndices[m_AccIdx] < ph.m_FrameSize);
	FastAssert(m_Type != kFormatTypeFloat);
	*reinterpret_cast<Vector_4V*>(ph.m_Frame + ph.m_FrameIndices[m_AccIdx]) = v;
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void crExprOpFrame::SetRelative(const ExprStreamHelper& ph, Vector_4V_In v) const
{
	FastAssert(m_AccIdx < ph.m_NumIndices && ph.m_FrameIndices[m_AccIdx] < ph.m_FrameSize);
	Vector_4V* pDest = reinterpret_cast<Vector_4V*>(ph.m_Frame + ph.m_FrameIndices[m_AccIdx]);

	u32 boneIdx = ph.m_SkelIndices[m_AccIdx];
	if(boneIdx != USHRT_MAX)
	{
		const crBoneData& bd = ph.m_Bones[boneIdx];
		switch(m_Track)
		{
		case kTrackBoneRotation:
			*pDest = V4QuatMultiply(bd.GetDefaultRotation().GetIntrin128(), v); break;

		case kTrackBoneTranslation:
			*pDest = V4Add(v, bd.GetDefaultTranslation().GetIntrin128()); break;

		case kTrackBoneScale:
			*pDest = V4Add(v, bd.GetDefaultScale().GetIntrin128()); break;
		}
	}
	else
	{
		*pDest = v;
	}
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void crExprOpFrame::SetComp(const ExprStreamHelper& ph, Vector_4V_In v) const
{
	FastAssert(m_AccIdx < ph.m_NumIndices && ph.m_FrameIndices[m_AccIdx] < ph.m_FrameSize);
	f32* pDest = reinterpret_cast<f32*>(ph.m_Frame + ph.m_FrameIndices[m_AccIdx]);
	switch(m_Type)
	{
	case kFormatTypeQuaternion:
		{
			QuatV dest = *reinterpret_cast<QuatV*>(pDest);
			Vec3V e = QuatVToEulersXYZ(dest);
			V4StoreScalar32FromSplatted(e[m_Component], v);
			dest = QuatVFromEulersXYZ(e);
			*reinterpret_cast<QuatV*>(pDest) = dest;
			break;
		}

	case kFormatTypeVector3:
			V4StoreScalar32FromSplatted(pDest[m_Component], v);
			break;

	default:
		V4StoreScalar32FromSplatted(pDest[m_Component], v);
	}
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void crExprOpFrame::SetCompRelative(const ExprStreamHelper& ph, Vector_4V_In v) const
{
	FastAssert(m_AccIdx < ph.m_NumIndices && ph.m_FrameIndices[m_AccIdx] < ph.m_FrameSize);
	f32* pDest = reinterpret_cast<f32*>(ph.m_Frame + ph.m_FrameIndices[m_AccIdx]);

	u32 boneIdx = ph.m_SkelIndices[m_AccIdx];
	if(boneIdx != USHRT_MAX)
	{
		const crBoneData& bd = ph.m_Bones[boneIdx];

		switch(m_Track)
		{
		case kTrackBoneRotation:
			{
				Vec3V ed = QuatVToEulersXYZ(bd.GetDefaultRotation());

				QuatV dest = *reinterpret_cast<QuatV*>(pDest);
				Vec3V e = QuatVToEulersXYZ(Multiply(InvertNormInput(bd.GetDefaultRotation()), dest));

				Vec3V rel = Add(Vec3V(v), ed);
				V4StoreScalar32FromSplatted(rel[m_Component], e.GetIntrin128());

				dest = Multiply(bd.GetDefaultRotation(), QuatVFromEulersXYZ(e));
				*reinterpret_cast<QuatV*>(pDest) = dest;
			}
			break;

		case kTrackBoneTranslation:
			{
				v = V4Add(v, bd.GetDefaultTranslation().GetIntrin128());
				pDest[m_Component] = GetElemRef( &v, m_Component );
			}
			break;

		case kTrackBoneScale:
			{
				v = V4Add(v, bd.GetDefaultScale().GetIntrin128());
				pDest[m_Component] = GetElemRef( &v, m_Component );
			}
			break;

		default:
			FastAssert(false);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

__forceinline Vector_4V_Out crExprOpFrame::GetObject(const ExprStreamHelper& ph) const
{
	TransformV trans;
	CalcObjectSpaceMatrix(ph, trans, true);

	switch(m_Track)
	{
	case kTrackBoneTranslation:
		return trans.GetPosition().GetIntrin128();

	case kTrackBoneRotation:
		return trans.GetRotation().GetIntrin128();
	}

	return V4VConstant(V_ZERO_WONE);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void crExprOpFrame::SetObject(const ExprStreamHelper& ph, Vector_4V_In v) const
{
	TransformV trans;
	CalcObjectSpaceMatrix(ph, trans, false);

	FastAssert(m_AccIdx < ph.m_NumIndices && ph.m_FrameIndices[m_AccIdx] < ph.m_FrameSize);
	u8* dest = ph.m_Frame + ph.m_FrameIndices[m_AccIdx];
	switch(m_Track)
	{
	case kTrackBoneTranslation:
		*reinterpret_cast<Vec3V*>(dest) = UnTransform(trans, Vec3V(v));
		break;

	case kTrackBoneRotation:
		*reinterpret_cast<QuatV*>(dest) = UnTransform(trans, QuatV(v));
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

__forceinline Vector_4V_Out crExprOpFrame::ConvertFrom(const ExprStreamHelper& ph, Vector_4V_In v) const
{
	TransformV trans;
	CalcObjectSpaceMatrix(ph, trans, false);

	switch(m_Track)
	{
	case kTrackBoneTranslation:
		return UnTransform(trans, Vec3V(v)).GetIntrin128();

	case kTrackBoneRotation:
		return UnTransform(trans, QuatV(v)).GetIntrin128();
	}

	return v;
}

////////////////////////////////////////////////////////////////////////////////

__forceinline Vector_4V_Out crExprOpFrame::ConvertTo(const ExprStreamHelper& ph, Vector_4V_In v) const
{
	TransformV trans;
	CalcObjectSpaceMatrix(ph, trans, false);

	switch(m_Track)
	{
	case kTrackBoneTranslation:
		return Transform(trans, Vec3V(v)).GetIntrin128();

	case kTrackBoneRotation:
		return Transform(trans, QuatV(v)).GetIntrin128();
	}

	return v;
}

////////////////////////////////////////////////////////////////////////////////

void crExprOpFrame::CalcObjectSpaceMatrix(const ExprStreamHelper& ph, TransformV_InOut outTrans, bool inclusive) const
{
	u32 boneIdx = ph.m_SkelIndices[m_AccIdx];
	const crBoneData* bd = boneIdx != USHRT_MAX ? ph.m_Bones + boneIdx : NULL;

	u16 boneId = USHRT_MAX;
	if(inclusive)
	{
		boneId = u16(m_Id);
	}
	else if(bd)
	{
		bd = bd->GetParent();
		boneId = bd->GetBoneId();
	}

	QuatV currQ(V_IDENTITY);
	Vec3V currT(V_ZERO);

	while(boneId != USHRT_MAX)
	{
		int dofRotIdx = FindDofIdx(ph.m_NumDofs, ph.m_Dofs, kTrackBoneRotation, boneId);
		int dofTransIdx = FindDofIdx(ph.m_NumDofs, ph.m_Dofs, kTrackBoneTranslation, boneId);

		QuatV q(V_IDENTITY);
		Vec3V t(V_ZERO);
		if(bd)
		{
			q = bd->GetDefaultRotation();
			t = bd->GetDefaultTranslation();
			bd = bd->GetParent();
		}

		boneId = bd ? bd->GetBoneId() : USHRT_MAX;

		if(dofRotIdx >= 0)
		{
			const u32* srcRot = reinterpret_cast<const u32*>(ph.m_Frame + ph.m_Dofs[dofRotIdx].m_Offset);
			if(*srcRot != UINT_MAX)
			{
				q = *reinterpret_cast<const QuatV*>(srcRot);
			}
		}

		if(dofTransIdx >= 0)
		{
			const u32* srcTrans = reinterpret_cast<const u32*>(ph.m_Frame + ph.m_Dofs[dofTransIdx].m_Offset);
			if(*srcTrans != UINT_MAX)
			{
				t = *reinterpret_cast<const Vec3V*>(srcTrans);
			}
		}

		currT = Transform(q, currT) + t;
		currQ = Multiply(q, currQ);
	}

	outTrans = TransformV(Normalize(currQ), currT);
}

////////////////////////////////////////////////////////////////////////////////

Vector_4V_Out crExprOpLookAt::Process(Vector_4V_In v0, Vector_4V_In v1, Vector_4V_In v2) const
{
	static const uaVector_4 _axes[] =
	{
		{U32_ONE,U32_ZERO,U32_ZERO,U32_ZERO},
		{U32_ZERO,U32_ONE,U32_ZERO,U32_ZERO},
		{U32_ZERO,U32_ZERO,U32_ONE,U32_ZERO},
		{U32_NEGONE,U32_ZERO,U32_ZERO,U32_ZERO},
		{U32_ZERO,U32_NEGONE,U32_ZERO,U32_ZERO},
		{U32_ZERO,U32_ZERO,U32_NEGONE,U32_ZERO}
	};

	const Vector_4V* axes = reinterpret_cast<const Vector_4V*>(_axes);
	Vec3V lookAtPos = Vec3V(v0);
	QuatV lookAtRot = QuatV(v1);
	Vec3V pos = Vec3V(v2);

	Vec3V dir = NormalizeSafe(lookAtPos - pos, Vec3V(V_Y_AXIS_WZERO));

	QuatV rot0 = QuatVFromVectors(Vec3V(axes[m_LookAtAxis]), dir);

	Vec3V originUpDir = Transform(rot0, Vec3V(axes[m_OriginUpAxis]));

	Vec3V lookAtUpDir = Transform(lookAtRot, Vec3V(axes[m_LookAtUpAxis]));

	QuatV rotTwist = QuatVFromVectors(originUpDir, lookAtUpDir, dir);

	rot0 = Multiply(rotTwist, rot0);
	rot0 = Multiply(rot0, m_Offset);

	return rot0.GetIntrin128();
}

////////////////////////////////////////////////////////////////////////////////

Vector_4V_Out crExprOpCurve::Process(Vector_4V_In v) const
{
	f32 in;
	V4StoreScalar32FromSplatted(in, v);

	f32 out = 0.f;
	if(in <= m_Keys[0].m_In)
	{
		out = m_Keys[0].m_Out;
	}
	else if(in >= m_Keys[m_NumKeys-1].m_In)
	{
		out = m_Keys[m_NumKeys-1].m_Out;
	}
	else
	{
		u32 i=1;
		for(; i<m_NumKeys; ++i)
		{
			if(in <= m_Keys[i].m_In)
			{
				out = Lerp((in-m_Keys[i-1].m_In) / (m_Keys[i].m_In-m_Keys[i-1].m_In), m_Keys[i-1].m_Out, m_Keys[i].m_Out);
				break;
			}
		}
		if(i==m_NumKeys)
		{
			out = m_Keys[m_NumKeys-1].m_Out;
		}
	}
	return V4LoadScalar32IntoSplatted(out);
}

////////////////////////////////////////////////////////////////////////////////

Vector_4V_Out crExprOpLinear::ProcessQuaternions(const ExprStreamHelper& ph) const
{
	const u8* frame = ph.m_Frame;
	const u16* indices = ph.m_FrameIndices;
	const ua16* src = m_Data;
	const SoA_Vec3V* param = reinterpret_cast<const SoA_Vec3V*>(src + m_NumSources*2);

	SoA_QuatV sum(SoA_QuatV::IDENTITY);
	SoA_ScalarV _invalid(V4VConstant(V_MASKXYZW));

	u32 extraCount = m_IntervalPerSource - 1;
	u32 count4 = m_NumSources >> 2;
	for(; count4; count4--)
	{
		// get inputs
		SoA_ScalarV raw;
		const f32* p0 = reinterpret_cast<const f32*>(frame + indices[src[0]] + src[1]);
		const f32* p1 = reinterpret_cast<const f32*>(frame + indices[src[2]] + src[3]);
		const f32* p2 = reinterpret_cast<const f32*>(frame + indices[src[4]] + src[5]);
		const f32* p3 = reinterpret_cast<const f32*>(frame + indices[src[6]] + src[7]);
		ToSoA(raw, ScalarVFromF32(*p0), ScalarVFromF32(*p1), ScalarVFromF32(*p2), ScalarVFromF32(*p3));
		src += 8;

		SoA_VecBool1V invalid = IsEqualInt(raw, _invalid);
		SoA_ScalarV input = Andc(raw, invalid);

		// compute linear function
		SoA_Vec3V partial;
		AddScaled(partial, param[1], param[0], input);
		param += 2;

		u32 count = extraCount;
		for(; count; count--)
		{
			SoA_VecBool3V greater;
			SoA_Vec3V scalar;
			IsGreaterThan(greater, input, param[0]);
			AddScaled(scalar, param[2], param[1], input);
			SelectFT(partial, greater, partial, scalar);
			param += 3;
		}

		SoA_QuatV q;
		QuatVFromEulersXYZ(q, partial);
		Multiply(sum, sum, q);
	}

	// convert back to AoS
	QuatV sum0, sum1, sum2, sum3;
	ToAoS(sum0, sum1, sum2, sum3, sum);
	return Multiply(Multiply(sum0, sum1), Multiply(sum2, sum3)).GetIntrin128();
}

////////////////////////////////////////////////////////////////////////////////

Vector_4V_Out crExprOpLinear::ProcessVectors(const ExprStreamHelper& ph) const
{
	const u8* frame = ph.m_Frame;
	const u16* indices = ph.m_FrameIndices;
	const ua16* src = m_Data;
	const SoA_Vec3V* param = reinterpret_cast<const SoA_Vec3V*>(src + m_NumSources*2);

	SoA_Vec3V sum(SoA_Vec3V::ZERO);
	SoA_ScalarV _invalid(V4VConstant(V_MASKXYZW));

	u32 extraCount = m_IntervalPerSource - 1;
	u32 count4 = m_NumSources >> 2;
	for(; count4; count4--)
	{
		// get inputs
		SoA_ScalarV raw;
		const f32* p0 = reinterpret_cast<const f32*>(frame + indices[src[0]] + src[1]);
		const f32* p1 = reinterpret_cast<const f32*>(frame + indices[src[2]] + src[3]);
		const f32* p2 = reinterpret_cast<const f32*>(frame + indices[src[4]] + src[5]);
		const f32* p3 = reinterpret_cast<const f32*>(frame + indices[src[6]] + src[7]);
		ToSoA(raw, ScalarVFromF32(*p0), ScalarVFromF32(*p1), ScalarVFromF32(*p2), ScalarVFromF32(*p3));
		src += 8;

		SoA_VecBool1V invalid = IsEqualInt(raw, _invalid);
		SoA_ScalarV input = Andc(raw, invalid);

		// compute linear function
		SoA_Vec3V partial;
		AddScaled(partial, param[1], param[0], input);
		param += 2;

		u32 count = extraCount;
		for(; count; count--)
		{
			SoA_VecBool3V greater;
			SoA_Vec3V scalar;
			IsGreaterThan(greater, input, param[0]);
			AddScaled(scalar, param[2], param[1], input);
			SelectFT(partial, greater, partial, scalar);
			param += 3;
		}

		Add(sum, sum, partial);
	}

	// convert back to AoS
	Vec3V sum0, sum1, sum2, sum3;
	ToAoS(sum0, sum1, sum2, sum3, sum);
	return Add(Add(sum0, sum1), Add(sum2, sum3)).GetIntrin128();
}

////////////////////////////////////////////////////////////////////////////////

Vector_4V_Out crExprOpVariable::Get(const ExprStreamHelper& ph) const
{
	return (m_Idx<ph.m_NumVariables)?ph.m_Variables[m_Idx]:V4VConstant(V_ZERO);
}

////////////////////////////////////////////////////////////////////////////////

void crExprOpVariable::Set(const ExprStreamHelper& ph, Vector_4V_In v) const
{
	if(m_Idx<ph.m_NumVariables)
	{
		ph.m_Variables[m_Idx] = v;
	}
}

////////////////////////////////////////////////////////////////////////////////

};  // namespace rage