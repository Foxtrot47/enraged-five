//
// crextra/expressions.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CREXTRA_EXPRESSIONS_H
#define CREXTRA_EXPRESSIONS_H

#include "atl/array.h"
#include "atl/string.h"
#include "cranimation/frameaccelerator.h"
#include "creature/componentphysical.h"
#include "paging/dictionary.h"

#include "expressionops.h"

#define CR_TEST_EXPRESSIONS (0)

namespace rage
{

class crCommonPool;
class crCreature;
class crExpression;
class crExpressions;
class crFrameDataFactory;
struct crExpressionStream;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Dictionary of expressions
typedef pgDictionary<crExpressions> crExpressionsDictionary;

////////////////////////////////////////////////////////////////////////////////


// PURPOSE: Expressions
// Contains a series of expressions
class crExpressions : public pgBase
{
public:

	// PURPOSE: Constructor
	crExpressions();

	// PURPOSE: Resource constructor
	crExpressions(datResource&);

	// PURPOSE: Destructor
	~crExpressions();

	// PURPOSE: Shutdown, free/release dynamic resources
	void Shutdown();

	// PURPOSE: Off line resourcing structure declaration
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Off line resourcing version
	static const int RORC_VERSION = 25;

	// PURPOSE: Placement
	DECLARE_PLACE(crExpressions);

	// PURPOSE: Are expressions packed
	// RETURNS: true - all expressions are packed
	bool IsPacked() const;
	
	// PURPOSE: Get the size of the largest packed expression
	// RETURNS: size of largest packed expression (in bytes)
	u32 GetMaxPackedSize() const;

	// PURPOSE: Get expression file name
	// RETURNS: Expression file name
	const char* GetName() const;


	// PURPOSE: Get number of expressions
	// RETURNS: number of expressions
	int GetNumExpressions() const;

	// PURPOSE: Get number of expression streams
	// RETURNS: number of expressions
	int GetNumExpressionStreams() const;

	// PURPOSE: Get expression by index (non-const)
	// PARAMS: idx - expression index [0..numExpressions-1]
	// RETURNS: pointer to expression
	// NOTES: Expressions are applied in sequence, starting with expression zero
	crExpression* GetExpression(int idx);

	// PURPOSE: Get expression by index  (const)
	// PARAMS: idx - expression index [0..numExpressions-1]
	// RETURNS: const pointer to expression
	// NOTES: Expressions are applied in sequence, starting with expression zero
	const crExpression* GetExpression(int idx) const;

	// PURPOSE: Get array of expressions
	crExpression*const* GetExpressions() const;

	// PURPOSE: Get array of expression streams
	crExpressionStream*const* GetExpressionStreams() const;

	// PURPOSE: Get number of variables in variable table
	u32 GetNumVariables() const;

	// PURPOSE: Get variable hash by index
	// PARAMS: idx - variable index
	// RETURN: variable hash id
	u32 GetVariableByIndex(u32 idx) const;
	
	// PURPOSE: Find variable index, given hash id
	// PARAMS: hash - variable hash id
	// idx - variable index, if found (undefined otherwise)
	// RETURNS: true - variable hash id found
	bool ConvertVariableHashToIndex(u32 hash, u32& idx) const;


	// PURPOSE: Initialize creature, add additional DOFs that are inputs to expressions
	// PARAMS: creature - creature to initialize, factory - optional factory to create the new frame data
	// NOTES: Traverses all expressions, looking at all expression operations
	// Adds any non-standard input DOFs found in expression operations, that aren't already
	// present in the creature.  Will create new creature components where required.
	void InitializeCreature(crCreature& creature, crFrameDataFactory* factory=NULL, crCommonPool* framePool=NULL) const;

	// PURPOSE: Initialize expression system
	static void InitClass();

	// PURPOSE: Shutdown the expression system
	static void ShutdownClass();


	// PURPOSE: Take out an additional reference
	void AddRef() const;

	// PURPOSE: Release a reference (will destroy object, if references now zero)
	// RETURNS: New reference count
	u32 Release() const;

	// PURPOSE: Get input DOF filter signature
	// RETURNS: Current filter signature, which is a hash uniquely identifying
	// what the expression does.  Zero implies no valid signature.
	u32 GetSignature() const;

	// PURPOSE: Find the expression frame indices table in the accelerator
	void FindExpressionFrameIndices(crLock& outLock, crFrameAccelerator& accelerator, const crFrameData& frameData) const;

	// PURPOSE: Find the expression skeleton indices table in the accelerator
	void FindExpressionSkelIndices(crLock& outLock, crFrameAccelerator& accelerator, const crSkeletonData& skelData) const;

#if CR_DEV
	// PURPOSE: .expr file versions.
	// All versions found within array are legal versions that will load correctly.
	// The value in element[0] is the current version that is used during any save operations.
	static const u8 sm_SerializationVersions[];

	// PURPOSE:
	// Initialize frame data, populate additional degrees of freedom based on inputs/outputs to expressions
	// PARAMS: frameData - frameData to populate
	// inputs/outputs - look for input and/or output DOFs (at least one of these must be true)
	// destroyExistingDofs - destroy any existing DOFs in frame
	// NOTES: Traverses all expressions, looking at all expression operations
	// Populates frame with any DOFs found
	void InitializeFrameData(crFrameData& frameData, bool inputs, bool outputs, bool destroyExistingDofs=true) const;

	// PURPOSE: Apply expressions to frame
	// PARAMS:
	// inoutFrame - frame to apply expressions to
	// time - optional total time elapsed (in seconds)
	// deltaTime - optional delta time elapsed (in seconds)
	// skelData - optional skeleton data pointer (can be NULL)
	// filter - optional filter for output DOFs (can be NULL)
	// variables - optional pointer to local variable stack (can be NULL)
	// numVariables - number of local variables in stack 
	void Process(crFrame& inoutFrame, float time=0.f, float deltaTime=0.f, crCreature* creature=NULL, const crSkeletonData* skelData=NULL, crFrameFilter* filter=NULL, crExpressionOp::Value* variables=NULL, u32 numVariables=0) const;

	// PURPOSE: Dump out the expressions (direct to output, or optionally to buffer).
	// PARAMS: verbosity - verbosity of output [0..3] (see notes)
	// buf - optional buffer (NULL buffer indicates direct output).
	// bufSize - optional buffer's size (must be specified if buffer is not NULL)
	// NOTES: verbosity levels; 0==minimal, 1==terse, 2==full, 3==debug
	void Dump(int verbosity=2, char* buf=NULL, u32 bufSize=0) const;

	// PURPOSE: Allocate and create an expressions dictionary
	// PARAMS: listFileName - filename containing list of .expr filenames
	// RETURNS: Newly allocated expressions dictionary, or NULL if operation fails
	static crExpressionsDictionary* AllocateAndLoadDictionary(const char* listFileName);

	// PURPOSE: Allocate and create an expressions dictionary
	// PARAMS: exprFilenames - list of .expr filenames
	// RETURNS: Newly allocated expressions dictionary, or NULL if operation fails
	static crExpressionsDictionary* AllocateAndLoadDictionary(const atArray<atString>& exprFilenames);


	// PURPOSE: Allocate a load an expression file
	// RETURNS: Newly allocated expressions, or NULL if load operation fails
	static crExpressions* AllocateAndLoad(const char* fileName, bool pack=false);

	// PURPOSE: Load an expression file
	// PARAMS:
	// filename - .expr file to load
	// pack - pack expressions into contiguous buffers (default false)
	// RETURNS: true - if load operation succeeds, false - if load operation fails
	bool Load(const char* fileName, bool pack=false);

	// PURPOSE: Save an expression file
	// PARAMS: filename - filename to use when saving
	// RETURNS: true - if save operation succeeds, false - if save operation fails
	// NOTES: If filename is NULL, will attempt to use same filename used when loading
	bool Save(const char* fileName) const;

	// PURPOSE: Serialize an expression file (for internal use only)
	void Serialize(datSerialize& s);

	// PURPOSE: Pack expressions
	// NOTES: Reorganizes the memory allocation of internal structures, so
	// expressions are contiguous in memory (important for SPU use).
	void Pack(u32* outSizes=NULL);

	// PURPOSE: Insert an expression into the sequence
	// PARAMS: expression - expression to insert
	// idx - index of where to insert into sequence [0..numExpressions-1]
	// NOTES: Expressions are applied in sequence, starting with expression zero
	// Will take copy of expression and assume ownership of the copy.
	// Caller remains responsible for destruction of copy provided as parameter.
	void InsertExpression(const crExpression& expression, int idx);

	// PURPOSE: Append an expression to the sequence
	// PARAMS: expression - expression to append
	// NOTES: Expressions are applied in sequence, starting with expression zero
	// Will take copy of expression and assume ownership of the copy.
	// Caller remains responsible for destruction of copy provided as parameter.
	void AppendExpression(const crExpression& expression);

	// PURPOSE: Remove an expression from the sequence
	// PARAMS: idx - index of expression to remove from the sequence [0..numExpressions-1]
	// NOTES: Expressions are applied in sequence, starting with expression zero
	void RemoveExpression(int idx);

	// PURPOSE: Calculate input outputs
	void CalcInputOutputDofs();

	// PURPOSE: Calculate variables
	void CalcVariables();

	// PURPOSE: Set expression has optimized
	void SetOptimized(bool optimized);
#endif // CR_DEV

	// PURPOSE: Internal function, calculate a new set of expression frame indices
	static u32 CalcExpressionFrameIndices(const crExpressions& expressions, const crFrameData& frameData, u16* indices);

	// PURPOSE: Internal function, calculate a new set of expression skeleton data indices
	static u32 CalcExpressionSkelIndices(const crExpressions& expressions, const crSkeletonData& skelData, u16* indices);

private:

	// PURPOSE: Dof used by expression (use for accelerator indices)
	struct Track
	{
		// PURPOSE: Constructor
		Track() {}

		// PURPOSE: Resource constructor
		Track(datResource&) {}

		// PURPOSE: Placement
		IMPLEMENT_PLACE_INLINE(Track);

#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

		u16 m_Id;
		u8 m_Track;
		u8 m_Flags;

		// PURPOSE: Track flags
		enum { kIsInput = BIT7 };
	};

	// PURPOSE: Local variable used by the expression
	struct Variable
	{
		// PURPOSE: Constructor
		Variable() {}
		Variable(u32 hash) : m_Hash(hash) {}

		// PURPOSE: Resource constructor
		Variable(datResource&) {}

		// PURPOSE: Placement
		IMPLEMENT_PLACE_INLINE(Variable);

#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

		bool operator==(const Variable& var) const;
		bool operator<(const Variable& var) const;

		u32 m_Hash;
	};

	// PURPOSE: Expressions flags
	enum 
	{ 
		kOptimized = BIT0,
		kPacked = BIT1,
		kNonSerializableMask = kPacked,
	};

	atArray<datOwner<crExpression> > m_Expressions;
	atArray<datRef<crExpressionStream> > m_ExpressionStreams;
	atArray<Track> m_Tracks;
	atArray<crMotionDescription> m_Motions;
	atArray<Variable> m_Variables;
	atString m_Name;
	mutable u32 m_RefCount;
	u32 m_Signature;
	u32 m_MaxPackedSize;
	u16 m_Flags;
	datPadding<10> m_Padding;

	static bool sm_InitClassCalled;
	friend struct CalcInputOutputDofsIterator;
	friend struct CalcVariablesIterator;
	friend struct ExpressionCreatureInitializer;
};

////////////////////////////////////////////////////////////////////////////////

inline bool crExpressions::IsPacked() const
{
	return (m_Flags & kPacked) != 0;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crExpressions::GetMaxPackedSize() const
{
	return m_MaxPackedSize;
}

////////////////////////////////////////////////////////////////////////////////

inline const char* crExpressions::GetName() const
{
	return m_Name.c_str();
}

////////////////////////////////////////////////////////////////////////////////

inline int crExpressions::GetNumExpressions() const
{
	return m_Expressions.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline int crExpressions::GetNumExpressionStreams() const
{
	return m_ExpressionStreams.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline crExpression* crExpressions::GetExpression(int idx)
{
	return m_Expressions[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline const crExpression* crExpressions::GetExpression(int idx) const
{
	return m_Expressions[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline crExpression*const* crExpressions::GetExpressions() const
{
	return (crExpression*const*)m_Expressions.GetElements();
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionStream*const* crExpressions::GetExpressionStreams() const
{
	return (crExpressionStream*const*)m_ExpressionStreams.GetElements();
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crExpressions::GetNumVariables() const
{
	return u32(m_Variables.GetCount());
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crExpressions::GetVariableByIndex(u32 idx) const
{
	return m_Variables[idx].m_Hash;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crExpressions::ConvertVariableHashToIndex(u32 hash, u32& idx) const
{
	Variable var(hash);
	int index = m_Variables.BinarySearch(var);
	idx = u32(index);
	return index >= 0;
}

////////////////////////////////////////////////////////////////////////////////

inline void crExpressions::AddRef() const
{
	sysInterlockedIncrement(&m_RefCount);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crExpressions::Release() const
{
	u32 refCount = sysInterlockedDecrement(&m_RefCount);
	if(!refCount)
	{
		delete this;
		return 0;
	}
	else
	{
		return refCount;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crExpressions::GetSignature() const
{
	return m_Signature;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crExpressions::Variable::operator==(const crExpressions::Variable& var) const
{
	return m_Hash == var.m_Hash;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crExpressions::Variable::operator<(const crExpressions::Variable& var) const
{
	return m_Hash < var.m_Hash;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CREXTRA_EXPRESSIONS_H
