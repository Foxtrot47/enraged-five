//
// crextra/expressionplayer.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "expressionplayer.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crExpressionPlayer::crExpressionPlayer()
: m_Expressions(NULL)
, m_SkeletonData(NULL)
, m_Time(0.f)
, m_DeltaTime(0.f)
, m_Enabled(true)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionPlayer::crExpressionPlayer(crExpressions& expressions, const crSkeletonData* skelData)
: m_Expressions(NULL)
, m_SkeletonData(NULL)
, m_Time(0.f)
, m_DeltaTime(0.f)
, m_Enabled(true)
{
	Init(expressions, skelData);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionPlayer::~crExpressionPlayer()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPlayer::Shutdown()
{
	SetExpressions(NULL);
	SetSkeletonData(NULL);
	SetTime(0.f);
	SetDeltaTime(0.f);

	m_Variables.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPlayer::Init(const crExpressions& expressions, const crSkeletonData* skelData)
{
	SetExpressions(&expressions);
	SetSkeletonData(skelData);
	SetTime(0.f);
	SetDeltaTime(0.f);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPlayer::SetExpressions(const crExpressions* expressions)
{
	if(expressions != m_Expressions)
	{
		if(expressions)
		{
			expressions->AddRef();
		}
		if(m_Expressions)
		{
			m_Expressions->Release();
		}

		m_Expressions = expressions;
		m_Variables.ResetCount();
		if(m_Expressions)
		{
			m_Variables.ResizeGrow(m_Expressions->GetNumVariables());
			memset(m_Variables.GetElements(), 0, m_Expressions->GetNumVariables()*sizeof(Vec4V));
		}

		if(m_ExpressionsCallback)
		{
			m_ExpressionsCallback();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPlayer::SetFunctor(const Functor0& functor)
{
	m_ExpressionsCallback = functor;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
