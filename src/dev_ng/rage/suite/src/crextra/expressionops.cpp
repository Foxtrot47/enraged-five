//
// crextra/expressionops.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "expressionops.h"

#if CR_DEV
#include "atl/array_struct.h"
#include "cranimation/frame.h"
#include "cranimation/framedof.h"
#include "cranimation/framefilters.h"
#include "crskeleton/skeletondata.h"
#include "creature/creature.h"
#include "data/safestruct.h"
#include "math/random.h"
#include "system/cache.h"
#include "system/nelem.h"
#include "vectormath/classes.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crExpressionOp::crExpressionOp(eOpTypes opType)
: m_OpType(kOpTypeNone)
, m_OpSubType(0)
{
	Assert(opType != kOpTypeNone);

	m_OpType = u16(opType);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp::crExpressionOp(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpressionOp::CreateExpressionOp(eOpTypes opType)
{
	const ExpressionOpTypeInfo* info = FindExpressionOpTypeInfo(opType);
	Assert(info);

	ExpressionOpCreateFn* createFn = info->GetCreateFn();
	return createFn();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::Place(crExpressionOp *that, datResource& rsc)
{
	const ExpressionOpTypeInfo* info = FindExpressionOpTypeInfo(((crExpressionOp*)that)->GetOpType());
	Assert(info);

	ExpressionOpPlaceRscFn* placeFn = info->GetPlaceRscFn();
	placeFn((crExpressionOp*)that, rsc);
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
datSwapper_ENUM(crExpressionOp::eOpTypes);
void crExpressionOp::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crExpressionOp)
	SSTRUCT_FIELD(crExpressionOp, m_OpType)
	SSTRUCT_FIELD(crExpressionOp, m_OpSubType)
	SSTRUCT_END(crExpressionOp)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::VirtualConstructFromPtr(datResource& rsc, crExpressionOp* base)
{
	Assert(base);
	base->Place(base, rsc);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp::~crExpressionOp()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::Shutdown()
{
}

////////////////////////////////////////////////////////////////////////////////

u32 crExpressionOp::GetNumChildren() const
{
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

const crExpressionOp* crExpressionOp::GetChild(u32) const
{
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpressionOp::GetChild(u32)
{
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::SetChild(u32, crExpressionOp&)
{
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::Serialize(SerializeHelper& sh)
{
	int opType = int(m_OpType);
	sh << datLabel("OpType:") << opType << datNewLine;
	Assert(opType == int(m_OpType));
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::Dump(DumpHelper& dh) const
{
	dh.Outputf(1, "op type %d '%s'", int(GetOpType()), GetExpressionOpTypeInfo().GetOpName());
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

		sm_ExpressionOpTypeInfos.Resize(kOpTypeNum);
		for(int i=0; i<sm_ExpressionOpTypeInfos.GetCount(); ++i)
		{
			sm_ExpressionOpTypeInfos[i] = NULL;
		}

		// automatically register expression ops
		crExpressionOpConstant::InitClass();
		crExpressionOpConstantFloat::InitClass();

		crExpressionOpGet::InitClass();
		crExpressionOpSet::InitClass();
		crExpressionOpValid::InitClass();
		crExpressionOpComponentGet::InitClass();
		crExpressionOpComponentSet::InitClass();
		crExpressionOpObjectSpaceGet::InitClass();
		crExpressionOpObjectSpaceSet::InitClass();
		crExpressionOpObjectSpaceConvertTo::InitClass();
		crExpressionOpObjectSpaceConvertFrom::InitClass();

		crExpressionOpNullary::InitClass();
		crExpressionOpUnary::InitClass();
		crExpressionOpBinary::InitClass();
		crExpressionOpTernary::InitClass();
		crExpressionOpNary::InitClass();

		crExpressionOpSpecialBlend::InitClass();
		crExpressionOpSpecialLinear::InitClass();
		crExpressionOpSpecialCurve::InitClass();
		crExpressionOpSpecialLookAt::InitClass();

		crExpressionOpMotion::InitClass();

		crExpressionOpVariableGet::InitClass();
		crExpressionOpVariableSet::InitClass();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::ShutdownClass()
{
	sm_ExpressionOpTypeInfos.Reset();

	sm_InitClassCalled = false;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp::ExpressionOpTypeInfo::ExpressionOpTypeInfo(crExpressionOp::eOpTypes opType, const char* name, ExpressionOpCreateFn* createFn, ExpressionOpPlaceFn* placeFn, ExpressionOpPlaceRscFn placeRscFn, size_t size)
: m_OpType(opType)
, m_Name(name)
, m_CreateFn(createFn)
, m_PlaceFn(placeFn)
, m_PlaceRscFn(placeRscFn)
, m_Size(size)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp::ExpressionOpTypeInfo::~ExpressionOpTypeInfo()
{
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::ExpressionOpTypeInfo::Register() const
{
	crExpressionOp::RegisterExpressionOpTypeInfo(*this);
}

////////////////////////////////////////////////////////////////////////////////

const crExpressionOp::ExpressionOpTypeInfo* crExpressionOp::FindExpressionOpTypeInfo(crExpressionOp::eOpTypes opType)
{
	Assert(opType < kOpTypeNum);
	AssertMsg(opType < sm_ExpressionOpTypeInfos.GetCount() , "crExpressionOp::FindExpressionOpTypeInfo - expression op types not registered, missing crExpressionOp::InitClass call?");

	return sm_ExpressionOpTypeInfos[opType];
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOp::Value::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crExpressionOp::Value)
	SSTRUCT_CONTAINED_ARRAY(crExpressionOp::Value, m_Values)
	SSTRUCT_END(crExpressionOp::Value)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::Value::Dump(DumpHelper& dh) const
{
	dh.Outputf(2, "value %f %f %f %f", m_Values[0], m_Values[1], m_Values[2], m_Values[3]);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::Value::Serialize(SerializeHelper& sh)
{
	sh << datLabel("Values:") << m_Values[0] << m_Values[1] << m_Values[2] << m_Values[3] << datNewLine;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp::ConstIterator::ConstIterator()
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp::ConstIterator::~ConstIterator()
{
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::ConstIterator::Visit(const crExpressionOp& op)
{
	const u32 numChildren = op.GetNumChildren();
	for(u32 i=0; i<numChildren; ++i)
	{
		Visit(*op.GetChild(i));
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp::ModifyingIterator::ModifyingIterator()
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp::ModifyingIterator::~ModifyingIterator()
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpressionOp::ModifyingIterator::Visit(crExpressionOp& op)
{
	const u32 numChildren = op.GetNumChildren();
	for(u32 i=0; i<numChildren; ++i)
	{
		op.SetChild(i, *Visit(*op.GetChild(i)));
	}

	return &op;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp::DumpHelper::DumpHelper(int verbosity, char* outputBuffer, u32 outputBufferSize)
: m_Verbosity(verbosity)
, m_OutputBuffer(outputBuffer)
, m_OutputBufferSize(outputBufferSize)
, m_Indent(0)
, m_FirstLine(true)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp::DumpHelper::~DumpHelper()
{
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::DumpHelper::Outputf(int verbosity, const char* fmt, ...)
{
	if(m_Verbosity >= verbosity)
	{
		va_list args;
		va_start(args, fmt);

		InternalOutputf(fmt, args);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::DumpHelper::PushIndent()
{
	++m_Indent;
	m_FirstLine = true;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::DumpHelper::PopIndent()
{
	--m_Indent;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::DumpHelper::Visit(const crExpressionOp& op)
{
	PushIndent();
	op.Dump(*this);

	const u32 numChildren = op.GetNumChildren();
	if(numChildren > 0)
	{
		Outputf(2, "num sources %d", numChildren);

		ConstIterator::Visit(op);
	}
	PopIndent();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::DumpHelper::InternalOutputf(const char* fmt, va_list& args)
{
	int indent = m_Indent;
	if(m_FirstLine)
	{
		indent--;
	}
	for(int i=0; i<indent; ++i)
	{
		InternalPrintf("| ");
	}
	if(m_FirstLine)
	{
		InternalPrintf("+ ");
		m_FirstLine = false;
	}

	// convert variable arguments into buffer and output
	const int bufSize = 256;
	char buf[bufSize];
	if(fmt)
	{
		vformatf(buf, bufSize, fmt, args);
	}
	else
	{
		buf[0] = '\0';
	}
	InternalPrintf("%s\n", buf);

	va_end(args);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::DumpHelper::InternalPrintf(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);

	if(m_OutputBuffer)
	{
		if(m_OutputBufferSize > 0)
		{
			vformatf(m_OutputBuffer, m_OutputBufferSize, fmt, args);
			while(*m_OutputBuffer != '\0' && m_OutputBufferSize > 0)
			{
				m_OutputBuffer++;
				m_OutputBufferSize--;
			}
		}
	}
	else
	{
		const int bufSize = 256;
		char buf[bufSize];
		if(fmt)
		{
			vformatf(buf, bufSize, fmt, args);
		}
		else
		{
			buf[0] = '\0';
		}
		Printf("%s", buf);
	}

	va_end(args);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::SerializeHelper::operator<<(datOwner<crExpressionOp>& op)
{
	int opType = int(op ? op->GetOpType() : crExpressionOp::kOpTypeNone);
	(*m_Serialize) << datLabel("OpType:") << opType << datNewLine;

	if(m_Serialize->IsRead())
	{
		if(opType == crExpressionOp::kOpTypeNone)
		{
			op = NULL;
		}
		else
		{
			op = crExpressionOp::CreateExpressionOp(eOpTypes(opType));
		}
	}

	if(op != NULL)
	{
		op->Serialize(*this);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOp::RegisterExpressionOpTypeInfo(const crExpressionOp::ExpressionOpTypeInfo& info)
{
	eOpTypes opType = info.GetOpType();

	Assert(opType < kOpTypeNum);
	AssertMsg(sm_ExpressionOpTypeInfos.GetCount() == kOpTypeNum , "crExpressionOp::RegisterExpressionOpTypeInfo - expression op type attempting to register before call to crExpressionOp::InitClass");
	AssertMsg(!sm_ExpressionOpTypeInfos[opType] , "crExpressionOp::RegisterExpressionOpTypeInfo - expression op type registered twice");

	sm_ExpressionOpTypeInfos[opType] = &info;
}

////////////////////////////////////////////////////////////////////////////////

atArray<const crExpressionOp::ExpressionOpTypeInfo*> crExpressionOp::sm_ExpressionOpTypeInfos;

////////////////////////////////////////////////////////////////////////////////

bool crExpressionOp::sm_InitClassCalled = false;

////////////////////////////////////////////////////////////////////////////////

crExpressionOpConstant::crExpressionOpConstant()
: crExpressionOp(kOpTypeConstant)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpConstant::crExpressionOpConstant(const Value& val)
: crExpressionOp(kOpTypeConstant)
{
	Init(val);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpConstant::crExpressionOpConstant(datResource& rsc)
: crExpressionOp(rsc)
, m_Value(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpConstant::DeclareStruct(datTypeStruct& s)
{
	crExpressionOp::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpConstant, crExpressionOp)
	SSTRUCT_IGNORE(crExpressionOpConstant, m_Padding)
	SSTRUCT_FIELD(crExpressionOpConstant, m_Value)
	SSTRUCT_END(crExpressionOpConstant)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpConstant::~crExpressionOpConstant()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpConstant::Init(const Value& val)
{
	SetValue(val);
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpConstant, crExpressionOp::kOpTypeConstant);

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpConstant::Process(ProcessHelper& ph) const
{
	ph.PushValue() = m_Value;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpConstant::Serialize(SerializeHelper& sh)
{
	crExpressionOp::Serialize(sh);

	m_Value.Serialize(sh);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpConstant::Dump(DumpHelper& dh) const
{
	crExpressionOp::Dump(dh);

	m_Value.Dump(dh);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpConstantFloat::crExpressionOpConstantFloat()
: crExpressionOp(kOpTypeConstantFloat)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpConstantFloat::crExpressionOpConstantFloat(float f)
: crExpressionOp(kOpTypeConstantFloat)
{
	Init(f);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpConstantFloat::crExpressionOpConstantFloat(datResource& rsc)
: crExpressionOp(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpConstantFloat::DeclareStruct(datTypeStruct& s)
{
	crExpressionOp::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpConstantFloat, crExpressionOp)
	SSTRUCT_FIELD(crExpressionOpConstantFloat, m_Float)
	SSTRUCT_END(crExpressionOpConstantFloat)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpConstantFloat::~crExpressionOpConstantFloat()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpConstantFloat::Init(float f)
{
	SetFloat(f);	
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpConstantFloat, crExpressionOp::kOpTypeConstantFloat);

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpConstantFloat::Process(ProcessHelper& ph) const
{
	ph.PushValue().Set(m_Float);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpConstantFloat::Serialize(SerializeHelper& sh)
{
	crExpressionOp::Serialize(sh);

	sh << datLabel("Float:") << m_Float << datNewLine;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpConstantFloat::Dump(DumpHelper& dh) const
{
	crExpressionOp::Dump(dh);

	dh.Outputf(2, "float %f", m_Float);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpGetSet::crExpressionOpGetSet(crExpressionOp::eOpTypes opType)
: crExpressionOp(opType)
, m_Track(0)
, m_Type(0xff)
, m_Id(0)
, m_AccIdx(-1)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpGetSet::crExpressionOpGetSet(const crExpressionOpGetSet& other)
: crExpressionOp(other)
, m_Track(other.m_Track)
, m_Type(other.m_Type)
, m_Id(other.m_Id)
, m_AccIdx(other.m_AccIdx)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpGetSet::crExpressionOpGetSet(datResource& rsc)
: crExpressionOp(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpGetSet::DeclareStruct(datTypeStruct& s)
{
	crExpressionOp::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpGetSet, crExpressionOp)
	SSTRUCT_FIELD(crExpressionOpGetSet, m_Track)
	SSTRUCT_FIELD(crExpressionOpGetSet, m_Type)
	SSTRUCT_FIELD(crExpressionOpGetSet, m_Id)
	SSTRUCT_FIELD(crExpressionOpGetSet, m_AccIdx)
	SSTRUCT_END(crExpressionOpGetSet)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpGetSet::~crExpressionOpGetSet()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpGetSet::Serialize(SerializeHelper& sh)
{
	crExpressionOp::Serialize(sh);

	sh << datLabel("Track:") << m_Track << datNewLine;
	sh << datLabel("Id:") << m_Id << datNewLine;
	sh << datLabel("Type:") << m_Type << datNewLine;
	if(sh.m_Version >= 8)
	{
		bool isRelative = IsRelative();
		sh << datLabel("Relative:") << isRelative << datNewLine;
		if(sh.IsRead())
		{
			SetRelative(isRelative);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpGetSet::Dump(DumpHelper& dh) const
{
	crExpressionOp::Dump(dh);

	dh.Outputf(2, "track %d id %d type %d", int(m_Track), int(m_Id), int(m_Type));
	dh.Outputf(2, "relative %d", IsRelative());
	dh.Outputf(3, "accelerator idx %d", GetAcceleratedIndex());
}

////////////////////////////////////////////////////////////////////////////////

bool crExpressionOpGetSet::GetDefaultValue(ProcessHelper& ph, Value& outValue, bool isRelative) const
{
	switch(m_Track)
	{
	case kTrackBoneTranslation:
	case kTrackBoneRotation:
	case kTrackBoneScale:
		if(isRelative)
		{
			break;
		}

		if(ph.m_SkeletonData)
		{
			int idx;
			if(ph.m_SkeletonData->ConvertBoneIdToIndex(m_Id, idx))
			{
				const crBoneData* bd = ph.m_SkeletonData->GetBoneData(idx);
				if(bd)
				{
					switch(m_Track)
					{
					case kTrackBoneTranslation:
						Assert(m_Type == kFormatTypeVector3);	
						outValue.Set(bd->GetDefaultTranslation());
						return true;
						// break;

					case kTrackBoneRotation:
						Assert(m_Type == kFormatTypeQuaternion);
						outValue.Set(bd->GetDefaultRotation());
						return true;
						// break;

					case kTrackBoneScale:
						Assert(m_Type == kFormatTypeVector3);	
						outValue.Set(bd->GetDefaultScale());
						return true;
						// break;

					default:
						Assert(0);
						break;
					}
				}
			}
		}
		break;

	default:
		break;
	}

	// not a bone based dof, or failed to find default - fill with a suitable zero value
	switch(m_Type)
	{
	case kFormatTypeQuaternion:
		outValue.Set(QuatV(V_IDENTITY));
		break;

	case kFormatTypeVector3:
		{
			switch(m_Track)
			{
			case kTrackBoneScale:
			case kTrackFacialScale:
			case kTrackGenericScale:
				if(!isRelative)
				{
					outValue.Set(Vec3V(V_ONE_WZERO));
					break;
				}
				// break intentionally omitted

			default:
				outValue.Zero();
				break;
			}
		}
		break;

	default:
		outValue.Zero();
		break;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpGetSet::ApplyDefaultQuaternion(QuatV_InOut q, ProcessHelper& ph, bool stripNotApply) const
{
	switch(m_Track)
	{
	case kTrackBoneRotation:
		if(ph.m_SkeletonData)
		{
			int idx;
			if(ph.m_SkeletonData->ConvertBoneIdToIndex(m_Id, idx))
			{
				const crBoneData* bd = ph.m_SkeletonData->GetBoneData(idx);
				if(bd)
				{
					QuatV defaultRotation = bd->GetDefaultRotation();

					if(stripNotApply)
					{
						q = Multiply(InvertNormInput(defaultRotation), q);
					}
					else
					{
						q = Multiply(defaultRotation, q);
					}
				}
			}
		}
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpGetSet::ApplyDefaultVector3(Vec3V_InOut inoutVector3, ProcessHelper& ph, bool subtractNotAdd) const
{
	switch(m_Track)
	{
	case kTrackBoneTranslation:
	case kTrackBoneScale:
		if(ph.m_SkeletonData)
		{
			int idx;
			if(ph.m_SkeletonData->ConvertBoneIdToIndex(m_Id, idx))
			{
				const crBoneData* bd = ph.m_SkeletonData->GetBoneData(idx);
				if(bd)
				{
					const Vec3V& defaultVector3 = (m_Track==kTrackBoneTranslation)?bd->GetDefaultTranslation():(bd->GetDefaultScale());

					if(subtractNotAdd)
					{
						inoutVector3 -= defaultVector3;
					}
					else
					{
						inoutVector3 += defaultVector3;
					}
				}
			}
		}
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpGet::crExpressionOpGet()
: crExpressionOpGetSet(kOpTypeGet)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpGet::crExpressionOpGet(eOpTypes opType)
: crExpressionOpGetSet(opType)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpGet::crExpressionOpGet(const crExpressionOpGet& other)
: crExpressionOpGetSet(other)
{
	SetForceDefault(other.IsForceDefault());
	SetRelative(other.IsRelative());
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpGet::crExpressionOpGet(datResource& rsc)
: crExpressionOpGetSet(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpGet::DeclareStruct(datTypeStruct& s)
{
	crExpressionOpGetSet::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpGet, crExpressionOpGetSet)
	SSTRUCT_END(crExpressionOpGet)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpGet::~crExpressionOpGet()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpGet, crExpressionOp::kOpTypeGet);

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpGet::Process(ProcessHelper& ph) const
{
	const crFrame& frame = *ph.m_Frame;
	Value& val = ph.PushValue();
	val.Zero();

	if(!IsForceDefault())
	{
		switch(m_Type)
		{
		case kFormatTypeVector3:
			{
				Vec3V v;
				if(frame.GetVector3(m_Track, m_Id, v))
				{
					val.Set(v);
					if(IsRelative())
					{
						ApplyDefaultVector3(val.GetVector(), ph, true);
					}
					return;
				}
			}
			break;

		case kFormatTypeQuaternion:
			{
				QuatV q;
				if(frame.GetQuaternion(m_Track, m_Id, q))
				{
					val.Set(q);
					if(IsRelative())
					{
						ApplyDefaultQuaternion(val.GetQuaternion(), ph, true);
					}
					return;
				}
			}
			break;

		case kFormatTypeFloat:
			{
				float f;
				if(frame.GetFloat(m_Track, m_Id, f))
				{
					val.Set(f);
					return;
				}
			}
			break;
		}
	}

	// failed to get value, fall back to default
	GetDefaultValue(ph, val, IsRelative());
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpGet::Serialize(SerializeHelper& sh)
{
	crExpressionOpGetSet::Serialize(sh);

	if(sh.m_Version >= 3)
	{
		bool isForceDefault = IsForceDefault();
		sh << datLabel("ForceDefault:") << isForceDefault << datNewLine;
		if(sh.IsRead())
		{
			SetForceDefault(isForceDefault);
		}
	}

	if(sh.m_Version >= 4)
	{
		bool isRelative = IsRelative();
		sh << datLabel("OrientSpace:") << isRelative << datNewLine;  // NOTE - relative uses old orient space label for backward compatibility
		if(sh.IsRead())
		{
			SetRelative(isRelative);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpGet::Dump(DumpHelper& dh) const
{
	crExpressionOpGetSet::Dump(dh);

	dh.Outputf(2, "force default %d relative %d", int(IsForceDefault()), int(IsRelative()));
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSet::crExpressionOpSet()
: crExpressionOpGetSet(crExpressionOp::kOpTypeSet)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSet::crExpressionOpSet(const crExpressionOpSet& other)
: crExpressionOpGetSet(other)
{
	m_Source = other.m_Source ? other.m_Source->Clone() : NULL;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSet::crExpressionOpSet(datResource& rsc)
: crExpressionOpGetSet(rsc)
, m_Source(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpSet::DeclareStruct(datTypeStruct& s)
{
	crExpressionOpGetSet::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpSet, crExpressionOpGetSet)
	SSTRUCT_FIELD(crExpressionOpSet, m_Source)
	SSTRUCT_END(crExpressionOpSet)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSet::~crExpressionOpSet()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSet::Shutdown()
{
	if(m_Source)
	{
		delete m_Source;
		m_Source = NULL;
	}

	crExpressionOp::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpSet, crExpressionOp::kOpTypeSet);

////////////////////////////////////////////////////////////////////////////////

u32 crExpressionOpSet::GetNumChildren() const
{
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

const crExpressionOp* crExpressionOpSet::GetChild(u32 ASSERT_ONLY(idx)) const
{
	Assert(idx == 0);
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpressionOpSet::GetChild(u32 ASSERT_ONLY(idx))
{
	Assert(idx == 0);
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSet::SetChild(u32 ASSERT_ONLY(idx), crExpressionOp& op)
{
	Assert(idx == 0);
	m_Source = &op;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSet::Process(ProcessHelper& ph) const
{
	crFrame& frame = *ph.m_Frame;

#if !OPTIMIZE_CULL_SET_OPERATIONS
	m_Source->Process(ph);
	Value& val = ph.PopValue();
#endif // !OPTIMIZE_CULL_SET_OPERATIONS

	crFrameFilter* filter = ph.m_Filter;
	float weight = 1.f;
	if(filter && !filter->FilterDof(GetTrack(), GetId(), weight))
	{
		return;
	}

#if OPTIMIZE_CULL_SET_OPERATIONS
	m_Source->Process(ph);
	Value& val = ph.PopValue();
#endif // OPTIMIZE_CULL_SET_OPERATIONS

	switch(m_Type)
	{
	case kFormatTypeVector3:
		if(IsRelative())
		{
			ApplyDefaultVector3(val.GetVector(), ph, false);
		}
		frame.SetVector3(m_Track, m_Id, val.GetVector());
		break;

	case kFormatTypeQuaternion:
		if(IsRelative())
		{
			ApplyDefaultQuaternion(val.GetQuaternion(), ph, false);
		}
		frame.SetQuaternion(m_Track, m_Id, val.GetQuaternion());
		break;

	case kFormatTypeFloat:
		frame.SetFloat(m_Track, m_Id, val.GetFloat());
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSet::Serialize(SerializeHelper& sh)
{
	crExpressionOpGetSet::Serialize(sh);

	sh << m_Source;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpValid::crExpressionOpValid()
: crExpressionOpGetSet(kOpTypeValid)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpValid::crExpressionOpValid(eOpTypes opType)
: crExpressionOpGetSet(opType)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpValid::crExpressionOpValid(datResource& rsc)
: crExpressionOpGetSet(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpValid::DeclareStruct(datTypeStruct& s)
{
	crExpressionOpGetSet::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpValid, crExpressionOpGetSet)
	SSTRUCT_END(crExpressionOpValid)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpValid::~crExpressionOpValid()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpValid, crExpressionOp::kOpTypeValid);

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpValid::Process(ProcessHelper& ph) const
{
	const crFrame& frame = *ph.m_Frame;
	Value& val = ph.PushValue();
	val.Set(frame.HasDofValid(m_Track, m_Id));
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpComponentGetSet::crExpressionOpComponentGetSet(eOpTypes opType)
: crExpressionOpGetSet(opType)
{
	SetComponent(0);
	SetOrder(0);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpComponentGetSet::crExpressionOpComponentGetSet(datResource& rsc)
: crExpressionOpGetSet(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpComponentGetSet::DeclareStruct(datTypeStruct& s)
{
	crExpressionOpGetSet::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpComponentGetSet, crExpressionOpGetSet)
	SSTRUCT_END(crExpressionOpComponentGetSet)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpComponentGetSet::~crExpressionOpComponentGetSet()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpComponentGetSet::Serialize(SerializeHelper& sh)
{
	crExpressionOpGetSet::Serialize(sh);

	u8 component = GetComponent();
	u8 order = (u8)GetOrder();

	sh << datLabel("Component:") << component << datNewLine;
	sh << datLabel("Order:") << order << datNewLine;

	SetComponent(component);
	SetOrder(order);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpComponentGetSet::Dump(DumpHelper& dh) const
{
	crExpressionOpGetSet::Dump(dh);

	dh.Outputf(2, "component %d order %d", int(GetComponent()), int(GetOrder()));
}

////////////////////////////////////////////////////////////////////////////////

bool crExpressionOpComponentGetSet::GetDefaultComponentValue(ProcessHelper& ph, Value& outValue, bool isRelative) const
{
	switch(m_Track)
	{
	case kTrackBoneTranslation:
	case kTrackBoneRotation:
	case kTrackBoneScale:
		if(isRelative)
		{
			break;
		}
		// break intentionally omitted

		if(ph.m_SkeletonData)
		{
			int idx;
			if(ph.m_SkeletonData->ConvertBoneIdToIndex(m_Id, idx))
			{
				const crBoneData* bd = ph.m_SkeletonData->GetBoneData(idx);
				if(bd)
				{
					switch(m_Track)
					{
					case kTrackBoneTranslation:
						Assert(m_Type == kFormatTypeVector3);	
						outValue.Set(bd->GetDefaultTranslation()[GetComponent()]);
						return true;
						// break;

					case kTrackBoneRotation:
						Assert(m_Type == kFormatTypeQuaternion);
						{
							QuatV q = bd->GetDefaultRotation();
							Vec3V e = QuatVToEulers(q, EulerAngleOrder(GetOrder()));
							outValue.Set(e[GetComponent()]);
							return true;
						}
						// break;

					case kTrackBoneScale:
						Assert(m_Type == kFormatTypeVector3);	
						outValue.Set(bd->GetDefaultScale()[GetComponent()]);
						return true;
						// break;

					default:
						Assert(0);
						break;
					}
				}
			}
		}
		break;

	default:
		break;
	}

	// not a bone based dof, or failed to find default - fill with zero value
	outValue.Zero();
	return false;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpComponentGet::crExpressionOpComponentGet()
: crExpressionOpComponentGetSet(kOpTypeComponentGet)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpComponentGet::crExpressionOpComponentGet(const crExpressionOpComponentGet& other)
: crExpressionOpComponentGetSet(other)
{
	SetForceDefault(other.IsForceDefault());
	SetRelative(other.IsRelative());
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpComponentGet::crExpressionOpComponentGet(datResource& rsc)
: crExpressionOpComponentGetSet(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpComponentGet::DeclareStruct(datTypeStruct& s)
{
	crExpressionOpComponentGetSet::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpComponentGet, crExpressionOpGetSet)
	SSTRUCT_END(crExpressionOpComponentGet)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpComponentGet::~crExpressionOpComponentGet()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpComponentGet, crExpressionOp::kOpTypeComponentGet);

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpComponentGet::Process(ProcessHelper& ph) const
{
	const crFrame& frame = *ph.m_Frame;

	Value& val = ph.PushValue();
	val.Zero();

	if(!IsForceDefault())
	{
		switch(m_Type)
		{
		case kFormatTypeVector3:
			{
				Vec3V v;
				if(frame.GetVector3(m_Track, m_Id, v))
				{
					if(IsRelative())
					{
						ApplyDefaultVector3(v, ph, true);
					}
					val.Set(v[GetComponent()]);
					return;
				}
			}
			break;

		case kFormatTypeQuaternion:
			{
				QuatV q;
				if(frame.GetQuaternion(m_Track, m_Id, q))
				{
					if(IsRelative())
					{
						ApplyDefaultQuaternion(q, ph, true);
					}

					Vec3V e = QuatVToEulers(q, EulerAngleOrder(GetOrder()));
					val.Set(e[GetComponent()]);
					return;
				}
			}
			break;

		case kFormatTypeFloat:
			{
				float f;
				if(frame.GetFloat(m_Track, m_Id, f))
				{
					val.Set(f);
					return;
				}
			}
			break;
		}
	}

	// failed to get component value, revert to default
	GetDefaultComponentValue(ph, val, IsRelative());
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpComponentGet::Serialize(SerializeHelper& sh)
{
	crExpressionOpComponentGetSet::Serialize(sh);

	if(sh.m_Version >= 3)
	{
		bool isForceDefault = IsForceDefault();
		sh << datLabel("ForceDefault:") << isForceDefault << datNewLine;
		if(sh.IsRead())
		{
			SetForceDefault(isForceDefault);
		}
	}

	if(sh.m_Version >= 4)
	{
		bool isRelative = IsRelative();
		sh << datLabel("OrientSpace:") << isRelative << datNewLine; // NOTE - relative uses old orient space label for backward compatibility
		if(sh.IsRead())
		{
			SetRelative(isRelative);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpComponentGet::Dump(DumpHelper& dh) const
{
	crExpressionOpComponentGetSet::Dump(dh);

	dh.Outputf(2, "force default %d relative %d", int(IsForceDefault()), int(IsRelative()));
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpComponentSet::crExpressionOpComponentSet()
: crExpressionOpComponentGetSet(kOpTypeComponentSet)
, m_Source(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpComponentSet::crExpressionOpComponentSet(const crExpressionOpComponentSet& other)
: crExpressionOpComponentGetSet(other)
{
	m_Source = other.m_Source ? other.m_Source->Clone() : NULL;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpComponentSet::crExpressionOpComponentSet(datResource& rsc)
: crExpressionOpComponentGetSet(rsc)
, m_Source(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpComponentSet::DeclareStruct(datTypeStruct& s)
{
	crExpressionOpComponentGetSet::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpComponentSet, crExpressionOpGetSet)
	SSTRUCT_FIELD(crExpressionOpComponentSet, m_Source)
	SSTRUCT_END(crExpressionOpComponentSet)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpComponentSet::~crExpressionOpComponentSet()
{
	if(m_Source)
	{
		delete m_Source;
		m_Source = NULL;
	}

	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpComponentSet::Shutdown()
{
	if(m_Source)
	{
		delete m_Source;
		m_Source = NULL;
	}

	crExpressionOpComponentGetSet::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpComponentSet, crExpressionOp::kOpTypeComponentSet);

////////////////////////////////////////////////////////////////////////////////

u32 crExpressionOpComponentSet::GetNumChildren() const
{
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

const crExpressionOp* crExpressionOpComponentSet::GetChild(u32 ASSERT_ONLY(idx)) const
{
	Assert(idx == 0);
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpressionOpComponentSet::GetChild(u32 ASSERT_ONLY(idx))
{
	Assert(idx == 0);
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpComponentSet::SetChild(u32 ASSERT_ONLY(idx), crExpressionOp& op)
{
	Assert(idx == 0);
	m_Source = &op;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpComponentSet::Process(ProcessHelper& ph) const
{
	crFrame& frame = *ph.m_Frame;
	crFrameFilter* filter = ph.m_Filter;

#if !OPTIMIZE_CULL_SET_OPERATIONS
	m_Source->Process(ph);

	Value& val = ph.PopValue();
	float f = val.GetFloat();
#endif // !OPTIMIZE_CULL_SET_OPERATIONS

	float weight = 1.f;
	if(filter && !filter->FilterDof(GetTrack(), GetId(), weight))
	{
		return;
	}

#if OPTIMIZE_CULL_SET_OPERATIONS
	m_Source->Process(ph);

	const Value& val = ph.PopValue();
	float f;
	val.Get(f);
#endif // OPTIMIZE_CULL_SET_OPERATIONS

	switch(m_Type)
	{
	case kFormatTypeVector3:
		{
			Vec3V v;
			if(frame.GetVector3(m_Track, m_Id, v) || GetDefaultVector3(ph, v))
			{
				if(IsRelative())
				{
					Vec3V vd;
					if(GetDefaultVector3(ph, vd))
					{
						f += vd[GetComponent()];
					}
				}
				v[GetComponent()] = f;
				frame.SetVector3(m_Track, m_Id, v);
			}
		}
		break;

	case kFormatTypeQuaternion:
		{
			QuatV q;
			if(frame.GetQuaternion(m_Track, m_Id, q) || GetDefaultQuaternion(ph, q))
			{
				if(IsRelative())
				{
					QuatV qd;
					if(GetDefaultQuaternion(ph, qd))
					{
						EulerAngleOrder order = EulerAngleOrder(GetOrder());
						Vec3V ed = QuatVToEulers(qd, order);
						f += ed[GetComponent()];
					}
				}

				EulerAngleOrder order = EulerAngleOrder(GetOrder());
				Vec3V e = QuatVToEulers(q, order);
				e[GetComponent()] = f;
				q = QuatVFromEulers(e, order);
				frame.SetQuaternion(m_Track, m_Id, q);
			}
		}
		break;

	case kFormatTypeFloat:
		frame.SetFloat(m_Track, m_Id, f);
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpComponentSet::Serialize(SerializeHelper& sh)
{
	crExpressionOpComponentGetSet::Serialize(sh);

	sh << m_Source;
}

////////////////////////////////////////////////////////////////////////////////

bool crExpressionOpComponentSet::GetDefaultVector3(ProcessHelper& ph, Vec3V_InOut v) const
{
	switch(m_Track)
	{
	case kTrackBoneTranslation:
	case kTrackBoneScale:
		if(ph.m_SkeletonData)
		{
			int idx;
			if(ph.m_SkeletonData->ConvertBoneIdToIndex(m_Id, idx))
			{
				const crBoneData* bd = ph.m_SkeletonData->GetBoneData(idx);
				if(bd)
				{
					Assert(m_Type == kFormatTypeVector3);	
					v = (m_Track==kTrackBoneTranslation)?bd->GetDefaultTranslation():bd->GetDefaultScale();
					return true;
				}
			}
		}
		break;

	default:
		break;
	}

	v = Vec3V(V_ZERO);
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crExpressionOpComponentSet::GetDefaultQuaternion(ProcessHelper& ph, QuatV_InOut q) const
{
	switch(m_Track)
	{
	case kTrackBoneRotation:
		if(ph.m_SkeletonData)
		{
			int idx;
			if(ph.m_SkeletonData->ConvertBoneIdToIndex(m_Id, idx))
			{
				const crBoneData* bd = ph.m_SkeletonData->GetBoneData(idx);
				if(bd)
				{
					Assert(m_Type == kFormatTypeQuaternion);
					q = bd->GetDefaultRotation();
					return true;
				}
			}
		}
		break;

	default:
		break;
	}

	q = QuatV(V_IDENTITY);
	return false;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceGet::crExpressionOpObjectSpaceGet(eOpTypes opType)
: crExpressionOpGet(opType)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceGet::crExpressionOpObjectSpaceGet()
: crExpressionOpGet(kOpTypeObjectSpaceGet)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceGet::crExpressionOpObjectSpaceGet(datResource& rsc)
: crExpressionOpGet(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceGet::~crExpressionOpObjectSpaceGet()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpObjectSpaceGet, crExpressionOp::kOpTypeObjectSpaceGet);

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpObjectSpaceGet::Process(ProcessHelper& ph) const
{
	Value& val = ph.PushValue();
	switch(m_Type)
	{
	case kFormatTypeQuaternion:
		val.Set(QuatV(V_IDENTITY));
		break;

	default:
		val.Zero();
		break;
	}

	TransformV trans;
	CalcObjectSpaceMatrix(ph, trans);

	switch(m_Track)
	{
	case kTrackBoneTranslation:
		val.Set(trans.GetPosition());
		break;

	case kTrackBoneRotation:
		val.Set(trans.GetRotation());
		break;

	case kTrackBoneScale:
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpObjectSpaceGet::CalcObjectSpaceMatrix(ProcessHelper& ph, TransformV_InOut outTrans) const
{
	outTrans = TransformV(V_IDENTITY);
	if(ph.m_SkeletonData)
	{
		int idx;
		if(ph.m_SkeletonData->ConvertBoneIdToIndex(m_Id, idx))
		{
			const crBoneData* bd = ph.m_SkeletonData->GetBoneData(idx);
			while(bd)
			{
				Vec3V trans = bd->GetDefaultTranslation();
				QuatV rot = bd->GetDefaultRotation();
				ph.m_Frame->GetBoneTranslation(bd->GetBoneId(), trans);
				ph.m_Frame->GetBoneRotation(bd->GetBoneId(), rot);
				Transform(outTrans, TransformV(rot, trans), outTrans);
				outTrans.SetRotation(Normalize(outTrans.GetRotation()));
				bd = bd->GetParent();
			}
			return;
		}
	}

	Mat34V mtx;
	ph.m_Frame->GetBoneMatrix(m_Id, mtx, true);
	TransformVFromMat34V(outTrans, mtx);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpObjectSpaceGet::CalcParentObjectSpaceMatrix(ProcessHelper& ph, TransformV_InOut outTrans) const
{
	outTrans = TransformV(V_IDENTITY);
	if(ph.m_SkeletonData)
	{
		int idx;
		if(ph.m_SkeletonData->ConvertBoneIdToIndex(m_Id, idx))
		{
			const crBoneData* bd = ph.m_SkeletonData->GetBoneData(idx);
			if(bd)
			{
				bd = bd->GetParent();
				while(bd)
				{
					Vec3V trans = bd->GetDefaultTranslation();
					QuatV rot = bd->GetDefaultRotation();
					ph.m_Frame->GetBoneTranslation(bd->GetBoneId(), trans);
					ph.m_Frame->GetBoneRotation(bd->GetBoneId(), rot);
					Transform(outTrans, TransformV(rot, trans), outTrans);
					outTrans.SetRotation(Normalize(outTrans.GetRotation()));
					bd = bd->GetParent();
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceSet::crExpressionOpObjectSpaceSet(eOpTypes opType)
: crExpressionOpObjectSpaceGet(opType)
, m_Source(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceSet::crExpressionOpObjectSpaceSet()
: crExpressionOpObjectSpaceGet(kOpTypeObjectSpaceSet)
, m_Source(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceSet::crExpressionOpObjectSpaceSet(const crExpressionOpObjectSpaceSet& other)
: crExpressionOpObjectSpaceGet(other)
{
	m_Source = other.m_Source ? other.m_Source->Clone() : NULL;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceSet::crExpressionOpObjectSpaceSet(datResource& rsc)
: crExpressionOpObjectSpaceGet(rsc)
, m_Source(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpObjectSpaceSet::DeclareStruct(datTypeStruct& s)
{
	crExpressionOpObjectSpaceGet::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpObjectSpaceSet, crExpressionOpObjectSpaceGet)
	SSTRUCT_FIELD(crExpressionOpObjectSpaceSet, m_Source)
	SSTRUCT_END(crExpressionOpObjectSpaceSet)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceSet::~crExpressionOpObjectSpaceSet()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpObjectSpaceSet::Shutdown()
{
	if(m_Source)
	{
		delete m_Source;
		m_Source = NULL;
	}

	crExpressionOpObjectSpaceGet::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpObjectSpaceSet, crExpressionOp::kOpTypeObjectSpaceSet);

////////////////////////////////////////////////////////////////////////////////

u32 crExpressionOpObjectSpaceSet::GetNumChildren() const
{
	 return 1;
}

////////////////////////////////////////////////////////////////////////////////

const crExpressionOp* crExpressionOpObjectSpaceSet::GetChild(u32 ASSERT_ONLY(idx)) const
{
	Assert(idx == 0);
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpressionOpObjectSpaceSet::GetChild(u32 ASSERT_ONLY(idx))
{
	Assert(idx == 0);
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpObjectSpaceSet::SetChild(u32 ASSERT_ONLY(idx), crExpressionOp& op)
{
	Assert(idx == 0);
	m_Source = &op;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpObjectSpaceSet::Process(ProcessHelper& ph) const
{
	m_Source->Process(ph);
	Value& val = ph.PopValue();

	TransformV trans;
	CalcParentObjectSpaceMatrix(ph, trans);

	switch(m_Track)
	{
	case kTrackBoneTranslation:
		ph.m_Frame->SetBoneTranslation(m_Id, UnTransform(trans, val.GetVector()));
		break;

	case kTrackBoneRotation:
		ph.m_Frame->SetBoneRotation(m_Id, Multiply(InvertNormInput(trans.GetRotation()), val.GetQuaternion()));
		break;

	case kTrackBoneScale:
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpObjectSpaceSet::Serialize(SerializeHelper& sh)
{
	crExpressionOpObjectSpaceGet::Serialize(sh);

	sh << m_Source;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceConvertTo::crExpressionOpObjectSpaceConvertTo()
: crExpressionOpObjectSpaceSet(kOpTypeObjectSpaceConvertTo)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceConvertTo::crExpressionOpObjectSpaceConvertTo(datResource& rsc)
: crExpressionOpObjectSpaceSet(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceConvertTo::~crExpressionOpObjectSpaceConvertTo()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpObjectSpaceConvertTo, crExpressionOp::kOpTypeObjectSpaceConvertTo);

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpObjectSpaceConvertTo::Process(ProcessHelper& ph) const
{
	m_Source->Process(ph);
	Value& val = ph.TopValue();

	TransformV trans;
	CalcParentObjectSpaceMatrix(ph, trans);

	switch(m_Track)
	{
	case kTrackBoneTranslation:
		val.GetVector() = Transform(trans, val.GetVector());
		break;

	case kTrackBoneRotation:
		val.GetQuaternion() = Multiply(val.GetQuaternion(), trans.GetRotation());  // TODO - might be multiply from left?
		break;

	case kTrackBoneScale:
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceConvertFrom::crExpressionOpObjectSpaceConvertFrom()
: crExpressionOpObjectSpaceSet(kOpTypeObjectSpaceConvertFrom)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceConvertFrom::crExpressionOpObjectSpaceConvertFrom(datResource& rsc)
: crExpressionOpObjectSpaceSet(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpObjectSpaceConvertFrom::~crExpressionOpObjectSpaceConvertFrom()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpObjectSpaceConvertFrom, crExpressionOp::kOpTypeObjectSpaceConvertFrom);

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpObjectSpaceConvertFrom::Process(ProcessHelper& ph) const
{
	m_Source->Process(ph);
	Value& val = ph.TopValue();

	TransformV trans;
	CalcParentObjectSpaceMatrix(ph, trans);

	switch(m_Track)
	{
	case kTrackBoneTranslation:
		val.GetVector() = UnTransform(trans, val.GetVector());
		break;

	case kTrackBoneRotation:
		val.GetQuaternion() = Multiply(InvertNormInput(trans.GetRotation()), val.GetQuaternion());
		break;

	case kTrackBoneScale:
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpMotion::crExpressionOpMotion()
: crExpressionOp(kOpTypeMotion)
, m_AccRotIdx(-1)
, m_AccTransIdx(-1)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpMotion::crExpressionOpMotion(datResource& rsc)
: crExpressionOp(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpMotion::DeclareStruct(datTypeStruct& s)
{
	crExpressionOp::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpMotion, crExpressionOp)
	SSTRUCT_FIELD(crExpressionOpMotion, m_AccRotIdx)
	SSTRUCT_FIELD(crExpressionOpMotion, m_AccTransIdx)
	SSTRUCT_FIELD(crExpressionOpMotion, m_Descr)
	SSTRUCT_END(crExpressionOpMotion)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpMotion, crExpressionOp::kOpTypeMotion);

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpMotion::Process(ProcessHelper&) const
{
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpMotion::Serialize(SerializeHelper& sh)
{
	crExpressionOp::Serialize(sh);

	sh << datLabel("LinearStrength:") << m_Descr.m_Linear.m_Strength << datNewLine;
	sh << datLabel("LinearDamping:") << m_Descr.m_Linear.m_Damping << datNewLine;
	sh << datLabel("LinearMinConstraint:") << m_Descr.m_Linear.m_MinConstraint << datNewLine;
	sh << datLabel("LinearMaxConstraint:") << m_Descr.m_Linear.m_MaxConstraint << datNewLine;
	sh << datLabel("AngularStrength:") << m_Descr.m_Angular.m_Strength << datNewLine;
	sh << datLabel("AngularDamping:") << m_Descr.m_Angular.m_Damping << datNewLine;
	sh << datLabel("AngularMinConstraint:") << m_Descr.m_Angular.m_MinConstraint << datNewLine;
	sh << datLabel("AngularMaxConstraint:") << m_Descr.m_Angular.m_MaxConstraint << datNewLine;
	sh << datLabel("Gravity:") << m_Descr.m_Gravity << datNewLine;

	if(sh.m_Version >= 14)
	{
		sh << datLabel("Direction:") << m_Descr.m_Direction << datNewLine;
	}
	else
	{
		m_Descr.m_Direction = Vec3V(V_ZERO);
	}
}

void crExpressionOpMotion::Dump(DumpHelper& dh) const
{
	crExpressionOp::Dump(dh);

	dh.Outputf(2, "bone id '%d'", m_Descr.GetBoneId());
	dh.Outputf(2, "gravity (%f, %f, %f)", m_Descr.m_Gravity.GetXf(), m_Descr.m_Gravity.GetYf(), m_Descr.m_Gravity.GetZf());
	dh.Outputf(2, "direction (%f, %f, %f)", m_Descr.m_Direction.GetXf(), m_Descr.m_Direction.GetYf(), m_Descr.m_Direction.GetZf());
	dh.Outputf(2, "linear strength (%f, %f, %f)", m_Descr.m_Linear.m_Strength.GetXf(), m_Descr.m_Linear.m_Strength.GetYf(), m_Descr.m_Linear.m_Strength.GetZf());
	dh.Outputf(2, "angular strength (%f, %f, %f)", m_Descr.m_Angular.m_Strength.GetXf(), m_Descr.m_Angular.m_Strength.GetYf(), m_Descr.m_Angular.m_Strength.GetZf());
	dh.Outputf(2, "linear damping (%f, %f, %f)", m_Descr.m_Linear.m_Damping.GetXf(), m_Descr.m_Linear.m_Damping.GetYf(), m_Descr.m_Linear.m_Damping.GetZf());
	dh.Outputf(2, "angular damping (%f, %f, %f)", m_Descr.m_Angular.m_Damping.GetXf(), m_Descr.m_Angular.m_Damping.GetYf(), m_Descr.m_Angular.m_Damping.GetZf());
	dh.Outputf(2, "linear min constraint (%f, %f, %f)", m_Descr.m_Linear.m_MinConstraint.GetXf(), m_Descr.m_Linear.m_MinConstraint.GetYf(), m_Descr.m_Linear.m_MinConstraint.GetZf());
	dh.Outputf(2, "angular min constraint (%f, %f, %f)", m_Descr.m_Angular.m_MinConstraint.GetXf(), m_Descr.m_Angular.m_MinConstraint.GetYf(), m_Descr.m_Angular.m_MinConstraint.GetZf());
	dh.Outputf(2, "linear max constraint (%f, %f, %f)", m_Descr.m_Linear.m_MaxConstraint.GetXf(), m_Descr.m_Linear.m_MaxConstraint.GetYf(), m_Descr.m_Linear.m_MaxConstraint.GetZf());
	dh.Outputf(2, "angular max constraint (%f, %f, %f)", m_Descr.m_Angular.m_MaxConstraint.GetXf(), m_Descr.m_Angular.m_MaxConstraint.GetYf(), m_Descr.m_Angular.m_MaxConstraint.GetZf());
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpNullary::crExpressionOpNullary()
: crExpressionOp(kOpTypeNullary)
{
	SetNullaryOpType(kNullaryOpTypeNone);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpNullary::crExpressionOpNullary(datResource& rsc)
: crExpressionOp(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
datSwapper_ENUM(crExpressionOpNullary::eNullaryOpTypes);
void crExpressionOpNullary::DeclareStruct(datTypeStruct& s)
{	
	crExpressionOp::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpNullary, crExpressionOp)
	SSTRUCT_END(crExpressionOpNullary)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpNullary::~crExpressionOpNullary()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpNullary, crExpressionOp::kOpTypeNullary);

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpNullary::Process(ProcessHelper& ph) const
{
	Value& val = ph.PushValue();

	switch(GetNullaryOpType())
	{
	case kNullaryOpTypeZero:
		val.Set(0.f);
		break;

	case kNullaryOpTypeOne:
		val.Set(1.f);
		break;

	case kNullaryOpTypePi:
		val.Set(PI);
		break;

	case kNullaryOpTypeTime:
		val.Set(ph.m_Time);
		break;

	case kNullaryOpTypeRandom:
		val.Set(g_ReplayRand.GetFloat());
		break;

	case kNullaryOpTypeVectorZero:
		val.Set(Vec3V(V_ZERO));
		break;

	case kNullaryOpTypeVectorOne:
		val.Set(Vec3V(V_ONE));
		break;

	case kNullaryOpTypeDeltaTime:
		val.Set(ph.m_DeltaTime);
		break;

	case kNullaryOpTypeQuatIdentity:
		val.Set(QuatV(V_IDENTITY));
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpNullary::Serialize(SerializeHelper& sh)
{
	crExpressionOp::Serialize(sh);

	int nullaryOpType = GetNullaryOpType();
	sh << datLabel("NullaryOpType:") << nullaryOpType << datNewLine;
	if(sh.IsRead())
	{
		SetNullaryOpType(eNullaryOpTypes(nullaryOpType));
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpNullary::Dump(DumpHelper& dh) const
{
	crExpressionOp::Dump(dh);

	const char* nullaryOpTypeNames[] =
	{
		"NONE",
		"Zero",
		"One",
		"Pi",
		"Time",
		"Random",
		"VectorZero",
		"VectorOne",
		"DeltaTime",
		"QuatIdentity"
	};
	CompileTimeAssert(NELEM(nullaryOpTypeNames) == kNullaryOpTypeNum);

	dh.Outputf(2, "nullary op type %d '%s'", GetNullaryOpType(), nullaryOpTypeNames[GetNullaryOpType()]);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpUnary::crExpressionOpUnary()
: crExpressionOp(kOpTypeUnary)
, m_Source(NULL)
{
	SetUnaryOpType(kUnaryOpTypeNone);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpUnary::crExpressionOpUnary(const crExpressionOpUnary& other)
: crExpressionOp(other)
{
	m_Source = other.m_Source ? other.m_Source->Clone() : NULL;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpUnary::crExpressionOpUnary(datResource& rsc)
: crExpressionOp(rsc)
, m_Source(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
datSwapper_ENUM(crExpressionOpUnary::eUnaryOpTypes);
void crExpressionOpUnary::DeclareStruct(datTypeStruct& s)
{
	crExpressionOp::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpUnary, crExpressionOp)
	SSTRUCT_FIELD(crExpressionOpUnary, m_Source)
	SSTRUCT_END(crExpressionOpUnary)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpUnary::~crExpressionOpUnary()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpUnary::Shutdown()
{
	if(m_Source)
	{
		delete m_Source;
		m_Source = NULL;
	}

	crExpressionOp::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpUnary, crExpressionOp::kOpTypeUnary);

////////////////////////////////////////////////////////////////////////////////

u32 crExpressionOpUnary::GetNumChildren() const
{
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

const crExpressionOp* crExpressionOpUnary::GetChild(u32 ASSERT_ONLY(idx)) const
{
	Assert(idx == 0);
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpressionOpUnary::GetChild(u32 ASSERT_ONLY(idx))
{
	Assert(idx == 0);
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpUnary::SetChild(u32 ASSERT_ONLY(idx), crExpressionOp& op)
{
	Assert(idx == 0);
	m_Source = &op;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpUnary::Process(ProcessHelper& ph) const
{
	m_Source->Process(ph);
	Value& val = ph.TopValue();

	switch(GetUnaryOpType())
	{
	case kUnaryOpTypeLogicalNot:
		val.Set(!val.GetInt());
		break;

	case kUnaryOpTypeNegate:
		val.Set(-val.GetFloat());
		break;

	case kUnaryOpTypeReciprocal:
		val.Set(1.f/val.GetFloat());
		break;

	case kUnaryOpTypeSquare:
		val.Set(square(val.GetFloat()));
		break;

	case kUnaryOpTypeSqrt:
		val.Set(sqrt(val.GetFloat()));
		break;

	case kUnaryOpTypeAbsolute:
		val.Set(fabs(val.GetFloat()));
		break;

	case kUnaryOpTypeFloor:
		val.Set(floor(val.GetFloat()));
		break;

	case kUnaryOpTypeCeil:
		val.Set(ceil(val.GetFloat()));
		break;

	case kUnaryOpTypeLog:
		val.Set(log10(val.GetFloat()));
		break;

	case kUnaryOpTypeLn:
		val.Set(log(val.GetFloat()));
		break;

	case kUnaryOpTypeExp:
		val.Set(exp(val.GetFloat()));
		break;

	case kUnaryOpTypeClamp01:
		val.Set(Clamp(val.GetFloat(), 0.f, 1.f));
		break;

	case kUnaryOpTypeCos:
		val.Set(cos(val.GetFloat()));
		break;

	case kUnaryOpTypeSin:
		val.Set(sin(val.GetFloat()));
		break;

	case kUnaryOpTypeTan:
		val.Set(tan(val.GetFloat()));
		break;

	case kUnaryOpTypeArcCos:
		val.Set(acos(val.GetFloat()));
		break;

	case kUnaryOpTypeArcSin:
		val.Set(asin(val.GetFloat()));
		break;

	case kUnaryOpTypeArcTan:
		val.Set(atan(val.GetFloat()));
		break;

	case kUnaryOpTypeCosH:
		val.Set(cosh(val.GetFloat()));
		break;

	case kUnaryOpTypeSinH:
		val.Set(sinh(val.GetFloat()));
		break;

	case kUnaryOpTypeTanH:
		val.Set(tanh(val.GetFloat()));
		break;

	case kUnaryOpTypeDegreesToRadians:
		val.Set(val.GetFloat()*DtoR);
		break;

	case kUnaryOpTypeRadiansToDegrees:
		val.Set(val.GetFloat()*RtoD);
		break;

	case kUnaryOpTypeFromEuler:
		val.GetQuaternion() = QuatVFromEulersXYZ(val.GetVector());
		break;

	case kUnaryOpTypeToEuler:
		val.GetVector() = QuatVToEulersXYZ(val.GetQuaternion());
		break;

	case kUnaryOpTypeSplat:
		val.Set(Vec3VFromF32(val.GetFloat()));
		break;

	case kUnaryOpTypeVectorClamp01:
		val.Set( Clamp( val.GetVector(), Vec3V(V_ZERO), Vec3V(V_ONE) ) );
		break;

	case kUnaryOpTypeQuatInverse:
		val.Set(InvertNormInput(val.GetQuaternion()));
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpUnary::Serialize(SerializeHelper& sh)
{
	crExpressionOp::Serialize(sh);

	int unaryOpType = GetUnaryOpType();
	sh << datLabel("UnaryOpType:") << unaryOpType << datNewLine;
	if(sh.IsRead())
	{
		SetUnaryOpType(eUnaryOpTypes(unaryOpType));
	}

	sh << m_Source;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpUnary::Dump(DumpHelper& dh) const
{
	crExpressionOp::Dump(dh);

	const char* unaryOpTypeNames[] =
	{
		"NONE",
		"LogicalNot",
		"Negate",
		"Reciprocal",
		"Square",
		"Sqrt",
		"Absolute",
		"Floor",
		"Ceil",
		"Log",
		"Ln",
		"Exp",
		"Clamp01",
		"Cos",
		"Sin",
		"Tan",
		"ArcCos",
		"ArcSin",
		"ArcTan",
		"CosH",
		"SinH",
		"TanH",
		"DegreesToRadians",
		"RadiansToDegrees",
		"FromEuler",
		"ToEuler",
		"Splat",
		"VectorClamp01",
		"QuatInverse"
	};
	CompileTimeAssert(NELEM(unaryOpTypeNames) == kUnaryOpTypeNum);

	dh.Outputf(2, "unary op type %d '%s'", GetUnaryOpType(), unaryOpTypeNames[GetUnaryOpType()]);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpBinary::crExpressionOpBinary()
: crExpressionOp(kOpTypeBinary)
{
	SetBinaryOpType(kBinaryOpTypeNone);
	m_Sources[0] = m_Sources[1] = NULL;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpBinary::crExpressionOpBinary(const crExpressionOpBinary& other)
: crExpressionOp(other)
{
	for(int i=0; i<2; ++i)
	{
		m_Sources[i] = other.m_Sources[i] ? other.m_Sources[i]->Clone() : NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpBinary::crExpressionOpBinary(datResource& rsc)
: crExpressionOp(rsc)
//, m_Sources(rsc, true)  // not needed on a atRangeArray of datOwners
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
datSwapper_ENUM(crExpressionOpBinary::eBinaryOpTypes);
void crExpressionOpBinary::DeclareStruct(datTypeStruct& s)
{
	crExpressionOp::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpBinary, crExpressionOp)
	SSTRUCT_FIELD(crExpressionOpBinary, m_Sources)
	SSTRUCT_END(crExpressionOpBinary)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpBinary::~crExpressionOpBinary()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpBinary::Shutdown()
{
	for(int i=0; i<2; ++i)
	{
		if(m_Sources[i])
		{
			delete m_Sources[i];
			m_Sources[i] = NULL;
		}
	}

	crExpressionOp::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpBinary, crExpressionOp::kOpTypeBinary);

////////////////////////////////////////////////////////////////////////////////

u32 crExpressionOpBinary::GetNumChildren() const
{
	return 2;
}

////////////////////////////////////////////////////////////////////////////////

const crExpressionOp* crExpressionOpBinary::GetChild(u32 idx) const
{
	return m_Sources[idx];
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpressionOpBinary::GetChild(u32 idx)
{
	return m_Sources[idx];
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpBinary::SetChild(u32 idx, crExpressionOp& op)
{
	m_Sources[idx] = &op;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpBinary::Process(ProcessHelper& ph) const
{
	switch(GetBinaryOpType())
	{
	case kBinaryOpTypeLogicalAnd:
	case kBinaryOpTypeLogicalOr:
		{
			m_Sources[0]->Process(ph);
			Value& val0 = ph.TopValue();

			switch(GetBinaryOpType())
			{
			case kBinaryOpTypeLogicalAnd:
				if(val0.GetInt())
				{
					ph.PopValue();
					m_Sources[1]->Process(ph);
				}
				break;

			case kBinaryOpTypeLogicalOr:
				if(!val0.GetInt())
				{
					ph.PopValue();
					m_Sources[1]->Process(ph);
				}
				break;

			default:
				break;
			}
		}
		break;

	default:
		{
			m_Sources[0]->Process(ph);
			m_Sources[1]->Process(ph);

			Value& val1 = ph.PopValue();
			Value& val0 = ph.PopValue();

			Value& ret = ph.PushValue();

			switch(GetBinaryOpType())
			{
			case kBinaryOpTypeEqualTo:
				ret.Set(val0 == val1);
				break;

			case kBinaryOpTypeNotEqualTo:
				ret.Set(val0 != val1);
				break;

			case kBinaryOpTypeGreaterThan:
				ret.Set(val0.GetFloat() > val1.GetFloat());
				break;

			case kBinaryOpTypeLessThan:
				ret.Set(val0.GetFloat() < val1.GetFloat());
				break;

			case kBinaryOpTypeGreaterThanEqualTo:
				ret.Set(val0.GetFloat() >= val1.GetFloat());
				break;

			case kBinaryOpTypeLessThanEqualTo:
				ret.Set(val0.GetFloat() <= val1.GetFloat());
				break;

			case kBinaryOpTypeAdd:
				ret.Set(val0.GetFloat() + val1.GetFloat());
				break;

			case kBinaryOpTypeSubtract:
				ret.Set(val0.GetFloat() - val1.GetFloat());
				break;

			case kBinaryOpTypeMultiply:
				ret.Set(val0.GetFloat() * val1.GetFloat());
				break;

			case kBinaryOpTypeDivide:
				ret.Set(val0.GetFloat() / val1.GetFloat());
				break;

			case kBinaryOpTypeModulus:
				ret.Set(fmodf(val0.GetFloat(), val1.GetFloat()));
				break;

			case kBinaryOpTypeExponent:
				ret.Set(pow(val0.GetFloat(), val1.GetFloat()));
				break;

			case kBinaryOpTypeMax:
				ret.Set(Max(val0.GetFloat(), val1.GetFloat()));
				break;

			case kBinaryOpTypeMin:
				ret.Set(Min(val0.GetFloat(), val1.GetFloat()));
				break;

			case kBinaryOpTypeLogicalXor:
				ret.Set((val0.GetInt() || val1.GetInt()) && !(val0.GetInt() && val1.GetInt()));
				break;

			case kBinaryOpTypeQuatMultiply:
				ret.Set(Multiply(val0.GetQuaternion(), val1.GetQuaternion()));
				break;

			case kBinaryOpTypeVectorAdd:
				ret.Set(Add(val0.GetVector(), val1.GetVector()));
				break;

			case kBinaryOpTypeVectorSubtract:
				ret.Set(Subtract(val0.GetVector(), val1.GetVector()));
				break;

			case kBinaryOpTypeVectorMultiply:
				ret.Set(Scale(val0.GetVector(), val1.GetVector()));
				break;

			case kBinaryOpTypeVectorTransform:
				ret.Set(Transform(val1.GetQuaternion(), val0.GetVector()));
				break;

			case kBinaryOpTypeQuatScale:
				ret.Set(QuatVScaleAngle(val0.GetQuaternion(), ScalarVFromF32(val1.GetFloat())));
				break;

			default:
				break;
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpBinary::Serialize(SerializeHelper& sh)
{
	crExpressionOp::Serialize(sh);

	int binaryOpType = GetBinaryOpType();
	sh << datLabel("BinaryOpType:") << binaryOpType << datNewLine;
	if(sh.IsRead())
	{
		SetBinaryOpType(eBinaryOpTypes(binaryOpType));
	}

	for(int i=0; i<2; ++i)
	{
		sh << m_Sources[i];
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpBinary::Dump(DumpHelper& dh) const
{
	crExpressionOp::Dump(dh);

	const char* binaryOpTypeNames[] =
	{
		"NONE",
		"EqualTo",
		"NotEqualTo",
		"GreaterThan",
		"LessThan",
		"GreaterThanEqualTo",
		"LessThanEqualTo",
		"Add",
		"Subtract",
		"Multiply",
		"Divide",
		"Modulus",
		"Exponent",
		"Max",
		"Min",
		"LogicalAnd",
		"LogicalOr",
		"LogicalXor",
		"QuatMultiply",
		"VectorAdd",
		"VectorMultiply",
		"VectorTransform",
		"VectorSubtract",
		"QuatScale"
	};
	CompileTimeAssert(NELEM(binaryOpTypeNames) == kBinaryOpTypeNum);

	dh.Outputf(2, "binary op type %d '%s'", GetBinaryOpType(), binaryOpTypeNames[GetBinaryOpType()]);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpTernary::crExpressionOpTernary()
: crExpressionOp(kOpTypeTernary)
{
	SetTernaryOpType(kTernaryOpTypeNone);
	m_Sources[0] = m_Sources[1] = m_Sources[2] = NULL;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpTernary::crExpressionOpTernary(const crExpressionOpTernary& other)
: crExpressionOp(other)
{
	for(int i=0; i<3; ++i)
	{
		m_Sources[i] = other.m_Sources[i] ? other.m_Sources[i]->Clone() : NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpTernary::crExpressionOpTernary(datResource& rsc)
: crExpressionOp(rsc)
//, m_Sources(rsc, true)  // not needed on a atRangeArray of datOwners
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
datSwapper_ENUM(crExpressionOpTernary::eTernaryOpTypes);
void crExpressionOpTernary::DeclareStruct(datTypeStruct& s)
{
	crExpressionOp::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpTernary, crExpressionOp)
	SSTRUCT_FIELD(crExpressionOpTernary, m_Sources)
	SSTRUCT_END(crExpressionOpTernary)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpTernary::~crExpressionOpTernary()
{
	for(int i=0; i<3; ++i)
	{
		if(m_Sources[i])
		{
			delete m_Sources[i];
			m_Sources[i] = NULL;
		}
	}

	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpTernary::Shutdown()
{
	for(int i=0; i<3; ++i)
	{
		if(m_Sources[i])
		{
			delete m_Sources[i];
			m_Sources[i] = NULL;
		}
	}

	crExpressionOp::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpTernary, crExpressionOp::kOpTypeTernary);

////////////////////////////////////////////////////////////////////////////////

u32 crExpressionOpTernary::GetNumChildren() const
{
	return 3;
}

////////////////////////////////////////////////////////////////////////////////

const crExpressionOp* crExpressionOpTernary::GetChild(u32 idx) const
{
	return m_Sources[idx];
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpressionOpTernary::GetChild(u32 idx)
{
	return m_Sources[idx];
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpTernary::SetChild(u32 idx, crExpressionOp& op)
{
	m_Sources[idx] = &op;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpTernary::Process(ProcessHelper& ph) const
{
	if(GetTernaryOpType() == kTernaryOpTypeConditional)
	{
		m_Sources[0]->Process(ph);
		Value& val0 = ph.PopValue();
		if(val0.GetInt())
		{
			m_Sources[1]->Process(ph);
		}
		else
		{
			m_Sources[2]->Process(ph);
		}
	}
	else
	{
		m_Sources[0]->Process(ph);
		m_Sources[1]->Process(ph);
		m_Sources[2]->Process(ph);
		
		Value& val2 = ph.PopValue();
		Value& val1 = ph.PopValue();
		Value& val0 = ph.PopValue();

		Value& ret = ph.PushValue();

		switch(GetTernaryOpType())
		{
		case kTernaryOpTypeMultiplyAdd:
			ret.Set(val0.GetFloat()*val1.GetFloat()+val2.GetFloat());
			break;

		case kTernaryOpTypeClamp:
			ret.Set(Clamp(val0.GetFloat(), val1.GetFloat(), val2.GetFloat()));
			break;

		case kTernaryOpTypeLerp:
			ret.Set(Lerp(val0.GetFloat(), val1.GetFloat(), val2.GetFloat()));
			break;

		case kTernaryOpTypeToQuaternion:
			ret.GetQuaternion() = QuatVFromEulersXYZ(Vec3V(val0.GetFloat(), val1.GetFloat(), val2.GetFloat()));
			break;

		case kTernaryOpTypeToVector:
			ret.Set(Vec3V(val0.GetFloat(), val1.GetFloat(), val2.GetFloat()));
			break;

		case kTernaryOpTypeVectorMultiplyAdd:
			ret.GetVector() = AddScaled(val2.GetVector(), val0.GetVector(), val1.GetVector());
			break;

		case kTernaryOpTypeVectorLerpVector:
			ret.GetVector() = Lerp(val0.GetVector(), val1.GetVector(), val2.GetVector());
			break;

		case kTernaryOpTypeQuaternionLerp:
			ret.GetQuaternion() = Nlerp(ScalarVFromF32(val2.GetFloat()), val0.GetQuaternion(), PrepareSlerp(val0.GetQuaternion(), val1.GetQuaternion()));
			break;

		default:
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpTernary::Serialize(SerializeHelper& sh)
{
	crExpressionOp::Serialize(sh);

	int ternaryOpType = GetTernaryOpType();
	sh << datLabel("TernaryOpType:") << ternaryOpType << datNewLine;
	if(sh.IsRead())
	{
		SetTernaryOpType(eTernaryOpTypes(ternaryOpType));
	}

	for(int i=0; i<3; ++i)
	{
		sh << m_Sources[i];
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpTernary::Dump(DumpHelper& dh) const
{
	crExpressionOp::Dump(dh);

	const char* ternaryOpTypeNames[] =
	{
		"NONE",
		"MultiplyAdd",
		"Clamp",
		"Lerp",
		"ToQuaternion",
		"ToVector",
		"Conditional",
		"VectorMultiplyAdd",
		"VectorLerpVector",
		"QuaternionLerp"
	};
	CompileTimeAssert(NELEM(ternaryOpTypeNames) == kTernaryOpTypeNum);

	dh.Outputf(2, "ternary op type %d '%s'", GetTernaryOpType(), ternaryOpTypeNames[GetTernaryOpType()]);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpNary::crExpressionOpNary()
: crExpressionOp(kOpTypeNary)
{
	SetNaryOpType(kNaryOpTypeNone);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpNary::crExpressionOpNary(const crExpressionOpNary& other)
: crExpressionOp(other)
{
	const int numSources = other.m_Sources.GetCount();
	m_Sources.Resize(numSources);
	for(int i=0; i<numSources; ++i)
	{
		m_Sources[i] = other.m_Sources[i] ? other.m_Sources[i]->Clone() : NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpNary::crExpressionOpNary(datResource& rsc)
: crExpressionOp(rsc)
, m_Sources(rsc, true)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
datSwapper_ENUM(crExpressionOpNary::eNaryOpTypes);
void crExpressionOpNary::DeclareStruct(datTypeStruct& s)
{
	crExpressionOp::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpNary, crExpressionOp)
	SSTRUCT_FIELD(crExpressionOpNary, m_Sources)
	SSTRUCT_END(crExpressionOpNary)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpNary::~crExpressionOpNary()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpNary::Shutdown()
{
	const int numSources = m_Sources.GetCount();
	for(int i=0; i<numSources; ++i)
	{
		if(m_Sources[i])
		{
			delete m_Sources[i];
			m_Sources[i] = NULL;
		}
	}
	m_Sources.Reset();

	crExpressionOp::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpNary, crExpressionOp::kOpTypeNary);

////////////////////////////////////////////////////////////////////////////////

u32 crExpressionOpNary::GetNumChildren() const
{
	return m_Sources.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

const crExpressionOp* crExpressionOpNary::GetChild(u32 idx) const
{
	return m_Sources[idx];
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpressionOpNary::GetChild(u32 idx)
{
	return m_Sources[idx];
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpNary::SetChild(u32 idx, crExpressionOp& op)
{
	m_Sources[idx] = &op;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpNary::Process(ProcessHelper& ph) const
{
	const int numSources = m_Sources.GetCount();

	switch(GetNaryOpType())
	{
	case kNaryOpTypeComma:
		{
			ph.PushValue();
			for(int i=0; i<numSources; ++i)
			{
				ph.PopValue();
				m_Sources[i]->Process(ph);
			}
		}
		break;

	case kNaryOpTypeSum:
		{
			float sum = 0.f;
			for(int i=0; i<numSources; ++i)
			{
				m_Sources[i]->Process(ph);
				sum += ph.PopValue().GetFloat();
			}
			ph.PushValue().Set(sum);
		}
		break;

	case kNaryOpTypeList:
		{
			for(int i=0; i<numSources; ++i)
			{
				m_Sources[i]->Process(ph);
			}
		}
		break;

	case kNaryOpTypeLogicalAnd:
		{
			for(int i=0; i<numSources; ++i)
			{
				m_Sources[i]->Process(ph);
				if(ph.PopValue().GetInt() == 0)
				{
					ph.PushValue().Set(0);
					return;
				}
			}
			ph.PushValue().Set(1);
		}
		break;

	case kNaryOpTypeLogicalOr:
		{
			for(int i=0; i<numSources; ++i)
			{
				m_Sources[i]->Process(ph);
				if(ph.PopValue().GetInt() != 0)
				{
					ph.PushValue().Set(1);
					return;
				}
			}
			ph.PushValue().Set(0);
		}
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpNary::Serialize(SerializeHelper& sh)
{
	crExpressionOp::Serialize(sh);
	
	int naryOpType = GetNaryOpType();
	sh << datLabel("NaryOpType:") << naryOpType << datNewLine;
	if(sh.IsRead())
	{
		SetNaryOpType(eNaryOpTypes(naryOpType));
	}

	int numSources = m_Sources.GetCount();
	sh << datLabel("NumSources:") << numSources << datNewLine;
	if(sh.IsRead())
	{
		m_Sources.Resize(numSources);
	}

	for(int i=0; i<numSources; ++i)
	{
		sh << m_Sources[i];
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpNary::Dump(DumpHelper& dh) const
{
	crExpressionOp::Dump(dh);

	const char* naryOpTypeNames[] =
	{
		"NONE",
		"Comma",
		"Sum",
		"List",
		"LogicalAnd",
		"LogicalOr"
	};
	CompileTimeAssert(NELEM(naryOpTypeNames) == kNaryOpTypeNum);

	dh.Outputf(2, "nary op type %d '%s'", GetNaryOpType(), naryOpTypeNames[GetNaryOpType()]);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialBlend::crExpressionOpSpecialBlend()
: crExpressionOp(kOpTypeSpecialBlend)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialBlend::crExpressionOpSpecialBlend(const crExpressionOpSpecialBlend& other)
: crExpressionOp(other)
{
	const int numSourceWeights = other.m_SourceWeights.GetCount();
	m_SourceWeights.Resize(numSourceWeights);
	for(int i=0; i<numSourceWeights; ++i)
	{
		m_SourceWeights[i].m_Weight = other.m_SourceWeights[i].m_Weight;
		m_SourceWeights[i].m_Source = other.m_SourceWeights[i].m_Source ? other.m_SourceWeights[i].m_Source->Clone() : NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialBlend::crExpressionOpSpecialBlend(datResource& rsc)
: crExpressionOp(rsc)
, m_SourceWeights(rsc, true)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpSpecialBlend::DeclareStruct(datTypeStruct& s)
{
	crExpressionOp::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpSpecialBlend, crExpressionOp)
	SSTRUCT_FIELD(crExpressionOpSpecialBlend, m_SourceWeights)
	SSTRUCT_END(crExpressionOpSpecialBlend)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialBlend::~crExpressionOpSpecialBlend()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialBlend::Shutdown()
{
	m_SourceWeights.Reset();

	crExpressionOp::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpSpecialBlend, crExpressionOp::kOpTypeSpecialBlend);

////////////////////////////////////////////////////////////////////////////////

u32 crExpressionOpSpecialBlend::GetNumChildren() const
{
	return m_SourceWeights.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

const crExpressionOp* crExpressionOpSpecialBlend::GetChild(u32 idx) const
{
	return m_SourceWeights[idx].m_Source;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpressionOpSpecialBlend::GetChild(u32 idx)
{
	return m_SourceWeights[idx].m_Source;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialBlend::SetChild(u32 idx, crExpressionOp& op)
{
	m_SourceWeights[idx].m_Source = &op;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialBlend::Process(ProcessHelper& ph) const
{
	switch(GetBlendMode())
	{
	case kBlendModeFloat:
		{
			float sum = 0.f;

			const int numSources = m_SourceWeights.GetCount();
			for(int i=0; i<numSources; ++i)
			{
				const SourceWeight& sw = m_SourceWeights[i];

				sw.m_Source->Process(ph);

				float f = ph.PopValue().GetFloat();

				sum += f*sw.m_Weight;
			}

			ph.PushValue().Set(sum);
		}
		break;

	case kBlendModeVector:
		{
			Vec3V sum(V_ZERO);

			const int numSources = m_SourceWeights.GetCount();
			for(int i=0; i<numSources; ++i)
			{
				const SourceWeight& sw = m_SourceWeights[i];

				sw.m_Source->Process(ph);

				Vec3V v = ph.PopValue().GetVector();

				sum = AddScaled(sum, v, ScalarVFromF32(sw.m_Weight));
			}

			ph.PushValue().Set(sum);
		}
		break;

	case kBlendModeQuaternion:
		{
			QuatV sum(V_IDENTITY);

			const int numSources = m_SourceWeights.GetCount();
			for(int i=0; i<numSources; ++i)
			{
				const SourceWeight& sw = m_SourceWeights[i];

				sw.m_Source->Process(ph);

				QuatV q = ph.PopValue().GetQuaternion();
				if(sw.m_Weight != 1.f)
				{
					q = QuatVScaleAngle(q, ScalarVFromF32(sw.m_Weight));
				}
				sum = Multiply(sum, q);
			}

			ph.PushValue().Set(sum);
		}
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialBlend::Serialize(SerializeHelper& sh)
{
	crExpressionOp::Serialize(sh);

	if(sh.m_Version >= 4)
	{
		u16 blendMode = u16(GetBlendMode());
		sh << datLabel("BlendMode:") << blendMode << datNewLine;
		SetBlendMode(eBlendMode(blendMode));
	}
	else
	{
		Assert(GetBlendMode() != kBlendModeQuaternion);
		bool vectorized = (GetBlendMode() != kBlendModeFloat);
		sh << datLabel("Vectorized:") << vectorized << datNewLine;
		SetBlendMode(vectorized?kBlendModeVector:kBlendModeFloat);
	}

	int numSourceWeights = m_SourceWeights.GetCount();
	sh << datLabel("NumSourceWeights:") << numSourceWeights << datNewLine;
	if(sh.IsRead())
	{
		m_SourceWeights.Resize(numSourceWeights);
	}

	for(int i=0; i<numSourceWeights; ++i)
	{
		sh << datLabel("Weight:") << m_SourceWeights[i].m_Weight << datNewLine;
		sh << m_SourceWeights[i].m_Source;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialBlend::Dump(DumpHelper& dh) const
{
	crExpressionOp::Dump(dh);

	dh.Outputf(2, "blendmode %d", u16(GetBlendMode()));

	const int numSourceWeights = m_SourceWeights.GetCount();

	dh.Outputf(2, "num source weights %d", numSourceWeights);

	for(int i=0; i<numSourceWeights; ++i)
	{
		const SourceWeight& sw = m_SourceWeights[i];

		dh.Outputf(2, "source[%d] weight %f", i, sw.m_Weight);
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialBlend::SourceWeight::SourceWeight()
: m_Source(NULL)
, m_Weight(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialBlend::SourceWeight::SourceWeight(datResource& rsc)
: m_Source(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpSpecialBlend::SourceWeight::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crExpressionOpSpecialBlend::SourceWeight)
	SSTRUCT_FIELD(crExpressionOpSpecialBlend::SourceWeight, m_Source)
	SSTRUCT_FIELD(crExpressionOpSpecialBlend::SourceWeight, m_Weight)
	SSTRUCT_END(crExpressionOpSpecialBlend::SourceWeight)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialBlend::SourceWeight::~SourceWeight()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialBlend::SourceWeight::Shutdown()
{
	if(m_Source)
	{
		delete m_Source;
		m_Source = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialLinear::crExpressionOpSpecialLinear()
: crExpressionOp(kOpTypeSpecialLinear)
, m_Reference(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialLinear::crExpressionOpSpecialLinear(const crExpressionOpSpecialLinear& other)
: crExpressionOp(other)
, m_Sources(other.m_Sources)
, m_Intervals(other.m_Intervals)
, m_IntervalPerSource(other.m_IntervalPerSource)
, m_Mode(other.m_Mode)
, m_Reference(other.m_Reference ?  other.m_Reference->Clone() : NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialLinear::crExpressionOpSpecialLinear(datResource& rsc)
: crExpressionOp(rsc)
, m_Sources(rsc)
, m_Intervals(rsc)
, m_Reference(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpSpecialLinear::DeclareStruct(datTypeStruct& s)
{
	crExpressionOp::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpSpecialLinear, crExpressionOp)
	SSTRUCT_FIELD(crExpressionOpSpecialLinear, m_Sources)
	SSTRUCT_FIELD(crExpressionOpSpecialLinear, m_Intervals)
	SSTRUCT_FIELD(crExpressionOpSpecialLinear, m_Reference)
	SSTRUCT_FIELD(crExpressionOpSpecialLinear, m_IntervalPerSource)
	SSTRUCT_FIELD(crExpressionOpSpecialLinear, m_Mode)
	SSTRUCT_END(crExpressionOpSpecialLinear)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialLinear::~crExpressionOpSpecialLinear()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialLinear::Shutdown()
{
	delete m_Reference;
	m_Reference = NULL;

	m_Sources.Reset();

	crExpressionOp::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpSpecialLinear, crExpressionOp::kOpTypeSpecialLinear);

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialLinear::Process(ProcessHelper& ph) const
{
	static const float tolerance = 0.00001f;
	u32 numSources = m_Sources.GetCount();
	const Interval* interval = m_Intervals.GetElements();
	if(m_Mode == crExpressionOpSpecialBlend::kBlendModeVector)
	{
		Vec3V sum(V_ZERO);

		for(u32 i=0; i<numSources; ++i)
		{
			Vec3V input = GetSourceValue(ph, i);

			Vec3V result;
			for(u32 j=0; j < m_IntervalPerSource; j++)
			{
				VecBoolV greater = IsGreaterThan(input, interval->m_Begin);
				Vec3V scalar = AddScaled(interval->m_Add, interval->m_Mult, input);
				result = SelectFT(greater, result, scalar);
				interval++;
			}

			sum = Add(sum, result);
		}

		if(m_Reference)
		{
			m_Reference->Process(ph);
			Vec3V oldSum = ph.PopValue().GetVector();
			if(!IsCloseAll(oldSum, sum, ScalarVFromF32(tolerance)) && !IsEqualIntAll(oldSum, sum))
			{
				__debugbreak();
			}
		}
		ph.PushValue().Set(sum);
	}
	else if(m_Mode == crExpressionOpSpecialBlend::kBlendModeQuaternion)
	{
		QuatV sum(V_IDENTITY);

		for(u32 i=0; i<numSources; ++i)
		{
			Vec3V input = GetSourceValue(ph, i);

			Vec3V result;
			for(u32 j=0; j < m_IntervalPerSource; j++)
			{
				VecBoolV greater = IsGreaterThan(input, interval->m_Begin);
				Vec3V scalar = AddScaled(interval->m_Add, interval->m_Mult, input);
				result = SelectFT(greater, result, scalar);
				interval++;
			}

			QuatV q = QuatVFromEulersXYZ(result);
			sum = Multiply(sum, q);
		}

		if(m_Reference)
		{
			m_Reference->Process(ph);
			QuatV oldSum = ph.PopValue().GetQuaternion();
			if(!IsCloseAll(oldSum, sum, ScalarVFromF32(tolerance)) && !IsEqualIntAll(oldSum, sum))
			{
				__debugbreak();
			}
		}
		ph.PushValue().Set(sum);
	}
}

////////////////////////////////////////////////////////////////////////////////

Vec3V_Out crExpressionOpSpecialLinear::GetSourceValue(ProcessHelper& ph, u32 i) const
{
	const Source& src = m_Sources[i];
	switch(src.m_Type)
	{
	case kFormatTypeFloat:
		{
			float f = 0.f;
			ph.m_Frame->GetFloat(src.m_Track, src.m_Id, f);
			return Vec3VFromF32(f);
		}
	case kFormatTypeVector3:
		{
			Vec3V v(V_ZERO);
			ph.m_Frame->GetVector3(src.m_Track, src.m_Id, v);
			return Vec3VFromF32(v[src.m_Component]);
		}
	case kFormatTypeQuaternion:
		{
			QuatV q(V_IDENTITY);
			ph.m_Frame->GetQuaternion(src.m_Track, src.m_Id, q);
			return Vec3VFromF32(q[src.m_Component]);
		}
	default: return Vec3V(V_ZERO);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialLinear::Serialize(SerializeHelper& sh)
{
	crExpressionOp::Serialize(sh);

	sh  << m_IntervalPerSource << m_Mode << datNewLine;

	u32 numSources = m_Sources.GetCount();
	sh << numSources << datNewLine;
	if(sh.IsRead())
	{
		m_Sources.Resize(numSources);
	}

	for(u32 i=0; i<numSources; ++i)
	{
		Source& src = m_Sources[i];
		sh << src.m_Id << src.m_Track << src.m_Type << src.m_Component << datNewLine;
	}

	u32 numIntervals = m_Intervals.GetCount();
	sh << numIntervals << datNewLine;
	if(sh.IsRead())
	{
		m_Intervals.Resize(numIntervals);
	}

	for(u32 i=0; i<numIntervals; ++i)
	{
		Interval& interval = m_Intervals[i];
		sh << interval.m_Begin[0] << interval.m_Begin[1] << interval.m_Begin[2] << datNewLine;
		sh << interval.m_Mult[0] << interval.m_Mult[1] << interval.m_Mult[2] << datNewLine;
		sh << interval.m_Add[0] << interval.m_Add[1] << interval.m_Add[2] << datNewLine;
	}

	sh << m_Reference;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialLinear::Dump(DumpHelper& dh) const
{
	crExpressionOp::Dump(dh);

	u32 numSources = m_Sources.GetCount();

	dh.Outputf(2, "interval %u mode %u ", m_IntervalPerSource, m_Mode);

	const Interval* interval = m_Intervals.GetElements();
	for(u32 i=0; i<numSources; ++i)
	{
		const Source& src = m_Sources[i];
		dh.Outputf(2, "src[%d] track %u id %u type %u comp %u", i, src.m_Track, src.m_Id, src.m_Type, src.m_Component);
		for(u32 j=0; j < m_IntervalPerSource; j++)
		{
			if(!IsEqualAll(interval->m_Begin, Vec3V(V_FLT_MAX)))
			{
				if(!IsEqualAll(interval->m_Begin, Vec3V(V_NEG_FLT_MAX)))
				{
					Vec3V output = interval->m_Begin;
					output = SelectFT(IsEqualInt(output, Vec3V(V_FLT_MAX)), output, Vec3V(V_INF));
					output = SelectFT(IsEqualInt(output, Vec3V(V_NEG_FLT_MAX)), output, Vec3V(V_NEGINF));
					dh.Outputf(2, "\tbegin %f %f %f", output.GetXf(), output.GetYf(), output.GetZf());
				}
				if(!IsEqualAll(interval->m_Mult, Vec3V(V_ONE)))
				{
					dh.Outputf(2, "\tmult %f %f %f", interval->m_Mult.GetXf(), interval->m_Mult.GetYf(), interval->m_Mult.GetZf());
				}
				if(!IsEqualAll(interval->m_Add, Vec3V(V_ZERO)))
				{
					dh.Outputf(2, "\tadd %f %f %f", interval->m_Add.GetXf(), interval->m_Add.GetYf(), interval->m_Add.GetZf());
				}
			}
			interval++;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpSpecialLinear::Source::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crExpressionOpSpecialLinear::Source)
	SSTRUCT_FIELD(crExpressionOpSpecialLinear::Source, m_Component)
	SSTRUCT_FIELD(crExpressionOpSpecialLinear::Source, m_AccIdx)
	SSTRUCT_FIELD(crExpressionOpSpecialLinear::Source, m_Id)
	SSTRUCT_FIELD(crExpressionOpSpecialLinear::Source, m_Track)
	SSTRUCT_FIELD(crExpressionOpSpecialLinear::Source, m_Type)
	SSTRUCT_END(crExpressionOpSpecialLinear::Source)
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialLinear::Interval::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crExpressionOpSpecialLinear::Interval)
	SSTRUCT_FIELD(crExpressionOpSpecialLinear::Interval, m_Begin)
	SSTRUCT_FIELD(crExpressionOpSpecialLinear::Interval, m_Mult)
	SSTRUCT_FIELD(crExpressionOpSpecialLinear::Interval, m_Add)
	SSTRUCT_END(crExpressionOpSpecialLinear::Interval)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialCurve::crExpressionOpSpecialCurve()
: crExpressionOp(kOpTypeSpecialCurve)
{
}


////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialCurve::crExpressionOpSpecialCurve(const crExpressionOpSpecialCurve& other)
: crExpressionOp(other)
{
	const int numKeys = other.m_Keys.GetCount();
	m_Keys.Resize(numKeys);
	for(int i=0; i<numKeys; ++i)
	{
		m_Keys[i].m_In = other.m_Keys[i].m_In;
		m_Keys[i].m_Out = other.m_Keys[i].m_Out;
	}
	m_Source = other.m_Source ? other.m_Source->Clone() : NULL;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialCurve::crExpressionOpSpecialCurve(datResource& rsc)
: crExpressionOp(rsc)
, m_Source(rsc)
, m_Keys(rsc, true)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpSpecialCurve::DeclareStruct(datTypeStruct& s)
{
	crExpressionOp::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpSpecialCurve, crExpressionOp)
	SSTRUCT_FIELD(crExpressionOpSpecialCurve, m_Source)
	SSTRUCT_FIELD(crExpressionOpSpecialCurve, m_Keys)
	SSTRUCT_END(crExpressionOpSpecialCurve)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialCurve::~crExpressionOpSpecialCurve()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialCurve::Shutdown()
{
	m_Keys.Reset();

	if(m_Source)
	{
		delete m_Source;
		m_Source = NULL;
	}

	crExpressionOp::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpSpecialCurve, crExpressionOp::kOpTypeSpecialCurve);

////////////////////////////////////////////////////////////////////////////////

u32 crExpressionOpSpecialCurve::GetNumChildren() const
{
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

const crExpressionOp* crExpressionOpSpecialCurve::GetChild(u32 ASSERT_ONLY(idx)) const
{
	Assert(idx == 0);
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpressionOpSpecialCurve::GetChild(u32 ASSERT_ONLY(idx))
{
	Assert(idx == 0);
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialCurve::SetChild(u32 ASSERT_ONLY(idx), crExpressionOp& op)
{
	Assert(idx == 0);
	m_Source = &op;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialCurve::Process(ProcessHelper& ph) const
{
	m_Source->Process(ph);

	float in = ph.TopValue().GetFloat();
	float out = 0.f;

	const int numKeys = m_Keys.GetCount();
	if(numKeys == 2)
	{
		if(numKeys > 0)
		{
			if(in <= m_Keys[0].m_In)
			{
				out = m_Keys[0].m_Out;
			}
			else if(in >= m_Keys[1].m_In)
			{
				out = m_Keys[1].m_Out;
			}
			else
			{
				out = Lerp((in-m_Keys[0].m_In) / (m_Keys[1].m_In-m_Keys[0].m_In), m_Keys[0].m_Out, m_Keys[1].m_Out);
			}
		}
	}
	else if(numKeys > 0)
	{
		if(in <= m_Keys[0].m_In)
		{
			out = m_Keys[0].m_Out;
		}
		else if(in >= m_Keys[numKeys-1].m_In)
		{
			out = m_Keys[numKeys-1].m_Out;
		}
		else
		{
			int i=1;
			for(; i<numKeys; ++i)
			{
				if(in <= m_Keys[i].m_In)
				{
					out = Lerp((in-m_Keys[i-1].m_In) / (m_Keys[i].m_In-m_Keys[i-1].m_In), m_Keys[i-1].m_Out, m_Keys[i].m_Out);
					break;
				}
			}
			if(i==numKeys)
			{
				out = m_Keys[numKeys-1].m_Out;
			}
		}
	}

	ph.TopValue().Set(out);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialCurve::Serialize(SerializeHelper& sh)
{
	crExpressionOp::Serialize(sh);

	int numKeys = m_Keys.GetCount();
	sh << datLabel("NumKeys:") << numKeys << datNewLine;
	if(sh.IsRead())
	{
		m_Keys.Resize(numKeys);
	}

	for(int i=0; i<numKeys; ++i)
	{
		sh << datLabel("In:") << m_Keys[i].m_In << datNewLine;
		sh << datLabel("Out:") << m_Keys[i].m_Out << datNewLine;
	}

	sh << m_Source;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialCurve::Dump(DumpHelper& dh) const
{
	crExpressionOp::Dump(dh);

	const int numKeys = m_Keys.GetCount();

	dh.Outputf(2, "num keys %d", numKeys);

	for(int i=0; i<numKeys; ++i)
	{
		const Key& key = m_Keys[i];

		dh.Outputf(2, "key[%d] in %f out %f", i, key.m_In, key.m_Out);
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialCurve::Key::Key()
: m_In(0.f)
, m_Out(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialCurve::Key::Key(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpSpecialCurve::Key::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crExpressionOpSpecialCurve::Key)
	SSTRUCT_FIELD(crExpressionOpSpecialCurve::Key, m_In)
	SSTRUCT_FIELD(crExpressionOpSpecialCurve::Key, m_Out)
	SSTRUCT_END(crExpressionOpSpecialCurve::Key)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialLookAt::crExpressionOpSpecialLookAt()
: crExpressionOp(kOpTypeSpecialLookAt)
, m_LookAtPosSource(NULL)
, m_LookAtRotSource(NULL)
, m_OriginPosSource(NULL)
, m_Offset(V_IDENTITY)
, m_LookAtAxis(0)
, m_LookAtUpAxis(0)
, m_OriginUpAxis(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialLookAt::crExpressionOpSpecialLookAt(const crExpressionOpSpecialLookAt& other)
: crExpressionOp(other)
{
	m_Offset = other.m_Offset;
	m_LookAtPosSource = other.m_LookAtPosSource ? other.m_LookAtPosSource->Clone() : NULL;
	m_LookAtRotSource = other.m_LookAtRotSource ? other.m_LookAtRotSource->Clone() : NULL;
	m_OriginPosSource = other.m_OriginPosSource ? other.m_OriginPosSource->Clone() : NULL;
	m_LookAtAxis = other.m_LookAtAxis;
	m_LookAtUpAxis = other.m_LookAtUpAxis;
	m_OriginUpAxis = other.m_OriginUpAxis;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialLookAt::crExpressionOpSpecialLookAt(datResource& rsc)
: crExpressionOp(rsc)
, m_LookAtPosSource(rsc)
, m_LookAtRotSource(rsc)
, m_OriginPosSource(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpSpecialLookAt::DeclareStruct(datTypeStruct& s)
{
	crExpressionOp::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpSpecialLookAt, crExpressionOp)
	SSTRUCT_FIELD(crExpressionOpSpecialLookAt, m_LookAtPosSource)
	SSTRUCT_FIELD(crExpressionOpSpecialLookAt, m_LookAtRotSource)
	SSTRUCT_FIELD(crExpressionOpSpecialLookAt, m_Offset)
	SSTRUCT_FIELD(crExpressionOpSpecialLookAt, m_OriginPosSource)
	SSTRUCT_FIELD(crExpressionOpSpecialLookAt, m_LookAtAxis)
	SSTRUCT_FIELD(crExpressionOpSpecialLookAt, m_LookAtUpAxis)
	SSTRUCT_FIELD(crExpressionOpSpecialLookAt, m_OriginUpAxis)
	SSTRUCT_IGNORE(crExpressionOpSpecialLookAt, m_Padding)
	SSTRUCT_END(crExpressionOpSpecialLookAt)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialLookAt::~crExpressionOpSpecialLookAt()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialLookAt::Shutdown()
{
	if(m_LookAtPosSource)
	{
		delete m_LookAtPosSource;
		m_LookAtPosSource = NULL;
	}
	if(m_LookAtRotSource)
	{
		delete m_LookAtRotSource;
		m_LookAtRotSource = NULL;
	}
	if(m_OriginPosSource)
	{
		delete m_OriginPosSource;
		m_OriginPosSource = NULL;
	}

	crExpressionOp::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpSpecialLookAt, crExpressionOp::kOpTypeSpecialLookAt);

////////////////////////////////////////////////////////////////////////////////

u32 crExpressionOpSpecialLookAt::GetNumChildren() const
{
	return 3;
}

////////////////////////////////////////////////////////////////////////////////

const crExpressionOp* crExpressionOpSpecialLookAt::GetChild(u32 idx) const
{
	Assert(idx < 3);
	switch(idx)
	{
	case 0:
		return m_LookAtPosSource;
	case 1:
		return m_LookAtRotSource;
	case 2:
		return m_OriginPosSource;
	default:
		return NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpressionOpSpecialLookAt::GetChild(u32 idx)
{
	Assert(idx < 3);
	switch(idx)
	{
	case 0:
		return m_LookAtPosSource;
	case 1:
		return m_LookAtRotSource;
	case 2:
		return m_OriginPosSource;
	default:
		return NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialLookAt::SetChild(u32 idx, crExpressionOp& op)
{
	Assert(idx < 3);
	switch(idx)
	{
	case 0:
		m_LookAtPosSource = &op;
		break;
	case 1:
		m_LookAtRotSource = &op;
		break;
	case 2:
		m_OriginPosSource = &op;
		break;
	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialLookAt::Process(ProcessHelper& ph) const
{
	const Vec3V axes[] =
	{
		Vec3V(V_X_AXIS_WZERO),
		Vec3V(V_Y_AXIS_WZERO),
		Vec3V(V_Z_AXIS_WZERO),
		-Vec3V(V_X_AXIS_WZERO),
		-Vec3V(V_Y_AXIS_WZERO),
		-Vec3V(V_Z_AXIS_WZERO),
	};

	Value& val = ph.PushValue();
	val.Zero();

	m_LookAtPosSource->Process(ph);
	m_LookAtRotSource->Process(ph);
	m_OriginPosSource->Process(ph);

	Vec3V pos = ph.PopValue().GetVector();

	QuatV lookAtRot = ph.PopValue().GetQuaternion();
	
	Vec3V lookAtPos = ph.PopValue().GetVector();

	Vec3V dir = NormalizeSafe(lookAtPos - pos, Vec3V(V_Y_AXIS_WZERO));

	QuatV rot0 = QuatVFromVectors(axes[m_LookAtAxis], dir);

	Vec3V originUpDir = Transform(rot0, axes[m_OriginUpAxis]);

	Vec3V lookAtUpDir = Transform(lookAtRot, axes[m_LookAtUpAxis]);

	QuatV rotTwist = QuatVFromVectors(originUpDir, lookAtUpDir, dir);

	rot0 = Multiply(rotTwist, rot0);
	rot0 = Multiply(rot0, m_Offset);

	val.Set(rot0);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialLookAt::Serialize(SerializeHelper& sh)
{
	crExpressionOp::Serialize(sh);

	sh << m_Offset;
	sh << m_LookAtAxis;
	sh << m_LookAtUpAxis;
	sh << m_OriginUpAxis;
	sh << m_LookAtPosSource;
	sh << m_LookAtRotSource;
	sh << m_OriginPosSource;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpSpecialLookAt::Dump(DumpHelper& dh) const
{
	crExpressionOp::Dump(dh);

	static const char* axisNames[] = { "X", "Y", "Z", "-X", "-Y", "-Z" };
	dh.Outputf(2, "look at axis %s", axisNames[m_LookAtAxis]);
	dh.Outputf(2, "look at up axis %s", axisNames[m_LookAtUpAxis]);
	dh.Outputf(2, "origin up axis %s", axisNames[m_OriginUpAxis]);
	dh.Outputf(2, "offset %f %f %f %f", m_Offset.GetXf(), m_Offset.GetYf(), m_Offset.GetZf(), m_Offset.GetWf());
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpVariableGetSet::crExpressionOpVariableGetSet(eOpTypes type)
: crExpressionOp(type)
, m_Hash(0)
{
	SetIndex(0xffff);
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpVariableGetSet::crExpressionOpVariableGetSet(const crExpressionOpVariableGetSet& other)
: crExpressionOp(other)
{
	m_Hash = other.m_Hash;	
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpVariableGetSet::crExpressionOpVariableGetSet(datResource& rsc)
: crExpressionOp(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpVariableGetSet::DeclareStruct(datTypeStruct& s)
{
	crExpressionOp::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpVariableGetSet, crExpressionOp)
	SSTRUCT_FIELD(crExpressionOpVariableGetSet, m_Hash)
	SSTRUCT_END(crExpressionOpVariableGetSet)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crExpressionOpVariableGetSet::~crExpressionOpVariableGetSet()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpVariableGetSet::Shutdown()
{
	crExpressionOp::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpVariableGetSet::Serialize(SerializeHelper& sh)
{
	crExpressionOp::Serialize(sh);

	sh << m_Hash;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpVariableGetSet::Dump(DumpHelper& dh) const
{
	crExpressionOp::Dump(dh);

	dh.Outputf(2, "hash %d", GetHash());
	dh.Outputf(2, "index %d", GetIndex());
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpVariableGet::crExpressionOpVariableGet()
: crExpressionOpVariableGetSet(kOpTypeVariableGet)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpVariableGet::crExpressionOpVariableGet(const crExpressionOpVariableGet& other)
: crExpressionOpVariableGetSet(other)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpVariableGet::crExpressionOpVariableGet(datResource& rsc)
: crExpressionOpVariableGetSet(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpVariableGet::DeclareStruct(datTypeStruct& s)
{
	crExpressionOpVariableGetSet::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpVariableGet, crExpressionOpVariableGetSet)
	SSTRUCT_END(crExpressionOpVariableGet)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpVariableGet, crExpressionOp::kOpTypeVariableGet);

////////////////////////////////////////////////////////////////////////////////

u32 crExpressionOpVariableGet::GetNumChildren() const
{
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpVariableGet::Process(ProcessHelper& ph) const
{
	Value& val = ph.PushValue();
	val.Zero();

	if(GetIndex() < ph.m_NumVariables)
	{
		val = ph.m_Variables[GetIndex()];
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpVariableSet::crExpressionOpVariableSet()
: crExpressionOpVariableGetSet(crExpressionOp::kOpTypeVariableSet)
, m_Source(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpVariableSet::~crExpressionOpVariableSet()
{
	delete m_Source;
	m_Source = NULL;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpVariableSet::crExpressionOpVariableSet(const crExpressionOpVariableSet& other)
: crExpressionOpVariableGetSet(other)
{
	m_Source = other.m_Source ? other.m_Source->Clone() : NULL;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpVariableSet::crExpressionOpVariableSet(datResource& rsc)
: crExpressionOpVariableGetSet(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressionOpVariableSet::DeclareStruct(datTypeStruct& s)
{
	crExpressionOpVariableGetSet::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crExpressionOpVariableSet, crExpressionOpVariableGetSet)
	SSTRUCT_FIELD(crExpressionOpVariableSet, m_Source)
	SSTRUCT_END(crExpressionOpVariableSet)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_EXPRESSION_OP_TYPE(crExpressionOpVariableSet, crExpressionOp::kOpTypeVariableSet);

////////////////////////////////////////////////////////////////////////////////

u32 crExpressionOpVariableSet::GetNumChildren() const
{
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

const crExpressionOp* crExpressionOpVariableSet::GetChild(u32 ASSERT_ONLY(idx)) const
{
	Assert(idx == 0);
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpressionOpVariableSet::GetChild(u32 ASSERT_ONLY(idx))
{
	Assert(idx == 0);
	return m_Source;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpVariableSet::SetChild(u32 ASSERT_ONLY(idx), crExpressionOp& source)
{
	Assert(idx == 0);
	m_Source = &source;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpVariableSet::Process(ProcessHelper& ph) const
{
	if(GetIndex() < ph.m_NumVariables)
	{
		ph.m_Variables[GetIndex()] = ph.TopValue();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionOpVariableSet::Serialize(SerializeHelper& sh)
{
	crExpressionOpVariableGetSet::Serialize(sh);

	sh << m_Source;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
#endif // CR_DEV
