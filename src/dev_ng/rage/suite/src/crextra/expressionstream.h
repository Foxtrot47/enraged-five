//
// crextra/expressionstream.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef CREXTRA_EXPRESSIONSTREAM_H
#define CREXTRA_EXPRESSIONSTREAM_H

#include "cranimation/framedata.h"
#include "creature/componentphysical.h"

namespace rage
{

using namespace Vec;

////////////////////////////////////////////////////////////////////////////////

enum ExprOp
{
	OpHalt, OpPop, OpPush, OpZero, OpOne, OpConstantFloat, OpGet, OpGetComp, OpGetRelative, OpGetCompRelative, OpObjectGet, OpConstant,
	OpDeprecated0, OpDeprecated1, OpMotion, OpAbs, OpNegate, OpInvert, OpSqrt, OpLog, OpExp,
	OpCos, OpSin, OpTan, OpArcCos, OpArcSin, OpArcTan, OpQuatInvert, OpSquare, OpDegToRad, OpRadToDeg, OpClamp01, OpValid, OpFromEuler, OpToEuler,
	OpObjectConvertFrom, OpObjectConvertTo, OpCurve, OpSet, OpSetComp, OpSetRelative, OpSetCompRelative, OpObjectSet, OpBranch, OpBranchZero, OpBranchNotZero,
	OpAdd, OpSubtract, OpMultiply, OpMin, OpMax, OpQuatMultiply, OpQuatScale, OpGreaterThan, OpLessThan, OpGreaterThanEqual, OpLessThanEqual,
	OpClamp, OpLerp, OpMultiplyAdd, OpQuatLerp, OpToVec, OpLookAt, OpTime, OpTransform, OpXor, OpGetVariable, OpSetVariable, OpLinearVec, OpLinearQuat, OpDeltaTime, OpQuatIdentity,
	OpEqual, OpNotEqual, OpCosH, OpSinH, OpTanH, OpExponent
};

////////////////////////////////////////////////////////////////////////////////

struct ExprStreamHelper
{
	u8* m_Frame;
	const u16* m_FrameIndices;
	const u16* m_SkelIndices;
	const crFrameData::Dof* m_Dofs;
	const crBoneData* m_Bones;
	const crCreatureComponentPhysical::Motion* m_Motions;
	Vector_4V* m_Variables;
	u32 m_FrameSize;
	u32 m_NumIndices;
	u32 m_NumDofs;
	u32 m_NumBones;
	u32 m_NumMotions;
	u32 m_NumVariables;
	float m_Time;
	float m_DeltaTime;
};

////////////////////////////////////////////////////////////////////////////////

struct crExpressionStream
{
	void Process(const ExprStreamHelper& ph) const;
	u32 GetSize() const;

	u32 m_Hash;
	u32 m_AlignParamSize;
	u32 m_ParamSize;
	u16 m_NumOperations;
	u16 m_MaxStackDepth;
	u32 m_Data[];
};

////////////////////////////////////////////////////////////////////////////////

inline u32 crExpressionStream::GetSize() const
{
	return sizeof(crExpressionStream) + m_AlignParamSize + m_ParamSize + m_NumOperations;
}

////////////////////////////////////////////////////////////////////////////////

struct crExprOpBranch
{
	void Branch(const u8* RESTRICT &alignParams, const u8* RESTRICT &params, const u8* RESTRICT &ops) const;

	u32 m_AlignParamOffset;
	u32 m_ParamOffset;
	u32 m_OperationOffset;
};

////////////////////////////////////////////////////////////////////////////////

struct crExprOpFrame
{
	// gets
	Vector_4V_Out Get(const ExprStreamHelper& ph) const;
	Vector_4V_Out GetRelative(const ExprStreamHelper& ph) const;
	Vector_4V_Out GetComp(const ExprStreamHelper& ph) const;
	Vector_4V_Out GetCompRelative(const ExprStreamHelper& ph) const;
	Vector_4V_Out GetFloat(const ExprStreamHelper& ph) const;
	Vector_4V_Out GetObject(const ExprStreamHelper& ph) const;

	// sets
	void Set(const ExprStreamHelper& ph, Vector_4V_In v) const;
	void SetComp(const ExprStreamHelper& ph, Vector_4V_In v) const;
	void SetRelative(const ExprStreamHelper& ph, Vector_4V_In v) const;
	void SetCompRelative(const ExprStreamHelper& ph, Vector_4V_In v) const;
	void SetObject(const ExprStreamHelper& ph, Vector_4V_In v) const;

	Vector_4V_Out IsValid(const ExprStreamHelper& ph) const;
	Vector_4V_Out ConvertFrom(const ExprStreamHelper& ph, Vector_4V_In v) const;
	Vector_4V_Out ConvertTo(const ExprStreamHelper& ph, Vector_4V_In v) const;
	void CalcObjectSpaceMatrix(const ExprStreamHelper& ph, TransformV_InOut outTrans, bool inclusive) const;

	u16 m_AccIdx;
	u16 m_Id;
	u8 m_Track;
	u8 m_Type;
	u8 m_Component;
	u8 m_ForceDefault;
};

////////////////////////////////////////////////////////////////////////////////

struct crExprOpLookAt
{
	Vector_4V_Out Process(Vector_4V_In v0, Vector_4V_In v1, Vector_4V_In v2) const;

	QuatV m_Offset;
	u32 m_LookAtAxis;
	u32 m_LookAtUpAxis;
	u32 m_OriginUpAxis;
	u32 m_Padding;
};

////////////////////////////////////////////////////////////////////////////////

struct crExprOpCurve
{
	Vector_4V_Out Process(Vector_4V_In v) const;

	struct Key
	{
		f32 m_In;
		f32 m_Out;
	};

	u32 m_Size;
	u32 m_NumKeys;
	Key m_Keys[];
};

////////////////////////////////////////////////////////////////////////////////

struct crExprOpMotion
{
	void Process(const ExprStreamHelper& ph) const;

	crMotionDescription m_Descr;
	u32 m_AccRotIdx;
	u32 m_AccTransIdx;
};

////////////////////////////////////////////////////////////////////////////////

struct crExprOpLinear
{
	Vector_4V_Out ProcessVectors(const ExprStreamHelper& ph) const;
	Vector_4V_Out ProcessQuaternions(const ExprStreamHelper& ph) const;

	u32 m_Size;
	u32 m_NumSources;
	u32 m_IntervalPerSource;
	u32 m_Padding;
	u16 m_Data[];
};

////////////////////////////////////////////////////////////////////////////////

struct crExprOpVariable
{
	Vector_4V_Out Get(const ExprStreamHelper& ph) const;
	void Set(const ExprStreamHelper& ph, Vector_4V_In v) const;

	u32 m_Hash;
	u32 m_Idx;
};

////////////////////////////////////////////////////////////////////////////////


};  // namespace rage

#endif // CREXTRA_EXPRESSIONSTREAM_H
