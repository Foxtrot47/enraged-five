//
// crextra/expressions.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "expressions.h"

#include "expression.h"
#include "expressionops.h"
#include "expressionpacker.h"
#include "expressionstream.h"

#include "cranimation/framedatafactory.h"
#include "creature/component.h"
#include "creature/componentextradofs.h"
#include "creature/creature.h"
#include "creature/creatureiterator.h"
#include "crskeleton/bonedata.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "file/asset.h"
#include "file/serialize.h"
#include "file/stream.h"
#include "profile/element.h"
#include "system/miniheap.h"
#include "zlib/zlib.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

#if CR_STATS
	EXT_PF_VALUE_FLOAT(crFrameAccel_ExprFrameIdx);
	EXT_PF_VALUE_FLOAT(crFrameAccel_ExprSkelIdx);
#endif // CR_STATS

////////////////////////////////////////////////////////////////////////////////

crExpressions::crExpressions()
: m_RefCount(1)
, m_Signature(0)
, m_MaxPackedSize(0)
, m_Flags(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressions::crExpressions(datResource& rsc)
: m_ExpressionStreams(rsc, true)
#if CR_DEV
, m_Expressions(rsc, true)
#endif
, m_Tracks(rsc, true)
, m_Motions(rsc, true)
, m_Variables(rsc, true)
, m_Name(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpressions::~crExpressions()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressions::Shutdown()
{
#if CR_DEV
	const int numExpressions = m_Expressions.GetCount();
	for(int i=0; i<numExpressions; ++i)
	{
#if CR_TEST_EXPRESSIONS
		if(IsPacked())
		{
			delete [] reinterpret_cast<u8*>((crExpression*)m_Expressions[i]);
			m_Expressions[i] = NULL;
		}
		else
		{
			delete m_Expressions[i];
		}
#else
		delete m_Expressions[i];
#endif
	}
	m_Expressions.Reset();
#endif // CR_DEV

	const int numExpressionStreams = m_ExpressionStreams.GetCount();
	for(int i=0; i<numExpressionStreams; ++i)
	{
		delete m_ExpressionStreams[i];
	}
	m_ExpressionStreams.Reset();

	m_MaxPackedSize = 0;

	m_Name.Reset();
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressions::DeclareStruct(datTypeStruct& s)
{
	pgBase::DeclareStruct(s);
	STRUCT_BEGIN(crExpressions);
	STRUCT_FIELD(m_Expressions);
	STRUCT_FIELD(m_ExpressionStreams);
	STRUCT_FIELD(m_Tracks);
	STRUCT_FIELD(m_Motions);
	STRUCT_FIELD(m_Variables);
	STRUCT_FIELD(m_Name);
	STRUCT_FIELD(m_RefCount);
	STRUCT_FIELD(m_Signature);
	STRUCT_FIELD(m_MaxPackedSize);
	STRUCT_FIELD(m_Flags);
	STRUCT_IGNORE(m_Padding);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crExpressions);

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Used to initialize additional DOFs in creatures based on input DOFs in expressions
struct ExpressionCreatureInitializer
{
	// PURPOSE: Constructor
	// PARAMS: creature - creature to initialize
	ExpressionCreatureInitializer(crCreature& creature, crFrameDataFactory* factory, crCommonPool* framePool);

	// PURPOSE: Destructor
	~ExpressionCreatureInitializer();

	// PURPOSE: Visit expression operation override
	void Initialize(const crExpressions& expressions);

	// PURPOSE: Return true if this is a valid extra dof
	bool IsExtraDof(u8 track, u16 id, u8 type);


	crCreature* m_Creature;
	crFrameDataFactory* m_Factory;
	crCommonPool* m_FramePool;
};

////////////////////////////////////////////////////////////////////////////////

ExpressionCreatureInitializer::ExpressionCreatureInitializer(crCreature& creature, crFrameDataFactory* factory, crCommonPool* framePool)
: m_Creature(&creature)
, m_Factory(factory)
, m_FramePool(framePool)
{
}

////////////////////////////////////////////////////////////////////////////////

ExpressionCreatureInitializer::~ExpressionCreatureInitializer()
{
}

////////////////////////////////////////////////////////////////////////////////

void ExpressionCreatureInitializer::Initialize(const crExpressions& expressions)
{
	// create a temporary frame data
	crCreatureComponentExtraDofs* componentExtraDofs = static_cast<crCreatureComponentExtraDofs*>(m_Creature->FindComponent<crCreatureComponentExtraDofs>());
	u32 numTracks = expressions.m_Tracks.GetCount();
	u32 numDofs = componentExtraDofs ? componentExtraDofs->GetFrameData().GetNumDofs() : 0;
	u32 bufferSize = crFrameData::CalcDofsBufferSize(numTracks + numDofs);

	crFrameData frameData;
	u8* buffer = Alloca(u8, bufferSize);
	frameData.Init(buffer, bufferSize, false);

	for(u32 i=0; i < numTracks; i++)
	{
		const crExpressions::Track& track = expressions.m_Tracks[i];
		u8 type = track.m_Flags & ~crExpressions::Track::kIsInput;
		if(IsExtraDof(track.m_Track, track.m_Id, type))
		{
			frameData.FastAddDof(track.m_Track, track.m_Id, type);
		}
	}

	// refresh only if we have extra dofs
	if(frameData.GetNumDofs() > 0)
	{
		if(componentExtraDofs)
		{
			const crFrameData& frameExtraDofs = componentExtraDofs->GetFrameData();
			for(u32 i=0; i < numDofs; i++)
			{
				const crFrameData::Dof& dof = frameExtraDofs.GetDof(i);
				frameData.FastAddDof(dof.m_Track, dof.m_Id, dof.m_Type);
			}
		}
		frameData.SortDofs(true);

		// refresh only if we have more total dofs
		if(frameData.GetNumDofs() > numDofs)
		{
			frameData.CalcSignature();
			frameData.CalcOffsets();

			// create persistent frame data
			crFrameData* duplicatedFrame;
			if(m_Factory)
			{
				duplicatedFrame = m_Factory->DuplicateFrameData(frameData);
			}
			else
			{
				duplicatedFrame = rage_new crFrameData(frameData);
			}

			// add to component extra dofs
			if(!componentExtraDofs)
			{
				componentExtraDofs = static_cast<crCreatureComponentExtraDofs*>(m_Creature->AllocateComponent(crCreatureComponent::kCreatureComponentTypeExtraDofs));
				componentExtraDofs->Init(*m_Creature, *duplicatedFrame, m_FramePool);
				m_Creature->AddComponent(*componentExtraDofs);
			}
			else
			{
				componentExtraDofs->ExchangeFrameData(*duplicatedFrame);
			}

			duplicatedFrame->Release();
		}
	}

	// add physical expression motions
	u32 numMotions = expressions.m_Motions.GetCount();
	if(numMotions)
	{
		crCreatureComponentPhysical* componentPhysical = m_Creature->FindComponent<crCreatureComponentPhysical>();
		if(!componentPhysical)
		{
			componentPhysical = static_cast<crCreatureComponentPhysical*>(m_Creature->AllocateComponent(crCreatureComponent::kCreatureComponentTypePhysical));
			componentPhysical->Init(*m_Creature);
			m_Creature->AddComponent(*componentPhysical);
		}

		componentPhysical->AddMotions(expressions.m_Motions);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool ExpressionCreatureInitializer::IsExtraDof(u8 track, u16 id, u8 UNUSED_PARAM(type))
{
	switch(track)
	{
	case kTrackBoneTranslation:
	case kTrackBoneRotation:
	case kTrackBoneScale:
		{
			const crSkeletonData& skelData = m_Creature->GetSkeleton()->GetSkeletonData();
			int idx;
			if(skelData.ConvertBoneIdToIndex(id, idx))
			{
				const crBoneData* bd = skelData.GetBoneData(idx);
				if(bd)
				{
					switch(track)
					{
					case kTrackBoneTranslation:
						if(bd->HasDofs(crBoneData::TRANSLATION))
						{
							return false;
						}
						break;

					case kTrackBoneRotation:
						if(bd->HasDofs(crBoneData::ROTATION))
						{
							return false;
						}
						break;

					case kTrackBoneScale:
						if(bd->HasDofs(crBoneData::SCALE))
						{
							return false;
						}
						break;

					default:
						break;

					}
				}
				return true;
			}
		}
		break;

	default:
		{
			return !m_Creature->HasDof(track, id);
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressions::InitializeCreature(crCreature& creature, crFrameDataFactory* factory, crCommonPool* framePool) const
{
	ExpressionCreatureInitializer initializer(creature, factory, framePool);
	initializer.Initialize(*this);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressions::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;
#if CR_DEV
		crExpressionOp::InitClass();
#endif
		crFrameFilter::InitClass();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressions::ShutdownClass()
{
#if CR_DEV
	crExpressionOp::ShutdownClass();
#endif
	sm_InitClassCalled = false;
}

////////////////////////////////////////////////////////////////////////////////

struct CalcExpressionFrameIndicesCallbackData
{
	const crExpressions* m_Expressions;
	const crFrameData* m_FrameData;
};

////////////////////////////////////////////////////////////////////////////////

static u32 CalcExpressionFrameIndicesCallback(void* calcData, u8* buffer)
{
	CalcExpressionFrameIndicesCallbackData& cbData = *reinterpret_cast<CalcExpressionFrameIndicesCallbackData*>(calcData);
	return crExpressions::CalcExpressionFrameIndices(*cbData.m_Expressions, *cbData.m_FrameData, reinterpret_cast<u16*>(buffer));
}

////////////////////////////////////////////////////////////////////////////////

void crExpressions::FindExpressionFrameIndices(crLock& outLock, crFrameAccelerator& accelerator, const crFrameData& frameData) const
{
	u64 sigFrame = frameData.GetSignature();
	u64 sigExpressions = GetSignature();
	Assert(sigExpressions && sigFrame);

	u64 sig = (sigFrame << 32) | sigExpressions;
	CalcExpressionFrameIndicesCallbackData data;
	data.m_Expressions = this;
	data.m_FrameData = &frameData;

	accelerator.m_Accelerators[crFrameAccelerator::kExpressionFrameIndices].GetEntry(sig, m_Tracks.GetCount()*sizeof(u16), CalcExpressionFrameIndicesCallback, &data, outLock);

#if CR_STATS
	PF_SET(crFrameAccel_ExprFrameIdx, 100.f * float(accelerator.m_Accelerators[crFrameAccelerator::kExpressionFrameIndices].CalcLockedHeapSize()) / float(accelerator.m_Accelerators[crFrameAccelerator::kExpressionFrameIndices].GetTotalHeapSize()));
#endif // CR_STATS
}

////////////////////////////////////////////////////////////////////////////////

u32 crExpressions::CalcExpressionFrameIndices(const crExpressions& expressions, const crFrameData& frameData, u16* indices)
{
	u32 numTracks = expressions.m_Tracks.GetCount();
	u32 readOnlyOffset = frameData.GetReadOnlyOffset();
	u32 writeOnlyOffset = frameData.GetWriteOnlyOffset();

	for(u32 i=0; i < numTracks; i++)
	{
		const Track& track = expressions.m_Tracks[i];

		u32 offset = (track.m_Flags & crExpressions::Track::kIsInput) ? readOnlyOffset : writeOnlyOffset;
		u8 type = track.m_Flags & ~crExpressions::Track::kIsInput;

		const crFrameData::Dof* dof = frameData.FindDof(track.m_Track, track.m_Id);
		if(dof && type == dof->m_Type)
		{
			offset = dof->m_Offset;
		}
		indices[i] = u16(offset);
	}

	return numTracks*sizeof(u16);
}

////////////////////////////////////////////////////////////////////////////////

struct CalcExpressionSkelIndicesCallbackData
{
	const crExpressions* m_Expressions;
	const crSkeletonData* m_SkelData;
};

////////////////////////////////////////////////////////////////////////////////

static u32 CalcExpressionSkelIndicesCallback(void* calcData, u8* buffer)
{
	CalcExpressionSkelIndicesCallbackData& cbData = *reinterpret_cast<CalcExpressionSkelIndicesCallbackData*>(calcData);
	return crExpressions::CalcExpressionSkelIndices(*cbData.m_Expressions, *cbData.m_SkelData, reinterpret_cast<u16*>(buffer));
}

////////////////////////////////////////////////////////////////////////////////

void crExpressions::FindExpressionSkelIndices(crLock& outLock, crFrameAccelerator& accelerator, const crSkeletonData& skelData) const
{
	u64 sigSkelData = skelData.GetSignature();
	u64 sigExpressions = GetSignature();
	Assert(sigExpressions && sigSkelData);

	u64 sig = (sigSkelData << 32) | sigExpressions;
	CalcExpressionSkelIndicesCallbackData data;
	data.m_Expressions = this;
	data.m_SkelData = &skelData;

	accelerator.m_Accelerators[crFrameAccelerator::kExpressionSkelIndices].GetEntry(sig, m_Tracks.GetCount()*sizeof(u16), CalcExpressionSkelIndicesCallback, &data, outLock);

#if CR_STATS
	PF_SET(crFrameAccel_ExprSkelIdx, 100.f * float(accelerator.m_Accelerators[crFrameAccelerator::kExpressionSkelIndices].CalcLockedHeapSize()) / float(accelerator.m_Accelerators[crFrameAccelerator::kExpressionSkelIndices].GetTotalHeapSize()));
#endif // CR_STATS
}

////////////////////////////////////////////////////////////////////////////////

u32 crExpressions::CalcExpressionSkelIndices(const crExpressions& expressions, const crSkeletonData& skelData, u16* indices)
{
	u32 numTracks = expressions.m_Tracks.GetCount();
	for(u32 i=0; i < numTracks; i++)
	{
		u32 boneIdx = USHRT_MAX;

		const Track& t = expressions.m_Tracks[i];
		if(t.m_Track <= kTrackBoneConstraint)
		{
			int idx;
			if(skelData.ConvertBoneIdToIndex(t.m_Id, idx))
			{
				boneIdx = u32(idx);
			}
		}

		indices[i] = u16(boneIdx);
	}

	return numTracks*sizeof(u16);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
// PURPOSE: Used to populate additional degrees of freedom in frame data
// based on input/output degrees of freedom in expressions
template<bool inputs, bool outputs>
class ExpressionFrameDataInitializer : public crFrameDataInitializer
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS:
	// expressions - expressions to query for DOFs
	// destroyExistingDofs - destroy any existing DOFs contained in frame (default true)
	// filter - pointer to frame filter to use when adding new DOFs (default null, all DOFs added)
	ExpressionFrameDataInitializer(const crExpressions& expressions, bool destroyExistingDofs=true, crFrameFilter* filter=NULL)
		: crFrameDataInitializer(destroyExistingDofs, filter)
		, m_Expressions(&expressions)
	{
	}

	// PURPOSE: Destructor
	virtual ~ExpressionFrameDataInitializer()
	{
	}

protected:

	// PURPOSE: Pre-initialize iterator
	template <bool _inputs, bool _outputs>
	struct ExpressionFrameDataPreInititalizeIterator : public crExpressionOp::InputOutputIterator<_inputs, _outputs>
	{
		// PURPOSE: Constructor
		ExpressionFrameDataPreInititalizeIterator()
			: m_NumDofs(0)
		{
		}

		// PURPOSE: Destructor
		virtual ~ExpressionFrameDataPreInititalizeIterator()
		{
		}

		// PURPOSE: Input DOF callback override
		virtual bool Callback(u8, u16, u8, bool)
		{
			// TODO - wasteful, no duplicate tracking
			m_NumDofs++;	
			return false;
		}

		u32 m_NumDofs;
	};

	// PURPOSE: Implementation of preinitializing pass
	virtual u32 PreInitialize() const
	{
		ExpressionFrameDataPreInititalizeIterator<inputs, outputs> preInitInitializer;
		const int numExpressions = m_Expressions->GetNumExpressions();
		for(int i=0; i<numExpressions; ++i)
		{	
			const crExpressionOp* op = m_Expressions->GetExpression(i)->GetExpressionOp();
			if(op)
			{
				preInitInitializer.Visit(*op);
			}
		}
		return preInitInitializer.m_NumDofs;
	}


	// PURPOSE: Initialize iterator
	template <bool _inputs, bool _outputs>
	struct ExpressionFrameDataInititalizeIterator : public crExpressionOp::InputOutputIterator<_inputs, _outputs>
	{
		// PURPOSE: Constructor
		ExpressionFrameDataInititalizeIterator(const ExpressionFrameDataInitializer<_inputs, _outputs>& initializer)
			: m_Initializer(&initializer)
		{
		}

		// PURPOSE: Destructor
		virtual ~ExpressionFrameDataInititalizeIterator()
		{
		}

		// PURPOSE: Input DOF callback override
		virtual bool Callback(u8 track, u16 id, u8 type, bool)
		{
			// TODO - wasteful, no duplicate tracking
			m_Initializer->FastAddDof(track, id, type);
			return false;
		}

		const ExpressionFrameDataInitializer<_inputs, _outputs>* m_Initializer;
	};

	// PURPOSE: Implementation of initializing pass
	virtual bool Initialize() const
	{
		ExpressionFrameDataInititalizeIterator<inputs, outputs> initInitializer(*this);
		const int numExpressions = m_Expressions->GetNumExpressions();
		for(int i=0; i<numExpressions; ++i)
		{	
			const crExpressionOp* op = m_Expressions->GetExpression(i)->GetExpressionOp();
			if(op)
			{
				initInitializer.Visit(*op);
			}
		}
		return false;
	}

private:

	const crExpressions* m_Expressions;
};

////////////////////////////////////////////////////////////////////////////////

void crExpressions::InitializeFrameData(crFrameData& frameData, bool inputs, bool outputs, bool destroyExistingDofs) const
{
	if(inputs && outputs)
	{
		ExpressionFrameDataInitializer<true, true> initializer(*this, destroyExistingDofs);
		initializer.InitializeFrameData(frameData);
	}
	else if(inputs)
	{
		ExpressionFrameDataInitializer<true, false> initializer(*this, destroyExistingDofs);
		initializer.InitializeFrameData(frameData);
	}
	else if(outputs)
	{
		ExpressionFrameDataInitializer<false, true> initializer(*this, destroyExistingDofs);
		initializer.InitializeFrameData(frameData);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressions::Process(crFrame& inoutFrame, float time, float deltaTime, crCreature* creature, const crSkeletonData* skelData, crFrameFilter* filter, crExpressionOp::Value* variables, u32 numVariables) const
{
	const u32 numExpressions = m_Expressions.GetCount();
	for(u32 i=0; i<numExpressions; i++)
	{
		m_Expressions[i]->Process(inoutFrame, time, deltaTime, creature, skelData, filter, variables, numVariables);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressions::Dump(int verbosity, char* buf, u32 bufSize) const
{
	const int numExpressions = m_Expressions.GetCount();
	for(int i=0; i<numExpressions; ++i)
	{
		const crExpression& expression = *m_Expressions[i];

		char header[255];
		formatf(header, "expression[%d] optimized %d depth %d\n", i, (m_Flags & kOptimized)!=0, expression.GetStackDepth());

		if(buf)
		{
			if(strlen(header) > (bufSize-strlen(buf)-1))
			{
				header[bufSize-strlen(buf)-1] = '\0';
			}

			safecat(buf, header, bufSize);
			expression.Dump(verbosity, buf+strlen(buf), u32((bufSize-strlen(buf))));
		}
		else
		{
			Printf("%s", header);
			expression.Dump(verbosity);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionsDictionary* crExpressions::AllocateAndLoadDictionary(const char* listFileName)
{
	fiSafeStream f(ASSET.Open(listFileName, "exprlist"));
	fiTokenizer T(listFileName, f);	

	sysMemStartTemp();
	atArray<atString> exprFileNames;

	const int maxBufSize = RAGE_MAX_PATH;
	char buf[maxBufSize];
	while(T.GetLine(buf, maxBufSize) > 0)
	{
		exprFileNames.Grow() = buf;
	}
	sysMemEndTemp();

	crExpressionsDictionary* dictionary = AllocateAndLoadDictionary(exprFileNames);

	sysMemStartTemp();
	exprFileNames.Reset();
	sysMemEndTemp();

	return dictionary;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionsDictionary* crExpressions::AllocateAndLoadDictionary(const atArray<atString>& exprFilenames)
{
	const int numExpressions = exprFilenames.GetCount();

	crExpressionsDictionary* dictionary = rage_new crExpressionsDictionary(numExpressions);

	for(int i=0; i<numExpressions; ++i)
	{
		crExpressions* expressions = crExpressions::AllocateAndLoad(exprFilenames[i], true);
		if(expressions)
		{
			expressions->AddRef();
			if (!(dictionary->AddEntry(exprFilenames[i], expressions)))
			{
				Errorf("Could not add expression '%s' - it's probably being added twice", exprFilenames[i].c_str());
			}
		}
		else
		{
			Errorf("crExpressions::AllocateAndLoadDictionary - failed to load '%s'", (const char*)exprFilenames[i]);
			return NULL;
		}
	}

	return dictionary;
}

////////////////////////////////////////////////////////////////////////////////

__THREAD int s_ExpressionVersion = 0;

const u8 crExpressions::sm_SerializationVersions[] =
{
	16, // 10-OCT-12 - added SpecialLinear op
	15, // 21-SEP-12 - added expression local variables
	14, // 02-AUG-12 - add flags
	13, // 10-JUL-12 - add direction to physical expressions
	12, // 08-MAR-12 - reduce size of physical expressions
	11, // 01-MAR-11 - add gravity to physical expressions
	10, // 01-JAN-11 - replace special blend vector and remove the other composite operations
	9,	// 06-DEC-10 - changed SpecialBlendVector op - not backward compatible, added SpecialBlendQuaternion and SpecialList
	8,  // 17-SEP-10 - serialize relative flag on get/set ops
	7,  // 04-MAY-10 - optimized SpecialBlendVector op
};

////////////////////////////////////////////////////////////////////////////////

crExpressions* crExpressions::AllocateAndLoad(const char* fileName, bool pack)
{
	crExpressions* expressions = rage_aligned_new(16) crExpressions;
	if(expressions->Load(fileName, pack))
	{
		return expressions;
	}
	else
	{
		delete expressions;
		return NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crExpressions::Load(const char* fileName, bool pack)
{
	fiStream* f = ASSET.Open(fileName, "expr", false, true);
	if(!f)
	{
		Errorf("crExpressions - failed to open file '%s' for reading", fileName);
		return false;
	}

	m_Name = fileName;

	// if packing, allocate to temporary memory
	if(pack)
	{
		sysMemStartTemp();
	}

	int version;
	int magic = 0;
	f->ReadInt(&magic, 1);

	if(magic != 'RPXE')
	{
		Errorf("crExpressions - file is not an expresion file");
		f->Close();
		return false;
	}

	fiSerialize s(f, true, false);
	s << datLabel("ExpressionVersion:") << version << datNewLine;
	s_ExpressionVersion = version;

	const int numExpressionVersions = NELEM(sm_SerializationVersions);
	int i;
	for(i=0; i<numExpressionVersions; ++i)
	{
		if(version == sm_SerializationVersions[i])
		{
			break;
		}
	}
	if(i == numExpressionVersions)
	{
		Errorf("crExpressions - attempting to load unsupported expression version '%d' (latest version '%d' supported)", version, sm_SerializationVersions[0]);
		return false;
	}

	if(fiSerializeFrom(f, *this))
	{
		f->Close();

		if(pack)
		{
			sysMemEndTemp();
		}

		CalcInputOutputDofs();
		CalcVariables();

		if(pack)
		{
			Pack();

#if CR_TEST_EXPRESSIONS
			// reallocate expression pointer array to resource memory
			const int numExpressions = m_Expressions.GetCount();
			crExpression** expressions = Alloca(crExpression*, numExpressions);

			for(int j=0; j<numExpressions; ++j)
			{
				expressions[j] = m_Expressions[j];
			}

			sysMemStartTemp();
			m_Expressions.Reset();
			sysMemEndTemp();

			m_Expressions.Resize(numExpressions);

			for(int j=0; j<numExpressions; ++j)
			{
				m_Expressions[j] = expressions[j];
			}
#else
			sysMemStartTemp();
			for(int j=0; j < m_Expressions.GetCount(); ++j)
			{
				delete m_Expressions[j];
			}
			m_Expressions.Reset();
			sysMemEndTemp();
#endif
		}

		return true;
	}

	f->Close();

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crExpressions::Save(const char* fileName) const
{
	if(!fileName)
	{
		fileName = m_Name.c_str();
	}

	fiStream* f = ASSET.Create(fileName, "expr");
	if(!f)
	{
		Errorf("crExpressions - failed to open file '%s' for writing", fileName);
		return false;
	}

	const bool binary = true;
	int latestVersion = sm_SerializationVersions[0];

	int magic = 'RPXE';
	f->WriteInt(&magic, 1);

	fiSerialize s(f, false, binary);

	s << datLabel("ExpressionVersion:") << latestVersion << datNewLine;
	s_ExpressionVersion = latestVersion;

	bool success = !s.HasError() && fiSerializeTo(f, *const_cast<crExpressions*>(this), binary);

	if(!success)
	{
		Errorf("crExpressions - failed to save file '%s'", fileName);
	}

	f->Close();

	return success;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressions::Serialize(datSerialize& s)
{
	if(s_ExpressionVersion >= 14)
	{
		u16 flags = m_Flags & ~kNonSerializableMask;
		s << flags;
		if(s.IsRead())
		{
			m_Flags = flags;
		}
	}

	int numExpressions = m_Expressions.GetCount();
	s << datLabel("NumExpressions:") << numExpressions << datNewLine;

	if(s.IsRead())
	{
		m_Expressions.Resize(numExpressions);
	}

	for(int i=0; i<numExpressions; ++i)
	{
		if(s.IsRead())
		{
			m_Expressions[i] = rage_new crExpression;
		}
		s << *(m_Expressions[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressions::Pack(u32* outSizes)
{
	if(!IsPacked())
	{
		u32 numExpressions = GetNumExpressions();
		m_ExpressionStreams.Resize(numExpressions);
		m_MaxPackedSize = crExpressionPacker::Pack(*this, reinterpret_cast<crExpressionStream**>(m_ExpressionStreams.GetElements()), outSizes);
		m_Flags |= kPacked;
#if CR_TEST_EXPRESSIONS
		sysMemStartTemp();
		const size_t tempBufferSize = 1024 * 1024;
		char* tempBuffer = rage_new char[tempBufferSize];
		sysMemEndTemp();

		m_MaxPackedSize = 0;

		for(u32 i=0; i<numExpressions; ++i)
		{
			sysMemStartMiniHeap(tempBuffer, tempBufferSize);
			m_Expressions[i]->Clone();
			size_t tempBufferUsed = tempBufferSize - sysMemEndMiniHeap();

			size_t packedBufferSize = RAGE_ALIGN(tempBufferUsed, 4);
			char* packedBuffer = rage_new char[packedBufferSize];

			sysMemStartMiniHeap(packedBuffer, packedBufferSize);
			crExpression* packedExpression = m_Expressions[i]->Clone();
			sysMemEndMiniHeap();

			sysMemStartTemp();
			delete m_Expressions[i];
			sysMemEndTemp();

			m_Expressions[i] = packedExpression;

			m_MaxPackedSize = Max(m_MaxPackedSize, u32(packedBufferSize));
		}

		sysMemStartTemp();
		delete [] tempBuffer;
		sysMemEndTemp();
#endif // CR_TEST_EXPRESSIONS
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressions::InsertExpression(const crExpression& expression, int idx)
{
	Assert(!IsPacked());

	m_Expressions.Grow();
	m_Expressions.Pop();

	m_Expressions.Insert(idx) = rage_new crExpression(expression);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressions::AppendExpression(const crExpression& expression)
{
	InsertExpression(expression, GetNumExpressions());
}

////////////////////////////////////////////////////////////////////////////////

void crExpressions::RemoveExpression(int idx)
{
	if(IsPacked())
	{
		delete [] reinterpret_cast<u8*>((crExpression*)m_Expressions[idx]);
	}
	else
	{
		delete m_Expressions[idx];
	}
	m_Expressions.Delete(idx);

	if(GetNumExpressions() == 0)
	{
		m_MaxPackedSize = 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

struct CalcInputOutputDofsIterator : public crExpressionOp::ModifyingIterator
{
	CalcInputOutputDofsIterator()
		: m_Signature(0)
	{
	}

	virtual ~CalcInputOutputDofsIterator()
	{
	}

	void Shutdown()
	{
		m_Tracks.Reset();
		m_Motions.Reset();
	}

	virtual crExpressionOp* Visit(crExpressionOp& op)
	{
		int accIdx = -1;
		switch(op.GetOpType())
		{
		case crExpressionOp::kOpTypeGet:
		case crExpressionOp::kOpTypeValid:
		case crExpressionOp::kOpTypeComponentGet:
		case crExpressionOp::kOpTypeObjectSpaceGet:
		case crExpressionOp::kOpTypeObjectSpaceConvertTo:
		case crExpressionOp::kOpTypeObjectSpaceConvertFrom:
			{
				crExpressionOpGetSet& opGetSet = static_cast<crExpressionOpGetSet&>(op);
				AddInputOutputDof(true, opGetSet.GetTrack(), opGetSet.GetId(), opGetSet.GetType(), accIdx);
				opGetSet.SetAcceleratedIndex(accIdx);
			}
			break;

		case crExpressionOp::kOpTypeSet:
		case crExpressionOp::kOpTypeComponentSet:
		case crExpressionOp::kOpTypeObjectSpaceSet:
			{
				crExpressionOpGetSet& opGetSet = static_cast<crExpressionOpGetSet&>(op);
				AddInputOutputDof(false, opGetSet.GetTrack(), opGetSet.GetId(), opGetSet.GetType(), accIdx);
				opGetSet.SetAcceleratedIndex(accIdx);
			}
			break;

		case crExpressionOp::kOpTypeSpecialLinear:
			{
				crExpressionOpSpecialLinear& opLinear = static_cast<crExpressionOpSpecialLinear&>(op);
				for(int i=0; i < opLinear.m_Sources.GetCount(); ++i)
				{
					crExpressionOpSpecialLinear::Source& src = opLinear.m_Sources[i];
					AddInputOutputDof(true, src.m_Track, src.m_Id, src.m_Type, src.m_AccIdx);
				}
			}
			break;

		case crExpressionOp::kOpTypeMotion:
			{
				crExpressionOpMotion& opMotion = static_cast<crExpressionOpMotion&>(op);
				AddInputOutputDof(false, kTrackBoneRotation, opMotion.m_Descr.GetBoneId(), kFormatTypeQuaternion, opMotion.m_AccRotIdx);
				AddInputOutputDof(false, kTrackBoneTranslation, opMotion.m_Descr.GetBoneId(), kFormatTypeVector3, opMotion.m_AccTransIdx);
				m_Motions.Grow() = opMotion.m_Descr;
			}
			break;

		default:
			break;
		}

		return crExpressionOp::ModifyingIterator::Visit(op);
	}

	void AddInputOutputDof(bool isInput, u8 track, u16 id, u8 type, int& outAccIdx)
	{
		u32 numTracks = m_Tracks.GetCount();
		u32 i=0;
		for(; i<numTracks; ++i)
		{
			bool trackInput = (m_Tracks[i].m_Flags & crExpressions::Track::kIsInput) != 0;
			if(trackInput == isInput && m_Tracks[i].m_Track == track && m_Tracks[i].m_Id == id)
			{
				outAccIdx = i;
				break;
			}
		}
		if(i == numTracks)
		{
			outAccIdx = numTracks;
			crExpressions::Track& newTrack = m_Tracks.Grow();
			newTrack.m_Id = id;
			newTrack.m_Track = track;
			newTrack.m_Flags = type;
			if(isInput)
			{
				newTrack.m_Flags |= crExpressions::Track::kIsInput;
			}
		}

		u32 trackId = (u32(track)<<16)|u32(id);
		m_Signature = crc32(m_Signature, (const u8*)&trackId, sizeof(u32));
	}

	atArray<crExpressions::Track> m_Tracks;
	atArray<crMotionDescription> m_Motions;
	u32 m_Signature;
};

////////////////////////////////////////////////////////////////////////////////

void crExpressions::CalcInputOutputDofs()
{
	CalcInputOutputDofsIterator it;

	sysMemStartTemp();
	u32 numExpressions = GetNumExpressions();
	for(u32 i=0; i < numExpressions; i++)
	{
		crExpressionOp* expressionOp = GetExpression(i)->GetExpressionOp();
		if(expressionOp)
		{
			it.Visit(*expressionOp);
		}
	}
	sysMemEndTemp();

	m_Tracks = it.m_Tracks;
	m_Motions = it.m_Motions;
	m_Signature = it.m_Signature;

	sysMemStartTemp();
	it.Shutdown();
	sysMemEndTemp();
}

////////////////////////////////////////////////////////////////////////////////

struct CalcVariablesIterator : public crExpressionOp::ModifyingIterator
{
	CalcVariablesIterator()
	{
	}

	virtual ~CalcVariablesIterator()
	{
	}

	void Shutdown()
	{
		m_Variables.Reset();
	}

	virtual crExpressionOp* Visit(crExpressionOp& op)
	{
		switch(op.GetOpType())
		{
		case crExpressionOp::kOpTypeVariableGet:
		case crExpressionOp::kOpTypeVariableSet:
			{
				crExpressionOpVariableGetSet& opGetSet = static_cast<crExpressionOpVariableGetSet&>(op);
				opGetSet.SetIndex(AddVariable(opGetSet.GetHash()));
			}
			break;

		default:
			break;
		}
		return crExpressionOp::ModifyingIterator::Visit(op);
	}

	u16 AddVariable(u32 hash)
	{
		u32 numVariables = m_Variables.GetCount();
		u16 i=0;
		for(; i<numVariables; ++i)
		{
			if(m_Variables[i].m_Hash == hash)
			{
				return i;
			}
			else if(m_Variables[i].m_Hash > hash)
			{
				break;
			}
		}

		m_Variables.Grow();
		m_Variables.Pop();

		crExpressions::Variable& var = m_Variables.Insert(i);
		var.m_Hash = hash;
		return i;
	}

	atArray<crExpressions::Variable> m_Variables;
};

////////////////////////////////////////////////////////////////////////////////

void crExpressions::CalcVariables()
{
	CalcVariablesIterator it;

	sysMemStartTemp();
	u32 numExpressions = GetNumExpressions();
	for(u32 pass=0; pass<2; pass++)
	{
		for(u32 i=0; i<numExpressions; i++)
		{
			crExpressionOp* expressionOp = GetExpression(i)->GetExpressionOp();
			if(expressionOp)
			{
				it.Visit(*expressionOp);
			}
		}
	}
	sysMemEndTemp();

	m_Variables = it.m_Variables;
	
	sysMemStartTemp();
	it.Shutdown();
	sysMemEndTemp();
}

////////////////////////////////////////////////////////////////////////////////

void crExpressions::SetOptimized(bool optimized)
{
	if(optimized)
	{
		m_Flags |= kOptimized;
	}
	else
	{
		m_Flags &= ~kOptimized;
	}
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressions::Track::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(Track);
	STRUCT_FIELD(m_Id);
	STRUCT_FIELD(m_Track);
	STRUCT_FIELD(m_Flags);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpressions::Variable::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(Variable);
	STRUCT_FIELD(m_Hash);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

bool crExpressions::sm_InitClassCalled = false;

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
