//
// crextra/expressionpacker.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef CREXTRA_EXPRESSIONPACKER_H
#define CREXTRA_EXPRESSIONPACKER_H


#include "cranimation/animation_config.h"

#if CR_DEV
#include "atl/array.h"
#include "cranimation/streamer.h"
#include "crextra/expressionstream.h"

namespace rage
{

class crExpressions;
class crExpressionOp;
class crExpressionOpGetSet;
class crExpressionOpNullary;
class crExpressionOpUnary;
class crExpressionOpBinary;
class crExpressionOpTernary;

////////////////////////////////////////////////////////////////////////////////

class crExpressionPacker
{
public:
	static u32 Pack(crExpressions& expressions, crExpressionStream** outStreams, u32* outSizes);

private:
	struct ScratchStream
	{
		void Insert(ExprOp op);
		void Append(ExprOp op);
		void Merge(const ScratchStream& other);

		BaseStreamer m_AlignedParams;
		BaseStreamer m_Params;
		atArray<u8> m_Operations;
	};

	static void CreateBranch(ExprOp op, ScratchStream& dest, const ScratchStream& jumpOver);
	static void CreateBranch(ExprOp branchOp, ScratchStream& dest, const ScratchStream* jumpOver, u32 numJump);

	static void ConvertRecursive(const crExpressionOp* op, ScratchStream& stream);
	static void ConvertOp(const crExpressionOp* op, ScratchStream& stream);
	static void ConvertGetSetOp(const crExpressionOpGetSet* subOp, ScratchStream& stream);
	static void ConvertOpNullary(const crExpressionOpNullary* subOp, ScratchStream& stream);
	static void ConvertOpUnary(const crExpressionOpUnary* subOp, ScratchStream& stream);
	static void ConvertOpBinary(const crExpressionOpBinary* subOp, ScratchStream& stream);
	static void ConvertOpTernary(const crExpressionOpTernary* subOp, ScratchStream& stream);
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV

#endif // CREXTRA_EXPRESSIONPACKER_H