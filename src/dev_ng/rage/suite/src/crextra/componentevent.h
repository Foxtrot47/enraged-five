//
// crextra/componentevent.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CREXTRA_COMPONENTEVENT_H
#define CREXTRA_COMPONENTEVENT_H

#include "creature/component.h"

#include "atl/inlist.h"
#include "atl/pool.h"
#include "event/instance.h"
#include "event/player.h"
#include "system/criticalsection.h"

namespace rage {

class crClip;
class crpmParameterizedMotionData;

// PURPOSE: Data structure containing info about the event that will get played
class crEventData
{
public:
	crEventData() : m_EventSet(NULL), m_Clip(NULL), m_PmData(NULL), m_Rate(1.f) {}

	const evtSet*		m_EventSet; // The actual event we want to play
	const crClip*		m_Clip;		// If non-NULL, contains the clip that launches this event
	const crpmParameterizedMotionData* m_PmData;
	float m_Rate;

	void Reset()
	{
		m_EventSet = NULL;
		m_Clip = NULL;
		m_PmData = NULL;
		m_Rate = 1.0f;
		m_EventListNode.m_next = NULL;
		m_EventListNode.m_prev = NULL;
		ASSERT_ONLY(m_EventListNode.m_owner = NULL);
	}

protected:
	friend class crCreatureComponentEvent;

	inlist_node<crEventData>	m_EventListNode; // List node data, so we can make queues of events
};

// PURPOSE: A custom event player to play events on creatures
class crCreatureEventPlayer : public evtPlayer
{
public:
	// PURPOSE: Constructor
	crCreatureEventPlayer();

	// PURPOSE: Resource constructor
	crCreatureEventPlayer(datResource& rsc);

	// PURPOSE: Creates a parameter list for the params that events may want to read
	void CreateParameterList();

	// PURPOSE: Gets the parameter for animation rate
	atAny& GetAnimRate() { return m_Parameters.GetParam(s_AnimRate); }

	// PURPOSE: Gets the parameter for the creatures root position
	atAny& GetPosition() { return m_Parameters.GetParam(s_Position); }

	// PURPOSE: Gets the parameter for the creatures skeleton
	atAny& GetSkeleton() { return m_Parameters.GetParam(s_Skeleton); }

	// PURPOSE: Needs to be called once for any subclass of evtPlayer
	static void RegisterPlayerClass();
	// PURPOSE: Needs to be called once for any subclass of evtPlayer
	static void UnregisterPlayerClass();

private:
	static evtParamScheme s_ParameterScheme;
	static evtParamId s_Position;
	static evtParamId s_Skeleton;
	static evtParamId s_AnimRate;
};

// PURPOSE: Component for creatures that want to play motion tree event tags.
class crCreatureComponentEvent : public crCreatureComponent
{
public:

	// PURPOSE: Default constructor
	crCreatureComponentEvent();

	// PURPOSE: Initializing constructor
	// PARAMS: creature - reference to creature this component is part of
	crCreatureComponentEvent(crCreature& creature);

	// PURPOSE: Copy constructor
	crCreatureComponentEvent(const crCreatureComponentEvent&) { Assert(0); }

	// PURPOSE: Resource constructor
	crCreatureComponentEvent(datResource&);

	// PURPOSE: Destructor
	virtual ~crCreatureComponentEvent();

	// PURPOSE: Declare type
	CR_DECLARE_CREATURE_COMPONENT_TYPE(crCreatureComponentEvent);

	// PURPOSE: Off line resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT

	// PURPOSE: Initialization
	void Init(crCreature& creature);

	// PURPOSE: Free/release dynamic resources
	virtual void Shutdown();

	// PURPOSE: PostUpdate override
	virtual void PostUpdate();

	// PURPOSE: Adds a new event to the queue, to be triggered in PostUpdate
	// NOTES: If a clip is passed in, we'll increment the ref count for the clip so that
	// the clip doesn't get deleted before the eventSet gets run
	void AddEvent(const evtSet& eventSet, const crClip& clip, float rate = 1.f);
	void AddEvent(const evtSet& eventSet, const crpmParameterizedMotionData& pmData, float rate = 1.f);

	// PURPOSE: Init class
	// PARAMS: eventPoolSize - pass in a pool size for the event data pool (number of event sets that can be triggered in any one frame)
	static void InitClass(u16 eventPoolSize);

	// PURPOSE: Shutdown class
	static void ShutdownClass();

	// PURPOSE: Clears out the event queue without processing any events
	void FlushPendingEvents();

protected:
	crCreatureEventPlayer m_Player;
	inlist<crEventData, &crEventData::m_EventListNode> m_EventQueue;

	static atPool<crEventData> sm_EventDataPool;
	static sysCriticalSectionToken sm_EventQueueLock; // static to protect access to the pool, we could have a seperate lock per eventqueue for some operations.
};

} // namespace rage

#endif // CREXTRA_COMPONENTEVENT_H
