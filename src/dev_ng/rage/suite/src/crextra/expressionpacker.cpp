//
// crextra/expressionpacker.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#include "expressionpacker.h"

#if CR_DEV
#include "expressionops.h"
#include "expression.h"
#include "expressions.h"

#include "vectormath/layoutconvert.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

struct SimplifyIterator : public crExpressionOp::ModifyingIterator
{
	virtual crExpressionOp* Visit(crExpressionOp& op);
};

////////////////////////////////////////////////////////////////////////////////

void crExpressionPacker::ScratchStream::Insert(ExprOp op)
{
	m_Operations.Grow();
	m_Operations.Pop();
	m_Operations.Insert(0) = u8(op);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPacker::ScratchStream::Append(ExprOp op)
{
	m_Operations.PushAndGrow(u8(op));
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPacker::ScratchStream::Merge(const ScratchStream& other)
{
	m_AlignedParams.Grow(other.m_AlignedParams);
	m_Params.Grow(other.m_Params);
	for(int i=0; i < other.m_Operations.GetCount(); i++)
	{
		m_Operations.PushAndGrow(other.m_Operations[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

u32 crExpressionPacker::Pack(crExpressions& expressions, crExpressionStream** outStreams, u32* outSizes)
{
	u32 numExpressions = expressions.GetNumExpressions();
	u32 maxPackedSize = 0;
	for(u32 i=0; i < numExpressions; i++)
	{
		sysMemStartTemp();

		// simplify expression tree
		SimplifyIterator simplify;
		crExpressionOp* op = expressions.GetExpression(i)->GetExpressionOp()->Clone();
		crExpressionOp* simplified = simplify.Visit(*op);
		
		// generate scratch stream
		ScratchStream stream;
		ConvertRecursive(simplified, stream);
		stream.Append(OpHalt);
		crExpression* expr = rage_new crExpression();
		expr->SetExpressionOp(simplified);

		// create header
		Streamer<crExpressionStream> header;
		header.Grow(stream.m_AlignedParams);
		header.Grow(stream.m_Params);
		header.Grow(sizeof(u8)*stream.m_Operations.GetCount(), stream.m_Operations.GetElements());
		header->m_Hash = atDataHash((const char*)header->m_Data, header.GetSize()-sizeof(crExpressionStream));
		header->m_AlignParamSize = stream.m_AlignedParams.GetSize();
		header->m_ParamSize = stream.m_Params.GetSize();
		Assign(header->m_NumOperations, stream.m_Operations.GetCount());
		Assign(header->m_MaxStackDepth, expr->CalcStackDepth());
		BYTESWAP(header->m_Hash);
		BYTESWAP(header->m_AlignParamSize);
		BYTESWAP(header->m_ParamSize);
		BYTESWAP(header->m_NumOperations);
		BYTESWAP(header->m_MaxStackDepth);

		delete expr;
		stream.m_AlignedParams.Clear();
		stream.m_Params.Clear();
		stream.m_Operations.Reset();
		sysMemEndTemp();

		// allocate final buffer
		u32 totalSize = header.GetSize();
		crExpressionStream* buffer = reinterpret_cast<crExpressionStream*>(rage_aligned_new(16) u8[totalSize]);
		memcpy(buffer, header.GetPtr(), totalSize);
		outStreams[i] = buffer;
		maxPackedSize = Max(totalSize, maxPackedSize);
		if(outSizes)
		{
			outSizes[i] = totalSize;
		}

		// delete temporary header
		sysMemStartTemp();
		header.Clear();
		sysMemEndTemp();
	}

	return maxPackedSize;
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPacker::CreateBranch(ExprOp branchOp, ScratchStream& dest, const ScratchStream& jumpOver)
{
	Streamer<crExprOpBranch> branch;
	branch->m_AlignParamOffset = jumpOver.m_AlignedParams.GetSize();
	branch->m_ParamOffset = jumpOver.m_Params.GetSize();
	branch->m_OperationOffset = jumpOver.m_Operations.GetCount();
	BYTESWAP(branch->m_AlignParamOffset);
	BYTESWAP(branch->m_ParamOffset);
	BYTESWAP(branch->m_OperationOffset);
	dest.Append(branchOp);
	dest.m_Params.Grow(branch);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPacker::CreateBranch(ExprOp branchOp, ScratchStream& dest, const ScratchStream* jumpOver, u32 numJump)
{
	Streamer<crExprOpBranch> branch;
	branch->m_AlignParamOffset = 0;
	branch->m_ParamOffset = 0;
	branch->m_OperationOffset = 0;
	for(u32 i=0; i<numJump; ++i)
	{
		branch->m_AlignParamOffset += jumpOver[i].m_AlignedParams.GetSize();
		branch->m_ParamOffset += jumpOver[i].m_Params.GetSize();
		branch->m_OperationOffset += jumpOver[i].m_Operations.GetCount();
	}
	BYTESWAP(branch->m_AlignParamOffset);
	BYTESWAP(branch->m_ParamOffset);
	BYTESWAP(branch->m_OperationOffset);
	dest.Append(branchOp);
	dest.m_Params.Grow(branch);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPacker::ConvertRecursive(const crExpressionOp* op, ScratchStream& stream)
{
	u32 numChildren = op->GetNumChildren();
	ScratchStream* childStreams = rage_new ScratchStream[numChildren];
	for(u32 i=0; i < numChildren; ++i)
	{
		ConvertRecursive(op->GetChild(i), childStreams[i]);
	}

	switch(op->GetOpType())
	{
	case crExpressionOp::kOpTypeNary:
		{
			const crExpressionOpNary* subOp = static_cast<const crExpressionOpNary*>(op);
			if(subOp->GetNaryOpType() == crExpressionOpNary::kNaryOpTypeComma)
			{
				stream.Append(OpPush);
				for(u32 i=0; i < numChildren; i++)
				{
					childStreams[i].Insert(OpPop);
				}
			}
			else if(subOp->GetNaryOpType() == crExpressionOpNary::kNaryOpTypeLogicalAnd)
			{
				for(u32 i=numChildren-1; i > 0; i--)
				{
					childStreams[i-1].Insert(OpPop);
					CreateBranch(OpBranchZero, childStreams[i-1], &childStreams[i], numChildren-i);
				}
			}
			else if(subOp->GetNaryOpType() == crExpressionOpNary::kNaryOpTypeLogicalOr)
			{
				for(u32 i=numChildren-1; i > 0; i--)
				{
					childStreams[i-1].Insert(OpPop);
					CreateBranch(OpBranchNotZero, childStreams[i-1], &childStreams[i], numChildren-i);
				}
			}
			break;
		}

	case crExpressionOp::kOpTypeTernary:
		{
			const crExpressionOpTernary* subOp = static_cast<const crExpressionOpTernary*>(op);
			if(subOp->GetTernaryOpType() == crExpressionOpTernary::kTernaryOpTypeConditional)
			{
				childStreams[1].Insert(OpPop);
				childStreams[2].Insert(OpPop);
				CreateBranch(OpBranch, childStreams[1], childStreams[2]);
				CreateBranch(OpBranchZero, childStreams[0], childStreams[1]);
			}
			break;
		}

	case crExpressionOp::kOpTypeBinary:
		{
			const crExpressionOpBinary* subOp = static_cast<const crExpressionOpBinary*>(op);
			if(subOp->GetBinaryOpType() == crExpressionOpBinary::kBinaryOpTypeLogicalAnd)
			{
				childStreams[1].Insert(OpPop);
				CreateBranch(OpBranchZero, childStreams[0], childStreams[1]);
			}
			else if(subOp->GetBinaryOpType() == crExpressionOpBinary::kBinaryOpTypeLogicalOr)
			{
				childStreams[1].Insert(OpPop);
				CreateBranch(OpBranchNotZero, childStreams[0], childStreams[1]);
			}
			break;
		}

	default: break;
	}

	for(u32 i=0; i < numChildren; i++)
	{
		stream.Merge(childStreams[i]);
	}
	delete [] childStreams;

	ConvertOp(op, stream);
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPacker::ConvertOp(const crExpressionOp* op, ScratchStream& stream)
{
	// convert current operation
	u32 opType = op->GetOpType();
	switch(opType)
	{
	case crExpressionOp::kOpTypeConstant:
	{
		crExpressionOp::Value v = static_cast<const crExpressionOpConstant*>(op)->GetValue();
		BYTESWAP(v.GetVector());
		stream.m_AlignedParams.Grow(sizeof(Vector_4V), &v);
		stream.Append(OpConstant);
		break;
	}

	case crExpressionOp::kOpTypeConstantFloat:
	{
		f32 f = static_cast<const crExpressionOpConstantFloat*>(op)->GetFloat();
		BYTESWAP(f);
		stream.m_Params.Grow(sizeof(f32), &f);
		stream.Append(OpConstantFloat);
		break;
	}

	case crExpressionOp::kOpTypeGet:
	case crExpressionOp::kOpTypeSet:
	case crExpressionOp::kOpTypeValid:
	case crExpressionOp::kOpTypeComponentGet:
	case crExpressionOp::kOpTypeComponentSet:
	case crExpressionOp::kOpTypeObjectSpaceGet:
	case crExpressionOp::kOpTypeObjectSpaceSet:
	case crExpressionOp::kOpTypeObjectSpaceConvertTo:
	case crExpressionOp::kOpTypeObjectSpaceConvertFrom:
	{
		const crExpressionOpGetSet* subOp = static_cast<const crExpressionOpGetSet*>(op);
		Assert(subOp->GetAcceleratedIndex() >= 0);
		Assert(!subOp->GetOrder());
		Streamer<crExprOpFrame> param;
		Assign(param->m_AccIdx, subOp->GetAcceleratedIndex());
		param->m_Component = subOp->GetComponent();
		param->m_Track = subOp->GetTrack();
		param->m_Id = subOp->GetId();
		param->m_Type = subOp->GetType();
		param->m_ForceDefault = subOp->IsForceDefault();
		BYTESWAP(param->m_AccIdx);
		BYTESWAP(param->m_Component);
		BYTESWAP(param->m_Track);
		BYTESWAP(param->m_Id);
		BYTESWAP(param->m_Type);
		BYTESWAP(param->m_ForceDefault);
		stream.m_Params.Grow(param);
		ConvertGetSetOp(subOp, stream);
		break;
	}

	case crExpressionOp::kOpTypeNullary:
		ConvertOpNullary(static_cast<const crExpressionOpNullary*>(op), stream);
		break;

	case crExpressionOp::kOpTypeUnary:
		ConvertOpUnary(static_cast<const crExpressionOpUnary*>(op), stream);
		break;

	case crExpressionOp::kOpTypeBinary:
		ConvertOpBinary(static_cast<const crExpressionOpBinary*>(op), stream);
		break;

	case crExpressionOp::kOpTypeTernary:
		ConvertOpTernary(static_cast<const crExpressionOpTernary*>(op), stream);
		break;

	case crExpressionOp::kOpTypeNary:
		break;

	case crExpressionOp::kOpTypeSpecialLookAt:
	{
		const crExpressionOpSpecialLookAt* subOp = static_cast<const crExpressionOpSpecialLookAt*>(op);
		Streamer<crExprOpLookAt> param;
		param->m_Offset = subOp->GetOffset();
		param->m_LookAtAxis = subOp->GetLookAtAxis();
		param->m_LookAtUpAxis = subOp->GetLookAtUpAxis();
		param->m_OriginUpAxis = subOp->GetOriginUpAxis();
		BYTESWAP(param->m_Offset);
		BYTESWAP(param->m_LookAtAxis);
		BYTESWAP(param->m_LookAtUpAxis);
		BYTESWAP(param->m_OriginUpAxis);
		stream.m_AlignedParams.Grow(param);
		stream.Append(OpLookAt);
		break;
	}

	case crExpressionOp::kOpTypeSpecialCurve:
	{
		const crExpressionOpSpecialCurve* subOp = static_cast<const crExpressionOpSpecialCurve*>(op);
		Streamer<crExprOpCurve> param;
		param->m_NumKeys = subOp->GetNumKeys();
		for(int i=0; i < subOp->GetNumKeys(); ++i)
		{
			crExprOpCurve::Key key;
			subOp->GetKey(i, key.m_In, key.m_Out);
			param.Grow(sizeof(crExprOpCurve::Key), &key);
		}
		param->m_Size = param.GetSize();
		BYTESWAP_ARRAY(param.GetPtr(), param.GetSize());
		stream.m_Params.Grow(param);
		stream.Append(OpCurve);
		break;
	}

	case crExpressionOp::kOpTypeMotion:
	{
		const crExpressionOpMotion* subOp = static_cast<const crExpressionOpMotion*>(op);
		Streamer<crExprOpMotion> param;
		param->m_AccRotIdx = subOp->m_AccRotIdx;
		param->m_AccTransIdx = subOp->m_AccTransIdx;
		param->m_Descr = subOp->m_Descr;
		BYTESWAP(param->m_AccRotIdx);
		BYTESWAP(param->m_AccTransIdx);
		BYTESWAP_ARRAY(&param->m_Descr.m_Gravity, sizeof(Vec3V));
		BYTESWAP_ARRAY(&param->m_Descr.m_Angular, sizeof(crMotionDescription::Dof3));
		BYTESWAP_ARRAY(&param->m_Descr.m_Linear, sizeof(crMotionDescription::Dof3));
		stream.m_AlignedParams.Grow(param);
		stream.Append(OpMotion);
		break;
	}

	case crExpressionOp::kOpTypeSpecialLinear:
	{
		const crExpressionOpSpecialLinear* subOp = static_cast<const crExpressionOpSpecialLinear*>(op);
		Streamer<crExprOpLinear> param;

		// assign sources
		u32 numSource4 = subOp->m_Sources.GetCount() / 4;
		for(u32 i=0; i < numSource4; i++)
		{
			for(u32 j=0; j < 4; j++)
			{
				const crExpressionOpSpecialLinear::Source& src = subOp->m_Sources[i+j*numSource4];
				u16 accIdx, offset;
				Assign(accIdx, src.m_AccIdx);
				Assign(offset, src.m_Component*sizeof(f32));
				BYTESWAP(accIdx);
				BYTESWAP(offset);
				param.Grow(sizeof(u16), &accIdx);
				param.Grow(sizeof(u16), &offset);
			}
		}

		// assign intervals
		u32 intervalPerSource = subOp->m_IntervalPerSource;
		u32 numInterval4 = subOp->m_Intervals.GetCount() / 4;
		for(u32 i=0; i < numInterval4; i++)
		{
			Vec3V begin[4], mult[4], add[4];
			for(u32 j=0; j < 4; j++)
			{
				const crExpressionOpSpecialLinear::Interval& interval = subOp->m_Intervals[i+j*numInterval4];
				begin[j] = interval.m_Begin;
				mult[j] = interval.m_Mult;
				add[j] = interval.m_Add;
			}
			BYTESWAP_ARRAY(begin, sizeof(begin));
			BYTESWAP_ARRAY(mult, sizeof(mult));
			BYTESWAP_ARRAY(add, sizeof(add));

			SoA_Vec3V soaBegin, soaMult, soaAdd;
			ToSoA(soaBegin, begin[0], begin[1], begin[2], begin[3]);
			ToSoA(soaMult, mult[0], mult[1], mult[2], mult[3]);
			ToSoA(soaAdd, add[0], add[1], add[2], add[3]);

			 // we don't need begin for the first range
			if((i % intervalPerSource)!=0)
			{
				param.Grow(sizeof(SoA_Vec3V), &soaBegin);
			}
			param.Grow(sizeof(SoA_Vec3V), &soaMult);
			param.Grow(sizeof(SoA_Vec3V), &soaAdd);
		}

		param->m_IntervalPerSource = subOp->m_IntervalPerSource;
		param->m_NumSources = subOp->m_Sources.GetCount();
		param->m_Size = param.GetSize();
		BYTESWAP(param->m_NumSources);
		BYTESWAP(param->m_IntervalPerSource);
		BYTESWAP(param->m_Size);

		stream.m_AlignedParams.Grow(param);
		stream.Append(subOp->m_Mode == crExpressionOpSpecialBlend::kBlendModeVector ? OpLinearVec : OpLinearQuat);
		break;
	}

	case crExpressionOp::kOpTypeVariableGet:
	case crExpressionOp::kOpTypeVariableSet:
	{
		const crExpressionOpVariableGetSet* subOp = static_cast<const crExpressionOpVariableGetSet*>(op);
		Streamer<crExprOpVariable> param;
		param->m_Hash = subOp->GetHash();
		param->m_Idx = subOp->GetIndex();
		BYTESWAP(param->m_Hash);
		BYTESWAP(param->m_Idx);
		stream.m_Params.Grow(param);
		stream.Append((op->GetOpType()==crExpressionOp::kOpTypeVariableGet)? OpGetVariable : OpSetVariable);
	}
	break;

	default: Quitf("Unsupported expression op: %d", opType);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPacker::ConvertGetSetOp(const crExpressionOpGetSet* subOp, ScratchStream& stream)
{
	bool isRelative = subOp->IsRelative();
	switch(subOp->GetOpType())
	{
		case crExpressionOp::kOpTypeGet:
			if(subOp->GetType() == kFormatTypeFloat)
			{
				stream.Append(OpGetComp);
			}
			else
			{
				stream.Append(isRelative ? OpGetRelative : OpGet);
			}
			break;

		case crExpressionOp::kOpTypeSet:
			if(subOp->GetType() == kFormatTypeFloat)
			{
				stream.Append(OpSetComp);
			}
			else
			{
				stream.Append(isRelative ? OpSetRelative : OpSet);
			}
			break;

		case crExpressionOp::kOpTypeValid:
			stream.Append(OpValid);
			break;

		case crExpressionOp::kOpTypeComponentSet:
			stream.Append(isRelative ? OpSetCompRelative : OpSetComp);
			break;

		case crExpressionOp::kOpTypeComponentGet: 	
			stream.Append(isRelative ? OpGetCompRelative : OpGetComp);
			break;

		case crExpressionOp::kOpTypeObjectSpaceGet:
			stream.Append(OpObjectGet);
			break;

		case crExpressionOp::kOpTypeObjectSpaceSet:
			stream.Append(OpObjectSet);
			break;

		case crExpressionOp::kOpTypeObjectSpaceConvertTo:
			stream.Append(OpObjectConvertTo);
			break;

		case crExpressionOp::kOpTypeObjectSpaceConvertFrom:
			stream.Append(OpObjectConvertFrom);
			break;

		default: FastAssert(false);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPacker::ConvertOpNullary(const crExpressionOpNullary* subOp, ScratchStream& stream)
{
	switch(subOp->GetNullaryOpType())
	{
	case crExpressionOpNullary::kNullaryOpTypeZero:
	case crExpressionOpNullary::kNullaryOpTypeVectorZero:
		stream.Append(OpZero);
		break;

	case crExpressionOpNullary::kNullaryOpTypeOne:
	case crExpressionOpNullary::kNullaryOpTypeVectorOne:
		stream.Append(OpOne);
		break;

	case crExpressionOpNullary::kNullaryOpTypeTime:
		stream.Append(OpTime);
		break;

	case crExpressionOpNullary::kNullaryOpTypeDeltaTime:
		stream.Append(OpDeltaTime);
		break;

	case crExpressionOpNullary::kNullaryOpTypeQuatIdentity:
		stream.Append(OpQuatIdentity);
		break;

	default: Quitf("Unsupported nullary expression op: %d", subOp->GetNullaryOpType());
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPacker::ConvertOpUnary(const crExpressionOpUnary* subOp, ScratchStream& stream)
{
	switch(subOp->GetUnaryOpType())
	{
	case crExpressionOpUnary::kUnaryOpTypeNegate: stream.Append(OpNegate); break;

	case crExpressionOpUnary::kUnaryOpTypeClamp01:
	case crExpressionOpUnary::kUnaryOpTypeVectorClamp01: stream.Append(OpClamp01); break;

	case crExpressionOpUnary::kUnaryOpTypeFromEuler: stream.Append(OpFromEuler); break;
	case crExpressionOpUnary::kUnaryOpTypeToEuler: stream.Append(OpToEuler); break;
	case crExpressionOpUnary::kUnaryOpTypeDegreesToRadians: stream.Append(OpDegToRad); break;
	case crExpressionOpUnary::kUnaryOpTypeRadiansToDegrees: stream.Append(OpRadToDeg); break;
	case crExpressionOpUnary::kUnaryOpTypeReciprocal: stream.Append(OpInvert); break;
	case crExpressionOpUnary::kUnaryOpTypeQuatInverse: stream.Append(OpQuatInvert); break;
	case crExpressionOpUnary::kUnaryOpTypeSquare: stream.Append(OpSquare); break;
	case crExpressionOpUnary::kUnaryOpTypeSqrt: stream.Append(OpSqrt); break;
	case crExpressionOpUnary::kUnaryOpTypeAbsolute: stream.Append(OpAbs); break;
	case crExpressionOpUnary::kUnaryOpTypeLog: stream.Append(OpLog); break;
	case crExpressionOpUnary::kUnaryOpTypeExp: stream.Append(OpExp); break;
	case crExpressionOpUnary::kUnaryOpTypeCos: stream.Append(OpCos); break;
	case crExpressionOpUnary::kUnaryOpTypeSin: stream.Append(OpSin); break;
	case crExpressionOpUnary::kUnaryOpTypeTan: stream.Append(OpTan); break;
	case crExpressionOpUnary::kUnaryOpTypeArcCos: stream.Append(OpArcCos); break;
	case crExpressionOpUnary::kUnaryOpTypeArcSin: stream.Append(OpArcSin); break;
	case crExpressionOpUnary::kUnaryOpTypeArcTan: stream.Append(OpArcTan); break;
	case crExpressionOpUnary::kUnaryOpTypeCosH: stream.Append(OpCosH); break;
	case crExpressionOpUnary::kUnaryOpTypeSinH: stream.Append(OpSinH); break;
	case crExpressionOpUnary::kUnaryOpTypeTanH: stream.Append(OpTanH); break;
	case crExpressionOpUnary::kUnaryOpTypeSplat: break; // Scalars are already splatted

	default: Quitf("Unsupported unary expression op: %d", subOp->GetUnaryOpType());
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPacker::ConvertOpBinary(const crExpressionOpBinary* subOp, ScratchStream& stream)
{
	switch(subOp->GetBinaryOpType())
	{
	case crExpressionOpBinary::kBinaryOpTypeAdd:
	case crExpressionOpBinary::kBinaryOpTypeVectorAdd:
		stream.Append(OpAdd);
		break;

	case crExpressionOpBinary::kBinaryOpTypeSubtract:
	case crExpressionOpBinary::kBinaryOpTypeVectorSubtract:
		stream.Append(OpSubtract);
		break;

	case crExpressionOpBinary::kBinaryOpTypeMultiply:
	case crExpressionOpBinary::kBinaryOpTypeVectorMultiply:
		stream.Append(OpMultiply);
		break;

	case crExpressionOpBinary::kBinaryOpTypeDivide:
		stream.Append(OpInvert);
		stream.Append(OpMultiply);
		break;

	case crExpressionOpBinary::kBinaryOpTypeLogicalAnd:
	case crExpressionOpBinary::kBinaryOpTypeLogicalOr:
		break;

	case crExpressionOpBinary::kBinaryOpTypeLogicalXor:
		stream.Append(OpXor);
		break;

	case crExpressionOpBinary::kBinaryOpTypeExponent: stream.Append(OpExponent); break;
	case crExpressionOpBinary::kBinaryOpTypeGreaterThan: stream.Append(OpGreaterThan); break;
	case crExpressionOpBinary::kBinaryOpTypeGreaterThanEqualTo: stream.Append(OpGreaterThanEqual); break;
	case crExpressionOpBinary::kBinaryOpTypeLessThan: stream.Append(OpLessThan); break;
	case crExpressionOpBinary::kBinaryOpTypeLessThanEqualTo: stream.Append(OpLessThanEqual); break;
	case crExpressionOpBinary::kBinaryOpTypeEqualTo: stream.Append(OpEqual); break;
	case crExpressionOpBinary::kBinaryOpTypeNotEqualTo: stream.Append(OpNotEqual); break;
	case crExpressionOpBinary::kBinaryOpTypeMax: stream.Append(OpMax); break;
	case crExpressionOpBinary::kBinaryOpTypeMin: stream.Append(OpMin); break;
	case crExpressionOpBinary::kBinaryOpTypeQuatMultiply: stream.Append(OpQuatMultiply); break;
	case crExpressionOpBinary::kBinaryOpTypeQuatScale: stream.Append(OpQuatScale); break;
	case crExpressionOpBinary::kBinaryOpTypeVectorTransform: stream.Append(OpTransform); break;

	default: Quitf("Unsupported binary expression op: %d", subOp->GetBinaryOpType());
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpressionPacker::ConvertOpTernary(const crExpressionOpTernary* subOp, ScratchStream& stream)
{
	switch(subOp->GetTernaryOpType())
	{
	case crExpressionOpTernary::kTernaryOpTypeMultiplyAdd:
	case crExpressionOpTernary::kTernaryOpTypeVectorMultiplyAdd:
		stream.Append(OpMultiplyAdd);
		break;

	case crExpressionOpTernary::kTernaryOpTypeLerp:
	case crExpressionOpTernary::kTernaryOpTypeVectorLerpVector:
		stream.Append(OpLerp);
		break;

	case crExpressionOpTernary::kTernaryOpTypeQuaternionLerp: stream.Append(OpQuatLerp); break;
	case crExpressionOpTernary::kTernaryOpTypeClamp: stream.Append(OpClamp); break;
	case crExpressionOpTernary::kTernaryOpTypeToVector: stream.Append(OpToVec); break;

	case crExpressionOpTernary::kTernaryOpTypeToQuaternion:
		stream.Append(OpToVec);
		stream.Append(OpFromEuler);
		break;

	case crExpressionOpTernary::kTernaryOpTypeConditional: break;

	default: Quitf("Unsupported ternary expression op: %d", subOp->GetTernaryOpType());
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* SimplifyIterator::Visit(crExpressionOp& op)
{
	crExpressionOp* retOp = &op;

	switch(op.GetOpType())
	{
	case crExpressionOp::kOpTypeSpecialBlend:
	{
		crExpressionOpSpecialBlend* subOp = static_cast<crExpressionOpSpecialBlend*>(&op);

		crExpressionOpConstant* sum = static_cast<crExpressionOpConstant*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstant));
		retOp = sum;
		if(subOp->GetBlendMode() == crExpressionOpSpecialBlend::kBlendModeQuaternion)
		{
			sum->SetValue(QuatV(V_IDENTITY));
			for(u32 i=0; i < op.GetNumChildren(); ++i)
			{
				crExpressionOp* source = subOp->GetSource(i);
				float weight = subOp->GetWeight(i);
				if(weight != 1.f)
				{
					crExpressionOpConstant* constant = static_cast<crExpressionOpConstant*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstant));
					constant->SetValue(Vec3VFromF32(weight));

					crExpressionOpBinary* scale = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
					scale->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeQuatScale);
					scale->SetSource(0, source);
					scale->SetSource(1, constant);
					source = scale;
				}

				crExpressionOpBinary* multiply = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
				multiply->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeQuatMultiply);
				multiply->SetSource(0, retOp);
				multiply->SetSource(1, source);
				retOp = multiply;

				subOp->SetSource(i, NULL);
			}
		}
		else
		{
			sum->SetValue(Vec3V(V_ZERO));
			for(u32 i=0; i < op.GetNumChildren(); ++i)
			{
				crExpressionOp* source = subOp->GetSource(i);
				float weight = subOp->GetWeight(i);
				if(weight != 1.f)
				{
					crExpressionOpConstant* constant = static_cast<crExpressionOpConstant*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstant));
					constant->SetValue(Vec3VFromF32(subOp->GetWeight(i)));

					crExpressionOpTernary* multiplyAdd = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
					multiplyAdd->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeVectorMultiplyAdd);
					multiplyAdd->SetSource(0, constant);
					multiplyAdd->SetSource(1, source);
					multiplyAdd->SetSource(2, retOp);
					retOp = multiplyAdd;
				}
				else
				{
					crExpressionOpBinary* add = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
					add->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeVectorAdd);
					add->SetSource(0, source);
					add->SetSource(1, retOp);
					retOp = add;
				}

				subOp->SetSource(i, NULL);
			}
		}
		break;
	}

	case crExpressionOp::kOpTypeNary:
	{
		crExpressionOpNary* subOp = static_cast<crExpressionOpNary*>(&op);
		if(subOp->GetNaryOpType() == crExpressionOpNary::kNaryOpTypeSum)
		{
			for(u32 i=0; i < op.GetNumChildren(); ++i)
			{
				if(i == 0)
				{
					retOp = subOp->GetSource(0);
				}
				else
				{
					crExpressionOpBinary* newOp = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
					newOp->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeAdd);
					newOp->SetSource(0, retOp);
					newOp->SetSource(1, subOp->GetSource(i));
					retOp = newOp;
				}
				subOp->SetSource(i, NULL);
			}
		}
		break;
	}

	case crExpressionOp::kOpTypeNullary:
	{
		crExpressionOpNullary* subOp = static_cast<crExpressionOpNullary*>(&op);
		if(subOp->GetNullaryOpType() == crExpressionOpNullary::kNullaryOpTypePi)
		{
			crExpressionOpConstantFloat* newOp = static_cast<crExpressionOpConstantFloat*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstantFloat));
			newOp->SetFloat(PI);
			retOp = newOp;
		}
		break;
	}

	default: break;
	}

	if(retOp != &op)
	{
		delete &op;
	}

	return crExpressionOp::ModifyingIterator::Visit(*retOp);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV