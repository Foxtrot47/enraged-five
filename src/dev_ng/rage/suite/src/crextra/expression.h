//
// crextra/expression.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CREXTRA_EXPRESSION_H
#define CREXTRA_EXPRESSION_H

#include "expressionops.h"

#if CR_DEV

namespace rage
{

class crFrame;
class crFrameFilter;
class crSkeletonData;


////////////////////////////////////////////////////////////////////////////////


// PURPOSE: An individual expression
// An expression describes the calculation or modification of a degree of freedom
class crExpression
{
	friend class crExpressions;

public:

	// PURPOSE: Constructor
	crExpression();

	// PURPOSE: Copy constructor
	crExpression(const crExpression&);

	// PURPOSE: Clone
	crExpression* Clone() const;

	// PURPOSE: Resource constructor
	crExpression(datResource&);

	// PURPOSE: Destructor
	~crExpression();

	// PURPOSE: Placement
	DECLARE_PLACE(crExpression);

	// PURPOSE: Off line resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT


	// PURPOSE: Optional initialization call to set DOF component to constant
	// PARAMS:
	// destTrack - track of destination DOF (see crAnimTrack::TRACK_XXX for enumeration)
	// destId - id of destination DOF (ie a bone id for bone related tracks)
	// destType - type of destination DOF (see crAnimTrack::FORMAT_XXX for enumeration)
	// destComponent - component of destination DOF to set (0,1,2 for x,y,z of vector/rotation types, use 0 for float types)
	// constant - float constant to set
	// NOTES: Quaternion DOFs are automatically converted to radian Euler angles, using XYZ rotation order
	void InitComponentConstant(u8 destTrack, u16 destId, u8 destType, u8 destComponent, float constant);

	// PURPOSE: Optional initialization call to copy DOF component
	// PARAMS:
	// destTrack - track of destination DOF (see crAnimTrack::TRACK_XXX for enumeration)
	// destId - id of destination DOF (ie a bone id for bone related tracks)
	// destType - type of destination DOF (see crAnimTrack::FORMAT_XXX for enumeration)
	// destComponent - component of destination DOF to set (0,1,2 for x,y,z of vector/rotation types, use 0 for float types)
	// srcTrack - track of source DOF (see crAnimTrack::TRACK_XXX for enumeration)
	// srcId - id of source DOF (ie a bone id for bone related tracks)
	// srcType - type of source DOF (see crAnimTrack::FORMAT_XXX for enumeration)
	// srcComponent - component of source DOF to set (0,1,2 for x,y,z of vector/rotation types)
	// NOTES: Quaternion DOFs are automatically converted to radian Euler angles, using XYZ rotation order
	void InitComponentCopy(u8 destTrack, u16 destId, u8 destType, u8 destComponent, u8 srcTrack, u16 srcId, u8 srcType, u8 srcComponent);

	// PURPOSE: Optional initialization call to offset DOF component
	// destTrack - track of destination DOF (see crAnimTrack::TRACK_XXX for enumeration)
	// destId - id of destination DOF (ie a bone id for bone related tracks)
	// destType - type of destination DOF (see crAnimTrack::FORMAT_XXX for enumeration)
	// destComponent - component of destination DOF to set (0,1,2 for x,y,z of vector/rotation types, use 0 for float types)
	// offset - float constant to add
	// NOTES: Quaternion DOFs are automatically converted to radian Euler angles, using XYZ rotation order
	void InitComponentOffset(u8 destTrack, u16 destId, u8 destType, u8 destComponent, float offset);

	// PURPOSE: Optional initialization call to scale DOF component
	// destTrack - track of destination DOF (see crAnimTrack::TRACK_XXX for enumeration)
	// destId - id of destination DOF (ie a bone id for bone related tracks)
	// destType - type of destination DOF (see crAnimTrack::FORMAT_XXX for enumeration)
	// destComponent - component of destination DOF to set (0,1,2 for x,y,z of vector/rotation types, use 0 for float types)
	// scalar - float constant multiple
	// NOTES: Quaternion DOFs are automatically converted to radian Euler angles, using XYZ rotation order
	void InitComponentScale(u8 destTrack, u16 destId, u8 destType, u8 destComponent, float scalar);

	// PURPOSE: Optional initialization call to clamp DOF component
	// destTrack - track of destination DOF (see crAnimTrack::TRACK_XXX for enumeration)
	// destId - id of destination DOF (ie a bone id for bone related tracks)
	// destType - type of destination DOF (see crAnimTrack::FORMAT_XXX for enumeration)
	// destComponent - component of destination DOF to set (0,1,2 for x,y,z of vector/rotation types, use 0 for float types)
	// rangeMin - minimum value
	// rangeMax - maximum value
	// NOTES: Quaternion DOFs are automatically converted to radian Euler angles, using XYZ rotation order
	void InitComponentClamp(u8 destTrack, u16 destId, u8 destType, u8 destComponent, float rangeMin, float rangeMax);

	// PURPOSE: Get stack depth (in number of values)
	u32 GetStackDepth() const;

	// PURPOSE: Process the expression
	// PARAMS:
	// inoutFrame - frame containing DOFs to process
	// time - optional total time elapsed (in seconds)
	// deltaTime - optional delta time elapsed (in seconds)
	// skelData - optional skeleton data pointer (can be NULL)
	// filter - optional filter on output DOFs (can be NULL)
	// variables - pointer to local variable stack (can be NULL)
	// numVariables - number of local variables in stack
	void Process(crFrame& inoutFrame, float time=0.f, float deltaTime=0.f, crCreature* creature=NULL, const crSkeletonData* skelData=NULL, crFrameFilter* filter=NULL, crExpressionOp::Value* variables=NULL, u32 numVariables=0) const;

	// PURPOSE: Get expression operation
	// RETURNS: Current top expression op
	const crExpressionOp* GetExpressionOp() const;

	// PURPOSE: Get expression operation (non-const)
	// RETURNS: Current top expression op
	// NOTES: Warning, modifying this expression op (or any of its children)
	// will have unknown side effects, as internally cached properties,
	// such as signatures, are are only calculated during a SetExpressionOp call.
	crExpressionOp* GetExpressionOp();

	// PURPOSE: Set expression operation
	// PARAMS: op - new top expression op
	// NOTES: Any existing expression ops are automatically deleted.
	// Internally cached properties (like signatures) are calculated on set.
	void SetExpressionOp(crExpressionOp* op);

	// PURPOSE: Dump out the expression (direct to output, or optionally to buffer).
	// PARAMS: verbosity - verbosity of output [0..3] (see notes)
	// buf - optional buffer (NULL buffer indicates direct output).
	// bufSize - optional buffer's size (must be specified if buffer is not NULL)
	// NOTES: verbosity levels; 0==minimal, 1==terse, 2==full, 3==debug
	void Dump(int verbosity=2, char* buf=NULL, u32 bufSize=0) const;

	// PURPOSE: Serialization
	void Serialize(datSerialize& s);

	// PURPOSE: Internal function, calculate worst stack depth
	u32 CalcStackDepth() const;

protected:

	// PURPOSE: Internal function, used during optional init calls
	crExpressionOp* InternalInitConstant(float constant) const;

private:

	datOwner<crExpressionOp> m_ExpressionOp;	
	u32 m_StackDepth;
};

////////////////////////////////////////////////////////////////////////////////

inline u32 crExpression::GetStackDepth() const
{
	return m_StackDepth;
}

////////////////////////////////////////////////////////////////////////////////

inline const crExpressionOp* crExpression::GetExpressionOp() const
{
	return m_ExpressionOp;
}

////////////////////////////////////////////////////////////////////////////////

inline crExpressionOp* crExpression::GetExpressionOp()
{
	return m_ExpressionOp;
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage

#endif // CR_DEV

#endif // CREXTRA_EXPRESSION_H
