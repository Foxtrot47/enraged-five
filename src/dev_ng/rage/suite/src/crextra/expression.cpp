//
// crextra/expression.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "expression.h"

#if CR_DEV
#include "atl/array_struct.h"
#include "cranimation/frame.h"
#include "cranimation/framefilters.h"
#include "crskeleton/skeletondata.h"
#include "data/safestruct.h"
#include "math/angmath.h"
#include "profile/profiler.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

extern __THREAD int s_ExpressionVersion;

////////////////////////////////////////////////////////////////////////////////

crExpression::crExpression()
: m_StackDepth(0)
, m_ExpressionOp(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpression::crExpression(const crExpression& other)
{
	m_StackDepth = other.m_StackDepth;
	m_ExpressionOp = other.m_ExpressionOp ? other.m_ExpressionOp->Clone() : NULL;
}

////////////////////////////////////////////////////////////////////////////////

crExpression* crExpression::Clone() const
{
	return rage_new crExpression(*this);
}

////////////////////////////////////////////////////////////////////////////////

crExpression::crExpression(datResource& rsc)
: m_ExpressionOp(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crExpression::~crExpression()
{
	delete m_ExpressionOp;
	m_ExpressionOp = NULL;
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crExpression);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crExpression::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crExpression)
	SSTRUCT_FIELD(crExpression, m_ExpressionOp)
	SSTRUCT_FIELD(crExpression, m_StackDepth)
	SSTRUCT_END(crExpression)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crExpression::InitComponentConstant(u8 destTrack, u16 destId, u8 destType, u8 destComponent, float constant)
{
	crExpressionOpComponentSet* opComponentSet = static_cast<crExpressionOpComponentSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeComponentSet));
	opComponentSet->SetTrack(destTrack);
	opComponentSet->SetId(destId);
	opComponentSet->SetType(destType);
	opComponentSet->SetComponent(destComponent);
	opComponentSet->SetSource(InternalInitConstant(constant));

	SetExpressionOp(opComponentSet);
}

////////////////////////////////////////////////////////////////////////////////

void crExpression::InitComponentCopy(u8 destTrack, u16 destId, u8 destType, u8 destComponent, u8 srcTrack, u16 srcId, u8 srcType, u8 srcComponent)
{
	crExpressionOpComponentGet* opComponentGet = static_cast<crExpressionOpComponentGet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeComponentGet));
	opComponentGet->SetTrack(srcTrack);
	opComponentGet->SetId(srcId);
	opComponentGet->SetType(srcType);
	opComponentGet->SetComponent(srcComponent);

	crExpressionOpComponentSet* opComponentSet = static_cast<crExpressionOpComponentSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeComponentSet));
	opComponentSet->SetTrack(destTrack);
	opComponentSet->SetId(destId);
	opComponentSet->SetType(destType);
	opComponentSet->SetComponent(destComponent);
	opComponentSet->SetSource(opComponentGet);

	SetExpressionOp(opComponentSet);
}

////////////////////////////////////////////////////////////////////////////////

void crExpression::InitComponentOffset(u8 destTrack, u16 destId, u8 destType, u8 destComponent, float offset)
{
	crExpressionOpComponentGet* opComponentGet = static_cast<crExpressionOpComponentGet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeComponentGet));
	opComponentGet->SetTrack(destTrack);
	opComponentGet->SetId(destId);
	opComponentGet->SetType(destType);
	opComponentGet->SetComponent(destComponent);

	crExpressionOpBinary* opBinaryAdd = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
	opBinaryAdd->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeAdd);
	opBinaryAdd->SetSource(0, opComponentGet);
	opBinaryAdd->SetSource(1, InternalInitConstant(offset));

	crExpressionOpComponentSet* opComponentSet = static_cast<crExpressionOpComponentSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeComponentSet));
	opComponentSet->SetTrack(destTrack);
	opComponentSet->SetId(destId);
	opComponentSet->SetType(destType);
	opComponentSet->SetComponent(destComponent);
	opComponentSet->SetSource(opBinaryAdd);

	SetExpressionOp(opComponentSet);
}

////////////////////////////////////////////////////////////////////////////////

void crExpression::InitComponentScale(u8 destTrack, u16 destId, u8 destType, u8 destComponent, float scalar)
{
	crExpressionOpComponentGet* opComponentGet = static_cast<crExpressionOpComponentGet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeComponentGet));
	opComponentGet->SetTrack(destTrack);
	opComponentGet->SetId(destId);
	opComponentGet->SetType(destType);
	opComponentGet->SetComponent(destComponent);

	crExpressionOpBinary* opBinaryMultiply = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
	opBinaryMultiply->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeMultiply);
	opBinaryMultiply->SetSource(0, opComponentGet);
	opBinaryMultiply->SetSource(1, InternalInitConstant(scalar));

	crExpressionOpComponentSet* opComponentSet = static_cast<crExpressionOpComponentSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeComponentSet));
	opComponentSet->SetTrack(destTrack);
	opComponentSet->SetId(destId);
	opComponentSet->SetType(destType);
	opComponentSet->SetComponent(destComponent);
	opComponentSet->SetSource(opBinaryMultiply);

	SetExpressionOp(opComponentSet);
}

////////////////////////////////////////////////////////////////////////////////

void crExpression::InitComponentClamp(u8 destTrack, u16 destId, u8 destType, u8 destComponent, float rangeMin, float rangeMax)
{
	crExpressionOpComponentGet* opComponentGet = static_cast<crExpressionOpComponentGet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeComponentGet));
	opComponentGet->SetTrack(destTrack);
	opComponentGet->SetId(destId);
	opComponentGet->SetType(destType);
	opComponentGet->SetComponent(destComponent);

	crExpressionOpTernary* opTernaryClamp = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
	opTernaryClamp->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeClamp);
	opTernaryClamp->SetSource(0, opComponentGet);
	opTernaryClamp->SetSource(1, InternalInitConstant(rangeMin));
	opTernaryClamp->SetSource(2, InternalInitConstant(rangeMax));

	crExpressionOpComponentSet* opComponentSet = static_cast<crExpressionOpComponentSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeComponentSet));
	opComponentSet->SetTrack(destTrack);
	opComponentSet->SetId(destId);
	opComponentSet->SetType(destType);
	opComponentSet->SetComponent(destComponent);
	opComponentSet->SetSource(opTernaryClamp);

	SetExpressionOp(opComponentSet);
}

////////////////////////////////////////////////////////////////////////////////

void crExpression::Process(crFrame& inoutFrame, float time, float deltaTime, crCreature* creature, const crSkeletonData* skelData, crFrameFilter* filter, crExpressionOp::Value* variables, u32 numVariables) const
{
	if(m_ExpressionOp)
	{
		crExpressionOp::ProcessHelper ph;
		ph.m_Frame = &inoutFrame;
		ph.m_Time = time;
		ph.m_DeltaTime = deltaTime;
		ph.m_Creature = creature;
		ph.m_SkeletonData = skelData;
		ph.m_Filter = filter;
		ph.m_NumValues = m_StackDepth;
		ph.m_Values = Alloca(crExpressionOp::Value, m_StackDepth);
		ph.m_NumVariables = numVariables;
		ph.m_Variables = variables;

		m_ExpressionOp->Process(ph);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpression::Dump(int verbosity, char* buf, u32 bufSize) const
{
	if(m_ExpressionOp)
	{
		crExpressionOp::DumpHelper dh(verbosity, buf, bufSize);
		dh.Visit(*m_ExpressionOp);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpression::Serialize(datSerialize& s)
{
	bool enabled = true;
	s << datLabel("Enabled:") << enabled << datNewLine;

	int version = s_ExpressionVersion;
	crExpressionOp::SerializeHelper sh(s, version);

	sh << m_ExpressionOp;

	if(s.IsRead())
	{
		m_StackDepth = CalcStackDepth();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crExpression::SetExpressionOp(crExpressionOp* op)
{
	if(m_ExpressionOp && m_ExpressionOp != op)
	{
		delete m_ExpressionOp;
		m_ExpressionOp = NULL;
	}

	m_ExpressionOp = op;

	m_StackDepth = CalcStackDepth();
}

////////////////////////////////////////////////////////////////////////////////

struct CalcStackDepthIterator : public crExpressionOp::ConstIterator
{
	CalcStackDepthIterator()
		: m_MaxStackDepth(1)
		, m_StackDepth(1)
	{
	}

	virtual ~CalcStackDepthIterator()
	{
	}

	virtual void Visit(const crExpressionOp& op)
	{
		// iterate on children
		const u32 numChildren = op.GetNumChildren();
		switch(op.GetOpType())
		{
		case crExpressionOp::kOpTypeBinary:
			if(static_cast<const crExpressionOpBinary&>(op).GetBinaryOpType() == crExpressionOpBinary::kBinaryOpTypeLogicalAnd ||
				static_cast<const crExpressionOpBinary&>(op).GetBinaryOpType() == crExpressionOpBinary::kBinaryOpTypeLogicalOr)
			{
				CalcStackDepthIterator itChild0, itChild1;
				itChild0.Visit(*op.GetChild(0));
				itChild1.Visit(*op.GetChild(1));

				m_MaxStackDepth = Max(m_MaxStackDepth, m_StackDepth + Max(itChild0.m_MaxStackDepth, itChild1.m_MaxStackDepth));
				m_StackDepth += itChild0.m_StackDepth + itChild1.m_StackDepth;
			}
			else
			{
				crExpressionOp::ConstIterator::Visit(op);
			}
			break;

		case crExpressionOp::kOpTypeTernary:
			if(static_cast<const crExpressionOpTernary&>(op).GetTernaryOpType() == crExpressionOpTernary::kTernaryOpTypeConditional)
			{
				Visit(*op.GetChild(0));

				CalcStackDepthIterator itChild1, itChild2;
				itChild1.Visit(*op.GetChild(1));
				itChild2.Visit(*op.GetChild(2));

				m_MaxStackDepth = Max(m_MaxStackDepth, m_StackDepth + Max(itChild1.m_MaxStackDepth, itChild2.m_MaxStackDepth));
				m_StackDepth += Max(itChild1.m_StackDepth, itChild2.m_StackDepth);
			}
			else
			{
				crExpressionOp::ConstIterator::Visit(op);
			}
			break;

		case crExpressionOp::kOpTypeNary:
			if(static_cast<const crExpressionOpNary&>(op).GetNaryOpType() == crExpressionOpNary::kNaryOpTypeComma)
			{
				m_StackDepth++;
				for(u32 i=0; i<numChildren; ++i)
				{
					m_StackDepth--;
					Visit(*op.GetChild(i));
				}
			}
			else if(static_cast<const crExpressionOpNary&>(op).GetNaryOpType() == crExpressionOpNary::kNaryOpTypeSum ||
				static_cast<const crExpressionOpNary&>(op).GetNaryOpType() == crExpressionOpNary::kNaryOpTypeLogicalAnd ||
				static_cast<const crExpressionOpNary&>(op).GetNaryOpType() == crExpressionOpNary::kNaryOpTypeLogicalOr)
			{
				for(u32 i=0; i<numChildren; ++i)
				{
					Visit(*op.GetChild(i));
					m_StackDepth--;
				}
				m_StackDepth++;
			}
			else
			{
				crExpressionOp::ConstIterator::Visit(op);
			}
			break;

		case crExpressionOp::kOpTypeSpecialBlend:
			for(u32 i=0; i<numChildren; ++i)
			{
				Visit(*op.GetChild(i));
				m_StackDepth--;
			}
			break;

		default:
			crExpressionOp::ConstIterator::Visit(op);
		}

		// update stack from current operation
		switch(op.GetOpType())
		{
		case crExpressionOp::kOpTypeNullary:
		case crExpressionOp::kOpTypeConstant:
		case crExpressionOp::kOpTypeConstantFloat:
		case crExpressionOp::kOpTypeValid:
		case crExpressionOp::kOpTypeGet:
		case crExpressionOp::kOpTypeComponentGet:
		case crExpressionOp::kOpTypeObjectSpaceGet:
		case crExpressionOp::kOpTypeSpecialBlend:
		case crExpressionOp::kOpTypeSpecialLinear:
			m_StackDepth++;
			break;

		case crExpressionOp::kOpTypeSet:
		case crExpressionOp::kOpTypeComponentSet:
		case crExpressionOp::kOpTypeObjectSpaceSet:
		case crExpressionOp::kOpTypeBinary:
			m_StackDepth--;
			break;

		case crExpressionOp::kOpTypeTernary:
		case crExpressionOp::kOpTypeSpecialLookAt:
			m_StackDepth--;
			m_StackDepth--;
			break;

		default: break;
		}

		m_MaxStackDepth = Max(m_MaxStackDepth, m_StackDepth);
	}

	u32 m_MaxStackDepth;
	u32 m_StackDepth;
};

////////////////////////////////////////////////////////////////////////////////

u32 crExpression::CalcStackDepth() const
{
	if(m_ExpressionOp)
	{
		CalcStackDepthIterator it;
		it.Visit(*m_ExpressionOp);
		return it.m_MaxStackDepth;
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* crExpression::InternalInitConstant(float constant) const
{
	if(IsClose(constant, 0.f))
	{
		crExpressionOpNullary* opNullary = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
		opNullary->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeZero);
		return opNullary;
	}
	else if(IsClose(constant, 1.f))
	{
		crExpressionOpNullary* opNullary = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
		opNullary->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeOne);
		return opNullary;
	}
	else
	{
		crExpressionOpConstant* opConstant = static_cast<crExpressionOpConstant*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstant));
		opConstant->GetValue().Set(constant);
		return opConstant;
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV