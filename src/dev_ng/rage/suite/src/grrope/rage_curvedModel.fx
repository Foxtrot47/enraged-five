//-----------------------------------------------------
// Rage Curved Model
//

// Get the globals
//#include "../shaderlib/rage_samplers.fxh"
#define NO_SKINNING
#include "../../../base/src/shaderlib/rage_common.fxh"
#include "../../../base/src/shaderlib/rage_drawbucket.fxh"
#include "../../../base/src/shaderlib/rage_specular.fxh"

RAGE_DRAWBUCKET(4);


struct VertexInput
{
   	float3 pos			: POSITION;
	float4 diffuse		: COLOR0;
    float2 texCoord0	: TEXCOORD0;
    float2 texCoord1	: TEXCOORD2;
    float3 normal		: NORMAL;
	float4 tangent		: TANGENT0;
};

struct CurveGPUVertexInput
{
    float3 pos			: POSITION;
	float3 normal		: NORMAL0;
	float3 tangent		: TEXCOORD0;
	float2 tex			: TEXCOORD1;
	
};

struct VertexOutput 
{
    DECLARE_POSITION(Pos)
    float3 WorldPos	  : TEXCOORD0;
    float2 Tex		  : TEXCOORD1;
    float3 Normal     : TEXCOORD2;
    float4 colour		: TEXCOORD3;
    
    float3 Tangent     : TEXCOORD4;
    float3 Binormal    : TEXCOORD5;
    
     float3 worldEyePos	: TEXCOORD6;
};



// constants
#if __WIN32PC && __SHADERMODEL < 40
#define MAX_CURVE_POINTS		19	// curvedModel/curvedModel.h, l 27 
#else
#define MAX_CURVE_POINTS		39	// curvedModel/curvedModel.h, l 27 
#endif

BeginConstantBufferDX10(rage_curvedModel_locals)

float		SegmentSize : SegmentSize ;
float4		curveCoeffs[ MAX_CURVE_POINTS * 3 ] : CurveCoefficients REGISTER2(vs,c64);  // tan.x, tan.y, tan.z, dist along rope
float4		approxTangent[ MAX_CURVE_POINTS + 1] : ApproximateTangent REGISTER2(vs,c196);  // tan.x, tan.y, tan.z, dist along rope
float3		uvScales : UVScales;  // model u scale, model v scale, distance scale (1,1,0 to select tube model mapping, 1,0,1 to select distance mapping)
float		radius : Radius;

EndConstantBufferDX10(rage_curvedModel_locals)

float3 GetPosOnCurve( int curvePos, float t )
{
	float  t2 = t * t;
	float4	tv = float4( 1.0f, t, t2,  t2 * t );

	return  float3( dot( curveCoeffs[ curvePos].xyzw, tv.xyzw ),
				 dot( curveCoeffs[ curvePos + 1].xyzw, tv.xyzw ),
				 dot( curveCoeffs[ curvePos + 2].xyzw, tv.xyzw ) );
}

float3 GetCurvedBasePosition( int segmentIndex, float t,  out float3 tangent )
{
	int		curvePos = segmentIndex * 3;
	
	float3 pos = GetPosOnCurve( curvePos, t );
	float3 curvetangent = GetPosOnCurve( curvePos, t + 0.001f );
		
	tangent = normalize( curvetangent - pos );
	return pos;
}


//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput ModelCurveVS( VertexInput IN) 
{
   VertexOutput OUT = (VertexOutput)0;
    
    float curvePos = IN.pos.y;
    
	// calculate segment index
	int segmentIndex = curvePos * SegmentSize ; 
	float tValue = ( curvePos * SegmentSize )- (float)segmentIndex;

	float3 nt = float3( 0.0f, 1.0f, 0.0f );
	float3 basePos =  GetCurvedBasePosition( segmentIndex,  tValue,  nt );// + float3( 2.0f, 0.0f, 0.0f );
		
	// now need binormal
	float3 binormal;
	float3 tangent; 
	
	float3 approxTan1 = float3(approxTangent[segmentIndex].x, approxTangent[segmentIndex].y, approxTangent[segmentIndex].z);
	float3 approxTan2 = float3(approxTangent[segmentIndex+1].x, approxTangent[segmentIndex+1].y, approxTangent[segmentIndex+1].z);
	float3 approxTan = approxTan1 * ( 1 - tValue) +  approxTan2 *  tValue;

	float distAlong = approxTangent[segmentIndex].w + (tValue * (approxTangent[segmentIndex+1].w - approxTangent[segmentIndex].w) );

	binormal = normalize(cross( nt, approxTan ) );//;	// no normalize neccesary as is cross product of normalized vectors???
	tangent = normalize( cross( binormal, nt ) );

	float rad = radius;

	float3 pos = tangent * IN.pos.x * rad + binormal * IN.pos.z * rad +  basePos;
	float3 normal = tangent * IN.normal.x + binormal * IN.normal.z + nt * IN.normal.y;
	float3 tangentFinal = tangent * IN.tangent.x + binormal * IN.tangent.z + nt * IN.tangent.y;

	// output position value
    OUT.Pos = mul(float4( pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    OUT.WorldPos = (float3) mul(float4( pos,1), gWorld);
    
    // create tangent space system
	OUT.Normal = normalize( mul(normal, (float3x3)gWorld));
    OUT.Tangent =  normalize( mul(tangentFinal, (float3x3)gWorld));  
    OUT.Binormal = normalize(cross(normal, tangentFinal));  
    
    // output texture coordinate
    OUT.Tex = float2(IN.texCoord0.x * uvScales.x, IN.texCoord0.y * uvScales.y + distAlong * uvScales.z);
    OUT.colour = float4( IN.diffuse.xyz, 1.0f );
    
  // Compute the eye vector
    OUT.worldEyePos = normalize(gViewInverse[3].xyz - OUT.WorldPos); 
    return OUT;
}
VertexOutput RopeCurveGenerateTestVS( CurveGPUVertexInput IN) 
{

    VertexOutput OUT = (VertexOutput)0;
// output position value

    float curvePos = IN.pos.y;
	int segmentIndex = curvePos * SegmentSize ; 
	float tValue = ( curvePos * SegmentSize )- (float)segmentIndex;
	
	float3 nt = float3( 0.0f, 1.0f, 0.0f );
float3 basePos =  GetCurvedBasePosition( 12,  IN.pos.y,  nt );// + float3( 2.0f, 0.0f, 0.0f );

	basePos = GetPosOnCurve( 39, IN.pos.y );
	float3 pos = float3( IN.pos.x , 0.0f,  IN.pos.z)  +  basePos;
	//pos = IN.pos;
	
    OUT.Pos = mul(float4( pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    OUT.WorldPos = (float3) mul(float4( pos,1), gWorld);
    
    // create tangent space system
	OUT.Normal = normalize( mul( IN.normal, (float3x3)gWorld));
    OUT.Tangent =  normalize( mul(IN.tangent, (float3x3)gWorld));  
    OUT.Binormal = normalize(cross( IN.normal, IN.tangent));  
    
    // output texture coordinate
    
    OUT.Tex = float2(IN.tex.x * uvScales.x, IN.tex.y * uvScales.y);
    OUT.colour = float4( approxTangent[10].x, approxTangent[10].y, approxTangent[10].z, 1.0f);// curveCoeffs[2 ]* 10.0f ;//float4( IN.pos.yyy /**/, 1.0f);
    
  // Compute the eye vector
    OUT.worldEyePos = normalize(gViewInverse[3].xyz - OUT.WorldPos); 
    return OUT;
}

//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput RopeCurveGenerateVS( CurveGPUVertexInput IN) 
{
//return RopeCurveGenerateTestVS( IN );
    VertexOutput OUT = (VertexOutput)0;
 
	   
    float curvePos = IN.pos.y;
    
	// calculate segment index
	int segmentIndex = curvePos * SegmentSize ; 
	float tValue = ( curvePos * SegmentSize )- (float)segmentIndex;

	float3 nt = float3( 0.0f, 1.0f, 0.0f );
	float3 basePos =  GetCurvedBasePosition( segmentIndex,  tValue,  nt );// + float3( 2.0f, 0.0f, 0.0f );
		
	// now need binormal
	float3 binormal;
	float3 tangent; 
	
	float3 approxTan1 = float3(approxTangent[segmentIndex].x, approxTangent[segmentIndex].y, approxTangent[segmentIndex].z);
	float3 approxTan2 = float3(approxTangent[segmentIndex+1].x, approxTangent[segmentIndex+1].y, approxTangent[segmentIndex+1].z);
	float3 approxTan = approxTan1 * ( 1 - tValue) +  approxTan2 *  tValue;

	float distAlong = approxTangent[segmentIndex].w + (tValue * (approxTangent[segmentIndex+1].w - approxTangent[segmentIndex].w) );

	binormal = normalize(cross( nt, approxTan ) );//;	// no normalize neccesary as is cross product of normalized vectors???
	tangent = normalize( cross( binormal, nt ) );

	float rad = radius;

	float3 pos = tangent * IN.pos.x * rad + binormal * IN.pos.z * rad +  basePos;
	float3 normal = tangent * IN.normal.x + binormal * IN.normal.z + nt * IN.normal.y;
	float3 tangentFinal = tangent * IN.tangent.x + binormal * IN.tangent.z + nt * IN.tangent.y;

	// output position value
    OUT.Pos = mul(float4( pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    OUT.WorldPos = (float3) mul(float4( pos,1), gWorld);
    
    // create tangent space system
	OUT.Normal = normalize( mul(normal, (float3x3)gWorld));
    OUT.Tangent =  normalize( mul(tangentFinal, (float3x3)gWorld));  
    OUT.Binormal = normalize(cross(normal, tangentFinal));  
    
    // output texture coordinate
    OUT.Tex = float2(IN.tex.x * uvScales.x, IN.tex.y * uvScales.y + distAlong * uvScales.z);

    OUT.colour = 1.0f;
    
  // Compute the eye vector
    OUT.worldEyePos = normalize(gViewInverse[3].xyz - OUT.WorldPos); 
    return OUT;
}



BeginSampler(sampler2D, DiffuseTexture, DiffuseSampler, DiffuseTex)
    string UIName = "Diffuse Texture";
ContinueSampler(sampler2D, DiffuseTexture, DiffuseSampler, DiffuseTex)
		AddressU = DIFFUSE_CLAMP;
		AddressV = DIFFUSE_CLAMP;
		AddressW = DIFFUSE_CLAMP;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR; 
		LOD_BIAS = 0;
EndSampler;

float3 GetSurfaceNormal( float2 surfacePos )
{
	float3 norm = float3(   1- abs( sin( surfacePos.x * 250.0f ) ), 2.0f , 0.0f );
	return normalize( norm );
}
//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 RopePS( VertexOutput IN ) : COLOR
{

//return float4( 1.0f, 0.0f, 0.0f, 1.0f );
//return IN.colour;;

// calculate surface bump;
	float blur = IN.colour.w; 
	
	float3 normal = normalize( IN.Normal );
	
	float3 E = normalize(gViewInverse[3].xyz - IN.WorldPos); //move to vertex shader?
   
	// calculate lighting
	 // Compute the light direction info, store the falloff in w component 
    float3 LightDir = (float3) rageComputeLightData(IN.WorldPos, 0).lightPosDir;
    float3 LightDir1 = (float3) rageComputeLightData(IN.WorldPos, 1).lightPosDir;
    float3 LightDir2 = (float3) rageComputeLightData(IN.WorldPos, 2).lightPosDir;

	float4 spec = specSpecularBlinn( 64.0f, normal, E, LightDir, LightDir1, LightDir2, float3( 0.0f, 1.0f, 0.0f));
	float3 lambert = float3( saturate( dot( normalize(LightDir.xyz), normal ) ),
						saturate( dot( normalize(LightDir1.xyz), normal ) ),
						saturate( dot( normalize(LightDir2.xyz), normal ) ) );
				
	lambert  = lambert *0.5f  + spec.xyz;
	
    float3 L0 = lambert.x * gLightColor[0].rgb ;
    float3 L1 = lambert.y * gLightColor[1].rgb ;
    float3 L2 = lambert.z * gLightColor[2].rgb ;
    
    float3 diffuse = tex2D( DiffuseSampler, IN.Tex.yx ).xyz;
    float3 col = (( L0 + L1 + L2 )  + 0.1f ) * diffuse;
	
	
	return float4( col, IN.colour.w  );
}
//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 RopeModelPS( VertexOutput IN ) : COLOR
{
	
	float3 normal = normalize( IN.Normal );
	
	float3 E = normalize(gViewInverse[3].xyz - IN.WorldPos); //move to vertex shader?
   
     float3 LightDir = (float3) rageComputeLightData(IN.WorldPos, 0).lightPosDir;
    float3 LightDir1 = (float3) rageComputeLightData(IN.WorldPos, 1).lightPosDir;
    float3 LightDir2 = (float3) rageComputeLightData(IN.WorldPos, 2).lightPosDir;
    
	// calculate lighting
	 // Compute the light direction info, store the falloff in w component 
	float4 spec = specSpecularBlinn( 32.0f, normal, E, LightDir, LightDir1, LightDir2, float3( 0.0f, 1.0f, 0.0f));
	float3 lambert = float3( saturate( dot( normalize(LightDir.xyz), normal ) ),
						saturate( dot( normalize(LightDir1.xyz), normal ) ),
						saturate( dot( normalize(LightDir2.xyz), normal ) ) );
				
	lambert  = lambert *0.5f  + spec.xyz;
	
    float3 L0 = lambert.x * gLightColor[0].rgb ;
    float3 L1 = lambert.y * gLightColor[1].rgb ;
    float3 L2 = lambert.z * gLightColor[2].rgb ;
    
    float3 diffuse = IN.colour.xyz;//tex2D( DiffuseSampler, IN.Tex.yx ).xyz;
    float3 col = (( L0 + L1 + L2 )  + 0.3f ) * diffuse;
	
	return float4( col, IN.colour.w  );
}



technique drawCurved
{
    pass p0 // Should be No Normal, No Diffuse
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = true;
        CullMode = CCW;
        ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER RopeCurveGenerateVS();
        PixelShader  = compile PIXELSHADER RopePS();
    }

    pass p1 // Should be Diffuse only
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = true;
        CullMode = CCW;
        ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER RopeCurveGenerateVS();
        PixelShader  = compile PIXELSHADER RopePS();
    }

    pass p2 // Should be Diffuse + Normal Map
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = true;
        CullMode = CCW;
        ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER RopeCurveGenerateVS();
        PixelShader  = compile PIXELSHADER RopePS();
    }

}

technique draw
{
    pass p0 // Should be No Normal, No Diffuse
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = true;
        CullMode = CCW;
        VertexShader = compile VERTEXSHADER ModelCurveVS();
        PixelShader  = compile PIXELSHADER RopeModelPS();
    }


    pass p1 // Should be Diffuse only
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = true;
        CullMode = CCW;
        VertexShader = compile VERTEXSHADER ModelCurveVS();
        PixelShader  = compile PIXELSHADER RopeModelPS();
    }

    pass p2 // Should be Diffuse + Normal Map
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = true;
        CullMode = CCW;
        VertexShader = compile VERTEXSHADER ModelCurveVS();
        PixelShader  = compile PIXELSHADER RopeModelPS();
    }
}

