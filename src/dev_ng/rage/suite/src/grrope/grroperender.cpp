// 
// sample_physics/ropeRender.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "phbound/boundcomposite.h"
#include "phcore/phmath.h"


// RAY - test
#include "phbound/boundsphere.h"
//
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"

#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grcore/vertexbuffer.h"
#include "grcore/indexbuffer.h"
#include "grcore/device.h"
#include "grmodel/modelfactory.h"
#include "vector/vector3.h"
#include "math/amath.h"

#include "curve/curve.h"
#include "grmodel/curvedmodel.h"
#include "grrope/grroperender.h"


namespace rage
{


//------------------------------------------------------------------------------------------------------------------------
//  Rope rendering code
//--------------------------------------------------------------------------------------------------------------------

/*
void RopeModel::InitRopeModel (grmShader *shader)
{
	Assert(!RopeModel::Instance().HasLods());

	// This initializes a static class so it should be moved to execute only once without checking.
	RopeModel::Instance().AddLod( 4.0f, 100, 12 );  // high lod
	RopeModel::Instance().AddLod( 8.0f, 45, 7 );	// medium lod 
	RopeModel::Instance().AddLod( 30.0f, 20, 5 );	// low lod

	if( NULL == shader )
	{
		const char* shaderName = "rage_curvedModel";
		shader = grmShaderFactory::GetInstance().Create( );
		shader->Load( shaderName );
	}

	RopeModel::Instance().SetShader(shader);
}
*/

void RopeModel::AddLod( float MaximumZValue,		// The maximum Z value for the lod to be rendered at
			int NumberSegmentsLength,				//	The number of segments along the length of the rope piece.
			int NumberSegmentsWidth,				//	The number of segments along the width of the rope pieace
			Vector2 uvScale )
{
	m_LodList.Push( rage_new RopeLod( MaximumZValue, NumberSegmentsLength, NumberSegmentsWidth, uvScale ));
}


//
u8	RopeModel::GetLodIndex( float z )
{
	int cnt = 0;	
	for ( atArray<RopeModel::RopeLod*>::iterator itor  = m_LodList.begin();
		itor != m_LodList.end();
		++itor, cnt++ )
	{
		if ( (*itor)->GetMaximumDepth() > z )
		{
			return static_cast<u8>( cnt );
		}
	}
	return static_cast<u8>(  m_LodList.size() - 1 ) ;
}


void RopeModel::render (const Matrix34& cameraMtx, phVerletCloth* rope, const atFixedArray<Vector3,sm_MaxDrawVerts> *drawVerts, int numVerts, int lodIndex, float radius, int pass)
{
	FastAssert( lodIndex >= 0 && lodIndex < static_cast<int>( m_LodList.size() ) );

	RopeLod*	lod = m_LodList[ lodIndex ];

	renderRope(cameraMtx, rope, drawVerts, numVerts, lod->GetModel(), radius, m_UVScale, pass);
}


void RopeModel::renderRope( const Matrix34& cameraMtx, phVerletCloth* rope, const atFixedArray<Vector3,sm_MaxDrawVerts> *drawVerts, int numVerts, TubeModel& model, float radius, const Vector2 &uvScale, int pass )
{
	grcViewport::SetCurrentWorldIdentity();

	// Use the number of edges to get the number of vertices, to work with removing edges while keeping the whole vertex list.
//	int numEdgesInStrand = rope->GetNumActiveEdges();

	int numEdgesInStrand = numVerts-1;//rope->GetNumActiveEdges();
	int maxEdgesPerBatch = MAX_CURVE_POINTS-1;

	// Find the number of render batches needed.
	int numBatches = numEdgesInStrand/MAX_CURVE_POINTS + 1;

	int firstEdgeInBatch = 0; //clothData.GetFirstEdge(strandIndex);

	// Render the rope spline in batches.
	for (int batchIndex=0; batchIndex<numBatches; batchIndex++)
	{
		int numEdgesInBatch = Min(numEdgesInStrand-batchIndex*maxEdgesPerBatch,maxEdgesPerBatch);

		RopeVertices accessor(rope,firstEdgeInBatch,numEdgesInBatch,drawVerts);
		setupCurvedModelShader(cameraMtx,accessor,radius,uvScale,*m_shader, &m_modelShaderVar);

		// setup texture
		model.render(m_shader, grcetNONE, pass);

		firstEdgeInBatch += maxEdgesPerBatch;
	}
}

int rmcRopeDrawable::GetLodLevel(const Matrix34 &cameraMtx) const
{
	float dist2toCamera = GetDistToCamera2(cameraMtx);
	float ropeSize2 = GetRopeSize2();

	int lod = -1;
	if (dist2toCamera<ropeSize2)
	{
		// The camera is approximately within the rope's bounding sphere, so use high lod.
		lod = 0;
	}
	else
	{
		for (int lodIndex=0; lodIndex<3; lodIndex++)
		{
			float z = RopeModel::Instance().GetLODMaximumDepth(lodIndex);
			if (dist2toCamera < square(z))
			{
				lod = lodIndex;
				break;
			}
		}
	}
	
	return lod;
}

void rmcRopeDrawable::Draw(const Matrix34 &cameraMtx, u32 UNUSED_PARAM(bucket), int lod, u16 UNUSED_PARAM(stats), int pass) const
{
	if (m_DrawVerts.GetCount()<=0)
		return;

	RopeModel::Instance().render(cameraMtx, m_rope, &m_DrawVerts, m_DrawVerts.GetCount(), lod, m_radius, pass);
}

void rmcRopeDrawable::Draw(const Matrix34 &cameraMtx, u32 bucket, int lod, u16 stats) const
{
	Draw(cameraMtx,bucket,lod,stats,0);
}

} // namespace rage
