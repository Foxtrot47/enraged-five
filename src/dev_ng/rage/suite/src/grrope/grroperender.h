//
// ropeRender/ropeRender.h
// 
// Copyright (C) 1999-2004 Rockstar Games.  All Rights Reserved.
//
#ifndef ROPE_RENDERER_H
#define ROPE_RENDERER_H

#include "cloth/environmentcloth.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"
#include "grcore/viewport_inline.h"
#include "grmodel/curvedmodel.h"
#include "rmcore/drawable.h"
#include <vector>


namespace rage
{


//////////////////////////////////////////////////////////////////////////
// PURPOSE
//	Creates a rope model with various levels of detail.It renders the model
//  out as a curved object
// NOTES
//	This object should be used like a singleton, since it uses a some memory
//  You should only create it which is required. So normal use would be to create
//	it when a level with ropes is loaded and use it to render out your ropes
//
//
class RopeModel
{
	// Is private as is singleton
	RopeModel(
				int ApproxNumberOfLods = 3 		// Number of Lods for rope model
		)  : m_shader(NULL), m_UVScale(1.0f,1.0f)

			
	{
		m_LodList.Reserve( ApproxNumberOfLods );
	}

	// PURPOSE
	//	Holds information about an lod for the rope.
	// NOTES
	//	This could be extended to hold information about
	//  different shaders to use on the rope for different lods.
	//
	class RopeLod
	{
	public:
		RopeLod( float z, int segmentsLength, int segmentsWidth, const Vector2 &uvScale )
			: m_z( z ),
			m_model( segmentsLength, segmentsWidth, false, uvScale )
		{
		}

		TubeModel&			GetModel()			{ return m_model; }
		const TubeModel&	GetModel() const	{ return m_model; }

		float GetMaximumDepth() const { return m_z; }

	private:
		float		m_z;
		TubeModel	m_model;

	};

public:
	static const int sm_MaxDrawVerts=128;
//	static void InitRopeModel (grmShader *shader=NULL );


	static RopeModel& Instance()
	{
		static RopeModel rope;
		return rope;
	}
	// PURPOSE
	//	Adds a rope lod which is activated at a certain distance and contains the 
	//	given number of segments for both length and width.
	// NOTES
	//	The lods should be added in ascending z depth, so therefore the higher detail lods
	//	at lower z values. The final model added is rendered to infinite depth.
	//
	void AddLod( float MaximumZValue,					// The maximum Z value for the lod to be rendered at
				int NumberSegmentsLength,				//	The number of segments along the length of the rope piece.
				int NumberSegmentsWidth,				//	The number of segments along the width of the rope pieace
				Vector2 uvScale = Vector2(1.0f, 1.0f)
				);

	float GetLODMaximumDepth(int i)
	{
		Assert(i>=0 && i<static_cast<int>(m_LodList.size()));
		return m_LodList[i]->GetMaximumDepth();
	}
	bool HasLods ()
	{
		return (m_LodList.size()>0);
	}
	void Destroy()
	{
		while (m_LodList.size() > 0)
		{
			delete m_LodList.back();
			m_LodList.Pop();
		}
	}

	void SetShader( grmShader* shader )
	{
		m_shader = shader;
		ASSERT_ONLY(bool res = )LookupCurvedModelShaderVar(*m_shader,m_modelShaderVar);
		Assert(res == true);
	}
	
	grmShader *GetShader()
	{
		return m_shader;
	}
	
	void SetUVScale(const Vector2 scale) { m_UVScale=scale; }


	// PURPOSE
	//	Selects the correct lod to use depending on the z value
	//
	u8	GetLodIndex( float z	// z depth distance
				);

	// PURPOSE
	//	Draws the rope into the scene.
	// NOTES
	//	Chooses the correct lod to draw and renders it.
	//
	void render(const Matrix34& cameraMtx, phVerletCloth* rope, const atFixedArray<Vector3,sm_MaxDrawVerts> *drawVerts, int numVerts, int lodIndex, float radius, int pass);
	
private:

	atArray<RopeLod*> m_LodList;

	grmShader*				m_shader;
	CurvedModelShaderVar	m_modelShaderVar;

	Vector2					m_UVScale;
	

	// PURPOSE
	//	Draws the rope using the curved model rendering.
	//
	void renderRope( const Matrix34& cameraMtx, phVerletCloth* rope, const atFixedArray<Vector3,sm_MaxDrawVerts> *drawVerts, int numVerts, TubeModel& model, float radius, const Vector2 &uvScale, int pass );
};



//
// PURPOSE
//  Class which interacts with FastCurveGen to create to curve of a specified rope object 
//
// SEE ALSO
// FastCurveGen, SpiralVertices, RopeVertices
//
class RopeVertices : public CurveGenerator
{
public:

	RopeVertices (const phVerletCloth* verlet, int firstEdgeIndex, int numEdges, const atFixedArray<Vector3,RopeModel::sm_MaxDrawVerts> *drawVerts)
	{
// TODO: revise rope batches ??
#if ROPE_USE_BRIDGE
		int vertCount = numEdges+1;
		m_VertPositions.Resize(vertCount);
		for( int i=0; i < vertCount; ++i )
		{
			m_VertPositions[i] = (*drawVerts)[i];
		}

#else
		if(!verlet)
			return;
		m_RopeVerletCloth = verlet;

		int vertCount = numEdges+1;
		if( vertCount > RopeModel::sm_MaxDrawVerts)
		{
			vertCount = RopeModel::sm_MaxDrawVerts;
		}
		m_VertexIndices.Resize(vertCount);
		m_VertPositions.Resize(vertCount);

		// Copy the list of vertices from the edges.
		const atArray<phEdgeData>& ropeEdges = m_RopeVerletCloth->GetEdgeList();
		int edgeIndex;
		for (edgeIndex=0; edgeIndex<(vertCount-1); ++edgeIndex)
		{
			m_VertexIndices[edgeIndex] = ropeEdges[edgeIndex+firstEdgeIndex].m_vertIndices[0];
		}

		// Get the last vertex on the last edge.
		m_VertexIndices[edgeIndex] = ropeEdges[edgeIndex+firstEdgeIndex-1].m_vertIndices[1];

		for (int i=0; i<vertCount; i++)
		{
			m_VertPositions[i] = (*drawVerts)[m_VertexIndices[i]];
		}
#endif
	}


	~RopeVertices()
	{
	}

	Vector3 operator() (int index)  const
	{
		return m_VertPositions[ index];
	}

	int NumberVertices ()   const
	{
		return m_VertPositions.GetCount();
	}

#if ROPE_USE_BRIDGE
	Vector3 GetLastPosition (int index)   const
	{
		return m_VertPositions[index];
	}
#else
	Vector3 GetLastPosition (int index)   const
	{
		Assert( m_RopeVerletCloth );
		return VEC3V_TO_VECTOR3(m_RopeVerletCloth->GetClothData().GetVertexPrevPosition(m_VertexIndices[index]));
	}
#endif

protected:
#if !ROPE_USE_BRIDGE
	const phVerletCloth* m_RopeVerletCloth;
	atFixedArray<int,RopeModel::sm_MaxDrawVerts> m_VertexIndices;
#endif
	atFixedArray<Vector3,RopeModel::sm_MaxDrawVerts> m_VertPositions;
};


//
//PURPOSE
//	Finds the bounding sphere of a rope piece of a given thickness
//
inline Vector4 CalculateRopeSphereBound (const phVerletCloth* rope, float ropeRadius)
{
	Assert( rope );
#if NO_BOUND_CENTER_RADIUS
	Vec3V center = rope->GetCenter();
	Vector4 centerAndRadius;
	centerAndRadius.SetVector3( VEC3V_TO_VECTOR3(center) );
	centerAndRadius.SetW( rope->GetRadius(center) );
#else
	Vector4 centerAndRadius = rope->GetBoundingCenterAndRadius();
#endif
	centerAndRadius.w += ropeRadius;
	return centerAndRadius;
}





//
// PURPOSE
//	 Interface to rendering rope rendering in game titles
//
class rmcRopeDrawable: public rmcDrawable 
{

public:
	rmcRopeDrawable( phVerletCloth* rope, float radius  = 0.015f, u32 bucketMask = 0x1) 
		:	m_rope( rope),
			m_bucketMaskToUse( bucketMask ),			
			m_radius( radius )
	{
		Assert( rope );
		m_DrawVerts.Resize(RopeModel::sm_MaxDrawVerts);
	}

	bool Load(const char *UNUSED_PARAM(basename),rmcTypeFileParser *UNUSED_PARAM(parser)=0,bool UNUSED_PARAM(configParser)=true)
	{
		Assert(0);
		return false;
	}
protected:
	// Internal helper function, should not be called directly by new code.
	bool Load(fiTokenizer & UNUSED_PARAM(T),rmcTypeFileParser *UNUSED_PARAM(parser)=0,bool UNUSED_PARAM(configParser)=true) 
	{ 
		Assert( 0 );
		return true;
	}
	// Internal helper function, should not be called directly by new code.
	bool LoadFromParser(fiTokenizer & UNUSED_PARAM(T),rmcTypeFileParser *UNUSED_PARAM(parser)=0,bool UNUSED_PARAM(configParser)=true) 
	{
		Assert( 0 );
		return true;
	}

public:
	void Draw(const Matrix34 &mtx,u32 bucketMask, int lod, u16 stats, int pass) const;

	virtual void Draw(const Matrix34 &mtx,u32 bucketMask, int lod, u16 stats) const;

	void DrawSkinned(const Matrix34 &,const grmMatrixSet &,u32,int, u16) const
	{
		Quitf(ERR_GFX_DRAW_ROPE_1,"don't call this!");
	}

	void DrawNoShaders(const Matrix34&,u32,int) const
	{
		return;  // eventually support this!
	}

	void DrawSkinnedNoShaders(const Matrix34 &,const grmMatrixSet &,u32,int,EnumNoShadersSkinned) const
	{
		Quitf(ERR_GFX_DRAW_ROPE_2,"don't call this!");
	}

	void SetRadius(float r) { m_radius=r; }
	float GetRadius() const { return m_radius; }

	__forceinline float GetDistToCamera2(const Matrix34 &cameraMtx) const
	{
		const Vector3 &v1 = m_DrawVerts[0];
		const Vector3 &v2 = m_DrawVerts[(m_DrawVerts.GetCount()-1)/2];
		const Vector3 &v3 = m_DrawVerts[m_DrawVerts.GetCount()-1];

		float dist2toCamera = Min(cameraMtx.d.Dist2(v1), cameraMtx.d.Dist2(v2));
		dist2toCamera = Min(dist2toCamera, cameraMtx.d.Dist2(v3));

		return dist2toCamera;
	}
	
	__forceinline float GetRopeSize2() const
	{
		const Vector3 &v1 = m_DrawVerts[0];
		const Vector3 &v2 = m_DrawVerts[(m_DrawVerts.GetCount()-1)/2];
		const Vector3 &v3 = m_DrawVerts[m_DrawVerts.GetCount()-1];

		float ropeSize2 = Max(v1.Dist2(v2),v1.Dist(v3));
		return ropeSize2;
	}
	
	int GetLodLevel(const Matrix34 &cameraMtx) const;
	
	virtual grcCullStatus IsVisible(const Matrix34 &UNUSED_PARAM(mtx), const grcViewport &vp, u8 &lod, float * UNUSED_PARAM(zdist)=NULL) const
	{
		return IsVisible( vp, lod );
	}

	virtual grcCullStatus IsVisible(const Matrix44 &UNUSED_PARAM(mtx), const grcViewport &vp, u8 &lod, float * UNUSED_PARAM(zdist)=NULL) const 
	{
		return IsVisible( vp, lod );
	}

	// RETURNS: Bitmask of all draw buckets referenced by all shaders in this drawable
	virtual u32 GetBucketMask(int UNUSED_PARAM(lod)) const
	{
		return m_bucketMaskToUse;;
	}

	const phVerletCloth*			GetRope()	const { return m_rope; }

	mutable atFixedArray<Vector3,RopeModel::sm_MaxDrawVerts> m_DrawVerts;


private:

	grcCullStatus IsVisible( const grcViewport &vp, u8 &lod ) const
	{
		Matrix34 mtxIdentity;
		mtxIdentity.Identity();
		grcWorldMtx( mtxIdentity);

		float outZ;
		grcCullStatus visible = IsVisibleWithZ( vp, outZ ); 
	
		// calculate lod to use for rope
		lod = RopeModel::Instance().GetLodIndex( outZ );
		return visible;
	}

	// PURPOSE: rope object to draw
	phVerletCloth* m_rope;

	u32			m_bucketMaskToUse;		// bucket mask to use 
	float		m_radius;

protected:
	grcCullStatus IsVisibleWithZ( const grcViewport &vp, float &outZ ) const
	{
		Vector4 bound = CalculateRopeSphereBound( m_rope , m_radius ); // radius
		//	grcDrawSphere( bound.w, Vector3( bound.x, bound.y, bound.z ) );

		// calculate visibility 
		// calculate lod to use for rope
		return vp.IsSphereVisible( bound.x, bound.y, bound.z, bound.w , &outZ );
	}

};

};

#endif
