Project manylights

IncludePath $(RAGE_DIR)\base\src

Files {
	simpleLightManager.cpp
	simpleLightManager.h
	lightgrid.cpp
	lightgrid.h
	lightshadercontrollers.h
	lightshadercontrollers.cpp
	lightfog.h
	lightfog.cpp	
}
Parse {
	simpleLightManager
}