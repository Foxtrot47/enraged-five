// 
// sample_lighting/simpleLightManager.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Note: creates a 2D screenspace texture that holds all shadow relevant data
//

#include "simpleLightManager.h"
#include "grcore/im.h"
#include "atl/creator.h"
#include <queue>
#include "algorithm"
#include "vectormath/legacyconvert.h"

#include "grcore/viewport.h"
using namespace rage;

#include "simpleLightManager_parser.h"


namespace rage
{



	template< class _T1, class _T2>
	struct CompareKeyPair
	{
		_T1	first;
		_T2 second;

		CompareKeyPair() {}

		CompareKeyPair( _T1 f, _T2 s ) : first( f), second( s )
		{}

		_T2 value()	const { return second; }
		_T1 Key() const { return first; }

	};
	template< class _T1, class _T2>
	bool operator<( const CompareKeyPair<_T1,_T2>& v1, const CompareKeyPair<_T1,_T2>& v2 )
	{
		return v1.first < v2.first;
	}
	typedef	CompareKeyPair< float, int>	KeyIndexPair;


	Vector4 GetBoundSpherePosition( const atArray<Light>& list )
	{
		if ( list.GetCount() == 0 )
		{
			return Vector4( 0.0f, 0.0f, 0.0f, 1.0f );
		}
		Vector4 centre = Vector4( list[0].GetPosition().x, list[0].GetPosition().y, list[0].GetPosition().z, 0.0f  );
		for ( int i =1 ; i < list.GetCount(); i++)
		{
			centre +=  Vector4( list[i].GetPosition().x, list[i].GetPosition().y, list[i].GetPosition().z, 0.0f  );
		}
		centre /= static_cast<float>( list.GetCount() );

		// then get the maximum distance to any of the point from the average
		float maxRadius = 0.001f;
		for ( int i = 0 ; i < list.GetCount(); i++)
		{
			float diff = centre.Dist3( Vector4( list[i].GetPosition().x, list[i].GetPosition().y, list[i].GetPosition().z, 0.0f  ) );
			maxRadius = Max( diff , maxRadius );
		}
		return Vector4( centre.x, centre.y, centre.z,  maxRadius );
	}

	void GetBoundBoxPosition( const atArray<Light>& list, Vector3& min, Vector3& max )
	{
		if ( !list.GetCount() )
		{
			return;
		}
		min = list[0].GetPosition();
		max = list[0].GetPosition();

		for ( int i= 1; i < list.GetCount(); i++ )
		{
			min.Min( min, list[i].GetPosition() );
			max.Max( max, list[i].GetPosition() );
		}
	}

	inline Vector3	ConvertEulersToVector( const Vector3& Direction )
	{
		Vector3 orientation = Direction *( (2.0f* 3.142f)/360.0f ); // convert to rads
		Matrix34 orientMtx;
		orientMtx.Identity();
		orientMtx.FromEulersXYZ( orientation );
		// spotlight Direction is along the negative z axias
		Vector3 DirectionVector = Vector3( 0.0f, 0.0f, -1.0f );
		orientMtx.Transform( DirectionVector);
		return DirectionVector;
	}
	inline float Luminance( Vector3& v )
	{
		Vector3 wieghts( 0.3f, 0.6f, 0.1f );
		return v.Dot( wieghts );
	}



float Light::sm_SphereOfInfluence = 0.7f;
void Light::Reset()
{
	name = "Unset";
	Type = 0;
	Position = Vector3( 0.0f, 0.0f, 0.0f );
	Colour = Vector3(  0.0f, 0.0f, 0.0f );
	Direction = Vector3( 0.0f, 90.0f, 0.0f );
	Intensity = 1.0 ;
	ConeAngle =  360.0f;
	Penumbra =  10.0f;
	DirectionVector = Vector3( 0.0f, -1.0f, 0.0f );
	ShadowRange = Vector2( 0.001f, 0.002f );
	ShadowBias = 0.01f;
	ShadowEplision =  0.000001f ;
	m_boundSphere =  Vector4(  0.0f, 0.0f, 0.0f , 0.0f);
	m_invCosConeAngle = 0.0f;
	ShadowSize = DefaultShadowSize;
	ShadowFilter = 5;
}
void Light::Update( int version )
{	
	// spotlight Direction is along the negative z axias
	DirectionVector = Vector3( 0.0f, 0.0f, -1.0f );

	if ( version <= 1 )
	{
		DirectionVector = ConvertEulersToVector( Direction );
	}
	else
	{
		DirectionVector = Direction;
	}
	DirectionVector.Normalize();


	if ( ShadowSize == 0 )
	{
		ShadowSize = DefaultShadowSize;
	}

	if ( Penumbra == 0.0f )
	{
		Penumbra = 1.0f;
	}
	if ( Type == POINT )
	{
		m_boundSphere.x = Position.x;
		m_boundSphere.y = Position.y;
		m_boundSphere.z = Position.z;
	//	m_boundSphere.w = Intensity / sm_SphereOfInfluence; //100.0f;//sqrtf( Intensity/5.0f );
		float ignoreScale = 1.0f /( 0.05f *  sm_SphereOfInfluence);
		m_boundSphere.w = sqrtf( Intensity * ignoreScale);

		Vector3	w( m_boundSphere.w, m_boundSphere.w, m_boundSphere.w );
		m_boundMin = Position - w * 0.9f;
		m_boundMax = Position + w * 0.9f;
	}
	else
	{
		// spotlight
		// calculate  hit sphere on ground
		float radAngles = (0.5f * ( ConeAngle + fabs(Penumbra) ) /360.0f) * 2.0f * 3.14f;
		m_boundSphere.w = Intensity / sm_SphereOfInfluence;

		float grdPlane = 0.0f;
		if ( DirectionVector.y > 0.0f)
		{
			grdPlane = 10.0f;		// maximum value
		}
		float hitDist =( grdPlane - Position.y )/ DirectionVector.y;

		if ( hitDist < 0.0f)
		{
			hitDist = 0.0f;
		}
		float radius = ( hitDist ) * tan( radAngles );

		Vector3 coneDir  = hitDist * DirectionVector;
		m_boundSphere.x = Position.x + coneDir.x;
		m_boundSphere.y = Position.y + coneDir.y;
		m_boundSphere.z = Position.z + coneDir.z;

		float coneDirRad = sqrt( coneDir.x *  coneDir.x  + coneDir.z *  coneDir.z ); 
		radius = Max( coneDirRad + radius/2.0f, radius );

		if (( fabs(Penumbra) + ConeAngle ) < 200.0f || (DirectionVector.y > 0.0f) )
		{
			m_boundSphere.w = Min( m_boundSphere.w , radius );
		}
		if ( 1 || DirectionVector.y > -0.99999f)
		{
			CalculateSpotLightBound( *this, sm_SphereOfInfluence, m_boundMin, m_boundMax);
		}
		else
		{
			Vector3	w( m_boundSphere.w, m_boundSphere.w, m_boundSphere.w);
			m_boundMin = Position - w;
			m_boundMax = Position + w;
		}


	}

}
// PURPOSE : Calculates a scale and bias so spotlight attenuation can be calculated in one mad.
//
// NOTES: cone angle is the inner angle of a spotlight, and penumbra is the number of degrees the penumbra is
//
void lgtCalculateSpotLightScaleBias( float coneRadAngle, float penumbraRadAngle , float& scale, float& bias )
{


	scale = 1.0f/( cos( penumbraRadAngle + coneRadAngle) - cos( coneRadAngle  ) );
	bias = -cos( coneRadAngle  ) * scale;

	// now create inverse 1 - ( scale + bias ) = -scale + ( -bias + 1.0f );
	scale = -scale;
	bias = -bias + 1.0f;
}

void Light::CalculateSpotScaleBias( float& scale, float& bias ) const
{

	// dotVal * scale + bias;  1.0 -  ( dot * Penumbra  - (Cone) ) == 
	if ( Type != SPOT )
	{
		bias = 1.0f;
		scale = 0.0f;
		return;
	}
	float coneRadAngle = (0.5f * ( ConeAngle )/360.0f) * 2.0f * 3.14f;
	float penumbraRadAngle = (0.5f * ( Penumbra )/360.0f) * 2.0f * 3.14f;

	lgtCalculateSpotLightScaleBias( coneRadAngle, penumbraRadAngle, scale, bias );
}




// -------------------------------- lightlist -------------------------------------------------
void LightList::CompactLights( LightList& list, float compactDistance, float colorDelta )
{
	Assert( list.lights.GetCount() !=  0 ); // TODO - do this properally
	Clear();

	bool	removed[256] = {false};

	for ( int i = 0; i < list.lights.GetCount(); i++ )
	{
		if ( !removed[i] )
		{
			Light newLight =list.lights[i]; 

			for ( int j = i + 1; j< list.lights.GetCount(); j++ )
			{
				if ( newLight.IsClose( list.lights[j], compactDistance, colorDelta ))
				{
					// remove it as one the same is there
					removed[j] = true;
					newLight.Merge( list.lights[j], Version );
				}
			}
			if (  GetVersion() < 2 || newLight.CastsLight() )		// it's not an effect light
			{
				lights.Grow() = newLight;
			}
		}
	}
	fillLights = list.fillLights;
	CamPosition = list.CamPosition;
	CamDirection = list.CamDirection;
	DirectionalColor = list.DirectionalColor;
	DirectionalDirection = list.DirectionalDirection;
	Version = list.Version;
}
void LightList::GetStrongestNLights( int* results, const Vector3& point ,int N  ) const
{
	std::priority_queue< KeyIndexPair >	pqueue;

	for ( int i = 0; i < lights.GetCount(); i++ )
	{
		float lightIntensity = lights[i].GetLightIntensityOnPoint( point );
		pqueue.push( KeyIndexPair( lightIntensity , i ) );
	}
	int NumValues = Min( N, lights.GetCount() );
	for ( int i = 0; i < NumValues ; i++ )
	{
		KeyIndexPair	res = pqueue.top();
		results[i] = res.value();
		pqueue.pop();
	}
	for ( int i = NumValues; i < N  ; i++ )	// set the result to the black light
	{
		results[i] = lights.GetCount();
	}
}
Vector3 LightCalculateIncidentLighting( const atArray<Light>& lights, int* indices, int amt, const Vector3& point )
{
	Vector3 col( 0.0f, 0.0f, 0.0f );

	for ( int i = 0; i < amt; i++ )
	{
		if (indices[i ] < lights.GetCount() )
		{
			col +=  lights[ indices[i ] ].LightPoint(  point );
		}
	}
	return col;
}
void LightGetStrongestNLightsInBox( const atArray<Light>& lights, int* results, const Vector3& point ,int N  ,const Vector3& min, const Vector3& max ) 
{
	/*KeyIndexPair  keyVal[32];

	if ( lights.GetCount() > N )
	{
		int count = 0;
		for ( int i = 0; i < lights.GetCount(); i++ )
		{
			if ( lights[i].InBoundXZ(  min, max ) )
			{
				keyVal[count++] = KeyIndexPair( lights[i].GetLightIntensityOnPoint( point ), i);
			}
		}
		if ( count > N )
		{
			std::partial_sort( &keyVal[0], &keyVal[N], &keyVal[count] );
		}
					
		int NumValues = Min( N, count );
		for ( int i = 0; i < NumValues  ; i++ )	// set the result to the black light
		{
			results[i] =  keyVal[i].value();
		}	
		for ( int i = NumValues; i < N  ; i++ )	// set the result to the black light
		{
			results[i] = lights.GetCount();
		}
	}
	else
	{
		for ( int i = 0; i < lights.GetCount()  ; i++ )	// set the result to the black light
		{
			results[i] =  i;
		}	
		for ( int i = lights.GetCount(); i < N  ; i++ )	// set the result to the black light
		{
			results[i] = lights.GetCount();
		}
	}
	*/
	std::priority_queue<KeyIndexPair> pqueue;

	int count = 0;
	for ( int i = 0; i < lights.GetCount(); i++ )
	{
		if ( lights[i].InBoundXZ(  min, max ) )
		{
			float lightIntensity = lights[i].GetLightIntensityOnPoint( point );
			pqueue.push( KeyIndexPair( lightIntensity , i ) );
			count++;
		}
	}
	int NumValues = Min( N, count );
	for ( int i = 0; i < NumValues ; i++ )
	{
		KeyIndexPair	res = pqueue.top();
		results[i] = res.value();
		pqueue.pop();
	}
	for ( int i = NumValues; i < N  ; i++ )	// set the result to the black light
	{
		results[i] = lights.GetCount();
	}
}
void LightList::Cull( grcViewport* view )
{
	lights.clear();
	for ( int i = 0; i < AllLights.GetCount(); i++ )
	{
		if ( view->IsSphereVisible( VECTOR4_TO_VEC4V(AllLights[i].GetSphere() )) )
		{
			lights.Grow() = AllLights[i];
		}
	}
}
void LightList::Draw( bool showSpheres, bool showAABBs, bool showPos)
{
	for ( int i =0; i < lights.GetCount(); i++ )
	{
		Vector4 pos;
		pos.SetVector3( lights[i].GetPosition());
		pos.w = 0.1f;

		if ( showPos ) 	{ DrawSphere( pos, Color32( 0.0f, 1.0f, 0.0f, 1.0f )); }
		if ( showSpheres) DrawSphere( lights[i].GetSphere() , Color32( 1.0f, 0.0f, 0.0f, 1.0f ));
		if ( showAABBs)   DrawAABB( lights[i].GetMin(), lights[i].GetMax(), Color32( 1.0f,  1.0f, 0.0f, 1.0f ) );
	}
}
void DrawSphere( const Vector4& sphere , Color32 colour )
{
	Vector3	centre( sphere.x, sphere.y, sphere.z );

	grcColor( colour );
	grcDrawSphere( sphere.w, centre , 16, true );
}

void DrawAABB( const Vector3& min, const Vector3& max , Color32 color )
{
	Vector3 size = max - min;
	Matrix34 pos;
	pos.Identity();
	pos.Translate( min + size  * 0.5f );
	grcDrawBox( size, pos,  color);
}
void CalculateSpotLightBound( const Light& light, float clipIntensity, Vector3& min, Vector3& max )
{
	Vector3 col = Vector3( light.Colour.x, light.Colour.y, light.Colour.z );
	float maxDist = sqrt( ( light.Intensity * Min( Luminance( col ), 1.0f) )  / clipIntensity );


	Vector3 center = light.Position + maxDist * light.DirectionVector;
	float width = light.CalculateTanAngle() * maxDist;

	float grdPlane = 0;
	float hitDist =( grdPlane - light.Position.y )/ light.DirectionVector.y;

	if (0 &&( hitDist < maxDist && hitDist > 0.0f ))  // have hit the ground
	{
		hitDist += light.CalculateTanAngle() * hitDist;
		maxDist = Min( maxDist, hitDist );
		center = light.Position + maxDist * light.DirectionVector;
		width = light.CalculateTanAngle() * maxDist;
	}

	Vector3 widthW = Vector3( width, width, width );

	min = center - widthW;
	max = center + widthW;

	min.Min( light.Position, min );  // add the light position
	max.Max( light.Position, max );
}


void LightList::GetDirectional( Vector3& dir, Vector3& col )
{
	dir = Version <=1 ?  ConvertEulersToVector( DirectionalDirection ) : DirectionalDirection;
	col = DirectionalColor;
}
Matrix34 LightList::GetCamera()
{
	Matrix34	cameraMtx;
	Vector3 dir = ConvertEulersToVector( CamDirection );
	cameraMtx.LookAt( CamPosition, CamPosition + dir * 500.0f, YAXIS );
	return cameraMtx;
}


}
