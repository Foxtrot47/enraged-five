// 
// manylights/lightshadercontrollers.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "lightshadercontrollers.h"
#include "simpleLightManager.h"
//#include "grshadowmap/spotlightshadowmap.h"
#include "lightgrid.h"

using namespace rage;




void FastSpot4Lighting::SetupShader( grmShader& effect, VarCache* cVars ) 
{
	Assert( cVars );
	FastSpot4LightingVarCache* var = reinterpret_cast<FastSpot4LightingVarCache*>(cVars);

	if ( !m_setup )
	{
		return;
	}
	effect.SetVar( var->m_colorRID, m_colR, m_amtLights );
	effect.SetVar( var->m_colorGID, m_colG, m_amtLights  );
	effect.SetVar( var->m_colorBID, m_colB, m_amtLights  );

	effect.SetVar( var->m_intensityID, m_oneOverRangeSq, m_amtLights );
	//		effect.SetVar( var->m_amtID, m_amtLights );

	effect.SetVar( var->m_positionXID, m_positionX, m_amtLights );
	effect.SetVar( var->m_positionYID, m_positionY, m_amtLights  );
	effect.SetVar( var->m_positionZID, m_positionZ, m_amtLights  ); 

	effect.SetVar( var->m_DirectionID, m_direction, 3 * m_amtLights);
	effect.SetVar( var->m_FadeScaleID, m_spotScale,  m_amtLights);
	effect.SetVar( var->m_FadeOffsetID, m_spotOffset,  m_amtLights);
	effect.SetVar( var->m_DropOffID, m_spotDropOff, m_amtLights );

	effect.SetVar( var->m_ShadowMatrix1ID, m_ShadowMtx, 8);
	effect.SetVar( var->m_ShadowTexture1ID, m_ShadowTex[0]);
	effect.SetVar( var->m_ShadowTexture2ID, m_ShadowTex[1]);
	effect.SetVar( var->m_ShadowTexture3ID, m_ShadowTex[2]);
	effect.SetVar( var->m_ShadowTexture4ID, m_ShadowTex[3]);

	effect.SetVar( var->m_ShadowTexture5ID, m_ShadowTex[4]);
	effect.SetVar( var->m_ShadowTexture6ID, m_ShadowTex[5]);
	effect.SetVar( var->m_ShadowTexture7ID, m_ShadowTex[6]);
	effect.SetVar( var->m_ShadowTexture8ID, m_ShadowTex[7]);

	effect.SetVar( var->m_ShadowAttenuationID, m_ShadowAttenutation, Max_Fast_Lights);
	effect.SetVar( var->m_ShadowVSMEplisionID, m_ShadowVSMEplision, Max_Fast_Lights);
	effect.SetVar( var->m_ShadowDepthBiasID, m_ShadowDepthBias, Max_Fast_Lights);

	effect.SetVar( var->m_AmbientOcclusionFadeInID, m_AmbientOcclusionFadeIn );
	effect.SetVar( var->m_MaxShadowFadeID, m_MaxShadowFade );
}

void FastSpot4Lighting::FastSpot4LightingVarCache::CacheEffectVars( grmShader& effect )
{

	m_positionXID = effect.LookupVar( "Light4PosX" );
	m_positionYID = effect.LookupVar( "Light4PosY" );
	m_positionZID = effect.LookupVar( "Light4PosZ" );


	m_colorRID = effect.LookupVar( "Light4ColR" );
	m_colorGID = effect.LookupVar( "Light4ColG" );
	m_colorBID = effect.LookupVar( "Light4ColB" );  

	m_intensityID = effect.LookupVar( "Light4OneOverRange" ); 

	m_DirectionID = effect.LookupVar( "FastSpotDirection" );
	m_FadeScaleID = effect.LookupVar( "FastSpotFadeScale" );
	m_FadeOffsetID = effect.LookupVar( "FastSpotFadeOffset" );  
	m_DropOffID = effect.LookupVar( "FastSpotDropOff" );

	m_ShadowMatrix1ID = effect.LookupVar( "ShadowMatrix" );
	m_ShadowTexture1ID = effect.LookupVar( "Shadow1Texture" );
	m_ShadowTexture2ID = effect.LookupVar( "Shadow2Texture" );
	m_ShadowTexture3ID = effect.LookupVar( "Shadow3Texture" );
	m_ShadowTexture4ID = effect.LookupVar( "Shadow4Texture" );
	m_ShadowTexture5ID = effect.LookupVar( "Shadow5Texture" );
	m_ShadowTexture6ID = effect.LookupVar( "Shadow6Texture" );
	m_ShadowTexture7ID = effect.LookupVar( "Shadow7Texture" );
	m_ShadowTexture8ID = effect.LookupVar( "Shadow8Texture" );


	m_ShadowAttenuationID = effect.LookupVar( "gOneOverLightAttenuationEndx4" );
	m_ShadowVSMEplisionID = effect.LookupVar( "LightVSMEpsilonx4" );
	m_ShadowDepthBiasID = effect.LookupVar( "LightVSMBiasx4" );

	m_AmbientOcclusionFadeInID = effect.LookupVar( "AmbientOcclusionFadeIn");
	m_MaxShadowFadeID = effect.LookupVar("MaxShadowFade");

	//m_amtID = effect.LookupVar( "FastLightNumBatches" );
}

void FastSpot4Lighting::SetLightsFast4( atArray<Light>& list, float AmbOccFadeIn  ) 
{ 
	//Assert( ( list.GetCount() & 0x3 )== 0 );
	m_amtLights = Max_Fast_Lights;

	int loopMax = list.GetCount() >> 2;
	for ( int i = 0, lidx = 0; i < loopMax; i++, lidx +=4 )
	{
		m_positionX[i] = Vector4( list[lidx].Position.x, list[lidx + 1].Position.x, list[lidx + 2].Position.x, list[lidx + 3].Position.x );
		m_positionY[i] = Vector4( list[lidx].Position.y, list[lidx + 1].Position.y, list[lidx + 2].Position.y, list[lidx + 3].Position.y );
		m_positionZ[i] = Vector4( list[lidx].Position.z, list[lidx + 1].Position.z, list[lidx + 2].Position.z, list[lidx + 3].Position.z );

		m_colR[i] = Vector4( list[lidx].Colour.x, list[lidx + 1].Colour.x, list[lidx + 2].Colour.x, list[lidx + 3].Colour.x );
		m_colG[i] = Vector4( list[lidx].Colour.y, list[lidx + 1].Colour.y, list[lidx + 2].Colour.y, list[lidx + 3].Colour.y );
		m_colB[i] = Vector4( list[lidx].Colour.z, list[lidx + 1].Colour.z, list[lidx + 2].Colour.z, list[lidx + 3].Colour.z );

		m_direction[ i * 3 + 0]  = Vector4( list[lidx].DirectionVector.x, list[lidx + 1].DirectionVector.x, list[lidx + 2].DirectionVector.x, list[lidx + 3].DirectionVector.x );
		m_direction[ i * 3 + 1]  = Vector4( list[lidx].DirectionVector.y, list[lidx + 1].DirectionVector.y, list[lidx + 2].DirectionVector.y, list[lidx + 3].DirectionVector.y );
		m_direction[ i * 3 + 2]  = Vector4( list[lidx].DirectionVector.z, list[lidx + 1].DirectionVector.z, list[lidx + 2].DirectionVector.z, list[lidx + 3].DirectionVector.z );


		list[lidx + 0].CalculateSpotScaleBias( m_spotScale[i].x, m_spotOffset[i].x );
		list[lidx + 1].CalculateSpotScaleBias( m_spotScale[i].y, m_spotOffset[i].y );
		list[lidx + 2].CalculateSpotScaleBias( m_spotScale[i].z, m_spotOffset[i].z );
		list[lidx + 3].CalculateSpotScaleBias( m_spotScale[i].w, m_spotOffset[i].w );

		m_spotDropOff[i] = Vector4( list[ lidx].DropOff, list[ lidx + 1 ].DropOff, list[ lidx + 2 ].DropOff, list[ lidx  + 3 ].DropOff );

		m_oneOverRangeSq[i] = Vector4( list[lidx].Intensity, list[lidx + 1].Intensity, list[lidx + 2].Intensity, list[lidx + 3].Intensity);
		m_oneOverRangeSq[i].InvertSafe();
	}

	for ( int i = loopMax; i < Max_Fast_Lights; i++ )
	{
		m_positionX[i] = m_positionY[i]  = m_positionZ[i] = Vector4( 100000.0f, 1000000000.0f, 1000000.0f, 1000000.0f );
		m_colR[i] = m_colG[i] = m_colB[i] = Vector4( 1.0f, 1.0f, 1.0f, 1.0f );
		m_oneOverRangeSq[i] = Vector4( 1.0f, 1.0f, 1.0f, 1.0f );
	}

	m_AmbientOcclusionFadeIn.x = AmbOccFadeIn;
	m_AmbientOcclusionFadeIn.y = 1.0f - AmbOccFadeIn;
}


template<class SHADOWCACHE>
void FastSpot4Lighting::SetShadows( SHADOWCACHE& shadows, atArray<Light>& list )
{ 
	int loopMax = Min( list.GetCount() >> 2, Max_Fast_Lights );

	for ( int i = 0, lidx = 0; i < loopMax; i++, lidx +=4 )
	{

		for ( int j =0; j < 4; j++ )
		{
			m_ShadowMtx[ lidx + j ] = shadows[ lidx +j].GetMatrix();
			m_ShadowTex[ lidx + j] = shadows.GetTexture(); //shadows[ lidx +j].GetTexture();
		}
		m_ShadowAttenutation[i].x = 1.0f/shadows[lidx +0].GetRange();
		m_ShadowAttenutation[i].y = 1.0f/shadows[lidx +1].GetRange();
		m_ShadowAttenutation[i].z = 1.0f/shadows[lidx +2].GetRange();
		m_ShadowAttenutation[i].w = 1.0f/shadows[lidx +3].GetRange();

		m_ShadowDepthBias[i].x = m_ShadowAttenutation[i].x * shadows[lidx +0].GetStart() + list[lidx +0].GetShadowBias();
		m_ShadowDepthBias[i].y = m_ShadowAttenutation[i].y * shadows[lidx +1].GetStart() + list[lidx +1].GetShadowBias();
		m_ShadowDepthBias[i].z = m_ShadowAttenutation[i].z * shadows[lidx +2].GetStart() + list[lidx +2].GetShadowBias();
		m_ShadowDepthBias[i].w = m_ShadowAttenutation[i].w * shadows[lidx +3].GetStart() + list[lidx +3].GetShadowBias();

		m_ShadowDepthBias[i] = -m_ShadowDepthBias[i];
		m_ShadowVSMEplision[i] = Vector4( list[lidx +0].GetShadowEplision(), list[lidx +1].GetShadowEplision(), list[lidx +2].GetShadowEplision(), list[lidx +3].GetShadowEplision());
	}

	m_MaxShadowFade = shadows.GetSpotShadowMaxFade();
	m_setup = true;
}


void FastLighting::SetSortTexture( const grcTexture*	tex, const grcTexture* tex2, const grcTexture* sortedFillTexture, const Vector3& min, const Vector3& max )	
{ 
	m_sortScale = ( max - min );
	m_sortScale.InvertSafe();
	m_sortBias = -min * m_sortScale;
	m_sortedTexture = tex; 
	m_sortedTexture2 = tex2;
	m_sortedFillTexture = sortedFillTexture;
}
void FastLighting::SetPositionalTexture(  const grcTexture* position, const grcTexture* colors, const grcTexture* cellDividerTexture,  const grcTexture* position2, const grcTexture* colors2 )
{
	m_posTexture = position;
	m_colTexture = colors;
	m_posTexture2 = position2;
	m_colTexture2 = colors2;
	m_cellDividerTexture = cellDividerTexture;	
}

void FastLighting::SetupShader( grmShader& effect, VarCache* cVars ) 
{
	Assert( cVars );
	FastLightingVarCache* var = reinterpret_cast<FastLightingVarCache*>(cVars);


	effect.SetVar( var->m_positionID, vals[0].m_position, vals[0].m_amtNormalLights );
	effect.SetVar( var->m_colorID,  vals[0].m_color, vals[0].m_amtNormalLights );
#if !USE_COMPRESSED_DIRECTION
	effect.SetVar( var->m_directionID, vals[0].m_directions, vals[0].m_amtNormalLights );
#endif


	/*effect.SetVar( var->m_positionFillID, vals[1].m_position, vals[1].m_amtNormalLights );
	effect.SetVar( var->m_colorFillID,  vals[1].m_color, vals[1].m_amtNormalLights );
	effect.SetVar( var->m_directionFillID, vals[1].m_directions, vals[1].m_amtNormalLights );
	effect.SetVar( var->m_sortFillTextureID, m_sortedFillTexture ); */

	effect.SetVar( var->m_sortTextureID, m_sortedTexture );
	effect.SetVar( var->m_sortTexture2ID, m_sortedTexture2 );
	effect.SetVar( var->m_sortScaleID, m_sortScale );
	effect.SetVar( var->m_sortBiasID, m_sortBias );

	effect.SetVar( var->m_posTextureID, m_posTexture );
	effect.SetVar( var->m_posScaleID, m_posScale );
	effect.SetVar( var->m_posBiasID, m_posBias );
	effect.SetVar( var->m_colTextureID, m_colTexture );

}


//---------------------------------- fast lighting -------------------------
void FastLighting::SetGlobals()
{
	if ( ! m_GVars.IsSet() )
	{
		m_GVars.CacheEffectVars();
	}
	grcEffect::SetGlobalVar( m_GVars.m_positionID, (Vec4V*)vals[0].m_position, vals[0].m_amtNormalLights );
	grcEffect::SetGlobalVar( m_GVars.m_colorID,  (Vec4V*)vals[0].m_color, vals[0].m_amtNormalLights );

#if !USE_COMPRESSED_DIRECTION
	grcEffect::SetGlobalVar(m_GVars.m_directionID, vals[0].m_directions, vals[0].m_amtNormalLights );
#endif
	grcEffect::SetGlobalVar( m_GVars.m_sortTextureID, m_sortedTexture );
	grcEffect::SetGlobalVar( m_GVars.m_sortScaleID, VECTOR3_TO_VEC3V(m_sortScale) );
	grcEffect::SetGlobalVar(m_GVars.m_sortBiasID, VECTOR3_TO_VEC3V(m_sortBias) );

	//grcEffect::SetGlobalVar(m_GVars.m_attenID, vals[0].m_attenuation, vals[0].m_amtNormalLights );

	grcEffect::SetGlobalVar( m_GVars.m_posScaleID, VECTOR3_TO_VEC3V(m_posScale) );
	grcEffect::SetGlobalVar( m_GVars.m_posBiasID, VECTOR3_TO_VEC3V(m_posBias) );
	grcEffect::SetGlobalVar( m_GVars.m_cellDivisionsTextureID, m_cellDividerTexture );
	grcEffect::SetGlobalVar( m_GVars.m_colTextureID, m_colTexture );
	grcEffect::SetGlobalVar( m_GVars.m_posTextureID, m_posTexture );

	grcEffect::SetGlobalVar( m_GVars.m_colTexture2ID, m_colTexture2 );
	grcEffect::SetGlobalVar( m_GVars.m_posTexture2ID, m_posTexture2 );

	grcEffect::SetGlobalVar( m_GVars.m_heightTextureID, m_heightMap );
	grcEffect::SetGlobalVar( m_GVars.m_envMapTextureID, m_envMap );

	m_posTexture  = 0;
}

void FastLighting::FastLightingVarCache::CacheEffectVars( grmShader& effect )
{
	m_positionID = effect.LookupVar( "LightPositions" );
	m_colorID = effect.LookupVar("LightColors");
	m_directionID = effect.LookupVar( "LightDirections" );


	m_positionFillID = effect.LookupVar( "FillLightPositions", false  );
	m_colorFillID = effect.LookupVar("FillLightColors", false  );
	m_directionFillID = effect.LookupVar( "FillLightDirections" , false  );
	m_sortFillTextureID = effect.LookupVar( "FillLightSortTexture" , false  );

	m_sortTextureID = effect.LookupVar ("LightSortTexture" , false  );
	m_sortTexture2ID = effect.LookupVar ("LightSortTexture2" , false  );
	
	m_sortScaleID = effect.LookupVar("LightSortScale", false  );
	m_sortBiasID = effect.LookupVar("LightSortBias", false  );

	m_posTextureID = effect.LookupVar("PositionTexture", false  );
	m_posBiasID = effect.LookupVar("PositionBias", false  );
	m_posScaleID = effect.LookupVar("PositionScale", false  );
	m_colTextureID = effect.LookupVar("ColorT" , false  );

}

void FastLighting::SetLights( atArray<Light>& list , bool isDirect )
{
#if USE_COMPRESSED_DIRECTION
	const bool UseCompressedDirection = true;
#else
	const bool UseCompressedDirection = true;
#endif


	LightVals* lgt = &(vals[0]);
	if ( !isDirect )
	{
		lgt = &(vals[1]);
	}
	for ( int i = 0; i < list.GetCount(); i++ )
	{

		lgt->m_position[i].SetVector3( list[i].Position );
		if ( !UseCompressedDirection )
		{
			lgt->m_color[i].SetVector3( list[i].Colour );
			lgt->m_color[i].w = list[i].Intensity;
			lgt->m_directions[i].SetVector3(list[i].DirectionVector );
			lgt->m_directions[i].w = list[i].CalculateConeCosineAngle();
		}
		else
		{
			Vector4 color;
			color.SetVector3( list[i].Colour * 255.0f );
			color.RoundToNearestInt();

			color.AddVector3( list[i].DirectionVector * 0.5f + Vector3( 0.5f, 0.5f, 0.5f) );
			color.w = list[i].Intensity;
			lgt->m_color[i] = color;
		}
		lgt->m_attenuation[i]= list[i].GetMaxStyleAttenuation();

		list[i].CalculateSpotScaleBias( lgt->m_directions[i].w , lgt->m_position[i].w);
	}

	// use black light as null light
	lgt->m_position[ list.GetCount()] = Vector4( 1000.0f, 1000000.0f, 10000.0f , 0.0f);
	lgt->m_color[ list.GetCount() ] = Vector4( 0.0f, 0.0f, 1.0f, 1.0f );
	lgt->m_directions[ list.GetCount() ] = Vector4( 0.0f, -1.0f, 0.0f, 0.0f );
	lgt->m_amtNormalLights = list.GetCount() + 1;

	if ( isDirect )
	{

		GetLightPositionBoundsScaleUp( list, m_posScale, m_posBias );

	}
}
void FastLighting::SetLightsFast4( atArray<Light>& list  ) 
{ 
	//Assert( ( list.GetCount() & 0x3 )== 0 );
	m_amtLights = Max_Fast_Lights;

	int loopMax = list.GetCount() >> 2;
	for ( int i = 0, lidx = 0; i < loopMax; i++, lidx +=4 )
	{
		m_positionX[i] = Vector4( list[lidx].Position.x, list[lidx + 1].Position.x, list[lidx + 2].Position.x, list[lidx + 3].Position.x );
		m_positionY[i] = Vector4( list[lidx].Position.y, list[lidx + 1].Position.y, list[lidx + 2].Position.y, list[lidx + 3].Position.y );
		m_positionZ[i] = Vector4( list[lidx].Position.z, list[lidx + 1].Position.z, list[lidx + 2].Position.z, list[lidx + 3].Position.z );

		m_colR[i] = Vector4( list[lidx].Colour.x, list[lidx + 1].Colour.x, list[lidx + 2].Colour.x, list[lidx + 3].Colour.x );
		m_colG[i] = Vector4( list[lidx].Colour.y, list[lidx + 1].Colour.y, list[lidx + 2].Colour.y, list[lidx + 3].Colour.y );
		m_colB[i] = Vector4( list[lidx].Colour.z, list[lidx + 1].Colour.z, list[lidx + 2].Colour.z, list[lidx + 3].Colour.z );

		m_oneOverRangeSq[i] = Vector4( list[lidx].Intensity, list[lidx + 1].Intensity, list[lidx + 2].Intensity, list[lidx + 3].Intensity);
		m_oneOverRangeSq[i].InvertSafe();
	}

	for ( int i = loopMax; i < Max_Fast_Lights; i++ )
	{
		m_positionX[i] = m_positionY[i]  = m_positionZ[i] = Vector4( 100000.0f, 1000000000.0f, 1000000.0f, 1000000.0f );
		m_colR[i] = m_colG[i] = m_colB[i] = Vector4( 1.0f, 1.0f, 1.0f, 1.0f );
		m_oneOverRangeSq[i] = Vector4( 1.0f, 1.0f, 1.0f, 1.0f );
	}
}
void ManyShadows::SetGlobals()
{
	if ( !m_amtLights )
	{
		return;
	}
	if (!m_GVars.IsSet() )
	{
		m_GVars.CacheEffectVars();
	}
	grcEffect::SetGlobalVar( m_GVars.ShadowMatrixXID, (Vec4V*)ShadowMatrixX, m_amtLights );
	grcEffect::SetGlobalVar( m_GVars.ShadowMatrixYID,  (Vec4V*)ShadowMatrixY, m_amtLights );
	grcEffect::SetGlobalVar( m_GVars.ShadowSettingsID, (Vec4V*)ShadowSettings, m_amtLights );
	grcEffect::SetGlobalVar( m_GVars.ShadowTextureID, m_shadowTexture );
	Vec2V sFade( m_shadowFade.x, m_shadowFade.y);
	grcEffect::SetGlobalVar( m_GVars.m_MaxShadowFadeID, sFade );

}
void ManyShadows::SetupShader( grmShader& effect, VarCache* cVars ) 
{
	Assert( cVars );
	ManyShadowsVarCache* var = reinterpret_cast<ManyShadowsVarCache*>(cVars);

	effect.SetVar( var->ShadowMatrixXID, ShadowMatrixX, m_amtLights );
	effect.SetVar( var->ShadowMatrixYID,  ShadowMatrixY, m_amtLights );
	//effect.SetVar( var->ShadowMatrixWID, ShadowMatrixW, m_amtLights );
	effect.SetVar( var->ShadowSettingsID, ShadowSettings, m_amtLights );
	//effect.SetVar( var->ShadowMatCompXID ,ShadowMatCompX, m_amtLights );
	effect.SetVar( var->ShadowTextureID, m_shadowTexture );
	effect.SetVar( var->ShadowTexScaleID, m_shadScale );
	effect.SetVar( var->m_MaxShadowFadeID, m_shadowFade );
	effect.SetVar( var->m_AmbientOcclusionFadeInID, m_AmbientOcclusionFadeIn );
	effect.SetVar( var->m_BumpinessID, m_bump );

	effect.SetVar( var->m_DirectionDirID, m_dir );
	effect.SetVar( var->m_DirectionColID, m_dirCol );

}
