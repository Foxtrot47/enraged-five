// 
// manylights/lightfog.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "lightfog.h"
#include "vector/matrix44.h"
#include "grcore/viewport.h"
//#include "grcore/viewport_inline.h"

using namespace rage;

float Luminance( Vector3& v )
{
	Vector3 wieghts( 0.3f, 0.6f, 0.1f );
	return v.Dot( wieghts );
}
Matrix44  CreateSpotLightMatrix( const Light& light, float length )
{
	Vector3 tangent( 1.0f, 0.0f, 0.0f );
	Vector3 binormal;

	binormal.Cross( light.DirectionVector, tangent );
	binormal.NormalizeFast();
	tangent.Cross( binormal, light.DirectionVector );
	tangent.NormalizeFast();

	float width = length * light.CalculateTanAngle();
	Matrix34 res;
	res.a = binormal* width;
	res.b = tangent * width;
	res.c = light.DirectionVector * length;
	res.d = light.GetPosition();

	Matrix44 res44;
	res44.FromMatrix34( res );
	return res44;
}

SpotLightFogRenderer::SpotLightFogRenderer(grmShader *shader) 
	: m_cone( 8, 32 , true )
	, m_Shader(shader)
	, m_clipIntensity( 0.1f )
	, m_technique( grcetNONE )
	, m_minClip( 0.1f), m_maxClip( 16.0f )
{
	Assert( shader );
	m_PositionVar	= shader->LookupVar("LightPosition" );
	m_ColorVar		= shader->LookupVar("LightColor" );
	m_DirectionVar	= shader->LookupVar("LightDirection" );
	m_VolumeVar		= shader->LookupVar("LightVolume" );
	m_MtxVar		= shader->LookupVar("LightMatrix" );
	m_InvMtxVar		= shader->LookupVar("LightInvMatrix" );
}

void SpotLightFogRenderer::SetTechnique( const char* tech  )
{
	Assert( m_Shader );
	m_technique = m_Shader->LookupTechnique( tech, false);
}
void SpotLightFogRenderer::Set( Light& light, float volumeIntensity, float trans )
{
	Vector4 position( light.Position.x, light.Position.y, light.Position.z , 0.0f);
	Vector4 color( light.Colour.x, light.Colour.y, light.Colour.z, light.Intensity );
	Vector4 direction( light.DirectionVector.x, light.DirectionVector.y, light.DirectionVector.z , 0.0f);
	light.CalculateSpotScaleBias( direction.w, position.w );

	Vector4 volume( volumeIntensity, trans, 0.0f, 0.0f  );

	float clipIntensity = m_clipIntensity;
	Vector3 col = Vector3( light.Colour.x, light.Colour.y, light.Colour.z );
	float maxDist = sqrt( ( light.Intensity  *Luminance( col ) )   / clipIntensity );

	Vector4 offset( m_maxClip, m_minClip, 0.0f, 0.0f );
	offset.x = Min( offset.x, maxDist );
	volume.z = offset.y /offset.x;

	Matrix44	spotMtx;
	spotMtx.Identity();
	if ( offset.x > 0.0f)
	{
		spotMtx = CreateSpotLightMatrix( light, offset.x  ) ;
	}

	Assert( m_Shader );
	m_Shader->SetVar( m_MtxVar, spotMtx );

	spotMtx.Inverse();
	m_Shader->SetVar( m_InvMtxVar, spotMtx );
	m_Shader->SetVar( m_PositionVar, position );
	m_Shader->SetVar( m_ColorVar, color );
	m_Shader->SetVar( m_DirectionVar, direction );
	m_Shader->SetVar( m_VolumeVar, volume );

}
bool SpotLightFogRenderer::HasVolume( Light& light, float& outZ )
{
	if ( light.Type == Light::SPOT  && light.GetLightFogVolume() )
	{		
		if ( grcViewport::GetCurrent()->IsSphereVisible( VECTOR4_TO_VEC4V(light.GetCullSphere()), outZ ) )
		{
			return true;
		}
	}
	return false;
}
