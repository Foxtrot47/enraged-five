// 
// manylights/lightgrid.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "lightgrid.h"
#include "grcore\texture.h"
#include "grcore\image.h"
#include "vector/vector3.h"
#include "simpleLightManager.h"
#include "qa/qa.h"

namespace rage {

void CreateFLight( const Light& light, FastLight& flight , int index, const Vector3& gravy )
{
	Vector3 start;
	Vector3 end;
	Vector3 position;
	float intensity;
	light.GetRange( start, end, position, intensity );
	start -= gravy;
	end += gravy;
	flight = FastLight( start, end, position, intensity , index );
}


// assumptions : v1 has a value to sort on it's x component and an index in it's w component
void BubbleVectorSortGreaterThan( Vector4& v1, Vector4& v2 )
{
	Vector4 res = v1.IsGreaterThanV( v2 );
	res.SplatX();
	Vector4 tmp;
	tmp = res.Select( v2, v1);
	v2 = res.Select( v1, v2 );
	v1 = tmp;
}

// always assumes is more than 4 elements and does a particle sort of the four elements
u32 SortLights4( FastLight* fLightRes, int amt, const Vector3& min, const Vector3& max, const Vector3& pos , const Vector3& axes )
{
	Vector4 v1 = fLightRes[0].IntensityAtPoint( pos , min, max, axes );
	Vector4 v2 = fLightRes[1].IntensityAtPoint( pos , min, max, axes  );
	Vector4 v3 = fLightRes[2].IntensityAtPoint( pos , min, max, axes  );
	Vector4 v4 = fLightRes[3].IntensityAtPoint( pos , min, max, axes  );

	v1.MergeZW( fLightRes[0].GetLightIndexV() );
	v2.MergeZW( fLightRes[1].GetLightIndexV());
	v3.MergeZW( fLightRes[2].GetLightIndexV());
	v4.MergeZW( fLightRes[3].GetLightIndexV());

	BubbleVectorSortGreaterThan( v2, v4 );		// sort initial values 
	BubbleVectorSortGreaterThan( v1, v3 );
	BubbleVectorSortGreaterThan( v1, v2 );
	BubbleVectorSortGreaterThan( v3, v4 );

	Vector4 idx;
	Vector4 increment;
	for ( int i = 4; i < amt; i++ )
	{
		Vector4 v = fLightRes[i].IntensityAtPoint( pos , min, max, axes  );  // get new value and sort it 
		v.MergeZW( fLightRes[i].GetLightIndexV() );

		BubbleVectorSortGreaterThan( v4, v );		// and sort this value
		BubbleVectorSortGreaterThan( v3, v4 );
		BubbleVectorSortGreaterThan( v2, v3 );
		BubbleVectorSortGreaterThan( v1, v2 );
	}
	// move vectors about
	v1.SplatW();
	v2.SplatW();
	v3.SplatW();
	v4.SplatW();

	// and then create the indices
	v1 = Merge4VX( v1, v2, v3, v4 );
	v1.PackColor();

	return  *((u32*)&v1);
}
// always assumes is more than 4 elements and does a particle sort of the four elements
void SortLights8( FastLight* fLightRes, int amt, const Vector3& min, const Vector3& max, const Vector3& pos , const Vector3& axes, u32& res1, u32& res2 )
{
	Vector4 v1 = fLightRes[0].IntensityAtPoint( pos , min, max, axes );
	Vector4 v2 = fLightRes[1].IntensityAtPoint( pos , min, max, axes  );
	Vector4 v3 = fLightRes[2].IntensityAtPoint( pos , min, max, axes  );
	Vector4 v4 = fLightRes[3].IntensityAtPoint( pos , min, max, axes  );
	Vector4 v5 = fLightRes[4].IntensityAtPoint( pos , min, max, axes );
	Vector4 v6 = fLightRes[5].IntensityAtPoint( pos , min, max, axes  );
	Vector4 v7 = fLightRes[6].IntensityAtPoint( pos , min, max, axes  );
	Vector4 v8 = fLightRes[7].IntensityAtPoint( pos , min, max, axes  );


	v1.MergeZW( fLightRes[0].GetLightIndexV() );
	v2.MergeZW( fLightRes[1].GetLightIndexV());
	v3.MergeZW( fLightRes[2].GetLightIndexV());
	v4.MergeZW( fLightRes[3].GetLightIndexV());
	v5.MergeZW( fLightRes[4].GetLightIndexV() );
	v6.MergeZW( fLightRes[5].GetLightIndexV());
	v7.MergeZW( fLightRes[6].GetLightIndexV());
	v8.MergeZW( fLightRes[7].GetLightIndexV());

	BubbleVectorSortGreaterThan( v4, v8 );		// sort initial values 
	BubbleVectorSortGreaterThan( v3, v7 );
	BubbleVectorSortGreaterThan( v2, v6 );
	BubbleVectorSortGreaterThan( v1, v5 );

	BubbleVectorSortGreaterThan( v5, v7 );
	BubbleVectorSortGreaterThan( v6, v8 );

	BubbleVectorSortGreaterThan( v3, v5 );		// sort initial values 
	BubbleVectorSortGreaterThan( v4, v6 );
	
	BubbleVectorSortGreaterThan( v1, v3 );		// sort initial values 
	BubbleVectorSortGreaterThan( v2, v4 );

	BubbleVectorSortGreaterThan( v2, v4 );		// sort initial values 
	BubbleVectorSortGreaterThan( v1, v3 );
	BubbleVectorSortGreaterThan( v1, v2 );
	BubbleVectorSortGreaterThan( v3, v4 );

	BubbleVectorSortGreaterThan( v5, v6 );		// sort initial values 
	BubbleVectorSortGreaterThan( v6, v7 );
	BubbleVectorSortGreaterThan( v7, v8 );
	
	Vector4 idx;
	Vector4 increment;
	for ( int i = 8; i < amt; i++ )  // could do a piority queue instead?
	{
		Vector4 v = fLightRes[i].IntensityAtPoint( pos , min, max, axes  );  // get new value and sort it 
		v.MergeZW( fLightRes[i].GetLightIndexV() );

		BubbleVectorSortGreaterThan( v8, v );		// and sort this value
		BubbleVectorSortGreaterThan( v7, v8 );		// and sort this value
		BubbleVectorSortGreaterThan( v6, v7 );		// and sort this value
		BubbleVectorSortGreaterThan( v5, v6 );		// and sort this value
		BubbleVectorSortGreaterThan( v4, v5 );		// and sort this value
		BubbleVectorSortGreaterThan( v3, v4 );
		BubbleVectorSortGreaterThan( v2, v3 );
		BubbleVectorSortGreaterThan( v1, v2 );
	}
	// move vectors about
	v1.SplatW();
	v2.SplatW();
	v3.SplatW();
	v4.SplatW();

	// and then create the indices
	Vector4 r1 = Merge4VX( v1, v2, v3, v4 );
	r1.PackColor();

	res1 = *((u32*)&r1);
	
	// move vectors about
	v5.SplatW();
	v6.SplatW();
	v7.SplatW();
	v8.SplatW();

	// and then create the indices
	Vector4 r2 = Merge4VX( v5, v6, v7, v8 );
	r2.PackColor();
	res2 = *((u32*)&r2);
	return;
}
u32 QuantizeVector( const Vector4& v, const Vector4& min, const Vector4& max )
{
	Vector4 res;
	// Clamp first
	res.Min( v, max );
	res.Max( res, min );

	res =( res - min) / (max - min );  // convert to 0 -1
	
	res *= 255.0f;
	res.PackColor();		// convert to compressed color
	return *( (u32*)&res );  // convert to u32 
}
void Vector4Transpose( const Vector4& v1, const Vector4& v2, const Vector4& v3, const Vector4& v4, 
					Vector4& vX, Vector4& vY, Vector4& vZ, Vector4& vW )
{
	Vector4 t1;
	Vector4 t2;
	Vector4 t3;
	Vector4 t4;

	t1.MergeXY( v1, v3 );
	t2.MergeXY( v2, v4 );

	t3.MergeZW( v1, v3 );
	t4.MergeZW( v2, v4 );

	vX.MergeXY( t1, t2 );
	vY.MergeZW( t1, t2 );
	vZ.MergeXY( t3, t4 );
	vW.MergeZW( t3, t4 );
};

template<class T>
void CopyArrayToTexture( grcTexture* tex, T* bytes, int width, int height )
{
	Assert( tex );
	grcTextureLock lock;
	tex->LockRect( 0, 0, lock );

	Assert( lock.Width == width );
	Assert( lock.Height == height  );
	Assert( lock.BitsPerPixel == sizeof(T) * 8 );

	memcpy( lock.Base, bytes, width * height * sizeof(T) );

	tex->UnlockRect( lock );
}

// interfaceface class to game grid
class Grid
{
protected:
	Vector3							m_min;
	Vector3							m_max;
	Vector4							m_minH;
	Vector4							m_maxH;

	FastLightList<LIGHT_GRID_MAX_LIGHTS>	m_fLights;
public:
	virtual ~Grid() {}

	virtual int Width()	= 0;
	virtual int GetLevels()	= 0;
	virtual bool UseTwoTextures() const = 0;
	virtual int SizeInBytes() const = 0;

	virtual void CopyResultsToTexture( grcTexture* gridTexture )  const  = 0;
	virtual void CopyResults2ToTexture( grcTexture* gridTexture ) const  = 0;
	virtual void CopyHeightsToTexture( grcTexture* hieghtTexture ) const  = 0;

	virtual void CalculateLightGrid(  int InitialDivision  = 8 ) = 0;

	virtual void ClearHeights() = 0;

	void SetExtents( const Vector3& min, const Vector3& max )
	{
		m_min = min;
		m_max = max;

		m_minH.x = m_min.z;
		m_minH.SplatX();

		m_maxH.x = m_max.z;
		m_maxH.SplatX();

	}

	template<int NumLights>
	void SetFastLights( const FastLightList<NumLights>& other )
	{
		m_fLights.Copy( other );
	}
	static void CalculateThreadFunc( sysTaskParameters &hdr )
	{
		Grid* grid = (Grid*)(hdr.Input.Data);
		grid->CalculateLightGrid();
	}
	virtual Vector4 GetCellV( int x, int y, int l  ) = 0;
	virtual Vector4 GetCell2V( int x, int y, int l  ) = 0;

};


// PURPOSE : Creates a grid which stores the indexes of the closest four lights per cell.
//
// REMARKS: It is implemented in a way that allows it to be used as a task which allows for multithreading the calculation
// both on the xbox and PS3.
template<int WIDTH, int MaxLights = 200, int LEVELS=1, bool USETWO= false>
class LightGridMemory : public Grid
{	
	u32								m_heights[ WIDTH * WIDTH];
	u32								m_grid[ WIDTH * WIDTH * LEVELS];
	u32								m_gridLower[ WIDTH * WIDTH * LEVELS];
	int								m_Width;
public:
	LightGridMemory() :  m_Width( WIDTH ) { Clear(); ClearHeights();}

	virtual int Width()						{ return WIDTH; }
	virtual int GetLevels()					{ return LEVELS; }
	virtual bool UseTwoTextures()	const	{ return USETWO; }

	int SizeInBytes()	const { return sizeof( LightGridMemory<WIDTH,MaxLights,LEVELS,USETWO>); }

	u32 operator()( int x, int y , int l  = 0)
	{
		return m_grid[ x + y * WIDTH +  WIDTH*WIDTH * l ];
	}
	
	u32* GetMem()	{ return m_grid; }
	void CopyMem( u32* ptr ) const
	{
		memcpy( ptr, m_grid, WIDTH* WIDTH * sizeof(u32 ));
	}
	virtual void CopyResultsToTexture( grcTexture* gridTexture ) const
	{
		CopyArrayToTexture( gridTexture, m_grid, WIDTH, WIDTH * LEVELS );
	}
	virtual void CopyResults2ToTexture( grcTexture* gridTexture ) const
	{
		CopyArrayToTexture( gridTexture, m_gridLower, WIDTH, WIDTH * LEVELS );
	}
	virtual void CopyHeightsToTexture( grcTexture* hieghtTexture ) const
	{
		CopyArrayToTexture( hieghtTexture, m_heights, WIDTH, WIDTH );
	}
	template<int Size>
	void SetSortedLightGrids( int sx, int sy , int Divisions, FastLightList<Size>& fLightRes, const Vector3& min, Vector3& step )
	{
		Vector4 endIndex;
		endIndex.Set( (float) m_fLights.count );

		float startZ = min.z;
		float stepZ = step.z;
		Vector3 axes( 1.0f,0.0f, 1.0f );
		
		if ( g_UnitUp.z > 0.0f )  
		{
			startZ = min.y;
			stepZ = step.y;
			if ( LEVELS == 1 )	// compile time branch
			{
				axes = Vector3( 1.0f, 1.0f, 0.0f );
			}
			else
			{
				axes = Vector3( 1.0f, 1.0f, 0.0f );
			}
			step.z = 100000.0f;
		}
		else
		{
			step.y = 100000.0f;
		}

		int MaxPerCell = ( USETWO ) ? 8 : 4;
		

		// loop over ever sub grid square and calculate top four lights
		int endy = ( sy + Divisions ) * m_Width;
	
		int yp = sy * WIDTH;
		for ( float z = startZ; yp < endy; z += stepZ, yp += m_Width  )
		{
			int xp = sx + yp;
			int endx = xp + Divisions;

			for ( float x = min.x ; xp < endx; x += step.x , xp++) 
			{
				Vector3 bmin( x, z, z );
				bmin *= axes;
				Vector3 bmax;
				bmax = bmin + step;

				if ( LEVELS == 1 )	// compile time branch
				{
					Vector3 centre = bmin + step * VEC3_HALF;
					if ( USETWO )
					{
						SortLights8( fLightRes.fLights, fLightRes.GetCount(), bmin, bmax, centre, axes, m_grid[xp], m_gridLower[xp] );
					}
					else
					{
						m_grid[ xp  ] = SortLights4( fLightRes.fLights, fLightRes.GetCount(), bmin, bmax, centre, axes );
					}
				}
				else
				{
					FastLightList<LIGHT_GRID_MAX_LIGHTS> fLightsCell;
					fLightsCell.CullFastLights( fLightRes, bmin, bmax );
					if (  fLightsCell.GetCount() )
					{
						if ( fLightsCell.GetCount() <= MaxPerCell )
						{
							Vector4 res1;
							Vector4 res2;
							
							fLightsCell.GetIndices8( endIndex, res1, res2 );
							res1.PackColor();
							res2.PackColor();
							u32 val1 = *((u32*)&res1);
							u32 val2 = *((u32*)&res2);
							
							m_grid[xp ] = val1;
							m_gridLower[xp] = val2;
							
							m_heights[xp] = 0xFFFFFFFF;// use first one all the time.
						}
						else
						{
							Vector2 heightRange = fLightsCell.GetHeightRange( m_minH, m_maxH);
							Vector3 One( 1.0f, 1.0f, 1.0f );

							Vector3 boundMin( bmin.x, bmin.y, heightRange.x );
							Vector3 boundMax( bmax.x, bmax.y, heightRange.y );
							Vector3 miniStep = (boundMax - boundMin ) / (float)LEVELS;
							for ( int l = 0; l < LEVELS; l++ )
							{
								boundMax = boundMin + miniStep;
								Vector3 centre = (boundMin + boundMax ) * 0.5f;
								int idx = xp  + l * WIDTH *WIDTH;
								if ( USETWO )
								{
									SortLights8( fLightsCell.fLights, fLightsCell.GetCount(), boundMin, boundMax, centre, One,m_grid[idx], m_gridLower[idx]);
								}
								else
								{
									m_grid[ idx ] = SortLights4( fLightsCell.fLights, fLightsCell.GetCount(), boundMin, boundMax, centre, One );
								}
								boundMin.z += miniStep.z;
							}
							Vector4 h( heightRange.x + miniStep.z, heightRange.x + 2.0f * miniStep.z , m_maxH.x, m_maxH.x);
							Assert( h.x < m_maxH.x );
							Assert( h.y < m_maxH.x );
							Assert( m_heights[xp] == 0 );
							m_heights[xp] = QuantizeVector( h, m_minH, m_maxH );
						}
					}
					else
					{
						Vector3 centre = bmin + step * VEC3_HALF;
						if ( USETWO )
						{
							SortLights8( fLightRes.fLights, fLightRes.GetCount(), bmin, bmax, centre, axes, m_grid[xp], m_gridLower[xp] );
						}
						else
						{
							m_grid[ xp  ] = SortLights4( fLightRes.fLights, fLightRes.GetCount(), bmin, bmax, centre, axes );
						}
						m_heights[xp] = 0xFFFFFFFF;// use first one all the time.
					}
				}
			}
		}
	}
	void CopyToSubGrid( int sx, int sy , int Divisions, u32 val1, u32 val2 )
	{
		int off = 0;
		for ( int i = 0; i < LEVELS; i++ )
		{
			off = WIDTH *WIDTH * i;
			int endy = ( sy + Divisions ) * WIDTH ;
			for ( int yp = sy * m_Width ; yp < endy; yp += m_Width  )
			{
				u32* ptr = m_grid + yp + sx + off ;
				u32* ptr2 = m_gridLower + yp + sx + off ;
				u32* endPtr = ptr + Divisions;
				while( ptr != endPtr )
				{
					*ptr++ = val1;
					*ptr2++ = val2;
				}
			}
		}
		if ( LEVELS > 1 )
		{
			u32 cval = QuantizeVector( m_maxH, m_minH, m_maxH );

			int endy = ( sy + Divisions ) * WIDTH ;
			for ( int yp = sy * m_Width ; yp < endy; yp += m_Width  )
			{
				for ( int xp = 0; xp < Divisions; xp++ )
				{
					
					Assert( m_heights[sx + yp+ xp] == 0 );
					m_heights[ sx + yp+ xp] = cval; 
				}
			}
		}
		
	}
	

	


	void Clear()	
	{
		memset( m_grid, 0, WIDTH * WIDTH * LEVELS * sizeof( u32) ); 
	}
	void ClearHeights()	
	{
		memset( m_heights, 0, WIDTH * WIDTH * sizeof( u32) ); 
	}


	Vector4 GetCellV( int x, int y, int l  )
	{
		Vector4 res = *((Vector4*)&(m_grid[  y * m_Width + x + WIDTH*WIDTH * l]));
		res.UnpackColor();
		return res;
	}

	Vector4 GetCell2V( int x, int y, int l  )
	{
		Vector4 res = *((Vector4*)&(m_gridLower[  y * m_Width + x + WIDTH*WIDTH * l]));
		res.UnpackColor();
		return res;
	}

	void CalculateLightGrid(  int InitialDivision  = 8 )
	{
		int Subdivision = m_Width / InitialDivision;
		FastLightList<LIGHT_GRID_MAX_LIGHTS> fLightsRes;

		Vector3	step = ( m_max - m_min ) / (float) InitialDivision;
		Vector3 subStep = step / (float ) Subdivision;

		Vector4 endIndex;
		endIndex.Set( (float) m_fLights.count );

		ClearHeights();

		float startZ = m_min.z;
		float stepZ = step.z;
		Vector3 axes( 1.0f,0.0f, 1.0f );

		if ( g_UnitUp.z > 0.0f )
		{
			startZ = m_min.y;
			stepZ = step.y;
			axes = Vector3( 1.0f, 1.0f, 0.0f );
			step.z = 100000.0f;
		}
		else
		{
			step.y = 100000.0f;
		}

		int MaxPerCell = ( USETWO ) ? 8 : 4;
							
		int xp = 0;
		for ( float x = m_min.x ; xp < InitialDivision ; x += step.x, xp++) 
		{
			int yp =0;
			for ( float z = startZ; yp < InitialDivision; z += stepZ, yp++ )
			{
				Vector3 bmin( x, z, z );
				bmin *= axes;

				Vector3 bmax;
				bmax = bmin + step + step * 0.5f;

				int sx = xp * Subdivision;
				int sy = yp * Subdivision;

				fLightsRes.CullFastLights( m_fLights, bmin, bmax );
				if ( fLightsRes.count <= MaxPerCell )	// do a fast copy and don't care if not actually used on cell
				{
					if ( !USETWO )
					{
						Vector4 res= fLightsRes.GetIndices( endIndex );
						res.PackColor();
						CopyToSubGrid( sx,sy, Subdivision, *((u32*)&res) , 0 );
					}
					else
					{
						Vector4 res1;
						Vector4 res2;
							
						fLightsRes.GetIndices8( endIndex, res1, res2 );
						res1.PackColor();
						res2.PackColor();
						CopyToSubGrid( sx,sy, Subdivision, *((u32*)&res1) , *((u32*)&res2) );
					}
				}
				else
				{
					// should only 16 times
					SetSortedLightGrids( sx,sy, Subdivision, fLightsRes, bmin, subStep );
				}
			}
		}
	}
};

inline Vector4 ToVector4( Color32& col )
{
	return Vector4( col.GetRedf(), col.GetGreenf(), col.GetBluef(), col.GetAlphaf());
}
inline Color32 Col( const Vector4& v  )
{
	Assert( v.x >= 0.0f && v.x <= 1.0f );
	Assert( v.y >= 0.0f && v.y <= 1.0f );
	Assert( v.z >= 0.0f && v.z <= 1.0f );
	return Color32( v.x, v.y, v.z, v.w );
}

#if __ASSERT


void UnitTestLightGridMemory()
{
	//UnitTestFastLight();
	//UnitTestVectorFuncs();

	LightGridMemory<8,35,1, false>	mem;
	
	LightGridMemory<8,35,3, false>	mem3;

	// check copy works
	mem.CopyToSubGrid( 3, 3 ,4, 100, 0 );
	Assert( mem(3, 3) == 100 );
	Assert( mem(4, 4) == 100 );
	Assert( mem(6, 5) == 100 );


	// check multilevel copy works
	mem3.CopyToSubGrid( 3, 3 ,4, 100, 0 );
	Assert( mem3(3, 3,0) == 100 );
	Assert( mem3(3, 3,1) == 100 );
	Assert( mem3(3, 3,2) == 100 );
	Assert( mem3(4, 4,0) == 100 );
	Assert( mem3(4, 4,1) == 100 );
	Assert( mem3(4, 4,2) == 100 );
	Assert( mem3(6, 5,0) == 100 );
	Assert( mem3(6, 5,1) == 100 );
	Assert( mem3(6, 5,2) == 100 );

	Vector2 start( -10.0f, -10.0f );
	Vector2 end( 10.0f,   10.0f );
	Vector3 position( 1.0f, 0.0f, 1.0f );
	Vector3 position1( -3.0f, -3.0f, 1.0f );
	Vector3 position2( 5.0f, 0.0f, 5.0f );
	Vector3 position3( 9.0f, 0.0f, 9.0f );
	Vector3 position4( 3.0f, 0.0f, 3.0f );
	Vector3 position5( 3.0f, 0.0f, 3.0f );
	Vector3 position6( 3.0f, 0.0f, 3.0f );

	FastLightList<48>	lights;
	lights.count = 12;
	lights[0] = FastLight( start,  end, position1, 1.0f , 0  );
	lights[1] = FastLight( start,  end, position2, 10.0f  , 1);
	lights[2] = FastLight( start,  end, position3, 100.0f , 2 );
	lights[3] = FastLight( start,  end, position4, 300.0f , 3 );
	lights[4] = FastLight( start,  end, position5, 600.0f , 4 );
	lights[5] = FastLight( start,  end, position6, 1000.0f , 5 );
	lights[6] = FastLight( start,  end, position1, 2000.0f , 6  );
	lights[7] = FastLight( start,  end, position2, 7000.0f  , 7);
	lights[8] = FastLight( start,  end, position3, 12000.0f , 8 );
	lights[9] = FastLight( start,  end, position4, 16000.0f , 9 );
	lights[10] = FastLight( start,  end, position5, 32000.0f , 10 );
	lights[11] = FastLight( start,  end, position6, 65000.0f , 11 );


	Vector3 start3( -10.0f, 0.0f, -10.0f );
	Vector3 end3( 10.0f,  0.0f, 10.0f );
	Vector3 axes = Vector3( 1.0f,1.0f,1.0f);

	u32 res =  SortLights4( lights.fLights, 6, start3, end3, VEC3_ZERO, axes );
	Color32 result( res );
	int r = result.GetRed();
	int b = result.GetBlue();
	int g = result.GetGreen();
	int a = result.GetAlpha();

	Assert( r == 5);
	Assert( g == 4);
	Assert( b == 3);
	Assert( a == 2);

	u32 res1, res2;
	SortLights8( lights.fLights, lights.GetCount(), start3, end3, VEC3_ZERO, axes, res1, res2);
	result = Color32(res1 );
	r = result.GetRed();
	b = result.GetBlue();
	g = result.GetGreen();
	a = result.GetAlpha();

	Assert( r == 11);
	Assert( g == 10);
	Assert( b == 9);
	Assert( a == 7);

	result = Color32( res2 );
	r = result.GetRed();
	b = result.GetBlue();
	g = result.GetGreen();
	a = result.GetAlpha();

	Assert( r == 6);
	Assert( g == 8);
	Assert( b == 5);
	Assert( a == 4);
};
#endif


//template class LightGridMemory<64,200,1>;
void SetImage( grcImage* image,  int x, int y, int a, int b, int c, int d )
{
	Color32  col( a, b, c,  d );

	image->SetPixel( x, y, col.GetColor() );
}
void SetImage( grcImage* image,  int x, int y, float a, float b, float c, float d )
{
	Color32  col( a, b, c,  d );

	image->SetPixel( x, y, col.GetColor() );
}

void TextureLightStream::Init()
{
}

TextureLightStream::~TextureLightStream()
{
	if ( m_positions )	{		m_positions->Release();	}
	if ( m_colors)		{		m_colors->Release();	}
}


void TextureLightStream::Create(  Grid*  grid , bool createFromSecondTexture, bool lowTexDepth )
{
	Assert ( m_width == grid->Width() );
	lowTexDepth = false;
	grcImage* imagePos = grcImage::Create(	m_width * PositionPixDepth,m_width * grid->GetLevels(),1,	lowTexDepth ? grcImage::A8R8G8B8 : grcImage::A16B16G16R16,grcImage::STANDARD,0,0);
	grcImage* imageCol = grcImage::Create(	m_width * ColorPixDepth,m_width * grid->GetLevels(),1,	grcImage::A8R8G8B8,grcImage::STANDARD,0,0);

	if ( createFromSecondTexture && !grid->UseTwoTextures() )
	{
		return; // nothing to do for second texture.
	}
	Vector4 mLightIndex;
	Vector4 maxLightIndex;
	mLightIndex.x = (float)(m_packLights.GetCount() - 1);	
	mLightIndex.SplatX();
	maxLightIndex.Max( mLightIndex, Vector4( 0.0f,0.0f,0.0f, 0.0f));

	for (int x = 0; x < grid->Width(); x++ )
	{
		for( int y = 0; y < grid->Width(); y++)
		{
			for ( int l = 0; l < grid->GetLevels(); l++ )
			{
				Vector4 v;
				if ( !createFromSecondTexture )
				{
					v = grid->GetCellV( x, y ,l);
				}
				else
				{
					v = grid->GetCell2V( x, y ,l);
				}
				v.Min( v, maxLightIndex );
				v.Max( v, Vector4( 0.0f,0.0f,0.0f, 0.0f));
				
				int yp = y + l * grid->Width();
				//SetGrid( x,yp,imagePos, imageCol, scale, bias, lights[(int)v.x ], lights[(int)v.y], lights[(int)v.z], lights[(int)v.w], indices );
				SetGrid( x,yp,imagePos, imageCol,  m_packLights[(int)v.x ], m_packLights[(int)v.y], m_packLights[(int)v.z], m_packLights[(int)v.w] );
			}
		}
	}
	if ( m_positions )
	{
		m_positions->Release();
	}
	if ( m_colors)
	{
		m_colors->Release();
	}
	grcTextureFactory::TextureCreateParams params( grcTextureFactory::TextureCreateParams::SYSTEM, grcTextureFactory::TextureCreateParams::LINEAR );

	m_positions = grcTextureFactory::GetInstance().Create( imagePos, &params );
	m_colors = grcTextureFactory::GetInstance().Create( imageCol, &params );

	imagePos->Release();
	imageCol->Release();

}
void TextureLightStream::SetUpLights( const atArray<Light>& lights )
{
	Vector3 scale;
	Vector3 bias;
	GetLightPositionBoundsScaleDown( lights, m_lightScale, m_lightBias );

	m_packLights.Resize( lights.GetCount() + 1 );
	for (int i = 0; i < lights.GetCount(); i++)
	{
		float spotScale;
		float spotBias;
		lights[i].CalculateSpotScaleBias( spotScale, spotBias );

		m_packLights[i] = PackedLight(  lights[i].GetPosition(), lights[i].GetDirection(), lights[i].Colour, 
										lights[i].Intensity, m_lightScale, m_lightBias, spotScale, spotBias );
	}
	m_packLights[lights.GetCount()] =  m_packLights[0];
	m_packLights[lights.GetCount()].colorSpotBias.SetVector3( Vector3(0.0f, 0.0f, 0.0f));
	m_packLights[lights.GetCount()].colorSpotBias.w = 0.0f;

}
	
void TextureLightStream::SetGrid( int x, int y, grcImage* image, grcImage* colImage, 
								 const PackedLight& a, const PackedLight&  b, const PackedLight& c, const PackedLight& d )
{
	Vector4 pX;
	Vector4 pY;
	Vector4 pZ;
	Vector4 cR;
	Vector4 cG;
	Vector4 cB;
	Vector4 dX;
	Vector4 dY;
	Vector4 dZ;
	Vector4 spotScale;
	Vector4 spotBias;
	Vector4 inten;

	Vector4 directionSpotScale;
	Vector4 colorSpotBias;

	int xp = x * PositionPixDepth;

	Vector4Transpose( a.posIntensity, b.posIntensity, c.posIntensity, d.posIntensity,
					  pX, pY, pZ, inten );

	Vector4Transpose( a.directionSpotScale, b.directionSpotScale, c.directionSpotScale, d.directionSpotScale,
					  dX, dY, dZ, spotScale );
	Vector4Transpose( a.colorSpotBias, b.colorSpotBias, c.colorSpotBias, d.colorSpotBias,
					  cR, cG, cB, spotBias );

	image->SetPixelVector( xp, y, (float *)&pX);
	image->SetPixelVector( xp + 1, y, (float *)&pZ);
	image->SetPixelVector( xp + 2, y, (float *)&spotScale);
	image->SetPixelVector( xp + 3, y, (float *)&spotBias);

	int xc = x * ColorPixDepth;
	colImage->SetPixel( xc,  y, Col( pY  ).GetColor());
	colImage->SetPixel( xc + 1, y, Col(inten ).GetColor() );
	colImage->SetPixel( xc + 2, y, Col(cR).GetColor() );
	colImage->SetPixel( xc + 3, y, Col(cG).GetColor() );
	colImage->SetPixel( xc + 4, y, Col(cB).GetColor() );

	colImage->SetPixel( xc + 5, y, Col(dX).GetColor() );
	colImage->SetPixel( xc + 6, y, Col(dY).GetColor() );
	colImage->SetPixel( xc + 7, y, Col(dZ).GetColor() );
}

void TextureLightStream::SetGrid( int x, int y, grcImage* image, grcImage* colImage, const Vector3& scale, const Vector3& bias,  
								 const Light& a, const Light&  b, const Light& c, const Light& d, u32 Indices )
{

	Vector4 pX( a.Position.x, b.Position.x, c.Position.x, d.Position.x );
	Vector4 pZ( a.Position.z, b.Position.z, c.Position.z, d.Position.z );

	Vector4 pY( a.Position.y, b.Position.y, c.Position.y, d.Position.y );
	Vector4 atten( a.Intensity, b.Intensity, c.Intensity, d.Intensity );
	atten /=1024.0f;
	atten.x = Min( atten.x, 400.0f );
	atten.y = Min( atten.y, 400.0f );
	atten.z = Min( atten.z, 400.0f );

	Assert( atten.x >= 0.0f && atten.y <= 1.0f );
	//atten.Invert();

	pX = pX * scale.x + Vector4( bias.x, bias.x, bias.x, bias.x );
	pY = pY * scale.y + Vector4( bias.y, bias.y, bias.y, bias.y );
	pZ = pZ * scale.z + Vector4( bias.z, bias.z, bias.z,bias.z );
	int xp = x * PositionPixDepth;

	Assert( pX.x >= 0.0f && pX.x <= 1.0f );
	Assert( pY.x >= 0.0f && pY.x <= 1.0f );
	Assert( pZ.x >= 0.0f && pZ.x <= 1.0f );

	image->SetPixelVector( xp, y, (float *)&pX);
	image->SetPixelVector( xp + 1, y, (float *)&pZ);

	// set direction
	Vector4 dX( a.DirectionVector.x, b.DirectionVector.x, c.DirectionVector.x, d.DirectionVector.x );
	Vector4 dY( a.DirectionVector.y, b.DirectionVector.y, c.DirectionVector.y, d.DirectionVector.y );
	Vector4 dZ( a.DirectionVector.z, b.DirectionVector.z, c.DirectionVector.z, d.DirectionVector.z );

	Vector4 spotScale;
	Vector4 spotBias;
	a.CalculateSpotScaleBias( spotScale.x, spotBias.x );
	b.CalculateSpotScaleBias( spotScale.y, spotBias.y );
	c.CalculateSpotScaleBias( spotScale.z, spotBias.z );
	d.CalculateSpotScaleBias( spotScale.w, spotBias.w );

	Vector4	half( 0.5f, 0.5f, 0.5f, 0.5f );

	spotScale = spotScale * ( 1.0f/128.0f);
	spotBias = -spotBias * ( 1.0f/128.0f);
	image->SetPixelVector( xp + 2, y, (float *)&spotScale);
	image->SetPixelVector( xp + 3, y, (float *)&spotBias);

	// rescale to 0 - 1
	dX = dX * -0.5f + half;
	dY = dY * -0.5f + half;
	dZ = dZ * -0.5f + half;

	Vector3 ac = a.Colour;
#ifdef TEST_GRID_MAPPING
	if ( x % 2 )
	{
		ac.Zero();
	}
	else
	{
		ac = Vector3( 1.0f, 1.0f, 1.0f );
	}
#endif

	// set color
	Color32	colR( ac.x, b.Colour.x, c.Colour.x, d.Colour.x );
	Color32	colG( ac.y, b.Colour.y, c.Colour.y, d.Colour.y );
	Color32	colB( ac.z, b.Colour.z, c.Colour.z, d.Colour.z );

	
	Color32 cpY = Col(pY);
	Color32 cpAtten = Col(atten);
	int xc = x * ColorPixDepth;

	colImage->SetPixel( xc,  y, cpY.GetColor() );
	colImage->SetPixel( xc + 1, y, cpAtten.GetColor() );
	colImage->SetPixel( xc + 2, y, colR.GetColor() );
	colImage->SetPixel( xc + 3, y, colG.GetColor() );
	colImage->SetPixel( xc + 4, y, colB.GetColor() );

	colImage->SetPixel( xc + 5, y, Col(dX).GetColor() );
	colImage->SetPixel( xc + 6, y, Col(dY).GetColor() );
	colImage->SetPixel( xc + 7, y, Col(dZ).GetColor() );

	colImage->SetPixel( xc + 8, y, Indices );

}
//--------------------------- nearest map ----------------------------------------------
void NearestMap::Blur( grcImage*& image )
{
	grcImage* newImage = grcImage::Create(	image->GetWidth(), image->GetHeight(),1, image->GetFormat(),grcImage::STANDARD,0,0);

	Color32*	img = image->GetColor32();

	for (int x = 0; x < image->GetWidth(); x++ )
	{
		for( int y = 0; y < image->GetHeight(); y++ )
		{
			int h = image->GetHeight();
			int minx = Max( x - 1, 0 );
			int miny = Max( y - 1, 0 );
			int maxx = Min( x + 1, image->GetWidth() - 1 );
			int maxy = Min( y + 1, image->GetHeight() - 1 );
			Vector4	col =   Vector4(1.0f,1.0f,1.0f,1.0f) * ToVector4( img[ minx + h * miny ] ) +
				Vector4(2.0f,2.0f,2.0f,2.0f) * ToVector4( img[ x + h * miny ] ) +
				Vector4(1.0f,1.0f,1.0f,1.0f) * ToVector4( img[ maxx + h * miny ] )+
				Vector4(2.0f,2.0f,2.0f,2.0f) * ToVector4( img[ minx + h * y ]) +
				Vector4(4.0f,4.0f,4.0f,4.0f) * ToVector4( img[ x + h * y ]) +
				Vector4(2.0f,2.0f,2.0f,2.0f) * ToVector4( img[ maxx + h * y ] )+
				Vector4(1.0f,1.0f,1.0f,1.0f) * ToVector4( img[ minx + h * maxy ]) +
				Vector4(2.0f,2.0f,2.0f,2.0f) * ToVector4( img[ x + h * maxy ]) +
				Vector4(1.0f,1.0f,1.0f,1.0f) * ToVector4( img[ maxx + h * maxy ] );

			col *= 1.0f/16.0f ;	// box filter
			Color32 resColour( col.x, col.y, col.z, col.w );
			newImage->SetPixel( x, y, resColour.GetColor() );
		}
	}
	image->Release();
	image = newImage;
}

NearestMap::NearestMap( int levs, int width, bool  texturedGrid, bool useEight ) 
	:	m_width( width ),  
		m_thandle(0),
		m_textureStream( texturedGrid ? rage_new TextureLightStream(width, levs) : 0 ),
		m_textureStream2( 0)

{
	m_tex[0] = 0;
	m_tex2[0] = 0;
	m_heights[0]=0;

	//  
	if ( useEight )
	{
		if ( levs > 1 )
		{
			m_memGrid = rage_new LightGridMemory<LIGHT_GRID_WIDTH, LIGHT_GRID_MAX_LIGHTS, LIGHT_GRID_NUM_LEVELS,true>();
		}
		else
		{
			m_memGrid = rage_new LightGridMemory<LIGHT_GRID_WIDTH, LIGHT_GRID_MAX_LIGHTS, 1,true>();
		}
	}
	else
	{
		if ( levs > 1 )
		{
			m_memGrid = rage_new LightGridMemory<LIGHT_GRID_WIDTH, LIGHT_GRID_MAX_LIGHTS, LIGHT_GRID_NUM_LEVELS,false>();
		}
		else
		{
			m_memGrid = rage_new LightGridMemory<LIGHT_GRID_WIDTH, LIGHT_GRID_MAX_LIGHTS, 1,false>();
		}

	}
	if ( texturedGrid && m_memGrid->UseTwoTextures() )
	{
		m_textureStream2 = rage_new TextureLightStream(width, levs);
	}
}


void NearestMap::Init()
{
	if ( m_textureStream )	{		m_textureStream->Init();	}
	if ( m_textureStream2 ) {		m_textureStream2->Init(); }

	grcImage *image = grcImage::Create(	m_width,m_width * m_memGrid->GetLevels(),1,	grcImage::A8R8G8B8,grcImage::STANDARD,0,0);
	grcImage *imageHgts = grcImage::Create(	m_width,m_width,1,	grcImage::A8R8G8B8,grcImage::STANDARD,0,0);


	for ( int i = 0; i < bufferIndex.GetNumBuffers(); i++ )
	{
		grcTextureFactory::TextureCreateParams	params( grcTextureFactory::TextureCreateParams::SYSTEM, grcTextureFactory::TextureCreateParams::LINEAR );
		m_tex[i] = grcTextureFactory::GetInstance().Create( image, &params );
		m_tex2[i] = grcTextureFactory::GetInstance().Create( image, &params );
		if ( m_memGrid->GetLevels() > 1  )
		{
			m_heights[i] = grcTextureFactory::GetInstance().Create( imageHgts, &params );
		}
	}
	imageHgts->Release();
	image->Release();
}
NearestMap::~NearestMap()
{
	for ( int i = 0; i < bufferIndex.GetNumBuffers(); i++ )
	{
		if ( m_tex[i] )		{			m_tex[i]->Release();		}
		if ( m_heights[i] ) {			m_heights[i]->Release();	}
	}
	delete m_textureStream;	
	delete m_textureStream2;

#if USE_THREADING
	if (  m_thandle )	sysTaskManager::Wait( m_thandle );
#endif
	delete m_memGrid;
}

void NearestMap::Create(  const Vector3& min, const Vector3& max )	
{
	if ( !m_tex[0] )	// hasn't been initialized yet
	{
		return;
	}
#if !USE_THREADING
	// fire off thread function
	m_memGrid->SetExtents( min, max );
	m_memGrid->SetFastLights( m_fLights);
	m_memGrid->ClearHeights();
	m_memGrid->CalculateLightGrid();

#else
	if (  m_thandle )	sysTaskManager::Wait( m_thandle );	// wait untill completed
#endif

	m_memGrid->CopyResultsToTexture( m_tex[ bufferIndex.GetWorkingSet() ] );

	if ( m_tex2[0] )
	{
		m_memGrid->CopyResults2ToTexture( m_tex2[ bufferIndex.GetWorkingSet() ] );
	}

	if ( m_heights[0] )
	{
		m_memGrid->CopyHeightsToTexture( m_heights[ bufferIndex.GetWorkingSet() ] );
	}

	bufferIndex.inc();
	
	if ( m_textureStream )
	{
		m_textureStream->Create( m_memGrid );
	}
	if ( m_textureStream2)
	{
		m_textureStream2->Create( m_memGrid , true);
	}
	// fire off task to do it next frame
#if USE_THREADING
	m_memGrid->SetExtents( min, max );
	m_memGrid->SetFastLights( m_fLights);

	sysTaskParameters	params;
	memset(&params,0,sizeof(params));
	params.Input.Size  = m_memGrid->SizeInBytes();
	params.Input.Data = m_memGrid;

	params.Output.Size = m_memGrid->SizeInBytes();
	params.Output.Data = m_memGrid;

	m_thandle = sysTaskManager::Create(TASK_INTERFACE( Grid::CalculateThreadFunc), params);
	Assert( m_thandle && "Not Enough Tasks" );

#endif

}


void GetLightPositionBoundsScaleDown( const atArray<Light>& lights , Vector3& scale, Vector3& bias )
{
	GetLightPositionBoundsScaleUp( lights, scale, bias );
	scale.InvertSafe();
	bias = -bias;
	bias *= scale;
}
void GetLightPositionBoundsScaleUp( const atArray<Light>& lights , Vector3& scale, Vector3& bias )
{
	Vector4 bound = GetBoundSpherePosition( lights );
	scale = Vector3( 2.0f * bound.w, 2.0f *bound.w, 2.0f *bound.w );
	bias.Set( bound.x - bound.w, bound.y - bound.w, bound.z - bound.w );
}
void NearestMap::Create( CShadowCache& UNUSED_PARAM(shadows),	const atArray<Light>& lights, const Vector3& min, const Vector3& max, bool UNUSED_PARAM( hasShadows ) )
{
	if ( m_tex[0] ) 	{		m_tex[0]->Release();	}
	if ( m_tex2[0] )		{		m_tex2[0]->Release();		}

	Vector3 scale;
	Vector3 bias;

	GetLightPositionBoundsScaleDown( lights, scale, bias );

	grcImage *image = grcImage::Create(	m_width,m_width,1,	grcImage::A8R8G8B8,grcImage::STANDARD,0,0);
	//grcImage *image2 = grcImage::Create(	m_width,m_width,1,	grcImage::A8R8G8B8,grcImage::STANDARD,0,0);

	Vector3		step = max - min;
	step /= (float) m_width;

	Light	emptyLight;
	emptyLight.Reset();

	int xp = 0;
	for ( float x = min.x ; xp < (m_width ); x += step.x , xp++) // four pixels each for inleaved sampling
	{
		int yp =0;
		for ( float z = min.z ; yp < m_width; z += step.z, yp++ )
		{
			Vector3 bmin( x, 0.0f, z );
			Vector3 bmax;
			bmax = bmin + step;
			Vector3 centre = ( bmin + bmax ) * 0.5;
			int closestLights[4];

			LightGetStrongestNLightsInBox( lights, closestLights, centre , 4, bmin, bmax);		// bottom left

			SetImage( image, xp, yp, closestLights[0], closestLights[1], closestLights[2], closestLights[3]);
		}
	}

	//	Blur( image2 );
	//	Blur( image2 );
	m_tex[0] = grcTextureFactory::GetInstance().Create( image );
	
}
};


using namespace rage;

#if __QA

#define TEST_QA_ITEM_BEGIN(Name)					\
class qa##Name : public qaItem					\
{ public:										\
	void Init() {};								\
	void Update(qaResult& result)

#define TEST_QA_ITEM_END()			};

#if 0 
TEST_QA_ITEM_BEGIN(VMerge4)
{
	Vector4 v1;
	Vector4 v2;
	Vector4 v3;
	Vector4 v4;
	v1.Set( 4.0f);
	v2.Set( 3.0f);
	v3.Set( 2.0f );
	v4.Set( 1.0f );

	Vector4 merge = Merge4VX( v1, v2, v3, v4 );
	QA_CHECK( merge == Vector4( 4.0f, 3.0f, 2.0f, 1.0f ));
}
TEST_QA_ITEM_END();

TEST_QA_ITEM_BEGIN(VectorBubbleSort)
{
	Vector4 b1( 4.0f, 0.0f, 0.0f, 1.0f );
	Vector4 b2( 3.0f, 0.0f, 0.0f, 2.0f );
	BubbleVectorSortGreaterThan( b2, b1 );
	QA_CHECK(  ( b1.x == 3.0f  ) && ( b1.w == 2.0f  ) &&( b2.x == 4.0f  ) &&( b2.w == 1.0f  )  );
}
TEST_QA_ITEM_END();

TEST_QA_ITEM_BEGIN(QuantizeVector)
{
	Vector4 min(-100.0f, -100.0f, -100.0f, -100.0f );
	Vector4 max(100.0f, 100.0f, 100.0f, 100.0f );
	Vector4 v( 0.0f, -100.0f, -100.0f, -100.0f );

	u32 res = QuantizeVector( v, min, max );
	QA_CHECK(   res == 0x007F0000 );
}
TEST_QA_ITEM_END();

TEST_QA_ITEM_BEGIN(QuantizeVectorOverflow)
{
	Vector4 min(-100.0f, -100.0f, -100.0f, -100.0f );
	Vector4 max(100.0f, 100.0f, 100.0f, 100.0f );

	Vector4 vOverflow( 100.0f, -100.0f, -100.0f, -100.0f );
	// check that it 1 goes to max
	u32 res2 = QuantizeVector( vOverflow, min, max );
	QA_CHECK( res2 == 0x00FF0000 );
}
TEST_QA_ITEM_END();

TEST_QA_ITEM_BEGIN(QuantizeVectorOverflow2)
{
	Vector4 min(-100.0f, -100.0f, -100.0f, -100.0f );
	Vector4 max(100.0f, 100.0f, 100.0f, 100.0f );
	Vector4 vOverflow2( 200.0f, -100.0f, -100.0f, -100.0f );
	u32 res2 = QuantizeVector( vOverflow2, min, max );
	QA_CHECK( res2 == 0x00FF0000 );
}
TEST_QA_ITEM_END();

TEST_QA_ITEM_BEGIN(Vector4Transpose)
{
	Vector4 a( 1.1f, 1.2f, 1.3f, 1.4f );
	Vector4 b( 2.1f, 2.2f, 2.3f, 2.4f );
	Vector4 c( 3.1f, 3.2f, 3.3f, 3.4f );
	Vector4 d( 4.1f, 4.2f, 4.3f, 4.4f );

	Vector4 r1,r2,r3,r4;
	Vector4Transpose( a, b, c, d, r1, r2, r3, r4 );
	QA_CHECK( r1.IsClose( Vector4( 1.1f, 2.1f, 3.1f, 4.1f ), 0.0001f ) );
	QA_CHECK( r2.IsClose( Vector4( 1.2f, 2.2f, 3.2f, 4.2f ), 0.0001f ) );
	QA_CHECK( r3.IsClose( Vector4( 1.3f, 2.3f, 3.3f, 4.3f ), 0.0001f ) );
	QA_CHECK( r4.IsClose( Vector4( 1.4f, 2.4f, 3.4f, 4.4f ), 0.0001f ) );
}
TEST_QA_ITEM_END();

TEST_QA_ITEM_BEGIN(FastLightSetup)
{
	Vector2 start1( -10.0, -10.0f );
	Vector2 end( 10.0, 10.0f );
	Vector3 position( 1.0f, 0.0f, 1.0f );

	FastLight	lit( start1, end, position, 10.0f, 1 );

	QA_CHECK( lit.rangeMin.x = start1.x );
	QA_CHECK( lit.GetLightIndexV() == Vector4( 1.0f, 1.0f, 1.0f, 1.0f ));
	QA_CHECK( lit.GetLightIndex() == 1);
}
TEST_QA_ITEM_END();

TEST_QA_ITEM_BEGIN(FastLightIntensity)
{
	Vector2 start1( -10.0, -10.0f );
	Vector2 end( 10.0, 10.0f );
	Vector3 position( 1.0f, 0.0f, 1.0f );

	FastLight	lit( start1, end, position, 10.0f, 1 );

	Vector3 axes = Vector3( 1.0f,1.0f,1.0f);

	Vector3 newPos( 0.0f, 0.0f, 1.0f );
	Vector3 inten = lit.Intensity( position, axes );
	Vector3 infin = Vector3( 0.0f, 0.0f, 0.0f );
	infin.Invert();

	QA_CHECK( lit.Intensity( newPos , axes ) == lit.invInten );
}
TEST_QA_ITEM_END();

TEST_QA_ITEM_BEGIN(FastLightBoundsCheck)
{
	Vector2 start1( -10.0, -10.0f );
	Vector2 end( 10.0, 10.0f );
	Vector3 position( 1.0f, 0.0f, 1.0f );

	FastLight	lit( start1, end, position, 10.0f, 1 );

	Vector3 axes = Vector3( 1.0f,1.0f,1.0f);

	// test bounds
	Vector3 bminInside( 0.0f, 0.0f, 0.0f );
	Vector3 bmaxInside( 100.0f, 100.0f, 100.0f );

	QA_CHECK( lit.InBoundBool( bminInside, bmaxInside ) );

	Vector3 bminOutSide( 12.0f, 12.0f, 0.0f ); 
	QA_CHECK( lit.InBoundBool( bminOutSide, bmaxInside ) == false );

	QA_CHECK( lit.InBound( bminInside, bmaxInside ) == Vector3( 1.0f, 1.0f, 1.0f ));
	QA_CHECK( lit.InBound( bminOutSide, bmaxInside ) == Vector3( 0.0f, 0.0f, 0.0f ));

	Vector3 newPos( 0.0f, 0.0f, 1.0f );
	Vector3 outPos = Vector3( 30.0f, 30.0f, 30.0f );
	QA_CHECK( lit.IntensityAtPoint( newPos, bminInside, bmaxInside , axes  ).IsNonZero() );
	//Assert( lit.IntensityAtPoint( outPos,  bminOutSide, bmaxInside ).IsNonZero() == false );
}
TEST_QA_ITEM_END();

TEST_QA_ITEM_BEGIN(FastLightCreationFromLight)
{
	// test creation from fastlight
	Light light;
	light.m_boundSphere = Vector4( 0.0f, 0.0f, 0.0f, 10.0f );
	light.Intensity = 100.0f;

	Vector3 newPos( 0.0f, 0.0f, 1.0f );
	light.Position = newPos;
	FastLight flight;
	CreateFLight( light, flight, 10, Vector3( 0.0f, 0.0f, 0.0f) );
	QA_CHECK( flight.pos == newPos );
	QA_CHECK( flight.rangeMin.x = -10.0f );
	QA_CHECK( flight.GetLightIndex() == 10 );
}
TEST_QA_ITEM_END();

/*
TEST_QA_ITEM_BEGIN(LightGridCopy)
{
	LightGridMemory<8,35,1, false>	mem;
	LightGridMemory<8,35,3, false>	mem3;

	// check copy works
	mem.CopyToSubGrid( 3, 3 ,4, 100, 0 );
	QA_CHECK( mem(3, 3) == 100 );
	QA_CHECK( mem(4, 4) == 100 );
	QA_CHECK( mem(6, 5) == 100 );


	// check multilevel copy works
	mem3.CopyToSubGrid( 3, 3 ,4, 100, 0 );
	QA_CHECK( mem3(3, 3,0) == 100 );
	QA_CHECK( mem3(3, 3,1) == 100 );
	QA_CHECK( mem3(3, 3,2) == 100 );
	QA_CHECK( mem3(4, 4,0) == 100 );
	QA_CHECK( mem3(4, 4,1) == 100 );
	QA_CHECK( mem3(4, 4,2) == 100 );
	QA_CHECK( mem3(6, 5,0) == 100 );
	QA_CHECK( mem3(6, 5,1) == 100 );
	QA_CHECK( mem3(6, 5,2) == 100 );
}
TEST_QA_ITEM_END();
*/
TEST_QA_ITEM_BEGIN(FastLightSort)
{

	Vector2 start( -10.0f, -10.0f );
	Vector2 end( 10.0f,   10.0f );
	Vector3 position( 1.0f, 0.0f, 1.0f );
	Vector3 position1( -3.0f, -3.0f, 1.0f );
	Vector3 position2( 5.0f, 0.0f, 5.0f );
	Vector3 position3( 9.0f, 0.0f, 9.0f );
	Vector3 position4( 3.0f, 0.0f, 3.0f );
	Vector3 position5( 3.0f, 0.0f, 3.0f );
	Vector3 position6( 3.0f, 0.0f, 3.0f );

	FastLightList<48>	lights;
	lights.count = 12;
	lights[0] = FastLight( start,  end, position1, 1.0f , 0  );
	lights[1] = FastLight( start,  end, position2, 10.0f  , 1);
	lights[2] = FastLight( start,  end, position3, 100.0f , 2 );
	lights[3] = FastLight( start,  end, position4, 300.0f , 3 );
	lights[4] = FastLight( start,  end, position5, 600.0f , 4 );
	lights[5] = FastLight( start,  end, position6, 1000.0f , 5 );
	lights[6] = FastLight( start,  end, position1, 2000.0f , 6  );
	lights[7] = FastLight( start,  end, position2, 7000.0f  , 7);
	lights[8] = FastLight( start,  end, position3, 12000.0f , 8 );
	lights[9] = FastLight( start,  end, position4, 16000.0f , 9 );
	lights[10] = FastLight( start,  end, position5, 32000.0f , 10 );
	lights[11] = FastLight( start,  end, position6, 65000.0f , 11 );


	Vector3 start3( -10.0f, 0.0f, -10.0f );
	Vector3 end3( 10.0f,  0.0f, 10.0f );
	Vector3 axes = Vector3( 1.0f,1.0f,1.0f);

	u32 res =  SortLights4( lights.fLights, 6, start3, end3, VEC3_ZERO, axes );
	Color32 resCol( res );
	int r = resCol.GetRed();
	int b = resCol.GetBlue();
	int g = resCol.GetGreen();
	int a = resCol.GetAlpha();

	QA_CHECK( r == 5);
	QA_CHECK( g == 4);
	QA_CHECK( b == 3);
	QA_CHECK( a == 2);

	u32 res1, res2;
	SortLights8( lights.fLights, lights.GetCount(), start3, end3, VEC3_ZERO, axes, res1, res2);
	resCol = Color32(res1 );
	r = resCol.GetRed();
	b = resCol.GetBlue();
	g = resCol.GetGreen();
	a = resCol.GetAlpha();

	QA_CHECK( r == 11);
	QA_CHECK( g == 10);
	QA_CHECK( b == 9);
	QA_CHECK( a == 7);

	resCol = Color32( res2 );
	r = resCol.GetRed();
	b = resCol.GetBlue();
	g = resCol.GetGreen();
	a = resCol.GetAlpha();

	QA_CHECK( r == 6);
	QA_CHECK( g == 8);
	QA_CHECK( b == 5);
	QA_CHECK( a == 4);
}
TEST_QA_ITEM_END();


// vector functions
//QA_ITEM_FAMILY(qaVMerge4, (), ());
//QA_ITEM_FAST(qaVMerge4, (), qaResult::FAIL_OR_TOTAL_TIME);

//QA_ITEM_FAMILY(qaVectorBubbleSort, (), ());
//QA_ITEM_FAST(qaVectorBubbleSort, (), qaResult::FAIL_OR_TOTAL_TIME);

//QA_ITEM_FAMILY(qaQuantizeVector, (), ());
//QA_ITEM_FAST(qaQuantizeVector, (), qaResult::FAIL_OR_TOTAL_TIME);

//QA_ITEM_FAMILY(qaQuantizeVectorOverflow, (), ());
//QA_ITEM_FAST(qaQuantizeVectorOverflow, (), qaResult::FAIL_OR_TOTAL_TIME);

//QA_ITEM_FAMILY(qaQuantizeVectorOverflow2, (), ());
//QA_ITEM_FAST(qaQuantizeVectorOverflow2, (), qaResult::FAIL_OR_TOTAL_TIME);

//QA_ITEM_FAMILY(qaVector4Transpose, (), ());
//QA_ITEM_FAST(qaVector4Transpose, (), qaResult::FAIL_OR_TOTAL_TIME);




// fast light settings
//QA_ITEM_FAMILY(qaFastLightSetup, (), ());
//QA_ITEM_FAST(qaFastLightSetup, (), qaResult::FAIL_OR_TOTAL_TIME);

//QA_ITEM_FAMILY(qaFastLightIntensity, (), ());
//QA_ITEM_FAST(qaFastLightIntensity, (), qaResult::FAIL_OR_TOTAL_TIME);

//QA_ITEM_FAMILY(qaFastLightBoundsCheck, (), ());
//QA_ITEM_FAST(qaFastLightBoundsCheck, (), qaResult::FAIL_OR_TOTAL_TIME);

//QA_ITEM_FAMILY(qaFastLightCreationFromLight, (), ());
//QA_ITEM_FAST(qaFastLightCreationFromLight, (), qaResult::FAIL_OR_TOTAL_TIME);

// light grid
//QA_ITEM_FAMILY(qaLightGridCopy, (), ());
//QA_ITEM_FAMILY(qaFastLightSort, (), ());

//QA_ITEM_FAST(qaLightGridCopy, (), qaResult::FAIL_OR_TOTAL_TIME);
//QA_ITEM_FAST(qaFastLightSort, (), qaResult::FAIL_OR_TOTAL_TIME);



#endif // __QA


#endif
