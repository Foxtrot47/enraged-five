// 
// sample_lighting/simpleLightManager.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Note: creates a 2D screenspace texture that holds all shadow relevant data
//
#ifndef SAMPLE_LIGHTING_SIMPLELIGHTMANAGER_H
#define SAMPLE_LIGHTING_SIMPLELIGHTMANAGER_H

#include "vector/vector4.h"
#include "vector/vector3.h"
#include "data/base.h"
#include "atl/string.h"
#include "vector/color32.h"
#include "parser/manager.h"


namespace rage
{


class Light;
class grcViewport;


Vector4 GetBoundSpherePosition( const atArray<Light>& list );
void GetBoundBoxPosition( const atArray<Light>& list, Vector3& min, Vector3& max );
void CalculateSpotLightBound( const Light& light, float clipIntensity, Vector3& min, Vector3& max );


inline Vector2 FadeInFactors( float s, float e )
{
	if ( e <= s )
	{
		e = s + 0.000001f;
	}
	float scale = 1.0f/(e - s );
	float offset = -s * scale;
	return Vector2( scale, offset );
}
inline Vector4 CalculateMaxStyleAttenutionValues( float nearStart, float nearEnd, float farStart, float farEnd )
{
	Vector2 fadeIn = FadeInFactors( nearStart, nearEnd);
	Vector2 fadeOut = FadeInFactors( farStart, farEnd);

	return Vector4( fadeIn.x, fadeIn.y, fadeOut.x, fadeOut.y );
}

// PURPOSE : Volume.
//
// NOTES: This is the details required volume rendering for lights 
class Volume: public datBase
{

public:
	Volume() : FogEnd( -1.0f )
	{}
	bool IsOn()  const { return (FogEnd > 0.0f); }


	float Transparency;
	float FogDensity;
	float FogStart;
	float FogEnd;
	
	PAR_PARSABLE;
private:
	

};



// PURPOSE : light capable of replicating functionality of standard lights in both Max and Maya.
//
// NOTES: This light is the interface between rage and the art packages to support exact replication of lighting
//		from the art package renderers to ingame.
//
//	TODO: needs to be extended to support Max's light attenuation
//
class Light : public datBase
{
	static const int DefaultShadowSize  = 512;
public:
	enum LightType 
	{
		POINT = 0,
		SPOT,
		DIRECTIONAL,
		
	};
	atString	name;
	int			Type;

	// general light
	Vector3		Position;
	Vector3		Colour;
	float		Intensity;

	// spot
	Vector3		Direction;
	float		ConeAngle;
	float		Penumbra;
	float		DropOff;

	// shadow
	Vector2		ShadowRange;
	float		ShadowBias;
	float		ShadowEplision;
	int			ShadowSize;
	int			ShadowFilter;

	// Max style attenuation
	float NearAttenStart;
	float NearAttenEnd;
	float FarAttenStart;
	float FarAttenEnd;

	bool  CastLight;
	bool  CastShadow;



	Vector3		DirectionVector;
	Vector4		m_boundSphere;
	Vector3		m_boundMin;
	Vector3		m_boundMax;
	float		m_invCosConeAngle;
	
	bool		m_isDirty;



	Volume		LightVolume;

	Light() : 	ShadowSize(DefaultShadowSize), ShadowFilter( 5), m_isDirty( true ),CastLight(true)
	{}

	const Volume* GetLightFogVolume() 
	{
		return LightVolume.IsOn() ? &LightVolume : 0;
	}

	void Reset();
	void Update( int Version );
	void CalculateSpotScaleBias( float& scale, float& bias ) const;

	bool CastsLight() const 	{ return CastLight; }
	bool CastsShadow() const 	{ return CastShadow; }
	bool CastsVolume() const	{ return LightVolume.IsOn(); }

	bool IsClose( const Light& other, float dist, float colorDelta ) const 
	{
		float d = other.Position.Dist( Position );
		float c = other.Colour.Dist( Colour );
		return ( d < dist && c < colorDelta  );
	}

	void Merge( const Light& other , int version )
	{
		Colour =  ( Colour + other.Colour ) / 2.0f;
		Position = ( Position + other.Position ) / 2.0f;
		Intensity = ( Intensity + other.Intensity ); // note intensity is added
		Update( version );
	}

	Vector4 GetMaxStyleAttenuation() const 
	{
		return  CalculateMaxStyleAttenutionValues( NearAttenStart, NearAttenEnd, FarAttenStart, FarAttenEnd);
	}
	float GetAngleRange() const { return ConeAngle + Max( Penumbra, 0.0f); }

	// shadows
	Vector2 GetShadowRange()
	{
		Vector2 res =  ShadowRange;
		res.x  += 0.00001f;
		return res;
	}

	float GetShadowBias()		const	{ return ShadowBias; }
	float GetShadowEplision()	const	{ return ShadowEplision; }
	float GetShadowFilterSize()	const	{ return (float)ShadowFilter / 5.0f; }
	

	Vector3 GetPosition() const { return Position; }
	const Vector3& GetDirection() const { return DirectionVector; }
	int GetShadowSize()	 const { return ShadowSize /4; }


	float GetLightIntensityOnPoint( const Vector3& pt ) const 
	{
		float factor = 0.1f;

		float lightIntensity =  ( Intensity * factor / pt.Dist2( Position ) ) ;// Intensity//* Colour.Dot( Vector3( 0.3f, 0.6f, 0.17f ));
		return lightIntensity;
	}
	
	Vector3 LightPoint( const Vector3& pt ) const 
	{
		Vector3 dir = Position - pt;
		float lightIntensity =  Min( Intensity / dir.Dot( dir ), 1.0f ) ;// Intensity//* Colour.Dot( Vector3( 0.3f, 0.6f, 0.17f ));

		dir.NormalizeSafe();
		Vector3 spotDir;
		spotDir.Negate(DirectionVector);
		float spotInten = Max( dir.Dot( spotDir ), 0.0f);
		Vector3 res = lightIntensity * spotInten * Colour;
		return res;
	}
	Vector4& GetSphere()
	{
		return m_boundSphere;
	}

	Vector4 GetCullSphere()
	{
		Vector3 centre = ( m_boundMin + m_boundMax ) * 0.5f;
		Vector3 radius = ( m_boundMax - m_boundMin ) * 0.5f;
		Vector4 v( centre.x, centre.y, centre.z, radius.Mag());
		return v;
	}
	Vector3& GetMin()	{ return m_boundMin; }
	Vector3& GetMax()	{ return m_boundMax; }

	bool InBoundXZ( const Vector3& min, const Vector3& max ) const
	{
		bool xInside = ( ( m_boundSphere.x - m_boundSphere.w )  < max.x ) &&
						 ( ( m_boundSphere.x + m_boundSphere.w )  > min.x ) ;
		bool zInside = ( ( m_boundSphere.z - m_boundSphere.w )  < max.z ) &&
			( ( m_boundSphere.z + m_boundSphere.w )  > min.z ) ;

		return xInside && zInside;
	}
	
	void GetRange( Vector3& start, Vector3& end, Vector3& pos, float& Inten ) const
	{
		float factor = 1.0f;//0.1f;
		start = m_boundMin;// Vector2( m_boundSphere.x, m_boundSphere.z ) - Vector2( m_boundSphere.w, m_boundSphere.w );
		end = m_boundMax;// Vector2( m_boundSphere.x, m_boundSphere.z ) + Vector2( m_boundSphere.w, m_boundSphere.w );
		Inten = Intensity * factor ;
		pos = Position;
	}
	float CalculateConeCosineAngle()
	{
		if ( Type != SPOT )
		{
			return -1.001f;
		}
		float radAngle = (0.5f * ConeAngle/360.0f) * 2.0f * 3.14f;
		return cos( radAngle  );
	}
	float CalculateTanAngle() const
	{
		if ( Type != SPOT )
		{
			return -1.001f;
		}
		float radAngle = (0.5f * ( GetAngleRange() ) /360.0f) * 2.0f * 3.14f;
		return tan( radAngle  );
	}

	void	MarkDirty() 	{	m_isDirty = true;	}
	bool	IsDirty()		{ return m_isDirty; }
	void	ResetDirty()	{ m_isDirty = false; } 
	
	static void SetSphereOfInfluence( float val )	{ sm_SphereOfInfluence = val ; }

	virtual ~Light() {}

	PAR_PARSABLE;
private:
	static float sm_SphereOfInfluence;
};

void LightGetStrongestNLightsInBox( const atArray<Light>& lights, int* results, const Vector3& point ,int N  ,const Vector3& min, const Vector3& max ) ;
Vector3 LightCalculateIncidentLighting( const atArray<Light>& lights, int* indices, int amt, const Vector3& point );


// PURPOSE : simple manager for the lights ( a list )
//
// idea that this would be extended in game titles to support hierachies etc.
//
class LightList : public datBase
{
	int				Version;
	Vector3			DirectionalDirection;
	Vector3			DirectionalColor;

	Vector3			CamPosition;
	Vector3			CamDirection;
public:
	virtual ~LightList() {};
	LightList() :  Version(1) {}

	atArray<Light>		lights;
	atArray<Light>		fillLights;
	atArray<Light>		AllLights;


	// -- Give array interface
	int	GetCount()		{ return lights.GetCount(); }
	const Light&		operator[](int i )	{ return lights[i];}
	// --

	void Clear()			{		lights.clear(); 	}
	void CompactLights( LightList& list, float compactDistance, float colorDelta );

	void GetDirectional( Vector3& dir, Vector3& col );


	Matrix34 GetCamera();


	int GetVersion() const { return Version; }

	void GetStrongestNLights( int* results, const Vector3& point ,int N  ) const;

	void Cull( grcViewport* view );
	void Draw( bool showSpheres = false, bool showAABBs = true, bool showPos = true );

	void Update()
	{
		for ( int i =0; i < lights.GetCount(); i++ )
		{
			lights[i].Update( Version );
		}
		for ( int i =0; i < fillLights.GetCount(); i++ )
		{
			fillLights[i].Update( Version );
		}
		AllLights = lights;
	}
	void Scale( float sc, float inten )
	{
		for ( int i =0; i < lights.GetCount(); i++ )
		{
			lights[i].Position *= sc;
			lights[i].Intensity *= inten;
		}
	}

	void Pad()		// pads to 8 lights
	{
		if ( lights.GetCount() > 8 )
		{
			return;
		}
		while(  (lights.GetCount() & 0x7 ) != 0 )
		{
			Light& lit = lights.Append();
			lit.Reset();
		}
	}


	bool Empty()	{ return lights.GetCount() == 0; }


	PAR_PARSABLE;
};


void DrawSphere( const Vector4& sphere , Color32 colour );
void DrawAABB( const Vector3& min, const Vector3& max , Color32 color );


};

#endif
