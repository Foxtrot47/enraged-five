// 
// sample_lighting/lightgrid.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Note: creates a 2D screenspace texture that holds all shadow relevant data
//
#ifndef SAMPLE_LIGHTING_LIGHT_GRID_H
#define SAMPLE_LIGHTING_LIGHT_GRID_H

#if __XENON 
#define USE_THREADING 1
#else
#define USE_THREADING 0
#endif

#include "vector/vector4.h"
#include "system/task.h" 
#include "vector/color32.h"
#include "atl/array.h"
#include "atl/ptr.h"

namespace rage {

class grcTexture;
class grcImage;
class CShadowCache;
class Light;


// PURPOSE: simplifed light to allow for fast operations on lights for culling / sorting etc on the CPU
//
struct FastLight
{
	Vector3 rangeMax;
	Vector3 rangeMin;
	Vector3 pos;
	Vector3 invInten;
	Vector4 index;


	FastLight() {}

	FastLight(  const Vector3& start, const Vector3& end , const Vector3& position, float intensity, int i  )
		: pos( position ), rangeMin( start ), rangeMax( end )
	{
		invInten.Set( intensity );
		index.Set( (float) i );
	}
	int GetLightIndex() const 
	{ 
		Vector4 v = index;
		v.FloatToInt();
		return *( (int*)&v.x );
	}
	Vector4 GetLightIndexV() const { return index; }

	bool InBoundBool( const Vector3&boundMin, const Vector3&boundMax ) const
	{
		Vector3 in = rangeMin.IsGreaterThanV( boundMax );
		Vector3 in2 = rangeMax.IsLessThanV( boundMin );
		in.Or(in2 );
		return !in.IsNonZero(); 
	}
	Vector3 InBound( const Vector3& boundMin, const Vector3& boundMax ) const 
	{
		static const Vector3 one( 1.0f, 1.0f, 1.0f );

		Vector3 res = InBoundBool( boundMin, boundMax ) ?  one : VEC3_ZERO;
		return res;
	}
	Vector3	Intensity( const Vector3& position, const Vector3& axes  ) const 
	{
		Vector3 res;
		Vector3 diff = position - pos;
		diff *= axes;

		res.Invert( diff.Mag2V() );
		return res * invInten;
	}
	Vector4 IntensityAtPoint(  const Vector3& position , const Vector3& boundMin, const Vector3& boundMax, const Vector3& axes ) const 
	{
		Vector3 res = Intensity( position, axes) * InBound( boundMin, boundMax );
		Vector4 res4;
		res4.SetVector3( res );
		return res4;
	}
};


void CreateFLight( const Light& light, FastLight& flight , int index, const Vector3& gravy );

// Vector merge 4
inline Vector4 Merge4VX( Vector4& v1, Vector4& v2, Vector4& v3, Vector4& v4 )
{
	return Vector4( v1.x, v2.x, v3.x, v4.x);
}

template<int MaxLights>
struct FastLightList
{
	FastLight	fLights[MaxLights ];
	int			count;

	void Resize( int i )  
	{ 
		Assert( i < MaxLights  ); 
		count = i; 
	}

	int GetCount() const { return count; }

	FastLight& operator[]( int c )
	{
		Assert( c < count);
		return fLights[ c ];
	}

	template<int NumLights>
	void Copy( const FastLightList<NumLights>& other )
	{
		Resize(  other.GetCount()  );
		for (int i = 0; i < other.GetCount(); i++ )
		{
			fLights[i] = other.fLights[i];
		}
	}

	void CreateFastLights( const atArray<Light>& lights, const Vector3& diagOffset )
	{
		Vector3 gravy = diagOffset  * 0.5f/64.0f;

		Assert( lights.GetCount() <= MaxLights );
		for (int i = 0; i < lights.GetCount(); i++ )
		{
			CreateFLight( lights[i], fLights[i],i , gravy );
		}
		count= lights.GetCount();
	}
	void CullFastLights( const FastLightList& inLights, Vector3& boundMin, Vector3& boundMax )
	{
		count = 0;
		for ( int i = 0; i < inLights.count; i++ )
		{
			if ( inLights.fLights[i].InBoundBool( boundMin, boundMax))
			{
				fLights[ count++ ] = inLights.fLights[i];
			}
		}
	}
	// PURPOSE : get the range of difference in the lights allong the height access
	//
	// NOTES: This is not a simple bound on the lights, but the range from the minmium max light extent to the
	// maximum min light extent. So this gets the range of overlap on the lights with is more useful.
	//
	Vector2 GetHeightRange( const Vector4& gH , const Vector4& gMax )  
	{
		Vector3 groundV( gH.x, gH.x, gH.x);

		Vector3 maxVal = fLights[0].pos;
		Vector3 minVal = fLights[0].rangeMin;
		for ( int i = 1; i < count; i++ )
		{
			minVal.Min(minVal, fLights[i].rangeMin);
			maxVal.Max( maxVal, fLights[i].pos );
		}
	
		// clamp to ground
		minVal.Max( groundV, minVal );
		maxVal.Max( groundV, maxVal );

		maxVal.z = Min( maxVal.z , gMax.x );

		return Vector2( minVal.z, maxVal.z );
	}
	Vector4 GetIndices(  const Vector4& EndIndex )
	{
		Assert( count <= 4 );
		Vector4 v1 = count > 0 ? fLights[0].GetLightIndexV() : EndIndex;
		Vector4 v2 = count > 1 ? fLights[1].GetLightIndexV() : EndIndex;
		Vector4 v3 = count > 2 ? fLights[2].GetLightIndexV() : EndIndex;
		Vector4 v4 = count > 3 ? fLights[3].GetLightIndexV() : EndIndex;
		return Merge4VX( v1, v2, v3, v4 );
	}
	void GetIndices8( const Vector4& EndIndex, Vector4& res1, Vector4& res2 )
	{
		Assert( count <= 8 );
		Vector4 v1 = count > 0 ? fLights[0].GetLightIndexV() : EndIndex;
		Vector4 v2 = count > 1 ? fLights[1].GetLightIndexV() : EndIndex;
		Vector4 v3 = count > 2 ? fLights[2].GetLightIndexV() : EndIndex;
		Vector4 v4 = count > 3 ? fLights[3].GetLightIndexV() : EndIndex;
		Vector4 v5 = count > 4 ? fLights[4].GetLightIndexV() : EndIndex;
		Vector4 v6 = count > 5 ? fLights[5].GetLightIndexV() : EndIndex;
		Vector4 v7 = count > 6 ? fLights[6].GetLightIndexV() : EndIndex;
		Vector4 v8 = count > 7 ? fLights[7].GetLightIndexV() : EndIndex;

		res1 = Merge4VX( v1, v2, v3, v4 );
		res2 = Merge4VX( v5, v6, v7, v8 );
	}
};


template<int NumBuffers>
struct BufferNumber
{
	int count;
	BufferNumber() : count(0) {}

	int GetWorkingSet() const { return ( count  + NumBuffers - 1 )% NumBuffers; }
	int Get() const { return count % NumBuffers; }

	void inc() { count++;}

	int GetNumBuffers() const { return NumBuffers;}
};

// values of a light packed into 4
//
struct PackedLight
{
	Vector4 posIntensity;
	Vector4 directionSpotScale;
	Vector4 colorSpotBias;

	PackedLight() {}

	PackedLight( const Vector3& pos, 
				 const Vector3& dir,
				 const Vector3& col, 
				 float inten,  
				 const Vector3& scale, 
				 const Vector3& bias,
				 float spotScale,
				 float spotBias )
	{
		// rescale to 0 - 1 
		inten = Min( inten/1024.0f, 10.0f);
		Assert( inten >= 0.0f && inten  <= 1.0f );
		Vector3 scaledPos = pos * scale + bias;

		Assert( scaledPos.x >= 0.0f && scaledPos.x <= 1.0f);
		Assert( scaledPos.y >= 0.0f && scaledPos.y <= 1.0f);
		Assert( scaledPos.z >= 0.0f && scaledPos.z <= 1.0f);

		Assert( spotScale < 128.0f && spotScale >= 0.0f );
		Assert( -spotBias < 128.0f && spotBias <= 1.0f );

		spotScale = spotScale /128.0f;
		spotBias =( 1.0f-spotBias) /128.0f;

		//spotScale = spotScale /1024.0f;
		//spotBias = (1.0f -spotBias ) /1024.0f;

		Assert( spotBias >= 0.0f && spotBias <= 1.0f );

		Vector3 scaledDir = dir * -0.5f + Vector3( 0.5f, 0.5f, 0.5f );
		Assert( scaledDir.x >= 0.0f && scaledDir.x <= 1.0f);
		Assert( scaledDir.y >= 0.0f && scaledDir.y <= 1.0f);
		Assert( scaledDir.z >= 0.0f && scaledPos.z <= 1.0f);

		// pack it into structure
		posIntensity.SetVector3( scaledPos );
		posIntensity.w = inten;

		directionSpotScale.SetVector3( scaledDir );
		directionSpotScale.w = spotScale;

		colorSpotBias.SetVector3( col);
		colorSpotBias.w = spotBias;
	}
};

#define MAX_LIGHTS		256
// fwd reference
class Grid;

class TextureLightStream
{
	grcTexture*				m_positions;
	grcTexture*				m_colors;
	
	int									m_width;
	int									m_levels;

	Vector3								m_lightScale;
	Vector3								m_lightBias;

	static const int						PositionPixDepth = 6;
	static const int						ColorPixDepth = 9;
	
	atFixedArray<PackedLight,MAX_LIGHTS>		m_packLights;
public:
	TextureLightStream( int width, int levels) : m_width( width ), m_levels( levels),m_positions(0),m_colors(0)
	{}
	~TextureLightStream();

	void Init();

	void SetUpLights( const atArray<Light>& lights );

	const grcTexture* GetPosition() const	{ return m_positions; }
	const grcTexture* GetColors()	const	{ return m_colors; }

	void Create( Grid* grid , bool createFromSecondTexture = false, bool lowTexDepth = false );

	void SetGrid( int x, int y,  grcImage* image, grcImage* colImage, const Vector3& scale, const Vector3& bias,  
		const Light& a, const Light&  b, const Light& c, const Light& d, u32 Indices );

	void SetGrid( int x, int y, grcImage* image, grcImage* colImage, 
								 const PackedLight& a, const PackedLight&  b, const PackedLight& c, const PackedLight& d );
};
#define LIGHT_GRID_WIDTH		64
#define LIGHT_GRID_MAX_LIGHTS	256
#define LIGHT_GRID_USE_EIGHT	true
#define LIGHT_GRID_NUM_LEVELS	3

// PURPOSE : this class creates a texture on the CPU of with the indices of the four strongest lights in each cell.
//
//
class NearestMap 
{
	int			m_width;

	static const int NumBuffers = 3;
	BufferNumber<NumBuffers>	bufferIndex;
	grcTexture*	m_tex[NumBuffers];
	grcTexture*	m_heights[NumBuffers];
	grcTexture*	m_tex2[NumBuffers];

	Grid*	m_memGrid;

	sysTaskHandle	m_thandle;

	void Blur( grcImage*& image );
	
	TextureLightStream*		m_textureStream;
	TextureLightStream*		m_textureStream2;
	FastLightList<256>		m_fLights;
public:
	NearestMap( int levs = LIGHT_GRID_NUM_LEVELS, int width = 64 , bool texturedGrid  = false, bool useEight = true  );
	~NearestMap();
	
	void Init();

	const grcTexture*	GetGrid()	{ return m_tex[ bufferIndex.Get() ] ; }
	const grcTexture*	GetGrid2()		{ return m_tex2[ bufferIndex.Get() ]; }
	const grcTexture*	GetHeights() const { return m_heights[ bufferIndex.Get() ] ; }

	void SetUpLights( const atArray<Light>& lights )
	{
		if ( m_textureStream ) { m_textureStream->SetUpLights( lights ); }
		if ( m_textureStream2 ) { m_textureStream2->SetUpLights( lights ); }
	}

	const grcTexture*	GetPosition() { return m_textureStream ?  m_textureStream->GetPosition() : 0 ; }
	const grcTexture*	GetColors() { return m_textureStream ?  m_textureStream->GetColors() : 0 ; }

	const grcTexture*	GetPosition2() { return m_textureStream2 ?  m_textureStream2->GetPosition() : 0 ; }
	const grcTexture*	GetColors2() { return m_textureStream2 ?  m_textureStream2->GetColors() : 0 ; }

	
	void SetFastLights( const FastLightList<256>& other )	{ m_fLights.Copy( other ); }
	void Create( const Vector3& min, const Vector3& max );
	void Create( CShadowCache& UNUSED_PARAM(shadows),	const atArray<Light>& lights, const Vector3& min, const Vector3& max, bool UNUSED_PARAM( hasShadows ) );
};

void GetLightPositionBoundsScaleDown( const atArray<Light>& lights , Vector3& scale, Vector3& bias );
void GetLightPositionBoundsScaleUp( const atArray<Light>& lights , Vector3& scale, Vector3& bias );

void UnitTestLightGridMemory();

};

#endif

