<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<autoregister allInFile="true"/>

<structdef type="Volume">
<float name="Transparency"/>
<float name="FogDensity"/>
<float name="FogStart"/>
<float name="FogEnd"/>
</structdef>

<structdef type="Light">
<int name="Type"/>
<Vector3 name="Position"/>
<Vector3 name="Colour"/>
<float name="Intensity"/>
<float name="ConeAngle"/>
<float name="Penumbra"/>
<float name="DropOff"/>
<Vector3 name="Direction"/>
<Vector2 name="ShadowRange"/>
<float name="ShadowBias"/>
<float name="ShadowEplision"/>
<int name="ShadowSize"/>
<int name="ShadowFilter"/>
<float name="NearAttenStart"/>
<float name="NearAttenEnd"/>
<float name="FarAttenStart"/>
<float name="FarAttenEnd"/>
<struct name="LightVolume" type="Volume"/>
<bool name="CastLight"/>
<bool name="CastShadow"/>
</structdef>

<structdef type="LightList">
<array name="lights" type="atArray">
<struct type="Light"/>
</array>
<array name="fillLights" type="atArray">
<struct type="Light"/>
</array>
<int name="Version"/>
<Vector3 name="CamPosition"/>
<Vector3 name="CamDirection"/>
<Vector3 name="DirectionalDirection"/>
<Vector3 name="DirectionalColor"/>
</structdef>

</ParserSchema>