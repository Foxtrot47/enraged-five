// 
// sample_lighting/lightshadercontrollers.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Note: creates a 2D screenspace texture that holds all shadow relevant data
//
#ifndef SAMPLE_LIGHTING_LIGHTSHADERCONTROLLERS_H
#define SAMPLE_LIGHTING_LIGHTSHADERCONTROLLERS_H

#include "shadercontrollers/shaderfragmentcontrol.h"
#include "grshadowmap/spotlightshadowmap.h"

namespace rage {

	class Light;
	//class CShadowCache;
	class LightList;

class FastSpot4Lighting : public ShaderFragment
{
	struct FastSpot4LightingVarCache : public VarCache
	{
		grcEffectVar	m_positionXID;
		grcEffectVar	m_positionYID;
		grcEffectVar	m_positionZID;

		grcEffectVar	m_colorRID;
		grcEffectVar	m_colorGID;
		grcEffectVar	m_colorBID;

		grcEffectVar	m_DirectionID;
		grcEffectVar	m_FadeScaleID;
		grcEffectVar	m_FadeOffsetID;
		grcEffectVar	m_DropOffID;

		grcEffectVar	m_intensityID;
		grcEffectVar	m_amtID;

		grcEffectVar	m_ShadowMatrix1ID;
		grcEffectVar	m_ShadowTexture1ID;
		grcEffectVar	m_ShadowTexture2ID;
		grcEffectVar	m_ShadowTexture3ID;
		grcEffectVar	m_ShadowTexture4ID;
		grcEffectVar	m_ShadowTexture5ID;
		grcEffectVar	m_ShadowTexture6ID;
		grcEffectVar	m_ShadowTexture7ID;
		grcEffectVar	m_ShadowTexture8ID;

		grcEffectVar	m_ShadowAttenuationID;
		grcEffectVar	m_ShadowVSMEplisionID;
		grcEffectVar	m_ShadowDepthBiasID;
		grcEffectVar	m_AmbientOcclusionFadeInID;
		grcEffectVar	m_MaxShadowFadeID;

		void CacheEffectVars( grmShader& effect );

		
	};
public:
	FastSpot4Lighting() : m_amtLights(0), m_setup( false )
	{
	}
	bool UsesFragment( grmShader& effect )
	{
		return ( effect.LookupVar( "FastSpotFadeScale" , false ) != 0 );
	}

	VarCache* CreateVariableCache()	{ return rage_new FastSpot4LightingVarCache; }

	void Reset()	{ m_setup = false; }

	void SetupShader( grmShader& effect, VarCache* cVars ) ;

	void SetupShaderPass() 
	{}

	int MaxFastLights()		{ return Max_Fast_Lights; }

	// public interface
	void SetLightsFast4( atArray<Light>& list, float AmbOccFadeIn  ) ;

	template<class SHADOWCACHE>
	void SetShadows( SHADOWCACHE& shadows, atArray<Light>& list );

	
private:
	static const int Max_Fast_Lights = 3;

	Vector4		m_positionX[ Max_Fast_Lights ];
	Vector4		m_positionY[ Max_Fast_Lights ];
	Vector4		m_positionZ[ Max_Fast_Lights ];

	Vector4		m_colR[ Max_Fast_Lights ];
	Vector4		m_colG[ Max_Fast_Lights ];
	Vector4		m_colB[ Max_Fast_Lights ];

	Vector4		m_direction[ Max_Fast_Lights * 3 ];
	Vector4		m_spotScale[ Max_Fast_Lights ];
	Vector4		m_spotOffset[ Max_Fast_Lights ];
	Vector4		m_spotDropOff[ Max_Fast_Lights ];
	Vector4		m_oneOverRangeSq[ Max_Fast_Lights ];
	int			m_amtLights;


	// variance shadows 
	Matrix44			m_ShadowMtx[ 4* Max_Fast_Lights];
	const grcTexture*	m_ShadowTex[  4 * Max_Fast_Lights];

	Vector4 m_ShadowAttenutation[Max_Fast_Lights];
	Vector4 m_ShadowVSMEplision[Max_Fast_Lights];
	Vector4 m_ShadowDepthBias[Max_Fast_Lights];

	Vector2 m_AmbientOcclusionFadeIn;
	Vector2	m_MaxShadowFade;

	bool	m_setup;
};

#define USE_COMPRESSED_DIRECTION 1 
// 
// /sample_lighting.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

struct LightGridGlobalVarCache
{
	bool m_set;
	grcEffectGlobalVar	m_sortScaleID;
	grcEffectGlobalVar	m_sortBiasID;
	grcEffectGlobalVar	m_sortTextureID;
	grcEffectGlobalVar	m_cellDivisionsTextureID;
public:
	LightGridGlobalVarCache() : m_set( false) {}

	void CacheEffectVars()
	{
		m_sortTextureID = grcEffect::LookupGlobalVar ("LightSortTexture" , false  );
		m_sortScaleID = grcEffect::LookupGlobalVar("LightSortScale", false  );
		m_sortBiasID = grcEffect::LookupGlobalVar("LightSortBias", false  );		
		m_cellDivisionsTextureID = grcEffect::LookupGlobalVar ("CellDividerTexture" , false  );	
		m_set = true;
	}
	void SetGlobals(	const grcTexture* lightGrid, 
						const grcTexture* cellDividerTexture, 
						const Vector3& min, 
						const Vector3& max )
	{
		if ( !m_set )
		{
			CacheEffectVars();
		}
		Vector3 sortScale = ( max - min );
		sortScale.InvertSafe();
		Vector3  sortBias = -min * sortScale;

		grcEffect::SetGlobalVar( m_sortTextureID, lightGrid );
		grcEffect::SetGlobalVar( m_sortScaleID, (Vec3V&)sortScale );
		grcEffect::SetGlobalVar( m_sortBiasID, (Vec3V&)sortBias );
		grcEffect::SetGlobalVar( m_cellDivisionsTextureID, cellDividerTexture );
	}
};

//---- shader controller for fast lighting
class FastLighting : public ShaderFragment
{
	struct FastLightingVarCache : public VarCache
	{
		grcEffectVar	m_positionXID;
		grcEffectVar	m_positionYID;
		grcEffectVar	m_positionZID;

		grcEffectVar	m_colorRID;
		grcEffectVar	m_colorGID;
		grcEffectVar	m_colorBID;

		grcEffectVar	m_intensityID;
		grcEffectVar	m_amtID;

		grcEffectVar	m_positionID;
		grcEffectVar	m_colorID;
		grcEffectVar	m_directionID;

		grcEffectVar	m_sortTextureID;
		grcEffectVar	m_sortTexture2ID;
		grcEffectVar	m_sortScaleID;
		grcEffectVar	m_sortBiasID;

		grcEffectVar	m_posTextureID;
		grcEffectVar	m_posScaleID;
		grcEffectVar	m_posBiasID;
		grcEffectVar	m_colTextureID;

		grcEffectVar	m_positionFillID;
		grcEffectVar	m_colorFillID;
		grcEffectVar	m_directionFillID;
		grcEffectVar	m_sortFillTextureID;

		void CacheEffectVars( grmShader& effect );

		
	};

	struct FastLightingGlobalVarCache
	{
		
		grcEffectGlobalVar	m_positionID;
		grcEffectGlobalVar	m_colorID;
		grcEffectGlobalVar	m_directionID;
		grcEffectGlobalVar  m_attenID;

		grcEffectGlobalVar	m_sortTextureID;
		grcEffectGlobalVar	m_sortScaleID;
		grcEffectGlobalVar	m_sortBiasID;

		grcEffectGlobalVar	m_posBiasID;
		grcEffectGlobalVar	m_posScaleID;

		grcEffectGlobalVar	m_posTextureID;
		grcEffectGlobalVar	m_colTextureID;

		grcEffectGlobalVar	m_posTexture2ID;
		grcEffectGlobalVar	m_colTexture2ID;

		grcEffectGlobalVar	m_cellDivisionsTextureID;
		grcEffectGlobalVar	m_heightTextureID;
		grcEffectGlobalVar	m_envMapTextureID;

		bool				m_set;

		FastLightingGlobalVarCache() : m_set(false)
		{}
		void CacheEffectVars()
		{

			m_positionID = grcEffect::LookupGlobalVar( "LightPositions" );
			m_colorID = grcEffect::LookupGlobalVar("LightColors");
#if !USE_COMPRESSED_DIRECTION
			m_directionID = grcEffect::LookupGlobalVar( "LightDirections" );
#endif
			m_attenID  = grcEffect::LookupGlobalVar( "LightAttenuations", false );
			
			m_sortTextureID = grcEffect::LookupGlobalVar ("LightSortTexture" , false  );
			m_sortScaleID = grcEffect::LookupGlobalVar("LightSortScale", false  );
			m_sortBiasID = grcEffect::LookupGlobalVar("LightSortBias", false  );

			m_cellDivisionsTextureID = grcEffect::LookupGlobalVar ("CellDividerTexture" , false  );
			m_posTextureID = grcEffect::LookupGlobalVar("PositionTexture", false  );
			m_colTextureID = grcEffect::LookupGlobalVar("ColorT" , false  );

			m_posTexture2ID = grcEffect::LookupGlobalVar("PositionTextureFill", false  );
			m_colTexture2ID = grcEffect::LookupGlobalVar("ColorTFill" , false  );

			m_posBiasID = grcEffect::LookupGlobalVar("PositionBias", false  );
			m_posScaleID = grcEffect::LookupGlobalVar("PositionScale", false  );

			m_heightTextureID = grcEffect::LookupGlobalVar("HeightTexture", false );
			m_envMapTextureID = grcEffect::LookupGlobalVar("GlobalEnvironmentMap", false );
		
			
//			m_positionID = m_colorID = grcegvNONE;

			m_set = true;
		}
		bool IsSet() { return m_set; }
	};

	FastLightingGlobalVarCache  m_GVars;
public:
	FastLighting() : m_amtLights(0), m_sortedTexture(0 ), m_posTexture(0),m_heightMap(0)
	{}

	bool UsesFragment( grmShader& /*effect*/ )
	{
		return false;//	return ( effect.LookupVar( "LightDirections" , false ) != 0 );
	}

	VarCache* CreateVariableCache()	{ return rage_new FastLightingVarCache; }

	void SetGlobals();
	void SetupShader( grmShader& effect, VarCache* cVars ) ;

	void SetSortTexture( const grcTexture* tex, const grcTexture* tex2, const grcTexture* sortedFillTexture, const Vector3& min, const Vector3& max )	;
	void SetPositionalTexture(  const grcTexture* position, const grcTexture* colors, const grcTexture* cellDividerTexture, const grcTexture* position2, const grcTexture* colors2);
	void SetHeightMap( const grcTexture* tex ) { m_heightMap  = tex; }
	void SetRayTracedReflections( const grcTexture* heights, const grcTexture* envMap ) { m_heightMap  = heights; m_envMap = envMap; }


	void SetupShaderPass() 
	{}

	int MaxFastLights()		{ return Max_Fast_Lights; }

	void SetLights( atArray<Light>& list , bool isDirect );

	// public interface
	void SetLightsFast4( atArray<Light>& list  );

private:
	static const int Max_Fast_Lights = 4;
	static const int Max_Normal_Lights = 64;

	Vector4		m_positionX[ Max_Fast_Lights ];
	Vector4		m_positionY[ Max_Fast_Lights ];
	Vector4		m_positionZ[ Max_Fast_Lights ];

	Vector4		m_colR[ Max_Fast_Lights ];
	Vector4		m_colG[ Max_Fast_Lights ];
	Vector4		m_colB[ Max_Fast_Lights ];

	Vector4		m_oneOverRangeSq[ Max_Fast_Lights ];

	struct LightVals
	{
		Vector4		m_position[ Max_Normal_Lights ];
		Vector4		m_color[ Max_Normal_Lights ];
		Vector4		m_directions[ Max_Normal_Lights ];
		Vector4		m_attenuation[ Max_Normal_Lights ];
		int			m_amtNormalLights;
	};

	LightVals	vals[2];

	int			m_amtLights;

	int			m_numKdNodes;
	int			m_leafCount;

	const grcTexture*	m_sortedTexture;
	const grcTexture*	m_sortedTexture2;
	const grcTexture* m_sortedFillTexture;

	Vector3		m_sortScale;
	Vector3		m_sortBias;

	const grcTexture*	m_posTexture;
	const grcTexture*	m_colTexture;
	const grcTexture*	m_posTexture2;
	const grcTexture*	m_colTexture2;
	const grcTexture*	m_heightMap;
	const grcTexture*	m_envMap;


	const grcTexture*	m_cellDividerTexture;

	bool		m_useShadows;

	// compression stuff
	Vector3 m_posScale;
	Vector3 m_posBias;
};

class ManyShadows: public ShaderFragment
{
	struct ManyShadowsVarCache : public VarCache
	{
		grcEffectVar	ShadowMatrixXID;
		grcEffectVar	ShadowMatrixYID;
		grcEffectVar	ShadowMatrixWID;

		grcEffectVar	ShadowMatCompXID;

		grcEffectVar	ShadowSettingsID;
		grcEffectVar	ShadowTextureID;
		grcEffectVar	ShadowTexScaleID;
		grcEffectVar	m_MaxShadowFadeID;
		grcEffectVar	m_AmbientOcclusionFadeInID;
		grcEffectVar	m_BumpinessID;

		grcEffectVar	m_DirectionDirID;
		grcEffectVar	m_DirectionColID;


		void CacheEffectVars( grmShader& effect )
		{
			ShadowMatrixXID = effect.LookupVar("ShadowMatrixX" );
			ShadowMatrixYID = effect.LookupVar("ShadowMatrixY" );
			//ShadowMatrixWID = effect.LookupVar("ShadowMatrixW" );
			//ShadowMatCompXID = effect.LookupVar("ShadowMatCompX");
			ShadowSettingsID = effect.LookupVar("ShadowSettings" );
			ShadowTextureID =  effect.LookupVar("ShadowAtlasTexture" );
			ShadowTexScaleID = effect.LookupVar("ShadowTexScale" );
			m_MaxShadowFadeID = effect.LookupVar("MaxShadowFade");
			m_AmbientOcclusionFadeInID = effect.LookupVar( "AmbientOcclusionFadeIn");
			m_BumpinessID = effect.LookupVar("Bumpiness");
			m_DirectionDirID = effect.LookupVar("DirectionDir");
			m_DirectionColID = effect.LookupVar("DirectionCol");
		}
	};
	struct ManyShadowsGlobalVarCache
	{
		grcEffectGlobalVar	ShadowMatrixXID;
		grcEffectGlobalVar	ShadowMatrixYID;

		grcEffectGlobalVar	ShadowSettingsID;
		grcEffectGlobalVar	ShadowTextureID;
		grcEffectGlobalVar	m_MaxShadowFadeID;
		bool				m_set;

		ManyShadowsGlobalVarCache() : m_set(false)
		{}
		void CacheEffectVars()
		{
			ShadowMatrixXID = grcEffect::LookupGlobalVar( "manylights_ShadowMatrixX" );
			ShadowMatrixYID = grcEffect::LookupGlobalVar( "manylights_ShadowMatrixY" );
			ShadowSettingsID = grcEffect::LookupGlobalVar( "manylights_ShadowSettings" );
			ShadowTextureID = grcEffect::LookupGlobalVar( "ShadowAtlasTexture" );
			m_MaxShadowFadeID = grcEffect::LookupGlobalVar( "MaxShadowFade");

			m_set = true;
		}
		bool IsSet() { return m_set; }
	};
	ManyShadowsGlobalVarCache	m_GVars;

public:
	ManyShadows() : m_amtLights(0), m_shadowTexture(0)
	{
	}
	bool UsesFragment( grmShader& /*effect*/ )
	{
		return false; //return ( effect.LookupVar( "ShadowSettings" , false ) != 0 );
	}

	VarCache* CreateVariableCache()	{ return rage_new ManyShadowsVarCache; }

	void SetGlobals();

	void SetupShader( grmShader& effect, VarCache* cVars ); 


	void SetupShaderPass() 
	{}

	//template<class SHADOWCACHE>
	//void SetShadows( LightList& lights, SHADOWCACHE& shadows, atArray<Light>& list  , float ambOcc, float bump ); 
	
	template<class SHADOWCACHE, class LIGHTSLIST>
	void SetShadows( LIGHTSLIST& lights, SHADOWCACHE& shadows , float ambOcc, float bump ) 
	{
		m_shadowTexture = shadows.GetTexture();

		int cnt = Min(lights.GetCount(), 29);

		for ( int i = 0; i < cnt; i++ )
		{
			const CShadowedSpotLight& spotShadow = shadows[i]; 
			Matrix44	mat = spotShadow.GetMatrix();
			mat.Transpose();

			ShadowMatrixX[i] = mat.a;
			ShadowMatrixY[i] = mat.b;
			ShadowMatrixW[i] = mat.d;

			Vector4		settings( 1.0f/ spotShadow.GetRange(), 0.0f, 0.0f, 0.0f);
			settings.y = -(settings.x * spotShadow.GetStart() + lights[i].GetShadowBias());
			lights[i].CalculateSpotScaleBias( settings.z, settings.w);
			float compZ;
			ShadowMatCompX[i] = spotShadow.GetCompressedSpotMatrix( compZ);
			ShadowSettings[i] = settings;
		}
		m_shadScale = shadows.GetShadowTexScale();
		m_shadowFade = shadows.GetSpotShadowMaxFade();
		m_amtLights = lights.GetCount();

		m_AmbientOcclusionFadeIn.x = ambOcc;
		m_AmbientOcclusionFadeIn.y = 1.0f - ambOcc;
		m_bump = bump;

		lights.GetDirectional( m_dir, m_dirCol );

	}


private:
	static const int Max_Normal_Lights = 64;

	Vector4		ShadowMatrixX[ Max_Normal_Lights];
	Vector4		ShadowMatrixY[ Max_Normal_Lights];
	Vector4		ShadowMatrixW[ Max_Normal_Lights];
	Vector4		 ShadowSettings[Max_Normal_Lights];
	Vector2		m_shadScale;
	Vector2		m_shadowFade;
	Vector2		m_AmbientOcclusionFadeIn;

	Vector4		ShadowMatCompX[ Max_Normal_Lights];

	Vector3		m_dir;
	Vector3		m_dirCol;

	const grcTexture*	 m_shadowTexture;
	int			m_amtLights;
	float		m_bump;
};


class LightingComposite: public ShaderFragment
{
	struct LightingCompositeVarCache : public VarCache
	{


		grcEffectVar	AmbientOcclusionScaleID;
		grcEffectVar	BounceStrengthID;
		grcEffectVar	EnvironmentStrengthID;
		grcEffectVar	DirectStrengthID;
		grcEffectVar	DisplayLightingOnlyID;
		grcEffectVar	skyColorID;
		grcEffectVar	grdColorID;
		grcEffectVar	bounceFallOffID;

		void CacheEffectVars( grmShader& effect )
		{
			AmbientOcclusionScaleID = effect.LookupVar("AmbientOcclusionScale" );
			BounceStrengthID = effect.LookupVar("BounceStrength" );
		//	EnvironmentStrengthID = effect.LookupVar("EnvironmentStrength" );
		//	DirectStrengthID =  effect.LookupVar("DirectStrength" );
			DisplayLightingOnlyID = effect.LookupVar("DisplayLightingOnly" );
			skyColorID = effect.LookupVar("SkyColor");
			grdColorID = effect.LookupVar("GroundColor");
		//	bounceFallOffID = effect.LookupVar("BounceFallOff", false);
		}
	};
	struct LightingCompositeGlobalVarCache
	{
		grcEffectGlobalVar	skyColorID;
		grcEffectGlobalVar	grdColorID;

		bool				m_set;

		LightingCompositeGlobalVarCache() : m_set(false)
		{}
		void CacheEffectVars()
		{
			skyColorID = grcEffect::LookupGlobalVar( "manylights_SkyColor" );
			grdColorID = grcEffect::LookupGlobalVar( "manylights_GroundColor" );
			m_set = true;
		}
		bool IsSet() { return m_set; }
	};
	LightingCompositeGlobalVarCache	m_GVars;
public:
	LightingComposite() 
		:	m_ambOccScale( 1.0f, 0.1f ),
		m_bounceStrength( 2.5f),
		m_envStrength( 0.3f ),
		m_direct( 1.0f ),
		m_lightingOnly( false ),
		m_GroundColor(0.02f, 0.02f, 0.2f ),
		m_SkyColor(0.05f, 0.05f, 0.0f )
	{
	}
	bool UsesFragment( grmShader& /*effect*/ )
	{
		return false;//return ( effect.LookupVar( "BounceStrength" , false ) != 0 );
	}

	VarCache* CreateVariableCache()	{ return rage_new LightingCompositeVarCache; }

	void SetGlobals()
	{
		if (!m_GVars.IsSet() )
		{
			m_GVars.CacheEffectVars();	
		}
		Vec4f sky( m_SkyColor.x, m_SkyColor.y, m_SkyColor.z, m_direct);
		Vec4f grd( m_GroundColor.x, m_GroundColor.y, m_GroundColor.z, m_envStrength);
		grcEffect::SetGlobalVar( m_GVars.skyColorID, sky );
		grcEffect::SetGlobalVar( m_GVars.grdColorID, grd );
	}
	void SetupShader( grmShader& effect, VarCache* cVars ) 
	{
		Assert( cVars );
		LightingCompositeVarCache* var = reinterpret_cast<LightingCompositeVarCache*>(cVars);

		effect.SetVar( var->AmbientOcclusionScaleID, m_ambOccScale );
		effect.SetVar( var->BounceStrengthID,  m_bounceStrength );
	//	effect.SetVar( var->EnvironmentStrengthID, m_envStrength );
	//	effect.SetVar( var->DirectStrengthID, m_direct );
		effect.SetVar( var->DisplayLightingOnlyID, m_lightingOnly ? 1.0f : 0.0f );
		effect.SetVar( var->DirectStrengthID, m_direct );
		effect.SetVar( var->DisplayLightingOnlyID, m_lightingOnly ? 1.0f : 0.0f );
		effect.SetVar( var->skyColorID, m_SkyColor );
		effect.SetVar( var->grdColorID, m_GroundColor );
	//	effect.SetVar( var->bounceFallOffID, 1.0f/m_bounceFallOff );
	}


	void SetupShaderPass() 
	{}

#if __BANK
	void AddWidgets(bkBank* bank )	
	{

		bank->PushGroup("Compositing Lighting Layers");			// add lighting group
		bank->AddSlider( "Ambient Occlusion Scale", &m_ambOccScale.x, 0.001f, 2.0f, 0.01f );
		bank->AddSlider( "Ambient Occlusion Offset", &m_ambOccScale.y, 0.0f, 1.0f, 0.01f );
		bank->AddSlider( "Bounce Light Strength", &m_bounceStrength, 0.01f, 5.0f, 0.01f );
		bank->AddSlider( "Environment Light Strength", &m_envStrength, 0.01f, 5.0f, 0.01f );
		bank->AddSlider( "Direct Light Strength", &m_direct, 0.01f, 5.0f, 0.01f );
		bank->AddToggle( "Display Lighting only", &m_lightingOnly );

		bank->AddColor( "Env Sky Color", &m_SkyColor );
		bank->AddColor( "Env Ground Color", &m_GroundColor );

		//bank->AddSlider( "m_bounceFallOff", &m_bounceFallOff, 0.01f, 100.0f, 0.01f );
		
		bank->PopGroup();
	}
#endif

private:
	Vector3	m_SkyColor;
	Vector3 m_GroundColor;
	Vector2	m_ambOccScale;
	float	m_bounceStrength;
	float	m_envStrength;
	float	m_direct;
	bool	m_lightingOnly;

};

};

#endif
