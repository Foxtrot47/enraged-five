// 
// sample_lighting/lightfog.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Note: creates a 2D screenspace texture that holds all shadow relevant data
//

#ifndef SAMPLE_LIGHTING_LIGHTFOG_H
#define SAMPLE_LIGHTING_LIGHTFOG_H


#include "grmodel/curvedmodel.h"

#include "simpleLightManager.h"
#include <algorithm>
#include <atl/IterativeCombSorter.h>

namespace rage
{
	class Light;


	// PURPOSE: renders a cone of around a light to simulate atmospheric scattering due to the light
	//
	class SpotLightFogRenderer
	{
		typedef		std::pair<float, Light* >		SortLightStruct;
		struct backToFrontSort
		{
			bool operator()( SortLightStruct& a, SortLightStruct& b )
			{
				return a.first > b.first;
			}
		};

		
	public:
		SpotLightFogRenderer(grmShader*);
		
		
		void SetTechnique( const char* tech  );
		void SetMinClip( float v )	{ m_minClip = Max( v, 0.001f); }
		void SetMaxClip( float v )	{ m_maxClip = v; }

		void Set( Light& light, float volumeIntensity, float trans );

		void render()
		{
			Assert( m_Shader );
			m_cone.render( m_Shader, m_technique );
		}

		void SetClipIntensity( float clip )	{ m_clipIntensity = clip; }

		bool HasVolume( Light& light, float& v );

		void Render( Light& light, float volumeThickness, float trans = 1.0f)
		{
			if ( light.Type == Light::SPOT )
			{
				Set( light, volumeThickness, trans);
				Assert( m_Shader );
				m_cone.render( m_Shader, m_technique );
			}
			// TODO : add point light sphere support
		}

		
		template<class T>
		void CullSortRender( T& lights, float volumeIntensity, float trans = 1.0f )
		{
			IterativeCombSorter		sorter;
			SortLightStruct			sortLights[128];
			Assert( 128 > lights.GetCount() );

			int sortCount = 0;
			for ( int i =0; i < lights.GetCount(); i++ )
			{
				Light& light = lights[i];
				float outZ;

				if ( HasVolume( light, outZ ))
				{
					sortLights[ sortCount++ ] = std::make_pair( outZ, &light );
				}
			}
			// depth sort back to front
			 backToFrontSort	sortOrder;
			sorter.Sort( sortLights,  sortOrder,sortCount );
			for ( int i = 0; i < sortCount; i++ )
			{
				const Volume* vol = sortLights[i].second->GetLightFogVolume();
				Assert( vol );
				SetMinClip( vol->FogStart );
				SetMaxClip( vol->FogEnd );

				Set( *sortLights[i].second, vol->FogDensity * volumeIntensity, vol->Transparency * trans  );
				m_cone.render( m_technique );
			}
		}


	private:

		TubeModel			m_cone;
		grcEffectVar		m_PositionVar;
		grcEffectVar		m_ColorVar;
		grcEffectVar		m_DirectionVar;
		grcEffectVar		m_VolumeVar;

		grcEffectVar		m_TangentVar;
		grcEffectVar		m_BinormalVar;

		grcEffectVar		m_OffsetVar;
		grcEffectVar		m_Ray;

		grcEffectVar		m_MtxVar;
		grcEffectVar		m_InvMtxVar;

		float				m_minClip;
		float				m_maxClip;



		// 
		float										m_clipIntensity;
		grcEffectTechnique m_technique;
		grmShader*			m_Shader;

	};
};


#endif
