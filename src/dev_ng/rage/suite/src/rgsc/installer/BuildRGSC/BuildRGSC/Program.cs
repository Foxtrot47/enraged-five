﻿using System;
using System.IO;
using System.Diagnostics;



namespace BuildRGSC
{
    class Program
    {
        private static int intCount = 0;
        private static StreamWriter StreamFile;
        private static bool bDebugBuild = false;

        static void Main(string[] args)
        {
            // check for -debug flag
            if (Array.Exists(args, element => element == "-debug"))
            {
                bDebugBuild = true;
            }

            /*
            if (bDebugBuild)
            {
                BuildFileList(@"C:\Program Files (x86)\Rockstar Games\Social Club Debug", "x86");
                BuildFileList(@"C:\Program Files\Rockstar Games\Social Club Debug", "x64");
                BuildDeleteFileList(@"C:\Program Files (x86)\Rockstar Games\Social Club Debug", "x86");
                BuildDeleteFileList(@"C:\Program Files\Rockstar Games\Social Club Debug", "x64");
            }
            else
            {
                BuildFileList(@"C:\Program Files (x86)\Rockstar Games\Social Club", "x86");
                BuildFileList(@"C:\Program Files\Rockstar Games\Social Club", "x64");
                BuildDeleteFileList(@"C:\Program Files (x86)\Rockstar Games\Social Club", "x86");
                BuildDeleteFileList(@"C:\Program Files\Rockstar Games\Social Club", "x64");
            }
            */

            if (Directory.Exists(args[1]))
            {
                WriteCompileHeader(args[1], args[3]);
                BuildFileList(args[1], "x86");
                BuildFileList(args[2], "x64");
                BuildDeleteFileList(args[1], "x86");
                BuildDeleteFileList(args[2], "x64");
            }

            if (File.Exists(args[0]))
            {
                MakeNSISProject(args[0]);
            }
        }

        static void WriteCompileHeader(String sDataLoc, string strBuildLoc)
        {
            Console.WriteLine("Writing compile header.");
            Console.WriteLine("  Data Loc: \"" + sDataLoc + "\"");
            Console.WriteLine("  Build Loc: \"" + strBuildLoc + "\"");
            Console.WriteLine("");
            Console.WriteLine("Files:");

            string scDllPath = "";
            foreach (string strFile in Directory.GetFiles(sDataLoc))
            {
                Console.WriteLine(" + " + strFile.ToString());

                string[] strDll = strFile.Split('\\');
                string strFileName = strDll[strDll.Length - 1];

                if (string.Compare(strFileName.ToLower(), "socialclub.dll") == 0)
                {
                    scDllPath = strFile;
                }
            }

            if (!String.IsNullOrEmpty(scDllPath))
            {
                FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(scDllPath);

                if (myFileVersionInfo.FileVersion != null)
                {
                    string[] strVersion = myFileVersionInfo.FileVersion.Split(',');
                    WriteNSISFileINFO(sDataLoc, strVersion, strBuildLoc);
                }
            }

            Console.WriteLine("");
        }

        static void MakeNSISProject(string strProject)
        {
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.FileName = strProject;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.OutputDataReceived += (sender, args) => Console.WriteLine(args.Data);

            if (bDebugBuild)
            {
                proc.StartInfo.Arguments = Directory.GetCurrentDirectory() + "\\RGSCInstaller_v1.2_Dev.nsi";
            }
            else
            {
                proc.StartInfo.Arguments = Directory.GetCurrentDirectory() + "\\RGSCInstaller_v1.2.nsi";
            }

            Console.WriteLine("Making NSIS Project: ");
            Console.WriteLine("  " + proc.StartInfo.Arguments);
            Console.WriteLine("");

            //proc.StartInfo.Arguments = "X:\\rage\\dev\\rage\\suite\\src\\rgsc\\installer\\RGSCInstaller_v1.2.nsi";

            proc.Start();
            proc.BeginOutputReadLine();
            proc.WaitForExit();
            proc.Close();
        }


        static void WriteNSISFileINFO(string strDataLoc, string[] strVer, string strBuildLoc)
        {
            Console.WriteLine("");
            Console.WriteLine("Writing NSIS File Info:");

            string NSISFILENAME = Directory.GetCurrentDirectory() + "\\Global\\Includes\\CompileInfo.nsh";
            Console.WriteLine(" + " + NSISFILENAME);
            //string NSISFILENAME = "X:\\rage\\dev\\rage\\suite\\src\\rgsc\\installer\\Global\\Includes\\CompileInfo.nsh";

            if (!Directory.Exists(strBuildLoc))
                Directory.CreateDirectory(strBuildLoc);

            string strBuildLocDebug = strBuildLoc + "\\Debug";
            if (!Directory.Exists(strBuildLocDebug))
                Directory.CreateDirectory(strBuildLocDebug);

            if (!File.Exists(NSISFILENAME))
                File.Create(NSISFILENAME);

            File.SetAttributes(NSISFILENAME, FileAttributes.Normal);
            StreamWriter CompileInfoFile = new StreamWriter(NSISFILENAME, false, System.Text.Encoding.ASCII, 512);

            CompileInfoFile.Write("# General Symbol Definitions\n");
            CompileInfoFile.Write(";###################################################\n");
            CompileInfoFile.Write(";######### Needs to Match socialclub.dll ###########\n");
            CompileInfoFile.Write(";###################################################\n");
            
            if (bDebugBuild)
            {
                CompileInfoFile.Write("!define FILENAME \"" + strBuildLoc + "\\Debug\\Social-Club-Debug");
            }
            else
            {
                CompileInfoFile.Write("!define FILENAME \"" + strBuildLoc + "\\Social-Club");
            }

            CompileInfoFile.Write("-Setup.exe\"\n!define VERSION ");
            CompileInfoFile.Write(strVer[0]);

            for (int i = 1; i < strVer.Length; i++)
            {
                CompileInfoFile.Write("." + strVer[i].TrimStart(' '));
            }

            //CompileInfoFile.Write("\n!define DATALOC \"" + strDataLoc + "\"\n");
            CompileInfoFile.Close();
        }

        static void BuildFileList(string sDataLoc, string strBitFolder)
        {
            string FILELISTNAME = Directory.GetCurrentDirectory() + "\\Global\\Includes\\FileList" + strBitFolder + ".nsh";

            if (File.Exists(FILELISTNAME))
                File.SetAttributes(FILELISTNAME, FileAttributes.Normal);

            Console.WriteLine("Building file list");
            Console.WriteLine(" File Name: " + FILELISTNAME);
            Console.WriteLine(" Build Type: " + (bDebugBuild ? "Debug " : "Release ") + strBitFolder);

            StreamFile = new StreamWriter(FILELISTNAME, false, System.Text.Encoding.Unicode, 512);

            StreamFile.WriteLine("Function AddUpdateFiles" + strBitFolder);
            StreamFile.WriteLine("Pop $0\n");
            StreamFile.WriteLine("SetOverwrite on\n");
            StreamFile.WriteLine("SetOutPath $0");

            foreach (string f in Directory.GetFiles(sDataLoc, "*.*"))
            {
                StreamFile.WriteLine("!insertmacro CRCFile \"" + f.Substring(sDataLoc.Length) + "\" \"" + sDataLoc + "\"");

                intCount += 1;
            }

            StreamFile.WriteLine("\n");
            GenerateFileList(sDataLoc, sDataLoc);

            StreamFile.WriteLine("SetOutPath $0\n");
            StreamFile.WriteLine("Push $1\n");
            StreamFile.WriteLine("FunctionEnd");

            StreamFile.Flush();
            StreamFile.Close();

            Console.WriteLine("");
        }


        static void GenerateFileList(string sDir, string sParent)
        {
            int initCount;

            try
            {
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    initCount = intCount;

                    foreach (string f in Directory.GetFiles(d, "*.*"))
                    {
                        FileStream fsFile = File.OpenRead(f);

                        intCount += 1;

                        if (intCount == initCount + 1)
                        {
                            StreamFile.WriteLine("SetOutPath \"$0" + d.Substring(sParent.Length) + "\"");
                        }
                        //StreamFile.WriteLine("!insertmacro CRCFile $1 $0 \"" + d.Substring(sParent.Length) + "\\" + f.Substring(d.Length + 1) + "\" " + "\"" + d + "\"");
                        StreamFile.WriteLine("!insertmacro CRCFile \"\\" + f.Substring(d.Length + 1) + "\" " + "\"" + d + "\"");

                        fsFile.Close();
                    }

                    StreamFile.WriteLine("\n");

                    GenerateFileList(d, sParent);
                }
            }
            catch (System.Exception /*excpt*/)
            {
                // AddText(excpt.Message, true);
            }
        }

        static void BuildDeleteFileList(string sDataLoc, string strBitFolder)
        {
            string FILELISTNAME = Directory.GetCurrentDirectory() + "\\Global\\Includes\\DeleteFileList" + strBitFolder + ".nsh";

            if (File.Exists(FILELISTNAME))
                File.SetAttributes(FILELISTNAME, FileAttributes.Normal);

            Console.WriteLine("Building delete list");
            Console.WriteLine(" File Name: " + FILELISTNAME);
            Console.WriteLine(" Build Type: " + (bDebugBuild ? "Debug " : "Release ") + strBitFolder);

            StreamFile = new StreamWriter(FILELISTNAME, false, System.Text.Encoding.Unicode, 512);

            StreamFile.WriteLine("Function un.AddDeleteFiles" + strBitFolder);
            StreamFile.WriteLine("Pop $0\n");

            foreach (string f in Directory.GetFiles(sDataLoc, "*.*"))
            {
                StreamFile.WriteLine("!insertmacro DeleteFile \"" + f.Substring(sDataLoc.Length) + "\" $0");

                intCount += 1;
            }

            StreamFile.WriteLine("\n");
            GenerateDeleteFileList(sDataLoc, sDataLoc);
            StreamFile.WriteLine("FunctionEnd");

            StreamFile.Flush();
            StreamFile.Close();
            Console.WriteLine("");
        }

        static void GenerateDeleteFileList(string sDir, string sParent)
        {
            int initCount;

            try
            {
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    initCount = intCount;

                    foreach (string f in Directory.GetFiles(d, "*.*"))
                    {
                        FileStream fsFile = File.OpenRead(f);

                        intCount += 1;
                        StreamFile.WriteLine("!insertmacro DeleteFile \"\\" + f.Substring(d.Length + 1) + "\" \"$0" + d.Substring(sParent.Length) + "\"");

                        fsFile.Close();
                    }

                    StreamFile.WriteLine("\n");

                    GenerateDeleteFileList(d, sParent);
                }
            }
            catch (System.Exception /*excpt*/)
            {
                // AddText(excpt.Message, true);
            }
        }
    }
}
