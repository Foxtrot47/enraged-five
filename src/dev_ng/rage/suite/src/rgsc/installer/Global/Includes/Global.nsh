!include "LogicLib.nsh"                       ; Used for Syntax
!include "WinMessages.nsh"                  
!include "WinVer.nsh"
!include "WordFunc.nsh"

!insertmacro WordFind

!define GIS_NOT_INSTALLED   0 # might be wrong value
!define GIS_CURRENT_USER    1 # might be wrong value
!define GIS_ALL_USERS       2 # might be wrong value
!define GIS_ADMIN           3 # might be wrong value

!define StrStr "!insertmacro StrStr"
 
!macro StrStr ResultVar String SubString
  Push ${String}
  Push ${SubString}
  Call StrStr
  Pop  ${ResultVar}
!macroend

;!define DEBUG_MESSAGES      1

;var VersionInstall0
;var VersionInstall1
;var VersionInstall2
;var VersionInstall3
 
;var VersionReg0
;var VersionReg1
;var VersionReg2
;var VersionReg3

Var IsLanguageArgsSet		
Var ProcessWaitFail
Var STR_HAYSTACK
Var STR_NEEDLE
Var CHAR_COUNTER
Var ARG_LENGTH
Var STR_PROC
Var ARGS_LIST_LENGTH
Var STR_RETURN_VAR

!macro CRCFILE LocFile FileName
    FILE "${FileName}${LocFile}"   
!macroend  

!macro DELETEFILE LocFile FileName
	IfFileExists "${FileName}${LocFile}" 0 +2 
    DELETE /REBOOTOK "${FileName}${LocFile}"   
!macroend  

Function RemoveOldData
	pop $R0

	;MessageBox MB_OK "$R0 ${RemoveFile}"

	DetailPrint "$RemoveFile"
	RMDir /r $R0
FunctionEnd

!macro _StrContains INDEX VALUE NEEDLE HAYSTACK
  Push "${HAYSTACK}"
  Push "${NEEDLE}"
  Call cStrContains
  Pop "${INDEX}"
  pop "${VALUE}"
!macroend

!define StrContains '!insertmacro "_StrContains"'

!macro IfKeyExists ROOT MAIN_KEY KEY
  Push $R0
  Push $R1
  Push $R2
 
  # XXX bug if ${ROOT}, ${MAIN_KEY} or ${KEY} use $R0 or $R1
 
  StrCpy $R1 "0" # loop index
  StrCpy $R2 "0" # not found
 
  ${Do}
    EnumRegKey $R0 ${ROOT} "${MAIN_KEY}" "$R1"
    ${If} $R0 == "${KEY}"
      StrCpy $R2 "1" # found
      ${Break}
    ${EndIf}
    IntOp $R1 $R1 + 1
  ${LoopWhile} $R0 != ""
 
  ClearErrors
 
  Exch 2
  Pop $R0
  Pop $R1
  Exch $R2
!macroend

 ; GetParameters
 ; input, none
 ; output, top of stack (replaces, with e.g. whatever)
 ; modifies no other variables.
 
Function GetParameters
 
  Push $R0
  Push $R1
  Push $R2
  Push $R3
 
  StrCpy $R2 1
  StrLen $R3 $CMDLINE
 
  ;Check for quote or space
  StrCpy $R0 $CMDLINE $R2
  StrCmp $R0 '"' 0 +3
    StrCpy $R1 '"'
    Goto loop
  StrCpy $R1 " "
 
  loop:
    IntOp $R2 $R2 + 1
    StrCpy $R0 $CMDLINE 1 $R2
    StrCmp $R0 $R1 get
    StrCmp $R2 $R3 get
    Goto loop
 
  get:
    IntOp $R2 $R2 + 1
    StrCpy $R0 $CMDLINE 1 $R2
    StrCmp $R0 " " get
    StrCpy $R0 $CMDLINE "" $R2
 
  Pop $R3
  Pop $R2
  Pop $R1
  Exch $R0
 
FunctionEnd

Function CheckForLanguageArgs	
	Call GetParameters
	pop $1

	${StrContains} $R0 $R1 "1033" $1		;English
	StrCmp $R1 "1033" Set_English
	
	${StrContains} $R0 $R1 "1036" $1		;French
	StrCmp $R1 "1036" Set_French
	
	${StrContains} $R0 $R1 "1040" $1		;Italian
	StrCmp $R1 "1040" Set_Italian
	
	${StrContains} $R0 $R1 "3082" $1		;Spanish
	StrCmp $R1 "3082" Set_Spanish
	
	${StrContains} $R0 $R1 "1031" $1		;German
	StrCmp $R1 "1031" Set_German
	
	${StrContains} $R0 $R1 "1045" $1		;Polish
	StrCmp $R1 "1045" Set_Polish
	
	${StrContains} $R0 $R1 "1046" $1		;Portuguese
	StrCmp $R1 "1046" Set_Portuguese

	${StrContains} $R0 $R1 "1049" $1		;Russian
	StrCmp $R1 "1049" Set_Russian
	
	${StrContains} $R0 $R1 "1042" $1		;Korean
	StrCmp $R1 "1042" Set_Korean
	
	${StrContains} $R0 $R1 "1041" $1		;Japan
	StrCmp $R1 "1041" Set_Japan
	
	${StrContains} $R0 $R1 "2058" $1		;Mexican
	StrCmp $R1 "2058" Set_Mexican

	${StrContains} $R0 $R1 "0804" $1		;Traditional Chinese
	StrCmp $R1 "0804" Set_TradChinese
	
	${StrContains} $R0 $R1 "2052" $1		;Simplified Chinese
	StrCmp $R1 "2052" Set_SimpChinese

	goto CheckForLanguageArgsEnd

Set_English:
	StrCpy $LANGUAGE 1033 		;English
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_French:
    StrCpy $LANGUAGE 1036		;French
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_Italian:
    StrCpy $LANGUAGE 1040		;Italian
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_Spanish:
    StrCpy $LANGUAGE 3082		;Spanish
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_German:
    StrCpy $LANGUAGE 1031		;German
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_Russian:
    StrCpy $LANGUAGE 1049		;Russian
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_Polish:
	StrCpy $LANGUAGE 1045		;Polish
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_Portuguese:
	StrCpy $LANGUAGE 1046		;Portuguese
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_Korean:
	;messageBox MB_OK "Korean"
    StrCpy $LANGUAGE 1042		;Korean
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_Japan:
    StrCpy $LANGUAGE 1041		;Japan
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_Mexican:
    StrCpy $LANGUAGE 2058		;Mexican
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_TradChinese:
    StrCpy $LANGUAGE 0804		;TradChinese
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_SimpChinese:
    StrCpy $LANGUAGE 2052		;SimpChinese
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
	
	
CheckForLanguageArgsEnd: 

FunctionEnd

Function IsSilent
  
  Call GetParameters
  Pop  $1
  
  ${StrContains} $R0 $R1 "Silent" $1
  StrCmp $R1 "Silent" Silent
  
  ${StrContains} $R0 $R1 "?" $1
  StrCmp $R1 "?" CmdLineArgs IsSilentEND
  
  Silent: 
	SetSilent silent
    ;Banner::show /NOUNLOAD $InstallMsg 
    Goto IsSilentEND
    
CmdLineArgs:
	MessageBox MB_OK "/Silent	-Silent install$\n/?	- info$\n/1033	- English$\n/1036	- Français$\n/1040	- Italiano$\n/3082	- Español$\n/1031	- Deutsch$\n/1049	- Русско$\n/1041	- 日本語$\n/2052	   - 简体中文"
	Abort 
         
IsSilentEND: 

FunctionEnd

Function StrStr
/*After this point:
  ------------------------------------------
  $R0 = SubString (input)
  $R1 = String (input)
  $R2 = SubStringLen (temp)
  $R3 = StrLen (temp)
  $R4 = StartCharPos (temp)
  $R5 = TempStr (temp)*/
 
  ;Get input from user
  Exch $R0
  Exch
  Exch $R1
  Push $R2
  Push $R3
  Push $R4
  Push $R5
 
  ;Get "String" and "SubString" length
  StrLen $R2 $R0
  StrLen $R3 $R1
  ;Start "StartCharPos" counter
  StrCpy $R4 0
 
  ;Loop until "SubString" is found or "String" reaches its end
  ${Do}
    ;Remove everything before and after the searched part ("TempStr")
    StrCpy $R5 $R1 $R2 $R4
 
    ;Compare "TempStr" with "SubString"
    ${IfThen} $R5 == $R0 ${|} ${ExitDo} ${|}
    ;If not "SubString", this could be "String"'s end
    ${IfThen} $R4 >= $R3 ${|} ${ExitDo} ${|}
    ;If not, continue the loop
    IntOp $R4 $R4 + 1
  ${Loop}
 
/*After this point:
  ------------------------------------------
  $R0 = ResultVar (output)*/
 
  ;Remove part before "SubString" on "String" (if there has one)
  StrCpy $R0 $R1 `` $R4
 
  ;Return output to user
  Pop $R5
  Pop $R4
  Pop $R3
  Pop $R2
  Pop $R1
  Exch $R0
FunctionEnd

Function StrTok
  Exch $R1
  Exch 1
  Exch $R0
  Push $R2
  Push $R3
  Push $R4
  Push $R5
 
  ;R0 fullstring
  ;R1 tokens
  ;R2 len of fullstring
  ;R3 len of tokens
  ;R4 char from string
  ;R5 testchar
 
  StrLen $R2 $R0
  IntOp $R2 $R2 + 1
 
  loop1:
    IntOp $R2 $R2 - 1
    IntCmp $R2 0 exit
 
    StrCpy $R4 $R0 1 -$R2
 
    StrLen $R3 $R1
    IntOp $R3 $R3 + 1
 
    loop2:
      IntOp $R3 $R3 - 1
      IntCmp $R3 0 loop1
 
      StrCpy $R5 $R1 1 -$R3
 
      StrCmp $R4 $R5 Found
    Goto loop2
  Goto loop1
 
  exit:
  ;Not found!!!
  StrCpy $R1 ""
  StrCpy $R0 ""
  Goto Cleanup
 
  Found:
  StrLen $R3 $R0
  IntOp $R3 $R3 - $R2
  StrCpy $R1 $R0 $R3
 
  IntOp $R2 $R2 - 1
  IntOp $R3 $R3 + 1
  StrCpy $R0 $R0 $R2 $R3
 
  Cleanup:
  Pop $R5
  Pop $R4
  Pop $R3
  Pop $R2
  Exch $R0
  Exch 1
  Exch $R1
 
FunctionEnd

 
Function CheckForProcArgs	
	IntOp $ProcessWaitFail 0 + 0

	Call GetParameters
	pop $1
	
	${StrContains} $R0 $R1 "/waitforprocess" $1
	StrCmp $R1 "/waitforprocess" CheckForProcArgsRun CheckForProcNoParam ;GetProcArgsEXE
	
;GetProcArgsEXE:
	;push $1
	;push "/waitforprocess"							
	;Call GetProcArgs 
	;pop $0
	;StrCmp $STR_PROC "0" CheckForProcArgsEnd CheckForProcArgsRun
	

CheckForProcArgsRun:
	StrCpy $STR_PROC "SocialClubHelper.exe"

	Call WaitForProc
	goto CheckForProcArgsEnd

CheckForProcNoParam:
	
	StrCpy $STR_PROC "SocialClubHelper.exe"

	SetOutPath $TEMP
	GetTempFileName $8

	File /oname=$8 "Global\Plugins\FindProcDLL.dll"
	Push $STR_PROC
    CallInstDLL $8 FindProc
	IntCmp $R0 1 0 CheckForProcArgsEnd


	MessageBox MB_ICONSTOP|MB_OK $RunUpdateErr
	SetErrorLevel 911
	Abort

CheckForProcArgsEnd:
	
	Push 0
FunctionEnd

Function WaitForProc
	;MessageBox MB_OK "WaitForProc_$STR_PROC__"
	
	SetOutPath $TEMP
	GetTempFileName $8

WaitForProc_Loop:	
	File /oname=$8 "Global\Plugins\FindProcDLL.dll"
    Push $STR_PROC
	CallInstDLL $8 FindProc
    IntCmp $ProcessWaitFail 1 ProcessAborted
	IntCmp $R0 1 0 NotRunning
	;MessageBox MB_OK "Waiting on process"
	goto WaitForProc_Loop

ProcessAborted:
	
IfSilent +2
	MessageBox MB_ICONSTOP|MB_OK $RunUpdateErr
	
	SetErrorLevel 911
	Abort

NotRunning:

FunctionEnd

Function un.FindProc
	SetOutPath $TEMP
    GetTempFileName $8
    File /oname=$8 "Global\Plugins\FindProcDLL.dll"
    Push "SocialClubHelper.exe"
    CallInstDLL $8 FindProc
	IntCmp $R0 1 0 unNotRunning
    
	MessageBox MB_OK $RunUpdateErr
	Abort

unNotRunning:
    push $R0
FunctionEnd

Function un.IsApplicationInstalled
    ReadRegStr $0 HKEY_LOCAL_MACHINE "Software\Rockstar Games" "Installed Applications"
	strcmp $0 "" CheckSteamMP3

	${If} $0 > 0
		;MessageBox MB_OK "Check for InstallFolder Max Payne 3"
		ReadRegStr $0 HKEY_LOCAL_MACHINE "Software\Rockstar Games\Max Payne 3" "InstallFolder"
		
		strcmp $0 "" CheckGTAV
		MessageBox MB_OK|MB_ICONSTOP $AppErr 
		push "$0" 
		goto EndISAppRunning

CheckGTAV:
		;MessageBox MB_OK "Check for InstallFolder GTAV"
		ReadRegStr $0 HKEY_LOCAL_MACHINE "Software\Rockstar Games\Grand Theft Auto V" "InstallFolder"

		strcmp $0 "" Uninstallable
		MessageBox MB_OK|MB_ICONSTOP $AppErr 
		push "$0" 
		goto EndISAppRunning

Uninstallable:
	;MessageBox MB_OK "Resetting Installed Applications to 0"
	WriteRegStr HKLM "Software\Rockstar Games" "Installed Applications" "0"
	goto CheckSteamMP3
        
	${endif}

CheckSteamMP3:
	ReadRegStr $0 HKEY_LOCAL_MACHINE "Software\Rockstar Games\Max Payne 3" "InstallFolderSteam"
	
	;MessageBox MB_OK $0
	strcmp $0 "" CheckSteamLANVR
	MessageBox MB_OK|MB_ICONSTOP $AppErr 
	push "1" 
	goto EndISAppRunning

CheckSteamLANVR:
	ReadRegStr $0 HKEY_LOCAL_MACHINE "Software\Rockstar Games\LANVR" "InstallFolderSteam"
	
	strcmp $0 "" CheckSteamGTAV
	MessageBox MB_OK|MB_ICONSTOP $AppErr 
	push "1" 
	goto EndISAppRunning
	
CheckSteamGTAV:
	ReadRegStr $0 HKEY_LOCAL_MACHINE "Software\Rockstar Games\Grand Theft Auto V" "InstallFolderSteam"
	
	;MessageBox MB_OK $0
	strcmp $0 "" AllRemoved
	MessageBox MB_OK|MB_ICONSTOP $AppErr 
	push "1" 
	goto EndISAppRunning
	
AllRemoved:
    push "0"
    
EndISAppRunning:
        
FunctionEnd

Function VersionCheck
	Exch $0 ;second versionnumber
	Exch
	Exch $1 ;first versionnumber
	Push $R0 ;counter for $0
	Push $R1 ;counter for $1
	Push $3 ;temp char
	Push $4 ;temp string for $0
	Push $5 ;temp string for $1
	StrCpy $R0 "-1"
	StrCpy $R1 "-1"
Start:
	StrCpy $4 ""
DotLoop0:
	IntOp $R0 $R0 + 1
	StrCpy $3 $0 1 $R0
	StrCmp $3 "" DotFound0
	StrCmp $3 "." DotFound0
	StrCpy $4 $4$3
	Goto DotLoop0
DotFound0:
	StrCpy $5 ""
DotLoop1:
	IntOp $R1 $R1 + 1
	StrCpy $3 $1 1 $R1
	StrCmp $3 "" DotFound1
	StrCmp $3 "." DotFound1
	StrCpy $5 $5$3
	Goto DotLoop1
DotFound1:
	Strcmp $4 "" 0 Not4
	StrCmp $5 "" VersionCheckEqual
	Goto Ver2Less
Not4:
	StrCmp $5 "" Ver2More
	IntCmp $4 $5 Start Ver2Less Ver2More
VersionCheckEqual:
	StrCpy $0 "0"
	Goto VersionCheckFinish
Ver2Less:
	StrCpy $0 "1"
	Goto VersionCheckFinish
Ver2More:
	StrCpy $0 "2"
VersionCheckFinish:
	Pop $5
	Pop $4
	Pop $3
	Pop $R1
	Pop $R0
	Pop $1
	Exch $0
FunctionEnd

Function CheckRGSCVersion
    pop $1
	
    ReadRegStr $R0 HKEY_LOCAL_MACHINE "Software\Rockstar Games\RockStar Games Social Club" "InstallFolder"
    strcmp $R0 "" CheckRGSCVersionEND
    
	ReadRegStr $0 HKEY_LOCAL_MACHINE "Software\Rockstar Games\RockStar Games Social Club" "Version"
	strcmp $0 "" CheckRGSCVersionEND
	
	strCmp $1 $0 CheckRGSCVersionEND 	

	push $0
	push $1
	Call VersionCheck
	pop  $R3 
	
	${if} $R3 == 2
		goto CheckRGSCVersionEND	
	${endif}
	
	; If its install is less make sure its not a bad delete.
	IfFileExists $R0\socialclub.dll CheckRGSCVersionMSG
	;MessageBox MB_OK "File Doesnt exists and its less"
	goto CheckRGSCVersionEND

CheckRGSCVersionMSG:
	
	push $0

	${SWITCH} $LANGUAGE
        ${Case} 1033
            Call AddEnglishVerError        
            ${Break}
        ${Case} 1036
            Call AddFrenchVerError        
            ${Break}
        ${Case} 1040
            Call AddItalianVerError        
            ${Break}
        ${Case} 3082
           Call AddSpanishVerError        
            ${Break}
        ${Case} 1031
            Call AddGermanVerError        
            ${Break}
		${Case} 1041
            Call AddJapaneseVerError       
            ${Break}
		${Case} 1045            ;Polish
            Call AddPolishVerError   
            ${Break}
		${Case} 1046            ;PortugueseBR
           Call AddPortugueseVerError
		${Break}	
        ${Case} 1049
            Call AddRussianVerError      
            ${Break}
		${Case} 1042
            Call AddKoreanVerError      
            ${Break}
		${Case} 2058
            Call AddMexicanVerError      
            ${Break}
		${Case} 0804
            Call TraditionalChineseVerError      
            ${Break}
        ${Default}
            Call AddEnglishVerError        
            ${Break}
    ${EndSwitch}
	
	IfSilent +2
		MessageBox MB_OK|MB_ICONEXCLAMATION $NewerVerErr
    
	SetErrorLevel 0
	abort

CheckRGSCVersionEND:
   
FunctionEnd

 ; StrContains
; This function does a case sensitive searches for an occurrence of a substring in a string. 
; It returns the substring if it is found. 
; Otherwise it returns null(""). 
; Written by kenglish_hi
; Adapted from StrReplace written by dandaman32

; StrContains
; This function does a case sensitive searches for an occurrence of a substring in a string. 
; It returns the substring if it is found. 
; Otherwise it returns null(""). 
Function cStrContains
  Exch $STR_NEEDLE
  Exch 1
  Exch $STR_HAYSTACK
  ; Uncomment to debug
  ;MessageBox MB_OK 'STR_NEEDLE = $STR_NEEDLE STR_HAYSTACK = $STR_HAYSTACK '
	
	StrCpy $STR_RETURN_VAR ""
    StrCpy $CHAR_COUNTER 0	
	IntOp  $CHAR_COUNTER $CHAR_COUNTER - 1
    StrLen $ARG_LENGTH $STR_NEEDLE
    StrLen $ARGS_LIST_LENGTH $STR_HAYSTACK
    
	;MessageBox MB_ICONSTOP|MB_OK "CHAR_COUNTER = $CHAR_COUNTER, ARGS_LIST_LENGTH = $ARGS_LIST_LENGTH"

	loop:
      IntOp $CHAR_COUNTER $CHAR_COUNTER + 1
      StrCpy $STR_PROC $STR_HAYSTACK $ARG_LENGTH $CHAR_COUNTER
	  StrCmp $STR_PROC $STR_NEEDLE found
      StrCmp $CHAR_COUNTER $ARGS_LIST_LENGTH done
		
     ; MessageBox MB_ICONSTOP|MB_OK "CHAR_COUNTER = $CHAR_COUNTER, ARGS_LIST_LENGTH = $ARGS_LIST_LENGTH"

      Goto loop
    found:
      StrCpy $STR_RETURN_VAR $STR_NEEDLE
      Goto done
    done:
  
   Pop $STR_NEEDLE ;Prevent "invalid opcode" errors and keep the
   push $STR_RETURN_VAR  
   push $CHAR_COUNTER

FunctionEnd
 
Function GetProcArgs
	Exch $STR_NEEDLE
	Exch 1
	Exch $STR_HAYSTACK
	;Uncomment to debug
	
	StrCpy $STR_RETURN_VAR "0"
	StrLen $ARG_LENGTH $STR_NEEDLE
	StrLen $ARGS_LIST_LENGTH $STR_HAYSTACK

	;MessageBox MB_OK "STR_NEEDLE = $STR_NEEDLE"
	;MessageBox MB_ICONSTOP|MB_OK "STR_HAYSTACK = $STR_HAYSTACK"
	;MessageBox MB_ICONSTOP|MB_OK "ARG_LENGTH = $ARG_LENGTH"
	;MessageBox MB_ICONSTOP|MB_OK "ARGS_LIST_LENGTH = $ARGS_LIST_LENGTH"

	IntOp  $CHAR_COUNTER $ARG_LENGTH + $CHAR_COUNTER
	IntOp  $CHAR_COUNTER $CHAR_COUNTER + 1 
	
	;MessageBox MB_ICONSTOP|MB_OK "START INDEX = $CHAR_COUNTER"

	StrCpy $STR_PROC $STR_HAYSTACK $ARGS_LIST_LENGTH $CHAR_COUNTER

	;MessageBox MB_ICONSTOP|MB_OK "STR_PROC_$STR_PROC"

	push $STR_PROC
FunctionEnd 


