!include LogicLib.nsh                       ;Used for Syntax

!define sysOSVERSIONINFO '(i, i, i, i, i, &t128) i'
!define sysGetVersionEx 'kernel32::GetVersionExA(i) i'
!define sysVER_PLATFORM_WIN32_NT 2

;--------------------------------/-
; *** NOTES $(^NAME)   -- This is how you read text in .nlf files. ***

!macro MUI_LANGUAGE_MOD LANGUAGE LANG_NLF

  ;Include a language

  !verbose push
  !verbose ${MUI_VERBOSE}

  !insertmacro MUI_INSERT

  LoadLanguageFile "${LANG_NLF}"				;"${NSISDIR}\Contrib\Language files\${LANGUAGE}.nlf"

  ;Include language file
  ;!insertmacro LANGFILE_INCLUDE_WITHDEFAULT "${NSISDIR}\Contrib\Language files\${LANGUAGE}.nsh" "${NSISDIR}\Contrib\Language files\English.nsh"

  ;Add language to list of languages for selection dialog
  !ifndef MUI_LANGDLL_LANGUAGES
    !define MUI_LANGDLL_LANGUAGES "'${LANGFILE_${LANGUAGE}_NAME}' '${LANG_${LANGUAGE}}' "
    !define MUI_LANGDLL_LANGUAGES_CP "'${LANGFILE_${LANGUAGE}_NAME}' '${LANG_${LANGUAGE}}' '${LANG_${LANGUAGE}_CP}' "
  !else
    !ifdef MUI_LANGDLL_LANGUAGES_TEMP
      !undef MUI_LANGDLL_LANGUAGES_TEMP
    !endif
    !define MUI_LANGDLL_LANGUAGES_TEMP "${MUI_LANGDLL_LANGUAGES}"
    !undef MUI_LANGDLL_LANGUAGES

    !ifdef MUI_LANGDLL_LANGUAGES_CP_TEMP
      !undef MUI_LANGDLL_LANGUAGES_CP_TEMP
    !endif
    !define MUI_LANGDLL_LANGUAGES_CP_TEMP "${MUI_LANGDLL_LANGUAGES_CP}"
    !undef MUI_LANGDLL_LANGUAGES_CP

    !define MUI_LANGDLL_LANGUAGES "'${LANGFILE_${LANGUAGE}_NAME}' '${LANG_${LANGUAGE}}' ${MUI_LANGDLL_LANGUAGES_TEMP}"
    !define MUI_LANGDLL_LANGUAGES_CP "'${LANGFILE_${LANGUAGE}_NAME}' '${LANG_${LANGUAGE}}' '${LANG_${LANGUAGE}_CP}' ${MUI_LANGDLL_LANGUAGES_CP_TEMP}"
  !endif

  !verbose pop

!macroend

;Languages
!insertmacro MUI_LANGUAGE_MOD "English" "Global\LangFiles\English.nlf"         ;first language is the default language
!insertmacro MUI_LANGUAGE_MOD "French" "Global\LangFiles\French.nlf"
!insertmacro MUI_LANGUAGE_MOD "Italian" "Global\LangFiles\Italian.nlf"
!insertmacro MUI_LANGUAGE_MOD "German" "Global\LangFiles\German.nlf"
!insertmacro MUI_LANGUAGE_MOD "Spanish" "Global\LangFiles\Spanish.nlf"
!insertmacro MUI_LANGUAGE_MOD "Russian" "Global\LangFiles\Russian.nlf"
!insertmacro MUI_LANGUAGE_MOD "Japanese" "Global\LangFiles\Japanese.nlf"
!insertmacro MUI_LANGUAGE_MOD "Polish" "Global\LangFiles\Polish.nlf" 
!insertmacro MUI_LANGUAGE_MOD "PortugueseBR" "Global\LangFiles\PortugueseBR.nlf" 
!insertmacro MUI_LANGUAGE_MOD "Korean" "Global\LangFiles\Korean.nlf"
!insertmacro MUI_LANGUAGE_MOD "SpanishMX" "Global\LangFiles\SpanishMX.nlf"
!insertmacro MUI_LANGUAGE_MOD "TradChinese" "Global\LangFiles\TradChinese.nlf"
!insertmacro MUI_LANGUAGE_MOD "SimpChinese" "Global\LangFiles\SimpChinese.nlf"

LicenseLangString MUILicense  ${LANG_ENGLISH} "Global\EULA\English\License.rtf" 
LicenseLangString MUILicense  ${LANG_ITALIAN} "Global\EULA\Italian\License.rtf" 
LicenseLangString MUILicense  ${LANG_FRENCH} "Global\EULA\French\License.rtf" 
LicenseLangString MUILicense  ${LANG_GERMAN} "Global\EULA\German\License.rtf" 
LicenseLangString MUILicense  ${LANG_SPANISH} "Global\EULA\Spanish\License.rtf"
LicenseLangString MUILicense  ${LANG_JAPANESE} "Global\EULA\Japanese\License.rtf"
LicenseLangString MUILicense  ${LANG_RUSSIAN} "Global\EULA\Russian\License.rtf"
LicenseLangString MUILicense  ${LANG_POLISH} "Global\EULA\Polish\License.rtf"
LicenseLangString MUILicense  ${LANG_PORTUGUESEBR} "Global\EULA\PortugueseBR\License.rtf"
LicenseLangString MUILicense  ${LANG_KOREAN} "Global\EULA\Korean\License.rtf"
LicenseLangString MUILicense  ${LANG_SPANISHMX} "Global\EULA\SpanishMX\License.rtf"
LicenseLangString MUILicense  ${LANG_TRADCHINESE} "Global\EULA\ChineseTraditional\License.rtf"
LicenseLangString MUILicense  ${LANG_SIMPCHINESE} "Global\EULA\ChineseSimplified\License.rtf"

; Set name using the normal interface (Name command)
;LangString Name ${LANG_ENGLISH} "English"
;LangString Name ${LANG_ITALIAN} "Italiano"
;LangString Name ${LANG_FRENCH} "Français"
;LangString Name ${LANG_GERMAN} "Deutsch"
;LangString Name ${LANG_SPANISH} "Español"
;LangString Name ${LANG_JAPANESE} "日本語"
;LangString Name ${LANG_RUSSIAN} "Русско"
;LangString Name ${SUBLANG_SPANISH_MEXICAN} "Español mexicano"
;LangString Name ${LANG_TRADCHINESE} "繁體中文"

var AdminErrorRGSC		
var AdminErrorPatcher  
var AdminErrorDLC		
var OSError
var RGSCError
var GTAIVError
;var WelcomeHead
var WelcomeHead1
var WelcomeHead2
var WelcomeMsg1
Var WelcomeError
var WelcomeMsg2
var InstallHead
var InstallMsg
var RunError
var InitINSTALL
var RunUpdateErr
var AppErr
var NewerVerErr
var NeedReboot

var LangTitle 
var LangSelect
var AbortWarning
var TransferError

var OptHeader1
var OptHeader2
var ModifyHead					
var ModifyText	
var RemoveHead  
var RemoveText  
var ModHeader1
var ModHeader2
var CurrentLang
var ConfirmTitle
var	ConfirmMsg  
var MTLRunning
var RemoveFile
var sDeletefiles
var sDeleteReg
var sRemoveDir

Function SetUILanguage
    Push $R0
    Push $R1
    Push $R2
    ; Call GetUserDefaultUILanguage (available on Windows Me, 2000 and later)
    ; $R0 = GetUserDefaultUILanguage()
    System::Call 'kernel32::GetUserDefaultUILanguage() i.r10'
	
	;MessageBox MB_ICONSTOP|MB_OK "SetUILanguage $R0"
	
	;If The language is spanish check for Mexican.
	strCmp $R0 3082 OldFashionedWay CheckForError
	
CheckForError:
	StrCmp $R0 "error" OldFashionedWay Finish

OldFashionedWay:

	StrCpy $CurrentLang $R0

    ; GetUserDefaultUILanguage() doesn't exist, so root in the Registry
    ; First, check the Windows version
    ; $R0 = new OSVERSIONINFO(148, 0, 0, 0, 0, "")
    System::Call '*${sysOSVERSIONINFO}(148,0,0,0,0,"") .r10'
    ; GetVersionEx($R0)
    System::Call '${sysGetVersionEx}(r10)'
    ; $R1 = $R0->dwPlatformId
    System::Call "*$R0(i ., i ., i ., i ., i .r11)"
    ; delete $R0
    System::Free $R0
    IntCmp $R1 ${sysVER_PLATFORM_WIN32_NT} PlatformIsNT
    ; We're running on DOS-based Windows
    StrCpy $R0 "Control Panel\desktop\ResourceLocale"
    StrCpy $R1 ""
    GoTo GetLCIDFromHKCU
PlatformIsNT:
    ; We're running on Windows NT
    StrCpy $R0 "Control Panel\International"
    StrCpy $R1 "Locale"
        GoTo GetLCIDFromHKU
GetLCIDFromHKU:
    ReadRegStr $R2 HKCU $R0 $R1
    GoTo GetLangIDFromLCID
GetLCIDFromHKCU:
    ReadRegStr $R2 HKCU $R0 $R1
GetLangIDFromLCID:
	StrCpy $R2 "0x$R2"
	IntOp $R0 $R2 & 0xffff
	;MessageBox MB_ICONSTOP|MB_OK "OldFashionedWay This one $R0"
	
	; Used to see if the sub language is Mexican
	StrCmp $R0 2058 Finish SetForSubLang 
SetForSubLang:
	StrCpy $LANGUAGE $CurrentLang
	goto FinishSub

    ; Get just the primary LANGID (NSIS messes it up otherwise)
	;IntOp $R0 $R0 & 0x3ff ; remove SUBLANGID
    ;IntOp $R0 $R0 | 0x400 ; add SUBLANG_DEFAULT
	
Finish:
    StrCpy $LANGUAGE $R0

FinishSub:
	Pop $R2
    Pop $R1
    Pop $R0

	;MessageBox MB_ICONSTOP|MB_OK "So the lang is $LANGUAGE"
FunctionEnd

Function CheckSupportedLang()
    ${SWITCH} $LANGUAGE
        ${Case} 1033            ;English
            Push 1033
            ${Break}
        ${Case} 1036            ;French
            Push 1036
            ${Break}
        ${Case} 1040            ;Italian
            Push 1040
            ${Break}
        ${Case} 3082            ;Spanish
            Push 3082
            ${Break}
        ${Case} 1031            ;German
            Push 1031
            ${Break}
        ${Case} 1049            ;Russian
            Push 1049
            ${Break}
        ${Case} 1041            ;Japan
            Push 1041   
            ${Break}
	${Case} 1045				;Polish
            Push 1045   
            ${Break}
	${Case} 1046				;PortugueseBR
            Push 1046   
            ${Break}
	${Case} 1042				;Korean
            Push 1042   
            ${Break}
	${Case} 2058				;Mexican
            Push 2058   
            ${Break}
	${Case} 804					;Traditional Chinese
            Push 0804   
            ${Break}
	${Case} 2052				; Simplified Chinese
			Push 2052
			${Break}
        ${Default}
            Push "error"
            ${Break}
    ${EndSwitch} 
    
FunctionEnd


Function SelectLanguage                                 ;Language selection dialog
	ReadRegStr $0 HKLM "SOFTWARE\Rockstar Games\Rockstar Games Social Club" "InstallLang"
	
	;Comment out everything to SelectALang to test language drop down.
	;MessageBox MB_ICONSTOP|MB_OK "ReadRegStr $0"

	StrCmp $0 "" CheckForUILang SetLangFromReg

CheckForUILang:
	Call SetUILanguage
	;MessageBox MB_ICONSTOP|MB_OK $AdminErrorRGSC
	goto CheckIsSupported

SetLangFromReg:
	StrCpy $LANGUAGE $0 ; -10 15
	goto SelectLanguageFinished

CheckIsSupported:	
	Call CheckSupportedLang()
	Pop $R0

	StrCmp $R0 "error" SelectALang SelectLanguageFinished 

SelectALang:
	;!insertmacro MUI_LANGDLL_DISPLAY
	
	Push $LANGUAGE
	Call SetLangSelectText

	;MessageBox MB_ICONSTOP|MB_OK "Language $LANGUAGE ${LANG_SPANISHMX}"
	
	Push ""
	Push ${LANG_ENGLISH}
	Push "English"
	Push ${LANG_ITALIAN}
	Push "Italiano"
	Push ${LANG_FRENCH}
	Push "Français"
	Push ${LANG_GERMAN}
	Push "Deutsch"
	Push ${LANG_SPANISH}
	Push "Español"
	Push ${LANG_JAPANESE}
	Push "日本語"
	Push ${LANG_RUSSIAN}
	Push "Русский"
	Push ${LANG_POLISH}
	Push "Polski"
	Push ${LANG_PORTUGUESEBR}
	Push "Português Brasileiro"
	Push ${LANG_KOREAN}
	Push "한국말"
	Push ${LANG_SPANISHMX}
	Push "Español mexicano"
	Push ${LANG_TRADCHINESE}
	Push "繁體中文"
	Push ${LANG_SIMPCHINESE}
	Push "简体中文"

	Push A ; A means auto count languages
		   ; for the auto count to work the first empty push (Push "") must remain
	
	LangDLL::LangDialog $LangTitle $LangSelect
    Pop $LANGUAGE
	
	;MessageBox MB_ICONSTOP|MB_OK "Language $LANGUAGE"

    StrCmp $LANGUAGE "cancel" 0 +2
        Abort

SelectLanguageFinished:
FunctionEnd

Function SetLangSelectText
    pop $0
    ${SWITCH} $0
        ${Case} 1033
            Call AddLangSelectEng        
            ${Break}
        ${Case} 1036
            Call AddLangSelectFre        
            ${Break}
        ${Case} 1040
            Call AddLangSelectIta        
            ${Break}
        ${Case} 3082
            Call AddLangSelectSpa       
            ${Break}
        ${Case} 1031
            Call AddLangSelectGer        
            ${Break}
		${Case} 1041
            Call AddLangSelectJap        
            ${Break}
        ${Case} 1049
            Call AddLangSelectRus        
            ${Break}
		${Case} 1045
            Call AddLangSelectPol        
            ${Break}
		${Case} 1046
            Call AddLangSelectBrz        
            ${Break}
		${Case} 1042
            Call AddLangSelectKor        
            ${Break}
		${Case} 2058
            Call AddLangSelectMX        
            ${Break}
		${Case} 804
            Call AddLangSelectChina        
            ${Break}
		${Case} 2052
			Call AddLangSelectSimpChina
			${Break}
        ${Default}
            Call AddLangSelectEng        
        ${Break}
    ${EndSwitch}
FunctionEnd

Function SetLangText
    pop $0

    ${SWITCH} $0
        ${Case} 1033
            Call AddEnglish        
            ${Break}
        ${Case} 1036
            Call AddFrench        
            ${Break}
        ${Case} 1040
            Call AddItalian        
            ${Break}
        ${Case} 3082
           Call AddSpanish        
            ${Break}
        ${Case} 1031
            Call AddGerman        
            ${Break}
		${Case} 1041
            Call AddJapanese        
            ${Break}
        ${Case} 1049
            Call AddRussian        
            ${Break}
		${Case} 1045
            Call AddPolish        
            ${Break}
		${Case} 1046
            Call AddPortuguese        
            ${Break}
		${Case} 1042
	        Call AddKorean        
            ${Break}
		${Case} 2058
	        Call AddMexican        
            ${Break}
		${Case} 804
	        Call AddTraditionalChinese        
            ${Break}
		${Case} 2052
			Call AddSimpChinese
			${Break}
        ${Default}
            Call AddEnglish        
            ${Break}
    ${EndSwitch}
FunctionEnd

Function un.SetLangText
    pop $0
    
   ;MessageBox MB_OK "Language selected:: $0"
     
    ${SWITCH} $0
        ${Case} 1033
            Call un.AddEnglish        
            ${Break}
        ${Case} 1036
            Call un.AddFrench        
            ${Break}
        ${Case} 1040
            Call un.AddItalian        
            ${Break}
        ${Case} 3082
           Call un.AddSpanish        
            ${Break}
        ${Case} 1031
            Call un.AddGerman        
            ${Break}
		${Case} 1041
            Call un.AddJapanese        
            ${Break}
        ${Case} 1049
            Call un.AddRussian        
            ${Break}
		${Case} 1045
            Call un.AddPolish        
            ${Break}
		${Case} 1046
            Call un.AddPortuguese        
            ${Break}
		${Case} 1042
            Call un.AddKorean        
            ${Break}
		${Case} 2058
            Call un.AddMexican        
            ${Break}
		${Case} 804
            Call un.AddTraditionalChinese        
            ${Break}
		${Case} 2052
			Call un.AddSimpChinese
			${Break}
        ${Default}
            Call un.AddEnglish        
            ${Break}
    ${EndSwitch}
FunctionEnd

/*
Function SetLangSpaceMsg
    pop $0

    ${SWITCH} $0
        ${Case} 1033
            Call AddENGSpaceMsg        
            ${Break}
        ${Case} 1036
            Call AddFRESpaceMsg        
            ${Break}
        ${Case} 1040
            Call AddITASpaceMsg        
            ${Break}
        ${Case} 3082
           Call AddSPASpaceMsg        
            ${Break}
        ${Case} 1031
            Call AddGERSpaceMsg        
            ${Break}
	${Case} 1041
           Call AddJAPSpaceMsg        
            ${Break}    
        ${Case} 1049
            Call AddRUSSpaceMsg        
            ${Break}
	${Case} 1045
            Call AddPOLSpaceMsg        
            ${Break}
	${Case} 1046
            Call AddBRZSpaceMsg        
            ${Break}
	${Case} 1042
            Call AddKORSpaceMsg        
            ${Break}
	${Case} 2058
            Call AddMXSpaceMsg        
            ${Break}
	${Case} 804
            Call AddCHINASpaceMsg        
            ${Break}
        ${Default}
            Call AddENGSpaceMsg        
            ${Break}
    ${EndSwitch}
FunctionEnd   
*/

Function AddLangSelectEng
    StrCpy $LangTitle  "Installer Language" 
    StrCpy $LangSelect "Please select the language of the installer."
FunctionEnd

Function AddEnglish
    
	StrCpy $AdminErrorRGSC		"Rockstar Games Social Club requires administrator privileges to install.$\nPlease log in as Administrator and restart this application."
	StrCpy $AdminErrorPatcher  "$(^Name) updater requires administrator privileges to install.$\nPlease log in as Administrator and restart this application."
	StrCpy $AdminErrorDLC		"$(^Name) download installer requires administrator privileges.$\nPlease log in as Administrator and restart this application."
	
	StrCpy $OSError     "Your system is incompatible with $(^Name)."
    StrCpy $WelcomeHead1 "Welcome."
    StrCpy $WelcomeHead2 "This wizard will guide you through the installation of $(^Name). "
	StrCpy $WelcomeMsg1 "Welcome to the $(^Name) update installer."
    StrCpy $WelcomeMsg2 "This will install the latest features for $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Installing Files."
    StrCpy $InstallMsg  "Installing $(^Name) Files."
    StrCpy $RunError    "$(^Name) is currently running.$\n You Must close the application in order to complete this process."
    StrCpy $InitINSTALL  "Initializing $(^Name) Installer."
    StrCpy $WelcomeError "Error: Tying to run $(^Name) Installer."
    StrCpy $AbortWarning "Are you sure you want to quit $(^Name) Setup?"
    StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
    
    StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
    StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
	StrCpy $MTLRunning "Please close the Rockstar Games Launcher Application and try again"
	StrCpy $RemoveFile "Remove Files..."
FunctionEnd

Function AddEnglishVerError
	StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
FunctionEnd

Function un.AddEnglish
	StrCpy $WelcomeHead1 "Welcome."
    StrCpy $WelcomeHead2 "This wizard will guide you through the installation of $(^Name)."
	StrCpy $RunError     "$(^Name) is currently running.$\n You Must close the application in order to complete this process."   
    StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
    StrCpy $AppErr       "All Rockstar Applications must be removed before removing $(^Name)"
	
	StrCpy $OptHeader1	 "Welcome to $(^Name) setup maintenance program."
	StrCpy $OptHeader2	 "This program lets you modify the current installation. Click one of the options below, and the click Next to proceed."
	StrCpy $ModifyHead	 "Modify."					
	StrCpy $ModifyText	 "Select new program features to add or select currently installed features to remove."
	StrCpy $RemoveHead   "Remove."
	StrCpy $RemoveText   "Remove all installed features."
	StrCpy $ModHeader1	 "Select Installation Options for $(^Name)." 
	StrCpy $ModHeader2   "Select the features you want to retain, and deselect the features you want to remove."
	StrCpy $NeedReboot   "Your computer must be restarted in order to complete the uninstallation of $(^Name). Do you want to reboot now?"
	
	StrCpy $ConfirmTitle "Uninstall $(^Name)"
	StrCpy $ConfirmMsg   "Remove $(^Name) from your computer."
	StrCpy $sDeleteReg   "Delete Reg..."
	StrCpy $sDeletefiles "Delete Files..."
	StrCpy $sRemoveDir   "Remove Dir..."
FunctionEnd

Function AddLangSelectFre
    StrCpy $LangTitle   "Langue du programme d'installation" 
    StrCpy $LangSelect  "Veuillez sélectionner la langue du programme d'installation."
FunctionEnd

Function AddFrench
	StrCpy $AdminErrorRGSC "Pour installer le Rockstar Games Social Club, vous devez disposer de privilèges d'administrateur.$\nVeuillez vous connecter en tant qu'administrateur et redémarrer cette application."
	StrCpy $AdminErrorPatcher "Pour installer le logiciel de mise à jour de $(^Name), vous devez disposer de privilèges d'administrateur.$\nVeuillez vous connecter en tant qu'administrateur et redémarrer cette application."
	StrCpy $AdminErrorDLC "Pour utiliser le logiciel d'installation de contenus téléchargeables de $(^Name), vous devez disposer de privilèges d'administrateur.$\nVeuillez vous connecter en tant qu'administrateur et redémarrer cette application."

    StrCpy $OSError     "Votre système est incompatible avec $(^Name)."
    StrCpy $RGSCError   "Erreur : impossible de trouver le Rockstar Games Social Club.$\nAssurez-vous d'avoir bien installé le Rockstar Games Social Club et qu'il fonctionne correctement."
    StrCpy $GTAIVError  "Erreur : impossible de trouver $(^Name). Assurez-vous d'avoir bien installé $(^Name) et qu'il fonctionne correctement."
    StrCpy $WelcomeHead1 "Bienvenue."
    StrCpy $WelcomeHead2 "Vous êtes sur le point d’installer le Rockstar Games Social Club sur votre ordinateur."
    StrCpy $WelcomeMsg1 "Bienvenue dans le programme d'installation de mises à jour de $(^Name)."
    StrCpy $WelcomeMsg2 "Ce programme va installer les derniers composants de $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Installation des fichiers."
    StrCpy $InstallMsg  "Installation des fichiers de $(^Name)."
    StrCpy $RunError    "$(^Name) est déjà en cours d'utilisation.$\nVous devez fermer l'application pour que le programme de mise à jour soit pris en compte."
    StrCpy $InitINSTALL   "Initialisation du programme d'installation de $(^Name)."
    StrCpy $WelcomeError "Erreur : tentative d'accès au programme d'installation de $(^Name)."
    StrCpy $AbortWarning "Êtes-vous sûr de vouloir quitter l'installation de $(^Name) ?"
    StrCpy $TransferError "Echec de la copie de l'un des fichiers sur votre ordinateur.$\nCe fichier est nécessaire pour le bon fonctionnement de $(^Name).$\nPour afficher l'erreur, veuillez cliquer sur 'Plus d'infos' ou visitez www.rockstargames.com/support pour de plus amples informations."
    
    StrCpy $RunUpdateErr "$(^Name) est en cours d'exécution. Mise à jour impossible. Veuillez fermer toutes les applications Rockstar Games."
    StrCpy $NewerVerErr  "Une nouvelle version ($0) a été installée pour $(^Name). L’installation va se terminer."
	StrCpy $MTLRunning "Veuillez fermer le Rockstar Games Launcher et réessayer."
	StrCpy $RemoveFile "Suppression des fichiers…"
FunctionEnd

Function AddFrenchVerError
	StrCpy $NewerVerErr  "Une nouvelle version ($0) a été installée pour $(^Name). L’installation va se terminer."
FunctionEnd

Function un.AddFrench
    StrCpy $WelcomeHead1 	"Bienvenue."
	StrCpy $WelcomeHead2 	"Cet assistant d'installation va vous guider pendant l'installation du produit $(^Name)."
	StrCpy $RunError     	"$(^Name) est actuellement lancé.$\n Vous devez quitter l'application pour terminer ce processus."   
	StrCpy $RunUpdateErr 	"$(^Name) est lancé et ne peut pas être mis à jour. Veuillez quitter toutes les applications Rockstar Games."
	StrCpy $AppErr			"Toutes les applications Rockstar doivent être supprimées avant de pouvoir supprimer $(^Name)."
		
	StrCpy $OptHeader1		"Bienvenue dans le programme de maintenance pour $(^Name)."
	StrCpy $OptHeader2		"Ce programme vous permet de modifier l'installation actuelle. Cliquez sur l'une des options ci-après, puis sur Suivant pour continuer."
	StrCpy $ModifyHead		"Modifier."					
	StrCpy $ModifyText		"Sélectionnez de nouvelles fonctionnalités à ajouter ou des fonctionnalités déjà installées à supprimer."
	StrCpy $RemoveHead		"Supprimer."
	StrCpy $RemoveText		"Supprimez toutes les fonctionnalités installées."
	StrCpy $ModHeader1		"Sélectionnez vos options d'installation pour $(^Name)." 
	StrCpy $ModHeader2		"Sélectionnez les fonctionnalités à conserver et désélectionnez les fonctionnalités à supprimer."
	StrCpy $NeedReboot		"Votre ordinateur doit être redémarré pour terminer la désinstallation de $(^Name). Souhaitez-vous redémarrer maintenant ?"
	
	StrCpy $ConfirmTitle	"Désinstaller $(^Name)"
	StrCpy $ConfirmMsg		"Supprimer $(^Name) de votre ordinateur."
	StrCpy $sDeleteReg   	"Suppression des entrées du registre…"
	StrCpy $sDeletefiles 	"Suppression des fichiers…"
	StrCpy $sRemoveDir   	"Suppression du dossier…"
FunctionEnd

Function AddLangSelectIta
    StrCpy $LangTitle   "Lingua installer" 
    StrCpy $LangSelect  "Seleziona la lingua dell'installer."
FunctionEnd

Function AddItalian
	StrCpy $AdminErrorRGSC	 "Il Social Club di Rockstar Games necessita dei privilegi di amministratore per essere installato.$\nAccedi come Amministratore e riavvia l'applicazione."
	StrCpy $AdminErrorPatcher "L'updater di $(^Name) necessita dei privilegi di amministratore per essere installato.$\nAccedi come Amministratore e riavvia l'applicazione."
	StrCpy $AdminErrorDLC "Il programma d'installazione dei download di $(^Name) necessita dei privilegi di amministratore per essere installato.$\nAccedi come Amministratore e riavvia l'applicazione."

    StrCpy $OSError     "Il sistema è incompatibile con Social Club di Rockstar Games."
    StrCpy $RGSCError   "Errore: impossibile rilevare l'applicazione del Social Club di Rockstar Games.$\nAssicurati che l'applicazione del Social Club di Rockstar Games sia installata e funzioni correttamente."
    StrCpy $GTAIVError  "Errore: impossibile rilevare Social Club di Rockstar Games. Assicurati che $(^Name) sia installato e funzioni correttamente."
    StrCpy $WelcomeHead1 "Benvenuto."
    StrCpy $WelcomeHead2 "Questo programma installerà Social Club di Rockstar Games nel vostro computer."
    StrCpy $WelcomeMsg1 "Benvenuto all'installazione dell'aggiornamento di Social Club di Rockstar Games."
    StrCpy $WelcomeMsg2 "Questo programma installerà le funzionalità più recenti del Social Club di Rockstar Games.$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Installazione file in corso..."
    StrCpy $InstallMsg  "Installazione dei file di Social Club di Rockstar Games."
    StrCpy $RunError    "$(^Name) è in esecuzione.$\n È necessario chiudere l'applicazione per poter completare il processo."
    StrCpy $InitINSTALL   "Inizializzazione del programma di installazione del Social Club di Rockstar Games."
    StrCpy $WelcomeError "Errore nel tentativo di esecuzione dell'installer del Social Club di Rockstar Games."
    StrCpy $AbortWarning "Sei sicuro di voler interrompere l'installazione del Social Club di Rockstar Games?"
    StrCpy $TransferError "Uno dei file estratti non è stato copiato correttamente sul tuo sistema.$\nQuesto file è necessario per far funzionare Social Club di Rockstar Games correttamente.$\nPer mostrare i dettagli dell'errore, clicca su 'Mostra Dettagli' o visita www.rockstargames.com/support per ulteriori informazioni."
    
    StrCpy $RunUpdateErr "Social Club di Rockstar Games è aperto e non può essere aggiornato. Chiudi tutte le applicazioni Rockstar Games."
    StrCpy $NewerVerErr  "Una nuova versione ($0) del Social Club di Rockstar Games è stata installata. Installazione in chiusura."
	StrCpy $MTLRunning "Chiudi il Programma di avvio di Rockstar Games e riprova"
	StrCpy $RemoveFile "Rimozione file…"
FunctionEnd

Function AddItalianVerError
	StrCpy $NewerVerErr  "Una nuova versione ($0) del Social Club di Rockstar Games è stata installata. Installazione in chiusura."
FunctionEnd

Function un.AddItalian
	StrCpy $WelcomeHead1 	"Benvenuto."
	StrCpy $WelcomeHead2 	"Questo programma ti guiderà nell'installazione del Social Club di Rockstar Games."
	StrCpy $RunError     	"Social Club di Rockstar Games è attualmente in esecuzione.$\n Per concludere il processo, devi chiudere l'applicazione."   
	StrCpy $RunUpdateErr 	"Social Club di Rockstar Games è in esecuzione e non può essere aggiornato. Chiudi tutte le applicazioni di Rockstar."
	StrCpy $AppErr       	"Per rimuovere Social Club di Rockstar Games, occorre rimuovere tutte le applicazioni di Rockstar."
		
	StrCpy $OptHeader1	 	"Benvenuto al programma di manutenzione del Social Club di Rockstar Games."
	StrCpy $OptHeader2	 	"Questo programma ti permette di modificare lo stato dell'installazione attuale. Per continuare, clicca su una delle opzioni sotto e poi su Avanti."
	StrCpy $ModifyHead	 	"Modifica."					
	StrCpy $ModifyText	 	"Seleziona le nuove funzioni da aggiungere o seleziona quelle già installate da rimuovere."
	StrCpy $RemoveHead   	"Rimuovi."
	StrCpy $RemoveText   	"Rimuovi tutte le funzioni installate."
	StrCpy $ModHeader1	 	"Seleziona le opzioni di installazione per $(^Name)." 
	StrCpy $ModHeader2   	"Seleziona le funzioni che vuoi mantenere e deseleziona quelle che vuoi rimuovere."
	StrCpy $NeedReboot		"Il computer deve essere riavviato per completare l'installazione di $(^Name). Riavviarlo ora?"
	StrCpy $ConfirmTitle	"Disinstallazione di $(^Name)"
	StrCpy $ConfirmMsg		"$(^Name) verrà rimosso dal computer."
	StrCpy $sDeleteReg   	"Eliminazione file di registro…"
	StrCpy $sDeletefiles 	"Eliminazione file…"
	StrCpy $sRemoveDir   	"Rimozione cartelle…"
FunctionEnd

Function AddLangSelectSpa
    StrCpy $LangTitle   "Idioma del instalador" 
    StrCpy $LangSelect  "Selecciona el idioma del instalador."
FunctionEnd

Function AddSpanish
	StrCpy $AdminErrorRGSC "Para instalar el Social Club de Rockstar Games se requieren privilegios de administrador.$\nInicia sesión como administrador y reinicia esta aplicación."
	StrCpy $AdminErrorPatcher "Para instalar el programa de actualización de $(^Name) se requieren privilegios de administrador.$\nInicia sesión como administrador y reinicia esta aplicación."
	StrCpy $AdminErrorDLC "El instalador de descargas de $(^Name) requiere privilegios de administrador.$\nInicia sesión como administrador y reinicia esta aplicación."

    StrCpy $OSError     "Tu sistema no es compatible con $(^Name)."
    StrCpy $RGSCError   "Error: no se encuentra $(^Name).$\nAsegúrate de que $(^Name) esté instalado y en funcionamiento."
    StrCpy $GTAIVError  "Error: no se encuentra $(^Name). Asegúrate de que $(^Name) esté instalado y en funcionamiento."
    StrCpy $WelcomeHead1 "Bienvenido."
    StrCpy $WelcomeHead2 "Este programa instalará $(^Name) en su ordenador."
    StrCpy $WelcomeMsg1 "Bienvenido al instalador de la actualización de $(^Name)."
    StrCpy $WelcomeMsg2 "Este programa instalará las funciones más recientes de $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Instalando archivos."
    StrCpy $InstallMsg  "Instalando los archivos de $(^Name)."
    StrCpy $RunError    "$(^Name) está ejecutándose.$\nPara poder completar este procedimiento, debes cerrar la aplicación."
    StrCpy $InitINSTALL   "Inicializando el instalador de $(^Name)."
    StrCpy $WelcomeError "Error al intentar ejecutar el instalador de $(^Name)."
    StrCpy $AbortWarning "¿Está seguro de que desea salir de la instalación de $(^Name)?"
    StrCpy $TransferError "Error al copiar uno de los archivos extraídos a tu sistema.$\nSe requiere este archivo para ejecutar $(^Name) correctamente.$\nPara mostrar el error, haz clic en 'Ver detalles' o visita www.rockstargames.com/support para más información."
    
    StrCpy $RunUpdateErr "$(^Name) está abierto y no se puede actualizar. Cierra todas las aplicaciones de Rockstar Games."
	StrCpy $NewerVerErr  "Se ha instalado una nueva versión ($0) de $(^Name). Saliendo de la instalación."
	StrCpy $MTLRunning "Por favor, cierra la aplicación Iniciador de Rockstar Games e inténtalo de nuevo"
	StrCpy $RemoveFile "Eliminando archivos…"
FunctionEnd

Function AddSpanishVerError
	StrCpy $NewerVerErr  "Se ha instalado una nueva versión ($0) de $(^Name). Saliendo de la instalación."
FunctionEnd

Function un.AddSpanish
    StrCpy $WelcomeHead1	"Bienvenido."
	StrCpy $WelcomeHead2 	"Este asistente te guiará durante la instalación de $(^Name)."
	StrCpy $RunError     	"$(^Name) se está ejecutando.$\n Debes cerrar la aplicación para completar el proceso."   
	StrCpy $RunUpdateErr 	"$(^Name) se está ejecutando y no puede actualizarse. Cierra todas las aplicaciones de juego de Rockstar."
	StrCpy $AppErr       	"Todas las aplicaciones de Rockstar deben eliminarse antes de quitar $(^Name)."
		
	StrCpy $OptHeader1	 	"Bienvenido al programa de mantenimiento de $(^Name)."
	StrCpy $OptHeader2	 	"Este programa te permite modificar la instalación actual. Haz clic en una de las opciones de debajo y luego haz clic en Siguiente para continuar."
	StrCpy $ModifyHead	 	"Modificar."					
	StrCpy $ModifyText	 	"Elige características nuevas para añadírselas al programa o características instaladas actualmente para eliminarlas."
	StrCpy $RemoveHead   	"Borrar."
	StrCpy $RemoveText   	"Borra todas las características instaladas."
	StrCpy $ModHeader1	 	"Selecciona las opciones de instalación de $(^Name)." 
	StrCpy $ModHeader2   	"Selecciona las características que quieras conservar y desactiva las que quieras quitar."
	StrCpy $NeedReboot		"Su ordenador debe ser reiniciado para completar la desinstalación de $(^Name). ¿Desea reiniciar ahora?"

	StrCpy $ConfirmTitle	"Desinstalar $(^Name)"
	StrCpy $ConfirmMsg		"Elimina $(^Name) de su sistema."
	StrCpy $sDeleteReg   	"Borrando registros…"
	StrCpy $sDeletefiles 	"Borrando archivos…"
	StrCpy $sRemoveDir   	"Eliminando directorios…"
FunctionEnd

Function AddLangSelectGer
    StrCpy $LangTitle   "Installationssprache" 
    StrCpy $LangSelect  "Bitte wählen Sie die Installationssprache."
FunctionEnd

Function AddGerman
	StrCpy $AdminErrorRGSC "Rockstar Games Social Club benötigt Administratorrechte für die Installation.$\nBitte melden Sie sich als Administrator an und starten Sie diese Anwendung neu."
	StrCpy $AdminErrorPatcher "$(^Name) Updater benötigt Administratorrechte für die Installation.$\nBitte melden Sie sich als Administrator an und starten Sie diese Anwendung neu."
	StrCpy $AdminErrorDLC "$(^Name) Download-Installationsprogramm  benötigt Administratorrechte.$\nBitte melden Sie sich als Administrator an und starten Sie diese Anwendung neu."

    StrCpy $OSError     "Ihr System ist nicht kompatibel mit $(^Name)."
    StrCpy $RGSCError   "Fehler: $(^Name) wurde nicht gefunden.$\nBitte stellen Sie sicher, dass $(^Name) installiert ist und fehlerfrei läuft."
    StrCpy $GTAIVError  "Fehler: $(^Name) wurde nicht gefunden. Bitte stellen Sie sicher, dass $(^Name) installiert ist und fehlerfrei läuft."
    StrCpy $WelcomeHead1 "Willkommen."
    StrCpy $WelcomeHead2 "Dieser Assistent wird Sie durch die Installation von $(^Name) begleiten."
    StrCpy $WelcomeMsg1 "Willkommen zum $(^Name) Update Installer."
    StrCpy $WelcomeMsg2 "Die neuesten Features für $(^Name) werden installiert.$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Installiere Dateien."
    StrCpy $InstallMsg  "Installiere $(^Name) Dateien."
    StrCpy $RunError    "$(^Name) ist geöffnet.$\n Sie müssen die Anwendung schließen, um diesen Vorgang abzuschließen."
    StrCpy $InitINSTALL   "Initialisiere $(^Name) Installer."
    StrCpy $WelcomeError "Fehler: Versuche $(^Name) Installer zu starten."
    StrCpy $AbortWarning "Sind Sie sicher, dass Sie die Installation von $(^Name) abbrechen wollen?"
    StrCpy $TransferError "Eine der extrahierten Dateien konnte nicht auf ihr System kopiert werden.$\nDiese Datei wird benötigt um $(^Name) starten zu können.$\nUm sich den Fehler anzeigen zu lassen, klicken sie bitte auf Details anzeigen oder besuchen sie www.rockstargames.com/support für weitere Informationen und Anleitungen."
    
    StrCpy $RunUpdateErr "$(^Name) wird gerade ausgeführt und kann nicht aktualisiert werden. Bitte sämtliche Rockstar Games-Anwendungen beenden."
	StrCpy $NewerVerErr "Es ist bereits eine neuere Version ($0) von $(^Name) installiert. Die Installation wird jetzt beendet."
	StrCpy $MTLRunning "Bitte schließe den Rockstar Games Launcher und versuche es erneut."
	StrCpy $RemoveFile "Verzeichnisse entfernen …"
FunctionEnd

Function AddGermanVerError
	StrCpy $NewerVerErr "Es ist bereits eine neuere Version ($0) von $(^Name) installiert. Die Installation wird jetzt beendet."
FunctionEnd

Function un.AddGerman
    StrCpy $WelcomeHead1	"Willkommen."
	StrCpy $WelcomeHead2	"Dieser Assistent führt Sie durch die Installation von $(^Name)."
	StrCpy $RunError    	"$(^Name) läuft gerade.$\n Sie müssen das Programm beenden, um den Vorgang abzuschließen."   
	StrCpy $RunUpdateErr	"$(^Name) läuft und kann nicht aktualisiert werden. Bitte sämtliche Rockstar-Programme beenden."
	StrCpy $AppErr     		"Sämtliche Rockstar-Programme müssen vor dem Deinstallieren von $(^Name) entfernt werden."
		
	StrCpy $OptHeader1 		"Willkommen zum $(^Name) Setup-Wartungs-Programm."
	StrCpy $OptHeader2 		"Mit diesem Programm können Sie die aktuelle Installation modifizieren. Wählen Sie eine der unten aufgeführten Optionen und klicken Sie 'Weiter', um fortzufahren."
	StrCpy $ModifyHead 		"Modifizieren."					
	StrCpy $ModifyText 		"Fügen Sie neue Programm-Features hinzu oder entfernen Sie derzeit installierte Features."
	StrCpy $RemoveHead 		"Entfernen."
	StrCpy $RemoveText 		"Entfernen Sie sämtliche installierten Features."
	StrCpy $ModHeader1 		"Installationsoptionen für $(^Name) wählen." 
	StrCpy $ModHeader2 		"Wählen Sie die Features, die Sie behalten möchten und wählen Sie die Features ab, die Sie entfernen möchten."
	StrCpy $NeedReboot		"Windows muss neu gestartet werden, um die Deinstallation von $(^Name) zu vervollständigen. Möchten Sie Windows jetzt neu starten?"

	StrCpy $ConfirmTitle	"Deinstallation von $(^Name)"
	StrCpy $ConfirmMsg		"$(^Name) wird von Ihrem Computer entfernt."
	StrCpy $sDeleteReg   	"Registry-Einträge löschen …"
	StrCpy $sDeletefiles 	"Dateien löschen …"
	StrCpy $sRemoveDir   	"Ordner löschen …"
FunctionEnd

Function AddLangSelectRus
    StrCpy $LangTitle   "Язык установки" 
    StrCpy $LangSelect  "Пожалуйста, выберите язык установки."
FunctionEnd

Function AddRussian
    StrCpy $LANGUAGE 1049
	
	StrCpy $AdminErrorRGSC "Для установки Rockstar Games Social Club необходимы права администратора.$\nЗайдите как администратор системы и снова запустите это приложение."
	StrCpy $AdminErrorPatcher "Программе обновления $(^Name) необходимы права администратора.$\nЗайдите как администратор системы и снова запустите это приложение."
	StrCpy $AdminErrorDLC "Программе установки дополнений $(^Name) необходимы права администратора.$\nЗайдите как администратор системы и снова запустите это приложение."

    StrCpy $OSError     "Ваша система не поддерживается игрой $(^Name)."
    StrCpy $RGSCError   "Ошибка: Не обнаружено приложение $(^Name).$\nПожалуйста, проверьте что приложение $(^Name) установлено и работает без сбоев."
    StrCpy $GTAIVError  "Ошибка: Не найдена игра $(^Name). Пожалуйста, проверьте, что игра $(^Name) установлена и работает без сбоев."
    StrCpy $WelcomeHead1 "Добро пожаловать."
    StrCpy $WelcomeHead2 "Этот мастер установки направит вас через установку $(^Name)."
    StrCpy $WelcomeMsg1 "Вас приветствует программа установки обновления для $(^Name)."
    StrCpy $WelcomeMsg2 "Эта программа установит последние исправления для $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Установка файлов."
    StrCpy $InstallMsg  "Выполняется установка файлов $(^Name)."
    StrCpy $RunError    "У вас запущена игра $(^Name).$\nДля выполнения установки вам необходимо закрыть игровое приложение."
    StrCpy $InitINSTALL   "Подготовка к установке $(^Name)."
    StrCpy $WelcomeError "Ошибка: Не удалось запустить экран установки $(^Name)."
    StrCpy $AbortWarning "Вы действительно хотите отменить установку $(^Name)?"
    StrCpy $TransferError "Один из извлеченных файлов не смог быть скопирован в вашу систему.$\nЭтот файл нужен для того чтобы корректно запустить $(^Name).$\nПожалуйста, щелкните по кнопке Детали или зайдите на www.rockstargames.com/support для дольнейших инструкций."
    
    StrCpy $RunUpdateErr "$(^Name) работает и не может быть обновлен. Пожалуйста, закройте все приложения Rockstar Games."
	StrCpy $NewerVerErr  "Найдена более новая версия ($0) игры $(^Name). Выход из установки."
	StrCpy $MTLRunning "Закройте Rockstar Games Launcher и повторите попытку"
	StrCpy $RemoveFile "Удаление файлов…"
FunctionEnd 

Function AddRussianVerError
	StrCpy $NewerVerErr  "Найдена более новая версия ($0) игры $(^Name). Выход из установки."
FunctionEnd

Function un.AddRussian
    StrCpy $WelcomeHead1 	"Добро пожаловать."
	StrCpy $WelcomeHead2 	"Эта программа поможет вам установить $(^Name)."
	StrCpy $RunError     	"$(^Name) в данный момент запущена. $\n Вы должны закрыть ее, чтобы завершить процесс."
	StrCpy $RunUpdateErr 	"$(^Name) в данный момент запущена, и ее нельзя обновить. Пожалуйста, закройте все приложения Rockstar."
	StrCpy $AppErr       	"Перед удалением $(^Name) необходимо удалить все приложения Rockstar"
		
	StrCpy $OptHeader1		"Добро пожаловать в программу установки $(^Name)."
	StrCpy $OptHeader2		"Это программа позволяет вам работать с установленным приложением. Выберите нужную опцию и нажмите кнопку ''Далее''."
	StrCpy $ModifyHead		"Изменить."
	StrCpy $ModifyText		"Выберите компоненты, которые хотите добавить или удалить."
	StrCpy $RemoveHead   	"Удалить."
	StrCpy $RemoveText   	"Удалить все установленные компоненты."
	StrCpy $ModHeader1		"Выберите желаемые компоненты установки $(^Name)."
	StrCpy $ModHeader2   	"Выберите компоненты, которые хотите добавить, и снимите галочку с тех, которые хотите удалить."
	StrCpy $NeedReboot		"Для завершения удаления $(^Name) нужно перезагрузить компьютер. Хотите сделать это сейчас?"

	StrCpy $ConfirmTitle	"Удаление $(^Name)"
	StrCpy $ConfirmMsg		"Удаление $(^Name) из компьютера."
	StrCpy $sDeleteReg   	"Удаление из реестра…"
	StrCpy $sDeletefiles 	"Удаление файлов…"
	StrCpy $sRemoveDir   	"Удаление каталога…"
FunctionEnd 

Function AddLangSelectBRZ
    StrCpy $LangTitle  "Idioma do Instalador" 
    StrCpy $LangSelect "Por favor, selecione o idioma do instalador."
FunctionEnd

Function AddPortuguese
	StrCpy $AdminErrorRGSC "Rockstar Games Social Club requer privilégios de administrador para ser instalado.$\nPor favor faça o log-in como Administrador e reinicie este aplicativo."
	StrCpy $AdminErrorPatcher "O atualizador do $(^Name) requer privilégios de administrador para ser instaladol.$\nPor favor faça o log-in como Administrador e reinicie este aplicativo."
	StrCpy $AdminErrorDLC "O instalador do $(^Name) requer privilégios de administrador.$\nPor favor faça o log-in como Administrador e reinicie este aplicativo."

    StrCpy $OSError     "Seu sistema é incompatível com $(^Name)."
    StrCpy $WelcomeHead1 "Bem-vindo."
    StrCpy $WelcomeHead2 "Este assistente irá guiá-lo na instalação de $(^Name)."
    StrCpy $WelcomeMsg1 "Bem-vindo ao instalador de atualizações de $(^Name)."
    StrCpy $WelcomeMsg2 "Este irá instalar os recursos mais recentes de $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Instalando os arquivos."
    StrCpy $InstallMsg  "Instalando os arquivos de $(^Name)."
    StrCpy $RunError    "$(^Name) está sendo executado.$\n É necessário encerrar o aplicativo para completar este processo."
    StrCpy $InitINSTALL  "Inicializando o instalador de $(^Name)."
    StrCpy $WelcomeError "Erro: tentando executar o instalador de $(^Name)."
    StrCpy $AbortWarning "Tem certeza de que deseja sair da configuração de $(^Name)?"
    StrCpy $TransferError "Um dos arquivos extraídos falhou ao ser copiado em seu sistema.$\nEste arquivo é necessário para executar $(^Name) corretamente.$\nPara ver o erro, pressione Exibir Informações ou$\nacesse www.rockstargames.com/support para mais instruções."
    
    StrCpy $RunUpdateErr "$(^Name) está em andamento e não pode ser atualizado. Por favor, encerre todos os Aplicativos de Jogos da Rockstar."
    StrCpy $NewerVerErr  "A nova versão $0 para $(^Name) foi instalada. Encerrando a instalação."
	StrCpy $MTLRunning "Feche o Inicializador Rockstar Games e tente novamente."
	StrCpy $RemoveFile "Excluindo arquivos:"
FunctionEnd

Function AddPortugueseVerError
	StrCpy $NewerVerErr  "A nova versão $0 para $(^Name) foi instalada. Encerrando a instalação."
FunctionEnd

Function un.AddPortuguese
	StrCpy $WelcomeHead1	"Bem-vindo."
    StrCpy $WelcomeHead2	"Este assistente irá guiá-lo na instalação de $(^Name)."
    StrCpy $RunError		"$(^Name) está sendo executado.$\n É necessário encerrar o aplicativo para completar este processo."   
    StrCpy $RunUpdateErr	"$(^Name) está em andamento e não pode ser atualizado. Por favor, encerre todos os Aplicativos de Jogos da Rockstar."
    StrCpy $AppErr			"Todos os Aplicativos da Rockstar devem ser removidos antes de remover $(^Name)"
		
	StrCpy $OptHeader1	 	"Bem-vindo ao programa de manutenção da configuração do $(^Name)."
	StrCpy $OptHeader2	 	"Este programa permite que você modifique a instalação atual. Clique em uma das opções abaixo e clique em Avançar para continuar."
	StrCpy $ModifyHead	 	"Modificar."					
	StrCpy $ModifyText	 	"Selecione novos recursos do programa para adicionar ou selecione recursos atualmente instalados para remover."
	StrCpy $RemoveHead   	"Remover."
	StrCpy $RemoveText   	"Remova todos os recursos instalados."
	StrCpy $ModHeader1	 	"Selecione as opções de instalação do $(^Name)." 
	StrCpy $ModHeader2   	"Marque os recursos que deseja manter, e desmarque os recursos que deseja remover."
	StrCpy $NeedReboot		"Seu computador tem que ser reiniciado para completar a desinstalação da $(^Name). Você quer reiniciar agora?"

	StrCpy $ConfirmTitle	"Desinstalar a $(^Name)"
	StrCpy $ConfirmMsg		"Remover a $(^Name) do seu computador."
	StrCpy $sDeleteReg   	"Apagando registro:"
	StrCpy $sDeletefiles 	"Apagando arquivos:"
	StrCpy $sRemoveDir   	"Excluindo diretório:"
FunctionEnd 

Function AddLangSelectPol
    StrCpy $LangTitle		"Język programu instalacyjnego" 
    StrCpy $LangSelect		"Proszę wybrać język programu instalacyjnego."
FunctionEnd

Function AddPolish
	StrCpy $AdminErrorRGSC "Rockstar Games Social Club wymaga do instalacji przywilejów administratora.$\nProszę się zalogować jako administrator i ponownie uruchomić tę aplikację."
	StrCpy $AdminErrorPatcher "Aplikacja aktualizująca gry $(^Name) wymaga do instalacji przywilejów administratora.$\nProszę się zalogować jako administrator i ponownie uruchomić tę aplikację."
	StrCpy $AdminErrorDLC "Instalator gry $(^Name) wymaga przywilejów administratora.$\nProszę się zalogować jako administrator i ponownie uruchomić tę aplikację."

    StrCpy $OSError     "Twój system jest niekompatybilny z grą $(^Name)."
    StrCpy $WelcomeHead1 "Witaj."
    StrCpy $WelcomeHead2 "Ten program przeprowadzi cię przez proces instalacji gry $(^Name)."
    StrCpy $WelcomeMsg1 "Witaj w programie instalacyjnym aktualizacji gry $(^Name)."
    StrCpy $WelcomeMsg2 "Program zainstaluje najnowsze funkcje gry $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Instalowanie plików."
    StrCpy $InstallMsg  "Instalowanie plików gry $(^Name)."
    StrCpy $RunError    "Gra $(^Name) jest uruchomiona.$\n Musisz zamknąć aplikację, żeby dokończyć trwający proces."
    StrCpy $InitINSTALL  "Inicjalizacja programu instalacyjnego gry $(^Name)."
    StrCpy $WelcomeError "Błąd: Próba uruchomienia programu instalacyjnego $(^Name)."
    StrCpy $AbortWarning "Czy na pewno chcesz opuścić program instalacyjny gry $(^Name)?"
    StrCpy $TransferError "Jeden z rozpakowanych plików nie został przekopiowany na twój system.$\nPlik ten jest wymagany do prawidłowego działania gry $(^Name).$\nŻeby zapoznać się ze szczegółami tego błędu, kliknij przycisk szczegółów lub$\nodwiedź stronę www.rockstargames.com/support."
    
    StrCpy $RunUpdateErr "Gra $(^Name) jest uruchomiona i nie może zostać zaktualizowana. Zamknij wszystkie aplikacje Rockstar Games."
    StrCpy $NewerVerErr  "Zainstalowano już nowszą wersję gry $(^Name), $0. Opuszczanie programu instalacyjnego."
	StrCpy $MTLRunning "Proszę zamknąć aplikację uruchamiającą Rockstar Games i spróbować ponownie."
	StrCpy $RemoveFile "Usuwanie plików…"
FunctionEnd

Function AddPolishVerError
	StrCpy $NewerVerErr  "Zainstalowano już nowszą wersję gry $(^Name), $0. Opuszczanie programu instalacyjnego."
FunctionEnd

Function un.AddPolish
	StrCpy $WelcomeHead1 "Witaj."
    StrCpy $WelcomeHead2 "Ten program przeprowadzi cię przez proces instalacji gry $(^Name)."
    StrCpy $RunError    "Gra $(^Name) jest uruchomiona.$\n Musisz zamknąć aplikację, żeby dokończyć trwający proces."   
    StrCpy $RunUpdateErr "Gra $(^Name) jest uruchomiona i nie może zostać zaktualizowana. Zamknij wszystkie aplikacje Rockstar Games."
    StrCpy $AppErr       "Wszystkie aplikacje Rockstar muszą zostać usunięte, zanim możliwe będzie usunięcie gry $(^Name)."
		
	StrCpy $OptHeader1	"Witamy w programie instalacyjnym gry $(^Name)."
	StrCpy $OptHeader2	"Program umożliwia ci modyfikację aktualnej instalacji. Wybierz opcje poniżej i kliknij 'Dalej', żeby kontynuować."
	StrCpy $ModifyHead	"Zmień."
	StrCpy $ModifyText	"Wybierz nowe funkcje programu do dodania lub już zainstalowane do usunięcia."
	StrCpy $RemoveHead	"Usuń."
	StrCpy $RemoveText	"Usuń wszystkie zainstalowane funkcje."
	StrCpy $ModHeader1	"Wybierz opcje instalacji gry $(^Name)."
	StrCpy $ModHeader2	"Wybierz funkcje, które chcesz zachować, i odznacz te, które chcesz usunąć."
	StrCpy $NeedReboot  "Twój komputer musi zostać ponownie uruchomiony w celu zakończenia deinstalacji programu $(^Name). Czy chcesz zrobić to teraz?"

	StrCpy $ConfirmTitle	"Odinstaluj $(^Name)"
	StrCpy $ConfirmMsg		"Usuń $(^Name) z Twojego komputera."
	StrCpy $sDeleteReg   	"Usuwanie wpisów w rejestrze…"
	StrCpy $sDeletefiles 	"Usuwanie plików…"
	StrCpy $sRemoveDir   	"Usuwanie folderów…"
FunctionEnd 

Function AddLangSelectKor
	StrCpy $LangTitle "설치 관리자 언어" 
    StrCpy $LangSelect "설치 관리자에서 사용할 언어를 선택합니다."
FunctionEnd

Function AddKorean
    StrCpy $AdminErrorRGSC "$(^Name)을(를) 설치하려면 관리자 권한이 필요합니다.$\n관리자로 로그인한 다음 이 프로그램을 다시 시작하십시오."
	StrCpy $AdminErrorPatcher "$(^Name) 업데이트 관리자를 설치하려면 관리자 권한이 필요합니다.$\n관리자로 로그인한 다음 이 프로그램을 다시 시작하십시오."
	StrCpy $AdminErrorDLC "$(^Name) 다운로드 설치자는 관리자 권한이 필요합니다.$\n관리자로 로그인한 다음 이 프로그램을 다시 시작하십시오."

    StrCpy $OSError "사용자의 시스템이 $(^Name)와(과) 호환되지 않습니다."
    StrCpy $WelcomeHead1 "환영합니다."
    StrCpy $WelcomeHead2 "이 마법사는 $(^Name) 설치 과정을 안내합니다. "
	StrCpy $WelcomeMsg1 "$(^Name) 업데이트 설치 관리자에 오신 걸 환영합니다."
    StrCpy $WelcomeMsg2 "$(^Name)의 최신 기능을 설치합니다.$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "파일을 설치하고 있습니다."
    StrCpy $InstallMsg "$(^Name) 파일을 설치하고 있습니다."
    StrCpy $RunError "$(^Name)이(가) 이미 실행 중입니다.$\n 이 과정을 마치려면 프로그램을 종료하십시오."
    StrCpy $InitINSTALL "$(^Name) 설치 관리자를 초기화하고 있습니다."
    StrCpy $WelcomeError "오류: $(^Name) 설치 관리자를 실행하려고 하는 중입니다."
    StrCpy $AbortWarning "$(^Name) 설치를 그만두시겠습니까?"
    StrCpy $TransferError "추출된 파일 중 하나를 사용자의 시스템에 복사하지 못했습니다.$\n$(^Name)을(를) 올바르게 실행하려면 이 파일이 필요합니다.$\n오류 내용을 보려면 '자세한 정보 표시'를 클릭하시고, $\n자세한 내용은 www.rockstargames.com/support를 방문하십시오."
    
    StrCpy $RunUpdateErr "$(^Name)이(가) 이미 실행 중이며 업데이트할 수 없습니다. 모든 Rockstar 게임 프로그램을 종료하십시오."
    StrCpy $NewerVerErr "$(^Name)용 새 버전 $0이(가) 설치되었습니다. 설치를 마칩니다."
	StrCpy $MTLRunning "Rockstar Games 시작 관리자 애플리케이션을 닫고 다시 시도하십시오."
	StrCpy $RemoveFile "파일을 삭제합니다… "
FunctionEnd

Function AddKoreanVerError
    StrCpy $NewerVerErr "$(^Name)용 새 버전 $0이(가) 설치되었습니다. 설치를 마칩니다."
FunctionEnd

Function un.AddKorean
	StrCpy $WelcomeHead1 "환영합니다."
    StrCpy $WelcomeHead2 "이 마법사는 $(^Name) 설치 과정을 안내합니다."
	StrCpy $RunError "$(^Name)이(가) 이미 실행 중입니다.$\n 이 과정을 마치려면 프로그램을 종료하십시오."   
    StrCpy $RunUpdateErr "$(^Name)이(가) 이미 실행 중이며 업데이트할 수 없습니다. 모든 Rockstar 게임 프로그램을 종료하십시오."
    StrCpy $AppErr "$(^Name)을(를) 제거하려면 먼저 모든 Rockstar 프로그램을 제거해야 합니다."
	
	StrCpy $OptHeader1 "$(^Name) 설치 유지 관리 프로그램에 오신 것을 환영합니다."
	StrCpy $OptHeader2 "이 프로그램은 현재 설치를 수정합니다. 아래 항목 중 하나를 클릭하고 '다음'을 클릭해서 진행하십시오."
	StrCpy $ModifyHead "수정"					
	StrCpy $ModifyText "새 프로그램 기능을 선택하여 추가하거나 현재 설치되어 있는 기능을 삭제합니다."
	StrCpy $RemoveHead "제거"
	StrCpy $RemoveText "설치되어 있는 모든 기능을 제거합니다."
	StrCpy $ModHeader1 "$(^Name)의 설치 옵션을 선택합니다." 
	StrCpy $ModHeader2 "유지할 기능은 선택하고 제거할 기능은 선택을 해제하십시오."
	StrCpy $NeedReboot "$(^Name)의 제거를 완료하기 위해서는 시스템을 다시 시작해야 합니다. 지금 재부팅 하시겠습니까?"

	StrCpy $ConfirmTitle	"$(^Name) 제거"
	StrCpy $ConfirmMsg		"$(^Name) 제거하기"
	StrCpy $sDeleteReg   	"레지스트리를 삭제합니다…"
	StrCpy $sDeletefiles 	"파일을 삭제합니다…"
	StrCpy $sRemoveDir   	"디렉터리를 삭제합니다…"

FunctionEnd

Function AddLangSelectJap
	StrCpy $LangTitle "インストーラー言語" 
    StrCpy $LangSelect "インストーラーの言語を選択してください"
FunctionEnd

Function AddJapanese
	StrCpy $AdminErrorRGSC "$(^Name)のインストールには管理者権限が必要です。$\n管理者としてログインし、アプリケーションを再起動してください"
	StrCpy $AdminErrorPatcher "$(^Name)のアップデーターのインストールには管理者権限が必要です。$\n管理者としてログインし、アプリケーションを再起動してください"
	StrCpy $AdminErrorDLC "$(^Name)のダウンロードインストーラーには管理者権限が必要です。$\n管理者としてログインし、アプリケーションを再起動してください"

    StrCpy $OSError "ご利用のシステムは$(^Name)と互換性がありません"
    StrCpy $WelcomeHead1 "ようこそ"
    StrCpy $WelcomeHead2 "このウィザードは$(^Name)のインストール手順をご案内します"
    StrCpy $WelcomeMsg1 "$(^Name)のアップデートインストーラーへようこそ"
    StrCpy $WelcomeMsg2 "ここでは$(^Name)の最新機能をインストールします。$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "ファイルをインストール中"
    StrCpy $InstallMsg "$(^Name)のファイルをインストール中"
    StrCpy $RunError "現在、$(^Name)は起動中です。$\nこの作業を完了するには、アプリケーションを終了する必要があります"
    StrCpy $InitINSTALL "$(^Name)のインストーラーを初期化中"
    StrCpy $WelcomeError "エラー：$(^Name)インストーラーの起動に失敗"
    StrCpy $AbortWarning "$(^Name)のセットアップを終了しますか？"
    StrCpy $TransferError "システムへのコピーに失敗した解凍ファイルがあります。$\nこのファイルは$(^Name)を正常に起動するために必要です。$\nエラーを確認するには、「詳細の確認」をクリックするか、$\nwww.rockstargames.com/supportでより詳しい情報をご確認ください"
    
    StrCpy $RunUpdateErr "$(^Name)が起動中のため、アップデートできません。ロックスター・ゲームスのアプリケーションをすべて終了してください"
    StrCpy $NewerVerErr "より新しいバージョン$0がインストールされています。$(^Name)のインストールを終了します"
	StrCpy $MTLRunning "ロックスター・ゲームス ランチャー アプリを終了し、再度お試しください。"
	StrCpy $RemoveFile "ファイルを削除…"
FunctionEnd

Function AddJapaneseVerError
    StrCpy $NewerVerErr "より新しいバージョン$0がインストールされています。$(^Name)のインストールを終了します"
FunctionEnd

Function un.AddJapanese
    StrCpy $WelcomeHead1 "ようこそ"
    StrCpy $WelcomeHead2 "このウィザードは$(^Name)のインストール手順をご案内します"
    StrCpy $RunError "現在、$(^Name)は起動中です。$\nこの作業を完了するには、アプリケーションを終了する必要があります"   
    StrCpy $RunUpdateErr "$(^Name)が起動中のため、アップデートできません。ロックスター・ゲームスのアプリケーションをすべて終了してください"
    StrCpy $AppErr "ロックスター・ゲームスのアプリケーションをすべて削除してから$(^Name)を削除してください"
	
    StrCpy $OptHeader1 "$(^Name)のセットアップ管理プログラムへようこそ"
    StrCpy $OptHeader2 "このプログラムでは、現在インストールされている内容を変更できます。下記のオプションを1つ選択して「次へ」をクリックしてください"
    StrCpy $ModifyHead "変更"					
    StrCpy $ModifyText "新たに追加したいプログラムや、現在インストールされていて削除したい機能を選択できます"
    StrCpy $RemoveHead "削除"
    StrCpy $RemoveText "インストールした機能をすべて削除します"
    StrCpy $ModHeader1 "$(^Name)のインストールオプションを選択してください" 
    StrCpy $ModHeader2 "残したい機能を選択し、削除したい機能は選択を外してください"
	StrCpy $NeedReboot "$(^Name) のアンインストールを完了するには、このコンピュータを再起動する必要があります。今すぐ再起動しますか？"

	StrCpy $ConfirmTitle "$(^Name)のアンインストール"	
	StrCpy $ConfirmMsg "$(^Name)をこのコンピュータから削除します。"
	StrCpy $sDeleteReg   	"レジストリを削除…"
	StrCpy $sDeletefiles 	"ファイルを削除…"
	StrCpy $sRemoveDir   	"ディレクトリを削除…"
FunctionEnd

Function AddMexican
    
	StrCpy $AdminErrorRGSC		"El Social Club de Rockstar Games requiere privilegios de administrador para instalar.$\nPor favor accede como administrador y reinicia la apilicación."
	StrCpy $AdminErrorPatcher  "Updater requires administrator privileges to install.$\nPlease log in as Administrator and restart this application."
	StrCpy $AdminErrorDLC		"Download installer requires administrator privileges.$\nPlease log in as Administrator and restart this application."
	
	StrCpy $OSError			"Tu sistema es incompatible con $(^Name)."
    StrCpy $WelcomeHead1	"Bienvenido."
    StrCpy $WelcomeHead2	"Este wizard te guiará por la instalación de $(^Name)."
	StrCpy $WelcomeMsg1		"Bienvenido al instalador de $(^Name)."
    StrCpy $WelcomeMsg2		"Esto instalará las últimas características de $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead		"Instalando archivos."
    StrCpy $InstallMsg		"Instalando archivos de $(^Name)."
    StrCpy $RunError		"$(^Name) está activo actualmente. $\n Tienes que cerrar la aplicación para completar el proceso."
    StrCpy $InitINSTALL		"Inicializando el instalador de $(^Name)."
    StrCpy $WelcomeError	"Error: Intentando abrir el instalador de $(^Name)."
    StrCpy $AbortWarning	"¿Seguro que quieres salir de la configuración de $(^Name)?"
    StrCpy $TransferError	"Uno de los archivos extraídos no se ha podido copiar a tu sistema.$\nEste archivo es necesario para iniciar $(^Name) correctamente.$\nPara mostrar el error, por favor haz click en 'mostrar detalles' o$\nvisita www.rockstargames.com/support para más instrucciones."
    
    StrCpy $RunUpdateErr	"$(^Name) está activo y no puede ser actualizado. Por favor cierra todas las aplicaciones de Rockstar Games."
    StrCpy $NewerVerErr		"Una nueva versión $0 instalada para $(^Name). Saliendo de la instalación."
	StrCpy $MTLRunning "Cierra la aplicación de la plataforma de Rockstar Games y vuelve a intentarlo"
	StrCpy $RemoveFile "Eliminar archivos…"
FunctionEnd

Function AddMexicanVerError
	StrCpy $NewerVerErr  "Una nueva versión $0 instalada para $(^Name). Saliendo de la instalación."
FunctionEnd

Function un.AddMexican
	
	StrCpy $WelcomeHead1 "Bienvenido.."
    StrCpy $WelcomeHead2 "Este wizard te guiará por la instalación de $(^Name)."
	StrCpy $RunError     "$(^Name) está activo actualmente. $\n Tienes que cerrar la aplicación para completar el proceso."   
    StrCpy $RunUpdateErr "$(^Name) está activo y no puede ser actualizado. Por favor cierra todas las aplicaciones de Rockstar Games."
    StrCpy $AppErr       "Todas las aplicaciones de Rockstar tienen que ser eliminadas antes de eliminar $(^Name)."
	
	StrCpy $OptHeader1	 "Bienvenido al programa de configuración de mantenimiento de $(^Name)."
	StrCpy $OptHeader2	 "Este programa te permite modificar la instalación actual. Haz click en las opciones debajo y luego haz click en siguiente para continuar."
	StrCpy $ModifyHead	 "Modificar."					
	StrCpy $ModifyText	 "Selecciona características de programa que añadir o selecciona las ya instaladas para removerlas."
	StrCpy $RemoveHead   "Remover."
	StrCpy $RemoveText   "Remover todas las característiccas instaladas."
	StrCpy $ModHeader1	 "Selecciona las opciones de instalación para $(^Name)." 
	StrCpy $ModHeader2   "Selecciona las características que quieres mantener y desmarca las que quieres remover."
	StrCpy $NeedReboot	 "Su computadora debe ser reiniciada para finalizar la desinstalación de $(^Name). ¿Desea reiniciar ahora?"
	
	StrCpy $ConfirmTitle	"Desinstalar $(^Name)"
	StrCpy $ConfirmMsg		"Elimina $(^Name) de su sistema."
	StrCpy $sDeleteReg   	"Borrar registro…"
	StrCpy $sDeletefiles 	"Eliminar archivos…"
	StrCpy $sRemoveDir   	"Eliminar directorio…"
FunctionEnd

Function AddLangSelectMX
    StrCpy $LangTitle  	"Idioma del instalador" 
    StrCpy $LangSelect 	"Selecciona el idioma del instalador."
FunctionEnd

Function AddTraditionalChinese
    
	StrCpy $AdminErrorRGSC		"Rockstar Games Social Club 需要系統管理員權限才能安裝，$\n請以系統管理員的身分登入並重新執行此程式。"
	StrCpy $AdminErrorPatcher  "$(^Name) updater requires administrator privileges to install.$\nPlease log in as Administrator and restart this application."
	StrCpy $AdminErrorDLC		"$(^Name) download installer requires administrator privileges.$\nPlease log in as Administrator and restart this application."
	
	StrCpy $OSError     "您的系統與 $(^Name) 不相容。"
    StrCpy $WelcomeHead1 "歡迎。"
    StrCpy $WelcomeHead2 "安裝精靈將會於 $(^Name) 的安裝過程中引導您。"
	StrCpy $WelcomeMsg1 "歡迎使用 $(^Name) 更新安裝程式。"
    StrCpy $WelcomeMsg2 "此程式將會安裝 $(^Name) 的最新內容。$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "檔案安裝中。"
    StrCpy $InstallMsg  "$(^Name) 檔案安裝中。"
    StrCpy $RunError    "$(^Name) 目前執行中。您必須關閉程式以完成此程序。"
    StrCpy $InitINSTALL  "$(^Name) 安裝程式初始化中。"
    StrCpy $WelcomeError "錯誤：嘗試執行 $(^Name) 安裝程式。"
    StrCpy $AbortWarning "您確定要退出 $(^Name) 安裝程式？"
    StrCpy $TransferError "其中一個已解壓縮的檔案無法複製至您的系統。$\n必須擁有此檔案以正確地執行 $(^Name)。$\n點擊「顯示詳細資料」以顯示錯誤訊息，$\n如需更多資訊，請前往 www.rockstargames.com/support"
    
    StrCpy $RunUpdateErr "$(^Name) 正在執行中，無法更新。請關閉所有的 Rockstar 遊戲應用程式。"
    StrCpy $NewerVerErr  "$(^Name) 已安裝了較新的版本 $0。正在結束安裝程式。"
	StrCpy $MTLRunning "請關閉 Rockstar Games Launcher 應用程式，並再試一次"
	StrCpy $RemoveFile "移除檔案…"
FunctionEnd

Function TraditionalChineseVerError
	StrCpy $NewerVerErr  "$(^Name) 已安裝了較新的版本 $0。正在結束安裝程式。"
FunctionEnd

Function un.AddTraditionalChinese
	StrCpy $WelcomeHead1 "歡迎。"
    StrCpy $WelcomeHead2 "安裝精靈將會於 $(^Name) 的安裝過程中引導您。"
	StrCpy $RunError     "$(^Name) is currently running.$\n You Must close the application in order to complete this process."   
    StrCpy $RunUpdateErr "$(^Name) 正在執行中，無法更新。請關閉所有的 Rockstar 遊戲應用程式。"
    StrCpy $AppErr       "請於移除 $(^Name) 前移除所有的 Rockstar 應用程式。"
	
	StrCpy $OptHeader1	 "歡迎使用 $(^Name) 安裝維護程式。"
	StrCpy $OptHeader2	 "此程式可以讓您修改已安裝的程式。選擇下列的選項後，請按「下一步」繼續。"
	StrCpy $ModifyHead	 "修改。"					
	StrCpy $ModifyText	 "請選取欲新增的程式功能或是欲移除的已安裝功能。"
	StrCpy $RemoveHead   "移除。"
	StrCpy $RemoveText   "移除所有已安裝的功能。"
	StrCpy $ModHeader1	 "請選取 $(^Name) 的安裝選項。" 
	StrCpy $ModHeader2   "請選取您欲保留的功能或是取消選取您欲移除的功能。"
	StrCpy $NeedReboot	 "電腦需要重新啟動，以便完成 $(^Name) 的解除安裝。現在想要重新啟動嗎？"
	
	StrCpy $ConfirmTitle	"解除安裝 $(^Name)"
	StrCpy $ConfirmMsg		"從你的電腦解除安裝 $(^Name) 。"
	StrCpy $sDeleteReg   	"刪除登錄…"
	StrCpy $sDeletefiles 	"刪除檔案…"
	StrCpy $sRemoveDir   	"移除目錄…"
FunctionEnd

Function AddLangSelectChina
	StrCpy $LangTitle	“安装程序语言”
	StrCpy $LangSelect 	“请选择安装程序的语言。”
FunctionEnd

Function AddSimpChinese
    
	StrCpy $AdminErrorRGSC		"Rockstar Games Social Club 需要管理员权限才可安装。$\n请以管理员身份登录并重新运行此应用程序。"
	StrCpy $AdminErrorPatcher  "$(^Name) 更新程序需要管理员权限才能安装。$\n请以管理员身份登录并重新启动此应用程序。"
	StrCpy $AdminErrorDLC		"$(^Name) 下载安装程序需要管理员权限。$\n请以管理员身份登录并重新启动此应用程序。"
	
	StrCpy $OSError     "您的系统与 $(^Name) 不兼容。"
    StrCpy $WelcomeHead1 "欢迎。"
    StrCpy $WelcomeHead2 "该向导将引导您完成 $(^Name) 的安装。"
	StrCpy $WelcomeMsg1 "欢迎使用 $(^Name) 更新安装程序。"
    StrCpy $WelcomeMsg2 "此程序将安装 $(^Name) 的最新功能。$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "正在安装文件。"
    StrCpy $InstallMsg  "正在安装 $(^Name) 文件。"
    StrCpy $RunError    "$(^Name) 当前正在运行。$\n您必须关闭应用程序才能完成此安装过程。"
    StrCpy $InitINSTALL  "正在初始化 $(^Name) 安装程序。"
    StrCpy $WelcomeError "错误：正在尝试运行 $(^Name) 安装程序。"
    StrCpy $AbortWarning "您确定要退出 $(^Name) 安装程序吗？"
    StrCpy $TransferError "提取的文件中有一个无法复制到您的系统。$\n需要此文件以确保游戏正确运行。$\n若要显示错误，请单击“显示详细信息”或$\n访问 www.rockstargames.com/support 获取更多教学。"
    
    StrCpy $RunUpdateErr "$(^Name) 正在运行，无法更新。请关闭所有的 Rockstar 游戏应用程序。"
    StrCpy $NewerVerErr  "$(^Name) 的新版本$0安装完成。正在退出安装。"
	StrCpy $MTLRunning "Please close the Rockstar Games Launcher Application and try again"
	StrCpy $RemoveFile "删除文件…… "
FunctionEnd

Function SimpChineseVerError
	StrCpy $NewerVerErr  "$(^Name) 的新版本$0安装完成。正在退出安装。"
FunctionEnd

Function un.AddSimpChinese
	StrCpy $WelcomeHead1 "欢迎。"
    StrCpy $WelcomeHead2 "该向导将引导您完成 $(^Name) 的安装。"
	StrCpy $RunError     "$(^Name) 当前正在运行。$\n您必须关闭应用程序才能完成此安装过程。"   
    StrCpy $RunUpdateErr "$(^Name) 正在运行，无法更新。请关闭所有的 Rockstar 游戏应用程序。"
    StrCpy $AppErr       "在删除 $(^Name) 之前，必须删除所有 Rockstar 应用程序。"
	
	StrCpy $OptHeader1	 "欢迎使用 $(^Name) 安装维护程序。"
	StrCpy $OptHeader2	 "该程序允许您修改当前的安装。单击以下的一个选项，然后单击“下一步”继续。"
	StrCpy $ModifyHead	 "修改。"					
	StrCpy $ModifyText	 "选择添加新的程序功能，或选择删除当前已安装的功能。"
	StrCpy $RemoveHead   "删除。"
	StrCpy $RemoveText   "删除所有已安装的功能。"
	StrCpy $ModHeader1	 "选择 $(^Name) 的安装选项。" 
	StrCpy $ModHeader2   "选择您想要保留的功能，然后取消选择您想要删除的功能。"
	StrCpy $NeedReboot	 "您的计算机必须重新启动才能完成卸载 $(^Name)。你想立即重启吗？"
	
	StrCpy $ConfirmTitle	"卸载 $(^Name)"
	StrCpy $ConfirmMsg		"从您的计算机中删除 $(^Name)。"
	StrCpy $sDeleteReg   	"删除注册表信息……"
	StrCpy $sDeletefiles 	"删除文件……"
	StrCpy $sRemoveDir   	"删除目录……"
FunctionEnd

Function AddLangSelectSimpChina
FunctionEnd

!insertmacro LANGFILE "English" "English"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_ENGLISH} "License Agreement"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_ENGLISH} "Please review the license terms before installing $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_ENGLISH} "If you accept the terms of the agreement, click the check box below. You must accept the agreement to install $(^Name). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_ENGLISH} "Press Page Down to see the rest of the agreement."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_ENGLISH} "Installing"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_ENGLISH} "Please wait while $(^Name) is being installed."
LangString MUI_TEXT_FINISH_TITLE ${LANG_ENGLISH} "Install Complete"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_ENGLISH} "Setup was completed successfully. Click Close to finish."
LangString MUI_TEXT_ABORT_TITLE ${LANG_ENGLISH} "The uninstall was aborted"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_ENGLISH} "Setup was not completed successfully. Click Close to finish."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_ENGLISH} "Uninstalling"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_ENGLISH} "Please wait while $(^Name) is being uninstalled."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_ENGLISH} "Uninstallation was completed successfully. Click Close to finish."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_ENGLISH} "Uninstallation Complete"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_ENGLISH} "Uninstallation Aborted"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_ENGLISH} "Setup was not completed successfully. Click Close to finish."


!insertmacro LANGFILE "French" "French"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_FRENCH} "Licence utilisateur"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_FRENCH} "Veuillez examiner les termes de la licence avant d'installer $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_FRENCH} "Si vous acceptez les conditions de la licence utilisateur, cochez la case ci-dessous. Vous devez accepter la licence utilisateur afin d'installer $(^Name)."
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_FRENCH} "Appuyez sur Page Suivante pour lire le reste de la licence utilisateur."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_FRENCH} "Installation en cours"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_FRENCH} "Veuillez patienter pendant que $(^Name) est en train d'être installé."
LangString MUI_TEXT_FINISH_TITLE ${LANG_FRENCH} "Installation terminée"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_FRENCH} "L'installation s'est terminée avec succès."
LangString MUI_TEXT_ABORT_TITLE ${LANG_FRENCH} "Installation interrompue"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_FRENCH} "L'installation n'a pas été terminée."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_FRENCH} "Désinstallation"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_FRENCH} "Veuillez patienter pendant la désinstallation de $(^Name)."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_FRENCH} "Désinstallation réussie. Appuyez sur Fermer pour finir."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_FRENCH} "La désinstallation est terminée."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_FRENCH} "La désinstallation a été annulée."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_FRENCH} "L'installation a échoué. Appuyez sur Fermer pour finir."


!insertmacro LANGFILE "Italian" "Italian"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_ITALIAN} "Licenza d'uso"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_ITALIAN} "Prego leggere le condizioni della licenza d'uso prima di installare Social Club di Rockstar Games."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_ITALIAN} "Se si accettano i termini della licenza d'uso, selezionare la casella sottostante. È necessario accettare i termini della licenza d'uso per installare il Social Club di Rockstar Games. Per avviare l'installazione, selezionare Installa."
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_ITALIAN} "Premere PAG GIU per vedere il resto della licenza d'uso."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_ITALIAN} "Installazione in corso"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_ITALIAN} "Prego attendere mentre Social Club di Rockstar Games  viene installato."
LangString MUI_TEXT_FINISH_TITLE ${LANG_ITALIAN} "Installazione completata"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_ITALIAN} "L'installazione è stata completata con successo."
LangString MUI_TEXT_ABORT_TITLE ${LANG_ITALIAN} "Installazione interrotta"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_ITALIAN} "L'installazione non è stata completata correttamente."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_ITALIAN} "Disinstallazione in corso..."
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_ITALIAN} "Attendi mentre $(^Name) viene disinstallato."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_ITALIAN} "La disinstallazione è stata completata con successo. Clicca 'Chiudi' per uscire."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_ITALIAN} "La disinstallazione è stata completata."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_ITALIAN} "La disinstallazione è stata interrotta."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_ITALIAN} "L'installazione non è stata completata. Clicca 'Chiudi' per uscire."

!insertmacro LANGFILE "German" "German"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_GERMAN} "Lizenzabkommen"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_GERMAN} "Bitte lesen Sie die Lizenzbedingungen durch, bevor Sie mit der Installation fortfahren."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_GERMAN} "Falls Sie alle Bedingungen des Abkommens akzeptieren, aktivieren Sie das Kästchen. Sie müssen die Lizenzvereinbarungen anerkennen, um $(^Name) installieren zu können. $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_GERMAN} "Drücken Sie die Bild-nach-unten Taste, um den Rest des Abkommens zu sehen."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_GERMAN} "Installiere..."
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_GERMAN} "Bitte warten Sie, während $(^Name) installiert wird."
LangString MUI_TEXT_FINISH_TITLE ${LANG_GERMAN} "Die Installation ist vollständig"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_GERMAN} "Die Installation wurde erfolgreich abgeschlossen."
LangString MUI_TEXT_ABORT_TITLE ${LANG_GERMAN} "Abbruch der Installation"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_GERMAN} "Die Installation wurde nicht vollständig abgeschlossen."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_GERMAN} "Deinstallieren..."
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_GERMAN} "Bitte warten, während $(^Name) deinstalliert wird."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_GERMAN} "Deinstallation erfolgreich abgeschlossen. Zum Beenden 'Schließen' klicken."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_GERMAN} "Deinstallation erfolgreich."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_GERMAN} "Deinstallation abgebrochen."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_GERMAN} "Installation nicht erfolgreich. Zum Beenden 'Schließen' klicken."


!insertmacro LANGFILE "Spanish" "Spanish"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_SPANISH} "Acuerdo de licencia"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_SPANISH} "Por favor revise los términos de la licencia antes de instalar $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_SPANISH} "Si acepta los términos del acuerdo, marque abajo la casilla. Debe aceptar los términos para instalar $(^Name). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_SPANISH} "Presione Avanzar Página para ver el resto del acuerdo."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_SPANISH} "Instalando"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_SPANISH} "Por favor espere mientras $(^Name) se instala."
LangString MUI_TEXT_FINISH_TITLE ${LANG_SPANISH} "Instalación Completada"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_SPANISH} "La instalación se ha completado correctamente."
LangString MUI_TEXT_ABORT_TITLE ${LANG_SPANISH} "Instalación Anulada"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_SPANISH} "La instalación no se completó correctamente."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_SPANISH} "Desinstalando"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_SPANISH} "Espera mientras se desinstala $(^Name)."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_SPANISH} "La desinstalación se ha completado con éxito."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_SPANISH} "La desinstalación se ha completado."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_SPANISH} "La desinstalación ha sido abortada."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_SPANISH} "La instalación no ha sido completada con éxito. Haz clic en 'Cerrar' para finalizar."


!insertmacro LANGFILE "Russian" "Russian"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_RUSSIAN} "Лицензионное соглашение"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_RUSSIAN} "Перед установкой $(^Name) ознакомьтесь с лицензионным соглашением."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_RUSSIAN} "Если вы принимаете условия соглашения, установите флажок ниже. $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_RUSSIAN} "Нажмите Page Down, чтобы просмотреть оставшуюся часть соглашения."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_RUSSIAN} "Копирование файлов"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_RUSSIAN} "Выполняется установка $(^Name). Пожалуйста, подождите."
LangString MUI_TEXT_FINISH_TITLE ${LANG_RUSSIAN} "Установка завершена"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_RUSSIAN} "Установка завершена. Для завершения щелкните по кнопке Закрыть."
LangString MUI_TEXT_ABORT_TITLE ${LANG_RUSSIAN} "Установка прервана"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_RUSSIAN} "Установка не была произведена в полной мере. Для завершения щелкните по кнопке Закрыть."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_RUSSIAN} "Удаление"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_RUSSIAN} "Подождите, пока производится удаление $(^Name)."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_RUSSIAN} "Удаление было завершено успешно. Нажмите кнопку Закрыть, чтобы закончить."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_RUSSIAN} "Удаление завершено."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_RUSSIAN} "Удаление было прервано."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_RUSSIAN} "Установка не была завершена успешно. Нажмите кнопку 'Закрыть', чтобы закончить."


!insertmacro LANGFILE "Polish" "Polish"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_POLISH} "Umowa licencyjna"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_POLISH} "Przed instalacją programu $(^Name) zapoznaj się z warunkami licencji."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_POLISH} "Jeżeli akceptujesz warunki umowy, zaznacz pole wyboru poniżej, aby kontynuować. Musisz zaakceptować warunki umowy, aby zainstalować $(^NameDA). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_POLISH} "Naciśnij klawisz Page Down, aby zobaczyć dalszą część umowy."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_POLISH} "Instalacja"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_POLISH} "Proszę czekać, podczas gdy $(^Name) jest instalowany."
LangString MUI_TEXT_FINISH_TITLE ${LANG_POLISH} "Zakończono instalację"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_POLISH} "Instalacja zakończona pomyślnie."
LangString MUI_TEXT_ABORT_TITLE ${LANG_POLISH} "Instalacja przerwana"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_POLISH} "Instalacja nie została zakończona pomyślnie."

LangString  MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_POLISH} "Deinstalacja"
LangString  MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_POLISH} "Proszę czekać, $(^Name) jest odinstalowywany."
LangString  MUI_UNTEXT_FINISH_TITLE ${LANG_POLISH} "Zakończono odinstalowanie"
LangString  MUI_UNTEXT_FINISH_SUBTITLE ${LANG_POLISH} "Odinstalowanie zakończone pomyślnie."
LangString  MUI_UNTEXT_ABORT_TITLE ${LANG_POLISH} "Deinstalacja przerwana"
LangString  MUI_UNTEXT_ABORT_SUBTITLE ${LANG_POLISH} "Deinstalacja nie została zakończona pomyślnie."


!insertmacro LANGFILE "PortugueseBr" "PortugueseBr"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_PORTUGUESEBR} "Acordo da licença"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_PORTUGUESEBR} "Por favor, reveja os termos da licença antes de instalar $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_PORTUGUESEBR} "Se você aceita os termos do acordo, clique na caixa de seleção abaixo. Você deve aceitar o acordo para instalar a $(^NameDA). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_PORTUGUESEBR} "Pressione Page Down para ver o resto do acordo."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_PORTUGUESEBR} "Instalando"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_PORTUGUESEBR} "Por favor, aguarde enquanto $(^Name) está sendo instalado."
LangString MUI_TEXT_FINISH_TITLE ${LANG_PORTUGUESEBR} "Instalação Completa"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_PORTUGUESEBR} "O instalador completou com sucesso."
LangString MUI_TEXT_ABORT_TITLE ${LANG_PORTUGUESEBR} "Instalação Abortada"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_PORTUGUESEBR} "O instalador não completou com sucesso."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_PORTUGUESEBR} "Desinstalando"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_PORTUGUESEBR} "Por favor, espere enquanto a $(^Name) está sendo desinstalada."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_PORTUGUESEBR} "Desinstalação Completa"
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_PORTUGUESEBR} "A desinstalação foi completada com sucesso."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_PORTUGUESEBR} "Desinstalação Abortada"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_PORTUGUESEBR} "A desinstalação não foi completada com sucesso."


!insertmacro LANGFILE "Korean" "Korean"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_KOREAN} "사용권 계약"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_KOREAN} "$(^Name)을(를) 설치하기 전에 사용 약관을 검토하십시오."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_KOREAN} "사용권 계약 내용에 동의하는 경우, 아래의 확인 상자를 클릭하십시오. $(^Name)을(를) 설치하려면 반드시 계약에 동의해야 합니다. $_CLICK"

LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_KOREAN} "계약의 나머지 부분을 보려면 Page Down 키를 누르십시오."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_KOREAN} "설치 중"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_KOREAN} "$(^Name) 설치를 완료할 때까지 잠시만 기다리십시오."
LangString MUI_TEXT_FINISH_TITLE ${LANG_KOREAN} "설치 완료"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_KOREAN} "설치를 완료했습니다. 작업을 마치려면 닫기를 클릭하십시오."
LangString MUI_TEXT_ABORT_TITLE ${LANG_KOREAN} "설치 제거가 중단되었습니다."
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_KOREAN} "설치를 완료하지 못했습니다. 작업을 마치려면 닫기를 클릭하십시오."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_KOREAN} "설치 제거 중"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_KOREAN} "$(^Name) 설치를 제거할 때까지 잠시만 기다리십시오."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_KOREAN} "설치 제거를 완료했습니다. 작업을 마치려면 닫기를 클릭하십시오."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_KOREAN} "설치 제거 완료"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_KOREAN} "설치 제거 중단"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_KOREAN} "설치를 완료하지 못했습니다. 작업을 마치려면 닫기를 클릭하십시오."


!insertmacro LANGFILE "Japanese" "Japanese"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_JAPANESE} "ライセンス契約書"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_JAPANESE} "$(^NameDA)をインストールする前に、ライセンス条件を確認してください。"
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_JAPANESE} "契約書のすべての条件に同意するならば、下のチェックボックスをクリックしてください。$(^NameDA) をインストールするには、契約書に同意する必要があります。 $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_JAPANESE} "[Page Down]を押して契約書をすべてお読みください。" 
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_JAPANESE} "インストール"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_JAPANESE} "$(^NameDA)をインストールしています。しばらくお待ちください。"
LangString MUI_TEXT_FINISH_TITLE ${LANG_JAPANESE} "インストールの完了"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_JAPANESE} "インストールに成功しました。"
LangString MUI_TEXT_ABORT_TITLE ${LANG_JAPANESE} "インストールの中止"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_JAPANESE} "セットアップは正常に完了されませんでした。"

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_JAPANESE} "アンインストール"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_JAPANESE} "$(^NameDA)をアンインストールしています。しばらくお待ちください。"
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_JAPANESE} "アンインストールの完了"
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_JAPANESE} "アンインストールに成功しました。"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_JAPANESE} "アンインストールの中止"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_JAPANESE} "アンインストールは正常に完了されませんでした。"


!insertmacro LANGFILE "SpanishMX" "SpanishMX"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_SPANISHMX} "Acuerdo de licencia"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_SPANISHMX} "Por favor revisa el acuerdo de licencia antes de instalar $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_SPANISHMX} "Si aceptas los términos de del acuerdo. Haz click en la caja de abajo. Tienes que aceptar el acuerdo para instalar $(^Name). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_SPANISHMX} "Presiona avanzar página para ver el resto del acuerdo."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_SPANISHMX} "Instalando"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_SPANISHMX} "Por favor espera mientras se instala $(^Name)."
LangString MUI_TEXT_FINISH_TITLE ${LANG_SPANISHMX} "Instalación completa"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_SPANISHMX} "La configuración fue completada satisfactoriamente. Haz click en cerrar para terminar."
LangString MUI_TEXT_ABORT_TITLE ${LANG_SPANISHMX} "La instalación fue cancelada"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_SPANISHMX} "La configuración no fue completada satisfactoriamente. Haz click en cerrar para terminar."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_SPANISHMX} "Desinstalando"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_SPANISHMX} "Por favor espera mientras $(^Name) es desinstalado."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_SPANISHMX} "La desinstalación fue completada satisfactoriamente. Haz click en cerrar para terminar."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_SPANISHMX} "Desinstalación completa"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_SPANISHMX} "Desinstalación abortada"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_SPANISHMX} "La configuración no fue completada satisfactoriamente. Haz click en cerrar para terminar."


!insertmacro LANGFILE "TradChinese" "TradChinese"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_TRADCHINESE} "授權合約"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_TRADCHINESE} "請於安裝 $(^Name) 前詳閱授權內容。"
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_TRADCHINESE} "如果您接受此合約的內容，請勾選核取方塊。您必須接受此合約才能進行安裝 $(^Name)。 $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_TRADCHINESE} "請按「Page Down」鍵來閱讀合約的其他部份。"
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_TRADCHINESE} "安裝中"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_TRADCHINESE} "$(^Name) 正在安裝中，請稍候。"
LangString MUI_TEXT_FINISH_TITLE ${LANG_TRADCHINESE} "安裝完成"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_TRADCHINESE} "安裝已完成。請按「關閉」結束。"
LangString MUI_TEXT_ABORT_TITLE ${LANG_TRADCHINESE} "解除安裝已終止"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_TRADCHINESE} "安裝尚未完成。請按「關閉」結束。"

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_TRADCHINESE} "解除安裝中"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_TRADCHINESE} "$(^Name) 正在解除安裝中，請稍候。"
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_TRADCHINESE} "解除安裝已完成。請按「關閉」結束。"
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_TRADCHINESE} "解除安裝已完成"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_TRADCHINESE} "解除安裝已終止"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_TRADCHINESE} "解除安裝尚未完成。請按「關閉」結束。"


!insertmacro LANGFILE "SimpChinese" "SimpChinese"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_SIMPCHINESE} "许可协议"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_SIMPCHINESE} "安装 $(^Name) 前请查看许可条款。"
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_SIMPCHINESE} "如果您接受协议条款，请勾选下方的复选框。您必须接受协议才能安装 $(^Name)。$_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_SIMPCHINESE} "按 Page Down 键可查看协议的其余部分。"
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_SIMPCHINESE} "正在安装"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_SIMPCHINESE} "请稍候，$(^Name) 正在安装。"
LangString MUI_TEXT_FINISH_TITLE ${LANG_SIMPCHINESE} "安装完成"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_SIMPCHINESE} "已成功完成安装。单击“关闭”以完成。"
LangString MUI_TEXT_ABORT_TITLE ${LANG_SIMPCHINESE} "卸载已中止"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_SIMPCHINESE} "未成功完成设置。单击“关闭”以完成。"

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_SIMPCHINESE} "正在卸载"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_SIMPCHINESE} "请稍候，$(^Name) 正在卸载。"
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_SIMPCHINESE} "已成功完成卸载。单击“关闭”以完成。"
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_SIMPCHINESE} "卸载完成"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_SIMPCHINESE} "卸载已中止"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_SIMPCHINESE} "未成功完成设置。单击“关闭”以完成。"

