=============================
		NSIS Basics
=============================

To build the Social Club installer, you need to have NSIS installed.

BuildRGSC.exe "[Path to MakeNSIS.exe]" "[Path to Social Club DLLs]" "[Path where to make the installer]"

After installing NSIS, MakeNSIS.exe is usually located in \Program Files (x86)\NSIS\Unicode\MakeNSIS.exe.

=============================
	Building Uninstaller
=============================

1. Build all 4 configurations of the game - win32/x64 with debug/release, which generates files into C:\Program Files\Rockstar Games\...
2. Edit 'RGSCInstaller_v1.2.nsi' and 'RGSCInstaller_v1.2_Dev.nsi' to uncomment the 'WriteUninstaller' lines
3. Run BuildRGSC.bat and BuildDebugRGSC.bat to generate release and debug installers.
4. Run the resulting installers.
5. Check the installation folder for the newly generated 'uninstallRGSC[debug].exe'
6. Copy back into perforce in this folder.
7. Comment back out 'WriteUninstaller' lines in the NSIS scripts.
8. Rebuild the installers.
