# Rockstar Games Social Club Installer - Delevoper: Colin Orr
# Sep 20, 2011 3:35:50 PM

SetCompressor /SOLID lzma
Name "Rockstar Games Social Club"
									
!define GAME "Social Club"
!define COMPANY	"Rockstar Games"            
!define REGKEY	"SOFTWARE\${COMPANY}\$(^Name)"
!define URL		"http://socialclub.rockstargames.com/"

# MUI Symbol Definitions
!define MUI_ICON Global\IMG\2102094_SC_LOGO.ico
!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_UNICON Global\IMG\2102094_SC_LOGO.ico
!define MUI_UNFINISHPAGE_NOAUTOCLOSE
!define MUI_LANGDLL_REGISTRY_ROOT HKLM
!define MUI_LANGDLL_REGISTRY_KEY "SOFTWARE\Rockstar Games\Rockstar Games Social Club"  ;${REGKEY}
!define MUI_LANGDLL_REGISTRY_VALUENAME "InstallLang"
!define MUI_LICENSEPAGE_CHECKBOX 

!define INSTALLERx64 "$PROGRAMFILES64\Rockstar Games\Social Club"
!define INSTALLERx86 "$PROGRAMFILES\Rockstar Games\Social Club"

# Included files
!include "Global\Includes\CompileInfo.nsh"
!include "Sections.nsh"
!include "MUI2.nsh"
!include "Global\Includes\FileFunc.nsh"
!include "Global\Includes\Lang.nsh"
!include "Global\Includes\Global.nsh"
!include "Global\Includes\FileListx86.nsh"
!include "Global\Includes\FileListx64.nsh"
!include "Global\Includes\DeleteFileListx86.nsh"
!include "Global\Includes\DeleteFileListx64.nsh"
!include "x64.nsh"

# Reserved Files
!insertmacro MUI_RESERVEFILE_LANGDLL

# Variables
Var dWelcome
Var HControl
var GLOBAL_LANGID

# Installer pages
Page custom CUST_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE $(MUILicense)
!insertmacro MUI_PAGE_INSTFILES

;!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

# Installer attributes
OutFile "${FILENAME}"
InstallDir "$PROGRAMFILES\Rockstar Games\Social Club"
CRCCheck on
ShowInstDetails hide
VIProductVersion ${VERSION}
VIAddVersionKey /LANG=${LANG_ENGLISH} ProductName "Rockstar Games Social Club"
VIAddVersionKey /LANG=${LANG_ENGLISH} ProductVersion "${VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} CompanyName "${COMPANY}"
VIAddVersionKey /LANG=${LANG_ENGLISH} FileVersion "${VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} FileDescription "Rockstar Games Social Club"
VIAddVersionKey /LANG=${LANG_ENGLISH} LegalCopyright ""
InstallDirRegKey HKLM "${REGKEY}" InstallFolder

;ShowUninstDetails show



!macro IsUserAdmin RESULT
    !define Index "Line${__LINE__}"
    StrCpy ${RESULT} 0
    System::Call '*(&i1 0,&i4 0,&i1 5)i.r0'
    System::Call 'advapi32::AllocateAndInitializeSid(i r0,i 2,i 32,i 544,i 0,i 0,i 0,i 0,i 0,i 0,*i .R0)i.r5'
    System::Free $0
    System::Call 'advapi32::CheckTokenMembership(i n,i R0,*i .R1)i.r5'
    StrCmp $5 0 ${Index}_Error
    StrCpy ${RESULT} $R1
    Goto ${Index}_End
    ${Index}_Error:
    StrCpy ${RESULT} -1
    ${Index}_End:
    System::Call 'advapi32::FreeSid(i R0)i.r5'
    !undef Index
!macroend

; check if the MTL is running, if so close instller. 
!macro CheckIfTheMTLIsRunning STRING
	FindWindow $0 "Rockstar Games Launcher"
	StrCmp $0 0 notRunning
		IfSilent +2
		MessageBox MB_OK "${STRING}"
		Abort
	notRunning:
!macroend

Function CUST_PAGE_WELCOME                                                                                      ; Welcome Page
    
	strcpy $LANGUAGE $GLOBAL_LANGID 

	Push $LANGUAGE						; Lang.nsh: Language selection window
	Call SetLangText   

	!insertmacro MUI_HEADER_TEXT $WelcomeHead1 $WelcomeHead2

    Strcpy $2 "Welcome"
    
    nsDialogs::Create /NOUNLOAD 1018
    pop $dWelcome
    
    ${If} $dWelcome == error
        Abort
    ${EndIf}

    ;Push $LANGUAGE
    ;Call SetLangSpaceMsg                                                                                    ;Lang.nsh: Set Text Call
     
    GetDlgItem $0 $HWNDPARENT 1                                                                             ; Gets current window
    nsDialogs::CreateControl /NOUNLOAD STATIC ${WS_VISIBLE}|${WS_CHILD} 0 0 0 85% 100 $WelcomeMsg2         ;|${WS_CLIPSIBLINGS|${SS_CENTER} 145
    pop $HControl
    SetCtlColors $HControl 0x000000  ;transparent
    
	;GetDlgItem $0 $HWNDPARENT 2                                                                             ; Gets current window
    ;nsDialogs::CreateControl /NOUNLOAD STATIC ${WS_VISIBLE}|${WS_CHILD} 0 0 45 85% 60 $(^ClickNext)         ;|${WS_CLIPSIBLINGS|${SS_CENTER} 145
    ;pop $HControl
    ;SetCtlColors $HControl 0x000000  ;transparent
	
    nsDialogs::Show
FunctionEnd

# Installer sections
Section -Main SEC0000
	
	;MessageBox MB_OK "${INSTALLERx64}" 
	;MessageBox MB_OK "${INSTALLERx86}" 

	${If} ${RunningX64}					;Kills what was in the previous build;   Needs to be updated
		push "${INSTALLERx64}"
		Call RemoveOldData 
		
		push "${INSTALLERx86}"
		Call RemoveOldData 
	${Else}
		push "${INSTALLERx86}"
		Call RemoveOldData 
	${EndIf}


	${If} ${RunningX64}					; Installs both 64 and 32 files
		push "${INSTALLERx64}"
		Call AddUpdateFilesx64 
		push "${INSTALLERx86}"			; Installs 32 files
		Call AddUpdateFilesx86 
	${Else}
		push "${INSTALLERx86}"
		Call AddUpdateFilesx86 
	${EndIf} 
		
    ClearErrors
	
	;To write out the uninstall file, uncomment the WriteUninstaller lines and comment out the FILE lines
	;After the installer, finishes - run the installers.
	;Copy the 'uninstallRGSCRedistributable.exe' back into this build folder.

	${If} ${RunningX64}
		SetOutPath "${INSTALLERx64}"
		FILE uninstallRGSCRedistributable.exe
		;WriteUninstaller "${INSTALLERx64}\uninstallRGSCRedistributable.exe"
	${Else}
		SetOutPath "${INSTALLERx86}"
		FILE uninstallRGSCRedistributable.exe
		;WriteUninstaller "${INSTALLERx86}\uninstallRGSCRedistributable.exe"
	${EndIf}

    goto InstallEnd

    IfSilent +2
	MessageBox MB_OK $RunError
    
	abort
InstallEnd:          

SectionEnd

Section -post SEC0001

	${If} ${RunningX64}					
		strcpy $R0 "${INSTALLERx64}"
	${Else}
		strcpy $R0 "${INSTALLERx86}"
	${EndIf}
	
	WriteRegStr HKLM "${REGKEY}" InstallFolder $R0
	WriteRegStr HKLM "${REGKEY}" Version "${VERSION}"  
	WriteRegStr HKLM "${REGKEY}" InstallLang $LANGUAGE 

	;WriteUninstaller 

    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" DisplayName "$(^Name)"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" DisplayVersion "${VERSION}"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" Publisher "${COMPANY}"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" DisplayIcon $R0\uninstallRGSCRedistributable.exe
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" UninstallString $R0\uninstallRGSCRedistributable.exe
    WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" NoModify 1
    WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" NoRepair 1
SectionEnd

# Installer functions
Function .onInit
    BringToFront
	
    ; Check if already running
    ; If so don't open another but bring to front
    System::Call "kernel32::CreateMutexA(i 0, i 0, t '$(^Name)') i .r0 ?e"
    Pop $0
    StrCmp $0 0 launch
    StrLen $0 "$(^Name)"
    IntOp $0 $0 + 1
  
  loop:
	
    FindWindow $1 '#32770' '' 0 $1
    IntCmp $1 0 +5
    System::Call "user32::GetWindowText(i r1, t .r2, i r0) i."
    StrCmp $2 "$(^Name)" 0 loop
    System::Call "user32::ShowWindow(i r1,i 9) i."         ; If minimized then maximize
    System::Call "user32::SetForegroundWindow(i r1) i."    ; Bring to front
    Abort
  launch:

	strcpy $IsLanguageArgsSet 0
	Call CheckForLanguageArgs
	
	;Comment out the IF statment to test language drop down.
	${If} $IsLanguageArgsSet = 0
		Call SelectLanguage                                                 ; Lang.nsh: Sets Finds the OS Language 
	${EndIf}

	Push $LANGUAGE
	Call SetLangText                                                        ; Lang.nsh: Language selection window
	
	strcpy $GLOBAL_LANGID $LANGUAGE
	Call IsSilent
	
	IfSilent +1
	!insertmacro CheckIfTheMTLIsRunning "$MTLRunning"						; Check if the MTL is running, if so close it
	
	
	Call CheckForProcArgs													; Checks to see if /waitforprocess is passed in.
	;pop $0
	
	;${If} $0 = 0
	;	Call FindProcRunning
    ;${EndIf}

    push "${VERSION}"
    Call CheckRGSCVersion                                               

    SetOutPath "$TEMP\ROCKSTAR"                                             ; Used to set the Skin for the Installer
    InitPluginsDir                                                          ; Sets up the Plugins DIR usually System Temp DIR

    !insertmacro IsUserAdmin $0                                             ; Admin user check

    ${If} $0 < 1
        ;IfSilent +2
		MessageBox MB_ICONSTOP|MB_OK $AdminErrorRGSC
		SetErrorLevel 944
		Abort
   ${EndIf}
    
FunctionEnd

# Uninstaller functions
Function un.onInit
    ReadRegStr $INSTDIR HKLM "${REGKEY}" InstallFolder
    ReadRegStr $LANGUAGE HKLM "SOFTWARE\Rockstar Games\Rockstar Games Social Club" "InstallLang"
	
	DetailPrint "Set Lang Text.." 
	Push $LANGUAGE
	Call un.SetLangText
	
	DetailPrint "Scan for proc.."
    Call un.FindProc
    pop $0
    strcmp $0 "0"  CheckInstalledApps
    abort
 
 CheckInstalledApps:   
    
	DetailPrint "Scan for App.."	
    Call un.IsApplicationInstalled
    pop $0
    strcmp $0 "0" AskToUninstall 
    abort
    
AskToUninstall: 
	
	;ConfirmTitle

	MessageBox MB_YESNO|MB_ICONQUESTION "$ConfirmMsg" IDYES ContUninstallCheck
	abort
ContUninstallCheck:

    ;!insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuGroup
    
    ;Don't think these are required.
    ;!insertmacro MUI_UNGETLANGUAGE
    ;!insertmacro SELECT_UNSECTION Main ${UNSEC0000}
    
FunctionEnd

# Macro for selecting uninstaller sections
!macro SELECT_UNSECTION SECTION_NAME UNSECTION_ID
    Push $R0
    ReadRegStr $R0 HKLM "${REGKEY}\Components" "${SECTION_NAME}"
    StrCmp $R0 1 0 next${UNSECTION_ID}
    !insertmacro SelectSection "${UNSECTION_ID}"
    GoTo done${UNSECTION_ID}
next${UNSECTION_ID}:
    !insertmacro UnselectSection "${UNSECTION_ID}"
done${UNSECTION_ID}:
    Pop $R0
!macroend

# Uninstaller sections

Section /o -un.Main UNSEC0000
	DeleteRegValue HKLM "${REGKEY}\Components" Main
SectionEnd

Section -un.post UNSEC0001

	DetailPrint "$sDeletefiles"

	${If} ${RunningX64}					; Uninstalls both 64 and 32 files
		push "${INSTALLERx64}"
		CALL un.AddDeleteFilesx64 
		push "${INSTALLERx86}"				; Uninstalls 32 files
		CALL un.AddDeleteFilesx86 
	${Else}
		push "${INSTALLERx86}"				; Uninstalls 32 files
		CALL un.AddDeleteFilesx86
	${EndIf} 

UnInstallCont:
    
	DetailPrint "$sDeleteReg"
	DeleteRegKey HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)"
	DetailPrint "$sRemoveDir"
	
	${If} ${RunningX64}							; Uninstalls both 64 and 32 root dir

		IfFileExists "${INSTALLERx64}\uninstallRGSCRedistributable.exe" 0 +2
			Delete /REBOOTOK "${INSTALLERx64}\uninstallRGSCRedistributable.exe"

		RMDir /r  "${INSTALLERx64}"				; Uninstalls 64 root dir
		RMDir /r  "${INSTALLERx86}"				; Uninstalls 32 root dir
	${Else}
		
		IfFileExists "${INSTALLERx86}\uninstallRGSCRedistributable.exe" 0 +2
			Delete /REBOOTOK "${INSTALLERx86}\uninstallRGSCRedistributable.exe"

		RMDir /r  "${INSTALLERx86}"				; Uninstalls 32 root dir
	${EndIf}

	DetailPrint "$sDeleteReg"
	DeleteRegValue HKLM "${REGKEY}" InstallFolder
    DeleteRegKey /IfEmpty HKLM "${REGKEY}\Components"
    DeleteRegKey /IfEmpty HKLM "${REGKEY}"
    DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Rockstar Games\Rockstar Games Social Club"
    DeleteRegKey /IfEmpty HKEY_LOCAL_MACHINE "SOFTWARE\Rockstar Games"

	IfRebootFlag 0 noreboot
		MessageBox MB_YESNO "$NeedReboot" IDNO noreboot
		Reboot
noreboot:
	
SectionEnd

# Installer Language Strings
# TODO Update the Language Strings with the appropriate translations.

LangString ^UninstallLink ${LANG_ENGLISH} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_GERMAN} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_SPANISH} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_FRENCH} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_ITALIAN} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_RUSSIAN} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_JAPANESE} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_KOREAN} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_PORTUGUESEBR} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_POLISH} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_SPANISHMX} "Uninstall $(^Name)"
LangString ^UninstallLink ${LANG_TRADCHINESE} "Uninstall $(^Name)"

