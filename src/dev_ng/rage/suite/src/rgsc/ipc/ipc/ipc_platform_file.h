// Copyright (c) 2009 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef IPC_IPC_PLATFORM_FILE_H_
#define IPC_IPC_PLATFORM_FILE_H_
#pragma once

#include "base/basictypes.h"

#include "base/platform_file.h"

#if defined(OS_POSIX)
#include "base/file_descriptor_posix.h"
#endif

namespace IPC {

#if defined(OS_WIN)
typedef ipcbase::PlatformFile PlatformFileForTransit;
#elif defined(OS_POSIX)
typedef ipcbase::FileDescriptor PlatformFileForTransit;
#endif

inline PlatformFileForTransit InvalidPlatformFileForTransit() {
#if defined(OS_WIN)
  return ipcbase::kInvalidPlatformFileValue;
#elif defined(OS_POSIX)
  return ipcbase::FileDescriptor();
#endif
}

inline ipcbase::PlatformFile PlatformFileForTransitToPlatformFile(
    const PlatformFileForTransit& transit) {
#if defined(OS_WIN)
  return transit;
#elif defined(OS_POSIX)
  return transit.fd;
#endif
}

}  // namespace IPC

#endif  // IPC_IPC_PLATFORM_FILE_H_
