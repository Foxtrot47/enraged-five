// Copyright (c) 2006-2008 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef BASE_ATOMIC_SEQUENCE_NUM_H_
#define BASE_ATOMIC_SEQUENCE_NUM_H_
#pragma once

#include "base/atomicops.h"
#include "base/basictypes.h"

namespace ipcbase {

class AtomicSequenceNumber {
 public:
  AtomicSequenceNumber() : seq_(0) { }
  explicit AtomicSequenceNumber(ipcbase::LinkerInitialized x) { /* seq_ is 0 */ }

  int GetNext() {
    return static_cast<int>(
        ipcbase::subtle::NoBarrier_AtomicIncrement(&seq_, 1) - 1);
  }

 private:
  ipcbase::subtle::Atomic32 seq_;
  DISALLOW_COPY_AND_ASSIGN(AtomicSequenceNumber);
};

}  // namespace ipcbase

#endif  // BASE_ATOMIC_SEQUENCE_NUM_H_
