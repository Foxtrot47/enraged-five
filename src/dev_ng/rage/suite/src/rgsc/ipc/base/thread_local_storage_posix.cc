// Copyright (c) 2006-2008 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "base/thread_local_storage.h"

#include "base/logging.h"

ThreadLocalStorage::Slot::Slot(TLSDestructorFunc destructor)
    : initialized_(false) {
  Initialize(destructor);
}

bool ThreadLocalStorage::Slot::Initialize(TLSDestructorFunc destructor) {
  IPC_DCHECK(!initialized_);
  int error = pthread_key_create(&key_, destructor);
  if (error) {
    IPC_NOTREACHED();
    return false;
  }

  initialized_ = true;
  return true;
}

void ThreadLocalStorage::Slot::Free() {
  IPC_DCHECK(initialized_);
  int error = pthread_key_delete(key_);
  if (error)
    IPC_NOTREACHED();
  initialized_ = false;
}

void* ThreadLocalStorage::Slot::Get() const {
  IPC_DCHECK(initialized_);
  return pthread_getspecific(key_);
}

void ThreadLocalStorage::Slot::Set(void* value) {
  IPC_DCHECK(initialized_);
  int error = pthread_setspecific(key_, value);
  if (error)
    IPC_NOTREACHED();
}
