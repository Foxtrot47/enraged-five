// Copyright (c) 2006-2008 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef IPC_BASE_LOGGING_H_
#define IPC_BASE_LOGGING_H_
#pragma once

#include <string>
#include <cstring>
#include <sstream>

#include "base/basictypes.h"

//
// Optional message capabilities
// -----------------------------
// Assertion failed messages and fatal errors are displayed in a dialog box
// before the application exits. However, running this UI creates a message
// loop, which causes application messages to be processed and potentially
// dispatched to existing application windows. Since the application is in a
// bad state when this assertion dialog is displayed, these messages may not
// get processed and hang the dialog, or the application might go crazy.
//
// Therefore, it can be beneficial to display the error dialog in a separate
// process from the main application. When the logging system needs to display
// a fatal error dialog box, it will look for a program called
// "DebugMessage.exe" in the same directory as the application executable. It
// will run this application with the message as the command line, and will
// not include the name of the application as is traditional for easier
// parsing.
//
// The code for DebugMessage.exe is only one line. In WinMain, do:
//   MessageBox(NULL, GetCommandLineW(), L"Fatal Error", 0);
//
// If DebugMessage.exe is not found, the logging code will use a normal
// MessageBox, potentially causing the problems discussed above.


// Instructions
// ------------
//
// Make a bunch of macros for logging.  The way to log things is to stream
// things to IPC_LOG(<a particular severity level>).  E.g.,
//
//   IPC_LOG(INFO) << "Found " << num_cookies << " cookies";
//
// You can also do conditional logging:
//
//   IPC_LOG_IF(INFO, num_cookies > 10) << "Got lots of cookies";
//
// The above will cause log messages to be output on the 1st, 11th, 21st, ...
// times it is executed.  Note that the special COUNTER value is used to
// identify which repetition is happening.
//
// The IPC_CHECK(condition) macro is active in both debug and release builds and
// effectively performs a IPC_LOG(FATAL) which terminates the process and
// generates a crashdump unless a debugger is attached.
//
// There are also "debug mode" logging macros like the ones above:
//
//   IPC_DLOG(INFO) << "Found cookies";
//
//   IPC_DLOG_IF(INFO, num_cookies > 10) << "Got lots of cookies";
//
// All "debug mode" logging is compiled away to nothing for non-debug mode
// compiles.  IPC_LOG_IF and development flags also work well together
// because the code can be compiled away sometimes.
//
// We also have
//
//   IPC_LOG_ASSERT(assertion);
//   IPC_DLOG_ASSERT(assertion);
//
// which is syntactic sugar for {,D}IPC_LOG_IF(FATAL, assert fails) << assertion;
//
// There are "verbose level" logging macros.  They look like
//
//   IPC_VLOG(1) << "I'm printed when you run the program with --v=1 or more";
//   IPC_VLOG(2) << "I'm printed when you run the program with --v=2 or more";
//
// These always log at the INFO log level (when they log at all).
// The verbose logging can also be turned on module-by-module.  For instance,
//    --vmodule=profile=2,icon_loader=1,browser_*=3 --v=0
// will cause:
//   a. IPC_VLOG(2) and lower messages to be printed from profile.{h,cc}
//   b. IPC_VLOG(1) and lower messages to be printed from icon_loader.{h,cc}
//   c. IPC_VLOG(3) and lower messages to be printed from files prefixed with
//      "browser"
//   d. IPC_VLOG(0) and lower messages to be printed from elsewhere
//
// The wildcarding functionality shown by (c) supports both '*' (match
// 0 or more characters) and '?' (match any single character) wildcards.
//
// There's also IPC_VIPC_LOG_IS_ON(n) "verbose level" condition macro. To be used as
//
//   if (IPC_VIPC_LOG_IS_ON(2)) {
//     // do some logging preparation and logging
//     // that can't be accomplished with just IPC_VLOG(2) << ...;
//   }
//
// There is also a IPC_VLOG_IF "verbose level" condition macro for sample
// cases, when some extra computation and preparation for logs is not
// needed.
//
//   IPC_VLOG_IF(1, (size > 1024))
//      << "I'm printed when size is more than 1024 and when you run the "
//         "program with --v=1 or more";
//
// We also override the standard 'assert' to use 'IPC_DLOG_ASSERT'.
//
// Lastly, there is:
//
//   IPC_PLOG(ERROR) << "Couldn't do foo";
//   IPC_DPLOG(ERROR) << "Couldn't do foo";
//   IPC_PLOG_IF(ERROR, cond) << "Couldn't do foo";
//   IPC_DPLOG_IF(ERROR, cond) << "Couldn't do foo";
//   IPC_PCHECK(condition) << "Couldn't do foo";
//   IPC_DPCHECK(condition) << "Couldn't do foo";
//
// which append the last system error to the message in string form (taken from
// GetLastError() on Windows and errno on POSIX).
//
// The supported severity levels for macros that allow you to specify one
// are (in increasing order of severity) INFO, WARNING, ERROR, ERROR_REPORT,
// and FATAL.
//
// Very important: logging a message at the FATAL severity level causes
// the program to terminate (after the message is logged).
//
// Note the special severity of ERROR_REPORT only available/relevant in normal
// mode, which displays error dialog without terminating the program. There is
// no error dialog for severity ERROR or below in normal mode.
//
// There is also the special severity of DFATAL, which logs FATAL in
// debug mode, ERROR in normal mode.

namespace logging {

// Where to record logging output? A flat file and/or system debug log via
// OutputDebugString. Defaults on Windows to LOG_ONLY_TO_FILE, and on
// POSIX to LOG_ONLY_TO_SYSTEM_DEBUG_LOG (aka stderr).
enum LoggingDestination { LOG_NONE,
                          LOG_ONLY_TO_FILE,
                          LOG_ONLY_TO_SYSTEM_DEBUG_LOG,
                          LOG_TO_BOTH_FILE_AND_SYSTEM_DEBUG_LOG };

// Indicates that the log file should be locked when being written to.
// Often, there is no locking, which is fine for a single threaded program.
// If logging is being done from multiple threads or there can be more than
// one process doing the logging, the file should be locked during writes to
// make each log outut atomic. Other writers will block.
//
// All processes writing to the log file must have their locking set for it to
// work properly. Defaults to DONT_LOCK_LOG_FILE.
enum LogLockingState { LOCK_LOG_FILE, DONT_LOCK_LOG_FILE };

// On startup, should we delete or append to an existing log file (if any)?
// Defaults to APPEND_TO_OLD_LOG_FILE.
enum OldFileDeletionState { DELETE_OLD_LOG_FILE, APPEND_TO_OLD_LOG_FILE };

// TODO(avi): do we want to do a unification of character types here?
#if defined(OS_WIN)
typedef wchar_t PathChar;
#else
typedef char PathChar;
#endif

// Define different names for the BaseInitLoggingImpl() function depending on
// whether NDEBUG is defined or not so that we'll fail to link if someone tries
// to compile logging.cc with NDEBUG but includes logging.h without defining it,
// or vice versa.
#ifdef NDEBUG
#define BaseInitLoggingImpl BaseInitLoggingImpl_built_with_NDEBUG
#else
#define BaseInitLoggingImpl BaseInitLoggingImpl_built_without_NDEBUG
#endif

// Implementation of the InitLogging() method declared below.  We use a
// more-specific name so we can #define it above without affecting other code
// that has named stuff "InitLogging".
void BaseInitLoggingImpl(const PathChar* log_file,
                         LoggingDestination logging_dest,
                         LogLockingState lock_log,
                         OldFileDeletionState delete_old);

// Sets the log file name and other global logging state. Calling this function
// is recommended, and is normally done at the beginning of application init.
// If you don't call it, all the flags will be initialized to their default
// values, and there is a race condition that may leak a critical section
// object if two threads try to do the first log at the same time.
// See the definition of the enums above for descriptions and default values.
//
// The default log file is initialized to "debug.log" in the application
// directory. You probably don't want this, especially since the program
// directory may not be writable on an enduser's system.
inline void InitLogging(const PathChar* log_file,
                        LoggingDestination logging_dest,
                        LogLockingState lock_log,
                        OldFileDeletionState delete_old) {
  BaseInitLoggingImpl(log_file, logging_dest, lock_log, delete_old);
}

// Sets the log level. Anything at or above this level will be written to the
// log file/displayed to the user (if applicable). Anything below this level
// will be silently ignored. The log level defaults to 0 (everything is logged)
// if this function is not called.
void SetMinLogLevel(int level);

// Gets the current log level.
int GetMinLogLevel();

// Gets the current vlog level for the given file (usually taken from
// __FILE__).

// Note that |N| is the size *with* the null terminator.
int GetVlogLevelHelper(const char* file_start, size_t N);

template <size_t N>
int GetVlogLevel(const char (&file)[N]) {
  return GetVlogLevelHelper(file, N);
}

// Sets the common items you want to be prepended to each log message.
// process and thread IDs default to off, the timestamp defaults to on.
// If this function is not called, logging defaults to writing the timestamp
// only.
void SetLogItems(bool enable_process_id, bool enable_thread_id,
                 bool enable_timestamp, bool enable_tickcount);

// Sets whether or not you'd like to see fatal debug messages popped up in
// a dialog box or not.
// Dialogs are not shown by default.
void SetShowErrorDialogs(bool enable_dialogs);

// Sets the Log Assert Handler that will be used to notify of check failures.
// The default handler shows a dialog box and then terminate the process,
// however clients can use this function to override with their own handling
// (e.g. a silent one for Unit Tests)
typedef void (*LogAssertHandlerFunction)(const std::string& str);
void SetLogAssertHandler(LogAssertHandlerFunction handler);
// Sets the Log Report Handler that will be used to notify of check failures
// in non-debug mode. The default handler shows a dialog box and continues
// the execution, however clients can use this function to override with their
// own handling.
typedef void (*LogReportHandlerFunction)(const std::string& str);
void SetLogReportHandler(LogReportHandlerFunction handler);

// Sets the Log Message Handler that gets passed every log message before
// it's sent to other log destinations (if any).
// Returns true to signal that it handled the message and the message
// should not be sent to other log destinations.
typedef bool (*LogMessageHandlerFunction)(int severity, const std::string& str);
void SetLogMessageHandler(LogMessageHandlerFunction handler);

typedef int LogSeverity;
const LogSeverity LOG_INFO = 0;
const LogSeverity LOG_WARNING = 1;
const LogSeverity LOG_ERROR = 2;
const LogSeverity LOG_ERROR_REPORT = 3;
const LogSeverity LOG_FATAL = 4;
const LogSeverity LOG_NUM_SEVERITIES = 5;

// LOG_DFATAL is LOG_FATAL in debug mode, ERROR in normal mode
#ifdef NDEBUG
const LogSeverity LOG_DFATAL = LOG_ERROR;
#else
const LogSeverity LOG_DFATAL = LOG_FATAL;
#endif

// A few definitions of macros that don't generate much code. These are used
// by IPC_LOG() and IPC_LOG_IF, etc. Since these are used all over our code, it's
// better to have compact code for these operations.
#define IPC_COMPACT_GOOGLE_LOG_EX_INFO(ClassName, ...) \
  logging::ClassName(__FILE__, __LINE__, logging::LOG_INFO , ##__VA_ARGS__)
#define IPC_COMPACT_GOOGLE_LOG_EX_WARNING(ClassName, ...) \
  logging::ClassName(__FILE__, __LINE__, logging::LOG_WARNING , ##__VA_ARGS__)
#define IPC_COMPACT_GOOGLE_LOG_EX_ERROR(ClassName, ...) \
  logging::ClassName(__FILE__, __LINE__, logging::LOG_ERROR , ##__VA_ARGS__)
#define IPC_COMPACT_GOOGLE_LOG_EX_ERROR_REPORT(ClassName, ...) \
  logging::ClassName(__FILE__, __LINE__, \
                     logging::LOG_ERROR_REPORT , ##__VA_ARGS__)
#define IPC_COMPACT_GOOGLE_LOG_EX_FATAL(ClassName, ...) \
  logging::ClassName(__FILE__, __LINE__, logging::LOG_FATAL , ##__VA_ARGS__)
#define IPC_COMPACT_GOOGLE_LOG_EX_DFATAL(ClassName, ...) \
  logging::ClassName(__FILE__, __LINE__, logging::LOG_DFATAL , ##__VA_ARGS__)

#define IPC_COMPACT_GOOGLE_LOG_INFO \
  IPC_COMPACT_GOOGLE_LOG_EX_INFO(LogMessage)
#define IPC_COMPACT_GOOGLE_LOG_WARNING \
  IPC_COMPACT_GOOGLE_LOG_EX_WARNING(LogMessage)
#define IPC_COMPACT_GOOGLE_LOG_ERROR \
  IPC_COMPACT_GOOGLE_LOG_EX_ERROR(LogMessage)
#define IPC_COMPACT_GOOGLE_LOG_ERROR_REPORT \
  IPC_COMPACT_GOOGLE_LOG_EX_ERROR_REPORT(LogMessage)
#define IPC_COMPACT_GOOGLE_LOG_FATAL \
  IPC_COMPACT_GOOGLE_LOG_EX_FATAL(LogMessage)
#define IPC_COMPACT_GOOGLE_LOG_DFATAL \
  IPC_COMPACT_GOOGLE_LOG_EX_DFATAL(LogMessage)

// wingdi.h defines ERROR to be 0. When we call IPC_LOG(ERROR), it gets
// substituted with 0, and it expands to IPC_COMPACT_GOOGLE_LOG_0. To allow us
// to keep using this syntax, we define this macro to do the same thing
// as IPC_COMPACT_GOOGLE_LOG_ERROR, and also define ERROR the same way that
// the Windows SDK does for consistency.
#define ERROR 0
#define IPC_COMPACT_GOOGLE_LOG_EX_0(ClassName, ...) \
  IPC_COMPACT_GOOGLE_LOG_EX_ERROR(ClassName , ##__VA_ARGS__)
#define IPC_COMPACT_GOOGLE_LOG_0 IPC_COMPACT_GOOGLE_LOG_ERROR
// Needed for IPC_LOG_IS_ON(ERROR).
const LogSeverity LOG_0 = LOG_ERROR;

#define IPC_LOG_IS_ON(severity) \
  ((::logging::LOG_ ## severity) >= ::logging::GetMinLogLevel())

// We can't do any caching tricks with IPC_VIPC_LOG_IS_ON() like the
// google-glog version since it requires GCC extensions.  This means
// that using the v-logging functions in conjunction with --vmodule
// may be slow.
#define IPC_VIPC_LOG_IS_ON(verboselevel) \
  ((verboselevel) <= ::logging::GetVlogLevel(__FILE__))

// Helper macro which avoids evaluating the arguments to a stream if
// the condition doesn't hold.
#define IPC_LAZY_STREAM(stream, condition)                                  \
  !(condition) ? (void) 0 : ::logging::LogMessageVoidify() & (stream)

// We use the preprocessor's merging operator, "##", so that, e.g.,
// IPC_LOG(INFO) becomes the token IPC_COMPACT_GOOGLE_LOG_INFO.  There's some funny
// subtle difference between ostream member streaming functions (e.g.,
// ostream::operator<<(int) and ostream non-member streaming functions
// (e.g., ::operator<<(ostream&, string&): it turns out that it's
// impossible to stream something like a string directly to an unnamed
// ostream. We employ a neat hack by calling the stream() member
// function of LogMessage which seems to avoid the problem.
#define IPC_LOG_STREAM(severity) IPC_COMPACT_GOOGLE_LOG_ ## severity.stream()

#define IPC_LOG(severity) IPC_LAZY_STREAM(IPC_LOG_STREAM(severity), IPC_LOG_IS_ON(severity))
#define IPC_LOG_IF(severity, condition) \
  IPC_LAZY_STREAM(IPC_LOG_STREAM(severity), IPC_LOG_IS_ON(severity) && (condition))

#define IPC_SYSLOG(severity) IPC_LOG(severity)
#define IPC_SYSLOG_IF(severity, condition) IPC_LOG_IF(severity, condition)

#define IPC_VLOG(verboselevel) IPC_LOG_IF(INFO, IPC_VIPC_LOG_IS_ON(verboselevel))
#define IPC_VLOG_IF(verboselevel, condition) \
  IPC_LOG_IF(INFO, IPC_VIPC_LOG_IS_ON(verboselevel) && (condition))

// TODO(akalin): Add more IPC_VLOG variants, e.g. VPLOG.

#define IPC_LOG_ASSERT(condition)  \
  IPC_LOG_IF(FATAL, !(condition)) << "Assert failed: " #condition ". "
#define IPC_SYSLOG_ASSERT(condition) \
  IPC_SYSLOG_IF(FATAL, !(condition)) << "Assert failed: " #condition ". "

#if defined(OS_WIN)
#define LOG_GETLASTERROR_STREAM(severity) \
  IPC_COMPACT_GOOGLE_LOG_EX_ ## severity(Win32ErrorLogMessage, \
      ::logging::GetLastSystemErrorCode()).stream()
#define LOG_GETLASTERROR(severity) \
  IPC_LAZY_STREAM(LOG_GETLASTERROR_STREAM(severity), IPC_LOG_IS_ON(severity))
#define LOG_GETLASTERROR_MODULE_STREAM(severity, module) \
  IPC_COMPACT_GOOGLE_LOG_EX_ ## severity(Win32ErrorLogMessage, \
      ::logging::GetLastSystemErrorCode(), module).stream()
#define LOG_GETLASTERROR_MODULE(severity, module)                       \
  IPC_LAZY_STREAM(LOG_GETLASTERROR_STREAM(severity, module),                \
              IPC_LOG_IS_ON(severity))
// PIPC_LOG_STREAM is used by IPC_PLOG, which is the usual error logging macro
// for each platform.
#define PIPC_LOG_STREAM(severity) LOG_GETLASTERROR_STREAM(severity)
#elif defined(OS_POSIX)
#define LOG_ERRNO_STREAM(severity) \
  IPC_COMPACT_GOOGLE_LOG_EX_ ## severity(ErrnoLogMessage, \
      ::logging::GetLastSystemErrorCode()).stream()
#define LOG_ERRNO(severity) \
  IPC_LAZY_STREAM(LOG_ERRNO_STREAM(severity), IPC_LOG_IS_ON(severity))
// PIPC_LOG_STREAM is used by IPC_PLOG, which is the usual error logging macro
// for each platform.
#define PIPC_LOG_STREAM(severity) LOG_ERRNO_STREAM(severity)
// TODO(tschmelcher): Should we add OSStatus logging for Mac?
#endif

#define IPC_PLOG(severity)                                          \
  IPC_LAZY_STREAM(PIPC_LOG_STREAM(severity), IPC_LOG_IS_ON(severity))

#define IPC_PLOG_IF(severity, condition) \
  IPC_LAZY_STREAM(PIPC_LOG_STREAM(severity), IPC_LOG_IS_ON(severity) && (condition))

// IPC_CHECK dies with a fatal error if condition is not true.  It is *not*
// controlled by NDEBUG, so the check will be executed regardless of
// compilation mode.
//
// We make sure IPC_CHECK et al. always evaluates their arguments, as
// doing IPC_CHECK(FunctionWithSideEffect()) is a common idiom.
//
// TODO(akalin): Fix the problem where if the min log level is >
// FATAL, IPC_CHECK() et al. won't terminate the program.
#define IPC_CHECK(condition)                       \
  IPC_LAZY_STREAM(IPC_LOG_STREAM(FATAL), !(condition)) \
  << "Check failed: " #condition ". "

#define IPC_PCHECK(condition) \
  IPC_LAZY_STREAM(PIPC_LOG_STREAM(FATAL), !(condition)) \
  << "Check failed: " #condition ". "

// A container for a string pointer which can be evaluated to a bool -
// true iff the pointer is NULL.
struct CheckOpString {
  CheckOpString(std::string* str) : str_(str) { }
  // No destructor: if str_ is non-NULL, we're about to IPC_LOG(FATAL),
  // so there's no point in cleaning up str_.
  operator bool() const { return str_ != NULL; }
  std::string* str_;
};

// Build the error message string.  This is separate from the "Impl"
// function template because it is not performance critical and so can
// be out of line, while the "Impl" code should be inline.
template<class t1, class t2>
std::string* MakeCheckOpString(const t1& v1, const t2& v2, const char* names) {
  std::ostringstream ss;
  ss << names << " (" << v1 << " vs. " << v2 << ")";
  std::string* msg = new std::string(ss.str());
  return msg;
}

// MSVC doesn't like complex extern templates and DLLs.
#if !defined(COMPILER_MSVC)
// Commonly used instantiations of MakeCheckOpString<>. Explicitly instantiated
// in logging.cc.
extern template std::string* MakeCheckOpString<int, int>(
    const int&, const int&, const char* names);
extern template std::string* MakeCheckOpString<unsigned long, unsigned long>(
    const unsigned long&, const unsigned long&, const char* names);
extern template std::string* MakeCheckOpString<unsigned long, unsigned int>(
    const unsigned long&, const unsigned int&, const char* names);
extern template std::string* MakeCheckOpString<unsigned int, unsigned long>(
    const unsigned int&, const unsigned long&, const char* names);
extern template std::string* MakeCheckOpString<std::string, std::string>(
    const std::string&, const std::string&, const char* name);
#endif

// Helper macro for binary operators.
// Don't use this macro directly in your code, use IPC_CHECK_EQ et al below.
//
// TODO(akalin): Rewrite this so that constructs like if (...)
// IPC_CHECK_EQ(...) else { ... } work properly.
#define IPC_CHECK_OP(name, op, val1, val2)                          \
  if (logging::CheckOpString _result =                          \
      logging::Check##name##Impl((val1), (val2),                \
                                 #val1 " " #op " " #val2))      \
    logging::LogMessage(__FILE__, __LINE__, _result).stream()

// Helper functions for string comparisons.
// To avoid bloat, the definitions are in logging.cc.
//
// TODO(akalin): Actually have the implementations in logging.cc, or
// remove these.
#define DECLARE_CHECK_STROP_IMPL(func, expected) \
  std::string* Check##func##expected##Impl(const char* s1, \
                                           const char* s2, \
                                           const char* names);
DECLARE_CHECK_STROP_IMPL(strcmp, true)
DECLARE_CHECK_STROP_IMPL(strcmp, false)
DECLARE_CHECK_STROP_IMPL(_stricmp, true)
DECLARE_CHECK_STROP_IMPL(_stricmp, false)
#undef DECLARE_CHECK_STROP_IMPL

// Helper macro for string comparisons.
// Don't use this macro directly in your code, use CHECK_STREQ et al below.
#define CHECK_STROP(func, op, expected, s1, s2) \
  while (CheckOpString _result = \
      logging::Check##func##expected##Impl((s1), (s2), \
                                           #s1 " " #op " " #s2)) \
    IPC_LOG(FATAL) << *_result.str_

// String (char*) equality/inequality checks.
// CASE versions are case-insensitive.
//
// Note that "s1" and "s2" may be temporary strings which are destroyed
// by the compiler at the end of the current "full expression"
// (e.g. CHECK_STREQ(Foo().c_str(), Bar().c_str())).

#define CHECK_STREQ(s1, s2) CHECK_STROP(strcmp, ==, true, s1, s2)
#define CHECK_STRNE(s1, s2) CHECK_STROP(strcmp, !=, false, s1, s2)
#define CHECK_STRCASEEQ(s1, s2) CHECK_STROP(_stricmp, ==, true, s1, s2)
#define CHECK_STRCASENE(s1, s2) CHECK_STROP(_stricmp, !=, false, s1, s2)

#define CHECK_INDEX(I,A) IPC_CHECK(I < (sizeof(A)/sizeof(A[0])))
#define CHECK_BOUND(B,A) IPC_CHECK(B <= (sizeof(A)/sizeof(A[0])))

#define IPC_CHECK_EQ(val1, val2) IPC_CHECK_OP(EQ, ==, val1, val2)
#define IPC_CHECK_NE(val1, val2) IPC_CHECK_OP(NE, !=, val1, val2)
#define IPC_CHECK_LE(val1, val2) IPC_CHECK_OP(LE, <=, val1, val2)
#define IPC_CHECK_LT(val1, val2) IPC_CHECK_OP(LT, < , val1, val2)
#define IPC_CHECK_GE(val1, val2) IPC_CHECK_OP(GE, >=, val1, val2)
#define IPC_CHECK_GT(val1, val2) IPC_CHECK_OP(GT, > , val1, val2)

// http://crbug.com/16512 is open for a real fix for this.  For now, Windows
// uses OFFICIAL_BUILD and other platforms use the branding flag when NDEBUG is
// defined.
#if ( defined(OS_WIN) && defined(OFFICIAL_BUILD)) || \
    (!defined(OS_WIN) && defined(NDEBUG) && defined(GOOGLE_CHROME_BUILD))
// Used by unit tests.
#define LOGGING_IS_OFFICIAL_BUILD

// In order to have optimized code for official builds, remove DLOGs and
// DCHECKs.
#define ENABLE_DLOG 0
#define ENABLE_DCHECK 0

#elif defined(NDEBUG)
// Otherwise, if we're a release build, remove DLOGs but not DCHECKs
// (since those can still be turned on via a command-line flag).
#define ENABLE_DLOG 0
#define ENABLE_DCHECK 1

#else
// Otherwise, we're a debug build so enable DLOGs and DCHECKs.
#define ENABLE_DLOG 1
#define ENABLE_DCHECK 1
#endif

// Definitions for IPC_DLOG et al.

#if ENABLE_DLOG

#define IPC_DLOG_IF(severity, condition) IPC_LOG_IF(severity, condition)
#define IPC_DLOG_ASSERT(condition) IPC_LOG_ASSERT(condition)
#define IPC_DPLOG_IF(severity, condition) IPC_PLOG_IF(severity, condition)
#define IPC_DVLOG_IF(verboselevel, condition) IPC_VLOG_IF(verboselevel, condition)

#else  // ENABLE_DLOG

// If ENABLE_DLOG is off, we want to avoid emitting any references to
// |condition| (which may reference a variable defined only if NDEBUG
// is not defined).  Contrast this with IPC_DCHECK et al., which has
// different behavior.

#define DLOG_EAT_STREAM_PARAMETERS                                      \
  true ? (void) 0 : ::logging::LogMessageVoidify() & IPC_LOG_STREAM(FATAL)

#define IPC_DLOG_IF(severity, condition) DLOG_EAT_STREAM_PARAMETERS
#define IPC_DLOG_ASSERT(condition) DLOG_EAT_STREAM_PARAMETERS
#define IPC_DPLOG_IF(severity, condition) DLOG_EAT_STREAM_PARAMETERS
#define IPC_DVLOG_IF(verboselevel, condition) DLOG_EAT_STREAM_PARAMETERS

#endif  // ENABLE_DLOG

// DEBUG_MODE is for uses like
//   if (DEBUG_MODE) foo.CheckThatFoo();
// instead of
//   #ifndef NDEBUG
//     foo.CheckThatFoo();
//   #endif
//
// We tie its state to ENABLE_DLOG.
enum { DEBUG_MODE = ENABLE_DLOG };

#undef ENABLE_DLOG

#define IPC_DLOG_IS_ON(severity) (::logging::DEBUG_MODE && IPC_LOG_IS_ON(severity))

#define IPC_DLOG(severity)                                          \
  IPC_LAZY_STREAM(IPC_LOG_STREAM(severity), IPC_DLOG_IS_ON(severity))

#if defined(OS_WIN)
#define DLOG_GETLASTERROR(severity) \
  IPC_LAZY_STREAM(LOG_GETLASTERROR_STREAM(severity), IPC_DLOG_IS_ON(severity))
#define DLOG_GETLASTERROR_MODULE(severity, module)                      \
  IPC_LAZY_STREAM(LOG_GETLASTERROR_STREAM(severity, module),                \
              IPC_DLOG_IS_ON(severity))
#elif defined(OS_POSIX)
#define DLOG_ERRNO(severity)                                    \
  IPC_LAZY_STREAM(LOG_ERRNO_STREAM(severity), IPC_DLOG_IS_ON(severity))
#endif

#define IPC_DPLOG(severity)                                         \
  IPC_LAZY_STREAM(PIPC_LOG_STREAM(severity), IPC_DLOG_IS_ON(severity))

#define DIPC_VLOG(verboselevel) IPC_DLOG_IF(INFO, IPC_VIPC_LOG_IS_ON(verboselevel))

// Definitions for IPC_DCHECK et al.

#if ENABLE_DCHECK

#if defined(NDEBUG)

// Set to true in InitLogging when we want to enable the dchecks in release.
extern bool g_enable_dcheck;
#define IPC_DCHECK_IS_ON() (::logging::g_enable_dcheck)
#define DCHECK_SEVERITY ERROR_REPORT
const LogSeverity LOG_DCHECK = LOG_ERROR_REPORT;

#else  // defined(NDEBUG)

// On a regular debug build, we want to have DCHECKS enabled.
#define IPC_DCHECK_IS_ON() (true)
#define DCHECK_SEVERITY FATAL
const LogSeverity LOG_DCHECK = LOG_FATAL;

#endif  // defined(NDEBUG)

#else  // ENABLE_DCHECK

#define IPC_DCHECK_IS_ON() (false)
#define DCHECK_SEVERITY FATAL
const LogSeverity LOG_DCHECK = LOG_FATAL;

#endif  // ENABLE_DCHECK

// Unlike IPC_CHECK et al., IPC_DCHECK et al. *does* evaluate their arguments
// lazily.

// IPC_DCHECK et al. also make sure to reference |condition| regardless of
// whether DCHECKs are enabled; this is so that we don't get unused
// variable warnings if the only use of a variable is in a IPC_DCHECK.
// This behavior is different from IPC_DLOG_IF et al.

#define IPC_DCHECK(condition)                       \
  !IPC_DCHECK_IS_ON() ? (void) 0 :                  \
  IPC_LOG_IF(DCHECK_SEVERITY, !(condition))         \
  << "Check failed: " #condition ". "

#define IPC_DPCHECK(condition)                      \
  !IPC_DCHECK_IS_ON() ? (void) 0 :                  \
  IPC_PLOG_IF(DCHECK_SEVERITY, !(condition))        \
  << "Check failed: " #condition ". "

// Helper macro for binary operators.
// Don't use this macro directly in your code, use IPC_DCHECK_EQ et al below.
#define IPC_DCHECK_OP(name, op, val1, val2)                         \
  if (IPC_DLOG_IS_ON(DCHECK_SEVERITY))                              \
    if (logging::CheckOpString _result =                        \
        logging::Check##name##Impl((val1), (val2),              \
                                   #val1 " " #op " " #val2))    \
      logging::LogMessage(                                      \
          __FILE__, __LINE__, ::logging::LOG_DCHECK,            \
          _result).stream()

// Equality/Inequality checks - compare two values, and log a LOG_FATAL message
// including the two values when the result is not as expected.  The values
// must have operator<<(ostream, ...) defined.
//
// You may append to the error message like so:
//   IPC_DCHECK_NE(1, 2) << ": The world must be ending!";
//
// We are very careful to ensure that each argument is evaluated exactly
// once, and that anything which is legal to pass as a function argument is
// legal here.  In particular, the arguments may be temporary expressions
// which will end up being destroyed at the end of the apparent statement,
// for example:
//   IPC_DCHECK_EQ(string("abc")[1], 'b');
//
// WARNING: These may not compile correctly if one of the arguments is a pointer
// and the other is NULL. To work around this, simply static_cast NULL to the
// type of the desired pointer.

#define IPC_DCHECK_EQ(val1, val2) IPC_DCHECK_OP(EQ, ==, val1, val2)
#define IPC_DCHECK_NE(val1, val2) IPC_DCHECK_OP(NE, !=, val1, val2)
#define IPC_DCHECK_LE(val1, val2) IPC_DCHECK_OP(LE, <=, val1, val2)
#define IPC_DCHECK_LT(val1, val2) IPC_DCHECK_OP(LT, < , val1, val2)
#define IPC_DCHECK_GE(val1, val2) IPC_DCHECK_OP(GE, >=, val1, val2)
#define IPC_DCHECK_GT(val1, val2) IPC_DCHECK_OP(GT, > , val1, val2)

// Helper macro for string comparisons.
// Don't use this macro directly in your code, use IPC_DCHECK_STREQ et al below.
#define IPC_DCHECK_STROP(func, op, expected, s1, s2)        \
  if (IPC_DCHECK_IS_ON()) CHECK_STROP(func, op, expected, s1, s2)

// String (char*) equality/inequality checks.
// CASE versions are case-insensitive.
//
// Note that "s1" and "s2" may be temporary strings which are destroyed
// by the compiler at the end of the current "full expression"
// (e.g. IPC_DCHECK_STREQ(Foo().c_str(), Bar().c_str())).

#define IPC_DCHECK_STREQ(s1, s2) IPC_DCHECK_STROP(strcmp, ==, true, s1, s2)
#define IPC_DCHECK_STRNE(s1, s2) IPC_DCHECK_STROP(strcmp, !=, false, s1, s2)
#define IPC_DCHECK_STRCASEEQ(s1, s2) IPC_DCHECK_STROP(_stricmp, ==, true, s1, s2)
#define IPC_DCHECK_STRCASENE(s1, s2) IPC_DCHECK_STROP(_stricmp, !=, false, s1, s2)

#define IPC_DCHECK_INDEX(I,A) IPC_DCHECK(I < (sizeof(A)/sizeof(A[0])))
#define IPC_DCHECK_BOUND(B,A) IPC_DCHECK(B <= (sizeof(A)/sizeof(A[0])))

// Helper functions for IPC_CHECK_OP macro.
// The (int, int) specialization works around the issue that the compiler
// will not instantiate the template version of the function on values of
// unnamed enum type - see comment below.
#define IPC_DEFINE_CHECK_OP_IMPL(name, op) \
  template <class t1, class t2> \
  inline std::string* Check##name##Impl(const t1& v1, const t2& v2, \
                                        const char* names) { \
    if (v1 op v2) return NULL; \
    else return MakeCheckOpString(v1, v2, names); \
  } \
  inline std::string* Check##name##Impl(int v1, int v2, const char* names) { \
    if (v1 op v2) return NULL; \
    else return MakeCheckOpString(v1, v2, names); \
  }
IPC_DEFINE_CHECK_OP_IMPL(EQ, ==)
IPC_DEFINE_CHECK_OP_IMPL(NE, !=)
IPC_DEFINE_CHECK_OP_IMPL(LE, <=)
IPC_DEFINE_CHECK_OP_IMPL(LT, < )
IPC_DEFINE_CHECK_OP_IMPL(GE, >=)
IPC_DEFINE_CHECK_OP_IMPL(GT, > )
#undef IPC_DEFINE_CHECK_OP_IMPL

#define IPC_NOTREACHED() IPC_DCHECK(false)

// Redefine the standard assert to use our nice log files
#undef assert
#define assert(x) IPC_DLOG_ASSERT(x)

// This class more or less represents a particular log message.  You
// create an instance of LogMessage and then stream stuff to it.
// When you finish streaming to it, ~LogMessage is called and the
// full message gets streamed to the appropriate destination.
//
// You shouldn't actually use LogMessage's constructor to log things,
// though.  You should use the IPC_LOG() macro (and variants thereof)
// above.
class LogMessage {
 public:
  LogMessage(const char* file, int line, LogSeverity severity, int ctr);

  // Two special constructors that generate reduced amounts of code at
  // IPC_LOG call sites for common cases.
  //
  // Used for IPC_LOG(INFO): Implied are:
  // severity = LOG_INFO, ctr = 0
  //
  // Using this constructor instead of the more complex constructor above
  // saves a couple of bytes per call site.
  LogMessage(const char* file, int line);

  // Used for IPC_LOG(severity) where severity != INFO.  Implied
  // are: ctr = 0
  //
  // Using this constructor instead of the more complex constructor above
  // saves a couple of bytes per call site.
  LogMessage(const char* file, int line, LogSeverity severity);

  // A special constructor used for check failures.
  // Implied severity = LOG_FATAL
  LogMessage(const char* file, int line, const CheckOpString& result);

  // A special constructor used for check failures, with the option to
  // specify severity.
  LogMessage(const char* file, int line, LogSeverity severity,
             const CheckOpString& result);

  ~LogMessage();

  std::ostream& stream() { return stream_; }

 private:
  void Init(const char* file, int line);

  LogSeverity severity_;
  std::ostringstream stream_;
  size_t message_start_;  // Offset of the start of the message (past prefix
                          // info).
#if defined(OS_WIN)
  // Stores the current value of GetLastError in the constructor and restores
  // it in the destructor by calling SetLastError.
  // This is useful since the LogMessage class uses a lot of Win32 calls
  // that will lose the value of GLE and the code that called the log function
  // will have lost the thread error value when the log call returns.
  class SaveLastError {
   public:
    SaveLastError();
    ~SaveLastError();

    unsigned long get_error() const { return last_error_; }

   protected:
    unsigned long last_error_;
  };

  SaveLastError last_error_;
#endif

  DISALLOW_COPY_AND_ASSIGN(LogMessage);
};

// A non-macro interface to the log facility; (useful
// when the logging level is not a compile-time constant).
inline void LogAtLevel(int const log_level, std::string const &msg) {
  LogMessage(__FILE__, __LINE__, log_level).stream() << msg;
}

// This class is used to explicitly ignore values in the conditional
// logging macros.  This avoids compiler warnings like "value computed
// is not used" and "statement has no effect".
class LogMessageVoidify {
 public:
  LogMessageVoidify() { }
  // This has to be an operator with a precedence lower than << but
  // higher than ?:
  void operator&(std::ostream&) { }
};

#if defined(OS_WIN)
typedef unsigned long SystemErrorCode;
#elif defined(OS_POSIX)
typedef int SystemErrorCode;
#endif

// Alias for ::GetLastError() on Windows and errno on POSIX. Avoids having to
// pull in windows.h just for GetLastError() and DWORD.
SystemErrorCode GetLastSystemErrorCode();

#if defined(OS_WIN)
// Appends a formatted system message of the GetLastError() type.
class Win32ErrorLogMessage {
 public:
  Win32ErrorLogMessage(const char* file,
                       int line,
                       LogSeverity severity,
                       SystemErrorCode err,
                       const char* module);

  Win32ErrorLogMessage(const char* file,
                       int line,
                       LogSeverity severity,
                       SystemErrorCode err);

  std::ostream& stream() { return log_message_.stream(); }

  // Appends the error message before destructing the encapsulated class.
  ~Win32ErrorLogMessage();

 private:
  SystemErrorCode err_;
  // Optional name of the module defining the error.
  const char* module_;
  LogMessage log_message_;

  DISALLOW_COPY_AND_ASSIGN(Win32ErrorLogMessage);
};
#elif defined(OS_POSIX)
// Appends a formatted system message of the errno type
class ErrnoLogMessage {
 public:
  ErrnoLogMessage(const char* file,
                  int line,
                  LogSeverity severity,
                  SystemErrorCode err);

  std::ostream& stream() { return log_message_.stream(); }

  // Appends the error message before destructing the encapsulated class.
  ~ErrnoLogMessage();

 private:
  SystemErrorCode err_;
  LogMessage log_message_;

  DISALLOW_COPY_AND_ASSIGN(ErrnoLogMessage);
};
#endif  // OS_WIN

// Closes the log file explicitly if open.
// NOTE: Since the log file is opened as necessary by the action of logging
//       statements, there's no guarantee that it will stay closed
//       after this call.
void CloseLogFile();

// Async signal safe logging mechanism.
void RawLog(int level, const char* message);

#define RAW_LOG(level, message) logging::RawLog(logging::LOG_ ## level, message)

#define RAW_CHECK(condition)                                                   \
  do {                                                                         \
    if (!(condition))                                                          \
      logging::RawLog(logging::LOG_FATAL, "Check failed: " #condition "\n");   \
  } while (0)

}  // namespace logging

// These functions are provided as a convenience for logging, which is where we
// use streams (it is against Google style to use streams in other places). It
// is designed to allow you to emit non-ASCII Unicode strings to the log file,
// which is normally ASCII. It is relatively slow, so try not to use it for
// common cases. Non-ASCII characters will be converted to UTF-8 by these
// operators.
std::ostream& operator<<(std::ostream& out, const wchar_t* wstr);
//std::ostream& operator<<(std::ostream& out, const std::wstring& wstr);
// Note: already defined in the CEF
// inline std::ostream& operator<<(std::ostream& out, const std::wstring& wstr)
// {
//   return out << wstr.c_str();
// }

// The IPC_NOTIMPLEMENTED() macro annotates codepaths which have
// not been implemented yet.
//
// The implementation of this macro is controlled by NOTIMPLEMENTED_POLICY:
//   0 -- Do nothing (stripped by compiler)
//   1 -- Warn at compile time
//   2 -- Fail at compile time
//   3 -- Fail at runtime (IPC_DCHECK)
//   4 -- [default] IPC_LOG(ERROR) at runtime
//   5 -- IPC_LOG(ERROR) at runtime, only once per call-site

#ifndef NOTIMPLEMENTED_POLICY
// Select default policy: IPC_LOG(ERROR)
#define NOTIMPLEMENTED_POLICY 4
#endif

#if defined(COMPILER_GCC)
// On Linux, with GCC, we can use __PRETTY_FUNCTION__ to get the demangled name
// of the current function in the IPC_NOTIMPLEMENTED message.
#define NOTIMPLEMENTED_MSG "Not implemented reached in " << __PRETTY_FUNCTION__
#else
#define NOTIMPLEMENTED_MSG "NOT IMPLEMENTED"
#endif

#if NOTIMPLEMENTED_POLICY == 0
#define IPC_NOTIMPLEMENTED() ;
#elif NOTIMPLEMENTED_POLICY == 1
// TODO, figure out how to generate a warning
#define IPC_NOTIMPLEMENTED() COMPILE_ASSERT(false, NOT_IMPLEMENTED)
#elif NOTIMPLEMENTED_POLICY == 2
#define IPC_NOTIMPLEMENTED() COMPILE_ASSERT(false, NOT_IMPLEMENTED)
#elif NOTIMPLEMENTED_POLICY == 3
#define IPC_NOTIMPLEMENTED() IPC_NOTREACHED()
#elif NOTIMPLEMENTED_POLICY == 4
#define IPC_NOTIMPLEMENTED() IPC_LOG(ERROR) << NOTIMPLEMENTED_MSG
#elif NOTIMPLEMENTED_POLICY == 5
#define IPC_NOTIMPLEMENTED() do {\
  static int count = 0;\
  IPC_LOG_IF(ERROR, 0 == count++) << NOTIMPLEMENTED_MSG;\
} while(0)
#endif

#endif  // IPC_BASE_LOGGING_H_
