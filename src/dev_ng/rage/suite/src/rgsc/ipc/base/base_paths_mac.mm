// Copyright (c) 2006-2008 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "base/base_paths_mac.h"

#import <Cocoa/Cocoa.h>
#include <mach-o/dyld.h>

#include "base/compiler_specific.h"
#include "base/file_path.h"
#include "base/file_util.h"
#include "base/logging.h"
#include "base/mac_util.h"
#include "base/path_service.h"
#include "base/string_util.h"

namespace {

bool GetNSExecutablePath(FilePath* path) WARN_UNUSED_RESULT;

bool GetNSExecutablePath(FilePath* path) {
  IPC_DCHECK(path);
  // Executable path can have relative references ("..") depending on
  // how the app was launched.
  uint32_t executable_length = 0;
  _NSGetExecutablePath(NULL, &executable_length);
  IPC_DCHECK_GE(executable_length, 1u);
  std::string executable_path;
  char* executable_path_c = WriteInto(&executable_path, executable_length);
  int rv = _NSGetExecutablePath(executable_path_c, &executable_length);
  IPC_DCHECK_EQ(rv, 0);
  IPC_DCHECK(!executable_path.empty());
  if ((rv != 0) || (executable_path.empty()))
    return false;
  *path = FilePath(executable_path);
  return true;
}

}  // namespace

namespace ipcbase {

bool PathProviderMac(int key, FilePath* result) {
  switch (key) {
    case ipcbase::FILE_EXE:
    case ipcbase::FILE_MODULE: {
      return GetNSExecutablePath(result);
    }
    case ipcbase::DIR_USER_CACHE:
      return mac_util::GetUserDirectory(NSCachesDirectory, result);
    case ipcbase::DIR_APP_DATA:
      return mac_util::GetUserDirectory(NSApplicationSupportDirectory, result);
    case ipcbase::DIR_SOURCE_ROOT: {
      if (GetNSExecutablePath(result)) {
        // Start with the executable's directory.
        *result = result->DirName();
        if (mac_util::AmIBundled()) {
          // The bundled app executables (Chromium, TestShell, etc) live five
          // levels down, eg:
          // src/xcodebuild/{Debug|Release}/Chromium.app/Contents/MacOS/Chromium
          *result = result->DirName().DirName().DirName().DirName().DirName();
        } else {
          // Unit tests execute two levels deep from the source root, eg:
          // src/xcodebuild/{Debug|Release}/base_unittests
          *result = result->DirName().DirName();
        }
      }
      return true;
    }
    default:
      return false;
  }
}

}  // namespace ipcbase
