// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "base/cancellation_flag.h"

#include "base/logging.h"

namespace ipcbase {

void CancellationFlag::Set() {
#if !defined(NDEBUG)
  IPC_DCHECK_EQ(set_on_, PlatformThread::CurrentId());
#endif
  ipcbase::subtle::Release_Store(&flag_, 1);
}

bool CancellationFlag::IsSet() const {
  return ipcbase::subtle::Acquire_Load(&flag_) != 0;
}

}  // namespace ipcbase
