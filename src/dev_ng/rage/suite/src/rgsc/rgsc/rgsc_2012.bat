@echo off
setlocal
cd /d "X:\gta5\src\dev_ng\rage\suite\src\rgsc\rgsc"
for /f "usebackq delims==" %%v in (`set rs_`) do set %%v=
for /f "usebackq delims==" %%v in (`set rage_`) do set %%v=
set RS_PROJECT=gta5
set RS_PROJROOT=x:\gta5
set RS_CODEBRANCH=x:\gta5\src\dev_ng
set RS_BUILDBRANCH=x:\gta5\titleupdate\dev_ng
set RS_TOOLSROOT=x:\gta5\tools_ng
set RAGE_DIR=%RS_CODEBRANCH%\rage
set RUBYLIB=%RS_TOOLSROOT%\lib
set PATH=%RS_TOOLSROOT%\bin;%RS_TOOLSROOT%\bin\ruby\bin;%PATH%
call "%RS_TOOLSROOT%\bin\setenv.bat"
set SCE_PS3_ROOT=x:/ps3sdk/dev/usr/local/430_001/cell
set _NO_DEBUG_HEAP=1
echo Visual Studio environment:
echo RAGE_DIR:      %RAGE_DIR%
echo SCE_PS3_ROOT   %SCE_PS3_ROOT%
start "" rgsc_2012.sln
