// commerce.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "file/file_config.h"
#include "commerce.h"
#include "rline/entitlement/rlrosentitlement.h"
#include "rline/entitlement/rlrosentitlementtask.h"
#include "rline/entitlementV1/rlrosentitlementV1.h"
#include "rline/entitlementV1/rlrosentitlementV1task.h"
#include "rline/ros/rlros.h"

//#include "fwCommerce/CommerceUtil.h"

#if RSG_PC
#include "diag/output.h"
#include "diag/seh.h"
#include "json.h"
#include "rgsc.h"
#include "rgsc_ui/script.h"
#include "rline/rldiag.h"
#include "rline/cloud/rlcloud.h"
#include "system/nelem.h"
#include "system/timer.h"
#include <time.h>

using namespace rage;

namespace rgsc
{ 

#if __RAGE_DEPENDENCY
RAGE_DEFINE_SUBCHANNEL(rgsc_dbg, commerce)
#undef __rage_channel
#define __rage_channel rgsc_dbg_commerce
#endif


const char* RESULT_TRUE_JSON_RESPONSE = "{ \"Result\": true }";
const char* RESULT_FALSE_JSON_RESPONSE = "{ \"Result\": false }";
static const unsigned PURCHASE_FLOW_TELEMETY_BUF_SIZE = 8192;

// ===========================================================================
// ICommerceManager Public Interface
// ===========================================================================

RGSC_HRESULT RgscCommerceManager::QueryInterface(RGSC_REFIID riid, LPVOID* ppvObj)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObj == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<ICommerceManager*>(this);
	}
	else if(riid == IID_ICommerceManagerV1)
	{
		pUnknown = static_cast<ICommerceManagerV1*>(this);
	}
	else if(riid == IID_ICommerceManagerV2)
	{
		pUnknown = static_cast<ICommerceManagerV2*>(this);
	}
	else if(riid == IID_ICommerceManagerV3)
	{
		pUnknown = static_cast<ICommerceManagerV3*>(this);
	}
#if !PRELOADED_SOCIAL_CLUB
	else if(riid == IID_ICommerceManagerV4)
	{
		pUnknown = static_cast<ICommerceManagerV4*>(this);
	}
#endif
	else if (riid == IID_ICommerceManagerV5)
	{
		pUnknown = static_cast<ICommerceManagerV5*>(this);
	}

	*ppvObj = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

// ===========================================================================
// CommerceManager
// ===========================================================================

RgscCommerceManager::RgscCommerceManager()

#if RSG_CPU_X64
	:
	m_CurrentVoucherRedemptionState(VOUCHER_REDEMPTION_NEUTRAL),
	m_CurrentAgeDataState(AGE_DATA_STATE_INIT),
	m_CurrentCommerceManifestFetchState(COMMERCE_MANIFEST_FETCH_NEUTRAL),
	m_CheckoutUrlFetchState(CHECKOUT_URL_FETCH_NEUTRAL),
	m_LocalGamerIndex(0),
	m_LastCommerceManifestFetchFailed(false),
	m_EntitlementDataPopulated(false),
	m_EntitlementDataError(false)
#endif
{
    ClearCallbacks();	
}

RgscCommerceManager::~RgscCommerceManager()
{
    Shutdown();
}

bool
RgscCommerceManager::Init()
{
	ClearCallbacks();

#if RSG_CPU_X64
	m_CurrentAgeDataState = AGE_DATA_STATE_INIT;

	m_EntitlementDataPopulated = false;
#endif
	return true;
}

void
RgscCommerceManager::Update()
{
#if RSG_CPU_X64

#if !PRELOADED_SOCIAL_CLUB 
	UpdateGetEntitlement();
#endif
	UpdateVoucherRedemption();
	UpdateCheckoutUrlFetch();
    UpdateManifestFetch();
#endif
}

void
RgscCommerceManager::Shutdown()
{
	ClearCallbacks();
}

void
RgscCommerceManager::ClearCallbacks()
{
	m_RedemptionCodeCallback = NULL;
	m_UserResponseCallback = NULL;
#if RSG_CPU_X64
	m_CurrentSteamStoreOpenCallback = NULL;
	m_IsContainedPackageInstalledCallback = NULL;
#if !PRELOADED_SOCIAL_CLUB
	m_PurchaseFlowTelemetryCallback = NULL;
#endif
#endif
}

void 
RgscCommerceManager::SignOut()
{
#if RSG_CPU_X64
    m_CurrentAgeDataState = AGE_DATA_STATE_INIT;
    m_EntitlementDataPopulated = false;
#endif

}

static json_char* json_as_string_or_null(json_const JSONNODE* node)
{
	char* str = json_as_string(node);
	return (_stricmp("null", str) != 0) ? str : NULL;
}

RGSC_HRESULT 
RgscCommerceManager::ShowCommerceUi(const char* url)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		JSONNODE *n = json_new(JSON_NODE);
		rverify(n, catchall, );

		json_set_name(n, "Data");

		if(url && (url[0] != '\0'))
		{
			// a NULL URL tells the SCUI to bring up an empty panel with a spinner
			// until a second call to this function sends it the real URL.
			json_push_back(n, json_new_a("Url", url));
		}

		rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(true,
																				 RgscUi::UI_STATE_COMMERCE,
																				 RgscUi::UI_SUBSTATE_NONE,
																				 n),
																				 catchall,
																				 hr = RGSC_FAIL);

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT 
RgscCommerceManager::ShowRedemptionCodeUi(RedemptionCodeCallback callback)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(callback, catchall, hr = RGSC_INVALIDARG);

		rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(true,
																				 RgscUi::UI_STATE_COMMERCE,
																				 RgscUi::UI_SUBSTATE_REDEMPTION_CODE,
																				 NULL),
																				 catchall,
																				 hr = RGSC_FAIL);

		m_RedemptionCodeCallback = callback;

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

void 
RgscCommerceManager::RedemptionCodeResponse(const char* jsonResponse)
{
	if(AssertVerify(m_RedemptionCodeCallback))
	{
		char* redemptionCode = NULL;
		int action = 0;

		JSONNODE *n = json_parse(jsonResponse);

		if(!AssertVerify(n != NULL))
		{
			return;
		}

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "RedemptionCode") == 0)
			{
				redemptionCode = json_as_string_or_null(*i);
			}
			else if(_stricmp(node_name, "Action") == 0)
			{
				action = json_as_int(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);

		m_RedemptionCodeCallback(redemptionCode, (RedemptionAction)action);

		json_free_safe(redemptionCode);
	}
}

RGSC_HRESULT 
RgscCommerceManager::RedemptionCodeResult(const int result,
									  StringId message,
									  RedemptionCodeCallback callback)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		JSONNODE *n = json_new(JSON_NODE);
		rverify(n, catchall, );

		json_set_name(n, "RedemptionResult");

		json_push_back(n, json_new_i("Result", result));
		json_push_back(n, json_new_i("Message", message));

		json_char *jc = json_write_formatted(n);

		json_delete(n);

		rlDebug2("%s\n", jc);

		bool succeeded = false;
		rgsc::Script::JsRedemptionResult(&succeeded, jc);

		m_UserResponseCallback = callback;

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT 
RgscCommerceManager::RedemptionCodeResult(const int result,
									  StringId message,
									  const char* contentName,
									  ICommerceManagerV1::RedemptionCodeCallback callback)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		JSONNODE *n = json_new(JSON_NODE);
		rverify(n, catchall, );

		json_set_name(n, "RedemptionResult");

		json_push_back(n, json_new_i("Result", result));
		json_push_back(n, json_new_i("Message", message));

		if(contentName && (contentName[0] != '\0'))
		{
			json_push_back(n, json_new_a("ContentName", contentName));
		}

		json_char *jc = json_write_formatted(n);

		json_delete(n);

		rlDebug2("%s\n", jc);

		bool succeeded = false;
		rgsc::Script::JsRedemptionResult(&succeeded, jc);

		m_UserResponseCallback = callback;

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}


void 
RgscCommerceManager::UserResponse(const char* jsonResponse)
{
	if(AssertVerify(m_UserResponseCallback))
	{
		int action = 0;

		JSONNODE *n = json_parse(jsonResponse);

		if(!AssertVerify(n != NULL))
		{
			return;
		}

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "Action") == 0)
			{
				action = json_as_int(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);

		m_UserResponseCallback("", (RedemptionAction)action);
	}
}

void 
RgscCommerceManager::DownloadComplete(const char* name, bool success)
{
	rtry
	{
		JSONNODE* rootNode = json_new(JSON_NODE);
		rverify(rootNode, catchall, );

		json_push_back(rootNode, json_new_i("Count", 1));

		JSONNODE* notificationArray = json_new(JSON_ARRAY);
		rverify(notificationArray, catchall, );
		json_set_name(notificationArray, "Notifications");

		JSONNODE* notificationNode = json_new(JSON_NODE);
		rverify(notificationNode, catchall, );

		json_push_back(notificationNode, json_new_a("Type", "DownloadStatus"));

		JSONNODE* downloadStatusArray = json_new(JSON_ARRAY);
		rverify(downloadStatusArray, catchall, );

		json_set_name(downloadStatusArray, "Data");

		JSONNODE* downloadStatusNode = json_new(JSON_NODE);
		rverify(downloadStatusNode, catchall, );

		json_set_name(downloadStatusNode, "DownloadStatus");

		if(name && name[0] != '\0')
		{
			json_push_back(downloadStatusNode, json_new_a("Name", name));
		}

		json_push_back(downloadStatusNode, json_new_i("Message", success ? 2 : 3));

		json_push_back(downloadStatusArray, downloadStatusNode);

		json_push_back(notificationNode, downloadStatusArray);

		json_push_back(notificationArray, notificationNode);

		json_push_back(rootNode, notificationArray);

		json_char* jc = json_write_formatted(rootNode);

#if !__NO_OUTPUT
		size_t len = strlen(jc);
		for(size_t i = 0; i < len; i++)
		{
			printf("%c", jc[i]);
		}
		fflush(stdout);
#endif

		json_delete(rootNode);

		GetRgscConcreteInstance()->_GetUiInterface()->SendNotification(jc);

		json_free_safe(jc);
	}
	rcatchall
	{

	}
}

#if RSG_CPU_X64
void RgscCommerceManager::InitCommerceUtil()
{
// 	if ( mp_CommerceUtil == NULL )
// 	{
// 		//Currently forcing base version as per platform data setup is not ready.
// 		mp_CommerceUtil = rage::CommerceUtilInstance(true);
// 		mp_CommerceUtil->Init(m_LocalGamerIndex,"drCommerce/en");
// 	}
}

void RgscCommerceManager::ShutdownCommerceUtil()
{
// 	if ( mp_CommerceUtil != NULL )
// 	{
// 		delete mp_CommerceUtil;
// 	}
// 
// 	mp_CommerceUtil = NULL;
}

void RGSC_CALL RgscCommerceManager::InitData()
{
	//InitCommerceUtil();	
}

void RGSC_CALL RgscCommerceManager::DestroyData()
{
	//ShutdownCommerceUtil();
}

bool RGSC_CALL RgscCommerceManager::StartDataFetch( const char* parameterJson )
{
	JSONNODE *n = json_parse(parameterJson);

	if(!AssertVerify(n != NULL))
	{
		return false;
	}

	char* languageCode = NULL;
	char* countryCode = NULL;

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return false;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "languageCode") == 0)
		{
			languageCode = json_as_string_or_null(*i);
		}

		if(_stricmp(node_name, "countryCode") == 0)
		{
			countryCode = json_as_string_or_null(*i);
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	// A request is already in progress
	if (m_CommerceManifestFetchStatus.Pending())
	{
		json_free_safe(languageCode);
		json_free_safe(countryCode);
		return true;
	}
	else
	{
		m_CommerceManifest.Reset();
	}

	bool steamVersion = GetRgscConcreteInstance()->IsSteam();

	if (rlRosEntitlement::GetCommerceManifest(m_LocalGamerIndex,languageCode, countryCode == NULL ? "us" : countryCode, steamVersion ? "steam" : "dr" ,&m_CommerceManifest, &m_CommerceManifestFetchStatus))
	{
		m_CurrentCommerceManifestFetchState = COMMERCE_MANIFEST_FETCH_FETCHING;
	}
	else
	{
		json_free_safe(languageCode);
		json_free_safe(countryCode);
		m_LastCommerceManifestFetchFailed = true;
		return false;
	}

	json_free_safe(languageCode);
	json_free_safe(countryCode);

	return true;
}

const char* RGSC_CALL RgscCommerceManager::IsDataValidAsJson()
{
	rtry
	{
		JSONNODE* rootNode = json_new(JSON_NODE);
		rverify(rootNode, catchall, );

		if ( m_CommerceManifest.GetLength() == 0 )
		{
			json_push_back(rootNode, json_new_b("Result", false));
		}
		else
		{
			json_push_back(rootNode, json_new_b("Result", true));
		}

		json_char* jc = json_write_formatted(rootNode);

		json_delete(rootNode);

		return jc;
	}
	rcatchall
	{

	}

	return NULL;
}

const char* RGSC_CALL RgscCommerceManager::GetDataAsJson()
{
	if ( m_CommerceManifest.GetLength() == 0 )
	{
		return NULL;
	}	

	return CommerceDataResponse();
}

const char* RgscCommerceManager::CommerceDataResponse()
{
	if ( m_CommerceManifest.GetLength() == 0 )
	{
		return "NULL";
	}

	return m_CommerceManifest.c_str();
// 	rtry
// 	{
// 		JSONNODE* rootNode = json_new(JSON_NODE);
// 		rverify(rootNode, catchall, );
// 
// 		json_push_back(rootNode, json_new_i("Count", mp_CommerceUtil->GetNumItems()));
// 	
// 		int numProducts = 0;
// 		int numCategories = 0;
// 
// 		for ( int iItems = 0; iItems < mp_CommerceUtil->GetNumItems(); iItems++)
// 		{
// 			
// 
// 			const cCommerceItemData *itemData = mp_CommerceUtil->GetItemData(iItems);
// 
// 			if (itemData->GetItemType() == cCommerceItemData::ITEM_TYPE_CATEGORY)
// 			{
// 				numCategories++;
// 			}
// 			else if(itemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT)
// 			{
// 				const cCommerceProductData* productData = static_cast<const cCommerceProductData*>(itemData);
// 
// 				if ( productData->GetEnumeratedFlag() || productData->IsVirtualProduct())
// 				{
// 					numProducts++;
// 				}
// 			}
// 		}
// 
// 		json_push_back(rootNode, json_new_i("CategoryCount", numCategories));
// 		json_push_back(rootNode, json_new_i("ProductCount", numProducts));
// 		
// 		JSONNODE* categoryArray= json_new(JSON_ARRAY);
// 		json_set_name(categoryArray, "Categories");
// 
// 		for ( int iCat = 0; iCat < mp_CommerceUtil->GetNumItems(); iCat++)
// 		{
// 			const cCommerceItemData *itemData = mp_CommerceUtil->GetItemData(iCat);
// 			if (itemData->GetItemType() != cCommerceItemData::ITEM_TYPE_CATEGORY)
// 			{
// 				continue;
// 			}
// 			JSONNODE* categoryNode = json_new(JSON_NODE);
// 			rverify(categoryNode, catchall, );
// 
// 			json_push_back(categoryNode, json_new_a("Id", mp_CommerceUtil->GetItemId(iCat)));
// 			json_push_back(categoryNode, json_new_a("Name", mp_CommerceUtil->GetItemName(iCat)));
// 
// 			json_push_back(categoryArray,categoryNode);
// 		}
// 
// 		json_push_back(rootNode ,categoryArray);
// 
// 		JSONNODE* productArray = json_new(JSON_ARRAY);
// 		json_set_name(productArray, "Products");
// 
// 		for ( int i=0; i < mp_CommerceUtil->GetNumItems(); i++ )
// 		{
// 			const cCommerceItemData *itemData = mp_CommerceUtil->GetItemData(i);
// 			if (itemData->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
// 			{
// 				continue;
// 			}
// 
// 			const cCommerceProductData* productData = static_cast<const cCommerceProductData*>(itemData);
// 
// 			if (!productData->GetEnumeratedFlag() && !productData->IsVirtualProduct())
// 			{
// 				continue;
// 			}
// 
// 			JSONNODE* productNode = json_new(JSON_NODE);
// 			rverify(productNode, catchall, );
// 
// 			json_push_back(productNode, json_new_a("Id", mp_CommerceUtil->GetItemId(i)));
// 			json_push_back(productNode, json_new_a("Name", mp_CommerceUtil->GetItemName(i)));
// 
// 			
// 
// 			
// 			
// 			
// 			atString longDescString(mp_CommerceUtil->GetItemLongDescription(productData));
// 			longDescString.Replace("\n","<BR>");
// 			longDescString.Replace("\r","");
// 			longDescString.Replace("\t"," ");
// 			json_push_back(productNode, json_new_a("Desc", longDescString.c_str()));
// 
// 			const int priceStringLen = 256;
// 			char priceString[priceStringLen];
// 			priceString[0] = 0;
// 			float price = mp_CommerceUtil->GetProductPrice(i,priceString,priceStringLen);
// 			json_push_back(productNode, json_new_a("PriceString", priceString));
// 			json_push_back(productNode, json_new_f("Price", price));
// 			json_push_back(productNode, json_new_b("Purchased", mp_CommerceUtil->IsProductPurchased(i)));
// 			json_push_back(productNode, json_new_b("Installed", mp_CommerceUtil->IsProductInstalled(productData)));
// 			json_push_back(productNode, json_new_b("Consumable", productData->IsConsumable()));
// 			
// 			if ( productData->GetCategoryMemberships().GetCount() > 0 )
// 			{
// 				JSONNODE* categoryMembershipArray= json_new(JSON_ARRAY);
// 				json_set_name(categoryMembershipArray, "CategoryMemberships");
// 
// 				for ( int iCatMems = 0; iCatMems < productData->GetCategoryMemberships().GetCount(); iCatMems++ )
// 				{
// 					JSONNODE* catMemNode = json_new(JSON_NODE);
// 					rverify(catMemNode, catchall, );
// 
// 					json_push_back(catMemNode, json_new_a("MemberOf", productData->GetCategoryMemberships()[iCatMems]));
// 
// 					json_push_back(categoryMembershipArray, catMemNode);
// 				}
// 
// 				json_push_back(productNode, categoryMembershipArray);
// 			}
// 
// 			if ( productData->GetImagePaths().GetCount() > 0 )
// 			{
// 				JSONNODE* imagePathArray= json_new(JSON_ARRAY);
// 				json_set_name(imagePathArray, "ImagePaths");
// 
// 				for ( int iImagePaths = 0; iImagePaths < productData->GetImagePaths().GetCount(); iImagePaths++ )
// 				{
// 					JSONNODE* imagePathNode = json_new(JSON_NODE);
// 					rverify(imagePathNode, catchall, );
// 
// 					json_push_back(imagePathNode, json_new_a("ImagePath", productData->GetImagePaths()[iImagePaths]));
// 
// 					json_push_back(imagePathArray, imagePathNode);
// 				}
// 
// 				json_push_back(productNode, imagePathArray);
// 			}
// 
// 			
// 			json_push_back(productArray,productNode);
// 		}
// 		
// 		json_push_back(rootNode ,productArray);
// 
// 		json_char* jc = json_write_formatted(rootNode);
// 
// 		json_delete(rootNode);
// 
// 		return jc;
// 	}
// 	rcatchall
// 	{
// 
// 	}
// 
// 	return NULL;
}

const char* RGSC_CALL RgscCommerceManager::IsInErrorStateAsJson()
{
	rtry
	{
		JSONNODE* rootNode = json_new(JSON_NODE);
		rverify(rootNode, catchall, );
		
		if ( m_CommerceManifest.GetLength() == 0 )
		{
			json_push_back(rootNode, json_new_b("Result", false));
		}
		else
		{
			json_push_back(rootNode, json_new_b("Result", m_LastCommerceManifestFetchFailed));
		}
		

		json_char* jc = json_write_formatted(rootNode);

		json_delete(rootNode);

		return jc;
	}
	rcatchall
	{

	}

	return NULL;
}

const char* RGSC_CALL RgscCommerceManager::GetItemDataAsJson( const char* /*parameterJson*/ )
{
	if ( m_CommerceManifest.GetLength() == 0 )
	{
		return NULL;
	}

	return m_CommerceManifest.c_str();
	
// 	if ( mp_CommerceUtil == NULL )
// 	{
// 		return NULL;
// 	}
// 
// 	JSONNODE *n = json_parse(parameterJson);
// 
// 	if(!AssertVerify(n != NULL))
// 	{
// 		return false;
// 	}
// 
// 	char* productId = NULL;
// 
// 	JSONNODE_ITERATOR i = json_begin(n);
// 	while(i != json_end(n))
// 	{
// 		if(!AssertVerify(*i != NULL))
// 		{
// 			return false;
// 		}
// 
// 		// get the node name and value as a string
// 		json_char *node_name = json_name(*i);
// 
// 		if(_stricmp(node_name, "productId") == 0)
// 		{
// 			productId = json_as_string_or_null(*i);
// 		}
// 
// 		// cleanup and increment the iterator
// 		json_free_safe(node_name);
// 		++i;
// 	}
// 
// 	const cCommerceItemData *itemData = mp_CommerceUtil->GetItemData(productId);
// 
// 	if ( itemData == NULL )
// 	{
// 		return NULL;
// 	}
// 
// 	rtry
// 	{
// 		JSONNODE* rootNode = json_new(JSON_NODE);
// 		rverify(rootNode, catchall, );
// 
// 		if ( itemData->GetItemType() == cCommerceItemData::ITEM_TYPE_CATEGORY )
// 		{
// 			const cCommerceCategoryData* catData = static_cast<const cCommerceCategoryData*>(itemData);
// 			
// 			json_push_back(rootNode, json_new_a("Type", "Category"));
// 			json_push_back(rootNode, json_new_a("Id", catData->GetId()));
// 			json_push_back(rootNode, json_new_a("Name", catData->GetName()));
// 		}
// 		else if ( itemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT )
// 		{
// 			const cCommerceProductData* productData = static_cast<const cCommerceProductData*>(itemData);		
// 	
// 			json_push_back(rootNode, json_new_a("Type", "Product"));
// 			json_push_back(rootNode, json_new_a("Id", productData->GetId()));
// 			json_push_back(rootNode, json_new_a("Name", productData->GetName()));
// 			json_push_back(rootNode, json_new_a("Desc", mp_CommerceUtil->GetItemLongDescription(productData)));
// 
// 			const int priceStringLen = 256;
// 			char priceString[priceStringLen];
// 			priceString[0] = 0;
// 			float price = mp_CommerceUtil->GetProductPrice(productData,priceString,priceStringLen);
// 			json_push_back(rootNode, json_new_a("PriceString", priceString));
// 			json_push_back(rootNode, json_new_f("Price", price));
// 			json_push_back(rootNode, json_new_b("Purchased", mp_CommerceUtil->IsProductPurchased(productData)));
// 			json_push_back(rootNode, json_new_b("Installed", mp_CommerceUtil->IsProductInstalled(productData)));
// 			json_push_back(rootNode, json_new_b("Consumable", productData->IsConsumable()));
// 
// 			if ( productData->GetCategoryMemberships().GetCount() > 0 )
// 			{
// 				JSONNODE* categoryMembershipArray= json_new(JSON_ARRAY);
// 				json_set_name(categoryMembershipArray, "CategoryMemberships");
// 
// 				for ( int iCatMems = 0; iCatMems < productData->GetCategoryMemberships().GetCount(); iCatMems++ )
// 				{
// 					JSONNODE* catMemNode = json_new(JSON_NODE);
// 					rverify(catMemNode, catchall, );
// 
// 					json_push_back(catMemNode, json_new_a("MemberOf", productData->GetCategoryMemberships()[iCatMems]));
// 
// 					json_push_back(categoryMembershipArray, catMemNode);
// 				}
// 
// 				json_push_back(rootNode, categoryMembershipArray);
// 			}
// 
// 			if ( productData->GetImagePaths().GetCount() > 0 )
// 			{
// 				JSONNODE* imagePathArray= json_new(JSON_ARRAY);
// 				json_set_name(imagePathArray, "ImagePaths");
// 
// 				for ( int iImagePaths = 0; iImagePaths < productData->GetImagePaths().GetCount(); iImagePaths++ )
// 				{
// 					JSONNODE* imagePathNode = json_new(JSON_NODE);
// 					rverify(imagePathNode, catchall, );
// 
// 					json_push_back(imagePathNode, json_new_a("ImagePath", productData->GetImagePaths()[iImagePaths]));
// 
// 					json_push_back(imagePathArray, imagePathNode);
// 				}
// 
// 				json_push_back(rootNode, imagePathArray);
// 			}
// 		}
// 		else
// 		{
// 			json_push_back(rootNode, json_new_a("Type", "Invalid"));
// 		}
// 
// 
// 		json_char* jc = json_write_formatted(rootNode);
// 
// 		json_delete(rootNode);
// 
// 		return jc;
// 	}
// 	rcatchall
// 	{
// 
// 	}
// 
// 	return NULL;
}

RGSC_HRESULT RGSC_CALL RgscCommerceManager::ShowCommerceStore( const char* product, const char* category )
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		JSONNODE *n = json_new(JSON_NODE);
		rverify(n, catchall, );

		json_set_name(n, "Data");

		if(product && (product[0] != '\0'))
		{
			// a NULL URL tells the SCUI to bring up an empty panel with a spinner
			// until a second call to this function sends it the real URL.
			json_push_back(n, json_new_a("Product", product));
		}

		if(category && (category[0] != '\0'))
		{
			// a NULL URL tells the SCUI to bring up an empty panel with a spinner
			// until a second call to this function sends it the real URL.
			json_push_back(n, json_new_a("Category", category));
		}


		rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(true,
			RgscUi::UI_STATE_COMMERCE,
			RgscUi::UI_SUBSTATE_NONE,
			n),
			catchall,
			hr = RGSC_FAIL);

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}


const char* RGSC_CALL RgscCommerceManager::GetUserAgeAppropriateData()
{	
	rtry
	{
		//Early out for situations where Javascript really shouldnt be calling this anyway
		const rlRosCredentials& creds = rlRos::GetCredentials( m_LocalGamerIndex );
		if(!creds.IsValid())
		{
			
				JSONNODE* rootNode = json_new(JSON_NODE);
				rverify(rootNode, catchall, );
				json_push_back(rootNode, json_new_b("Valid", false));
				json_push_back(rootNode, json_new_b("AgeAppropriate", false ));
				json_char* jc = json_write_formatted(rootNode);
				json_delete(rootNode);

				return jc;
		}

		switch(m_CurrentAgeDataState)
		{
		case(AGE_DATA_STATE_INIT):
			//Start a fetch of age data for the currently signed in user
			//Content is currently unused
			rlRosEntitlement::GetValidateContentPlayerAge( m_LocalGamerIndex, "", &m_UserAgeDataFetchStatus );
			m_CurrentAgeDataState = AGE_DATA_STATE_FETCHING;
			break;
		case(AGE_DATA_STATE_FETCHING):
			if ( m_UserAgeDataFetchStatus.Pending() )
			{
				m_CurrentAgeDataState = AGE_DATA_STATE_FETCHING;
			}
			else if ( m_UserAgeDataFetchStatus.Succeeded() )
			{
				m_CurrentAgeDataState = AGE_DATA_STATE_APPROVED;
			}
			else if ( m_UserAgeDataFetchStatus.Failed() )
			{
				m_CurrentAgeDataState = AGE_DATA_STATE_DECLINED;
			}
			break;
		default:
			break;
		}
	
		JSONNODE* rootNode = json_new(JSON_NODE);
		rverify(rootNode, catchall, );

		bool dataReturned = (m_CurrentAgeDataState == AGE_DATA_STATE_APPROVED || m_CurrentAgeDataState == AGE_DATA_STATE_DECLINED);

		json_push_back(rootNode, json_new_b("Valid", dataReturned));
		json_push_back(rootNode, json_new_b("AgeAppropriate", m_CurrentAgeDataState == AGE_DATA_STATE_APPROVED));

		json_char* jc = json_write_formatted(rootNode);

		json_delete(rootNode);

		return jc;
	}
	rcatchall
	{
		return NULL;
	}
}

const char* RGSC_CALL RgscCommerceManager::RequestVoucherContent( const char* parameterJson )
{
	rtry
	{	
		if ( !m_VoucherRedemptionStatus.None() )
		{
			return RESULT_FALSE_JSON_RESPONSE;
		}

		JSONNODE *n = json_parse(parameterJson);

		rverify(n != NULL, catchall,  );
		
		JSONNODE_ITERATOR i = json_begin(n);

		char* voucherCode = NULL;
		
		char* languageCode = NULL;

		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return false;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "voucherCode") == 0)
			{
				voucherCode = json_as_string_or_null(*i);
			}

			if(_stricmp(node_name, "language") == 0)
			{
				languageCode = json_as_string_or_null(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		rverify(voucherCode != NULL, catchall,  );

		json_delete(n);

		m_VoucherPreviewData.Reset();
		if (rlRosEntitlement::GetVoucherContent( m_LocalGamerIndex, voucherCode, languageCode ? languageCode: "en", &m_VoucherPreviewData, &m_VoucherRedemptionStatus ))
		{
			json_free_safe(languageCode);
			json_free_safe(voucherCode);
			m_CurrentVoucherRedemptionState = VOUCHER_REDEMPTION_GETTING_CONTENTS;
			return RESULT_TRUE_JSON_RESPONSE; 
		}
		else
		{
			json_free_safe(languageCode);
			json_free_safe(voucherCode);
			m_VoucherRedemptionStatus.Reset();
			return RESULT_FALSE_JSON_RESPONSE;
		}
		
	}
	rcatchall
	{
		return RESULT_FALSE_JSON_RESPONSE;
	}
	
}

const char* RGSC_CALL RgscCommerceManager::RequestVoucherConsumption( const char* parameterJson )
{
	rtry
	{
		rverify(parameterJson, catchall, );

		if ( !m_VoucherRedemptionStatus.None() )
		{
			return RESULT_FALSE_JSON_RESPONSE;
		}

		JSONNODE *n = json_parse(parameterJson);

		rverify(n != NULL, catchall,  );

		JSONNODE_ITERATOR i = json_begin(n);

		char* voucherCode = NULL;

		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return false;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "voucherCode") == 0)
			{
				voucherCode = json_as_string_or_null(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);

		rverify(voucherCode != NULL, catchall,  );

		if (rlRosEntitlement::ConsumeVoucher( m_LocalGamerIndex, voucherCode, m_VoucherPreviewData.m_GrantTokenVersion, m_VoucherPreviewData.m_GrantToken.c_str() , &m_VoucherRedemptionStatus ))
		{
			json_free_safe(voucherCode);
			m_CurrentVoucherRedemptionState = VOUCHER_REDEMPTION_CONSUMING;
			return RESULT_TRUE_JSON_RESPONSE; 
		}
		else
		{
			json_free_safe(voucherCode);
			m_VoucherRedemptionStatus.Reset();
			return RESULT_FALSE_JSON_RESPONSE;
		}
	}
	rcatchall
	{
		return RESULT_FALSE_JSON_RESPONSE;
	}
}

void RgscCommerceManager::UpdateVouchingConsuming()
{
	rtry
	{
		if ( m_VoucherRedemptionStatus.Pending() )
		{
			return;
		}

		if ( m_VoucherRedemptionStatus.Succeeded() )
		{
			//Build the json response
			JSONNODE* rootNode = json_new(JSON_NODE);
			rverify(rootNode, catchall, );

			json_push_back(rootNode, json_new_b("Result", true));

			//Granted entitlements
			JSONNODE* grantedEntitlementsArray = json_new(JSON_ARRAY);
			rverify(grantedEntitlementsArray, catchall, );
			//@@: location COMMERCEMANAGER_UPDATEVOUCHINGCONSUMING_SUCCEDED_SET_ENTITLEMENTS_ARRAY
			json_set_name(grantedEntitlementsArray, "EntitlementsGranted");
			
			for (int i = 0; i < m_VoucherPreviewData.m_EntitlementsToGrant.GetCount(); i++)
			{
				JSONNODE* entitlementNode = json_new(JSON_NODE);
				json_push_back(entitlementNode, json_new_i("Count",m_VoucherPreviewData.m_EntitlementsToGrant[i].m_Count));
				json_push_back(entitlementNode, json_new_a("FriendlyName",m_VoucherPreviewData.m_EntitlementsToGrant[i].m_FriendlyName));
				json_push_back(entitlementNode, json_new_a("EntitlementCode",m_VoucherPreviewData.m_EntitlementsToGrant[i].m_FriendlyName));
				json_push_back(grantedEntitlementsArray, entitlementNode);
			}

			json_push_back(rootNode, grantedEntitlementsArray);

			json_char* jc = json_write_formatted(rootNode);
			json_delete(rootNode);

			bool succeeded = false;

			rgsc::Script::JsVoucherConsumeResult(&succeeded, jc);
			m_CurrentVoucherRedemptionState = VOUCHER_REDEMPTION_NEUTRAL;
			m_VoucherRedemptionStatus.Reset();
			m_VoucherPreviewData.Reset();
			json_free_safe(jc);

			// refresh the SCS ticket - some vouchers change privileges (eg. entitlement, etc.)
			rlRos::RenewCredentials(0);
		}

		if ( m_VoucherRedemptionStatus.Failed() )
		{
			//Build the json response
			JSONNODE* rootNode = json_new(JSON_NODE);
			rverify(rootNode, catchall, );

			json_push_back(rootNode, json_new_b("Result", false));

			//Add fail reason data
			json_char* jc = json_write_formatted(rootNode);
			json_delete(rootNode);

			bool succeeded = false;
			rgsc::Script::JsVoucherConsumeResult(&succeeded, jc);

			m_CurrentVoucherRedemptionState = VOUCHER_REDEMPTION_NEUTRAL;
			m_VoucherRedemptionStatus.Reset();
			//@@: location COMMERCEMANAGER_UPDATEVOUCHINGCONSUMING_FAILED_PREVIEW_DATA
			m_VoucherPreviewData.Reset();

			json_free_safe(jc);

		}
	}
	rcatchall
	{

	}
}

void RgscCommerceManager::UpdateVoucherGettingContents()
{
	rtry
	{
		if ( m_VoucherRedemptionStatus.Pending() )
		{
			return;
		}

		if ( m_VoucherRedemptionStatus.Succeeded() )
		{
			//Build the json response
			JSONNODE* rootNode = json_new(JSON_NODE);
			rverify(rootNode, catchall, );

			//@@: location COMMERCEMANAGER_UPDATEVOUCHERGETTINGCONTENTS_PUSH_RESULT
			json_push_back(rootNode, json_new_b("Result", true));
				
			JSONNODE* offerArray = json_new(JSON_ARRAY);
			rverify(offerArray, catchall, );
			json_set_name(offerArray, "Offers");

			for ( int i = 0; i < m_VoucherPreviewData.m_Offers.GetCount(); i++  )
			{
				JSONNODE* offerNode = json_new(JSON_NODE);
				json_push_back(offerNode, json_new_a("OfferCode",m_VoucherPreviewData.m_Offers[i].m_Code) );
				json_push_back(offerNode, json_new_a("FriendlyName",m_VoucherPreviewData.m_Offers[i].m_FriendlyName) );

				JSONNODE* entitlementArray = json_new(JSON_ARRAY);
				json_set_name(entitlementArray, "Entitlements");

				for (int j = 0; j < m_VoucherPreviewData.m_Offers[i].m_EntitlementDataArray.GetCount(); j++)
				{
					JSONNODE* entitlementNode = json_new(JSON_NODE);
					json_push_back(entitlementNode, json_new_i("Count",m_VoucherPreviewData.m_Offers[i].m_EntitlementDataArray[j].m_Count));
					json_push_back(entitlementNode, json_new_a("FriendlyName",m_VoucherPreviewData.m_Offers[i].m_EntitlementDataArray[j].m_FriendlyName));
					json_push_back(entitlementNode, json_new_a("EntitlementCode",m_VoucherPreviewData.m_Offers[i].m_EntitlementDataArray[j].m_Code));
                    json_push_back(entitlementNode, json_new_b("Visible",m_VoucherPreviewData.m_Offers[i].m_EntitlementDataArray[j].m_Visible));
					json_push_back(entitlementArray, entitlementNode);
				}
				
				json_push_back(offerNode, entitlementArray);

				json_push_back(offerArray, offerNode);
			}
			
			json_push_back(rootNode, offerArray);

			//Granted entitlements
			JSONNODE* grantedEntitlementsArray = json_new(JSON_ARRAY);
			rverify(grantedEntitlementsArray, catchall, );
			json_set_name(grantedEntitlementsArray, "EntitlementsGranted");

			for (int i = 0; i < m_VoucherPreviewData.m_EntitlementsToGrant.GetCount(); i++)
			{
				JSONNODE* entitlementNode = json_new(JSON_NODE);
				json_push_back(entitlementNode, json_new_i("Count",m_VoucherPreviewData.m_EntitlementsToGrant[i].m_Count));
				json_push_back(entitlementNode, json_new_a("FriendlyName",m_VoucherPreviewData.m_EntitlementsToGrant[i].m_FriendlyName));
				json_push_back(entitlementNode, json_new_a("EntitlementCode",m_VoucherPreviewData.m_EntitlementsToGrant[i].m_Code));
                json_push_back(entitlementNode, json_new_b("Visible",m_VoucherPreviewData.m_EntitlementsToGrant[i].m_Visible));
				json_push_back(grantedEntitlementsArray, entitlementNode);
			}

			json_push_back(rootNode, grantedEntitlementsArray);

 			json_char* jc = json_write_formatted(rootNode);
			json_delete(rootNode);

			bool succeeded = false;
			rgsc::Script::JsVoucherContentResult(&succeeded, jc);

			json_free_safe(jc);

			m_CurrentVoucherRedemptionState = VOUCHER_REDEMPTION_NEUTRAL;
			m_VoucherRedemptionStatus.Reset();
			
		}

		if ( m_VoucherRedemptionStatus.Failed() )
		{
			//Build the json response
			JSONNODE* rootNode = json_new(JSON_NODE);
			rverify(rootNode, catchall, );

			json_push_back(rootNode, json_new_b("Result", false));

			//RLEN_ERROR_AUTHENTICATIONFAILED_TICKET --> Needs new ID
			//RLEN_ERROR_SERIAL_DOES_NOT_EXIST --> i39
			//RLEN_ERROR_NOT_ENTITLED  ----> Needs new string id
			//RLEN_ERROR_SERIAL_ALREADY_ACTIVATED --> i38
			//RLSC_ERROR_UNEXPECTED_RESULT --> i40

			

			//Add fail reason data
			json_push_back(rootNode, json_new_i("ErrorStringId", ConvertVoucherResultCodeToStringId(m_VoucherRedemptionStatus.GetResultCode()) ));

			json_char* jc = json_write_formatted(rootNode);
			json_delete(rootNode);

			bool succeeded = false;
			rgsc::Script::JsVoucherContentResult(&succeeded, jc);

			m_CurrentVoucherRedemptionState = VOUCHER_REDEMPTION_NEUTRAL;
			m_VoucherRedemptionStatus.Reset();

			json_free_safe(jc);
		}
	}
	rcatchall
	{

	}
	
}

void RgscCommerceManager::UpdateVoucherRedemption()
{
	switch( m_CurrentVoucherRedemptionState )
	{
	case(VOUCHER_REDEMPTION_GETTING_CONTENTS):
		UpdateVoucherGettingContents();
		break;
	case(VOUCHER_REDEMPTION_CONSUMING):
		UpdateVouchingConsuming();
		break;
	}
}

void RgscCommerceManager::UpdateManifestFetch()
{
    rtry
    {
        if (m_CommerceManifestFetchStatus.Pending() || m_CommerceManifestFetchStatus.None())
        {
            return;
        }

        //The legacy manifest fetch
        if (m_CurrentCommerceManifestFetchState == COMMERCE_MANIFEST_FETCH_FETCHING)
        {
            //@@: location COMMERCEMANAGER_UPDATE_FETCHING
            if ( m_CommerceManifestFetchStatus.Succeeded() )
            {
                m_LastCommerceManifestFetchFailed = false;
                m_CurrentCommerceManifestFetchState = COMMERCE_MANIFEST_FETCH_NEUTRAL;
            }

            if ( m_CommerceManifestFetchStatus.Failed() )
            {
                m_LastCommerceManifestFetchFailed = true;
                m_CurrentCommerceManifestFetchState = COMMERCE_MANIFEST_FETCH_NEUTRAL;
            }
        }

        if (m_CommerceManifestFetchStatus.Succeeded())
        {                   
            bool succeeded = true;
            rgsc::Script::JsCommerceManifestRequestResult(&succeeded, m_CommerceManifest.c_str());
             
            m_CommerceManifestFetchStatus.Reset();
        }
        else
        {
            //Build the json response
            JSONNODE* rootNode = json_new(JSON_NODE);
            rverify(rootNode, catchall, );

            json_push_back(rootNode, json_new_b("Result", false));

            //Add fail reason data
            json_push_back(rootNode, json_new_i("ErrorCode", m_CommerceManifestFetchStatus.GetResultCode()));

            json_char* jc = json_write_formatted(rootNode);
            json_delete(rootNode);

            bool succeeded = false;
            rgsc::Script::JsCommerceManifestRequestResult(&succeeded, jc);
            
            m_CommerceManifestFetchStatus.Reset();

            json_free_safe(jc);
        }        
    }
    rcatchall
    {
        bool succeeded = false;
        rgsc::Script::JsCommerceManifestRequestResult(&succeeded, RESULT_FALSE_JSON_RESPONSE);
        m_CommerceManifestFetchStatus.Reset();
    }
}

bool RGSC_CALL RgscCommerceManager::ShowSteamStore()
{
	if ( m_CurrentSteamStoreOpenCallback != NULL )
	{
		u32 steamAppId = GetRgscConcreteInstance()->GetTitleId()->GetSteamAppId();
		m_CurrentSteamStoreOpenCallback( steamAppId );

		return true;
	}

	return false;
}

RGSC_HRESULT RGSC_CALL RgscCommerceManager::SetSteamStoreOpenCallback(SteamStoreOpenCallback newCallback)
{
	m_CurrentSteamStoreOpenCallback = newCallback;	

	return RGSC_OK;
}

const char* RGSC_CALL RgscCommerceManager::RequestUrlForCheckout( const char *parameterJson )
{
	rtry
	{
		rverify( m_CommerceManifest.GetLength() > 0, catchall, );
		JSONNODE *n = json_parse(parameterJson);

		rverify(n, catchall, );

		char* productId = NULL;
		char* language = NULL;
		char* country = NULL;

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return RESULT_FALSE_JSON_RESPONSE;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "ProductId") == 0 && productId == NULL)
			{
				productId = json_as_string_or_null(*i);
			}

			if(_stricmp(node_name, "Country") == 0 && country == NULL)
			{
				country = json_as_string_or_null(*i);
			}

			if(_stricmp(node_name, "Language") == 0 && language == NULL)
			{
				language = json_as_string_or_null(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		rverify(productId,catchall,);

		json_delete(n);

		//Get the item node
		JSONNODE *m = json_parse(m_CommerceManifest);
		JSONNODE *productArrayNode = json_get(m,"Products");

		rverify(productArrayNode,catchall,);

		
		JSONNODE *requestedProductNode = NULL;
		JSONNODE_ITERATOR productIt = json_begin(productArrayNode);
		while(productIt != json_end(productArrayNode))
		{
			rverify(*productIt != NULL,catchall,);
			
			JSONNODE *searchNode = *productIt;

			JSONNODE *nameNode = json_get(searchNode,"Id");

			
		

			if (nameNode)
			{
				char* idVal = json_as_string_or_null(nameNode);

				if ( idVal && strcmp(idVal, productId) == 0 )
				{
					requestedProductNode = searchNode;
					break;
				}
				json_free_safe(idVal);
			}
			
			++productIt;
		}

		

		rverify(requestedProductNode,catchall,);

		char *priceId = NULL;
		JSONNODE* priceIdNode = json_get(requestedProductNode,"PriceId");
		
		rverify(priceIdNode,catchall,);
		priceId = json_as_string_or_null(priceIdNode);

		rverify(priceId,catchall,);

		json_delete(m);
		
		bool retval = rlRosEntitlement::GetCheckoutUrl( m_LocalGamerIndex, productId, priceId, language ? language : "en", country ? country : "us", m_CheckoutUrlFetchBuffer, COMMERCE_URL_MAX_LENGTH , &m_CheckoutUrlFetchStatus );

		json_free_safe(productId);
		json_free_safe(country);
		json_free_safe(language);
		json_free_safe(priceId);

		if (retval)
		{
			m_CheckoutUrlFetchState = CHECKOUT_URL_FETCH_FETCHING;
			return RESULT_TRUE_JSON_RESPONSE; 
		}
		else
		{
			m_CheckoutUrlFetchStatus.Reset();
			return RESULT_FALSE_JSON_RESPONSE;
		}
		
	}
	rcatchall
	{
		return RESULT_FALSE_JSON_RESPONSE;
	}
}

void RgscCommerceManager::UpdateCheckoutUrlFetch()
{
	rtry
	{
		if ( m_CheckoutUrlFetchState == CHECKOUT_URL_FETCH_NEUTRAL )
		{
			return;
		}

		if ( m_CheckoutUrlFetchStatus.Pending() )
		{
			return;
		}

		if ( m_CheckoutUrlFetchStatus.Succeeded() )
		{
			//Build the json response
			JSONNODE* rootNode = json_new(JSON_NODE);
			rverify(rootNode, catchall, );

			json_push_back(rootNode, json_new_b("Result", true));
			json_push_back(rootNode, json_new_a("Url",m_CheckoutUrlFetchBuffer));

			json_char* jc = json_write_formatted(rootNode);
			json_delete(rootNode);

			bool succeeded = true;
			rgsc::Script::JsUrlRequestRequestResult(&succeeded, jc);

			m_CheckoutUrlFetchState = CHECKOUT_URL_FETCH_NEUTRAL;
			m_VoucherRedemptionStatus.Reset();

			json_free_safe(jc);
		}

		if ( m_CheckoutUrlFetchStatus.Failed() )
		{
			bool succeeded = false;
			rgsc::Script::JsUrlRequestRequestResult(&succeeded, RESULT_FALSE_JSON_RESPONSE);
		}


		m_CheckoutUrlFetchState = CHECKOUT_URL_FETCH_NEUTRAL;
		m_CheckoutUrlFetchStatus.Reset();
	}
	rcatchall
	{
		bool succeeded = false;
		rgsc::Script::JsUrlRequestRequestResult(&succeeded, RESULT_FALSE_JSON_RESPONSE);
		m_CheckoutUrlFetchState = CHECKOUT_URL_FETCH_NEUTRAL;
		m_CheckoutUrlFetchStatus.Reset();
	}

}

const char* RGSC_CALL RgscCommerceManager::IsProductInstalled(const char* parameterJson)
{
	rtry
	{

		if ( m_CommerceManifest.GetLength() == 0 )
		{
			return RESULT_FALSE_JSON_RESPONSE;
		}

		if ( m_IsContainedPackageInstalledCallback == NULL )
		{
			return RESULT_FALSE_JSON_RESPONSE;
		}

		if ( parameterJson == NULL )
		{
			return RESULT_FALSE_JSON_RESPONSE;
		}
			
		JSONNODE *n = json_parse(parameterJson);

		rverify(n, catchall, );

		const char* productId = NULL;

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return RESULT_FALSE_JSON_RESPONSE;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "productId") == 0)
			{
				productId = json_as_string_or_null(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);

		rverify(productId,catchall,);

		//Get the item node
		JSONNODE *m = json_parse(m_CommerceManifest);
		JSONNODE *productArrayNode = json_get(m,"Products");
		rverify(productArrayNode,catchall,);


		JSONNODE *requestedProductNode = NULL;
		JSONNODE_ITERATOR productIt = json_begin(productArrayNode);
		while(productIt != json_end(productArrayNode))
		{
			rverify(*productIt != NULL,catchall,);

			JSONNODE *searchNode = *productIt;

			JSONNODE *nameNode = json_get(searchNode,"Id");

			if (nameNode)
			{
				char* idVal = json_as_string_or_null(nameNode);

				if ( idVal && strcmp(idVal, productId) == 0 )
				{
					requestedProductNode = searchNode;
					break;
				}

				json_free_safe(idVal);
			}

			++productIt;
		}

		rverify(requestedProductNode,catchall,);

		JSONNODE* containedPackageNode = json_get(requestedProductNode,"ContainedPackageNames");
		rverify(containedPackageNode,catchall,);
		JSONNODE_ITERATOR containedIt = json_begin(containedPackageNode);
		
		int numContainedIds = 0;
		bool allInstalled = true;
		while(containedIt != json_end(containedPackageNode))
		{
			
			char* containedPackageName = json_as_string_or_null(*containedIt);
			if (containedPackageName)
			{
				numContainedIds++;
				bool isPackageInstalled = m_IsContainedPackageInstalledCallback(containedPackageName);

				if (allInstalled)
				{
					//So if the package is installed we still read as all installed, otherwise we switch to the no response
					allInstalled = isPackageInstalled;
				}

				json_free_safe(containedPackageName);
			}
			containedIt++;
		}

		if ( numContainedIds )
		{
			//If there were no IDs to check we return a false result.
			allInstalled = false;
		}

		json_delete(m);

		if (allInstalled)
		{
			return RESULT_TRUE_JSON_RESPONSE;
		}
		else
		{
			return RESULT_FALSE_JSON_RESPONSE;
		}


	}
	rcatchall
	{
		return RESULT_FALSE_JSON_RESPONSE;
	}
}

RGSC_HRESULT RGSC_CALL RgscCommerceManager::SetContainedPackageInstalledCallback(IsContainedPackageInstalledCallback newCallback)
{
	m_IsContainedPackageInstalledCallback = newCallback;
	return RGSC_OK;
}

const char* RGSC_CALL RgscCommerceManager::StartSteamPurchase( const char* parameterJson )
{
	char* offerId = NULL;
	char* language = NULL;

	rtry
	{
		JSONNODE *n = json_parse(parameterJson);

		rverify(n, catchall, );

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return RESULT_FALSE_JSON_RESPONSE;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "ProductId") == 0 && offerId == NULL)
			{
				offerId = json_as_string_or_null(*i);
			}

			if(_stricmp(node_name, "Language") == 0 && language == NULL)
			{
				language = json_as_string_or_null(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);
		
		rverify(offerId,catchall,);
		rverify(!m_StartSteamTransactionStatus.Pending(),catchall,);

        m_StartSteamTransactionStatus.Reset();
		rlRosEntitlement::BeginSteamPurchase(m_LocalGamerIndex, GetRgscConcreteInstance()->GetTitleId()->GetSteamAppId(), offerId, GetRgscConcreteInstance()->GetTitleId()->GetSteamId(), language ? language : "en", &m_StartSteamTransactionStatus);
		json_free_safe(offerId);
		json_free_safe(language);

		return RESULT_TRUE_JSON_RESPONSE;
	}
	rcatchall
	{
		json_free_safe(offerId);
		json_free_safe(language);

		return RESULT_FALSE_JSON_RESPONSE;
	}
}

#if !PRELOADED_SOCIAL_CLUB 
bool RGSC_CALL RgscCommerceManager::RequestEntitlementData(const char* machineHash, const char* language)
{
	rtry
	{
		rverify(machineHash,catchall,);
		rverify(language,catchall,);

		if ( m_EntitlementStatus.Pending() )
		{
			return false;
		}

		m_EntitlementDataError = false;
		m_EntitlementDataPopulated = false;

		m_EntitlementData.Reset();

		return rlRosEntitlement::GetEntitlements(m_LocalGamerIndex, language, machineHash, &m_EntitlementData, &m_EntitlementStatus );
	}
	rcatchall
	{
		return false;
	}
}

bool RGSC_CALL RgscCommerceManager::IsEntitlementDataValid()
{
	return m_EntitlementDataPopulated;
}

const char* RGSC_CALL RgscCommerceManager::GetEntitlementDataAsJson()
{
	rtry
	{
		rverify(m_EntitlementDataPopulated,catchall,);
		rverify(!m_EntitlementDataError,catchall,);

		//Build the json response
		JSONNODE* rootNode = json_new(JSON_NODE);
		rverify(rootNode, catchall, );

		JSONNODE* entitlementArray = json_new(JSON_ARRAY);
		rverify(entitlementArray, catchall, );
		json_set_name(entitlementArray, "Entitlements");

		for (int i = 0; i < m_EntitlementData.m_EntitlementInstanceArray.GetCount(); i++)
		{
			JSONNODE* entitlementNode = json_new(JSON_NODE);
			json_push_back(entitlementNode, json_new_i("Count",m_EntitlementData.m_EntitlementInstanceArray[i].m_Count));
			json_push_back(entitlementNode, json_new_a("EntitlementCode",m_EntitlementData.m_EntitlementInstanceArray[i].m_Code));
			json_push_back(entitlementNode, json_new_a("FriendlyName",m_EntitlementData.m_EntitlementInstanceArray[i].m_FriendlyName));
			
			json_push_back(entitlementArray, entitlementNode);
		}

		json_push_back(rootNode, json_new_a("Signature",m_EntitlementData.m_Signature));
		json_push_back(rootNode, json_new_a("MachineHash",m_EntitlementData.m_MachineHash));

		json_push_back(rootNode, entitlementArray);

		json_char* jc = json_write_formatted(rootNode);
		json_delete(rootNode);
		return jc;
	}
	rcatchall
	{
		return NULL;
	}
}

bool RGSC_CALL RgscCommerceManager::IsEntitlementDataInErrorState()
{
	return m_EntitlementDataError;
}

void RgscCommerceManager::UpdateGetEntitlement()
{
	rtry
	{
		rverify(true,catchall,);
		if ( m_EntitlementStatus.Pending() )
		{
			//@@: location COMMERCEMANAGER_UPDATEGETENTITLEMENT_PENDING_STATUS
			return;
		}

		if ( m_EntitlementStatus.Succeeded() )
		{
			m_EntitlementDataPopulated = true;
			m_EntitlementDataError = false;
		}

		if ( m_EntitlementStatus.Failed() )
		{
			m_EntitlementDataPopulated = false;
			m_EntitlementDataError = true;
		}

		m_EntitlementStatus.Reset();
	}
	rcatchall
	{
		
	}
}


class RequestEntitlementBlockTask : public SimpleOperationBaseTask<RgscCommerceManager>
{
public:
	RequestEntitlementBlockTask():
	  m_BufferSize(0),
	  m_OutputBuffer(NULL),
	  m_MachineHash(NULL),
	  m_OutputDataSize(NULL)
	{
		//@@: location REQUESTENTITLEMENTBLOCKTASK_CONSTRUCTOR
	}

	bool Configure( RgscCommerceManager* ctx, const RockstarId rockstarId,  const char* machineHash, const char* locale, char* outputBuffer, unsigned int bufferSize, int* outputVersion, unsigned int* outputDataSize )
	{
		m_MachineHash =  machineHash;
		m_OutputBuffer = outputBuffer;
		//@@: location REQUESTENTITLEMENTBLOCKTASK_CONFIGURE
		m_BufferSize = bufferSize;
		m_OutputVersion = outputVersion;
		m_OutputDataSize = outputDataSize;
		m_Locale = locale;

		return SimpleOperationBaseTask<RgscCommerceManager>::Configure(ctx,rockstarId);
	}
private:
	unsigned int m_BufferSize;
	char* m_OutputBuffer;
	const char* m_MachineHash;
	const char* m_Locale;
	int* m_OutputVersion;
	unsigned int* m_OutputDataSize;
	
protected:
	virtual bool PerformOperation()
	{
		//@@: location REQUESTENTITLEMENTBLOCKTASK_PERFORMOPERATION
		return rlRosEntitlement::GetEntitlementBlock(0, m_MachineHash, m_Locale, m_OutputBuffer, m_BufferSize, m_OutputVersion, m_OutputDataSize, &m_MyStatus);
	}

	virtual void ProcessResult(bool /*success*/)
	{
		
	}
};

bool RGSC_CALL RgscCommerceManager::RequestEntitlementBlock(const char* machineHash, const char* locale, char* outputBuffer, unsigned int bufferSize, int* outputVersion, unsigned int* outputDataSize, IAsyncStatus* status)
{
	RLPC_CREATETASK_IASYNC(RequestEntitlementBlockTask, 0, machineHash, locale, outputBuffer, bufferSize,outputVersion, outputDataSize);
}

bool RGSC_CALL RgscCommerceManager::DoesEntitlementBlockGrantEntitlement(const RockstarId rockstarId, const char* machineHash, const char* entitlementId, void* dataBuffer, unsigned int dataBufferSize)
{
	return rlRosEntitlement::DoesDataBlockGrantEntitlement((long)rockstarId, machineHash, dataBuffer,dataBufferSize,entitlementId);
}

u8* RGSC_CALL RgscCommerceManager::AllocateOfflineEntitlementBlob(unsigned size)
{
	return rlRosEntitlement::AllocateOfflineEntitlementBlob(size);
}

bool RGSC_CALL RgscCommerceManager::SaveOfflineEntitlement(u8* entitlementBuf, unsigned entitlementBufLen, u8* machineHash)
{
	return rlRosEntitlement::SaveOfflineEntitlement(entitlementBuf, entitlementBufLen, machineHash);
}

bool RGSC_CALL RgscCommerceManager::LoadOfflineEntitlement(u8* machineHash)
{
	return rlRosEntitlement::LoadOfflineEntitlement(machineHash);
}

bool RGSC_CALL RgscCommerceManager::HasOfflineEntitlement()
{
	return rlRosEntitlement::HasOfflineEntitlement();
}

u8* RGSC_CALL RgscCommerceManager::GetOfflineEntitlement()
{
	return rlRosEntitlement::GetOfflineEntitlement();
}

unsigned RGSC_CALL RgscCommerceManager::GetOfflineEntitlementLength()
{
	return rlRosEntitlement::GetOfflineEntitlementLength();
}

bool RGSC_CALL RgscCommerceManager::ClearOfflineEntitlement()
{
	return rlRosEntitlement::ClearOfflineEntitlement();
}

bool RGSC_CALL RgscCommerceManager::IsEntitlementIOInProgress()
{
	return rlRosEntitlement::IsEntitlementIOInProgress();
}

void RGSC_CALL RgscCommerceManager::SetPurchaseFlowTelemetryCallback(PurchaseFlowTelemetryCallback callback)
{
#if !PRELOADED_SOCIAL_CLUB
	m_PurchaseFlowTelemetryCallback = callback;
#endif
}

const char* RgscCommerceManager::GetPurchaseFlowTelemetry()
{
#if !PRELOADED_SOCIAL_CLUB
	rtry
	{
		rcheck(m_PurchaseFlowTelemetryCallback, catchall, );

		char telemetry[PURCHASE_FLOW_TELEMETY_BUF_SIZE] = {0};
		m_PurchaseFlowTelemetryCallback(telemetry, sizeof(telemetry));

		// parse the string into a json node, then format it to sanitize/escape the json
		JSONNODE *n = json_parse(telemetry);
		rverify(n, catchall, );

		json_char *jc = json_write_formatted(n);

		json_delete(n);

		return jc;
	}
	rcatchall
	{

	}
#endif

	return "";
}

class RequestEntitlementUrlTask : public SimpleOperationBaseTask<RgscCommerceManager>
{
public:
	RequestEntitlementUrlTask():
	  m_OutputBufferSize(0),
		  m_OutputBuffer(NULL),
		  m_LocalUserIndex()
	  {

	  }

	  bool Configure( RgscCommerceManager* ctx, const RockstarId rockstarId,  const char* entitlementCode, char* outputBuffer, unsigned int outputBufferSize )
	  {
		  m_EntitlementCode =  entitlementCode;
		  m_OutputBuffer = outputBuffer;
		  m_OutputBufferSize = outputBufferSize;
		 
		  return SimpleOperationBaseTask<RgscCommerceManager>::Configure(ctx,rockstarId);
	  }
private:
	unsigned int m_OutputBufferSize;
	char* m_OutputBuffer;
	atString m_EntitlementCode;
	int m_LocalUserIndex;

protected:
	virtual bool PerformOperation()
	{
		return rlRosEntitlement::GetEntitlementUrl(0, m_EntitlementCode, m_OutputBuffer, m_OutputBufferSize, &m_MyStatus);
	}

	virtual void ProcessResult(bool /*success*/)
	{

	}
};


bool RGSC_CALL RgscCommerceManager::GetEntitlementDownloadUrl(const char* entitlementCode, char* outputBuffer, unsigned int outputBufferLength, IAsyncStatus* status)
{
	RLPC_CREATETASK_IASYNC(RequestEntitlementUrlTask, 0, entitlementCode, outputBuffer, outputBufferLength);	
}


bool RGSC_CALL RgscCommerceManager::DidLastSteamPurchaseFail()
{
    return m_StartSteamTransactionStatus.Failed();
}

#endif

class RequestContentPathUrlTask : public SimpleOperationBaseTask<RgscCommerceManager>
{
public:
    RequestContentPathUrlTask():
        m_OutputBufferSize(0),
        m_OutputBuffer(NULL),
        m_LocalUserIndex()
    {

    }

    bool Configure( RgscCommerceManager* ctx, const RockstarId rockstarId,  const char* path, char* outputBuffer, unsigned int outputBufferSize )
    {
        m_Path = path;
        m_OutputBuffer = outputBuffer;
        m_OutputBufferSize = outputBufferSize;

        return SimpleOperationBaseTask<RgscCommerceManager>::Configure(ctx,rockstarId);
    }
private:
    unsigned int m_OutputBufferSize;
    char* m_OutputBuffer;
    atString m_Path;
    int m_LocalUserIndex;

protected:
    virtual bool PerformOperation()
    {
        return rlRosEntitlement::GetContentUrl(0, m_Path, m_OutputBuffer, m_OutputBufferSize, &m_MyStatus);
    }

    virtual void ProcessResult(bool /*success*/)
    {

    }
};

bool RGSC_CALL RgscCommerceManager::GetContentPathUrl(const char* path, char* outputBuffer, unsigned int outputBufferLength, IAsyncStatus* status)
{
    RLPC_CREATETASK_IASYNC(RequestContentPathUrlTask, 0, path, outputBuffer, outputBufferLength);	
}
int RgscCommerceManager::ConvertVoucherResultCodeToStringId(int resultCode)
{
	int retVal = -1;

	//RLEN_ERROR_AUTHENTICATIONFAILED_TICKET --> Needs new ID
	//RLEN_ERROR_SERIAL_DOES_NOT_EXIST --> i39
	//RLEN_ERROR_NOT_ENTITLED  ----> Needs new string id
	//RLEN_ERROR_SERIAL_ALREADY_ACTIVATED --> i38
	//RLSC_ERROR_UNEXPECTED_RESULT --> i40

    

	switch(resultCode)
	{
	case(RLEN_ERROR_NONE):
		retVal = 0;
		break;
	case(RLEN_ERROR_AUTHENTICATIONFAILED_TICKET):
		retVal = 46;
		break;
	case(RLEN_ERROR_SERIAL_DOES_NOT_EXIST):
		retVal = 39;
		break;
	case(RLEN_ERROR_NOT_ENTITLED):
		retVal = 47;
		break;
	case(RLEN_ERROR_SERIAL_ALREADY_ACTIVATED):
		retVal = 38;
		break;
	case(RLSC_ERROR_UNEXPECTED_RESULT):
		retVal = 40;
		break;
    case(RLEN_ERROR_NOT_ALLOWED_RULE):
        retVal = 48;
        break;
    default:
        rlError("Unknown result code %d in ConvertVoucherResultCodeToStringId.", resultCode);
	}

	return retVal;
}

#endif // x64

#if RSG_CPU_X86
void RgscCommerceManager::EntitlementV1_RefreshData(bool bForceBackendSync)
{
#if ENABLE_ENTITLEMENT_V1
	rlRosEntitlementV1::RefreshEntitlementData(RL_RGSC_GAMER_INDEX, bForceBackendSync);
#endif
}

bool RgscCommerceManager::EntitlementV1_IsCheckingForEntitlement()
{
#if ENABLE_ENTITLEMENT_V1
	return rlRosEntitlementV1::IsCheckingForEntitlement();
#else
	return false;
#endif
}

bool RgscCommerceManager::EntitlementV1_IsEntitlementUpToDate()
{
#if ENABLE_ENTITLEMENT_V1
	return rlRosEntitlementV1::IsEntitlementUpToDate();
#else
	return false;
#endif
}

bool RgscCommerceManager::EntitlementV1_RegisterPurchaseBySN(int entitlementProjectId, char* serialNumber, bool* isActivated, char* skuName, 
														  unsigned skuNameBufLen, char* friendlyName, unsigned friendlyLength, IAsyncStatus* status)
{
#if ENABLE_ENTITLEMENT_V1
	if (serialNumber == NULL || isActivated == NULL || friendlyName == NULL || status == NULL)
		return false;

	AsyncStatus* asyncStatus = NULL;
	RGSC_HRESULT hr = status->QueryInterface(IID_IAsyncStatusV1, (void**) &asyncStatus);
	if (SUCCEEDED(hr) && asyncStatus)
	{
		//@@: location COMMERCEMANAGER_ENTITLEMENTV1_REGISTERPURCHASEBYSN
		return rlRosEntitlementV1::RegisterPurchaseBySN(RL_RGSC_GAMER_INDEX, entitlementProjectId, serialNumber, isActivated, skuName, 
														skuNameBufLen, friendlyName, friendlyLength, asyncStatus->GetNetStatus());
	}

	return false;
#else
	return false;
#endif
}

bool RgscCommerceManager::EntitlementV1_RegisterPurchaseAndActivationBySN(int entitlementProjectId, char* serialNumber, IAsyncStatus* status)
{
#if ENABLE_ENTITLEMENT_V1
	if (serialNumber == NULL || status == NULL)
		return false;

	AsyncStatus* asyncStatus = NULL;
	RGSC_HRESULT hr = status->QueryInterface(IID_IAsyncStatusV1, (void**) &asyncStatus);
	if (SUCCEEDED(hr) && asyncStatus)
	{
		return rlRosEntitlementV1::RegisterPurchaseAndActivationBySN(RL_RGSC_GAMER_INDEX, entitlementProjectId, serialNumber, asyncStatus->GetNetStatus());
	}

	return false;
#else
	return false;
#endif
}

bool RgscCommerceManager::EntitlementV1_GetValidateContentPlayerAge(const char* contentName, IAsyncStatus* status)
{
#if ENABLE_ENTITLEMENT_V1
	if (contentName == NULL || status == NULL)
		return false;

	AsyncStatus* asyncStatus = NULL;
	RGSC_HRESULT hr = status->QueryInterface(IID_IAsyncStatusV1, (void**) &asyncStatus);
	if (SUCCEEDED(hr) && asyncStatus)
	{
		return rlRosEntitlementV1::GetValidateContentPlayerAge(RL_RGSC_GAMER_INDEX, contentName, asyncStatus->GetNetStatus());
	}

	return false;
#else
	return false;
#endif
}

bool RgscCommerceManager::EntitlementV1_IsEntitledToContent(const char* skuName)
{
#if ENABLE_ENTITLEMENT_V1
	if (skuName == NULL)
		return false;
	//@@: location COMMERCEMANAGER_ENTITLEMENTV1_ISENTITLEDTOCONTENT_ENTRY
	return rlRosEntitlementV1::IsEntitledToContent(skuName) != ENTITLEMENT_NOT_OWNED;
#else
	return false;
#endif
	//@@: location COMMERCEMANAGER_ENTITLEMENTV1_ISENTITLEDTOCONTENT_EXIT
}

bool RgscCommerceManager::EntitlementV1_GetActivationState(const char* skuName, bool* bIsOwned, char (&serialKey)[256], bool* bIsActivated)
{
#if ENABLE_ENTITLEMENT_V1
	if (!rlRosEntitlementV1::IsEntitlementUpToDate() || rlRosEntitlementV1::IsCheckingForEntitlement())
		return false;

	if (bIsOwned == NULL || bIsActivated == NULL)
		return false;

	serialKey[0] = '\0';
	*bIsOwned = false;
	*bIsActivated = false;

	rlRosEntitlementV1Data* pEntitlementData = rlRosEntitlementV1::GetCurrentEntitlementData();

	if (pEntitlementData)
	{
		rlRosEntitlementV1Sku* pSku = pEntitlementData->GetSkuByName(skuName);
		if (pSku)
		{
			//@@: location COMMERCEMANAGER_ENTITLEMENTV1_GETACTIVATIONSTATE_SET_OWNED
			*bIsOwned = true;
			*bIsActivated = pSku->m_IsActivated;
			safecpy(serialKey, pSku->m_SerialNum);
		}

		return true;
	}

	return false;	
#else
	return false;
#endif
}

bool RgscCommerceManager::EntitlementV1_GetContentNameBySku(const char * skuName, char * contentName, unsigned int contentNameLen)
{
#if ENABLE_ENTITLEMENT_V1
	if (skuName == NULL || contentName == NULL)
		return false;
	//@@: location COMMERCEMANAGER_ENTITLEMENTV1_GETCONTENTNAMEBYSKU_ENTRY
	return rlRosEntitlementV1::GetContentNameBySku(skuName, contentName, contentNameLen);
#else
	return false;
#endif
	//@@: location COMMERCEMANAGER_ENTITLEMENTV1_GETCONTENTNAMEBYSKU_EXIT
}

bool RgscCommerceManager::EntitlementV1_GetSkuDownloadURL(const char* skuName, char* downloadUrl, unsigned int downloadUrlBufLen, IAsyncStatus* status)
{
#if ENABLE_ENTITLEMENT_V1
	if (skuName == NULL || downloadUrl == NULL || status == NULL)
		return false;

	AsyncStatus* asyncStatus = NULL;
	RGSC_HRESULT hr = status->QueryInterface(IID_IAsyncStatusV1, (void**) &asyncStatus);
	if (SUCCEEDED(hr) && asyncStatus)
	{
		return rlRosEntitlementV1::GetSkuDownloadURL(RL_RGSC_GAMER_INDEX, skuName, downloadUrl, downloadUrlBufLen, asyncStatus->GetNetStatus());
	}

	return false;
#else
	return false;
#endif
}

#endif // RSG_CPU_X86

} //namespace rgsc

#endif // RSG_PC


