// 
// presences.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RGSC_PRESENCE_MANAGER_H
#define RGSC_PRESENCE_MANAGER_H

#include "tasks.h"
#include "presence_interface.h"
#include "metadata.h"
#include "rline/rlpresence.h"

using namespace rage;

namespace rgsc
{
// this needs to be >= the maximum of any current or previously
// released game's rlSessionInfo::TO_STRING_BUFFER_SIZE. I made this
// into a separate constant in case a future version of rage ever has
// a *smaller* rlSessionInfo::TO_STRING_BUFFER_SIZE than a previous game.
#define MAX_SESSION_INFO_BUFFER_SIZE (1024)

// error C4265: class has virtual functions, but destructor is not virtual
// the binary interface for virtual destructors isn't standardized, so don't make the destructor virtual
#pragma warning(push)
#pragma warning(disable: 4265)

class ScGameInvite
{
public:

	ScGameInvite();
	~ScGameInvite();

	static const unsigned GAME_INVITE_TIME_TO_LIVE_MS = 3 * 60 * 1000;

	void Clear();
	bool IsValid() const;
	void Reset(const rlGamerHandle& gamerHandle,
			   const rlScPresenceMessage& msgBuf,
			   const char* avatarUrl,
			   const char* inviterName,
			   const char* subject,
			   const char* salutation,
			   time_t timestamp,
			   u32 timeToLiveMs);

	char m_AvatarUrl[RLSC_MAX_AVATAR_URL_CHARS];
	char m_Salutation[RL_MAX_INVITE_SALUTATION_CHARS];
	char m_Subject[RL_MAX_INVITE_SUBJECT_CHARS];
	char m_InviterName[RL_MAX_NAME_BUF_SIZE];
	char m_Message[256 + MAX_SESSION_INFO_BUFFER_SIZE];
	rlGamerHandle m_InviterGamerHandle;
	time_t m_ReceivedTimestamp;
	int m_TimeToLiveMs;
	int m_InviteId;
};

class ScGameInvites
{
public:
	static const unsigned MAX_SC_GAME_INVITES = 10;

	ScGameInvites();
	~ScGameInvites();

	void Clear();
	void Update();

	bool Add(const ScGameInvite& invite);
	void Remove(unsigned index);
	unsigned GetCount() const;
	bool HasInviteFromPlayer(const rlGamerHandle& gh, unsigned &index);
	bool IsFull() const;
	bool GetInvite(unsigned index, ScGameInvite& invite) const;
	void Sort();

private:
	int FindFreeSlot();
	ScGameInvite m_Invites[MAX_SC_GAME_INVITES];
	unsigned m_Count;
	unsigned m_LastUpdateTime;
};

// ===========================================================================
// PresenceManager
// ===========================================================================

class RgscPresenceManager : public IPresenceManagerLatestVersion
{
// ===============================================================================================
// inherited from IPresenceManager
// ===============================================================================================
public:
	// ===============================================================================================
	// inherited from IRgscUnknown
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

	// ===============================================================================================
	// inherited from IPresenceManagerV1
	// ===============================================================================================
    virtual bool RGSC_CALL SetIntAttribute(const int localGamerIndex,
										   const char* name,
										   const s64 value);
    virtual bool RGSC_CALL SetDoubleAttribute(const int localGamerIndex,
											  const char* name,
											  const double value);
    virtual bool RGSC_CALL SetStringAttribute(const int localGamerIndex,
											  const char* name,
											  const char* value);
    virtual bool RGSC_CALL GetIntAttribute(const int localGamerIndex,
										   const char* name,
										   s64* value);
    virtual bool RGSC_CALL GetDoubleAttribute(const int localGamerIndex,
											  const char* name,
											  double* value);
    virtual bool RGSC_CALL GetStringAttribute(const int localGamerIndex,
											  const char* name,
											  char* value,
											  const unsigned sizeofValue);
    virtual bool RGSC_CALL GetAttributesForGamer(const RockstarId rockstarId,
												 IPresenceAttribute* attrs,
												 const unsigned numAttrs,
												 IAsyncStatus* status);
    virtual bool RGSC_CALL PostMessage(const int localGamerindex,
									   const RockstarId* recipients,
									   const unsigned numRecipients,
									   IPresenceMessage* message,
									   const unsigned ttlSeconds,
									   IAsyncStatus* status);
	virtual bool RGSC_CALL Query(const char* queryName,
								 const char* paramNameValueCsv,
								 const int offset,
								 const int count,
								 char* recordsBuf,
								 const unsigned sizeofRecordsBuf,
								 char** records,
                                 unsigned* numRecordsRetrieved,
								 unsigned* numRecords,
								 IAsyncStatus* status);
	virtual bool RGSC_CALL QueryCount(const char* queryName,
									  const char* paramNameValueCsv,
									  unsigned* count,
									  IAsyncStatus* status);
    virtual bool RGSC_CALL SetRichPresence(const int presenceId,
										   IPresenceAttribute* attrs,
										   const unsigned numAttrs,
										   IAsyncStatus* status);
	virtual bool RGSC_CALL AllowGameInvites(bool allowInvites);
	virtual bool RGSC_CALL SetGameSessionInfo(const char* sessionInfo);

	virtual bool RGSC_CALL AllowPartyInvites(bool allowInvites);
	virtual bool RGSC_CALL SetPartySessionInfo(const char* sessionInfo);

	// ===============================================================================================
	// inherited from IPresenceManagerV2
	// ===============================================================================================
    virtual bool RGSC_CALL Subscribe(const int localGamerIndex,
									 const char** channels,
									 const unsigned numChannels);

    virtual bool RGSC_CALL Unsubscribe(const int localGamerIndex,
									   const char** channels,
									   const unsigned numChannels);

    virtual bool RGSC_CALL UnsubscribeAll(const int localGamerIndex);

    virtual bool RGSC_CALL Publish(const int localGamerIndex,
								   const char** channels,
								   const unsigned numChannels,
								   const char* filterName,
								   const char* paramNameValueCsv,
								   const IPresenceMessage* message);

	// ===============================================================================================
	// inherited from IPresenceManagerV3
	// ===============================================================================================
	virtual bool RGSC_CALL SetAdditionalSessionInfo(const char* sessionInfo);
	virtual bool RGSC_CALL AllowAdditionalSessionGameInvites(bool allowInvites);

	// ===============================================================================================
	// inherited from IPresenceManagerV4
	// ===============================================================================================
	virtual bool RGSC_CALL NotifyGameInviteConsumed(RockstarId inviterId, const char* sessionInfo, IPresenceManagerV4::GameInviteConsumeReason reason);

	// ===============================================================================================
	// inherited from IPresenceManagerV5
	// ===============================================================================================
	virtual bool RGSC_CALL NotifySocialClubEvent(IPresenceMessage* message);

	// ===============================================================================================
	// inherited from IPresenceManagerV6
	// ===============================================================================================
	virtual bool RGSC_CALL SetPresenceMessageHandler(HandlePresenceMessageCallback cb);

	// ===============================================================================================
	// inherited from IPresenceManagerV7
	// ===============================================================================================
	virtual bool RGSC_CALL SetDelegate(IRgscPresenceDelegate* delegate);

// ===============================================================================================
// accessible from anywhere in the dll
// ===============================================================================================
public:

    RgscPresenceManager();
    ~RgscPresenceManager();

	bool Init();
	void Update();
	void Shutdown();
	void Clear();
	void SignOut();
	void OnFriendEvent(const rlFriendEvent* evt);

	bool HandleGameInvite(const rlScPresenceMessage& msgBuf);
	void HandleFriendStatusChanged(bool isOnline, const rlScPresenceMessage& msgBuf);
	void HandleFriendListChanged(const rlScPresenceMessage& msgBuf);
	void HandleFriendRequestReceived(const rlScPresenceMessage& msgBuf);
	void HandleFriendRequestAccepted(const rlScPresenceMessage& msgBuf);
	void HandleTextMessageReceived(const rlScPresenceMessage& msgBuf);
	void HandleFriendRichPresenceChanged(const rlScPresenceMessage& msgBuf);

	bool OnSocialClubEvent(const rlScPresenceMessage& msgBuf);

	int GetGameInviteIdFromJson(const char* json);
	RockstarId GetRockstarIdFromJson(const char* json);
	void GetRequestIdAndRockstarIdFromJson(const char* json, int &requestId, RockstarId &rockstarId);

	const char* GetGameInviteListAsJson();
	bool GameInviteAccepted(const char* jsonInvite);
	void GameInviteDeclined(const char* jsonInvite);
	bool CanSendGameInvite(RockstarId rockstarId);
	bool CanSendAdditionalSessionInvite(RockstarId rockstarId);
	RGSC_HRESULT GameInviteSend(const char* jsonInvite);
	unsigned GameNumGameInvites();
	RGSC_HRESULT JoinViaPresence(const char* jsonJoinViaPresence);
	bool GetAllowGameInvites() {return m_AllowGameInvites;}
	bool GetAllowPartyInvites() {return m_AllowPartyInvites;}
	bool GetAllowAdditionalSessionInvite() {return m_AllowAdditionalSessionGameInvites;}
	const char* GetGameSessionInfo() {return m_GameSessionInfo;}
	const char* GetPartySessionInfo() {return m_PartySessionInfo;}
	const char* GetAdditionalSessionInfo() { return m_AdditionalSessionInfo; }

	void MessageSent(const char* jsonNotification);

	bool PostMessageFireAndForget(PresenceSendMethod sendMethod, rlGamerHandle* handles, unsigned numHandles, const char* msgContents, int ttlSeconds = 0);

private:

	bool BuildRichPresenceString(const int presenceId, IPresenceAttribute* attrs, const unsigned numAttrs, char* buf, unsigned sizeOfBuf);
	bool TokenizeRichPresenceStringV1(atString& presenceString, PresenceAttribute* attr, unsigned index, unsigned fieldId);
	bool TokenizeRichPresenceStringV2(atString& presenceString, PresenceAttribute* attr, unsigned index, unsigned fieldId);

	bool SendGameInviteToScUi(const ScGameInvite& invite);
	bool SendFriendStatusToScUi(const rlFriend* f);
	bool SendFriendRequestToScUi(const rlScPresenceMessage& msgBuf);
	bool SendFriendRequestAcceptedToScUi(const rlScPresenceMessage& msgBuf);
	bool SendPresenceMessageToScUi(const rlScPresenceMessage& msgBuf);

	ScGameInvites m_GameInvites;
	rlFriendsManager::Delegate m_FriendDlgt;
	bool m_AllowGameInvites;
	bool m_AllowPartyInvites;
	bool m_AllowAdditionalSessionGameInvites;

	CompileTimeAssert(MAX_SESSION_INFO_BUFFER_SIZE >= rlSessionInfo::TO_STRING_BUFFER_SIZE);
	char m_GameSessionInfo[MAX_SESSION_INFO_BUFFER_SIZE];
	char m_PartySessionInfo[MAX_SESSION_INFO_BUFFER_SIZE];
	char m_AdditionalSessionInfo[MAX_SESSION_INFO_BUFFER_SIZE];

	IRgscPresenceDelegateV1* m_PresenceDlgt;

	u32 m_RichPresenceWriteTime;
	char m_RichPresenceLastMsg[RGSC_PRESENCE_STRING_MAX_SIZE];
	HandlePresenceMessageCallback m_PresenceMessageHandler;
};

} // namespace rgsc

#pragma warning(pop)

#endif  // RGSC_PRESENCE_MANAGER_H
