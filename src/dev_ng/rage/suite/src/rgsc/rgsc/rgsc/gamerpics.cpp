// 
// rline/gamerpics.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "file/file_config.h"
#include "gamerpics.h"

#if RSG_PC

#include "diag/seh.h"
#include "rlhttptask.h"
#include "rgsc.h"
#include "diag/output.h"
#include "file/device.h"
#include "parser/manager.h"
#include "rline/rldiag.h"
#include "string/stringhash.h"
#include "system/nelem.h"
#include "system/timer.h"

namespace rgsc
{

extern sysMemAllocator* g_RgscAllocator;

////////////////////////////////////////////////////////////////////////////////
// RgscGamerPicGroups
////////////////////////////////////////////////////////////////////////////////
void RgscGamerPicGroups::Clear()
{
    while(!m_List.empty())
    {
        RgscGamerPicGroups::Group* group = m_List.front();

        m_List.pop_front();

        RGSC_DELETE(group);
    }
}

RgscGamerPicGroups::Group& 
RgscGamerPicGroups::GetGroup(const unsigned index)
{
    Assert(index < m_List.size());

    unsigned i = 0;
    inlist<Group, &Group::m_ListLink>::iterator iter = m_List.begin();
    while(iter != m_List.end())
    {
        if(i == index)
        {
            break;
        }
        ++i;
        ++iter;
    }
    return *(*iter);
}

////////////////////////////////////////////////////////////////////////////////
// rlPcGamerPicGroups::Group
////////////////////////////////////////////////////////////////////////////////
void RgscGamerPicGroups::Group::ClearList()
{
    m_IsPopulated = false;

    while(!m_List.empty())
    {
        RgscGamerPicGroups::Group::Pic* pic = m_List.front();

        m_List.pop_front();

        RGSC_DELETE(pic);
    }

    m_List.clear();
}

void RgscGamerPicGroups::Group::Clear()
{
    m_Name[0] = '\0';
    m_Id = 0;
    ClearList();
}

bool RgscGamerPicGroups::Group::Reset(const char* groupName, const char* displayName)
{
    Clear();
    safecpy(m_Name, displayName);
    m_Id = atStringHash(groupName);
    m_IsPopulated = false;
    return true;
}

RgscGamerPicGroups::Group::Pic& 
RgscGamerPicGroups::Group::GetPic(const unsigned index)
{
    Assert(index < m_List.size());

    unsigned i = 0;
    inlist<Pic, &Pic::m_ListLink>::iterator iter = m_List.begin();
    while(iter != m_List.end())
    {
        if(i == index)
        {
            break;
        }
        ++i;
        ++iter;
    }
    return *(*iter);
}

////////////////////////////////////////////////////////////////////////////////
// rlPcGamerPicGroups::Group::Pic
////////////////////////////////////////////////////////////////////////////////
void RgscGamerPicGroups::Group::Pic::Clear()
{
    m_Name[0] = '\0';
    m_Caption[0] = '\0';
}

bool RgscGamerPicGroups::Group::Pic::Reset(const char* name, const char* caption)
{
    Clear();
    safecpy(m_Name, name);
    safecpy(m_Caption, caption);
    return true;
}

////////////////////////////////////////////////////////////////////////////////
// rlPcGamerPic
////////////////////////////////////////////////////////////////////////////////

// ===============================================================================================
// inherited from IGamerPicManager
// ===============================================================================================
RGSC_HRESULT RgscGamerPicManager::QueryInterface(RGSC_REFIID riid, void** ppvObj)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObj == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IGamerPicManager*>(this);
	}
	else if(riid == IID_IGamerPicManagerV1)
	{
		pUnknown = static_cast<IGamerPicManagerV1*>(this);
	}

	*ppvObj = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

// ===============================================================================================
// inherited from IGamerPicManagerV1
// ===============================================================================================
bool RgscGamerPicManager::GetDefaultGamerPicPath(char (&path)[RGSC_MAX_PATH], AvatarSize avatarSize)
{
	if(AssertVerify(GetRgscConcreteInstance()->_GetFileSystem()->GetOfflineUiAssetsDirectory(path, false)))
	{
		safecat(path, "ext\\scui\\images\\avatars\\");

		switch(avatarSize)
		{
		case IGamerPicManager::AvatarSize::SMALL:
			safecat(path, "32x32\\default.png");
			break;
		case IGamerPicManager::AvatarSize::XLARGE:
			safecat(path, "128x128\\default.png");
			break;
		case IGamerPicManager::LARGE:
			safecat(path, "default.png");
			break;
		}

		return true;
	}

	return false;
}

bool RgscGamerPicManager::DownloadGamerPic(const char* gamerPicName, int avatarSizeFlags, IAsyncStatus* status)
{
	// TODO: NS - assert that we're signedonline
	RLPC_CREATETASK_IASYNC(DownloadGamerPicTask, gamerPicName, avatarSizeFlags);
}

bool  RgscGamerPicManager::GetGamerPicPath(const char (&gamerPicName)[RGSC_MAX_AVATAR_URL_CHARS], AvatarSize avatarSize, char (&path)[RGSC_MAX_PATH]) const
{
	bool success = false;
	path[0] = '\0';

	rtry
	{
		rverify(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformCacheDirectory(path, true), catchall, );
		safecat(path, "Images\\Avatars\\");

		char httpScheme[] = "http://" ;
		bool isHttpScheme = strncmp(gamerPicName, httpScheme, sizeof(httpScheme) - 1) == 0;

		char httpsScheme[] = "https://" ;
		bool isHttpsScheme = strncmp(gamerPicName, httpsScheme, sizeof(httpsScheme) - 1) == 0;

		// If the avatar gamer pic is a HTTP or HTTPS scheme, create a hash for it
		if(isHttpScheme || isHttpsScheme)
		{
			safecat(path, "custom\\");

			u32 hashPath = atStringHash(gamerPicName);
			safecatf(path, "%u", hashPath);
		}
		else
		{
			if(avatarSize == AvatarSize::SMALL)
			{
				safecat(path, "32x32\\");
			}
			else if (avatarSize == AvatarSize::XLARGE)
			{
				safecat(path, "128x128\\");
			}

			char fixedGamerPicName[RGSC_MAX_AVATAR_URL_CHARS] = {0};
			rverify(GetNormalizedGamerPicName(gamerPicName, fixedGamerPicName), catchall, );

			safecat(path, fixedGamerPicName);
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool  RgscGamerPicManager::GetGamerPicUrl(const char (&gamerPicName)[RGSC_MAX_AVATAR_URL_CHARS], AvatarSize avatarSize, char (&url)[RGSC_MAX_PATH]) const
{
	bool success = false;
	rtry
	{
		char httpScheme[] = "http://" ;
		bool isHttpScheme = strncmp(gamerPicName, httpScheme, sizeof(httpScheme) - 1) == 0;

		char httpsScheme[] = "https://" ;
		bool isHttpsScheme = strncmp(gamerPicName, httpsScheme, sizeof(httpsScheme) - 1) == 0;

		// If the avatar gamer pic is a HTTP or HTTPS scheme, just copy it directly.
		if(isHttpScheme || isHttpsScheme)
		{
			safecpy(url, gamerPicName);
		}
		else
		{
			safecpy(url, "http://cdn.sc.rockstargames.com/images/avatars/");

			if(avatarSize == AvatarSize::SMALL)
			{
				safecat(url, "32x32/");
			}
			else if (avatarSize == AvatarSize::XLARGE)
			{
				safecat(url, "128x128/");
			}

			char fixedGamerPicName[RGSC_MAX_AVATAR_URL_CHARS] = {0};
			rverify(GetNormalizedGamerPicName(gamerPicName, fixedGamerPicName), catchall, );

			// convert '\' to '/'
			size_t len = strlen(fixedGamerPicName);
			for(size_t i = 0; i < len; i++)
			{
				if(fixedGamerPicName[i] == '\\')
				{
					fixedGamerPicName[i] = '/';
				}
			}

			safecat(url, fixedGamerPicName);
		}

		success = true;
	}
	rcatchall
	{
		memset(url, 0, sizeof(url));
	}

	return success;
}


// ===============================================================================================
// RgscGamerPicManager
// ===============================================================================================
RgscGamerPicManager::RgscGamerPicManager()
{

}

RgscGamerPicManager::~RgscGamerPicManager()
{

}

bool
RgscGamerPicManager::Init()
{
    bool success = false;

    rtry
    {
		rverify(ClearOutdatedCustomAvatars(), catchall, );

        success = true;
    }
    rcatchall
    {

    }

    return success;
}

void
RgscGamerPicManager::Shutdown()
{
	if (m_MyStatus.Pending())
	{
		m_MyStatus.Cancel();
	}

	m_GamerPicQueue.Reset();
}

void
RgscGamerPicManager::Update()
{
	if (m_GamerPicQueue.GetCount() > 0 && !m_MyStatus.Pending())
	{
		atString& relativeUrl = m_GamerPicQueue.Pop();

		rlDebug3("Downloading queued gamer pic: %s", relativeUrl.c_str());

		rlVerify(DownloadGamerPic(relativeUrl.c_str(), AvatarSize::ALL, &m_MyStatus));

		// free memory from this string
		relativeUrl.Clear();
	}
}

bool 
RgscGamerPicManager::PopulateGamerPicGroupNames(RgscGamerPicGroups* groups, netStatus* status)
{
	// TODO: NS - assert that we're signedonline

    RLPC_CREATETASK(PopulateGamerPicGroupNamesTask,
                    groups, 
                    status);
}

bool 
RgscGamerPicManager::DownloadGamerPicGroup(RgscGamerPicGroups::Group* group,
                                    netStatus* status)
{
	// TODO: NS - assert that we're signedonline

    RLPC_CREATETASK(DownloadGamerPicGroupTask,
                    group,
                    status);
}

u8* 
RgscGamerPicManager::GetGamerPicRaw(const char (&gamerPicName)[RGSC_MAX_AVATAR_URL_CHARS], AvatarSize avatarSize, unsigned int* sizeInBytes) const
{
	u8* data = NULL;
	char path[RGSC_MAX_PATH];

	if(sizeInBytes)
	{
		*sizeInBytes = 0;
	}

	if(gamerPicName[0] != '\0')
	{
		if(AssertVerify(GetRgscConcreteInstance()->_GetGamerPicManager()->GetGamerPicPath(gamerPicName, avatarSize, path)))
		{
			const fiDevice *device = fiDevice::GetDevice(path, true);
			if(device)
			{
				int size = (int)device->GetFileSize(path);

				if(size > 0)
				{
					fiHandle hFile = device->Open(path, true);
					if(fiIsValidHandle(hFile))
					{
						data = rage_new u8[size];
						if(AssertVerify(data))
						{
							int numBytesRead = device->Read(hFile, data, (int)size);
							Assert(numBytesRead == size);
							if(sizeInBytes)
							{
								*sizeInBytes = numBytesRead;
							}
						}
						device->Close(hFile);
					}
				}
			}
		}
	}
	return data;
}

bool RgscGamerPicManager::GetNormalizedGamerPicName(const char* gamerPicName, char (&normalizedGamerPicName)[RGSC_MAX_AVATAR_URL_CHARS]) const
{
	// need to handle the following paths:
	// ../images/avatars/GTACW/GTACW_06.png
	// /images/avatars/GTACW/GTACW_06.png
	// /avatars/GTACW/GTACW_06.png
	// avatars/GTACW/GTACW_06.png
	// ../GTACW/GTACW_06.png
	// ./GTACW/GTACW_06.png
	// /GTACW/GTACW_06.png
	// GTACW/GTACW_06.png
	// GTACW_06.png

	// SECURITY: validate gamerPicName - this comes from an online source

	bool success = false;

	rtry
	{
		memset(normalizedGamerPicName, 0, sizeof(normalizedGamerPicName));
		rverify(gamerPicName != NULL, catchall, );

		size_t len = strlen(gamerPicName);
		rverify(len > 4, catchall, ); // must end with .png
		rverify(len < RGSC_MAX_AVATAR_URL_CHARS, catchall, );

		char szAvatarUrl[RGSC_MAX_AVATAR_URL_CHARS] = {0};
		safecpy(szAvatarUrl, gamerPicName);

		rverify(stricmp(&szAvatarUrl[len - 4], ".png") == 0, catchall, );
		
		// convert '/' to '\'
		for(unsigned int i = 0; i < len; i++)
		{
			if(szAvatarUrl[i] == '/')
			{
				szAvatarUrl[i] = '\\';
			}
		}

		const char* avatarUrl = szAvatarUrl;

		const char* toRemove = "avatars\\";
		const char* start = strstr(avatarUrl, toRemove);
		if(start)
		{
			avatarUrl = start + strlen(toRemove);
		}

		if(strncmp(avatarUrl, "\\", 1) == 0)
		{
			avatarUrl += 1;
		}

		if(strncmp(avatarUrl, ".\\", 2) == 0)
		{
			avatarUrl += 2;
		}

		if(strncmp(avatarUrl, "..\\", 3) == 0)
		{
			avatarUrl += 3;
		}

		rverify(strstr(avatarUrl, ".\\") == NULL, catchall, );
		rverify(strstr(avatarUrl, "..\\") == NULL, catchall, );
		rverify(strstr(avatarUrl, "\\\\") == NULL, catchall, );

		// these characters are invalid in the Windows filesystem
		char invalidChars[] = {':', '*' , '?', '<', '>', '|', '"'};
		for(unsigned int i = 0; i < sizeof(invalidChars); i++)
		{
			rverify(strchr(avatarUrl, invalidChars[i]) == NULL, catchall, );
		}

		safecpy(normalizedGamerPicName, avatarUrl);

		success = true;
	}
	rcatchall
	{
		Assertf(false, "rlPcGamerPic::GetNormalizedGamerPicName(): Invalid gamer pic name: '%s'", gamerPicName);
		memset(normalizedGamerPicName, 0, sizeof(normalizedGamerPicName));
	}

	return success;
}

static json_char* json_as_string_or_null(json_const JSONNODE* node)
{
	char* str = json_as_string(node);
	return (_stricmp("null", str) != 0) ? str : NULL;
}

const char*
RgscGamerPicManager::GetGamerPicBase64(const char* jsonRequest)
{
	char* avatarUrl = NULL;
	bool largeVersion = false;

	JSONNODE *n = json_parse(jsonRequest);

	if(!AssertVerify(n != NULL))
	{
		return "";
	}

	// Default: Small
	AvatarSize avatarSize = IGamerPicManager::SMALL;

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return "";
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "AvatarUrl") == 0)
		{
			avatarUrl = json_as_string_or_null(*i);
		}
		else if(_stricmp(node_name, "LargeVersion") == 0)
		{
			largeVersion = json_as_bool(*i) != 0;

			// Legacy support for "largeVersion"
			if (largeVersion)
			{
				avatarSize = IGamerPicManager::XLARGE;
			}
		}
		else if (_stricmp(node_name, "Size") == 0)
		{
			int sizeProperty = json_as_int(*i);
			switch(sizeProperty)
			{
			case 0:
				avatarSize = IGamerPicManager::SMALL;
				break;
			case 1:
				avatarSize = IGamerPicManager::LARGE;
				break;
			case 2:
				avatarSize = IGamerPicManager::XLARGE;
				break;
			}
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	const json_char* avatarBase64 = "";

	if(avatarUrl)
	{
		unsigned int avatarSizeInBytes = 0;
		char _gamerPicName[RGSC_MAX_AVATAR_URL_CHARS] = {0};
		safecpy(_gamerPicName, avatarUrl);

		u8* avatarData = GetGamerPicRaw(_gamerPicName, avatarSize, &avatarSizeInBytes);

		if(avatarData)
		{
			avatarBase64 = json_encode64(avatarData, avatarSizeInBytes);
			delete avatarData;
		}

		json_free_safe(avatarUrl);
	}

	return avatarBase64;
}

// TODO: JRM
// Later versions of RAGE have this baked into device_win32, and can be removed.
void ConvertSystemTimeToFileTime(fiDevice::SystemTime& inTime, u64& outTime)
{
	CompileTimeAssert(sizeof(fiDevice::SystemTime) == sizeof(SYSTEMTIME)); // make sure the cast below doesn't do anything bad

	FILETIME localFileTime, fileTime;
	if (SystemTimeToFileTime((SYSTEMTIME*)&inTime, &localFileTime) && // convert the system time to local time
		LocalFileTimeToFileTime(&localFileTime, &fileTime)) // and convert the local time to the file time
	{
		ULARGE_INTEGER large;
		large.LowPart  = fileTime.dwLowDateTime;
		large.HighPart = fileTime.dwHighDateTime;
		outTime = large.QuadPart;
	}
	else
	{
		outTime = 0;
	}
}

static const wchar_t *FixNameW(wchar_t *dest,int destSize,const char *src) {
	char namebuf[RAGE_MAX_PATH];
	fiDeviceLocal::FixName(namebuf,RAGE_MAX_PATH,src);	
	MultiByteToWideChar(CP_UTF8 , NULL , namebuf, -1, dest, destSize);
	return dest;
}

u64 GetLastAccessFileTime(const char *filename)
{
	WIN32_FILE_ATTRIBUTE_DATA fd;
	wchar_t namebuf[RAGE_MAX_PATH];
	if (!GetFileAttributesExW(FixNameW(namebuf,RAGE_MAX_PATH,filename),GetFileExInfoStandard,&fd))
		return 0;
	return ((u64)fd.ftLastAccessTime.dwHighDateTime << 32) | fd.ftLastAccessTime.dwLowDateTime;
}

bool RgscGamerPicManager::ClearOutdatedCustomAvatars()
{
	rtry
	{
		// Files older than 1 month will no longer be cached.
		const u64 SECONDS_PER_MONTH = 2592000;

		// Construct the Custom Avatar images path
		char path[RGSC_MAX_PATH];
		rverify(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformCacheDirectory(path, true), catchall, );
		safecat(path, "Images\\Avatars\\custom\\");

		// Extract the local device
		const fiDevice* device = fiDevice::GetDevice(path, false);
		rverify(device, catchall, );

		// Read the current file time
		u64 currentFileTime;
		fiDevice::SystemTime currentTime;
		fiDevice::GetLocalSystemTime(currentTime);
		ConvertSystemTimeToFileTime(currentTime, currentFileTime);

		fiFindData data;
		fiHandle h = device->FindFileBegin(path, data);

		// Enumerate through files in the custom avatar folder
		if(fiIsValidHandle(h))
		{
			do
			{
				// Skip directories
				if(data.m_Name[0] != '.' && !(data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)) 
				{
					char fullPath[RGSC_MAX_PATH];
					formatf(fullPath, "%s%s", path, data.m_Name);

					// Calculate the number of 100-nanosecond intervals between the current time and the file time
					u64 lastAccessTime = GetLastAccessFileTime(fullPath);
					if (data.m_LastWriteTime > lastAccessTime)
						lastAccessTime = data.m_LastWriteTime;

					u64 ageNanoSeconds = (currentFileTime - lastAccessTime);

					// Convert to seconds
					double fileAgeSeconds = (double)ageNanoSeconds / 10000000.0;

					// If the file is old enough to delete, delete it. The fiFindData will continue
					// to the next file without issue
					if (fileAgeSeconds > SECONDS_PER_MONTH)
					{
						rlDisplay("Deleting stale gamer pic: %s (%f seconds old)", fullPath, fileAgeSeconds);
						device->Delete(fullPath);
					}
				}
			}
			while(device->FindFileNext(h,data));

			// end the file search
			device->FindFileEnd(h);
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool RgscGamerPicManager::QueueDownloadGamerPic(const char* relativeAvatarUrl)
{
	if (m_GamerPicQueue.IsFull())
	{
		rlWarning("Cannot queue a gamer pic for download, queue is full");
		return false;
	}

	atString& pic = m_GamerPicQueue.Append();
	pic = relativeAvatarUrl;
	return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlPcGamerPic::DownloadGamerPicTask
///////////////////////////////////////////////////////////////////////////////

RgscGamerPicManager::DownloadGamerPicTask::DownloadGamerPicTask()
: m_State(STATE_DOWNLOAD)
, m_AvatarSizeFlags(0)
{
}

bool
RgscGamerPicManager::DownloadGamerPicTask::Configure(RgscGamerPicManager* /*ctx*/, const char* gamerPicName, int avatarSizeFlags)
{
    bool success = false;

    if(AssertVerify((gamerPicName != NULL) && (gamerPicName[0] != '\0')))
    {
        safecpy(m_GamerPicName, gamerPicName);
		m_AvatarSizeFlags = avatarSizeFlags;
        success = true;
    }

    return success;
}

void
RgscGamerPicManager::DownloadGamerPicTask::Start()
{
    rlDebug2("Retrieving gamer pic '%s'...", m_GamerPicName);

    this->TaskBase::Start();
}

void
RgscGamerPicManager::DownloadGamerPicTask::Finish(const FinishType finishType, const int resultCode)
{
    rlDebug2("Retrieving gamer pic '%s' %s", m_GamerPicName, FinishString(finishType));

    this->TaskBase::Finish(finishType, resultCode);
}

void 
RgscGamerPicManager::DownloadGamerPicTask::DoCancel()
{
	for (int i = 0; i < COUNTOF(m_MyStatus); i++)
	{
		RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus[i]);
	}
}

void
RgscGamerPicManager::DownloadGamerPicTask::Update(const unsigned timeStep)
{
    this->TaskBase::Update(timeStep);

	bool anyPending = false;
	bool anyFailed = false;

	int resultCode = 0;
	for (int i = 0; i < DL_COUNT; i++)
	{
		if (m_MyStatus[i].Pending())
		{
			anyPending = true;
		}
		else if (m_MyStatus[i].Failed())
		{
			// cache any of the failed result codes to return for this task.
			resultCode = m_MyStatus[i].GetResultCode();
			anyFailed = true;
		}
	}

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!anyPending)
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

//@@: range RLPCGAMERPIC_DOWNLOADGAMERPICTASK_UPDATE {
    switch(m_State)
    {
    case STATE_DOWNLOAD:
		if (m_AvatarSizeFlags & AvatarSize::SMALL)
		{
			//@@: location RLPCGAMERPIC_DOWNLOADGAMERPICTASK_UPDATE_GET_SMALL_PIC
			DownloadGamerPic(AvatarSize::SMALL, &m_MyStatus[DL_SMALL]);
		}

		if (m_AvatarSizeFlags & AvatarSize::LARGE)
		{
			//@@: location RLPCGAMERPIC_DOWNLOADGAMERPICTASK_UPDATE_GET_LARGE_PIC
			DownloadGamerPic(AvatarSize::LARGE, &m_MyStatus[DL_LARGE]);
		}

		if (m_AvatarSizeFlags & AvatarSize::XLARGE)
		{
			DownloadGamerPic(AvatarSize::XLARGE, &m_MyStatus[DL_XLARGE]);
		}

		m_State = STATE_DOWNLOADING;
        break;

    case STATE_DOWNLOADING:
		{
			if (!anyPending)
			{
				if (anyFailed)
				{
					this->Finish(FINISH_FAILED, resultCode);
				}
				else
				{
					this->Finish(FINISH_SUCCEEDED);
				}
			}
		}

		break;
    }
	//@@: } RLPCGAMERPIC_DOWNLOADGAMERPICTASK_UPDATE
}

bool
RgscGamerPicManager::DownloadGamerPicTask::SetupPaths(AvatarSize avatarSize)
{
    bool success = false;

    rtry
    {
        rverify(m_Ctx->GetGamerPicPath(m_GamerPicName, avatarSize, m_LocalPath), catchall, );
        rverify(m_Ctx->GetGamerPicUrl(m_GamerPicName, avatarSize, m_Url), catchall, );

        success = true;
    }
    rcatchall
    {

    }

    return success;
}

bool
RgscGamerPicManager::DownloadGamerPicTask::DownloadGamerPic(AvatarSize avatarSize, netStatus* status)
{
    // TODO: NS - use RL_HTTP_OVERWRITE_IF_CHANGED
    if (SetupPaths(avatarSize) &&
        rlHttpDownloadTask::DownloadFile(m_Url, m_LocalPath, rlHttpDownloadTask::RL_HTTP_OVERWRITE_NEVER, status))
	{
		return true;
	}
	else
	{
		status->ForceFailed();
		return false;
	}
}

///////////////////////////////////////////////////////////////////////////////
//  rlPcGamerPic::PopulateGamerPicGroupNamesTask
///////////////////////////////////////////////////////////////////////////////

RgscGamerPicManager::PopulateGamerPicGroupNamesTask::PopulateGamerPicGroupNamesTask()
: m_State(STATE_INVALID)
{
}

bool
RgscGamerPicManager::PopulateGamerPicGroupNamesTask::Configure(RgscGamerPicManager* /*ctx*/,
                                                        RgscGamerPicGroups* groups)
{
    rlDebug("Mem available: %d", g_RgscAllocator->GetMemoryAvailable());

    m_Groups = groups;
    m_State = STATE_INVALID;

    return true;
}

void
RgscGamerPicManager::PopulateGamerPicGroupNamesTask::Start()
{
    rlDebug2("Populating gamer pic group names...");

    this->TaskBase::Start();

    m_State = STATE_DOWNLOAD_LAYOUT_FILE;
}

void
RgscGamerPicManager::PopulateGamerPicGroupNamesTask::Finish(const FinishType finishType, const int resultCode)
{
    rlDebug2("Populating gamer pic group names (%d groups) %s", m_Groups->GetNumGroups(), FinishString(finishType));

    this->TaskBase::Finish(finishType, resultCode);
}

void 
RgscGamerPicManager::PopulateGamerPicGroupNamesTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
}

void
RgscGamerPicManager::PopulateGamerPicGroupNamesTask::Update(const unsigned timeStep)
{
    this->TaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

    do 
    {
        switch(m_State)
        {
        case STATE_DOWNLOAD_LAYOUT_FILE:
            if(DownloadLayoutFile())
            {
                m_State = STATE_DOWNLOADING_LAYOUT_FILE;
            }
            else
            {
                this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
            }
            break;

        case STATE_DOWNLOADING_LAYOUT_FILE:
            if(m_MyStatus.Succeeded())
            {
                m_State = STATE_POPULATE_GROUP_NAMES;
            }
            else if(!m_MyStatus.Pending())
            {
                this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
            }
            break;

        case STATE_POPULATE_GROUP_NAMES:
            if(PopulateGroupNames())
            {
                this->Finish(FINISH_SUCCEEDED);
            }
            else
            {
                this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
            }
            break;
        }
    }
    while(!m_MyStatus.Pending() && this->IsActive());
}

bool
RgscGamerPicManager::PopulateGamerPicGroupNamesTask::DownloadLayoutFile()
{
    bool success = false;

    rtry
    {
        const char filename[RGSC_MAX_AVATAR_URL_CHARS] = "avatar.xml";
        rverify(m_Ctx->GetGamerPicPath(filename, AvatarSize::LARGE, m_LocalPath), catchall, );

        // TODO: NS - env?
        safecpy(m_Url, "http://socialclub.rockstargames.com/xml/avatar.xml");

        // TODO: NS - use RL_HTTP_OVERWRITE_IF_CHANGED
        success = rlHttpDownloadTask::DownloadFile(m_Url, m_LocalPath, rlHttpDownloadTask::RL_HTTP_OVERWRITE_ALWAYS, &m_MyStatus);
    }
    rcatchall
    {

    }

    return success;
}

bool
RgscGamerPicManager::PopulateGamerPicGroupNamesTask::PopulateGroupNames()
{
    bool success = false;

    INIT_PARSER;

    PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);
    parTree* tree = PARSER.LoadTree(m_LocalPath, "xml");
    PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, false);

    if (tree && tree->GetRoot())
    {
        rtry
        {
            parTreeNode* pGroups = tree->GetRoot();
            rverify(pGroups, catchall, );

            for(unsigned i = 0; i < (unsigned)pGroups->FindNumChildren(); i++)
            {
                parTreeNode* pNode = pGroups->FindChildWithIndex(i);
                rverify(pNode, catchall, );

                if(stricmp(pNode->GetElement().GetName(), "g") != 0)
                {
                    // not a group
                    continue;
                }

                parAttribute *attr = pNode->GetElement().FindAttributeAnyCase("n");
                if(attr == NULL)
                {
                    continue;
                }
                const char* groupName = attr->GetStringValue();

                attr = pNode->GetElement().FindAttributeAnyCase("dn");
                if(attr == NULL)
                {
                    continue;
                }
                const char* displayName = attr->GetStringValue();

                RgscGamerPicGroups::Group *group = RGSC_NEW(PopulateGamerPicGroupNamesTask, RgscGamerPicGroups::Group);
                rverify(group, catchall, );

                bool valid = group->Reset(groupName, displayName);

                if(valid)
                {
                    m_Groups->m_List.push_back(group);
                }
            }
            success = true;
        }
        rcatchall
        {
        }

        delete tree;
        tree = NULL;
    }

    SHUTDOWN_PARSER;

    return success;
}

///////////////////////////////////////////////////////////////////////////////
//  rlPcGamerPic::DownloadGamerPicGroupTask
///////////////////////////////////////////////////////////////////////////////

RgscGamerPicManager::DownloadGamerPicGroupTask::DownloadGamerPicGroupTask()
: m_State(STATE_INVALID)
, m_StartTime(0)
, m_NumStartedTasks(0)
, m_NumCompletedTasks(0)
, m_NumSucceededTasks(0)
, m_NumFailedTasks(0)
{
}

bool
RgscGamerPicManager::DownloadGamerPicGroupTask::Configure(RgscGamerPicManager* /*ctx*/,
                                                   RgscGamerPicGroups::Group* group)
{
    rlDebug("Mem available: %d", g_RgscAllocator->GetMemoryAvailable());
    rlDebug("sizeof(DownloadGamerPicGroupTask): %d", sizeof(DownloadGamerPicGroupTask));

    m_Group = group;
    m_State = STATE_INVALID;

    return true;
}

void
RgscGamerPicManager::DownloadGamerPicGroupTask::Start()
{
    rlDebug2("Downloading gamer pic group '%s'...", m_Group->GetName());

    this->TaskBase::Start();

    m_StartTime = sysTimer::GetSystemMsTime();

    m_State = STATE_POPULATE_GROUP;
}

void
RgscGamerPicManager::DownloadGamerPicGroupTask::Finish(const FinishType finishType, const int resultCode)
{
    rlDebug2("Downloading gamer pic group '%s' (%d pics) %s (%ums)", m_Group->GetName(), m_Group->GetNumPics(), FinishString(finishType), sysTimer::GetSystemMsTime() - m_StartTime);

    this->TaskBase::Finish(finishType, resultCode);
}

int
RgscGamerPicManager::DownloadGamerPicGroupTask::FindAvailableSubTask()
{
    int index = -1;
    for(unsigned i = 0; i < COUNTOF(m_SubTaskStatus); i++)
    {
        if(m_SubTaskStatus[i].GetStatus() == netStatus::NET_STATUS_NONE)
        {
            index = i;
            break;
        }
    }
    return index;
}

void 
RgscGamerPicManager::DownloadGamerPicGroupTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);

	for(unsigned i = 0; i < COUNTOF(m_SubTaskStatus); ++i)
	{
		RgscTaskManager::CancelTaskIfNecessary(&m_SubTaskStatus[i]);
	}
}

void
RgscGamerPicManager::DownloadGamerPicGroupTask::Update(const unsigned timeStep)
{
    this->TaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		bool isPending = m_MyStatus.Pending();

		if(!isPending)
		{
			for(unsigned i = 0; i < COUNTOF(m_SubTaskStatus); ++i)
			{
				if(m_SubTaskStatus[i].Pending())
				{
					isPending = true;
					break;
				}
			}
		}

		if(!isPending)
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}
    do 
    {
        switch(m_State)
        {
        case STATE_POPULATE_GROUP:
            if(PopulateGroup())
            {
                m_MyStatus.SetPending();
                m_State = STATE_DOWNLOADING_GAMER_PICS;
            }
            else
            {
                this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
            }
            break;

        case STATE_DOWNLOADING_GAMER_PICS:
            while(m_NumStartedTasks < m_Group->GetNumPics())
            {
                int i = FindAvailableSubTask();

                if(i < 0)
                {
                    break;
                }

                if(rlTaskBase::Configure(&m_DownloadGamerPicTask[i],
                                        m_Ctx,
                                        m_Group->GetPic(m_NumStartedTasks).GetName(),
										IGamerPicManager::SMALL | IGamerPicManager::LARGE,
                                        &m_SubTaskStatus[i]))
                {
                    m_DownloadGamerPicTask[i].Start();
                }
                else
                {
                    m_SubTaskStatus[i].SetFailed();
                }

                m_NumStartedTasks++;
            }

            for(unsigned i = 0; i < COUNTOF(m_DownloadGamerPicTask); i++)
            {
                if(m_SubTaskStatus[i].GetStatus() != netStatus::NET_STATUS_NONE)
                {
                    if(m_SubTaskStatus[i].Pending())
                    {
                        m_DownloadGamerPicTask[i].Update(timeStep);
                    }
                    else
                    {
                        m_SubTaskStatus[i].Succeeded() ? m_NumSucceededTasks++ : m_NumFailedTasks++;
                        m_NumCompletedTasks++;
                        m_SubTaskStatus[i].Reset();
                    }
                }
            }

            if(m_NumCompletedTasks == m_Group->GetNumPics())
            {
                //Assert(m_NumCompletedTasks == m_NumSucceededTasks);
                m_MyStatus.SetSucceeded();
                this->Finish(FINISH_SUCCEEDED);
            }
            break;
        }
    }
    while(!m_MyStatus.Pending() && this->IsActive());
}

bool
RgscGamerPicManager::DownloadGamerPicGroupTask::PopulateGroup()
{
    bool success = false;

    const char filename[RLSC_MAX_AVATAR_URL_CHARS] = "avatar.xml";
    char path[RGSC_MAX_PATH];
    bool gotPath = m_Ctx->GetGamerPicPath(filename, AvatarSize::LARGE, path);
    if(gotPath == false)
    {
        return false;
    }

    INIT_PARSER;

    PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);
    parTree* tree = PARSER.LoadTree(path, "xml");
    PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, false);

    if (tree && tree->GetRoot())
    {
        rtry
        {
            parTreeNode* pGroups = tree->GetRoot();
            rverify(pGroups, catchall, );

            for(unsigned i = 0; i < (unsigned)pGroups->FindNumChildren(); i++)
            {
                parTreeNode* pGroupNode = pGroups->FindChildWithIndex(i);
                rverify(pGroupNode, catchall, );

                if(stricmp(pGroupNode->GetElement().GetName(), "g") != 0)
                {
                    // not a group
                    continue;
                }

                parAttribute *attr = pGroupNode->GetElement().FindAttributeAnyCase("n");
                if(attr == NULL)
                {
                    continue;
                }
                const char* groupName = attr->GetStringValue();

                attr = pGroupNode->GetElement().FindAttributeAnyCase("dn");
                if(attr == NULL)
                {
                    continue;
                }
                const char* displayName = attr->GetStringValue();

                RgscGamerPicGroups::Group group;

                bool valid = group.Reset(groupName, displayName);

                if(valid)
                {
                    if(group.GetId() == m_Group->GetId())
                    {
                        // this is the group we want to populate
                        for(unsigned j = 0; j < (unsigned)pGroupNode->FindNumChildren(); j++)
                        {
                            parTreeNode* pAvatarNode = pGroupNode->FindChildWithIndex(j);
                            rverify(pAvatarNode, catchall, );

                            if(stricmp(pAvatarNode->GetElement().GetName(), "a") != 0)
                            {
                                // not an avatar
                                continue;
                            }

                            parAttribute *attr = pAvatarNode->GetElement().FindAttributeAnyCase("url");
                            if(attr == NULL)
                            {
                                continue;
                            }

							char fixedAvatarUrl[RLSC_MAX_AVATAR_URL_CHARS];
							const char* avatarUrl = attr->GetStringValue();
							if(avatarUrl == NULL)
							{
								continue;
							}

							m_Ctx->GetNormalizedGamerPicName(avatarUrl, fixedAvatarUrl);

                            const char* caption = pAvatarNode->GetData();

                            RgscGamerPicGroups::Group::Pic *pic = RGSC_NEW(DownloadGamerPicGroupTask, RgscGamerPicGroups::Group::Pic);
                            rverify(pic, catchall, );

                            bool valid = pic->Reset(fixedAvatarUrl, caption);

                            if(valid)
                            {
                                m_Group->m_List.push_back(pic);
                            }
                        }

                        m_Group->m_IsPopulated = true;
                        success = true;
                        break;
                    }
                }
            }
        }
        rcatchall
        {
        }

        delete tree;
        tree = NULL;
    }

    SHUTDOWN_PARSER;

    return success;
}

} //namespace rgsc

#endif // RSG_PC
