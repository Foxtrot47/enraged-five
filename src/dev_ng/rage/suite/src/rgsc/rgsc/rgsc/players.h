// 
// players.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RGSC_PLAYER_MANAGER_H
#define RGSC_PLAYER_MANAGER_H

#include "tasks.h"
#include "atl/array.h"
#include "players_interface.h"
#include "profiles_interface.h"
#include "rline/rl.h"
#include "system/criticalsection.h"

using namespace rage;

namespace rgsc
{

// error C4265: class has virtual functions, but destructor is not virtual
// the binary interface for virtual destructors isn't standardized, so don't make the destructor virtual
#pragma warning(push)
#pragma warning(disable: 4265)

// ===========================================================================
// Player
// ===========================================================================
class Player : public IPlayerLatestVersion
{
public:
	// ===============================================================================================
	// inherited from IPlayerV1
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);
	virtual RockstarId RGSC_CALL GetRockstarId() const;
	virtual const char* RGSC_CALL GetName() const;
	virtual IPlayerLatestVersion::Relationship RGSC_CALL GetRelationship() const;
	virtual bool RGSC_CALL IsOnline() const;
	virtual bool RGSC_CALL IsPlayingSameTitle() const;

	// ===============================================================================================
	// inherited from IPlayerV2
	// ===============================================================================================
	virtual unsigned RGSC_CALL GetFlags() const;

	// ===============================================================================================
	// inherited from IPlayerV3
	// ===============================================================================================
	virtual const char* RGSC_CALL GetAvatarUrl() const;

public:
	Player();

	virtual bool IsValid() const;
	virtual void Clear();

	char* m_Name;
	RockstarId m_RockstarId;
	char m_AvatarUrl[RGSC_MAX_AVATAR_URL_CHARS];
	IPlayerLatestVersion::Relationship m_Relationship;
	u8 m_IsOnline : 1;
	u8 m_IsPlayingSameTitle : 1;
	u8 m_IsInJoinableSession : 1;
	u8 m_IsInJoinableParty : 1;
};

// ===========================================================================
// IPlayerListV1 implementation
// ===========================================================================
class PlayerListV1 : public IPlayerListV1
{
public:
	// ===============================================================================================
	// inherited from IPlayerListV1
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);
	virtual u32 RGSC_CALL GetNumPlayers() const;
	virtual Player* RGSC_CALL GetPlayer(const u32 index);

public:
	static u32 GetMemoryRequired(u32 maxPlayers);
	PlayerListV1(u32 maxPlayers, u8* buffer);

	virtual u32 GetMaxPlayers() const;
	virtual Player* GetPlayerArray();
	virtual void Clear();
	virtual bool AddPlayer();

	u32 m_MaxPlayers;
	u32 m_NumPlayers;
	Player* m_Players;
};

typedef PlayerListV1 PlayerList;

// ===========================================================================
// PlayerManager
// ===========================================================================

class RgscPlayerManager : public IPlayerManagerLatestVersion
{
// ===============================================================================================
// inherited from IPlayerManager
// ===============================================================================================
public:
	// ===============================================================================================
	// inherited from IRgscUnknown
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

	// ===============================================================================================
	// inherited from IPlayerManagerV1
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL CreatePlayerEnumerator(RGSC_REFIID riid,
														 const RockstarId profileId,
														 const PlayerListType playerListType,
														 const u32 firstPlayerIndex,
														 const u32 maxPlayersToRead,
														 u32* numBytesRequired,
														 void** handle);

	virtual RGSC_HRESULT RGSC_CALL EnumeratePlayers(void* handle,
													void* buffer,
													const u32 bufferSize,
													IPlayerList** players,
													IAsyncStatus* status);

	virtual bool RGSC_CALL IsFriend(const RockstarId hGamer);
	virtual RGSC_HRESULT RGSC_CALL ShowFriendRequestUi(const RockstarId hGamer);
	virtual RGSC_HRESULT RGSC_CALL ShowPlayerProfileUi(const RockstarId rockstarId);
	virtual RGSC_HRESULT RGSC_CALL SendInvite(const RockstarId otherGamer, IAsyncStatus* status);
	virtual bool RGSC_CALL IsBlocked(const RockstarId hGamer);

	// ===============================================================================================
	// inherited from IPlayerManagerV2
	// ===============================================================================================
	virtual unsigned RGSC_CALL GetTotalNumFriends();
	virtual bool RGSC_CALL RequestFriendSync();
	virtual bool RGSC_CALL IsSyncingFriends();
	virtual RGSC_HRESULT RGSC_CALL ShowFriendSearchUi();

	// these may be exposed to the game in the future.
// 	virtual RGSC_HRESULT RGSC_CALL CancelInvite(const RockstarId otherGamer, IAsyncStatus* status);
// 	virtual RGSC_HRESULT RGSC_CALL DeclineInvite(const RockstarId otherGamer, IAsyncStatus* status);
// 	virtual RGSC_HRESULT RGSC_CALL AcceptInvite(const RockstarId otherGamer, IAsyncStatus* status);
// 	virtual RGSC_HRESULT RGSC_CALL DeleteFriend(const RockstarId otherGamer, IAsyncStatus* status);
// 	virtual RGSC_HRESULT RGSC_CALL BlockPlayer(const RockstarId hGamer, IAsyncStatus* status);
// 	virtual RGSC_HRESULT RGSC_CALL UnblockPlayer(const RockstarId hGamer, IAsyncStatus* status);

// ===============================================================================================
// accessible from anywhere in the dll
// ===============================================================================================
public:

    RgscPlayerManager();
    ~RgscPlayerManager();

	bool Init();
	void Update();
	void Shutdown();

	void Clear();

	bool SyncPlayers(const RockstarId rockstarId, netStatus* status);
	void SignOut();

	bool ReadPlayers(const RockstarId profileId,
					 IPlayerManagerLatestVersion::PlayerListType playerListType,
					 const u32 firstPlayerIndex,
					 IPlayerList* players,
					 IAsyncStatus* status);

	RGSC_HRESULT ReadPlayerListAsJson(const char* json);
	RGSC_HRESULT ReadPlayerListCountsAsJson(const char* json);
	RGSC_HRESULT GetPlayerInfoAsJson(const char* json);
	RGSC_HRESULT GetPlayerMultiplayerPresenceInfoAsJson(const char* json);

	void SortPlayers(IPlayerList* players);

	// javascript to C++ methods
	RockstarId GetRockstarIdFromJson(const char* json);
	void GetRequestIdAndRockstarIdFromJson(const char* json, int &requestId, RockstarId &rockstarId);
	RGSC_HRESULT SendInvite(const char* json);
	RGSC_HRESULT CancelInvite(const char* json);
	RGSC_HRESULT DeclineInvite(const char* json);
	RGSC_HRESULT AcceptInvite(const char* json);
	RGSC_HRESULT DeleteFriend(const char* json);
	RGSC_HRESULT BlockPlayer(const char* json);
	RGSC_HRESULT UnblockPlayer(const char* json);
	bool IsBlocked(const char* json);
	bool IsFriend(const char* json);
	RGSC_HRESULT GetFacebookLinkInfoAsJson(const char* json);

	bool GetNumFriendsOnline(unsigned& numFriendsOnline, unsigned& numFriendsPlayingSameTitle);
	void SetPlayerListCounts(unsigned numBlocked, unsigned numFriends, unsigned numInvitesReceieved, unsigned numInvitesSent);
	void GetPlayerListCounts(unsigned& numBlocked, unsigned& numFriends, unsigned& numInvitesReceieved, unsigned& numInvitesSent);

	typedef atFixedArray<RockstarId, RLSC_FRIENDS_MAX_BLOCKED_PLAYERS> BlockedPlayerIds;
	BlockedPlayerIds* GetBlockedPlayerIdCache();
	void AddPlayerToBlockList(const RockstarId rockstarId);
	void RemovePlayerFromBlockList(const RockstarId rockstarId);

	bool HasDoneInitialRichPresenceQuery() { return m_HasDoneInitialRichPresenceQuery; }

private:
	BlockedPlayerIds m_BlockedPlayerIds;

	unsigned m_NumBlocked;
	unsigned m_NumFriends;
	unsigned m_NumInvitesReceived;
	unsigned m_NumInvitesSent;

	bool m_HasDoneInitialRichPresenceQuery;
};

} // namespace rgsc

#pragma warning(pop)

#endif  // RGSC_PLAYER_MANAGER_H
