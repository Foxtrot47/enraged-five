// 
// rgsc/mfa.h 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPC_MFA_H
#define RLINE_RLPC_MFA_H

// rage includes
#include "atl/array.h"
#include "atl/string.h"

// rgsc includes
#include "json.h"
#include "data/rson.h"
#include "titleid_interface.h"

using namespace rage;

namespace rgsc
{

///////////////////////////////////////////
// FingerprintKvp
///////////////////////////////////////////
struct FingerprintKvp
{
	atString key;
	atString value;
};

///////////////////////////////////////////
// FingerprintKvp
///////////////////////////////////////////
class RgscMultiFactorAuth
{
public:
	
	RgscMultiFactorAuth();

	// PURPOSE
	//	Call 'Init' once at startup and 'Shutdown' again during termination.
	bool Init(TitleId& titleId);
	void Shutdown();

	// PURPOSE
	//	Returns the numeric fingerprint hash calculated by GenerateFingerprint
	u64 GetFingerprint();

	// PURPOSE
	//	Fills out a JSONNODE object with the fingerprint key/values.
	void GetFingerprintJSON(JSONNODE* fpJsonNode);

	// PURPOSE
	//	Fills out an RsonWriter object with the fingerprint key/values.
	void GetFingerPrintRSON(RsonWriter* rw);

private:

	// PURPOSE
	//	Generates a fingerprint for MFA. Requires the titleId for device name.
	u64 GenerateFingerprint(TitleId& titleId);

	// PURPOSE
	//	Fingerprinting utilities - adds the key/value pair to the array.
	void AddFingerPrint		(const char* key, const char* value);
	void AddFingerPrintInt	(const char* key, int value);
	void AddFingerPrintUns	(const char* key, unsigned value);
	void AddFingerPrintStr	(const char* key, const char* value);
	void AddFingerPrintStrW	(const char* key, const wchar_t* value);

	u64						m_Fingerprint;
	atArray<FingerprintKvp> m_FingerprintKvp;
	
};

} // namespace rgsc

#endif  //RLINE_RLPC_MFA_H
