@echo on

cd /d %RAGE_DIR%\suite\src\rgsc\rgsc\

set OPTIONS=/F /R /Y
set CEF_DIR=..\cef
set CEF32_DIR=..\cef32
set RGSC_UI_ZIP=..\scui.pak
set INSTALLER_DIR=..\installer
set THIRD_PARTY_DIR=..\thirdparty
set STEAM_DIR=%THIRD_PARTY_DIR%\steam

IF "%1"=="release" GOTO Release

:Debug

set DEST_DIR="%ProgramFiles(x86)%\Rockstar Games\Social Club Debug\"
set DEST_DIR_X64="%ProgramW6432%\Rockstar Games\Social Club Debug\"

REM UNCOMMENT ON BUILD MACHINE TO ENABLE SIGNING

REM IF "%2"=="x64" (
REM 	"%ProgramFiles(x86)%\Windows Kits\8.0\bin\x86\signtool.exe" sign /f "%RAGE_DIR%\suite\src\rgsc\rockstar.pfx" /p C!EZxYUxVGPzQDj3 /t http://timestamp.verisign.com/scripts/timstamp.dll /v "x64\Debug\socialclub.dll"
REM ) ELSE (
REM 	"%ProgramFiles(x86)%\Windows Kits\8.0\bin\x86\signtool.exe" sign /f "%RAGE_DIR%\suite\src\rgsc\rockstar.pfx" /p C!EZxYUxVGPzQDj3 /t http://timestamp.verisign.com/scripts/timstamp.dll /v "Debug\socialclub.dll"
REM )

xcopy "Debug\socialclub.dll" %DEST_DIR% %OPTIONS%
xcopy "Debug\socialclub.pdb" %DEST_DIR% %OPTIONS%
xcopy "Debug\socialclub.map" %DEST_DIR% %OPTIONS%
xcopy "Debug\SocialClubHelper.exe" %DEST_DIR% %OPTIONS%
xcopy "Debug\SocialClubHelper.pdb" %DEST_DIR% %OPTIONS%
xcopy "Debug\SocialClubHelper.map" %DEST_DIR% %OPTIONS%
xcopy "%INSTALLER_DIR%\uninstallRGSCRedistributableDebug.exe" %DEST_DIR% %OPTIONS%

xcopy "x64\Debug\socialclub.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "x64\Debug\socialclub.pdb" %DEST_DIR_X64% %OPTIONS%
xcopy "x64\Debug\socialclub.map" %DEST_DIR_X64% %OPTIONS%
xcopy "x64\Debug\SocialClubHelper.exe" %DEST_DIR_X64% %OPTIONS%
xcopy "x64\Debug\SocialClubHelper.pdb" %DEST_DIR_X64% %OPTIONS%
xcopy "x64\Debug\SocialClubHelper.map" %DEST_DIR_X64% %OPTIONS%
xcopy "%INSTALLER_DIR%\uninstallRGSCRedistributableDebug.exe" %DEST_DIR_X64% %OPTIONS%

REM Note: we're using the release version of the CEF binaries even in debug so that Chrome assertions don't crash the SocialClubHelper

xcopy "%CEF32_DIR%\Release\d3dcompiler_43.dll" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Release\d3dcompiler_47.dll" %DEST_DIR% %OPTIONS%
REM xcopy "%CEF32_DIR%\Release\ffmpegsumo.dll" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Release\libcef.dll" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Release\libEGL.dll" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Release\libGLESv2.dll" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Release\natives_blob.bin" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Release\snapshot_blob.bin" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Release\widevinecdmadapter.dll" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Resources\icudtl.dat" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Resources\cef.pak" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Resources\cef_100_percent.pak" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Resources\cef_200_percent.pak" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Resources\devtools_resources.pak" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Resources\locales" %DEST_DIR%\locales\ %OPTIONS% /E

xcopy "%CEF_DIR%\Release\swiftshader\libEGL.dll" %DEST_DIR_X64%\swiftshader\ %OPTIONS%
xcopy "%CEF_DIR%\Release\swiftshader\libGLESv2.dll" %DEST_DIR_X64%\swiftshader\ %OPTIONS%
xcopy "%CEF_DIR%\Release\chrome_elf.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\d3dcompiler_43.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\d3dcompiler_47.dll" %DEST_DIR_X64% %OPTIONS%
REM xcopy "%CEF_DIR%\Release\ffmpegsumo.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\libcef.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\libEGL.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\libGLESv2.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\natives_blob.bin" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\snapshot_blob.bin" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\v8_context_snapshot.bin" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\widevinecdmadapter.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Resources\icudtl.dat" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Resources\cef.pak" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Resources\cef_100_percent.pak" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Resources\cef_200_percent.pak" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Resources\devtools_resources.pak" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Resources\locales" %DEST_DIR_X64%\locales\ %OPTIONS% /E

REM No longer support Steam with the SDK - see RGSC_STEAM_EXPLICIT_SUPPORT in Steam.cpp
REM xcopy "%STEAM_DIR%\bin\steam_api.dll" %DEST_DIR% %OPTIONS%
REM xcopy "%STEAM_DIR%\bin\steam_api64.dll" %DEST_DIR_X64% %OPTIONS%

xcopy "%RGSC_UI_ZIP%" %DEST_DIR% %OPTIONS%
xcopy "%RGSC_UI_ZIP%" %DEST_DIR_X64% %OPTIONS%

GOTO End

:Release

set DEST_DIR="%ProgramFiles(x86)%\Rockstar Games\Social Club\"
set DEST_DIR_X64="%ProgramW6432%\Rockstar Games\Social Club\"

REM UNCOMMENT ON BUILD MACHINE TO ENABLE SIGNING

REM IF "%2"=="x64" (
REM 	"%ProgramFiles(x86)%\Windows Kits\8.0\bin\x86\signtool.exe" sign /f "%RAGE_DIR%\suite\src\rgsc\rockstar.pfx" /p C!EZxYUxVGPzQDj3 /t http://timestamp.verisign.com/scripts/timstamp.dll /v "x64\Release\socialclub.dll"
REM ) ELSE (
REM 	"%ProgramFiles(x86)%\Windows Kits\8.0\bin\x86\signtool.exe" sign /f "%RAGE_DIR%\suite\src\rgsc\rockstar.pfx" /p C!EZxYUxVGPzQDj3 /t http://timestamp.verisign.com/scripts/timstamp.dll /v "Release\socialclub.dll"
REM )

xcopy "Release\socialclub.dll" %DEST_DIR% %OPTIONS%
xcopy "Release\socialclub.pdb" %DEST_DIR% %OPTIONS%
xcopy "Release\socialclub.map" %DEST_DIR% %OPTIONS%
xcopy "Release\SocialClubHelper.exe" %DEST_DIR% %OPTIONS%
xcopy "Release\SocialClubHelper.pdb" %DEST_DIR% %OPTIONS%
xcopy "Release\SocialClubHelper.map" %DEST_DIR% %OPTIONS%
xcopy "%INSTALLER_DIR%\uninstallRGSCRedistributable.exe" %DEST_DIR% %OPTIONS%

xcopy "x64\Release\socialclub.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "x64\Release\socialclub.pdb" %DEST_DIR_X64% %OPTIONS%
xcopy "x64\Release\socialclub.map" %DEST_DIR_X64% %OPTIONS%
xcopy "x64\Release\SocialClubHelper.exe" %DEST_DIR_X64% %OPTIONS%
xcopy "x64\Release\SocialClubHelper.pdb" %DEST_DIR_X64% %OPTIONS%
xcopy "x64\Release\SocialClubHelper.map" %DEST_DIR_X64% %OPTIONS%
xcopy "%INSTALLER_DIR%\uninstallRGSCRedistributable.exe" %DEST_DIR_X64% %OPTIONS%

xcopy "%CEF32_DIR%\Release\d3dcompiler_43.dll" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Release\d3dcompiler_47.dll" %DEST_DIR% %OPTIONS%
REM xcopy "%CEF32_DIR%\Release\ffmpegsumo.dll" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Release\libcef.dll" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Release\libEGL.dll" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Release\libGLESv2.dll" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Release\natives_blob.bin" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Release\snapshot_blob.bin" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Release\widevinecdmadapter.dll" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Resources\icudtl.dat" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Resources\cef.pak" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Resources\cef_100_percent.pak" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Resources\cef_200_percent.pak" %DEST_DIR% %OPTIONS%
xcopy "%CEF32_DIR%\Resources\locales" %DEST_DIR%\locales\ %OPTIONS% /E

xcopy "%CEF_DIR%\Release\swiftshader\libEGL.dll" %DEST_DIR_X64%\swiftshader\ %OPTIONS%
xcopy "%CEF_DIR%\Release\swiftshader\libGLESv2.dll" %DEST_DIR_X64%\swiftshader\ %OPTIONS%
xcopy "%CEF_DIR%\Release\chrome_elf.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\d3dcompiler_43.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\d3dcompiler_47.dll" %DEST_DIR_X64% %OPTIONS%
REM xcopy "%CEF_DIR%\Release\ffmpegsumo.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\libcef.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\libEGL.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\libGLESv2.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\natives_blob.bin" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\snapshot_blob.bin" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\v8_context_snapshot.bin" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Release\widevinecdmadapter.dll" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Resources\icudtl.dat" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Resources\cef.pak" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Resources\cef_100_percent.pak" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Resources\cef_200_percent.pak" %DEST_DIR_X64% %OPTIONS%
xcopy "%CEF_DIR%\Resources\locales" %DEST_DIR_X64%\locales\ %OPTIONS% /E

REM xcopy "%STEAM_DIR%\bin\steam_api.dll" %DEST_DIR% %OPTIONS%
REM xcopy "%STEAM_DIR%\bin\steam_api64.dll" %DEST_DIR_X64% %OPTIONS%

xcopy "%RGSC_UI_ZIP%" %DEST_DIR% %OPTIONS%
xcopy "%RGSC_UI_ZIP%" %DEST_DIR_X64% %OPTIONS%

:End
pause
