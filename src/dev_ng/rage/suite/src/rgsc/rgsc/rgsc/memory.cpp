#include "memory.h"

#define SIMPLE_HEAP_SIZE rgsc::SIMPLE_HEAP_SIZE_IN_KB
#define SIMPLE_PHYSICAL_SIZE 32 // TODO: NS - not really sure what this is used for so make it something small (32k)

#include "system/main.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#define _WINSOCKAPI_ //Prevent windows.h from including winsock.h
#include <unknwn.h>
#pragma warning(pop)


#include "rgsc.h"
#include "system/simpleallocator.h"

// this is just to satisfy Rage since it thinks we're an exe
int Main() {return 0;}

using namespace rage;

namespace rgsc
{

extern sysMemAllocator* g_RgscAllocator;

#if __DEV
void* 
#if RAGE_TRACKING
rgscLoggedAllocate(const size_t size, const char* fileName, int lineNumber, const char* trackName)
#else
rgscLoggedAllocate(const size_t size, const char* fileName, int lineNumber, const char*)
#endif
{
    if (!g_RgscAllocator)
    {
        Errorf("g_RgscAllocator is null, possibly shutdown? can't allocate");
        return 0;
    }

    RAGE_TRACK(rgsc);
    RAGE_TRACK_NAME(trackName);

    return g_RgscAllocator->TryLoggedAllocate(size, 0, 0, fileName, lineNumber);
}
#else
void* 
rgscAllocate(const size_t size)
{
	if (!g_RgscAllocator)
	{
		Errorf("g_RgscAllocator is null, possibly shutdown? can't allocate");
		return 0;
	}

	return g_RgscAllocator->Allocate(size, 0, 0);
}
#endif

size_t
rgscGetAllocSize(const void* ptr)
{
    if (!g_RgscAllocator)
    {
        Errorf("g_RgscAllocator is null, possibly shutdown? can't get alloc size");
        return 0;
    }

    return g_RgscAllocator->GetSize(ptr);
}

void
rgscFree(const void* ptr)
{
    if(ptr)
    {
        if (!g_RgscAllocator)
        {
            Errorf("g_RgscAllocator is null, possibly shutdown? can't free");
            return;
        }

        Assert(g_RgscAllocator->IsValidPointer(ptr));
        g_RgscAllocator->Free(ptr);
    }
}

} //namespace rgsc
