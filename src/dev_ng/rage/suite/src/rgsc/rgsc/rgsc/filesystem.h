#ifndef RLINE_RLPCFILESYSTEM_H
#define RLINE_RLPCFILESYSTEM_H

#include "file/file_config.h"
#include "file_system_interface.h"
#include "profiles.h"

#if RSG_PC

#include "rgsc_common.h"

using namespace rage;

namespace rgsc
{

class RgscFileSystem : public IFileSystemLatestVersion
{
// ===============================================================================================
// inherited from public interfaces
// ===============================================================================================
public:

	static const char* UI_PATH;

	// ===============================================================================================
	// inherited from IRgscUnknown
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

	// ===============================================================================================
	// inherited from IFileSystemV1
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL GetTitleDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const;
	RGSC_HRESULT RGSC_CALL GetTitleProfileDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const;

	// ===============================================================================================
	// inherited from IFileSystemV2
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL GetProfileDirectoryId(char (&path)[RGSC_MAX_PATH]) const;

	// ===============================================================================================
	// inherited from IFileSystemV3
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL GetRootDataDirectory(char (&path)[RGSC_MAX_PATH]) const;

	// ===============================================================================================
	// inherited from IFileSystemV4
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL GetProfileDirectoryForTitle(char (&path)[RGSC_MAX_PATH], const char* titleDirectoryName, bool createDirectory) const;

	// ===============================================================================================
	// inherited from IFileSystemV5
	// ===============================================================================================
	virtual void RGSC_CALL EnableUtf8FilePaths(const bool bEnabled);

public:
    RgscFileSystem();
    virtual ~RgscFileSystem();

    bool Init();
    void Shutdown();
    void Update();

	bool GetRockstarGamesDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory, bool utf8) const;
	bool GetPlatformDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const;
	bool GetPlatformTitlesDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const;
	bool GetPlatformTitleDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const;
	bool GetPlatformTitleDirectoryName(char (&path)[RGSC_MAX_PATH]) const;
	bool GetPlatformProfilesDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const;
	bool GetPlatformProfileDirectory(char (&path)[RGSC_MAX_PATH], unsigned folderId, bool bCreateDirectory) const;
	u32 GetPlatformProfileDirectoryId(const UniqueKey& key) const;
	bool GetPlatformProfileDirectoryName(char (&path)[RGSC_MAX_PATH], const UniqueKey& key) const;
	bool GetPlatformProfileTitleDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const;
	bool GetPlatformCacheDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const;
	bool GetBrowserCacheDirectory(char (&path)[RGSC_MAX_PATH], bool launcher, bool bCreateDirectory) const;
	bool GetPlatformTempDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const;

	bool GetSocialClubDllName(char (&path)[RGSC_MAX_PATH]) const;
	bool GetSocialClubDllDirectory(char (&path)[RGSC_MAX_PATH]) const;
	bool GetSocialClubDllPath(char (&path)[RGSC_MAX_PATH]) const;
	bool GetSubprocessPath(char (&path)[RGSC_MAX_PATH]) const;

	bool GetProcessExeName(char (&path)[RGSC_MAX_PATH]) const;
	bool GetProcessExeDirectory(char (&path)[RGSC_MAX_PATH]) const;
	bool GetProcessExePath(char (&path)[RGSC_MAX_PATH]) const;

	bool GetSteamDllPath(char (&path)[RGSC_MAX_PATH]) const;

	bool GetOfflineUiAssetsDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const;
	bool GetOfflineUiAssetsDownloadDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const;

private:
	char m_RootDllDirectory[RGSC_MAX_PATH];
	char m_RootDataDirectory[RGSC_MAX_PATH];
	char m_RootDataDirectoryA[RGSC_MAX_PATH];
	bool m_bUtf8PathsEnabled;

	bool GetKnownFolderPath(int folderId, char (&path)[RGSC_MAX_PATH], bool utf8) const;
	bool CreateFolder(const char* folderPath) const;
};

}

#endif // RSG_PC

#endif  // RLINE_RLPCFILESYSTEM_H
