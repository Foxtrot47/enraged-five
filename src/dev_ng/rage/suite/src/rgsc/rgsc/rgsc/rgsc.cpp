#include "rgsc.h"
#include "configuration_interface.h"
#include "delegate_interface.h"
#include "titleid_interface.h"
#include "rgsctasks.h"
#include "rgsc_ui/input.h"
#include "rgsc_ui/script.h"

// rage includes
#include "data/base64.h"
#include "diag/seh.h"
#include "diag/tracker_remote2.h"
#include "net/http.h"
#include "net/nethardware.h"
#include "rline/rltitleid.h"
#include "rline/ros/rlros.h"
#include "rline/ros/rlrostitleid.h"
#include "rline/rltelemetry.h"
#include "rline/scmatchmaking/rlscmatchmaking.h"
#include "system/memvisualize.h"
#include "system/simpleallocator.h"
#include "system/timer.h"
#include "net/tcp.h"
#include "diag/output.h"
#include "rgsc/diag/output.h"

// shlobj + warnings
#pragma warning(push)
#pragma warning(disable: 4668)
#include <shlobj.h>
#pragma warning(pop)

// wininet
#include <Wininet.h>
#pragma comment(lib, "wininet.lib")

// version
#pragma comment(lib, "version.lib") 

// PARAMS
PARAM(rgsclogfile, "Enables RGSC logging to the specified log file");
PARAM(rgscLongShutdown, "Enables a 10 second delay in IsReadyToShutdown");
PARAM(noScuiDisconnect, "Disables the SCUI disconnect when internet connection is lost.");
PARAM(scNoDebugLog, "Disables the SCUI debug log");
PARAM(noConnectionStateChecks, "Disables connection state checks, always appears online");
PARAM(scMemVisualize, "Enable memvisualize");

// Ensure our Custom Windows Messages are correct.
CompileTimeAssert(CustomWindowMessages::MSG_FIRST == WM_USER);
CompileTimeAssert(CustomWindowMessages::MSG_LAST == WM_APP);
CompileTimeAssert(RgscReturnCodes::RGSC_INVALIDARG == E_INVALIDARG);
CompileTimeAssert(RgscReturnCodes::RGSC_NOINTERFACE == E_NOINTERFACE);
CompileTimeAssert(RgscReturnCodes::RGSC_FAIL == E_FAIL);
CompileTimeAssert(RgscReturnCodes::RGSC_OK == S_OK);

namespace rage
{
	PARAM(scofflineonly, "");
}

BOOLEAN WINAPI DllMain(IN HINSTANCE hDllHandle,
					   IN DWORD nReason, 
					   IN LPVOID Reserved)
{
	BOOLEAN bSuccess = TRUE;

	switch(nReason)
	{
		case DLL_PROCESS_ATTACH:
			break;

		case DLL_PROCESS_DETACH:
			Rgsc* rgsc = GetRgscConcreteInstance();
			if(rgsc)
			{
				rgsc->ProcessDetach();
			}
			break;
	}

	return bSuccess;
}

#if __DEV
extern rage::diagLogCallbackType rage::diagLogCallback;
#endif

namespace rgsc
{
PARAM(scemail, "the email to use to sign-in to the social club. Currently only works when -scnoui is specified.");
PARAM(scpassword, "the password to use to sign-in to the social club. Currently only works when -scnoui is specified.");
PARAM(scnoui, "can be used with -scemail and -scpassword to sign into the social club without a UI");

#if __DEV
PARAM(breakonalloc,"[startup] Break on alloc ordinal value - value in {braces} in debug output (use =0 first to stabilize allocations)");
#endif

#if __RAGE_DEPENDENCY
RAGE_DEFINE_CHANNEL(rgsc_dbg)
#endif

static Rgsc* sm_Instance = NULL;

// TODO: NS - this is used to store stats data for one frame, then it gets freed on the next frame.
// Do something better than this in the future once a better stats system is built.
static char* m_StatsData = NULL;

sysMemAllocator* g_RgscAllocator = NULL;

bool Rgsc::IsInitialized()
{
	return (sm_Instance != NULL) && sm_Instance->m_Initialized;
}

bool Rgsc::IsInitializedForOutput()
{
	return (sm_Instance != NULL) && (sm_Instance->m_Initialized || sm_Instance->m_PreInitializedOutput);
}

// ===========================================================================
// IRgscUnknown
// ===========================================================================
RGSC_HRESULT Rgsc::QueryInterface(RGSC_REFIID riid, LPVOID* ppvObj)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObj == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IRgsc*>(this);
	}
	else if(riid == IID_IRgscV1)
	{
		pUnknown = static_cast<IRgscV1*>(this);
	}
	else if(riid == IID_IRgscV2)
	{
		pUnknown = static_cast<IRgscV2*>(this);
	}
	else if(riid == IID_IRgscV3)
	{
		pUnknown = static_cast<IRgscV3*>(this);
	}
	else if(riid == IID_IRgscV4)
	{
		pUnknown = static_cast<IRgscV4*>(this);
	}
	else if(riid == IID_IRgscV5)
	{
		pUnknown = static_cast<IRgscV5*>(this);
	}
	else if(riid == IID_IRgscV6)
	{
		pUnknown = static_cast<IRgscV6*>(this);
	}
	else if(riid == IID_IRgscV7)
	{
		pUnknown = static_cast<IRgscV7*>(this);
	}
	else if (riid == IID_IRgscV8)
	{
		pUnknown = static_cast<IRgscV8*>(this);
	}

	*ppvObj = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

// ===========================================================================
// Init/Update/Shutdown
// ===========================================================================
Rgsc::Rgsc()
: m_Delegate(NULL)
, m_DelegateV1(NULL)
, m_DelegateV2(NULL)
, m_DelegateV3(NULL)
, m_FileDelegate(NULL)
, m_OfflineOnlyMode(false)
, m_PatchingEnabled(false)
, m_LocalProfilesEnabled(true)
, m_IsLauncher(false)
, m_IsUiDisabled(false)
, m_RgscAuthServices(RGSC_AUTH_ASMX)
, m_HandleAutoSignIn(false)
, m_Initialized(false)
, m_PreInitializedOutput(false)
, m_MetadataEnabled(true)
, m_UseHttpsForRosServices(false) // legacy behaviour
, m_TimeOfLastConnectionCheck(0)
, m_TimeOfLastConnected(0)
, m_WasConnectedToInternet(FALSE)
, m_RgscLoggingEnabled(false)
, m_GamepadSupport(IConfigurationV6::GAMEPADS_SUPPORTED)
, m_AdditionalWindowHandles(NULL)
, m_NumAdditionalWindowHandles(0)
, m_SignInTransferBehaviour(IConfigurationV8::DEFAULT)
, m_FriendsBehaviour(IConfigurationV8::DEFAULT)
, m_PresenceBehaviour(IConfigurationV8::DEFAULT)
, m_AchievementBehaviour(IConfigurationV8::DEFAULT)
, m_ConnectionChangeBehaviour(IConfigurationV8::DEFAULT)
, m_RosFilterBehaviour(IConfigurationV11::FILTER_ENABLED_WITH_SECURITY)
{
	memset(m_RgscLogPath, 0, sizeof(m_RgscLogPath));
	memset(m_AdditionalSessionAttr, 0, sizeof(m_AdditionalSessionAttr));
	memset(m_AdditionalJoinAttr, 0, sizeof(m_AdditionalJoinAttr));
	memset(m_MetaDataPath, 0, sizeof(m_MetaDataPath));
	memset(m_OfflineScuiPakPath, 0, sizeof(m_OfflineScuiPakPath));

	RgscDisplay("Rockstar Games Social Club SDK: %s %s", RSG_CPU_X86 ? "x86" : "x64", __DEV ? "DEBUG" : "RELEASE");
}

Rgsc::~Rgsc()
{
	RgscRetailLog::RgscCloseRetailLog();

	if (m_AdditionalWindowHandles)
	{
		delete[] m_AdditionalWindowHandles;
		m_AdditionalWindowHandles = NULL;
	}
}

RGSC_HRESULT Rgsc::CreateAllocator()
{
	// TODO: NS - how much memory?
// 	static const int TOTAL_SIZEOF_HEAP = 10240 * 1024;	// 10MB
// 	u8* heap = rage_new u8[TOTAL_SIZEOF_HEAP];
// 	g_RgscAllocator = rage_new sysMemSimpleAllocator(heap, TOTAL_SIZEOF_HEAP, sysMemSimpleAllocator::HEAP_APP_0, true);

	g_RgscAllocator = &sysMemAllocator::GetCurrent();
	g_RgscAllocator->BeginLayer();

//	g_RgscAllocator->SetQuitOnFail(false);

	return RGSC_OK;
}

RGSC_HRESULT Rgsc::InitRline(ITitleId* titleId, RgscLanguage language)
{
	RGSC_HRESULT hr = RGSC_FAIL;
	
	rtry
	{
		rverify(titleId, invalidArg, rlError("titleId cannot be NULL"));

		// determine which version of the titleId interface was passed to us
		ITitleIdV1* titleIdV1 = NULL;
		hr = titleId->QueryInterface(IID_ITitleIdV1, (void**) &titleIdV1);
		rverify(SUCCEEDED(hr) && (titleIdV1 != NULL), catchall, rlError("titleId doesn't support ITitleIdV1 interface"));

		rverify((titleIdV1->GetRosTitleName() != NULL) && (titleIdV1->GetRosTitleName()[0] != '\0'), catchall, rlError("ROS Title Name is invalid"));

		m_TitleId.SetRosTitleName(titleIdV1->GetRosTitleName());
		m_TitleId.SetRosEnvironment(titleIdV1->GetRosEnvironment());
		m_TitleId.SetRosTitleVersion(titleIdV1->GetRosTitleVersion());
		m_TitleId.SetTitleDirectoryName(titleIdV1->GetTitleDirectoryName());

		RgscDisplay(" Running title: %s", titleIdV1->GetRosTitleName());

		// in case hackers change the game's code to pass true for local profile support
		if(strcmp(titleIdV1->GetRosTitleName(), "mp3") == 0)
		{
			m_LocalProfilesEnabled = false;
		}

		const char* titleDirectoryName = m_TitleId.GetTitleDirectoryName();
		rverify((titleDirectoryName != NULL) && (titleDirectoryName[0] != '\0'), catchall, rlError("Title Directory Name is invalid"));

		ITitleIdV2* titleIdV2 = NULL;
		hr = titleId->QueryInterface(IID_ITitleIdV2, (void**) &titleIdV2);
		if(SUCCEEDED(hr) && (titleIdV2 != NULL))
		{
			m_TitleId.SetPlatform(titleIdV2->GetPlatform());
			RgscDisplay(" Platform: %s", titleIdV2->GetPlatform() == ITitleId::PLATFORM_STEAM ? "Steam" : "PC");

			char rootDataDir[RGSC_MAX_PATH];
			safecpy(rootDataDir, titleIdV2->GetRootDataDirectory());			

			// remove trailing slash if there is one
			size_t len = strlen(rootDataDir);

			if(len > 0)
			{
				if(rootDataDir[len - 1] == '\\' || rootDataDir[len - 1] == '/')
				{
					rootDataDir[len - 1] = '\0';
				}
			}

			m_TitleId.SetRootDataDirectory(rootDataDir);
		}
		else
		{
			// default to PC platform for the V1 interface
			m_TitleId.SetPlatform(ITitleId::PLATFORM_PC);
		}

		int scVersion = 999; // version passed by V1/V2 titles.
		const char* rosTitleSecrets = NULL;
		ITitleIdV3* titleIdV3 = NULL;
		hr = titleId->QueryInterface(IID_ITitleIdV3, (void**) &titleIdV3);
		if(SUCCEEDED(hr) && (titleIdV3 != NULL))
		{
			rosTitleSecrets = titleIdV3->GetRosTitleSecrets();
			scVersion = titleIdV3->GetScVersion();
		}

		ITitleIdV4* titleIdV4 = NULL;
		hr = titleId->QueryInterface(IID_ITitleIdV4, (void**) &titleIdV4);
		if(SUCCEEDED(hr) && (titleIdV4 != NULL))
		{
			m_TitleId.SetSteamAuthTicket(titleIdV4->GetSteamAuthTicket());
			m_TitleId.SetSteamAppId(titleIdV4->GetSteamAppId());
		}

		//@@: location RGSC_INITRLINE_CHECK_TITLEV5
		ITitleIdV5* titleIdV5 = NULL;
		hr = titleId->QueryInterface(IID_ITitleIdV5, (void**) &titleIdV5);
		if(SUCCEEDED(hr) && (titleIdV5 != NULL))
		{
			if((m_TitleId.GetPlatform() == ITitleId::PLATFORM_MAC_APP_STORE))
			{
				u8* receipt = NULL;
				int receiptLen = 0;
				titleIdV5->GetMacAppStoreReceipt(&receipt, &receiptLen);

				if(receiptLen > 0)
				{
					// make a copy of it so the calling process can delete its copy
					u8* receiptCopy = rage_new u8[receiptLen];
					if(AssertVerify(receiptCopy))
					{
						memcpy(receiptCopy, receipt, receiptLen);
						m_TitleId.SetMacAppStoreReceipt(receiptCopy, receiptLen);
					}
				}
			}
		}

		ITitleIdV6* titleIdV6 = NULL;
		hr = titleId->QueryInterface(IID_ITitleIdV6, (void**) &titleIdV6);
		if(SUCCEEDED(hr) && (titleIdV6 != NULL))
		{
			m_TitleId.SetSteamId(titleIdV6->GetSteamId());

			const u8* publicRsaKey = NULL;
			unsigned publicRsaKeyLen = 0;
			titleIdV6->GetPublicRsaKey(&publicRsaKey, &publicRsaKeyLen);
			m_TitleId.SetPublicRsaKey(publicRsaKey, publicRsaKeyLen);
		}

		ITitleIdV7* titleIdV7 = NULL;
		hr = titleId->QueryInterface(IID_ITitleIdV7, (void**) &titleIdV7);
		if(SUCCEEDED(hr) && (titleIdV7 != NULL))
		{
			m_TitleId.SetSteamPersona(titleIdV7->GetSteamPersona());
			if (IsSteam())
			{
				RgscDisplay(" Steam user: %s", m_TitleId.GetSteamPersona());
			}
		}

		const char* hostNamePrefix = NULL;
		ITitleIdV8* titleIdV8 = NULL;
		hr = titleId->QueryInterface(IID_ITitleIdV8, (void**) &titleIdV8);
		if(SUCCEEDED(hr) && (titleIdV8 != NULL))
		{
			hostNamePrefix = titleIdV8->GetHostnamePrefix();
			if (hostNamePrefix && hostNamePrefix[0] != '\0')
			{
				m_TitleId.SetHostnamePrefix(hostNamePrefix);
				RgscDisplay(" Using prefix '%s' for Social Club Services", hostNamePrefix);
			}
		}

		rverify(m_TitleId.GetPlatform() != ITitleId::PLATFORM_INVALID, catchall, rlError("Platform is invalid"));

		// for the Mac App Store version, enable local profiles (it's an Apple requirement)
		if((m_TitleId.GetPlatform() == ITitleId::PLATFORM_MAC_APP_STORE))
		{
			m_LocalProfilesEnabled = true;
		}

		// these characters are invalid in filenames on Windows
		char invalidChars[] = {'\\', '/', '*' , '?', '<', '>', '|', '"'};
		for(unsigned int i = 0; i < sizeof(invalidChars); i++)
		{
			rverify(strchr(titleDirectoryName, invalidChars[i]) == NULL, catchall, rlError("title directory name contains invalid characters"));
		}

		// convert between our publicly-exposed language enum and rlLanguage enum
		sysLanguage rlLang = LANGUAGE_UNDEFINED;
		//@@: location RGSC_INITRLINE_SWITCH_LANGUAGE
		switch(language)
		{
			case RGSC_LANGUAGE_ENGLISH:
				RgscDisplay(" Language: English");
				rlLang = LANGUAGE_ENGLISH;
				break;
			case RGSC_LANGUAGE_SPANISH:
				RgscDisplay(" Language: Spanish");
				rlLang = LANGUAGE_SPANISH;
				break;
			case RGSC_LANGUAGE_FRENCH:
				RgscDisplay(" Language: French");
				rlLang = LANGUAGE_FRENCH;
				break;
			case RGSC_LANGUAGE_GERMAN:
				RgscDisplay(" Language: German");
				rlLang = LANGUAGE_GERMAN;
				break;
			case RGSC_LANGUAGE_ITALIAN:
				RgscDisplay(" Language: Italian");
				rlLang = LANGUAGE_ITALIAN;
				break;
			case RGSC_LANGUAGE_JAPANESE:
				RgscDisplay(" Language: Japanese");
				rlLang = LANGUAGE_JAPANESE;
				break;
			case RGSC_LANGUAGE_RUSSIAN:
				RgscDisplay(" Language: Russian");
				rlLang = LANGUAGE_RUSSIAN;
				break;
			case RGSC_LANGUAGE_PORTUGUESE:
				RgscDisplay(" Language: Portuguese");
				rlLang = LANGUAGE_PORTUGUESE;
				break;
			case RGSC_LANGUAGE_POLISH:
				RgscDisplay(" Language: Polish");
				rlLang = LANGUAGE_POLISH;
				break;
			case RGSC_LANGUAGE_KOREAN:
				RgscDisplay(" Language: Korean");
				rlLang = LANGUAGE_KOREAN;
				break;
			case RGSC_LANGUAGE_CHINESE_TRADITIONAL:
				RgscDisplay(" Language: Chinese Traditional");
				rlLang = LANGUAGE_CHINESE_TRADITIONAL;
				break;
			case RGSC_LANGUAGE_CHINESE_SIMPLIFIED:
				RgscDisplay(" Language: Chinese Simplified");
				rlLang = LANGUAGE_CHINESE_SIMPLIFIED;
				break;
			case RGSC_LANGUAGE_MEXICAN:
				RgscDisplay(" Language: Mexican");
				rlLang = LANGUAGE_MEXICAN;
				break;
			default:
				Assertf(false, "unknown language specified: %d", language);
				RgscDisplay(" Language: Unknown (English)");
				rlLang = LANGUAGE_ENGLISH;
				break;
		}

		// convert between our publicly-exposed env enum and RLROS env enum
		ITitleId::RosEnvironment rosEnv = titleIdV1->GetRosEnvironment();

		rlRosEnvironment rlRosEnv = RLROS_ENV_UNKNOWN;
		switch(rosEnv)
		{
			case ITitleId::RLROS_ENV_RAGE:
				rlRosEnv = RLROS_ENV_RAGE;
				RgscDisplay(" Environment: RAGE");
				break;
			case ITitleId::RLROS_ENV_DEV:
				rlRosEnv = RLROS_ENV_DEV;
				RgscDisplay(" Environment: Dev");
				break;
			case ITitleId::RLROS_ENV_CERT:
				rlRosEnv = RLROS_ENV_CERT;
				RgscDisplay(" Environment: Cert");
				break;
			case ITitleId::RLROS_ENV_PROD:
				rlRosEnv = RLROS_ENV_PROD;
				RgscDisplay(" Environment: Production");
				break;
			case ITitleId::RLROS_ENV_CI_RAGE:
				rlRosEnv = RLROS_ENV_CI_RAGE;
				RgscDisplay(" Environment: CI Rage");
				break;
			default:
				rlRosEnv = RLROS_ENV_UNKNOWN;
				RgscDisplay(" Environment: Unknown");
				break;
		}

		rverify(rlRosEnv != RLROS_ENV_UNKNOWN, catchall, rlError("unknown ROS environment specified: %d", rlRosEnv));

		//Create an instance of the network hardware and start it.
		netInit(g_RgscAllocator, 0, 0);

		// Setup the public RSA key
		const u8* publicRsaKey = NULL;
		unsigned publicRsaKeyLen = 0;
		m_TitleId.GetPublicRsaKey(&publicRsaKey, &publicRsaKeyLen);

		// Setup the ROS title ID
		rlRosTitleId rosTitleId(titleIdV1->GetRosTitleName(), titleDirectoryName, scVersion, titleIdV1->GetRosTitleVersion(), rosTitleSecrets, rlRosEnv, publicRsaKey, publicRsaKeyLen);
		rosTitleId.SetDomainNamePrefix(hostNamePrefix);
		rosTitleId.SetUseHttpsByDefault(m_UseHttpsForRosServices);

		// Setup ROS filtering
		//  FILTER_DISABLED - No Filter, No security
		//	FILTER_ENABLED - Filtered, No Security
		//  FILTER_ENABLED_WITH_SECURITY - Filtered, Security Default
		if (m_RosFilterBehaviour == IConfigurationV11::FILTER_DISABLED)
		{
			rosTitleId.SetUseFilterByDefault(false);
			rosTitleId.SetDefaultSecurityFlags(RLROS_SECURITY_NONE);
		}
		else if (m_RosFilterBehaviour == IConfigurationV11::FILTER_ENABLED)
		{
			rosTitleId.SetUseFilterByDefault(true);
			rosTitleId.SetDefaultSecurityFlags(RLROS_SECURITY_NONE);
		}
		else // FILTER_ENABLED_WITH_SECURITY
		{
			rosTitleId.SetUseFilterByDefault(true);
			rosTitleId.SetDefaultSecurityFlags(RLROS_SECURITY_DEFAULT);
		}

		// Setup the rline title id
	    rlTitleId rlineTitleId(rosTitleId);

		// TODO: NS - age rating?
		const unsigned MIN_AGE_RATING = 0;
		rverify(rlInit(g_RgscAllocator, netRelay::GetSocket(), &rlineTitleId, MIN_AGE_RATING), catchall, rlError("rlInit failed"));
		
		// localisation
		rlSetLanguage(rlLang);

		// Determine if we should use Launcher Services
		if (IsTitleMTL())
		{
			rlRosHttpTask::SetDefaultServices(rlRosHttpTask::LAUNCHER_SERVICES);
		}

		// telemetry
		rlPcTelemetryPolicy policy;
		policy.SetSubmissionIntervalSeconds(rlTelemetryPolicies::DEFAULT_SUBMISSION_INTERVAL_SECONDS);
		policy.SetSubmissionTimeoutSeconds(rlTelemetryPolicies::DEFAULT_SUBMISSION_INTERVAL_SECONDS);
		policy.SetLogLevel(ITelemetryPolicyV1::RGSC_LOGLEVEL_DEBUG_NEVER);
		m_TelemetryManager.SetPolicies(&policy);

		RgscRetailLog::AddInterval();
		RgscDisplay("Network initialized");

		RgscDisplay(" Presence Supported: %d", IsPresenceSupported());
		RgscDisplay(" Friends Supported: %d", IsFriendsSupported());
		RgscDisplay(" Connection State Change Supported: %d", IsConnectionStateChangeSupported());
		RgscDisplay(" Achievements Supported: %d", IsAchievementsSupported());
		RgscDisplay(" Autologin/Transfer Supported: %d", IsSignInTransferAutoLoginSupported());

		hr = RGSC_OK;
	}
	rcatch(invalidArg)
	{
		hr = RGSC_INVALIDARG;
	}
	rcatchall
	{
		hr = RGSC_FAIL;
	}

	return hr;
}

#if __DEV

const char* rgsc_severity_to_str(diagSeverity s)
{
	switch(s)
	{
	case DIAG_SEVERITY_FATAL:
		return "Fatal: ";
	case DIAG_SEVERITY_ASSERT:
		return "Assert: ";
	case DIAG_SEVERITY_ERROR:
		return "Error: ";
	case DIAG_SEVERITY_WARNING: 
		return "Warning: ";
	case DIAG_SEVERITY_DISPLAY:
		return "";
	case DIAG_SEVERITY_DEBUG1:
		return "Debug1: ";
	case DIAG_SEVERITY_DEBUG2:
		return "Debug2: ";
	case DIAG_SEVERITY_DEBUG3:
		return "Debug3: ";
	case DIAG_SEVERITY_COUNT:
		return "Count: ";
	}
	return "";
}

bool RageOutputCallback(const diagChannel &channel,diagSeverity severity,const char *file,int line,const char *fmt, va_list args)
{
	if(!Rgsc::IsInitializedForOutput())
	{
		return true;
	}

	int rgscSeverity = IRgscDelegateLatestVersion::RGSC_OUTPUT_SEVERITY_INFO;

	switch(severity)
	{
	case DIAG_SEVERITY_FATAL:
	case DIAG_SEVERITY_ASSERT:
		rgscSeverity = IRgscDelegateLatestVersion::RGSC_OUTPUT_SEVERITY_ASSERT;
		break;
	case DIAG_SEVERITY_ERROR:
		rgscSeverity = IRgscDelegateLatestVersion::RGSC_OUTPUT_SEVERITY_ERROR;
		break;
	case DIAG_SEVERITY_WARNING:
		rgscSeverity = IRgscDelegateLatestVersion::RGSC_OUTPUT_SEVERITY_WARNING;
		break;
	default:
		rgscSeverity = IRgscDelegateLatestVersion::RGSC_OUTPUT_SEVERITY_INFO;
		break;
	}

	static u32 firstLogTime = sysTimer::GetSystemMsTime();
	u32 thisTimer = sysTimer::GetSystemMsTime() - firstLogTime;

	char msg[1024];
	vformatf(msg,sizeof(msg),fmt,args);

	if (channel.Tag && channel.Tag[0] != '\0')
	{
		OutputMsg(rgscSeverity, file, line, NULL, NULL, "[%08u] %s%s %s", thisTimer, rgsc_severity_to_str(severity), channel.Tag, msg);
	}
	else
	{
		OutputMsg(rgscSeverity, file, line, NULL, NULL, "[%08u] %s%s", thisTimer, rgsc_severity_to_str(severity), msg);
	}
	
	return true;
}

void RagePrintCallback(const char* msg)
{
	OutputMsg(IRgscDelegateLatestVersion::RGSC_OUTPUT_SEVERITY_INFO, "", 0, NULL, NULL, "%s", msg);
}
#endif

RGSC_HRESULT Rgsc::InitRage(IConfiguration* config, ITitleId* titleId, RgscLanguage language)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
#if __DEV
		diagLogCallback = rgsc::RageOutputCallback;
		diagPrintCallback = rgsc::RagePrintCallback;
#endif

		if(config)
		{
			//@@: range RGSC_INITRAGE_QUERY_INTERFACES {
			IConfigurationV1* configV1 = NULL;
			hr = config->QueryInterface(IID_IConfigurationV1, (void**) &configV1);
			rverify(SUCCEEDED(hr) && (configV1 != NULL), catchall, rlError("configuration doesn't support the IConfigurationV1 interface"));

			static int argc = configV1->GetCommandLineParamCount();
			static char **argv = configV1->GetCommandLineParams();

			ReadResponseFile(argc,argv);

			sysParam::Init(argc,argv);
			InitMemVisualize();

			RgscRetailLog::AddInterval();
			RgscDisplay("Settings:");

			IConfigurationV2* configV2 = NULL;
			//@@: location RGSC_INITRAGE_QUERY_V2_INTERFACE
			hr = config->QueryInterface(IID_IConfigurationV2, (void**) &configV2);
			if(SUCCEEDED(hr) && (configV2 != NULL))
			{
				m_OfflineOnlyMode = configV2->IsOfflineOnlyMode();
				RgscDisplay(" Offline Only Mode: %s", m_OfflineOnlyMode ? "true" : "false");
				m_PatchingEnabled = configV2->IsPatchingEnabled();
			}

			IConfigurationV3* configV3 = NULL;
			hr = config->QueryInterface(IID_IConfigurationV3, (void**) &configV3);
			if(SUCCEEDED(hr) && (configV3 != NULL))
			{
				m_LocalProfilesEnabled = configV3->AreLocalProfilesEnabled();
			}

			IConfigurationV4* configV4 = NULL;
			hr = config->QueryInterface(IID_IConfigurationV4, (void**) &configV4);
			if(SUCCEEDED(hr) && (configV4 != NULL))
			{
				m_IsLauncher = configV4->IsLauncher();
				RgscDisplay(" Launcher Mode: %s", m_IsLauncher ? "true" : "false");
				m_HandleAutoSignIn = configV4->HandleAutoSignIn();
			}

			IConfigurationV5* configV5 = NULL;
			hr = config->QueryInterface(IID_IConfigurationV5, (void**) &configV5);
			if(SUCCEEDED(hr) && (configV5 != NULL))
			{
				safecpy(m_AdditionalSessionAttr, configV5->GetAdditionalSessionAttr());
				safecpy(m_AdditionalJoinAttr, configV5->GetAdditionalJoinAttr());
				safecpy(m_MetaDataPath, configV5->GetMetaDataPath());
			}

			IConfigurationV6* configV6 = NULL;
			hr = config->QueryInterface(IID_IConfigurationV6, (void**) &configV6);
			if(SUCCEEDED(hr) && (configV6 != NULL))
			{
				m_GamepadSupport = configV6->GetGamePadSupport();
			}

			IConfigurationV7* configV7 = NULL;
			hr = config->QueryInterface(IID_IConfigurationV7, (void**) &configV7);
			if (SUCCEEDED(hr) && (configV7 != NULL))
			{
				// Query to see if we have additional window handles
				unsigned numAdditionalWindowHandles = configV7->GetNumAdditionalWindowHandles();
				if (numAdditionalWindowHandles > 0)
				{
					// Allocate additional handles and copy over.
					m_AdditionalWindowHandles = rage_new WindowHandle[numAdditionalWindowHandles];
					if (m_AdditionalWindowHandles)
					{
						WindowHandle* handles = configV7->GetAdditionalWindowHandles();
						for (unsigned i = 0; i < numAdditionalWindowHandles; i++)
						{
							m_AdditionalWindowHandles[i] = handles[i];
						}
						m_NumAdditionalWindowHandles = numAdditionalWindowHandles;
					}
				}
			}

			IConfigurationV8* configV8 = NULL;
			hr = config->QueryInterface(IID_IConfigurationV8, (void**)&configV8);
			if (SUCCEEDED(hr) && (configV8 != NULL))
			{
				m_SignInTransferBehaviour = configV8->GetSignIntransferBehaviour();
				m_FriendsBehaviour = configV8->GetFriendsBehaviour();
				m_PresenceBehaviour = configV8->GetPresenceBehaviour();
				m_AchievementBehaviour = configV8->GetAchievementBehaviour();
				m_ConnectionChangeBehaviour = configV8->GetConnectionStateChangeBehaviour();
			}

			IConfigurationV9* configV9 = NULL;
			hr = config->QueryInterface(IID_IConfigurationV9, (void**)&configV9);
			if (SUCCEEDED(hr) && (configV9 != NULL))
			{
				safecpy(m_OfflineScuiPakPath, configV9->GetOfflineScuiPakPath());
				m_MetadataEnabled = configV9->IsMetadataEnabled();
			}

			IConfigurationV10* configV10 = NULL;
			hr = config->QueryInterface(IID_IConfigurationV10, (void**)&configV10);
			if (SUCCEEDED(hr) && (configV10 != NULL))
			{
				m_IsUiDisabled = !configV10->IsUiEnabled();
				m_RgscAuthServices = configV10->GetAuthServices();
			}

			IConfigurationV11* configV11 = NULL;
			hr = config->QueryInterface(IID_IConfigurationV11, (void**)&configV11);
			if (SUCCEEDED(hr) && (configV11 != NULL))
			{
				m_UseHttpsForRosServices = configV11->UseHttpsForRosServices();
				m_RosFilterBehaviour = configV11->GetRosFilterBehaviour();
				m_Ui.SetOnlineUrl(configV11->GetOnlineUrl());
			}

			//@@: } RGSC_INITRAGE_QUERY_INTERFACES
		}
#if __DEV
		else
		{
			static char* params[] = {"rgsc"};
			static char **argv = params;
			static int argc = 1;
			ReadResponseFile(argc, argv);
			sysParam::Init(argc,argv);
		}
		OUTPUT_ONLY(diagChannel::InitArgs());

		if (PARAM_breakonalloc.Get())
		{
			int allocId;
			PARAM_breakonalloc.Get(allocId);
			sysMemAllocator::GetCurrent().SetBreakOnAlloc(allocId);
		}
#endif

#if __DEV
		const char* rgscPath;
		if (PARAM_rgsclogfile.Get(rgscPath))
		{
			safecpy(m_RgscLogPath, rgscPath);
			m_RgscLoggingEnabled = true;
			RgscOpenRageLog();
		}

		if(PARAM_scofflineonly.Get())
		{
			m_OfflineOnlyMode = true;
		}
#endif

        //@@: location RGSC_INITRAGE_INITRLINE
		hr = InitRline(titleId, language);
		rverify(SUCCEEDED(hr), catchall, rlError("InitRline failed. hr = 0x%X", hr));

		RgscRetailLog::AddInterval();
		RgscDisplay("RAGE initialized");
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT Rgsc::SetDelegate(IRgscDelegate* dlgt)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(dlgt, invalidArg, rlError("delegate cannot be NULL"));

		// determine which version of our delegate the client supports
		IRgscDelegateV1* v1 = NULL;
		hr = dlgt->QueryInterface(IID_IRgscDelegateV1, (void**) &v1);
		rverify(SUCCEEDED(hr) && (v1 != NULL), catchall, rlError("delegate doesn't support the IRgscDelegateV1 interface"));

		m_Delegate = dlgt;
		m_DelegateV1 = v1;

		IRgscDelegateV2* v2 = NULL;
		hr = dlgt->QueryInterface(IID_IRgscDelegateV2, (void**) &v2);
		if(SUCCEEDED(hr) && (v2 != NULL))
		{
			m_DelegateV2 = v2;
		}

		IRgscDelegateV3* v3 = NULL;
		hr = dlgt->QueryInterface(IID_IRgscDelegateV3, (void**) &v3);
		if(SUCCEEDED(hr) && (v3 != NULL))
		{
			m_DelegateV3 = v3;
		}

		hr = RGSC_OK;
	}
	rcatch(invalidArg)
	{
		hr = RGSC_INVALIDARG;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT Rgsc::UpdateRegistry()
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		// The Social Club installer sets the version in the registry and uses it for version checking when installing newer versions.
		// However, the Social Club used to update itself using Yummy Interactive's patching system which doesn't update the registry.
		// To keep the registry up-to-date with what is actually installed, we update the version in the registry when the DLL is running.

		// HKEY_LOCAL_MACHINE\SOFTWARE\Rockstar Games\Rockstar Games Social Club.
		// (on 64-bit Windows, this will automatically redirect to HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Rockstar Games\Rockstar Games Social Club).
		char version[128];
		rverify(GetVersionInfo(version), catchall, );

		RgscDisplay("Version Info: %s", version);

		HKEY hKey;
		LONG result = RegOpenKeyExA(HKEY_LOCAL_MACHINE, "Software\\Rockstar Games\\Rockstar Games Social Club\\", 0, KEY_WRITE | KEY_WOW64_32KEY, &hKey);
		rcheck(result == ERROR_SUCCESS, catchall, );
		RegSetValueExA(hKey, "Version", 0, REG_SZ, (BYTE*)version, sizeof(version));
		RegCloseKey(hKey);	

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

bool Rgsc::DeleteTempDirectory()
{
	char path[RGSC_MAX_PATH] = {0};

	if(!_GetFileSystem()->GetPlatformTempDirectory(path, false))
	{
		return false;
	}

	const fiDevice *device = fiDevice::GetDevice(path, false);
	if(device)
	{
		bool exists = device->GetAttributes(path) == FILE_ATTRIBUTE_DIRECTORY;
		if(exists)
		{
			return fiDevice::DeleteDirectory(path, true);
		}
	}
	return true;
}

bool rgsc_is_ws(char ch) 
{ 
	return ch==32 || ch==9 || ch==10 || ch==13; 
}

// heavily inspired by main.txt RecursiveReadResponseFile, retrofitted for RGSC use
void Rgsc::ReadResponseFile(int &argc, char** &argv) 
{
	const int ARGV_BUF_SIZE = 8192;
	const int maxArgs = (ARGV_BUF_SIZE/16);

	static char argv_heap[ARGV_BUF_SIZE];
	static char *new_argv[maxArgs];

	int oldArgc = argc;
	char **oldArgv = argv;

	// If any args were specified, keep argv[0] at the front. The rest will be appended later.
	if (oldArgc > 0)
	{
		new_argv[0] = argv[0];
		argc = 1;
	}

	// point argv to our static arguments
	argv = new_argv;

	fiStream *S = NULL;

	rtry
	{
		wchar_t pathW[RGSC_MAX_PATH] = {0};
		rverify(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, SHGFP_TYPE_CURRENT, pathW) == RGSC_OK, catchall, );

		char path[RGSC_MAX_PATH] = {0};
		rverify(WideCharToMultiByte(CP_UTF8, 0, pathW, -1, path, COUNTOF(path), 0, 0) != 0, catchall, );

#if __DEV
		safecat(path, "\\Rockstar Games\\Social Club Debug\\commandline.txt");
#else
		safecat(path, "\\Rockstar Games\\Social Club\\commandline.txt");
#endif

		S = fiStream::Open(path,true);
		if (S) 
		{
			rverify(S->Size() < ARGV_BUF_SIZE-1, catchall, rlError("RGSC commandline.txt was too large"));

			/*int offset = */S->Read(argv_heap,ARGV_BUF_SIZE-1);
			S->Close();
			S = NULL;

			char *p = argv_heap;
			while (argc < maxArgs-1) 
			{
				while (rgsc_is_ws(*p)) 
				{
					++p;
				}

				if (*p == '"') 
				{
					argv[argc++] = ++p;
					while (*p && *p != '"')
					{
						++p;
					}

					if (*p) 
					{
						*p++ = 0;
					}
					else 
					{
						break;
					}
				}
				// Check for comments in response files
				else if(';' == *p || '#' == *p || ('/' == *p && '/' == p[1]))
				{
					while(*p && '\n' != *p)
					{
						++p;
					}

					if(!*p) 
					{
						break;
					}
				}
				else if (*p) 
				{
					argv[argc++] = p;
					while (*p && !rgsc_is_ws(*p)) 
					{
						++p;
					}

					if (*p)
					{
						*p++ = 0;
					}

					// No recursion
					//if (*argv[argc-1]=='@') 
					//{
					//	--argc;
					//	RecurseResponseFile(argv[argc]+1,argc,argv,maxArgs,argv_heap + offset + 1,argv_heap_size - offset - 1);
					//}

					if (!*p)
					{
						break;
					}
				}
				else 
				{
					break;
				}
			}
		}
	}
	rcatchall
	{

	}

	if (S)
	{
		S->Close();
		S = NULL;
	}

	Displayf("SC SDK command line params:");
	for(int i = 0; i < argc; i++)
	{
		Displayf("\t %s", argv[i]);
	}

	// Append any additional args past the response file.
	for (int i=1; i<oldArgc; i++)
	{
		if (oldArgv[i][0] != '@' && argc<maxArgs-1)
		{
			argv[argc++] = oldArgv[i];
		}
	}

	argv[argc] = 0;
}

void Rgsc::InitMemVisualize()
{
#if __DEV
	// memvisualize is dev-only and must be enabled using the command line
	if (!PARAM_scMemVisualize.Get())
		return;

	sysMemVisualize& memVisualize = sysMemVisualize::GetInstance();
	memVisualize.Set("adfghnpru");

	++g_TrackerDepth;

	int memVisualizePort = 0;
	diagTrackerRemote2* t = rage_new diagTrackerRemote2(false, memVisualizePort);

	sysMemAllocator& master = sysMemAllocator::GetMaster();
	if (memVisualize.HasGame() || memVisualize.HasMisc())
	{
		sysMemSimpleAllocator* pSimple = dynamic_cast<sysMemSimpleAllocator*>(master.GetAllocator(MEMTYPE_GAME_VIRTUAL));
		if (pSimple)
			t->InitHeap("RGSC", master.GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetHeapBase(), master.GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetHeapSize());
	}

	diagTracker::SetCurrent(t);
	--g_TrackerDepth;

	t->StartNetworkWorker();
#endif
}

void Rgsc::SendSteamInfoToScui()
{
	char buf[512] = {0};
	rtry
	{
		RsonWriter rw;
		rw.Init(buf, sizeof(buf), RSON_FORMAT_JSON);
		rcheckall(rw.Begin(NULL, NULL));

		char szSteamId[64];
		formatf(szSteamId, "%" I64FMT "u", m_TitleId.GetSteamId());

		rcheckall(rw.WriteString("SteamId", szSteamId));
		rcheckall(rw.WriteUns("SteamAppId", m_TitleId.GetSteamAppId()));
		rcheckall(rw.WriteString("SteamPersona", m_TitleId.GetSteamPersona()));
		rcheckall(rw.End());

		bool succeeded = false;
		rgsc::Script::JsSteamInfoChanged(&succeeded, buf);
	}
	rcatchall
	{
	}
}

void Rgsc::NotifyFatalError(int errCode)
{
	RgscError("Fatal Error: code %d", errCode);
	HandleNotification(rgsc::NOTIFY_FATAL_ERROR, &errCode);
}

void Rgsc::SetFileDelegate(IFileDelegate* fileDelegate)
{
	m_FileDelegate = fileDelegate;
}

// ===============================================================================================
// inherited from IRgscV4
// ===============================================================================================
void Rgsc::SetSteamAuthTicket(const char* steamAuthTicket)
{
	m_TitleId.SetSteamAuthTicket(steamAuthTicket);
	
	bool succeeded = false;
	const int FORMAT_BUF = 16;
	const int MAX_TEXT_LEN = 2049;
	const int BUF_LEN = MAX_TEXT_LEN + FORMAT_BUF;
	
	char ticket[BUF_LEN] = {0};
	rtry
	{
		RsonWriter rw;
		rw.Init(ticket, sizeof(ticket), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		rw.WriteString("Ticket", steamAuthTicket);
		rcheck(rw.End(), catchall, );

		rgsc::Script::JsRefreshSteamTicketResult(&succeeded, ticket);
	}
	rcatchall
	{
		rgsc::Script::JsRefreshSteamTicketResult(&succeeded, "");
	}
}

// ===============================================================================================
// inherited from IRgscV5
// ===============================================================================================
RGSC_HRESULT Rgsc::OnGameCrashed(u64 actionToTake)
{
	if (GetRgscConcreteInstance()->_GetProfileManager()->IsOnline())
	{
		return CreateGameCrashTask(actionToTake) ? RGSC_OK : S_FALSE;
	}

	return RGSC_OK;
}

bool Rgsc::IsReadyToShutdown()
{
	// We perform some cleanup on game crash, we're not ready to shut down until this is no longer pending.
	if (m_GameCrashStatus.Pending())
	{
		return false;
	}

	// If a telemetry flush is in progress, we're not ready to shut down
	if (rlTelemetry::FlushInProgress())
	{
		return false;
	}

	// If we're waiting for a SCUI shutdown
	if (m_Ui.IsWaitingToShutdown())
	{
		return false;
	}

	// Other critical shutdown logic can go here...

	// ...

#if __DEV
	if (PARAM_rgscLongShutdown.Get())
	{
		static u32 requestTime = sysTimer::GetSystemMsTime();
		if (!sysTimer::HasElapsedIntervalMs(requestTime, 10000))
			return false;
	}
#endif

	return true;
}

// ===============================================================================================
// inherited from IRgscV8
// ===============================================================================================

void Rgsc::SetSteamPersona(const char* steamPersona)
{
	rlDebug("Updating Steam Persona from '%s' to '%s'", m_TitleId.GetSteamPersona(), steamPersona);
	m_TitleId.SetSteamPersona(steamPersona);
	SendSteamInfoToScui();
}

void Rgsc::SetSteamId(u64 steamId)
{
	rlDebug("Updating Steam ID from '%" I64FMT "u' to '%" I64FMT "u'", m_TitleId.GetSteamId(), steamId);
	m_TitleId.SetSteamId(steamId);
	SendSteamInfoToScui();
}

void Rgsc::SetSteamAppId(u32 steamAppId)
{
	rlDebug("Updating Steam App ID from '%u' to '%u'", m_TitleId.GetSteamAppId(), steamAppId);
	m_TitleId.SetSteamAppId(steamAppId);
	SendSteamInfoToScui();
}

// ===========

RGSC_HRESULT Rgsc::Init(IConfiguration* config, ITitleId* titleId, RgscLanguage language, IRgscDelegate* dlgt)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(!m_Initialized, catchall, rlError("Rgsc::Init() - already initialized"));

		// make sure to set the delegate first so we can report initialization errors
		hr = SetDelegate(dlgt);
		rverify(SUCCEEDED(hr), catchall, rlError("error setting the delegate"));

		hr = CreateAllocator();
		rverify(SUCCEEDED(hr), catchall, rlError("error creating the allocator"));

		hr = InitRage(config, titleId, language);
		rverify(SUCCEEDED(hr), catchall, NotifyFatalError(FATAL_ERROR_INIT_RAGE));

		// Initialize for output after Rage to reduce spam
		m_PreInitializedOutput = true;

		rverify(m_FileSystem.Init(), unknownError, NotifyFatalError(FATAL_ERROR_INIT_FILE_SYSTEM));

		// Open the Retail Log now that the FIle System is initialized.
		RgscRetailLog::RgscOpenRetailLog();
		RgscDisplay("File System Initialized");

		RgscRetailLog::AddInterval();
		RgscDisplay("Command Line Arguments:");

		const int MAX_CL_ARGS_RESULT_COUNT = 100;
		for(int i = 1; i < sysParam::GetArgCount() && i < MAX_CL_ARGS_RESULT_COUNT; i++)
		{
			RgscDisplay("\t %s", sysParam::GetArg(i));
		}
		RgscRetailLog::AddInterval();

		//@@: location RGSC_INIT_UPDATE_REGISTRY
		UpdateRegistry();

		// delete temp directory on startup
		DeleteTempDirectory();
 
		rverify(m_Json.Init(), unknownError, NotifyFatalError(FATAL_ERROR_INIT_JSON));
		RgscDisplay("JSON Interface initialized");
		rverify(m_Metadata.Init(), unknownError, NotifyFatalError(FATAL_ERROR_INIT_METADATA));
		RgscDisplay("Metadata initialized");
		rverify(m_AchievementManager.Init(), unknownError, NotifyFatalError(FATAL_ERROR_INIT_ACHIEVEMENT_MANAGER));
		RgscDisplay("Achievement Manager initialized");
		rverify(m_PlayerManager.Init(), unknownError, NotifyFatalError(FATAL_ERROR_INIT_PLAYER_MANAGER));
		RgscDisplay("Player Manager initialized");
		rverify(m_GamerPicManager.Init(), unknownError, NotifyFatalError(FATAL_ERROR_INIT_GAMERPIC_MANAGER));
		RgscDisplay("Gamer Pic Manager initialized");
		rverify(m_ProfileManager.Init(), unknownError, NotifyFatalError(FATAL_ERROR_INIT_PROFILE_MANAGER));
		RgscDisplay("Profile Manager initialized");
		rverify(m_PresenceManager.Init(), unknownError, NotifyFatalError(FATAL_ERROR_INIT_PRESENCE_MANAGER));
		RgscDisplay("Presence Manager initialized");
		rverify(m_CommerceManager.Init(), unknownError, NotifyFatalError(FATAL_ERROR_INIT_COMMERCE_MANAGER));
		RgscDisplay("Commerce Manager initialized");
		rverify(m_TaskManager.Init(), unknownError, NotifyFatalError(FATAL_ERROR_INIT_TASK_MANAGER));
		RgscDisplay("Task Manager initialized");
		rverify(m_TelemetryManager.Init(), unknownError, NotifyFatalError(FATAL_ERROR_TELEMETRY_MANAGER));
		RgscDisplay("Telemetry Manager initialized");
		rverify(m_NetworkInterface.Init(), unknownError, NotifyFatalError(FATAL_ERROR_NETWORK_INTERFACE));
		RgscDisplay("Network interface initialized");
#if RGSC_STEAM_EXPLICIT_SUPPORT
		rverify(m_SteamManager.Init(), unknownError, NotifyFatalError(FATAL_ERROR_STEAM_MANAGER));
		RgscDisplay("Steam Manager initialized");
#endif
		rverify(m_CloudSaveManager.Init(), unknownError, NotifyFatalError(FATAL_ERROR_CLOUD_INTERFACE));
		RgscDisplay("Cloud Saves interface initialized");
		rverify(m_MultiFactorAuth.Init(m_TitleId), unknownError, NotifyFatalError(FATAL_ERROR_MULTI_FACTOR_AUTH));
		RgscDisplay("Multi-Factor Authentication initialized");

#if 0
		const char *logfile = NULL;
		char temp[RGSC_MAX_PATH] = {0};
		_GetFileSystem()->GetPlatformTitleDirectory(temp,false);
		safecat(temp, "sclog.txt");
		PARAM_logfile.Set(temp);
		PARAM_logfile.Get(logfile);
		if(logfile && logfile[0])
		{
			diagChannel::OpenLogFile(logfile);
		}
#endif

#if __DEV
		const char* email = NULL;
		const char* password = NULL;
		if(PARAM_scemail.Get(email) && PARAM_scpassword.Get(password) && PARAM_scnoui.Get())
		{
			m_IsUiDisabled = true;
			rverify(m_ProfileManager.SignInWithoutScUi(email, password), catchall, NotifyFatalError(FATAL_ERROR_INIT_LOGIN));
		}
		else
#endif
		if(IsUiDisabled())
		{
			hr = m_ProfileManager.AttemptAutoSignIn();
			rverify(SUCCEEDED(hr), catchall, NotifyFatalError(FATAL_ERROR_INIT_LOGIN));
		}
		else
		{
			hr = m_Ui.Init();
			if(hr == E_HANDLE)
			{
				rverify(SUCCEEDED(hr), catchall, NotifyFatalError(FATAL_ERROR_INIT_SUBPROCESS_NOT_FOUND));
			}
			else if(hr == RGSC_NOINTERFACE)
			{
				rverify(SUCCEEDED(hr), catchall, NotifyFatalError(FATAL_ERROR_INIT_SUBPROCESS_WRONG_VERSION));
			}
			else
			{
				rverify(SUCCEEDED(hr), catchall, NotifyFatalError(FATAL_ERROR_INIT_UI));
			}
		}

		m_FocusEvent.Clear();

		m_Initialized = true;
		m_PreInitializedOutput = false; // 'm_PreInitializedOutput' no longer true, we're fully initialized

		RgscDisplay("SDK fully initialized");
		RgscRetailLog::AddInterval();

		hr = RGSC_OK;
	}
	rcatch(unknownError)
	{
		hr = RGSC_FAIL;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT Rgsc::Init(ITitleId* titleId, RgscLanguage language, IRgscDelegate* dlgt)
{
	return Init(NULL, titleId, language, dlgt);
}

void Rgsc::Shutdown()
{
	if(m_Initialized)
	{
		rlDebug1("Rgsc::Shutdown() called.");
		m_Initialized = false;
		m_PreInitializedOutput = false;

		if(IsUiDisabled() == false)
		{
			m_Ui.Shutdown();
		}

		m_TaskManager.Shutdown();
		m_ProfileManager.Shutdown();
		m_PresenceManager.Shutdown();
		m_CommerceManager.Shutdown();
		m_GamerPicManager.Shutdown();
		m_AchievementManager.Shutdown();
		m_PlayerManager.Shutdown();
		m_Metadata.Shutdown();
		m_Json.Shutdown();
		m_FileSystem.Shutdown();
		m_TelemetryManager.Shutdown();
		m_NetworkInterface.Shutdown();
#if RGSC_STEAM_EXPLICIT_SUPPORT
		m_SteamManager.Shutdown();
#endif
		m_CloudSaveManager.Shutdown();
		m_MultiFactorAuth.Shutdown();
		netTcp::ShutdownNetThreadPool();

		// don't delete the temp directory on shutdown because this is where we
		// download the updater which gets run by the game after shutting us down
		//DeleteTempDirectory();

#if __DEV
		// DestroyRgscConcreteInstance called below will delete the RGSC instance
		//	that we query to see if logging is enabled. Capture it here.
		bool bWasLoggingEnabled = m_RgscLoggingEnabled;
#endif

		netShutdown();
		DestroyRgscConcreteInstance();
		rlShutdown();


#if __DEV
		if (bWasLoggingEnabled)
		{
			RgscCloseRageLog();
		}
#endif

		AssertVerify(g_RgscAllocator->EndLayer("rgsc", NULL) == 0);
	}
	else
	{
		netTcp::ShutdownNetThreadPool();
	}
}

void Rgsc::ProcessDetach()
{
	if(IsInitialized())
	{
		m_Initialized = false;
		m_PreInitializedOutput = false;

		// the game probably called ExitProcess() without bothering
		// to shut us down cleanly. ExitProcess() kills all the threads
		// before even getting here, so Rage's shutdown code will
		// end up deadlocking waiting for threads to exit.
	}
}

void Rgsc::Update()
{
	netUpdate();
	rlUpdate();
	m_FileSystem.Update();
	m_Json.Update();
	m_Metadata.Update();
	m_AchievementManager.Update();
	m_PlayerManager.Update();
	m_GamerPicManager.Update();
	m_ProfileManager.Update();
	m_PresenceManager.Update();
	m_CommerceManager.Update();
	m_TaskManager.Update();
	m_TelemetryManager.Update();
	m_NetworkInterface.Update();

	UpdateConnectionState();
	UpdateTextEvents();

	if(IsUiDisabled() == false)
	{
		m_Ui.Update();
	}

	// TODO: NS - we need a better stats system
	FreeStatsData(m_StatsData);

#if !__NO_OUTPUT
	static size_t lowWaterMark = 0x7FFFFFFF;
	sysMemAllocator *current = sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL);
	size_t curLowWaterMark = current->GetLowWaterMark(false);
	size_t heapSize = current->GetHeapSize();
	size_t memUsed = heapSize - curLowWaterMark;

	if(curLowWaterMark < lowWaterMark)
	{
		lowWaterMark = curLowWaterMark;
		Displayf("rgsc memory low water mark: %u", curLowWaterMark);
		Displayf("rgsc max memory used: %u (%.2f%%)", memUsed, ((float)memUsed / (float)heapSize) * 100.0f);
		Displayf("rgsc largest free block: %u", current->GetLargestAvailableBlock());
	}
#endif

#if __DEV && 1
	static bool sm_TestShutdown = false;
	if(sm_TestShutdown)
	{
		sm_TestShutdown = false;
		Shutdown();
	}
#endif
}

bool Rgsc::IsOfflineOnlyMode()
{
	return m_OfflineOnlyMode;
}

bool Rgsc::IsPatchingEnabled()
{
	return m_PatchingEnabled;
}

bool Rgsc::AreLocalProfilesEnabled()
{
	return m_LocalProfilesEnabled;
}

bool Rgsc::IsLauncher()
{
	return m_IsLauncher;
}

bool Rgsc::IsUiDisabled()
{
	return m_IsUiDisabled;
}

const char* Rgsc::GetAdditionalSessionAttr()
{
	return m_AdditionalSessionAttr;
}

const char* Rgsc::GetAdditionalJoinAttr()
{
	return m_AdditionalJoinAttr;
}

bool Rgsc::GetHandleAutoSignIn()
{
	return m_HandleAutoSignIn;
}

bool Rgsc::IsLoggingEnabled()
{
	return m_RgscLoggingEnabled;
}

const char* Rgsc::GetLoggingPath()
{
	return m_RgscLogPath;
}

const char* Rgsc::GetMetaDataPath()
{
	return m_MetaDataPath;
}

const char* Rgsc::GetScuiPakPath()
{
	return m_OfflineScuiPakPath;
}

bool Rgsc::IsMac()
{
	return (m_TitleId.GetPlatform() == ITitleId::PLATFORM_MAC_STEAM) ||
		   (m_TitleId.GetPlatform() == ITitleId::PLATFORM_MAC_APP_STORE);
}

bool Rgsc::IsSteam()
{
	return (m_TitleId.GetPlatform() == ITitleId::PLATFORM_MAC_STEAM) ||
			(m_TitleId.GetPlatform() == ITitleId::PLATFORM_STEAM);
}

bool Rgsc::IsTitleMp3()
{
	return stricmp(g_rlTitleId->m_RosTitleId.GetTitleName(), "mp3") == 0;
}

bool Rgsc::IsTitleLaNoire()
{
	return stricmp(g_rlTitleId->m_RosTitleId.GetTitleName(), "lanoire") == 0;
}

bool Rgsc::IsTitleGtaV()
{
	return stricmp(g_rlTitleId->m_RosTitleId.GetTitleName(), "gta5") == 0;
}

bool Rgsc::IsTitleMTL()
{
	return m_IsLauncher && stricmp(g_rlTitleId->m_RosTitleId.GetTitleName(), "launcher") == 0;
}

bool Rgsc::HasNetwork()
{
	// TODO: NS - this just tells me if the PC has a LAN connection available. Need to call a ROS service to see if we can connect to the backend.
	return netHardware::IsAvailable();
}

bool Rgsc::IsConnectedToInternet()
{
	return (m_WasConnectedToInternet == TRUE);
}

void Rgsc::UpdateConnectionState()
{
	static const int CONNECTION_RECHECK_INTERVAL_MS = 5 * 1000;
	static const int OFFLINE_TOLERANCE_MS = 15 * 1000;

	// Disable WinINet connection checks, default to always online.
	if (PARAM_noConnectionStateChecks.Get())
	{
		m_WasConnectedToInternet = TRUE;
		m_TimeOfLastConnected = sysTimer::GetSystemMsTime();
		return;
	}

	if (sysTimer::HasElapsedIntervalMs(m_TimeOfLastConnectionCheck, CONNECTION_RECHECK_INTERVAL_MS))
	{
		unsigned flags = 0;
		BOOL bConnected = InternetGetConnectedState((LPDWORD)&flags, 0);

		// If we're connected, update the time of last connected to the current time
		if (bConnected == TRUE)
		{
			m_TimeOfLastConnected = sysTimer::GetSystemMsTime();
		}
		else if (m_WasConnectedToInternet == TRUE)
		{
			// We're disconnected...
			RgscDisplay("UpdateConnectionState :: Not Connected :: Flags - %d", flags);

			if (flags & INTERNET_CONNECTION_CONFIGURED)
			{
				// Local system has a valid connection to the Internet, but it might or might not be currently connected.
				rlDebug3("UpdateConnectionState :: Flags - INTERNET_CONNECTION_CONFIGURED");
			}
			else if (flags & INTERNET_CONNECTION_LAN)
			{
				// Local system uses a local area network to connect to the Internet.
				rlDebug3("UpdateConnectionState :: Flags - INTERNET_CONNECTION_LAN");
			}
			else if (flags & INTERNET_CONNECTION_MODEM)
			{
				// Local system uses a modem to connect to the Internet.
				rlDebug3("UpdateConnectionState :: Flags - INTERNET_CONNECTION_MODEM");
			}
			else if (flags & INTERNET_CONNECTION_MODEM_BUSY)
			{
				// No longer used.
				rlDebug3("UpdateConnectionState :: Flags - INTERNET_CONNECTION_MODEM_BUSY");
			}
			else if (flags & INTERNET_CONNECTION_OFFLINE)
			{
				// Local system is in offline mode.
				rlDebug3("UpdateConnectionState :: Flags - INTERNET_CONNECTION_OFFLINE");
			}
			else if (flags & INTERNET_CONNECTION_PROXY)
			{
				// Local system uses a proxy server to connect to the Internet.
				rlDebug3("UpdateConnectionState :: Flags - INTERNET_CONNECTION_PROXY");
			}
			else if (flags & INTERNET_RAS_INSTALLED)
			{
				// Local system has RAS installed.
				rlDebug3("UpdateConnectionState :: Flags - INTERNET_RAS_INSTALLED");
			}
		}
		
		// Update cached values, reset check time
		if (bConnected != m_WasConnectedToInternet)
		{
			RgscDisplay("UpdateConnectionState :: Connection state changed from %s to %s", m_WasConnectedToInternet ? "online" : "offline",  bConnected ? "online" : "offline");

			m_WasConnectedToInternet = bConnected;

			// Let the SCUI know that the internet connected state changed
			if (m_Ui.IsReadyToAcceptCommands())
			{
				// If a connection state change is not supported or we're in offline mode, send a message to the SCUI.
				// If we're in a connection state change and we're in online mode, don't send the message as we're about to restart.
				if (!IsConnectionStateChangeSupported() || m_Ui.IsOfflineMode())
				{
					char buf[256] = {0};
					RsonWriter rw;
					rw.Init(buf, sizeof(buf), RSON_FORMAT_JSON);
					rw.Begin(NULL, NULL);
					rw.WriteBool("Result", (m_WasConnectedToInternet == TRUE));
					rw.End();

					bool succeeded;
					rgsc::Script::JsInternetConnectedStateChanged(&succeeded, buf);
				}
			}
		}

		// If not connected and we're in offline mode and we're offline long enough to transition, load the offline site
		if (!bConnected && IsConnectionStateChangeSupported())
		{
			if (sysTimer::HasElapsedIntervalMs(m_TimeOfLastConnected, OFFLINE_TOLERANCE_MS) && m_Ui.IsReadyToAcceptCommands() && !m_Ui.IsOfflineMode())
			{
				rlDebug1("disconnected long enough to trigger a load of the offline site");
				m_Ui.ReloadUi(true);
			}
		}

		// Update the time of last connection check to the current system time
		m_TimeOfLastConnectionCheck = sysTimer::GetSystemMsTime();
	}
}

TitleId* 
Rgsc::GetTitleId()
{
	return &m_TitleId;
}

const char* 
Rgsc::GetTitleIdAsJson()
{
	JSONNODE *n = NULL;

	rtry
	{

		n = json_new(JSON_NODE);
		rverify(n, catchall, );

		json_set_name(n, "TitleId");

		char szEnv[32] = {0};
		switch(m_TitleId.GetRosEnvironment())
		{
		case RLROS_ENV_RAGE:
			safecpy(szEnv, "rage");
			break;
		case RLROS_ENV_CI_RAGE:
			safecpy(szEnv, "ci-rage");
			break;
		case RLROS_ENV_DEV:
			safecpy(szEnv, "dev");
			break;
		case RLROS_ENV_CERT:
			safecpy(szEnv, "cert");
			break;
		case RLROS_ENV_PROD:
			safecpy(szEnv, "prod");
			break;
		default:
			safecpy(szEnv, "unknown");
			break;
		}

		char szPlatform[32] = {0};
		ITitleId::Platform platform = m_TitleId.GetPlatform();
		switch(platform)
		{
		case ITitleId::PLATFORM_INVALID:
			safecpy(szPlatform, "unknown");
			break;
		case ITitleId::PLATFORM_PC:
			safecpy(szPlatform, "PC");
			break;
		case ITitleId::PLATFORM_STEAM:
			safecpy(szPlatform, "Steam");
			break;
		case ITitleId::PLATFORM_ONLIVE:
			safecpy(szPlatform, "OnLive");
			break;
		case ITitleId::PLATFORM_NHN:
			safecpy(szPlatform, "NHN");
			break;
		case ITitleId::PLATFORM_MAC_STEAM:
			safecpy(szPlatform, "MacSteam");
			break;
		case ITitleId::PLATFORM_MAC_APP_STORE:
			safecpy(szPlatform, "MacAppStore");
			break;
		case ITitleId::PLATFORM_VIVEPORT:
			safecpy(szPlatform, "Viveport");
			break;
		case ITitleId::PLATFORM_OCULUS:
			safecpy(szPlatform, "Oculus");
			break;
		}

		json_push_back(n, json_new_a("RosEnvironment", szEnv));
		json_push_back(n, json_new_a("RosPlatform", "pcros"));
		json_push_back(n, json_new_i("RosPlatformId", 8));	// 8 = pcros
		json_push_back(n, json_new_a("RosTitleName", m_TitleId.GetRosTitleName()));
		json_push_back(n, json_new_i("RosTitleVersion", m_TitleId.GetRosTitleVersion()));
		json_push_back(n, json_new_i("RosAuthServices", m_RgscAuthServices));
		json_push_back(n, json_new_a("TitleDisplayName", m_TitleId.GetTitleDirectoryName()));
		json_push_back(n, json_new_a("Platform", szPlatform));
		json_push_back(n, json_new_a("Language", rlGetLanguageCode(rlGetLanguage())));
		json_push_back(n, json_new_b("IsLauncher", IsLauncher()));
		json_push_back(n, json_new_b("EnableLocalProfiles", AreLocalProfilesEnabled()));
		json_push_back(n, json_new_b("OfflineOnly", IsOfflineOnlyMode()));
		json_push_back(n, json_new_b("EnableDebugLog", !PARAM_scNoDebugLog.Get()));

		OSVERSIONINFO osVersion;
		sysMemSet(&osVersion, 0, sizeof(OSVERSIONINFO));
		osVersion.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		
		// Note: Applications not manifested for Windows 8.1 or Windows 10 will return the Windows 8 OS version value (6.2).
		if (GetVersionEx(&osVersion) == FALSE)
		{
			rlError("GetVersionExA failed, error: %d", GetLastError());
			json_push_back(n, json_new_i("OsVersionMajor", 0));
			json_push_back(n, json_new_i("OsVersionMinor", 0));
		}
		else
		{
			json_push_back(n, json_new_i("OsVersionMajor", osVersion.dwMajorVersion));
			json_push_back(n, json_new_i("OsVersionMinor", osVersion.dwMinorVersion));
		}

		char fingerPrint[64];
		rage::formatf(fingerPrint, "%"I64FMT"u", m_MultiFactorAuth.GetFingerprint());
		json_push_back(n, json_new_a("Fingerprint", fingerPrint));

		JSONNODE* fpJsonNode = json_new(JSON_NODE);
		rverify(fpJsonNode, catchall, );
		json_set_name(fpJsonNode, "FingerprintKvp");

		m_MultiFactorAuth.GetFingerprintJSON(fpJsonNode);
		json_push_back(n, fpJsonNode);

		// NHN (Naver) works the same way as Steam
		if((m_TitleId.GetPlatform() == ITitleId::PLATFORM_STEAM) ||
			(m_TitleId.GetPlatform() == ITitleId::PLATFORM_MAC_STEAM) ||
			(m_TitleId.GetPlatform() == ITitleId::PLATFORM_NHN))
		{
			if(m_TitleId.GetSteamAppId() != 0)
			{
				json_push_back(n, json_new_a("SteamAuthTicket", m_TitleId.GetSteamAuthTicket()));
				json_push_back(n, json_new_i("SteamAppId", m_TitleId.GetSteamAppId()));
			}
		}
		else if(m_TitleId.GetPlatform() == ITitleId::PLATFORM_MAC_APP_STORE)
		{
			u8* receipt = NULL;
			int receiptLen = 0;
			m_TitleId.GetMacAppStoreReceipt(&receipt, &receiptLen);
			const json_char* receiptBase64 = "";
			if(receipt)
			{
				receiptBase64 = json_encode64(receipt, receiptLen);

				// we made a copy of this in InitRline(), delete it here.
				delete [] receipt;
			}

			json_push_back(n, json_new_a("MacAppStoreReceipt", receiptBase64));
		}

		// If we're already signed in:
		if (m_ProfileManager.IsSignedIn())
		{
			JSONNODE* profileNode = json_new(JSON_NODE);
			if (profileNode)
			{
				json_set_name(profileNode, "Profile");

				if (m_ProfileManager.GetSignedInProfileAsJson(profileNode))
				{
					json_push_back(n, profileNode);
				}
				else
				{
					json_delete(profileNode);
					profileNode = NULL;
				}
			}
		}

		json_char* jc = json_write_formatted(n);

		rlDebug2("%s\n", jc);

		json_delete(n);

		return jc;
	}
	rcatchall
	{
		if (n != NULL)
		{
			json_delete(n);
		}

		return "";
	}
}

bool 
Rgsc::GetVersionInfo(char (&version)[128])
{
	memset(version, 0, sizeof(version));

	char fileName[RGSC_MAX_PATH] = {0};
	bool success = m_FileSystem.GetSocialClubDllPath(fileName);
	if(success == false)
	{
		return false;
	}

	wchar_t fileNameW[RGSC_MAX_PATH] = {0};
	int result = MultiByteToWideChar(CP_UTF8, NULL, fileName, -1, fileNameW, COUNTOF(fileNameW));
	if(result == 0)
	{
		return false;
	}

	DWORD handle = 0;
	DWORD size = GetFileVersionInfoSize(fileNameW, &handle);
	BYTE* versionInfo = rage_new BYTE[size];
	if(!GetFileVersionInfo(fileNameW, handle, size, versionInfo))
	{
		delete[] versionInfo;
		return false;
	}

	// we have version information
	UINT len = 0;
	VS_FIXEDFILEINFO* vsfi = NULL;
	VerQueryValue(versionInfo, L"\\", (void**)&vsfi, &len);

	int aVersion[4] = {0};
	aVersion[0] = HIWORD(vsfi->dwFileVersionMS);
	aVersion[1] = LOWORD(vsfi->dwFileVersionMS);
	aVersion[2] = HIWORD(vsfi->dwFileVersionLS);
	aVersion[3] = LOWORD(vsfi->dwFileVersionLS);

	delete[] versionInfo;

	formatf(version, "%d.%d.%d.%d", aVersion[0], aVersion[1], aVersion[2], aVersion[3]);
	return true;
}

bool 
Rgsc::GetTitleVersionInfo(char (&version)[128])
{
	memset(version, 0, sizeof(version));

	wchar_t fileName[MAX_PATH] = {0};
	bool success = GetModuleFileNameW(NULL, fileName, COUNTOF(fileName)) != 0;

	if(success == false)
	{
		return false;
	}

	DWORD handle = 0;
	DWORD size = GetFileVersionInfoSizeW(fileName, &handle);
	BYTE* versionInfo = rage_new BYTE[size];
	if(!GetFileVersionInfoW(fileName, handle, size, versionInfo))
	{
		delete[] versionInfo;
		return false;
	}

	// we have version information
	UINT len = 0;
	VS_FIXEDFILEINFO* vsfi = NULL;
	VerQueryValue(versionInfo, L"\\", (void**)&vsfi, &len);

	int aVersion[4] = {0};
	aVersion[0] = HIWORD(vsfi->dwFileVersionMS);
	aVersion[1] = LOWORD(vsfi->dwFileVersionMS);
	aVersion[2] = HIWORD(vsfi->dwFileVersionLS);
	aVersion[3] = LOWORD(vsfi->dwFileVersionLS);

	delete[] versionInfo;

	formatf(version, "%d.%d.%d.%d", aVersion[0], aVersion[1], aVersion[2], aVersion[3]);
	return true;
}

bool 
Rgsc::GetModuleVersionInfo(const char* fileName, char (&version)[128])
{
	version[0] = '\0';

	wchar_t pathW[RGSC_MAX_PATH];
	int result = MultiByteToWideChar(CP_UTF8 , NULL , fileName, -1, pathW, RGSC_MAX_PATH);
	if(result == 0)
	{
		return false;
	}

	memset(version, 0, sizeof(version));

	DWORD handle = 0;
	DWORD size = GetFileVersionInfoSize(pathW, &handle);
	BYTE* versionInfo = rage_new BYTE[size];
	if(!GetFileVersionInfo(pathW, handle, size, versionInfo))
	{
		delete[] versionInfo;
		return false;
	}

	// we have version information
	UINT len = 0;
	VS_FIXEDFILEINFO* vsfi = NULL;
	VerQueryValue(versionInfo, L"\\", (void**)&vsfi, &len);

	int aVersion[4] = {0};
	aVersion[0] = HIWORD(vsfi->dwFileVersionMS);
	aVersion[1] = LOWORD(vsfi->dwFileVersionMS);
	aVersion[2] = HIWORD(vsfi->dwFileVersionLS);
	aVersion[3] = LOWORD(vsfi->dwFileVersionLS);

	delete[] versionInfo;

	formatf(version, "%d.%d.%d.%d", aVersion[0], aVersion[1], aVersion[2], aVersion[3]);
	return true;
}

const char*
Rgsc::GetVersionInfoAsJson()
{
	JSONNODE *n = json_new(JSON_NODE);
	json_set_name(n, "Version Info");

	char version[128] = {0};
	GetVersionInfo(version);

	json_push_back(n, json_new_a("Version", version));

	char titleVersion[128] = {0};
	GetTitleVersionInfo(titleVersion);
	json_push_back(n, json_new_a("TitleVersion", titleVersion));

	json_char *jc = json_write_formatted(n);

	rlDebug2("%s\n", jc);

	json_delete(n);

	return jc;
}

const char* Rgsc::GetStatsData()
{
	rtry
	{
		FreeStatsData(m_StatsData);

		rcheck(GetStatsData(&m_StatsData), catchall, );
		rverify(m_StatsData, catchall, );
	}
	rcatchall
	{

	}

	if(m_StatsData)
	{
		return m_StatsData;
	}

	return "";
}

// ===========================================================================
// Public Subsystem Interfaces
// ===========================================================================
IRgsc* GetRgscInterface()
{
	return GetRgscConcreteInstance();
}

IAchievementManager* Rgsc::GetAchievementManager()
{
	return &m_AchievementManager;
}

IPlayerManager* Rgsc::GetPlayerManager()
{
	return &m_PlayerManager;
}

IProfileManager* Rgsc::GetProfileManager()
{
	return &m_ProfileManager;
}

IFileSystem* Rgsc::GetFileSystem()
{
	return &m_FileSystem;
}

ITaskManager* Rgsc::GetTaskManager()
{
	return &m_TaskManager;
}

IPresenceManager* Rgsc::GetPresenceManager()
{
	return &m_PresenceManager;
}

ICommerceManager* Rgsc::GetCommerceManager()
{
	return &m_CommerceManager;
}

ITelemetry* Rgsc::GetTelemetry()
{
	return &m_TelemetryManager;
}

IRgscUi* Rgsc::GetUiInterface()
{
	return &m_Ui;
}

INetwork* Rgsc::GetNetworkInterface()
{
	return &m_NetworkInterface;
}

IGamepadManager* Rgsc::GetGamepadMgr()
{
	return &m_GamepadManager;
}

ICloudSaveManager* Rgsc::GetCloudSaveManager()
{
	return &m_CloudSaveManager;
}

IGamerPicManager* Rgsc::GetGamerPicManager()
{
	return &m_GamerPicManager;
}

// ===========================================================================
// Private Subsystem Interfaces
// ===========================================================================
Rgsc* GetRgscConcreteInstance()
{
	//@@: location RLPCPROFILEMANAGER_GETCONCRETEINSTANCE
	if(sm_Instance == NULL)
	{
		sm_Instance = rage_new Rgsc;
	}

	return sm_Instance;
}

void DestroyRgscConcreteInstance()
{
	if(sm_Instance != NULL)
	{
		delete sm_Instance;
		sm_Instance = NULL;
	}
}

RgscGamerPicManager* Rgsc::_GetGamerPicManager()
{
	return &m_GamerPicManager;
}

RgscMetadata* Rgsc::_GetLocalMetadata()
{
	return &m_Metadata;
}

RgscTaskManager* Rgsc::_GetTaskManager()
{
	return &m_TaskManager;
}

RgscPresenceManager* Rgsc::_GetPresenceManager()
{
	return &m_PresenceManager;
}

RgscCommerceManager* Rgsc::_GetCommerceManager()
{
	return &m_CommerceManager;
}

RgscAchievementManager* Rgsc::_GetAchievementManager()
{
	return &m_AchievementManager;
}

RgscPlayerManager* Rgsc::_GetPlayerManager()
{
	return &m_PlayerManager;
}

RgscProfileManager* Rgsc::_GetProfileManager()
{
	return &m_ProfileManager;
}

RgscFileSystem* Rgsc::_GetFileSystem()
{
	return &m_FileSystem;
}

RgscUi* Rgsc::_GetUiInterface()
{
	return &m_Ui;
}

RgscTelemetryManager* Rgsc::_GetTelemetry()
{
	return &m_TelemetryManager;
}

IFileDelegate* Rgsc::_GetFileDelegate()
{
	return m_FileDelegate;
}

RgscGamePadManager* Rgsc::_GetGamepadManager()
{
	return &m_GamepadManager;
}

RgscNetwork* Rgsc::_GetNetwork()
{
	return &m_NetworkInterface;
}

RgscCloudSaveManager* Rgsc::_GetCloudSaveManager()
{
	return &m_CloudSaveManager;
}

RgscMultiFactorAuth* Rgsc::_GetMultiFactorAuth()
{
	return &m_MultiFactorAuth;
}

#if RGSC_STEAM_EXPLICIT_SUPPORT
RgscSteamManager* Rgsc::_GetSteam()
{
	return &m_SteamManager;
}
#endif

// ===========================================================================
// Delegate Functions
// ===========================================================================
#define REQUIRES_DELEGATE(V) if(m_Delegate ## V == NULL) return; IRgscDelegate ## V* dlgt = m_Delegate ## V;
#define REQUIRES_DELEGATE_RET(V, ReturnVal) if(m_Delegate ## V == NULL) return ReturnVal; IRgscDelegate ## V* dlgt = m_Delegate ## V;

void Rgsc::Output(IRgscDelegateLatestVersion::OutputSeverity severity, const char* msg)
{
	REQUIRES_DELEGATE(V1);
	dlgt->Output(severity, msg);
}

bool Rgsc::GetStatsData(char** data)
{
	REQUIRES_DELEGATE_RET(V1, false);
	return dlgt->GetStatsData(data);
}

void Rgsc::FreeStatsData(const char* data)
{
	REQUIRES_DELEGATE(V1);

	if(data)
	{
		dlgt->FreeStatsData(data);
		m_StatsData = NULL;
	}
}

void Rgsc::SetTextBoxHasFocus(const bool hasFocus)
{
	REQUIRES_DELEGATE(V1);
	dlgt->SetTextBoxHasFocus(hasFocus);
}

void Rgsc::SetTextBoxHasFocusV2(const bool hasFocus, const char* prompt, const char* text, const bool isPassword, const unsigned int maxNumChars)
{
	// If we are in controller mode and the A button is still pressed, queue the event, overwriting any previously queued event
	if (hasFocus && Rgsc_Input::IsUsingController() && Rgsc_Input::IsButtonDown(IGamepadLatestVersion::RGSC_RDOWN))
	{
		m_FocusEvent.HasFocus = hasFocus;
		safecpy(m_FocusEvent.Prompt, prompt);
		safecpy(m_FocusEvent.Text, text);
		m_FocusEvent.IsPassword = isPassword;
		m_FocusEvent.MaxNumChars = maxNumChars;
		m_FocusEvent.IsPending = true;
	}
	else
	{
		if(m_DelegateV2 == NULL)
		{
			SetTextBoxHasFocus(hasFocus);
		}
		else
		{
			REQUIRES_DELEGATE(V2);
			dlgt->SetTextBoxHasFocusV2(hasFocus, prompt, text, isPassword, maxNumChars);
		}

		// MP3/LAN - The new Steam Big Picture/Virtual Keyboard flow has the game titles accepting the hasFocus callback, 
		//	attempting to show the Steam virtual keyboard, and falling back to our own. Because MP3/LAN are before this flow,
		//	we need to manually show the virtual keyboard from within the SDK.
		if (hasFocus && Rgsc_Input::IsUsingController() && (IsTitleMp3() || IsTitleLaNoire()))
		{
			m_Ui.ShowVirtualKeyboard(text, isPassword, maxNumChars);
		}
	}
}

void Rgsc::UpdateSocialClubDll(const char* commandLine)
{
	REQUIRES_DELEGATE(V1);
	dlgt->UpdateSocialClubDll(commandLine);
}

void Rgsc::HandleNotification(const rgsc::NotificationType id, const void* param)
{
	REQUIRES_DELEGATE(V3);
	dlgt->HandleNotification(id, param);
}

void
Rgsc::UpdateTextEvents()
{
	// If we have a pending focus event
	if (m_FocusEvent.IsPending)
	{
		// If we stop using the controller or the button is released, send the event to the delegate.
		if (!Rgsc_Input::IsUsingController() || !Rgsc_Input::IsButtonDown(IGamepadLatestVersion::RGSC_RDOWN))
		{
			if(m_DelegateV2 == NULL)
			{
				SetTextBoxHasFocus(m_FocusEvent.HasFocus);
			}
			else
			{
				REQUIRES_DELEGATE(V2);
				dlgt->SetTextBoxHasFocusV2(m_FocusEvent.HasFocus, m_FocusEvent.Prompt, m_FocusEvent.Text, m_FocusEvent.IsPassword, m_FocusEvent.MaxNumChars);
			}

			// MP3/LAN - The new Steam Big Picture/Virtual Keyboard flow has the game titles accepting the hasFocus callback, 
			//	attempting to show the Steam virtual keyboard, and falling back to our own. Because MP3/LAN are before this flow,
			//	we need to manually show the virtual keyboard from within the SDK.
			if (m_FocusEvent.HasFocus && Rgsc_Input::IsUsingController() && (IsTitleMp3() || IsTitleLaNoire()))
			{
				m_Ui.ShowVirtualKeyboard(m_FocusEvent.Text, m_FocusEvent.IsPassword, m_FocusEvent.MaxNumChars);
			}

			m_FocusEvent.Clear();
		}	
	}
}

bool Rgsc::IsPresenceSupported()
{
	// Cache the result, since this may be polled frequently.
	static int result = -1;

	rtry
	{
		// Early out if a result has been calculated.
		rcheck(result == -1, catchall, );

		if (m_PresenceBehaviour == IConfigurationV8::ENABLED)
		{
			result = 1;
		}
		else if (m_PresenceBehaviour == IConfigurationV8::DISABLED)
		{
			result = 0;
		}
		else
		{
			rcheck(!IsLauncher(), catchall, result = 0); // no presence in the launchers
			rcheck(IsTitleMp3() || IsTitleGtaV(), catchall, result = 0); // only MP3 and GTAV require presence marshalling
			result = 1;
		}
	}
	rcatchall
	{
		
	}

	return (result == 1);
}

bool Rgsc::IsFriendsSupported()
{
	static int result = -1;
	rtry
	{
		// Early out if a result has been calculated.
		rcheck(result == -1, catchall, );

		if (m_FriendsBehaviour == IConfigurationV8::ENABLED)
		{
			result = 1;
		}
		else if (m_FriendsBehaviour == IConfigurationV8::DISABLED)
		{
			result = 0;
		}
		else
		{
			rcheck(!IsLauncher(), catchall, result = 0); // no friends in the launchers
			rcheck(!IsTitleLaNoire(), catchall, result = 0); // LA Noire is pre-friends
			result = 1;
		}
	}
	rcatchall
	{

	}

	return (result == 1);
}

bool Rgsc::IsConnectionStateChangeSupported()
{
	static int result = -1;

	rtry
	{
		// Early out if a result has been calculated.
		rcheck(result == -1, catchall, );

		if (m_ConnectionChangeBehaviour == IConfigurationV8::ENABLED)
		{
			result = 1;
		}
		else if (m_ConnectionChangeBehaviour == IConfigurationV8::DISABLED)
		{
			result = 0;
		}
		else
		{
			rcheck(!IsLauncher(), catchall, result = 0); // no connection state change in the launchers
			rcheck(!IsTitleLaNoire(), catchall, result = 0); // no connection state change in LA Noire
			rcheck(!IsTitleMp3(), catchall, result = 0); // no connection state change in MP3
			rcheck(!PARAM_noScuiDisconnect.Get(), catchall, result = 0); // disable connection change
			result = 1;
		}
	}
	rcatchall
	{

	}

	return (result == 1);
}

bool Rgsc::IsAchievementsSupported()
{
	// Achievements require metadata
	if (!IsMetadataSupported())
		return false;

	if (m_AchievementBehaviour == IConfigurationV8::ENABLED)
	{
		return true;
	}
	else if (m_AchievementBehaviour == IConfigurationV8::DISABLED)
	{
		return false;
	}
	else
	{
		// no achievements in the launcher
		if (IsLauncher())
		{
			return false;
		}

		return true;
	}
}

bool Rgsc::IsSignInTransferAutoLoginSupported()
{
	static int result = -1;

	rtry
	{
		// Early out if a result has been calculated.
		rcheck(result == -1, catchall, );
		
		if (m_SignInTransferBehaviour == IConfigurationV8::ENABLED)
		{
			result = 1;
		}
		else if (m_SignInTransferBehaviour == IConfigurationV8::DISABLED)
		{
			result = 0;
		}
		else
		{
			// By default: Launcher, LANoire and MP3 do not support autologin.
			// The MTL build variants can support it, and will set this overridden behaviour
			// using the configuration interface and queried above.
			rcheck(!IsLauncher(), catchall, result = 0); // no signin transfer autologin the launchers
			rcheck(!IsTitleLaNoire(), catchall, result = 0); // no signin transfer autologin in LA Noire
			rcheck(!IsTitleMp3(), catchall, result = 0); // no signin transfer autologin in MP3

			result = 1;
		}
	}
	rcatchall
	{

	}

	return (result == 1);
}

bool Rgsc::IsMetadataSupported()
{
	if (IsLauncher())
		return false;

	return m_MetadataEnabled;
}

IConfigurationV6::GamepadSupport Rgsc::GetGamepadSupport()
{
	return m_GamepadSupport;	
}

bool Rgsc::CreateGameCrashTask(u64 actionToTake)
{
	netStatus* status = &m_GameCrashStatus;
	Assert(!status->Pending());
	RLPC_CREATETASK(OnGameCrashTask, 0, actionToTake, status);
}

const char* Rgsc::GetSteamPersona()
{
	return m_TitleId.GetSteamPersona();
}

const char* Rgsc::GetSteamPersonaAsJson()
{
	rtry
	{
#if RGSC_STEAM_EXPLICIT_SUPPORT
		rcheck(m_SteamFriends, catchall, );
#endif

		JSONNODE* rootNode = json_new(JSON_NODE);
		rcheck(rootNode, catchall, );

#if RGSC_STEAM_EXPLICIT_SUPPORT
		json_push_back(rootNode, json_new_a("Name", m_SteamFriends->GetPersonaName()));
#else
		json_push_back(rootNode, json_new_a("Name", GetSteamPersona()));
#endif

		json_char* jc = json_write_formatted(rootNode);
		rlDebug2("RGSC_GET_STEAM_PERSONA: %s\n", jc);
		json_delete(rootNode);

		return jc;
	}
	rcatchall
	{
		return "";
	}
}

const WindowHandle* Rgsc::GetAdditionalWindowHandles()
{
	return m_AdditionalWindowHandles;
}

const unsigned Rgsc::GetNumAdditionalWindowHandles()
{
	return m_NumAdditionalWindowHandles;
}

const char* Rgsc::GetCommandLineArgumentsAsJson()
{
	JSONNODE* resultJson = NULL;

	rtry
	{
		JSONNODE* resultJson = json_new(JSON_NODE);
		rverify(resultJson, catchall, );
		json_set_name(resultJson, "Command Line");

		JSONNODE *argArray = json_new(JSON_ARRAY);
		rverify(argArray, catchall, );
		json_set_name(argArray, "Arguments");

		const int MAX_CL_ARGS_RESULT_COUNT = 100;
		for(int i = 1; i < sysParam::GetArgCount() && i < MAX_CL_ARGS_RESULT_COUNT; i++)
		{
			// escape the string for SCUI
			atString argument(sysParam::GetArg(i));
			argument.Replace("\\", "\\\\");

			json_push_back(argArray, json_new_a("Argument", argument.c_str()));
		}

		json_push_back(resultJson, argArray);

		json_char *jc = json_write_formatted(resultJson);

		rlDebug2("%s\n", jc);

		// free from result
		json_delete(resultJson);

		return jc;
	}
	rcatchall
	{
		if (resultJson)
		{
			json_delete(resultJson);
		}

		return "";
	}
}

RgscAuthServices Rgsc::GetAuthServiceMethod()
{
	return m_RgscAuthServices;
}

} // namespace rgsc

// Temporary hack to allow for linking until a fix is made for the RageCore/RageFramework dependency
#include "paging/streamer.h"
namespace rage
{
	u32 GetStrIndexFromHandle(const pgStreamer::Handle & /*handle*/)
	{
		return 0;
	}
}
