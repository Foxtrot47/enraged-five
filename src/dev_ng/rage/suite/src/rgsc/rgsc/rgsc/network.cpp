// 
// rgsc/network.cpp
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 

// rgsc_includes
#include "rgsc.h"
#include "network.h"

// rage_includes
#include "diag/seh.h"
#include "net/natdetector.h"

namespace rgsc
{
	// ===============================================================================================
	// rgscNetwork
	// ===============================================================================================
	RGSC_HRESULT RgscNetwork::QueryInterface(RGSC_REFIID riid, void** ppvObj)
	{
		IRgscUnknown *pUnknown = NULL;

		if(ppvObj == NULL)
		{
			return RGSC_INVALIDARG;
		}

		if(riid == IID_IRgscUnknown)
		{
			pUnknown = static_cast<INetwork*>(this);
		}
		else if(riid == IID_INetworkV1)
		{
			pUnknown = static_cast<INetworkV1*>(this);
		}

		*ppvObj = pUnknown;
		if(pUnknown == NULL)
		{
			return RGSC_NOINTERFACE;
		}

		return RGSC_OK;
	}

	RgscNetwork::RgscNetwork()
	{

	}

	RgscNetwork::~RgscNetwork()
	{

	}

	bool RgscNetwork::Init()
	{
		m_NetworkInfoIsValid = false;
		m_NatDetectionState = INetworkInfoV1::NAT_NDS_UNATTEMPTED;
		m_UpNpState = INetworkInfoV1::NAT_UPNP_UNATTEMPTED;
		m_PcpState = INetworkInfoV2::NAT_PCP_UNATTEMPTED;

		m_ShowNatTypeWarning = false;
		if(GetRgscConcreteInstance()->IsTitleMp3() || GetRgscConcreteInstance()->IsTitleLaNoire())
		{
			// older titles don't measure the UDP port binding timeout
			netNatDetector::SetAllowAdjustableUdpSendInterval(false);
		}

		return true;
	}

	void RgscNetwork::Shutdown()
	{

	}

	void RgscNetwork::Update()
	{

	}

	void RgscNetwork::SetNetworkInfo(INetworkInfo* info)
	{
		rtry
		{
			// determine which version of the network info interface was passed to us
			INetworkInfoV1* v1 = NULL;
			RGSC_HRESULT hr = info->QueryInterface(IID_INetworkInfoV1, (void**) &v1);
			rverify(SUCCEEDED(hr) && (v1 != NULL), catchall, rlError("network info struct doesn't support INetworkInfoV1 interface"));

			netNatInfo natInfo;

			// TODO: NS - will need to map enums between RGSC and Rage in case they get out of sync in the future
			natInfo.Init(v1->GetNatDetected(),
						 (netNatType)v1->GetNatType(),
						 (netNatPortMappingMethod)v1->GetNatPortMappingMethod(),
						 (netNatFilteringMode)v1->GetNatFilteringMode(),
						 (netNatPortAllocationStrategy)v1->GetNatPortAllocationStrategy(),
						 (s8)v1->GetPortIncrement(),
						 (netNatUdpTimeoutState)v1->GetUdpTimeoutState(),
						 (u16)v1->GetUdpTimeoutSec());

			m_UpNpState = v1->GetuPnPState();

			m_PcpState = INetworkInfoV2::NAT_PCP_UNATTEMPTED;

			// check the v2 interface for PCP status
			INetworkInfoV2* v2 = NULL;
			hr = info->QueryInterface(IID_INetworkInfoV2, (void**) &v2);
			if(SUCCEEDED(hr) && (v2 != NULL))
			{
				m_PcpState = v2->GetPcpState();
			}

			netNatDetector::SetAllowAdjustableUdpSendInterval(v1->GetAllowAdjustablePresencePingInterval());
			netNatDetector::SetLocalNatInfo(natInfo);
			
			m_NatDetectionState = v1->GetNatDetectionState();
			m_PublicAddr.Init(v1->GetPublicAddress());
			m_PrivateAddr.Init(v1->GetPrivateAddress());

			m_NetworkInfoIsValid = true;
		}
		rcatchall
		{
			
		}
	}

	void RgscNetwork::SetShowNatTypeWarning(bool showNatTypeWarning)
	{
		m_ShowNatTypeWarning = showNatTypeWarning;
	}

	const char* RgscNetwork::GetNetworkInfoAsJson()
	{
		rtry
		{
			rcheck(m_NetworkInfoIsValid, catchall, );
			JSONNODE *n = json_new(JSON_NODE);
			rverify(n, catchall, );
			json_set_name(n, "Network Info");

			netNatInfo natInfo = netNatDetector::GetLocalNatInfo();

			char publicAddr[netSocketAddress::MAX_STRING_BUF_SIZE] = {0};
			m_PublicAddr.Format(publicAddr, true);
			json_push_back(n, json_new_a("PublicAddress", publicAddr));

			char privateAddr[netSocketAddress::MAX_STRING_BUF_SIZE] = {0};
			m_PrivateAddr.Format(privateAddr, true);
			json_push_back(n, json_new_a("PrivateAddress", privateAddr));

			json_push_back(n, json_new_i("NATDetectionState", m_NatDetectionState));

			json_push_back(n, json_new_i("NATType", natInfo.GetNatType()));
			json_push_back(n, json_new_i("uPnP", m_UpNpState));
			json_push_back(n, json_new_b("ShowNatTypeWarning", m_ShowNatTypeWarning));
			json_push_back(n, json_new_i("PCP", m_PcpState));

			json_char *jc = json_write_formatted(n);

			json_delete(n);

			return jc;
		}
		rcatchall
		{
			return "{}";
		}
	}
	
} // namespace rgsc
