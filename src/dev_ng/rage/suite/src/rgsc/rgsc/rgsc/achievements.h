// 
// rline/achievements.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPCACHIEVEMENT_H
#define RLINE_RLPCACHIEVEMENT_H

#include "achievements_interface.h"
#include "file/file_config.h"

// rage headers
#include "atl/bitset.h"
#include "atl/queue.h"
#include "data/datprotect.h"
#include "metadata.h"
#include "system/criticalsection.h"
#include "rline/rltask.h"
#include "system/timer.h"

using namespace rage;

namespace rgsc
{

// error C4265: class has virtual functions, but destructor is not virtual
// the binary interface for virtual destructors isn't standardized, so don't make the destructor virtual
#pragma warning(push)
#pragma warning(disable: 4265)

class RgscAchievementManager : public IAchievementManagerLatestVersion
{
// ===============================================================================================
// inherited from IAchievementManager
// ===============================================================================================
public:
	// ===============================================================================================
	// inherited from IRgscUnknown
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

	// ===============================================================================================
	// inherited from IAchievementManagerV1
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL CreateAchievementEnumerator(RGSC_REFIID riid,
															  const ITitleId* titleId,
															  const RockstarId profileId,
															  const IAchievement::DetailFlags detailFlags,
															  const AchievementId firstAchievementId,
															  const u32 maxAchievementsToRead,
															  u32* numBytesRequired,
															  void** handle);

	virtual RGSC_HRESULT RGSC_CALL EnumerateAchievements(void* handle,
														void* buffer,
														const u32 bufferSize,
														IAchievementList** achievements);

	virtual RGSC_HRESULT RGSC_CALL WriteAchievement(const AchievementId achievementId);

#if __DEV
	virtual RGSC_HRESULT RGSC_CALL DeleteAllAchievements();
#endif


	// ===============================================================================================
	// inherited from IAchievementManagerV2
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL EnumerateAchievements(void* handle,
														void* buffer,
														const u32 bufferSize,
														IAchievementList** achievements,
														IAsyncStatus* status);

	// ===============================================================================================
	// inherited from IAchievementManagerV3
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL SetAchievementProgress(const AchievementId achievementId, const s64 progress);

// ===============================================================================================
// accessible from anywhere in the dll
// ===============================================================================================
public:

	////////////////////////////////////////////////////////////////////////////////
	// AchievementProgress
	////////////////////////////////////////////////////////////////////////////////
	static const int PROGRESS_QUEUE_SIZE = 10;

	struct AchievementProgress
	{
		AchievementProgress()
		{
			m_Id = 0;
			m_Progress = -1;
		}

		AchievementProgress(AchievementId id, s64 progress)
		{
			m_Id = id;
			m_Progress = progress;
		}

		AchievementId m_Id;
		s64 m_Progress;
	};

	////////////////////////////////////////////////////////////////////////////////
	// ImageRecord
	////////////////////////////////////////////////////////////////////////////////

	class ImageRecord
	{
		u8* m_Data;
		u32 m_Size;

	public:

		ImageRecord();
		~ImageRecord();
		void Clear();

		const u8* GetData() {return m_Data;}
		u32 GetSize() {return m_Size;}

		bool Import(const void* buf,
					const unsigned sizeOfBuf,
					unsigned* size = 0);
	};


    RgscAchievementManager();
    ~RgscAchievementManager();

	bool Init();
	void Update();
	void Shutdown();

//private: // only accessible from achievements.cpp

	RGSC_HRESULT ReadAchievements(const RockstarId profileId,
								 const IAchievement::DetailFlags detailFlags,
								 const AchievementId firstAchievementId,
								 const u32 maxAchievementsToRead,
								 IAchievementList* achievements);

	// retrieves the local player's offline achievement data in json format
	const char* ReadLocalAchievementsAsJson(const AchievementId firstAchievementId, const u32 maxAchievements);

	// retrieves the local player's achievement image as base64 in json format
	const char* GetAchievementImageAsJson(const char* jsonRequest);

	// retrieves the local player's achievement image as an ImageRecord
	bool GetAchievementImageRecord(AchievementId achievementId, ImageRecord* imageRecord);

	bool GetAchievementAsJson(const IAchievement* achievement, void* p);

	void Clear();

	u32 GetVersion() { return m_Metadata ? m_Metadata->GetVersion() : 0; }

	bool IsAchievementProgressSupported();

	bool IsSynchronized() const;
	void SetSynchronized(bool synchronized);

	void SignOut();

	void SortAchievements(IAchievementList* achievements);

	bool ReadMetadata(const RockstarId profileId,
					  const IAchievement::DetailFlags detailFlags,
					  const AchievementId firstAchievementId,
					  const u32 maxAchievementsToRead,
					  IAchievementList* achievements);

	bool SyncAchievements(const RockstarId profileId,
						  netStatus* status);

	bool ReadAchievements(const RockstarId profileId,
						  const IAchievement::DetailFlags detailFlags,
						  IAchievementList* achievements,
						  netStatus* status);

	bool WriteSingleAchievement(const AchievementId achievementId,
								netStatus* status);

	bool WriteSingleAchievementProgress(const AchievementId achievementId, 
										const s64 progress, 
										netStatus* status);

	bool RaiseAchievementAwardedEvent(const IAchievement* achievement,
									  netStatus* status);

#if __DEV
	bool DeleteAllAchievements(netStatus* status);
#endif

	static bool ConvertRosTimeStringToTmStruct(tm &t, const char* szTime);

	sysCriticalSectionToken m_Cs;
	sysCriticalSectionToken m_ProgressCs;

	bool m_Synchronized;
	RgscMetadata* m_Metadata;
	RgscMetadata::Table* m_MetadataTable;
	RgscMetadata::Table* m_StringsTable; 
	RgscMetadata::Table* m_ImagesTable;

	atQueue<AchievementProgress, PROGRESS_QUEUE_SIZE> m_ProgressQueue;
	s64 m_AchievementProgress[MAX_NUM_ACHIEVEMENTS];
	netStatus m_ProgressWriteStatus;

	static bool sm_FileIoInProgress;
};

#if !__NO_OUTPUT
class TaskBase : public rlTask<RgscAchievementManager>
{
	u32 m_StartTime;

public:
	TaskBase() : m_StartTime(0) {}
	void Start()
	{
		m_StartTime = sysTimer::GetSystemMsTime();
		rlTask<RgscAchievementManager>::Start();
	}

	u32 GetElapsedTime()
	{
		return sysTimer::GetSystemMsTime() - m_StartTime;
	}
};
#else
typedef rlTask<RgscAchievementManager> TaskBase;
#endif


// ===========================================================================
// Achievement implementation
// ===========================================================================

class Achievement : public IAchievementLatestVersion
{
public:
	virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject)
	{
		IRgscUnknown *pUnknown = NULL;

		if(ppvObject == NULL)
		{
			return RGSC_INVALIDARG;
		}

		if(riid == IID_IRgscUnknown)
		{
			pUnknown = static_cast<IAchievement*>(this);
		}
		else if(riid == IID_AchievementV1)
		{
			pUnknown = static_cast<IAchievementV1*>(this);
		}
		else if(riid == IID_AchievementV2)
		{
			pUnknown = static_cast<IAchievementV2*>(this);
		}

		*ppvObject = pUnknown;
		if(pUnknown == NULL)
		{
			return RGSC_NOINTERFACE;
		}

		return RGSC_OK;
	}

	Achievement()
	{
		Clear();
	}

	//PURPOSE
	//  Returns the achievement id.
	virtual AchievementId RGSC_CALL GetId() const
	{
		return m_Id;
	}

	//PURPOSE
	//  Returns number of points this achievement is worth.
	virtual u32 RGSC_CALL GetPoints() const
	{
		return m_Points;
	}

	//PURPOSE
	//  Returns the type of the achievement.
	virtual AchievementType RGSC_CALL GetType() const
	{
		return m_Type;
	}

	//PURPOSE
	//  Returns whether to display the achievement before it is earned.
	virtual bool RGSC_CALL ShowUnachieved() const
	{
		return m_ShowUnachieved != 0;
	}

	//PURPOSE
	//  Returns the label (name) of the achievement.
	virtual const char* RGSC_CALL GetLabel() const
	{
		return m_Label;
	}

	//PURPOSE
	//  Returns the achievement description.
	virtual const char* RGSC_CALL GetDescription() const
	{
		return m_Description;
	}

	//PURPOSE
	//  Returns the unachieved string of the achievement.
	virtual const char* RGSC_CALL GetUnachievedString() const
	{
		return m_UnachievedString;
	}

	//PURPOSE
	//  Returns true if the achievement has been achieved.
	virtual bool RGSC_CALL IsAchieved() const
	{
		return m_Achieved != 0;
	}

	//PURPOSE
	//  If the achievement has been awarded, returns the time at which it was awarded, else zero.
	virtual time_t RGSC_CALL GetAchievedTime() const
	{
		return m_AchievedTime;
	}

	//PURPOSE
	//	If the achievement allows progress tracking, returns the current progress.
	virtual s64 RGSC_CALL GetProgress() const
	{
		return m_Progress;
	}

	virtual bool IsValid() const
	{
		return (GetId() >= 0) && (m_Type > ACHIEVEMENT_TYPE_INVALID) && (m_Type <= ACHIEVEMENT_TYPE_OTHER);
	}

	virtual void Clear()
	{
		m_Id = 0;
		m_Points = 0;
		m_Type = ACHIEVEMENT_TYPE_INVALID;
		m_ShowUnachieved = false;;
		m_Label = NULL;
		m_Description = NULL;
		m_UnachievedString = NULL;
		m_Achieved = false;
		m_AchievedTime = 0;
		m_Progress = 0;
		m_MaxProgress = 0;
	}

	virtual void SetAchieved(bool achieved)
	{
		m_Achieved = achieved;
	}

	virtual void SetTimestamp(time_t timestamp)
	{
		m_AchievedTime = timestamp;
	}

	virtual s64 GetMaxProgress() const
	{
		return m_MaxProgress;
	}

	// basic metadata
	AchievementId m_Id;
	u32 m_Points;
	AchievementType m_Type;
	u8 m_ShowUnachieved;

	// string metadata
	char* m_Label;
	char* m_Description;
	char* m_UnachievedString;

	// per-player data
	u8 m_Achieved;
	time_t m_AchievedTime;
	s64 m_Progress;
	s64 m_MaxProgress;
};

// ===========================================================================
// IAchievementListV1 implementation
// ===========================================================================

class AchievementListV1 : public IAchievementListV1
{
public:
	virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject)
	{
		IRgscUnknown *pUnknown = NULL;

		if(ppvObject == NULL)
		{
			return RGSC_INVALIDARG;
		}

		if(riid == IID_IRgscUnknown)
		{
			pUnknown = static_cast<IAchievementList*>(this);
		}
		else if(riid == IID_IAchievementListV1)
		{
			pUnknown = static_cast<IAchievementListV1*>(this);
		}

		*ppvObject = pUnknown;
		if(pUnknown == NULL)
		{
			return RGSC_NOINTERFACE;
		}

		return RGSC_OK;
	}

	AchievementListV1(IAchievement::DetailFlags detailFlags, u32 maxAchievements, u8* buffer)
	{
		Clear();

		u32 bytesUsed = 0;
		u8* curBuf = &(buffer[bytesUsed]);

		bytesUsed += sizeof(AchievementListV1);
		bytesUsed = (bytesUsed + 16 - 1) & ~(16 - 1);
		curBuf = &(buffer[bytesUsed]);

		m_MaxAchievements = maxAchievements;
		m_Achievements = new (curBuf) Achievement[maxAchievements];

		bytesUsed += sizeof(Achievement) * maxAchievements;
		bytesUsed = (bytesUsed + 16 - 1) & ~(16 - 1);
		curBuf = &(buffer[bytesUsed]);

		if(detailFlags & IAchievement::ACHIEVEMENT_DETAILS_STRINGS)
		{
			for(u32 i = 0; i < m_MaxAchievements; i++)
			{
				m_Achievements[i].m_Label = new (curBuf) char[IAchievement::MAX_LABEL_LEN];

				bytesUsed += IAchievement::MAX_LABEL_LEN;
				curBuf = &(buffer[bytesUsed]);

				m_Achievements[i].m_Description = new (curBuf) char[IAchievement::MAX_DESC_LEN];

				bytesUsed += IAchievement::MAX_DESC_LEN;
				curBuf = &(buffer[bytesUsed]);

				m_Achievements[i].m_UnachievedString = new (curBuf) char[IAchievement::MAX_UNACHIEVED_LEN];

				bytesUsed += IAchievement::MAX_UNACHIEVED_LEN;
				curBuf = &(buffer[bytesUsed]);
			}
		}

	}

	static u32 GetMemoryRequired(IAchievement::DetailFlags detailFlags, u32 maxAchievements)
	{
		u32 bytesRequired = 0;
		bytesRequired += sizeof(AchievementListV1);
		bytesRequired = (bytesRequired + 16 - 1) & ~(16 - 1);
		bytesRequired += sizeof(Achievement) * maxAchievements;
		bytesRequired = (bytesRequired + 16 - 1) & ~(16 - 1);

		if(detailFlags & IAchievement::ACHIEVEMENT_DETAILS_STRINGS)
		{
			u32 bytesRequiredForStrings = IAchievement::MAX_LABEL_LEN + IAchievement::MAX_DESC_LEN + IAchievement::MAX_UNACHIEVED_LEN;
			bytesRequired += bytesRequiredForStrings * maxAchievements;
		}

		return bytesRequired;
	}

	virtual u32 GetMaxAchievements() const
	{
		return m_MaxAchievements;
	}

	virtual u32 GetNumAchievements() const
	{
		return m_NumAchievments;
	}

	virtual Achievement* GetAchievement(const u32 index)
	{
		if(index < m_NumAchievments)	
		{
			return &m_Achievements[index];
		}
		return NULL;
	}

	virtual Achievement* GetAchievementArray()
	{
		return m_Achievements;
	}

	virtual void Clear()
	{
		m_MaxAchievements = 0;
		m_NumAchievments = 0;
		m_Achievements = NULL;
	}

	virtual bool AddAchievement()
	{
		if(m_NumAchievments >= m_MaxAchievements)
		{
			return false;
		}

		++m_NumAchievments;
		return true;
	}

	u32 m_MaxAchievements;
	u32 m_NumAchievments;
	Achievement* m_Achievements;
};

typedef AchievementListV1 AchievementList;

////////////////////////////////////////////////////////////////////////////////
// AchievementBitSet
////////////////////////////////////////////////////////////////////////////////
class AchievementBitSet
{
private:
	atFixedBitSet<IAchievementManager::MAX_NUM_ACHIEVEMENTS> m_Bits;
	CompileTimeAssert((IAchievementManager::MAX_NUM_ACHIEVEMENTS % 8) == 0);

	const atFixedBitSet<IAchievementManager::MAX_NUM_ACHIEVEMENTS>& GetBitSet() const;

public:
	static const unsigned MAX_BYTE_SIZEOF_PAYLOAD = (IAchievementManager::MAX_NUM_ACHIEVEMENTS >> 3);

	AchievementBitSet();
	void Clear();
	bool IsSet(AchievementId id) const;
	bool Set(AchievementId id);
	void Union(const AchievementBitSet& bits);
	void Intersect(const AchievementBitSet& bits);
	int CountOnBits() const;
	bool Import(const void* buf, const unsigned sizeOfBuf, unsigned* size = 0);
	bool Export(void* buf, const unsigned sizeOfBuf, unsigned* size) const;
};

////////////////////////////////////////////////////////////////////////////////
// AchievementsFile
// The achievements file is a record of awarded achievements for the
// locally signed-in player. It must be kept in sync with the online database.
// If achievements are awarded while the player is offline, we sync those
// achievements to the backend when the player signs online.
//
// The file also acts as a cache for achievement timestamps that come from
// the backend database. This is to minimize the number of timestamp queries
// we send to the servers.
////////////////////////////////////////////////////////////////////////////////
class AchievementsFile
{
    friend class RgscAchievementManager;

private:

	////////////////////////////////////////////////////////////////////////////////
	// File Format
	////////////////////////////////////////////////////////////////////////////////
	/*
	achievements.dat:

	// entire file is encrypted and signed using datProtect with the profile's unique key so it's tied to the specific player.
	{
		{
			version					// 32-bits, currently version = 1
			'ACHV'					// 4-byte file 'sig'
			ros title name			// example "lanoire" (padded to 32 bytes, extra bytes set to 0's) 
		}

		// inner block is encrypted and signed using datProtect with the ros title name so it's tied to the specific title.
		// Important: the DLL must be able to read and write achievement data for any title, not just the current title.
		//            This is required so that the DLL can do a two-way sync of achievement data for all titles when signing into the platform.
		{
			bits					// 256-bits, each bit represents an achievement, bit position = achievement Id, 1 = awarded, 0 = not awarded.
			offline bits			// same as above but bits that are set to 1 in this bitset represent achievements that were awarded offline
			timestamps				// 64-bit timestamp for each achievement (256 x 64-bits = 2048 bytes = 2KB)
			datProtect signature	// 44 byte datProtect signature for the inner block
		}

		datSign signature			// 44 byte datProtect signature for the entire file
	}

	Total file size: 2240 bytes.
	*/

    ////////////////////////////////////////////////////////////////////////////////
    // Header
    ////////////////////////////////////////////////////////////////////////////////
    class Header
    {
        friend class AchievementsFile;

    private:

		static const unsigned SIG_LENGTH = 4;
        static const unsigned MAX_BYTE_SIZEOF_PAYLOAD = sizeof(u32) +				// m_Version
                                                        sizeof(char) * SIG_LENGTH;	// m_Sig, null terminator is not stored in the file


        u32 m_Version;
        char m_Sig[SIG_LENGTH + 1]; // + 1 for null terminator

        bool IsValidVersion(u32 version) const;
		u32 GetVersion();

        char* GetExpectedSig() const;
        char* GetSig();

        Header();

        void Clear();

        bool Import(const void* buf, const unsigned sizeOfBuf, unsigned* size = 0);

        bool Export(u32 version, void* buf, const unsigned sizeOfBuf, unsigned* size) const;
    };

private:
    Header m_Header;
    AchievementBitSet m_AwardedAchievements;
    AchievementBitSet m_AwardedOffline;
    time_t m_Timestamps[IAchievementManager::MAX_NUM_ACHIEVEMENTS];
    bool m_Dirty : 1;

    static const unsigned MAX_BYTE_SIZEOF_PAYLOAD_V1 = Header::MAX_BYTE_SIZEOF_PAYLOAD +										// header
													32 +																	// ROS title name
                                                    AchievementBitSet::MAX_BYTE_SIZEOF_PAYLOAD +							// awarded bits
                                                    AchievementBitSet::MAX_BYTE_SIZEOF_PAYLOAD +							// awarded offline bits
                                                    (IAchievementManager::MAX_NUM_ACHIEVEMENTS * sizeof(time_t)) +			// timestamps
                                                    datProtectSignatureSize * 2;											// two datProtect signatures (profile + title)
	
	static const unsigned MAX_BYTE_SIZEOF_PAYLOAD_V2 = Header::MAX_BYTE_SIZEOF_PAYLOAD +										// header
													   32 +																	// ROS title name
													   AchievementBitSet::MAX_BYTE_SIZEOF_PAYLOAD +							// awarded bits
													   AchievementBitSet::MAX_BYTE_SIZEOF_PAYLOAD +							// awarded offline bits
													   (IAchievementManager::MAX_NUM_ACHIEVEMENTS * sizeof(time_t)) +		// timestamps
													   (IAchievementManager::MAX_NUM_ACHIEVEMENTS * sizeof(s64)) +			// progress
													   datProtectSignatureSize * 2;											// two datProtect signatures (profile + title)

	static const unsigned MAX_BYTE_SIZEOF_PAYLOAD = MAX_BYTE_SIZEOF_PAYLOAD_V2;

	static const int VERSION_1 = 1;
	static const int VERSION_2 = 2;

	static const int MIN_VERSION = VERSION_1;
	static const int MAX_VERSION = VERSION_2;
	static const int MIN_VERSION_FOR_PROGRESS = 2;

public:
	AchievementsFile();

    ~AchievementsFile();

    void Clear();

    const char* GetFilename();

	void SetExportVersion(u32 version) { m_ExportVersion = version; }

    bool Open();

    void Close();

#if __DEV
	bool Delete();
#endif

    bool IsDirty();

	bool IsOpen();

    const AchievementBitSet& GetAchievementBitSet();

    const AchievementBitSet& GetAwardedOfflineBitSet();

	bool WriteProgress(const AchievementId achievementId, const s64 progress);
	const s64 GetProgress(const AchievementId achievementId) const;

	static u32 GetMinVersionForProgress();
	static u32 GetExpectedPayloadSize(u32 version);
	static u32 IsPayloadSizeValid(u32 size);

    bool Import(const void* buf, const unsigned sizeOfBuf, unsigned* size = NULL, unsigned* importVersion = NULL);

    bool Read();

    bool Export(void* buf, const unsigned sizeOfBuf, unsigned* size) const;

    bool Write();

    bool WriteAchievement(const AchievementId achievementId);

    bool MergeAchievementBitSets(const AchievementBitSet &toMerge, bool &changed);

    bool MergeAwardedOfflineBitSets(const AchievementBitSet &toMerge, bool &changed);

    bool SetAchievementTimestamp(const AchievementId achievementId, time_t timestamp);
    time_t GetAchievementTimestamp(const AchievementId achievementId);

    const fiDevice* m_Device;
    fiHandle m_FileHandle;
	s64 m_AchievementProgress[RgscAchievementManager::MAX_NUM_ACHIEVEMENTS];
	u32 m_ExportVersion;
};

} // namespace rgsc

#pragma warning(pop)

#endif  //RLINE_RLPCACHIEVEMENT_H
