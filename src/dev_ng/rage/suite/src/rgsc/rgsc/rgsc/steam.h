// 
// rgsc/steam.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPSTEAM_H
#define RLINE_RLPSTEAM_H

#define RGSC_STEAM_EXPLICIT_SUPPORT (RSG_PC && 0)
#if RGSC_STEAM_EXPLICIT_SUPPORT

#include "file/file_config.h"

#include "diag/seh.h"
#include "tasks_interface.h"
#include "net/status.h"
#include "net/task.h"
#include "rline/rltask.h"
#include "system/criticalsection.h"

#pragma warning(disable: 4265)
#include "../../thirdparty/steam/include/steam_api.h"

using namespace rage;

#define STEAM_DECL_FUNC(T, funcName) \
	typedef T* (*funcName##Ptr)(); \
	funcName##Ptr m_##funcName;

#define STEAM_DECL_MEMBER(T, member) \
	typedef T* (*member##Ptr)(); \
	T* m_##member;

namespace rgsc
{
	class RgscSteamManager
	{
	public:
		RgscSteamManager();
		bool Init();
		bool Shutdown();

	public:
		const char* GetSteamPersona();

	private:
		HMODULE m_hSteamDll;

		STEAM_DECL_FUNC(bool, SteamAPI_Init);
		STEAM_DECL_FUNC(void, SteamAPI_Shutdown);

		STEAM_DECL_MEMBER(ISteamUser, SteamUser);
		STEAM_DECL_MEMBER(ISteamFriends, SteamFriends);
		STEAM_DECL_MEMBER(ISteamUtils, SteamUtils);
		STEAM_DECL_MEMBER(ISteamMatchmaking, SteamMatchmaking);
		STEAM_DECL_MEMBER(ISteamUserStats, SteamUserStats);
		STEAM_DECL_MEMBER(ISteamApps, SteamApps);
		STEAM_DECL_MEMBER(ISteamNetworking, SteamNetworking);
		STEAM_DECL_MEMBER(ISteamMatchmakingServers, SteamMatchmakingServers);
		STEAM_DECL_MEMBER(ISteamRemoteStorage, SteamRemoteStorage);
		STEAM_DECL_MEMBER(ISteamScreenshots, SteamScreenshots);
		STEAM_DECL_MEMBER(ISteamHTTP, SteamHTTP);
		STEAM_DECL_MEMBER(ISteamUnifiedMessages, SteamUnifiedMessages);
		STEAM_DECL_MEMBER(ISteamController, SteamController);
		STEAM_DECL_MEMBER(ISteamUGC, SteamUGC);
	};
}

#endif // RGSC_STEAM_EXPLICIT_SUPPORT

#endif  //RLINE_RLPSTEAM_H
