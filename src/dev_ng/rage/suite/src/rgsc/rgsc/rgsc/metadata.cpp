// 
// rline/metadata.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "metadata.h"

#if RSG_PC

#include "rgsc.h"
#include "data/bitbuffer.h"
#include "diag/channel.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "file/device.h"
#include "rline/rldiag.h"
#include "string/stringhash.h"
#include "system/memops.h"
#include "system/memory.h"
#include "system/timer.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <shlobj.h>
#include <time.h>
#pragma warning(pop)

using namespace rage;

namespace rgsc
{

extern sysMemAllocator* g_RgscAllocator;

#if __RAGE_DEPENDENCY
RAGE_DECLARE_SUBCHANNEL(rline, pc);
RAGE_DEFINE_SUBCHANNEL(rgsc_dbg, metadata);
#undef __rage_channel
#define __rage_channel rgsc_dbg_metadata
#endif

////////////////////////////////////////////////////////////////////////////////
// RgscMetadata::Table::Index::Metadata
////////////////////////////////////////////////////////////////////////////////
RgscMetadata::Table::Index::Metadata::Metadata()
{
	Clear();
}

RgscMetadata::Table::Index::Metadata::~Metadata()
{
	Clear();
}

void
RgscMetadata::Table::Index::Metadata::Clear()
{
	m_IsLoaded = false;

	m_IndexType = NO_INDEX;
	m_KeySize = 0;
	m_OffsetSize = 0;
	m_Reserved = 0;

	m_MinRecordSize = 0;
	m_MaxRecordSize = 0;
	m_MinKey = 0;
	m_MaxKey = 0;
	m_NumRecords = 0;
	m_NumDummyRecords = 0;
	m_TotalIndexSize = 0;
	m_TotalDataSize = 0;
}

bool 
RgscMetadata::Table::Index::Metadata::Import(const void* buf,
											   const unsigned sizeOfBuf,
											   unsigned* size)
{
	datImportBuffer bb;
	bb.SetReadOnlyBytes(buf, sizeOfBuf);

	bool success = false;

	Clear();

	rtry
	{
		rverify(buf, catchall, );

		rverify(bb.ReadBytes((void *)&m_IndexType, sizeof(u8)), catchall, );
		rverify(m_IndexType <= BTREE_INDEX, catchall, );

		rverify(bb.ReadBytes((void *)&m_KeySize, sizeof(m_KeySize)), catchall, );
		rverify(m_KeySize <= sizeof(u32), catchall, );

		rverify(bb.ReadBytes((void *)&m_OffsetSize, sizeof(m_OffsetSize)), catchall, );
		rverify(m_OffsetSize <= sizeof(u32), catchall, );

		rverify(bb.ReadBytes((void *)&m_Reserved, sizeof(m_Reserved)), catchall, );
		rverify(m_Reserved == 0, catchall, );

		rverify(bb.ReadUns(m_MinRecordSize, sizeof(m_MinRecordSize) << 3), catchall, );
		rverify(bb.ReadUns(m_MaxRecordSize, sizeof(m_MaxRecordSize) << 3), catchall, );
		rverify(bb.ReadUns(m_MinKey, sizeof(m_MinKey) << 3), catchall, );
		rverify(bb.ReadUns(m_MaxKey, sizeof(m_MaxKey) << 3), catchall, );
		rverify(bb.ReadUns(m_NumRecords, sizeof(m_NumRecords) << 3), catchall, );
		rverify(bb.ReadUns(m_NumDummyRecords, sizeof(m_NumDummyRecords) << 3), catchall, );
		rverify(bb.ReadUns(m_TotalIndexSize, sizeof(m_TotalIndexSize) << 3), catchall, );
		rverify(bb.ReadUns(m_TotalDataSize, sizeof(m_TotalDataSize) << 3), catchall, );

		m_IsLoaded = true;

		success = true;        
	}
	rcatchall
	{
	}

	if(size){*size = success ? bb.GetNumBytesRead() : 0;}

	return success;
}

////////////////////////////////////////////////////////////////////////////////
// RgscMetadata::Table::Index::ImplicitKC
////////////////////////////////////////////////////////////////////////////////
bool
RgscMetadata::Table::Index::ImplicitKC::Import(const void* /*buf*/,
											   const unsigned ASSERT_ONLY(sizeOfBuf),
											   unsigned* size)
{
	Assert(sizeOfBuf == 0);
	Assert(m_Metadata.IsLoaded());
	Assert(m_Metadata.GetIndexType() == NO_INDEX);
	Assert(m_Metadata.GetMinRecordSize() == m_Metadata.GetMaxRecordSize());
	Assert(m_Metadata.GetKeySize() == 0);
	Assert(m_Metadata.GetTotalIndexSize() == 0);
	
	if(size)
	{
		*size = 0;
	}

	return true;
}

bool
RgscMetadata::Table::Index::ImplicitKC::GetRecordOffsetAndSize(u32 key, u32 &offset, u32 &size)
{
	Assert(m_Metadata.IsLoaded());
	Assert(m_Metadata.GetIndexType() == NO_INDEX);
	Assert(m_Metadata.GetMinRecordSize() == m_Metadata.GetMaxRecordSize());
	Assert(m_Metadata.GetKeySize() == 0);
	Assert(m_Metadata.GetTotalIndexSize() == 0);

	bool exists = false;
	offset = 0;
	size = 0;

	if(AssertVerify(key >= m_Metadata.GetMinKey() && key <= m_Metadata.GetMaxKey()))
	{
		u32 index = key - m_Metadata.GetMinKey();
		if(AssertVerify(index < m_Metadata.GetNumRecords()))
		{
			exists = true;
			size = m_Metadata.GetMinRecordSize();
			offset = size * index;
		}
	}
	return exists;
}

////////////////////////////////////////////////////////////////////////////////
// RgscMetadata::Table::Index::SequentialArrayKC
////////////////////////////////////////////////////////////////////////////////
RgscMetadata::Table::Index::SequentialArrayKC::SequentialArrayKC(const Metadata &metadata)
: KeyContainer(metadata)
, m_Array(NULL)
{

}

RgscMetadata::Table::Index::SequentialArrayKC::~SequentialArrayKC()
{
	if(m_Array)
	{
		RGSC_FREE(m_Array);
		m_Array = NULL;
	}
}

bool
RgscMetadata::Table::Index::SequentialArrayKC::Import(const void* buf,
											   const unsigned sizeOfBuf,
											   unsigned* size)
{
	Assert(m_Metadata.IsLoaded());
	Assert(m_Metadata.GetIndexType() == ARRAY_INDEX);
	Assert(m_Metadata.GetKeySize() == 0);
	Assert(m_Metadata.GetTotalIndexSize() >= 0);
	
	bool success = false;

	rtry
	{
		rverify(buf, catchall, );
		rverify(sizeOfBuf >= m_Metadata.GetTotalIndexSize(), catchall, );

		m_Array = RGSC_ALLOCATE(RgscMetadata, m_Metadata.GetTotalIndexSize());
		rverify(m_Array, catchall, );
		memcpy((void*)m_Array, buf, m_Metadata.GetTotalIndexSize());

		success = true;        
	}
	rcatchall
	{
	}

	if(size){*size = success ? m_Metadata.GetTotalIndexSize() : 0;}

	return success;
}

bool
RgscMetadata::Table::Index::SequentialArrayKC::GetRecordOffsetAndSize(u32 key, u32 &offset, u32 &size)
{
	Assert(m_Metadata.IsLoaded());
	Assert(m_Metadata.GetIndexType() == ARRAY_INDEX);
	Assert(m_Metadata.GetKeySize() == 0);
	Assert(m_Metadata.GetTotalIndexSize() > 0);

	bool exists = false;
	offset = 0;
	size = 0;

	if(AssertVerify(key >= m_Metadata.GetMinKey() && key <= m_Metadata.GetMaxKey()))
	{
		u32 index = key - m_Metadata.GetMinKey();
		if(AssertVerify(index < m_Metadata.GetNumRecords()))
		{
			datImportBuffer bb;
			bb.SetReadOnlyBytes(m_Array, m_Metadata.GetTotalIndexSize());

			exists = true;
			u32 offsetSize = m_Metadata.GetOffsetSize();

			if(index == 0)
			{
				offset = 0;
				bb.ReadUns(size, offsetSize << 3);
			}
			else
			{
				index = index - 1;
				bb.SkipBytes(index * offsetSize);
				bb.ReadUns(offset, offsetSize << 3);
				bb.ReadUns(size, offsetSize << 3);
				size -= offset;
				if(size == 0)
				{
					// this is a dummy record that was added only to make
					// the data sequential so the index is more efficient
					offset = 0;
					exists = false;
				}
			}
		}
	}
	return exists;
}

////////////////////////////////////////////////////////////////////////////////
// RgscMetadata::Table::Index::HashTableKC
////////////////////////////////////////////////////////////////////////////////
RgscMetadata::Table::Index::HashTableKC::HashTableKC(const Metadata &metadata)
: KeyContainer(metadata)
, m_HashTable(NULL)
{

}

RgscMetadata::Table::Index::HashTableKC::~HashTableKC()
{
	if(m_HashTable)
	{
		RGSC_DELETE(m_HashTable);
		m_HashTable = NULL;
	}
}

bool
RgscMetadata::Table::Index::HashTableKC::Import(const void* buf,
											   const unsigned sizeOfBuf,
											   unsigned* size)
{
	Assert(m_Metadata.IsLoaded());
	Assert(m_Metadata.GetIndexType() == HASHTABLE_INDEX);
	Assert(m_Metadata.GetKeySize() > 0);
	Assert(m_Metadata.GetTotalIndexSize() > 0);
	
	datImportBuffer bb;
	bb.SetReadOnlyBytes(buf, sizeOfBuf);

	bool success = false;

	rtry
	{
		rverify(buf, catchall, );
		rverify(sizeOfBuf >= m_Metadata.GetTotalIndexSize(), catchall, );

		m_HashTable = RGSC_NEW(RgscMetadata, HashTable);

		rverify(m_HashTable, catchall, );

		u32 key = 0;
		u32 offset = 0;
		u32 size = 0;
		u32 numRecords = 0;

		do
		{
			rverify(bb.ReadUns(key, m_Metadata.GetKeySize() << 3), catchall, );
			rverify(bb.ReadUns(size, m_Metadata.GetOffsetSize() << 3), catchall, );

			key += m_Metadata.GetMinKey();
			u64 data = offset;
			data = (data << 32) | size;
			(*m_HashTable)[key] = data;
			offset = size;
			numRecords++;
		} while(numRecords < m_Metadata.GetNumRecords());

		Assert(bb.GetNumBytesRead() == (int)m_Metadata.GetTotalIndexSize());

		success = true;        
	}
	rcatchall
	{
	}

	if(size){*size = success ? bb.GetNumBytesRead() : 0;}

	return success;
}

bool
RgscMetadata::Table::Index::HashTableKC::GetRecordOffsetAndSize(u32 key, u32 &offset, u32 &size)
{
	Assert(m_Metadata.IsLoaded());
	Assert(m_Metadata.GetIndexType() == HASHTABLE_INDEX);
	Assert(m_Metadata.GetKeySize() > 0);
	Assert(m_Metadata.GetTotalIndexSize() > 0);

	bool exists = false;
	offset = 0;
	size = 0;

	if(AssertVerify(key >= m_Metadata.GetMinKey() && key <= m_Metadata.GetMaxKey()))
	{
		u64 *mapData = m_HashTable->Access(key);
		if(mapData != NULL)
		{
			exists = true;
			size = (u32)((*mapData) & 0x00000000FFFFFFFF);
			offset = (u32)(*mapData >> 32);
		}
	}
	return exists;
}

////////////////////////////////////////////////////////////////////////////////
// RgscMetadata::Table::Index
////////////////////////////////////////////////////////////////////////////////
RgscMetadata::Table::Index::Index()
: m_Table(NULL)
, m_KeyContainer(NULL)
{
	Clear();
}

RgscMetadata::Table::Index::~Index()

{
	Clear();
}

void
RgscMetadata::Table::Index::Clear()
{
	m_Metadata.Clear();
	m_IsLoaded = false;

	if(m_KeyContainer)
	{
		RGSC_DELETE(m_KeyContainer);
		m_KeyContainer = NULL;
	}
}

bool 
RgscMetadata::Table::Index::ReadMetadata()
{
	Assert(m_Metadata.IsLoaded() == false);

	bool success = false;
	u8 buf[Metadata::MAX_BYTE_SIZEOF_PAYLOAD] = {0};
	unsigned sizeOfBuf = sizeof(buf);
	int bytesRead = m_Table->Read(0, buf, sizeOfBuf);

	if(bytesRead == (int)sizeOfBuf)
	{
		success = m_Metadata.Import(buf, sizeOfBuf);
	}

	return success;
}

RgscMetadata::Table::Index::KeyContainer* 
RgscMetadata::Table::Index::CreateKeyContainer(const Metadata &metadata)
{
	// factory for key containers

	IndexType type = metadata.GetIndexType();
	KeyContainer *container = NULL;

	if(type == NO_INDEX)
	{
		container = RGSC_ALLOCATE_T(RgscMetadata, ImplicitKC);
		if(container)
		{
			new(container) ImplicitKC(metadata);
		}
	}
	else if(type == ARRAY_INDEX)
	{
		container = RGSC_ALLOCATE_T(RgscMetadata, SequentialArrayKC);
		if(container)
		{
			new(container) SequentialArrayKC(metadata);
		}
	}
	else if(type == HASHTABLE_INDEX)
	{
		container = RGSC_ALLOCATE_T(RgscMetadata, HashTableKC);
		if(container)
		{
			new(container) HashTableKC(metadata);
		}
	}
	else
	{
		Assert(false);
	}

	return container;
}

bool 
RgscMetadata::Table::Index::Read()
{
	Assert(IsLoaded() == false);

	bool success = false;
	if(m_Metadata.IsLoaded() == false)
	{
		ReadMetadata();
	}

	if(m_Metadata.IsLoaded())
	{
		m_KeyContainer = CreateKeyContainer(m_Metadata);

		// attempt to allocate the read buffer on the stack if we can
		u32 sizeOfBuf = m_Metadata.GetTotalIndexSize();
		u8 stackBuf[1024];
		void* indexBuf = (void*)stackBuf;
		if(sizeOfBuf > sizeof(stackBuf))
		{
			// index is too large, allocate on the heap instead
			indexBuf = RGSC_ALLOCATE(RgscMetadata, sizeOfBuf);
		}

		if(indexBuf)
		{
			int bytesRead = m_Table->Read(Metadata::MAX_BYTE_SIZEOF_PAYLOAD, indexBuf, sizeOfBuf);

			if(bytesRead == (int)sizeOfBuf)
			{
				bool imported = m_KeyContainer->Import(indexBuf, sizeOfBuf);
				if(imported)
				{
					m_IsLoaded = true;
					success = true;
				}
			}

			if(indexBuf != stackBuf)
			{
				RGSC_FREE(indexBuf);
			}
		}
	}

	return success;
}

bool 
RgscMetadata::Table::Index::GetRecordOffsetAndSize(u32 key, u32 &offset, u32 &size)
{
	bool exists = false;
	if(IsLoaded() == false)
	{
		Read();
	}
	if(IsLoaded())
	{
		exists = m_KeyContainer->GetRecordOffsetAndSize(key, offset, size);
		offset += m_Metadata.GetTotalIndexSize();
	}
	return exists;
}

////////////////////////////////////////////////////////////////////////////////
// RgscMetadata::Table
////////////////////////////////////////////////////////////////////////////////
RgscMetadata::Table::Table(RgscMetadata* database, u32 offset)
{
	Clear();
	m_Index.SetOwningTable(this);
	m_Database = database;
	m_Offset = offset;
	m_IsLoaded = m_Index.Read();
}

RgscMetadata::Table::~Table()
{
	Clear();
}

void 
RgscMetadata::Table::Clear()
{
	m_Index.Clear();
	m_Database = NULL;
	m_Offset = 0;
}

u32 
RgscMetadata::Table::GetMinKey()
{
	Assert(m_Index.IsLoaded());
	return m_Index.m_Metadata.GetMinKey();
}

u32 
RgscMetadata::Table::GetMaxKey()
{
	Assert(m_Index.IsLoaded());
	return m_Index.m_Metadata.GetMaxKey();
}

u32 
RgscMetadata::Table::GetNumRecords()
{
	Assert(m_Index.IsLoaded());
	return m_Index.m_Metadata.GetNumRecords() - m_Index.m_Metadata.GetNumDummyRecords();
}

bool 
RgscMetadata::Table::GetRecordOffsetAndSize(u32 key, u32 &offset, u32 &size)
{
	bool exists = m_Index.GetRecordOffsetAndSize(key, offset, size);
	if(exists)
	{
		offset += Index::Metadata::MAX_BYTE_SIZEOF_PAYLOAD;
	}

	return exists;
}

int 
RgscMetadata::Table::Read(u32 offset,
							void* buf,
							const unsigned sizeOfBuf)
{
	return m_Database->Read(offset + m_Offset, buf, sizeOfBuf);
}

//template<typename T> bool 
//RgscMetadata::Table::GetRecord(u32 key, T& record)
//{
//	u32 offset = 0;
//	u32 size = 0;
//	bool exists = GetRecordOffsetAndSize(key, offset, size);
//	if(exists)
//	{
//		// attempt to allocate the read buffer on the stack if we can
//		u8 stackBuf[1024];
//		void* buf = (void*)stackBuf;
//		if(size > sizeof(stackBuf))
//		{
//			// buf size is too large, allocate on the heap instead
//			buf = RGSC_ALLOCATE(RgscMetadata, size);
//		}
//
//		if(buf)
//		{
//			int bytesRead = Read(offset, buf, size);
//			if(bytesRead == (int)size)
//			{
//				record.Import(buf, size);
//			}
//
//			if(buf != stackBuf)
//			{
//				RGSC_FREE(buf);
//			}
//		}
//	}
//	return exists;
//}

//RgscMetadata::Table::Record* 
//RgscMetadata::Table::GetRecords(u32 firstKey, u32 lastKey)
//{
//	RgscMetadata::Table::Record* record = NULL;
//	u32 offset1 = 0;
//	u32 size1 = 0;
//	bool exists1 = GetRecordOffsetAndSize(firstKey, offset1, size1);
//
//	u32 offset2 = 0;
//	u32 size2 = 0;
//	bool exists2 = GetRecordOffsetAndSize(lastKey, offset2, size2);
//
//	if(exists1 && exists2)
//	{
//		u32 sizeOfBuf = (offset2 + size2) - offset1;
//		void* buf = RGSC_ALLOCATE(RgscMetadata, sizeOfBuf);
//		if(buf)
//		{
//			int bytesRead = Read(offset1, buf, sizeOfBuf);
//			if(bytesRead == (int)sizeOfBuf)
//			{
//				int i = 0;
//				i++;
//			}
//		}
//	}
//
//	return record;
//}

////////////////////////////////////////////////////////////////////////////////
// RgscMetadataHeader
////////////////////////////////////////////////////////////////////////////////
class RgscMetadataHeader
{
	friend class RgscMetadata;

private:

	static const unsigned MAX_BYTE_SIZEOF_PAYLOAD = sizeof(u32) +		// m_Version
													sizeof(char) * 4 +	// m_Sig, null terminator is not stored in the file
													sizeof(char) * 19;	// m_Timestamp, null terminator is not stored in the file

	u32 m_Version;
	char m_Sig[5];
	char m_Timestamp[20];

	u32 GetVersion() {return m_Version;}
	char* GetSig() {return m_Sig;}
	char* GetTimestamp() {return m_Timestamp;}
	bool GetTimestampAsTmStruct(tm &t)
	{
		/*
		struct tm {
			int tm_sec;     // seconds after the minute - [0,59]
			int tm_min;     // minutes after the hour - [0,59]
			int tm_hour;    // hours since midnight - [0,23]
			int tm_mday;    // day of the month - [1,31]
			int tm_mon;     // months since January - [0,11]
			int tm_year;    // years since 1900
			int tm_wday;    // days since Sunday - [0,6]
			int tm_yday;    // days since January 1 - [0,365]
			int tm_isdst;   // daylight savings time flag
		};
		*/

		bool success = false;

		rtry
		{
			// Example: "2011-02-11T02:24:58"
			int n = sscanf_s(m_Timestamp, "%d-%d-%dT%d:%d:%d", &t.tm_year, &t.tm_mon, &t.tm_mday, &t.tm_hour, &t.tm_min, &t.tm_sec);
			rverify(n == 6, catchall, rlError("GetTimestampAsTmStruct: sscanf_s failed to find all expected fields"));

			t.tm_year -= 1900;
			t.tm_mon -= 1;
			success = true;
		}
		rcatchall
		{
		}
		
		return success;
	}

	RgscMetadataHeader()
	{
		Clear();
	}

	void Clear()
	{
		m_Version = 0;
		memset(m_Sig, 0, sizeof(m_Sig));
		memset(m_Timestamp, 0, sizeof(m_Timestamp));
	}

	bool Import(const void* buf,
				const unsigned sizeOfBuf,
				unsigned* size = 0)
	{
		datImportBuffer bb;
		bb.SetReadOnlyBytes(buf, sizeOfBuf);

		bool success = false;

		Clear();

		rtry
		{
			rverify(buf, catchall, );

			rverify(bb.ReadUns(m_Version, sizeof(m_Version) << 3), catchall, );
			rverify(bb.ReadBytes(m_Sig, (sizeof(m_Sig) - 1)), catchall, );
			rverify(bb.ReadBytes(m_Timestamp, (sizeof(m_Timestamp) - 1)), catchall, );

			success = true;        
		}
		rcatchall
		{
		}

		if(size){*size = success ? bb.GetNumBytesRead() : 0;}

		return success;
	}
};

////////////////////////////////////////////////////////////////////////////////
// RgscMetadata
////////////////////////////////////////////////////////////////////////////////
RgscMetadata::RgscMetadata()
: m_Tables(NULL)
, m_Device(NULL)
, m_FileHandle(fiHandleInvalid)
, m_Version(0)
{

}

RgscMetadata::~RgscMetadata()
{
	Shutdown();
}

bool 
RgscMetadata::Init()
{
	if(!GetRgscConcreteInstance()->IsMetadataSupported())
	{
		// early out if the client has no metadata.
		return true;
	}

	bool success = false;

//	OUTPUT_ONLY(size_t memAvail = g_RgscAllocator->GetMemoryAvailable());

	// metadata.dat should be in <current working directory> or <current working directory>/pc/
	// if it's not there, also look in <calling process directory> or <calling process directory>/pc/
	// UPDATE: As of ConfigV5 the game can also set the metadata path, we'll use that path first.
	fiHandle hFile = fiHandleInvalid;

	wchar_t currentDirectoryW[RGSC_MAX_PATH] = {0};
	GetCurrentDirectory(COUNTOF(currentDirectoryW), currentDirectoryW);

	char currentDirectory[RAGE_MAX_PATH];
	WideCharToMultiByte(CP_UTF8, 0, currentDirectoryW, -1, currentDirectory, COUNTOF(currentDirectory), 0, 0);

	char path[RGSC_MAX_PATH];
	safecpy(path, currentDirectory);
	const fiDevice *device = fiDevice::GetDevice(path, true);

	rtry
	{
		// metaDataFilePath passed by the game is assumed to be UTF8 encoded
		const char* metaDataFilePath = GetRgscConcreteInstance()->GetMetaDataPath();
		if(metaDataFilePath && metaDataFilePath[0] != '\0')
		{
			safecpy(path, metaDataFilePath);
			hFile = device->Open(path, true);
			rcheck(!fiIsValidHandle(hFile), foundfile, );
			rlError("Game set metadata path \"%s\" ; could not be loaded", path);
		}

		//check \\metadata.dat
		safecpy(path, currentDirectory);
		safecat(path, "\\metadata.dat");
		hFile = device->Open(path, true);
		rcheck(!fiIsValidHandle(hFile), foundfile, );

		// check \\pc\\metadata.dat
		safecpy(path, currentDirectory);
		safecat(path, "\\pc\\metadata.dat");
		hFile = device->Open(path, true);
		rcheck(!fiIsValidHandle(hFile), foundfile, );

		// check \\x64\\metadata.dat
		safecpy(path, currentDirectory);
		safecat(path, "\\x64\\metadata.dat");
		hFile = device->Open(path, true);
		rcheck(!fiIsValidHandle(hFile), foundfile, );

		// check EXE DIR\\metadata.dat
		rcheck(GetRgscConcreteInstance()->_GetFileSystem()->GetProcessExeDirectory(path), catchall, );
		safecat(path, "metadata.dat");
		hFile = device->Open(path, true);
		rcheck(!fiIsValidHandle(hFile), foundfile, );

		// check EXE DIR\\pc\\metadata.dat
		rcheck(GetRgscConcreteInstance()->_GetFileSystem()->GetProcessExeDirectory(path), catchall, );
		safecat(path, "pc\\metadata.dat");
		hFile = device->Open(path, true);
		rcheck(!fiIsValidHandle(hFile), foundfile, );

		// check EXE DIR\\x64\\metadata.dat
		rcheck(GetRgscConcreteInstance()->_GetFileSystem()->GetProcessExeDirectory(path), catchall, );
		safecat(path, "x64\\metadata.dat");
		hFile = device->Open(path, true);
		rcheck(!fiIsValidHandle(hFile), foundfile, );

		rlError("Failed to load metadata file");
	}
	rcatch(foundfile)
	{
		rlDebug3("Found metadata file at \"%s\"", path);
	}
	rcatchall
	{
		rlError("Failed to load metadata file");
	}

	if(fiIsValidHandle(hFile))
	{
		// copy to global titles directory. This is so we can access
		// this title's metadata from another title.
		char dstPath[RGSC_MAX_PATH] = {0};
		GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformTitleDirectory(dstPath, true);
		safecat(dstPath, "metadata.dat");
		if(AssertVerify(fiDevice::CopySingleFile(path, dstPath)))
		{
			// preserve modification time
			u64 origFileTime = device->GetFileTime(path);
			AssertVerify(device->SetFileTime(dstPath, origFileTime));
		}

		m_Device = device;
		m_FileHandle = hFile;

		u8 buf[RgscMetadataHeader::MAX_BYTE_SIZEOF_PAYLOAD] = {0};		
		unsigned sizeOfBuf = sizeof(buf);
		int bytesRead = Read(0, buf, sizeOfBuf);

		if(bytesRead == (int)sizeOfBuf)
		{
			RgscMetadataHeader header;
			bool imported = header.Import(buf, sizeOfBuf);
			if(imported)
			{
				m_Version = header.GetVersion();
				if(IsValidVersion(m_Version) && (strcmp(header.GetSig(), "META") == 0))
				{
#if !__NO_OUTPUT
					// testing timestamp code
					tm t;
					bool valid = header.GetTimestampAsTmStruct(t);
					if(AssertVerify(valid))
					{
						time_t timet = mktime(&t);
						char szTime[26] = {0};
						time_t testTime = timet;
						struct tm* local = localtime(&testTime);
						safecpy(szTime, asctime(local));
						szTime[strlen(szTime) - 1] = 0;
						rlDebug3("Timestamp %s, converted:%s", header.GetTimestamp(), szTime);
					}
#endif

					m_Tables = RGSC_ALLOCATE_T(RgscMetadata, Table);
					if(m_Tables)
					{
						new(m_Tables) Table(this, RgscMetadataHeader::MAX_BYTE_SIZEOF_PAYLOAD);
						success = m_Tables->IsLoaded();
					}
				}
			}
		}
	}
	else
	{
		Assertf(false, "Could not find metadata.dat");
	}

//	OUTPUT_ONLY(size_t memUsed = memAvail - g_RgscAllocator->GetMemoryAvailable());
//	rlDebug("Mem used: %zu", memUsed);

	return success;
}

void 
RgscMetadata::Update()
{

}

void 
RgscMetadata::Shutdown()
{
	if(m_Device)
	{
		m_Device->Close(m_FileHandle);
		m_Device = NULL;
	}

	m_FileHandle = fiHandleInvalid;

	if(m_Tables)
	{
		m_Tables->~Table();
		RGSC_FREE(m_Tables);
	}

	m_Tables = NULL;
}

int 
RgscMetadata::Read(u32 offset,
					 void* buf,
					 const unsigned sizeOfBuf)
{
	Assert(m_Device);
	Assert(fiIsValidHandle(m_FileHandle));
	m_Device->Seek(m_FileHandle, offset, seekSet);
	return m_Device->Read(m_FileHandle, buf, sizeOfBuf);
}

bool
RgscMetadata::IsValidVersion(u32 version)
{
	// lan/mp3/gta5 = 1
	return (version == 1) || (version == 2);
}

RgscMetadata::Table* 
RgscMetadata::GetTable(const char* tableName)
{
	u32 offset = 0;
	u32 size = 0;

	char lowercaseTableName[100];
	Assert(strlen(tableName) < sizeof(lowercaseTableName));
	safecpy(lowercaseTableName, tableName);
	strlwr(lowercaseTableName);

	bool exists = m_Tables->GetRecordOffsetAndSize(atStringHash(lowercaseTableName), offset, size);
	offset += RgscMetadataHeader::MAX_BYTE_SIZEOF_PAYLOAD;
	RgscMetadata::Table* table = NULL;
	if(exists)
	{
		table = RGSC_ALLOCATE_T(RgscMetadata, Table);
		if(table)
		{
			new(table) Table(this, offset);
			if(table->IsLoaded() == false)
			{
				table->~Table();
				RGSC_FREE(table);
				table = NULL;
			}
		}
	}
	return table;
}

}
#endif // RSG_PC
