// 
// commerces.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RGSC_COMMERCE_MANAGER_H
#define RGSC_COMMERCE_MANAGER_H

#include "commerce_interface.h"
#include "net/status.h"

#include "atl/string.h"
#include "rline/entitlement/rlrosentitlement.h"

using namespace rage;

namespace rgsc
{

// error C4265: class has virtual functions, but destructor is not virtual
// the binary interface for virtual destructors isn't standardized, so don't make the destructor virtual
#pragma warning(push)
#pragma warning(disable: 4265)




// ===========================================================================
// CommerceManager
// ===========================================================================

class RgscCommerceManager : public ICommerceManagerLatestVersion
{
// ===============================================================================================
// inherited from ICommerceManager
// ===============================================================================================
public:
	// ===============================================================================================
	// inherited from IRgscUnknown
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

	// ===============================================================================================
	// inherited from ICommerceManagerV1
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL ShowCommerceUi(const char* url);

	virtual RGSC_HRESULT RGSC_CALL ShowRedemptionCodeUi(RedemptionCodeCallback callback);

	virtual RGSC_HRESULT RGSC_CALL RedemptionCodeResult(const int result, StringId message, RedemptionCodeCallback callback);

	virtual void RGSC_CALL DownloadComplete(const char* name, bool success);

	// ===============================================================================================
	// inherited from ICommerceManagerV2
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL RedemptionCodeResult(const int result, StringId message, const char* contentName, ICommerceManagerV1::RedemptionCodeCallback callback);

	// ===============================================================================================
	// inherited from ICommerceManagerV3
	// ===============================================================================================
#if RSG_CPU_X64
	virtual void RGSC_CALL InitData();
	virtual void RGSC_CALL DestroyData();

	virtual bool RGSC_CALL StartDataFetch( const char* parameterJson );
	virtual const char* RGSC_CALL IsDataValidAsJson();
	virtual const char* RGSC_CALL IsInErrorStateAsJson();

	virtual const char* RGSC_CALL GetDataAsJson();
	virtual const char* RGSC_CALL GetItemDataAsJson( const char *parameterJson );
	virtual const char* RGSC_CALL RequestUrlForCheckout( const char *parameterJson );
	virtual const char* RGSC_CALL GetUserAgeAppropriateData();

	virtual RGSC_HRESULT RGSC_CALL ShowCommerceStore(const char* product, const char* category);

	virtual const char* RGSC_CALL RequestVoucherContent( const char* parameterJson );
	virtual const char* RGSC_CALL RequestVoucherConsumption( const char* parameterJson );

	virtual RGSC_HRESULT RGSC_CALL SetSteamStoreOpenCallback(SteamStoreOpenCallback newCallback); 
	virtual bool RGSC_CALL ShowSteamStore();

	virtual const char* RGSC_CALL StartSteamPurchase( const char *parameterJson );


	virtual const char* RGSC_CALL IsProductInstalled(const char* parameterJson);
	virtual RGSC_HRESULT RGSC_CALL SetContainedPackageInstalledCallback(IsContainedPackageInstalledCallback newCallback);
	virtual bool RGSC_CALL GetContentPathUrl(const char* path, char* outputBuffer, unsigned int outputBufferLength, IAsyncStatus* status);

	// ===============================================================================================
	// inherited from ICommerceManagerV4
	// ===============================================================================================
#if !PRELOADED_SOCIAL_CLUB
	virtual bool RGSC_CALL RequestEntitlementData( const char* machineHash, const char* language );
	virtual bool RGSC_CALL IsEntitlementDataValid();
	virtual bool RGSC_CALL IsEntitlementDataInErrorState();
	virtual const char* RGSC_CALL GetEntitlementDataAsJson();
	virtual bool RGSC_CALL GetEntitlementDownloadUrl(const char* entitlementCode, char* outputBuffer, unsigned int outputBufferLength,IAsyncStatus* status);
    

	virtual bool RGSC_CALL RequestEntitlementBlock( const char* machineHash, const char* locale, char* outputBuffer, unsigned int outputBufferSize, int* outputVersion, unsigned int* outputDataSize, IAsyncStatus* status );
	virtual bool RGSC_CALL DoesEntitlementBlockGrantEntitlement( const RockstarId rockstarId, const char* machineHash, const char* entitlementId, void* dataBuffer, unsigned int dataBufferSize );
// 	virtual bool RGSC_CALL RequestEntitlementBlock( const char* machineHash );
// 	virtual bool RGSC_CALL IsEntitlementBlockValid();
// 	virtual bool RGSC_CALL IsEntitlementBlockInErrorState();
// 	virtual bool RGSC_CALL DoesEntitlementBlockGrantEntitlement( const char* entitlementId );

	// Offline Entitlement
	virtual u8* RGSC_CALL AllocateOfflineEntitlementBlob(unsigned size);
	virtual bool RGSC_CALL SaveOfflineEntitlement(u8* entitlementBuf, unsigned entitlementBufLen, u8* machineHash);
	virtual bool RGSC_CALL LoadOfflineEntitlement(u8* machineHash);
	virtual bool RGSC_CALL HasOfflineEntitlement();
	virtual u8* RGSC_CALL GetOfflineEntitlement();
	virtual unsigned RGSC_CALL GetOfflineEntitlementLength();
	virtual bool RGSC_CALL ClearOfflineEntitlement();
	virtual bool RGSC_CALL IsEntitlementIOInProgress();

	virtual void RGSC_CALL SetPurchaseFlowTelemetryCallback(PurchaseFlowTelemetryCallback callback);
    //Steam status query
    virtual bool RGSC_CALL DidLastSteamPurchaseFail();
#endif // PRELOADED_SOCIAL_CLUB
#endif // RSG_CPU_X64

	// ===============================================================================================
	// inherited from ICommerceManagerV5
	// ===============================================================================================
#if RSG_CPU_X86
	virtual void RGSC_CALL EntitlementV1_RefreshData(bool bForceBackendSync);
	virtual bool RGSC_CALL EntitlementV1_IsCheckingForEntitlement();
	virtual bool RGSC_CALL EntitlementV1_IsEntitlementUpToDate();
	virtual bool RGSC_CALL EntitlementV1_RegisterPurchaseBySN(int entitlementProjectId, char* serialNumber, bool* isActivated, char* skuName, 
															  unsigned skuNameBufLen, char* friendlyName, unsigned friendlyLength, IAsyncStatus* status);
	virtual bool RGSC_CALL EntitlementV1_RegisterPurchaseAndActivationBySN(int entitlementProjectId, char* serialNumber, IAsyncStatus* status);
	virtual bool RGSC_CALL EntitlementV1_GetValidateContentPlayerAge(const char* contentName, IAsyncStatus* status);
	virtual bool RGSC_CALL EntitlementV1_IsEntitledToContent(const char* skuName);
	virtual bool RGSC_CALL EntitlementV1_GetActivationState(const char* skuName, bool* bIsOwned, char (&serialKey)[256], bool* bIsActivated);
	virtual bool RGSC_CALL EntitlementV1_GetContentNameBySku(const char * skuName, char * contentName, unsigned int contentNameLen);
	virtual bool RGSC_CALL EntitlementV1_GetSkuDownloadURL(const char* skuName, char* downloadUrl, unsigned int downloadUrlBufLen, IAsyncStatus* status);
#endif // RSG_CPU_X86

// ===============================================================================================
// accessible from anywhere in the dll
// ===============================================================================================
public:

    RgscCommerceManager();
    ~RgscCommerceManager();

	bool Init();
	void Update();
	void Shutdown();

	void SignOut();

	void ClearCallbacks();

	void RedemptionCodeResponse(const char* jsonResponse);
	void UserResponse(const char* jsonResponse);

	RedemptionCodeCallback m_RedemptionCodeCallback;
	RedemptionCodeCallback m_UserResponseCallback;
	
#if RSG_CPU_X64
	const char* CommerceDataResponse();
	const char* GetPurchaseFlowTelemetry();	

private:

	void InitCommerceUtil();
	void ShutdownCommerceUtil();

	void UpdateVoucherRedemption();
	void UpdateVouchingConsuming();
	void UpdateVoucherGettingContents();
    void UpdateManifestFetch();

	void UpdateCheckoutUrlFetch();

#if !PRELOADED_SOCIAL_CLUB 
	void UpdateGetEntitlement();
#endif

	const char* GetItemNodeFromItemArray( const char *id );
	
	int ConvertVoucherResultCodeToStringId(int resultCode);

	enum eAgeDataState
	{
		AGE_DATA_STATE_INIT,
		AGE_DATA_STATE_FETCHING,
		AGE_DATA_STATE_APPROVED,
		AGE_DATA_STATE_DECLINED,
		AGE_DATA_STATE_ERROR,
		AGE_DATA_STATE_NUM_ERRORS
	};

	enum eVoucherRedemptionState
	{
		VOUCHER_REDEMPTION_NEUTRAL,
		VOUCHER_REDEMPTION_GETTING_CONTENTS,
		VOUCHER_REDEMPTION_CONSUMING,
		VOUCHER_REDEMPTION_ERROR
	};

	eVoucherRedemptionState m_CurrentVoucherRedemptionState;

	enum eCommerceManifestFetchState
	{
		COMMERCE_MANIFEST_FETCH_NEUTRAL = 0,
		COMMERCE_MANIFEST_FETCH_FETCHING,
		COMMERCE_MANIFEST_FETCH_NUM_STATES
	};

	eCommerceManifestFetchState m_CurrentCommerceManifestFetchState;

	int m_LocalGamerIndex;
	eAgeDataState m_CurrentAgeDataState;

	netStatus m_UserAgeDataFetchStatus;
	netStatus m_VoucherRedemptionStatus;
	rlV2VoucherPreviewResponse m_VoucherPreviewData;

	atString m_CommerceManifest;
	netStatus m_CommerceManifestFetchStatus;
	bool m_LastCommerceManifestFetchFailed;

	enum 
	{
		COMMERCE_URL_MAX_LENGTH = 2048
	};

	enum eCheckoutUrlFetchState
	{
		CHECKOUT_URL_FETCH_NEUTRAL = 0,
		CHECKOUT_URL_FETCH_FETCHING,
		CHECKOUT_URL_FETCH_NUM_STATES
	};

	netStatus m_CheckoutUrlFetchStatus;
	eCheckoutUrlFetchState m_CheckoutUrlFetchState;
	char m_CheckoutUrlFetchBuffer[COMMERCE_URL_MAX_LENGTH];

	SteamStoreOpenCallback m_CurrentSteamStoreOpenCallback;
	IsContainedPackageInstalledCallback m_IsContainedPackageInstalledCallback;
#if !PRELOADED_SOCIAL_CLUB
	PurchaseFlowTelemetryCallback m_PurchaseFlowTelemetryCallback;
#endif

	netStatus m_StartSteamTransactionStatus;

	//Entitlement data
	netStatus m_EntitlementStatus;
	rlV2EntitlementResponse m_EntitlementData;
	bool m_EntitlementDataPopulated;
	bool m_EntitlementDataError;
#endif
};

} // namespace rgsc

#pragma warning(pop)

#endif  // RGSC_COMMERCE_MANAGER_H
