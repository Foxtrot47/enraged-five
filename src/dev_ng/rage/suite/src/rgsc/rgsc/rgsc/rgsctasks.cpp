// 
// rline/rgsctasks.cpp 
// 
// Copyright (C) 2014-2014 Rockstar Games.  All Rights Reserved. 
//
#include "rgsctasks.h"

// rage includes
#include "rline/scpresence/rlscpresencetasks.h"
#include "rline/scmatchmaking/rlscmatchmaking.h"

using namespace rage;

namespace rgsc
{

#if __RAGE_DEPENDENCY
RAGE_DEFINE_SUBCHANNEL(rgsc_dbg, rgsctask)
#undef __rage_channel
#define __rage_channel rgsc_dbg_rgsctask
#endif

///////////////////////////////////////////////////////////////////////////////
//  OnGameCrashTask
///////////////////////////////////////////////////////////////////////////////
OnGameCrashTask::OnGameCrashTask()
	: m_State(STATE_INVALID)
	, m_LocalGamerIndex(-1)
{
	
}

bool OnGameCrashTask::Configure(Rgsc* /*ctx*/, const int localGamerIndex, const u64 actionToTake)
{
	bool success = false;

	rtry
	{
		m_State = STATE_INVALID;
		rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex), catchall, );
		rverify(actionToTake != 0, catchall, );
		m_LocalGamerIndex = localGamerIndex;
		m_ActionToTake = actionToTake;
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void OnGameCrashTask::Start()
{
	rlDebug2("On Game Crash...");

	this->RgscTaskBase::Start();

	m_State = STATE_UNADVERTISE_SESSIONS;
}

void OnGameCrashTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("OnGameCrashTask %s", FinishString(finishType));
	this->RgscTaskBase::Finish(finishType, resultCode);
}

void OnGameCrashTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
}

void OnGameCrashTask::Update(const unsigned timeStep)
{
	this->RgscTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_UNADVERTISE_SESSIONS:
			if (UnAdvertiseAll())
			{
				m_State = STATE_UNADVERTISING_SESSIONS;
			}
			else
			{
				// Try to sign out anyway
				m_State = STATE_SIGN_OUT;
			}
			break;
		case STATE_UNADVERTISING_SESSIONS:
			if (m_MyStatus.Succeeded())
			{
				m_State = STATE_SIGN_OUT;
			}
			else if (!m_MyStatus.Pending())
			{
				// Try to sign out anyway
				m_State = STATE_SIGN_OUT;
			}
			break;
		case STATE_SIGN_OUT:
			if (SignOut())
			{
				m_State = STATE_SIGNING_OUT;
			}
			else
			{
				// Task succeeds regardless
				m_State = STATE_SUCCEEDED;
			}
			break;
		case STATE_SIGNING_OUT:
			if (m_MyStatus.Succeeded())
			{
				m_State = STATE_SUCCEEDED;
			}
			else if (m_MyStatus.Failed())
			{
				m_State = STATE_FAILED;
			}
			break;
		case STATE_SUCCEEDED:
			this->Finish(FINISH_SUCCEEDED);
			break;
		case STATE_FAILED:
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool OnGameCrashTask::UnAdvertiseAll()
{
	rtry
	{
		rcheck((m_ActionToTake & OGC_UNADVERTISE_ALL_MATCHES) != 0, catchall, );
		rverify(rlScMatchmaking::UnadvertiseAll(m_LocalGamerIndex, &m_MyStatus), catchall, );
		return true;
	}
	rcatchall
	{
		return false;
	}

}

bool OnGameCrashTask::SignOut()
{
	bool success = false;
	rlScSignOutTask* task = NULL;

	rtry
	{
		rcheck((m_ActionToTake & OGC_PRESENCE_SIGNOUT) != 0, catchall, );

		netStatus* status = &m_MyStatus;
		rverify(rlGetTaskManager()->CreateTask(&task),catchall, rlError("Error creating task"));
		rverify(rlTaskBase::Configure(task, m_LocalGamerIndex, status), catchall, rlError("Error configuring task"));
		rverify(rlGetTaskManager()->AddParallelTask(task), catchall, rlError("Error queuing task"));

		success = true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}
	}

	return success;
}

} // namespace rgsc
