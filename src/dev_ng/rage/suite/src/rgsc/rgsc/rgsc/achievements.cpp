// 
// rline/achievements.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "file/file_config.h"
#include "achievements.h"
#include "achievementtasks.h"

#if RSG_PC

#include "rgsc.h"
#include "json.h"
#include "rline/rldiag.h"
#include "ros/rlros.h"
#include "ros/rlroshttptask.h"
#include "atl/bitset.h"
#include "data/bitbuffer.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "parser/manager.h"
#include "system/nelem.h"
#include "system/timer.h"

#include <time.h>

using namespace rage;

namespace rgsc
{

#if __RAGE_DEPENDENCY
RAGE_DEFINE_SUBCHANNEL(rgsc_dbg, achievement)
#undef __rage_channel
#define __rage_channel rgsc_dbg_achievement
#endif

#if 0 //RSG_CPU_X64
NOSTRIP_PC_PARAM(sccrashtest, "crashy crash crash");
#endif

// MAX_NUM_ACHIEVEMENTS must always be exactly 256 for backward compatibility
CompileTimeAssert((IAchievementManager::MAX_NUM_ACHIEVEMENTS) == 256);

PARAM(redoAchievements, "Allows you to send notifications for the same achievement more than once");

bool RgscAchievementManager::sm_FileIoInProgress = false;

////////////////////////////////////////////////////////////////////////////////
// AchievementBitSet
////////////////////////////////////////////////////////////////////////////////
AchievementBitSet::AchievementBitSet()
{
	Clear();
}

const atFixedBitSet<IAchievementManager::MAX_NUM_ACHIEVEMENTS>& AchievementBitSet::GetBitSet() const
{
	return m_Bits;
}

void AchievementBitSet::Clear()
{
	m_Bits.Reset();
}

bool AchievementBitSet::IsSet(AchievementId id) const
{
	bool isSet = false;
	if(AssertVerify((int)id < m_Bits.GetNumBits()))
	{
		isSet = m_Bits.IsSet(id);
	}

	return isSet;
}

bool AchievementBitSet::Set(AchievementId id)
{
	bool success = false;
	if(AssertVerify((int)id < m_Bits.GetNumBits()))
	{
		Assert(m_Bits.IsSet(id) == false);
		m_Bits.Set(id);
		success = true;
	}

	return success;
}

void AchievementBitSet::Union(const AchievementBitSet& bits)
{
	m_Bits.Union(bits.GetBitSet());
}

void AchievementBitSet::Intersect(const AchievementBitSet& bits)
{
	m_Bits.Intersect(bits.GetBitSet());
}

int AchievementBitSet::CountOnBits() const
{
    return m_Bits.CountOnBits();
}

bool AchievementBitSet::Import(const void* buf, const unsigned sizeOfBuf, unsigned* size)
{
	datImportBuffer bb;
	bb.SetReadOnlyBytes(buf, sizeOfBuf);

	bool success = false;

	Clear();

	rtry
	{
		rverify(buf, catchall, );
		rverify(sizeOfBuf >= MAX_BYTE_SIZEOF_PAYLOAD, catchall, );

		u32* rawBits = NULL;
		u32 ignored = 0;
		rverify(m_Bits.GetRaw(&rawBits, &ignored), catchall, );
		rverify(m_Bits.GetNumBits() == IAchievementManager::MAX_NUM_ACHIEVEMENTS, catchall, );
		rverify(bb.ReadBits((void*)rawBits, m_Bits.GetNumBits(), 0), catchall, );

#if !__NO_OUTPUT
		rlDebug("Importing achievement bits:");
		for(int i = 0; i < m_Bits.GetNumBits(); i++)
		{
			if(m_Bits.IsSet(i))
			{
				rlDebug("    %d", i);
			}
		}
#endif

		success = true;        
	}
	rcatchall
	{
	}

	if(size){*size = success ? bb.GetNumBytesRead() : 0;}

	return success;
}

bool AchievementBitSet::Export(void* buf, const unsigned sizeOfBuf, unsigned* size) const
{
	datExportBuffer bb;
	bb.SetReadWriteBytes(buf, sizeOfBuf);

	bool success = false;

	rtry
	{
		rverify(buf, catchall, );
		rverify(sizeOfBuf >= MAX_BYTE_SIZEOF_PAYLOAD, catchall, );
		sysMemSet(buf, 0, sizeOfBuf);

		const u32* rawBits = NULL;
		u32 ignored = 0;
		rverify(m_Bits.GetRaw(&rawBits, &ignored), catchall, );
		rverify(m_Bits.GetNumBits() == IAchievementManager::MAX_NUM_ACHIEVEMENTS, catchall, );
		rverify(bb.WriteBits((void*)rawBits, m_Bits.GetNumBits(), 0), catchall, );

#if !__NO_OUTPUT
		rlDebug("Exporting achievement bits:");
		for(int i = 0; i < m_Bits.GetNumBits(); i++)
		{
			if(m_Bits.IsSet(i))
			{
				rlDebug("    %d", i);
			}
		}
#endif

		success = true;        
	}
	rcatchall
	{
	}

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

	return success;
}

////////////////////////////////////////////////////////////////////////////////
// AchievementsFile
////////////////////////////////////////////////////////////////////////////////
bool AchievementsFile::Header::IsValidVersion(u32 version) const 
{
	return (version >= MIN_VERSION) && (version <= MAX_VERSION);
}

u32 AchievementsFile::Header::GetVersion() 
{
	return m_Version;
}

char* AchievementsFile::Header::GetExpectedSig() const
{
	return "ACHV";
}

char* AchievementsFile::Header::GetSig() 
{
	return m_Sig;
}

AchievementsFile::Header::Header()
{
	Clear();
}

void AchievementsFile::Header::Clear()
{
	m_Version = 0;
	memset(m_Sig, 0, sizeof(m_Sig));
}

bool AchievementsFile::Header::Import(const void* buf, const unsigned sizeOfBuf, unsigned* size)
{
    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeOfBuf);

    bool success = false;

    Clear();

    rtry
    {
        rverify(buf, catchall, );

        // read
        rverify(bb.ReadUns(m_Version, sizeof(m_Version) << 3), catchall, );
        rverify(bb.ReadBytes(m_Sig, sizeof(char) * SIG_LENGTH), catchall, );

        // verify
        rverify(IsValidVersion(m_Version), catchall, );
        rverify(strcmp(GetSig(), GetExpectedSig()) == 0, catchall, );

        success = true;        
    }
    rcatchall
    {
    }

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}

    return success;
}

bool AchievementsFile::Header::Export(u32 version, void* buf, const unsigned sizeOfBuf, unsigned* size) const
{
	datExportBuffer bb;
	bb.SetReadWriteBytes(buf, sizeOfBuf);

	bool success = false;

	char *sig = GetExpectedSig();

	rtry
	{
		rverify(buf, catchall, );
		rverify(sizeOfBuf >= MAX_BYTE_SIZEOF_PAYLOAD, catchall, );

		rverify(bb.WriteUns(version, sizeof(version) << 3), catchall, );
		rverify(bb.WriteBytes(sig, sizeof(char) * SIG_LENGTH), catchall, );

		success = true;        
	}
	rcatchall
	{
	}

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

	return success;
}

AchievementsFile::AchievementsFile()
    : m_Device(NULL)
    , m_FileHandle(fiHandleInvalid)
	, m_Dirty(false)
	, m_ExportVersion(MIN_VERSION)
{
	//@@: location ACHIEVEMENTS_CONSTRUCTOR
    Clear();
    Close();
}

AchievementsFile::~AchievementsFile()
{
	Clear();
	Close();
}

void AchievementsFile::Clear()
{
    m_Header.Clear();
    m_AwardedAchievements.Clear();
    m_AwardedOffline.Clear();
    sysMemSet(m_Timestamps, 0, sizeof(m_Timestamps));
	sysMemSet(m_AchievementProgress, 0, sizeof(m_AchievementProgress));
    Assert(IsDirty() == false);
    m_Dirty = false;
}

const char* AchievementsFile::GetFilename()
{
	return "achievements.dat";
}

bool AchievementsFile::Open()
{
    bool success = false;

    rtry
    {
        Clear();
        Close();

        char path[RGSC_MAX_PATH] = {0};
        rverify(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfileTitleDirectory(path, true), catchall, );
        safecat(path, GetFilename());
            
        const fiDevice *device = fiDevice::GetDevice(path);
        fiHandle hFile = device->Open(path, false);
        bool isFileValid = false;

        if(fiIsValidHandle(hFile))
        {
            // file exists, read it in
            int size = device->Seek(hFile, 0, seekEnd);
            if(rlVerifyf(IsPayloadSizeValid(size), "Achievement file with size %u was invalid", size))
            {
                m_Device = device;
                m_FileHandle = hFile;

                if(Read())
                {
                    isFileValid = true;
                    m_Device = device;
                    m_FileHandle = hFile;
                }
            }

            if(isFileValid == false)
            {
                device->Close(hFile);
                device->Delete(path);
            }
        }
            
        if(isFileValid == false)
        {
            // file does not exist, create it
            // reserve space for the max file size here, initialized to zero
            // TODO: NS - check for disk space?
            // TODO: NS - if the file fails to be created, everything should still work. Currently WriteSingleAchievementTask doesn't work if the file is invalid/not there.
            u8 buf[MAX_BYTE_SIZEOF_PAYLOAD] = {0};
            unsigned sizeOfBuf = sizeof(buf);
            hFile = device->Create(path);
            rverify(fiIsValidHandle(hFile), catchall, );

			u32 bytesToWrite = GetExpectedPayloadSize(m_ExportVersion);
			rverify(bytesToWrite <= sizeOfBuf, catchall, );

            int bytesWritten = device->Write(hFile, buf, bytesToWrite);
            rverify(bytesWritten == (int)bytesToWrite, catchall, );

            m_Device = device;
            m_FileHandle = hFile;

            m_Dirty = true;
            rverify(Write(), catchall, );
        }

        success = true;
    }
    rcatchall
    {
        Clear();
        Close();
    }

    return success;
}

void AchievementsFile::Close()
{
	Assert(IsDirty() == false);

	if(m_Device)
	{
		m_Device->Close(m_FileHandle);
		m_Device = NULL;
	}

	m_FileHandle = fiHandleInvalid;
}

#if __DEV
bool AchievementsFile::Delete()
{
	Close();

	bool success = false;

	rtry
	{
		char path[RGSC_MAX_PATH] = {0};
		rverify(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfileTitleDirectory(path, true), catchall, );
		safecat(path, GetFilename());

		const fiDevice *device = fiDevice::GetDevice(path);
		rverify(device, catchall, );

		device->Delete(path);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}
#endif

bool AchievementsFile::IsDirty() 
{
	return m_Dirty;
}

bool AchievementsFile::IsOpen()
{
	return m_FileHandle != fiHandleInvalid;
}

const AchievementBitSet& AchievementsFile::GetAchievementBitSet()
{
	return m_AwardedAchievements;
}

const AchievementBitSet& AchievementsFile::GetAwardedOfflineBitSet()
{
	return m_AwardedOffline;
}

const s64 AchievementsFile::GetProgress(const AchievementId achievementId) const
{
	if (rlVerify(achievementId < RgscAchievementManager::MAX_NUM_ACHIEVEMENTS))
	{
		return m_AchievementProgress[achievementId];
	}
	
	return 0;
}

bool AchievementsFile::WriteProgress(const AchievementId achievementId, const s64 progress)
{
	bool success = false;

	rtry
	{
		rverify(achievementId < RgscAchievementManager::MAX_NUM_ACHIEVEMENTS, catchall, );
		m_AchievementProgress[achievementId] = progress;

		m_Dirty = true;

		rverify(Write(), catchall, );

		success = true;        
	}
	rcatchall
	{
	}

	return success;
}

u32 AchievementsFile::GetMinVersionForProgress()
{
	return MIN_VERSION_FOR_PROGRESS;
}

u32 AchievementsFile::GetExpectedPayloadSize(u32 version)
{
	if (version == VERSION_1)
	{
		return MAX_BYTE_SIZEOF_PAYLOAD_V1;
	}
	else if (version == VERSION_2)
	{
		return MAX_BYTE_SIZEOF_PAYLOAD_V2;
	}

	return MAX_BYTE_SIZEOF_PAYLOAD;
}

u32 AchievementsFile::IsPayloadSizeValid(u32 size)
{
	if (size == MAX_BYTE_SIZEOF_PAYLOAD_V1)
		return true;

	if (size == MAX_BYTE_SIZEOF_PAYLOAD_V2)
		return true;

	return false;
}

bool AchievementsFile::Import(const void* buf, const unsigned sizeOfBuf, unsigned* size, unsigned* importVersion)
{
    bool success = false;

	datImportBuffer bb;
	bb.SetReadOnlyBytes(buf, sizeOfBuf);

    Clear();

    unsigned consumed = 0;
	unsigned version = MIN_VERSION;

    rtry
    {
        rverify(buf, catchall, );

        // header
        unsigned importedSize = 0;
        rverify(m_Header.Import(buf, sizeOfBuf, &importedSize), catchall, );
        rverify(importedSize == Header::MAX_BYTE_SIZEOF_PAYLOAD, catchall, );
        consumed += importedSize;
		bb.SetNumBitsRead(consumed << 3);
		version = m_Header.GetVersion();

		// verify that the buffer is large enough for the payload 
		rverify(sizeOfBuf >= GetExpectedPayloadSize(version) - datProtectSignatureSize, catchall, );

		// ROS title name
		char rosTitleName[32] = {0};
		rverify(bb.ReadBytes(rosTitleName, sizeof(rosTitleName)), catchall, );
		consumed += sizeof(rosTitleName);
		Assert((bb.GetCursorPos() % 8) == 0);
		Assert((unsigned)bb.GetCursorPos() == (consumed << 3));
		Assert((unsigned)bb.GetNumBitsRead() == (consumed << 3));

		// read in the datProtect signature for the inner block
		const unsigned int innerBlockStart = bb.GetNumBitsRead();
		Assert(innerBlockStart == (consumed << 3));

		const unsigned int innerBlockSize = (sizeOfBuf << 3) - (datProtectSignatureSize << 3) - innerBlockStart;

		u8 signature[datProtectSignatureSize];
		rverify(bb.SetCursorPos((sizeOfBuf - sizeof(signature)) << 3), catchall, );
		rverify(bb.ReadBytes(signature, sizeof(signature)), catchall, );

		// decrypt and verify authenticity
		datProtectContext context(true, (u8*)rosTitleName, (u32)strlen(rosTitleName));
		bool valid = datUnprotect(&((u8*)buf)[innerBlockStart >> 3], innerBlockSize >> 3, context, signature);
		rverify(valid, catchall, rlError("achievement data is corrupt or has been modified by an untrusted source."));

		bb.SetCursorPos(innerBlockStart);

		{
			// awarded bits
			importedSize = 0;
			rverify(m_AwardedAchievements.Import((void*)((u8*)buf + consumed), sizeOfBuf - consumed, &importedSize), catchall, );
			rverify(importedSize == AchievementBitSet::MAX_BYTE_SIZEOF_PAYLOAD, catchall, );
			consumed += importedSize;

			// awarded offline bits
			importedSize = 0;
			rverify(m_AwardedOffline.Import((void*)((u8*)buf + consumed), sizeOfBuf - consumed, &importedSize), catchall, );
			rverify(importedSize == AchievementBitSet::MAX_BYTE_SIZEOF_PAYLOAD, catchall, );
			consumed += importedSize;

			// timestamps
			sysMemCpy(m_Timestamps, (void*)((u8*)buf + consumed), sizeof(m_Timestamps));
			importedSize = sizeof(m_Timestamps);
			consumed += importedSize;

			// progress
			if (version >= GetMinVersionForProgress())
			{
				sysMemCpy(m_AchievementProgress, (void*)((u8*)buf + consumed), sizeof(m_AchievementProgress));
				importedSize = sizeof(m_AchievementProgress);
				consumed += importedSize;
			}
		}

		consumed += sizeof(signature);

		rverify(consumed == GetExpectedPayloadSize(version) - datProtectSignatureSize, catchall, );

		// copy the version to the outvalue
		*importVersion = version;

        success = true;        
    }
    rcatchall
    {
    }

    if(size){*size = success ? consumed : 0;}

    return success;
}

bool AchievementsFile::Read()
{
    bool success = false;

    rtry
    {
        rverify(m_Device && fiIsValidHandle(m_FileHandle), catchall, );

        ASSERT_ONLY(int position = )m_Device->Seek(m_FileHandle, 0, seekSet);
        Assert(position == 0);

        // read
        u8 buf[MAX_BYTE_SIZEOF_PAYLOAD] = {0};		
        unsigned sizeOfBuf = sizeof(buf);
        int bytesRead = m_Device->Read(m_FileHandle, buf, sizeOfBuf);
        rverify(IsPayloadSizeValid(bytesRead), catchall, );

        // decrypt and verify integrity and authenticity
        u8 signature[datProtectSignatureSize];
        bytesRead -= sizeof(signature);
        memcpy(signature, buf + bytesRead, sizeof(signature));

		const UniqueKey& key = GetRgscConcreteInstance()->_GetProfileManager()->GetSignedInProfile().GetUniqueKey();
		rverify(key.IsSet(), catchall, );
		datProtectContext context(true, key.GetKey(), UniqueKey::SIZE_OF_KEY_IN_BYTES);
        bool valid = datUnprotect(buf, bytesRead, context, signature);
        rverify(valid, catchall, rlError("achievements file is corrupt or has been modified by an untrusted source."));

        // import
        unsigned importedSize = 0;
		unsigned importVersion = MIN_VERSION;
        rverify(Import(buf, bytesRead, &importedSize, &importVersion), catchall, );
        rverify(importedSize == GetExpectedPayloadSize(importVersion) - sizeof(signature), catchall, );

        success = true;
    }
    rcatchall
    {

    }

    return success;
}

bool AchievementsFile::Export(void* buf,
                const unsigned sizeOfBuf,
                unsigned* size) const
{
    bool success = false;
    unsigned consumed = 0;

	datExportBuffer bb;
	bb.SetReadWriteBytes(buf, sizeOfBuf);

    rtry
    {
        rverify(buf, catchall, );
        rverify(sizeOfBuf >= MAX_BYTE_SIZEOF_PAYLOAD - datProtectSignatureSize, catchall, );
        sysMemSet(buf, 0, sizeOfBuf);

        // header
        unsigned exportedSize = 0;
        rverify(m_Header.Export(m_ExportVersion, buf, sizeOfBuf, &exportedSize), catchall, );
        rverify(exportedSize == Header::MAX_BYTE_SIZEOF_PAYLOAD, catchall, );
        consumed += exportedSize;
		bb.SetNumBitsWritten(consumed << 3);

		// ROS title name
		// want to always write out the full 32 bytes to get a predictable file size
		const char* rosTitleName = GetRgscConcreteInstance()->GetTitleId()->GetRosTitleName();
		const unsigned int rosTitleNameLen = (unsigned int)strlen(rosTitleName);
		rverify(rosTitleName && (rosTitleNameLen > 0) && (rosTitleNameLen < 32), catchall, );
		char rosTitleNameCopy[32] = {0};
		safecpy(rosTitleNameCopy, rosTitleName);
		rverify(bb.WriteBytes(rosTitleNameCopy, sizeof(rosTitleNameCopy)), catchall, );
		consumed += sizeof(rosTitleNameCopy);
		Assert((bb.GetCursorPos() % 8) == 0);
		Assert((unsigned)bb.GetCursorPos() == (consumed << 3));

		const unsigned int innerBlockStart = bb.GetBitLength();
		Assert(innerBlockStart == (consumed << 3));

		{
			// awarded bits
			exportedSize = 0;
			rverify(m_AwardedAchievements.Export((void*)((u8*)buf + consumed), sizeOfBuf - consumed, &exportedSize), catchall, );
			rverify(exportedSize == AchievementBitSet::MAX_BYTE_SIZEOF_PAYLOAD, catchall, );
			consumed += exportedSize;

			// awarded offline bits
			exportedSize = 0;
			rverify(m_AwardedOffline.Export((void*)((u8*)buf + consumed), sizeOfBuf - consumed, &exportedSize), catchall, );
			rverify(exportedSize == AchievementBitSet::MAX_BYTE_SIZEOF_PAYLOAD, catchall, );
			consumed += exportedSize;

			// timestamps
			sysMemCpy((void*)((u8*)buf + consumed), m_Timestamps, sizeof(m_Timestamps));
			exportedSize = sizeof(m_Timestamps);
			consumed += exportedSize;

			// progress
			if (m_ExportVersion >= GetMinVersionForProgress())
			{
				sysMemCpy((void*)((u8*)buf + consumed), m_AchievementProgress, sizeof(m_AchievementProgress));
				exportedSize = sizeof(m_AchievementProgress);
				consumed += exportedSize;
			}
		}

		bb.SetNumBitsWritten(consumed << 3);
		Assert((unsigned)bb.GetBitLength() == (consumed << 3));

		const unsigned int innerBlockSize = (unsigned)bb.GetBitLength() - innerBlockStart;
		Assert((innerBlockSize % 8) == 0);

		// lock the inner block to the ROS title name
		u8 checksum[datProtectSignatureSize];
		datProtectContext context(true, (u8*)rosTitleName, rosTitleNameLen);
		datProtect(&((u8*)buf)[innerBlockStart >> 3], innerBlockSize >> 3, context, checksum);
		rverify(bb.WriteBytes(checksum, sizeof(checksum)), catchall, );
		consumed += sizeof(checksum);

		rverify(((unsigned)bb.GetNumBitsWritten() == (consumed << 3)), catchall, );
		rverify((bb.GetNumBitsWritten() % 8) == 0, catchall, );

        rverify(consumed == GetExpectedPayloadSize(m_ExportVersion) - datProtectSignatureSize, catchall, );

        success = true;        
    }
    rcatchall
    {
    }

    if(size){*size = success ? consumed : 0;}

    return success;
}

bool AchievementsFile::Write()
{
    bool success = false;

    Assert(IsDirty());

    rtry
    {
        rverify(m_Device && fiIsValidHandle(m_FileHandle), catchall, );

        ASSERT_ONLY(int position = )m_Device->Seek(m_FileHandle, 0, seekSet);
        Assert(position == 0);

        // export
        u8 buf[MAX_BYTE_SIZEOF_PAYLOAD] = {0};

        unsigned sizeOfBuf = sizeof(buf) - datProtectSignatureSize;
        unsigned exportedSize = 0;
        rverify(Export(buf, sizeOfBuf, &exportedSize), catchall, );
        rverify(exportedSize == (GetExpectedPayloadSize(m_ExportVersion) - datProtectSignatureSize), catchall, );

        // sign and encrypt using datProtect, locking the data to the profile's unique key
        u8 signature[datProtectSignatureSize];
		const UniqueKey& key = GetRgscConcreteInstance()->_GetProfileManager()->GetSignedInProfile().GetUniqueKey();
		rverify(key.IsSet(), catchall, );
		datProtectContext context(true, key.GetKey(), UniqueKey::SIZE_OF_KEY_IN_BYTES);
        datProtect(buf, exportedSize, context, signature);
        memcpy(buf + exportedSize, signature, sizeof(signature));
        exportedSize += sizeof(signature);
        Assert(exportedSize == GetExpectedPayloadSize(m_ExportVersion));

        // write
        int bytesWritten = m_Device->Write(m_FileHandle, buf, exportedSize);
        rverify(bytesWritten == (int)exportedSize, catchall, );

		rlDebug3("Wrote achievements file (size: %d bytes)", bytesWritten);

        m_Dirty = false;

        success = true;
    }
    rcatchall
    {

    }

    return success;
}

bool AchievementsFile::WriteAchievement(const AchievementId achievementId)
{
	bool success = false;

	rtry
	{
		rverify(m_AwardedAchievements.Set(achievementId), catchall, );

		if(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal() == false)
		{
			rverify(m_AwardedOffline.Set(achievementId), catchall, );
		}

		m_Dirty = true;

		rverify(Write(), catchall, );

		success = true;        
	}
	rcatchall
	{
	}

	return success;
}

bool AchievementsFile::MergeAchievementBitSets(const AchievementBitSet &toMerge, bool &changed)
{
	bool success = false;

	changed = false;

	rtry
	{
		int oldNumBits = m_AwardedAchievements.CountOnBits();
		m_AwardedAchievements.Union(toMerge);
		int newNumBits = m_AwardedAchievements.CountOnBits();
		rverify(newNumBits >= oldNumBits, catchall, );

		if(newNumBits > oldNumBits)
		{
			changed = true;
			m_Dirty = true;
			rverify(Write(), catchall, );
		}

		success = true;        
	}
	rcatchall
	{
	}

	return success;
}

bool AchievementsFile::MergeAwardedOfflineBitSets(const AchievementBitSet &toMerge, bool &changed)
{
    bool success = false;

    changed = false;

    rtry
    {
        int oldNumBits = m_AwardedOffline.CountOnBits();

        // m_AwardedOffline |= toMerge
        m_AwardedOffline.Union(toMerge);

        // for safety, make sure there are no awarded offline bits set
        // that aren't also set in the awarded achievements bit set
        // (m_AwardedOffline is always a subset of m_AwardedAchievements)

        // m_AwardedOffline &= m_AwardedAchievements
        m_AwardedOffline.Intersect(m_AwardedAchievements);

        int newNumBits = m_AwardedOffline.CountOnBits();
        rverify(newNumBits >= oldNumBits, catchall, );

        if(newNumBits > oldNumBits)
        {
            changed = true;
            m_Dirty = true;
            rverify(Write(), catchall, );
        }

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

bool AchievementsFile::SetAchievementTimestamp(const AchievementId achievementId, time_t timestamp)
{
	bool success = false;

	if(AssertVerify((m_AwardedAchievements.IsSet(achievementId))))
	{
		if(AssertVerify(achievementId < COUNTOF(m_Timestamps)))
		{
			if(m_Timestamps[achievementId] != timestamp)
			{
				m_Timestamps[achievementId] = timestamp;
				m_Dirty = true;
			}
			success = true;
		}
	}

	return success;
}

time_t AchievementsFile::GetAchievementTimestamp(const AchievementId achievementId)
{
    time_t timestamp = 0;

    if(AssertVerify(achievementId < COUNTOF(m_Timestamps)))
    {
        timestamp = m_Timestamps[achievementId];
    }

    return timestamp;	
}

////////////////////////////////////////////////////////////////////////////////
// AchievementMetadataRecord
////////////////////////////////////////////////////////////////////////////////

class AchievementMetadataRecord
{
    u8 m_Points;
    u8 m_Type;
    u8 m_ShowUnachieved;
	s64 m_MaxProgress;

	bool m_bAchievementProgress;

public:

    AchievementMetadataRecord()
    {
        Clear();
    }

    ~AchievementMetadataRecord()
    {
        Clear();
    }

    void Clear()
    {
        m_Points = 0;
        m_Type = 0;
        m_ShowUnachieved = 0;
		m_MaxProgress = 0;
		m_bAchievementProgress = false;
    }

    const u8 GetPoints() {return m_Points;}
    const u8 GetType() {return m_Type;}
    const bool ShowUnachieved() {return m_ShowUnachieved != 0;}
	const s64 GetMaxProgress() { return m_MaxProgress; }
	void EnableReadAchievementProgress(bool enable) { m_bAchievementProgress = enable; }

    bool Import(const void* buf,
                const unsigned sizeOfBuf,
                unsigned* size = 0)
    {
        datImportBuffer bb;
        bb.SetReadOnlyBytes(buf, sizeOfBuf);

        bool success = false;

        Clear();

        rtry
        {
            rverify(buf, catchall, );

            u8 flags = 0;
            rverify(bb.ReadUns(m_Points, sizeof(u8) << 3), catchall, );
            rverify(bb.ReadUns(flags, sizeof(u8) << 3), catchall, );

			if (m_bAchievementProgress)
			{
				s64 hi, lo;
				rverify(bb.ReadInt(hi, sizeof(int) << 3), catchall, );
				rverify(bb.ReadInt(lo, sizeof(int) << 3), catchall, );
				m_MaxProgress = (hi<<32) | lo;
			}

            m_ShowUnachieved = flags & 0x01;
            m_Type = (flags & 0x0E) >> 1;
            Assert((m_Type > 0) && (m_Type <= 7));

            success = true;        
        }
        rcatchall
        {
        }

        if(size){*size = success ? sizeOfBuf : 0;}

        return success;
    }
};

////////////////////////////////////////////////////////////////////////////////
// StringRecord
////////////////////////////////////////////////////////////////////////////////

class StringRecord
{
    char* m_String;

public:

    StringRecord()
    : m_String(NULL)
    {
        Clear();
    }

    ~StringRecord()
    {
        Clear();
    }

    void Clear()
    {
        if(m_String)
        {
            RGSC_FREE(m_String);
            m_String = NULL;
        }
    }

    const char* GetString() {return m_String;}

    bool Import(const void* buf,
                const unsigned sizeOfBuf,
                unsigned* size = 0)
    {
        bool success = false;

        Clear();

        rtry
        {
            rverify(buf, catchall, );

            m_String = (char*)RGSC_ALLOCATE(RgscMetadata, sizeOfBuf + 1);
            rverify(m_String, catchall, );

            memcpy(m_String, buf, sizeOfBuf);
            m_String[sizeOfBuf] = '\0';

            success = true;        
        }
        rcatchall
        {
        }

        if(size){*size = success ? sizeOfBuf : 0;}

        return success;
    }
};

////////////////////////////////////////////////////////////////////////////////
// ImageRecord
////////////////////////////////////////////////////////////////////////////////

RgscAchievementManager::ImageRecord::ImageRecord()
: m_Data(NULL)
{
    Clear();
}

RgscAchievementManager::ImageRecord::~ImageRecord()
{
    Clear();
}

void RgscAchievementManager::ImageRecord::Clear()
{
    m_Size = 0;
    if(m_Data)
    {
        RGSC_FREE(m_Data);
        m_Data = NULL;
    }
}

bool RgscAchievementManager::ImageRecord::Import(const void* buf,
										const unsigned sizeOfBuf,
										unsigned* size/* = 0*/)
{
    bool success = false;

    Clear();

    rtry
    {
        rverify(buf, catchall, );

        m_Data = (u8*)RGSC_ALLOCATE(RgscMetadata, sizeOfBuf);
        rverify(m_Data, catchall, );
        m_Size = sizeOfBuf;

        memcpy(m_Data, buf, sizeOfBuf);

        success = true;        
    }
    rcatchall
    {
    }

    if(size){*size = success ? sizeOfBuf : 0;}

    return success;
}

// ===========================================================================
// IAchievementManager Public Interface
// ===========================================================================

RGSC_HRESULT RgscAchievementManager::QueryInterface(RGSC_REFIID riid, LPVOID* ppvObj)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObj == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IAchievementManager*>(this);
	}
	else if(riid == IID_IAchievementManagerV1)
	{
		pUnknown = static_cast<IAchievementManagerV1*>(this);
	}
	else if(riid == IID_IAchievementManagerV2)
	{
		pUnknown = static_cast<IAchievementManagerV2*>(this);
	}
	else if(riid == IID_IAchievementManagerV3)
	{
		pUnknown = static_cast<IAchievementManagerV3*>(this);
	}

	*ppvObj = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

class AchievementEnumerationHandle
{
public:
	RGSC_IID m_RRGSC_IID;
	RockstarId m_ProfileId;
	IAchievement::DetailFlags m_DetailFlags;
	AchievementId m_FirstAchievementId;
	u32 m_MaxAchievementsToRead;
	u32 m_NumBytesRequired;
	bool m_IsCreated;
	bool m_EnumerationStarted;
};

// could support more than one enumeration at a time if required
static const u32 MAX_ENUMERATIONS = 1;
static u32 m_Handles[MAX_ENUMERATIONS] = {0};
static AchievementEnumerationHandle sm_AchievementEnumerationHandle[MAX_ENUMERATIONS];

RGSC_HRESULT 
RgscAchievementManager::CreateAchievementEnumerator(RGSC_REFIID riid,
											 const ITitleId* UNUSED_PARAM(titleId),
											 const RockstarId profileId,
											 const IAchievement::DetailFlags detailFlags,
											 const AchievementId firstAchievementId,
											 const u32 maxAchievementsToRead,
											 u32* numBytesRequired,
											 void** handle)
{
	if(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedIn() == false)
	{
		return HRESULT_FROM_WIN32(ERROR_NOT_LOGGED_ON);
	}

	if(!AssertVerify(numBytesRequired != NULL))
	{
		return RGSC_INVALIDARG;
	}

	*numBytesRequired = 0;

	if(!AssertVerify(firstAchievementId <= IAchievementManager::MAX_NUM_ACHIEVEMENTS))
	{
		return RGSC_INVALIDARG;
	}

	if(!AssertVerify(maxAchievementsToRead <= IAchievementManager::MAX_NUM_ACHIEVEMENTS))
	{
		return RGSC_INVALIDARG;
	}

	if(!AssertVerify(handle != NULL))
	{
		return RGSC_INVALIDARG;
	}

	u32 bytesRequired = 0;

	if(riid == IID_IAchievementListV1)
	{
		bytesRequired = AchievementListV1::GetMemoryRequired(detailFlags, maxAchievementsToRead);

		// TODO: NS - find a free handle if more than 1
		CompileTimeAssert(MAX_ENUMERATIONS == 1);
		u32 handleIndex = 0;
		*handle = &m_Handles[handleIndex];

		AchievementEnumerationHandle* details = &sm_AchievementEnumerationHandle[handleIndex];

		details->m_RRGSC_IID = riid;
		details->m_ProfileId = profileId;
		details->m_DetailFlags = detailFlags;
		details->m_FirstAchievementId = firstAchievementId;
		details->m_MaxAchievementsToRead = maxAchievementsToRead;
		details->m_NumBytesRequired = bytesRequired;
		details->m_IsCreated = true;
		details->m_EnumerationStarted = false;

		*numBytesRequired = bytesRequired;

		return RGSC_OK;
	}

	return RGSC_NOINTERFACE;
}

RGSC_HRESULT 
RgscAchievementManager::EnumerateAchievements(void* handle,
									   void* buffer,
									   const u32 bufferSize,
									   IAchievementList** achievements)
{
	if(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedIn() == false)
	{
		return ERROR_NOT_LOGGED_ON;
	}

	if(!AssertVerify(handle != NULL))
	{
		return RGSC_INVALIDARG;
	}

	u32 handleIndex = *((u32*)handle);
	if(!AssertVerify(handleIndex < MAX_ENUMERATIONS))
	{
		return RGSC_INVALIDARG;
	}

	AchievementEnumerationHandle* details = &sm_AchievementEnumerationHandle[handleIndex];

	if(!AssertVerify(buffer != NULL))
	{
		return RGSC_INVALIDARG;
	}

	if(!AssertVerify(bufferSize >= details->m_NumBytesRequired))
	{
		return RGSC_INVALIDARG;
	}

	if(!AssertVerify(achievements != NULL))
	{
		return RGSC_INVALIDARG;
	}

	if(!AssertVerify(details->m_IsCreated == true))
	{
		return RGSC_INVALIDARG;
	}

	details->m_EnumerationStarted = true;

	AchievementListV1* list = new (buffer) AchievementListV1(details->m_DetailFlags, details->m_MaxAchievementsToRead, (u8*)buffer);

	RGSC_HRESULT hr = ReadAchievements(details->m_ProfileId,
					 details->m_DetailFlags,
					 details->m_FirstAchievementId,
					 details->m_MaxAchievementsToRead,
					 list);

	*achievements = (AchievementListV1*)buffer;

	details->m_IsCreated = false;

	return hr;
}

RGSC_HRESULT 
RgscAchievementManager::EnumerateAchievements(void* handle,
									   void* buffer,
									   const u32 bufferSize,
									   IAchievementList** achievements,
									   IAsyncStatus* status)
{
	if(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedIn() == false)
	{
		return ERROR_NOT_LOGGED_ON;
	}

	if(!AssertVerify(handle != NULL))
	{
		return RGSC_INVALIDARG;
	}

	u32 handleIndex = *((u32*)handle);
	if(!AssertVerify(handleIndex < MAX_ENUMERATIONS))
	{
		return RGSC_INVALIDARG;
	}

	AchievementEnumerationHandle* details = &sm_AchievementEnumerationHandle[handleIndex];

	if(!AssertVerify(buffer != NULL))
	{
		return RGSC_INVALIDARG;
	}

	if(!AssertVerify(bufferSize >= details->m_NumBytesRequired))
	{
		return RGSC_INVALIDARG;
	}

	if(!AssertVerify(achievements != NULL))
	{
		return RGSC_INVALIDARG;
	}

	if(!AssertVerify(details->m_IsCreated == true))
	{
		return RGSC_INVALIDARG;
	}

	details->m_EnumerationStarted = true;

	AchievementListV1* list = new (buffer) AchievementListV1(details->m_DetailFlags, details->m_MaxAchievementsToRead, (u8*)buffer);

	// TODO: NS - make async
	ReadAchievements(details->m_ProfileId,
		details->m_DetailFlags,
		details->m_FirstAchievementId,
		details->m_MaxAchievementsToRead,
		list);

	*achievements = (AchievementListV1*)buffer;

	details->m_IsCreated = false;

	return RGSC_OK;
}

RGSC_HRESULT 
RgscAchievementManager::ReadAchievements(const RockstarId profileId,
								  const IAchievement::DetailFlags detailFlags,
								  const AchievementId firstAchievementId,
								  const u32 maxAchievementsToRead,
								  IAchievementList* achievements)
{
    SYS_CS_SYNC(m_Cs);

	RGSC_HRESULT hr = RGSC_FAIL;

    rtry
    {
		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

		// determine which version of the achievement list interface we are working with
		AchievementListV1* listV1 = NULL;
		hr = achievements->QueryInterface(IID_IAchievementListV1, (void**) &listV1);
		rverify(SUCCEEDED(hr) && (listV1 != NULL), catchall, );

        rverify(ReadMetadata(profileId,
                             detailFlags,
                             firstAchievementId,
							 maxAchievementsToRead,
                             achievements), catchall, );

        if((listV1->GetNumAchievements() > 0) &&
           ((detailFlags & IAchievement::ACHIEVEMENT_DETAILS_IS_AWARDED) ||
			(detailFlags & IAchievement::ACHIEVEMENT_DETAILS_TIMESTAMP)))
        {
            netStatus status;
            rverify(ReadAchievements(profileId,
                                     detailFlags,
                                     achievements,
                                     &status), catchall, );

            // need to loop here until task completes since we're
            // running this on a worker thread from IAchievement
            while(status.Pending())
            {
                Sleep(30);	// use WaitSingleObject instead
            }

			hr = status.Succeeded() ? RGSC_OK : RGSC_FAIL;
        }
        else
        {
            hr = RGSC_OK;
        }

		rverify(hr == RGSC_OK, catchall, );

		SortAchievements(achievements);
	}
    rcatchall
    {

    }

    return hr;
}

RGSC_HRESULT 
RgscAchievementManager::WriteAchievement(const AchievementId achievementId)
{
    SYS_CS_SYNC(m_Cs);

	// NOTE: this function must be called from another thread

    RGSC_HRESULT hr = RGSC_FAIL;

	u8 stackBuf[1024];
	void* buf = (void*)stackBuf;

    rtry
    {
		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

		u32 size = AchievementList::GetMemoryRequired(IAchievement::ACHIEVEMENT_DETAILS_ALL, 1);

		// attempt to allocate the read buffer on the stack if we can
		if(size > sizeof(stackBuf))
		{
			// buf size is too large, allocate on the heap instead
			buf = RGSC_ALLOCATE(RgscAchievementManager, size);
		}

		rverify(buf, catchall, );

        // read in the achievement's metadata so we can send the achievement awarded notification
        AchievementList achievements(IAchievement::ACHIEVEMENT_DETAILS_ALL, 1, (u8*)buf);
        rverify(ReadMetadata(GetRgscConcreteInstance()->_GetProfileManager()->GetSignedInProfile().GetProfileId(),
                             IAchievement::ACHIEVEMENT_DETAILS_ALL,
                             achievementId,
							 1,
                             &achievements),
                             catchall, );

        rverify(achievements.GetNumAchievements() == 1, catchall, );
		rverify(achievements.GetAchievement(0)->GetId() == achievementId, catchall, );
        
		// Wait for any achievement IO to finish. Good thing we're on the game's achievement thread....
		while (sm_FileIoInProgress)
		{
			Sleep(30);	// use WaitSingleObject instead
		}

        // write the achievement to the local file
		// even if writing to the file fails, try to write online
        AchievementsFile file;
		file.SetExportVersion(GetVersion());

		if(AssertVerify(file.Open()))	
		{
			const AchievementBitSet& fileSet = file.GetAchievementBitSet();
			bool isAchieved = fileSet.IsSet(achievementId);
			if(isAchieved == false)
			{
				AssertVerify(file.WriteAchievement(achievementId));
			}
			file.Close();
			rcheck(isAchieved == false || PARAM_redoAchievements.Get(), catchall, rlWarning("Not awarding achievement %d because it has already been awarded.", achievementId));
		}

		// queue a notification to be sent on the main thread
		{
			netStatus status;
			if(AssertVerify(RaiseAchievementAwardedEvent(achievements.GetAchievement(0), &status)))
			{
				// need to loop here until task completes since we're
				// running this on a worker thread from IAchievement
				while(status.Pending())
				{
					Sleep(30);	// use WaitSingleObject instead
				}

				hr = status.Succeeded() ? RGSC_OK : RGSC_FAIL;
			}
		}

		// if the notification task failed, we still want to write the achievement
		hr = RGSC_OK;

        // write the achievement to the online database

        if(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal())
        {
			Displayf("Calling WriteSingleAchievement()");
            netStatus status;
            rverify(WriteSingleAchievement(achievementId, &status), catchall, );

            // need to loop here until task completes since we're
            // running this on a worker thread from IAchievement
            while(status.Pending())
            {
                Sleep(30);	// use WaitSingleObject instead
            }

			Displayf("Finished WriteSingleAchievement()");

			hr = status.Succeeded() ? RGSC_OK : RGSC_FAIL;
        }
    }
    rcatchall
    {
    }

	if(buf != stackBuf)
	{
		RGSC_FREE(buf);
		buf = NULL;
	}

	Displayf("Exiting rlPcAchievement::WriteAchievement()");

    return hr;
}


RGSC_HRESULT
RgscAchievementManager::SetAchievementProgress(const AchievementId achievementId, const s64 progress)
{
	SYS_CS_SYNC(m_Cs);

	// NOTE: this function must be called from another thread

	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );
		rverify(achievementId < MAX_NUM_ACHIEVEMENTS, catchall, );

		s64 currentProgress = m_AchievementProgress[achievementId];
		rcheck(progress > currentProgress, catchall, );

		m_AchievementProgress[achievementId] = progress;

		// write the achievement to the local file - even if writing to the file fails, try to write online
		AchievementsFile file;
		file.SetExportVersion(GetVersion());

		if(AssertVerify(file.Open()))
		{
			s64 savedProgress = file.GetProgress(achievementId);
			if (savedProgress >= progress)
			{
				rcheck(PARAM_redoAchievements.Get(), catchall, rlWarning("Not setting progress for %u because progress has not improved.", achievementId));
			}

			file.WriteProgress(achievementId, progress);
			file.Close();
		}
		
		bool bAlreadyQueued = false;
		// Critical Section Progress Accessing
		{
			SYS_CS_SYNC(m_ProgressCs);
			for (int i = 0; i < m_ProgressQueue.GetCount(); i++)
			{
				AchievementProgress& queuedProgress = m_ProgressQueue[i];
				if (queuedProgress.m_Id == achievementId)
				{
					queuedProgress.m_Progress = progress;
					bAlreadyQueued = true;
					rlDebug3("Refreshed queued achievement progress write for ID \"%u\", Progress: \"%"I64FMT"d\"", achievementId, progress);
					break;
				}
			}
		}
		
		// If its already queued, bail out successfully.
		rcheck(bAlreadyQueued == false, catchall, hr = RGSC_OK);

		AchievementProgress& queuedProgress = m_ProgressQueue.Append();
		queuedProgress.m_Id = achievementId;
		queuedProgress.m_Progress = progress;

		rlDebug3("Queued achievement progress write for ID \"%u\", Progress: \"%"I64FMT"d\"", achievementId, progress);

		hr = RGSC_OK;
	}
	rcatchall
	{
	}

	return hr;
}

#if __DEV
RGSC_HRESULT 
RgscAchievementManager::DeleteAllAchievements()
{
	SYS_CS_SYNC(m_Cs);

	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedIn(), catchall, )

		AchievementsFile file;
		file.SetExportVersion(GetVersion());

		rverify(file.Delete(), catchall, );

		hr = RGSC_OK;

		// delete the achievements from the online database
		if(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal())
		{
			netStatus status;
			rverify(DeleteAllAchievements(&status), catchall, );

			// need to loop here until task completes since we're
			// running this on a worker thread from IAchievement
			while(status.Pending())
			{
				Sleep(30);	// use WaitSingleObject instead
			}

			hr = status.Succeeded() ? RGSC_OK : RGSC_FAIL;
		}
	}
	rcatchall
	{
	}

	return hr;
}
#endif

////////////////////////////////////////////////////////////////////////////////
// RgscAchievementManager
////////////////////////////////////////////////////////////////////////////////

RgscAchievementManager::RgscAchievementManager()
: m_Metadata(NULL)
, m_MetadataTable(NULL)
, m_StringsTable(NULL)
, m_ImagesTable(NULL)
{
    Clear();
}

RgscAchievementManager::~RgscAchievementManager()
{
    Shutdown();
}

void
RgscAchievementManager::Clear()
{
    if(m_MetadataTable)
    {
        RGSC_DELETE(m_MetadataTable);
        m_MetadataTable = NULL;
    }

    if(m_StringsTable)
    {
        RGSC_DELETE(m_StringsTable);
        m_StringsTable = NULL;
    }

    if(m_ImagesTable)
    {
        RGSC_DELETE(m_ImagesTable);
        m_ImagesTable = NULL;
    }

    m_Metadata = NULL;

	m_ProgressQueue.Reset();
	sysMemSet(m_AchievementProgress, 0, sizeof(m_AchievementProgress));

    SignOut();
}

bool
RgscAchievementManager::Init()
{
	if(!GetRgscConcreteInstance()->IsMetadataSupported())
	{
		// No metadata -> no achievements
		return true;
	}

	bool success = false;

    rtry
    {
        Clear();

        m_Metadata = GetRgscConcreteInstance()->_GetLocalMetadata();
        rverify(m_Metadata, catchall, );

		//@@: location RLPCACHIEVEMENT_INIT_LOAD_METADATA
        m_MetadataTable = m_Metadata->GetTable("achievement_metadata");
        rverify(m_MetadataTable, catchall, );

        m_ImagesTable = m_Metadata->GetTable("achievement_images");
        rverify(m_ImagesTable, catchall, );

		sysLanguage lang = rlGetLanguage();
		if(lang == LANGUAGE_MEXICAN)
		{
			// we don't have achievement strings for Mexican Spanish. Fallback to Spanish.
			lang = LANGUAGE_SPANISH;
		}

        char tableName[50];
        formatf(tableName, "achievement_strings_%s", rlGetLanguageCode(lang));
        m_StringsTable = m_Metadata->GetTable(tableName);
		Assertf(m_StringsTable, "There are no Achievement strings for language code %s. Will default to English.", rlGetLanguageCode(lang));
		if(m_StringsTable == NULL)
		{
			// default to English
			formatf(tableName, "achievement_strings_%s", rlGetLanguageCode(LANGUAGE_ENGLISH));
			m_StringsTable = m_Metadata->GetTable(tableName);
		}
        rverify(m_StringsTable, catchall, rlError("There are no Achievement strings for language code %s", rlGetLanguageCode(lang)));

        success = true;
    }
    rcatchall
    {

    }

    return success;
}

void
RgscAchievementManager::Update()
{
	{
		SYS_CS_SYNC(m_ProgressCs);

		if (m_ProgressQueue.GetCount() > 0 && !m_ProgressWriteStatus.Pending())
		{
#if 0 // TODO: JRM - Enable and potentially batch multiple queries into one
			AchievementProgress& progress = m_ProgressQueue.Pop();
			WriteSingleAchievementProgress(progress.m_Id, progress.m_Progress, &m_ProgressWriteStatus);
#else
			// We don't have an SCS endpoint for writing progress yet. For now, simply
			// pop the progress queue if it contains anything.
			m_ProgressQueue.Pop();
#endif
		}
	}
}

void
RgscAchievementManager::Shutdown()
{
    Clear();
}

bool RgscAchievementManager::ConvertRosTimeStringToTmStruct(tm &t, const char* szTime)
{
	/*
	struct tm {
		int tm_sec;     // seconds after the minute - [0,59]
		int tm_min;     // minutes after the hour - [0,59]
		int tm_hour;    // hours since midnight - [0,23]
		int tm_mday;    // day of the month - [1,31]
		int tm_mon;     // months since January - [0,11]
		int tm_year;    // years since 1900
		int tm_wday;    // days since Sunday - [0,6]
		int tm_yday;    // days since January 1 - [0,365]
		int tm_isdst;   // daylight savings time flag
	};
	*/

	bool success = false;

	rtry
	{
		// Example: "2011-02-11T02:24:58.713"
		int n = sscanf_s(szTime, "%d-%d-%dT%d:%d:%d", &t.tm_year, &t.tm_mon, &t.tm_mday, &t.tm_hour, &t.tm_min, &t.tm_sec);
		rverify(n == 6, catchall, rlError("ConvertRosTimeStringToTmStruct: sscanf_s failed to find all expected fields"));

		t.tm_year -= 1900;
		t.tm_mon -= 1;
		t.tm_isdst = 0;
		success = true;
	}
	rcatchall
	{
	}
	
	return success;
}

void
RgscAchievementManager::SortAchievements(IAchievementList* achievements)
{
	struct SortPredicateV1
	{
		static bool Sort(Achievement& a, Achievement& b)
		{
			/*
			Sort Achievements in the same order that the Xbox 360 Guide does. The Xbox 360 Guide sorts and displays Achievements as follows:
				- Earned Achievements are sorted in most-recently-earned order.
				- Unearned, non-hidden Achievements are sorted (ascending) by Progress and then AchievementID.
				- Unearned, hidden Achievements are sorted (ascending) by AchievementID.
			*/
			if(a.m_Achieved)
			{
				if(b.m_Achieved)
				{
					// both are achieved, sorted in most-recently-earned order
					return a.m_AchievedTime > b.m_AchievedTime;
				}
				else
				{
					// a is achieved, but b is not, therefore a comes before b
					return true;
				}
			}
			else if(b.m_Achieved)
			{
				// b is achieved, but a is not, therefore b comes before a
				return false;
			}
			else if(a.m_ShowUnachieved)
			{
				if(b.m_ShowUnachieved)
				{
					// If A has a Max Progress but
					if (a.m_Progress > b.m_Progress)
						return true;
					else if (a.m_Progress < b.m_Progress)
						return false;

					// both are visible, sorted ascending by AchievementId
					return a.m_Id < b.m_Id;
				}
				else
				{
					// a is visible, but b is not, therefore a comes before b
					return true;
				}
			}
			else if(b.m_ShowUnachieved)
			{
				// b is visible, but a is not, therefore b comes before a
				return false;
			}
			else
			{
				// unearned, hidden achievements are sorted ascending by AchievementId.
				return a.m_Id < b.m_Id;
			}
		}
	};

	// determine which version of the achievement list interface we are working with
	RGSC_HRESULT hr = RGSC_NOINTERFACE;
	AchievementListV1* listV1 = NULL;
	hr = achievements->QueryInterface(IID_IAchievementListV1, (void**) &listV1);
	if(AssertVerify(SUCCEEDED(hr) && (listV1 != NULL)))
	{
		Achievement* array = listV1->GetAchievementArray();
		std::sort(array, array + listV1->GetNumAchievements(), SortPredicateV1::Sort);
	}
}

bool 
RgscAchievementManager::ReadMetadata(const RockstarId UNUSED_PARAM(profileId),
                              const IAchievement::DetailFlags detailFlags,
                              const AchievementId firstAchievementId,
							  const u32 maxAchievementsToRead,
                              IAchievementList* achievements)
{
	bool success = false;

	if(m_MetadataTable == NULL)
	{
		return false;
	}

	// determine which version of the achievement list interface we are working with
	RGSC_HRESULT hr = RGSC_NOINTERFACE;
	AchievementListV1* listV1 = NULL;
	hr = achievements->QueryInterface(IID_IAchievementListV1, (void**) &listV1);
	if(FAILED(hr))
	{
		return false;
	}

	AchievementId achievementId = m_MetadataTable->GetMinKey();
    if(firstAchievementId >= (int)achievementId)
    {
        achievementId = firstAchievementId;
    }

    u32 numRecords = 0;

    while((numRecords < m_MetadataTable->GetNumRecords()) &&
          (maxAchievementsToRead > (int)numRecords) &&
          achievementId <= m_MetadataTable->GetMaxKey())
    {
        rtry
        {
            rverify(achievements, catchall, );

            rverify(m_MetadataTable, catchall, );

            AchievementMetadataRecord m;
			m.EnableReadAchievementProgress(IsAchievementProgressSupported());

            bool exists = m_MetadataTable->GetRecord(achievementId, m);
            rcheck(exists, catchall, );

            StringRecord s[3];

            // could add support for reading a sequential range of records
            // if this is too slow, but it would take up a lot of memory
            AchievementId keys[3] = {(achievementId * 3) - 2,
                           (achievementId * 3) - 1,
                           (achievementId * 3) - 0};

            rverify(m_StringsTable, catchall, );

            if(detailFlags & IAchievement::ACHIEVEMENT_DETAILS_STRINGS)
            {
                exists = m_StringsTable->GetRecord(keys[0], s[0]);
                rverify(exists, catchall, );

                exists = m_StringsTable->GetRecord(keys[1], s[1]);
                rverify(exists, catchall, );
            }

            if(detailFlags & IAchievement::ACHIEVEMENT_DETAILS_STRINGS)
            {
                exists = m_StringsTable->GetRecord(keys[2], s[2]);
                rverify(exists, catchall, );
            }

#if __RAGE_DEPENDENCY
			grcImage* image = NULL;
#endif

            if(detailFlags & IAchievement::ACHIEVEMENT_DETAILS_IMAGE)
            {
#if __RAGE_DEPENDENCY
				rverify(m_ImagesTable, catchall, );

                // read PNG data
                ImageRecord pngImage;
                exists = m_ImagesTable->GetRecord(achievementId, pngImage);
                rverify(exists, catchall, );

				// convert it to a grcImage
                char filename[64];
                fiDeviceMemory::MakeMemoryFileName(filename, sizeof(filename), pngImage.GetData(), pngImage.GetSize(), false, NULL);
                image = grcImage::LoadPNG(filename);	
                rverify(image, catchall, );
#endif
			}

			rverify(listV1->AddAchievement(), catchall, );

			Achievement* v1 = listV1->GetAchievement(numRecords);

#if __RAGE_DEPENDENCY
			achievement->Reset(achievementId, (IAchievement::AchievementType)m.GetType(),
								 m.GetPoints(), s[0].GetString(), s[1].GetString(), s[2].GetString(),
								 image, m.ShowUnachieved(), 0);
#else

			v1->m_Id = achievementId;
			v1->m_Type = (IAchievement::AchievementType)m.GetType();
			v1->m_Points = m.GetPoints();
			if(detailFlags & IAchievement::ACHIEVEMENT_DETAILS_STRINGS)
			{
				safecpy(v1->m_Label, s[0].GetString(), IAchievement::MAX_LABEL_LEN);
				safecpy(v1->m_Description, s[1].GetString(), IAchievement::MAX_DESC_LEN);
				safecpy(v1->m_UnachievedString, s[2].GetString(), IAchievement::MAX_UNACHIEVED_LEN);
			}
			v1->m_ShowUnachieved = m.ShowUnachieved();
			v1->m_AchievedTime = 0;
			v1->m_Achieved = false;
			v1->m_MaxProgress = m.GetMaxProgress();
#endif

#if !__NO_OUTPUT
			if(detailFlags & IAchievement::ACHIEVEMENT_DETAILS_IMAGE)
			{
#if __RAGE_DEPENDENCY
// 				rlDebug("Achievement Id:%d, Type:%d, Title:%s, Description:%s, Unachieved String:%s, Image Size:%dx%d, Points:%d, Show Unachieved:%s", 
// 						achievementId, m.GetType(), s[0].GetString(), s[1].GetString(), s[2].GetString(), image->GetWidth(), image->GetHeight(),
// 						m.GetPoints(), m.ShowUnachieved() ? "yes" : "no");
#else
//   				rlDebug("Achievement Id:%d, Type:%d, Title:%s, Description:%s, Unachieved String:%s, Points:%d, Show Unachieved:%s", 
//   					achievementId, m.GetType(), s[0].GetString(), s[1].GetString(), s[2].GetString(),
//   					m.GetPoints(), m.ShowUnachieved() ? "yes" : "no");
#endif
			}
			else
			{
//  				rlDebug("Achievement Id:%d, Type:%d, Title:%s, Description:%s, Unachieved String:%s, Points:%d, Show Unachieved:%s", 
//  					achievementId, m.GetType(), s[0].GetString(), s[1].GetString(), s[2].GetString(),
//  					m.GetPoints(), m.ShowUnachieved() ? "yes" : "no");
			}
#endif

            numRecords++;

            success = true;

        }
        rcatchall
        {
            rlDebug("Achievement Id:%d Does not exist", achievementId);
        }

        achievementId++;
    }

    return success;
}

bool RgscAchievementManager::GetAchievementImageRecord(AchievementId achievementId, ImageRecord* imageRecord)
{
	bool success = false;
	rtry
	{
		rverify(achievementId > 0, catchall, );
		rverify(imageRecord, catchall, );
		rverify(m_MetadataTable, catchall, );
		rverify(m_ImagesTable, catchall, );

		bool exists = m_ImagesTable->GetRecord(achievementId, *imageRecord);
		rverify(exists, catchall, rlError("GetAchievementImageRecord: Achievement Id:%d Does not exist", achievementId));

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

const char* RgscAchievementManager::GetAchievementImageAsJson(const char* jsonRequest)
{
	int achievementId = -1;

	JSONNODE *n = json_parse(jsonRequest);

	if(!AssertVerify(n != NULL))
	{
		return "";
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return "";
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "AchievementId") == 0)
		{
			achievementId = json_as_int(*i);
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	const json_char* avatarBase64 = "";

	rtry
	{
		rverify(achievementId != -1, catchall, );
		rverify(m_MetadataTable, catchall, );

		ImageRecord pngImage;
		bool exists = m_ImagesTable->GetRecord(achievementId, pngImage);
		rverify(exists, catchall, rlError("GetAchievementImageAsJson: Achievement Id:%d Does not exist", achievementId));

		avatarBase64 = json_encode64(pngImage.GetData(), pngImage.GetSize());
	}
	rcatchall
	{

	}

	return avatarBase64;
}

bool RgscAchievementManager::GetAchievementAsJson(const IAchievement* achievement, JSONNODE *p)
{
	if(!AssertVerify(achievement))
	{
		return false;
	}

	if(!AssertVerify(p))
	{
		return false;
	}

	// determine which version of the achievement interface we are working with
	Achievement* v1 = NULL;
	RGSC_HRESULT hr = ((IAchievement*)achievement)->QueryInterface(IID_AchievementV1, (void**) &v1);
	if(FAILED(hr))
	{
		return false;
	}

	if(v1)
	{
		if(!AssertVerify(v1->IsValid()))
		{
			return false;
		}

		time_t secs = v1->GetAchievedTime();

// 		// convert from UTC time to local time
// 		char szTime[32] = {0};
// 
// 		if(secs > 0)
// 		{
// 			struct tm* t = localtime(&secs);
// 			formatf(szTime, "%04u-%02u-%02uT%02u:%02u:%02u", t->tm_year + 1900, t->tm_mon + 1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);
// 		}

		char szTime[64];
		formatf(szTime, "%"I64FMT"u", secs);

		json_push_back(p, json_new_i("Id", v1->GetId()));
		json_push_back(p, json_new_a("Name", v1->GetLabel()));
		json_push_back(p, json_new_a("Description", v1->GetDescription()));
		json_push_back(p, json_new_a("HowTo", v1->GetUnachievedString()));
		json_push_back(p, json_new_i("Type", (int)v1->GetType()));
		json_push_back(p, json_new_b("Visible", v1->ShowUnachieved()));

#if 0 //RSG_CPU_X64
		// TODO: NS - remove after we've determined whether we can get crash dumps with security enabled build
		if(PARAM_sccrashtest.Get())
		{
			v1 = NULL;
		}
#endif

		json_push_back(p, json_new_i("Points", v1->GetPoints()));
		json_push_back(p, json_new_b("Achieved", v1->IsAchieved()));
		json_push_back(p, json_new_a("DateAchieved", szTime));

		Achievement* v2 = NULL;
		RGSC_HRESULT hr = ((IAchievement*)achievement)->QueryInterface(IID_AchievementV2, (void**) &v2);
		if(SUCCEEDED(hr))
		{
			char progress[64] = {0};
			formatf(progress, "%"I64FMT"d", v2->GetProgress());
			json_push_back(p, json_new_a("Progress", progress));

			char maxProgress[64] = {0};
			formatf(maxProgress, "%"I64FMT"d", v2->GetMaxProgress());
			json_push_back(p, json_new_a("MaxProgress", maxProgress));
		}
	}

	return true;
}

const char* RgscAchievementManager::ReadLocalAchievementsAsJson(const AchievementId firstAchievementId,
														 const u32 maxAchievements)
{
	json_char *jc = "";

	// This can be called on the main thread while the Achievements thread is reading achievements. If we can't lock the CS,
	// instead return an empty achievement panel to the UI. This should be very rare.
	// Unlock() is called at the end of this block.
    if (m_Cs.TryLock())
	{
		bool success = false;

		rtry
		{
			rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

			IAchievement::DetailFlags detailFlags = (IAchievement::DetailFlags)(
				IAchievement::ACHIEVEMENT_DETAILS_STRINGS | 
				IAchievement::ACHIEVEMENT_DETAILS_IS_AWARDED |
				IAchievement::ACHIEVEMENT_DETAILS_TIMESTAMP);

			u32 size = AchievementList::GetMemoryRequired(detailFlags, maxAchievements);

			void* buf = RGSC_ALLOCATE(RgscAchievementManager, size);

			rverify(buf, catchall, );

			AchievementList achievements(IAchievement::ACHIEVEMENT_DETAILS_ALL, maxAchievements, (u8*)buf);

			rverify(ReadMetadata(GetRgscConcreteInstance()->_GetProfileManager()->GetSignedInProfile().GetProfileId(),
				detailFlags,
				firstAchievementId,
				maxAchievements,
				&achievements), catchall, );

			if((achievements.GetNumAchievements() > 0) &&
				((detailFlags & IAchievement::ACHIEVEMENT_DETAILS_IS_AWARDED) ||
				(detailFlags & IAchievement::ACHIEVEMENT_DETAILS_TIMESTAMP)))
			{
				AchievementsFile file;
				file.SetExportVersion(GetVersion());

				rverify(file.Open(), catchall, );

				const AchievementBitSet& fileSet = file.GetAchievementBitSet();

				for(unsigned int i = 0; i < achievements.GetNumAchievements(); i++)
				{
					Achievement* achievement = achievements.GetAchievement(i);
					if(achievement->IsValid())
					{
						AchievementId id = achievement->GetId();
						bool isAchieved = fileSet.IsSet(id);
						achievement->SetAchieved(isAchieved);
						if(isAchieved)
						{
							achievement->SetTimestamp(file.GetAchievementTimestamp(id));
						}
					}
				}

				file.Close();

				success = true;
			}
			else
			{
				success = true;
			}

			rverify(success, catchall, );

			SortAchievements(&achievements);

			// now convert to json
			JSONNODE* n = json_new(JSON_NODE);
			rverify(n, catchall, );

			JSONNODE* c = json_new(JSON_ARRAY);
			rverify(c, catchall, );

			json_set_name(c, "Achievements");

			u32 numAchievements = 0;
			u32 numAchievementsAwarded = 0;
			u32 numPointsAwarded = 0;
			u32 maxPoints = 0;

			for(AchievementId i = 0; i < achievements.GetNumAchievements(); i++)
			{
				Achievement* achievement = achievements.GetAchievement(i);

				if(achievement->IsValid())
				{
					JSONNODE* p = json_new(JSON_NODE);
					rverify(p, catchall, );

					rverify(GetAchievementAsJson(achievement, p), catchall, );

					numAchievements++;

					if(achievement->IsAchieved())
					{
						numAchievementsAwarded++;
						numPointsAwarded += achievement->GetPoints();
					}

					maxPoints += achievement->GetPoints();

					json_push_back(c, p);
				}
			}

			if(buf)
			{
				RGSC_FREE(buf);
				buf = NULL;

			}

			json_push_back(n, json_new_i("NumAchievements", numAchievements));
			json_push_back(n, json_new_i("NumAchievementsAwarded", numAchievementsAwarded));
			json_push_back(n, json_new_i("NumPointsAwarded", numPointsAwarded));
			json_push_back(n, json_new_i("MaxPoints", maxPoints));

			json_push_back(n, c);

			jc = json_write_formatted(n);

#if !__NO_OUTPUT
			// 		unsigned int len = strlen(jc);
			// 		for(unsigned i = 0; i < len; i++)
			// 		{
			// 			printf("%c", jc[i]);
			// 		}
#endif

			json_delete(n);

			m_Cs.Unlock();
		}
		rcatchall
		{

		}
	}
	else
	{
		// now convert to json
		JSONNODE* n = json_new(JSON_NODE);
		rverify(n, catchall, );

		JSONNODE* c = json_new(JSON_ARRAY);
		rverify(c, catchall, );

		json_set_name(c, "Achievements");

		json_push_back(n, json_new_i("NumAchievements", 0));
		json_push_back(n, json_new_i("NumAchievementsAwarded", 0));
		json_push_back(n, json_new_i("NumPointsAwarded", 0));
		json_push_back(n, json_new_i("MaxPoints", 0));

		json_push_back(n, c);

		jc = json_write_formatted(n);

		json_delete(n);
	}

	return jc;
}

bool 
RgscAchievementManager::IsAchievementProgressSupported()
{
	return m_Metadata->GetVersion() >= AchievementsFile::GetMinVersionForProgress();
}

bool 
RgscAchievementManager::IsSynchronized() const
{
	return m_Synchronized;
}

void 
RgscAchievementManager::SetSynchronized(bool synchronized)
{
	m_Synchronized = synchronized;
}

void 
RgscAchievementManager::SignOut()
{
	m_Synchronized = false;

	for(unsigned int i = 0; i < MAX_ENUMERATIONS; ++i)
	{
		sm_AchievementEnumerationHandle[i].m_IsCreated = false;
	}
}

} //namespace rgsc

#endif // RSG_PC
