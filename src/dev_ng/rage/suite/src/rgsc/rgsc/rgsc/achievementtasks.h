// 
// rline/rlpcachievement.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPCACHIEVEMENTTASKS_H
#define RLINE_RLPCACHIEVEMENTTASKS_H

#include "file/file_config.h"
#include "achievements.h"

// rage headers
#include "ros/rlroshttptask.h"
#include "rline/rlworker.h"

using namespace rage;

namespace rgsc
{

class AchievementBitSet;

///////////////////////////////////////////////////////////////////////////////
// AchievementFileLoadWorker
///////////////////////////////////////////////////////////////////////////////
struct AchievementFileLoadWorker : public rlWorker
{
	AchievementFileLoadWorker();
	bool Configure(AchievementsFile* file, netStatus* status);
	bool Start(const int cpuAffinity);

private:

	//Make Start() un-callable
	bool Start(const char* /*name*/, const unsigned /*stackSize*/, const int /*cpuAffinity*/)
	{
		FastAssert(false);
		return false;
	}

	virtual void Perform();

	AchievementsFile* m_File; 
	netStatus* m_Status;
};

///////////////////////////////////////////////////////////////////////////////
// AchievementFileSaveWorker
///////////////////////////////////////////////////////////////////////////////
struct AchievementFileSaveWorker : public rlWorker
{
	AchievementFileSaveWorker();
	bool Configure(AchievementsFile* file, netStatus* status);
	bool Start(const int cpuAffinity);

private:

	//Make Start() un-callable
	bool Start(const char* /*name*/, const unsigned /*stackSize*/, const int /*cpuAffinity*/)
	{
		FastAssert(false);
		return false;
	}

	virtual void Perform();

	AchievementsFile* m_File; 
	netStatus* m_Status;
};

//////////////////////////////////////////////////////////////////////////
// rlRosReadAchievementsTask
//////////////////////////////////////////////////////////////////////////
class rlRosReadAchievementsTask : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosReadAchievementsTask);

#if __RAGE_DEPENDENCY
	RL_TASK_USE_CHANNEL(rgsc_dbg_achievement);
#endif

	rlRosReadAchievementsTask();
	virtual ~rlRosReadAchievementsTask();

	bool Configure(void* ctx,
		RockstarId profileId,
		AchievementBitSet* achievementsFromRos,
		AchievementBitSet* achievementsAwardedOffline,
		time_t* timestamps);

protected:
	virtual const char* GetServiceMethod() const;
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const rlRosResult& result, int& resultCode);

private:
	AchievementBitSet* m_AchievementsFromRos;
	AchievementBitSet* m_AchievementsAwardedOffline;
	time_t* m_Timestamps;
};


//////////////////////////////////////////////////////////////////////////
// rlRosWriteOfflineAchievementsTask
//////////////////////////////////////////////////////////////////////////
class rlRosWriteOfflineAchievementsTask : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosWriteOfflineAchievementsTask);

#if __RAGE_DEPENDENCY
	RL_TASK_USE_CHANNEL(rgsc_dbg_achievement);
#endif

	rlRosWriteOfflineAchievementsTask();
	virtual ~rlRosWriteOfflineAchievementsTask();

	bool Configure(void* ctx,
		AchievementBitSet* offlineAchievements);

protected:
	virtual const char* GetServiceMethod() const;
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const rlRosResult& result, int& resultCode);

private:
	u64 GetBitFieldAsScaler(AchievementId start, AchievementId end);
	AchievementBitSet* m_OfflineAchievements;
};


///////////////////////////////////////////////////////////////////////////////
//  SyncAchievementsTask
///////////////////////////////////////////////////////////////////////////////
class SyncAchievementsTask : public TaskBase
{
public:
	RL_TASK_DECL(SyncAchievementsTask);

	enum State
	{
		STATE_INVALID = -1,
		STATE_READ_RECORD,
		STATE_READING_RECORD,
		STATE_LOAD_FILE,
		STATE_LOADING_FILE,
		STATE_MERGE_ACHIEVEMENTS,
		STATE_SAVE_FILE,
		STATE_SAVING_FILE,
		STATE_WRITE_RECORD,
		STATE_WRITING_RECORD,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	SyncAchievementsTask();

	bool Configure(RgscAchievementManager* ctx, const RockstarId profileId);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	bool ReadRecord();
	bool MergeAchievements();
	bool WriteRecord();

	bool m_NeedBackendWrite;

	AchievementBitSet m_AchievementsFromRos;
	AchievementBitSet m_AchievementsMissingTimestamps;
	time_t m_Timestamps[IAchievementManager::MAX_NUM_ACHIEVEMENTS];

	AchievementBitSet m_OfflineAchievementsToWrite;

	AchievementsFile m_File;
	AchievementFileLoadWorker m_FileLoadWorker;
	AchievementFileSaveWorker m_FileSaveWorker;
	bool m_Error;

	RockstarId m_ProfileId;
	int m_State;
	netStatus m_MyStatus;
};


///////////////////////////////////////////////////////////////////////////////
//  RaiseAchievementAwardedEventTask
///////////////////////////////////////////////////////////////////////////////
class RaiseAchievementAwardedEventTask : public TaskBase
{
public:
	RL_TASK_DECL(RaiseAchievementAwardedEventTask);

	enum State
	{
		STATE_INVALID   = -1,
		STATE_RAISE_EVENT,
	};

	RaiseAchievementAwardedEventTask();

	bool Configure(RgscAchievementManager* ctx, const IAchievement* achievement);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	bool RaiseEvent();

	const IAchievement* m_Achievement;

	int m_State;
	netStatus m_MyStatus;
};


//////////////////////////////////////////////////////////////////////////
// rlRosWriteAchievementsTask
//////////////////////////////////////////////////////////////////////////
class rlRosWriteAchievementsTask : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosWriteAchievementsTask);

#if __RAGE_DEPENDENCY
	RL_TASK_USE_CHANNEL(rgsc_dbg_achievement);
#endif

	rlRosWriteAchievementsTask();
	virtual ~rlRosWriteAchievementsTask();

	// TODO: NS - get rid of the ctx parameter in all configure methods, no longer needed
	bool Configure(void* ctx, AchievementId achievementId, time_t* timestamp);

	virtual unsigned GetTimeoutSecs() {return 90;}

protected:
	virtual const char* GetServiceMethod() const;
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const rlRosResult& result, int& resultCode);

private:
	time_t* m_Timestamp;
	AchievementId m_AchievementId;
};

//////////////////////////////////////////////////////////////////////////
// rlRosWriteAchievementProgressTask
//////////////////////////////////////////////////////////////////////////
class rlRosWriteAchievementProgressTask : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosWriteAchievementProgressTask);

#if __RAGE_DEPENDENCY
	RL_TASK_USE_CHANNEL(rgsc_dbg_achievement);
#endif

	rlRosWriteAchievementProgressTask();
	virtual ~rlRosWriteAchievementProgressTask();
	bool Configure(void* ctx, AchievementId achievementId, s64 progress);

	virtual unsigned GetTimeoutSecs() {return 90;}

protected:
	virtual const char* GetServiceMethod() const;
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const rlRosResult& result, int& resultCode);

private:
	AchievementId m_AchievementId;
	s64 m_Progress;
};

///////////////////////////////////////////////////////////////////////////////
//  WriteSingleAchievementTask
///////////////////////////////////////////////////////////////////////////////
class WriteSingleAchievementTask : public TaskBase
{
public:
	RL_TASK_DECL(WriteSingleAchievementTask);

	enum State
	{
		STATE_INVALID   = -1,
		STATE_LOAD_FILE,
		STATE_LOADING_FILE,
		STATE_WRITE,
		STATE_WRITING,
		STATE_CACHE_RESULTS,
		STATE_SAVE_FILE,
		STATE_SAVING_FILE
	};

	WriteSingleAchievementTask();

	bool Configure(RgscAchievementManager* ctx, const AchievementId achievementId);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	bool WriteAchievement();
	bool CacheResults();

	AchievementId m_AchievementId;
	time_t m_Timestamp;

	AchievementsFile m_File;
	AchievementFileLoadWorker m_FileLoadWorker;
	AchievementFileSaveWorker m_FileSaveWorker;
	bool m_bError;

	int m_State;
	netStatus m_MyStatus;
};

///////////////////////////////////////////////////////////////////////////////
//  ReadAchievementsTask
///////////////////////////////////////////////////////////////////////////////
class ReadAchievementsTask : public TaskBase
{
public:
	RL_TASK_DECL(ReadAchievementsTask);

	enum State
	{
		STATE_INVALID   = -1,
		STATE_BEGIN,
		STATE_LOAD_FILE,
		STATE_LOADING_FILE,
		STATE_READ_FROM_DATABASE,
		STATE_READING_FROM_DATABASE,
		STATE_PROCESS_RESULTS_FROM_DATABASE,
		STATE_PROCESS_RESULTS_FROM_CACHE,
		STATE_CACHE_RESULTS,
		STATE_SAVE_FILE,
		STATE_SAVING_FILE,
	};

	ReadAchievementsTask();

	bool Configure(RgscAchievementManager* ctx, RockstarId profileId, const IAchievement::DetailFlags detailFlags, IAchievementList* achievements);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	bool ReadAchievements();
	bool ProcessResultsFromDatabase();
	bool ProcessResultsFromCache();
	bool ProcessResults(const AchievementBitSet &bits, time_t (&timestamps)[IAchievementManager::MAX_NUM_ACHIEVEMENTS]);
	bool CacheResults();

	IAchievementList* m_Achievements;
	IAchievement::DetailFlags m_DetailFlags;

	RockstarId m_ProfileId;
	bool m_RequesterIsLocalGamer;

	AchievementBitSet m_AchievementsFromRos;
	AchievementBitSet m_AchievementsMissingTimestamps;
	time_t m_Timestamps[IAchievementManager::MAX_NUM_ACHIEVEMENTS];

	u32 m_NumAwardedAchievements;
	bool m_NeedToQueryRos;

	AchievementsFile m_File;
	AchievementFileLoadWorker m_FileLoadWorker;
	AchievementFileSaveWorker m_FileSaveWorker;
	bool m_Error;

	int m_State;
	netStatus m_MyStatus;
};

#if __DEV
//////////////////////////////////////////////////////////////////////////
// rlRosDeleteAchievementsTask
//////////////////////////////////////////////////////////////////////////
class rlRosDeleteAchievementsTask : public rlRosHttpTask
{
public:
	RL_TASK_DECL(rlRosDeleteAchievementsTask);

#if __RAGE_DEPENDENCY
	RL_TASK_USE_CHANNEL(rgsc_dbg_achievement);
#endif

	rlRosDeleteAchievementsTask();
	virtual ~rlRosDeleteAchievementsTask();

	bool Configure(void* ctx);

protected:
	virtual const char* GetServiceMethod() const;
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const rlRosResult& result, int& resultCode);

private:
};


///////////////////////////////////////////////////////////////////////////////
//  DeleteAllAchievementsTask
///////////////////////////////////////////////////////////////////////////////
class DeleteAllAchievementsTask : public TaskBase
{
public:
	RL_TASK_DECL(DeleteAllAchievementsTask);

	enum State
	{
		STATE_INVALID   = -1,
		STATE_DELETE,
		STATE_DELETING,
	};

	DeleteAllAchievementsTask();

	bool Configure(RgscAchievementManager* ctx);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);
	
	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	bool Delete();

	int m_State;
	netStatus m_MyStatus;
};
#endif // __DEV

} // namespace rgsc

#endif // RLINE_RLPCACHIEVEMENTTASKS_H