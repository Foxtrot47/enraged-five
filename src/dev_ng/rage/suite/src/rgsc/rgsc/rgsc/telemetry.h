// 
// rgsc/rgsc/telemetry.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RGSC_TELEMETRY_H 
#define RGSC_TELEMETRY_H

// rgsc includes
#include "file/file_config.h"
#include "telemetry_interface.h"

using namespace rgsc;
using namespace rage;

namespace rgsc
{
	// ===============================================================================================
	// ITelemetry
	// ===============================================================================================
	class RgscTelemetryManager : public ITelemetryLatestVersion
	{
	public:
		// ===============================================================================================
		// inherited from IRgscUnknown
		// ===============================================================================================
		virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

		// ===============================================================================================
		// inherited from ITelemetryV1
		// ===============================================================================================
		virtual RGSC_HRESULT RGSC_CALL SetPolicies(ITelemetryPolicy* policies);
		virtual bool RGSC_CALL Write(const char* metricName, const char* metricBlob);
		virtual bool RGSC_CALL Flush(bool bFlushImmediate);
		virtual bool RGSC_CALL HasAnyMetrics();
		virtual bool RGSC_CALL IsFlushInProgress();
		virtual u32 RGSC_CALL GetAvailableMemory();
		virtual bool RGSC_CALL HasMemoryForMetric();
		virtual bool RGSC_CALL IsAcceptingWrites();

		// ===============================================================================================
		// inherited from ITelemetryV2
		// ===============================================================================================
		virtual void RGSC_CALL SetGameHeaderInfoCallback(GameHeaderInfoCallback cb);

		// ===============================================================================================
		// inherited from ITelemetryV3
		// ===============================================================================================
		virtual bool RGSC_CALL Write(const char* metricName, const char* metricBlob, const int logChannel, const int logLevel);

		// ===============================================================================================
		// inherited from ITelemetryV4
		// ===============================================================================================
		virtual void RGSC_CALL CancelFlushes();

		// ===============================================================================================
		// accessible from anywhere in the dll
		// ===============================================================================================
		RgscTelemetryManager();
		virtual ~RgscTelemetryManager();

		bool Init();
		bool Shutdown();
		void Update();

		static void OnDeferredFlush(void);
		static void OnTelemetryFlushEnd(const netStatus& status);

	private:

		bool SetGamerHeader(RsonWriter* rw);

		u32 m_FlushRequestedTime;
		GameHeaderInfoCallback m_GameHeaderInfoCallback;
		atDelegate<bool (RsonWriter*)> m_HeaderInfoCallback;
	};

} // namespace rgsc

#endif // RGSC_TELEMETRY_H 
