// 
// rline/achievementtasks.cpp 
// 
// Copyright (C) 2014-2014 Rockstar Games.  All Rights Reserved. 
//
#include "achievementtasks.h"

#include "achievements_interface.h"
#include "achievements.h"
#include "json.h"
#include "tasks.h"
#include "rgsc.h"

// rage includes
#include "parser/manager.h"
#include "rline/rldiag.h"
#include "rline/ros/rlros.h"
#include "system/timer.h"

#include <time.h>

using namespace rage;

namespace rgsc
{

class AchievementV1;

#if __RAGE_DEPENDENCY
RAGE_DEFINE_SUBCHANNEL(rgsc_dbg, achievementtask)
#undef __rage_channel
#define __rage_channel rgsc_dbg_achievementtask
#endif

bool LoadAchievementFile(AchievementFileLoadWorker* worker, AchievementsFile* file, netStatus* status)
{
	rtry
	{
		rverify(worker, catchall, );
		rverify(file, catchall, );
		rverify(status, catchall, );

		RgscAchievementManager::sm_FileIoInProgress = true;
		
		rlDebug3("LoadAchievementFile: achievement file opened");

		rcheck(worker->Configure(file, status), catchall, );
		rcheck(worker->Start(0), catchall, );
		status->SetPending();
		worker->Wakeup();

		return true;
	}
	rcatchall
	{
		RgscAchievementManager::sm_FileIoInProgress = false;
		rlDebug3("LoadAchievementFile: achievement file failed open");
		return false;
	}
}

bool SaveAchievementFile(AchievementFileSaveWorker* worker, AchievementsFile* file, netStatus* status)
{
	rtry
	{
		rverify(worker, catchall, );
		rverify(file, catchall, );
		rverify(status, catchall, );

		rcheck(worker->Configure(file, status), catchall, );
		rcheck(worker->Start(0), catchall, );
		status->SetPending();
		worker->Wakeup();

		return true;
	}
	rcatchall
	{
		RgscAchievementManager::sm_FileIoInProgress = false;
		rlDebug3("SaveAchievementFile: achievement file failed open");
		return false;
	}
}

//////////////////////////////////////////////////////////////////////////
// rlRosReadAchievementsTask
//////////////////////////////////////////////////////////////////////////

rlRosReadAchievementsTask::rlRosReadAchievementsTask()
{
}

rlRosReadAchievementsTask::~rlRosReadAchievementsTask()
{
}

const char* rlRosReadAchievementsTask::GetServiceMethod() const
{
	return "achievements.asmx/GetPlayerAchievements";
}

bool  rlRosReadAchievementsTask::Configure(void* /*ctx*/, RockstarId profileId, AchievementBitSet* achievementsFromRos, AchievementBitSet* achievementsAwardedOffline, time_t* timestamps)
{
	bool success = false;

	rtry
	{
		m_AchievementsFromRos = achievementsFromRos;
		m_AchievementsAwardedOffline = achievementsAwardedOffline;
		m_Timestamps = timestamps;

		m_AchievementsFromRos->Clear();
		m_AchievementsAwardedOffline->Clear();
		memset(m_Timestamps, 0, IAchievementManager::MAX_NUM_ACHIEVEMENTS * sizeof(time_t));

		rverify(rlRosHttpTask::Configure(0), catchall, );

		rlRosCredentials cred = rlRos::GetCredentials(0);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );
		rverify(AddIntParameter("playerRockstarId", profileId), catchall, );
		//rverify(AddIntParameter("crossTitleId", 0), catchall, );
		rverify(AddStringParameter("crossTitleId", "", true), catchall, );
		rverify(AddStringParameter("crossTitleName", "", true), catchall, );
		rverify(AddStringParameter("crossTitlePlatformName", "", true), catchall, );

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

bool rlRosReadAchievementsTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
	// 	<Response>
	// 		<Status>1</Status>
	// 		<AchievementList Count="1">
	// 		<Achievements Id="3" DateAchieved="2011-02-11T02:24:58.713"/>
	// 		</AchievementList>
	// 	</Response>

	resultCode = 0;

	Assert(node);

	rtry
	{
		const parTreeNode* results = node->FindChildWithName("AchievementList");
		rcheck(results, catchall, rlTaskError("Failed to find <AchievementList> element"));

		int count;
		rverify(rlHttpTaskHelper::ReadInt(count, results, NULL, "count"), catchall, );

		if(count > 0)
		{
			parTreeNode::ChildNodeIterator iter = results->BeginChildren();

			for(unsigned int index = 0; iter != results->EndChildren() && index < IAchievementManager::MAX_NUM_ACHIEVEMENTS; ++iter, ++index)
			{
				parTreeNode* record = *iter;
				Assert(record);

				if(stricmp(record->GetElement().GetName(), "Achievement") != 0) continue;

				int achievementId = 0;
				bool exists = rlHttpTaskHelper::ReadInt(achievementId, record, NULL, "Id");
				rcheck(exists, catchall, rlTaskError("Failed to find <Achievement.Id> attribute"));
				rcheck(achievementId >= 0, catchall, rlTaskError("Negative Achievement Id %d not expected", achievementId));
				rcheck(achievementId < IAchievementManager::MAX_NUM_ACHIEVEMENTS, catchall, rlTaskError("Achievement Id %d is too large", achievementId));

				m_AchievementsFromRos->Set((AchievementId) achievementId);

				// ReadBool doesn't parse "true" and "false", it expects an int (zero, non-zero)
				bool awardedOffline = false;
				const char* szAwardedOffline = rlHttpTaskHelper::ReadString(record, NULL, "AchievedOffline");
				rcheck(szAwardedOffline, catchall, rlTaskError("Failed to find <Achievement.AchievedOffline> attribute"));
				awardedOffline = stricmp(szAwardedOffline, "false") != 0;

				m_Timestamps[achievementId] = 0;

				const char* timestamp = rlHttpTaskHelper::ReadString(record, NULL, "DateAchieved");
				//rcheck(timestamp, catchall, rlTaskError("Failed to find <Achievement.DateAchieved> attribute"));
				if(AssertVerify(timestamp && (timestamp[0] != '\0')))
				{
					tm t;
					bool valid = RgscAchievementManager::ConvertRosTimeStringToTmStruct(t, timestamp);
					if(AssertVerify(valid))
					{
						m_Timestamps[achievementId] = _mkgmtime(&t);
#if !__NO_OUTPUT
						char szTime[26] = {0};
						time_t testTime = m_Timestamps[achievementId];
						struct tm* local = localtime(&testTime);
						safecpy(szTime, asctime(local));
						szTime[strlen(szTime) - 1] = 0;
						rlDebug3("UTC Timestamp for achievement %d from SC:%s, converted to local time:%s", achievementId, timestamp, szTime);
#endif
					}
				}

				if(m_Timestamps[achievementId] == 0)
				{
					m_AchievementsAwardedOffline->Set(achievementId);
				}
			}
		}

		return true;
	}
	rcatchall
	{
		resultCode = -1;
		return false;
	}
}

void rlRosReadAchievementsTask::ProcessError(const rlRosResult& result, int& UNUSED_PARAM(resultCode))
{
	Assertf(false, "rlRosReadAchievementsTask: ROS Returned Error: Code: %s, CodeEx: %s, Ctx: %s, Loc: %s, Msg: %s",
		result.m_Code, result.m_CodeEx, result.m_Ctx, result.m_Location, result.m_Msg);

	// 	*m_ResultInvitesCount = 0;
	// 	resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlRosWriteOfflineAchievementsTask
//////////////////////////////////////////////////////////////////////////
rlRosWriteOfflineAchievementsTask::rlRosWriteOfflineAchievementsTask()
{
}

rlRosWriteOfflineAchievementsTask::~rlRosWriteOfflineAchievementsTask()
{
}

const char* rlRosWriteOfflineAchievementsTask::GetServiceMethod() const
{
	return "achievements.asmx/AwardOfflineAchievements";
}

u64 rlRosWriteOfflineAchievementsTask::GetBitFieldAsScaler(AchievementId start, AchievementId end)
{
	Assert((end - start + 1) == 64);
	Assert(start > 0);
	Assert(end <= IAchievementManager::MAX_NUM_ACHIEVEMENTS);

	// ROS treats bit 0 as achievementId 1, so there is no achievementId 0
	// because of this, both achievementIds 0 and 256 are invalid. Only 1-255 are valid.
	if(end == IAchievementManager::MAX_NUM_ACHIEVEMENTS)
	{
		end -= 1;
	}

	u64 val = 0;
	for(AchievementId i = start; i <= end; i++)
	{
		if(m_OfflineAchievements->IsSet(i))
		{
			val |= ((u64)1 << (u64)(i - start));
		}
	}
	return val;
}

bool rlRosWriteOfflineAchievementsTask::Configure(void* /*ctx*/, AchievementBitSet* offlineAchievements)
{
	bool success = false;

	rtry
	{
		m_OfflineAchievements = offlineAchievements;

		rverify(rlRosHttpTask::Configure(0), catchall, );

		// Get credentials
		rlRosCredentials cred = rlRos::GetCredentials(0);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

		// convert bitset into format ROS wants
		rverify(AddUnsParameter("achievementIdBits1_64", GetBitFieldAsScaler(1, 64)), catchall, );
		rverify(AddUnsParameter("achievementIdBits65_128", GetBitFieldAsScaler(65, 128)), catchall, );
		rverify(AddUnsParameter("achievementIdBits129_192", GetBitFieldAsScaler(129, 192)), catchall, );
		rverify(AddUnsParameter("achievementIdBits193_256", GetBitFieldAsScaler(193, 256)), catchall, );

		rverify(AddStringParameter("crossTitleId", "", true), catchall, );
		rverify(AddStringParameter("crossTitleName", "", true), catchall, );
		rverify(AddStringParameter("crossTitlePlatformName", "", true), catchall, );

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

bool  rlRosWriteOfflineAchievementsTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
	resultCode = 0;

	Assert(node);

	return true;
}

void  rlRosWriteOfflineAchievementsTask::ProcessError(const rlRosResult& result, int& UNUSED_PARAM(resultCode))
{
	Assertf(false, "rlRosWriteOfflineAchievementsTask: ROS Returned Error: Code: %s, CodeEx: %s, Ctx: %s, Loc: %s, Msg: %s",
		result.m_Code, result.m_CodeEx, result.m_Ctx, result.m_Location, result.m_Msg);
}

///////////////////////////////////////////////////////////////////////////////
//  SyncAchievementsTask
///////////////////////////////////////////////////////////////////////////////
bool RgscAchievementManager::SyncAchievements(const RockstarId profileId, netStatus* status)
{
	RLPC_CREATETASK(SyncAchievementsTask, profileId, status);
}

SyncAchievementsTask::SyncAchievementsTask()
	: m_State(STATE_INVALID)
	, m_NeedBackendWrite(false)
	, m_Error(false)
{
}

bool SyncAchievementsTask::Configure(RgscAchievementManager* ctx, const RockstarId profileId)
{
	bool success = false;

	rtry
	{
		rverify(rlRos::IsOnline(0), catchall, );

		m_NeedBackendWrite = false;
		m_ProfileId = profileId;

		m_File.SetExportVersion(ctx->GetVersion());

		Assert(m_Ctx->IsSynchronized() == false);

		m_State = STATE_INVALID;

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void SyncAchievementsTask::Start()
{
	rlDebug2("Syncing achievements to/from online database...");

	this->TaskBase::Start();

	m_State = STATE_LOAD_FILE;
}

void SyncAchievementsTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("Syncing achievements to/from online database %s (%ums)", FinishString(finishType), GetElapsedTime());
	this->TaskBase::Finish(finishType, resultCode);
}

void SyncAchievementsTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
}

void SyncAchievementsTask::Update(const unsigned timeStep)
{
	this->TaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			// If we've locked the file, release it.
			if (m_State > STATE_LOAD_FILE)
			{
				RgscAchievementManager::sm_FileIoInProgress = false;
			}

			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_LOAD_FILE:
			{
				if (RgscAchievementManager::sm_FileIoInProgress)
				{
					// wait for file IO to finish
				}
				else if (LoadAchievementFile(&m_FileLoadWorker, &m_File, &m_MyStatus))
				{
					m_State = STATE_LOADING_FILE;
				}
				else
				{
					m_State = STATE_FAILED;
				}
			}
			break;
		case STATE_LOADING_FILE:
			if (m_MyStatus.Succeeded())
			{
				m_State = STATE_READ_RECORD;
			}
			else if (!m_MyStatus.Pending())
			{
				RgscAchievementManager::sm_FileIoInProgress = false;
				m_State = STATE_FAILED;
			}
			break;
		case STATE_READ_RECORD:
			if(this->ReadRecord())
			{
				m_State = STATE_READING_RECORD;
			}
			else
			{
				m_Error = true;
				m_State = STATE_SAVE_FILE;
			}
			break;

		case STATE_READING_RECORD:
			if(m_MyStatus.Succeeded())
			{
				m_State = STATE_MERGE_ACHIEVEMENTS;
			}
			else if(!m_MyStatus.Pending())
			{
				m_Error = true;
				m_State = STATE_SAVE_FILE;
			}
			break;
		case STATE_MERGE_ACHIEVEMENTS:
			if(this->MergeAchievements())
			{
				if(m_NeedBackendWrite)
				{
					m_State = STATE_WRITE_RECORD;
				}
				else
				{
					m_State = STATE_SAVE_FILE;
				}
			}
			else
			{
				m_Error = true;
				m_State = STATE_SAVE_FILE;
			}
			break;

		case STATE_WRITE_RECORD:
			if(this->WriteRecord())
			{
				m_State = STATE_WRITING_RECORD;
			}
			else
			{
				m_Error = true;
				m_State = STATE_SAVE_FILE;
			}
			break;
		case STATE_WRITING_RECORD:
			if(m_MyStatus.Succeeded())
			{
				m_State = STATE_SAVE_FILE;
			}
			else if(!m_MyStatus.Pending())
			{
				m_Error = true;
				m_State = STATE_SAVE_FILE;
			}
			break;
		case STATE_SAVE_FILE:
			if (SaveAchievementFile(&m_FileSaveWorker, &m_File, &m_MyStatus))
			{
				m_State = STATE_SAVING_FILE;
			}
			else
			{
				RgscAchievementManager::sm_FileIoInProgress = false;
				rlDebug3("SyncAchievementsTask: achievement file failed open");
				m_State = STATE_FAILED;
			}
			break;
		case STATE_SAVING_FILE:
			if (m_MyStatus.Succeeded())
			{
				m_State = m_Error ? STATE_FAILED : STATE_SUCCEEDED;
			}
			else if (!m_MyStatus.Pending())
			{
				m_State = STATE_FAILED;
			}
			break;
		case STATE_SUCCEEDED:
			m_Ctx->SetSynchronized(true);
			this->Finish(FINISH_SUCCEEDED);
			break;

		case STATE_FAILED:
			m_Ctx->SetSynchronized(false);
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive() && !RgscAchievementManager::sm_FileIoInProgress);
}

bool SyncAchievementsTask::ReadRecord()
{
	netStatus* status = &m_MyStatus;
	RLPC_CREATETASK(rlRosReadAchievementsTask, m_ProfileId, &m_AchievementsFromRos, &m_AchievementsMissingTimestamps, m_Timestamps, status);
}

bool SyncAchievementsTask::MergeAchievements()
{
	bool success = false;

	rtry
	{
		rverify(m_File.IsOpen(), catchall, );

		// we have to be careful to include only those achievements which have not
		// been previously awarded according to ROS. Any duplicates will result
		// in the AwardOfflineAchievements operation failing.
		const AchievementBitSet& fileSet = m_File.GetAchievementBitSet();

		for(AchievementId i = 0; i < IAchievementManager::MAX_NUM_ACHIEVEMENTS; i++)
		{
			if(fileSet.IsSet(i) && m_AchievementsFromRos.IsSet(i) == false)
			{
				m_OfflineAchievementsToWrite.Set(i);
				m_NeedBackendWrite = true;
			}
		}

		// make sure that any awarded achievements not recorded online are marked in the local file as 'awarded offline'
		bool changed = false;
		m_File.MergeAwardedOfflineBitSets(m_OfflineAchievementsToWrite, changed);
		if(changed)
		{
			rlDebug("there were achievements in our local file that we thought were awarded online, but weren't recorded online. Marking as awarded offline in our local file.");
		}

		changed = false;
		rverify(m_File.MergeAchievementBitSets(m_AchievementsFromRos, changed), catchall, );
		if(changed)
		{
			rlDebug("there were achievements from the online database that weren't recorded in our local file");
		}

		changed = false;
		m_File.MergeAwardedOfflineBitSets(m_AchievementsMissingTimestamps, changed);
		if(changed)
		{
			rlDebug("there were awarded offline achievements from the online database that weren't recorded in our local file");
		}

		// write the server's timestamps to the local file
		for(int i = 0; i < IAchievementManager::MAX_NUM_ACHIEVEMENTS; i++)
		{
			if(m_Timestamps[i] > 0)
			{
				if(AssertVerify(m_File.GetAchievementBitSet().IsSet(i)))
				{
					AssertVerify(m_File.SetAchievementTimestamp(i, m_Timestamps[i]));
				}
			}
		}

		if(m_NeedBackendWrite)
		{
			rlDebug("there were achievements in our local file that weren't recorded in the online database");
		}

		success = true;
	}
	rcatchall
	{

	}
	return success;
}

bool SyncAchievementsTask::WriteRecord()
{
	netStatus* status = &m_MyStatus;
	RLPC_CREATETASK(rlRosWriteOfflineAchievementsTask, &m_OfflineAchievementsToWrite, status);
}

///////////////////////////////////////////////////////////////////////////////
//  RaiseAchievementAwardedEventTask
///////////////////////////////////////////////////////////////////////////////
bool RgscAchievementManager::RaiseAchievementAwardedEvent(const IAchievement* achievement, netStatus* status)
{
	RLPC_CREATETASK(RaiseAchievementAwardedEventTask, achievement, status);
}

RaiseAchievementAwardedEventTask::RaiseAchievementAwardedEventTask()
	: m_State(STATE_INVALID)
	, m_Achievement(NULL)
{
}

bool RaiseAchievementAwardedEventTask::Configure(RgscAchievementManager* /*ctx*/, const IAchievement* achievement)
{
	bool success = false;

	rtry
	{
		rverify(achievement != NULL, catchall, );

		// determine which version of the achievement interface we are working with
		Achievement* v1 = NULL;
		RGSC_HRESULT hr = ((IAchievement*)achievement)->QueryInterface(IID_AchievementV1, (void**) &v1);

		rverify(SUCCEEDED(hr) && (v1 != NULL), catchall, );
		rverify(v1->IsValid(), catchall, );

		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

		m_Achievement = achievement;

		m_State = STATE_INVALID;

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void RaiseAchievementAwardedEventTask::Start()
{
	rlDebug2("Raising achievement awarded event...");

	this->TaskBase::Start();

	m_State = STATE_RAISE_EVENT;
}

void RaiseAchievementAwardedEventTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("Raising achievement awarded event %s (%ums)", FinishString(finishType), GetElapsedTime());

	this->TaskBase::Finish(finishType, resultCode);
}

void RaiseAchievementAwardedEventTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
}

void RaiseAchievementAwardedEventTask::Update(const unsigned timeStep)
{
	this->TaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_RAISE_EVENT:
			if(this->RaiseEvent())
			{
				this->Finish(FINISH_SUCCEEDED);
			}
			else
			{
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			}
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool RaiseAchievementAwardedEventTask::RaiseEvent()
{
	bool success = false;

	rtry
	{
		// now convert to json
		JSONNODE* rootNode = json_new(JSON_NODE);
		rverify(rootNode, catchall, );

		json_push_back(rootNode, json_new_i("Count", 1));

		JSONNODE* notificationArray = json_new(JSON_ARRAY);
		rverify(notificationArray, catchall, );
		json_set_name(notificationArray, "Notifications");

		JSONNODE* notificationNode = json_new(JSON_NODE);
		rverify(notificationNode, catchall, );

		json_push_back(notificationNode, json_new_a("Type", "Achievements"));
		json_push_back(notificationNode, json_new_i("Count", 1));

		JSONNODE* achievementArray = json_new(JSON_ARRAY);
		rverify(achievementArray, catchall, );

		json_set_name(achievementArray, "Data");

		JSONNODE* achievementNode = json_new(JSON_NODE);
		rverify(achievementNode, catchall, );

		json_set_name(achievementNode, "Achievement");
		rverify(m_Ctx->GetAchievementAsJson(m_Achievement, achievementNode), catchall, );

		json_push_back(achievementArray, achievementNode);

		json_push_back(notificationNode, achievementArray);

		json_push_back(notificationArray, notificationNode);

		json_push_back(rootNode, notificationArray);

		json_char* jc = json_write_formatted(rootNode);

#if !__NO_OUTPUT
		size_t len = strlen(jc);
		for(size_t i = 0; i < len; i++)
		{
			printf("%c", jc[i]);
		}
#endif

		json_delete(rootNode);

		GetRgscConcreteInstance()->_GetUiInterface()->SendNotification(jc);

		json_free_safe(jc);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

//////////////////////////////////////////////////////////////////////////
// rlRosWriteAchievementsTask
//////////////////////////////////////////////////////////////////////////
rlRosWriteAchievementsTask::rlRosWriteAchievementsTask()
{
}

rlRosWriteAchievementsTask::~rlRosWriteAchievementsTask()
{
}

const char* rlRosWriteAchievementsTask::GetServiceMethod() const
{
	return "achievements.asmx/AwardAchievement";
}

bool rlRosWriteAchievementsTask::Configure(void* /*ctx*/, AchievementId achievementId, time_t* timestamp)
{
	bool success = false;

	rtry
	{
		m_AchievementId = achievementId;
		m_Timestamp = timestamp;

		rverify(m_Timestamp, catchall, );
		*m_Timestamp = 0;

		rverify(rlRosHttpTask::Configure(0), catchall, );

		rlRosCredentials cred = rlRos::GetCredentials(0);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

		rverify(AddIntParameter("achievementId", m_AchievementId), catchall, );
		rverify(AddIntParameter("achievedOffline", 0), catchall, );

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

bool  rlRosWriteAchievementsTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
	// <Result DateAchieved="2011-07-04T19:32:12.573" />

	resultCode = 0;

	Assert(node);

	rtry
	{
		const parTreeNode* results = node->FindChildWithName("Result");
		rcheck(results, catchall, rlTaskError("Failed to find <Result> element"));

		*m_Timestamp = 0;

		const char* timestamp = rlHttpTaskHelper::ReadString(results, NULL, "DateAchieved");
		rverify(timestamp && (timestamp[0] != '\0'), catchall, rlError("Failed to find <Result.DateAchieved> attribute"));

		tm t;
		rverify(RgscAchievementManager::ConvertRosTimeStringToTmStruct(t, timestamp), catchall, );

		*m_Timestamp = _mkgmtime(&t);

#if !__NO_OUTPUT
		char szTime[26] = {0};
		time_t testTime = *m_Timestamp;
		struct tm* local = localtime(&testTime);
		safecpy(szTime, asctime(local));
		szTime[strlen(szTime) - 1] = 0;
		rlDebug3("UTC Timestamp for achievement %d from SC:%s, converted to local time:%s", m_AchievementId, timestamp, szTime);
#endif

		return true;
	}
	rcatchall
	{
		resultCode = -1;
		return false;
	}
}

void rlRosWriteAchievementsTask::ProcessError(const rlRosResult& result, int& UNUSED_PARAM(resultCode))
{
	Assertf(false, "rlRosWriteAchievementsTask: ROS Returned Error: Code: %s, CodeEx: %s, Ctx: %s, Loc: %s, Msg: %s",
		result.m_Code, result.m_CodeEx, result.m_Ctx, result.m_Location, result.m_Msg);

	// 	*m_ResultInvitesCount = 0;
	// 	resultCode = ConvertRosResultToClanError(result);
}

//////////////////////////////////////////////////////////////////////////
// rlRosWriteAchievementProgressTask
//////////////////////////////////////////////////////////////////////////
bool RgscAchievementManager::WriteSingleAchievementProgress(const AchievementId achievementId, const s64 progress, netStatus* status)
{
	RLPC_CREATETASK(rlRosWriteAchievementProgressTask, achievementId, progress, status);
}

rlRosWriteAchievementProgressTask::rlRosWriteAchievementProgressTask()
{
}

rlRosWriteAchievementProgressTask::~rlRosWriteAchievementProgressTask()
{
}

const char* rlRosWriteAchievementProgressTask::GetServiceMethod() const
{
	return "achievements.asmx/AwardAchievementProgress";
}

bool rlRosWriteAchievementProgressTask::Configure(void* /*ctx*/, AchievementId achievementId, s64 progress)
{
	bool success = false;

	rtry
	{
		rverify(achievementId < IAchievementManager::MAX_NUM_ACHIEVEMENTS, catchall, );

		m_AchievementId = achievementId;
		m_Progress = progress;

		rverify(rlRosHttpTask::Configure(0), catchall, );

		rlRosCredentials cred = rlRos::GetCredentials(0);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

		rverify(AddIntParameter("achievementId", m_AchievementId), catchall, );
		rverify(AddIntParameter("achievementProgress", progress), catchall, );
		rverify(AddIntParameter("achievedOffline", 0), catchall, );

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

bool rlRosWriteAchievementProgressTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* node, int& resultCode)
{
	// <Result DateAchieved="2011-07-04T19:32:12.573" />

	resultCode = 0;

	Assert(node);

	rtry
	{
		const parTreeNode* results = node->FindChildWithName("Result");
		rcheck(results, catchall, rlTaskError("Failed to find <Result> element"));

		return true;
	}
	rcatchall
	{
		resultCode = -1;
		return false;
	}
}

void rlRosWriteAchievementProgressTask::ProcessError(const rlRosResult& result, int& UNUSED_PARAM(resultCode))
{
	Assertf(false, "rlRosWriteAchievementsTask: ROS Returned Error: Code: %s, CodeEx: %s, Ctx: %s, Loc: %s, Msg: %s",
		result.m_Code, result.m_CodeEx, result.m_Ctx, result.m_Location, result.m_Msg);

	// 	*m_ResultInvitesCount = 0;
	// 	resultCode = ConvertRosResultToClanError(result);
}

///////////////////////////////////////////////////////////////////////////////
//  WriteSingleAchievementTask
///////////////////////////////////////////////////////////////////////////////
bool RgscAchievementManager::WriteSingleAchievement(const AchievementId achievementId, netStatus* status)
{
	RLPC_CREATETASK(WriteSingleAchievementTask, achievementId, status);
}

WriteSingleAchievementTask::WriteSingleAchievementTask()
	: m_State(STATE_INVALID)
	, m_AchievementId(0)
	, m_bError(false)
{
}

bool  WriteSingleAchievementTask::Configure(RgscAchievementManager* ctx, const AchievementId achievementId)
{
	bool success = false;

	rtry
	{
		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal(), catchall, );
		rverify(achievementId < IAchievementManager::MAX_NUM_ACHIEVEMENTS, catchall, );

		m_AchievementId = achievementId;
		m_Timestamp = 0;

		m_File.SetExportVersion(ctx->GetVersion());

		m_State = STATE_INVALID;

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void WriteSingleAchievementTask::Start()
{
	rlDebug2("Writing Achievement to online database...");

	this->TaskBase::Start();

	m_State = STATE_LOAD_FILE;
}

void WriteSingleAchievementTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("Writing Achievement to online database %s (%ums)", FinishString(finishType), GetElapsedTime());

	this->TaskBase::Finish(finishType, resultCode);
}

void WriteSingleAchievementTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
}

void WriteSingleAchievementTask::Update(const unsigned timeStep)
{
	this->TaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			// If we've locked the file, release it.
			if (m_State > STATE_LOAD_FILE)
			{
				RgscAchievementManager::sm_FileIoInProgress = false;
			}

			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_LOAD_FILE:
			{
				if (RgscAchievementManager::sm_FileIoInProgress)
				{
					// wait for file IO to finish
				}
				else if (LoadAchievementFile(&m_FileLoadWorker, &m_File, &m_MyStatus))
				{
					m_State = STATE_LOADING_FILE;
				}
				else
				{
					this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
				}
			}
			break;
		case STATE_LOADING_FILE:
			if (m_MyStatus.Succeeded())
			{
				m_State = STATE_WRITE;
			}
			else if (!m_MyStatus.Pending())
			{
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			}
			break;
		case STATE_WRITE:
			if(this->WriteAchievement())
			{
				m_State = STATE_WRITING;
			}
			else
			{
				m_bError = true;
				m_State = STATE_SAVE_FILE;
			}
			break;
		case STATE_WRITING:
			if(m_MyStatus.Succeeded())
			{
				m_State = STATE_CACHE_RESULTS;
			}
			else if(!m_MyStatus.Pending())
			{
				m_bError = true;
				m_State = STATE_SAVE_FILE;
			}
			break;
		case STATE_CACHE_RESULTS:
			if(this->CacheResults())
			{
				m_State = STATE_SAVE_FILE;
			}
			else
			{
				// even if caching fails (m_bError), we still succeeded at writing the achievement to the backend
				// m_bError = false
				m_State = STATE_SAVE_FILE;
			}
			break;
		case STATE_SAVE_FILE:
			if (SaveAchievementFile(&m_FileSaveWorker, &m_File, &m_MyStatus))
			{
				m_State = STATE_SAVING_FILE;
			}
			else
			{
				RgscAchievementManager::sm_FileIoInProgress = false;
				rlDebug3("WriteSingleAchievementTask: achievement file failed open");
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			}
			break;
		case STATE_SAVING_FILE:
			if (!m_MyStatus.Pending())
			{
				if (m_bError)
				{
					this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
				}
				else
				{
					this->Finish(FINISH_SUCCEEDED);
				}
			}
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive() && !RgscAchievementManager::sm_FileIoInProgress);
}

bool WriteSingleAchievementTask::WriteAchievement()
{
	netStatus* status = &m_MyStatus;
	RLPC_CREATETASK(rlRosWriteAchievementsTask, m_AchievementId, &m_Timestamp, status);
}

bool WriteSingleAchievementTask::CacheResults()
{
	bool success = false;

	rtry
	{
		rverify(m_File.IsOpen(), catchall, );

		if(AssertVerify(m_File.GetAchievementBitSet().IsSet(m_AchievementId)))
		{
			if(AssertVerify(m_Timestamp > 0))
			{
				AssertVerify(m_File.SetAchievementTimestamp(m_AchievementId, m_Timestamp));
			}
		}

		success = true;
	}
	rcatchall
	{
		
	}

	return success;
}

///////////////////////////////////////////////////////////////////////////////
//  ReadAchievementsTask
///////////////////////////////////////////////////////////////////////////////
bool RgscAchievementManager::ReadAchievements(const RockstarId profileId, const IAchievement::DetailFlags detailFlags, IAchievementList* achievements, netStatus* status)
{
	RLPC_CREATETASK(ReadAchievementsTask,
		profileId,
		detailFlags,
		achievements,
		status);
}

ReadAchievementsTask::ReadAchievementsTask()
	: m_State(STATE_INVALID)
	, m_Error(false)
{
	rlDebug2("sizeof(ReadAchievementsTask): %d", sizeof(ReadAchievementsTask));
}

bool ReadAchievementsTask::Configure(RgscAchievementManager* ctx, RockstarId profileId, const IAchievement::DetailFlags detailFlags, IAchievementList* achievements)
{
	bool success = false;

	rtry
	{
		m_File.SetExportVersion(ctx->GetVersion());

		// we can read achievements for the local player even when offline (using the local cache),
		// but we can read achievements for remote players only when we're online
		m_ProfileId = profileId;
		RockstarId localProfileId = GetRgscConcreteInstance()->_GetProfileManager()->GetSignedInProfile().GetProfileId();
		m_RequesterIsLocalGamer = localProfileId == profileId;
		bool isSignedIn = GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal();
		bool isOnline = GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal();

		rverify((m_RequesterIsLocalGamer && isSignedIn) || isOnline, catchall, );
		rverify(achievements != NULL, catchall, );

		AchievementListV1* listV1 = NULL;
		RGSC_HRESULT hr = achievements->QueryInterface(IID_IAchievementListV1, (void**) &listV1);
		rverify(SUCCEEDED(hr) && (listV1 != NULL), catchall, );

		rverify(listV1->GetNumAchievements() > 0, catchall, );
		rverify((detailFlags & IAchievement::ACHIEVEMENT_DETAILS_IS_AWARDED) ||
			(detailFlags & IAchievement::ACHIEVEMENT_DETAILS_TIMESTAMP), catchall, );

		m_DetailFlags = detailFlags;

		m_Achievements = achievements;
		m_NumAwardedAchievements = 0;
		m_NeedToQueryRos = false;

		m_AchievementsMissingTimestamps.Clear();

		m_State = STATE_INVALID;

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void ReadAchievementsTask::Start()
{
	rlDebug2("Reading achievements...");

	this->TaskBase::Start();

	m_State = STATE_LOAD_FILE;
}

void ReadAchievementsTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("Reading achievements %s (%ums)", FinishString(finishType), GetElapsedTime());

	this->TaskBase::Finish(finishType, resultCode);
}

void ReadAchievementsTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
}

void ReadAchievementsTask::Update(const unsigned timeStep)
{
	this->TaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			// If we've locked the file, release it.
			if (m_State > STATE_LOAD_FILE)
			{
				RgscAchievementManager::sm_FileIoInProgress = false;
			}

			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_LOAD_FILE:
			{
				if (RgscAchievementManager::sm_FileIoInProgress)
				{
					// wait for file IO to finish
				}
				else if (LoadAchievementFile(&m_FileLoadWorker, &m_File, &m_MyStatus))
				{
					m_State = STATE_LOADING_FILE;
				}
				else
				{
					this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
				}
			}
			break;
		case STATE_LOADING_FILE:
			if (m_MyStatus.Succeeded())
			{
				m_State = STATE_BEGIN;
			}
			else if (!m_MyStatus.Pending())
			{
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			}
			break;
		case STATE_BEGIN:
			if(m_RequesterIsLocalGamer)
			{
				m_State = STATE_PROCESS_RESULTS_FROM_CACHE;
			}
			else
			{
				m_State = STATE_READ_FROM_DATABASE;
			}
			break;

		case STATE_PROCESS_RESULTS_FROM_CACHE:
			if(this->ProcessResultsFromCache())
			{
				if(m_NeedToQueryRos)
				{
					m_State = STATE_READ_FROM_DATABASE;
				}
				else
				{
					m_State = STATE_SAVE_FILE;
				}
			}
			else
			{
				m_State = STATE_READ_FROM_DATABASE;
			}
			break;
		case STATE_READ_FROM_DATABASE:
			if(this->ReadAchievements())
			{
				m_State = STATE_READING_FROM_DATABASE;
			}
			else
			{
				m_Error = true;
				m_State = STATE_SAVE_FILE;
			}
			break;

		case STATE_READING_FROM_DATABASE:
			if(m_MyStatus.Succeeded())
			{
				m_State = STATE_PROCESS_RESULTS_FROM_DATABASE;
			}
			else if(!m_MyStatus.Pending())
			{
				m_Error = true;
				m_State = STATE_SAVE_FILE;
			}
			break;

		case STATE_PROCESS_RESULTS_FROM_DATABASE:
			if(this->ProcessResultsFromDatabase())
			{
				m_State = STATE_CACHE_RESULTS;
			}
			else
			{
				m_Error = true;
				m_State = STATE_SAVE_FILE;;
			}
			break;

		case STATE_CACHE_RESULTS:
			if(this->CacheResults())
			{
				this->Finish(FINISH_SUCCEEDED);
			}
			else
			{
				// even if caching fails, we still succeeded at reading all the data
				this->Finish(FINISH_SUCCEEDED);
			}
			break;
		case STATE_SAVE_FILE:
			if (SaveAchievementFile(&m_FileSaveWorker, &m_File, &m_MyStatus))
			{
				m_State = STATE_SAVING_FILE;
			}
			else
			{
				RgscAchievementManager::sm_FileIoInProgress = false;
				rlDebug3("ReadAchievementsTask: achievement file failed open");

				if (m_Error)
				{
					this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
				}
				else
				{
					this->Finish(FINISH_SUCCEEDED);
				}
			}
			break;
		case STATE_SAVING_FILE:
			if (!m_MyStatus.Pending())
			{
				// even if caching fails, we still succeeded at reading all the data
				if (m_Error)
				{
					this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
				}
				else
				{
					this->Finish(FINISH_SUCCEEDED);
				}
			}
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive() && !RgscAchievementManager::sm_FileIoInProgress);
}

bool ReadAchievementsTask::ReadAchievements()
{
	netStatus* status = &m_MyStatus;
	RLPC_CREATETASK(rlRosReadAchievementsTask, m_ProfileId, &m_AchievementsFromRos, &m_AchievementsMissingTimestamps, m_Timestamps, status);
}

bool ReadAchievementsTask::ProcessResults(const AchievementBitSet &bits, time_t (&timestamps)[IAchievementManager::MAX_NUM_ACHIEVEMENTS])
{
	bool success = false;

	rtry
	{
		AchievementListV1* listV1 = NULL;
		RGSC_HRESULT hr = m_Achievements->QueryInterface(IID_IAchievementListV1, (void**) &listV1);
		rverify(SUCCEEDED(hr) && (listV1 != NULL), catchall, );

		success = true;

		for(u32 i = 0; i < listV1->GetNumAchievements(); i++)
		{
			Achievement* v1 = listV1->GetAchievement(i);

			bool isAwarded = bits.IsSet(v1->GetId());
			v1->SetAchieved(isAwarded);
			v1->SetTimestamp(timestamps[v1->GetId()]);
		}
	}
	rcatchall
	{

	}

	return success;
}

bool ReadAchievementsTask::ProcessResultsFromCache()
{
	bool success = false;

	rtry
	{
		rverify(m_File.IsOpen(), catchall, );
		const AchievementBitSet &bits = m_File.GetAchievementBitSet();
		const AchievementBitSet &offlineBits = m_File.GetAwardedOfflineBitSet();

		// determine which version of the achievement list interface we are working with
		AchievementListV1* listV1 = NULL;
		RGSC_HRESULT hr = m_Achievements->QueryInterface(IID_IAchievementListV1, (void**) &listV1);
		rverify(SUCCEEDED(hr) && (listV1 != NULL), catchall, );

		for(u32 i = 0; i < listV1->GetNumAchievements(); i++)
		{
			Achievement* v1 = listV1->GetAchievement(i);
			bool isAwarded = bits.IsSet(v1->GetId());
			v1->SetAchieved(isAwarded);

			if(isAwarded)
			{
				time_t timestamp = m_File.GetAchievementTimestamp(v1->GetId());
				if(timestamp > 0)
				{
					// use the cached timestamp
					rlDebug2("using cached timestamp for achievement id %d", v1->GetId());
					v1->SetTimestamp(timestamp);
				}
				else
				{
					bool offline = offlineBits.IsSet(v1->GetId());
					if(offline)
					{
						rlDebug2("achievement id %d was awarded offline so there is no timestamp to query", v1->GetId());
					}
					else
					{
						Assertf(false, "we don't have the timestamp cached locally for achievement %d which was awarded online.", v1->GetId());
						//rlDebug2("we don't have the timestamp cached locally for awarded achievement %d. Will query the online database.", v1->GetId());
						//m_NeedToQueryRos = true;
					}
				}
			}
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool ReadAchievementsTask::ProcessResultsFromDatabase()
{
	bool success = false;

	rtry
	{
		rverify(ProcessResults(m_AchievementsFromRos, m_Timestamps), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool ReadAchievementsTask::CacheResults()
{
	bool success = false;

	rtry
	{
		// we currently only cache results for the local player
		if(m_RequesterIsLocalGamer)
		{
			rverify(m_File.IsOpen(), catchall, );

			// determine which version of the achievement list interface we are working with
			AchievementListV1* listV1 = NULL;
			RGSC_HRESULT hr = m_Achievements->QueryInterface(IID_IAchievementListV1, (void**) &listV1);
			rverify(SUCCEEDED(hr) && (listV1 != NULL), catchall, );

			for(u32 i = 0; i < listV1->GetNumAchievements(); i++)
			{
				Achievement* v1 = listV1->GetAchievement(i);
				if(v1->IsAchieved())
				{
					time_t timestamp = v1->GetAchievedTime();
					if(timestamp > 0)
					{
						AssertVerify(m_File.SetAchievementTimestamp(v1->GetId(), timestamp));
					}
				}
			}

			bool changed = false;
			m_File.MergeAwardedOfflineBitSets(m_AchievementsMissingTimestamps, changed);
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

#if __DEV
//////////////////////////////////////////////////////////////////////////
// rlRosDeleteAchievementsTask
//////////////////////////////////////////////////////////////////////////
rlRosDeleteAchievementsTask::rlRosDeleteAchievementsTask()
{
}

rlRosDeleteAchievementsTask::~rlRosDeleteAchievementsTask()
{
}

const char* rlRosDeleteAchievementsTask::GetServiceMethod() const
{
	return "achievements.asmx/AdminClearPlayerAchievements";
}

bool rlRosDeleteAchievementsTask::Configure(void* /*ctx*/)
{
	bool success = false;

	rtry
	{
		rverify(rlRosHttpTask::Configure(0), catchall, );

		// Get credentials
		rlRosCredentials cred = rlRos::GetCredentials(0);
		rverify(AddStringParameter("ticket", cred.GetTicket(), true), catchall, );

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

bool rlRosDeleteAchievementsTask::ProcessSuccess(const rlRosResult& /*result*/, const parTreeNode* UNUSED_PARAM(node), int& resultCode)
{
	resultCode = 0;
	return true;
}

void rlRosDeleteAchievementsTask::ProcessError(const rlRosResult& result, int& UNUSED_PARAM(resultCode))
{
	Assertf(false, "rlRosDeleteAchievementsTask: ROS Returned Error: Code: %s, CodeEx: %s, Ctx: %s, Loc: %s, Msg: %s",
		result.m_Code, result.m_CodeEx, result.m_Ctx, result.m_Location, result.m_Msg);
}

bool RgscAchievementManager::DeleteAllAchievements(netStatus* status)
{
	RLPC_CREATETASK(DeleteAllAchievementsTask,
		status);
}

DeleteAllAchievementsTask::DeleteAllAchievementsTask()
	: m_State(STATE_INVALID)
{
}

bool DeleteAllAchievementsTask::Configure(RgscAchievementManager* /*ctx*/)
{
	bool success = false;

	rtry
	{
		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedIn(), catchall, );

		m_State = STATE_INVALID;

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void DeleteAllAchievementsTask::Start()
{
	rlDebug2("Deleting achievements from online database...");

	this->TaskBase::Start();

	m_State = STATE_DELETE;
}

void DeleteAllAchievementsTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("Deleting achievements from online database %s (%ums)", FinishString(finishType), GetElapsedTime());
	this->TaskBase::Finish(finishType, resultCode);
}

void DeleteAllAchievementsTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
}

void DeleteAllAchievementsTask::Update(const unsigned timeStep)
{
	this->TaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_DELETE:
			if(this->Delete())
			{
				m_State = STATE_DELETING;
			}
			else
			{
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			}
			break;

		case STATE_DELETING:
			if(m_MyStatus.Succeeded())
			{
				this->Finish(FINISH_SUCCEEDED);
			}
			else if(!m_MyStatus.Pending())
			{
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			}
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool DeleteAllAchievementsTask::Delete()
{
	netStatus* status = &m_MyStatus;
	RLPC_CREATETASK(rlRosDeleteAchievementsTask, status);
}

#endif // __DEV

///////////////////////////////////////////////////////////////////////////////
// AchievementFileLoadWorker
///////////////////////////////////////////////////////////////////////////////
AchievementFileLoadWorker::AchievementFileLoadWorker()
	: m_File(NULL)
	, m_Status(NULL)
{
}

bool AchievementFileLoadWorker::Configure(AchievementsFile* file, netStatus* status)
{
	bool success = false;

	rtry
	{
		rverify(file, catchall, );
		rverify(status, catchall, );

		m_File = file;
		m_Status = status;

		success = true;
	}
	rcatchall
	{
		success = false;
	}

	return success;
}

bool AchievementFileLoadWorker::Start(const int cpuAffinity)
{
	bool success = this->rlWorker::Start("[RGSC] Achievements Load", sysIpcMinThreadStackSize, cpuAffinity, true);
	return success;
}

void AchievementFileLoadWorker::Perform()
{
	bool success = m_File->Open();
	if (success)
	{
		m_Status->SetSucceeded(0);
	}
	else 
	{
		rlError("Failed to open achievements file");
		m_Status->SetFailed(-1);
	}
}

///////////////////////////////////////////////////////////////////////////////
// AchievementFileSaveWorker
///////////////////////////////////////////////////////////////////////////////
AchievementFileSaveWorker::AchievementFileSaveWorker()
	: m_File(NULL)
	, m_Status(NULL)
{
}

bool AchievementFileSaveWorker::Configure(AchievementsFile* file, netStatus* status)
{
	bool success = false;

	rtry
	{
		rverify(file, catchall, );
		rverify(status, catchall, );

		m_File = file;
		m_Status = status;

		success = true;
	}
	rcatchall
	{
		success = false;
	}

	return success;
}

bool AchievementFileSaveWorker::Start(const int cpuAffinity)
{
	bool success = this->rlWorker::Start("[RGSC] Achievements Save", sysIpcMinThreadStackSize, cpuAffinity, true);
	return success;
}

void AchievementFileSaveWorker::Perform()
{
	if(m_File->IsDirty())
	{
		m_File->Write();
	}

	m_File->Close();

	RgscAchievementManager::sm_FileIoInProgress = false;
	m_Status->SetSucceeded(0);

	rlDebug3("AchievementFileSaveWorker: achievement file saved and closed");
}

} // namespace rage
