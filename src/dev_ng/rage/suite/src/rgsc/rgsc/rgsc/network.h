// 
// rgsc/network.h 
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RGSC_NETWORK_H 
#define RGSC_NETWORK_H

// rgsc includes
#include "network_interface.h"

// rage includes
#include "net/netaddress.h"

namespace rgsc
{
	// ===============================================================================================
	// INetwork
	// ===============================================================================================
	class RgscNetwork : public INetworkLatestVersion
	{
	public:
		// ===============================================================================================
		// inherited from IRgscUnknown
		// ===============================================================================================
		virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

		// ===============================================================================================
		// inherited from INetworkV1
		// ===============================================================================================
		virtual void RGSC_CALL SetNetworkInfo(INetworkInfo* info);
		virtual void RGSC_CALL SetShowNatTypeWarning(bool showStrictNatWarning);

		// ===============================================================================================
		// accessible from anywhere in the DLL
		// ===============================================================================================
		RgscNetwork();
		virtual ~RgscNetwork();

		bool Init();
		void Shutdown();
		void Update();

		const char* GetNetworkInfoAsJson();

	private:
		bool m_NetworkInfoIsValid;
		INetworkInfoV1::NatDetectionState m_NatDetectionState;
		INetworkInfoV1::UpNpState m_UpNpState;
		INetworkInfoV2::PcpState m_PcpState;
		netSocketAddress m_PublicAddr;
		netSocketAddress m_PrivateAddr;
		bool m_ShowNatTypeWarning;
	};

} // namespace rgsc

#endif // RGSC_NETWORK_H 
