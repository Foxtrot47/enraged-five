#include "rgsc.h"

#if RSG_PC

#include "diag/output.h"

#if __64BIT
#if __DEV && !__OPTIMIZED
#pragma comment(lib, "statLibJsond_x64.lib") 
#else
#pragma comment(lib, "statLibJson_x64.lib") 
#endif
#else
#if __DEV && !__OPTIMIZED
#pragma comment(lib, "statLibJsond.lib") 
#else
#pragma comment(lib, "statLibJson.lib") 
#endif
#endif

//When 1, this tracks number of allocations and sizes, including highwater marks.
//This generates a huge amount of log output, though.
#define RL_PC_JSON_DEBUG_MEM 0 //__DEV

//Makes libjson allocations go through the allocator passed to rlInit().
//When 0, it uses the global heap allocator.
#define RL_PC_JSON_USE_RL_ALLOCATOR 1

using namespace rage;

namespace rgsc
{

#if __RAGE_DEPENDENCY
RAGE_DEFINE_SUBCHANNEL(rgsc_dbg, json);
#undef __rage_channel
#define __rage_channel rgsc_dbg_json
#endif

#if RL_PC_JSON_DEBUG_MEM
static int s_NumAllocs = 0;
static int s_MaxAllocs = 0;
static s64 s_BytesAllocated = 0;
static s64 s_MaxBytesAllocated = 0;
static sysCriticalSectionToken s_JsonDebugMemCs;
#endif

#if RL_PC_JSON_USE_RL_ALLOCATOR
extern sysMemAllocator* g_RgscAllocator;
#endif

static void * libjsonAlloc(unsigned long size)
{
#if RL_PC_JSON_USE_RL_ALLOCATOR
	Assert(g_RgscAllocator);
	u8* new_p = (u8*)RGSC_ALLOCATE(rlpc, size);

	// if we don't have enough available memory in the 
	// rgsc heap then fall back to the global heap.
	if(!new_p)
	{
		RL_RAGE_TRACK(rlpc);
		new_p = rage_new u8[size];

#if RL_PC_JSON_DEBUG_MEM
		rlDebug("Allocated %u bytes from global heap (rgsc's largest block is only %u bytes)", size, g_RgscAllocator->GetLargestAvailableBlock());
#endif
	}

#else
	RL_RAGE_TRACK(rlpc);
	u8* new_p = rage_new u8[size];
#endif

#if RL_PC_JSON_DEBUG_MEM
	if(new_p)
	{
		SYS_CS_SYNC(s_JsonDebugMemCs);

		++s_NumAllocs;

		if (s_NumAllocs > s_MaxAllocs)
		{
			s_MaxAllocs = s_NumAllocs;
		}

#if RL_PC_JSON_USE_RL_ALLOCATOR
		size_t s = rgscGetAllocSize(new_p);
#else
		size_t s = sysMemAllocator::GetCurrent().GetSize(new_p);
#endif
		Assert(s);

		s_BytesAllocated += s;

		if(s_BytesAllocated > s_MaxBytesAllocated)
		{
			s_MaxBytesAllocated = s_BytesAllocated;
		}

		rlDebug("Json Allocated %u bytes (allocs=%d  maxallocs=%d  totalbytes=%"I64FMT"d  maxbytes=%"I64FMT"d)", 
				s, s_NumAllocs, s_MaxAllocs, s_BytesAllocated, s_MaxBytesAllocated);
	}
#endif

	return new_p;
}

static void libjsonDealloc(void* ptr)
{
#if RL_PC_JSON_DEBUG_MEM
	SYS_CS_SYNC(s_JsonDebugMemCs);

	Assert(s_NumAllocs > 0);
	--s_NumAllocs;

	size_t s;

#if RL_PC_JSON_USE_RL_ALLOCATOR
	if(g_RgscAllocator->IsValidPointer(ptr))
	{
		s = rgscGetAllocSize(ptr);
	}
	else
	{
		s = sysMemAllocator::GetCurrent().GetSize(ptr);
	}
#else
	s = sysMemAllocator::GetCurrent().GetSize(ptr);
#endif
	Assert(s);

	Assert(s_BytesAllocated > 0);      
	s_BytesAllocated -= s;
	Assert(s_BytesAllocated >= 0);

	rlDebug("Json Freed %u bytes (allocs=%d  maxallocs=%d  totalbytes=%"I64FMT"d  maxbytes=%"I64FMT"d)", 
			s, s_NumAllocs, s_MaxAllocs, s_BytesAllocated, s_MaxBytesAllocated);
#endif

#if RL_PC_JSON_USE_RL_ALLOCATOR
	if(g_RgscAllocator->IsValidPointer(ptr))
	{
		rgscFree(ptr);
	}
	else
	{
		delete [] (u8*)ptr;
	}
#else
	delete [] (u8*)ptr;
#endif
}

static void * libjsonRealloc(void* ptr, unsigned long size)
{
	u8* new_p = size ? (u8*)libjsonAlloc(size) : 0;

	if(ptr)
	{
		if(new_p)
		{
#if RL_PC_JSON_USE_RL_ALLOCATOR
			size_t s = rgscGetAllocSize(ptr);
#else
			size_t s = sysMemAllocator::GetCurrent().GetSize(ptr);
#endif
			s = s > size ? size : s;

			sysMemCpy(new_p, ptr, s);
		}
		libjsonDealloc(ptr);
	}

	return new_p;
}

#if __DEV
static void libjsonDebugCallback(const json_char* message)
{
	//rlError("libjson: %s", message);
	Assertf(false, "libjson: %s", message);
}
#endif

RgscJson::RgscJson()
{

}

RgscJson::~RgscJson()
{

}

bool
RgscJson::Init()
{
	// init libjson
#if __DEV && !__OPTIMIZED
	json_register_debug_callback(libjsonDebugCallback);
#endif

	json_register_memory_callbacks(libjsonAlloc, libjsonRealloc, libjsonDealloc);

	return true;
}

void 
RgscJson::FreeAllMemory()
{
	json_free_all();
	json_delete_all();

	// some memory is still allocated for structures
// #if RL_PC_JSON_DEBUG_MEM
// 	Assert((s_NumAllocs == 0) && (s_BytesAllocated == 0));
// #endif
}

void
RgscJson::Shutdown()
{
	FreeAllMemory();
}

void
RgscJson::Update()
{
	FreeAllMemory();
}

void json_free_safe(void* str)
{
	if(str != NULL)
	{
		json_free(str);
	}
}

} //namespace rgsc

#endif // RSG_PC
