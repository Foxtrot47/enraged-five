// 
// rline/rlpcgamerpic.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPCGAMERPIC_H
#define RLINE_RLPCGAMERPIC_H

#include "file/file_config.h"

#if RSG_PC

#include "gamerpics_interface.h"
#include "rgsc_common.h"
#include "tasks.h"

#include "atl/inlist.h"
#include "atl/string.h"
#include "atl/queue.h"
#include "net/status.h"
#include "rline/rltask.h"
#include "rline/socialclub/rlsocialclubcommon.h"

using namespace rage;

namespace rgsc
{

class RgscGamerPicGroups
{
public:

    class Group
    {
        friend class RgscGamerPicGroups;
    public:
    
        class Pic
        {
            friend class Group;
            friend class RgscGamerPicManager;
        public:
            Pic() {Clear();}
            ~Pic() {Clear();}

			// Note: the caller is responsible for releasing the image
            const char* GetCaption() const {return m_Caption;}

        private:
            inlist_node<Pic> m_ListLink;

            const char* GetName() const {return m_Name;}
            void Clear();
            bool Reset(const char* name, const char* caption);

            char m_Name[RGSC_MAX_AVATAR_URL_CHARS];
            char m_Caption[64];
        };

        Group() {Clear();}
        ~Group() {Clear();}

        void ClearList();
        u32 GetId() const {return m_Id;}
        const char* GetName() const {return m_Name;}
        bool IsPopulated() const {return m_IsPopulated;}
        u32 GetNumPics() const {return (u32)m_List.size();}
        Pic& GetPic(const unsigned index);

        typedef inlist<Pic, &Pic::m_ListLink> PicList;

    private:
        //prevent copying
        Group(const Group&);
        Group& operator=(Group&);

        friend class RgscGamerPicManager;
        void Clear();
        bool Reset(const char* groupName, const char* displayName);
    
        char m_Name[64];
        u32 m_Id;
        bool m_IsPopulated : 1;
        PicList m_List;

        inlist_node<Group> m_ListLink;
    };

    RgscGamerPicGroups() {Clear();}
    ~RgscGamerPicGroups() {Clear();}

    void Clear();
    u32 GetNumGroups() const {return (u32)m_List.size();}
    Group& GetGroup(const unsigned index);

    typedef inlist<Group, &Group::m_ListLink> rlPcGamerPicGroupList;

private:
    //prevent copying
    RgscGamerPicGroups(const RgscGamerPicGroups&);
    RgscGamerPicGroups& operator=(RgscGamerPicGroups&);

    friend class RgscGamerPicManager;
    rlPcGamerPicGroupList m_List;
};

// ===============================================================================================
// RgscGamerPicManager
// ===============================================================================================
class RgscGamerPicManager : public IGamerPicManagerLatestVersion
{
public:
	// ===============================================================================================
	// inherited from IRgscUnknown
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

	// ===============================================================================================
	// inherited from IGamerPicManagerV1
	// ===============================================================================================
	virtual bool RGSC_CALL GetDefaultGamerPicPath(char (&path)[RGSC_MAX_PATH], AvatarSize avatarSize);
	virtual bool RGSC_CALL DownloadGamerPic(const char* relativeAvatarUrl, int avatarSizeFlags, IAsyncStatus* status);
	virtual bool RGSC_CALL GetGamerPicPath(const char (&gamerPicName)[RGSC_MAX_AVATAR_URL_CHARS], AvatarSize avatarSize, char (&path)[RGSC_MAX_PATH]) const;
	virtual bool RGSC_CALL GetGamerPicUrl(const char (&gamerPicName)[RGSC_MAX_AVATAR_URL_CHARS], AvatarSize avatarSize, char (&url)[RGSC_MAX_PATH]) const;

	// ===============================================================================================
	// accessible from anywhere in the dll
	// ===============================================================================================
    RgscGamerPicManager();
    virtual ~RgscGamerPicManager();

    bool Init();
    void Shutdown();
    void Update();

    //Populate a rlPcGamerPicGroups structure with just the group names.
    //After that, you can download a list of pics from a group by calling
    //DownloadGamerPicGroup().
    bool PopulateGamerPicGroupNames(RgscGamerPicGroups* groups, netStatus* status);

    //Downloads all of the pics in the specified group, and populates
    //the group's gamer pic captions for all the pics in the group.
    //The group you pass must be in the collection populated by
    //PopulateGamerPicGroupNames().
    bool DownloadGamerPicGroup(RgscGamerPicGroups::Group* group, netStatus* status);

	//Returns the raw data for gamer pic by name if it already exists locally, NULL otherwise
	u8* GetGamerPicRaw(const char (&gamerPicName)[RGSC_MAX_AVATAR_URL_CHARS], AvatarSize avatarSize, unsigned int* sizeInBytes) const;

	//Returns true if gamerPicName is valid. Writes the normalized gamer pic name to the normalizedGamerPicName argument.
	bool GetNormalizedGamerPicName(const char* gamerPicName, char (&normalizedGamerPicName)[RGSC_MAX_AVATAR_URL_CHARS]) const;

	//Returns png image encoded as base64
	const char* GetGamerPicBase64(const char* jsonRequest);

	//! Deletes all outdated avatars from the custom folder
	bool ClearOutdatedCustomAvatars();

	//! Queues up a download of a gamer pic. This is a fire and forget action, usually to set up
	//	gamer pics to be used in offline mode.
	bool QueueDownloadGamerPic(const char* relativeAvatarUrl);

private:

	static const int MAX_QUEUED_GAMER_PICS = 8;
	atQueue <atString, MAX_QUEUED_GAMER_PICS> m_GamerPicQueue;
	AsyncStatus m_MyStatus;

    class DownloadGamerPicTask : public rgscTask<RgscGamerPicManager>
    {
    public:

        RL_TASK_DECL(DownloadGamerPicTask);

        enum DownloadPics
        {
            DL_SMALL = 0,
			DL_LARGE = 1,
			DL_XLARGE = 2,
			DL_COUNT
        };

		enum State
		{
			STATE_DOWNLOAD,
			STATE_DOWNLOADING
		};

        DownloadGamerPicTask();

        bool Configure(RgscGamerPicManager* ctx, const char* gamerPicName, int avatarSizeFlags);

        virtual void Start();

        virtual void Finish(const FinishType finishType, const int resultCode = 0);

        virtual void Update(const unsigned timeStep);

		virtual bool IsCancelable() const {return true;}
		virtual void DoCancel();

        bool SetupPaths(AvatarSize avatarSize);
        bool DownloadGamerPic(AvatarSize avatarSize, netStatus* status);

        char m_GamerPicName[RGSC_MAX_AVATAR_URL_CHARS];
        char m_Url[RGSC_MAX_PATH];
		char m_LocalPath[RGSC_MAX_PATH];
		int m_AvatarSizeFlags;

		State m_State;
        netStatus m_MyStatus[DL_COUNT];
    };

	typedef rlTask<RgscGamerPicManager> TaskBase;

    class PopulateGamerPicGroupNamesTask : public TaskBase
    {
    public:
        RL_TASK_DECL(PopulateGamerPicGroupNamesTask);

        enum State
        {
            STATE_INVALID   = -1,
            STATE_DOWNLOAD_LAYOUT_FILE,
            STATE_DOWNLOADING_LAYOUT_FILE,
            STATE_POPULATE_GROUP_NAMES,
        };

        PopulateGamerPicGroupNamesTask();

        bool Configure(RgscGamerPicManager* ctx,
                       RgscGamerPicGroups* groups);

        virtual void Start();

        virtual void Finish(const FinishType finishType, const int resultCode = 0);

        virtual void Update(const unsigned timeStep);

		virtual bool IsCancelable() const {return true;}
		virtual void DoCancel();

        bool DownloadLayoutFile();
        bool PopulateGroupNames();

        char m_Url[RGSC_MAX_PATH];
        char m_LocalPath[RGSC_MAX_PATH];
        RgscGamerPicGroups *m_Groups;
        int m_State;
        netStatus m_MyStatus;
    };

    class DownloadGamerPicGroupTask : public TaskBase
    {
    public:
        RL_TASK_DECL(DownloadGamerPicGroupTask);

        enum State
        {
            STATE_INVALID   = -1,
            STATE_POPULATE_GROUP,
            STATE_DOWNLOADING_GAMER_PICS,
        };

        DownloadGamerPicGroupTask();

        bool Configure(RgscGamerPicManager* ctx,
                       RgscGamerPicGroups::Group* group);

        virtual void Start();

        virtual void Finish(const FinishType finishType, const int resultCode = 0);

        virtual void Update(const unsigned timeStep);

		virtual bool IsCancelable() const {return true;}
		virtual void DoCancel();

        bool PopulateGroup();
        int FindAvailableSubTask();

        RgscGamerPicGroups::Group *m_Group;
        unsigned m_NumStartedTasks;
        unsigned m_NumCompletedTasks;
        unsigned m_NumSucceededTasks;
        unsigned m_NumFailedTasks;
        static const unsigned m_MaxSimultaneousQueries = 25;
        DownloadGamerPicTask m_DownloadGamerPicTask[m_MaxSimultaneousQueries];
        netStatus m_SubTaskStatus[m_MaxSimultaneousQueries];

        u32 m_StartTime;
        int m_State;
        netStatus m_MyStatus;
    };
};

}

#endif // RSG_PC

#endif  //RLINE_RLPCGAMERPIC_H
