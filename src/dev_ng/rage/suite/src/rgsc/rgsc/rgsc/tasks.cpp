// 
// rline/rlpctask.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "tasks.h"

#if RSG_PC

#include "rgsc.h"
#include "rltask.h"
#include "diag/seh.h"
#include "rline/rldiag.h"

using namespace rage;

namespace rgsc
{

#if __RAGE_DEPENDENCY
RAGE_DEFINE_SUBCHANNEL(rgsc_dbg, task);
#undef __rage_channel
#define __rage_channel rgsc_dbg_task
#endif

extern sysMemAllocator* g_RgscAllocator;

RGSC_HRESULT AsyncStatus::QueryInterface(RGSC_REFIID riid, void** ppvObject)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObject == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IAsyncStatus*>(this);
	}
	else if(riid == IID_IAsyncStatusV1)
	{
		pUnknown = static_cast<IAsyncStatusV1*>(this);
	}

	*ppvObject = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

// ===============================================================================================
// inherited from IAsyncStatusV1
// ===============================================================================================
unsigned int AsyncStatus::AddRef()
{
	return ++m_RefCount;
}

unsigned int AsyncStatus::Release()
{
	unsigned int refCount = --m_RefCount;

	if(refCount == 0)
	{
		delete this;
	}

	return refCount;
}

void AsyncStatus::SetOperationFinishedCallback(OperationFinishedCallback callback, void* callbackData)
{
	m_Callback = callback;
	m_CallbackData = callbackData;
}

int AsyncStatus::GetResultCode()
{
	return m_Status.GetResultCode();
}

void AsyncStatus::Cancel()
{
	GetRgscConcreteInstance()->_GetTaskManager()->CancelTask(&m_Status);
}

bool AsyncStatus::Pending()
{
	return m_Status.Pending();
}

bool AsyncStatus::Succeeded()
{
	return m_Status.Succeeded();
}

bool AsyncStatus::Failed()
{
	return m_Status.Failed();
}

bool AsyncStatus::Canceled()
{
	return m_Status.Canceled();
}

RGSC_HRESULT RgscTaskManager::QueryInterface(RGSC_REFIID riid, void** ppvObject)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObject == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<ITaskManager*>(this);
	}
	else if(riid == IID_ITaskManagerV1)
	{
		pUnknown = static_cast<ITaskManagerV1*>(this);
	}

	*ppvObject = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

RGSC_HRESULT RgscTaskManager::CreateAsyncStatus(IAsyncStatus** status)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		// TODO: NS - create a pool of these (max capacity of 32 should be enough)
		//@@: location RLPCTASK_CREATEASYNCSTATUS_NEW_STATUS
		AsyncStatus* ret = rage_new AsyncStatus;
		rverify(ret, catchall, );
		AssertVerify(ret->AddRef() == 1);
		*status = ret;
		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

RgscTaskManager::RgscTaskManager()
: m_ThreadId(0)
{

}

RgscTaskManager::~RgscTaskManager()
{

}

bool
RgscTaskManager::Init()
{
	bool success = false;

	rtry
	{
		m_ThreadId = GetCurrentThreadId();

		rverify(m_TaskMgr.Init(g_RgscAllocator), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void
RgscTaskManager::Shutdown()
{
	m_ThreadId = 0;
	m_TaskQueue.Clear();
	m_TaskMgr.CancelAll();
	m_TaskMgr.Shutdown();
}

void
RgscTaskManager::Update()
{
    rlTaskBase* task = NULL;
	while(m_TaskQueue.NextTask(&task))
	{
		if(!task->IsStatusCanceled())
		{
			m_TaskMgr.AddParallelTask(task);
			//		AddParallelTask(task);
		}
	}

	m_TaskMgr.Update();
}

bool 
RgscTaskManager::AddParallelTask(rlTaskBase* task)
{
	bool success = false;

	if(GetCurrentThreadId() == m_ThreadId)
	{
		rlDebug("Queuing rlPcTask '%s' on main thread", task->GetTaskName());
		success = m_TaskMgr.AddParallelTask(task);
	}
	else
	{
		rlDebug("Queuing rlPcTask '%s' from separate thread, will execute on main thread", task->GetTaskName());
		success = m_TaskQueue.QueueTask(task);
	}

	return success;
}

void RgscTaskManager::CancelTask(netStatus* status)
{
	CancelTaskIfNecessary(status);
}

void RgscTaskManager::CancelTaskIfNecessary(netStatus* status)
{
	rlDebug("rlPcTask::CancelTaskIfNecessary");

	if(status == NULL)
	{
		return;
	}

	if(!status->Pending())
	{
		return;
	}

	rlTaskBase* task = GetRgscConcreteInstance()->_GetTaskManager()->m_TaskMgr.FindTask(status);
	if(task)
	{
		GetRgscConcreteInstance()->_GetTaskManager()->m_TaskMgr.CancelTask(task);
	}
	else if (netTask::HasTask(status))
	{
		netTask::Cancel(status);
	}
	else if (rlGetTaskManager() && rlGetTaskManager()->FindTask(status) != NULL)
	{
		rlGetTaskManager()->CancelTask(status);
	}

	if(!status->Canceled())
	{
		status->SetCanceled();
	}
}

void 
RgscTaskManager::DestroyTask(rlTaskBase* task)
{
	//@@: location RLPCTASK_DESTROYTASK
	m_TaskMgr.DestroyTask(task);
}

} // namespace rgsc

#endif // RSG_PC
