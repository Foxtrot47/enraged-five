// 
// rline/json.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPC_JSON_H
#define RLINE_RLPC_JSON_H

#include "file/file_config.h"

#if RSG_PC

#if __DEV && !__OPTIMIZED
#define JSON_DEBUG
#else
#ifndef NDEBUG
#define NDEBUG
#endif
#endif

#include "../../3rdParty/libjson_7.6.1/libjson/libjson.h"

namespace rgsc
{

class RgscJson
{
public:

    RgscJson();	
    ~RgscJson();

    bool Init();
    void Shutdown();
    void Update();

private:
	void FreeAllMemory();
};

void json_free_safe(void* str);

}

#endif // RSG_PC

#endif  //RLINE_RLPC_JSON_H
