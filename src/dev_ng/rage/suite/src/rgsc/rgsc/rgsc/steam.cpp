// 
// rgsc/steam.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "steam.h"

#if RGSC_STEAM_EXPLICIT_SUPPORT

#include "rgsc/diag/output.h"
#include "rgsc.h"
#include "rgsc_ui/weakstring.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <ShlObj.h>
#pragma warning(pop)

using namespace rage;

#define GET_PROC_ADDR_FUNC(funcName, module) \
	m_##funcName = (funcName##Ptr)GetProcAddress(module, #funcName); \
	rcheck(m_##funcName != NULL, catchall, rlError("Could not map function: %s", #funcName));

#define GET_PROC_ADDR_MEMBER(member, module) \
	member##Ptr l##member = (member##Ptr)GetProcAddress(module, #member); \
	rcheck(l##member != NULL, catchall, rlError("GetProcAddresss failed for: %s", #member)); \
	m_##member = l##member(); \
	rcheck(m_##member != NULL, catchall, rlError("Could not map member: %s", #member));

namespace rgsc
{

#if __RAGE_DEPENDENCY
RAGE_DEFINE_SUBCHANNEL(rgsc_dbg, steam);
#undef __rage_channel
#define __rage_channel rgsc_dbg_steam
#endif

RgscSteamManager::RgscSteamManager()
	: m_SteamAPI_Init(NULL)
	, m_SteamAPI_Shutdown(NULL)
	, m_SteamUser(NULL)
	, m_SteamFriends(NULL)
	, m_SteamUtils(NULL)
	, m_SteamMatchmaking(NULL)
	, m_SteamUserStats(NULL)
	, m_SteamApps(NULL)
	, m_SteamNetworking(NULL)
	, m_SteamMatchmakingServers(NULL)
	, m_SteamRemoteStorage(NULL)
	, m_SteamScreenshots(NULL)
	, m_SteamHTTP(NULL)
	, m_SteamUnifiedMessages(NULL)
	, m_SteamController(NULL)
	, m_SteamUGC(NULL)
{

}

bool RgscSteamManager::Init()
{
	bool bSuccess = false;

	rtry
	{
		// Only initialize if we're a Steam SKU
		rcheck(GetRgscConcreteInstance()->IsSteam(), nonsteam, );

		char dllPath[RGSC_MAX_PATH] = {0};
		GetRgscConcreteInstance()->_GetFileSystem()->GetSteamDllPath(dllPath);

		wchar_t wideDllPath[RGSC_MAX_PATH] = {0};
		MultiByteToWideChar(CP_UTF8 , NULL , dllPath, -1, wideDllPath, RGSC_MAX_PATH);

		// We use the LOAD_WITH_ALTERED_SEARCH_PATH flag to guarantee we load our Steam DLL
		// instead of the default LoadLibrary options (which would pick up the game's steam_api64.dll)
		m_hSteamDll = LoadLibraryEx(wideDllPath, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
		rverify(m_hSteamDll != NULL, catchall, );

		GET_PROC_ADDR_FUNC(SteamAPI_Init, m_hSteamDll);
		GET_PROC_ADDR_FUNC(SteamAPI_Shutdown, m_hSteamDll);

		// Initialize Steam before attempting to get members
		rverify(m_SteamAPI_Init(), catchall, );

		GET_PROC_ADDR_MEMBER(SteamUser, m_hSteamDll);
		GET_PROC_ADDR_MEMBER(SteamFriends, m_hSteamDll);
		GET_PROC_ADDR_MEMBER(SteamUtils, m_hSteamDll);
		GET_PROC_ADDR_MEMBER(SteamMatchmaking, m_hSteamDll);
		GET_PROC_ADDR_MEMBER(SteamUserStats, m_hSteamDll);
		GET_PROC_ADDR_MEMBER(SteamApps, m_hSteamDll);
		GET_PROC_ADDR_MEMBER(SteamNetworking, m_hSteamDll);
		GET_PROC_ADDR_MEMBER(SteamMatchmakingServers, m_hSteamDll);
		GET_PROC_ADDR_MEMBER(SteamRemoteStorage, m_hSteamDll);
		GET_PROC_ADDR_MEMBER(SteamScreenshots, m_hSteamDll);
		GET_PROC_ADDR_MEMBER(SteamHTTP, m_hSteamDll);
		GET_PROC_ADDR_MEMBER(SteamUnifiedMessages, m_hSteamDll);
		GET_PROC_ADDR_MEMBER(SteamController, m_hSteamDll);
		GET_PROC_ADDR_MEMBER(SteamUGC, m_hSteamDll);

		RgscDisplay("Steam API initialized for steam user: %s", m_SteamFriends->GetPersonaName());
		rlDebug3("RGSC Steam API, Client Version: %s", STEAMCLIENT_INTERFACE_VERSION);
		bSuccess = true;
	}
	rcatch(nonsteam)
	{
		bSuccess = true;
	}
	rcatchall
	{

	}

	return bSuccess;
}

bool RgscSteamManager::Shutdown()
{
	if (GetRgscConcreteInstance()->IsSteam())
	{
		m_SteamAPI_Shutdown();
	}

	return true;
}

} // namespace rgsc

#endif // RGSC_STEAM_EXPLICIT_SUPPORT

