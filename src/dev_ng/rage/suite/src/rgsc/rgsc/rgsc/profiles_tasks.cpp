// 
// rgsc/profiles.cpp 
// 
// Copyright (C) 1999-2017 Rockstar Games.  All Rights Reserved. 
// 

#include "profiles_tasks.h"
#include "../diag/output.h"

// rgsc includes
#include "rgsc.h"
#include "profiles.h"

// rage includes
#include "parser/manager.h"
#include "rline/rltitleid.h"
#include "rline/ros/rlros.h"
#include "rline/ros/rlroscredtasks.h"
#include "rline/ros/rlrostitleid.h"
#include "rline/socialclub/rlsocialclub.h"
#include "string/stringutil.h"
#include "system/param.h"

PARAM(scAuthTicketTtl, "Sets a custom TTL time for ScAuthTokens");
PARAM(scAuthTicketAudience, "Sets a custom Audience for ScAuthTokens");

namespace rage
{
	NOSTRIP_PC_XPARAM(scNoAutologin);
	extern const rlTitleId* g_rlTitleId;
	extern bool UseGameServicesAuth();
}

namespace rgsc
{

// Sc Auth Token Creation defaults
static const char* DEFAULT_SC_AUTH_AUDIENCE = "Game"; // "Game" audience to be used by launcher and game clients
static const int DEFAULT_SC_AUTH_TTL_MINUTES = 60 * 24 * 30; // 30 days

///////////////////////////////////////////////////////////////////////////////
//  SignInTask
///////////////////////////////////////////////////////////////////////////////

/*
	Be very careful if you're modifying this task. It has to handle all of the following cases:
	1. signing into an online profile while connected to the internet
	2. signing into an online profile while not connected to the internet
	3. signing into an online profile from a computer that has the profile stored locally
	4. signing into an online profile from a computer that doesn't have the profile stored locally
	5. signing into an offline profile while connected to the internet
	6. signing into an offline profile while not connected to the internet
	7. signing into an online profile even if the user has changed their e-mail and/or password
		from the Social Club website - in that case, the SC is the authority.
*/

SignInTask::SignInTask()
: m_State(STATE_INVALID)
, m_ProfileId(InvalidRockstarId)
, m_XmlResponseDecoded(NULL)
, m_SigningIn(NULL)
, m_ConnectedToRos(false)
, m_ScAuthTokenError(false)
, m_AchievementsSynced(false)
, m_FriendsSynced(false)
, m_SignedInLocally(false)
, m_SaveEmail(false)
, m_SavePassword(false)
, m_SaveMachine(false)
, m_AutoSignIn(false)
, m_IsOfflineProfile(false)
, m_CallbackData(0)
, m_FriendSyncStartTime(0)
{
    sysMemSet(m_Nickname, 0, sizeof(m_Nickname));
	sysMemSet(m_Email,  0, sizeof(m_Email));
	sysMemSet(m_Password,  0, sizeof(m_Password));
	sysMemSet(m_ScAuthToken, 0, sizeof(m_ScAuthToken));
	sysMemSet(m_MachineToken, 0, sizeof(m_MachineToken));
	sysMemSet(m_AvatarUrl,  0, sizeof(m_AvatarUrl));
}

SignInTask::~SignInTask()
{
	if(m_XmlResponseDecoded)
	{
		RL_FREE(m_XmlResponseDecoded);
		m_XmlResponseDecoded = NULL;
	}

    if(m_SigningIn)
    {
        *m_SigningIn = false;
    }

	// clear from memory on task cleanup
	sysMemSet(m_Email, 0, sizeof(m_Email));
	sysMemSet(m_Password, 0, sizeof(m_Password));
	sysMemSet(m_ScAuthToken, 0, sizeof(m_ScAuthToken));
	sysMemSet(m_MachineToken, 0, sizeof(m_MachineToken));
}

bool
SignInTask::Configure(RgscProfileManager* /*ctx*/,
					  const char* email,
					  const char* password,
					  const char* nickname,
					  const char* avatarUrl,
					  const bool saveEmail,
					  const bool savePassword,
					  const bool saveMachine,
					  const bool autoSignIn,
					  const char* xmlResponse,
					  const RockstarId rockstarId,
					  const bool isOfflineProfile,
					  const int callbackData)
{
	bool success = false;

	rtry
	{
 		rverify(m_Ctx->IsSignedInInternal() == false, catchall, );
 		rverify(m_Ctx->IsSigningIn() == false, catchall, );

		if ( !GetRgscConcreteInstance()->IsSteam() )
		{
			if (GetRgscConcreteInstance()->_GetUiInterface()->HadSignInError())
			{
				rcheck(email && (email[0] != '\0'), catchall, );
			}
			else
			{
				rverify(email && (email[0] != '\0'), catchall, );
			}
		}
		else
		{
			rcheck(!GetRgscConcreteInstance()->_GetUiInterface()->HadSignInError(), catchall, );
		}

		rverify((saveEmail >= savePassword) && (savePassword >= autoSignIn), catchall, );

		m_Ctx->DeleteSignInTransferData();

		m_State = STATE_INVALID;
		m_CallbackData = callbackData;
		m_ConnectedToRos = false;
		m_ScAuthTokenError = false;
		//m_RetrievedGamerInfo = false;
		m_AchievementsSynced = false;
		m_FriendsSynced = false;
		//@@: location RLPCPROFILEMANAGER_SIGNINTASK_CONFIGURE
		m_SignedInLocally = false;
		m_IsOfflineProfile = isOfflineProfile;
		m_ProfileId = rockstarId;
		m_SavedProfile.Clear();
		m_RosCredentials.Clear();

		if(ParseRosCredentials(xmlResponse))
		{
			m_ConnectedToRos = true;
		}
		else
		{
			m_RosCredentials.Clear();
		}

		safecpy(m_Email, email);
		safecpy(m_Password, password);
		safecpy(m_AvatarUrl, avatarUrl);
		safecpy(m_Nickname, nickname);

		// Load e-mail saving from cache for reload profiles
		if (GetRgscConcreteInstance()->_GetUiInterface()->IsReloadingUi() && m_Ctx->m_ReloadProfile.IsValid())
		{
			m_SaveEmail = m_Ctx->m_ReloadProfile.IsLoginSaved();
		}
		else
		{
			m_SaveEmail = saveEmail;
		}

		m_SavePassword = savePassword;
		m_SaveMachine = saveMachine;
		m_AutoSignIn = autoSignIn;

		m_SigningIn = &m_Ctx->m_SigningIn;
		*m_SigningIn = true;

		RgscDisplay("Signing in to profile '%s' (Online: %d, Auto Sign In: %d, Save Email: %d)", m_Nickname, m_ConnectedToRos, m_AutoSignIn, m_SaveEmail);
		success = true;
	}
	rcatchall
	{
		// We failed to configure, notify the SCUI.
		NotifyPlatform();
	}

	return success;
}

void
SignInTask::Start()
{
    rlDebug2("Signing in...");

    this->ProfileTaskBase::Start();

	m_State = STATE_FIND_PROFILE;
}

void
SignInTask::Finish(const FinishType finishType, const int resultCode)
{
    rlDebug2("Signing in %s", FinishString(finishType));

    this->ProfileTaskBase::Finish(finishType, resultCode);
}

void
SignInTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
}

void
SignInTask::Update(const unsigned timeStep)
{
    this->ProfileTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			NotifyPlatform();
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

    do
    {
        switch(m_State)
        {
		case STATE_FIND_PROFILE:
			{
				bool bHasLocalProfile = FindProfile() && m_Profile.IsValid();
				if(m_ConnectedToRos)
				{
					DownloadGamerPic();
					m_State = STATE_GET_SC_AUTHTOKEN;
				}
				else if (bHasLocalProfile)
				{
					// We're not downloading a new auth token - keep the old machine token and auth token.
					safecpy(m_MachineToken, m_Profile.GetMachineToken());
					if (m_Profile.IsAuthTokenSaved())
					{
						safecpy(m_ScAuthToken, m_Profile.GetSavedAuthToken());
					}
					
					m_State = STATE_LOCAL_SIGN_IN;
				}
				else
				{
					m_State = STATE_FAILED;
				}
			}
			break;

		case STATE_LOCAL_SIGN_IN:
            {
                if(AssertVerify(m_Profile.IsValid()))
                {
                    if(SignInLocally())
                    {
						m_State = STATE_SAVE_PROFILE;
                    }
                    else
                    {
                        m_State = STATE_FAILED;
                    }
                }
                else
                {
                    m_State = STATE_FAILED;
                }
            }
            break;

		case STATE_GET_SC_AUTHTOKEN:
			if(this->GetScAuthToken())
			{
				m_State = STATE_GETTING_SC_AUTHTOKEN;
			}
			else
			{
				m_ConnectedToRos = false;
				m_ScAuthTokenError = true;
				m_State = STATE_LOCAL_SIGN_IN;
			}
			break;
		case STATE_GETTING_SC_AUTHTOKEN:
			if(!m_MyStatus.Pending())
			{
				if(m_MyStatus.Succeeded())
				{
					m_State = STATE_SAVE_PROFILE;
				}
				else
				{
					RgscDisplay("Failed to retrieve authorization token (error: %d)", m_MyStatus.GetResultCode());
					m_ConnectedToRos = false;
					m_ScAuthTokenError = true;
					m_State = STATE_LOCAL_SIGN_IN;
				}
			}
			break;
        case STATE_SAVE_PROFILE:
			if(this->SaveProfile(m_SavedProfile))
            {
				Assert(m_SavedProfile.IsValid());
				Assert(m_Ctx->m_SignedInProfile.IsValid() == false);
				m_Ctx->m_SignedInProfile.Clear();
				//@@: location RLPCPROFILEMANAGER_SIGNINTASK_UPDATE
				sysMemSet(m_Ctx->m_SignedInEmail, 0, sizeof(m_Ctx->m_SignedInEmail));
				sysMemSet(m_Ctx->m_SignedInScAuthToken, 0, sizeof(m_Ctx->m_SignedInScAuthToken));
				m_Ctx->m_SignedInProfile = m_SavedProfile;
				safecpy(m_Ctx->m_SignedInEmail, m_Email);
				safecpy(m_Ctx->m_SignedInScAuthToken, m_ScAuthToken);

				m_SignedInLocally = true;

				// TODO: NS - move this to a TitleManager later
				// create the Profile/Title directory so we know that the player has played this title
				char path[RGSC_MAX_PATH] = {0};
				AssertVerify(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfileTitleDirectory(path, true));

				if(m_ConnectedToRos)
				{
					SetRosCredentials();

					if(GetRgscConcreteInstance()->IsAchievementsSupported())
					{
						m_State = STATE_SYNC_ACHIEVEMENTS;
					}
					else if (GetRgscConcreteInstance()->IsFriendsSupported())
					{
						m_State = STATE_SYNC_FRIENDS;
					}
					else
					{
						m_State = STATE_SUCCEEDED;
					}
				}
				else
				{
					m_State = STATE_SUCCEEDED;
				}
			}
            else
            {
                m_State = STATE_FAILED;
            }
            break;

        case STATE_SYNC_ACHIEVEMENTS:
            if(this->SyncAchievements())
            {
                m_State = STATE_SYNCING_ACHIEVEMENTS;
            }
            else
            {
				// allow the sign in task to continue even if we can't get achievement data
				m_State = STATE_SYNC_FRIENDS;
			}
            break;

		case STATE_SYNCING_ACHIEVEMENTS:
            if(!m_MyStatus.Pending())
            {
				if(m_MyStatus.Succeeded())
				{
					m_AchievementsSynced = true;
					m_State = STATE_SYNC_FRIENDS;
				}
				else
				{
					// allow the sign in task to continue even if we can't get achievement data
					m_State = STATE_SYNC_FRIENDS;
				}
            }
            break;

		case STATE_SYNC_FRIENDS:
			if (GetRgscConcreteInstance()->IsFriendsSupported())
			{
				// Set the start time for friends syncing if unset.
				if (m_FriendSyncStartTime == 0)
					m_FriendSyncStartTime = sysTimer::GetSystemMsTime();

				// We can't sync friends until we're online in presence.
				if (!rlPresence::IsOnline(RL_RGSC_GAMER_INDEX))
				{
					// If we're not online and the friend sync has timed out, throw an error and continue;
					if (sysTimer::HasElapsedIntervalMs(m_FriendSyncStartTime, SYNC_FRIENDS_WAIT_FOR_ONLINE_MS))
					{
						rlError("Friends syncing timed out");
						m_State = STATE_SUCCEEDED;
					}

					// exit the loop to allow RageNet to update
					return;
				}
				else if(this->SyncFriends())
				{
					m_State = STATE_SYNCING_FRIENDS;
				}
				else
				{
					// allow the sign in task to succeed even if we can't get friend data
					m_State = STATE_SUCCEEDED;
				}
			}
			else
			{
				m_State = STATE_SUCCEEDED;
			}
			break;

		case STATE_SYNCING_FRIENDS:
			if(!m_MyStatus.Pending())
			{
				if(m_MyStatus.Succeeded())
				{
					m_FriendsSynced = true;
					m_State = STATE_SUCCEEDED;
				}
				else
				{
					// allow the sign in task to succeed even if we can't get friend data
					m_State = STATE_SUCCEEDED;
				}
			}
			break;

        case STATE_SUCCEEDED:
			NotifyPlatform();
            this->Finish(FINISH_SUCCEEDED);
            break;

        case STATE_FAILED:
			NotifyPlatform();
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
            break;
        }
    }
    while(!m_MyStatus.Pending() && this->IsActive());
}

bool
SignInTask::ParseRosCredentials(const char* xmlResponse)
{
	bool success = false;

	int xmlResponseLen = xmlResponse ? (int)strlen(xmlResponse) : 0;
	if(xmlResponseLen == 0)
	{
		return false;
	}

	INIT_PARSER;

	parTree* tree = NULL;

	rtry
	{
		// the SCUI passes the xmlReponse URL encoded.

		// TODO: NS - is the decoded length always <= the encoded length?
		// * 2 just in case it can be larger than encoded size
		const unsigned xmlMaxResponseDecodedLen = (xmlResponseLen + 1) * 2;
		unsigned xmlResponseDecodedLen = xmlMaxResponseDecodedLen;

		m_XmlResponseDecoded = (char*)RL_ALLOCATE(RgscProfileManager, xmlMaxResponseDecodedLen);
		rverify(m_XmlResponseDecoded, catchall, );
		sysMemSet(m_XmlResponseDecoded, 0, xmlMaxResponseDecodedLen);

		unsigned numConsumed = 0;
		rverify(netHttpRequest::UrlDecode(m_XmlResponseDecoded,
										  &xmlResponseDecodedLen,
										  xmlResponse,
										  xmlResponseLen,
										  &numConsumed), catchall, );

		// make a copy since PARSER.LoadTree() clears the memory it's passed
		char* xmlResponseDecoded = (char*)RL_ALLOCATE(RgscProfileManager, xmlMaxResponseDecodedLen);
		rverify(xmlResponseDecoded, catchall, );
		sysMemSet(xmlResponseDecoded, 0, xmlMaxResponseDecodedLen);
		memcpy(xmlResponseDecoded, m_XmlResponseDecoded, xmlMaxResponseDecodedLen);

		// treat the response as a memory file so we can use the XML parser
		char filename[64];
		fiDeviceMemory::MakeMemoryFileName(filename, sizeof(filename), xmlResponseDecoded, xmlResponseDecodedLen, false, NULL);

		PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);
		tree = PARSER.LoadTree(filename, "xml");
		PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, false);

		rverify(tree && tree->GetRoot(), catchall, );

		parTreeNode* pRoot = tree->GetRoot();
		rverify(pRoot, catchall, );

		rlRosCreateTicketResponse response;
		rverify(rlRosXmlCreateTicketResponseParser::Parse(pRoot, &response), catchall, );
		success = rlRosCreateTicketResponseHelper::ApplyCreateTicketResponse(response, RL_RGSC_GAMER_INDEX, &m_RosCredentials);
	}
	rcatchall
	{
	}

	if(tree)
	{
		delete tree;
		tree = NULL;
	}

	SHUTDOWN_PARSER;

	return success;
}

bool
SignInTask::SetRosCredentials()
{
	bool success = false;

	rtry
	{
		rcheck(m_RosCredentials.IsValid(), catchall, );
		rverify(rlSocialClub::SetLogin(RL_RGSC_GAMER_INDEX, m_Email, m_ScAuthToken), catchall, );
		rverify(rlRos::SetCredentials(0, &m_RosCredentials), catchall, );

		Assert(rlRos::IsOnline(RL_RGSC_GAMER_INDEX));

		// Clear out out reload sc auth token, its no longer needed
		sysMemSet(m_Ctx->m_ReloadScAuthToken, 0, sizeof(m_Ctx->m_ReloadScAuthToken));

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool
SignInTask::FindProfile()
{
	bool success = false;
	bool found = false;

	rtry
	{
		// use the profile id to folder id map to load the profile.
		// If the loaded profile doesn't have the same IsOffline flag, then do the full enumeration
		// because technically we can have an offline profile and an online profile with the same profileId.
		u32 *mapData = m_Ctx->m_ProfileIdToFolderIdMap.Access(m_ProfileId);
		bool profileExists = mapData != NULL;

		if(profileExists)
		{
			RgscProfile profile;
			unsigned folderId = *mapData;
			rcheck(m_Ctx->LoadProfile(folderId, profile), catchall, );
			rcheck(profile.IsValid(), catchall, );
			rcheck(profile.GetProfileId() == m_ProfileId, catchall, );
			rcheck(profile.GetFolderId() == folderId, catchall, );

			if(profile.IsOfflineProfile() == m_IsOfflineProfile)
			{
				m_Profile.Clear();
				m_Profile = profile;
				found = true;
			}
		}
	}
	rcatchall
	{

	}

	if(found == false)
	{
		RgscProfiles profiles;
		m_Ctx->EnumerateProfiles(profiles);
		for(unsigned i = 0; i < profiles.GetNumProfiles(); i++)
		{
			const RgscProfile &profile = profiles.GetProfile(i);

			if((profile.GetProfileId() == m_ProfileId) &&
			   (profile.IsOfflineProfile() == m_IsOfflineProfile))
			{
				m_Profile.Clear();
				m_Profile = profile;
				break;
			}
		}
	}

	success = true;

	return success;
}

bool
SignInTask::SignInLocally()
{
    bool success = false;
    
    if(m_Ctx->Authenticate(m_Profile, m_Email, NULL))
    {
		if(AssertVerify(m_ProfileId == m_Profile.GetProfileId()))
		{
			safecpy(m_Nickname, m_Profile.GetName());
			success = true;
		}
    }

    return success;
}

// bool
// rlPcProfileManager::SignInTask::GetGamerInfo()
// {
//     return GetRgscConcreteInstance()->_GetPresenceManager()->GetGamerInfo(m_RosCredentials.GetRockstarId(), m_GamerInfo, true, &m_MyStatus);
// }

bool
SignInTask::DownloadGamerPic()
{
	bool success = false;

	const char* gamerPic = (m_AvatarUrl[0] != '\0') ?
							m_AvatarUrl :
							m_Profile.GetGamerPicName();

	if(!StringNullOrEmpty(gamerPic))
	{
		success = GetRgscConcreteInstance()->_GetGamerPicManager()->QueueDownloadGamerPic(gamerPic);
	}

	RgscDisplay("Queue download of profile avatar");
	return success;
}

bool
SignInTask::GetScAuthToken()
{
	const char* audience = DEFAULT_SC_AUTH_AUDIENCE;
	if (PARAM_scAuthTicketAudience.Get())
	{
		PARAM_scAuthTicketAudience.Get(audience);
	}

	int ttlMinutes = DEFAULT_SC_AUTH_TTL_MINUTES;
	if (PARAM_scAuthTicketTtl.Get())
	{
		PARAM_scAuthTicketTtl.Get(ttlMinutes);
	}

	// Do we have a machine token?
	if (m_Profile.IsValid())
	{
		safecpy(m_MachineToken, m_Profile.GetMachineToken());
	}

	// Determine if the profile should send KeepMeSignedIn
	// Autosignin requires both keepMeSignedIn and saveMachine to be true
	bool keepMeSignedIn = false;
	if (m_AutoSignIn)
	{
		m_SaveMachine = true; // could already be true
		keepMeSignedIn = true;
	}

	// Generate the fingerprint for the Auth Token request
	char fingerprint[4096] = {0};
	RsonWriter rw;
	rw.Init(fingerprint, RSON_FORMAT_JSON);
	rw.Begin(NULL, NULL);
	rw.Begin("fp", NULL);
	GetRgscConcreteInstance()->_GetMultiFactorAuth()->GetFingerPrintRSON(&rw);
	rw.End();
	rw.End();

	// Temporarily assign the SignInTask credentials to the profile manager. The social club tasks within can use this ticket in lieu of the
	// fully signed in ticket (rlRosCredentials::GetCredentials).
	m_Ctx->SetSignInCredentials(&m_RosCredentials);
	bool bSuccess = rlSocialClub::CreateScAuthToken2(RL_RGSC_GAMER_INDEX, m_RosCredentials.GetTicket(),
													m_ScAuthToken, sizeof(m_ScAuthToken),
													m_MachineToken, sizeof(m_MachineToken),
													m_SaveMachine, keepMeSignedIn,
													fingerprint, audience, ttlMinutes, 
													NULL, &m_MyStatus);
	m_Ctx->ClearSignInCredentials();

	RgscDisplay("Requesting authorization token from server.");
	return bSuccess;
}

bool
SignInTask::SaveProfile(RgscProfile &savedProfile)
{
    bool success = false;

// 	const char* gamerPic = m_RetrievedGamerInfo ?
//                            m_GamerInfo.GetGamerPicName() : 
//                            m_Profile.GetGamerPicName();

	const char* gamerPic = (m_AvatarUrl[0] != '\0') ?
							m_AvatarUrl :
							m_Profile.GetGamerPicName();

	RockstarId profileId = m_ConnectedToRos ?
						  (RockstarId)m_RosCredentials.GetRockstarId() :
						  m_Profile.GetProfileId();

	Assert(profileId != InvalidRockstarId);

// 	const char* nickname = m_RetrievedGamerInfo ?
// 						   m_GamerInfo.GetNickname() : 
// 						   m_Profile.GetName();

	const char* nickname = (m_Nickname[0] != '\0') ?
						   m_Nickname :
						   m_Profile.GetName();

	unsigned profileNumber = m_IsOfflineProfile ?
							m_Profile.GetProfileNumber() : 
							0;

	Assert(nickname && nickname[0] != '\0');

	// if we're in the launcher, then we need to always save the user's login and password in a temporary
	// file so that when the game launches, it can auto sign in with the same profile selected in the launcher.
	//	As for the MTL, they explicitly exchange their ticket for a game ticket prior to launching the title.
	bool saveSignInTransferData = GetRgscConcreteInstance()->IsLauncher() && !GetRgscConcreteInstance()->IsTitleMTL();

	// Pull the steam ID from the profile, default to the TitleID interface if 0
	u64 steamId = m_Profile.GetSteamId();
	if (steamId == 0)
	{
		steamId = GetRgscConcreteInstance()->GetTitleId()->GetSteamId();
	}

    success = m_Ctx->SaveProfile(&m_Profile.GetUniqueKey(),
                                 profileId,
								 nickname,
								 m_Email,
								 m_Password,
								 m_SaveEmail,
								 m_SavePassword,
                                 m_AutoSignIn,
								 saveSignInTransferData,
                                 gamerPic,
                                 m_IsOfflineProfile,
								 profileNumber,
								 steamId,
								 m_RosCredentials.GetTicket(),
								 m_ScAuthToken,
								 m_MachineToken,
								 savedProfile);

    return success;
}

bool
SignInTask::SyncAchievements()
{
	RgscDisplay("Syncing achievements.");
    return GetRgscConcreteInstance()->_GetAchievementManager()->SyncAchievements(m_SavedProfile.GetProfileId(), &m_MyStatus);
}

bool
SignInTask::SyncFriends()
{
	RgscDisplay("Syncing friends.");
	return rlFriendsManager::Activate(RL_RGSC_GAMER_INDEX) && GetRgscConcreteInstance()->_GetPlayerManager()->SyncPlayers(m_SavedProfile.GetProfileId(), &m_MyStatus);
}

void
SignInTask::NotifyPlatform()
{
	if(m_SigningIn)
	{
		*m_SigningIn = false;
	}

	bool signedIn = m_ConnectedToRos || m_SignedInLocally;

	bool getFriendsAndPlayerInfo = m_ConnectedToRos && GetRgscConcreteInstance()->IsFriendsSupported();
	unsigned numFriendsOnline = 0;
	unsigned numFriendsPlayingSameTitle = 0;
	if(getFriendsAndPlayerInfo)
	{
		GetRgscConcreteInstance()->_GetPlayerManager()->GetNumFriendsOnline(numFriendsOnline, numFriendsPlayingSameTitle);
	}

	unsigned numBlocked = 0;
	unsigned numFriends = 0;
	unsigned numInvitesReceived = 0;
	unsigned numInvitesSent = 0;
	if(getFriendsAndPlayerInfo)
	{
		GetRgscConcreteInstance()->_GetPlayerManager()->GetPlayerListCounts(numBlocked, numFriends, numInvitesReceived, numInvitesSent);
	}

	char rsonBuf[4096] = {0};
	RsonWriter rw;
	rw.Init(rsonBuf, RSON_FORMAT_JSON);
	rw.Begin(NULL, NULL);
	rw.WriteBool("SignedIn", signedIn);
	rw.WriteBool("SignedOnline", m_ConnectedToRos);
	rw.WriteString("ScAuthToken", m_ScAuthToken);
	rw.WriteBool("ScAuthTokenError", m_ScAuthTokenError);
	rw.WriteBool("ProfileSaved", m_SavedProfile.IsValid());
	rw.WriteBool("AchievementsSynced", m_AchievementsSynced);
	rw.WriteBool("FriendsSynced", m_FriendsSynced);
	rw.WriteBool("Local", m_IsOfflineProfile);
	rw.WriteInt("NumFriendsOnline", numFriendsOnline);
	rw.WriteInt("NumFriendsPlayingSameTitle", numFriendsPlayingSameTitle);
	rw.WriteInt("NumBlocked", numBlocked);
	rw.WriteInt("NumFriends", numFriends);
	rw.WriteInt("NumInvitesReceieved", numInvitesReceived);
	rw.WriteInt("NumInvitesSent", numInvitesSent);
	if(m_CallbackData != -1)
	{
		rw.WriteInt("CallbackData", m_CallbackData);
	}
	rw.End();

	rlDebug2("SignInTask::NotifyPlatform:\n\t%s\n", rw.ToString());

	GetRgscConcreteInstance()->_GetUiInterface()->FinishSignIn(rw.ToString());

	// now send an event to the game
	u32 state = 0;
	if(signedIn)
	{
		state |= rgsc::STATE_SIGNED_IN;
	}

	if(m_ConnectedToRos)
	{
		state |= rgsc::STATE_SIGNED_ONLINE;
	}

	RgscDisplay("Signed in %s to profile: %s", m_ConnectedToRos ? "online" : "offline", m_Nickname);

	// Notify the app that the signin state has changed
	GetRgscConcreteInstance()->HandleNotification( rgsc::NOTIFY_SIGN_IN_STATE_CHANGED, &state);

	// make sure this comes after we've sent the online event above.
	if(m_ConnectedToRos)
	{
		GetRgscConcreteInstance()->HandleNotification(rgsc::NOTIFY_ROS_TICKET_CHANGED, m_XmlResponseDecoded);
	}
}

//////////////////////////////////////////////////////////////////////////
// CreateTicketTask
//////////////////////////////////////////////////////////////////////////
bool CreateTicketTask::ProcessResponse(const char* response, int& resultCode)
{
	*m_XmlResponse = StringDuplicate(response);
	return rlRosHttpTask::ProcessResponse(response, resultCode);
}

const char* CreateTicketTask::GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const
{
	//Look up the host name from the service we're calling.
	char servicePath[1024];
	const char* hn = NULL;
	if (GetServicePath(servicePath, sizeof(servicePath)))
	{
		hn = rlRos::GetServiceHost(servicePath, hostnameBuf, sizeofBuf);
	}

	if (!hn)
	{
		//If no service/hostname mapping was found then just use the default.
		if (UseGameServicesAuth())
		{
			hn = formatf(hostnameBuf, sizeofBuf, "auth-%s", g_rlTitleId->m_RosTitleId.GetDomainName());
		}
		else
		{
			hn = formatf(hostnameBuf, sizeofBuf, "auth-%s-%s",
				g_rlTitleId->m_RosTitleId.GetTitleName(),
				g_rlTitleId->m_RosTitleId.GetDomainName());
		}
	}

	return hn;
}

bool CreateTicketTask::GetServicePath(char* svcPath, const unsigned maxLen) const
{
	if (GetRgscConcreteInstance()->GetAuthServiceMethod() == RgscAuthServices::RGSC_AUTH_LEGACY_SVC)
	{
		const rlRosTitleId& titleId = g_rlTitleId->m_RosTitleId;
		formatf(svcPath, maxLen, "%s/%d/wcf%s/%s", titleId.GetTitleName(), titleId.GetTitleVersion(), GetServiceSet(), GetServiceMethod());
		return true;
	}

	return rlRosHttpTask::GetServicePath(svcPath, maxLen);
}

rlRosSecurityFlags CreateTicketTask::GetSecurityFlags() const
{
	// Don't use any security behind Ssl
	return UseHttps() ? RLROS_SECURITY_NONE : RLROS_SECURITY_DEFAULT;
}

bool CreateTicketTask::UseFilter() const
{				
	//WCF services only use HTTPS, not ROS encryption.
	if (GetRgscConcreteInstance()->GetAuthServiceMethod() == RgscAuthServices::RGSC_AUTH_LEGACY_SVC)
		return false;

	return rlRosHttpTask::UseFilter();
}

bool CreateTicketTask::ProcessSuccess(const rlRosResult& /*result*/,  const parTreeNode* node,  int& /*resultCode*/)
{
    rtry
    {
		rverify(node, catchall, );

		rlRosCreateTicketResponse response;
		rverify(rlRosXmlCreateTicketResponseParser::Parse(node, &response), catchall, rlError("Failed to parse create ticket response"));
		rverify(rlRosCreateTicketResponseHelper::ApplyCreateTicketResponse(response, RL_RGSC_GAMER_INDEX, m_Cred), catchall, rlError("Failed to apply create ticket response"));
        return true;
    }
    rcatchall
    {
        return false;
    }
}

#if RSG_DEV
//////////////////////////////////////////////////////////////////////////
// BindSteamAccountTask
//////////////////////////////////////////////////////////////////////////
bool BindSteamAccountTask::Configure(SignInWithoutScUiTask* /*ctx*/, const char* scTicket)
{
	rtry
	{
		rverify(rlRosHttpTask::Configure(NO_LOCAL_GAMER), catchall, );

		rverify(AddStringParameter("ticket", scTicket, true), catchall, );
		rverify(AddIntParameter("steamAppId", GetRgscConcreteInstance()->GetTitleId()->GetSteamAppId(), true), catchall, );
		rverify(AddStringParameter("steamAuthTicket", GetRgscConcreteInstance()->GetTitleId()->GetSteamAuthTicket(), true), catchall, );

		return true;
	}
	rcatchall
	{
		return false;
	}
}

const char* BindSteamAccountTask::GetServiceMethod() const
{
	return "socialclub.asmx/BindSteamAccount"; // todo: WCF?
}


//////////////////////////////////////////////////////////////////////////
// CreateTicketPasswordTask
//////////////////////////////////////////////////////////////////////////
bool CreateTicketPasswordTask::Configure(SignInWithoutScUiTask* /*ctx*/, const char* email, const char* password, rlRosCredentials* cred, char** xmlResponse)
{
	Assert(email);
	Assert(password);
	Assert(cred);

	if(!rlRosHttpTask::Configure(NO_LOCAL_GAMER))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	if( !AddStringParameter("platformName", "pcros", true)
		|| !AddStringParameter("email", email, true)
		|| !AddStringParameter("password", password, true))
	{
		rlTaskError("Failed to add params");
		return false;
	}

	m_Cred = cred;
	m_XmlResponse = xmlResponse;

	return true;
}

const char* CreateTicketPasswordTask::GetServiceMethod() const
{
	if (GetRgscConcreteInstance()->GetAuthServiceMethod() == RgscAuthServices::RGSC_AUTH_LEGACY_SVC)
		return "legacyauth.svc/CreateTicketSc3";
	else
		return "auth.asmx/CreateTicketSc";
}
#endif // #if RSG_DEV

//////////////////////////////////////////////////////////////////////////
// CreateTicketScAuthTokenTask
//////////////////////////////////////////////////////////////////////////
bool CreateTicketScAuthTokenTask::Configure(SignInWithoutScUiTask* /*ctx*/, const char* scAuthToken, rlRosCredentials* cred, char** xmlResponse)
{
	Assert(scAuthToken);
	Assert(cred);

	if(!rlRosHttpTask::Configure(NO_LOCAL_GAMER))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	if( !AddStringParameter("platformName", "pcros", true)
		|| !AddStringParameter("scAuthToken", scAuthToken, true))
	{
		rlTaskError("Failed to add params");
		return false;
	}

	m_Cred = cred;
	m_XmlResponse = xmlResponse;

	return true;
}

const char* CreateTicketScAuthTokenTask::GetServiceMethod() const
{
	if (GetRgscConcreteInstance()->GetAuthServiceMethod() == RgscAuthServices::RGSC_AUTH_LEGACY_SVC)
		return "legacyauth.svc/CreateTicketScAuthToken";
	else
		return "auth.asmx/CreateTicketScAuthToken";
}

//////////////////////////////////////////////////////////////////////////
// CreateTicketRefreshTask
//////////////////////////////////////////////////////////////////////////
bool CreateTicketRefreshTask::Configure(SignInWithoutScUiTask* /*ctx*/, const char* email, const char* scTicket, rlRosCredentials* cred, char** xmlResponse)
{
	Assert(email);
	Assert(scTicket);
	Assert(cred);

	if(!rlRosHttpTask::Configure(NO_LOCAL_GAMER))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	if( !AddStringParameter("platformName", "pcros", true)
		|| !AddStringParameter("nickname", "", true)
		|| !AddStringParameter("password", "", true)
		|| !AddStringParameter("ticket", scTicket, true)
		|| !AddStringParameter("email", email, true))
	{
		rlTaskError("Failed to add params");
		return false;
	}

	m_Cred = cred;
	m_XmlResponse = xmlResponse;

	return true;
}

const char* CreateTicketRefreshTask::GetServiceMethod() const
{
	if (GetRgscConcreteInstance()->GetAuthServiceMethod() == RgscAuthServices::RGSC_AUTH_LEGACY_SVC)
		return "legacyauth.svc/CreateTicketSc3";
	else
		return "auth.asmx/CreateTicketSc3";
}

//////////////////////////////////////////////////////////////////////////
// CreateTicketScSteamTask
//////////////////////////////////////////////////////////////////////////
bool CreateTicketScSteamTask::Configure(SignInWithoutScUiTask* /*ctx*/, const char* email, const char* password, const char* scTicket, rlRosCredentials* cred, char** xmlResponse)
{
	Assert(email);
	Assert(password);
	Assert(scTicket);
	Assert(cred);

	if(!rlRosHttpTask::Configure(NO_LOCAL_GAMER))
	{
		rlTaskError("Failed to configure base class");
		return false;
	}

	bool notify = false;
	GetRgscConcreteInstance()->HandleNotification(rgsc::NOTIFY_REFRESH_STEAM_AUTH_TICKET, &notify);

	// If both email and password are present ,include them
	if (email && email[0] != '\0' && password && password[0] != '\0')
	{
		if (!AddStringParameter("email", email, true) || !AddStringParameter("password", password, true))
		{
			rlTaskError("Failed to add params");
			return false;
		}
	}
	else
	{
		if (!AddStringParameter("email", "", true) || !AddStringParameter("password", "", true))
		{
			rlTaskError("Failed to add params");
			return false;
		}
	}

	// Add the remaining parameters
	if (!AddStringParameter("platformName", "pcros", true) ||
		!AddStringParameter("ticket", scTicket, true) ||
		!AddStringParameter("nickname", "", true) ||
		!AddIntParameter("steamAppId", GetRgscConcreteInstance()->GetTitleId()->GetSteamAppId(), true) ||
		!AddStringParameter("steamAuthTicket", GetRgscConcreteInstance()->GetTitleId()->GetSteamAuthTicket(), true))
	{
		rlTaskError("Failed to add params");
		return false;
	}

	m_Cred = cred;
	m_XmlResponse = xmlResponse;

	return true;
}

const char* CreateTicketScSteamTask::GetServiceMethod() const
{
	if (GetRgscConcreteInstance()->GetAuthServiceMethod() == RgscAuthServices::RGSC_AUTH_LEGACY_SVC)
		return "legacyauth.svc/CreateTicketScSteam2";
	else
		return "auth.asmx/CreateTicketScSteam2";
}

//////////////////////////////////////////////////////////////////////////
// SignInWithoutScUiTask
//////////////////////////////////////////////////////////////////////////
SignInWithoutScUiTask::SignInWithoutScUiTask()
: m_State(STATE_INVALID)
, m_SigningIn(NULL)
, m_XmlResponse(NULL)
, m_HasPassword(0)
, m_HasScAuthToken(0)
, m_HasTicket(0)
{
	m_Email[0] = '\0';
	m_Password[0] = '\0';
	m_ScAuthToken[0] = '\0';
	m_ScTicket[0] = '\0';
}

SignInWithoutScUiTask::~SignInWithoutScUiTask()
{
    if(m_SigningIn)
    {
        *m_SigningIn = false;
    }

	if(m_XmlResponse)
	{
		delete m_XmlResponse;
		m_XmlResponse = NULL;
	}
}

bool
SignInWithoutScUiTask::Configure(RgscProfileManager* ctx, const char* email, const char* password, const char* scAuthToken, const char* scTicket)
{
	bool success = false;

	rtry
	{
 		rverify(m_Ctx->IsSignedInInternal() == false, catchall, );
 		rverify(m_Ctx->IsSigningIn() == false, catchall, );
		rverify(email && (email[0] != '\0'), catchall, );

		m_HasPassword = password && (password[0] != '\0') ? 1 : 0;
		m_HasScAuthToken = scAuthToken && (scAuthToken[0] != '\0') ? 1 : 0;
		m_HasTicket = scTicket && (scTicket[0] != '\0') ? 1 : 0;

		// Maximum one of either 'password', 'auth token' or 'ticket' should be specified -- but on Steam, none are required.
		bool bIsSteam = GetRgscConcreteInstance()->IsSteam();
		if (bIsSteam)
		{
			rverify(m_HasPassword + m_HasScAuthToken + m_HasTicket <= 1, catchall, );
		}
		else
		{
			rverify(m_HasPassword + m_HasScAuthToken + m_HasTicket == 1, catchall, );
		}

		m_State = STATE_INVALID;

		safecpy(m_Email, email);

		if (m_HasPassword)
		{
			safecpy(m_Password, password);
		}
		else if (m_HasScAuthToken)
		{
			safecpy(m_ScAuthToken, scAuthToken);
		}
		else if (m_HasTicket)
		{
			safecpy(m_ScTicket, scTicket);
		}

		m_SigningIn = &m_Ctx->m_SigningIn;
		*m_SigningIn = true;

		m_XmlResponse = NULL;

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void
SignInWithoutScUiTask::Start()
{
    rlDebug2("Signing in without scui...");

    this->ProfileTaskBase::Start();

	m_State = STATE_CREATE_TICKET;
}

void
SignInWithoutScUiTask::Finish(const FinishType finishType, const int resultCode)
{
    rlDebug2("Signing in without scui %s", FinishString(finishType));

    this->ProfileTaskBase::Finish(finishType, resultCode);
}

void
SignInWithoutScUiTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
}

void
SignInWithoutScUiTask::Update(const unsigned timeStep)
{
    this->ProfileTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

    do
    {
        switch(m_State)
        {
		case STATE_CREATE_TICKET:
			if(this->CreateTicket())
			{
				m_State = STATE_CREATING_TICKET;
			}
			else
			{
				// look for local profile?
				m_State = STATE_FAILED;
			}
			break;

		case STATE_CREATING_TICKET:
			if(m_MyStatus.Succeeded())
			{
#if RSG_DEV
				// If we're logging with a password, we may need to bind an unlinked account.
				m_State = (m_HasPassword && GetRgscConcreteInstance()->IsSteam()) ? STATE_BIND_STEAM_ACCOUNT : STATE_SIGN_IN;
#else
				m_State = STATE_SIGN_IN;
#endif
			}
			else if(!m_MyStatus.Pending())
			{
				// look for local profile?
				m_State = STATE_FAILED;
			}
			break;
#if RSG_DEV
		case STATE_BIND_STEAM_ACCOUNT:
			if (this->BindSteamAccount())
			{
				m_State = STATE_BINDING_STEAM_ACCOUNT;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;
		case STATE_BINDING_STEAM_ACCOUNT:
			if(m_MyStatus.Succeeded())
			{
				m_State = STATE_SIGN_IN;
			}
			else if(!m_MyStatus.Pending())
			{
				// look for local profile?
				m_State = STATE_FAILED;
			}
			break;
#endif
		case STATE_SIGN_IN:
			if(this->SignIn())
			{
				m_State = STATE_SIGNING_IN;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_SIGNING_IN:
			// Wait for the ProfileManager's SignIn/Modify tasks to complete.
			if (!m_Ctx->IsPending())
			{
				if(m_Ctx->IsSignedInInternal())
				{
					m_MyStatus.SetSucceeded();
					m_State = STATE_SUCCEEDED;
				}
				else if(!m_Ctx->IsSigningIn())
				{
					m_MyStatus.SetFailed();
					m_State = STATE_FAILED;
				}
			}
			break;

        case STATE_SUCCEEDED:
            this->Finish(FINISH_SUCCEEDED);
            break;

        case STATE_FAILED:
			Assertf(false, "Signing in without the scui using email '%s' failed", m_Email);
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
            break;
        }
    }
    while(!m_MyStatus.Pending() && this->IsActive());
}

bool 
SignInWithoutScUiTask::CreateTicket()
{
	netStatus* status = &m_MyStatus;

	if (GetRgscConcreteInstance()->IsSteam())
	{
		return CreateTicketWithSteam(status);
	}
	else if (m_HasTicket)
	{
		return RefreshTicket(status);
	}
	else if (m_HasScAuthToken)
	{
		return CreateTicketWithScAuthToken(status);
	}
#if RSG_DEV
	else if (m_HasPassword)
	{
		return CreateTicketWithPassword(status);
	}
#endif // RSG_DEV

	return false;
}

bool 
SignInWithoutScUiTask::SignIn()
{
	*m_SigningIn = false;
	bool isOfflineProfile = false;
	const rlScAccountInfo& acct = m_RosCred.GetRockstarAccount();
	m_MyStatus.SetPending();

	return m_Ctx->SignIn(m_Email,
						 m_Password,
						 acct.m_Nickname,
						 acct.m_AvatarUrl,
						 true,
						 true,
						 false,
						 (m_HasPassword || m_HasScAuthToken) && !PARAM_scNoAutologin.Get(), // -scemail login autologins by default
						 m_XmlResponse,
						 acct.m_RockstarId,
						 isOfflineProfile,
						 -1);
}

#if RSG_DEV
bool SignInWithoutScUiTask::CreateTicketWithPassword(netStatus* status)
{
	RLPC_CREATETASK(CreateTicketPasswordTask, m_Email, m_Password, &m_RosCred, &m_XmlResponse, status);
}
#endif

bool SignInWithoutScUiTask::CreateTicketWithScAuthToken(netStatus* status)
{
	RLPC_CREATETASK(CreateTicketScAuthTokenTask, m_ScAuthToken, &m_RosCred, &m_XmlResponse, status);
}

bool SignInWithoutScUiTask::RefreshTicket(netStatus* status)
{
	RLPC_CREATETASK(CreateTicketRefreshTask, m_Email, m_ScTicket, &m_RosCred, &m_XmlResponse, status);
}

bool SignInWithoutScUiTask::CreateTicketWithSteam(netStatus* status)
{
	RLPC_CREATETASK(CreateTicketScSteamTask, m_Email, m_Password, m_ScTicket, &m_RosCred, &m_XmlResponse, status);
}

#if RSG_DEV
bool SignInWithoutScUiTask::BindSteamAccount()
{
	netStatus* status = &m_MyStatus;
	RLPC_CREATETASK(BindSteamAccountTask, m_RosCred.GetTicket(), status);
}
#endif

///////////////////////////////////////////////////////////////////////////////
//  ModifyProfileTask
///////////////////////////////////////////////////////////////////////////////

ModifyProfileTask::ModifyProfileTask()
: m_State(STATE_INVALID)
, m_RetrievedAvatar(false)
, m_RetrievedScAuthToken(false)
, m_OfflineProfile(false)
, m_AutoSignIn(-1)
, m_SaveEmail(-1)
{
	sysMemSet(m_Email, 0, sizeof(m_Email));
	sysMemSet(m_Password, 0, sizeof(m_Password));
	sysMemSet(m_Nickname, 0, sizeof(m_Nickname));
	sysMemSet(m_ScAuthToken, 0, sizeof(m_ScAuthToken));
	sysMemSet(m_AvatarUrl, 0, sizeof(m_AvatarUrl));
	sysMemSet(m_MachineToken, 0, sizeof(m_MachineToken));
}

ModifyProfileTask::~ModifyProfileTask()
{
	// clear from memory on task cleanup
	sysMemSet(m_Email, 0, sizeof(m_Email));
	sysMemSet(m_Password, 0, sizeof(m_Password));
	sysMemSet(m_ScAuthToken, 0, sizeof(m_ScAuthToken));
	sysMemSet(m_MachineToken, 0, sizeof(m_MachineToken));
}

bool
ModifyProfileTask::Configure(RgscProfileManager* /*ctx*/,
												 const char* email,
												 const char* password,
												 const char* nickname,
												 const char* avatarUrl,
												 const int autoSignIn,
												 const int saveEmail)
{
	bool success = false;

	rtry
	{
		rverify(m_Ctx->IsSignedInInternal(), catchall, );

		const RgscProfile& signedInProfile = m_Ctx->GetSignedInProfile();
		rverify(signedInProfile.IsValid(), catchall, );
		m_OfflineProfile = signedInProfile.IsOfflineProfile();
		rverify(m_OfflineProfile || m_Ctx->IsOnline(), catchall, );

		m_State = STATE_INVALID;
		m_RetrievedAvatar = false;
		m_RetrievedScAuthToken = false;
		m_SavedProfile.Clear();
		m_AutoSignIn = autoSignIn;
		m_SaveEmail = saveEmail;

		// if autoSignIn is set to '1' (on)...
		if (autoSignIn == 1)
		{
			m_SaveEmail = autoSignIn; // implicitly true for autologin
		}

		if(m_OfflineProfile)
		{
			// can't change the e-mail or password of an offline profile
			rverify(email == NULL, catchall, );
			rverify(password == NULL, catchall, );
			rverify(signedInProfile.IsLoginSaved(), catchall, );
			rverify(signedInProfile.IsPasswordSaved(), catchall, );

			safecpy(m_Email, signedInProfile.GetSavedLogin());
			safecpy(m_Password, signedInProfile.GetSavedPassword());
		}
		else
		{
			if((email == NULL) || (email[0] == '\0'))
			{
				if(signedInProfile.IsLoginSaved())
				{
					safecpy(m_Email, signedInProfile.GetSavedLogin());
				}
				else
				{
					safecpy(m_Email, m_Ctx->m_SignedInEmail);
				}
			}
			else
			{
				safecpy(m_Email, email);
			}

			if(password != NULL)
			{
				safecpy(m_Password, password);
			}
		}
		
		if((nickname == NULL) || (nickname[0] == '\0'))
		{
			safecpy(m_Nickname, signedInProfile.GetName());
		}
		else
		{
			safecpy(m_Nickname, nickname);
		}

		if((avatarUrl == NULL) || (avatarUrl[0] == '\0'))
		{
			safecpy(m_AvatarUrl, signedInProfile.GetGamerPicName());
		}
		else
		{
			safecpy(m_AvatarUrl, avatarUrl);
		}

		safecpy(m_MachineToken, signedInProfile.GetMachineToken());

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void
ModifyProfileTask::Start()
{
	rlDebug2("Modifying profile...");

	this->ProfileTaskBase::Start();

	// If we've just enabled auto signin (== 1), we must refresh it.
	if (m_AutoSignIn == 1)
	{
		m_State = STATE_GET_SC_AUTHTOKEN;
	}
	else
	{
		m_State = STATE_SAVE_PROFILE;
	}
}

void
ModifyProfileTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("Modifying profile %s", FinishString(finishType));

	this->ProfileTaskBase::Finish(finishType, resultCode);
}

void
ModifyProfileTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
}

void
ModifyProfileTask::Update(const unsigned timeStep)
{
	this->ProfileTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do
	{
		switch(m_State)
		{
		case STATE_GET_SC_AUTHTOKEN:
			if(this->GetScAuthToken())
			{
				m_State = STATE_GETTING_SC_AUTHTOKEN;
			}
			else
			{
				// Always save profile in ModifyProfile task, we never want
				// to ignore their updated email/autosignin settings 
				m_State = STATE_SAVE_PROFILE;
			}
			break;
		case STATE_GETTING_SC_AUTHTOKEN:
			if(m_MyStatus.Succeeded())
			{
				// Flag the fact that we've refreshed our ScAuthToken.
				m_RetrievedScAuthToken = true;

				m_State = STATE_SAVE_PROFILE;
			}
			else if(!m_MyStatus.Pending())
			{
				// Always save profile in ModifyProfile task, we never want
				// to ignore their updated email/autosignin settings 
				m_State = STATE_SAVE_PROFILE;
			}
			break;
		case STATE_SAVE_PROFILE:
			if(this->SaveProfile(m_SavedProfile))
			{
				Assert(m_SavedProfile.IsValid());
				Assert(m_Ctx->m_SignedInProfile.IsValid());
				m_Ctx->m_SignedInProfile.Clear();
				sysMemSet(m_Ctx->m_SignedInEmail, 0, sizeof(m_Ctx->m_SignedInEmail));
				m_Ctx->m_SignedInProfile = m_SavedProfile;
				safecpy(m_Ctx->m_SignedInEmail, m_Email);

				// if we retrieved a new auth token
				if (!StringNullOrEmpty(m_ScAuthToken))
				{
					safecpy(m_Ctx->m_SignedInScAuthToken, m_ScAuthToken);
				}

				if(m_OfflineProfile)
				{
					m_State = STATE_SUCCEEDED;
				}
				else
				{
					DownloadGamerPic();
					m_State = STATE_SUCCEEDED;
				}
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;
		case STATE_SUCCEEDED:
			NotifyPlatform();
			this->Finish(FINISH_SUCCEEDED);
			break;

		case STATE_FAILED:
			NotifyPlatform();
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool
ModifyProfileTask::GetScAuthToken()
{
	const char* audience = DEFAULT_SC_AUTH_AUDIENCE;
	const int ttlMinutes = DEFAULT_SC_AUTH_TTL_MINUTES;

	bool saveMachine = false;

	bool keepMeSignedIn = false;
	if (m_AutoSignIn == 1)
	{
		saveMachine = true;
		keepMeSignedIn = true;
	}

	char fingerprint[4096] = {0};
	RsonWriter rw;
	rw.Init(fingerprint, RSON_FORMAT_JSON);
	rw.Begin(NULL, NULL);
	rw.Begin("fp", NULL);
	GetRgscConcreteInstance()->_GetMultiFactorAuth()->GetFingerPrintRSON(&rw);
	rw.End();
	rw.End();

	const rlRosCredentials& creds = rlRos::GetCredentials(RL_RGSC_GAMER_INDEX);
	return rlSocialClub::CreateScAuthToken2(RL_RGSC_GAMER_INDEX, creds.GetTicket(), 
											m_ScAuthToken, sizeof(m_ScAuthToken), 
											m_MachineToken, sizeof(m_MachineToken),
										    saveMachine, keepMeSignedIn, 
											fingerprint, audience, ttlMinutes, NULL, &m_MyStatus);
}

bool
ModifyProfileTask::SaveProfile(RgscProfile &savedProfile)
{
	bool success = false;

	const RgscProfile& signedInProfile = m_Ctx->GetSignedInProfile();
	bool autoSignIn = false;
	bool saveEmail = false;

	// m_AutoSignIn is -1 if not modified, 0 if set to false, 1 if set to true
	if(m_AutoSignIn == -1)
	{
		// use whatever setting we had already
		autoSignIn = GetRgscConcreteInstance()->_GetProfileManager()->IsAutoSignInProfile(signedInProfile);
	}
	else
	{
		// modify it to the desired setting
		autoSignIn = (m_AutoSignIn == 1) && m_RetrievedScAuthToken;
	}

	// m_SaveEmail is -1 if not modified, 0 if set to false, 1 if set to true
	if (m_SaveEmail == -1)
	{
		saveEmail = signedInProfile.IsLoginSaved();
	}
	else
	{
		// modify it to the desired setting
		saveEmail = (m_SaveEmail == 1);
	}

	// Pull the steam ID from the profile, default to the TitleID interface if 0
	u64 steamId = savedProfile.GetSteamId();
	if (steamId == 0)
	{
		steamId = GetRgscConcreteInstance()->GetTitleId()->GetSteamId();
	}

	// If we didn't retrieve a new ScAuthToken, re-use the existing values.
	if (!m_RetrievedScAuthToken)
	{
		// Only if autosignin is enabled.
		if (signedInProfile.IsAuthTokenSaved() && autoSignIn)
		{
			safecpy(m_ScAuthToken, signedInProfile.GetSavedAuthToken());
		}
		
		// We can always recover the saved machine token
		if (signedInProfile.IsMachineTokenSaved())
		{
			safecpy(m_MachineToken, signedInProfile.GetMachineToken());
		}
	}

	success = m_Ctx->SaveProfile(&signedInProfile.GetUniqueKey(),
								 signedInProfile.GetProfileId(),
								 m_Nickname,
								 m_Email,
								 m_Password,
								 saveEmail || autoSignIn,
								 signedInProfile.IsPasswordSaved() || autoSignIn,
								 autoSignIn,
								 false,
								 m_AvatarUrl,
								 signedInProfile.IsOfflineProfile(),
								 signedInProfile.GetProfileNumber(),
								 steamId,
								 "",
								 m_ScAuthToken,
								 m_MachineToken,
								 savedProfile);

	return success;
}

bool
ModifyProfileTask::DownloadGamerPic()
{
	// if avatarUrl is an empty string, then the user isn't modifying their avatar
	bool success = true;

	if(m_AvatarUrl[0] != '\0')
	{
		success = GetRgscConcreteInstance()->_GetGamerPicManager()->QueueDownloadGamerPic(m_AvatarUrl);
		m_RetrievedAvatar = success;
	}

	return success;
}

void
ModifyProfileTask::NotifyPlatform()
{
	char rsonBuf[4096] = {0};
	RsonWriter rw(rsonBuf, RSON_FORMAT_JSON);
	rw.Begin(NULL, NULL);
	rw.WriteBool("ProfileSaved", m_SavedProfile.IsValid());
	rw.WriteBool("DownloadedAvatar", m_RetrievedAvatar);
	rw.WriteBool("DownloadedAuthToken", m_RetrievedScAuthToken);
	rw.WriteString("ScAuthToken", m_ScAuthToken);
	rw.End();

	rlDebug2("ModifyProfileTask::NotifyPlatform:\n%s\n", rw.ToString());

	GetRgscConcreteInstance()->_GetUiInterface()->FinishModifyProfile(rw.ToString());
}

///////////////////////////////////////////////////////////////////////////////
//  RgscProfileManager::LauncherTicketExchangeTask
///////////////////////////////////////////////////////////////////////////////
CreateTitleSpecificTransferProfileTask::CreateTitleSpecificTransferProfileTask()
	: m_RosTitleId(0)
	, m_State(STATE_INVALID)
{
	m_Email[0] = '\0';
	m_Credentials.Clear();
}

CreateTitleSpecificTransferProfileTask::~CreateTitleSpecificTransferProfileTask()
{
	// clear memory associated with the email
	sysMemSet(m_Email, 0, sizeof(m_Email));
}

bool CreateTitleSpecificTransferProfileTask::Configure(RgscProfileManager* ctx, int rosTitleId, const char* email)
{
	rtry
	{
		rverify(rosTitleId > 0, catchall, );
		rverify(email && email[0] != '\0', catchall, );
		
		m_Ctx = ctx;
		
		m_RosTitleId = rosTitleId;
		safecpy(m_Email, email);

		m_State = STATE_EXCHANGE_TICKET;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void CreateTitleSpecificTransferProfileTask::Start()
{
	this->rgscTask<RgscProfileManager>::Start();
}

void CreateTitleSpecificTransferProfileTask::Update(const unsigned timeStep)
{
	this->rgscTask<RgscProfileManager>::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_INVALID:
			break;
		case STATE_EXCHANGE_TICKET:
			if (ExchangeTicket())
			{
				m_State = STATE_EXCHANGING_TICKET;
			}
			else
			{
				this->Finish(FINISH_FAILED);
			}
			break;
		case STATE_EXCHANGING_TICKET:
			if (m_MyStatus.Succeeded())
			{
				const RgscProfile& profile = m_Ctx->GetSignedInProfile();
				if (m_Ctx->SaveSignInTransferData(profile, m_Email, m_Credentials.GetTicket(), ""))
				{
					this->Finish(FINISH_SUCCEEDED);
				}
				else
				{
					this->Finish(FINISH_FAILED);
				}
			}
			else if (!m_MyStatus.Pending())
			{
				this->Finish(FINISH_FAILED);
			}
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

void CreateTitleSpecificTransferProfileTask::Finish(const FinishType finishType, const int resultCode)
{
	this->rgscTask<RgscProfileManager>::Finish(finishType, resultCode);
}

bool CreateTitleSpecificTransferProfileTask::IsCancelable() const
{
	return true;
}

void CreateTitleSpecificTransferProfileTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
	rgscTask::DoCancel();
}

bool CreateTitleSpecificTransferProfileTask::ExchangeTicket()
{
	bool success = false;

	rlRosTicketExchangeTask* task = NULL;

	rtry
	{
		rverify(rlGetTaskManager()->CreateTask(&task),catchall,);
		rverify(rlTaskBase::Configure(task, m_RosTitleId, &m_Credentials, &m_MyStatus), catchall,);
		rverify(rlGetTaskManager()->AddParallelTask(task), catchall,);

		success = true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}
	}

	return success;
}

} // namespace rgsc