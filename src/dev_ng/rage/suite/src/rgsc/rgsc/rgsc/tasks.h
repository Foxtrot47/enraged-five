// 
// rline/rlpctask.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPCTASK_H
#define RLINE_RLPCTASK_H

#include "file/file_config.h"

#if RSG_PC

#include "diag/seh.h"
#include "tasks_interface.h"
#include "net/status.h"
#include "net/task.h"
#include "rline/rltask.h"
#include "system/criticalsection.h"

using namespace rage;

namespace rgsc
{

///////////////////////////////////////////////////////////////////////////////
//  Convenience macros to create and queue tasks
///////////////////////////////////////////////////////////////////////////////
#define RLPC_CREATETASK_IASYNC(T, ...)														\
    bool success = false;																	\
    T* task = NULL;																			\
    rtry																					\
    {																						\
        task = GetRgscConcreteInstance()->_GetTaskManager()->CreateTask<T>();				\
        rverify(task,catchall,);															\
		AsyncStatus* asyncStatus = NULL;													\
		netStatus* rageStatus = NULL;														\
		if(status)																			\
		{																					\
			RGSC_HRESULT hr = status->QueryInterface(IID_IAsyncStatusV1, (void**) &asyncStatus);	\
			rverify(SUCCEEDED(hr) && (asyncStatus != NULL), catchall, );					\
			rageStatus = asyncStatus->GetNetStatus();										\
		}																					\
		else																				\
		{																					\
			rageStatus = task->GetRageStatus();												\
		}																					\
		task->SetAsyncStatus(asyncStatus);													\
        rverify(rlTaskBase::Configure(task, this, __VA_ARGS__, rageStatus), catchall,);		\
        rverify(GetRgscConcreteInstance()->_GetTaskManager()->AddParallelTask(task), catchall,);					\
        success = true;																		\
    }																						\
    rcatchall																				\
    {																						\
        if(task)																			\
        {																					\
			task->Finish(rlTaskBase::FINISH_FAILED, -1);									\
            GetRgscConcreteInstance()->_GetTaskManager()->DestroyTask(task);				\
        }																					\
    }																						\
    return success;

#define RLPC_CREATETASK(T, ...)																\
    bool success = false;																	\
    T* task = NULL;																			\
    rtry																					\
    {																						\
        task = GetRgscConcreteInstance()->_GetTaskManager()->CreateTask<T>();				\
        rverify(task,catchall,);															\
        rcheck(rlTaskBase::Configure(task, this, __VA_ARGS__), catchall,);					\
        rverify(GetRgscConcreteInstance()->_GetTaskManager()->AddParallelTask(task), catchall,);					\
        success = true;																		\
    }																						\
    rcatchall																				\
    {																						\
        if(task)																			\
        {																					\
            if(task->IsPending())															\
            {																				\
                task->Finish(rlTaskBase::FINISH_FAILED, -1);								\
            }																				\
            else																			\
            {																				\
                status->ForceFailed();														\
            }																				\
            GetRgscConcreteInstance()->_GetTaskManager()->DestroyTask(task);				\
        }																					\
    }																						\
    return success;

#define RLPC_CREATETASK_NO_STATUS(T, ...)													\
    RGSC_HRESULT success = RGSC_FAIL;																\
    T* task = NULL;																			\
    rtry																					\
    {																						\
        task = GetRgscConcreteInstance()->_GetTaskManager()->CreateTask<T>();				\
        rverify(task,catchall,);															\
        rverify(rlTaskBase::Configure(task, this, __VA_ARGS__, task->GetRageStatus()), catchall,);		\
        rverify(GetRgscConcreteInstance()->_GetTaskManager()->AddParallelTask(task), catchall,);	\
        success = RGSC_OK;																		\
    }																						\
    rcatchall																				\
    {																						\
        if(task)																			\
        {																					\
            if(task->IsPending())															\
            {																				\
                task->Finish(rlTaskBase::FINISH_FAILED, -1);								\
            }																				\
            else																			\
            {																				\
                task->GetRageStatus()->ForceFailed();										\
            }																				\
            GetRgscConcreteInstance()->_GetTaskManager()->DestroyTask(task);				\
        }																					\
    }																						\
    return success;


class AsyncStatus : public IAsyncStatusLatestVersion
{
// ===============================================================================================
// inherited from public interfaces
// ===============================================================================================
public:
	// ===============================================================================================
	// inherited from IRgscUnknown
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);
	unsigned int RGSC_CALL AddRef();
	unsigned int RGSC_CALL Release();

	// ===============================================================================================
	// inherited from IAsyncStatusV1
	// ===============================================================================================
	void RGSC_CALL SetOperationFinishedCallback(OperationFinishedCallback callback, void* callbackData);
	int RGSC_CALL GetResultCode();
	void RGSC_CALL Cancel();
	bool RGSC_CALL Pending();
	bool RGSC_CALL Succeeded();
	bool RGSC_CALL Failed();
	bool RGSC_CALL Canceled();

// ===============================================================================================
// accessible from anywhere in the dll
// ===============================================================================================
public:
	AsyncStatus() : m_RefCount(0), m_Callback(NULL), m_CallbackData(NULL) {Displayf("AsyncStatus created");}
	virtual ~AsyncStatus() {Displayf("AsyncStatus destroyed");}

	netStatus* GetNetStatus() {return &m_Status;}
	OperationFinishedCallback GetOperationFinishedCallback() {return m_Callback;}
	void* GetOperationFinishedCallbackData() {return m_CallbackData;}

// ===============================================================================================
// only accessible from tasks.cpp
// ===============================================================================================
private:
	unsigned int m_RefCount;
	netStatus m_Status;
	OperationFinishedCallback m_Callback;
	void* m_CallbackData;
};

template<typename CTX>
class rgscTask : public rlTask<CTX>
{
public:

	rgscTask() : m_AsyncStatus(NULL)
	{
	}
	
	void SetAsyncStatus(AsyncStatus* status) {m_AsyncStatus = status;}
	netStatus* GetRageStatus() {return &m_RageStatus;}
protected:

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel()
	{
		if(m_AsyncStatus)
		{
			IAsyncStatusLatestVersion::OperationFinishedCallback callback = m_AsyncStatus->GetOperationFinishedCallback();
			if(callback)
			{
				callback(m_AsyncStatus, m_AsyncStatus->GetOperationFinishedCallbackData());
			}
		}

		m_AsyncStatus = NULL;
	}

	virtual void Finish(const FinishType finishType, const int resultCode)
	{
		if(this->IsPending())
		{
			this->rlTask<CTX>::Finish(finishType, resultCode);
		}
		else
		{
			if(m_AsyncStatus && m_AsyncStatus->GetNetStatus())
			{
				m_AsyncStatus->GetNetStatus()->ForceFailed();
			}
		}

		m_Ctx = NULL;

		if(m_AsyncStatus)
		{
			IAsyncStatusLatestVersion::OperationFinishedCallback callback = m_AsyncStatus->GetOperationFinishedCallback();
			if(callback)
			{
				callback(m_AsyncStatus, m_AsyncStatus->GetOperationFinishedCallbackData());
			}
		}
	}

	//Context on which the task operates.
	AsyncStatus* m_AsyncStatus;
	netStatus m_RageStatus;
};

class RgscTaskManager : public ITaskManagerLatestVersion
{
// ===============================================================================================
// inherited from public interfaces
// ===============================================================================================
public:
	// ===============================================================================================
	// inherited from IRgscUnknown
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

	// ===============================================================================================
	// inherited from ITaskManagerV1
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL CreateAsyncStatus(IAsyncStatus** status);

// ===============================================================================================
// accessible from anywhere in the dll
// ===============================================================================================
public:
    RgscTaskManager();
    virtual ~RgscTaskManager();

    bool Init();
    void Shutdown();
    void Update();

    template<class T>
    T* CreateTask()
    {
		return m_TaskMgr.CreateTask<T>();
	}

	static void RgscTaskManager::CancelTaskIfNecessary(netStatus* status);
	bool AddParallelTask(rlTaskBase* task);
	void CancelTask(netStatus* status);
	void DestroyTask(rlTaskBase* task);

private:

	class TaskQueue
	{
	public:

		TaskQueue()
		{
			this->Clear();
		}

		void Clear()
		{
			SYS_CS_SYNC(m_Cs);

			m_Count = m_Next = m_Free = 0;
		}

		bool QueueTask(rlTaskBase* task)
		{
			SYS_CS_SYNC(m_Cs);

			bool success = AssertVerify(m_Count < QUEUE_SIZE);
			if(success)
			{
				m_Queue[m_Free] = task;
				++m_Count;
				++m_Free;
				if(m_Free >= QUEUE_SIZE)
				{
					m_Free = 0;
				}
			}

			return success;
		}

		bool NextTask(rlTaskBase** task)
		{
			SYS_CS_SYNC(m_Cs);

			Assert(m_Count >= 0);
			Assert(m_Count <= QUEUE_SIZE);

			bool success = false;
			if(m_Count > 0)
			{
				*task = m_Queue[m_Next];
				--m_Count;
				++m_Next;
				if(m_Next >= QUEUE_SIZE)
				{
					m_Next = 0;
				}

				success = true;
			}

			return success;
		};

		unsigned GetCount() const
		{
			return (unsigned) m_Count;
		}

	private:

		sysCriticalSectionToken m_Cs;
		static const u8 QUEUE_SIZE = 100;
		rlTaskBase* m_Queue[QUEUE_SIZE];
		int m_Count;
		int m_Next;
		int m_Free;
	};

    unsigned int m_ThreadId;
    TaskQueue m_TaskQueue;
	rlTaskManager m_TaskMgr;
};

///////////////////////////////////////////////////////////////////////////////
//  SimpleOperationBaseTask
///////////////////////////////////////////////////////////////////////////////

template<typename T>
class SimpleOperationBaseTask : public rgscTask<T>
{
public:
	RL_TASK_DECL(SimpleOperationBaseTask);

	enum State
	{
		STATE_INVALID   = -1,
		STATE_RUN,
		STATE_RUNNING,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	SimpleOperationBaseTask() :
		m_Callback(NULL)
	{

	}

	bool Configure(T* ctx,
		RockstarId rockstarId)
	{
		bool success = false;

		rtry
		{
			m_RockstarId = rockstarId;

			rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsOnline(), catchall, );

			m_State = STATE_INVALID;

			success = true;
		}
		rcatchall
		{

		}

		return success;
	}

	typedef void (*JsonCallback)(const bool success, const RockstarId rockstarId, const int resultCode, int requestId);

	bool Configure(T* ctx,
		RockstarId rockstarId,
		JsonCallback callback,
		int requestId)
	{
		m_Callback = callback;
		m_RequestId = requestId;
		return Configure(m_Ctx, rockstarId);
	}

	virtual void Start()
	{
		rlDebug2("Starting simple base operation...");
		this->rgscTask<T>::Start();
		m_State = STATE_RUN;
	}

	virtual void Finish(const FinishType finishType, const int resultCode = 0)
	{
		//rlDebug2("Simple base operation %s (%ums)", FinishString(finishType), GetElapsedTime());
		this->rgscTask<T>::Finish(finishType, resultCode);
	}

	void DoCancel()
	{
		RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
		rgscTask::DoCancel();
	}

	virtual void Update(const unsigned timeStep)
	{
		this->rgscTask<T>::Update(timeStep);

		if(this->WasCanceled())
		{
			//Wait for dependencies to finish
			if(!m_MyStatus.Pending())
			{
				this->Finish(FINISH_CANCELED);
			}

			return;
		}

		do 
		{
			switch(m_State)
			{
			case STATE_RUN:
				if(this->PerformOperation())
				{
					m_State = STATE_RUNNING;
				}
				else
				{
					m_State = STATE_FAILED;
				}
				break;

			case STATE_RUNNING:
				if(m_MyStatus.Succeeded())
				{
					m_State = STATE_SUCCEEDED;
				}
				else if(!m_MyStatus.Pending())
				{
					m_State = STATE_FAILED;
				}
				break;

			case STATE_SUCCEEDED:
				Callback(true);
				this->Finish(FINISH_SUCCEEDED);
				break;

			case STATE_FAILED:
				Callback(false);
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
				break;
			}
		}
		while(!m_MyStatus.Pending() && this->IsActive());
	}

protected:
	virtual bool PerformOperation() = 0;
	virtual void ProcessResult(bool success) {};
	void Callback(bool success)
	{
		ProcessResult(success);

		if(m_Callback)
		{
			m_Callback(success, m_RockstarId, m_MyStatus.GetResultCode(), m_RequestId);
		}
	}

	// parameters and returns
	RockstarId m_RockstarId;
	JsonCallback m_Callback;
	int m_RequestId;

	int m_State;
	netStatus m_MyStatus;
};

}

#endif // RSG_PC

#endif  //RLINE_RLPCTASK_H
