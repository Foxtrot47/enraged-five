// 
// rgsc/profiles.h
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPCPROFILE_H 
#define RLINE_RLPCPROFILE_H

#include "file/file_config.h"
#include "profiles_interface.h"

#if RSG_PC

//#include "presence.h"
#include "gamerpics.h"
#include "atl/array.h"
#include "atl/map.h"
#include "data/datprotect.h"
#include "data/sha1.h"
#include "json.h"
#include "net/status.h"
#include "rline/ros/rlroscommon.h"
#include "rline/rltask.h"

using namespace rgsc;

namespace rgsc
{
#if __RAGE_DEPENDENCY
	class grcImage;
#endif

class FileDevice;

class UniqueKey
{
public:
	static const unsigned int SIZE_OF_KEY_IN_BYTES = 256 >> 3;

	UniqueKey()
	{
		Clear();
	}

	~UniqueKey()
	{
		Clear();
	}

	void Clear()
	{
		memset(m_UniqueKey, 0, sizeof(m_UniqueKey));
	}

	bool IsSet() const
	{
		u8 zero[SIZE_OF_KEY_IN_BYTES];
		memset(zero, 0, sizeof(zero));

		return memcmp(m_UniqueKey, zero, sizeof(m_UniqueKey)) != 0;
	}

	const u8 (&GetKey() const)[SIZE_OF_KEY_IN_BYTES]
	{
		return m_UniqueKey;
	}

	void SetKey(u8 (&key)[SIZE_OF_KEY_IN_BYTES])
	{
		memcpy(m_UniqueKey, key, sizeof(m_UniqueKey));
	}

	void Generate();
	void GenerateFromRockstarId(RockstarId rockstarId);

private:
	u8 m_UniqueKey[SIZE_OF_KEY_IN_BYTES];
};

class RgscProfile
{
    friend class RgscProfileManager;
    friend class RgscProfiles;
    friend class rlPc;
	friend class rlGamerHandle;
	friend class AchievementsFile;

public:
    RgscProfile();
    ~RgscProfile();

    const char* GetName() const;
    time_t GetLastSignInTime() const;

    bool IsLoginSaved() const;
    const char* GetSavedLogin() const;

    bool IsPasswordSaved() const;
    const char* GetSavedPassword() const;

	bool IsAuthTokenSaved() const;
	const char* GetSavedAuthToken() const;

    const char* GetGamerPicName() const;

	bool IsMachineTokenSaved() const;
	const char* GetMachineToken() const;

#if __RAGE_DEPENDENCY
    // Note: the caller is responsible for releasing the image
    grcImage* GetGamerPic(bool largeVersion) const;
#endif

	// Note: the caller is responsible for releasing the image
	// Returns the number of bytes read in sizeInBytes
	u8* GetGamerPicRaw(IGamerPicManager::AvatarSize avatarSize, unsigned int* sizeInBytes) const;

    bool IsOfflineProfile() const;

	unsigned GetProfileNumber() const;

//private:

    RgscProfile(unsigned folderId,
                RockstarId profileId,
				const UniqueKey& key,
                const char* name,
                const char* login,
                const char* password,
                bool saveLogin,
                bool savePassword,
				bool saveScAuthToken,
                const char* gamerPic,
                bool offlineProfile,
				unsigned profileNumber,
				u64 steamId,
				const char* scAuthToken,
				const char* machineToken);

    void Clear();

    bool IsValid() const;
    unsigned GetVersion() const;
    RockstarId GetProfileId() const;
    unsigned GetFolderId() const;
	const UniqueKey& GetUniqueKey() const;
    bool IsFullyDecrypted() const;
	u64 GetSteamId() const;

    bool Authenticate(const char* login, const char* password) const;

	bool ToJson(void *p, const char* transitionLogin = NULL, const char* transitionTicket = NULL, const char* transitionScAuthToken = NULL) const;

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(const char* login, const char* password, const char* scAuthToken, void* buf, const unsigned sizeofBuf, unsigned* size = 0) const;

	// PURPOSE
	//  Implements the V1 interface of Export
	bool ExportV1(datExportBuffer& bb, const char* login, const char* password, void* buf, const unsigned sizeofBuf, unsigned* size = 0) const;

	// PURPOSE
	//  Implements the V2 interface of Export
	bool ExportV2(datExportBuffer& bb, const char* login, const char* password, const char* scAuthToken, void* buf, const unsigned sizeofBuf, unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //	folderId	- the identifier that uniquely describes where to find the profile on disk
    //  login		- the user's login that was used to encrypt the data
    //  password	- the user's password that was used to encrypt the data
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(unsigned folderId, const char* login, const char* password, const char* scAuthToken,  const void* buf, const unsigned sizeofBuf, unsigned* size = 0);

	// PURPOSE
	//	Imports data exported with ExportV1. Is the V1 implementation of Import();
	bool ImportV1(datImportBuffer& bb, const char* login, const char* password, const void* buf, const unsigned sizeofBuf);

	// PURPOSE
	//	Imports data exported with ExportV2. Is the V2 implementation of Import();
	bool ImportV2(datImportBuffer& bb, const char* login, const char* password, const char* scAuthToken, const void* buf, const unsigned sizeofBuf);

	static const unsigned RGSC_PROFILE_VERSION_UNKNOWN = 0;

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Version 1 of the Profile - Username + Password
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static const unsigned RGSC_PROFILE_VERSION_1 = 1;
	static const unsigned INITIAL_RESERVED_SPACE_V1 = 128;

    static const unsigned MAX_BYTE_SIZEOF_PAYLOAD_V1 = sizeof(unsigned) +					// m_Version
														sizeof(RockstarId) +				// m_ProfileId
														UniqueKey::SIZE_OF_KEY_IN_BYTES +	// m_UniqueKey
														RGSC_MAX_NAME_CHARS +				// m_Name
														RGSC_MAX_EMAIL_CHARS +				// m_Login
														RGSC_MAX_PASSWORD_LEGACY_CHARS +	// m_Password (legacy - 31 chars)
														Sha1::SHA1_DIGEST_LENGTH +			// m_LoginHash
														Sha1::SHA1_DIGEST_LENGTH +			// m_PasswordHash
														RGSC_MAX_AVATAR_URL_CHARS +			// m_GamerPic
														1 +									// m_OfflineProfile
														sizeof(unsigned) +					// m_ProfileNumber
														sizeof(time_t) +					// m_LastSignInTime
														32 +								// padding for encryption
														datProtectSignatureSize * 2 +		// checksums
														(INITIAL_RESERVED_SPACE_V1 * 2);		// reserved for future use

	// The outer reserved space represents the remaining bytes in the initial reserved space (128 bytes) that is
	//	with the common key, and thus less secure than the inner reserved space.
	static const unsigned OUTER_RESERVED_SPACE_V1 = INITIAL_RESERVED_SPACE_V1 -
													sizeof(u64);				// m_SteamId - u64 - 8 bytes

	// The inner reserved space represents the remaining bytes in the initial reserved space (128) bytes
	//	that is encrypted with a key generated from the user's login and password
	static const unsigned INNER_RESERVED_SPACE_V1 = INITIAL_RESERVED_SPACE_V1;

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Version 2 of the Profile - ScAuthToken
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static const unsigned RGSC_PROFILE_VERSION_2 = 2;
	static const unsigned INITIAL_RESERVED_SPACE_V2 = 1024;
	static const unsigned MAX_BYTE_SIZEOF_PAYLOAD_V2 = sizeof(unsigned) +					// m_Version
														sizeof(RockstarId) +				// m_ProfileId
														UniqueKey::SIZE_OF_KEY_IN_BYTES +	// m_UniqueKey
														RGSC_MAX_NAME_CHARS +				// m_Name
														RGSC_MAX_EMAIL_CHARS +				// m_Login
														RGSC_MAX_AUTHTOKEN_CHARS +			// m_ScAuthToken
														RGSC_MAX_AVATAR_URL_CHARS +			// m_GamerPic
														1 +									// m_OfflineProfile
														sizeof(unsigned) +					// m_ProfileNumber
														sizeof(time_t) +					// m_LastSignInTime
														32 +								// padding for encryption
														datProtectSignatureSize * 2 +		// checksums
														(INITIAL_RESERVED_SPACE_V2 * 2);	// reserved for future use

	// The outer reserved space represents the remaining bytes in the initial reserved space (1024 bytes) that is
	//	with the common key, and thus less secure than the inner reserved space.
	static const unsigned OUTER_RESERVED_SPACE_V2 = INITIAL_RESERVED_SPACE_V2 -
													RGSC_MAX_MACHINETOKEN_CHARS; // m_MachineToken

	// The inner reserved space represents the remaining bytes in the initial reserved space (1024) bytes
	//	that is encrypted with a key generated from the user's most recent scauthtoken
	static const unsigned INNER_RESERVED_SPACE_V2 = INITIAL_RESERVED_SPACE_V2;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// All Versions of the Profile
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Determine which version has the largest payload at runtime
	static const unsigned MAX_BYTE_SIZEOF_PAYLOAD = (MAX_BYTE_SIZEOF_PAYLOAD_V1 > MAX_BYTE_SIZEOF_PAYLOAD_V2) 
														? MAX_BYTE_SIZEOF_PAYLOAD_V1 
														: MAX_BYTE_SIZEOF_PAYLOAD_V2;


	// Note: if you add any member variables that need to be saved to
	// disk with the profile, make sure to add its size to the
	// MAX_BYTE_SIZE_OF_PAYLOAD_V? constant above of the latest version.

    unsigned m_Version;
	unsigned m_ExportVersion;
    unsigned m_FolderId;
    RockstarId m_ProfileId;
	UniqueKey m_UniqueKey;
    char m_Name[RGSC_MAX_NAME_CHARS];
    char m_GamerPic[RGSC_MAX_AVATAR_URL_CHARS];
    bool m_OfflineProfile;
	unsigned m_ProfileNumber;
    time_t m_LastSignInTime;
    bool m_IsFullyDecrypted;
	u64 m_SteamId;
	char m_Login[RGSC_MAX_EMAIL_CHARS];

	// V1 Only
	char m_Password[RGSC_MAX_PASSWORD_LEGACY_CHARS];
	u8 m_LoginHash[Sha1::SHA1_DIGEST_LENGTH];
	u8 m_PasswordHash[Sha1::SHA1_DIGEST_LENGTH];

	// V2 Only
	char m_ScAuthToken[RGSC_MAX_AUTHTOKEN_CHARS];
	char m_MachineToken[RGSC_MAX_MACHINETOKEN_CHARS];
};

class RgscProfileAutoSignInData
{
    friend class RgscProfileManager;

    RgscProfileAutoSignInData();
    RgscProfileAutoSignInData(unsigned autoSignInFolderId);

    ~RgscProfileAutoSignInData();

    bool IsValid() const;
    unsigned GetVersion() const;
    unsigned GetAutoSignInFolderId() const;

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

    void Clear();

    static const unsigned RGSC_PROFILE_AUTOSIGNIN_DATA_VERSION = 1;

    static const unsigned MAX_BYTE_SIZEOF_PAYLOAD = sizeof(unsigned) +		// m_Version
                                                    sizeof(unsigned);		// m_AutoSignInFolderId;

    unsigned m_Version;
    unsigned m_AutoSignInFolderId;
};

class RgscProfileSignInTransferData
{
    friend class RgscProfileManager;

    RgscProfileSignInTransferData();
    RgscProfileSignInTransferData(unsigned autoSignInFolderId, const char* login, const char* scTicket, const char* scAuthToken);

    ~RgscProfileSignInTransferData();

    bool IsValid() const;
    unsigned GetVersion() const;
    unsigned GetAutoSignInFolderId() const;
	const char* GetLogin() const;
	const char* GetScTicket() const;
	const char* GetScAuthToken() const;

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

    void Clear();

    static const unsigned RGSC_PROFILE_SIGNIN_TRANSFER_DATA_VERSION = 2;

	static const unsigned RESERVED_SPACE = 512;

    static const unsigned MAX_BYTE_SIZEOF_PAYLOAD = sizeof(unsigned) +			// m_Version
                                                    sizeof(unsigned) +			// m_AutoSignInFolderId;
													RGSC_MAX_EMAIL_CHARS +		// m_Login
													RGSC_MAX_AUTHTOKEN_CHARS +  // m_ScAuthToken
													RGSC_ROS_MAX_TICKET_LEN +		// sc ticket
													16 +						// padding for encryption
													datProtectSignatureSize +	// checksum
													// RESERVED
													(RESERVED_SPACE);						// reserved for future use

    unsigned m_Version;
    unsigned m_AutoSignInFolderId;
	char m_Login[RGSC_MAX_EMAIL_CHARS];
	char m_ScTicket[RGSC_ROS_MAX_TICKET_LEN];
	char m_ScAuthToken[RGSC_MAX_AUTHTOKEN_CHARS];
};

class RgscProfiles
{
    friend class RgscProfileManager;
	friend class RgscDelegate;

public:
    RgscProfiles();
    ~RgscProfiles();

    static const u8 sm_MaxProfiles = 20;

    unsigned GetNumProfiles() const;
    const RgscProfile& GetProfile(const unsigned index) const;

private:
	const char* ToJson();
    void Clear();
    void AddProfile(const RgscProfile& profile);

    static int SortCompare(RgscProfile* const* a, RgscProfile* const* b);
    void Sort();

    atFixedArray<RgscProfile*, sm_MaxProfiles> m_Profiles;
};

// error C4265: class has virtual functions, but destructor is not virtual
// the binary interface for virtual destructors isn't standardized, so don't make the destructor virtual
#pragma warning(push)
#pragma warning(disable: 4265)

class RgscProfileManager : public IProfileManagerLatestVersion
{
// ===============================================================================================
// inherited from IProfileManager
// ===============================================================================================
public:
	// ===============================================================================================
	// inherited from IRgscUnknown
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

	// ===============================================================================================
	// inherited from IProfileManagerV1
	// ===============================================================================================
	virtual bool RGSC_CALL IsSignedIn();
	virtual bool RGSC_CALL IsOnline();
	virtual RGSC_HRESULT RGSC_CALL GetSignInInfo(IProfile* profile);
	virtual RGSC_HRESULT RGSC_CALL AttemptAutoSignIn();

	// ===============================================================================================
	// inherited from IProfileManagerV2
	// ===============================================================================================
	virtual bool RGSC_CALL IsSigningIn() const;
	virtual bool RGSC_CALL CreateSigninTransferProfile();
	virtual void RGSC_CALL OnDuplicateSignInMessageReceived(const int localGamerIndex);
	virtual void RGSC_CALL OnLauncherReceivedUpdatedSocialClubTicket(RockstarId id, const char* base64credentials);
	virtual void RGSC_CALL OnLauncherReceivedSignOutEvent();
	virtual void RGSC_CALL OnLauncherReceivedSignInEvent(RockstarId id);

	// ===============================================================================================
	// inherited from IProfileManagerV3
	// ===============================================================================================
	virtual bool RGSC_CALL CreateTitleSpecificTransferProfile(int rosTitleId, IAsyncStatus* status);

// ===============================================================================================
// accessible from anywhere in the dll
// ===============================================================================================
public:

    static const u8 sm_MaxProfiles = RgscProfiles::sm_MaxProfiles;

    RgscProfileManager();
    ~RgscProfileManager();

    bool Init();
    void Shutdown();
    void Update();

	//  Returns true if any profiles exist. Does not check to see if any are valid.
	bool CheckForProfilesFast();

    //  Retrieves the profiles stored on the local disk.
    bool EnumerateProfiles(RgscProfiles& profiles, bool bValidLoginProfilesOnly = false);

    //  Returns true if the specified profile is the auto sign-in profile.
    bool IsAutoSignInProfile(const RgscProfile &profile) const;

    // Creates a new offline account and signs in
    bool CreateOfflineProfile(const char* name,
                              const bool autoSignIn,
							  RgscProfile &profile);

    // Signs the profile into the platform, and connects to online services
    // If it can't connect to online services, then it signs in locally only.
	bool SignIn(const char* email,
				const char* password,
				const char* nickname,
				const char* avatarUrl,
				bool saveEmail,
				bool savePassword,
				bool saveMachine,
				bool autoSignIn,
				const char* xmlResponse,
				const RockstarId rockstarId,
				const bool isOfflineProfile,
				const int callbackData);

#if RSG_DEV
	bool SignInWithoutScUi(const char* email, const char* password);
#endif
	bool SignInWithoutScUi(const char* email, const char* scAuthToken, const char* scTicket);

	// Signs into an offline profile
	bool SignInOfflineProfile(const RockstarId rockstarId, const char* nickname, const bool autoSignIn, const int callbackData);

	bool SignInAfterTransition();

	bool IsSignedInInternal();
	bool IsOnlineInternal();

	// called from the web browser
	bool SignInResponse(const char* json);
	bool ModifyProfile(const char* jsonInfo);
	bool DeleteProfile(const char* jsonProfile);

	// Modifies the signed-in profile and saves it to disk
	bool ModifyProfile(const char* email, const char* password, const char* nickname, const char* avatarUrl, const int autoSignIn, const int saveEmail);

    //  Returns the currently signed-in profile.
    const RgscProfile& GetSignedInProfile() const;
	bool GetSignedInProfileAsJson(JSONNODE* n);

    // Disconnects from online services, and signs the profile out of the platform
	void DuplicateSignInKick(const int localGamerIndex);
    void SignOut();

	// Returns the list of profile in json format.
	const char* GetProfileList(bool bLoginProfilesOnly = false);

	// Returns a list of profile in json format, filtered by the supplied filter.
	const char* GetProfileList(const char* json);

	// Returns ROS credentials in json format
	const char* GetRosCredentials();

	// Sets the reload profile
	void SetReloadProfile(const RgscProfile& profile);
	void ClearReloadProfile() { m_ReloadProfile.Clear(); }
	bool HasReloadProfile() const;
	bool HasReloadEmail() const;
	bool HasReloadScAuthToken() const;
	bool HasReloadTicket() const;
	bool GetReloadProfileAsJson(const RgscProfile& profile, JSONNODE* n);

	// If the signin task is pending
	bool IsPending() { return m_Status.Pending(); }

	//private:
	RockstarId GetRockstarIdFromJson(const char* json);
	bool GetNicknameFromJson(const char* json, char (&nickname)[RGSC_MAX_NAME_CHARS]);

    unsigned m_AutoSignInFolderId;
    RgscProfile m_SignedInProfile;
	unsigned m_NextOfflineProfileNumber;

	// Reload profile when transitioning from offline to online site or vice-versa
	RgscProfile m_ReloadProfile;
	char m_ReloadEmail[RGSC_MAX_EMAIL_CHARS];
	char m_ReloadScAuthToken[RGSC_MAX_AUTHTOKEN_CHARS];
	char m_ReloadTicket[RGSC_ROS_MAX_TICKET_LEN];

	// Sign In Credentials (Valid during sign in)
	void SetSignInCredentials(const rlRosCredentials* cred);
	const rlRosCredentials& GetSignInCredentials() const;
	void ClearSignInCredentials();

	rlRosCredentials m_SignInCredentials;

	// we need to save the e-mail and password of the signed in profile so that
	// we can later modify profiles that don't save their login info
	// TODO: NS - this should be stored in memory encrypted
	char m_SignedInEmail[RGSC_MAX_EMAIL_CHARS];
	char m_SignedInScAuthToken[RGSC_MAX_AUTHTOKEN_CHARS];

	bool m_Enumerated;
    bool m_SigningIn;
	u64 m_DuplicateSigninCheckTime;
	u64 m_SignInTime;
	netStatus m_Status;
	netStatus m_NoScuiStatus;
	FileDevice* m_FileDevice;

    // for security purposes, the rockstarid is encrypted inside the profile data
    // and profiles are loaded and saved by a randomly generated folder id.
    // this map is necessary to avoid saving multiple profiles with the same profile ids,
    // which can happen if a player attempts to retrieve an existing profile when
    // they already have the profile stored locally.
    atMap<RockstarId, u32, atMapHashFn<u64> >	m_ProfileIdToFolderIdMap;

    // this map is necessary to prevent the player from creating
    // multiple offline profiles with the same nickname
    atMap<u32, u32>	m_NameToFolderIdMap;

	//  Returns true if the specified credentials match the profile's stored credentials
	bool Authenticate(const RgscProfile &profile, const char* login, const char* password);

#if 0
	unsigned GenerateUniqueFolderId() const;
#endif

	const FileDevice* GetFileDevice(const char *filename, bool readOnly = true);

	// auto signin
	RGSC_HRESULT AutoSignInWithoutUi();
	RGSC_HRESULT AutoSigninWithUi();

    // auto sign-in data
    bool SaveAutoSignInData(const RgscProfile &profile);
    bool LoadAutoSignInData();
    bool DeleteAutoSignInData();

	// sign in transfer data
	bool SaveSignInTransferData(const RgscProfile &profile, const char* login, const char* scTicket, const char* scAuthToken);
	bool LoadSignInTransferData(RgscProfileSignInTransferData &data);
	bool DeleteSignInTransferData();

    // profile data
    RockstarId GenerateUniqueProfileId() const;

    bool SaveProfile(const UniqueKey* key,
                     RockstarId profileId,
					 const char* name,
					 const char* login,
                     const char* password,
					 bool saveLogin,
					 bool savePassword,
                     bool autoSignIn,
					 bool signInTransfer,
                     const char* gamerPic,
                     bool offlineProfile,
					 unsigned profileNumber,
					 u64 steamId,
					 const char* scTicket,
					 const char* scAuthToken,
					 const char* machineToken,
                     RgscProfile &savedProfile);

    bool LoadAutoSignInProfile(RgscProfile &profile);
	bool LoadAutoSignInProfileSteam(RgscProfile &profile);
	bool LoadSignInTransferProfile(RgscProfile &profile, RgscProfileSignInTransferData &data);
	bool LoadProfile(unsigned folderId, RgscProfile &profile);
    bool LoadProfile(unsigned folderId, RgscProfile &profile, const char* login, const char* password, const char* scAuthToken);
	bool LoadReloadProfile(RgscProfile& profile);
    bool DeleteProfile(const RgscProfile &profile);

	u64 GetOldestProfileTimeToInclude(const FileDevice *device, const char* path);

};

#pragma warning(pop)

} // namespace rgsc

#endif // RSG_PC

#endif // RLINE_RLPCPROFILE_H 
