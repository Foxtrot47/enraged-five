// 
// rgsc/profiles_tasks.h
// 
// Copyright (C) 1999-2017 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RGSC_PROFILES_TASKS_H 
#define RGSC_PROFILES_TASKS_H

#include "profiles.h"

#include "net/status.h"
#include "rline/rltask.h"
#include "rline/ros/rlroshttptask.h"

namespace rgsc
{

// Task Declarations
typedef rlTask<RgscProfileManager> ProfileTaskBase;

// ==================================================
// SignInWithoutScUiTask
// ==================================================
class SignInWithoutScUiTask : public ProfileTaskBase
{
public:
	RL_TASK_DECL(SignInWithoutScUiTask);

	enum State
	{
		STATE_INVALID   = -1,
		STATE_CREATE_TICKET,
		STATE_CREATING_TICKET,
#if RSG_DEV
		STATE_BIND_STEAM_ACCOUNT,
		STATE_BINDING_STEAM_ACCOUNT,
#endif
		STATE_SIGN_IN,
		STATE_SIGNING_IN,
		STATE_SUCCEEDED,
		STATE_FAILED
	};

	SignInWithoutScUiTask();
	~SignInWithoutScUiTask();

	bool Configure(RgscProfileManager* ctx, const char* email, const char* password, const char* scAuthToken, const char* scTicket);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	bool CreateTicket();
	bool SignIn();
#if RSG_DEV
	bool BindSteamAccount();
	bool CreateTicketWithPassword(netStatus* status);
#endif
	bool CreateTicketWithScAuthToken(netStatus* status);
	bool CreateTicketWithSteam(netStatus* status);
	bool RefreshTicket(netStatus* status);	

	bool *m_SigningIn;
	rlRosCredentials m_RosCred;
	char* m_XmlResponse;
	int m_State;
	netStatus m_MyStatus;

	int m_HasPassword;
	int m_HasScAuthToken;
	int m_HasTicket;

	char m_Email[RGSC_MAX_EMAIL_CHARS];
	char m_Password[RGSC_MAX_PASSWORD_LEGACY_CHARS];
	char m_ScTicket[RGSC_ROS_MAX_TICKET_LEN];
	char m_ScAuthToken[RGSC_MAX_AUTHTOKEN_CHARS];
	char m_MachineToken[RGSC_MAX_MACHINETOKEN_CHARS];
};

// ==================================================
// SignInTask
// ==================================================
class SignInTask : public ProfileTaskBase
{
public:
	RL_TASK_DECL(SignInTask);

	static const int SYNC_FRIENDS_WAIT_FOR_ONLINE_MS = (5 * 1000);

	enum State
	{
		STATE_INVALID   = -1,
		STATE_FIND_PROFILE,
		STATE_LOCAL_SIGN_IN,
		STATE_GET_INFO,
		STATE_GETTING_INFO,
		STATE_GET_SC_AUTHTOKEN,
		STATE_GETTING_SC_AUTHTOKEN,
		STATE_SAVE_PROFILE,
		STATE_SYNC_ACHIEVEMENTS,
		STATE_SYNCING_ACHIEVEMENTS,
		STATE_SYNC_FRIENDS,
		STATE_SYNCING_FRIENDS,
		STATE_SUCCEEDED,
		STATE_FAILED
	};

	SignInTask();
	~SignInTask();

	bool Configure(RgscProfileManager* ctx,
		const char* email,
		const char* password,
		const char* nickname,
		const char* avatarUrl,
		bool saveEmail,
		bool savePassword,
		bool saveMachine,
		bool autoSignIn,
		const char* xmlResponse,
		const RockstarId rockstarId,
		const bool isOfflineProfile,
		const int callbackData);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	bool ParseRosCredentials(const char* xmlResponse);
	bool SetRosCredentials();
	bool FindProfile();
	bool SignInLocally();
	//bool GetGamerInfo();
	bool DownloadGamerPic();
	bool GetScAuthToken();
	bool SaveProfile(RgscProfile &savedProfile);
	bool SyncAchievements();
	bool SyncFriends();
	void NotifyPlatform();

	bool m_ConnectedToRos;
	bool m_ScAuthTokenError;
	//bool m_RetrievedGamerInfo;
	bool m_AchievementsSynced;
	bool m_FriendsSynced;
	bool m_SignedInLocally;
	RockstarId m_ProfileId;
	RgscProfile m_Profile;
	RgscProfile m_SavedProfile;
	char m_Nickname[RGSC_MAX_NAME_CHARS];
	char m_Email[RGSC_MAX_EMAIL_CHARS];
	char m_Password[RGSC_MAX_PASSWORD512_CHARS];
	char m_ScAuthToken[RGSC_MAX_AUTHTOKEN_CHARS];
	char m_MachineToken[RGSC_MAX_MACHINETOKEN_CHARS];
	char m_AvatarUrl[RGSC_MAX_AVATAR_URL_CHARS];
	bool m_SaveEmail;
	bool m_SavePassword;
	bool m_SaveMachine;
	bool m_AutoSignIn;
	char* m_XmlResponseDecoded;
	rlRosCredentials m_RosCredentials;
	bool m_IsOfflineProfile;
	bool *m_SigningIn;
	//rlPcGamerInfo m_GamerInfo;
	int m_CallbackData;
	int m_State;
	u32 m_FriendSyncStartTime;
	netStatus m_MyStatus;
	AsyncStatus m_AsyncStatus; 
};

// ==================================================
// ModifyProfileTask
// ==================================================
class ModifyProfileTask : public ProfileTaskBase
{
public:
	RL_TASK_DECL(ModifyProfileTask);

	enum State
	{
		STATE_INVALID   = -1,
		STATE_GET_SC_AUTHTOKEN,
		STATE_GETTING_SC_AUTHTOKEN,
		STATE_SAVE_PROFILE,
		STATE_SUCCEEDED,
		STATE_FAILED
	};

	ModifyProfileTask();
	~ModifyProfileTask();

	bool Configure(RgscProfileManager* ctx,
		const char* email,
		const char* password,
		const char* nickname,
		const char* avatarUrl,
		const int autoSignIn,
		const int saveEmail);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	bool DownloadGamerPic();

	bool GetScAuthToken();
	bool SaveProfile(RgscProfile &savedProfile);

	void NotifyPlatform();

	bool m_RetrievedAvatar;
	bool m_RetrievedScAuthToken;

	RgscProfile m_SavedProfile;
	bool m_OfflineProfile;
	char m_Nickname[RGSC_MAX_NAME_CHARS];
	char m_Email[RGSC_MAX_EMAIL_CHARS];
	char m_Password[RGSC_MAX_PASSWORD512_CHARS];
	char m_ScAuthToken[RGSC_MAX_AUTHTOKEN_CHARS];
	char m_AvatarUrl[RGSC_MAX_AVATAR_URL_CHARS];
	char m_MachineToken[RGSC_MAX_MACHINETOKEN_CHARS];
	int m_AutoSignIn;
	int m_SaveEmail;

	int m_State;
	netStatus m_MyStatus;
};

// ==================================================
// CreateTitleSpecificTransferProfileTask
// ==================================================
class CreateTitleSpecificTransferProfileTask : public rgscTask<RgscProfileManager>
{
public:

	RL_TASK_DECL(CreateTitleSpecificTransferProfileTask);

	CreateTitleSpecificTransferProfileTask();
	~CreateTitleSpecificTransferProfileTask();

	enum State
	{
		STATE_INVALID   = -1,
		STATE_EXCHANGE_TICKET,
		STATE_EXCHANGING_TICKET,
	};

	bool Configure(RgscProfileManager* /*ctx*/, int rosTitleId, const char* email);
	void Start();
	void Update(const unsigned timeStep);
	void Finish(const FinishType finishType, const int resultCode = 0);
	bool IsCancelable() const;
	void DoCancel();

private:

	bool ExchangeTicket();

	State m_State;
	netStatus m_MyStatus;

	int m_RosTitleId;
	char m_Email[RGSC_MAX_EMAIL_CHARS];
	rlRosCredentials m_Credentials;
};

//////////////////////////////////////////////////////////////////////////
// CreateTicketTask
//////////////////////////////////////////////////////////////////////////
class CreateTicketTask : public rlRosHttpTask
{
protected:

	const char* GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const;
	virtual bool GetServicePath(char* svcPath, const unsigned maxLen) const;
	virtual bool UseHttps() const { return true; }
	virtual bool UseFilter() const;
	virtual rlRosSecurityFlags GetSecurityFlags() const;
	virtual bool ProcessResponse(const char* response, int& resultCode);  
	virtual bool ProcessSuccess(const rlRosResult& result, const parTreeNode* node, int& resultCode);

	rlRosCredentials* m_Cred;
	char **m_XmlResponse;
};

#if RSG_DEV
//////////////////////////////////////////////////////////////////////////
// BindSteamAccount
//////////////////////////////////////////////////////////////////////////
class BindSteamAccountTask : public rlRosHttpTask
{
public:
	RL_TASK_DECL(BindSteamAccountTask);

	bool Configure(SignInWithoutScUiTask* ctx, const char* scTicket);

protected:

	virtual const char* GetServiceMethod() const;

	rlRosCredentials* m_Cred;
	char **m_XmlResponse;
};

//////////////////////////////////////////////////////////////////////////
// CreateTicketPasswordTask
//////////////////////////////////////////////////////////////////////////
class CreateTicketPasswordTask : public CreateTicketTask
{
public:
	RL_TASK_DECL(CreateTicketPasswordTask);

	bool Configure(SignInWithoutScUiTask* ctx, const char* email, const char* password, rlRosCredentials* cred, char** xmlResponse);

protected:
	virtual const char* GetServiceMethod() const;
};
#endif

//////////////////////////////////////////////////////////////////////////
// CreateTicketScAuthTokenTask
//////////////////////////////////////////////////////////////////////////
class CreateTicketScAuthTokenTask : public CreateTicketTask
{
public:
	RL_TASK_DECL(CreateTicketScAuthTokenTask);

	bool Configure(SignInWithoutScUiTask* ctx, const char* scAuthToken, rlRosCredentials* cred, char** xmlResponse);

protected:
	virtual const char* GetServiceMethod() const;
};

//////////////////////////////////////////////////////////////////////////
// CreateTicketRefreshTask
//////////////////////////////////////////////////////////////////////////
class CreateTicketRefreshTask : public CreateTicketTask
{
public:
	RL_TASK_DECL(CreateTicketRefreshTask);

	bool Configure(SignInWithoutScUiTask* ctx, const char* email, const char* scTicket, rlRosCredentials* cred, char** xmlResponse);

protected:
	virtual const char* GetServiceMethod() const;
};

//////////////////////////////////////////////////////////////////////////
// CreateTicketScSteamTask
//////////////////////////////////////////////////////////////////////////
class CreateTicketScSteamTask : public CreateTicketTask
{
public:
	RL_TASK_DECL(CreateTicketScSteamTask);

	bool Configure(SignInWithoutScUiTask* ctx, const char* email, const char* password, const char* scTicket, rlRosCredentials* cred, char** xmlResponse);

protected:
	virtual const char* GetServiceMethod() const;
};

} // namespace rgsc

#endif // RGSC_PROFILES_TASKS_H