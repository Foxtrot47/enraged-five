// player.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "file/file_config.h"
#include "players.h"

#if RSG_PC

#include "rgsc.h"
#include "json.h"

// rage
#include "data/bitbuffer.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "parser/manager.h"
#include "rline/rldiag.h"
#include "rline/scfriends/rlscfriends.h"
#include "rline/rlfriendsmanager.h"
#include "rline/facebook/rlfacebook.h"
#include "rline/facebook/rlfacebookcommon.h"
#include "ros/rlros.h"
#include "ros/rlroshttptask.h"
#include "system/nelem.h"
#include "system/timer.h"
#include "rgsc_ui/script.h"
#include <time.h>

using namespace rage;

namespace rgsc
{

#if __RAGE_DEPENDENCY
RAGE_DEFINE_SUBCHANNEL(rgsc_dbg, player)
#undef __rage_channel
#define __rage_channel rgsc_dbg_player
#endif

// ===========================================================================
// PlayerEnumerationHandle
// ===========================================================================

class PlayerEnumerationHandle
{
public:
	RGSC_IID m_RRGSC_IID;
	RockstarId m_ProfileId;
	IPlayerManagerLatestVersion::PlayerListType m_PlayerListType;
	u32 m_FirstPlayerIndex;
	u32 m_MaxPlayersToRead;
	u32 m_NumBytesRequired;
	bool m_IsCreated;
	bool m_EnumerationStarted;
};

// could support more than one enumeration at a time if required
static const u32 MAX_ENUMERATIONS = 1;
static u32 m_Handles[MAX_ENUMERATIONS] = {0};
static PlayerEnumerationHandle sm_PlayerEnumerationHandle[MAX_ENUMERATIONS];

// ===========================================================================
// IPlayerV1
// ===========================================================================

RGSC_HRESULT Player::QueryInterface(RGSC_REFIID riid, void** ppvObject)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObject == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IPlayer*>(this);
	}
	else if(riid == IID_PlayerV1)
	{
		pUnknown = static_cast<IPlayerV1*>(this);
	}
	else if(riid == IID_PlayerV2)
	{
		pUnknown = static_cast<IPlayerV2*>(this);
	}
	else if(riid == IID_PlayerV3)
	{
		pUnknown = static_cast<IPlayerV3*>(this);
	}

	*ppvObject = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

Player::Player()
{
	Clear();
}

RockstarId Player::GetRockstarId() const
{
	return m_RockstarId;
}

const char* Player::GetName() const
{
	return m_Name;
}

IPlayerLatestVersion::Relationship Player::GetRelationship() const
{
	return m_Relationship;
}

bool Player::IsOnline() const
{
	return m_IsOnline;
}

bool Player::IsPlayingSameTitle() const
{
	return m_IsPlayingSameTitle;
}

unsigned
Player::GetFlags() const
{
	unsigned flags = 0;

	if(m_IsInJoinableSession)
	{
		flags |= PLAYER_FLAG_IN_JOINABLE_SESSION;
	}

	if(m_IsInJoinableParty)
	{
		flags |= PLAYER_FLAG_IN_JOINABLE_PARTY;
	}

	return flags;
}

const char*
Player::GetAvatarUrl() const
{
	return m_AvatarUrl;
}

bool Player::IsValid() const
{
	return (GetRockstarId() != InvalidRockstarId);
}

void Player::Clear()
{
	m_RockstarId = InvalidRockstarId;
	m_Name = NULL;
	m_Relationship = IPlayerLatestVersion::RELATIONSHIP_INVALID;
	m_IsOnline = false;
	m_IsPlayingSameTitle = false;
	m_IsInJoinableSession = false;
	m_IsInJoinableParty = false;
	m_AvatarUrl[0] = '\0';
}

// ===========================================================================
// IPlayerListV1
// ===========================================================================

RGSC_HRESULT PlayerListV1::QueryInterface(RGSC_REFIID riid, void** ppvObject)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObject == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IPlayerList*>(this);
	}
	else if(riid == IID_IPlayerListV1)
	{
		pUnknown = static_cast<IPlayerListV1*>(this);
	}

	*ppvObject = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

u32 PlayerListV1::GetNumPlayers() const
{
	return m_NumPlayers;
}

Player* PlayerListV1::GetPlayer(const u32 index)
{
	if(index < m_NumPlayers)	
	{
		return &m_Players[index];
	}
	return NULL;
}

PlayerListV1::PlayerListV1(u32 maxPlayers, u8* buffer)
{
	Clear();

	u32 bytesUsed = 0;
	u8* curBuf = &(buffer[bytesUsed]);

	bytesUsed += sizeof(PlayerListV1);
	bytesUsed = (bytesUsed + 16 - 1) & ~(16 - 1);
	curBuf = &(buffer[bytesUsed]);

	m_MaxPlayers = maxPlayers;
	m_Players = new (curBuf) Player[maxPlayers];

	bytesUsed += sizeof(Player) * maxPlayers;
	bytesUsed = (bytesUsed + 16 - 1) & ~(16 - 1);
	curBuf = &(buffer[bytesUsed]);

	for(u32 i = 0; i < m_MaxPlayers; i++)
	{
		m_Players[i].m_Name = new (curBuf) char[IPlayer::MAX_NICKNAME_CHARS];
		bytesUsed += IPlayer::MAX_NICKNAME_CHARS;
		curBuf = &(buffer[bytesUsed]);
	}
}

u32 PlayerListV1::GetMemoryRequired(u32 maxPlayers)
{
	u32 bytesRequired = 0;
	bytesRequired += sizeof(PlayerListV1);
	bytesRequired = (bytesRequired + 16 - 1) & ~(16 - 1);
	bytesRequired += sizeof(Player) * maxPlayers;
	bytesRequired = (bytesRequired + 16 - 1) & ~(16 - 1);

	u32 bytesRequiredForStrings = IPlayer::MAX_NICKNAME_CHARS;
	bytesRequired += bytesRequiredForStrings * maxPlayers;

	return bytesRequired;
}

u32 PlayerListV1::GetMaxPlayers() const
{
	return m_MaxPlayers;
}

Player* PlayerListV1::GetPlayerArray()
{
	return m_Players;
}

void PlayerListV1::Clear()
{
	m_MaxPlayers = 0;
	m_NumPlayers = 0;
	m_Players = NULL;
}

bool PlayerListV1::AddPlayer()
{
	if(m_NumPlayers >= m_MaxPlayers)
	{
		return false;
	}
	
	++m_NumPlayers;
	return true;
}

// ===========================================================================
// IPlayerManager Public Interface
// ===========================================================================

RGSC_HRESULT RgscPlayerManager::QueryInterface(RGSC_REFIID riid, LPVOID* ppvObj)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObj == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IPlayerManager*>(this);
	}
	else if(riid == IID_IPlayerManagerV1)
	{
		pUnknown = static_cast<IPlayerManagerV1*>(this);
	}
	else if (riid == IID_IPlayerManagerV2)
	{
		pUnknown = static_cast<IPlayerManagerV2*>(this);
	}

	*ppvObj = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

RGSC_HRESULT 
RgscPlayerManager::CreatePlayerEnumerator(RGSC_REFIID riid,
									  const RockstarId profileId,
									  const PlayerListType playerListType,
									  const u32 firstPlayerIndex,
									  const u32 maxPlayersToRead,
									  u32* numBytesRequired,
									  void** handle)
{
	if(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedIn() == false)
	{
		return HRESULT_FROM_WIN32(ERROR_NOT_LOGGED_ON);
	}

	if(!AssertVerify(numBytesRequired != NULL))
	{
		return RGSC_INVALIDARG;
	}

	*numBytesRequired = 0;

	if(!AssertVerify(handle != NULL))
	{
		return RGSC_INVALIDARG;
	}

	u32 bytesRequired = 0;

	if(riid == IID_IPlayerListV1)
	{
		bytesRequired = PlayerListV1::GetMemoryRequired(maxPlayersToRead);

		// find a free handle if more than 1
		CompileTimeAssert(MAX_ENUMERATIONS == 1);
		u32 handleIndex = 0;
		*handle = &m_Handles[handleIndex];

		PlayerEnumerationHandle* details = &sm_PlayerEnumerationHandle[handleIndex];

		details->m_RRGSC_IID = riid;
		details->m_ProfileId = profileId;
		details->m_PlayerListType = playerListType;
		details->m_FirstPlayerIndex = firstPlayerIndex;
		details->m_MaxPlayersToRead = maxPlayersToRead;
		details->m_NumBytesRequired = bytesRequired;
		details->m_IsCreated = true;
		details->m_EnumerationStarted = false;

		*numBytesRequired = bytesRequired;

		return RGSC_OK;
	}

	return RGSC_NOINTERFACE;
}

RGSC_HRESULT 
RgscPlayerManager::EnumeratePlayers(void* handle,
								void* buffer,
								const u32 bufferSize,
								IPlayerList** players,
								IAsyncStatus* status)
{
	if(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal() == false)
	{
		return ERROR_NOT_LOGGED_ON;
	}

	if(!AssertVerify(handle != NULL))
	{
		return RGSC_INVALIDARG;
	}

	u32 handleIndex = *((u32*)handle);
	if(!AssertVerify(handleIndex < MAX_ENUMERATIONS))
	{
		return RGSC_INVALIDARG;
	}

	PlayerEnumerationHandle* details = &sm_PlayerEnumerationHandle[handleIndex];

	if(!AssertVerify(buffer != NULL))
	{
		return RGSC_INVALIDARG;
	}

	if(!AssertVerify(bufferSize >= details->m_NumBytesRequired))
	{
		return RGSC_INVALIDARG;
	}

	if(!AssertVerify(players != NULL))
	{
		return RGSC_INVALIDARG;
	}

	if(!AssertVerify(details->m_IsCreated == true))
	{
		return RGSC_INVALIDARG;
	}

	details->m_EnumerationStarted = true;

	PlayerListV1* list = new (buffer) PlayerListV1(details->m_MaxPlayersToRead, (u8*)buffer);

	ReadPlayers(details->m_ProfileId,
				details->m_PlayerListType,
				details->m_FirstPlayerIndex,
				list,
				status);

	*players = (PlayerListV1*)buffer;

	details->m_IsCreated = false;

	return RGSC_OK;
}

// ===========================================================================
// PlayerManager
// ===========================================================================

RgscPlayerManager::RgscPlayerManager()
{
	Clear();
}

RgscPlayerManager::~RgscPlayerManager()
{
	Shutdown();
}

void
RgscPlayerManager::Clear()
{
	SignOut();
}

bool
RgscPlayerManager::Init()
{
	Clear();

	return true;
}

void
RgscPlayerManager::Update()
{
}

void
RgscPlayerManager::Shutdown()
{
	Clear();
}

void 
RgscPlayerManager::SetPlayerListCounts(unsigned numBlocked,
								   unsigned numFriends,
								   unsigned numInvitesReceieved,
								   unsigned numInvitesSent)
{
	m_NumBlocked = numBlocked;
	m_NumFriends = numFriends;
	m_NumInvitesSent = numInvitesSent;
	m_NumInvitesReceived = numInvitesReceieved;

	Displayf("m_NumBlocked: %d\nm_NumFriends: %d\nm_NumInvitesSent: %d\nm_NumInvitesRecevied: %d",
			 m_NumBlocked, m_NumFriends, m_NumInvitesSent, m_NumInvitesReceived);
}

void 
RgscPlayerManager::GetPlayerListCounts(unsigned& numBlocked,
								   unsigned& numFriends,
								   unsigned& numInvitesReceieved,
								   unsigned& numInvitesSent)
{
	numBlocked = m_NumBlocked;
	numFriends = m_NumFriends;
	numInvitesSent = m_NumInvitesSent;
	numInvitesReceieved = m_NumInvitesReceived;
}

RgscPlayerManager::BlockedPlayerIds* 
RgscPlayerManager::GetBlockedPlayerIdCache()
{
	return &m_BlockedPlayerIds;
}

void RgscPlayerManager::AddPlayerToBlockList(const RockstarId rockstarId)
{
	if(m_BlockedPlayerIds.GetCount() < m_BlockedPlayerIds.GetMaxCount())
	{
		int index = m_BlockedPlayerIds.BinarySearch(rockstarId);
		if(AssertVerify(index < 0))
		{
			m_BlockedPlayerIds.Append() = rockstarId;
			m_NumBlocked = m_BlockedPlayerIds.GetCount();

			struct SortPredicate
			{
				static int Sort(const RockstarId* a, const RockstarId* b)
				{
					return *a < *b ? -1 : 1;
				}
			};

			m_BlockedPlayerIds.QSort(0, -1, SortPredicate::Sort);
		}
	}
}

void RgscPlayerManager::RemovePlayerFromBlockList(const RockstarId rockstarId)
{
	if(AssertVerify(m_BlockedPlayerIds.GetCount() > 0))
	{
		int index = m_BlockedPlayerIds.BinarySearch(rockstarId);
		if(AssertVerify(index >= 0))
		{
			// deleting an element won't affect sort order
			m_BlockedPlayerIds.Delete(index);
			m_NumBlocked = m_BlockedPlayerIds.GetCount();
		}
	}
}

void
RgscPlayerManager::SortPlayers(IPlayerList* players)
{
	struct SortPredicateV1
	{
		static bool Sort(Player& a, Player& b)
		{
			/*
			Sort Players as follows:
				- Online players, listed alphabetically from A-Z.
				- Offline players, listed alphabetically from A-Z.
			*/
			if(a.m_IsOnline)
			{
				if(b.m_IsOnline)
				{
					// both are online, sort alphabetically
					return _stricmp(a.m_Name, b.m_Name) <= 0;
				}
				else
				{
					// a is online, but b is not, therefore a comes before b
					return true;
				}
			}
			else if(b.m_IsOnline)
			{
				// b is online, but a is not, therefore b comes before a
				return false;
			}

			// both are offline, sort alphabetically
			return _stricmp(a.m_Name, b.m_Name) <= 0;
		}
	};

	// determine which version of the player list interface we are working with
	RGSC_HRESULT hr = RGSC_NOINTERFACE;
	PlayerListV1* listV1 = NULL;
	hr = players->QueryInterface(IID_IPlayerListV1, (void**) &listV1);
	if(AssertVerify(SUCCEEDED(hr) && (listV1 != NULL)))
	{
		Player* array = listV1->GetPlayerArray();
		std::sort(array, array + listV1->GetNumPlayers(), SortPredicateV1::Sort);
	}
}

void 
RgscPlayerManager::SignOut()
{
	for(unsigned int i = 0; i < MAX_ENUMERATIONS; ++i)
	{
		sm_PlayerEnumerationHandle[i].m_IsCreated = false;
	}

	m_BlockedPlayerIds.Reset();

	m_NumBlocked = 0;
	m_NumFriends = 0;
	m_NumInvitesReceived = 0;
	m_NumInvitesSent = 0;
	m_HasDoneInitialRichPresenceQuery = false;

	if (rlFriendsManager::IsActivated())
	{
		rlFriendsManager::Deactivate();
	}
}

bool 
RgscPlayerManager::GetNumFriendsOnline(unsigned& numFriendsOnline, unsigned& numFriendsPlayingSameTitle)
{
	bool success = false;
	rtry
	{
		rlFriend* friends = NULL;
		unsigned numFriends = 0;

		rverify(rlPresence::GetFriendsArray(0, &friends, &numFriends), catchall, rlError("Couldn't get cached friend list from rlPresence"));

		for(unsigned i = 0; i < numFriends; ++i)
		{
			if(friends[i].IsOnline())
			{
				++numFriendsOnline;

				if(friends[i].IsInSameTitle())
				{
					++numFriendsPlayingSameTitle;
				}
			}
		
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

static IPlayerLatestVersion::Relationship ConvertRlRelationshipToRgsc(rlScRelationship relationship)
{
	// convert between rline relationship enum and our publicly-exposed relationship enum (in case rage ever changes enum values)
	IPlayerLatestVersion::Relationship rgscRelationship = IPlayerLatestVersion::RELATIONSHIP_INVALID;

	switch(relationship)
	{
	case RLSC_RELATIONSHIP_NONE:
		rgscRelationship = IPlayerLatestVersion::RELATIONSHIP_NONE;
		break;
	case RLSC_RELATIONSHIP_BLOCKED_BY_ME:
		rgscRelationship = IPlayerLatestVersion::RELATIONSHIP_BLOCKED_BY_ME;
		break;
	case RLSC_RELATIONSHIP_BLOCKED_BY_THEM:
		rgscRelationship = IPlayerLatestVersion::RELATIONSHIP_BLOCKED_BY_THEM;
		break;
	case RLSC_RELATIONSHIP_BLOCKED_BY_BOTH:
		rgscRelationship = IPlayerLatestVersion::RELATIONSHIP_BLOCKED_BY_BOTH;
		break;
	case RLSC_RELATIONSHIP_INVITED_BY_ME:
		rgscRelationship = IPlayerLatestVersion::RELATIONSHIP_INVITED_BY_ME;
		break;
	case RLSC_RELATIONSHIP_INVITED_BY_THEM:
		rgscRelationship = IPlayerLatestVersion::RELATIONSHIP_INVITED_BY_THEM;
		break;
	case RLSC_RELATIONSHIP_FRIEND:
		rgscRelationship = IPlayerLatestVersion::RELATIONSHIP_FRIEND;
		break;
	default:
		rgscRelationship = IPlayerLatestVersion::RELATIONSHIP_INVALID;
		break;
	}

	Assertf(rgscRelationship != RLSC_RELATIONSHIP_INVALID, "unknown relationship specified: %d", relationship);

	return rgscRelationship;
}

static json_char* json_as_string_or_null(json_const JSONNODE* node)
{
	char* str = json_as_string(node);
	return (_stricmp("null", str) != 0) ? str : NULL;
}

#if !__NO_OUTPUT
	class PlayerManagerTaskBase : public rgscTask<RgscPlayerManager>
	{
		u32 m_StartTime;

	public:
		PlayerManagerTaskBase() : m_StartTime(0) {}
		void Start()
		{
			m_StartTime = sysTimer::GetSystemMsTime();
			rlTask<RgscPlayerManager>::Start();
		}
		
		u32 GetElapsedTime()
		{
			return sysTimer::GetSystemMsTime() - m_StartTime;
		}
	};
#else
	typedef rgscTask<RgscPlayerManager> PlayerManagerTaskBase;
#endif

///////////////////////////////////////////////////////////////////////////////
//  ReadCachedFriendListTask
///////////////////////////////////////////////////////////////////////////////

class ReadCachedFriendListTask : public PlayerManagerTaskBase
{
public:
	RL_TASK_DECL(ReadCachedFriendListTask);

	enum State
	{
		STATE_INVALID   = -1,
		STATE_READ,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	ReadCachedFriendListTask();

	bool Configure(void* ctx,
				   IPlayerManagerLatestVersion::PlayerListType playerListType,
				   RockstarId profileId,
				   const u32 firstPlayerIndex,
				   const u32 maxPlayersToRead,
				   IPlayerList* players);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

private:
	bool Read();

	// parameters and returns
	RockstarId m_ProfileId;
	IPlayerManagerLatestVersion::PlayerListType m_ListType;
	u32 m_FirstPlayerIndex;
	u32 m_MaxPlayersToRead;
	IPlayerList* m_Players;

	int m_State;
	netStatus m_MyStatus;
};

ReadCachedFriendListTask::ReadCachedFriendListTask()
: m_State(STATE_INVALID)
{
	rlDebug2("sizeof(ReadCachedFriendListTask): %d", sizeof(ReadCachedFriendListTask));
}

bool 
ReadCachedFriendListTask::Configure(void* /*ctx*/,
									IPlayerManagerLatestVersion::PlayerListType listType,
									RockstarId profileId,
									const u32 firstPlayerIndex,
									const u32 maxPlayersToRead,
									IPlayerList* players)
{
	bool success = false;

	rtry
	{
		rverify(maxPlayersToRead > 0, catchall, );

		bool isOnline = GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal();
		bool isSigningIn = GetRgscConcreteInstance()->_GetProfileManager()->IsSigningIn();

		rverify(isOnline || isSigningIn, catchall, );
		rverify(players != NULL, catchall, );

		PlayerListV1* listV1 = NULL;
		RGSC_HRESULT hr = players->QueryInterface(IID_IPlayerListV1, (void**) &listV1);
		rverify(SUCCEEDED(hr) && (listV1 != NULL), catchall, );

		m_ListType = listType;
		m_ProfileId = profileId;

		m_FirstPlayerIndex = firstPlayerIndex;
		m_MaxPlayersToRead = maxPlayersToRead;
		if(m_MaxPlayersToRead > rlFriendsPage::MAX_FRIEND_PAGE_SIZE)
		{
			m_MaxPlayersToRead = rlFriendsPage::MAX_FRIEND_PAGE_SIZE;
		}

		m_Players = players;

		m_State = STATE_INVALID;

		success = true;
	}
	rcatchall
	{

	}
	
	return success;
}

void
ReadCachedFriendListTask::Start()
{
	rlDebug2("Reading cached player list...");

	this->PlayerManagerTaskBase::Start();

	m_State = STATE_READ;
}

void
ReadCachedFriendListTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("Reading cached player list %s (%ums)", FinishString(finishType), GetElapsedTime());

	if(finishType == FINISH_SUCCEEDED)
	{
		m_Ctx->SortPlayers(m_Players);
	}

	this->PlayerManagerTaskBase::Finish(finishType, resultCode);
}

void
ReadCachedFriendListTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
	this->PlayerManagerTaskBase::DoCancel();
}

void
ReadCachedFriendListTask::Update(const unsigned timeStep)
{
	this->PlayerManagerTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	switch(m_State)
	{
	case STATE_READ:
		if(this->Read())
		{
			m_State = STATE_SUCCEEDED;
		}
		else
		{
			m_State = STATE_FAILED;
		}
		break;

	case STATE_SUCCEEDED:
		this->Finish(FINISH_SUCCEEDED);
		break;

	case STATE_FAILED:
		this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
		break;
	}
}

bool 
ReadCachedFriendListTask::Read()
{
	bool success = false;

	rtry
	{
		PlayerListV1* listV1 = NULL;
		RGSC_HRESULT hr = m_Players->QueryInterface(IID_IPlayerListV1, (void**) &listV1);
		rverify(SUCCEEDED(hr) && (listV1 != NULL), catchall, );

		rlFriend* friends = NULL;
		unsigned numFriends = 0;

		rverify(rlPresence::GetFriendsArray(0, &friends, &numFriends), catchall, rlError("Couldn't get cached friend list from rlPresence"));
		
		if(numFriends == 0)
		{
			// no friends to return
			return true;
		}

		if(m_FirstPlayerIndex >= numFriends)
		{
			// no friends in the requested range
			return true;
		}

		rverify(m_MaxPlayersToRead > 0, catchall, );

		unsigned lastPlayerIndex = m_FirstPlayerIndex + m_MaxPlayersToRead;
		if(lastPlayerIndex > numFriends)
		{
			lastPlayerIndex = numFriends;
		}

		for(unsigned i = m_FirstPlayerIndex; i < lastPlayerIndex; ++i)
		{
			const rlScPlayer& rosPlayer = friends[i].GetScFriend();
			if(AssertVerify(rosPlayer.IsValid()))
			{
				u32 playerIndex = listV1->GetNumPlayers();
				rverify(listV1->AddPlayer(), catchall, );

				Player* rgscPlayer = listV1->GetPlayer(playerIndex);
				rgscPlayer->m_RockstarId = rosPlayer.m_RockstarId;
				safecpy(rgscPlayer->m_Name, rosPlayer.m_Nickname, IPlayer::MAX_NICKNAME_CHARS);
				rgscPlayer->m_Relationship = ConvertRlRelationshipToRgsc(rosPlayer.m_Relationship);
				rgscPlayer->m_IsOnline = rosPlayer.m_IsOnline;
				rgscPlayer->m_IsPlayingSameTitle = rosPlayer.m_IsPlayingSameTitle;
				safecpy(rgscPlayer->m_AvatarUrl, rosPlayer.m_AvatarUrl, RGSC_MAX_AVATAR_URL_CHARS);
			}
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

///////////////////////////////////////////////////////////////////////////////
//  ReadFriendListTask
///////////////////////////////////////////////////////////////////////////////

class ReadFriendListTask : public PlayerManagerTaskBase
{
public:
	RL_TASK_DECL(ReadFriendListTask);

	enum AttrIndex
	{
		GAME_JOINABLE_IDX = 0,
		PARTY_JOINABLE_IDX,
		ADDITIONAL_JOIN_IDX,
		MAX_NUM_ATTRS,
	};

	enum State
	{
		STATE_INVALID   = -1,
		STATE_GET_FRIENDS,
		STATE_GETTING_FRIENDS,
		STATE_GET_PRESENCE_DATA,
		STATE_GETTING_PRESENCE_DATA,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	ReadFriendListTask();

	bool Configure(RgscPlayerManager* ctx,
				   IPlayerManagerLatestVersion::PlayerListType playerListType,
				   RockstarId profileId,
				   const u32 firstPlayerIndex,
				   const u32 maxPlayersToRead,
				   IPlayerList* players);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

private:
	bool Read();
	bool GetPresenceData();

	// parameters and returns
	RockstarId m_ProfileId;
	IPlayerManagerLatestVersion::PlayerListType m_ListType;
	u32 m_FirstPlayerIndex;
	u32 m_MaxPlayersToRead;
	IPlayerList* m_Players;

	// internal state
	static const u32 MAX_PAGE_SIZE = 100;
	rlScPresenceAttribute m_PresenceAttrs[MAX_PAGE_SIZE][MAX_NUM_ATTRS];
	rlScPresenceAttribute* m_Attrs[MAX_PAGE_SIZE];

	CompileTimeAssert(MAX_PAGE_SIZE <= RLSC_PRESENCE_GET_ATTRS_MAX_GAMERS_PER_REQUEST);
	CompileTimeAssert(MAX_NUM_ATTRS <= RLSC_PRESENCE_GET_ATTRS_MAX_ATTRS_PER_GAMER);

	rlGamerHandle m_GamerHandles[MAX_PAGE_SIZE];
	PlayerListV1* m_PlayerList;

	int m_State;
	netStatus m_MyStatus;
};

ReadFriendListTask::ReadFriendListTask()
: m_State(STATE_INVALID)
{
	rlDebug2("sizeof(ReadFriendListTask): %d", sizeof(ReadFriendListTask));
}

bool 
ReadFriendListTask::Configure(RgscPlayerManager* /*ctx*/,
							  IPlayerManagerLatestVersion::PlayerListType listType,
							  RockstarId profileId,
							  const u32 firstPlayerIndex,
							  const u32 maxPlayersToRead,
							  IPlayerList* players)
{
	bool success = false;

	rtry
	{
		rverify(maxPlayersToRead > 0, catchall, );

		bool isOnline = GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal();
		bool isSigningIn = GetRgscConcreteInstance()->_GetProfileManager()->IsSigningIn();

		rverify(isOnline || isSigningIn, catchall, );
		rverify(players != NULL, catchall, );

		m_PlayerList = NULL;
		RGSC_HRESULT hr = players->QueryInterface(IID_IPlayerListV1, (void**) &m_PlayerList);
		rverify(SUCCEEDED(hr) && (m_PlayerList != NULL), catchall, );

		m_ListType = listType;
		m_ProfileId = profileId;

		m_FirstPlayerIndex = firstPlayerIndex;
		m_MaxPlayersToRead = maxPlayersToRead;
		if(m_MaxPlayersToRead > rlFriendsPage::MAX_FRIEND_PAGE_SIZE)
		{
			m_MaxPlayersToRead = rlFriendsPage::MAX_FRIEND_PAGE_SIZE;
		}

		Assertf(m_ListType != RgscPlayerManager::LIST_TYPE_FRIENDS_WITH_PRESENCE || m_MaxPlayersToRead < MAX_PAGE_SIZE,
				"Can only request presence data for up to %d friends at a time", MAX_PAGE_SIZE);

		m_Players = players;

		m_State = STATE_INVALID;

		success = true;
	}
	rcatchall
	{

	}
	
	return success;
}

void
ReadFriendListTask::Start()
{
	rlDebug2("Reading player list...");

	this->PlayerManagerTaskBase::Start();

	m_State = STATE_GET_FRIENDS;
}

void
ReadFriendListTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("Reading player list %s (%ums)", FinishString(finishType), GetElapsedTime());

	this->PlayerManagerTaskBase::Finish(finishType, resultCode);
}

void
ReadFriendListTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
	this->PlayerManagerTaskBase::DoCancel();
}

void
ReadFriendListTask::Update(const unsigned timeStep)
{
	this->PlayerManagerTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	switch(m_State)
	{
	case STATE_GET_FRIENDS:
		if(this->Read())
		{
			m_State = STATE_GETTING_FRIENDS;
		}
		else
		{
			m_State = STATE_FAILED;
		}
		break;

	case STATE_GETTING_FRIENDS:
		if(!m_MyStatus.Pending())
		{
			if(m_MyStatus.Succeeded())
			{
				if(m_ListType == RgscPlayerManager::LIST_TYPE_FRIENDS_WITH_PRESENCE)
				{
					m_State = STATE_GET_PRESENCE_DATA;
				}
				else
				{
					m_State = STATE_SUCCEEDED;
				}
			}
			else
			{
				m_State = STATE_FAILED;
			}
		}
		break;

	case STATE_GET_PRESENCE_DATA:
		if(this->GetPresenceData())
		{
			m_State = STATE_GETTING_PRESENCE_DATA;
		}
		else
		{
			// if we failed to get presence data
			// we can still return the player list
			m_State = STATE_SUCCEEDED;
		}
		break;

	case STATE_GETTING_PRESENCE_DATA:
		if(m_MyStatus.Succeeded())
		{
			u32 numGamerHandlesToQuery = 0;
			u32 numPlayers = m_PlayerList->GetNumPlayers();
			for(unsigned i = 0; i < numPlayers; ++i)
			{
				s64 isGameJoinable = 0;
				s64 isPartyJoinable = 0;
				s64 isAdditonalSessionJoinable = 0;

				Player* playerV1 = m_PlayerList->GetPlayer(i);
				if(playerV1->GetRelationship() == IPlayerLatestVersion::RELATIONSHIP_FRIEND &&
					playerV1->IsOnline() &&
					playerV1->IsPlayingSameTitle() &&
					rlVerify(m_GamerHandles[numGamerHandlesToQuery].GetRockstarId() == playerV1->GetRockstarId()))
				{
					if(m_PresenceAttrs[numGamerHandlesToQuery][GAME_JOINABLE_IDX].Type == RLSC_PRESTYPE_S64)
					{
						m_PresenceAttrs[numGamerHandlesToQuery][GAME_JOINABLE_IDX].GetValue(&isGameJoinable);
					}

					if(m_PresenceAttrs[numGamerHandlesToQuery][PARTY_JOINABLE_IDX].Type == RLSC_PRESTYPE_S64)
					{
						m_PresenceAttrs[numGamerHandlesToQuery][PARTY_JOINABLE_IDX].GetValue(&isPartyJoinable);
					}

					if(m_PresenceAttrs[numGamerHandlesToQuery][ADDITIONAL_JOIN_IDX].Type == RLSC_PRESTYPE_S64)
					{
						m_PresenceAttrs[numGamerHandlesToQuery][ADDITIONAL_JOIN_IDX].GetValue(&isAdditonalSessionJoinable);
					}

					numGamerHandlesToQuery++;
				}

				playerV1->m_IsInJoinableParty = isPartyJoinable;
				playerV1->m_IsInJoinableSession = isGameJoinable || isAdditonalSessionJoinable;
			}

			m_State = STATE_SUCCEEDED;
		}
		else if(!m_MyStatus.Pending())
		{
			u32 numPlayers = m_PlayerList->GetNumPlayers();
			for(unsigned i = 0; i < numPlayers; ++i)
			{
				Player* playerV1 = m_PlayerList->GetPlayer(i);
				playerV1->m_IsInJoinableParty = 0;
				playerV1->m_IsInJoinableSession = 0;
			}

			// if we failed to get presence data
			// we can still return the player list
			m_MyStatus.Reset();
			m_MyStatus.ForceSucceeded();
			m_State = STATE_SUCCEEDED;
		}
		break;

	case STATE_SUCCEEDED:
		this->Finish(FINISH_SUCCEEDED);
		break;

	case STATE_FAILED:
		this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
		break;
	}
}

bool 
ReadFriendListTask::Read()
{
	netStatus* status = &m_MyStatus;
	RLPC_CREATETASK(ReadCachedFriendListTask,
				    m_ListType,
				    m_ProfileId,
				    m_FirstPlayerIndex,
				    m_MaxPlayersToRead,
				    m_Players,
					status);
}

bool 
ReadFriendListTask::GetPresenceData()
{
	rtry
	{
		const char* addJoinAttr = GetRgscConcreteInstance()->GetAdditionalJoinAttr();
		bool queryAddJoinAttr = (addJoinAttr != NULL && addJoinAttr[0] != '\0');
		
		u32 numGamerHandlesToQuery = 0;
		u32 numPlayers = m_PlayerList->GetNumPlayers();
		for(u32 i = 0; (i < numPlayers) && (i < MAX_PAGE_SIZE); i++)
		{
			Player* playerV1 = m_PlayerList->GetPlayer(i);
			if(playerV1->GetRelationship() == IPlayerLatestVersion::RELATIONSHIP_FRIEND &&
			   playerV1->IsOnline() &&
			   playerV1->IsPlayingSameTitle())
			{
				m_GamerHandles[numGamerHandlesToQuery].ResetSc(playerV1->GetRockstarId());
				m_PresenceAttrs[numGamerHandlesToQuery][GAME_JOINABLE_IDX].Reset(rlScAttributeId::IsGameJoinable.Name, (s64)0);
				m_PresenceAttrs[numGamerHandlesToQuery][PARTY_JOINABLE_IDX].Reset(rlScAttributeId::IsPartyJoinable.Name, (s64)0);
				
				// additional joinable attribute
				if(queryAddJoinAttr)
				{
					m_PresenceAttrs[numGamerHandlesToQuery][ADDITIONAL_JOIN_IDX].Reset(addJoinAttr, (s64)0);
				}

				m_Attrs[numGamerHandlesToQuery] = m_PresenceAttrs[numGamerHandlesToQuery];

				numGamerHandlesToQuery++;
			}
		}

		u32 numAttrs = MAX_NUM_ATTRS;

		// the additional join attribute is requested conditionally
		if(queryAddJoinAttr == false)
		{
			--numAttrs;
		}

		if((numGamerHandlesToQuery > 0) && (numAttrs > 0))
		{
			rverify(rlPresence::GetAttributesForGamers(RL_RGSC_GAMER_INDEX, m_GamerHandles, numGamerHandlesToQuery, m_Attrs, numAttrs, &m_MyStatus), catchall, );
		}
		else
		{
			m_MyStatus.ForceSucceeded();
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

// this will be needed if we decide to expose other types of lists to the game.
// the task attempts to map Xbox Live style queries (i.e. startingIndex, maxRecords)
// to ROS style queries (pageIndex, pageSize).
// NOTE: not thoroughly tested
#if 0
///////////////////////////////////////////////////////////////////////////////
//  ReadPlayerListBaseTask
///////////////////////////////////////////////////////////////////////////////

class ReadPlayerListBaseTask : public PlayerManagerTaskBase
{
public:
	RL_TASK_DECL(ReadPlayerListBaseTask);

	enum State
	{
		STATE_INVALID   = -1,
		STATE_READ,
		STATE_READING,
		STATE_PROCESS_RESULTS,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	ReadPlayerListBaseTask();

	bool Configure(PlayerManager* ctx,
				   IPlayerManagerLatestVersion::PlayerListType playerListType,
				   RockstarId profileId,
				   const u32 firstPlayerIndex,
				   const u32 maxPlayersToRead,
				   IPlayerList* players);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

private:
	bool ReadPage();
	bool ProcessResults();
	void UpdateState();

	// parameters and returns
	RockstarId m_ProfileId;
	IPlayerManagerLatestVersion::PlayerListType m_ListType;
	u32 m_FirstPlayerIndex;
	u32 m_MaxPlayersToRead;
	IPlayerList* m_Players;

	// internal state
	static const u32 PAGE_SIZE = 100;
	u32 m_LastPlayerIndex;
	u32 m_CurrentPlayerIndex;

	u32 m_FirstPageIndex;
	u32 m_LastPageIndex;
	u32 m_CurrentPageIndex;

	rlScPlayer m_RosResults[PAGE_SIZE];
	u32 m_RosResultCount;
	u32 m_RosResultTotalCount;

	int m_State;
	netStatus m_MyStatus;
};

ReadPlayerListBaseTask::ReadPlayerListBaseTask()
: m_State(STATE_INVALID)
{
	rlDebug2("sizeof(ReadPlayerListBaseTask): %d", sizeof(ReadPlayerListBaseTask));
}

bool 
ReadPlayerListBaseTask::Configure(PlayerManager* /*ctx*/,
								  IPlayerManagerLatestVersion::PlayerListType listType,
								  RockstarId profileId,
								  const u32 firstPlayerIndex,
								  const u32 maxPlayersToRead,
								  IPlayerList* players)
{
	bool success = false;

	rtry
	{
		rverify(maxPlayersToRead > 0, catchall, );

		bool isOnline = GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal();
		bool isSigningIn = GetRgscConcreteInstance()->_GetProfileManager()->IsSigningIn();

		rverify(isOnline || isSigningIn, catchall, );
		rverify(players != NULL, catchall, );

		PlayerListV1* listV1 = NULL;
		RGSC_HRESULT hr = players->QueryInterface(IID_IPlayerListV1, (void**) &listV1);
		rverify(SUCCEEDED(hr) && (listV1 != NULL), catchall, );

		m_ListType = listType;
		m_ProfileId = profileId;

		m_RosResultCount = 0;
		m_RosResultTotalCount = 0;

		m_FirstPlayerIndex = firstPlayerIndex;
		m_LastPlayerIndex = m_FirstPlayerIndex + maxPlayersToRead - 1;
		m_CurrentPlayerIndex = m_FirstPlayerIndex;

		m_FirstPageIndex = m_FirstPlayerIndex / PAGE_SIZE;
		m_LastPageIndex = m_LastPlayerIndex / PAGE_SIZE;
		m_CurrentPageIndex = m_FirstPageIndex;

		m_Players = players;

		m_State = STATE_INVALID;

		success = true;
	}
	rcatchall
	{

	}
	
	return success;
}

void
ReadPlayerListBaseTask::Start()
{
	rlDebug2("Reading player list...");

	this->PlayerManagerTaskBase::Start();

	m_State = STATE_READ;
}

void
ReadPlayerListBaseTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("Reading player list %s (%ums)", FinishString(finishType), GetElapsedTime());

	if(finishType == FINISH_SUCCEEDED)
	{
		m_Ctx->SortPlayers(m_Players);
	}

	this->PlayerManagerTaskBase::Finish(finishType, resultCode);
}

void
ReadPlayerListBaseTask::DoCancel()
{
	rlPcTask::CancelTaskIfNecessary(&m_MyStatus);

	for(unsigned i = 0; i < COUNTOF(m_SubTaskStatus); ++i)
	{
		rlPcTask::CancelTaskIfNecessary(&m_SubTaskStatus[i]);
	}

	this->PlayerManagerTaskBase::DoCancel();
}

void
ReadPlayerListBaseTask::Update(const unsigned timeStep)
{
	this->PlayerManagerTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		bool isPending = m_MyStatus.Pending();

		if(!isPending)
		{
			for(unsigned i = 0; i < COUNTOF(m_SubTaskStatus); ++i)
			{
				if(m_SubTaskStatus[i].Pending())
				{
					isPending = true;
					break;
				}
			}
		}

		if(!isPending)
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_READ:
			if(this->ReadPage())
			{
				m_State = STATE_READING;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_READING:
			if(m_MyStatus.Succeeded())
			{
				m_State = STATE_PROCESS_RESULTS;
			}
			else if(!m_MyStatus.Pending())
			{
				m_State = STATE_FAILED;

				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			}
			break;

		case STATE_PROCESS_RESULTS:
			if(this->ProcessResults())
			{
				UpdateState();
			}
			else
			{
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			}
			break;

		case STATE_SUCCEEDED:
			this->Finish(FINISH_SUCCEEDED);
			break;

		case STATE_FAILED:
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool 
ReadPlayerListBaseTask::ReadPage()
{
	if(m_ListType == PlayerManager::LIST_TYPE_FRIENDS)
	{
		return rlScFriends::GetFriends(0,
									   m_CurrentPageIndex,
									   PAGE_SIZE,
									   m_RosResults,
									   sizeof(m_RosResults[0]),
									   &m_RosResultCount,
									   &m_RosResultTotalCount,
									   &m_MyStatus);
	}
	else if(m_ListType == PlayerManager::LIST_TYPE_INVITES_SENT)
	{
		return rlScFriends::GetInvitesSent(0,
										   m_CurrentPageIndex,
										   PAGE_SIZE,
										   m_RosResults,
										   &m_RosResultCount,
										   &m_RosResultTotalCount,
										   &m_MyStatus);
	}
	else if(m_ListType == PlayerManager::LIST_TYPE_INVITES_RECEIVED)
	{
		return rlScFriends::GetInvitesReceived(0,
											   m_CurrentPageIndex,
											   PAGE_SIZE,
											   m_RosResults,
											   &m_RosResultCount,
											   &m_RosResultTotalCount,
											   &m_MyStatus);
	}
	else if(m_ListType == (PlayerManager::LIST_TYPE_FRIENDS |
						   PlayerManager::LIST_TYPE_INVITES_SENT))
	{
		return rlScFriends::GetFriendsAndInvitesSent(0,
													 m_CurrentPageIndex,
													 PAGE_SIZE,
													 m_RosResults,
													 sizeof(m_RosResults[0]),
													 &m_RosResultCount,
													 &m_RosResultTotalCount,
													 &m_MyStatus);
	}
	else if(m_ListType == PlayerManager::LIST_TYPE_BLOCKED_PLAYERS)
	{
		return rlScFriends::GetBlockedPlayers(0,
											  m_CurrentPageIndex,
											  PAGE_SIZE,
											  m_RosResults,
											  sizeof(m_RosResults[0]),
											  &m_RosResultCount,
											  &m_RosResultTotalCount,
											  &m_MyStatus);
	}

	Assertf(false, "invalid list type or combination requested");
	return false;
}

bool 
ReadPlayerListBaseTask::ProcessResults()
{
	bool success = false;

	rtry
	{
		rcheck(m_RosResultCount > 0, noresults, );
		PlayerListV1* listV1 = NULL;
		RGSC_HRESULT hr = m_Players->QueryInterface(IID_IPlayerListV1, (void**) &listV1);
		rverify(SUCCEEDED(hr) && (listV1 != NULL), catchall, );

		u32 firstPlayerIndexOnPage = m_CurrentPageIndex * PAGE_SIZE;
		Assert(firstPlayerIndexOnPage < IPlayerManager::MAX_FRIENDS);

		u32 lastPlayerIndexOnPage = firstPlayerIndexOnPage + m_RosResultCount - 1;
		Assert(lastPlayerIndexOnPage < IPlayerManager::MAX_FRIENDS);

		u32 firstResultIndexToUse = m_CurrentPlayerIndex - firstPlayerIndexOnPage;

		rcheck(firstResultIndexToUse < m_RosResultCount, noresults, );

		u32 lastResultIndexToUse = lastPlayerIndexOnPage;
		if(lastResultIndexToUse > m_LastPlayerIndex)
		{
			lastResultIndexToUse = m_LastPlayerIndex;
		}

		Assert(firstResultIndexToUse <= lastResultIndexToUse);

		for(u32 i = firstResultIndexToUse; i <= lastResultIndexToUse; i++)
		{
			Assert(i < PAGE_SIZE);
			Assert(i <= m_RosResultCount);
			rlScPlayer* rosPlayer = &m_RosResults[i];
			Assert(rosPlayer->IsValid());

			u32 playerIndex = listV1->GetNumPlayers();
			Assert(playerIndex == m_CurrentPlayerIndex);
			m_CurrentPlayerIndex++;
			rverify(listV1->AddPlayer(), catchall, );
			PlayerV1* v1 = listV1->GetPlayer(playerIndex);
			v1->m_RockstarId = rosPlayer->m_RockstarId;
			safecpy(v1->m_Name, rosPlayer->m_Nickname, IPlayer::MAX_NICKNAME_CHARS);
			v1->m_Relationship = ConvertRlRelationshipToRgsc(rosPlayer->m_Relationship);
		}

		success = true;
	}
	rcatch(noresults)
	{
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void 
ReadPlayerListBaseTask::UpdateState()
{
	// if the last page wasn't full, then there are no more players to grab
	if((m_CurrentPlayerIndex < m_LastPlayerIndex) &&
	   (m_RosResultCount == PAGE_SIZE))
	{
		m_CurrentPageIndex++;
		m_State = STATE_READ;
	}
	else
	{
		m_State = STATE_SUCCEEDED;
	}
}
#endif

///////////////////////////////////////////////////////////////////////////////
//  GetPlayerInfoAsJsonTask
///////////////////////////////////////////////////////////////////////////////

class GetPlayerInfoAsJsonTask : public PlayerManagerTaskBase
{
public:
	RL_TASK_DECL(GetPlayerInfoAsJsonTask);

	enum State
	{
		STATE_INVALID   = -1,
		STATE_GET_INFO,
		STATE_GETTING_INFO,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	GetPlayerInfoAsJsonTask();

	bool Configure(RgscPlayerManager* ctx,
				   const int requestId,
				   RockstarId rockstarId);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

private:
	bool GetInfo();
	bool SendResults(bool operationSucceeded);

	// parameters and returns
	int m_RequestId;
	RockstarId m_RockstarId;

	// internal state
	int m_State;
	netStatus m_MyStatus;
};

RGSC_HRESULT 
RgscPlayerManager::GetPlayerInfoAsJson(const char* json)
{
	int requestId = -1;
	RockstarId rockstarId = InvalidRockstarId;
	GetRequestIdAndRockstarIdFromJson(json, requestId, rockstarId);

	RLPC_CREATETASK_NO_STATUS(GetPlayerInfoAsJsonTask,
							  requestId,
							  rockstarId);
}

GetPlayerInfoAsJsonTask::GetPlayerInfoAsJsonTask()
: m_State(STATE_INVALID)
{

}

bool 
GetPlayerInfoAsJsonTask::Configure(RgscPlayerManager* /*ctx*/,
								   const int requestId,
								   RockstarId rockstarId)
{
	bool success = false;

	rtry
	{
		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal(), catchall, );

		m_RequestId = requestId;
		m_RockstarId = rockstarId;

		m_State = STATE_INVALID;

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void
GetPlayerInfoAsJsonTask::Start()
{
	rlDebug2("Getting player info...");

	this->PlayerManagerTaskBase::Start();

	m_State = STATE_GET_INFO;
}

void
GetPlayerInfoAsJsonTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("Getting player info %s (%ums)", FinishString(finishType), GetElapsedTime());

	this->PlayerManagerTaskBase::Finish(finishType, resultCode);
}

void
GetPlayerInfoAsJsonTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
	this->PlayerManagerTaskBase::DoCancel();
}

void
GetPlayerInfoAsJsonTask::Update(const unsigned timeStep)
{
	this->PlayerManagerTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
			case STATE_GET_INFO:
				if(this->GetInfo())
				{
					m_State = STATE_GETTING_INFO;
				}
				else
				{
					m_State = STATE_FAILED;
				}
				break;

			case STATE_GETTING_INFO:
				if(m_MyStatus.Succeeded())
				{
					m_State = STATE_SUCCEEDED;
				}
				else if(!m_MyStatus.Pending())
				{
					m_State = STATE_FAILED;
				}
				break;

			case STATE_SUCCEEDED:
				SendResults(true);
				this->Finish(FINISH_SUCCEEDED);
				break;

			case STATE_FAILED:
				SendResults(false);
				this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
				break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool 
GetPlayerInfoAsJsonTask::GetInfo()
{
	// TODO: NS - placeholder until we get a web service to retrieve the info we need.
	//			  The real work is done in SendResults() for now.

	m_MyStatus.SetPending();
	m_MyStatus.SetSucceeded();
	return true;
}

bool 
GetPlayerInfoAsJsonTask::SendResults(bool operationSucceeded)
{
	json_char *jc = "";
	bool success = false;

	rtry
	{
		JSONNODE* n = json_new(JSON_NODE);
		rverify(n, catchall, );

		rlGamerHandle gh;
		gh.ResetSc(m_RockstarId);
		
		rlFriend* f = rlFriendsManager::GetDefaultFriendPage()->GetFriend(gh);

		if(f == NULL)
		{
			json_push_back(n, json_new_i("RequestId", m_RequestId));
			json_push_back(n, json_new_b("Success", false));
			json_push_back(n, json_new_i("ResultCode", 0));
		}
		else
		{
			json_push_back(n, json_new_i("RequestId", m_RequestId));
			json_push_back(n, json_new_b("Success", true));
			json_push_back(n, json_new_i("ResultCode", 0));
			
			char szRockstarId[64];
			formatf(szRockstarId, "%"I64FMT"d", m_RockstarId);
			json_push_back(n, json_new_a("RockstarId", szRockstarId));
			json_push_back(n, json_new_a("DisplayName", f->GetName()));
			json_push_back(n, json_new_b("IsOnline", f->IsOnline()));
			json_push_back(n, json_new_b("IsPlayingSameTitle", f->IsInSameTitle()));
			json_push_back(n, json_new_a("RichPresence", f->GetRichPresence()));
		}

		jc = json_write_formatted(n);
		rverify(jc, catchall, );

		json_delete(n);

		bool succeeded = false;
		rgsc::Script::JsPlayerInfoResponse(&succeeded, jc);

		json_free_safe(jc);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

///////////////////////////////////////////////////////////////////////////////
//  ReadPlayerListAsJsonBaseTask
///////////////////////////////////////////////////////////////////////////////

class ReadPlayerListAsJsonBaseTask : public PlayerManagerTaskBase
{
public:
	RL_TASK_DECL(ReadPlayerListAsJsonBaseTask);

	enum AttrIndex
	{
		GAME_JOINABLE_IDX = 0,
		PARTY_JOINABLE_IDX,
		ADDITIONAL_JOIN_IDX,
		MAX_NUM_ATTRS,
	};

	enum State
	{
		STATE_INVALID   = -1,
		STATE_READ,
		STATE_READING,
		STATE_GET_PRESENCE_DATA,
		STATE_GETTING_PRESENCE_DATA,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	ReadPlayerListAsJsonBaseTask();

	bool Configure(RgscPlayerManager* ctx,
				   const int requestId,
				   IPlayerManagerLatestVersion::PlayerListType listType,
				   RockstarId profileId,
				   const u32 pageIndex,
				   const u32 pageSize);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

private:
	bool ReadPage();
	bool GetPresenceData();
	bool SendResults();

	// parameters and returns
	int m_RequestId;
	RockstarId m_ProfileId;
	IPlayerManagerLatestVersion::PlayerListType m_ListType;
	u32 m_PageIndex;
	u32 m_PageSize;

	// internal state
	static const u32 MAX_PAGE_SIZE = 100;
	rlScPlayer m_RosResults[MAX_PAGE_SIZE];
	rlScPresenceAttribute m_PresenceAttrs[MAX_PAGE_SIZE][MAX_NUM_ATTRS];
	netStatus m_SubTaskStatus[MAX_PAGE_SIZE];
	u8 m_CanJoinViaPresence[MAX_PAGE_SIZE];

	u32 m_RosResultCount;
	u32 m_RosResultTotalCount;

	int m_State;
	netStatus m_MyStatus;
};

RGSC_HRESULT 
RgscPlayerManager::ReadPlayerListAsJson(const char* json)
{
	int requestId = -1;
	RockstarId rockstarId = InvalidRockstarId;
	PlayerListType listType = LIST_TYPE_INVALID;
	int pageIndex = -1;
	int pageSize = -1;

	JSONNODE *n = json_parse(json);

	if(!AssertVerify(n != NULL))
	{
		return false;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return false;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "RequestId") == 0)
		{
			requestId = json_as_int(*i);
		}
		else if(_stricmp(node_name, "RockstarId") == 0)
		{
			json_char *node_value = json_as_string_or_null(*i);
			sscanf_s(node_value, "%I64d", &rockstarId);
			json_free_safe(node_value);
		}
		else if(_stricmp(node_name, "PlayerListType") == 0)
		{
			listType = (PlayerListType)json_as_int(*i);
		}
		else if(_stricmp(node_name, "PageIndex") == 0)
		{
			pageIndex = json_as_int(*i);
		}
		else if(_stricmp(node_name, "PageSize") == 0)
		{
			pageSize = json_as_int(*i);
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	RLPC_CREATETASK_NO_STATUS(ReadPlayerListAsJsonBaseTask,
							  requestId,
							  listType,
							  rockstarId,
							  (u32)pageIndex,
							  (u32)pageSize);
}

ReadPlayerListAsJsonBaseTask::ReadPlayerListAsJsonBaseTask()
: m_State(STATE_INVALID)
{
	rlDebug2("sizeof(ReadPlayerListAsJsonBaseTask): %d", sizeof(ReadPlayerListAsJsonBaseTask));
}

bool 
ReadPlayerListAsJsonBaseTask::Configure(RgscPlayerManager* /*ctx*/,
										const int requestId,
										IPlayerManagerLatestVersion::PlayerListType listType,
										RockstarId profileId,
										const u32 pageIndex,
										const u32 pageSize)
{
	bool success = false;

	rtry
	{
		rverify(pageSize <= MAX_PAGE_SIZE, catchall, );

		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal(), catchall, );

		m_RequestId = requestId;
		m_ListType = listType;
		m_ProfileId = profileId;
		m_PageIndex = pageIndex;
		m_PageSize = pageSize;
		m_RosResultCount = 0;
		m_RosResultTotalCount = 0;

		for(unsigned i = 0; i < MAX_PAGE_SIZE; ++i)
		{
			m_CanJoinViaPresence[i] = 0;
		}

		m_State = STATE_INVALID;

		success = true;
	}
	rcatchall
	{

	}
	
	return success;
}

void
ReadPlayerListAsJsonBaseTask::Start()
{
	rlDebug2("Reading player list...");

	this->PlayerManagerTaskBase::Start();

	m_State = STATE_READ;
}

void
ReadPlayerListAsJsonBaseTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("Reading player list %s (%ums)", FinishString(finishType), GetElapsedTime());

	this->PlayerManagerTaskBase::Finish(finishType, resultCode);
}

void
ReadPlayerListAsJsonBaseTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);

	for(unsigned i = 0; i < COUNTOF(m_SubTaskStatus); ++i)
	{
		RgscTaskManager::CancelTaskIfNecessary(&m_SubTaskStatus[i]);
	}

	this->PlayerManagerTaskBase::DoCancel();
}

void
ReadPlayerListAsJsonBaseTask::Update(const unsigned timeStep)
{
	this->PlayerManagerTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		bool isPending = m_MyStatus.Pending();

		if(!isPending)
		{
			for(unsigned i = 0; i < COUNTOF(m_SubTaskStatus); ++i)
			{
				if(m_SubTaskStatus[i].Pending())
				{
					isPending = true;
					break;
				}
			}
		}

		if(!isPending)
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_READ:
			if(this->ReadPage())
			{
				m_State = STATE_READING;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_READING:
			if(m_MyStatus.Succeeded())
			{
				m_State = STATE_GET_PRESENCE_DATA;
			}
			else if(!m_MyStatus.Pending())
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_GET_PRESENCE_DATA:
			if(this->GetPresenceData())
			{
				m_State = STATE_GETTING_PRESENCE_DATA;
			}
			else
			{
				// if we failed to get presence data
				// we can still return the player list
				m_State = STATE_SUCCEEDED;
			}
			break;

		case STATE_GETTING_PRESENCE_DATA:
			{
				bool isPending = false;
				for(unsigned i = 0; i < COUNTOF(m_SubTaskStatus); ++i)
				{
					if(m_SubTaskStatus[i].Pending())
					{
						isPending = true;
						break;
					}
				}

				if(isPending == false)
				{
					for(unsigned i = 0; i < COUNTOF(m_SubTaskStatus); ++i)
					{
						if(m_SubTaskStatus[i].Succeeded())
						{
							s64 isGameJoinable = 0;
							s64 isPartyJoinable = 0;
							s64 isAdditionalJoinable = 0;

							if(m_PresenceAttrs[i][GAME_JOINABLE_IDX].Type == RLSC_PRESTYPE_S64)
							{
								m_PresenceAttrs[i][GAME_JOINABLE_IDX].GetValue(&isGameJoinable);
							}

							if(m_PresenceAttrs[i][PARTY_JOINABLE_IDX].Type == RLSC_PRESTYPE_S64)
							{
								m_PresenceAttrs[i][PARTY_JOINABLE_IDX].GetValue(&isPartyJoinable);
							}

							if(m_PresenceAttrs[i][ADDITIONAL_JOIN_IDX].Type == RLSC_PRESTYPE_S64)
							{
								m_PresenceAttrs[i][ADDITIONAL_JOIN_IDX].GetValue(&isAdditionalJoinable);
							}

							m_CanJoinViaPresence[i] = (isGameJoinable == 1) || (isPartyJoinable == 1) || (isAdditionalJoinable == 1);
						}
					}

					// if we failed to get presence data
					// we can still return the player list
					m_MyStatus.SetSucceeded();
					m_State = STATE_SUCCEEDED;
				}
			}
			break;

		case STATE_SUCCEEDED:
			SendResults();
			this->Finish(FINISH_SUCCEEDED);
			break;

		case STATE_FAILED:
			m_RosResultCount = 0;
			m_RosResultTotalCount = 0;
			SendResults();
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool 
ReadPlayerListAsJsonBaseTask::ReadPage()
{
	// these may be exposed to the game in the future in players_interface.h
	// 		LIST_TYPE_INVITES_SENT = 0x02,
	// 		LIST_TYPE_INVITES_RECEIVED = 0x04,
	// 		LIST_TYPE_BLOCKED_PLAYERS = 0x08,

	const unsigned LIST_TYPE_INVITES_SENT = 0x02;
	const unsigned LIST_TYPE_INVITES_RECEIVED = 0x04;
	const unsigned LIST_TYPE_BLOCKED_PLAYERS = 0x08;

	if(m_ListType == RgscPlayerManager::LIST_TYPE_FRIENDS)
	{
		return rlScFriends::GetFriends(0,
									   m_PageIndex,
									   m_PageSize,
									   m_RosResults,
									   sizeof(m_RosResults[0]),
									   &m_RosResultCount,
									   &m_RosResultTotalCount,
									   &m_MyStatus);
	}
	else if(m_ListType == LIST_TYPE_INVITES_SENT)
	{
		return rlScFriends::GetInvitesSent(0,
										   m_PageIndex,
										   m_PageSize,
										   m_RosResults,
										   sizeof(m_RosResults[0]),
										   &m_RosResultCount,
										   &m_RosResultTotalCount,
										   &m_MyStatus);
	}
	else if(m_ListType == LIST_TYPE_INVITES_RECEIVED)
	{
		return rlScFriends::GetInvitesReceived(0,
											   m_PageIndex,
											   m_PageSize,
											   m_RosResults,
											   sizeof(m_RosResults[0]),
											   &m_RosResultCount,
											   &m_RosResultTotalCount,
											   &m_MyStatus);
	}
	else if(m_ListType == (RgscPlayerManager::LIST_TYPE_FRIENDS |
						   LIST_TYPE_INVITES_SENT))
	{
		return rlScFriends::GetFriendsAndInvitesSent(0,
													 m_PageIndex,
													 m_PageSize,
													 m_RosResults,
													 sizeof(m_RosResults[0]),
													 &m_RosResultCount,
													 &m_RosResultTotalCount,
													 &m_MyStatus);
	}
	else if(m_ListType == LIST_TYPE_BLOCKED_PLAYERS)
	{
		return rlScFriends::GetBlocked(0,
									   m_PageIndex,
									   m_PageSize,
									   m_RosResults,
									   sizeof(m_RosResults[0]),
									   &m_RosResultCount,
									   &m_RosResultTotalCount,
									   &m_MyStatus);
	}

	Assertf(false, "invalid list type or combination requested");
	return false;
}

bool 
ReadPlayerListAsJsonBaseTask::GetPresenceData()
{
	bool success = false;

	rtry
	{
		for(u32 i = 0; i < m_RosResultCount; i++)
		{
			rlScPlayer* rosPlayer = &m_RosResults[i];

			if(rosPlayer->m_Relationship == RLSC_RELATIONSHIP_FRIEND &&
			   rosPlayer->m_IsOnline &&
			   rosPlayer->m_IsPlayingSameTitle)
			{
				int numAttrs = 0;

				rlGamerHandle gh;
				gh.ResetSc(rosPlayer->m_RockstarId);
				m_PresenceAttrs[i][GAME_JOINABLE_IDX].Reset(rlScAttributeId::IsGameJoinable.Name, (s64)0); numAttrs++;
				m_PresenceAttrs[i][PARTY_JOINABLE_IDX].Reset(rlScAttributeId::IsPartyJoinable.Name, (s64)0); numAttrs++;

				// additional joinable attribute
				const char* addJoinAttr = GetRgscConcreteInstance()->GetAdditionalJoinAttr();
				if (addJoinAttr != NULL && addJoinAttr[0] != '\0')
				{
					m_PresenceAttrs[i][ADDITIONAL_JOIN_IDX].Reset(addJoinAttr, (s64)0); numAttrs++;
				}

				rverify(rlPresence::GetAttributesForGamer(0, gh, &m_PresenceAttrs[i][0], numAttrs, &m_SubTaskStatus[i]), catchall, );
			}
		}

		success = true;
	}
	rcatchall
	{

	}

	if(success)
	{
		m_MyStatus.SetPending();
	}
	else
	{
		m_MyStatus.ForceFailed();
	}

	return success;
}

bool 
ReadPlayerListAsJsonBaseTask::SendResults()
{
	json_char *jc = "";
	bool success = false;

	rtry
	{
		JSONNODE* n = json_new(JSON_NODE);
		rverify(n, catchall, );

		JSONNODE* c = json_new(JSON_ARRAY);
		rverify(c, catchall, );

		json_set_name(c, "Players");

		for(u32 i = 0; i < m_RosResultCount; i++)
		{
			rlScPlayer* rosPlayer = &m_RosResults[i];
			Assert(rosPlayer->IsValid());

			JSONNODE* p = json_new(JSON_NODE);
			rverify(p, catchall, );

			IPlayerLatestVersion::Relationship relationship = ConvertRlRelationshipToRgsc(rosPlayer->m_Relationship);
			bool isOnline = false;
			bool isPlayingSameTitle = false;
			bool canSendGameInvite = false;
			bool canJoinGame = false;
			if(relationship == IPlayerLatestVersion::RELATIONSHIP_FRIEND)
			{
				isOnline = rosPlayer->m_IsOnline;
				isPlayingSameTitle = rosPlayer->m_IsPlayingSameTitle;
				canSendGameInvite = GetRgscConcreteInstance()->_GetPresenceManager()->CanSendGameInvite(rosPlayer->m_RockstarId);
				canSendGameInvite = canSendGameInvite && isOnline && isPlayingSameTitle;
				canJoinGame = m_CanJoinViaPresence[i] == 1 && isPlayingSameTitle;
			}

			char szRockstarId[64];
			formatf(szRockstarId, "%"I64FMT"d", rosPlayer->m_RockstarId);
			json_push_back(p, json_new_a("RockstarId", szRockstarId));
			json_push_back(p, json_new_a("DisplayName", rosPlayer->m_Nickname));
			json_push_back(p, json_new_a("AvatarUrl", rosPlayer->m_AvatarUrl));
			json_push_back(p, json_new_i("Relationship", relationship));
			json_push_back(p, json_new_b("IsOnline", isOnline));
			json_push_back(p, json_new_b("IsPlayingSameTitle", isPlayingSameTitle));
			json_push_back(p, json_new_b("CanSendGameInvite", canSendGameInvite));
			json_push_back(p, json_new_b("CanJoinGame", canJoinGame));
			
			json_push_back(c, p);
		}

		json_push_back(n, json_new_i("RequestId", m_RequestId));
		json_push_back(n, json_new_b("Success", m_MyStatus.Succeeded()));
		json_push_back(n, json_new_i("ResultCode", m_MyStatus.GetResultCode()));
		json_push_back(n, json_new_i("PlayerListType", m_ListType));
		json_push_back(n, json_new_i("PageIndex", m_PageIndex));
		json_push_back(n, json_new_i("PageSize", m_PageSize));
		json_push_back(n, json_new_i("Count", m_RosResultCount));
		json_push_back(n, json_new_i("Total", m_RosResultTotalCount));

		json_push_back(n, c);

		jc = json_write_formatted(n);

#if !__NO_OUTPUT
		size_t len = strlen(jc);
		for(size_t i = 0; i < len; i++)
		{
			printf("%c", jc[i]);
		}
#endif

		json_delete(n);

		bool succeeded = false;
		rgsc::Script::JsPlayerListResponse(&succeeded, jc);

		json_free_safe(jc);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool 
RgscPlayerManager::ReadPlayers(const RockstarId profileId,
						   IPlayerManagerLatestVersion::PlayerListType playerListType,
						   const u32 firstPlayerIndex,
						   IPlayerList* players,
						   IAsyncStatus* status)
{
	rtry
	{
		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal(), catchall2, );

		PlayerListV1* listV1 = NULL;
		RGSC_HRESULT hr = players->QueryInterface(IID_IPlayerListV1, (void**) &listV1);
		rverify(SUCCEEDED(hr) && (listV1 != NULL), catchall2, );
		u32 maxPlayersToRead = listV1->GetMaxPlayers();

		// if 0 is passed as the rockstarId, it means the game wants the local player's friend list
		RockstarId localProfileId = GetRgscConcreteInstance()->_GetProfileManager()->GetSignedInProfile().GetProfileId();
		RockstarId requestedProfileId = profileId;
		if((profileId == InvalidRockstarId) || (profileId == 0))
		{
			requestedProfileId = localProfileId;
		}
		
		rverify((localProfileId == requestedProfileId) && ((playerListType == RgscPlayerManager::LIST_TYPE_FRIENDS) || (playerListType == RgscPlayerManager::LIST_TYPE_FRIENDS_WITH_PRESENCE)), catchall2, 
			rlError("The Social Club SDK currently only allows retreiving the local player's friend list."));

		RLPC_CREATETASK_IASYNC(ReadFriendListTask,
							   playerListType,
							   requestedProfileId,
							   firstPlayerIndex,
							   maxPlayersToRead,
							   players);
	}
	rcatch(catchall2)
	{

	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////
//  GetMultiplayerPresenceInfoAsJsonTask
///////////////////////////////////////////////////////////////////////////////

class GetMultiplayerPresenceInfoAsJsonTask : public PlayerManagerTaskBase
{
public:
	RL_TASK_DECL(GetMultiplayerPresenceInfoAsJsonTask);

	static const u32 MAX_NUM_PLAYERS = 100;

	enum AttrIndex
	{
		GAME_JOINABLE_IDX = 0,
		PARTY_JOINABLE_IDX = 1,
		GAME_SESSION_INFO_IDX = 2,
		PARTY_SESSION_INFO_IDX = 3,
		NUM_CONST_ATTRS = 4,
		// the others are conditional and won't have constant indices
 		//ADDITIONAL_ATTR_IDX = 4,
 		//ADDITIONAL_JOIN_IDX = 5,
 		//RICH_PRESENCE_ATTR_IDX = 6,
		MAX_NUM_ATTRS = 7,
	};

	enum State
	{
		STATE_INVALID   = -1,
		STATE_GET_PRESENCE_DATA,
		STATE_GETTING_PRESENCE_DATA,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	GetMultiplayerPresenceInfoAsJsonTask();

	bool Configure(RgscPlayerManager* ctx,
					const int requestId,
					const unsigned numPlayers,
					const RockstarId* players);
	virtual void Start();
	virtual void Finish(const FinishType finishType, const int resultCode = 0);
	virtual void Update(const unsigned timeStep);
	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

private:
	bool GetPresenceData();
	bool SendResults();

	int m_RequestId;
	unsigned m_NumPofileIds;
	RockstarId m_ProfileIds[MAX_NUM_PLAYERS];
	rlGamerHandle m_GamerHandles[MAX_NUM_PLAYERS];
	rlScPresenceAttribute m_PresenceAttrs[MAX_NUM_PLAYERS][MAX_NUM_ATTRS];
	rlScPresenceAttribute* m_Attrs[MAX_NUM_PLAYERS];

	CompileTimeAssert(MAX_NUM_PLAYERS <= RLSC_PRESENCE_GET_ATTRS_MAX_GAMERS_PER_REQUEST);
	CompileTimeAssert(MAX_NUM_ATTRS <= RLSC_PRESENCE_GET_ATTRS_MAX_ATTRS_PER_GAMER);

	u8 m_CanJoinViaPresence[MAX_NUM_PLAYERS];
	u8 m_AdditionalSessionJoinable[MAX_NUM_PLAYERS];

	int m_RichPresenceIdx;
	int m_AdditionalJoinAttribIdx;
	int m_AdditionalAttribIdx;
	unsigned m_NumAttrs;

	int m_State;
	netStatus m_MyStatus;
};

RGSC_HRESULT 
RgscPlayerManager::GetPlayerMultiplayerPresenceInfoAsJson(const char* json)
{
	/*
		Example:
		{
			"RequestId" : 5,
			"Players" : [
				{
					"RockstarId" : "28942581238732",
				},
				{
					"RockstarId" : "138871347913491",
				},
				{
					"RockstarId" : "7734266369619707088",
				}
			]
		}
	*/

	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rlFriend* friends = NULL;
		unsigned numFriends = 0;

		rverify(rlPresence::GetFriendsArray(0, &friends, &numFriends), catchall2, rlError("Couldn't get cached friend list from rlPresence"));

		int requestId = -1;
		RockstarId rockstarIds[GetMultiplayerPresenceInfoAsJsonTask::MAX_NUM_PLAYERS] = {0};
		unsigned numPlayers = 0;

		RsonReader rr(json, (unsigned)strlen(json));
		rr.ReadInt("RequestId", requestId);

		RsonReader rrPlayers;
		rverify(rr.GetMember("Players", &rrPlayers), catchall2, );

		RsonReader rrPlayer;
		bool morePlayers = rrPlayers.GetFirstMember(&rrPlayer);

		while(morePlayers && (numPlayers < COUNTOF(rockstarIds)))
		{
			RockstarId rockstarId = InvalidRockstarId;
			if(rrPlayer.ReadInt64("RockstarId", rockstarId))
			{
				// filter out players who are not playing the same title and players who aren't friends
				for(unsigned i = 0; i < numFriends; ++i)
				{
					rlGamerHandle gh;
					gh.ResetSc(rockstarId);

					rlGamerHandle friendHandle;
					friends[i].GetGamerHandle(&friendHandle);

					if(friendHandle == gh)
					{
						if(friends[i].IsOnline() && friends[i].IsInSameTitle())
						{
							rockstarIds[numPlayers++] = rockstarId;
						}
						break;
					}
				}
			}

			morePlayers = rrPlayer.GetNextSibling(&rrPlayer);
		}

		RLPC_CREATETASK_NO_STATUS(GetMultiplayerPresenceInfoAsJsonTask,
									requestId,
									numPlayers,
									rockstarIds);
	}
	rcatch(catchall2)
	{

	}

	return hr;
}

GetMultiplayerPresenceInfoAsJsonTask::GetMultiplayerPresenceInfoAsJsonTask()
: m_State(STATE_INVALID)
{

}

bool 
GetMultiplayerPresenceInfoAsJsonTask::Configure(RgscPlayerManager* ctx,
													const int requestId,
													const unsigned numProfileIds,
													const RockstarId* profileIds)
{
	bool success = false;

	rtry
	{
		rverify(numProfileIds <= MAX_NUM_PLAYERS, catchall, );
		rverify(profileIds !=  NULL, catchall, );
		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal(), catchall, );

		// init arrays
		for(unsigned i = 0; i < MAX_NUM_PLAYERS; ++i)
		{
			m_ProfileIds[MAX_NUM_PLAYERS] = InvalidRockstarId;
			m_CanJoinViaPresence[i] = 0;
			m_AdditionalSessionJoinable[i] = 0;
			for (int j = 0; j < MAX_NUM_ATTRS; j++)
			{
				m_PresenceAttrs[i][j].Clear();
			}
			m_Attrs[i] = m_PresenceAttrs[i];
		}

		// determine attribute indicies and num attributes to request
		m_RichPresenceIdx = -1;
		m_AdditionalJoinAttribIdx = -1;
		m_AdditionalAttribIdx = -1;

		m_NumAttrs = NUM_CONST_ATTRS;

		const char* addSessionAttr = GetRgscConcreteInstance()->GetAdditionalSessionAttr();
		if (addSessionAttr != NULL && addSessionAttr[0] != '\0')
		{
			m_AdditionalAttribIdx = m_NumAttrs++;

			// only request additional joinable (i.e. trjoin) if the session info is valid.
			// we don't want to mix up our indices
			const char* addJoinAttr = GetRgscConcreteInstance()->GetAdditionalJoinAttr();
			if (addJoinAttr != NULL && addJoinAttr[0] != '\0')
			{
				m_AdditionalJoinAttribIdx = m_NumAttrs++;
			}
		}

		if(ctx->HasDoneInitialRichPresenceQuery() == false)
		{
			m_RichPresenceIdx = m_NumAttrs++;
		}

		rverify(m_NumAttrs <= MAX_NUM_ATTRS, catchall, );

		m_RequestId = requestId;
		m_NumPofileIds = numProfileIds;

		for(unsigned i = 0; i < m_NumPofileIds; ++i)
		{
			m_ProfileIds[i] = profileIds[i];
			m_GamerHandles[i].ResetSc(profileIds[i]);
		}

		m_State = STATE_INVALID;

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void
GetMultiplayerPresenceInfoAsJsonTask::Start()
{
	rlDebug2("Getting multiplayer presence info...");

	this->PlayerManagerTaskBase::Start();

	m_State = STATE_GET_PRESENCE_DATA;
}

void
GetMultiplayerPresenceInfoAsJsonTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("Getting multiplayer presence info %s (%ums)", FinishString(finishType), GetElapsedTime());

	this->PlayerManagerTaskBase::Finish(finishType, resultCode);
}

void
GetMultiplayerPresenceInfoAsJsonTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
	this->PlayerManagerTaskBase::DoCancel();
}

void
GetMultiplayerPresenceInfoAsJsonTask::Update(const unsigned timeStep)
{
	this->PlayerManagerTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_GET_PRESENCE_DATA:
			if (m_NumPofileIds == 0)
			{
				// Skip all reads, and go straight to the results.
				m_MyStatus.SetPending();
				m_MyStatus.SetSucceeded();
				m_State = STATE_SUCCEEDED;
			}
			else if(this->GetPresenceData())
			{
				m_State = STATE_GETTING_PRESENCE_DATA;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_GETTING_PRESENCE_DATA:
			if(m_MyStatus.Succeeded())
			{
				for(unsigned i = 0; i < m_NumPofileIds; ++i)
				{
					s64 isGameJoinable = 0;
					s64 isPartyJoinable = 0;
					s64 isAdditionalSessionJoinable = 0;

					if(rlVerify(m_GamerHandles[i].GetRockstarId() == m_ProfileIds[i]))
					{
						if(m_PresenceAttrs[i][GAME_JOINABLE_IDX].Type == RLSC_PRESTYPE_S64)
						{
							m_PresenceAttrs[i][GAME_JOINABLE_IDX].GetValue(&isGameJoinable);
						}

						if(m_PresenceAttrs[i][PARTY_JOINABLE_IDX].Type == RLSC_PRESTYPE_S64)
						{
							m_PresenceAttrs[i][PARTY_JOINABLE_IDX].GetValue(&isPartyJoinable);
						}

						if((m_AdditionalJoinAttribIdx >= 0) && m_PresenceAttrs[i][m_AdditionalJoinAttribIdx].Type == RLSC_PRESTYPE_S64)
						{
							m_PresenceAttrs[i][m_AdditionalJoinAttribIdx].GetValue(&isAdditionalSessionJoinable);
						}
					}

					m_CanJoinViaPresence[i] = (isGameJoinable == 1) || (isPartyJoinable == 1);
					m_AdditionalSessionJoinable[i] = (isAdditionalSessionJoinable == 1);
				}

				m_State = STATE_SUCCEEDED;
			}
			else if(!m_MyStatus.Pending())
			{
				for(unsigned i = 0; i < MAX_NUM_PLAYERS; ++i)
				{
					m_CanJoinViaPresence[i] = 0;
					m_AdditionalSessionJoinable[i] = 0;
				}

				// if we failed to get presence data
				// we tell the SCUI that we can't invite/join this player
				m_MyStatus.Reset();
				m_MyStatus.ForceSucceeded();
				m_State = STATE_SUCCEEDED;
			}
			break;

		case STATE_SUCCEEDED:
			SendResults();
			this->Finish(FINISH_SUCCEEDED);
			break;

		case STATE_FAILED:
			SendResults();
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool 
GetMultiplayerPresenceInfoAsJsonTask::GetPresenceData()
{
	rtry
	{
		for(u32 i = 0; i < m_NumPofileIds; i++)
		{
			m_PresenceAttrs[i][GAME_JOINABLE_IDX].Reset(rlScAttributeId::IsGameJoinable.Name, (s64)0);
			m_PresenceAttrs[i][PARTY_JOINABLE_IDX].Reset(rlScAttributeId::IsPartyJoinable.Name, (s64)0);
			m_PresenceAttrs[i][GAME_SESSION_INFO_IDX].Reset(rlScAttributeId::GameSessionInfo.Name, "");
			m_PresenceAttrs[i][PARTY_SESSION_INFO_IDX].Reset(rlScAttributeId::PartySessionInfo.Name, "");

			if (m_AdditionalAttribIdx >= 0)
			{
				const char* addSessionAttr = GetRgscConcreteInstance()->GetAdditionalSessionAttr();
				m_PresenceAttrs[i][m_AdditionalAttribIdx].Reset(addSessionAttr, "");
			}

			if(m_AdditionalJoinAttribIdx >= 0)
			{
				const char* addJoinAttr = GetRgscConcreteInstance()->GetAdditionalJoinAttr();
				m_PresenceAttrs[i][m_AdditionalJoinAttribIdx].Reset(addJoinAttr, (s64)0);
			}

			if (m_RichPresenceIdx >= 0)
			{
				m_PresenceAttrs[i][m_RichPresenceIdx].Reset(rlScAttributeId::RichPresence.Name, "");
			}

			m_Attrs[i] = m_PresenceAttrs[i];
		}

		if((m_NumPofileIds > 0) && (m_NumAttrs > 0))
		{
			rverify(rlPresence::GetAttributesForGamers(RL_RGSC_GAMER_INDEX, m_GamerHandles, m_NumPofileIds, m_Attrs, m_NumAttrs, &m_MyStatus), catchall, );
		}
		else
		{
			m_MyStatus.ForceSucceeded();
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool 
GetMultiplayerPresenceInfoAsJsonTask::SendResults()
{
	json_char *jc = "";
	bool success = false;

	rtry
	{
		JSONNODE* n = json_new(JSON_NODE);
		rverify(n, catchall, );

		JSONNODE* c = json_new(JSON_ARRAY);
		rverify(c, catchall, );

		json_set_name(c, "Players");

		for(u32 i = 0; i < m_NumPofileIds; i++)
		{
			JSONNODE* p = json_new(JSON_NODE);
			rverify(p, catchall, );

			IPlayerLatestVersion::Relationship relationship = IPlayerLatestVersion::RELATIONSHIP_FRIEND;
			bool canSendGameInvite = false;
			bool canJoinGame = false;

			bool bIsMyGameInvitable = GetRgscConcreteInstance()->_GetPresenceManager()->CanSendGameInvite(m_GamerHandles[i].GetRockstarId());
			bool bIsMyAdditionalSessionInvitable = GetRgscConcreteInstance()->_GetPresenceManager()->CanSendAdditionalSessionInvite(m_GamerHandles[i].GetRockstarId());

			// Get references to my/users game/additional session
			const char* userSessionInfo = m_PresenceAttrs[i][GAME_SESSION_INFO_IDX].StringValue;
			const char* mySessionInfo = GetRgscConcreteInstance()->_GetPresenceManager()->GetGameSessionInfo();
			const char* myAddSessionInfo = GetRgscConcreteInstance()->_GetPresenceManager()->GetAdditionalSessionInfo();
			const char* userAddSessionInfo = (m_AdditionalAttribIdx >= 0) ? m_PresenceAttrs[i][m_AdditionalAttribIdx].StringValue : "";

			// Check if we're in a game session or additional session (or both)
			bool bIsInGameSession = mySessionInfo && mySessionInfo[0] != '\0';
			bool bIsInAddSession = myAddSessionInfo && myAddSessionInfo[0] != '\0';

			// If not in a game session, fall back to the party session
			if (!bIsInGameSession)
			{
				mySessionInfo = GetRgscConcreteInstance()->_GetPresenceManager()->GetPartySessionInfo();
				bIsInGameSession = mySessionInfo && mySessionInfo[0] != '\0';
			}

			// Check if the user is in a game session , or an additional session (or both). 
			bool bIsUserInGameSession = (userSessionInfo[0] != '\0');
			bool bIsUserInAddSession = (userAddSessionInfo[0] != '\0');

			// If user is not in a game session, fall back to their party session
			if (!bIsUserInGameSession)
			{
				userSessionInfo = m_PresenceAttrs[i][PARTY_SESSION_INFO_IDX].StringValue;
				bIsUserInGameSession = (userSessionInfo[0] != '\0');
			}

			// User is in my game session if I'm in a game session and either of the users session infos match my session info
			bool bIsUserInMyGameSession = bIsInGameSession && (strcmp(mySessionInfo, userSessionInfo) == 0 || strcmp(mySessionInfo, userAddSessionInfo) == 0);

			// User is in my additional session if I'm in an additional game session and either of the users session infos match my additional session info
			bool bIsUserInMyAddSession = bIsInAddSession && (strcmp(myAddSessionInfo, userSessionInfo) == 0 || strcmp(myAddSessionInfo, userAddSessionInfo) == 0);

			// We're in the user's game session if the user is in a game session and either of our session infos match their session info
			bool bIsInUsersGameSession = bIsUserInGameSession && (strcmp(userSessionInfo, mySessionInfo) == 0 || strcmp(userSessionInfo, myAddSessionInfo) == 0);

			// We're in the users additional session if the user is in an additional session and either of our session infos match their additional session info
			bool bIsInUsersAddSession = bIsUserInAddSession  && (strcmp(userAddSessionInfo, mySessionInfo) == 0 || strcmp(userAddSessionInfo, myAddSessionInfo) == 0);

			// We can send a game invite if we're in a game session or transition session and the other user is not in that session
			bool bCouldSendGameInvite = (bIsInGameSession && bIsMyGameInvitable && !bIsUserInMyGameSession) || (bIsInAddSession && bIsMyAdditionalSessionInvitable && !bIsUserInMyAddSession);

			// We can join a session if the user is in a game session we're not a part of, or the user is in an additional session we're not a part of
			bool bCouldJoinSession = (bIsUserInGameSession && (m_CanJoinViaPresence[i] == 1) && !bIsInUsersGameSession) || (bIsUserInAddSession && (m_AdditionalSessionJoinable[i] == 1) && !bIsInUsersAddSession);

			if(relationship == IPlayerLatestVersion::RELATIONSHIP_FRIEND)
			{
				canSendGameInvite = bCouldSendGameInvite;
				canJoinGame = bCouldJoinSession;
			}

			char szRockstarId[64];
			formatf(szRockstarId, "%"I64FMT"d", m_GamerHandles[i].GetRockstarId());
			json_push_back(p, json_new_a("RockstarId", szRockstarId));
			json_push_back(p, json_new_b("CanSendGameInvite", canSendGameInvite));
			json_push_back(p, json_new_b("CanJoinGame", canJoinGame));
			json_push_back(p, json_new_b("InSameSession", bIsUserInMyGameSession || bIsInUsersAddSession));

			const char* richPresence = "";
			if (m_RichPresenceIdx >= 0)
			{
				richPresence = m_PresenceAttrs[i][m_RichPresenceIdx].StringValue;
				rlFriendsManager::OnFriendRichPresenceChanged(m_GamerHandles[i], richPresence);
			}
			else
			{
				rlFriend* f = rlFriendsManager::GetDefaultFriendPage()->GetFriend(m_GamerHandles[i]);
				richPresence = f->GetRichPresence();
			}

			json_push_back(p, json_new_a("RichPresence", richPresence));

			json_push_back(c, p);
		}

		json_push_back(n, json_new_i("RequestId", m_RequestId));
		json_push_back(n, json_new_b("Success", m_MyStatus.Succeeded()));
		json_push_back(n, json_new_i("ResultCode", m_MyStatus.GetResultCode()));
		json_push_back(n, json_new_i("Count", m_NumPofileIds));

		json_push_back(n, c);

		jc = json_write_formatted(n);

#if !__NO_OUTPUT
		size_t len = strlen(jc);
		for(size_t i = 0; i < len; i++)
		{
			printf("%c", jc[i]);
		}
#endif

		json_delete(n);

		bool succeeded = false;
		rgsc::Script::JsPlayerMultiplayerPresenceInfoResponse(&succeeded, jc);

		json_free_safe(jc);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

///////////////////////////////////////////////////////////////////////////////
//  ReadPlayerListCountsAsJsonBaseTask
///////////////////////////////////////////////////////////////////////////////

class ReadPlayerListCountsAsJsonBaseTask : public PlayerManagerTaskBase
{
public:
	RL_TASK_DECL(ReadPlayerListCountsAsJsonBaseTask);

	enum State
	{
		STATE_INVALID   = -1,
		STATE_READ_COUNTS,
		STATE_READING_COUNTS,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	ReadPlayerListCountsAsJsonBaseTask();

	bool Configure(RgscPlayerManager* ctx,
				   const int requestId);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

private:
	bool ReadCounts();
	bool ReadCountsFromCache();
	bool SendResults();

	// parameters and returns
	int m_RequestId;
	unsigned m_NumBlocked;
	unsigned m_NumFriends;
	unsigned m_NumInvitesReceived;
	unsigned m_NumInvitesSent;

	// internal state
	int m_State;
	netStatus m_MyStatus;
};

RGSC_HRESULT 
RgscPlayerManager::ReadPlayerListCountsAsJson(const char* json)
{
	int requestId = -1;

	JSONNODE *n = json_parse(json);

	if(!AssertVerify(n != NULL))
	{
		return false;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return false;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "RequestId") == 0)
		{
			requestId = json_as_int(*i);
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	RLPC_CREATETASK_NO_STATUS(ReadPlayerListCountsAsJsonBaseTask,
							  requestId);
}

ReadPlayerListCountsAsJsonBaseTask::ReadPlayerListCountsAsJsonBaseTask()
: m_State(STATE_INVALID)
{

}

bool 
ReadPlayerListCountsAsJsonBaseTask::Configure(RgscPlayerManager* /*ctx*/,
											  const int requestId)
{
	bool success = false;

	rtry
	{
		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal(), catchall, );

		m_RequestId = requestId;
		m_NumBlocked = 0;
		m_NumFriends = 0;
		m_NumInvitesReceived = 0;
		m_NumInvitesSent = 0;

		m_State = STATE_INVALID;

		success = true;
	}
	rcatchall
	{

	}
	
	return success;
}

void
ReadPlayerListCountsAsJsonBaseTask::Start()
{
	rlDebug2("Reading player list counts...");

	this->PlayerManagerTaskBase::Start();

	m_State = STATE_READ_COUNTS;
}

void
ReadPlayerListCountsAsJsonBaseTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("Reading player list counts %s (%ums)", FinishString(finishType), GetElapsedTime());

	this->PlayerManagerTaskBase::Finish(finishType, resultCode);
}

void
ReadPlayerListCountsAsJsonBaseTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
	this->PlayerManagerTaskBase::DoCancel();
}

void
ReadPlayerListCountsAsJsonBaseTask::Update(const unsigned timeStep)
{
	this->PlayerManagerTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
			// TODO: NS - need to set a flag on sign out/sign in that tells this to re-fetch from the web service.
#if 0 //__FINAL
			case STATE_READ_COUNTS:
			if(this->ReadCountsFromCache())
			{
				m_State = STATE_SUCCEEDED;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;
#else
		case STATE_READ_COUNTS:
			if(this->ReadCounts())
			{
				m_State = STATE_READING_COUNTS;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_READING_COUNTS:
			if(m_MyStatus.Succeeded())
			{
#if 0 // __DEV
				// TODO: NS - I still need to do some more work to keep the invites received counts up to date.
				unsigned numBlocked = 0;
				unsigned numFriends = 0;
				unsigned numInvitesReceived = 0;
				unsigned numInvitesSent = 0;
				m_Ctx->GetPlayerListCounts(numBlocked, numFriends, numInvitesReceived, numInvitesSent);

				Assertf(m_NumBlocked == numBlocked, "Cached block list count is out of sync.");
				Assertf(m_Ctx->GetBlockedPlayerIdCache()->GetCount() == (int)numBlocked, "Cached block list count is out of sync.");
				Assertf(m_NumFriends == numFriends, "Cached friend list count is out of sync.");

				rlFriend* friends = NULL;
				unsigned numCachedFriends = 0;
				rlPresence::GetFriendsArray(0, &friends, &numCachedFriends);
				Assertf(numCachedFriends == numFriends, "Cached friend list count is out of sync.");
				Assertf(m_NumInvitesReceived == numInvitesReceived, "Cached invites receieved list count is out of sync.");
				Assertf(m_NumInvitesSent == numInvitesSent, "Cached invites sent list count is out of sync.");
#endif

				m_State = STATE_SUCCEEDED;
			}
			else if(!m_MyStatus.Pending())
			{
				m_State = STATE_FAILED;
			}
			break;
#endif

		case STATE_SUCCEEDED:
			m_Ctx->SetPlayerListCounts(m_NumBlocked, m_NumFriends, m_NumInvitesReceived, m_NumInvitesSent);
			SendResults();
			this->Finish(FINISH_SUCCEEDED);
			break;

		case STATE_FAILED:
			m_NumBlocked = 0;
			m_NumFriends = 0;
			m_NumInvitesReceived = 0;
			m_NumInvitesSent = 0;
			SendResults();
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool 
ReadPlayerListCountsAsJsonBaseTask::ReadCountsFromCache()
{
	m_Ctx->GetPlayerListCounts(m_NumBlocked, m_NumFriends, m_NumInvitesReceived, m_NumInvitesSent);

	rlFriend* friends = NULL;
	unsigned numCachedFriends = 0;
	if(rlPresence::GetFriendsArray(0, &friends, &numCachedFriends) == false)
	{
		m_NumFriends = numCachedFriends;
	}

	RgscPlayerManager::BlockedPlayerIds* blockedPlayerIds = m_Ctx->GetBlockedPlayerIdCache();
	m_NumBlocked = blockedPlayerIds->GetCount();

	return true;
}

bool 
ReadPlayerListCountsAsJsonBaseTask::ReadCounts()
{
	return rlScFriends::CountAll(0,
								 (int*)&m_NumBlocked,
								 (int*)&m_NumFriends,
								 (int*)&m_NumInvitesReceived,
								 (int*)&m_NumInvitesSent,
								 &m_MyStatus);
}

bool 
ReadPlayerListCountsAsJsonBaseTask::SendResults()
{
	json_char *jc = "";
	bool success = false;

	rtry
	{
		JSONNODE* n = json_new(JSON_NODE);
		rverify(n, catchall, );

		unsigned numGameInvites = GetRgscConcreteInstance()->_GetPresenceManager()->GameNumGameInvites();

		json_push_back(n, json_new_i("NumBlocked", m_NumBlocked));
		json_push_back(n, json_new_i("NumFriends", m_NumFriends));
		json_push_back(n, json_new_i("NumInvitesReceieved", m_NumInvitesReceived));
		json_push_back(n, json_new_i("NumInvitesSent", m_NumInvitesSent));
		json_push_back(n, json_new_i("NumGameInvites", numGameInvites));

		jc = json_write_formatted(n);

#if !__NO_OUTPUT
		size_t len = strlen(jc);
		for(size_t i = 0; i < len; i++)
		{
			printf("%c", jc[i]);
		}
#endif

		json_delete(n);

		bool succeeded = false;
		rgsc::Script::JsPlayerListCountsResponse(&succeeded, jc);

		json_free_safe(jc);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}



class SendInviteTask : public SimpleOperationBaseTask<RgscPlayerManager>
{
protected:
	virtual bool PerformOperation()
	{
		return rlScFriends::SendInvite(0, m_RockstarId, &m_MyStatus);
	}

	virtual void ProcessResult(bool success)
	{
		if(success)
		{
			unsigned numBlocked = 0;
			unsigned numFriends = 0;
			unsigned numInvitesReceived = 0;
			unsigned numInvitesSent = 0;
			m_Ctx->GetPlayerListCounts(numBlocked, numFriends, numInvitesReceived, numInvitesSent);
			numInvitesSent++;
			m_Ctx->SetPlayerListCounts(numBlocked, numFriends, numInvitesReceived, numInvitesSent);
		}
	}
};

class CancelInviteTask : public SimpleOperationBaseTask<RgscPlayerManager>
{
protected:
	virtual bool PerformOperation()
	{
		return rlScFriends::CancelInvite(0, m_RockstarId, &m_MyStatus);
	}

	virtual void ProcessResult(bool success)
	{
		if(success)
		{
			unsigned numBlocked = 0;
			unsigned numFriends = 0;
			unsigned numInvitesReceived = 0;
			unsigned numInvitesSent = 0;
			m_Ctx->GetPlayerListCounts(numBlocked, numFriends, numInvitesReceived, numInvitesSent);
			if(numInvitesSent > 0)
			{
				numInvitesSent--;
				m_Ctx->SetPlayerListCounts(numBlocked, numFriends, numInvitesReceived, numInvitesSent);
			}
		}
	}
};

class DeclineInviteTask : public SimpleOperationBaseTask<RgscPlayerManager>
{
protected:
	virtual bool PerformOperation()
	{
		return rlScFriends::DeclineInvite(0, m_RockstarId, &m_MyStatus);
	}

	virtual void ProcessResult(bool success)
	{
		if(success)
		{
			unsigned numBlocked = 0;
			unsigned numFriends = 0;
			unsigned numInvitesReceived = 0;
			unsigned numInvitesSent = 0;
			m_Ctx->GetPlayerListCounts(numBlocked, numFriends, numInvitesReceived, numInvitesSent);
			if(numInvitesReceived > 0)
			{
				numInvitesReceived--;
				m_Ctx->SetPlayerListCounts(numBlocked, numFriends, numInvitesReceived, numInvitesSent);
			}
		}
	}
};

class AcceptInviteTask : public SimpleOperationBaseTask<RgscPlayerManager>
{
protected:
	virtual bool PerformOperation()
	{
		return rlScFriends::AcceptInvite(0, m_RockstarId, &m_MyStatus);
	}

	virtual void ProcessResult(bool success)
	{
		if(success)
		{
			unsigned numBlocked = 0;
			unsigned numFriends = 0;
			unsigned numInvitesReceived = 0;
			unsigned numInvitesSent = 0;
			m_Ctx->GetPlayerListCounts(numBlocked, numFriends, numInvitesReceived, numInvitesSent);
			if(numInvitesReceived > 0)
			{
				numInvitesReceived--;
				numFriends++;
				m_Ctx->SetPlayerListCounts(numBlocked, numFriends, numInvitesReceived, numInvitesSent);
			}
		}
	}
};

class DeleteFriendTask : public SimpleOperationBaseTask<RgscPlayerManager>
{
protected:
	virtual bool PerformOperation()
	{
		return rlScFriends::RemoveFriend(0, m_RockstarId, &m_MyStatus);
	}

	virtual void ProcessResult(bool success)
	{
		if(success)
		{
			unsigned numBlocked = 0;
			unsigned numFriends = 0;
			unsigned numInvitesReceived = 0;
			unsigned numInvitesSent = 0;
			m_Ctx->GetPlayerListCounts(numBlocked, numFriends, numInvitesReceived, numInvitesSent);
			if(numFriends > 0)
			{
				numFriends--;
				m_Ctx->SetPlayerListCounts(numBlocked, numFriends, numInvitesReceived, numInvitesSent);
			}
		}
	}
};

class BlockPlayerTask : public SimpleOperationBaseTask<RgscPlayerManager>
{
protected:
	virtual bool PerformOperation()
	{
		m_Ctx->AddPlayerToBlockList(m_RockstarId);
		return rlScFriends::BlockPlayer(0, m_RockstarId, &m_MyStatus);
	}
};

class UnblockPlayerTask : public SimpleOperationBaseTask<RgscPlayerManager>
{
protected:
	virtual bool PerformOperation()
	{
		m_Ctx->RemovePlayerFromBlockList(m_RockstarId);
		return rlScFriends::UnblockPlayer(0, m_RockstarId, &m_MyStatus);
	}
};

///////////////////////////////////////////////////////////////////////////////
//  SyncPlayersTask
///////////////////////////////////////////////////////////////////////////////

class SyncPlayersTask : public PlayerManagerTaskBase
{
public:
	RL_TASK_DECL(SyncPlayersTask);

	enum State
	{
		STATE_INVALID   = -1,
		STATE_WAIT_FOR_SERVICES,
		STATE_READ_FRIEND_COUNTS,
		STATE_READING_FRIEND_COUNTS,
		STATE_READ_BLOCKED_PLAYERS,
		STATE_READING_BLOCKED_PLAYERS,
		STATE_PROCESS_BLOCKED_PLAYER_RESULTS,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	SyncPlayersTask();

	bool Configure(RgscPlayerManager* ctx,
				   RockstarId profileId);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

private:
	bool ReadPlayerListCounts();
	bool ReadBlockedPlayers();
	bool ProcessBlockedResults();
	bool NeedToReadMoreBlockedPlayers();
	void SortBlockedPlayers();

	RockstarId m_ProfileId;

	u32 m_CurrentPageIndex;

	static const u32 PAGE_SIZE = 100;
	rlScPlayer m_RosResults[PAGE_SIZE];
	u32 m_RosResultCount;
	u32 m_RosResultTotalCount;

	unsigned m_NumBlocked;
	unsigned m_NumFriends;
	unsigned m_NumInvitesReceived;
	unsigned m_NumInvitesSent;

	int m_State;
	netStatus m_MyStatus;
};

bool 
RgscPlayerManager::SyncPlayers(RockstarId profileId,
						   netStatus* status)
{
	RLPC_CREATETASK(SyncPlayersTask,
					profileId,
					status);
}

SyncPlayersTask::SyncPlayersTask()
: m_State(STATE_INVALID)
{
	rlDebug2("sizeof(SyncPlayersTask): %d", sizeof(SyncPlayersTask));
}

bool 
SyncPlayersTask::Configure(RgscPlayerManager* /*ctx*/,
						   RockstarId profileId)
{
	bool success = false;

	rtry
	{
		rverify(GetRgscConcreteInstance()->IsUiDisabled() || GetRgscConcreteInstance()->_GetProfileManager()->IsSigningIn(), catchall, );

		m_CurrentPageIndex = 0;
		m_RosResultCount = 0;
		m_RosResultTotalCount = 0;

		m_Ctx->GetBlockedPlayerIdCache()->Reset();

		m_NumBlocked = 0;
		m_NumFriends = 0;
		m_NumInvitesReceived = 0;
		m_NumInvitesSent = 0;

		m_State = STATE_INVALID;

		success = true;
	}
	rcatchall
	{

	}
	
	return success;
}

void
SyncPlayersTask::Start()
{
	rlDebug2("Syncing players...");

	this->PlayerManagerTaskBase::Start();

	m_State = STATE_WAIT_FOR_SERVICES;
}

void
SyncPlayersTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("Syncing players %s (%ums)", FinishString(finishType), GetElapsedTime());

	this->PlayerManagerTaskBase::Finish(finishType, resultCode);
}

void
SyncPlayersTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
	this->PlayerManagerTaskBase::DoCancel();
}

void
SyncPlayersTask::Update(const unsigned timeStep)
{
	this->PlayerManagerTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_WAIT_FOR_SERVICES:
			// wait for rlPresence to finish reading the friend list
			if(rlPresence::IsInitialFriendSyncDone())
			{
				rlDebug2("Services finishing syncing, proceeding to read player counts.");
				m_State = STATE_READ_FRIEND_COUNTS;
			}
			break;

		case STATE_READ_FRIEND_COUNTS:
			if(this->ReadPlayerListCounts())
			{
				m_State = STATE_READING_FRIEND_COUNTS;
			}
			else
			{
				m_State = STATE_READ_BLOCKED_PLAYERS;
			}
			break;

		case STATE_READING_FRIEND_COUNTS:
			if(m_MyStatus.Succeeded())
			{
				m_Ctx->SetPlayerListCounts(m_NumBlocked, m_NumFriends, m_NumInvitesReceived, m_NumInvitesSent);
				m_State = STATE_READ_BLOCKED_PLAYERS;
			}
			else if(!m_MyStatus.Pending())
			{
				m_State = STATE_READ_BLOCKED_PLAYERS;
			}
			break;

		case STATE_READ_BLOCKED_PLAYERS:
			if(this->ReadBlockedPlayers())
			{
				m_State = STATE_READING_BLOCKED_PLAYERS;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_READING_BLOCKED_PLAYERS:
			if(m_MyStatus.Succeeded())
			{
				m_State = STATE_PROCESS_BLOCKED_PLAYER_RESULTS;
			}
			else if(!m_MyStatus.Pending())
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_PROCESS_BLOCKED_PLAYER_RESULTS:
			if(this->ProcessBlockedResults())
			{
				if(NeedToReadMoreBlockedPlayers())
				{
					m_CurrentPageIndex++;
					m_State = STATE_READ_BLOCKED_PLAYERS;
				}
				else
				{
					m_CurrentPageIndex = 0;
					SortBlockedPlayers();
					m_State = STATE_SUCCEEDED;
				}
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_SUCCEEDED:
			this->Finish(FINISH_SUCCEEDED);
			break;

		case STATE_FAILED:
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			break;
		}
	}
	while((m_State != STATE_WAIT_FOR_SERVICES) && !m_MyStatus.Pending() && this->IsActive());
}

bool 
SyncPlayersTask::ReadPlayerListCounts()
{
	return rlScFriends::CountAll(0,
								 (int*)&m_NumBlocked,
								 (int*)&m_NumFriends,
								 (int*)&m_NumInvitesReceived,
								 (int*)&m_NumInvitesSent,
								 &m_MyStatus);
}

bool 
SyncPlayersTask::ReadBlockedPlayers()
{
	m_RosResultCount = 0;
	m_RosResultTotalCount = 0;

	return rlScFriends::GetBlocked(0,
								   m_CurrentPageIndex,
								   PAGE_SIZE,
								   m_RosResults,
								   sizeof(m_RosResults[0]),
								   &m_RosResultCount,
								   &m_RosResultTotalCount,
								   &m_MyStatus);
}

bool 
SyncPlayersTask::ProcessBlockedResults()
{
	bool success = false;

	rtry
	{
		rcheck(m_RosResultCount, noresults, );

		RgscPlayerManager::BlockedPlayerIds* blockedPlayerIds = m_Ctx->GetBlockedPlayerIdCache();
		rverify(blockedPlayerIds, catchall, );

		for(u32 i = 0; i < m_RosResultCount; i++)
		{
			if(blockedPlayerIds->GetCount() < blockedPlayerIds->GetMaxCount())
			{
				blockedPlayerIds->Append() = m_RosResults[i].m_RockstarId;
			}
		}

		success = true;
	}
	rcatch(noresults)
	{
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool 
SyncPlayersTask::NeedToReadMoreBlockedPlayers()
{
	// if the last page wasn't full, then there are no more players to grab
	return (m_Ctx->GetBlockedPlayerIdCache()->GetCount() < (int)m_RosResultTotalCount) && (m_RosResultCount == PAGE_SIZE);
}

void 
SyncPlayersTask::SortBlockedPlayers()
{
	struct SortPredicate
	{
		static int Sort(const RockstarId* a, const RockstarId* b)
		{
			return *a < *b ? -1 : 1;
		}
	};

	m_Ctx->GetBlockedPlayerIdCache()->QSort(0, -1, SortPredicate::Sort);
}

class GetFacebookAppInfoAndStatusTask : public PlayerManagerTaskBase
{
public:
	RL_TASK_DECL(GetFacebookAppInfoAndStatusTask);

	enum State
	{
		STATE_INVALID,
		STATE_GET_APP_INFO,
		STATE_GETTING_APP_INFO,
		STATE_GET_LINK_STATUS,
		STATE_GETTING_LINK_STATUS,
		STATE_FAILED,
		STATE_SUCCEEDED
	};

	GetFacebookAppInfoAndStatusTask::GetFacebookAppInfoAndStatusTask()
		: m_State(STATE_INVALID)
		, m_FbAppId(0)
		, m_RockstarId(InvalidRockstarId)
	{
		m_AppNamespace[0] = '\0';
		m_ScLinkUrl[0] = '\0';
	}

	~GetFacebookAppInfoAndStatusTask() {}

	bool Configure(RgscPlayerManager* /*ctx*/, int requestId, RockstarId rockstarId)
	{
		bool success = false;

		rtry
		{
			rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal(), catchall, );

			m_RequestId = requestId;
			m_RockstarId = rockstarId;

			// clear token
			rlFacebook::ClearAppPermissions();

			m_State = STATE_INVALID;

			success = true;
		}
		rcatchall
		{

		}

		return success;
	}

	virtual void Start()
	{
		rlDebug2("Facebook App Info and Status...");
		this->PlayerManagerTaskBase::Start();
		m_State = STATE_GET_APP_INFO;
	}

	virtual void Finish(const FinishType finishType, const int resultCode = 0)
	{
		rlDebug2("Facebook App Info and Status in %s", FinishString(finishType));
		this->PlayerManagerTaskBase::Finish(finishType, resultCode);
	}

	virtual bool IsCancelable() const {return true;}

	virtual void DoCancel()
	{
		RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
	}

	virtual void Update(const unsigned timeStep)
	{
		this->PlayerManagerTaskBase::Update(timeStep);

		if(this->WasCanceled())
		{
			//Wait for dependencies to finish
			if(!m_MyStatus.Pending())
			{
				this->Finish(FINISH_CANCELED);
			}

			return;
		}

		do
		{
			switch(m_State)
			{
			case STATE_GET_APP_INFO:
				if(GetAppInfo())
				{
					m_State = STATE_GETTING_APP_INFO;
				}
				else
				{
					m_State = STATE_FAILED;
				}
				break;
			case STATE_GETTING_APP_INFO:
				if(m_MyStatus.Succeeded())
				{
					m_State = STATE_GET_LINK_STATUS;
				}
				else if(!m_MyStatus.Pending())
				{
					m_State = STATE_FAILED;
				}
				break;
			case STATE_GET_LINK_STATUS:
				if(GetLinkStatus())
				{
					m_State = STATE_GETTING_LINK_STATUS;
				}
				else
				{
					m_State = STATE_FAILED;
				}
				break;
			case STATE_GETTING_LINK_STATUS:
				if(m_MyStatus.Succeeded())
				{
					m_State = STATE_SUCCEEDED;
				}
				else if(!m_MyStatus.Pending())
				{
					m_State = STATE_FAILED;
				}
				break;
			case STATE_SUCCEEDED:
				{
					SendResults(true);
					this->Finish(FINISH_SUCCEEDED);
				}
				break;
			case STATE_FAILED:
				{
					SendResults(false);
					this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
				}
				break;
			}
		}
		while(!m_MyStatus.Pending() && this->IsActive());
	}

	bool GetAppInfo()
	{
		return rlFacebook::GetAppInfo(0, &m_FbAppId, 
			m_DesiredPermissions, RL_FACEBOOK_PERMISSIONS_LENGTH_MAX, 
			m_AppNamespace, RL_FACEBOOK_APP_NAMESPACE_LENGTH_MAX, 
			m_ScLinkUrl, RL_FACEBOOK_LINK_URL_LENGTH_MAX,
			&m_MyStatus);
	}

	bool GetLinkStatus()
	{
		return rlFacebook::HaveAppPermissions(0, &m_MyStatus);
	}

	bool SendResults(bool operationSucceeded)
	{
		json_char *jc = "";
		bool success = false;

		rtry
		{
			JSONNODE* n = json_new(JSON_NODE);
			rverify(n, catchall, );

			json_push_back(n, json_new_i("RequestId", m_RequestId));
			json_push_back(n, json_new_b("Success", operationSucceeded));

			char szRockstarId[64];
			formatf(szRockstarId, "%"I64FMT"d", m_RockstarId);
			json_push_back(n, json_new_a("RockstarId", szRockstarId));

			char szAppId[64];
			formatf(szAppId, "%"I64FMT"u", m_FbAppId);
			json_push_back(n, json_new_a("FbAppId", szAppId));

			json_push_back(n, json_new_a("Permissions", m_DesiredPermissions));
			json_push_back(n, json_new_b("IsLinked", rlFacebook::HasValidToken(0)));

			// construct link url
			const char * ticket = rlRos::GetCredentials(0).GetTicket();
			char ticketBuf[RLROS_MAX_TICKET_SIZE] = {0};
			unsigned inLen = RLROS_MAX_TICKET_SIZE, dstLen = 0;
			netHttpRequest::UrlEncode(&ticketBuf[0], &inLen, ticket, (int)strlen(ticket), &dstLen, NULL);
			safecatf(m_ScLinkUrl, "?userdata=%s", ticketBuf);

			json_push_back(n, json_new_a("LinkUrl", m_ScLinkUrl));

			jc = json_write_formatted(n);
			rverify(jc, catchall, );

			json_delete(n);

			bool succeeded = false;
			rgsc::Script::JsFacebookLinkInfoResult(&succeeded, jc);

			json_free_safe(jc);

			success = true;
		}
		rcatchall
		{

		}

		return success;
	}

	char m_AppNamespace[RL_FACEBOOK_APP_NAMESPACE_LENGTH_MAX];
	char m_DesiredPermissions[RL_FACEBOOK_PERMISSIONS_LENGTH_MAX];
	char m_ScLinkUrl[RL_FACEBOOK_LINK_URL_LENGTH_MAX];
	u64 m_FbAppId;

	int m_RequestId;
	RockstarId m_RockstarId;

	int m_State;
	netStatus m_MyStatus;
};

RGSC_HRESULT 
RgscPlayerManager::ShowFriendRequestUi(const RockstarId rockstarId)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(rockstarId != InvalidRockstarId, catchall, hr = RGSC_INVALIDARG);

		JSONNODE *n = json_new(JSON_NODE);

		json_set_name(n, "Data");

		char szRockstarId[64];
		formatf(szRockstarId, "%"I64FMT"d", rockstarId);
		json_push_back(n, json_new_a("RockstarId", szRockstarId));

		rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(true,
																				 RgscUi::UI_STATE_FRIENDS,
																				 RgscUi::UI_SUBSTATE_FRIEND_REQUEST,
																				 n),
																				 catchall,
																				 hr = RGSC_FAIL);

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT
RgscPlayerManager::ShowFriendSearchUi()
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		JSONNODE *n = json_new(JSON_NODE);

		json_set_name(n, "Data");

		rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(true,
			RgscUi::UI_STATE_FRIENDS,
			RgscUi::UI_SUBSTATE_FRIEND_SEARCH,
			n),
			catchall,
			hr = RGSC_FAIL);

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT 
RgscPlayerManager::ShowPlayerProfileUi(const RockstarId rockstarId)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(rockstarId != InvalidRockstarId, catchall, hr = RGSC_INVALIDARG);

		JSONNODE *n = json_new(JSON_NODE);

		json_set_name(n, "Data");

		char szRockstarId[64];
		formatf(szRockstarId, "%"I64FMT"d", rockstarId);
		json_push_back(n, json_new_a("RockstarId", szRockstarId));

		rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(true,
																				 RgscUi::UI_STATE_PLAYERS,
																				 RgscUi::UI_SUBSTATE_PROFILE,
																				 n),
																				 catchall,
																				 hr = RGSC_FAIL);

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;

}

RockstarId 
RgscPlayerManager::GetRockstarIdFromJson(const char* json)
{
	RockstarId rockstarId = InvalidRockstarId;

	JSONNODE *n = json_parse(json);

	if(!AssertVerify(n != NULL))
	{
		return rockstarId;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return rockstarId;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "RockstarId") == 0)
		{
			json_char *node_value = json_as_string_or_null(*i);
			sscanf_s(node_value, "%I64d", &rockstarId);
			json_free_safe(node_value);
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	return rockstarId;
}

void  
RgscPlayerManager::GetRequestIdAndRockstarIdFromJson(const char* json,
												 int &requestId,
												 RockstarId &rockstarId)
{
	requestId = -1;
	rockstarId = InvalidRockstarId;

	JSONNODE *n = json_parse(json);

	if(!AssertVerify(n != NULL))
	{
		return;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "RequestId") == 0)
		{
			requestId = json_as_int(*i);
		}
		else if(_stricmp(node_name, "RockstarId") == 0)
		{
			json_char *node_value = json_as_string_or_null(*i);
			sscanf_s(node_value, "%I64d", &rockstarId);
			json_free_safe(node_value);
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);
}

RGSC_HRESULT 
RgscPlayerManager::SendInvite(const RockstarId rockstarId,
						  IAsyncStatus* status)
{
	RLPC_CREATETASK_IASYNC(SendInviteTask,
						   rockstarId);
}

// these may be exposed to the game in the future.
#if 0
RGSC_HRESULT 
PlayerManager::CancelInvite(const RockstarId rockstarId,
							IAsyncStatus* status)
{
	RLPC_CREATETASK_IASYNC(CancelInviteTask,
						   rockstarId);
}

RGSC_HRESULT 
PlayerManager::DeclineInvite(const RockstarId rockstarId,
							 IAsyncStatus* status)
{
	RLPC_CREATETASK_IASYNC(DeclineInviteTask,
						   rockstarId);
}

RGSC_HRESULT 
PlayerManager::AcceptInvite(const RockstarId rockstarId,
							IAsyncStatus* status)
{
	RLPC_CREATETASK_IASYNC(AcceptInviteTask,
						   rockstarId);
}

RGSC_HRESULT 
PlayerManager::DeleteFriend(const RockstarId rockstarId,
							IAsyncStatus* status)
{
	RLPC_CREATETASK_IASYNC(DeleteFriendTask,
						   rockstarId);
}

RGSC_HRESULT 
PlayerManager::BlockPlayer(const RockstarId rockstarId,
						   IAsyncStatus* status)
{
	RLPC_CREATETASK_IASYNC(BlockPlayerTask,
						   rockstarId);
}

RGSC_HRESULT 
PlayerManager::UnblockPlayer(const RockstarId rockstarId,
							 IAsyncStatus* status)
{
	RLPC_CREATETASK_IASYNC(UnblockPlayerTask,
						   rockstarId);
}
#endif

bool 
RgscPlayerManager::IsFriend(const RockstarId rockstarId)
{
	// the friends list is now cached in rlPresence
	rlGamerHandle gh;
	gh.ResetSc(rockstarId);
	return rlFriendsManager::IsFriendsWith(0, gh);
}

bool 
RgscPlayerManager::IsFriend(const char* json)
{
	return IsFriend(GetRockstarIdFromJson(json));
}

RGSC_HRESULT
RgscPlayerManager::GetFacebookLinkInfoAsJson(const char* json)
{
	int requestId = -1;
	RockstarId rockstarId = InvalidRockstarId;
	GetRequestIdAndRockstarIdFromJson(json, requestId, rockstarId);

	RLPC_CREATETASK_NO_STATUS(GetFacebookAppInfoAndStatusTask, requestId, rockstarId);
}

bool 
RgscPlayerManager::IsBlocked(const RockstarId rockstarId)
{
	return m_BlockedPlayerIds.BinarySearch(rockstarId) >= 0;
}

bool 
RgscPlayerManager::IsBlocked(const char* json)
{
	return IsBlocked(GetRockstarIdFromJson(json));
}

unsigned
RgscPlayerManager::GetTotalNumFriends()
{
	return rlFriendsManager::GetTotalNumFriends(0);
}

bool 
RgscPlayerManager::RequestFriendSync()
{
	return rlFriendsManager::RequestFriendSync();
}

bool
RgscPlayerManager::IsSyncingFriends()
{
	return rlFriendsManager::IsSyncingFriends();
}

#define JAVASCRIPT_PLAYER_OP_WRAPPER(name)											\
																					\
RGSC_HRESULT																		\
RgscPlayerManager::##name##(const char* json)										\
{																					\
	struct Callback																	\
	{																				\
		static void Func(const bool success, const RockstarId rockstarId, const int resultCode, int requestId)	\
		{																			\
			rtry																	\
			{																		\
				JSONNODE *n = json_new(JSON_NODE);									\
				rverify(n, catchall, rlError("json_new failed to create node,"		\
											 "possibly out of memory."));			\
																					\
				json_push_back(n, json_new_i("RequestId", requestId));				\
				json_push_back(n, json_new_b("Success", success));					\
				json_push_back(n, json_new_i("ResultCode", resultCode));			\
				char szRockstarId[64];												\
				formatf(szRockstarId, "%"I64FMT"d", rockstarId);					\
				json_push_back(n, json_new_a("RockstarId", szRockstarId));			\
				json_char *jc = json_write_formatted(n);							\
				rverify(jc, catchall, );											\
																					\
				bool succeeded = false;												\
				rgsc::Script::JsFriend##name##Result(&succeeded, jc);				\
																					\
				json_free_safe(jc);													\
				json_delete(n);														\
			}																		\
			rcatchall																\
			{																		\
																					\
			}																		\
		}																			\
	};																				\
																					\
	int requestId = -1;																\
	RockstarId rockstarId = InvalidRockstarId;										\
	GetRequestIdAndRockstarIdFromJson(json, requestId, rockstarId);					\
	RLPC_CREATETASK_NO_STATUS(##name##Task,											\
							  rockstarId,											\
							  Callback::Func,										\
							  requestId);											\
}																				
// JAVASCRIPT_PLAYER_OP_WRAPPER

JAVASCRIPT_PLAYER_OP_WRAPPER(SendInvite)
JAVASCRIPT_PLAYER_OP_WRAPPER(CancelInvite)
JAVASCRIPT_PLAYER_OP_WRAPPER(DeclineInvite)
JAVASCRIPT_PLAYER_OP_WRAPPER(AcceptInvite)
JAVASCRIPT_PLAYER_OP_WRAPPER(DeleteFriend)
JAVASCRIPT_PLAYER_OP_WRAPPER(BlockPlayer)
JAVASCRIPT_PLAYER_OP_WRAPPER(UnblockPlayer)

} //namespace rgsc

#endif // RSG_PC
