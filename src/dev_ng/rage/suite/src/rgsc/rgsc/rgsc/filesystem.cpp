#include "filesystem.h"

#if RSG_PC

#include "rgsc.h"
#include "diag/diagerrorcodes.h"
#include "diag/seh.h"
#include "rgsc_ui/stringutil.h"
#include "rgsc/diag/output.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <shlobj.h>
#pragma warning(pop)

// VS2013's CRT (used by the CEF) only checks the existence of FMA3 instruction support at the
// hardware level, not the enabled-ness of them at the OS level (this is fixed in VS2015).
// We use the 32-bit version of the subprocess and CEF to avoid using that path and
// hitting illegal instructions when running on CPUs that support FMA3, but
// OSs that don't. Note, this won't be needed when Chrome and CEF disables FMA3
// instruction support when running on an OS that doesn't support it.
#define WORKAROUD_VS2013_CRT_BUG 1

#if WORKAROUD_VS2013_CRT_BUG
#include <stdint.h>
#include <intrin.h>
#endif

#define SUBPROCESS_EXE_NAME "SocialClubHelper.exe"

namespace rgsc
{

const char* RgscFileSystem::UI_PATH = "UI\\";

extern sysMemAllocator* g_RgscAllocator;

RGSC_HRESULT RgscFileSystem::QueryInterface(RGSC_REFIID riid, LPVOID* ppvObj)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObj == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IFileSystem*>(this);
	}
	else if(riid == IID_IFileSystemV1)
	{
		pUnknown = static_cast<IFileSystemV1*>(this);
	}
	else if(riid == IID_IFileSystemV2)
	{
		pUnknown = static_cast<IFileSystemV2*>(this);
	}
	else if(riid == IID_IFileSystemV3)
	{
		pUnknown = static_cast<IFileSystemV3*>(this);
	}
	else if(riid == IID_IFileSystemV4)
	{
		pUnknown = static_cast<IFileSystemV4*>(this);
	}
	else if(riid == IID_IFileSystemV5)
	{
		pUnknown = static_cast<IFileSystemV5*>(this);
	}

	*ppvObj = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

RgscFileSystem::RgscFileSystem()
{
	memset(m_RootDllDirectory, 0, sizeof(m_RootDllDirectory));
	memset(m_RootDataDirectory, 0, sizeof(m_RootDataDirectory));
	memset(m_RootDataDirectoryA, 0, sizeof(m_RootDataDirectoryA));

	//! Enables or Disables UTF8 file paths in the File System
	//	By default, this is enabled on x64 builds.
	//	On x86 builds, this feature is disabled for legacy purposes with L.A. Noire and Max Payne 3 (ANSI paths only).
	//	This can be enabled using EnableUtf8FilePaths
	m_bUtf8PathsEnabled = RSG_CPU_X64 ? true : false;
}

RgscFileSystem::~RgscFileSystem()
{

}

// IMPORTANT: Max Payne 3 and L.A. Noire require ANSI encoded paths to be returned, but any time the DLL calls these functions (x86 or x64), they need to return UTF8, all games after MP3/LAN need to return UTF8.
bool 
RgscFileSystem::GetKnownFolderPath(int folderId, char (&path)[RGSC_MAX_PATH], bool utf8) const
{
	bool success = false;
	path[0] = '\0';

	rtry
	{

#if 0
		if(folderId == 5)
		{
			wchar_t pathW[RGSC_MAX_PATH] = {0};

			safecpy(pathW, L"C:\\\u3142\\Documents");

			RgscUtf8String utf8 = rgsc::WideToUTF8(WideString::point_to(pathW)); 
			safecpy(path, utf8.data());
			stringUtil_free(utf8);

			Displayf("FileSystem::GetKnownFolderPath(%d): UTF8: %s", folderId, path);

			wchar_t pathTempW[RGSC_MAX_PATH];
			MultiByteToWideChar(CP_UTF8 , NULL , path, -1, pathTempW, RGSC_MAX_PATH);
			Displayf("FileSystem::GetKnownFolderPath(%d): UTF8 to Wide: %ls", folderId, pathTempW);
			OutputDebugString(pathTempW);

			return true;	
		}
#endif

		if(utf8)
		{
			// use the XP compatible function here. SHGetKnownFolderPath is Vista+ only.
			wchar_t pathW[RGSC_MAX_PATH] = {0};
			rverify(SHGetFolderPath(NULL, folderId, NULL, SHGFP_TYPE_CURRENT, pathW) == RGSC_OK, catchall, );

			// TODO: NS - technically the UTF8 string could be longer than RGSC_MAX_PATH if the original path was very long as well
			rverify(WideCharToMultiByte(CP_UTF8, 0, pathW, -1, path, COUNTOF(path), 0, 0) != 0, catchall, );
		}
		else
		{
			// use the XP compatible function here. SHGetKnownFolderPath is Vista+ only.
			rverify(SHGetFolderPathA(NULL, folderId, NULL, SHGFP_TYPE_CURRENT, path) == RGSC_OK, catchall, );
		}

		Displayf("FileSystem::GetKnownFolderPath(folderId=%d, utf8=%s): %s", folderId, utf8 ? "true" : "false", path);
		
		success = true;
	}
	rcatchall
	{
#if !__NO_OUTPUT
		char buf[1024] = {0};
		DWORD errorCode = GetLastError();
		rlError("FileSystem::GetKnownFolderPath() failed. Error code %u : %s", errorCode, diagErrorCodes::Win32ErrorToString(errorCode, buf, NELEM(buf)));
#endif

	}

	return success;
}

bool
RgscFileSystem::CreateFolder(const char* folderPath) const
{
	wchar_t pathW[RGSC_MAX_PATH];

	int result = MultiByteToWideChar(CP_UTF8 , NULL , folderPath, -1, pathW, RGSC_MAX_PATH);
	if(result != 0)
	{
		result = SHCreateDirectoryEx(NULL, pathW, NULL);
		return (result == ERROR_SUCCESS) ||
			   (result == ERROR_ALREADY_EXISTS) || 
			   (result == ERROR_FILE_EXISTS);
	}

	return false;
}

bool 
RgscFileSystem::Init()
{
	bool success = false;

	rtry
	{
		ITitleId::Platform platform = GetRgscConcreteInstance()->GetTitleId()->GetPlatform();
		ITitleId::RosEnvironment env = GetRgscConcreteInstance()->GetTitleId()->GetRosEnvironment();
		//@@: location FILESYSTEM_INIT_SET_ROS_ENV
		bool production = (env == ITitleId::RLROS_ENV_PROD) && !__DEV;

		char path[RGSC_MAX_PATH] = {0};

		// we want to be able to run from any directory during development
		if((platform == ITitleId::PLATFORM_ONLIVE) ||
			(production == false))
		{
			// we can load the DLL from any directory.
			HMODULE hModule;
			
			rverify(GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, (LPCTSTR)&g_RgscAllocator, &hModule) != 0, catchall, );

			wchar_t pathW[RGSC_MAX_PATH] = {0};
			rverify(GetModuleFileName(hModule, pathW, COUNTOF(pathW)) != 0, catchall, );
			rverify(WideCharToMultiByte(CP_UTF8, 0, pathW, -1, path, COUNTOF(path), 0, 0) != 0, catchall, );

			char* lastSlash = strrchr(path, '\\');
			if(lastSlash == NULL)
			{
				lastSlash = strrchr(path, '/');
			}
			rverify(lastSlash != NULL, catchall, );
			lastSlash[1] = '\0';
			safecpy(m_RootDllDirectory, path);
		}
		else
		{
			rverify(GetKnownFolderPath(CSIDL_PROGRAM_FILES, path, true), catchall, );
#if __DEV
			safecat(path, "\\Rockstar Games\\Social Club Debug\\");
#else
			safecat(path, "\\Rockstar Games\\Social Club\\");
#endif
			safecpy(m_RootDllDirectory, path);
		}

		RgscDisplayUtf8("Social Club Directory: %s", m_RootDllDirectory);

		const char* rootDataDir = GetRgscConcreteInstance()->GetTitleId()->GetRootDataDirectory();
		if(((platform == ITitleId::PLATFORM_ONLIVE) || (production == false)) && (rootDataDir != NULL) && (rootDataDir[0] != '\0'))
		{
			safecpy(m_RootDataDirectory, rootDataDir);
			safecat(m_RootDataDirectory, "\\Rockstar Games\\");
			safecpy(m_RootDataDirectoryA, m_RootDataDirectory);
		}
		else
		{
			// Get the Root Data Directory (UTF8)
			rverify(GetKnownFolderPath(CSIDL_MYDOCUMENTS, m_RootDataDirectory, true), catchall, );			
			safecat(m_RootDataDirectory, "\\Rockstar Games\\");

			// Get the Root Data Directory (ANSI)
			rverify(GetKnownFolderPath(CSIDL_MYDOCUMENTS, m_RootDataDirectoryA, false), catchall, );			
			safecat(m_RootDataDirectoryA, "\\Rockstar Games\\");
		}

		RgscDisplayUtf8("Root Data Directory: %s", m_RootDataDirectory);

		success = true;
	}
	rcatchall
	{

	}
	
	return success;
}

void 
RgscFileSystem::Shutdown()
{

}

void 
RgscFileSystem::Update()
{

}

bool 
RgscFileSystem::GetRockstarGamesDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory, bool utf8) const
{
	bool success = false;
	path[0] = '\0';

	if(m_RootDataDirectory[0] != '\0')
	{
		safecpy(path, m_RootDataDirectory);

		if(bCreateDirectory)
		{
			success = CreateFolder(path);
		}
		else
		{
			success = true;
		}
	}

	// If we don't support UTF8 file paths, get the ANSI file path instead and return it.
	//	We still want the above logic to create the folder locally on disk.
	if(!utf8 && !m_bUtf8PathsEnabled)
	{
		if(m_RootDataDirectoryA[0] != '\0')
		{
			safecpy(path, m_RootDataDirectoryA);
		}
	}

	return success;
}

bool 
RgscFileSystem::GetPlatformDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const
{
	bool success = false;
	path[0] = '\0';

	if(this->GetRockstarGamesDirectory(path, bCreateDirectory, true))
	{
		safecat(path, "Social Club\\");

		if(bCreateDirectory)
		{
			success = CreateFolder(path);
		}
		else
		{
			success = true;
		}
	}
	return success;
}

bool
RgscFileSystem::GetPlatformTitleDirectoryName(char (&path)[RGSC_MAX_PATH]) const
{
	path[0] = '\0';
	const char* gameName = GetRgscConcreteInstance()->GetTitleId()->GetTitleDirectoryName();
	safecpy(path, gameName);
	return true;
}

u32 
RgscFileSystem::GetPlatformProfileDirectoryId(const UniqueKey& key) const
{
	if(key.IsSet())
	{
		// the directory id is the hash of the key 
		u8 digest[Sha1::SHA1_DIGEST_LENGTH];
		Sha1 sha1;
		sha1.Update(key.GetKey(), UniqueKey::SIZE_OF_KEY_IN_BYTES);
		sha1.Final(digest);

		// now hash the hash into a 32-bit value to make it shorter
		return atDataHash((const char*)digest, sizeof(digest));
	}

	return 0;
}

bool
RgscFileSystem::GetPlatformProfileDirectoryName(char (&path)[RGSC_MAX_PATH], const UniqueKey& key) const
{
	path[0] = '\0';
	u32 folderId = GetPlatformProfileDirectoryId(key);
	formatf(path, "%08X", folderId);
	return true;
}

bool 
RgscFileSystem::GetPlatformTitlesDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const
{
	bool success = false;
	path[0] = '\0';

	if(this->GetPlatformDirectory(path, bCreateDirectory))
	{
		safecat(path, "Titles\\");

		if(bCreateDirectory)
		{
			success = CreateFolder(path);
		}
		else
		{
			success = true;
		}
	}
	return success;
}

bool 
RgscFileSystem::GetPlatformTitleDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const
{
	bool success = false;
	path[0] = '\0';

	if(this->GetPlatformTitlesDirectory(path, bCreateDirectory))
	{
		char folderName[RGSC_MAX_PATH];
		GetPlatformTitleDirectoryName(folderName);
		safecat(path, folderName);
		safecat(path, "\\");

		if(bCreateDirectory)
		{
			success = CreateFolder(path);
		}
		else
		{
			success = true;
		}
	}
	return success;
}

bool 
RgscFileSystem::GetPlatformProfilesDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const
{
    bool success = false;
    path[0] = '\0';

    if(GetPlatformDirectory(path, bCreateDirectory))
    {
        safecat(path, "Profiles");
        safecat(path, "\\");

        if(bCreateDirectory)
        {
			success = CreateFolder(path);
        }
        else
        {
            success = true;
        }
    }
    return success;
}

bool 
RgscFileSystem::GetPlatformProfileDirectory(char (&path)[RGSC_MAX_PATH], unsigned folderId, bool bCreateDirectory) const
{
    bool success = false;
	path[0] = '\0';

    if(GetPlatformProfilesDirectory(path, bCreateDirectory))
    {
        char szFolderId[20] = {0};
        formatf(szFolderId, "%08X", folderId);
        safecat(path, szFolderId);
        safecat(path, "\\");

        if(bCreateDirectory)
        {
			success = CreateFolder(path);
        }
        else
        {
            success = true;
        }
    }

    return success;
}

bool 
RgscFileSystem::GetPlatformProfileTitleDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const
{
	bool success = false;
	path[0] = '\0';

	// be careful - don't call profileManager->IsSignedInInternal() here, since this function 
	// can be called *while* signing in. In which case IsSignedInInternal() returns false.
	RgscProfileManager* profileManager = GetRgscConcreteInstance()->_GetProfileManager();
	const RgscProfile &profile = profileManager->GetSignedInProfile();

	if(profile.IsValid())
	{
		unsigned folderId = profile.GetFolderId();

		if(GetPlatformProfileDirectory(path, folderId, bCreateDirectory))
		{
			safecat(path, "Titles\\");

			char folderName[RGSC_MAX_PATH];
			GetPlatformTitleDirectoryName(folderName);
			safecat(path, folderName);
			safecat(path, "\\");

			if(bCreateDirectory)
			{
				success = CreateFolder(path);
			}
			else
			{
				success = true;
			}
		}
	}

	return success;
}

bool 
RgscFileSystem::GetPlatformCacheDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const
{
	bool success = false;
	path[0] = '\0';

	if(this->GetPlatformDirectory(path, bCreateDirectory))
	{
		safecat(path, "Cache\\");

		if(bCreateDirectory)
		{
			success = CreateFolder(path);
		}
		else
		{
			success = true;
		}
	}
	return success;
}

bool 
RgscFileSystem::GetBrowserCacheDirectory(char (&path)[RGSC_MAX_PATH], bool launcher, bool bCreateDirectory) const
{
	bool success = false;
	path[0] = '\0';

	if(GetPlatformDirectory(path, true))
	{
		if(launcher)
		{
			safecat(path, "Launcher\\Renderer\\");
		}
		else
		{
			safecat(path, "Renderer\\");
		}

		if(bCreateDirectory)
		{
			success = CreateFolder(path);
		}
		else
		{
			success = true;
		}
	}
	return success;
}

bool 
RgscFileSystem::GetPlatformTempDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const
{
	bool success = false;
	path[0] = '\0';

	if(this->GetPlatformDirectory(path, bCreateDirectory))
	{
		safecat(path, "Temp\\");

		if(bCreateDirectory)
		{
			success = CreateFolder(path);
		}
		else
		{
			success = true;
		}
	}
	return success;
}

RGSC_HRESULT 
RgscFileSystem::GetTitleDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const
{
	bool success = false;
	path[0] = '\0';

	//@@: location FILESTYSTEM_GETTITLEDIRECTORY_GET_ROCKSTAR_DIRECTORY
	if(this->GetRockstarGamesDirectory(path, bCreateDirectory, true))
	{
		safecatf(path, "%s\\", GetRgscConcreteInstance()->GetTitleId()->GetTitleDirectoryName());

		if(bCreateDirectory)
		{
			success = CreateFolder(path);
		}
		else
		{
			success = true;
		}

		// If we don't support UTF8 file paths, get the ANSI file path instead and return it.
		//	We still want the above logic to create the folder locally on disk.
		if(success && !m_bUtf8PathsEnabled)
		{
			if(this->GetRockstarGamesDirectory(path, false, false))
			{
				safecatf(path, "%s\\", GetRgscConcreteInstance()->GetTitleId()->GetTitleDirectoryName());
			}
		}
	}

	return success ? RGSC_OK : RGSC_FAIL;
}

RGSC_HRESULT 
RgscFileSystem::GetTitleProfileDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const
{
    return GetProfileDirectoryForTitle(path, GetRgscConcreteInstance()->GetTitleId()->GetTitleDirectoryName(), bCreateDirectory);
}

RGSC_HRESULT RgscFileSystem::GetProfileDirectoryId(char (&path)[RGSC_MAX_PATH]) const
{
	bool success = false;
	path[0] = '\0';

	// be careful - don't call profileManager->IsSignedInInternal() here, since this function 
	// can be called *while* signing in. In which case IsSignedInInternal() returns false.
	RgscProfileManager* profileManager = GetRgscConcreteInstance()->_GetProfileManager();
	const RgscProfile &profile = profileManager->GetSignedInProfile();

	if(profile.IsValid())
	{
		unsigned folderId = profile.GetFolderId();

		char szFolderId[20] = {0};
		formatf(szFolderId, "%08X", folderId);
		safecat(path, szFolderId);

		success = true;
	}

	return success ? RGSC_OK : RGSC_FAIL;
}

RGSC_HRESULT
RgscFileSystem::GetRootDataDirectory(char (&path)[RGSC_MAX_PATH]) const
{
	bool utf8 = RSG_CPU_X64 ? true : false;

	bool success = GetRockstarGamesDirectory(path, false, utf8);
	return success ? RGSC_OK : RGSC_FAIL;
}

RGSC_HRESULT
RgscFileSystem::GetProfileDirectoryForTitle(char (&path)[RGSC_MAX_PATH], const char* titleDirectoryName, bool bCreateDirectory) const
{
	bool success = false;
	path[0] = '\0';

	// be careful - don't call profileManager->IsSignedInInternal() here, since this function 
	// can be called *while* signing in. In which case IsSignedInInternal() returns false.
	RgscProfileManager* profileManager = GetRgscConcreteInstance()->_GetProfileManager();
	const RgscProfile &profile = profileManager->GetSignedInProfile();

	if(profile.IsValid())
	{
		unsigned folderId = profile.GetFolderId();

		if(this->GetRockstarGamesDirectory(path, bCreateDirectory, true))
		{
			safecatf(path, "%s\\", titleDirectoryName);

			if(bCreateDirectory)
			{
				success = CreateFolder(path);
			}
			else
			{
				success = true;
			}
		}

		if(success)
		{
			safecat(path, "Profiles\\");

			char szFolderId[20] = {0};
			formatf(szFolderId, "%08X", folderId);
			safecat(path, szFolderId);
			safecat(path, "\\");

			if(bCreateDirectory)
			{
				success = CreateFolder(path);
			}
			else
			{
				success = true;
			}
		}

		// If we don't support UTF8 file paths, get the ANSI file path instead and return it.
		//	We still want the above logic to create the folder locally on disk.
		if(success && !m_bUtf8PathsEnabled)
		{
			if(this->GetRockstarGamesDirectory(path, false, false))
			{
				safecatf(path, "%s\\", GetRgscConcreteInstance()->GetTitleId()->GetTitleDirectoryName());
				safecat(path, "Profiles\\");

				char szFolderId[20] = {0};
				formatf(szFolderId, "%08X", folderId);
				safecat(path, szFolderId);
				safecat(path, "\\");
			}
		}
	}

	return success ? RGSC_OK : RGSC_FAIL;
}

void RgscFileSystem::EnableUtf8FilePaths(const bool bEnabled)
{
	m_bUtf8PathsEnabled = bEnabled;
}

bool RgscFileSystem::GetSocialClubDllName(char (&path)[RGSC_MAX_PATH]) const
{
	safecpy(path, "socialclub.dll");
	return true;
}

bool RgscFileSystem::GetSocialClubDllDirectory(char (&path)[RGSC_MAX_PATH]) const
{
	bool success = false;
	path[0] = '\0';

	if(m_RootDllDirectory[0] != '\0')
	{
		safecpy(path, m_RootDllDirectory);
		success = true;
	}

	return success;
}

bool RgscFileSystem::GetSocialClubDllPath(char (&path)[RGSC_MAX_PATH]) const
{
	bool success = false;
	path[0] = '\0';

	if(AssertVerify(GetSocialClubDllDirectory(path)))
	{
		char dllName[RGSC_MAX_PATH];
		if(AssertVerify(GetSocialClubDllName(dllName)))
		{
			safecat(path, dllName);
			success = true;
		}
	}

	return success;
}

#if RSG_CPU_X64 && WORKAROUD_VS2013_CRT_BUG

void run_cpuid(uint32_t eax, uint32_t ecx, uint32_t (&abcd)[4])
{
	__cpuidex((int*)abcd, eax, ecx);
}     

int check_xcr0_ymm() 
{
    uint32_t xcr0;
    xcr0 = (uint32_t)_xgetbv(0);  /* min VS2010 SP1 compiler is required */
	return ((xcr0 & 6) == 6); /* checking if xmm and ymm state are enabled in XCR0 */
}

#if 0
int check_4th_gen_intel_core_features()
{
    uint32_t abcd[4];
    uint32_t fma_movbe_osxsave_mask = ((1 << 12) | (1 << 22) | (1 << 27));
    uint32_t avx2_bmi12_mask = (1 << 5) | (1 << 3) | (1 << 8);

    /* CPUID.(EAX=01H, ECX=0H):ECX.FMA[bit 12]==1   && 
       CPUID.(EAX=01H, ECX=0H):ECX.MOVBE[bit 22]==1 && 
       CPUID.(EAX=01H, ECX=0H):ECX.OSXSAVE[bit 27]==1 */
    run_cpuid( 1, 0, abcd );
    if ( (abcd[2] & fma_movbe_osxsave_mask) != fma_movbe_osxsave_mask ) 
	{
		Displayf("fma_movbe_osxsave_mask == 0");
		return 0;
	}
	Displayf("fma_movbe_osxsave_mask == 1");


    if ( ! check_xcr0_ymm() )
	{
		Displayf("check_xcr0_ymm == 0");
		return 0;
	}
	Displayf("check_xcr0_ymm == 1");

    /*  CPUID.(EAX=07H, ECX=0H):EBX.AVX2[bit 5]==1  &&
        CPUID.(EAX=07H, ECX=0H):EBX.BMI1[bit 3]==1  &&
        CPUID.(EAX=07H, ECX=0H):EBX.BMI2[bit 8]==1  */
    run_cpuid( 7, 0, abcd );
    if ( (abcd[1] & avx2_bmi12_mask) != avx2_bmi12_mask ) 
	{
		Displayf("avx2_bmi12_mask == 0");

		return 0;
	}
	Displayf("avx2_bmi12_mask == 0");

    /* CPUID.(EAX=80000001H):ECX.LZCNT[bit 5]==1 */
    run_cpuid( 0x80000001, 0, abcd );
    if ( (abcd[2] & (1 << 5)) == 0)
	{
		Displayf("last check == 0");
		return 0;
	}
	Displayf("last check == 1");

    return 1;
}
#endif

int check_fma()
{
    uint32_t abcd[4];
    uint32_t fma_movbe_osxsave_mask = ((1 << 12) | (1 << 22) | (1 << 27));

    /* CPUID.(EAX=01H, ECX=0H):ECX.FMA[bit 12]==1   && 
       CPUID.(EAX=01H, ECX=0H):ECX.MOVBE[bit 22]==1 && 
       CPUID.(EAX=01H, ECX=0H):ECX.OSXSAVE[bit 27]==1 */
    run_cpuid( 1, 0, abcd );
    if ( (abcd[2] & fma_movbe_osxsave_mask) != fma_movbe_osxsave_mask ) 
        return 0;

    return 1;
}

int check_chrome_depreciated()
{
	// Chrome has depreciated support for Windows XP and Vista in Chrome 50, so we need to check
	// the OS version to see if we must use the 32-bit subprocess which is locked to chrome 49.

	// cache result and if present, early out.
	static int result = -1;
	if (result != -1)
	{
		return result;
	}

	// Get the version from windows.
	OSVERSIONINFOEX version_info;
	version_info.dwOSVersionInfoSize = sizeof version_info;
	BOOL success = GetVersionEx(reinterpret_cast<OSVERSIONINFO*>(&version_info));
	
	if(!success)
	{
		Displayf("GetVersionEx() failed, falling back to 32-bit DLL to be safe.");
		return 1;
	}

	Displayf("GetVersionEx returned %d.%d", version_info.dwMajorVersion, version_info.dwMinorVersion);

	if (version_info.dwMajorVersion < 6)
	{
		// XP, ME, 2000, 98, etc
		result = 1;
	}
	else if (version_info.dwMajorVersion == 6) 
	{
		if (version_info.dwMinorVersion == 0) 
		{
			// Vista
			result = 1;
		} 
		else 
		{
			// Windows 7, Windows 8, Windows 8.1
			result = 0;
		}
	}
	else
	{
		// Newer (Windows 10)
		result = 0;
	}

	return result;
}

bool RgscFileSystem::GetSubprocessPath(char (&path)[RGSC_MAX_PATH]) const
{
	bool success = false;
	path[0] = '\0';

	rtry
	{
		Displayf("Determining if we should fallback to the 32-bit subprocess and CEF...");

		// Operating system support check - really this is a check for Windows 7 SP1 or higher.
		// Don't want to use GetVersionEx since that lies to us in compatibility mode.
		// GetEnabledXStateFeatures only exists in Windows 7 SP1 and up.

		bool osSupport = true;
		HMODULE kernel32Handle = GetModuleHandle(L"kernel32.dll");
		auto pGetEnabledXStateFeatures = GetProcAddress(kernel32Handle, "GetEnabledXStateFeatures");
		if(pGetEnabledXStateFeatures == NULL)
		{
			Displayf("GetEnabledXStateFeatures doesn't exist - must be older than Windows 7 SP1");
			osSupport = false;
		}
		else
		{
			Displayf("GetEnabledXStateFeatures exists - must be on Windows 7 SP1 or higher - safe to use 64-bit subprocess");
		}

		bool use64bit = true;

		if(!osSupport)
		{
			Displayf("OS doesn't support FMA3, checking if hardware does...");

			if(check_fma() != 0)
			{
				Displayf("OS doesn't support FMA3 but hardware does - must use 32-bit subprocess to avoid VS 2013 CRT bug that will crash the 64-bit subprocess");
				use64bit = false;
			}
			else
			{
				Displayf("OS doesn't support FMA3 but neither does the hardware - safe to use 64-bit subprocess");
			}
		}

		if(check_chrome_depreciated() != 0)
		{
			Displayf("Chrome doesn't support this OS in later versions - must use 32-bit subprocess which is locked to Chrome 49");
			use64bit = false;
		}

		if(use64bit)
		{
			rverify(GetSocialClubDllDirectory(path), catchall, );
		}
		else
		{
			rverify(GetKnownFolderPath(CSIDL_PROGRAM_FILESX86, path, true), catchall, );
			
#if __DEV
			safecat(path, "\\Rockstar Games\\Social Club Debug\\");
#else
			safecat(path, "\\Rockstar Games\\Social Club\\");
#endif
		}

		safecat(path, SUBPROCESS_EXE_NAME);

		RgscDisplayUtf8("Subprocess path: %s", path);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}
#else
bool RgscFileSystem::GetSubprocessPath(char (&path)[RGSC_MAX_PATH]) const
{
	bool success = false;
	path[0] = '\0';

	rtry
	{
		rverify(GetSocialClubDllDirectory(path), catchall, );
		safecat(path, SUBPROCESS_EXE_NAME);
		success = true;
	}
	rcatchall
	{

	}

	return success;
}
#endif

bool RgscFileSystem::GetProcessExeName(char (&path)[RGSC_MAX_PATH]) const
{
	bool success = false;
	path[0] = '\0';

	char fullPath[RGSC_MAX_PATH] = {0};

	if(AssertVerify(GetProcessExePath(fullPath)))
	{
		char* lastSlash = strrchr(fullPath, '\\');
		if(lastSlash == NULL)
		{
			lastSlash = strrchr(fullPath, '/');
		}

		if(AssertVerify(lastSlash != NULL))
		{
			safecpy(path, &lastSlash[1]);
		}

		success = true;
	}

	return success;
}

bool RgscFileSystem::GetProcessExeDirectory(char (&path)[RGSC_MAX_PATH]) const
{
	bool success = false;
	path[0] = '\0';

	if(AssertVerify(GetProcessExePath(path)))
	{
		char* lastSlash = strrchr(path, '\\');
		if(lastSlash == NULL)
		{
			lastSlash = strrchr(path, '/');
		}

		if(AssertVerify(lastSlash != NULL))
		{
			lastSlash[1] = '\0';
		}

		success = true;
	}

	return success;
}

bool RgscFileSystem::GetProcessExePath(char (&path)[RGSC_MAX_PATH]) const
{
	bool success = false;
	path[0] = '\0';

	wchar_t pathW[RGSC_MAX_PATH] = {0};
	if(AssertVerify(GetModuleFileName(NULL, pathW, COUNTOF(pathW)) != 0))
	{
		success = WideCharToMultiByte(CP_UTF8, 0, pathW, -1, path, COUNTOF(path), 0, 0) != 0;
	}

	return success;
}

bool RgscFileSystem::GetSteamDllPath(char (&path)[RGSC_MAX_PATH]) const
{
	bool success = false;
	path[0] = '\0';

	rtry
	{
		rverify(GetSocialClubDllDirectory(path), catchall, );
#if RSG_CPU_X64
		safecat(path, "steam_api64.dll");
#else
		safecat(path, "steam_api.dll");
#endif
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool RgscFileSystem::GetOfflineUiAssetsDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const
{
	bool success = false;
	path[0] = '\0';

	if(AssertVerify(GetSocialClubDllDirectory(path)))
	{
		safecat(path, UI_PATH);
		if(bCreateDirectory)
		{
			success = CreateFolder(path);
		}
		else
		{
			success = true;
		}
	}

	return success;
}

bool 
RgscFileSystem::GetOfflineUiAssetsDownloadDirectory(char (&path)[RGSC_MAX_PATH], bool bCreateDirectory) const
{
	bool success = false;
	path[0] = '\0';

	if(this->GetPlatformTempDirectory(path, bCreateDirectory))
	{
		safecat(path, UI_PATH);

		if(bCreateDirectory)
		{
			success = CreateFolder(path);
		}
		else
		{
			success = true;
		}
	}
	return success;
}

} //namespace rgsc

#endif // RSG_PC
