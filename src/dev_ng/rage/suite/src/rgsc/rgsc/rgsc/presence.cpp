// presence.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "file/file_config.h"
#include "presence.h"

#if RSG_PC

#include "atl/string.h"
#include "rgsc.h"
#include "rline/rlpresence.h"
#include "scpresence/rlscpresencemessage.h"
#include "json.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "net/nethardware.h"
#include "rline/rldiag.h"
#include "rline/ros/rlros.h"
#include "system/nelem.h"
#include "system/timer.h"
#include "rgsc_ui/script.h"
#include <time.h>

using namespace rage;

CompileTimeAssert(RGSC_PRESENCE_ATTR_NAME_MAX_SIZE == RLSC_PRESENCE_ATTR_NAME_MAX_SIZE);
//CompileTimeAssert(RGSC_PRESENCE_STRING_MAX_SIZE == RLSC_PRESENCE_STRING_MAX_SIZE);

#undef PostMessage

namespace rgsc
{

#if __RAGE_DEPENDENCY
RAGE_DEFINE_SUBCHANNEL(rgsc_dbg, presence)
#undef __rage_channel
#define __rage_channel rgsc_dbg_presence
#endif

// ===========================================================================
// IPresenceManager Public Interface
// ===========================================================================

RGSC_HRESULT RgscPresenceManager::QueryInterface(RGSC_REFIID riid, LPVOID* ppvObj)
{
	IRgscUnknown *pUnknown = NULL;
	if(ppvObj == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IPresenceManager*>(this);
	}
	else if(riid == IID_IPresenceManagerV1)
	{
		pUnknown = static_cast<IPresenceManagerV1*>(this);
	}
	else if(riid == IID_IPresenceManagerV2)
	{
		pUnknown = static_cast<IPresenceManagerV2*>(this);
	}
	else if(riid == IID_IPresenceManagerV3)
	{
		pUnknown = static_cast<IPresenceManagerV3*>(this);
	}
	else if(riid == IID_IPresenceManagerV4)
	{
		pUnknown = static_cast<IPresenceManagerV4*>(this);
	}
	else if(riid == IID_IPresenceManagerV5)
	{
		pUnknown = static_cast<IPresenceManagerV5*>(this);
	}
	else if(riid == IID_IPresenceManagerV6)
	{
		pUnknown = static_cast<IPresenceManagerV6*>(this);
	}
	else if(riid == IID_IPresenceManagerV7)
	{
		pUnknown = static_cast<IPresenceManagerV7*>(this);
	}

	*ppvObj = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

// ===========================================================================
// PresenceManager
// ===========================================================================

RgscPresenceManager::RgscPresenceManager()
{
	Clear();
}

RgscPresenceManager::~RgscPresenceManager()
{
	Shutdown();
}

void
RgscPresenceManager::Clear()
{
	m_GameInvites.Clear();
	m_AllowGameInvites = false;
	m_AllowPartyInvites = false;
	m_AllowAdditionalSessionGameInvites = false;
	memset(m_GameSessionInfo, 0, sizeof(m_GameSessionInfo));
	memset(m_PartySessionInfo, 0, sizeof(m_PartySessionInfo));
	memset(m_AdditionalSessionInfo, 0, sizeof(m_AdditionalSessionInfo));
	m_RichPresenceWriteTime = 0;
	m_RichPresenceLastMsg[0] = '\0';
	m_PresenceMessageHandler = NULL;
	m_PresenceDlgt = NULL;
}

bool
RgscPresenceManager::Init()
{
	Clear();
	m_FriendDlgt.Bind(this, &RgscPresenceManager::OnFriendEvent);

	rlFriendsManager::AddDelegate(&m_FriendDlgt);

	return true;
}

void
RgscPresenceManager::Update()
{
	//@@: location PRESENCEMANAGER_UPDATE
	m_GameInvites.Update();
}

void
RgscPresenceManager::Shutdown()
{
	rlFriendsManager::RemoveDelegate(&m_FriendDlgt);
	
	Clear();
}

void 
RgscPresenceManager::SignOut()
{
	// rlScPresence::SignOut() is called in rlPresence when we sign out.
	Clear();
}

// Note: we don't currently sign into presence when in launcher mode, but we also don't
// want to fire a bunch of asserts when testing launcher mode in sample session, so we
// return true if we're in launcher mode for these functions.
bool RgscPresenceManager::SetIntAttribute(const int localGamerIndex, const char* name, const s64 value)
{
	if (m_PresenceDlgt)
	{
		return m_PresenceDlgt->SetIntAttribute(localGamerIndex, name, value);
	}
	else if (GetRgscConcreteInstance()->IsPresenceSupported())
	{
		return rlPresence::SetIntAttribute(localGamerIndex, name, value);
	}
	else
	{
		return true;
	}
}

bool RgscPresenceManager::SetDoubleAttribute(const int localGamerIndex, const char* name, const double value)
{
	if (m_PresenceDlgt)
	{
		return m_PresenceDlgt->SetDoubleAttribute(localGamerIndex, name, value);
	}
	else if (GetRgscConcreteInstance()->IsPresenceSupported())
	{
		return rlPresence::SetDoubleAttribute(localGamerIndex, name, value);
	}
	else
	{
		return true;
	}
}

bool RgscPresenceManager::SetStringAttribute(const int localGamerIndex, const char* name, const char* value)
{
	if (m_PresenceDlgt)
	{
		return m_PresenceDlgt->SetStringAttribute(localGamerIndex, name, value);
	}
	else if (GetRgscConcreteInstance()->IsPresenceSupported())
	{
		return rlPresence::SetStringAttribute(localGamerIndex, name, value);
	}
	else
	{
		return true;
	}
}

bool RgscPresenceManager::GetIntAttribute(const int localGamerIndex, const char* name, s64* value)
{
	if (m_PresenceDlgt)
	{
		return m_PresenceDlgt->GetIntAttribute(localGamerIndex, name, value);
	}
	else if (GetRgscConcreteInstance()->IsPresenceSupported())
	{
		return rlPresence::GetIntAttribute(localGamerIndex, name, value);
	}
	else
	{
		*value = 0;
		return true;
	}
}

bool RgscPresenceManager::GetDoubleAttribute(const int localGamerIndex, const char* name, double* value)
{
	if (m_PresenceDlgt)
	{
		return m_PresenceDlgt->GetDoubleAttribute(localGamerIndex, name, value);
	}
	else if (GetRgscConcreteInstance()->IsPresenceSupported())
	{
		return rlPresence::GetDoubleAttribute(localGamerIndex, name, value);
	}
	else
	{
		*value = 0;
		return true;
	}
}

bool RgscPresenceManager::GetStringAttribute(const int localGamerIndex, const char* name, char* value, const unsigned sizeofValue)
{
	if (m_PresenceDlgt)
	{
		return m_PresenceDlgt->GetStringAttribute(localGamerIndex, name, value, sizeofValue);
	}
	else if (GetRgscConcreteInstance()->IsPresenceSupported())
	{
		return rlPresence::GetStringAttribute(localGamerIndex, name, value, sizeofValue);
	}
	else
	{
		if (sizeofValue > 0)
		{
			value[0] = '\0';
		}

		return true;
	}
}

bool 
RgscPresenceManager::Subscribe(const int localGamerIndex,
						   const char** channels,
						   const unsigned numChannels)
{
	if (m_PresenceDlgt)
	{
		return m_PresenceDlgt->Subscribe(localGamerIndex, channels, numChannels);
	}
	else if (GetRgscConcreteInstance()->IsPresenceSupported())
	{
		return rlPresence::Subscribe(localGamerIndex, channels, numChannels);
	}
	else
	{
		return true;
	}
}

bool 
RgscPresenceManager::Unsubscribe(const int localGamerIndex,
							 const char** channels,
							 const unsigned numChannels)
{
	if (m_PresenceDlgt)
	{
		return m_PresenceDlgt->Unsubscribe(localGamerIndex, channels, numChannels);
	}
	else if (GetRgscConcreteInstance()->IsPresenceSupported())
	{
		return rlPresence::Unsubscribe(localGamerIndex, channels, numChannels);
	}
	else
	{
		return true;
	}
}


bool 
RgscPresenceManager::UnsubscribeAll(const int localGamerIndex)
{
	if (m_PresenceDlgt)
	{
		return m_PresenceDlgt->UnsubscribeAll(localGamerIndex);
	}
	else if (GetRgscConcreteInstance()->IsPresenceSupported())
	{
		return rlPresence::UnsubscribeAll(localGamerIndex);
	}
	else
	{
		return true;
	}
}

bool 
RgscPresenceManager::Publish(const int localGamerIndex,
						 const char** channels,
						 const unsigned numChannels,
						 const char* filterName,
						 const char* paramNameValueCsv,
						 const IPresenceMessage* message)
{
	if (m_PresenceDlgt)
	{
		return m_PresenceDlgt->Publish(localGamerIndex, channels, numChannels, filterName, paramNameValueCsv, message);
	}
	else if (GetRgscConcreteInstance()->IsPresenceSupported())
	{
		PresenceMessageV1* v1 = NULL;
		RGSC_HRESULT hr = const_cast<IPresenceMessage*>(message)->QueryInterface(IID_IPresenceMessageV1, (void**) &v1);
		if(AssertVerify(SUCCEEDED(hr) && (v1 != NULL)))
		{
			return rlPresence::Publish(localGamerIndex, channels, numChannels, filterName, paramNameValueCsv, v1->GetContents());
		}

		return false;
	}
	else
	{
		return true;
	}
}

////////////////////////////////////////////////////////////////////////////////
// StringRecord
////////////////////////////////////////////////////////////////////////////////

class StringRecord
{
	char* m_String;

public:

	StringRecord()
		: m_String(NULL)
	{
		Clear();
	}

	~StringRecord()
	{
		Clear();
	}

	void Clear()
	{
		if(m_String)
		{
			RGSC_FREE(m_String);
			m_String = NULL;
		}
	}

	const char* GetString() {return m_String;}

	bool Import(const void* buf,
		const unsigned sizeOfBuf,
		unsigned* size = 0)
	{
		bool success = false;

		Clear();

		rtry
		{
			rverify(buf, catchall, );

			m_String = (char*)RGSC_ALLOCATE(RgscMetadata, sizeOfBuf + 1);
			rverify(m_String, catchall, );

			memcpy(m_String, buf, sizeOfBuf);
			m_String[sizeOfBuf] = '\0';

			success = true;        
		}
		rcatchall
		{
		}

		if(size){*size = success ? sizeOfBuf : 0;}

		return success;
	}
};

bool 
RgscPresenceManager::BuildRichPresenceString(const int presenceId,
										 IPresenceAttribute* attrs,
										 const unsigned numAttrs,
										 char* buf,
										 unsigned sizeOfBuf)
{
	bool success = false;

	// TODO: NS - speed this function up
	//			- preload the rich presence table indices
	//			- try to eliminate dynamic allocations (allocate on stack or keep in memory)

	// TODO: NS - use default values when applicable (or just use an empty string).
	// Xbox Live rules:
	// When a gamer starts in a game, if the game code does not specify a presence mode,
	// the Xbox LIVE service shows the default presence mode (mode 0) to other gamers.
	// The Xbox LIVE service also sets any un-set context values to the default context
	// value in the default presence mode. However, if the default presence mode contains
	// properties and those properties are not set for a gamer, rich presence is empty
	// for that gamer. 

	// TODO: NS - test rich presence strings that contain a line break (there can be at most one line break)

	RgscMetadata::Table* metadataTable = NULL;

	rtry
	{
		rverify(buf && (sizeOfBuf > 0), catchall, );
		buf[0] = '\0';

		RgscMetadata* metadata = GetRgscConcreteInstance()->_GetLocalMetadata();

		char richPresenceTemplateTableName[40];
		formatf(richPresenceTemplateTableName, "rich_presence_templates_%s", rlGetLanguageCode(rlGetLanguage()));
		metadataTable = metadata->GetTable(richPresenceTemplateTableName);
		rverify(metadataTable, catchall, );

		// get the rich presence template
		StringRecord templateString;
		bool exists = metadataTable->GetRecord(presenceId, templateString);
		rverify(exists, catchall, );

		// parse the template and fill in set members from player's schema
		// v1 example: "{c6} - Progress: {p0x1000003C}/{p0x1000003D}"
		// v2 example: "{0} - Progress: {1}"
		atString presenceString(templateString.GetString());

		int numFields = numAttrs;
		rverify(numFields >= 0, catchall, );

		PresenceAttributeV1* attrV1 = NULL;
		RGSC_HRESULT hr = attrs->QueryInterface(IID_IPresenceAttributeV1, (void**) &attrV1);
		rverify(SUCCEEDED(hr) && (attrV1 != NULL), catchall, );

		for(unsigned i = 0; i < (unsigned)numFields; i++)
		{
			PresenceAttributeV1* v1 = &attrV1[i];

			unsigned fid = 0;
			rverify(1 == sscanf(v1->GetName(), "%x", &fid), catchall, );

			if (metadata->GetVersion() == 1)
			{
				rverify(TokenizeRichPresenceStringV1(presenceString, v1, i, fid), catchall, );
			}
			else
			{
				rverify(TokenizeRichPresenceStringV2(presenceString, v1, i, fid), catchall, );
			}
		}

		safecpy(buf, presenceString.c_str(), sizeOfBuf);
		Assert((strlen(buf) + 1) < sizeOfBuf);

		rverify(strstr(buf, "{") == NULL, catchall, );
		rverify(strstr(buf, "{c") == NULL, catchall, );
		rverify(strstr(buf, "{p") == NULL, catchall, );

		success = true;
	}
	rcatchall
	{
		memset(buf, 0, sizeOfBuf);
	}

	if(metadataTable)
	{
		RL_DELETE(metadataTable);
	}

	return success;
}

bool RgscPresenceManager::TokenizeRichPresenceStringV1(atString& presenceString, PresenceAttribute* attr, unsigned index, unsigned fieldId)
{
	bool success = false;

#define XUSER_DATA_TYPE_CONTEXT     ((rage::u8)0)
#define XUSER_DATA_TYPE_INT32       ((rage::u8)1)
#define XUSER_DATA_TYPE_INT64       ((rage::u8)2)
#define XUSER_DATA_TYPE_DOUBLE      ((rage::u8)3)
#define XUSER_DATA_TYPE_UNICODE     ((rage::u8)4)
#define XUSER_DATA_TYPE_FLOAT       ((rage::u8)5)
#define XUSER_DATA_TYPE_BINARY      ((rage::u8)6)
#define XUSER_DATA_TYPE_DATETIME    ((rage::u8)7)
#define XUSER_DATA_TYPE_NULL        ((rage::u8)0xFF)
#define TYPE_FROM_ID(id)            ((((id) & 0xF0000000) >> 28) & 0x0F)

	RgscMetadata* metadata = GetRgscConcreteInstance()->_GetLocalMetadata();
	RgscMetadata::Table* contextValueTable = NULL;

	rtry
	{
		const unsigned ftype = TYPE_FROM_ID(fieldId);
		bool isContext = ftype == XUSER_DATA_TYPE_CONTEXT;

		if(isContext)
		{
			// get the string table for this context id
			char contextValueTableName[60];
			formatf(contextValueTableName, "rich_presence_context_%u_strings_%s", fieldId, rlGetLanguageCode(rlGetLanguage()));
			contextValueTable = metadata->GetTable(contextValueTableName);
			rverify(contextValueTable, catchall, );

			// get the string associated with the given context value
			StringRecord contextValue;

			s64 s64Key = 0;
			rverify(attr->GetValue(&s64Key), catchall, );

			u32 key = (u32)s64Key;
			bool exists = contextValueTable->GetRecord(key, contextValue);
			rverify(exists, catchall, );

			// context strings shouldn't themselves contain context/property tokens
			rverify(strstr(contextValue.GetString(), "{c") == NULL, catchall, );
			rverify(strstr(contextValue.GetString(), "{p") == NULL, catchall, );

			// replace occurrences of the context token with the context string from the table
			char contextToken[15];
			formatf(contextToken, "{c%u}", fieldId);

			// TODO: NS - test with UTF-8 strings (Japanese) - it should work since we are
			//			  always replacing standard ascii tokens with a sequence of UTF-8 bytes
			Assert(strstr(presenceString.c_str(), contextToken) != NULL);
			presenceString.Replace(contextToken, contextValue.GetString());

			RL_DELETE(contextValueTable);
			contextValueTable = NULL;
		}
		else // property
		{
			// replace occurrences of the property token with the player's schema value
			char propertyToken[15];
			formatf(propertyToken, "{p0x%08X}", fieldId);

			char propertyValue[64];

			switch(ftype)
			{
			case XUSER_DATA_TYPE_INT32:
				{
					s64 value = 0;
					rverify(attr->GetValue(&value), catchall, );
					formatf(propertyValue, "%d", (u32)value);
				}
				break;

			case XUSER_DATA_TYPE_INT64:
				{
					// TODO: NS - how does Xbox format 64-bit ints?
					s64 value = 0;
					rverify(attr->GetValue(&value), catchall, );
					formatf(propertyValue, "%"I64FMT"d", value);
				}
				break;
			case XUSER_DATA_TYPE_FLOAT:
			case XUSER_DATA_TYPE_DOUBLE:
				{
					double value = 0.0f;
					rverify(attr->GetValue(&value), catchall, );

					// TODO: NS - how does Xbox format floats? what precision?
					formatf(propertyValue, "%f", value);
				}
				break;
			default:
				rverify(false, catchall, );
				break;
			}

			// TODO: NS - test with UTF-8 strings (Japanese) - it should work since we are
			//			  always replacing standard ascii tokens with a sequence of UTF-8 bytes
			Assert(strstr(presenceString.c_str(), propertyToken) != NULL);
			presenceString.Replace(propertyToken, propertyValue);
		}

		success = true;
	}
	rcatchall
	{

	}

	if(contextValueTable)
	{
		RL_DELETE(contextValueTable);
		contextValueTable = NULL;
	}

	return success;
}

bool RgscPresenceManager::TokenizeRichPresenceStringV2(atString& presenceString, PresenceAttribute* attr, unsigned index, unsigned fieldId)
{
	bool success = false;

	RgscMetadata* metadata = GetRgscConcreteInstance()->_GetLocalMetadata();
	RgscMetadata::Table* contextValueTable = NULL;

	rtry
	{
		// get the string table for this context id
		char contextValueTableName[60];
		formatf(contextValueTableName, "rich_presence_context_%u_strings_%s", fieldId, rlGetLanguageCode(rlGetLanguage()));
		contextValueTable = metadata->GetTable(contextValueTableName);
		rverify(contextValueTable, catchall, );

		// get the string associated with the given context value
		StringRecord contextValue;

		s64 s64Key = 0;
		rverify(attr->GetValue(&s64Key), catchall, );

		u32 key = (u32)s64Key;
		bool exists = contextValueTable->GetRecord(key, contextValue);
		rverify(exists, catchall, );

		// context strings shouldn't themselves contain context/property tokens
		rverify(strstr(contextValue.GetString(), "{") == NULL, catchall, );

		// replace occurrences of the indexed token with the set member we've looked up
		char contextToken[15];
		formatf(contextToken, "{%u}", index);

		// TODO: NS - test with UTF-8 strings (Japanese) - it should work since we are
		//			  always replacing standard ascii tokens with a sequence of UTF-8 bytes
		Assert(strstr(presenceString.c_str(), contextToken) != NULL);
		presenceString.Replace(contextToken, contextValue.GetString());

		success = true;
	}
	rcatchall
	{

	}

	if(contextValueTable)
	{
		RL_DELETE(contextValueTable);
		contextValueTable = NULL;
	}


	return success;
}

bool 
RgscPresenceManager::SetRichPresence(const int presenceId,
								 IPresenceAttribute* attrs,
								 const unsigned numAttrs,
								 IAsyncStatus* status)
{
	bool success = false;

	rtry
	{
		// TODO JRM:
		// Ideally I'd like to have SetRichPresence apply a queued message, that we consume every 'n' seconds.
		// This would allow the game to write as frequently as possible, and only the latest message would be sent.
		// As it is, the game's RichPresenceManager already throttles requests for XB1/PS4 so we can leverage that.


		// TODO JRM:
		// MP3 could support this, but until we're sure we'll simply bail out for v1 metadata
		RgscMetadata* metadata = GetRgscConcreteInstance()->_GetLocalMetadata();
		if (metadata->GetVersion() == 1)
			return false;

		// Enforce a minimum of 30 seconds between rich presence writes. 
		static const int MIN_RICH_PRESENCE_WRITE_INTERVAL_MS = (30 * 1000);
		rverify(sysTimer::HasElapsedIntervalMs(m_RichPresenceWriteTime, MIN_RICH_PRESENCE_WRITE_INTERVAL_MS), catchall,);

		char buf[RGSC_PRESENCE_STRING_MAX_SIZE] = {0};
		rverify(BuildRichPresenceString(presenceId, attrs, numAttrs, buf, sizeof(buf)), catchall, );
		rlDebug("Setting '%s' rich presence to '%s'", rlGetLanguageCode(rlGetLanguage()), buf);

		// Check to see if the string is different than our last write.
		// Bail early if there is no change.
		rcheck(stricmp(m_RichPresenceLastMsg, buf) != 0, redundant, rlDebug3("Rich presence string '%s' same as last write, ignoring", m_RichPresenceLastMsg));
		safecpy(m_RichPresenceLastMsg, buf);

		// Set our attribute
		rverify(SetStringAttribute(RL_RGSC_GAMER_INDEX, rlScAttributeId::RichPresence.Name, buf), catchall, );

		rlGamerHandle gh;
		rverify(rlPresence::GetGamerHandle(RL_RGSC_GAMER_INDEX, &gh), catchall, );

		rlUserIdBuf userIdBuf;
		rverify(gh.ToUserId(userIdBuf) != NULL, catchall, );
		
		// Construct the friends channel for ourselves
		char friendChannel[256];
		formatf(friendChannel, "friend.%s", userIdBuf);

		// Create a rich presence message changed event containing our gamer handle and rich presence
		rlScPresenceMessageRichPresenceChanged msg;
		msg.m_GamerHandle = gh;
		safecpy(msg.m_PresenceMsg, buf);

		// Msg must contain enclosing braces (i.e. be valid json).
		char msgBuf[1024];
		RsonWriter rw;
		rw.Init(msgBuf, RSON_FORMAT_JSON);
		rw.Begin(NULL, NULL);
		msg.Export(rw);
		rw.End();

		// send message to game
		PresenceMessageV1 rgscMsg;
		rgscMsg.SetContents(rw.ToString());
		rgscMsg.SetTimestamp(0);

		// Publish
		const char* channel = friendChannel;
		rverify(Publish(RL_RGSC_GAMER_INDEX, &channel, 1, "", "", &rgscMsg), catchall, );
		
		success = true;
	}
	rcatch(redundant)
	{
		success = true;
	}
	rcatchall
	{

	}

	if (status)
	{
		AsyncStatus* asyncStatus = NULL;
		RGSC_HRESULT hr = status->QueryInterface(IID_IAsyncStatusV1, (void**)&asyncStatus);
		if (SUCCEEDED(hr))
		{
			asyncStatus->GetNetStatus()->SetPending();

			if (success)
			{
				asyncStatus->GetNetStatus()->SetSucceeded();
			}
			else
			{
				asyncStatus->GetNetStatus()->SetFailed();
			}
		}
	}


	return success;
}

bool 
RgscPresenceManager::AllowGameInvites(bool allowInvites)
{
	m_AllowGameInvites = allowInvites;
	return true;
}

bool 
RgscPresenceManager::SetGameSessionInfo(const char* sessionInfo)
{
	safecpy(m_GameSessionInfo, sessionInfo);
	return true;
}

bool 
RgscPresenceManager::AllowPartyInvites(bool allowInvites)
{
	m_AllowPartyInvites = allowInvites;
	return true;
}

bool
RgscPresenceManager::SetPartySessionInfo(const char* sessionInfo)
{
	safecpy(m_PartySessionInfo, sessionInfo);
	return true;
}

bool 
RgscPresenceManager::SetAdditionalSessionInfo(const char* sessionInfo)
{
	safecpy(m_AdditionalSessionInfo, sessionInfo);
	return true;
}

bool 
RgscPresenceManager::AllowAdditionalSessionGameInvites(bool allowInvites)
{
	m_AllowAdditionalSessionGameInvites = allowInvites;
	return true;
}

bool 
RgscPresenceManager::NotifyGameInviteConsumed(RockstarId inviterId, const char* sessionInfo, IPresenceManagerV4::GameInviteConsumeReason OUTPUT_ONLY(reason))
{
	rlDisplay("Consuming game invite from %"I64FMT"d, for reason %d", inviterId, (int)reason);

	int numInvites = m_GameInvites.GetCount();
	for (int i = 0; i < numInvites; ++i)
	{
		ScGameInvite invite; 
		m_GameInvites.GetInvite(i, invite);

		// Find an invite with a matching inviter and which message contains session info
		if (invite.m_InviterGamerHandle.GetRockstarId() == inviterId &&
			strstr(invite.m_Message, sessionInfo) != NULL)
		{
			rlDisplay("Consumed");
			m_GameInvites.Remove(i);
			return true;
		}
	}

	return false;
}

bool
RgscPresenceManager::NotifySocialClubEvent(IPresenceMessage* message)
{
	bool bEventConsumed = false;

	if (message == NULL)
		return bEventConsumed;

	// If the title uses Presence from the SC DLL, it should not be notifying
	//	the DLL of presence messages -- this would be handling twice.
	if (GetRgscConcreteInstance()->IsPresenceSupported())
		return bEventConsumed;

	PresenceMessageV1* v1 = NULL;
	RGSC_HRESULT hr = message->QueryInterface(IID_IPresenceMessageV1, (void**) &v1);
	if(AssertVerify(SUCCEEDED(hr) && (v1 != NULL)))
	{
		rlScPresenceMessage scPresMsg(v1->GetContents(), v1->GetTimestamp());
		bEventConsumed = OnSocialClubEvent(scPresMsg);
	}

	return bEventConsumed;
}

bool RgscPresenceManager::SetPresenceMessageHandler(HandlePresenceMessageCallback cb)
{
	rlAssert(m_PresenceMessageHandler == NULL);
	m_PresenceMessageHandler = cb;
	return true;
}

bool RgscPresenceManager::SetDelegate(IRgscPresenceDelegate* dlgt)
{
	bool success = false;

	if (dlgt)
	{
		// V1 Attribute Delegate is required
		HRESULT hr = dlgt->QueryInterface(IID_IRgscPresenceDelegateV1, (void**)&m_PresenceDlgt);
		if (SUCCEEDED(hr) && m_PresenceDlgt)
		{
			success = true;
		}
	}
	else
	{
		m_PresenceDlgt = NULL;
		success = true;
	}

	return success;
}

///////////////////////////////////////////////////////////////////////////////
//  ScGameInvite
///////////////////////////////////////////////////////////////////////////////
ScGameInvite::ScGameInvite()
{
	Clear();
}

ScGameInvite::~ScGameInvite()
{
	Clear();
}

void 
ScGameInvite::Clear()
{
	memset(m_AvatarUrl, 0, sizeof(m_AvatarUrl));
	memset(m_Salutation, 0, sizeof(m_Salutation));
	memset(m_Subject, 0, sizeof(m_Subject));
	memset(m_InviterName, 0, sizeof(m_InviterName));
	memset(m_Message, 0, sizeof(m_Message));
	m_InviterGamerHandle.Clear();
	m_ReceivedTimestamp = 0;
	m_TimeToLiveMs = 0;
	m_InviteId = -1;
}

bool 
ScGameInvite::IsValid() const
{
	return m_InviterGamerHandle.IsValid();
}

void
ScGameInvite::Reset(const rlGamerHandle& gamerHandle,
					const rlScPresenceMessage& msgBuf,
					const char* avatarUrl,
					const char* inviterName,
					const char* subject,
					const char* salutation,
					time_t timestamp,
					u32 timeToLiveMs)
{
	m_InviterGamerHandle = gamerHandle;
	safecpy(m_Message, msgBuf.m_Contents);
	safecpy(m_AvatarUrl, avatarUrl);
	safecpy(m_InviterName, inviterName);
	safecpy(m_Subject, subject);
	safecpy(m_Salutation, salutation);
	m_ReceivedTimestamp = timestamp;
	m_TimeToLiveMs = timeToLiveMs;
	m_InviteId = -1;
}

///////////////////////////////////////////////////////////////////////////////
//  ScGameInvites
///////////////////////////////////////////////////////////////////////////////
ScGameInvites::ScGameInvites()
{
	Clear();
}

ScGameInvites::~ScGameInvites()
{
	Clear();
}

void 
ScGameInvites::Clear()
{
	m_Count = 0;
	m_LastUpdateTime = 0;
	for(unsigned i = 0; i < MAX_SC_GAME_INVITES; ++i)
	{
		m_Invites[i].Clear();
	}
}

void 
ScGameInvites::Sort()
{
	struct GameInviteSortPredicate
	{
		static bool Sort(ScGameInvite& a, ScGameInvite& b)
		{
			/*
			Sort game invites descending by time received (most recent on top)
			*/
			return b.m_ReceivedTimestamp < a.m_ReceivedTimestamp;
		}
	};

	std::sort(m_Invites, m_Invites + MAX_SC_GAME_INVITES, GameInviteSortPredicate::Sort);
}

void 
ScGameInvites::Update()
{
#if __ASSERT
	for(unsigned i = 0; i < MAX_SC_GAME_INVITES; ++i)
	{
		Assert((!m_Invites[i].IsValid() && (m_Invites[i].m_InviteId == -1)) ||
			   (m_Invites[i].IsValid() && (m_Invites[i].m_InviteId == (int)i)));
	}
#endif

	// expire game invites when their time to live reaches 0.

	const unsigned curTime = sysTimer::GetSystemMsTime() | 0x01;
	unsigned timeStep = 0;

	if(m_LastUpdateTime)
	{
		timeStep = curTime - m_LastUpdateTime;
	}

	m_LastUpdateTime = curTime;

	unsigned numProcessed = 0;
	unsigned count = m_Count; // m_Count can change inside the loop when Remove() is called
	for(unsigned i = 0; numProcessed < count; ++i)
	{
		ScGameInvite& invite = m_Invites[i];
		if(invite.IsValid())
		{
			invite.m_TimeToLiveMs -= timeStep;

			if(invite.m_TimeToLiveMs <= 0)
			{
				rlDebug2("Invite from %s has expired", invite.m_InviterName);

				Remove(i);
			}
			else
			{
				//rlDebug3("Invite from %s expires in %d ms", invite.m_InviterName, invite.m_TimeToLiveMs);
			}

			numProcessed++;
		}
	}
}

int 
ScGameInvites::FindFreeSlot()
{
	for(unsigned i = 0; i < MAX_SC_GAME_INVITES; ++i)
	{
		if(m_Invites[i].IsValid() == false)
		{
			return i;
		}
	}
	return -1;
}

bool
ScGameInvites::IsFull() const
{
	return m_Count == MAX_SC_GAME_INVITES;
}

bool 
ScGameInvites::Add(const ScGameInvite& invite)
{
	int freeSlot = FindFreeSlot();
	if(freeSlot >= 0)
	{
		m_Invites[freeSlot] = invite;
		m_Invites[freeSlot].m_InviteId = freeSlot;
		m_Count++;
		return true;
	}

	return false;
}

void 
ScGameInvites::Remove(unsigned index)
{
	if(AssertVerify(m_Invites[index].IsValid()))
	{
		Assert(m_Invites[index].m_InviteId == (int)index);
		m_Invites[index].Clear();
		--m_Count;
	}
}

unsigned 
ScGameInvites::GetCount() const
{
	return m_Count;
}

bool 
ScGameInvites::HasInviteFromPlayer(const rlGamerHandle& gh, unsigned &index)
{
	for(unsigned i = 0; i < MAX_SC_GAME_INVITES; ++i)
	{
		if(m_Invites[i].IsValid() && (m_Invites[i].m_InviterGamerHandle == gh))
		{
			index = i;
			return true;
		}
	}
	return false;
}

bool 
ScGameInvites::GetInvite(unsigned index, ScGameInvite& invite) const
{
	invite = m_Invites[index];
	return invite.IsValid();
}

const char* 
RgscPresenceManager::GetGameInviteListAsJson()
{
	json_char *jc = "";

	rtry
	{
		rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal(), catchall, );

		JSONNODE* n = json_new(JSON_NODE);
		rverify(n, catchall, );

		JSONNODE* c = json_new(JSON_ARRAY);
		rverify(c, catchall, );

		json_set_name(c, "GameInvites");

		// make a copy so we can sort the way the UI wants it
		ScGameInvites invites = m_GameInvites;
		invites.Sort();

		u32 numInvites = 0;
		
		for(unsigned i = 0; i < invites.GetCount(); i++)
		{
			ScGameInvite invite;
			if(invites.GetInvite(i, invite))
			{
				// don't show the invite in the UI if it's just about to expire
				if(invite.m_TimeToLiveMs > (3 * 1000))
				{
					JSONNODE* p = json_new(JSON_NODE);
					rverify(p, catchall, );

					char szTime[64];
					formatf(szTime, "%"I64FMT"u", invite.m_ReceivedTimestamp);

					char szRockstarId[64];
					formatf(szRockstarId, "%"I64FMT"d", invite.m_InviterGamerHandle.GetRockstarId());
					
					json_push_back(p, json_new_i("GameInviteId", invite.m_InviteId));
					json_push_back(p, json_new_a("RockstarId", szRockstarId));
					json_push_back(p, json_new_a("Nickname", invite.m_InviterName));
					json_push_back(p, json_new_a("AvatarUrl", invite.m_AvatarUrl));
					json_push_back(p, json_new_a("ReceivedTime", szTime));
					json_push_back(p, json_new_a("Salutation", invite.m_Salutation));

					numInvites++;

					json_push_back(c, p);
				}
			}
		}

		json_push_back(n, json_new_i("Count", numInvites));

		json_push_back(n, c);

		jc = json_write_formatted(n);

#if !__NO_OUTPUT
		size_t len = strlen(jc);
		for(size_t i = 0; i < len; i++)
		{
			printf("%c", jc[i]);
		}
		fflush(stdout);
#endif

		json_delete(n);
	}
	rcatchall
	{

	}

	return jc;
}

static json_char* json_as_string_or_null(json_const JSONNODE* node)
{
	char* str = json_as_string(node);
	return (_stricmp("null", str) != 0) ? str : NULL;
}

int
RgscPresenceManager::GetGameInviteIdFromJson(const char* json)
{
	int gameInviteId = -1;

	JSONNODE *n = json_parse(json);

	if(!AssertVerify(n != NULL))
	{
		return gameInviteId;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return gameInviteId;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "GameInviteId") == 0)
		{
			gameInviteId = json_as_int(*i);
		}

		// cleanup and increment the iterator
		++i;
	}

	json_delete(n);

	return gameInviteId;
}

bool 
RgscPresenceManager::GameInviteAccepted(const char* jsonInvite)
{
	bool success = false;

	rtry
	{
		int gameInviteId = GetGameInviteIdFromJson(jsonInvite);
		rverify(gameInviteId >= 0, catchall, );

		ScGameInvite invite;
		rverify(m_GameInvites.GetInvite(gameInviteId, invite), catchall, );
	
		PresenceMessage msg;
		msg.SetContents(invite.m_Message);

		GetRgscConcreteInstance()->HandleNotification(rgsc::NOTIFY_GAME_INVITE_ACCEPTED, &msg);

		m_GameInvites.Remove(gameInviteId);

		GetRgscConcreteInstance()->_GetUiInterface()->HideUi();

		success = true;
	}
	rcatchall
	{
	}

	return success;
}

void 
RgscPresenceManager::GameInviteDeclined(const char* jsonInvite)
{
	int gameInviteId = GetGameInviteIdFromJson(jsonInvite);
	if(gameInviteId >= 0)
	{
		m_GameInvites.Remove(gameInviteId);
	}
}

bool 
RgscPresenceManager::CanSendGameInvite(RockstarId rockstarId)
{
	bool isBlocked = GetRgscConcreteInstance()->_GetPlayerManager()->IsBlocked(rockstarId);
	return (isBlocked == false) && (m_AllowGameInvites || m_AllowPartyInvites);
}

bool
RgscPresenceManager::CanSendAdditionalSessionInvite(RockstarId rockstarId)
{
	bool isBlocked = GetRgscConcreteInstance()->_GetPlayerManager()->IsBlocked(rockstarId);
	return (isBlocked == false) && (m_AllowAdditionalSessionGameInvites);
}

RockstarId 
RgscPresenceManager::GetRockstarIdFromJson(const char* json)
{
	RockstarId rockstarId = InvalidRockstarId;

	JSONNODE *n = json_parse(json);

	if(!AssertVerify(n != NULL))
	{
		return rockstarId;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return rockstarId;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "RockstarId") == 0)
		{
			json_char *node_value = json_as_string_or_null(*i);
			sscanf_s(node_value, "%I64d", &rockstarId);
			json_free_safe(node_value);
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	return rockstarId;
}

void  
RgscPresenceManager::GetRequestIdAndRockstarIdFromJson(const char* json,
												   int &requestId,
												   RockstarId &rockstarId)
{
	requestId = -1;
	rockstarId = InvalidRockstarId;

	JSONNODE *n = json_parse(json);

	if(!AssertVerify(n != NULL))
	{
		return;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "RequestId") == 0)
		{
			requestId = json_as_int(*i);
		}
		else if(_stricmp(node_name, "RockstarId") == 0)
		{
			json_char *node_value = json_as_string_or_null(*i);
			sscanf_s(node_value, "%I64d", &rockstarId);
			json_free_safe(node_value);
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);
}

unsigned 
RgscPresenceManager::GameNumGameInvites()
{
	return m_GameInvites.GetCount();
}

bool 
RgscPresenceManager::SendGameInviteToScUi(const ScGameInvite& invite)
{
	bool success = false;

	rtry
	{
		JSONNODE* rootNode = json_new(JSON_NODE);
		rverify(rootNode, catchall, );

		json_push_back(rootNode, json_new_i("Count", 1));

		JSONNODE* notificationArray = json_new(JSON_ARRAY);
		rverify(notificationArray, catchall, );
		json_set_name(notificationArray, "Notifications");

		JSONNODE* notificationNode = json_new(JSON_NODE);
		rverify(notificationNode, catchall, );

		json_push_back(notificationNode, json_new_a("Type", "GameInvites"));
		json_push_back(notificationNode, json_new_i("Count", 1));

		JSONNODE* gameInviteArray = json_new(JSON_ARRAY);
		rverify(gameInviteArray, catchall, );

		json_set_name(gameInviteArray, "Data");

		JSONNODE* gameInviteNode = json_new(JSON_NODE);
		rverify(gameInviteNode, catchall, );

		json_set_name(gameInviteNode, "GameInvite");

		char szTime[64];
		formatf(szTime, "%"I64FMT"u", invite.m_ReceivedTimestamp);

		char szRockstarId[64];
		formatf(szRockstarId, "%"I64FMT"d", invite.m_InviterGamerHandle.GetRockstarId());
		json_push_back(gameInviteNode, json_new_a("RockstarId", szRockstarId));
		json_push_back(gameInviteNode, json_new_a("Nickname", invite.m_InviterName));
		json_push_back(gameInviteNode, json_new_a("AvatarUrl", invite.m_AvatarUrl));
		json_push_back(gameInviteNode, json_new_a("ReceivedTime", szTime));
		json_push_back(gameInviteNode, json_new_a("Salutation", invite.m_Salutation));

		json_push_back(gameInviteArray, gameInviteNode);

		json_push_back(notificationNode, gameInviteArray);

		json_push_back(notificationArray, notificationNode);

		json_push_back(rootNode, notificationArray);

		json_char* jc = json_write_formatted(rootNode);

#if !__NO_OUTPUT
		size_t len = strlen(jc);
		for(size_t i = 0; i < len; i++)
		{
			printf("%c", jc[i]);
		}
		fflush(stdout);
#endif

		json_delete(rootNode);

		GetRgscConcreteInstance()->_GetUiInterface()->SendNotification(jc);

		json_free_safe(jc);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool 
RgscPresenceManager::SendFriendStatusToScUi(const rlFriend* f)
{
	bool success = false;

	rtry
	{
		rverify(f, catchall, );

		JSONNODE* rootNode = json_new(JSON_NODE);
		rverify(rootNode, catchall, );

		json_push_back(rootNode, json_new_i("Count", 1));

		JSONNODE* notificationArray = json_new(JSON_ARRAY);
		rverify(notificationArray, catchall, );
		json_set_name(notificationArray, "Notifications");

		JSONNODE* notificationNode = json_new(JSON_NODE);
		rverify(notificationNode, catchall, );

		json_push_back(notificationNode, json_new_a("Type", "FriendStatus"));
		json_push_back(notificationNode, json_new_i("Count", 1));

		JSONNODE* friendStatusArray = json_new(JSON_ARRAY);
		rverify(friendStatusArray, catchall, );

		json_set_name(friendStatusArray, "Data");

		JSONNODE* friendStatusNode = json_new(JSON_NODE);
		rverify(friendStatusNode, catchall, );

		json_set_name(friendStatusNode, "FriendStatus");

		char szRockstarId[64];
		formatf(szRockstarId, "%"I64FMT"d", f->GetScFriend().m_RockstarId);
		json_push_back(friendStatusNode, json_new_a("RockstarId", szRockstarId));
		json_push_back(friendStatusNode, json_new_a("Nickname", f->GetScFriend().m_Nickname));
		json_push_back(friendStatusNode, json_new_a("AvatarUrl", f->GetScFriend().m_AvatarUrl));
		json_push_back(friendStatusNode, json_new_b("IsOnline", f->GetScFriend().m_IsOnline));
		json_push_back(friendStatusNode, json_new_b("IsPlayingSameTitle", f->GetScFriend().m_IsPlayingSameTitle));

		json_push_back(friendStatusArray, friendStatusNode);

		json_push_back(notificationNode, friendStatusArray);

		json_push_back(notificationArray, notificationNode);

		json_push_back(rootNode, notificationArray);

		json_char* jc = json_write_formatted(rootNode);

#if !__NO_OUTPUT
		size_t len = strlen(jc);
		for(size_t i = 0; i < len; i++)
		{
			printf("%c", jc[i]);
		}
		fflush(stdout);
#endif

		json_delete(rootNode);

		GetRgscConcreteInstance()->_GetUiInterface()->SendNotification(jc);

		json_free_safe(jc);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool 
RgscPresenceManager::SendFriendRequestToScUi(const rlScPresenceMessage& msgBuf)
{
	bool success = false;

	rtry
	{
		rlScPresenceMessageFriendInviteReceived msg;
		rverify(msg.Import(msgBuf), catchall, rlError("Failed to import rlScPresenceMessageFriendInviteReceived"));

		JSONNODE* rootNode = json_new(JSON_NODE);
		rverify(rootNode, catchall, );

		json_push_back(rootNode, json_new_i("Count", 1));

		JSONNODE* notificationArray = json_new(JSON_ARRAY);
		rverify(notificationArray, catchall, );
		json_set_name(notificationArray, "Notifications");

		JSONNODE* notificationNode = json_new(JSON_NODE);
		rverify(notificationNode, catchall, );

		json_push_back(notificationNode, json_new_a("Type", "FriendRequestReceived"));
		json_push_back(notificationNode, json_new_i("Count", 1));

		JSONNODE* friendRequestArray = json_new(JSON_ARRAY);
		rverify(friendRequestArray, catchall, );

		json_set_name(friendRequestArray, "Data");

		JSONNODE* friendRequestNode = json_new(JSON_NODE);
		rverify(friendRequestNode, catchall, );

		json_set_name(friendRequestNode, "FriendRequest");

 		char szRockstarId[64];
 		formatf(szRockstarId, "%"I64FMT"d", msg.m_hInvitee.GetRockstarId());
 		json_push_back(friendRequestNode, json_new_a("RockstarId", szRockstarId));
 		json_push_back(friendRequestNode, json_new_a("Nickname", msg.m_InviteeName));
 		json_push_back(friendRequestNode, json_new_a("AvatarUrl", msg.m_InviteeAvatarUrl));

		json_push_back(friendRequestArray, friendRequestNode);

		json_push_back(notificationNode, friendRequestArray);

		json_push_back(notificationArray, notificationNode);

		json_push_back(rootNode, notificationArray);

		json_char* jc = json_write_formatted(rootNode);

#if !__NO_OUTPUT
		size_t len = strlen(jc);
		for(size_t i = 0; i < len; i++)
		{
			printf("%c", jc[i]);
		}
		fflush(stdout);
#endif

		json_delete(rootNode);

		GetRgscConcreteInstance()->_GetUiInterface()->SendNotification(jc);

		json_free_safe(jc);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool 
RgscPresenceManager::SendFriendRequestAcceptedToScUi(const rlScPresenceMessage& msgBuf)
{
	bool success = false;

	rtry
	{
		rlScPresenceMessageFriendInviteAccepted msg;
		rverify(msg.Import(msgBuf), catchall, rlError("Failed to import rlScPresenceMessageFriendInviteAccepted"));

		JSONNODE* rootNode = json_new(JSON_NODE);
		rverify(rootNode, catchall, );

		json_push_back(rootNode, json_new_i("Count", 1));

		JSONNODE* notificationArray = json_new(JSON_ARRAY);
		rverify(notificationArray, catchall, );
		json_set_name(notificationArray, "Notifications");

		JSONNODE* notificationNode = json_new(JSON_NODE);
		rverify(notificationNode, catchall, );

		json_push_back(notificationNode, json_new_a("Type", "FriendRequestAccepted"));
		json_push_back(notificationNode, json_new_i("Count", 1));

		JSONNODE* friendRequestArray = json_new(JSON_ARRAY);
		rverify(friendRequestArray, catchall, );

		json_set_name(friendRequestArray, "Data");

		JSONNODE* friendRequestNode = json_new(JSON_NODE);
		rverify(friendRequestNode, catchall, );

		json_set_name(friendRequestNode, "FriendRequest");

 		char szRockstarId[64];
 		formatf(szRockstarId, "%"I64FMT"d", msg.m_hInvitee.GetRockstarId());
 		json_push_back(friendRequestNode, json_new_a("RockstarId", szRockstarId));
 		json_push_back(friendRequestNode, json_new_a("Nickname", msg.m_InviteeName));
 		json_push_back(friendRequestNode, json_new_a("AvatarUrl", msg.m_InviteeAvatarUrl));

		json_push_back(friendRequestArray, friendRequestNode);

		json_push_back(notificationNode, friendRequestArray);

		json_push_back(notificationArray, notificationNode);

		json_push_back(rootNode, notificationArray);

		json_char* jc = json_write_formatted(rootNode);

#if !__NO_OUTPUT
		size_t len = strlen(jc);
		for(size_t i = 0; i < len; i++)
		{
			printf("%c", jc[i]);
		}
		fflush(stdout);
#endif

		json_delete(rootNode);

		GetRgscConcreteInstance()->_GetUiInterface()->SendNotification(jc);

		json_free_safe(jc);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool 
RgscPresenceManager::HandleGameInvite(const rlScPresenceMessage& msgBuf)
{
	bool success = false;

	// be careful not to read/interpret the session info struct as it can change at any time
	
	rtry
	{
		RsonReader rr(msgBuf.m_Contents, (unsigned)strlen(msgBuf.m_Contents));
		rlGamerHandle inviterGamerHandle;
		char inviterName[128]; // in case RL_MAX_NAME_BUF_SIZE ever changes
		CompileTimeAssert(RL_MAX_NAME_BUF_SIZE <= sizeof(inviterName));

		rverify(rr.ReadValue("h", inviterGamerHandle), catchall, rlError("Game invite missing inviter gamer handle"));
		rverify(rr.ReadString("n", inviterName), catchall, rlError("Game invite is missing inviter name"));

		rlDebug("Received an invite from: \"%s\"", inviterName);

		unsigned existingIndex = 0;
		bool duplicate = m_GameInvites.HasInviteFromPlayer(inviterGamerHandle, existingIndex);
		if(duplicate)
		{
			m_GameInvites.Remove(existingIndex);
		}

		bool isBlocked = GetRgscConcreteInstance()->_GetPlayerManager()->IsBlocked(inviterGamerHandle.GetRockstarId());
		rcheck(isBlocked == false, catchall, Displayf("Ignoring game invite from %s because they are on our block list.", inviterName));

		if(!m_GameInvites.IsFull())
		{
			ScGameInvite invite;

			// if we only support receiving game invites from friends, we can get the avatarUrl from the friend list.
			// if we support receiving game invites from non-friends, we'll need to start a task to get that info from ROS here.
			const char* avatarUrl = NULL;
			rlFriend* f = rlFriendsManager::GetDefaultFriendPage()->GetFriend(inviterGamerHandle);
			if(f)
			{
				avatarUrl = f->GetScFriend().m_AvatarUrl;
			}
			else
			{
				rlWarning("Received a game invite but could not find the friend '%s' in our list.", inviterName);
			}

			// url:bugstar:2814299 - We got a game invite for a friend who just added us to our list before the friends list updated.
			//	Assert becomes ignorable.
			//Assertf(avatarUrl, "Couldn't find friend %s in our friend list", inviterName);

			invite.Reset(inviterGamerHandle,
						 msgBuf,
						 avatarUrl,
						 inviterName,
						 "",		// TODO: NS - subject not supported?
						 "",		// TODO: NS - salutations not supported?
						 time(NULL),
						 ScGameInvite::GAME_INVITE_TIME_TO_LIVE_MS);

			if(AssertVerify(m_GameInvites.Add(invite)))
			{
				if(duplicate == false)
				{
					SendGameInviteToScUi(invite);
				}
				success = true;
			}
		}
	}
	rcatchall
	{

	}

	return success;
}

void 
RgscPresenceManager::HandleFriendStatusChanged(bool isOnline, const rlScPresenceMessage& msgBuf)
{
	rlGamerHandle gh;

	if(isOnline)
	{
		rlScPresenceMessageGamerOnline msg;
		if(msg.Import(msgBuf))
		{
			gh = msg.m_GamerHandle;
		}
	}
	else
	{
		rlScPresenceMessageGamerOffline msg;
		if(msg.Import(msgBuf))
		{
			gh = msg.m_GamerHandle;
		}
	}

	if(gh.IsValid())
	{
		rlFriend* f = rlFriendsManager::GetDefaultFriendPage()->GetFriend(gh);
		if(f)
		{
			if(isOnline)
			{
				// TODO: NS - Waiting for the event to include the IsPlayingSameTitle flag.
				//			  For now we assume they are playing the same title.
				//rlPresence::SyncFriends();
				rlPresence::SetFriendIsOnline(0, f, true, true);
			}
			else
			{
				rlPresence::SetFriendIsOnline(0, f, false, false);
			}

			// don't send this to the game or SCUI if we're not fully signed online yet.
			// the game can query the friend list when we say we're online and we'll
			// tell the SCUI how many friends are online shortly after we're signed online.
			if(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal())
			{
				GetRgscConcreteInstance()->HandleNotification(
					rgsc::NOTIFY_FRIEND_STATUS_CHANGED, NULL);

				SendFriendStatusToScUi(f);
			}
		}
	}
}

void 
RgscPresenceManager::HandleFriendListChanged(const rlScPresenceMessage& msgBuf)
{
	rlScPresenceMessageFriendListChanged msg;
	if(msg.Import(msgBuf))
	{
		rlDebug("SC friend list changed");
		rlPresence::SyncFriends();
	}
}

void 
RgscPresenceManager::HandleFriendRequestReceived(const rlScPresenceMessage& msgBuf)
{
	SendFriendRequestToScUi(msgBuf);
}

void 
RgscPresenceManager::HandleFriendRequestAccepted(const rlScPresenceMessage& msgBuf)
{
	SendFriendRequestAcceptedToScUi(msgBuf);
}

void 
RgscPresenceManager::HandleTextMessageReceived(const rlScPresenceMessage& msgBuf)
{
	bool success = false;

	rtry
	{
		RsonReader rr(msgBuf.m_Contents, (unsigned)strlen(msgBuf.m_Contents));
		rlGamerHandle sender;
		char senderName[128];
		CompileTimeAssert(RL_MAX_NAME_BUF_SIZE <= sizeof(senderName));

		rverify(rr.ReadValue("h", sender), catchall, rlError("Text message missing sender gamer handle"));
		rverify(rr.ReadString("n", senderName), catchall, rlError("Text message missing sender name"));

		JSONNODE* rootNode = json_new(JSON_NODE);
		rverify(rootNode, catchall, );

		json_push_back(rootNode, json_new_i("Count", 1));

		JSONNODE* notificationArray = json_new(JSON_ARRAY);
		rverify(notificationArray, catchall, );
		json_set_name(notificationArray, "Notifications");

		JSONNODE* notificationNode = json_new(JSON_NODE);
		rverify(notificationNode, catchall, );

		json_push_back(notificationNode, json_new_a("Type", "MessageReceived"));
		json_push_back(notificationNode, json_new_i("Count", 1));

		JSONNODE* textMessageArray = json_new(JSON_ARRAY);
		rverify(textMessageArray, catchall, );

		json_set_name(textMessageArray, "Data");

		JSONNODE* textMessageNode = json_new(JSON_NODE);
		rverify(textMessageNode, catchall, );

		json_set_name(textMessageNode, "MessageReceived");

		char szRockstarId[64];
		formatf(szRockstarId, "%"I64FMT"d", sender.GetRockstarId());
		json_push_back(textMessageNode, json_new_a("RockstarId", szRockstarId));
		json_push_back(textMessageNode, json_new_a("Nickname", senderName));

		json_push_back(textMessageArray, textMessageNode);

		json_push_back(notificationNode, textMessageArray);

		json_push_back(notificationArray, notificationNode);

		json_push_back(rootNode, notificationArray);

		json_char* jc = json_write_formatted(rootNode);

#if !__NO_OUTPUT
		size_t len = strlen(jc);
		for(size_t i = 0; i < len; i++)
		{
			printf("%c", jc[i]);
		}
		fflush(stdout);
#endif

		json_delete(rootNode);

		GetRgscConcreteInstance()->_GetUiInterface()->SendNotification(jc);

		json_free_safe(jc);

		success = true;
	}
	rcatchall
	{

	}
}

void RgscPresenceManager::HandleFriendRichPresenceChanged(const rlScPresenceMessage& msgBuf)
{
	rlScPresenceMessageRichPresenceChanged msg;
	if (msg.Import(msgBuf))
	{
		rlFriendsManager::OnFriendRichPresenceChanged(msg.m_GamerHandle, msg.m_PresenceMsg);
	}
}

#if 0
void 
PresenceManager::HandleFriendRequestDeclined(const rlScPresenceMessage& msgBuf)
{
	// TODO: NS - might have to start an async task here to retrieve additional info (i.e. name, avatar url, etc.)

	rlGamerHandle gh;

	rlScPresenceMessageFriendRequestDeclined msg;
	if(msg.Import(msgBuf))
	{
		gh = msg.m_GamerHandle;
	}

	if(gh.IsValid())
	{
		// tell the SCUI?
	}
}

void 
PresenceManager::HandleFriendRemoved(const rlScPresenceMessage& msgBuf)
{
	rlGamerHandle gh;

	rlScPresenceMessageFriendRemoved msg;
	if(msg.Import(msgBuf))
	{
		gh = msg.m_GamerHandle;
	}

	if(gh.IsValid())
	{
		// tell the SCUI?
	}

	rlPresence::SyncFriends();
}
#endif

bool 
RgscPresenceManager::SendPresenceMessageToScUi(const rlScPresenceMessage& msgBuf)
{
	bool success = false;

	char jsonMsg[4096];

	// if this is a message published to a channel, the SCUI team has requested
	// that we send them just the 'msg' part
	if(msgBuf.IsA<rlScPresenceMessagePublish>())
	{		
		rlScPresenceMessagePublish msg;
		if(rlVerifyf(msg.Import(msgBuf), "Error importing '%s'", msg.Name()))
		{
			formatf(jsonMsg, "%s", msg.m_Message);
			success = true;
		}
	}
	else
	{
		formatf(jsonMsg, "{%s}", msgBuf.m_Contents);
		success = true;
	}

	if(success)
	{
		rgsc::Script::JsPresenceMessageReceived(&success, jsonMsg);
	}

	return success;
}

void RgscPresenceManager::MessageSent(const char* jsonNotification)
{
	RockstarId recipient = GetRockstarIdFromJson(jsonNotification);
	const RgscProfile& profile = GetRgscConcreteInstance()->_GetProfileManager()->GetSignedInProfile();

	if(profile.IsValid() && (recipient != InvalidRockstarId))
	{
		rlScPresenceMessageTextMessageRecieved textMessage;

		textMessage.m_SenderGamerHandle.ResetSc((RockstarId)profile.GetProfileId());
		safecpy(textMessage.m_SenderName, profile.GetName());

		rlGamerHandle ghRecipient;
		ghRecipient.ResetSc(recipient);

		char buf[256] = {0};
		RsonWriter rw(buf, RSON_FORMAT_JSON);
		textMessage.Export(rw);

		PostMessageFireAndForget(PEER_TO_PEER, &ghRecipient, 1, buf);
	}
}

bool RgscPresenceManager::PostMessageFireAndForget(PresenceSendMethod sendMethod, rlGamerHandle* handles, unsigned numHandles, const char* msgContents, int ttlSeconds)
{
	bool success = false;

	rtry
	{
		rverify(numHandles > 0, catchall, );
		rcheck(m_PresenceMessageHandler, catchall, );

		PresenceMessage msg;
		msg.SetTimestamp(0);
		msg.SetContents(msgContents);

		RockstarId* rockstarIds =  (RockstarId*)Alloca(RockstarId, numHandles);
		rverify(rockstarIds, catchall, );

		// copy rockstar ids 
		for (unsigned i = 0; i < numHandles; i++)
		{
			rockstarIds[i] = handles[i].GetRockstarId();
		}

		// activate callback
		success = m_PresenceMessageHandler(sendMethod, rockstarIds, numHandles, &msg, ttlSeconds);
	}
	rcatchall
	{
	}

	// Fallback to the default rlPresence::PostMessage if the message isn't handled.
	if (!success)
	{
		success = rlPresence::PostMessage(RL_RGSC_GAMER_INDEX, handles, numHandles, msgContents, ttlSeconds);
	}

	return success;
}

bool 
RgscPresenceManager::OnSocialClubEvent(const rlScPresenceMessage& msgBuf)
{
	bool bEventConsumed = true;

	SendPresenceMessageToScUi(msgBuf);

	if(msgBuf.IsA<rlScPresenceMessageMpInvite>())
	{
		// Game invites appear in the SCUI and can also handled by the game client
		HandleGameInvite(msgBuf);
		bEventConsumed = false;
	}
	else if(msgBuf.IsA<rlScPresenceMessageGamerOnline>())
	{
		HandleFriendStatusChanged(true, msgBuf);
	}
	else if(msgBuf.IsA<rlScPresenceMessageGamerOffline>())
	{
		HandleFriendStatusChanged(false, msgBuf);
	}
	else if(msgBuf.IsA<rlScPresenceMessageFriendListChanged>())
	{
		HandleFriendListChanged(msgBuf);
	}
	else if(msgBuf.IsA<rlScPresenceMessageFriendInviteReceived>())
	{
		HandleFriendRequestReceived(msgBuf);
	}
	else if (msgBuf.IsA<rlScPresenceMessageRichPresenceChanged>())
	{
		HandleFriendRichPresenceChanged(msgBuf);
	}
	else if(msgBuf.IsA<rlScPresenceMessageFriendInviteAccepted>())
	{
		HandleFriendRequestAccepted(msgBuf);
	}
	else if(msgBuf.IsA<rlScPresenceMessageFriendInviteCanceled>())
	{
		rlScPresenceMessageFriendInviteCanceled msg;
		if(msg.Import(msgBuf))
		{
			rlDebug("SC friend invite to %"I64FMT"d canceled", msg.m_InviteeId);
		}
	}
	else if(msgBuf.IsA<rlScPresenceMessageTextMessageRecieved>())
	{
		HandleTextMessageReceived(msgBuf);
	}
	else if(msgBuf.IsA<rlScPresenceRenewTicket>())
	{
		rlDebug("Renewing ticket due to %s event", rlScPresenceRenewTicket::MSG_ID());
		rlRos::RenewCredentials(0);
	}
	else
	{
		// All other events are forwarded to the game.
		bEventConsumed = false;
	}

	// For presence enabled titles, if the event is not consumed, it must be forwarded to the game.
	if(!bEventConsumed && GetRgscConcreteInstance()->IsPresenceSupported())
	{
		// pass message to the game
		PresenceMessage msg;
		msg.SetTimestamp(msgBuf.m_PosixTimeStamp);
		msg.SetContents(msgBuf.m_Contents);

		GetRgscConcreteInstance()->HandleNotification(rgsc::NOTIFY_SOCIAL_CLUB_EVENT, &msg);
	}

	return bEventConsumed;
}

void
RgscPresenceManager::OnFriendEvent(const rlFriendEvent* evt)
{
	const unsigned evtId = evt->GetId();
	if (RLFRIEND_EVENT_LIST_CHANGED == evtId)
	{
		// don't send this to the game if we're not fully signed online yet.
		// the game can query the friend list when we say we're online.
		if(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal())
		{
			GetRgscConcreteInstance()->HandleNotification(rgsc::NOTIFY_FRIEND_STATUS_CHANGED, NULL);
		}
	}
}

#if !__NO_OUTPUT
class PresenceManagerTaskBase : public rgscTask<RgscPresenceManager>
{
	u32 m_StartTime;

public:
	PresenceManagerTaskBase() : m_StartTime(0) {}
	void Start()
	{
		m_StartTime = sysTimer::GetSystemMsTime();
		rlTask<RgscPresenceManager>::Start();
	}

	u32 GetElapsedTime()
	{
		return sysTimer::GetSystemMsTime() - m_StartTime;
	}
};
#else
	typedef rgscTask<RgscPresenceManager> PresenceManagerTaskBase;
#endif

///////////////////////////////////////////////////////////////////////////////
//  SendGameInviteTask
///////////////////////////////////////////////////////////////////////////////

class SendGameInviteTask : public PresenceManagerTaskBase
{
public:
	RL_TASK_DECL(SendGameInviteTask);

	enum State
	{
		STATE_INVALID = -1,
		STATE_SEND_GAME_INVITE,
		STATE_SENDING_GAME_INVITE,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	SendGameInviteTask();

	bool Configure(RgscPresenceManager* ctx,
				   int requestId,
				   const RockstarId rockstarId);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);
	
	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

private:
	bool SendGameInvite();
	bool SendResults();

	int m_RequestId;
	RockstarId m_RockstarId;

	int m_State;
	netStatus m_MyStatus;
};

RGSC_HRESULT 
RgscPresenceManager::GameInviteSend(const char* jsonInvite)
{
	int requestId = -1;
	RockstarId rockstarId = InvalidRockstarId;

	GetRequestIdAndRockstarIdFromJson(jsonInvite, requestId, rockstarId);

	RLPC_CREATETASK_NO_STATUS(SendGameInviteTask,
							  requestId,
							  rockstarId);
}

SendGameInviteTask::SendGameInviteTask()
: m_State(STATE_INVALID)
{

}

bool 
SendGameInviteTask::Configure(RgscPresenceManager* /*ctx*/,
							   int requestId,
							   const RockstarId rockstarId)
{
	m_State = STATE_INVALID;
	m_RequestId = requestId;
	m_RockstarId = rockstarId;
	return true;
}

void
SendGameInviteTask::Start()
{
	rlDebug2("SendGameInviteTask...");

	this->PresenceManagerTaskBase::Start();

	m_State = STATE_SEND_GAME_INVITE;
}

void
SendGameInviteTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("SendGameInviteTask %s (%ums)", FinishString(finishType), GetElapsedTime());

	this->PresenceManagerTaskBase::Finish(finishType, resultCode);
}

void
SendGameInviteTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
	this->PresenceManagerTaskBase::DoCancel();
}

void
SendGameInviteTask::Update(const unsigned timeStep)
{
	this->PresenceManagerTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_SEND_GAME_INVITE:
			if(this->SendGameInvite())
			{
				m_State = STATE_SENDING_GAME_INVITE;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_SENDING_GAME_INVITE:
			if(m_MyStatus.Succeeded())
			{
				m_State = STATE_SUCCEEDED;
			}
			else if(!m_MyStatus.Pending())
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_SUCCEEDED:
			SendResults();
			this->Finish(FINISH_SUCCEEDED);
			break;

		case STATE_FAILED:
			SendResults();
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool
SendGameInviteTask::SendGameInvite()
{
	bool success = false;

	rtry
	{
		// be careful not to interpret the session info struct as it can change at any time
		rverify(m_RockstarId != InvalidRockstarId, catchall, );

		rverify(m_Ctx->CanSendGameInvite(m_RockstarId), catchall, );

		rlGamerHandle invitee;
		invitee.ResetSc(m_RockstarId);

		rlScPresenceMessageMpInvite invite;

		rlPresence::GetGamerHandle(0, &invite.m_InviterGamerHandle);
		rlPresence::GetName(0, invite.m_InviterName);

		// the session info string attribute comes directly from the game, so it's 
		// guaranteed to encode the version of the session info struct that's used by the game.

		// using the special version of rlScPresenceMessageMpInvite::Write()
		// that doesn't interpret/decode the session info struct.
		char scMessageBuf[1024 + MAX_SESSION_INFO_BUFFER_SIZE];
		RsonWriter rw(scMessageBuf, RSON_FORMAT_JSON);

		// Get the game session or party session info
		const char* sessionInfo = NULL;
		
		// Send the game session info if either gsinfo or additional(i.e. trinfo) are set
		if(m_Ctx->GetAllowGameInvites() || m_Ctx->GetAllowAdditionalSessionInvite())
		{
			sessionInfo = m_Ctx->GetGameSessionInfo();
		}
		else if(m_Ctx->GetAllowPartyInvites())
		{
			sessionInfo = m_Ctx->GetPartySessionInfo();
		}

		// Validate the session info and export it
		rverify(sessionInfo, catchall, rlError("Session Info null"));
		rverify(sessionInfo[0] != '\0', catchall, rlError("Session Info empty"));
		rverify(invite.Export(rw, sessionInfo), catchall, );

		rverify(GetRgscConcreteInstance()->_GetPresenceManager()->PostMessageFireAndForget(RgscPresenceManager::PEER_TO_PEER, &invitee, 1, scMessageBuf), catchall, );
        m_MyStatus.ForceSucceeded();

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool 
SendGameInviteTask::SendResults()
{
	bool success = false;

	rtry
	{
		// send result back to the UI
		json_char *jc = "";

		JSONNODE* n = json_new(JSON_NODE);
		rverify(n, catchall, );

		char szRockstarId[64];
		formatf(szRockstarId, "%"I64FMT"d", m_RockstarId);
		json_push_back(n, json_new_a("RockstarId", szRockstarId));
		json_push_back(n, json_new_i("RequestId", m_RequestId));
		json_push_back(n, json_new_b("Success", m_MyStatus.Succeeded()));
		json_push_back(n, json_new_i("ResultCode", m_MyStatus.GetResultCode()));

		jc = json_write_formatted(n);
		rverify(jc, catchall, );

		json_delete(n);

		bool succeeded = false;
		rgsc::Script::JsGameInviteSendResult(&succeeded, jc);

		json_free_safe(jc);
	}
	rcatchall
	{

	}

	return success;
}

///////////////////////////////////////////////////////////////////////////////
//  JoinViaPresenceTask
///////////////////////////////////////////////////////////////////////////////

class JoinViaPresenceTask : public PresenceManagerTaskBase
{
public:
	RL_TASK_DECL(JoinViaPresenceTask);

	enum State
	{
		STATE_INVALID = -1,
		STATE_GET_PRESENCE_DATA,
		STATE_GETTING_PRESENCE_DATA,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	JoinViaPresenceTask();

	bool Configure(RgscPresenceManager* ctx,
				   int requestId,
				   const RockstarId rockstarId);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

private:
	bool GetPresenceData();
	bool JoinViaPresence();
	bool SendResults(bool operationSucceeded);

	static const unsigned int INDEX_IS_GAME_JOINABLE = 0;
	static const unsigned int INDEX_GAME_SESSION_INFO = 1;
	static const unsigned int INDEX_IS_PARTY_JOINABLE = 2;
	static const unsigned int INDEX_PARTY_SESSION_INFO = 3;

	rlScPresenceAttribute m_PresenceAttrs[4];
	int m_RequestId;
	rlGamerHandle m_GamerHandle;

	int m_State;
	netStatus m_MyStatus;
};

RGSC_HRESULT 
RgscPresenceManager::JoinViaPresence(const char* jsonJoinViaPresence)
{
	int requestId = -1;
	RockstarId rockstarId = InvalidRockstarId;

	GetRequestIdAndRockstarIdFromJson(jsonJoinViaPresence, requestId, rockstarId);

	if(AssertVerify(rockstarId != InvalidRockstarId))
	{
		RLPC_CREATETASK_NO_STATUS(JoinViaPresenceTask,
								  requestId,
								  rockstarId);
	}
	return false;
}

JoinViaPresenceTask::JoinViaPresenceTask()
: m_State(STATE_INVALID)
{

}

bool 
JoinViaPresenceTask::Configure(RgscPresenceManager* /*ctx*/,
							   int requestId,
							   const RockstarId rockstarId)
{
	m_State = STATE_INVALID;
	m_RequestId = requestId;
	m_GamerHandle.ResetSc(rockstarId);
	return true;
}

void
JoinViaPresenceTask::Start()
{
	rlDebug2("JoinViaPresenceTask...");

	this->PresenceManagerTaskBase::Start();

	m_State = STATE_GET_PRESENCE_DATA;
}

void
JoinViaPresenceTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("JoinViaPresenceTask %s (%ums)", FinishString(finishType), GetElapsedTime());

	this->PresenceManagerTaskBase::Finish(finishType, resultCode);
}

void
JoinViaPresenceTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
	this->PresenceManagerTaskBase::DoCancel();
}

void
JoinViaPresenceTask::Update(const unsigned timeStep)
{
	this->PresenceManagerTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_GET_PRESENCE_DATA:
			if(this->GetPresenceData())
			{
				m_State = STATE_GETTING_PRESENCE_DATA;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_GETTING_PRESENCE_DATA:
			if(!m_MyStatus.Pending())
			{
				// some of the presence attributes may not exist (i.e. party session if they're
				// in a game session) and therefore the task fails, but we still have the info
				// we need (eg. game session info)
				if(JoinViaPresence())
				{
					m_State = STATE_SUCCEEDED;
				}
				else
				{
					m_State = STATE_FAILED;
				}
			}
			break;

		case STATE_SUCCEEDED:
			SendResults(true);
			this->Finish(FINISH_SUCCEEDED);
			break;

		case STATE_FAILED:
			SendResults(false);
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool 
JoinViaPresenceTask::SendResults(bool operationSucceeded)
{
	json_char *jc = "";
	bool success = false;

	rtry
	{
		JSONNODE* n = json_new(JSON_NODE);
		rverify(n, catchall, );

		json_push_back(n, json_new_i("RequestId", m_RequestId));
		char szRockstarId[64];
		formatf(szRockstarId, "%"I64FMT"d", m_GamerHandle.GetRockstarId());
		json_push_back(n, json_new_a("RockstarId", szRockstarId));
		json_push_back(n, json_new_b("Success", operationSucceeded));
		json_push_back(n, json_new_i("ResultCode", m_MyStatus.GetResultCode()));

		jc = json_write_formatted(n);
		rverify(jc, catchall, );

		json_delete(n);

		bool succeeded = false;
		rgsc::Script::JsJoinViaPresenceResult(&succeeded, jc);

		json_free_safe(jc);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool 
JoinViaPresenceTask::GetPresenceData()
{
	bool success = false;

	rtry
	{
		m_PresenceAttrs[INDEX_IS_GAME_JOINABLE].Reset(rlScAttributeId::IsGameJoinable.Name, (s64)0);
		m_PresenceAttrs[INDEX_GAME_SESSION_INFO].Reset(rlScAttributeId::GameSessionInfo.Name, "");
		m_PresenceAttrs[INDEX_IS_PARTY_JOINABLE].Reset(rlScAttributeId::IsPartyJoinable.Name, (s64)0);
		m_PresenceAttrs[INDEX_PARTY_SESSION_INFO].Reset(rlScAttributeId::PartySessionInfo.Name, "");

		rverify(rlPresence::GetAttributesForGamer(0, m_GamerHandle, m_PresenceAttrs, COUNTOF(m_PresenceAttrs), &m_MyStatus), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool 
JoinViaPresenceTask::JoinViaPresence()
{
	bool success = false;

	rtry
	{
		// be careful not to interpret the session info struct as it can change at any time

		s64 isGameJoinable = 0;
		char szGameSessionInfo[RGSC_PRESENCE_STRING_MAX_SIZE];
		if(m_PresenceAttrs[INDEX_IS_GAME_JOINABLE].Type == RLSC_PRESTYPE_S64)
		{
			rverify(m_PresenceAttrs[INDEX_IS_GAME_JOINABLE].GetValue(&isGameJoinable), catchall, );

			if(isGameJoinable)
			{
				rverify(m_PresenceAttrs[INDEX_GAME_SESSION_INFO].Type == RLSC_PRESTYPE_STRING, catchall, );
				rverify(m_PresenceAttrs[INDEX_GAME_SESSION_INFO].GetValue(szGameSessionInfo), catchall, );
			}
		}

		s64 isPartyJoinable = 0;
		char szPartySessionInfo[RGSC_PRESENCE_STRING_MAX_SIZE];
		if(m_PresenceAttrs[INDEX_IS_PARTY_JOINABLE].Type == RLSC_PRESTYPE_S64)
		{
			rverify(m_PresenceAttrs[INDEX_IS_PARTY_JOINABLE].GetValue(&isPartyJoinable), catchall, );
			if(isPartyJoinable)
			{
				rverify(m_PresenceAttrs[INDEX_PARTY_SESSION_INFO].Type == RLSC_PRESTYPE_STRING, catchall, );
				rverify(m_PresenceAttrs[INDEX_PARTY_SESSION_INFO].GetValue(szPartySessionInfo), catchall, );
			}
		}

		char* szSessionInfo = NULL;
		if(isGameJoinable)
		{
			// join game session
			szSessionInfo = szGameSessionInfo;
		}
		else if(isPartyJoinable)
		{
			// join party session
			szSessionInfo = szPartySessionInfo;
		}

		rcheck(szSessionInfo, catchall, );

		rlScPresenceMessageMpInvite invite;
		invite.m_InviterGamerHandle = m_GamerHandle;
		safecpy(invite.m_InviterName, "JVP");

		// using the special version of rlScPresenceMessageMpInvite::Write()
		// that doesn't interpret/decode the session info struct directly.
		char scMessageBuf[1024 + MAX_SESSION_INFO_BUFFER_SIZE];
		RsonWriter rw(scMessageBuf, RSON_FORMAT_JSON);
		invite.Export(rw, szSessionInfo);

		PresenceMessage rgscMsg;
		rgscMsg.SetTimestamp(0);
		rgscMsg.SetContents(scMessageBuf);

		GetRgscConcreteInstance()->HandleNotification(
			rgsc::NOTIFY_JOINED_VIA_PRESENCE,
			&rgscMsg);

		GetRgscConcreteInstance()->_GetUiInterface()->HideUi();

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

///////////////////////////////////////////////////////////////////////////////
//  GetAttributesForGamerTask
///////////////////////////////////////////////////////////////////////////////

class GetAttributesForGamerTask : public PresenceManagerTaskBase
{
public:
	RL_TASK_DECL(GetAttributesForGamerTask);

	enum State
	{
		STATE_INVALID = -1,
		STATE_CALL,
		STATE_CALLING,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	GetAttributesForGamerTask();

	bool Configure(RgscPresenceManager* ctx,
				   const int localGamerIndexOfCaller,
				   const RockstarId rockstarId,
				   IPresenceAttribute* attrs,
				   const unsigned numAttrs);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	int m_LocalGamerIndexOfCaller;
	IPresenceAttribute* m_RgscAttrs;
	rlGamerHandle m_GamerHandle;
	rlScPresenceAttribute* m_RlScAttrs;
	unsigned m_NumAttrs;

private:
	bool Call();
	bool ProcessResults();

	int m_State;
	netStatus m_MyStatus;
};

bool 
RgscPresenceManager::GetAttributesForGamer(/*const int localGamerIndexOfCaller,*/
									   const RockstarId rockstarId,
									   IPresenceAttribute* attrs,
									   const unsigned numAttrs,
									   IAsyncStatus* status)
{
	const int localGamerIndexOfCaller = 0;
	RLPC_CREATETASK_IASYNC(GetAttributesForGamerTask,
						   localGamerIndexOfCaller,
						   rockstarId,
						   attrs,
						   numAttrs);
}

GetAttributesForGamerTask::GetAttributesForGamerTask()
: m_State(STATE_INVALID)
, m_LocalGamerIndexOfCaller(-1)
, m_RlScAttrs(NULL)
{

}

bool 
GetAttributesForGamerTask::Configure(RgscPresenceManager* /*ctx*/,
									 const int localGamerIndexOfCaller,
									 const RockstarId rockstarId,
									 IPresenceAttribute* attrs,
									 const unsigned numAttrs)
{
	m_State = STATE_INVALID;
	m_LocalGamerIndexOfCaller = localGamerIndexOfCaller;
	m_GamerHandle.ResetSc(rockstarId);
	m_RgscAttrs = attrs;
	m_NumAttrs = numAttrs;
	
	return true;
}

void
GetAttributesForGamerTask::Start()
{
	rlDebug2("GetAttributesForGamerTask...");

	this->PresenceManagerTaskBase::Start();

	m_State = STATE_CALL;
}

void
GetAttributesForGamerTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("GetAttributesForGamerTask %s (%ums)", FinishString(finishType), GetElapsedTime());

	if(m_RlScAttrs)
	{
		RL_DELETE_ARRAY(m_RlScAttrs, m_NumAttrs);
		m_RlScAttrs = NULL;
	}
	this->PresenceManagerTaskBase::Finish(finishType, resultCode);
}

void
GetAttributesForGamerTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
	this->PresenceManagerTaskBase::DoCancel();
}

void
GetAttributesForGamerTask::Update(const unsigned timeStep)
{
	this->PresenceManagerTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_CALL:
			if(Call())
			{
				m_State = STATE_CALLING;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_CALLING:
			if(m_MyStatus.Succeeded())
			{
				if(ProcessResults())
				{
					m_State = STATE_SUCCEEDED;
				}
				else
				{
					m_State = STATE_FAILED;
				}
			}
			else if(!m_MyStatus.Pending())
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_SUCCEEDED:
			this->Finish(FINISH_SUCCEEDED);
			break;

		case STATE_FAILED:
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool 
GetAttributesForGamerTask::Call()
{
	bool success = false;

	rtry
	{
		m_RlScAttrs = RL_NEW_ARRAY(GetAttributesForGamerTask, rlScPresenceAttribute, m_NumAttrs);
		rverify(m_RlScAttrs, catchall, );

		unsigned numAttrs = 0;

		PresenceAttributeV1* attrV1 = NULL;
		RGSC_HRESULT hr = m_RgscAttrs->QueryInterface(IID_IPresenceAttributeV1, (void**) &attrV1);
		rverify(SUCCEEDED(hr) && (attrV1 != NULL), catchall, );

		for(unsigned i = 0; i < m_NumAttrs; ++i)
		{
			PresenceAttributeV1* v1 = &attrV1[i];

			if(v1->GetType() == IPresenceAttributeV1::PRESTYPE_S64)
			{
				s64 value;
				AssertVerify(v1->GetValue(&value));
				m_RlScAttrs[numAttrs++].Reset(v1->GetName(), value);
			}
			else if(v1->GetType() == IPresenceAttributeV1::PRESTYPE_DOUBLE)
			{
				double value;
				AssertVerify(v1->GetValue(&value));
				m_RlScAttrs[numAttrs++].Reset(v1->GetName(), value);
			}
			else if(v1->GetType() == IPresenceAttributeV1::PRESTYPE_STRING)
			{
				char value[RGSC_PRESENCE_STRING_MAX_SIZE];
				AssertVerify(v1->GetValue(value, sizeof(value)));
				m_RlScAttrs[numAttrs++].Reset(v1->GetName(), value);
			}
			else if(v1->GetType() == IPresenceAttributeV1::PRESTYPE_INVALID)
			{
				Assertf(false, "presence attribute is invalid");
				numAttrs++;
			}
			else
			{
				Assertf(false, "Unknown presence attribute type %d", (int)v1->GetType());
				numAttrs++;
			}
		}

		Assert(m_NumAttrs == numAttrs);
		success = rlPresence::GetAttributesForGamer(m_LocalGamerIndexOfCaller, m_GamerHandle, m_RlScAttrs, numAttrs, &m_MyStatus);
	}
	rcatchall
	{

	}

	return success;
}

bool 
GetAttributesForGamerTask::ProcessResults()
{
	bool success = false;

	rtry
	{

		PresenceAttributeV1* attrV1 = NULL;
		RGSC_HRESULT hr = m_RgscAttrs->QueryInterface(IID_IPresenceAttributeV1, (void**) &attrV1);
		rverify(SUCCEEDED(hr) && (attrV1 != NULL), catchall, );

		for(unsigned i = 0; i < m_NumAttrs; ++i)
		{
			PresenceAttributeV1* v1 = &attrV1[i];

			if(m_RlScAttrs[i].Type == RLSC_PRESTYPE_S64)
			{
				s64 value;
				AssertVerify(m_RlScAttrs[i].GetValue(&value));
				v1->SetValue(value);
			}
			else if(m_RlScAttrs[i].Type == RLSC_PRESTYPE_DOUBLE)
			{
				double value;
				AssertVerify(m_RlScAttrs[i].GetValue(&value));
				v1->SetValue(value);
			}
			else if(m_RlScAttrs[i].Type == RLSC_PRESTYPE_STRING)
			{
				char value[RGSC_PRESENCE_STRING_MAX_SIZE];
				AssertVerify(m_RlScAttrs[i].GetValue(value, sizeof(value)));
				v1->SetValue(value);
			}
			else if(m_RlScAttrs[i].Type == RLSC_PRESTYPE_INVALID)
			{
				v1->SetType(IPresenceAttributeV1::PRESTYPE_INVALID);
			}
			else
			{
				Assertf(false, "Unknown presence attribute type %d", (int)v1->GetType());
			}
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

///////////////////////////////////////////////////////////////////////////////
//  PostMessageTask
///////////////////////////////////////////////////////////////////////////////

class PostMessageTask : public PresenceManagerTaskBase
{
public:
	RL_TASK_DECL(PostMessageTask);

	enum State
	{
		STATE_INVALID = -1,
		STATE_CALL,
		STATE_CALLING,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	PostMessageTask();

	bool Configure(RgscPresenceManager* ctx,
				   const int localGamerindex,
				   const RockstarId* recipients,
				   const unsigned numRecipients,
				   IPresenceMessage* message,
				   const unsigned ttlSeconds);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	int m_LocalGamerindex;
	rlGamerHandle* m_Recipients;
	unsigned m_NumRecipients;
	unsigned m_TtlSeconds;
	PresenceMessage m_MessageBuffer;

private:
	bool Call();
	int m_State;
	netStatus m_MyStatus;
};

bool 
RgscPresenceManager::PostMessage(const int localGamerindex,
							 const RockstarId* recipients,
							 const unsigned numRecipients,
							 IPresenceMessage* message,
							 const unsigned ttlSeconds,
							 IAsyncStatus* status)
{
	// TODO: NS - this is a hack to get the game to send us a presence message
	//			  that arrived over the game's relay connection.
	if(recipients == NULL)
	{
		PresenceMessageV1* v1 = NULL;
		RGSC_HRESULT hr = message->QueryInterface(IID_IPresenceMessageV1, (void**) &v1);
		if(AssertVerify(SUCCEEDED(hr) && (v1 != NULL)))
		{
			RsonReader rr(v1->GetContents(), (unsigned)strlen(v1->GetContents()));

			if(rr.CheckName("ros.msgs"))
			{
				netRelayEventMessageReceived msg(v1->GetContents(), (unsigned)strlen(v1->GetContents()), NULL);
				rlPresence::OnRelayEvent(msg);
				return true;
			}
		}
		return false;
	}
	else
	{
		RLPC_CREATETASK_IASYNC(PostMessageTask,
							   localGamerindex,
							   recipients,
							   numRecipients,
							   message, 
							   ttlSeconds);
	}
}

PostMessageTask::PostMessageTask()
: m_State(STATE_INVALID)
, m_Recipients(NULL)
{

}

bool 
PostMessageTask::Configure(RgscPresenceManager* /*ctx*/,
						   const int localGamerindex,
						   const RockstarId* recipients,
						   const unsigned numRecipients,
						   IPresenceMessage* message,
						   const unsigned ttlSeconds)
{
	bool success = false;

	rtry
	{
		m_Recipients = RL_NEW_ARRAY(PostMessageTask, rlGamerHandle, numRecipients);
		rverify(m_Recipients, catchall, );

		for(unsigned i = 0; i < numRecipients; i++)
		{
			m_Recipients[i].ResetSc(recipients[i]);
		}

		m_LocalGamerindex = localGamerindex;
		m_NumRecipients = numRecipients;

		PresenceMessageV1* v1 = NULL;
		RGSC_HRESULT hr = message->QueryInterface(IID_IPresenceMessageV1, (void**) &v1);
		rverify(SUCCEEDED(hr) && (v1 != NULL), catchall, );

		m_MessageBuffer.SetContents(v1->GetContents());
		m_MessageBuffer.SetTimestamp(v1->GetTimestamp());

		m_TtlSeconds = ttlSeconds;

		m_State = STATE_INVALID;
		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void
PostMessageTask::Start()
{
	rlDebug2("PostMessageTask...");

	this->PresenceManagerTaskBase::Start();

	m_State = STATE_CALL;
}

void
PostMessageTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("PostMessageTask %s (%ums)", FinishString(finishType), GetElapsedTime());

	if(m_Recipients)
	{
		RL_DELETE_ARRAY(m_Recipients, m_NumRecipients);
		m_Recipients = NULL;
	}
	this->PresenceManagerTaskBase::Finish(finishType, resultCode);
}

void
PostMessageTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
	this->PresenceManagerTaskBase::DoCancel();
}

void
PostMessageTask::Update(const unsigned timeStep)
{
	this->PresenceManagerTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_CALL:
			if(Call())
			{
				m_State = STATE_CALLING;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_CALLING:
			if(m_MyStatus.Succeeded())
			{
				m_State = STATE_SUCCEEDED;
			}
			else if(!m_MyStatus.Pending())
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_SUCCEEDED:
			this->Finish(FINISH_SUCCEEDED);
			break;

		case STATE_FAILED:
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool 
PostMessageTask::Call()
{
	Assert(strlen(m_MessageBuffer.GetContents()) > 0);
    m_MyStatus.ForceSucceeded();
	return GetRgscConcreteInstance()->_GetPresenceManager()->PostMessageFireAndForget(RgscPresenceManager::PEER_TO_PEER, m_Recipients, m_NumRecipients, m_MessageBuffer.GetContents(), m_TtlSeconds);
}

///////////////////////////////////////////////////////////////////////////////
//  QueryTask
///////////////////////////////////////////////////////////////////////////////

class QueryTask : public PresenceManagerTaskBase
{
public:
	RL_TASK_DECL(QueryTask);

	enum State
	{
		STATE_INVALID = -1,
		STATE_CALL,
		STATE_CALLING,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	QueryTask();
	~QueryTask();

	bool Configure(RgscPresenceManager* ctx,
				   const int localGamerIndex,
				   const char* queryName,
				   const char* paramNameValueCsv,
				   const int offset,
				   const int count,
				   char* recordsBuf,
				   const unsigned sizeofRecordsBuf,
				   char** records,
				   unsigned* numRecordsRetrieved,
				   unsigned* numRecords);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	int m_LocalGamerIndex;
	char* m_QueryName;
	char* m_ParamNameValueCsv;
	int m_Offset;
	int m_Count;
	char* m_RecordsBuf;
	unsigned m_SizeofRecordsBuf;
	char** m_Records;
	unsigned* m_NumRecordsRetrieved;
	unsigned* m_NumRecords;

private:
	bool Call();
	int m_State;
	netStatus m_MyStatus;
};

bool 
RgscPresenceManager::Query(/*const int localGamerIndex,*/
					   const char* queryName,
					   const char* paramNameValueCsv,
					   const int offset,
					   const int count,
					   char* recordsBuf,
					   const unsigned sizeofRecordsBuf,
					   char** records,
						unsigned* numRecordsRetrieved,
					   unsigned* numRecords,
					   IAsyncStatus* status)
{
	const int localGamerIndex = 0;
	RLPC_CREATETASK_IASYNC(QueryTask,
						   localGamerIndex,
						   queryName,
						   paramNameValueCsv,
						   offset,
						   count,
						   recordsBuf,
						   sizeofRecordsBuf,
						   records,
						   numRecordsRetrieved,
						   numRecords);
}

QueryTask::QueryTask()
: m_State(STATE_INVALID)
, m_LocalGamerIndex(-1)
, m_QueryName(NULL)
, m_ParamNameValueCsv(NULL)
{

}

QueryTask::~QueryTask()
{
	if(m_QueryName)
	{
		StringFree(m_QueryName);
		m_QueryName = NULL;
	}

	if(m_ParamNameValueCsv)
	{
		StringFree(m_ParamNameValueCsv);
		m_ParamNameValueCsv = NULL;
	}
}

bool 
QueryTask::Configure(RgscPresenceManager* /*ctx*/,
					 const int localGamerIndex,
					 const char* queryName,
					 const char* paramNameValueCsv,
					 const int offset,
					 const int count,
					 char* recordsBuf,
					 const unsigned sizeofRecordsBuf,
					 char** records,
					 unsigned* numRecordsRetrieved,
					 unsigned* numRecords)
{
	m_LocalGamerIndex = localGamerIndex;
	m_QueryName = StringDuplicate(queryName);
	m_ParamNameValueCsv = StringDuplicate(paramNameValueCsv);
	m_Offset = offset;
	m_Count = count;
	m_RecordsBuf = recordsBuf;
	m_SizeofRecordsBuf = sizeofRecordsBuf;
	m_Records = records;
	m_NumRecordsRetrieved = numRecordsRetrieved;
	m_NumRecords = numRecords;

	m_State = STATE_INVALID;

	return true;
}

void
QueryTask::Start()
{
	rlDebug2("QueryTask...");

	this->PresenceManagerTaskBase::Start();

	m_State = STATE_CALL;
}

void
QueryTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("QueryTask %s (%ums)", FinishString(finishType), GetElapsedTime());
	this->PresenceManagerTaskBase::Finish(finishType, resultCode);
}

void
QueryTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
	this->PresenceManagerTaskBase::DoCancel();
}

void
QueryTask::Update(const unsigned timeStep)
{
	this->PresenceManagerTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_CALL:
			if(Call())
			{
				m_State = STATE_CALLING;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_CALLING:
			if(m_MyStatus.Succeeded())
			{
				m_State = STATE_SUCCEEDED;
			}
			else if(!m_MyStatus.Pending())
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_SUCCEEDED:
			this->Finish(FINISH_SUCCEEDED);
			break;

		case STATE_FAILED:
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool 
QueryTask::Call()
{
	return rlPresence::Query(m_LocalGamerIndex,
							   m_QueryName,
							   m_ParamNameValueCsv,
							   m_Offset,
							   m_Count,
							   m_RecordsBuf,
							   m_SizeofRecordsBuf,
							   m_Records,
							   m_NumRecordsRetrieved,
							   m_NumRecords,
							   &m_MyStatus);
}

///////////////////////////////////////////////////////////////////////////////
//  QueryCountTask
///////////////////////////////////////////////////////////////////////////////

class QueryCountTask : public PresenceManagerTaskBase
{
public:
	RL_TASK_DECL(QueryCountTask);

	enum State
	{
		STATE_INVALID = -1,
		STATE_CALL,
		STATE_CALLING,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	QueryCountTask();
	~QueryCountTask();

	bool Configure(RgscPresenceManager* ctx,
				   const int localGamerIndex,
				   const char* queryName,
				   const char* paramNameValueCsv,
				   unsigned* count);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	int m_LocalGamerIndex;
	char* m_QueryName;
	char* m_ParamNameValueCsv;
	unsigned* m_Count;

private:
	bool Call();
	int m_State;
	netStatus m_MyStatus;
};

bool 
RgscPresenceManager::QueryCount(/*const int localGamerIndex,*/
							const char* queryName,
							const char* paramNameValueCsv,
							unsigned* count,
							IAsyncStatus* status)
{
	const int localGamerIndex = 0;
	RLPC_CREATETASK_IASYNC(QueryCountTask,
						   localGamerIndex,
						   queryName,
						   paramNameValueCsv,
						   count);
}

QueryCountTask::QueryCountTask()
: m_State(STATE_INVALID)
, m_LocalGamerIndex(-1)
, m_QueryName(NULL)
, m_ParamNameValueCsv(NULL)
{

}

QueryCountTask::~QueryCountTask()
{
	if(m_QueryName)
	{
		StringFree(m_QueryName);
		m_QueryName = NULL;
	}

	if(m_ParamNameValueCsv)
	{
		StringFree(m_ParamNameValueCsv);
		m_ParamNameValueCsv = NULL;
	}
}

bool 
QueryCountTask::Configure(RgscPresenceManager* /*ctx*/,
						  const int localGamerIndex,
						  const char* queryName,
						  const char* paramNameValueCsv,
						  unsigned* count)
{
	m_LocalGamerIndex = localGamerIndex;
	m_QueryName = StringDuplicate(queryName);
	m_ParamNameValueCsv = StringDuplicate(paramNameValueCsv);
	m_Count = count;

	m_State = STATE_INVALID;

	return true;
}

void
QueryCountTask::Start()
{
	rlDebug2("QueryCountTask...");

	this->PresenceManagerTaskBase::Start();

	m_State = STATE_CALL;
}

void
QueryCountTask::Finish(const FinishType finishType, const int resultCode)
{
	rlDebug2("QueryCountTask %s (%ums)", FinishString(finishType), GetElapsedTime());
	this->PresenceManagerTaskBase::Finish(finishType, resultCode);
}

void
QueryCountTask::DoCancel()
{
	RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
	this->PresenceManagerTaskBase::DoCancel();
}

void
QueryCountTask::Update(const unsigned timeStep)
{
	this->PresenceManagerTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	do 
	{
		switch(m_State)
		{
		case STATE_CALL:
			if(Call())
			{
				m_State = STATE_CALLING;
			}
			else
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_CALLING:
			if(m_MyStatus.Succeeded())
			{
				m_State = STATE_SUCCEEDED;
			}
			else if(!m_MyStatus.Pending())
			{
				m_State = STATE_FAILED;
			}
			break;

		case STATE_SUCCEEDED:
			this->Finish(FINISH_SUCCEEDED);
			break;

		case STATE_FAILED:
			this->Finish(FINISH_FAILED, m_MyStatus.GetResultCode());
			break;
		}
	}
	while(!m_MyStatus.Pending() && this->IsActive());
}

bool 
QueryCountTask::Call()
{
	return rlPresence::QueryCount(m_LocalGamerIndex,
									m_QueryName,
									m_ParamNameValueCsv,
									m_Count,
									&m_MyStatus);
}

} //namespace rgsc

#endif // RSG_PC
