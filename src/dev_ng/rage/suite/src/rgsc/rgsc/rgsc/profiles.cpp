// 
// rgsc/profiles.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "file/file_config.h"
#include "profiles.h"
#include "profiles_tasks.h"

// rgsc
#include "rgsc.h"
#include "json.h"
#include "rgsc_ui/script.h"
#include "../diag/output.h"

// rage
#include "data/base64.h"
#include "data/bitbuffer.h"
#include "data/pbkdf2.h"
#include "diag/channel.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "file/device.h"
#include "math/random.h"
#include "net/nethardware.h"
#include "net/netnonce.h"
#include "parser/manager.h"
#include "string/stringhash.h"
#include "system/memops.h"
#include "system/timer.h"
#include "rline/socialclub/rlsocialclub.h"
#include "rline/ros/rlros.h"
#include "rline/ros/rlroscredtasks.h"
#include "rline/rldiag.h"
#include "string/stringutil.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <shlobj.h>
#include <time.h>
#pragma warning(pop)

using namespace rage;

CompileTimeAssert(RGSC_MAX_NAME_CHARS == RLSC_MAX_NICKNAME_CHARS);
CompileTimeAssert(RGSC_ROS_MAX_TICKET_LEN == RLROS_MAX_TICKET_SIZE);
CompileTimeAssert(RGSC_MAX_EMAIL_CHARS == RLSC_MAX_EMAIL_CHARS);
CompileTimeAssert(RGSC_MAX_PASSWORD512_CHARS == RLSC_MAX_PASSWORD_CHARS);
CompileTimeAssert(RGSC_MAX_AUTHTOKEN_CHARS >= RLSC_MAX_SCAUTHTOKEN_CHARS);
CompileTimeAssert(RGSC_MAX_AVATAR_URL_CHARS == RLSC_MAX_AVATAR_URL_CHARS);
CompileTimeAssert(RGSC_PROFILE_UNIQUE_KEY_LEN == UniqueKey::SIZE_OF_KEY_IN_BYTES);
CompileTimeAssert(RGSC_MAX_MACHINETOKEN_CHARS == RLSC_MAX_MACHINETOKEN_CHARS);

// This value cannot change as it will impact legacy structures
CompileTimeAssert(RGSC_MAX_PASSWORD_LEGACY_CHARS == 31);

namespace rgsc
{

extern sysMemAllocator* g_RgscAllocator;

#if __RAGE_DEPENDENCY
RAGE_DEFINE_SUBCHANNEL(rgsc_dbg, profile);
#undef __rage_channel
#define __rage_channel rgsc_dbg_profile
#endif

PARAM(scMaxLocalProfiles, "Custom override how many max profiles can be enumerated on disk");

static bool s_IsInitialized = false;
static mthRandom sm_Random;

////////////////////////////////////////////////////////////////////////////////
// Utility functions used throughout this file
////////////////////////////////////////////////////////////////////////////////
unsigned 
WeakHash(const void *buf, unsigned const sizeofBuf)
{
    return atDataHash((const char*)buf, (int)sizeofBuf);
}

unsigned 
WeakHash(const char *str)
{
    return atStringHash(str);
}

bool CanSteamSingleSignOn()
{
	if (!GetRgscConcreteInstance()->IsSteam())
		return false;

	if (GetRgscConcreteInstance()->GetTitleId()->GetSteamId() == 0)
		return false;

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// RgscProfileAutoSignInData
////////////////////////////////////////////////////////////////////////////////

RgscProfileAutoSignInData::RgscProfileAutoSignInData()
{
    Clear();
}

RgscProfileAutoSignInData::RgscProfileAutoSignInData(unsigned autoSignInFolderId)
{
    Clear();

    m_AutoSignInFolderId = autoSignInFolderId;
}

RgscProfileAutoSignInData::~RgscProfileAutoSignInData()
{
    Clear();
}

bool
RgscProfileAutoSignInData::IsValid() const
{
    return true;
}

void 
RgscProfileAutoSignInData::Clear()
{
    m_Version = RGSC_PROFILE_AUTOSIGNIN_DATA_VERSION;
    m_AutoSignInFolderId = 0;
}

unsigned
RgscProfileAutoSignInData::GetVersion() const
{
    return m_Version;
}

unsigned
RgscProfileAutoSignInData::GetAutoSignInFolderId() const
{
    return m_AutoSignInFolderId;
}

bool
RgscProfileAutoSignInData::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
    datExportBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);

    bool success = false;

    rtry
    {
        rverify(buf, catchall, );
        rverify(sizeofBuf >= MAX_BYTE_SIZEOF_PAYLOAD, catchall, );

        rverify(bb.WriteUns(m_Version, sizeof(m_Version) << 3), catchall, );
        rverify(bb.WriteUns(m_AutoSignInFolderId, sizeof(m_AutoSignInFolderId) << 3), catchall, );
        
        success = true;        
    }
    rcatchall
    {
    }

    if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

    return success;
}

bool
RgscProfileAutoSignInData::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

    bool success = false;

    Clear();

    rtry
    {
        rverify(buf, catchall, );

        rverify(bb.ReadUns(m_Version, sizeof(m_Version) << 3), catchall, );
		rverify(m_Version == RGSC_PROFILE_AUTOSIGNIN_DATA_VERSION, catchall, );
        rverify(bb.ReadUns(m_AutoSignInFolderId, sizeof(m_AutoSignInFolderId) << 3), catchall, );

        success = true;        
    }
    rcatchall
    {
    }

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}

    return success;
}

////////////////////////////////////////////////////////////////////////////////
// RgscProfileSignInTransferData
////////////////////////////////////////////////////////////////////////////////

RgscProfileSignInTransferData::RgscProfileSignInTransferData()
{
	Clear();
}

RgscProfileSignInTransferData::RgscProfileSignInTransferData(unsigned autoSignInFolderId, const char* login, const char* scTicket, const char* scAuthToken)
{
	Clear();

	m_AutoSignInFolderId = autoSignInFolderId;
	safecpy(m_Login, login);
	safecpy(m_ScTicket, scTicket);
	safecpy(m_ScAuthToken, scAuthToken);
}

RgscProfileSignInTransferData::~RgscProfileSignInTransferData()
{
	Clear();
}

bool
RgscProfileSignInTransferData::IsValid() const
{
	return true;
}

void 
RgscProfileSignInTransferData::Clear()
{
	m_Version = RGSC_PROFILE_SIGNIN_TRANSFER_DATA_VERSION;
	m_AutoSignInFolderId = 0;
	sysMemSet(m_Login, 0, sizeof(m_Login));
	sysMemSet(m_ScTicket, 0, sizeof(m_ScTicket));
	sysMemSet(m_ScAuthToken, 0, sizeof(m_ScAuthToken));
}

unsigned
RgscProfileSignInTransferData::GetVersion() const
{
	return m_Version;
}

unsigned
RgscProfileSignInTransferData::GetAutoSignInFolderId() const
{
	return m_AutoSignInFolderId;
}

const char*
RgscProfileSignInTransferData::GetLogin() const
{
	return m_Login;
}

const char*
RgscProfileSignInTransferData::GetScTicket() const
{
	return m_ScTicket;
}

const char*
RgscProfileSignInTransferData::GetScAuthToken() const
{
	return m_ScAuthToken;
}

bool
RgscProfileSignInTransferData::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
    sysMemSet(buf, 0, sizeofBuf);
    datExportBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);

    bool success = false;

    rtry
    {
        rverify(buf, catchall, );
        rverify(sizeofBuf >= MAX_BYTE_SIZEOF_PAYLOAD, catchall, );

        unsigned plaintextSize = 0;
        unsigned outerBlockSize = 0;
        const u8 pad[16] = {0};

        // format
        {
            // plaintext
            {
                // serialize data here
                {
                    rverify(bb.WriteUns(m_Version, sizeof(m_Version) << 3), catchall, );
                }
                plaintextSize = (unsigned)bb.GetBitLength();
                rverify((plaintextSize % 8) == 0, catchall, );
            }

            // outer block is encrypted with the common key
            {
                unsigned outerBlockStart = bb.GetBitLength();

                // serialize data here
                {
					rverify(bb.WriteUns(m_AutoSignInFolderId, sizeof(m_AutoSignInFolderId) << 3), catchall, );
					rverify(bb.WriteBytes(m_Login, sizeof(m_Login)), catchall, );
                    rverify(bb.WriteBytes(m_ScAuthToken, sizeof(m_ScAuthToken)), catchall, );
					rverify(bb.WriteBytes(m_ScTicket, sizeof(m_ScTicket)), catchall, );

					// NOTE: reserving space for the future
					const u8 reserve[RESERVED_SPACE] = {0};
					rverify(bb.WriteBytes((void*)reserve, sizeof(reserve)), catchall, );
                }

                outerBlockSize = (unsigned)bb.GetBitLength() - outerBlockStart;

                // pad outer block to multiple of 16 bytes
                unsigned paddedSize = (outerBlockSize + (16 << 3) - 1) & ~((16 << 3) - 1);
                Assert((paddedSize - outerBlockSize) < (16 << 3));
                rverify(bb.WriteBits(pad, paddedSize - outerBlockSize, 0), catchall, );
                Assert(paddedSize == ((unsigned)bb.GetBitLength() - outerBlockStart));

                outerBlockSize = (unsigned)bb.GetBitLength() - outerBlockStart;

                rverify((outerBlockSize % (16 << 3)) == 0, catchall, );

                u8 checksum[datProtectSignatureSize];
                datProtectContext context(true, NULL, 0);
                datProtect(&((u8*)buf)[outerBlockStart >> 3], outerBlockSize >> 3, context, checksum);
                rverify(bb.WriteBytes(checksum, sizeof(checksum)), catchall, );

                rverify((bb.GetNumBitsWritten() % 8) == 0, catchall, );
            }
        }

        success = true;
    }
    rcatchall
    {
    }

    if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

    return success;
}

bool
RgscProfileSignInTransferData::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

    bool success = false;

    Clear();

    rtry
    {
        rverify(buf, catchall, );

        unsigned plaintextSize = 0;
        unsigned outerBlockSize = 0;

        // format
        {
            // plaintext
            {
                // deserialize data here
                {
                    rverify(bb.ReadUns(m_Version, sizeof(m_Version) << 3), catchall, );
					rcheck(m_Version == RGSC_PROFILE_SIGNIN_TRANSFER_DATA_VERSION, catchall, );
                }
                plaintextSize = (unsigned)bb.GetCursorPos();
                rverify((plaintextSize % 8) == 0, catchall, );
            }

            {
                unsigned outerBlockStart = bb.GetCursorPos();
                outerBlockSize = (sizeofBuf << 3) - (datProtectSignatureSize << 3) - outerBlockStart;

                // decrypt outer block with common key
                u8 signature[datProtectSignatureSize];

                rverify(bb.SetCursorPos((sizeofBuf - sizeof(signature)) << 3), catchall, );
                rverify(bb.ReadBytes(signature, sizeof(signature)), catchall, );

                datProtectContext context(true, NULL, 0);
                bool valid = datUnprotect(&((u8*)buf)[outerBlockStart >> 3], outerBlockSize >> 3, context, signature);
                rverify(valid, catchall, rlError("profile is corrupt or has been modified by an untrusted source."));

                bb.SetCursorPos(outerBlockStart);

                // deserialize data here
                {
					rverify(bb.ReadUns(m_AutoSignInFolderId, sizeof(m_AutoSignInFolderId) << 3), catchall, );
					rverify(bb.ReadBytes(m_Login, sizeof(m_Login)), catchall, );
					rverify(bb.ReadBytes(m_ScAuthToken, sizeof(m_ScAuthToken)), catchall, );
					rverify(bb.ReadBytes(m_ScTicket, sizeof(m_ScTicket)), catchall, );

					// NOTE: reserving space for the future
					const u8 reserve[RESERVED_SPACE] = {0};
					rverify(bb.ReadBytes((void*)reserve, sizeof(reserve)), catchall, );
                }

                // pad outer block to multiple of 16 bytes
                outerBlockSize = (unsigned)bb.GetCursorPos() - outerBlockStart;

                unsigned paddedSize = (outerBlockSize + (16 << 3) - 1) & ~((16 << 3) - 1);
                Assert((paddedSize - outerBlockSize) < (16 << 3));
                u8 zero[16] = {0};
                u8 pad[16] = {0};
                rverify(bb.ReadBits(pad, paddedSize - outerBlockSize, 0), catchall, );
                rverify(memcmp(zero, pad, sizeof(pad)) == 0, catchall, );

                Assert(paddedSize == ((unsigned)bb.GetCursorPos() - outerBlockStart));
            }
        }

        success = true;        
    }
    rcatchall
    {
        Clear();
    }

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}

    return success;
}

////////////////////////////////////////////////////////////////////////////////
// UniqueKey
////////////////////////////////////////////////////////////////////////////////
void UniqueKey::Generate()
{
	u32* p = (u32*)m_UniqueKey;

	mthRandom r(sysTimer::GetSystemMsTime());

	for(unsigned i = 0; i < (sizeof(m_UniqueKey) / sizeof(u32)); i++, p++)
	{
		*p = (u32)r.GetInt();
	}

	CompileTimeAssert(sizeof(m_UniqueKey) >= sizeof(u64));

	if(AssertVerify(sizeof(m_UniqueKey) >= sizeof(u64)))
	{
		u64 uuid64 = 0;
		if(rlCreateUUID(&uuid64))
		{
			memcpy(m_UniqueKey, &uuid64, sizeof(uuid64));
		}
	}
}

void UniqueKey::GenerateFromRockstarId(RockstarId rockstarId)
{
	// WARNING: never change how this function generates the key from a rockstar id.
	//			existing profiles rely on this being generated this way.
	const unsigned char salt[32] = {0x8c, 0x9a, 0x88, 0x5a, 0x2e, 0x7c, 0x13, 0xff,
									0xb7, 0x4c, 0x3d, 0x60, 0x3e, 0x82, 0xa5, 0x4c,
									0x9f, 0xca, 0x51, 0x78, 0xb4, 0xce, 0xe2, 0xca,
									0xbb, 0xe8, 0x72, 0x44, 0x38, 0xf, 0xe7, 0x24}; 

	pkcs5_pbkdf2((const char*)&rockstarId, sizeof(rockstarId),
				 (const char*)salt, sizeof(salt),
				 m_UniqueKey, sizeof(m_UniqueKey),
				 2000);
}

////////////////////////////////////////////////////////////////////////////////
// RgscProfile
////////////////////////////////////////////////////////////////////////////////
RgscProfile::RgscProfile()
{
    Clear();
}

RgscProfile::RgscProfile(unsigned folderId,
                         RockstarId profileId,
						 const UniqueKey& key,
                         const char* name,
                         const char* login,
                         const char* password,
                         bool saveLogin,
                         bool savePassword,
						 bool saveScAuthToken,
                         const char* gamerPic,
                         bool offlineProfile,
						 unsigned profileNumber,
						 u64 steamId,
						 const char* scAuthToken,
						 const char* machineToken)
{
    Clear();

    if(AssertVerify(folderId > 0) &&
       AssertVerify(profileId != InvalidRockstarId) &&
	   AssertVerify(key.IsSet()) &&
	   AssertVerify(name && (name[0] != '\0')) &&
	   AssertVerify((login && (login[0] != '\0')) || GetRgscConcreteInstance()->IsSteam()) &&
	   AssertVerify(!savePassword || saveLogin))
    {
        m_FolderId = folderId;
        m_ProfileId = profileId;
		m_UniqueKey = key;
        safecpy(m_Name, name);
        safecpy(m_GamerPic, gamerPic);
		safecpy(m_MachineToken, machineToken);

        // we save the user's credentials if the user chose to have them saved. 
        // A hash of the credentials is also saved so we can perform local sign-in and
        // allow the user to play the game even if we can't connect to the backend.

        if(saveLogin)
        {
            safecpy(m_Login, login);
            if(savePassword)
            {
                safecpy(m_Password, password);
            }

			if (saveScAuthToken)
			{
				safecpy(m_ScAuthToken, scAuthToken);
			}
        }

        Sha1 sha1_login;
        sha1_login.Update((u8*)login, (u32)strlen(login));
        sha1_login.Final(m_LoginHash);

        Sha1 sha1_password;
        sha1_password.Update((u8*)password, (u32)strlen(password));
        sha1_password.Final(m_PasswordHash);

        m_OfflineProfile = offlineProfile;
		m_ProfileNumber = profileNumber;
		m_SteamId = steamId;
    }
}

RgscProfile::~RgscProfile()
{
    Clear();
}

bool
RgscProfile::IsValid() const
{
    return (m_ProfileId != InvalidRockstarId) && (m_Name[0] != '\0');
}

void 
RgscProfile::Clear()
{
    m_Version = RGSC_PROFILE_VERSION_UNKNOWN;
	m_ExportVersion = RGSC_PROFILE_VERSION_2;
    m_ProfileId = InvalidRockstarId;
    m_FolderId = 0;
    m_OfflineProfile = false;
	m_ProfileNumber = 0;
	m_SteamId = 0;
    sysMemSet(m_Name, 0, sizeof(m_Name));
    sysMemSet(m_Login, 0, sizeof(m_Login));
    sysMemSet(m_Password, 0, sizeof(m_Password));
	sysMemSet(m_ScAuthToken, 0, sizeof(m_ScAuthToken));
	sysMemSet(m_MachineToken, 0, sizeof(m_MachineToken));
    sysMemSet(m_LoginHash, 0, sizeof(m_LoginHash));
    sysMemSet(m_PasswordHash, 0, sizeof(m_PasswordHash));
    sysMemSet(m_GamerPic, 0, sizeof(m_GamerPic));

    m_LastSignInTime = 0;
    m_IsFullyDecrypted = false;
}

unsigned
RgscProfile::GetVersion() const
{
    return m_Version;
}

unsigned 
RgscProfile::GetFolderId() const
{
    return m_FolderId;
}

const UniqueKey& 
RgscProfile::GetUniqueKey() const
{
	return m_UniqueKey;
}

RockstarId
RgscProfile::GetProfileId() const
{
    return m_ProfileId;
}

const char*
RgscProfile::GetName() const
{
    return m_Name;
}

const char*
RgscProfile::GetGamerPicName() const
{
    return m_GamerPic;
}

bool RgscProfile::IsMachineTokenSaved() const
{
	return m_MachineToken[0] != '\0';
}

const char*
RgscProfile::GetMachineToken() const
{
	return m_MachineToken;
}

#if __RAGE_DEPENDENCY
grcImage*
RgscProfile::GetGamerPic(bool largeVersion) const
{
    return g_rlPc.GetGamerPicManager().GetGamerPic(m_GamerPic, largeVersion);
}
#endif

u8*
RgscProfile::GetGamerPicRaw(IGamerPicManager::AvatarSize avatarSize, unsigned int* sizeInBytes) const
{
	return GetRgscConcreteInstance()->_GetGamerPicManager()->GetGamerPicRaw(m_GamerPic, avatarSize, sizeInBytes);
}

bool
RgscProfile::IsOfflineProfile() const
{
    return m_OfflineProfile;
}

unsigned
RgscProfile::GetProfileNumber() const
{
	return m_ProfileNumber;
}

bool
RgscProfile::IsLoginSaved() const
{
    return m_Login[0] != '\0';
}

bool
RgscProfile::IsPasswordSaved() const
{
    return m_Password[0] != '\0';
}

const char*
RgscProfile::GetSavedLogin() const
{
    Assert(IsLoginSaved());
    return m_Login;
}

const char*
RgscProfile::GetSavedPassword() const
{
    Assert(IsPasswordSaved());
    return m_Password;
}

bool RgscProfile::IsAuthTokenSaved() const
{
	return (m_ScAuthToken[0] != '\0');
}

const char* RgscProfile::GetSavedAuthToken() const
{
	Assert(IsAuthTokenSaved());
	return m_ScAuthToken;
}

bool
RgscProfile::IsFullyDecrypted() const
{
    return m_IsFullyDecrypted;
}

u64
RgscProfile::GetSteamId() const
{
	return m_SteamId;
}

time_t
RgscProfile::GetLastSignInTime() const
{
    return m_LastSignInTime;
}

bool 
RgscProfile::ToJson(JSONNODE *p, const char* transitionEmail, const char* transitionTicket, const char* transitionScAuthToken) const
{
	if(!AssertVerify(p))
	{
		return false;
	}

	time_t secs = this->GetLastSignInTime();
// 	struct tm* t = localtime(&secs);
// 	char szTime[32];
// 	formatf(szTime, "%04u-%02u-%02uT%02u:%02u:%02u", t->tm_year + 1900, t->tm_mon + 1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);

	char szTime[64];
	formatf(szTime, "%"I64FMT"u", secs);

	char szRockstarId[64];
	formatf(szRockstarId, "%"I64FMT"d", this->GetProfileId());
	//@@: location RLPCPROFILE_TOJSON
	json_push_back(p, json_new_a("RockstarId", szRockstarId));
	json_push_back(p, json_new_a("Nickname", this->GetName()));
	json_push_back(p, json_new_a("LastSignInTime", szTime));
	json_push_back(p, json_new_b("AutoSignIn", GetRgscConcreteInstance()->_GetProfileManager()->IsAutoSignInProfile(*this)));
	json_push_back(p, json_new_b("Local", IsOfflineProfile()));

	if(IsOfflineProfile())
	{
		json_push_back(p, json_new_i("ProfileNumber", GetProfileNumber()));
	}
	else
	{
		json_push_back(p, json_new_b("SaveEmail", this->IsLoginSaved()));
		json_push_back(p, json_new_b("SavePassword", this->IsPasswordSaved()));

		if(this->IsLoginSaved())
		{
			json_push_back(p, json_new_a("Email", this->GetSavedLogin()));
		}
		else if(transitionEmail)
		{
			json_push_back(p, json_new_a("Email", transitionEmail));
		}

		if(this->IsPasswordSaved())
		{
			json_push_back(p, json_new_a("Password", this->GetSavedPassword()));
		}

		if(this->IsAuthTokenSaved())
		{
			json_push_back(p, json_new_a("ScAuthToken", this->GetSavedAuthToken()));
		}
		else if(transitionScAuthToken)
		{
			json_push_back(p, json_new_a("ScAuthToken", transitionScAuthToken));
		}

		if (transitionTicket)
		{
			json_push_back(p, json_new_a("Ticket", transitionTicket));
		}
		else
		{
			json_push_back(p, json_new_a("Ticket", ""));
		}
	}

	json_push_back(p, json_new_a("AvatarUrl", this->GetGamerPicName()));
	json_push_back(p, json_new_a("RememberedMachineToken", this->GetMachineToken()));

	return true;
}

bool
RgscProfile::Export(const char* login, const char* password, const char* scAuthToken, void* buf, const unsigned sizeofBuf, unsigned* size) const
{
    sysMemSet(buf, 0, sizeofBuf);
    datExportBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);

    bool success = false;

    rtry
    {
		rverify(m_ExportVersion == RGSC_PROFILE_VERSION_1 || m_ExportVersion == RGSC_PROFILE_VERSION_2, catchall, );

		if ( !GetRgscConcreteInstance()->IsSteam() )
		{
			rverify((login != NULL) && (login[0] != '\0'), catchall,);
		}

        rverify(buf, catchall, );
        rverify(sizeofBuf >= MAX_BYTE_SIZEOF_PAYLOAD, catchall, );

        unsigned plaintextSize = 0;

        // format
        {
            // plaintext
            {
                // serialize data here
                {
                    rverify(bb.WriteUns(m_ExportVersion, sizeof(m_ExportVersion) << 3), catchall, );
                }

                plaintextSize = (unsigned)bb.GetBitLength();
                rverify((plaintextSize % 8) == 0, catchall, );

				if (m_ExportVersion == RGSC_PROFILE_VERSION_1)
				{
					ExportV1(bb, login, password, buf, sizeofBuf, size);
				}
				else if (m_ExportVersion == RGSC_PROFILE_VERSION_2)
				{
					ExportV2(bb, login, password, scAuthToken, buf, sizeofBuf, size);
				}
            }
        }

        success = true;
    }
    rcatchall
    {
    }

    if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

    return success;
}

// PURPOSE
//  Implements the V1 interface of Export
bool RgscProfile::ExportV1(datExportBuffer& bb, const char* login, const char* password, void* buf, const unsigned sizeofBuf, unsigned* size) const
{
	rtry
	{
		rverify((login != NULL) && (login[0] != '\0'), catchall,);
		if ( !GetRgscConcreteInstance()->IsSteam() )
		{
			rverify((password != NULL) && (password[0] != '\0'), catchall, );
		}

		unsigned outerBlockSize = 0;
		unsigned innerBlockSize = 0;
		const u8 pad[16] = {0};

		// outer block is encrypted with the common key, and is therefore less secure than the 2nd block below
		// it contains only the data that we need to be able to access without knowing the user's password
		{
			unsigned outerBlockStart = bb.GetBitLength();

			// serialize data here
			{
				rverify(m_UniqueKey.IsSet(), catchall, 0);
				rverify(bb.WriteBytes(m_UniqueKey.GetKey(), UniqueKey::SIZE_OF_KEY_IN_BYTES), catchall, );

				rverify(bb.WriteInt(m_ProfileId, sizeof(m_ProfileId) << 3), catchall, );

				rverify(bb.WriteStr(m_Name, sizeof(m_Name)), catchall, );

				// the login and/or password may be empty strings depending on whether the user chose to have them saved
				rverify(bb.WriteStr(m_Login, sizeof(m_Login)), catchall, );
				rverify(bb.WriteStr(m_Password, sizeof(m_Password)), catchall, );

				rverify(bb.WriteBool(m_OfflineProfile), catchall, );
				rverify(bb.WriteStr(m_GamerPic, sizeof(m_GamerPic)), catchall, );
				rverify(bb.WriteUns(m_ProfileNumber, sizeof(m_ProfileNumber) << 3), catchall, );

				time_t curTime = time(NULL);
				rverify(bb.WriteBytes((void*)&curTime, sizeof(curTime)), catchall, );

				// RESERVED SPACE - PLEASE READ BEFORE EDITING
				//  RgscProfile is designed with 128 bytes of reserved space. If you add a variable to import/export,
				//	it must be used within this reserved space
				// Steam ID - u64 - 8 bytes
				rverify(bb.WriteUns(m_SteamId, sizeof(m_SteamId) << 3), catchall, );
				const u8 reserve[OUTER_RESERVED_SPACE_V1] = {0};
				rverify(bb.WriteBytes((void*)reserve, sizeof(reserve)), catchall, );
			}

			outerBlockSize = (unsigned)bb.GetBitLength() - outerBlockStart;

			// pad outer block to multiple of 16 bytes
			unsigned paddedSize = (outerBlockSize + (16 << 3) - 1) & ~((16 << 3) - 1);
			Assert((paddedSize - outerBlockSize) < (16 << 3));
			rverify(bb.WriteBits(pad, paddedSize - outerBlockSize, 0), catchall, );
			Assert(paddedSize == ((unsigned)bb.GetBitLength() - outerBlockStart));

			outerBlockSize = (unsigned)bb.GetBitLength() - outerBlockStart;

			rverify((outerBlockSize % (16 << 3)) == 0, catchall, );

			// inner block is encrypted with a key generated from the user's login and password
			// and then encrypted with the common key
			{
				unsigned innerBlockStart = bb.GetBitLength();

				// serialize data here
				{
					rverify(bb.WriteBytes(m_LoginHash, sizeof(m_LoginHash)), catchall, );
					rverify(bb.WriteBytes(m_PasswordHash, sizeof(m_PasswordHash)), catchall, );

					// NOTE: reserving space for the future
					const u8 reserve[INNER_RESERVED_SPACE_V1] = {0};
					rverify(bb.WriteBytes((void*)reserve, sizeof(reserve)), catchall, );
				}

				// pad inner block to multiple of 16 bytes
				innerBlockSize = (unsigned)bb.GetBitLength() - innerBlockStart;

				unsigned paddedSize = (innerBlockSize + (16 << 3) - 1) & ~((16 << 3) - 1);
				Assert((paddedSize - innerBlockSize) < (16 << 3));
				rverify(bb.WriteBits(pad, paddedSize - innerBlockSize, 0), catchall, );
				Assert(paddedSize == ((unsigned)bb.GetBitLength() - innerBlockStart));

				innerBlockSize = (unsigned)bb.GetBitLength() - innerBlockStart;

				rverify((innerBlockSize % (16 << 3)) == 0, catchall, );
				rverify((bb.GetBitLength() % 8) == 0, catchall, );

				// generate private data context from login, password, and profileId
				u8 digest[Sha1::SHA1_DIGEST_LENGTH];
				Sha1 sha1((u8*)&m_ProfileId, sizeof(m_ProfileId));
				sha1.Update((u8*)login, (u32)strlen(login));
				sha1.Update((u8*)password, (u32)strlen(password));
				sha1.Final(digest);

				// sign and encrypt using private data
				u8 checksum[datProtectSignatureSize];
				datProtectContext context(true, digest, sizeof(digest));
				datProtect(&((u8*)buf)[innerBlockStart >> 3], innerBlockSize >> 3, context, checksum);
				rverify(bb.WriteBytes(checksum, sizeof(checksum)), catchall, );
			}

			rverify((bb.GetBitLength() % 8) == 0, catchall, );

			outerBlockSize = (unsigned)bb.GetBitLength() - outerBlockStart;

			u8 checksum[datProtectSignatureSize];
			datProtectContext context(true, NULL, 0);
			datProtect(&((u8*)buf)[outerBlockStart >> 3], outerBlockSize >> 3, context, checksum);
			rverify(bb.WriteBytes(checksum, sizeof(checksum)), catchall, );

			rverify((bb.GetNumBitsWritten() % 8) == 0, catchall, );
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

// PURPOSE
//  Implements the V2 interface of Export
bool RgscProfile::ExportV2(datExportBuffer& bb, const char* login, const char* password, const char* scAuthToken, void* buf, const unsigned sizeofBuf, unsigned* size) const
{
	rtry
	{
		unsigned outerBlockSize = 0;
		unsigned innerBlockSize = 0;
		const u8 pad[16] = {0};

		// outer block is encrypted with the common key, and is therefore less secure than the 2nd block below
		// it contains only the data that we need to be able to access without knowing the user's password
		{
			unsigned outerBlockStart = bb.GetBitLength();

			// serialize data here
			{
				rverify(m_UniqueKey.IsSet(), catchall, 0);
				rverify(bb.WriteBytes(m_UniqueKey.GetKey(), UniqueKey::SIZE_OF_KEY_IN_BYTES), catchall, );

				rverify(bb.WriteInt(m_ProfileId, sizeof(m_ProfileId) << 3), catchall, );

				rverify(bb.WriteStr(m_Name, sizeof(m_Name)), catchall, );

				// the login and/or password may be empty strings depending on whether the user chose to have them saved
				rverify(bb.WriteStr(m_Login, sizeof(m_Login)), catchall, );
				rverify(bb.WriteStr(m_ScAuthToken, sizeof(m_ScAuthToken)), catchall, );

				rverify(bb.WriteBool(m_OfflineProfile), catchall, );
				rverify(bb.WriteStr(m_GamerPic, sizeof(m_GamerPic)), catchall, );
				rverify(bb.WriteUns(m_ProfileNumber, sizeof(m_ProfileNumber) << 3), catchall, );

				time_t curTime = time(NULL);
				rverify(bb.WriteBytes((void*)&curTime, sizeof(curTime)), catchall, );

				rverify(bb.WriteUns(m_SteamId, sizeof(m_SteamId) << 3), catchall, );

				// RESERVED SPACE - PLEASE READ BEFORE EDITING
				//  RgscProfile is designed with INITIAL_RESERVED_SPACE_V2(1024) bytes of reserved space. 
				// If you add a variable to import/export, it must be used within this reserved space
				// m_MachineToken ID - RGSC_MAX_MACHINETOKEN_CHARS chars (including null terminator)
				rverify(bb.WriteBytes(m_MachineToken, sizeof(m_MachineToken)), catchall, );
				const u8 reserve[OUTER_RESERVED_SPACE_V2] = {0};
				rverify(bb.WriteBytes((void*)reserve, sizeof(reserve)), catchall, );
			}

			outerBlockSize = (unsigned)bb.GetBitLength() - outerBlockStart;

			// pad outer block to multiple of 16 bytes
			unsigned paddedSize = (outerBlockSize + (16 << 3) - 1) & ~((16 << 3) - 1);
			Assert((paddedSize - outerBlockSize) < (16 << 3));
			rverify(bb.WriteBits(pad, paddedSize - outerBlockSize, 0), catchall, );
			Assert(paddedSize == ((unsigned)bb.GetBitLength() - outerBlockStart));

			outerBlockSize = (unsigned)bb.GetBitLength() - outerBlockStart;

			rverify((outerBlockSize % (16 << 3)) == 0, catchall, );

			// inner block is encrypted with a key generated from the user's login and password
			// and then encrypted with the common key
			{
				unsigned innerBlockStart = bb.GetBitLength();

				// serialize data here
				{
					rverify(bb.WriteBytes(m_LoginHash, sizeof(m_LoginHash)), catchall, );

					// NOTE: reserving space for the future
					const u8 reserve[INNER_RESERVED_SPACE_V2] = {0};
					rverify(bb.WriteBytes((void*)reserve, sizeof(reserve)), catchall, );
				}

				// pad inner block to multiple of 16 bytes
				innerBlockSize = (unsigned)bb.GetBitLength() - innerBlockStart;

				unsigned paddedSize = (innerBlockSize + (16 << 3) - 1) & ~((16 << 3) - 1);
				if (paddedSize > innerBlockSize)
				{
					Assert((paddedSize - innerBlockSize) < (16 << 3));
					rverify(bb.WriteBits(pad, paddedSize - innerBlockSize, 0), catchall, );
					Assert(paddedSize == ((unsigned)bb.GetBitLength() - innerBlockStart));
				}

				innerBlockSize = (unsigned)bb.GetBitLength() - innerBlockStart;

				rverify((innerBlockSize % (16 << 3)) == 0, catchall, );
				rverify((bb.GetBitLength() % 8) == 0, catchall, );

				// generate private data context from login, password, and profileId
				u8 digest[Sha1::SHA1_DIGEST_LENGTH];
				Sha1 sha1((u8*)&m_ProfileId, sizeof(m_ProfileId));
				sha1.Update((u8*)login, (u32)strlen(login));
				sha1.Final(digest);

				// sign and encrypt using private data
				u8 checksum[datProtectSignatureSize];
				datProtectContext context(true, digest, sizeof(digest));
				datProtect(&((u8*)buf)[innerBlockStart >> 3], innerBlockSize >> 3, context, checksum);
				rverify(bb.WriteBytes(checksum, sizeof(checksum)), catchall, );
			}

			rverify((bb.GetBitLength() % 8) == 0, catchall, );

			outerBlockSize = (unsigned)bb.GetBitLength() - outerBlockStart;

			u8 checksum[datProtectSignatureSize];
			datProtectContext context(true, NULL, 0);
			datProtect(&((u8*)buf)[outerBlockStart >> 3], outerBlockSize >> 3, context, checksum);
			rverify(bb.WriteBytes(checksum, sizeof(checksum)), catchall, );

			rverify((bb.GetNumBitsWritten() % 8) == 0, catchall, );
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
RgscProfile::Import(unsigned folderId, const char* login, const char* password, const char* scAuthToken, const void* buf, const unsigned sizeofBuf, unsigned* size)
{
    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

    bool success = false;

    Clear();

    rtry
    {
        rverify(folderId > 0, catchall, );
        rverify(buf, catchall, );

		unsigned plaintextSize = 0;

        // format
        {
            // plaintext
            {
                // deserialize data here
                {
                    rverify(bb.ReadUns(m_Version, sizeof(m_Version) << 3), catchall, );
					rverify(m_Version == RGSC_PROFILE_VERSION_1 || m_Version == RGSC_PROFILE_VERSION_2, catchall, );
                }
                plaintextSize = (unsigned)bb.GetCursorPos();
                rverify((plaintextSize % 8) == 0, catchall, );
            }

			if (m_Version == RGSC_PROFILE_VERSION_1)
			{
				ImportV1(bb, login, password, buf, sizeofBuf);
			}
			else if (m_Version == RGSC_PROFILE_VERSION_2)
			{
				ImportV2(bb, login, password, scAuthToken, buf, sizeofBuf);
			}
			else
			{
				return false;
			}
        }

        m_FolderId = folderId;

        success = true;        
    }
    rcatchall
    {
        Clear();
    }

    if(size)
	{
		*size = success ? bb.GetNumBytesRead() : 0;
	}

    return success;
}

// PURPOSE
//	Imports data exported with ExportV1. Is the V1 implementation of Import();
bool RgscProfile::ImportV1(datImportBuffer& bb, const char* login, const char* password, const void* buf, const unsigned sizeofBuf)
{
	rtry
	{
		unsigned outerBlockSize = 0;
		unsigned innerBlockSize = 0;

		// outer block is encrypted with the common key, and is therefore less secure than the 2nd block below
		// it contains only the data that we need to be able to access without knowing the user's password
		{
			unsigned outerBlockStart = bb.GetCursorPos();
			outerBlockSize = (sizeofBuf << 3) - (datProtectSignatureSize << 3) - outerBlockStart;

			// decrypt outer block with common key
			u8 signature[datProtectSignatureSize];
			u8 signature2[datProtectSignatureSize];

			rverify(bb.SetCursorPos((sizeofBuf - sizeof(signature2)) << 3), catchall, );
			rverify(bb.ReadBytes(signature2, sizeof(signature2)), catchall, );

			datProtectContext context(true, NULL, 0);
			bool valid = datUnprotect(&((u8*)buf)[outerBlockStart >> 3], outerBlockSize >> 3, context, signature2);
			rverify(valid, catchall, rlError("profile is corrupt or has been modified by an untrusted source."));

			rverify(bb.SetCursorPos((sizeofBuf - (sizeof(signature) * 2)) << 3), catchall, );
			rverify(bb.ReadBytes(signature, sizeof(signature)), catchall, );

			bb.SetCursorPos(outerBlockStart);

			// the outer block checksum includes all of the data in the buffer (including the *encrypted* inner block)
			// so we have to calculate it before decrypting the inner block

			// deserialize data here
			{
				// SC will need to send us the key (if upgraded from a local profile,
				// or NULL if it wasn't upgraded from a local profile).
				u8 key[UniqueKey::SIZE_OF_KEY_IN_BYTES] = {0};
				rverify(bb.ReadBytes(key, sizeof(key)), catchall, );
				m_UniqueKey.SetKey(key);
				rverify(m_UniqueKey.IsSet(), catchall, 0);

				rverify(bb.ReadInt(m_ProfileId, sizeof(m_ProfileId) << 3), catchall, );

				rverify(bb.ReadStr(m_Name, sizeof(m_Name)), catchall, );

				// the login and/or password may be empty strings depending on whether the user chose to have them saved
				rverify(bb.ReadStr(m_Login, sizeof(m_Login)), catchall, );
				rverify(bb.ReadStr(m_Password, sizeof(m_Password)), catchall, );
				rverify(bb.ReadBool(m_OfflineProfile), catchall, );
				rverify(bb.ReadStr(m_GamerPic, sizeof(m_GamerPic)), catchall, );
				rverify(bb.ReadUns(m_ProfileNumber, sizeof(m_ProfileNumber) << 3), catchall, );

				rverify(bb.ReadBytes((void*)&m_LastSignInTime, sizeof(m_LastSignInTime)), catchall, );

				// NOTE: reserving space for the future
				rverify(bb.ReadUns(m_SteamId, sizeof(m_SteamId) << 3), catchall, );
				const u8 reserve[OUTER_RESERVED_SPACE_V1] = {0};
				rverify(bb.ReadBytes((void*)reserve, sizeof(reserve)), catchall, );
			}

			// pad outer block to multiple of 16 bytes
			outerBlockSize = (unsigned)bb.GetCursorPos() - outerBlockStart;

			unsigned paddedSize = (outerBlockSize + (16 << 3) - 1) & ~((16 << 3) - 1);
			Assert((paddedSize - outerBlockSize) < (16 << 3));
			u8 zero[16] = {0};
			u8 pad[16] = {0};
			rverify(bb.ReadBits(pad, paddedSize - outerBlockSize, 0), catchall, );
			rverify(memcmp(zero, pad, sizeof(pad)) == 0, catchall, );

			Assert(paddedSize == ((unsigned)bb.GetCursorPos() - outerBlockStart));

			outerBlockSize = (unsigned)bb.GetCursorPos() - outerBlockStart;

			// inner block is encrypted with a key generated from login and password
			// and then encrypted with the common key
			{
				unsigned innerBlockStart = bb.GetCursorPos();
				innerBlockSize = (sizeofBuf << 3) - ((datProtectSignatureSize * 2) << 3) - innerBlockStart;

				rverify((innerBlockSize % (16 << 3)) == 0, catchall, );

				const char* l = (login != NULL) ? login : m_Login;
				const char* p = (password != NULL) ? password : m_Password;

				if((l[0] != 0) && (p[0] != 0))
				{
					// generate private data context from login, password, and profileId
					u8 digest[Sha1::SHA1_DIGEST_LENGTH];
					Sha1 sha1((u8*)&m_ProfileId, sizeof(m_ProfileId));
					sha1.Update((u8*)l, (u32)strlen(l));
					sha1.Update((u8*)p, (u32)strlen(p));
					sha1.Final(digest);

					// decrypt inner block with private key
					datProtectContext context(true, digest, sizeof(digest));
					bool isValid = datUnprotect(&((u8*)buf)[innerBlockStart >> 3], innerBlockSize >> 3, context, signature);
					rverify(isValid, catchall, rlError("profile is corrupt or has been modified by an untrusted source."));

					// deserialize data here
					{
						rverify(bb.ReadBytes(m_LoginHash, sizeof(m_LoginHash)), catchall, );
						rverify(bb.ReadBytes(m_PasswordHash, sizeof(m_PasswordHash)), catchall, );

						// NOTE: reserving space for the future
						const u8 reserve[128] = {0};
						rverify(bb.ReadBytes((void*)reserve, sizeof(reserve)), catchall, );
					}

					rverify(Authenticate(l, p), catchall, );

					m_IsFullyDecrypted = true;
				}
			}
		}
		
		return true;
	}
	rcatchall
	{
		return false;
	}
}


bool RgscProfile::ImportV2(datImportBuffer& bb, const char* login, const char* password, const char* scAuthToken, const void* buf, const unsigned sizeofBuf)
{
	rtry
	{
		unsigned outerBlockSize = 0;
		unsigned innerBlockSize = 0;

		// outer block is encrypted with the common key, and is therefore less secure than the 2nd block below
		// it contains only the data that we need to be able to access without knowing the user's password
		{
			unsigned outerBlockStart = bb.GetCursorPos();
			outerBlockSize = (sizeofBuf << 3) - (datProtectSignatureSize << 3) - outerBlockStart;

			// decrypt outer block with common key
			u8 signature[datProtectSignatureSize];
			u8 signature2[datProtectSignatureSize];

			rverify(bb.SetCursorPos((sizeofBuf - sizeof(signature2)) << 3), catchall, );
			rverify(bb.ReadBytes(signature2, sizeof(signature2)), catchall, );

			datProtectContext context(true, NULL, 0);
			bool valid = datUnprotect(&((u8*)buf)[outerBlockStart >> 3], outerBlockSize >> 3, context, signature2);
			rverify(valid, catchall, rlError("profile is corrupt or has been modified by an untrusted source."));

			rverify(bb.SetCursorPos((sizeofBuf - (sizeof(signature) * 2)) << 3), catchall, );
			rverify(bb.ReadBytes(signature, sizeof(signature)), catchall, );

			bb.SetCursorPos(outerBlockStart);

			// the outer block checksum includes all of the data in the buffer (including the *encrypted* inner block)
			// so we have to calculate it before decrypting the inner block

			// deserialize data here
			{
				// SC will need to send us the key (if upgraded from a local profile,
				// or NULL if it wasn't upgraded from a local profile).
				u8 key[UniqueKey::SIZE_OF_KEY_IN_BYTES] = {0};
				rverify(bb.ReadBytes(key, sizeof(key)), catchall, );
				m_UniqueKey.SetKey(key);
				rverify(m_UniqueKey.IsSet(), catchall, 0);

				rverify(bb.ReadInt(m_ProfileId, sizeof(m_ProfileId) << 3), catchall, );

				rverify(bb.ReadStr(m_Name, sizeof(m_Name)), catchall, );

				// the login and/or password may be empty strings depending on whether the user chose to have them saved
				rverify(bb.ReadStr(m_Login, sizeof(m_Login)), catchall, );
				rverify(bb.ReadStr(m_ScAuthToken, sizeof(m_ScAuthToken)), catchall, );
				rverify(bb.ReadBool(m_OfflineProfile), catchall, );
				rverify(bb.ReadStr(m_GamerPic, sizeof(m_GamerPic)), catchall, );
				rverify(bb.ReadUns(m_ProfileNumber, sizeof(m_ProfileNumber) << 3), catchall, );
				rverify(bb.ReadBytes((void*)&m_LastSignInTime, sizeof(m_LastSignInTime)), catchall, );
				rverify(bb.ReadUns(m_SteamId, sizeof(m_SteamId) << 3), catchall, );

				// RESERVED SPACE - PLEASE READ BEFORE EDITING
				//  RgscProfile is designed with INITIAL_RESERVED_SPACE_V2(1024) bytes of reserved space. 
				// If you add a variable to import/export, it must be used within this reserved space
				// m_MachineToken ID - RGSC_MAX_MACHINETOKEN_CHARS chars (including null terminator)
				rverify(bb.ReadBytes(m_MachineToken, sizeof(m_MachineToken)), catchall, );
				const u8 reserve[OUTER_RESERVED_SPACE_V2] = {0};
				rverify(bb.ReadBytes((void*)reserve, sizeof(reserve)), catchall, );
			}

			// pad outer block to multiple of 16 bytes
			outerBlockSize = (unsigned)bb.GetCursorPos() - outerBlockStart;

			unsigned paddedSize = (outerBlockSize + (16 << 3) - 1) & ~((16 << 3) - 1);
			Assert((paddedSize - outerBlockSize) < (16 << 3));
			u8 zero[16] = {0};
			u8 pad[16] = {0};
			rverify(bb.ReadBits(pad, paddedSize - outerBlockSize, 0), catchall, );
			rverify(memcmp(zero, pad, sizeof(pad)) == 0, catchall, );

			Assert(paddedSize == ((unsigned)bb.GetCursorPos() - outerBlockStart));

			outerBlockSize = (unsigned)bb.GetCursorPos() - outerBlockStart;

			// inner block is encrypted with a key generated from login and password
			// and then encrypted with the common key
			{
				unsigned innerBlockStart = bb.GetCursorPos();
				innerBlockSize = (sizeofBuf << 3) - ((datProtectSignatureSize * 2) << 3) - innerBlockStart;

				rverify((innerBlockSize % (16 << 3)) == 0, catchall, );

				const char* l = (login != NULL) ? login : m_Login;
				if(l && l[0] != 0)
				{
					// generate private data context from login, password, and profileId
					u8 digest[Sha1::SHA1_DIGEST_LENGTH];
					Sha1 sha1((u8*)&m_ProfileId, sizeof(m_ProfileId));

					if(l && l[0] != 0)
					{
						sha1.Update((u8*)l, (u32)strlen(l));
					}
					
					sha1.Final(digest);

					// decrypt inner block with private key
					datProtectContext context(true, digest, sizeof(digest));
					bool isValid = datUnprotect(&((u8*)buf)[innerBlockStart >> 3], innerBlockSize >> 3, context, signature);
					rverify(isValid, catchall, rlError("profile is corrupt or has been modified by an untrusted source."));

					// deserialize data here
					{
						rverify(bb.ReadBytes(m_LoginHash, sizeof(m_LoginHash)), catchall, );

						// NOTE: reserving space for the future
						const u8 reserve[128] = {0};
						rverify(bb.ReadBytes((void*)reserve, sizeof(reserve)), catchall, );
					}

					m_IsFullyDecrypted = true;
				}
			}
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
RgscProfile::Authenticate(const char* login, const char* password) const
{ 
	// On steam, just the logged in steam ID matching the saved profile is enough
	if (GetRgscConcreteInstance()->IsSteam() && (m_SteamId > 0 && m_SteamId == GetRgscConcreteInstance()->GetTitleId()->GetSteamId()))
	{
		return true;
	}

	bool loginMatches = false;
	bool passwordMatches = false;

	if(AssertVerify(login))
	{
		Sha1 sha1;
		u8 hash[Sha1::SHA1_DIGEST_LENGTH];
		sha1.Update((u8*)login, (u32)strlen(login));
		sha1.Final(hash);
		loginMatches = memcmp(hash, m_LoginHash, sizeof(hash)) == 0;
	}
	//@@: location RLPCPROFILE_AUTHENTICATE
	if(loginMatches && password)
	{
		Sha1 sha1;
		u8 hash[Sha1::SHA1_DIGEST_LENGTH];
		sha1.Update((u8*)password, (u32)strlen(password));
		sha1.Final(hash);
		passwordMatches = memcmp(hash, m_PasswordHash, sizeof(hash)) == 0;
	}

	return loginMatches && (passwordMatches || !password);
}

////////////////////////////////////////////////////////////////////////////////
// RgscProfiles
////////////////////////////////////////////////////////////////////////////////
RgscProfiles::RgscProfiles()
{
    Clear();
}

RgscProfiles::~RgscProfiles()
{
    Clear();
}

const char*
RgscProfiles::ToJson()
{
	JSONNODE *n = json_new(JSON_NODE);
	json_push_back(n, json_new_i("NumProfiles", GetNumProfiles()));

	JSONNODE *c = json_new(JSON_ARRAY);
	json_set_name(c, "Profiles");

	for(unsigned i = 0; i < (unsigned)m_Profiles.size(); i++)
	{
		RgscProfile *profile = m_Profiles[i];

		if(AssertVerify(profile))
		{
			JSONNODE *p = json_new(JSON_NODE);
			if(profile->ToJson(p))
			{
				json_push_back(c, p);
			}
			else
			{
				json_delete(p);
			}
		}
	}

	json_push_back(n, c);

	json_char *jc = json_write_formatted(n);

	rlDebug2("%s\n", jc);

	json_delete(n);

	return jc;
}

void 
RgscProfiles::Clear()
{
    for(unsigned i = 0; i < (unsigned)m_Profiles.size(); i++)
    {
        RgscProfile *profile = m_Profiles[i];

        if(AssertVerify(profile))
        {
            // private destructor
            profile->~RgscProfile();
            RGSC_FREE(profile);
        }
    }

    m_Profiles.clear();
}

void 
RgscProfiles::AddProfile(const RgscProfile& profile)
{
    if(profile.IsValid())
    {
        // private constructor
        RgscProfile* copy = RGSC_ALLOCATE_T(RgscProfiles, RgscProfile);

        if(AssertVerify(copy))
        {
            new(copy) RgscProfile();
            *copy = profile;
            m_Profiles.Append() = copy;
        }
    }
}

unsigned
RgscProfiles::GetNumProfiles() const
{
    return m_Profiles.GetCount();
}

const RgscProfile& 
RgscProfiles::GetProfile(const unsigned index) const
{
    return *(m_Profiles[index]);
}

int
RgscProfiles::SortCompare(RgscProfile* const* a, RgscProfile* const* b)
{
    return ((*a)->GetLastSignInTime() < (*b)->GetLastSignInTime()) ? 1 : -1;
}

void 
RgscProfiles::Sort()
{
    m_Profiles.QSort(0, -1, &RgscProfiles::SortCompare);
}

////////////////////////////////////////////////////////////////////////////////
// rlPcProfileManager
////////////////////////////////////////////////////////////////////////////////
static char sm_AutoSignInFilename[] = "autosignin.dat";
static char sm_ProfileFilename[] = "profile.dat";
static char sm_SignInTransferFilename[] = "signintransfer.dat";

// ===========================================================================
// IProfileManager Public Interface
// ===========================================================================
RGSC_HRESULT RgscProfileManager::QueryInterface(RGSC_REFIID riid, LPVOID* ppvObj)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObj == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IProfileManager*>(this);
	}
	else if(riid == IID_IProfileManagerV1)
	{
		pUnknown = static_cast<IProfileManagerV1*>(this);
	}
	else if(riid == IID_IProfileManagerV2)
	{
		pUnknown = static_cast<IProfileManagerV2*>(this);
	}
	else if(riid == IID_IProfileManagerV3)
	{
		pUnknown = static_cast<IProfileManagerV3*>(this);
	}

	*ppvObj = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

bool RgscProfileManager::IsSignedIn()
{
	bool bIsReloadingUi = GetRgscConcreteInstance()->_GetUiInterface()->IsReloadingUi();
	bool bIsSignedInReloadingUi = GetRgscConcreteInstance()->_GetUiInterface()->IsReloadingUiSignedIn();

	if (bIsReloadingUi && bIsSignedInReloadingUi)
	{
		return true;
	}
	else
	{
		return m_SignedInProfile.IsValid() && !IsSigningIn();
	}
}

bool RgscProfileManager::IsOnline()
{
	//@@: range RLPCPROFILEMANAGER_ISONLINE_RANGE {
	//@@: location RLPCPROFILEMANAGER_ISONLINE
	bool bIsReloadingUi = GetRgscConcreteInstance()->_GetUiInterface()->IsReloadingUi();
	bool bIsOnlineReloadUi = GetRgscConcreteInstance()->_GetUiInterface()->IsReloadingUiOnline();

	if (bIsReloadingUi && bIsOnlineReloadUi)
	{
		return true;
	}
	else
	{
		return IsSignedIn() && rlRos::IsOnline(RL_RGSC_GAMER_INDEX);
	}
	//@@: } RLPCPROFILEMANAGER_ISONLINE_RANGE

}

RGSC_HRESULT RgscProfileManager::GetSignInInfo(IProfile* profile)
{
 	if(profile == NULL)
	{
		return ERROR_BAD_ARGUMENTS;
	}

	if(IsSignedIn() == false)
	{
		return ERROR_NO_SUCH_USER;
	}

	RgscProfile signedInProfile;
	if (GetRgscConcreteInstance()->_GetUiInterface()->IsReloadingUi())
	{
		if (HasReloadProfile())
		{
			signedInProfile = m_ReloadProfile;
		}
		else
		{
			return ERROR_NO_SUCH_USER;
		}
	}
	else
	{
		const RgscProfile& p = GetSignedInProfile();
		signedInProfile = p;
	}

	RGSC_HRESULT hr = RGSC_NOINTERFACE;

	// determine which profile version structure was passed to us
	ProfileV1* v1 = NULL;

	hr = profile->QueryInterface(IID_IProfileV1, (void**) &v1);

	if(SUCCEEDED(hr) && (v1 != NULL))
	{
		v1->SetProfileId(signedInProfile.GetProfileId());
		v1->SetNickname(signedInProfile.GetName());
		v1->SetIsOfflineProfile(signedInProfile.IsOfflineProfile());
		v1->SetProfileUniqueKey(signedInProfile.GetUniqueKey().GetKey());

		if(IsOnlineInternal())
		{
			const rlRosCredentials& creds = rlRos::GetCredentials(RL_RGSC_GAMER_INDEX);
			v1->SetRosTicket(creds.GetTicket());
		}
		else
		{
			v1->SetRosTicket("");
		}
	}

	ProfileV2* v2 = NULL;

	hr = profile->QueryInterface(IID_IProfileV2, (void**) &v2);

	if(SUCCEEDED(hr) && (v2 != NULL))
	{
		if(IsOnlineInternal())
		{
			v2->SetSocialClubEmail(m_SignedInEmail);
			v2->SetSocialClubPassword("");
		}
		else
		{
			v2->SetSocialClubEmail("");
			v2->SetSocialClubPassword("");
		}
	}

	ProfileV3* v3 = NULL;
	hr = profile->QueryInterface(IID_IProfileV3, (void**) &v3);

	if(SUCCEEDED(hr) && (v3 != NULL))
	{
		if(IsOnlineInternal())
		{
			v3->SetSocialClubAuthToken(m_SignedInScAuthToken);
		}
		else
		{
			v3->SetSocialClubAuthToken("");
		}
	}

	return RGSC_OK;
}

RGSC_HRESULT
RgscProfileManager::AutoSignInWithoutUi()
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		RgscProfile autoSignInProfile;
		RgscProfile signInTransferProfile;
		RgscProfileSignInTransferData transferData;

		// The sign in transfer profile overrides the normal auto sign in profile.
		// If we have a sign in transfer profile, then someone has signed into a game's launcher
		// and we want to auto sign that profile into the game. We still need to load the normal auto
		// sign in profile to get its side-effects of setting the auto sign in profile id.
		bool hasAutoSignInProfile = false;
		bool hasSignInTransferProfile = false;

		if ( GetRgscConcreteInstance()->IsSteam() )
		{
			RgscDisplay("Steam build - Attempting to load auto sign in profile.");
			hasAutoSignInProfile = LoadAutoSignInProfileSteam(autoSignInProfile);
			RgscDisplay("Steam build - %s a valid auto sign in profile.", hasAutoSignInProfile ? "had" : "did not have");
		}
		else
		{
			hasAutoSignInProfile = LoadAutoSignInProfile(autoSignInProfile);
		}

		// If the title should autosignin via a signintransfer file.
		if(GetRgscConcreteInstance()->IsSignInTransferAutoLoginSupported())
		{
			hasSignInTransferProfile = LoadSignInTransferProfile(signInTransferProfile, transferData);
		}

		// the sign in transfer file is only meant 
		// to be kept around until the game reads it in. 
		DeleteSignInTransferData();

		rcheck(hasAutoSignInProfile || hasSignInTransferProfile, noAutoSignInProfile, hr = ERROR_NO_SUCH_USER);

		if(hasSignInTransferProfile)
		{
			RgscDisplay("Transferring profile '%s' from launcher", transferData.GetLogin());

			// If we have a social club ticket, we can refresh it and do not need the scauthtoken
			if (transferData.GetScTicket() && transferData.GetScTicket()[0] != '\0')
			{
				transferData.m_ScAuthToken[0] = '\0';
			}

			rverify(SignInWithoutScUi(transferData.GetLogin(), transferData.GetScAuthToken(), transferData.GetScTicket()), catchall, );
		}
		else
		{
			rverify(SignInWithoutScUi(autoSignInProfile.IsLoginSaved() ? autoSignInProfile.GetSavedLogin() : NULL, 
									  autoSignInProfile.IsAuthTokenSaved() ? autoSignInProfile.GetSavedAuthToken() : NULL, 
									  NULL), catchall, );
		}

		hr = S_OK;
	}
	rcatch(noAutoSignInProfile)
	{

	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT
RgscProfileManager::AutoSigninWithUi()
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		RgscProfile autoSignInProfile;
		RgscProfile signInTransferProfile;
		RgscProfile reloadProfile;
		RgscProfileSignInTransferData transferData;

		// The sign in transfer profile overrides the normal auto sign in profile.
		// If we have a sign in transfer profile, then someone has signed into a game's launcher
		// and we want to auto sign that profile into the game. We still need to load the normal auto
		// sign in profile to get its side-effects of setting the auto sign in profile id.
		bool hasAutoSignInProfile = false;
		bool hasReloadProfile = false;
		bool hasSignInTransferProfile = false;

		// If we're reloading, we want to ignore auto signin and only trigger the reload. This is 
		// because we could have auto signin profile A but we're reloading on profile B. 
		if (GetRgscConcreteInstance()->_GetUiInterface()->IsReloadingUi())
		{
			hasReloadProfile = LoadReloadProfile(reloadProfile);
		}
		else if ( GetRgscConcreteInstance()->IsSteam() )
		{
			RgscDisplay("Steam build - Attempting to load auto sign in profile.");
			hasAutoSignInProfile = LoadAutoSignInProfileSteam(autoSignInProfile);
			RgscDisplay("Steam build - %s a valid auto sign in profile.", hasAutoSignInProfile ? "had" : "did not have");
		}
		else
		{
			hasAutoSignInProfile = LoadAutoSignInProfile(autoSignInProfile);
		}

		// If the title should autosignin via a signintransfer file.
		if(GetRgscConcreteInstance()->IsSignInTransferAutoLoginSupported())
		{
			hasSignInTransferProfile = LoadSignInTransferProfile(signInTransferProfile, transferData);
		}

		// the sign in transfer file is only meant 
		// to be kept around until the game reads it in. 
		DeleteSignInTransferData();

		rcheck(hasAutoSignInProfile || hasSignInTransferProfile || hasReloadProfile, noAutoSignInProfile, hr = ERROR_NO_SUCH_USER);

		JSONNODE *n = json_new(JSON_NODE);
		rverify(n, catchall, rlError("json_new failed to create node, possibly out of memory."));

		// Again, reload profile takes first priority, then transfer profile, then auto signin profile
		if (hasReloadProfile)
		{
			// If we have a social club ticket, we can refresh it and do not need the scauthtoken
			rverify(GetReloadProfileAsJson(reloadProfile, n), catchall, );

			// If we're refreshing a ticket, pass in the optional ticket refresh
			if (HasReloadTicket())
			{
				json_push_back(n, json_new_b("TicketRefresh", true));
			}
		}
		else if(hasSignInTransferProfile)
		{
			// If we have a social club ticket, we can refresh it and do not need the scauthtoken
			bool ticketRefresh = false;
			if (transferData.GetScTicket() && transferData.GetScTicket()[0] != '\0')
			{
				transferData.m_ScAuthToken[0] = '\0';
				ticketRefresh = true;
			}

			// Export the signin transfer to JSON
			rverify(signInTransferProfile.ToJson(n, transferData.GetLogin(), transferData.GetScTicket(), transferData.GetScAuthToken()), catchall, );

			// If we're refreshing a ticket, pass in the optional ticket refresh
			if (ticketRefresh)
			{
				json_push_back(n, json_new_b("TicketRefresh", true));
			}
		}
		else
		{
			rverify(autoSignInProfile.ToJson(n), catchall, );
		}

		json_char *jc = json_write_formatted(n);
		rverify(jc, catchall, );

		rlDebug2("AttemptAutoSignIn\n%s\n", jc);
		RgscDisplay("Auto sign in profile detected.");

		hr = GetRgscConcreteInstance()->_GetUiInterface()->SignIn(jc);

		json_free_safe(jc);
		json_delete(n);
	}
	rcatch(noAutoSignInProfile)
	{

	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT 
RgscProfileManager::AttemptAutoSignIn()
{
	// If we're already signed in, ignore this request.
	if (IsSignedInInternal())
	{
		RgscDisplay("Ignore auto login attempt, already signed in to '%s'", m_SignedInProfile.GetName());
		return RGSC_OK;
	}

	if (GetRgscConcreteInstance()->IsUiDisabled())
		return AutoSignInWithoutUi();
	else
		return AutoSigninWithUi();
}

bool RgscProfileManager::SignInAfterTransition()
{
	bool success = false;

	rtry
	{
		const RgscProfile& profile = GetSignedInProfile();

		rcheck(IsSignedInInternal(), catchall, );
		rcheck(profile.IsValid(), catchall, );

		RgscProfile loadedProfile;
		rcheck(LoadProfile(profile.GetFolderId(), loadedProfile, m_SignedInEmail, NULL, m_SignedInScAuthToken), catchall, );
		rcheck(loadedProfile.IsValid(), catchall, );
		rcheck(loadedProfile.GetProfileId() == profile.GetProfileId(), catchall, );
		rcheck(loadedProfile.IsOfflineProfile() == profile.IsOfflineProfile(), catchall, );

		JSONNODE *n = json_new(JSON_NODE);
		rverify(n, catchall, rlError("json_new failed to create node, possibly out of memory."));
		rverify(loadedProfile.ToJson(n, m_SignedInEmail, NULL, m_SignedInScAuthToken), catchall, );

		json_char *jc = json_write_formatted(n);
		rverify(jc, catchall, );

		rlDebug2("SignInAfterTransition\n%s\n", jc);

		RGSC_HRESULT hr = GetRgscConcreteInstance()->_GetUiInterface()->SignIn(jc);

		json_free_safe(jc);
		json_delete(n);

		success = SUCCEEDED(hr);
	}
	rcatchall
	{

	}

	return success;
}

// ===========================================================================
// FileDevice
// Note: this is a special-purpose feature to support Korean internet cafes
// that need to store profile data via their cloud system. The calling process
// can provide a delegate which will serve our file I/O requests. 
// ===========================================================================
class FileDevice
{
public:
	FileDevice()
	{
		m_FileDelegateV1 = NULL;
		m_Device = NULL;

		IFileDelegate* dlgt = GetRgscConcreteInstance()->_GetFileDelegate();
		if(dlgt)
		{
			IFileDelegateV1* v1 = NULL;
			RGSC_HRESULT hr = dlgt->QueryInterface(IID_IFileDelegateV1, (void**) &v1);
			if(SUCCEEDED(hr) && (v1 != NULL))
			{
				m_FileDelegateV1 = v1;
			}
		}
	}

	void InitDevice(const char *filename, bool readOnly)
	{
		if(m_FileDelegateV1 == NULL)
		{
			m_Device = fiDevice::GetDevice(filename, readOnly);
		}
	}

	fiHandle Open(const char *filename, bool readOnly) const
	{
		if(m_FileDelegateV1)
		{
			return (fiHandle)m_FileDelegateV1->Open(filename, readOnly);
		}
		else
		{
 			Assert(m_Device);
			return m_Device->Open(filename, readOnly);
		}
	}

	fiHandle Create(const char *filename) const
	{
		if(m_FileDelegateV1)
		{
			return (fiHandle)m_FileDelegateV1->Create(filename);
		}
		else
		{
			Assert(m_Device);
			return m_Device->Create(filename);
		}
	}

	int Read(fiHandle handle,void *outBuffer,int bufferSize) const
	{
		if(m_FileDelegateV1)
		{
			return m_FileDelegateV1->Read(handle, outBuffer, bufferSize);
		}
		else
		{
			Assert(m_Device);
			return m_Device->Read(handle, outBuffer, bufferSize);
		}
	}

	int Write(fiHandle handle,const void *buffer,int bufferSize) const
	{
		if(m_FileDelegateV1)
		{
			return m_FileDelegateV1->Write(handle, buffer, bufferSize);
		}
		else
		{
			Assert(m_Device);
			return m_Device->Write(handle, buffer, bufferSize);
		}
	}

	int Close(fiHandle handle) const
	{
		if(m_FileDelegateV1)
		{
			return m_FileDelegateV1->Close(handle);
		}
		else
		{
			Assert(m_Device);
			return m_Device->Close(handle);
		}
	}

	bool Delete(const char *filename) const
	{
		if(m_FileDelegateV1)
		{
			return m_FileDelegateV1->Delete(filename);
		}
		else
		{
			Assert(m_Device);
			return m_Device->Delete(filename);
		}
	}

	fiHandle FindFileBegin(const char *directoryName,fiFindData &outData) const
	{
		if(m_FileDelegateV1)
		{
			IFileDelegate::FileFindData data;
			fiHandle handle = (fiHandle)m_FileDelegateV1->FindFileBegin(directoryName, data);
			safecpy(outData.m_Name, data.m_Name);
			outData.m_Size = data.m_Size;
			outData.m_Attributes = data.m_Attributes;
			outData.m_LastWriteTime = data.m_LastWriteTime;
			return handle;
		}
		else
		{
			Assert(m_Device);
			return m_Device->FindFileBegin(directoryName, outData);
		}
	}

	bool FindFileNext(fiHandle handle,fiFindData &outData) const
	{
		if(m_FileDelegateV1)
		{
			IFileDelegate::FileFindData data;
			memset(&data, 0, sizeof(data));
			bool success = m_FileDelegateV1->FindFileNext(handle, data);
			if(success)
			{
				safecpy(outData.m_Name, data.m_Name);
				outData.m_Size = data.m_Size;
				outData.m_Attributes = data.m_Attributes;
				outData.m_LastWriteTime = data.m_LastWriteTime;
			}
			return success;
		}
		else
		{
			Assert(m_Device);
			return m_Device->FindFileNext(handle, outData);
		}
	}

	int FindFileEnd(fiHandle handle) const
	{
		if(m_FileDelegateV1)
		{
			return m_FileDelegateV1->FindFileEnd(handle);
		}
		else
		{
			Assert(m_Device);
			return m_Device->FindFileEnd(handle);
		}
	}

	bool MakeDirectory(const char *pathname) const
	{
		if(m_FileDelegateV1)
		{
			return m_FileDelegateV1->MakeDirectory(pathname);
		}
		else
		{
			Assert(m_Device);
			return m_Device->MakeDirectory(pathname);
		}
	}

	bool DeleteDirectory(const char *pathname, bool deleteContents, bool ignoreReadOnly) const
	{
		if(m_FileDelegateV1)
		{
			return m_FileDelegateV1->DeleteDirectory(pathname, deleteContents, ignoreReadOnly);
		}
		else
		{
			Assert(m_Device);
			return m_Device->DeleteDirectory(pathname, deleteContents, ignoreReadOnly);
		}
	}

	u64 GetFileTime(const char* pathname) const
	{
		return m_Device->GetFileTime(pathname);
	}

private:
	const IFileDelegateV1* m_FileDelegateV1;
	const fiDevice* m_Device;
};

const FileDevice* RgscProfileManager::GetFileDevice(const char *filename, bool readOnly)
{
	if(m_FileDevice == NULL)
	{
		m_FileDevice = rage_new FileDevice;
	}

	Assert(m_FileDevice);
	m_FileDevice->InitDevice(filename, readOnly);
	return m_FileDevice;
}

// ===========================================================================
// Init/Update/Shutdown
// ===========================================================================
RgscProfileManager::RgscProfileManager()
: m_FileDevice(NULL)
{

}

RgscProfileManager::~RgscProfileManager()
{

}

bool
RgscProfileManager::Init()
{
    bool success = false;

    if(AssertVerify(!s_IsInitialized))
    {
        s_IsInitialized = true;
		m_FileDevice = NULL;
		sm_Random.Reset(sysTimer::GetSystemMsTime());

        m_AutoSignInFolderId = 0;
        m_SignedInProfile.Clear();
		sysMemSet(m_SignedInEmail, 0, sizeof(m_SignedInEmail));
		sysMemSet(m_SignedInScAuthToken, 0, sizeof(m_SignedInScAuthToken));
		sysMemSet(m_ReloadEmail, 0, sizeof(m_ReloadEmail));
		sysMemSet(m_ReloadScAuthToken, 0, sizeof(m_ReloadScAuthToken));
		sysMemSet(m_ReloadTicket, 0, sizeof(m_ReloadTicket));
        m_ProfileIdToFolderIdMap.Reset();
        m_NameToFolderIdMap.Reset();
        m_Enumerated = false;
		m_NextOfflineProfileNumber = 1;
        m_SigningIn = false;
        success = true;
    }

    return success;
}

void
RgscProfileManager::Shutdown()
{
    if(s_IsInitialized)
    {
        m_AutoSignInFolderId = 0;
        m_SignedInProfile.Clear();
		sysMemSet(m_SignedInEmail, 0, sizeof(m_SignedInEmail));
		sysMemSet(m_SignedInScAuthToken, 0, sizeof(m_SignedInScAuthToken));
		sysMemSet(m_ReloadEmail, 0, sizeof(m_ReloadEmail));
		sysMemSet(m_ReloadScAuthToken, 0, sizeof(m_ReloadScAuthToken));
		sysMemSet(m_ReloadTicket, 0, sizeof(m_ReloadTicket));
        m_ProfileIdToFolderIdMap.Kill();
        m_NameToFolderIdMap.Kill();
        m_Enumerated = false;
		m_NextOfflineProfileNumber = 1;
        m_SigningIn = false;

		if(m_FileDevice)
		{
			delete m_FileDevice;
			m_FileDevice = NULL;
		}

		DeleteSignInTransferData();

        s_IsInitialized = false;
    }
}

void
RgscProfileManager::Update()
{
#if RSG_ASSERT
	bool bSignedOut = (IsSignedInInternal() == false);
	bool bValidScAuthToken = (m_SignedInScAuthToken[0] != '\0');
	bool bValidEmail = m_SignedInEmail[0] != '\0' && ((GetSignedInProfile().IsLoginSaved() == false) || (strcmp(GetSignedInProfile().GetSavedLogin(), m_SignedInEmail) == 0));
	bool bValidSteam = !IsSignedInInternal() || GetRgscConcreteInstance()->IsSteam() && GetSignedInProfile().GetSteamId();

	// Either an auth token, email creds or steam Id must be valid at all times that we're signed in.
	Assert(bSignedOut || bValidScAuthToken || bValidEmail || bValidSteam);
#endif
}

bool RgscProfileManager::IsSignedInInternal()
{
	return m_SignedInProfile.IsValid();
}

bool RgscProfileManager::IsOnlineInternal()
{
	return IsSignedInInternal() && (rlRos::IsOnline(RL_RGSC_GAMER_INDEX));
}

bool
RgscProfileManager::SignIn(const char* email,
						   const char* password,
						   const char* nickname,
						   const char* avatarUrl,
						   bool saveEmail,
						   bool savePassword,
						   bool saveMachine,
						   bool autoSignIn,
						   const char* xmlResponse,
						   const RockstarId rockstarId,
						   const bool isOfflineProfile,
						   const int callbackData)
{
	netStatus *status = &m_Status;
	if (status->Pending())
		return false;

	RLPC_CREATETASK(SignInTask,
					email,
					password,
					nickname,
					avatarUrl,
					saveEmail,
					savePassword,
					saveMachine,
					autoSignIn,
					xmlResponse,
					rockstarId,
					isOfflineProfile,
					callbackData,
					status);
}

bool RgscProfileManager::SignInWithoutScUi(const char* email, const char* scAuthToken, const char* scTicket)
{
	netStatus* status = &m_NoScuiStatus;
	if (status->Pending())
		return false;

	RLPC_CREATETASK(SignInWithoutScUiTask, email, "", scAuthToken, scTicket, status);
}

#if RSG_DEV
bool
RgscProfileManager::SignInWithoutScUi(const char* email, const char* password)
{
	netStatus* status = &m_NoScuiStatus;
	if (status->Pending())
		return false;

	RLPC_CREATETASK(SignInWithoutScUiTask, email, password, "", "", status);
}
#endif // #if RSG_DEV

bool
RgscProfileManager::SignInOfflineProfile(const RockstarId rockstarId,
										 const char* nickname,
										 const bool autoSignIn,
										 const int callbackData)
{
	bool success = false;

	rtry
	{
		rverify(GetRgscConcreteInstance()->AreLocalProfilesEnabled(), catchall, );

		bool profileExists = false;
		RgscProfile profile;

		if(rockstarId != InvalidRockstarId)
		{
			RgscProfiles profiles;
			EnumerateProfiles(profiles);
			for(unsigned i = 0; i < profiles.GetNumProfiles(); i++)
			{
				const RgscProfile &p = profiles.GetProfile(i);

				if((p.GetProfileId() == rockstarId) && p.IsOfflineProfile())
				{
					profileExists = true;
					profile = p;
					break;
				}
			}

			rverify(profileExists, catchall, );
		}

		if(profileExists == false)
		{
			rverify(nickname && (nickname[0] != '\0'), catchall, );
			bool profileCreated = CreateOfflineProfile(nickname, autoSignIn, profile);
			if (!profileCreated || !profile.IsValid())
			{
				rlError("Could not create offline profile");
				GetRgscConcreteInstance()->_GetUiInterface()->OnSignInError();
				profile.Clear();
			}
		}

		rverify(SignIn(profile.IsLoginSaved() ? profile.GetSavedLogin() : "",
					   profile.IsPasswordSaved() ? profile.GetSavedPassword() : "",
					   profile.GetName(),
					   profile.GetGamerPicName(),
					   true,
					   true,
					   false,
					   autoSignIn,
					   NULL,
					   profile.GetProfileId(),
					   true,
					   callbackData), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool
RgscProfileManager::ModifyProfile(const char* email,
								  const char* password,
								  const char* nickname,
								  const char* avatarUrl,
								  const int autoSignIn,
								  const int saveEmail)
{
	netStatus *status = &m_Status;
	if (status->Pending())
		return false;

	RLPC_CREATETASK(ModifyProfileTask,
					email,
					password,
					nickname,
					avatarUrl,
					autoSignIn,
					saveEmail,
					status);
}

static json_char* json_as_string_or_null(json_const JSONNODE* node)
{
	char* str = json_as_string(node);
	return (_stricmp("null", str) != 0) ? str : NULL;
}

bool RgscProfileManager::DeleteProfile(const char* jsonProfile)
{
	bool success = false;

	rtry
	{
		RockstarId rockstarId = InvalidRockstarId;
		
		JSONNODE *n = json_parse(jsonProfile);

		if (n == NULL)
		{
			Errorf("Invalid JSON Node\n");
			return false;
		}

		JSONNODE_ITERATOR i = json_begin(n);
		while (i != json_end(n))
		{
			if (*i == NULL)
			{
				Errorf("Invalid JSON Node\n");
				return false;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);
		
			if(_stricmp(node_name, "RockstarId") == 0)
			{
				json_char *node_value = json_as_string_or_null(*i);
				sscanf_s(node_value, "%I64d", &rockstarId);
				json_free_safe(node_value);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);

		rverify(rockstarId != InvalidRockstarId, catchall, );

		bool profileExists = false;

		RgscProfiles profiles;
		RgscProfile profile;
		rverify(EnumerateProfiles(profiles), catchall, );
		for(unsigned i = 0; i < profiles.GetNumProfiles(); i++)
		{
			const RgscProfile &p = profiles.GetProfile(i);

			if(rockstarId == p.GetProfileId())
			{
				profileExists = true;
				profile = p;
				break;
			}
		}

		rverify(profileExists, catchall, );

		rverify(DeleteProfile(profile), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool RgscProfileManager::SignInResponse(const char* json)
{
	bool success = false;
	char* email = NULL;
	char* password = NULL;
	char* nickname = NULL;
	char* avatarUrl = NULL;
	bool saveEmail = false;
	bool savePassword = false;
	bool saveMachine = false;
	bool autoSignIn = false;
	char* xmlResponse = NULL;
	RockstarId rockstarId = InvalidRockstarId;
	bool isOfflineProfile = false;
	int callbackData = -1;

	bool hadError = false;
	char* errorCode = NULL;
	char* errorCodeEx = NULL;

	JSONNODE *n = json_parse(json);

	if(n == NULL)
	{
		Errorf("Invalid JSON Node\n");
		return false;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(*i == NULL)
		{
			Errorf("Invalid JSON Node\n");
			return false;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "email") == 0)
		{
			email = json_as_string_or_null(*i);
		}
		else if(_stricmp(node_name, "password") == 0)
		{
			password = json_as_string_or_null(*i);
		}
		else if(_stricmp(node_name, "nickname") == 0)
		{
			nickname = json_as_string_or_null(*i);
		}
		else if(_stricmp(node_name, "avatarUrl") == 0)
		{
			avatarUrl = json_as_string_or_null(*i);
		}
		else if(_stricmp(node_name, "saveEmail") == 0)
		{
			saveEmail = json_as_bool(*i) != 0;
		}
		else if(_stricmp(node_name, "savePassword") == 0)
		{
			savePassword = json_as_bool(*i) != 0;
		}
		else if(_stricmp(node_name, "saveMachine") == 0)
		{
			saveMachine = json_as_bool(*i) != 0;
		}
		else if(_stricmp(node_name, "autoSignIn") == 0)
		{
			autoSignIn = json_as_bool(*i) != 0;
		}
		else if(_stricmp(node_name, "xmlResponse") == 0)
		{
			xmlResponse = json_as_string_or_null(*i);
		}
		else if(_stricmp(node_name, "rockstarId") == 0)
		{
			json_char *node_value = json_as_string_or_null(*i);
			if (node_value)
			{
				sscanf_s(node_value, "%I64d", &rockstarId);
				json_free_safe(node_value);
			}
		}
		else if(_stricmp(node_name, "local") == 0)
		{
			isOfflineProfile = json_as_bool(*i) != 0;
		}
		else if(_stricmp(node_name, "callbackData") == 0)
		{
			callbackData = json_as_int(*i);
		}
		else if (_stricmp(node_name, "Error") == 0)
		{
			hadError = true;

			JSONNODE *errNode = *i;

			if (errNode)
			{
				JSONNODE_ITERATOR err = json_begin(errNode);

				while(err != json_end(errNode))
				{
					json_char* err_node_name = json_name(*err);
					if (err_node_name)
					{
						if (_stricmp(err_node_name, "ErrorCode") == 0)
						{
							errorCode = json_as_string_or_null(*err);
						}
						else if (_stricmp(err_node_name, "ErrorCodeEx") == 0)
						{
							errorCodeEx = json_as_string_or_null(*err);
						}
					}

					json_free_safe(err_node_name);
					++err;
				}
			}
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	// Attempt to retrieve the reload email and save settings from the reload. The email will be empty
	//	from the SCUI if 'remember me' is unchecked, since the user has opted not to remember their email
	//	for booting into offline mode. However, for this case of losing internet connection, we want to allow
	//	the game to transition into offline mode for this session.
	bool bFreeEmail = true;
	if (GetRgscConcreteInstance()->_GetUiInterface()->IsReloadingUi() && HasReloadProfile())
	{
		if (email)
		{
			json_free_safe(email);
		}

		saveEmail = m_ReloadProfile.IsLoginSaved();
		savePassword = m_ReloadProfile.IsPasswordSaved();
		email = m_ReloadEmail;
		bFreeEmail = false;
	}

	if(autoSignIn)
	{
		savePassword = true;
	}

	if(savePassword)
	{
		saveEmail = true;
	}

	if (hadError)
	{
		rlDebug2("rlPcProfileManager::SignInResponse - Had Error\n\tCode: %s\n\tCodeEx: %s", errorCode ? errorCode : "", errorCodeEx ? errorCodeEx : "");
		GetRgscConcreteInstance()->_GetUiInterface()->OnSignInError();
	}
	else
	{
		GetRgscConcreteInstance()->_GetUiInterface()->ClearSignInError();
	}

	if(isOfflineProfile)
	{
		success = SignInOfflineProfile(rockstarId, nickname, autoSignIn, callbackData);
	}
	else
	{
		success = SignIn(email, password, nickname, avatarUrl, saveEmail, savePassword, saveMachine, autoSignIn, xmlResponse, rockstarId, isOfflineProfile, callbackData);
	}

	if (bFreeEmail)
	{
		json_free_safe(email);
	}
	
	json_free_safe(password);
	json_free_safe(nickname);
	json_free_safe(avatarUrl);
	json_free_safe(xmlResponse);
	json_free_safe(errorCode);
	json_free_safe(errorCodeEx);

	return success;
}

bool RgscProfileManager::ModifyProfile(const char* json)
{
	bool success = false;
	char* email = NULL;
	char* password = NULL;
	char* nickname = NULL;
	char* avatarUrl = NULL;
	int saveEmail = -1;
	int autoSignIn = -1;

	JSONNODE *n = json_parse(json);

	if (n == NULL){
		Errorf("Invalid JSON Node\n");
		return false;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while (i != json_end(n)){
		if (*i == NULL){
			Errorf("Invalid JSON Node\n");
			return false;
		}

		// recursively call ourselves to dig deeper into the tree
		if (json_type(*i) == JSON_ARRAY || json_type(*i) == JSON_NODE){
			//			ParseJSON(*i);
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "email") == 0)
		{
			email = json_as_string_or_null(*i);
		}
		else if(_stricmp(node_name, "password") == 0)
		{
			password = json_as_string_or_null(*i);
		}
		else if(_stricmp(node_name, "nickname") == 0)
		{
			nickname = json_as_string_or_null(*i);
		}
		else if(_stricmp(node_name, "avatarUrl") == 0)
		{
			avatarUrl = json_as_string_or_null(*i);
		}
		else if(_stricmp(node_name, "autoSignIn") == 0)
		{
			autoSignIn = (json_as_bool(*i) != 0) ? 1 : 0;
		}
		else if(_stricmp(node_name, "SaveEmail") == 0)
		{
			saveEmail = (json_as_bool(*i) != 0) ? 1 : 0;
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	success = ModifyProfile(email,
							password,
							nickname,
							avatarUrl,
							autoSignIn,
							saveEmail);

	json_free_safe(email);
	json_free_safe(password);
	json_free_safe(nickname);
	json_free_safe(avatarUrl);

	return success;
}

bool 
RgscProfileManager::CreateOfflineProfile(const char* name,
										 const bool autoSignIn,
										 RgscProfile &profile)
{
    bool success = false;

	profile.Clear();
	
	if(AssertVerify(GetRgscConcreteInstance()->AreLocalProfilesEnabled()))
	{
		if(AssertVerify(IsSignedInInternal() == false))
		{
			if(AssertVerify(m_Enumerated))
			{
				// make sure we don't create two profiles with the same name
				u32 *mapData = m_NameToFolderIdMap.Access(WeakHash(name));
				if(mapData == NULL)
				{
					// make sure we don't create two profiles with the same id
					RockstarId profileId = GenerateUniqueProfileId();
					if(profileId != InvalidRockstarId)
					{
						UniqueKey key;
						key.Generate();

						unsigned profileNumber = m_NextOfflineProfileNumber;

						// MTL requests signintransfer data for the title using ticket exchange before launching it.
						bool saveSignInTransferData = GetRgscConcreteInstance()->IsLauncher() && !GetRgscConcreteInstance()->IsTitleMTL();

						u64 steamId = GetRgscConcreteInstance()->GetTitleId()->GetSteamId();
						success = SaveProfile(&key, profileId, name, "offline", "offline", true, true, autoSignIn, saveSignInTransferData, NULL, true, profileNumber, steamId, "", "offline", "", profile);
						success = AssertVerify(profile.IsValid()) && success;
						m_NextOfflineProfileNumber++;
					}
				}
				else
				{
					rlError("Profile with that name already exists");
				}
			}
		}
	}

    return success;
}

void
RgscProfileManager::DuplicateSignInKick(const int localGamerIndex)
{
	rlDebug("We're being kicked due to duplicate sign in.");

	char jsonKick[256] = {0};

	rtry
	{
		RsonWriter rw;
		rw.Init(jsonKick, sizeof(jsonKick), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		rw.WriteBool("DuplicateSignIn", true);
		rcheck(rw.End(), catchall, );
	}
	rcatchall
	{
		jsonKick[0] = '\0';
	}

	bool succeeded = false;
	rgsc::Script::JsSignOut(&succeeded, jsonKick);

	// signout
	GetRgscConcreteInstance()->_GetProfileManager()->SignOut();

	// set the dupe login flag
	rlPresence::SetKickedByDuplicateSignIn(RL_RGSC_GAMER_INDEX);
}

void 
RgscProfileManager::SignOut()
{
	DeleteSignInTransferData();

    m_SignedInProfile.Clear();
	sysMemSet(m_SignedInEmail, 0, sizeof(m_SignedInEmail));
	sysMemSet(m_SignedInScAuthToken, 0, sizeof(m_SignedInScAuthToken));
	
	// reload profile not cleared here because when we lose internet connection, we do a 'sign out'
	// into the reload profile and we need the reload email and scauthtoken to persist until our next
	// sign in, so that we can seamless sign you back in online without re-entering your password

    GetRgscConcreteInstance()->_GetAchievementManager()->SignOut();
	GetRgscConcreteInstance()->_GetPlayerManager()->SignOut();
	GetRgscConcreteInstance()->_GetPresenceManager()->SignOut();
    GetRgscConcreteInstance()->_GetCommerceManager()->SignOut();

	// we notify the game of the sign in state change in rlPresence::RefreshSignInState().
	RgscDisplay("Signed out.");
}

void
RgscProfileManager::SetReloadProfile(const RgscProfile& profile)
{
	// Cache Reload profile
	m_ReloadProfile = profile;

	// The profile may contain SaveEmail=false, so we want to use our cache
	// values for the reload.
	if (!strcmp(m_ReloadEmail, m_SignedInEmail))
	{
		// If we're reloading into the same profile as our last re-load, try to maintain the ScAuthToken if possible. 
		// We do this because reloading from online->offline will cache the auth token.
		// When we attempt to return from offline->online, will need an ScAuthToken for logging back in.
		
		// Only copy the new token if it contains data, otherwise, keep the old token.
		if (m_SignedInScAuthToken[0] != '\0')
		{
			rlDebug3("Reloading, saving auth token: %s", m_SignedInScAuthToken);
			safecpy(m_ReloadScAuthToken, m_SignedInScAuthToken);
		}
	}
	else
	{
		// Clear out the old token completely, and then copy the new one
		sysMemSet(m_ReloadScAuthToken, 0, sizeof(m_ReloadScAuthToken));
		safecpy(m_ReloadScAuthToken, m_SignedInScAuthToken);

		// Clear out the old ticket
		sysMemSet(m_ReloadTicket, 0, sizeof(m_ReloadTicket));
	}

	const rlRosCredentials& creds = rlRos::GetCredentials(RL_RGSC_GAMER_INDEX);
	if (creds.IsValid())
	{
		rlDebug3("Reloading, saving sc ticket: %s", creds.GetTicket());
		safecpy(m_ReloadTicket, creds.GetTicket());	
	}

	 // Copy to the reload email in all cases
	 safecpy(m_ReloadEmail, m_SignedInEmail);
}

RockstarId 
RgscProfileManager::GetRockstarIdFromJson(const char* json)
{
	RockstarId rockstarId = InvalidRockstarId;

	JSONNODE *n = json_parse(json);

	if(!AssertVerify(n != NULL))
	{
		return rockstarId;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return rockstarId;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "RockstarId") == 0)
		{
			json_char *node_value = json_as_string_or_null(*i);
			sscanf_s(node_value, "%I64d", &rockstarId);
			json_free_safe(node_value);
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	return rockstarId;
}

bool  
RgscProfileManager::GetNicknameFromJson(const char* json, char (&nickname)[RLSC_MAX_NICKNAME_CHARS])
{
	JSONNODE *n = json_parse(json);

	if(!AssertVerify(n != NULL))
	{
		return false;
	}

	nickname[0] = '\0';

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return false;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "Nickname") == 0)
		{
			json_char *node_value = json_as_string_or_null(*i);
			safecpy(nickname, node_value);
			json_free_safe(node_value);
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	return nickname[0] != '\0';
}

const char*
RgscProfileManager::GetProfileList(bool bValidLoginProfilesOnly)
{
	RgscProfiles profiles;
	EnumerateProfiles(profiles, bValidLoginProfilesOnly);
	return profiles.ToJson();
}

const char*
RgscProfileManager::GetProfileList(const char* json)
{
	RgscProfiles profiles;

	rtry
	{
		rverify(IsSignedInInternal() == false, catchall, );

		RockstarId rockstarId = GetRockstarIdFromJson(json);
		char nickname[RGSC_MAX_NAME_CHARS] = {0};
		if(rockstarId == InvalidRockstarId)
		{
			GetNicknameFromJson(json, nickname);
		}

		rverify((rockstarId != InvalidRockstarId) || (nickname[0] != '\0'), catchall, );

		// it's assumed that the SCUI only requests a filtered list after retrieving
		// the full list of profiles, but we'll handle the case where that isn't true.
		if(m_Enumerated == false)
		{
			RgscProfiles profiles;
			EnumerateProfiles(profiles);
		}

		rverify(m_Enumerated, catchall, );

		RgscProfile loadedProfile;
		u32 *mapData = NULL;
		if(rockstarId != InvalidRockstarId)
		{
			mapData = m_ProfileIdToFolderIdMap.Access(rockstarId);
		}
		else if(nickname[0] != '\0')
		{
			mapData = m_NameToFolderIdMap.Access(WeakHash(nickname));
		}

		bool profileExists = mapData != NULL;

		if(profileExists)
		{
			unsigned folderId = *mapData;
			rcheck(LoadProfile(folderId, loadedProfile), catchall, );
			rcheck(loadedProfile.IsValid(), catchall, );
			rcheck(loadedProfile.GetFolderId() == folderId, catchall, );
			profiles.AddProfile(loadedProfile);
		}
	}
	rcatchall
	{

	}

	return profiles.ToJson();
}

const char*
RgscProfileManager::GetRosCredentials()
{
	json_char *jc = "{\"Ticket\" : \"\"}";

	if(IsOnlineInternal())
	{
		JSONNODE *n = json_new(JSON_NODE);
		json_set_name(n, "Credentials");

		const rlRosCredentials& creds = rlRos::GetCredentials(RL_RGSC_GAMER_INDEX);
		json_push_back(n, json_new_a("Ticket", creds.GetTicket() ? creds.GetTicket() : ""));

		jc = json_write_formatted(n);

		json_delete(n);
	}

	rlDebug2("RgscDelegate::GetRosCredentials:\n\t%s\n", jc);

	return jc;
}

void RgscProfileManager::SetSignInCredentials(const rlRosCredentials* cred)
{
	if (rlVerify(cred))
	{
		m_SignInCredentials = *cred;
	}
}

const rlRosCredentials& RgscProfileManager::GetSignInCredentials() const
{
	return m_SignInCredentials;
}

void RgscProfileManager::ClearSignInCredentials()
{
	m_SignInCredentials.Clear();
}

bool 
RgscProfileManager::IsSigningIn() const
{
    return m_SigningIn;
}

bool RgscProfileManager::CreateSigninTransferProfile()
{
	rtry
	{
		rcheck(IsSignedIn(), catchall, );
		rverify(m_SignedInProfile.IsValid(), catchall, );
		
		// delete
		DeleteSignInTransferData();

		rlRosCredentials creds = rlRos::GetCredentials(RL_RGSC_GAMER_INDEX);
		const char* scTicket = "";
		if (creds.IsValid())
		{
			scTicket = creds.GetTicket();
		}

		// and save
		rcheck(SaveSignInTransferData(m_SignedInProfile, m_SignedInEmail, scTicket, m_SignedInScAuthToken), catchall, );

		return true;
	}
	rcatchall
	{

	}

	return false;
}

void RgscProfileManager::OnDuplicateSignInMessageReceived(const int localGamerIndex)
{
	DuplicateSignInKick(localGamerIndex);
}

void RgscProfileManager::OnLauncherReceivedUpdatedSocialClubTicket(RockstarId id, const char* base64credentials)
{
	rlDebug2("OnLauncherReceivedUpdatedSocialClubTicket");
	RgscDisplayUtf8("Session synced with launcher.");

	rlRosCredentials creds;
	parTree* tree = NULL;
	char* credentialsBuf = NULL;
	unsigned decodedLen = 0;

	INIT_PARSER;

	rtry
	{
		rcheck(IsSignedIn(), catchall, rlWarning("Ignoring ticket changed event becuase we're not signed in"));
		rcheck(id == m_SignedInProfile.GetProfileId(), catchall, rlWarning("Id %"I64FMT"d does not match signed in ID %"I64FMT"d", id, m_SignedInProfile.GetProfileId()));

		// Calculate the maximum decoded size for the credentials, and then allocate enough room for it.
		int len = datBase64::GetMaxDecodedSize(base64credentials);
		credentialsBuf = (char*)RL_ALLOCATE(RgscProfileManager, len);
		rcheck(credentialsBuf, catchall, );

		// Decode the data, and output the response
		rcheck(datBase64::Decode(base64credentials, len, (u8*)&credentialsBuf[0], &decodedLen), catchall, );
		rlDisplay("Xml Response:\n%s", credentialsBuf);

		// treat the response as a memory file so we can use the XML parser
		char filename[64];
		fiDeviceMemory::MakeMemoryFileName(filename, sizeof(filename), credentialsBuf, decodedLen, false, NULL);
		PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);
		tree = PARSER.LoadTree(filename, "xml");
		PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, false);

		rverify(tree && tree->GetRoot(), catchall, );
		parTreeNode* pRoot = tree->GetRoot();
		rverify(pRoot, catchall, );

		// Parse the ticket response and update our credentials
		rlRosCreateTicketResponse response;
		rverify(rlRosXmlCreateTicketResponseParser::Parse(pRoot, &response), catchall, rlError("Failed to parse create ticket response"));
		rverify(rlRosCreateTicketResponseHelper::ApplyCreateTicketResponse(response, RL_RGSC_GAMER_INDEX, &creds), catchall, rlError("Failed to apply create ticket response"));
		rverify(rlRos::SetCredentials(RL_RGSC_GAMER_INDEX, &creds), catchall, rlError("Failed to update credentials"));
	}
	rcatchall
	{

	}

	if (credentialsBuf)
	{
		RL_FREE(credentialsBuf);
		credentialsBuf = NULL;
	}

	if(tree)
	{
		delete tree;
		tree = NULL;
	}
}

//! Event fired in the launcher when the the game has signed out
void RgscProfileManager::OnLauncherReceivedSignOutEvent()
{
	rlDebug2("OnLauncherReceivedSignOutEvent");

	char jsonKick[256] = {0};

	rtry
	{
		RsonWriter rw;
		rw.Init(jsonKick, sizeof(jsonKick), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		rw.WriteBool("DuplicateSignIn", false);
		rcheck(rw.End(), catchall, );
	}
	rcatchall
	{
		jsonKick[0] = '\0';
	}

	bool succeeded = false;
	rgsc::Script::JsSignOut(&succeeded, jsonKick);
}

//! Event fired in the launcher when the the game has signed in
void RgscProfileManager::OnLauncherReceivedSignInEvent(RockstarId id)
{
	rtry
	{
		rcheck(GetRgscConcreteInstance()->IsLauncher(), catchall, );
		rcheck(!IsSignedIn(), catchall, );

		RgscProfileSignInTransferData transferData;
		RgscProfile signInTransferProfile;

		// Ensure we have latest autosignin data
		LoadAutoSignInData();

		if (LoadSignInTransferProfile(signInTransferProfile, transferData))
		{
			rcheck(signInTransferProfile.GetProfileId() == id, catchall, );
			JSONNODE *n = json_new(JSON_NODE);
			rverify(n, catchall, rlError("json_new failed to create node, possibly out of memory."));

			// CreateTicketScAuthToken doesn't do ticket refreshing. We'll deliberate wipe our ScAuthToken here for the Launcher
			sysMemSet(signInTransferProfile.m_ScAuthToken, 0, sizeof(signInTransferProfile.m_ScAuthToken));
			sysMemSet(transferData.m_ScAuthToken, 0, sizeof(transferData.m_ScAuthToken));

			rverify(signInTransferProfile.ToJson(n, transferData.GetLogin(), transferData.GetScTicket(), transferData.GetScAuthToken()), catchall, );
			json_push_back(n, json_new_b("TicketRefresh", true));

			json_char *jc = json_write_formatted(n);
			rverify(jc, catchall, );

			rlDebug2("OnLauncherReceivedSignInEvent\n%s\n", jc);

			GetRgscConcreteInstance()->_GetUiInterface()->SignIn(jc);

			json_free_safe(jc);
			json_delete(n);
		}
	}
	rcatchall
	{

	}

	DeleteSignInTransferData();
}

bool RgscProfileManager::CreateTitleSpecificTransferProfile(int rosTitleId, IAsyncStatus* status)
{
	RLPC_CREATETASK_IASYNC(CreateTitleSpecificTransferProfileTask, rosTitleId, m_SignedInEmail);
}

const RgscProfile&
RgscProfileManager::GetSignedInProfile() const
{
	if (m_SignedInProfile.IsValid())
	{
		return m_SignedInProfile;
	}
	else if (HasReloadProfile())
	{
		return m_ReloadProfile;
	}

	AssertMsg(m_SignedInProfile.IsValid() || HasReloadProfile(), "Signin and Reload Profiles both invalid. GetSignedInProfile called in a bad state.");
    return m_SignedInProfile;
}

bool RgscProfileManager::GetSignedInProfileAsJson(JSONNODE* n)
{
	rlRosCredentials creds = rlRos::GetCredentials(RL_RGSC_GAMER_INDEX);
	const char* scTicket = "";
	if (creds.IsValid())
	{
		scTicket = creds.GetTicket();
	}

	if (m_SignedInProfile.IsValid())
	{
		return m_SignedInProfile.ToJson(n, m_SignedInEmail, scTicket, m_SignedInScAuthToken);
	}
	else if (HasReloadProfile())
	{
		return GetReloadProfileAsJson(m_ReloadProfile, n);
	}

	return false;
}

bool RgscProfileManager::HasReloadProfile() const
{
	return m_ReloadProfile.IsValid();
}

bool RgscProfileManager::HasReloadEmail() const
{
	return StringNullOrEmpty(m_ReloadEmail) == false;
}

bool RgscProfileManager::HasReloadScAuthToken() const
{
	return StringNullOrEmpty(m_ReloadScAuthToken) == false;
}

bool RgscProfileManager::HasReloadTicket() const
{
	return StringNullOrEmpty(m_ReloadTicket) == false;
}

bool RgscProfileManager::GetReloadProfileAsJson(const RgscProfile& profile, JSONNODE* n)
{
	if (HasReloadTicket())
	{
		// Prefer to extend the previous ticket if we had one.
		return profile.ToJson(n, m_ReloadEmail, m_ReloadTicket, "");
	}
	else
	{
		// Otherwise, generate a new ticket with the ScAuthToken.
		return profile.ToJson(n, m_ReloadEmail, "", m_ReloadScAuthToken);
	}
}

#if 0
unsigned rlPcProfileManager::GenerateUniqueFolderId() const
{
    unsigned folderId = 0;
    const unsigned maxAttempts = 500;
    unsigned numAttempts = 0;

    char profilesPath[RGSC_MAX_PATH] = {0};

    if(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfilesDirectory(profilesPath, true))
    {
        while(true)
        {
            folderId = (((unsigned)sm_Random.GetInt() % 0xFFFF) << 16) | ((unsigned)sm_Random.GetInt() % 0xFFFF);
            numAttempts++;

            if(folderId > 0)
            {
                char newFolderPath[RGSC_MAX_PATH] = {0};
                formatf(newFolderPath, "%s%08X\\", profilesPath, folderId);

                int result = SHCreateDirectoryEx(NULL, newFolderPath, NULL);
                if(result == ERROR_SUCCESS)
                {
                    break;
                }
            }

            if(numAttempts >= maxAttempts)
            {
                folderId = 0;
                break;
            }
        }
    }

    return folderId;
}
#endif

RockstarId RgscProfileManager::GenerateUniqueProfileId() const
{
    RockstarId profileId = InvalidRockstarId;
    const unsigned maxAttempts = 500;
    unsigned numAttempts = 0;

    Assert(m_Enumerated);

    while(true)
    {
        profileId = InvalidRockstarId;
        u64 uuid64 = 0;
		
        if(rlCreateUUID(&uuid64) && (uuid64 > 0))
        {
			// note: casting to s64
            profileId = (RockstarId)uuid64;

			if(profileId < 0)
			{
				profileId *= -1;
			}
        }

        numAttempts++;

        if((profileId != InvalidRockstarId) && (profileId > 0))
        {
            const u32 *mapData = m_ProfileIdToFolderIdMap.Access(profileId);
            if(mapData == NULL)
            {
                break;
            }
        }

        if(numAttempts >= maxAttempts)
        {
            profileId = InvalidRockstarId;
            break;
        }
    }
	//@@: location RLPCPROFILEMANAGER_GENERATEUNIQUEPROFILEID_RETURN
    return profileId;
}

bool 
RgscProfileManager::DeleteAutoSignInData()
{
    bool success = false;

	m_AutoSignInFolderId = 0;
    
    char path[RGSC_MAX_PATH] = {0};
    if(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfilesDirectory(path, true))
    {
        safecat(path, sm_AutoSignInFilename);

		const FileDevice *device = GetFileDevice(path);
        success = device->Delete(path);
    }

    return success;
}

bool 
RgscProfileManager::SaveAutoSignInData(const RgscProfile &profile)
{
    bool success = false;

    m_AutoSignInFolderId = 0;

    if(AssertVerify(profile.IsValid()))
    {
        char path[RGSC_MAX_PATH] = {0};
        if(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfilesDirectory(path, true))
        {
            safecat(path, sm_AutoSignInFilename);
            
            RgscProfileAutoSignInData data(profile.GetFolderId());

            u8 saveData[RgscProfileAutoSignInData::MAX_BYTE_SIZEOF_PAYLOAD] = {0};
            unsigned saveSize = 0;
            bool bExported = data.Export(saveData, sizeof(saveData), &saveSize);

            if(bExported)
            {
                const FileDevice *device = GetFileDevice(path);
                fiHandle hFile = device->Create(path);

                if(fiIsValidHandle(hFile))
                {
                    int bytesWritten = device->Write(hFile, saveData, saveSize);
                    device->Close(hFile);
                    if(bytesWritten == (int)saveSize)
                    {
                        m_AutoSignInFolderId = profile.GetFolderId();
                        success = true;
                    }
                }
            }
        }
    }

    return success;
}

bool 
RgscProfileManager::LoadAutoSignInData()
{
    bool success = false;

    m_AutoSignInFolderId = 0;

    char path[RGSC_MAX_PATH] = {0};
    if(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfilesDirectory(path, false))
    {
        safecat(path, sm_AutoSignInFilename);

        const FileDevice *device = GetFileDevice(path);
        fiHandle hFile = device->Open(path, true);

        if(fiIsValidHandle(hFile))
        {
            u8 saveData[RgscProfileAutoSignInData::MAX_BYTE_SIZEOF_PAYLOAD] = {0};
            unsigned saveSize = sizeof(saveData); 
            
            int bytesRead = device->Read(hFile, saveData, saveSize);
            device->Close(hFile);

            if(bytesRead > 0)
            {
                RgscProfileAutoSignInData data;
                bool bImported = data.Import(saveData, bytesRead, &saveSize);
                if(bImported && (bytesRead == (int)saveSize) && data.IsValid())
                {
                    m_AutoSignInFolderId = data.GetAutoSignInFolderId();
                    success = true;
                }
            }
        }
    }

    return success;
}

bool 
RgscProfileManager::DeleteSignInTransferData()
{
	bool success = false;

	char path[RGSC_MAX_PATH] = {0};
	if(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfilesDirectory(path, true))
	{
		safecat(path, sm_SignInTransferFilename);

		const FileDevice *device = GetFileDevice(path);
		success = device->Delete(path);
	}

	return success;
}

bool 
RgscProfileManager::SaveSignInTransferData(const RgscProfile &profile, const char* login, const char* scTicket, const char* scAuthToken)
{
	bool success = false;

	if(AssertVerify(profile.IsValid()))
	{
		char path[RGSC_MAX_PATH] = {0};
		if(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfilesDirectory(path, true))
		{
			safecat(path, sm_SignInTransferFilename);

			RgscProfileSignInTransferData data(profile.GetFolderId(), login, scTicket, scAuthToken);

			//@@: range RLPCPROFILEMANAGER_SAVESIGNINTRANSFERDATA_SAVE_DATA {
			u8 saveData[RgscProfileSignInTransferData::MAX_BYTE_SIZEOF_PAYLOAD] = {0};
			unsigned saveSize = 0;
			//@@: location RLPCPROFILEMANAGER_SAVESIGNINTRANSFERDATA_EXPORT
			bool bExported = data.Export(saveData, sizeof(saveData), &saveSize);

			if(bExported)
			{
				DeleteSignInTransferData();
				const FileDevice *device = GetFileDevice(path);
				fiHandle hFile = device->Create(path);

				if(fiIsValidHandle(hFile))
				{
					int bytesWritten = device->Write(hFile, saveData, saveSize);
					device->Close(hFile);
					if(bytesWritten == (int)saveSize)
					{
						success = true;
					}
				}
			}
			//@@: } RLPCPROFILEMANAGER_SAVESIGNINTRANSFERDATA_SAVE_DATA
		}
	}

	return success;
}

bool 
RgscProfileManager::LoadSignInTransferData(RgscProfileSignInTransferData &data)
{
	bool success = false;

	data.Clear();

	char path[RGSC_MAX_PATH] = {0};
	if(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfilesDirectory(path, false))
	{
		safecat(path, sm_SignInTransferFilename);

		const FileDevice *device = GetFileDevice(path);
		fiHandle hFile = device->Open(path, true);

		if(fiIsValidHandle(hFile))
		{
			u8 saveData[RgscProfileSignInTransferData::MAX_BYTE_SIZEOF_PAYLOAD] = {0};
			unsigned saveSize = sizeof(saveData); 

			int bytesRead = device->Read(hFile, saveData, saveSize);
			device->Close(hFile);

			if(bytesRead > 0)
			{
				bool bImported = data.Import(saveData, bytesRead, &saveSize);
				if(bImported && (bytesRead == (int)saveSize) && data.IsValid())
				{
					success = true;
				}
			}
		}
	}

	return success;
}

bool
RgscProfileManager::SaveProfile(const UniqueKey* uniqueKey,
								RockstarId profileId,
								const char* name,
								const char* login,
								const char* password,
								bool saveLogin,
								bool savePassword,
								bool autoSignIn,
								bool signInTransfer,
								const char* gamerPic,
								bool offlineProfile,
								unsigned profileNumber,
								u64 steamId,
								const char* scTicket,
								const char* scAuthToken,
								const char* machineToken,
								RgscProfile &savedProfile)
{
	bool success = false;

	rtry
	{
		savedProfile.Clear();

		bool hasKey = uniqueKey && uniqueKey->IsSet();
		u32 *mapData = m_ProfileIdToFolderIdMap.Access(profileId);
		bool profileExistsLocally = mapData != NULL;

		rverify(hasKey || (offlineProfile == false), catchall, );
		rverify(hasKey || (profileExistsLocally == false), catchall, );

		Assert((profileNumber == 0) || (offlineProfile == true));

		UniqueKey key;
 		if(hasKey)
 		{
			 key = *uniqueKey;
		}
		else
		{
			// online profile that doesn't exist locally and doesn't have a key
 			key.GenerateFromRockstarId(profileId);
 		}

		unsigned folderId = GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfileDirectoryId(key);

		if(profileExistsLocally)
		{
			rverify((folderId == *mapData), catchall, );
		}

		// if you try to retrieve an existing
		// profile when that profile already exists on disk

		// TODO: NS - this can also happen if you create or retrieve an online 
		// profile that happens to have the same profileId as one of your existing 
		// randomly generated offline profileIds. Unlikely, but possible.
		// To solve this, we would either have to guarantee that our offline
		// profileIds can never be a valid online profileId, or have two separate
		// profile folders (online profiles and offline profiles).

		rverify(folderId > 0, catchall, );

		// only save sc auth tokens for the auto-signin profile
		bool saveScAuthToken = autoSignIn;

		RgscProfile profile(folderId, profileId, key, name, login, password, saveLogin, savePassword, saveScAuthToken, gamerPic, offlineProfile, profileNumber, steamId, scAuthToken, machineToken);
		rverify(profile.IsValid(), catchall, );
		rverify(profile.GetFolderId() == folderId, catchall, );

		// if we sign into a profile that isn't the auto-sign-in profile, then
		// delete the auto sign in file, so we don't auto sign in the previous
		// profile next time.
		if((IsAutoSignInProfile(profile) && (autoSignIn == false)) ||
			(IsAutoSignInProfile(profile) == false))
		{
			DeleteAutoSignInData();
		}

		// get the path to the profile file
		char path[RGSC_MAX_PATH] = {0};
		bool gotDirectory = GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfileDirectory(path, profile.GetFolderId(), true);
		rverify(gotDirectory, catchall, );
		safecat(path, sm_ProfileFilename);

		// add this profile to the maps
		m_ProfileIdToFolderIdMap[profile.GetProfileId()] = profile.GetFolderId();
		m_NameToFolderIdMap[WeakHash(profile.GetName())] = profile.GetFolderId();

		// export file format to memory
		u8 saveData[RgscProfile::MAX_BYTE_SIZEOF_PAYLOAD] = {0};
		unsigned saveSize = 0;
		bool bExported = profile.Export(login, password, scAuthToken, saveData, sizeof(saveData), &saveSize);
		rverify(bExported, catchall, );

		// write file to disk
		const FileDevice *device = GetFileDevice(path);
		fiHandle hFile = device->Create(path);
		rverify(fiIsValidHandle(hFile), catchall, );
		int bytesWritten = device->Write(hFile, saveData, saveSize);
		device->Close(hFile);
		rverify(bytesWritten == (int)saveSize, catchall, );

		if(autoSignIn)
		{
			// set this profile as the auto-sign-in profile
			SaveAutoSignInData(profile);
		}

		DeleteSignInTransferData();

		if(signInTransfer)
		{
			// set this profile as the sign in transfer profile
			SaveSignInTransferData(profile, login, scTicket, scAuthToken);
		}

		savedProfile = profile;

		RgscDisplay("Saved profile for %s", profile.GetName());

		success = true;
	}
	rcatchall
	{
		RgscError("Failed to save profile for %s", name);
	}

	return success;
}

bool 
RgscProfileManager::LoadAutoSignInProfile(RgscProfile &profile)
{
    bool success = false;

    profile.Clear();

    LoadAutoSignInData();

    if(m_AutoSignInFolderId != 0)
    {
        success = LoadProfile(m_AutoSignInFolderId, profile);
    }

	if(profile.IsOfflineProfile() && (GetRgscConcreteInstance()->AreLocalProfilesEnabled() == false))
	{
		profile.Clear();
		success = false;
	}

	// We can't auto signin to a profile without an auth token.
	if (!profile.IsAuthTokenSaved())
	{
		rlWarning("Autologin profile has no saved auth token, ignoring.");
		profile.Clear();
		success = false;
	}

    return success;
}

bool 
RgscProfileManager::LoadAutoSignInProfileSteam(RgscProfile &profile)
{
	// Old platforms do not include the steam ID and should use the regular autosignin flow.
	u64 steamId = GetRgscConcreteInstance()->GetTitleId()->GetSteamId();
	if (steamId == 0)
	{
		return LoadAutoSignInProfile(profile);
	}

	// Enumerate stored local profiles
	RgscProfiles profiles;
	if (!EnumerateProfiles(profiles))
		return false;

	bool profileExists = false;
	for(unsigned i = 0; i < profiles.GetNumProfiles(); i++)
	{
		const RgscProfile &p = profiles.GetProfile(i);
		if(steamId == p.GetSteamId())
		{
			profileExists = true;
			profile = p;
			break;
		}
	}

	// If the profile has a e-mail but no password, SCUI signin will fail. Wipe the e-mail to force the SCUI to
	// to perform steam auto signin. Login will return in the SCUI signin before saving back out to the profile file.
	if (profile.m_Password[0] == '\0')
	{
		profile.m_Login[0] = '\0';
	}

	// On Steam, the profile always "exists" as long as the steam id is valid. The auto signin will fail on the SCS side due 
	// to unlinked, and then the user will be able to authenticate with username/password. 
	// Only attempt this when NOT in offline only mode
	if (!GetRgscConcreteInstance()->IsOfflineOnlyMode() && !profileExists && steamId > 0)
	{
		rlDebug2("No steam profile on disk, attempting autosignin with steam id only");
		profile.m_SteamId = steamId;
		profileExists = true;
	}

	return profileExists;
}

bool 
RgscProfileManager::LoadSignInTransferProfile(RgscProfile &profile, RgscProfileSignInTransferData &data)
{
	bool success = false;

	profile.Clear();

	if(LoadSignInTransferData(data) && (data.GetAutoSignInFolderId() != 0))
	{
		success = LoadProfile(data.GetAutoSignInFolderId(), profile, "", "", data.GetScAuthToken());
	}

	if(profile.IsOfflineProfile() && (GetRgscConcreteInstance()->AreLocalProfilesEnabled() == false))
	{
		profile.Clear();
		success = false;
	}

	return success;
}

bool 
RgscProfileManager::LoadReloadProfile(RgscProfile& profile)
{
	bool success = false;

	if (HasReloadProfile())
	{
		profile.Clear();
		profile = m_ReloadProfile;

		// Steam Flow
		if (GetRgscConcreteInstance()->IsSteam())
		{
			// For steam single sign on, if no password is present, there must also be no e-mail. Otherwise,
			//	the login+password flow is used in the SCUI, which will obviously fail. By providing an empty email/password,
			//	single login flow is attempted.
			if (!profile.IsPasswordSaved())
			{
				profile.m_Login[0] = '\0';
			}

			// For Steam, when loading the offline site, we may need to recover the reload email if the user
			//	has chosen not to store it. Otherwise, there is no way to lookup the profile.
			if (GetRgscConcreteInstance()->_GetUiInterface()->IsOfflineMode() && !profile.IsLoginSaved() && HasReloadEmail())
			{
				safecpy(profile.m_Login, m_ReloadEmail);
			}
		}
		else // Non-Steam Flow
		{
			// If a user has not chosen to store their email, reload from memory
			if (!profile.IsLoginSaved() && HasReloadEmail())
			{
				safecpy(profile.m_Login, m_ReloadEmail);
			}

			// If the user has no stored ticket, ensure the profile contains the reload sc auth token.
			if (!HasReloadTicket())
			{
				// Only replace the scauth token on the profile if it didn't have one.
				if (!profile.IsAuthTokenSaved() && HasReloadScAuthToken())
				{
					safecpy(profile.m_ScAuthToken, m_ReloadScAuthToken);
				}
			}

			// Either a ticket or scauth token is needed for reloading into online mode.
			// Otherwise, reload without being signed in.
			bool bHasTicketOrAuthToken = HasReloadTicket() || profile.IsAuthTokenSaved();
			if (!bHasTicketOrAuthToken && !GetRgscConcreteInstance()->_GetUiInterface()->IsOfflineMode())
			{
				profile.Clear();
				return false;
			}
		}

		success = true;
	}

	return success;
}

bool
RgscProfileManager::LoadProfile(unsigned folderId, RgscProfile &profile)
{
    return LoadProfile(folderId, profile, NULL, NULL, NULL);
}

bool
RgscProfileManager::LoadProfile(unsigned folderId, RgscProfile &profile, const char* login, const char* password, const char* scAuthToken)
{
    bool success = false;

    profile.Clear();

    char path[RGSC_MAX_PATH] = {0};
	//@@: location RLPCPROFILEMANAGER_LOADPROFILE_GET_PLATFORM_DIRECTORY
    if(AssertVerify(folderId > 0) && GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfileDirectory(path, folderId, false))
    {
        safecat(path, sm_ProfileFilename);

        const FileDevice *device = GetFileDevice(path);
        fiHandle hFile = device->Open(path, true);

        if(fiIsValidHandle(hFile))
        {
            u8 saveData[RgscProfile::MAX_BYTE_SIZEOF_PAYLOAD] = {0};
            unsigned saveSize = sizeof(saveData); 

            int bytesRead = device->Read(hFile, saveData, saveSize);
            device->Close(hFile);

            if(bytesRead > 0)
            {
                bool bImported = profile.Import(folderId, login, password, scAuthToken, saveData, bytesRead, &saveSize);
                if(bImported && (bytesRead == (int)saveSize) && profile.IsValid())
                {
                    Assert(profile.GetFolderId() == folderId);

                    m_ProfileIdToFolderIdMap[profile.GetProfileId()] = folderId;
                    m_NameToFolderIdMap[WeakHash(profile.GetName())] = profile.GetFolderId();

					rlDebug("Profile type:%s, Profile Id:%" I64FMT "u, ProfileNumber:%d, Name:%s, Login:%s, Password:%s, Login Saved:%s, Password Saved:%s, Auto Sign In:%s, Steam Id:%"I64FMT"u",
                        profile.IsOfflineProfile() ? "offline" : "online", profile.GetProfileId(), profile.GetProfileNumber(), profile.GetName(),
                        profile.IsLoginSaved() ? profile.GetSavedLogin() : "", profile.IsPasswordSaved()? profile.GetSavedPassword() : "", profile.IsLoginSaved() ? "yes" : "no",
                        profile.IsPasswordSaved() ? "yes" : "no", IsAutoSignInProfile(profile) ? "yes" : "no", profile.GetSteamId());

                    success = true;
                }
            }
        }
    }

    return success;
}

bool 
RgscProfileManager::DeleteProfile(const RgscProfile &profile)
{
    bool success = false;

    Assert(profile.IsValid());
    Assert(profile.GetFolderId() > 0);

    m_ProfileIdToFolderIdMap.Delete(profile.GetProfileId());
    m_NameToFolderIdMap.Delete(WeakHash(profile.GetName()));

    if(IsAutoSignInProfile(profile))
    {
        DeleteAutoSignInData();
    }

    char path[RGSC_MAX_PATH] = {0};
    if(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfileDirectory(path, profile.GetFolderId(), false))
    {
		const FileDevice *device = GetFileDevice(path);
        success = device->DeleteDirectory(path, true, true);
    }

    return success;
}

bool 
RgscProfileManager::CheckForProfilesFast()
{
	bool exist = false;

	char path[RGSC_MAX_PATH];
	if(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfilesDirectory(path, false))
	{
		const FileDevice *device = GetFileDevice(path);

		fiFindData data;
		fiHandle h = device->FindFileBegin(path, data);
		if(fiIsValidHandle(h))
		{
			do
			{
				if(data.m_Name[0] != '.' && (data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)) 
				{
					unsigned folderId = 0;
					if((sscanf_s(data.m_Name, "%x", &folderId) == 1) && (folderId > 0))
					{
						exist = true;
						break;
					}
				}
			}
			while(device->FindFileNext(h, data));

			device->FindFileEnd(h);
		}
	}

	return exist;
}

u64 
RgscProfileManager::GetOldestProfileTimeToInclude(const FileDevice *device, const char* path)
{
	u64 lastWriteTime[sm_MaxProfiles] = {0};
	u32 numProfiles = 0;

	u32 maxProfiles = 0;
	if (!PARAM_scMaxLocalProfiles.Get(maxProfiles))
	{
		// stop iterating after a ridiculous number of profiles
		maxProfiles = 1024;
	}

	char profilePath[RGSC_MAX_PATH] = {0};
	u64 modTime = 0;

	fiFindData data;
	fiHandle h = device->FindFileBegin(path, data);
	if(fiIsValidHandle(h))
	{
		do
		{
			if(data.m_Name[0] != '.' && (data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)) 
			{
				unsigned folderId = 0;
				if((sscanf_s(data.m_Name, "%x", &folderId) == 1) && (folderId > 0))
				{
					formatf(profilePath, "%s%s\\%s", path, data.m_Name, sm_ProfileFilename);
					modTime = device->GetFileTime(profilePath);
					if(modTime > 0)
					{
						if(numProfiles < sm_MaxProfiles)
						{
							lastWriteTime[numProfiles] = modTime;
						}
						else
						{
							// evict the oldest one
							u64 oldestWriteTime = 0xFFFFFFFFFFFFFFFF;
							u32 indexOfOldest = 0;
							for(u32 i = 0; i < sm_MaxProfiles; ++i)
							{
								if(lastWriteTime[i] < oldestWriteTime)
								{
									oldestWriteTime = lastWriteTime[i];
									indexOfOldest = i;
								}
							}

							// Only evict the oldest one if its older than the current time
							if (lastWriteTime[indexOfOldest] < modTime)
							{
								lastWriteTime[indexOfOldest] = modTime;
							}
						}

						numProfiles++;
					}
				}
			}
		}
		while((numProfiles < maxProfiles) && device->FindFileNext(h, data));

		device->FindFileEnd(h);
	}

	if(numProfiles > sm_MaxProfiles)
	{
		rlDebug2("There are %u profiles on disk, only enumerating the most recently saved %u...", numProfiles, sm_MaxProfiles);
	}

	u64 oldestWriteTime = 0xFFFFFFFFFFFFFFFF;
	for(u32 i = 0; i < sm_MaxProfiles; ++i)
	{
		if(lastWriteTime[i] < oldestWriteTime)
		{
			oldestWriteTime = lastWriteTime[i];
		}
	}
	
	return oldestWriteTime;
}

bool 
RgscProfileManager::EnumerateProfiles(RgscProfiles& profiles, bool bValidLoginProfilesOnly)
{
    bool success = false;

    profiles.Clear();

    m_ProfileIdToFolderIdMap.Reset();
    m_NameToFolderIdMap.Reset();
    m_Enumerated = false;
	m_NextOfflineProfileNumber = 1;

    LoadAutoSignInData();

    if(AssertVerify(IsSignedInInternal() == false))
    {
        char path[RGSC_MAX_PATH];
        if(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformProfilesDirectory(path, false))
        {
            const FileDevice *device = GetFileDevice(path);

			u64 oldestProfileTime = GetOldestProfileTimeToInclude(device, path);

			char profilePath[RGSC_MAX_PATH] = {0};
			u64 modTime = 0;

            fiFindData data;
            fiHandle h = device->FindFileBegin(path, data);
            if(fiIsValidHandle(h))
            {
                do
                {
                    if(data.m_Name[0] != '.' && (data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)) 
                    {
                        unsigned folderId = 0;
                        if((sscanf_s(data.m_Name, "%x", &folderId) == 1) && (folderId > 0))
                        {
							formatf(profilePath, "%s%s\\%s", path, data.m_Name, sm_ProfileFilename);
							modTime = device->GetFileTime(profilePath);
							if(modTime >= oldestProfileTime)
							{
								RgscProfile profile;
								if(LoadProfile(folderId, profile))
								{
									Assert(profile.IsValid());
									Assert(folderId == profile.GetFolderId());

									if(profile.IsOfflineProfile())
									{
										if(profile.GetProfileNumber() >= m_NextOfflineProfileNumber)
										{
											m_NextOfflineProfileNumber = profile.GetProfileNumber() + 1;
										}
									}

									bool bExcludeNoLoginProfile = bValidLoginProfilesOnly && !profile.IsLoginSaved();
									bool bExcludeOfflineProfile = profile.IsOfflineProfile() && !GetRgscConcreteInstance()->AreLocalProfilesEnabled();
									bool bExcludeSteamProfile = GetRgscConcreteInstance()->IsSteam() &&
																profile.GetSteamId() != GetRgscConcreteInstance()->GetTitleId()->GetSteamId();

									if(!bExcludeNoLoginProfile && !bExcludeOfflineProfile && !bExcludeSteamProfile)
									{
										profiles.AddProfile(profile);
									}
								}
							}
                        }
                    }
                }
                while((profiles.GetNumProfiles() < sm_MaxProfiles) && device->FindFileNext(h, data));

                device->FindFileEnd(h);
            }

            profiles.Sort();

            m_Enumerated = true;
            success = true;
        }
    }

    return success;
}

bool 
RgscProfileManager::IsAutoSignInProfile(const RgscProfile &profile) const
{
    return profile.IsValid() && (m_AutoSignInFolderId > 0) && (m_AutoSignInFolderId == profile.GetFolderId());
}

bool 
RgscProfileManager::Authenticate(const RgscProfile &profile, const char* login, const char* password)
{
    bool authenticated = false;

    if(profile.IsValid())
    {
        if(profile.IsFullyDecrypted())
        {
            authenticated = profile.Authenticate(login, password);
        }
        else
        {
            RgscProfile tempProfile;
            if(LoadProfile(profile.GetFolderId(), tempProfile, login, password, NULL))
            {
                Assert(tempProfile.IsFullyDecrypted() || GetRgscConcreteInstance()->IsSteam());
                authenticated = tempProfile.Authenticate(login, password);
            }
        }
    }

    return authenticated;
}

} // namespace rgsc


