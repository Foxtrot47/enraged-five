#ifndef RGSC_H
#define RGSC_H

#pragma warning(disable: 4668)

#include "memory.h"
#include "configuration_interface.h"
#include "delegate_interface.h"
#include "rgsc_interface.h"
#include "titleid_interface.h"
#include "achievements.h"
#include "players.h"
#include "filesystem.h"
#include "file_delegate_interface.h"
#include "cloudsave.h"
#include "commerce.h"
#include "presence.h"
#include "profiles.h"
#include "gamerpics.h"
#include "steam.h"
#include "json.h"
#include "metadata.h"
#include "mfa.h"
#include "network.h"
#include "tasks.h"
#include "telemetry.h"
#include "../../rgsc_ui/pad.h"
#include "../../rgsc_ui/rgsc_ui.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <unknwn.h>
#pragma warning(pop)

// error C4265: 'Rgsc' : class has virtual functions, but destructor is not virtual
// the binary interface for virtual destructors isn't standardized, so don't make the destructor virtual
#pragma warning(push)
#pragma warning(disable: 4265)

namespace rage
{
	class netHardware;
	class netSocket;
}

// TODO: NS - remove
using namespace rgsc;

namespace rgsc
{

#if __RAGE_DEPENDENCY
RAGE_DECLARE_CHANNEL(rgsc_dbg)
#endif

class Rgsc : public IRgscLatestVersion,
			 public IRgscDelegateLatestVersion
{
// ===============================================================================================
// inherited from public interfaces
// ===============================================================================================
public:
	// ===============================================================================================
	// inherited from IRgscUnknown
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

	// ===============================================================================================
	// inherited from IRgscV1
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL Init(ITitleId* titleId, RgscLanguage language, IRgscDelegate* dlgt);
	void RGSC_CALL Shutdown();
	void RGSC_CALL Update();

	IAchievementManager* RGSC_CALL GetAchievementManager();
	IProfileManager* RGSC_CALL GetProfileManager();
	IFileSystem* RGSC_CALL GetFileSystem();
	IRgscUi* RGSC_CALL GetUiInterface();

	// ===============================================================================================
	// inherited from IRgscV2
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL Init(IConfiguration* config, ITitleId* titleId, RgscLanguage language, IRgscDelegate* dlgt);
	IPlayerManager* RGSC_CALL GetPlayerManager();
	ITaskManager* RGSC_CALL GetTaskManager();
	IPresenceManager* RGSC_CALL GetPresenceManager();
	ICommerceManager* RGSC_CALL GetCommerceManager();

	// ===============================================================================================
	// inherited from IRgscV3
	// ===============================================================================================
	virtual void RGSC_CALL SetFileDelegate(IFileDelegate* fileDelegate);

	// ===============================================================================================
	// inherited from IRgscV4
	// ===============================================================================================
	virtual void RGSC_CALL SetSteamAuthTicket(const char* steamAuthTicket);

	// ===============================================================================================
	// inherited from IRgscV5
	// ===============================================================================================
	ITelemetry* RGSC_CALL GetTelemetry();
	virtual RGSC_HRESULT RGSC_CALL OnGameCrashed(u64 actionToTake);
	virtual bool RGSC_CALL IsReadyToShutdown();

	// ===============================================================================================
	// inherited from IRgscV6
	// ===============================================================================================
	IGamepadManager* RGSC_CALL GetGamepadMgr();
	INetwork* RGSC_CALL GetNetworkInterface();

	// ===============================================================================================
	// inherited from IRgscV7
	// ===============================================================================================
	ICloudSaveManager* RGSC_CALL GetCloudSaveManager();
	IGamerPicManager* RGSC_CALL GetGamerPicManager();

	// ===============================================================================================
	// inherited from IRgscV8
	// ===============================================================================================
	void RGSC_CALL SetSteamPersona(const char* steamPersona);
	void RGSC_CALL SetSteamId(u64 steamId);
	void RGSC_CALL SetSteamAppId(u32 steamAppId);

	// ===============================================================================================
	// inherited from IRgscDelegateV1
	// ===============================================================================================
	void RGSC_CALL Output(IRgscDelegateLatestVersion::OutputSeverity severity, const char* msg);
	virtual bool RGSC_CALL GetStatsData(char** data);
	virtual void RGSC_CALL FreeStatsData(const char* data);
	virtual void RGSC_CALL SetTextBoxHasFocus(const bool hasFocus);
	virtual void RGSC_CALL UpdateSocialClubDll(const char* commandLine);

	// ===============================================================================================
	// inherited from IRgscDelegateV2
	// ===============================================================================================
	virtual void RGSC_CALL SetTextBoxHasFocusV2(const bool hasFocus, const char* prompt, const char* text, const bool isPassword, const unsigned int maxNumChars);

	// ===============================================================================================
	// inherited from IRgscDelegateV3
	// ===============================================================================================
	virtual void RGSC_CALL HandleNotification(const rgsc::NotificationType id, const void* param);

// ===============================================================================================
// accessible from anywhere in the dll
// ===============================================================================================
public:
	Rgsc();
	~Rgsc(); // for binary compatibility reasons, don't make this virtual

	RgscGamerPicManager* _GetGamerPicManager();
	RgscMetadata* _GetLocalMetadata();
	RgscTaskManager* _GetTaskManager();
	RgscAchievementManager* _GetAchievementManager();
	RgscPlayerManager* _GetPlayerManager();
	RgscProfileManager* _GetProfileManager();
	RgscPresenceManager* _GetPresenceManager();
	RgscCommerceManager* _GetCommerceManager();
	RgscFileSystem* _GetFileSystem();
	RgscGamePadManager* _GetGamepadManager();
	IFileDelegate* _GetFileDelegate();
	RgscUi* _GetUiInterface();
	RgscTelemetryManager* _GetTelemetry();
	RgscNetwork* _GetNetwork();
	RgscCloudSaveManager* _GetCloudSaveManager();
	RgscMultiFactorAuth* _GetMultiFactorAuth();

#if RGSC_STEAM_EXPLICIT_SUPPORT
	RgscSteamManager* _GetSteam();
#endif

	void NotifyFatalError(int errCode);

	// Configuration
	bool IsOfflineOnlyMode();
	bool IsPatchingEnabled();
	bool AreLocalProfilesEnabled();
	bool IsLauncher();
	bool IsUiDisabled();
	bool IsMac();
	bool IsSteam();
	bool IsTitleMp3();
	bool IsTitleLaNoire();
	bool IsTitleGtaV();
	bool IsTitleMTL();
	const char* GetAdditionalSessionAttr();
	const char* GetAdditionalJoinAttr();
	bool GetHandleAutoSignIn();
	bool IsLoggingEnabled();
	const char* GetLoggingPath();
	const char* GetMetaDataPath();
	const char* GetScuiPakPath();
	bool CreateGameCrashTask(u64 actionToTake);
	const char* GetSteamPersona();
	const char* GetSteamPersonaAsJson();
	const WindowHandle* GetAdditionalWindowHandles();
	const unsigned GetNumAdditionalWindowHandles();
	const char* GetCommandLineArgumentsAsJson();
	RgscAuthServices GetAuthServiceMethod();

	// Network Connectivity
	bool HasNetwork();
	bool IsConnectedToInternet();
	void UpdateConnectionState();
	
	void UpdateTextEvents();

	// Compatibility
	bool IsPresenceSupported();
	bool IsFriendsSupported();
	bool IsConnectionStateChangeSupported();
	bool IsAchievementsSupported();
	bool IsSignInTransferAutoLoginSupported();
	bool IsMetadataSupported();

	IConfigurationV6::GamepadSupport GetGamepadSupport();

	TitleId* GetTitleId();
	const char* GetTitleIdAsJson();
	bool GetVersionInfo(char (&version)[128]);
	bool GetTitleVersionInfo(char (&version)[128]);
	bool GetModuleVersionInfo(const char* fileName, char (&version)[128]);
	const char* GetVersionInfoAsJson();
	static bool IsInitialized();
	static bool IsInitializedForOutput();
	const char* GetStatsData();

	void ProcessDetach();

// ===============================================================================================
// only accessible from rgsc.cpp
// ===============================================================================================
private:

	struct TextBoxHasFocusEvent
	{
		static const int MAX_PROMPT_CHARS = 48;
		static const int MAX_TEXT_CHARS = 512;

		void Clear()
		{
			IsPending = false;
			HasFocus = false;
			Prompt[0] = '\0';
			Text[0] = '\0';
			IsPassword = false;
			MaxNumChars = 0;
		}

		bool IsPending;
		bool HasFocus;
		char Prompt[MAX_PROMPT_CHARS];
		char Text[MAX_TEXT_CHARS];
		bool IsPassword;
		unsigned MaxNumChars;
	};


	RGSC_HRESULT SetDelegate(IRgscDelegate* dlgt);
	RGSC_HRESULT CreateAllocator();
	RGSC_HRESULT SecurityChecks();
	RGSC_HRESULT UpdateRegistry();
	RGSC_HRESULT InitRline(ITitleId* titleId, RgscLanguage language);
	RGSC_HRESULT InitRage(IConfiguration* config, ITitleId* titleId, RgscLanguage language);
	bool DeleteTempDirectory();
	void InitMemVisualize();
	void ReadResponseFile(int &argc,char** &argv);
	void SendSteamInfoToScui();

	IRgscDelegate *m_Delegate;
	IRgscDelegateV1 *m_DelegateV1;
	IRgscDelegateV2 *m_DelegateV2;
	IRgscDelegateV3 *m_DelegateV3;
	IFileDelegate *m_FileDelegate;

	TitleId m_TitleId;

	RgscUi m_Ui;
	RgscAchievementManager m_AchievementManager;
	RgscPlayerManager m_PlayerManager;
	RgscGamerPicManager m_GamerPicManager;
	//rlPcPresence m_PresenceManager;
	RgscJson m_Json;
	RgscMetadata m_Metadata;
	RgscProfileManager m_ProfileManager;
	RgscTaskManager m_TaskManager;
	RgscTelemetryManager m_TelemetryManager;
	RgscPresenceManager m_PresenceManager;
	RgscCommerceManager m_CommerceManager;
	RgscFileSystem m_FileSystem;
	RgscGamePadManager m_GamepadManager;
	RgscNetwork m_NetworkInterface;
	RgscCloudSaveManager m_CloudSaveManager;
	RgscMultiFactorAuth m_MultiFactorAuth;

#if RGSC_STEAM_EXPLICIT_SUPPORT
	RgscSteamManager m_SteamManager;
#endif

	// Configuration
	bool m_OfflineOnlyMode;
	bool m_PatchingEnabled;
	bool m_LocalProfilesEnabled;
	bool m_IsLauncher;
	bool m_HandleAutoSignIn;
	bool m_PreInitializedOutput;
	bool m_MetadataEnabled;
	bool m_UseHttpsForRosServices;
	bool m_Initialized;
	char m_AdditionalSessionAttr[RGSC_ADDITIONAL_SESSION_ATTR_BUF_SIZE];
	char m_AdditionalJoinAttr[RGSC_ADDITIONAL_SESSION_ATTR_BUF_SIZE];
	char m_MetaDataPath[RGSC_MAX_PATH];
	char m_OfflineScuiPakPath[RGSC_MAX_PATH];
	IConfigurationV6::GamepadSupport m_GamepadSupport;
	WindowHandle* m_AdditionalWindowHandles;
	unsigned m_NumAdditionalWindowHandles;
	IConfigurationV8::FeatureBehaviour m_SignInTransferBehaviour;
	IConfigurationV8::FeatureBehaviour m_FriendsBehaviour;
	IConfigurationV8::FeatureBehaviour m_PresenceBehaviour;
	IConfigurationV8::FeatureBehaviour m_AchievementBehaviour;
	IConfigurationV8::FeatureBehaviour m_ConnectionChangeBehaviour;
	IConfigurationV11::RosFilterBehaviour m_RosFilterBehaviour;
	RgscAuthServices m_RgscAuthServices;

	netStatus m_GameCrashStatus;
	bool m_IsUiDisabled;

	// Logging
	char m_RgscLogPath[RGSC_MAX_PATH];
	bool m_RgscLoggingEnabled;

	// Input Event
	TextBoxHasFocusEvent m_FocusEvent;

	// Connection states
	unsigned m_TimeOfLastConnectionCheck;
	unsigned m_TimeOfLastConnected;
	BOOL m_WasConnectedToInternet;
};

extern Rgsc* GetRgscConcreteInstance();
extern void DestroyRgscConcreteInstance();

#pragma warning(pop)

} // namespace rgsc

#endif //RGSC_H
