// 
// rgsc/mfa.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#include "mfa.h"

// rage includes
#include "diag/seh.h"
#include "string/stringhash.h"
#include "system/nelem.h"

// windows includes
#pragma warning(push)
#pragma warning(disable: 4668)
#include <shlobj.h>
#pragma warning(pop)

/////////////////////////////////////////////////
// Fingerprinting Constants
/////////////////////////////////////////////////
#define FP_NUM_KEYS			9
#define FP_MAX_STR_LENGTH   (4 * 1024)
#define FP_CPU_0			"cpuInfo0"
#define FP_CPU_1			"cpuInfo1"
#define FP_CPU_2			"cpuInfo2"
#define FP_CPU_3			"cpuInfo3"
#define FP_COMPUTER_NAME	"computerName"
#define FP_VOLUME_NAME		"volumeName"
#define FP_VOLUME_SERIAL	"volumeSerial"
#define FP_FILE_SYSTEM_NAME	"fileSystemName"
#define FP_DEVICE_NAME		"device_name"
#define FP_UNKNOWN_STR		"Unknown"
/////////////////////////////////////////////////

namespace rgsc
{

/////////////////////////////////////////////////
// RgscMultiFactorAuth
/////////////////////////////////////////////////
RgscMultiFactorAuth::RgscMultiFactorAuth()
	: m_Fingerprint(0)
{

}

bool RgscMultiFactorAuth::Init(TitleId& titleId)
{
	m_FingerprintKvp.Reserve(FP_NUM_KEYS);
	m_Fingerprint = GenerateFingerprint(titleId);

	// fingerprint generation fails gracefully
	return true;
}

void RgscMultiFactorAuth::Shutdown()
{
	m_FingerprintKvp.clear();
}

u64 RgscMultiFactorAuth::GetFingerprint()
{
	return m_Fingerprint;
}

void RgscMultiFactorAuth::AddFingerPrint(const char* key, const char* value)
{
	FingerprintKvp kvp;
	kvp.key = key;
	kvp.value = value;

	m_FingerprintKvp.PushAndGrow(kvp);
}

void RgscMultiFactorAuth::AddFingerPrintInt(const char* key, int value)
{
	char buf[64];
	formatf(buf, "%d", value);
	AddFingerPrint(key, buf);
}

void RgscMultiFactorAuth::AddFingerPrintUns(const char* key, unsigned value)
{
	char buf[64];
	formatf(buf, "%u", value);
	AddFingerPrint(key, buf);
}

void RgscMultiFactorAuth::AddFingerPrintStr(const char* key, const char* value)
{
	unsigned hash = atStringHash(value);
	AddFingerPrintUns(key, hash);
}

void RgscMultiFactorAuth::AddFingerPrintStrW(const char* key, const wchar_t* value)
{
	rtry
	{
		// find length needed for string
		size_t len = WideCharToMultiByte(CP_UTF8, 0, value, -1, NULL, 0, NULL, NULL);
		rcheck(len < FP_MAX_STR_LENGTH, catchall, );

		// allocate a work buffer big enough to convert the string to utf8
		char* buf = (char*)alloca(len);
		rcheck(buf, catchall, );

		rcheck(WideCharToMultiByte(CP_UTF8, 0, value, -1, buf, (int)len, 0, 0) != 0, catchall, );
		AddFingerPrintStr(key, buf);
	}
	rcatchall
	{
		AddFingerPrintUns(key, 0);
	}
}

// Create a list of fingerprint key/value pairs, and also return an atDataHash of these values for legacy purposes.
u64 RgscMultiFactorAuth::GenerateFingerprint(TitleId& UNUSED_PARAM(titleId))
{
	u64 result = 0;
	m_FingerprintKvp.clear();

	// ====================================
	//	Initial Hash using CPU capabilities
	// ====================================
	// See: https://msdn.microsoft.com/en-us/library/hskdteyh.aspx
	int cpuInfo[4] = {0};
	__cpuid( cpuInfo, 0 );

	AddFingerPrintInt(FP_CPU_0, cpuInfo[0]);
	AddFingerPrintInt(FP_CPU_1, cpuInfo[1]);
	AddFingerPrintInt(FP_CPU_2, cpuInfo[2]);
	AddFingerPrintInt(FP_CPU_3, cpuInfo[3]);
	result = atDataHash64(cpuInfo, sizeof(cpuInfo));

	// ====================================
	//   Combine Hash with Computer Name
	// ====================================
	wchar_t buffer[MAX_COMPUTERNAME_LENGTH+1] = {0};
	DWORD bufferSize = MAX_COMPUTERNAME_LENGTH+1;
	if (GetComputerNameW(buffer, &bufferSize) != 0)
	{
		result = atDataHash64(buffer, sizeof(buffer), result);
		AddFingerPrintStrW(FP_COMPUTER_NAME, buffer);
	}
	else
	{
		AddFingerPrint(FP_COMPUTER_NAME, FP_UNKNOWN_STR);
	}

	// =====================================
	//   Combine Hash with Drive Information
	// =====================================
	bool bParsedVolumeInformation = false;
	CHAR systemDrive[64] = {0};
	if (GetEnvironmentVariableA("SystemDrive", systemDrive, COUNTOF(systemDrive)) != 0)
	{
		safecat(systemDrive, "\\");

		CHAR volumeName[MAX_PATH+1] = {0};
		CHAR fileSystemName[MAX_PATH+1] = {0};
		DWORD serialNumber, maxComponentLen, fileSystemFlags;
		if (GetVolumeInformationA(systemDrive, volumeName, MAX_PATH, &serialNumber, &maxComponentLen, &fileSystemFlags, fileSystemName, MAX_PATH) != 0)
		{
			AddFingerPrintUns(FP_VOLUME_SERIAL, serialNumber);
			AddFingerPrintStr(FP_VOLUME_NAME, volumeName);
			AddFingerPrintStr(FP_FILE_SYSTEM_NAME, fileSystemName);

			result = atDataHash64(&serialNumber, sizeof(serialNumber), result);
			result = atDataHash64(volumeName, sizeof(volumeName), result);
			result = atDataHash64(fileSystemName, sizeof(fileSystemName), result);
			bParsedVolumeInformation = true;
		}
	}

	if (!bParsedVolumeInformation)
	{
		AddFingerPrint(FP_VOLUME_SERIAL, FP_UNKNOWN_STR);
		AddFingerPrint(FP_VOLUME_NAME, FP_UNKNOWN_STR);
		AddFingerPrint(FP_FILE_SYSTEM_NAME, FP_UNKNOWN_STR);
	}

	// add "device_name"
	char deviceName[128] = {0};

	//	NOTE: We want to share logins between titles, so this is replaced with a generic "scui" for now.
	//	If the backend is updated to support this, prefer the Title Name again if possible.
	//formatf(deviceName, "%s on %s", m_TitleId.GetRosTitleName(), "PC");
	formatf(deviceName, "%s on %s", "Rockstar Games", "PC");
	AddFingerPrint(FP_DEVICE_NAME, deviceName);

	return result;
}

void RgscMultiFactorAuth::GetFingerprintJSON(JSONNODE* fpJsonNode)
{
	if (!fpJsonNode)
		return;

	// enumerate the fingerprint objects and append to the node.
	for (int i = 0; i < m_FingerprintKvp.GetCount(); i++)
	{
		json_push_back(fpJsonNode, json_new_a(m_FingerprintKvp[i].key.c_str(), m_FingerprintKvp[i].value.c_str()));
	}
}

void RgscMultiFactorAuth::GetFingerPrintRSON(RsonWriter* rw)
{
	if (!rw)
		return;

	// enumerate the fingerprint objects and append to the node.
	for (int i = 0; i < m_FingerprintKvp.GetCount(); i++)
	{
		rw->WriteString(m_FingerprintKvp[i].key.c_str(), m_FingerprintKvp[i].value.c_str());
	}
}

} // namespace rgsc


