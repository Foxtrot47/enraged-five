// 
// rline/metadata.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPCMETADATA_H 
#define RLINE_RLPCMETADATA_H

#include "file/file_config.h"

#if RSG_PC

#include "memory.h"

#include "atl/array.h"
#include "atl/map.h"
#include "file/device.h"
#include "net/status.h"

using namespace rage;

namespace rgsc
{
    class RgscMetadata
    {
		friend class Table;

	public:
		class Table
		{
			friend class RgscMetadata;

		public:
			Table(RgscMetadata* database, u32 offset);
			~Table();
			bool GetRecordOffsetAndSize(u32 key, u32 &offset, u32 &size);

			template<typename T> bool 
			GetRecord(u32 key, T& record)
			{
				u32 offset = 0;
				u32 size = 0;
				bool exists = GetRecordOffsetAndSize(key, offset, size);
				if(exists)
				{
					// attempt to allocate the read buffer on the stack if we can
					u8 stackBuf[1024];
					void* buf = (void*)stackBuf;
					if(size > sizeof(stackBuf))
					{
						// buf size is too large, allocate on the heap instead
						buf = RGSC_ALLOCATE(RgscMetadata, size);
					}

					if(buf)
					{
						int bytesRead = Read(offset, buf, size);
						if(bytesRead == (int)size)
						{
							record.Import(buf, size);
						}

						if(buf != stackBuf)
						{
							RGSC_FREE(buf);
						}
					}
				}
				return exists;
			}

			u32 GetMinKey();
			u32 GetMaxKey();
			u32 GetNumRecords();

		private:

			class Index
			{
				friend class Table;

			private:
				enum IndexType
				{
					NO_INDEX = 0,
					ARRAY_INDEX = 1,
					HASHTABLE_INDEX = 2,
					BTREE_INDEX = 3,
				};

				static const u32 INVALID_OFFSET = 0xFFFFFFFF;

				class Metadata
				{
					friend class Table;
					friend class Index;

					static const unsigned MAX_BYTE_SIZEOF_PAYLOAD = sizeof(u8) +		// m_IndexType
																	sizeof(u8) +		// m_KeySize
																	sizeof(u8) +		// m_OffsetSize
																	sizeof(u8) +		// m_Reserved
																	sizeof(u32) +		// m_MinRecordSize
																	sizeof(u32) +		// m_MaxRecordSize
																	sizeof(u32) +		// m_MinKey
																	sizeof(u32) +		// m_MaxKey
																	sizeof(u32) +		// m_NumRecords
																	sizeof(u32) +		// m_NumDummyRecords
																	sizeof(u32) +		// m_TotalIndexSize
																	sizeof(u32);		// m_TotalDataSize

					IndexType m_IndexType;
					u8 m_KeySize;
					u8 m_OffsetSize;
					u8 m_Reserved;

					u32 m_MinRecordSize;
					u32 m_MaxRecordSize;
					u32 m_MinKey;
					u32 m_MaxKey;
					u32 m_NumRecords;
					u32 m_NumDummyRecords;
					u32 m_TotalIndexSize;
					u32 m_TotalDataSize;

					bool m_IsLoaded;

					IndexType GetIndexType() const {return m_IndexType;}
					u8 GetKeySize() const {return m_KeySize;}
					u8 GetOffsetSize() const {return m_OffsetSize;}
					u32 GetMinRecordSize() const {return m_MinRecordSize;}
					u32 GetMaxRecordSize() const {return m_MaxRecordSize;}
					u32 GetMinKey() const {return m_MinKey;}
					u32 GetMaxKey() const {return m_MaxKey;}
					u32 GetNumRecords() const {return m_NumRecords;}
					u32 GetNumDummyRecords() const {return m_NumDummyRecords;}
					u32 GetTotalIndexSize() const {return m_TotalIndexSize;}
					u32 GetTotalDataSize() const {return m_TotalDataSize;}

					bool IsLoaded() const {return m_IsLoaded;}

					Metadata();
					~Metadata();

					void Clear();

					bool Import(const void* buf,
								const unsigned sizeofBuf,
								unsigned* size = 0);
				};

				class KeyContainer
				{
					friend class Index;

				public:
					virtual ~KeyContainer() {};

				protected:
					KeyContainer(const Metadata &metadata) : m_Metadata(metadata) {}
					bool virtual Import(const void* buf,
								const unsigned sizeofBuf,
								unsigned* size = 0) = 0;
					bool virtual GetRecordOffsetAndSize(u32 key, u32 &offset, u32 &size) = 0;
					KeyContainer& operator=(const KeyContainer&) {}
					const Metadata &m_Metadata;
				};

				class ImplicitKC : public KeyContainer
				{
					friend class Index;
					ImplicitKC(const Metadata &metadata) : KeyContainer(metadata) {}
					bool Import(const void* buf,
								const unsigned sizeofBuf,
								unsigned* size = 0);
					bool GetRecordOffsetAndSize(u32 key, u32 &offset, u32 &size);
				};

				class SequentialArrayKC : public KeyContainer
				{
				public:
					~SequentialArrayKC();
				protected:
					friend class Index;
					SequentialArrayKC(const Metadata &metadata);
					
					bool Import(const void* buf,
								const unsigned sizeofBuf,
								unsigned* size = 0);
					bool GetRecordOffsetAndSize(u32 key, u32 &offset, u32 &size);
					void* m_Array;
				};

				class HashTableKC : public KeyContainer
				{
				public:
					~HashTableKC();
				protected:
					friend class Index;
					HashTableKC(const Metadata &metadata);
					bool Import(const void* buf,
								const unsigned sizeofBuf,
								unsigned* size = 0);
					bool GetRecordOffsetAndSize(u32 key, u32 &offset, u32 &size);
					typedef atMap<u32, u64> HashTable;
					HashTable *m_HashTable;
				};

				Index();
				~Index();

				void SetOwningTable(Table* table) {m_Table = table;}

				KeyContainer* CreateKeyContainer(const Metadata &metadata);

				bool IsLoaded() {return m_IsLoaded;}
				void Clear();
				bool ReadMetadata();
				bool Read();
				bool GetRecordOffsetAndSize(u32 key, u32 &offset, u32 &size);

				Table* m_Table;
				bool m_IsLoaded;
				Metadata m_Metadata;
				KeyContainer* m_KeyContainer;
			};

			RgscMetadata* m_Database;
			u32 m_Offset;
			Index m_Index;
			bool m_IsLoaded;

			bool IsLoaded() const {return m_IsLoaded;}

			void Clear();
			int Read(u32 offset,
					 void* buf,
					 const unsigned sizeOfBuf);
		};

	public:
		RgscMetadata();
		~RgscMetadata();

		bool Init();
		void Update();
		void Shutdown();
		Table* GetTable(const char* tableName);
		u32 GetVersion() { return m_Version; }

	private:
		int Read(u32 offset,
				 void* buf,
				 const unsigned sizeOfBuf);

		bool IsValidVersion(u32 version);

		const fiDevice* m_Device;
		fiHandle m_FileHandle;
		Table* m_Tables;
		u32 m_Version;
	};
} // namespace rgsc

#endif // RSG_PC

#endif // RLINE_RLPCMETADATA_H 
