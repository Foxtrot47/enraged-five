// 
// rgsc/rgsc/cloudsave.cpp
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 

// rgsc_includes
#include "file/file_config.h"
#include "cloudsave.h"
#include "rgsc.h"
#include "rgsc/diag/output.h"
#include "tasks.h"

// rage includes
#include "file/asset.h"
#include "file/device.h"
#include "net/netaddress.h"
#include "rline/rl.h"
#include "rline/cloudsave/rlcloudsave.h"
#include "rline/rltelemetry.h"
#include "string/stringutil.h"

// Name of the cloud save metadata file.
#define RGSC_CLOUDSAVE_MANIFEST "cloudsavedata.dat"

// Extensions used by cloud save files
#define RGSC_CLOUDSAVE_EXTENSION ".csf"
#define RGSC_CLOUDSAVE_BACKUP_EXTENSION ".csb%d"
#define RGSC_CLOUDSAVE_TEMP_EXTENSION ".temp%d"
#define RGSC_CLOUDSAVE_BACKUP_INFO_EXTENSION ".csbe%d"

// We want to ensure we have enough room to create cloudsave paths.
#define RGSC_LARGEST_PATH_EXTENSION (COUNTOF(RGSC_CLOUDSAVE_TEMP_EXTENSION))

// Empty metadata JSON
#define EMPTY_CLOUD_METADATA "{}"

// Compile time check to enable cloud saves md5 hashing
#define RGSC_CHECK_CLOUDSAVE_MD5 (1)

// The backup files use 1 bit per slot up to a maximum of 64 slots (s64)
#define MAX_NUM_BACKUP_SLOTS (64)
CompileTimeAssert(ICloudSaveManagerV3::RGSC_CLOUDSAVE_DEFAULT_NUM_BACKUP_FILES <= MAX_NUM_BACKUP_SLOTS);

#define RetailCloudSaveTaskLog(fmt, ...) \
	rlTaskDebug3(fmt, ##__VA_ARGS__); \
	RgscDisplay(fmt, ##__VA_ARGS__); 

namespace rage
{
	RAGE_DEFINE_SUBCHANNEL(rline, rgsccloudsave)
	#undef __rage_channel
	#define __rage_channel rline_rgsccloudsave

	extern sysThreadPool netThreadPool;
}

namespace rgsc
{
	CompileTimeAssert(ICloudSaveManager::HARDWARE_ID_MAX_BUF_SIZE == rlCloudSaveFile::HARDWARE_ID_MAX_BUF_SIZE);
	CompileTimeAssert(ICloudSaveManager::HARDWARE_ID_MAX_LEN == rlCloudSaveFile::HARDWARE_ID_MAX_LEN);
	CompileTimeAssert(ICloudSaveManager::MAX_CLOUD_SAVE_SLOTS == rlCloudSaveManifest::MAX_CLOUD_SAVE_SLOTS);
	CompileTimeAssert(ICloudSaveManagerV3::RGSC_MAX_COHORTS_CLOUD_FILE_LENGTH == rlQueryCloudSaveBetaAccess::MAX_COHORTS_CLOUD_FILE_LENGTH);
	CompileTimeAssert(ICloudSaveManagerV3::RGSC_MAX_COHORTS_CLOUD_FILE_BUF_SIZE == rlQueryCloudSaveBetaAccess::MAX_COHORTS_CLOUD_FILE_BUF_SIZE);

#if !__NO_OUTPUT
	const char* RgscGetConflictStateString(ICloudSaveFileV2::ConflictState conflict)
	{
		switch(conflict)
		{
		case ICloudSaveFileV2::ConflictState::CS_None:
			return "None";
		case ICloudSaveFileV2::ConflictState::CS_FileMissing:
			return "FileMissing";
		case ICloudSaveFileV2::ConflictState::CS_ServerHasNewer:
			return "ServerHasNewer";
		case ICloudSaveFileV2::ConflictState::CS_ClientHasNewer:
			return "ClientHasNewer";
		case ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict:
			return "ClientHasNewerWithConflict";
		case ICloudSaveFileV2::ConflictState::CS_FileDeleted:
			return "FileDeleted";
		case ICloudSaveFileV2::ConflictState::CS_FileDeletedWithConflict:
			return "FileDeletedWithConflict";
		case ICloudSaveFileV2::ConflictState::CS_ServerHasNewerWithConflict:
			return "ServerHasNewerWithConflict";
		}

		return "";
	}
#endif

	const char* PrintFinishType(rlTaskBase::FinishType finishType)
	{
		switch (finishType)
		{
		case rlTaskBase::FINISH_SUCCEEDED:
			return "success";
		case rlTaskBase::FINISH_FAILED:
			return "failure";
		case rlTaskBase::FINISH_CANCELED:
			return "cancellation";
		}

		return "unknown";
	}

	rage::eCloudSaveResolveType RgscToRageResolveType(ICloudSaveFileV2::ConflictResolutionType resolutionType)
	{
		switch(resolutionType)
		{
		case ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptLocal:
			return eCloudSaveResolveType::AcceptLocal;
		case ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptRemote:
			return eCloudSaveResolveType::AcceptRemote;
		case ICloudSaveFileV2::ConflictResolutionType::CRT_None:
			return eCloudSaveResolveType::None;
		}

		return eCloudSaveResolveType::None;
	}

	u64 WindowsTimeToPosixTime(u64 fileTime)
	{
		static u64 TICKS_PER_SECOND = 10000000;
		static u64 WINDOWS_TO_UNIX = 11644473600;
		return (fileTime / TICKS_PER_SECOND) - WINDOWS_TO_UNIX;
	}

	u64 PosixTimeToWindowsTime(u64 posixTime)
	{
		static u64 TICKS_PER_SECOND = 10000000;
		static u64 WINDOWS_TO_UNIX = 11644473600;
		return (posixTime + WINDOWS_TO_UNIX) * TICKS_PER_SECOND;
	}

	bool BuildFullPath(char (&path)[RGSC_MAX_PATH], ICloudSaveTitleConfigurationV1* config, ICloudSaveFileV1* file)
	{
		path[0] = '\0';

		rtry
		{
			// Begin with grabbing the save directory
			safecpy(path, config->GetSaveDirectory());

			// Ensure its backslash terminated. Lets assert here to get it fixed in the config.
			if (!rlVerify(StringEndsWithI(path, "\\")))
			{
				safecat(path, "\\");
			}

			// Verify that the path we are generating is within the bounds of RGSC_MAX_PATH, error if not.
			int requiredLength = (int)(strlen(path) + strlen(file->GetFileName()) + RGSC_LARGEST_PATH_EXTENSION);
			rverify(requiredLength < RGSC_MAX_PATH, catchall, );

			// Add file name to path
			const char* fileName = file->GetFileName();
			rverify(fileName && fileName[0] != '\0', catchall, );
			safecat(path, fileName);

			return true;
		}
		rcatchall
		{
			path[0] = '\0';
			return false;
		}
	}

	// Returns the path where backups will be stored for a file.
	//	 .....\{SaveDirectory}\backup\filename
	bool BuildBackupPath(char (&path)[RGSC_MAX_PATH], ICloudSaveTitleConfigurationV1* config, ICloudSaveFileV1* file)
	{
		path[0] = '\0';

		rtry
		{
			// Begin with grabbing the save directory
			safecpy(path, config->GetSaveDirectory());

			// Ensure its backslash terminated. Lets assert here to get it fixed in the config.
			if (!rlVerify(StringEndsWithI(path, "\\")))
			{
				safecat(path, "\\");
			}

			// Verify that the path we are generating is within the bounds of RGSC_MAX_PATH, error if not.
			int requiredLength = (int)(strlen(path) + strlen("backup\\") + strlen(file->GetFileName()) + RGSC_LARGEST_PATH_EXTENSION);
			rverify(requiredLength < RGSC_MAX_PATH, catchall, );

			// append backup folder
			safecat(path, "backup\\");

			// get device for this path
			const fiDevice* device = fiDevice::GetDevice(path);
			rverify(device, catchall, );

			// create directory if it doesn't exist
			u32 attribs = device->GetAttributes(path);
			if (!(attribs != INVALID_FILE_ATTRIBUTES && attribs & FILE_ATTRIBUTE_DIRECTORY))
			{
				rlDebug3("Backups folder did not exist, creating: '%s'", path);
				device->MakeDirectory(path);
			}

			// Add file name to path
			const char* fileName = file->GetFileName();
			rverify(fileName && fileName[0] != '\0', catchall, );
			safecat(path, fileName);

			return true;
		}
		rcatchall
		{
			path[0] = '\0';
			return false;
		}
	}

	void ClearReadOnlyAttribute(const fiDevice* device, char (&path)[RGSC_MAX_PATH])
	{
		u32 dstAttributes = device->GetAttributes(path);
		if (dstAttributes != INVALID_FILE_ATTRIBUTES && dstAttributes & FILE_ATTRIBUTE_READONLY)
		{
			rlDebug3("Removing read-only attribute from path '%s'", path);
			device->SetAttributes(path, dstAttributes & ~FILE_ATTRIBUTE_READONLY);
		}
	}

	void FillOutHttpDebugInfo(ICloudSaveManifest* manifest, const netHttpRequest::DebugInfo& debugInfo)
	{
		rtry
		{
			// ==========
			//	manifest
			// ==========
			rverify(manifest, catchall, );

			ICloudSaveManifestV4* v4 = NULL;
			RGSC_HRESULT hr = manifest->QueryInterface(IID_ICloudSaveManifestV4, (void**)&v4);
			rcheck(SUCCEEDED(hr) && v4, catchall, );

			// ============
			//	debug info
			// ============
			ICloudSaveHttpDebugInfo* info = v4->GetHttpDebugInfo();
			rcheck(info, catchall, );

			// V1 Details
			ICloudSaveHttpDebugInfoV1* v1 = NULL;
			hr = info->QueryInterface(IID_ICloudSaveHttpDebugInfoV1, (void**)&v1);
			rcheck(SUCCEEDED(hr) && v1, catchall, );

			v1->SetCommitted(debugInfo.m_Committed);
			v1->SetInContentBytesRcvd(debugInfo.m_InContentBytesRcvd);
			v1->SetOutContentBytesSent(debugInfo.m_OutChunkBytesSent);
			v1->SetRecvState(debugInfo.m_RecvState);
			v1->SetSendState(debugInfo.m_SendState);
			v1->SetHttpStatusCode(debugInfo.m_HttpStatusCode);

			// V2 Details
			ICloudSaveHttpDebugInfoV2* v2 = NULL;
			hr = info->QueryInterface(IID_ICloudSaveHttpDebugInfoV2, (void**)&v2);
			rcheck(SUCCEEDED(hr) && v2, catchall, );

			v2->SetAbortReason(debugInfo.m_AbortReason);
			v2->SetHadMemoryAllocationError(debugInfo.m_ChunkAllocationError);
			v2->SetTcpResultState((int)debugInfo.m_TcpResult);
			v2->SetTcpError(debugInfo.m_TcpError);
		}
		rcatchall
		{

		}
	}

	const char* GetAccessTokenFromManifest(ICloudSaveManifest* manifest)
	{
		rtry
		{
			RGSC_HRESULT hr = RGSC_OK;

			ICloudSaveManifestV2* manifestV2 = NULL;
			hr = manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&manifestV2);
			rcheck(SUCCEEDED(hr) && manifestV2, catchall,);

			ICloudSaveTitleConfiguration* config = manifestV2->GetConfiguration();
			rcheck(config, catchall, );

			ICloudSaveTitleConfigurationV2* configV2 = NULL;
			hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV2, (void**)&configV2);
			rcheck(SUCCEEDED(hr) && configV2, catchall, );

			return configV2->GetTitleAccessToken();
		}
		rcatchall
		{
			return NULL;
		}
	}

	struct RgscCloudSaveFileMetricInfo
	{
		RgscCloudSaveFileMetricInfo()
		{
			m_FileIndex = 0;
			m_Duration = 0;
			m_FileSize = 0;
			m_HttpStatusCode = 0;
			m_bDidWork = false;
		}

		s32 m_Duration;
		s32 m_FileSize;
		s8 m_FileIndex;
		s16 m_HttpStatusCode;
		bool m_bDidWork;
	};

	// ===============================================================================================
	// RgscCloudSaveConflictMetric
	// ===============================================================================================
	class RgscCloudSaveConflictMetric : public rlMetric
	{
		RL_DECLARE_METRIC(SC_CS_CONFLICT, rlTelemetryPolicies::DEFAULT_RAGE_LOG_CHANNEL, 3);

	public:

		RgscCloudSaveConflictMetric(ICloudSaveManifestV2* manifest) 
			: m_Manifest(manifest) 
		{

		}

		bool Write(RsonWriter* rw) const
		{
			rtry
			{
				// extract hardware id from configuration
				ICloudSaveTitleConfiguration* config = m_Manifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify(SUCCEEDED(hr) && configV1, catchall, );

				netIpAddress ipAddr;
				rverify(netHardware::GetPublicIpAddress(&ipAddr), catchall, );

				char publicIpAddress[netIpAddress::MAX_STRING_BUF_SIZE] = {0}; 
				ipAddr.Format(publicIpAddress); 

				rverify(rw->WriteString("h",configV1->GetHardwareId()), catchall, );
				rverify(rw->WriteString("i", publicIpAddress), catchall, );
				rverify(rw->WriteInt("pl", GetRgscConcreteInstance()->GetTitleId()->GetPlatform()), catchall, );

				rw->BeginArray("a", NULL);

				// Iterate through the entire configuration
				for (unsigned configIndex = 0; configIndex < configV1->GetNumCloudFiles(); configIndex++)
				{
					// Iterate through the manifest, searching for the file with the name that matches the configuration's file list at the current index
					for (unsigned manifestIndex = 0; manifestIndex < m_Manifest->GetNumFiles(); manifestIndex++)
					{
						// Ensure the manifest file is a ICloudSaveFileV2
						ICloudSaveFile* manifestFile = m_Manifest->GetFile(manifestIndex);
						ICloudSaveFileV2* rgscSaveFileV2 = NULL;
						if (SUCCEEDED(manifestFile->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2)
						{
							// If the manifest file and the configuration file match
							if (stricmp(rgscSaveFileV2->GetFileName(), configV1->GetFileName(configIndex)) == 0)
							{
								rw->Begin(NULL, NULL);
								rw->WriteInt("f", configIndex); // file index in the configuration
								rw->WriteInt("c", (int)rgscSaveFileV2->GetConflictState());
								rw->WriteInt("r", (int)rgscSaveFileV2->GetResolveType());
								rw->End();
								break;
							}
						}
					}
				}

				rw->End();

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

	private:
		ICloudSaveManifestV2* m_Manifest;
	};

	// ===============================================================================================
	// RgscCloudSavePostFilesMetric
	// ===============================================================================================
	class RgscCloudSavePostFilesMetric : public rlMetric
	{
		RL_DECLARE_METRIC(SC_CS_POST, rlTelemetryPolicies::DEFAULT_RAGE_LOG_CHANNEL, 3);

	public:

		RgscCloudSavePostFilesMetric()
			: m_Manifest(NULL) 
		{

		}

		bool Write(RsonWriter* rw) const

		{
			rtry
			{
				rverify(m_Manifest, catchall, );

				// extract hardware id from configuration
				ICloudSaveTitleConfiguration* config = m_Manifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify(SUCCEEDED(hr) && configV1, catchall, );

				netIpAddress ipAddr;
				rverify(netHardware::GetPublicIpAddress(&ipAddr), catchall, );

				char publicIpAddress[netIpAddress::MAX_STRING_BUF_SIZE] = {0}; 
				ipAddr.Format(publicIpAddress); 

				rverify(rw->WriteString("h",configV1->GetHardwareId()), catchall, );
				rverify(rw->WriteString("i", publicIpAddress), catchall, );
				rverify(rw->WriteInt("pl", GetRgscConcreteInstance()->GetTitleId()->GetPlatform()), catchall, );

				rw->BeginArray("a", NULL);

				for (int i = 0; i < COUNTOF(m_MetricInfo); i++)
				{
					const RgscCloudSaveFileMetricInfo& info = m_MetricInfo[i];
					if (info.m_bDidWork)
					{
						rw->Begin(NULL, NULL);
						rw->WriteInt("f", info.m_FileIndex); // file index in the configuration
						rw->WriteInt64("d", info.m_Duration);
						rw->WriteInt("c", info.m_HttpStatusCode);
						rw->WriteInt64("s", info.m_FileSize);
						rw->End();
					}
				}

				rw->End();

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		ICloudSaveManifestV2* m_Manifest;
		RgscCloudSaveFileMetricInfo m_MetricInfo[ICloudSaveManager::MAX_CLOUD_SAVE_SLOTS];
	};

	// ===============================================================================================
	// RgscCloudSaveGetFilesMetric
	// ===============================================================================================
	class RgscCloudSaveGetFilesMetric : public rlMetric
	{
		RL_DECLARE_METRIC(SC_CS_GET, rlTelemetryPolicies::DEFAULT_RAGE_LOG_CHANNEL, 3);

	public:

		RgscCloudSaveGetFilesMetric()
			: m_Manifest(NULL) 
		{

		}

		bool Write(RsonWriter* rw) const

		{
			rtry
			{
				rverify(m_Manifest, catchall, );

				// extract hardware id from configuration
				ICloudSaveTitleConfiguration* config = m_Manifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify(SUCCEEDED(hr) && configV1, catchall, );

				netIpAddress ipAddr;
				rverify(netHardware::GetPublicIpAddress(&ipAddr), catchall, );

				char publicIpAddress[netIpAddress::MAX_STRING_BUF_SIZE] = {0}; 
				ipAddr.Format(publicIpAddress); 

				rverify(rw->WriteString("h",configV1->GetHardwareId()), catchall, );
				rverify(rw->WriteString("i", publicIpAddress), catchall, );
				rverify(rw->WriteInt("pl", GetRgscConcreteInstance()->GetTitleId()->GetPlatform()), catchall, );

				rw->BeginArray("a", NULL);

				for (int i = 0; i < COUNTOF(m_MetricInfo); i++)
				{
					const RgscCloudSaveFileMetricInfo& info = m_MetricInfo[i];
					if (info.m_bDidWork)
					{
						rw->Begin(NULL, NULL);
						rw->WriteInt("f", info.m_FileIndex); // file index in the configuration
						rw->WriteInt64("d", info.m_Duration);
						rw->WriteInt("c", info.m_HttpStatusCode);
						rw->WriteInt64("s", info.m_FileSize);
						rw->End();
					}
				}

				rw->End();

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		ICloudSaveManifestV2* m_Manifest;
		RgscCloudSaveFileMetricInfo m_MetricInfo[ICloudSaveManager::MAX_CLOUD_SAVE_SLOTS];
	};

	// ===============================================================================================
	// RgscCloudSaveDeleteFilesMetric
	// ===============================================================================================
	class RgscCloudSaveDeleteFilesMetric : public rlMetric
	{
		RL_DECLARE_METRIC(SC_CS_DELETE, rlTelemetryPolicies::DEFAULT_RAGE_LOG_CHANNEL, 3);

	public:

		RgscCloudSaveDeleteFilesMetric()
			: m_Manifest(NULL) 
		{

		}

		bool Write(RsonWriter* rw) const

		{
			rtry
			{
				rverify(m_Manifest, catchall, );

				// extract hardware id from configuration
				ICloudSaveTitleConfiguration* config = m_Manifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify(SUCCEEDED(hr) && configV1, catchall, );

				netIpAddress ipAddr;
				rverify(netHardware::GetPublicIpAddress(&ipAddr), catchall, );

				char publicIpAddress[netIpAddress::MAX_STRING_BUF_SIZE] = {0}; 
				ipAddr.Format(publicIpAddress); 

				rverify(rw->WriteString("h",configV1->GetHardwareId()), catchall, );
				rverify(rw->WriteString("i", publicIpAddress), catchall, );
				rverify(rw->WriteInt("pl", GetRgscConcreteInstance()->GetTitleId()->GetPlatform()), catchall, );

				rw->BeginArray("a", NULL);

				for (int i = 0; i < COUNTOF(m_MetricInfo); i++)
				{
					const RgscCloudSaveFileMetricInfo& info = m_MetricInfo[i];
					if (info.m_bDidWork)
					{
						rw->Begin(NULL, NULL);
						rw->WriteInt("f", info.m_FileIndex); // file index in the configuration
						rw->WriteInt64("d", info.m_Duration);
						rw->WriteInt("c", info.m_HttpStatusCode);
						rw->WriteInt64("s", info.m_FileSize);
						rw->End();
					}
				}

				rw->End();

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		ICloudSaveManifestV2* m_Manifest;
		RgscCloudSaveFileMetricInfo m_MetricInfo[ICloudSaveManager::MAX_CLOUD_SAVE_SLOTS];
	};

	// ===============================================================================================
	// GetCloudSaveManifestTask
	// ===============================================================================================
	class GetCloudSaveManifestTask : public SimpleOperationBaseTask<RgscCloudSaveManager>
	{
	public:
		GetCloudSaveManifestTask() {}

		bool Configure( RgscCloudSaveManager* ctx, const RockstarId rockstarId, ICloudSaveManifest* manifest)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(manifest, catchall, );

				m_RgscManifest = manifest;

				return SimpleOperationBaseTask<RgscCloudSaveManager>::Configure(ctx, rockstarId);
			}
			rcatchall
			{
				return false;
			}
		}

	protected:

		virtual bool PerformOperation()
		{
			return rlCloudSave::GetCloudSaveManifest(RL_RGSC_GAMER_INDEX, &m_Manifest, GetAccessTokenFromManifest(m_RgscManifest), &m_MyStatus);
		}

		virtual void ProcessResult(bool success) 
		{
			if (success)
			{
				u32 count = m_Manifest.GetCount();
				for (unsigned i = 0; i < count; i++)
				{
					rlCloudSaveFile* rageSaveFile = m_Manifest.GetSaveFile(i);
					if (rageSaveFile)
					{
						ICloudSaveManifestV1* manifestV1 = NULL;
						if (SUCCEEDED(m_RgscManifest->QueryInterface(IID_ICloudSaveManifestV1, (void**)&manifestV1)) && manifestV1)
						{
							// Copy the bytes used property
							manifestV1->SetBytesUsed(m_Manifest.GetBytesUsed());

							// Add a file to the manifest
							ICloudSaveFile* rgscSaveFile = manifestV1->AddFile();
							if (rgscSaveFile)
							{
								ICloudSaveFileV1* rgscSaveFileV1 = NULL;
								if (SUCCEEDED(rgscSaveFile->QueryInterface(IID_ICloudSaveFileV1, (void**)&rgscSaveFileV1)) && rgscSaveFileV1)
								{
									// Copy the file properties
									rgscSaveFileV1->SetFileId(rageSaveFile->GetFileId());
									rgscSaveFileV1->SetMd5Hash(rageSaveFile->GetMd5Hash());
									rgscSaveFileV1->SetFileName(rageSaveFile->GetFileName());
									rgscSaveFileV1->SetFileVersion(rageSaveFile->GetFileVersion());
									rgscSaveFileV1->SetClientLastModifiedDate(rageSaveFile->GetClientLastModifiedDate());
									rgscSaveFileV1->SetServerLastModifiedDate(rageSaveFile->GetServerLastModifiedDate());
									rgscSaveFileV1->SetHardwareId(rageSaveFile->GetHardwareId());

									char ipBuf[netIpAddress::MAX_STRING_BUF_SIZE];
									const netIpAddress& address = rageSaveFile->GetLastIpAddress();
									address.Format(ipBuf);
									rgscSaveFileV1->SetLastIpAddress(ipBuf);	
								}

								ICloudSaveFileV2* rgscSaveFileV2 = NULL;
								if (SUCCEEDED(rgscSaveFile->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2)
								{
									// Copy the file properties
									rgscSaveFileV2->SetServerFileSize(rageSaveFile->GetServerFileSize());
									rgscSaveFileV2->SetServerMetadata(rageSaveFile->GetServerMetadata());
									rgscSaveFileV2->SetNextExpectedVersion(rageSaveFile->GetNextExpectedVersion());
								}
							}
						}
					}
				}
			}
		}

	private:

		rlCloudSaveManifest m_Manifest;
		ICloudSaveManifest* m_RgscManifest;
	};

	// ===============================================================================================
	// GetCloudSaveFileTask
	// ===============================================================================================
	class GetCloudSaveFileTask : public SimpleOperationBaseTask<RgscCloudSaveManager>
	{
	public:
		GetCloudSaveFileTask() {}

		bool Configure( RgscCloudSaveManager* ctx, const RockstarId rockstarId, ICloudSaveFile* file, ICloudSaveManager::ResolveType resolveType, const char* /*hardwareId*/)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(file, catchall, );

				m_RgscFile = file;

				switch(resolveType)
				{
				case None:
					m_ResolveType = eCloudSaveResolveType::None;
					break;
				case AcceptRemote:
					m_ResolveType = eCloudSaveResolveType::AcceptRemote;
					break;
				case AcceptLocal:
					m_ResolveType = eCloudSaveResolveType::AcceptLocal;
					break;
				}

				return SimpleOperationBaseTask<RgscCloudSaveManager>::Configure(ctx, rockstarId);
			}
			rcatchall
			{
				return false;
			}
		}

	protected:
		virtual bool PerformOperation()
		{
			return SetupFile() && rlCloudSave::GetFile(RL_RGSC_GAMER_INDEX, &m_File, m_ResolveType, NULL, &m_MyStatus);
		}

		virtual void ProcessResult(bool success) 
		{
			if (success)
			{
				ICloudSaveFileV1* rgscSaveFileV1 = NULL;
				if (SUCCEEDED(m_RgscFile->QueryInterface(IID_ICloudSaveFileV1, (void**)&rgscSaveFileV1)) && rgscSaveFileV1)
				{
					rgscSaveFileV1->CopyData(m_File.GetData(), m_File.GetDataLength());
					rgscSaveFileV1->SetMd5Hash(m_File.GetMd5Hash());
					rgscSaveFileV1->SetServerLastModifiedDate(m_File.GetServerLastModifiedDate());
				}
			}
		}

	private:

		bool SetupFile()
		{
			ICloudSaveFileV1* rgscSaveFileV1 = NULL;
			if (SUCCEEDED(m_RgscFile->QueryInterface(IID_ICloudSaveFileV1, (void**)&rgscSaveFileV1)) && rgscSaveFileV1)
			{
				m_File.SetFileId(rgscSaveFileV1->GetFileId());
				m_File.SetMd5Hash(rgscSaveFileV1->GetMd5Hash());
				m_File.SetFileName(rgscSaveFileV1->GetFileName());
				m_File.SetFileVersion(rgscSaveFileV1->GetFileVersion());
				m_File.SetClientLastModifiedDate(rgscSaveFileV1->GetClientLastModifiedDate());
				m_File.SetServerLastModifiedDate(rgscSaveFileV1->GetServerLastModifiedDate());
				m_File.SetHardwareId(rgscSaveFileV1->GetHardwareId());

				netIpAddress address;
				address.FromString(rgscSaveFileV1->GetLastIpAddress());
				m_File.SetLastIpAddress(address);

				return true;
			}

			return false;
		}

		rlCloudSaveFile m_File;
		ICloudSaveFile* m_RgscFile;
		eCloudSaveResolveType m_ResolveType;
	};

	// ===============================================================================================
	// PostCloudSaveFileTask
	// ===============================================================================================
	class PostCloudSaveFileTask : public SimpleOperationBaseTask<RgscCloudSaveManager>
	{
	public:
		PostCloudSaveFileTask() {}

		bool Configure( RgscCloudSaveManager* ctx, const RockstarId rockstarId, ICloudSaveFile* file, ICloudSaveManager::ResolveType resolveType)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(file, catchall, );

				m_RgscFile = file;

				switch(resolveType)
				{
				case None:
					m_ResolveType = eCloudSaveResolveType::None;
					break;
				case AcceptRemote:
					m_ResolveType = eCloudSaveResolveType::AcceptRemote;
					break;
				case AcceptLocal:
					m_ResolveType = eCloudSaveResolveType::AcceptLocal;
					break;
				}

				return SimpleOperationBaseTask<RgscCloudSaveManager>::Configure(ctx, rockstarId);
			}
			rcatchall
			{
				return false;
			}
		}

	protected:

		virtual bool PerformOperation()
		{
			return SetupFile() && rlCloudSave::PostFile(RL_RGSC_GAMER_INDEX, &m_File, m_ResolveType, NULL, &m_MyStatus);
		}

		virtual void ProcessResult(bool success) 
		{
			if (success)
			{
				ICloudSaveFileV1* rgscSaveFileV1 = NULL;
				if (SUCCEEDED(m_RgscFile->QueryInterface(IID_ICloudSaveFileV1, (void**)&rgscSaveFileV1)) && rgscSaveFileV1)
				{
					rgscSaveFileV1->SetFileId(m_File.GetFileId());
					rgscSaveFileV1->SetFileVersion(m_File.GetFileVersion());
					rgscSaveFileV1->SetFileName(m_File.GetFileName());
					rgscSaveFileV1->SetServerLastModifiedDate(m_File.GetServerLastModifiedDate());
				}
			}
		}

	private:

		bool SetupFile()
		{
			ICloudSaveFileV1* rgscSaveFileV1 = NULL;
			if (SUCCEEDED(m_RgscFile->QueryInterface(IID_ICloudSaveFileV1, (void**)&rgscSaveFileV1)) && rgscSaveFileV1)
			{
				m_File.SetFileId(rgscSaveFileV1->GetFileId());
				m_File.SetMd5Hash(rgscSaveFileV1->GetMd5Hash());
				m_File.SetFileName(rgscSaveFileV1->GetFileName());
				m_File.SetFileVersion(rgscSaveFileV1->GetFileVersion());
				m_File.SetClientLastModifiedDate(rgscSaveFileV1->GetClientLastModifiedDate());
				m_File.SetServerLastModifiedDate(rgscSaveFileV1->GetServerLastModifiedDate());
				m_File.SetHardwareId(rgscSaveFileV1->GetHardwareId());

				netIpAddress address;
				address.FromString(rgscSaveFileV1->GetLastIpAddress());
				m_File.SetLastIpAddress(address);

				m_File.CopyData(rgscSaveFileV1->GetData(), rgscSaveFileV1->GetDataLength());

				return true;
			}

			return false;
		}

		rlCloudSaveFile m_File;
		ICloudSaveFile* m_RgscFile;
		eCloudSaveResolveType m_ResolveType;
	};

	class PostUpdatedFilesTask : public rgscTask<RgscCloudSaveManager>
	{
	public:
		RL_TASK_DECL(PostUpdatedFilesTask);

		enum State
		{
			STATE_INVALID   = -1,
			STATE_POST_FILES,
			STATE_POSTING_FILES
		};

		PostUpdatedFilesTask()
			: m_State(STATE_INVALID)
			, m_LocalManifest(NULL)
			, m_LocalManifestV3(NULL)
			, m_ProgressTrackerV1(NULL)
			, m_ProgressTrackerV2(NULL)
			, m_CurrentIndex(0)
			, m_uLastPostTime(0)
			, m_FilesPosted(0)
			, m_bMetricsCollected(false)
		{

		}

		bool Configure(RgscCloudSaveManager* /*ctx*/, ICloudSaveManifest* manifest)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(manifest, catchall, );

				// V2 is the earliest accepted manifest for this task
				RGSC_HRESULT hr = manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&m_LocalManifest);
				rverify(SUCCEEDED(hr) && m_LocalManifest, catchall, );

				// V3 enables optional functionality
				hr = manifest->QueryInterface(IID_ICloudSaveManifestV3, (void**)&m_LocalManifestV3);
				if (SUCCEEDED(hr) && m_LocalManifestV3)
				{
					ICloudSaveOperationProgress* progress = m_LocalManifestV3->GetProgressTracker();
					rverify(progress, catchall, );

					hr = progress->QueryInterface(IID_ICloudSaveOperationProgressV1, (void**)&m_ProgressTrackerV1);
					rverify(SUCCEEDED(hr) && m_ProgressTrackerV1, catchall, );

					progress->QueryInterface(IID_ICloudSaveOperationProgressV2, (void**)&m_ProgressTrackerV2);
				}

				// Check if this manifest has cloud saves enabled.
				rverify(m_LocalManifest->GetCloudSaveEnabled() == ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES, catchall, );

				// verify conflicts are resolved
				rverify(m_LocalManifest->GetNumUnresolvedConflicts() == 0, catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		virtual void Start()
		{
			RetailCloudSaveTaskLog("Uploading files to the cloud.");

			this->rgscTask<RgscCloudSaveManager>::Start();

			if (m_ProgressTrackerV1)
			{
				m_ProgressTrackerV1->SetStepCount(0);
				m_ProgressTrackerV1->SetCompletedCount(0);
				m_ProgressTrackerV1->SetIsActive(true);

				SetupProgressTracker();
			}

			m_State = STATE_POST_FILES;
		}

		virtual void Finish(const FinishType finishType, const int resultCode = 0)
		{
			RetailCloudSaveTaskLog("Uploading files completed with %s (%d files uploaded) (code: %d)", PrintFinishType(finishType), m_FilesPosted, resultCode);

			if (finishType != rlTaskBase::FINISH_CANCELED)
			{
				WritePostTelemetry();

				if (m_ProgressTrackerV1)
				{
					m_ProgressTrackerV1->SetIsActive(false);
				}
			}

			this->rgscTask<RgscCloudSaveManager>::Finish(finishType, resultCode);
		}

		virtual bool IsCancelable() const
		{
			return true;
		}

		virtual void DoCancel()
		{
			RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
			rgscTask::DoCancel();
		}

		virtual void Update(const unsigned timeStep)
		{
			this->rgscTask<RgscCloudSaveManager>::Update(timeStep);

			if(this->WasCanceled())
			{
				//Wait for dependencies to finish
				if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_CANCELED);
				}

				return;
			}

			do 
			{
				switch(m_State)
				{
				case STATE_INVALID:
					break;
				case STATE_POST_FILES:
					if (HasEnumeratedAllFiles())
					{
						this->Finish(FINISH_SUCCEEDED);
					}
					else if (DoesFileRequireUpload(m_CurrentIndex))
					{
						if (TryPostCurrentFile())
						{
							m_uLastPostTime = sysTimer::GetSystemMsTime();
							m_State = STATE_POSTING_FILES;
						}
						else
						{
							this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV2::PostUpdatedFilesErrors::PUFE_ERROR_SETUP_FILE);
						}
					}
					else
					{
						m_CurrentIndex++;
					}
					break;
				case STATE_POSTING_FILES:
					if (!m_MyStatus.Pending())
					{
						if (m_MyStatus.Succeeded())
						{
							m_FilesPosted++;
							if (m_ProgressTrackerV1)
							{
								m_ProgressTrackerV1->SetCompletedCount(m_FilesPosted);
							}
							if (m_ProgressTrackerV2)
							{
								netHttpRequest::DebugInfo& debugInfo = m_PostFile.GetHttpDebugInfo();

								// We previously estimate progress based on the post file's size,
								//	but the chunked encoding adjusts this size slightly.
								u64 estimatedLength = m_ProgressTrackerV2->GetTotalLength();
								estimatedLength -= m_PostFile.GetDataLength();
								estimatedLength += debugInfo.m_OutContentBytesSent;
								m_ProgressTrackerV2->SetTotalLength(estimatedLength);

								u64 totalProgress = m_ProgressTrackerV2->GetTotalProgress();
								totalProgress += debugInfo.m_OutContentBytesSent;
								m_ProgressTrackerV2->SetTotalProgress(totalProgress);
							}
							AddCurrentMetric();
							ProcessSuccess();
							m_State = STATE_POST_FILES;
						}
						else
						{
							// copy http debug info from the post file request to the out manifest.
							const netHttpRequest::DebugInfo& debugInfo = m_PostFile.GetHttpDebugInfo();
							FillOutHttpDebugInfo(m_LocalManifest, debugInfo);

							RetailCloudSaveTaskLog("Cloud Save file upload failed. HTTP Code: %d, Reason: %d", debugInfo.m_HttpStatusCode, debugInfo.m_AbortReason);

							AddCurrentMetric();
							this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV2::PostUpdatedFilesErrors::PUFE_ERROR_POSTING_FILE);
						}
					}
					else if (m_ProgressTrackerV2)
					{
						netHttpRequest::DebugInfo& debugInfo = m_PostFile.GetHttpDebugInfo();
						m_ProgressTrackerV2->SetStepProgress(debugInfo.m_OutChunkBytesSent);
						m_ProgressTrackerV2->SetStepLength(debugInfo.m_OutContentBytesSent);
					}
					break;
				}
			}
			while(!m_MyStatus.Pending() && this->IsActive());
		}

	private:

		void AddCurrentMetric()
		{
			rtry
			{
				rverify(m_CurrentIndex < COUNTOF(m_PostMetric.m_MetricInfo), catchall, );

				// Extract the V1 configuration from the manifest.
				//	Manifest without a V1+ configuration cannot identify conflicts and will fail this task.
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify(SUCCEEDED(hr) && configV1, catchall, );

				// Iterate through the entire configuration
				bool bFoundMetricIndex = false;
				for (unsigned configIndex = 0; configIndex < configV1->GetNumCloudFiles(); configIndex++)
				{
					if (stricmp(m_PostFile.GetFileName(), configV1->GetFileName(configIndex)) == 0)
					{
						m_PostMetric.m_MetricInfo[m_CurrentIndex].m_FileIndex = (s8)configIndex;
						bFoundMetricIndex = true;
						break;
					}
				}

				rverify(bFoundMetricIndex, catchall, );
				m_PostMetric.m_MetricInfo[m_CurrentIndex].m_bDidWork = true;
				m_PostMetric.m_MetricInfo[m_CurrentIndex].m_Duration = (s32)(sysTimer::GetSystemMsTime() - m_uLastPostTime);
				m_PostMetric.m_MetricInfo[m_CurrentIndex].m_FileSize = (s32)m_PostFile.GetDataLength();
				m_PostMetric.m_MetricInfo[m_CurrentIndex].m_HttpStatusCode = (s16)m_MyStatus.GetResultCode();
				m_bMetricsCollected = true;
			}
			rcatchall
			{

			}
		}

		bool WritePostTelemetry()
		{
			rtry
			{
				rcheck(m_bMetricsCollected, catchall, );
				rverify(m_LocalManifest, catchall, );

				// Sanity check that posting this metric won't overflow
				rverify(m_LocalManifest->GetNumFiles() <= COUNTOF(m_PostMetric.m_MetricInfo), catchall, );
				m_PostMetric.m_Manifest = m_LocalManifest;

				const unsigned DOUBLE_MEMORY_POLICY = 1024; // double the size of the default policy

				rlMetricList list;
				list.Append(&m_PostMetric);

				rlTelemetrySubmissionMemoryPolicy memPolicy(DOUBLE_MEMORY_POLICY);
				rlTelemetry::SubmitMetricList(RL_RGSC_GAMER_INDEX, &list, &memPolicy);
				memPolicy.ReleaseAndClear();

#if !__NO_OUTPUT
				char postTelemetry[RL_TELEMETRY_DEFAULT_WORK_BUFFER_SIZE] = {0};
				RsonWriter rw(postTelemetry, RSON_FORMAT_JSON);
				m_PostMetric.Write(&rw);
				rlDebug3("WritePostTelemetry: %s", rw.ToString());
#endif

				return true;
			}
			rcatchall
			{
				return false;
			}
		}


		bool SetupFile(ICloudSaveFile* file)
		{
			rtry
			{
				// Clear the file before copying the rgsc file's properties
				m_PostFile.Clear();

				// Verify a V2 file for this operation
				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				// Copy over the data relevant for posting
				m_PostFile.SetFileId(rgscSaveFileV2->GetFileId());
				m_PostFile.SetMd5Hash(rgscSaveFileV2->GetMd5Hash());
				m_PostFile.SetFileName(rgscSaveFileV2->GetFileName());
				m_PostFile.SetFileVersion(rgscSaveFileV2->GetFileVersion());
				m_PostFile.SetClientLastModifiedDate(rgscSaveFileV2->GetClientLastModifiedDate());
				m_PostFile.SetServerLastModifiedDate(rgscSaveFileV2->GetServerLastModifiedDate());
				m_PostFile.SetNextExpectedVersion(rgscSaveFileV2->GetNextExpectedVersion());
				m_PostFile.SetMetaData(rgscSaveFileV2->GetMetaData());

				// extract hardware id from configuration
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify(SUCCEEDED(hr) && configV1, catchall, );
				m_PostFile.SetHardwareId(configV1->GetHardwareId());

				// construct the file path on disk
				char fullPath[RGSC_MAX_PATH];
				rverify(BuildFullPath(fullPath, configV1, rgscSaveFileV2), catchall, );
				rverify(fullPath[0] != '\0', catchall, );

				// From the full path, have the post file load the data from disk.
				rverify(m_PostFile.LoadData(fullPath), catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		bool HasEnumeratedAllFiles()
		{
			return m_CurrentIndex >= m_LocalManifest->GetNumFiles();
		}

		void SetupProgressTracker()
		{
			rtry
			{
				rcheck(m_ProgressTrackerV1, catchall, );

				u64 totalLength = 0;
				int numFilesToDownload = 0;
				for (unsigned i = 0; i < m_LocalManifest->GetNumFiles(); i++)
				{
					if (DoesFileRequireUpload(i))
					{
						numFilesToDownload++;

						ICloudSaveFile* file = m_LocalManifest->GetFile(i);
						if (file)
						{
							ICloudSaveFileV2* rgscSaveFileV2 = NULL;
							if (SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2)
							{
								totalLength += rgscSaveFileV2->GetClientFileSize();
							}
						}
					}
				}

				m_ProgressTrackerV1->SetStepCount(numFilesToDownload);

				rcheck(m_ProgressTrackerV2, catchall, );
				m_ProgressTrackerV2->SetStepProgress(0);
				m_ProgressTrackerV2->SetStepLength(0);
				m_ProgressTrackerV2->SetTotalProgress(0);
				m_ProgressTrackerV2->SetTotalLength(totalLength);
			}
			rcatchall
			{

			}
		}

		bool DoesFileRequireUpload(u32 index)
		{
			rtry
			{
				rverify(index < m_LocalManifest->GetNumFiles(), catchall, );

				ICloudSaveFile* file = m_LocalManifest->GetFile(index);
				rverify(file, catchall, );

				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				// Validate as an upload file
				if (rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_ClientHasNewer ||
					(rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict &&
					rgscSaveFileV2->GetResolveType() == ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptLocal) || 
					(rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_ServerHasNewerWithConflict &&
					rgscSaveFileV2->GetResolveType() == ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptLocal))
				{
					return true;
				}
			}
			rcatchall
			{
			}

			return false;
		}

		bool TryPostCurrentFile()
		{
			rtry
			{
				rverify(m_CurrentIndex < m_LocalManifest->GetNumFiles(), catchall, );
				m_PostFile.Clear();

				ICloudSaveFile* file = m_LocalManifest->GetFile(m_CurrentIndex);
				rverify(file, catchall, );
				rverify(SetupFile(file), catchall, );

				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				RetailCloudSaveTaskLog("Uploading cloud file '%s' (Version %d)", m_PostFile.GetFileName(), m_PostFile.GetFileVersion());
				
				if (m_ProgressTrackerV2)
				{
					m_ProgressTrackerV2->SetStepProgress(0);
					m_ProgressTrackerV2->SetStepLength(m_PostFile.GetDataLength());
				}
				
				rage::eCloudSaveResolveType resolveType = RgscToRageResolveType(rgscSaveFileV2->GetResolveType());
				const char* titleAccessToken = GetAccessTokenFromManifest(m_LocalManifest);
				return rlCloudSave::PostFile(RL_RGSC_GAMER_INDEX, &m_PostFile, resolveType, titleAccessToken, &m_MyStatus);

			}
			rcatchall
			{

			}

			return false;
		}

		void ProcessSuccess()
		{
			ICloudSaveFile* file = m_LocalManifest->GetFile(m_CurrentIndex);
			ICloudSaveFileV2* rgscSaveFileV2 = NULL;
			if (file && SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2)
			{
				rgscSaveFileV2->SetFileId(m_PostFile.GetFileId());
				rgscSaveFileV2->SetFileVersion(m_PostFile.GetFileVersion());
				rgscSaveFileV2->SetFileName(m_PostFile.GetFileName());
				rgscSaveFileV2->SetServerLastModifiedDate(m_PostFile.GetServerLastModifiedDate());
				rgscSaveFileV2->SetIsDirty(false);
				rgscSaveFileV2->SetConflictState(ICloudSaveFileV2::ConflictState::CS_None);
				rgscSaveFileV2->SetNextExpectedVersion(m_PostFile.GetNextExpectedVersion());

				RetailCloudSaveTaskLog("Cloud file uploaded: '%s' (Version %d)", rgscSaveFileV2->GetFileName(), rgscSaveFileV2->GetFileVersion());

				// update client last modified time in the manifest to the result of the server last modified time,
				//	and write that as the file time as well. This ensures the client time, server time both match so 
				//	the file will not be seen as dirty in the conflict identification.
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				if (SUCCEEDED(hr) && configV1)
				{
					char fullPath[RGSC_MAX_PATH];
					BuildFullPath(fullPath, configV1, rgscSaveFileV2);

					const fiDevice* device = fiDevice::GetDevice(fullPath);
					if (device)
					{
						// copy server last modified time to client
						rgscSaveFileV2->SetClientLastModifiedDate(rgscSaveFileV2->GetServerLastModifiedDate());

						// we have to convert to windows file time before we can write.
						u64 windowsTime = PosixTimeToWindowsTime(rgscSaveFileV2->GetClientLastModifiedDate());
						device->SetFileTime(fullPath, windowsTime);
					}
				}
			}

			m_CurrentIndex++;
		}

		State m_State;
		netStatus m_MyStatus;
		ICloudSaveManifestV2* m_LocalManifest;
		ICloudSaveManifestV3* m_LocalManifestV3;
		ICloudSaveOperationProgressV1* m_ProgressTrackerV1;
		ICloudSaveOperationProgressV2* m_ProgressTrackerV2;
		rlCloudSaveFile m_PostFile;
		unsigned m_CurrentIndex;
		u32 m_uLastPostTime;

		RgscCloudSavePostFilesMetric m_PostMetric;
		bool m_bMetricsCollected;

		int m_FilesPosted;
	};

	class GetUpdatedFilesTask : public rgscTask<RgscCloudSaveManager>
	{
	public:
		RL_TASK_DECL(GetUpdatedFilesTask);

		enum State
		{
			STATE_INVALID   = -1,
			STATE_GET_FILES,
			STATE_GETTING_FILES
		};

		GetUpdatedFilesTask()
			: m_State(STATE_INVALID)
			, m_LocalManifest(NULL)
			, m_LocalManifestV3(NULL)
			, m_ProgressTrackerV1(NULL)
			, m_ProgressTrackerV2(NULL)
			, m_CurrentIndex(0)
			, m_uLastGetTime(0)
			, m_FilesRetrieved(0)
			, m_bMetricsCollected(false)
		{

		}

		~GetUpdatedFilesTask()
		{
		}

		bool Configure(RgscCloudSaveManager* /*ctx*/, ICloudSaveManifest* manifest)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(manifest, catchall, );

				// V2 is the earliest accepted manifest for this task
				RGSC_HRESULT hr = manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&m_LocalManifest);
				rverify(SUCCEEDED(hr) && m_LocalManifest, catchall, );

				// V3 enables optional functionality
				hr = manifest->QueryInterface(IID_ICloudSaveManifestV3, (void**)&m_LocalManifestV3);
				if (SUCCEEDED(hr) && m_LocalManifestV3)
				{
					ICloudSaveOperationProgress* progress = m_LocalManifestV3->GetProgressTracker();
					rverify(progress, catchall, );

					hr = progress->QueryInterface(IID_ICloudSaveOperationProgressV1, (void**)&m_ProgressTrackerV1);
					rverify(SUCCEEDED(hr) && m_ProgressTrackerV1, catchall, );

					progress->QueryInterface(IID_ICloudSaveOperationProgressV2, (void**)&m_ProgressTrackerV2);
				}

				// Check if this manifest has cloud saves enabled.
				rverify(m_LocalManifest->GetCloudSaveEnabled() == ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES, catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		virtual void Start()
		{
			RetailCloudSaveTaskLog("Downloading files from the cloud.");

			this->rgscTask<RgscCloudSaveManager>::Start();

			if (m_ProgressTrackerV1)
			{
				m_ProgressTrackerV1->SetStepCount(0);
				m_ProgressTrackerV1->SetCompletedCount(0);
				m_ProgressTrackerV1->SetIsActive(true);

				SetupProgressTracker();
			}

			m_State = STATE_GET_FILES;
		}

		virtual void Finish(const FinishType finishType, const int resultCode = 0)
		{
			RetailCloudSaveTaskLog("Downloading files completed with %s (%d files downloaded) (code: %d)", PrintFinishType(finishType), m_FilesRetrieved, resultCode);

			if (finishType != rlTaskBase::FINISH_CANCELED)
			{
				WriteGetTelemetry();

				if (m_ProgressTrackerV1)
				{
					m_ProgressTrackerV1->SetIsActive(false);
				}
			}

			this->rgscTask<RgscCloudSaveManager>::Finish(finishType, resultCode);
		}

		virtual bool IsCancelable() const
		{
			return true;
		}

		virtual void DoCancel()
		{
			RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
			rgscTask::DoCancel();
		}

		virtual void Update(const unsigned timeStep)
		{
			this->rgscTask<RgscCloudSaveManager>::Update(timeStep);

			if(this->WasCanceled())
			{
				//Wait for dependencies to finish
				if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_CANCELED);
				}

				return;
			}

			do 
			{
				switch(m_State)
				{
				case STATE_INVALID:
					break;
				case STATE_GET_FILES:
					if (HasEnumeratedAllFiles())
					{
						this->Finish(FINISH_SUCCEEDED);
					}
					else if (DoesFileRequireDownload(m_CurrentIndex))
					{
						if (TryGetCurrentFile())
						{
							m_uLastGetTime = sysTimer::GetSystemMsTime();
							m_State = STATE_GETTING_FILES;
						}
						else
						{
							this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV2::GetUpdatedFilesErrors::GUFE_ERROR_SETUP_FILE);
						}
					}
					else
					{
						m_CurrentIndex++;
					}
					break;
				case STATE_GETTING_FILES:
					if (!m_MyStatus.Pending())
					{
						if (m_MyStatus.Succeeded())
						{
							m_FilesRetrieved++;
							if (m_ProgressTrackerV1)
							{
								m_ProgressTrackerV1->SetCompletedCount(m_FilesRetrieved);
							}
							if (m_ProgressTrackerV2)
							{
								netHttpRequest::DebugInfo& debugInfo = m_GetFile.GetHttpDebugInfo();
								u64 totalProgress = m_ProgressTrackerV2->GetTotalProgress();
								totalProgress += debugInfo.m_InContentBytesRcvd;
								m_ProgressTrackerV2->SetTotalProgress(totalProgress);
							}
							AddCurrentMetric();
							ProcessSuccess();
							m_State = STATE_GET_FILES;
						}
						else
						{
							// copy http debug info from the post file request to the out manifest.
							const netHttpRequest::DebugInfo& debugInfo = m_GetFile.GetHttpDebugInfo();
							FillOutHttpDebugInfo(m_LocalManifest, debugInfo);

							RetailCloudSaveTaskLog("Cloud Save file download failed. HTTP Code: %d, Reason: %d", debugInfo.m_HttpStatusCode, debugInfo.m_AbortReason);

							AddCurrentMetric();
							this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV2::GetUpdatedFilesErrors::GUFE_ERROR_GETTING_FILE);
						}
					}
					else if (m_ProgressTrackerV2)
					{
						netHttpRequest::DebugInfo& debugInfo = m_GetFile.GetHttpDebugInfo();
						m_ProgressTrackerV2->SetStepProgress(debugInfo.m_InContentBytesRcvd);
					}
					break;
				}
			}
			while(!m_MyStatus.Pending() && this->IsActive());
		}

	private:

		void AddCurrentMetric()
		{
			rtry
			{
				rverify(m_CurrentIndex < COUNTOF(m_GetMetric.m_MetricInfo), catchall, );

				// Extract the V1 configuration from the manifest.
				//	Manifest without a V1+ configuration cannot identify conflicts and will fail this task.
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify(SUCCEEDED(hr) && configV1, catchall, );

				// Iterate through the entire configuration
				bool bFoundMetricIndex = false;
				for (unsigned configIndex = 0; configIndex < configV1->GetNumCloudFiles(); configIndex++)
				{
					if (stricmp(m_GetFile.GetFileName(), configV1->GetFileName(configIndex)) == 0)
					{
						m_GetMetric.m_MetricInfo[m_CurrentIndex].m_FileIndex = (s8)configIndex;
						bFoundMetricIndex = true;
						break;
					}
				}

				rverify(bFoundMetricIndex, catchall, );
				m_GetMetric.m_MetricInfo[m_CurrentIndex].m_bDidWork = true;
				m_GetMetric.m_MetricInfo[m_CurrentIndex].m_Duration = (s32)(sysTimer::GetSystemMsTime() - m_uLastGetTime);
				m_GetMetric.m_MetricInfo[m_CurrentIndex].m_FileSize = (s32)m_GetFile.GetServerFileSize();
				m_GetMetric.m_MetricInfo[m_CurrentIndex].m_HttpStatusCode = (s16)m_MyStatus.GetResultCode();
				m_bMetricsCollected = true;
			}
			rcatchall
			{

			}
		}

		bool WriteGetTelemetry()
		{
			rtry
			{
				rcheck(m_bMetricsCollected, catchall, );
				rverify(m_LocalManifest, catchall, );

				// Sanity check that posting this metric won't overflow
				rverify(m_LocalManifest->GetNumFiles() <= COUNTOF(m_GetMetric.m_MetricInfo), catchall, );
				m_GetMetric.m_Manifest = m_LocalManifest;

				const unsigned DOUBLE_MEMORY_POLICY = 1024; // double the size of the default policy

				rlMetricList list;
				list.Append(&m_GetMetric);

				rlTelemetrySubmissionMemoryPolicy memPolicy(DOUBLE_MEMORY_POLICY);
				rlTelemetry::SubmitMetricList(RL_RGSC_GAMER_INDEX, &list, &memPolicy);
				memPolicy.ReleaseAndClear();

#if !__NO_OUTPUT
				char getTelemetry[DOUBLE_MEMORY_POLICY] = {0};
				RsonWriter rw(getTelemetry, RSON_FORMAT_JSON);
				m_GetMetric.Write(&rw);
				rlDebug3("WriteGetTelemetry: %s", rw.ToString());
#endif

				return true;
			}
			rcatchall
			{
				return false;
			}
		}


		bool HasEnumeratedAllFiles()
		{
			return m_CurrentIndex >= m_LocalManifest->GetNumFiles();
		}

		void SetupProgressTracker()
		{
			rtry
			{
				rcheck(m_ProgressTrackerV1, catchall, );

				u64 totalLength = 0;
				int numFilesToDownload = 0;
				for (unsigned i = 0; i < m_LocalManifest->GetNumFiles(); i++)
				{
					if (DoesFileRequireDownload(i))
					{
						numFilesToDownload++;

						ICloudSaveFile* file = m_LocalManifest->GetFile(i);
						if (file)
						{
							ICloudSaveFileV2* rgscSaveFileV2 = NULL;
							if (SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2)
							{
								totalLength += rgscSaveFileV2->GetClientFileSize();
							}
						}
					}
				}

				m_ProgressTrackerV1->SetStepCount(numFilesToDownload);

				rcheck(m_ProgressTrackerV2, catchall, );
				m_ProgressTrackerV2->SetStepProgress(0);
				m_ProgressTrackerV2->SetStepLength(0);
				m_ProgressTrackerV2->SetTotalProgress(0);
				m_ProgressTrackerV2->SetTotalLength(totalLength);
			}
			rcatchall
			{

			}
		}

		bool DoesFileRequireDownload(unsigned index)
		{
			rtry
			{
				rverify(index < m_LocalManifest->GetNumFiles(), catchall, );

				ICloudSaveFile* file = m_LocalManifest->GetFile(index);
				rverify(file, catchall, );

				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				// Files not on the server do not require metadata.
				//	A 'NextExpectedVersion' of 0 indicates a deleted file.
				if (rgscSaveFileV2->GetNextExpectedVersion() == 0)
					return false;

				// Validate as an download file
				if ((rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_ServerHasNewer) ||
					rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_FileMissing ||
					(rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_ServerHasNewerWithConflict && 
					rgscSaveFileV2->GetResolveType() == ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptRemote) ||
					(rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict &&
					rgscSaveFileV2->GetResolveType() == ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptRemote) ||
					(rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_FileDeletedWithConflict &&
					rgscSaveFileV2->GetResolveType() == ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptRemote))
				{
					return true;
				}
			}
			rcatchall
			{
			}

			return false;
		}

		bool TryGetCurrentFile()
		{
			rtry
			{
				rverify(m_CurrentIndex < m_LocalManifest->GetNumFiles(), catchall, );
				m_GetFile.Clear();

				ICloudSaveFile* file = m_LocalManifest->GetFile(m_CurrentIndex);
				rverify(file, catchall, );

				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );
				
				rverify(SetupFile(file), catchall, );

				RetailCloudSaveTaskLog("Downloading cloud file '%s' (Version %d)", rgscSaveFileV2->GetFileName(), rgscSaveFileV2->GetFileVersion());

				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify (SUCCEEDED(hr) && configV1, catchall, );

				// Create file path
				char fullPath[RGSC_MAX_PATH];
				rverify(BuildFullPath(fullPath, configV1, rgscSaveFileV2), catchall, );

				// Append the .csf extension
				safecat(fullPath, RGSC_CLOUDSAVE_EXTENSION); // ".csf"

				rage::eCloudSaveResolveType resolveType = RgscToRageResolveType(rgscSaveFileV2->GetResolveType());
				const char* titleAccessToken = GetAccessTokenFromManifest(m_LocalManifest);
				rverify(rlCloudSave::DownloadFile(RL_RGSC_GAMER_INDEX, &m_GetFile, resolveType, fullPath, m_CustomBounceBuffer, COUNTOF(m_CustomBounceBuffer), titleAccessToken, &m_MyStatus), catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		bool SetupFile(ICloudSaveFile* file)
		{
			ICloudSaveFileV2* rgscSaveFileV2 = NULL;
			if (SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2)
			{
				m_GetFile.SetFileId(rgscSaveFileV2->GetFileId());
				m_GetFile.SetMd5Hash(rgscSaveFileV2->GetMd5Hash());
				m_GetFile.SetFileName(rgscSaveFileV2->GetFileName());
				m_GetFile.SetFileVersion(rgscSaveFileV2->GetLatestServerVersion());
				m_GetFile.SetClientLastModifiedDate(rgscSaveFileV2->GetClientLastModifiedDate());
				m_GetFile.SetServerLastModifiedDate(rgscSaveFileV2->GetServerLastModifiedDate());
				m_GetFile.SetHardwareId(rgscSaveFileV2->GetHardwareId());
				m_GetFile.SetNextExpectedVersion(rgscSaveFileV2->GetNextExpectedVersion());
				m_GetFile.SetLatestServerVersion(rgscSaveFileV2->GetLatestServerVersion());
				m_GetFile.SetServerFileSize(rgscSaveFileV2->GetServerFileSize());

				netIpAddress address;
				address.FromString(rgscSaveFileV2->GetLastIpAddress());
				m_GetFile.SetLastIpAddress(address);

				// extract hardware id from configuration
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				if (SUCCEEDED(hr) && configV1)
				{
					m_GetFile.SetHardwareId(configV1->GetHardwareId());
				}

				return true;
			}

			return false;
		}

		void ProcessSuccess()
		{
			ICloudSaveManagerV2::GetUpdatedFilesErrors resultCode = ICloudSaveManagerV2::GetUpdatedFilesErrors::GUFE_ERROR_GETTING_FILE;

			rtry
			{
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify (SUCCEEDED(hr) && configV1, catchall, );

				// extract the file at this index
				ICloudSaveFile* file = m_LocalManifest->GetFile(m_CurrentIndex);
				rverify(file, catchall, );

				// verify its status as a genuine bona fide v2 file
				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				// validate the data we got matches what the manifest expected
				rverify(rgscSaveFileV2->GetServerFileSize() == m_GetFile.GetClientFileSize(), catchall, resultCode = ICloudSaveManagerV2::GetUpdatedFilesErrors::GUFE_ERROR_GETTING_FILE_INVALID_SIZE);

				// Create file path
				char fullPath[RGSC_MAX_PATH];
				rverify(BuildFullPath(fullPath, configV1, rgscSaveFileV2), catchall, );

				// Append the .csf extension
				safecat(fullPath, RGSC_CLOUDSAVE_EXTENSION); // ".csf"

#if RGSC_CHECK_CLOUDSAVE_MD5
				// The ::DownloadFile task will have successfully saved to disk, so we must validate md5 hash
				rverify(rlCloudSave::VerifyMd5Hash(fullPath, m_GetFile.GetMd5Hash(), NULL), catchall, resultCode = ICloudSaveManagerV2::GetUpdatedFilesErrors::GUFE_ERROR_GETTING_FILE_MD5_HASH);
#endif

				// copy over the new file version, metadata and md5 hash
				rgscSaveFileV2->SetFileVersion(m_GetFile.GetFileVersion());
				rgscSaveFileV2->SetMd5Hash(m_GetFile.GetMd5Hash());

				// These values come back from the GetFile API, so we can validate them against the manifest.
				rlAssert(rgscSaveFileV2->GetServerFileSize() == m_GetFile.GetServerFileSize());
				rlAssert(rgscSaveFileV2->GetLatestServerVersion() == m_GetFile.GetFileVersion());

				// flag the file as copyable
				rgscSaveFileV2->SetShouldCopyFile(true);

				RetailCloudSaveTaskLog("Cloud file downloaded: '%s' (Version %d)", rgscSaveFileV2->GetFileName(), rgscSaveFileV2->GetFileVersion());
			}
			rcatchall
			{
				this->Finish(FINISH_FAILED, (int)resultCode);
			}

			// increment the current index for the next iteration
			m_CurrentIndex++;
		}

		State m_State;
		netStatus m_MyStatus;
		ICloudSaveManifestV2* m_LocalManifest;
		ICloudSaveManifestV3* m_LocalManifestV3;
		ICloudSaveOperationProgressV1* m_ProgressTrackerV1;
		ICloudSaveOperationProgressV2* m_ProgressTrackerV2;
		rlCloudSaveFile m_GetFile;
		unsigned m_CurrentIndex;
		u32 m_uLastGetTime;

		RgscCloudSaveGetFilesMetric m_GetMetric;
		bool m_bMetricsCollected;

		// the default 8k bounce buffer in RageNet/HTTP is fairly small and results in slower downloads
		//	at lower tick rates (i.e. 30tick launcher). Use a larger bounce buffer for the request.
		// As a future improvement, a new CloudManager interface could expose a custom bounce buffer (and if
		// not provided, we could instead allocate the memory here).
		u8 m_CustomBounceBuffer[32 * 1024];

		unsigned m_FilesRetrieved;
	};

	class CopyUpdatedFilesTask : public rgscTask<RgscCloudSaveManager>
	{
	public:
		RL_TASK_DECL(CopyUpdatedFilesTask);

		enum State
		{
			STATE_INVALID   = -1,
			STATE_COPY_FILES
		};

		CopyUpdatedFilesTask()
			: m_State(STATE_INVALID)
			, m_LocalManifest(NULL)
			, m_FilesCopied(0)
		{

		}

		bool Configure(RgscCloudSaveManager* /*ctx*/, ICloudSaveManifest* manifest)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(manifest, catchall, );

				// V2 is the earliest accepted manifest for this task
				RGSC_HRESULT hr = manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&m_LocalManifest);
				rverify(SUCCEEDED(hr) && m_LocalManifest, catchall, );

				// Check if this manifest has cloud saves enabled.
				rverify(m_LocalManifest->GetCloudSaveEnabled() == ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES, catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		virtual void Start()
		{
			RetailCloudSaveTaskLog("Merging cloud files on disk.");

			this->rgscTask<RgscCloudSaveManager>::Start();

			m_State = STATE_COPY_FILES;
		}

		virtual void Finish(const FinishType finishType, const int resultCode = 0)
		{
			RetailCloudSaveTaskLog("Merging cloud files completed with %s (%d files merged) (code: %d)", PrintFinishType(finishType), m_FilesCopied, resultCode);

			this->rgscTask<RgscCloudSaveManager>::Finish(finishType, resultCode);
		}

		virtual void Update(const unsigned timeStep)
		{
			this->rgscTask<RgscCloudSaveManager>::Update(timeStep);

			if(this->WasCanceled())
			{
				//Wait for dependencies to finish
				if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_CANCELED);
				}

				return;
			}

			do 
			{
				switch(m_State)
				{
				case STATE_INVALID:
					break;
				case STATE_COPY_FILES:
					if (ValidateAllFilesPresent() && CopyFiles())
					{
						this->Finish(FINISH_SUCCEEDED);
					}
					else
					{
						this->Finish(rlTaskBase::FINISH_FAILED, (int)m_CopyError);
					}
					break;
				}
			}
			while(!m_MyStatus.Pending() && this->IsActive());
		}


		virtual bool IsCancelable() const
		{
			return true;
		}

		virtual void DoCancel()
		{
			RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
			rgscTask::DoCancel();
		}

	private:

		bool ValidateAllFilesPresent()
		{
			rtry
			{
				// Iterate through all files in the manifest that are flagged for copy. Generate the ".csf" path and verify
				//	that it exists for each file, and that the file has data (fileSize > 0). 

				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify (SUCCEEDED(hr) && configV1, catchall, );

				for (unsigned i = 0; i < m_LocalManifest->GetNumFiles(); i++)
				{
					ICloudSaveFile* file = m_LocalManifest->GetFile(i);
					rverify(file, catchall, );

					ICloudSaveFileV2* rgscSaveFileV2 = NULL;
					rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

					// file must have been flagged for copy ON THIS BOOT
					if (!rgscSaveFileV2->ShouldCopyFile())
						continue;

					// path to use when moving the save files
					char fullPath[RGSC_MAX_PATH];
					rverify(BuildFullPath(fullPath, configV1, rgscSaveFileV2), catchall, );

					// Create the 'cloudsave' path which is the full path + ".csf"
					char csPath[RGSC_MAX_PATH];
					safecpy(csPath, fullPath);
					safecat(csPath, RGSC_CLOUDSAVE_EXTENSION);

					const fiDevice* device = fiDevice::GetDevice(fullPath);
					rverify(device, catchall, );

					// validate that the cloudsave version exists
					u64 csFileSize = device->GetFileSize(csPath);
					rverify(csFileSize > 0, catchall, rlTaskError("File: %s did not exist or had no size (size: %" I64FMT "u)", csPath, csFileSize));
				}

				return true;
			}
			rcatchall
			{
				m_CopyError = ICloudSaveManagerV2::CopyUpdatedFilesErrors::CUFE_ERROR_FILE_MISSING;
				return false;
			}
		}

		bool CopyFiles()
		{
			rtry
			{
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify (SUCCEEDED(hr) && configV1, catchall, );

				for (unsigned i = 0; i < m_LocalManifest->GetNumFiles(); i++)
				{
					ICloudSaveFile* file = m_LocalManifest->GetFile(i);
					rverify(file, catchall, );

					ICloudSaveFileV2* rgscSaveFileV2 = NULL;
					rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

					// file must have been flagged for copy ON THIS BOOT
					if (!rgscSaveFileV2->ShouldCopyFile())
						continue;

					// path to use when moving the save files
					char destinationPath[RGSC_MAX_PATH];
					rverify(BuildFullPath(destinationPath, configV1, rgscSaveFileV2), catchall, );
					
					// Create the 'cloudsave' path which is the full path + ".csf"
					char csPath[RGSC_MAX_PATH];
					safecpy(csPath, destinationPath);
					safecat(csPath, RGSC_CLOUDSAVE_EXTENSION);

					const fiDevice* device = fiDevice::GetDevice(destinationPath);
					rverify(device, catchall, );

#if RGSC_CHECK_CLOUDSAVE_MD5
					// validate the md5 hash
					rverify(rlCloudSave::VerifyMd5Hash(csPath, rgscSaveFileV2->GetMd5Hash(), NULL), catchall, m_CopyError = ICloudSaveManagerV2::CopyUpdatedFilesErrors::CUFE_ERROR_VALIDATING_MD5_HASHES);
#endif

					// Check if the destination file is read only and replace it.
					ClearReadOnlyAttribute(device, destinationPath);

					// Rename ==> DeviceWin32 file copy
					rverify(device->Rename(csPath, destinationPath), catchall, m_CopyError = ICloudSaveManagerV2::CopyUpdatedFilesErrors::CUFE_ERROR_FILE_COPY_FAILED);

					// update the manifest - clear conflicts, clear dirty/delete flags
					rgscSaveFileV2->SetConflictState(ICloudSaveFileV2::ConflictState::CS_None);
					rgscSaveFileV2->SetResolveType(ICloudSaveFileV2::ConflictResolutionType::CRT_None);
					rgscSaveFileV2->SetShouldCopyFile(false);
					rgscSaveFileV2->SetIsDirty(false);
					rgscSaveFileV2->SetMarkedForDeletion(false);

					// update the manifest - using the server's last modify time, set this to the file time,
					//	and subsequently use this as our client's last modified time.
					rgscSaveFileV2->SetClientLastModifiedDate(rgscSaveFileV2->GetServerLastModifiedDate());

					// we have to convert to windows file time before we can write.
					u64 windowsTime = PosixTimeToWindowsTime(rgscSaveFileV2->GetClientLastModifiedDate());
					device->SetFileTime(destinationPath, windowsTime);

					// update the manifest - set the file's file size to that from the disk.
					u64 fileSize = device->GetFileSize(destinationPath);
					rgscSaveFileV2->SetClientFileSize(fileSize);

					// update the manifest - copy the metadata from server to client if valid
					if (rgscSaveFileV2->GetServerMetadata()[0] != '\0')
					{
						rgscSaveFileV2->SetMetaData(rgscSaveFileV2->GetServerMetadata());
					}

					rlTaskDebug1("Copied cloud save from '%s' to '%s'", csPath, destinationPath);
					m_FilesCopied++;
				}

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		State m_State;
		netStatus m_MyStatus;
		ICloudSaveManifestV2* m_LocalManifest;
		int m_FilesCopied;
		ICloudSaveManagerV2::CopyUpdatedFilesErrors m_CopyError;
	};


	class IdentifyConflictsTask : public rgscTask<RgscCloudSaveManager>
	{
	public:
		RL_TASK_DECL(IdentifyConflictsTask);

		enum State
		{
			STATE_INVALID   = -1,
			STATE_LOAD_LOCAL_MANIFEST,
			STATE_DOWNLOAD_SERVER_MANIFEST,
			STATE_DOWNLOADING_SERVER_MANIFEST,
			STATE_RESOLVE_SERVER_MANIFEST,
			STATE_SAVE_LOCAL_MANIFEST,
			STATE_SAVING_LOCAL_MANIFEST
		};

		IdentifyConflictsTask()
			: m_State(STATE_INVALID)
			, m_LocalManifest(NULL)
		{

		}

		bool Configure(RgscCloudSaveManager* /*ctx*/, ICloudSaveManifest* manifest)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(manifest, catchall, );
				
				// V2 is the earliest accepted manifest for this task
				RGSC_HRESULT hr = manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&m_LocalManifest);
				rverify(SUCCEEDED(hr) && m_LocalManifest, catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		virtual void Start()
		{
			RetailCloudSaveTaskLog("Identifying conflicts");

			this->rgscTask<RgscCloudSaveManager>::Start();

			m_State = STATE_LOAD_LOCAL_MANIFEST;
		}

		virtual void Finish(const FinishType finishType, const int resultCode = 0)
		{
			int numConflicts = 0;

			if (finishType != rlTaskBase::FINISH_CANCELED)
			{
				if (m_LocalManifest)
				{
					numConflicts = m_LocalManifest->GetNumUnresolvedConflicts();		
				}
			}

			RetailCloudSaveTaskLog("Identifying conflicts completed with %s (%d total conflicts) (code: %d)", PrintFinishType(finishType), numConflicts, resultCode);

			this->rgscTask<RgscCloudSaveManager>::Finish(finishType, resultCode);
		}

		virtual void Update(const unsigned timeStep)
		{
			this->rgscTask<RgscCloudSaveManager>::Update(timeStep);

			if(this->WasCanceled())
			{
				//Wait for dependencies to finish
				if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_CANCELED);
				}

				return;
			}

			do 
			{
				switch(m_State)
				{
				case STATE_INVALID:
					break;
				case STATE_LOAD_LOCAL_MANIFEST:
					if (LoadLocalManifest())
					{
						m_State = STATE_DOWNLOAD_SERVER_MANIFEST;
					}
					else
					{
						this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV2::IdentifyConflictsErrors::ICE_ERROR_LOAD_LOCAL_MANIFEST_FAILED);
					}
					break;
				case STATE_DOWNLOAD_SERVER_MANIFEST:
					if (rlCloudSave::GetCloudSaveManifest(RL_RGSC_GAMER_INDEX, &m_ServerManifest, GetAccessTokenFromManifest(m_LocalManifest), &m_MyStatus))
					{
						m_State = STATE_DOWNLOADING_SERVER_MANIFEST;
					}
					else
					{
						this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV2::IdentifyConflictsErrors::ICE_ERROR_SETUP_SERVER_MANIFEST_TASK_FAILED);
					}
					break;
				case STATE_DOWNLOADING_SERVER_MANIFEST:
					if(!m_MyStatus.Pending())
					{
						if (m_MyStatus.Succeeded())
						{
							m_State = STATE_RESOLVE_SERVER_MANIFEST;
						}
						else
						{
							// copy http debug info from the server manifest request to the out manifest.
							const netHttpRequest::DebugInfo& debugInfo = m_ServerManifest.GetHttpDebugInfo();
							FillOutHttpDebugInfo(m_LocalManifest, debugInfo);

							RetailCloudSaveTaskLog("Cloud Save server manifest download failed. HTTP Code: %d, Reason: %d", debugInfo.m_HttpStatusCode, debugInfo.m_AbortReason);

							this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV2::IdentifyConflictsErrors::ICE_ERROR_LOAD_SERVER_MANIFEST_FAILED);
						}
					}
					break;
				case STATE_RESOLVE_SERVER_MANIFEST:
					if (ResolveServerManifest())
					{
						this->Finish(FINISH_SUCCEEDED);
					}
					else
					{
						this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV2::IdentifyConflictsErrors::ICE_ERROR_MANIFEST_RESOLVE_FAILED);
					}
					break;
				}
			}
			while(!m_MyStatus.Pending() && this->IsActive());
		}

		virtual bool IsCancelable() const
		{
			return true;
		}

		virtual void DoCancel()
		{
			RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
			rgscTask::DoCancel();
		}

	private:

		bool LoadLocalManifest()
		{
			rtry
			{
				// Load the manifest from disk.
				rverify(GetRgscConcreteInstance()->_GetCloudSaveManager()->LoadManifest(m_LocalManifest), catchall, );

				// Extract the V1 configuration from the manifest.
				//	Manifest without a V1+ configuration cannot identify conflicts and will fail this task.
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify(SUCCEEDED(hr) && configV1, catchall, );

				// For each cloud file that is declared in the configuration...
				for (unsigned i = 0; i < configV1->GetNumCloudFiles(); i++)
				{
					// Create the save directory
					char fullPath[RGSC_MAX_PATH];
					safecpy(fullPath, configV1->GetSaveDirectory());

					// Ensure its backslash terminated. Lets assert here to let the game team fix the config.
					if (!rlVerify(StringEndsWithI(fullPath, "\\")))
					{
						safecat(fullPath, "\\");
					}

					// Extract the file name at the 'i' index
					const char* fileName = configV1->GetFileName(i);
					rverify(fileName && fileName[0] != '\0', catchall, );
					safecat(fullPath, fileName);

					const fiDevice* device = fiDevice::GetDevice(fullPath);
					rverify(device, catchall, );
						
					// Find it on disk. If it exists on disk...
					u64 fileSize = device->GetFileSize(fullPath);
					if (fileSize > 0)
					{
						// Extract the last modified time
						u64 fileTime = device->GetFileTime(fullPath);
						u64 fileLastModifiedTime = WindowsTimeToPosixTime(fileTime);

						ICloudSaveFile* saveFile = NULL;

						// Find this file in our local manifest
						for (unsigned fileIndex = 0; fileIndex < m_LocalManifest->GetNumFiles(); fileIndex++)
						{
							ICloudSaveFile* indexFile = m_LocalManifest->GetFile(fileIndex);
							ICloudSaveFileV2* rgscSaveFileV2 = NULL;
							if (SUCCEEDED(indexFile->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2)
							{
								// We found the file from the configuration in the local manifest
								if (stricmp(rgscSaveFileV2->GetFileName(), fileName) == 0)
								{
									if (!rgscSaveFileV2->IsDirty() && fileLastModifiedTime != rgscSaveFileV2->GetClientLastModifiedDate())
									{
										RetailCloudSaveTaskLog("Local file change detected in non-dirty file, clearing local metadata");
										rgscSaveFileV2->SetMetaData(EMPTY_CLOUD_METADATA);
									}

									rlAssert(fileLastModifiedTime > 0);
									rgscSaveFileV2->SetClientLastModifiedDate(fileLastModifiedTime);
									rgscSaveFileV2->SetClientFileSize(fileSize);
									saveFile = rgscSaveFileV2;

									break;
								}
							}
						}

						// We couldn't find the save file in the manifest, but we know it exists on disk. Add it.
						if (saveFile == NULL)
						{
							rlTaskDebug1("File from configuration '%s' not found in manifest, but found on disk. Adding to manifest", fileName);
							rlTaskDebug1("Untracked cloud save file '%s' found on disk.", fileName);

							ICloudSaveFile* rgscSaveFile = m_LocalManifest->AddFile();
							if (rgscSaveFile)
							{
								ICloudSaveFileV2* rgscSaveFileV2 = NULL;
								if (SUCCEEDED(rgscSaveFile->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2)
								{
									// Copy the file properties. Since this is a client file we've yet to ever seen in a manifest,
									//	we must assume it is a new file that is safe to upload. In the server resolve state, if this file
									//	comes down from the server this conflict state will be overwritten.
									rgscSaveFileV2->SetFileName(fileName);
									rlAssert(fileLastModifiedTime > 0);
									rgscSaveFileV2->SetClientLastModifiedDate(fileLastModifiedTime);
									rgscSaveFileV2->SetConflictState(ICloudSaveFileV2::ConflictState::CS_ClientHasNewer);
									rgscSaveFileV2->SetIsDirty(true);
									rgscSaveFileV2->SetClientFileSize(fileSize);
									rgscSaveFileV2->SetMetaData(EMPTY_CLOUD_METADATA);

#if RGSC_CHECK_CLOUDSAVE_MD5
									char md5Hash[rlCloudSaveFile::MD5_HASH_BUF_SIZE];
									rlCloudSave::CalculateMd5Hash(fullPath, md5Hash);
									rgscSaveFileV2->SetMd5Hash(md5Hash);
#endif // RGSC_CHECK_CLOUDSAVE_MD5
								}
							}
						}
					}
					else
					{
						// File was not on disk, clear any last modified metadata. 
						// First, find this file in our local manifest
						for (unsigned fileIndex = 0; fileIndex < m_LocalManifest->GetNumFiles(); fileIndex++)
						{
							ICloudSaveFile* indexFile = m_LocalManifest->GetFile(fileIndex);
							ICloudSaveFileV2* rgscSaveFileV2 = NULL;
							if (SUCCEEDED(indexFile->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2)
							{
								// We found the file from the configuration in the local manifest
								if (stricmp(rgscSaveFileV2->GetFileName(), fileName) == 0)
								{
									rlTaskDebug3("Cloud file '%s' from config was in manifest, but with no file size on disk. Setting 'last modified date' to 0 to request new version from server.", fileName);
									rlTaskDebug1("Tracked cloud save file '%s' could not be found on disk.", fileName);

									rgscSaveFileV2->SetClientLastModifiedDate(0);
									break;
								}
							}
						}
					}
				}

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		bool ResolveServerManifest()
		{
			// Iterate through all local files, check if they exist on the server. If they do not, but previously did, we must mark them as 
			//	'client has newer with conflict' to be resolved later. We reverse iterate since 'ResolveClientFile' can remove manifest entries.
			unsigned localManifestCount = m_LocalManifest->GetNumFiles();
			for (unsigned i = localManifestCount ; i --> 0; )
			{
				ICloudSaveFile* file = m_LocalManifest->GetFile(i);
				ResolveClientFile(i, file);
			}

			// Iterate through all server files, updating local files
			unsigned serverManifestCount = m_ServerManifest.GetCount();
			for (unsigned i = 0; i < serverManifestCount; i++)
			{
				rlCloudSaveFile* file = m_ServerManifest.GetSaveFile(i);
				ResolveServerFile(file);
			}

			m_LocalManifest->SetBytesUsed(m_ServerManifest.GetBytesUsed());
			return true;
		}

		void ResolveClientFile(int index, ICloudSaveFile* localFile)
		{
			rtry
			{
				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(localFile->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				// extract hardware id from configuration
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify(SUCCEEDED(hr) && configV1, catchall, );

				// construct the file path on disk
				char fullPath[RGSC_MAX_PATH];
				rverify(BuildFullPath(fullPath, configV1, rgscSaveFileV2), catchall, );
				rverify(fullPath[0] != '\0', catchall, );

				const fiDevice* device = fiDevice::GetDevice(fullPath);
				rverify(device, catchall, );

				// Get the file size of the file on disk (==0 if the file is missing from disk).
				u64 fileSize = device->GetFileSize(fullPath);

				// If a file is marked for deletion, let's make sure its been deleted from the disk.
				//	If it hasn't been deleted from disk, we clear its delete flag.
				if (rgscSaveFileV2->IsMarkedForDeletion() && fileSize > 0)
				{
					RetailCloudSaveTaskLog("Cloud save file '%s' was unregistered but still exists on disk.", rgscSaveFileV2->GetFileName());
					rlTaskDebug3("Clearing mark for deletion flag.");
					rgscSaveFileV2->SetMarkedForDeletion(false);
					return;
				}

				// If a file is marked for upload, but doesn't exist on disk, clear its dirty flag.
				if (rgscSaveFileV2->IsDirty() && fileSize == 0)
				{
					RetailCloudSaveTaskLog("Cloud save file '%s' was registered for upload but could not be found.", rgscSaveFileV2->GetFileName());
					rlTaskDebug3("Clearing dirty flag.");
					rgscSaveFileV2->SetIsDirty(false);
				}

				// Check to see if the file is missing metadata
				if (rgscSaveFileV2->GetMetaData()[0] == '\0')
				{
					RetailCloudSaveTaskLog("Local file tracked without metadata, replacing with empty metadata.");
					rgscSaveFileV2->SetMetaData(EMPTY_CLOUD_METADATA);
				}

				// Search for file in the server manifest. If it exists in the server manifest,
				//	we can early out here as 'ResolveServerFile' will resolve the file.
				unsigned serverManifestCount = m_ServerManifest.GetCount();
				for (unsigned i = 0; i < serverManifestCount; i++)
				{
					rlCloudSaveFile* serverFile = m_ServerManifest.GetSaveFile(i);
					if (stricmp(serverFile->GetFileName(), rgscSaveFileV2->GetFileName()) == 0)
					{
						return;
					}
				}

				// ---------
				// At this point, we have a file in our manifest that does not exist in the server manifest.
				// More logic will follow.
				// ---------

				// Clear out any cached server metadata - empty string means 'removed'
				rgscSaveFileV2->SetServerMetadata("");

				//	If the file size is zero, we now have no way of recovering this file (its been removed locally + on the server).
				//	Remove it from the manifest
				if (fileSize == 0)
				{
					RetailCloudSaveTaskLog("Cloud save file '%s' no longer exists locally or on the cloud save server.", rgscSaveFileV2->GetFileName());
					rlTaskDebug3("Removing manifest entry.");
					m_LocalManifest->RemoveFile(index);
					return;
				}

				// If the file is dirty, it must be uploaded.
				if (rgscSaveFileV2->IsDirty())
				{
					if (rgscSaveFileV2->GetFileVersion() > 0)
					{
						rgscSaveFileV2->SetConflictState(ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict);

						RetailCloudSaveTaskLog("A dirty file exists locally on disk with a tracked version, with no current server knowledge. It must have been deleted on the server.");
						rlTaskDebug3("Cloud File: '%s' - Conflict State: ClientHasNewerWithConflict", rgscSaveFileV2->GetFileName());
					}
					else
					{
						rgscSaveFileV2->SetConflictState(ICloudSaveFileV2::ConflictState::CS_ClientHasNewer);

						RetailCloudSaveTaskLog("A dirty file exists locally on disk with no tracked version and no current server knowledge. It must be a new file to be uploaded.");
						rlTaskDebug3("Cloud File: '%s' - Conflict State: ClientHasNewer", rgscSaveFileV2->GetFileName());
					}
				}
				else
				{
					// If we have a non-dirty file, that doesn't exist on the server, but has a version number.
					//	We know that it once existed, but has been deleted on the server.
					//	Because a user could manually attempt a backup restoration, we must tag this as a conflict.
					if (rgscSaveFileV2->GetFileVersion() > 0 || rgscSaveFileV2->GetServerLastModifiedDate() > 0)
					{
						rgscSaveFileV2->SetConflictState(ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict);

						RetailCloudSaveTaskLog("A non-dirty file exists locally on disk with a tracked version or date, and no current server knowledge.");
						rlTaskDebug3("Cloud File: '%s' - Conflict State: ClientHasNewerWithConflict", rgscSaveFileV2->GetFileName());
					}
					else
					{
						rgscSaveFileV2->SetConflictState(ICloudSaveFileV2::ConflictState::CS_ClientHasNewer);
						RetailCloudSaveTaskLog("A file exists locally on disk with no tracked version, and no current server knowledge.");
						rlTaskDebug3("Cloud File: '%s' - Conflict State: ClientHasNewer", rgscSaveFileV2->GetFileName());
					}
				}

				// Clear properties that may have come from a restored file.
				rgscSaveFileV2->SetFileId(0);
				rgscSaveFileV2->SetFileVersion(0);
				rgscSaveFileV2->SetLatestServerVersion(0);
				rgscSaveFileV2->SetNextExpectedVersion(0);
			}
			rcatchall
			{

			}
		}

		void ResolveServerFile(rlCloudSaveFile* serverFile)
		{
			unsigned localManifestCount = m_LocalManifest->GetNumFiles();
			for (unsigned i = 0; i < localManifestCount; i++)
			{
				ICloudSaveFile* localFile = m_LocalManifest->GetFile(i);
				ICloudSaveFileV2* localFileV2 = NULL;
				if (SUCCEEDED(localFile->QueryInterface(IID_ICloudSaveFileV2, (void**)&localFileV2) && localFileV2))
				{
					if (stricmp(serverFile->GetFileName(), localFileV2->GetFileName()) == 0)
					{
						ResolveFile(serverFile, localFileV2);
						return;
					}
				}
			}

			// We have a server file with no corresponding local file
			// Add a file to the manifest
			ICloudSaveFile* rgscSaveFile = m_LocalManifest->AddFile();
			if (rgscSaveFile)
			{
				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				if (SUCCEEDED(rgscSaveFile->QueryInterface(IID_ICloudSaveFileV1, (void**)&rgscSaveFileV2)) && rgscSaveFileV2)
				{
					RetailCloudSaveTaskLog("Resolving server cloud file: '%s'", serverFile->GetFileName());
					RetailCloudSaveTaskLog("Server file '%s' was missing locally, adding to the manifest.", serverFile->GetFileName());

					// Copy the v1 file properties
					rgscSaveFileV2->SetFileId(serverFile->GetFileId());
					rgscSaveFileV2->SetMd5Hash(serverFile->GetMd5Hash());
					rgscSaveFileV2->SetFileName(serverFile->GetFileName());
					rgscSaveFileV2->SetFileVersion(serverFile->GetFileVersion());
					rgscSaveFileV2->SetClientLastModifiedDate(serverFile->GetClientLastModifiedDate());
					rgscSaveFileV2->SetServerLastModifiedDate(serverFile->GetServerLastModifiedDate());
					rgscSaveFileV2->SetHardwareId(serverFile->GetHardwareId());

					char ipBuf[netIpAddress::MAX_STRING_BUF_SIZE];
					const netIpAddress& address = serverFile->GetLastIpAddress();
					address.Format(ipBuf);
					rgscSaveFileV2->SetLastIpAddress(ipBuf);

					// copy the v2 file properties over to the local file
					rgscSaveFileV2->SetServerFileSize(serverFile->GetServerFileSize());
					rgscSaveFileV2->SetServerMetadata(serverFile->GetServerMetadata());
					rgscSaveFileV2->SetNextExpectedVersion(serverFile->GetNextExpectedVersion());
					rgscSaveFileV2->SetLatestServerVersion(serverFile->GetFileVersion());

					// Set the missing state, we don't have track of it locally.
					rgscSaveFileV2->SetConflictState(ICloudSaveFileV2::ConflictState::CS_FileMissing);
					rlTaskDebug3("Cloud File: '%s' - Conflict State: FileMissing", serverFile->GetFileName());
				}
			}
		}

		void ResolveFile(rlCloudSaveFile* serverFile, ICloudSaveFileV2* localFile)
		{
			// Default to a conflict state. Our matrix below should handle all cases, but this can be used just in case.
			ICloudSaveFileV2::ConflictState conflictState = ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict;

			RetailCloudSaveTaskLog("Resolving cloud file: '%s'", serverFile->GetFileName());

			// ============================================
			//			Validate Untracked File
			// ============================================
			// If we're resolving an untracked file, we may be able to identify it as a synced file.
			//	The client's write time matches the server write time, the size is correct and the file hash matches. Update the local file's
			//	server last modified date and file version.
			if (localFile->GetClientLastModifiedDate() == serverFile->GetServerLastModifiedDate() && localFile->GetServerLastModifiedDate() == 0)
			{
				rlTaskDebug3("The local file had no server last modified date, but the client date matched the server. This indicates a missing/corrupted manifest that we could recover from.");

				if (localFile->GetClientFileSize() == serverFile->GetServerFileSize()
#if RGSC_CHECK_CLOUDSAVE_MD5 
					&& stricmp(localFile->GetMd5Hash(),serverFile->GetMd5Hash()) == 0
#endif
					)
				{
					rlTaskDebug3("The local file size and hash matched the servers, in addition to the local write time. Updating server write time and version since this file is in sync");
					localFile->SetServerLastModifiedDate(serverFile->GetServerLastModifiedDate());
					localFile->SetFileVersion(serverFile->GetFileVersion());
					localFile->SetIsDirty(false);
				}
			}

			// ============================================
			//			Set Up Fetch Properties
			// ============================================
			// copy the v2 server properties over to the local file to help with the fetch request
			localFile->SetServerFileSize(serverFile->GetServerFileSize());
			localFile->SetServerMetadata(serverFile->GetServerMetadata());
			localFile->SetNextExpectedVersion(serverFile->GetNextExpectedVersion());
			localFile->SetLatestServerVersion(serverFile->GetFileVersion());

			// ============================================
			//			Identify Conflict Operators
			// ============================================
			// Client Time
			bool bLocalClientDateMoreRecentThanServer = localFile->GetClientLastModifiedDate() > serverFile->GetServerLastModifiedDate();
			bool bLocalClientDateOlderThanServer = localFile->GetClientLastModifiedDate() < serverFile->GetServerLastModifiedDate();
			bool bLocalClientHasNoLastModifiedTime = localFile->GetClientLastModifiedDate() == 0;
			bool bLocalClientTimeOlderThanItsSyncTime = localFile->GetClientLastModifiedDate() < localFile->GetServerLastModifiedDate();

			// Server Time
			bool bLocalServerTimeMatchesServerTime = localFile->GetServerLastModifiedDate() == serverFile->GetServerLastModifiedDate();
			bool bLocalServerTimeMoreRecentThanServerTime = localFile->GetServerLastModifiedDate() > serverFile->GetServerLastModifiedDate();
			bool bLocalServerTimeOlderThanServerTime = localFile->GetServerLastModifiedDate() < serverFile->GetServerLastModifiedDate();

			// File Properties
			bool bLocalFileVersionMatchesServer = localFile->GetFileVersion() == serverFile->GetFileVersion();
			bool bLocalFileSizeMatchesServer = localFile->GetClientFileSize() == serverFile->GetServerFileSize();
			bool bLocalFileIsDirty = localFile->IsDirty();
			bool bLocalFileMarkedForDeletion = localFile->IsMarkedForDeletion();
			bool bValidFileId = localFile->GetFileId() == 0 || localFile->GetFileId() == serverFile->GetFileId();

			// ============================================
			//			Conflict Output Logging
			// ============================================
			RetailCloudSaveTaskLog("The local file's file was %s", bLocalFileIsDirty ? "registered for upload." : "not registered for upload.");
			
			if (bLocalFileMarkedForDeletion)
			{
				RetailCloudSaveTaskLog("The local file was marked for deletion");
			}

			if (!bValidFileId)
			{
				RetailCloudSaveTaskLog("The local file ID was invalid and must be presented for conflict resolution");
			}

			bool bLocalClientDateMatchesServer = localFile->GetClientLastModifiedDate() == serverFile->GetServerLastModifiedDate();
			if (bLocalClientHasNoLastModifiedTime)
			{
				RetailCloudSaveTaskLog("The local file has no client last modified time.");
			}
			else if (bLocalClientDateMatchesServer)
			{
				RetailCloudSaveTaskLog("The local file had a client last modified date (%" I64FMT "u) matches the results from the server's (%" I64FMT "u)", 
					localFile->GetClientLastModifiedDate(), serverFile->GetServerLastModifiedDate());
			}
			else if (bLocalClientDateMoreRecentThanServer)
			{
				RetailCloudSaveTaskLog("The local file had a client last modified date (%" I64FMT "u) was more recent than the server's (%" I64FMT "u)", 
								localFile->GetClientLastModifiedDate(), serverFile->GetServerLastModifiedDate());
			}
			else if (bLocalClientDateOlderThanServer)
			{
				RetailCloudSaveTaskLog("The local file had a client last modified date (%" I64FMT "u) was older than the server's (%" I64FMT "u)",
								localFile->GetClientLastModifiedDate(), serverFile->GetServerLastModifiedDate());
			}

			if (bLocalClientTimeOlderThanItsSyncTime)
			{
				RetailCloudSaveTaskLog("The local file had a client last modified time (%" I64FMT "u), older than its own server sync time (%" I64FMT "u)",
								localFile->GetClientLastModifiedDate(), serverFile->GetServerLastModifiedDate());
			}

			if (bLocalServerTimeMatchesServerTime)
			{
				RetailCloudSaveTaskLog("The local file's server last modified time (%" I64FMT "u) matches the results from the server (%" I64FMT "u)",
								localFile->GetServerLastModifiedDate(), serverFile->GetServerLastModifiedDate());
			}
			else if (bLocalServerTimeMoreRecentThanServerTime)
			{
				RetailCloudSaveTaskLog("The local file's server last modified time (%" I64FMT "u) is more recent than the server (%" I64FMT "u) (This should not happen!)",
								localFile->GetServerLastModifiedDate(), serverFile->GetServerLastModifiedDate());
			}
			else if (bLocalServerTimeOlderThanServerTime)
			{
				RetailCloudSaveTaskLog("The local file's server last modified time (%" I64FMT "u) was older than the server's. (%" I64FMT "u)",
								localFile->GetServerLastModifiedDate(), serverFile->GetServerLastModifiedDate());
			}
			
			RetailCloudSaveTaskLog("The local file's file version (%d) %s the file version of the server (%d)", 
								localFile->GetFileVersion(), bLocalFileVersionMatchesServer ? "matches" : "does not match", serverFile->GetFileVersion());

			RetailCloudSaveTaskLog("The local file's file size (%d) %s the file size of the server (%d)",
								localFile->GetClientFileSize(), bLocalFileSizeMatchesServer ? "matches" : "does not match", serverFile->GetServerFileSize());

			// An invalid file ID is always a conflict, no matter what. This would represent an extremely bad issue on the server,
			//	or more likely that the user is trying to copy single player saves from another user. In either case, we require
			//	the user to specify which version they want to keep.
			if (!bValidFileId)
			{
				if (bLocalClientDateMoreRecentThanServer)
				{
					conflictState = ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict;
				}
				else
				{
					conflictState = ICloudSaveFileV2::ConflictState::CS_ServerHasNewerWithConflict;
				}
			}
			//	When a file is deleted, we must check to see how the manifest was synced up. If it was synced before the delete,
			//	then the operation is a simple delete operation. If it wasn't (i.e. has a different version than the server),
			//	we should ask the user if the file should be removed or not.
			else if (bLocalFileMarkedForDeletion)
			{
				if (bLocalFileVersionMatchesServer)
				{
					conflictState = ICloudSaveFileV2::ConflictState::CS_FileDeleted;
				}
				else
				{
					conflictState = ICloudSaveFileV2::ConflictState::CS_FileDeletedWithConflict;
				}
			}
			// ============================================
			//		Dirty File Conflict Identification
			// ============================================
			// A file becomes dirty in one of two cases:
			//	1. An upload has been queued for the file, but has yet to be uploaded.
			//  2. The local manifest has a 'client last modified' time that does not match the actual
			//	   file time of the file.
			else if (bLocalFileIsDirty)
			{
				// The local file has been written more recently than the server time.
				if (bLocalClientDateMoreRecentThanServer)
				{
					// This local file's server last modified date matches what is on the server and file version match This means
					//	that the client was the last to write to the server, and has new data to push without issue.
					if (bLocalServerTimeMatchesServerTime && bLocalFileVersionMatchesServer)
					{
						conflictState = ICloudSaveFileV2::ConflictState::CS_ClientHasNewer;
					}
					else
					{
						conflictState = ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict;
					}
				}
				// The server has a newer version of the file. A conflict must always be assumed if we have local dirty data.
				else if (bLocalClientDateOlderThanServer)
				{
					conflictState = ICloudSaveFileV2::ConflictState::CS_ServerHasNewerWithConflict;
				}
				else
				{
					// local file is dirty, but up to date (and likely restored from backup)
					conflictState = ICloudSaveFileV2::ConflictState::CS_ClientHasNewer;
				}
			}
			// ============================================
			//		On-Disk File Conflict Identification
			// ============================================
			else
			{
				// At this point, we know the file isn't dirty, so we just need to check the server last modified times from the
				//	local manifest and compare them to the server. 

				// No local last modified date, file does not exist on disk.
				if (bLocalClientHasNoLastModifiedTime)
				{
					// Our manifest thinks its in sync. The file was deleted manually by the user.
					//	We should present a conflict UI to help a user delete a file without requiring an in-game delete.
					if (bLocalFileVersionMatchesServer && bLocalServerTimeMatchesServerTime)
					{
						conflictState = ICloudSaveFileV2::ConflictState::CS_FileDeletedWithConflict;
					}
					else
					{
						conflictState = ICloudSaveFileV2::ConflictState::CS_FileMissing;
					}
				}
				// In this second instance, we have a server file time more recent than what the server thinks it has.
				//	This is a very weird case and would indicate some sort of failed write on the backend. Flag the 
				//	file as "client has newer with conflict", so that we attempt to resolve the conflict and upload the file.
				else if (bLocalServerTimeMoreRecentThanServerTime)
				{
					conflictState = ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict;
				}
				// This state is more straight forward. The local file is newer than what is on the server, but isn't dirty.
				//	This would be the result of a manual edit of the file (i.e. restoring from .bak, downloading from the internet)
				//	We should display the conflict resolution screen here.
				else if (bLocalClientDateMoreRecentThanServer)
				{
					conflictState = ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict;
				}
				// The non-dirty local file was modified or synced prior to another write on the server, and should be overwritten.
				else if (bLocalServerTimeOlderThanServerTime || bLocalClientDateOlderThanServer)
				{
					// If a local file wasn't even in sync with its own manifest (i.e. a manual restoration), lets force the conflict screen.
					//	This will allow a user to drop in an older file and still be able to restore it without having it be overwritten.
					if (bLocalClientTimeOlderThanItsSyncTime)
					{
						conflictState = ICloudSaveFileV2::ConflictState::CS_ServerHasNewerWithConflict;
					}
					else
					{
						conflictState = ICloudSaveFileV2::ConflictState::CS_ServerHasNewer;
					}
				}
				// The server last modified dates are in sync with the version
				else if (bLocalServerTimeMatchesServerTime && bLocalFileVersionMatchesServer)
				{
					// If both files are the exact same size and the hashes match, there is no conflict.
					if (bLocalFileSizeMatchesServer)
					{
						conflictState = ICloudSaveFileV2::ConflictState::CS_None;
					}
					else
					{
						conflictState = ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict;
					}
				}
				// We couldn't detect an obvious conflict, but we're also not in sync. Possibly cases:
				//	-> The server last write time matches locally and on the server, but the file version mismatches. We should resolve this manually.
				else
				{
					rlAssert(false); // assert here to raise a flag that something abnormal happened
					RgscDisplay("Abnormal client state, forcing conflict resolution");
					conflictState = ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict;
				}
			}

			// ===================================================================================
			//		Update the local file with the server's last modified date and conflict state
			// ===================================================================================
			localFile->SetServerLastModifiedDate(serverFile->GetServerLastModifiedDate());
			localFile->SetFileId(serverFile->GetFileId());
			localFile->SetConflictState(conflictState);

			// Logging
			rlTaskDebug3("Cloud File: '%s' - Conflict State: %s", serverFile->GetFileName(), RgscGetConflictStateString(conflictState));
			RgscDisplay("Cloud File: '%s' - Conflict State: %d", serverFile->GetFileName(), (int)(conflictState));
		}

		State m_State;
		rlCloudSaveManifest m_ServerManifest;
		ICloudSaveManifestV2* m_LocalManifest;
		netStatus m_MyStatus;
	};

	class RegisterFileWorkItem : public sysThreadPool::WorkItem
	{
	public:

		bool Configure(ICloudSaveManifestV2* manifest, const char* fileName, const char* metaData, bool unregister, netStatus* status)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(manifest, catchall, );
				rverify(fileName && fileName[0] != '\0', catchall, );
				rverify(metaData && (metaData[0] != '\0' || unregister), catchall, );
				rverify(status, catchall, );

				m_FileName = fileName;
				m_MetaData = metaData;
				m_Manifest = manifest;
				m_bUnregister = unregister;

				m_Status = status;
				m_Status->SetPending();
				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		virtual void DoWork()
		{
			SYS_CS_SYNC(sm_RegisterFileCs);

			rtry
			{
				// Clear the manifest and reload from disk to ensure we have the most recent state
				m_Manifest->Clear();
				
				bool bLocalManifestLoaded = GetRgscConcreteInstance()->_GetCloudSaveManager()->LoadManifest(m_Manifest);

				// url:bugstar:2694344 - GTAV PC Cloud Saves - Crash while writing manifest can create a broken state
				//	We could not load the local manifest. We still want to attempt to write to disk, so that the Launcher
				//	is aware of the updated metadata. This will result in additional conflict resolution required on the next
				//	launcher sequence.
				if (!rlVerify(bLocalManifestLoaded))
				{
					m_Manifest->Clear();
				}

				// NOTE:
				//	We specifically do not check for Cloud Saves enabled at this point. We 
				//	want to write the metadata to disk, so if cloud saves are enabled the metadata
				//	is as up to date as possible.

				// Register the file within the manifest for upload, and save to disk.
				if (m_bUnregister)
				{
					rverify(m_Manifest->UnregisterFile(m_FileName), catchall, );
				}
				else
				{
					rverify(m_Manifest->RegisterForUpload(m_FileName, m_MetaData), catchall, );
				}

				rverify(GetRgscConcreteInstance()->_GetCloudSaveManager()->SaveManifest(m_Manifest), catchall, );

				rlDebug1("File '%s' successfully registered for upload with metadata: %s", m_FileName, m_MetaData);

				m_Status->SetSucceeded();
			}
			rcatchall
			{
				m_Status->SetFailed();
			}
		}

	private:

		static sysCriticalSectionToken sm_RegisterFileCs;

		netStatus* m_Status;
		ICloudSaveManifestV2* m_Manifest;
		const char* m_FileName;
		const char* m_MetaData;
		bool m_bUnregister;
	};

	sysCriticalSectionToken RegisterFileWorkItem::sm_RegisterFileCs;

	class RegisterFileTask : public rgscTask<RgscCloudSaveManager>
	{
	public:
		RL_TASK_DECL(RegisterFileTask);

		enum State
		{
			STATE_INVALID   = -1,
			STATE_CONFIGURE,
			STATE_RUNNING
		};

		RegisterFileTask()
			: m_State(STATE_INVALID)
			, m_Manifest(NULL)
			, m_bUnregister(false)
		{
			m_FileName[0] = '\0';
			m_MetaData[0] = '\0';
		}

		bool Configure(RgscCloudSaveManager* ctx, ICloudSaveManifest* manifest, const char* fileName, const char* metaData, bool unregister)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(manifest, catchall, );
				rverify(fileName && fileName[0] != '\0', catchall, );
				
				// no metadata required during unregister
				rverify(metaData && (metaData[0] != '\0' || unregister), catchall, );

				m_Manifest = NULL;
				RGSC_HRESULT hr = manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&m_Manifest);
				rverify(SUCCEEDED(hr) && m_Manifest, catchall, );

				safecpy(m_FileName, fileName);
				safecpy(m_MetaData, metaData);
				m_bUnregister = unregister;

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		virtual void Start()
		{
			rlTaskDebug2("%s file...", m_bUnregister ? "Unregistering" : "Registering");

			this->rgscTask<RgscCloudSaveManager>::Start();

			m_State = STATE_CONFIGURE;
		}

		virtual void Finish(const FinishType finishType, const int resultCode = 0)
		{
			rlTaskDebug2("Finished %s file.", m_bUnregister ? "unregistering" : "registering");

			this->rgscTask<RgscCloudSaveManager>::Finish(finishType, resultCode);
		}

		virtual void Update(const unsigned timeStep)
		{
			this->rgscTask<RgscCloudSaveManager>::Update(timeStep);

			if(this->WasCanceled())
			{
				//Wait for dependencies to finish
				if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_CANCELED);
				}

				return;
			}

			do 
			{
				switch(m_State)
				{
				case STATE_CONFIGURE:
					if (m_WorkItem.Configure(m_Manifest, m_FileName, m_MetaData, m_bUnregister, &m_MyStatus) && netThreadPool.QueueWork(&m_WorkItem))
					{
						m_State = STATE_RUNNING;
					}
					else
					{
						rlTaskError("Failed in STATE_CONFIGURE");
						this->Finish(FINISH_FAILED);
					}
					break;
				case STATE_RUNNING:
					if (!m_MyStatus.Pending())
					{
						if (m_MyStatus.Succeeded())
						{
							this->Finish(FINISH_SUCCEEDED);
						}
						else
						{
							rlTaskError("Failed in STATE_RUNNING");
							this->Finish(FINISH_FAILED);
						}
					}
					break;
				}
			}
			while(!m_MyStatus.Pending() && this->IsActive());

		}

		virtual bool IsCancelable() const {return true;}

		virtual void DoCancel()
		{
			if (m_WorkItem.Pending())
			{
				netThreadPool.CancelWork(m_WorkItem.GetId());
			}
		}

	private:

		State m_State;

		RegisterFileWorkItem m_WorkItem;
		netStatus m_MyStatus;

		ICloudSaveManifestV2* m_Manifest;
		char m_FileName[rlCloudSaveFile::FILENAME_MAX_BUF_SIZE];
		char m_MetaData[rlCloudSaveFile::METADATA_MAX_BUF_SIZE];
		bool m_bUnregister;
	};

	class DeleteUpdatedFilesTask : public rgscTask<RgscCloudSaveManager>
	{
	public:
		RL_TASK_DECL(DeleteUpdatedFilesTask);

		enum State
		{
			STATE_INVALID   = -1,
			STATE_DELETE_FILES,
			STATE_DELETING_FILES
		};

		DeleteUpdatedFilesTask()
			: m_State(STATE_INVALID)
			, m_LocalManifest(NULL)
			, m_LocalManifestV3(NULL)
			, m_ProgressTrackerV1(NULL)
			, m_CurrentIndex(0)
			, m_uLastDeleteTime(0)
			, m_FilesDeleted(0)
			, m_DeleteMetricIndex(0)
			, m_bMetricsCollected(false)
		{

		}

		bool Configure(RgscCloudSaveManager* /*ctx*/, ICloudSaveManifest* manifest)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(manifest, catchall, );

				// V2 is the earliest accepted manifest for this task
				RGSC_HRESULT hr = manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&m_LocalManifest);
				rverify(SUCCEEDED(hr) && m_LocalManifest, catchall, );

				// V3 enables optional functionality
				hr = manifest->QueryInterface(IID_ICloudSaveManifestV3, (void**)&m_LocalManifestV3);
				if (SUCCEEDED(hr) && m_LocalManifestV3)
				{
					ICloudSaveOperationProgress* progress = m_LocalManifestV3->GetProgressTracker();
					rverify(progress, catchall, );

					hr = progress->QueryInterface(IID_ICloudSaveOperationProgressV1, (void**)&m_ProgressTrackerV1);
					rverify(SUCCEEDED(hr) && m_ProgressTrackerV1, catchall, );
				}

				// Check if this manifest has cloud saves enabled.
				rverify(m_LocalManifest->GetCloudSaveEnabled() == ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES, catchall, );

				// verify conflicts are resolved
				rverify(m_LocalManifest->GetNumUnresolvedConflicts() == 0, catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		virtual void Start()
		{
			RetailCloudSaveTaskLog("Deleting unregistered cloud files");

			this->rgscTask<RgscCloudSaveManager>::Start();

			if (m_ProgressTrackerV1)
			{
				m_ProgressTrackerV1->SetStepCount(0);
				m_ProgressTrackerV1->SetCompletedCount(0);
				m_ProgressTrackerV1->SetIsActive(true);

				SetupProgressTracker();
			}

			m_State = STATE_DELETE_FILES;
		}

		virtual void Finish(const FinishType finishType, const int resultCode = 0)
		{
			RetailCloudSaveTaskLog("Deleting unregistered cloud files completed with %s (%d files deleted) (code: %d)", PrintFinishType(finishType), m_FilesDeleted, resultCode);

			if (finishType != FINISH_CANCELED)
			{
				WriteDeleteTelemetry();

				if (m_ProgressTrackerV1)
				{
					m_ProgressTrackerV1->SetIsActive(false);
				}
			}

			this->rgscTask<RgscCloudSaveManager>::Finish(finishType, resultCode);
		}

		virtual bool IsCancelable() const
		{
			return true;
		}

		virtual void DoCancel()
		{
			RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
			rgscTask::DoCancel();
		}

		virtual void Update(const unsigned timeStep)
		{
			this->rgscTask<RgscCloudSaveManager>::Update(timeStep);

			if(this->WasCanceled())
			{
				//Wait for dependencies to finish
				if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_CANCELED);
				}

				return;
			}

			do 
			{
				switch(m_State)
				{
				case STATE_INVALID:
					break;
				case STATE_DELETE_FILES:
					if (HasEnumeratedAllFiles())
					{
						this->Finish(FINISH_SUCCEEDED);
					}
					else if (DoesFileRequireDelete(m_CurrentIndex, true))
					{
						if (TryDeleteCurrentFile())
						{
							m_uLastDeleteTime = sysTimer::GetSystemMsTime();
							m_State = STATE_DELETING_FILES;
						}
						else
						{
							this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV2::DeleteUpdatedFilesErrors::ERROR_SETUP_FILE);
						}
					}
					else
					{
						m_CurrentIndex++;
					}
					break;
				case STATE_DELETING_FILES:
					if (!m_MyStatus.Pending())
					{
						if (m_MyStatus.Succeeded())
						{
							m_FilesDeleted++;
							if (m_ProgressTrackerV1)
							{
								m_ProgressTrackerV1->SetCompletedCount(m_FilesDeleted);
							}
							AddCurrentMetric();
							ProcessSuccess();
							m_State = STATE_DELETE_FILES;
						}
						else
						{
							// copy http debug info from the delete file request to the out manifest.
							const netHttpRequest::DebugInfo& debugInfo = m_DeleteFile.GetHttpDebugInfo();
							FillOutHttpDebugInfo(m_LocalManifest, debugInfo);

							RetailCloudSaveTaskLog("Cloud Save file delete failed. HTTP Code: %d, Reason: %d", debugInfo.m_HttpStatusCode, debugInfo.m_AbortReason);

							AddCurrentMetric();
							this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV2::DeleteUpdatedFilesErrors::ERROR_DELETING_FILE);
						}
					}
					break;
				}
			}
			while(!m_MyStatus.Pending() && this->IsActive());
		}

	private:

		void AddCurrentMetric()
		{
			rtry
			{
				rverify(m_DeleteMetricIndex < COUNTOF(m_DeleteMetric.m_MetricInfo), catchall, );

				// Extract the V1 configuration from the manifest.
				//	Manifest without a V1+ configuration cannot identify conflicts and will fail this task.
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify(SUCCEEDED(hr) && configV1, catchall, );

				// Iterate through the entire configuration
				bool bFoundMetricIndex = false;
				for (unsigned configIndex = 0; configIndex < configV1->GetNumCloudFiles(); configIndex++)
				{
					if (stricmp(m_DeleteFile.GetFileName(), configV1->GetFileName(configIndex)) == 0)
					{
						m_DeleteMetric.m_MetricInfo[m_DeleteMetricIndex].m_FileIndex = (s8)configIndex;
						bFoundMetricIndex = true;
						break;
					}
				}

				rverify(bFoundMetricIndex, catchall, );
				m_DeleteMetric.m_MetricInfo[m_DeleteMetricIndex].m_bDidWork = true;
				m_DeleteMetric.m_MetricInfo[m_DeleteMetricIndex].m_Duration = (s32)(sysTimer::GetSystemMsTime() - m_uLastDeleteTime);
				m_DeleteMetric.m_MetricInfo[m_DeleteMetricIndex].m_FileSize = (s32)m_DeleteFile.GetServerFileSize();
				m_DeleteMetric.m_MetricInfo[m_DeleteMetricIndex].m_HttpStatusCode = (s16)m_MyStatus.GetResultCode();
				
				// track the metric index seperately than the current index
				m_DeleteMetricIndex++;
				m_bMetricsCollected = true;
			}
			rcatchall
			{

			}
		}

		bool WriteDeleteTelemetry()
		{
			rtry
			{
				rcheck(m_bMetricsCollected, catchall, );
				rverify(m_LocalManifest, catchall, );

				// Sanity check that posting this metric won't overflow
				rverify(m_LocalManifest->GetNumFiles() <= COUNTOF(m_DeleteMetric.m_MetricInfo), catchall, );
				m_DeleteMetric.m_Manifest = m_LocalManifest;

				const unsigned DOUBLE_MEMORY_POLICY = 1024; // double the size of the default policy

				rlMetricList list;
				list.Append(&m_DeleteMetric);

				rlTelemetrySubmissionMemoryPolicy memPolicy(DOUBLE_MEMORY_POLICY);
				rlTelemetry::SubmitMetricList(RL_RGSC_GAMER_INDEX, &list, &memPolicy);
				memPolicy.ReleaseAndClear();

#if !__NO_OUTPUT
				char deleteTelemetry[RL_TELEMETRY_DEFAULT_WORK_BUFFER_SIZE] = {0};
				RsonWriter rw(deleteTelemetry, RSON_FORMAT_JSON);
				m_DeleteMetric.Write(&rw);
				rlDebug3("WriteDeleteTelemetry: %s", rw.ToString());
#endif

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		bool SetupFile(ICloudSaveFile* file)
		{
			rtry
			{
				// Clear the file before copying the rgsc file's properties
				m_DeleteFile.Clear();

				// Verify a V2 file for this operation
				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				// Copy over the data relevant for deleting
				m_DeleteFile.SetFileId(rgscSaveFileV2->GetFileId());
				m_DeleteFile.SetMd5Hash(rgscSaveFileV2->GetMd5Hash());
				m_DeleteFile.SetFileName(rgscSaveFileV2->GetFileName());
				m_DeleteFile.SetFileVersion(rgscSaveFileV2->GetLatestServerVersion());
				m_DeleteFile.SetClientLastModifiedDate(rgscSaveFileV2->GetClientLastModifiedDate());
				m_DeleteFile.SetServerLastModifiedDate(rgscSaveFileV2->GetServerLastModifiedDate());
				m_DeleteFile.SetNextExpectedVersion(rgscSaveFileV2->GetNextExpectedVersion());
				m_DeleteFile.SetServerFileSize(rgscSaveFileV2->GetServerFileSize());

				// extract hardware id from configuration
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify(SUCCEEDED(hr) && configV1, catchall, );
				m_DeleteFile.SetHardwareId(configV1->GetHardwareId());

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		bool HasEnumeratedAllFiles()
		{
			return m_CurrentIndex >= m_LocalManifest->GetNumFiles();
		}

		void SetupProgressTracker()
		{
			rtry
			{
				rcheck(m_ProgressTrackerV1, catchall, );

				int numFilesToDownload = 0;
				for (unsigned i = 0; i < m_LocalManifest->GetNumFiles(); i++)
				{
					if (DoesFileRequireDelete(i, false))
					{
						numFilesToDownload++;
					}
				}

				m_ProgressTrackerV1->SetStepCount(numFilesToDownload);
			}
			rcatchall
			{

			}
		}

		bool DoesFileRequireDelete(u32 index, bool bEvaluateLocalFileForDeletion)
		{
			rtry
			{
				rverify(index < m_LocalManifest->GetNumFiles(), catchall, );

				ICloudSaveFile* file = m_LocalManifest->GetFile(index);
				rverify(file, catchall, );

				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				if (bEvaluateLocalFileForDeletion)
				{
					// Potentially clear the delete flag. If 'AcceptRemote' was selected, we should clear our deletion flag.
					if (rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_FileDeletedWithConflict &&
						rgscSaveFileV2->GetResolveType() == ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptRemote)
					{
						rgscSaveFileV2->SetMarkedForDeletion(false);
						return false;
					}

					// Remove locally
					if (rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict &&
						rgscSaveFileV2->GetResolveType() == ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptRemote &&
						rgscSaveFileV2->GetLatestServerVersion() == 0)
					{
						ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
						ICloudSaveTitleConfigurationV1* configV1 = NULL;
						RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
						rverify(SUCCEEDED(hr) && configV1, catchall, );

						// construct the file path on disk
						char fullPath[RGSC_MAX_PATH];
						rverify(BuildFullPath(fullPath, configV1, rgscSaveFileV2), catchall, );
						rverify(fullPath[0] != '\0', catchall, );

						const fiDevice* device = fiDevice::GetDevice(fullPath);
						rverify(device, catchall, );

						if (device->GetFileSize(fullPath) > 0)
						{
							rlTaskDebug3("Deleting local file: '%s'", fullPath);
							rverify(device->Delete(fullPath), catchall, );
						}

						m_LocalManifest->RemoveFile(m_CurrentIndex);

						// Deleting while iterating. m_CurrentIndex++ is called with each iteration, so
						//	we go back to the previous index to avoid skipping the next index.
						m_CurrentIndex--;
						return false;
					}
				}

				// Validate as a file that needs deleted.
				if (rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_FileDeleted ||
					(rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_FileDeletedWithConflict &&
					rgscSaveFileV2->GetResolveType() == ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptLocal))
				{
					return true;
				}
			}
			rcatchall
			{
			}

			return false;
		}

		bool TryDeleteCurrentFile()
		{
			rtry
			{
				rverify(m_CurrentIndex < m_LocalManifest->GetNumFiles(), catchall, );
				m_DeleteFile.Clear();

				ICloudSaveFile* file = m_LocalManifest->GetFile(m_CurrentIndex);
				rverify(file, catchall, );
				rverify(SetupFile(file), catchall, );

				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				RetailCloudSaveTaskLog("Unregistering cloud file '%s' (Version %d)", m_DeleteFile.GetFileName(), m_DeleteFile.GetFileVersion());

				rage::eCloudSaveResolveType resolveType = RgscToRageResolveType(rgscSaveFileV2->GetResolveType());
				const char* titleAccessToken = GetAccessTokenFromManifest(m_LocalManifest);
				return rlCloudSave::UnregisterFile(RL_RGSC_GAMER_INDEX, &m_DeleteFile, resolveType, titleAccessToken, &m_MyStatus);
			}
			rcatchall
			{

			}

			return false;
		}

		void ProcessSuccess()
		{
			m_LocalManifest->RemoveFile(m_CurrentIndex);

			RetailCloudSaveTaskLog("Cloud file unregistered from server: '%s'.", m_DeleteFile.GetFileName());
		}

		State m_State;
		netStatus m_MyStatus;
		ICloudSaveManifestV2* m_LocalManifest;
		ICloudSaveManifestV3* m_LocalManifestV3;
		ICloudSaveOperationProgressV1* m_ProgressTrackerV1;
		rlCloudSaveFile m_DeleteFile;
		
		unsigned m_CurrentIndex;
		unsigned m_DeleteMetricIndex;

		u32 m_uLastDeleteTime;

		RgscCloudSaveDeleteFilesMetric m_DeleteMetric;
		bool m_bMetricsCollected;

		int m_FilesDeleted;
	};

	class QueryBetaAccessTask : public rgscTask<RgscCloudSaveManager>
	{
	public:
		RL_TASK_DECL(QueryBetaAccessTask);

		enum State
		{
			STATE_INVALID   = -1,
			STATE_QUERY_BETA_ACCESS,
			STATE_QUERYING_BETA_ACCESS
		};

		QueryBetaAccessTask()
			: m_State(STATE_INVALID)
			, m_ResultPtr(NULL)
			, m_BetaAccess(false)
		{
			m_CohortsCloudfile[0] = '\0';
		}

		bool Configure(RgscCloudSaveManager* /*ctx*/, const char* cohortsFilePath, bool* resultPtr)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(resultPtr, catchall, );
				rverify(cohortsFilePath && cohortsFilePath[0] != '\0', catchall, );

				safecpy(m_CohortsCloudfile, cohortsFilePath);
				m_ResultPtr = resultPtr;

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		virtual void Start()
		{
			rlTaskDebug2("Querying beta access...");

			this->rgscTask<RgscCloudSaveManager>::Start();

			m_State = STATE_QUERY_BETA_ACCESS;
		}

		virtual void Finish(const FinishType finishType, const int resultCode = 0)
		{
			rlTaskDebug2("Finished querying beta access");

			this->rgscTask<RgscCloudSaveManager>::Finish(finishType, resultCode);
		}

		virtual void Update(const unsigned timeStep)
		{
			this->rgscTask<RgscCloudSaveManager>::Update(timeStep);

			if(this->WasCanceled())
			{
				//Wait for dependencies to finish
				if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_CANCELED);
				}

				return;
			}

			do 
			{
				switch(m_State)
				{
				case STATE_INVALID:
					break;
				case STATE_QUERY_BETA_ACCESS:
					if (QueryBetaAccess())
					{
						m_State = STATE_QUERYING_BETA_ACCESS;
					}
					else
					{
						this->Finish(rlTaskBase::FINISH_FAILED);
					}
					break;
				case STATE_QUERYING_BETA_ACCESS:
					if (!m_MyStatus.Pending())
					{
						if (m_MyStatus.Succeeded())
						{
							RetailCloudSaveTaskLog("Cloud Save Beta Access Queried: %s", m_BetaAccess ? "Has Access" : "No Access");

							*m_ResultPtr = m_BetaAccess;

							this->Finish(FINISH_SUCCEEDED);
						}
						else
						{

							this->Finish(FINISH_FAILED);
						}
					}
					break;
				}
			}
			while(!m_MyStatus.Pending() && this->IsActive());
		}

		bool QueryBetaAccess()
		{
			return rlCloudSave::QueryHasBetaAccess(RL_RGSC_GAMER_INDEX, m_CohortsCloudfile, &m_BetaAccess, &m_MyStatus);
		}

		virtual bool IsCancelable() const
		{
			return true;
		}

		virtual void DoCancel()
		{
			RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
			rgscTask::DoCancel();
		}

	private:

		State m_State;
		netStatus m_MyStatus;
		bool* m_ResultPtr;
		bool m_BetaAccess;
		char m_CohortsCloudfile[ICloudSaveManagerV3::RGSC_MAX_COHORTS_CLOUD_FILE_BUF_SIZE];
	};

	class BackupConflictedFilesTask : public rgscTask<RgscCloudSaveManager>
	{
	public:
		RL_TASK_DECL(BackupConflictedFilesTask);

		enum State
		{
			STATE_INVALID   = -1,
			STATE_GET_FILES,
			STATE_GETTING_FILES
		};

		BackupConflictedFilesTask()
			: m_State(STATE_INVALID)
			, m_LocalManifest(NULL)
			, m_LocalManifestV3(NULL)
			, m_ProgressTrackerV1(NULL)
			, m_CurrentIndex(0)
			, m_FilesBackedUp(0)
		{

		}

		~BackupConflictedFilesTask()
		{
		}

		bool Configure(RgscCloudSaveManager* /*ctx*/, ICloudSaveManifest* manifest)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(manifest, catchall, );

				// V2 is the earliest accepted manifest for this task
				RGSC_HRESULT hr = manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&m_LocalManifest);
				rverify(SUCCEEDED(hr) && m_LocalManifest, catchall, );


				// V3 enables optional functionality
				hr = manifest->QueryInterface(IID_ICloudSaveManifestV3, (void**)&m_LocalManifestV3);
				if (SUCCEEDED(hr) && m_LocalManifestV3)
				{
					ICloudSaveOperationProgress* progress = m_LocalManifestV3->GetProgressTracker();
					rverify(progress, catchall, );

					hr = progress->QueryInterface(IID_ICloudSaveOperationProgressV1, (void**)&m_ProgressTrackerV1);
					rverify(SUCCEEDED(hr) && m_ProgressTrackerV1, catchall, );
				}

				// Check if this manifest has cloud saves enabled.
				rverify(m_LocalManifest->GetCloudSaveEnabled() == ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES, catchall, );

				// verify conflicts are resolved
				rverify(m_LocalManifest->GetNumUnresolvedConflicts() == 0, catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		virtual void Start()
		{
			RetailCloudSaveTaskLog("Backing up conflicting files...");

			this->rgscTask<RgscCloudSaveManager>::Start();

			if (m_ProgressTrackerV1)
			{
				m_ProgressTrackerV1->SetStepCount(0);
				m_ProgressTrackerV1->SetCompletedCount(0);
				m_ProgressTrackerV1->SetIsActive(true);

				SetupProgressTracker();
			}

			m_State = STATE_GET_FILES;
		}

		virtual void Finish(const FinishType finishType, const int resultCode = 0)
		{
			RetailCloudSaveTaskLog("Finished backing up conflicting files - %d files backed up (code: %d)", m_FilesBackedUp, resultCode);

			if (finishType != FINISH_CANCELED)
			{
				if (m_ProgressTrackerV1)
				{
					m_ProgressTrackerV1->SetIsActive(false);
				}
			}

			this->rgscTask<RgscCloudSaveManager>::Finish(finishType, resultCode);
		}

		virtual bool IsCancelable() const
		{
			return true;
		}

		virtual void DoCancel()
		{
			RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
			rgscTask::DoCancel();
		}

		virtual void Update(const unsigned timeStep)
		{
			this->rgscTask<RgscCloudSaveManager>::Update(timeStep);

			if(this->WasCanceled())
			{
				//Wait for dependencies to finish
				if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_CANCELED);
				}

				return;
			}

			do 
			{
				switch(m_State)
				{
				case STATE_INVALID:
					break;
				case STATE_GET_FILES:
					if (HasEnumeratedAllFiles())
					{
						this->Finish(FINISH_SUCCEEDED);
					}
					else if (DoesFileRequireDownload(m_CurrentIndex))
					{
						if (TryGetCurrentFile())
						{
							m_State = STATE_GETTING_FILES;
						}
						else
						{
							this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV3::BackupConflictedFilesErrors::BCFE_ERROR_SETUP_FILE);
						}
					}
					else if (DoesFileRequireBackup(m_CurrentIndex))
					{
						if (TryBackupCurrentFile())
						{
							// increment the current index for the next iteration
							m_CurrentIndex++;
							m_FilesBackedUp++;
							if (m_ProgressTrackerV1)
							{
								m_ProgressTrackerV1->SetCompletedCount(m_FilesBackedUp);
							}
						}
						else
						{
							this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV3::BackupConflictedFilesErrors::BCFE_ERROR_SETUP_FILE);
						}
					}
					else
					{
						m_CurrentIndex++;
					}
					break;
				case STATE_GETTING_FILES:
					if (!m_MyStatus.Pending())
					{
						if (m_MyStatus.Succeeded())
						{
							m_FilesBackedUp++;
							if (m_ProgressTrackerV1)
							{
								m_ProgressTrackerV1->SetCompletedCount(m_FilesBackedUp);
							}
							DownloadComplete();
							m_State = STATE_GET_FILES;
						}
						else
						{
							// copy http debug info from the get file request to the out manifest.
							const netHttpRequest::DebugInfo& debugInfo = m_GetFile.GetHttpDebugInfo();
							FillOutHttpDebugInfo(m_LocalManifest, debugInfo);

							RetailCloudSaveTaskLog("Cloud Save backup file download failed. HTTP Code: %d, Reason: %d", debugInfo.m_HttpStatusCode, debugInfo.m_AbortReason);

							this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV3::BackupConflictedFilesErrors::BCFE_ERROR_GETTING_FILE);
						}
					}
					break;
				}
			}
			while(!m_MyStatus.Pending() && this->IsActive());
		}

	private:

		bool HasEnumeratedAllFiles()
		{
			return m_CurrentIndex >= m_LocalManifest->GetNumFiles();
		}

		void SetupProgressTracker()
		{
			rtry
			{
				rcheck(m_ProgressTrackerV1, catchall, );

				int numFiles = 0;
				for (unsigned i = 0; i < m_LocalManifest->GetNumFiles(); i++)
				{
					if (DoesFileRequireDownload(i) || DoesFileRequireBackup(i))
					{
						numFiles++;
					}
				}

				m_ProgressTrackerV1->SetStepCount(numFiles);
			}
			rcatchall
			{

			}
		}

		bool DoesFileRequireDownload(u32 index)
		{
			rtry
			{
				rverify(index < m_LocalManifest->GetNumFiles(), catchall, );
				m_GetFile.Clear();

				ICloudSaveFile* file = m_LocalManifest->GetFile(index);
				rverify(file, catchall, );

				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				// Files not on the server do not require metadata.
				//	A 'NextExpectedVersion' of 0 indicates a deleted file.
				if (rgscSaveFileV2->GetNextExpectedVersion() == 0)
					return false;

				// Validate as an conflict file that requires a download.
				if (rgscSaveFileV2->GetResolveType() == ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptLocal &&
						(rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict ||
						rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_ServerHasNewerWithConflict ||
						rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_FileDeletedWithConflict)
					)
				{
					return true;
				}
			}
			rcatchall
			{
			}

			return false;
		}

		bool DoesFileRequireBackup(u32 index)
		{
			rtry
			{
				rverify(index < m_LocalManifest->GetNumFiles(), catchall, );

				ICloudSaveFile* file = m_LocalManifest->GetFile(index);
				rverify(file, catchall, );

				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify (SUCCEEDED(hr) && configV1, catchall, );

				// Get the current file path
				char fullPath[RGSC_MAX_PATH];
				rverify(BuildFullPath(fullPath, configV1, rgscSaveFileV2), catchall, );

				const fiDevice* device = fiDevice::GetDevice(fullPath);
				rverify(device, catchall, );

				// If the file doesn't exist (i.e. was deleted), we clearly can't back it up.
				if (device->GetFileSize(fullPath) == 0)
				{
					return false;
				}

				// Validate as an conflict file that requires a backup of the local file
				if (rgscSaveFileV2->GetResolveType() == ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptRemote)
				{
					return true;
				}
			}
			rcatchall
			{
			}

			return false;
		}

		bool CascadeBackupFiles()
		{
			rtry
			{
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify (SUCCEEDED(hr) && configV1, catchall, );

				// extract the file at this index
				ICloudSaveFile* file = m_LocalManifest->GetFile(m_CurrentIndex);
				rverify(file, catchall, );

				// verify its status as a genuine bona fide v2 file
				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				// Create file path for backups
				char csPath[RGSC_MAX_PATH];
				rverify(BuildBackupPath(csPath, configV1, rgscSaveFileV2), catchall, );

				const fiDevice* device = fiDevice::GetDevice(csPath);
				rverify(device, catchall, );

				// Move .csb1->csb2, csb0->csb1
				// For 3 files, updates: csb1->csb2, csb0->csb1.
				//	If the num slots is adjusted to something larger than 3, i.e. 5
				//		csb3->csb4, csb2->csb3, csb1->csb2, csb0->csb1
				//	If the num slots is set to 0 or 1, no cascade is done (and the final copy takes precedence).
				int numSlots = GetRgscConcreteInstance()->_GetCloudSaveManager()->GetNumBackupSlots();
				for (int i = numSlots - 2; i >= 0; i--)
				{
					// CASCADE the file
					{
						// Create the original path, which is .csb(i)
						char backupPath[RGSC_MAX_PATH];
						safecpy(backupPath, csPath);
						safecatf(backupPath, RGSC_CLOUDSAVE_BACKUP_EXTENSION, i);

						// The destination path is .csb(i+1).
						char newPath[RGSC_MAX_PATH];
						safecpy(newPath, csPath);
						safecatf(newPath, RGSC_CLOUDSAVE_BACKUP_EXTENSION, i + 1);

						// Copy if the file exists
						if (device->GetFileSize(backupPath) > 0)
						{
							// Clear read only attribute
							ClearReadOnlyAttribute(device, newPath);

							rlTaskDebug2("Copying from '%s' to '%s'", backupPath, newPath);
							rverify(device->Rename(backupPath, newPath), catchall, rlTaskError("Rename failed: %d", GetLastError()));
						}
					}

					// Cascade the info
					{
						// Create the original path, which is .csbe(i)
						char backupPath[RGSC_MAX_PATH];
						safecpy(backupPath, csPath);
						safecatf(backupPath, RGSC_CLOUDSAVE_BACKUP_INFO_EXTENSION, i);

						// The destination path is .csbe(i+1).
						char newPath[RGSC_MAX_PATH];
						safecpy(newPath, csPath);
						safecatf(newPath, RGSC_CLOUDSAVE_BACKUP_INFO_EXTENSION, i + 1);

						// Copy if the file exists
						if (device->GetFileSize(backupPath) > 0)
						{
							// Clear read only attribute
							ClearReadOnlyAttribute(device, newPath);

							rlTaskDebug2("Copying from '%s' to '%s'", backupPath, newPath);
							rverify(device->Rename(backupPath, newPath), catchall, rlTaskError("Rename failed: %d", GetLastError()));
						}
					}
				}

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		bool CreateBackupInfo()
		{
			rtry
			{

				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify (SUCCEEDED(hr) && configV1, catchall, );

				// extract the file at this index
				ICloudSaveFile* file = m_LocalManifest->GetFile(m_CurrentIndex);
				rverify(file, catchall, );

				// verify its status as a genuine bona fide v2 file
				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				// Create the backup info
				RgscCloudSaveBackupInfo info;
				info.SetClientLastModifiedDate(rgscSaveFileV2->GetClientLastModifiedDate());
				info.SetFileName(rgscSaveFileV2->GetFileName());
				info.SetMd5Hash(rgscSaveFileV2->GetMd5Hash());
				info.SetMetaData(rgscSaveFileV2->GetMetaData());
				info.SetHardwareId(rgscSaveFileV2->GetHardwareId());
				if (info.GetMetaData()[0] == '\0')
				{
					rlError("Empty client metadata, using empty JSON string for backup.");
					info.SetMetaData(EMPTY_CLOUD_METADATA);
				}

				// Export the backup info
				u32 bytesExported;
				u8 buf[RgscCloudSaveBackupInfo::MAX_EXPORTED_SIZE_IN_BYTES];
				rverify(info.Export(buf, RgscCloudSaveBackupInfo::MAX_EXPORTED_SIZE_IN_BYTES, &bytesExported), catchall, );

				// Create a backup info path
				char backupPath[RGSC_MAX_PATH];
				rverify(BuildBackupPath(backupPath, configV1, rgscSaveFileV2), catchall, );
				safecatf(backupPath, RGSC_CLOUDSAVE_BACKUP_INFO_EXTENSION, 0);

				const fiDevice* device = fiDevice::GetDevice(backupPath);
				rverify(device, catchall, );

				// Clear readonly flag
				ClearReadOnlyAttribute(device, backupPath);

				// Open the file
				fiHandle hFile = device->Create(backupPath);
				rverify(fiIsValidHandle(hFile), catchall, );

				// Write to it
				int bytesWritten = device->Write(hFile, buf, bytesExported);
				device->Close(hFile);

				RetailCloudSaveTaskLog("Exported backup info to '%s' (%d bytes)", backupPath, bytesWritten);

				// Verify that we wrote enough
				rverify(bytesWritten == (int)bytesExported, catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		bool TryBackupCurrentFile()
		{
			rtry
			{
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify (SUCCEEDED(hr) && configV1, catchall, );

				// extract the file at this index
				ICloudSaveFile* file = m_LocalManifest->GetFile(m_CurrentIndex);
				rverify(file, catchall, );

				// verify its status as a genuine bona fide v2 file
				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				// Shift all the cloud files down a slot.
				rverify(CascadeBackupFiles(), catchall, );

				// Get the current file path
				char fullPath[RGSC_MAX_PATH];
				rverify(BuildFullPath(fullPath, configV1, rgscSaveFileV2), catchall, );

				// Get the path of the first backup index
				char backupPath[RGSC_MAX_PATH];
				rverify(BuildBackupPath(backupPath, configV1, rgscSaveFileV2), catchall, );
				safecatf(backupPath, RGSC_CLOUDSAVE_BACKUP_EXTENSION, 0);

				// Convert to wide path
				wchar_t srcPathW[RGSC_MAX_PATH];
				rverify(MultiByteToWideChar(CP_UTF8 , NULL , fullPath, -1, srcPathW, RGSC_MAX_PATH) != 0, catchall, );

				// Convert to wide path
				wchar_t dstPathW[RGSC_MAX_PATH];
				rverify(MultiByteToWideChar(CP_UTF8 , NULL , backupPath, -1, dstPathW, RGSC_MAX_PATH) != 0, catchall, );

				// Copy the cloud file to .csb0
				RetailCloudSaveTaskLog("Creating backup from '%ls' to '%ls'", srcPathW, dstPathW);
				rverify(CopyFileW(srcPathW, dstPathW, FALSE) != 0, catchall, rlTaskError("CopyFileW failed: %d", GetLastError()));

				// Create the backup information for this file.
				rverify(CreateBackupInfo(), catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		bool TryGetCurrentFile()
		{
			rtry
			{
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify (SUCCEEDED(hr) && configV1, catchall, );

				rverify(m_CurrentIndex < m_LocalManifest->GetNumFiles(), catchall, );
				m_GetFile.Clear();

				ICloudSaveFile* file = m_LocalManifest->GetFile(m_CurrentIndex);
				rverify(file, catchall, );

				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				rverify(SetupFile(file), catchall, );

				RetailCloudSaveTaskLog("Backing up file from server: '%s'", rgscSaveFileV2->GetFileName());

				// Append the .csf extension
				char fullPath[RGSC_MAX_PATH];
				rverify(BuildBackupPath(fullPath, configV1, rgscSaveFileV2), catchall, );
				safecat(fullPath, RGSC_CLOUDSAVE_EXTENSION); // ".csf"

				rage::eCloudSaveResolveType resolveType = RgscToRageResolveType(rgscSaveFileV2->GetResolveType());
				const char* titleAccessToken = GetAccessTokenFromManifest(m_LocalManifest);

				rverify(rlCloudSave::DownloadFile(RL_RGSC_GAMER_INDEX, &m_GetFile, resolveType, fullPath, m_CustomBounceBuffer, 
												  COUNTOF(m_CustomBounceBuffer), titleAccessToken, &m_MyStatus), catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		bool SetupFile(ICloudSaveFile* file)
		{
			ICloudSaveFileV2* rgscSaveFileV2 = NULL;
			if (SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2)
			{
				m_GetFile.SetFileId(rgscSaveFileV2->GetFileId());
				m_GetFile.SetMd5Hash(rgscSaveFileV2->GetMd5Hash());
				m_GetFile.SetFileName(rgscSaveFileV2->GetFileName());
				m_GetFile.SetFileVersion(rgscSaveFileV2->GetLatestServerVersion());
				m_GetFile.SetClientLastModifiedDate(rgscSaveFileV2->GetClientLastModifiedDate());
				m_GetFile.SetServerLastModifiedDate(rgscSaveFileV2->GetServerLastModifiedDate());
				m_GetFile.SetHardwareId(rgscSaveFileV2->GetHardwareId());
				m_GetFile.SetNextExpectedVersion(rgscSaveFileV2->GetNextExpectedVersion());
				m_GetFile.SetLatestServerVersion(rgscSaveFileV2->GetLatestServerVersion());

				netIpAddress address;
				address.FromString(rgscSaveFileV2->GetLastIpAddress());
				m_GetFile.SetLastIpAddress(address);

				// extract hardware id from configuration
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				if (SUCCEEDED(hr) && configV1)
				{
					m_GetFile.SetHardwareId(configV1->GetHardwareId());
				}

				return true;
			}

			return false;
		}

		void DownloadComplete()
		{
			ICloudSaveManagerV3::BackupConflictedFilesErrors resultCode = ICloudSaveManagerV3::BackupConflictedFilesErrors::BCFE_ERROR_GETTING_FILE;

			rtry
			{
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify (SUCCEEDED(hr) && configV1, catchall, );

				// extract the file at this index
				ICloudSaveFile* file = m_LocalManifest->GetFile(m_CurrentIndex);
				rverify(file, catchall, );

				// verify its status as a genuine bona fide v2 file
				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				// validate the data we got matches what the manifest expected
				rverify(rgscSaveFileV2->GetServerFileSize() == m_GetFile.GetClientFileSize(), catchall, resultCode = ICloudSaveManagerV3::BackupConflictedFilesErrors::BCFE_ERROR_GETTING_FILE_INVALID_SIZE);

				// Append the .csf extension
				char fullPath[RGSC_MAX_PATH];
				rverify(BuildBackupPath(fullPath, configV1, rgscSaveFileV2), catchall, );
				safecat(fullPath, RGSC_CLOUDSAVE_EXTENSION); // ".csf"

#if RGSC_CHECK_CLOUDSAVE_MD5
				// validate md5 hash
				rverify(rlCloudSave::VerifyMd5Hash(fullPath, m_GetFile.GetMd5Hash(), NULL), catchall, resultCode = ICloudSaveManagerV3::BackupConflictedFilesErrors::BCFE_ERROR_GETTING_FILE_MD5_HASH);
#endif

				rverify(CascadeBackupFiles(), catchall, resultCode = ICloudSaveManagerV3::BackupConflictedFilesErrors::BCFE_ERROR_FILE_COPY_FAILED);

				// Copy the new file to .csb0
				char backupPath[RGSC_MAX_PATH];
				rverify(BuildBackupPath(backupPath, configV1, rgscSaveFileV2), catchall, );
				safecatf(backupPath, RGSC_CLOUDSAVE_BACKUP_EXTENSION, 0);

				// Copy the downloaded file to its new location
				const fiDevice* device = fiDevice::GetDevice(backupPath);
				rverify(device, catchall, );

				// Clear read only attribute
				ClearReadOnlyAttribute(device, backupPath);

				rlTaskDebug3("Moving from '%s' to '%s'", fullPath, backupPath);
				rverify(device->Rename(fullPath, backupPath), catchall, resultCode = ICloudSaveManagerV3::BackupConflictedFilesErrors::BCFE_ERROR_FILE_COPY_FAILED);

				// Create the backup info for the file we just created.
				rverify(CreateBackupInfo(), catchall, resultCode = ICloudSaveManagerV3::BackupConflictedFilesErrors::BCFE_ERROR_BACKUP_INFO_FAILED);
			}
			rcatchall
			{
				this->Finish(FINISH_FAILED, (int)resultCode);
			}

			// increment the current index for the next iteration
			m_CurrentIndex++;
		}

		State m_State;
		netStatus m_MyStatus;
		ICloudSaveManifestV2* m_LocalManifest;
		ICloudSaveManifestV3* m_LocalManifestV3;
		ICloudSaveOperationProgressV1* m_ProgressTrackerV1;
		rlCloudSaveFile m_GetFile;
		unsigned m_CurrentIndex;
		unsigned m_FilesBackedUp;

		// the default 8k bounce buffer in RageNet/HTTP is fairly small and results in slower downloads
		//	at lower tick rates (i.e. 30tick launcher). Use a larger bounce buffer for the request.
		// As a future improvement, a new CloudManager interface could expose a custom bounce buffer (and if
		// not provided, we could instead allocate the memory here).
		u8 m_CustomBounceBuffer[32 * 1024];
	};

	class SearchBackupFilesTask : public rgscTask<RgscCloudSaveManager>
	{
	public:
		RL_TASK_DECL(SearchBackupFilesTask);

		SearchBackupFilesTask()
			: m_LocalManifest(NULL)
			, m_CurrentIndex(0)
		{

		}

		~SearchBackupFilesTask()
		{
		}

		bool Configure(RgscCloudSaveManager* /*ctx*/, ICloudSaveManifest* manifest)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(manifest, catchall, );

				// V2 is the earliest accepted manifest for this task
				RGSC_HRESULT hr = manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&m_LocalManifest);
				rverify(SUCCEEDED(hr) && m_LocalManifest, catchall, );

				// Check if this manifest has cloud saves enabled.
				rverify(m_LocalManifest->GetCloudSaveEnabled() == ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES, catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		virtual void Start()
		{
			rlTaskDebug2("Searching for backup files...");

			this->rgscTask<RgscCloudSaveManager>::Start();
		}

		virtual void Finish(const FinishType finishType, const int resultCode = 0)
		{
			rlTaskDebug2("Finished searching for backup files");

			this->rgscTask<RgscCloudSaveManager>::Finish(finishType, resultCode);
		}

		virtual bool IsCancelable() const
		{
			return true;
		}

		virtual void Update(const unsigned timeStep)
		{
			this->rgscTask<RgscCloudSaveManager>::Update(timeStep);

			if(this->WasCanceled())
			{
				return;
			}

			do 
			{
				if (HasEnumeratedAllFiles())
				{
					this->Finish(FINISH_SUCCEEDED);
				}
				else if (SearchCurrentIndex())
				{
					m_CurrentIndex++;
				}
				else
				{
					this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV2::GetUpdatedFilesErrors::GUFE_ERROR_SETUP_FILE);
				}
			}
			while(this->IsActive());
		}

	private:

		bool HasEnumeratedAllFiles()
		{
			return m_CurrentIndex >= m_LocalManifest->GetNumFiles();
		}

		bool SearchCurrentIndex()
		{
			rtry
			{
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify (SUCCEEDED(hr) && configV1, catchall, );

				// extract the file at this index
				ICloudSaveFile* file = m_LocalManifest->GetFile(m_CurrentIndex);
				rverify(file, catchall, );

				// verify its status as a genuine bona fide v3 file
				ICloudSaveFileV3* rgscSaveFileV3 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV3)) && rgscSaveFileV3, catchall, );

				// Create file path for backups
				char csPath[RGSC_MAX_PATH];
				rverify(BuildBackupPath(csPath, configV1, rgscSaveFileV3), catchall, );

				const fiDevice* device = fiDevice::GetDevice(csPath);
				rverify(device, catchall, );

				s64 backupSlots = 0;

#if !__NO_OUTPUT
				char debugOutput[65] = {0};
#endif

				int numSlots = GetRgscConcreteInstance()->_GetCloudSaveManager()->GetNumBackupSlots();
				for (int i = 0; i < numSlots; i++)
				{
					// Create the backup path, which is .csb(i)
					char backupPath[RGSC_MAX_PATH];
					safecpy(backupPath, csPath);
					safecatf(backupPath, RGSC_CLOUDSAVE_BACKUP_EXTENSION, i);

					if (device->GetFileSize(backupPath) > 0)
					{
						rlTaskDebug3("Found backup for '%s' at '%s'", rgscSaveFileV3->GetFileName(), backupPath);
						backupSlots = BIT_SET<s64>(i, backupSlots);
#if !__NO_OUTPUT
						safecat(debugOutput, "1");
#endif !__NO_OUTPUT
					}
					else
					{
#if !__NO_OUTPUT
						safecat(debugOutput, "0");
#endif
					}
				}

				rgscSaveFileV3->SetHasSearchedBackups(true);
				rgscSaveFileV3->SetBackupSlots(backupSlots);

				rlTaskDebug3("Backup slot/flags for '%s' : %" I64FMT "d (%s)", rgscSaveFileV3->GetFileName(), backupSlots, debugOutput);
				return true;
			}
			rcatchall
			{
			}

			return false;
		}
		
		ICloudSaveManifestV2* m_LocalManifest;
		unsigned m_CurrentIndex;
	};

	class RestoreBackupTask : public rgscTask<RgscCloudSaveManager>
	{
	public:
		RL_TASK_DECL(RestoreBackupTask);

		enum State
		{
			SWAP_FILES,
			REGISTER_FILE,
			REGISTERING_FILE
		};

		RestoreBackupTask()
			: m_LocalManifest(NULL)
			, m_FileIndex(0)
			, m_BackupIndex(0)
			, m_ConfigV1(NULL)
			, m_RgscSaveFileV2(NULL)
			, m_State(SWAP_FILES)
		{

		}

		~RestoreBackupTask()
		{
		}

		bool Configure(RgscCloudSaveManager* /*ctx*/, ICloudSaveManifest* manifest, int fileIndex, int backupIndex)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(manifest, catchall, );
				rverify(fileIndex >= 0, catchall, );
				rverify(backupIndex >= 0 && backupIndex < MAX_NUM_BACKUP_SLOTS, catchall, );

				m_FileIndex = fileIndex;
				m_BackupIndex = backupIndex;

				// V2 is the earliest accepted manifest for this task
				RGSC_HRESULT hr = manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&m_LocalManifest);
				rverify(SUCCEEDED(hr) && m_LocalManifest, catchall, );

				// Check if this manifest has cloud saves enabled.
				rverify(m_LocalManifest->GetCloudSaveEnabled() == ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES, catchall, );

				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&m_ConfigV1);
				rverify (SUCCEEDED(hr) && m_ConfigV1, catchall, );

				// extract the file at this index
				ICloudSaveFile* file = m_LocalManifest->GetFile(m_FileIndex);
				rverify(file, catchall, );
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&m_RgscSaveFileV2)) && m_RgscSaveFileV2, catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		virtual void Start()
		{
			RetailCloudSaveTaskLog("Restoring backup file at file index %d (backup slot: %d)", m_FileIndex, m_BackupIndex);

			m_State = SWAP_FILES;

			this->rgscTask<RgscCloudSaveManager>::Start();
		}

		virtual void Finish(const FinishType finishType, const int resultCode = 0)
		{
			RetailCloudSaveTaskLog("Finished restoring backup.");

			this->rgscTask<RgscCloudSaveManager>::Finish(finishType, resultCode);
		}

		virtual bool IsCancelable() const
		{
			return true;
		}

		virtual void DoCancel()
		{
			if (m_Status.Pending())
			{
				m_Status.Cancel();
			}
			rgscTask::DoCancel();
		}

		virtual void Update(const unsigned timeStep)
		{
			this->rgscTask<RgscCloudSaveManager>::Update(timeStep);

			if(this->WasCanceled())
			{
				return;
			}

			do 
			{
				switch(m_State)
				{
				case SWAP_FILES:
					if (SwapFiles())
					{
						m_State = REGISTER_FILE;
					}
					else
					{
						this->Finish(FINISH_FAILED);
					}
					break;
				case REGISTER_FILE:
					if (RegisterFile())
					{
						m_State = REGISTERING_FILE;
					}
					else
					{
						this->Finish(FINISH_FAILED);
					}
					break;
				case REGISTERING_FILE:
					if (!m_Status.Pending())
					{
						if (m_Status.Succeeded())
						{
							this->Finish(FINISH_SUCCEEDED);
						}
						else
						{
							this->Finish(FINISH_FAILED);
						}
					}
					break;
				}
			}
			while(!m_Status.Pending() && this->IsActive());
		}

		bool LoadBackupInfo()
		{
			rtry
			{
				// Create file path for backups
				char backupPath[RGSC_MAX_PATH];
				rverify(BuildBackupPath(backupPath, m_ConfigV1, m_RgscSaveFileV2), catchall, );
				safecatf(backupPath, RGSC_CLOUDSAVE_BACKUP_INFO_EXTENSION, m_BackupIndex);

				// get device for this path
				const fiDevice* device = fiDevice::GetDevice(backupPath);
				rverify(device, catchall, );

				u64 file_size_u64 = device->GetFileSize(backupPath);

				// Sanity check. lets make sure we're within reason, Win32 compile compatibility below
				rverify(file_size_u64 < (u32)-1, catchall, );

				// convert to u32
				u32 fileSize = (u32)file_size_u64;

				u8 buf[RgscCloudSaveBackupInfo::MAX_EXPORTED_SIZE_IN_BYTES];

				// verify the file size is reasonable within our IO buffer
				rverify(fileSize <= RgscCloudSaveBackupInfo::MAX_EXPORTED_SIZE_IN_BYTES, catchall, );

				// Open the file
				fiHandle hFile = device->Open(backupPath, true);
				rverify(fiIsValidHandle(hFile), catchall, );

				// read the manifest and import
				int bytesRead = device->Read(hFile, buf, fileSize);
				device->Close(hFile);

				// import the file
				unsigned importSize;
				rverify(m_BackupInfo.Import(buf, bytesRead, &importSize), catchall, );
				rverify(bytesRead == (int)importSize, catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		bool WriteBackupInfo()
		{
			rtry
			{
				// Export the backup info
				u32 bytesExported;
				u8 buf[RgscCloudSaveBackupInfo::MAX_EXPORTED_SIZE_IN_BYTES];
				rverify(m_BackupInfo.Export(buf, RgscCloudSaveBackupInfo::MAX_EXPORTED_SIZE_IN_BYTES, &bytesExported), catchall, );

				// Create a backup info path
				char backupPath[RGSC_MAX_PATH];
				rverify(BuildBackupPath(backupPath, m_ConfigV1, m_RgscSaveFileV2), catchall, );
				safecatf(backupPath, RGSC_CLOUDSAVE_BACKUP_INFO_EXTENSION, m_BackupIndex);

				const fiDevice* device = fiDevice::GetDevice(backupPath);
				rverify(device, catchall, );

				// Clear readonly flag
				ClearReadOnlyAttribute(device, backupPath);

				// Open the file
				fiHandle hFile = device->Create(backupPath);
				rverify(fiIsValidHandle(hFile), catchall, );

				// Write to it
				int bytesWritten = device->Write(hFile, buf, bytesExported);
				device->Close(hFile);

				RetailCloudSaveTaskLog("Exported backup info to '%s' (%d bytes)", backupPath, bytesWritten);

				// Verify that we wrote enough
				rverify(bytesWritten == (int)bytesExported, catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		bool SwapFiles()
		{
			bool success = false;

			char fullPath[RGSC_MAX_PATH];
			char tempPath[RGSC_MAX_PATH];
			char backupPath[RGSC_MAX_PATH];

			const fiDevice* device = NULL;

			rtry
			{
				rverify(LoadBackupInfo(), catchall, );

				// Get the current file path
				rverify(BuildFullPath(fullPath, m_ConfigV1, m_RgscSaveFileV2), catchall, );

				// Get the path of the temporary backup (ie: .temp0 )
				rverify(BuildBackupPath(tempPath, m_ConfigV1, m_RgscSaveFileV2), catchall, );
				safecatf(tempPath, RGSC_CLOUDSAVE_TEMP_EXTENSION, m_BackupIndex);

				// Get the path of the backup file (ie: .csb0 )
				rverify(BuildBackupPath(backupPath, m_ConfigV1, m_RgscSaveFileV2), catchall, );
				safecatf(backupPath, RGSC_CLOUDSAVE_BACKUP_EXTENSION, m_BackupIndex);

				device = fiDevice::GetDevice(backupPath);
				rverify(device, catchall, );

				// Validate that both file exist
				rverify(device->GetFileSize(fullPath) > 0, catchall, rlError("Cloud file did not exist"));
				rverify(device->GetFileSize(backupPath) > 0, catchall, rlError("Backup file did not exist"));

				// Clear read only attributes
				ClearReadOnlyAttribute(device, fullPath);
				ClearReadOnlyAttribute(device, backupPath);
				ClearReadOnlyAttribute(device, tempPath);

				// Convert to wide path
				wchar_t srcPathW[RGSC_MAX_PATH];
				MultiByteToWideChar(CP_UTF8 , NULL , fullPath, -1, srcPathW, RGSC_MAX_PATH);

				// Convert to wide path
				wchar_t tempPathW[RGSC_MAX_PATH];
				MultiByteToWideChar(CP_UTF8 , NULL , tempPath, -1, tempPathW, RGSC_MAX_PATH);

				// Copy the cloud save file to .temp
				rlTaskDebug3("Copying from '%ls' to '%ls'", srcPathW, tempPathW);
				rverify(CopyFileW(srcPathW, tempPathW, FALSE) != 0, catchall, rlTaskError("CopyFileW failed: %d", GetLastError()));

				// Move the backup file to the cloud save file
				rlTaskDebug3("Moving from '%s' to '%s'", backupPath, fullPath);
				rverify(device->Rename(backupPath, fullPath), catchall, );

				// Move the temp file to the backup file
				rlTaskDebug3("Moving from '%s' to '%s'", tempPath, backupPath);
				rverify(device->Rename(tempPath, backupPath), restoreFile, ); /* runs the restore path if this fails */ 

				RetailCloudSaveTaskLog("Restoring backup '%s' to '%s'", backupPath, fullPath);

				// Read the current file time
				u64 currentFileTime;
				fiDevice::SystemTime currentTime;
				fiDevice::GetLocalSystemTime(currentTime);

				// Set the restored files last modified time to now
				FILETIME localFileTime, fileTime;
				if (SystemTimeToFileTime((SYSTEMTIME*)&currentTime, &localFileTime) && // convert the system time to local time
					LocalFileTimeToFileTime(&localFileTime, &fileTime)) // and convert the local time to the file time
				{
					ULARGE_INTEGER large;
					large.LowPart  = fileTime.dwLowDateTime;
					large.HighPart = fileTime.dwHighDateTime;
					currentFileTime = large.QuadPart;
				}
				else
				{
					currentFileTime = 0;
				}

				// Convert to POSIX time
				u64 currentPosixTime = WindowsTimeToPosixTime(currentFileTime);

				// create a copy of the last modified time, metadata and md5 hash
				u64 lastModifiedTime = m_RgscSaveFileV2->GetClientLastModifiedDate();

				char metadata[rlCloudSaveFile::METADATA_MAX_BUF_SIZE];
				safecpy(metadata, m_RgscSaveFileV2->GetMetaData());

				char md5Hash[rlCloudSaveFile::MD5_HASH_BUF_SIZE];
				safecpy(md5Hash, m_RgscSaveFileV2->GetMd5Hash());
				
				// Copy over the metadata and md5 hash to the manifest file
				device->SetFileTime(fullPath, currentFileTime);
				m_RgscSaveFileV2->SetClientLastModifiedDate(currentPosixTime);
				m_RgscSaveFileV2->SetMetaData(m_BackupInfo.GetMetaData());
				m_RgscSaveFileV2->SetMd5Hash(m_BackupInfo.GetMd5Hash());

				// Write new backup info using the old manifest file's data
				device->SetFileTime(backupPath, PosixTimeToWindowsTime(lastModifiedTime));
				m_BackupInfo.SetClientLastModifiedDate(lastModifiedTime);
				m_BackupInfo.SetMetaData(metadata);
				m_BackupInfo.SetMd5Hash(md5Hash);

				// export the backup info
				rverify(WriteBackupInfo(), catchall, );
				
				// save the manifest to disk
				rverify(m_LocalManifest->Save(), catchall, );

				success = true;
			}
			rcatch(restoreFile)
			{
				// Remove read-only attribute
				ClearReadOnlyAttribute(device, fullPath);

				// Move the temporary file back over top of the original
				rlTaskDebug3("Moving from '%s' to '%s'", tempPath, fullPath);
				rlVerify(device->Rename(tempPath, fullPath));
			}
			rcatchall
			{
			}

			return success;
		}

		bool RegisterFile()
		{
			return GetRgscConcreteInstance()->_GetCloudSaveManager()->RegisterFileForUploadAsync(m_LocalManifest, m_RgscSaveFileV2->GetFileName(), m_RgscSaveFileV2->GetMetaData(), &m_Status);
		}

	private:

		State m_State;
		int m_FileIndex;
		int m_BackupIndex;
		RgscCloudSaveBackupInfo m_BackupInfo;

		ICloudSaveManifestV2* m_LocalManifest;
		ICloudSaveFileV2* m_RgscSaveFileV2;
		ICloudSaveTitleConfigurationV1* m_ConfigV1;

		AsyncStatus m_Status;
	};

	class GetConflictMetadataTask : public rgscTask<RgscCloudSaveManager>
	{
	public:
		RL_TASK_DECL(GetConflictMetadataTask);

		enum State
		{
			STATE_INVALID   = -1,
			STATE_GET_METADATA,
			STATE_GETTING_METADATA
		};

		GetConflictMetadataTask()
			: m_State(STATE_INVALID)
			, m_LocalManifest(NULL)
			, m_LocalManifestV3(NULL)
			, m_ProgressTrackerV1(NULL)
			, m_CurrentIndex(0)
			, m_MetadataDownloaded(0)
		{

		}

		~GetConflictMetadataTask()
		{
		}

		bool Configure(RgscCloudSaveManager* /*ctx*/, ICloudSaveManifest* manifest)
		{
			rtry
			{
				rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

				rverify(manifest, catchall, );

				// V2 is the earliest accepted manifest for this task
				RGSC_HRESULT hr = manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&m_LocalManifest);
				rverify(SUCCEEDED(hr) && m_LocalManifest, catchall, );

				// V3 enables optional functionality
				hr = manifest->QueryInterface(IID_ICloudSaveManifestV3, (void**)&m_LocalManifestV3);
				if (SUCCEEDED(hr) && m_LocalManifestV3)
				{
					ICloudSaveOperationProgress* progress = m_LocalManifestV3->GetProgressTracker();
					rverify(progress, catchall, );

					hr = progress->QueryInterface(IID_ICloudSaveOperationProgressV1, (void**)&m_ProgressTrackerV1);
					rverify(SUCCEEDED(hr) && m_ProgressTrackerV1, catchall, );
				}

				// Check if this manifest has cloud saves enabled.
				rverify(m_LocalManifest->GetCloudSaveEnabled() == ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES, catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		virtual void Start()
		{
			rlTaskDebug2("Downloading metadata for conflicted files....");

			this->rgscTask<RgscCloudSaveManager>::Start();

			if (m_ProgressTrackerV1)
			{
				m_ProgressTrackerV1->SetStepCount(0);
				m_ProgressTrackerV1->SetCompletedCount(0);
				m_ProgressTrackerV1->SetIsActive(true);

				SetupProgressTracker();
			}

			m_State = STATE_GET_METADATA;
		}

		virtual void Finish(const FinishType finishType, const int resultCode = 0)
		{
			rlTaskDebug2("Finished downloading metadata for %d conflicted files", m_MetadataDownloaded);

			if (finishType != rlTaskBase::FINISH_CANCELED)
			{
				if (m_ProgressTrackerV1)
				{
					m_ProgressTrackerV1->SetIsActive(false);
				}
			}

			this->rgscTask<RgscCloudSaveManager>::Finish(finishType, resultCode);
		}

		virtual bool IsCancelable() const
		{
			return true;
		}

		virtual void DoCancel()
		{
			RgscTaskManager::CancelTaskIfNecessary(&m_MyStatus);
			rgscTask::DoCancel();
		}

		virtual void Update(const unsigned timeStep)
		{
			this->rgscTask<RgscCloudSaveManager>::Update(timeStep);

			if(this->WasCanceled())
			{
				//Wait for dependencies to finish
				if(!m_MyStatus.Pending())
				{
					this->Finish(FINISH_CANCELED);
				}

				return;
			}

			do 
			{
				switch(m_State)
				{
				case STATE_INVALID:
					break;
				case STATE_GET_METADATA:
					if (HasEnumeratedAllFiles())
					{
						this->Finish(FINISH_SUCCEEDED);
					}
					else if (DoesFileRequireMetadata(m_CurrentIndex))
					{
						if (TryGetCurrentFile())
						{
							m_State = STATE_GETTING_METADATA;
						}
						else
						{
							this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV3::GetConflictMetadataErrors::GCME_ERROR_SETUP_FILE);
						}
					}
					else
					{
						m_CurrentIndex++;
					}
					break;
				case STATE_GETTING_METADATA:
					if (!m_MyStatus.Pending())
					{
						if (m_MyStatus.Succeeded())
						{
							m_MetadataDownloaded++;
							if (m_ProgressTrackerV1)
							{
								m_ProgressTrackerV1->SetCompletedCount(m_MetadataDownloaded);
							}
							OnGetMetadata();
							m_State = STATE_GET_METADATA;
						}
						else if (m_MyStatus.Failed() && m_MyStatus.GetResultCode() == RLCS_ERROR_DOES_NOT_EXIST)
						{
							m_MetadataDownloaded++;
							if (m_ProgressTrackerV1)
							{
								m_ProgressTrackerV1->SetCompletedCount(m_MetadataDownloaded);
							}
							OnMetadataMissing();
							m_State = STATE_GET_METADATA;
						}
						else
						{
							// copy http debug info from the metadata file request to the out manifest.
							const netHttpRequest::DebugInfo& debugInfo = m_GetFile.GetHttpDebugInfo();
							FillOutHttpDebugInfo(m_LocalManifest, debugInfo);

							RetailCloudSaveTaskLog("Cloud Save retrieve conflict metadata failed. HTTP Code: %d, Reason: %d", debugInfo.m_HttpStatusCode, debugInfo.m_AbortReason);

							this->Finish(FINISH_FAILED, (int)ICloudSaveManagerV3::GetConflictMetadataErrors::GCME_ERROR_GETTING_METADATA);
						}
					}
					break;
				}
			}
			while(!m_MyStatus.Pending() && this->IsActive());
		}

	private:

		bool HasEnumeratedAllFiles()
		{
			return m_CurrentIndex >= m_LocalManifest->GetNumFiles();
		}
		
		void SetupProgressTracker()
		{
			rtry
			{
				rcheck(m_ProgressTrackerV1, catchall, );

				int numFiles = 0;
				for (unsigned i = 0; i < m_LocalManifest->GetNumFiles(); i++)
				{
					if (DoesFileRequireMetadata(i))
					{
						numFiles++;
					}
				}

				m_ProgressTrackerV1->SetStepCount(numFiles);
			}
			rcatchall
			{

			}
		}

		void OnMetadataMissing()
		{
			rtry
			{
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify (SUCCEEDED(hr) && configV1, catchall, );

				// extract the file at this index
				ICloudSaveFile* file = m_LocalManifest->GetFile(m_CurrentIndex);
				rverify(file, catchall, );

				// verify its status as a genuine bona fide v2 file
				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				rlError("Empty metadata from server, using empty JSON string.");
				rgscSaveFileV2->SetServerMetadata(EMPTY_CLOUD_METADATA);
			}
			rcatchall
			{
				this->Finish(FINISH_FAILED);
			}

			// increment the current index for the next iteration
			m_CurrentIndex++;
		}

		void OnGetMetadata()
		{
			rtry
			{
				ICloudSaveTitleConfiguration* config = m_LocalManifest->GetConfiguration();
				ICloudSaveTitleConfigurationV1* configV1 = NULL;
				RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
				rverify (SUCCEEDED(hr) && configV1, catchall, );

				// extract the file at this index
				ICloudSaveFile* file = m_LocalManifest->GetFile(m_CurrentIndex);
				rverify(file, catchall, );

				// verify its status as a genuine bona fide v2 file
				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				rgscSaveFileV2->SetServerMetadata(m_GetFile.GetServerMetadata());
			}
			rcatchall
			{
				this->Finish(FINISH_FAILED);
			}

			// increment the current index for the next iteration
			m_CurrentIndex++;
		}

		bool DoesFileRequireMetadata(u32 index)
		{
			rtry
			{
				rverify(index < m_LocalManifest->GetNumFiles(), catchall, );

				ICloudSaveFile* file = m_LocalManifest->GetFile(index);
				rverify(file, catchall, );

				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				// Files not on the server do not require metadata.
				//	A 'NextExpectedVersion' of 0 indicates a deleted file.
				if (rgscSaveFileV2->GetNextExpectedVersion() == 0)
					return false;

				// url:bugstar:2833276 - 
				//	Note: For FileMissing, if we're fully in sync we can skip the metadata.
				//	Otherwise, if we're not in sync or the client metadata is missing, force retrieve it.
				if (rgscSaveFileV2->GetConflictState() == ICloudSaveFileV2::ConflictState::CS_FileMissing)
				{
					if (rgscSaveFileV2->GetFileVersion() == rgscSaveFileV2->GetLatestServerVersion() && rgscSaveFileV2->GetMetaData()[0] != '\0')
					{
						// Copy to the server metadata, and skip.
						rgscSaveFileV2->SetServerMetadata(rgscSaveFileV2->GetMetaData());
						return false;
					}

					return true;
				}

				// Validate as an conflict file that requires a backup of the local file
				if (rgscSaveFileV2->GetConflictState() != ICloudSaveFileV2::ConflictState::CS_None &&
					rgscSaveFileV2->GetConflictState() != ICloudSaveFileV2::ConflictState::CS_ClientHasNewer &&
					rgscSaveFileV2->GetConflictState() != ICloudSaveFileV2::ConflictState::CS_FileDeleted)
				{
					return true;
				}
			}
			rcatchall
			{
			}

			return false;
		}

		bool TryGetCurrentFile()
		{
			rtry
			{
				rverify(m_CurrentIndex < m_LocalManifest->GetNumFiles(), catchall, );
				m_GetFile.Clear();

				ICloudSaveFile* file = m_LocalManifest->GetFile(m_CurrentIndex);
				rverify(file, catchall, );

				ICloudSaveFileV2* rgscSaveFileV2 = NULL;
				rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

				rverify(SetupFile(file), catchall, );

				rlTaskDebug3("Getting metadata from server for '%s'", rgscSaveFileV2->GetFileName());

				rverify(rlCloudSave::GetFileMetadata(RL_RGSC_GAMER_INDEX, &m_GetFile, GetAccessTokenFromManifest(m_LocalManifest), &m_MyStatus), catchall, );

				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		bool SetupFile(ICloudSaveFile* file)
		{
			ICloudSaveFileV2* rgscSaveFileV2 = NULL;
			if (SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2)
			{
				m_GetFile.SetFileId(rgscSaveFileV2->GetFileId());
				m_GetFile.SetFileVersion(rgscSaveFileV2->GetLatestServerVersion());
				return true;
			}

			return false;
		}

		State m_State;
		netStatus m_MyStatus;
		rlCloudSaveFile m_GetFile;
		ICloudSaveManifestV2* m_LocalManifest;
		ICloudSaveManifestV3* m_LocalManifestV3;
		ICloudSaveOperationProgressV1* m_ProgressTrackerV1;
		unsigned m_CurrentIndex;
		unsigned m_MetadataDownloaded;
	};

	// ===============================================================================================
	// RgscCloudSaveManifest
	// ===============================================================================================
	RGSC_HRESULT RgscCloudSaveManifest::QueryInterface(RGSC_REFIID riid, void** ppvObj)
	{
		IRgscUnknown *pUnknown = NULL;

		if(ppvObj == NULL)
		{
			return RGSC_INVALIDARG;
		}

		if(riid == IID_IRgscUnknown)
		{
			pUnknown = static_cast<ICloudSaveManifest*>(this);
		}
		else if(riid == IID_ICloudSaveManifestV1)
		{
			pUnknown = static_cast<ICloudSaveManifestV1*>(this);
		}
		else if (riid == IID_ICloudSaveManifestV2)
		{
			pUnknown = static_cast<ICloudSaveManifestV2*>(this);
		}
		else if (riid == IID_ICloudSaveManifestV3)
		{
			pUnknown = static_cast<ICloudSaveManifestV3*>(this);
		}
		else if (riid == IID_ICloudSaveManifestV4)
		{
			pUnknown = static_cast<ICloudSaveManifestV4*>(this);
		}

		*ppvObj = pUnknown;
		if(pUnknown == NULL)
		{
			return RGSC_NOINTERFACE;
		}

		return RGSC_OK;
	}
	
	RgscCloudSaveManifest::RgscCloudSaveManifest()
		: m_Version(0)
		, m_BytesUsed(0)
		, m_CloudSavesEnabledState(ICloudSaveManifestV2::CloudSavesEnabledState::INVALID)
		, m_Configuration(NULL)
		, m_ConfigurationV1(NULL)
	{

	}

	RgscCloudSaveManifest::RgscCloudSaveManifest(u32 numSlots)
		: m_Version(0)
		, m_BytesUsed(0)
		, m_CloudSavesEnabledState(ICloudSaveManifestV2::CloudSavesEnabledState::INVALID)
		, m_Configuration(NULL)
		, m_ConfigurationV1(NULL)
	{
		Reserve(numSlots);
	}

	RgscCloudSaveManifest::~RgscCloudSaveManifest()
	{
		Clear();

		// Clean up any loose .csf files that may have been left on disk.
		if (GetRgscConcreteInstance()->_GetProfileManager()->IsSignedIn())
		{
			Cleanup();
		}
	}

	ICloudSaveTitleConfiguration* RgscCloudSaveManifest::GetConfiguration()
	{
		return m_Configuration;
	}

	const char* RgscCloudSaveManifest::GetTitleName()
	{
		if (m_ConfigurationV1)
		{
			return m_ConfigurationV1->GetTitleName();
		}

		return "";
	}

	bool RgscCloudSaveManifest::Export(void* buf, const unsigned sizeofBuf, unsigned* size) 
	{
		SYS_CS_SYNC(m_ManifestCs);

		datExportBuffer bb;
		bb.SetReadWriteBytes(buf, sizeofBuf);

		bool success = false;

		rtry
		{
			// export as the latest version: V1
			m_Version = VERSION_LATEST;

			rverify(buf, catchall, );

			// Write the Version
			rverify(bb.WriteInt(m_Version, sizeof(m_Version) << 3), catchall, );

			// Write the enabled state and byte count
			int enabledState = (int)m_CloudSavesEnabledState;
			rverify(bb.WriteInt(enabledState, sizeof(enabledState) << 3), catchall, );
			rverify(bb.WriteUns(m_BytesUsed, sizeof(m_BytesUsed) << 3), catchall, );

			// write the file count
			int numFiles = m_CloudFiles.GetCount();
			rverify(bb.WriteInt(numFiles, sizeof(numFiles) << 3), catchall, );

			// write each file individually
			for (int i = 0; i < numFiles; i++)
			{
				rverify(bb.SerUser(*m_CloudFiles[i]), catchall, );
			}

			success = true;        
		}
		rcatchall
		{
		}

		if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

		return success;
	}

	bool RgscCloudSaveManifest::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
	{
		SYS_CS_SYNC(m_ManifestCs);

		datImportBuffer bb;
		bb.SetReadOnlyBytes(buf, sizeofBuf);

		bool success = false;

		Clear();

		rtry
		{
			// Make sure our bufer is valid, and we're empty
			rverify(buf, catchall, );
			rverify(m_CloudFiles.GetCount() == 0, catchall, );

			// read and verify the version
			rverify(bb.ReadUns(m_Version, sizeof(m_Version) << 3), catchall, );
			rverify(IsKnownVersion(m_Version), catchall, );

			if (m_Version == VERSION_1)
			{
				int enabledState = 0;
				rverify(bb.ReadInt(enabledState, sizeof(enabledState) << 3), catchall, );
				m_CloudSavesEnabledState = (ICloudSaveManifestV2::CloudSavesEnabledState)enabledState;

				rverify(bb.ReadUns(m_BytesUsed, sizeof(m_BytesUsed) << 3), catchall, );

				// Read the file count
				int numFiles = m_CloudFiles.GetCount();
				rverify(bb.ReadInt(numFiles, sizeof(numFiles) << 3), catchall, );
				rverify(numFiles < ICloudSaveManager::MAX_CLOUD_SAVE_SLOTS, catchall, );

				// Reserve for the file count if necessary
				if (numFiles > m_CloudFiles.GetCapacity())
				{
					m_CloudFiles.ResizeGrow(numFiles);
				}

				rlDebug3("Loading manifest: %d cloud files", numFiles);
				
				// Import each cloud file individually
				for (int i = 0; i < numFiles; i++)
				{
					// allocate a new file, and import it
					AddFile();
					rverify(bb.SerUser(*m_CloudFiles[i]), catchall, );

					rlDebug3("Manifest file (%d): %s - Version %d - %s(Last Modified: %" I64FMT "u / %" I64FMT "u)", 
						i, 
						m_CloudFiles[i]->GetFileName(), 
						m_CloudFiles[i]->GetFileVersion(), 
						m_CloudFiles[i]->IsDirty() ? " DIRTY " : "",
						m_CloudFiles[i]->GetClientLastModifiedDate(),
						m_CloudFiles[i]->GetServerLastModifiedDate());

					rlDebug3("\tMetaData: %s", m_CloudFiles[i]->GetMetaData());
				}

			}

			success = true;        
		}
		rcatchall
		{
			Clear();
		}

		if(size){*size = success ? bb.GetNumBytesRead() : 0;}

		return success;
	}

	unsigned RgscCloudSaveManifest::GetMaxExportBufSize()
	{
		return MAX_EXPORTED_SIZE_IN_BYTES;
	}

	ICloudSaveManifestV2::CloudSavesEnabledState RgscCloudSaveManifest::GetCloudSaveEnabled()
	{
		return m_CloudSavesEnabledState;
	}

	void RgscCloudSaveManifest::SetCloudSavesEnabled(ICloudSaveManifestV2::CloudSavesEnabledState state)
	{
		m_CloudSavesEnabledState = state;
	}

	bool RgscCloudSaveManifest::SetConfiguration(ICloudSaveTitleConfiguration* config)
	{
		rtry
		{
			rverify(m_Configuration == NULL, catchall, );
			rverify(config, catchall, );

			m_Configuration = config;

			RGSC_HRESULT hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&m_ConfigurationV1);
			rverify(SUCCEEDED(hr), catchall, );
			rverify(m_ConfigurationV1, catchall, );

			Reserve(m_ConfigurationV1->GetNumCloudFiles());

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void RgscCloudSaveManifest::Reserve(u32 numSlots)
	{
		SYS_CS_SYNC(m_ManifestCs);

		m_CloudFiles.Reserve(numSlots);
	}

	void RgscCloudSaveManifest::GetClientLastModifiedDates()
	{
		SYS_CS_SYNC(m_ManifestCs);

		for (int i = 0; i < m_CloudFiles.GetCount(); i++)
		{
			char fullPath[RGSC_MAX_PATH];
			if (BuildFullPath(fullPath, m_ConfigurationV1, m_CloudFiles[i]))
			{

				const fiDevice* device = fiDevice::GetDevice(fullPath);
				if (device)
				{
					u64 fileTime = device->GetFileTime(fullPath);
					u64 fileLastModifiedTime = WindowsTimeToPosixTime(fileTime);

					u64 manifestFileTime = m_CloudFiles[i]->GetClientLastModifiedDate();
					if (fileLastModifiedTime != manifestFileTime)
					{
						rlDebug1("Cloud file '%s' last modified time did not match the manifest. Flagging file as dirty.", m_CloudFiles[i]->GetFileName());
						m_CloudFiles[i]->SetIsDirty(true);
					}
				}
			}
		}
	}

	bool RgscCloudSaveManifest::IsValidFileName(const char* fileName)
	{
		SYS_CS_SYNC(m_ManifestCs);

		rtry
		{
			rverify(m_ConfigurationV1, catchall, );
			rverify(fileName && fileName[0] != '\0', catchall, );

			unsigned count = m_ConfigurationV1->GetNumCloudFiles();
			for (unsigned i = 0; i < count; i++)
			{
				if (stricmp(m_ConfigurationV1->GetFileName(i), fileName) == 0)
					return true;
			}

			return false;
		}
		rcatchall
		{

		}

		return false;
	}

	bool RgscCloudSaveManifest::RegisterForUpload(const char* fileName, const char* metadata)
	{
		rtry
		{
			rlDebug1("Registering cloud file for upload: '%s'", fileName);
			rlDebug1("	File Metadata: %s", metadata);

			rverify(IsValidFileName(fileName), catchall, );

			RgscCloudSaveFile* file = FindFile(fileName);
			if (file == NULL)
			{
				rlDebug1("File does not belong to manifest, adding...");
				file = CreateSaveFile();
				if (file)
				{
					// Server does not know the file, so it must be newer.
					file->SetConflictState(ICloudSaveFileV2::ConflictState::CS_ClientHasNewer);
					file->SetFileName(fileName);
				}
			}

			// validate file and configuration
			rverify(file, catchall, );
			rverify(m_ConfigurationV1, catchall, );

			// update client last modified time
			char fullPath[RGSC_MAX_PATH];
			rverify(BuildFullPath(fullPath, m_ConfigurationV1, file), catchall, );

			const fiDevice* device = fiDevice::GetDevice(fullPath);
			rverify(device, catchall, );

			// get write time from disk
			u64 fileTime = device->GetFileTime(fullPath);
			u64 fileLastModifiedTime = WindowsTimeToPosixTime(fileTime);
			file->SetClientLastModifiedDate(fileLastModifiedTime);

			// debug output the new last modified time
			rlDebug1("	Client Last Modified Time: %" I64FMT "u", fileLastModifiedTime);
			
			// update its metadata
			file->SetMetaData(metadata);

			// flag as dirty
			file->SetIsDirty(true);

			// clear the delete flag
			file->SetMarkedForDeletion(false);

			RgscDisplay("Cloud file registered: %s", fileName);

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool RgscCloudSaveManifest::UnregisterFile(const char* fileName)
	{
		rtry
		{
			rlDebug1("Unregistering cloud file: '%s'", fileName);

			rverify(IsValidFileName(fileName), catchall, );

			RgscCloudSaveFile* file = FindFile(fileName);
			if (!rlVerify(file != NULL))
			{
				rlError("File does not belong to manifest, ignoring...");
				return true;
			}

			// validate file and configuration
			rverify(file, catchall, );

			// file is no longer dirty
			file->SetIsDirty(false);

			// mark for deletion
			file->SetMarkedForDeletion(true);

			RgscDisplay("Cloud file unregistered: %s", fileName);

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	ICloudSaveOperationProgress* RgscCloudSaveManifest::GetProgressTracker()
	{
		return &m_ProgressTracker;
	}


	ICloudSaveHttpDebugInfo* RgscCloudSaveManifest::GetHttpDebugInfo()
	{
		return &m_HttpDebugInfo;
	}

	int RgscCloudSaveManifest::GetNumUnresolvedConflicts()
	{
		int numUnresolved = 0;

		for (int i = 0; i < m_CloudFiles.GetCount(); i++)
		{
			// Is one of the 3 "no-conflict" states and is unresolved.
			if (m_CloudFiles[i]->GetConflictState() != ICloudSaveFileV2::ConflictState::CS_None &&
				m_CloudFiles[i]->GetConflictState() != ICloudSaveFileV2::ConflictState::CS_FileMissing &&
				m_CloudFiles[i]->GetConflictState() != ICloudSaveFileV2::ConflictState::CS_ClientHasNewer &&
				m_CloudFiles[i]->GetConflictState() != ICloudSaveFileV2::ConflictState::CS_ServerHasNewer &&
				m_CloudFiles[i]->GetConflictState() != ICloudSaveFileV2::ConflictState::CS_FileDeleted &&
				m_CloudFiles[i]->GetResolveType() == ICloudSaveFileV2::ConflictResolutionType::CRT_None)
			{
				numUnresolved++;
			}
		}

		return numUnresolved;
	}

	int RgscCloudSaveManifest::GetNumDirtyFiles()
	{
		SYS_CS_SYNC(m_ManifestCs);

		int numDirty = 0;

		for (int i = 0; i < m_CloudFiles.GetCount(); i++)
		{
			if (m_CloudFiles[i]->IsDirty())
			{
				numDirty++;
			}
		}

		return numDirty;
	}

	bool RgscCloudSaveManifest::Load()
	{
		return GetRgscConcreteInstance()->_GetCloudSaveManager()->LoadManifest(this);
	}

	bool RgscCloudSaveManifest::Save()
	{
		return GetRgscConcreteInstance()->_GetCloudSaveManager()->SaveManifest(this);
	}

	bool RgscCloudSaveManifest::RemoveFile(unsigned index)
	{
		if (rlVerify(index < (unsigned)m_CloudFiles.GetCount()))
		{
			m_CloudFiles.Delete(index);
			return true;
		}

		return false;
	}

	ICloudSaveFile* RgscCloudSaveManifest::AddFile()
	{
		SYS_CS_SYNC(m_ManifestCs);

		return CreateSaveFile();
	}

	void RgscCloudSaveManifest::Clear()
	{
		SYS_CS_SYNC(m_ManifestCs);

		while(m_CloudFiles.GetCount() > 0)
		{
			RgscCloudSaveFile* file = m_CloudFiles.Pop();
			delete file;
		}

		m_Version = 0;
		m_BytesUsed = 0;
		
		// note: cloud saves enabled + configurations left intact. The configurations are per title,
		// and must be set using the CloudManager RegisterTitle APIs.
	}

	u32 RgscCloudSaveManifest::GetNumFiles() const
	{
		return m_CloudFiles.GetCount();
	}

	ICloudSaveFile* RgscCloudSaveManifest::GetFile(unsigned index)
	{
		SYS_CS_SYNC(m_ManifestCs);

		if (rlVerify(index < GetNumFiles()))
		{
			return m_CloudFiles[index]; 
		}

		return NULL; 
	}

	void RgscCloudSaveManifest::SetBytesUsed(u64 bytesUsed)
	{
		m_BytesUsed = bytesUsed;
	}

	u64 RgscCloudSaveManifest::GetBytesUsed() const
	{
		return m_BytesUsed;
	}

	RgscCloudSaveFile* RgscCloudSaveManifest::CreateSaveFile()
	{
		if (rlVerify(m_CloudFiles.GetCount() < m_CloudFiles.GetCapacity()))
		{
			RgscCloudSaveFile* saveFile = rage_new RgscCloudSaveFile();

			if (saveFile)
			{
				m_CloudFiles.Push(saveFile);
			}

			return saveFile;
		}

		return NULL;
	}

	bool RgscCloudSaveManifest::IsKnownVersion(int version)
	{
		return (m_Version == VERSION_1); // || (m_Version == VERSION_2) || ....
	}

	RgscCloudSaveFile* RgscCloudSaveManifest::FindFile(const char* fileName)
	{
		SYS_CS_SYNC(m_ManifestCs);

		for (int i = 0; i < m_CloudFiles.GetCount(); i++)
		{
			if (stricmp(m_CloudFiles[i]->GetFileName(), fileName) == 0)
			{
				return m_CloudFiles[i];
			}
		}

		return NULL;
	}

	void RgscCloudSaveManifest::Cleanup()
	{
		rtry
		{
			// Create the save directory
			char fullPath[RGSC_MAX_PATH];

			// For each cloud file that is declared in the configuration...
			for (unsigned i = 0; i < m_ConfigurationV1->GetNumCloudFiles(); i++)
			{
				safecpy(fullPath, m_ConfigurationV1->GetSaveDirectory());

				// Ensure its backslash terminated. Lets assert here to let the game team fix the config.
				if (!rlVerify(StringEndsWithI(fullPath, "\\")))
				{
					safecat(fullPath, "\\");
				}

				// Extract the file name at the 'i' index
				const char* fileName = m_ConfigurationV1->GetFileName(i);
				rverify(fileName && fileName[0] != '\0', catchall, );
				safecat(fullPath, fileName);
				safecat(fullPath, RGSC_CLOUDSAVE_EXTENSION);

				const fiDevice* device = fiDevice::GetDevice(fullPath);
				rverify(device, catchall, );

				if (device->GetFileSize(fullPath) > 0)
				{
					rlWarning("Stale cloudsave file '%s' found: removing.", fullPath);
					device->Delete(fullPath);
				}
			}
		}
		rcatchall
		{

		}
	}

	// ===============================================================================================
	// RgscCloudSaveFile
	// ===============================================================================================
	RGSC_HRESULT RgscCloudSaveFile::QueryInterface(RGSC_REFIID riid, void** ppvObj)
	{
		IRgscUnknown *pUnknown = NULL;

		if(ppvObj == NULL)
		{
			return RGSC_INVALIDARG;
		}

		if(riid == IID_IRgscUnknown)
		{
			pUnknown = static_cast<ICloudSaveFile*>(this);
		}
		else if(riid == IID_ICloudSaveFileV1)
		{
			pUnknown = static_cast<ICloudSaveFileV1*>(this);
		}
		else if(riid == IID_ICloudSaveFileV2)
		{
			pUnknown = static_cast<ICloudSaveFileV2*>(this);
		}
		else if (riid == IID_ICloudSaveFileV3)
		{
			pUnknown = static_cast<ICloudSaveFileV3*>(this);
		}

		*ppvObj = pUnknown;
		if(pUnknown == NULL)
		{
			return RGSC_NOINTERFACE;
		}

		return RGSC_OK;
	}

	RgscCloudSaveFile::RgscCloudSaveFile()
		// By Default, we assume the client has the newest state.
		: m_ImportVersion(0)
		, m_ConflictState(ConflictState::CS_ClientHasNewer) 
		, m_ResolveType(ConflictResolutionType::CRT_None)
		, m_bIsDirty(false)
		, m_bShouldCopy(false)
		, m_bMarkedForDeletion(false)
		, m_bHasSearchedBackups(false)
		, m_BackupSlots(0)
	{
		m_IpBuf[0] = '\0';
		m_SaveFile.Clear();
	}

	RgscCloudSaveFile::~RgscCloudSaveFile()
	{
		Clear();
	}

	bool RgscCloudSaveFile::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
	{
		datExportBuffer bb;
		bb.SetReadWriteBytes(buf, sizeofBuf);

		sysMemSet(buf, 0, sizeofBuf);

		bool success = false;

		rtry
		{
			// Export as the latest version: version 1
			unsigned exportVersion = VERSION_LATEST;
			rverify(bb.WriteUns(exportVersion, sizeof(exportVersion) << 3), catchall, );

			int conflictState = (int)m_ConflictState;
			rverify(bb.WriteInt(conflictState, sizeof(conflictState) << 3), catchall, );
			rverify(bb.WriteBool(m_bIsDirty), catchall, );
			rverify(bb.WriteBool(m_bMarkedForDeletion), catchall, );
			rverify(bb.SerUser(m_SaveFile), catchall, );
			success = true;
		}
		rcatchall
		{

		}

		Assert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

		if(size)
		{
			*size = success ? bb.GetNumBytesWritten() : 0;
		}

		return success;
	}

	bool RgscCloudSaveFile::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
	{

		datImportBuffer bb;
		bb.SetReadOnlyBytes(buf, sizeofBuf);

		Clear();

		bool success = false;

		rtry
		{
			// Read the import version and validate it
			rverify(bb.ReadUns(m_ImportVersion, sizeof(m_ImportVersion) << 3), catchall, );
			rverify(IsValidImportVersion(m_ImportVersion), catchall, );

			// Additional import versions would be handled below.
			if (m_ImportVersion == VERSION_1)
			{
				int conflictState = 0;
				rverify(bb.ReadInt(conflictState, sizeof(conflictState) << 3), catchall, );
				m_ConflictState = (ICloudSaveFileV2::ConflictState)conflictState;

				rverify(bb.ReadBool(m_bIsDirty), catchall, );
				rverify(bb.ReadBool(m_bMarkedForDeletion), catchall, );
				rverify(bb.SerUser(m_SaveFile), catchall, );

				char ipBuf[netIpAddress::MAX_STRING_BUF_SIZE];
				const netIpAddress& address = m_SaveFile.GetLastIpAddress();
				if (address.Format(ipBuf) != NULL)
				{
					safecpy(m_IpBuf, ipBuf);
				}

				success = true;
			}
			else
			{
				success = false;
			}
		}
		rcatchall
		{
			Clear();
		}

		if(size)
		{
			*size = success ? bb.GetNumBytesRead() : 0;
		}

		return success;
	}

	void RgscCloudSaveFile::Clear()
	{
		m_IpBuf[0] = '\0';
		m_SaveFile.Clear();
		m_bIsDirty = false;
		m_bMarkedForDeletion = false;
		m_bShouldCopy = false;
		m_bHasSearchedBackups = false;
		m_BackupSlots = 0;
	}

	void RgscCloudSaveFile::SetFileId(u64 fileId)
	{
		m_SaveFile.SetFileId(fileId);
	}

	u64 RgscCloudSaveFile::GetFileId() const
	{
		return m_SaveFile.GetFileId();
	}

	void RgscCloudSaveFile::SetMd5Hash(const char* md5Hash)
	{
		m_SaveFile.SetMd5Hash(md5Hash);
	}

	const char* RgscCloudSaveFile::GetMd5Hash() const
	{
		return m_SaveFile.GetMd5Hash();
	}

	void RgscCloudSaveFile::SetFileName(const char* fileName)
	{
		m_SaveFile.SetFileName(fileName);
	}

	const char* RgscCloudSaveFile::GetFileName() const
	{
		return m_SaveFile.GetFileName();
	}

	void RgscCloudSaveFile::SetFileVersion(u64 fileVersion)
	{
		m_SaveFile.SetFileVersion(fileVersion);
	}

	u64 RgscCloudSaveFile::GetFileVersion() const
	{
		return m_SaveFile.GetFileVersion();
	}

	void RgscCloudSaveFile::SetClientLastModifiedDate(u64 clientLastModifiedDate)
	{
		m_SaveFile.SetClientLastModifiedDate(clientLastModifiedDate);
	}

	u64 RgscCloudSaveFile::GetClientLastModifiedDate() const
	{
		return m_SaveFile.GetClientLastModifiedDate();
	}

	void RgscCloudSaveFile::SetServerLastModifiedDate(u64 serverLastModifiedDate)
	{
		m_SaveFile.SetServerLastModifiedDate(serverLastModifiedDate);
	}

	u64 RgscCloudSaveFile::GetServerLastModifiedDate() const
	{
		return m_SaveFile.GetServerLastModifiedDate();
	}

	void RgscCloudSaveFile::SetHardwareId(const char* hardwareId)
	{
		m_SaveFile.SetHardwareId(hardwareId);
	}

	const char* RgscCloudSaveFile::GetHardwareId() const
	{
		return m_SaveFile.GetHardwareId();
	}

	void RgscCloudSaveFile::SetLastIpAddress(const char* ipAddressString)
	{
		rage::netIpAddress address;
		if (rlVerify(address.FromString(ipAddressString)))
		{
			m_SaveFile.SetLastIpAddress(address);
			safecpy(m_IpBuf, ipAddressString);
		}
	}

	const char* RgscCloudSaveFile::GetLastIpAddress() const
	{
		return m_IpBuf;
	}

	void RgscCloudSaveFile::CopyData(u8* data, unsigned dataLen)
	{
		m_SaveFile.CopyData(data, dataLen);
	}

	u8* RgscCloudSaveFile::GetData() const
	{
		return m_SaveFile.GetData();
	}

	u32 RgscCloudSaveFile::GetDataLength() const
	{
		return m_SaveFile.GetDataLength();
	}

	const char* RgscCloudSaveFile::GetMetaData()
	{
		return m_SaveFile.GetMetaData();
	}

	void RgscCloudSaveFile::SetMetaData(const char* metaData)
	{
		m_SaveFile.SetMetaData(metaData);
	}

	ICloudSaveFileV2::ConflictState RgscCloudSaveFile::GetConflictState()
	{
		return m_ConflictState;
	}

	void RgscCloudSaveFile::SetConflictState(ICloudSaveFileV2::ConflictState state)
	{
		m_ConflictState = state;
	}

	bool RgscCloudSaveFile::IsDirty()
	{
		return m_bIsDirty;
	}

	void RgscCloudSaveFile::SetIsDirty(bool bDirty)
	{
		m_bIsDirty = bDirty;
	}

	ICloudSaveFileV2::ConflictResolutionType RgscCloudSaveFile::GetResolveType()
	{
		return m_ResolveType;
	}

	void RgscCloudSaveFile::SetResolveType(ICloudSaveFileV2::ConflictResolutionType resolveType)
	{
		if (m_ConflictState == ICloudSaveFileV2::ConflictState::CS_None || m_ConflictState == ICloudSaveFileV2::ConflictState::CS_FileMissing || 
			m_ConflictState == ICloudSaveFileV2::ConflictState::CS_ClientHasNewer || m_ConflictState == ICloudSaveFileV2::ConflictState::CS_FileDeleted)
		{
			rlAssertf(resolveType == ICloudSaveFileV2::ConflictResolutionType::CRT_None, "Attempting to set Local/Remote resolve type for a non-conflicted file.");
		}

		m_ResolveType = resolveType;
	}

	void RgscCloudSaveFile::FreeSaveData()
	{
		m_SaveFile.FreeSaveData();
	}

	void RgscCloudSaveFile::SetShouldCopyFile(bool bShouldCopy)
	{
		m_bShouldCopy = bShouldCopy;
	}

	bool RgscCloudSaveFile::ShouldCopyFile()
	{
		return m_bShouldCopy;
	}

	void RgscCloudSaveFile::SetNextExpectedVersion(u64 fileVersion)
	{
		m_SaveFile.SetNextExpectedVersion(fileVersion);
	}

	u64 RgscCloudSaveFile::GetNextExpectedVersion() const
	{
		return m_SaveFile.GetNextExpectedVersion();
	}

	void RgscCloudSaveFile::SetServerMetadata(const char* metadata)
	{
		m_SaveFile.SetServerMetadata(metadata);
	}

	const char* RgscCloudSaveFile::GetServerMetadata() const 
	{
		return m_SaveFile.GetServerMetadata();
	}

	void RgscCloudSaveFile::SetClientFileSize(u64 fileSize)
	{
		m_SaveFile.SetClientFileSize(fileSize);
	}

	u64 RgscCloudSaveFile::GetClientFileSize() const
	{
		return m_SaveFile.GetClientFileSize();
	}

	void RgscCloudSaveFile::SetServerFileSize(u64 fileSize)
	{
		m_SaveFile.SetServerFileSize(fileSize);
	}

	u64 RgscCloudSaveFile::GetServerFileSize() const
	{
		return m_SaveFile.GetServerFileSize();
	}

	void RgscCloudSaveFile::SetLatestServerVersion(u64 fileVersion)
	{
		m_SaveFile.SetLatestServerVersion(fileVersion);
	}

	u64 RgscCloudSaveFile::GetLatestServerVersion() const
	{
		return m_SaveFile.GetLatestServerVersion();
	}

	void RgscCloudSaveFile::SetMarkedForDeletion(bool bMarkedForDeletion)
	{
		m_bMarkedForDeletion = bMarkedForDeletion;
	}

	bool RgscCloudSaveFile::IsMarkedForDeletion() const
	{
		return m_bMarkedForDeletion;
	}

	// ===============================================================================================
	// inherited from ICloudSaveFileV3
	// ===============================================================================================
	bool RgscCloudSaveFile::HasSearchedBackups()
	{
		return m_bHasSearchedBackups;
	}

	void RgscCloudSaveFile::SetHasSearchedBackups(bool bSearched)
	{
		m_bHasSearchedBackups = bSearched;
	}

	s64 RgscCloudSaveFile::GetBackupSlots()
	{
		return m_BackupSlots;
	}

	void RgscCloudSaveFile::SetBackupSlots(s64 backupSlots)
	{
		m_BackupSlots = backupSlots;
	}

	// ===============================================================================================
	// RgscCloudSaveHttpDebugInfo
	// ===============================================================================================
	RgscCloudSaveHttpDebugInfo::RgscCloudSaveHttpDebugInfo()
	{
		Reset();
	}

	RGSC_HRESULT RgscCloudSaveHttpDebugInfo::QueryInterface(RGSC_REFIID riid, void** ppvObj)
	{
		IRgscUnknown *pUnknown = NULL;

		if(ppvObj == NULL)
		{
			return RGSC_INVALIDARG;
		}

		if(riid == IID_IRgscUnknown)
		{
			pUnknown = static_cast<ICloudSaveHttpDebugInfo*>(this);
		}
		else if(riid == IID_ICloudSaveHttpDebugInfoV1)
		{
			pUnknown = static_cast<ICloudSaveHttpDebugInfoV1*>(this);
		}
		else if(riid == IID_ICloudSaveHttpDebugInfoV2)
		{
			pUnknown = static_cast<ICloudSaveHttpDebugInfoV2*>(this);
		}

		*ppvObj = pUnknown;
		if(pUnknown == NULL)
		{
			return RGSC_NOINTERFACE;
		}

		return RGSC_OK;
	}

	void RgscCloudSaveHttpDebugInfo::Reset()
	{
		m_DebugInfo.Clear();
	}

	int RgscCloudSaveHttpDebugInfo::GetSendState()
	{
		return m_DebugInfo.m_SendState;
	}

	void RgscCloudSaveHttpDebugInfo::SetSendState(int sendState)
	{
		m_DebugInfo.m_SendState = sendState;
	}

	int RgscCloudSaveHttpDebugInfo::GetRecvState()
	{
		return m_DebugInfo.m_RecvState;
	}

	void RgscCloudSaveHttpDebugInfo::SetRecvState(int recvState)
	{
		m_DebugInfo.m_RecvState = recvState;
	}

	unsigned RgscCloudSaveHttpDebugInfo::GetInContentBytesRcvd()
	{
		return m_DebugInfo.m_InContentBytesRcvd;
	}

	void RgscCloudSaveHttpDebugInfo::SetInContentBytesRcvd(int bytes)
	{
		m_DebugInfo.m_InContentBytesRcvd = bytes;
	}

	unsigned RgscCloudSaveHttpDebugInfo::GetOutContentBytesSent()
	{
		return m_DebugInfo.m_OutChunkBytesSent;
	}

	void RgscCloudSaveHttpDebugInfo::SetOutContentBytesSent(int bytes)
	{
		m_DebugInfo.m_OutChunkBytesSent = bytes;
	}

	bool RgscCloudSaveHttpDebugInfo::IsCommitted()
	{
		return m_DebugInfo.m_Committed;
	}

	void RgscCloudSaveHttpDebugInfo::SetCommitted(bool committed)
	{
		m_DebugInfo.m_Committed = committed;
	}

	int RgscCloudSaveHttpDebugInfo::GetHttpStatusCode()
	{
		return m_DebugInfo.m_HttpStatusCode;
	}

	void RgscCloudSaveHttpDebugInfo::SetHttpStatusCode(int code)
	{
		m_DebugInfo.m_HttpStatusCode = code;
	}

	int RgscCloudSaveHttpDebugInfo::GetAbortReason()
	{
		return m_DebugInfo.m_AbortReason;
	}

	void RgscCloudSaveHttpDebugInfo::SetAbortReason(int abortReason)
	{
		m_DebugInfo.m_AbortReason = (netHttpAbortReason)abortReason;
	}

	bool RgscCloudSaveHttpDebugInfo::HadMemoryAllocationError()
	{
		return m_DebugInfo.m_ChunkAllocationError;
	}

	void RgscCloudSaveHttpDebugInfo::SetHadMemoryAllocationError(bool bHadMemoryError)
	{
		m_DebugInfo.m_ChunkAllocationError = bHadMemoryError;
	}

	int RgscCloudSaveHttpDebugInfo::GetTcpResultState()
	{
		return (int)m_DebugInfo.m_TcpResult;
	}

	void RgscCloudSaveHttpDebugInfo::SetTcpResultState(int resultState)
	{
		m_DebugInfo.m_TcpResult = (netTcpResult)resultState;
	}

	int RgscCloudSaveHttpDebugInfo::GetTcpError()
	{
		return m_DebugInfo.m_TcpError;
	}

	void RgscCloudSaveHttpDebugInfo::SetTcpError(int error)
	{
		m_DebugInfo.m_TcpError = error;
	}

	// ===============================================================================================
	// RgscCloudSaveBackupInfo
	// ===============================================================================================
	RgscCloudSaveBackupInfo::RgscCloudSaveBackupInfo()
	{
		Clear();
	}

	RgscCloudSaveBackupInfo::~RgscCloudSaveBackupInfo()
	{

	}

	RGSC_HRESULT RgscCloudSaveBackupInfo::QueryInterface(RGSC_REFIID riid, void** ppvObj)
	{
		IRgscUnknown *pUnknown = NULL;

		if(ppvObj == NULL)
		{
			return RGSC_INVALIDARG;
		}

		if(riid == IID_IRgscUnknown)
		{
			pUnknown = static_cast<ICloudSaveBackupInfo*>(this);
		}
		else if(riid == IID_ICloudSaveBackupInfoV1)
		{
			pUnknown = static_cast<ICloudSaveBackupInfoV1*>(this);
		}

		*ppvObj = pUnknown;
		if(pUnknown == NULL)
		{
			return RGSC_NOINTERFACE;
		}

		return RGSC_OK;
	}


	void RgscCloudSaveBackupInfo::Clear()
	{
		m_ImportVersion = 0;
		m_ClientLastModifiedDate = 0;
		m_Md5Hash[0] = '\0';
		m_MetaData[0] = '\0';
		m_FileName[0] = '\0';
		m_HardwareId[0] = '\0';
	}

	void RgscCloudSaveBackupInfo::SetMd5Hash(const char* md5Hash)
	{
		safecpy(m_Md5Hash, md5Hash);
	}

	const char* RgscCloudSaveBackupInfo::GetMd5Hash() const
	{
		return m_Md5Hash;
	}

	void RgscCloudSaveBackupInfo::SetMetaData(const char* metaData)
	{
		safecpy(m_MetaData, metaData);
	}

	const char* RgscCloudSaveBackupInfo::GetMetaData()
	{
		return m_MetaData;
	}

	void RgscCloudSaveBackupInfo::SetFileName(const char* fileName)
	{
		safecpy(m_FileName, fileName);
	}

	const char* RgscCloudSaveBackupInfo::GetFileName() const
	{
		return m_FileName;
	}

	void RgscCloudSaveBackupInfo::SetClientLastModifiedDate(u64 clientLastModifiedDate)
	{
		m_ClientLastModifiedDate = clientLastModifiedDate;
	}

	u64 RgscCloudSaveBackupInfo::GetClientLastModifiedDate() const
	{
		return m_ClientLastModifiedDate;
	}

	void RgscCloudSaveBackupInfo::SetHardwareId(const char* hardwareId)
	{
		safecpy(m_HardwareId, hardwareId);
	}

	const char* RgscCloudSaveBackupInfo::GetHardwareId() const
	{
		return m_HardwareId;
	}

	bool RgscCloudSaveBackupInfo::Export(void* buf, const unsigned sizeofBuf, unsigned* size)
	{
		datExportBuffer bb;
		bb.SetReadWriteBytes(buf, sizeofBuf);

		sysMemSet(buf, 0, sizeofBuf);

		bool success = false;

		rtry
		{
			// Export as the latest version: version 1
			unsigned exportVersion = VERSION_LATEST;
			rverify(bb.WriteUns(exportVersion, sizeof(exportVersion) << 3), catchall, );

			rverify(bb.WriteUns(m_ClientLastModifiedDate, sizeof(m_ClientLastModifiedDate) << 3), catchall, );
			rverify(bb.WriteBytes(m_Md5Hash, sizeof(m_Md5Hash)), catchall, );
			rverify(bb.WriteBytes(m_MetaData, sizeof(m_MetaData)), catchall, );
			rverify(bb.WriteBytes(m_FileName, sizeof(m_FileName)), catchall, );
			rverify(bb.WriteBytes(m_HardwareId, sizeof(m_HardwareId)), catchall, );

			success = true;
		}
		rcatchall
		{

		}

		Assert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

		if(size)
		{
			*size = success ? bb.GetNumBytesWritten() : 0;
		}

		return success;
	}

	bool RgscCloudSaveBackupInfo::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
	{
		datImportBuffer bb;
		bb.SetReadOnlyBytes(buf, sizeofBuf);

		Clear();

		bool success = false;

		rtry
		{
			// Read the import version and validate it
			rverify(bb.ReadUns(m_ImportVersion, sizeof(m_ImportVersion) << 3), catchall, );
			rverify(IsValidImportVersion(m_ImportVersion), catchall, );

			// Additional import versions would be handled below.
			if (m_ImportVersion == VERSION_1)
			{
				rverify(bb.ReadUns(m_ClientLastModifiedDate, sizeof(m_ClientLastModifiedDate) << 3), catchall, );
				rverify(bb.ReadBytes(m_Md5Hash, sizeof(m_Md5Hash)), catchall, );
				rverify(bb.ReadBytes(m_MetaData, sizeof(m_MetaData)), catchall, );
				rverify(bb.ReadBytes(m_FileName, sizeof(m_FileName)), catchall, );
				rverify(bb.ReadBytes(m_HardwareId, sizeof(m_HardwareId)), catchall, );

				success = true;
			}
			else
			{
				success = false;
			}
		}
		rcatchall
		{
			Clear();
		}

		if(size)
		{
			*size = success ? bb.GetNumBytesRead() : 0;
		}

		return success;
	}

	// ===============================================================================================
	// RgscCloudSaveOperationProgress
	// ===============================================================================================
	RgscCloudSaveOperationProgress::RgscCloudSaveOperationProgress()
		: m_bIsActive(false)
		, m_StepCount(0)
		, m_CompletionCount(0)
		, m_TotalLength(0)
		, m_TotalProgress(0)
		, m_StepLength(0)
		, m_StepProgress(0)
	{

	}

	RGSC_HRESULT RgscCloudSaveOperationProgress::QueryInterface(RGSC_REFIID riid, void** ppvObj)
	{
		IRgscUnknown *pUnknown = NULL;

		if(ppvObj == NULL)
		{
			return RGSC_INVALIDARG;
		}

		if(riid == IID_IRgscUnknown)
		{
			pUnknown = static_cast<ICloudSaveOperationProgress*>(this);
		}
		else if(riid == IID_ICloudSaveOperationProgressV1)
		{
			pUnknown = static_cast<ICloudSaveOperationProgressV1*>(this);
		}
		else if (riid == IID_ICloudSaveOperationProgressV2)
		{
			pUnknown = static_cast<ICloudSaveOperationProgressV2*>(this);
		}

		*ppvObj = pUnknown;
		if(pUnknown == NULL)
		{
			return RGSC_NOINTERFACE;
		}

		return RGSC_OK;
	}

	void RgscCloudSaveOperationProgress::Reset()
	{
		m_bIsActive = false;
		m_StepCount = 0;
		m_CompletionCount = 0;
		m_TotalLength = 0;
		m_TotalProgress = 0;
		m_StepLength = 0;
		m_StepProgress = 0;
	}

	void RgscCloudSaveOperationProgress::SetIsActive(bool bActive)
	{
		m_bIsActive = bActive;
	}

	bool RgscCloudSaveOperationProgress::IsActive()
	{
		return m_bIsActive;
	}

	void RgscCloudSaveOperationProgress::SetStepCount(u32 stepCount)
	{
		m_StepCount = stepCount;
	}

	u32 RgscCloudSaveOperationProgress::GetStepCount()
	{
		return m_StepCount;
	}

	void RgscCloudSaveOperationProgress::SetCompletedCount(u32 completedCount)
	{
		m_CompletionCount = completedCount;
	}

	u32 RgscCloudSaveOperationProgress::GetCompletedCount()
	{
		return m_CompletionCount;
	}

	u64 RgscCloudSaveOperationProgress::GetTotalLength()
	{
		return m_TotalLength;
	}

	void RgscCloudSaveOperationProgress::SetTotalLength(u64 length)
	{
		m_TotalLength = length;
	}

	u64 RgscCloudSaveOperationProgress::GetTotalProgress()
	{
		return m_TotalProgress;
	}

	void RgscCloudSaveOperationProgress::SetTotalProgress(u64 progress)
	{
		m_TotalProgress = progress;
	}

	u64 RgscCloudSaveOperationProgress::GetStepLength()
	{
		return m_StepLength;
	}

	void RgscCloudSaveOperationProgress::SetStepLength(u64 length)
	{
		m_StepLength = length;
	}

	u64 RgscCloudSaveOperationProgress::GetStepProgress()
	{
		return m_StepProgress;
	}

	void RgscCloudSaveOperationProgress::SetStepProgress(u64 progress)
	{
		m_StepProgress = progress;
	}

	// ===============================================================================================
	// RgscCloudSaveManager
	// ===============================================================================================
	RGSC_HRESULT RgscCloudSaveManager::QueryInterface(RGSC_REFIID riid, void** ppvObj)
	{
		IRgscUnknown *pUnknown = NULL;

		if(ppvObj == NULL)
		{
			return RGSC_INVALIDARG;
		}

		if(riid == IID_IRgscUnknown)
		{
			pUnknown = static_cast<ICloudSaveManager*>(this);
		}
		else if(riid == IID_ICloudSaveManagerV1)
		{
			pUnknown = static_cast<ICloudSaveManagerV1*>(this);
		}
		else if(riid == IID_ICloudSaveManagerV2)
		{
			pUnknown = static_cast<ICloudSaveManagerV2*>(this);
		}
		else if(riid == IID_ICloudSaveManagerV3)
		{
			pUnknown = static_cast<ICloudSaveManagerV3*>(this);
		}
		else if(riid == IID_ICloudSaveManagerV4)
		{
			pUnknown = static_cast<ICloudSaveManagerV4*>(this);
		}

		*ppvObj = pUnknown;
		if(pUnknown == NULL)
		{
			return RGSC_NOINTERFACE;
		}

		return RGSC_OK;
	}

	RgscCloudSaveManager::RgscCloudSaveManager()
		: m_ManifestIOBufferLength(0)
		, m_ManifestIOBuffer(NULL)
		, m_iNumBackupSlots(RGSC_CLOUDSAVE_DEFAULT_NUM_BACKUP_FILES)
		, m_NumAllocatedBackupInfo(0)
	{
	}

	RgscCloudSaveManager::~RgscCloudSaveManager()
	{
		FreeManifestIOBuffer();
	}

	bool RgscCloudSaveManager::Init()
	{
		// Create an input/output buffer for manifests that is the size of its max exported byte size.
		//	This prevents us from potentially running out of rline memory or fragmenting as manifests
		//	are saved/load with each registered file.
		m_ManifestIOBufferLength = RgscCloudSaveManifest::MAX_EXPORTED_SIZE_IN_BYTES;
		m_ManifestIOBuffer = rage_new u8[m_ManifestIOBufferLength];

		return m_ManifestIOBuffer != NULL;
	}

	bool RgscCloudSaveManager::Shutdown()
	{
		FreeManifestIOBuffer();
		return true;
	}

	void RgscCloudSaveManager::FreeManifestIOBuffer()
	{
		if (m_ManifestIOBuffer)
		{
			delete[] m_ManifestIOBuffer;
			m_ManifestIOBuffer = NULL;
			m_ManifestIOBufferLength = 0;
		}
	}

	bool RgscCloudSaveManager::IsTitleRegistered(const char* titleName)
	{
		for (int i = 0; i < m_TitleManifests.GetCount(); i++)
		{
			if (stricmp(m_TitleManifests[i]->GetTitleName(), titleName) == 0)
			{
				return true;
			}
		}

		return false;
	}

	bool RgscCloudSaveManager::GetLocalSaveLocation(ICloudSaveManifestV2* manifestV2, char (&path)[RGSC_MAX_PATH])
	{
		rtry
		{
			path[0] = '\0';

			rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal(), catchall, );

			ICloudSaveTitleConfiguration* configuration = manifestV2->GetConfiguration();
			rverify(configuration, catchall, );

			ICloudSaveTitleConfigurationV1* configV1 = NULL;
			RGSC_HRESULT hr = configuration->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
			rverify(SUCCEEDED(hr) && configV1, catchall, );

			rverify(configV1->GetSaveDirectory(), catchall, );

			// Copy the save directory to the path
			safecpy(path, configV1->GetSaveDirectory());

			rverify(path[0] != '\0', catchall, );

			// Ensure it ends with a trailing slash
			if (!StringEndsWithI(path, "\\"))
			{
				safecat(path, "\\");
			}

			return true;
		}
		rcatchall
		{
			path[0] = '\0';
			return false;
		}
	}

	bool RgscCloudSaveManager::GetManifestPath(ICloudSaveManifestV2* manifestV2, char (&path)[RGSC_MAX_PATH])
	{
		rtry
		{
			rverify(GetLocalSaveLocation(manifestV2, path), catchall,);
			safecat(path, RGSC_CLOUDSAVE_MANIFEST);
			return true;
		}
		rcatchall
		{
			path[0] = '\0';
			return false;
		}
	}

	bool RgscCloudSaveManager::SaveManifest(ICloudSaveManifest* manifest)
	{
		// Save/Load share the same IO buffer, so lock to prevent issue.
		SYS_CS_SYNC(m_ManifestIOCs);

		bool success = false;

		rtry
		{
			// verify our cloud save buffer is initialized
			rverify(m_ManifestIOBuffer, catchall, );

			ICloudSaveManifestV2* manifestV2 = NULL;
			RGSC_HRESULT hr = manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&manifestV2);
			rverify(SUCCEEDED(hr) && manifestV2, catchall, );

			char path[RGSC_MAX_PATH];
			rverify(GetManifestPath(manifestV2, path), catchall, );

			const fiDevice* device = fiDevice::GetDevice(path, true);
			rverify(device, catchall, );

			// double check our IO buffer is large enough
			rverify(manifestV2->GetMaxExportBufSize() <= m_ManifestIOBufferLength, catchall, );

			unsigned bytesExported;
			rverify(manifestV2->Export(m_ManifestIOBuffer, m_ManifestIOBufferLength, &bytesExported), catchall, );

			// Clear readonly flag
			ClearReadOnlyAttribute(device, path);

			// Create missing folders
			ASSET.CreateLeadingPath(path);

			// Open the file
			fiHandle hFile = device->Create(path);
			rverify(fiIsValidHandle(hFile), catchall, );

			int bytesWritten = device->Write(hFile, m_ManifestIOBuffer, bytesExported);
			device->Close(hFile);

			rverify(bytesWritten == (int)bytesExported, catchall, );
			success = true;
		}
		rcatchall
		{
			success = false;
		}

		return success;
	}

	bool RgscCloudSaveManager::LoadManifest(ICloudSaveManifest* manifest)
	{
		// Save/Load share the same IO buffer, so lock to prevent issue.
		SYS_CS_SYNC(m_ManifestIOCs);

		bool success = false;

		rtry
		{
			// verify our cloud save buffer is initialized
			rverify(m_ManifestIOBuffer, catchall, );

			ICloudSaveManifestV2* manifestV2 = NULL;
			RGSC_HRESULT hr = manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&manifestV2);
			rverify(SUCCEEDED(hr) && manifestV2, catchall, );

			char path[RGSC_MAX_PATH];
			rverify(GetManifestPath(manifestV2, path), catchall, );

			const fiDevice* device = fiDevice::GetDevice(path, true);
			rverify(device, catchall, );

			u64 file_size_u64 = device->GetFileSize(path);

			// Sanity check. lets make sure we're within reason, Win32 compile compatibility below
			rverify(file_size_u64 < (u32)-1, catchall, );

			// convert to u32
			u32 fileSize = (u32)file_size_u64;
			if (fileSize == 0)
			{
				// We don't have a local manifest on disk - The client may already have unsaved state, so only update
				//	if the state is set to INVALID.
				if (manifestV2->GetCloudSaveEnabled() == ICloudSaveManifestV2::CloudSavesEnabledState::INVALID)
				{
					rlDebug1("No local manifest on disk and invalid enabled state: updating to UNKNOWN.");
					manifestV2->SetCloudSavesEnabled(ICloudSaveManifestV2::CloudSavesEnabledState::UNKNOWN);
				}
				
				return true;
			}
			
			// verify the file size is reasonable within our IO buffer
			rverify(fileSize <= m_ManifestIOBufferLength, catchall, );

			// Open the file
			fiHandle hFile = device->Open(path, true);
			rverify(fiIsValidHandle(hFile), catchall, );

			// read the manifest and import
			int bytesRead = device->Read(hFile, m_ManifestIOBuffer, fileSize);
			device->Close(hFile);

			// clear the manifest before importing
			manifestV2->Clear();

			unsigned importSize;
			success = manifestV2->Import(m_ManifestIOBuffer, bytesRead, &importSize);

			return success;
		}
		rcatchall
		{
			success = false;
		}

		return success;
	}

	static json_char* json_as_string_or_null(json_const JSONNODE* node)
	{
		char* str = json_as_string(node);
		return (_stricmp("null", str) != 0) ? str : NULL;
	}

	void RgscCloudSaveManager::SetCloudSaveEnabledFromJson(const char* jsonRequest)
	{
		char* rosTitleName = NULL;
		
		bool isValidEnabledState = false;
		bool isEnabled = false;

		// PARSE REQUEST

		JSONNODE *n = json_parse(jsonRequest);

		if(n == NULL)
		{
			Errorf("Invalid JSON Node\n");
			return;
		}

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(*i == NULL)
			{
				Errorf("Invalid JSON Node\n");
				return;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "RosTitleName") == 0)
			{
				rosTitleName = json_as_string_or_null(*i);
			}
			else if(_stricmp(node_name, "Enabled") == 0)
			{
				isEnabled = json_as_bool(*i) != 0;
				isValidEnabledState = true;
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}


		// Ensure the JSON had the data we need.
		if (isValidEnabledState && rosTitleName)
		{
			for (int i = 0; i < m_TitleManifests.GetCount(); i++)
			{
				if (stricmp(m_TitleManifests[i]->GetTitleName(), rosTitleName) == 0)
				{
					if (isEnabled)
					{
						m_TitleManifests[i]->SetCloudSavesEnabled(ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES);
					}
					else
					{
						m_TitleManifests[i]->SetCloudSavesEnabled(ICloudSaveManifestV2::CloudSavesEnabledState::DISABLE_CLOUD_SAVES);
					}

					GetRgscConcreteInstance()->HandleNotification( rgsc::NOTIFY_CLOUD_SAVE_ENABLED_UPDATED, rosTitleName);

					break;
				}
			}
		}
		
		// free from input
		json_delete(n);
		json_free_safe(rosTitleName);
	}

	const char* RgscCloudSaveManager::GetCloudSaveEnabledAsJson(const char* jsonRequest)
	{	
		char* rosTitleName = NULL;

		// PARSE REQUEST

		JSONNODE *n = json_parse(jsonRequest);

		if(n == NULL)
		{
			Errorf("Invalid JSON Node\n");
			return false;
		}

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(*i == NULL)
			{
				Errorf("Invalid JSON Node\n");
				return false;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "RosTitleName") == 0)
			{
				rosTitleName = json_as_string_or_null(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}


		// RESULT

		JSONNODE* resultJson = json_new(JSON_NODE);
		json_set_name(resultJson, "Cloud Save Enabled");

		json_push_back(resultJson, json_new_a("RosTitleName", rosTitleName));

		ICloudSaveManifestV2::CloudSavesEnabledState csEnabledState = ICloudSaveManifestV2::CloudSavesEnabledState::UNKNOWN;
		if (rosTitleName)
		{
			for (int i = 0; i < m_TitleManifests.GetCount(); i++)
			{
				if (stricmp(m_TitleManifests[i]->GetTitleName(), rosTitleName) == 0)
				{
					// Manifest has not been loaded yet from disk - load it in place.
					if (m_TitleManifests[i]->GetCloudSaveEnabled() == ICloudSaveManifestV2::CloudSavesEnabledState::INVALID)
					{
						rlDebug1("GetCloudSaveEnabledAsJson - Manifest has not yet been loaded from disk.");
						m_TitleManifests[i]->Load();
					}

					csEnabledState = m_TitleManifests[i]->GetCloudSaveEnabled();
					break;
				}
			}
		}

		switch(csEnabledState)
		{
		case ICloudSaveManifestV2::CloudSavesEnabledState::INVALID:
		case ICloudSaveManifestV2::CloudSavesEnabledState::UNKNOWN:
			json_push_back(resultJson, json_new_a("Enabled", "unknown"));
			break;
		case ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES:
			json_push_back(resultJson, json_new_a("Enabled", "enabled"));
			break;
		case ICloudSaveManifestV2::CloudSavesEnabledState::DISABLE_CLOUD_SAVES:
			json_push_back(resultJson, json_new_a("Enabled", "disabled"));
			break;
		}

		json_char *jc = json_write_formatted(resultJson);

		rlDebug2("%s\n", jc);

		// free from input
		json_delete(n);
		json_free_safe(rosTitleName);

		// free from result
		json_delete(resultJson);

		return jc;
	}

	bool RgscCloudSaveManager::RegisterFileForUploadAsync(ICloudSaveManifest* manifest, const char* fileName, const char* metaData, IAsyncStatus* status)
	{
		RLPC_CREATETASK_IASYNC(RegisterFileTask, manifest, fileName, metaData, false);
	}

	// ===============================================================================================
	// inherited from ICloudSaveV1
	// ===============================================================================================
	void RgscCloudSaveManager::SetMaximumFileSize(u64 maximumFileSize)
	{
		rlCloudSave::SetMaximumFileSize(maximumFileSize);
	}

	ICloudSaveManifest* RgscCloudSaveManager::CreateManifest(u32 numSlots)
	{
		rtry
		{
			rverify(numSlots <= MAX_CLOUD_SAVE_SLOTS, catchall, );
			return rage_new RgscCloudSaveManifest(numSlots);
		}
		rcatchall
		{
			return NULL;
		}
	}

	bool RgscCloudSaveManager::GetCloudSaveManifest(ICloudSaveManifest* manifest, IAsyncStatus* status)
	{
		RLPC_CREATETASK_IASYNC(GetCloudSaveManifestTask, RL_RGSC_GAMER_INDEX, manifest);
	}

	bool RgscCloudSaveManager::GetFile(ICloudSaveFile* saveFile, ResolveType resolveType, const char* hardwareId, IAsyncStatus* status)
	{
		RLPC_CREATETASK_IASYNC(GetCloudSaveFileTask, RL_RGSC_GAMER_INDEX, saveFile, resolveType, hardwareId);
	}

	bool RgscCloudSaveManager::PostFile(ICloudSaveFile* saveFile, ResolveType resolveType, IAsyncStatus* status)
	{
		RLPC_CREATETASK_IASYNC(PostCloudSaveFileTask, RL_RGSC_GAMER_INDEX, saveFile, resolveType);
	}

	// ===============================================================================================
	// inherited from ICloudSaveV2
	// ===============================================================================================
	ICloudSaveManifest* RgscCloudSaveManager::RegisterTitle(ICloudSaveTitleConfiguration* configuration)
	{
		RgscCloudSaveManifest* manifest = NULL;

		rtry
		{
			rverify(configuration, catchall, );

			ICloudSaveTitleConfigurationV1* configV1 = NULL;
			RGSC_HRESULT hr = configuration->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
			rverify(SUCCEEDED(hr), catchall, );

			// verify that the title has not already been registered
			rverify(!IsTitleRegistered(configV1->GetTitleName()), catchall, );

			// allocate a new manifest for the configuration
			manifest = rage_new RgscCloudSaveManifest();
			rverify(manifest, catchall, );
			rverify(manifest->SetConfiguration(configuration), catchall, );
			rlDebug1("Title '%s' successfully registered for cloud saves.", configV1->GetTitleName());

			m_TitleManifests.PushAndGrow(manifest, 4);

			return manifest;
		}
		rcatchall
		{
			if (manifest)
			{
				delete manifest;
				manifest = NULL;
			}

			return false;
		}
	}

	void RgscCloudSaveManager::UnregisterTitle(ICloudSaveManifest* manifest)
	{
		rtry
		{
			rverify(manifest, catchall, );

			ICloudSaveManifestV2* manifestV2 = NULL;
			rverify(SUCCEEDED(manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&manifestV2)) && manifestV2, catchall, );

			// Search the manifest in the list
			for (int i = 0; m_TitleManifests.GetCount(); i++)
			{
				if (stricmp(m_TitleManifests[i]->GetTitleName(), manifestV2->GetTitleName()) == 0)
				{
					m_TitleManifests.Delete(i);
					break;
				}
			}

			rlDebug1("Title '%s' successfully unregistered.", manifestV2->GetTitleName());

			// delete the manifest
			delete manifest;
		}
		rcatchall
		{

		}
	}
	
	bool RgscCloudSaveManager::IdentifyConflicts(ICloudSaveManifest* manifest, IAsyncStatus* status)
	{
		RLPC_CREATETASK_IASYNC(IdentifyConflictsTask, manifest);
	}

	bool RgscCloudSaveManager::PostUpdatedFiles(ICloudSaveManifest* manifest, IAsyncStatus* status)
	{
		RLPC_CREATETASK_IASYNC(PostUpdatedFilesTask, manifest);
	}

	bool RgscCloudSaveManager::GetUpdatedFiles(ICloudSaveManifest* manifest, IAsyncStatus* status)
	{
		RLPC_CREATETASK_IASYNC(GetUpdatedFilesTask, manifest);
	}

	bool RgscCloudSaveManager::CopyUpdatedFiles(ICloudSaveManifest* manifest, IAsyncStatus* status)
	{
		RLPC_CREATETASK_IASYNC(CopyUpdatedFilesTask, manifest);
	}

	bool RgscCloudSaveManager::RegisterFileForUpload(ICloudSaveManifest* manifest, const char* fileName, const char* metaData)
	{
		return SUCCEEDED(CreateRegisterFileTask(manifest, fileName, metaData));
	}

	bool RgscCloudSaveManager::UnregisterFile(ICloudSaveManifest* manifest, const char* fileName)
	{
		return SUCCEEDED(CreateUnregisterFileTask(manifest, fileName));
	}

	bool RgscCloudSaveManager::DeleteUpdatedfiles(ICloudSaveManifest* manifest, IAsyncStatus* status)
	{
		RLPC_CREATETASK_IASYNC(DeleteUpdatedFilesTask, manifest);
	}

	RGSC_HRESULT RgscCloudSaveManager::CreateRegisterFileTask(ICloudSaveManifest* manifest, const char* fileName, const char* metaData)
	{
		RLPC_CREATETASK_NO_STATUS(RegisterFileTask, manifest, fileName, metaData, false);
	}

	RGSC_HRESULT RgscCloudSaveManager::CreateUnregisterFileTask(ICloudSaveManifest* manifest, const char* fileName)
	{
		RLPC_CREATETASK_NO_STATUS(RegisterFileTask, manifest, fileName, "", true);
	}

	bool RgscCloudSaveManager::HasBetaAccessAsync(const char* cohortsFilePath, bool* bOutResult, IAsyncStatus* status)
	{
		RLPC_CREATETASK_IASYNC(QueryBetaAccessTask, cohortsFilePath, bOutResult);
	}

	bool RgscCloudSaveManager::WriteConflictTelemetry(ICloudSaveManifest* manifest)
	{
		rtry
		{
			rverify(manifest, catchall, );

			ICloudSaveManifestV2* manifestV2 = NULL;
			rverify(SUCCEEDED(manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&manifestV2)) && manifestV2, catchall, );

			RgscCloudSaveConflictMetric metric(manifestV2);
			rverify(rlTelemetry::Write(RL_RGSC_GAMER_INDEX, metric), catchall, );

#if !__NO_OUTPUT
			char conflictTelemetry[RL_TELEMETRY_DEFAULT_WORK_BUFFER_SIZE] = {0};
			RsonWriter rw(conflictTelemetry, RSON_FORMAT_JSON);
			metric.Write(&rw);
			rlDebug3("WriteConflictTelemetry: %s", rw.ToString());
#endif

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool RgscCloudSaveManager::BackupConflictedFiles(ICloudSaveManifest* manifest, IAsyncStatus* status)
	{
		RLPC_CREATETASK_IASYNC(BackupConflictedFilesTask, manifest);
	}

	bool RgscCloudSaveManager::SearchForBackups(ICloudSaveManifest* manifest, IAsyncStatus* status)
	{
		RLPC_CREATETASK_IASYNC(SearchBackupFilesTask, manifest);
	}

	int RgscCloudSaveManager::GetNumBackupSlots()
	{
		// Defaults ICloudSaveManagerV3::RGSC_CLOUDSAVE_DEFAULT_NUM_BACKUP_FILES (3)
		return m_iNumBackupSlots;
	}

	void RgscCloudSaveManager::SetNumBackupSlots(int numSlots)
	{
		// Setting 0 slots doesn't make any sense -- just don't call the Backup APIs
		if (rlVerify(numSlots > 0))
		{
			m_iNumBackupSlots = numSlots;
		}
	}

	ICloudSaveBackupInfo* RgscCloudSaveManager::AllocateBackupInfo()
	{
		ICloudSaveBackupInfo* result = rage_new RgscCloudSaveBackupInfo();

		if (result)
		{
			m_NumAllocatedBackupInfo++;
		}

		return result;
	}

	void RgscCloudSaveManager::FreeBackupInfo(ICloudSaveBackupInfo* info)
	{
		if (rlVerify(info))
		{
			delete info;

			rlAssert(m_NumAllocatedBackupInfo > 0);
			m_NumAllocatedBackupInfo--;
		}
	}

	RGSC_HRESULT RgscCloudSaveManager::GetBackupInfo(ICloudSaveManifest* manifest, const int fileIndex, const int backupIndex, ICloudSaveBackupInfo* out_info)
	{
		RGSC_HRESULT hr = RGSC_OK;

		rtry
		{
			// validate the input info
			rverify(manifest, catchall, hr = RGSC_INVALIDARG);
			rverify(backupIndex < MAX_NUM_BACKUP_SLOTS, catchall, hr = RGSC_INVALIDARG);
			rverify(out_info, catchall, hr = RGSC_INVALIDARG);

			// validate a v1 backup info
			ICloudSaveBackupInfoV1* out_infoV1 = NULL;
			hr = out_info->QueryInterface(IID_ICloudSaveBackupInfoV1, (void**)&out_infoV1);
			rverify(SUCCEEDED(hr) && out_infoV1, catchall, );
			out_infoV1->Clear();

			// V2 is the earliest accepted manifest for this task
			ICloudSaveManifestV2* manifestV2 = NULL;
			hr = manifest->QueryInterface(IID_ICloudSaveManifestV2, (void**)&manifestV2);
			rverify(SUCCEEDED(hr) && manifestV2, catchall, );

			// Extract the configuration from the manifest
			ICloudSaveTitleConfiguration* config = manifestV2->GetConfiguration();
			ICloudSaveTitleConfigurationV1* configV1 = NULL;
			hr = config->QueryInterface(IID_ICloudSaveTitleConfigurationV1, (void**)&configV1);
			rverify(SUCCEEDED(hr) && configV1, catchall, );

			// validate the file index
			rverify(fileIndex >= 0 && fileIndex <= (int)manifestV2->GetNumFiles(), catchall, hr = RGSC_INVALIDARG);

			// Extract the cloud file
			ICloudSaveFile* file = manifestV2->GetFile(fileIndex);
			rverify(file, catchall, );

			// Verify that it is at least a v2 cloud file
			ICloudSaveFileV2* rgscSaveFileV2 = NULL;
			rverify(SUCCEEDED(file->QueryInterface(IID_ICloudSaveFileV2, (void**)&rgscSaveFileV2)) && rgscSaveFileV2, catchall, );

			// Create file path for backups
			char backupPath[RGSC_MAX_PATH];
			rverify(BuildBackupPath(backupPath, configV1, rgscSaveFileV2), catchall, );
			safecatf(backupPath, RGSC_CLOUDSAVE_BACKUP_INFO_EXTENSION, backupIndex);

			// get device for this path
			const fiDevice* device = fiDevice::GetDevice(backupPath);
			rverify(device, catchall, );

			u64 file_size_u64 = device->GetFileSize(backupPath);

			// Sanity check. lets make sure we're within reason, Win32 compile compatibility below
			rverify(file_size_u64 < (u32)-1, catchall, );

			// convert to u32
			u32 fileSize = (u32)file_size_u64;

			u8 buf[RgscCloudSaveBackupInfo::MAX_EXPORTED_SIZE_IN_BYTES];
			
			// verify the file size is reasonable within our IO buffer
			rverify(fileSize <= RgscCloudSaveBackupInfo::MAX_EXPORTED_SIZE_IN_BYTES, catchall, );

			// Open the file
			fiHandle hFile = device->Open(backupPath, true);
			rverify(fiIsValidHandle(hFile), catchall, );

			// read the manifest and import
			int bytesRead = device->Read(hFile, buf, fileSize);
			device->Close(hFile);

			// import the file
			unsigned importSize;
			rverify(out_infoV1->Import(buf, bytesRead, &importSize), catchall, );
			rverify(bytesRead == (int)importSize, catchall, );

			rlDebug3("Imported from '%s' : %u bytes", backupPath, importSize);

			hr = RGSC_OK;
		}
		rcatchall
		{

		}

		return hr;
	}

	bool RgscCloudSaveManager::RestoreBackup(ICloudSaveManifest* manifest, const int fileIndex, const int backupIndex, IAsyncStatus* status)
	{
		RLPC_CREATETASK_IASYNC(RestoreBackupTask, manifest, fileIndex, backupIndex);
	}

	bool RgscCloudSaveManager::GetConflictMetadata(ICloudSaveManifest* manifest, IAsyncStatus* status)
	{
		RLPC_CREATETASK_IASYNC(GetConflictMetadataTask, manifest);
	}

	void RgscCloudSaveManager::CreateWorkerThread()
	{
		rlCloudSave::CreateWorker();
	}

	void RgscCloudSaveManager::ShutdownWorkerThread()
	{
		rlCloudSave::ShutdownWorker();
	}

} // namespace rgsc

