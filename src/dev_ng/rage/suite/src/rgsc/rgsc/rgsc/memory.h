#ifndef RGSC_MEMORY_H
#define RGSC_MEMORY_H

namespace rgsc
{
#if __DEV
// extra memory needed for debug messages that come from the SCUI via RGSC_DEBUG_EVENT
static const unsigned int SIMPLE_HEAP_SIZE_IN_KB = 4 * 1024; // 4MB
#else
static const unsigned int SIMPLE_HEAP_SIZE_IN_KB = 3 * 1024; // 3MB
#endif

// Convenience macros for allocating bytes and objects with the rgsc allocator.
#if __DEV
#define RGSC_ALLOCATE(trackName, size)        rgscLoggedAllocate(size, __FILE__, __LINE__, #trackName)
#define RGSC_ALLOCATE_T(trackName, type)      (type*)rgscLoggedAllocate(sizeof(type), __FILE__, __LINE__, #trackName)
#define RGSC_NEW(trackName, type)             rgscLoggedNew< type >(1, __FILE__, __LINE__, #trackName)
#define RGSC_NEW_ARRAY(trackName, type, len)  rgscLoggedNew< type >(len, __FILE__, __LINE__, #trackName)
#else
#define RGSC_ALLOCATE(trackName, size)        rgscAllocate(size)
#define RGSC_ALLOCATE_T(trackName, type)      (type*)rgscAllocate(sizeof(type))
#define RGSC_NEW(trackName, type)             rgscNew< type >(1)
#define RGSC_NEW_ARRAY(trackName, type, len)  rgscNew< type >(len)
#endif

#define RGSC_FREE(a)                          rgscFree(a)
#define RGSC_DELETE(a)                        rgscDelete(a)
#define RGSC_DELETE_ARRAY(a, len)             rgscDelete(a, len)

#if __DEV
void* rgscLoggedAllocate(const size_t size, const char* fileName, int lineNumber, const char* trackName);

template<typename T>
T* rgscLoggedNew(const size_t len, const char* fileName, int lineNumber, const char* trackName)
{
	T* t = (T*)rgscLoggedAllocate(sizeof(T) * len, fileName, lineNumber, trackName);
	if(t)
	{
		for(unsigned i = 0; i < len; i++) 
		{
			new(&t[i]) T();
		}
	}
	return t;
}
#else
	void* rgscAllocate(const size_t size);

	template<typename T>
	T* rgscNew(const size_t len)
	{
		T* t = (T*)rgscAllocate(sizeof(T) * len);
		if(t)
		{
			for(unsigned i = 0; i < len; i++) 
			{
				new(&t[i]) T();
			}
		}
		return t;
	}
#endif

size_t rgscGetAllocSize(const void* ptr);

void rgscFree(const void* ptr);

template<typename T>
void rgscDelete(T* t, const unsigned count = 1)
{
	if(t)
	{
		for(unsigned i = 0; i < count; ++i)
		{
			t[i].~T();
		}

		rgscFree(t);
	}
}

} // namespace rgsc

#endif //RGSC_MEMORY_H
