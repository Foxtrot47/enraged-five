// 
// rgsc/rgsc/telemetry.cpp
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

// rgsc_includes
#include "file/file_config.h"
#include "rgsc.h"
#include "telemetry.h"

// rage_includes
#include "atl/delegate.h"
#include "diag/seh.h"
#include "data/rson.h"
#include "rline/rltelemetry.h"
#include "rline/ros/rlros.h"
#include "string/stringhash.h"
#include "system/timer.h"

using namespace rgsc;

static rlTelemetry::OnDeferredFlushDelegate s_RgscTelemetryDeferredFlushDelegate(&RgscTelemetryManager::OnDeferredFlush);
static rlTelemetry::OnFlushEndDelegate s_RgscOnFlushEndDelegate(&RgscTelemetryManager::OnTelemetryFlushEnd);

static const int RGSC_DEFAULT_DELAY_FLUSH_INTERVAL_SECS = 3 * 1000; // Delay flushes by 3 seconds to batch flush requests together

namespace rgsc
{
	// ===============================================================================================
	// RgscMetric
	// ===============================================================================================
	class RgscMetric : public rlMetric
	{
	public:
		RgscMetric()
		{
			memset(m_MetricBlob, 0, sizeof(m_MetricBlob));
			memset(m_MetricName, 0, sizeof(m_MetricName));
			m_LogLevel = rlTelemetryPolicies::DEFAULT_LOG_LEVEL;
			m_LogChannel = rlTelemetryPolicies::DEFAULT_LOG_CHANNEL;
		}

		bool SetMetricBlob(const char* blob, unsigned blobLen)
		{
			rtry
			{
				rverify(blobLen < RGSC_TELEMETRY_MAX_BUF_SIZE, catchall, );
				safecpy(m_MetricBlob, blob);
				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		bool SetLogChannel(int logChannel)
		{
			m_LogChannel = logChannel;
			return true;
		}

		bool SetLogLevel(int logLevel)
		{
			m_LogLevel = logLevel;
			return true;
		}

		bool SetName(const char* name, unsigned nameLen)
		{
			rtry
			{
				rverify(nameLen < RGSC_TELEMETRY_MAX_NAME_BUF_SIZE, catchall, );
				safecpy(m_MetricName, name);
				return true;
			}
			rcatchall
			{
				return false;
			}
		}

		virtual const char* GetMetricName() const
		{
			return m_MetricName;
		}

		virtual bool Write(RsonWriter* rw) const
		{
			bool success = rw->Insert(m_MetricBlob);
			return success;
		}

		virtual size_t GetMetricSize() const { return sizeof(*this); }
		virtual u32 GetMetricId() const {return atStringHash(GetMetricName());}
		virtual int GetMetricLogLevel() const { return m_LogLevel; }
		virtual int GetMetricLogChannel() const { return m_LogChannel; }

	private:
		char m_MetricBlob[RGSC_TELEMETRY_MAX_BUF_SIZE];
		char m_MetricName[RGSC_TELEMETRY_MAX_NAME_BUF_SIZE];
		int m_LogLevel;
		int m_LogChannel;
	};

	// ===============================================================================================
	// rlPcTelemetry
	// ===============================================================================================
	RGSC_HRESULT RgscTelemetryManager::QueryInterface(RGSC_REFIID riid, void** ppvObj)
	{
		IRgscUnknown *pUnknown = NULL;

		if(ppvObj == NULL)
		{
			return RGSC_INVALIDARG;
		}

		if(riid == IID_IRgscUnknown)
		{
			pUnknown = static_cast<ITelemetry*>(this);
		}
		else if(riid == IID_ITelemetryV1)
		{
			pUnknown = static_cast<ITelemetryV1*>(this);
		}
		else if(riid == IID_ITelemetryV2)
		{
			pUnknown = static_cast<ITelemetryV2*>(this);
		}
		else if(riid == IID_ITelemetryV3)
		{
			pUnknown = static_cast<ITelemetryV3*>(this);
		}
		else if(riid == IID_ITelemetryV4)
		{
			pUnknown = static_cast<ITelemetryV4*>(this);
		}

		*ppvObj = pUnknown;
		if(pUnknown == NULL)
		{
			return RGSC_NOINTERFACE;
		}

		return RGSC_OK;
	}

	RgscTelemetryManager::RgscTelemetryManager()
		: m_FlushRequestedTime(0)
		, m_GameHeaderInfoCallback(NULL)
	{
		m_HeaderInfoCallback.Bind(this, &RgscTelemetryManager::SetGamerHeader);
	}

	RgscTelemetryManager::~RgscTelemetryManager()
	{
		m_HeaderInfoCallback.Clear();
	}

	bool RgscTelemetryManager::Init()
	{
		//@@: location RLPCTELEMETRY_INIT_CALL_INIT
		rlTelemetry::Init(s_RgscTelemetryDeferredFlushDelegate, s_RgscOnFlushEndDelegate);
		return true;
	}

	void RgscTelemetryManager::Update()
	{
		rlTelemetry::UpdateClass();

		if (m_FlushRequestedTime > 0 && sysTimer::HasElapsedIntervalMs(m_FlushRequestedTime, RGSC_DEFAULT_DELAY_FLUSH_INTERVAL_SECS))
		{
			rlTelemetry::Flush();
			m_FlushRequestedTime = 0;
		}
	}

	bool RgscTelemetryManager::Shutdown()
	{
		rlTelemetry::Shutdown();
		return true;
	}

	void RgscTelemetryManager::OnDeferredFlush( void )
	{
		// defining a callback here is required, but we're not doing anything. 
		// This was hooked up in RAGE for GTAV specific functionality.
	}

	void RgscTelemetryManager::OnTelemetryFlushEnd( const netStatus& status )
	{
		// defining a callback here is required, but we're not doing anything. 
		// This was hooked up in RAGE for GTAV specific functionality.

		if(status.Failed())
		{
			rlDebug3("rlPcTelemetry::OnTelemetryFlushEnd() - flush failed");
		}
		else if(status.Succeeded())
		{
			rlDebug3("rlPcTelemetry::OnTelemetryFlushEnd() - flush succeeded");
		}
	}

	RGSC_HRESULT RgscTelemetryManager::SetPolicies(ITelemetryPolicy* policies)
	{
		if(policies == NULL)
		{
			return ERROR_BAD_ARGUMENTS;
		}

		RGSC_HRESULT hr = RGSC_NOINTERFACE;
		rlTelemetryPolicies policy;

		ITelemetryPolicyV1* v1 = NULL;
		hr = policies->QueryInterface(IID_ITelemetryPolicyV1, (void**) &v1);
		if (SUCCEEDED(hr) && (v1 != NULL))
		{
			policy.m_SubmissionIntervalSeconds = v1->GetSubmissionIntervalSeconds();
			policy.m_SubmissionTimeoutSeconds = v1->GetSubmissionTimeoutSeconds();

			for (int i = 0; i < rlTelemetryPolicies::MAX_NUM_LOG_CHANNELS; i++)
			{
				policy.m_Channels[i].Set(v1->GetLogLevel());
			}
		}

		policy.GameHeaderInfoCallback = m_HeaderInfoCallback;

		// Set the policies
		rlTelemetry::SetPolicies(policy);

		return ERROR_SUCCESS;
	}

	bool RgscTelemetryManager::Write(const char* metricName, const char* metricBlob)
	{
		return Write(metricName, metricBlob, rlTelemetryPolicies::DEFAULT_LOG_CHANNEL, rlTelemetryPolicies::DEFAULT_LOG_LEVEL);
	}

	bool RgscTelemetryManager::Write(const char* metricName, const char* metricBlob, const int logChannel, const int logLevel)
	{
		RgscMetric metric;

		rtry
		{
			unsigned blobLen = (unsigned)strlen(metricBlob);
			rverify(RsonReader::ValidateJsonObject(metricBlob, blobLen + 1 /* null terminator */, true), catchall, );
			rverify(metric.SetMetricBlob(metricBlob, blobLen), catchall, );
			rverify(metric.SetName(metricName, (unsigned)strlen(metricName)), catchall, );
			rverify(metric.SetLogChannel(logChannel), catchall, );
			rverify(metric.SetLogLevel(logLevel), catchall, );

			rverify(rlTelemetry::Write(RL_RGSC_GAMER_INDEX, metric), catchall, );

			rlDebug3("Queued custom telemetry metric \"%s\" : { %s }", metricName, metricBlob);
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void RgscTelemetryManager::CancelFlushes()
	{
		rlTelemetry::CancelFlushes();
	}

	bool RgscTelemetryManager::Flush(bool bFlushImmediate)
	{
		rtry
		{
			rverify(GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal(), catchall, );
			rverify(rlRos::IsOnline(0), catchall, );
			rverify(rlTelemetry::HasAnyMetrics(), catchall, );
			rcheck(!rlTelemetry::AreAllPending(), catchall, );

			if (bFlushImmediate)
			{
				m_FlushRequestedTime = 0;
				return rlTelemetry::Flush();
			}
			else
			{
				m_FlushRequestedTime = sysTimer::GetSystemMsTime();
				return true;
			}
		}
		rcatchall
		{
			return false;
		}
	}

	bool RgscTelemetryManager::HasAnyMetrics()
	{
		return rlTelemetry::HasAnyMetrics();
	}

	bool RgscTelemetryManager::IsFlushInProgress()
	{
		return rlTelemetry::FlushInProgress();
	}

	u32 RgscTelemetryManager::GetAvailableMemory()
	{
		return rlTelemetry::GetAvailableSize();
	}

	bool RgscTelemetryManager::HasMemoryForMetric()
	{
		u32 memoryAvailable = GetAvailableMemory();
		u32 memoryRequired = sizeof(RgscMetric);
		return memoryAvailable > memoryRequired;
	}

	bool RgscTelemetryManager::IsAcceptingWrites()
	{
		// memory is required
		if (!HasMemoryForMetric())
		{
			return false;
		}

		// rlPresence gamer handle is required for the rltelemetry code
		rlGamerHandle gh;
		if(!rlPresence::GetGamerHandle(0, &gh))
		{
			return false;
		}

		// online is required
		if (!GetRgscConcreteInstance()->_GetProfileManager()->IsOnlineInternal())
		{
			return false;
		}

		return true;
	}

	void RgscTelemetryManager::SetGameHeaderInfoCallback(GameHeaderInfoCallback cb)
	{
		Assert(m_GameHeaderInfoCallback == NULL);
		m_GameHeaderInfoCallback = cb;
	}

	bool RgscTelemetryManager::SetGamerHeader(RsonWriter* rw)
	{
		if (m_GameHeaderInfoCallback)
		{
			const char* gameHeader = m_GameHeaderInfoCallback(); 
			if (gameHeader)
			{
				// Game header successful, write
				return rw->Insert(gameHeader);
			}

			// Callback is bound, but game header came back NULL
			return false;
		}

		// No callback bound, successs
		return true;
	}

} // namespace rgsc

