// 
// rgsc/rgsc/cloudsave.h 
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RGSC_CLOUDSAVE_H 
#define RGSC_CLOUDSAVE_H

// rgsc includes
#include "file/file_config.h"
#include "cloudsave_interface.h"
#include "rgsc_common.h"

// rage includes
#include "atl/array.h"
#include "data/bitbuffer.h"
#include "net/http.h"
#include "net/netaddress.h"
#include "rline/cloudsave/rlcloudsave.h"
#include "system/criticalsection.h"

namespace rgsc
{

	// ===============================================================================================
	// RgscCloudSaveFile
	// ===============================================================================================
	class RgscCloudSaveFile : public ICloudSaveFileLatestVersion
	{
	public:

		static const int VERSION_1 = 1;
		static const int VERSION_LATEST = VERSION_1;

		// Simple RGSC wrapper for RAGE cloud save file, so return the same max export size + additional bytes for the conflict state and dirty flags
		static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = sizeof(unsigned) +									// m_ImportVersion
															sizeof(ICloudSaveFileV2::ConflictState) +			// m_ConflictState
															sizeof(bool) +										// m_Dirty
															sizeof(bool) +										// m_bMarkedForDeletion
															rage::rlCloudSaveFile::MAX_EXPORTED_SIZE_IN_BYTES;	// m_SaveFile

		// ===============================================================================================
		// inherited from IRgscUnknown
		// ===============================================================================================
		virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

		// ===============================================================================================
		// inherited from ICloudSaveFileV1
		// ===============================================================================================
		virtual void RGSC_CALL Clear();
		virtual void RGSC_CALL SetFileId(u64 fileId);
		virtual u64 RGSC_CALL GetFileId() const;
		virtual void RGSC_CALL SetMd5Hash(const char* md5Hash);
		virtual const char* RGSC_CALL GetMd5Hash() const;
		virtual void RGSC_CALL SetFileName(const char* fileName);
		virtual const char* RGSC_CALL GetFileName() const;
		virtual void RGSC_CALL SetFileVersion(u64 fileVersion);
		virtual u64 RGSC_CALL GetFileVersion() const;
		virtual void RGSC_CALL SetClientLastModifiedDate(u64 clientLastModifiedDate);
		virtual u64 RGSC_CALL GetClientLastModifiedDate() const;
		virtual void RGSC_CALL SetServerLastModifiedDate(u64 serverLastModifiedDate);
		virtual u64 RGSC_CALL GetServerLastModifiedDate() const;
		virtual void RGSC_CALL SetHardwareId(const char* hardwareId);
		virtual const char* RGSC_CALL GetHardwareId() const;
		virtual void RGSC_CALL SetLastIpAddress(const char* ipAddressString);
		virtual const char* RGSC_CALL GetLastIpAddress() const;
		virtual void RGSC_CALL CopyData(u8* data, unsigned dataLen);
		virtual u8* RGSC_CALL GetData() const;
		virtual u32 RGSC_CALL GetDataLength() const;

		// ===============================================================================================
		// inherited from ICloudSaveFileV2
		// ===============================================================================================
		virtual const char* RGSC_CALL GetMetaData();
		virtual void RGSC_CALL SetMetaData(const char* metaData);
		virtual ConflictState RGSC_CALL GetConflictState();
		virtual void RGSC_CALL SetConflictState(ConflictState state);
		virtual bool RGSC_CALL IsDirty();
		virtual void RGSC_CALL SetIsDirty(bool bDirty);
		virtual ICloudSaveFileV2::ConflictResolutionType RGSC_CALL GetResolveType();
		virtual void RGSC_CALL SetResolveType(ICloudSaveFileV2::ConflictResolutionType resolveType);
		virtual void RGSC_CALL FreeSaveData();
		virtual void RGSC_CALL SetShouldCopyFile(bool bShouldCopy);
		virtual bool RGSC_CALL ShouldCopyFile();
		virtual void RGSC_CALL SetNextExpectedVersion(u64 fileVersion);
		virtual u64 RGSC_CALL GetNextExpectedVersion() const;
		virtual void RGSC_CALL SetServerMetadata(const char* metadata);
		virtual const char* RGSC_CALL GetServerMetadata() const;
		virtual void RGSC_CALL SetClientFileSize(u64 fileSize);
		virtual u64 RGSC_CALL GetClientFileSize() const;
		virtual void RGSC_CALL SetServerFileSize(u64 fileSize);
		virtual u64 RGSC_CALL GetServerFileSize() const;
		virtual void RGSC_CALL SetLatestServerVersion(u64 fileVersion);
		virtual u64 RGSC_CALL GetLatestServerVersion() const;
		virtual void RGSC_CALL SetMarkedForDeletion(bool bMarkedForDeletion);
		virtual bool RGSC_CALL IsMarkedForDeletion() const;

		// ===============================================================================================
		// inherited from ICloudSaveFileV3
		// ===============================================================================================
		virtual bool RGSC_CALL HasSearchedBackups();
		virtual void RGSC_CALL SetHasSearchedBackups(bool bSearched);
		virtual s64 RGSC_CALL GetBackupSlots();
		virtual void RGSC_CALL SetBackupSlots(s64 numBackups);

		// ===============================================================================================
		// accessible from anywhere in the dll
		// ===============================================================================================
		RgscCloudSaveFile();
		virtual ~RgscCloudSaveFile();

		// Import/Export operations
		bool Export(void* buf, const unsigned sizeofBuf, unsigned* size = 0) const;
		bool Import(const void* buf, const unsigned sizeofBuf, unsigned* size = 0);

	private:

		bool IsValidImportVersion(int version) { return version == VERSION_1; }
		
		// Exported Variables
		unsigned m_ImportVersion;
		ICloudSaveFileV2::ConflictState m_ConflictState;
		bool m_bIsDirty;
		bool m_bMarkedForDeletion;
		rage::rlCloudSaveFile m_SaveFile;

		// Non-Exported Variables (Short-Term/Single Operation)
		ICloudSaveFileV2::ConflictResolutionType m_ResolveType;
		char m_IpBuf[rage::netIpAddress::MAX_STRING_BUF_SIZE];
		bool m_bShouldCopy;
		bool m_bHasSearchedBackups;
		s64 m_BackupSlots;

	};

	// ===============================================================================================
	// RgscCloudSaveHttpDebugInfo
	// ===============================================================================================
	class RgscCloudSaveHttpDebugInfo : public ICloudSaveHttpDebugInfoLatestVersion
	{
	public:

		// ===============================================================================================
		// inherited from IRgscUnknown
		// ===============================================================================================
		virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

		// ===============================================================================================
		// inherited from ICloudSaveHttpDebugInfoV1
		// ===============================================================================================
		virtual void RGSC_CALL Reset();
		virtual int RGSC_CALL GetSendState();
		virtual void RGSC_CALL SetSendState(int sendState);
		virtual int RGSC_CALL GetRecvState();
		virtual void RGSC_CALL SetRecvState(int recvState);
		virtual unsigned RGSC_CALL GetInContentBytesRcvd();
		virtual void RGSC_CALL SetInContentBytesRcvd(int bytes);
		virtual unsigned RGSC_CALL GetOutContentBytesSent();
		virtual void RGSC_CALL SetOutContentBytesSent(int bytes);
		virtual bool RGSC_CALL IsCommitted();
		virtual void RGSC_CALL SetCommitted(bool committed);
		virtual int RGSC_CALL GetHttpStatusCode();
		virtual void RGSC_CALL SetHttpStatusCode(int code);

		// ===============================================================================================
		// inherited from ICloudSaveHttpDebugInfoV2
		// ===============================================================================================
		virtual int RGSC_CALL GetAbortReason();
		virtual void RGSC_CALL SetAbortReason(int abortReason);
		virtual bool RGSC_CALL HadMemoryAllocationError();
		virtual void RGSC_CALL SetHadMemoryAllocationError(bool bHadMemoryError);
		virtual int RGSC_CALL GetTcpResultState();
		virtual void RGSC_CALL SetTcpResultState(int resultState);
		virtual int RGSC_CALL GetTcpError();
		virtual void RGSC_CALL SetTcpError(int error);

		// ===============================================================================================
		// accessible from anywhere in the dll
		// ===============================================================================================
		RgscCloudSaveHttpDebugInfo();

	protected:

		rage::netHttpRequest::DebugInfo m_DebugInfo;
	};

	// ===============================================================================================
	// RgscCloudSaveBackupInfo
	// ===============================================================================================
	class RgscCloudSaveBackupInfo : public ICloudSaveBackupInfoLatestVersion
	{
	public:

		static const int VERSION_1 = 1;
		static const int VERSION_LATEST = VERSION_1;

		static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = sizeof(u32) + // m_ImportVersion
															sizeof(u64) + // m_ClientLastModifiedDate
															rage::datMaxBytesNeededForString<rage::rlCloudSaveFile::MD5_HASH_BUF_SIZE>::COUNT + // m_Md5Hash
															rage::datMaxBytesNeededForString<rage::rlCloudSaveFile::METADATA_MAX_BUF_SIZE>::COUNT + // m_MetaData
															rage::datMaxBytesNeededForString<rage::rlCloudSaveFile::FILENAME_MAX_BUF_SIZE>::COUNT +
															rage::datMaxBytesNeededForString<rage::rlCloudSaveFile::HARDWARE_ID_MAX_BUF_SIZE>::COUNT; // m_FileName

		// ===============================================================================================
		// inherited from IRgscUnknown
		// ===============================================================================================
		virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

		// ===============================================================================================
		// inherited from ICloudSaveBackupInfoV1
		// ===============================================================================================
		virtual void RGSC_CALL Clear();
		virtual void RGSC_CALL SetMd5Hash(const char* md5Hash);
		virtual const char* RGSC_CALL GetMd5Hash() const;
		virtual const char* RGSC_CALL GetMetaData();
		virtual void RGSC_CALL SetMetaData(const char* metaData);
		virtual void RGSC_CALL SetFileName(const char* fileName);
		virtual const char* RGSC_CALL GetFileName() const;
		virtual void RGSC_CALL SetClientLastModifiedDate(u64 clientLastModifiedDate);
		virtual u64 RGSC_CALL GetClientLastModifiedDate() const;
		virtual void RGSC_CALL SetHardwareId(const char* hardwareId);
		virtual const char* RGSC_CALL GetHardwareId() const;
		virtual bool RGSC_CALL Export(void* buf, const unsigned sizeofBuf, unsigned* size = 0);
		virtual bool RGSC_CALL Import(const void* buf, const unsigned sizeofBuf, unsigned* size = 0);

		// ===============================================================================================
		// accessible from anywhere in the dll
		// ===============================================================================================
		RgscCloudSaveBackupInfo();
		virtual ~RgscCloudSaveBackupInfo();

	private:

		bool IsValidImportVersion(int version) { return version == VERSION_1; }

		u32 m_ImportVersion;
		u64 m_ClientLastModifiedDate;
		char m_Md5Hash[rage::rlCloudSaveFile::MD5_HASH_BUF_SIZE];
		char m_MetaData[rage::rlCloudSaveFile::METADATA_MAX_BUF_SIZE];
		char m_FileName[rage::rlCloudSaveFile::FILENAME_MAX_BUF_SIZE];
		char m_HardwareId[rage::rlCloudSaveFile::HARDWARE_ID_MAX_BUF_SIZE];
	};

	// ===============================================================================================
	// RgscCloudSaveOperationProgress
	// ===============================================================================================
	class RgscCloudSaveOperationProgress : public ICloudSaveOperationProgressLatestVersion
	{
	public:
		// ===============================================================================================
		// inherited from IRgscUnknown
		// ===============================================================================================
		virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

		// ===============================================================================================
		// inherited from ICloudSaveOperationProgressV1
		// ===============================================================================================
		virtual void RGSC_CALL Reset();
		virtual void RGSC_CALL SetIsActive(bool bActive);
		virtual bool RGSC_CALL IsActive();
		virtual void RGSC_CALL SetStepCount(u32 stepCount);
		virtual u32 RGSC_CALL GetStepCount();
		virtual void RGSC_CALL SetCompletedCount(u32 completedCount);
		virtual u32 RGSC_CALL GetCompletedCount();

		// ===============================================================================================
		// inherited from ICloudSaveOperationProgressV2
		// ===============================================================================================
		virtual u64 RGSC_CALL GetTotalLength();
		virtual void RGSC_CALL SetTotalLength(u64 length);
		virtual u64 RGSC_CALL GetTotalProgress();
		virtual void RGSC_CALL SetTotalProgress(u64 progress);
		virtual u64 RGSC_CALL GetStepLength();
		virtual void RGSC_CALL SetStepLength(u64 length);
		virtual u64 RGSC_CALL GetStepProgress();
		virtual void RGSC_CALL SetStepProgress(u64 progress);

		// ===============================================================================================
		// accessible from anywhere in the dll
		// ===============================================================================================
		RgscCloudSaveOperationProgress();

	private:

		bool m_bIsActive;
		u32 m_StepCount;
		u32 m_CompletionCount;
		u64 m_TotalLength;
		u64 m_TotalProgress;
		u64 m_StepLength;
		u64 m_StepProgress;
	};
	
	// ===============================================================================================
	// RgscCloudSaveManifest
	// ===============================================================================================
	class RgscCloudSaveManifest : public ICloudSaveManifestLatestVersion
	{
	public:

		static const int VERSION_1 = 1;
		static const int VERSION_LATEST = VERSION_1;

		static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = sizeof(unsigned) +									// m_Version
															sizeof(int) +										// m_CloudSavesEnabledState
															sizeof(u64) +										// m_BytesUsed
														(ICloudSaveManager::MAX_CLOUD_SAVE_SLOTS *				// Max Num Save Slots * Max Export Size of Cloud Save File
															RgscCloudSaveFile::MAX_EXPORTED_SIZE_IN_BYTES);			

		// ===============================================================================================
		// inherited from IRgscUnknown
		// ===============================================================================================
		virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

		// ===============================================================================================
		// inherited from ICloudSaveManifestV1
		// ===============================================================================================
		virtual ICloudSaveFile* RGSC_CALL AddFile();
		virtual void RGSC_CALL Clear();
		virtual u32 RGSC_CALL GetNumFiles() const;
		virtual ICloudSaveFile* RGSC_CALL GetFile(unsigned index);
		virtual u64 RGSC_CALL GetBytesUsed() const;
		virtual void RGSC_CALL SetBytesUsed(u64 bytesUsed);

		// ===============================================================================================
		// inherited from ICloudSaveManifestV2
		// ===============================================================================================
		virtual ICloudSaveTitleConfiguration* RGSC_CALL GetConfiguration();
		virtual const char* RGSC_CALL GetTitleName();
		virtual bool RGSC_CALL Export(void* buf, const unsigned sizeofBuf, unsigned* size = 0);
		virtual bool RGSC_CALL Import(const void* buf, const unsigned sizeofBuf, unsigned* size = 0);
		virtual unsigned RGSC_CALL GetMaxExportBufSize();
		virtual ICloudSaveManifestV2::CloudSavesEnabledState RGSC_CALL GetCloudSaveEnabled();
		virtual void RGSC_CALL SetCloudSavesEnabled(ICloudSaveManifestV2::CloudSavesEnabledState state);
		virtual bool RGSC_CALL RegisterForUpload(const char* fileName, const char* metadata);
		virtual int RGSC_CALL GetNumUnresolvedConflicts();
		virtual int RGSC_CALL GetNumDirtyFiles();
		virtual bool RGSC_CALL Load();
		virtual bool RGSC_CALL Save();
		virtual bool RGSC_CALL RemoveFile(unsigned index);
		virtual bool RGSC_CALL UnregisterFile(const char* fileName);

		// ===============================================================================================
		// inherited from ICloudSaveManifestV3
		// ===============================================================================================
		virtual ICloudSaveOperationProgress* RGSC_CALL GetProgressTracker();

		// ===============================================================================================
		// inherited from ICloudSaveManifestV4
		// ===============================================================================================
		virtual ICloudSaveHttpDebugInfo* RGSC_CALL GetHttpDebugInfo();

		// ===============================================================================================
		// accessible from anywhere in the dll
		// ===============================================================================================
		RgscCloudSaveManifest();
		RgscCloudSaveManifest(u32 numSlots);
		virtual ~RgscCloudSaveManifest();
		bool SetConfiguration(ICloudSaveTitleConfiguration* config);
		void Reserve(u32 numSlots);
		void GetClientLastModifiedDates();
		bool IsValidFileName(const char* fileName);

	private:

		RgscCloudSaveFile* CreateSaveFile();
		bool IsKnownVersion(int version);
		RgscCloudSaveFile* FindFile(const char* fileName);
		void Cleanup();

		int m_Version;
		rage::atArray<RgscCloudSaveFile*> m_CloudFiles;
		ICloudSaveManifestV2::CloudSavesEnabledState m_CloudSavesEnabledState;
		u64 m_BytesUsed;

		ICloudSaveTitleConfiguration* m_Configuration;
		ICloudSaveTitleConfigurationV1* m_ConfigurationV1;

		RgscCloudSaveOperationProgress m_ProgressTracker;
		RgscCloudSaveHttpDebugInfo m_HttpDebugInfo;

		rage::sysCriticalSectionToken m_ManifestCs;
	};

	// ===============================================================================================
	// RgscCloudSaveManager
	// ===============================================================================================
	class RgscCloudSaveManager : public ICloudSaveManagerLatestVersion
	{
	public:

		// ===============================================================================================
		// inherited from IRgscUnknown
		// ===============================================================================================
		virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

		// ===============================================================================================
		// inherited from ICloudSaveManagerV1
		// ===============================================================================================
		virtual void RGSC_CALL SetMaximumFileSize(u64 maximumFileSize);
		virtual ICloudSaveManifest* RGSC_CALL CreateManifest(u32 numSlots);
		virtual bool RGSC_CALL GetCloudSaveManifest(ICloudSaveManifest* manifest, IAsyncStatus* status);
		virtual bool RGSC_CALL GetFile(ICloudSaveFile* saveFile, ResolveType resolveType, const char* hardwareId, IAsyncStatus* status);
		virtual bool RGSC_CALL PostFile(ICloudSaveFile* saveFile, ResolveType resolveType, IAsyncStatus* status);
		
		// ===============================================================================================
		// inherited from ICloudSaveManagerV2
		// ===============================================================================================
		virtual ICloudSaveManifest* RGSC_CALL RegisterTitle(ICloudSaveTitleConfiguration* configuration);
		virtual void RGSC_CALL UnregisterTitle(ICloudSaveManifest* manifest);
		virtual bool RGSC_CALL IdentifyConflicts(ICloudSaveManifest* manifest, IAsyncStatus* status);
		virtual bool RGSC_CALL PostUpdatedFiles(ICloudSaveManifest* manifest, IAsyncStatus* status);
		virtual bool RGSC_CALL GetUpdatedFiles(ICloudSaveManifest* manifest, IAsyncStatus* status);
		virtual bool RGSC_CALL CopyUpdatedFiles(ICloudSaveManifest* manifest, IAsyncStatus* status);
		virtual bool RGSC_CALL RegisterFileForUpload(ICloudSaveManifest* manifest, const char* fileName, const char* metaData);
		virtual bool RGSC_CALL UnregisterFile(ICloudSaveManifest* manifest, const char* fileName);
		virtual bool RGSC_CALL DeleteUpdatedfiles(ICloudSaveManifest* manifest, IAsyncStatus* status);

		// ===============================================================================================
		// inherited from ICloudSaveManagerV3
		// ===============================================================================================
		virtual bool RGSC_CALL HasBetaAccessAsync(const char* cohortsFilePath, bool* bOutResult, IAsyncStatus* status);
		virtual bool RGSC_CALL WriteConflictTelemetry(ICloudSaveManifest* manifest);
		virtual bool RGSC_CALL BackupConflictedFiles(ICloudSaveManifest* manifest, IAsyncStatus* status);
		virtual bool RGSC_CALL SearchForBackups(ICloudSaveManifest* manifest, IAsyncStatus* status);
		virtual int RGSC_CALL GetNumBackupSlots();
		virtual void RGSC_CALL SetNumBackupSlots(int numSlots);
		virtual ICloudSaveBackupInfo* RGSC_CALL AllocateBackupInfo();
		virtual void RGSC_CALL FreeBackupInfo(ICloudSaveBackupInfo* info);
		virtual RGSC_HRESULT RGSC_CALL GetBackupInfo(ICloudSaveManifest* manifest, const int fileIndex, const int backupIndex, ICloudSaveBackupInfo* out_info);
		virtual bool RGSC_CALL RestoreBackup(ICloudSaveManifest* manifest, const int fileIndex, const int backupIndex, IAsyncStatus* status);
		virtual bool RGSC_CALL GetConflictMetadata(ICloudSaveManifest* manifest, IAsyncStatus* status);

		// ===============================================================================================
		// inherited from ICloudSaveManagerV4
		// ===============================================================================================
		virtual void RGSC_CALL CreateWorkerThread();
		virtual void RGSC_CALL ShutdownWorkerThread();

		// ===============================================================================================
		// accessible from anywhere in the dll
		// ===============================================================================================
		RgscCloudSaveManager();
		virtual ~RgscCloudSaveManager();
		bool Init();
		bool Shutdown();
		bool IsTitleRegistered(const char* titleName);
		bool SaveManifest(ICloudSaveManifest* manifest);
		bool LoadManifest(ICloudSaveManifest* manifest);
		bool GetManifestPath(ICloudSaveManifestV2* manifestV2, char (&path)[RGSC_MAX_PATH]);
		bool GetLocalSaveLocation(ICloudSaveManifestV2* manifestV2, char (&path)[RGSC_MAX_PATH]);
		const char* GetCloudSaveEnabledAsJson(const char* jsonRequest);
		void SetCloudSaveEnabledFromJson(const char* jsonRequest);
		bool RegisterFileForUploadAsync(ICloudSaveManifest* manifest, const char* fileName, const char* metaData, IAsyncStatus* status);

	private:

		void FreeManifestIOBuffer();
		RGSC_HRESULT CreateRegisterFileTask(ICloudSaveManifest* manifest, const char* fileName, const char* metaData);
		RGSC_HRESULT CreateUnregisterFileTask(ICloudSaveManifest* manifest, const char* fileName);

		rage::atArray<ICloudSaveManifestV2*> m_TitleManifests;
		u8* m_ManifestIOBuffer;
		u32 m_ManifestIOBufferLength;
		rage::sysCriticalSectionToken m_ManifestIOCs;
		
		int m_iNumBackupSlots;
		int m_NumAllocatedBackupInfo;
	};

} // namespace rgsc

#endif // RGSC_CLOUDSAVE_H 
