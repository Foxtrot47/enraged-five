// 
// rline/rgsctasks.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RGSCTASKS_H
#define RLINE_RGSCTASKS_H

#include "file/file_config.h"
#include "tasks.h"
#include "rgsc.h"

// rage headers
#include "ros/rlroshttptask.h"
#include "rline/rlworker.h"

using namespace rage;

namespace rgsc
{

typedef rgscTask<Rgsc> RgscTaskBase;

///////////////////////////////////////////////////////////////////////////////
//  OnGameCrashTask
///////////////////////////////////////////////////////////////////////////////
class OnGameCrashTask : public RgscTaskBase
{
public:
	RL_TASK_DECL(OnGameCrashTask);

	enum State
	{
		STATE_INVALID = -1,
		STATE_UNADVERTISE_SESSIONS,
		STATE_UNADVERTISING_SESSIONS,
		STATE_SIGN_OUT,
		STATE_SIGNING_OUT,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	OnGameCrashTask();

	bool Configure(Rgsc* ctx, const int localGamerIndex, const u64 actionToTake);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	bool UnAdvertiseAll();
	bool SignOut();

	u64 m_ActionToTake;
	int m_LocalGamerIndex;
	int m_State;
	netStatus m_MyStatus;
};


} // namespace rgsc

#endif // RLINE_RGSCTASKS_H