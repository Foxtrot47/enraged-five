#ifndef RGSC_OUTPUT_H
#define RGSC_OUTPUT_H

#include "rgsc_common.h"

namespace rgsc
{
#if __DEV
	void RgscOpenRageLog();
	void RgscCloseRageLog();
	void OutputMsg(int severity, const char *file, int line, const char* func, const char* condition, const char *fmt, ...);
#endif

#define RgscError RgscRetailLog::RgscErrorf
#define RgscWarning RgscRetailLog::RgscWarningf
#define RgscDisplay RgscRetailLog::RgscDisplayf
#define RgscErrorW RgscRetailLog::RgscErrorWf
#define RgscWarningW RgscRetailLog::RgscWarningWf
#define RgscDisplayW RgscRetailLog::RgscDisplayWf
#define RgscErrorUtf8 RgscRetailLog::RgscErrorUtf8f
#define RgscWarningUtf8 RgscRetailLog::RgscWarningUtf8f
#define RgscDisplayUtf8 RgscRetailLog::RgscDisplayUtf8f

	class RgscRetailLog
	{
	public:
		static void RgscOpenRetailLog();
		static void RgscCloseRetailLog();
		static void AddInterval();

		static void RgscErrorf(const char* fmt, ...);
		static void RgscWarningf(const char* fmt, ...);
		static void RgscDisplayf(const char* fmt, ...);

		static void RgscErrorWf(const wchar_t* fmt, ...);
		static void RgscWarningWf(const wchar_t* fmt, ...);
		static void RgscDisplayWf(const wchar_t* fmt, ...);

		static void RgscErrorUtf8f(const char* fmt, ...);
		static void RgscWarningUtf8f(const char* fmt, ...);
		static void RgscDisplayUtf8f(const char* fmt, ...);

	private:
		static u32 GetLogTime();
		static void ProcessMessageQueue();
		static void QueueMessage(const wchar_t* msg);

		static void RgscRetailMsg(bool utf8, int severity, const char* fmt, va_list args);
		static void RgscRetailMsgW(int severity, const wchar_t* fmt, va_list args);
	};

} // namespace rgsc

#endif // RGSC_OUTPUT_H
