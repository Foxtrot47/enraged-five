#pragma warning(push)
#pragma warning(disable: 4668)
#include <vadefs.h>
#include <stdarg.h>
#include <stdio.h>
#include <shlobj.h>
#include <time.h>
#pragma warning(pop)

// rgsc includes
#include "output.h"
#include "delegate_interface.h"
#include "../rgsc/rgsc.h"

// rage includes
#include "system/criticalsection.h"
#include "atl/queue.h"
#include "atl/string.h"

namespace rgsc
{

static const int MAX_MSG_QUEUE = 30;
static FILE* s_RgscRageOutFile = NULL;
static FILE* s_RgscRetailLogFile = NULL;
static sysCriticalSectionToken s_LogToken;
static atQueue<wchar_t*, MAX_MSG_QUEUE> s_MsgQueue;
static bool s_bLogError = false;

#if __DEV
void RgscOpenRageLog()
{
	const char* filePath = GetRgscConcreteInstance()->GetLoggingPath();
	s_RgscRageOutFile = fopen(filePath, "w+");
	if (s_RgscRageOutFile)
	{
		fprintf(s_RgscRageOutFile, "********************** SOCIAL CLUB SDK LOG **********************\n");
	}
}

void RgscCloseRageLog()
{
	if (s_RgscRageOutFile)
	{
		fclose(s_RgscRageOutFile);
		s_RgscRageOutFile = NULL;
	}
}

void OutputMsg( int severity, const char* file, int line, const char* func, const char* condition, const char *fmt, ...)
{
	char message[4096];

	int len = 0;
	char* msg = &message[0];

	bool hasCondition = (condition != NULL) && (condition[0] != '\0');
	bool hasFmt = (fmt != NULL) && (fmt[0] != '\0');

	if(hasCondition)
	{
		if(func)
		{
			formatf(message, "\n\nFile: %s\nLine: %d\nFunction: %s\nAssertion: %s%s", file, line, func, condition, hasFmt ? "\nMessage: " : "");
		}
		else
		{
			formatf(message, "\n\nFile: %s\nLine: %d\nAssertion: %s%s", file, line, condition, hasFmt ? "\nMessage: " : "");
		}
		len = (int)strlen(message);
		msg = &message[len];
	}

	if(hasFmt)
	{
		va_list args;
		va_start(args, fmt);
		_vsnprintf_s(msg, sizeof(message) - len - 1, _TRUNCATE, fmt, args);
		va_end(args); 
	}

	if(Rgsc::IsInitializedForOutput())
	{
		GetRgscConcreteInstance()->Output((IRgscDelegateV1::OutputSeverity)severity, message);

		if (GetRgscConcreteInstance()->IsLoggingEnabled())
		{
			OutputDebugStringA(message);

			if (s_RgscRageOutFile)
			{
				fprintf(s_RgscRageOutFile, "%s\n", message);
				fflush(s_RgscRageOutFile);
			}
		}
	}
}
#endif // __DEV

char* GetRetailLogPath(char (&dataRootDirectory)[RGSC_MAX_PATH])
{
	rtry
	{
		dataRootDirectory[0] = '\0';

		rverify(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformDirectory(dataRootDirectory, true), catchall, );

		const char* logName = 
#if __DEV
		GetRgscConcreteInstance()->IsLauncher() ? "\\socialclub_launcher_debug.log" : "\\socialclub_debug.log";
#else
		GetRgscConcreteInstance()->IsLauncher() ? "\\socialclub_launcher.log" : "\\socialclub.log";
#endif

		safecat(dataRootDirectory, logName);
	}
	rcatchall
	{
		dataRootDirectory[0] = '\0';
		s_bLogError = true;
	}

	return dataRootDirectory;
}

void RgscRetailLog::RgscOpenRetailLog()
{
	char filePath[RGSC_MAX_PATH];
	if (GetRetailLogPath(filePath))
	{
		s_RgscRetailLogFile = _fsopen(filePath, "w+, ccs=UTF-16LE", _SH_DENYWR);
		if (s_RgscRetailLogFile != NULL)
		{
			char szTime[26] = {0};
			time_t testTime = time(0);
			struct tm* local = localtime(&testTime);
			if (local)
			{
				safecpy(szTime, asctime(local));
				szTime[strlen(szTime) - 1] = 0;
				RgscDisplayf("%s", szTime);
			}					

			ProcessMessageQueue();
		}
		else
		{
			s_bLogError = true;
		}
	}
}

void RgscRetailLog::RgscCloseRetailLog()
{
	if (s_RgscRetailLogFile)
	{
		RgscDisplayf("Shutting down...");
		fclose(s_RgscRetailLogFile);
		s_RgscRetailLogFile = NULL;
	}
}

void RgscRetailLog::AddInterval()
{
	RgscDisplayf("");
}

void RgscRetailLog::QueueMessage(const wchar_t* msg)
{
	SYS_CS_SYNC(s_LogToken);
	if (AssertVerify(!s_MsgQueue.IsFull()))
	{
		s_MsgQueue.Push(StringDuplicate(msg));
	}
}

void RgscRetailLog::ProcessMessageQueue()
{
	SYS_CS_SYNC(s_LogToken);
	if (s_RgscRetailLogFile)
	{
		while(s_MsgQueue.GetCount() > 0)
		{
			fwprintf(s_RgscRetailLogFile, L"[%08u] ", GetLogTime());

			wchar_t*& messageW = s_MsgQueue.Pop();
			fwprintf(s_RgscRetailLogFile, L"%s\n", messageW);
			StringFree(messageW);
		}

		fflush(s_RgscRetailLogFile);
	}
}

u32 RgscRetailLog::GetLogTime()
{
	static u32 firstLogTime = sysTimer::GetSystemMsTime();
	u32 thisTimer = sysTimer::GetSystemMsTime() - firstLogTime;
	return thisTimer;
}

void RgscRetailLog::RgscErrorf(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	RgscRetailMsg(false, IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_ERROR, fmt, args);
	va_end(args);
}

void RgscRetailLog::RgscWarningf(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	RgscRetailMsg(false, IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_WARNING, fmt, args);
	va_end(args);
}

void RgscRetailLog::RgscDisplayf(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	RgscRetailMsg(false, IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_INFO, fmt, args);
	va_end(args);
}

void RgscRetailLog::RgscErrorWf(const wchar_t* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	RgscRetailMsgW(IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_ERROR, fmt, args);
	va_end(args);
}

void RgscRetailLog::RgscWarningWf(const wchar_t* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	RgscRetailMsgW(IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_WARNING, fmt, args);
	va_end(args);
}

void RgscRetailLog::RgscDisplayWf(const wchar_t* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	RgscRetailMsgW(IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_INFO, fmt, args);
	va_end(args);
}

void RgscRetailLog::RgscErrorUtf8f(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	RgscRetailMsg(true, IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_ERROR, fmt, args);
	va_end(args);
}

void RgscRetailLog::RgscWarningUtf8f(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	RgscRetailMsg(true, IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_WARNING, fmt, args);
	va_end(args);
}

void RgscRetailLog::RgscDisplayUtf8f(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	RgscRetailMsg(true, IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_INFO, fmt, args);
	va_end(args);
}

void RgscRetailLog::RgscRetailMsg(bool utf8, int severity, const char* fmt, va_list args)
{
	if (s_bLogError)
		return;

	IRgscDelegateV1::OutputSeverity sev = (IRgscDelegateV1::OutputSeverity)severity;

	char message[1024];
	char* msg = &message[0];

	bool hasFmt = (fmt != NULL) && (fmt[0] != '\0');
	if (hasFmt)
	{
		_vsnprintf_s(msg, COUNTOF(message), _TRUNCATE, fmt, args);
	}
	else
	{
		message[0] = '\0';
	}

	SYS_CS_SYNC(s_LogToken);

	wchar_t messageW[sizeof(message)] = {0};
	MultiByteToWideChar(utf8 ? CP_UTF8 : CP_ACP, NULL, message, -1, messageW, COUNTOF(messageW));

	// Allow for a few messages to queue up at startup until we realize if we're a launcher and open the log file
	if (s_RgscRetailLogFile == NULL)
	{
		QueueMessage(messageW);
		return;
	}

	fwprintf(s_RgscRetailLogFile, L"[%08u] ", GetLogTime());

	switch(sev)
	{
	case IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_ASSERT:
	case IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_ERROR:
		fwprintf(s_RgscRetailLogFile, L"[ERROR] ");
		break;
	case IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_WARNING:
		fwprintf(s_RgscRetailLogFile, L"[WARNING] ");
		break;
	case IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_INFO:
	default:
		break;
	}

	fwprintf(s_RgscRetailLogFile, L"%ls\n", messageW);
	fflush(s_RgscRetailLogFile);
}

void RgscRetailLog::RgscRetailMsgW(int severity, const wchar_t* fmt, va_list args)
{
	if (s_bLogError)
		return;

	IRgscDelegateV1::OutputSeverity sev = (IRgscDelegateV1::OutputSeverity)severity;

	if (s_RgscRetailLogFile == NULL)
	{
		return;
	}

	wchar_t message[1024];
	wchar_t* msg = &message[0];

	bool hasFmt = (fmt != NULL) && (fmt[0] != L'\0');
	if (hasFmt)
	{
		_vsnwprintf_s(msg, COUNTOF(message), _TRUNCATE, fmt, args);
	}
	else
	{
		message[0] = L'\0';
	}

	SYS_CS_SYNC(s_LogToken);

	fwprintf(s_RgscRetailLogFile, L"[%08u] ", GetLogTime());

	switch(sev)
	{
	case IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_ASSERT:
	case IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_ERROR:
		fwprintf(s_RgscRetailLogFile, L"[ERROR] ");
		break;
	case IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_WARNING:
		fwprintf(s_RgscRetailLogFile, L"[WARNING] ");
		break;
	case IRgscDelegateV1::RGSC_OUTPUT_SEVERITY_INFO:
	default:
		break;
	}

	fwprintf(s_RgscRetailLogFile, L"%ls\n", message);
	fflush(s_RgscRetailLogFile);
}

} // namespace rgsc
