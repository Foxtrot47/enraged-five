﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace texture2c
{
    class Program
    {
        static void PrintUsage()
        {
            Console.WriteLine("texture2c.exe [path to png]");
        }

        static int Main(string[] args)
        {
            // Ensure the first argument is a valid file
            if (args.Length != 1 || !File.Exists(args[0]))
            {
                PrintUsage();
                return -1;
            }

            try
            {
                string path = args[0];

                // load the file as a bitmap - rotate 90 degrees
                Bitmap bm = new Bitmap(path);
                bm.RotateFlip(RotateFlipType.Rotate90FlipNone);

                // start a string that will output the int[] array
                string result = "static int sm_Texture[] = {" + Environment.NewLine;

                // using the image width/height, output the pixel data
                int width = bm.Width;
                int height = bm.Height;
                for (int i = 0; i < width; i++)
                {
                    result += "\t";

                    for (int j = 0; j < height; j++)
                    {
                        // get the current pixel using i=column,j=row
                        Color c = bm.GetPixel(i, j);

                        // print the pixel data
                        int rbga = (c.A << 24) | (c.B << 16) | (c.G << 8) | c.R;
                        result += rbga;

                        if (i != height && j != width)
                            result += ",";
                    }

                    // append newline at the end of each row
                    result += Environment.NewLine;
                }

                // close the array initialization
                result += "};";

                // using the file name, remove the image extension and replace with .h
                string outFile = Path.GetDirectoryName(path) + "\\" + Path.GetFileNameWithoutExtension(path) + ".h";
                File.WriteAllText(outFile, result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }

            Console.ReadKey();
            return 0;
        }
    }
}
