@echo off

echo Make sure you have built all of the following:
echo 1. rgsc debug win32
echo 2. rgsc final win32
echo 3. rgsc debug x64
echo 4. rgsc final x64
echo 5. sample_session debug win32
echo 6. sample_session final win32
echo 7. sample_session debug x64
echo 8. sample_session final x64
pause

@echo on

set OPTIONS=/D /F /R /Y
set DATELESS_OPTIONS=/F /R /Y
set DESKTOP_DIR=%USERPROFILE%\Desktop

set RGSC_RAGE_DIR=%RAGE_DIR%

set SAMPLE_DBG_SESSION_DIR=%RGSC_RAGE_DIR%\suite\samples\sample_snet\win32_debug_2012
set SAMPLE_SESSION_DIR=%RGSC_RAGE_DIR%\suite\samples\sample_snet\win32_final_2012
set SAMPLE_DBG_SESSION_64_DIR=%RGSC_RAGE_DIR%\suite\samples\sample_snet\win64_debug_2012
set SAMPLE_SESSION_64_DIR=%RGSC_RAGE_DIR%\suite\samples\sample_snet\win64_final_2012
set SHADOWLIBDLL=%RGSC_RAGE_DIR%\3rdparty\NVidia\GFSDK_ShadowLib\bin\GFSDK_ShadowLib.win64.dll
set ALPHARESOLVEDLL=%RGSC_RAGE_DIR%\3rdparty\NVidia\NVTXAA\lib\GFSDK_TXAA_AlphaResolve.win64.dll

set GFSDKTXAADLL=%RGSC_RAGE_DIR%\3rdparty\NVidia\NVTXAA\lib\GFSDK_TXAA.win64.dll
set UNINSTALLER_EXE=%RGSC_RAGE_DIR%\suite\src\rgsc\installer\uninstallRGSCRedistributable.exe
set UNINSTALLER_EXE_DEBUG=%RGSC_RAGE_DIR%\suite\src\rgsc\installer\uninstallRGSCRedistributableDebug.exe

set SIGN_TOOL_ROOT=%ProgramFiles(x86)%\Windows Kits\8.0\bin\x86\

set METADATA_DIR=%RGSC_RAGE_DIR%\suite\samples\sample_snet\pc
set SIGNING_KEY=%RGSC_RAGE_DIR%\suite\src\rgsc\rockstar.pfx

set RGSC_DBG_DIR=%PROGRAMFILES(X86)%\Rockstar Games\Social Club Debug
set RGSC_DIR=%PROGRAMFILES(X86)%\Rockstar Games\Social Club
set RGSC_DBG_DIR_X64=%ProgramW6432%\Rockstar Games\Social Club Debug
set RGSC_DIR_X64=%ProgramW6432%\Rockstar Games\Social Club

set RGSC_DBG_UI_DIR=%RGSC_RAGE_DIR%\suite\src\rgsc\UI
set RGSC_UI_DIR=%RGSC_RAGE_DIR%\suite\src\rgsc\UI

set RGSC_TOOLS_ROOT=%RS_TOOLSROOT%
set RGSC_PROTECTION_ROOT=%RGSC_TOOLS_ROOT%\script\coding\protection

set PUBLIC_INTERFACE_HEADERS=%RGSC_RAGE_DIR%\suite\src\rgsc\rgsc\rgsc\public_interface

set DEST_DIR_ROOT=%DESKTOP_DIR%\Rockstar Games
set DEST_DIR_SDK=%DEST_DIR_ROOT%\Social Club SDK
set DEST_DIR_SDK_SAMPLES=%DEST_DIR_SDK%\samples
set DEST_DIR_SDK_DOCUMENTATION=%DEST_DIR_SDK%\documentation
set DEST_DIR_SDK_INCLUDES=%DEST_DIR_SDK%\include
set DEST_DIR_SDK_GUARDIFACTS=%DEST_DIR_SDK%\guardifacts

set DEST_DBG_DIR_TEST_APP=%DEST_DIR_SDK_SAMPLES%\Debug\
set DEST_DIR_TEST_APP=%DEST_DIR_SDK_SAMPLES%\Release\
set DEST_DBG_DIR_TEST_APP_64=%DEST_DIR_SDK_SAMPLES%\x64\Debug\
set DEST_DIR_TEST_APP_64=%DEST_DIR_SDK_SAMPLES%\x64\Release\

set DEST_DBG_DIR_RGSC=%DEST_DIR_ROOT%\x86\Social Club Debug\
set DEST_DIR_RGSC=%DEST_DIR_ROOT%\x86\Social Club\
set DEST_DBG_DIR_RGSC_X64=%DEST_DIR_ROOT%\x64\Social Club Debug\
set DEST_DIR_RGSC_X64=%DEST_DIR_ROOT%\x64\Social Club\

set DEST_DBG_DIR_RGSC_UI=%DEST_DBG_DIR_RGSC%\UI\
set DEST_DIR_RGSC_UI=%DEST_DIR_RGSC%\UI\
set DEST_DBG_DIR_RGSC_UI_X64=%DEST_DBG_DIR_RGSC_X64%\UI\
set DEST_DIR_RGSC_UI_X64=%DEST_DIR_RGSC_X64%\UI\

mkdir "%DEST_DIR_ROOT%"
cd /D "%DEST_DIR_ROOT%"

xcopy "%SAMPLE_DBG_SESSION_DIR%\sample_session_win32_debug.exe" "%DEST_DBG_DIR_TEST_APP%" %OPTIONS%
xcopy "%SAMPLE_DBG_SESSION_DIR%\sample_session_win32_debug.pdb" "%DEST_DBG_DIR_TEST_APP%" %OPTIONS%
xcopy "%SAMPLE_DBG_SESSION_DIR%\sample_session_win32_debug.map" "%DEST_DBG_DIR_TEST_APP%" %OPTIONS%
xcopy "%SAMPLE_DBG_SESSION_64_DIR%\sample_session_win64_debug.exe" "%DEST_DBG_DIR_TEST_APP_64%" %OPTIONS%
xcopy "%SAMPLE_DBG_SESSION_64_DIR%\sample_session_win64_debug.pdb" "%DEST_DBG_DIR_TEST_APP_64%" %OPTIONS%
xcopy "%SAMPLE_DBG_SESSION_64_DIR%\sample_session_win64_debug.map" "%DEST_DBG_DIR_TEST_APP_64%" %OPTIONS%
xcopy "%SHADOWLIBDLL%" "%DEST_DBG_DIR_TEST_APP_64%" %OPTIONS%
xcopy "%GFSDKTXAADLL%" "%DEST_DBG_DIR_TEST_APP_64%" %OPTIONS%
xcopy "%ALPHARESOLVEDLL%" "%DEST_DBG_DIR_TEST_APP_64%" %OPTIONS%

xcopy "%METADATA_DIR%\metadata.dat" "%DEST_DBG_DIR_TEST_APP%" %OPTIONS%
xcopy "%METADATA_DIR%\metadata.dat" "%DEST_DBG_DIR_TEST_APP_64%" %OPTIONS%

xcopy "%SAMPLE_SESSION_DIR%\sample_session_win32_final_2008.exe" "%DEST_DIR_TEST_APP%" %OPTIONS%
xcopy "%SAMPLE_SESSION_DIR%\sample_session_win32_final_2008.pdb" "%DEST_DIR_TEST_APP%" %OPTIONS%
xcopy "%SAMPLE_SESSION_DIR%\sample_session_win32_final_2008.map" "%DEST_DIR_TEST_APP%" %OPTIONS%
xcopy "%SAMPLE_SESSION_64_DIR%\sample_session_win64_final_2008.exe" "%DEST_DIR_TEST_APP_64%" %OPTIONS%
xcopy "%SAMPLE_SESSION_64_DIR%\sample_session_win64_final_2008.pdb" "%DEST_DIR_TEST_APP_64%" %OPTIONS%
xcopy "%SAMPLE_SESSION_64_DIR%\sample_session_win64_final_2008.map" "%DEST_DIR_TEST_APP_64%" %OPTIONS%

xcopy "%METADATA_DIR%\metadata.dat" "%DEST_DIR_TEST_APP%" %OPTIONS%
xcopy "%METADATA_DIR%\metadata.dat" "%DEST_DIR_TEST_APP_64%" %OPTIONS%

xcopy "%PUBLIC_INTERFACE_HEADERS%" "%DEST_DIR_SDK_INCLUDES%" %OPTIONS% /I

REM Running RTTI Script; this will copy the original binary to socialclub64.unpatchedstrings.dll
REM REM REM ruby %RGSC_PROTECTION_ROOT%\rttihandling\parseForStrings.rb --path "%RGSC_DBG_DIR_X64%\socialclub.dll"
ruby %RGSC_PROTECTION_ROOT%\rttihandling\parseForStrings.rb --path "%RGSC_DIR_X64%\socialclub.dll"

REM now we need to resign the DLL it just modified
REM REM REM "%SIGN_TOOL_ROOT%\signtool.exe" sign /f "%SIGNING_KEY%" /p Abcd1234% /t http://timestamp.verisign.com/scripts/timstamp.dll /v "%RGSC_DBG_DIR_X64%\socialclub.dll"
"%SIGN_TOOL_ROOT%\signtool.exe" sign /f "%SIGNING_KEY%" /p Abcd1234% /t http://timestamp.verisign.com/scripts/timstamp.dll /v "%RGSC_DIR_X64%\socialclub.dll"
pause

REM Rockstar Games\x86\Social Club Debug\
xcopy "%RGSC_DBG_DIR%\d3dcompiler_43.dll" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\d3dcompiler_47.dll" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\ffmpegsumo.dll" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\icudtl.dat" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\cef.pak" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\cef_100_percent.pak" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\cef_200_percent.pak" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\locales" "%DEST_DBG_DIR_RGSC%\locales\" %OPTIONS% /E

xcopy "%RGSC_DBG_DIR%\libcef.dll" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\libEGL.dll" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\libGLESv2.dll" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\socialclub.dll" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\socialclub.pdb" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\socialclub.map" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\SocialClubHelper.exe" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\SocialClubHelper.pdb" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DBG_DIR%\SocialClubHelper.map" "%DEST_DBG_DIR_RGSC%" %OPTIONS%
xcopy "%UNINSTALLER_EXE_DEBUG%" "%DEST_DBG_DIR_RGSC%" %OPTIONS%

xcopy "%RGSC_DBG_UI_DIR%" "%DEST_DBG_DIR_RGSC_UI%" %OPTIONS% /E

REM Rockstar Games\x64\Social Club Debug\
xcopy "%RGSC_DBG_DIR_X64%\d3dcompiler_43.dll" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\d3dcompiler_47.dll" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\ffmpegsumo.dll" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\icudtl.dat" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\cef.pak" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\cef_100_percent.pak" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\cef_200_percent.pak" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\locales" "%DEST_DBG_DIR_RGSC_X64%\locales\" %OPTIONS% /E

xcopy "%RGSC_DBG_DIR_X64%\libcef.dll" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\libEGL.dll" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\libGLESv2.dll" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\socialclub.dll" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\socialclub.pdb" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\socialclub.map" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\SocialClubHelper.exe" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\SocialClubHelper.pdb" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_DIR_X64%\SocialClubHelper.map" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%UNINSTALLER_EXE_DEBUG%" "%DEST_DBG_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DBG_UI_DIR%" "%DEST_DBG_DIR_RGSC_UI_X64%" %OPTIONS% /E

REM Rockstar Games\x86\Social Club\
xcopy "%RGSC_DIR%\d3dcompiler_43.dll" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR%\d3dcompiler_47.dll" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR%\ffmpegsumo.dll" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR%\icudtl.dat" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR%\cef.pak" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR%\cef_100_percent.pak" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR%\cef_200_percent.pak" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR%\locales" "%DEST_DIR_RGSC%\locales\" %OPTIONS% /E

xcopy "%RGSC_DIR%\libcef.dll" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR%\libEGL.dll" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR%\libGLESv2.dll" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR%\socialclub.dll" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR%\SocialClubHelper.exe" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%UNINSTALLER_EXE%" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_UI_DIR%" "%DEST_DIR_RGSC_UI%" %OPTIONS% /E

REM Rockstar Games\x64\Social Club\
xcopy "%RGSC_DIR_X64%\d3dcompiler_43.dll" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DIR_X64%\d3dcompiler_47.dll" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DIR_X64%\ffmpegsumo.dll" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DIR_X64%\icudtl.dat" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DIR_X64%\cef.pak" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DIR_X64%\cef_100_percent.pak" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DIR_X64%\cef_200_percent.pak" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DIR_X64%\locales" "%DEST_DIR_RGSC_X64%\locales\" %OPTIONS% /E

xcopy "%RGSC_DIR_X64%\libcef.dll" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DIR_X64%\libEGL.dll" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DIR_X64%\libGLESv2.dll" "%DEST_DIR_RGSC_X64%" %OPTIONS%
REM xcopy "%RGSC_DIR_X64%\socialclub.dll" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DIR_X64%\SocialClubHelper.exe" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%UNINSTALLER_EXE%" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_UI_DIR%" "%DEST_DIR_RGSC_UI_X64%" %OPTIONS% /E

pause
REM Running GuardIT
ruby %RGSC_PROTECTION_ROOT%\ProtectIT.rb -t -r release -g %RGSC_PROTECTION_ROOT%\rgsc.gsml -c -i "%RGSC_DIR_X64%\socialclub.dll"
ruby %RGSC_PROTECTION_ROOT%\artifactCollector\artifactCollector.rb -s "%DEST_DIR_SDK_GUARDIFACTS%"

pause
REM Sign all EXEs and DLLs
cd /D %DEST_DIR_RGSC%
for /f "tokens=*" %%a IN ('dir /b *.exe') do "%SIGN_TOOL_ROOT%\signtool.exe" sign /f "%SIGNING_KEY%" /p uci5eNJRxv! /t http://timestamp.verisign.com/scripts/timstamp.dll /v "%%a"
for /f "tokens=*" %%a IN ('dir /b *.dll') do "%SIGN_TOOL_ROOT%\signtool.exe" sign /f "%SIGNING_KEY%" /p uci5eNJRxv! /t http://timestamp.verisign.com/scripts/timstamp.dll /v "%%a"

cd /D %DEST_DIR_RGSC_X64%
for /f "tokens=*" %%a IN ('dir /b *.exe') do "%SIGN_TOOL_ROOT%\signtool.exe" sign /f "%SIGNING_KEY%" /p uci5eNJRxv! /t http://timestamp.verisign.com/scripts/timstamp.dll /v "%%a"
for /f "tokens=*" %%a IN ('dir /b *.dll') do "%SIGN_TOOL_ROOT%\signtool.exe" sign /f "%SIGNING_KEY%" /p uci5eNJRxv! /t http://timestamp.verisign.com/scripts/timstamp.dll /v "%%a"
pause

REM Build the documentation
mkdir "%DEST_DIR_SDK_DOCUMENTATION%"
cd /D "%RGSC_RAGE_DIR%\suite\src\rgsc\documentation"
(type "%RGSC_RAGE_DIR%\suite\src\rgsc\documentation\Doxyfile" && echo OUTPUT_DIRECTORY = "%RGSC_RAGE_DIR%\suite\src\rgsc\documentation" && echo HHC_LOCATION = "%PROGRAMFILES(X86)%\HTML Help Workshop\hhc.exe" && echo CHM_FILE = "%DEST_DIR_SDK_DOCUMENTATION%\social club.chm") | doxygen -

pause

REM Build the Social Club Installer
REM Note: make sure there are no trailing slashes at the end of the arguments to BuildRGSC.exe or it won't work.
cd /D "%RGSC_RAGE_DIR%\suite\src\rgsc\installer"
"%RGSC_RAGE_DIR%\suite\src\rgsc\installer\BuildRGSC.exe" "%PROGRAMFILES(X86)%\\NSIS\\Unicode\\MakeNSIS.exe" "%DEST_DIR_ROOT%\x86\Social Club" "%DEST_DIR_ROOT%\x64\Social Club" "%DEST_DIR_SDK%\installer"

pause
REM Sign the Social Club Installer
cd /D "%DEST_DIR_SDK%\installer"
for /f "tokens=*" %%a IN ('dir /b *.exe') do "%SIGN_TOOL_ROOT%\signtool.exe" sign /f "%SIGNING_KEY%" /p uci5eNJRxv! /t http://timestamp.verisign.com/scripts/timstamp.dll /v "%%a"
pause


REM make sure to copy the pdbs to the release directory *after* building the installer so it doesn't get included as part of the installer!
xcopy "%RGSC_DIR%\socialclub.pdb" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR%\socialclub.map" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR%\SocialClubHelper.pdb" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR%\SocialClubHelper.map" "%DEST_DIR_RGSC%" %OPTIONS%
xcopy "%RGSC_DIR_X64%\socialclub.pdb" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DIR_X64%\socialclub.map" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DIR_X64%\SocialClubHelper.pdb" "%DEST_DIR_RGSC_X64%" %OPTIONS%
xcopy "%RGSC_DIR_X64%\SocialClubHelper.map" "%DEST_DIR_RGSC_X64%" %OPTIONS%

REM Copy over the original binaries after the installer's been made so that developers can run the game/apps in debug mode
REM REM REM xcopy "%RGSC_DBG_DIR_X64%\socialclub.dll" "%DEST_DBG_DIR_RGSC_X64%" %DATELESS_OPTIONS%
xcopy "%RGSC_DIR_X64%\socialclub.dll" "%DEST_DIR_RGSC_X64%" %DATELESS_OPTIONS%
:End
pause
