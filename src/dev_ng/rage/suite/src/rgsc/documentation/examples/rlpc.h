// 
// rline/rlpc.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLINE_RLPC_H
#define RLINE_RLPC_H

#include "file/file_config.h"

#if __GAMESPYPC

// #include <rgsc_interface.h>
// #include <achievements_interface.h>
// #include <profiles_interface.h>
// #include <delegate_interface.h>
// #include <titleid_interface.h>
// #include <rgsc_ui_interface.h>

// TODO: NS - figure out pathing to RGSC_SDK public interface headers
#include "../../../suite/src/rgsc/rgsc/rgsc/public_interface/rgsc_common.h"
#include "../../../suite/src/rgsc/rgsc/rgsc/public_interface/rgsc_interface.h"
#include "../../../suite/src/rgsc/rgsc/rgsc/public_interface/achievements_interface.h"
#include "../../../suite/src/rgsc/rgsc/rgsc/public_interface/profiles_interface.h"
#include "../../../suite/src/rgsc/rgsc/rgsc/public_interface/delegate_interface.h"
#include "../../../suite/src/rgsc/rgsc/rgsc/public_interface/titleid_interface.h"
#include "../../../suite/src/rgsc/rgsc/rgsc/public_interface/file_system_interface.h"
#include "../../../suite/src/rgsc/rgsc/rgsc/public_interface/rgsc_ui_interface.h"
#include "../../../suite/src/rgsc/rgsc/rgsc/public_interface/activation_interface.h"
#include "../../../suite/src/rgsc/rgsc/rgsc/public_interface/patching_interface.h"

#include "rline/rlgamerinfo.h"

namespace rgsc
{
	class IAchievementManager;
	class IProfileManager;
	class IFileSystem;
	class IRgscUi;
}

namespace rage
{

/* This class presents the interface to the external PC Platform DLL.
   It acts as the glue that binds Rage to the DLL and vice versa.
*/
class rlPc
{
public:

    rlPc();
    ~rlPc();

    bool Init();
    void Shutdown();
    void Update();

	bool IsInitialized();

	rgsc::IAchievementManagerLatestVersion* GetAchievementManager();
	rgsc::IProfileManagerLatestVersion* GetProfileManager();
	rgsc::IFileSystemLatestVersion* GetFileSystem();
	rgsc::IRgscUiLatestVersion* GetUiInterface();
	rgsc::IActivationLatestVersion* GetActivationInterface();
	rgsc::IPatchingLatestVersion* GetPatchingInterface();

	//PURPOSE
	//  Show the sign-in UI.
	bool ShowSigninUi();

	//PURPOSE
	//  Shows the gamer profile UI (gamer card).
	//PARAMS
	//  target              - Gamer handle of gamer who's profile will be shown.
	bool ShowGamerProfileUi(const rlGamerHandle& target);

	//PURPOSE
	//  Returns true if a system UI is showing.
	bool IsUiShowing() const;

	//PURPOSE
	//  Returns true if the UI is loaded and ready to accept commands.
	bool IsUiAcceptingCommands();

	void SetPatchingCommandLine(const char* commandLine);

private:
	char m_PatchingCommandLine[8191 + 1];

#if !__FINAL && !__RGSC_DLL
	u32 GetDllVersion(const char* fileName);
	bool InstallDlls();
#endif
	bool LoadDll();
	void UpdateSocialClubDll();
};

//Global instance.  
extern rlPc g_rlPc;

} // namespace rage

#endif //__GAMESPYPC

#endif // RLINE_RLPC_H
