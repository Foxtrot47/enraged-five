// 
// rline/rlpc.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "rlpc.h"

#if __GAMESPYPC

#include "diag/seh.h"
#include "file/device.h"
#include "grcore/device.h"
#include "input/mouse.h"
#include "rline/rltitleid.h"
#include "ros/rlros.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <ShlObj.h>
#include <d3dx11.h>
#pragma warning(pop)

using namespace rgsc;

#if !__FINAL && !__RGSC_DLL
#pragma comment(lib, "version.lib") 
#endif

#if !__RGSC_DLL

// If 1, we implicitly link the DLL using a .lib file.
// If 0, we explicitly link via LoadLibrary().
// NOTE: Implicit requires you to have your /Program Files(x86)/Rockstar Games/Social Club/ directory added to your PATH environment variable.
#define __RGSC_DLL_IMPLICIT_LINKING 0

#if __RGSC_DLL_IMPLICIT_LINKING
#if __DEV
#pragma comment(lib, "../../../suite/src/rgsc/rgsc/debug/socialclub.lib")
#else
#pragma comment(lib, "../../../suite/src/rgsc/rgsc/release/socialclub.lib")
#endif // __DEV
#endif // __RGSC_DLL_IMPLICIT_LINKING
#endif // !__RGSC_DLL

static IRgscLatestVersion* m_Rgsc = NULL;
static IProfileManagerLatestVersion* m_ProfileManager = NULL;
static IAchievementManagerLatestVersion* m_AchievementManager = NULL;
static IActivationLatestVersion* m_ActivationSystem = NULL;
static IPatchingLatestVersion* m_PatchingSystem = NULL;
static IFileSystemLatestVersion* m_FileSystem = NULL;
static IRgscUiLatestVersion* m_RgscUi = NULL;
static HMODULE m_hRgscDll = NULL;

namespace rage
{

PARAM(noSocialClub, "Disables loading of the Social Club DLL.");

RAGE_DEFINE_SUBCHANNEL(rline, pc);
#undef __rage_channel
#define __rage_channel rline_pc

rlPc g_rlPc;

extern const rlTitleId* g_rlTitleId;

class RgscDelegate : public IRgscDelegateLatestVersion
{
public:
	//IUnknown
	HRESULT RGSC_CALL QueryInterface(REFIID riid, void** ppvObject);
	void RGSC_CALL Output(OutputSeverity severity, const char* msg);
	bool RGSC_CALL GetStatsData(char** data);
	void RGSC_CALL FreeStatsData(const char* data);
	void RGSC_CALL SetTextBoxHasFocus(const bool hasFocus);
	void RGSC_CALL UpdateSocialClubDll(const char* commandLine);
};

static RgscDelegate m_Delegate;

HRESULT RgscDelegate::QueryInterface(REFIID riid, LPVOID* ppvObj)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObj == NULL)
	{
		return E_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IRgscDelegate*>(this);
	}
	else if(riid == IID_IRgscDelegateV1)
	{
		pUnknown = static_cast<IRgscDelegateV1*>(this);
	}

	*ppvObj = pUnknown;
	if(pUnknown == NULL)
	{
		return E_NOINTERFACE;
	}

	return S_OK;
}

void
RgscDelegate::Output(RgscDelegate::OutputSeverity severity, const char* msg)
{
	if(severity == RGSC_OUTPUT_SEVERITY_ASSERT)
	{
		Assertf(false, "%s", msg);
	}
	else if(severity == RGSC_OUTPUT_SEVERITY_ERROR)
	{
		rlError("%s", msg);
	}
	else if(severity == RGSC_OUTPUT_SEVERITY_WARNING)
	{
		rlWarning("%s", msg);
	}
	else if(severity == RGSC_OUTPUT_SEVERITY_INFO)
	{
		rlDebug3("%s", msg);
	}
}

bool 
RgscDelegate::GetStatsData(char** data)
{
	if(data == NULL)
	{
		return false;
	}

	*data = rage_new char[1024];
	safecpy(*data, "coins collected=50, enemies killed=18", 1024);
	
	return true;
}

void 
RgscDelegate::FreeStatsData(const char* data)
{
	delete data;
}

void 
RgscDelegate::SetTextBoxHasFocus(const bool hasFocus)
{
	// show or hide the OnLive virtual keyboard
	if(hasFocus)
	{
		printf("Show OnLive Virtual Keyboard");
	}
	else
	{
		printf("Hide OnLive Virtual Keyboard");
	}

	fflush(stdout);
}

void 
RgscDelegate::UpdateSocialClubDll(const char* commandLine)
{
// 	printf("RgscDelegate::UpdateSocialClubDll(): %s", commandLine);
// 	fflush(stdout);

	g_rlPc.SetPatchingCommandLine(commandLine);
}

rlPc::rlPc()
{

}

rlPc::~rlPc()
{

}

void rlPc::SetPatchingCommandLine(const char* commandLine)
{
	safecpy(m_PatchingCommandLine, commandLine);
}

void RgscPresentCallback(grcDeviceHandle *device,
						 const tagRECT *pSourceRect,
						 const tagRECT *pDestRect,
						 HWND__ *hwndOverride,
						 const tagRGNDATA *dirtyRegion)
{
	if(m_RgscUi)
	{
		m_RgscUi->Render();
	}
}

void RgscDeviceLostCB()
{
	// TODO: NS - why does this function get linked in the DLL?
#if !__RGSC_DLL
	if(m_RgscUi)
	{
		m_RgscUi->OnLostDevice(); // TODO: NS - check return value - what to do on fail?
	}
#endif
}

void RgscDeviceResetCB()
{
	// TODO: NS - why does this function get linked in the DLL?
#if !__RGSC_DLL
	if(m_RgscUi)
	{
		if(GRCDEVICE.GetDxVersion() >= 1000)
		{
			IDXGISwapChain* pSwapChain = (IDXGISwapChain*)GRCDEVICE.GetSwapChain();
			DXGI_SWAP_CHAIN_DESC desc;
			pSwapChain->GetDesc(&desc);
			m_RgscUi->OnResetDevice(&desc); // TODO: NS - check return value - what to do on fail?
		}
		else
		{
			grcPresentParameters *pPresentParams = GRCDEVICE.GetPresentParameters();
			m_RgscUi->OnResetDevice(pPresentParams); // TODO: NS - check return value - what to do on fail?
		}
	}
#endif
}

#if !__FINAL && !__RGSC_DLL
u32 
rlPc::GetDllVersion(const char* fileName)
{
	DWORD handle = 0;
	DWORD size = GetFileVersionInfoSize(fileName, &handle);
	BYTE* versionInfo = rage_new BYTE[size];
	if(!GetFileVersionInfo(fileName, handle, size, versionInfo))
	{
		delete[] versionInfo;
		return 0;
	}

	// we have version information
	UINT len = 0;
	VS_FIXEDFILEINFO* vsfi = NULL;
	VerQueryValue(versionInfo, "\\", (void**)&vsfi, &len);

	int aVersion[4] = {0};
	aVersion[0] = HIWORD(vsfi->dwFileVersionMS);
	aVersion[1] = LOWORD(vsfi->dwFileVersionMS);
	aVersion[2] = HIWORD(vsfi->dwFileVersionLS);
	aVersion[3] = LOWORD(vsfi->dwFileVersionLS);

	delete[] versionInfo;

	// TODO: NS - this only works if we have something like a.b.c.d where a, b, c, and d are 1 digit.
	// Not the best way to do this since we could have version 1.0.1.2813, etc.
	Assert(aVersion[0] >= 0 && aVersion[0] <= 9);
	Assert(aVersion[1] >= 0 && aVersion[1] <= 9);
	Assert(aVersion[2] >= 0 && aVersion[2] <= 9);
	Assert(aVersion[3] >= 0 && aVersion[3] <= 9);

	return aVersion[0] * 1000 + aVersion[1] * 100 + aVersion[2] * 10 + aVersion[3];
}

bool 
rlPc::InstallDlls()
{
	/*
		Cases:
		Local Exists	Dest Exists		Local is higher version		Copy?
		0				0				-							0
		0				0				-							0
		0				1				-							0			
		0				1				-							0
		1				0				-							1			
		1				0				-							1			
		1				1				0							0
		1				1				1							1
	*/

	rtry
	{
		char localPath[RGSC_MAX_PATH] = {0};
		char localDllPath[RGSC_MAX_PATH] = {0};

		GetCurrentDirectory(sizeof(localPath), localPath);

		// TODO: NS - make sure the game doesn't ship with the SDK in this directory
		strcat_s(localPath, "\\pc\\Social Club SDK\\Rockstar Games");
		safecpy(localDllPath, localPath);
		strcat_s(localDllPath, "\\Social Club Debug\\socialclub.dll");
		u32 localDllVersion = GetDllVersion(localDllPath);
		rcheck(localDllVersion != 0, catchall, );

		char destPath[MAX_PATH] = {0};
		char destDllPath[MAX_PATH] = {0};
		rcheck(SHGetSpecialFolderPath(NULL, destPath, CSIDL_PROGRAM_FILES, false), catchall, );
		strcat_s(destPath, "\\Rockstar Games");
		safecpy(destDllPath, destPath);
		strcat_s(destDllPath, "\\Social Club Debug\\socialclub.dll");
		u32 destDllVersion = GetDllVersion(destDllPath);

		if(localDllVersion > destDllVersion)
		{
			char pathToDelete[MAX_PATH] = {0};
			safecpy(pathToDelete, destPath);
			strcat_s(pathToDelete, "\\Social Club");
			fiDevice::DeleteDirectory(pathToDelete, true);

			safecpy(pathToDelete, destPath);
			strcat_s(pathToDelete, "\\Social Club Debug");
			fiDevice::DeleteDirectory(pathToDelete, true);

			safecpy(pathToDelete, destPath);
			strcat_s(pathToDelete, "\\Social Club SDK");
			fiDevice::DeleteDirectory(pathToDelete, true);

			ASSERT_ONLY(bool success = )fiDevice::CopyDirectory(localPath, destPath);
			Assertf(success, "Failed to copy Social Club SDK from '%s' to '%s'", localPath, destPath);
		}
	}
	rcatchall
	{

	}

	return true;
}
#endif

bool 
rlPc::LoadDll()
{
	bool success = false;
#if !__RGSC_DLL
	rtry
	{
#if __RGSC_DLL_IMPLICIT_LINKING == 1
		IRgsc* rgsc = GetRgscInterface();
#else
		char path[MAX_PATH];
		path[0] = '\0';
		rcheck(SHGetSpecialFolderPath(NULL, path, CSIDL_PROGRAM_FILES, false), catchall, );

#if __DEV
		strcat_s(path, "\\Rockstar Games\\Social Club Debug\\socialclub.dll");
#else
		strcat_s(path, "\\Rockstar Games\\Social Club\\socialclub.dll");
#endif

		// TODO: NS - make sure the file exists before attempting to load it, because if it's not there,
		// LoadLibraryEx() will start searching for the DLL in other directories and will eventually
		// look in the current working directory which could contain a hacked DLL.

		// TODO: NS - do security checks on the DLL here (checksums verified with online server? decrypt the dll?)

		typedef IRgsc* (*GetRgscInstancePtr)();
		const unsigned int kFunctionOrdinal = 1;

		m_hRgscDll = LoadLibraryEx(path, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);

		rcheck(m_hRgscDll, catchall, );

		GetRgscInstancePtr func = (GetRgscInstancePtr)GetProcAddress(m_hRgscDll, (LPCSTR)kFunctionOrdinal);

		rcheck(func, catchall, );

		IRgsc* rgsc = func();
		rcheck(rgsc, catchall, );
#endif // __RGSC_DLL_IMPLICIT_LINKING

		Assert(m_Rgsc == NULL);

		// if this fails, then the DLL on the local computer is out of date
		HRESULT hr = rgsc->QueryInterface(IID_IRgscLatestVersion, (void**) &m_Rgsc);
		rverify((hr == S_OK) && m_Rgsc, catchall, );

		// convert language enums
		RgscLanguage rgscLang = RGSC_LANGUAGE_INVALID;

		switch(rlGetLanguage())
		{
			case RL_LANGUAGE_ENGLISH:
				rgscLang = RGSC_LANGUAGE_ENGLISH;
				break;
			case RL_LANGUAGE_SPANISH:
				rgscLang = RGSC_LANGUAGE_SPANISH;
				break;
			case RL_LANGUAGE_FRENCH:
				rgscLang = RGSC_LANGUAGE_FRENCH;
				break;
			case RL_LANGUAGE_GERMAN:
				rgscLang = RGSC_LANGUAGE_GERMAN;
				break;
			case RL_LANGUAGE_ITALIAN:
				rgscLang = RGSC_LANGUAGE_ITALIAN;
				break;
			case RL_LANGUAGE_JAPANESE:
				rgscLang = RGSC_LANGUAGE_JAPANESE;
				break;
			case RL_LANGUAGE_RUSSIAN:
				rgscLang = RGSC_LANGUAGE_RUSSIAN;
				break;
			default:
				rgscLang = RGSC_LANGUAGE_INVALID;
				break;
		}

		rverify(rgscLang != RGSC_LANGUAGE_INVALID, catchall, );

		// convert env enums
		ITitleId::RosEnvironment rgscEnvironment = ITitleId::RLROS_ENV_UNKNOWN;
		switch(g_rlTitleId->m_RosTitleId.GetEnvironment())
		{
			case RLROS_ENV_RAGE:
				rgscEnvironment = ITitleId::RLROS_ENV_RAGE;
				break;
			case RLROS_ENV_DEV:
				rgscEnvironment = ITitleId::RLROS_ENV_DEV;
				break;
			case RLROS_ENV_CERT:
				rgscEnvironment = ITitleId::RLROS_ENV_CERT;
				break;
			case RLROS_ENV_PROD:
				rgscEnvironment = ITitleId::RLROS_ENV_PROD;
				break;
			default:
				rgscEnvironment = ITitleId::RLROS_ENV_UNKNOWN;
				break;
		}

		rverify(rgscEnvironment != RLROS_ENV_UNKNOWN, catchall, );

		rgsc::TitleIdV1 rgscTitleId;
		rgscTitleId.SetRosTitleName(g_rlTitleId->m_RosTitleId.GetTitleName());
		rgscTitleId.SetRosEnvironment(rgscEnvironment);
		rgscTitleId.SetRosTitleVersion(g_rlTitleId->m_RosTitleId.GetTitleVersion());
		rgscTitleId.SetTitleDirectoryName("L.A. Noire");
		rgscTitleId.SetRosSharedSecret("kgOWh5UXuEEIZQPXBpko98VnW5he3LMH+3OokfhzVzY=");

		rverify(m_Rgsc->Init(&rgscTitleId, rgscLang, &m_Delegate) == S_OK, catchall, );

		IProfileManager* profileManager = m_Rgsc->GetProfileManager();
		rverify(profileManager, catchall, );
		Assert(m_ProfileManager == NULL);

		// if this fails, then the DLL on the local computer is out of date
		hr = profileManager->QueryInterface(IID_IProfileManagerLatestVersion, (void**) &m_ProfileManager);
		rverify((hr == S_OK) && m_ProfileManager, catchall, );

		IAchievementManager* achievementManager = m_Rgsc->GetAchievementManager();
		rverify(achievementManager, catchall, );
		Assert(m_AchievementManager == NULL);

		// if this fails, then the DLL on the local computer is out of date
		hr = achievementManager->QueryInterface(IID_IAchievementManagerLatestVersion, (void**) &m_AchievementManager);
		rverify((hr == S_OK) && m_AchievementManager, catchall, );

		IFileSystem* fileSystem = m_Rgsc->GetFileSystem();
		rverify(fileSystem, catchall, );
		Assert(m_FileSystem == NULL);

		// if this fails, then the DLL on the local computer is out of date
		hr = fileSystem->QueryInterface(IID_IFileSystemLatestVersion, (void**) &m_FileSystem);
		rverify((hr == S_OK) && m_FileSystem, catchall, );

		IRgscUi* rgscUi = m_Rgsc->GetUiInterface();
		rverify(rgscUi, catchall, );
		Assert(m_RgscUi == NULL);

		// if this fails, then the DLL on the local computer is out of date
		hr = rgscUi->QueryInterface(IID_IRgscUiLatestVersion, (void**) &m_RgscUi);
		rcheck((hr == S_OK) && m_RgscUi, catchall, );

		IActivation* activationSystem = m_RgscUi->GetActivationSystem();
		rverify(activationSystem, catchall, );
		Assert(m_ActivationSystem == NULL);

		// if this fails, then the DLL on the local computer is out of date
		hr = activationSystem->QueryInterface(IID_IActivationLatestVersion, (void**) &m_ActivationSystem);
		rverify((hr == S_OK) && m_ActivationSystem, catchall, );

		IPatching* patchingSystem = m_RgscUi->GetPatchingSystem();
		rverify(patchingSystem, catchall, );
		Assert(m_PatchingSystem == NULL);

		// if this fails, then the DLL on the local computer is out of date
		hr = patchingSystem->QueryInterface(IID_IPatchingLatestVersion, (void**) &m_PatchingSystem);
		rverify((hr == S_OK) && m_PatchingSystem, catchall, );

		success = true;
	}
	rcatchall
	{
		m_Rgsc = NULL;
		m_ProfileManager = NULL;
		m_AchievementManager = NULL;
		m_ActivationSystem = NULL;
		m_PatchingSystem = NULL;
		m_FileSystem = NULL;
		m_RgscUi = NULL;
		m_hRgscDll = NULL;
	}
#endif

	return success;
}

bool
rlPc::Init()
{
	bool success = false;

	sysMemSet(m_PatchingCommandLine, 0, sizeof(m_PatchingCommandLine));

	// TODO: NS - why does this function get linked in the DLL?
#if !__RGSC_DLL
	rtry
	{
		rcheck(!PARAM_noSocialClub.Get(), noDll, rlWarning("Not loading the Social Club DLL due to command line argument -noSocialClub"));

#if !__FINAL
		InstallDlls();
#endif

		rcheck(LoadDll(), noDll, );
		rcheck(m_Rgsc != NULL, catchall, );

		grcDevice::sm_PresentCallback = RgscPresentCallback;

		Functor0 lostCb	 = MakeFunctor(RgscDeviceLostCB);
		Functor0 resetCb = MakeFunctor(RgscDeviceResetCB);
		GRCDEVICE.RegisterDeviceLostCallbacks(lostCb, resetCb);

		if(GRCDEVICE.GetDxVersion() >= 1000)
		{
			IDXGISwapChain* pSwapChain = (IDXGISwapChain*)GRCDEVICE.GetSwapChain();
			DXGI_SWAP_CHAIN_DESC desc;
			pSwapChain->GetDesc(&desc);

			rcheck(m_RgscUi->OnCreateDevice((IUnknown*)GRCDEVICE.GetCurrent11(), &desc) == S_OK, catchall, );
		}
		else
		{
			grcPresentParameters *pPresentParams = GRCDEVICE.GetPresentParameters();
			rcheck(m_RgscUi->OnCreateDevice((IUnknown*)GRCDEVICE.GetCurrent(),pPresentParams) == S_OK, catchall, );
		}

		success = true;
	}
	rcatch(noDll)
	{
		success = true;
	}
	rcatchall
	{

	}
#endif

	return success;
}

void
rlPc::Shutdown()
{
	if(m_Rgsc)
	{
		m_Rgsc->Shutdown();
		FreeLibrary(m_hRgscDll);
		m_Rgsc = NULL;
		m_RgscUi = NULL;
		m_hRgscDll = NULL;

		m_ProfileManager = NULL;
		m_AchievementManager = NULL;
		m_ActivationSystem = NULL;
		m_PatchingSystem = NULL;
		m_FileSystem = NULL;
	}
}

void
rlPc::Update()
{
	// TODO: NS - this function is not available at the head of Rage. Integrate from MP3.
//	MOUSE.SetExclusive(IsUiShowing() == false);

	if(m_Rgsc)
	{
		m_Rgsc->Update();

		if(m_PatchingCommandLine[0] != '\0')
		{
			UpdateSocialClubDll();
			sysMemSet(m_PatchingCommandLine, 0, sizeof(m_PatchingCommandLine));
		}
	}
}

void 
rlPc::UpdateSocialClubDll()
{
	g_rlPc.Shutdown();

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	if(0 != ::CreateProcess(NULL,
							m_PatchingCommandLine,
							NULL,
							NULL,
							FALSE,
							0,
							NULL,
							NULL,
							&si,
							&pi))
	{
		::WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
	}
	else
	{
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
	}

	g_rlPc.Init();
}

bool 
rlPc::IsInitialized()
{
	return m_Rgsc != NULL;
}

IAchievementManagerLatestVersion* 
rlPc::GetAchievementManager()
{
	return m_AchievementManager;
}

IProfileManagerLatestVersion* 
rlPc::GetProfileManager()
{
	return m_ProfileManager;
}

IFileSystemLatestVersion* 
rlPc::GetFileSystem()
{
	return m_FileSystem;
}

IRgscUiLatestVersion* 
rlPc::GetUiInterface()
{
	return m_RgscUi;
}

IActivationLatestVersion* 
rlPc::GetActivationInterface()
{
	return m_ActivationSystem;
}

IPatchingLatestVersion* 
rlPc::GetPatchingInterface()
{
	return m_PatchingSystem;
}

bool
rlPc::IsUiShowing() const
{
	return m_RgscUi && m_RgscUi->IsVisible();
}

bool
rlPc::ShowSigninUi()
{
	Assert(!IsUiShowing());
	return m_RgscUi && m_RgscUi->ShowSignInUi() == S_OK;
}

bool
rlPc::ShowGamerProfileUi(const rlGamerHandle& target)
{
	Assert(!IsUiShowing());

#if __GAMESPY_NEEDS_REPLACEMENT
	return m_RgscUi && m_RgscUi->ShowGamerProfileUi(target.GetRockstarId()) == S_OK;
#else
	return false;
#endif
}

bool
rlPc::IsUiAcceptingCommands()
{
	return m_RgscUi && m_RgscUi->IsReadyToAcceptCommands();
}

} //namespace rage

#endif
