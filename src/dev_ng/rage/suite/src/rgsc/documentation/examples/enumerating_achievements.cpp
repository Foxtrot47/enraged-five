
bool
rlAchievement::NativeRead()
{
	bool success = true;

	rtry
	{
		rverify(m_ReadCommand, catchall, );

		rlDebug2("Reading achievements...");

		rlGamerHandle gamerHandle = m_ReadCommand->m_GamerInfo.IsValid() ?
									m_ReadCommand->m_GamerInfo.GetGamerHandle() :
									m_ReadCommand->m_GamerHandle;

		rverify(gamerHandle.IsValid(), catchall, );

		rverify(g_rlPc.IsInitialized(), catchall, );

		// convert rage flags to rgsc flags
		DetailFlags flags = m_ReadCommand->m_DetailFlags;
		u32 intFlags = 0;
		if(flags & ACHIEVEMENT_DETAILS_STRINGS)
		{
			intFlags |= rgsc::IAchievement::ACHIEVEMENT_DETAILS_STRINGS;
		}

		if(flags & ACHIEVEMENT_DETAILS_IMAGE)
		{
			intFlags |= rgsc::IAchievement::ACHIEVEMENT_DETAILS_IMAGE;
		}

		if(flags & ACHIEVEMENT_DETAILS_IS_AWARDED)
		{
			intFlags |= rgsc::IAchievement::ACHIEVEMENT_DETAILS_IS_AWARDED;
		}

		if(flags & ACHIEVEMENT_DETAILS_TIMESTAMP)
		{
			intFlags |= rgsc::IAchievement::ACHIEVEMENT_DETAILS_TIMESTAMP;
		}

		rverify(intFlags > 0, catchall, );

		bool extendedInfo = m_ReadCommand->m_Achievements[0].RTTI() > 0;

		rgsc::AchievementId firstAchievementId = (rgsc::AchievementId)m_ReadCommand->m_FirstAchievementId;
		u32 maxAchievementsToRead = m_ReadCommand->m_MaxAchievements;
		u32 numBytesRequired = 0;
		void* handle = NULL;
		g_rlPc.GetAchievementManager()->CreateAchievementEnumerator(rgsc::IID_IAchievementListLatestVersion,
																	NULL,
																	(rgsc::RockstarId) gamerHandle.GetRockstarId(),
																	(rgsc::IAchievement::DetailFlags) intFlags,
																	firstAchievementId,
																	maxAchievementsToRead,
																	&numBytesRequired,
																	&handle);

		void* achievementBuf = RL_ALLOCATE(rlAchievement, numBytesRequired);

		rverify(achievementBuf,
				catchall,
				rlError("Error allocating achievement buffer"));

		rgsc::IAchievementList* listInterface = NULL;
		g_rlPc.GetAchievementManager()->EnumerateAchievements(handle,
															  achievementBuf,
															  numBytesRequired,
															  &listInterface);

		rgsc::IAchievementListLatestVersion* list = (rgsc::IAchievementListLatestVersion*)listInterface;

		*m_ReadCommand->m_NumAchievements = (int) list->GetNumAchievements();

		for(unsigned i = 0; i < list->GetNumAchievements(); i++)
		{
			rgsc::IAchievementLatestVersion* achievement = list->GetAchievement(i);

			if(extendedInfo)
			{
				rlAchievementInfoEx* rageAchievement = &(((rlAchievementInfoEx*)m_ReadCommand->m_Achievements)[i]);
				rlAchievementInfoEx::AchievementType type = ConvertRgscTypeToRageType(achievement->GetType());

				rageAchievement->ResetEx(achievement->GetId(),
										 type,
										 achievement->GetPoints(),
										 achievement->GetLabel(),
										 achievement->GetDescription(),
										 achievement->GetUnachievedString(),
										 NULL,
										 achievement->ShowUnachieved(),
										 achievement->IsAchieved(),
										 achievement->GetAchievedTime());
			}
			else
			{
				rlAchievementInfo* rageAchievement = &m_ReadCommand->m_Achievements[i];

				rageAchievement->Reset(achievement->GetId(),
									   achievement->GetPoints(),
									   achievement->GetLabel(),
									   achievement->GetDescription(),
									   achievement->IsAchieved());
			}
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool
rlAchievement::NativeWrite()
{
	bool success = false;

	rlDebug2("Writing achievement:%d...", m_WriteCommand->m_AchievementId);

	rtry
	{
		rverify(m_WriteCommand, catchall, );

		rverify(m_WriteCommand->m_GamerInfo.IsLocal() && m_WriteCommand->m_GamerInfo.IsSignedIn(), catchall, );

		rverify(g_rlPc.IsInitialized(), catchall, );

		rgsc::AchievementId achievementId = (rgsc::AchievementId) m_WriteCommand->m_AchievementId;

		HRESULT hr = g_rlPc.GetAchievementManager()->WriteAchievement(achievementId);

		success = hr == ERROR_SUCCESS;
	}
	rcatchall
	{

	}

    return success;
}
