  if(g_rlPc.IsInitialized())
  {
	if(g_rlPc.GetProfileManager()->IsSignedIn())
	{
	      rgsc::Profile profile;
	      HRESULT hr = g_rlPc.GetProfileManager()->GetSignInInfo(&profile);
	      if(SUCCEEDED(hr))
	      {
		    const char *nickname = profile.GetNickname();
		    if(AssertVerify(nickname))
		    {
			  const unsigned len = strlen(nickname);
			  if(AssertVerify((len > 0) && (len < nameLen)))
			  {
				safecpy(name, nickname, nameLen);
				success = true;
			  }
		    }
	      }
	}
  }
