#ifndef SUBPROCESS_WINDOW_H
#define SUBPROCESS_WINDOW_H

#include "borderlesswindow.h"
#include "subprocess/subprocess/subprocess.h"
#include "rgsc_common.h"
#include "types.h"
#include "resourceInterceptor.h"

#pragma warning(push)
#pragma warning(disable: 4310)
#include "base/shared_memory.h"
#pragma warning(pop)

// CEF includes
#include "include/cef_base.h"
#include "include/cef_drag_handler.h"
#include "include/cef_client.h"
#include "include/internal/cef_ptr.h"
#include "include/wrapper/cef_resource_manager.h"

namespace IPC
{
	class Message;
}

//class ClientHandler;

#define SUPPORT_WIDGETS 1

namespace rgsc
{

class SubProcessWindow : public CefClient,
						 public CefContextMenuHandler,
						 public CefDisplayHandler,
						 public CefDragHandler,
						 public CefGeolocationHandler,
						 public CefKeyboardHandler,
						 public CefLifeSpanHandler,
						 public CefLoadHandler,
						 public CefRenderHandler,
						 public CefRequestHandler,
						 public CefFocusHandler,
						 public CefJSDialogHandler,
						 public SubProcess::BrowserDelegate
{
public:

	SubProcessWindow(unsigned int windowId, unsigned int width, unsigned int height, float scale, ViewMsg_Tile_Params tiles, std::string sharedMemoryName);
	SubProcessWindow(unsigned int windowId, ViewMsg_Rect_Params rect, ViewMsg_CreateWindow_Params window);

	~SubProcessWindow();

	unsigned int getBufferSizeInBytes();
	unsigned int getPixelBufferSizeInBytes();
	unsigned int getDirtyTileBufferSizeInBytes();

	// messages from the DLL
	void onMouseMovedIpc(__int64 msg, __int64 wParam, __int64 lParam);
	void onMouseButtonIpc(__int64 msg, __int64 wParam, __int64 lParam);
	void onMouseWheelIpc(int xScroll, int yScroll);
	void onKeyEventIpc(__int64 msg, __int64 wParam, __int64 lParam, __int64 scModifiers);
	bool onNavigateToIpc(std::string url, std::string offlineRoot, std::string zipFile);
	void onAddBindOnStartLoadingIpc(std::wstring functionName, bool synchronous);
	void onExecuteJavascriptIpc(std::wstring javascript);

	// IPC
	virtual void onLostDeviceIpc();
	virtual void onResizeIpc(unsigned int width, unsigned int height, float scale, ViewMsg_Tile_Params tiles, std::string sharedMemoryName);
	virtual void onAddCaptionExclusionIpc(unsigned int x, unsigned int y, unsigned int width, unsigned int height, unsigned int referencePoint);
	virtual void onClearCaptionExclusionIpc();
	virtual void onCloseBrowserIpc();
	virtual void onResizeOrMoveIpc(ViewMsg_Rect_Params rect);
	virtual void onModalChangedIpc(bool bIsModal);
	virtual void onSetWindowCanCloseIpc(bool bCanClose);
	virtual void onSetWindowModalBlockerIpc(__int64 modalBlocker);
	virtual void onMouseInCaptionChangedIpc(bool isMouseInCaptionRegion);
	virtual void onSetFocusIpc(bool bEnabled);
	virtual void onCustomWindowMessage(__int64 msg, __int64 wParam, __int64 lParam);

	// IPC Send/Receive
	virtual void Send(IPC::Message* message);
	virtual void OnMessageReceived(const IPC::Message& message);

	// BrowserDelegate
	virtual void OnContextInitialized(CefRefPtr<SubProcess> app);
	virtual void OnBeforeChildProcessLaunch(CefRefPtr<SubProcess> app, CefRefPtr<CefCommandLine> command_line);
	virtual void OnRenderProcessThreadCreated(CefRefPtr<SubProcess> app, CefRefPtr<CefListValue> extra_info);

	// CEF delegate callbacks
	// Interface implemented to handle off-screen rendering.
// 	class RenderHandler : public CefRenderHandler {
// 	public:
// 		virtual void OnBeforeClose(CefRefPtr<CefBrowser> browser) =0;
// 	};

	//typedef std::set<CefMessageRouterBrowserSide::Handler*> MessageHandlerSet;

	// CefClient methods
	virtual CefRefPtr<CefContextMenuHandler> GetContextMenuHandler() OVERRIDE {return this;}
	virtual CefRefPtr<CefDisplayHandler> GetDisplayHandler() OVERRIDE {return this;}
	virtual CefRefPtr<CefDragHandler> GetDragHandler() OVERRIDE {return this;}
	virtual CefRefPtr<CefGeolocationHandler> GetGeolocationHandler() OVERRIDE {return this;}
	virtual CefRefPtr<CefKeyboardHandler> GetKeyboardHandler() OVERRIDE {return this;}
	virtual CefRefPtr<CefLifeSpanHandler> GetLifeSpanHandler() OVERRIDE {return this;}
	virtual CefRefPtr<CefLoadHandler> GetLoadHandler() OVERRIDE {return this;}
	virtual CefRefPtr<CefRenderHandler> GetRenderHandler() OVERRIDE {return this;}
	virtual CefRefPtr<CefRequestHandler> GetRequestHandler() OVERRIDE {return this;}
	virtual CefRefPtr<CefJSDialogHandler> GetJSDialogHandler() OVERRIDE {return this;}

// 	virtual bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser,
// 										CefProcessId source_process,
// 										CefRefPtr<CefProcessMessage> message)
// 										OVERRIDE;

	// CefContextMenuHandler methods
 	virtual void OnBeforeContextMenu(CefRefPtr<CefBrowser> browser,
 									CefRefPtr<CefFrame> frame,
 									CefRefPtr<CefContextMenuParams> params,
 									CefRefPtr<CefMenuModel> model) OVERRIDE;
// 	virtual bool OnContextMenuCommand(CefRefPtr<CefBrowser> browser,
// 									CefRefPtr<CefFrame> frame,
// 									CefRefPtr<CefContextMenuParams> params,
// 									int command_id,
// 									EventFlags event_flags) OVERRIDE;

	// CefDisplayHandler methods
	virtual void OnAddressChange(CefRefPtr<CefBrowser> browser,
								CefRefPtr<CefFrame> frame,
								const CefString& url) OVERRIDE;
	virtual void OnTitleChange(CefRefPtr<CefBrowser> browser,
								const CefString& title) OVERRIDE;
	virtual bool OnConsoleMessage(CefRefPtr<CefBrowser> browser,
								RGSC_X64_ONLY(cef_log_severity_t level,)
								const CefString& message,
								const CefString& source,
								int line) OVERRIDE;

	// CefJSDialogHandler methods
	virtual bool OnJSDialog(CefRefPtr<CefBrowser> browser,
                        const CefString& origin_url,
                        const CefString& accept_lang,
                        CefJSDialogHandler::JSDialogType dialog_type,
                        const CefString& message_text,
                        const CefString& default_prompt_text,
                        CefRefPtr<CefJSDialogCallback> callback,
                        bool& suppress_message);

	// CefDownloadHandler methods
// 	virtual void OnBeforeDownload(
// 		CefRefPtr<CefBrowser> browser,
// 		CefRefPtr<CefDownloadItem> download_item,
// 		const CefString& suggested_name,
// 		CefRefPtr<CefBeforeDownloadCallback> callback) OVERRIDE;
// 	virtual void OnDownloadUpdated(
// 		CefRefPtr<CefBrowser> browser,
// 		CefRefPtr<CefDownloadItem> download_item,
// 		CefRefPtr<CefDownloadItemCallback> callback) OVERRIDE;
// 
// 	// CefDragHandler methods
	virtual bool OnDragEnter(CefRefPtr<CefBrowser> browser,
 							CefRefPtr<CefDragData> dragData,
 							CefDragHandler::DragOperationsMask mask) OVERRIDE;
// 
// 	// CefGeolocationHandler methods
// 	virtual void OnRequestGeolocationPermission(
// 		CefRefPtr<CefBrowser> browser,
// 		const CefString& requesting_url,
// 		int request_id,
// 		CefRefPtr<CefGeolocationCallback> callback) OVERRIDE;

	// CefKeyboardHandler methods
	virtual bool OnPreKeyEvent(CefRefPtr<CefBrowser> browser,
								const CefKeyEvent& event,
								CefEventHandle os_event,
								bool* is_keyboard_shortcut) OVERRIDE;

	// CefLifeSpanHandler methods
	virtual bool OnBeforePopup(CefRefPtr< CefBrowser > browser, 
								CefRefPtr< CefFrame > frame, 
								const CefString& target_url, 
								const CefString& target_frame_name, 
								CefLifeSpanHandler::WindowOpenDisposition target_disposition, 
								bool user_gesture, const CefPopupFeatures& popupFeatures, 
								CefWindowInfo& windowInfo, 
								CefRefPtr< CefClient >& client, 
								CefBrowserSettings& settings, bool* no_javascript_access ) OVERRIDE;
	virtual void OnAfterCreated(CefRefPtr<CefBrowser> browser) OVERRIDE;
	virtual bool DoClose(CefRefPtr<CefBrowser> browser) OVERRIDE;
	virtual void OnBeforeClose(CefRefPtr<CefBrowser> browser) OVERRIDE;

	// CefLoadHandler methods
	virtual void OnLoadingStateChange(CefRefPtr<CefBrowser> browser,
									bool isLoading,
									bool canGoBack,
									bool canGoForward) OVERRIDE;
	virtual void OnLoadError(CefRefPtr<CefBrowser> browser,
							CefRefPtr<CefFrame> frame,
							ErrorCode errorCode,
							const CefString& errorText,
							const CefString& failedUrl) OVERRIDE;

	// CefRequestHandler methods
	virtual bool OnBeforeBrowse(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request, bool is_redirect) OVERRIDE;
	virtual ReturnValue OnBeforeResourceLoad( CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request, CefRefPtr<CefRequestCallback> callback) OVERRIDE;
	virtual CefRefPtr<CefResourceHandler> GetResourceHandler(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request) OVERRIDE;
// 	virtual bool OnQuotaRequest(CefRefPtr<CefBrowser> browser, const CefString& origin_url, int64 new_size, CefRefPtr<CefQuotaCallback> callback) OVERRIDE;
	virtual void OnProtocolExecution(CefRefPtr<CefBrowser> browser, const CefString& url, bool& allow_os_execution) OVERRIDE;
	virtual void OnRenderProcessTerminated(CefRefPtr<CefBrowser> browser, TerminationStatus status) OVERRIDE;
	virtual bool OnResourceResponse(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request, CefRefPtr<CefResponse> response) OVERRIDE;
	virtual bool OnCertificateError(CefRefPtr<CefBrowser> browser, cef_errorcode_t cert_error, const CefString& request_url, CefRefPtr<CefSSLInfo> ssl_info, CefRefPtr<CefRequestCallback> callback);
	virtual bool OnOpenURLFromTab(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& target_url, CefRequestHandler::WindowOpenDisposition target_disposition, bool user_gesture) OVERRIDE;

	// CefFocusHandler methods
	virtual bool OnSetFocus(CefRefPtr<CefBrowser> browser, CefFocusHandler::FocusSource source ) OVERRIDE;
	CefRefPtr<CefFocusHandler> GetFocusHandler() OVERRIDE { return this; }

	// CefRenderHandler methods
	virtual bool GetRootScreenRect(CefRefPtr<CefBrowser> browser,
									CefRect& rect) OVERRIDE;
	virtual bool GetViewRect(CefRefPtr<CefBrowser> browser,
							CefRect& rect) OVERRIDE;
	virtual bool GetScreenPoint(CefRefPtr<CefBrowser> browser,
								int viewX,
								int viewY,
								int& screenX,
								int& screenY) OVERRIDE;
	virtual bool GetScreenInfo(CefRefPtr<CefBrowser> browser,
								CefScreenInfo& screen_info) OVERRIDE;
	virtual void OnPopupShow(CefRefPtr<CefBrowser> browser, bool show) OVERRIDE;
	virtual void OnPopupSize(CefRefPtr<CefBrowser> browser,
							const CefRect& rect) OVERRIDE;
	virtual void OnPaint(CefRefPtr<CefBrowser> browser,
						PaintElementType type,
						const RectList& dirtyRects,
						const void* buffer,
						int width,
						int height) OVERRIDE;

	virtual void OnCursorChange(CefRefPtr<CefBrowser> browser,
								CefCursorHandle cursor,
								CursorType type,
								const CefCursorInfo& custom_cursor_info) OVERRIDE;

	CefRefPtr<CefBrowser> GetBrowser() { return m_Browser; }

	void CreateBorderlessWindowWrapper(ViewMsg_Rect_Params& rect, ViewMsg_CreateWindow_Params& params);

	int GetBrowserId() const { return m_BrowserId; }
	unsigned int GetWindowId() const { return m_WindowId; }

	HWND GetBrowserHandle() { return m_hWnd; }
	HWND GetModalBlocker() { return m_hModalBlocker; }

	// Request that all existing browser windows close.
	void CloseAllBrowsers(bool force_close);

	//std::string GetLogFile();

// 	void SetLastDownloadFile(const std::string& fileName);
// 	std::string GetLastDownloadFile();

	// Send a notification to the application. Notifications should not block the caller.
	enum NotificationType {
		NOTIFY_CONSOLE_MESSAGE,
		NOTIFY_DOWNLOAD_COMPLETE,
		NOTIFY_DOWNLOAD_ERROR,
	};

	void SendNotification(NotificationType type);

	bool IsClosing() { return m_bIsClosing; }

	bool CanClose() { return m_bCanClose || IsClosing(); }

	void Close(bool bForce = true);

	void OnWindowStateChanged(WindowState windowState);

// 	void ShowDevTools(CefRefPtr<CefBrowser> browser);
// 	void CloseDevTools(CefRefPtr<CefBrowser> browser);

// 	void BeginTracing();
// 	void EndTracing();

//	bool Save(const std::string& path, const std::string& data);

	// Some windows messages use client space (i.e. WM_MOUSEMOVE), others use screen(window) space (WM_NCMOUSEMOVE)
	enum RgscRectRetrievalMethod
	{
		USE_CLIENT_RECT,
		USE_WINDOW_RECT
	};

	// Due to an issue seen in CEF/Chrome, hit testing in the lower right corner is not resulting
	// in a non-client message as expected (instead, WM_NCHITTEST is spammed). As a workaround, we
	// return the upper left corner HTTOPLEFT when OVERRIDE is specified so the cursor is the same, and then
	// we translate back to HTBOTTOMRIGHT when forwarding the message to the parent window.
	enum RgscHitTestBottomRightBehaviour
	{
		NORMAL,
		OVERRIDE
	};

	// Hit Testing and Subclassing
	static LRESULT CALLBACK HandleHitTest(SubProcessWindow* window, HWND hwnd, WPARAM wParam, LPARAM lParam, RgscRectRetrievalMethod rectMethod, RgscHitTestBottomRightBehaviour bLowerRightOverride);
	static LRESULT CALLBACK WindowsMessageHandler(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);
	static BOOL CALLBACK SubclassChildren(HWND hwnd, LPARAM lp);

private:

#if	SUPPORT_WIDGETS
	enum WidgetState
	{
		WIDGET_INVALID,
		WIDGET_CREATED,
		WIDGET_RESIZED,
		WIDGET_PAINTED,
	};

	bool m_Widget;
	unsigned char* m_WidgetBuffer;
	unsigned m_WidgetBufferSize;
	WidgetState m_WidgetState;
#endif

	// A caption exclusion is a rectangle offset from a reference point (i.e. upper left corner) that should not 
	// return HTCAPTION for hit testing. Used to mask client areas within a fake non-client area in transparent window.
	// Example: Exclude the close/minimize buttons in the top right corner using WE_TOP|WE_RIGHT.
	struct CaptionExclusion
	{
		int x;
		int y;
		int width;
		int height;
		int referencePoint; // Combination of WindowEdgeFlags
	};

	std::vector<CaptionExclusion> m_CaptionExclusions;

	unsigned int m_WindowId;
	HWND m_hWnd, m_hRootParent, m_hModalBlocker;
	BorderlessWindowWrapper m_BorderlessWindow;

	ipcbase::SharedMemory* m_SharedMemory;
	std::vector<rgsc::RgscRect> m_TileRects;
	unsigned int m_Width;
	unsigned int m_Height;
	float m_Scale;
	int m_BorderWidth;
	int m_CaptionHeight;
	int m_ResizeEdges;
	int m_SizeGripSize;

	bool m_bIsClosing;
	bool m_bIsModal;
	bool m_bCanClose;
	bool m_bIsMouseInCaptionRegion;

	RgscWindowType m_WindowType;

	void SetZoomLevel();
	CefRect GetPopupRectInWebView(const CefRect& original_rect);

	void onWidgetCreated(CefRefPtr<CefBrowser> browser);
	void onWidgetDestroyed(CefRefPtr<CefBrowser> browser);
	void onWidgetResize(const CefRect& rect);
	bool onWidgetPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, const void* buffer, int width, int height);
	void ClearPopupRects();
	bool renderToBuffer(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, const void* buffer, int width, int height);

#if !__NO_OUTPUT
	std::string DumpRequestContents(CefRefPtr<CefRequest> request);
	std::string DumpResponseContents(CefRefPtr<CefRequest> request, CefRefPtr<CefResponse> response, int* status);
#endif

	struct JavascriptBinding
	{
		std::wstring funcName;
		bool synchronous;
	};
	std::vector<JavascriptBinding> m_JavascriptBindings;

	ResourceInterceptor m_ResourceInterceptor;

	CefRefPtr<CefBrowser> m_Browser;
	int m_BrowserId;

	CefString m_Url;
	CefString m_OfflineRoot;
	CefRefPtr<CefResourceManager> m_ResourceManager;

#if RSG_DEV
	std::string m_DisableCaptchaHeader;
	std::string m_DisableCaptchaHeaderValue;
#endif

	int m_MouseX;
	int m_MouseY;

	CefRect popup_rect_;
	CefRect original_popup_rect_;
	
	// Include the default reference counting implementation.
	IMPLEMENT_REFCOUNTING(SubProcessWindow);
	// Include the default locking implementation.
	//IMPLEMENT_LOCKING(SubProcessWindow);

};

} // namespace rgsc

#endif //SUBPROCESS_WINDOW_H
