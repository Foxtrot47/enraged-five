#include "window.h"
#include "rgsc_messages.h"
#include "rgsc_messages_params.h"
#include "subprocess.h"
#include "types.h"
#include "include/wrapper/cef_stream_resource_handler.h"
#include "include/wrapper/cef_resource_manager.h"

#define IPC_SEND(x) rgsc::SubProcess::GetInstance()._GetIpc()->Send(x)

#define REQUIRE_UI_THREAD()   Assert(CefCurrentlyOn(TID_UI));
#define REQUIRE_IO_THREAD()   Assert(CefCurrentlyOn(TID_IO));
#define REQUIRE_FILE_THREAD() Assert(CefCurrentlyOn(TID_FILE));

namespace rgsc
{

std::map<HWND, RgscWndProcMap> s_WndProcs;

SubProcessWindow::SubProcessWindow(unsigned int windowId, unsigned int width, unsigned int height, float scale, ViewMsg_Tile_Params tiles, std::string sharedMemoryName)
: m_WindowId(windowId)
, m_Width(width)
, m_Height(height)
, m_Scale(scale)
, m_BorderWidth(0)
, m_CaptionHeight(0)
, m_ResizeEdges(0)
, m_SizeGripSize(0)
, m_hWnd(0)
, m_hRootParent(0)
, m_hModalBlocker(0)
, m_bIsClosing(false)
, m_bIsModal(false)
, m_bCanClose(true)
, m_bIsMouseInCaptionRegion(false)
, m_WindowType(WINDOWLESS)
#if SUPPORT_WIDGETS
, m_WidgetState(WIDGET_INVALID)
, m_Widget(false)
, m_WidgetBuffer(NULL)
, m_WidgetBufferSize(0)
#endif
{
	m_ResourceManager = new CefResourceManager();
 	m_SharedMemory = new ipcbase::SharedMemory();
	if(m_SharedMemory)
	{
		if(m_SharedMemory->Open(sharedMemoryName, false))
		{
			m_SharedMemory->Map(getBufferSizeInBytes());
		}
	}

	CefWindowInfo info;
#if RSG_CPU_X64
	info.SetAsWindowless(NULL);
#else
	info.SetAsWindowless(NULL, true);
#endif

	CefBrowserSettings settings;
	settings.file_access_from_file_urls = STATE_ENABLED;

	CefBrowserHost::CreateBrowser(info, this, "", settings, NULL);

	m_TileRects = tiles.tile_rects;
}

SubProcessWindow::SubProcessWindow(unsigned int windowId, ViewMsg_Rect_Params rect, ViewMsg_CreateWindow_Params window)
: m_WindowId(windowId)
, m_Width(rect.width)
, m_Height(rect.height)
, m_Scale(window.scale)
, m_BorderWidth(window.borderWidth)
, m_CaptionHeight(window.captionHeight)
, m_ResizeEdges(window.resizeEdges)
, m_SizeGripSize(window.sizeGripSize)
, m_SharedMemory(NULL)
, m_hWnd((HWND)window.hWnd)
, m_hRootParent((HWND)window.hRootParent)
, m_hModalBlocker(0)
, m_bIsClosing(false)
, m_bIsModal(false)
, m_bCanClose(!window.hideOnClose)
, m_bIsMouseInCaptionRegion(false)
#if SUPPORT_WIDGETS
, m_WidgetState(WIDGET_INVALID)
, m_Widget(false)
, m_WidgetBuffer(NULL)
, m_WidgetBufferSize(0)
#endif
, m_WindowType((RgscWindowType)window.windowType)
{
	m_ResourceManager = new CefResourceManager();

	// Create the borderless window wrapper if necessary
	if (m_WindowType == BORDERLESS)
	{
		CreateBorderlessWindowWrapper(rect, window);
	}

	CefBrowserSettings settings;
	settings.file_access_from_file_urls = STATE_ENABLED;

	RECT wnd_rect;
	CefWindowInfo info;

	switch(m_WindowType)
	{
	case BORDERLESS:
	case CHILD:
		wnd_rect.left = 0;
		wnd_rect.top = 0;
		wnd_rect.right = rect.width;
		wnd_rect.bottom = rect.height;
		info.SetAsChild(m_hWnd, wnd_rect); 
		break;
	case POPUP:
		info.SetAsPopup(m_hWnd, window.windowTitle);
		info.x = rect.x;
		info.y = rect.y;
		info.width = rect.width;
		info.height = rect.height;
		break;
	default:
		Assert(false);
		break;
	}

	CefBrowserHost::CreateBrowser(info, this, "", settings, NULL);
}


SubProcessWindow::~SubProcessWindow()
{
	rgsc::SubProcess::GetInstance().SetWindowIsClosed();
}

void SubProcessWindow::Close(bool bForce)
{
	if(m_Browser.get())
	{
		m_Browser->GetHost()->CloseBrowser(bForce);
	}
}

void SubProcessWindow::OnWindowStateChanged(WindowState windowState)
{
	Send(new ViewHostMsg_OnWindowStateChanged(m_WindowId, windowState));
}

static bool isKeyDown(WPARAM wparam)
{
	return (GetKeyState((int)wparam) & 0x8000) != 0;
}

static int GetCefMouseModifiers(WPARAM wparam) {
  int modifiers = 0;
  if (wparam & MK_CONTROL)
    modifiers |= EVENTFLAG_CONTROL_DOWN;
  if (wparam & MK_SHIFT)
    modifiers |= EVENTFLAG_SHIFT_DOWN;
  if (isKeyDown(VK_MENU))
    modifiers |= EVENTFLAG_ALT_DOWN;
  if (wparam & MK_LBUTTON)
    modifiers |= EVENTFLAG_LEFT_MOUSE_BUTTON;
  if (wparam & MK_MBUTTON)
    modifiers |= EVENTFLAG_MIDDLE_MOUSE_BUTTON;
  if (wparam & MK_RBUTTON)
    modifiers |= EVENTFLAG_RIGHT_MOUSE_BUTTON;

  // Low bit set from GetKeyState indicates "toggled".
  if (::GetKeyState(VK_NUMLOCK) & 1)
    modifiers |= EVENTFLAG_NUM_LOCK_ON;
  if (::GetKeyState(VK_CAPITAL) & 1)
    modifiers |= EVENTFLAG_CAPS_LOCK_ON;
  return modifiers;
}

static int GetCefKeyboardModifiers(WPARAM wparam, LPARAM lparam)
{
	int modifiers = 0;

	if(isKeyDown(VK_SHIFT))
	{
		modifiers |= EVENTFLAG_SHIFT_DOWN;
	}

	if(isKeyDown(VK_CONTROL))
	{
		modifiers |= EVENTFLAG_CONTROL_DOWN;
	}

	if(isKeyDown(VK_MENU))
	{
		modifiers |= EVENTFLAG_ALT_DOWN;
	}

	// Low bit set from GetKeyState indicates "toggled".
	if(::GetKeyState(VK_NUMLOCK) & 1)
	{
		modifiers |= EVENTFLAG_NUM_LOCK_ON;
	}

	if(::GetKeyState(VK_CAPITAL) & 1)
	{
		modifiers |= EVENTFLAG_CAPS_LOCK_ON;
	}

	switch (wparam)
	{
	case VK_RETURN:
		if((lparam >> 16) & KF_EXTENDED)
		{
			modifiers |= EVENTFLAG_IS_KEY_PAD;
		}
		break;
	case VK_INSERT:
	case VK_DELETE:
	case VK_HOME:
	case VK_END:
	case VK_PRIOR:
	case VK_NEXT:
	case VK_UP:
	case VK_DOWN:
	case VK_LEFT:
	case VK_RIGHT:
		if(!((lparam >> 16) & KF_EXTENDED))
		{
			modifiers |= EVENTFLAG_IS_KEY_PAD;
		}
		break;
	case VK_NUMLOCK:
	case VK_NUMPAD0:
	case VK_NUMPAD1:
	case VK_NUMPAD2:
	case VK_NUMPAD3:
	case VK_NUMPAD4:
	case VK_NUMPAD5:
	case VK_NUMPAD6:
	case VK_NUMPAD7:
	case VK_NUMPAD8:
	case VK_NUMPAD9:
	case VK_DIVIDE:
	case VK_MULTIPLY:
	case VK_SUBTRACT:
	case VK_ADD:
	case VK_DECIMAL:
	case VK_CLEAR:
		modifiers |= EVENTFLAG_IS_KEY_PAD;
		break;
	case VK_SHIFT:
		if(isKeyDown(VK_LSHIFT))
		{
			modifiers |= EVENTFLAG_IS_LEFT;
		}
		else if(isKeyDown(VK_RSHIFT))
		{
			modifiers |= EVENTFLAG_IS_RIGHT;
		}
		break;
	case VK_CONTROL:
		if(isKeyDown(VK_LCONTROL))
		{
			modifiers |= EVENTFLAG_IS_LEFT;
		}
		else if(isKeyDown(VK_RCONTROL))
		{
			modifiers |= EVENTFLAG_IS_RIGHT;
		}
		break;
	case VK_MENU:
		if(isKeyDown(VK_LMENU))
		{
			modifiers |= EVENTFLAG_IS_LEFT;
		}
		else if(isKeyDown(VK_RMENU))
		{
			modifiers |= EVENTFLAG_IS_RIGHT;
		}
		break;
	case VK_LWIN:
		modifiers |= EVENTFLAG_IS_LEFT;
		break;
	case VK_RWIN:
		modifiers |= EVENTFLAG_IS_RIGHT;
		break;
	}
	return modifiers;
}

static int GetScKeyboardModifiers(__int64 scModifiers)
{
	int modifiers = 0;

	if (scModifiers & RGSC_SHIFT_MOD)
	{
		modifiers |= EVENTFLAG_SHIFT_DOWN;
	}

	if (scModifiers & RGSC_CONTROL_MOD)
	{
		modifiers |= EVENTFLAG_CONTROL_DOWN;
	}

	if (scModifiers & RGSC_ALT_MOD)
	{
		modifiers |= EVENTFLAG_ALT_DOWN;
	}

	return modifiers;
}

static __int64 ToScKeyboardModifiers(uint32 modifiers)
{
	int scModifiers = 0;

	if (modifiers & EVENTFLAG_SHIFT_DOWN)
	{
		scModifiers |= RGSC_SHIFT_MOD;
	}

	if (modifiers & EVENTFLAG_CONTROL_DOWN)
	{
		scModifiers |= RGSC_CONTROL_MOD;
	}

	if (modifiers & EVENTFLAG_ALT_DOWN)
	{
		scModifiers |= RGSC_ALT_MOD;
	}

	return scModifiers;
}

static RgscCursorType CefCursorToRgscCursor(int cursorType)
{
	switch(cursorType)
	{
	case CT_POINTER:
		return RGSC_CURSOR_POINTER;
	case CT_CROSS:
		return RGSC_CURSOR_CROSS;
	case CT_HAND:
		return RGSC_CURSOR_HAND;
	case CT_IBEAM:
		return RGSC_CURSOR_IBEAM;
	case CT_WAIT:
		return RGSC_CURSOR_WAIT;
	case CT_HELP:
		return RGSC_CURSOR_HELP;
	case CT_EASTRESIZE:
		return RGSC_CURSOR_EASTRESIZE;
	case CT_NORTHRESIZE:
		return RGSC_CURSOR_NORTHRESIZE;
	case CT_NORTHEASTRESIZE:
		return RGSC_CURSOR_NORTHEASTRESIZE;
	case CT_NORTHWESTRESIZE:
		return RGSC_CURSOR_NORTHWESTRESIZE;
	case CT_SOUTHRESIZE:
		return RGSC_CURSOR_SOUTHRESIZE;
	case CT_SOUTHEASTRESIZE:
		return RGSC_CURSOR_SOUTHEASTRESIZE;
	case CT_SOUTHWESTRESIZE:
		return RGSC_CURSOR_SOUTHWESTRESIZE;
	case CT_WESTRESIZE:
		return RGSC_CURSOR_WESTRESIZE;
	case CT_NORTHSOUTHRESIZE:
		return RGSC_CURSOR_NORTHSOUTHRESIZE;
	case CT_EASTWESTRESIZE:
		return RGSC_CURSOR_EASTWESTRESIZE;
	case CT_NORTHEASTSOUTHWESTRESIZE:
		return RGSC_CURSOR_NORTHEASTSOUTHWESTRESIZE;
	case CT_NORTHWESTSOUTHEASTRESIZE:
		return RGSC_CURSOR_NORTHWESTSOUTHEASTRESIZE;
	case CT_COLUMNRESIZE:
		return RGSC_CURSOR_COLUMNRESIZE;
	case CT_ROWRESIZE:
		return RGSC_CURSOR_ROWRESIZE;
	case CT_MIDDLEPANNING:
		return RGSC_CURSOR_MIDDLEPANNING;
	case CT_EASTPANNING:
		return RGSC_CURSOR_EASTPANNING;
	case CT_NORTHPANNING:
		return RGSC_CURSOR_NORTHPANNING;
	case CT_NORTHEASTPANNING:
		return RGSC_CURSOR_NORTHEASTPANNING;
	case CT_NORTHWESTPANNING:
		return RGSC_CURSOR_NORTHWESTPANNING;
	case CT_SOUTHPANNING:
		return RGSC_CURSOR_SOUTHPANNING;
	case CT_SOUTHEASTPANNING:
		return RGSC_CURSOR_SOUTHEASTPANNING;
	case CT_SOUTHWESTPANNING:
		return RGSC_CURSOR_SOUTHWESTPANNING;
	case CT_WESTPANNING:
		return RGSC_CURSOR_WESTPANNING;
	case CT_MOVE:
		return RGSC_CURSOR_MOVE;
	case CT_VERTICALTEXT:
		return RGSC_CURSOR_VERTICALTEXT;
	case CT_CELL:
		return RGSC_CURSOR_CELL;
	case CT_CONTEXTMENU:
		return RGSC_CURSOR_CONTEXTMENU;
	case CT_ALIAS:
		return RGSC_CURSOR_ALIAS;
	case CT_PROGRESS:
		return RGSC_CURSOR_PROGRESS;
	case CT_NODROP:
		return RGSC_CURSOR_NODROP;
	case CT_COPY:
		return RGSC_CURSOR_COPY;
	case CT_NONE:
		return RGSC_CURSOR_NONE;
	case CT_NOTALLOWED:
		return RGSC_CURSOR_NOTALLOWED;
	case CT_ZOOMIN:
		return RGSC_CURSOR_ZOOMIN;
	case CT_ZOOMOUT:
		return RGSC_CURSOR_ZOOMOUT;
	case CT_GRAB:
		return RGSC_CURSOR_GRAB;
	case CT_GRABBING:
		return RGSC_CURSOR_GRABBING;
	case CT_CUSTOM:
		return RGSC_CURSOR_CUSTOM;
	}

	// default
	return RGSC_CURSOR_POINTER;
}

void SubProcessWindow::CreateBorderlessWindowWrapper(ViewMsg_Rect_Params& rect, ViewMsg_CreateWindow_Params& params)
{
	Assert(m_hWnd == 0 && m_hRootParent == 0);

	AssertVerify(m_BorderlessWindow.Init(this, rect, params));

	m_hWnd = m_BorderlessWindow.GetWindowHandle();
	m_hRootParent = m_BorderlessWindow.GetWindowHandle();
}

void SubProcessWindow::onMouseMovedIpc(__int64 msg, __int64 wParam, __int64 lParam)
{
	m_MouseX = (int)(short)LOWORD(lParam);
	m_MouseY = (int)(short)HIWORD(lParam);

	if(m_Browser.get())
	{
        CefMouseEvent mouse_event;
        mouse_event.x = m_MouseX;
        mouse_event.y = m_MouseY;

		// TODO: NS - ApplyPopupOffset
		//window->ApplyPopupOffset(mouse_event.x, mouse_event.y);
        mouse_event.modifiers = GetCefMouseModifiers((WPARAM)wParam);
        m_Browser->GetHost()->SendMouseMoveEvent(mouse_event, false);
    }
}

void SubProcessWindow::onMouseButtonIpc(__int64 msg, __int64 wParam, __int64 lParam)
{
	__int64 currentTime = 0;
	bool cancelPreviousClick = false;
	static int gLastClickCount = 0;
	static __int64 gLastClickTime = 0;
	static int lastClickX = 0;
	static int lastClickY = 0;


	// From the Windows docs:
	// "Double-clicking the left mouse button actually generates a sequence of
	// four messages: WM_LBUTTONDOWN, WM_LBUTTONUP, WM_LBUTTONDBLCLK, and WM_LBUTTONUP."
	// Windows can send a button down or a double-click depending on the
	// window style. We can handle both cases by treating the double
	// click event as a button down and tracking multi-clicks ourselves.
	if(msg == WM_LBUTTONDBLCLK)
	{
		msg = WM_LBUTTONDOWN;
	}

	if(msg == WM_LBUTTONDOWN || msg == WM_RBUTTONDOWN ||
       msg == WM_MBUTTONDOWN || msg == WM_MOUSEMOVE ||
	   msg == WM_MOUSELEAVE)
	{
		currentTime = timeGetTime();

		int x = (int)(short)LOWORD(lParam);
		int y = (int)(short)HIWORD(lParam);

		cancelPreviousClick = (abs(lastClickX - x) > (GetSystemMetrics(SM_CXDOUBLECLK) / 2))
								|| (abs(lastClickY - y) > (GetSystemMetrics(SM_CYDOUBLECLK) / 2))
								|| ((currentTime - gLastClickTime) > GetDoubleClickTime());

// 		if(cancelPreviousClick)
// 		{
// 			gLastClickCount = 0;
// 			gLastClickTime = 0;
// 			lastClickX = 0;
// 			lastClickY = 0;
// 		}
	}

	if(msg == WM_LBUTTONDOWN)
	{
		// If we receive a mouse event without window focus, take focus.
		if (m_Browser.get())
		{
			m_Browser->GetHost()->SetFocus(true);
		}

		m_MouseX = (int)(short)LOWORD(lParam);
		m_MouseY = (int)(short)HIWORD(lParam);

		CefBrowserHost::MouseButtonType btnType = MBT_LEFT;

		if(!cancelPreviousClick)
		{
			++gLastClickCount;
		}
		else
		{
			gLastClickCount = 1;
			lastClickX = m_MouseX;
			lastClickY = m_MouseY;
		}
		gLastClickTime = currentTime;

		if(m_Browser.get())
		{
			CefMouseEvent mouse_event;
			mouse_event.x = m_MouseX;
			mouse_event.y = m_MouseY;
			//gLastMouseDownOnView = !window->IsOverPopupWidget(x, y);
			//window->ApplyPopupOffset(mouse_event.x, mouse_event.y);
			mouse_event.modifiers = GetCefMouseModifiers((WPARAM)wParam);
			m_Browser->GetHost()->SendMouseClickEvent(mouse_event, btnType, false, gLastClickCount);
		}
	}

	if(msg == WM_LBUTTONUP)
	{
		m_MouseX = (int)(short)LOWORD(lParam);
		m_MouseY = (int)(short)HIWORD(lParam);

		CefBrowserHost::MouseButtonType btnType = MBT_LEFT;

		if(m_Browser.get())
		{
			CefMouseEvent mouse_event;
			mouse_event.x = m_MouseX;
			mouse_event.y = m_MouseY;
// 			if(gLastMouseDownOnView &&
// 			        window->IsOverPopupWidget(x, y) &&
// 			        (window->GetPopupXOffset() || window->GetPopupYOffset()))
// 			{
// 				break;
// 			}
			//window->ApplyPopupOffset(mouse_event.x, mouse_event.y);
			mouse_event.modifiers = GetCefMouseModifiers((WPARAM)wParam);
			m_Browser->GetHost()->SendMouseClickEvent(mouse_event, btnType, true, gLastClickCount);
		}
	}
}

void SubProcessWindow::onMouseWheelIpc(int xScroll, int yScroll)
{
	// TODO: NS - see client_handler.cpp and do the same thing

	if(m_Browser.get())
	{
		CefMouseEvent mouse_event;
		mouse_event.x = m_MouseX;
		mouse_event.y = m_MouseY;

		m_Browser->GetHost()->SendMouseWheelEvent(mouse_event, xScroll, yScroll);
	}
}

void SubProcessWindow::onKeyEventIpc(__int64 msg, __int64 wParam, __int64 lParam, __int64 scModifiers)
{
    CefKeyEvent event;
    event.windows_key_code = (int)wParam;
    event.native_key_code = (int)lParam;
    event.is_system_key = msg == WM_SYSCHAR ||
                          msg == WM_SYSKEYDOWN ||
                          msg == WM_SYSKEYUP;

    if(msg == WM_KEYDOWN || msg == WM_SYSKEYDOWN)
	{
		event.type = KEYEVENT_RAWKEYDOWN;
	}
    else if(msg == WM_KEYUP || msg == WM_SYSKEYUP)
	{
		event.type = KEYEVENT_KEYUP;
	}
    else
	{
		event.type = KEYEVENT_CHAR;
	}

    event.modifiers = GetCefKeyboardModifiers((WPARAM)wParam, (LPARAM)lParam);
	event.modifiers |= GetScKeyboardModifiers(scModifiers);
	
	if(m_Browser.get())
	{
		m_Browser->GetHost()->SendKeyEvent(event);
	}
}

bool SubProcessWindow::onNavigateToIpc(std::string url, std::string offlineRoot, std::string zipFile)
{
	m_Url = url;
	m_OfflineRoot = offlineRoot;

	if (offlineRoot.length() > 0)
	{
		m_ResourceManager->AddArchiveProvider(offlineRoot, zipFile, "", 0, "");
	}
	else
	{
		// Clear providers
		m_ResourceManager->RemoveAllProviders();
	}

	if(m_Browser.get())
	{
		m_BrowserId = m_Browser->GetIdentifier();
	 	m_Browser->GetMainFrame()->LoadURL(m_Url);
	 	return true;
	}

	return false;
}

void SubProcessWindow::onAddBindOnStartLoadingIpc(std::wstring functionName, bool synchronous)
{
	JavascriptBinding binding;
	binding.funcName = functionName;
	binding.synchronous = synchronous;

	m_JavascriptBindings.push_back(binding);
}

void SubProcessWindow::onExecuteJavascriptIpc(std::wstring javascript)
{
	if(m_Browser.get())
	{
		m_Browser->GetMainFrame()->ExecuteJavaScript(javascript, "", 0);
	}
	else
	{
		Errorf("executeJavascript called and m_browser.get() failed.");
	}
}

unsigned int SubProcessWindow::getPixelBufferSizeInBytes()
{
	const int kBytesPerPixel = 4;
	return (m_Width * m_Height * kBytesPerPixel);
}

unsigned int SubProcessWindow::getBufferSizeInBytes()
{
	return getPixelBufferSizeInBytes() + getDirtyTileBufferSizeInBytes();
}

unsigned int SubProcessWindow::getDirtyTileBufferSizeInBytes()
{
	const unsigned maxTiles = 128 * 128;
	const unsigned dirtyTileMem = maxTiles * sizeof(unsigned short);
	return dirtyTileMem;
}

void SubProcessWindow::onLostDeviceIpc()
{
	if(m_SharedMemory)
	{
		m_SharedMemory->Close();
	}
}

void SubProcessWindow::onResizeIpc(unsigned int width,
								 unsigned int height,
								 float scale,
								 ViewMsg_Tile_Params tiles,
								 std::string sharedMemoryName)
{
	Assert(m_WindowType == WINDOWLESS);

	m_Width = width;
	m_Height = height;
	m_Scale = scale;

	m_TileRects = tiles.tile_rects;

	if(m_SharedMemory)
	{
		m_SharedMemory->Close();
		if(m_SharedMemory->Open(sharedMemoryName, false))
		{
			m_SharedMemory->Map(getBufferSizeInBytes());
		}
	}

	if(m_Browser.get())
	{
		SetZoomLevel();

		// to resize the browser call CefBrowserHost::WasResized(). This will result in a
		// call to GetViewRect() to retrieve the new size followed by a call to OnPaint().
		m_Browser->GetHost()->WasResized();

		// we also need to tell the browser that our screen info (width/height) changed
		// otherwise dropdown lists can get sized incorrectly after a resize
		m_Browser->GetHost()->NotifyScreenInfoChanged();

		m_Browser->GetHost()->Invalidate(PaintElementType::PET_VIEW);
	}
}

void SubProcessWindow::onAddCaptionExclusionIpc(unsigned int x, unsigned int y, unsigned int width, unsigned int height, unsigned int referencePoint)
{
	Assert(m_WindowType != WINDOWLESS);

	CaptionExclusion c;
	c.x = x;
	c.y = y;
	c.width = width;
	c.height = height;
	c.referencePoint = referencePoint;

	m_CaptionExclusions.push_back(c);
}

void SubProcessWindow::onClearCaptionExclusionIpc()
{
	Assert(m_WindowType != WINDOWLESS);
	m_CaptionExclusions.clear();
}

void SubProcessWindow::onCloseBrowserIpc()
{
	Assert(m_WindowType != WINDOWLESS);
	Close(false);
}

void SubProcessWindow::onResizeOrMoveIpc(ViewMsg_Rect_Params rect)
{
	Assert(m_WindowType != WINDOWLESS);

	if (m_Browser.get())
	{
		m_Browser->GetHost()->NotifyMoveOrResizeStarted();
	}
}

void SubProcessWindow::onModalChangedIpc(bool bIsModal)
{
	m_bIsModal = bIsModal;
}

void SubProcessWindow::onSetWindowCanCloseIpc(bool bCanClose)
{
	m_bCanClose = bCanClose;
}

void SubProcessWindow::onSetWindowModalBlockerIpc(__int64 modalBlocker)
{
	m_hModalBlocker = (HWND)modalBlocker;
}

void SubProcessWindow::onMouseInCaptionChangedIpc(bool isMouseInCaptionRegion)
{
	m_bIsMouseInCaptionRegion = isMouseInCaptionRegion;
}

void SubProcessWindow::onSetFocusIpc(bool bEnabled)
{
	if (m_Browser.get())
	{
		m_Browser->GetHost()->SetFocus(bEnabled);
	}
}

void SubProcessWindow::onCustomWindowMessage(__int64 msg, __int64 wParam, __int64 lParam)
{
	if (m_WindowType == BORDERLESS)
	{
		m_BorderlessWindow.OnCustomWindowMessage((UINT)msg, (WPARAM)wParam, (LPARAM)lParam);
	}
}

void SubProcessWindow::OnContextInitialized(CefRefPtr<SubProcess> app)
{

}

void SubProcessWindow::OnBeforeChildProcessLaunch(CefRefPtr<SubProcess> app, CefRefPtr<CefCommandLine> command_line)
{
	CefString type = command_line->GetSwitchValue("type");
	if(type == "renderer")
	{
		std::string ipcChannelName;
		IPC_SEND(new ViewHostMsg_CreateIpcChannel(&ipcChannelName));

		char commandLine[Ipc::MAX_CHANNEL_NAME_LEN + 32];
		sprintf(commandLine, "--rgsc_ipc_channel_name=%s", ipcChannelName.c_str());
		command_line->AppendArgument(commandLine);

#if RSG_DEV
		m_DisableCaptchaHeader = command_line->GetSwitchValue("no-captcha-header").ToString().c_str();
		m_DisableCaptchaHeaderValue = command_line->GetSwitchValue("no-captcha-header-value").ToString().c_str();
#endif // RSG_DEV
	}
}

void SubProcessWindow::OnRenderProcessThreadCreated(CefRefPtr<SubProcess> app, CefRefPtr<CefListValue> extra_info)
{
	if(extra_info.get())
	{
		int index = (int)extra_info->GetSize();

		for(unsigned i = 0; i < m_JavascriptBindings.size(); ++i)
		{
			extra_info->SetString(index++, m_JavascriptBindings[i].funcName);
			extra_info->SetBool(index++, m_JavascriptBindings[i].synchronous);
		}
	}
}

// this is used to send replies to synchronous IPC messages
void SubProcessWindow::Send(IPC::Message* message)
{
	// We can't send messages for our window if we're closing
	if (m_bIsClosing)
		return;

	IPC_SEND(message);
}

void SubProcessWindow::OnMessageReceived(const IPC::Message& message)
{
	bool handled = true;
	bool valid = false;

	IPC_BEGIN_MESSAGE_MAP_EX(SubProcessWindow, message, valid)
		IPC_MESSAGE_HANDLER(ViewMsg_MouseMoved, onMouseMovedIpc)
		IPC_MESSAGE_HANDLER(ViewMsg_MouseButton, onMouseButtonIpc)
		IPC_MESSAGE_HANDLER(ViewMsg_MouseWheel, onMouseWheelIpc)
		IPC_MESSAGE_HANDLER(ViewMsg_KeyEvent, onKeyEventIpc)
		IPC_MESSAGE_HANDLER(ViewMsg_NavigateTo, onNavigateToIpc)
		IPC_MESSAGE_HANDLER(ViewMsg_ExecuteJavascript, onExecuteJavascriptIpc)
		IPC_MESSAGE_HANDLER(ViewMsg_AddBindOnStartLoading, onAddBindOnStartLoadingIpc)
		IPC_MESSAGE_HANDLER(ViewMsg_OnLostDevice, onLostDeviceIpc)
		IPC_MESSAGE_HANDLER(ViewMsg_ResizeWindow, onResizeIpc)
		IPC_MESSAGE_HANDLER(ViewMsg_AddCaptionExclusion, onAddCaptionExclusionIpc)
		IPC_MESSAGE_HANDLER(ViewMsg_ClearCaptionExclusion, onClearCaptionExclusionIpc)
		IPC_MESSAGE_HANDLER(ViewMsg_CloseBrowser, onCloseBrowserIpc);
		IPC_MESSAGE_HANDLER(ViewMsg_OnResizeOrMove, onResizeOrMoveIpc);
		IPC_MESSAGE_HANDLER(ViewMsg_OnModalChanged, onModalChangedIpc);
		IPC_MESSAGE_HANDLER(ViewMsg_SetWindowCanClose, onSetWindowCanCloseIpc);
		IPC_MESSAGE_HANDLER(ViewMsg_SetWindowModalBlocker, onSetWindowModalBlockerIpc);
		IPC_MESSAGE_HANDLER(ViewMsg_OnMouseInCaptionChanged, onMouseInCaptionChangedIpc);
		IPC_MESSAGE_HANDLER(ViewMsg_OnSetFocus, onSetFocusIpc);
		IPC_MESSAGE_HANDLER(ViewMsg_OnCustomWindowMessage, onCustomWindowMessage);
		IPC_MESSAGE_UNHANDLED(handled = false)
	IPC_END_MESSAGE_MAP_EX()

	Assert(handled);
	Assert(valid);
}

//////////////////////////////////////////////////////////////////////////
// CEF Delegate Callbacks
//////////////////////////////////////////////////////////////////////////

// bool SubProcessWindow::OnProcessMessageReceived(
//     CefRefPtr<CefBrowser> browser,
//     CefProcessId source_process,
//     CefRefPtr<CefProcessMessage> message) {
//   return false;
// }

 void SubProcessWindow::OnBeforeContextMenu( CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefContextMenuParams> params, CefRefPtr<CefMenuModel> model) {
   if (model)
   {
	   model->Clear();
   }
 }
 
// bool SubProcessWindow::OnContextMenuCommand(
//     CefRefPtr<CefBrowser> browser,
//     CefRefPtr<CefFrame> frame,
//     CefRefPtr<CefContextMenuParams> params,
//     int command_id,
//     EventFlags event_flags) {
//   switch (command_id) {
//     case CLIENT_ID_SHOW_DEVTOOLS:
//       ShowDevTools(browser);
//       return true;
//     case CLIENT_ID_CLOSE_DEVTOOLS:
//       CloseDevTools(browser);
//       return true;
//     default:  // Allow default handling, if any.
//       return ExecuteTestMenu(command_id);
//   }
// }

bool SubProcessWindow::OnConsoleMessage(CefRefPtr<CefBrowser> browser,
										RGSC_X64_ONLY(cef_log_severity_t level,)
										const CefString& message,
										const CefString& source,
										int line)
{
	REQUIRE_UI_THREAD();

#if RSG_CPU_X86
	// Win32/Chrome 49 - level is not provided.
	int level = 0;
#endif

	Send(new ViewHostMsg_OnConsoleMessage(m_WindowId, level, std::wstring(message), std::wstring(source), line));

	return false;
}

bool SubProcessWindow::OnJSDialog(CefRefPtr<CefBrowser> browser,
									const CefString& origin_url,
									const CefString& accept_lang,
									CefJSDialogHandler::JSDialogType dialog_type,
									const CefString& message_text,
									const CefString& default_prompt_text,
									CefRefPtr<CefJSDialogCallback> callback,
									bool& suppress_message)
{
	Send(new ViewHostMsg_OnScriptAlert(m_WindowId, std::wstring(message_text), std::wstring(default_prompt_text), std::string(origin_url), 0));

	suppress_message = true;
	return false;
}

// void SubProcessWindow::OnBeforeDownload(
//     CefRefPtr<CefBrowser> browser,
//     CefRefPtr<CefDownloadItem> download_item,
//     const CefString& suggested_name,
//     CefRefPtr<CefBeforeDownloadCallback> callback) {
//   REQUIRE_UI_THREAD();
//   // Continue the download and show the "Save As" dialog.
//   callback->Continue(GetDownloadPath(suggested_name), true);
// }
// 
// void SubProcessWindow::OnDownloadUpdated(
//     CefRefPtr<CefBrowser> browser,
//     CefRefPtr<CefDownloadItem> download_item,
//     CefRefPtr<CefDownloadItemCallback> callback) {
//   REQUIRE_UI_THREAD();
//   if(download_item->IsComplete()) {
//     SetLastDownloadFile(download_item->GetFullPath());
//     SendNotification(NOTIFY_DOWNLOAD_COMPLETE);
//   }
// }
// 
bool SubProcessWindow::OnDragEnter(CefRefPtr<CefBrowser> browser, CefRefPtr<CefDragData> dragData, CefDragHandler::DragOperationsMask mask) 
{
	REQUIRE_UI_THREAD();
	return true;
}
// 
// void SubProcessWindow::OnRequestGeolocationPermission(
//       CefRefPtr<CefBrowser> browser,
//       const CefString& requesting_url,
//       int request_id,
//       CefRefPtr<CefGeolocationCallback> callback) {
//   // Allow geolocation access from all websites.
//   callback->Continue(true);
// }

bool SubProcessWindow::OnPreKeyEvent(CefRefPtr<CefBrowser> browser,
                                  const CefKeyEvent& event,
                                  CefEventHandle os_event,
                                  bool* is_keyboard_shortcut)
{
	// Borderless Windows are fully owned by the SubProcess process/thread,
	// so the SDK does not subclass the window and get first crack at the message.
	// Thus, we must forward keyboard events back to the SDK if they would like to
	// take action.
	if (m_WindowType == BORDERLESS)
	{
		__int64 iMsg, wParam, lParam, scModifiers;

		switch(event.type)
		{
		case KEYEVENT_RAWKEYDOWN:
		case KEYEVENT_KEYDOWN:
			iMsg = event.is_system_key ? WM_SYSKEYDOWN : WM_KEYDOWN;
			break;
		case KEYEVENT_KEYUP:
			iMsg = event.is_system_key ? WM_SYSKEYUP : WM_KEYUP;
			break;
		case KEYEVENT_CHAR:
		default:
			iMsg = event.is_system_key ? WM_SYSCHAR : WM_CHAR;
		}
		
		wParam = event.windows_key_code;
		lParam = event.native_key_code;
		scModifiers = ToScKeyboardModifiers(event.modifiers);

		Send(new ViewHostMsg_OnKeyEvent(m_WindowId, iMsg, wParam, lParam, scModifiers));
	}

	return false;
}

bool SubProcessWindow::OnBeforePopup(CefRefPtr< CefBrowser > browser, 
									 CefRefPtr< CefFrame > frame, 
									 const CefString& target_url, 
									 const CefString& target_frame_name, 
									 CefLifeSpanHandler::WindowOpenDisposition target_disposition, 
									 bool user_gesture, const CefPopupFeatures& popupFeatures, 
									 CefWindowInfo& windowInfo, 
									 CefRefPtr< CefClient >& client, 
									 CefBrowserSettings& settings, bool* no_javascript_access )
{
	// We don't support popups - allow the app to open in an external browser.
	std::string url(target_url);
	Send(new ViewHostMsg_OnUrlRequested(m_WindowId, url));

	// Return true to set this request as handled.
	return true;
}

RgscWndProcMap* FindWndProc(HWND h)
{
	auto search = s_WndProcs.find(h);
	if(search != s_WndProcs.end()) 
	{
		return &search->second;
	}
	else 
	{
		return NULL;
	}
}

LRESULT CALLBACK SubProcessWindow::HandleHitTest(SubProcessWindow* window, HWND hwnd, WPARAM wParam, LPARAM lParam, RgscRectRetrievalMethod rectMethod, RgscHitTestBottomRightBehaviour bLowerRightOverride)
{
	// safety check - early out
	if (window == NULL)
	{
		return 0;
	}

	// during modal dialog mode, we don't want any regions considered for borders/captions
	if (window->m_bIsModal)
	{
		return HTCLIENT;
	}

	// lp - point from the lParams - could be in client or screen space
	POINT lp;

	// clientPoint - mouse point in client space.
	POINT clientPoint;

#define GET_X_LPARAM(lp)                        ((int)(short)LOWORD(lp))
#define GET_Y_LPARAM(lp)                        ((int)(short)HIWORD(lp))

	lp.x = GET_X_LPARAM(lParam);
	lp.y = GET_Y_LPARAM(lParam);

	RECT rect;

	// If the hit test is being done in client space, get the client rect and assign the client point.
	if (rectMethod == USE_CLIENT_RECT)
	{
		::GetClientRect(hwnd, &rect);
		clientPoint = lp;
	}
	else
	{
		// hit testing is done relative to the screen, so get the window rect
		::GetWindowRect(hwnd, &rect);
		
		// hit testing is done relative to the screen, so map the screen point to the client point.
		clientPoint = lp;
		::ScreenToClient(hwnd, &clientPoint);
	}

	// grab the border width from the window config
	const int border_width = window->m_BorderWidth;

	// bottom left corner
	if (window->m_ResizeEdges & WE_BOTTOM && window->m_ResizeEdges & WE_LEFT &&
		lp.x >= rect.left && lp.x < rect.left + border_width && lp.y <= rect.bottom && lp.y > rect.bottom - border_width) 
	{
		return HTBOTTOMLEFT;
	}

	// bottom right corner - use the "size grip" size, since generally windows are expected to be easier to drag from the lower right
	if (window->m_ResizeEdges & WE_BOTTOM && window->m_ResizeEdges & WE_RIGHT &&
		lp.x <= rect.right && lp.x > rect.right - window->m_SizeGripSize && lp.y < rect.bottom && lp.y > rect.bottom - window->m_SizeGripSize) 
	{
		// NOTE:
		//	Something very weird going on internally to CEF/chrome when returning HTBOTTOMRIGHT. Overriding it with
		//	HTTOPLEFT uses the same cursor, and allows to send the proper result later.
		if (bLowerRightOverride == OVERRIDE)
		{
			return HTTOPLEFT;
		}
		else
		{
			return HTBOTTOMRIGHT;
		}
	}

	// top left corner
	if (window->m_ResizeEdges & WE_TOP && window->m_ResizeEdges & WE_LEFT &&
		lp.x >= rect.left && lp.x < rect.left + border_width && lp.y >= rect.top && lp.y < rect.top + border_width) 
	{
		return HTTOPLEFT;
	}

	// top right corner
	if (window->m_ResizeEdges & WE_TOP && window->m_ResizeEdges & WE_RIGHT &&
		lp.x <= rect.right && lp.x > rect.right - border_width && lp.y >= rect.top && lp.y < rect.top + border_width) 
	{
		return HTTOPRIGHT;
	}

	// left border
	if (window->m_ResizeEdges & WE_LEFT && lp.x >= rect.left && lp.x < rect.left + border_width) 
	{
		return HTLEFT;
	}

	// right border
	if (window->m_ResizeEdges & WE_RIGHT && lp.x <= rect.right && lp.x > rect.right - border_width) 
	{
		return HTRIGHT;
	}

	// bottom border
	if (window->m_ResizeEdges & WE_BOTTOM && lp.y <= rect.bottom && lp.y > rect.bottom - border_width) 
	{
		return HTBOTTOM;
	}

	// top border
	if (window->m_ResizeEdges & WE_TOP && lp.y >= rect.top && lp.y < rect.top + border_width) 
	{
		return HTTOP;
	}

	// If the mouse is within the top 'm_CaptionHeight' pixels, we treat it as HTCAPTION.
	//	The SCUI also sends an event when the mouse is over the regions of the caption area that we consider for dragging.
	//	However, we do allow for user-defined exclusion zones that should remain in client space (HTCLIENT) not defined by the SCUI.
	if (window->m_bIsMouseInCaptionRegion && lp.y >= rect.top && lp.y < rect.top + window->m_CaptionHeight)
	{
		for (unsigned int i = 0; i < window->m_CaptionExclusions.size(); i++)
		{
			CaptionExclusion& c = window->m_CaptionExclusions[i];

			// Get the client rect so we can map the exclusion rect
			RECT clientRect;
			::GetClientRect(hwnd, &clientRect);

			// Create the exclusion rect by taking the caption exclusion, and offsetting it
			//	from the client rect using the windows edge flags.
			RECT captionRect;
			if (c.referencePoint & WE_RIGHT)
			{
				captionRect.right = clientRect.right - c.x;
				captionRect.left = clientRect.right - c.x - c.width;
			}
			else
			{
				captionRect.left = clientRect.left + c.x;
				captionRect.right = clientRect.left + c.x + c.width;
			}

			if (c.referencePoint & WE_BOTTOM)
			{
				captionRect.bottom = clientRect.bottom - c.y;
				captionRect.top = clientRect.bottom - c.y - c.height;
			}
			else
			{
				captionRect.top = clientRect.top + c.y;
				captionRect.bottom = clientRect.top + c.y + c.height;
			}

			// If the client mouse point (clientPoint) is within the caption rect, return the client area.
			if (PtInRect(&captionRect, clientPoint))
			{
				return HTCLIENT;
			}
		}

		return HTCAPTION;
	}

	return HTCLIENT;
}

LRESULT CALLBACK SubProcessWindow::WindowsMessageHandler(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	RgscWndProcMap* p = FindWndProc(hwnd);
	if (p == NULL)
	{
		return DefWindowProc(hwnd, iMsg, wParam, lParam);
	}

	SubProcessWindow* window = (SubProcessWindow*)p->m_UserData;
	if (window == NULL)
	{
		return DefWindowProc(hwnd, iMsg, wParam, lParam);
	}

	if (iMsg == WM_NCCALCSIZE)
	{
		// All subprocess windows have an empty (frameless) non-client region.
		return 0;
	}
	else if (iMsg == WM_LBUTTONDOWN)
	{
		LRESULT result = HandleHitTest(window, hwnd, wParam, lParam, USE_CLIENT_RECT, NORMAL);
		if (result == HTCAPTION)
		{
			ReleaseCapture();
			SendMessageA(window->m_hRootParent, WM_NCLBUTTONDOWN, HTCAPTION, 0);
			return 0;
		}
	}
	else if (iMsg == WM_LBUTTONDBLCLK)
	{
		// Determine which area of the window was clicked.
		LRESULT result = HandleHitTest(window, hwnd, wParam, lParam, USE_CLIENT_RECT, OVERRIDE);

		// If it was double clicked in the caption region...
		if (result == HTCAPTION)
		{
			SendMessageA(window->m_hRootParent, WM_NCLBUTTONDBLCLK, result, 0);
			return 0;
		}
		else if (result != HTCLIENT)
		{
			return result;
		}
	}
	else if (iMsg == WM_MOUSEMOVE)
	{
		// WM_MOUSEMOVE comes in client coordinates
		window->m_MouseX = GET_X_LPARAM(lParam); 
		window->m_MouseY = GET_Y_LPARAM(lParam); 

		// Notify the parent that the mouse is in use (to bail out of gamepad input)
		window->Send(new ViewHostMsg_OnMouseMoved(window->m_WindowId, iMsg, wParam, lParam));
	}
	else if (iMsg == WM_NCMOUSEMOVE)
	{
		// WM_NCMOUSEMOVE comes in screen coordinates
		POINT p;
		p.x = GET_X_LPARAM(lParam); 
		p.y = GET_Y_LPARAM(lParam);
		
		
		// convert to client coordinates
		::ScreenToClient(window->m_hWnd, &p);

		// capture mouse position
		window->m_MouseX = p.x;
		window->m_MouseX = p.y;

		// Notify the parent that the mouse is in use (to bail out of gamepad input)
		window->Send(new ViewHostMsg_OnMouseMoved(window->m_WindowId, iMsg, wParam, lParam));
	}
	else if (iMsg == WM_NCHITTEST)
	{
		//	Handle hit test messages by applying our custom caption height and border widths.
		LRESULT result = HandleHitTest(window, hwnd, wParam, lParam, USE_WINDOW_RECT, OVERRIDE);

		// For now -- treat HTCAPTION as the client area.
		// This lets our chrome window javascript fire events determining if the mouse is in a draggable area.
		// Once the actual left mouse button is clicked, we'll do a followup hit test to determine how to handle the event.
		if (result == HTCAPTION)
		{
			return HTCLIENT;
		}
		else if (result != HTCLIENT)
		{
			// All clicks outside of the client and capture area are transparent and sent to our parent.
			return HTTRANSPARENT;
		}
		else
		{
			// Should just be HTCLIENT.
			return result;
		}
	}
	else if (iMsg == WM_SIZE)
	{
		int width = GET_X_LPARAM(lParam);
		int height = GET_Y_LPARAM(lParam);

		window->Send(new ViewHostMsg_OnSizeChanged(window->m_WindowId, width, height));
	}
	else if (iMsg >= WM_KEYFIRST && iMsg <= WM_KEYLAST)
	{
		::PostMessage((HWND)p->m_hParent, iMsg, wParam, lParam);
	}

	// For all other messages, allow the original window proc to handle the results.
	return CallWindowProc(p->m_OriginalProc, hwnd, iMsg, wParam, lParam);
}

BOOL CALLBACK SubProcessWindow::SubclassChildren(HWND hWnd, LPARAM lp)
{
	WNDPROC proc = (WNDPROC)SetWindowLongPtr(hWnd, GWLP_WNDPROC, (LONG_PTR)WindowsMessageHandler);
	if (proc == 0)
	{
		// stop enumerating if we fail to subclass
		return FALSE;
	}
	else
	{
		SubProcessWindow* window = (SubProcessWindow*)lp;
		s_WndProcs[hWnd] = RgscWndProcMap(hWnd, proc, window->m_hRootParent, window);
		return TRUE;
	}
}

void SubProcessWindow::OnAfterCreated(CefRefPtr<CefBrowser> browser)
{
  REQUIRE_UI_THREAD();

  if(!m_Browser.get())   
  {
	  m_Browser = browser;
	  m_BrowserId = browser->GetIdentifier();

	  if(m_Url.length() > 0)
	  {
		  m_Browser->GetMainFrame()->LoadURL(m_Url);
	  }
  }

  // Get the browser's window handle.
  HWND browserHWND = m_Browser->GetHost()->GetWindowHandle();
  HWND optionalParentHWND = 0;

  // When we create the window as a child of another window, we need to intercept windows messages
  // so that we can add additional logic that will ignore mouse preferences near the edge of the client area.
  if (m_WindowType == BORDERLESS)
  {
	  m_BorderlessWindow.SetBrowserHandle(browserHWND);

	  // Subclass the browser window
	  WNDPROC proc = (WNDPROC)SetWindowLongPtr(browserHWND, GWLP_WNDPROC, (LONG_PTR)WindowsMessageHandler);

	  // Subclass the internal chrome windows
	  s_WndProcs[browserHWND] = RgscWndProcMap(browserHWND, proc, m_hRootParent, this);
	  EnumChildWindows(browserHWND, SubclassChildren, (LPARAM)this);

	  // Return the borderless window (our root window) to the client.
	  optionalParentHWND = m_BorderlessWindow.GetWindowHandle();
  }

  Send(new ViewHostMsg_OnWindowCreated(m_WindowId, (uint64)browserHWND, (uint64)optionalParentHWND));

//   if(!message_router_) {
//     // Create the browser-side router for query handling.
//     CefMessageRouterConfig config;
//     message_router_ = CefMessageRouterBrowserSide::Create(config);
// 
//     // Register handlers with the router.
//     CreateMessageHandlers(message_handler_set_);
//     MessageHandlerSet::const_iterator it = message_handler_set_.begin();
//     for (; it != message_handler_set_.end(); ++it)
//       message_router_->AddHandler(*(it), false);
//   }
// 
//   // Disable mouse cursor change if requested via the command-line flag.
//   if(m_bMouseCursorChangeDisabled)
//     browser->GetHost()->SetMouseCursorChangeDisabled(true);
// 
//   AutoLock lock_scope(this);
//   if(!m_Browser.get())   {
//     // We need to keep the main child window, but not popup windows
//     m_Browser = browser;
//     m_BrowserId = browser->GetIdentifier();
//   } else if(browser->IsPopup()) {
//     // Add to the list of popup browsers.
//     m_PopupBrowsers.push_back(browser);
// 
//     // Give focus to the popup browser.
//     browser->GetHost()->SetFocus(true);
//   }
// 
//   m_BrowserCount++;
}

bool SubProcessWindow::DoClose(CefRefPtr<CefBrowser> browser)
{
  REQUIRE_UI_THREAD();

  // Closing the main window requires special handling. See the DoClose()
  // documentation in the CEF header for a detailed description of this
  // process.
  if(m_BrowserId == browser->GetIdentifier()) 
  {
	  Send(new ViewHostMsg_OnWindowClosing(m_WindowId));
	  m_bIsClosing = true;
   }

  // Allow the close. For windowed browsers this will result in the OS close
  // event being sent.
  return false;
}

void SubProcessWindow::OnBeforeClose(CefRefPtr<CefBrowser> browser)
{
  REQUIRE_UI_THREAD();
  //OutputDebugStringA("SubProcessWindow::OnBeforeClose");

//  message_router_->OnBeforeClose(browser);

  if(m_BrowserId == browser->GetIdentifier()) 
  {
		// Free the browser pointer so that the browser can be destroyed
		m_Browser = NULL;

//     if(m_OSRHandler.get()) {
//       m_OSRHandler->OnBeforeClose(browser);
//       m_OSRHandler = NULL;
//     }
  } else if(browser->IsPopup()) {
    // Remove from the browser popup list.
//     BrowserList::iterator bit = m_PopupBrowsers.begin();
//     for (; bit != m_PopupBrowsers.end(); ++bit) {
//       if((*bit)->IsSame(browser)) {
//         m_PopupBrowsers.erase(bit);
//         break;
//       }
//     }
  }

  //if(--m_BrowserCount == 0) {
    // All browser windows have closed.
    // Remove and delete message router handlers.
//     MessageHandlerSet::const_iterator it = message_handler_set_.begin();
//     for (; it != message_handler_set_.end(); ++it) {
//       //message_router_->RemoveHandler(*(it));
//       delete *(it);
//     }
//     message_handler_set_.clear();
    //message_router_ = NULL;

    // Quit the application message loop.
    //AppQuitMessageLoop();
  //}
}

void SubProcessWindow::SetZoomLevel()
{
	if(!m_Browser.get())
	{
		return;
	}

	double zoomLevel = log(m_Scale) / log(1.2);
	m_Browser->GetHost()->SetZoomLevel(zoomLevel);
}

void SubProcessWindow::OnLoadingStateChange(CefRefPtr<CefBrowser> browser,
                                         bool isLoading,
                                         bool canGoBack,
                                         bool canGoForward)
{
	REQUIRE_UI_THREAD();

	if(m_Browser.get())
	{
		m_Browser->GetHost()->SendFocusEvent(true);
		SetZoomLevel();
	}

	Send(new ViewHostMsg_OnLoadingStateChanged(m_WindowId, isLoading));
}

void SubProcessWindow::OnLoadError(CefRefPtr<CefBrowser> browser,
                                CefRefPtr<CefFrame> frame,
                                ErrorCode errorCode,
                                const CefString& errorText,
                                const CefString& failedUrl)
{
	REQUIRE_UI_THREAD();

	Send(new ViewHostMsg_OnProvisionalLoadError(m_WindowId, std::string(failedUrl), errorCode, std::wstring(errorText), frame->IsMain()));
}

bool SubProcessWindow::OnBeforeBrowse(CefRefPtr<CefBrowser> browser,
                                   CefRefPtr<CefFrame> frame,
                                   CefRefPtr<CefRequest> request,
                                   bool is_redirect)
{
	CefRequest::TransitionType type = request->GetTransitionType();

	// Block Forward+Back navigation
	if (type & TT_FORWARD_BACK_FLAG)
	{
		return true;
	}

	// Block Form Submits
	if (type == TT_FORM_SUBMIT)
	{
		return true;
	}

	bool bValid = false;
	
	CefString requestUrl = request->GetURL();

	// We're only filtering requests made to the main browser frame.
	if (browser->GetMainFrame()->GetIdentifier() == frame->GetIdentifier())
	{
		// Check if this is a URL we've explicitly loaded.
		if (m_Url == requestUrl)
		{
			bValid = true;
		}

		// Check if this URL was loaded from our offline pack file.
		//	The archive providers will substitute our offline root into the request url.
		//	A request to 'C:\Program Files\Rockstar Games\Social Club Debug\UI\default.html' becomes:
		//	  m_Url			= "C:\Program Files\Rockstar Games\Social Club Debug\UI\default.html"
		//	  m_OfflineRoot = "file:///C:/Program Files/Rockstar Games/Social Club/UI/"
		//	  requestUrl	= "file:///C:/Program Files/Rockstar Games/Social Club/UI/default.html"
		if (!m_OfflineRoot.empty())
		{
			if (StartsWith(requestUrl, m_OfflineRoot, false))
			{
				bValid = true;
			}
		}
	}
	else
	{
		// We'll trust that all requests to another frame is valid
		bValid = true;
	}

	// If the URL is valid, return 'false' which means this request was not filtered.
	if (bValid)
	{
		return false;
	}
	else
	{
		// Allow the app to load the url externally.
		Send(new ViewHostMsg_OnUrlRequested(m_WindowId, std::string(requestUrl)));

		// Return 'true' to filter the request.
		return true;
	}
}

cef_return_value_t SubProcessWindow::OnBeforeResourceLoad( CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request, CefRefPtr<CefRequestCallback> callback)
{
#if RSG_DEV
	if (!m_DisableCaptchaHeader.empty() && !m_DisableCaptchaHeaderValue.empty())
	{
		// Apply the captcha header to login requests
		if (strstr(request->GetURL().ToString().c_str(), "login") != NULL)
		{
			CefRequest::HeaderMap hMap;
			request->GetHeaderMap(hMap);
			hMap.insert( std::make_pair(m_DisableCaptchaHeader, m_DisableCaptchaHeaderValue));
			request->SetHeaderMap(hMap);
		}
	}
#endif

	return m_ResourceManager->OnBeforeResourceLoad(browser, frame, request, callback);
}

#if !__NO_OUTPUT
std::string SubProcessWindow::DumpRequestContents(CefRefPtr<CefRequest> request)
{
	std::stringstream ss;

	ss << "URL: " << std::string(request->GetURL());
	ss << "\nMethod: " << std::string(request->GetMethod());

	CefRequest::HeaderMap headerMap;
	request->GetHeaderMap(headerMap);
	if(headerMap.size() > 0)
	{
		ss << "\nHeaders:";
		CefRequest::HeaderMap::const_iterator it = headerMap.begin();
		for(; it != headerMap.end(); ++it)
		{
			ss << "\n\t" << std::string((*it).first) << ": " <<
			   std::string((*it).second);
		}
	}
	
	CefRefPtr<CefPostData> postData = request->GetPostData();
	if(postData.get())
	{
		CefPostData::ElementVector elements;
		postData->GetElements(elements);
		if(elements.size() > 0)
		{
			ss << "\nPost Data:";
			CefRefPtr<CefPostDataElement> element;
			CefPostData::ElementVector::const_iterator it = elements.begin();
			for(; it != elements.end(); ++it)
			{
				element = (*it);
				if(element->GetType() == PDE_TYPE_BYTES)
				{
					// the element is composed of bytes
					ss << "\n\tBytes: ";
					if(element->GetBytesCount() == 0)
					{
						ss << "(empty)";
					}
					else
					{
						// retrieve the data.
						size_t size = element->GetBytesCount();
						char* bytes = new char[size];
						element->GetBytes(size, bytes);
						ss << std::string(bytes, size);
						delete [] bytes;
					}
				}
				else if(element->GetType() == PDE_TYPE_FILE)
				{
					ss << "\n\tFile: " << std::string(element->GetFile());
				}
			}
		}
	}

	return ss.str();
}

std::string SubProcessWindow::DumpResponseContents(CefRefPtr<CefRequest> request, CefRefPtr<CefResponse> response, int* status)
{
	std::stringstream ss;

	if(status)
	{
		*status = response->GetStatus();
	}

	ss << "URL: " << std::string(request->GetURL());
	ss << "\nStatus: " << response->GetStatus() << " " << std::string(response->GetStatusText());

	CefResponse::HeaderMap headerMap;
	response->GetHeaderMap(headerMap);
	if(headerMap.size() > 0)
	{
		ss << "\nHeaders:";
		CefResponse::HeaderMap::const_iterator it = headerMap.begin();
		for(; it != headerMap.end(); ++it)
		{
			ss << "\n\t" << std::string((*it).first) << ": " <<
			   std::string((*it).second);
		}
	}

	return ss.str();
}
#endif

CefRefPtr<CefResourceHandler> SubProcessWindow::GetResourceHandler(
    CefRefPtr<CefBrowser> browser,
    CefRefPtr<CefFrame> frame,
    CefRefPtr<CefRequest> request)
{
#if !__NO_OUTPUT
	std::string contents = DumpRequestContents(request);
	Send(new ViewHostMsg_DebugRequestContents(m_WindowId, contents));
#endif

#if 0
	std::string url = request->GetURL();
	if(url.find(kTestOrigin) == 0) {
		// Handle URLs in the test origin.
		std::string file_name, mime_type;
		if(ParseTestUrl(url, &file_name, &mime_type)) {
			if(file_name == "request.html") {
				// Show the request contents.
				std::string dump;
				DumpRequestContents(request, dump);
				std::string str = "<html><body bgcolor=\"white\"><pre>" + dump +
					"</pre></body></html>";
				CefRefPtr<CefStreamReader> stream =
					CefStreamReader::CreateForData(
					static_cast<void*>(const_cast<char*>(str.c_str())),
					str.size());
				Assert(stream.get());
				return new CefStreamResourceHandler("text/html", stream);
			} else {
				// Load the resource from file.
				CefRefPtr<CefStreamReader> stream =
					GetBinaryResourceReader(file_name.c_str());
				if(stream.get())
					return new CefStreamResourceHandler(mime_type, stream);
			}
		}
	}
#endif

	ResourceRequestJob* job = m_ResourceInterceptor.MaybeIntercept(request->GetURL().ToString().c_str(), (unsigned)request->GetURL().ToString().size());
	if(job)
	{
		CefRefPtr<CefResourceHandler> handler = NULL;
		CefRefPtr<CefStreamReader> stream = job->GetStream();
		if (stream != NULL)
		{
			const char* mimeType = job->GetMimeType();
			handler = new CefStreamResourceHandler(mimeType, stream);
		}

		job->Release();
		return handler;
	}
	else
	{
		return m_ResourceManager->GetResourceHandler(browser, frame, request);
	}
}

bool SubProcessWindow::OnResourceResponse(CefRefPtr<CefBrowser> browser,
											CefRefPtr<CefFrame> frame,
											CefRefPtr<CefRequest> request,
											CefRefPtr<CefResponse> response)
{
#if !__NO_OUTPUT
	int status = 0;
	std::string contents = DumpResponseContents(request, response, &status);
	Send(new ViewHostMsg_DebugResponseContents(m_WindowId, status, contents));
#endif

	// false tells Chrome to load the response normally without redirection
	return false;
}

bool SubProcessWindow::OnCertificateError(CefRefPtr<CefBrowser> browser, cef_errorcode_t cert_error, const CefString& request_url, CefRefPtr<CefSSLInfo> ssl_info, CefRefPtr<CefRequestCallback> callback)
{
#if !__NO_OUTPUT
	Send(new ViewHostMsg_CertificateError(m_WindowId, cert_error)); 
#endif

	// false tells Chrome to deny the request immediately
	return false;
}

bool SubProcessWindow::OnOpenURLFromTab(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& target_url, CefRequestHandler::WindowOpenDisposition target_disposition, bool user_gesture)
{
	Send(new ViewHostMsg_OnUrlRequested(m_WindowId, std::string(target_url)));

	// Return true to cancel the navigation.
	// Return false to allow the navigation to proceed in the source browser's top-level frame.
	return true;
}

// bool SubProcessWindow::OnQuotaRequest(CefRefPtr<CefBrowser> browser,
//                                    const CefString& origin_url,
//                                    int64 new_size,
//                                    CefRefPtr<CefQuotaCallback> callback) {
//   static const int64 max_size = 1024 * 1024 * 20;  // 20mb.
// 
//   // Grant the quota request if the size is reasonable.
//   callback->Continue(new_size <= max_size);
//   return true;
// }

void SubProcessWindow::OnProtocolExecution(CefRefPtr<CefBrowser> browser,
                                        const CefString& url,
                                        bool& allow_os_execution)
{
//   std::string urlStr = url;
// 
//   // Allow OS execution of Spotify URIs.
//   if(urlStr.find("spotify:") == 0)
//     allow_os_execution = true;
}

void SubProcessWindow::OnRenderProcessTerminated(CefRefPtr<CefBrowser> browser,
                                              TerminationStatus status)
{
	Send(new ViewHostMsg_OnRendererCrashed(m_WindowId));
  //message_router_->OnRenderProcessTerminated(browser);
}

bool SubProcessWindow::OnSetFocus(CefRefPtr<CefBrowser> browser, CefFocusHandler::FocusSource source )
{
	// CEF explicitly calls SetFocus() when loading a URL - pulling focus away from another window.
	if (source == FOCUS_SOURCE_NAVIGATION)
	{
		// For child/popup windows, if we're not visible, reject this focus shift.
		if (m_WindowType != WINDOWLESS && !IsWindowVisible(m_hWnd))
		{
			// Return true to reject the focus shift.
			return true;
		}
	}

	// Return false to allow the focus to be set 
	return false;
}

bool SubProcessWindow::GetRootScreenRect(CefRefPtr<CefBrowser> browser, CefRect& rect)
{
//   if(!m_OSRHandler.get())
//     return false;
//   return m_OSRHandler->GetRootScreenRect(browser, rect);
	rect = CefRect(0, 0, m_Width, m_Height);
	return true;
}

bool SubProcessWindow::GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect)
{
//   if(!m_OSRHandler.get())
//     return false;
//   return m_OSRHandler->GetViewRect(browser, rect);

	rect.x = rect.y = 0;
	rect.width = m_Width;
	rect.height = m_Height;
	return true;
}

bool SubProcessWindow::GetScreenPoint(CefRefPtr<CefBrowser> browser,
                                   int viewX,
                                   int viewY,
                                   int& screenX,
                                   int& screenY)
{
//   if(!m_OSRHandler.get())
//     return false;
//   return m_OSRHandler->GetScreenPoint(browser, viewX, viewY, screenX, screenY);
	screenX = viewX;
	screenY = viewY;
	return true;
}

bool SubProcessWindow::GetScreenInfo(CefRefPtr<CefBrowser> browser,
                                  CefScreenInfo& screen_info)
{
//   if(!m_OSRHandler.get())
//     return false;
//   return m_OSRHandler->GetScreenInfo(browser, screen_info);
	return false;
}


void SubProcessWindow::onWidgetCreated(CefRefPtr<CefBrowser> browser)
{
#if SUPPORT_WIDGETS
	/* About widgets:
	   Widgets are 2D overlays over the main window.
	   Dropdown lists are implemented as widgets.
	   Widgets can be positioned in a way that overlaps the edges of the main window
	   (such as when a dropdown list is opened near the bottom of a web browser window).
	   I only handle the case where one widget is active at a time. Since you can't
	   open more than one dropdown list at time, this should work fine.
	*/

	//Displayf("onWidgetCreated");

	if(m_Widget)
	{
		// If you have a dropdown list open, and you click another dropdown list,
		// you get an onWidgetCreated() call before onWidgetDestroy(). To handle
		// this, I just destroy the old widget before creating the new one.
		onWidgetDestroyed(browser);
	}

	if(m_Widget == false)
	{
		m_Widget = true;
		m_WidgetState = WIDGET_CREATED;
	}
#else
	AssertMsg(false, "Widgets (i.e. native OS dropdown lists) are not supported. #define SUPPORT_WIDGETS to 1 if they're needed.");
#endif
}

void SubProcessWindow::onWidgetDestroyed(CefRefPtr<CefBrowser> browser)
{
#if SUPPORT_WIDGETS
	//Displayf("onWidgetDestroyed");

	m_Widget = false;
	VirtualFree(m_WidgetBuffer,0,MEM_RELEASE);
	m_WidgetBuffer = NULL;
	m_WidgetBufferSize = 0;

	ClearPopupRects();
	browser->GetHost()->Invalidate(PET_VIEW);
#endif
}

void SubProcessWindow::onWidgetResize(const CefRect& rect)
{

#if SUPPORT_WIDGETS
	if (rect.width <= 0 || rect.height <= 0)
	{
		return;
	}
	original_popup_rect_ = rect;
	popup_rect_ = GetPopupRectInWebView(original_popup_rect_);

	//Displayf("onWidgetResize");

	AssertMsg(m_WidgetBuffer == NULL, "We don't support resizing of already-sized widgets");

	m_WidgetState = WIDGET_RESIZED;

	if(m_WidgetBuffer == NULL)
	{
		m_WidgetBufferSize = rect.width * rect.height * 4;
		m_WidgetBuffer = (unsigned char*)VirtualAlloc(NULL, m_WidgetBufferSize, MEM_COMMIT, PAGE_READWRITE);
	}
#endif
}

bool SubProcessWindow::onWidgetPaint(CefRefPtr<CefBrowser> browser,
									 PaintElementType type,
									 const RectList& dirtyRects,
									 const void* bufferIn,
									 int width,
									 int height)
{
#if SUPPORT_WIDGETS
	if(!AssertVerify(m_WidgetBuffer))
	{
		return false;
	}

	m_WidgetState = WIDGET_PAINTED;

	if((width > (int)popup_rect_.width) || (height > (int)popup_rect_.height))
	{
		return false;
	}

	bool updated = false;
	const int kBytesPerPixel = 4;
	unsigned char* buffer = m_WidgetBuffer;

	unsigned int srcPitch = width * kBytesPerPixel;
	unsigned int destPitch = width * kBytesPerPixel;

	int wid = width;
	int hig = height;
	int srcTop = 0;
	int srcLeft = 0;
	int dstTop = 0;
	int dstLeft = 0;

	const unsigned char* src = ((unsigned char*)bufferIn) + (srcLeft + (srcTop * width)) * kBytesPerPixel;
	unsigned char* dest = buffer + (dstLeft + (dstTop * width)) * kBytesPerPixel;
	const unsigned int rowSizeInBytes = wid * kBytesPerPixel;

	for(int row = 0; row < hig; ++row)
	{
		Assert((dest + rowSizeInBytes) <= (buffer + m_WidgetBufferSize));
		Assert(dest >= buffer);

		memcpy(dest, src, rowSizeInBytes);

		src += srcPitch;
		dest += destPitch;
	}

	updated = hig > 0;

	return updated;
#endif
}

void SubProcessWindow::OnPopupShow(CefRefPtr<CefBrowser> browser, bool show)
{
	if(show)
	{
		onWidgetCreated(browser);
	}
	else
	{
		onWidgetDestroyed(browser);
	}
}

void SubProcessWindow::OnPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect)
{
	onWidgetResize(rect);
}

CefRect SubProcessWindow::GetPopupRectInWebView(const CefRect& original_rect) {
  CefRect rc(original_rect);
  // if x or y are negative, move them to 0.
  if (rc.x < 0)
    rc.x = 0;
  if (rc.y < 0)
    rc.y = 0;
  // if popup goes outside the view, try to reposition origin
  if (rc.x + rc.width > (int)m_Width)
    rc.x = m_Width - rc.width;
  if (rc.y + rc.height > (int)m_Height)
    rc.y = m_Height - rc.height;
  // if x or y became negative, move them to 0 again.
  if (rc.x < 0)
    rc.x = 0;
  if (rc.y < 0)
    rc.y = 0;
  return rc;
}

void SubProcessWindow::ClearPopupRects() {
  popup_rect_.Set(0, 0, 0, 0);
  original_popup_rect_.Set(0, 0, 0, 0);
}

bool SubProcessWindow::renderToBuffer(CefRefPtr<CefBrowser> browser,
                            PaintElementType type,
                            const RectList& dirtyRects,
                            const void* bufferIn,
                            int width,
                            int height)
{
#if 0
 	char dbg[256];
 	sprintf(dbg, "OnPaint(): type: %s, width: %d, height: %d, numRects: %d\n", type == PaintElementType::PET_VIEW ? "view" : "popup", width, height, dirtyRects.size());
 	OutputDebugStringA(dbg);
#endif

	bool bail = (m_SharedMemory == NULL) || (m_SharedMemory->memory() == NULL) ||
				(bufferIn == NULL) || (width > (int)m_Width) || (height > (int)m_Height) ||
				(width <= 0) || (height <= 0) || (dirtyRects.size() < 1) ||
				((type != PET_VIEW) && (type != PET_POPUP));

	if(bail)
	{
		return false;
	}

	bool updated = false;
	const int kBytesPerPixel = 4;
	unsigned char* buffer = (unsigned char*)m_SharedMemory->memory() + getDirtyTileBufferSizeInBytes();

	unsigned int srcPitch = width * kBytesPerPixel;
	unsigned int destPitch = m_Width * kBytesPerPixel;

	std::vector<RgscRect> copy_rects;

	if(type == PET_VIEW)
	{
		for(CefRenderHandler::RectList::const_iterator i = dirtyRects.begin(); i != dirtyRects.end(); ++i)
		{
			const CefRect& rect = *i;

			int wid = rect.width;
			int hig = rect.height;
			int top = rect.y;
			int left = rect.x;

			const unsigned char* src = ((unsigned char*)bufferIn) + (left + (top * width)) * kBytesPerPixel;
			unsigned char* dest = buffer + (left + (top * m_Width)) * kBytesPerPixel;
			const unsigned int rowSizeInBytes = wid * kBytesPerPixel;

			for(int row = 0; row < hig; ++row)
			{
				Assert((dest + rowSizeInBytes) <= (buffer + getPixelBufferSizeInBytes()));
				Assert(dest >= buffer);

				memcpy(dest, src, rowSizeInBytes);

				src += srcPitch;
				dest += destPitch;
			}

			RgscRect rgscRect;
			rgscRect.mLeft = rect.x;
			rgscRect.mTop = rect.y;
			rgscRect.mWidth = rect.width;
			rgscRect.mHeight = rect.height;

			copy_rects.push_back(rgscRect);

#if 0
			sprintf(dbg, "\nDirty Rect: (l,t,width,height) = (%d, %d, %d, %d,)\n", rgscRect.left(), rgscRect.top(), rgscRect.width(), rgscRect.height());
			OutputDebugStringA(dbg);
#endif
		}
	}
	
	if((type == PET_POPUP) || (copy_rects.size() > 0))
	{
#if SUPPORT_WIDGETS
		if(m_Widget && m_WidgetBuffer && (m_WidgetState == WIDGET_PAINTED))
		{
			bool drawWidget = false;

			RgscRect rgscPopupRect;
			rgscPopupRect.mLeft = popup_rect_.x;
			rgscPopupRect.mTop = popup_rect_.y;
			rgscPopupRect.mWidth = popup_rect_.width;
			rgscPopupRect.mHeight = popup_rect_.height;

			if(type == PET_POPUP)
			{
				drawWidget = true;
			}
			else
			{
				// if we have an active widget and we just drew over top of it, then re-draw the widget now
				for(CefRenderHandler::RectList::const_iterator i = dirtyRects.begin(); i != dirtyRects.end(); ++i)
				{
					const CefRect& rect = *i;

					RgscRect copy_rect;
					copy_rect.mLeft = rect.x;
					copy_rect.mTop = rect.y;
					copy_rect.mWidth = rect.width;
					copy_rect.mHeight = rect.height;

					if(copy_rect.intersects(rgscPopupRect))
					{
						drawWidget = true;
						break;
					}
				}
			}

			if(drawWidget)
			{
				//Displayf("Redrawing widget to main window");

				int wid = rgscPopupRect.mWidth;
				int hig = rgscPopupRect.mHeight;
				int srcTop = 0;
				int srcLeft = 0;
				int dstTop = rgscPopupRect.mTop;
				int dstLeft = rgscPopupRect.mLeft;

				unsigned int srcPitch = wid * kBytesPerPixel;
				unsigned int destPitch = m_Width * kBytesPerPixel;

				const unsigned char* src = ((unsigned char*)m_WidgetBuffer) + (srcLeft + (srcTop * width)) * kBytesPerPixel;
				unsigned char* dest = buffer + (dstLeft + (dstTop * m_Width)) * kBytesPerPixel;
				const unsigned int rowSizeInBytes = wid * kBytesPerPixel;

				for(int row = 0; row < hig; ++row)
				{
					Assert((dest + rowSizeInBytes) <= (buffer + getPixelBufferSizeInBytes()));
					Assert(dest >= buffer);

					memcpy(dest, src, rowSizeInBytes);

					src += srcPitch;
					dest += destPitch;
				}

				copy_rects.push_back(rgscPopupRect);
			}
		}
#endif

		// update the dirty rects structure, which the DLL reads to determine which textures to update/render
		unsigned short* dirtyTileMem = (unsigned short*)m_SharedMemory->memory();

		for(size_t i = 0; i < m_TileRects.size(); ++i)
		{
			for(size_t j = 0; j < copy_rects.size(); ++j)
			{
				if(copy_rects[j].intersects(m_TileRects[i]))
				{
					dirtyTileMem[i]++;
				}
			}
		}

		updated = true;
	}

	return updated;
}

void SubProcessWindow::OnPaint(CefRefPtr<CefBrowser> browser,
									  PaintElementType type,
									  const RectList& dirtyRects,
									  const void* bufferIn,
									  int width,
									  int height)
{
#if SUPPORT_WIDGETS
	if(type == PET_POPUP)
	{
		// draw the popup (aka dropdown list) to the widget buffer
		onWidgetPaint(browser,
						type,
						dirtyRects,
						bufferIn,
						width,
						height);

		// draw the widget buffer to the main buffer
		renderToBuffer(browser,
						type,
						dirtyRects,
						bufferIn,
						width,
						height);
	}
	else
#endif
	if(type == PET_VIEW)
	{
		renderToBuffer(browser,
						type,
						dirtyRects,
						bufferIn,
						width,
						height);
	}
}

void SubProcessWindow::OnCursorChange(CefRefPtr<CefBrowser> browser,
									  CefCursorHandle cursor,
									  CursorType type,
									  const CefCursorInfo& custom_cursor_info)
{
	// an HCURSOR is a 64-bit value in 64-bit processes, so convert it to a uint64 to pass to the 64-bit dll
	HCURSOR hCursor = cursor;

	rgsc::RgscCursorType rgscCursor = CefCursorToRgscCursor(type);
	IPC_SEND(new ViewHostMsg_OnCursorUpdated(m_WindowId, (uint64)hCursor, (int)rgscCursor));
}

void SubProcessWindow::CloseAllBrowsers(bool force_close)
{
//   if(!CefCurrentlyOn(TID_UI)) {
//     // Execute on the UI thread.
//     CefPostTask(TID_UI,
//         NewCefRunnableMethod(this, &SubProcessWindow::CloseAllBrowsers,
//                              force_close));
//     return;
//   }
// 
//   if(!m_PopupBrowsers.empty()) {
//     // Request that any popup browsers close.
//     BrowserList::const_iterator it = m_PopupBrowsers.begin();
//     for (; it != m_PopupBrowsers.end(); ++it)
//       (*it)->GetHost()->CloseBrowser(force_close);
//   }
// 
//   if(m_Browser.get()) {
//     // Request that the main browser close.
//     m_Browser->GetHost()->CloseBrowser(force_close);
//   }
}

// std::string SubProcessWindow::GetLogFile() {
//   AutoLock lock_scope(this);
//   return m_LogFile;
// }

// void SubProcessWindow::SetLastDownloadFile(const std::string& fileName) {
//   AutoLock lock_scope(this);
//   m_LastDownloadFile = fileName;
// }
// 
// std::string SubProcessWindow::GetLastDownloadFile() {
//   AutoLock lock_scope(this);
//   return m_LastDownloadFile;
// }

// void SubProcessWindow::ShowDevTools(CefRefPtr<CefBrowser> browser) {
//   CefWindowInfo windowInfo;
//   CefBrowserSettings settings;
// 
// #if defined(OS_WIN)
//   windowInfo.SetAsPopup(browser->GetHost()->GetWindowHandle(), "DevTools");
// #endif
// 
//   browser->GetHost()->ShowDevTools(windowInfo, this, settings);
// }
// 
// void SubProcessWindow::CloseDevTools(CefRefPtr<CefBrowser> browser) {
//   browser->GetHost()->CloseDevTools();
// }

// void SubProcessWindow::BeginTracing() {
//   if(CefCurrentlyOn(TID_UI)) {
//     CefBeginTracing(CefString());
//   } else {
//     CefPostTask(TID_UI,
//         NewCefRunnableMethod(this, &SubProcessWindow::BeginTracing));
//   }
// }
// 
// void SubProcessWindow::EndTracing() {
//   if(CefCurrentlyOn(TID_UI)) {
//     class Client : public CefEndTracingCallback,
//                    public CefRunFileDialogCallback {
//      public:
//       explicit Client(CefRefPtr<ClientHandler> handler)
//           : handler_(handler) {
//         RunDialog();
//       }
// 
//       void RunDialog() {
//         static const char kDefaultFileName[] = "trace.txt";
//         std::string path = handler_->GetDownloadPath(kDefaultFileName);
//         if(path.empty())
//           path = kDefaultFileName;
// 
//         // Results in a call to OnFileDialogDismissed.
//         handler_->GetBrowser()->GetHost()->RunFileDialog(
//             FILE_DIALOG_SAVE, CefString(), path, std::vector<CefString>(),
//             this);
//       }
// 
//       virtual void OnFileDialogDismissed(
//           CefRefPtr<CefBrowserHost> browser_host,
//           const std::vector<CefString>& file_paths) OVERRIDE {
//         if(!file_paths.empty()) {
//           // File selected. Results in a call to OnEndTracingComplete.
//           CefEndTracingAsync(file_paths.front(), this);
//         } else {
//           // No file selected. Discard the trace data.
//           CefEndTracingAsync(CefString(), NULL);
//         }
//       }
// 
//       virtual void OnEndTracingComplete(const CefString& tracing_file) OVERRIDE {
//         handler_->SetLastDownloadFile(tracing_file.ToString());
//         handler_->SendNotification(NOTIFY_DOWNLOAD_COMPLETE);
//       }
// 
//      private:
//       CefRefPtr<ClientHandler> handler_;
// 
//       IMPLEMENT_REFCOUNTING(Callback);
//     };
// 
//     new Client(this);
//   } else {
//     CefPostTask(TID_UI,
//         NewCefRunnableMethod(this, &SubProcessWindow::EndTracing));
//   }
// }

// bool SubProcessWindow::Save(const std::string& path, const std::string& data) {
//   FILE* f = fopen(path.c_str(), "w");
//   if(!f)
//     return false;
//   size_t total = 0;
//   do {
//     size_t write = fwrite(data.c_str() + total, 1, data.size() - total, f);
//     if(write == 0)
//       break;
//     total += write;
//   } while (total < data.size());
//   fclose(f);
//   return true;
// }

// static
//void SubProcessWindow::CreateMessageHandlers(MessageHandlerSet& handlers) {
//   // Create the dialog test handlers.
//   dialog_test::CreateMessageHandlers(handlers);
// 
//   // Create the binding test handlers.
//   binding_test::CreateMessageHandlers(handlers);
// 
//   // Create the window test handlers.
//   window_test::CreateMessageHandlers(handlers);
//}

void SubProcessWindow::OnAddressChange(CefRefPtr<CefBrowser> browser,
                                    CefRefPtr<CefFrame> frame,
                                    const CefString& url)
{
	REQUIRE_UI_THREAD();

	if(m_BrowserId == browser->GetIdentifier() && frame->IsMain())
	{
		IPC_SEND(new ViewHostMsg_OnAddressBarChanged(m_WindowId, std::string(url)));
	}
}

void SubProcessWindow::OnTitleChange(CefRefPtr<CefBrowser> browser,
                                  const CefString& title)
{
	REQUIRE_UI_THREAD();

	IPC_SEND(new ViewHostMsg_OnTitleChanged(m_WindowId, std::wstring(title)));
}

void SubProcessWindow::SendNotification(NotificationType type)
{
//   UINT id;
//   switch (type) {
//   case NOTIFY_CONSOLE_MESSAGE:
//     id = ID_WARN_CONSOLEMESSAGE;
//     break;
//   case NOTIFY_DOWNLOAD_COMPLETE:
//     id = ID_WARN_DOWNLOADCOMPLETE;
//     break;
//   case NOTIFY_DOWNLOAD_ERROR:
//     id = ID_WARN_DOWNLOADERROR;
//     break;
//   default:
//     return;
//   }
//   PostMessage(m_MainHwnd, WM_COMMAND, id, 0);
}


} // namespace rgsc
