@echo on

cd /d %RAGE_DIR%\suite\src\rgsc\rgsc\

set OPTIONS=/D /F /R /Y
set RGSC_UI_DIR=..\UI

IF "%1"=="release" GOTO Release

:Debug

set DEST_DIR="%ProgramFiles(x86)%\Rockstar Games\Social Club Debug\"
set DEST_DIR_X64="%ProgramW6432%\Rockstar Games\Social Club Debug\"

xcopy "Debug\SocialClubHelper.exe" %DEST_DIR% %OPTIONS%
xcopy "Debug\SocialClubHelper.pdb" %DEST_DIR% %OPTIONS%
xcopy "Debug\SocialClubHelper.map" %DEST_DIR% %OPTIONS%

xcopy "x64\Debug\SocialClubHelper.exe" %DEST_DIR_X64% %OPTIONS%
xcopy "x64\Debug\SocialClubHelper.pdb" %DEST_DIR_X64% %OPTIONS%
xcopy "x64\Debug\SocialClubHelper.map" %DEST_DIR_X64% %OPTIONS%

GOTO End

:Release

set DEST_DIR="%ProgramFiles(x86)%\Rockstar Games\Social Club\"
set DEST_DIR_X64="%ProgramW6432%\Rockstar Games\Social Club\"

xcopy "Release\SocialClubHelper.exe" %DEST_DIR% %OPTIONS%
xcopy "Release\SocialClubHelper.pdb" %DEST_DIR% %OPTIONS%
xcopy "Release\SocialClubHelper.map" %DEST_DIR% %OPTIONS%

xcopy "x64\Release\SocialClubHelper.exe" %DEST_DIR_X64% %OPTIONS%
xcopy "x64\Release\SocialClubHelper.pdb" %DEST_DIR_X64% %OPTIONS%
xcopy "x64\Release\SocialClubHelper.map" %DEST_DIR_X64% %OPTIONS%

:End
pause
