#include "borderlesswindow.h"
#include "window.h"

// Windows includes
#include <windows.h>

#define CAN_USE_DWM _WIN64

#if CAN_USE_DWM
// Can only be enabled for the x64 subprocess - requires windows Vista
// LAN/MP3 only shipped 32-bit subprocess, GTAV+ requires x64 and has a min system of Vista
#include <propkey.h>
#include <propvarutil.h>
#include <propsys.h>
#include <dwmapi.h>
#include <Uxtheme.h>
#pragma comment (lib, "uxtheme.lib")
#pragma comment(lib, "dwmapi.lib")
#endif

namespace rgsc
{

BorderlessWindowWrapper::BorderlessWindowWrapper()
	: m_hWnd(0)
	, m_hBrowser(0)
	, m_hInstance(GetModuleHandleA(NULL))
	, m_bIsDwmCompositionEnabled(false)
	, m_bShadowEnabled(false)
	, m_MinimumHeight(0)
	, m_MinimumWidth(0)
	, m_MaximumWidth(0)
	, m_MaximumHeight(0)
	, m_Owner(NULL)
	, m_WindowState(WS_UNKNOWN)
{
	memset(&m_Geometry, 0, sizeof(m_Geometry));
}

BorderlessWindowWrapper::~BorderlessWindowWrapper()
{
	DetachFromWindow();
}

bool BorderlessWindowWrapper::Init(SubProcessWindow* owner, ViewMsg_Rect_Params& rect, ViewMsg_CreateWindow_Params& params)
{
	// cache the owner
	m_Owner = owner;

	// cache the setup window geometry
	m_Geometry.mLeft = rect.x;
	m_Geometry.mTop = rect.y;
	m_Geometry.mWidth = rect.width;
	m_Geometry.mHeight = rect.height;

	// cache the min/max limits. a default of zero means no limit.
	m_MinimumWidth = params.minWidth;
	m_MinimumHeight = params.minHeight;
	m_MaximumWidth = params.maxWidth;
	m_MaximumHeight = params.maxHeight;

	// Create the setup for the windows class. Use the same name for the title/windowclass.
	WNDCLASSEX wcx = { 0 };
	wcx.cbSize = sizeof( WNDCLASSEX );
	wcx.style = CS_HREDRAW | CS_VREDRAW;
	wcx.hInstance = m_hInstance;
	wcx.lpfnWndProc = WndProc;
	wcx.cbClsExtra	= 0;
	wcx.cbWndExtra	= 0;
	wcx.lpszClassName = params.windowTitle.c_str();
	wcx.hbrBackground = CreateSolidBrush(RGB( 0, 0, 0 ));
	wcx.hCursor = LoadCursor( m_hInstance, IDC_ARROW );

	// Initialize composition - if disabled, add the window style drop shadow.
#if CAN_USE_DWM
	m_bIsDwmCompositionEnabled = FALSE;
	::DwmIsCompositionEnabled(&m_bIsDwmCompositionEnabled);
	if (m_bIsDwmCompositionEnabled == FALSE)
#endif
	{
		wcx.style |= CS_DROPSHADOW;
	}

	// Register the widnow class
	if (!SUCCEEDED(RegisterClassExW(&wcx)))
		return false;

	// Create the window using borderless style.
	const DWORD BsStyle = WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_CLIPCHILDREN;
	m_hWnd = CreateWindowW( params.windowTitle.c_str(), params.windowTitle.c_str(), BsStyle, rect.x, rect.y, rect.width, rect.height, 0, 0, m_hInstance, nullptr );

	// If composition is disabled, clear the window theme for this application as well.
	//	url:bugstar:2916436 - Non-compositioned themes cause a variety of rendering irregularities.
	UpdateWindowTheme();

	// Set our userdata as ourselves, so in the WndProc callback we can cast back to this instance.
	SetWindowLongPtr( m_hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>( this ));

	return true;
}


bool FlashModalWindow(HWND windowHandle)
{
	FLASHWINFO info;
	info.hwnd = windowHandle;
	info.dwFlags = FLASHW_ALL;
	info.uCount = 3;
	info.dwTimeout = 100;
	info.cbSize = sizeof(info);

	return FlashWindowEx(&info) == TRUE;
}

// There is a windows message 0x313 that is sent when the taskbar's icon is right clicked to show a context menu.
//	http://stackoverflow.com/questions/10430377/winapi-undocumented-windows-message-0x0313-stable
#define WM_POPUPSYSTEMMENU 0x313

LRESULT BorderlessWindowWrapper::WndProc( HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam )
{
	long x, y;

#define GET_X_LPARAM(lp)                        ((int)(short)LOWORD(lp))
#define GET_Y_LPARAM(lp)                        ((int)(short)HIWORD(lp))

	BorderlessWindowWrapper* window = reinterpret_cast<BorderlessWindowWrapper*>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) );

	switch(iMsg)
	{
	case WM_NCCALCSIZE:
		if (window && window->IsMaximized())
		{
			// When a window is maximized, windows assumes the frame border will be present when setting its position.
			//	This frame border is defined by SM_CXSIZEFRAME and SM_CYSIZEFRAME. We need to offset the edges by
			//	removing these widths from the client rect
			if (wParam == TRUE)
			{
				// rgrc[0] defines the new coordinates of the window that has been moved or resized.
				RECT* newRect = &reinterpret_cast<NCCALCSIZE_PARAMS*>(lParam)->rgrc[0];
				if (newRect)
				{
					int frameX = GetSystemMetrics(SM_CXSIZEFRAME);
					newRect->left += frameX;
					newRect->right -= frameX;

					int frameY = GetSystemMetrics(SM_CYSIZEFRAME);
					newRect->top += frameY;
					newRect->bottom -= frameY;
				}
			}
		}
		return 0;

	case WM_SIZE: 
		if (window) 
		{
			x = GET_X_LPARAM(lParam);
			y = GET_Y_LPARAM(lParam);

			// update window geometry
			window->m_Geometry.mWidth = x;
			window->m_Geometry.mHeight = y;

			// call resize event
			window->OnResizeMove();
			window->OnWindowStateChanged(wParam);
		}
		break;

	case WM_MOVING:
		if (window)
		{
			// extract the new x,y coordinates from the WM_MOVING message
			RECT* r = (RECT*)lParam;
			if (r)
			{
				// update window geometry
				window->m_Geometry.mLeft = r->left;
				window->m_Geometry.mTop = r->top;
				window->OnResizeMove();
				return TRUE;
			}
		}
		break;

	case WM_MOVE:
		if (window)
		{
			// extract the new x,y coordinates from the WM_MOVE message
			x = GET_X_LPARAM(lParam);
			y = GET_Y_LPARAM(lParam);

			// update window geometry
			window->m_Geometry.mLeft = x;
			window->m_Geometry.mTop = y;
			window->OnResizeMove();

			return TRUE;
		}
		break;

	case WM_NCHITTEST:
		if (window)
		{
			// SubprocessWindow implements a custom hit test handler that will use drag height, caption regions, etc to identify the hit location.
			return SubProcessWindow::HandleHitTest(window->m_Owner, window->m_hWnd, wParam, lParam, SubProcessWindow::USE_WINDOW_RECT, SubProcessWindow::NORMAL);
		}
		break;

	case WM_POPUPSYSTEMMENU:
		if (window && window->GetOwner() && window->GetOwner()->GetModalBlocker() != 0)
		{
			// When in modal blocker mode, disable the popup system menu.
			return 0;
		}
		break;

	case WM_SYSCOMMAND: 
		if ( wParam == SC_KEYMENU && window && window->GetOwner() && window->GetOwner()->GetModalBlocker() != 0) 
		{
			// When in modal blocker mode, disable the f10 system menu.
			return 0;
		}
		break;

	case WM_GETMINMAXINFO:
		if (window) 
		{
			// Return the cached minimum and maximum values. If a value of zero is provided,
			// allow the min/max to use the default values.
			LPMINMAXINFO lpMMI = (LPMINMAXINFO)lParam;
			if (window->m_MinimumWidth != 0 && window->m_MinimumHeight != 0)
			{
				lpMMI->ptMinTrackSize.x = window->m_MinimumWidth;
				lpMMI->ptMinTrackSize.y = window->m_MinimumHeight;
			}

			if (window->m_MaximumWidth != 0 && window->m_MaximumHeight != 0)
			{
				lpMMI->ptMaxTrackSize.x = window->m_MaximumWidth;
				lpMMI->ptMaxTrackSize.y = window->m_MaximumHeight;
			}
		}
		break;

	case WM_WINDOWPOSCHANGING:
		if (window && window->GetOwner())
		{
			HWND blocker = window->GetOwner()->GetModalBlocker();
			if (blocker != 0)
			{
				// Force the blocker window to the top, but do not send any move, size or activation events.
				UINT uflags = SWP_NOMOVE | SWP_NOSIZE;
				
				// If we're already on top, don't activate the modal blocker (causes a notable flicker).
				if (::GetForegroundWindow() == window->GetWindowHandle())
				{
					uflags |= SWP_NOACTIVATE;
				}

				// force the modal blocker to be a 'topmost' always on top window to immediately place it on the very top.
				//	then clear this flag, so it simply becomes HWND_TOP. Sending HWND_TOP here normally does not seem to 
				//  guarantee topmost for our hwndInsertAfter call below.
				::SetWindowPos(blocker, HWND_TOPMOST, 0, 0, 0, 0, uflags);
				::SetWindowPos(blocker, HWND_NOTOPMOST, 0, 0, 0, 0, uflags);

				// Intercept our window changing method, and ensure we're inserted below the blocker.
				WINDOWPOS* pos = (WINDOWPOS*)lParam;
				pos->hwndInsertAfter = blocker;
			}
		}
		break;

	case WM_DWMCOMPOSITIONCHANGED:
		if (window)
		{
			// Update method when the DWM composition changes.
			window->OnDwmCompositionChanged();
		}
		break;

	case WM_PARENTNOTIFY:
		if (window)
		{
			// If we're notified of a child click event -- check for a modal blocker
			//	and if we have one, flash it.
			switch(wParam)
			{
			case WM_LBUTTONDOWN:
			case WM_RBUTTONDOWN:
			case WM_MBUTTONDOWN:
				{
					HWND blocker = window->GetOwner()->GetModalBlocker();
					if (blocker != 0)
					{
						FlashModalWindow(blocker);
					}
				}
			}
		}
		break;

	case WM_ACTIVATE:
		if (window)
		{
			if (wParam != WA_INACTIVE)
			{
				// If we're being activated but we have a modal blocker -- flash the modal window.
				//	The use case here is when we're on screen and the modal blocker is above us --
				//	if the user clicks the modal blocker, we want to flash it to indicate it must be
				//  actioned on. The WM_WINDOWPOSCHANGING handler will keep it on top.
				HWND blocker = window->GetOwner()->GetModalBlocker();
				if (blocker != 0)
				{
					FlashModalWindow(blocker);
				}
			}
		}
		break;

	case WM_NCACTIVATE:
		// From Docs: When the wParam parameter is FALSE, an application should return TRUE to indicate that the system should proceed with the default processing, 
		//	or it should return FALSE to prevent the change. When wParam is TRUE, the return value is ignored.
		// With composition disabled, if we return FALSE or DefWindowProc here, the non-client area will flash as it activated, which causes a flickering due to
		//	our borderless setup. For composition enabled, it flashes the drop shadow which is desirable.
		if (window && window->m_bIsDwmCompositionEnabled == FALSE)
		{
			return TRUE;
		}
		break;

	case WM_CLOSE:
		{
			// CEF Browser Lifespan:
			// https://bitbucket.org/chromiumembedded/cef/wiki/GeneralUsage#markdown-header-browser-life-span
			SubProcessWindow* owner = window->GetOwner();
			if (window && owner)
			{
				// If we can't close, hide...
				if (!owner->CanClose())
				{
					::ShowWindow(hWnd, SW_HIDE);
					return 0;
				}
				else
				{
					CefRefPtr<CefBrowser> browser = owner->GetBrowser();
					if (browser.get() && !owner->IsClosing()) 
					{
						// Notify the browser window that we would like to close it.
						browser->GetHost()->CloseBrowser(false);

						// Cancel the close.
						return 0;
					}
				}
			}

		}

		// Allow the close.
		break;

	case WM_DESTROY:
		// Quitting CEF is handled in MyHandler::OnBeforeClose().
		if (window)
		{
			window->ClearPropertyStore();
			window->DetachFromWindow();
		}
		return 0;
	}

	if (window && iMsg > CustomWindowMessages::MSG_FIRST && iMsg < CustomWindowMessages::MSG_LAST)
	{
		window->OnCustomWindowMessage(iMsg, wParam, lParam);
	}

	return DefWindowProc(hWnd, iMsg, wParam, lParam);
}

void BorderlessWindowWrapper::OnResizeMove()
{
	if (m_Owner)
	{
		// Notify that the browser is about to resize or move
		ViewMsg_Rect_Params p;
		m_Owner->onResizeOrMoveIpc(p);

		// Update the browser position
		::SetWindowPos(m_hBrowser, NULL, 0, 0, m_Geometry.width(), m_Geometry.height(), SWP_NOZORDER);
		::UpdateWindow (m_hBrowser);
	}
}

void BorderlessWindowWrapper::OnWindowStateChanged(WPARAM wParam)
{
	WindowState newState = WS_UNKNOWN;
	if (wParam == SIZE_MAXIMIZED)
	{
		newState = WS_MAXIMIZED;
	}
	else if (wParam == SIZE_MINIMIZED)
	{
		newState = WS_MINIMIZED;
	}
	else if (wParam == SIZE_RESTORED)
	{
		newState = WS_NORMAL;
	}

	if (newState != WS_UNKNOWN && m_WindowState != newState)
	{
		m_WindowState = newState;
		m_Owner->OnWindowStateChanged(m_WindowState);
	}
}

bool BorderlessWindowWrapper::IsMaximized()
{
	WINDOWPLACEMENT p;
	p.length = sizeof(WINDOWPLACEMENT);

	if (::GetWindowPlacement(m_hWnd, &p) == TRUE)
	{
		return p.showCmd == SW_SHOWMAXIMIZED;
	}

	return false;
}

void BorderlessWindowWrapper::OnCustomWindowMessage(UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (msg == CustomWindowMessages::MSG_BRING_WINDOW_TO_FRONT)
	{
		HWND h = (HWND)wParam;
		::BringWindowToTop(h);
	}
}

void BorderlessWindowWrapper::DetachFromWindow()
{
	if (m_hWnd)
	{
		SetWindowLongPtr( m_hWnd, GWLP_USERDATA, NULL);
		m_hWnd = 0;
	}
}

void BorderlessWindowWrapper::OnDwmCompositionChanged()
{
#if CAN_USE_DWM
	bool bBringToFrontAfter = (::GetActiveWindow() == m_hWnd);

	// Note: As of Windows 8 (i.e. Windows 8, 8.1, 10+), DWM composition cannot be disabled.
	//	This code is more or less for Windows 7 only.
	BOOL bPrevious = m_bIsDwmCompositionEnabled;
	::DwmIsCompositionEnabled(&m_bIsDwmCompositionEnabled);

	// If no change, early out.
	if (bPrevious == m_bIsDwmCompositionEnabled)
	{
		return;
	}

	// Retrieve the window class style, and either enable/disable drop shadow.
	ULONG_PTR style = ::GetClassLongPtr(m_hWnd, GCL_STYLE);
	if (m_bIsDwmCompositionEnabled == TRUE)
	{
		style &= ~CS_DROPSHADOW;
	}
	else
	{
		style |= CS_DROPSHADOW;
	}
	::SetClassLongPtr(m_hWnd, GCL_STYLE, style);

	// force a window update. Haven't found a better way to do this, otherwise
	// the CS_DROPSHADOW will be updated but still be visible/invisible.
	if (::IsWindowVisible(m_hWnd))
	{
		::ShowWindow(m_hWnd, SW_HIDE);
		::ShowWindow(m_hWnd, SW_SHOW);
	}

	// Update the shadow.
	UpdateShadow();

	// Update the window theme if necessary.
	UpdateWindowTheme();

	// The act of processing the DWM message can minimize the application, bring it back to the front.
	if (bBringToFrontAfter)
	{
		::SetForegroundWindow(m_hWnd);
		::BringWindowToTop(m_hWnd);
	}
#endif 
}

void BorderlessWindowWrapper::UpdateDwmComposition()
{
#if CAN_USE_DWM
	m_bIsDwmCompositionEnabled = FALSE;
	::DwmIsCompositionEnabled(&m_bIsDwmCompositionEnabled);
#endif
}
void BorderlessWindowWrapper::UpdateShadow()
{
#if CAN_USE_DWM
	UpdateDwmComposition();
	if (m_bIsDwmCompositionEnabled == TRUE)
	{
		static const MARGINS shadow = {1};
		DwmExtendFrameIntoClientArea(m_hWnd, &shadow);
	}
#endif
}

void BorderlessWindowWrapper::UpdateWindowTheme()
{
#if CAN_USE_DWM
	//	url:bugstar:2916436 - Non-compositioned themes cause a variety of rendering irregularities.
	if (m_bIsDwmCompositionEnabled == FALSE)
	{
		// Passing empty strings disables the windows theme for this application.
		::SetWindowTheme(m_hWnd, L"", L"");
	}
	else
	{
		// Passing NULL reverts to the global windows theme.
		::SetWindowTheme(m_hWnd, NULL, NULL);
	}

	// Run an immediate update of the window.
	::UpdateWindow(m_hWnd);
#endif
}

void BorderlessWindowWrapper::ClearPropertyStore()
{
#if CAN_USE_DWM
	IPropertyStore *pps;
	HRESULT hr = SHGetPropertyStoreForWindow(m_hWnd, IID_PPV_ARGS(&pps));
	if (SUCCEEDED(hr))
	{
		PROPVARIANT pv;
		PropVariantInit(&pv);

		pps->SetValue(PKEY_AppUserModel_RelaunchCommand, pv);
		pps->SetValue(PKEY_AppUserModel_RelaunchDisplayNameResource, pv);
		pps->SetValue(PKEY_AppUserModel_RelaunchIconResource, pv);
		pps->SetValue(PKEY_AppUserModel_ID, pv);
		pps->Commit();

		PropVariantClear(&pv);
		pps->Release();
	}
#endif
}

} // namespace rgsc