#ifndef SUBPROCESS_BORDERLESSWINDOW_H
#define SUBPROCESS_BORDERLESSWINDOW_H

#include "rgsc_messages_params.h"
#include "rgsc_common.h"
#include "types.h"

namespace rgsc
{
	class SubProcessWindow;

	class BorderlessWindowWrapper
	{
	public:

		BorderlessWindowWrapper();
		~BorderlessWindowWrapper();

		// PURPOSE
		//	Creates the borderless window wrapper for the target owner with the input dimensions (rect) and setup parameters (params).
		bool Init(SubProcessWindow* owner, ViewMsg_Rect_Params& rect, ViewMsg_CreateWindow_Params& params);

		// PURPOSE
		//	Gets the window handle of the wrapper window
		HWND GetWindowHandle() { return m_hWnd; }

		// PURPOSE
		//	Gets or sets the handle with the underlying browser. Should be set by the SubprocessWindow owner for ease of use.
		void SetBrowserHandle(HWND hwnd) { m_hBrowser = hwnd; }
		HWND GetBrowserHandle() { return m_hBrowser; }

		// PURPOSE
		//	Callback when the wrapper window is about to resize or move.
		void OnResizeMove();

		// PURPOSE
		//	When receiving a WM_SIZE (window resize) event, the wParam property
		//  indicates if the window has been Maximized, Minimized or simply moved.
		void OnWindowStateChanged(WPARAM wParam);

		// PURPOSE
		//	Utility function that returns true if the window is maximized.
		bool IsMaximized();

		// PURPOSE
		// Handle custom windows messages.
		void OnCustomWindowMessage(UINT msg, WPARAM wParam, LPARAM lParam);

		// PURPOSE
		//	Accessor for the parent/owner SubprocessWindow.
		SubProcessWindow* GetOwner() { return m_Owner; }

		// PURPOSE
		//	Windows Message Handler
		static LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam );

	private:

		// Remove our instance from the GWLP_USERDATA so that no windows callbacks attempt to 
		// reference a null ptr.
		void DetachFromWindow();

		// PURPOSE
		//	Internal callback for when desktop windows composition changes.
		void OnDwmCompositionChanged();

		// PURPOSE
		//	Internal updates to query desktop windows composition, shadows, themes etc. Helps to drive
		//	if we use DWM/Aero shadows, or the basic CS_DROPSHADOW
		void UpdateDwmComposition();
		void UpdateShadow();
		void UpdateWindowTheme();

		// PURPOSE
		//	Clears any property store values associated with this window.
		void ClearPropertyStore();
	
		// Windows properties
		HINSTANCE m_hInstance;
		HWND m_hWnd;
		HWND m_hBrowser;
		BOOL m_bIsDwmCompositionEnabled;
		bool m_bShadowEnabled;
		WindowState m_WindowState;

		// SubProcessWindow references
		SubProcessWindow* m_Owner;

		// Geometry and Limits
		RgscRect m_Geometry;
		int m_MinimumHeight;
		int m_MinimumWidth;
		int m_MaximumWidth;
		int m_MaximumHeight;
	};

} // namespace rgsc

#endif