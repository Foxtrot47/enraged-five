#include "subprocess.h"
#include "resourceinterceptor.h"
#include "rgsc_messages.h"
#include "rgsc_messages_params.h"
#include "types.h"

#define IPC_SEND(x) rgsc::SubProcess::GetInstance()._GetIpc()->Send(x)
#define USE_READ_HANDLER 1

namespace rgsc
{

static const char* ShouldOpenInExternalBrowser(const char* url)
{
	char cmd[] = "RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/" ;
	bool isCmd = strncmp(url, cmd, sizeof(cmd) - 1) == 0;

	if(isCmd)
	{
		const char* externalUrl = &url[sizeof(cmd) - 1];

		// attempt to make sure the encoded url is not malicious
		char httpScheme[] = "http://" ;
		bool isHttpScheme = strncmp(externalUrl, httpScheme, sizeof(httpScheme) - 1) == 0;

		char httpsScheme[] = "https://" ;
		bool isHttpsScheme = strncmp(externalUrl, httpsScheme, sizeof(httpsScheme) - 1) == 0;

		if(isHttpScheme || isHttpsScheme)
		{
			const char *start = isHttpScheme ? &externalUrl[sizeof(httpScheme) - 1] : &externalUrl[sizeof(httpsScheme) - 1];

			const unsigned NUM_ALLOWED_DOMAINS = 3;
			const char* allowedDomains[NUM_ALLOWED_DOMAINS] = {"rockstarwarehouse.com",
											"rockstargames.com",
											"arvatocim.com",
										   };

			// Examples of allowed urls
			// www.rockstargames.com/support
			// rockstargames.com/support
			// rockstargames.com
			// support.rockstargames.com
			// support.rockstargames.com/
			// support.rockstargames.com/support2

			// examples of urls not allowed:
			// www.myrockstargames.com
			// www.hackers.com/rockstargames.com
			// www.rockstargames.com.hackers.com
			// etc.

			// 1. find the first '/' and start there, if none exists, start at the end of the string
			// 2. compare backwards from that starting point
			// 3. make sure the character just before the allowed substring is either a '.' or a '/' to ensure nothing is prepended to the domain (i.e. www.myrockstargames.com)
			const char* endOfDomain = strstr(start, "/");
			if(endOfDomain == NULL)
			{
				endOfDomain = &start[strlen(start)];
			}

			for(unsigned i = 0; i < NUM_ALLOWED_DOMAINS; ++i)
			{
				const char* startOfDomain = endOfDomain - strlen(allowedDomains[i]) - 1;

				// check one character back so we don't allow (for example) http://www.myrockstargames.com, but do allow http://rockstargames.com
				if(startOfDomain[0] == '.' || startOfDomain[0] == '/')
				{
					startOfDomain += 1;
					if(startOfDomain >= start)
					{
						bool isAllowed = strncmp(startOfDomain, allowedDomains[i], strlen(allowedDomains[i])) == 0;
						if(isAllowed)
						{
							return externalUrl;
						}
					}
				}
			}
		}
	}

	return NULL;
}


// ===========================================================================
// ResourceInterceptor
// ===========================================================================
ResourceRequestJob* ResourceInterceptor::MaybeIntercept(const char* url, unsigned int urlLength)
{
	//Displayf("SCUI requested resource: %s", url);

	// 	char fileScheme[] = "file://" ;
	// 	bool isFileScheme = strncmp(url, fileScheme, sizeof(fileScheme) - 1) == 0;

	char httpScheme[] = "http://" ;
	bool isHttpScheme = strncmp(url, httpScheme, sizeof(httpScheme) - 1) == 0;

	char httpsScheme[] = "https://" ;
	bool isHttpsScheme = strncmp(url, httpsScheme, sizeof(httpsScheme) - 1) == 0;

	if(isHttpScheme || isHttpsScheme)
	{
		// determine if it's an rgsc command
		const char *start = isHttpScheme ? &url[sizeof(httpScheme) - 1] : &url[sizeof(httpsScheme) - 1];
		char rgscIdentifier[] = "rgsc/";
		bool isRgscCommand = strncmp(start, rgscIdentifier, sizeof(rgscIdentifier) - 1) == 0;

		if(isRgscCommand)
		{
			start = &start[sizeof(rgscIdentifier) - 1];

			// determine if it's a request for an achievement image
			unsigned int achievementId;
			int n = sscanf_s(start, "RGSC_GET_ACHIEVEMENT_IMAGE/%u", &achievementId);

			if(n == 1)
			{
				return new AchievementImageRequestJob(achievementId);
			}
			else
			{

#if 0//__DEV
				// test the external url validater
				const char* testUrlsValid[] = {"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://www.rockstargames.com/support",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://www.rockstargames.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://rockstargames.com/support",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://rockstargames.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://support.rockstargames.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://support.rockstargames.com/support2",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://VADER.rockstargames.com/SuPPort/",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://www.rockstargames.com/",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://rockstargames.com/support/",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://support.rockstargames.com/",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://rockstargames.com/",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://support.rockstargames.com/support2/",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/https://www.rockstargames.com/support",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/https://www.rockstargames.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/https://rockstargames.com/support",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/https://rockstargames.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/https://support.rockstargames.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/https://support.rockstargames.com/support2",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/https://www.rockstargames.com/support/",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/https://www.rockstargames.com/",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/https://rockstargames.com/support/",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/https://support.rockstargames.com/",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/https://rockstargames.com/",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/https://support.rockstargames.com/support2/"};

				const char* testUrlsInvalid[] = {"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://www.myrockstargames.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://myrockstargames.com",	
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://www.hackers.com/rockstargames.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://www.rockstargames.com.hackers.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://www.hackers.com/rockstargames.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://www.hackers.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/www.hackers.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/hackers.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/https://www.hackers.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://www.hackers.com/rockstargames.com/support",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://rockstargame.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://rockstargames.com.co",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://rockstargames.co",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://rockstargames.co.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://rockstargames.net",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://rockstargames.org",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://abc.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://123412341341234123412341241242342341234123412341234123421341234.com",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/http://",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER/",
					"RGSC_LAUNCH_EXTERNAL_WEB_BROWSER"};

				for(unsigned i = 0; i < COUNTOF(testUrlsValid); ++i)
				{
					Assert(ShouldOpenInExternalBrowser(testUrlsValid[i]) != NULL);
				}

				for(unsigned i = 0; i < COUNTOF(testUrlsInvalid); ++i)
				{
					Assert(ShouldOpenInExternalBrowser(testUrlsInvalid[i]) == NULL);
				}
#endif

				// determine if it's a url that should be opened in an external browser
				const char* externalUrl = ShouldOpenInExternalBrowser(start);

				if(externalUrl)
				{
					ASSERT_ONLY(int r = )(int)ShellExecuteA(NULL, "open", externalUrl, NULL, NULL, SW_SHOWMAXIMIZED);
					Assertf(r > 32, "ShellExecute returned an error when trying to open a url in an external web browser: %d", r);
					return new ResourceRequestJob();
				}
			}
		}
	}

	// returning NULL informs Chrome to handle the request.
	return NULL;
}

// ===========================================================================
// ResourceRequestJob
// ===========================================================================
CefRefPtr<CefStreamReader> ResourceRequestJob::GetStream()
{
	char* body = NULL;
	unsigned bodyLength = 0;

	bool success = GetResponse(&body, &bodyLength);

	if(success)
	{
		CefRefPtr<CefStreamReader> stream = CefStreamReader::CreateForData( static_cast<void*>(const_cast<char*>(body)), bodyLength);
		Assert(stream.get());
		return stream;
	}	

	return NULL;
}

bool ResourceRequestJob::GetResponse(char** body, unsigned int* bodyLength)
{
	*bodyLength = NULL;
	*body = NULL;
	return false;
}

const char* ResourceRequestJob::GetMimeType()
{
	return "";
}

void ResourceRequestJob::Release()
{
	delete this;
}

// ===========================================================================
// AchievementImageRequestJob
// ===========================================================================
AchievementImageRequestJob::AchievementImageRequestJob(unsigned int achievementId)
	: m_AchievementId(achievementId)
{
}

AchievementImageRequestJob::~AchievementImageRequestJob()
{
}

CefRefPtr<CefStreamReader> AchievementImageRequestJob::GetStream()
{
	CefRefPtr<CefStreamReader> stream = NULL;

#if USE_READ_HANDLER
	CefRefPtr<CefReadHandler> handler (new AchievementImageReadHandler(m_AchievementId));
	stream = CefStreamReader::CreateForHandler(handler);
#else
	char* body = NULL;
	unsigned bodyLength = 0;

	// IPC synchronously read the achievement image
	IPC_SEND(new ViewHostMsg_GetAchievementImage(m_AchievementId, &m_AchievementImage));

	// create the CEF stream reader using the achievement image data
	body = (char*)&m_AchievementImage[0];
	bodyLength = (unsigned)m_AchievementImage.size();
	stream = CefStreamReader::CreateForData( static_cast<void*>(body), bodyLength);
#endif

	Assert(stream.get());
	return stream;
}

const char* AchievementImageRequestJob::GetMimeType()
{
	return "image/png";
}

//////////////////////////////////////////////////////////////////////
// AchievementImageReadHandler
//////////////////////////////////////////////////////////////////////
AchievementImageReadHandler::AchievementImageReadHandler(int achievementId)
	: m_ReadAchievement(false)
	, m_Offset(0)
	, m_Size(0) 
	, m_AchievementId(achievementId)
{

}

AchievementImageReadHandler::~AchievementImageReadHandler()
{

}

size_t AchievementImageReadHandler::Read(void* ptr, size_t size, size_t n)
{
	base::AutoLock lock_scope(m_Lock);

	Assert(size > 0);
	if (size == 0)
	{
		return 0;

	}
	if (!m_ReadAchievement)
	{
		IPC_SEND(new ViewHostMsg_GetAchievementImage(m_AchievementId, &m_AchievementImage));
		m_Size = (unsigned)m_AchievementImage.size();
		m_ReadAchievement = true;
	}

	size_t s = static_cast<size_t>(m_Size - m_Offset) / size;
	size_t ret = std::min(n, s);
	
	if (ret > 0)
	{
		char* start = (char*)&m_AchievementImage[(unsigned)m_Offset];
		memcpy(ptr, start, ret * size);
		m_Offset += ret * size;
	}

	return ret;
}

int AchievementImageReadHandler::Seek(int64 offset, int whence)
{
	int ret = -1;
	base::AutoLock lock_scope(m_Lock);

	// We haven't read our achievement yet
	if (!m_ReadAchievement)
	{
		return ret;
	}

	switch (whence) 
	{
	case SEEK_CUR:
		{
			if (m_Offset + offset > m_Size || m_Offset + offset < 0)
				break;

			m_Offset += offset;
			ret = 0;
		}
		break;
	case SEEK_END: 
		{
			int64 absOffset = _abs64(offset);
			if (absOffset > m_Size)
				break;

			m_Offset = m_Size - absOffset;
			ret = 0;
		}
		break;
	case SEEK_SET:
		{
			if (offset > m_Size || offset < 0)
				break;
			
			m_Offset = offset;
			ret = 0;
		}
		break;
	}

	return ret;
}

int64 AchievementImageReadHandler::Tell()
{
	base::AutoLock lock_scope(m_Lock);
	return m_Offset;
}

int AchievementImageReadHandler::Eof()
{
	base::AutoLock lock_scope(m_Lock);
	return m_Offset >= m_Size;
}

bool AchievementImageReadHandler::MayBlock() 
{ 
	return true; 
};

} // namespace rgsc
