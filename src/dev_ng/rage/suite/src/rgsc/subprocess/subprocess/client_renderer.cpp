// Copyright (c) 2012 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#include "client_renderer.h"

#include <sstream>
#include <string>

#include "include/cef_dom.h"
#include "include/wrapper/cef_message_router.h"

#include "rgsc_messages.h"
#include "rgsc_messages_params.h"

using namespace rgsc;

namespace client_renderer
{

const char kFocusedNodeChangedMessage[] = "ClientRenderer.FocusedNodeChanged";

namespace
{

class ClientRenderDelegate : public SubProcess::RenderDelegate
{
public:
	struct JavascriptBinding
	{
		std::wstring funcName;
		bool synchronous;
	};

	class V8HandlerImpl : public CefV8Handler
	{
	public:
		std::vector<JavascriptBinding> m_JavascriptBindings;

		V8HandlerImpl(const std::vector<JavascriptBinding>& bindings)
		{
			m_JavascriptBindings = bindings;
		}

		virtual bool Execute(const CefString& name,
		                     CefRefPtr<CefV8Value> object,
		                     const CefV8ValueList& arguments,
		                     CefRefPtr<CefV8Value>& retval,
		                     CefString& exception) OVERRIDE
		{
			bool exists = false;
			bool isSynchronous = false;
			for(unsigned i = 0; i < m_JavascriptBindings.size(); ++i)
			{
				if(m_JavascriptBindings[i].funcName == std::wstring(name))
				{
					exists = true;
					isSynchronous = m_JavascriptBindings[i].synchronous;
					break;
				}
			}

			if(!exists)
			{
				return false;
			}

			ViewHostMsg_OnJavascriptCallback_Params params;
			params.m_FunctionName = std::wstring(name);
			params.m_IpcChannel = rgsc::SubProcess::GetInstance().GetIpcChannelName();

			size_t numArgs = arguments.size();

			// TODO: NS - currently only supports function calls passing one string arg, or no args
			if(numArgs == 1)
			{
				if(arguments[0]->IsValid() && arguments[0]->IsString())
				{
					params.m_Arg = arguments[0]->GetStringValue();
				}
			}

			if(isSynchronous)
			{
				// send sync version of the msg. This will block until the DLL responds with a reply message.
				ViewHostMsg_OnJavascriptCallback_ReturnVal ret;

				// TODO: NS - window id
				int windowId = 1;
				rgsc::SubProcess::GetInstance()._GetIpc()->Send(new ViewHostMsg_OnJavascriptCallbackSync(windowId, params, &ret));

				// convert the response to a CefRefPtr<CefV8Value>&
				if(ret.m_Type == ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_STRING)
				{
					retval = CefV8Value::CreateString(ret.m_String);
				}
				else if(ret.m_Type == ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_BOOL)
				{
					retval = CefV8Value::CreateBool(ret.m_Bool);
				}
				else if(ret.m_Type == ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_INT)
				{
					retval = CefV8Value::CreateInt(ret.m_Int);
				}
				else if(ret.m_Type == ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_DOUBLE)
				{
					retval = CefV8Value::CreateDouble(ret.m_Double);
				}
			}
			else
			{
				// TODO: NS - window id
				int windowId = 1;
				rgsc::SubProcess::GetInstance()._GetIpc()->Send(new ViewHostMsg_OnJavascriptCallbackAsync(windowId, params));
			}

// 			CefRefPtr<CefProcessMessage> message = CefProcessMessage::Create(query_message_name_);
//
// 			CefRefPtr<CefListValue> args = message->GetArgumentList();
// 			args->SetInt(0, CefInt64GetLow(frame_id));
// 			args->SetInt(1, CefInt64GetHigh(frame_id));
// 			args->SetInt(2, context_id);
// 			args->SetInt(3, request_id);
// 			args->SetString(4, request);
// 			args->SetBool(5, persistent);
//
// 			browser->SendProcessMessage(PID_BROWSER, message);

			return true;
		}

	private:

		IMPLEMENT_REFCOUNTING(V8HandlerImpl);
	};

	ClientRenderDelegate()
		: last_node_is_editable_(false)
	{
	}

	virtual void OnRenderThreadCreated(CefRefPtr<SubProcess> app, CefRefPtr<CefListValue> extra_info)
	{
		unsigned size = (unsigned)extra_info->GetSize();

		for(unsigned i = 0; i < size; i += 2)
		{
			JavascriptBinding binding;
			binding.funcName = extra_info->GetString(i);
			binding.synchronous = extra_info->GetBool(i + 1);
			m_JavascriptBindings.push_back(binding);
		}
	}

	virtual void OnWebKitInitialized(CefRefPtr<SubProcess> app) OVERRIDE
	{
		// Create the renderer-side router for query handling.
// 		CefMessageRouterConfig config;
// 		message_router_ = CefMessageRouterRendererSide::Create(config);
	}

	virtual void OnContextCreated(CefRefPtr<SubProcess> app,
	                              CefRefPtr<CefBrowser> browser,
	                              CefRefPtr<CefFrame> frame,
	                              CefRefPtr<CefV8Context> context) OVERRIDE
	{
//		message_router_->OnContextCreated(browser, frame, context);

// 		if(!browser.get() || browser->GetMainFrame() != frame)
// 		{
// 			// we only want to bind our functions to the main frame
// 			return;
// 		}

		if(!sm_Handler.get())
		{
			sm_Handler = new V8HandlerImpl(m_JavascriptBindings);
		}

		//CEF_REQUIRE_RENDERER_THREAD();

		// Register function handlers with the 'window' object.
		CefRefPtr<CefV8Value> window = context->GetGlobal();

		CefV8Value::PropertyAttribute attributes =
		static_cast<CefV8Value::PropertyAttribute>(
		    V8_PROPERTY_ATTRIBUTE_READONLY |
		    V8_PROPERTY_ATTRIBUTE_DONTENUM |
		    V8_PROPERTY_ATTRIBUTE_DONTDELETE);

		// bind our functions
		for(unsigned i = 0; i < m_JavascriptBindings.size(); ++i)
		{
			CefString funcName = m_JavascriptBindings[i].funcName;
			CefRefPtr<CefV8Value> func = CefV8Value::CreateFunction(funcName, sm_Handler.get());
			window->SetValue(funcName, func, attributes);
		}

		// notify the socialclub.dll that we're finished binding our js functions
		// TODO: NS - have the browser process pass this function as the first parameter of extra_info
		CefString js("RGSC_FINISHED_FUNCTION_BINDINGS()");
		frame->ExecuteJavaScript(js, "", 0);
	}

	virtual void OnContextReleased(CefRefPtr<SubProcess> app,
	                               CefRefPtr<CefBrowser> browser,
	                               CefRefPtr<CefFrame> frame,
	                               CefRefPtr<CefV8Context> context) OVERRIDE
	{
//		message_router_->OnContextReleased(browser,  frame, context);
	}

	virtual void OnFocusedNodeChanged(CefRefPtr<SubProcess> app,
	                                  CefRefPtr<CefBrowser> browser,
	                                  CefRefPtr<CefFrame> frame,
	                                  CefRefPtr<CefDOMNode> node) OVERRIDE
	{
		bool is_editable = (node.get() && node->IsEditable());
		if(is_editable != last_node_is_editable_)
		{
			// Notify the browser of the change in focused element type.
			last_node_is_editable_ = is_editable;
			CefRefPtr<CefProcessMessage> message = CefProcessMessage::Create(kFocusedNodeChangedMessage);
			message->GetArgumentList()->SetBool(0, is_editable);
			browser->SendProcessMessage(PID_BROWSER, message);
		}
	}

	virtual bool OnProcessMessageReceived(
	    CefRefPtr<SubProcess> app,
	    CefRefPtr<CefBrowser> browser,
	    CefProcessId source_process,
	    CefRefPtr<CefProcessMessage> message) OVERRIDE
	{
		return false;
		//return message_router_->OnProcessMessageReceived(browser, source_process, message);
	}

private:
	std::vector<JavascriptBinding> m_JavascriptBindings;
	static CefRefPtr<ClientRenderDelegate::V8HandlerImpl> sm_Handler;

	bool last_node_is_editable_;

	// Handles the renderer side of query routing.
	//CefRefPtr<CefMessageRouterRendererSide> message_router_;

	IMPLEMENT_REFCOUNTING(ClientRenderDelegate);
};

CefRefPtr<ClientRenderDelegate::V8HandlerImpl> ClientRenderDelegate::sm_Handler = NULL;

}  // namespace

void CreateRenderDelegates(SubProcess::RenderDelegateSet& delegates)
{
	delegates.insert(new ClientRenderDelegate);
}

}  // namespace client_renderer
