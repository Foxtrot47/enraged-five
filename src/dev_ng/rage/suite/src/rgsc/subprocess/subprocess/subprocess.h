#ifndef RGSC_SUBPROCESS_H
#define RGSC_SUBPROCESS_H

#include "ipc.h"
#include "rgsc_common.h"
#include <map>
#include <set>
#include <string>
#include <utility>
#include <vector>
#include "rgsc_messages_params.h"

#include "include/cef_app.h"

#if RSG_CPU_X86
#define CefBaseRefCounted CefBase
#define CefRawPtr CefRefPtr
#endif

namespace rgsc
{
class SubProcessErrorDelegate;
class SubProcessWindow;

class SubProcess : public CefApp,
				   public CefBrowserProcessHandler,
				   public CefRenderProcessHandler
{
public:
	static SubProcess& GetInstance();
	~SubProcess();
	int Init(int argc, LPWSTR* argv);
	void Update();
	void Shutdown();
	Ipc* _GetIpc();
	
	const char* GetIpcChannelName() {return m_IpcChannelName;}
	SubProcessWindow* GetWindow() { return m_Window; }

	void OnCreateTiledWindowMessage(unsigned int id, unsigned int width, unsigned int height, float scale, ViewMsg_Tile_Params tiles, std::string sharedMemoryName);
	void OnCreateWindowMessage(unsigned int id, ViewMsg_Rect_Params rect, ViewMsg_CreateWindow_Params window);

	void OnHibernate();
	void OnDestroyWindowMessage(unsigned int id);
	void OnMessageReceived(const IPC::Message& message);
	void OnShutdownMessage();

	// Interface for browser delegates. All BrowserDelegates must be returned via
	// CreateBrowserDelegates. Do not perform work in the BrowserDelegate
	// constructor. See CefBrowserProcessHandler for documentation.
	class BrowserDelegate : public virtual CefBaseRefCounted
	{
	public:
		virtual void OnContextInitialized(CefRefPtr<SubProcess> app) {}

		virtual void OnBeforeChildProcessLaunch(CefRefPtr<SubProcess> app, CefRefPtr<CefCommandLine> command_line)
		{

		}

		virtual void OnRenderProcessThreadCreated(CefRefPtr<SubProcess> app, CefRefPtr<CefListValue> extra_info)
		{

		}
	};

	typedef std::set<CefRefPtr<BrowserDelegate> > BrowserDelegateSet;

	// Interface for renderer delegates. All RenderDelegates must be returned via
	// CreateRenderDelegates. Do not perform work in the RenderDelegate
	// constructor. See CefRenderProcessHandler for documentation.
	class RenderDelegate : public virtual CefBaseRefCounted
	{
	public:
		virtual void OnRenderThreadCreated(CefRefPtr<SubProcess> app,
		                                   CefRefPtr<CefListValue> extra_info) {}

		virtual void OnWebKitInitialized(CefRefPtr<SubProcess> app) {}

		virtual void OnBrowserCreated(CefRefPtr<SubProcess> app,
		                              CefRefPtr<CefBrowser> browser) {}

		virtual void OnBrowserDestroyed(CefRefPtr<SubProcess> app,
		                                CefRefPtr<CefBrowser> browser) {}

		virtual CefRefPtr<CefLoadHandler> GetLoadHandler(CefRefPtr<SubProcess> app)
		{
			return NULL;
		}

		virtual bool OnBeforeNavigation(CefRefPtr<SubProcess> app,
		                                CefRefPtr<CefBrowser> browser,
		                                CefRefPtr<CefFrame> frame,
		                                CefRefPtr<CefRequest> request,
		                                cef_navigation_type_t navigation_type,
		                                bool is_redirect);

		virtual void OnContextCreated(CefRefPtr<SubProcess> app,
		                              CefRefPtr<CefBrowser> browser,
		                              CefRefPtr<CefFrame> frame,
		                              CefRefPtr<CefV8Context> context) {};

		virtual void OnContextReleased(CefRefPtr<SubProcess> app,
		                               CefRefPtr<CefBrowser> browser,
		                               CefRefPtr<CefFrame> frame,
		                               CefRefPtr<CefV8Context> context) {}

		virtual void OnUncaughtException(CefRefPtr<SubProcess> app,
		                                 CefRefPtr<CefBrowser> browser,
		                                 CefRefPtr<CefFrame> frame,
		                                 CefRefPtr<CefV8Context> context,
		                                 CefRefPtr<CefV8Exception> exception,
		                                 CefRefPtr<CefV8StackTrace> stackTrace) {}

		virtual void OnFocusedNodeChanged(CefRefPtr<SubProcess> app,
		                                  CefRefPtr<CefBrowser> browser,
		                                  CefRefPtr<CefFrame> frame,
		                                  CefRefPtr<CefDOMNode> node) {}

		// Called when a process message is received. Return true if the message was
		// handled and should not be passed on to other handlers. RenderDelegates
		// should check for unique message names to avoid interfering with each
		// other.
		virtual bool OnProcessMessageReceived(CefRefPtr<SubProcess> app,
		                                      CefRefPtr<CefBrowser> browser,
		                                      CefProcessId source_process,
		                                      CefRefPtr<CefProcessMessage> message)
		{
			return false;
		}
	};

	typedef std::set<CefRefPtr<RenderDelegate> > RenderDelegateSet;

	bool WantsToQuit() {return m_Quit;}
	bool WindowIsClosed()  {return m_WindowIsClosed;}
	void SetWindowIsClosed();

private:
	// singleton
	SubProcess();
	SubProcess(SubProcess const&);
	void operator=(SubProcess const&);

	int SubProcess::InitCef(int argc, LPWSTR* argv);

	Ipc m_Ipc;
	unsigned m_ParentPid;
	char m_IpcChannelName[64];
	wchar_t m_HomeDir[MAX_PATH];
	bool m_Quit;
	bool m_WindowIsClosed;
	unsigned m_NextParentCheckTime;
	unsigned m_ParentClosedTime;

#if RSG_DEV
	std::string m_DisableCaptchaHeader;
	std::string m_DisableCaptchaHeaderValue;
#endif

	Ipc::Delegate m_IpcDelegate;
	SubProcessWindow* m_Window;
	bool m_IsRenderer;
	bool m_IsBrowser;

	bool IsParentProcessRunning(DWORD pid);

	// Creates all of the BrowserDelegate objects. Implemented in
	// client_app_delegates.
	static void CreateBrowserDelegates(BrowserDelegateSet& delegates);

	// Creates all of the RenderDelegate objects. Implemented in
	// client_app_delegates.
	static void CreateRenderDelegates(RenderDelegateSet& delegates);

	// Registers custom schemes. Implemented in client_app_delegates.
	static void RegisterCustomSchemes(CefRawPtr<CefSchemeRegistrar> registrar, std::vector<CefString>& cookiable_schemes);

	// CefApp methods.
	virtual void OnBeforeCommandLineProcessing(const CefString& process_type, CefRefPtr<CefCommandLine> command_line);

	virtual void OnRegisterCustomSchemes(CefRawPtr<CefSchemeRegistrar> registrar) OVERRIDE;
	virtual CefRefPtr<CefBrowserProcessHandler> GetBrowserProcessHandler() OVERRIDE { return this; }
	virtual CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() OVERRIDE { return this; }

	// CefBrowserProcessHandler methods.
	virtual void OnContextInitialized() OVERRIDE;
	virtual void OnBeforeChildProcessLaunch(CefRefPtr<CefCommandLine> command_line) OVERRIDE;
	virtual void OnRenderProcessThreadCreated(CefRefPtr<CefListValue> extra_info) OVERRIDE;

	// CefRenderProcessHandler methods.
	virtual void OnRenderThreadCreated(CefRefPtr<CefListValue> extra_info) OVERRIDE;
	virtual void OnWebKitInitialized() OVERRIDE;
	virtual void OnBrowserCreated(CefRefPtr<CefBrowser> browser) OVERRIDE;
	virtual void OnBrowserDestroyed(CefRefPtr<CefBrowser> browser) OVERRIDE;
	virtual CefRefPtr<CefLoadHandler> GetLoadHandler() OVERRIDE;
	virtual bool OnBeforeNavigation(CefRefPtr<CefBrowser> browser,
	                                CefRefPtr<CefFrame> frame,
	                                CefRefPtr<CefRequest> request,
	                                NavigationType navigation_type,
	                                bool is_redirect) OVERRIDE;
	virtual void OnContextCreated(CefRefPtr<CefBrowser> browser,
	                              CefRefPtr<CefFrame> frame,
	                              CefRefPtr<CefV8Context> context) OVERRIDE;
	virtual void OnContextReleased(CefRefPtr<CefBrowser> browser,
	                               CefRefPtr<CefFrame> frame,
	                               CefRefPtr<CefV8Context> context) OVERRIDE;
	virtual void OnUncaughtException(CefRefPtr<CefBrowser> browser,
	                                 CefRefPtr<CefFrame> frame,
	                                 CefRefPtr<CefV8Context> context,
	                                 CefRefPtr<CefV8Exception> exception,
	                                 CefRefPtr<CefV8StackTrace> stackTrace) OVERRIDE;
	virtual void OnFocusedNodeChanged(CefRefPtr<CefBrowser> browser,
	                                  CefRefPtr<CefFrame> frame,
	                                  CefRefPtr<CefDOMNode> node) OVERRIDE;
	virtual bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser,
	                                      CefProcessId source_process,
	                                      CefRefPtr<CefProcessMessage> message) OVERRIDE;

	// Set of supported BrowserDelegates. Only used in the browser process.
	BrowserDelegateSet browser_delegates_;

	// Set of supported RenderDelegates. Only used in the renderer process.
	RenderDelegateSet render_delegates_;

	// Schemes that will be registered with the global cookie manager. Used in
	// both the browser and renderer process.
	std::vector<CefString> cookieable_schemes_;

	IMPLEMENT_REFCOUNTING(SubProcess);
};

} // namespace rgsc

#endif //RGSC_SUBPROCESS_H
