#ifndef RESOURCE_INTERCEPTOR_H
#define RESOURCE_INTERCEPTOR_H

#include "include/base/cef_lock.h"

namespace rgsc
{

// ===========================================================================
// ResourceRequestJob
// ===========================================================================
class ResourceRequestJob
{
public:
	ResourceRequestJob() {}
	virtual ~ResourceRequestJob() {}

	virtual CefRefPtr<CefStreamReader> GetStream();

	virtual const char* GetMimeType();

	virtual bool GetResponse(char** body, unsigned int* bodyLength);

	virtual void Release();
};

// ===========================================================================
// AchievementImageRequestJob
// ===========================================================================
class AchievementImageRequestJob : public ResourceRequestJob
{
public:
	AchievementImageRequestJob(unsigned int achievementId);
	virtual ~AchievementImageRequestJob();
	virtual CefRefPtr<CefStreamReader> GetStream();
	virtual const char* GetMimeType();

private:
	unsigned int m_AchievementId;
	std::vector<char> m_AchievementImage;
};

// ===========================================================================
// ResourceInterceptor
// ===========================================================================
class ResourceInterceptor
{
public:
	ResourceInterceptor() {}
	virtual ~ResourceInterceptor() {}

	virtual ResourceRequestJob* MaybeIntercept(const char* url, unsigned int urlLength);
};

class AchievementImageReadHandler : public CefReadHandler 
{
public:
	AchievementImageReadHandler(int achievementId);
	~AchievementImageReadHandler();

	// Read raw binary data.
	virtual size_t Read(void* ptr, size_t size, size_t n);

	// Seek to the specified offset position. |whence| may be any one of SEEK_CUR, SEEK_END or SEEK_SET. 
	// Return zero on success and non-zero on failure.
	virtual int Seek(int64 offset, int whence);

	// Return the current offset position.
	virtual int64 Tell();

	// Return non-zero if at end of file.
	virtual int Eof();

	// Return true if this handler performs work like accessing the file system which may block. 
	// Used as a hint for determining the thread to access the handler from.
	virtual bool MayBlock();

private:
	int64 m_Offset;
	int64 m_Size;
	int m_AchievementId;
	bool m_ReadAchievement;
	std::vector<char> m_AchievementImage;

	base::Lock m_Lock;

	IMPLEMENT_REFCOUNTING(AchievementImageReadHandler);
	DISALLOW_COPY_AND_ASSIGN(AchievementImageReadHandler);
};

} // namespace rgsc

#endif //RESOURCE_INTERCEPTOR_H
