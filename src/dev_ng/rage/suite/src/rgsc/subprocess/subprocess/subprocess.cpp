#include "subprocess.h"
#include "window.h"
#include "../../rgsc_ui/rgsc_messages.h"

// CEF includes
#include "include/internal/cef_win.h"
#include "include/internal/cef_ptr.h"
#include "include/cef_app.h"
#include "include/cef_cookie.h"
#include "include/cef_process_message.h"
#include "include/cef_task.h"
#include "include/cef_v8.h"

#include "client_renderer.h"

// the CEF Sandbox requires VS 2013
#define CEF_ENABLE_SANDBOX 0

#if CEF_ENABLE_SANDBOX
#include "cef/include/cef_sandbox_win.h"
#pragma comment(lib, "cef_sandbox.lib")
#endif

#if _WIN64
#include <Shobjidl.h>
#endif

namespace rgsc
{

/////////////////////////////////////////////////////////////////////////////
// SubProcess::RenderDelegate
/////////////////////////////////////////////////////////////////////////////	
bool SubProcess::RenderDelegate::OnBeforeNavigation(CefRefPtr<SubProcess> app,
	CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame,
	CefRefPtr<CefRequest> request,
	cef_navigation_type_t navigation_type,
	bool is_redirect)
{
	// Return 'true' to cancel the navigation or
	// Return 'false' to allow the navigation to proceed.
	switch(navigation_type)
	{
	case NAVIGATION_FORM_SUBMITTED:
	case NAVIGATION_BACK_FORWARD:
	case NAVIGATION_RELOAD:
	case NAVIGATION_FORM_RESUBMITTED:
		// Block navigation based on back/forward, forms.
		return true;
	case NAVIGATION_LINK_CLICKED:
		// Allow links to be opened in an external browser
		if (app)
		{
			int windowId = 0;
			if (app->GetWindow())
			{
				windowId = app->GetWindow()->GetWindowId();
			}

			std::string url(request->GetURL());
			app->_GetIpc()->Send(new ViewHostMsg_OnUrlRequested(windowId, url));
		}
		return true;
	case NAVIGATION_OTHER:
		// Allow other (i.e. page loads)
		return false;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////
// SubProcess
/////////////////////////////////////////////////////////////////////////////
SubProcess& SubProcess::GetInstance()
{
	static SubProcess* s_Instance = NULL;

	if(s_Instance == NULL)
	{
		s_Instance = new SubProcess;
	}

	return *s_Instance;
}

SubProcess::SubProcess()
	: m_Window(NULL)
	, m_IsRenderer(false)
	, m_IsBrowser(false)
	, m_ParentPid(0)
	, m_Quit(false)
	, m_WindowIsClosed(false)
	, m_NextParentCheckTime(0)
	, m_ParentClosedTime(0)
{
	m_IpcChannelName[0] = '\0';
}

SubProcess::~SubProcess()
{

}

int SubProcess::InitCef(int argc, LPWSTR* argv)
{
	void* sandbox_info = NULL;

#if CEF_ENABLE_SANDBOX
	// Manage the life span of the sandbox information object. This is necessary
	// for sandbox support on Windows. See cef_sandbox_win.h for complete details.
	CefScopedSandboxInfo scoped_sandbox;
	sandbox_info = scoped_sandbox.sandbox_info();
#endif

	HINSTANCE hInstance = GetModuleHandle(NULL);
	CefMainArgs main_args(hInstance);

	CefRefPtr<SubProcess> app(this);

	// Execute the secondary process, if any.
	int exit_code = CefExecuteProcess(main_args, app.get(), sandbox_info);
	if(exit_code >= 0)
	{
		Shutdown();
		return exit_code;
	}

	CefSettings settings;
	settings.multi_threaded_message_loop = false;

#if __DEV
	for(int i = 0; i < argc; ++i)
	{
		if(_wcsicmp(argv[i], L"--rgsc_ignore_certificate_errors") == 0)
		{
			settings.ignore_certificate_errors = true;
		}
	}
#endif

	// need cef.pak for scroll bars, check boxes, buttons, etc.
	// also need to load the locale pak files for certain foreign language glyphs
	// (japanese vs chinese glyphs that use the same unicode value for example)
	settings.pack_loading_disabled = false;
	
	settings.no_sandbox = true;
	settings.windowless_rendering_enabled = true;

#if __DEV
	settings.log_severity = LOGSEVERITY_INFO;
#else
	settings.log_severity = LOGSEVERITY_DISABLE;
#endif

#if !CEF_ENABLE_SANDBOX
	settings.no_sandbox = true;
#endif

	CefString(&settings.cache_path) = m_HomeDir;

	// Initialize CEF.
	CefInitialize(main_args, settings, app.get(), sandbox_info);

	// < 0 means everything is fine (to match CEF)
	return -1;
}

int SubProcess::Init(int argc, LPWSTR* argv)
{
#if _WIN64
	// Application User Model IDs (AppUserModelIDs) are used extensively by the taskbar in Windows 7 and later systems 
	// to associate processes, files, and windows with a particular application. 
	//	Format - 128 chars - no spaces: CompanyName.ProductName{.SubProduct.VersionInformation}
#if RSG_FINAL
	SetCurrentProcessExplicitAppUserModelID(RGSC_AUMID);
#else
	SetCurrentProcessExplicitAppUserModelID(RGSC_AUMID_DEBUG);
#endif // RSG_FINAL

	CefEnableHighDPISupport();
#endif // _WIN64

	// formatf(commandLine, "\"%s\" --rgsc_ipc_channel_name=%s --rgsc_home_dir=%s %s", subprocessPath, channelName, cachePath, chromeCommandLine);

	const wchar_t* channelNameArg = L"--rgsc_ipc_channel_name=";
	const wchar_t* homeDir = L"--rgsc_home_dir=";
	const wchar_t* parentId = L"--rgsc_pid=";

	m_IsBrowser = true;
	m_IsRenderer = false;
	m_ParentPid = 0;
	for(int i = 0; i < argc; ++i)
	{
		if(_wcsnicmp(argv[i], L"--type", wcslen(L"--type")) == 0)
		{
			m_IsBrowser = false;
		}

		if(_wcsicmp(argv[i], L"--type=renderer") == 0)
		{
			m_IsRenderer = true;
		}

#if RSG_DEV
		const wchar_t* noCaptchaHeaderArg = L"--no-captcha-header=";
		if(_wcsnicmp(argv[i], noCaptchaHeaderArg, wcslen(noCaptchaHeaderArg)) == 0)
		{
			const int NO_CAPTCHA_BUF_SIZE = 128;
			wchar_t noCaptchaHeaderW[NO_CAPTCHA_BUF_SIZE] = {0};
			wcsncpy(noCaptchaHeaderW, argv[i] + wcslen(noCaptchaHeaderArg), NO_CAPTCHA_BUF_SIZE - 1);

			char noCaptchaHeader[NO_CAPTCHA_BUF_SIZE];
			WideCharToMultiByte(CP_UTF8, 0, noCaptchaHeaderW, -1, noCaptchaHeader, sizeof(noCaptchaHeader), 0, 0);

			m_DisableCaptchaHeader = noCaptchaHeader;
		}

		const wchar_t* noCaptchaHeaderValueArg = L"--no-captcha-header-value=";
		if(_wcsnicmp(argv[i], noCaptchaHeaderValueArg, wcslen(noCaptchaHeaderValueArg)) == 0)
		{
			const int NO_CAPTCHA_BUF_SIZE = 128;
			wchar_t noCaptchaHeaderValueW[NO_CAPTCHA_BUF_SIZE] = {0};
			wcsncpy(noCaptchaHeaderValueW, argv[i] + wcslen(noCaptchaHeaderValueArg), NO_CAPTCHA_BUF_SIZE - 1);

			char noCaptchaHeaderValue[NO_CAPTCHA_BUF_SIZE];
			WideCharToMultiByte(CP_UTF8, 0, noCaptchaHeaderValueW, -1, noCaptchaHeaderValue, sizeof(noCaptchaHeaderValue), 0, 0);

			m_DisableCaptchaHeaderValue = noCaptchaHeaderValue;
		}
#endif

		if(_wcsnicmp(argv[i], channelNameArg, wcslen(channelNameArg)) == 0)
		{
			wchar_t ipcChannelNameW[Ipc::MAX_CHANNEL_NAME_LEN] = {0};
			wcsncpy(ipcChannelNameW, argv[i] + wcslen(channelNameArg), Ipc::MAX_CHANNEL_NAME_LEN - 1);
			WideCharToMultiByte(CP_UTF8, 0, ipcChannelNameW, -1, m_IpcChannelName, sizeof(m_IpcChannelName), 0, 0);
		}

		if(_wcsnicmp(argv[i], homeDir, wcslen(homeDir)) == 0)
		{
			wcsncpy(m_HomeDir, argv[i] + wcslen(homeDir), MAX_PATH - 1);
		}

		if(_wcsnicmp(argv[i], parentId, wcslen(parentId)) == 0)
		{
			const unsigned MAX_PARENT_ID_LEN = 64;
			wchar_t szParentId[MAX_PARENT_ID_LEN] = {0};
			wcsncpy(szParentId, argv[i] + wcslen(parentId), MAX_PARENT_ID_LEN - 1);
			swscanf(szParentId, L"%u", &m_ParentPid);
		}
	}

	if(m_IsBrowser && (m_ParentPid == 0))
	{
		// browser subprocess, but we didn't get an --rgsc_pid command line
		return 0;
	}

	size_t pos = 0;
	std::wstring tmp(m_HomeDir);
	std::wstring toFind(L"*%20*"); // can't have a * in a real filename so using *%20* as a placeholder
	std::wstring replacement(L" ");
	while(true)
	{
		pos = tmp.find(toFind, pos);
		if(pos != std::wstring::npos)
		{
			tmp.replace(pos, toFind.length(), replacement);
			pos += replacement.length();
		}
		else
		{
			break;
		}
	}

	wcsncpy(m_HomeDir, tmp.c_str(), MAX_PATH - 1);

	if(m_IpcChannelName[0] != '\0')
	{
		m_Ipc.Init(Ipc::MODE_CLIENT, m_IpcChannelName);
		m_IpcDelegate.Bind(this, &SubProcess::OnMessageReceived);
		m_Ipc.AddDelegate(&m_IpcDelegate);
	}

	int exit_code = InitCef(argc, argv);
	if(exit_code >= 0)
	{
		return exit_code;
	}

	// if we get this far, we're the browser process
	// < 0 means everything is fine (to match CEF)
	return -1;
}

bool SubProcess::IsParentProcessRunning(DWORD pid)
{
	// TODO: NS - it would be better to duplicate the handle in the parent process and send it to the subprocess.
	//			  that way we wouldn't have to openprocess/closehandle here, we could just WaitForSingleObject on the parent process handle.
	HANDLE process = OpenProcess(SYNCHRONIZE, FALSE, pid);
	DWORD ret = WaitForSingleObject(process, 0);
	CloseHandle(process);
	return ret == WAIT_TIMEOUT;
}

void SubProcess::Update()
{
	if(m_ParentPid != 0)
	{
		unsigned curTime = timeGetTime();

		bool isTimeToCheckParent = (m_Quit == false) && (curTime >= m_NextParentCheckTime);
		if(isTimeToCheckParent)
		{
			// check every 2 seconds
			m_NextParentCheckTime = curTime + 2000;
		}

		// if the renderer subprocess crashed/exited before the browser subprocess
		// we would never get a window is closed event. In order to prevent a situation
		// that would cause the browser subprocess to never exit in this case, set
		// the window is closed flag if we haven't received the signal for some time.
		if(m_Quit == true)
		{
			if((curTime >= m_ParentClosedTime + 5000))
			{
				SetWindowIsClosed();
			}
		}

		if(isTimeToCheckParent && (IsParentProcessRunning(m_ParentPid) == false))
		{
			// to shutdown the CEF properly, we need to:
			// 1. close all browser windows,
			// 2. wait for all the OnBeforeClose() events (received in window.cpp)
			//		- this calls SetWindowClosed() for each window which we listen for before calling CefShutdown()
			// 3. call CefShutdown()

			// now that we've detected the parent process has exited, start the
			// shutdown procedure by first closing all browser windows
			m_ParentClosedTime = curTime;

			// If the window is not yet closed, force it closed.
			if(m_Window && !m_Window->IsClosing())
			{
				m_Window->Close(true);
			}

			m_Window = NULL;
			m_Quit = true;
		}
		else
		{
			m_Ipc.Update();
			CefDoMessageLoopWork();
		}
	}
}

void SubProcess::Shutdown()
{
 	m_Ipc.RemoveDelegate(&m_IpcDelegate);
	m_Ipc.Shutdown();

	m_ParentPid = 0;

	// finally, call CefShutdown(). This deletes the subprocess object and our destructor is called.
	CefShutdown(); 
}

Ipc* SubProcess::_GetIpc()
{
	return &m_Ipc;
}

void SubProcess::SetWindowIsClosed()
{
	m_WindowIsClosed = true;
	m_Window = NULL;
}

void SubProcess::OnCreateTiledWindowMessage(unsigned int id, unsigned int width, unsigned int height, float scale, ViewMsg_Tile_Params tiles, std::string sharedMemoryName)
{
	m_Window = new SubProcessWindow(id, (int)width, (int)height, scale, tiles, sharedMemoryName);
}

void SubProcess::OnCreateWindowMessage(unsigned int id, ViewMsg_Rect_Params rect, ViewMsg_CreateWindow_Params window)
{
	m_Window = new SubProcessWindow(id, rect, window);
}

void SubProcess::OnHibernate()
{
	m_Quit = true;
}

void SubProcess::OnDestroyWindowMessage(unsigned int id)
{
	if(m_Window)
	{
		delete m_Window;
		m_Window = NULL;
	}
}

void SubProcess::OnShutdownMessage()
{
	m_Quit = true;
}

void SubProcess::OnMessageReceived(const IPC::Message& message)
{
	if(message.routing_id() == MSG_ROUTING_CONTROL)
	{
		bool handled = true;
		bool valid = false;

		IPC_BEGIN_MESSAGE_MAP_EX(SubProcess, message, valid)
		IPC_MESSAGE_HANDLER(ViewMsg_CreateWindow, OnCreateWindowMessage)			
		IPC_MESSAGE_HANDLER(ViewMsg_CreateTiledWindow, OnCreateTiledWindowMessage)
		IPC_MESSAGE_HANDLER(ViewMsg_Hibernate, OnHibernate);
		IPC_MESSAGE_HANDLER(ViewMsg_DestroyWindow, OnDestroyWindowMessage)
		IPC_MESSAGE_HANDLER(ViewMsg_Shutdown, OnShutdownMessage);
		IPC_MESSAGE_UNHANDLED(handled = false)
		IPC_END_MESSAGE_MAP_EX()

		Assert(handled);
		Assert(valid);
	}
	else
	{
		// TODO: NS - get window out of map (key = message.routing_id())
		SubProcessWindow* window = m_Window;

		if(!m_WindowIsClosed && AssertVerify(window))
		{
			window->OnMessageReceived(message);
		}
		else if(message.is_sync())
		{
			// The window doesn't exist, so we must respond or else the caller will hang waiting for a reply.
			IPC::Message* reply = IPC::SyncMessage::GenerateReply(&message);
			reply->set_reply_error();
			m_Ipc.Send(reply);
		}
	}
}

#if __DEV
void OutputMsg(int severity, const char* file, int line, const char* func, const char* condition, const char *fmt, ...)
{
	char message[4096];

	int len = 0;
	char* msg = &message[0];

	bool hasCondition = (condition != NULL) && (condition[0] != '\0');
	bool hasFmt = (fmt != NULL) && (fmt[0] != '\0');

	if(hasCondition)
	{
		if(func)
		{
			_snprintf(message, sizeof(message), "\n\nFile: %s\nLine: %d\nFunction: %s\nAssertion: %s%s", file, line, func, condition, hasFmt ? "\nMessage: " : "");
		}
		else
		{
			_snprintf(message, sizeof(message), "\n\nFile: %s\nLine: %d\nAssertion: %s%s", file, line, condition, hasFmt ? "\nMessage: " : "");
		}
		message[sizeof(message) - 1] = '\0';

		len = (int)strlen(message);
		msg = &message[len];
	}

	if(hasFmt)
	{
		va_list args;
		va_start(args, fmt);
		_vsnprintf_s(msg, sizeof(message) - len - 1, _TRUNCATE, fmt, args);
		va_end(args);
	}

	std::string sMessage("Subprocess: ");
	sMessage += message;
	rgsc::SubProcess::GetInstance()._GetIpc()->Send(new ViewHostMsg_DebugOutput(severity, sMessage));
}
#endif

void SubProcess::CreateBrowserDelegates(BrowserDelegateSet& delegates)
{

}

void SubProcess::CreateRenderDelegates(RenderDelegateSet& delegates)
{
	client_renderer::CreateRenderDelegates(delegates);
}

void SubProcess::RegisterCustomSchemes(CefRawPtr<CefSchemeRegistrar> registrar, std::vector<CefString>& cookiable_schemes)
{
	//scheme_test::RegisterCustomSchemes(registrar, cookiable_schemes);
}

void SubProcess::OnBeforeCommandLineProcessing(const CefString& process_type, CefRefPtr<CefCommandLine> command_line)
{

}

void SubProcess::OnRegisterCustomSchemes(CefRawPtr<CefSchemeRegistrar> registrar)
{
	// Default schemes that support cookies.
	cookieable_schemes_.push_back("http");
	cookieable_schemes_.push_back("https");

	RegisterCustomSchemes(registrar, cookieable_schemes_);
}

void SubProcess::OnContextInitialized()
{
	CreateBrowserDelegates(browser_delegates_);

	// Register cookieable schemes with the global cookie manager.
	CefRefPtr<CefCookieManager> manager = CefCookieManager::GetGlobalManager(nullptr);
	Assert(manager.get());
	manager->SetSupportedSchemes(cookieable_schemes_, nullptr);

	BrowserDelegateSet::iterator it = browser_delegates_.begin();
	for(; it != browser_delegates_.end(); ++it)
	{
		(*it)->OnContextInitialized(this);
	}
}

void SubProcess::OnBeforeChildProcessLaunch(CefRefPtr<CefCommandLine> command_line)
{
	if(m_Window)
	{
#if RSG_DEV
		if (!m_DisableCaptchaHeader.empty() && !m_DisableCaptchaHeaderValue.empty())
		{
			command_line->AppendSwitchWithValue("no-captcha-header", m_DisableCaptchaHeader.c_str());		
			command_line->AppendSwitchWithValue("no-captcha-header-value", m_DisableCaptchaHeaderValue.c_str());		
		}
#endif

		m_Window->OnBeforeChildProcessLaunch(this, command_line);
	}

	BrowserDelegateSet::iterator it = browser_delegates_.begin();
	for(; it != browser_delegates_.end(); ++it)
	{
		(*it)->OnBeforeChildProcessLaunch(this, command_line);
	}
}

void SubProcess::OnRenderProcessThreadCreated(
    CefRefPtr<CefListValue> extra_info)
{
	if(m_Window)
	{
		m_Window->OnRenderProcessThreadCreated(this, extra_info);
	}

	BrowserDelegateSet::iterator it = browser_delegates_.begin();
	for(; it != browser_delegates_.end(); ++it)
	{
		(*it)->OnRenderProcessThreadCreated(this, extra_info);
	}
}

void SubProcess::OnRenderThreadCreated(CefRefPtr<CefListValue> extra_info)
{
	CreateRenderDelegates(render_delegates_);

	RenderDelegateSet::iterator it = render_delegates_.begin();
	for(; it != render_delegates_.end(); ++it)
	{
		(*it)->OnRenderThreadCreated(this, extra_info);
	}
}

void SubProcess::OnWebKitInitialized()
{
	RenderDelegateSet::iterator it = render_delegates_.begin();
	for(; it != render_delegates_.end(); ++it)
	{
		(*it)->OnWebKitInitialized(this);
	}
}

void SubProcess::OnBrowserCreated(CefRefPtr<CefBrowser> browser)
{
	RenderDelegateSet::iterator it = render_delegates_.begin();
	for(; it != render_delegates_.end(); ++it)
	{
		(*it)->OnBrowserCreated(this, browser);
	}
}

void SubProcess::OnBrowserDestroyed(CefRefPtr<CefBrowser> browser)
{
	RenderDelegateSet::iterator it = render_delegates_.begin();
	for(; it != render_delegates_.end(); ++it)
	{
		(*it)->OnBrowserDestroyed(this, browser);
	}
}

CefRefPtr<CefLoadHandler> SubProcess::GetLoadHandler()
{
	CefRefPtr<CefLoadHandler> load_handler;
	RenderDelegateSet::iterator it = render_delegates_.begin();
	for(; it != render_delegates_.end() && !load_handler.get(); ++it)
	{
		load_handler = (*it)->GetLoadHandler(this);
	}

	return load_handler;
}

bool SubProcess::OnBeforeNavigation(CefRefPtr<CefBrowser> browser,
                                    CefRefPtr<CefFrame> frame,
                                    CefRefPtr<CefRequest> request,
                                    NavigationType navigation_type,
                                    bool is_redirect)
{
	RenderDelegateSet::iterator it = render_delegates_.begin();
	for(; it != render_delegates_.end(); ++it)
	{
		if((*it)->OnBeforeNavigation(this, browser, frame, request,
		                             navigation_type, is_redirect))
		{
			return true;
		}
	}

	return false;
}

void SubProcess::OnContextCreated(CefRefPtr<CefBrowser> browser,
                                  CefRefPtr<CefFrame> frame,
                                  CefRefPtr<CefV8Context> context)
{
	RenderDelegateSet::iterator it = render_delegates_.begin();
	for(; it != render_delegates_.end(); ++it)
	{
		(*it)->OnContextCreated(this, browser, frame, context);
	}
}

void SubProcess::OnContextReleased(CefRefPtr<CefBrowser> browser,
                                   CefRefPtr<CefFrame> frame,
                                   CefRefPtr<CefV8Context> context)
{
	RenderDelegateSet::iterator it = render_delegates_.begin();
	for(; it != render_delegates_.end(); ++it)
	{
		(*it)->OnContextReleased(this, browser, frame, context);
	}
}

void SubProcess::OnUncaughtException(CefRefPtr<CefBrowser> browser,
                                     CefRefPtr<CefFrame> frame,
                                     CefRefPtr<CefV8Context> context,
                                     CefRefPtr<CefV8Exception> exception,
                                     CefRefPtr<CefV8StackTrace> stackTrace)
{
	RenderDelegateSet::iterator it = render_delegates_.begin();
	for(; it != render_delegates_.end(); ++it)
	{
		(*it)->OnUncaughtException(this, browser, frame, context, exception,
		                           stackTrace);
	}
}

void SubProcess::OnFocusedNodeChanged(CefRefPtr<CefBrowser> browser,
                                      CefRefPtr<CefFrame> frame,
                                      CefRefPtr<CefDOMNode> node)
{
	RenderDelegateSet::iterator it = render_delegates_.begin();
	for(; it != render_delegates_.end(); ++it)
	{
		(*it)->OnFocusedNodeChanged(this, browser, frame, node);
	}
}

bool SubProcess::OnProcessMessageReceived(
    CefRefPtr<CefBrowser> browser,
    CefProcessId source_process,
    CefRefPtr<CefProcessMessage> message)
{
	Assert(source_process == PID_BROWSER);

	bool handled = false;

	RenderDelegateSet::iterator it = render_delegates_.begin();
	for(; it != render_delegates_.end() && !handled; ++it)
	{
		handled = (*it)->OnProcessMessageReceived(this, browser, source_process,
		          message);
	}

	return handled;
}

} // namespace rgsc

// Note: multiple processes are spawned from this executable.
// CEF will spawn these processes with command-line args that turn the process into the desired type (eg. --type=renderer)
// Process types:
// 1. The browser - handles browser management (virtual offscreen windows/tabs), mouse/keyboard input, etc.
// 2. The renderer(s) - handles HTML rendering and houses the V8 Javascript engine. Up to one renderer per URL loaded, but renderers are sometimes shared between multiple URLs.
// 3. The GPU process(s) - GPU accelerated compositing, etc.
// 4. Other CEF/Chrome processes - eg. plugins if necessary
int WINAPI wWinMain(HINSTANCE hInstance,
					HINSTANCE hPrevInstance,
					PWSTR pCmdLine,
					int nCmdShow)
{
	// wait for debugger to attach
//   	while(true)
//   	{
//   		Sleep(50);
//   	}

	// all normal invocations pass at least 2 params
	// if the user manually launches the subprocess, exit immediately
	if((pCmdLine == NULL) || (pCmdLine[0] == L'\0'))
	{
		return -1;
	}
		
	int nArgs = 0;
	LPWSTR* szArglist = CommandLineToArgvW(GetCommandLineW(), &nArgs);
	if(szArglist == NULL)
	{
		return -1;
	}

	// all normal invocations pass at least 2 params
	if(nArgs < 2)
	{
		return -1;
	}

	rgsc::SubProcess& subprocess = rgsc::SubProcess::GetInstance();

	int exit_code = subprocess.Init(nArgs, szArglist);

	if(exit_code >= 0)
	{
		return exit_code;
	}

	while(!subprocess.WantsToQuit() || !subprocess.WindowIsClosed())
	{
		subprocess.Update();

		// save cpu cycles for other processes, we don't need to update more than 100 times per second
		Sleep(10);
	}

	// found that even after closing the browser windows and waiting for the close events,
	// I need to let it process the message loop for a few more iterations before exiting
	// the subprocess, otherwise the Chrome cache is unusable on all subsequent launches.
	for(unsigned i = 0; i < 200; ++i)
	{
		subprocess.Update();
		Sleep(10);
	}

	subprocess.Shutdown();

	return 0;
}
