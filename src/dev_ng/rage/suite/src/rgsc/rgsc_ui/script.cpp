#include "script.h"
#include "js2c_helper.h"
#include "rgscwindow.h"

namespace rgsc
{
RgscWindow* Script::m_Window = NULL;

typedef unsigned int u32;

template <class T,int initSize = 0> class scrHash {
	struct Slot { u32 code; T data; };

	enum { EMPTY, DELETED };
public:

	scrHash() : m_Slots(NULL), m_InitSize(initSize) { }

	void Init(int size) {
		m_Slots = rage_new Slot[m_Size = size];
		for (int i=0; i<size; i++)
			m_Slots[i].code = 0;
		m_Occupancy = 0;
	}

	void Kill() {
		delete[] m_Slots;
		m_Slots = NULL;
	}

	~scrHash() { Kill(); }

	void Delete() {
		// This invokes delete on every non-empty slot.
		int size = m_Size;
		for (int i=0; i<size; i++) {
			if (m_Slots[i].code) {
				m_Slots[i].code = 0;
				delete m_Slots[i].data;
			}
		}
		m_Occupancy = 0;
	}

	T Lookup(u32 hashCode) {
		if (!m_Size)
			return NULL;
		FastAssert(hashCode);
		u32 slot = hashCode % m_Size;
		u32 delta = hashCode;
		u32 thisCode;
		while ((thisCode = m_Slots[slot].code) != hashCode && thisCode) {
			delta = (delta >> 1) + 1;
			slot = (slot + delta) % m_Size;
		}
		if (!thisCode)
			return NULL;
		else
			return m_Slots[slot].data;
	}

#if __DEV
	void Test()
	{
		if (!m_Size)
			return;

		u32 totalProbes = 0;
		u32 maxProbes = 0;

		for(int i=0; i<m_Size; i++) {
			u32 hashCode = m_Slots[i].code;
			if(hashCode > DELETED)
			{
				u32 slot = hashCode % m_Size;
				u32 delta = hashCode;
				u32 thisCode;
				u32 numProbes = 1;
				while ((thisCode = m_Slots[slot].code) != hashCode && thisCode) {
					numProbes++;
					delta = (delta >> 1) + 1;
					slot = (slot + delta) % m_Size;
				}

				Assert(thisCode);
				Displayf("%u took %u probes", hashCode, numProbes);
				totalProbes += numProbes;

				if(numProbes > maxProbes)
				{
					maxProbes = numProbes;
				}
			}
		}

		Displayf("Load Factor: %f", (float)m_Occupancy / m_Size);
		Displayf("Avg number of probes: %f", (float)totalProbes / m_Occupancy);
		Displayf("Max probes: %d", maxProbes);
	}
#endif

	bool Insert(u32 hashCode,T data) {
		if (!m_Size)
			Init(m_InitSize);
		if (m_Occupancy == m_Size)
			return false;
		FastAssert(hashCode);
		u32 slot = hashCode % m_Size;
		u32 delta = hashCode;
		while (m_Slots[slot].code > DELETED) {
			if (m_Slots[slot].code == hashCode)
				return false;
			delta = (delta >> 1) + 1;
			slot = (slot + delta) % m_Size;
		}
		m_Slots[slot].code = hashCode;
		m_Slots[slot].data = data;
		++m_Occupancy;
		return true;
	}

	bool Remove(u32 hashCode) {
		u32 slot = hashCode % m_Size;
		u32 delta = hashCode;
		while (m_Slots[slot].code) {
			if (m_Slots[slot].code == hashCode) {
				m_Slots[slot].code = DELETED;
				m_Slots[slot].data = NULL;
				--m_Occupancy;
				return true;
			}
			delta = (delta >> 1) + 1;
			slot = (slot + delta) % m_Size;
		}
		return false;
	}

private:
	Slot *m_Slots;
	int m_Size, m_InitSize, m_Occupancy;
};

static scrHash<Cmd*> s_CommandHash;

inline u32 atFinalizeHash(const u32 partialHashValue)
{
	u32 key = partialHashValue;
	key += (key << 3);	//lint !e701
	key ^= (key >> 11);	//lint !e702
	key += (key << 15);	//lint !e701
	return key;
}

u32 atDataHash(const char *data, int size, const u32 initValue = 0)
{
	// This is the one-at-a-time hash from this page:
	// http://burtleburtle.net/bob/hash/doobs.html
	// (borrowed from /soft/swat/src/swcore/string2key.cpp)

	u32 key = initValue;
	for(int count = 0; count < size; count++)
	{
		key += data[count];
		key += (key << 10);	//lint !e701
		key ^= (key >> 6);	//lint !e702
	}

	return atFinalizeHash(key);
}

u32 ComputeHash(RgscWideString cmdName)
{
	u32 hashCode = atDataHash((char*)cmdName.data(), (int)cmdName.length() * sizeof(RgscWideString::Type));
	if(hashCode < 2)
	{
		hashCode += 2; // disallow zero (EMPTY) and one (DELETED)
	}
	return hashCode;
}

void Script::RegisterCommand(RgscWideString cmdName, rgsc::Cmd* cmd) {
	u32 hashCode = ComputeHash(cmdName);
	if(!s_CommandHash.Insert(hashCode, cmd))
	{
		AssertMsg(false, "Existing native command collided with '%ls', or hash table is full (raise scrThread::InitClass value).", cmdName.data());
	}
}

void Script::CallFunction(Info& info)
{
	u32 commandHash = ComputeHash(*info.m_FuncName);

	Cmd* cmd = s_CommandHash.Lookup(commandHash);
	AssertMsg(cmd, "Unable to find the command for %ls.", info.m_FuncName->data());

	if(cmd)
	{
		(*cmd)(info);
	}
}

void Script::Shutdown()
{
	s_CommandHash.Kill();
}

// called by the renderer subprocess when our function bindings are bound to the window/frame
void RGSC_FINISHED_FUNCTION_BINDINGS()
{
	GetRgscConcreteInstance()->_GetUiInterface()->SetJavascriptReadyToAcceptCommands();
}

// called when C++ attempts to call a Javascript function that doesn't exist
void RGSC_FUNCTION_NOT_DEFINED(RgscWideString functionName)
{
	AssertMsg(false, "Javascript function '%ls' does not exist", functionName.data());

	RgscUtf8String utf8 = rgsc::WideToUTF8(functionName); 
	GetRgscConcreteInstance()->_GetUiInterface()->JavascriptFunctionDoesntExist(utf8.data());
	stringUtil_free(utf8);
}

void Script::SetupCommands(RgscWindow* window, unsigned int maxScriptCommands)
{
	m_Window = window;

	s_CommandHash.Init(maxScriptCommands + 32);

	SCR_REGISTER(RGSC_FINISHED_FUNCTION_BINDINGS);

	SCR_REGISTER(RGSC_FUNCTION_NOT_DEFINED);

	SetupCommands();
}

} // namespace rgsc
