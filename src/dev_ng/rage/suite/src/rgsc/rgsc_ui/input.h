#ifndef RGSC_INPUT_H
#define RGSC_INPUT_H

# pragma warning(push)
# pragma warning(disable: 4668)
# include <windows.h>
# include <SdkDdkVer.h>
# pragma warning(pop)

// #ifdef around touch input support for people who don't have Windows SDK 7.1 installed.
#define SUPPORT_TOUCH_INPUT (_WIN32_WINNT > 0x0600)

// #ifdef around gamepad remapping which is a feature we can add support for (i.e. Steam controllers)
#define SUPPORT_SCUI_GAMEPAD_REMAPPING (0)

// make the SC SDK IME-aware
#define IME_TEXT_INPUT (0)
#define IME_USE_OS_UI false

#include "eventq.h"

// interfaces
#include "pad_interface.h"
#include "rgsc_ui_interface.h"

// rage includes
#include "system/ipc.h"

using namespace rage;

namespace rgsc
{

typedef IGamepadManagerLatestVersion::InputMode Mode;

class Rgsc_Input
{
public:

	enum MouseButton
	{
		MOUSE_LEFT		= 0x00, 
		MOUSE_MIDDLE	= 0x01, 
		MOUSE_RIGHT		= 0x02, 
		MOUSE_EXTRABTN1	= 0x08, 
		MOUSE_EXTRABTN2	= 0x10,
		MOUSE_EXTRABTN3	= 0x12,
		MOUSE_EXTRABTN4	= 0x14,
		MOUSE_EXTRABTN5	= 0x18,
	};

	enum CloseReason
	{
		METHOD_ESCAPE = 1,
		METHOD_HOME = 2,
		METHOD_CONTROLLER = 3,
	};

	enum InputUpdateRate
	{
		INPUT_UPDATE_RATE_HIGH = 4,
		INPUT_UPDATE_RATE_LOW = 16,
		INPUT_UPDATE_RATE_NOT_VISIBLE = 33
	};

	struct InputState
	{
		int Held;
		int Pressed;
		int Released;

		float LeftX, LeftY;
		float RightX, RightY;

		bool HadInputThisUpdate;

		InputState()
		{
			Clear();
		}

		void Clear()
		{
			Held = 0;
			Pressed = 0;
			Released = 0;
			LeftX = 0;
			LeftY = 0;
			RightX = 0;
			RightY = 0;
			HadInputThisUpdate = false;
		}

		void operator=( const InputState& that )
		{
			Held = that.Held;
			Pressed = that.Pressed;
			Released = that.Released;
			LeftX = that.LeftX;
			LeftY = that.LeftY;
			RightX = that.RightX;
			RightY = that.RightY;
			HadInputThisUpdate = that.HadInputThisUpdate;
		}
		void operator+=( const InputState& that )
		{
			Held |= that.Held;
			Pressed |= that.Pressed;
			Released |= that.Released;
		}
		bool operator!=( const InputState& that ) const
		{
			return !(*this == that);
		}
		bool operator==( const InputState& that ) const
		{
			if (Held != that.Held)
				return false;
			if (Pressed != that.Pressed)
				return false;
			if (Released != that.Released)
				return false;
			if (LeftX != that.LeftX)
				return false;
			if (LeftY != that.LeftY)
				return false;
			if (RightX != that.RightX)
				return false;
			if (RightY != that.RightY)
				return false;
			if (HadInputThisUpdate != that.HadInputThisUpdate)
				return false;
			return true;
		}
	};

	static void InputThread(void*);
	static void Init(WindowHandle hDeviceWindow, const WindowHandle* additionalWindows, unsigned numAdditionalWindows, bool bSubclassWndProc = true);
	static void Update();
	static void UpdateInternal(Mode mode, unsigned numPads);
	static void UpdatePadDeflection(const unsigned ctrlIdx, Mode mode, float deflectionX, float deflectionY, float scrollDeflectionX, float scrollDeflectionY);
	static void UpdatePadPointers(const unsigned ctrlIdx, Mode mode, bool bHasPointerData, int px, int py);
	static void UpdatePadInput(InputState& state);
	static void Shutdown();

	static void SendInputMethodToScui(IGamepadManagerLatestVersion::InputMode mode, unsigned controllerIndex);
	static void SetIsUsingController(bool isUsingController, Mode = IGamepadManagerLatestVersion::INPUTMODE_INVALID, unsigned controllerIndex = 0);
	static bool IsUsingController();
	static bool IsButtonDown(int button);
	static Mode GetControllerInputMode();
	static int GetControllerIndex(Mode inputMode);
	static int CountPads();

	static void ClearControllerEvent();
	static void ConsumeControllerEvent(ioEvent::ioEventType eventType);
	static void ProduceControllerEvent(ioEvent::ioEventType eventType);

	static int GetModifiers();

	static bool IsInputEnabled();
	static bool IsForeground();
	static bool IsMouseInWindow();
	static void SetWindowMargins(int top, int left, int bottom, int right);

	enum ImeEvent
	{
		IME_PAGE_UP,
		IME_PAGE_DOWN,
	};


	// PURPOSE:	Handles an IME event from the UI.
	static void HandleImeEvent(ImeEvent eventId);

	// PURPOSE
	//	Map between RgscPlatformMsg and Windows messages.
	static UINT MapRgscPlatformMsgToWindowsMsg(RgscPlatformMessageHandler::RgscPlatformMsg msg);
	static RgscPlatformMessageHandler::RgscPlatformMsg MapWindowsMsgToRgscPlatformMsg(UINT iMsg);

	// PURPOSE
	//	Enable/Disable the forwarding of mouse and/or keyboard events to the chrome subprocess
	static void SetKeyboardMessageForwarding(bool bEnabled);
	static void SetMouseMessageForwarding(bool bEnabled);

	// PURPOSE
	//	Handle an incoming mouse move message - either from the subprocess or from windows.
	static void HandleMouseMove(int posX, int posY);

private:

#if SUPPORT_TOUCH_INPUT
	static BOOL GetTouchInputInfo(HTOUCHINPUT hTouchInput, UINT cInputs, PTOUCHINPUT pInputs);
	static BOOL CloseTouchInputHandle(HTOUCHINPUT hTouchInput);
	static LRESULT OnTouch(WindowHandle hWnd, WPARAM wParam, LPARAM lParam);
#endif

#if IME_TEXT_INPUT
	static const unsigned TEXT_BUFFER_LENGTH = 31;

	static void SendImeEventToScui(const char* msg, const char* lParam, bool composition, bool candidates, bool finalized);

	static LRESULT CALLBACK ImeMessageHandler(WindowHandle hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

	// PURPOSE:	Starts an IME text input session.
	static void ImeStartTextInput(WindowHandle hwnd, const char* msg, const char* lParam);

	// PURPOSE:	Updates the IME composition text.
	// NOTES:	The composition test is the text being entered.
	static void ImeUpdateCompositionText(WindowHandle hwnd, const char* msg, const char* lParam);

	// PURPOSE:	Updates the IME text candidates.
	// NOTES:	The text candidates are the available candidates that the entered text could be. These are paged so there
	//			could be more than available than we return. The system handles the paging.
	static void ImeUpdateCandidateTexts(WindowHandle hwnd, const char* msg, const char* lParam);

	// PURPOSE:	Stops an IME text input session.
	static void ImeStopTextInput(WindowHandle hwnd, const char* msg, const char* lParam);

	// PURPOSE:	Retrieves the IME candidate that the user selected.
	// NOTES:	The selected text will be added to the text available in GetInputText().
	static void ImeRetrieveTextResult(WindowHandle hwnd, const char* msg, const char* lParam);

	// PURPOSE:	Indicates if an IME text input session is in progress.
	static bool ImeIsInProgress();

	// PURPOSE:	Indicates if there are any IME text candidates available.
	static bool ImeHasCandidates();

	// PURPOSE:	Indicates the number of IME text candidates available.
	static u32 ImeGetNumberOfCandidates();

	// PURPOSE:	Indicates which text candidate is currently selected by the user.
	// RETURNS:	The selected candidate index or IME_INVALID_CANDIDATE_INDEX if no candidate is selected.
	// NOTES:	This is used so that we can highlight the relevant candidate and can be passed in to ImeGetCandidateText()
	//			to get the relevant text (unless IME_INVALID_CANDIDATE_INDEX is returned). The candidate selection is
	//			handled by the system.
	static u32 ImeGetSelectedCandidateIndex();

	// PURPOSE:	Retrieves the text of a candidate.
	// PARAMS:	candidateIndex - The index of a candidate. This must be between 0 and ImeGetNumberOfCandidates().
	static const wchar_t* ImeGetCandidateText(u32 candidateIndex);

	// PURPOSE:	Retrieves the composition text.
	// NOTES:	The composition test is the text being entered.
	static const wchar_t* ImeGetCompositionText();

	// PURPOSE:	Maximum number of string candidates.
	// NOTES:	This is 9 as the user can select text from 1-9 by pressing the relevant number.
	const static u32 MAX_IME_CANDIDATES = 9;

	// PURPOSE:	Indicates an invalid candidate index.
	const static u32 IME_INVALID_CANDIDATE_INDEX = MAX_IME_CANDIDATES;

	// PURPOSE:	Initialize IME text input.
	static void ImeBegin();

	// PURPOSE:	Shuts down IME text input.
	static void ImeEnd();

	enum ImeStateType
	{
		// PURPOSE:	The index of the currently editing IME state.
		IME_CURRENT_STATE,

		// PURPOSE:	The number of IME editing states.
		IME_NUM_STATES,
	};

	// PURPOSE:	Contains the state of IME text input.
	struct ImeState
	{
		// PURPOSE: Constructs an IME state.
		ImeState();

		// PURPOSE: Clears an IME state.
		void Clear();

		HIMC m_hImc;

		// PURPOSE:	The number of IME candidates available.
		u32 m_NumCandidates;

		// PURPOSE:	The total number of IME candidates available.
		u32 m_TotalCandidates;

		// PURPOSE:	The index of the first candidate being shown
		u32 m_PageStart;

		// PURPOSE:	The number of candidates on each page.
		u32 m_PageSize;

		// PURPOSE: The index of the selected IME candidate.
		u32 m_SelectedCandidateIndex;

		// PURPOSE:	The IME composition text.
		wchar_t m_CompositionText[TEXT_BUFFER_LENGTH];

		// PURPOSE:	The IME candidates.
		wchar_t m_Candidates[MAX_IME_CANDIDATES][TEXT_BUFFER_LENGTH];

		// PURPOSE: Indicates that IME text input is in progress.
		bool m_InProgress;

		// PURPOSE: Indicates that IME candidates list is open.
		bool m_IsCandidatesWindowOpen;
	};

	// PURPOSE:	The states of the IME text input.
	static ImeState sm_ImeState[IME_NUM_STATES];
	//static wchar_t sm_TextBuffer[TEXT_BUFFER_LENGTH];
	static wchar_t sm_CurrentTextBuffer[TEXT_BUFFER_LENGTH];
	//static ioMapperParameter sm_CurrentTextBufferKeys[TEXT_BUFFER_LENGTH];
	static u32  sm_CurrentLength;

#endif // IME_TEXT_INPUT

	// Mouse Utilities
	static bool GetCursorPosition(LPPOINT p);
	static bool GetClientCursorPosition(LPPOINT p);
	static bool ClampToScreen(float& x, float& y);
	static bool ClampToScreen(LPPOINT p);
	static void SetNewCursorPos(float dx, float dy);
	static void CheckMouseInWindow();

	// Simulation
	static void SimulateKeyboardEvent(ioEvent::ioEventType eventType, unsigned msg, WPARAM wParam, LPARAM lParam);
	static void SimulateMouseEvent(ioEvent::ioEventType eventType, unsigned msg, WPARAM wParam, LPARAM lParam);
	static bool IsSimulatingKeyboardEvents();
	static bool IsSimulatingMouseEvents();

	// Windows Message Handlers
	static LRESULT MessageHandlerReturnValue(WNDPROC originalProc, WindowHandle hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);
	static LRESULT CALLBACK WindowsMessageHandler(WindowHandle hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

#if !__NO_OUTPUT
	static const char* GetInputString(int inputMode);
#endif

	// Cursor properties
	static HCURSOR m_Cursor;
	static unsigned int m_MouseMoveCount;
	static int s_OldMouseX;
	static int s_OldMouseY;
	static float sm_TargetX, sm_TargetY;
	static RECT sm_WindowMargins;

	// Gamepad properties
	static int m_ControllerEventsToConsume;
	static unsigned m_ControllerMouseStartTime;
	static unsigned m_ControllerScrollStartTime;
	static unsigned sm_uTimeOfLastDeviceChange;
	static bool sm_bInputThisFrame;
	static bool m_IsUsingController[IGamepadManagerLatestVersion::INPUTMODE_MAX];

	// Input properties
	static bool sm_bInputEnabled;
	static bool sm_bInputSubclassing;
	static bool sm_bIsForegroundWindow;
	static bool sm_bMouseInWindow;

	// Settings
	static bool sm_bMouseEventForwarding;
	static bool sm_bKeyboardEventForwarding;

	static sysCriticalSectionToken sm_DeviceChangeCs;
	static sysCriticalSectionToken sm_Cs;

	static sysIpcThreadId sm_ThreadId;

#if	SUPPORT_TOUCH_INPUT
	static unsigned m_TouchStartTime;
#endif
};

} // namespace rgsc

#endif // RGSC_INPUT_H
