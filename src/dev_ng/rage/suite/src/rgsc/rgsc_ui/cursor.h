#ifndef RGSC_CURSOR_H 
#define RGSC_CURSOR_H

#include "rgsc_common.h"

// D3D9 and D3D11 includes
#pragma warning(push)
#pragma warning(disable: 4668)
#include <d3d11.h>
#include <d3d9.h>
#pragma warning(pop)

// RAGE includes
#include "atl/array.h"
#include "atl/map.h"
#include "graphics.h"

namespace rgsc
{

////////////////////////////////////////////////////////////////////
// RgscCursorResource
//  - Base class for cursors that are rendered on screen.
//	- Contains dimensions for the cursor texture.
//  - Subclasses must implement texture setup, rendering and freeing.
////////////////////////////////////////////////////////////////////
class RgscCursorResource
{
public:

	RgscCursorResource(int width, int height);

	// PURPOSE
	//	Returns true if the cursor has a valid texture to render.
	virtual bool IsValid() = 0;

	// PURPOSE
	//	Release all resources with this cursor.
	virtual void ReleaseResources() = 0;

	// PURPOSE
	//	Return width/height of the cursor
	int GetWidth() { return m_Width; }
	int GetHeight() { return m_Height; }

protected:

	int m_Width;
	int m_Height;
};

////////////////////////////////////////////////////////////////////
// RgscCursorResourceD3D9
//	- D3D9 implementation of cursor resources
//	- Requires a D3D9 device, dimensions and ARGB pixel data.
////////////////////////////////////////////////////////////////////
class RgscCursorResourceD3D9 : public RgscCursorResource
{
	friend class RgscCursor;

public:

	RgscCursorResourceD3D9(IDirect3DDevice9* device, int width, int height, int* pixelData);
	~RgscCursorResourceD3D9();
	
	// PURPOSE
	//	Returns true if the cursor has a valid texture to render.
	bool IsValid();

	// PURPOSE
	//	Release all resources with this cursor.
	void ReleaseResources();

private:

	IDirect3DTexture9* m_Texture;
};

////////////////////////////////////////////////////////////////////
// RgscCursorResourceD3D11
//	- D3D11 implementation of cursor resources
//	- Requires a D3D11 device, dimensions and ARGB pixel data.
////////////////////////////////////////////////////////////////////
class RgscCursorResourceD3D11 : public RgscCursorResource
{
	friend class RgscCursor;

public:

	RgscCursorResourceD3D11(ID3D11Device* device, int width, int height, int* pixelData);
	~RgscCursorResourceD3D11();

	// PURPOSE
	//	Returns true if the cursor has a valid texture to render.
	bool IsValid();

	// Release all resources with this cursor.
	void ReleaseResources();

private:

	ID3D11Texture2D* m_Texture;
	ID3D11ShaderResourceView* m_ShaderResourceView;
};


////////////////////////////////////////////////////////////////////
// RgscCursor
//	- Virtual cursor used for rendering instead of the windows pointer
//	- Intended for use in VR titles, but could be used by any sku 
//	  that sends virtual pointer position.
//  - Virtual game pads set the X/Y position using SetPosition
//  - The graphics devices (graphics_d3d9 and graphics_d3d11) are 
//    responsible for initializing, resetting and rendering the cursor.
//  - When the browser updates the cursor type based on which SCUI 
//    elements are selected, the virtual cursor will update accordingly.
////////////////////////////////////////////////////////////////////
class RgscCursor
{
public:

	RgscCursor();
	~RgscCursor();

	// PURPOSE
	//	Free resources and reset the cursor.
	void Shutdown();

	// D3D9 Functionality
	//	A D3D9 device must call InitD3D9 initially, and then UpdateD3D9/RenderD3D9
	//  on each frame to render the cursor.
	void InitD3D9(IDirect3DDevice9* device);
	void UpdateD3D9(float windowWidth, float windowHeight);
	void RenderD3D9();

	// D3D11 Functionality
	//	A D3D11 device must call InitD3D11 initially, and then UpdateD3D11/RenderD3D11
	//  on each frame to render the cursor.
	void InitD3D11(ID3D11Device* device);
	void UpdateD3D11(ID3D11DeviceContext* context, Rgsc_Graphics::RenderRect& rect);
	void RenderD3D11(ID3D11DeviceContext* context);

	// PURPOSE
	//	Cleanup resources created by the virtual cursor.
	//	Called on device lost/reset.
	void OnLost();

	// PURPOSE
	//	Called when the device for this cursor is reset.
	void OnReset();

	// PURPOSE
	//	Enable/Disable the cursor.
	//	Should be enabled when a valid cursor device is setup.
	void SetEnabled(bool bEnabled);
	bool IsEnabled();

	// PURPOSE
	//	Sets the target x/y pixel coordinate.
	void SetPosition(float x, float y);

	// PURPOSE
	//	Return the virtual cursor coordinates
	float GetX();
	float GetY();

	// PURPOSE
	//	Sets the active cursor type
	void SetCursorType(RgscCursorType cursorType);
	
	// PURPOSE
	//	Returns 'true' if the cursor has been initialized
	bool IsInitialized();

	// PURPOSE
	//	Returns 'true' when the cursor position is within the window,
	//  and we have pointer to render.
	bool IsValid();

private:

	// PURPOSE
	//	Resets all properties to their defaults
	void Clear();

	// PURPOSE
	//	Frees any resources for this cursor.
	void ReleaseResources();

	// PURPOSE
	//	Updates if the cursor is within the rendering window
	void UpdateInWindow(Rgsc_Graphics::RenderRect& rect);

	// PURPOSE
	//	Creates a new cursor for the given type.
	bool CreateNewCursor(RgscCursorType* cursorTypes, int numCursors, int width, int height, int* cursorData);

	enum eDeviceType
	{
		DEVICE_INVALID,
		DEVICE_D3D9,
		DEVICE_D3D11
	};

	// D3D9 Devices and Vertex Buffers
	struct ResourcesD3D9
	{
		ResourcesD3D9();
		~ResourcesD3D9() { Shutdown(); }

		bool Setup(IDirect3DDevice9* device);
		void ReleaseResources();
		void Shutdown();

		IDirect3DDevice9* m_Device;
		IDirect3DVertexBuffer9* m_VertexBuffer;
	};

	// D3D11 Devices and Vertex Buffers
	struct ResourcesD3D11
	{
		ResourcesD3D11();
		~ResourcesD3D11() { Shutdown(); }

		bool Setup(ID3D11Device* device);
		void ReleaseResources();
		void Shutdown();

		ID3D11Device* m_Device;
		ID3D11Buffer* m_VertexBuffer;
	};

	// Common properties
	eDeviceType m_DeviceType;
	RgscCursorType m_CursorType;
	bool m_bEnabled;
	bool m_bInWindow;
	bool m_bDirty;
	float m_X;
	float m_Y;

	// Cursor resources
	RgscCursorResource* m_ActiveCursor;
	rage::atArray<RgscCursorResource*> m_Resources;
	rage::atMap<RgscCursorType, RgscCursorResource*> m_CursorMap;

	// Device specific resources
	ResourcesD3D9   m_D3D9;
	ResourcesD3D11  m_D3D11;
};

extern RgscCursor g_VirtualCursor;

} // namespace rgsc

#endif // RGSC_CURSOR_H