#ifndef RGSC_SCRIPT
#define RGSC_SCRIPT

#include "stringutil.h"
#include "variant.h"
#include "c2js_commands.h"
#include "rgscwindow.h"
#include "rgsc.h"
#include "string/string.h"

using namespace rage;
namespace rgsc
{
class Cmd;

class Script
{
public:
	class Info
	{
	public:
		Info(RgscWideString* funcName,
			 RgscVariant* args,
			 size_t numArgs)
			 : m_FuncName(funcName)
			 , m_Args(args)
			 , m_NumArgs(numArgs)
			 , m_IsAutoAsync(false)
		{
		}

	//private:
		RgscWideString* m_FuncName;
		RgscVariant* m_Args;
		RgscVariant m_Result;
		size_t m_NumArgs;
		bool m_IsAutoAsync;
	};

	static void CallFunction(Info& info);
	static void RegisterCommand(RgscWideString cmdName, rgsc::Cmd* cmd);

	static void SetupCommands(RgscWindow* window, unsigned int maxScriptCommands);
	static void SetupCommands();
	static void Shutdown();

	static RgscWindow *m_Window;
#define json const char*
	JS_COMMANDS
#undef json
};

class Cmd
{
public:
	typedef void (*cmdFnPtr)(rgsc::Script::Info&);

	Cmd(cmdFnPtr cmd) : m_Cmd(cmd) {}

	void operator()(rgsc::Script::Info& info) 
	{
		(*m_Cmd)(info);
	}

	cmdFnPtr GetCmd() {return m_Cmd;}
private:
	cmdFnPtr m_Cmd;
};

} // namespace rgsc

#endif //RGSC_SCRIPT
