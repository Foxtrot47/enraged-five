#ifndef C2JSCOMMANDS
#define C2JSCOMMANDS

#ifndef DOXYGEN_PREPROCESSOR
#include "c2js_helper.h"
#endif

namespace rgsc
{

/** @defgroup C2JS_API C++ to Javascript API 
 *  @{
 */

//! Called when the game wants to auto sign-in a profile.
/**
	\fn RGSC_JS_SIGN_IN(json profile)
	\param profile the profile to sign in.
	\remark
	Example profile:
	\verbatim
	{
		"RockstarId" : 12345
		"Nickname" : "Pestilence",
		"LastSignInTime" : "12312312331",
		"AutoSignIn" : true,
		"Local" : false,
		"ProfileNumber" : 12345, (offline only)
		"SaveEmail" : true,
		"SavePassword" : true,
		"Email" : "pestilence@rockstartoronto.com",
		"Password" : "pesty123",
		"ScAuthToken" : "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789==",
		"Ticket" : "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789==",
		"AvatarUrl" : "GTAIV/GTAIV3.png"
		"RememberedMachineToken" : "1234567890/ABCDEFGHIJKLMNOPQRSTUVWXYZ==
		"TicketRefresh" : true (optional)
	}
	\endverbatim
	LastSignInTime is the number of seconds since 1/1/1970 @ 12:00am UTC.\n
	Email may or may not be omitted if SaveEmail is false.\n
	Password may or may not be omitted if SavePassowrd is false.\n
	It is possible for Email and Password to be given even if they weren't saved.
	For example, when sign-in details are transferred to the game via the launcher.
	Ticket is included when we're doing a signin transfer from game client to launcher or vice-versa.
	The TicketRefresh property indicates the user's Ticket should be used instead of password/tokens.

	\see RGSC_JS_FINISH_SIGN_IN()
	\see RGSC_SIGN_IN()

	\dot
	digraph AutoSignInProtocol {
		label = "Auto Sign-In Protocol";
		graph [truecolor bgcolor="#00000000"]
		node [shape=record, fontname=Arial, fontsize=10];
		rankdir=LR;
		subgraph cluster0 {
			label = "C++";
			AutoSignIn [shape=ellipse, label="Auto Sign-In\nProfile Available"]
			RGSC_SIGN_IN [URL="\ref RGSC_SIGN_IN()"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "Javascript";
			RGSC_JS_SIGN_IN [color=red, URL="\ref RGSC_JS_SIGN_IN()"];
			RGSC_JS_FINISH_SIGN_IN [URL="\ref RGSC_JS_FINISH_SIGN_IN()"];
			color=lightgrey;
		}

		AutoSignIn -> RGSC_JS_SIGN_IN -> RGSC_SIGN_IN -> RGSC_JS_FINISH_SIGN_IN;
	}
	\enddot
*/

//! Called when the game wants to sign the user out.
/**
	\fn RGSC_JS_SIGN_OUT(json signOut)
	\param signOut information about the signOut requested.
	\remark
	Example result:
	\verbatim
	{
		"DuplicateSignIn" : true,
	}
	\endverbatim
	The SCUI should respond with a call to RGSC_SIGN_OUT().\n
	DuplicateSignIn flag may or may not be present. If present and value is true,\n
	this indicates that we are signing out due to a duplicate sign-in attempt.
*/

//! Called when the game has finished its part of the sign in process.
/**
	\fn void RGSC_JS_FINISH_SIGN_IN(json result)
	\param result the results of the sign-in process.
	\remark
	Example result:
	\verbatim
	{
		"SignedIn" : true,
		"SignedOnline" : true,
		"ScAuthTokenError" : false,
		"ProfileSaved" : true,
		"AchievementsSynced" : true,
		"FriendsSynced" : true,
		"Local" : false,
		"NumFriendsOnline" : 42,
		"NumFriendsPlayingSameTitle" : 10,
		"NumBlocked" : 1000,
		"NumFriends" : 250,
		"NumInvitesReceieved" : 100,
		"NumInvitesSent" : 100
	}

	'ScAuthTokenError' is set to TRUE when we were unable to request an SC Auth Token during signin. This should
	be treated as a sign in error, and 'SignedIn' will also return false.
	\endverbatim
	\see RGSC_JS_SIGN_IN()
	\see RGSC_SIGN_IN()

	\dot
	digraph SignInProtocol {
		label = "Sign-In Protocol";
		graph [truecolor bgcolor="#00000000"]
		node [shape=record, fontname=Arial, fontsize=10];
		rankdir=LR;
		subgraph cluster0 {
			label = "C++";
			RGSC_SIGN_IN [URL="\ref RGSC_SIGN_IN()"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "Javascript";
			UserSignIn [shape=ellipse, label="User Signs In"]
			RGSC_JS_FINISH_SIGN_IN [color=red, URL="\ref RGSC_JS_FINISH_SIGN_IN()"];
			color=lightgrey;
		}

		UserSignIn -> RGSC_SIGN_IN -> RGSC_JS_FINISH_SIGN_IN;
	}
	\enddot
*/

//! Called when the game has finished modifying a profile in response to RGSC_MODIFY_PROFILE().
/**
	\fn void RGSC_JS_FINISH_MODIFY_PROFILE(json result)
	\param result the results of the profile modification process.
	\remark
	Example result:
	\verbatim
	{
		"ProfileSaved" : true,
		"ScAuthToken" : "1234567890abcdef=="
		"DownloadedAvatar" : true
	}
	\endverbatim
	\see RGSC_MODIFY_PROFILE()

	\dot
	digraph ModifyProfileProtocol {
		label = "Modify Profile Protocol";
		graph [truecolor bgcolor="#00000000"]
		node [shape=record, fontname=Arial, fontsize=10];
		rankdir=LR;
		subgraph cluster0 {
			label = "C++";
			RGSC_MODIFY_PROFILE [URL="\ref RGSC_MODIFY_PROFILE()"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "Javascript";
			UserModifiesProfile [shape=ellipse, label="User Modifies Profile"]
			RGSC_JS_FINISH_MODIFY_PROFILE [color=red, URL="\ref RGSC_JS_FINISH_MODIFY_PROFILE()"];
			color=lightgrey;
		}

		UserModifiesProfile -> RGSC_MODIFY_PROFILE -> RGSC_JS_FINISH_MODIFY_PROFILE;
	}
	\enddot
*/

//! Called when the player or programmer wants to open or close the blade UI.
/**
	\fn void RGSC_JS_REQUEST_UI_STATE(json request)
	\param request the state that is being requested.
	\remark
		\ref ALL_UI_STATE_REQUESTS "List of all UI state requests"\n

		Rendering of the blade UI will only start or stop when RGSC_UI_STATE_RESPONSE() is called in response.\n
		This allows javascripts to perform animations before rendering stops, or do any preparations necessary before rendering begins.\n\n
		Types of requests:\n
			\arg \c Visible \n true or false. Requests the visibility state.\n For example, if the UI is hidden and Visible is set to true in the
					  state request, then we're asking if the UI can be made visible. Do any necessary preparations, and then call
					  RGSC_UI_STATE_RESPONSE() with the Visible flag set to true in the state response.\n
					  Similarly, if Visible is set to false, we're asking to hide the UI. Any animations can be done at this point to
					  animate hiding the UI, then call RGSC_UI_STATE_RESPONSE() with the Visible flag set to false in the state response.\n\n
			\arg \c State \n this is a request to go to a certain UI element.\n\n
			\arg \c SubState \n this is a request to go to a specific UI element of a larger system.\n\n
			\arg \c Data \n Contains extended data specific to the state.\n\n

		The following request is sent to hide the UI. Recall that the UI will not stop rendering until Javascript responds by calling RGSC_UI_STATE_RESPONSE().
	\verbatim
	{
		"Visible" : false
	}
	\endverbatim
	\ref ALL_UI_STATE_REQUESTS "List of all UI state requests"\n
	\see RGSC_UI_STATE_RESPONSE()

	\dot
	digraph StateRequestResponseProtocol {
		label = "Solicited State Request/Response Protocol";
		graph [truecolor bgcolor="#00000000"]
		node [shape=record, fontname=Arial, fontsize=10];
		rankdir=LR;
		subgraph cluster0 {
			label = "C++";
			StateRequested [shape=ellipse, label="The player or programmer\nwants to open or\nclose the UI"]
			RGSC_UI_STATE_RESPONSE [URL="\ref RGSC_UI_STATE_RESPONSE()"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "Javascript";
			RGSC_JS_REQUEST_UI_STATE [color=red, URL="\ref RGSC_JS_REQUEST_UI_STATE()"];
			color=lightgrey;
		}

		StateRequested -> RGSC_JS_REQUEST_UI_STATE -> RGSC_UI_STATE_RESPONSE;
	}
	\enddot

	\dot
	digraph Example {
		label = "Example flow: User wants to open the Sign-In/Sign-Up UI";
		graph [truecolor bgcolor="#00000000"]
		node [shape=record, fontname=Arial, fontsize=10];
		rankdir=LR;
		subgraph cluster0 {
			label = "C++";
			Closed [shape=ellipse, label="UI is closed"];
			StateRequested [shape=ellipse, label="The player wants\nto open the\nSign-In/Sign-Up UI"];
			RGSC_UI_STATE_RESPONSE [URL="\ref RGSC_UI_STATE_RESPONSE()"];
			StartsRendering [shape=ellipse, label="DLL starts\nrendering the UI"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "Javascript";
			RGSC_JS_REQUEST_UI_STATE [color=red, URL="\ref RGSC_JS_REQUEST_UI_STATE()"];
			JSPrepares [shape=ellipse, label="Javascript prepares\nto open the\nSign-In/Sign-Up UI"];
			JSAnimates [shape=ellipse, label="Javascript animates\nthe opening of the\nSign-In/Sign-Up UI"];
			color=lightgrey;
		}

		Closed -> StateRequested -> RGSC_JS_REQUEST_UI_STATE -> JSPrepares -> RGSC_UI_STATE_RESPONSE -> StartsRendering -> JSAnimates;
	}
	\enddot
*/

//! Called in response to RGSC_REQUEST_UI_STATE().
/**
	\fn void RGSC_JS_UI_STATE_RESPONSE(json response)
	\param response the response to the previous call to RGSC_REQUEST_UI_STATE().
	\remark
	At a minimum, the Visible flag will be set to true or false. True meaning the UI is now being rendered, false meaning the UI is not being rendered.
	Example response:
	\verbatim
	{
		"Visible" : true,
	}
	\endverbatim
	\see RGSC_REQUEST_UI_STATE()

	\dot
	digraph StateRequestResponseProtocol {
		label = "Unsolicited State Request/Response Protocol";
		graph [truecolor bgcolor="#00000000"]
		node [shape=record, fontname=Arial, fontsize=10];
		rankdir=LR;
		subgraph cluster0 {
			label = "C++";
			RGSC_REQUEST_UI_STATE [URL="\ref RGSC_REQUEST_UI_STATE()"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "Javascript";
			StateRequested [shape=ellipse, label="The UI wants\nto open or\nclose itself"]
			RGSC_JS_UI_STATE_RESPONSE [color=red, URL="\ref RGSC_JS_UI_STATE_RESPONSE()"];
			color=lightgrey;
		}

		StateRequested -> RGSC_REQUEST_UI_STATE -> RGSC_JS_UI_STATE_RESPONSE;
	}
	\enddot
*/

//! Notifies Javascript that events have occurred, such as an achievement being awarded.
/**
	\fn void RGSC_JS_NOTIFICATION(json notifications)
	\param notifications a list of notifications that have occurred.
	\remark
	\ref ALL_NOTIFICATIONS "List of notifications"\n
	There can be more than one notification sent at a time.\n
	Example of an 'achievement awarded' notification:
	\verbatim
	{
		"Count" : 1,
		"Notifications" : [
			{
				"Type" : "Achievements",
				"Count" : 1,
				"Achievements" : [
					{
						"Id" : 1,
						"Name" : "Police Academy",
						"Description" : "You completed all cases on the Patrol desk.",
						"HowTo" : "Complete all cases on the Patrol desk.",
						"Type" : 1,
						"Visible" : false,
						"Points" : 15,
						"Achieved" : true,
						"DateAchieved" : 13413414132
					}
				]
			}
		]
	}
	\endverbatim
	\ref ALL_NOTIFICATIONS "List of notifications"\n
*/

//! Notifies Javascript that a presence message has been received.
/**
	\fn void RGSC_JS_PRESENCE_MESSAGE_RECEIVED(json msg)
	\param msg the presence message that was received.
	\remark

	Examples:
	\verbatim
	{
		"ros.clan.invite": {
			"ci": 4411,
			"cn": "Bullworth Alumni",
			"ct": "BUL",
			"rn": "Member",
			"msg": ""
		}
	}
	\endverbatim

	\verbatim
	{
		"ros.clan.left": 4411
	}
	\endverbatim
*/

//! Called when the game has finished processing a previous activation or deactivation action.
/**
	\fn void RGSC_JS_ACTIVATION_RESULT(json result)
	\param result the results of the previous activation or deactivation action.
	\remark
	Example result:
	\verbatim
	{
		"ContentId" : 1234567,
		"Result" : 1,
		"Message" : [StringId]
	}
	\endverbatim

	The same function is used to pass the result of a Steam Account Linking operation:
	\verbatim
	{
		"Result" : 1,
		"Message" : [StringId]
	}
	\endverbatim

	\see RGSC_ACTIVATION_CODE_RESPONSE()
*/

//! Called when the game has finished processing a previous license extension attempt.
/**
	\fn void RGSC_JS_LICENSE_EXPIRATION_RESULT(json result)
	\param result the results of the previous license extension attempt.
	\remark
	Example result:
	\verbatim
	{
		"ContentId" : 1234567,
		"Result" : 1
		"Message" : [StringId]
	}
	\endverbatim
	\see RGSC_LICENSE_EXPIRATION_RESPONSE()
*/

//! Called when the game has finished processing a previous license recovery attempt.
/**
	\fn void RGSC_JS_LICENSE_RECOVERY_RESULT(json result)
	\param result the results of the previous license recovery attempt.
	\remark
	Example result:
	\verbatim
	{
		"ContentId" : 1234567,
		"Result" : 1
		"Message" : [StringId]
	}
	\endverbatim
	\see RGSC_LICENSE_RECOVERY_RESPONSE()
*/

//! Called when the game's patching system wants to update the patching progress.
/**
	\fn void RGSC_JS_PATCHING_SET_PROGRESS(json progress)
	\param progress the progress message and percent complete.
	\remark
	Example progress:
	\verbatim
	{
		"Message" : [StringId],
		"Progress" : 15
	}
	\endverbatim
	Or:
	\verbatim
	{
		"DynamicMessage" : "This is dynamic text.",
		"Progress" : 15
	}
	\endverbatim
	Note that either a StringId will be sent via Message, or a dynamic utf-8 encoded string will be sent via DynamicMessage.

	Progress is the percent complete (0-100).\n
	\see RGSC_PATCHING_ANIMATION_STATE()
	\see RGSC_JS_PATCHING_SET_OPTIONS()
	\see RGSC_JS_PATCHING_FINISHED()
	\see RGSC_PATCHING_DIALOG_RESPONSE()
	\see RGSC_PATCHING_OPTIONS_RESPONSE()
*/

//! Called when the game's patching system wants to display a list of optional updates.
/**
	\fn void RGSC_JS_PATCHING_SET_OPTIONS(json options)
	\param options the list of options to display (see below).
	\remark
	Example options:
	\verbatim
	{
		   "Message" : "This is dynamic text.",
		   "NumOptions" : 3,
		   "Options" : [
				  {
						 "Index" : 1,
						 "Name" : "Option 1",
						 "Optional" : true,
						 "Hidden" : false
				  },
				  {
						 "Index" : 2,
						 "Name" : "Option 2",
						 "Optional" : false,
						 "Hidden" : false
				  },
				  {
						 "Index" : 3,
						 "Name" : "Option 3",
						 "Optional" : true,
						 "Hidden" : true
				  }
		   ]
	}
	\endverbatim
	All options are sent to this function, including optional updates, mandatory updates, and hidden updates.

	\see RGSC_PATCHING_ANIMATION_STATE()
	\see RGSC_JS_PATCHING_SET_PROGRESS()
	\see RGSC_PATCHING_DIALOG_RESPONSE()
	\see RGSC_PATCHING_OPTIONS_RESPONSE()
	\see RGSC_JS_PATCHING_FINISHED()
*/

//! Called when the game's patching system is finished with the UI.
/**
	\fn void RGSC_JS_PATCHING_FINISHED(json result)
	\remarks
		Result is currently an empty string.\n
	\see RGSC_PATCHING_ANIMATION_STATE()
	\see RGSC_JS_PATCHING_SET_PROGRESS()
	\see RGSC_JS_PATCHING_SET_OPTIONS()
	\see RGSC_PATCHING_DIALOG_RESPONSE()
	\see RGSC_PATCHING_OPTIONS_RESPONSE()
*/

//! Called when controller input is received.
/**
	\fn void RGSC_JS_HANDLE_CONTROLLER_INPUT(json input)
	\param input the input received (see below).
	\remark
	Example input:
	\verbatim
	{
		   "PressedButtons" : 32768,
		   "ReleasedButtons" : 8,
		   "HeldButtons" : 36864
	}
	\endverbatim
	PressedButtons are the buttons that were just pressed.
	ReleasedButtons are the buttons that were just released.
	HeldButtons are the buttons that are currently held down.

	Buttons are reported as a bit mask consisting of a combination of these values:\n
	L2=0x0001, R2=0x0002, L1=0x0004, R1=0x0008,
	RUP=0x0010, RRIGHT=0x0020, RDOWN=0x0040, RLEFT=0x0080,
	SELECT=0x0100, L3=0x0200, R3=0x0400, START=0x0800,
	LUP=0x1000, LRIGHT=0x2000, LDOWN=0x4000, LLEFT=0x8000.

	The RUP, RRIGHT, RDOWN, RLEFT buttons represent the face buttons on the right side of the controller.
	The LUP, LRIGHT, LDOWN, LLEFT buttons represent the face buttons on the left side of the controller, and usually correspond to the d-pad.
*/

//! Returns the requested player list (example: friend list) in json format.
/**
	\fn void RGSC_JS_PLAYER_LIST_RESPONSE(json response)
	\param response the player list that was requested via RGSC_REQUEST_PLAYER_LIST().
	\remark
	Example response:
	\verbatim
	{
		"RequestId" : 5,
		"Success" : true,
		"ResultCode" : 0,
		"PlayerListType" : 1,
		"PageIndex" : 0,
		"PageSize" : 25,
		"Count" : 10,
		"Total" : 100,
		"Players" : [
			{
				"RockstarId" : "28741",
				"DisplayName" : "UndeadGoat",
				"AvatarUrl" : "GTAIV/GTAIV3.png"
				"Relationship" : 32,
				"IsOnline" : true,
				"IsPlayingSameTitle" : true,
				"CanSendGameInvite" : true,
				"CanJoinGame" : true
			},
			{
				"RockstarId" : "63548",
				"DisplayName" : "OneArmedJoe",
				"AvatarUrl" : "RDR/RED_DEAD_1.png"
				"Relationship" : 8
				"IsOnline" : true,
				"IsPlayingSameTitle" : false,
				"CanSendGameInvite" : false,
				"CanJoinGame" : false
			},
			...
		]
	}
	\endverbatim
	- RequestId - if a RequestId (integer) is passed to RGSC_REQUEST_PLAYER_LIST(),
				the same RequestId will be returned in the response. This is to allow the
				UI to identify which response belongs to which request.\n
	- Success - true if the request completed successfully, false if an error occurred.
	- ResultCode - error code if a failure occurred.
	- ListType - This will be the same ListType that was passed to RGSC_REQUEST_PLAYER_LIST().\n
	- PageIndex - The same page index that was passed to RGSC_REQUEST_PLAYER_LIST().\n
	- PageSize - The same page size that was passed to RGSC_REQUEST_PLAYER_LIST().\n
	- Count - The actual number of players returned for the requested page. This will always equal pageSize
			unless the pageIndex is beyond the result set, or the final page contains fewer than pageSize friends. 
	- Total - the total number of players available to retrieve for the given list type.
	- Players - an array of size Count. Each element is a player on the requested list.\n
				Possible relationship types:\n
				RL_RELATIONSHIP_NONE = 0x01,				// They are neither friends, inviting, nor blocking each other.\n
				RL_RELATIONSHIP_BLOCKED_BY_ME = 0x02,		// I am blocking them\n
				RL_RELATIONSHIP_BLOCKED_BY_THEM = 0x04,		// They are blocking me\n
				RL_RELATIONSHIP_BLOCKED_BY_BOTH = 0x06,		// Both sides are blocking the other\n
				RL_RELATIONSHIP_INVITED_BY_ME = 0x08,		// I am inviting them\n
				RL_RELATIONSHIP_INVITED_BY_THEM = 0x10,		// They are inviting me\n
				RL_RELATIONSHIP_FRIEND = 0x20,				// We are friends\n\n

	- IsOnline - true if the player is online, false if the player is offline\n
	- IsPlayingSameTitle - true if the player currently playing the same title as the local player\n
	- CanSendGameInvite - true of the local player is in a multiplayer match and can send a game invite to the player via RGSC_CAN_SEND_GAME_INVITE()\n
	- CanJoinGame - true of the player is in a multiplayer match that the local player can join via RGSC_JOIN_VIA_PRESENCE()\n

	\see RGSC_REQUEST_PLAYER_LIST()
	\see RGSC_JOIN_VIA_PRESENCE()
	\see RGSC_CAN_SEND_GAME_INVITE()
*/

//! Returns the requested player list counts in json format.
/**
	\fn void RGSC_JS_PLAYER_LIST_COUNTS_RESPONSE(json response)
	\param response the player list counts that were requested via RGSC_REQUEST_PLAYER_LIST_COUNTS().
	\remark
	Example response:
	\verbatim
	{
		"RequestId" : 6,
		"NumBlocked" : 1000,
		"NumFriends" : 250,
		"NumInvitesReceieved" : 100,
		"NumInvitesSent" : 100,
		"NumGameInvites" : 10
	}
	\endverbatim
	- RequestId - if a RequestId (integer) is passed to RGSC_REQUEST_PLAYER_LIST_COUNTS(),
				the same RequestId will be returned in the response. This is to allow the
				UI to identify which response belongs to which request.\n

	\see RGSC_REQUEST_PLAYER_LIST_COUNTS()
*/

//! Returns the requested player info in json format.
/**
	\fn void RGSC_JS_PLAYER_INFO_RESPONSE(json response)
	\param response info about the player requested via RGSC_REQUEST_PLAYER_INFO().
	\remark
	Example response:
	\verbatim
	{
		"RequestId" : 6,
		"RockstarId" : "28741",
		"DisplayName" : "UndeadGoat",
		"IsOnline" : true,
		"IsPlayingSameTitle" : true
	}
	\endverbatim
	- RequestId - if a RequestId (integer) is passed to RGSC_REQUEST_PLAYER_INFO(),
				the same RequestId will be returned in the response. This is to allow the
				UI to identify which response belongs to which request.\n

	\see RGSC_REQUEST_PLAYER_INFO()
*/

//! Returns multiplayer presence info for the requested list of players in json format.
/**
	\fn void RGSC_JS_PLAYER_MULTIPLAYER_PRESENCE_INFO_RESPONSE(json response)
	\param response info about the player requested via RGSC_REQUEST_PLAYER_MULTIPLAYER_PRESENCE_INFO().
	\remark
	Example response:
	\verbatim
	{
		"RequestId": 5,
		"Success": true,
		"ResultCode": 0,
		"Count": 2,
		"Players": [
			{
				"RockstarId": "28942581238732",
				"CanSendGameInvite": false,
				"CanJoinGame": false,
				"RichPresence" : "Playing video games",
				"InSameSession" : true
			},
			{
				"RockstarId": "138871347913491",
				"CanSendGameInvite": false,
				"CanJoinGame": false,
				"RichPresence" : "Playing video games",
				"InSameSession" : false
			}
		]
	}
	\endverbatim
	- RequestId - if a RequestId (integer) is passed to RGSC_REQUEST_PLAYER_LIST(),
				the same RequestId will be returned in the response. This is to allow the
				UI to identify which response belongs to which request.\n
	- Success - true if the request completed successfully, false if an error occurred.
	- ResultCode - error code if a failure occurred.
	- Count - The actual number of players returned.
	- CanSendGameInvite - true of the local player is in a multiplayer match and can send a game invite to the player via RGSC_CAN_SEND_GAME_INVITE()\n
	- CanJoinGame - true of the player is in a multiplayer match that the local player can join via RGSC_JOIN_VIA_PRESENCE()\n
	\see RGSC_REQUEST_PLAYER_MULTIPLAYER_PRESENCE_INFO()
*/

//! Called in response to RGSC_FRIEND_SEND_INVITE().
/**
	\fn void RGSC_JS_FRIEND_SEND_INVITE_RESULT(json response)
	\param response the result of the asynchronous call to RGSC_FRIEND_SEND_INVITE().
	\remark
	Example response:
	\verbatim
	{
		"RequestId" : 5,
		"Success" : true,
		"ResultCode" : 0,
		"RockstarId" : 123456789
	}
	\endverbatim
	- RequestId - if a RequestId (integer) is passed to RGSC_FRIEND_SEND_INVITE(),
				the same RequestId will be returned in the response. This is to allow the
				UI to identify which response belongs to which request.\n
	- Success - true if the request completed successfully, false if an error occurred.
	- ResultCode - error code if a failure occurred.\n\n

	Note: the user can also send an invite through the UI via a call to RGSC_JS_REQUEST_UI_STATE() with:
	\verbatim
	{
		"Visible" : true,
		"State" : "Friends",
		"SubState" : "SendInvite",
		"Data" : {
			"RockstarId" : "284071"
		}
	}
	\endverbatim
*/

//! Called in response to RGSC_FRIEND_CANCEL_INVITE().
/**
	\fn void RGSC_JS_FRIEND_CANCEL_INVITE_RESULT(json response)
	\param response the result of the asynchronous call to RGSC_FRIEND_CANCEL_INVITE().
	\remark
	Example response:
	\verbatim
	{
		"RequestId" : 5,
		"Success" : true,
		"ResultCode" : 0,
		"RockstarId" : 123456789
	}
	\endverbatim
	- RequestId - if a RequestId (integer) is passed to RGSC_FRIEND_CANCEL_INVITE(),
				the same RequestId will be returned in the response. This is to allow the
				UI to identify which response belongs to which request.\n
	- Success - true if the request completed successfully, false if an error occurred.
	- ResultCode - error code if a failure occurred.
*/

//! Called in response to RGSC_FRIEND_DECLINE_INVITE().
/**
	\fn void RGSC_JS_FRIEND_DECLINE_INVITE_RESULT(json response)
	\param response the result of the asynchronous call to RGSC_FRIEND_DECLINE_INVITE().
	\remark
	Example response:
	\verbatim
	{
		"RequestId" : 5,
		"Success" : true,
		"ResultCode" : 0,
		"RockstarId" : 123456789
	}
	\endverbatim
	- RequestId - if a RequestId (integer) is passed to RGSC_FRIEND_DECLINE_INVITE(),
				the same RequestId will be returned in the response. This is to allow the
				UI to identify which response belongs to which request.\n
	- Success - true if the request completed successfully, false if an error occurred.
	- ResultCode - error code if a failure occurred.
*/

//! Called in response to RGSC_FRIEND_ACCEPT_INVITE().
/**
	\fn void RGSC_JS_FRIEND_ACCEPT_INVITE_RESULT(json response)
	\param response the result of the asynchronous call to RGSC_FRIEND_ACCEPT_INVITE().
	\remark
	Example response:
	\verbatim
	{
		"RequestId" : 5,
		"Success" : true,
		"ResultCode" : 0,
		"RockstarId" : 123456789
	}
	\endverbatim
	- RequestId - if a RequestId (integer) is passed to RGSC_FRIEND_ACCEPT_INVITE(),
				the same RequestId will be returned in the response. This is to allow the
				UI to identify which response belongs to which request.\n
	- Success - true if the request completed successfully, false if an error occurred.
	- ResultCode - error code if a failure occurred.
*/

//! Called in response to RGSC_FRIEND_DELETE_FRIEND().
/**
	\fn void RGSC_JS_FRIEND_DELETE_FRIEND_RESULT(json response)
	\param response the result of the asynchronous call to RGSC_FRIEND_DELETE_FRIEND().
	\remark
	Example response:
	\verbatim
	{
		"RequestId" : 5,
		"Success" : true,
		"ResultCode" : 0,
		"RockstarId" : 123456789
	}
	\endverbatim
	- RequestId - if a RequestId (integer) is passed to RGSC_FRIEND_DELETE_FRIEND(),
				the same RequestId will be returned in the response. This is to allow the
				UI to identify which response belongs to which request.\n
	- Success - true if the request completed successfully, false if an error occurred.
	- ResultCode - error code if a failure occurred.
*/

//! Called in response to RGSC_FRIEND_BLOCK_PLAYER().
/**
	\fn void RGSC_JS_FRIEND_BLOCK_PLAYER_RESULT(json response)
	\param response the result of the asynchronous call to RGSC_FRIEND_BLOCK_PLAYER().
	\remark
	Example response:
	\verbatim
	{
		"RequestId" : 5,
		"Success" : true,
		"ResultCode" : 0,
		"RockstarId" : 123456789
	}
	\endverbatim
	- RequestId - if a RequestId (integer) is passed to RGSC_FRIEND_BLOCK_PLAYER(),
				the same RequestId will be returned in the response. This is to allow the
				UI to identify which response belongs to which request.\n
	- Success - true if the request completed successfully, false if an error occurred.
	- ResultCode - error code if a failure occurred.
*/

//! Called in response to RGSC_FRIEND_UNBLOCK_PLAYER().
/**
	\fn void RGSC_JS_FRIEND_UNBLOCK_PLAYER_RESULT(json response)
	\param response the result of the asynchronous call to RGSC_FRIEND_UNBLOCK_PLAYER().
	\remark
	Example response:
	\verbatim
	{
		"RequestId" : 5,
		"Success" : true,
		"ResultCode" : 0,
		"RockstarId" : 123456789
	}
	\endverbatim
	- RequestId - if a RequestId (integer) is passed to RGSC_FRIEND_UNBLOCK_PLAYER(),
				the same RequestId will be returned in the response. This is to allow the
				UI to identify which response belongs to which request.\n
	- Success - true if the request completed successfully, false if an error occurred.
	- ResultCode - error code if a failure occurred.
*/

//! Called in response to RGSC_JOIN_VIA_PRESENCE().
/**
	\fn void RGSC_JS_JOIN_VIA_PRESENCE_RESULT(json response)
	\param response the result of the asynchronous call to RGSC_JOIN_VIA_PRESENCE().
	\remark
	Example response:
	\verbatim
	{
		"RequestId" : 5,
		"RockstarId" : 12345689,
		"Success" : true,
		"ResultCode" : 0,
	}
	\endverbatim
	- RequestId - if a RequestId (integer) is passed to RGSC_JOIN_VIA_PRESENCE(),
				the same RequestId will be returned in the response. This is to allow the
				UI to identify which response belongs to which request.\n
	- Success - true if the request completed successfully, false if an error occurred.
	- ResultCode - error code if a failure occurred.
*/

//! Called in response to RGSC_GAME_INVITE_SEND().
/**
	\fn void RGSC_JS_GAME_INVITE_SEND_RESULT(json response)
	\param response the result of the asynchronous call to RGSC_GAME_INVITE_SEND().
	\remark
	Example response:
	\verbatim
	{
		"RequestId" : 5,
		"RockstarId" : "272617",
		"Success" : true,
		"ResultCode" : 0,
	}
	\endverbatim
	- RequestId - if a RequestId (integer) is passed to RGSC_JOIN_VIA_PRESENCE(),
				the same RequestId will be returned in the response. This is to allow the
				UI to identify which response belongs to which request.\n
	- RockstarId - the same RockstarId that was passed to RGSC_GAME_INVITE_SEND()
	- Success - true if the request completed successfully, false if an error occurred.
	- ResultCode - error code if a failure occurred.
*/

//! Called when the game has finished processing a previous code redemption action.
/**
	\fn void RGSC_JS_REDEMPTION_RESULT(json result)
	\param result the results of the previous redemption action.
	\remark
	Example result:
	\verbatim
	{
		"Result" : 1
		"Message" : [StringId],
		"ContentName" : "Max Payne 3 Multiplayer Map Pack"
	}
	\endverbatim
	- ContentName may be omitted (for example if the redemption was unsuccessful).
	\see RGSC_REDEMPTION_CODE_RESPONSE()
*/

//! Called when the a request for a checkout url has been completed.
/**
\fn void RGSC_JS_CHECKOUT_URL_REQUEST_RESULT(json result)
	\param returns the results of the previous checkout url request.
	\remark
	Example result:
	\verbatim
	{
	"result" : true,
	"url" : "https://rockstar.uat.pok.fatfoogoo.com/store/rockstar.com/gta5/v1/en?hash=d309d946ad6eba1847c1a4bdabd50caa22d60ad0&referenceId=ShDxLoQhMDuYRZinWEfgnCgyBACXk%2b2CX6kSiskX86QMH8C9GmVNfkrqX9XGJ57F&locale=en&priceId=GTA5-PC-SharkCardUSDc8d4eb59-bdfa-483d-a98d-c286eb490aa2"
	}
	\endverbatim
	- url may be omitted (for example if the redemption was unsuccessful).
*/

//! Called when the a request for the contents of a voucher has been completed.
/**
\fn void RGSC_JS_VOUCHER_CONTENT_RESULT(json result)
	\param returns the results of the previous voucher content request.
	\remark
	Example result:
	\verbatim
	{
	  "result": true,
	  "Offers": [
		{
		  "OfferCode": "GTAV-SharkCard-x2",
		  "FriendlyName": "",
		  "Entitlements": [
			{
			  "Count": 1,
			  "FriendlyName": "GTAV-SharkCard"
			},
			{
			  "Count": 2,
			  "FriendlyName": "GTAV-SharkCard"
			}
		  ]
		}
	  ],
	  "EntitlementsGranted": [
		{
		  "Count": 1,
		  "FriendlyName": "GTAV-SharkCard"
		},
		{
		  "Count": 2,
		  "FriendlyName": "GTAV-SharkCard"
		}
	  ]
	}
	\endverbatim
	- Keys other than result may be omitted if the call failed.

	- Offers - An array of the offers granted by this voucher. 
		- FriendlyName - A localised string for presentation to the user if required
		- Entitlements - An array of entitlements granted by this offer

	- EntitlementsGranted - An array of entitlements granted by this voucher

	This callback is a result of a call to RGSC_REQUEST_VOUCHER_CONTENTS
*/

//! Called when the a request for the consumption of a voucher has been completed.
/**
\fn void RGSC_JS_VOUCHER_CONSUME_RESULT(json result)
	\param returns the results of the previous voucher consumption request.
	\remark
	Example result:
	\verbatim
	{
	  "result": true
	}
	\endverbatim

	This callback is a result of a call to RGSC_REQUEST_VOUCHER_CONSUMPTION
*/

//! Called when the user has changed their input method for the SCUI
/**
\fn void RGSC_JS_SET_INPUT_METHOD(json inputMethod)
	\param inputMethod - enum representing the input method (keyboard, controller, etc). See below.
	\param PadInfo - (optional) array of objects describing buttons on the controller. See below.
	\remark
	Example result:
	\verbatim
	{
	  "InputMethod" : 0,
	  "PadType" : 0,
	  "PadName" : "Xbox Controller",
	  "PadInfo" : [
		{
			"Button" : 0,
			"Text" : "Left Trigger"
		},
		{
			"Button" : 1,
			"Text" : "Right Trigger"
		}
		...
	  ]
	}
	\endverbatim
	- InputMethod - the new input method
					Possible input methods :\n
					IM_KEYBOARDMOUSE = 0\n
					IM_CONTROLLER = 1\n\n
	- PadType	  - RGSC_CT_UNKNOWN = 0,	// Unknown controller type\n
					RGSC_CT_DINPUT,			// Generic/unknown direct input controller\n
					RGSC_CT_XINPUT,			// Generic/unknown xinput controller\n
					RGSC_CT_XBOX360,		// Xbox 360 controller\n
					RGSC_CT_XBOXONE,		// Xbox One controller\n
					RGSC_CT_PS3,			// PlayStation 3 controller\n
					RGSC_CT_PS4,			// PlayStation 4 controller\n
					RGSC_CT_OCULUS,			// Oculus controller\n
					RGSC_CT_OPENVR,			// OpenVR controller\n\n
	- Button	 -  Possible buttons (Xbox 360 Layout) :\n
					RGSC_L2_INDEX = 0 (Left Trigger)\n
					RGSC_R2_INDEX = 1 (Right Trigger)\n
					RGSC_L1_INDEX = 2 (Left Bumper)\n
					RGSC_R1_INDEX = 3 (Right Bumper)\n
					RGSC_RUP_INDEX = 4 (Y Button)\n
					RGSC_RRIGHT_INDEX = 5 (B Button)\n
					RGSC_RDOWN_INDEX = 6 (A Button)\n
					RGSC_RLEFT_INDEX = 7 (X Button)\n
					RGSC_SELECT_INDEX = 8 (Back/Select)\n
					RGSC_L3_INDEX = 9 (Left Stick - Click)\n
					RGSC_R3_INDEX = 10 (Right Stick - Click)\n
					RGSC_START_INDEX = 11 (Start)\n
					RGSC_LUP_INDEX = 12 (DPad Up)\n
					RGSC_LRIGHT_INDEX = 13 (DPad Right)\n
					RGSC_LDOWN_INDEX = 14 (DPad Down)\n
					RGSC_LLEFT_INDEX = 15 (DPad Left)\n\n
*/

//! Called when the game has requested the SCUI show its on-screen virtual keyboard
/**
\fn void RGSC_JS_SHOW_VIRTUAL_KEYBOARD(json kbParams)
	\param kbParams - json object representing the on-screen keyboard params
	\remark
	Example:
	\verbatim
	{
		"InitialText" : "test"
		"IsPassword" : false
		"MaxNumChars" : 32
	}
	\endverbatim
*/

//! Called when the game has entered virtual keyboard text destined for the SCUI
/**
\fn void RGSC_JS_VIRTUAL_KEYBOARD_RESULT(json kbParams)
	\param kbParams - json object representing the keyboard entry
	\remark
	Example:
	\verbatim
	{
		"Result" : "texttexttext"
	}
	\endverbatim
*/

//! Called when a popup URL is clicked in the SCUI, and we must present a dialog to ask the 
//  user if they want to open the url in a new tab. Respond with RSGC_LAUNCH_EXTERNAL_WEB_BROWSER.
/**
\fn void RGSC_JS_REQUEST_URL(json urlParams)
	\param urlParams - json object respresenting the url to display
	\remark
	Example:
	\verbatim
	{
		"Url" : "http://wwww.rockstargames.com"
	}
	\endverbatim
*/

//! Called when the game has refreshed the steam ticket
/**
\fn void RGSC_JS_REFRESH_STEAM_TICKET_RESULT(json ticket)
	\param ticket - json object representing the updated steam ticket
	\remark
	Example:
	\verbatim
	{
		"Ticket" : "ABCDEF0123456789..."
	}
	\endverbatim
*/

//! Called when the client has received updated Steam information.
/**
\fn void RGSC_JS_STEAM_INFO_CHANGED(json steamInfo)
	\param steamInfo - json object representing steam information (including values that have not changed)
	\remark
	For Steam Auth Ticket changes, listen for RGSC_JS_REFRESH_STEAM_TICKET_RESULT.
	Example:
	\verbatim
	{
		"SteamId" : "1234567890",	// u64 as string
		"SteamAppId" : 112233,		// u32
		"SteamPersona" : "rsjmoore" // string
	}
	\endverbatim
/

//! Returns the requested facebook link info in JSON format
/**
	\fn void RGSC_JS_FACEBOOK_LINK_INFO_RESULT(json response)
	\param response info about the player requested via RGSC_GET_FACEBOOK_LINK_INFO().
	\remark
	Example response:
	\verbatim
	{
		"RequestId" : 6,
		"Success" : true,
		"RockstarId" : "987654321",
		"FbAppId" : "1234567",
		"Permissions" : "publish_actions,publish_stream",
		"IsLinked" : true,
		"LinkUrl" : "https://dev.sc.rockstargames.com/gtavapp/fblink?userdata={url_encoded_ticket}"
	}
	\endverbatim
	- RequestId - if a RequestId (integer) is passed to RGSC_GET_FACEBOOK_LINK_INFO(),
				the same RequestId will be returned in the response. This is to allow the
				UI to identify which response belongs to which request.\n

	\see RGSC_GET_FACEBOOK_LINK_INFO()
*/

//! Sends a ping message to the SCUI, and expects an RGSC_PONG message in return.
/**
	\fn void RGSC_JS_PING()
	\see RGSC_PONG()
*/

//! Event that is fired when windows detects an internet connection state change.
/**
	\fn void RGSC_JS_INTERNET_CONNECTED_STATE_CHANGED()
	\remark
	Example response:
	\verbatim
	{
		"Result" : true
	}
	\endverbatim
		- Result - true if the internet connection is valid
	\see RGSC_INTERNET_CONNECTED_STATE()
*/

/** @} */ // end of C2JS_API group

/*!
\page ALL_UI_STATE_REQUESTS

Landing Page / Main Menu

\verbatim
{
	"Visible" : true,
	"State" : "Main",
}
\endverbatim

\n
<HR>
\n

Profile List / Sign In:
\verbatim
{
	"Visible" : true,
	"State" : "SignIn"
	"Data" : {
		"ProfilesExist" : true
	}
}
\endverbatim
\remark
	If ProfilesExist is true, show the profiles list.\n
	If ProfilesExist is false, show the sign up screen.

\n
<HR>
\n

Friends

	Show friend request UI:
	\verbatim
	{
		"Visible" : true,
		"State" : "Friends",
		"SubState" : "SendInvite",
		"Data" : {
			"RockstarId" : "284071"
		}
	}
	\endverbatim
	\remark
	RockstarId - the id of the player to whom to send the friend request if the user confirms through the UI.
	\see RGSC_FRIEND_SEND_INVITE()

\n
<HR>
\n

Players

	Show the profile of the specified player:
	\verbatim
	{
		"Visible" : true,
		"State" : "Players",
		"SubState" : "Profile",
		"Data" : {
			"RockstarId" : "284071"
		}
	}
	\endverbatim
	\remark
	RockstarId - the id of the player whose profile is to be displayed.

\n
<HR>
\n

Commerce

	Show the commerce/marketplace/store:
	\verbatim
	{
		"Visible" : true,
		"State" : "Commerce",
		"Data" : {
			"Url" : <url of commerce page>
		}
	}
	\endverbatim
	\remarks
		if no URL is specified, this tells the SCUI to bring up a 'waiting...' panel with 
		a spinner until a second call to this function sends it the real URL. This is
		because it can take awhile to get the URL from our commerce provider.

\n
<HR>
\n

Steam account linking

	Show the Steam account linking UI.
	\verbatim
	{
		"Visible" : true,
		"State" : "Activation",
		"SubState" : "SteamLink",
		"Data" : {
			"Message" : [StringId],
		}
	}
	\endverbatim

\n
<HR>
\n

Activation
	\see RGSC_ACTIVATION_CODE_RESPONSE()
	\see RGSC_CORRUPT_LICENSE_DIALOG_RESPONSE()
	\see RGSC_LICENSE_RECOVERY_RESPONSE()
	\see RGSC_LICENSE_EXPIRATION_RESPONSE()

\n
<HR>
\n

Patching
	\see RGSC_PATCHING_DIALOG_RESPONSE()
	\see RGSC_PATCHING_OPTIONS_RESPONSE()
	\see RGSC_PATCHING_PROGRESS_RESPONSE()
*/

/*!
\page ALL_NOTIFICATIONS

Achievement Awarded

\verbatim
{
	"Count" : 1,
	"Notifications" : [
		{
			"Type" : "Achievements",
			"Count" : 1,
			"Data" : [
				{
					"Id" : 1,
					"Name" : "Police Academy",
					"Description" : "You completed all cases on the Patrol desk.",
					"HowTo" : "Complete all cases on the Patrol desk.",
					"Type" : 1,
					"Visible" : false,
					"Points" : 15,
					"Achieved" : true,
					"DateAchieved" : 13413414132
				}
			]
		}
	]
}
\endverbatim

\see RGSC_GET_ACHIEVEMENT_LIST()
\see RGSC_GET_ACHIEVEMENT_IMAGE()

\n
<HR>
\n

Game Invitation Received

\verbatim
{
	"Count" : 1,
	"Notifications" : [
		{
			"Type" : "GameInvites",
			"Count" : 1,
			"Data" : [
				{
					"GameInviteId" : 1,
					"RockstarId" : "272617",
					"Nickname" : "Pestilence",
					"AvatarUrl" : "GTAIV/GTAIV3.png",
					"ReceivedTime" : "1143413414",
					"Salutation" : "Come play my game I'll test ya"
				}
			]
		}
	]
}
\endverbatim
\see RGSC_GET_GAME_INVITE_LIST()
\see RGSC_GAME_INVITE_DECLINED()
\see RGSC_GAME_INVITE_ACCEPTED()

\n
<HR>
\n

Friend Status Changed (Came Online/Went Offline)

\verbatim
{
	"Count" : 1,
	"Notifications" : [
		{
			"Type" : "FriendStatus",
			"Count" : 1,
			"Data" : [
				{
					"RockstarId" : "272617",
					"Nickname" : "Pestilence",
					"AvatarUrl" : "GTAIV/GTAIV3.png",
					"IsOnline" : true,
					"IsPlayingSameTitle" : true
				}
			]
		}
	]
}
\endverbatim

\n
<HR>
\n

Friend Request Received

\verbatim
{
	"Count" : 1,
	"Notifications" : [
		{
			"Type" : "FriendRequestReceived",
			"Count" : 1,
			"Data" : [
				{
					"RockstarId" : "272617",
					"Nickname" : "Pestilence",
					"AvatarUrl" : "GTAIV/GTAIV3.png",
				}
			]
		}
	]
}
\endverbatim

\n
<HR>
\n

Friend Request Accepted

\verbatim
{
	"Count" : 1,
	"Notifications" : [
		{
			"Type" : "FriendRequestAccepted",
			"Count" : 1,
			"Data" : [
				{
					"RockstarId" : "272617",
					"Nickname" : "Pestilence",
					"AvatarUrl" : "GTAIV/GTAIV3.png",
				}
			]
		}
	]
}
\endverbatim

\n
<HR>
\n

Message Received

\verbatim
{
	"Count" : 1,
	"Notifications" : [
		{
			"Type" : "MessageReceived",
			"Count" : 1,
			"Data" : [
				{
					"RockstarId" : "272617",
					"Nickname" : "Pestilence",
					"AvatarUrl" : "GTAIV/GTAIV3.png",
					"IsOnline" : true,
					"IsPlayingSameTitle" : true
					"Message" : "Remember that girl from last night? Maaan she was so wasted."
				}
			]
		}
	]
}
\endverbatim

\n
<HR>
\n

Number of Friends Online

When signing in, RGSC_JS_FINISH_SIGN_IN() returns information about
the number of friends online and the number of friends who are
playing the same title as the local player.

/see RGSC_JS_FINISH_SIGN_IN()

\n
<HR>
\n

Download Status

The download manager will send a notification when it has completed downloading content/DLC.

\verbatim
{
	"Count" : 1,
	"Notifications" : [
		{
			"Type" : "DownloadStatus",
			"Count" : 1,
			"Data" : [
				{
					"Name" : "Maximum Pain Weapon Pack",
					"Message" : [StringId]
				}
			]
		}
	]
}
\endverbatim
Note: games may pass a name field or the name may be omitted or an empty string.

\n
<HR>
\n

Welcome message

This should show information on how to bring up the Social Club UI.

\verbatim
{
        "Count" : 1,
        "Notifications" : [
                {
                        "Type" : "Welcome",
                        "Data" : [
                                {
                                        "Message" : [StringId],
                                        "NumControllers" : [Number of Plugged in Controllers]
                                }
                        ]
                }
        ]
}
\endverbatim
*/

// NOTE: We now only support calling JS functions that take at most 1 parameter, and it must be a string.
// JS_REGISTER(JavascriptName, C++Name, Parameters)

#define JS_COMMANDS																											\
																															\
JS_REGISTER(RGSC_JS_SIGN_IN, JsSignIn, json);																				\
JS_REGISTER(RGSC_JS_SIGN_OUT, JsSignOut, json);																				\
JS_REGISTER(RGSC_JS_FINISH_SIGN_IN, JsFinishSignIn, json);																	\
JS_REGISTER(RGSC_JS_FINISH_MODIFY_PROFILE, JsFinishModifyProfile, json);													\
JS_REGISTER(RGSC_JS_NOTIFICATION, JsSendNotification, json);																\
JS_REGISTER(RGSC_JS_PRESENCE_MESSAGE_RECEIVED, JsPresenceMessageReceived, json);											\
JS_REGISTER(RGSC_JS_REQUEST_UI_STATE, JsRequestUiState, json);																\
JS_REGISTER(RGSC_JS_UI_STATE_RESPONSE, JsUiStateResponse, json);															\
JS_REGISTER(RGSC_JS_ACTIVATION_RESULT, JsActivationResult, json);															\
JS_REGISTER(RGSC_JS_LICENSE_EXPIRATION_RESULT, JsLicenseExpirationResult, json);											\
JS_REGISTER(RGSC_JS_LICENSE_RECOVERY_RESULT, JsLicenseRecoveryResult, json);												\
JS_REGISTER(RGSC_JS_PATCHING_SET_PROGRESS, JsPatchingSetProgress, json);													\
JS_REGISTER(RGSC_JS_PATCHING_SET_OPTIONS, JsPatchingSetOptions, json);														\
JS_REGISTER(RGSC_JS_PATCHING_FINISHED, JsFinishedPatching, json);															\
JS_REGISTER(RGSC_JS_READY_TO_ACCEPT_COMMANDS, JsReadyToAcceptCommands);														\
JS_REGISTER(RGSC_JS_HANDLE_CONTROLLER_INPUT, JsHandleControllerInput, json);												\
JS_REGISTER(RGSC_JS_PLAYER_LIST_RESPONSE, JsPlayerListResponse, json);														\
JS_REGISTER(RGSC_JS_PLAYER_LIST_COUNTS_RESPONSE, JsPlayerListCountsResponse, json);											\
JS_REGISTER(RGSC_JS_PLAYER_INFO_RESPONSE, JsPlayerInfoResponse, json);														\
JS_REGISTER(RGSC_JS_PLAYER_MULTIPLAYER_PRESENCE_INFO_RESPONSE, JsPlayerMultiplayerPresenceInfoResponse, json);				\
JS_REGISTER(RGSC_JS_FRIEND_SEND_INVITE_RESULT, JsFriendSendInviteResult, json);												\
JS_REGISTER(RGSC_JS_FRIEND_CANCEL_INVITE_RESULT, JsFriendCancelInviteResult, json);											\
JS_REGISTER(RGSC_JS_FRIEND_DECLINE_INVITE_RESULT, JsFriendDeclineInviteResult, json);										\
JS_REGISTER(RGSC_JS_FRIEND_ACCEPT_INVITE_RESULT, JsFriendAcceptInviteResult, json);											\
JS_REGISTER(RGSC_JS_FRIEND_DELETE_FRIEND_RESULT, JsFriendDeleteFriendResult, json);											\
JS_REGISTER(RGSC_JS_FRIEND_BLOCK_PLAYER_RESULT, JsFriendBlockPlayerResult, json);											\
JS_REGISTER(RGSC_JS_FRIEND_UNBLOCK_PLAYER_RESULT, JsFriendUnblockPlayerResult, json);										\
JS_REGISTER(RGSC_JS_JOIN_VIA_PRESENCE_RESULT, JsJoinViaPresenceResult, json);												\
JS_REGISTER(RGSC_JS_GAME_INVITE_SEND_RESULT, JsGameInviteSendResult, json);													\
JS_REGISTER(RGSC_JS_REDEMPTION_RESULT, JsRedemptionResult, json);															\
JS_REGISTER(RGSC_JS_RECEIVE_MESSAGE, JsReceiveMessage, json);				  												\
JS_REGISTER(RGSC_JS_VOUCHER_CONTENT_RESULT, JsVoucherContentResult, json);													\
JS_REGISTER(RGSC_JS_VOUCHER_CONSUME_RESULT, JsVoucherConsumeResult, json);													\
JS_REGISTER(RGSC_JS_CHECKOUT_URL_REQUEST_RESULT, JsUrlRequestRequestResult, json);											\
JS_REGISTER(RGSC_JS_COMMERCE_MANIFEST_REQUEST_RESULT, JsCommerceManifestRequestResult, json);                               \
JS_REGISTER(RGSC_JS_SET_INPUT_METHOD, JsSetInputMethod, json);																\
JS_REGISTER(RGSC_JS_SHOW_VIRTUAL_KEYBOARD, JsShowVirtualKeyboard, json);													\
JS_REGISTER(RGSC_JS_VIRTUAL_KEYBOARD_RESULT, JsVirtualKeyboardResult, json);												\
JS_REGISTER(RGSC_JS_REFRESH_STEAM_TICKET_RESULT, JsRefreshSteamTicketResult, json);											\
JS_REGISTER(RGSC_JS_STEAM_INFO_CHANGED, JsSteamInfoChanged, json);															\
JS_REGISTER(RGSC_JS_FACEBOOK_LINK_INFO_RESULT, JsFacebookLinkInfoResult, json);												\
JS_REGISTER_SILENT(RGSC_JS_PING, JsPing, json);																				\
JS_REGISTER(RGSC_JS_INTERNET_CONNECTED_STATE_CHANGED, JsInternetConnectedStateChanged, json);								\
JS_REGISTER(RGSC_JS_IME_EVENT, JsImeEvent, json);																			\
JS_REGISTER(RGSC_JS_SHUTDOWN, JsShutdown, json);																			\
JS_REGISTER(RGSC_JS_WINDOW_STATE_CHANGED, JsWindowStateChanged, json);														\
JS_REGISTER(RGSC_JS_REQUEST_URL, JsRequestUrl, json);																		\

#ifdef DOXYGEN_PREPROCESSOR
typedef const char* json;
// doxygen has to see the function signature or it won't document it. Generate the function signature here.
#define JS_REGISTER(JsName, Name, ...) void JsName(__VA_ARGS__)
JS_COMMANDS
#endif

// END

} // namespace rgsc

#endif //C2JSCOMMANDS
