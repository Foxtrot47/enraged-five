//
// input/eventq.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "eventq.h"
#pragma warning(push)
#pragma warning(disable: 4668)
#include <windows.h>
#include <stdio.h>
#pragma warning(pop)

namespace rgsc
{

int ioEventQueue::sm_Head;
int ioEventQueue::sm_Tail;
ioEvent ioEventQueue::sm_TheQueue[MAX_EVENTS];

#define DEBUG_EVENTS 0

// TODO: NS - need to make this a lock-free or non-blocking queue
// since this is a multithreaded producer/consumer problem
// (render thread produces events, main thread consumes events)

bool ioEventQueue::Pop(ioEvent &dest)
{
	if(sm_Head != sm_Tail)
	{
		sm_Tail = (sm_Tail + 1) & (MAX_EVENTS-1);
		dest = sm_TheQueue[sm_Tail];
		return true;
	}

	return false;
}

bool ioEventQueue::Peek(ioEvent &dest, int &offset)
{
	int tail = (sm_Tail + offset) & (MAX_EVENTS-1);
	if(sm_Head != tail)
	{
		dest = sm_TheQueue[tail];
		++offset;
		return true;
	}

	return false;
}

void ioEventQueue::Queue(ioEvent::ioEventType type, __int64 x, __int64 y, __int64 data, __int64 modifiers)
{
	// Merge redundant events together if possible
	if(sm_Head != sm_Tail && (type == ioEvent::IO_MOUSE_MOVE) && sm_TheQueue[sm_Head].m_Type == type) 
	{
		sm_TheQueue[sm_Head].m_X = x;
		sm_TheQueue[sm_Head].m_Y = y;
		return;
	}

	// Check for overflow.
	int newHead = (sm_Head + 1) & MAX_EVENTS - 1;

	if (newHead == sm_Tail)
	{
		Warningf("Event Queue Overflow");
		return;
	}

	sm_TheQueue[newHead].m_Type = type;
	sm_TheQueue[newHead].m_X = x;
	sm_TheQueue[newHead].m_Y = y;
	sm_TheQueue[newHead].m_Data = data;
	sm_TheQueue[newHead].m_Modifiers = modifiers;
	sm_Head = newHead;
}

} // namespace rgsc