#ifndef _RGSC_STRING_UTIL_H_
#define _RGSC_STRING_UTIL_H_

#include "weakstring.h"

namespace rgsc {

WideString UTF8ToWide(const UTF8String &in);
UTF8String WideToUTF8(const WideString &in);

void stringUtil_free(WideString returnedValue);
void stringUtil_free(UTF8String returnedValue);

}

typedef rgsc::WideString RgscWideString;
typedef rgsc::UTF8String RgscUrlString;
typedef rgsc::UTF8String RgscUtf8String;

#endif // _RGSC_STRING_UTIL_H_
