#ifndef RGSC_TILED_BUFFER_H
#define RGSC_TILED_BUFFER_H

namespace ipcbase
{
	class SharedMemory;
}

namespace rgsc
{

struct TileRect
{
	unsigned int m_Left;
	unsigned int m_Right;
	unsigned int m_Top;
	unsigned int m_Bottom;
	bool IsIntersecting(const TileRect& rect) const;

};

class Tile
{
public:
	Tile();
	~Tile();

	void Init(const TileRect& rect, unsigned char* buffer, unsigned short* currentFrame);

	void setDirty();
	bool isDirty(unsigned short& dirtyFrame);
	void setLastUpdatedFrame(unsigned short frame);

	void setVisible(bool visible) {m_Visible = visible;}
	bool isVisible() {return m_Visible;}

	void clear();

	unsigned char* getBuffer() {return m_Buffer;}
	unsigned int getWidth() {return m_Width;}
	unsigned int getHeight() {return m_Height;}

	unsigned int getEffectiveHeight() {return m_EffectiveHeight;}
	unsigned int getRowSizeInBytes() {return m_RowSize;}

	unsigned int getLeft() {return m_Rect.m_Left;}
	unsigned int getRight() {return m_Rect.m_Right;}
	unsigned int getTop() {return m_Rect.m_Top;}
	unsigned int getBottom() {return m_Rect.m_Bottom;}

	const TileRect& getRect() {return m_Rect;}

private:
	unsigned char* m_Buffer;
	TileRect m_Rect;
	unsigned int m_RowSize;
	unsigned int m_EffectiveHeight;
	unsigned int m_Width;
	unsigned int m_Height;
	unsigned short* m_CurrentFrame;
	unsigned short m_LastFrame;

	bool m_Dirty;
	bool m_Visible;
};

class TiledBuffer
{
public:
	TiledBuffer(unsigned width, unsigned height);
	~TiledBuffer();

	void clear();
	unsigned getDirtyTileBufferSize();
	unsigned char* getBuffer();
	unsigned int getBufferSize();

	const char* getSharedMemoryName() const {return m_SharedMemoryName;}

	unsigned int getNumTiles();
	unsigned int getNumRows() {return m_NumRows;}
	unsigned int getNumCols() {return m_NumCols;}
	unsigned int getRowPitch() {return m_Width * 4;} // 4 bytes per pixel
	unsigned int getColPitch() {return m_Height * 4;} // 4 bytes per pixel
	unsigned int getWidth() { return m_Width; }
	unsigned int getHeight() { return m_Height; }

	Tile* getTile(unsigned int index);
	Tile* getTile(unsigned int row, unsigned int col);

private:
	void CalculateTileSize(unsigned int width, unsigned int height, unsigned int &bestTileWidth, unsigned int &bestTileHeight);
	void CreateTiles();

	static const unsigned MAX_SHARED_MEMORY_NAME_LEN = 64;
	ipcbase::SharedMemory* m_SharedMemory;
	char m_SharedMemoryName[MAX_SHARED_MEMORY_NAME_LEN + 1];

	unsigned char* m_Buffer;
	unsigned int m_Width;
	unsigned int m_Height;
	unsigned int m_NumTiles;
	unsigned int m_NumRows;
	unsigned int m_NumCols;
	Tile* m_Tiles;
};

} // namespace rgsc

#endif // RGSC_TILED_BUFFER_H
