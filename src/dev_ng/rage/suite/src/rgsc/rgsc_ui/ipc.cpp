// 
// ipc.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "ipc.h"
#pragma warning(push)
#pragma warning(disable: 4310)
#include "base/at_exit.h"
#include "base/thread.h"
#include "base/waitable_event.h"
#include "ipc/ipc_descriptors.h"
#include "ipc/ipc_channel.h"
#include "ipc/ipc_channel_proxy.h"
#include "ipc/ipc_message_utils.h"
#include "ipc/ipc_switches.h"
#include "ipc/ipc_sync_channel.h"
#pragma warning(pop)

namespace rgsc
{

static ipcbase::AtExitManager m_AtExitManager;

class ChannelListener : public IPC::Channel::Listener
{
public:

	virtual void OnMessageReceived(const IPC::Message& message)
	{
		m_Ipc->DispatchEvent(message);
	}

	virtual void OnChannelError()
	{
		Displayf("IPC - OnChannelError()");
		m_Ipc->DeleteChannel(m_ChannelName);
	}

	virtual void OnChannelConnected(int32 remoteProcessId)
	{
		Displayf("IPC - OnChannelConnected - remoteProcessId %d", remoteProcessId);
	}

private:
	// only rgsc::Ipc can create an rgsc::ChannelListener
	friend class Ipc;
	ChannelListener(Ipc* ipc, const char* channelName)
	{
		strcpy(m_ChannelName, channelName);
		m_Ipc = ipc;
	}

	~ChannelListener()
	{

	}

	char m_ChannelName[Ipc::MAX_CHANNEL_NAME_LEN];
	Ipc* m_Ipc;
};

Ipc::Ipc()
: m_MessageLoop(NULL)
, m_NumChannels(0)
, m_NextChannelId(0)
{
	for(unsigned i = 0; i < MAX_IPC_CHANNELS; ++i)
	{
		m_Channels[i] = NULL;
		m_ShutdownEvent[i] = NULL;
		m_Thread[i] = NULL;
		m_ChannelListener[i] = NULL;
		m_ChannelName[i][0] = '\0';
	}
}

Ipc::~Ipc()
{
	Shutdown();
}

bool Ipc::Init(Mode mode, const char* ipcChannelName)
{
	bool success = false;

	m_MessageLoop = new MessageLoopForIO();

	// The thread needs to out-live the IPC::SyncChannel.

	success = AddChannel(mode, ipcChannelName);

	return success;
}

bool Ipc::AddChannel(Mode mode, const char* ipcChannelName)
{
	// setup IPC channel
	bool createPipeNow = true;

	char tmp[64] = {0};
	sprintf(tmp, "RgscIpc_%u", m_NumChannels);
	m_Thread[m_NumChannels] = new ipcbase::Thread(tmp);
	ipcbase::Thread::Options options;
	options.message_loop_type = MessageLoop::TYPE_IO;
	m_Thread[m_NumChannels]->StartWithOptions(options);
	m_ChannelListener[m_NumChannels] = new ChannelListener(this, ipcChannelName);

	strcpy(m_ChannelName[m_NumChannels], ipcChannelName);

	m_ShutdownEvent[m_NumChannels] = new ipcbase::WaitableEvent(true, false);
	m_Channels[m_NumChannels] = new IPC::SyncChannel(ipcChannelName,
								     mode == MODE_SERVER ?
								     IPC::Channel::MODE_SERVER :
								     IPC::Channel::MODE_CLIENT,
								     m_ChannelListener[m_NumChannels],
								     NULL,
								     m_Thread[m_NumChannels]->message_loop(),
								     createPipeNow,
								     m_ShutdownEvent[m_NumChannels]);

	m_NumChannels++;
	m_NextChannelId++;

	return true;
}

bool Ipc::DeleteChannel(const char* ipcChannelName)
{
	int channelIndex = -1;

	for(unsigned i = 0; i < m_NumChannels; ++i)
	{
		if(_stricmp(ipcChannelName, m_ChannelName[i]) == 0)
		{
			channelIndex = i;
			break;
		}
	}

	if(channelIndex >= 0 && channelIndex < (int)m_NumChannels)
	{
		DeleteChannel(channelIndex);

		m_ChannelName[channelIndex][0] = '\0';

		for(unsigned i = channelIndex; i < (m_NumChannels - 1); i++)
		{
			m_ShutdownEvent[i] = m_ShutdownEvent[i + 1];
			m_Thread[i] = m_Thread[i + 1];
			m_ChannelListener[i] = m_ChannelListener[i + 1];
			m_Channels[i] = m_Channels[i + 1];
			strcpy(m_ChannelName[i], m_ChannelName[i + 1]);

			m_ShutdownEvent[i + 1] = NULL;
			m_Thread[i + 1] = NULL;
			m_ChannelListener[i + 1] = NULL;
			m_Channels[i + 1] = NULL;
			m_ChannelName[i + 1][0] = '\0';
		}

		--m_NumChannels;

		return true;
	}

	return false;
}

void Ipc::DeleteChannel(int channelIndex)
{
	if(m_ShutdownEvent[channelIndex])
	{
		m_ShutdownEvent[channelIndex]->Signal();
	}

	if(m_Channels[channelIndex])
	{
		delete m_Channels[channelIndex];
		m_Channels[channelIndex] = NULL;
	}

	if(m_Thread[channelIndex])
	{
		delete m_Thread[channelIndex];
		m_Thread[channelIndex] = NULL;
	}

	if(m_ChannelListener[channelIndex])
	{
		delete m_ChannelListener[channelIndex];
		m_ChannelListener[channelIndex] = NULL;
	}

	if(m_ShutdownEvent[channelIndex])
	{
		m_ShutdownEvent[channelIndex]->Signal();
		delete m_ShutdownEvent[channelIndex];
		m_ShutdownEvent[channelIndex] = NULL;
	}
}

unsigned Ipc::GetNumChannels()
{
	return m_NumChannels;
}

unsigned Ipc::GetNextChannelId()
{
	return m_NextChannelId;
}

const char* Ipc::GetChannelName()
{
	return m_ChannelName[0];
}

void Ipc::Update()
{
	if (m_MessageLoop)
	{
		m_MessageLoop->RunAllPending();
	}
}

void Ipc::Shutdown()
{
	for(unsigned i = 0; i < m_NumChannels; ++i)
	{
		if(m_ShutdownEvent[i])
		{
			m_ShutdownEvent[i]->Signal();
		}

		if(m_Channels[i])
		{
			m_Channels[i] = NULL;
		}

		if(m_Thread[i])
		{
			delete m_Thread[i];
			m_Thread[i] = NULL;
		}

		if(m_ChannelListener[i])
		{
			delete m_ChannelListener[i];
			m_ChannelListener[i] = NULL;
		}

		if(m_ShutdownEvent[i])
		{
			m_ShutdownEvent[i]->Signal();
			delete m_ShutdownEvent[i];
			m_ShutdownEvent[i] = NULL;
		}
	}

	m_NumChannels = 0;

	if(m_MessageLoop)
	{
		delete m_MessageLoop;
		m_MessageLoop = NULL;
	}
}

void Ipc::AddDelegate(Delegate* dlgt)
{
	m_Delegator.AddDelegate(dlgt);
}

void Ipc::RemoveDelegate(Delegate* dlgt)
{
	m_Delegator.RemoveDelegate(dlgt);
}

void Ipc::DispatchEvent(const IPC::Message& message)
{
	m_Delegator.Dispatch(message);
}

bool Ipc::Send(IPC::Message* message)
{
	return Send(message, m_ChannelName[0]);
}

bool Ipc::Send(IPC::Message* message, const char* channelName)
{	
	int channelIndex = -1;

	for(unsigned i = 0; i < m_NumChannels; ++i)
	{
		if(_stricmp(channelName, m_ChannelName[i]) == 0)
		{
			channelIndex = i;
			break;
		}
	}

	if(channelIndex >= 0 && channelIndex < (int)m_NumChannels)
	{
		return m_Channels[channelIndex] && m_Channels[channelIndex]->Send(message);
	}

	return false;
}

} // namespace rgsc
