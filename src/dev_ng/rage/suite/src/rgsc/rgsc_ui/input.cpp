#define _WINSOCKAPI_ //Prevent windows.h from including winsock.h
#include "input.h"
#include "cursor.h"
#include "joystick.h"
#include "pad.h"
#include "rgsc_ui.h"
#include "script.h"
#include "rgsc.h"
#include "json.h"
#include "types.h"
#include "../rgsc/diag/output.h"

// rage
#include "diag/seh.h"
#include "net/time.h"

#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "Imm32.lib")

#pragma warning(push)
#pragma warning(disable: 4668)

#include <stdio.h>
#include <tpcshrd.h>
#include <windows.h>
#pragma warning(pop)

namespace rgsc
{

// Windows, Threads, etc
static WindowHandle sm_hDeviceWindow = 0;
sysIpcThreadId Rgsc_Input::sm_ThreadId = sysIpcThreadIdInvalid;
sysCriticalSectionToken Rgsc_Input::sm_Cs;
sysCriticalSectionToken Rgsc_Input::sm_DeviceChangeCs;
bool Rgsc_Input::sm_bInputEnabled = false;
bool Rgsc_Input::sm_bInputSubclassing = false;
bool Rgsc_Input::sm_bIsForegroundWindow = false;
bool Rgsc_Input::sm_bMouseInWindow = false;
bool Rgsc_Input::sm_bInputThisFrame = false;
RECT Rgsc_Input::sm_WindowMargins = {0};
bool Rgsc_Input::sm_bMouseEventForwarding = true;
bool Rgsc_Input::sm_bKeyboardEventForwarding = true;

#if IME_TEXT_INPUT
wchar_t Rgsc_Input::sm_CurrentTextBuffer[TEXT_BUFFER_LENGTH];
u32 Rgsc_Input::sm_CurrentLength = 0;
#endif

// Mouse
HCURSOR Rgsc_Input::m_Cursor;
int Rgsc_Input::s_OldMouseX = 0;
int Rgsc_Input::s_OldMouseY = 0;
unsigned int Rgsc_Input::m_MouseMoveCount = 0;
float Rgsc_Input::sm_TargetX = 0;
float Rgsc_Input::sm_TargetY = 0;

// Controllers
Rgsc_Input::InputState s_AllControllers;
unsigned int s_ControllerIndex[IGamepadManagerLatestVersion::INPUTMODE_MAX] = {0};
bool Rgsc_Input::m_IsUsingController[IGamepadManagerLatestVersion::INPUTMODE_MAX] = {0};
int Rgsc_Input::m_ControllerEventsToConsume = 0;

// Time
rage::netTimeStep m_TimeStep;
unsigned Rgsc_Input::m_ControllerScrollStartTime = 0;
unsigned Rgsc_Input::m_ControllerMouseStartTime = 0;
unsigned Rgsc_Input::sm_uTimeOfLastDeviceChange = 0;

// SCUI closing
static bool sm_bIgnoreRBInputUntilRelease = false;
static bool sm_bCloseButtonWasPressed[IGamepadManagerLatestVersion::INPUTMODE_MAX] = {0};

#if SUPPORT_TOUCH_INPUT
unsigned int Rgsc_Input::m_TouchStartTime = 0;

typedef BOOL (CALLBACK* LPFNGETTOUCHINPUTINFO)(HTOUCHINPUT hTouchInput, UINT cInputs, PTOUCHINPUT pInputs, int cbSize);
typedef BOOL (CALLBACK* LPFNCLOSETOUCHINPUTHANDLE)(HTOUCHINPUT hTouchInput);

static LPFNGETTOUCHINPUTINFO sm_FuncGetTouchInputInfo = NULL;
static LPFNCLOSETOUCHINPUTHANDLE sm_FuncCloseTouchInputHandle = NULL;

// the amount of time in ms to ignore mouse events after handling a WM_TOUCH event
static const unsigned int sm_TimeToIgnoreMouseAfterTouch = 2000;
#endif

#ifndef MIN
#define MIN(x,y) ((x)<(y)?(x):(y))
#endif

#ifndef MAX
#define MAX(x,y) ((x)>(y)?(x):(y))
#endif

////////////////////////////////////////////////////////////
// Global Functionality
////////////////////////////////////////////////////////////
atMap<WindowHandle, RgscWndProcMap> sm_WndProcs;
sysCriticalSectionToken sm_WndProcCs;

WNDPROC FindWndProc(WindowHandle h)
{
	SYS_CS_SYNC(sm_WndProcCs);
	RgscWndProcMap* p = sm_WndProcs.Access(h);
	if (p)
	{
		return p->m_OriginalProc;
	}

	return 0;
}

////////////////////////////////////////////////////////////
// Rgsc_Input
////////////////////////////////////////////////////////////
void Rgsc_Input::Init(WindowHandle hDeviceWindow, const WindowHandle* additionalWindows, unsigned numAdditionalWindows, bool bSubclassWndProc)
{
	sm_hDeviceWindow = hDeviceWindow;

	if (bSubclassWndProc)
	{
		SYS_CS_SYNC(sm_WndProcCs);

		// insert the device window
		WNDPROC deviceProc = (WNDPROC)SetWindowLongPtr((HWND)sm_hDeviceWindow, GWLP_WNDPROC, (LONG_PTR)WindowsMessageHandler);
		sm_WndProcs.Insert(sm_hDeviceWindow, RgscWndProcMap((HWND)sm_hDeviceWindow, deviceProc));

		// iterate through the additional windows
		for(unsigned i = 0; i < numAdditionalWindows; i++)
		{
			WNDPROC wndProc = (WNDPROC)SetWindowLongPtr((HWND)additionalWindows[i], GWLP_WNDPROC, (LONG_PTR)WindowsMessageHandler);
			sm_WndProcs.Insert(additionalWindows[i], RgscWndProcMap((HWND)additionalWindows[i], wndProc));
		}

		sm_bInputSubclassing = true;
	}

	sm_bInputEnabled = true;

#if IME_TEXT_INPUT
	ImeBegin();
#endif

	sm_ThreadId = sysIpcCreateThread(InputThread, NULL, sysIpcMinThreadStackSize, PRIO_NORMAL, "[RGSC Input Thread]");
}

void Rgsc_Input::InputThread(void*)
{
#if RGSC_DINPUT_SUPPORT
	ioJoystick::BeginAll();
#endif // RGSC_DINPUT_SUPPORT

	GetRgscConcreteInstance()->_GetGamepadManager()->BeginAll();

	// install windows message handler
	m_MouseMoveCount = 0;
	m_TimeStep.Init(sysTimer::GetSystemMsTime());

#if SUPPORT_TOUCH_INPUT
	m_TouchStartTime = 0;

	// these functions aren't available on XP so we get an import failure at DLL load time. Explicitly request the function instead.
	sm_FuncGetTouchInputInfo = (LPFNGETTOUCHINPUTINFO)GetProcAddress(GetModuleHandle(TEXT("USER32.dll")), "GetTouchInputInfo");
	sm_FuncCloseTouchInputHandle = (LPFNCLOSETOUCHINPUTHANDLE)GetProcAddress(GetModuleHandle(TEXT("USER32.dll")), "CloseTouchInputHandle");
#endif

	sm_uTimeOfLastDeviceChange = sysTimer::GetSystemMsTime();

	while(sm_bInputEnabled)
	{
		sm_bInputThisFrame = false;

		m_TimeStep.SetTime(sysTimer::GetSystemMsTime());

#if RGSC_DINPUT_SUPPORT
		ioJoystick::PollAll();	
		ioJoystick::UpdateAll();
#endif // RGSC_DINPUT_SUPPORT

		GetRgscConcreteInstance()->_GetGamepadManager()->UpdateAll();

		CheckMouseInWindow();

		// With GamePadManagerV3, the game can specifically opt into background input.
		if (!GetRgscConcreteInstance()->_GetGamepadManager()->IsBackgroundInputEnabled())
		{
			// Only listen to input on the foreground
			HWND fgWindow = ::GetForegroundWindow();
			HWND curWindow = (HWND)sm_hDeviceWindow;
			while(curWindow != NULL && curWindow != fgWindow)
			{
				curWindow = ::GetParent(curWindow);
			}

			sm_bIsForegroundWindow = (fgWindow == curWindow);
			if (!sm_bIsForegroundWindow)
			{
				SetIsUsingController(false);
				sysIpcSleep(16);
				continue;
			}
		}

		// Clear the current held buttons
		s_AllControllers.Clear();

		if (GetRgscConcreteInstance()->GetGamepadSupport() == IConfigurationV6::GAMEPADS_SUPPORTED)
		{
#if RGSC_DINPUT_SUPPORT
			UpdateInternal(IGamepadManagerLatestVersion::INPUTMODE_D3D, RGSC_MAX_PADS);
#endif // RGSC_DINPUT_SUPPORT
#if RGSC_XINPUT_SUPPORT
			UpdateInternal(IGamepadManagerLatestVersion::INPUTMODE_XINPUT, RGSC_MAX_PADS);
#endif // RGSC_XINPUT_SUPPORT
		}
		else if (GetRgscConcreteInstance()->GetGamepadSupport() == IConfigurationV6::GAMEPADS_MARSHALLED)
		{
			if (rlVerifyf(GetRgscConcreteInstance()->_GetGamepadManager()->GetNumMarshalledPads() > 0, "No marshalled pads - SetMarshalledGamepads not configured."))
			{
				UpdateInternal(IGamepadManagerLatestVersion::INPUTMODE_MARSHALLED, GetRgscConcreteInstance()->_GetGamepadManager()->GetNumMarshalledPads());
			}
		}

		// Potentially stalling operation, check 5 seconds after a WM_DEVICECHANGE event is fired.
		static const int TIME_BETWEEN_CONTROLLER_CHECKS = 5000;
		if (sm_uTimeOfLastDeviceChange > 0 && sysTimer::HasElapsedIntervalMs(sm_uTimeOfLastDeviceChange, TIME_BETWEEN_CONTROLLER_CHECKS)) 
		{
			GetRgscConcreteInstance()->_GetGamepadManager()->CheckForNewlyPluggedInDevices();
			{
				SYS_CS_SYNC(sm_DeviceChangeCs);
				sm_uTimeOfLastDeviceChange = 0;
			}
		}

		if (GetRgscConcreteInstance()->_GetUiInterface()->IsVisible())
		{
			sysIpcSleep(sm_bInputThisFrame ? INPUT_UPDATE_RATE_HIGH : INPUT_UPDATE_RATE_LOW);
		}
		else
		{
			sysIpcSleep(INPUT_UPDATE_RATE_NOT_VISIBLE);
		}
	}

#if RGSC_DINPUT_SUPPORT
	ioJoystick::EndAll();
#endif // RGSC_DINPUT_SUPPORT
}

void Rgsc_Input::Shutdown()
{
	sm_bInputEnabled = false;

#if IME_TEXT_INPUT
	ImeEnd();
#endif

	//if (sm_ThreadId != sysIpcThreadIdInvalid)
	//{
	//	sysIpcWaitThreadExit(sm_ThreadId);
	//	sm_ThreadId = sysIpcThreadIdInvalid;
	//}

	SYS_CS_SYNC(sm_WndProcCs);

	// restore old windows message handlers
	for (atMap<WindowHandle, RgscWndProcMap>::Iterator i = sm_WndProcs.CreateIterator(); !i.AtEnd(); i.Next()) 
	{
		RgscWndProcMap& procMap = i.GetData();
		SetWindowLongPtr((HWND)procMap.m_hWnd, GWLP_WNDPROC, (LONG_PTR)procMap.m_OriginalProc);
	}

	sm_WndProcs.Kill();

	sm_bInputSubclassing = false;
}

void Rgsc_Input::Update()
{

}

void Rgsc_Input::UpdateInternal(Mode mode, unsigned numPads)
{
	// No input until the UI is in a ready state
	if (!GetRgscConcreteInstance()->_GetUiInterface()->IsReadyToAcceptCommands())
		return;

	if (!rlVerify(numPads > 0))
		return;

	bool isUiVisible = GetRgscConcreteInstance()->_GetUiInterface()->IsVisible();

	const int RGSC_BUTTON1 = IGamepadLatestVersion::RGSC_SELECT;
	const int RGSC_BUTTON2 = IGamepadLatestVersion::RGSC_R1;
	const int RGSC_BUTTON_COMBO = RGSC_BUTTON1 | RGSC_BUTTON2;
	const int RGSC_BUTTON_ALT_CLOSE = IGamepadLatestVersion::RGSC_RRIGHT;	// can also use B to close the SCUI

	// Allocate a set of bools to track input for this frame. 
	InputState* inputStates = Alloca(InputState, numPads);
	if (inputStates == NULL)
		return;

	bool hadAnyNewInputThisUpdate = false;

	//////////////////////////////////////////////////////////////////////////
	// PHASE ONE
	//	- Enumerate all gamepads and cache their input states.
	//	- Consume the input such that the next run through, buttons
	//    pressed/released states can be calculated correctly.
	//	- Track the aggregate state of all gamepads.
	//	- If the SCUI open/close button combination is pressed, open the SCUI.
	//////////////////////////////////////////////////////////////////////////
	for(unsigned i = 0; i < numPads; i++)
	{
		// init each state
		inputStates[i].Clear();

		IGamepad* pad = GetRgscConcreteInstance()->_GetGamepadManager()->GetPad(mode, i);

		// skip invalid pads
		if (pad == NULL)
			continue;

		IGamepadV1* v1 = NULL; 
		RGSC_HRESULT hr = pad->QueryInterface(IID_IGamepadV1, (void**) &v1);
		if(!SUCCEEDED(hr) || v1 == NULL || !v1->IsConnected())
		{
			// skip disconnected or unusable pads
			continue;
		}

		// If the gamepad does not have new input, skip this update.
		IGamepadV2* v2 = NULL; 
		hr = pad->QueryInterface(IID_IGamepadV2, (void**) &v2);
		if(!SUCCEEDED(hr) || v2 == NULL || !v2->HasInput())
		{
			// Skip unusable pads, or those without input
			continue;
		}

		// flag for input this update
		hadAnyNewInputThisUpdate = true;

		// update this state.
		inputStates[i].HadInputThisUpdate = true;
		inputStates[i].Pressed |= v1->GetPressedButtons();
		inputStates[i].Released |= v1->GetReleasedButtons();
		inputStates[i].Held |= v1->GetButtons();
		inputStates[i].LeftX = v1->GetNormLeftX();
		inputStates[i].LeftY  = v1->GetNormLeftY();
		inputStates[i].RightX = v1->GetNormRightX();
		inputStates[i].RightY  = v1->GetNormRightY();

		// Update the the state that tracks all buttons held on all pads.
		s_AllControllers += inputStates[i];

		// Clear the input flag when we've consumed it.
		if(SUCCEEDED(hr) && (v2 != NULL))
		{
			v2->ClearHasInput();
		}

		// If the UI is not visible, check for the open button.
		if(!isUiVisible)
		{
			// If the Open/Close button combination is held, and we're not waiting for the Right Bumper to be 
			// released (to avoid rapid state toggling). We use 'held' in this case, because we want to immediately
			// trigger the UI without letting the game consume this input as an ingame action.
			if((inputStates[i].Held & RGSC_BUTTON_COMBO) == RGSC_BUTTON_COMBO  && !sm_bIgnoreRBInputUntilRelease)
			{
				// Ignore all RB input until it has been released, as this button being held is part of the SCUI
				// open logic, and not to be interpreted as input.
				sm_bIgnoreRBInputUntilRelease = true;

				// recapture the mouse position
				POINT lpPoint;
				BOOL bResult = ::GetCursorPos(&lpPoint);
				if (bResult == TRUE)
				{
					sm_TargetX = (float)lpPoint.x;
					sm_TargetY = (float)lpPoint.y;
				}

				SetIsUsingController(true, mode, i);
				Displayf("Opening Social Club UI from %s controller %d", GetInputString(mode), i);
				ioEventQueue::Queue(ioEvent::IO_SOCIAL_CLUB_OPEN, 0, 0, 0);
				break;
			}
			else if (inputStates[i].Released & IGamepadLatestVersion::RGSC_R1)
			{
				// If the R1 button is already released, we can top ignoring input for it.
				sm_bIgnoreRBInputUntilRelease = false;
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////
	//	PHASE TWO
	// - Only perform the following checks when the SCUI is on screen (visible)
	// - If a controller is in use, we will only respond to that controller to avoid conflicts.
	// - If no controller is in use, we will wait for input from any controller.
	// - We're actively seeking for buttons that close the SCUI, either Back+RB or B
	////////////////////////////////////////////////////////////////////////////////////////////
	if (isUiVisible)
	{
		// if a controller is not being used, poll each controller to see if we get input
		unsigned int firstCtrlIdx = 0;
		unsigned int lastCtrlIdx = numPads - 1;

		// if a controller is being used, just respond to that controller
		if(m_IsUsingController[mode])
		{
			firstCtrlIdx = s_ControllerIndex[mode];
			lastCtrlIdx = s_ControllerIndex[mode];
		}

		bool bCloseScuiButton = false;
		bool bCloseScuiAltButton = false;
		bool bCloseScuiButtonLinger = false;

		// Enumerate all controllers, looking for the SCUI open/close button.
		// If a user is still holding down either of the combination buttons, set the 'bCloseScuiButtonLinger'
		// flag so that we won't attempt to close the SCUI yet.
		for(unsigned int ctrlIdx = firstCtrlIdx; ctrlIdx <= lastCtrlIdx; ctrlIdx++)
		{
			// This pad had no input this update, continue
			if (!inputStates[ctrlIdx].HadInputThisUpdate)
				continue;

			// Detect the SCUI close button
			bCloseScuiButton |= (inputStates[ctrlIdx].Held & RGSC_BUTTON_COMBO) == RGSC_BUTTON_COMBO;
			bCloseScuiAltButton |= (inputStates[ctrlIdx].Released & RGSC_BUTTON_ALT_CLOSE) == RGSC_BUTTON_ALT_CLOSE;

			// check lingering button1/2 held
			bCloseScuiButtonLinger |= (inputStates[ctrlIdx].Held & RGSC_BUTTON1) == RGSC_BUTTON1;
			bCloseScuiButtonLinger |= (inputStates[ctrlIdx].Held & RGSC_BUTTON2) == RGSC_BUTTON2;

			// RGSC close button is held, and we're not ignoring RB input
			if (bCloseScuiButton && !sm_bIgnoreRBInputUntilRelease)
			{
				sm_bCloseButtonWasPressed[mode] = true;
			}
		}

		// Checking for all conditions:
		//	 hadAnyNewInputThisUpdate  - We detected a change in controller buttons on this frame.
		//   sm_bCloseButtonWasPressed - The open/close button combo was held at one point, but is no longer held (i.e. has been released)
		//	 sm_bIgnoreRBInputUntilRelease - We are not ignoring the Right Bumper to avoid rapid toggling.
		// OR this single other condition:
		//   bCloseScuiAltButton - The alternate (B) close button is released.
		if (hadAnyNewInputThisUpdate && !sm_bIgnoreRBInputUntilRelease &&
			((sm_bCloseButtonWasPressed[mode] && !bCloseScuiButtonLinger) || bCloseScuiAltButton))
		{
			//SetIsUsingController(false);
			Displayf("Closing Social Club UI from %s", GetInputString(mode));
			sm_bCloseButtonWasPressed[mode] = false;
			ioEventQueue::Queue(ioEvent::IO_SOCIAL_CLUB_CLOSE, 0, 0, METHOD_CONTROLLER);
			return;
		}

		////////////////////////////////////////////////////////////////////////////////////////////
		//	PHASE THREE
		// - In this phase we will attempt to handle gamepad input.
		// - The above conditions dictate that the SCUI is visible.
		// - If a controller is in use, we will only respond to that controller to avoid conflicts.
		// - If no controller is in use, we will wait for input from any controller.
		// - Gamepad buttons will be converted to mouse events, or sent to the SCUI.
		////////////////////////////////////////////////////////////////////////////////////////////
		for(unsigned int ctrlIdx = firstCtrlIdx; ctrlIdx <= lastCtrlIdx; ctrlIdx++)
		{
			IGamepad* pad = GetRgscConcreteInstance()->_GetGamepadManager()->GetPad(mode, ctrlIdx);
			if (pad == NULL)
				continue;

			IGamepadV1* v1 = NULL; 
			RGSC_HRESULT hr = pad->QueryInterface(IID_IGamepadV1, (void**) &v1);
			if(!SUCCEEDED(hr) || v1 == NULL || !v1->IsConnected())
			{
				continue;
			}

			// If buttons are now pressed or released on this frame, we enter controller mode.
			// The current controller index becomes the main controller index.
			if((inputStates[ctrlIdx].Pressed != 0) || (inputStates[ctrlIdx].Released != 0))
			{
				SetIsUsingController(true, mode, ctrlIdx);

				// Forward the input change to the SCUI.
				if (GetRgscConcreteInstance()->_GetGamepadManager()->IsForwardingPadInputToBrowser())
				{
					ioEventQueue::Queue(ioEvent::IO_CONTROLLER_BUTTONS, inputStates[ctrlIdx].Pressed, inputStates[ctrlIdx].Released, inputStates[ctrlIdx].Held);
				}
			}

			// Simulate mouse and keyboard events.
			UpdatePadDeflection(ctrlIdx, mode, v1->GetNormLeftX(), v1->GetNormLeftY(), v1->GetNormRightX(), v1->GetNormRightY());
			UpdatePadInput(inputStates[ctrlIdx]);

			IGamepadV5* v5 = NULL;
			hr = pad->QueryInterface(IID_IGamepadV5, (void**) &v5);
			if (SUCCEEDED(hr) && v5 != NULL)
			{
				UpdatePadPointers(ctrlIdx, mode, v5->HasPointerData(), v5->GetPointerX(), v5->GetPointerY());
			}

			// Check to see if we're released the R1 button yet
			if (inputStates[ctrlIdx].Released & IGamepadLatestVersion::RGSC_R1)
			{
				sm_bIgnoreRBInputUntilRelease = false;
			}

			// if we've attached to a controller, don't accept input from any other controller. We can early out.
			if(m_IsUsingController[mode])
			{
				break;
			}
		}
	}
}

void Rgsc_Input::UpdatePadDeflection(const unsigned ctrlIdx, Mode mode, const float deflectionX, float deflectionY, float /*scrollDeflectionX*/, float scrollDeflectionY)
{
	if (!GetRgscConcreteInstance()->_GetGamepadManager()->IsSimulatingMouseEvents())
		return;

	// Extract the ClientRect of the current window, and calculate the current window width
	RECT rc;
	::GetClientRect((HWND)sm_hDeviceWindow, (LPRECT)&rc);
	float width = (float)(rc.right - rc.left);

	// If in "maximum" deflection mode (70%+ deflection)
	if (Abs(deflectionX) > 0.70f || Abs(deflectionY) > 0.70f)
	{
		// Reset the mouse move count used to calculate when to give control back to keyboard/mouse
		m_MouseMoveCount = 0;

		// If the controller-based mouse timer is 0, set it to the current timestamp
		if (m_ControllerMouseStartTime == 0)
		{
			m_ControllerMouseStartTime = sysTimer::GetSystemMsTime();
		}		

		static const int ACCELERATION_TIME_MS = 2500; // We accelerate to max speed over 2.5 seconds

		// Calculate the time since we started using max-deflection controller-based mouse movement
		unsigned timeSinceStart = sysTimer::GetSystemMsTime() - m_ControllerMouseStartTime;

		// The speed ratio is what percentage of acceleration time has elapsed
		float speedRatio = (float)timeSinceStart / ACCELERATION_TIME_MS;
		if (speedRatio > 1.0f)
		{
			speedRatio = 1.0f;
		}

		// Define some baseline minimum and maximum speeds 
		static const float MIN_MOUSE_SPEED_FAST = 500.0f;
		static const float MAX_MOUSE_SPEED_FAST = 1200.0f;

		// Fast motion is based on 0.7 (70%) of the window width per second.
		static const float FAST_SPEED_RATIO = 0.7f;

		// Calculate the fast speed using the fast speed ratio, and clamp based on our min/max thresholds
		float fastSpeed = width * FAST_SPEED_RATIO;
		float mouseMoveSpeed = MIN(MAX_MOUSE_SPEED_FAST, MAX(MIN_MOUSE_SPEED_FAST, fastSpeed * speedRatio));

		// We ARE using the controller
		SetIsUsingController(true, mode, ctrlIdx);

		// Apply TimeStep, and calculate x/y mouse deltas
		int timeStep = m_TimeStep.GetTimeStep();
		float timeScale = ((float)timeStep) / 1000.0f;
		float dx = (deflectionX * mouseMoveSpeed * timeScale);
		float dy = (deflectionY * mouseMoveSpeed * timeScale);
		SetNewCursorPos(dx, dy);
	}
	// Otherwise, is the deflection within the slow range (30% to 70%)
	else if (Abs(deflectionX) > 0.30f || Abs(deflectionY) > 0.30f)
	{
		// Reset the mouse move count used to calculate when to give control back to keyboard/mouse
		m_MouseMoveCount = 0;

		// Define some baseline minimum and maximum speeds
		static const float MIN_MOUSE_SPEED_SLOW = 300.0f;
		static const float MAX_MOUSE_SPEED_SLOW = 500.0f;

		// Fast motion is based on 0.25 (25%) of the window width per second.
		static const float SLOW_SPEED_RATIO = 0.25f;

		// Calculate the slow speed using the slow speed ratio, and clamp based on our min/max thresholds
		float slowSpeed = MIN(MAX_MOUSE_SPEED_SLOW, MAX(MIN_MOUSE_SPEED_SLOW, width * SLOW_SPEED_RATIO));

		// We ARE using the controller
		SetIsUsingController(true, mode, ctrlIdx);

		// Apply TimeStep, and calculate x/y mouse deltas
		int timeStep = m_TimeStep.GetTimeStep();
		float timeScale = timeStep / 1000.0f;
		float dx = (deflectionX * slowSpeed * timeScale);
		float dy = (deflectionY * slowSpeed * timeScale);
		SetNewCursorPos(dx, dy);
	}
	else
	{
		m_ControllerMouseStartTime = 0;
	}

	// Use a 30% dead zone for right stick up/down
	if (Abs(scrollDeflectionY) > 0.30f)
	{
		// By using a scroll interval of 100ms, we allow up to 10 mouse wheel ticks per second
		static const int SCROLL_INTERVAL_MS = 100;

		// Set the controller start time if not present
		if (m_ControllerScrollStartTime == 0)
		{
			m_ControllerScrollStartTime = sysTimer::GetSystemMsTime();
		}
		// If the time since the start time has surpassed the interval, send a mouse scroll event
		else if (sysTimer::GetSystemMsTime() - m_ControllerScrollStartTime > SCROLL_INTERVAL_MS)
		{
			// Reset the start time
			m_ControllerScrollStartTime = sysTimer::GetSystemMsTime();

			DWORD wParam = MAKEWPARAM(0, scrollDeflectionY > 0 ? -WHEEL_DELTA : WHEEL_DELTA);
			ioEventQueue::Queue(ioEvent::IO_MOUSE_WHEEL, 0, 0, ((short)HIWORD(wParam) / WHEEL_DELTA));
		}

		SetIsUsingController(true, mode, ctrlIdx);
	}
}

void Rgsc_Input::UpdatePadPointers(const unsigned ctrlIdx, Mode mode, bool bHasPointerData, int px, int py)
{
	if (!bHasPointerData)
		return;

	if (!GetRgscConcreteInstance()->_GetUiInterface()->IsVisible())
		return;

	// Update the virtual pointer.
	g_VirtualCursor.SetPosition((float)px, (float)py);

	// Convert the point to screen coordinates and send to the event queue.
	POINT p;
	p.x = px;
	p.y = py;

	// Keep the last position in memory to avoid duplicating events.
	static POINT lastPos = { 0 };
 	if (lastPos.x != p.x || lastPos.y != p.y)
	{
		SetIsUsingController(true, mode, ctrlIdx);

		// Queue the mouse event - we're already in the client coordinates needed by the SCUI, so avoid mapping the mouse.
		ioEventQueue::Queue(ioEvent::IO_MOUSE_MOVE, WM_MOUSEMOVE, 0, MAKELPARAM(p.x, p.y), RGSC_MOUSE_MOD_NO_MAP);
	}
	
	lastPos = p;
}

void Rgsc_Input::UpdatePadInput(InputState& state)
{
	if (!GetRgscConcreteInstance()->_GetGamepadManager()->IsSimulatingKeyboardEvents())
		return;

	// A button PRESS
	if (state.Pressed & IGamepadLatestVersion::RGSC_RDOWN)
	{
		// Simulates a mouse down on press
		POINT p;
		if (GetClientCursorPosition(&p))
		{
			SimulateMouseEvent(ioEvent::IO_MOUSE_LEFT_DOWN, WM_LBUTTONDOWN, 0, MAKELPARAM(p.x, p.y));
		}
	}
	// A button RELEASE
	if (state.Released & IGamepadLatestVersion::RGSC_RDOWN)
	{
		// Simulates a mouse up on release
		POINT p;
		if (GetClientCursorPosition(&p))
		{
			ProduceControllerEvent(ioEvent::IO_MOUSE_LEFT_UP);
			SimulateMouseEvent(ioEvent::IO_MOUSE_LEFT_UP, WM_LBUTTONUP, 0, MAKELPARAM(p.x, p.y));
		}
	}

	// L2/LT -> Page Up
	if (state.Pressed & IGamepadLatestVersion::RGSC_L2)
	{
		SimulateKeyboardEvent(ioEvent::IO_KEY_DOWN, WM_KEYDOWN, VK_PRIOR, 0);
	}
	else if (state.Released & IGamepadLatestVersion::RGSC_L2)
	{
		ProduceControllerEvent(ioEvent::IO_KEY_UP);
		SimulateKeyboardEvent(ioEvent::IO_KEY_UP, WM_KEYUP, VK_PRIOR, 0);
	}

	// R2/RT -> Page Down
	if (state.Pressed & IGamepadLatestVersion::RGSC_R2)
	{
		SimulateKeyboardEvent(ioEvent::IO_KEY_DOWN, WM_KEYDOWN, VK_NEXT, 0);
	}
	else if (state.Released & IGamepadLatestVersion::RGSC_R2)
	{
		// Increment the controller events to consume
		ProduceControllerEvent(ioEvent::IO_KEY_UP);
		SimulateKeyboardEvent(ioEvent::IO_KEY_UP, WM_KEYUP, VK_NEXT, 0);
	}

	// START = ENTER
	if (state.Pressed & IGamepadLatestVersion::RGSC_START)
	{
		SimulateKeyboardEvent(ioEvent::IO_KEY_DOWN, WM_KEYDOWN, VK_RETURN, 0);
	}
	else if (state.Released & IGamepadLatestVersion::RGSC_START)
	{
		// Increment the controller events to consume
		ProduceControllerEvent(ioEvent::IO_KEY_UP);
		SimulateKeyboardEvent(ioEvent::IO_KEY_UP, WM_KEYUP, VK_RETURN, 0);
	}

	// L1: Shift+Tab, R1: Tab. Slightly different approach here due to RGSC_SHIFT_MOD
	if(state.Released & IGamepadLatestVersion::RGSC_L1)
	{
		ioEventQueue::Queue(ioEvent::IO_KEY_DOWN, WM_KEYDOWN, VK_TAB, 0, RGSC_SHIFT_MOD);
		ioEventQueue::Queue(ioEvent::IO_KEY_DOWN, WM_KEYUP, VK_TAB, 0, RGSC_SHIFT_MOD);
	} 

	// We released RB, and its not part of the BACK+RB combo, and we're not ignoring RB
	if((state.Released & IGamepadLatestVersion::RGSC_R1) && 
		!(state.Held & IGamepadLatestVersion::RGSC_SELECT) && !sm_bIgnoreRBInputUntilRelease)
	{
		ioEventQueue::Queue(ioEvent::IO_KEY_DOWN, WM_KEYDOWN, VK_TAB, 0, 0);
		ioEventQueue::Queue(ioEvent::IO_KEY_DOWN, WM_KEYUP, VK_TAB, 0, 0);
	}
}

////////////////////////////////////////////////////////////
// Utility Functions
////////////////////////////////////////////////////////////
bool Rgsc_Input::GetCursorPosition(LPPOINT p)
{
	if (g_VirtualCursor.IsEnabled())
	{
		p->x = (LONG)g_VirtualCursor.GetX();
		p->y = (LONG)g_VirtualCursor.GetY();
		return g_VirtualCursor.IsInitialized();
	}
	else
	{
		return (::GetCursorPos(p) == TRUE);
	}
}

bool Rgsc_Input::GetClientCursorPosition(LPPOINT p)
{
	if (g_VirtualCursor.IsEnabled())
	{
		p->x = (LONG)g_VirtualCursor.GetX();
		p->y = (LONG)g_VirtualCursor.GetY();
		return g_VirtualCursor.IsInitialized();
	}
	else
	{
		return (::GetCursorPos(p) == TRUE) && (::ScreenToClient((HWND)sm_hDeviceWindow, p) == TRUE);
	}
}

bool Rgsc_Input::ClampToScreen(LPPOINT p)
{
	// temporaries to use for clamp function
	float x = (float)p->x;
	float y = (float)p->y;

	// cache result
	bool result = ClampToScreen(x, y);

	// set out params
	p->x = (LONG)x;
	p->y = (LONG)y;

	return result;
}

bool Rgsc_Input::ClampToScreen(float& x, float& y)
{
	bool bClamped = false;

	RECT rc;

	// Original LANoire PC doesn't set the render rect when multiple monitors are in use.
	// So if the rect has 0 width or height, fall back to the full client rect.
	const Rgsc_Graphics::RenderRect& r = GetRgscConcreteInstance()->_GetUiInterface()->GetRenderRect();

	if(r.GetWidth() == 0 || r.GetHeight() == 0)
	{
		::GetClientRect((HWND)sm_hDeviceWindow, (LPRECT)&rc);
	}
	else
	{
		rc.left = r.m_Left;
		rc.top = r.m_Top;
		rc.bottom = r.m_Bottom;
		rc.right = r.m_Right;
	}

	::ClientToScreen((HWND)sm_hDeviceWindow, (LPPOINT)&rc.left);
	::ClientToScreen((HWND)sm_hDeviceWindow, (LPPOINT)&rc.right);

	// Clamp to screen
	if (x <= rc.left)
	{
		x = (float)(rc.left + 1);
		bClamped = true;
	}
	else if (x >= rc.right)
	{
		x = (float)(rc.right - 1);
		bClamped = true;
	}

	if (y <= rc.top)
	{
		y = (float)(rc.top + 1);
		bClamped = true;
	}
	else if (y >= rc.bottom)
	{
		y = (float)(rc.bottom - 1);
		bClamped = true;
	}

	return bClamped;
}

void Rgsc_Input::SetNewCursorPos(float dx, float dy)
{
	Rgsc_Input::sm_bInputThisFrame = true;

	// If any delta is present
	if (dx != 0 || dy != 0)
	{
		// Get the current cursor position, and apply the deltas
		sm_TargetX += dx;
		sm_TargetY += dy;
		
		ClampToScreen(sm_TargetX, sm_TargetY);

		int newX = (int)(sm_TargetX);
		int newY = (int)(sm_TargetY);

		::SetCursorPos(newX, newY);
	}
}

void Rgsc_Input::SimulateKeyboardEvent(ioEvent::ioEventType eventType, unsigned msg, WPARAM wParam, LPARAM lParam)
{
	Assert(ioEvent::IsKeyboardEvent(eventType));
	Rgsc_Input::sm_bInputThisFrame = true;
	
	if (IsSimulatingKeyboardEvents())
	{
		SendMessage((HWND)sm_hDeviceWindow, msg, wParam, lParam);
	}
	else
	{
		ioEventQueue::Queue(eventType, msg, wParam, lParam);
	}
}

void Rgsc_Input::SimulateMouseEvent(ioEvent::ioEventType eventType, unsigned msg, WPARAM wParam, LPARAM lParam)
{
	Assert(ioEvent::IsMouseEvent(eventType));
	Rgsc_Input::sm_bInputThisFrame = true;

	if (IsSimulatingMouseEvents())
	{
		SendMessage((HWND)sm_hDeviceWindow, msg, wParam, lParam);
	}
	else
	{
		__int64 mods = g_VirtualCursor.IsEnabled() ? RGSC_MOUSE_MOD_NO_MAP : RGSC_MOUSE_MOD_NONE;
		ioEventQueue::Queue(eventType, msg, wParam, lParam, mods);
	}
}

bool Rgsc_Input::IsSimulatingKeyboardEvents()
{
	return (sm_bInputSubclassing && sm_bKeyboardEventForwarding);
}

bool Rgsc_Input::IsSimulatingMouseEvents()
{
	return (sm_bInputSubclassing && sm_bMouseEventForwarding);
}

void Rgsc_Input::CheckMouseInWindow()
{
	rtry
	{
		RECT rc;
		BOOL bResult = ::GetClientRect((HWND)sm_hDeviceWindow, (LPRECT)&rc);
		rcheck(bResult == TRUE, catchall, );

		// Apply window margins
		rc.left += sm_WindowMargins.left;
		rc.right -= sm_WindowMargins.right;
		rc.top += sm_WindowMargins.top;
		rc.bottom -= sm_WindowMargins.bottom;

		POINT lpPoint;
		bResult = ::GetCursorPos(&lpPoint);
		rcheck(bResult == TRUE, catchall, );

		bResult = ::ScreenToClient((HWND)sm_hDeviceWindow, &lpPoint);
		rcheck(bResult == TRUE, catchall, );

		bResult = ::PtInRect(&rc, lpPoint);
		rcheck(bResult == TRUE, catchall, );

		sm_bMouseInWindow = true;
	}
	rcatchall
	{
		sm_bMouseInWindow = false;
	}
}

void Rgsc_Input::HandleMouseMove(int newMouseX, int newMouseY)
{
	s_OldMouseX = newMouseX;
	s_OldMouseY = newMouseY;

	//Displayf("WM_MOUSEMOVE %d, %d", newMouseX, newMouseY);
	if(IsUsingController())
	{
		// only switch to keyboard/mouse mode after we receive a few mouse move events
		const unsigned int NumMouseMoveEventsBeforeSwitchingModes = 10;
		if(m_MouseMoveCount < NumMouseMoveEventsBeforeSwitchingModes)
		{
			m_MouseMoveCount++;
		}
		else
		{
			SetIsUsingController(false);
		}
	}
}

static inline int TranslateKey(unsigned wparam,unsigned special) {
	return wparam;
// 	if (special & (1<<24)) {
// 		switch ((special >> 16) & 0xFF) {
// 		case KEY_LCONTROL: return KEY_RCONTROL;
// 		case KEY_LMENU: return KEY_RMENU;
// 		case KEY_RETURN: return KEY_NUMPADENTER;
// 		case KEY_NUMPAD7: return KEY_HOME;
// 		case KEY_NUMPAD8: return KEY_UP;
// 		case KEY_NUMPAD9: return KEY_PAGEUP;
// 		case KEY_NUMPAD4: return KEY_LEFT;
// 		case KEY_NUMPAD6: return KEY_RIGHT;
// 		case KEY_NUMPAD1: return KEY_END;
// 		case KEY_NUMPAD2: return KEY_DOWN;
// 		case KEY_NUMPAD3: return KEY_PAGEDOWN;
// 		case KEY_NUMPAD0: return KEY_INSERT;
// 		case KEY_DECIMAL: return KEY_DELETE;
// 		case KEY_SLASH: return KEY_DIVIDE;
// 		}
// 	}
// 	return (int)((special >> 16) & 0xFF);
}

///////////////////////////////////////////////////////////////////
// Touch
///////////////////////////////////////////////////////////////////
#if SUPPORT_TOUCH_INPUT
BOOL Rgsc_Input::GetTouchInputInfo(HTOUCHINPUT hTouchInput, UINT cInputs, PTOUCHINPUT pInputs)
{
	if(sm_FuncGetTouchInputInfo == NULL)
	{
		return FALSE;
	}

	return sm_FuncGetTouchInputInfo(hTouchInput, cInputs, pInputs, sizeof(TOUCHINPUT));
}

BOOL Rgsc_Input::CloseTouchInputHandle(HTOUCHINPUT hTouchInput)
{
	if(sm_FuncCloseTouchInputHandle == NULL)
	{
		return FALSE;
	}

	return sm_FuncCloseTouchInputHandle(hTouchInput);
}

LRESULT Rgsc_Input::OnTouch(WindowHandle hWnd, WPARAM wParam, LPARAM lParam )
{
	m_TouchStartTime = timeGetTime();

	UINT cInputs = LOWORD(wParam);

	const unsigned int MAX_TOUCHES = 10;
	Assert(cInputs <= MAX_TOUCHES);
	TOUCHINPUT pInputs[MAX_TOUCHES];

	if(GetTouchInputInfo((HTOUCHINPUT)lParam, cInputs, pInputs))
	{
		// TODO: NS - mouse events need msg, wParam, lParam. Need to convert x, y, to lparam, etc.
		for(UINT i = 0; i < cInputs; i++)
		{
			TOUCHINPUT ti = pInputs[i];

			POINT point;
			point.x = (LONG)(ti.x / 100.0f);
			point.y = (LONG)(ti.y / 100.0f);

			if((::ScreenToClient((HWND)hWnd, &point) == TRUE) &&
				(ti.dwFlags & TOUCHEVENTF_PRIMARY) &&
				((ti.dwFlags & TOUCHEVENTF_PALM) == 0))
			{
				if(ti.dwFlags & TOUCHEVENTF_DOWN)
				{
					Displayf("Touched down %u, %u", point.x, point.y);
					ioEventQueue::Queue(ioEvent::IO_MOUSE_MOVE, point.x, point.y, 0);
					ioEventQueue::Queue(ioEvent::IO_MOUSE_LEFT_DOWN, point.x, point.y, 0);
					s_OldMouseX = point.x;
					s_OldMouseY = point.y;
				}
				else if(ti.dwFlags & TOUCHEVENTF_MOVE)
				{
					if((point.x != s_OldMouseX) || (point.y != s_OldMouseY)) 
					{
						s_OldMouseX = point.x;
						s_OldMouseY = point.y;

						Displayf("Touch moved %u, %u", point.x, point.y);
						ioEventQueue::Queue(ioEvent::IO_MOUSE_MOVE, point.x, point.y, 0);
					}
				}
				else if(ti.dwFlags & TOUCHEVENTF_UP)
				{
					Displayf("Touch removed %u, %u", point.x, point.y);
					ioEventQueue::Queue(ioEvent::IO_MOUSE_MOVE, point.x, point.y, 0);
					ioEventQueue::Queue(ioEvent::IO_MOUSE_LEFT_UP, point.x, point.y, 0);
					s_OldMouseX = 0;
					s_OldMouseY = 0;
				}
			}
		}
	}
	else
	{
		/* handle the error here */
		Warningf("GetTouchInputInfo() failed");
	}

	// if you handled the message, close the touch input handle and return
	if(!CloseTouchInputHandle((HTOUCHINPUT)lParam))
	{
		Warningf("CloseTouchInputHandle() failed");
	}

	return 0;
}
#endif

///////////////////////////////////////////////////////////////////
// Misc
///////////////////////////////////////////////////////////////////
int Rgsc_Input::GetModifiers()
{
	int modifiers = 0;

	if (GetAsyncKeyState(VK_SHIFT) & 0x8000)
	{
		modifiers |= RGSC_SHIFT_MOD;
	}

	if (GetAsyncKeyState(VK_CONTROL) & 0x8000)
	{
		modifiers |= RGSC_CONTROL_MOD;
	}

	if (GetAsyncKeyState(VK_MENU) & 0x8000)
	{
		modifiers |= RGSC_ALT_MOD;
	}

	return modifiers;
}

bool Rgsc_Input::IsInputEnabled()
{
	return sm_bInputEnabled;
}

bool Rgsc_Input::IsForeground()
{
	return sm_bIsForegroundWindow;
}

bool Rgsc_Input::IsMouseInWindow()
{
	return sm_bMouseInWindow;
}

void Rgsc_Input::SetWindowMargins(int top, int left, int bottom, int right)
{
	sm_WindowMargins.left = left;
	sm_WindowMargins.top = top;
	sm_WindowMargins.right = right;
	sm_WindowMargins.bottom = bottom;
}

#if IME_TEXT_INPUT
void Rgsc_Input::SendImeEventToScui(const char* msg, const char* lParam, bool composition, bool candidates, bool finalized)
{
	bool succeeded = false;

	char imeEvent[4096] = {0};

	rtry
	{
		RsonWriter rw;
		rw.Init(imeEvent, sizeof(imeEvent), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		rw.WriteString("Event", msg);

		if(lParam)
		{
			rw.WriteString("Param", lParam);
		}

		bool compositionOpen = sm_ImeState[IME_CURRENT_STATE].m_InProgress;
		bool candidatesOpen = sm_ImeState[IME_CURRENT_STATE].m_IsCandidatesWindowOpen;

		// composition window must be open if candidates window is open
		rw.WriteBool("CompositionOpen", compositionOpen || candidatesOpen);
		rw.WriteBool("CandidatesOpen", candidatesOpen);

		if(candidatesOpen)
		{
			bool canPageUp = sm_ImeState[IME_CURRENT_STATE].m_PageStart > 0;
			bool canPageDown = (sm_ImeState[IME_CURRENT_STATE].m_TotalCandidates - (sm_ImeState[IME_CURRENT_STATE].m_PageStart + sm_ImeState[IME_CURRENT_STATE].m_PageSize)) > 0;
			rw.WriteBool("CanPageUp", canPageUp);
			rw.WriteBool("CanPageDown", canPageDown);
			Displayf("CanPageUp: %s", canPageUp ? "true" : "false");
			Displayf("CanPageDown: %s", canPageDown ? "true" : "false");
		}

		RgscUtf8String utf8 = rgsc::WideToUTF8(RgscWideString::point_to(sm_ImeState[IME_CURRENT_STATE].m_CompositionText)); 
		if(finalized)
		{
			rw.WriteString("Result", utf8.data());
		}

		if(composition)
		{
			rw.WriteString("Composition", utf8.data());
		}

		stringUtil_free(utf8);

		if(candidates && candidatesOpen)
		{
			unsigned numCandidates = sm_ImeState[IME_CURRENT_STATE].m_NumCandidates;
			if(numCandidates > 0) 
			{
				rcheck(rw.BeginArray("Candidates", NULL), catchall, );

				for(unsigned i = 0; i < numCandidates; ++i)
				{
					utf8 = rgsc::WideToUTF8(RgscWideString::point_to(sm_ImeState[IME_CURRENT_STATE].m_Candidates[i])); 
					rw.WriteString(NULL, utf8.data());
					stringUtil_free(utf8);
				}

				rcheck(rw.End(), catchall, );
			}
		}

		rcheck(rw.End(), catchall, );
	}
	rcatchall
	{
		imeEvent[0] = '\0';
	}

#if !__NO_OUTPUT && __DEV
	// we can't print Unicode characters to TTY or normal debug log, so output to Unicode-aware retail log in __DEV builds for debugging purposes
	RgscWideString wide = rgsc::UTF8ToWide(RgscUtf8String::point_to(imeEvent)); 
	RgscDisplayW(L"%ls", wide.data());
	stringUtil_free(wide);
#endif

	rgsc::Script::JsImeEvent(&succeeded, imeEvent);
}

// Windows IME implementation.
void Rgsc_Input::ImeRetrieveTextResult(WindowHandle hwnd, const char* msg, const char* lParam)
{
	HIMC hImc = ImmGetContext(hwnd);

	const u32 numEmptyChars = TEXT_BUFFER_LENGTH - sm_CurrentLength;

	// + sizeof(wchar_t) to buffer size as according to https://msdn.microsoft.com/en-us/library/windows/desktop/dd319104%28v=vs.85%29.aspx it does not include
	// NULL terminator.
	Assertf(ImmGetCompositionStringW(hImc, GCS_RESULTSTR, NULL, 0) + sizeof(wchar_t) <= numEmptyChars * sizeof(wchar_t),
		"Buffer not large enough for IME composition text!" );

	// ImmGetCompositionStringW does not add the NULL terminator so null array first.
	sysMemSet(&sm_CurrentTextBuffer[sm_CurrentLength], 0, numEmptyChars * sizeof(wchar_t));

	// -1 for NULL space.
	ImmGetCompositionStringW( hImc,
		GCS_RESULTSTR,
		reinterpret_cast<wchar_t*>(&sm_CurrentTextBuffer[sm_CurrentLength]),
		(numEmptyChars - 1) * sizeof(wchar_t));

	// Fill sm_CurrentTextBufferKeys from the current position to the end will NULL so that it does not contain incorrect key values!
	sm_CurrentLength = StringLength(sm_CurrentTextBuffer);	

	ImmReleaseContext(hwnd, hImc);

	SendImeEventToScui(msg, lParam, false, false, true);
}

void Rgsc_Input::ImeUpdateCandidateTexts(WindowHandle hwnd, const char* msg, const char* lParam)
{
	HIMC hImc = ImmGetContext(hwnd);
	const u32 len = ImmGetCandidateListW(hImc, 0, NULL, 0);
	if(len)
	{
		LPCANDIDATELIST pCandidateList = NULL;

		// To ensure we do not use too much memory on the stack we use the heap if we are going to RageAlloca() a lot of memory.
		const bool useHeap = len > 2048;
		if(useHeap)
		{
			pCandidateList = reinterpret_cast<LPCANDIDATELIST>(rage_new u8[len]);
		}
		else
		{
			pCandidateList =  reinterpret_cast<LPCANDIDATELIST>(RageAlloca(len));
		}

		if(rlVerifyf(ImmGetCandidateListW(hImc, 0, pCandidateList, len), "ImmGetCandidateListW failed!"))
		{
			//Displayf("pCandidateList->dwPageSize: %d, pCandidateList->dwCount: %d, pCandidateList->dwPageStart: %d", pCandidateList->dwPageSize, pCandidateList->dwCount, pCandidateList->dwPageStart);

			if(pCandidateList->dwPageSize >= 0 && pCandidateList->dwPageSize <= pCandidateList->dwCount)
			{
				Assertf( pCandidateList->dwPageSize <= MAX_IME_CANDIDATES,
					"Too many candidates available (%u), MAX_IME_CANDIDATES (%u) might need increasing!",
					pCandidateList->dwPageSize,
					MAX_IME_CANDIDATES );

				const DWORD maxCandidates = Min(pCandidateList->dwPageSize, static_cast<DWORD>(MAX_IME_CANDIDATES));
				const DWORD lastCandidateIndex = Min(pCandidateList->dwPageStart + maxCandidates, pCandidateList->dwCount);

				sysMemSet(sm_ImeState[IME_CURRENT_STATE].m_Candidates, 0, sizeof(sm_ImeState[IME_CURRENT_STATE].m_Candidates));
				for(u32 i = pCandidateList->dwPageStart, candidateIndex = 0u; i < lastCandidateIndex; ++i, ++candidateIndex)
				{
					// NOTE: Yuk, Microsoft give us the offset from sm_pImeCandidateList to the wchar_t* string we are after rather than just giving us the pointer.
					// That means to get he pointer, we need to cast sm_pImeCandidateList to a u8 to add the offset correctly. We then cast the result to a wchar_t to return.
					const wchar_t* candidate = reinterpret_cast<const wchar_t*>(reinterpret_cast<const u8*>(pCandidateList) + pCandidateList->dwOffset[i]);

					Assertf(StringLength(candidate) <= TEXT_BUFFER_LENGTH, "Buffer not large enough to receive buffer!");
					safecpy(sm_ImeState[IME_CURRENT_STATE].m_Candidates[candidateIndex], candidate);
				}

				sm_ImeState[IME_CURRENT_STATE].m_NumCandidates = lastCandidateIndex - pCandidateList->dwPageStart;
				sm_ImeState[IME_CURRENT_STATE].m_TotalCandidates = pCandidateList->dwCount;
				sm_ImeState[IME_CURRENT_STATE].m_PageStart = pCandidateList->dwPageStart;
				sm_ImeState[IME_CURRENT_STATE].m_PageSize = maxCandidates;
				sm_ImeState[IME_CURRENT_STATE].m_SelectedCandidateIndex = pCandidateList->dwSelection - pCandidateList->dwPageStart;
				//unsigned selectedIndex = sm_ImeState[IME_CURRENT_STATE].m_SelectedCandidateIndex;

				SendImeEventToScui(msg, lParam, false, true, false);
			}
		}

		if(useHeap)
		{
			delete[] pCandidateList;
		}
	}

	ImmReleaseContext(hwnd, hImc);
}

void Rgsc_Input::ImeUpdateCompositionText(WindowHandle hwnd, const char* msg, const char* lParam)
{
	HIMC hImc = ImmGetContext(hwnd);

	// + sizeof(wchar_t) to buffer size as according to https://msdn.microsoft.com/en-us/library/windows/desktop/dd319104%28v=vs.85%29.aspx
	// it does not include NULL terminator.
	Assertf( ImmGetCompositionStringW(hImc, GCS_COMPSTR, NULL, 0) + sizeof(wchar_t) <= sizeof(sm_ImeState[IME_CURRENT_STATE].m_CompositionText),
		"Buffer not large enough for IME composition text!" );

	// ImmGetCompositionStringW does not add the NULL terminator so null array first.
	sysMemSet(sm_ImeState[IME_CURRENT_STATE].m_CompositionText, 0, sizeof(sm_ImeState[IME_CURRENT_STATE].m_CompositionText));

	// -sizeof(wchar_t) for NULL space.
	ImmGetCompositionStringW( hImc,
		GCS_COMPSTR,
		reinterpret_cast<wchar_t*>(sm_ImeState[IME_CURRENT_STATE].m_CompositionText),
		sizeof(sm_ImeState[IME_CURRENT_STATE].m_CompositionText) - sizeof(wchar_t) );

	Displayf("ImeUpdateCompositionText: %S", sm_ImeState[IME_CURRENT_STATE].m_CompositionText);

	ImmReleaseContext(hwnd, hImc);

	SendImeEventToScui(msg, lParam, true, false, false);
}

Rgsc_Input::ImeState Rgsc_Input::sm_ImeState[IME_NUM_STATES];
void Rgsc_Input::ImeBegin()
{
	sm_ImeState[IME_CURRENT_STATE].Clear();
}

void Rgsc_Input::ImeEnd()
{
	sm_ImeState[IME_CURRENT_STATE].Clear();
}

void Rgsc_Input::HandleImeEvent(ImeEvent eventId)
{
#if 0
	static DWORD page = 9;
	if(sm_ImeState[IME_CURRENT_STATE].m_InProgress == true)
	{
		switch(eventId)
		{
		case IME_PAGE_UP:
			{
#if 1
				page += 9;
				HIMC hImc = ImmGetContext(sm_hWindow);
				//HIMC hImc = sm_ImeState[IME_CURRENT_STATE].m_hImc;
				//BOOL res = ImmNotifyIME(hImc, NI_SETCANDIDATE_PAGESTART, 0, page);
			
				//BOOL res = ImmNotifyIME(hImc, NI_SELECTCANDIDATESTR, 0, 1);

				BOOL res = ImmGetOpenStatus(hImc);

				//res = ImmNotifyIME(hImc, NI_SELECTCANDIDATESTR, 0, 1);
				res = ImmNotifyIME(hImc, NI_SETCANDIDATE_PAGESTART, 0, page);

				//BOOL res = ImmNotifyIME(hImc, NI_COMPOSITIONSTR, CPS_CANCEL, 0);
				//BOOL res = ImmNotifyIME(hImc, NI_SETCANDIDATE_PAGESIZE, 0, 5);

				char buf[256] = {0};
				int iErrorCode = GetLastError();
				rlError("HandleImeEvent failed error code %d", iErrorCode);
				
				Displayf("res: %d", res);
				ImmReleaseContext(sm_hWindow, hImc);
#endif
			}
			break;
		case IME_PAGE_DOWN:
			{
				if(page >= 2)
				{
					page -= 1;
				}
				HIMC hImc = ImmGetContext(sm_hWindow);
				//HIMC hImc = sm_ImeState[IME_CURRENT_STATE].m_hImc;

				BOOL res = ImmNotifyIME(hImc, NI_SELECTCANDIDATESTR, 0, 2);
				Displayf("res: %d", res);
				ImmReleaseContext(sm_hWindow, hImc);
			}
			break;
		}
	}
#endif
}

void Rgsc_Input::ImeState::Clear()
{
	m_NumCandidates = 0u;
	m_SelectedCandidateIndex = Rgsc_Input::IME_INVALID_CANDIDATE_INDEX;
	sysMemSet(m_CompositionText, 0, sizeof(m_CompositionText));
	sysMemSet(m_Candidates,      0, sizeof(m_Candidates));
	m_InProgress = false;
	m_IsCandidatesWindowOpen = false;
}

void Rgsc_Input::ImeStartTextInput(WindowHandle hwnd, const char* msg, const char* lParam)
{
	sm_ImeState[IME_CURRENT_STATE].m_InProgress = true;
	ImeUpdateCompositionText(hwnd, msg, NULL);
}

void Rgsc_Input::ImeStopTextInput(WindowHandle /*hwnd*/, const char* msg, const char* lParam)
{
	sm_ImeState[IME_CURRENT_STATE].Clear();
}

inline const wchar_t* Rgsc_Input::ImeGetCompositionText()
{
	return sm_ImeState[IME_CURRENT_STATE].m_CompositionText;
}

inline bool Rgsc_Input::ImeIsInProgress()
{
	return sm_ImeState[IME_CURRENT_STATE].m_InProgress;
}

inline bool Rgsc_Input::ImeHasCandidates()
{
	return ImeGetNumberOfCandidates() > 0;
}

inline u32 Rgsc_Input::ImeGetNumberOfCandidates()
{
	return sm_ImeState[IME_CURRENT_STATE].m_NumCandidates;
}

inline u32 Rgsc_Input::ImeGetSelectedCandidateIndex()
{
	return sm_ImeState[IME_CURRENT_STATE].m_SelectedCandidateIndex;
}

inline const wchar_t* Rgsc_Input::ImeGetCandidateText(u32 candidateIndex)
{
	if(rlVerifyf(candidateIndex < ImeGetNumberOfCandidates(), "Invalid index %d, count %d!", candidateIndex, ImeGetNumberOfCandidates()))
	{
		return sm_ImeState[IME_CURRENT_STATE].m_Candidates[candidateIndex];
	}

	return reinterpret_cast<const wchar_t*>(L""); // Empty string.
}

inline Rgsc_Input::ImeState::ImeState()
{
	Clear();
}

LRESULT CALLBACK Rgsc_Input::ImeMessageHandler(WindowHandle hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	Displayf("ImeMessageHandler %u", msg);

	bool useOsUi = IME_USE_OS_UI;

	WNDPROC originalProc = FindWndProc(hwnd);
	
	switch (msg)
	{
	case WM_IME_SETCONTEXT:
		{
			// Actively disable the IME windows as they will kick us out of fullscreen. We will render these ourselves.
			Displayf("WM_IME_SETCONTEXT");

			if(!useOsUi)
			{
				lParam &= ~ISC_SHOWUIALL;
			}

			return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
		}
	case WM_IME_STARTCOMPOSITION:
		{
			Displayf("WM_IME_STARTCOMPOSITION");

			Rgsc_Input::ImeStartTextInput(hwnd, "WM_IME_STARTCOMPOSITION", NULL);
			return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
		}
	case WM_IME_COMPOSITION:
		{
			Displayf("WM_IME_COMPOSITION");

			if(lParam & GCS_RESULTSTR)
			{
				Displayf("    GCS_RESULTSTR");
				Rgsc_Input::ImeRetrieveTextResult(hwnd, "WM_IME_COMPOSITION", "GCS_RESULTSTR");
			}

			if(lParam & GCS_COMPSTR)
			{
				Displayf("    GCS_COMPSTR");
				Rgsc_Input::ImeUpdateCompositionText(hwnd, "WM_IME_COMPOSITION", "GCS_COMPSTR");
			}

			return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
		}
	case WM_IME_ENDCOMPOSITION:
		{
			Displayf("WM_IME_ENDCOMPOSITION");

			Rgsc_Input::ImeStopTextInput(hwnd, "WM_IME_ENDCOMPOSITION", NULL);
			SendImeEventToScui("WM_IME_ENDCOMPOSITION", NULL, false, false, false);
			return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
		}
	case WM_IME_NOTIFY:
		Displayf("WM_IME_NOTIFY");

		switch(wParam)
		{
		case IMN_OPENCANDIDATE:
			Displayf("IMN_OPENCANDIDATE");
			sm_ImeState[IME_CURRENT_STATE].m_IsCandidatesWindowOpen = true;
			Rgsc_Input::ImeUpdateCandidateTexts(hwnd, "WM_IME_NOTIFY", "IMN_OPENCANDIDATE");
			return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);

		case IMN_CHANGECANDIDATE:
			Displayf("IMN_CHANGECANDIDATE");
			Rgsc_Input::ImeUpdateCandidateTexts(hwnd, "WM_IME_NOTIFY", "IMN_CHANGECANDIDATE");
			return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);

		case IMN_CLOSECANDIDATE:
			Displayf("IMN_CLOSECANDIDATE");
			sm_ImeState[IME_CURRENT_STATE].m_IsCandidatesWindowOpen = false;
			SendImeEventToScui("WM_IME_NOTIFY", "IMN_CLOSECANDIDATE", false, false, false);
			return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);

		default:
			return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
		}
	}

	return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
}
#endif // IME_TEXT_INPUT

UINT Rgsc_Input::MapRgscPlatformMsgToWindowsMsg(RgscPlatformMessageHandler::RgscPlatformMsg msg)
{
	switch(msg)
	{
	case RgscPlatformMessageHandler::RGSC_MSG_DEVICECHANGE:
		return WM_DEVICECHANGE;
	case RgscPlatformMessageHandler::RGSC_MSG_MOUSEMOVE:
		return WM_MOUSEMOVE;
	case RgscPlatformMessageHandler::RGSC_MSG_MOUSEWHEEL:
		return WM_MOUSEWHEEL;
	case RgscPlatformMessageHandler::RGSC_MSG_LBUTTONDOWN:
		return WM_LBUTTONDOWN;
	case RgscPlatformMessageHandler::RGSC_MSG_LBUTTONDBLCLK:
		return WM_LBUTTONDBLCLK;
	case RgscPlatformMessageHandler::RGSC_MSG_LBUTTONUP:
		return WM_LBUTTONUP;
	case RgscPlatformMessageHandler::RGSC_MSG_RBUTTONDOWN:
		return WM_RBUTTONDOWN;
	case RgscPlatformMessageHandler::RGSC_MSG_RBUTTONUP:
		return WM_RBUTTONUP;
	case RgscPlatformMessageHandler::RGSC_MSG_RBUTTONDBLCLK:
		return WM_RBUTTONDBLCLK;
	case RgscPlatformMessageHandler::RGSC_MSG_MBUTTONDOWN:
		return WM_MBUTTONDOWN;
	case RgscPlatformMessageHandler::RGSC_MSG_MBUTTONDBLCLK:
		return WM_MBUTTONDBLCLK;
	case RgscPlatformMessageHandler::RGSC_MSG_MBUTTONUP:
		return WM_MBUTTONUP;
	case RgscPlatformMessageHandler::RGSC_MSG_KEYUP:
		return WM_KEYUP;
	case RgscPlatformMessageHandler::RGSC_MSG_KEYDOWN:
		return WM_KEYDOWN;
	case RgscPlatformMessageHandler::RGSC_MSG_CHAR:
		return WM_CHAR;
	case RgscPlatformMessageHandler::RGSC_MSG_SETCURSOR:
		return WM_SETCURSOR;
	case RgscPlatformMessageHandler::RGSC_MSG_IME_SETCONTEXT:
		return WM_IME_SETCONTEXT;
	case RgscPlatformMessageHandler::RGSC_MSG_IME_STARTCOMPOSITION:
		return WM_IME_STARTCOMPOSITION;
	case RgscPlatformMessageHandler::RGSC_MSG_IME_COMPOSITION:
		return WM_IME_COMPOSITION;
	case RgscPlatformMessageHandler::RGSC_MSG_IME_ENDCOMPOSITION:
		return WM_IME_ENDCOMPOSITION;
	case RgscPlatformMessageHandler::RGSC_MSG_IME_NOTIFY:
		return WM_IME_NOTIFY;
	case RgscPlatformMessageHandler::RGSC_MSG_IME_CHAR:
		return WM_IME_CHAR;			
	case RgscPlatformMessageHandler::RGSC_MSG_TABLET_QUERYSYSTEMGESTURESTATUS: 
		return WM_TABLET_QUERYSYSTEMGESTURESTATUS;
	case RgscPlatformMessageHandler::RGSC_MSG_MOUSEFIRST: // Same as WM_MOUSEMOVE
		return WM_MOUSEFIRST;
	case RgscPlatformMessageHandler::RGSC_MSG_MOUSELAST:
		return WM_MOUSELAST;
	case RgscPlatformMessageHandler::RGSC_MSG_NCCALCSIZE:
		return WM_NCCALCSIZE;
	case RgscPlatformMessageHandler::RGSC_MSG_NCHITTEST:
		return WM_NCHITTEST;
	}

	return WM_NULL;
}

RgscPlatformMessageHandler::RgscPlatformMsg Rgsc_Input::MapWindowsMsgToRgscPlatformMsg(UINT iMsg)
{
	switch(iMsg)
	{
	case WM_DEVICECHANGE:
		return RgscPlatformMessageHandler::RGSC_MSG_DEVICECHANGE;
	case WM_MOUSEMOVE:
		return RgscPlatformMessageHandler::RGSC_MSG_MOUSEMOVE;
	case WM_MOUSEWHEEL:
		return RgscPlatformMessageHandler::RGSC_MSG_MOUSEWHEEL;
	case WM_LBUTTONDOWN:
		return RgscPlatformMessageHandler::RGSC_MSG_LBUTTONDOWN;
	case WM_LBUTTONDBLCLK:
		return RgscPlatformMessageHandler::RGSC_MSG_LBUTTONDBLCLK;
	case WM_LBUTTONUP:
		return RgscPlatformMessageHandler::RGSC_MSG_LBUTTONUP;
	case WM_RBUTTONDOWN:
		return RgscPlatformMessageHandler::RGSC_MSG_RBUTTONDOWN;
	case WM_RBUTTONUP:
		return RgscPlatformMessageHandler::RGSC_MSG_RBUTTONUP;
	case WM_RBUTTONDBLCLK:
		return RgscPlatformMessageHandler::RGSC_MSG_RBUTTONDBLCLK;
	case WM_MBUTTONDOWN:
		return RgscPlatformMessageHandler::RGSC_MSG_MBUTTONDOWN;
	case WM_MBUTTONDBLCLK:
		return RgscPlatformMessageHandler::RGSC_MSG_MBUTTONDBLCLK;
	case WM_MBUTTONUP:
		return RgscPlatformMessageHandler::RGSC_MSG_MBUTTONUP;
	case WM_KEYUP:
		return RgscPlatformMessageHandler::RGSC_MSG_KEYUP;
	case WM_KEYDOWN:
		return RgscPlatformMessageHandler::RGSC_MSG_KEYDOWN;
	case WM_CHAR:
		return RgscPlatformMessageHandler::RGSC_MSG_CHAR;
	case WM_SETCURSOR:
		return RgscPlatformMessageHandler::RGSC_MSG_SETCURSOR;
	case WM_IME_SETCONTEXT:
		return RgscPlatformMessageHandler::RGSC_MSG_IME_SETCONTEXT;
	case WM_IME_STARTCOMPOSITION:
		return RgscPlatformMessageHandler::RGSC_MSG_IME_STARTCOMPOSITION;
	case WM_IME_COMPOSITION:
		return RgscPlatformMessageHandler::RGSC_MSG_IME_COMPOSITION;
	case WM_IME_ENDCOMPOSITION:
		return RgscPlatformMessageHandler::RGSC_MSG_IME_ENDCOMPOSITION;
	case WM_IME_NOTIFY:
		return RgscPlatformMessageHandler::RGSC_MSG_IME_NOTIFY;
	case WM_IME_CHAR:
		return RgscPlatformMessageHandler::RGSC_MSG_IME_CHAR;			
	case WM_TABLET_QUERYSYSTEMGESTURESTATUS: 
		return RgscPlatformMessageHandler::RGSC_MSG_TABLET_QUERYSYSTEMGESTURESTATUS;
	//case WM_MOUSEFIRST: // Same as WM_MOUSEMOVE
	//	return RgscPlatformMessageHandlerV1::RGSC_MSG_MOUSEFIRST;
	case WM_MOUSELAST:
		return RgscPlatformMessageHandler::RGSC_MSG_MOUSELAST;
	case WM_NCCALCSIZE:
		return RgscPlatformMessageHandler::RGSC_MSG_NCCALCSIZE;
	case WM_NCHITTEST:
		return RgscPlatformMessageHandler::RGSC_MSG_NCHITTEST;
	}

	return RgscPlatformMessageHandler::RGSC_MSG_NULL;
}

LRESULT Rgsc_Input::MessageHandlerReturnValue(WNDPROC originalProc, WindowHandle hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	RgscPlatformMessageHandler::RgscPlatformMsg rgscMsg = MapWindowsMsgToRgscPlatformMsg(iMsg);
	return GetRgscConcreteInstance()->_GetUiInterface()->TryForwardRgscPlatformMessage(originalProc, hwnd, rgscMsg, iMsg, wParam, lParam);
}

void Rgsc_Input::SetKeyboardMessageForwarding(bool bEnabled)
{
	sm_bKeyboardEventForwarding = bEnabled;
}

void Rgsc_Input::SetMouseMessageForwarding(bool bEnabled)
{
	sm_bMouseEventForwarding = bEnabled;
}

LRESULT CALLBACK Rgsc_Input::WindowsMessageHandler(WindowHandle hwnd, UINT msg, WPARAM wParam, LPARAM lParam) 
{
	//Displayf("WindowsMessageHandler %u", msg);

	bool isUiVisible = GetRgscConcreteInstance()->_GetUiInterface()->IsVisible();
	bool isMac = GetRgscConcreteInstance()->IsMac();

	WNDPROC originalProc = FindWndProc(hwnd);

	WPARAM uiHotkey = VK_HOME;
	WPARAM altUiHotKey = VK_HOME;
	if(isMac)
	{
		// many new Mac notebooks don't have a Home key so we're also accepting the backslash key ('\') instead 
		altUiHotKey = VK_OEM_5; // backslash key
	}

	if (msg == WM_DEVICECHANGE)
	{
		{
			SYS_CS_SYNC(sm_DeviceChangeCs);
			sm_uTimeOfLastDeviceChange = sysTimer::GetSystemMsTime();
		}

		return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
	}

	if(isUiVisible == false)
	{
		if((msg == WM_KEYUP) && ((wParam == uiHotkey) || (wParam == altUiHotKey)) && (GetModifiers() == 0))
		{
			// recapture mouse pos
			POINT lpPoint;
			BOOL bResult = ::GetCursorPos(&lpPoint);
			if (bResult == TRUE)
			{
				sm_TargetX = (float)lpPoint.x;
				sm_TargetY = (float)lpPoint.y;
			}

			SetIsUsingController(false);
			ioEventQueue::Queue(ioEvent::IO_SOCIAL_CLUB_OPEN, 0, 0, 0);
			
			// specifically does not use our map. The game can disable SCUI hotkey instead.
			return 0;
		}

		return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
	}

	// Ignore mouse events by default if we're not forwarding them.
	// Touch builds can ignore touch events if we've recently touched.
	bool ignoreMouseEvents = (sm_bMouseEventForwarding == false);

#if SUPPORT_TOUCH_INPUT	
	unsigned int elaspedTimeSinceTouch = 0;

	if(m_TouchStartTime > 0)
	{
		elaspedTimeSinceTouch = timeGetTime() - m_TouchStartTime;
		if(elaspedTimeSinceTouch < sm_TimeToIgnoreMouseAfterTouch)
		{
			ignoreMouseEvents = true;
		}
		else
		{
			m_TouchStartTime = 0;
		}
	}
#endif

	switch (msg)
	{
#if IME_TEXT_INPUT
	case WM_IME_SETCONTEXT:
	case WM_IME_STARTCOMPOSITION:
	case WM_IME_COMPOSITION:
	case WM_IME_ENDCOMPOSITION:
	case WM_IME_NOTIFY:
	case WM_IME_CHAR:
		{
			return ImeMessageHandler(hwnd, msg, wParam, lParam);
		}
#endif // IME_TEXT_INPUT

	case WM_MOUSEMOVE:
		{
			if(ignoreMouseEvents == false)
			{
				int newMouseX = (int)(short)LOWORD(lParam);
				int newMouseY = (int)(short)HIWORD(lParam);

				if((newMouseX != s_OldMouseX) || (newMouseY != s_OldMouseY)) 
				{
					HandleMouseMove(newMouseX, newMouseY);
					ioEventQueue::Queue(ioEvent::IO_MOUSE_MOVE, msg, wParam, lParam);
				}
			}
		}
		return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
	case WM_MOUSEWHEEL:
		if(ignoreMouseEvents == false)
		{
			ioEventQueue::Queue(ioEvent::IO_MOUSE_WHEEL, 0, 0, ((short) HIWORD(wParam) / WHEEL_DELTA));
		}
		return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
	case WM_TABLET_QUERYSYSTEMGESTURESTATUS:
		{
			return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
		}
		break;
#if SUPPORT_TOUCH_INPUT
	case WM_TOUCH:
		{
			Displayf("WM_TOUCH");
			SetIsUsingController(false);
			return OnTouch(hwnd, wParam, lParam);
		}
#endif
	case WM_LBUTTONDOWN:
		::SetCapture((HWND)hwnd);
		if(ignoreMouseEvents == false)
		{
			ioEventQueue::Queue(ioEvent::IO_MOUSE_LEFT_DOWN, msg, wParam, lParam);
		}
		return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
	case WM_LBUTTONDBLCLK:
		if(ignoreMouseEvents == false)
		{
			ioEventQueue::Queue(ioEvent::IO_MOUSE_LEFT_DBLCLK, msg, wParam, lParam);
		}
		return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);

	case WM_LBUTTONUP:
		if(::GetCapture() == (HWND)hwnd)
		{
			ReleaseCapture();
		}

		if(ignoreMouseEvents == false)
		{
			ConsumeControllerEvent(ioEvent::IO_MOUSE_LEFT_UP);
			ioEventQueue::Queue(ioEvent::IO_MOUSE_LEFT_UP, msg, wParam, lParam);
		}
		return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
	case WM_RBUTTONDOWN:
		if(ignoreMouseEvents == false)
		{
			SetIsUsingController(false);
			ioEventQueue::Queue(ioEvent::IO_MOUSE_RIGHT_DOWN, msg, wParam, lParam);
		}
		return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
	case WM_RBUTTONUP:
		if(ignoreMouseEvents == false)
		{
			SetIsUsingController(false);
			ioEventQueue::Queue(ioEvent::IO_MOUSE_RIGHT_UP, msg, wParam, lParam);
		}
		return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
	case WM_MBUTTONDOWN:
		if(ignoreMouseEvents == false)
		{
			SetIsUsingController(false);
			ioEventQueue::Queue(ioEvent::IO_MOUSE_MIDDLE_DOWN, msg, wParam, lParam);
		}
		return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
	case WM_MBUTTONUP:
		if(ignoreMouseEvents == false)
		{
			SetIsUsingController(false);
			ioEventQueue::Queue(ioEvent::IO_MOUSE_MIDDLE_UP, msg, wParam, lParam);
		}
		return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
	case WM_KEYDOWN:
		{
			if (sm_bKeyboardEventForwarding)
			{
				__int64 mods = Rgsc_Input::GetModifiers();
				ioEventQueue::Queue(ioEvent::IO_KEY_DOWN, msg, wParam, lParam, mods);
			}

			return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
		}
	case WM_KEYUP:
		{
			// Decrement the controller events to consume, if < 0, set back into KB/Mouse Mode
			ConsumeControllerEvent(ioEvent::IO_KEY_UP);

			bool closeUi = isUiVisible && (msg == WM_KEYUP) && ((wParam == VK_ESCAPE) || (((wParam == uiHotkey) || (wParam == altUiHotKey)) && (GetModifiers() == 0)));
			if(closeUi)
			{				
				ioEventQueue::Queue(ioEvent::IO_SOCIAL_CLUB_CLOSE, 0, 0, wParam == VK_ESCAPE ? METHOD_ESCAPE : METHOD_HOME);
				if (sm_bKeyboardEventForwarding)
				{
					SetIsUsingController(false);
				}
			}
			else if (sm_bKeyboardEventForwarding)
			{
				__int64 mods = Rgsc_Input::GetModifiers();
				ioEventQueue::Queue(ioEvent::IO_KEY_UP, msg, wParam, lParam, mods);
			}
			return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
		}
	case WM_CHAR:
		{
			SetIsUsingController(false);
	// 		const unsigned char ASCII_BACKSPACE = 8;
	// 		const unsigned char ASCII_TAB       = 9;
	// 		const unsigned char ASCII_ENTER     = 13;
	// 		const unsigned char ASCII_ESCAPE    = 27;
			const unsigned char ASCII_DELETE    = 127;

			// anything under 32 is handled by the WM_KEYDOWN event
			if(wParam >= 32 && wParam != ASCII_DELETE && sm_bKeyboardEventForwarding)
			{
				ioEventQueue::Queue(ioEvent::IO_KEY_CHAR, msg, wParam, lParam);
			}

			return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
		}
	case WM_SETCURSOR:
		{
			// checking for NULL pointers here due to race condition at shutdown
			// found by WER crash reports.
			Rgsc* rgscInst = GetRgscConcreteInstance();
			if(rgscInst)
			{
				RgscUi* rgscUi = rgscInst->_GetUiInterface();
				if(rgscUi)
				{
					GetRgscConcreteInstance()->_GetUiInterface()->SetOsCursorShape();
				}
			}
			return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
		}
	default:
		{
			return MessageHandlerReturnValue(originalProc, hwnd, msg, wParam, lParam);
		}
	}
}

void Rgsc_Input::SendInputMethodToScui(Mode inputMode, unsigned controllerIndex)
{
	// {"InputMethod":#,"PadInfo":[###]} = 33, round up to 64
	const int WRAPPER_BUF_SIZE = 64; 

	// {"Button":##,"Text":"################################"}, = RGSC_MAX_BUTTON_TEXT_BUF_SIZE(64) + 32(slush)
	const int PER_BUTTON_BUF_SIZE = RgscGamepad::RGSC_MAX_BUTTON_TEXT_BUF_SIZE + 32; 
	const int JSON_BUF_SIZE = WRAPPER_BUF_SIZE + (PER_BUTTON_BUF_SIZE * IGamepadV1::RGSC_NUMBUTTONS);

	char json[JSON_BUF_SIZE] = {0};

	bool bIsUsingController = IsUsingController();

	RsonWriter rw;
	rw.Init(json, sizeof(json), RSON_FORMAT_JSON);
	rw.Begin(NULL, NULL);
	rw.WriteInt("InputMethod", bIsUsingController ? RGSC_IM_CONTROLLER : RGSC_IM_KEYBOARDMOUSE);

	if (bIsUsingController)
	{
		IGamepad* pad = GetRgscConcreteInstance()->_GetGamepadManager()->GetPad(inputMode, controllerIndex);

#if SUPPORT_SCUI_GAMEPAD_REMAPPING
		IGamepadV3* padV3 = NULL;
		if (pad && pad->QueryInterface(IID_IGamepadV3, (void**)&padV3) == RGSC_OK)
		{
			rw.BeginArray("PadInfo", NULL);

			for (int i = 0; i < IGamepadV1::RGSC_NUMBUTTONS; i++)
			{
				rw.Begin(NULL, NULL);
				rw.WriteInt("Button", i);
				rw.WriteString("Text", padV3->GetButtonText((IGamepadV1::PadButtonIndex)i));
				rw.End();
			}
			rw.End();
		}
#endif

		IGamepadV4* padV4 = NULL;
		if (pad && pad->QueryInterface(IID_IGamepadV4, (void**)&padV4) == RGSC_OK)
		{
			rw.WriteInt("PadType", padV4->GetType());
			rw.WriteString("PadName", padV4->GetName());
		}
	}
	rw.End();

	bool succeeded = false;
	rgsc::Script::JsSetInputMethod(&succeeded, json);
}

void Rgsc_Input::SetIsUsingController(bool bIsUsingController, Mode inputMode, unsigned controllerIndex)
{
	SYS_CS_SYNC(sm_Cs);
	AssertMsg(!(bIsUsingController && inputMode == IGamepadManagerLatestVersion::INPUTMODE_INVALID), "SetIsUsingController used incorrectly");

	bool bWasUsingController = IsUsingController();

	// Clears are called with an invalid input mode to clear all input modes
	if (inputMode == IGamepadManagerLatestVersion::INPUTMODE_INVALID)
	{
		// reset controller indices for each mode
		s_ControllerIndex[IGamepadManagerLatestVersion::INPUTMODE_D3D] = controllerIndex;
		s_ControllerIndex[IGamepadManagerLatestVersion::INPUTMODE_XINPUT] = controllerIndex;
		s_ControllerIndex[IGamepadManagerLatestVersion::INPUTMODE_MARSHALLED] = controllerIndex;

		// reset 'in use' flag for each mode.
		m_IsUsingController[IGamepadManagerLatestVersion::INPUTMODE_D3D] = bIsUsingController;
		m_IsUsingController[IGamepadManagerLatestVersion::INPUTMODE_XINPUT] = bIsUsingController;
		m_IsUsingController[IGamepadManagerLatestVersion::INPUTMODE_MARSHALLED] = bIsUsingController;
	}
	else
	{
		// Set the index and 'in use' flag for the specified mode.
		s_ControllerIndex[inputMode] = controllerIndex;
		m_IsUsingController[inputMode] = bIsUsingController;
	}

	if(bWasUsingController != bIsUsingController)
	{
		// get mouse pos
		POINT lpPoint;
		BOOL bResult = ::GetCursorPos(&lpPoint);
		if (bResult == TRUE)
		{
			sm_TargetX = (float)lpPoint.x;
			sm_TargetY = (float)lpPoint.y;
		}

		// Get the current mouse coords
		if (bIsUsingController)
		{
			Displayf("Switching input to %s Controller %d", GetInputString(inputMode), controllerIndex);

			// reset timestep
			m_TimeStep.SetTime(sysTimer::GetSystemMsTime());
		}
		else
		{
			Displayf("Switching input to Mouse/Keyboard");
			ClearControllerEvent();

			// reset mouse pos
			sm_TargetX = 0;
			sm_TargetY = 0;
		}

		m_MouseMoveCount = 0;

		SendInputMethodToScui(inputMode, controllerIndex);
	}
}

bool Rgsc_Input::IsUsingController()
{
	return GetControllerInputMode() != IGamepadManagerLatestVersion::INPUTMODE_INVALID;
}

bool Rgsc_Input::IsButtonDown(int button)
{
	if (s_AllControllers.Held & button)
		return true;

	return false;
}

Mode Rgsc_Input::GetControllerInputMode()
{
	if (m_IsUsingController[IGamepadManagerLatestVersion::INPUTMODE_D3D])
		return IGamepadManagerLatestVersion::INPUTMODE_D3D;
	if (m_IsUsingController[IGamepadManagerLatestVersion::INPUTMODE_XINPUT])
		return IGamepadManagerLatestVersion::INPUTMODE_XINPUT;
	if (m_IsUsingController[IGamepadManagerLatestVersion::INPUTMODE_MARSHALLED])
		return IGamepadManagerLatestVersion::INPUTMODE_MARSHALLED;

	return IGamepadManagerLatestVersion::INPUTMODE_INVALID;
}

int Rgsc_Input::GetControllerIndex(Mode inputMode)
{
	if (inputMode < IGamepadManagerLatestVersion::INPUTMODE_MAX)
	{
		return s_ControllerIndex[inputMode];
	}

	return 0;
}

int Rgsc_Input::CountPads()
{
	SYS_CS_SYNC(sm_Cs);
	return GetRgscConcreteInstance()->_GetGamepadManager()->CountPads();
}

void Rgsc_Input::ClearControllerEvent()
{
	SYS_CS_SYNC(sm_Cs);
	m_ControllerEventsToConsume = 0;
}

void Rgsc_Input::ConsumeControllerEvent(ioEvent::ioEventType eventType)
{
	if (ioEvent::IsMouseEvent(eventType) && !IsSimulatingMouseEvents())
		return;
	else if (ioEvent::IsKeyboardEvent(eventType) && !IsSimulatingKeyboardEvents())
		return;

	SYS_CS_SYNC(sm_Cs);
	m_ControllerEventsToConsume--;
	if (m_ControllerEventsToConsume < 0)
	{
		SetIsUsingController(false);
		m_ControllerEventsToConsume = 0;
	}
}

void Rgsc_Input::ProduceControllerEvent(ioEvent::ioEventType eventType)
{
	if (ioEvent::IsMouseEvent(eventType) && !IsSimulatingMouseEvents())
		return;
	else if (ioEvent::IsKeyboardEvent(eventType) && !IsSimulatingKeyboardEvents())
		return;

	SYS_CS_SYNC(sm_Cs);
	m_ControllerEventsToConsume++;
}

#if !__NO_OUTPUT
const char* Rgsc_Input::GetInputString(int inputMode)
{
	switch(inputMode)
	{
	case IGamepadManagerLatestVersion::INPUTMODE_D3D:
		return "D3D";
	case IGamepadManagerLatestVersion::INPUTMODE_XINPUT:
		return "XINPUT";
	case IGamepadManagerLatestVersion::INPUTMODE_MARSHALLED:
		return "MARSHALLED";
	default:
		return "UNKNOWN";
	}
}
#endif

} // namespace rgsc
