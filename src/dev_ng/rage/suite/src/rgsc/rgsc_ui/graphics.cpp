#include "graphics.h"
#include "graphics_d3d9.h"
#include "graphics_d3d11.h"
#include "tiledbuffer.h"
#include "system/new.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <d3d9.h>
#include <d3d11.h>
#pragma warning(pop)

#pragma comment(lib,"d3dx11.lib")
#pragma comment(lib,"d3dx9.lib")

#pragma comment(lib,"d3dcompiler.lib")

namespace rgsc
{

Rgsc_Graphics::Rgsc_Graphics()
: m_Width(0)
, m_Height(0)
, m_Tiles(NULL)
{
}

Rgsc_Graphics::~Rgsc_Graphics()
{
	if(m_Tiles)
	{
		delete m_Tiles;
		m_Tiles = NULL;
	}
}

Rgsc_Graphics* Rgsc_Graphics::DeviceFactory(IUnknown *device, void *params, const RenderRect& rect)
{
	if(device && params)
	{
		void* device_d3d;
		HRESULT hr = device->QueryInterface(__uuidof(ID3D11Device), &device_d3d);
		if(SUCCEEDED(hr))
		{
			return rage_new Rgsc_Graphics_D3D11(static_cast<ID3D11Device*>(device_d3d), params, rect);
		}

		hr = device->QueryInterface(__uuidof(IDirect3DDevice9), &device_d3d);
		if(SUCCEEDED(hr))
		{
			return rage_new Rgsc_Graphics_D3D9(static_cast<IDirect3DDevice9*>(device_d3d), params, rect);
		}
	}
	return NULL;
}

float Rgsc_Graphics::MapNumFromRangeToRange(float fValue, float fMin, float fMax, float fMinValue, float fMaxValue) 
{
	if ( fValue < fMin )
		fValue = fMin;

	if ( fValue > fMax )
		fValue = fMax;

	// From: converted fValue to fPercent using fMin and fMax 
	float fRangeA = fMax - fMin;
	float fPercent = ( fValue - fMin ) / fRangeA;

	// To: convert fPercent to value between fMinValue and fMaxValue
	float fRangeB = fMaxValue - fMinValue;
	return  fMinValue + ( fPercent * fRangeB ); 
}

} // namespace rgsc
