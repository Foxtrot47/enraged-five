#ifndef C2JS_HELPER_H
#define C2JS_HELPER_H

#include "variant.h"
#include "weakstring.h"
#include "stringutil.h"
#include "rgsc_ui.h"
#include "script.h"
#include "string/string.h"

namespace rgsc {

	namespace c2jsWrapper {

		template<size_t _Size, typename T>
		struct Arg {
			inline static void ToString(wchar_t (&s)[_Size], T t) { return UnsuportedNativeFunctionArgumentType; }
		};

		template<size_t _Size>
		struct Arg<_Size, float> {
			inline static void ToString(wchar_t (&s)[_Size], float f) {safecatf(s, L"%.20f", f);}
		};

		template<size_t _Size>
		struct Arg<_Size, int> {
			inline static void ToString(wchar_t (&s)[_Size], int i) {safecatf(s, L"%d", i);}
		};

		template<size_t _Size>
		struct Arg<_Size, unsigned int> {
			inline static void ToString(wchar_t (&s)[_Size], unsigned int u) {safecatf(s, L"%u", u);}
		};

 		template<size_t _Size>
 		struct Arg<_Size, bool> {
 			inline static void ToString(wchar_t (&s)[_Size], bool b) {safecatf(s, L"%ls", b ? L"true" : L"false");}
 		};

 		template<size_t _Size>
		struct Arg<_Size, RgscWideString> {
			inline static void ToString(wchar_t (&s)[_Size], RgscWideString arg)
			{
				RgscVariant v(arg);
				RgscWideString wsJson = rgsc::toJSON(v);
				safecatf(s, L"%ls", wsJson.data());
				rgsc::toJSON_free(wsJson);
			}
 		};

		template<size_t _Size>
		struct Arg<_Size, RgscUtf8String> {
			inline static void ToString(wchar_t (&s)[_Size], RgscUtf8String arg)
			{
				RgscVariant v(arg.data());
				RgscWideString wsJson = rgsc::toJSON(v);
				safecatf(s, L"%ls", wsJson.data());
				rgsc::toJSON_free(wsJson);
			}
		};

		template<size_t _Size>
		struct Arg<_Size, const char*> {
			inline static void ToString(wchar_t (&s)[_Size], const char* arg)
			{
				RgscVariant v(arg);
				RgscWideString wsJson = rgsc::toJSON(v);
				safecatf(s, L"%ls", wsJson.data());
				rgsc::toJSON_free(wsJson);
			}
		};

		// Non-void return types
		template <typename RT>
		inline void ParamsToString(wchar_t* wszJavascript, RT(*fn)())
		{
			Unsupported
			//AssignReturnValue(s, fn());
		}

		template <typename RT, typename T0>
		inline void ParamsToString(wchar_t* wszJavascript, RT(*fn)(T0 t0))
		{
			Unsupported
// 			int N = 0;
// 			T0 t0 = Arg<T0>::Value(s, N);
// 			AssignReturnValue(s, fn(t0));
		}

		template <size_t _Size>
		inline void AppendComma(wchar_t (&s)[_Size])
		{
			safecat(s, L", ");
		}

		// Void return types
		template <size_t _Size>
		inline void ParamsToString(wchar_t (&s)[_Size], void(*fn)(), va_list vl)
		{
			// no parameters, nothing to do
		}

		template <size_t _Size, typename T0>
		inline void ParamsToString(wchar_t (&s)[_Size], void(*fn)(T0), va_list vl)
		{
			Arg<_Size, T0>::ToString(s, va_arg(vl, T0));
		}

		template <size_t _Size, typename T0, typename T1>
		inline void ParamsToString(wchar_t (&s)[_Size], void(*fn)(T0, T1), va_list vl)
		{
			Arg<_Size, T0>::ToString(s, va_arg(vl, T0)); AppendComma(s);
			Arg<_Size, T1>::ToString(s, va_arg(vl, T1));
		}

		template <size_t _Size, typename T0, typename T1, typename T2>
		inline void ParamsToString(wchar_t (&s)[_Size], void(*fn)(T0, T1, T2), va_list vl)
		{
			Arg<_Size, T0>::ToString(s, va_arg(vl, T0)); AppendComma(s);
			Arg<_Size, T1>::ToString(s, va_arg(vl, T1)); AppendComma(s);
			Arg<_Size, T2>::ToString(s, va_arg(vl, T2));
		}

		template <size_t _Size, typename T0, typename T1, typename T2, typename T3>
		inline void ParamsToString(wchar_t (&s)[_Size], void(*fn)(T0, T1, T2, T3), va_list vl)
		{
			Arg<_Size, T0>::ToString(s, va_arg(vl, T0)); AppendComma(s);
			Arg<_Size, T1>::ToString(s, va_arg(vl, T1)); AppendComma(s);
			Arg<_Size, T2>::ToString(s, va_arg(vl, T2)); AppendComma(s);
			Arg<_Size, T3>::ToString(s, va_arg(vl, T3));
		}

		template <size_t _Size, typename T0, typename T1, typename T2, typename T3, typename T4>
		inline void ParamsToString(wchar_t (&s)[_Size], void(*fn)(T0, T1, T2, T3, T4), va_list vl)
		{
			Arg<_Size, T0>::ToString(s, va_arg(vl, T0)); AppendComma(s);
			Arg<_Size, T1>::ToString(s, va_arg(vl, T1)); AppendComma(s);
			Arg<_Size, T2>::ToString(s, va_arg(vl, T2)); AppendComma(s);
			Arg<_Size, T3>::ToString(s, va_arg(vl, T3)); AppendComma(s);
			Arg<_Size, T4>::ToString(s, va_arg(vl, T4));
		}

		template <size_t _Size, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5>
		inline void ParamsToString(wchar_t (&s)[_Size], void(*fn)(T0, T1, T2, T3, T4, T5), va_list vl)
		{
			Arg<_Size, T0>::ToString(s, va_arg(vl, T0)); AppendComma(s);
			Arg<_Size, T1>::ToString(s, va_arg(vl, T1)); AppendComma(s);
			Arg<_Size, T2>::ToString(s, va_arg(vl, T2)); AppendComma(s);
			Arg<_Size, T3>::ToString(s, va_arg(vl, T3)); AppendComma(s);
			Arg<_Size, T4>::ToString(s, va_arg(vl, T4)); AppendComma(s);
			Arg<_Size, T5>::ToString(s, va_arg(vl, T5));
		}

		template <size_t _Size, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
		inline void ParamsToString(wchar_t (&s)[_Size], void(*fn)(T0, T1, T2, T3, T4, T5, T6), va_list vl)
		{
			Arg<_Size, T0>::ToString(s, va_arg(vl, T0)); AppendComma(s);
			Arg<_Size, T1>::ToString(s, va_arg(vl, T1)); AppendComma(s);
			Arg<_Size, T2>::ToString(s, va_arg(vl, T2)); AppendComma(s);
			Arg<_Size, T3>::ToString(s, va_arg(vl, T3)); AppendComma(s);
			Arg<_Size, T4>::ToString(s, va_arg(vl, T4)); AppendComma(s);
			Arg<_Size, T5>::ToString(s, va_arg(vl, T5)); AppendComma(s);
			Arg<_Size, T6>::ToString(s, va_arg(vl, T6));
		}

		template <size_t _Size, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
		inline void ParamsToString(wchar_t (&s)[_Size], void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7), va_list vl)
		{
			Arg<_Size, T0>::ToString(s, va_arg(vl, T0)); AppendComma(s);
			Arg<_Size, T1>::ToString(s, va_arg(vl, T1)); AppendComma(s);
			Arg<_Size, T2>::ToString(s, va_arg(vl, T2)); AppendComma(s);
			Arg<_Size, T3>::ToString(s, va_arg(vl, T3)); AppendComma(s);
			Arg<_Size, T4>::ToString(s, va_arg(vl, T4)); AppendComma(s);
			Arg<_Size, T5>::ToString(s, va_arg(vl, T5)); AppendComma(s);
			Arg<_Size, T6>::ToString(s, va_arg(vl, T6)); AppendComma(s);
			Arg<_Size, T7>::ToString(s, va_arg(vl, T7));
		}

		template <size_t _Size, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
		inline void ParamsToString(wchar_t (&s)[_Size], void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8), va_list vl)
		{
			Arg<_Size, T0>::ToString(s, va_arg(vl, T0)); AppendComma(s);
			Arg<_Size, T1>::ToString(s, va_arg(vl, T1)); AppendComma(s);
			Arg<_Size, T2>::ToString(s, va_arg(vl, T2)); AppendComma(s);
			Arg<_Size, T3>::ToString(s, va_arg(vl, T3)); AppendComma(s);
			Arg<_Size, T4>::ToString(s, va_arg(vl, T4)); AppendComma(s);
			Arg<_Size, T5>::ToString(s, va_arg(vl, T5)); AppendComma(s);
			Arg<_Size, T6>::ToString(s, va_arg(vl, T6)); AppendComma(s);
			Arg<_Size, T7>::ToString(s, va_arg(vl, T7)); AppendComma(s);
			Arg<_Size, T8>::ToString(s, va_arg(vl, T8));
		}

		template <size_t _Size, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
		inline void ParamsToString(wchar_t (&s)[_Size], void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9), va_list vl)
		{
			Arg<_Size, T0>::ToString(s, va_arg(vl, T0)); AppendComma(s);
			Arg<_Size, T1>::ToString(s, va_arg(vl, T1)); AppendComma(s);
			Arg<_Size, T2>::ToString(s, va_arg(vl, T2)); AppendComma(s);
			Arg<_Size, T3>::ToString(s, va_arg(vl, T3)); AppendComma(s);
			Arg<_Size, T4>::ToString(s, va_arg(vl, T4)); AppendComma(s);
			Arg<_Size, T5>::ToString(s, va_arg(vl, T5)); AppendComma(s);
			Arg<_Size, T6>::ToString(s, va_arg(vl, T6)); AppendComma(s);
			Arg<_Size, T7>::ToString(s, va_arg(vl, T7)); AppendComma(s);
			Arg<_Size, T8>::ToString(s, va_arg(vl, T8)); AppendComma(s);
			Arg<_Size, T9>::ToString(s, va_arg(vl, T9));
		}

		template <size_t _Size, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
		inline void ParamsToString(wchar_t (&s)[_Size], void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10), va_list vl)
		{
			Arg<_Size, T0>::ToString(s, va_arg(vl, T0)); AppendComma(s);
			Arg<_Size, T1>::ToString(s, va_arg(vl, T1)); AppendComma(s);
			Arg<_Size, T2>::ToString(s, va_arg(vl, T2)); AppendComma(s);
			Arg<_Size, T3>::ToString(s, va_arg(vl, T3)); AppendComma(s);
			Arg<_Size, T4>::ToString(s, va_arg(vl, T4)); AppendComma(s);
			Arg<_Size, T5>::ToString(s, va_arg(vl, T5)); AppendComma(s);
			Arg<_Size, T6>::ToString(s, va_arg(vl, T6)); AppendComma(s);
			Arg<_Size, T7>::ToString(s, va_arg(vl, T7)); AppendComma(s);
			Arg<_Size, T8>::ToString(s, va_arg(vl, T8)); AppendComma(s);
			Arg<_Size, T9>::ToString(s, va_arg(vl, T9)); AppendComma(s);
			Arg<_Size, T10>::ToString(s, va_arg(vl, T10));
		}
	} // namespace c2jsWrapper

static const unsigned int MAX_JAVASCRIPT_LENGTH = (64 * 1024);

#if __DEV && !__64BIT
#define TYPE_CHECK_ONLY_IN_DEV_BUILDS(...) __VA_ARGS__
#else
// can't use va_lists in non-variadic functions in optimized builds (nor 64-bit builds either)
#define TYPE_CHECK_ONLY_IN_DEV_BUILDS(...) ...
#endif

#define JS_REGISTER(JsName, Name, ...)																				\
	static void Name(bool *succeeded, TYPE_CHECK_ONLY_IN_DEV_BUILDS(__VA_ARGS__))									\
	{																												\
		if(GetRgscConcreteInstance()->IsUiDisabled())																\
		{																											\
			return;																									\
		}																											\
		va_list vl;																									\
		va_start(vl, succeeded);																					\
		if(AssertVerify(GetRgscConcreteInstance()->_GetUiInterface()->IsJavascriptReadyToAcceptCommands()))			\
		{																											\
			wchar_t wszJavascript[MAX_JAVASCRIPT_LENGTH] =															\
								L"if(typeof window['" L#JsName L"'] == 'function') {" L#JsName L"(";				\
			void(*fn)(__VA_ARGS__) = NULL;																			\
			::rgsc::c2jsWrapper::ParamsToString(wszJavascript, fn, vl);												\
			safecatf(wszJavascript, L");} else {RGSC_FUNCTION_NOT_DEFINED('" L#JsName L"');}");						\
			ASSERT_ONLY(size_t msgLen = wcslen(wszJavascript));														\
			Assertf((msgLen + sizeof(wszJavascript[0])) <															\
				   (sizeof(wszJavascript) / sizeof(wszJavascript[0])),												\
					"Message length was too long (%d)", msgLen);													\
			Assertf(msgLen < (0.75 * MAX_JAVASCRIPT_LENGTH), 														\
					"Approaching limits of SCUI Javascript messages: %d/%d", msgLen, MAX_JAVASCRIPT_LENGTH);		\
			m_Window->CallJavascript(RgscWideString::point_to(wszJavascript));										\
			*succeeded = true;																						\
		}																											\
		else																										\
		{																											\
			*succeeded = false;																						\
		}																											\
		va_end(vl);																									\
	}
	// END

#define JS_REGISTER_SILENT(JsName, Name, ...)																		\
	static void Name(bool *succeeded, TYPE_CHECK_ONLY_IN_DEV_BUILDS(__VA_ARGS__))									\
	{																												\
		if(GetRgscConcreteInstance()->IsUiDisabled())																\
		{																											\
			return;																									\
		}																											\
		va_list vl;																									\
		va_start(vl, succeeded);																					\
		if(AssertVerify(GetRgscConcreteInstance()->_GetUiInterface()->IsJavascriptReadyToAcceptCommands()))			\
		{																											\
			wchar_t wszJavascript[MAX_JAVASCRIPT_LENGTH] =															\
								L"if(typeof window['" L#JsName L"'] == 'function') {" L#JsName L"(";				\
			void(*fn)(__VA_ARGS__) = NULL;																			\
			::rgsc::c2jsWrapper::ParamsToString(wszJavascript, fn, vl);												\
			safecatf(wszJavascript, L");} ");																		\
			Assert((wcslen(wszJavascript) + sizeof(wszJavascript[0])) <												\
				   (sizeof(wszJavascript) / sizeof(wszJavascript[0])));												\
			m_Window->CallJavascript(RgscWideString::point_to(wszJavascript));										\
			*succeeded = true;																						\
		}																											\
		else																										\
		{																											\
			*succeeded = false;																						\
		}																											\
		va_end(vl);																									\
	}
	// END


} // namespace rgsc

#endif // C2JS_HELPER_H
