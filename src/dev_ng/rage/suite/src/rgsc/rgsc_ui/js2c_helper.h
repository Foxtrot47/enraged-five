#ifndef JS2C_HELPER_H
#define JS2C_HELPER_H

#include "variant.h"
#include "weakstring.h"
#include "stringutil.h"

namespace rgsc {

	namespace scrWrapper {

		//////////////////////////////////
		// Return value templates.

		template<typename T>
		inline void AssignReturnValue(rgsc::Script::Info& info, T t) { info = t.UnsuportedNativeFunctionReturnType; }

		template<>
		inline void AssignReturnValue(rgsc::Script::Info& info, float f) {
			info.m_Result.destroy();
			info.m_Result.initdbl(f);
		}

		template<>
		inline void AssignReturnValue(rgsc::Script::Info& info, int i) {
			info.m_Result.destroy();
			info.m_Result.initint(i);
		}

 		template<>
 		inline void AssignReturnValue(rgsc::Script::Info& info, unsigned int i) {
			info.m_Result.destroy();
			info.m_Result.initint((int)i);
 		}

		template<>
		inline void AssignReturnValue(rgsc::Script::Info& info, bool b) {
			info.m_Result.destroy();
			info.m_Result.initbool(b);
		}

		template<>
		inline void AssignReturnValue(rgsc::Script::Info& info, RgscWideString s) {
			info.m_Result.destroy();
			info.m_Result.initwc(s.data(), s.length());
			if(s.isAllocatedOnHeap())
			{
				stringUtil_free(s);
			}
		}

		template<>
		inline void AssignReturnValue(rgsc::Script::Info& info, RgscUtf8String s) {
			info.m_Result.destroy();
			info.m_Result.initmb(s.data(), s.length());
			if(s.isAllocatedOnHeap())
			{
				stringUtil_free(s);
			}
		}

		//////////////////////////////////
		// Argument type templates.

#if __ASSERT
		static const char* VariantEnumToString(RgscVariant::Type t)
		{
			#define INPUT_ENUM_TO_STRING(x, y) y

			static const char* s_aEnumStringNames[] =
			{
				INPUT_ENUM_TO_STRING(RgscVariant::JSSTRING,			"string"),
				INPUT_ENUM_TO_STRING(RgscVariant::JSDOUBLE,			"double"),
				INPUT_ENUM_TO_STRING(RgscVariant::JSINTEGER,		"integer"),
				INPUT_ENUM_TO_STRING(RgscVariant::JSBOOLEAN,		"boolean"),
				INPUT_ENUM_TO_STRING(RgscVariant::JSNULL,			"NULL"),
				INPUT_ENUM_TO_STRING(RgscVariant::JSEMPTYOBJECT,	"object"),
				INPUT_ENUM_TO_STRING(RgscVariant::JSEMPTYARRAY,		"array"),
				INPUT_ENUM_TO_STRING(RgscVariant::JSBINDFUNC,		"function"),
				INPUT_ENUM_TO_STRING(RgscVariant::JSBINDSYNCFUNC,	"function"),
			};

			#undef INPUT_ENUM_TO_STRING

			return s_aEnumStringNames[t];
		}
#endif

		template<typename T>
		struct Arg {
			inline static T Value(rgsc::Script::Info& s, int& N) { return UnsuportedNativeFunctionArgumentType; }
		};

		template<>
		struct Arg<rgsc::Script::Info&> {
			inline static rgsc::Script::Info& Value(rgsc::Script::Info& info, int&) {return info;}
		};

		template<>
		struct Arg<float> {
			inline static float Value(rgsc::Script::Info& info, int& N) {
				AssertMsg(info.m_Args[N].type() == RgscVariant::JSDOUBLE,
					"Error: '%ls' : cannot convert parameter %d from '%s' to 'double'",
					info.m_FuncName->data(), N + 1, VariantEnumToString(info.m_Args[N].type()));
				
				return (float)info.m_Args[N++].toDouble();
			}
		};

		template<>
		struct Arg<int> {
			inline static int Value(rgsc::Script::Info& info, int& N) {
				AssertMsg(info.m_Args[N].type() == RgscVariant::JSINTEGER,
					"Error: '%ls' : cannot convert parameter %d from '%s' to 'integer'",
					info.m_FuncName->data(), N + 1, VariantEnumToString(info.m_Args[N].type()));
				
				return info.m_Args[N++].toInteger();
			}
 		};

 		template<>
 		struct Arg<unsigned int> {
 			inline static unsigned int Value(rgsc::Script::Info& info, int& N) {
				AssertMsg(info.m_Args[N].type() == RgscVariant::JSINTEGER,
					"Error: '%ls' : cannot convert parameter %d from '%s' to 'unsigned integer'",
					info.m_FuncName->data(), N + 1, VariantEnumToString(info.m_Args[N].type()));
				
				return (unsigned int)info.m_Args[N++].toInteger();
			}
  		};


		template<>
		struct Arg<bool> {
			inline static bool Value(rgsc::Script::Info& info, int& N) {
				AssertMsg(info.m_Args[N].type() == RgscVariant::JSBOOLEAN,
					"Error: '%ls' : cannot convert parameter %d from '%s' to 'boolean'",
					info.m_FuncName->data(), N + 1, VariantEnumToString(info.m_Args[N].type()));
				
				return info.m_Args[N++].toDouble() != 0;
			}
		};

 		template<>
		struct Arg<RgscWideString> {
 			inline static const RgscWideString Value(rgsc::Script::Info& info, int& N) {
				AssertMsg(info.m_Args[N].type() == RgscVariant::JSSTRING,
					"Error: '%ls' : cannot convert parameter %d from '%s' to 'string'",
					info.m_FuncName->data(), N + 1, VariantEnumToString(info.m_Args[N].type()));
				
				return info.m_Args[N++].toString();
			}
 		};

		///////////////////////////////////
		// Function call templates.
		// Each of these comes in two forms. One for a void return value, one for non-void.

		// Non-void return types
		template <typename RT>
		inline bool ReturnsAValue(RT(*fn)())
		{
			return true;
		}

		template <typename RT, typename T0>
		inline bool ReturnsAValue(RT(*fn)(T0))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1>
		inline bool ReturnsAValue(RT(*fn)(T0, T1))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2, T3))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2, T3, T4))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2, T3, T4, T5))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2, T3, T4, T5, T6))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15))
		{
			return true;
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16>
		inline bool ReturnsAValue(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16))
		{
			return true;
		}

		// Void return types
		template <>
		inline bool ReturnsAValue(void(*fn)())
		{
			return false;
		}

		template <typename T0>
		inline bool ReturnsAValue(void(*fn)(T0))
		{
			return false;
		}

		template <typename T0, typename T1>
		inline bool ReturnsAValue(void(*fn)(T0, T1))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2, typename T3>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2, T3))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2, T3, T4))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2, T3, T4, T5))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2, T3, T4, T5, T6))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15))
		{
			return false;
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16>
		inline bool ReturnsAValue(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16))
		{
			return false;
		}

#if __DEV
		#define CheckNumberOfArguments(numExpectedArgs, s)																					\
		{																																	\
			if(numExpectedArgs == 0)																										\
			{																																\
				Assertf(s.m_NumArgs == 0 || ((s.m_NumArgs == 1) && (s.m_Args[0].type() == RgscVariant::JSNULL)),							\
						"Error: '%ls' : function takes 0 arguments but %d %s being passed.",												\
						s.m_FuncName->data(), s.m_NumArgs, s.m_NumArgs == 1 ? "is" : "are");												\
			}																																\
			else																															\
			{																																\
				Assertf((int)s.m_NumArgs == numExpectedArgs, "Error: '%ls' : function takes %u argument(s) but %d %s being passed.",		\
						s.m_FuncName->data(), numExpectedArgs, s.m_NumArgs, s.m_NumArgs == 1 ? "is" : "are");								\
			}																																\
		}																				
#else
		#define CheckNumberOfArguments(...)
#endif

		// Non-void return types
		template <typename RT>
		inline void CallFunction(RT(*fn)(), rgsc::Script::Info& s)
		{
			CheckNumberOfArguments(0, s);
			AssignReturnValue(s, fn());
		}

		template <typename RT, typename T0>
		inline void CallFunction(RT(*fn)(T0), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0));
		}

		template <typename RT, typename T0, typename T1>
		inline void CallFunction(RT(*fn)(T0 t0, T1 t1), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1));
		}

		template <typename RT, typename T0, typename T1, typename T2>
		inline void CallFunction(RT(*fn)(T0, T1, T2), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2));
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3>
		inline void CallFunction(RT(*fn)(T0, T1, T2, T3), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2, t3));
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4>
		inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2, t3, t4));
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5>
		inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5));
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
		inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6));
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
		inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7));
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
		inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8));
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
		inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9));
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
		inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			T10 t10 = Arg<T10>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10));
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
		inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			T10 t10 = Arg<T10>::Value(s, N);
			T11 t11 = Arg<T11>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11));
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
		inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			T10 t10 = Arg<T10>::Value(s, N);
			T11 t11 = Arg<T11>::Value(s, N);
			T12 t12 = Arg<T12>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12));
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13>
		inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			T10 t10 = Arg<T10>::Value(s, N);
			T11 t11 = Arg<T11>::Value(s, N);
			T12 t12 = Arg<T12>::Value(s, N);
			T13 t13 = Arg<T13>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13));
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
		inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			T10 t10 = Arg<T10>::Value(s, N);
			T11 t11 = Arg<T11>::Value(s, N);
			T12 t12 = Arg<T12>::Value(s, N);
			T13 t13 = Arg<T13>::Value(s, N);
			T14 t14 = Arg<T14>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14));
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
		inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			T10 t10 = Arg<T10>::Value(s, N);
			T11 t11 = Arg<T11>::Value(s, N);
			T12 t12 = Arg<T12>::Value(s, N);
			T13 t13 = Arg<T13>::Value(s, N);
			T14 t14 = Arg<T14>::Value(s, N);
			T15 t15 = Arg<T15>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15));
		}

		template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16>
		inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			T10 t10 = Arg<T10>::Value(s, N);
			T11 t11 = Arg<T11>::Value(s, N);
			T12 t12 = Arg<T12>::Value(s, N);
			T13 t13 = Arg<T13>::Value(s, N);
			T14 t14 = Arg<T14>::Value(s, N);
			T15 t15 = Arg<T15>::Value(s, N);
			T16 t16 = Arg<T16>::Value(s, N);
			CheckNumberOfArguments(N, s);
			AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16));
		}

		// Void return types
		template <>
		inline void CallFunction(void(*fn)(), rgsc::Script::Info& s)
		{
			CheckNumberOfArguments(0, s);
			fn();
		}

		template <typename T0>
		inline void CallFunction(void(*fn)(T0), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0);
		}

		template <typename T0, typename T1>
		inline void CallFunction(void(*fn)(T0, T1), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1);
		}

		template <typename T0, typename T1, typename T2>
		inline void CallFunction(void(*fn)(T0, T1, T2), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2);
		}

		template <typename T0, typename T1, typename T2, typename T3>
		inline void CallFunction(void(*fn)(T0, T1, T2, T3), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2, t3);
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4>
		inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2, t3, t4);
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5>
		inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2, t3, t4, t5);
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
		inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2, t3, t4, t5, t6);
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
		inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2, t3, t4, t5, t6, t7);
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
		inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2, t3, t4, t5, t6, t7, t8);
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
		inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9);
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
		inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			T10 t10 = Arg<T10>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10);
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
		inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			T10 t10 = Arg<T10>::Value(s, N);
			T11 t11 = Arg<T11>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11);
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
		inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			T10 t10 = Arg<T10>::Value(s, N);
			T11 t11 = Arg<T11>::Value(s, N);
			T12 t12 = Arg<T12>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12);
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13>
		inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			T10 t10 = Arg<T10>::Value(s, N);
			T11 t11 = Arg<T11>::Value(s, N);
			T12 t12 = Arg<T12>::Value(s, N);
			T13 t13 = Arg<T13>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13);
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
		inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			T10 t10 = Arg<T10>::Value(s, N);
			T11 t11 = Arg<T11>::Value(s, N);
			T12 t12 = Arg<T12>::Value(s, N);
			T13 t13 = Arg<T13>::Value(s, N);
			T14 t14 = Arg<T14>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14);
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
		inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			T10 t10 = Arg<T10>::Value(s, N);
			T11 t11 = Arg<T11>::Value(s, N);
			T12 t12 = Arg<T12>::Value(s, N);
			T13 t13 = Arg<T13>::Value(s, N);
			T14 t14 = Arg<T14>::Value(s, N);
			T15 t15 = Arg<T15>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15);
		}

		template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16>
		inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16), rgsc::Script::Info& s)
		{
			int N = 0;
			T0 t0 = Arg<T0>::Value(s, N);
			T1 t1 = Arg<T1>::Value(s, N);
			T2 t2 = Arg<T2>::Value(s, N);
			T3 t3 = Arg<T3>::Value(s, N);
			T4 t4 = Arg<T4>::Value(s, N);
			T5 t5 = Arg<T5>::Value(s, N);
			T6 t6 = Arg<T6>::Value(s, N);
			T7 t7 = Arg<T7>::Value(s, N);
			T8 t8 = Arg<T8>::Value(s, N);
			T9 t9 = Arg<T9>::Value(s, N);
			T10 t10 = Arg<T10>::Value(s, N);
			T11 t11 = Arg<T11>::Value(s, N);
			T12 t12 = Arg<T12>::Value(s, N);
			T13 t13 = Arg<T13>::Value(s, N);
			T14 t14 = Arg<T14>::Value(s, N);
			T15 t15 = Arg<T15>::Value(s, N);
			T16 t16 = Arg<T16>::Value(s, N);
			CheckNumberOfArguments(N, s);
			fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16);
		}

	} // namespace scrWrapper


	// Given a function pointer, return a function pointer that calls CallFunction with the f.p. and the rgsc::Script::Info
#define SCR_REGISTER(name)																			\
	m_Window->sendAddBindOnStartLoading(RgscWideString::point_to(L#name),								\
									rgsc::scrWrapper::ReturnsAValue(name));							\
	struct scrWrapped_ ## name {																	\
		static void Call(rgsc::Script::Info& s) {													\
			::rgsc::scrWrapper::CallFunction(name, s);												\
		}																							\
	};																								\
	static Cmd Cmd_ ## name(&scrWrapped_ ## name::Call);											\
	Cmd_ ## name.GetCmd();																			\
	rgsc::Script::RegisterCommand(RgscWideString::point_to(L#name), &Cmd_ ## name);					\
	if(rgsc::scrWrapper::ReturnsAValue(name)) {SCR_REGISTER_AUTO_ASYNC(name)}						\
	//END

#define SCR_REGISTER_AUTO_ASYNC(name)																\
	m_Window->sendAddBindOnStartLoading(RgscWideString::point_to(L#name ## L"_ASYNC"),					\
									false);															\
	struct scrWrapped_ ## name ## _ASYNC {															\
		static void Call(rgsc::Script::Info& s) {													\
			s.m_IsAutoAsync = true;																	\
			::rgsc::scrWrapper::CallFunction(name, s);												\
		}																							\
	};																								\
	static Cmd Cmd_ ## name ## _ASYNC(&scrWrapped_ ## name ## _ASYNC::Call);						\
	Cmd_ ## name ## _ASYNC.GetCmd();																\
	rgsc::Script::RegisterCommand(RgscWideString::point_to(L#name ## L"_ASYNC"), &Cmd_ ## name ## _ASYNC);
	//END


} // namespace rgsc

#endif // JS2C_HELPER_H
