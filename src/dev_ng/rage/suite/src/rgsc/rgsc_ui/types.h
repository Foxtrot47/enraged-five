#ifndef RGSC_TYPES_H
#define RGSC_TYPES_H

#include <windows.h>

#if RSG_CPU_X64
#define RGSC_X64_ONLY(...) __VA_ARGS__
#else
#define RGSC_X64_ONLY(...)
#endif

namespace rgsc
{

enum RgscWindowType
{
	INVALID = -1,
	CHILD, // Browser created as a child of another window 
	POPUP, // Browser created as a popup window
	WINDOWLESS, // Browser created for off-screen rendering
	BORDERLESS // Browser creates its own borderless parent window.
};

struct RgscRect
{
    int mLeft;
    int mTop;
    int mWidth;
    int mHeight;

    inline int y() const { return mTop; }
    inline int x() const { return mLeft; }
    inline int top() const { return mTop; }
    inline int left() const { return mLeft; }
    inline int width() const { return mWidth; }
    inline int height() const { return mHeight; }
    inline int right() const { return mLeft + mWidth; }
    inline int bottom() const { return mTop + mHeight; }

    template <class T>
    inline void setFromRect(const T&sourceRect) {
        mLeft = sourceRect.x();
        mTop = sourceRect.y();
        mWidth = sourceRect.width();
        mHeight = sourceRect.height();
    }

    inline bool contains(int x, int y) const {
        return (x >= left() && x < right() &&
                y >= top() && y < bottom());
    }
    RgscRect intersect(const RgscRect &rect) const {
        int rx = rectmax(left(), rect.left());
        int ry = rectmax(top(), rect.top());
        int rr = rectmin(right(), rect.right());
        int rb = rectmin(bottom(), rect.bottom());
        if (rx >= rr || ry >= rb)
            rx = ry = rr = rb = 0;  // non-intersecting
        RgscRect ret;
        ret.mLeft = rx;
        ret.mTop = ry;
        ret.mWidth = rr-rx;
        ret.mHeight = rb-ry;
        return ret;
    }

	bool intersects(const RgscRect &rect) const {
        int rx = rectmax(left(), rect.left());
        int ry = rectmax(top(), rect.top());
        int rr = rectmin(right(), rect.right());
        int rb = rectmin(bottom(), rect.bottom());
		return !(rx >= rr || ry >= rb);
    }

    /** Create a new Rect equivalent to this Rect translated by the specified
     *  amounts in either direction.
     *  \param dx amount to translate along the X axis
     *  \param dy amount to translate along the Y axis
     *  \returns a new translated Rect
     */
    RgscRect translate(int dx, int dy) const {
        RgscRect ret = *this;
        ret.mLeft += dx;
        ret.mTop += dy;
        return ret;
    }

private:
    static int rectmax(int a, int b) {
        return a>b?a:b;
    }
    static int rectmin(int a, int b) {
        return a<b?a:b;
    }
};

struct RgscWndProcMap
{
public:

	RgscWndProcMap() : m_OriginalProc(0), m_hWnd(0), m_hParent(0), m_UserData(NULL) { }
	RgscWndProcMap(HWND hWnd, WNDPROC proc, HWND parent = 0, void* userData = 0)
	{
		m_hWnd = hWnd;
		m_OriginalProc = proc;
		m_hParent = parent;
		m_UserData = userData;
	}

	// The original WndProc to be called when a message is not handled.
	WNDPROC m_OriginalProc;

	// The hWnd that this WndProc is mapped to.
	HWND m_hWnd;

	// The parent window handle that messages should be forwarded to.
	HWND m_hParent;

	// A container for custom user data to avoid calling SetWindowLongPtr with
	//	GWLP_USERDATA, which may already have been called for a window.
	void* m_UserData;
};


} // namespace rgsc

#endif //RGSC_TYPES_H
