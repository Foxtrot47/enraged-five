#include "graphics_d3d9.h"
#include "rgsc_common.h"
#include "tiledbuffer.h"
#include "system/new.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <d3dx9.h>
#include <d3dcompiler.h>
#include <xnamath.h>
#include <stdio.h>
#include <shlobj.h>
#pragma warning(pop)

namespace rgsc
{

// Vertex shader data. This was simply dumped out after compiling the shaders with D3DXCompileShaderFromFileA.
static unsigned char sm_CompiledVertexShader[] = {
	0,3,254,255,254,255,22,0,67,84,65,66,28,0,0,0,35,0,0,0,0,3,254,255,0,0,0,0,0,0,0,0,0,129,4,
	0,28,0,0,0,118,115,95,51,95,48,0,77,105,99,114,111,115,111,102,116,32,40,82,41,32,72,76,83,
	76,32,83,104,97,100,101,114,32,67,111,109,112,105,108,101,114,32,57,46,50,57,46,57,53,50,
	46,51,49,49,49,0,31,0,0,2,0,0,0,128,0,0,15,144,31,0,0,2,5,0,0,128,1,0,15,144,31,0,0,2,0,0,
	0,128,0,0,15,224,31,0,0,2,5,0,0,128,1,0,3,224,1,0,0,2,0,0,15,224,0,0,228,144,1,0,0,2,1,0,3,
	224,1,0,228,144,255,255,0,0
};

// Pixel shader data. This was simply dumped out after compiling the shaders with D3DXCompileShaderFromFileA.
static unsigned char sm_CompiledPixelShader[] = {
	0,3,255,255,254,255,33,0,67,84,65,66,28,0,0,0,79,0,0,0,0,3,255,255,1,0,0,0,28,0,0,0,0,129,4,
	0,72,0,0,0,48,0,0,0,3,0,0,0,1,0,0,0,56,0,0,0,0,0,0,0,84,101,120,48,0,171,171,171,4,0,12,0,1,
	0,1,0,1,0,0,0,0,0,0,0,112,115,95,51,95,48,0,77,105,99,114,111,115,111,102,116,32,40,82,41,
	32,72,76,83,76,32,83,104,97,100,101,114,32,67,111,109,112,105,108,101,114,32,57,46,50,57,46,
	57,53,50,46,51,49,49,49,0,31,0,0,2,5,0,0,128,0,0,3,144,31,0,0,2,0,0,0,144,0,8,15,160,66,0,0,
	3,0,8,15,128,0,0,228,144,0,8,228,160,255,255,0,0
};

//--------------------------------------------------------------------------------------
// Structures
//--------------------------------------------------------------------------------------

HRESULT Rgsc_Graphics_D3D9::CreateTextures()
{
	HRESULT hr = RGSC_OK;

	m_Textures = rage_new IDirect3DTexture9*[m_Tiles->getNumTiles()];

	for(unsigned int i = 0; i < m_Tiles->getNumTiles(); i++)
	{
		Tile* tile = m_Tiles->getTile(i);

		hr = m_Device->CreateTexture(tile->getWidth(), tile->getHeight(), 1, D3DUSAGE_DYNAMIC, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &m_Textures[i], NULL);
		if(FAILED(hr))
		{
			return hr;
		}
	}

	return hr;
}

Rgsc_Graphics_D3D9::Rgsc_Graphics_D3D9(IDirect3DDevice9* device, void* params, const RenderRect& rect)
: m_Device(NULL)
, m_Textures(NULL)
, m_VertexBuffer(NULL)
, m_VertexShader(NULL)
, m_PixelShader(NULL)
, m_FirstStateBlock(NULL)
, m_OldStateBlock(NULL)
, m_StateBlock(NULL)
{
	AssertVerify(SUCCEEDED(Init(device, (D3DPRESENT_PARAMETERS*)params, rect)));
}

Rgsc_Graphics_D3D9::~Rgsc_Graphics_D3D9()
{
	Shutdown();
}

struct SimpleVertex
{
	XMFLOAT4 Pos;
	XMFLOAT2 Tex;
};

bool Rgsc_Graphics_D3D9::OnLost()
{
	ReleaseResources();
	return true;
}

bool Rgsc_Graphics_D3D9::OnReset(void *params, const RenderRect& rect)
{
	D3DPRESENT_PARAMETERS* pD3DPP = (D3DPRESENT_PARAMETERS*)params;
	return SUCCEEDED(Init(m_Device, pD3DPP, rect));
}

#if 0
static bool GetShaderPath(char (&path)[MAX_PATH])
{
	bool success = false;
	path[0] = '\0';

	success = SHGetSpecialFolderPathA(NULL, path, CSIDL_PROGRAM_FILES, false) == TRUE;

#if _DEBUG
	strcat_s(path, "\\Rockstar Games\\Social Club Debug\\shader.fx");
#else
	strcat_s(path, "\\Rockstar Games\\Social Club\\shader.fx");
#endif

	return success;
}
#endif

bool Rgsc_Graphics_D3D9::MapMouse(int &x, int &y)
{
	if((x < (int)m_MappedOffsetX) || (x > (int)(m_MappedWidth + m_MappedOffsetX)))
	{
		return false;
	}

	if((y < (int)m_MappedOffsetY) || (y > (int)(m_MappedHeight + m_MappedOffsetY)))
	{
		return false;
	}

	x = (int)MapNumFromRangeToRange((float)x, (float)m_MappedOffsetX, (float)m_MappedWidth + (float)m_MappedOffsetX, (float)0.0f, (float)m_Width);
	y = (int)MapNumFromRangeToRange((float)y, (float)m_MappedOffsetY, (float)m_MappedHeight + (float)m_MappedOffsetY, 0.0f, (float)m_Height);

	return true;
}

HRESULT Rgsc_Graphics_D3D9::Init(IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* params, const RenderRect& rect)
{
	m_Rect = rect;

	m_Device = device;

	m_WindowHandle = params->hDeviceWindow;

	RECT rc;
	GetClientRect(m_WindowHandle, &rc);
	m_ClientWidth = rc.right - rc.left;
	m_ClientHeight = rc.bottom - rc.top;

	m_OffsetX = 0;
	m_OffsetY = 0;
	m_MappedOffsetX = 0;
	m_MappedOffsetY = 0;

	m_Width = params->BackBufferWidth;
	m_Height = params->BackBufferHeight;

	if((m_Width != m_ClientWidth) || (m_Height != m_ClientHeight))
	{
		Warningf("Window's Client Area (%d x %d) does not match the resolution of the D3D Device (%d x %d). "
				 "This might be unintentional. The Social Club UI will still work fine.",
				 m_ClientWidth, m_ClientHeight, m_Width, m_Height);
	}

 	if(m_Rect.IsSet())
	{
		if(AssertVerify(m_Rect.GetWidth() <= m_Width && m_Rect.GetHeight() <= m_Height))
		{
			m_OffsetX = m_Rect.m_Left;
			m_OffsetY = m_Rect.m_Top;

			m_MappedOffsetX = (int)MapNumFromRangeToRange((float)m_OffsetX, (float)0.0f, (float)m_Width, (float)0.0f, (float)m_ClientWidth);
			m_MappedOffsetY = (int)MapNumFromRangeToRange((float)m_OffsetY, (float)0.0f, (float)m_Height, (float)0.0f, (float)m_ClientHeight);

			m_Width = m_Rect.GetWidth();
			m_Height = m_Rect.GetHeight();
		}
		else
		{
			Errorf("Render Rect is outside of the frame buffer, ignoring.");
		}
	}

	m_MappedWidth = (int)MapNumFromRangeToRange((float)m_Width, (float)0.0f, (float)params->BackBufferWidth, (float)0.0f, (float)m_ClientWidth);
	m_MappedHeight = (int)MapNumFromRangeToRange((float)m_Height, (float)0.0f, (float)params->BackBufferHeight, (float)0.0f, (float)m_ClientHeight);

	m_Tiles = rage_new TiledBuffer(m_Width, m_Height);

	if(m_Tiles == NULL)
	{
		return RGSC_FAIL;
	}

	return RGSC_OK;
}

HRESULT Rgsc_Graphics_D3D9::CreateResources()
{
	HRESULT hr = S_OK;

#if 0
	char shaderPath[MAX_PATH] = {0};
	if(GetShaderPath(shaderPath) == false)
	{
		return S_FALSE;
	}

	ID3DXBuffer* m_VertexShaderBuffer;
	DWORD dwShaderFlags = /*D3DCOMPILE_ENABLE_STRICTNESS | */D3DCOMPILE_WARNINGS_ARE_ERRORS | D3DCOMPILE_OPTIMIZATION_LEVEL3;

	// TODO: NS - load in the pre-compiled shader (embedded shader as rage does it)
	hr = D3DXCompileShaderFromFileA(shaderPath,				//filepath
		NULL,					//macro's
		NULL,					//includes
		"VS",					//main function
		"vs_3_0",				//shader profile
		dwShaderFlags,			//flags
		&m_VertexShaderBuffer,	//compiled operations
		NULL,					//errors
		NULL);					//constants

	if(FAILED(hr))
	{
		return hr;
	}

	{
		printf("DX9 Vertex Shader:\n");
		DWORD bufSize = m_VertexShaderBuffer->GetBufferSize();
		unsigned char* ptr = (unsigned char*)m_VertexShaderBuffer->GetBufferPointer();
		for(DWORD i = 0; i < bufSize; i++)
		{
			printf("%d,", ptr[i]);
		}
		printf("End of Vertex Shader\n\n");
	}

	m_Device->CreateVertexShader((DWORD*)m_VertexShaderBuffer->GetBufferPointer(), &m_VertexShader);
	m_VertexShaderBuffer->Release();

	ID3DXBuffer* m_PixelShaderBuffer;

	hr = D3DXCompileShaderFromFileA(shaderPath,				//filepath
		NULL,					//macro's            
		NULL,					//includes           
		"PS",					//main function      
		"ps_3_0",				//shader profile     
		dwShaderFlags,			//flags              
		&m_PixelShaderBuffer,	//compiled operations
		NULL,					//errors
		NULL);					//constants
	if(FAILED(hr))
	{
		return hr;
	}

	{
		printf("DX9 Pixel Shader:\n");
		DWORD bufSize = m_PixelShaderBuffer->GetBufferSize();
		unsigned char* ptr = (unsigned char*)m_PixelShaderBuffer->GetBufferPointer();
		for(DWORD i = 0; i < bufSize; i++)
		{
			printf("%d,", ptr[i]);
		}
		printf("End of Pixel Shader\n\n");
	}

	m_Device->CreatePixelShader((DWORD*)m_PixelShaderBuffer->GetBufferPointer(), &m_PixelShader);
	m_PixelShaderBuffer->Release();
#else
	hr = m_Device->CreateVertexShader((DWORD*)sm_CompiledVertexShader, &m_VertexShader);
	if(FAILED(hr))
	{
		return hr;
	}

	hr = m_Device->CreatePixelShader((DWORD*)sm_CompiledPixelShader, &m_PixelShader);
	if(FAILED(hr))
	{
		return hr;
	}
#endif

	SimpleVertex *allVertices = rage_new SimpleVertex[4 * m_Tiles->getNumTiles()];

	for(unsigned int i = 0; i < m_Tiles->getNumTiles(); i++)
	{
		Tile* tile = m_Tiles->getTile(i);

		float left = (float)(tile->getLeft() + m_OffsetX);
		float top = (float)(tile->getTop() + m_OffsetY);
		float right = left + (float)tile->getWidth();
		float bottom = top + (float)tile->getHeight();

		// Note: we're subtracting 0.5f from each vertex.x and vertex.y to precisely map texels to pixels. See http://msdn.microsoft.com/en-us/library/windows/desktop/bb219690(v=vs.85).aspx.
		// Create vertex buffer
		SimpleVertex vertices[] =
		{
			{XMFLOAT4(left - 0.5f, top - 0.5f, 1.0f, 1.0f), XMFLOAT2(0.0f, 0.0f)},
			{XMFLOAT4(left - 0.5f, bottom - 0.5f, 1.0f, 1.0f), XMFLOAT2(0.0f, 1.0f)},
			{XMFLOAT4(right - 0.5f, top - 0.5f, 1.0f, 1.0f), XMFLOAT2(1.0f, 0.0f)},
			{XMFLOAT4(right - 0.5f, bottom - 0.5f, 1.0f, 1.0f), XMFLOAT2(1.0f, 1.0f)},
		};

		memcpy(&allVertices[i * 4], vertices, sizeof(SimpleVertex) * 4);
	}

	hr = m_Device->CreateVertexBuffer(sizeof(SimpleVertex) * 4 * m_Tiles->getNumTiles(), D3DUSAGE_WRITEONLY, D3DFVF_XYZRHW | D3DFVF_TEX1, D3DPOOL_MANAGED, &m_VertexBuffer, NULL);
	if(FAILED(hr))
	{
		return hr;
	}

	void* data;
	hr = m_VertexBuffer->Lock(0, 0, &data, 0);
	if(FAILED(hr))
	{
		return hr;
	}

	memcpy(data, allVertices, sizeof(SimpleVertex) * 4 * m_Tiles->getNumTiles());

	m_VertexBuffer->Unlock();

	delete [] allVertices;

	hr = CreateTextures();
	if(hr != D3D_OK)
	{
		return hr;
	}

	return RGSC_OK;
}

void Rgsc_Graphics_D3D9::ReleaseResources()
{
	ULONG refCount = 0;

	if(m_Textures)
	{
		for(unsigned int i = 0; i < m_Tiles->getNumTiles(); i++)
		{
			if(m_Textures[i])
			{
				refCount = m_Textures[i]->Release();
				Assert(refCount == 0);
				m_Textures[i] = NULL;
			}
		}

		delete [] m_Textures;
		m_Textures = NULL;
	}

	if(m_VertexBuffer)
	{
		refCount = m_VertexBuffer->Release();
		Assert(refCount == 0);
		m_VertexBuffer = NULL;
	}

	if(m_VertexShader)
	{
		refCount = m_VertexShader->Release();
		Assert(refCount == 0);
		m_VertexShader = NULL;
	}

	if(m_PixelShader)
	{
		refCount = m_PixelShader->Release();
		Assert(refCount == 0);
		m_PixelShader = NULL;
	}

	if(m_FirstStateBlock)
	{
		refCount = m_FirstStateBlock->Release();
		Assert(refCount == 0);
		m_FirstStateBlock = NULL;
	}

	if(m_OldStateBlock)
	{
		refCount = m_OldStateBlock->Release();
		Assert(refCount == 0);
		m_OldStateBlock = NULL;
	}

	if(m_StateBlock)
	{
		refCount = m_StateBlock->Release();
		m_StateBlock = NULL;
	}

	if(m_Tiles)
	{
		delete m_Tiles;
		m_Tiles = NULL;
	}
}

void Rgsc_Graphics_D3D9::Shutdown()
{
	ReleaseResources();

	// if we're doing a full shutdown, release our reference to the device
	// we added a ref to the device when we called QueryInterface in graphics.cpp
	if(m_Device)
	{
		m_Device->Release();
		m_Device = NULL;
	}
}

void Rgsc_Graphics_D3D9::SetRenderStates()
{
	// depth/stencil
	m_Device->SetRenderState(D3DRS_ZENABLE, FALSE);
	m_Device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	m_Device->SetRenderState(D3DRS_ZFUNC, D3DCMP_ALWAYS);

	// alpha blending
	m_Device->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	m_Device->SetRenderState(D3DRS_ALPHAREF, 0);
	m_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_Device->SetRenderState(D3DRS_SEPARATEALPHABLENDENABLE, FALSE);
	m_Device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS);
	m_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	m_Device->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_Device->SetRenderState(D3DRS_SRCBLENDALPHA, D3DBLEND_ONE);
	m_Device->SetRenderState(D3DRS_DESTBLENDALPHA, D3DBLEND_ZERO);
	m_Device->SetRenderState(D3DRS_BLENDOPALPHA, D3DBLENDOP_ADD);

	// other states
	m_Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_Device->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_FLAT);
	m_Device->SetRenderState(D3DRS_LASTPIXEL, TRUE);
	m_Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	m_Device->SetRenderState(D3DRS_DITHERENABLE, FALSE);
	m_Device->SetRenderState(D3DRS_FOGENABLE, FALSE);
	m_Device->SetRenderState(D3DRS_RANGEFOGENABLE, FALSE);
	m_Device->SetRenderState(D3DRS_STENCILENABLE, FALSE);
	m_Device->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
	m_Device->SetRenderState(D3DRS_COLORWRITEENABLE, 0x0000000F);
	m_Device->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
	m_Device->SetRenderState(D3DRS_LIGHTING, FALSE);

	// sampler state
	m_Device->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	m_Device->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
	m_Device->SetSamplerState(0, D3DSAMP_ADDRESSW, D3DTADDRESS_CLAMP);
	m_Device->SetSamplerState(0, D3DSAMP_BORDERCOLOR, 0x00000000);
	m_Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);	// because bilinear filtering makes text look blurry
	m_Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	m_Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);
	m_Device->SetSamplerState(0, D3DSAMP_MIPMAPLODBIAS, 0);
	m_Device->SetSamplerState(0, D3DSAMP_MAXMIPLEVEL, 0);
	m_Device->SetSamplerState(0, D3DSAMP_MAXANISOTROPY, 1);
	m_Device->SetSamplerState(0, D3DSAMP_SRGBTEXTURE, 0);
	m_Device->SetSamplerState(0, D3DSAMP_ELEMENTINDEX, 0);
	m_Device->SetSamplerState(0, D3DSAMP_DMAPOFFSET, 256);

	// viewport, shaders
	D3DVIEWPORT9 viewData = {m_OffsetX, m_OffsetY, m_Width, m_Height, 0.0f, 1.0f};
	m_Device->SetViewport(&viewData);
	m_Device->SetVertexShader(m_VertexShader);
	m_Device->SetPixelShader(m_PixelShader);
	m_Device->SetFVF(D3DFVF_XYZRHW | D3DFVF_TEX1);
	m_Device->SetStreamSource(0, m_VertexBuffer, 0, sizeof(SimpleVertex));
}

void Rgsc_Graphics_D3D9::UpdateTextures(bool onlyNotifications)
{
	if(m_Tiles)
	{
		for(unsigned int i = 0; i < m_Tiles->getNumTiles(); i++)
		{
			Tile* tile = m_Tiles->getTile(i);
			tile->setVisible(true);

			unsigned short currentFrame;
			bool isDirty = tile->isDirty(currentFrame);
			if(isDirty && onlyNotifications)
			{
				// TODO: NS - temporary hack to only render the notification area when the rest of the SCUI is invisible
				// This is necessary because when a notification comes on screen, Chrome/CEF says the entire
				// screen is dirty and we stall the game every time a notification comes on screen. 

				int scale = (m_Height > 1080) ? 2 : 1;
				const unsigned maxNotificationWidth = 256 * scale;
				const unsigned maxNotificationAreaHeight = 500 * scale;
				TileRect notificationRect;
				notificationRect.m_Left = (maxNotificationWidth < m_Width) ? (m_Width - maxNotificationWidth) : 0;
				notificationRect.m_Top = 0;
				notificationRect.m_Right = m_Width;
				notificationRect.m_Bottom = maxNotificationAreaHeight;
				if(notificationRect.m_Bottom > m_Height)
				{
					notificationRect.m_Bottom = m_Height;
				}
				isDirty = tile->getRect().IsIntersecting(notificationRect);
				if(isDirty == false)
				{
					tile->setVisible(false);
				}
			}

			if(isDirty)
			{
		 		D3DLOCKED_RECT rect;
		 		HRESULT hr = m_Textures[i]->LockRect(0, &rect, NULL, D3DLOCK_DISCARD);
				if(SUCCEEDED(hr))
				{
					unsigned char* pbyDst = (unsigned char*)(rect.pBits);
					unsigned char* pbySrc = tile->getBuffer();
					unsigned int rowSize = tile->getRowSizeInBytes();
					unsigned int srcRowPitch = m_Tiles->getRowPitch();
					unsigned int dstRowPitch = rect.Pitch;

					if(srcRowPitch == dstRowPitch)
					{
						// if the texture is the full width of the screen, we can copy it in one memcpy
						memcpy(pbyDst, pbySrc, rowSize * tile->getEffectiveHeight());
					}
					else
					{
						for (unsigned int row = 0; row < tile->getEffectiveHeight(); row++)
						{
							Assert((pbySrc + rowSize) <= (m_Tiles->getBuffer() + m_Tiles->getBufferSize()));
							Assert(pbySrc >= m_Tiles->getBuffer());

							memcpy(pbyDst, pbySrc, rowSize);
							pbySrc += srcRowPitch;
							pbyDst += dstRowPitch;
						}
					}
					m_Textures[i]->UnlockRect(0);
				}

				tile->setLastUpdatedFrame(currentFrame);
			}
		}
	}
}

void Rgsc_Graphics_D3D9::Render(bool onlyNotifications)
{
	// defer creation of D3D9 resources here in case the game is using
	// a separate thread for rendering and uses D3D9 in single-threaded mode.
 	if(m_Textures == NULL)
 	{
 		AssertVerify(CreateResources() == S_OK);
 		Assert(m_Textures);
 	}

	bool useFirstStateBlock = false;
	if(m_OldStateBlock)
	{
		// save current render states that we will change
		m_OldStateBlock->Capture();
	}
	else
	{
		// On the first render, I capture all the states and restore them when I'm done rendering.
		// Doing a full capture/restore every frame seems to degrade performance over time for
		// some reason. So on all subsequent renders, I only capture the values of the states
		// I'm changing, then restoring just those states. I can't do the partial capture/restore
		// on the first render since I have to change states in order to create a state block.
		useFirstStateBlock = true;
		m_Device->CreateStateBlock(D3DSBT_ALL, &m_FirstStateBlock);
		m_FirstStateBlock->Capture();

		// start creating the state block
		m_Device->BeginStateBlock();

		SetRenderStates();

		// save the state block
		m_Device->EndStateBlock(&m_OldStateBlock);
	}

	if(m_StateBlock)
	{
		m_StateBlock->Apply();
	}
	else
	{
		// start creating the state block
		m_Device->BeginStateBlock();

		SetRenderStates();

		// save the state block
		m_Device->EndStateBlock(&m_StateBlock);

		// for some reason I need to call Apply() here even though I just set the renderstates.
		// this fixed a crash that could occur on the first call to Render() in DX9 mode.
		m_StateBlock->Apply();
	}

	// Update the textures just before rendering to pick up any recent changes that came from the game's update thread
	UpdateTextures(onlyNotifications);

	for(unsigned int i = 0; i < m_Tiles->getNumTiles(); i++)
	{
		Tile* tile = m_Tiles->getTile(i);

		if(tile && tile->isVisible())
		{
			m_Device->SetTexture(0, m_Textures[i]);
			m_Device->DrawPrimitive(D3DPT_TRIANGLESTRIP, i * 4, 2);
		}
	}

	// restore old render states
	if(useFirstStateBlock)
	{
		m_FirstStateBlock->Apply();
		m_FirstStateBlock->Release();
		m_FirstStateBlock = NULL;
	}
	else
	{
		m_OldStateBlock->Apply();
	}
}

} // namespace rgsc
