#ifndef INPUT_EVENTQ_H
#define INPUT_EVENTQ_H

namespace rgsc
{

struct ioEvent
{
	enum ioEventType
	{ 
		// Mouse Events
		//	m_Modifiers will refer to RgscMouseModifier
		IO_MOUSE_BEGIN,
		IO_MOUSE_MOVE,				// Mouse movement.  Queue will merge adjacent move messages when possible
		IO_MOUSE_LEFT_DOWN,			// Left mouse button pressed
		IO_MOUSE_LEFT_DBLCLK,		// Left mouse button double clicked
		IO_MOUSE_LEFT_UP,			// Left mouse button released
		IO_MOUSE_RIGHT_DOWN,		// Right mouse button pressed
		IO_MOUSE_RIGHT_UP,			// Right mouse button released
		IO_MOUSE_MIDDLE_DOWN,		// Middle mouse button pressed
		IO_MOUSE_MIDDLE_UP,			// Middle mouse button released
		IO_MOUSE_WHEEL,				// Mouse wheel
		IO_MOUSE_X1_DOWN,			// First X mouse button pressed
		IO_MOUSE_X1_UP,				// First X mouse button released
		IO_MOUSE_X2_DOWN,			// Second X mouse button pressed
		IO_MOUSE_X2_UP,				// Second X mouse button released
		IO_MOUSE_SET_CURSOR,		// Set the mouse cursor
		IO_MOUSE_END,

		// Keyboard Events
		//	m_Modifiers will refer to RgscKeyModifier
		IO_KEYBOARD_BEGIN,
		IO_KEY_DOWN,				// Key down (stored as virtual key, or KEY_... value as per input/keys.h)
		IO_KEY_CHAR,				// ASCII representation of keypress (with typematic repeat, suitable for user input)
		IO_KEY_UP,					// Key up (stored as virtual key, or KEY_... value as per input/keys.h)
		IO_KEYBOARD_END,

		IO_CUSTOM_BEGIN,
		IO_CONTROLLER_BUTTONS,		// Controller buttons pressed.
		IO_SOCIAL_CLUB_OPEN,		// Key/combo/controller input pressed to open the Social Club UI
		IO_SOCIAL_CLUB_CLOSE,		// Key/combo/controller input pressed to close the Social Club UI
		IO_WINDOW_MESSAGE,			// Raw message sent to the window proc
		IO_CUSTOM_END
	};

	static inline bool IsKeyboardEvent(ioEventType io) {
		return io > IO_KEYBOARD_BEGIN && io < IO_KEYBOARD_END;
	}

	static inline bool IsMouseEvent(ioEventType io) {
		return io > IO_MOUSE_BEGIN && io < IO_MOUSE_END;
	}

	static inline bool IsCustomEvent(ioEventType io) {
		return io > IO_CUSTOM_BEGIN && io < IO_CUSTOM_END;
	}
	
	ioEventType m_Type;	// Event type

	__int64 m_X;
	__int64 m_Y;
	__int64 m_Data;
	__int64 m_Modifiers;
};

class ioEventQueue
{
public:
	// PURPOSE: Pop an event from the event queue, if one is available
	// PARAMS: ev - Variable receiving event (valid only if function returns true)
	// RETURNS: True if event was available, else false
	static bool Pop(ioEvent& ev);

	// PURPOSE: Inserts an arbitrary event into the head of the queue
	// PARAMS: type - ioEventType from ioEvent structure
	//		x - X window-relative position of mouse (if mouse message)
	//		y - Y window-relative position of mouse (if mouse message)
	//		data - Button state, key data, or user payload
	// NOTES: No error is returned on a full queue.  The system maintains the
	//	event queue even if applications don't use it, so an error spew would
	//	be really irritating.
	static void Queue(ioEvent::ioEventType type,__int64 x,__int64 y,__int64 data, __int64 modifiers = 0);

	// PURPOSE: Obtain the next event from the queue without removing it
	// PARAMS: ev - Variable receiving event (valid only if function returns true)
	//		offset - Offset from start of queue to get event.  If a valid event
	//			is returned, offset is also incremented
	// RETURNS: True if event was available, else false
	// NOTES:
	// To scan the event queue without draining it, do this:
	// <CODE>
	// int offset = 0;
	// ioEvent EV;
	// while (Peek(EV,offset)) {
	//   // process events ...
	// } 
	// </CODE>
	static bool Peek(ioEvent& ev,int &offset);

	// PURPOSE: Resets the contents of the event queue
	static void Drain();

private:
	enum { MAX_EVENTS = 32 };

	static int sm_Head;
	static int sm_Tail;

	static ioEvent sm_TheQueue[MAX_EVENTS];
};

inline void ioEventQueue::Drain() 
{ 
	sm_Tail = sm_Head; 
}

} // namespace rgsc

#endif // INPUT_EVENTQ_H
