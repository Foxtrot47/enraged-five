// 
// patching.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "patching.h"
#include "diag/seh.h"
#include "json.h"
#include "rgsc.h"
#include "script.h"

using namespace rage;

namespace rgsc
{

RGSC_HRESULT Patching::QueryInterface(RGSC_REFIID riid, LPVOID* ppvObj)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObj == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IPatching*>(this);
	}
	else if(riid == IID_IPatchingV1)
	{
		pUnknown = static_cast<IPatchingV1*>(this);
	}

	*ppvObj = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

Patching::Patching()
{
}

Patching::~Patching()
{
}

bool Patching::Init()
{
	ClearCallbacks();
	return true;
}

void Patching::Update()
{

}

void Patching::Shutdown()
{
	ClearCallbacks();
}

void Patching::ClearCallbacks()
{
	m_AnimationCallback = NULL;
	m_AnimationCallbackData = NULL;

	m_ShowOptionsCallback = NULL;
	m_ShowOptionsCallbackData = NULL;

	m_ShowDialogCallback = NULL;
	m_ShowDialogCallbackData = NULL;

	m_SetProgressCallback = NULL;
	m_SetProgressCallbackData = NULL;
}

bool Patching::CanPerformTitleUpdate()
{
	return false;
}

RGSC_HRESULT Patching::InitPatchingUi(AnimationCallback callback, void* callbackData)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(GetRgscConcreteInstance()->_GetUiInterface()->IsReadyToAcceptCommands(), catchall, );
		//rverify(CanPerformTitleUpdate(), catchall, );

		//rverify(callback, catchall, hr = RGSC_INVALIDARG);

		m_AnimationCallback = callback;
		m_AnimationCallbackData = callbackData;

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT Patching::InitPatchingUiInternal(AnimationCallback callback, void* callbackData)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	//rtry
	{
		//rverify(callback, catchall, hr = RGSC_INVALIDARG);

		m_AnimationCallback = callback;
		m_AnimationCallbackData = callbackData;

		hr = RGSC_OK;
	}
	//rcatchall
	//{
	//
	//}

	return hr;
}

// RGSC_HRESULT Patching::CheckForSocialClubUpdate(SocialClubUpdateCallback callback, void* callbackData)
// {
// 	bool success = GetRgscConcreteInstance()->_GetUpdateManager()->CheckForUpdates(callback, callbackData);
// 	return success ? RGSC_OK : RGSC_FAIL;
// }
// 
// RGSC_HRESULT Patching::UpdateSocialClub(SocialClubUpdateCallback callback, void* callbackData)
// {
// 	bool success = GetRgscConcreteInstance()->_GetUpdateManager()->UpdateSocialClub(callback, callbackData);
// 
// }

RGSC_HRESULT Patching::ShowMessage(StringId messageId, const Utf8String utf8String, const bool modal, const bool animateSpinner)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		JSONNODE *n = json_new(JSON_NODE);

		json_set_name(n, "Data");

		if(utf8String)
		{
			json_push_back(n, json_new_a("DynamicMessage", utf8String));
		}

		json_push_back(n, json_new_i("Message", messageId));

		json_push_back(n, json_new_b("Modal", modal));

		json_push_back(n, json_new_b("AnimateSpinner", animateSpinner));

		rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(true,
																				 RgscUi::UI_STATE_PATCHING,
																				 RgscUi::UI_SUBSTATE_PATCHING_MESSAGE,
																				 n),
																				 catchall,
																				 hr = RGSC_FAIL);

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT Patching::SetProgress(StringId messageId, const Utf8String utf8String, const unsigned int progress, SetProgressCallback callback, void* callbackData)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(callback, catchall, hr = RGSC_INVALIDARG);

		JSONNODE *n = json_new(JSON_NODE);

		if(utf8String)
		{
			json_push_back(n, json_new_a("DynamicMessage", utf8String));
		}

		json_push_back(n, json_new_i("Message", messageId));

		json_push_back(n, json_new_i("Progress", progress));

		json_char *jc = json_write_formatted(n);

		json_delete(n);

		rlDebug2("%s\n", jc);

		bool succeeded = false;
		rgsc::Script::JsPatchingSetProgress(&succeeded, jc);
		json_free_safe(jc);
		rverify(succeeded, catchall, );

		m_SetProgressCallback = callback;
		m_SetProgressCallbackData = callbackData;

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT Patching::ShowOptions(const Utf8String message, const Utf8String* options, const bool* optional, const bool* hidden, const unsigned int numOptions, ShowOptionsCallback callback, void* callbackData)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(callback, catchall, hr = RGSC_INVALIDARG);

		JSONNODE *n = json_new(JSON_NODE);

		if(message)
		{
			json_push_back(n, json_new_a("Message", message));
		}

		json_push_back(n, json_new_i("NumOptions", numOptions));

		JSONNODE* c = json_new(JSON_ARRAY);
		rverify(c, catchall, );
		json_set_name(c, "Options");

		for(unsigned int i = 0; i < numOptions; i++)
		{
			JSONNODE* p = json_new(JSON_NODE);
			rverify(p, catchall, );

			json_push_back(p, json_new_i("Index", i + 1));
			json_push_back(p, json_new_a("Name", options[i]));
			json_push_back(p, json_new_b("Optional", optional[i]));
			json_push_back(p, json_new_b("Hidden", hidden[i]));

			json_push_back(c, p);
		}

		json_push_back(n, c);

		json_char *jc = json_write_formatted(n);

		json_delete(n);

		rlDebug2("%s\n", jc);

		bool succeeded = false;
		rgsc::Script::JsPatchingSetOptions(&succeeded, jc);
		json_free_safe(jc);
		rverify(succeeded, catchall, );

		m_ShowOptionsCallback = callback;
		m_ShowOptionsCallbackData = callbackData;

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT Patching::ShowDialog(StringId messageId, const Utf8String utf8String, DialogType type, const bool modal, ShowDialogCallback callback, void* callbackData)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(callback, catchall, hr = RGSC_INVALIDARG);

		JSONNODE *n = json_new(JSON_NODE);

		json_set_name(n, "Data");

		if(utf8String)
		{
			json_push_back(n, json_new_a("DynamicMessage", utf8String));
		}

		json_push_back(n, json_new_i("Message", messageId));

		json_push_back(n, json_new_i("DialogType", (int)type));

		json_push_back(n, json_new_b("Modal", modal));

		rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(true,
																				 RgscUi::UI_STATE_PATCHING,
																				 RgscUi::UI_SUBSTATE_PATCHING_DIALOG,
																				 n),
																				 catchall,
																				 hr = RGSC_FAIL);

		m_ShowDialogCallback = callback;
		m_ShowDialogCallbackData = callbackData;

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT Patching::ClosePatchingUi(bool showFinalMessage)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		ClearCallbacks();

		if(showFinalMessage)
		{
			bool succeeded = false;
			rgsc::Script::JsFinishedPatching(&succeeded, "");
			rverify(succeeded, catchall, );
		}
		else
		{
			if(GetRgscConcreteInstance()->_GetUiInterface()->IsVisible())
			{
 				rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(false,
 																						 RgscUi::UI_STATE_NONE,
 																						 RgscUi::UI_SUBSTATE_NONE,
 																						 NULL),
 																						 catchall,
 																						 hr = RGSC_FAIL);
			}
		}

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

void Patching::DialogResponse(const char* jsonResponse)
{
	if(AssertVerify(m_ShowDialogCallback))
	{
		int response = -1;

		JSONNODE *n = json_parse(jsonResponse);

		if(!AssertVerify(n != NULL))
		{
			return;
		}

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "response") == 0)
			{
				response = json_as_int(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);

		if(AssertVerify(response != -1))
		{
			m_ShowDialogCallback((DialogButton)response, m_ShowDialogCallbackData);
		}
	}
}

void Patching::OptionsResponse(const char* jsonResponse)
{
	if(AssertVerify(m_ShowOptionsCallback))
	{
		const unsigned int MAX_OPTIONS = 128;
		int numOptions = 0;
		int curOption = 0;

		struct InstallOption
		{
			InstallOption() : index(0), install(0), hide(0) {}

			u8 index;
			u8 install;
			u8 hide;
		};

		InstallOption options[MAX_OPTIONS];

		JSONNODE *n = json_parse(jsonResponse);

		if(!AssertVerify(n != NULL))
		{
			return;
		}

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "numOptions") == 0)
			{
				numOptions = json_as_int(*i);
			}

			if(json_type(*i) == JSON_ARRAY)
			{
				JSONNODE_ITERATOR j = json_begin(*i);

				while(j != json_end(*i))
				{
					if(!AssertVerify(*j != NULL))
					{
						return;
					}

					if(json_type(*j) == JSON_NODE)
					{
						JSONNODE_ITERATOR k = json_begin(*j);
						while(k != json_end(*j))
						{
							if(!AssertVerify(*k != NULL))
							{
								return;
							}

							// get the node name and value as a string
							json_char *node_name2 = json_name(*k);

							if(_stricmp(node_name2, "index") == 0)
							{
								options[curOption].index = (u8)json_as_int(*k);
							}
							else if(_stricmp(node_name2, "install") == 0)
							{
								options[curOption].install = json_as_int(*k) != 0 ? 1 : 0;
							}
							else if(_stricmp(node_name2, "hide") == 0)
							{
								options[curOption].hide = json_as_int(*k) != 0 ? 1 : 0;
							}

							// cleanup and increment the iterator
							json_free_safe(node_name2);

							++k;
						}

						++curOption;
					}

					++j;
				}
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);

		// re-order them back to their original order
		struct SortPredicate
		{
			static bool Sort(InstallOption& a, InstallOption& b)
			{
				return a.index < b.index;
			}
		};

		std::sort(options, options + numOptions, SortPredicate::Sort);

		u8 install[MAX_OPTIONS] = {0};
		u8 hide[MAX_OPTIONS] = {0};

		for(int i = 0; i < numOptions; i++)
		{
			install[i] = options[i].install;
			hide[i] = options[i].hide;
		}

		m_ShowOptionsCallback(install, hide, numOptions, m_ShowOptionsCallbackData);
	}
}

void Patching::ProgressResponse(const char* jsonResponse)
{
	if(AssertVerify(m_SetProgressCallback))
	{
		bool stateExists = false;
		int state = 0;

		JSONNODE *n = json_parse(jsonResponse);

		if(!AssertVerify(n != NULL))
		{
			return;
		}

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "state") == 0)
			{
				stateExists = true;
				state = json_as_int(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);

		if(stateExists)
		{
			m_SetProgressCallback(state, m_SetProgressCallbackData);
		}
	}
}

void Patching::AnimationState(const char* jsonResponse)
{
	if(m_AnimationCallback)
	{
		bool stateExists = false;
		int state = 0;

		JSONNODE *n = json_parse(jsonResponse);

		if(!AssertVerify(n != NULL))
		{
			return;
		}

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "state") == 0)
			{
				stateExists = true;
				state = json_as_int(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);

		if(stateExists)
		{
			m_AnimationCallback(state, m_AnimationCallbackData);
		}
	}
}

} // namespace rgsc
