//
// system/timer.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "timer.h"

#pragma comment(lib, "winmm.lib")
#include <windows.h>

float sysTimerConsts::TicksToSeconds=0.0f;
float sysTimerConsts::TicksToMilliseconds=0.0f;
float sysTimerConsts::TicksToMicroseconds=0.0f;
float sysTimerConsts::CpuSpeed;

unsigned sysTimer::GetSystemMsTime() {
	// This timer is not expected to wrap around during a single run!
	return timeGetTime();
}

utimer_t sysTimer::GetTicks() {
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return li.QuadPart;
}

sysTimer::sysTimer() {
	if (!GetTicksToSeconds()) {
		// Correlate internal timer to external timer.
		__try { 
			utimer_t cy;
			LARGE_INTEGER tmp, tmp2, freq;
			QueryPerformanceFrequency(&freq);
			QueryPerformanceCounter(&tmp);
			cy = GetTicks();
			Sleep(100);	// duration doesn't really matter
			QueryPerformanceCounter(&tmp2);
			cy = GetTicks() - cy;

			double elapsed = (double)tmp2.QuadPart - (double)tmp.QuadPart;
			// count / counts/sec = seconds.
			elapsed /= (double)freq.QuadPart;

			sysTimerConsts::TicksToMilliseconds = (float)((elapsed * 1000.0) / cy);
			sysTimerConsts::TicksToMicroseconds = sysTimerConsts::TicksToMilliseconds * 1000.0f;
			sysTimerConsts::TicksToSeconds = sysTimerConsts::TicksToMilliseconds / 1000.0f;
			sysTimerConsts::CpuSpeed  = 0.001f / sysTimerConsts::TicksToMilliseconds;
		}
		__except(1) {
			//printf("rdtsc instruction not supported on your CPU.");
		}
	}
	Reset();
}
