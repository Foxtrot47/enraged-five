//
// input/pad.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef RGSC_INPUT_PAD_H
#define RGSC_INPUT_PAD_H

// dinput/xinput support deprecated for the x64 which now requires the game to send all joystick input virtually
// this reduces alleged deadlock issues with dinput/xinput running within the main app and the library,
// and allows us a broader range of controller support (steam, ps4, etc).
#define RGSC_DINPUT_SUPPORT (RSG_CPU_X86)
#define RGSC_XINPUT_SUPPORT (RSG_CPU_X86)

#include "pad_interface.h"
#include "rgsc_common.h"

// rage includes
#include "system/criticalsection.h"

using namespace rage;

namespace rgsc
{

#if RGSC_XINPUT_SUPPORT
class ioXInputPad : public RgscGamepad
{
public:
	// ===============================================================================================
	// IGamePadV2
	// ===============================================================================================
	bool RGSC_CALL HasInput() const { return true; }

public:
	void Update();
	void Refresh();
};
#endif // RGSC_XINPUT_SUPPORT

#if RGSC_DINPUT_SUPPORT
class ioD3DPad : public RgscGamepad
{
public:
	// ===============================================================================================
	// IGamePadV2
	// ===============================================================================================
	bool RGSC_CALL HasInput() const { return true; }

public:
	void Update();
	void Refresh();
};
#endif // RGSC_DINPUT_SUPPORT

// error C4265: class has virtual functions, but destructor is not virtual
// the binary interface for virtual destructors isn't standardized, so don't make the destructor virtual
#pragma warning(push)
#pragma warning(disable: 4265)

class RgscGamePadManager : public IGamepadManagerLatestVersion
{
public:

	// ===============================================================================================
	// inherited from IRgscUnknown
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

	// ===============================================================================================
	// inherited from IGamepadManagerV1
	// ===============================================================================================
	virtual RGSC_HRESULT RGSC_CALL SetMarshalledGamepads(IGamepad** gamePads, unsigned numGamePads);
	virtual void RGSC_CALL ClearMarshalledGamepads();

	// ===============================================================================================
	// inherited from IGamepadManagerV2
	// ===============================================================================================
	virtual void RGSC_CALL NotifyGamepadsChanged();

	// ===============================================================================================
	// inherited from IGamepadManagerV3
	// ===============================================================================================
	virtual void RGSC_CALL SetBackgroundInputEnabled(bool bEnabled);

	// ===============================================================================================
	// inherited from IGamepadManagerV4
	// ===============================================================================================
	virtual void RGSC_CALL SetSimulateKeyboardEventsEnabled(bool bEnabled);
	virtual void RGSC_CALL SetSimulateMouseEventsEnabled(bool bEnabled);
	virtual void RGSC_CALL SetSendPadInputToBrowserEnabled(bool bEnabled);

	// ===============================================================================================
	// accessible from anywhere in the dll
	// ===============================================================================================

	// PURPOSE
	// Constructors
	RgscGamePadManager();
	~RgscGamePadManager();

	// PURPOSE
	//	Initialize all possible pads on system
	void BeginAll();

	// PURPOSE
	//	Updates all possible pads on system (gets current values, identifies buttons that have changed)
	void UpdateAll();

	// PURPOSE
	//	Returns a count of plugged in pads
	int CountPads();

	// RETURNS: Number of pads for an input mode.
	const int GetNumPads(IGamepadManagerV1::InputMode mode);

	// RETURNS: Reference to the specified pad object (always numbered left to right)
	// PARAMS: Index of pad to return
	inline IGamepad* GetPad(IGamepadManagerV1::InputMode mode, unsigned index);

	// PURPOSE
	//	Checks to see if a new device has been plugged in
	void CheckForNewlyPluggedInDevices();

	// PURPOSE
	//	Returns the number of marshaled gamepads
	int GetNumMarshalledPads() { return m_NumMarshalledPads; }

	// PURPOSE
	//	Returns true if background input is enabled.
	bool IsBackgroundInputEnabled() const;

	// PURPOSE
	//	Returns true if the input system should simulate keyboard/mouse events based on gamepad input.
	bool IsSimulatingKeyboardEvents() const;
	bool IsSimulatingMouseEvents() const;

	// PURPOSE
	//	Returns true if we're forwarding gamepad input to the browser.
	bool IsForwardingPadInputToBrowser() const;

private:

	// Queue SCUI updates so they only execute once per 5 seconds if the game is spammy with pad changes.
	//  i.e. User is rapidly using both gamepad and kbm.
	static const int MIN_SCUI_UPDATE_INTERVAL_MS = 5000;

	// PURPOSE
	//  Clears out the Pad arrays
	void Clear();

	// PURPOSE
	//	Sends the controller configuration to the SCUI
	void SendConfigurationToScui();

#if RGSC_DINPUT_SUPPORT
	ioD3DPad m_D3DPads[RGSC_MAX_PADS];
#endif
#if RGSC_XINPUT_SUPPORT
	ioXInputPad m_XInputPads[RGSC_MAX_PADS];
#endif

	IGamepad** m_MarshalledPads;
	unsigned m_NumMarshalledPads;

	u32 m_TimeOfLastScuiUpdate;
	bool m_bSendConfigurationToScui;
	bool m_bBackgroundInputEnabled;
	bool m_bSimulateKeyboardEvents;
	bool m_bSimulateMouseEvents;
	bool m_bSendGamepadInputToBrowser;

	static sysCriticalSectionToken sm_Cs;

};

#pragma warning(pop)

inline IGamepad* RgscGamePadManager::GetPad(IGamepadManagerLatestVersion::InputMode mode, unsigned index) 
{
	FastAssert(mode == IGamepadManagerLatestVersion::INPUTMODE_D3D || 
				mode == IGamepadManagerLatestVersion::INPUTMODE_XINPUT || 
				mode == IGamepadManagerLatestVersion::INPUTMODE_MARSHALLED);

	if (mode == IGamepadManagerLatestVersion::INPUTMODE_MARSHALLED)
	{
		FastAssert(index >= 0 && index < m_NumMarshalledPads);
		if (index < m_NumMarshalledPads)
		{
			return m_MarshalledPads[index];
		}
		else
		{
			return NULL;
		}
	}
#if RGSC_DINPUT_SUPPORT
	else if (mode == IGamepadManagerLatestVersion::INPUTMODE_D3D)
	{
		FastAssert(index >= 0 && index < RGSC_MAX_PADS);
		return (IGamepad*)&m_D3DPads[index];
	}
#endif // RGSC_DINPUT_SUPPORT
#if RGSC_XINPUT_SUPPORT
	else if (mode == IGamepadManagerLatestVersion::INPUTMODE_XINPUT)
	{
		FastAssert(index >= 0 && index < RGSC_MAX_PADS);
		return (IGamepad*)&m_XInputPads[index];
	}
#endif // RGSC_XINPUT_SUPPORT

	return NULL;
}

} // namespace rgsc

#endif // RGSC_INPUT_PAD_H