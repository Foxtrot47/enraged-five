#ifndef DOXYGEN_PREPROCESSOR
#include "script.h"
#include "js2c_helper.h"
#include "string/string.h"
#include "stringutil.h"
#include "rgsc.h"
#include "rgsc_ui.h"
#endif

namespace rgsc
{

typedef RgscWideString json;

/** @defgroup JS2C_API Javascript to C++ API
 *  @{
 */

//! Retrieves version information for the Social Club SDK and the currently running game.
/**
	\return
		Version information in json format.
	\remark
	Example:
	\verbatim
	{
		"Version" : "1.2.3.4"
		"TitleVersion" : "1.0.1.0"

	}
	Version is the version of the Social Club SDK.\n
	TitleVersion is the version of the currently running game.
	\endverbatim
*/
RgscUtf8String RGSC_GET_VERSION_INFO()
{
	return RgscUtf8String::point_to(GetRgscConcreteInstance()->GetVersionInfoAsJson());
}

//! Javascript calls this when a sign in has occurred.
/**
	\param
		profile the profile being signed in.
	\remark
		This function should be called after attempting to sign into ROS for any reason.\n
		This includes signing-in after creating a new profile, when signing-in in response
		to RGSC_JS_SIGN_IN(), etc.\n
		If signing into ROS fails, this function must still be called to sign-in locally.\n
		The UI should continue animating the 'signing in' widget until RGSC_JS_FINISH_SIGN_IN() is called in response.

	Example: when signing into a Social Club account:
	\verbatim
	{
		"RockstarId" : "272617",
		"Email" : "pestilence@rockstartoronto.com",
		"Password" : "pesty123",
		"Nickname" : "Pestilence",
		"AvatarUrl" : "GTAIV/GTAIV3.png"
		"SaveEmail" : false,
		"SavePassword" : false,
		"SaveMachine" : false,
		"AutoSignIn" : false,
		"Ticket" : "uSZdACKPfjSxmzUUUGSH6UeYlcAQ7lEXdDJb9r1v7X2Sxw8eVMaolWwFJhluNWxVZYU35Lshkh/VsP4A7AtEVks7JSxR97TShMXjQEibQ5kOBsWD5RuO+M/zqdvvSZbemgPdxYc=",
		"ScAuthToken" : "uSZdACKPfjSxmzUUUGSH6UeYlcAQ7lEXdDJb9r1v7X2Sxw8eVMaolWwFJhluNWxVZYU35Lshkh/VsP4A7AtEVks7JSxR97TShMXjQEibQ5kOBsWD5RuO+M/zqdvvSZbemgPdxYc=",
		"SessionKey" : "OxOx7ZBgJQJj234dMFB7Rg==",
		"XmlResponse" : "<full xml response returned from ROS>"
		"Expiration" : 600,
		"AccountId" : 2822456,
		"CallbackData" : 1
		"TicketRefresh" : true // OPTIONAL - Is included when refreshing a ticket with email/ticket.
	}
	\endverbatim
	Note: if CallbackData is supplied (and its value is different from -1), this value will be passed back to RGSC_JS_FINISH_SIGN_IN().

	Example: when signing into an existing Local profile:
	\verbatim
	{
		"RockstarId" : "2146061124947649993",
		"Nickname" : "Witch Slayer"
		"Local" : true,
		"AutoSignIn" : false,
		"CallbackData" : 1
	}
	\endverbatim

	Example: when signing into a new Local profile (i.e. creating a new Local profile):
	\verbatim
	{
		"Nickname" : "Pollywog"
		"Local" : true,
		"AutoSignIn" : false,
		"CallbackData" : 1
	}
	\endverbatim

	Example: when signin error occurs
	\verbatim
	{
	"Error" : {
			"ErrorCode" : "SomeError",
			"ErrorCodeEx" : "SomeDetailedError"
		}
	}
	\endverbatim

	\see RGSC_JS_SIGN_IN()
	\see RGSC_JS_FINISH_SIGN_IN()

	\dot
	digraph AutoSignInProtocol {
		label = "Auto Sign-In Protocol";
		graph [truecolor bgcolor="#00000000"]
		node [shape=record, fontname=Arial, fontsize=10];
		rankdir=LR;
		subgraph cluster0 {
			label = "C++";
			AutoSignIn [shape=ellipse, label="Auto Sign-In\nProfile Available"]
			RGSC_SIGN_IN [color=red, URL="\ref RGSC_SIGN_IN()"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "Javascript";
			RGSC_JS_SIGN_IN [URL="\ref RGSC_JS_SIGN_IN()"];
			RGSC_JS_FINISH_SIGN_IN [URL="\ref RGSC_JS_FINISH_SIGN_IN()"];
			color=lightgrey;
		}

		AutoSignIn -> RGSC_JS_SIGN_IN -> RGSC_SIGN_IN -> RGSC_JS_FINISH_SIGN_IN;
	}
	\enddot

	\dot
	digraph SignInProtocol {
		label = "Sign-In Protocol";
		graph [truecolor bgcolor="#00000000"]
		node [shape=record, fontname=Arial, fontsize=10];
		rankdir=LR;
		subgraph cluster0 {
			label = "C++";
			RGSC_SIGN_IN [color=red, URL="\ref RGSC_SIGN_IN()"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "Javascript";
			UserSignIn [shape=ellipse, label="User Signs In"]
			RGSC_JS_FINISH_SIGN_IN [URL="\ref RGSC_JS_FINISH_SIGN_IN()"];
			color=lightgrey;
		}

		UserSignIn -> RGSC_SIGN_IN -> RGSC_JS_FINISH_SIGN_IN;
	}
	\enddot
*/
void RGSC_SIGN_IN(json profile)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(profile); 
	GetRgscConcreteInstance()->_GetProfileManager()->SignInResponse(utf8.data());
	stringUtil_free(utf8);
}

//! Retrieves the list of stored profiles, sorted descending by last sign-in time.
/**
	\return
		The list of profiles in json format.
	\remark
		Must be signed out to call this function.\n
	Example list of profiles:
	\verbatim
	{
		"NumProfiles" : 3,
		"Profiles" : [
			{
				"RockstarId" : "28942581238732",
				"Nickname" : "Pestilence",
				"LastSignInTime" : "134134123",
				"SaveEmail" : true,
				"SavePassword" : true,
				"AutoSignIn" : false,
				"Local" : false,
				"Email" : "pestilence@rockstartoronto.com",
				"Password" : "pesty12",
				"AvatarUrl" : "GTAIV/GTAIV3.png",
				"RememberedMachineToken" : "1234567890abcdefghijkl=="
			},
			{
				"RockstarId" : "138871347913491",
				"Nickname" : "TempNickname",
				"LastSignInTime" : "1143413414",
				"SaveEmail" : false,
				"SavePassword" : false,
				"AutoSignIn" : false,
				"Local" : false,
				"AvatarUrl" : "",
				"RememberedMachineToken" : "1234567890abcdefghijkl=="
			}
			{
				"RockstarId" : "7734266369619707088",
				"Nickname" : "MyOfflineProfile",
				"LastSignInTime" : "134133241",
				"AutoSignIn" : false,
				"Local" : true,
				"ProfileNumber" : 1,
				"AvatarUrl" : "",
				"RememberedMachineToken" : "1234567890abcdefghijkl=="
			}
		]
	}
	\endverbatim
	LastSignInTime is the number of seconds since 1/1/1970 @ 12:00am UTC.\n
	Email will only be given if SaveEmail is true.\n
	Password will only be given if SavePassword is true.\n
	ProfileNumber will only be given if Local = true.\n
	A profile's AvatarUrl can be passed to RGSC_GET_AVATAR() to get the avatar image associated with the profile.
	\see RGSC_GET_AVATAR()
*/
RgscUtf8String RGSC_GET_PROFILE_LIST()
{
 	return RgscUtf8String::point_to(GetRgscConcreteInstance()->_GetProfileManager()->GetProfileList());
}

//! Retrieves a list of stored profiles, filtered by a custom filter, sorted descending by last sign-in time.
/**
	\return
		The list of profiles in json format.
	\remark
		Must be signed out to call this function.\n

	Example jsonRequest:
	\verbatim
	{
		"RockstarId" : "28942581238732"
	}
	\endverbatim

	Example list of profiles:
	\verbatim
	{
		"NumProfiles" : 1,
		"Profiles" : [
			{
				"RockstarId" : "28942581238732",
				"Nickname" : "Pestilence",
				"LastSignInTime" : "134134123",
				"SaveEmail" : true,
				"SavePassword" : true,
				"AutoSignIn" : false,
				"Local" : false,
				"Email" : "pestilence@rockstartoronto.com",
				"Password" : "pesty12",
				"AvatarUrl" : "GTAIV/GTAIV3.png",
				"RememberedMachineToken" : "1234567890abcdefghijkl=="
			}
		]
	}
	\endverbatim
	Currently supports filtering by RockstarId or Nickname to request a single profile.
	Other filters/instructions can be added in the future.
	LastSignInTime is the number of seconds since 1/1/1970 @ 12:00am UTC.\n
	Email will only be given if SaveEmail is true.\n
	Password will only be given if SavePassword is true.\n
	ProfileNumber will only be given if Local = true.\n
	A profile's AvatarUrl can be passed to RGSC_GET_AVATAR() to get the avatar image associated with the profile.
	\see RGSC_GET_AVATAR()
*/
RgscUtf8String RGSC_GET_PROFILE_LIST_FILTERED(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	const char* profiles = GetRgscConcreteInstance()->_GetProfileManager()->GetProfileList(utf8.data());
	stringUtil_free(utf8);
	return RgscUtf8String::point_to(profiles);
}

//! Updates the currently signed-in profile with the details provided.
/**
	\param
		changes the profile data that is being modified.
	\return
		true on success, false if an error occurred.
	\remark
		The player must be signed online for this function to succeed.\n
		Only provide the information that is changing, omit fields that are not changing.

	Example changes:
	\verbatim
	{
		"Email" : "me@here.com",
		"Password" : "mypassword",
		"Nickname" : "mynickname",
		"AvatarUrl" : "GTAIV/GTAIV3.png",
		"SaveEmail" : true,
		"AutoSignIn" : false
	}
	\endverbatim
	\see RGSC_JS_FINISH_MODIFY_PROFILE()

	\dot
	digraph ModifyProfileProtocol {
		label = "Modify Profile Protocol";
		graph [truecolor bgcolor="#00000000"]
		node [shape=record, fontname=Arial, fontsize=10];
		rankdir=LR;
		subgraph cluster0 {
			label = "C++";
			RGSC_MODIFY_PROFILE [color=red, URL="\ref RGSC_MODIFY_PROFILE()"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "Javascript";
			UserModifiesProfile [shape=ellipse, label="User Modifies Profile"]
			RGSC_JS_FINISH_MODIFY_PROFILE [URL="\ref RGSC_JS_FINISH_MODIFY_PROFILE()"];
			color=lightgrey;
		}

		UserModifiesProfile -> RGSC_MODIFY_PROFILE -> RGSC_JS_FINISH_MODIFY_PROFILE;
	}
	\enddot
*/
bool RGSC_MODIFY_PROFILE(json changes)
{
	bool success = false;
	RgscUtf8String utf8 = rgsc::WideToUTF8(changes); 
	success = GetRgscConcreteInstance()->_GetProfileManager()->ModifyProfile(utf8.data());
	stringUtil_free(utf8);
	return success;
}

//! Deletes a profile off the local hard drive.
/**
	\param
		profile the information that uniquely identifies the profile to delete (see below).
	\return
		true on success, false if an error occurred.
	\remark
		Must be signed out to call this function.\n
		If a local profile gets deleted, all save files created by any game using that
		profile will be permanently inaccessible. Achievement data will also be deleted.
		This action should be confirmed by the user with appropriate UI.

	Example profile:
	\verbatim
	{
		"RockstarId" : "3484828129",
	}
	\endverbatim
*/
bool RGSC_DELETE_PROFILE(json profile)
{
	bool success = false;
	RgscUtf8String utf8 = rgsc::WideToUTF8(profile); 
	success = GetRgscConcreteInstance()->_GetProfileManager()->DeleteProfile(utf8.data());
	stringUtil_free(utf8);
	return success;
}

//! Javascript calls this when a sign out has occurred.
/**
*/
void RGSC_SIGN_OUT()
{
	GetRgscConcreteInstance()->_GetProfileManager()->SignOut();
	GetRgscConcreteInstance()->_GetProfileManager()->DeleteAutoSignInData();
}

//! Opens a url in the user's default web browser.
/**
	\param
		jsonRequest the url to open (see below).
	\remark
	Example url:
	\verbatim
	{
		"Url" : "www.rockstargames.com"
	}
	\endverbatim
	\return
		true on success, false if an error occurred.
*/
bool RSGC_LAUNCH_EXTERNAL_WEB_BROWSER(json jsonRequest)
{
	bool success = false;
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	success = GetRgscConcreteInstance()->_GetUiInterface()->LaunchExternalWebBrowser(utf8.data());
	stringUtil_free(utf8);
	return success;
}

//! Retrieves an avatar png image encoded as base64.
/**
	\param
		jsonRequest the avatar request (see below)
	\remark
	Example jsonRequest:
	\verbatim
	{
		"AvatarUrl" : "GTAIV/GTAIV3.png"
		"LargeVersion" : true
	}
	\endverbatim
	LargeVersion - true: retrieves the 64x64 image. false: retrieves the 32x32 image.
	\return
		The avatar png image encoded as base64 if the call was successful.\n
		An empty string if an error occurred or if the image isn't available locally.
	\see RGSC_GET_PROFILE_LIST()
*/
RgscUtf8String RGSC_GET_AVATAR(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	const char* avatar = GetRgscConcreteInstance()->_GetGamerPicManager()->GetGamerPicBase64(utf8.data());
	stringUtil_free(utf8);
	return RgscUtf8String::point_to(avatar);
}

//! Retrieves the list of achievements in json format.
/**
	\return
		The list of achievements in json format.\n
		An empty string if an error occurred.
	\remark
		Hidden achievements should not be shown in the UI unless they are achieved (i.e. show = Achieved || Visible).\n
		Aggregate metadata is provided so the UI can show information such as "21 of 50 achievements earned, 595/1000 points earned."\n
		To get the image for an achievement, call RGSC_GET_ACHIEVEMENT_IMAGE().

		Sort order:
		- Earned achievements are sorted in most-recently-earned order.\n
		- Un-earned, non-hidden Achievements are sorted (ascending) by AchievementID.\n
		- Un-earned, hidden Achievements are sorted (ascending) by AchievementID.\n

	Example list of achievements:
	\verbatim
	{
		"NumAchievements" : 40,
		"NumAchievementsAwarded" : 18,
		"NumPointsAwarded" : 595,
		"MaxPoints" : 1000,
		"Achievements" : [
			{
				"Id" : 1,
				"Name" : "Police Academy",
				"Description" : "You completed all cases on the Patrol desk.",
				"HowTo" : "Complete all cases on the Patrol desk.",
				"Type" : 1,
				"Visible" : false,
				"Points" : 15,
				"Achieved" : true,
				"DateAchieved" : 1341341134,
				"Progress" : 10
				"MaxProgress" : 10
			},
			{
				"Id" : 2,
				"Name" : "Paved With Good Intentions",
				"Description" : "You completed all cases on the Traffic desk.",
				"HowTo" : "Complete all cases on the Traffic desk.",
				"Type" : 1,
				"Visible" : false,
				"Points" : 15,
				"Achieved" : false,
				"DateAchieved" : 0
				"Progress" : 5,
				"MaxProgress" : 10
			},
			...
		]
	}
	\endverbatim
	DateAchieved is the number of seconds since 1/1/1970 @ 12:00am UTC. A value of 0 indicates that the achievement was awarded offline.\n
	\see RGSC_GET_ACHIEVEMENT_IMAGE()
*/
RgscUtf8String RGSC_GET_ACHIEVEMENT_LIST()
{
	return RgscUtf8String::point_to(GetRgscConcreteInstance()->_GetAchievementManager()->ReadLocalAchievementsAsJson(0, IAchievementManager::MAX_NUM_ACHIEVEMENTS));
}

//! Retrieves an achievement png image encoded as base64.
/**
	\param
		jsonRequest the requested achievement image (see below).
	\remark
	Example jsonRequest:
	\verbatim
	{
		"AchievementId" : 42
	}
	\endverbatim
	\return
		An achievement png image encoded as base64.\n
		An empty string if an error occurred.
	\see RGSC_GET_ACHIEVEMENT_LIST()
*/
RgscUtf8String RGSC_GET_ACHIEVEMENT_IMAGE(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	const char* image = GetRgscConcreteInstance()->_GetAchievementManager()->GetAchievementImageAsJson(utf8.data());
	stringUtil_free(utf8);
	return RgscUtf8String::point_to(image);
}

//! Returns the title id information necessary to call ROS services.
/**
	\return
		the title id information necessary to call ROS services.
	\remark
	The information in the title id should be used to construct ROS web service URLs.\n
	The language code should be used to localize text.\n
	Applications not manifested for Windows 8.1 or Windows 10 will return the Windows 8 OS version value (6.2) for OsVersionMajor\OsVersionMinor.\n
	Example title id:
	\verbatim
	{
		"RosEnvironment" : "dev",
		"RosPlatform" : "pcros",
		"RosPlatformId" : 8,
		"RosTitleName" : "lanoire",
		"RosTitleVersion" : 8,
		"RosAuthServices" : 0,
		"TitleDisplayName" : "L.A. Noire",
		"Platform" : "PC",
		"Language" : "en-US"   
		"IsLauncher" : false,
		"EnableLocalProfiles" : false,
		"OfflineOnly" : true,
		"OsVersionMajor" : 6,
		"OsVersionMinor" 1,
		"Fingerprint" : 0123456789,
		"FingerprintKvp" : [
			{
				"someKey" : "testvalue"
			},
			{
				"anotherKey" : "abcdefg123="
			}
		]
		"EnableDebugLog" : true
	}
	\endverbatim
	- Possible RosEnvironments:\n
		- dev\n
		- prod\n
		- rage\n
		- cert\n
		- unknown\n
	- Possible Platforms:\n
		- PC\n
		- Steam\n
		- OnLive\n
		- NHN\n
		- Mac\n
		- MacSteam\n
		- MacAppStore\n
	- Possible language codes:\n
		- en-US - English\n
		- es-ES - Spanish\n
		- fr-FR - French\n
		- de-DE - German\n
		- it-IT - Italian\n
		- ja-JP - Japanese\n
		- ru-RU - Russian\n
		- pt-PT - Portuguese\n
		- pl-PL - Polish\n
		- ko-KR - Korean\n
	- Possible RosAuthServices:\n
		- 0 - RGSC_AUTH_ASMX		- Original ASP service(default)
		- 1 - RGSC_AUTH_LEGACY_SVC	- WCF service that uses same protocol as ASP service (POST form request, XML response)
		- 2 - RGSC_AUTH_SVC			- WCF service that uses JSON for both request and response.
*/
RgscUtf8String RGSC_GET_TITLE_ID()
{
	return RgscUtf8String::point_to(GetRgscConcreteInstance()->GetTitleIdAsJson());
}

//! Returns the ROS ticket and any other credentials needed to call ROS web services.
/**
	\return
		the ROS ticket and any other credentials needed to call ROS web services.
	\remark
		This function can only be called after RGSC_JS_FINISH_SIGN_IN() has been called.\n
		ROS credentials expire every x seconds. The UI should never cache credentials.\n
		Instead, request the credentials from the DLL whenever you need to use them.

	Example credentials:
	\verbatim
	{
		"Ticket" : <base64 encoded ticket>
	}
	\endverbatim
*/
RgscUtf8String RGSC_GET_ROS_CREDENTIALS()
{
	return RgscUtf8String::point_to(GetRgscConcreteInstance()->_GetProfileManager()->GetRosCredentials());
}

//! This is called in response to RGSC_JS_REQUEST_UI_STATE() when the UI is ready to start or stop being rendered.
/**
	\param
		response the response to the previous request made by RGSC_JS_REQUEST_UI_STATE().
	\remark
	Example response:
	\verbatim
	{
		"Visible" : true
	}
	\endverbatim
	The UI will start rendering if the Visible flag is true.\n
	The UI will stop rendering if the Visible flag is false.
	\see RGSC_JS_REQUEST_UI_STATE()

	\dot
	digraph StateRequestResponseProtocol {
		label = "Solicited State Request/Response Protocol";
		graph [truecolor bgcolor="#00000000"]
		node [shape=record, fontname=Arial, fontsize=10];
		rankdir=LR;
		subgraph cluster0 {
			label = "C++";
			StateRequested [shape=ellipse, label="The player or programmer\nwants to open or\nclose the UI"]
			RGSC_UI_STATE_RESPONSE [color=red, URL="\ref RGSC_UI_STATE_RESPONSE()"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "Javascript";
			RGSC_JS_REQUEST_UI_STATE [URL="\ref RGSC_JS_REQUEST_UI_STATE()"];
			color=lightgrey;
		}

		StateRequested -> RGSC_JS_REQUEST_UI_STATE -> RGSC_UI_STATE_RESPONSE;
	}
	\enddot

	\dot
	digraph Example {
		label = "Example flow: User wants to open the Sign-In/Sign-Up UI";
		graph [truecolor bgcolor="#00000000"]
		node [shape=record, fontname=Arial, fontsize=10];
		rankdir=LR;
		subgraph cluster0 {
			label = "C++";
			Closed [shape=ellipse, label="UI is closed"];
			StateRequested [shape=ellipse, label="The player wants\nto open the\nSign-In/Sign-Up UI"];
			RGSC_UI_STATE_RESPONSE [color=red, URL="\ref RGSC_UI_STATE_RESPONSE()"];
			StartsRendering [shape=ellipse, label="DLL starts\nrendering the UI"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "Javascript";
			RGSC_JS_REQUEST_UI_STATE [URL="\ref RGSC_JS_REQUEST_UI_STATE()"];
			JSPrepares [shape=ellipse, label="Javascript prepares\nto open the\nSign-In/Sign-Up UI"];
			JSAnimates [shape=ellipse, label="Javascript animates\nthe opening of the\nSign-In/Sign-Up UI"];
			color=lightgrey;
		}

		Closed -> StateRequested -> RGSC_JS_REQUEST_UI_STATE -> JSPrepares -> RGSC_UI_STATE_RESPONSE -> StartsRendering -> JSAnimates;
	}
	\enddot
*/
void RGSC_UI_STATE_RESPONSE(json response)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(response); 
	GetRgscConcreteInstance()->_GetUiInterface()->Js2CUiStateResponse(utf8.data());
	stringUtil_free(utf8);
}

//! Requests the DLL to start or stop rendering the UI. This allows the UI to show itself whenever it needs to.
/**
	\param
		request the state the UI is requesting.
	\remark
		The UI will not be shown or hidden until the DLL responds with RGSC_JS_UI_STATE_RESPONSE().\n
		There can potentially be a long delay before getting back a RGSC_JS_UI_STATE_RESPONSE().\n
		The UI should wait until getting the response before taking action (such as animating in the UI, etc.).\n
		It is possible that the DLL will refuse a request. For example, if a request is being made to start
		rendering the UI but the DLL cannot comply due to some condition, it will respond with "Visible" : false.\n

	Example request:
	\verbatim
	{
		"Visible" : true
	}
	\endverbatim
	\see RGSC_JS_UI_STATE_RESPONSE()

	\dot
	digraph StateRequestResponseProtocol {
		label = "Unsolicited State Request/Response Protocol";
		graph [truecolor bgcolor="#00000000"]
		node [shape=record, fontname=Arial, fontsize=10];
		rankdir=LR;
		subgraph cluster0 {
			label = "C++";
			RGSC_REQUEST_UI_STATE [color=red, URL="\ref RGSC_REQUEST_UI_STATE()"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "Javascript";
			StateRequested [shape=ellipse, label="The UI wants\nto open or\nclose itself"]
			RGSC_JS_UI_STATE_RESPONSE [URL="\ref RGSC_JS_UI_STATE_RESPONSE()"];
			color=lightgrey;
		}

		StateRequested -> RGSC_REQUEST_UI_STATE -> RGSC_JS_UI_STATE_RESPONSE;
	}
	\enddot
*/
void RGSC_REQUEST_UI_STATE(json request)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(request); 
	GetRgscConcreteInstance()->_GetUiInterface()->Js2CRequestUiState(utf8.data());
	stringUtil_free(utf8);
}

//! Notifies the DLL that the web page and javascript functions have finished loading and it is ready to accept function calls from C++.
/**
	\remarks
		The UI will not be rendered until this function is called.
*/
void RGSC_READY_TO_ACCEPT_COMMANDS()
{
	GetRgscConcreteInstance()->_GetUiInterface()->SetReadyToAcceptCommands();
}

//! Javascript calls this when the user has confirmed the corrupt license dialog.
/**
	\remark
	The corrupt license dialog can be requested by RGSC_JS_REQUEST_UI_STATE() with:
	\verbatim
	{
		"Visible" : true,
		"Online" : true,
		"State" : "Activation",
		"SubState" : "LicenseCorrupted",
		"Data" : {
			"Message" : [StringId],
		}
	}
	\endverbatim
*/
void RGSC_CORRUPT_LICENSE_DIALOG_RESPONSE()
{
	GetRgscConcreteInstance()->_GetUiInterface()->_GetActivationSystem()->CorruptLicenseResponse();
}

//! Javascript calls this when the user has entered an activation code.
/**
	\param
		jsonActivationResponse the activation details (see below).
	\remark
	Example jsonActivationResponse:
	\verbatim
	{
		"ContentId" : 1234567
		"ActivationCode" : "1234A1234B1234C1234D"
		"Action" : 1
	}
	\endverbatim
	Possible values for Action:\n
	ACTIVATION_ERROR = 0,\n
	ACTIVATION_ACTIVATED_CONTENT = 1,\n
	ACTIVATION_DEACTIVATED_CONTENT = 2,\n
	ACTIVATION_USER_CANCELLED = 3,\n
	ACTIVATION_USER_QUIT = 4,\n\n

	The activation code UI can be requested by RGSC_JS_REQUEST_UI_STATE() with:
	\verbatim
	{
		"Visible" : true,
		"Online" : true,
		"State" : "Activation",
		"SubState" : "Main",
		"Data" : {
			"ContentId" : 1234567,
			"CurrentSerialNumber" : "1234A1234B1234C1234D",
			"IsAlreadyActivated" : false
		}
	}
	\endverbatim
	\see RGSC_JS_REQUEST_UI_STATE()

	\dot
	digraph GetActivationCode {
		label = "Getting an activation code";
		graph [truecolor bgcolor="#00000000"];
		node [shape=record, fontname=Arial, fontsize=10];

		rankdir=LR;

		subgraph cluster0 {
			label = "Javascript";
			RGSC_JS_REQUEST_UI_STATE [URL="\ref RGSC_JS_REQUEST_UI_STATE()"];
			UserEntersCode [shape=ellipse, label="User enters a code"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "DLL";
			color=lightgrey;
			ShowActivationCodeUI [label="ShowActivationCodeUI\n(const int contentId,\nconst char* currentSerialNumber,\nconst bool isAlreadyActivated,\nActivationCodeCallback callback)"]
			RGSC_UI_STATE_RESPONSE [URL="\ref RGSC_UI_STATE_RESPONSE()"];
			RGSC_ACTIVATION_CODE_RESPONSE [color=red, URL="\ref RGSC_ACTIVATION_CODE_RESPONSE()"];
		}

		subgraph cluster2 {
			label = "Game";
			Start [shape=ellipse, label="Game wants an\nactivation code"];
			GameCallback [label="void Callback(const int contentId,\nconst char* activationCode,\nconst ActivationAction action)"];
			color=lightgrey;
		}

		Start -> ShowActivationCodeUI -> RGSC_JS_REQUEST_UI_STATE -> RGSC_UI_STATE_RESPONSE -> UserEntersCode -> RGSC_ACTIVATION_CODE_RESPONSE -> GameCallback;
	}
	\enddot

	\dot
	digraph ActivationCodeResult {
		label = "Activation code result";
		graph [truecolor bgcolor="#00000000"];
		node [shape=record, fontname=Arial, fontsize=10];

		rankdir=LR;

		subgraph cluster0 {
			label = "Javascript";
			RGSC_JS_ACTIVATION_RESULT [URL="\ref RGSC_JS_ACTIVATION_RESULT()"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "DLL";
			color=lightgrey;
			ActivationCodeResult [label="void ActivationCodeResult(const int ContentID, const int result)"]
		}

		subgraph cluster2 {
			label = "Game";
			Start [shape=ellipse, label="Game handles\nactivation code"];
			color=lightgrey;
		}

		Start -> ActivationCodeResult -> RGSC_JS_ACTIVATION_RESULT;
	}
	\enddot
*/
void RGSC_ACTIVATION_CODE_RESPONSE(json jsonActivationResponse)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonActivationResponse); 
	GetRgscConcreteInstance()->_GetUiInterface()->_GetActivationSystem()->ActivationCodeResponse(utf8.data());
	stringUtil_free(utf8);
}

//! Javascript calls this when the user has clicked a button on the Steam Account Linking UI.
/**
	\param
		jsonResponse the action the user took (see below).
	\remark
	Example jsonResponse:
	\verbatim
	{
		"Action" : 5
	}
	\endverbatim
	Possible values for Action:\n
	ACTIVATION_USER_CANCELLED = 3,\n
	ACTIVATION_USER_QUIT = 4,\n
	ACTIVATION_LINK_STEAM_ACCOUNT = 5,\n\n

	The Steam account linking UI can be requested by RGSC_JS_REQUEST_UI_STATE() with:
	\verbatim
	{
		"Visible" : true,
		"Online" : true,
		"State" : "Activation",
		"SubState" : "SteamLink",
		"Data" : {
			"Message" : [StringId]
		}
	}
	\endverbatim
	\see RGSC_JS_REQUEST_UI_STATE()
*/
void RGSC_ACTIVATION_STEAM_LINK_RESPONSE(json jsonResponse)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonResponse); 
	GetRgscConcreteInstance()->_GetUiInterface()->_GetActivationSystem()->SteamLinkingResponse(utf8.data());
	stringUtil_free(utf8);
}

//! Javascript calls this when the user has performed an account linking operation.
/**
	\param
		jsonResponse the result of the account link (see below).
	\remark
	Example jsonResponse:
	\verbatim
	{
		"Result" : 1
	}
	\endverbatim
	Possible values for Result:\n
	ACCOUNT_LINK_SUCCESS = 0,\n
	ACCOUNT_LINK_FAIL = 1,\n
	ACCOUNT_LINK_USER_CANCELLED = 2,\n
	ACCOUNT_LINK_USER_QUIT = 3,\n

	For example, the Facebook account linking UI can be requested by RGSC_JS_REQUEST_UI_STATE() with:
	\verbatim
	{
		"Visible" : true,
		"Online" : true,
		"State" : "AccountLinking",
		"SubState" : "Facebook",
	}
	\endverbatim
	Possible values for SubState:\n
	Facebook\n
	Google\n
	TwitchTV\n

	\see RGSC_JS_REQUEST_UI_STATE()
*/
void RGSC_ACCOUNT_LINK_RESULT(json jsonResponse)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonResponse); 
	GetRgscConcreteInstance()->_GetUiInterface()->AcountLinkResult(utf8.data());
	stringUtil_free(utf8);
}

//! Javascript calls this when the user has entered a license recovery code.
/**
	\param
		jsonResponse the license recovery details (see below).
	\remark
	Example jsonResponse:
	\verbatim
	{
		"ContentId" : 1234567
		"RequestCode" : "1234A1234B1234C1234D"
		"ResponseCode" : "2389189D18A11B91C93E"
		"Action" : 1
	}
	\endverbatim
	RequestCode is the same code passed to RGSC_JS_REQUEST_UI_STATE() when the UI was requested.

	Possible values for Action:\n
	ACTIVATION_ERROR = 0,\n
	ACTIVATION_ACTIVATED_CONTENT = 1,\n
	ACTIVATION_DEACTIVATED_CONTENT = 2,\n
	ACTIVATION_USER_CANCELLED = 3,\n
	ACTIVATION_USER_QUIT = 4,\n\n

	The license recovery UI can be requested by RGSC_JS_REQUEST_UI_STATE() with:
	\verbatim
	{
		"Visible" : true,
		"Online" : true,
		"State" : "Activation",
		"SubState" : "LicenseRecovery",
		"Data" : {
			"ContentId" : 1234567,
			"RequestCode" : "1234A1234B1234C1234D",
		}
	}
	\endverbatim
	\see RGSC_JS_REQUEST_UI_STATE()
*/
void RGSC_LICENSE_RECOVERY_RESPONSE(json jsonResponse)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonResponse); 
	GetRgscConcreteInstance()->_GetUiInterface()->_GetActivationSystem()->LicenseRecoveryResponse(utf8.data());
	stringUtil_free(utf8);
}

//! Javascript calls this when the user has entered a license extension code.
/**
	\param
		jsonResponse the license extension details (see below).
	\remark
	Example jsonResponse:
	\verbatim
	{
		"ContentId" : 1234567
		"ActivationCode" : "1234A1234B1234C1234D"
		"Action" : 1
	}
	\endverbatim
	Possible values for Action:\n
	ACTIVATION_ERROR = 0,\n
	ACTIVATION_ACTIVATED_CONTENT = 1,\n
	ACTIVATION_DEACTIVATED_CONTENT = 2,\n
	ACTIVATION_USER_CANCELLED = 3,\n
	ACTIVATION_USER_QUIT = 4,\n\n

	The license recovery UI can be requested by RGSC_JS_REQUEST_UI_STATE() with:
	\verbatim
	{
		"Visible" : true,
		"Online" : true,
		"State" : "Activation",
		"SubState" : "LicenseExpiration",
		"Data" : {
			"ContentId" : 1234567,
			"CurrentSerialNumber" : "1234A1234B1234C1234D",
		}
	}
	\endverbatim
	\see RGSC_JS_REQUEST_UI_STATE()
*/
void RGSC_LICENSE_EXPIRATION_RESPONSE(json jsonResponse)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonResponse); 
	GetRgscConcreteInstance()->_GetUiInterface()->_GetActivationSystem()->ExpirationCodeResponse(utf8.data());
	stringUtil_free(utf8);
}

//! Javascript calls this when the user has clicked yes or no on a patching dialog box.
/**
	\param
		jsonResponse the action the user chose in the dialog box (see below).
	\remark
	Example jsonResponse:
	\verbatim
	{
		"Response" : 1
	}
	\endverbatim
	Response:\n
	DB_OK = 1,		- The OK button was selected.\n
	DB_CANCEL = 2,	- The Cancel button was selected.\n
	DB_YES = 6,		- The Yes button was selected.\n
	DB_NO = 7,		- The No button was selected.

	\see RGSC_PATCHING_ANIMATION_STATE()
	\see RGSC_JS_PATCHING_SET_OPTIONS()
	\see RGSC_JS_PATCHING_SET_PROGRESS()
	\see RGSC_PATCHING_OPTIONS_RESPONSE()
	\see RGSC_PATCHING_PROGRESS_RESPONSE()
	\see RGSC_JS_PATCHING_FINISHED()

	The patching dialog box UI can be requested by RGSC_JS_REQUEST_UI_STATE() with:
	\verbatim
	{
		"Visible" : true,
		"Online" : true,
		"State" : "Patching",
		"SubState" : "Dialog",
		"Data" : {
			"Message" : [StringId]
			"DialogType" : 1
			"Modal" : true
		}
	}
	\endverbatim
	Or if a dynamic message is used:
	\verbatim
	{
		"Visible" : true,
		"Online" : true,
		"State" : "Patching",
		"SubState" : "Dialog",
		"Data" : {
			"DynamicMessage" : "This is dynamic text."
			"DialogType" : 1
			"Modal" : true
		}
	}
	\endverbatim
	Note that either a StringId will be sent via Message, or a dynamic utf-8 encoded string will be sent via DynamicMessage.
	The DialogType parameter indicates which type of dialog to display.
	Modal indicates whether or not javascript should allow the user to close the UI by prsesing escape or home.
	The response passed to RGSC_PATCHING_DIALOG_RESPONSE() should indicate which button was pressed.
	Legal values for DialogType:
	DT_OK = 1,
	DT_OKCANCEL = 2,
	DT_YESNO = 3,
	DT_YESNOCANCEL = 4,

	\see RGSC_JS_REQUEST_UI_STATE()

	\dot
	digraph ShowDialog {
		label = "Patching System: Show Dialog";
		graph [truecolor bgcolor="#00000000"];
		node [shape=record, fontname=Arial, fontsize=10];

		rankdir=LR;

		subgraph cluster0 {
			label = "Javascript";
			RGSC_JS_REQUEST_UI_STATE [URL="\ref RGSC_JS_REQUEST_UI_STATE()"];
			UserClicksButton [shape=ellipse, label="User clicks yes or no"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "DLL";
			color=lightgrey;
			RGSC_UI_STATE_RESPONSE [URL="\ref RGSC_UI_STATE_RESPONSE()"];
			ShowDialog [label="ShowDialog\n(StringId message,\nShowDialogCallback callback)"]
			RGSC_PATCHING_DIALOG_RESPONSE [color=red, URL="\ref RGSC_PATCHING_DIALOG_RESPONSE()"];
		}

		subgraph cluster2 {
			label = "Game";
			Start [shape=ellipse, label="Game wants to\nshow a dialog box"];
			GameCallback [label="void Callback(const bool response)"];
			color=lightgrey;
		}

		Start -> ShowDialog -> RGSC_JS_REQUEST_UI_STATE -> RGSC_UI_STATE_RESPONSE -> UserClicksButton -> RGSC_PATCHING_DIALOG_RESPONSE -> GameCallback;
	}
	\enddot
*/
void RGSC_PATCHING_DIALOG_RESPONSE(json jsonResponse)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonResponse); 
	GetRgscConcreteInstance()->_GetUiInterface()->_GetPatchingSystem()->DialogResponse(utf8.data());
	stringUtil_free(utf8);
}

//! Javascript calls this when the user has chosen which patches to install or hide.
/**
	\param
		jsonResponse the list of patches to install or hide (see below).
	\remark
	Example jsonResponse:
	\verbatim
	{
		"NumOptions" : 3,
		"Options" : [
			{
				"Index" : 2,
				"Install" : true,
				"Hide" : false
			},
			{
				"Index" : 1,
				"Install" : false,
				"Hide" : true
			},
			{
				"Index" : 3,
				"Install" : true,
				"Hide" : false
			}
		]
	}
	\endverbatim
	\see RGSC_PATCHING_ANIMATION_STATE()
	\see RGSC_JS_PATCHING_SET_OPTIONS()
	\see RGSC_JS_PATCHING_SET_PROGRESS()
	\see RGSC_PATCHING_DIALOG_RESPONSE()
	\see RGSC_PATCHING_PROGRESS_RESPONSE()
	\see RGSC_JS_PATCHING_FINISHED()

	\dot
	digraph ShowOptions {
		label = "Patching System: Show Options";
		graph [truecolor bgcolor="#00000000"];
		node [shape=record, fontname=Arial, fontsize=10];

		rankdir=LR;

		subgraph cluster0 {
			label = "Javascript";
			RGSC_JS_REQUEST_UI_STATE [URL="\ref RGSC_JS_REQUEST_UI_STATE()"];
			UserClicksButton [shape=ellipse, label="User clicks yes or no"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "DLL";
			color=lightgrey;
			RGSC_UI_STATE_RESPONSE [URL="\ref RGSC_UI_STATE_RESPONSE()"];
			ShowOptions [label="ShowOptions\n(StringId message,\nShowOptionsCallback callback)"]
			RGSC_PATCHING_OPTIONS_RESPONSE [color=red, URL="\ref RGSC_PATCHING_OPTIONS_RESPONSE()"];
		}

		subgraph cluster2 {
			label = "Game";
			Start [shape=ellipse, label="Game wants to show\nlist of update options"];
			GameCallback [label="void Callback(const unsigned char* install,\nconst unsigned char* hide,\nconst unsigned int numOptions)"];
			color=lightgrey;
		}

		Start -> ShowOptions -> RGSC_JS_REQUEST_UI_STATE -> RGSC_UI_STATE_RESPONSE -> UserClicksButton -> RGSC_PATCHING_OPTIONS_RESPONSE -> GameCallback;
	}
	\enddot

	The patching UI can be requested by RGSC_JS_REQUEST_UI_STATE() with:
	\verbatim
	{
		"Visible" : true,
		"Online" : true,
		"State" : "Patching",
		"SubState" : "Message",
		"Data" : {
			"Message" : [StringId],
			"Modal" : true,
			"AnimateSpinner" : true
		}
	}
	\endverbatim
	Or if a dynamic message is used:
	\verbatim
	{
		"Visible" : true,
		"Online" : true,
		"State" : "Patching",
		"SubState" : "Message",
		"Data" : {
			"DynamicMessage" : "This is dynamic text.",
			"Modal" : true,
			"AnimateSpinner" : true
		}
	}
	\endverbatim
	Note that either a StringId will be sent via Message, or a dynamic utf-8 encoded string will be sent via DynamicMessage.
	Modal indicates whether or not javascript should allow the user to close the UI by prsesing escape or home.

	\remark
		This is requested when the patching system needs to display an initial message while it checks for updates.\n
*/
void RGSC_PATCHING_OPTIONS_RESPONSE(json jsonResponse)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonResponse);
	GetRgscConcreteInstance()->_GetUiInterface()->_GetPatchingSystem()->OptionsResponse(utf8.data());
	stringUtil_free(utf8);
}

//! Javascript calls this in response to RGSC_JS_PATCHING_SET_PROGRESS.
/**
	\param
		jsonResponse the response (see below).
	\remark
	Example jsonResponse:
	\verbatim
	{
		"State" : 1
	}
	\endverbatim
	Response:\n
	The State value will be passed back to the game unmodified.\n
	\see RGSC_PATCHING_ANIMATION_STATE()
	\see RGSC_JS_PATCHING_SET_OPTIONS()
	\see RGSC_JS_PATCHING_SET_PROGRESS()
	\see RGSC_PATCHING_DIALOG_RESPONSE()
	\see RGSC_PATCHING_OPTIONS_RESPONSE()
	\see RGSC_JS_PATCHING_FINISHED()
*/
void RGSC_PATCHING_PROGRESS_RESPONSE(json jsonResponse)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonResponse); 
	GetRgscConcreteInstance()->_GetUiInterface()->_GetPatchingSystem()->ProgressResponse(utf8.data());
	stringUtil_free(utf8);
}

//! Javascript calls this to notify the game that a patching UI animation/transition has completed.
/**
	\param
		jsonResponse the response (see below).
	\remark
	Example jsonResponse:
	\verbatim
	{
		"State" : 1
	}
	\endverbatim
	Response:\n
	The State value will be passed back to the game unmodified.\n
	\see RGSC_JS_PATCHING_SET_OPTIONS()
	\see RGSC_JS_PATCHING_SET_PROGRESS()
	\see RGSC_PATCHING_DIALOG_RESPONSE()
	\see RGSC_PATCHING_OPTIONS_RESPONSE()
	\see RGSC_JS_PATCHING_FINISHED()
*/
void RGSC_PATCHING_ANIMATION_STATE(json jsonResponse)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonResponse); 
	GetRgscConcreteInstance()->_GetUiInterface()->_GetPatchingSystem()->AnimationState(utf8.data());
	stringUtil_free(utf8);
}

//! Retrieves stats data from the game.
/**
	\return
		Stats data as a string, formatted by the game.
*/
RgscUtf8String RGSC_GET_STATS_DATA()
{
	return RgscUtf8String::point_to(GetRgscConcreteInstance()->GetStatsData());
}

//! Javascript calls this to notify the game that a text box has gained or lost focus.
/**
	\param
		jsonHasFocus the response (see below).
	\remark
	Example jsonHasFocus:
	\verbatim
	{
		"HasFocus" : true,
		"Prompt" : "Text to describe what text goes in this textbox.",
		"Text" : "Text that was in the textbox before gaining focus.",
		"IsPassword" : false,
		"MaxNumChars" : 32,
		"ForwardToGame" : false
	}
	\endverbatim
*/
void RGSC_SET_TEXT_BOX_HAS_FOCUS(json jsonHasFocus)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonHasFocus); 
	GetRgscConcreteInstance()->_GetUiInterface()->SetTextBoxHasFocus(utf8.data());
	stringUtil_free(utf8);
}

//! Javascript calls this to notify the DLL that we're going online from an offline state.
/**
	\param
		jsonNetworkChange the response (see below).
	\remark
	Example jsonNetworkChange:
	\verbatim
	{
		"Online" : true
	}
	\endverbatim
*/
void RGSC_NETWORK_CHANGE(json jsonNetworkChange)
{
	// TODO: NS - implement
// 	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonNetworkChange); 
// 	GetRgscConcreteInstance()->_GetUiInterface()->NetworkChange(utf8.data());
// 	stringUtil_free(utf8);
}

//! Requests a specified type of player list (example: friend list) in json format.
/**
	\remark
		- This function starts an asynchronous operation. The requested player list
		will be returned via a callback to RGSC_JS_PLAYER_LIST_RESPONSE().
		- This function can request the player's friend list, invites sent,
		invites received, or the player's block list.
		- Certain list combinations are supported (i.e. friend + invites sent)

	Example: Friend List Request:

	\verbatim
	{
		"RequestId" : 5,
		"PlayerListType" : 1,
		"PageIndex" : 0,
		"PageSize" : 10,
	}
	\endverbatim
	Parameters:
	- RequestId - if a RequestId (integer) is passed, the same RequestId will be returned in the response.
				This is to allow the UI to identify which response belongs to which request.
	- PlayerListType - the type of list being requested. Possible values for PlayerListType:\n
	LIST_TYPE_FRIENDS = 1,\n
	LIST_TYPE_INVITES_SENT = 2,\n
	LIST_TYPE_FRIENDS_AND_INVITES_SENT = 3,\n
	LIST_TYPE_INVITES_RECEIVED = 4,\n
	LIST_TYPE_BLOCKED_PLAYERS = 8,\n
	- PageIndex - Player lists are requested in pages, starting at index 0.
	- PageSize - indicates the maximum number of players to return per page.

	\see RGSC_JS_PLAYER_LIST_RESPONSE()
*/
void RGSC_REQUEST_PLAYER_LIST(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	GetRgscConcreteInstance()->_GetPlayerManager()->ReadPlayerListAsJson(utf8.data());
	stringUtil_free(utf8);
}

//! Requests the number of players on each type of player list.
/**
	\remark
		- This function starts an asynchronous operation. The requested player list counts
		will be returned via a callback to RGSC_JS_PLAYER_LIST_COUNTS_RESPONSE().

	Example Request:

	\verbatim
	{
		"RequestId" : 6
	}
	\endverbatim
	Parameters:
	- RequestId - if a RequestId (integer) is passed, the same RequestId will be returned in the response.
				This is to allow the UI to identify which response belongs to which request.

	\see RGSC_JS_PLAYER_LIST_COUNTS_RESPONSE()
*/
void RGSC_REQUEST_PLAYER_LIST_COUNTS(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	GetRgscConcreteInstance()->_GetPlayerManager()->ReadPlayerListCountsAsJson(utf8.data());
	stringUtil_free(utf8);
}

//! Retrieves the list of active game invites for the signed in player in json format.
/**
	\return
		the list of active game invites for the signed in player in json format.
	\remark

	Example: Game Invites List:

	\verbatim
	{
		"Count" : 2,
		"GameInvites" : [
			{
				"GameInviteId" : 1,
				"RockstarId" : "272617",
				"Nickname" : "Pestilence",
				"AvatarUrl" : "GTAIV/GTAIV3.png",
				"ReceivedTime" : "1143413414",
				"Salutation" : "Come play my game I'll test ya"
			},
			{
				"GameInviteId" : 2,
				"RockstarId" : "28451",
				"Nickname" : "DungeonDefender",
				"AvatarUrl" : "MP3/MP3_3.png",
				"ReceivedTime" : "1053599412",
				"Salutation" : ""
			},
			...
		]
	}
	\endverbatim
	Parameters:
	- GameInviteId - the id of the game invite. This id must be passed to perform operatations
	  on the invite (eg. RGSC_GAME_INVITE_ACCEPTED(), RGSC_GAME_INVITE_DECLINED()).
    - RockstarId (etc.) - the info of the player who sent the invite.
	- ReceivedTime - posix time at which the game invite was received.
	- Salutation - an optional parameter that may have been sent along with the game invite.
*/
RgscUtf8String RGSC_GET_GAME_INVITE_LIST()
{
	return RgscUtf8String::point_to(GetRgscConcreteInstance()->_GetPresenceManager()->GetGameInviteListAsJson());
}

//! Accepts a game invite.
/**
	\return
		true if the game invite was still valid at the time it was accepted, false otherwise.
	Example:
	\verbatim
	{
		"GameInviteId" : 5
	}
	\endverbatim
	Parameters:
	- GameInviteId - the id of the game invite as specified via RGSC_GET_GAME_INVITE_LIST()
	\remark
		If this function returns true, close the UI via RGSC_REQUEST_UI_STATE().
		If it returns false, don't close the UI, and display a message saying 
		something like "The selected game invitation is no longer valid."

	\see RGSC_GET_GAME_INVITE_LIST()
	\see RGSC_GAME_INVITE_DECLINED()
	\see RGSC_REQUEST_UI_STATE()
*/
bool RGSC_GAME_INVITE_ACCEPTED(json jsonInvite)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonInvite); 
	bool success = GetRgscConcreteInstance()->_GetPresenceManager()->GameInviteAccepted(utf8.data());
	stringUtil_free(utf8);
	return success;
}

//! Declines a game invite.
/**
	Example:
	\verbatim
	{
		"GameInviteId" : 5
	}
	\endverbatim
	Parameters:
	- GameInviteId - the id of the game invite as specified via RGSC_GET_GAME_INVITE_LIST()

	\see RGSC_GET_GAME_INVITE_LIST()
	\see RGSC_GAME_INVITE_ACCEPTED()
*/
void RGSC_GAME_INVITE_DECLINED(json jsonInvite)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonInvite); 
	GetRgscConcreteInstance()->_GetPresenceManager()->GameInviteDeclined(utf8.data());
	stringUtil_free(utf8);
}

//! Sends a game invite.
/**
	\return
		true if the game invite was sent, false otherwise.

	Example:
	\verbatim
	{
		"RequestId" : 5,
		"RockstarId" : "321381839",
		"Salutation" : "Come play my game I'll test ya"
	}
	\endverbatim
	Parameters:
	- RequestId - if a RequestId (integer) is passed, the same RequestId will be returned in the response.
				  This is to allow the UI to identify which response belongs to which request.
	- RockstarId - the id of the player to whom to send a game invitation.
	- Salutation - an optional text message to send along with the invite

	\see RGSC_GAME_INVITE_SEND_RESULT
*/
bool RGSC_GAME_INVITE_SEND(json jsonInvite)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonInvite); 
	RGSC_HRESULT hr = GetRgscConcreteInstance()->_GetPresenceManager()->GameInviteSend(utf8.data());
	stringUtil_free(utf8);
	return SUCCEEDED(hr);
}

//! Joins a game via the presence system without requiring an invite.
/**
	Example:
	\verbatim
	{
		"RequestId" : 5,
		"RockstarId" : "321381839"
	}
	\endverbatim
	Parameters:
	- RequestId - if a RequestId (integer) is passed, the same RequestId will be returned in the response.
				  This is to allow the UI to identify which response belongs to which request.
	- RockstarId - the id of the player to join.

	\see RGSC_JOIN_VIA_PRESENCE_RESPONSE
*/
void RGSC_JOIN_VIA_PRESENCE(json jsonJoinViaPresence)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonJoinViaPresence); 
	GetRgscConcreteInstance()->_GetPresenceManager()->JoinViaPresence(utf8.data());
	stringUtil_free(utf8);
}

//! Sends a friend invite to another player.
/**
	\remark
		This function starts an asynchronous operation. The result will
		be returned via a callback to RGSC_JS_FRIEND_SEND_INVITE_RESULT().

	Example:
	\verbatim
	{
		"RequestId" : 5,
		"RockstarId" : "28741"
	}
	\endverbatim
	Parameters:
	- RequestId - if a RequestId (integer) is passed, the same RequestId will be returned in the response.
				This is to allow the UI to identify which response belongs to which request.
	- RockstarId - the rockstar id of the player being invited.

	\see RGSC_JS_FRIEND_SEND_INVITE_RESULT()
*/
void RGSC_FRIEND_SEND_INVITE(json jsonFriendOperation)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonFriendOperation); 
	GetRgscConcreteInstance()->_GetPlayerManager()->SendInvite(utf8.data());
	stringUtil_free(utf8);
}

//! Cancels a pending invite that was sent by the local player.
/**
	\remark
		This function starts an asynchronous operation. The result will
		be returned via a callback to RGSC_JS_FRIEND_CANCEL_INVITE_RESULT().

	Example:
	\verbatim
	{
		"RequestId" : 5,
		"RockstarId" : "28741"
	}
	\endverbatim
	Parameters:
	- RequestId - if a RequestId (integer) is passed, the same RequestId will be returned in the response.
				This is to allow the UI to identify which response belongs to which request.
	- RockstarId - the rockstar id of the player who was previously invited.

	\see RGSC_JS_FRIEND_CANCEL_INVITE_RESULT()
*/
void RGSC_FRIEND_CANCEL_INVITE(json jsonFriendOperation)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonFriendOperation); 
	GetRgscConcreteInstance()->_GetPlayerManager()->CancelInvite(utf8.data());
	stringUtil_free(utf8);
}

//! Declines a pending invite that was received by the local player.
/**
	\remark
		This function starts an asynchronous operation. The result will
		be returned via a callback to RGSC_JS_FRIEND_DECLINE_INVITE_RESULT().

	Example:
	\verbatim
	{
		"RequestId" : 5,
		"RockstarId" : "28741"
	}
	\endverbatim
	Parameters:
	- RequestId - if a RequestId (integer) is passed, the same RequestId will be returned in the response.
				This is to allow the UI to identify which response belongs to which request.
	- RockstarId - the rockstar id of the player who sent the invitation.

	\see RGSC_JS_FRIEND_DECLINE_INVITE_RESULT()
*/
void RGSC_FRIEND_DECLINE_INVITE(json jsonFriendOperation)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonFriendOperation); 
	GetRgscConcreteInstance()->_GetPlayerManager()->DeclineInvite(utf8.data());
	stringUtil_free(utf8);
}

//! Accepts a pending invite that was received by the local player.
/**
	\remark
		This function starts an asynchronous operation. The result will
		be returned via a callback to RGSC_JS_FRIEND_ACCEPT_INVITE_RESULT().

	Example:
	\verbatim
	{
		"RequestId" : 5,
		"RockstarId" : "28741"
	}
	\endverbatim
	Parameters:
	- RequestId - if a RequestId (integer) is passed, the same RequestId will be returned in the response.
				This is to allow the UI to identify which response belongs to which request.
	- RockstarId - the rockstar id of the player who sent the invitation.

	\see RGSC_JS_FRIEND_ACCEPT_INVITE_RESULT()
*/
void RGSC_FRIEND_ACCEPT_INVITE(json jsonFriendOperation)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonFriendOperation); 
	GetRgscConcreteInstance()->_GetPlayerManager()->AcceptInvite(utf8.data());
	stringUtil_free(utf8);
}

//! Removes a friend from the local player's friend list.
/**
	\remark
		This function starts an asynchronous operation. The result will
		be returned via a callback to RGSC_JS_FRIEND_DELETE_FRIEND_RESULT().

	Example:
	\verbatim
	{
		"RequestId" : 5,
		"RockstarId" : "28741"
	}
	\endverbatim
	Parameters:
	- RequestId - if a RequestId (integer) is passed, the same RequestId will be returned in the response.
				This is to allow the UI to identify which response belongs to which request.
	- RockstarId - the rockstar id of the player to remove.

	\see RGSC_JS_FRIEND_DELETE_FRIEND_RESULT()
*/
void RGSC_FRIEND_DELETE_FRIEND(json jsonFriendOperation)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonFriendOperation); 
	GetRgscConcreteInstance()->_GetPlayerManager()->DeleteFriend(utf8.data());
	stringUtil_free(utf8);
}

//! Adds a player to the local player's block list.
/**
	\remark
		This function starts an asynchronous operation. The result will
		be returned via a callback to RGSC_JS_FRIEND_BLOCK_PLAYER_RESULT().

	Example:
	\verbatim
	{
		"RequestId" : 5,
		"RockstarId" : "28741"
	}
	\endverbatim
	Parameters:
	- RequestId - if a RequestId (integer) is passed, the same RequestId will be returned in the response.
				This is to allow the UI to identify which response belongs to which request.
	- RockstarId - the rockstar id of the player to block.

	\see RGSC_JS_FRIEND_BLOCK_PLAYER_RESULT()
*/
void RGSC_FRIEND_BLOCK_PLAYER(json jsonFriendOperation)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonFriendOperation); 
	GetRgscConcreteInstance()->_GetPlayerManager()->BlockPlayer(utf8.data());
	stringUtil_free(utf8);
}

//! Removes a player from the local player's block list.
/**
	\remark
		This function starts an asynchronous operation. The result will
		be returned via a callback to RGSC_JS_FRIEND_UNBLOCK_PLAYER_RESULT().

	Example:
	\verbatim
	{
		"RequestId" : 5,
		"RockstarId" : "28741"
	}
	\endverbatim
	Parameters:
	- RequestId - if a RequestId (integer) is passed, the same RequestId will be returned in the response.
				This is to allow the UI to identify which response belongs to which request.
	- RockstarId - the rockstar id of the player to unblock.

	\see RGSC_JS_FRIEND_UNBLOCK_PLAYER_RESULT()
*/
void RGSC_FRIEND_UNBLOCK_PLAYER(json jsonFriendOperation)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonFriendOperation); 
	GetRgscConcreteInstance()->_GetPlayerManager()->UnblockPlayer(utf8.data());
	stringUtil_free(utf8);
}

//! Determines whether a specified player is on the local player's friend list.
/**
	\return
		true if the specified player is on the local player's friend list, false otherwise.
	\remark
		This function returns the result immediately and is not asynchronous.

	Example:
	\verbatim
	{
		"RockstarId" : "28741"
	}
	\endverbatim
*/
bool RGSC_FRIEND_IS_FRIEND(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	bool isFriend = GetRgscConcreteInstance()->_GetPlayerManager()->IsFriend(utf8.data());
	stringUtil_free(utf8);
	return isFriend;
}

//! Determines whether a specified player is on the local player's block list.
/**
	\return
		true if the specified player is on the local player's block list, false otherwise.
	\remark
		This function returns the result immediately and is not asynchronous.

	Example:
	\verbatim
	{
		"RockstarId" : "28741"
	}
	\endverbatim
*/
bool RGSC_FRIEND_IS_BLOCKED(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	bool isBlocked = GetRgscConcreteInstance()->_GetPlayerManager()->IsBlocked(utf8.data());
	stringUtil_free(utf8);
	return isBlocked;
}

//! Notifies the Social Club DLL that a message was sent via the SCUI.
/**
	Example jsonNotification:
	\verbatim
	{
		"RockstarId" : "211403",
	}
	\endverbatim
	Parameters:
	- RockstarId - the RockstarId of the player to whom the message was sent.
*/
void RGSC_PLAYER_MESSAGE_SENT(json jsonNotification)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonNotification); 
	GetRgscConcreteInstance()->_GetPresenceManager()->MessageSent(utf8.data());
	stringUtil_free(utf8);
}

//! Requests info about a specified player.
/**
	\remark
		- This function starts an asynchronous operation. The requested info
		will be returned via a callback to RGSC_JS_PLAYER_INFO_RESPONSE().

	Example Request:

	\verbatim
	{
		"RequestId" : 5,
		"RockstarId" : "321381839"
	}
	\endverbatim
	Parameters:
	- RequestId - if a RequestId (integer) is passed, the same RequestId will be returned in the response.
				This is to allow the UI to identify which response belongs to which request.
	- RockstarId - the RockstarId of the player for whom to retrieve info.

	\see RGSC_JS_PLAYER_INFO_RESPONSE()
*/
void RGSC_REQUEST_PLAYER_INFO(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	GetRgscConcreteInstance()->_GetPlayerManager()->GetPlayerInfoAsJson(utf8.data());
	stringUtil_free(utf8);
}

//! Requests multiplayer presence info about a list of players.
/**
	\remark
		- This function assumes the specified RockstarIds are online and playing the same title.
		- This function starts an asynchronous operation. The requested info
		will be returned via a callback to RGSC_JS_PLAYER_MULTIPLAYER_PRESENCE_INFO_RESPONSE().

	Example Request:

	\verbatim
	{
		"RequestId" : 5,
		"Players" : [
			{
				"RockstarId" : "28942581238732",
			},
			{
				"RockstarId" : "138871347913491",
			},
			{
				"RockstarId" : "7734266369619707088",
			}
		]
	}
	\endverbatim
	Parameters:
	- RequestId - if a RequestId (integer) is passed, the same RequestId will be returned in the response.
				This is to allow the UI to identify which response belongs to which request.
	- RockstarId - the RockstarId of the player for whom to retrieve info.

	\see RGSC_JS_PLAYER_MULTIPLAYER_PRESENCE_INFO_RESPONSE()
*/
void RGSC_REQUEST_PLAYER_MULTIPLAYER_PRESENCE_INFO(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	GetRgscConcreteInstance()->_GetPlayerManager()->GetPlayerMultiplayerPresenceInfoAsJson(utf8.data());
	stringUtil_free(utf8);
}

//! Notifies the DLL when a notification becomes visible or invisible.
/**
	\remark
		This function is be used to start/stop rendering the window for notifications.

	Example:
	\verbatim
	{
		"Visible" : true
	}
	\endverbatim
	- Visible - true if a notification became visible, false if a notification became invisible.
*/
void RGSC_SET_NOTIFICATION_VISIBLE(json jsonVisible)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonVisible); 
	GetRgscConcreteInstance()->_GetUiInterface()->SetNotificationVisible(utf8.data());
	stringUtil_free(utf8);
}

//! Javascript calls this when the user has entered a redemption code.
/**
	\param
		jsonRedemptionCodeResponse the redemption code details (see below).
	\remark
	Example jsonRedemptionCodeResponse:
	\verbatim
	{
		"RedemptionCode" : "1234A1234B1234C1234D"
		"Action" : 1
	}
	\endverbatim
	Possible values for Action:\n

	REDEMPTION_ENTERED_CODE = 1,\n
	REDEMPTION_USER_CANCELLED = 2,\n

	The redemption code UI can be requested by RGSC_JS_REQUEST_UI_STATE() with:
	\verbatim
	{
		"Visible" : true,
		"Online" : true,
		"State" : "Commerce",
		"SubState" : "RedemptionCode"
	}
	\endverbatim
	\see RGSC_JS_REQUEST_UI_STATE()

	\dot
	digraph GetRedemptionCode {
		label = "Getting a redemption code";
		graph [truecolor bgcolor="#00000000"];
		node [shape=record, fontname=Arial, fontsize=10];

		rankdir=LR;

		subgraph cluster0 {
			label = "Javascript";
			RGSC_JS_REQUEST_UI_STATE [URL="\ref RGSC_JS_REQUEST_UI_STATE()"];
			UserEntersCode [shape=ellipse, label="User enters a code"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "DLL";
			color=lightgrey;
			ShowRedemptionCodeUI [label="ShowRedemptionCodeUI\n(RedemptionCodeCallback callback)"]
			RGSC_UI_STATE_RESPONSE [URL="\ref RGSC_UI_STATE_RESPONSE()"];
			RGSC_REDEMPTION_CODE_RESPONSE [color=red, URL="\ref RGSC_REDEMPTION_CODE_RESPONSE()"];
		}

		subgraph cluster2 {
			label = "Game";
			Start [shape=ellipse, label="Game wants a\nredemption code"];
			GameCallback [label="void Callback(const char* redemptionCode,\nconst RedemptionAction action)"];
			color=lightgrey;
		}

		Start -> ShowRedemptionCodeUI -> RGSC_JS_REQUEST_UI_STATE -> RGSC_UI_STATE_RESPONSE -> UserEntersCode -> RGSC_REDEMPTION_CODE_RESPONSE -> GameCallback;
	}
	\enddot

	\dot
	digraph RedemptionCodeResult {
		label = "Redemption code result";
		graph [truecolor bgcolor="#00000000"];
		node [shape=record, fontname=Arial, fontsize=10];

		rankdir=LR;

		subgraph cluster0 {
			label = "Javascript";
			RGSC_JS_REDEMPTION_RESULT [URL="\ref RGSC_JS_REDEMPTION_RESULT()"];
			UserClicksDownload [shape=ellipse, label="User clicks Download button"];
			color=lightgrey;
		}

		subgraph cluster1 {
			label = "DLL";
			color=lightgrey;
			RedemptionCodeResult [label="void RedemptionCodeResult(const int result)"]
			RGSC_REDEMPTION_USER_RESPONSE [color=red, URL="\ref RGSC_REDEMPTION_CODE_RESPONSE()"];
		}

		subgraph cluster2 {
			label = "Game";
			Start [shape=ellipse, label="Game handles\nredemption code"];
			GameCallback [label="void Callback(NULL,\nconst RedemptionAction action)"];
			color=lightgrey;
		}

		Start -> RedemptionCodeResult -> RGSC_JS_REDEMPTION_RESULT -> UserClicksDownload -> RGSC_REDEMPTION_USER_RESPONSE -> GameCallback;
	}
	\enddot
*/
void RGSC_REDEMPTION_CODE_RESPONSE(json jsonRedemptionCodeResponse)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRedemptionCodeResponse); 
	GetRgscConcreteInstance()->_GetCommerceManager()->RedemptionCodeResponse(utf8.data());
	stringUtil_free(utf8);
}

//! Javascript calls this when the user has chosen an action on the redemption code result screen.
/**
	\param
		jsonRedemptionUserResponse the action performed by the user (see below).
	\remark
	Example jsonRedemptionUserResponse:
	\verbatim
	{
		"Action" : 3
	}
	\endverbatim
	Possible values for Action:\n

	REDEMPTION_USER_CANCELLED = 2,\n
	REDEMPTION_DOWNLOAD_CONTENT = 3,\n\n

	\see RGSC_REDEMPTION_CODE_RESPONSE()
*/
void RGSC_REDEMPTION_USER_RESPONSE(json jsonRedemptionUserResponse)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRedemptionUserResponse); 
	GetRgscConcreteInstance()->_GetCommerceManager()->UserResponse(utf8.data());
	stringUtil_free(utf8);
}

//! Javascript calls this to notify the DLL or the game of an event (such as the user clicking minimize or close buttons, etc.)
/**
	\param
		jsonEvent the event to raise.
	\remark
	Example jsonEvent:
	\verbatim
	{
		"EventId" : 3,
		"Data" : {
			"ButtonId" : 4,
		}
	}
	EventId (int > 0) must exist.
	Data element is optional. This contains any data related to the event.
	Note that older games will ignore events they don't know about.
	\endverbatim
*/
void RGSC_RAISE_UI_EVENT(json jsonEvent)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonEvent); 
	GetRgscConcreteInstance()->_GetUiInterface()->RaiseUiEvent(utf8.data());
	stringUtil_free(utf8);
}

#if RSG_CPU_X64
//! Javascript call to initialise the commerce data. Should be performed after connectivity has been verified.
/**
	\see RGSC_DESTROY_COMMERCE_DATA()
*/
void RGSC_INITIALISE_COMMERCE_DATA()
{
	GetRgscConcreteInstance()->_GetCommerceManager()->InitData();
}

//! Javascript call to destroy the commerce data. Should be paired with RGSC_INITIALISE_COMMERCE_DATA.
/**
	\see RGSC_INITIALISE_COMMERCE_DATA()
*/
void RGSC_DESTROY_COMMERCE_DATA()
{
	GetRgscConcreteInstance()->_GetCommerceManager()->DestroyData();
}

//! Javascript call to request the commerce data. Should be called after initialisation of the commerce data.
/**
	\see RGSC_INITIALISE_COMMERCE_DATA()
*/
void RGSC_REQUEST_COMMERCE_DATA(json languageCode)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(languageCode); 
	GetRgscConcreteInstance()->_GetCommerceManager()->StartDataFetch(utf8.data());
	stringUtil_free(utf8);
}

//! Javascript call to query if the commerce util has fetched valid data.
/**
	\return
	{ "result" : true }
	or
	{ "result" : false }
*/

RgscUtf8String RGSC_IS_COMMERCE_DATA_VALID()
{
	return RgscUtf8String::point_to(GetRgscConcreteInstance()->_GetCommerceManager()->IsDataValidAsJson());
}

//! Javascript call to query if the commerce util is in an error state.
/**
	\return
	{ "result" : true }
	or
	{ "result" : false }
*/
RgscUtf8String RGSC_IS_COMMERCE_IN_ERROR_STATE()
{
	return RgscUtf8String::point_to(GetRgscConcreteInstance()->_GetCommerceManager()->IsInErrorStateAsJson());
}


//! Javascript call to get the commerce data once it has been fetched.
/**
	\return
	The commerce data constructed from the relevant sources.
	\remark
	This function should be called after RGSC_REQUEST_COMMERCE_DATA, and once RGSC_IS_COMMERCE_DATA_VALID has returned true.\n
	Example data:
	\verbatim
	{
	"Count" : 20,
	"CategoryCount" : 4,
	"ProductCount" : 16,
	"Categories" : [
		{
			"Id" : "TOP_DEFAULT",
			"Name" : ""
		},
		{
			"Id" : "Extras",
			"Name" : "PACKS"
		}
	],
	"Products" : [
		{
			"Id" : "collectors_ed",
			"Name" : "Collector's Edition Pack",
			"Desc" : "The Collector’s Edition Pack provides additional content for Grand Theft Auto V including Stunt Plane Trials, a special ability boost, additional weapons, garage property with unique vehicles, bonus outfits and tattoos, special deals from shopkeepers, access to the Atomic Blimp aircraft and custom characters for Grand Theft Auto Online.

Grand Theft Auto Online specific content will become available at the launch of Grand Theft Auto Online.",
			"PriceString" : "",
			"Price" : 0,
			"Purchased" : false,
			"Installed" : false,
			"Consumable" : false,
			"CategoryMemberships" : [
				{
					"MemberOf" : "Extras"
				}
			],
			"ImagePaths" : [
				{
					"ImagePath" : "store/images/editions/Editions_Collectors.dds"
				}
			]
		}
	]
}
	\endverbatim
*/
RgscUtf8String RGSC_GET_COMMERCE_CATALOG()
{
	return RgscUtf8String::point_to(GetRgscConcreteInstance()->_GetCommerceManager()->GetDataAsJson());
}

//! Javascript call to get data on a specific item.
/**
	\return
	The commerce data constructed for the requested item.\n
	\remark
	This function should be called after RGSC_REQUEST_COMMERCE_DATA, and once RGSC_IS_COMMERCE_DATA_VALID has returned true.\n
	Example jsonItemId:
	\verbatim
	{
		"Id" : "Cash"
	}
	\endverbatim
*/
RgscUtf8String RGSC_GET_COMMERCE_ITEM_DATA(json jsonItemId)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonItemId); 
	const char *itemData = GetRgscConcreteInstance()->_GetCommerceManager()->GetItemDataAsJson(utf8.data());
	stringUtil_free(utf8);
	return RgscUtf8String::point_to(itemData);
}


//! Javascript call to request the checkout URL for an item.
/**
	\return
	None
	\remark
	This function will request a callback containing the checkout URL for a specific product. The callback will be RGSC_JS_CHECKOUT_URL_REQUEST_RESULT\n
	Country and language will default to us/en if not provided
	Example jsonParam:
	\verbatim
	{
		"productId" : "TestOffer1"
		"country"	: "us"
		"language"	: "en"
	}
	\endverbatim
	Example return data:
	\verbatim
	{ 
		"result" : true 
	}
	\endverbatim
*/

RgscUtf8String RGSC_REQUEST_CHECKOUT_URL(json jsonParam)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonParam); 
	const char* retString = GetRgscConcreteInstance()->_GetCommerceManager()->RequestUrlForCheckout(utf8.data());
	stringUtil_free(utf8);
	return RgscUtf8String::point_to(retString);
}

//! Javascript call to get data on a users age appropriate status for store access
// Sample output
// { 
//		"Valid" : true,
//		"AgeAppropriate" : true
// }

RgscUtf8String RGSC_GET_USER_STORE_AGE_APPROPRIATE_INFO()
{
	const char *ageResponse = GetRgscConcreteInstance()->_GetCommerceManager()->GetUserAgeAppropriateData();
	return RgscUtf8String::point_to(ageResponse);
}

bool RGSC_OPEN_STEAM_STORE_PANEL()
{
	return GetRgscConcreteInstance()->_GetCommerceManager()->ShowSteamStore();
}

//! Javascript call to check if a product is installed.
/**
	\return
	None
	\remark
	This function will check if all of the packages marked as being part of a product have been installed.
	Example jsonParam:
	\verbatim
	{
	"productId": "test_product_1"
	}
	\endverbatim
	Example return data:
	\verbatim
	{ 
		"result" : true 
	}

	or

	{ 
		"result" : false 
	}
	\endverbatim
*/

RgscUtf8String RGSC_IS_COMMERCE_PRODUCT_INSTALLED(json jsonProductId)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonProductId); 
	const char *installedResponse = GetRgscConcreteInstance()->_GetCommerceManager()->IsProductInstalled(utf8.data());
	stringUtil_free(utf8);
	return RgscUtf8String::point_to(installedResponse);
}

//! Javascript call to request the purchase of a steam product.
/**
	\return
	None
	\remark
	This function will begin the process of a steam product purchase. This is analogous with RGSC_REQUEST_CHECKOUT_URL for non steam versions
	Example jsonParam:
	\verbatim
	{
	"productId": "TEST-GTAV-SHARK-MULTI"
	"language" : "en"
	}
	\endverbatim
	Example return data:
	\verbatim
	{ 
		"result" : true 
	}
	\endverbatim
*/

RgscUtf8String RGSC_START_STEAM_PURCHASE(json jsonProductId)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonProductId); 
	const char *steamPurchaseResponse = GetRgscConcreteInstance()->_GetCommerceManager()->StartSteamPurchase(utf8.data());
	stringUtil_free(utf8);
	return RgscUtf8String::point_to(steamPurchaseResponse);
}

bool RGSC_DID_LAST_STEAM_PURCHASE_FAIL()
{
#if !PRELOADED_SOCIAL_CLUB
	return GetRgscConcreteInstance()->_GetCommerceManager()->DidLastSteamPurchaseFail();
#else
	return false;
#endif
}

//! Javascript call to request the contents of a voucher, along with its token for consumption.
/**
	\return
	Json
	\remark
	This function will request a callback containing the contents of the passed voucher code\n
	language will default to en if not provided\n
	This function will cause a callback to RGSC_JS_VOUCHER_CONTENT_RESULT
	Example jsonParam:
	\verbatim
	{
	"voucherCode": "TEST-GTAV-SHARK-MULTI"
	"language" : "en"
	}
	\endverbatim
	Example return data:
	\verbatim
	{ 
		"result" : true 
	}
	\endverbatim
*/

RgscUtf8String RGSC_REQUEST_VOUCHER_CONTENTS(json jsonParam)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonParam); 
	const char *voucherContentResponse = GetRgscConcreteInstance()->_GetCommerceManager()->RequestVoucherContent(utf8.data());
	stringUtil_free(utf8);
	return RgscUtf8String::point_to(voucherContentResponse);
}

//! Javascript call to request the consumption of a voucher.
/**
	\return
	Json
	\remark
	This function will request a callback containing the status of the vouchers consumption\n
	This function will cause a callback to RGSC_JS_VOUCHER_CONSUME_RESULT
	Example jsonParam:
	\verbatim
	{
	"voucherCode": "TEST-GTAV-SHARK-MULTI"
	}
	\endverbatim
	Example return data:
	\verbatim
	{ 
		"result" : true 
	}
	\endverbatim
*/

RgscUtf8String RGSC_REQUEST_VOUCHER_CONSUMPTION(json jsonProductId)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonProductId); 
	const char *voucherConsumptionResponse = GetRgscConcreteInstance()->_GetCommerceManager()->RequestVoucherConsumption(utf8.data());
	stringUtil_free(utf8);
	return RgscUtf8String::point_to(voucherConsumptionResponse);
}

//! Retrieves purchase flow telemetry from the game.
/**
	\remark
		This function should be called whenever the Store UI is opened.
		This is used to obtain Purchase Flow Telemetry from the game so we can gather analytics
		data about when and why people choose to purchase items in the Store.
		Note that if there is no telemetry available, the result will be an empty string.
*/
RgscUtf8String RGSC_COMMERCE_GET_PURCHASE_FLOW_TELEMETRY()
{
#if !PRELOADED_SOCIAL_CLUB
	return RgscUtf8String::point_to(GetRgscConcreteInstance()->_GetCommerceManager()->GetPurchaseFlowTelemetry());
#else
	return RgscUtf8String::point_to("");
#endif
}

#endif


//! Notifies the game that it needs to refresh the steam auth ticket.
/**
	\remark
		This function requires game code to send a result, and thus MP3/LAN will not refresh.\n
		Only GTA5 and newer titles can update the steam auth ticket. RGSC_JS_REFRESH_STEAM_TICKET_RESULT will\n
		be called when the steam ticket has finished refreshing
*/
void RGSC_REFRESH_STEAM_TICKET()
{
	bool NotifyScui = true;
	GetRgscConcreteInstance()->HandleNotification(rgsc::NOTIFY_REFRESH_STEAM_AUTH_TICKET, &NotifyScui);
}

//! Requests the facebook link info for the logged in user.
/**
	\remark
		- This function starts an asynchronous operation. The facebook link info and app info
		will be returned via a callback to RGSC_JS_FACEBOOK_LINK_INFO_RESULT().

	Example Request:

	\verbatim
	{
		"RequestId" : 6
		"RockstarId" : "321381839"
	}
	\endverbatim
	Parameters:
	- RequestId - if a RequestId (integer) is passed, the same RequestId will be returned in the response.
				This is to allow the UI to identify which response belongs to which request.

	\see RGSC_JS_FACEBOOK_LINK_INFO_RESULT()
*/
void RGSC_GET_FACEBOOK_LINK_INFO(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	GetRgscConcreteInstance()->_GetPlayerManager()->GetFacebookLinkInfoAsJson(utf8.data());
	stringUtil_free(utf8);
}

//! Requests the SCUI to be reloaded
/**
	\remark
		- This function unloads the website and restarts it in either online or offline mode. You can only
			request a change of state (i.e. if the online site is loaded, you cannot call request a reload of the
			online site or you'll see an assert and the request will fail).

	Example Request:

	\verbatim
	{
		"OfflineSite" : false
	}
	\endverbatim
	Parameters:
	- OfflineSite - if OfflineSite is true, we'll reload using the Offline SCUI. Otherwise, the online site will be loaded.
*/
void RGSC_RELOAD_UI(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	GetRgscConcreteInstance()->_GetUiInterface()->RequestReloadUi(utf8.data());
	stringUtil_free(utf8);
}

//! Sent by the SCUI as a response to RGSC_JS_PING
/**
	\remark
		- Before communication can occur between the SDK and the SCUI, we must verify that the SCUI is ready and willing to
		  accept commands. After receiving a page load event from Chrome, we transition to a state where we PING the SCUI
		  until receiving a PONG. This state times out after 15 seconds.
	\see RGSC_JS_PING
*/
void RGSC_PONG()
{
	GetRgscConcreteInstance()->_GetUiInterface()->OnPongReceived();
}

//! Sent by the SCUI to request the internet connected state stored in the RgscConcreteInstance
bool RGSC_INTERNET_CONNECTED_STATE()
{
	return GetRgscConcreteInstance()->IsConnectedToInternet();
}

//! Notifies the game that an entitlement event has occured
/**
	\remark
		This function requires game code to send a result, and thus older SKUs well ignore it.\n
	\verbatim
	{
		"ProductId": "TEST-GTAV-SHARK-MULTI",
		"Action" : "Download"
	}
	\endverbatim
*/
void RGSC_ENTITLEMENT_EVENT(json jsonRequest)
{	
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	GetRgscConcreteInstance()->HandleNotification(rgsc::NOTIFY_ENTITLEMENT_EVENT, utf8.data());
	stringUtil_free(utf8);
}

//! Sent by the SCUI if the page is loading in error (i.e. CSS couldn't load), allowing us to early out of our page load state machines
/**
	\remark
		This function allows a JSON blob of the reason for the failure for diagnostic purposes
	\verbatim
	{
		"Message": "CSS failed to load.",
	}
	\endverbatim
*/
void RGSC_PAGE_LOAD_ERROR(json jsonReason)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonReason); 
	GetRgscConcreteInstance()->_GetUiInterface()->OnPageLoadError(utf8.data());
	stringUtil_free(utf8);
}

//! Returns a Steam Persona object to the SCUI.
/**
	\remark
		This function returns a JSON blob of the current user's Steam persona.
	\verbatim
	{
		"Name" : "rsjmoore"
	}
	\endverbatim
*/
RgscUtf8String RGSC_GET_STEAM_PERSONA()
{
	return RgscUtf8String::point_to(GetRgscConcreteInstance()->GetSteamPersonaAsJson());
}

//! Javascript calls this to raise debugging events (such as debug output).
/**
	\param
		jsonEvent the debug event.
	\remark
	Example jsonEvent:
	\verbatim
	{
		"MyString" : "Ajax call to http...",
	}
	\endverbatim
*/
#if __DEV
void RGSC_DEBUG_EVENT(json jsonEvent)
{
	// note: don't convert to UTF8 since there can be a lot of data and we can run out of memory.
	GetRgscConcreteInstance()->_GetUiInterface()->RaiseDebugEvent(jsonEvent.data());
}
#endif

//! Javascript calls this to raise debugging events (such as debug output).
/**
	\param
		jsonEvent the debug event.
	\remark
	Example jsonEvent:
	\verbatim
	{
		"MyString" : "Ajax call to http...",
	}
	\endverbatim
*/
void RGSC_RETAIL_OUTPUT(json jsonEvent)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonEvent); 
	GetRgscConcreteInstance()->_GetUiInterface()->RaiseRetailOutputEvent(utf8.data());
	stringUtil_free(utf8);
}

//! Javascript calls this to notify the DLL of an IME event (such as the user paging through the candidate list).
/**
	\param
		jsonEvent the event to raise.
	\remark
	Example jsonEvent:
	\verbatim
	{
		"EventId" : 3,
		"Data" : {
			"ButtonId" : 4,
		}
	}
	EventId (int > 0) must exist.
	Data element is optional. This contains any data related to the event.
	\endverbatim
*/
void RGSC_RAISE_IME_EVENT(json jsonEvent)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonEvent); 
	GetRgscConcreteInstance()->_GetUiInterface()->RaiseImeEvent(utf8.data());
	stringUtil_free(utf8);
}

//! Returns a the cloud saves enabled state for a title
/**
	\remark
		This function returns a JSON blob of the cloud saved enabled state.
	\verbatim
	Input:
	{
		"RosTitleName" : "gta5",
	}
	Result:
	{
		"RosTitleName" : "gta5",
		"Enabled" : "unknown/enabled/disabled"
	}
	\endverbatim
*/
RgscUtf8String RGSC_GET_CLOUD_SAVE_ENABLED(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	const char* result = GetRgscConcreteInstance()->_GetCloudSaveManager()->GetCloudSaveEnabledAsJson(utf8.data());
	stringUtil_free(utf8);
	return RgscUtf8String::point_to(result);
}

//! Sets the the cloud saves enabled state for a title
/**
	\remark
		This function sets the cloud saves enabled state for a title.
	\verbatim
	{
		"RosTitleName" : "gta5",
		"Enabled" : true
	}
	\endverbatim
*/
void RGSC_SET_CLOUD_SAVE_ENABLED(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	GetRgscConcreteInstance()->_GetCloudSaveManager()->SetCloudSaveEnabledFromJson(utf8.data());
	stringUtil_free(utf8);
}

//! Returns a list of command line arguments known by the SC SDK
/**
	\remark
		Escape characters will be escaped once for JSON and again for transmission.
		Example, for: -rootdir=x:\gta5\build\dev_ng/
	\verbatim
	{
		"Arguments" : [
			"-rootdir=x:\\\\gta5\\\\build\\\\dev_ng/"
		]
	}
*/
RgscUtf8String RGSC_GET_COMMAND_LINE_ARGUMENTS()
{
	const char* result = GetRgscConcreteInstance()->GetCommandLineArgumentsAsJson();
	return RgscUtf8String::point_to(result);
}

//! Javascript calls this to notify that the mouse has entered a specific region. 
/**
	\remark
		Javascript should specify an identifier of which region is entered. The only supported value currently is "caption"
	\verbatim
	{
		"identifier" : "caption",
	}
	\endverbatim
*/
void RGSC_MOUSE_ENTER(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest); 
	GetRgscConcreteInstance()->_GetUiInterface()->OnMouseEnterExitEvent(utf8.data(), RgscUi::MOUSE_ENTERED);
}

//! Javascript calls this to notify that the mouse has exited a specific region. 
/**
	\remark
		Javascript should specify an identifier of which region is exited. The only supported value currently is "caption"
	\verbatim
	{
		"identifier" : "caption",
	}
	\endverbatim
*/
void RGSC_MOUSE_EXIT(json jsonRequest)
{
	RgscUtf8String utf8 = rgsc::WideToUTF8(jsonRequest);
	GetRgscConcreteInstance()->_GetUiInterface()->OnMouseEnterExitEvent(utf8.data(), RgscUi::MOUSE_EXITED);
}

//! Javascript calls this in response to RGSC_JS_SHUTDOWN, to indicate that shutdown/cleanup processing has completed.
void RGSC_SHUTDOWN_RESPONSE()
{
	GetRgscConcreteInstance()->_GetUiInterface()->OnShutdownComplete();
}

//! Javascript calls this to request network information.
/**
	\remark
	Note: will return '{}' on games prior to GTA V.
	May also return '{}' if an error has occurred.
	Example of a Moderate NAT:
	\verbatim
	{
		"PublicAddress" : "216.221.152.66:11111",
		"PrivateAddress" : "10.2.48.103:11111",
		"NATDetectionState" : 2,
		"NATType" : 2,
		"uPnP" : 0,
		"ShowNatTypeWarning" : false
	}

	Example of a Strict NAT with uPnP enabled:

	{
		"PublicAddress" : "216.221.152.71:61440",
		"PrivateAddress" : "192.168.0.123:11111",
		"NATDetectionState" : 2,
		"NATType" : 3,
		"uPnP" : 2,
		"ShowNatTypeWarning" : true
	}

	Public and Private Address may or may not include the port, may be IPv4 or IPv6\n\n

	NAT Detection State values:\n
	0 = UNATTEMPTED\n
	1 = IN_PROGRESS\n
	2 = SUCCEEDED\n
	3 = FAILED\n\n

	NAT Type values:\n
	0 = UNKNOWN\n
	1 = OPEN\n
	2 = MODERATE\n
	3 = STRICT\n\n

	uPnP values:\n
	0 = UNATTEMPTED\n
	1 = IN_PROGRESS\n
	2 = SUCCEEDED\n
	3 = FAILED\n
	\endverbatim
*/
#if RSG_CPU_X64
RgscUtf8String RGSC_GET_NETWORK_INFO()
{
	return RgscUtf8String::point_to(GetRgscConcreteInstance()->_GetNetwork()->GetNetworkInfoAsJson());
}
#endif

void Script::SetupCommands()
{
	SCR_REGISTER(RGSC_GET_VERSION_INFO);
	SCR_REGISTER(RGSC_GET_TITLE_ID);
	SCR_REGISTER(RGSC_GET_ROS_CREDENTIALS);
 	SCR_REGISTER(RGSC_SIGN_IN);
 	SCR_REGISTER(RGSC_SIGN_OUT);
 	SCR_REGISTER(RSGC_LAUNCH_EXTERNAL_WEB_BROWSER);
 	SCR_REGISTER(RGSC_GET_PROFILE_LIST);
	SCR_REGISTER(RGSC_GET_PROFILE_LIST_FILTERED);
	SCR_REGISTER(RGSC_GET_AVATAR);
 	SCR_REGISTER(RGSC_UI_STATE_RESPONSE);
 	SCR_REGISTER(RGSC_REQUEST_UI_STATE);
	SCR_REGISTER(RGSC_READY_TO_ACCEPT_COMMANDS);
 	SCR_REGISTER(RGSC_MODIFY_PROFILE);
	SCR_REGISTER(RGSC_DELETE_PROFILE);
	SCR_REGISTER(RGSC_GET_ACHIEVEMENT_LIST);
 	SCR_REGISTER(RGSC_GET_ACHIEVEMENT_IMAGE);
	SCR_REGISTER(RGSC_ACTIVATION_CODE_RESPONSE);
	SCR_REGISTER(RGSC_ACTIVATION_STEAM_LINK_RESPONSE);
	SCR_REGISTER(RGSC_CORRUPT_LICENSE_DIALOG_RESPONSE);
	SCR_REGISTER(RGSC_LICENSE_RECOVERY_RESPONSE);
	SCR_REGISTER(RGSC_LICENSE_EXPIRATION_RESPONSE);
	SCR_REGISTER(RGSC_PATCHING_DIALOG_RESPONSE);
	SCR_REGISTER(RGSC_PATCHING_OPTIONS_RESPONSE);
	SCR_REGISTER(RGSC_PATCHING_PROGRESS_RESPONSE);
	SCR_REGISTER(RGSC_PATCHING_ANIMATION_STATE);
	SCR_REGISTER(RGSC_GET_STATS_DATA);
	SCR_REGISTER(RGSC_SET_TEXT_BOX_HAS_FOCUS);
	SCR_REGISTER(RGSC_NETWORK_CHANGE);
	SCR_REGISTER(RGSC_REQUEST_PLAYER_LIST);
	SCR_REGISTER(RGSC_REQUEST_PLAYER_LIST_COUNTS);
	SCR_REGISTER(RGSC_FRIEND_SEND_INVITE);
	SCR_REGISTER(RGSC_FRIEND_CANCEL_INVITE);
	SCR_REGISTER(RGSC_FRIEND_DECLINE_INVITE);
	SCR_REGISTER(RGSC_FRIEND_ACCEPT_INVITE);
	SCR_REGISTER(RGSC_FRIEND_DELETE_FRIEND);
	SCR_REGISTER(RGSC_FRIEND_BLOCK_PLAYER);
	SCR_REGISTER(RGSC_FRIEND_UNBLOCK_PLAYER);
	SCR_REGISTER(RGSC_FRIEND_IS_FRIEND);
	SCR_REGISTER(RGSC_FRIEND_IS_BLOCKED);
	SCR_REGISTER(RGSC_PLAYER_MESSAGE_SENT);
	SCR_REGISTER(RGSC_REQUEST_PLAYER_INFO);
	SCR_REGISTER(RGSC_REQUEST_PLAYER_MULTIPLAYER_PRESENCE_INFO);
	SCR_REGISTER(RGSC_GET_GAME_INVITE_LIST);
	SCR_REGISTER(RGSC_GAME_INVITE_ACCEPTED);
	SCR_REGISTER(RGSC_GAME_INVITE_DECLINED);
	SCR_REGISTER(RGSC_GAME_INVITE_SEND);
	SCR_REGISTER(RGSC_JOIN_VIA_PRESENCE);
	SCR_REGISTER(RGSC_SET_NOTIFICATION_VISIBLE);
	SCR_REGISTER(RGSC_REDEMPTION_CODE_RESPONSE);
	SCR_REGISTER(RGSC_REDEMPTION_USER_RESPONSE);
	SCR_REGISTER(RGSC_RAISE_UI_EVENT);
#if RSG_CPU_X64
	SCR_REGISTER(RGSC_INITIALISE_COMMERCE_DATA);
	SCR_REGISTER(RGSC_DESTROY_COMMERCE_DATA);
	SCR_REGISTER(RGSC_REQUEST_COMMERCE_DATA);
	SCR_REGISTER(RGSC_IS_COMMERCE_DATA_VALID);
	SCR_REGISTER(RGSC_IS_COMMERCE_IN_ERROR_STATE);
	SCR_REGISTER(RGSC_GET_COMMERCE_CATALOG);
	SCR_REGISTER(RGSC_GET_COMMERCE_ITEM_DATA);
	SCR_REGISTER(RGSC_REQUEST_CHECKOUT_URL);
	SCR_REGISTER(RGSC_GET_USER_STORE_AGE_APPROPRIATE_INFO);
	SCR_REGISTER(RGSC_OPEN_STEAM_STORE_PANEL);
	SCR_REGISTER(RGSC_IS_COMMERCE_PRODUCT_INSTALLED);
	SCR_REGISTER(RGSC_START_STEAM_PURCHASE);
	SCR_REGISTER(RGSC_REQUEST_VOUCHER_CONTENTS);
	SCR_REGISTER(RGSC_REQUEST_VOUCHER_CONSUMPTION);
	SCR_REGISTER(RGSC_COMMERCE_GET_PURCHASE_FLOW_TELEMETRY);
    SCR_REGISTER(RGSC_DID_LAST_STEAM_PURCHASE_FAIL);

	// The SCUI won't show the network info UI if this isn't registered.
	// Don't register this on titles older than GTA V since they
	// don't provide the info.
	SCR_REGISTER(RGSC_GET_NETWORK_INFO);
#endif
	SCR_REGISTER(RGSC_REFRESH_STEAM_TICKET);
	SCR_REGISTER(RGSC_GET_FACEBOOK_LINK_INFO);
	SCR_REGISTER(RGSC_RELOAD_UI);
	SCR_REGISTER(RGSC_PONG);
	SCR_REGISTER(RGSC_INTERNET_CONNECTED_STATE);
	SCR_REGISTER(RGSC_ENTITLEMENT_EVENT);
	SCR_REGISTER(RGSC_PAGE_LOAD_ERROR);
	SCR_REGISTER(RGSC_GET_STEAM_PERSONA);

#if __DEV
	SCR_REGISTER(RGSC_DEBUG_EVENT);
#endif

	SCR_REGISTER(RGSC_RETAIL_OUTPUT);
	SCR_REGISTER(RGSC_RAISE_IME_EVENT);
	SCR_REGISTER(RGSC_GET_CLOUD_SAVE_ENABLED);
	SCR_REGISTER(RGSC_SET_CLOUD_SAVE_ENABLED);
	SCR_REGISTER(RGSC_GET_COMMAND_LINE_ARGUMENTS);
	SCR_REGISTER(RGSC_MOUSE_ENTER);
	SCR_REGISTER(RGSC_MOUSE_EXIT);
	SCR_REGISTER(RGSC_SHUTDOWN_RESPONSE);
}

/** @} */ // end of JS2C_API group

} // namespace rgsc
