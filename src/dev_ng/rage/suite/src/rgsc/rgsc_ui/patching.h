// 
// Patching.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RGSC_PATCHING_H 
#define RGSC_PATCHING_H

#include "patching_interface.h"

namespace rgsc
{

class Patching : public IPatchingLatestVersion
{
	// ===============================================================================================
	// inherited from public interfaces
	// ===============================================================================================
public:
	// ===============================================================================================
	// inherited from IRgscUnknown
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

	// ===============================================================================================
	// inherited from IPatchingV1
	// ===============================================================================================
	bool RGSC_CALL CanPerformTitleUpdate();
	RGSC_HRESULT RGSC_CALL InitPatchingUi(AnimationCallback callback, void* callbackData);
	RGSC_HRESULT RGSC_CALL ShowMessage(StringId messageId, const Utf8String utf8String, const bool modal, const bool animateSpinner);
	RGSC_HRESULT RGSC_CALL SetProgress(StringId messageId, const Utf8String utf8String, const unsigned int progress, SetProgressCallback callback, void* callbackData);
	RGSC_HRESULT RGSC_CALL ShowOptions(const Utf8String message, const Utf8String* options, const bool* optional, const bool* hidden, const unsigned int numOptions, ShowOptionsCallback callback, void* callbackData);
	RGSC_HRESULT RGSC_CALL ShowDialog(StringId messageId, const Utf8String utf8String, DialogType type, const bool modal, ShowDialogCallback callback, void* callbackData);
	RGSC_HRESULT RGSC_CALL ClosePatchingUi(bool showFinalMessage);

public:
	Patching();
	virtual ~Patching();

	bool Init();
	void Shutdown();
	void Update();

	void AnimationState(const char* jsonResponse);
	void DialogResponse(const char* jsonResponse);
	void OptionsResponse(const char* jsonResponse);
	void ProgressResponse(const char* jsonResponse);

	RGSC_HRESULT InitPatchingUiInternal(AnimationCallback callback, void* callbackData);

private:

	void ClearCallbacks();

	AnimationCallback m_AnimationCallback;
	void* m_AnimationCallbackData;

	ShowOptionsCallback m_ShowOptionsCallback;
	void* m_ShowOptionsCallbackData;

	ShowDialogCallback m_ShowDialogCallback;
	void* m_ShowDialogCallbackData;

	SetProgressCallback m_SetProgressCallback;
	void* m_SetProgressCallbackData;
};

} // namespace rgsc

#endif // RGSC_PATCHING_H 
