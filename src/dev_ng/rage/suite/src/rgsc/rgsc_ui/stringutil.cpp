#include "stringutil.h"
#include "weakstring.h"

#pragma warning(push)
#pragma warning(disable: 4310)
#include "base/utf_string_conversions.h"
#pragma warning(pop)

#include <cstring>

namespace rgsc {

namespace {
template <class CharType, class StrType>
inline WeakString<CharType> newWeakString(const StrType &str) {
	CharType *outChar = new CharType[str.length() + 1];
	std::memcpy(outChar, str.data(), sizeof(CharType) * str.length());
	outChar[str.length()] = 0;
	return WeakString<CharType>::point_to(outChar, str.length(), true);
}
}

WideString UTF8ToWide(const UTF8String &in) {
	std::wstring out;
	::UTF8ToWide(in.data(), in.length(), &out);
	return newWeakString<wchar_t, std::wstring>(out);
}
UTF8String WideToUTF8(const WideString &in) {
	std::string out;
	::WideToUTF8(in.data(), in.length(), &out);
	return newWeakString<char, std::string>(out);
}

void stringUtil_free(WideString returnedValue) {
	delete []returnedValue.data();
}

void stringUtil_free(UTF8String returnedValue) {
	delete []returnedValue.data();
}

}
