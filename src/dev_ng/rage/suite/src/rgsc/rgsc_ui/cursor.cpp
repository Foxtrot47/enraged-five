#include "cursor.h"
#include "cursor_default.h"

// rgsc includes
#include "graphics.h"
#include "graphics_d3d9.h"
#include "graphics_d3d11.h"

// rage includes
#include "atl/string.h"
#include "diag/seh.h"
#include "rline/rldiag.h"
#include "system/memops.h"
#include "system/nelem.h"
#include "system/new.h"

using namespace rage;

namespace rgsc
{

// Global instance
RgscCursor g_VirtualCursor;

////////////////////////////////////////////////////////////////////
// RgscCursorResource
////////////////////////////////////////////////////////////////////
RgscCursorResource::RgscCursorResource(int width, int height)
	: m_Width(width)
	, m_Height(height)
{
}

////////////////////////////////////////////////////////////////////
// RgscCursorResourceD3D9
////////////////////////////////////////////////////////////////////
RgscCursorResourceD3D9::RgscCursorResourceD3D9(IDirect3DDevice9* device, int width, int height, int* /*pixelData*/)
	: RgscCursorResource(width, height)
	, m_Texture(NULL)
{
	// We're only using virtual pointers on DirectX 11 skus at the moment.
	// This needs to be implemented for DirectX9 devices.
}

RgscCursorResourceD3D9::~RgscCursorResourceD3D9()
{
	ReleaseResources();
}

bool RgscCursorResourceD3D9::IsValid()
{
	return m_Texture != NULL;
}

void RgscCursorResourceD3D9::ReleaseResources()
{
	if (m_Texture)
	{
		m_Texture->Release();
		m_Texture = NULL;
	}
}

////////////////////////////////////////////////////////////////////
// RgscCursorResourceD3D11
////////////////////////////////////////////////////////////////////
RgscCursorResourceD3D11::RgscCursorResourceD3D11(ID3D11Device* device, int width, int height, int* pixelData)
	: RgscCursorResource(width, height)
	, m_Texture(NULL)
	, m_ShaderResourceView(NULL)
{
	rtry
	{
		rverify(device, catchall, );
		rverify(pixelData, catchall, );

		// Setup for the pointer texture
		D3D11_TEXTURE2D_DESC oDesc;
		oDesc.Width = m_Width;
		oDesc.Height = m_Height;
		oDesc.MipLevels = 1;
		oDesc.ArraySize = 1;
		oDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		oDesc.SampleDesc.Count = 1;
		oDesc.SampleDesc.Quality = 0;
		oDesc.Usage = D3D11_USAGE_DYNAMIC;
		oDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		oDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		oDesc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA initData;
		initData.pSysMem = pixelData;
		initData.SysMemPitch = 4 * m_Width;
		initData.SysMemSlicePitch = 4 * m_Width * m_Height;

		// Create the texture using this data and validate that it was successful
		HRESULT hr = device->CreateTexture2D(&oDesc, &initData, &m_Texture);
		rverify(SUCCEEDED(hr), catchall, Errorf("RgscCursor - CreateTexture2D failed: %x", hr));

		// Setup for the shader resource view
		D3D11_SHADER_RESOURCE_VIEW_DESC oViewDesc;
		oViewDesc.Format = oDesc.Format;
		oViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		oViewDesc.Texture2D.MipLevels = oDesc.MipLevels;
		oViewDesc.Texture2D.MostDetailedMip = 0;
		oViewDesc.Texture2DArray.FirstArraySlice = 0;
		oViewDesc.Texture2DArray.ArraySize = oDesc.ArraySize;

		// Create the shader resource view
		hr = device->CreateShaderResourceView((ID3D11Resource*)m_Texture, &oViewDesc, &m_ShaderResourceView);
		rverify(SUCCEEDED(hr), catchall, Errorf("RgscCursor - CreateShaderResourceView failed: %x", hr));
	}
	rcatchall
	{
		ReleaseResources();
	}
}

RgscCursorResourceD3D11::~RgscCursorResourceD3D11()
{
	ReleaseResources();
}

bool RgscCursorResourceD3D11::IsValid()
{
	return m_Texture != NULL;
}

void RgscCursorResourceD3D11::ReleaseResources()
{
	if (m_Texture)
	{
		m_Texture->Release();
		m_Texture = NULL;
	}

	if (m_ShaderResourceView)
	{
		m_ShaderResourceView->Release();
		m_ShaderResourceView = NULL;
	}
}

////////////////////////////////////////////////////////////////////
// RgscCursor::ResourcesD3D9
////////////////////////////////////////////////////////////////////
RgscCursor::ResourcesD3D9::ResourcesD3D9()
	: m_VertexBuffer(NULL)
	, m_Device(NULL)
{
}

void RgscCursor::ResourcesD3D9::ReleaseResources()
{
	if (m_VertexBuffer)
	{
		m_VertexBuffer->Release();
		m_VertexBuffer = NULL;
	}
}


void RgscCursor::ResourcesD3D9::Shutdown()
{
	ReleaseResources();
	m_Device = NULL;
}

bool RgscCursor::ResourcesD3D9::Setup(IDirect3DDevice9* /*device*/)
{
	// We're only using virtual cursors on DirectX 11 skus at the moment.
	// This needs to be implemented for DirectX9 devices.
	return false;
}

////////////////////////////////////////////////////////////////////
// RgscCursor::ResourcesD3D11
////////////////////////////////////////////////////////////////////
RgscCursor::ResourcesD3D11::ResourcesD3D11()
	: m_VertexBuffer(NULL)
	, m_Device(NULL)
{
}

void RgscCursor::ResourcesD3D11::ReleaseResources()
{
	// Cleanup the D3D11 Vertex Buffer
	if(m_VertexBuffer)
	{
		m_VertexBuffer->Release();
		m_VertexBuffer = NULL;
	}
}

void RgscCursor::ResourcesD3D11::Shutdown()
{
	ReleaseResources();
	m_Device = NULL;
}

bool RgscCursor::ResourcesD3D11::Setup(ID3D11Device* device)
{
	bool success = false;
	SimpleVertex11 *allVertices = NULL;

	rtry
	{
		HRESULT hr;

		// Copy ptr to the device
		rverify(device, catchall, );
		m_Device = device;

		// Allocate 4 vertices for the quad
		allVertices = rage_new SimpleVertex11[4];
		rverify(allVertices, catchall, );

		// Setup the vertex buffer
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = sizeof(SimpleVertex11) * 4;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		// Initialize the vertex buffer with the simple vertex data
		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = allVertices;

		// Create and verify the vertex buffer
		hr = m_Device->CreateBuffer(&bd, &InitData, &m_VertexBuffer);
		rverify(SUCCEEDED(hr), catchall, Errorf("RgscCursor - CreateBuffer failed: %x", hr));

		success = true;
	}
	rcatchall
	{
		ReleaseResources();
		m_Device = NULL;
	}

	// Cleanup the vertices if they were allocated.
	if (allVertices)
	{
		delete[] allVertices;
		allVertices = NULL;
	}

	return success;
}


////////////////////////////////////////////////////////////////////
// RgscCursor
////////////////////////////////////////////////////////////////////
RgscCursor::RgscCursor()
{
	Clear();
}

RgscCursor::~RgscCursor()
{
	Shutdown();
}

void RgscCursor::Clear()
{
	// Reset common properties
	m_DeviceType = DEVICE_INVALID;
	m_CursorType = RgscCursorType::RGSC_CURSOR_TYPE_INVALID;
	m_bEnabled = false;
	m_bInWindow = false;
	m_bDirty = true;
	m_X = 0.0f;
	m_Y = 0.0f;
	m_ActiveCursor = NULL;
}

void RgscCursor::Shutdown()
{
	Clear();

	// Release any resources associated with the device
	ReleaseResources();

	// Shutdown devices
	m_D3D9.Shutdown();
	m_D3D11.Shutdown();

	// clear resource maps
	m_CursorMap.Kill();
	m_Resources.Reset();
}

void RgscCursor::OnLost()
{
	ReleaseResources();
}

void RgscCursor::OnReset()
{
	if (m_DeviceType == DEVICE_D3D9 && rlVerify(m_D3D9.m_Device))
	{
		m_D3D9.Setup(m_D3D9.m_Device);
	}
	else if (m_DeviceType == DEVICE_D3D11 && rlVerify(m_D3D11.m_Device))
	{
		m_D3D11.Setup(m_D3D11.m_Device);
	}
}

void RgscCursor::InitD3D9(IDirect3DDevice9* device)
{
	// Don't override an existing setup
	if (m_DeviceType != DEVICE_INVALID)
		return;

	rtry
	{
		// setup the resources for the device
		rverify(m_D3D9.Setup(device), catchall, );
		m_DeviceType = DEVICE_D3D9;
	}
	rcatchall
	{
		m_D3D9.ReleaseResources();
		m_DeviceType = DEVICE_INVALID;
	}
}

void RgscCursor::UpdateD3D9(float /*windowWidth*/, float /*windowHeight*/)
{
	// We're only using virtual pointers on DirectX 11 skus at the moment.
	// This needs to be implemented for DirectX9 devices.
}

void RgscCursor::RenderD3D9()
{
	// We're only using virtual pointers on DirectX 11 skus at the moment.
	// This needs to be implemented for DirectX9 devices.
}

void RgscCursor::InitD3D11(ID3D11Device* device)
{
	// Don't override an existing setup
	if (m_DeviceType != DEVICE_INVALID)
		return;

	rtry
	{
		// setup the resources for the device
		rverify(m_D3D11.Setup(device), catchall, );
		m_DeviceType = DEVICE_D3D11;

		// create cursor resources
		RgscCursorType pointers[] = {RGSC_CURSOR_POINTER};
		rverify(CreateNewCursor(pointers, COUNTOF(pointers), rgsc_cursor::Pointer::s_Size, rgsc_cursor::Pointer::s_Size, rgsc_cursor::Pointer::s_Texture), catchall, );

		RgscCursorType activePointers[] = {RGSC_CURSOR_HAND, RGSC_CURSOR_IBEAM};
		rverify(CreateNewCursor(activePointers, COUNTOF(activePointers), rgsc_cursor::ActivePointer::s_Size, rgsc_cursor::ActivePointer::s_Size, rgsc_cursor::ActivePointer::s_Texture), catchall, );

		// Set Default cursor
		SetCursorType(RGSC_CURSOR_POINTER);
	}
	rcatchall
	{
		m_D3D11.ReleaseResources();
		m_DeviceType = DEVICE_INVALID;
	}
}

void RgscCursor::UpdateD3D11(ID3D11DeviceContext* context, Rgsc_Graphics::RenderRect& rect)
{
	rtry
	{
		// Must be initialized and dirty in order to update
		rcheck(IsInitialized(), catchall, );
		rcheck(m_bDirty, catchall, );
		
		// Verify if we're in the window and clear the dirty flag
		UpdateInWindow(rect);
		m_bDirty = false;

		// Only need to update the vertex buffer 
		rcheck(m_ActiveCursor, catchall, );

		// Calculate the width and height of the pointer from 0.0 - 1.0f
		float ptrWidth = Rgsc_Graphics::MapNumFromRangeToRange((float)m_ActiveCursor->GetWidth(), 0.0f, (float)rect.GetWidth(), 0.0f, 1.0f);
		float ptrHeight = Rgsc_Graphics::MapNumFromRangeToRange((float)m_ActiveCursor->GetHeight(), 0.0f, (float)rect.GetHeight(), 0.0f, 1.0f);

		// Calculate the x/y position of the pointer
		float xPos = Rgsc_Graphics::MapNumFromRangeToRange(m_X, (float)rect.m_Left, (float)rect.m_Right, -1.0f, 1.0f);
		float yPos = -Rgsc_Graphics::MapNumFromRangeToRange(m_Y, (float)rect.m_Top, (float)rect.m_Bottom, -1.0f, 1.0f);

		// From the x,y position and width/height, calculate left/right/top/bottom
		float left = xPos - ptrWidth;
		float right = xPos + ptrWidth;
		float top = yPos - ptrHeight;
		float bottom = yPos + ptrHeight;

		// Setup the vertices based on left/right/top/bottom
		SimpleVertex11 vertices[] =
		{
			{ XMFLOAT3(left, top, 0.0f), XMFLOAT2(0.0f, 0.0f) },		// top left
			{ XMFLOAT3(left, bottom, 0.0f), XMFLOAT2(0.0f, 1.0f) },		// bottom left
			{ XMFLOAT3(right, top, 0.0f), XMFLOAT2(1.0f, 0.0f) },		// top right
			{ XMFLOAT3(right, bottom, 0.0f), XMFLOAT2(1.0f, 1.0f) },	// bottom right
		};

		// Get a pointer to the vertex buffer, lock it, write the current pointer position, and unlock it.
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		HRESULT result = context->Map(m_D3D11.m_VertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
		if (SUCCEEDED(result))
		{
			void* ptr = mappedResource.pData;
			sysMemCpy(ptr, vertices, sizeof(SimpleVertex11) * 4);
			context->Unmap(m_D3D11.m_VertexBuffer, 0);
		}
	}
	rcatchall
	{

	}
}

void RgscCursor::RenderD3D11(ID3D11DeviceContext* context)
{
	if (!IsInitialized() || !IsValid())
		return;
	
	UINT stride = sizeof(SimpleVertex11);
	UINT offset = 0;

	// IsValid() above ensures an active pointer.
	RgscCursorResourceD3D11* cursor = static_cast<RgscCursorResourceD3D11*>(m_ActiveCursor);
	if (cursor)
	{
		// Set to our vertex buffer and shader view, and draw.
		context->IASetVertexBuffers(0, 1, &m_D3D11.m_VertexBuffer, &stride, &offset);
		context->PSSetShaderResources(0, 1, &cursor->m_ShaderResourceView);
		context->Draw(4, 0);
	}
}

void RgscCursor::SetEnabled(bool bEnabled)
{
	m_bEnabled = bEnabled;
}

bool RgscCursor::IsEnabled()
{
	return m_bEnabled;
}

void RgscCursor::SetPosition(float x, float y)
{
	// Set dirty flag if the position changes
	if (m_X != x || m_Y != y)
	{
		m_bDirty = true;
	}

	m_X = x;
	m_Y = y;
}

float RgscCursor::GetX()
{
	return m_X;
}

float RgscCursor::GetY()
{
	return m_Y;
}

void RgscCursor::SetCursorType(RgscCursorType cursorType)
{
	// early out if already using this cursor type
	if (m_CursorType == cursorType)
		return;

	// Change the cached cursor type and mark the cursor as dirty.
	m_CursorType = cursorType;
	m_bDirty = true;

	// Try to find a cursor that matches the cursor type
	RgscCursorResource** cursor = m_CursorMap.Access(cursorType);
	if (cursor)
	{
		m_ActiveCursor = *cursor;
		return;
	}

	// Try to find the default pointer cursor
	cursor = m_CursorMap.Access(RgscCursorType::RGSC_CURSOR_POINTER);
	if (cursor)
	{
		m_ActiveCursor = *cursor;
		return;
	}
	
	// No matching cursor or default pointer cursor - no active cursor
	m_ActiveCursor = NULL;
}

bool RgscCursor::IsInitialized()
{
	// Check if the device is initialized using the type we're setup as.
	if (m_DeviceType == DEVICE_D3D11)
		return (m_D3D11.m_Device != NULL);
	else if (m_DeviceType == DEVICE_D3D9)
		return (m_D3D9.m_Device != NULL);
	else
		return false;
}

bool RgscCursor::IsValid()
{
	// We're valid if we're: initialized, within the render rect, and have a valid cursor resource.
	return IsInitialized() && m_bInWindow && (m_ActiveCursor != NULL);
}

void RgscCursor::ReleaseResources()
{
	// No more active cursor
	m_ActiveCursor = NULL;

	// Free all cursor resources
	while(!m_Resources.empty())
	{
		delete m_Resources.Pop();
	}

	// Reset cursor mapping
	m_CursorMap.Reset();
	m_Resources.Reset();

	// Delete device resources
	m_D3D9.ReleaseResources();
	m_D3D11.ReleaseResources();
}

void RgscCursor::UpdateInWindow(Rgsc_Graphics::RenderRect& rect)
{
	// Check the x-coordinates to see if we're outside the render rect
	if (m_X < rect.m_Left || m_X > rect.m_Right)
	{
		m_bInWindow = false;
		return;
	}

	// Check the y-coordinates to see if we're in the render rect
	if (m_Y < rect.m_Top || m_Y > rect.m_Bottom)
	{
		m_bInWindow = false;
		return;
	}

	m_bInWindow = true;
}

bool RgscCursor::CreateNewCursor(RgscCursorType* cursorTypes, int numCursors, int width, int height, int* cursorData)
{
	bool success = false;

	RgscCursorResource* resource = NULL;

	rtry
	{
		// Can't create a new cursor resource if we're uninitialized
		rverify(IsInitialized(), catchall, );
		rverify(cursorTypes && numCursors > 0, catchall, );

		// Decide which type of cursor to create
		switch(m_DeviceType)
		{
		case DEVICE_D3D9:
			resource = rage_new RgscCursorResourceD3D9(m_D3D9.m_Device, width, height, cursorData);
			break;
		case DEVICE_D3D11:
			resource = rage_new RgscCursorResourceD3D11(m_D3D11.m_Device, width, height, cursorData);
			break;
		}

		// Resource must have created and initialized successfully.
		rverify(resource, catchall, );
		rverify(resource->IsValid(), catchall, );

		// Register the cursor types
		for (int i = 0; i < numCursors; i++)
		{
			RgscCursorType cursor = cursorTypes[i];
			rlAssertf(m_CursorMap.Access(cursor) == NULL, "Cursor %d already registered", cursor);
			m_CursorMap[cursor] = resource;
		}
		
		// Add to resource list
		m_Resources.PushAndGrow(resource);
		success = true;
	}
	rcatchall
	{
		// Cleanup the resource if it failed to create.
		if (resource)
		{
			delete resource;
			resource = NULL;
		}
	}

	return success;
}

} // namespace rgsc