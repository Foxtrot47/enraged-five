#ifndef RGSC_UI_INTERNAL_H
#define RGSC_UI_INTERNAL_H

#include "rgsc_ui.h"
#include "output.h"
#include "berkelium/Berkelium.hpp"

class RgscWindow;
class Rgsc_Graphics;

class RgscErrorDelegate : public Berkelium::ErrorDelegate
{
public:
	virtual void HandleAssert(const char* msg) {Assert(msg);}
	virtual	void HandleError(const char* msg) {Errorf(msg);}
	virtual bool HandleMessage(int severity, const char* msg)
	{
		// these must match Chromium's log severity constants
		const unsigned int LOG_INFO = 0;
		const unsigned int LOG_WARNING = 1;
		const unsigned int LOG_ERROR = 2;
		const unsigned int LOG_ERROR_REPORT = 3;
		const unsigned int LOG_FATAL = 4;

		switch(severity)
		{
		case LOG_INFO:
			Displayf(msg);
			break;
		case LOG_WARNING:
			Warningf(msg);
			break;
		case LOG_ERROR:
		case LOG_ERROR_REPORT:
			Errorf(msg);
			break;
		case LOG_FATAL:
			// TODO: NS - probably need to handle fatal errors by shutting down Berkelium and restarting
			AssertMsg(false, msg);
			break;
		}

		return true; // true means don't let chrome display error dialogs or exit the process
	}

	virtual ~RgscErrorDelegate() {}
};

class RgscUi : public IRgscUiLatestVersion,
			   public IRgscDelegateLatestVersion
{
public: // inherited from IRgscUi
	RgscUi();
	~RgscUi() {}

	// IUnknown
	HRESULT RGSC_CALL QueryInterface(REFIID riid, void** ppvObject);
	ULONG RGSC_CALL AddRef(void);
	ULONG RGSC_CALL Release(void);

	// IRgscUiV1
	HRESULT RGSC_CALL Init(IRgscDelegate &dlgt);
	void RGSC_CALL Shutdown();
	void RGSC_CALL Update();
	void RGSC_CALL Render();

	bool RGSC_CALL IsVisible();
	HRESULT RGSC_CALL OnCreateDevice(IUnknown* d3d, void* params);
	HRESULT RGSC_CALL OnResetDevice(void* params);
	HRESULT RGSC_CALL SignIn(const char* nickname, const char* password);
	HRESULT RGSC_CALL ShowSignInUi();
	HRESULT RGSC_CALL ShowGamerProfileUi(unsigned int profileId);

public: // inherited from RgscDelegate
	void RGSC_CALL Output(IRgscDelegateLatestVersion::OutputSeverity severity, const char* msg);
	//void RGSC_CALL OnSignIn(const char* nickname, int profileId);
	//void RGSC_CALL NewCallbackFunc();

public: // accessible from anywhere in the rgsc_ui.dll
	void CallMyInternalFunction() {}

private: // only accessible from rgsc_ui.cpp
	static const unsigned int RAGE_MAX_PATH = MAX_PATH;
	void SetDelegate(IRgscDelegate* dlgt);
	bool GetPlatformDirectory(char (&path)[RAGE_MAX_PATH], bool bCreateDirectory);
	void ShowUi();
	void HideUi();
	void LoadUrl(const char* url);
	void SetupScriptCommands();

	ULONG m_RefCount;                 // COM reference count
	IRgscDelegate *m_Delegate;
	IRgscDelegateV1 *m_DelegateV1;
	RgscWindow* m_Window;
	Rgsc_Graphics* m_Device;
	HCURSOR m_Cursor;
	CURSORINFO m_OldCursorInfo;
	RgscErrorDelegate m_ErrorDelegate;
	bool m_WasUiJustHidden;
};

extern RgscUi* GetRgscUiConcreteInstance();

#endif //RGSC_UI_INTERNAL_H
