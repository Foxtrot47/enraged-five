// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef RGSC_COMMON_MESSAGES_PARAMS_H_
#define RGSC_COMMON_MESSAGES_PARAMS_H_
#pragma once

#include "ipc/ipc_param_traits.h"
#pragma warning(push)
#pragma warning(disable: 4310)
#include "base/basictypes.h"
#pragma warning(pop)
#include "types.h"

#include <string>
#include <vector>

struct ViewHostMsg_OnJavascriptCallback_Params
{
	ViewHostMsg_OnJavascriptCallback_Params();
	~ViewHostMsg_OnJavascriptCallback_Params();

	std::wstring m_FunctionName;
	std::wstring m_Arg;
	std::string m_IpcChannel;
};

struct ViewHostMsg_OnJavascriptCallback_ReturnVal
{
	ViewHostMsg_OnJavascriptCallback_ReturnVal();
	~ViewHostMsg_OnJavascriptCallback_ReturnVal();

	enum ValueType {
		TYPE_INVALID,
		TYPE_STRING,
		TYPE_BOOL,
		TYPE_INT,
		TYPE_DOUBLE,
	};

	ValueType m_Type;
	std::wstring m_String;
	bool m_Bool;
	int m_Int;
	double m_Double;
};

struct ViewMsg_Tile_Params
{
	ViewMsg_Tile_Params();
	~ViewMsg_Tile_Params();

	std::vector<rgsc::RgscRect> tile_rects;
};

struct ViewMsg_Rect_Params
{
	ViewMsg_Rect_Params();
	~ViewMsg_Rect_Params();

	int x;
	int y;
	int width;
	int height;
};

struct ViewMsg_CreateWindow_Params
{
	ViewMsg_CreateWindow_Params();
	~ViewMsg_CreateWindow_Params();

	uint64 hWnd; // window to create inside
	uint64 hRootParent; // upper-most parent
	float scale;
	int borderWidth;
	int resizeEdges;
	int captionHeight;
	int sizeGripSize;
	int windowType; // rgsc::RgscWindowType
	int minWidth;
	int minHeight;
	int maxWidth;
	int maxHeight;
	bool hideOnClose;
	std::wstring windowTitle;
};

namespace IPC
{
	class Message;

	template <>
	struct ParamTraits<ViewHostMsg_OnJavascriptCallback_Params> {
		typedef ViewHostMsg_OnJavascriptCallback_Params param_type;
		static void Write(Message* m, const param_type& p);
		static bool Read(const Message* m, void** iter, param_type* p);
		static void Log(const param_type& p, std::string* l);
	};

	template <>
	struct ParamTraits<ViewHostMsg_OnJavascriptCallback_ReturnVal> {
		typedef ViewHostMsg_OnJavascriptCallback_ReturnVal param_type;
		static void Write(Message* m, const param_type& p);
		static bool Read(const Message* m, void** iter, param_type* p);
		static void Log(const param_type& p, std::string* l);
	};

	template <>
	struct ParamTraits<ViewMsg_Tile_Params> {
		typedef ViewMsg_Tile_Params param_type;
		static void Write(Message* m, const param_type& p);
		static bool Read(const Message* m, void** iter, param_type* p);
		static void Log(const param_type& p, std::string* l);
	};

	template <>
	struct ParamTraits<ViewMsg_Rect_Params> {
		typedef ViewMsg_Rect_Params param_type;
		static void Write(Message* m, const param_type& p);
		static bool Read(const Message* m, void** iter, param_type* p);
		static void Log(const param_type& p, std::string* l);
	};

	template <>
	struct ParamTraits<ViewMsg_CreateWindow_Params> {
		typedef ViewMsg_CreateWindow_Params param_type;
		static void Write(Message* m, const param_type& p);
		static bool Read(const Message* m, void** iter, param_type* p);
		static void Log(const param_type& p, std::string* l);
	};
}

#endif  // RGSC_COMMON_MESSAGES_PARAMS_H_
