// 
// Ipc.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RGSC_IPC_H 
#define RGSC_IPC_H

#include "atl/delegate.h"

// ipc forward declarations
class MessageLoopForIO;

namespace ipcbase
{
	class Thread;
	class WaitableEvent;
}

namespace IPC
{
	class SyncChannel;
	class Message;
}

namespace rgsc
{

class ChannelListener;

class Ipc
{
	typedef rage::atDelegator<void (const class ::IPC::Message&)> Delegator;
	friend class ChannelListener;
public:

	static const int MAX_CHANNEL_NAME_LEN = 64;
	static const int MAX_IPC_CHANNELS = 64;

	enum Mode {
		MODE_SERVER,
		MODE_CLIENT
	};

	typedef Delegator::Delegate Delegate;

	Ipc();
	virtual ~Ipc();

	bool Init(Mode mode, const char* ipcChannelName);
	void Shutdown();
	void Update();
	bool AddChannel(Mode mode, const char* ipcChannelName);
	bool DeleteChannel(const char* ipcChannelName);
	unsigned GetNumChannels();
	unsigned GetNextChannelId();
	const char* GetChannelName();

	//PURPOSE
	//  Add/remove a delegate that will be called when messages arrive over ipc.
	void AddDelegate(Delegate* dlgt);
	void RemoveDelegate(Delegate* dlgt);
	void DispatchEvent(const IPC::Message& message);

	bool Send(IPC::Message* message);
	bool Send(IPC::Message* message, const char* channelName);

private:

	void DeleteChannel(int channelIndex);
	void OnMessageReceived(const IPC::Message& message);
	void OnChannelConnected(rage::s32 peerPid);
	void OnChannelError();

	Delegator m_Delegator;
	MessageLoopForIO* m_MessageLoop;
	ChannelListener* m_ChannelListener[MAX_IPC_CHANNELS];
	ipcbase::Thread* m_Thread[MAX_IPC_CHANNELS];
	IPC::SyncChannel* m_Channels[MAX_IPC_CHANNELS];
	char m_ChannelName[MAX_IPC_CHANNELS][MAX_CHANNEL_NAME_LEN];
	ipcbase::WaitableEvent* m_ShutdownEvent[MAX_IPC_CHANNELS];

	unsigned m_NumChannels;
	unsigned m_NextChannelId;
};

} // namespace rgsc

#endif // RGSC_IPC_H 
