// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "rgsc_messages_params.h"
#include "rgsc_messages.h"

#if 0

#include "chrome/common/navigation_gesture.h"
#include "chrome/common/common_param_traits.h"
#include "chrome/common/indexed_db_param_traits.h"
#include "chrome/common/render_messages.h"
#include "net/base/upload_data.h"

ViewMsg_Navigate_Params::ViewMsg_Navigate_Params()
    : page_id(-1),
      pending_history_list_offset(-1),
      current_history_list_offset(-1),
      current_history_list_length(0),
      transition(PageTransition::LINK),
      navigation_type(NORMAL) {
}

ViewMsg_Navigate_Params::~ViewMsg_Navigate_Params() {
}

ViewHostMsg_FrameNavigate_Params::ViewHostMsg_FrameNavigate_Params()
    : page_id(0),
      frame_id(0),
      transition(PageTransition::TYPED),
      should_update_history(false),
      gesture(NavigationGestureUser),
      is_post(false),
      is_content_filtered(false),
      was_within_same_page(false),
      http_status_code(0) {
}

ViewHostMsg_FrameNavigate_Params::~ViewHostMsg_FrameNavigate_Params() {
}

ViewMsg_ClosePage_Params::ViewMsg_ClosePage_Params()
    : closing_process_id(0),
      closing_route_id(0),
      for_cross_site_transition(false),
      new_render_process_host_id(0),
      new_request_id(0) {
}

ViewMsg_ClosePage_Params::~ViewMsg_ClosePage_Params() {
}

ViewHostMsg_Resource_Request::ViewHostMsg_Resource_Request()
    : load_flags(0),
      origin_child_id(0),
      resource_type(ResourceType::MAIN_FRAME),
      request_context(0),
      appcache_host_id(0),
      download_to_file(false),
      host_renderer_id(0),
      host_render_view_id(0) {
}

ViewHostMsg_Resource_Request::~ViewHostMsg_Resource_Request() {
}

ViewMsg_Print_Params::ViewMsg_Print_Params()
    : margin_top(0),
      margin_left(0),
      dpi(0),
      min_shrink(0),
      max_shrink(0),
      desired_dpi(0),
      document_cookie(0),
      selection_only(false) {
}

ViewMsg_Print_Params::~ViewMsg_Print_Params() {
}

bool ViewMsg_Print_Params::Equals(const ViewMsg_Print_Params& rhs) const {
  return page_size == rhs.page_size &&
         printable_size == rhs.printable_size &&
         margin_top == rhs.margin_top &&
         margin_left == rhs.margin_left &&
         dpi == rhs.dpi &&
         min_shrink == rhs.min_shrink &&
         max_shrink == rhs.max_shrink &&
         desired_dpi == rhs.desired_dpi &&
         selection_only == rhs.selection_only;
}

bool ViewMsg_Print_Params::IsEmpty() const {
  return !document_cookie && !desired_dpi && !max_shrink && !min_shrink &&
         !dpi && printable_size.IsEmpty() && !selection_only &&
         page_size.IsEmpty() && !margin_top && !margin_left;
}

ViewMsg_PrintPage_Params::ViewMsg_PrintPage_Params()
    : page_number(0) {
}

ViewMsg_PrintPage_Params::~ViewMsg_PrintPage_Params() {
}

ViewMsg_PrintPages_Params::ViewMsg_PrintPages_Params() {
}

ViewMsg_PrintPages_Params::~ViewMsg_PrintPages_Params() {
}

ViewHostMsg_DidPrintPage_Params::ViewHostMsg_DidPrintPage_Params()
    : data_size(0),
      document_cookie(0),
      page_number(0),
      actual_shrink(0),
      has_visible_overlays(false) {
#if defined(OS_WIN)
    // Initialize |metafile_data_handle| only on Windows because it maps
  // ipcbase::SharedMemoryHandle to HANDLE. We do not need to initialize this
  // variable on Posix because it maps ipcbase::SharedMemoryHandle to
  // FileDescriptior, which has the default constructor.
  metafile_data_handle = INVALID_HANDLE_VALUE;
#endif
}

ViewHostMsg_DidPrintPage_Params::~ViewHostMsg_DidPrintPage_Params() {
}

ViewHostMsg_Audio_CreateStream_Params::ViewHostMsg_Audio_CreateStream_Params()
    : packet_size(0) {
}

ViewHostMsg_Audio_CreateStream_Params::
    ~ViewHostMsg_Audio_CreateStream_Params() {
}

ViewHostMsg_ScriptedPrint_Params::ViewHostMsg_ScriptedPrint_Params()
    : routing_id(0),
      host_window_id(0),
      cookie(0),
      expected_pages_count(0),
      has_selection(false),
      use_overlays(false) {
}

ViewHostMsg_ScriptedPrint_Params::~ViewHostMsg_ScriptedPrint_Params() {
}

ViewMsg_DOMStorageEvent_Params::ViewMsg_DOMStorageEvent_Params()
    : storage_type_(DOM_STORAGE_LOCAL) {
}

ViewMsg_DOMStorageEvent_Params::~ViewMsg_DOMStorageEvent_Params() {
}

ViewHostMsg_IDBFactoryOpen_Params::ViewHostMsg_IDBFactoryOpen_Params()
    : routing_id_(0),
      response_id_(0),
      maximum_size_(0) {
}

ViewHostMsg_IDBFactoryOpen_Params::~ViewHostMsg_IDBFactoryOpen_Params() {
}

ViewHostMsg_IDBDatabaseCreateObjectStore_Params::
    ViewHostMsg_IDBDatabaseCreateObjectStore_Params()
    : auto_increment_(false),
      transaction_id_(0),
      idb_database_id_(0) {
}

ViewHostMsg_IDBDatabaseCreateObjectStore_Params::
    ~ViewHostMsg_IDBDatabaseCreateObjectStore_Params() {
}

ViewHostMsg_IDBIndexOpenCursor_Params::ViewHostMsg_IDBIndexOpenCursor_Params()
    : response_id_(0),
      key_flags_(0),
      direction_(0),
      idb_index_id_(0),
      transaction_id_(0) {
}

ViewHostMsg_IDBIndexOpenCursor_Params::
    ~ViewHostMsg_IDBIndexOpenCursor_Params() {
}


ViewHostMsg_IDBObjectStorePut_Params::ViewHostMsg_IDBObjectStorePut_Params()
    : idb_object_store_id_(0),
      response_id_(0),
      add_only_(false),
      transaction_id_(0) {
}

ViewHostMsg_IDBObjectStorePut_Params::~ViewHostMsg_IDBObjectStorePut_Params() {
}

ViewHostMsg_IDBObjectStoreCreateIndex_Params::
ViewHostMsg_IDBObjectStoreCreateIndex_Params()
    : unique_(false),
      transaction_id_(0),
      idb_object_store_id_(0) {
}

ViewHostMsg_IDBObjectStoreCreateIndex_Params::
~ViewHostMsg_IDBObjectStoreCreateIndex_Params() {
}


ViewHostMsg_IDBObjectStoreOpenCursor_Params::
    ViewHostMsg_IDBObjectStoreOpenCursor_Params()
    : response_id_(0),
      flags_(0),
      direction_(0),
      idb_object_store_id_(0),
      transaction_id_(0) {
}

ViewHostMsg_IDBObjectStoreOpenCursor_Params::
    ~ViewHostMsg_IDBObjectStoreOpenCursor_Params() {
}

ViewMsg_ExecuteCode_Params::ViewMsg_ExecuteCode_Params() {
}

ViewMsg_ExecuteCode_Params::ViewMsg_ExecuteCode_Params(
    int request_id,
    const std::string& extension_id,
    bool is_javascript,
    const std::string& code,
    bool all_frames)
    : request_id(request_id), extension_id(extension_id),
      is_javascript(is_javascript),
      code(code), all_frames(all_frames) {
}

ViewMsg_ExecuteCode_Params::~ViewMsg_ExecuteCode_Params() {
}

ViewHostMsg_CreateWorker_Params::ViewHostMsg_CreateWorker_Params()
    : is_shared(false),
      document_id(0),
      render_view_route_id(0),
      route_id(0),
      parent_appcache_host_id(0),
      script_resource_appcache_id(0) {
}

ViewHostMsg_CreateWorker_Params::~ViewHostMsg_CreateWorker_Params() {
}

ViewHostMsg_ShowNotification_Params::ViewHostMsg_ShowNotification_Params()
    : is_html(false),
      direction(WebKit::WebTextDirectionDefault),
      notification_id(0) {
}

ViewHostMsg_ShowNotification_Params::~ViewHostMsg_ShowNotification_Params() {
}

ViewMsg_New_Params::ViewMsg_New_Params()
    : parent_window(0),
      view_id(0),
      session_storage_namespace_id(0) {
}

ViewMsg_New_Params::~ViewMsg_New_Params() {
}

ViewHostMsg_CreateWindow_Params::ViewHostMsg_CreateWindow_Params()
    : opener_id(0),
      user_gesture(false),
      window_container_type(WINDOW_CONTAINER_TYPE_NORMAL),
      session_storage_namespace_id(0) {
}

ViewHostMsg_CreateWindow_Params::~ViewHostMsg_CreateWindow_Params() {
}

ViewHostMsg_RunFileChooser_Params::ViewHostMsg_RunFileChooser_Params()
    : mode(Open) {
}

ViewHostMsg_RunFileChooser_Params::~ViewHostMsg_RunFileChooser_Params() {
}

ViewMsg_ExtensionRendererInfo::ViewMsg_ExtensionRendererInfo()
    : location(Extension::INVALID),
      allowed_to_execute_script_everywhere(false) {
}

ViewMsg_ExtensionRendererInfo::~ViewMsg_ExtensionRendererInfo() {
}

ViewMsg_ExtensionsUpdated_Params::ViewMsg_ExtensionsUpdated_Params() {
}

ViewMsg_ExtensionsUpdated_Params::~ViewMsg_ExtensionsUpdated_Params() {
}

ViewMsg_DeviceOrientationUpdated_Params::
    ViewMsg_DeviceOrientationUpdated_Params()
    : can_provide_alpha(false),
      alpha(0),
      can_provide_beta(false),
      beta(0),
      can_provide_gamma(false),
      gamma(0) {
}

ViewMsg_DeviceOrientationUpdated_Params::
    ~ViewMsg_DeviceOrientationUpdated_Params() {
}

ViewHostMsg_DomMessage_Params::ViewHostMsg_DomMessage_Params()
    : request_id(0),
      has_callback(false),
      user_gesture(false) {
}

ViewHostMsg_DomMessage_Params::~ViewHostMsg_DomMessage_Params() {
}

namespace IPC {

// Self contained templates which are only used inside serializing Params
// structs.
template<>
struct ParamTraits<ViewMsg_Navigate_Params::NavigationType> {
  typedef ViewMsg_Navigate_Params::NavigationType param_type;
  static void Write(Message* m, const param_type& p) {
    m->WriteInt(p);
  }
  static bool Read(const Message* m, void** iter, param_type* p) {
    int type;
    if (!m->ReadInt(iter, &type))
      return false;
    *p = static_cast<ViewMsg_Navigate_Params::NavigationType>(type);
    return true;
  }
  static void Log(const param_type& p, std::string* l) {
    std::string event;
    switch (p) {
      case ViewMsg_Navigate_Params::RELOAD:
        event = "NavigationType_RELOAD";
        break;

      case ViewMsg_Navigate_Params::RELOAD_IGNORING_CACHE:
        event = "NavigationType_RELOAD_IGNORING_CACHE";
        break;

      case ViewMsg_Navigate_Params::RESTORE:
        event = "NavigationType_RESTORE";
        break;

      case ViewMsg_Navigate_Params::NORMAL:
        event = "NavigationType_NORMA";
        break;

      default:
        event = "NavigationType_UNKNOWN";
        break;
    }
    LogParam(event, l);
  }
};

template <>
struct ParamTraits<ResourceType::Type> {
  typedef ResourceType::Type param_type;
  static void Write(Message* m, const param_type& p) {
    m->WriteInt(p);
  }
  static bool Read(const Message* m, void** iter, param_type* p) {
    int type;
    if (!m->ReadInt(iter, &type) || !ResourceType::ValidType(type))
      return false;
    *p = ResourceType::FromInt(type);
    return true;
  }
  static void Log(const param_type& p, std::string* l) {
    std::string type;
    switch (p) {
      case ResourceType::MAIN_FRAME:
        type = "MAIN_FRAME";
        break;
      case ResourceType::SUB_FRAME:
        type = "SUB_FRAME";
        break;
      case ResourceType::SUB_RESOURCE:
        type = "SUB_RESOURCE";
        break;
      case ResourceType::OBJECT:
        type = "OBJECT";
        break;
      case ResourceType::MEDIA:
        type = "MEDIA";
        break;
      default:
        type = "UNKNOWN";
        break;
    }

    LogParam(type, l);
  }
};

template<>
struct ParamTraits<NavigationGesture> {
  typedef NavigationGesture param_type;
  static void Write(Message* m, const param_type& p) {
    m->WriteInt(p);
  }
  static bool Read(const Message* m, void** iter, param_type* p) {
    int type;
    if (!m->ReadInt(iter, &type))
      return false;
    *p = static_cast<NavigationGesture>(type);
    return true;
  }
  static void Log(const param_type& p, std::string* l) {
    std::string event;
    switch (p) {
      case NavigationGestureUser:
        event = "GESTURE_USER";
        break;
      case NavigationGestureAuto:
        event = "GESTURE_AUTO";
        break;
      default:
        event = "GESTURE_UNKNOWN";
        break;
    }
    LogParam(event, l);
  }
};

// Traits for AudioManager::Format.
template <>
struct ParamTraits<AudioParameters::Format> {
  typedef AudioParameters::Format param_type;
  static void Write(Message* m, const param_type& p) {
    m->WriteInt(p);
  }
  static bool Read(const Message* m, void** iter, param_type* p) {
    int type;
    if (!m->ReadInt(iter, &type))
      return false;
    *p = static_cast<AudioParameters::Format>(type);
    return true;
  }
  static void Log(const param_type& p, std::string* l) {
    std::string format;
    switch (p) {
     case AudioParameters::AUDIO_PCM_LINEAR:
       format = "AUDIO_PCM_LINEAR";
       break;
     case AudioParameters::AUDIO_PCM_LOW_LATENCY:
       format = "AUDIO_PCM_LOW_LATENCY";
       break;
     case AudioParameters::AUDIO_MOCK:
       format = "AUDIO_MOCK";
       break;
     default:
       format = "AUDIO_LAST_FORMAT";
       break;
    }
    LogParam(format, l);
  }
};

template <>
struct ParamTraits<WindowContainerType> {
  typedef WindowContainerType param_type;
  static void Write(Message* m, const param_type& p) {
    int val = static_cast<int>(p);
    WriteParam(m, val);
  }
  static bool Read(const Message* m, void** iter, param_type* p) {
    int val = 0;
    if (!ReadParam(m, iter, &val) ||
        val < WINDOW_CONTAINER_TYPE_NORMAL ||
        val >= WINDOW_CONTAINER_TYPE_MAX_VALUE)
      return false;
    *p = static_cast<param_type>(val);
    return true;
  }
  static void Log(const param_type& p, std::string* l) {
    ParamTraits<int>::Log(static_cast<int>(p), l);
  }
};

template <>
struct ParamTraits<Extension::Location> {
  typedef Extension::Location param_type;
  static void Write(Message* m, const param_type& p) {
    int val = static_cast<int>(p);
    WriteParam(m, val);
  }
  static bool Read(const Message* m, void** iter, param_type* p) {
    int val = 0;
    if (!ReadParam(m, iter, &val) ||
        val < Extension::INVALID ||
        val > Extension::EXTERNAL_PREF_DOWNLOAD)
      return false;
    *p = static_cast<param_type>(val);
    return true;
  }
  static void Log(const param_type& p, std::string* l) {
    ParamTraits<int>::Log(static_cast<int>(p), l);
  }
};

template <>
struct ParamTraits
    <ViewHostMsg_AccessibilityNotification_Params::NotificationType> {
  typedef ViewHostMsg_AccessibilityNotification_Params params;
  typedef params::NotificationType param_type;
  static void Write(Message* m, const param_type& p) {
    int val = static_cast<int>(p);
    WriteParam(m, val);
  }
  static bool Read(const Message* m, void** iter, param_type* p) {
    int val = 0;
    if (!ReadParam(m, iter, &val) ||
        val < params::NOTIFICATION_TYPE_CHECK_STATE_CHANGED ||
        val > params::NOTIFICATION_TYPE_SELECTED_TEXT_CHANGED) {
      return false;
    }
    *p = static_cast<param_type>(val);
    return true;
  }
  static void Log(const param_type& p, std::string* l) {
    ParamTraits<int>::Log(static_cast<int>(p), l);
  }
};


void ParamTraits<ViewMsg_Navigate_Params>::Write(Message* m,
                                                 const param_type& p) {
  WriteParam(m, p.page_id);
  WriteParam(m, p.pending_history_list_offset);
  WriteParam(m, p.current_history_list_offset);
  WriteParam(m, p.current_history_list_length);
  WriteParam(m, p.url);
  WriteParam(m, p.referrer);
  WriteParam(m, p.transition);
  WriteParam(m, p.state);
  WriteParam(m, p.navigation_type);
  WriteParam(m, p.request_time);
}

bool ParamTraits<ViewMsg_Navigate_Params>::Read(const Message* m, void** iter,
                                                param_type* p) {
  return
      ReadParam(m, iter, &p->page_id) &&
      ReadParam(m, iter, &p->pending_history_list_offset) &&
      ReadParam(m, iter, &p->current_history_list_offset) &&
      ReadParam(m, iter, &p->current_history_list_length) &&
      ReadParam(m, iter, &p->url) &&
      ReadParam(m, iter, &p->referrer) &&
      ReadParam(m, iter, &p->transition) &&
      ReadParam(m, iter, &p->state) &&
      ReadParam(m, iter, &p->navigation_type) &&
      ReadParam(m, iter, &p->request_time);
}

void ParamTraits<ViewMsg_Navigate_Params>::Log(const param_type& p,
                                               std::string* l) {
  l->append("(");
  LogParam(p.page_id, l);
  l->append(", ");
  LogParam(p.url, l);
  l->append(", ");
  LogParam(p.transition, l);
  l->append(", ");
  LogParam(p.state, l);
  l->append(", ");
  LogParam(p.navigation_type, l);
  l->append(", ");
  LogParam(p.request_time, l);
  l->append(")");
}

void ParamTraits<ViewMsg_AudioStreamState_Params>::Write(Message* m,
                                                         const param_type& p) {
  m->WriteInt(p.state);
}

bool ParamTraits<ViewMsg_AudioStreamState_Params>::Read(const Message* m,
                                                        void** iter,
                                                        param_type* p) {
  int type;
  if (!m->ReadInt(iter, &type))
    return false;
  p->state = static_cast<ViewMsg_AudioStreamState_Params::State>(type);
  return true;
}

void ParamTraits<ViewMsg_AudioStreamState_Params>::Log(const param_type& p,
                                                       std::string* l) {
  std::string state;
  switch (p.state) {
    case ViewMsg_AudioStreamState_Params::kPlaying:
      state = "ViewMsg_AudioStreamState_Params::kPlaying";
      break;
    case ViewMsg_AudioStreamState_Params::kPaused:
      state = "ViewMsg_AudioStreamState_Params::kPaused";
      break;
    case ViewMsg_AudioStreamState_Params::kError:
      state = "ViewMsg_AudioStreamState_Params::kError";
      break;
    default:
      state = "UNKNOWN";
      break;
  }
  LogParam(state, l);
}

void ParamTraits<ViewMsg_StopFinding_Params>::Write(Message* m,
                                                    const param_type& p) {
  m->WriteInt(p.action);
}

bool ParamTraits<ViewMsg_StopFinding_Params>::Read(const Message* m,
                                                   void** iter,
                                                   param_type* p) {
  int type;
  if (!m->ReadInt(iter, &type))
    return false;
  p->action = static_cast<ViewMsg_StopFinding_Params::Action>(type);
  return true;
}

void ParamTraits<ViewMsg_StopFinding_Params>::Log(const param_type& p,
                                                  std::string* l) {
  std::string action;
  switch (p.action) {
    case ViewMsg_StopFinding_Params::kClearSelection:
      action = "ViewMsg_StopFinding_Params::kClearSelection";
      break;
    case ViewMsg_StopFinding_Params::kKeepSelection:
      action = "ViewMsg_StopFinding_Params::kKeepSelection";
      break;
    case ViewMsg_StopFinding_Params::kActivateSelection:
      action = "ViewMsg_StopFinding_Params::kActivateSelection";
      break;
    default:
      action = "UNKNOWN";
      break;
  }
  LogParam(action, l);
}

void ParamTraits<ViewHostMsg_PageHasOSDD_Type>::Write(Message* m,
                                                      const param_type& p) {
  m->WriteInt(p.type);
}

bool ParamTraits<ViewHostMsg_PageHasOSDD_Type>::Read(const Message* m,
                                                     void** iter,
                                                     param_type* p) {
  int type;
  if (!m->ReadInt(iter, &type))
    return false;
  p->type = static_cast<param_type::Type>(type);
  return true;
}

void ParamTraits<ViewHostMsg_PageHasOSDD_Type>::Log(const param_type& p,
                                                    std::string* l) {
  std::string type;
  switch (p.type) {
    case ViewHostMsg_PageHasOSDD_Type::AUTODETECTED_PROVIDER:
      type = "ViewHostMsg_PageHasOSDD_Type::AUTODETECTED_PROVIDER";
      break;
    case ViewHostMsg_PageHasOSDD_Type::EXPLICIT_PROVIDER:
      type = "ViewHostMsg_PageHasOSDD_Type::EXPLICIT_PROVIDER";
      break;
    case ViewHostMsg_PageHasOSDD_Type::EXPLICIT_DEFAULT_PROVIDER:
      type = "ViewHostMsg_PageHasOSDD_Type::EXPLICIT_DEFAULT_PROVIDER";
      break;
    default:
      type = "UNKNOWN";
      break;
  }
  LogParam(type, l);
}

void ParamTraits<ViewHostMsg_GetSearchProviderInstallState_Params>::Write(
    Message* m, const param_type& p) {
  m->WriteInt(p.state);
}

bool ParamTraits<ViewHostMsg_GetSearchProviderInstallState_Params>::Read(
    const Message* m, void** iter, param_type* p) {
  int type;
  if (!m->ReadInt(iter, &type))
    return false;
  p->state = static_cast<param_type::State>(type);
  return true;
}

void ParamTraits<ViewHostMsg_GetSearchProviderInstallState_Params>::Log(
    const param_type& p, std::string* l) {
  std::string state;
  switch (p.state) {
    case ViewHostMsg_GetSearchProviderInstallState_Params::DENIED:
      state = "ViewHostMsg_GetSearchProviderInstallState_Params::DENIED";
      break;
    case ViewHostMsg_GetSearchProviderInstallState_Params::NOT_INSTALLED:
      state =
          "ViewHostMsg_GetSearchProviderInstallState_Params::NOT_INSTALLED";
      break;
    case ViewHostMsg_GetSearchProviderInstallState_Params::
        INSTALLED_BUT_NOT_DEFAULT:
      state = "ViewHostMsg_GetSearchProviderInstallState_Params::"
              "INSTALLED_BUT_NOT_DEFAULT";
      break;
    case ViewHostMsg_GetSearchProviderInstallState_Params::
        INSTALLED_AS_DEFAULT:
      state = "ViewHostMsg_GetSearchProviderInstallState_Params::"
              "INSTALLED_AS_DEFAULT";
      break;
    default:
      state = "UNKNOWN";
      break;
  }
  LogParam(state, l);
}

void ParamTraits<ViewHostMsg_FrameNavigate_Params>::Write(Message* m,
                                                          const param_type& p) {
  WriteParam(m, p.page_id);
  WriteParam(m, p.frame_id);
  WriteParam(m, p.url);
  WriteParam(m, p.referrer);
  WriteParam(m, p.transition);
  WriteParam(m, p.redirects);
  WriteParam(m, p.should_update_history);
  WriteParam(m, p.searchable_form_url);
  WriteParam(m, p.searchable_form_encoding);
  WriteParam(m, p.password_form);
  WriteParam(m, p.security_info);
  WriteParam(m, p.gesture);
  WriteParam(m, p.contents_mime_type);
  WriteParam(m, p.is_post);
  WriteParam(m, p.is_content_filtered);
  WriteParam(m, p.was_within_same_page);
  WriteParam(m, p.http_status_code);
}

bool ParamTraits<ViewHostMsg_FrameNavigate_Params>::Read(const Message* m,
                                                         void** iter,
                                                         param_type* p) {
  return
      ReadParam(m, iter, &p->page_id) &&
      ReadParam(m, iter, &p->frame_id) &&
      ReadParam(m, iter, &p->url) &&
      ReadParam(m, iter, &p->referrer) &&
      ReadParam(m, iter, &p->transition) &&
      ReadParam(m, iter, &p->redirects) &&
      ReadParam(m, iter, &p->should_update_history) &&
      ReadParam(m, iter, &p->searchable_form_url) &&
      ReadParam(m, iter, &p->searchable_form_encoding) &&
      ReadParam(m, iter, &p->password_form) &&
      ReadParam(m, iter, &p->security_info) &&
      ReadParam(m, iter, &p->gesture) &&
      ReadParam(m, iter, &p->contents_mime_type) &&
      ReadParam(m, iter, &p->is_post) &&
      ReadParam(m, iter, &p->is_content_filtered) &&
      ReadParam(m, iter, &p->was_within_same_page) &&
      ReadParam(m, iter, &p->http_status_code);
}

void ParamTraits<ViewHostMsg_FrameNavigate_Params>::Log(const param_type& p,
                                                        std::string* l) {
  l->append("(");
  LogParam(p.page_id, l);
  l->append(", ");
  LogParam(p.frame_id, l);
  l->append(", ");
  LogParam(p.url, l);
  l->append(", ");
  LogParam(p.referrer, l);
  l->append(", ");
  LogParam(p.transition, l);
  l->append(", ");
  LogParam(p.redirects, l);
  l->append(", ");
  LogParam(p.should_update_history, l);
  l->append(", ");
  LogParam(p.searchable_form_url, l);
  l->append(", ");
  LogParam(p.searchable_form_encoding, l);
  l->append(", ");
  LogParam(p.password_form, l);
  l->append(", ");
  LogParam(p.security_info, l);
  l->append(", ");
  LogParam(p.gesture, l);
  l->append(", ");
  LogParam(p.contents_mime_type, l);
  l->append(", ");
  LogParam(p.is_post, l);
  l->append(", ");
  LogParam(p.is_content_filtered, l);
  l->append(", ");
  LogParam(p.was_within_same_page, l);
  l->append(", ");
  LogParam(p.http_status_code, l);
  l->append(")");
}

void ParamTraits<ViewMsg_Tile_Params>::Write(
    Message* m, const param_type& p) {
  WriteParam(m, p.tile_rects);
}

bool ParamTraits<ViewMsg_Tile_Params>::Read(
    const Message* m, void** iter, param_type* p) {
  return
      ReadParam(m, iter, &p->tile_rects);
}

void ParamTraits<ViewMsg_Tile_Params>::Log(const param_type& p,
                                                     std::string* l) {
  l->append("(");
  LogParam(p.tile_rects, l);
  l->append(")");
}

void ParamTraits<ViewMsg_ClosePage_Params>::Write(Message* m,
                                                  const param_type& p) {
  WriteParam(m, p.closing_process_id);
  WriteParam(m, p.closing_route_id);
  WriteParam(m, p.for_cross_site_transition);
  WriteParam(m, p.new_render_process_host_id);
  WriteParam(m, p.new_request_id);
}

bool ParamTraits<ViewMsg_ClosePage_Params>::Read(const Message* m,
                                                 void** iter,
                                                 param_type* r) {
  return ReadParam(m, iter, &r->closing_process_id) &&
      ReadParam(m, iter, &r->closing_route_id) &&
      ReadParam(m, iter, &r->for_cross_site_transition) &&
      ReadParam(m, iter, &r->new_render_process_host_id) &&
      ReadParam(m, iter, &r->new_request_id);
}

void ParamTraits<ViewMsg_ClosePage_Params>::Log(const param_type& p,
                                                std::string* l) {
  l->append("(");
  LogParam(p.closing_process_id, l);
  l->append(", ");
  LogParam(p.closing_route_id, l);
  l->append(", ");
  LogParam(p.for_cross_site_transition, l);
  l->append(", ");
  LogParam(p.new_render_process_host_id, l);
  l->append(", ");
  LogParam(p.new_request_id, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_Resource_Request>::Write(Message* m,
                                                      const param_type& p) {
  WriteParam(m, p.method);
  WriteParam(m, p.url);
  WriteParam(m, p.first_party_for_cookies);
  WriteParam(m, p.referrer);
  WriteParam(m, p.frame_origin);
  WriteParam(m, p.main_frame_origin);
  WriteParam(m, p.headers);
  WriteParam(m, p.load_flags);
  WriteParam(m, p.origin_child_id);
  WriteParam(m, p.resource_type);
  WriteParam(m, p.request_context);
  WriteParam(m, p.appcache_host_id);
  WriteParam(m, p.upload_data);
  WriteParam(m, p.download_to_file);
  WriteParam(m, p.host_renderer_id);
  WriteParam(m, p.host_render_view_id);
}

bool ParamTraits<ViewHostMsg_Resource_Request>::Read(const Message* m,
                                                     void** iter,
                                                     param_type* r) {
  return
      ReadParam(m, iter, &r->method) &&
      ReadParam(m, iter, &r->url) &&
      ReadParam(m, iter, &r->first_party_for_cookies) &&
      ReadParam(m, iter, &r->referrer) &&
      ReadParam(m, iter, &r->frame_origin) &&
      ReadParam(m, iter, &r->main_frame_origin) &&
      ReadParam(m, iter, &r->headers) &&
      ReadParam(m, iter, &r->load_flags) &&
      ReadParam(m, iter, &r->origin_child_id) &&
      ReadParam(m, iter, &r->resource_type) &&
      ReadParam(m, iter, &r->request_context) &&
      ReadParam(m, iter, &r->appcache_host_id) &&
      ReadParam(m, iter, &r->upload_data) &&
      ReadParam(m, iter, &r->download_to_file) &&
      ReadParam(m, iter, &r->host_renderer_id) &&
      ReadParam(m, iter, &r->host_render_view_id);
}

void ParamTraits<ViewHostMsg_Resource_Request>::Log(const param_type& p,
                                                    std::string* l) {
  l->append("(");
  LogParam(p.method, l);
  l->append(", ");
  LogParam(p.url, l);
  l->append(", ");
  LogParam(p.referrer, l);
  l->append(", ");
  LogParam(p.frame_origin, l);
  l->append(", ");
  LogParam(p.main_frame_origin, l);
  l->append(", ");
  LogParam(p.load_flags, l);
  l->append(", ");
  LogParam(p.origin_child_id, l);
  l->append(", ");
  LogParam(p.resource_type, l);
  l->append(", ");
  LogParam(p.request_context, l);
  l->append(", ");
  LogParam(p.appcache_host_id, l);
  l->append(", ");
  LogParam(p.download_to_file, l);
  l->append(", ");
  LogParam(p.host_renderer_id, l);
  l->append(", ");
  LogParam(p.host_render_view_id, l);
  l->append(")");
}

void ParamTraits<ViewMsg_Print_Params>::Write(Message* m, const param_type& p) {
  WriteParam(m, p.page_size);
  WriteParam(m, p.printable_size);
  WriteParam(m, p.margin_top);
  WriteParam(m, p.margin_left);
  WriteParam(m, p.dpi);
  WriteParam(m, p.min_shrink);
  WriteParam(m, p.max_shrink);
  WriteParam(m, p.desired_dpi);
  WriteParam(m, p.document_cookie);
  WriteParam(m, p.selection_only);
}

bool ParamTraits<ViewMsg_Print_Params>::Read(const Message* m,
                                             void** iter,
                                             param_type* p) {
  return ReadParam(m, iter, &p->page_size) &&
      ReadParam(m, iter, &p->printable_size) &&
      ReadParam(m, iter, &p->margin_top) &&
      ReadParam(m, iter, &p->margin_left) &&
      ReadParam(m, iter, &p->dpi) &&
      ReadParam(m, iter, &p->min_shrink) &&
      ReadParam(m, iter, &p->max_shrink) &&
      ReadParam(m, iter, &p->desired_dpi) &&
      ReadParam(m, iter, &p->document_cookie) &&
      ReadParam(m, iter, &p->selection_only);
}

void ParamTraits<ViewMsg_Print_Params>::Log(const param_type& p,
                                            std::string* l) {
  l->append("<ViewMsg_Print_Params>");
}

void ParamTraits<ViewMsg_PrintPage_Params>::Write(Message* m,
                                                  const param_type& p) {
  WriteParam(m, p.params);
  WriteParam(m, p.page_number);
}

bool ParamTraits<ViewMsg_PrintPage_Params>::Read(const Message* m,
                                                 void** iter,
                                                 param_type* p) {
  return ReadParam(m, iter, &p->params) &&
      ReadParam(m, iter, &p->page_number);
}

void ParamTraits<ViewMsg_PrintPage_Params>::Log(const param_type& p,
                                                std::string* l) {
  l->append("<ViewMsg_PrintPage_Params>");
}

void ParamTraits<ViewMsg_PrintPages_Params>::Write(Message* m,
                                                   const param_type& p) {
  WriteParam(m, p.params);
  WriteParam(m, p.pages);
}

bool ParamTraits<ViewMsg_PrintPages_Params>::Read(const Message* m,
                                                  void** iter,
                                                  param_type* p) {
  return ReadParam(m, iter, &p->params) &&
      ReadParam(m, iter, &p->pages);
}

void ParamTraits<ViewMsg_PrintPages_Params>::Log(const param_type& p,
                                                 std::string* l) {
  l->append("<ViewMsg_PrintPages_Params>");
}

void ParamTraits<ViewHostMsg_DidPrintPage_Params>::Write(Message* m,
                                                         const param_type& p) {
  WriteParam(m, p.metafile_data_handle);
  WriteParam(m, p.data_size);
  WriteParam(m, p.document_cookie);
  WriteParam(m, p.page_number);
  WriteParam(m, p.actual_shrink);
  WriteParam(m, p.page_size);
  WriteParam(m, p.content_area);
  WriteParam(m, p.has_visible_overlays);
}

bool ParamTraits<ViewHostMsg_DidPrintPage_Params>::Read(const Message* m,
                                                        void** iter,
                                                        param_type* p) {
  return ReadParam(m, iter, &p->metafile_data_handle) &&
      ReadParam(m, iter, &p->data_size) &&
      ReadParam(m, iter, &p->document_cookie) &&
      ReadParam(m, iter, &p->page_number) &&
      ReadParam(m, iter, &p->actual_shrink) &&
      ReadParam(m, iter, &p->page_size) &&
      ReadParam(m, iter, &p->content_area) &&
      ReadParam(m, iter, &p->has_visible_overlays);
}

void ParamTraits<ViewHostMsg_DidPrintPage_Params>::Log(const param_type& p,
                                                       std::string* l) {
  l->append("<ViewHostMsg_DidPrintPage_Params>");
}

void ParamTraits<ViewHostMsg_Audio_CreateStream_Params>::Write(
    Message* m,
    const param_type& p) {
  WriteParam(m, p.params.format);
  WriteParam(m, p.params.channels);
  WriteParam(m, p.params.sample_rate);
  WriteParam(m, p.params.bits_per_sample);
  WriteParam(m, p.packet_size);
}

bool ParamTraits<ViewHostMsg_Audio_CreateStream_Params>::Read(const Message* m,
                                                              void** iter,
                                                              param_type* p) {
  return
      ReadParam(m, iter, &p->params.format) &&
      ReadParam(m, iter, &p->params.channels) &&
      ReadParam(m, iter, &p->params.sample_rate) &&
      ReadParam(m, iter, &p->params.bits_per_sample) &&
      ReadParam(m, iter, &p->packet_size);
}

void ParamTraits<ViewHostMsg_Audio_CreateStream_Params>::Log(
    const param_type& p,
    std::string* l) {
  l->append("<ViewHostMsg_Audio_CreateStream_Params>(");
  LogParam(p.params.format, l);
  l->append(", ");
  LogParam(p.params.channels, l);
  l->append(", ");
  LogParam(p.params.sample_rate, l);
  l->append(", ");
  LogParam(p.params.bits_per_sample, l);
  l->append(", ");
  LogParam(p.packet_size, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_OnJavascriptCallback_Params>::Write(Message* m,
																 const param_type& p) {
  WriteParam(m, p.m_FunctionName);
  WriteParam(m, p.m_Arg);
  WriteParam(m, p.m_IpcChannel);
}

bool ParamTraits<ViewHostMsg_OnJavascriptCallback_Params>::Read(const Message* m,
																void** iter,
																param_type* p) {
  return
      ReadParam(m, iter, &p->m_FunctionName) &&
      ReadParam(m, iter, &p->m_Arg) &&
	  ReadParam(m, iter, &p->m_IpcChannel);
}

void ParamTraits<ViewHostMsg_OnJavascriptCallback_Params>::Log(const param_type& p,
															   std::string* l) {
  l->append("(");
  LogParam(p.m_FunctionName, l);
  l->append(", ");
  LogParam(p.m_Arg, l);
  l->append(", ");
  LogParam(p.m_IpcChannel, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_OnJavascriptCallback_ReturnVal>::Write(Message* m,
																 const param_type& p) {
  WriteParam(m, p.m_FunctionName);
  WriteParam(m, p.m_Arg);
}

bool ParamTraits<ViewHostMsg_OnJavascriptCallback_ReturnVal>::Read(const Message* m,
																void** iter,
																param_type* p) {
  return
      ReadParam(m, iter, &p->m_FunctionName) &&
      ReadParam(m, iter, &p->m_Arg);
}

void ParamTraits<ViewHostMsg_OnJavascriptCallback_ReturnVal>::Log(const param_type& p,
															   std::string* l) {
  l->append("(");
  LogParam(p.m_FunctionName, l);
  l->append(", ");
  LogParam(p.m_Arg, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_ScriptedPrint_Params>::Write(Message* m,
                                                          const param_type& p) {
  WriteParam(m, p.routing_id);
  WriteParam(m, p.host_window_id);
  WriteParam(m, p.cookie);
  WriteParam(m, p.expected_pages_count);
  WriteParam(m, p.has_selection);
  WriteParam(m, p.use_overlays);
}

bool ParamTraits<ViewHostMsg_ScriptedPrint_Params>::Read(const Message* m,
                                                         void** iter,
                                                         param_type* p) {
  return
      ReadParam(m, iter, &p->routing_id) &&
      ReadParam(m, iter, &p->host_window_id) &&
      ReadParam(m, iter, &p->cookie) &&
      ReadParam(m, iter, &p->expected_pages_count) &&
      ReadParam(m, iter, &p->has_selection) &&
      ReadParam(m, iter, &p->use_overlays);
}

void ParamTraits<ViewHostMsg_ScriptedPrint_Params>::Log(const param_type& p,
                                                        std::string* l) {
  l->append("(");
  LogParam(p.routing_id, l);
  l->append(", ");
  LogParam(p.host_window_id, l);
  l->append(", ");
  LogParam(p.cookie, l);
  l->append(", ");
  LogParam(p.expected_pages_count, l);
  l->append(", ");
  LogParam(p.has_selection, l);
  l->append(",");
  LogParam(p.use_overlays, l);
  l->append(")");
}

void ParamTraits<ViewMsg_DOMStorageEvent_Params>::Write(Message* m,
                                                        const param_type& p) {
  WriteParam(m, p.key_);
  WriteParam(m, p.old_value_);
  WriteParam(m, p.new_value_);
  WriteParam(m, p.origin_);
  WriteParam(m, p.url_);
  WriteParam(m, p.storage_type_);
}

bool ParamTraits<ViewMsg_DOMStorageEvent_Params>::Read(const Message* m,
                                                       void** iter,
                                                       param_type* p) {
  return
      ReadParam(m, iter, &p->key_) &&
      ReadParam(m, iter, &p->old_value_) &&
      ReadParam(m, iter, &p->new_value_) &&
      ReadParam(m, iter, &p->origin_) &&
      ReadParam(m, iter, &p->url_) &&
      ReadParam(m, iter, &p->storage_type_);
}

void ParamTraits<ViewMsg_DOMStorageEvent_Params>::Log(const param_type& p,
                                                      std::string* l) {
  l->append("(");
  LogParam(p.key_, l);
  l->append(", ");
  LogParam(p.old_value_, l);
  l->append(", ");
  LogParam(p.new_value_, l);
  l->append(", ");
  LogParam(p.origin_, l);
  l->append(", ");
  LogParam(p.url_, l);
  l->append(", ");
  LogParam(p.storage_type_, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_IDBFactoryOpen_Params>::Write(
    Message* m,
    const param_type& p) {
  WriteParam(m, p.routing_id_);
  WriteParam(m, p.response_id_);
  WriteParam(m, p.origin_);
  WriteParam(m, p.name_);
  WriteParam(m, p.description_);
  WriteParam(m, p.maximum_size_);
}

bool ParamTraits<ViewHostMsg_IDBFactoryOpen_Params>::Read(const Message* m,
                                                          void** iter,
                                                          param_type* p) {
  return
      ReadParam(m, iter, &p->routing_id_) &&
      ReadParam(m, iter, &p->response_id_) &&
      ReadParam(m, iter, &p->origin_) &&
      ReadParam(m, iter, &p->name_) &&
      ReadParam(m, iter, &p->description_) &&
      ReadParam(m, iter, &p->maximum_size_);
}

void ParamTraits<ViewHostMsg_IDBFactoryOpen_Params>::Log(const param_type& p,
                                                         std::string* l) {
  l->append("(");
  LogParam(p.routing_id_, l);
  l->append(", ");
  LogParam(p.response_id_, l);
  l->append(", ");
  LogParam(p.origin_, l);
  l->append(", ");
  LogParam(p.name_, l);
  l->append(", ");
  LogParam(p.description_, l);
  l->append(", ");
  LogParam(p.maximum_size_, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_IDBDatabaseCreateObjectStore_Params>::Write(
    Message* m,
    const param_type& p) {
  WriteParam(m, p.name_);
  WriteParam(m, p.key_path_);
  WriteParam(m, p.auto_increment_);
  WriteParam(m, p.transaction_id_);
  WriteParam(m, p.idb_database_id_);
}

bool ParamTraits<ViewHostMsg_IDBDatabaseCreateObjectStore_Params>::Read(
    const Message* m,
    void** iter,
    param_type* p) {
  return
      ReadParam(m, iter, &p->name_) &&
      ReadParam(m, iter, &p->key_path_) &&
      ReadParam(m, iter, &p->auto_increment_) &&
      ReadParam(m, iter, &p->transaction_id_) &&
      ReadParam(m, iter, &p->idb_database_id_);
}

void ParamTraits<ViewHostMsg_IDBDatabaseCreateObjectStore_Params>::Log(
    const param_type& p,
    std::string* l) {
  l->append("(");
  LogParam(p.name_, l);
  l->append(", ");
  LogParam(p.key_path_, l);
  l->append(", ");
  LogParam(p.auto_increment_, l);
  l->append(", ");
  LogParam(p.transaction_id_, l);
  l->append(", ");
  LogParam(p.idb_database_id_, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_IDBIndexOpenCursor_Params>::Write(
    Message* m,
    const param_type& p) {
  WriteParam(m, p.response_id_);
  WriteParam(m, p.left_key_);
  WriteParam(m, p.right_key_);
  WriteParam(m, p.key_flags_);
  WriteParam(m, p.direction_);
  WriteParam(m, p.idb_index_id_);
  WriteParam(m, p.transaction_id_);
}

bool ParamTraits<ViewHostMsg_IDBIndexOpenCursor_Params>::Read(
    const Message* m,
    void** iter,
    param_type* p) {
  return
      ReadParam(m, iter, &p->response_id_) &&
      ReadParam(m, iter, &p->left_key_) &&
      ReadParam(m, iter, &p->right_key_) &&
      ReadParam(m, iter, &p->key_flags_) &&
      ReadParam(m, iter, &p->direction_) &&
      ReadParam(m, iter, &p->idb_index_id_) &&
      ReadParam(m, iter, &p->transaction_id_);
}

void ParamTraits<ViewHostMsg_IDBIndexOpenCursor_Params>::Log(
    const param_type& p,
    std::string* l) {
  l->append("(");
  LogParam(p.response_id_, l);
  l->append(", ");
  LogParam(p.left_key_, l);
  l->append(", ");
  LogParam(p.right_key_, l);
  l->append(", ");
  LogParam(p.key_flags_, l);
  l->append(", ");
  LogParam(p.direction_, l);
  l->append(", ");
  LogParam(p.idb_index_id_, l);
  l->append(",");
  LogParam(p.transaction_id_, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_IDBObjectStorePut_Params>::Write(
    Message* m,
    const param_type& p) {
  WriteParam(m, p.idb_object_store_id_);
  WriteParam(m, p.response_id_);
  WriteParam(m, p.serialized_value_);
  WriteParam(m, p.key_);
  WriteParam(m, p.add_only_);
  WriteParam(m, p.transaction_id_);
}

bool ParamTraits<ViewHostMsg_IDBObjectStorePut_Params>::Read(
    const Message* m,
    void** iter,
    param_type* p) {
  return
      ReadParam(m, iter, &p->idb_object_store_id_) &&
      ReadParam(m, iter, &p->response_id_) &&
      ReadParam(m, iter, &p->serialized_value_) &&
      ReadParam(m, iter, &p->key_) &&
      ReadParam(m, iter, &p->add_only_) &&
      ReadParam(m, iter, &p->transaction_id_);
}

void ParamTraits<ViewHostMsg_IDBObjectStorePut_Params>::Log(
    const param_type& p,
    std::string* l) {
  l->append("(");
  LogParam(p.idb_object_store_id_, l);
  l->append(", ");
  LogParam(p.response_id_, l);
  l->append(", ");
  LogParam(p.serialized_value_, l);
  l->append(", ");
  LogParam(p.key_, l);
  l->append(", ");
  LogParam(p.add_only_, l);
  l->append(", ");
  LogParam(p.transaction_id_, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_IDBObjectStoreCreateIndex_Params>::Write(
    Message* m,
    const param_type& p) {
  WriteParam(m, p.name_);
  WriteParam(m, p.key_path_);
  WriteParam(m, p.unique_);
  WriteParam(m, p.transaction_id_);
  WriteParam(m, p.idb_object_store_id_);
}

bool ParamTraits<ViewHostMsg_IDBObjectStoreCreateIndex_Params>::Read(
    const Message* m,
    void** iter,
    param_type* p) {
  return
      ReadParam(m, iter, &p->name_) &&
      ReadParam(m, iter, &p->key_path_) &&
      ReadParam(m, iter, &p->unique_) &&
      ReadParam(m, iter, &p->transaction_id_) &&
      ReadParam(m, iter, &p->idb_object_store_id_);
}

void ParamTraits<ViewHostMsg_IDBObjectStoreCreateIndex_Params>::Log(
    const param_type& p,
    std::string* l) {
  l->append("(");
  LogParam(p.name_, l);
  l->append(", ");
  LogParam(p.key_path_, l);
  l->append(", ");
  LogParam(p.unique_, l);
  l->append(", ");
  LogParam(p.transaction_id_, l);
  l->append(", ");
  LogParam(p.idb_object_store_id_, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_IDBObjectStoreOpenCursor_Params>::Write(
    Message* m,
    const param_type& p) {
  WriteParam(m, p.response_id_);
  WriteParam(m, p.left_key_);
  WriteParam(m, p.right_key_);
  WriteParam(m, p.flags_);
  WriteParam(m, p.direction_);
  WriteParam(m, p.idb_object_store_id_);
  WriteParam(m, p.transaction_id_);
}

bool ParamTraits<ViewHostMsg_IDBObjectStoreOpenCursor_Params>::Read(
    const Message* m,
    void** iter,
    param_type* p) {
  return
      ReadParam(m, iter, &p->response_id_) &&
      ReadParam(m, iter, &p->left_key_) &&
      ReadParam(m, iter, &p->right_key_) &&
      ReadParam(m, iter, &p->flags_) &&
      ReadParam(m, iter, &p->direction_) &&
      ReadParam(m, iter, &p->idb_object_store_id_) &&
      ReadParam(m, iter, &p->transaction_id_);
}

void ParamTraits<ViewHostMsg_IDBObjectStoreOpenCursor_Params>::Log(
    const param_type& p,
    std::string* l) {
  l->append("(");
  LogParam(p.response_id_, l);
  l->append(", ");
  LogParam(p.left_key_, l);
  l->append(", ");
  LogParam(p.right_key_, l);
  l->append(", ");
  LogParam(p.flags_, l);
  l->append(", ");
  LogParam(p.direction_, l);
  l->append(", ");
  LogParam(p.idb_object_store_id_, l);
  l->append(",");
  LogParam(p.transaction_id_, l);
  l->append(")");
}

void ParamTraits<ViewMsg_ExecuteCode_Params>::Write(Message* m,
                                                    const param_type& p) {
  WriteParam(m, p.request_id);
  WriteParam(m, p.extension_id);
  WriteParam(m, p.is_javascript);
  WriteParam(m, p.code);
  WriteParam(m, p.all_frames);
}

bool ParamTraits<ViewMsg_ExecuteCode_Params>::Read(const Message* m,
                                                   void** iter,
                                                   param_type* p) {
  return
      ReadParam(m, iter, &p->request_id) &&
      ReadParam(m, iter, &p->extension_id) &&
      ReadParam(m, iter, &p->is_javascript) &&
      ReadParam(m, iter, &p->code) &&
      ReadParam(m, iter, &p->all_frames);
}

void ParamTraits<ViewMsg_ExecuteCode_Params>::Log(const param_type& p,
                                                  std::string* l) {
  l->append("<ViewMsg_ExecuteCode_Params>");
}

void ParamTraits<ViewHostMsg_CreateWorker_Params>::Write(Message* m,
                                                         const param_type& p) {
  WriteParam(m, p.url);
  WriteParam(m, p.is_shared);
  WriteParam(m, p.name);
  WriteParam(m, p.document_id);
  WriteParam(m, p.render_view_route_id);
  WriteParam(m, p.route_id);
  WriteParam(m, p.parent_appcache_host_id);
  WriteParam(m, p.script_resource_appcache_id);
}

bool ParamTraits<ViewHostMsg_CreateWorker_Params>::Read(const Message* m,
                                                        void** iter,
                                                        param_type* p) {
  return
      ReadParam(m, iter, &p->url) &&
      ReadParam(m, iter, &p->is_shared) &&
      ReadParam(m, iter, &p->name) &&
      ReadParam(m, iter, &p->document_id) &&
      ReadParam(m, iter, &p->render_view_route_id) &&
      ReadParam(m, iter, &p->route_id) &&
      ReadParam(m, iter, &p->parent_appcache_host_id) &&
      ReadParam(m, iter, &p->script_resource_appcache_id);
}

void ParamTraits<ViewHostMsg_CreateWorker_Params>::Log(const param_type& p,
                                                       std::string* l) {
  l->append("(");
  LogParam(p.url, l);
  l->append(", ");
  LogParam(p.is_shared, l);
  l->append(", ");
  LogParam(p.name, l);
  l->append(", ");
  LogParam(p.document_id, l);
  l->append(", ");
  LogParam(p.render_view_route_id, l);
  l->append(",");
  LogParam(p.route_id, l);
  l->append(", ");
  LogParam(p.parent_appcache_host_id, l);
  l->append(",");
  LogParam(p.script_resource_appcache_id, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_ShowNotification_Params>::Write(
    Message* m,
    const param_type& p) {
  WriteParam(m, p.origin);
  WriteParam(m, p.is_html);
  WriteParam(m, p.contents_url);
  WriteParam(m, p.icon_url);
  WriteParam(m, p.title);
  WriteParam(m, p.body);
  WriteParam(m, p.direction);
  WriteParam(m, p.replace_id);
  WriteParam(m, p.notification_id);
}

bool ParamTraits<ViewHostMsg_ShowNotification_Params>::Read(
    const Message* m,
    void** iter,
    param_type* p) {
  return
      ReadParam(m, iter, &p->origin) &&
      ReadParam(m, iter, &p->is_html) &&
      ReadParam(m, iter, &p->contents_url) &&
      ReadParam(m, iter, &p->icon_url) &&
      ReadParam(m, iter, &p->title) &&
      ReadParam(m, iter, &p->body) &&
      ReadParam(m, iter, &p->direction) &&
      ReadParam(m, iter, &p->replace_id) &&
      ReadParam(m, iter, &p->notification_id);
}

void ParamTraits<ViewHostMsg_ShowNotification_Params>::Log(
    const param_type &p,
    std::string* l) {
  l->append("(");
  LogParam(p.origin, l);
  l->append(", ");
  LogParam(p.is_html, l);
  l->append(", ");
  LogParam(p.contents_url, l);
  l->append(", ");
  LogParam(p.icon_url, l);
  l->append(", ");
  LogParam(p.title, l);
  l->append(",");
  LogParam(p.body, l);
  l->append(",");
  LogParam(p.direction, l);
  l->append(",");
  LogParam(p.replace_id, l);
  l->append(",");
  LogParam(p.notification_id, l);
  l->append(")");
}

void ParamTraits<ViewMsg_New_Params>::Write(Message* m, const param_type& p) {
  WriteParam(m, p.parent_window);
  WriteParam(m, p.renderer_preferences);
  WriteParam(m, p.web_preferences);
  WriteParam(m, p.view_id);
  WriteParam(m, p.session_storage_namespace_id);
  WriteParam(m, p.frame_name);
}

bool ParamTraits<ViewMsg_New_Params>::Read(const Message* m,
                                           void** iter,
                                           param_type* p) {
  return
      ReadParam(m, iter, &p->parent_window) &&
      ReadParam(m, iter, &p->renderer_preferences) &&
      ReadParam(m, iter, &p->web_preferences) &&
      ReadParam(m, iter, &p->view_id) &&
      ReadParam(m, iter, &p->session_storage_namespace_id) &&
      ReadParam(m, iter, &p->frame_name);
}

void ParamTraits<ViewMsg_New_Params>::Log(const param_type& p, std::string* l) {
  l->append("(");
  LogParam(p.parent_window, l);
  l->append(", ");
  LogParam(p.renderer_preferences, l);
  l->append(", ");
  LogParam(p.web_preferences, l);
  l->append(", ");
  LogParam(p.view_id, l);
  l->append(", ");
  LogParam(p.session_storage_namespace_id, l);
  l->append(", ");
  LogParam(p.frame_name, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_CreateWindow_Params>::Write(Message* m,
                                                         const param_type& p) {
  WriteParam(m, p.opener_id);
  WriteParam(m, p.user_gesture);
  WriteParam(m, p.window_container_type);
  WriteParam(m, p.session_storage_namespace_id);
  WriteParam(m, p.frame_name);
}

bool ParamTraits<ViewHostMsg_CreateWindow_Params>::Read(const Message* m,
                                                        void** iter,
                                                        param_type* p) {
  return
      ReadParam(m, iter, &p->opener_id) &&
      ReadParam(m, iter, &p->user_gesture) &&
      ReadParam(m, iter, &p->window_container_type) &&
      ReadParam(m, iter, &p->session_storage_namespace_id) &&
      ReadParam(m, iter, &p->frame_name);
}

void ParamTraits<ViewHostMsg_CreateWindow_Params>::Log(const param_type& p,
                                                       std::string* l) {
  l->append("(");
  LogParam(p.opener_id, l);
  l->append(", ");
  LogParam(p.user_gesture, l);
  l->append(", ");
  LogParam(p.window_container_type, l);
  l->append(", ");
  LogParam(p.session_storage_namespace_id, l);
  l->append(", ");
  LogParam(p.frame_name, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_RunFileChooser_Params>::Write(
    Message* m,
    const param_type& p) {
  WriteParam(m, static_cast<int>(p.mode));
  WriteParam(m, p.title);
  WriteParam(m, p.default_file_name);
  WriteParam(m, p.accept_types);
}

bool ParamTraits<ViewHostMsg_RunFileChooser_Params>::Read(
    const Message* m,
    void** iter,
    param_type* p) {
  int mode;
  if (!ReadParam(m, iter, &mode))
    return false;
  if (mode != param_type::Open &&
      mode != param_type::OpenMultiple &&
      mode != param_type::OpenFolder &&
      mode != param_type::Save)
    return false;
  p->mode = static_cast<param_type::Mode>(mode);
  return
      ReadParam(m, iter, &p->title) &&
      ReadParam(m, iter, &p->default_file_name) &&
      ReadParam(m, iter, &p->accept_types);
};

void ParamTraits<ViewHostMsg_RunFileChooser_Params>::Log(
    const param_type& p,
    std::string* l) {
  switch (p.mode) {
    case param_type::Open:
      l->append("(Open, ");
      break;
    case param_type::OpenMultiple:
      l->append("(OpenMultiple, ");
      break;
    case param_type::OpenFolder:
      l->append("(OpenFolder, ");
      break;
    case param_type::Save:
      l->append("(Save, ");
      break;
    default:
      l->append("(UNKNOWN, ");
  }
  LogParam(p.title, l);
  l->append(", ");
  LogParam(p.default_file_name, l);
  l->append(", ");
  LogParam(p.accept_types, l);
}

void ParamTraits<ViewMsg_ExtensionRendererInfo>::Write(Message* m,
                                                       const param_type& p) {
  WriteParam(m, p.id);
  WriteParam(m, p.web_extent);
  WriteParam(m, p.name);
  WriteParam(m, p.icon_url);
  WriteParam(m, p.location);
  WriteParam(m, p.allowed_to_execute_script_everywhere);
  WriteParam(m, p.host_permissions);
}

bool ParamTraits<ViewMsg_ExtensionRendererInfo>::Read(const Message* m,
                                                      void** iter,
                                                      param_type* p) {
  return ReadParam(m, iter, &p->id) &&
      ReadParam(m, iter, &p->web_extent) &&
      ReadParam(m, iter, &p->name) &&
      ReadParam(m, iter, &p->icon_url) &&
      ReadParam(m, iter, &p->location) &&
      ReadParam(m, iter, &p->allowed_to_execute_script_everywhere) &&
      ReadParam(m, iter, &p->host_permissions);
}

void ParamTraits<ViewMsg_ExtensionRendererInfo>::Log(const param_type& p,
                                                     std::string* l) {
  LogParam(p.id, l);
}

void ParamTraits<ViewMsg_ExtensionsUpdated_Params>::Write(
    Message* m,
    const param_type& p) {
  WriteParam(m, p.extensions);
}

bool ParamTraits<ViewMsg_ExtensionsUpdated_Params>::Read(
    const Message* m,
    void** iter,
    param_type* p) {
  return ReadParam(m, iter, &p->extensions);
}

void ParamTraits<ViewMsg_ExtensionsUpdated_Params>::Log(
    const param_type& p,
    std::string* l) {
  LogParam(p.extensions, l);
}

void ParamTraits<ViewMsg_DeviceOrientationUpdated_Params>::Write(
    Message* m,
    const param_type& p) {
  WriteParam(m, p.can_provide_alpha);
  WriteParam(m, p.alpha);
  WriteParam(m, p.can_provide_beta);
  WriteParam(m, p.beta);
  WriteParam(m, p.can_provide_gamma);
  WriteParam(m, p.gamma);
}

bool ParamTraits<ViewMsg_DeviceOrientationUpdated_Params>::Read(
    const Message* m,
    void** iter,
    param_type* p) {
  return
      ReadParam(m, iter, &p->can_provide_alpha) &&
      ReadParam(m, iter, &p->alpha) &&
      ReadParam(m, iter, &p->can_provide_beta) &&
      ReadParam(m, iter, &p->beta) &&
      ReadParam(m, iter, &p->can_provide_gamma) &&
      ReadParam(m, iter, &p->gamma);
}

void ParamTraits<ViewMsg_DeviceOrientationUpdated_Params>::Log(
    const param_type& p,
    std::string* l) {
  l->append("(");
  LogParam(p.can_provide_alpha, l);
  l->append(", ");
  LogParam(p.alpha, l);
  l->append(", ");
  LogParam(p.can_provide_beta, l);
  l->append(", ");
  LogParam(p.beta, l);
  l->append(", ");
  LogParam(p.can_provide_gamma, l);
  l->append(", ");
  LogParam(p.gamma, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_DomMessage_Params>::Write(Message* m,
                                                       const param_type& p) {
  WriteParam(m, p.name);
  WriteParam(m, p.arguments);
  WriteParam(m, p.source_url);
  WriteParam(m, p.request_id);
  WriteParam(m, p.has_callback);
  WriteParam(m, p.user_gesture);
}

bool ParamTraits<ViewHostMsg_DomMessage_Params>::Read(const Message* m,
                                                      void** iter,
                                                      param_type* p) {
  return
      ReadParam(m, iter, &p->name) &&
      ReadParam(m, iter, &p->arguments) &&
      ReadParam(m, iter, &p->source_url) &&
      ReadParam(m, iter, &p->request_id) &&
      ReadParam(m, iter, &p->has_callback) &&
      ReadParam(m, iter, &p->user_gesture);
}

void ParamTraits<ViewHostMsg_DomMessage_Params>::Log(const param_type& p,
                                                     std::string* l) {
  l->append("(");
  LogParam(p.name, l);
  l->append(", ");
  LogParam(p.arguments, l);
  l->append(", ");
  LogParam(p.source_url, l);
  l->append(", ");
  LogParam(p.request_id, l);
  l->append(", ");
  LogParam(p.has_callback, l);
  l->append(", ");
  LogParam(p.user_gesture, l);
  l->append(")");
}

void ParamTraits<ipcbase::file_util_proxy::Entry>::Write(
    Message* m,
    const param_type& p) {
  WriteParam(m, p.name);
  WriteParam(m, p.is_directory);
}

bool ParamTraits<ipcbase::file_util_proxy::Entry>::Read(
    const Message* m,
    void** iter,
    param_type* p) {
  return
      ReadParam(m, iter, &p->name) &&
      ReadParam(m, iter, &p->is_directory);
}

void ParamTraits<ipcbase::file_util_proxy::Entry>::Log(
    const param_type& p,
    std::string* l) {
  l->append("(");
  LogParam(p.name, l);
  l->append(", ");
  LogParam(p.is_directory, l);
  l->append(")");
}

void ParamTraits<ViewHostMsg_AccessibilityNotification_Params>::Write(
    Message* m,
    const param_type& p) {
  WriteParam(m, p.notification_type);
  WriteParam(m, p.acc_obj);
}

bool ParamTraits<ViewHostMsg_AccessibilityNotification_Params>::Read(
    const Message* m,
    void** iter,
    param_type* p) {
  return
      ReadParam(m, iter, &p->notification_type) &&
      ReadParam(m, iter, &p->acc_obj);
}

void ParamTraits<ViewHostMsg_AccessibilityNotification_Params>::Log(
    const param_type& p,
    std::string* l) {
  l->append("(");
  LogParam(p.notification_type, l);
  l->append(", ");
  LogParam(p.acc_obj, l);
  l->append(")");
}

}  // namespace IPC

#endif

ViewHostMsg_OnJavascriptCallback_Params::ViewHostMsg_OnJavascriptCallback_Params()
{

}

ViewHostMsg_OnJavascriptCallback_Params::~ViewHostMsg_OnJavascriptCallback_Params()
{

}

ViewHostMsg_OnJavascriptCallback_ReturnVal::ViewHostMsg_OnJavascriptCallback_ReturnVal()
{

}

ViewHostMsg_OnJavascriptCallback_ReturnVal::~ViewHostMsg_OnJavascriptCallback_ReturnVal()
{

}

ViewMsg_Tile_Params::ViewMsg_Tile_Params()
{

}

ViewMsg_Tile_Params::~ViewMsg_Tile_Params()
{

}

ViewMsg_Rect_Params::ViewMsg_Rect_Params()
	: x(0), y(0), width(0), height(0)
{

}

ViewMsg_Rect_Params::~ViewMsg_Rect_Params()
{

}

ViewMsg_CreateWindow_Params::ViewMsg_CreateWindow_Params()
	: hWnd(0)
	, hRootParent(0)
	, scale(0)
	, resizeEdges(0)
	, borderWidth(0)
	, captionHeight(0)
	, sizeGripSize(0)
	, windowType(0)
	, minWidth(0)
	, minHeight(0)
	, maxWidth(0)
	, maxHeight(0)
	, hideOnClose(false)
{

}

ViewMsg_CreateWindow_Params::~ViewMsg_CreateWindow_Params()
{

}

namespace IPC
{
	void ParamTraits<ViewHostMsg_OnJavascriptCallback_Params>::Write(Message* m, const param_type& p)
	{
		WriteParam(m, p.m_FunctionName);
		WriteParam(m, p.m_Arg);
		WriteParam(m, p.m_IpcChannel);
	}

	bool ParamTraits<ViewHostMsg_OnJavascriptCallback_Params>::Read(const Message* m, void** iter, param_type* p)
	{
	  return 
 		  ReadParam(m, iter, &p->m_FunctionName) &&
 		  ReadParam(m, iter, &p->m_Arg) &&
		  ReadParam(m, iter, &p->m_IpcChannel);
	}

	void ParamTraits<ViewHostMsg_OnJavascriptCallback_Params>::Log(const param_type& p, std::string* l)
	{
		l->append("(");
		LogParam(p.m_FunctionName, l);
		l->append(", ");
		LogParam(p.m_Arg, l);
		l->append(", ");
		LogParam(p.m_IpcChannel, l);
		l->append(")");
	}

	void ParamTraits<ViewHostMsg_OnJavascriptCallback_ReturnVal>::Write(Message* m, const param_type& p)
	{
		int type = (int)p.m_Type;
		WriteParam(m, type);

		if(p.m_Type == ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_STRING)
		{
			WriteParam(m, p.m_String);
		}
		else if(p.m_Type == ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_BOOL)
		{
			WriteParam(m, p.m_Bool);
		}
		else if(p.m_Type == ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_INT)
		{
			WriteParam(m, p.m_Int);
		}
		else if(p.m_Type == ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_DOUBLE)
		{
			WriteParam(m, p.m_Double);
		}
	}

	bool ParamTraits<ViewHostMsg_OnJavascriptCallback_ReturnVal>::Read(const Message* m, void** iter, param_type* p)
	{
		int type;
		bool success = ReadParam(m, iter, &type);
		p->m_Type = (ViewHostMsg_OnJavascriptCallback_ReturnVal::ValueType)type;

		if(success)
		{
			if(p->m_Type == ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_STRING)
			{
				success = ReadParam(m, iter, &p->m_String);
			}
			else if(p->m_Type == ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_BOOL)
			{
				success = ReadParam(m, iter, &p->m_Bool);
			}
			else if(p->m_Type == ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_INT)
			{
				success = ReadParam(m, iter, &p->m_Int);
			}
			else if(p->m_Type == ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_DOUBLE)
			{
				success = ReadParam(m, iter, &p->m_Double);
			}
		}

		return success;
	}

	void ParamTraits<ViewHostMsg_OnJavascriptCallback_ReturnVal>::Log(const param_type& p, std::string* l)
	{
		// TODO: NS
	}

	void ParamTraits<ViewMsg_Tile_Params>::Write(Message* m, const param_type& p)
	{
		WriteParam(m, p.tile_rects);
	}

	bool ParamTraits<ViewMsg_Tile_Params>::Read(const Message* m, void** iter, param_type* p)
	{
		return 
			ReadParam(m, iter, &p->tile_rects);
	}

	void ParamTraits<ViewMsg_Tile_Params>::Log(const param_type& p, std::string* l)
	{
		// TODO: NS
	}

	void ParamTraits<ViewMsg_Rect_Params>::Write(Message* m, const param_type& p)
	{
		WriteParam(m, p.x);
		WriteParam(m, p.y);
		WriteParam(m, p.width);
		WriteParam(m, p.height);
	}

	bool ParamTraits<ViewMsg_Rect_Params>::Read(const Message* m, void** iter, param_type* p)
	{
		return ReadParam(m, iter, &p->x) && 
			   ReadParam(m, iter, &p->y) && 
			   ReadParam(m, iter, &p->width) && 
			   ReadParam(m, iter, &p->height);
	}

	void ParamTraits<ViewMsg_Rect_Params>::Log(const param_type& p, std::string* l)
	{
		// TODO: NS
	}
	
	void ParamTraits<ViewMsg_CreateWindow_Params>::Write(Message* m, const param_type& p)
	{
		WriteParam(m, p.hWnd);
		WriteParam(m, p.hRootParent);
		WriteParam(m, p.scale);
		WriteParam(m, p.borderWidth);
		WriteParam(m, p.resizeEdges);
		WriteParam(m, p.captionHeight);
		WriteParam(m, p.sizeGripSize);
		WriteParam(m, p.windowType);
		WriteParam(m, p.minWidth);
		WriteParam(m, p.minHeight);
		WriteParam(m, p.maxWidth);
		WriteParam(m, p.maxHeight);
		WriteParam(m, p.hideOnClose);
		WriteParam(m, p.windowTitle);
	}

	bool ParamTraits<ViewMsg_CreateWindow_Params>::Read(const Message* m, void** iter, param_type* p)
	{
		return ReadParam(m, iter, &p->hWnd) &&
				ReadParam(m, iter, &p->hRootParent) &&
				ReadParam(m, iter, &p->scale) && 
				ReadParam(m, iter, &p->borderWidth) &&
				ReadParam(m, iter, &p->resizeEdges) &&
				ReadParam(m, iter, &p->captionHeight) &&
				ReadParam(m, iter, &p->sizeGripSize) &&
				ReadParam(m, iter, &p->windowType) &&
				ReadParam(m, iter, &p->minWidth) &&
				ReadParam(m, iter, &p->minHeight) &&
				ReadParam(m, iter, &p->maxWidth) &&
				ReadParam(m, iter, &p->maxHeight) &&
				ReadParam(m, iter, &p->hideOnClose) &&
				ReadParam(m, iter, &p->windowTitle);
	}

	void ParamTraits<ViewMsg_CreateWindow_Params>::Log(const param_type& p, std::string* l)
	{
		// TODO: NS
	}
}
