#ifndef RENDERER_DELEGATE_H
#define RENDERER_DELEGATE_H

#include "rgsc_common.h"
#include "stringutil.h"
#include "types.h"

#include <string>
#include <vector>
struct ViewHostMsg_OnJavascriptCallback_Params;
struct ViewHostMsg_OnJavascriptCallback_ReturnVal;

#pragma warning(push)
#pragma warning(disable: 4668)
#define _WINSOCKAPI_ //Prevent windows.h from including winsock.h
#include <windows.h>
#pragma warning(pop)

namespace IPC
{
	class Message;
}

namespace rgsc
{

#define LOAD_PAGE_TIMEOUT 20000

class TiledBuffer;

class RgscWindow
{
public:
    RgscWindow(unsigned int width, unsigned int height, float scale);
    ~RgscWindow();

    void clear();

	// SDK -> Subprocess IPC messages
	void sendSetTiledBuffer(TiledBuffer* tiledBuffer);
	void sendMouseMoved(__int64 msg, __int64 wParam, __int64 lParam);
	void sendMouseButton(__int64 msg, __int64 wParam, __int64 lParam);
	void sendMouseWheel(int xScroll, int yScroll);
	void sendKeyEvent(__int64 msg, __int64 wParam, __int64 lParam, __int64 scModifiers);
	void sendNavigateTo(const char *url, const char* offlineRoot = "");
	void sendAddBindOnStartLoading(RgscWideString functionName, bool synchronous);

	// Subprocess -> SDK IPC messages
	void onLoadingStateChangedIpc(bool isLoading);
	void onAddressBarChangedIpc(std::string newURL);
	void onProvisionalLoadErrorIpc(std::string url, int errorCode, std::wstring errorText, bool isMainFrame);
	void onConsoleMessageIpc(int logLevel, std::wstring message, std::wstring sourceId, int line_no);
	void onScriptAlertIpc(std::wstring message, std::wstring defaultValue, std::string url, int flags);
	void onTitleChangedIpc(std::wstring title);
	void onTooltipChangedIpc(std::wstring text);
	void onCreatedWindowIpc();
	void onCursorUpdatedIpc(rage::u64 hCursor, int cursorType);
	void onJavascriptCallback(const ViewHostMsg_OnJavascriptCallback_Params& params, ViewHostMsg_OnJavascriptCallback_ReturnVal* returnVal);
	void onJavascriptCallbackAsyncIpc(ViewHostMsg_OnJavascriptCallback_Params params);
	void onJavascriptCallbackSync(ViewHostMsg_OnJavascriptCallback_Params params, ViewHostMsg_OnJavascriptCallback_ReturnVal* returnVal);
	void onWindowCreatedIpc(rage::u64 browserHandle, rage::u64 parentWindowHandle);
	void onWindowSizeChangedIpc(int width, int height);
	void onWindowClosingIpc();
	void onRendererCrashedIpc();
	void onMouseMovedIpc(__int64 msg, __int64 wParam, __int64 lParam);
	void onKeyEventIpc(__int64 msg, __int64 wParam, __int64 lParam, __int64 scKeyMods);
	void onDebugRequestContentsIpc(std::string contents);
	void onDebugResponseContentsIpc(int status, std::string contents);
	void onCertficateErrorIpc(int error_code);
	void onUrlRequestedIpc(std::string url);
	void onWindowStateChangedIpc(int windowState);

	// Handling IPC messages - These functions are made virtual so that derived windows
	// can implement override behaviour.
	virtual void onAddressBarChanged(RgscUrlString newURL);
	virtual void onProvisionalLoadError(RgscUrlString url, int errorCode, RgscWideString errorText, bool isMainFrame);
	virtual void onConsoleMessage(int logLevel, RgscWideString message, RgscWideString sourceId, int line_no);
	virtual void onScriptAlert(RgscWideString message, RgscWideString defaultValue, RgscUrlString url, int flags, bool &success, RgscWideString &value);
	virtual void onLoadingStateChanged(bool isLoading);
	virtual void onTitleChanged(RgscWideString title);
	virtual void onTooltipChanged(RgscWideString text);
	virtual void onKeyEvent(s64 msg, s64 wParam, s64 lParam, s64 scKeyMods);

	// Message Send/Receive
	void Send(IPC::Message* message);
	void OnMessageReceived(const IPC::Message& message);

	// Cursors
	HCURSOR getCursor() {return m_Cursor;}
	RgscCursorType getCursorType() {return m_CursorType;}

	// Publicly Available API
	void ShowUi();
	void HideUi();
	bool IsUiVisible();
	bool TimedOut();
	void CallJavascript(RgscWideString javascript);
	void Resize(unsigned int width, unsigned int height, float scale);
	void AddCaptionExclusion(int x, int y, int width, int height, int referencePoint);
	void ClearCaptionExclusion();
	void DestroyWindow();

private:

	class TestForwardingObj
	{
		RgscWindow* m_Window;
		std::string m_IpcChannelName;
	public:
		TestForwardingObj(RgscWindow* window)
			: m_Window(window)
		{

		}

		void Send(IPC::Message* message);

		virtual void onJavascriptCallbackSync(ViewHostMsg_OnJavascriptCallback_Params params, ViewHostMsg_OnJavascriptCallback_ReturnVal* returnVal);
	};

	TestForwardingObj m_MyTestObj;

	unsigned int m_WindowId;
	unsigned int m_Width;
	unsigned int m_Height;
	float m_Scale;
    TiledBuffer* m_Buffer;
	bool m_RemoteWindowCreated;
	unsigned int m_LoadStartTime;
	bool m_Visible;
	bool m_ErrorOnLoad;
	HCURSOR m_Cursor;
	RgscCursorType m_CursorType;
};

} // namespace rgsc

#endif //RENDERER_DELEGATE_H
