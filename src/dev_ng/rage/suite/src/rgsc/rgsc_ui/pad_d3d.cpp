// 
// input/pad_d3d.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "pad.h"

#if RGSC_DINPUT_SUPPORT

#include "../../rgsc_ui/joystick.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <math.h>
#pragma warning(pop)

namespace rgsc
{

template <class Type> inline Type Clamp(Type t,Type a,Type b)
{
	return (t < a) ? a : (t > b) ? b : t;
}

template <class Type> inline Type Max(Type a,Type b)
{
	return a>b?a:b;
}

void ioD3DPad::Update()
{
	ClearInputs();
	m_IsConnected = false;

	if (ioJoystick::GetStickCount() > m_PadIndex)
	{
		const ioJoystick &S = ioJoystick::GetStick(m_PadIndex);

		// Do not handle input from unknown pads at this point
		if (S.GetJoyType() == ioJoystick::TYPE_UNKNOWN)
		{
			return;
		}

		m_IsConnected = true;

		/* Displayf("AXIS: %d %d %d %d %d %d %d %d BTN %x",
			S.GetAxis(0),S.GetAxis(1),S.GetAxis(2),S.GetAxis(3),
			S.GetAxis(4),S.GetAxis(5),S.GetAxis(6),S.GetAxis(7),S.GetButtons());  */

		m_Axis[0] = (u8) (S.GetAxis(0) >> 8);
		m_Axis[1] = (u8) (S.GetAxis(1) >> 8);
		if (S.GetAxisCount() >= 4)
		{
			if (S.GetJoyType()==ioJoystick::TYPE_DUAL_USB_FFJ_MP866)
			{
				m_Axis[2] = (u8) (S.GetAxis(6) >> 8);
				m_Axis[3] = (u8) (S.GetAxis(2) >> 8);
			}
			else if (S.GetJoyType()==ioJoystick::TYPE_SMARTJOYPAD || 
					    S.GetJoyType()==ioJoystick::TYPE_XKPC2002 || 
						S.GetJoyType()==ioJoystick::TYPE_DUALJOYPAD || 
						S.GetJoyType()==ioJoystick::TYPE_PSX_TO_USB_CONVERTER ||
						S.GetJoyType()==ioJoystick::TYPE_LOGITECH_DUAL)
			{
				m_Axis[2] = (u8) (S.GetAxis(2) >> 8);
				m_Axis[3] = (u8) (S.GetAxis(5) >> 8);
			}
			else if (S.GetJoyType()==ioJoystick::TYPE_KIKYXSERIES)
			{
				m_Axis[2] = (u8) (S.GetAxis(5) >> 8);
				m_Axis[3] = (u8) (S.GetAxis(2) >> 8);
			}
			else if (S.GetJoyType()==ioJoystick::TYPE_LOGITECH_CORDLESS)
			{
				m_Axis[2] = (u8) (S.GetAxis(5) >> 8);
				m_Axis[3] = (u8) (S.GetAxis(6) >> 8);
			}
			else if (S.GetJoyType()==ioJoystick::TYPE_XBCD_XBOX_USB)
			{
				m_Axis[2] = (u8) (S.GetAxis(3) >> 8);
				m_Axis[3] = (u8) (S.GetAxis(4) >> 8);
			}
			else if (S.GetJoyType()==ioJoystick::TYPE_TIGERGAME_XBOX_USB)
			{
				m_Axis[2] = (u8) (S.GetAxis(2) >> 8);
				m_Axis[3] = (u8) (S.GetAxis(5) >> 8);
			}
			else if (S.GetJoyType()==ioJoystick::TYPE_XNA_GAMEPAD_XENON)
			{
			#if 1 //WIN32
				m_Axis[2] = (u8) (S.GetAxis(3) >> 8);
				m_Axis[3] = (u8) (S.GetAxis(4) >> 8);
			#else
				m_Axis[2] = 0xff - (u8) (S.GetAxis(3) >> 8);
				m_Axis[3] = 0xff - (u8) (S.GetAxis(4) >> 8);
			#endif
			}
			else if (S.GetJoyType()==ioJoystick::TYPE_PS4_WIRELESS_PAD)
			{
				m_Axis[2] = (u8) (S.GetAxis(2) >> 8);
				m_Axis[3] = (u8) (S.GetAxis(5) >> 8);
			}
			else if (S.GetJoyType()==ioJoystick::TYPE_SAITEK_P480)
			{
				m_Axis[2] = (u8) (S.GetAxis(5) >> 8);
				m_Axis[3] = (u8) (S.GetAxis(2) >> 8);
			}
			else if (S.GetJoyType()==ioJoystick::TYPE_SAITEK_CYBORG)
			{
				m_Axis[2] = (u8) (S.GetAxis(3) >> 8);
				m_Axis[3] = (u8) (S.GetAxis(4) >> 8);
			}
			else if (S.GetJoyType()==ioJoystick::TYPE_LOGITECH_RUMBLEPAD_2)
			{
				m_Axis[2] = (u8) (S.GetAxis(2) >> 8);
				m_Axis[3] = (u8) (S.GetAxis(5) >> 8);
			}
			else
			{
				m_Axis[2] = (u8) (S.GetAxis(2) >> 8);
				m_Axis[3] = (u8) (S.GetAxis(3) >> 8);
			}
		}
		else if (S.GetPOVCount())
		{
			short dir = (short) S.GetPOV(0);
			m_Axis[2] = m_Axis[3] = 0x80;
			if (dir == 0)
			{
				m_Axis[3] = 0;
			}
			else if (dir == 9000)
			{
				m_Axis[2] = 0xff;
			}
			else if (dir == 18000)
			{
				m_Axis[3] = 0xff;
			}
			else if (dir == 27000)
			{
				m_Axis[2] = 0;
			}
		}

		// Map POV hat.
		if (S.GetJoyType()==ioJoystick::TYPE_XNA_GAMEPAD_XENON)
		{
			// Map POV to left dpad.
			short dir = (short) S.GetPOV(0);
			if (dir >= 0)
			{
				if (dir < 9000 || dir > 27000)
				{
					m_Buttons |= RGSC_LUP;
				}
				if (dir > 0 && dir < 18000)
				{
					m_Buttons |= RGSC_LRIGHT;
				}
				if (dir > 9000 && dir < 27000)
				{
					m_Buttons |= RGSC_LDOWN;
				}
				if (dir > 18000 && dir < 36000)
				{
					m_Buttons |= RGSC_LLEFT;
				}
			}
		}
		else if (S.GetPOVCount() && S.GetButtonCount()+4<32)
		{
			// This maps POV directly to m_Buttons, and
			// doesn't offer the chance to remap.
			short dir = (short) S.GetPOV(0);
			if (dir == 0)
			{
				m_Buttons |= (1 << (S.GetButtonCount()));
			}
			else if (dir == 4500)
			{
				m_Buttons |= (1 << (S.GetButtonCount()));
				m_Buttons |= (1 << (S.GetButtonCount()+1));
			}
			else if (dir == 9000)
			{
				m_Buttons |= (1 << (S.GetButtonCount()+1));
			}
			else if (dir == 13500)
			{
				m_Buttons |= (1 << (S.GetButtonCount()+1));
				m_Buttons |= (1 << (S.GetButtonCount()+2));
			}
			else if (dir == 18000)
			{
				m_Buttons |= (1 << (S.GetButtonCount()+2));
			}
			else if (dir == 22500)
			{
				m_Buttons |= (1 << (S.GetButtonCount()+2));
				m_Buttons |= (1 << (S.GetButtonCount()+3));
			}
			else if (dir == 27000)
			{
				m_Buttons |= (1 << (S.GetButtonCount()+3));
			}
			else if (dir == 31500)
			{
				m_Buttons |= (1 << (S.GetButtonCount()+3));
				m_Buttons |= (1 << (S.GetButtonCount()));
			}
		}

		if (S.GetJoyType()==ioJoystick::TYPE_DUALJOYPAD || 
			S.GetJoyType()==ioJoystick::TYPE_DUAL_USB_FFJ_MP866 ||
			S.GetJoyType()==ioJoystick::TYPE_LOGITECH_DUAL ||
			S.GetJoyType()==ioJoystick::TYPE_SAITEK_P480 ||
			S.GetJoyType()==ioJoystick::TYPE_SAITEK_CYBORG ||
			S.GetJoyType()==ioJoystick::TYPE_LOGITECH_RUMBLEPAD_2)
		{
			short dir = (short) S.GetPOV(0);
			if (dir != -1)
			{
				if (dir < 9000 || dir > 27000)
				{
					m_Buttons |= RGSC_LUP;
				}
				else if (dir > 9000 && dir < 27000)
				{
					m_Buttons |= RGSC_LDOWN;
				}
				if (dir > 0 && dir < 18000)
				{
					m_Buttons |= RGSC_LRIGHT;
				}
				else if (dir > 18000)
				{
					m_Buttons |= RGSC_LLEFT;
				}
			}
		}
		else if (S.GetJoyType()==ioJoystick::TYPE_TIGERGAME_XBOX_USB)
		{
			// digital
			if (S.GetAxis(7) > 0x7fff)
			{
				m_Buttons |= RGSC_L2;
				//m_AnalogButtons[analogL2] = S.GetAxis(7);
			}
			if (S.GetAxis(6) > 0x7fff)
			{
				m_Buttons |= RGSC_R2;
				//m_AnalogButtons[analogR2] = S.GetAxis(6);
			}
		}
		else if (S.GetJoyType()==ioJoystick::TYPE_XNA_GAMEPAD_XENON)
		{
			// digital
			if (S.GetAxis(2) > 0x7fff)
			{
				m_Buttons |= RGSC_L2;
			}
			if (S.GetAxis(2) < 0x7fff)
			{
				m_Buttons |= RGSC_R2;
			}
			// analog
			int value;
			// right
			value = 0x7fff - S.GetAxis(2);
			value = Max(value,0);
			m_AnalogButtons[RGSC_R2_INDEX] = (u8) (value >> 7);
			// left
			value = S.GetAxis(2) - 0x7fff;
			value = Clamp(value,0,0x7fff);
			m_AnalogButtons[RGSC_L2_INDEX] = (u8) (value >> 7);
		}
		else if (S.GetJoyType()==ioJoystick::TYPE_PS4_WIRELESS_PAD)
		{
			// TODO if necessary
			// extra handling DPad, TouchPad, etc.
		}
		else if (S.GetAxisCount() == 6)
		{	// last two axes are really the D-pad
			if (S.GetAxis(7) < 20000)
			{
				m_Buttons |= RGSC_LLEFT;
			}
			else if (S.GetAxis(7) > 40000)
			{
				m_Buttons |= RGSC_LRIGHT;
			}
			if (S.GetAxis(6) < 20000)
			{
				m_Buttons |= RGSC_LUP;
			}
			else if (S.GetAxis(6) > 40000)
			{
				m_Buttons |= RGSC_LDOWN;
			}	
		}

		int whichMap = (int) S.GetJoyType();

		// first map is direct pad, second is smart joypad:
		CompileTimeAssert(18==ioJoystick::NUM_TYPES);
		static unsigned map[ioJoystick::NUM_TYPES][16] =
		{ 
			// UNKNOWN
			{RGSC_RUP,	RGSC_RLEFT,	RGSC_RDOWN,	RGSC_RRIGHT,
			 RGSC_R1,		RGSC_R2,		RGSC_L1,		RGSC_L2,
			 RGSC_START,	RGSC_SELECT,	RGSC_R3,		RGSC_L3},

			// DIRECTPAD
			{RGSC_RUP,	RGSC_RLEFT,	RGSC_RDOWN,	RGSC_RRIGHT,
			 RGSC_R1,		RGSC_R2,		RGSC_L1,		RGSC_L2,
			 RGSC_START,	RGSC_SELECT,	RGSC_R3,		RGSC_L3},

			 // SMARTJOYPAD
			{RGSC_RUP,	RGSC_RRIGHT,	RGSC_RDOWN,	RGSC_RLEFT,
			 RGSC_L2,		RGSC_R2,		RGSC_L1,		RGSC_R1,
			 RGSC_SELECT,	RGSC_START,	RGSC_R3,		RGSC_L3,
			 RGSC_LUP,	RGSC_LRIGHT,	RGSC_LDOWN,	RGSC_LLEFT},

			 // TYPE_GAMEPADPRO
			{RGSC_RDOWN,	RGSC_RRIGHT,	RGSC_R2,		RGSC_RLEFT,
			RGSC_RUP,		RGSC_L2,		RGSC_L1,		RGSC_R1,
			RGSC_START,	0,				0,				0},

			// TYPE_XKPC2002
			{RGSC_RUP,	RGSC_RRIGHT,	RGSC_RDOWN,	RGSC_RLEFT,
			 RGSC_L2,		RGSC_R2,		RGSC_L1,		RGSC_R1,
			 RGSC_SELECT,	RGSC_L3,		RGSC_R3,		RGSC_START,
			 RGSC_LUP,	RGSC_LRIGHT,	RGSC_LDOWN,	RGSC_LLEFT},

			 // TYPE_DUALPAD
			{RGSC_RUP,	RGSC_RRIGHT,	RGSC_RDOWN,	RGSC_RLEFT,
			 RGSC_L2,		RGSC_R2,		RGSC_L1,		RGSC_R1,
			 RGSC_START,	RGSC_SELECT,	RGSC_L3,		RGSC_R3,
			 RGSC_LUP,	RGSC_LRIGHT,	RGSC_LDOWN,	RGSC_LLEFT},

			// TYPE_DUAL_USB_FFJ_MP866
			{RGSC_RUP,	RGSC_RRIGHT,	RGSC_RDOWN,	RGSC_RLEFT,
			 RGSC_L1,		RGSC_R1,		RGSC_L2,		RGSC_R2,
			 RGSC_START,	RGSC_SELECT,	RGSC_L3,		RGSC_R3,
			 RGSC_LUP,	RGSC_LRIGHT,	RGSC_LDOWN,	RGSC_LLEFT},

			// TYPE_KIKYXSERIES
			{RGSC_RUP,	RGSC_RRIGHT,	RGSC_RDOWN,	RGSC_RLEFT,
			 RGSC_L2,		RGSC_R2,		RGSC_L1,		RGSC_R1,
			 RGSC_START,	RGSC_SELECT,	RGSC_L3,		RGSC_R3,
			 RGSC_LUP,	RGSC_LRIGHT,	RGSC_LDOWN,	RGSC_LLEFT},

			// TYPE_LOGITECH_CORDLESS
			{RGSC_RDOWN,	RGSC_RLEFT,	RGSC_RUP,		RGSC_RRIGHT,
			RGSC_R1,		RGSC_R2,		RGSC_L1,		RGSC_L2,
			RGSC_START,	RGSC_SELECT,	RGSC_R3,		RGSC_L3,
			0,				0,				0,				0},

			// TYPE_PSX_TO_USB_CONVERTER
			{RGSC_RUP,	RGSC_RRIGHT,	RGSC_RDOWN,	RGSC_RLEFT,
			RGSC_L2,		RGSC_R2,		RGSC_L1,		RGSC_R1,
			RGSC_SELECT,	RGSC_START,	RGSC_L3,		RGSC_R3,
			RGSC_LUP,		RGSC_LRIGHT,	RGSC_LDOWN,	RGSC_LLEFT},

			// TYPE_XBCD_XBOX_USB
			{RGSC_RDOWN,	RGSC_RRIGHT,	RGSC_RLEFT,	RGSC_RUP,
			RGSC_R1,		RGSC_L1,		RGSC_START,	RGSC_SELECT,
			RGSC_L3,		RGSC_R3,		RGSC_L2,		RGSC_R2,
			RGSC_LUP,		RGSC_LRIGHT,	RGSC_LDOWN,	RGSC_LLEFT},

			// TYPE_TIGERGAME_XBOX_USB
			{RGSC_RDOWN,	RGSC_RRIGHT,	RGSC_RLEFT,	RGSC_RUP,
			RGSC_R1,		RGSC_L1,		RGSC_L3,		RGSC_R3,
			RGSC_START,	RGSC_SELECT,	RGSC_L2,		RGSC_R2,
			RGSC_LUP,		RGSC_LRIGHT,	RGSC_LDOWN,	RGSC_LLEFT},

			// TYPE_XNA_GAMEPAD_XENON
			{RGSC_RDOWN,	RGSC_RRIGHT,	RGSC_RLEFT,	RGSC_RUP,
			RGSC_L1,		RGSC_R1,		RGSC_SELECT,	RGSC_START,
			RGSC_L3,		RGSC_R3,		0,				0,
			0,				0,				0,				0},

			// TYPE_PS4_WIRELESS_PAD
			{RGSC_RLEFT /* square */,	RGSC_RDOWN /* x */, RGSC_RRIGHT /* circle */,	RGSC_RUP /* triangle */,	
			RGSC_L1,		RGSC_R1,		RGSC_L2 /* l1 */,		RGSC_R2 /* r3 */,
			RGSC_SELECT,	RGSC_START,	0,0,
			0,0,0,0},

			// TYPE_LOGITECH_DUAL
			{RGSC_RLEFT,	RGSC_RDOWN, RGSC_RRIGHT,	RGSC_RUP,	
			RGSC_L1,		RGSC_R1,		RGSC_L2,		RGSC_R2,
			RGSC_SELECT,	RGSC_START,	RGSC_L3,		RGSC_R3,
			0,0,0,0},

			// TYPE_SAITEK_P480
			{RGSC_RLEFT,	RGSC_RUP,		RGSC_RDOWN,	RGSC_RRIGHT,	
			RGSC_L1,		RGSC_L2,		RGSC_R1,		RGSC_R2,
			RGSC_SELECT,	RGSC_START,	RGSC_L3,		RGSC_R3,
			0,0,0,0},

			// TYPE_SAITEK_CYBORG
			{RGSC_RLEFT,	RGSC_RDOWN,	RGSC_RRIGHT,	RGSC_RUP,	
			RGSC_L1,		RGSC_R1,		RGSC_L2,		RGSC_R2,
			RGSC_SELECT,	RGSC_START,	RGSC_L3,		RGSC_R3,
			0,0,0,0},

			// TYPE_LOGITECH_RUMBLEPAD_2
			{RGSC_RLEFT,	RGSC_RDOWN,	RGSC_RRIGHT,	RGSC_RUP,	
			RGSC_L1,		RGSC_R1,		RGSC_L2,		RGSC_R2,
			RGSC_SELECT,	RGSC_START,	RGSC_L3,		RGSC_R3,
			0,0,0,0},
		};

		for (int i=0; i<16; i++)
		{
			if (S.GetButtons() & (1<<i))
			{
				m_Buttons |= map[whichMap][i];
			}
		}
	}
}



void ioD3DPad::Refresh()
{
#if __DEV
	int numSticks = ioJoystick::GetStickCount();
#endif

	ioJoystick::EndAll();
	ioJoystick::BeginAll();

#if __DEV
	if (numSticks != ioJoystick::GetStickCount())
	{
		Displayf("SCUI Input thread -- detected joypad %s", numSticks > ioJoystick::GetStickCount() ? "removal" : "addition");
	}
#endif
}

} // namespace rgsc

#endif // RGSC_DINPUT_SUPPORT