//
// input/joystick.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "joystick.h"

#if RGSC_DINPUT_SUPPORT

#include "string/string.h"
#include "system/nelem.h"

#define DIRECTINPUT_VERSION 0x0800

#pragma warning(push)
#pragma warning(disable: 4668)
#include <dinput.h>
#include <atlconv.h>
#pragma warning(push)

#pragma comment(lib,"dinput8.lib")
#pragma comment(lib,"dxguid.lib")

using namespace rage;

namespace rgsc
{

typedef u16 char16;

#define ditry(x) do																				\
				 {																				\
					 HRESULT hr;																\
					 if((hr = (x)) != DI_OK)													\
					 {																			\
						 AssertMsg(false, "%s failed, code=%08x (%d)", #x, hr, hr & 0xffff);	\
					 }																			\
				 } while(false)

static int s_joyDebug = 0;

// initiate the number of arrays for stick
ioJoystick ioJoystick::sm_Sticks[MAX_STICKS];
int ioJoystick::sm_StickCount;

#if DIRECTINPUT_VERSION > 0x300

int ioJoystick::GetDeviceInfo(DIDEVICEINSTANCEW &info) const
{
	Assert(m_Device);
	info.dwSize = sizeof(DIDEVICEINSTANCEW);
	return m_Device->GetDeviceInfo(&info);
}

int ioJoystick::GetDevType() const
{
	DIDEVICEINSTANCEW deviceInfo;
	GetDeviceInfo(deviceInfo);
	return deviceInfo.dwDevType;
}

void ioJoystick::GetProductName(u16 *_name, u32 size) const
{
	CompileTimeAssert(sizeof(u16)==sizeof(WCHAR));
	WCHAR *name = (WCHAR*)_name;
	DIDEVICEINSTANCEW deviceInfo;
	CompileTimeAssert(sizeof(u16)==sizeof(deviceInfo.tszProductName[0])); // make sure we're getting a wide string
	GetDeviceInfo(deviceInfo);
	wcsncpy_s(name, size - 1, deviceInfo.tszProductName,size - 1);
	name[size-1]=0;
}

bool ioJoystick::IsValid() const
{
	return m_Device!=0;
}

BOOL CALLBACK ioJoystick::EnumDeviceProc(LPCDIDEVICEINSTANCEW lpddi,LPVOID /*pvRef*/) 
{
	USES_CONVERSION;
	CompileTimeAssert(sizeof(WCHAR) == sizeof(char16));
	CompileTimeAssert(sizeof(char16) == sizeof(lpddi->tszProductName[0])); // make sure we're getting a wide string
	const char *productName = W2A(lpddi->tszProductName);
	const char *instanceName = W2A(lpddi->tszInstanceName);

	//Displayf("Joystick Subtype = %d, Instance = [%s], Product = [%s]",subtype,instanceName,productName);

	char *pJoystickType = NULL;
	size_t len = 0;
	errno_t err = _dupenv_s(&pJoystickType, &len, "JOYSTICK_TYPE");

	if((err == 0) && (len > 0) && (pJoystickType != NULL))
	{
		sm_Sticks[sm_StickCount].m_Type=(ioJoystick::EnumJoyType)atoi(pJoystickType);
		free(pJoystickType);
	}
	else if (!_stricmp("PSX/USB Pad Adaptor (4-axis, 12-button, POV,effects)", productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_KIKYXSERIES;
	}
	else if (!_stricmp("USB PSX JOYDAPTER",productName) ||
		!_stricmp("USB PSX JOYDAPTER",instanceName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_SMARTJOYPAD;
	}
	else if (!_stricmp("PSX for USB Converter",instanceName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_PSX_TO_USB_CONVERTER;
	}
	else if (!_stricmp("DirectPad Pro Controller",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_DIRECTPAD;
	}
	else if (!strncmp("Microsoft SideWinder Game Pad Pro",productName,33) || !strncmp("SideWinder Game Pad Pro",instanceName,23))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_GAMEPADPRO;
	}
	else if (!_stricmp("HID-compliant game controller",productName) || !_stricmp("USB Human Interface Device",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_SMARTJOYPAD;
	}
    else if (!_stricmp("MP-8866 Dual USB Joypad",productName))
	{
        sm_Sticks[sm_StickCount].m_Type=TYPE_DUALJOYPAD;
	}
	else if (!_stricmp("MP-866 Dual USB Joypad",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_DUAL_USB_FFJ_MP866;
	}
	else if (!_stricmp("Dual USB Force Feedback Joypad (MP-8866)",productName))
	{
        sm_Sticks[sm_StickCount].m_Type=TYPE_DUALJOYPAD;
	}
	else if (!_stricmp("GIC USB Joystick",productName))
	{
        sm_Sticks[sm_StickCount].m_Type=TYPE_XKPC2002;
	}
	else if (!_stricmp("Logitech WingMan Cordless Gamepad USB",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_LOGITECH_CORDLESS;
	}
	else if (!_stricmp("WingMan Cordless Gamepad",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_LOGITECH_CORDLESS;
	}
	else if (!_stricmp("SmartJoy PLUS Adapter",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_DUALJOYPAD;
	}
	else if(!_stricmp("Psjoy Vibration feedback (USB) Joystick", productName))
	{	
		sm_Sticks[sm_StickCount].m_Type = TYPE_XKPC2002;
	}
	else if(!_stricmp("PS Vibration Feedback Converter ", productName))
	{	
		sm_Sticks[sm_StickCount].m_Type = TYPE_XKPC2002;
	}
	else if (!_stricmp("MP-8888 USB Joypad",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_SMARTJOYPAD;
	}
	else if (!_stricmp("4 axis 16 button joystick",productName)) { // this is how the SmartJoy pads are being reported by DX8 runtime
		sm_Sticks[sm_StickCount].m_Type=TYPE_SMARTJOYPAD;
		Errorf("*****");
		Errorf("***** Do you have a 'smart joy pad' or a 'PS-PC USB XK-PC200x'?");
		Errorf("***** Add JOYSTICK_TYPE=2 to your environment for 'smart joy pad'");
		Errorf("***** Add JOYSTICK_TYPE=4 to your environment for 'XK-PC200x'");
		Errorf("*****");
	}
	else if (!_stricmp("Interact Gaming Device",productName)) // the temporary Xbox USB controllers report thisb
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_SMARTJOYPAD;
	}
	else if (!_stricmp("XBCD XBox Gamepad",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_XBCD_XBOX_USB;
	}
	else if (!_stricmp("TigerGame Xbox to USB Controller",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_TIGERGAME_XBOX_USB;
	}
	else if (!_stricmp("Logitech Dual Action", productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_LOGITECH_DUAL;
	}
	else if(!_stricmp("Logitech RumblePad 2 USB", productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_LOGITECH_RUMBLEPAD_2;
	}
	else if(!_stricmp("Saitek P480 Rumble Pad", productName))
	{
		sm_Sticks[sm_StickCount].m_Type = TYPE_SAITEK_P480;
	}
	else if(!_stricmp("Saitek Cyborg Rumble Pad", productName))
	{
		sm_Sticks[sm_StickCount].m_Type = TYPE_SAITEK_CYBORG;
	}
	/*
	else if (!_stricmp("XNA Gamepad",productName) || !_stricmp("XBOX 360 For Windows (Controller)",productName) ||
			!_stricmp("Microsoft Xbox Wired Gamepad (Controller)",productName) ||
			!_stricmp("Controller (XBOX 360 For Windows)",productName) ||
			!_stricmp("Microsoft Xbox One Controller",productName) ||
			!_stricmp("Controller (XBOX One For Windows)", productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_XNA_GAMEPAD_XENON;
	}
	*/
	else if (!_stricmp("SmartJoy PLUS USB Adapter",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_DUALJOYPAD;
	}
	else if (!strcmp("USB Vibration Gamepad", productName))
	{
		sm_Sticks[sm_StickCount].m_Type = TYPE_XKPC2002;
	}
	else if (!_stricmp("Wireless Controller", productName))
	{
		sm_Sticks[sm_StickCount].m_Type = TYPE_PS4_WIRELESS_PAD;
	}
	else
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_UNKNOWN;
	}

	safecpy(sm_Sticks[sm_StickCount].productName, productName);
	sm_Sticks[sm_StickCount++].m_Guid = lpddi->guidInstance;

	if(sm_StickCount >= COUNTOF(sm_Sticks))
	{
		return DIENUM_STOP;
	}

	return DIENUM_CONTINUE;
}

#if _DEBUG
void ioJoystick::PrintDeviceCaps() const
{
	Assert(m_Device);
    DIDEVCAPS DevCaps;
	DevCaps.dwSize = sizeof(DIDEVCAPS);
    m_Device->GetCapabilities(&DevCaps);

	DIDEVICEINSTANCEW DevInst;
	DevInst.dwSize = sizeof(DIDEVICEINSTANCEW);
	m_Device->GetDeviceInfo(&DevInst);
	DWORD flags = DevCaps.dwFlags;

	USES_CONVERSION;

	Displayf("JOYSTICK DEVICE DESCRIPTION:");

	CompileTimeAssert(sizeof(WCHAR) == sizeof(char16));
	CompileTimeAssert(sizeof(char16) == sizeof(DevInst.tszProductName[0]));
	Displayf("%s", DevInst.tszInstanceName);
	Displayf("%s", DevInst.tszProductName);

	Displayf("GUID Product Code = %x", DevInst.guidProduct);
	Displayf("Axes = %d, Butons = %d, POVs = %d", DevCaps.dwAxes, DevCaps.dwButtons, DevCaps.dwPOVs);
	Displayf("The Joystick Sub-type returned indicates:");
	//Microsoft SideWinder game pad
	//Microsoft SideWinder Force Feedback Wheel
	//Microsoft SideWinder Force Feedback Pro
	/*
	switch (GET_DIDEVICE_SUBTYPE(DevInst.dwDevType)){
		case DIDEVTYPEJOYSTICK_TRADITIONAL:
			Displayf("A traditional joystick.");
			break;
  		case DIDEVTYPEJOYSTICK_FLIGHTSTICK:
			Displayf("A joystick optimized for flight simulation.");
			break;
		case DIDEVTYPEJOYSTICK_GAMEPAD:
			Displayf("A device whose primary purpose is to provide button input.");
			break;
		case DIDEVTYPEJOYSTICK_RUDDER:
			Displayf("A device for yaw control.");
			break;
		case DIDEVTYPEJOYSTICK_WHEEL:
			Displayf("A steering wheel.");
			break;
		case DIDEVTYPEJOYSTICK_HEADTRACKER:
			Displayf("A device that tracks the movement of the user's head");
			break;
		default:
		case DIDEVTYPEJOYSTICK_UNKNOWN:
			Displayf("The subtype could not be determined.");
			break;
	}
	*/
	Displayf("Device Caps::Flags indicate device has:");
	if (flags & DIDC_ATTACHED) 
	{
		Displayf("Been physically attatched");
	}
	if (flags & DIDC_DEADBAND) 
	{
		Displayf("Support for deadband");
	}
	if (flags & DIDC_EMULATED) 
	{
		Displayf("Emulated functionality");
	}
	if (flags & DIDC_FORCEFEEDBACK) 
	{
		Displayf("Force-feedback");
	}
	if (flags & DIDC_FFFADE) 
	{
		Displayf("Support for fade(FF)");
	}
	if (flags & DIDC_FFATTACK) 
	{
		Displayf("Support for Attack env (FF)");
	}
	if (flags & DIDC_POLLEDDATAFORMAT) 
	{
		Displayf("One device (at least) that is polled in the current data format");
	}
	if (flags & DIDC_POLLEDDEVICE) 
	{
		Displayf("One device (at least) that is polled");
	}
	if (flags & DIDC_POSNEGCOEFFICIENTS) 
	{
		Displayf("Support for two coefficient values for FF displacement");
	}
	if (flags & DIDC_POSNEGSATURATION) 
	{
		Displayf("Support for max FF saturation");
	}
	if (flags & DIDC_SATURATION) 
	{
		Displayf("Support for condition saturation");
	}

}
#else // _DEBUG
// void ioJoystick::PrintDeviceCaps(){}
#endif // _DEBUG


bool ioJoystick::IsWheel() const
{
	if (!m_Device) return false;
	DIDEVICEINSTANCEW deviceInfo;
	GetDeviceInfo(deviceInfo);
	return ((deviceInfo.dwDevType&0xff)== DI8DEVTYPE_DRIVING );
}

LPDIRECTINPUTW lpDI = NULL;

void ioJoystick::BeginAll() {
	// ARGS_SET_DEBUG_LEVEL(s_joyDebug);

	if (!lpDI) {
#if DIRECTINPUT_VERSION > 0x700
		ditry(DirectInput8Create(GetModuleHandle(0),DIRECTINPUT_VERSION,IID_IDirectInput8W,(void**)&lpDI,NULL));
#else
		ditry(DirectInputCreate(GetModuleHandle(0),DIRECTINPUT_VERSION,&lpDI,NULL));
#endif // DIRECTINPUT_VERSION
	}

	if(lpDI == NULL)
	{
		return;
	}
	
	// We only enumerate attached devices.
	lpDI->EnumDevices(
#if DIRECTINPUT_VERSION > 0x700
		DI8DEVCLASS_GAMECTRL,
#else // DIRECTINPUT_VERSION > 0x700
		DIDEVTYPE_JOYSTICK,
#endif // DIRECTINPUT_VERSION > 0x700
		EnumDeviceProc,0,DIEDFL_ATTACHEDONLY);

	for (int i=0; i<sm_StickCount; i++)
	{
		sm_Sticks[i].Begin();
	}
}

void ioJoystick::PollAll() 
{
	for (int i=0; i<sm_StickCount; i++)
	{
		sm_Sticks[i].Poll();
	}
}

void ioJoystick::UpdateAll() 
{
	for (int i=0; i<sm_StickCount; i++)
	{
		sm_Sticks[i].Update();
	}
}

void ioJoystick::EndAll() 
{
	while (sm_StickCount)
	{
		sm_Sticks[--sm_StickCount].End();
	}
}

int __stdcall ioJoystick::EnumObjectProc(LPCDIDEVICEOBJECTINSTANCEW lpddoi,LPVOID /*pvRef*/) 
{
	// ioJoystick *ths = (ioJoystick*)(pvRef);

#if _DEBUG
	unsigned didft = lpddoi->dwType;
	unsigned short instNum = DIDFT_GETINSTANCE(lpddoi->dwType);

	if (s_joyDebug) {
		USES_CONVERSION;
		Displayf("Instnum %d, Type: ",instNum);
		if (didft & DIDFT_ABSAXIS) 
		{
			Displayf("ABS ");
		}
		if (didft & DIDFT_RELAXIS) 
		{
			Displayf("REL ");
		}
		if (didft & DIDFT_AXIS) 
		{
			Displayf("AXIS ");
		}
		if (didft & DIDFT_TGLBUTTON) 
		{
			Displayf("TOGGLE ");
		}
		if (didft & DIDFT_PSHBUTTON) 
		{
			Displayf("PUSH ");
		}
		if (didft & DIDFT_BUTTON) 
		{
			Displayf("BUTTON ");
		}
		if (didft & DIDFT_POV) 
		{
			Displayf("POV ");
		}
		CompileTimeAssert(sizeof(WCHAR) == sizeof(char16));
		CompileTimeAssert(sizeof(char16) == sizeof(lpddoi->tszName[0]));
		Displayf("named [%s]",W2A(lpddoi->tszName));
	}
#else // _DEBUG
	lpddoi;
#endif // _DEBUG

	return DIENUM_CONTINUE;
}

void ioJoystick::Begin() 
{
	LPDIRECTINPUTDEVICEW dev = 0;
	m_Device = NULL;

	if(lpDI == NULL)
	{
		return;
	}

	lpDI->CreateDevice(m_Guid,&dev,0);

	if(dev == NULL)
	{
		return;
	}

#if DIRECTINPUT_VERSION > 0x700
	ditry(dev->QueryInterface(IID_IDirectInputDevice8W,(LPVOID*)&m_Device));
#else // DIRECTINPUT_VERSION > 0x700
	ditry(dev->QueryInterface(IID_IDirectInputDevice2W,(LPVOID*)&m_Device));
#endif // DIRECTINPUT_VERSION > 0x700

	dev->Release();

	if(m_Device == NULL)
	{
		return;
	}

	ditry(m_Device->EnumObjects(EnumObjectProc,this,DIDFT_ALL));

	ditry(m_Device->SetDataFormat(&c_dfDIJoystick2));
	/*ditry*/(m_Device->Acquire());

	//DIDEVCAPS dc;
	DIDEVCAPS_DX3 dc;
	memset(&dc,0,sizeof(dc));
	dc.dwSize = sizeof(dc);
	ditry(m_Device->GetCapabilities((DIDEVCAPS*)&dc));
	m_AxisCount = dc.dwAxes;
	m_ButtonCount = dc.dwButtons;
	m_POVCount = dc.dwPOVs;

	//Displayf("%d axes, %d buttons, %d POV hats",m_AxisCount,m_ButtonCount,m_POVCount);
}

void ioJoystick::Poll() {
	if (m_Device)
	{
		m_Device->Poll();
	}
}

void ioJoystick::Update() 
{
	if (m_Device) 
	{
		DIJOYSTATE2 state;
		if (m_Device->GetDeviceState(sizeof(state),&state) != DI_OK)
		{
			return;
		}

		m_Axes[AXIS_LX] = state.lX;
		m_Axes[AXIS_LY] = state.lY;
		m_Axes[AXIS_LZ] = state.lZ;
		m_Axes[AXIS_LRX] = state.lRx;
		m_Axes[AXIS_LRY] = state.lRy;
		m_Axes[AXIS_LRZ] = state.lRz;
		m_Axes[AXIS_SLIDER0] = state.rglSlider[0];
		m_Axes[AXIS_SLIDER1] = state.rglSlider[1];

		m_POVs[0] = (short)state.rgdwPOV[0];
		m_POVs[1] = (short)state.rgdwPOV[1];
		m_POVs[2] = (short)state.rgdwPOV[2];
		m_POVs[3] = (short)state.rgdwPOV[3];

		unsigned buttons = 0, mask = 1;
		const int numButtons = sizeof(state.rgbButtons) < 30 ? sizeof(state.rgbButtons) : 30;
		for (int i=0; i<numButtons; i++,mask<<=1)
		{
			if (state.rgbButtons[i] & 0x80)
			{
				buttons |= mask;
			}
		}

		m_LastButtons = m_Buttons;
		m_Buttons = buttons;

		memcpy(m_LastAnalogButtons, m_AnalogButtons, MAX_BUTTONS);
		memcpy(m_AnalogButtons, state.rgbButtons, MAX_BUTTONS);

		//Displayf("X = %d; Y = %d; Buttons = %x, POV = %d", m_Axes[0], m_Axes[1], m_Buttons, m_POVs[0]);
	}
}

void ioJoystick::End() 
{
	if (m_Device) 
	{
		m_Device->Unacquire();

		m_Device->Release();
		m_Device = 0;
	}
}

#else

// Stubbed-out versions for DI 3

// PURPOSE:	Update all devices
void ioJoystick::UpdateAll() 
{

}

// PURPOSE:	Poll all devices
void ioJoystick::PollAll() 
{

}

// PURPOSE:	Initialize all devices
void ioJoystick::BeginAll() 
{

}

// PURPOSE:	Shut down all devices
void ioJoystick::EndAll() 
{

}

#endif // DIRECTINPUT_VERSION

} // namespace rgsc

#endif // RGSC_DINPUT_SUPPORT