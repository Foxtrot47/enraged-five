#ifndef RGSC_GRAPHICS_H
#define RGSC_GRAPHICS_H

#pragma warning(push)
#pragma warning(disable: 4668)
#include <unknwn.h>
#pragma warning(pop)

namespace rgsc
{

class TiledBuffer;

class Rgsc_Graphics
{
public:
	struct RenderRect
	{
		unsigned int m_Top;
		unsigned int m_Left;
		unsigned int m_Bottom;
		unsigned int m_Right;
		unsigned char m_IsSet;

		RenderRect()
		: m_Top(0)
		, m_Left(0)
		, m_Bottom(0)
		, m_Right(0)
		, m_IsSet(0)
		{

		}

		void Set(unsigned int top, unsigned int left, unsigned int bottom, unsigned int right)
		{
			m_Top = top;
			m_Left = left;
			m_Bottom = bottom;
			m_Right = right;
			m_IsSet = 1;
		}

		bool IsSet() const {return m_IsSet == 1;}

		unsigned int GetWidth() const {return m_Right - m_Left;}
		unsigned int GetHeight() const {return m_Bottom - m_Top;}
	};

	static const unsigned int sm_MaxTileWidth = 256;
	static const unsigned int sm_MaxTileHeight = 256;

	static Rgsc_Graphics* DeviceFactory(IUnknown *pD3D, void *params, const RenderRect& rect);
	Rgsc_Graphics();
	virtual ~Rgsc_Graphics();
 	virtual void Shutdown() = 0;
 	virtual void Render(bool onlyNotifications) = 0;
	virtual bool OnLost() = 0;
	virtual bool OnReset(void *params, const RenderRect& rect) = 0;
	virtual void SetContext(void* context) = 0;

	HWND GetWindowHandle() {return m_WindowHandle;}
	unsigned int GetWidth() {return m_Width;}
	unsigned int GetHeight() {return m_Height;}
	virtual bool MapMouse(int &x, int &y) = 0;

	TiledBuffer* GetTiles() {return m_Tiles;}

	static float MapNumFromRangeToRange(float fValue, float fMin, float fMax, float fMinValue, float fMaxValue);

protected:
	HWND m_WindowHandle;
	RenderRect m_Rect;
	unsigned int m_Width;
	unsigned int m_Height;
	unsigned int m_ClientWidth;
	unsigned int m_ClientHeight;
	unsigned int m_OffsetX;
	unsigned int m_OffsetY;
	unsigned int m_MappedOffsetX;
	unsigned int m_MappedOffsetY;
	unsigned int m_MappedWidth;
	unsigned int m_MappedHeight;
	TiledBuffer* m_Tiles;
};

} // namespace rgsc

#endif //RGSC_GRAPHICS_H
