#include "RgscWindow.h"

#include "filesystem.h"
#include "input.h"
#include "tiledbuffer.h"
#include "string/string.h"
#include "script.h"

#include "../rgsc/diag/output.h"
#include "rgsc_messages.h"
#include "rgsc_messages_params.h"

#define IPC_SEND(x) GetRgscConcreteInstance()->_GetUiInterface()->_GetIpc()->Send(x)

static const unsigned int INVALID_WINDOW_ID = 0xFFFFFFFF;

namespace rage
{
	XPARAM(nethttpdump);
}

namespace rgsc
{

void RgscWindow::TestForwardingObj::Send(IPC::Message* message)
{
	GetRgscConcreteInstance()->_GetUiInterface()->_GetIpc()->Send(message, m_IpcChannelName.c_str());
}

void RgscWindow::TestForwardingObj::onJavascriptCallbackSync(ViewHostMsg_OnJavascriptCallback_Params params, ViewHostMsg_OnJavascriptCallback_ReturnVal* returnVal)
{
	m_IpcChannelName = params.m_IpcChannel;
	m_Window->onJavascriptCallback(params, returnVal);
}

RgscWindow::RgscWindow(unsigned int width, unsigned int height, float scale)
: m_WindowId(INVALID_WINDOW_ID)
, m_MyTestObj(this)
, m_Width(width)
, m_Height(height)
, m_Scale(scale)
, m_RemoteWindowCreated(false)
, m_ErrorOnLoad(false)
, m_CursorType(RGSC_CURSOR_POINTER)
, m_Buffer(NULL)
{
	m_Visible = false;
	m_LoadStartTime = 0;
	
	// TODO: NS - this needs to be passed in by a future WindowManager/ViewManager
	m_WindowId = 1;

	// start off with the arrow cursor
	m_Cursor = LoadCursor(NULL, IDC_ARROW);
}

RgscWindow::~RgscWindow()
{
	// TODO: NS - need to make sure the remote window is destroyed before the sharedmemory is unmapped
	//IPC_SEND(new ViewMsg_DestroyWindow(m_WindowId));
}

void RgscWindow::sendMouseMoved(__int64 msg, __int64 wParam, __int64 lParam)
{
	IPC_SEND(new ViewMsg_MouseMoved(m_WindowId, msg, wParam, lParam));
}

void RgscWindow::sendMouseButton(__int64 msg, __int64 wParam, __int64 lParam)
{
	IPC_SEND(new ViewMsg_MouseButton(m_WindowId, msg, wParam, lParam));
}

void RgscWindow::sendMouseWheel(int xScroll, int yScroll)
{
	IPC_SEND(new ViewMsg_MouseWheel(m_WindowId, xScroll, yScroll));
}

void RgscWindow::sendKeyEvent(__int64 msg, __int64 wParam, __int64 lParam, __int64 scModifiers)
{
	IPC_SEND(new ViewMsg_KeyEvent(m_WindowId, msg, wParam, lParam, scModifiers));
}

void RgscWindow::sendNavigateTo(const char *url, const char* offlineRoot)
{
	std::string sUrl(url, strlen(url));

	// If an offline root is specified, create a mapping to a ZIP file.
	//	i.e. C:\\Program Files\\Rockstar Games\\Social Club\\UI\\ 
	if (offlineRoot && offlineRoot[0] != '\0')
	{
		// Set up the offline root in the form that CefResourceManager's AchiveProvider requires.
		// i.e. file:///C:/Program%20Files/Rockstar%20Games/Social%20Club/UI/
		atString fullOfflineRoot;
		fullOfflineRoot += "file:///"; // requires file:/// prefix
		fullOfflineRoot += offlineRoot;
		fullOfflineRoot.Replace("\\", "/"); // requires forward slashes
		fullOfflineRoot.Replace(" ", "%20"); // requires encoded spaces

		// Set up the zip path CefResourceManager's AchiveProvider requires.
		// i.e. C:\\Program Files\\Rockstar Games\\Social Club\scui.pak
		const char* offlineScuiOverride = GetRgscConcreteInstance()->GetScuiPakPath();
		if (offlineScuiOverride && offlineScuiOverride[0] != '\0')
		{
			IPC_SEND(new ViewMsg_NavigateTo(m_WindowId, sUrl, std::string(fullOfflineRoot.c_str()), GetRgscConcreteInstance()->GetScuiPakPath()));
		}
		else
		{
			atString fullZipFile(offlineRoot);
			fullZipFile.Truncate(fullZipFile.GetLength() - (int)strlen(RgscFileSystem::UI_PATH)); // remove "UI\\" folder path
			fullZipFile += "scui.pak"; // add the scui.pak extension

			IPC_SEND(new ViewMsg_NavigateTo(m_WindowId, sUrl, std::string(fullOfflineRoot.c_str()), std::string(fullZipFile.c_str())));
		}
	}
	else
	{
		IPC_SEND(new ViewMsg_NavigateTo(m_WindowId, sUrl, "", ""));
	}
}
 
void RgscWindow::sendAddBindOnStartLoading(RgscWideString functionName, bool synchronous)
{
	//Displayf("addBindOnStartLoading: %ls, synchronous: %s", functionName.data(), synchronous ? "true" : "false");
	IPC_SEND(new ViewMsg_AddBindOnStartLoading(m_WindowId, functionName.data(), synchronous));
}

void RgscWindow::sendSetTiledBuffer(TiledBuffer* tiledBuffer)
{
	m_Buffer = tiledBuffer;

	if(tiledBuffer)
	{
		if(m_RemoteWindowCreated == false)
		{
			m_RemoteWindowCreated = true;

			ViewMsg_Tile_Params tiles;
			RgscRect rect;
			for(unsigned int i = 0; i < tiledBuffer->getNumTiles(); i++)
			{
				Tile* tile = tiledBuffer->getTile(i);
				if(tile)
				{
					rect.mLeft = tile->getLeft();
					rect.mTop = tile->getTop();
					rect.mWidth = tile->getWidth();
					rect.mHeight = tile->getHeight();
					tiles.tile_rects.push_back(rect);
				}
			}
			
			IPC_SEND(new ViewMsg_CreateTiledWindow(m_WindowId, m_Width, m_Height, m_Scale, tiles, tiledBuffer->getSharedMemoryName()));
		}
	}
	else
	{
		IPC_SEND(new ViewMsg_OnLostDevice(m_WindowId));
	}
}

bool RgscWindow::TimedOut()
{
	bool timedOut = false;
	unsigned int curTime = timeGetTime();
	if(m_LoadStartTime != 0)
	{
		unsigned int elapsedTime = curTime - m_LoadStartTime;
		if(elapsedTime >= LOAD_PAGE_TIMEOUT)
		{
			Warningf("Loading URL timed out");
			m_LoadStartTime = 0;
			timedOut = true;
		}
	}

	return timedOut;
}

void RgscWindow::ShowUi()
{
	m_Visible = true;
}

void RgscWindow::HideUi()
{
	m_Visible = false;
}

bool RgscWindow::IsUiVisible()
{
	return m_Visible;
}

void RgscWindow::clear()
{
	if(m_Buffer)
	{
		m_Buffer->clear();
	}
}

void RgscWindow::onAddressBarChanged(RgscUrlString newURL)
{
	Displayf("onAddressBarChanged(): newURL: %s", newURL.data());
}

void RgscWindow::onProvisionalLoadError(RgscUrlString url,
										int errorCode,
										RgscWideString errorText,
										bool isMainFrame)
{
	// TODO: NS - parameters
	Errorf("onProvisionalLoadError() : errorCode: %d, errorText: %ls url: %s", errorCode, errorText.data(), url.data());

	// We could get a load aborted error on the previous URL
	if (strcmp(url.data(), GetRgscConcreteInstance()->_GetUiInterface()->GetCurrentUrl()) == 0)
	{
		GetRgscConcreteInstance()->_GetUiInterface()->PageLoadComplete(errorCode);
		m_ErrorOnLoad = true;
	}
}

void RgscWindow::onConsoleMessage(int logLevel, RgscWideString message, RgscWideString sourceId, int line_no)
{
#if !__NO_OUTPUT
	// Don't show an assert for console messages that won't be fixed for awhile (if ever)
	if((wcsstr(message.data(), L"was loaded over HTTPS, but displayed insecure content from") != NULL) ||
	  (wcsstr(message.data(), L"was loaded over HTTPS, but requested an insecure image") != NULL) ||
	  (wcsstr(message.data(), L"[SCUI]") != NULL))
	{
		// ignore...
	}
	else if ((wcsstr(message.data(), L"Synchronous XMLHttpRequest on the main thread is deprecated") != NULL) ||
		     (wcsstr(message.data(), L"Invoking 'send()' on a sync XHR during microtask execution is deprecated") != NULL) || 
			 (wcsstr(message.data(), L"'webkitIDBKeyRange' is deprecated. Please use 'IDBKeyRange' instead") != NULL) ||
			 (wcsstr(message.data(), L"'webkitURL' is deprecated. Please use 'URL' instead") != NULL))
	{
		// log out depreciated errors, since we need to address these
		Errorf("onConsoleMessage: %ls(%d) : error: %ls", sourceId.data(), line_no, message.data());
	}
	else
	{
		// assert
		Assertf(false, "onConsoleMessage [%d]: %ls(%d) : error: %ls", logLevel, sourceId.data(), line_no, message.data());
	}
#endif
}

void RgscWindow::onScriptAlert(RgscWideString message,
							   RgscWideString defaultValue,
							   RgscUrlString url,
							   int flags,
							   bool &success,
							   RgscWideString &value)
{
	// TODO: NS - parameters
	Assertf(false, "onScriptAlert: %ls", message.data());
}

void RgscWindow::onLoadingStateChanged(bool isLoading)
{
	if(isLoading)
	{
		m_LoadStartTime = timeGetTime();
	}

	Displayf("onLoadingStateChanged(): loading state changed to '%s'", isLoading ? "started" : "stopped");

	if(isLoading == false)
	{
		// if the SCUI website failed to load due to a non 2xx HTTP status code, then the renderer subprocess
		// won't create a Javascript context and we'll never call SetJavascriptReadyToAcceptCommands().
		// This causes us to wait indefinitely for javascript bindings to complete. Instead, if we get a loading
		// state change with isLoading == false, we know the page has finished loading so we can move on to
		// the next stage.
		GetRgscConcreteInstance()->_GetUiInterface()->SetJavascriptReadyToAcceptCommands();
	}

	// Page load 'Complete' only if no errors encountered during load
	if(isLoading == false && !m_ErrorOnLoad)
	{
		GetRgscConcreteInstance()->_GetUiInterface()->PageLoadComplete(0);
	}

	// Clear error flag when loading starts or stops
	m_ErrorOnLoad = false;
}

void RgscWindow::onTitleChanged(RgscWideString title)
{
	Displayf("onTitleChanged(): title: '%ls'", title.data());
}

void RgscWindow::onTooltipChanged(RgscWideString text)
{
	Displayf("onTooltipChanged(): tooltip text: '%ls'", text.data());
}

void RgscWindow::onKeyEvent(s64 msg, s64 wParam, s64 lParam, s64 scKeyMods)
{
	// todo - multiple window support
	if (m_WindowId == 1)
	{
		GetRgscConcreteInstance()->_GetUiInterface()->OnKeyEvent(msg, wParam, lParam, scKeyMods);
	}
}

void RgscWindow::onJavascriptCallback(const ViewHostMsg_OnJavascriptCallback_Params& params,
									  ViewHostMsg_OnJavascriptCallback_ReturnVal* returnVal)
{
	// TODO: NS - new multiprocess version only supports up to 1 arg at the moment
	size_t numArgs = 0;
	if(params.m_Arg.length() > 0)
	{
		numArgs = 1;
	}

#if __DEV
	wchar_t coutput[4096] = {0};
	safecatf(coutput, L"Javascript called %ls(", params.m_FunctionName.c_str());

	for(size_t i = 0; i < numArgs; i++)
	{
		if(params.m_Arg.length() != 0)
		{
			safecatf(coutput, L"'%ls'", params.m_Arg.c_str());
		}

		if(i < numArgs - 1)
		{
			safecatf(coutput, L", ");
		}
	}

	safecatf(coutput, L") %ls", returnVal ? L"(synchronous)" : L"(async)");

	// TODO: NS - how to print out long strings using Rage (i.e. not printf("%c") loop)?
	Displayf("%ls", coutput);
#endif

	RgscVariant var;
	if(numArgs == 1)
	{
		var.initwc(params.m_Arg.c_str(), params.m_Arg.length());
	}
	RgscWideString szFunctionName = RgscWideString::point_to(params.m_FunctionName.c_str());
	rgsc::Script::Info info(&szFunctionName, &var, numArgs);
	rgsc::Script::CallFunction(info);

	if(info.m_IsAutoAsync)
	{
		// asynchronous function returning a value asynchronously

		rtry
		{
			rverify(GetRgscConcreteInstance()->IsUiDisabled() == false, catchall, );
			rverify(GetRgscConcreteInstance()->_GetUiInterface()->IsJavascriptReadyToAcceptCommands(), catchall, );

			std::wstring funcReturnName(info.m_FuncName->data(), info.m_FuncName->length());
			funcReturnName.append(L"_RESULT");

			{
				std::wstring toFind(L"RGSC_");
				std::wstring replacement(L"RGSC_JS_");
				size_t pos = funcReturnName.find(toFind);
				if(pos == 0)
				{
					funcReturnName.replace(pos, toFind.length(), replacement);
				}
			}

			wchar_t wszJavascript[MAX_JAVASCRIPT_LENGTH] = {0};
			formatf(wszJavascript, L"if(typeof window['%ls'] == 'function') {%ls(", funcReturnName.c_str(), funcReturnName.c_str());

			if(info.m_Result.type() == RgscVariant::JSSTRING)
			{
				if(info.m_Result.toString().data())
				{
					RgscVariant v(info.m_Result.toString().data());
					RgscWideString wsJson = rgsc::toJSON(v);
					safecatf(wszJavascript, L"%ls", wsJson.data());
					rgsc::toJSON_free(wsJson);
				}
				else
				{
					safecat(wszJavascript, L"''");
				}
			}
			else if(info.m_Result.type() == RgscVariant::JSBOOLEAN)
			{
				safecatf(wszJavascript, L"'{\"Result\" : %ls}'", info.m_Result.toBoolean() ? L"true" : L"false");
			}
			else if(info.m_Result.type() == RgscVariant::JSDOUBLE)
			{
				safecatf(wszJavascript, L"'{\"Result\" : %.20f}'", info.m_Result.toDouble());
			}
			else if(info.m_Result.type() == RgscVariant::JSINTEGER)
			{
				safecatf(wszJavascript, L"'{\"Result\" : %d}'", info.m_Result.toInteger());
			}

			safecatf(wszJavascript, L");} else {RGSC_FUNCTION_NOT_DEFINED('%ls');}", funcReturnName.c_str());
			Assert((wcslen(wszJavascript) + sizeof(wszJavascript[0])) < (sizeof(wszJavascript) / sizeof(wszJavascript[0])));

			ASSERT_ONLY(size_t msgLen = wcslen(wszJavascript));
			Assertf((msgLen + sizeof(wszJavascript[0])) < (sizeof(wszJavascript) / sizeof(wszJavascript[0])), "Message length was too long (%d)", msgLen);
			Assertf(msgLen < (0.75 * MAX_JAVASCRIPT_LENGTH), "Approaching limits of SCUI Javascript messages: %d/%d", msgLen, MAX_JAVASCRIPT_LENGTH);

			CallJavascript(RgscWideString::point_to(wszJavascript));
		}
		rcatchall
		{

		}
	}
	else
	{
		if(returnVal == NULL)
		{
			// asynchronous, nothing to return
			return;
		}

		// synchronous
#if __DEV
		wchar_t wszJavascript[MAX_JAVASCRIPT_LENGTH] = {0};
		formatf(wszJavascript, L"%ls returning: ", info.m_FuncName->data());
#endif

		if(info.m_Result.type() == RgscVariant::JSBOOLEAN)
		{
			returnVal->m_Type = ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_BOOL;
			returnVal->m_Bool = info.m_Result.toBoolean();
#if __DEV
			safecatf(wszJavascript, L"%ls", info.m_Result.toBoolean() ? L"true" : L"false");
#endif
		}
		else if(info.m_Result.type() == RgscVariant::JSDOUBLE)
		{
			returnVal->m_Type = ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_DOUBLE;
			returnVal->m_Double = info.m_Result.toDouble();
#if __DEV
			safecatf(wszJavascript, L"%.20f", info.m_Result.toDouble());
#endif
		}
		else if(info.m_Result.type() == RgscVariant::JSINTEGER)
		{
			returnVal->m_Type = ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_INT;
			returnVal->m_Int = info.m_Result.toInteger();
#if __DEV
			safecatf(wszJavascript, L"%d", info.m_Result.toInteger());
#endif
		}
		else if(info.m_Result.type() == RgscVariant::JSSTRING)
		{
			std::wstring wStr(info.m_Result.toString().data(), info.m_Result.toString().length());
			returnVal->m_Type = ViewHostMsg_OnJavascriptCallback_ReturnVal::TYPE_STRING;
			returnVal->m_String = wStr;
#if __DEV
			safecatf(wszJavascript, L"%ls", wStr.c_str());
#endif
		}
		else
		{
			Assertf(info.m_Result.type() == RgscVariant::JSNULL, "Unexpected result type %d", info.m_Result.type());
		}

		Displayf("%ls", wszJavascript);
	}
}

void RgscWindow::onJavascriptCallbackAsyncIpc(ViewHostMsg_OnJavascriptCallback_Params params)
{
	onJavascriptCallback(params, NULL);
}

void RgscWindow::onJavascriptCallbackSync(ViewHostMsg_OnJavascriptCallback_Params params,
										  ViewHostMsg_OnJavascriptCallback_ReturnVal* returnVal)
{
	onJavascriptCallback(params, returnVal);
}

void RgscWindow::onWindowCreatedIpc(rage::u64 browserHandle, rage::u64 parentWindowHandle)
{
	if (GetRgscConcreteInstance()->_GetUiInterface())
	{
		GetRgscConcreteInstance()->_GetUiInterface()->OnWindowCreated(browserHandle, parentWindowHandle);
	}
}

void RgscWindow::onWindowSizeChangedIpc(int width, int height)
{
	if (GetRgscConcreteInstance()->_GetUiInterface())
	{
		GetRgscConcreteInstance()->_GetUiInterface()->OnWindowSizeChanged(width, height);
	}
}

void RgscWindow::onWindowClosingIpc()
{
	if (GetRgscConcreteInstance()->_GetUiInterface())
	{
		GetRgscConcreteInstance()->_GetUiInterface()->OnWindowClosing();
	}
}

void RgscWindow::onRendererCrashedIpc()
{
	if (GetRgscConcreteInstance()->_GetUiInterface())
	{
		GetRgscConcreteInstance()->_GetUiInterface()->OnRendererCrashed();
	}
}

void RgscWindow::onMouseMovedIpc(__int64 /*msg*/, __int64 wParam, __int64 lParam)
{
	int newMouseX = (int)(short)LOWORD(lParam);
	int newMouseY = (int)(short)HIWORD(lParam);
	Rgsc_Input::HandleMouseMove(newMouseX, newMouseY);
}

void RgscWindow::onKeyEventIpc(__int64 msg, __int64 wParam, __int64 lParam, __int64 scKeyMods)
{
	onKeyEvent(msg, wParam, lParam, scKeyMods);
}

void RgscWindow::CallJavascript(RgscWideString javascript)
{
	Displayf("Executing javascript: %ls", javascript.data());
	std::wstring wJavascript(javascript.data());
	IPC_SEND(new ViewMsg_ExecuteJavascript(m_WindowId, wJavascript));
}

void RgscWindow::Resize(unsigned int width, unsigned int height, float scale)
{
	m_Width = width;
	m_Height = height;
	m_Scale = scale;

	if(AssertVerify(m_Buffer))
	{
		ViewMsg_Tile_Params tiles;
		RgscRect rect;
		for(unsigned int i = 0; i < m_Buffer->getNumTiles(); i++)
		{
			Tile* tile = m_Buffer->getTile(i);
			if(tile)
			{
				rect.mLeft = tile->getLeft();
				rect.mTop = tile->getTop();
				rect.mWidth = tile->getWidth();
				rect.mHeight = tile->getHeight();
				tiles.tile_rects.push_back(rect);
			}
		}

		IPC_SEND(new ViewMsg_ResizeWindow(m_WindowId, m_Width, m_Height, m_Scale, tiles, m_Buffer->getSharedMemoryName()));
	}
}

void RgscWindow::AddCaptionExclusion(int x, int y, int width, int height, int referencePoint)
{
	IPC_SEND(new ViewMsg_AddCaptionExclusion(m_WindowId,x,y,width,height,referencePoint));
}

void RgscWindow::ClearCaptionExclusion()
{
	IPC_SEND(new ViewMsg_ClearCaptionExclusion(m_WindowId));
}

void RgscWindow::DestroyWindow()
{
	IPC_SEND(new ViewMsg_CloseBrowser(m_WindowId));
}

// TODO: NS - the window delegate may need to be derived in a 'window manager' type class which accepts the WindowId in each message.
// The window manager would then call the method of the window indicated by the WindowId
void RgscWindow::onLoadingStateChangedIpc(bool isLoading)
{
	onLoadingStateChanged(isLoading);
}

void RgscWindow::onAddressBarChangedIpc(std::string newURL)
{
	onAddressBarChanged(RgscUrlString::point_to(newURL.c_str()));
}

void RgscWindow::onProvisionalLoadErrorIpc(std::string url, int errorCode, std::wstring errorText, bool isMainFrame)
{
	onProvisionalLoadError(RgscUrlString::point_to(url.c_str()), errorCode, RgscWideString::point_to(errorText.c_str()), isMainFrame);
}

void RgscWindow::onConsoleMessageIpc(int logLevel, std::wstring message, std::wstring sourceId, int line_no)
{
	onConsoleMessage(logLevel, RgscWideString::point_to(message.c_str()), RgscWideString::point_to(sourceId.c_str()), line_no);
}

void RgscWindow::onScriptAlertIpc(std::wstring message, std::wstring defaultValue, std::string url, int flags)
{
	bool success;
	RgscWideString value;
	onScriptAlert(RgscWideString::point_to(message.c_str()), RgscWideString::point_to(defaultValue.c_str()), RgscUrlString::point_to(url.c_str()), flags, success, value);
}

void RgscWindow::onTitleChangedIpc(std::wstring title)
{
	onTitleChanged(RgscWideString::point_to(title.c_str()));
}

void RgscWindow::onTooltipChangedIpc(std::wstring text)
{
	onTooltipChanged(RgscWideString::point_to(text.c_str()));
}

void RgscWindow::onCreatedWindowIpc()
{
	Displayf("onCreatedWindow()");
}

void RgscWindow::onCursorUpdatedIpc(uint64 hCursor, int cursorType)
{
	m_Cursor = (HCURSOR)hCursor;
	m_CursorType = (RgscCursorType)cursorType;
}

void RgscWindow::onDebugRequestContentsIpc(std::string contents)
{
#if !__NO_OUTPUT
	if(PARAM_nethttpdump.Get())
	{
		contents = "SCUI requested resource: " + contents;
		Displayf("%s", contents.c_str());
	}
#endif
}

void RgscWindow::onDebugResponseContentsIpc(int status, std::string contents)
{
#if !__NO_OUTPUT
	if(status >= 400)
	{
		contents = "SCUI response for resource (error): " + contents;
		Errorf("%s", contents.c_str());
	}
	else if(PARAM_nethttpdump.Get())
	{
		contents = "SCUI response for resource: " + contents;
		Displayf("%s", contents.c_str());
	}
#endif
}

void RgscWindow::onCertficateErrorIpc(int error_code)
{
	// error code is CEF 'cef_errorcode_t'
	Errorf("onCertficateErrorIpc() : errorCode: %d", error_code);
}

void RgscWindow::onUrlRequestedIpc(std::string url)
{
	// Handle url navigation by launching an external web browser.
	RgscUi* ui = GetRgscConcreteInstance()->_GetUiInterface();
	if (ui->GetWindowType() == WINDOWLESS)
	{
		GetRgscConcreteInstance()->_GetUiInterface()->RequestExternalUrl(url.c_str());
	}
	else
	{
		GetRgscConcreteInstance()->_GetUiInterface()->LaunchExternalUrl(url.c_str());
	}
}

void RgscWindow::onWindowStateChangedIpc(int windowState)
{
	GetRgscConcreteInstance()->_GetUiInterface()->OnWindowStateChanged((rgsc::WindowState)windowState);
}

// this is used to send replies to synchronous IPC messages
void RgscWindow::Send(IPC::Message* message)
{
	IPC_SEND(message);
}

void RgscWindow::OnMessageReceived(const IPC::Message& message)
{
	bool handled = true;
	bool valid = false;

	IPC_BEGIN_MESSAGE_MAP_EX(RgscWindow, message, valid)
 		IPC_MESSAGE_HANDLER(ViewHostMsg_OnLoadingStateChanged, onLoadingStateChangedIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnAddressBarChanged, onAddressBarChangedIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnProvisionalLoadError, onProvisionalLoadErrorIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnConsoleMessage, onConsoleMessageIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnScriptAlert, onScriptAlertIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnTitleChanged, onTitleChangedIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnTooltipChanged, onTooltipChangedIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnCursorUpdated, onCursorUpdatedIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_DebugRequestContents, onDebugRequestContentsIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_DebugResponseContents, onDebugResponseContentsIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_CertificateError, onCertficateErrorIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnUrlRequested, onUrlRequestedIpc);
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnJavascriptCallbackAsync, onJavascriptCallbackAsyncIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnWindowCreated, onWindowCreatedIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnSizeChanged, onWindowSizeChangedIpc);
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnWindowClosing, onWindowClosingIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnRendererCrashed, onRendererCrashedIpc)
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnMouseMoved, onMouseMovedIpc);
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnKeyEvent, onKeyEventIpc);
		IPC_MESSAGE_HANDLER(ViewHostMsg_OnWindowStateChanged, onWindowStateChangedIpc);
		IPC_MESSAGE_UNHANDLED(handled = false)
	IPC_END_MESSAGE_MAP_EX()

	if(!handled)
	{
		handled = true;
		IPC_BEGIN_MESSAGE_MAP_EX(TestForwardingObj, message, valid)
			IPC_MESSAGE_HANDLER_OBJ(ViewHostMsg_OnJavascriptCallbackSync, &m_MyTestObj, onJavascriptCallbackSync)
			IPC_MESSAGE_UNHANDLED(handled = false)
		IPC_END_MESSAGE_MAP_EX()
	}

	Assert(handled);
	Assert(valid);
}

} // namespace rgsc
