// 
// Activation.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RGSC_ACTIVATION_H 
#define RGSC_ACTIVATION_H

#include "activation_interface.h"

namespace rgsc
{

class Activation : public IActivationLatestVersion
{
	// ===============================================================================================
	// inherited from public interfaces
	// ===============================================================================================
public:
	// ===============================================================================================
	// inherited from IRgscUnknown
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

	// ===============================================================================================
	// inherited from IActivationV1
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL ShowCorruptLicenseUi(StringId message, CorruptLicenseCallback callback);
	RGSC_HRESULT RGSC_CALL ShowActivationCodeUi(const int contentId, const char* currentSerialNumber, const bool isAlreadyActivated, ActivationCodeCallback callback);
	RGSC_HRESULT RGSC_CALL ActivationCodeResult(const int contentId, const int result, StringId message);

	RGSC_HRESULT RGSC_CALL ShowLicenseRecoveryUi(const int contentId, const char* recoveryRequestCode, LicenseRecoveryCallback callback);

	RGSC_HRESULT RGSC_CALL LicenseRecoveryResult(const int contentId, const int result, StringId message);

	RGSC_HRESULT RGSC_CALL ShowExpirationCodeUi(const int contentId, const char* currentSerialNumber, ExpirationCodeCallback callback);

	RGSC_HRESULT RGSC_CALL ExpirationCodeResult(const int contentId, const int result, StringId message);

	// ===============================================================================================
	// inherited from IActivationV2
	// ===============================================================================================
	RGSC_HRESULT RGSC_CALL ShowSteamLinkingUi(SteamLinkingCallback callback);

	RGSC_HRESULT RGSC_CALL SteamLinkingResult(const bool linkSucceeded, StringId message);

public:
	Activation();
	virtual ~Activation();

	bool Init();
	void Shutdown();
	void Update();

	void ClearCallbacks();
	void CorruptLicenseResponse();
	void ActivationCodeResponse(const char* jsonResponse);
	void LicenseRecoveryResponse(const char* jsonResponse);
	void ExpirationCodeResponse(const char* jsonResponse);
	void SteamLinkingResponse(const char* jsonResponse);

	ActivationCodeCallback m_ActivationCodeCallback;
	CorruptLicenseCallback m_CorruptLicenseCallback;
	LicenseRecoveryCallback m_LicenseRecoveryCallback;
	ExpirationCodeCallback m_ExpirationCodeCallback;
	SteamLinkingCallback m_SteamLinkingCallback;
};

} // namespace rgsc

#endif // RGSC_ACTIVATION_H 
