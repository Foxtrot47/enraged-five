
#ifndef _RGSC_VARIANT_H_
#define _RGSC_VARIANT_H_

#include "weakstring.h"
#include "stringutil.h"

namespace rgsc {

class Variant {
public:
	enum Type {
		JSSTRING,
		JSDOUBLE,
		JSINTEGER,
		JSBOOLEAN,
		JSNULL,
		JSEMPTYOBJECT,
		JSEMPTYARRAY,
		JSBINDFUNC,
		JSBINDSYNCFUNC
	};
private:
	union {
		WideString mStrPointer;
		double mDoubleValue;
		int mIntValue;
	};

	Type mType;
	bool hasString() {
		return mType == JSSTRING || mType == JSBINDFUNC || mType == JSBINDSYNCFUNC;
	}
	Variant(Type type) {
		initnull(type);
	}
	Variant(WideString str, Type type);
public:
	void initwc(const wchar_t* str, size_t length) ;
	void initmb(const char* str, size_t length);
	void initint(int intval);
	void initdbl(double dblval);
	void initbool(bool boolval);
	void initnull(Type typ);
	void initvariant(const Variant& other);
	void destroy();

	Variant(const char* str);
	Variant(const wchar_t* str);
	Variant(WideString str);
	Variant(double dblval) {
		initdbl(dblval);
	}
	Variant(int intval) {
		initint(intval);
	}
	Variant(bool boolval) {
		initbool(boolval);
	}
	Variant() {
		initnull(JSNULL);
	}

	static Variant emptyArray();
	static Variant emptyObject();

	static Variant bindFunction(WideString name, bool synchronous) {
		return Variant(name, synchronous ? JSBINDSYNCFUNC: JSBINDFUNC);
	}

	Variant(const Variant& other);
	Variant& operator=(const Variant& other);

	bool toBoolean() const {
		if (mType == JSDOUBLE || mType == JSBOOLEAN) {
			return mDoubleValue != 0;
		} else if (mType == JSSTRING) {
			return mStrPointer.length() ? true : false;
		} else {
			return false;
		}
	}

	int toInteger() const {
		if(mType == JSINTEGER) {
			return mIntValue;
		} else if (mType == JSDOUBLE || mType == JSBOOLEAN) {
			return (int)mDoubleValue;
		} else {
			return 0;
		}
	}

	double toDouble() const {
		if (mType == JSDOUBLE || mType == JSBOOLEAN) {
			return mDoubleValue;
		} else {
			return 0;
		}
	}

	WideString toString() const {
		if (mType == JSSTRING) {
			return mStrPointer;
		} else {
			return WideString::empty();
		}
	}

	WideString toFunctionName() const {
		if (mType == JSBINDFUNC || mType == JSBINDSYNCFUNC) {
			return mStrPointer;
		} else {
			return WideString::empty();
		}
	}

	Type type() const {
		return mType;
	}

	~Variant();
};

typedef Variant RgscVariant;
RgscWideString toJSON(const RgscVariant &var);
void toJSON_free(RgscWideString returnedValue);

} // namespace rgsc

#endif // _RGSC_VARIANT_H_
