#include "variant.h"
#include "stringutil.h"
#include "string/string.h"

#include <cstdlib>
#include <string>

namespace rgsc {

	void Variant::initwc(const wchar_t* str, size_t length) {
		mType = JSSTRING;
		if (str && length) {
			// R* CHANGE - make sure string is null terminated
			wchar_t* wide = new wchar_t[length + 1];
			memcpy(wide, str, length * sizeof(wchar_t));
			wide[length] = 0;
			mStrPointer = WideString::point_to(wide, length, true);
		} else {
			mStrPointer = WideString::empty();
		}
	}
	void Variant::initmb(const char* str, size_t length) {
		mType = JSSTRING;
		if (str && length) {
			mStrPointer = UTF8ToWide(UTF8String::point_to(str, length, false));
		} else {
			mStrPointer = WideString::empty();
		}
	}
	void Variant::initdbl(double dblval) {
		mType = JSDOUBLE;
		mDoubleValue = dblval;
	}
	void Variant::initint(int intval) {
		mType = JSINTEGER;
		mIntValue = intval;
	}
	void Variant::initbool(bool boolval) {
		mType = JSBOOLEAN;
		mDoubleValue = boolval ? 1 : 0;
	}
	void Variant::initnull(Type typ) {
		mType = typ;
		mDoubleValue = 0;
	}
	Variant Variant::emptyArray() {
		return Variant(JSEMPTYARRAY);
	}
	Variant Variant::emptyObject() {
		return Variant(JSEMPTYOBJECT);
	}
	void Variant::initvariant(const Variant& other) {
		switch (other.mType) {
		case JSSTRING:
		case JSBINDFUNC:
		case JSBINDSYNCFUNC:
			initwc(other.mStrPointer.data(), other.mStrPointer.length());
			break;
		case JSDOUBLE:
			initdbl(other.mDoubleValue);
			break;
		case JSINTEGER:
			initint(other.mIntValue);
			break;
		case JSBOOLEAN:
			initbool(other.toBoolean());
			break;
		default:
			break;
		}
		mType = other.mType;
	}

	Variant::Variant(const char* str) {
		initmb(str, std::strlen(str));
	}

	Variant::Variant(const wchar_t* str) {
		size_t length = 0;
		while (str[length]) length++;
		initwc(str, length);
	}

	Variant::Variant(const WideString str) {
  		initwc(str.data(), str.length());
	}

	Variant::Variant(const WideString str, Type typ) {
		initwc(str.data(), str.length());
		mType = typ;
	}

	void Variant::destroy() {
		if (hasString()) {
			delete []mStrPointer.data();
		}
		initnull(JSNULL);
	}

	Variant::Variant(const Variant& other) {
		initvariant(other);
	}
	Variant& Variant::operator=(const Variant& other) {
		destroy();
		initvariant(other);
		return *this;
	}

	Variant::~Variant() {
		destroy();
	}

	RgscWideString toJSON(const RgscVariant &var)
	{
		// TODO: NS - this currently only handles strings, and it
		// just removes \n and \r from the string and escapes quotes.

		std::wstring tmp(L"'");
		if(var.toString().data() != NULL)
		{
			tmp.append(var.toString().data());
		}

		{

			std::wstring toFindCr(L"\n");
			std::wstring replacementCr(L"");
			while(true)
			{
				size_t pos = tmp.find(toFindCr);
				if(pos != std::string::npos)
				{
					tmp.replace(pos, toFindCr.length(), replacementCr);
				}
				else
				{
					break;
				}
			}
		}

		{
			std::wstring toFindCr(L"\r");
			std::wstring replacementCr(L"");
			while(true)
			{
				size_t pos = tmp.find(toFindCr);
				if(pos != std::string::npos)
				{
					tmp.replace(pos, toFindCr.length(), replacementCr);
				}
				else
				{
					break;
				}
			}
		}

		{
			std::wstring toFindCr(L"\t");
			std::wstring replacementCr(L"");
			while(true)
			{
				size_t pos = tmp.find(toFindCr);
				if(pos != std::string::npos)
				{
					tmp.replace(pos, toFindCr.length(), replacementCr);
				}
				else
				{
					break;
				}
			}
		}

		{
			std::wstring toFindQuote(L"'");
			std::wstring replacementQuote(L"\\'");
			size_t pos = 1;
			while(true)
			{
				pos = tmp.find(toFindQuote, pos);
				if(pos != std::string::npos)
				{
					tmp.replace(pos, toFindQuote.length(), replacementQuote);
					pos += replacementQuote.length();
				}
				else
				{
					break;
				}
			}
		}

		{
			std::wstring toFindQuote(L"\"");
			std::wstring replacementQuote(L"\\\"");
			size_t pos = 1;
			while(true)
			{
				pos = tmp.find(toFindQuote, pos);
				if(pos != std::string::npos)
				{
					tmp.replace(pos, toFindQuote.length(), replacementQuote);
					pos += replacementQuote.length();
				}
				else
				{
					break;
				}
			}
		}

		tmp.append(L"'");

		wchar_t* wStr = new wchar_t[tmp.length() + 1];
		rage::safecpy(wStr, tmp.c_str(), tmp.length() + 1);
		return RgscWideString::point_to(wStr, tmp.length(), false);
	}

	void toJSON_free(RgscWideString returnedValue)
	{
		delete [] returnedValue.data();
	}

} // namespace rgsc
