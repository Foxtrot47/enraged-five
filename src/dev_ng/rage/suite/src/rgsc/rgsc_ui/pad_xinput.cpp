// 
// input/pad_xenon.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "pad.h"

#if RGSC_XINPUT_SUPPORT 

#include "system/xtl.h"

#include <xinput.h>

#if __D3D11_1
#pragma comment(lib,"XInput9_1_0.lib")
#else
#pragma comment(lib,"xinput.lib")
#endif // __D3D11_1

namespace rgsc
{

static inline u8 RemapAxis(short axis) 
{ 
	return (u8)((axis >> 8) + 128); 
}

u8 xboxAnalogThreshold = 64;
static bool AnaDown(u8 b) 
{ 
	return b >= xboxAnalogThreshold; 
}

void ioXInputPad::Update() 
{
	ClearInputs();

	if (!m_IsConnected)
		return;

	XINPUT_STATE state;
	DWORD result = XInputGetState(m_PadIndex, &state);

	if (result != ERROR_SUCCESS) 
	{
		m_IsConnected = false;
		return;
	}

	m_IsConnected = true;

	XINPUT_GAMEPAD &pad = state.Gamepad;

	// Copy analog axes
	m_Axis[0] = RemapAxis(pad.sThumbLX);
	m_Axis[1] = (u8) ~RemapAxis(pad.sThumbLY);
	m_Axis[2] = RemapAxis(pad.sThumbRX);
	m_Axis[3] = (u8) ~RemapAxis(pad.sThumbRY);

	if (pad.wButtons & XINPUT_GAMEPAD_A) m_Buttons |= RGSC_RDOWN;
	if (pad.wButtons & XINPUT_GAMEPAD_B) m_Buttons |= RGSC_RRIGHT;
	if (pad.wButtons & XINPUT_GAMEPAD_X) m_Buttons |= RGSC_RLEFT;
	if (pad.wButtons & XINPUT_GAMEPAD_Y) m_Buttons |= RGSC_RUP;

	if (pad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) m_Buttons |= RGSC_L1;
	if (pad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) m_Buttons |= RGSC_R1;

	if (AnaDown(pad.bLeftTrigger)) m_Buttons |= RGSC_L2;
	if (AnaDown(pad.bRightTrigger)) m_Buttons |= RGSC_R2;

	if (pad.wButtons & XINPUT_GAMEPAD_START) m_Buttons |= RGSC_START;
	if (pad.wButtons & XINPUT_GAMEPAD_BACK) m_Buttons |= RGSC_SELECT;
	if (pad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB) m_Buttons |= RGSC_L3;
	if (pad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB) m_Buttons |= RGSC_R3;

	// Translate DPAD
	if (pad.wButtons & XINPUT_GAMEPAD_DPAD_UP) m_Buttons |= RGSC_LUP;
	if (pad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) m_Buttons |= RGSC_LDOWN;
	if (pad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) m_Buttons |= RGSC_LLEFT;
	if (pad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) m_Buttons |= RGSC_LRIGHT;

	m_AnalogButtons[RGSC_L2_INDEX] = pad.bLeftTrigger;
	m_AnalogButtons[RGSC_R2_INDEX] = pad.bRightTrigger;
}

void ioXInputPad::Refresh() 
{
	XINPUT_STATE state;
	if (!IsConnected())
	{
		bool isConnected = (XInputGetState(m_PadIndex, &state) == ERROR_SUCCESS);
		SetIsConnected(isConnected);
	}
}

} // namespace rgsc

#endif // RGSC_XINPUT_SUPPORT 