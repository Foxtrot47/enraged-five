//
// system/timer.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef RGSC_TIMER_H
#define RGSC_TIMER_H

// The raw data type used for time values in timers
typedef signed __int64 utimer_t;
typedef unsigned short u16;
typedef unsigned int u32;

// Constants for use by the system timer
struct sysTimerConsts
{
	// Conversion factor between ticks and seconds
	static float TicksToSeconds;
	
	// Conversion factor between ticks and milliseconds
	static float TicksToMilliseconds;

	// Conversion factor between ticks and microseconds
	static float TicksToMicroseconds;

	// The speed of the CPU
	static float CpuSpeed;
};

/*
PURPOSE
	This class implements a timer object.
	Useful for profiling. 

NOTES
	For a platform without a fixed CPU speed (e.g. __WIN32PC) the sysTimerConsts are zero until the first
	sysTimer object is constructed, at which point the CPU speed is measured. It is not measured again
	on subsequent constructions.

SEE ALSO:
	sysTimeManger, TIME
<FLAG Component>
*/
class sysTimer {
public:
	//PURPOSE: Creates a new sysTimer.
	//NOTES:
	//	The timer is reset on construction, so time is measured since the timer is created.
	//	On platforms without a fixed CPU speed, the first call to this function will measure the CPU speed.
	//  (and sleeps approximately 100ms)
	sysTimer();

	//PURPOSE: Resets the timer
	void	Reset();

	//PURPOSE: Return the time since construction or last Reset in seconds.
	//RETURNS: Time in seconds.
	float	GetTime() const;

	//PURPOSE: Return the time since construction or last Reset in seconds and reset the timer.
	//RETURNS: Time in seconds.
	float	GetTimeAndReset();

	//PURPOSE: Return the time since construction or last Reset in milli-seconds.
	//RETURNS: Time in milli-seconds.
	float	GetMsTime() const;

	//PURPOSE: Return the time since construction or last Reset in milli-seconds and reset the timer.
	//RETURNS: Time in milli-seconds.
	float	GetMsTimeAndReset();

	//PURPOSE: Return the time since construction or last Reset in micro-seconds.
	//RETURNS: Time in micro-seconds.
	float	GetUsTime() const;

	//PURPOSE: Return the time since construction or last Reset in micro-seconds and reset the timer.
	//RETURNS: Time in micro-seconds.
	float	GetUsTimeAndReset();

	//PURPOSE: Return the amt of ticks since construction or last Reset.
	//RETURNS: Delta in num ticks.
	utimer_t	GetTickTime() const;

	// PURPOSE
	//	Returns number of milliseconds elapsed since some point in time
	//	(typically system startup, but you shouldn't count on that)
	// NOTES
	//	Higher-level code should be able to assume that this function
	//	will never overflow during a single run.  Overflow will happen
	//	after 1193 hours (nearly fifty days).
	static u32 GetSystemMsTime();

	//PURPOSE
	//	Begin the benchmark. Raises process priority temporarily so that timer 
	//  results are more accurate.  This is useful for testing a small subroutine.
	//NOTES
	//	Only works on <c>__WIN32</c> platforms
	static void BeginBenchmark();

	//PURPOSE: End the benchmark. Restores process priority to previous value. 
	//NOTES
	//	Only works on <c>__WIN32</c> platforms
	static void EndBenchmark();

	// PURPOSE: Returns the conversion factor for converting from ticks to seconds.
	static float GetTicksToSeconds();

	// PURPOSE: Returns the conversion factor for converting from ticks to milliseconds.
	static float GetTicksToMilliseconds();

	// PURPOSE: Returns the conversion factor for converting from ticks to microseconds.
	static float GetTicksToMicroseconds();

	// PURPOSE: Returns the CPU speed in MHz.
	// RETURNS: CPU speed, or 0.0f if speed is unknown.
	static float GetCpuSpeed();

	// PURPOSE: Returns the raw tick count. On many systems this is a cycle count
	static utimer_t GetTicks();

private:
	// unsigned instead of ulong because PSX2 ulong is 64 bits
	// but the timer hardware is only 32 bits.
	volatile utimer_t m_Start;
};

inline void sysTimer::Reset()
{
	m_Start=GetTicks();
}

inline float sysTimer::GetTicksToSeconds()
{
	return sysTimerConsts::TicksToSeconds;
}

inline float sysTimer::GetTicksToMilliseconds()
{
	return sysTimerConsts::TicksToMilliseconds;
}

inline float sysTimer::GetTicksToMicroseconds()
{
	return sysTimerConsts::TicksToMicroseconds;
}

inline float sysTimer::GetCpuSpeed()
{
	return sysTimerConsts::CpuSpeed;
}

inline float sysTimer::GetTime() const
{
	// It's safe to ignore any carry here.
	utimer_t delta = GetTicks() - m_Start;
	return delta * sysTimerConsts::TicksToSeconds;
}

inline float sysTimer::GetTimeAndReset()
{
	// It's safe to ignore any carry here.
	utimer_t now = GetTicks();
	utimer_t delta = now - m_Start;
	m_Start = now;
	return delta * sysTimerConsts::TicksToSeconds;
}

inline float sysTimer::GetMsTime() const
{
	// It's safe to ignore any carry here.
	utimer_t delta = GetTicks() - m_Start;
	return delta * sysTimerConsts::TicksToMilliseconds;
}

inline float sysTimer::GetMsTimeAndReset()
{
	// It's safe to ignore any carry here.
	utimer_t now = GetTicks();
	utimer_t delta = now - m_Start;
	m_Start = now;
	return delta * sysTimerConsts::TicksToMilliseconds;
}

inline float sysTimer::GetUsTime() const
{
	// It's safe to ignore any carry here.
	utimer_t delta = GetTicks() - m_Start;
	return delta * sysTimerConsts::TicksToMicroseconds;
}

inline utimer_t sysTimer::GetTickTime() const
{
	utimer_t delta = GetTicks() - m_Start;
	return delta;
}

inline float sysTimer::GetUsTimeAndReset()
{
	// It's safe to ignore any carry here.
	utimer_t now = GetTicks();
	utimer_t delta = now - m_Start;
	m_Start = now;
	return delta * sysTimerConsts::TicksToMicroseconds;
}

#endif // RGSC_TIMER_H
