#ifndef RGSC_GRAPHICS_D3D9_H
#define RGSC_GRAPHICS_D3D9_H

#include "graphics.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <d3d9.h>
#pragma warning(pop)

namespace rgsc
{

class Rgsc_Graphics_D3D9 : public Rgsc_Graphics
{
public:
	Rgsc_Graphics_D3D9(IDirect3DDevice9* device, void* params, const RenderRect& rect);
	~Rgsc_Graphics_D3D9();
	HRESULT Init(IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* params, const RenderRect& rect);
	void Shutdown();
	void Render(bool onlyNotifications);
	bool OnLost();
	bool OnReset(void *params, const RenderRect& rect);
	void SetContext(void* /*context*/) {}
	HWND GetWindowHandle();
	unsigned int GetWidth();
	unsigned int GetHeight();
	bool MapMouse(int &x, int &y);
private:
	HRESULT CreateResources();
	void ReleaseResources();
	HRESULT CreateTextures();
	void SetRenderStates();
	void UpdateTextures(bool onlyNotifications);

	IDirect3DDevice9* m_Device;
	IDirect3DTexture9** m_Textures;
	IDirect3DVertexBuffer9* m_VertexBuffer;
	IDirect3DVertexShader9* m_VertexShader;
	IDirect3DPixelShader9* m_PixelShader;
	IDirect3DStateBlock9* m_FirstStateBlock;
	IDirect3DStateBlock9* m_OldStateBlock;
	IDirect3DStateBlock9* m_StateBlock;
};

} // namespace rgsc

#endif //RGSC_GRAPHICS_D3D9_H
