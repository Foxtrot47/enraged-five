#define _WINSOCKAPI_ //Prevent windows.h from including winsock.h
#pragma warning(push)
#pragma warning(disable: 4668)
#include <windows.h>
#include <shlobj.h>
#pragma warning(pop)

#include "../diag/output.h"
#include "diag/seh.h"
#include "rline/rltitleid.h"
#include "system/timer.h"
#include "rgsc_ui.h"
#include "rgscwindow.h"
#include "cursor.h"
#include "graphics.h"
#include "input.h"
#include "eventq.h"
#include "script.h"
#include "rgsc_common.h"
#include "rgsc_messages.h"

// #include "system/main.h"
#include "diag/diagerrorcodes.h"
#include "file/device.h"
#include "string/unicode.h"
#include "rline/ros/rlros.h"
#include "rline/socialclub/rlsocialclub.h"
#include "string/stringutil.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <tpcshrd.h>
#pragma warning(pop)

namespace rage
{
	extern const rlTitleId* g_rlTitleId;
#if __DEV
	NOSTRIP_PC_XPARAM(nethttpproxy);
#endif
}

namespace rgsc
{

#if __DEV
PARAM(scuiurl, "full url of the social club ui website");
PARAM(nosandbox, "disables the Chrome sandbox. This is needed to run the game through Pix for Windows.");
PARAM(noCaptchaHeader, "Header Name to pass to SCUI to disable vCaptcha/Recaptcha checks during login");
PARAM(noCaptchaHeaderValue, "Header Value to pass to SCUI to disable vCaptcha/Recaptcha checks during login");
PARAM(scshowpaintrects, "Enables Chrome's command lines to shows paint and damage rects");
PARAM(scremotedebuggingport, "Enables Chrome's remote debugging tools");
PARAM(scuiIgnoreCertErrors, "Ignores cert errors in the SCUI");
#endif

NOSTRIP_PC_PARAM(noScuiLoadTimeouts, "Disables timeouts loading SCUI web site.");
NOSTRIP_PC_PARAM(scuiurlpath, "relative path of the social club ui website");
NOSTRIP_PC_PARAM(scDebugLogging, "Enables debug logging to the retail output from SCUI");
NOSTRIP_PC_PARAM(scEnableGpu, "Enables GPU hardware acceleration");
NOSTRIP_PC_PARAM(scNoSoftwareRasterizer, "Disables software rasterizer");

#define ALLOW_ANY_WEB_SITE __DEV
#define DEBUG_CURSOR __DEV && 0

// max number of times we will attempt to reload the page if an error occurs during loading
#define MAX_PAGE_RELOAD_ATTEMPTS 1

// time interval between subprocess reboots
#define SUBPROCESS_REBOOT_INTERVAL_MS (10 * 1000)

#define IPC_SEND(x) GetRgscConcreteInstance()->_GetUiInterface()->_GetIpc()->Send(x)

extern sysMemAllocator* g_RgscAllocator;

RGSC_HRESULT RgscUi::QueryInterface(RGSC_REFIID riid, LPVOID* ppvObj)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObj == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IRgscUi*>(this);
	}
	else if(riid == IID_IRgscUiV1)
	{
		pUnknown = static_cast<IRgscUiV1*>(this);
	}
	else if(riid == IID_IRgscUiV2)
	{
		pUnknown = static_cast<IRgscUiV2*>(this);
	}
	else if(riid == IID_IRgscUiV3)
	{
		pUnknown = static_cast<IRgscUiV3*>(this);
	}
	else if (riid == IID_IRgscUiV4)
	{
		pUnknown = static_cast<IRgscUiV4*>(this);
	}
	else if (riid == IID_IRgscUiV5)
	{
		pUnknown = static_cast<IRgscUiV5*>(this);
	}
	else if (riid == IID_IRgscUiV6)
	{
		pUnknown = static_cast<IRgscUiV6*>(this);
	}
	else if (riid == IID_IRgscUiV7)
	{
		pUnknown = static_cast<IRgscUiV7*>(this);
	}
	else if (riid == IID_IRgscUiV8)
	{
		pUnknown = static_cast<IRgscUiV8*>(this);
	}
	else if (riid == IID_IRgscUiV9)
	{
		pUnknown = static_cast<IRgscUiV9*>(this);
	}
	else if (riid == IID_IRgscUiV10)
	{
		pUnknown = static_cast<IRgscUiV10*>(this);
	}
	else if (riid == IID_IRgscUiV11)
	{
		pUnknown = static_cast<IRgscUiV11*>(this);
	}

	*ppvObj = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

bool RgscUi::InitBrowserCache()
{
	bool success = false;

	rtry
	{
		char path[RGSC_MAX_PATH];
		rverify(GetRgscConcreteInstance()->_GetFileSystem()->GetPlatformCacheDirectory(path, true), catchall, );

		safecat(path, "browser_cache_settings.dat");

		const fiDevice *device = fiDevice::GetDevice(path, false);
		rverify(device, catchall, );

		char version[128] = {0};
		char fileVersion[128] = {0};
		rverify(GetRgscConcreteInstance()->GetVersionInfo(version), catchall, );

		fiHandle hFile = device->Open(path, false);
		if(fiIsValidHandle(hFile))
		{
			device->Read(hFile, fileVersion, sizeof(fileVersion));
			if(_stricmp(version, fileVersion) == 0)
			{
				// same version, no need to delete the cache
				device->Close(hFile);
				return true;
			}

			// seek to the beginning of the file so we can overwrite the version
			device->Seek(hFile, 0, fiSeekWhence::seekSet);
		}
		else
		{
			hFile = device->Create(path);
			rverify(fiIsValidHandle(hFile), catchall, );
		}

		// write version number
		int bufferSize = (int)strlen(version) + 1;
		int bytesWritten = device->Write(hFile, &version, bufferSize);
		rverify(bytesWritten == bufferSize, catchall, );
		device->Close(hFile);

		success = true;
	}
	rcatchall
	{

	}

	// delete the browser cache for both the launcher and the game
	// NOTE:
	//	This code is only called if the 'early out' above fails (i.e. there is a new version of the DLL)
	char browserCachePath[RGSC_MAX_PATH] = {0};
	if(GetRgscConcreteInstance()->_GetFileSystem()->GetBrowserCacheDirectory(browserCachePath, true, false))
	{
		fiDevice::DeleteDirectory(browserCachePath, true);
	}

	if(GetRgscConcreteInstance()->_GetFileSystem()->GetBrowserCacheDirectory(browserCachePath, false, false))
	{
		fiDevice::DeleteDirectory(browserCachePath, true);
	}

	return success;
}

void RgscUi::InsertMessageHandler(RgscPlatformMessageHandler::ResponseBehaviour scuiVisibleBehaviour, RgscPlatformMessageHandler::ResponseBehaviour scuiHiddenBehaviour, 
						  RgscPlatformMessageHandler::RgscPlatformMsg platformMsg, s64 scuiVisibleReturnValue, s64 scuiHiddenReturnValue)
{
	RgscPlatformMessageHandler* v1 = NULL;

	// Lookup a previous handler for this message
	RgscPlatformMessageHandler** v1ptr = m_MsgMap.Access(platformMsg);
	if (v1ptr)
	{
		v1 = *v1ptr;
	}
	else
	{
		v1 = rage_new RgscPlatformMessageHandler();
		m_MsgMap.Insert(platformMsg, v1);
	}

	// Ensure the lookup or allocation succeeded
	if (v1)
	{
		v1->SetBehaviours(scuiVisibleBehaviour, scuiHiddenBehaviour);
		v1->SetReturnValues(scuiVisibleReturnValue, scuiHiddenReturnValue);
		v1->SetMessageToHandle(platformMsg);
	}
}

void RgscUi::InsertMessageRangeHandler(RgscPlatformMessageHandler::ResponseBehaviour scuiVisibleBehaviour, RgscPlatformMessageHandler::ResponseBehaviour scuiHiddenBehaviour, 
							   RgscPlatformMessageHandler::RgscPlatformMsg start, RgscPlatformMessageHandler::RgscPlatformMsg end, s64 scuiVisibleReturnValue, s64 scuiHiddenReturnValue)
{
	RgscPlatformMessageHandler* v1 = NULL;

	// Lookup a previous handler for this message
	RgscPlatformMessageHandler** v1ptr = m_MsgRangeMap.Access(start);
	if (v1ptr)
	{
		v1 = *v1ptr;
	}
	else
	{
		v1 = rage_new RgscPlatformMessageHandler();
		m_MsgRangeMap.Insert(start, v1);
	}

	// Ensure the lookup or allocation succeeded
	if (v1)
	{
		v1->SetBehaviours(scuiVisibleBehaviour, scuiHiddenBehaviour);
		v1->SetReturnValues(scuiVisibleReturnValue, scuiHiddenReturnValue);
		v1->SetMessageRange(start, end);
		m_MsgRangeMap.Insert(start, v1);
	}
}

void RgscUi::InitDefaultMessageHandlers()
{
	// Forward DEVICECHANGE messages to the game in all cases.
	InsertMessageHandler(RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_DEVICECHANGE);

	// Forward WM_KEYUP, WM_KEYDOWN and WM_CHAR messages when the SCUI is not visible. Handle them when the SCUI is visible.
	// Note: When the SCUI is not visible and the SCUI HotKey is pressed, this message will not be forwarded. This logic exists in Rgsc_Input.
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_KEYUP, 0, 0);
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_KEYDOWN, 0, 0);
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_CHAR, 0, 0);

	// IME messages are handled when the SCUI is visible, except RGSC_MSG_IME_SETCONTEXT which is always forwarded
	InsertMessageHandler(RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_IME_SETCONTEXT, 0, 0);
	
	// IME messages are handled when the SCUI is visible
	// If the SCUI is not IME aware, always use the OS UI.
	// If the SCUI is IME aware, only use the OS UI if 'IME_USE_OS_UI' is defined.
	bool useOsUi = (!IME_TEXT_INPUT) || (IME_TEXT_INPUT && !IME_USE_OS_UI);
	RgscPlatformMessageHandler::ResponseBehaviour imeBehaviour = useOsUi ? RgscPlatformMessageHandler::FORWARD : RgscPlatformMessageHandler::RETURN_VALUE;
	InsertMessageHandler(imeBehaviour, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_IME_STARTCOMPOSITION, 0, 0);
	InsertMessageHandler(imeBehaviour, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_IME_COMPOSITION, 0, 0);
	InsertMessageHandler(imeBehaviour, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_IME_ENDCOMPOSITION, 0, 0);
	InsertMessageHandler(imeBehaviour, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_IME_NOTIFY, 0, 0);
	InsertMessageHandler(imeBehaviour, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_IME_CHAR, 0, 0);

	// Mouse Events are handled when the SCUI is visible
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_MOUSEMOVE, 0, 0);
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_MOUSEWHEEL, 0, 0);
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_TABLET_QUERYSYSTEMGESTURESTATUS, 0, TABLET_DISABLE_PRESSANDHOLD);
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_LBUTTONDOWN, 0, 0);
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_LBUTTONDBLCLK, 0, 0);
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_LBUTTONUP, 0, 0);
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_RBUTTONDOWN, 0, 0);
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_RBUTTONDBLCLK, 0, 0);
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_RBUTTONUP, 0, 0);
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_MBUTTONDOWN, 0, 0);
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_MBUTTONUP, 0, 0);
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_MBUTTONDBLCLK, 0, 0);
	InsertMessageHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_SETCURSOR, 0, 0);

	// All remaining mouse values in range are handled when the SCUI is visible
	InsertMessageRangeHandler(RgscPlatformMessageHandler::RETURN_VALUE, RgscPlatformMessageHandler::FORWARD, RgscPlatformMessageHandler::RGSC_MSG_MOUSEFIRST, RgscPlatformMessageHandler::RGSC_MSG_MOUSELAST, 0, 0);
}

RgscUi::RgscUi()
: m_Initialized(false)
, m_Window(NULL)
, m_WindowType(INVALID)
, m_BrowserHandle(0)
, m_WindowHandle(0)
, m_Device(NULL)
, m_WasUiJustShown(false)
, m_WasUiJustHidden(false)
, m_ReadyToAcceptCommands(false)
, m_PongReceived(false)
, m_ScuiPageLoadError(false)
, m_JavascriptReadyToAcceptCommands(false)
, m_ScUiVisible(false)
, m_WaitingForUiStateResponse(false)
, m_WaitingForUiToBecomeVisible(false)
, m_AccountLinkingCallback(NULL)
, m_State(STATE_INVALID)
, m_LoadingStatus(LS_NOT_LOADED)
, m_HotkeyEnabled(true)
, m_ClosingEnabled(true)
, m_TextboxHasFocus(false)
, m_ForwardExternalUrls(false)
, m_ReloadingUi(false)
, m_SignInOnReload(false)
, m_SignOnlineOnReload(false)
, m_TransitioningToOnline(false)
, m_NumNotificationsVisible(0)
, m_AutoSignInStartTime(0)
, m_LastJavascriptPingTime(0)
, m_SignInError(false)
, m_NumPageReloadAttempts(0)
, m_NonScuiUrl(false)
, m_SendWelcomeNotification(true)
, m_ShowUiAfterSiteLoad(false)
, m_SendConnectionLostNotification(false)
, m_UiScaleFactor(1.0f)
, m_UserDefinedScaled(false)
, m_bIsModal(false)
, m_bIsWindowDestroyed(false)
, m_WindowConfig(NULL)
, m_WindowConfigV1(NULL)
, m_WindowConfigV2(NULL)
, m_uShutdownTimeoutMs(0)
, m_uShutdownStartTime(0)
, m_uLastSubprocessRebootTime(0)
, m_UiVisibleOnSubprocessReboot(-1)
, m_bWaitingForScuiShutdown(false)
, m_bHibernating(false)
, m_KeyCallback(NULL)
{
	memset(m_CurrentUrl, 0, sizeof(m_CurrentUrl));
	memset(m_OnlineUrl, 0, sizeof(m_OnlineUrl));
	m_StateLoadTimeout.Clear();
}

RGSC_HRESULT RgscUi::Init()
{
	InitBrowserCache();
	InitDefaultMessageHandlers();

	char channelName[Ipc::MAX_CHANNEL_NAME_LEN] = {0};
	DWORD pid = GetCurrentProcessId();
	rage::formatf(channelName, "rgsc_ipc_%x", pid);

	m_Ipc.Init(Ipc::MODE_SERVER, channelName);
	m_IpcDelegate.Bind(this, &RgscUi::OnMessageReceived);
	m_Ipc.AddDelegate(&m_IpcDelegate);

	RGSC_HRESULT hr = InitSubProcess();
	if(FAILED(hr))
	{
		return hr;
	}

	// Setup default modal color as dark, 25% transparency.
	m_ModalColor.Set((int)0, (int)0, (int)0, (int)64);

	m_ActivationSystem.Init();
	m_PatchingSystem.Init();

	m_Initialized = true;

	return RGSC_OK;
}

void RgscUi::Shutdown()
{
	SYS_CS_SYNC(m_Cs);

	m_PatchingSystem.Shutdown();
	m_ActivationSystem.Shutdown();

	if (Rgsc_Input::IsInputEnabled())
	{
		Rgsc_Input::Shutdown();
	}

	if(m_Window)
	{
		delete m_Window;
		m_Window = NULL;
	}

	if(m_Device)
	{
		delete m_Device;
		m_Device = NULL;
	}

	rgsc::Script::Shutdown();

	m_Ipc.RemoveDelegate(&m_IpcDelegate);
	m_Ipc.Shutdown();

	m_MsgMap.Kill();
	m_MsgRangeMap.Kill();
}

RGSC_HRESULT RgscUi::InitSubProcess()
{
	char path[RGSC_MAX_PATH] = {0};
	if(!GetRgscConcreteInstance()->_GetFileSystem()->GetBrowserCacheDirectory(path, GetRgscConcreteInstance()->IsLauncher(), true))
	{
		return RGSC_FAIL;
	}

	RgscDisplayUtf8("Browser Cache Directory: %s", path);
	
	// need to disable the sandbox on OnLive and Mac (Transgaming) platforms
	// UPDATE (September 2014): always disable the sandbox as it randomly prevents some people from loading the SCUI
	bool disableSandbox = true;
	ITitleId::Platform platform = GetRgscConcreteInstance()->GetTitleId()->GetPlatform();
	if((platform == ITitleId::PLATFORM_ONLIVE) || 
	   (platform == ITitleId::PLATFORM_MAC_STEAM) ||
	   (platform == ITitleId::PLATFORM_MAC_APP_STORE))
	{
		disableSandbox = true;
	}

	// proxy detection can be very slow on some networks. We decided to disable automatic proxy
	// detection since most people don't use a proxy or their proxy is passive and doesn't need this.
	// rage head compiles all param_get() calls out in final, so find the proxy param manually since
	// this commandline is available in retail builds.
	bool enableProxyDetection = false;
	for(int i = 0; i < sysParam::GetArgCount(); i++)
	{
		const char* arg = sysParam::GetArg(i);
		if(stricmp(arg, "-scdetectproxy") == 0 || stricmp(arg, "-fiddler") == 0)
		{
			enableProxyDetection = true;
		}
		else if(stricmp(arg, "-scnosandbox") == 0)
		{
			disableSandbox = true;
		}
		else if(stricmp(arg, "-pix") == 0)
		{
			disableSandbox = true;
		}
	}

#if __DEV
	char szChromeCommandLine[256] = {0};
	const char* proxy = NULL;
	if(PARAM_nethttpproxy.Get(proxy))
	{
		safecatf(szChromeCommandLine, "--proxy-server=%s ", proxy);
	}
	else if(enableProxyDetection == false)
	{
		safecatf(szChromeCommandLine, "--no-proxy-server ");
	}

	const char* noCaptchaHeader, *noCaptchaHeaderValue;
	if(PARAM_noCaptchaHeader.Get(noCaptchaHeader) && PARAM_noCaptchaHeaderValue.Get(noCaptchaHeaderValue))
	{
		safecatf(szChromeCommandLine, "--no-captcha-header=%s ", noCaptchaHeader);
		safecatf(szChromeCommandLine, "--no-captcha-header-value=%s ", noCaptchaHeaderValue);
	}

	if(PARAM_nosandbox.Get())
	{
		disableSandbox = true;
	}
#else
	char szChromeCommandLine[256] = {0};
	if(enableProxyDetection == false)
 	{
 		safecatf(szChromeCommandLine, "--no-proxy-server ");
 	}
#endif

#if RSG_CPU_X64
	// This will be the default behaviour in Chrome 65/CEF3325+ branches, and can be removed when we upgrade.
	// This fixes a bug with page reload to the same URL.
	safecatf(szChromeCommandLine, "--enable-browser-side-navigation ");
#endif

	// safe browsing was causing an assert to go off in the depths of Chrome
	// which led to a ton of IPC messages (each time the assert was hit)
	// which made the DLL run out of memory and crash the game.
	// We control the URLs we hit, so we can disable this.
	safecatf(szChromeCommandLine, "--safebrowsing-disable-auto-update ");
	safecatf(szChromeCommandLine, "--disable-spell-checking ");

	// disable extensions (so far only seems to be used for PDFs)
	//	added in cef b7a56d9 (issue 1565)
	// fixes spam in debug.log:
	//	[0819/143611:ERROR:main_delegate.cc(766)] Could not load cef_extensions.pak
	safecatf(szChromeCommandLine, "--disable-extensions ");

#if __DEV
	if(PARAM_scshowpaintrects.Get())
	{
		safecatf(szChromeCommandLine, "--show-paint-rects ");
		safecatf(szChromeCommandLine, "--show-surface-damage-rects  ");
		// 	safecatf(szChromeCommandLine, "--show-property-changed-rects ");
		// 	safecatf(szChromeCommandLine, "--show-property-changed-rects ");
		// 	safecatf(szChromeCommandLine, "--show-screenspace-rects ");
		// 	safecatf(szChromeCommandLine, "--show-composited-layer-borders ");
		// 	safecatf(szChromeCommandLine, "--show-layer-animation-bounds ");
		// 	safecatf(szChromeCommandLine, "--show-nonoccluding-rects ");
		// 	safecatf(szChromeCommandLine, "--show-occluding-rects ");
	}

	u32 remoteDebuggingPort = 0;
	if(PARAM_scremotedebuggingport.Get(remoteDebuggingPort))
	{
		safecatf(szChromeCommandLine, "--remote-debugging-port=%u ", remoteDebuggingPort);
	}
#endif
	
	// GPU acceleration actually slows the game down when animations are being played (such as notifications/toasts)
	// This might be a bug in the current version of the CEF/Chrome. 
	// Note: Recent (x64) builds no longer seem to require this - but we can enable using -scNoGpu
	if (!PARAM_scEnableGpu.Get())
	{
		safecatf(szChromeCommandLine, "--disable-gpu ");
		safecatf(szChromeCommandLine, "--disable-gpu-compositing ");
	}

	// Disables the 3D software rasterizer, preventing the 'type=gpu-process' subprocess from being created.
	if (PARAM_scNoSoftwareRasterizer.Get())
	{
		safecatf(szChromeCommandLine, "--disable-software-rasterizer ");
	}

	// need to tell Chrome which locale we're using so that the correct glyphs are displayed
	// (japanese vs chinese glyphs that use the same unicode value for example).
	// Chrome requires two-character ISO 639 language codes.
	sysLanguage language = rlGetLanguage();
	const char* languageCode = "en";
	switch(language)
	{
	case LANGUAGE_ENGLISH:
		languageCode = "en";
		break;
	case LANGUAGE_SPANISH:
		languageCode = "es";
		break;
	case LANGUAGE_FRENCH:
		languageCode = "fr";
		break;
	case LANGUAGE_GERMAN:
		languageCode = "de";
		break;
	case LANGUAGE_ITALIAN:
		languageCode = "it";
		break;
	case LANGUAGE_JAPANESE:
		languageCode = "ja";
		break;
	case LANGUAGE_RUSSIAN:
		languageCode = "ru";
		break;
	case LANGUAGE_PORTUGUESE:
		languageCode = "pt";
		break;
	case LANGUAGE_POLISH:
		languageCode = "pl";
		break;
	case LANGUAGE_KOREAN:
		languageCode = "ko";
		break;
	case LANGUAGE_CHINESE_TRADITIONAL:
		languageCode = "zh-TW";
		break;
	case LANGUAGE_CHINESE_SIMPLIFIED:
		languageCode = "zh-CN";
		break;
	case LANGUAGE_MEXICAN:
		languageCode = "es";
		break;
	default:
		Assertf(false, "unknown language specified: %d", language);
		break;
	}

	safecatf(szChromeCommandLine, "--lang=%s ", languageCode);
	
#if __DEV
	// requested by Transgaming
	char* envParams = getenv("RGSC_CHROME_PARAMS");
	if(envParams && envParams[0] != '\0')
	{
		safecat(szChromeCommandLine, envParams);
		safecat(szChromeCommandLine, " ");
	}

	if (PARAM_scuiIgnoreCertErrors.Get())
	{
		safecat(szChromeCommandLine, "--rgsc_ignore_certificate_errors ");
	}
#endif

	RGSC_HRESULT subprocessHr = StartSubProcess(path, szChromeCommandLine);
	if(FAILED(subprocessHr))
	{
		return subprocessHr;
	}

	return RGSC_OK;
}

RGSC_HRESULT RgscUi::StartSubProcess(const char* cachePath, const char* chromeCommandLine)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		if(m_Ipc.GetNumChannels() == 0)
		{
			std::string channelName;
			CreateIpcChannel(&channelName);
		}

		DWORD childProcessCreationFlags = 0;

		char subprocessPath[RGSC_MAX_PATH] = {0};	
		rverify(GetRgscConcreteInstance()->_GetFileSystem()->GetSubprocessPath(subprocessPath), catchall, );

		STARTUPINFOW si;

		ZeroMemory(&si, sizeof(si));
		si.cb = sizeof(si);
		si.dwFlags = STARTF_USESHOWWINDOW;
		si.wShowWindow = SW_HIDE;

		ZeroMemory(&m_SubprocessInfo, sizeof(m_SubprocessInfo));

		// check file exists
		const fiDevice *device = fiDevice::GetDevice(subprocessPath);
		if(device && (device->GetAttributes(subprocessPath) == FILE_ATTRIBUTE_INVALID))
		{
			rlDebug2("Subprocess does not exist in: %s", subprocessPath);
			return E_HANDLE;
		}

		rlDebug2("Subprocess exists in: %s", subprocessPath);

		// check version number
		char dllVersion[128] = {0};
		char subProcessVersion[128] = {0};
		if(GetRgscConcreteInstance()->GetVersionInfo(dllVersion) &&
			GetRgscConcreteInstance()->GetModuleVersionInfo(subprocessPath, subProcessVersion) &&
			(stricmp(dllVersion, subProcessVersion) != 0))
		{
			rlDebug2("Subprocess is out of date. Version %s. Requires version: %s", subProcessVersion, dllVersion);
			return RGSC_NOINTERFACE;
		}

 		char commandLine[4096] = {0};

		// TODO: NS - base64-encode the home dir instead of this
		size_t pos = 0;
		std::string tmp(cachePath);
		std::string toFind(" ");
		std::string replacement("*%20*"); // can't have a * in a real filename so using *%20* as a placeholder

		while(true)
		{
			pos = tmp.find(toFind, pos);
			if(pos != std::string::npos)
			{
				tmp.replace(pos, toFind.length(), replacement);
				pos += replacement.length();
			}
			else
			{
				break;
			}
		}
		
		formatf(commandLine, "\"%s\" --rgsc_pid=%u --rgsc_ipc_channel_name=%s --rgsc_home_dir=%s %s", subprocessPath, GetCurrentProcessId(), m_Ipc.GetChannelName(), tmp.c_str(), chromeCommandLine);
		rlDebug2("Creating subprocess: %s", commandLine);

		wchar_t commandLineW[4096] = {0};
		MultiByteToWideChar(CP_UTF8 , NULL , commandLine, -1, commandLineW, COUNTOF(commandLineW));

		wchar_t subprocessPathW[RGSC_MAX_PATH] = {0};
		MultiByteToWideChar(CP_UTF8 , NULL , subprocessPath, -1, subprocessPathW, COUNTOF(subprocessPathW));

		rverify(CreateProcessW(subprocessPathW,
							   commandLineW,
							   NULL,
							   NULL,
							   FALSE,
							   childProcessCreationFlags,
							   NULL,
							   NULL,
							   &si,
							   &m_SubprocessInfo) != 0, catchall, rlError("CreateProcessW failed."));

		rlDebug2("Subprocess created successfully.");

		hr = RGSC_OK;
	}
	rcatchall
	{
#if !__NO_OUTPUT
		char buf[256] = {0};
		int iErrorCode = GetLastError();
		rlError("RgscUi::StartSubProcess failed error code %d : %s", iErrorCode, diagErrorCodes::Win32ErrorToString(iErrorCode, buf, NELEM(buf)));
#endif
	}

	return hr;
}

RGSC_HRESULT RgscUi::SetRenderRect(unsigned int top, unsigned int left, unsigned int bottom, unsigned int right)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(top >= 0 && left >= 0 && bottom > top && right > left, catchall, );
		rlDebug2("Setting SCUI render rect to (top, left, right, bottom) = (%u, %u, %u, %u)", top, left, right, bottom);
		m_RenderRect.Set(top, left, bottom, right);
		hr = RGSC_OK;
	}
	rcatchall
	{

	}
	return hr;
}

void RgscUi::SetUiScale(float scale)
{
	RgscDisplay("Setting Social Club UI scaling to %.2f", scale);
	m_UiScaleFactor = scale;
	m_UserDefinedScaled = true;
}

bool RgscUi::SetMessageHandlers(IRgscPlatformMessageHandler** handlers, const unsigned numHandlers)
{
	rtry
	{
		RGSC_HRESULT hr;
		for (unsigned i = 0; i < numHandlers; i++)
		{
			IRgscPlatformMessageHandlerV1* v1 = NULL;
			hr = handlers[i]->QueryInterface(IID_IRgscMessageHandlerV1, (void**) &v1);
			rverify(SUCCEEDED(hr) && (v1 != NULL), catchall, rlError("platform message handler doesn't support RgscPlatformMessageHandler interface"));

			if (v1->GetMessageToHandle() > RgscPlatformMessageHandler::RGSC_MSG_NULL)
			{
				rlDebug2("Adding message handler for %u", v1->GetMessageToHandle());
				InsertMessageHandler(v1->GetResponseBehaviour(true), v1->GetResponseBehaviour(false), v1->GetMessageToHandle(), v1->GetReturnValue(true), v1->GetReturnValue(false));
			}
			else
			{
				rlDebug2("Adding message handler from %u to %u", v1->GetStartMessageRange(), v1->GetEndMessageRange());
				InsertMessageRangeHandler(v1->GetResponseBehaviour(true), v1->GetResponseBehaviour(false),  v1->GetStartMessageRange(), v1->GetEndMessageRange(), v1->GetReturnValue(true), v1->GetReturnValue(false));
			}
		}
	}
	rcatchall
	{
		return false;
	}

	return true;
}

bool RgscUi::IsOverridingCursor()
{
	if (m_Window)
	{
		// The cursor type maps to CEF's "cef_cursor_type_t".
		//	The standard arrow pointer is zero.
		//	Ensure that input is enabled, the mouse is in the window and the cursor is non-standard.
		return Rgsc_Input::IsInputEnabled() && 
				Rgsc_Input::IsMouseInWindow() &&
				(m_Window->getCursorType() != RGSC_CURSOR_POINTER);
	}

	return false;
}

void RgscUi::SetWindowMargins(int top, int left, int bottom, int right)
{
	Rgsc_Input::SetWindowMargins(top, left, bottom, right);
}

void RgscUi::SetIsModal(bool isModal)
{
	if (m_Window && m_bIsModal != isModal)
	{
		wchar_t modalJsBuf[512] = {0};
		if (isModal)
		{
			// setup the color
			wchar_t colorBuf[64];
			formatf(colorBuf, L"rgba(%u,%u,%u,%.2f)", m_ModalColor.GetRed(), m_ModalColor.GetGreen(), m_ModalColor.GetBlue(), m_ModalColor.GetAlphaf());

			// setup the modal
			formatf(modalJsBuf, L"document.body.insertAdjacentHTML( 'afterbegin', '<div id=\"__sdk_interjected_modal_dialog\" style=\"position: absolute; top: 0px; left: 0px; width: 100%%; height: 100%%; z-index: 999999; overflow: hidden; background: %ls;\"></div>');", colorBuf);
		}
		else
		{
			formatf(modalJsBuf, L"var __sdk_modal = document.getElementById(\"__sdk_interjected_modal_dialog\"); if (__sdk_modal != null) {__sdk_modal.remove(); }");
		}

		m_Window->CallJavascript(RgscWideString::point_to(modalJsBuf));
	}

	m_bIsModal = isModal;

	// enable/disable resizing when the window is modal
	IPC_SEND(new ViewMsg_OnModalChanged(1, m_bIsModal));
}

bool RgscUi::IsModal()
{
	return m_bIsModal;
}

RGSC_HRESULT RgscUi::CreateInWindow(IRgscUiWindowConfiguration* config)
{
	m_WindowType = CHILD;
	return CreateWindowInternal(config);
}

RGSC_HRESULT RgscUi::CreatePopupWindow(IRgscUiWindowConfiguration* config)
{
	m_WindowType = POPUP;
	return CreateWindowInternal(config);
}

RGSC_HRESULT RgscUi::CreateBorderlessWindow(IRgscUiWindowConfiguration* config)
{
	m_WindowType = BORDERLESS;
	return CreateWindowInternal(config);
}

void RgscUi::SetWindowCanClose(bool bCanClose)
{
	// enable/disable resizing when the window is modal
	IPC_SEND(new ViewMsg_SetWindowCanClose(1, bCanClose));
}

void RGSC_CALL RgscUi::SetWindowModalBlocker(WindowHandle handle)
{
	SetIsModal(handle != 0);

	IPC_SEND(new ViewMsg_SetWindowModalBlocker(1, (u64)handle));
}

RGSC_HRESULT RgscUi::SetBrowserKeyEventCallback(BrowserKeyEventCallback cb)
{
	m_KeyCallback = cb;
	return RGSC_OK;
}

RGSC_HRESULT RgscUi::SendMessageToBrowser(CustomWindowMessages msg, RGSC_WPARAM wParam, RGSC_LPARAM lParam)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(m_BrowserHandle != 0 || m_WindowHandle != 0, catchall, rlError("No valid browser to send to."));
		rverify(msg > MSG_FIRST && msg < MSG_LAST, catchall, rlError("Invalid message"));
		rverify(IPC_SEND(new ViewMsg_OnCustomWindowMessage(1, msg, wParam, lParam)), catchall,);
		hr = RGSC_OK;
	}
	rcatchall
	{
	}
	return hr;
}

void RgscUi::SetDeviceContext(void* d3dDeviceContext)
{
	if (m_Device)
	{
		m_Device->SetContext(d3dDeviceContext);
	}
}

void RgscUi::SetKeyboardEventForwarding(bool bEnabled)
{
	Rgsc_Input::SetKeyboardMessageForwarding(bEnabled);
}

void RgscUi::SetMouseEventForwarding(bool bEnabled)
{
	Rgsc_Input::SetMouseMessageForwarding(bEnabled);
}

void RgscUi::SetVirtualCursor(bool bEnabled)
{
	g_VirtualCursor.SetEnabled(bEnabled);
}

void RgscUi::SetModalBackgroundColor(u8 red, u8 green, u8 blue, u8 alpha)
{
	m_ModalColor.Set((int)red, (int)green, (int)blue, (int)alpha);
}

void RgscUi::SetWindowDimensions(int x, int y, int width, int height)
{
	SetRenderRect(0, 0, height, width);

	WindowHandle hWndToResize;
	if (m_WindowType == BORDERLESS)
	{
		// In Borderless Window mode, the parent window will be valid and must be resized to match the requested dimensions.
		hWndToResize = m_WindowHandle;
	}
	else
	{
		// In CHILD/POPUP mode, the browser window must be resized to the new dimensions (as the parent will be owned by the client).
		hWndToResize = m_BrowserHandle;
	}
	
	if (rlVerify(hWndToResize != 0))
	{
		ViewMsg_Rect_Params rect;
		rect.x = x;
		rect.y = y;
		rect.width = width;
		rect.height = height;

		IPC_SEND(new ViewMsg_OnResizeOrMove(1, rect));

		// SetWindowPos instead of m_Window->Resize. m_Window->Resize resizes the tiled buffer for offscreen rendering.
		::SetWindowPos((HWND)hWndToResize, NULL, x, y, width, height, SWP_NOZORDER);
		::UpdateWindow ((HWND)hWndToResize);
	}
}

void RgscUi::AddCaptionExclusion(int x, int y, int width, int height, int referencePoint)
{
	if (rlVerify(m_Window))
	{
		m_Window->AddCaptionExclusion(x,y,width,height,referencePoint);
	}
}

void RgscUi::ClearCaptionExclusion()
{
	if (rlVerify(m_Window))
	{
		m_Window->ClearCaptionExclusion();
	}
}

void RgscUi::DestroyWindow()
{
	if (!IsWindowDestroyed() && rlVerify(m_Window))
	{
		m_Window->DestroyWindow();
	}

	// Shut down input immediately early.
	// TODO: Multiple window support?
	Rgsc_Input::Shutdown();
}

bool RgscUi::IsWindowDestroyed()
{
	return m_bIsWindowDestroyed;
}

void RgscUi::SetWindowFocus(bool bEnabled)
{
	if (m_Window)
	{
		IPC_SEND(new ViewMsg_OnSetFocus(1, bEnabled));
	}
}

void RgscUi::BeginShutdown(int timeOutMs)
{
	m_uShutdownTimeoutMs = timeOutMs;
	m_uShutdownStartTime = sysTimer::GetSystemMsTime();

	char buf[256] = {0};
	RsonWriter rw;
	rw.Init(buf, sizeof(buf), RSON_FORMAT_JSON);
	rw.Begin(NULL, NULL);
	rw.WriteInt("timeOutMs", timeOutMs);
	rw.End();

	bool succeeded;
	rgsc::Script::JsShutdown(&succeeded, buf);

	m_bWaitingForScuiShutdown = succeeded;
}

void RgscUi::Hibernate()
{
	// reset RGSC UI states
	m_ReadyToAcceptCommands = false;
	m_JavascriptReadyToAcceptCommands = false;
	m_ScUiVisible = false;
	m_TextboxHasFocus = false;
	m_NumNotificationsVisible = false;
	ClearSignInError();
	m_NumPageReloadAttempts = 0;
	m_NonScuiUrl = false;

	if (Rgsc_Input::IsInputEnabled())
	{
		Rgsc_Input::Shutdown();
	}

	IPC_SEND(new ViewMsg_Hibernate());

	if(m_Window)
	{
		delete m_Window;
		m_Window = NULL;
	}

	if(m_Device)
	{
		delete m_Device;
		m_Device = NULL;
	}

	RgscDisplay("Hibernating the user interface");
	m_bHibernating = true;
}

bool RgscUi::IsHibernating()
{
	return m_bHibernating;
}

void RgscUi::WakeUp()
{
	if (AssertVerify(m_bHibernating))
	{
		RgscDisplay("Waking the user interface from hibernation.");
		m_bHibernating = false;
		InitSubProcess();
	}
}

WindowHandle RgscUi::GetBrowserHandle()
{
	return m_BrowserHandle;
}

WindowHandle RgscUi::GetWindowHandle()
{
	return m_WindowHandle;
}

RGSC_HRESULT RgscUi::OnCreateDevice(void* d3d, void* params)
{
	if(GetRgscConcreteInstance()->IsUiDisabled())
	{
		// the socialclub dll can now be run in environments where the scui doesn't exist (during development)
		return RGSC_OK;
	}

	m_Device = Rgsc_Graphics::DeviceFactory(static_cast<IUnknown*>(d3d), params, m_RenderRect);
	m_WindowType = WINDOWLESS; // off-screen rendering

	if(m_Device)
	{
		Rgsc_Input::Init((WindowHandle)m_Device->GetWindowHandle(), GetRgscConcreteInstance()->GetAdditionalWindowHandles(), GetRgscConcreteInstance()->GetNumAdditionalWindowHandles());
		if(!LoadUi(false))
		{
			rlDebug("RgscUi::OnCreateDevice returning RGSC_FAIL (LoadUi() failed).");
			return RGSC_FAIL;
		}

		return RGSC_OK;
	}

	rlDebug("RgscUi::OnCreateDevice returning RGSC_INVALIDARG.");

	return RGSC_INVALIDARG;
}

RGSC_HRESULT RgscUi::OnLostDevice()
{
	SYS_CS_SYNC(m_Cs);

	rlDebug("RgscUi::OnLostDevice()");

	if(sysMemAllocator::IsCurrentSet() == false)
	{
		sysMemAllocator::SetCurrent(*g_RgscAllocator);
	}

	bool success = false;

	if(m_Device)
	{
		g_VirtualCursor.OnLost();

		if(m_Window)
		{
			m_Window->sendSetTiledBuffer(NULL);
		}

		success = m_Device->OnLost();
	}

	if(success)
	{
		return RGSC_OK;
	}
	else
	{
		return RGSC_FAIL;		
	}
}

float RgscUi::GetScaleBasedOnResolution(unsigned int width, unsigned int height)
{
	/*
		I'm applying a scale based on the vertical resolution. Any height less than or equal to 1080
		will remain at scale 1.0. Anything larger will enlarge the SCUI by a factor of Height / 1080.

		Examples:
		My 30" monitor is 2560x1600. So the scale will be 1600/1080 = 1.48x.
		A 4K monitor is 3840x2160. So the scale will be 2160/1080 = 2.0x.

		The max scale factor is capped at 2.0x, so even larger resolutions will be capped to 2.0x.
	*/

	const unsigned MAX_VERTICAL_RESOLUTION_BEFORE_SCALING = 1080;
	const unsigned MIN_WIDTH = 800;

	double scale = 1.0;

	if(height > MAX_VERTICAL_RESOLUTION_BEFORE_SCALING)
	{
		scale = ((double)height) / ((double)MAX_VERTICAL_RESOLUTION_BEFORE_SCALING);
		if(scale > 2.0)
		{
			scale = 2.0;
		}

		// if the scale factor would make the SCUI too wide for the window's width,
		// calculate the scale factor to fit the width of the window instead.
		if((MIN_WIDTH * scale) > width)
		{
			scale = ((double)width) / ((double)MIN_WIDTH);
		}

		if(scale > 2.0)
		{
			scale = 2.0;
		}

		if(scale < 1.0)
		{
			scale = 1.0;
		}
	}

	rlDebug2("Setting SCUI scale factor to %.2f based on window resolution of %d x %d", scale, width, height);

	return (float)scale;
}

RGSC_HRESULT RgscUi::OnResetDevice(void* params)
{
	SYS_CS_SYNC(m_Cs);

	rlDebug("RgscUi::OnResetDevice()");

	bool success = false;
	if(m_Device)
	{
		success = m_Device->OnReset(params, m_RenderRect);
		if(success)
		{
			if(m_Window)
			{
				rlDebug("RgscUi::OnResetDevice() - Resize");

				m_Window->sendSetTiledBuffer(m_Device->GetTiles());

				if(!m_UserDefinedScaled)
				{
					m_UiScaleFactor = GetScaleBasedOnResolution(m_Device->GetWidth(), m_Device->GetHeight());
				}
				m_Window->Resize(m_Device->GetWidth(), m_Device->GetHeight(), m_UiScaleFactor);
			}

			g_VirtualCursor.OnReset();
		}
	}

	if(success)
	{
		return RGSC_OK;
	}
	else
	{
		return RGSC_FAIL;		
	}
}

const char* RgscUi::GetOnlineWebSiteUrl(char (&urlBuf)[RGSC_MAX_URL_BUF_SIZE])
{
	static const char* s_OnlineUrlPath = "scui/v2/desktop"; // SCUIv1: "scui/desktop/default.aspx";
	const char* path = s_OnlineUrlPath;
	PARAM_scuiurlpath.Get(path);

#if __DEV
	// Allow for the command line to override any SCUI url passed into the configuration interface.
	if(PARAM_scuiurl.Get(path))
 	{
 		formatf(m_OnlineUrl, "%s", path);
 	}
#endif

	// If the ScuiURL has not been set, generate it.
	if (StringNullOrEmpty(m_OnlineUrl))
	{
		//bool useHttps = g_rlTitleId->m_RosTitleId.GetEnvironment() == RLROS_ENV_PROD;
		// we always want the SCUI to be loaded via HTTPS now (even in dev)
		bool useHttps = true;
		formatf(m_OnlineUrl, "%s://%s/%s", useHttps ? "https" : "http", g_rlTitleId->m_RosTitleId.GetDomainName(), path);
	}

#if ALLOW_ANY_WEB_SITE
	if(strstr(m_OnlineUrl, "ros.rockstargames.com") == NULL)
	{
		m_NonScuiUrl = true;
	}
#endif

	// Copy the online URL to the out buffer
	safecpy(urlBuf, m_OnlineUrl);
	
	return urlBuf;
}

const char* RgscUi::GetOfflineWebSiteUrl()
{
	static char s_OfflineUiPath[RGSC_MAX_PATH] = {0};

#if USE_RPF_FOR_OFFLINE_ASSETS
	if(AssertVerify(GetRgscConcreteInstance()->_GetFileSystem()->GetSocialClubDllDirectory(s_OfflineUiPath)))
	{
		safecat(s_OfflineUiPath, "UI.rpf");

		if(m_OfflinePackFile.IsInitialized() == false)
		{
			if(AssertVerify(m_OfflinePackFile.Init(s_OfflineUiPath)))
			{
				// resourceinterceptor.cpp intercepts requests from Chrome and fulfills them by loading files from the rpf
				m_OfflinePackFile.MountAs("scui:/");
				AssertVerify(LoadUrl("file://UI/default.html"));
			}
		}
	}
#else
	if(AssertVerify(GetRgscConcreteInstance()->_GetFileSystem()->GetOfflineUiAssetsDirectory(s_OfflineUiPath, false)))
	{
		safecat(s_OfflineUiPath, "default.html");
	}
#endif

	return s_OfflineUiPath;
}

void RgscUi::SetOnlineUrl(const char* onlineUrl)
{
	safecpy(m_OnlineUrl, onlineUrl);
}

const char* RgscUi::GetCurrentUrl()
{
	return m_CurrentUrl;
}

RgscWindowType RgscUi::GetWindowType() const
{
	return m_WindowType;
}

void RgscUi::StartState(State state)
{
	/*
		Startup sequence:

		1. Setup script bindings to call at start of page load.
		2. LoadUrl()
		3. Page load completes.
		4. Javascript calls RGSC_FINISHED_FUNCTION_BINDINGS() as early as possible.
		5. We call RGSC_JS_READY_TO_ACCEPT_COMMANDS()
		6. Javascript calls RGSC_READY_TO_ACCEPT_COMMANDS().
	*/

	m_State = state;
	m_StateLoadTimeout.Reset();
	m_StateLoadTimeout.InitSeconds(GetStateTimeout(state));

	static const char* s_StateNames[] =
	{
		"LOADING",
		"WAITING_FOR_PAGE_LOAD_COMPLETION",
		"WAITING_FOR_JAVASCRIPT_BINDINGS",
		"WAITING_FOR_JAVASCRIPT_PONG",
		"WAITING_FOR_JAVASCRIPT_READY",
		"JAVASCRIPT_READY",
		"WAITING_FOR_RESTART",
		"START_SIGN_IN",
		"SIGNING_IN",
		"FINISHED",
		"FAILED"
	}; 

	CompileTimeAssert(COUNTOF(s_StateNames) == STATE_NUM_STATES);
	if (m_State >= 0 && m_State < STATE_NUM_STATES)
	{
		RgscDisplay("Social Club UI - Entering State: '%s'", s_StateNames[m_State]);
	}

	Assert(m_AsyncStatus.Pending() == false);

	switch(state)
	{
	case STATE_LOADING:
		{
			m_JavascriptReadyToAcceptCommands = false;
			m_ReadyToAcceptCommands = false;
			m_PongReceived = false;
			m_ScuiPageLoadError = false;
			m_LastJavascriptPingTime = 0;

			if(m_LoadingStatus == LS_LOADING_ONLINE)
			{
				char urlBuf[RGSC_MAX_URL_BUF_SIZE];
				AssertVerify(LoadUrl(GetOnlineWebSiteUrl(urlBuf)));
			}
			else
			{
				Assert(m_LoadingStatus == LS_LOADING_OFFLINE);
				AssertVerify(LoadUrl(GetOfflineWebSiteUrl()));
			}

			StartState(STATE_WAITING_FOR_PAGE_LOAD_COMPLETION);
		}
		break;
	case STATE_WAITING_FOR_JAVASCRIPT_BINDINGS:
		{
			if(m_JavascriptReadyToAcceptCommands)
			{
				StartState(STATE_WAITING_FOR_JAVASCRIPT_PONG);
			}
		}
		break;
	case STATE_WAITING_FOR_JAVASCRIPT_PONG:
		{
			if(m_ReadyToAcceptCommands)
			{
				StartState(STATE_JAVASCRIPT_READY);
			}
		}
		break;
	case STATE_WAITING_FOR_JAVASCRIPT_READY:
		{
			if(m_ReadyToAcceptCommands)
			{
				StartState(STATE_JAVASCRIPT_READY);
			}
			else
			{
				bool succeeded = false;
				rgsc::Script::JsReadyToAcceptCommands(&succeeded);
			}
		}
		break;
	case STATE_JAVASCRIPT_READY:
		{
			Assert((m_LoadingStatus == LS_LOADING_ONLINE) || (m_LoadingStatus == LS_LOADING_OFFLINE));
			if(m_LoadingStatus == LS_LOADING_ONLINE)
			{
				m_LoadingStatus = LS_LOADED_ONLINE;
				if(m_TransitioningToOnline)
				{
					if(GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal())
					{
						GetRgscConcreteInstance()->_GetProfileManager()->SignInAfterTransition();
					}
					StartState(STATE_FINISHED);
				}
				else
				{
					StartState(STATE_START_SIGN_IN);
				}
			}
			else
			{
				m_LoadingStatus = LS_LOADED_OFFLINE;

				StartState(STATE_START_SIGN_IN);
			}
		}
		break;
	case STATE_START_SIGN_IN:
		if((GetRgscConcreteInstance()->GetHandleAutoSignIn() == false) ||
			(GetRgscConcreteInstance()->_GetProfileManager()->AttemptAutoSignIn() != RGSC_OK))
		{
			// either the game wants to handle auto sign in or there is no auto sign in profile
			StartState(STATE_FINISHED);
		}
		else
		{
			m_AutoSignInStartTime = sysTimer::GetSystemMsTime();
		}
		break;
	case STATE_SIGNING_IN:
		// nothing to do here. See UpdateState().
		break;
	case STATE_FINISHED:
		m_NumPageReloadAttempts = 0;
		ClearReloadingUi();
		SendWelcomeNotification();
		SendConnectionLostNotification();
		RgscDisplay("Finished loading Social Club UI");
		RgscRetailLog::AddInterval();

		if (m_ShowUiAfterSiteLoad)
		{
			ShowUi();
			m_ShowUiAfterSiteLoad = false;
		}
		break;
	case STATE_FAILED:
		m_NumPageReloadAttempts = 0;
		break;
	default:
		break;
	}
}

void RgscUi::SendWelcomeNotification()
{
	rtry
	{
		rcheck(m_SendWelcomeNotification,catchall, );

		JSONNODE* rootNode = json_new(JSON_NODE);
		rverify(rootNode, catchall, );

		json_push_back(rootNode, json_new_i("Count", 1));

		JSONNODE* notificationArray = json_new(JSON_ARRAY);
		rverify(notificationArray, catchall, );
		json_set_name(notificationArray, "Notifications");

		JSONNODE* notificationNode = json_new(JSON_NODE);
		rverify(notificationNode, catchall, );

		json_push_back(notificationNode, json_new_a("Type", "Welcome"));

 		JSONNODE* welcomeArray = json_new(JSON_ARRAY);
 		rverify(welcomeArray, catchall, );
 
 		json_set_name(welcomeArray, "Data");
 
 		JSONNODE* welcomeNode = json_new(JSON_NODE);
 		rverify(welcomeNode, catchall, );
 
 		json_set_name(welcomeNode, "Welcome");
 
		// these are the message Ids Aaron gave me.
		// On Mac we use the backslash key ('\') to bring up the SCUI
		// since many Mac laptops don't have a Home key.
		int messageId = GetRgscConcreteInstance()->IsMac() ? 4 : 1;
		json_push_back(welcomeNode, json_new_i("Message", messageId));

		// Display the number of controllers
		int numPads = 0;
	
		// Display each pad type so the SCUI knows if we're using Xbox, PS4, Vive, etc.
		JSONNODE* controllerArray = json_new(JSON_ARRAY);
		rverify(controllerArray, catchall, );
		json_set_name(controllerArray, "ControllerTypes");

		for (int j = 0; j < IGamepadManagerV1::InputMode::INPUTMODE_MAX; j++)
		{
			IGamepadManagerV1::InputMode inputMode = (IGamepadManagerV1::InputMode)j;
			const int countPads = GetRgscConcreteInstance()->_GetGamepadManager()->GetNumPads(inputMode);

			for (int i = 0; i < countPads; i++)
			{
				ControllerType type = RGSC_CT_UNKNOWN;
				IGamepad* pad = GetRgscConcreteInstance()->_GetGamepadManager()->GetPad(inputMode, i);

				// skip invalid pads
				if (pad == NULL)
					continue;

				IGamepadV1* v1 = NULL; 
				RGSC_HRESULT hr = pad->QueryInterface(IID_IGamepadV1, (void**) &v1);
				if(!SUCCEEDED(hr) || v1 == NULL || !v1->IsConnected())
				{
					// skip disconnected or unusable pads
					continue;
				}

				// If the pad is v4, query its type
				IGamepadV4* v4 = NULL; 
				hr = pad->QueryInterface(IID_IGamepadV4, (void**) &v4);
				if(SUCCEEDED(hr) && v4 != NULL)
				{
					type = v4->GetType();
				}

				json_push_back(controllerArray, json_new_i(NULL, type));
				numPads++;
			}
		}
		
		json_push_back(welcomeNode, json_new_i("NumControllers", numPads));
		json_push_back(welcomeNode, controllerArray);

 		json_push_back(welcomeArray, welcomeNode);
 
 		json_push_back(notificationNode, welcomeArray);

		json_push_back(notificationArray, notificationNode);

		json_push_back(rootNode, notificationArray);

		json_char* jc = json_write_formatted(rootNode);

// #if !__NO_OUTPUT
// 		unsigned int len = strlen(jc);
// 		for(unsigned i = 0; i < len; i++)
// 		{
// 			printf("%c", jc[i]);
// 		}
// 		fflush(stdout);
// #endif

		json_delete(rootNode);

		GetRgscConcreteInstance()->_GetUiInterface()->SendNotification(jc);

		json_free_safe(jc);
	}
	rcatchall
	{

	}

	// Disable the welcome notification for the remainder of the session
	m_SendWelcomeNotification = false;
}

void RgscUi::SendConnectionLostNotification()
{
	rtry
	{
		rcheck(m_SendConnectionLostNotification,catchall, );

		JSONNODE* rootNode = json_new(JSON_NODE);
		rverify(rootNode, catchall, );

		json_push_back(rootNode, json_new_i("Count", 1));

		JSONNODE* notificationArray = json_new(JSON_ARRAY);
		rverify(notificationArray, catchall, );
		json_set_name(notificationArray, "Notifications");

		JSONNODE* notificationNode = json_new(JSON_NODE);
		rverify(notificationNode, catchall, );

		json_push_back(notificationNode, json_new_a("Type", "ConnectionLost"));

		JSONNODE* connectionLostArray = json_new(JSON_ARRAY);
		rverify(connectionLostArray, catchall, );

		json_set_name(connectionLostArray, "Data");

		JSONNODE* connectionLostNode = json_new(JSON_NODE);
		rverify(connectionLostNode, catchall, );

		json_set_name(connectionLostNode, "ConnectionLost");

		// 1 -- Your connection to Social Club servers has been lost.
		json_push_back(connectionLostNode, json_new_i("Message", 1));

		json_push_back(connectionLostArray, connectionLostNode);

		json_push_back(notificationNode, connectionLostArray);

		json_push_back(notificationArray, notificationNode);

		json_push_back(rootNode, notificationArray);

		json_char* jc = json_write_formatted(rootNode);

		json_delete(rootNode);

		rlDebug2("%s\n", jc);

		GetRgscConcreteInstance()->_GetUiInterface()->SendNotification(jc);

		json_free_safe(jc);
	}
	rcatchall
	{

	}

	m_SendConnectionLostNotification = false;
}

u32 RgscUi::GetStateTimeout(State state)
{
	switch(state)
	{
	case STATE_WAITING_FOR_PAGE_LOAD_COMPLETION:
		return 60;
	case STATE_WAITING_FOR_JAVASCRIPT_BINDINGS:
		return 15;
	case STATE_WAITING_FOR_JAVASCRIPT_PONG:
		return 15;
	case STATE_WAITING_FOR_JAVASCRIPT_READY:
		return 15;
	default:
		return 15;
	}
}

bool RgscUi::IsTimeoutErrorCode(int errorCode)
{
	switch(errorCode)
	{
	case TIMEOUT_WAITING_PAGE_LOAD:
	case TIMEOUT_WAITING_JS_BINDINGS:
	case TIMEOUT_WAITING_JS_PONG:
	case TIMEOUT_WAITING_JS_READY:
	case TIMEOUT_ERROR_SCUI_PAGE_LOAD:
		return true;
	default:
		return false;
	}
}

void RgscUi::UpdateState()
{
	switch(m_State)
	{
	case STATE_START_SIGN_IN:
		{
			const int AUTOSIGNIN_TIMEOUT = (20*1000);
			// we're waiting for the 'SigningIn' flag to get set to true since it
			// can take a few frames before the sign in procedure is started.
			if(GetRgscConcreteInstance()->_GetProfileManager()->IsSigningIn() || 
			   GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal())
			{
				StartState(STATE_SIGNING_IN);
			}
			else if(sysTimer::HasElapsedIntervalMs(m_AutoSignInStartTime, AUTOSIGNIN_TIMEOUT) || m_SignInError)
			{
				// auto sign in must have failed but we don't get any notification of this from the SCUI.
				rlDebug("auto sign in timeout");
				RgscWarning("Auto login timed out");
				StartState(STATE_FINISHED);
				ClearSignInError();
			}
		}
		break;
	case STATE_SIGNING_IN:
		if(GetRgscConcreteInstance()->_GetProfileManager()->IsSigningIn() == false)
		{
			StartState(STATE_FINISHED);
		}
		break;
	case STATE_WAITING_FOR_PAGE_LOAD_COMPLETION:
		{
			if (!PARAM_noScuiLoadTimeouts.Get() && m_StateLoadTimeout.IsTimedOut())
			{			
				m_StateLoadTimeout.Clear();
				rlWarning("Waiting for SCUI page load completion : timed out");
				RgscWarning("Timed out loading Social Club UI");

				// will fallback or fail. Don't call page load complete here, since the page is actually still loading
				JavascriptFunctionDoesntExist("RGSC_JS_READY_TO_ACCEPT_COMMANDS");
			}
		}
		break;
	case STATE_WAITING_FOR_JAVASCRIPT_BINDINGS:
		{
			if (!PARAM_noScuiLoadTimeouts.Get() &&  m_StateLoadTimeout.IsTimedOut())
			{			
				m_StateLoadTimeout.Clear();
				rlWarning("Waiting for chrome javascript bindings : timed out");
				RgscWarning("Timed out loading Javascript bindings");

				// will fallback or fail
				PageLoadComplete(TIMEOUT_WAITING_JS_BINDINGS);
			}
		}
		break;
	case STATE_WAITING_FOR_JAVASCRIPT_PONG:
		{
			if (m_PongReceived)
			{
				StartState(STATE_WAITING_FOR_JAVASCRIPT_READY);
			}
			else if (m_ScuiPageLoadError)
			{
				m_StateLoadTimeout.Clear();
				rlWarning("Waiting for SCUI javascript pong : SCUI page load error");
				RgscWarning("Social Club UI experienced a load error.");

				// will fallback or fail
				PageLoadComplete(TIMEOUT_ERROR_SCUI_PAGE_LOAD);
			}
			else
			{
				if (!PARAM_noScuiLoadTimeouts.Get() && m_StateLoadTimeout.IsTimedOut())
				{			
					m_StateLoadTimeout.Clear();
					rlWarning("Waiting for SCUI javascript pong : timed out");
					RgscWarning("Timed out waiting for Javascript callbacks");

					// will fallback or fail
					PageLoadComplete(TIMEOUT_WAITING_JS_PONG);
				}
				else
				{
					const int JAVASCRIPT_PING_INTERVAL = 1 * 1000;
					if (sysTimer::HasElapsedIntervalMs(m_LastJavascriptPingTime, JAVASCRIPT_PING_INTERVAL))
					{
						m_LastJavascriptPingTime = sysTimer::GetSystemMsTime();
						SendScuiPing();
					}
				}
			}
		}
		break;
	case STATE_WAITING_FOR_JAVASCRIPT_READY:
		{
			if (!PARAM_noScuiLoadTimeouts.Get() && m_StateLoadTimeout.IsTimedOut())
			{			
				rlWarning("Waiting for SCUI javascript ready : timed out");
				RgscWarning("Timed out waiting for Javascript ready");

				// will fallback or fail
				PageLoadComplete(TIMEOUT_WAITING_JS_READY);
			}
		}
		break;
	default:
		break;
	}
}

void RgscUi::UpdateShutdown()
{
	if (m_bWaitingForScuiShutdown)
	{
		// Add a small window (100ms) of additional leeway time to let the SCUI also process the timeout.
		const int SHUTDOWN_LEEWAY_MS = 100;

		// If '0' is specified as the timeout, there is no timeout.
		//	Otherwise, wait for the timeoutMs + leeway time to pass, and then clear the shutdown values and continue processing.
		if (m_uShutdownTimeoutMs != 0 && sysTimer::HasElapsedIntervalMs(m_uShutdownStartTime, m_uShutdownTimeoutMs + SHUTDOWN_LEEWAY_MS))
		{
			m_bWaitingForScuiShutdown = false;
			m_uShutdownStartTime = 0;
			m_uShutdownTimeoutMs = 0;
		}
	}
}

void RgscUi::Update()
{
	UpdateState();
	UpdateShutdown();

	Rgsc_Input::Update();
	
	m_Ipc.Update();

	m_ActivationSystem.Update();
	m_PatchingSystem.Update();

	//If a frame takes longer than this (e.g. due to an assert or breakpoint) we compensate by adjusting the timeout.
	const int PAGE_LOAD_COMPENSATION_THRESHOLD_MS = (5 *1000);
	const unsigned msSinceTimeoutUpdate = m_StateLoadTimeout.GetMillisecondsSinceLastUpdate();
	if(msSinceTimeoutUpdate >= PAGE_LOAD_COMPENSATION_THRESHOLD_MS)
	{
		rlWarning("Frame time was %u milliseconds - adjusting timeout...", msSinceTimeoutUpdate);
		m_StateLoadTimeout.ExtendMilliseconds(msSinceTimeoutUpdate);
	}

	m_StateLoadTimeout.Update();

#if 0
	static int tmp = 0;
	static int tmpDur = 10000;
	static int s_Time = timeGetTime();
	int elapsed = timeGetTime() - s_Time;
	if(elapsed >= tmpDur && tmp > 0)
	{
		s_Time = timeGetTime();

#if 1
		m_SendWelcomeNotification = true;
		SendWelcomeNotification();
#else
		rlScPresenceMessageTextMessageRecieved t;
		rlGamerHandle gh;
		gh.ResetSc(GetRgscConcreteInstance()->_GetProfileManager()->GetSignedInProfile().GetProfileId());
		t.m_SenderGamerHandle = gh;
		safecpy(t.m_SenderName, "Undead Goat");
		char tmp[1024];
		t.Export(tmp);
		rlScPresenceMessage msgBuf(tmp, 0);

		GetRgscConcreteInstance()->_GetPresenceManager()->HandleTextMessageReceived(msgBuf);
#endif
	}
#endif

	bool queueShowUi = false;
	bool queueHideUi = false;

	ioEvent ev;
	while(ioEventQueue::Pop(ev))
	{
		// for all mouse events, need to convert lparam to x,y, map the x,y to the render rect, then back to lparam
		switch(ev.m_Type)
		{
			case ev.IO_MOUSE_MOVE:
			case ev.IO_MOUSE_LEFT_DBLCLK:
			case ev.IO_MOUSE_LEFT_DOWN:
			case ev.IO_MOUSE_LEFT_UP:
			{
				int x = (int)(short)LOWORD(ev.m_Data);
				int y = (int)(short)HIWORD(ev.m_Data);

				if(m_Device && (ev.m_Modifiers & RGSC_MOUSE_MOD_NO_MAP) == 0)
				{
					if (m_Device->MapMouse(x, y))
					{
						ev.m_Data = MAKELPARAM(x, y);
					}
				}
				else
				{
					ev.m_Data = MAKELPARAM(x, y);
				}
				break;
			}
			default:
			{
				break;
			}
		}

		switch(ev.m_Type)
		{
		case ev.IO_MOUSE_MOVE:
			if(IsVisible())
			{
				m_Window->sendMouseMoved(ev.m_X, ev.m_Y, ev.m_Data);
			}
			break;

		case ev.IO_MOUSE_LEFT_DBLCLK:
		case ev.IO_MOUSE_LEFT_DOWN:
		case ev.IO_MOUSE_LEFT_UP:
			if(IsVisible())
			{
				m_Window->sendMouseButton(ev.m_X, ev.m_Y, ev.m_Data);
			}
			break;

		case ev.IO_MOUSE_WHEEL:
			if(IsVisible())
			{
				int wheel = (int)ev.m_Data;
				static int offsetPerTick = 35;
				static int xScroll = 0;
				if(wheel != 0)
				{
					m_Window->sendMouseWheel(wheel * xScroll, wheel * offsetPerTick);
				}
			}
			break;

		case ev.IO_KEY_DOWN:
		case ev.IO_KEY_UP:
		case ev.IO_KEY_CHAR:
			if(IsVisible())
			{
				m_Window->sendKeyEvent(ev.m_X, ev.m_Y, ev.m_Data, ev.m_Modifiers);
			}
			break;

		case ev.IO_CONTROLLER_BUTTONS:
			{
				HandleControllerInput((int)ev.m_X, (int)ev.m_Y, (int)ev.m_Data);
			}
			break;

		case ev.IO_SOCIAL_CLUB_OPEN:
			if(IsHotkeyEnabled())
			{
				if((IsVisible() == false) && IsReadyToAcceptCommands())
				{
					if(m_WasUiJustHidden == false)
					{
						queueShowUi = true;
					}
				}
			}
			break;

		case ev.IO_SOCIAL_CLUB_CLOSE:
			if(IsClosingEnabled())
			{
#if 0
				tmp++;
				if(tmp > 3)
				{
					tmp = 0;
				}
				if(tmp == 1)
				{
					tmpDur = 10000;
				}
				else if(tmp == 2)
				{
					tmpDur = 1000;
				}
				else
				{
					tmpDur = 500;
				}
#endif

				if(IsVisible() || (m_NumNotificationsVisible > 0))
				{
					if((ev.m_Data == Rgsc_Input::METHOD_HOME) && DoesTextBoxHaveFocus())
					{
						// this prevents the UI from disappearing if you press Home while in a text entry area.
					}
					else
					{
						if(m_WasUiJustShown == false)
						{
							m_NumNotificationsVisible = 0;
							queueHideUi = true;
						}
					}
				}
			}
			break;

		default:
			break;
		}
	}

//  	if(IsVisible())
//  	{
//  		TVHITTESTINFO hit;
//  		GetCursorPos(&hit.pt);
//  		MapWindowPoints(NULL,m_Device->GetWindowHandle(),&hit.pt,1);
//  		m_Window->mouseMoved(hit.pt.x, hit.pt.y);
//  	}


	// this has to be called here as well as in Render().
	// If it's only called in Render(), the cursor shape doesn't change in L.A. Noire.
	// If it's only called here, the cursor shape doesn't change in Max Payne 3.
	SetOsCursorShape();

	if(queueHideUi)
	{
		C2JsRequestUiState(false, UI_STATE_NONE, UI_SUBSTATE_NONE, NULL);
	}
	else if(queueShowUi)
	{
		C2JsRequestUiState(true, UI_STATE_MAIN_MENU, UI_SUBSTATE_NONE, NULL);
	}

	UpdateSubprocess();
}

void RgscUi::UpdateSubprocess()
{
	if((m_Ipc.GetNumChannels() == 0) && !m_bHibernating)
	{
		bool running = WaitForSingleObject(m_SubprocessInfo.hProcess, 0) == WAIT_TIMEOUT;
		if(!running)
		{
			// Only set this flag once when the subprocess dies.
			//	If the subprocess has died recently, we will have to use it again later.
			if (m_UiVisibleOnSubprocessReboot == -1)
			{
				m_UiVisibleOnSubprocessReboot = IsVisible() ? 1 : 0;
			}

			// Destroy the RgscWindow and enter the failed state so that IsUiAcceptingCommands returns false.
			CleanupWindow();
			m_State = STATE_FAILED;

			// Ensure enough time has passed since the last subprocess reboot.
			if (sysTimer::HasElapsedIntervalMs(m_uLastSubprocessRebootTime, SUBPROCESS_REBOOT_INTERVAL_MS))
			{
				rlDebug1("Restarting subprocess.");

				// Capture the last reboot time and clear the visibility flag.
				m_uLastSubprocessRebootTime = sysTimer::GetSystemMsTime();
				InitSubProcess();
				ReloadUi(IsOfflineMode(), (m_UiVisibleOnSubprocessReboot == 1));
				m_UiVisibleOnSubprocessReboot = -1;
			}
		}
	}
}

void RgscUi::SetOsCursorShape()
{
	SYS_CS_SYNC(m_Cs);

	// checking m_Window is not NULL here due to a race
	// condition on shutdown - found by WER crash reports
	if(m_ScUiVisible && m_Window)
	{
		// On the launcher, we only want to set the shape if the mouse is over our window.
		if (GetRgscConcreteInstance()->IsLauncher() && Rgsc_Input::IsInputEnabled() && !Rgsc_Input::IsMouseInWindow())
		{
			return;
		}

		if (g_VirtualCursor.IsInitialized())
		{
			g_VirtualCursor.SetCursorType(m_Window->getCursorType());

			// If we're using a virtual cursor, don't update the OS cursor shape.
			if (g_VirtualCursor.IsEnabled())
				return;
		}

		// If the UI was created as a sub window, the cursor should not require updates.
		// This should only be a requirement for windowless (off-screen rendering)
		if (m_WindowConfig)
		{
			return;
		}

		m_Cursor = m_Window->getCursor();
		SetCursor(m_Cursor);
	}
}

void RgscUi::SetOsCursor()
{
	SYS_CS_SYNC(m_Cs);

#if 0
	if(m_ScUiVisible)
	{
		m_Cursor = m_Window->getCursor();

		CURSORINFO cursorInfo;
		cursorInfo.cbSize = sizeof(CURSORINFO);
		GetCursorInfo(&cursorInfo);
		bool isCursorShowing = (cursorInfo.flags & CURSOR_SHOWING) != 0;

		if(Rgsc_Input::IsUsingController())
		{
			SetCursor(NULL);

			if(isCursorShowing)
			{
				int c = ShowCursor(FALSE);
#if DEBUG_CURSOR
				printf("SetOsCursor: ShowCursor(false): %d\n", c);
				fflush(stdout);
#endif
			}
		}
		else
		{
			SetCursor(m_Cursor);

			if(isCursorShowing == false)
			{
				int c = ShowCursor(TRUE);
#if DEBUG_CURSOR
				printf("SetOsCursor: ShowCursor(true): %d\n", c);
				fflush(stdout);
#endif
			}
		}
	}
#else

	SetOsCursorShape();

	if(m_WasUiJustShown)
	{
		m_WasUiJustShown = false;
		if(m_ScUiVisible)
		{
			int c;
			do 
			{
				c = ShowCursor(TRUE);
#if DEBUG_CURSOR
				printf("SetOsCursor: ShowCursor(true): %d\n", c);
				fflush(stdout);
#endif
			} while (c < 0);
		}
	}

	if(m_ScUiVisible)
	{
		CURSORINFO cursorInfo;
		cursorInfo.cbSize = sizeof(CURSORINFO);
		GetCursorInfo(&cursorInfo);
		bool isCursorShowing = (cursorInfo.flags & CURSOR_SHOWING) != 0;
		if(isCursorShowing == false)
		{
			ShowCursor(FALSE);

			int c;
			do 
			{
				c = ShowCursor(TRUE);
#if DEBUG_CURSOR
				printf("SetOsCursor: ShowCursor(true): %d\n", c);
				fflush(stdout);
#endif
			} while (c < 0);

		}
	}
#endif
}

void RgscUi::Render()
{
	if(sysMemAllocator::IsCurrentSet() == false)
	{
		sysMemAllocator::SetCurrent(*g_RgscAllocator);
	}

	if(IsVisible() || (m_NumNotificationsVisible > 0))
	{
		// IMPORTANT: make sure this stays in a separate block so the critical section only affects what is in the block.
		{
			// The critical section prevents the system memory copy of the web page from being modified
			// while the render thread is updating the textures (for games that use a separate render thread).
			SYS_CS_SYNC(m_Cs);

			// Render if a device is present.
			if (m_Device)
			{
				m_Device->Render(!IsVisible());
			}
			
			SetOsCursor();
		}
	}
	else if(m_WasUiJustHidden)
	{
		SYS_CS_SYNC(m_Cs);

		// reset cursor
		SetCursor(m_OldCursorInfo.hCursor);

#if 0
		bool showCursor = (m_OldCursorInfo.flags & CURSOR_SHOWING) != 0;

		CURSORINFO cursorInfo;
		cursorInfo.cbSize = sizeof(CURSORINFO);
		GetCursorInfo(&cursorInfo);
		bool isCursorShowing = (cursorInfo.flags & CURSOR_SHOWING) != 0;

		if(showCursor)
		{
			if(isCursorShowing == false)
			{
				int c = ShowCursor(TRUE);
#if DEBUG_CURSOR
				printf("Close: ShowCursor(true): %d\n", c);
				fflush(stdout);
#endif
			}
		}
		else
		{
			if(isCursorShowing)
			{
				int c = ShowCursor(FALSE);
#if DEBUG_CURSOR
				printf("Close: ShowCursor(false): %d\n", c);
				fflush(stdout);
#endif
			}
		}

#if DEBUG_CURSOR
		printf("Close: m_OldCursorInfo.flags: %d\n", m_OldCursorInfo.flags);
		fflush(stdout);
#endif
#else
		ShowCursor(FALSE);
#if DEBUG_CURSOR
		int c = ShowCursor(FALSE);
		printf("Close: ShowCursor(false): %d\n", c);
		fflush(stdout);
#endif

#endif
		m_WasUiJustHidden = false;
	}
}

// TODO: NS - this function is duplicated several times, put it in a central location and re-use it.
static json_char* json_as_string_or_null(json_const JSONNODE* node)
{
	char* str = json_as_string(node);
	return (_stricmp("null", str) != 0) ? str : NULL;
}

void RgscUi::HandleControllerInput(const u32 pressedButtons, const u32 releasedButtons, const u32 heldButtons)
{
	rtry
	{
		rverify((pressedButtons != 0) || (releasedButtons != 0), catchall, );

		JSONNODE *n = json_new(JSON_NODE);

		json_push_back(n, json_new_i("PressedButtons", pressedButtons));
		json_push_back(n, json_new_i("ReleasedButtons", releasedButtons));
		json_push_back(n, json_new_i("HeldButtons", heldButtons));

		json_char *jc = json_write_formatted(n);

		json_delete(n);

		rlDebug2("%s\n", jc);

		bool succeeded = false;
		rgsc::Script::JsHandleControllerInput(&succeeded, jc);
		json_free_safe(jc);
		rverify(succeeded, catchall, );
	}
	rcatchall
	{

	}
}

bool RgscUi::LaunchExternalWebBrowser(const char* jsonRequest)
{
	bool success = false;
	char* url = NULL;

	JSONNODE *n = json_parse(jsonRequest);

	if(!AssertVerify(n != NULL))
	{
		return success;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return success;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "Url") == 0)
		{
			url = json_as_string_or_null(*i);
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	if(url)
	{
		success = LaunchExternalUrl(url);
	}

	json_free_safe(url);

	return success;
}

bool RgscUi::LaunchExternalUrl(const char* url)
{
	bool success = false;

	rlDebug2("Launching external url: %s", url);

	if (m_ForwardExternalUrls)
	{
		GetRgscConcreteInstance()->HandleNotification(rgsc::NOTIFY_EXTERNAL_BROWSER_URL, url);
		success = true;
	}
	else
	{
		HINSTANCE hRes = ShellExecuteA(NULL, "open", url, NULL, NULL, SW_SHOWMAXIMIZED);
		Assertf(hRes > (HINSTANCE)32, "ShellExecute returned an error when trying to open a url in an external web browser: %d", hRes);
		success = hRes > (HINSTANCE)32;
	}

	return success;
}

bool RgscUi::RequestExternalUrl(const char* url)
{
	bool succeeded = false;

	char urlParams[RL_MAX_URL_BUF_LENGTH + 256] = {0};

	rtry
	{
		RsonWriter rw;
		rw.Init(urlParams, sizeof(urlParams), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		rw.WriteString("Url", url);
		rcheck(rw.End(), catchall, );

		rgsc::Script::JsRequestUrl(&succeeded, urlParams);
	}
	rcatchall
	{
		succeeded = false;
	}

	return succeeded;
}

bool RgscUi::LoadUrl(const char* url)
{
	m_ReadyToAcceptCommands = false;
	m_JavascriptReadyToAcceptCommands = false;

#if ALLOW_ANY_WEB_SITE
	if(m_NonScuiUrl)
	{
		m_ReadyToAcceptCommands = true;
		m_JavascriptReadyToAcceptCommands = true;
		m_ScUiVisible = true;
	}
#endif

	rlDebug("RgscUi::LoadUrl() called with url '%s'", url);

	if ((m_Window == NULL) || (url == NULL) || (url[0] == '\0'))
	{
		rlDebug("RgscUi::LoadUrl() returning false.");
		return false;
	}

	safecpy(m_CurrentUrl, url);
	m_Window->clear();

#if USE_ZIP_FOR_OFFLINE_ASSETS
	// If we're loading the offline website, we want to include the offline zip root folder with navigateTo
	//	i.e. C:\Program Files\Rockstar Games\Social Club\UI\
	// Internally, we'll map all requests to this path to load from a zip file.
	//	i.e. C:\Program Files\Rockstar Games\Social Club\UI.zip
	if (m_LoadingStatus == RgscUi::LS_LOADING_OFFLINE)
	{
		char offlineZipPath[RGSC_MAX_PATH] = {0};
		if(AssertVerify(GetRgscConcreteInstance()->_GetFileSystem()->GetOfflineUiAssetsDirectory(offlineZipPath, false)))
		{
			m_Window->sendNavigateTo(url, offlineZipPath);
		}
		else
		{
			return false;
		}
	}
	else
#endif
	{
		m_Window->sendNavigateTo(url);
	}

	return true;
}

RGSC_HRESULT RgscUi::CreateWindowInternal(IRgscUiWindowConfiguration* config)
{
	if(GetRgscConcreteInstance()->IsUiDisabled())
	{
		// the social club dll can now be run in environments where the scui doesn't exist (during development)
		return RGSC_OK;
	}

	rtry
	{
		rverify(config, catchall, );
		m_WindowConfig = config;

		m_WindowConfigV1 = NULL;
		RGSC_HRESULT hr = config->QueryInterface(IID_IRgscUiWindowConfigurationV1, (void**)&m_WindowConfigV1);
		rverify(SUCCEEDED(hr) && m_WindowConfigV1, catchall, );

		m_WindowConfigV2 = NULL;
		// optional - don't check return value
		config->QueryInterface(IID_IRgscUiWindowConfigurationV2, (void**)&m_WindowConfigV2);

		// cache window geometry
		SetRenderRect(0, 0, m_WindowConfigV1->GetHeight(), m_WindowConfigV1->GetWidth());

		// Disable keyboard and mouse messages from being forwarded to chrome, allow chrome to handle keyboard and mouse messages itself.
		Rgsc_Input::SetKeyboardMessageForwarding(false);
		Rgsc_Input::SetMouseMessageForwarding(false);

		// Initialize the input system - borderless is initialized after the window is created, and passes the browser device directly.
		if (m_WindowType != BORDERLESS)
		{
			Rgsc_Input::Init(m_WindowConfigV1->GetParent(), GetRgscConcreteInstance()->GetAdditionalWindowHandles(), GetRgscConcreteInstance()->GetNumAdditionalWindowHandles());
		}

		// Load the SCUI.
		if(!LoadUi(false, m_WindowConfigV1->GetWidth(), m_WindowConfigV1->GetHeight()))
		{
			rlDebug("RgscUi::CreateInWindow returning RGSC_FAIL (LoadUi() failed).");
			return RGSC_FAIL;
		}

		return RGSC_OK;
	}
	rcatchall
	{
		return RGSC_FAIL;
	}
}

bool RgscUi::CreateUiWindow(int width, int height)
{
	SYS_CS_SYNC(m_Cs);

	if (m_Window == NULL)
	{
		if(!m_UserDefinedScaled)
		{
			m_UiScaleFactor = GetScaleBasedOnResolution(width, height);
		}

		m_Window = rage_new RgscWindow(width, height, m_UiScaleFactor);

		if(m_Window == NULL)
		{
			rlDebug("RgscUi::CreateUiWindow() failed to create RgscWindow");
			ClearReloadingUi();
			return false;
		}

		if (m_Device)
		{
			m_Window->sendSetTiledBuffer(m_Device->GetTiles());
			m_Window->clear();
		}
		else if (rlVerify(m_WindowConfigV1)) // a v1 configuration is required2
		{
			ViewMsg_Rect_Params rect;
			rect.x = m_WindowConfigV1->GetX();
			rect.y = m_WindowConfigV1->GetY();
			rect.width = m_WindowConfigV1->GetWidth();
			rect.height = m_WindowConfigV1->GetHeight();

			ViewMsg_CreateWindow_Params window;
			window.hWnd = (u64)m_WindowConfigV1->GetParent();
			window.hRootParent = (u64)m_WindowConfigV1->GetRootParent();
			window.scale = m_UiScaleFactor;
			window.borderWidth = m_WindowConfigV1->GetBorderWidth();
			window.resizeEdges = m_WindowConfigV1->GetResizeEdgeFlags();
			window.captionHeight = m_WindowConfigV1->GetCaptionHeight();
			window.sizeGripSize = m_WindowConfigV1->GetSizeGripSize();
			window.windowType = m_WindowType;
			
			if (m_WindowConfigV2)
			{
				window.minWidth = m_WindowConfigV2->GetMinimumWidth();
				window.minHeight = m_WindowConfigV2->GetMinimumHeight();
				window.maxWidth = m_WindowConfigV2->GetMaximumWidth();
				window.maxHeight = m_WindowConfigV2->GetMaximumHeight();
				window.hideOnClose = m_WindowConfigV2->ShouldMinimizeOnClose();
				window.windowTitle = m_WindowConfigV2->GetWindowTitle();
			}

			IPC_SEND(new ViewMsg_CreateWindow(1, rect, window));
		}

		SetupScriptCommands();
	}

	m_bIsWindowDestroyed = false;
	return true;
}

void RgscUi::CleanupWindow()
{
	m_bIsWindowDestroyed = true;

	if(m_Window)
	{
		delete m_Window;
		m_Window = NULL;
	}
}

bool RgscUi::LoadUi(bool bOffline)
{
	int width, height;

	// If we have a device, use its width/height. If no device is present, fall
	// back to the render rect's dimensions.
	if (m_Device)
	{
		width = m_Device->GetWidth();
		height = m_Device->GetHeight();
	}
	else
	{
		// else: default to the render rect
		width = m_RenderRect.GetWidth();
		height = m_RenderRect.GetHeight();
	}

	return LoadUi(bOffline, width, height);
}

bool RgscUi::LoadUi(bool bOffline, int width, int height)
{
	SYS_CS_SYNC(m_Cs);

	// Create our window if necessary
	if (!CreateUiWindow(width, height))
		return false;

	if(GetRgscConcreteInstance()->IsOfflineOnlyMode() || bOffline)
	{
		m_LoadingStatus = LS_LOADING_OFFLINE;
	}
	else
	{
		m_LoadingStatus = LS_LOADING_ONLINE;
	}

	StartState(STATE_LOADING);

	return true;
}

void RgscUi::EnableHotkey(bool enable)
{
	m_HotkeyEnabled = enable;
}

void RgscUi::EnableClosingUi(bool enable)
{
	m_ClosingEnabled = enable;
}

bool RgscUi::IsHotkeyEnabled()
{
	return m_HotkeyEnabled;
}

bool RgscUi::IsClosingEnabled()
{
	return m_ClosingEnabled;
}

bool RgscUi::DoesTextBoxHaveFocus()
{
	return m_TextboxHasFocus;
}

RGSC_HRESULT RgscUi::ShowUi()
{
	return C2JsRequestUiState(true, UI_STATE_MAIN_MENU, UI_SUBSTATE_NONE, NULL) ? RGSC_OK : RGSC_FAIL;
}

RGSC_HRESULT RgscUi::CloseUi()
{
	return C2JsRequestUiState(false, UI_STATE_NONE, UI_SUBSTATE_NONE, NULL);
}

RGSC_HRESULT RgscUi::HideUi()
{
	if(GetRgscConcreteInstance()->_GetUiInterface()->IsVisible())
	{
		return C2JsRequestUiState(false, UI_STATE_NONE, UI_SUBSTATE_NONE, NULL);
	}
	return RGSC_FAIL;
}

RGSC_HRESULT RgscUi::ShowSignInUi()
{
	JSONNODE *n = json_new(JSON_NODE);

	bool profilesExist = GetRgscConcreteInstance()->_GetProfileManager()->CheckForProfilesFast();

	json_set_name(n, "Data");
	json_push_back(n, json_new_b("ProfilesExist", profilesExist));

	// If we're in the process of signing in, simply show the UI.
	if (GetRgscConcreteInstance()->_GetProfileManager()->IsSigningIn())
	{
		return ShowUi();
	}
	else
	{
		return C2JsRequestUiState(true, UI_STATE_SIGN_IN, UI_SUBSTATE_NONE, n) ? RGSC_OK : RGSC_FAIL;
	}
}

RGSC_HRESULT RgscUi::ShowGamerProfileUi(unsigned int profileId)
{
	// deprecated - should not be called
	Assertf(false, "ShowGamerProfileUi() is deprecated. Use PlayerManager()->ShowPlayerProfileUi() instead");
	return RGSC_FAIL;
}

RGSC_HRESULT RgscUi::ShowAccountLinkingUi(const AccountLinkPlatform platform, AccountLinkingCallback callback)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		Assertf(m_AccountLinkingCallback == NULL, "There is already an account link in progress");

		UiSubState subState = RgscUi::UI_SUBSTATE_NONE;

		switch(platform)
		{
		case IRgscUiV3::ACCOUNT_PLATFORM_FACEBOOK:
			subState = RgscUi::UI_SUBSTATE_LINK_FACEBOOK_ACCOUNT;
			break;
		case IRgscUiV3::ACCOUNT_PLATFORM_GOOGLE:
			subState = RgscUi::UI_SUBSTATE_LINK_GOOGLE_ACCOUNT;
			break;
		case IRgscUiV3::ACCOUNT_PLATFORM_TWITCHTV:
			subState = RgscUi::UI_SUBSTATE_LINK_TWITCHTV_ACCOUNT;
			break;
		}

		rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(true,
																				 RgscUi::UI_STATE_ACCOUNT_LINKING,
																				 subState,
																				 NULL),
																				 catchall,
																				 hr = RGSC_FAIL);

		m_AccountLinkingCallback = callback;

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

void RgscUi::AcountLinkResult(const char* jsonResponse)
{
	if(AssertVerify(m_AccountLinkingCallback))
	{
		int result = 0;

		JSONNODE *n = json_parse(jsonResponse);

		if(!AssertVerify(n != NULL))
		{
			return;
		}

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "Result") == 0)
			{
				result = json_as_int(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);

		m_AccountLinkingCallback((AccountLinkingResult)result);
		m_AccountLinkingCallback = NULL;
	}
}

void RgscUi::SendVirtualKeyboardResult(const wchar_t* text)
{
	if (text)
	{
		bool succeeded = false;

		const int FORMAT_BUF = 16;
		const int MAX_TEXT_LEN = 256;
		const int BUF_LEN = MAX_TEXT_LEN + FORMAT_BUF;
		char kbParams[BUF_LEN] = {0};

		rtry
		{
			// WIDE_TO_UTF8 can triple the length
			rverify(wcslen(text)*3 < MAX_TEXT_LEN, catchall, );

			RsonWriter rw;
			rw.Init(kbParams, sizeof(kbParams), RSON_FORMAT_JSON);
			rcheck(rw.Begin(NULL, NULL), catchall, );

			USES_CONVERSION;
			rw.WriteString("Result", WIDE_TO_UTF8((rage::char16*)text));

			rcheck(rw.End(), catchall, );
			rgsc::Script::JsVirtualKeyboardResult(&succeeded, kbParams);
		}
		rcatchall
		{
		}
	}
}

void RgscUi::ShowVirtualKeyboard(const char* initialText, bool bIsPassword, int maxNumChars)
{
	bool succeeded = false;

	char kbParams[256] = {0};

	rtry
	{
		RsonWriter rw;
		rw.Init(kbParams, sizeof(kbParams), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		rw.WriteString("InitialText", initialText);
		rw.WriteBool("IsPassword", bIsPassword);
		rw.WriteInt("MaxNumChars", maxNumChars);
		rcheck(rw.End(), catchall, );
	}
	rcatchall
	{
		kbParams[0] = '\0';
	}

	rgsc::Script::JsShowVirtualKeyboard(&succeeded, kbParams);
}

InputMethod RgscUi::GetInputMethod()
{
	InputMethod im = Rgsc_Input::IsUsingController() ? RGSC_IM_CONTROLLER : RGSC_IM_KEYBOARDMOUSE;
	return im;
}

void RgscUi::ForwardExternalUrls(bool enable)
{
	m_ForwardExternalUrls = enable;
}

bool RgscUi::IsInFailState()
{
	return (m_LoadingStatus == RgscUi::LS_LOADING_FAILED);
}

RGSC_HRESULT RgscUi::RequestReloadUi(const char* jsonReloadRequest)
{
	rtry
	{
		bool bLoadOfflineSite = false;
		RsonReader rr(jsonReloadRequest, (unsigned)strlen(jsonReloadRequest));
		rverify(rr.ReadBool("OfflineSite", bLoadOfflineSite), catchall, );
		rverify(ReloadUi(bLoadOfflineSite), catchall, );

		return RGSC_OK;
	}
	rcatchall
	{
		return RGSC_FAIL;
	}
}

bool RgscUi::ReloadUi(bool bOffline, bool showUiAfterSiteLoad)
{
	rtry
	{
		// make sure we're allowed to go online
		rcheck(bOffline || (GetRgscConcreteInstance()->IsOfflineOnlyMode() == false), catchall, );

		// Verify that we're not reloading online from online or offline from offline. i.e. Require a state change
		if (bOffline)
		{
			rverify((m_State == STATE_FAILED) || (m_State == STATE_FINISHED && m_LoadingStatus != RgscUi::LS_LOADING_OFFLINE && m_LoadingStatus != RgscUi::LS_LOADING_ONLINE), catchall, );
		}
		else
		{
			rverify((m_State == STATE_FAILED) || (m_State == STATE_FINISHED && m_LoadingStatus != RgscUi::LS_LOADING_ONLINE && m_LoadingStatus != RgscUi::LS_LOADING_OFFLINE), catchall, );
		}

		m_ShowUiAfterSiteLoad = showUiAfterSiteLoad;
		m_SendConnectionLostNotification = bOffline;

		// Save the transfer data to immediately log back in
		RgscProfileManager* profileManager = GetRgscConcreteInstance()->_GetProfileManager();
		if (profileManager->IsSignedInInternal())
		{
			const RgscProfile &profile = profileManager->GetSignedInProfile();
			if(profile.IsValid())
			{
				profileManager->SetReloadProfile(profile);
				m_SignInOnReload = true;
			}

			m_SignOnlineOnReload = profileManager->IsOnlineInternal();
		}

		// sign out internally
		profileManager->SignOut();

		// reset RGSC UI states
		m_ReadyToAcceptCommands = false;
		m_JavascriptReadyToAcceptCommands = false;
		m_ScUiVisible = false;
		m_TextboxHasFocus = false;
		m_NumNotificationsVisible = false;
		ClearSignInError();
		m_NumPageReloadAttempts = 0;
		m_NonScuiUrl = false;

		// clear our ROS credentials
		rlRos::SetCredentials(0, NULL);

		// clear platform creds when going offline
		if (bOffline)
		{
			rlSocialClub::ClearLogin(0);
		}

		// set Reloading UI to true
		m_ReloadingUi = true;

		// Load the site
		LoadUi(bOffline);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool RgscUi::ReloadUi(bool bOffline)
{
	return ReloadUi(bOffline, IsVisible());
}

void RgscUi::ClearReloadingUi()
{
	m_ReloadingUi = false;
	m_SignInOnReload = false;
	m_SignOnlineOnReload = false;
	GetRgscConcreteInstance()->_GetProfileManager()->ClearReloadProfile();
}

IActivation* RgscUi::GetActivationSystem()
{
	return &m_ActivationSystem;
}

Activation* RgscUi::_GetActivationSystem()
{
	return &m_ActivationSystem;
}

IPatching* RgscUi::GetPatchingSystem()
{
	return &m_PatchingSystem;
}

Patching* RgscUi::_GetPatchingSystem()
{
	return &m_PatchingSystem;
}

Ipc* RgscUi::_GetIpc()
{
	return &m_Ipc;
}

RGSC_HRESULT RgscUi::SignIn(const char* jsonProfile)
{
	bool succeeded = false;

	// clear error flag on new signin
	ClearSignInError();

	// call js
	rgsc::Script::JsSignIn(&succeeded, jsonProfile);

	return succeeded ? RGSC_OK : RGSC_FAIL;
}

void RgscUi::FinishSignIn(const char* jsonResult)
{
	bool succeeded = false;
	rgsc::Script::JsFinishSignIn(&succeeded, jsonResult);
}

void RgscUi::FinishModifyProfile(const char* jsonResult)
{
	bool succeeded = false;
	rgsc::Script::JsFinishModifyProfile(&succeeded, jsonResult);
}

void RgscUi::OnSignInError()
{
	m_SignInError = true;
}

void RgscUi::ClearSignInError()
{
	m_SignInError = false;
}

RGSC_HRESULT RgscUi::SendNotification(const char* jsonNotification)
{
	bool succeeded = false;

	if(GetRgscConcreteInstance()->IsLauncher() == false)
	{
		// don't display notifications in the launcher
		rgsc::Script::JsSendNotification(&succeeded, jsonNotification);
	}

	return succeeded ? RGSC_OK : RGSC_FAIL;
}

void RgscUi::SetTextBoxHasFocus(const char* jsonHasFocus)
{
	int hasFocus = -1;
	char* prompt = NULL;
	char* text = NULL;
	bool isPassword = false;
	bool forwardToGame = true;
	unsigned int maxNumChars = 0xFFFFFFFF;

	// if the text box contains a backslash ('\'), libjson thinks
	// it's the beginning of an escape sequence, when in reality
	// the string has already been unescaped.
	// Re-escape backslash chars so libjson will unescape them.

	//size_t pos = 0;
//	std::string tmp(jsonHasFocus);
// 	std::string toFind("\\");
// 	std::string replacement("\\\\");
// 	while(true)
// 	{
// 		pos = tmp.find(toFind, pos);
// 		if(pos != std::string::npos)
// 		{
// 			tmp.replace(pos, toFind.length(), replacement);
// 			pos += replacement.length();
// 		}
// 		else
// 		{
// 			break;
// 		}
// 	}

	JSONNODE *n = json_parse(jsonHasFocus);

	if(!AssertVerify(n != NULL))
	{
		return;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "HasFocus") == 0)
		{
			hasFocus = json_as_bool(*i) != 0 ? 1 : 0;
		}
		else if(_stricmp(node_name, "Prompt") == 0)
		{
			prompt = json_as_string_or_null(*i);
		}
		else if(_stricmp(node_name, "Text") == 0)
		{
			text = json_as_string_or_null(*i);
		}
		else if(_stricmp(node_name, "IsPassword") == 0)
		{
			isPassword = json_as_bool(*i) != 0 ? 1 : 0;
		}
		else if(_stricmp(node_name, "MaxNumChars") == 0)
		{
			maxNumChars = json_as_int(*i);
		}
		else if(_stricmp(node_name, "ForwardToGame") == 0)
		{
			forwardToGame = json_as_bool(*i) != 0 ? 1 : 0;
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	if(hasFocus == 0)
	{
		m_TextboxHasFocus = false;
		Displayf("Text box lost focus");
		if (forwardToGame)
		{
			GetRgscConcreteInstance()->SetTextBoxHasFocusV2(m_TextboxHasFocus, prompt, text, isPassword, maxNumChars);
		}
	}
	else if(hasFocus == 1)
	{
		m_TextboxHasFocus = true;
		Displayf("Text box gained focus");

		if (forwardToGame)
		{
			GetRgscConcreteInstance()->SetTextBoxHasFocusV2(m_TextboxHasFocus, prompt, text, isPassword, maxNumChars);
		}

#if IME_TEXT_INPUT
		if(m_Device)
		{
			// this is to prevent Windows from displaying the default IME UI
			ImmIsUIMessage(m_Device->GetWindowHandle(), WM_IME_SETCONTEXT, 1, 0);
		}
#endif
	}

	json_free_safe(prompt);
	json_free_safe(text);
}

void RgscUi::NetworkChange(const char* jsonNetworkChange)
{
	int online = -1;

	JSONNODE *n = json_parse(jsonNetworkChange);

	if(!AssertVerify(n != NULL))
	{
		return;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "Online") == 0)
		{
			online = json_as_bool(*i) != 0 ? 1 : 0;
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	if(online == 0)
	{
	}
	else if(online == 1)
	{
		if((m_LoadingStatus == LS_LOADED_OFFLINE) && IsReadyToAcceptCommands())
		{
			m_TransitioningToOnline = true;
			m_LoadingStatus = LS_LOADING_ONLINE;
			StartState(STATE_LOADING);
		}
	}
}

bool RgscUi::C2JsRequestUiState(bool visible, UiState state, UiSubState subState, JSONNODE* data)
{
#if ALLOW_ANY_WEB_SITE
	if(m_NonScuiUrl)
	{
		if(visible)
		{
			ShowUiInternal();
		}
		else
		{
			HideUiInternal(false);
		}
		return true;
	}
#endif

	JSONNODE *n = json_new(JSON_NODE);

	json_set_name(n, "StateRequest");

	char szState[32] = {0};
	switch(state)
	{
	case UI_STATE_NONE:
		safecpy(szState, "");
		break;
	case UI_STATE_MAIN_MENU:
		safecpy(szState, "Main");
		break;
	case UI_STATE_SIGN_IN:
		safecpy(szState, "SignIn");
		break;
	case UI_STATE_ACHIEVEMENTS:
		safecpy(szState, "Achievements");
		break;
	case UI_STATE_ACTIVATION:
		safecpy(szState, "Activation");
		break;
	case UI_STATE_PATCHING:
		safecpy(szState, "Patching");
		break;
	case UI_STATE_FRIENDS:
		safecpy(szState, "Friends");
		break;
	case UI_STATE_PLAYERS:
		safecpy(szState, "Players");
		break;
	case UI_STATE_COMMERCE:
		safecpy(szState, "Commerce");
		break;
	case UI_STATE_ACCOUNT_LINKING:
		safecpy(szState, "AccountLinking");
		break;
	}

	Assert((state == UI_STATE_NONE) || (szState[0] != '\0'));

	char szSubState[32] = {0};
	switch(subState)
	{
	case UI_SUBSTATE_NONE:
		safecpy(szSubState, "");
		break;
	case UI_SUBSTATE_ACTIVATION_MAIN:
		safecpy(szSubState, "Main");
		break;
	case UI_SUBSTATE_ACTIVATION_CORRUPTED:
		safecpy(szSubState, "LicenseCorrupted");
		break;
	case UI_SUBSTATE_ACTIVATION_EXPIRATION:
		safecpy(szSubState, "LicenseExpiration");
		break;
	case UI_SUBSTATE_ACTIVATION_RECOVERY:
		safecpy(szSubState, "LicenseRecovery");
		break;
	case UI_SUBSTATE_PATCHING_MESSAGE:
		safecpy(szSubState, "Message");
		break;
	case UI_SUBSTATE_PATCHING_DIALOG:
		safecpy(szSubState, "Dialog");
		break;
	case UI_SUBSTATE_FRIEND_REQUEST:
		safecpy(szSubState, "SendInvite");
		break;
	case UI_SUBSTATE_PROFILE:
		safecpy(szSubState, "Profile");
		break;
	case UI_SUBSTATE_REDEMPTION_CODE:
		safecpy(szSubState, "RedemptionCode");
		break;
	case UI_SUBSTATE_LINK_STEAM_ACCOUNT:
		safecpy(szSubState, "SteamLink");
		break;
	case UI_SUBSTATE_LINK_FACEBOOK_ACCOUNT:
		safecpy(szSubState, "Facebook");
		break;
	case UI_SUBSTATE_LINK_GOOGLE_ACCOUNT:
		safecpy(szSubState, "Google");
		break;
	case UI_SUBSTATE_LINK_TWITCHTV_ACCOUNT:
		safecpy(szSubState, "TwitchTV");
		break;
	case UI_SUBSTATE_FRIEND_SEARCH:
		safecpy(szSubState, "FriendSearch");
		break;
	}

	Assert((subState == UI_SUBSTATE_NONE) || (szSubState[0] != '\0'));

	json_push_back(n, json_new_b("Visible", visible));

	json_push_back(n, json_new_b("Online", m_LoadingStatus == LS_LOADED_ONLINE));

	if(szState[0] != '\0')
	{
		json_push_back(n, json_new_a("State", szState));
	}

	if(szSubState[0] != '\0')
	{
		json_push_back(n, json_new_a("SubState", szSubState));
	}

	if(data)
	{
		json_push_back(n, data);
	}

	json_char *jc = json_write_formatted(n);

	json_delete(n);

	rlDebug2("%s\n", jc);

	if(!AssertVerify((m_LoadingStatus == LS_LOADED_ONLINE) || (m_LoadingStatus == LS_LOADED_OFFLINE)))
	{
		return false;
	}

	m_WaitingForUiStateResponse = true;

	m_WaitingForUiToBecomeVisible = visible;

	bool succeeded = false;
	rgsc::Script::JsRequestUiState(&succeeded, jc);
	return succeeded;
}

bool RgscUi::Js2CUiStateResponse(const char* jsonResponse)
{
	m_WaitingForUiToBecomeVisible = false;

	if(!AssertVerify(m_WaitingForUiStateResponse))
	{
		return false;
	}

	m_WaitingForUiStateResponse = false;

	int visible = -1;

	JSONNODE *n = json_parse(jsonResponse);

	if(!AssertVerify(n != NULL))
	{
		return false;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return false;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "Visible") == 0)
		{
			visible = json_as_bool(*i) != 0 ? 1 : 0;
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	Assert((visible == 0) || (visible == 1));
	if(visible == 0)
	{
		m_ScUiVisible = false;
 		m_NumNotificationsVisible = 0;
		HideUiInternal(true);
	}
	else if(visible == 1)
	{
		m_ScUiVisible = true;
		ShowUiInternal();
	}

	return true;
}

void RgscUi::Js2CRequestUiState(const char* jsonRequest)
{
	int visible = -1;

	JSONNODE *n = json_parse(jsonRequest);

	if(!AssertVerify(n != NULL))
	{
		return;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "Visible") == 0)
		{
			visible = json_as_bool(*i) != 0 ? 1 : 0;
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	Assert(visible != -1);

	// we don't need to do anything to prepare currently,
	// just set visibility to requested state and respond immediately
	if(visible == 0)
	{
		m_ScUiVisible = false;
		m_NumNotificationsVisible = 0;
		HideUiInternal(true);
	}
	else if(visible == 1)
	{
		m_ScUiVisible = true;
		ShowUiInternal();
	}

	n = json_new(JSON_NODE);

	json_set_name(n, "StateResponse");

	json_push_back(n, json_new_b("Visible", visible));

	json_char *jc = json_write_formatted(n);

	rlDebug2("%s\n", jc);

	C2JsUiStateResponse(jc);

	json_delete(n);
}

void RgscUi::SetNotificationVisible(const char* jsonVisible)
{
	int visible = -1;

	JSONNODE *n = json_parse(jsonVisible);

	if(!AssertVerify(n != NULL))
	{
		return;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "Visible") == 0)
		{
			visible = json_as_bool(*i) != 0 ? 1 : 0;
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	Assert(visible != -1);

	if(visible == 0)
	{
		m_NumNotificationsVisible = 0; // the SCUI only sends "Visible" : false when *all* active toasts are finished.
		if((m_ScUiVisible == false))
		{
			HideUiInternal(false);
		}
	}
	else if(visible == 1)
	{
		//++m_NumNotificationsVisible;
		m_NumNotificationsVisible = 1;
		ShowUiInternal();
	}
}

void RgscUi::RaiseUiEvent(const char* jsonEvent)
{
	int eventId = -1;

	JSONNODE *n = json_parse(jsonEvent);

	if(!AssertVerify(n != NULL))
	{
		return;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "EventId") == 0)
		{
			eventId = json_as_int(*i) != 0 ? 1 : 0;
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	Assert(eventId > 0);

	GetRgscConcreteInstance()->HandleNotification(rgsc::NOTIFY_UI_EVENT, jsonEvent);
}

void RgscUi::RaiseDebugEvent(const wchar_t* jsonEvent)
{
	if (!PARAM_scDebugLogging.Get())
		return;

	RgscDisplayW(L"Debug message from Social Club:");
	RgscDisplayW(L"\t%ls", jsonEvent);
}

void RgscUi::RaiseRetailOutputEvent(const char* jsonEvent)
{
	if (!PARAM_scDebugLogging.Get())
		return;

	RgscDisplay("Output from Social Club:");
	RgscDisplay("\t%s", jsonEvent);
}

void RgscUi::RaiseImeEvent(const char* jsonEvent)
{
#if IME_TEXT_INPUT
	int eventId = -1;

	JSONNODE *n = json_parse(jsonEvent);

	if(!AssertVerify(n != NULL))
	{
		return;
	}

	JSONNODE_ITERATOR i = json_begin(n);
	while(i != json_end(n))
	{
		if(!AssertVerify(*i != NULL))
		{
			return;
		}

		// get the node name and value as a string
		json_char *node_name = json_name(*i);

		if(_stricmp(node_name, "EventId") == 0)
		{
			eventId = json_as_int(*i) != 0 ? 1 : 0;
		}

		// cleanup and increment the iterator
		json_free_safe(node_name);
		++i;
	}

	json_delete(n);

	Assert(eventId > 0);

	Rgsc_Input::HandleImeEvent(Rgsc_Input::ImeEvent(eventId));
#endif
}

void RgscUi::C2JsUiStateResponse(const char* jsonResponse)
{
	bool succeeded = false;
	rgsc::Script::JsUiStateResponse(&succeeded, jsonResponse);
}

void RgscUi::PageLoadComplete(int errorCode)
{
	Displayf("Social Club: PageLoadComplete(%d)", errorCode);
	AssertMsg(errorCode != -130, "Proxy error -- did you forget Fiddler again?");

#if ALLOW_ANY_WEB_SITE
	if(m_NonScuiUrl)
	{
		m_Window->ShowUi();
		return;
	}
#endif

	/* See chromium\src\net\base\net_error_list.h for error codes.
		-3 = ERR_ABORTED enum
		-7 = ERR_TIMED_OUT
		-401 = CACHE_READ_FAILURE
		-106 = INTERNET_DISCONNETED
	  SCUI timeout error codes
	    -1000 = TIMEOUT_WAITING_PAGE_LOAD,
	    -1001 = TIMEOUT_WAITING_JS_BINDINGS,
	    -1002 = TIMEOUT_WAITING_JS_PONG,
	    -1003 = TIMEOUT_WAITING_JS_READY,
	*/
	if(errorCode < 0)
	{
		RgscError("Page load error, code %d", errorCode);

		// try reload the page 'MAX_PAGE_RELOAD_ATTEMPTS' times
		if(m_NumPageReloadAttempts < MAX_PAGE_RELOAD_ATTEMPTS)
		{
			Displayf("Chrome reported an error loading the page, we will attempt to reload the page");
			++m_NumPageReloadAttempts;
			StartState(STATE_LOADING);
		}
		else
		{
			if (m_LoadingStatus == LS_LOADING_OFFLINE)
			{
				Displayf("Chrome reported an error after reloading the offline page, we will now enter a fail state.");
			}
			else
			{
				Displayf("Chrome reported an error after reloading the page, will attempt to load the offline website");
			}

			JavascriptFunctionDoesntExist("RGSC_JS_READY_TO_ACCEPT_COMMANDS");
		}
	}
	else
	{
		if(m_State == STATE_WAITING_FOR_PAGE_LOAD_COMPLETION)
		{
			StartState(STATE_WAITING_FOR_JAVASCRIPT_BINDINGS);
		}
	}
}

void RgscUi::SetReadyToAcceptCommands()
{
	Displayf("RgscUi::SetReadyToAcceptCommands");

	m_ReadyToAcceptCommands = true;

	if(m_State == STATE_WAITING_FOR_JAVASCRIPT_READY)
	{
		StartState(STATE_JAVASCRIPT_READY);
	}
}

bool RgscUi::IsReadyToAcceptCommands()
{
	//@@: location RSGCUI_ISREADYTOACCEPTCOMMAND
	return (m_State == STATE_FINISHED) ||
		  (GetRgscConcreteInstance()->IsUiDisabled() &&
		  (GetRgscConcreteInstance()->_GetProfileManager()->IsSignedInInternal() ||
		  GetRgscConcreteInstance()->_GetProfileManager()->IsSigningIn() == false));
}

void RgscUi::SetJavascriptReadyToAcceptCommands()
{
	Displayf("RgscUi::SetJavascriptReadyToAcceptCommands");

	m_JavascriptReadyToAcceptCommands = true;

	if(m_State == STATE_WAITING_FOR_JAVASCRIPT_BINDINGS)
	{
		StartState(STATE_WAITING_FOR_JAVASCRIPT_PONG);
	}
}

bool RgscUi::IsJavascriptReadyToAcceptCommands()
{
	return m_JavascriptReadyToAcceptCommands || GetRgscConcreteInstance()->IsUiDisabled();
}

void RgscUi::JavascriptFunctionDoesntExist(const char* functionName)
{
	if((m_ReadyToAcceptCommands == false) && 
	   strcmp(functionName, "RGSC_JS_READY_TO_ACCEPT_COMMANDS") == 0)
	{
		m_NumPageReloadAttempts = 0;
		Assert((m_LoadingStatus == LS_LOADING_ONLINE) || (m_LoadingStatus == LS_LOADING_OFFLINE));
		if(m_LoadingStatus == LS_LOADING_ONLINE)
		{
			// page failed to load or it's not the correct page.
			Displayf("Failed to load online web site, attempting to load offline web site...");
			RgscError("Failed to load social club UI, falling back to offline site");

			// either the web site is down, or we're not connected
			// to the internet. Load the offline page.
			m_LoadingStatus = LS_LOADING_OFFLINE;
			StartState(STATE_LOADING);
		}
		else if(m_LoadingStatus == LS_LOADING_OFFLINE)
		{
			m_LoadingStatus = LS_LOADING_FAILED;
			AssertMsg(false, "Failed to load both the online and offline web site");
			RgscError("Failed to load both the online and offline sites.");
			StartState(STATE_FAILED);
		}
	}
}

void RgscUi::ShowUiInternal()
{
	SYS_CS_SYNC(m_Cs);

	if(m_Window && (m_Window->IsUiVisible() == false))
	{
		if(m_ScUiVisible)
		{
			m_OldCursorInfo.cbSize = sizeof(CURSORINFO);
			GetCursorInfo(&m_OldCursorInfo);

#if DEBUG_CURSOR
			printf("Show: m_OldCursorInfo.flags: %d\n", m_OldCursorInfo.flags);
			fflush(stdout);
#endif

			m_WasUiJustShown = true;

			m_Window->ShowUi();
			m_Cursor = LoadCursor(NULL, IDC_ARROW);
		}
		else
		{
			m_Window->ShowUi();
		}
	}
}

void RgscUi::HideUiInternal(bool scUiWasVisible)
{
	SYS_CS_SYNC(m_Cs);

	if(m_Window && m_Window->IsUiVisible())
	{
		m_Window->HideUi();

		if(scUiWasVisible)
		{
			m_WasUiJustHidden = true;
		}
	}
}

bool RgscUi::IsVisible()
{
	// games expect our UI to behave the same way as the Xbox Live guide:
	// when a request is sent to show the UI, they expect IsVisible() to return true
	// immediately after the ShowUi() call returns. In reality, we send a request to javascript
	// to make the UI visible and wait for a response from javascript when it's ready
	// to be displayed. So if we're waiting for that response, tell the game we're visible.
	return (m_ScUiVisible && m_Window && m_Window->IsUiVisible()) || m_WaitingForUiToBecomeVisible;
}

bool RgscUi::IsOfflineMode()
{
	return m_LoadingStatus == LS_LOADING_OFFLINE || m_LoadingStatus == LS_LOADED_OFFLINE;
}

RGSC_HRESULT RgscUi::SendMessageToScui(const char* jsonMessage)
{
	bool succeeded = false;
	rgsc::Script::JsReceiveMessage(&succeeded, jsonMessage);
	return succeeded ? RGSC_OK : RGSC_FAIL;
}

void RgscUi::SetupScriptCommands()
{
	Assert(m_Window);
	rgsc::Script::SetupCommands(m_Window, 256);
}

void RgscUi::GetAchievementImageIpc(unsigned int achievementId, std::vector<char>* achievementImage)
{
	rtry
	{
		achievementImage->clear();
		RgscAchievementManager::ImageRecord imageRecord;
		rverify(GetRgscConcreteInstance()->_GetAchievementManager()->GetAchievementImageRecord(achievementId, &imageRecord), catchall, );
		achievementImage->resize(imageRecord.GetSize());
		memcpy(&(*achievementImage)[0], imageRecord.GetData(), imageRecord.GetSize());
	}
	rcatchall
	{
	}
}

void RgscUi::CreateIpcChannel(std::string* ipcChannelName)
{
	// add an IPC channel so the renderer process can call Javascript-bound functions
	// when the browser subprocess is about to launch a renderer subprocess, it requests a channel name from the DLL
	// and passes the name of the channel to the renderer subprocess as a commandline param.
	// So each renderer subprocess gets it's own channel to the DLL so it can call the DLL's Javascript-bound functions.
	char name[Ipc::MAX_CHANNEL_NAME_LEN] = {0};
	DWORD pid = GetCurrentProcessId();
	rage::formatf(name, "rgsc_ipc_%x_channel_%u", pid, GetRgscConcreteInstance()->_GetUiInterface()->_GetIpc()->GetNextChannelId());
	m_Ipc.AddChannel(Ipc::MODE_SERVER, name);
	*ipcChannelName = name;
	rlDebug2("CreateIpcChannel returning channel name: %s", ipcChannelName->c_str());
}

void RgscUi::OnWindowCreated(u64 browserHandle, u64 parentWindowHandle)
{
	m_BrowserHandle = (WindowHandle)browserHandle;
	m_WindowHandle = (WindowHandle)parentWindowHandle;
	
	rlDebug2("Browser window created with handle: %" I64FMT "u (optional parent: %" I64FMT "u)", m_BrowserHandle, m_WindowHandle);
	
	// Borderless Window must initialize input after window creation.
	if (m_WindowType == BORDERLESS)
	{
		Rgsc_Input::Init((WindowHandle)m_BrowserHandle, GetRgscConcreteInstance()->GetAdditionalWindowHandles(), GetRgscConcreteInstance()->GetNumAdditionalWindowHandles(), false);
		GetRgscConcreteInstance()->HandleNotification(rgsc::NOTIFY_BROWSER_CREATED, &m_WindowHandle);
	}
	else
	{
		GetRgscConcreteInstance()->HandleNotification(rgsc::NOTIFY_BROWSER_CREATED, &m_BrowserHandle);
	}
}

void RgscUi::OnWindowSizeChanged(int width, int height)
{
	m_RenderRect.m_Right = m_RenderRect.m_Left + width;
	m_RenderRect.m_Bottom = m_RenderRect.m_Top + height;
}

void RgscUi::OnWindowClosing()
{
	rlDebug2("Browser window closing/destroyed");
	CleanupWindow();
}

void RgscUi::OnRendererCrashed()
{
	rlDebug2("Renderer crashed");

	// Cache if we should display the SCUI.
	if (m_UiVisibleOnSubprocessReboot == -1)
	{
		m_UiVisibleOnSubprocessReboot = IsVisible() ? 1 : 0;
	}

	// Cleanup the window
	CleanupWindow();

	// Idle in a 'FAILED' state
	m_State = STATE_FAILED;

	// Shutdown the subprocess
	IPC_SEND(new ViewMsg_Shutdown());
}

void RgscUi::OnMouseEnterExitEvent(const char* jsonReason, MouseEnterExitEventType eventType)
{
	rtry
	{
		RsonReader rr(jsonReason, (unsigned)strlen(jsonReason));

		char identifier[256];
		rverify(rr.ReadString("identifier", identifier), catchall, );

		if (!stricmp(identifier,"caption"))
		{
			// ViewMsg_OnMouseInCaptionChanged param #2 is a bool for 'bIsInCaption'. 
			// Send true for 'ENTERED' and false for 'EXITED'
			IPC_SEND(new ViewMsg_OnMouseInCaptionChanged(1, eventType == RgscUi::MOUSE_ENTERED));
		}
	}
	rcatchall
	{

	}
}

void RgscUi::OnKeyEvent(s64 msg, s64 wParam, s64 lParam, s64 scKeyMods)
{
	if (m_KeyCallback)
	{
		m_KeyCallback(msg, wParam, lParam, (int)scKeyMods);
	}
}

void RgscUi::OnShutdownComplete()
{
	// clear flag
	m_bWaitingForScuiShutdown = false;

	// clear timer values
	m_uShutdownTimeoutMs = 0;
	m_uShutdownStartTime = 0;
}

void RgscUi::OnWindowStateChanged(WindowState windowState)
{
	char jsWindowState[256] = {0};

	switch(windowState)
	{
	case WS_NORMAL:
		rlDebug3("Window state is now 'normal'");
		break;
	case WS_MAXIMIZED:
		rlDebug3("Window state is now 'maximized'");
		break;
	case WS_MINIMIZED:
		rlDebug3("Window state is now 'minimized'");
		break;
	}

	rtry
	{
		RsonWriter rw;
		rw.Init(jsWindowState, sizeof(jsWindowState), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		rcheck(rw.WriteInt("windowState", (int)windowState), catchall, );
		rcheck(rw.End(), catchall, );
	}
	rcatchall
	{
		jsWindowState[0] = '\0';
	}

	bool succeeded;
	rgsc::Script::JsWindowStateChanged(&succeeded, jsWindowState);
}

void RgscUi::SubProcessDebugOutput(int severity, std::string message)
{
	if(Rgsc::IsInitialized())
	{
		GetRgscConcreteInstance()->Output((IRgscDelegateV1::OutputSeverity)severity, message.c_str());
	}
}

// this is used to send replies to synchronous IPC messages
void RgscUi::Send(IPC::Message* message)
{
	_GetIpc()->Send(message);
}

void RgscUi::OnMessageReceived(const IPC::Message& message)
{
	if(message.routing_id() == MSG_ROUTING_CONTROL)
	{
		bool handled = true;
		bool valid = false;

		IPC_BEGIN_MESSAGE_MAP_EX(RgscUi, message, valid)
			IPC_MESSAGE_HANDLER(ViewHostMsg_DebugOutput, SubProcessDebugOutput)
			IPC_MESSAGE_HANDLER(ViewHostMsg_GetAchievementImage, GetAchievementImageIpc)
			IPC_MESSAGE_HANDLER(ViewHostMsg_CreateIpcChannel, CreateIpcChannel)
			IPC_MESSAGE_UNHANDLED(handled = false)
		IPC_END_MESSAGE_MAP_EX()

		Assert(handled);
		Assert(valid);
	}
	else
	{
		// TODO: NS - get window out of map (key = message.routing_id())
		RgscWindow* window = m_Window;

		if(window)
		{
			window->OnMessageReceived(message);
		}
		else if(message.is_sync())
		{
			// The window doesn't exist, so we must respond or else the caller will hang waiting for a reply.
			IPC::Message* reply = IPC::SyncMessage::GenerateReply(&message);
			reply->set_reply_error();
			m_Ipc.Send(reply);
		}
		else
		{
			rlWarning("Received a message for an unknown window, discarding");
		}
	}
}

bool RgscUi::SendScuiPing()
{
	char jsPing[256] = {0};

	rtry
	{
		RsonWriter rw;
		rw.Init(jsPing, sizeof(jsPing), RSON_FORMAT_JSON);
		rcheck(rw.Begin(NULL, NULL), catchall, );
		rcheck(rw.End(), catchall, );
	}
	rcatchall
	{
		jsPing[0] = '\0';
	}

	bool succeeded;
	rgsc::Script::JsPing(&succeeded, jsPing);
	return succeeded;
}

void RgscUi::OnPongReceived()
{
	rlDebug("On Pong Received");
	m_PongReceived = true;
}

void RgscUi::OnPageLoadError(const char* jsonReason)
{
	rlDebug("Page Load Error from SCUI: %s", jsonReason);
	m_ScuiPageLoadError = true;
}

LRESULT RgscUi::TryForwardRgscPlatformMessage(WNDPROC originalProc, WindowHandle hwnd, RgscPlatformMessageHandler::RgscPlatformMsg rgscMsg, UINT msg, WPARAM wParam, LPARAM lParam)
{
	// Assert and early out
	if (!rlVerify(originalProc != 0))
	{
		return 0;
	}

	// Look for the message in our message map
	if (rgscMsg > RgscPlatformMessageHandler::RGSC_MSG_NULL)
	{
		RgscPlatformMessageHandler** v1 = m_MsgMap.Access(rgscMsg);
		if (v1)
		{
			RgscPlatformMessageHandler* handler = *v1;
			RgscPlatformMessageHandler::ResponseBehaviour behaviour = handler->GetResponseBehaviour(IsVisible());
			if (behaviour == RgscPlatformMessageHandler::FORWARD)
			{
				return CallWindowProc(originalProc, (HWND)hwnd, msg, wParam, lParam);
			}
			else
			{
				// Cast to LRESULT, will be 32-bit on Win32 build and truncate.
				return (LRESULT)handler->GetReturnValue(IsVisible());
			}
		}
	}

	// Look for the message in our range map
	for (atMap<RgscPlatformMessageHandler::RgscPlatformMsg,RgscPlatformMessageHandler*>::Iterator i = m_MsgRangeMap.CreateIterator(); !i.AtEnd(); i.Next()) 
	{
		RgscPlatformMessageHandler* handler = i.GetData();

		// Map the start and end values to the windows values
		UINT start = Rgsc_Input::MapRgscPlatformMsgToWindowsMsg(handler->GetStartMessageRange());
		UINT end = Rgsc_Input::MapRgscPlatformMsgToWindowsMsg(handler->GetEndMessageRange());
		
		// For range values, we map the RGSC range to the platform start/end (windows for now)
		if (msg >= start && msg <= end)
		{
			RgscPlatformMessageHandler::ResponseBehaviour behaviour = handler->GetResponseBehaviour(IsVisible());
			if (behaviour == RgscPlatformMessageHandler::FORWARD)
			{
				return CallWindowProc(originalProc, (HWND)hwnd, msg, wParam, lParam);
			}
			else
			{
				// Cast to LRESULT, will be 32-bit on Win32 build and truncate.
				return (LRESULT)handler->GetReturnValue(IsVisible());
			}
		}
	}

	return CallWindowProc(originalProc, (HWND)hwnd, msg, wParam, lParam);
}

} // namespace rgsc
