// This header is meant to be included in multiple passes, hence no traditional
// header guard.
// See ipc_message_macros.h for explanation of the macros and passes.

#include "base/shared_memory.h"
#include "ipc/ipc_message_macros.h"
#include "rgsc_messages_params.h"

// messages from the Social Club DLL to the subprocess
IPC_BEGIN_MESSAGES(View)

	IPC_MESSAGE_CONTROL6(ViewMsg_CreateTiledWindow,
						 unsigned int /*id*/,
						 unsigned int /*width*/,
						 unsigned int /*height*/,
						 float /*scale*/,				 
						 ViewMsg_Tile_Params /*params*/,
						 std::string /*sharedMemoryName*/)

	IPC_MESSAGE_CONTROL3(ViewMsg_CreateWindow,
						unsigned int /*id*/,
						ViewMsg_Rect_Params /*params*/,
						ViewMsg_CreateWindow_Params /* params */)

	IPC_MESSAGE_CONTROL0(ViewMsg_Hibernate)

	IPC_MESSAGE_CONTROL0(ViewMsg_Shutdown)

	IPC_MESSAGE_CONTROL1(ViewMsg_DestroyWindow,
						 unsigned int /*id*/)

	IPC_MESSAGE_ROUTED3(ViewMsg_MouseMoved,
						uint64 /*msg*/,
						uint64 /*wparam*/,
						uint64 /*lparam*/)

	IPC_MESSAGE_ROUTED3(ViewMsg_MouseButton,
						uint64 /*msg*/,
						uint64 /*wparam*/,
						uint64 /*lparam*/)

	IPC_MESSAGE_ROUTED2(ViewMsg_MouseWheel,
						int /*xScroll*/,
						int /*yScroll*/)

	IPC_MESSAGE_ROUTED4(ViewMsg_KeyEvent,
						uint64 /*msg*/,
						uint64 /*wparam*/,
						uint64 /*lparam*/,
						uint64 /*scModifiers*/)

	IPC_MESSAGE_ROUTED3(ViewMsg_NavigateTo,
						std::string /*url*/,
						std::string /*offlineRoot*/,
						std::string /*zipFile*/)

	IPC_MESSAGE_ROUTED1(ViewMsg_ExecuteJavascript,
						std::wstring /*javascript*/)

	IPC_MESSAGE_ROUTED2(ViewMsg_AddBindOnStartLoading,
						std::wstring /*functionName*/,
						bool /*synchronous*/)

	IPC_MESSAGE_ROUTED0(ViewMsg_OnLostDevice)

	IPC_MESSAGE_ROUTED5(ViewMsg_ResizeWindow,
						unsigned int /*width*/,
						unsigned int /*height*/,
						float /*scale*/,
						ViewMsg_Tile_Params /*params*/,
						std::string /*sharedMemoryName*/)

	IPC_MESSAGE_ROUTED5(ViewMsg_AddCaptionExclusion,
						unsigned int /* x */,
						unsigned int /* y */,
						unsigned int /* width */,
						unsigned int /* height */,
						unsigned int /* referencePoint */)

	IPC_MESSAGE_ROUTED0(ViewMsg_ClearCaptionExclusion)

	IPC_MESSAGE_ROUTED0(ViewMsg_CloseBrowser)

	IPC_MESSAGE_ROUTED1(ViewMsg_OnResizeOrMove,
						ViewMsg_Rect_Params /* rect */)

	IPC_MESSAGE_ROUTED1(ViewMsg_OnModalChanged,
						bool /* isModal */)

	IPC_MESSAGE_ROUTED1(ViewMsg_SetWindowCanClose,
						bool /* canClose */)

	IPC_MESSAGE_ROUTED1(ViewMsg_SetWindowModalBlocker,
						uint64 /*hWnd*/)

	IPC_MESSAGE_ROUTED1(ViewMsg_OnMouseInCaptionChanged,
						bool /* bIsInCaption */)

	IPC_MESSAGE_ROUTED1(ViewMsg_OnSetFocus,
						bool /* bEnabled */ )

	IPC_MESSAGE_ROUTED3(ViewMsg_OnCustomWindowMessage,
						uint64 /* msg */,
						uint64 /* wparam */,
						uint64 /* lparam */)
						

IPC_END_MESSAGES(View)

// messages from the subprocess to the Social Club DLL
IPC_BEGIN_MESSAGES(ViewHost)

	IPC_MESSAGE_CONTROL2(ViewHostMsg_DebugOutput,
						 int /*severity*/,
						 std::string /*message*/)

	IPC_MESSAGE_ROUTED1(ViewHostMsg_DebugRequestContents,
						std::string /*message*/)

	IPC_MESSAGE_ROUTED2(ViewHostMsg_DebugResponseContents,
						int /*status*/,
						std::string /*message*/)

	IPC_MESSAGE_ROUTED1(ViewHostMsg_CertificateError,
						int /* cert error */)

	IPC_MESSAGE_ROUTED1(ViewHostMsg_OnUrlRequested,
						std::string /* url */)

	IPC_MESSAGE_ROUTED1(ViewHostMsg_OnLoadingStateChanged,
						bool /*isLoading*/)

	IPC_MESSAGE_ROUTED1(ViewHostMsg_OnJavascriptCallbackAsync,
						ViewHostMsg_OnJavascriptCallback_Params /*params*/)

	IPC_SYNC_MESSAGE_ROUTED1_1(ViewHostMsg_OnJavascriptCallbackSync,
							   ViewHostMsg_OnJavascriptCallback_Params /*params*/,
							   ViewHostMsg_OnJavascriptCallback_ReturnVal /*[out] returnVal*/)

	IPC_MESSAGE_ROUTED1(ViewHostMsg_OnAddressBarChanged,
						std::string /*newURL*/)
	
	IPC_MESSAGE_ROUTED4(ViewHostMsg_OnProvisionalLoadError,
						std::string /*url*/,
						int /*errorCode*/,
						std::wstring /*errorText*/,
						bool /*isMainFrame*/)

	IPC_MESSAGE_ROUTED4(ViewHostMsg_OnConsoleMessage,
						int /*logLevel*/,
						std::wstring /*message*/,
						std::wstring /*sourceId*/,
						int /*line_no*/)

	IPC_MESSAGE_ROUTED4(ViewHostMsg_OnScriptAlert,
						std::wstring /*message*/,
						std::wstring /*defaultValue*/,
						std::string /*url*/,
						int /*flags*/)

	IPC_MESSAGE_ROUTED1(ViewHostMsg_OnTitleChanged,
						std::wstring /*title*/)

	IPC_MESSAGE_ROUTED1(ViewHostMsg_OnTooltipChanged,
						std::wstring /*text*/)

	IPC_MESSAGE_ROUTED0(ViewHostMsg_OnRendererCrashed)

	IPC_MESSAGE_ROUTED2(ViewHostMsg_OnCursorUpdated,
						uint64 /*hCursor*/,
						int /*cursorType*/)

	IPC_MESSAGE_ROUTED2(ViewHostMsg_OnWindowCreated,
						uint64 /*browserHWnd*/,
						uint64 /*optionalParentHWnd*/)

	IPC_MESSAGE_ROUTED2(ViewHostMsg_OnSizeChanged,
						int /* width */,
						int /* height */)

	IPC_MESSAGE_ROUTED3(ViewHostMsg_OnMouseMoved,
						uint64 /*msg*/,
						uint64 /*wparam*/,
						uint64 /*lparam*/)

	IPC_MESSAGE_ROUTED4(ViewHostMsg_OnKeyEvent,
						uint64 /*msg*/,
						uint64 /*wparam*/,
						uint64 /*lparam*/,
						uint64 /*scModifiers*/)

	IPC_MESSAGE_ROUTED0(ViewHostMsg_OnWindowClosing)

	IPC_MESSAGE_ROUTED2(ViewHostMsg_OnNonClientAreaMouseDown,
						uint64 /*wparam*/,
						uint64 /*lParam*/)

	IPC_MESSAGE_ROUTED1(ViewHostMsg_OnWindowStateChanged,
						int /* windowState - rgsc::WindowState */)

	IPC_SYNC_MESSAGE_CONTROL1_1(ViewHostMsg_GetAchievementImage,
								unsigned int /*achievementId*/,
								std::vector<char> /*[out] achievementImageBase64*/)

	IPC_SYNC_MESSAGE_CONTROL0_1(ViewHostMsg_CreateIpcChannel,
								std::string /*[out] ipcChannel*/)

IPC_END_MESSAGES(ViewHost)
