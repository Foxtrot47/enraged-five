//
// input/joystick.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_JOYSTICK_H
#define INPUT_JOYSTICK_H

#include "pad.h"

#if RGSC_DINPUT_SUPPORT

/* Unlike the keyboard and mouse, there can be more than one joystick,
	so this is not a pure static class. */

#if __WIN32PC
struct DIDEVICEINSTANCEA;
struct DIDEVICEINSTANCEW;
struct DIDEVICEOBJECTINSTANCEA;
struct DIDEVICEOBJECTINSTANCEW;
struct IDirectInputDevice2A;
struct IDirectInputDevice2W;

#ifndef GUID_DEFINED
#define GUID_DEFINED
typedef struct  _GUID
    {
    unsigned Data1;
    unsigned short Data2;
    unsigned short Data3;
    unsigned char Data4[ 8 ];
    }   GUID;
#endif	// GUID_DEFINED
#endif  // __WIN32PC

namespace rgsc
{

// PURPOSE:
//	This class should not be directly used in any projects because it only
//	works on the PC.  All projects should use the ioPad class instead, which
//	itself uses ioJoystick internally on PC builds.
// <FLAG Component>
class ioJoystick {
public:

	// PURPOSE:	Begin to enumerate each attached devices
	static void BeginAll();

	// PURPOSE:	Run Updates for all devices
	static void UpdateAll();

	// PURPOSE:	Run Poll function for each devices
	static void PollAll();

	// PURPOSE: Run End function for all devices
	static void EndAll();

	// PURPOSE:	Get the number of devices
	// RETURNS:	int - the number of devices
	static int GetStickCount();

	// PURPOSE:	Get the desire device refference
	// PARAMS:	i - the index for the device's array. 
	// RETURNS:	const ioJoystikc & - the const refferenced object
	static const ioJoystick& GetStick(int i);

	// PURPOSE: Get number of Axis that available in the device
	// RETURNS:	unsgined int - the total number of axis on the device
	unsigned GetAxisCount() const;

	// PURPOSE:	Get the number of button on the device
	// RETURNS:	unsgined int - the total number of buttons on the device
	unsigned GetButtonCount() const;

	// PURPOSE:	Get POV Count
	// RETURNS:	unsgined int - the total number of POV in the device
	unsigned GetPOVCount() const;
    
	// the order of the axes (active axes get sorted to the top)
	// these are the directInput meanings of each axis, and where that axis can be found
	enum { AXIS_LX, AXIS_LY, AXIS_LZ, AXIS_LRX, AXIS_LRY, AXIS_LRZ, AXIS_SLIDER0, AXIS_SLIDER1, NUM_AXES};
	
	// PURPOSE:	Get the desire axis
	// PARAMS:	axis - the index number of the Axes array
	// RETURNS:	int - the desire Axis
	int GetAxis(int axis) const;

	// PURPOSE:	Get the Norm Axis
	// PARAMS:	axis - the desire index from the Axes's array
	// RETURNS:	float - the Norm for the desire axis
	float GetNormAxis(int axis) const;

	// PURPOSE:	Get the button state bitsete
	// RETURNS:	unsigned int - bitset with 0 = button not pressed, 1 = button pressed.
	unsigned GetButtons() const;

	// PURPOSE: Gets the value of the nth analog button
	unsigned char GetAnalogButton(int btn) const;

	// PURPOSE: Gets the previous value of the nth analog button
	unsigned char GetLastAnalogButton(int btn) const;

	// PURPOSE:	Get the desire POV
	// PARAMS:	pov - the desire index from the POVs's array
	// RETURNS:	int - the desire POV
	int GetPOV(int pov) const;

	// PURPOSE:	Get the changed buttons
	// RETURNS:	unsigned int - the changed buttons
	unsigned GetChangedButtons() const;

	// PURPOSE:	Get Pressed Buttons
	// RETURNS:	unsigned int - the pressed buttons
	unsigned GetPressedButtons() const;

	// PURPOSE:	get Release Buttons
	// RETURNS:	unsigned int - the released buttons
	unsigned GetReleasedButtons() const;

	// PURPOSE:	Determine if this connected controller is a steering wheel
	// PARAMS:	NONE
	// RETURNS: true if this is a driving type device
	bool IsWheel() const;

	// set up Enum for EnumJoyType
	enum EnumJoyType {
		TYPE_UNKNOWN,
		TYPE_DIRECTPAD,
		TYPE_SMARTJOYPAD,
		TYPE_GAMEPADPRO,
		TYPE_XKPC2002,
		TYPE_DUALJOYPAD,
		TYPE_DUAL_USB_FFJ_MP866,
		TYPE_KIKYXSERIES,
		TYPE_LOGITECH_CORDLESS,
		TYPE_PSX_TO_USB_CONVERTER,
		TYPE_XBCD_XBOX_USB,
		TYPE_TIGERGAME_XBOX_USB,
		TYPE_XNA_GAMEPAD_XENON,
		TYPE_PS4_WIRELESS_PAD,
		TYPE_LOGITECH_DUAL,
		TYPE_SAITEK_P480,
		TYPE_SAITEK_CYBORG,
		TYPE_LOGITECH_RUMBLEPAD_2,
		NUM_TYPES};

	// PURPOSE:	Get the joystick type
	// RETURNS:	int - m_Type ( this joy type )
	int GetJoyType() const;

#if __WIN32PC
	// PURPOSE: Get the device type unique to that directinput device
	// RETURNS:	int - device type
	int GetDevType() const;

	// PURPOSE:	Get the name of the attached device
	// PARAMS:	name - wide character pointer to contain NULL terminated string; size - max length of string (including trailing 0)
	void GetProductName(unsigned short *name, unsigned size) const;


	bool IsValid() const;

	// PURPOSE:	function to get information about the attached stick for use in higher level game code
	// PARAMS:	info	- reference to a DIDEVICEINSTANCEA device information struct to be populated by this function
	// RETURNS: HRESULT returned from m_Device->GetDeviceInfo
	int GetDeviceInfo(DIDEVICEINSTANCEW &) const;

	// PURPOSE:	Display the capabilities of the directInput device
	void PrintDeviceCaps() const;
#endif

protected:
	// PURPOSE:	Begin to initialize the device 
	void Begin();

	// PURPOSE:	Poll the device. MUST call this before every Update!
	void Poll();

	// PURPOSE:	Update the device
	void Update();

	// PURPOSE:	Shut down device
	void End();


private:

	// Enum setup
	enum { MAX_STICKS = 4, MAX_AXES = 8,  MAX_BUTTONS = 32, MAX_POVS = 4 };

	// number of Axis, Buttons and POV count
	int m_AxisCount, m_ButtonCount, m_POVCount;

	// array of Axis
	int m_Axes[MAX_AXES];

	unsigned char m_AnalogButtons[MAX_BUTTONS], m_LastAnalogButtons[MAX_BUTTONS];

	// number of this buttons, and last buttons
	unsigned m_Buttons, m_LastButtons;

	// intentional, because some drivers mess up and report 0xffff instead of -1
	// highest value a pov will likely return is 7/8*36000, which still fits
	// in a signed short.
	short m_POVs[MAX_POVS];	

	// type of the device
	EnumJoyType	m_Type;

#if __WIN32PC
	GUID m_Guid;
	IDirectInputDevice2W *m_Device;

	// PURPOSE:	callback function for EnumDeviceProc
	// PARAMS:	lpddi	- long pointer to the device instance 
	//			LPVOID	- long void pointer
	// RETURNS: DIENUM_CONTINUE
	static int __stdcall EnumDeviceProc(const DIDEVICEINSTANCEW*,void*);
	static int __stdcall EnumObjectProc(const DIDEVICEOBJECTINSTANCEW*,void*);
#endif

	// static arry member for the maximum device
	static ioJoystick sm_Sticks[MAX_STICKS];

	// static number of the device count
	static int sm_StickCount;

public:
	char productName[256];

};


inline int ioJoystick::GetStickCount() 
{
	return sm_StickCount;
}

inline const ioJoystick& ioJoystick::GetStick(int i)
{
	return sm_Sticks[i];
}

inline unsigned ioJoystick::GetAxisCount() const
{
	return m_AxisCount;
}

inline unsigned ioJoystick::GetButtonCount() const
{
	return m_ButtonCount;
}

inline unsigned ioJoystick::GetPOVCount() const
{
	return m_POVCount;
}

inline int ioJoystick::GetAxis(int axis) const
{
	return m_Axes[axis];
}

inline float ioJoystick::GetNormAxis(int axis) const
{
	return (m_Axes[axis] - 32768) * (1.0f / 32768);
}

inline unsigned ioJoystick::GetButtons() const
{
	return m_Buttons;
}

inline unsigned char ioJoystick::GetAnalogButton(int btn) const
{
	return m_AnalogButtons[btn];
}

inline unsigned char ioJoystick::GetLastAnalogButton(int btn) const
{
	return m_LastAnalogButtons[btn];
}

inline int ioJoystick::GetPOV(int pov) const
{
	return m_POVs[pov];
}

inline unsigned ioJoystick::GetChangedButtons() const
{
	return m_LastButtons ^ GetButtons();
}

inline unsigned ioJoystick::GetPressedButtons() const
{
	return GetChangedButtons() & GetButtons();
}

inline unsigned ioJoystick::GetReleasedButtons() const
{
	return GetChangedButtons() & m_LastButtons;
}

inline int ioJoystick::GetJoyType() const
{
	return m_Type;
}

} // namespace rgsc

#endif // RGSC_DINPUT_SUPPORT
#endif // INPUT_JOYSTICK_H
