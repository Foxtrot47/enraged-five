#include "graphics_d3d11.h"

#include "diag/seh.h"
#include "cursor.h"
#include "file/limits.h"
#include "input.h"
#include "math/amath.h"
#include "rgsc_common.h"
#include "tiledbuffer.h"
#include "string/string.h"
#include "system/new.h"
#include "system/param.h"
#include "system/timer.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <windows.h>
#include <d3dx11.h>
#include <d3dcompiler.h>
#include <xnamath.h>
#include <shlobj.h>
#include <stdio.h>
#pragma warning(pop)

// ===================================================
//				ENABLE_SAVE_PIXEL_DATA
//	Includes for rendering SCUI pixel data to a bitmap
// ===================================================
#if ENABLE_SAVE_PIXEL_DATA

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>

namespace Gdiplus
{
	using std::min;
	using std::max;
};

#include <GdiPlus.h>
#pragma comment(lib,"gdiplus.lib")

#endif // ENABLE_SAVE_PIXEL_DATA
// ====================================================

using namespace rage;

PARAM(scuiRenderSaveLocation, "Sets the folder to save the SCUI pixeldata/textures without a trailing slash (i.e. x:\\scui_textures ");
PARAM(scuiRenderSaveIntervalMs, "Sets the time interval in milliseconds between saving the SCUI pixeldata/textures to file (default 5000)");

namespace rgsc
{

// Vertex shader data. This was simply dumped out after compiling the shaders with D3DX11CompileFromFileA.
static unsigned char sm_CompiledVertexShader[] = {
	68,88,66,67,126,143,218,193,241,69,115,24,136,202,161,19,189,105,78,18,1,0,0,0,36,
	2,0,0,5,0,0,0,52,0,0,0,140,0,0,0,224,0,0,0,56,1,0,0,168,1,0,0,82,68,69,70,80,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,28,0,0,0,0,4,254,255,0,129,4,0,28,0,0,0,77,105,99,114,111,
	115,111,102,116,32,40,82,41,32,72,76,83,76,32,83,104,97,100,101,114,32,67,111,109,
	112,105,108,101,114,32,57,46,50,57,46,57,53,50,46,51,49,49,49,0,171,171,171,73,83,
	71,78,76,0,0,0,2,0,0,0,8,0,0,0,56,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,15,15,0,0,
	65,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,1,0,0,0,3,3,0,0,80,79,83,73,84,73,79,78,0,84,69,
	88,67,79,79,82,68,0,171,171,79,83,71,78,80,0,0,0,2,0,0,0,8,0,0,0,56,0,0,0,0,0,0,0,
	1,0,0,0,3,0,0,0,0,0,0,0,15,0,0,0,68,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,1,0,0,0,3,12,0,0,
	83,86,95,80,79,83,73,84,73,79,78,0,84,69,88,67,79,79,82, 68,0,171,171,171,83,72,68,
	82,104,0,0,0,64,0,1,0,26,0,0,0,95,0,0,3,242,16,16,0,0,0,0,0,95,0,0,3,50,16,16,0,1,
	0,0,0,103,0,0,4,242,32,16,0,0,0,0,0,1,0,0,0,101,0,0,3,50,32,16,0,1,0,0,0,54,0,0,5,
	242,32,16,0,0,0,0,0,70,30,16,0,0,0,0,0,54,0,0,5,50,32,16,0,1,0,0,0,70,16,16,0,1,0,
	0,0,62,0,0,1,83,84,65,84,116,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0
};

// Pixel shader data. This was simply dumped out after compiling the shaders with D3DX11CompileFromFileA.
static unsigned char sm_CompiledPixelShader[] = {
	68,88,66,67,63,156,114,48,83,108,37,59,213,107,200,124,172,31,44,146,1,0,0,0,84,2,
	0,0,5,0,0,0,52,0,0,0,224,0,0,0,56,1,0,0,108,1,0,0,216,1,0,0,82,68,69,70,164,0,0,0,
	0,0,0,0,0,0,0,0,2,0,0,0,28,0,0,0,0,4,255,255,0,129,4,0,112,0,0,0,92,0,0,0,3,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,102,0,0,0,2,0,0,0,5,0,0, 0,4,0,0,
	0,255,255,255,255,0,0,0,0,1,0,0,0,13,0,0,0,115,97,109,76,105,110,101,97,114,0,116,
	120,68,105,102,102,117,115,101,0,77,105,99,114,111,115,111,102,116,32,40,82,41,32,
	72,76,83,76,32,83,104,97,100,101,114,32,67,111,109,112,105,108,101,114,32,57,46,
	50,57,46,57,53,50,46,51,49,49,49,0,171,171,171,73,83,71,78,80,0,0,0,2,0,0,0,8,0,0,
	0,56,0,0,0,0,0,0,0,1,0,0,0,3,0,0,0,0,0,0,0,15,0,0,0,68,0,0,0,0,0,0,0,0,0,0,0,3,0,
	0,0,1,0,0,0,3,3,0,0,83,86,95,80,79,83,73,84,73,79,78,0,84,69,88,67,79,79,82,68,0,
	171,171,171,79,83,71,78,44,0,0,0,1,0,0,0,8,0,0,0,32,0,0,0,0,0,0,0,0,0,0,0,3,0,0,
	0,0,0,0,0,15,0,0,0,83,86,95,84,65,82,71,69,84,0,171,171,83,72,68,82,100,0,0,0,64,
	0,0,0,25,0,0,0,90,0,0,3,0,96,16,0,0,0,0,0,88,24,0,4,0,112,16,0,0,0,0,0,85,85,0,0,
	98,16,0,3,50,16,16,0,1,0,0,0,101,0,0,3,242,32,16,0,0,0,0,0,69,0,0,9,242,32,16,0,
	0,0,0,0,70,16,16,0,1,0,0,0,70,126,16,0,0,0,0,0,0,96,16,0,0,0,0,0,62,0,0,1,83,84,
	65,84,116,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0
};

//--------------------------------------------------------------------------------------
// Structures
//--------------------------------------------------------------------------------------

#if 0
static bool GetShaderPath(char (&path)[MAX_PATH])
{
	bool success = false;
	path[0] = '\0';

	success = SHGetSpecialFolderPathA(NULL, path, CSIDL_PROGRAM_FILES, false) == TRUE;

#if __DEV
	strcat_s(path, "\\Rockstar Games\\Social Club Debug\\shader.fx");
#else
	strcat_s(path, "\\Rockstar Games\\Social Club\\shader.fx");
#endif

	return success;
}

HRESULT CompileShaderFromFile(LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut)
{
	HRESULT hr = RGSC_OK;

	char shaderPath[MAX_PATH] = {0};
	if(GetShaderPath(shaderPath) == false)
	{
		return S_FALSE;
	}

	DWORD dwShaderFlags = /*D3DCOMPILE_ENABLE_STRICTNESS | */D3DCOMPILE_WARNINGS_ARE_ERRORS | D3DCOMPILE_OPTIMIZATION_LEVEL3;
#if 0 //defined(DEBUG) || defined(_DEBUG)
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

	ID3DBlob* pErrorBlob;

	D3D10_SHADER_MACRO defines[] = {"__DX11", "1", NULL, NULL};
	hr = D3DX11CompileFromFileA(shaderPath, defines, NULL, szEntryPoint, szShaderModel, dwShaderFlags, 0, NULL, ppBlobOut, &pErrorBlob, NULL);
	if(FAILED(hr))
	{
		if(pErrorBlob != NULL)
			OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());
		if(pErrorBlob) pErrorBlob->Release();
		return hr;
	}
	if(pErrorBlob) pErrorBlob->Release();

	return RGSC_OK;
}
#endif

HRESULT Rgsc_Graphics_D3D11::CreateTextures()
{
	D3D11_TEXTURE2D_DESC oDesc;
	oDesc.MipLevels = 1;
	oDesc.ArraySize = 1;
#if ENABLE_SAVE_TEXTURE_DATA
	oDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // the d3d11 save to file function requires rgba - colors will be swapped.
#else
	oDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM; // the chrome pixel data uses bgra
#endif
	oDesc.SampleDesc.Count = 1;
	oDesc.SampleDesc.Quality = 0;
	oDesc.Usage = D3D11_USAGE_DYNAMIC;
	oDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	oDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	oDesc.MiscFlags = 0;

	D3D11_SHADER_RESOURCE_VIEW_DESC oViewDesc;
	oViewDesc.Format = oDesc.Format;
	oViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	oViewDesc.Texture2D.MipLevels = oDesc.MipLevels;
	oViewDesc.Texture2D.MostDetailedMip = 0;
	oViewDesc.Texture2DArray.FirstArraySlice = 0;
	oViewDesc.Texture2DArray.ArraySize = oDesc.ArraySize;

	m_Textures = rage_new ID3D11Texture2D*[m_Tiles->getNumTiles()];
	m_TextureResourceViews = rage_new ID3D11ShaderResourceView*[m_Tiles->getNumTiles()];

	HRESULT hr = RGSC_OK;
	for(unsigned int i = 0; i < m_Tiles->getNumTiles(); i++)
	{
		Tile* tile = m_Tiles->getTile(i);
		oDesc.Width = tile->getWidth();
		oDesc.Height = tile->getHeight();

		hr = m_Device->CreateTexture2D(&oDesc, NULL, &m_Textures[i]);
		if(FAILED(hr))
		{
			return hr;
		}

		hr = m_Device->CreateShaderResourceView((ID3D11Resource*)m_Textures[i], &oViewDesc, &m_TextureResourceViews[i]);
		if(FAILED(hr))
		{
			return hr;
		}
	}

	return hr;
}

Rgsc_Graphics_D3D11::Rgsc_Graphics_D3D11(ID3D11Device* device, void* params, const RenderRect& rect)
: m_Device(NULL)
, m_VertexShader(NULL)
, m_PixelShader(NULL)
, m_VertexLayout(NULL)
, m_VertexBuffer(NULL)
, m_TextureResourceViews(NULL)
, m_TextureSampler(NULL)
, m_RasterizerState(NULL)
, m_DepthStencilState(NULL)
, m_BlendState(NULL)
, m_Textures(NULL)
, m_DeviceContext(NULL)
#if ENABLE_SAVE_TEXTURE_DATA
, m_LastTextureSaveTime(sysTimer::GetSystemMsTime())
#endif
#if ENABLE_SAVE_PIXEL_DATA
, m_LastPixelSaveTime(sysTimer::GetSystemMsTime())
#endif
{
	DXGI_SWAP_CHAIN_DESC* sd = (DXGI_SWAP_CHAIN_DESC*)params;

	AssertVerify(SUCCEEDED(Init(device, sd, rect)));
}

Rgsc_Graphics_D3D11::~Rgsc_Graphics_D3D11()
{
	Shutdown();
}

bool Rgsc_Graphics_D3D11::OnLost()
{
	ReleaseResources();
	return true;
}

bool Rgsc_Graphics_D3D11::OnReset(void *params, const RenderRect& rect)
{
	DXGI_SWAP_CHAIN_DESC* sd = (DXGI_SWAP_CHAIN_DESC*)params;
	return SUCCEEDED(Init(m_Device, sd, rect));
}

void Rgsc_Graphics_D3D11::SetContext(void* context)
{ 
	m_DeviceContext = static_cast<ID3D11DeviceContext*>(context);
}

bool Rgsc_Graphics_D3D11::MapMouse(int &x, int &y)
{
	if((x < (int)m_MappedOffsetX) || (x > (int)(m_MappedWidth + m_MappedOffsetX)))
	{
		return false;
	}

	if((y < (int)m_MappedOffsetY) || (y > (int)(m_MappedHeight + m_MappedOffsetY)))
	{
		return false;
	}

	x = (int)MapNumFromRangeToRange((float)x, (float)m_MappedOffsetX, (float)m_MappedWidth + (float)m_MappedOffsetX, (float)0.0f, (float)m_Width);
	y = (int)MapNumFromRangeToRange((float)y, (float)m_MappedOffsetY, (float)m_MappedHeight + (float)m_MappedOffsetY, 0.0f, (float)m_Height);

	return true;
}

HRESULT Rgsc_Graphics_D3D11::Init(ID3D11Device* device, DXGI_SWAP_CHAIN_DESC* sd, const RenderRect& rect)
{
	HRESULT hr = S_OK;

	m_Rect = rect;

	m_Device = device;

	m_WindowHandle = sd->OutputWindow;

	RECT rc;
	GetClientRect(m_WindowHandle, &rc);
	m_ClientWidth = rc.right - rc.left;
	m_ClientHeight = rc.bottom - rc.top;

	m_OffsetX = 0;
	m_OffsetY = 0;
	m_MappedOffsetX = 0;
	m_MappedOffsetY = 0;

	m_Width = sd->BufferDesc.Width;
	m_Height = sd->BufferDesc.Height;

	if((m_Width != m_ClientWidth) || (m_Height != m_ClientHeight))
	{
		Warningf("Window's Client Area (%d x %d) does not match the resolution of the D3D Device (%d x %d). "
				 "This might be unintentional. The Social Club UI will still work fine.",
				 m_ClientWidth, m_ClientHeight, m_Width, m_Height);
	}

	if(m_Rect.IsSet())
	{
		if(AssertVerify(m_Rect.GetWidth() <= m_Width && m_Rect.GetHeight() <= m_Height))
		{
			m_OffsetX = m_Rect.m_Left;
			m_OffsetY = m_Rect.m_Top;

			m_MappedOffsetX = (int)MapNumFromRangeToRange((float)m_OffsetX, (float)0.0f, (float)m_Width, (float)0.0f, (float)m_ClientWidth);
			m_MappedOffsetY = (int)MapNumFromRangeToRange((float)m_OffsetY, (float)0.0f, (float)m_Height, (float)0.0f, (float)m_ClientHeight);

			m_Width = m_Rect.GetWidth();
			m_Height = m_Rect.GetHeight();
		}
		else
		{
			Errorf("Render Rect is outside of the frame buffer, ignoring.");
		}
	}

	m_MappedWidth = (int)MapNumFromRangeToRange((float)m_Width, (float)0.0f, (float)sd->BufferDesc.Width, (float)0.0f, (float)m_ClientWidth);
	m_MappedHeight = (int)MapNumFromRangeToRange((float)m_Height, (float)0.0f, (float)sd->BufferDesc.Height, (float)0.0f, (float)m_ClientHeight);

	// Setup the viewport
	m_Viewport.Width = (FLOAT)m_Width;
	m_Viewport.Height = (FLOAT)m_Height;
	m_Viewport.MinDepth = 0.0f;
	m_Viewport.MaxDepth = 1.0f;
	m_Viewport.TopLeftX = (float)m_OffsetX;
	m_Viewport.TopLeftY = (float)m_OffsetY;

#if 0
	// Compile the vertex shader
	ID3DBlob* pVSBlob = NULL;
	hr = CompileShaderFromFile("VS", "vs_4_0", &pVSBlob);
	if(FAILED(hr))
	{
		return hr;
	}

	{
		fflush(stdout);
		printf("DX11 Vertex Shader:\n");
		fflush(stdout);
		DWORD bufSize = pVSBlob->GetBufferSize();
		unsigned char* ptr = (unsigned char*)pVSBlob->GetBufferPointer();
		for(DWORD i = 0; i < bufSize; i++)
		{
			printf("%d,", ptr[i]);
			fflush(stdout);
		}
		printf("End of Vertex Shader\n\n");
		fflush(stdout);
	}

	// Create the vertex shader
	hr = m_Device->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &m_VertexShader);

	if(FAILED(hr))
	{    
		pVSBlob->Release();
		return hr;
	}

	pVSBlob->Release();
#else
	// Create the vertex shader
	hr = m_Device->CreateVertexShader(sm_CompiledVertexShader, sizeof(sm_CompiledVertexShader), NULL, &m_VertexShader);
#endif

	// Create the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0},
	};
	UINT numElements = ARRAYSIZE(layout);

	hr = m_Device->CreateInputLayout(layout, numElements, sm_CompiledVertexShader, sizeof(sm_CompiledVertexShader), &m_VertexLayout);
	if(FAILED(hr))
	{
		return hr;
	}

#if 0
	// Compile the pixel shader
	ID3DBlob* pPSBlob = NULL;
	hr = CompileShaderFromFile("PS", "ps_4_0", &pPSBlob);
	if(FAILED(hr))
	{
		return hr;
	}

	{
		fflush(stdout);
		printf("DX11 Pixel Shader:\n");
		fflush(stdout);
		DWORD bufSize = pPSBlob->GetBufferSize();
		unsigned char* ptr = (unsigned char*)pPSBlob->GetBufferPointer();
		for(DWORD i = 0; i < bufSize; i++)
		{
			printf("%d,", ptr[i]);
			fflush(stdout);
		}
		printf("End of Pixel Shader\n\n");
		fflush(stdout);
	}

	// Create the pixel shader
	hr = m_Device->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &m_PixelShader);
	pPSBlob->Release();
	if(FAILED(hr))
	{
		return hr;
	}
#else
	// Create the pixel shader
	hr = m_Device->CreatePixelShader(sm_CompiledPixelShader, sizeof(sm_CompiledPixelShader), NULL, &m_PixelShader);
	if(FAILED(hr))
	{
		return hr;
	}
#endif

	m_Tiles = rage_new TiledBuffer(m_Width, m_Height);

	SimpleVertex11 *allVertices = rage_new SimpleVertex11[4 * m_Tiles->getNumTiles()];

	for(unsigned int i = 0; i < m_Tiles->getNumTiles(); i++)
	{
		Tile* tile = m_Tiles->getTile(i);

		float left = MapNumFromRangeToRange((float)tile->getLeft(), 0.0f, (float)m_Width, -1.0f, 1.0f);
		float top = MapNumFromRangeToRange((float)tile->getTop(), 0.0f, (float)m_Height, 1.0f, -1.0f);
		float right = left + MapNumFromRangeToRange((float)tile->getWidth(), 0.0f, (float)m_Width, 0.0f, 2.0f);
		float bottom = top - MapNumFromRangeToRange((float)tile->getHeight(), 0.0f, (float)m_Height, 0.0f, 2.0f);

		// Create vertex buffer
		SimpleVertex11 vertices[] =
		{
			{XMFLOAT3(left, top, 0.0f), XMFLOAT2(0.0f, 0.0f)},
			{XMFLOAT3(left, bottom, 0.0f), XMFLOAT2(0.0f, 1.0f)},
			{XMFLOAT3(right, top, 0.0f), XMFLOAT2(1.0f, 0.0f)},
			{XMFLOAT3(right, bottom, 0.0f), XMFLOAT2(1.0f, 1.0f)},
		};

		memcpy(&allVertices[i * 4], vertices, sizeof(SimpleVertex11) * 4);
	}

	// Cache the virtual pointer vertices
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(SimpleVertex11) * 4 * m_Tiles->getNumTiles();
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = allVertices;

	hr = m_Device->CreateBuffer(&bd, &InitData, &m_VertexBuffer);

	delete [] allVertices;

	if(FAILED(hr))
	{
		return hr;
	}

	// Create textures
	hr = CreateTextures();
	if(FAILED(hr))
	{
		return hr;
	}

	// Create the virtual pointer
	g_VirtualCursor.InitD3D11(m_Device);

	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT; // because bilinear filtering makes text look blurry
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.MipLODBias = 0.0f;
	sampDesc.MaxAnisotropy = 0;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.BorderColor[0] = 0;
	sampDesc.BorderColor[1] = 0;
	sampDesc.BorderColor[2] = 0;
	sampDesc.BorderColor[3] = 0;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = m_Device->CreateSamplerState(&sampDesc, &m_TextureSampler);
	if(FAILED(hr))
	{
		return hr;
	}

	// Create rasterizer state block
	D3D11_RASTERIZER_DESC rasterizerDesc;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.FrontCounterClockwise = true;
	rasterizerDesc.DepthBias = false;
	rasterizerDesc.DepthBiasClamp = 0;
	rasterizerDesc.SlopeScaledDepthBias = 0;
	rasterizerDesc.DepthClipEnable = 0;
	rasterizerDesc.ScissorEnable = false;
	rasterizerDesc.MultisampleEnable = false;
	rasterizerDesc.AntialiasedLineEnable = false;
	hr = m_Device->CreateRasterizerState(&rasterizerDesc, &m_RasterizerState);
	if(FAILED(hr))
	{
		return hr;
	}

	// Create depth/stencil state block
	D3D11_DEPTH_STENCIL_DESC dsDesc;
	dsDesc.DepthEnable = FALSE;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;

	dsDesc.StencilEnable = FALSE;
//	dsDesc.StencilReadMask = 0xFF;
//	dsDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing
// 	dsDesc.FrontFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
// 	dsDesc.FrontFace.StencilDepthFailOp = FailOp[i];
// 	dsDesc.FrontFace.StencilPassOp = PassOp[i];
// 	dsDesc.FrontFace.StencilFunc = D3D10_COMPARISON_ALWAYS;
// 
// 	// Stencil operations if pixel is back-facing
// 	dsDesc.BackFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
// 	dsDesc.BackFace.StencilDepthFailOp = FailOp[i];
// 	dsDesc.BackFace.StencilPassOp = PassOp[i];
// 	dsDesc.BackFace.StencilFunc = D3D10_COMPARISON_ALWAYS;

	hr = m_Device->CreateDepthStencilState(&dsDesc, &m_DepthStencilState);
	if(FAILED(hr))
	{
		return hr;
	}

	// Create blend state block
	D3D11_BLEND_DESC blendState;

	blendState.AlphaToCoverageEnable = FALSE;
	blendState.IndependentBlendEnable = FALSE;

	blendState.RenderTarget[0].BlendEnable = TRUE;

	blendState.RenderTarget[0].SrcBlend  = D3D11_BLEND_SRC_ALPHA;
	blendState.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendState.RenderTarget[0].BlendOp   = D3D11_BLEND_OP_ADD;

	blendState.RenderTarget[0].SrcBlendAlpha  = D3D11_BLEND_INV_DEST_ALPHA;
	blendState.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blendState.RenderTarget[0].BlendOpAlpha   = D3D11_BLEND_OP_ADD;

	blendState.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	hr = m_Device->CreateBlendState(&blendState, &m_BlendState);
	if(FAILED(hr))
	{
		return hr;
	}

	return S_OK;
}

void Rgsc_Graphics_D3D11::ReleaseResources()
{
	if(m_Textures)
	{
		for(unsigned int i = 0; i < m_Tiles->getNumTiles(); i++)
		{
			if(m_Textures[i])
			{
				m_Textures[i]->Release();
				m_Textures[i] = NULL;
			}
		}
		delete [] m_Textures;
		m_Textures = NULL;
	}

	if(m_TextureResourceViews)
	{
		for(unsigned int i = 0; i < m_Tiles->getNumTiles(); i++)
		{
			if(m_TextureResourceViews[i])
			{
				m_TextureResourceViews[i]->Release();
				m_TextureResourceViews[i] = NULL;
			}
		}
		delete [] m_TextureResourceViews;
		m_TextureResourceViews = NULL;
	}

	if(m_VertexBuffer)
	{
		m_VertexBuffer->Release();
		m_VertexBuffer = NULL;
	}

	if(m_TextureSampler)
	{
		m_TextureSampler->Release();
		m_TextureSampler = NULL;
	}

	if(m_VertexLayout)
	{
		m_VertexLayout->Release();
		m_VertexLayout = NULL;
	}

	if(m_VertexShader)
	{
		m_VertexShader->Release();
		m_VertexShader = NULL;
	}

	if(m_PixelShader)
	{
		m_PixelShader->Release();
		m_PixelShader = NULL;
	}

	if(m_RasterizerState)
	{
		m_RasterizerState->Release();
		m_RasterizerState = NULL;
	}

	if(m_DepthStencilState)
	{
		m_DepthStencilState->Release();
		m_DepthStencilState = NULL;
	}

	if(m_BlendState)
	{
		m_BlendState->Release();
		m_BlendState = NULL;
	}

	if(m_Tiles)
	{
		delete m_Tiles;
		m_Tiles = NULL;
	}
}

void Rgsc_Graphics_D3D11::Shutdown()
{
	ReleaseResources();

	// we added a ref to the device when we called QueryInterface in graphics.cpp
	if(m_Device)
	{
		m_Device->Release();
		m_Device = NULL;
	}
}

void Rgsc_Graphics_D3D11::UpdateTextures(bool onlyNotifications, ID3D11DeviceContext* context)
{
	if(m_Tiles)
	{
		for(unsigned int i = 0; i < m_Tiles->getNumTiles(); i++)
		{
			Tile* tile = m_Tiles->getTile(i);
			tile->setVisible(true);

			unsigned short currentFrame;
			bool isDirty = tile->isDirty(currentFrame);
			if(isDirty && onlyNotifications)
			{
				// TODO: NS - temporary hack to only render the notification area when the rest of the SCUI is invisible
				// This is necessary because when a notification comes on screen, Chrome/CEF says the entire
				// screen is dirty and we stall the game every time a notification comes on screen. 

				int scale = (m_Height > 1080) ? 2 : 1;
				const unsigned maxNotificationWidth = 256 * scale;
				const unsigned maxNotificationAreaHeight = 500 * scale;
				TileRect notificationRect;
				notificationRect.m_Left = (maxNotificationWidth < m_Width) ? (m_Width - maxNotificationWidth) : 0;
				notificationRect.m_Top = 0;
				notificationRect.m_Right = m_Width;
				notificationRect.m_Bottom = maxNotificationAreaHeight;
				if(notificationRect.m_Bottom > m_Height)
				{
					notificationRect.m_Bottom = m_Height;
				}
				isDirty = tile->getRect().IsIntersecting(notificationRect);
				if(isDirty == false)
				{
					tile->setVisible(false);
				}
			}

			if(isDirty)
			{
				D3D11_MAPPED_SUBRESOURCE oMap;
				HRESULT hRes = context->Map(static_cast<ID3D11Resource*>(m_Textures[i]), 0, D3D11_MAP_WRITE_DISCARD, 0, &oMap);

				if(hRes == S_OK)
				{
					unsigned char* pbyDst = (unsigned char*)(oMap.pData);
					unsigned char* pbySrc = tile->getBuffer();
					unsigned int rowSize = tile->getRowSizeInBytes();
					unsigned int srcRowPitch = m_Tiles->getRowPitch();
					unsigned int dstRowPitch = oMap.RowPitch;

					if(srcRowPitch == dstRowPitch)
					{
						// if the texture is the full width of the screen, we can copy it in one memcpy
						memcpy(pbyDst, pbySrc, rowSize * tile->getEffectiveHeight());
					}
					else
					{
						for(unsigned int row = 0; row < tile->getEffectiveHeight(); row++)
						{
							Assert((pbySrc + rowSize) <= (m_Tiles->getBuffer() + m_Tiles->getBufferSize()));
							Assert(pbySrc >= m_Tiles->getBuffer());

							memcpy(pbyDst, pbySrc, rowSize);
							pbySrc += srcRowPitch;
							pbyDst += dstRowPitch;
						}
					}

					context->Unmap(static_cast<ID3D11Resource*>(m_Textures[i]), 0);
				}

				tile->setLastUpdatedFrame(currentFrame);
			}
		}
	}
}

struct D3DX11_STATE_BLOCK
{
    ID3D11VertexShader*       VS;
    ID3D11ClassInstance*      VSInterfaces[D3D11_SHADER_MAX_INTERFACES];
    UINT                      VSInterfaceCount;

    ID3D11PixelShader*        PS;
    ID3D11SamplerState*       PSSamplers[D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT];
    ID3D11ShaderResourceView* PSShaderResources[D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT];
    ID3D11ClassInstance*      PSInterfaces[D3D11_SHADER_MAX_INTERFACES];
    UINT                      PSInterfaceCount;

	ID3D11GeometryShader*     GS;
	ID3D11ClassInstance*      GSInterfaces[D3D11_SHADER_MAX_INTERFACES];
	UINT                      GSInterfaceCount;

	ID3D11DomainShader*       DS;
	ID3D11ClassInstance*      DSInterfaces[D3D11_SHADER_MAX_INTERFACES];
	UINT                      DSInterfaceCount;

	ID3D11ComputeShader*      CS;
	ID3D11ClassInstance*      CSInterfaces[D3D11_SHADER_MAX_INTERFACES];
	UINT                      CSInterfaceCount;

	ID3D11HullShader*         HS;
	ID3D11ClassInstance*      HSInterfaces[D3D11_SHADER_MAX_INTERFACES];
	UINT                      HSInterfaceCount;

    ID3D11Buffer*             IAVertexBuffers[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT];
    UINT                      IAVertexBuffersStrides[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT];
    UINT                      IAVertexBuffersOffsets[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT];
    ID3D11InputLayout*        IAInputLayout;
    D3D11_PRIMITIVE_TOPOLOGY  IAPrimitiveTopology;

    ID3D11DepthStencilState*  OMDepthStencilState;
    UINT                      OMDepthStencilRef;
    ID3D11BlendState*         OMBlendState;
    FLOAT                     OMBlendFactor[4];
	UINT                      OMSampleMask;

    UINT                      RSViewportCount;
    D3D11_VIEWPORT            RSViewports[D3D11_VIEWPORT_AND_SCISSORRECT_OBJECT_COUNT_PER_PIPELINE];
    ID3D11RasterizerState*    RSRasterizerState;
};

void CreateStateblock(ID3D11DeviceContext* dc, D3DX11_STATE_BLOCK* sb)
{
    memset(sb, 0, sizeof(D3DX11_STATE_BLOCK));

    dc->VSGetShader(&sb->VS, sb->VSInterfaces, &sb->VSInterfaceCount);

    dc->PSGetShader(&sb->PS, sb->PSInterfaces, &sb->PSInterfaceCount);
    dc->PSGetSamplers(0, D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT, sb->PSSamplers);
    dc->PSGetShaderResources(0, D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, sb->PSShaderResources);

	dc->GSGetShader(&sb->GS, sb->GSInterfaces, &sb->GSInterfaceCount);
	dc->DSGetShader(&sb->DS, sb->DSInterfaces, &sb->DSInterfaceCount);
	dc->CSGetShader(&sb->CS, sb->CSInterfaces, &sb->CSInterfaceCount);
	dc->HSGetShader(&sb->HS, sb->HSInterfaces, &sb->HSInterfaceCount);

    dc->IAGetVertexBuffers(0, D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT, sb->IAVertexBuffers, sb->IAVertexBuffersStrides, sb->IAVertexBuffersOffsets);
    dc->IAGetInputLayout(&sb->IAInputLayout);
    dc->IAGetPrimitiveTopology(&sb->IAPrimitiveTopology);

    dc->OMGetDepthStencilState(&sb->OMDepthStencilState, &sb->OMDepthStencilRef);
    dc->OMGetBlendState(&sb->OMBlendState, sb->OMBlendFactor, &sb->OMSampleMask);

    sb->RSViewportCount = D3D11_VIEWPORT_AND_SCISSORRECT_OBJECT_COUNT_PER_PIPELINE;
    dc->RSGetViewports(&sb->RSViewportCount, sb->RSViewports);
    dc->RSGetState(&sb->RSRasterizerState);
}

template<class T>
unsigned calc_count(T** arr, unsigned max_count)
{
    for(unsigned i = 0; i < max_count; ++i)
	{
		if(arr[i] == 0)
		{
			return i;
		}
	}
    return max_count;
}

void ApplyStateblock(ID3D11DeviceContext* dc, D3DX11_STATE_BLOCK* sb)
{
    dc->VSSetShader(sb->VS, sb->VSInterfaces, sb->VSInterfaceCount);
    dc->PSSetShader(sb->PS, sb->PSInterfaces, sb->PSInterfaceCount);
    UINT PSSamplerCount = calc_count(sb->PSSamplers, D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT);
    if(PSSamplerCount)
	{
		dc->PSSetSamplers(0, PSSamplerCount, sb->PSSamplers);
	}
    UINT PSShaderResourceCount = calc_count(sb->PSShaderResources, D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT);
    if(PSShaderResourceCount)
	{
		dc->PSSetShaderResources(0, PSShaderResourceCount, sb->PSShaderResources);
	}

	dc->GSSetShader(sb->GS, sb->GSInterfaces, sb->GSInterfaceCount);
	dc->DSSetShader(sb->DS, sb->DSInterfaces, sb->DSInterfaceCount);
	dc->CSSetShader(sb->CS, sb->CSInterfaces, sb->CSInterfaceCount);
	dc->HSSetShader(sb->HS, sb->HSInterfaces, sb->HSInterfaceCount);

    UINT IAVertexBufferCount = calc_count(sb->IAVertexBuffers, D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT);
    if(IAVertexBufferCount)
	{
		dc->IASetVertexBuffers(0, IAVertexBufferCount, sb->IAVertexBuffers, sb->IAVertexBuffersStrides, sb->IAVertexBuffersOffsets);
	}
    dc->IASetInputLayout(sb->IAInputLayout);
    dc->IASetPrimitiveTopology(sb->IAPrimitiveTopology);

    dc->OMSetDepthStencilState(sb->OMDepthStencilState, sb->OMDepthStencilRef);
    dc->OMSetBlendState(sb->OMBlendState, sb->OMBlendFactor, sb->OMSampleMask);

    dc->RSSetViewports(sb->RSViewportCount, sb->RSViewports);
    dc->RSSetState(sb->RSRasterizerState);
}

void FreeStateblock(D3DX11_STATE_BLOCK* sb)
{
	if(sb->VS)
	{
		sb->VS->Release();
	}

	for(unsigned i = 0; i < D3D11_SHADER_MAX_INTERFACES; ++i)
	{
		if(sb->VSInterfaces[i])
		{
			sb->VSInterfaces[i]->Release();
		}
		else
		{
			break;
		}
	}

	if(sb->PS)
	{
		sb->PS->Release();
	}

	for(unsigned i = 0; i < D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT; ++i)
	{
		if(sb->PSSamplers[i])
		{
			sb->PSSamplers[i]->Release();
		}
		else
		{
			break;
		}
	}

	for(unsigned i = 0; i < D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT; ++i)
	{
		if(sb->PSShaderResources[i])
		{
			sb->PSShaderResources[i]->Release();
		}
		else
		{
			break;
		}
	}

	for(unsigned i = 0; i < D3D11_SHADER_MAX_INTERFACES; ++i)
	{
		if(sb->PSInterfaces[i])
		{
			sb->PSInterfaces[i]->Release();
		}
		else
		{
			break;
		}
	}

	for(unsigned i = 0; i < D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT; ++i)
	{
		if(sb->IAVertexBuffers[i])
		{
			sb->IAVertexBuffers[i]->Release();
		}
		else
		{
			break;
		}
	}

	if(sb->IAInputLayout)
	{
		sb->IAInputLayout->Release();
	}

	if(sb->OMDepthStencilState)
	{
		sb->OMDepthStencilState->Release();
	}

	if(sb->OMBlendState)
	{
		sb->OMBlendState->Release();
	}

	if(sb->RSRasterizerState)
	{
		sb->RSRasterizerState->Release();
	}
}

void Rgsc_Graphics_D3D11::Render(bool onlyNotifications)
{
	// If a device context has been passed to us, use it.
	// Otherwise, use the immediate context.
	ID3D11DeviceContext* context = static_cast<ID3D11DeviceContext*>(m_DeviceContext);
	if (context == nullptr)
	{
		m_Device->GetImmediateContext(&context);
	}

	// capture state for restoring later
	D3DX11_STATE_BLOCK sb;
	CreateStateblock(context, &sb);

	// set our state and render
	context->CSSetShader(NULL, NULL, 0);
	context->DSSetShader(NULL, NULL, 0);
	context->GSSetShader(NULL, NULL, 0);
	context->HSSetShader(NULL, NULL, 0);

	// Set the viewport
	context->RSSetViewports(1, &m_Viewport);

	// Set the state
	context->RSSetState(m_RasterizerState);
	context->OMSetDepthStencilState(m_DepthStencilState, 0);
	context->OMSetBlendState(m_BlendState, NULL, 0xFFFFFFFF);

	// Set the input layout
	context->IASetInputLayout(m_VertexLayout);

	// Set primitive topology
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	// Set shaders and sampler
	context->VSSetShader(m_VertexShader, NULL, 0);
	context->PSSetShader(m_PixelShader, NULL, 0);
	context->PSSetSamplers(0, 1, &m_TextureSampler);

	// Set vertex buffer
	UINT stride = sizeof(SimpleVertex11);
	UINT offset = 0;
	context->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);

	// Update the textures just before rendering to pick up any recent changes that came from the game's update thread
	UpdateTextures(onlyNotifications, context);

#if ENABLE_SAVE_PIXEL_DATA
	SavePixelDataToFile();
#endif

#if ENABLE_SAVE_TEXTURE_DATA
	SaveTextureDataToFile();
#endif

	for(unsigned int i = 0; i < m_Tiles->getNumTiles(); i++)
	{
		Tile* tile = m_Tiles->getTile(i);

		if(tile && tile->isVisible())
		{
			// Set the texture
			context->PSSetShaderResources(0, 1, &m_TextureResourceViews[i]);

			// Draw
			context->Draw(4, i * 4);
		}
	}

	// Update and Render the virtual pointer
	// Do not render the cursor if only notifications are displaying.
	if (g_VirtualCursor.IsEnabled() && !onlyNotifications)
	{
		if (m_Rect.IsSet())
		{
			g_VirtualCursor.UpdateD3D11(context, m_Rect);
		}
		else
		{
			RenderRect rect;
			rect.Set(0, 0, m_MappedWidth, m_MappedHeight);
			g_VirtualCursor.UpdateD3D11(context, rect);
		}

		g_VirtualCursor.RenderD3D11(context);
	}

	// restore state
	ApplyStateblock(context, &sb);
	FreeStateblock(&sb);
	
	// If we requested the immediate context, release it to avoid a memory leak.
	if (m_DeviceContext == nullptr)
	{
		context->Release();
	}
}

#if ENABLE_SAVE_PIXEL_DATA

void Rgsc_Graphics_D3D11::SavePixelDataToFile()
{
	BYTE* imgData = NULL;

	int saveIntervalMs = 5000; // default: one save per 5 seconds.
	char outPath[RAGE_MAX_PATH] = {0};

	rtry
	{
		// The user must specify the location, and optionally can set the save interval.
		const char* saveLocation = NULL;
		rcheck(PARAM_scuiRenderSaveLocation.Get(saveLocation) && saveLocation, catchall, );
		PARAM_scuiRenderSaveIntervalMs.Get(saveIntervalMs);

		// Validate that we have a valid tile buffer
		rcheck(m_Tiles, catchall, );

		const u8* tiledBuffer = m_Tiles->getBuffer();
		rcheck(tiledBuffer, catchall, );

		// Check to see if the save interval has passed.
		rcheck(rage::sysTimer::HasElapsedIntervalMs(m_LastPixelSaveTime, saveIntervalMs), catchall, );

		// Attempt to open a file stream for the file
		formatf(outPath, "%s\\%d.bmp", saveLocation, sysTimer::GetSystemMsTime());
		std::ofstream file(outPath, std::ios::out | std::ios::binary);
		if (!file.is_open()) 
		{
			return;
		}

		// Extract widht/height from tiles.
		int width = m_Tiles->getWidth();
		int height = m_Tiles->getHeight();

		// RGB - 24 bits, 3 bytes
		int numBits = 24;
		int numBytes = numBits / 8;

		// Create the bitmap file header and info header
		BITMAPFILEHEADER bmfh;
		memset ( &bmfh, 0, sizeof (BITMAPFILEHEADER ) );
		bmfh.bfType = 0x4d42;
		bmfh.bfReserved1 = 0;
		bmfh.bfReserved2 = 0;
		bmfh.bfOffBits = 0x36;

		BITMAPINFOHEADER info;
		memset ( &info, 0, sizeof (BITMAPINFOHEADER ) );
		info.biSize = sizeof(BITMAPINFOHEADER);
		info.biWidth = width;
		info.biHeight = height;
		info.biPlanes = 1;
		info.biBitCount = 24;
		info.biCompression = BI_RGB;   
		info.biSizeImage = 0;
		info.biXPelsPerMeter = 0x0ec4;
		info.biYPelsPerMeter = 0x0ec4;     
		info.biClrUsed = 0;
		info.biClrImportant = 0;

		// Get the line width in bits. If the width isn't divisible by 4, 
		// padding may be needed.
		unsigned long lineWidthInBits = 0;
		if ((numBytes % 4) != 0)
		{
			lineWidthInBits = ((width * numBits + 31 ) & ~31) >> 3;
		}
		else
		{
			lineWidthInBits = width;
		}

		// Multiple by the height
		unsigned long paddedsize = lineWidthInBits * height; 
		bmfh.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + paddedsize;

		imgData = new BYTE[paddedsize];
		memset ( imgData, 0, paddedsize );

		unsigned long padding = 0;
		unsigned long widthInBytes = width * 4;
		while ( ( widthInBytes + padding ) % (4) != 0 )
			padding++;

		// get the padded scan line width
		unsigned long paddedWidthInBytes = widthInBytes + padding;

		unsigned long bufpos = 0;   
		unsigned long newpos = 0;
		for (long y = 0; y < height; y++ )
		{
			int bytePos = 0;
			for (long x = 0; x < numBytes * width; x += numBytes)
			{
				newpos = y * lineWidthInBits + x;     
				bufpos = (height - y - 1) * paddedWidthInBytes + bytePos;

				imgData[newpos++] = tiledBuffer[bufpos++];       
				imgData[newpos++] = tiledBuffer[bufpos++]; 
				imgData[newpos++] = tiledBuffer[bufpos++]; 

				bytePos += 4;
			}
		}

		file.write((char*) &bmfh, sizeof(BITMAPFILEHEADER));
		file.write((char*) &info, sizeof(BITMAPINFOHEADER));
		file.write((char*) ((void*)imgData), paddedsize);
		file.close();

		m_LastPixelSaveTime = rage::sysTimer::GetSystemMsTime();
	}
	rcatchall
	{

	}

	if (imgData)
	{
		delete imgData;
		imgData = NULL;
	}
}

#endif // ENABLE_SAVE_SCUI_TO_BITMAP

#if ENABLE_SAVE_TEXTURE_DATA

void Rgsc_Graphics_D3D11::SaveTextureDataToFile()
{
	int saveIntervalMs = 5000; // default: one save per 5 seconds.
	char outPath[RAGE_MAX_PATH] = {0};

	rtry
	{
		// The user must specify the location, and optionally can set the save interval.
		const char* saveLocation = NULL;
		rcheck(PARAM_scuiRenderSaveLocation.Get(saveLocation) && saveLocation, catchall, );
		PARAM_scuiRenderSaveIntervalMs.Get(saveIntervalMs);

		// Validate that we have a valid tile buffer
		rcheck(m_Tiles, catchall, );

		const u8* tiledBuffer = m_Tiles->getBuffer();
		rcheck(tiledBuffer, catchall, );

		// Check to see if the save interval has passed.
		rcheck(rage::sysTimer::HasElapsedIntervalMs(m_LastTextureSaveTime, saveIntervalMs), catchall, );

		HRESULT hr;

		// get immediate context
		ID3D11DeviceContext* immediateContext;
		m_Device->GetImmediateContext(&immediateContext);

		if (sysTimer::HasElapsedIntervalMs(m_LastTextureSaveTime, 5000))
		{
			u32 writeTime = sysTimer::GetSystemMsTime();
	
			// Attempt to create folder for the savess
			char filePath[256] = {0};
			formatf(filePath, "%s\\%d", saveLocation, writeTime);
			::CreateDirectoryA(filePath, 0);

			for(unsigned int i = 0; i < m_Tiles->getNumTiles(); i++)
			{
				safecatf(filePath, "\\%d.png", i);

				hr = D3DX11SaveTextureToFileA(immediateContext, m_Textures[i], D3DX11_IFF_PNG, filePath);
				if (hr == S_OK)
				{
					m_LastTextureSaveTime = writeTime;
				}
			}

			m_LastTextureSaveTime = writeTime;
		}

		immediateContext->Release();
	}
	rcatchall
	{

	}
}

#endif // ENABLE_SAVE_TEXTURE_DATA


} // namespace rgsc
