// 
// input/pad_common.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "pad.h"
#include "rgsc.h"
#include "input.h"

// rage includes
#include "diag/seh.h"
#include "rline/rldiag.h"
#include "system/memops.h"

namespace rage
{
	PARAM(ignoreFocus, "Gamepad does not require window focus to function");
}

namespace rgsc
{

sysCriticalSectionToken RgscGamePadManager::sm_Cs;

RgscGamePadManager::RgscGamePadManager()
	: m_bSendConfigurationToScui(false)
	, m_bBackgroundInputEnabled(false)
	, m_bSimulateKeyboardEvents(true) // true by default, legacy behaviour
	, m_bSimulateMouseEvents(true) // true by default, legacy behaviour
	, m_bSendGamepadInputToBrowser(false) // false by default, legacy behaviour
{
	Clear();
}

RgscGamePadManager::~RgscGamePadManager()
{
	Clear();
}

void RgscGamePadManager::Clear()
{
#if RGSC_DINPUT_SUPPORT || RGSC_XINPUT_SUPPORT
	for (int i = 0; i < RGSC_MAX_PADS; i++)
	{
#if RGSC_DINPUT_SUPPORT
		m_D3DPads[i].ClearInputs();
#endif // RGSC_DINPUT_SUPPORT
#if RGSC_XINPUT_SUPPORT
		m_XInputPads[i].ClearInputs();
#endif // RGSC_XINPUT_SUPPORT
	}
#endif // #if RGSC_DINPUT_SUPPORT || RGSC_XINPUT_SUPPORT

	m_MarshalledPads = NULL;
	m_NumMarshalledPads = 0;
}

RGSC_HRESULT RgscGamePadManager::QueryInterface(RGSC_REFIID riid, void** ppvObject)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObject == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IGamepadManager*>(this);
	}
	else if(riid == IID_IGamepadManagerV1)
	{
		pUnknown = static_cast<IGamepadManagerV1*>(this);
	}
	else if(riid == IID_IGamepadManagerV2)
	{
		pUnknown = static_cast<IGamepadManagerV2*>(this);
	}
	else if(riid == IID_IGamepadManagerV3)
	{
		pUnknown = static_cast<IGamepadManagerV3*>(this);
	}
	else if(riid == IID_IGamepadManagerV4)
	{
		pUnknown = static_cast<IGamepadManagerV4*>(this);
	}

	*ppvObject = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

RGSC_HRESULT RgscGamePadManager::SetMarshalledGamepads(IGamepad** gamePads, unsigned numGamePads)
{
	SYS_CS_SYNC(sm_Cs);
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(gamePads, catchall, hr = RGSC_INVALIDARG);

		rlDebug3("Enabling input marshalling for %u gamepads", numGamePads);

		m_MarshalledPads = gamePads;
		m_NumMarshalledPads = numGamePads;

		hr = RGSC_OK;
	}
	rcatchall
	{
		
	}

	return hr;
}

void RgscGamePadManager::ClearMarshalledGamepads()
{
	SYS_CS_SYNC(sm_Cs);
	
	Assert(m_NumMarshalledPads > 0);
	rlDebug3("Clearing input marshalling for %u gamepads", m_NumMarshalledPads);

	m_MarshalledPads = 0;
	m_NumMarshalledPads = 0;
}

void RgscGamePadManager::NotifyGamepadsChanged()
{
	m_bSendConfigurationToScui = true;
}

void RgscGamePadManager::SetBackgroundInputEnabled(bool bEnabled)
{
	if (m_bBackgroundInputEnabled != bEnabled)
	{
		rlDebug3("%s background input", bEnabled ? "Enabling" : "Disabling");
		m_bBackgroundInputEnabled = bEnabled;
	}
}

void RgscGamePadManager::SetSimulateKeyboardEventsEnabled(bool bEnabled)
{
	if (m_bSimulateKeyboardEvents != bEnabled)
	{
		rlDebug3("%s simulating keyboard events", bEnabled ? "Enabling" : "Disabling");
		m_bSimulateKeyboardEvents = bEnabled;
	}
}

void RgscGamePadManager::SetSimulateMouseEventsEnabled(bool bEnabled)
{
	if (m_bSimulateMouseEvents != bEnabled)
	{
		rlDebug3("%s simulating mouse events", bEnabled ? "Enabling" : "Disabling");
		m_bSimulateMouseEvents = bEnabled;
	}
}

void RgscGamePadManager::SetSendPadInputToBrowserEnabled(bool bEnabled)
{
	if (m_bSendGamepadInputToBrowser != bEnabled)
	{
		rlDebug3("%s sending of pad input to browser", bEnabled ? "Enabling" : "Disabling");
		m_bSendGamepadInputToBrowser = bEnabled;
	}
}

void RgscGamePadManager::BeginAll() 
{
	SYS_CS_SYNC(sm_Cs);
	if (GetRgscConcreteInstance()->GetGamepadSupport() == IConfigurationV6::GAMEPADS_DISABLED ||
		GetRgscConcreteInstance()->GetGamepadSupport() == IConfigurationV6::GAMEPADS_MARSHALLED)
	{
		return;
	}

#if RGSC_DINPUT_SUPPORT || RGSC_XINPUT_SUPPORT
	for (int i=0; i < RGSC_MAX_PADS;i++) 
	{
#if RGSC_DINPUT_SUPPORT
		m_D3DPads[i].SetPadIndex(i);
		m_D3DPads[i].ClearInputs();
#endif // RGSC_DINPUT_SUPPORT
#if RGSC_XINPUT_SUPPORT
		m_XInputPads[i].SetPadIndex(i);
		m_XInputPads[i].ClearInputs();
#endif // RGSC_XINPUT_SUPPORT
	}
#endif // #if RGSC_DINPUT_SUPPORT || RGSC_XINPUT_SUPPORT
}


void RgscGamePadManager::UpdateAll()
{
	SYS_CS_SYNC(sm_Cs);

	if (GetRgscConcreteInstance()->GetGamepadSupport() == IConfigurationV6::GAMEPADS_SUPPORTED)
	{
#if RGSC_DINPUT_SUPPORT || RGSC_XINPUT_SUPPORT
		for (unsigned i = 0; i < RGSC_MAX_PADS; i++)
		{
#if RGSC_DINPUT_SUPPORT
			m_D3DPads[i].Update();
#endif // RGSC_DINPUT_SUPPORT
#if RGSC_XINPUT_SUPPORT
			m_XInputPads[i].Update();
#endif // RGSC_XINPUT_SUPPORT
		}
#endif // #if RGSC_DINPUT_SUPPORT || RGSC_XINPUT_SUPPORT
	}

	SendConfigurationToScui();
}

int RgscGamePadManager::CountPads()
{
	SYS_CS_SYNC(sm_Cs);
	int numPads = 0;

#if RGSC_DINPUT_SUPPORT
	for (unsigned i = 0; i < RGSC_MAX_PADS; i++)
	{
		if (GetRgscConcreteInstance()->GetGamepadSupport() == IConfigurationV6::GAMEPADS_SUPPORTED && m_D3DPads[i].IsConnected())
		{
			numPads++;
		}

// 		xinput pads are also d3d pads that we don't handle
//		if (m_XInputPads[i].IsConnected())
// 			numPads++;
	}
#endif // RGSC_DINPUT_SUPPORT

	if (m_MarshalledPads)
	{
		for (unsigned i = 0; i < m_NumMarshalledPads; i++)
		{
			IGamepadV1* v1 = NULL;
			RGSC_HRESULT hr = m_MarshalledPads[i]->QueryInterface(IID_IGamepadV1, (void**) &v1);
			if(SUCCEEDED(hr) && (v1 != NULL))
			{
				if (v1->IsConnected())
					numPads++;
			}
		}
	}

	return numPads;
}

const int RgscGamePadManager::GetNumPads(IGamepadManagerV1::InputMode mode)
{
	if (mode == IGamepadManagerV1::INPUTMODE_MARSHALLED)
	{
		return GetNumMarshalledPads();
	}
	else
	{
		return RGSC_MAX_PADS;
	}
}

void RgscGamePadManager::CheckForNewlyPluggedInDevices()
{
	SYS_CS_SYNC(sm_Cs);
	if (GetRgscConcreteInstance()->GetGamepadSupport() == IConfigurationV6::GAMEPADS_DISABLED ||
		GetRgscConcreteInstance()->GetGamepadSupport() == IConfigurationV6::GAMEPADS_MARSHALLED)
	{
		return;
	}

#if RGSC_DINPUT_SUPPORT || RGSC_XINPUT_SUPPORT
	for (unsigned i = 0; i < RGSC_MAX_PADS; i++)
	{
#if RGSC_DINPUT_SUPPORT
		m_D3DPads[i].Refresh();
#endif // RGSC_DINPUT_SUPPORT
#if RGSC_XINPUT_SUPPORT
		m_XInputPads[i].Refresh();
#endif // RGSC_XINPUT_SUPPORT
	}
#endif // #if RGSC_DINPUT_SUPPORT || RGSC_XINPUT_SUPPORT
}

void RgscGamePadManager::SendConfigurationToScui()
{
	rtry
	{
		rcheck(m_bSendConfigurationToScui, catchall, );
		rcheck(sysTimer::HasElapsedIntervalMs(m_TimeOfLastScuiUpdate, MIN_SCUI_UPDATE_INTERVAL_MS), catchall, );
		
		// Attempt to send an update, clearing out the send flag and setting the send time.
		m_TimeOfLastScuiUpdate = sysTimer::GetSystemMsTime();
		m_bSendConfigurationToScui = false;

		// If input is not using controller, changes will be picked up when a swap is made to the controller. 
		// No need to refresh, so we can exit out here without worry,
		InputMode inputMode = Rgsc_Input::GetControllerInputMode();
		rcheck(inputMode != IGamepadManagerV1::INPUTMODE_INVALID, catchall, );
			
		Rgsc_Input::SendInputMethodToScui(inputMode, Rgsc_Input::GetControllerIndex(inputMode));
	}
	rcatchall
	{

	}
}

bool RgscGamePadManager::IsBackgroundInputEnabled() const
{
	return m_bBackgroundInputEnabled || PARAM_ignoreFocus.Get();
}

bool RgscGamePadManager::IsSimulatingKeyboardEvents() const
{
	return m_bSimulateKeyboardEvents;
}

bool RgscGamePadManager::IsSimulatingMouseEvents() const
{
	return m_bSimulateMouseEvents;
}

bool RgscGamePadManager::IsForwardingPadInputToBrowser() const
{
	return m_bSendGamepadInputToBrowser;
}

} // namespace rgsc
