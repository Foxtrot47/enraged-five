#ifndef RGSC_GRAPHICS_D3D11_H
#define RGSC_GRAPHICS_D3D11_H

#include "graphics.h"
#pragma warning(push)
#pragma warning(disable: 4668)
#include <d3d11.h>
#include <xnamath.h>
#pragma warning(pop)

// If enabled, you can specify an output folder with -scuiRenderSaveLocation and the pixel data
// from chrome will be saved to a bitmap in this folder at a fixed interval.
#define ENABLE_SAVE_PIXEL_DATA (0)

// If enabled, you can specify an output folder with -scuiRenderSaveLocation and the d3d11 textures
//	will be saved to a png in this folder at a fixed interval. 
// NOTE: rgb channels will be swapped as the D3DX11SaveTextureToFile function requires rgba data and the chrome 
//	renders using bgra. This debug functionality was enabled to track a corruption issue so colour did not matter, 
//	but if you want correct colors you'll need to add mapping from bgra to rgba.
#define ENABLE_SAVE_TEXTURE_DATA (0)

namespace rgsc
{

struct SimpleVertex11
{
	XMFLOAT3 Pos;
	XMFLOAT2 Tex;
};

class Rgsc_Graphics_D3D11 : public Rgsc_Graphics
{
public:
	Rgsc_Graphics_D3D11(ID3D11Device* device, void* params, const RenderRect& rect);
	virtual ~Rgsc_Graphics_D3D11();
	HRESULT Init(ID3D11Device* device, DXGI_SWAP_CHAIN_DESC* sd, const RenderRect& rect);
	void Shutdown();
	void Render(bool onlyNotifications);
	bool OnLost();
	bool OnReset(void *params, const RenderRect& rect);
	void SetContext(void* context);
	bool MapMouse(int &x, int &y);

private:

	void ReleaseResources();
	HRESULT CreateTextures();
	void UpdateTextures(bool onlyNotifications, ID3D11DeviceContext* context);
	void UpdateVirtualPointer();

	ID3D11Device* m_Device;
	ID3D11VertexShader* m_VertexShader;
	ID3D11PixelShader* m_PixelShader;
	ID3D11InputLayout* m_VertexLayout;
	ID3D11Buffer* m_VertexBuffer;
	ID3D11ShaderResourceView** m_TextureResourceViews;
	ID3D11SamplerState* m_TextureSampler;
	ID3D11RasterizerState* m_RasterizerState;
	ID3D11DepthStencilState* m_DepthStencilState;
	ID3D11BlendState* m_BlendState;
	ID3D11Texture2D** m_Textures;
	D3D11_VIEWPORT m_Viewport;
	ID3D11DeviceContext* m_DeviceContext;

#if ENABLE_SAVE_TEXTURE_DATA
	void SaveTextureDataToFile();
	unsigned m_LastTextureSaveTime;
#endif

#if ENABLE_SAVE_PIXEL_DATA
	void SavePixelDataToFile();
	unsigned m_LastPixelSaveTime;
#endif

};

} // namespace rgsc

#endif //RGSC_GRAPHICS_D3D11_H
