#include "tiledbuffer.h"
#include "system/new.h"
#include "diag/seh.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <windows.h> // for VirtualAlloc
#include <math.h>
#include <string.h>
#include <stdio.h>
#pragma warning(pop)

#pragma warning(push)
#pragma warning(disable: 4310)
#include "base/shared_memory.h"
#pragma warning(pop)
#include "string/string.h"

namespace rgsc
{

#define FORCE_POWER_OF_2_TEXTURES 0

Tile::Tile()
{

}

Tile::~Tile()
{

}

static unsigned int RoundUpToPowerOf2(unsigned int k)
{
	k--;

	for(int i = 1; i < 32; i <<= 1)
	{
		k = k | k >> i;
	}

	return k + 1;
}

void Tile::Init(const TileRect& rect, unsigned char* buffer, unsigned short* currentFrame)
{
	m_Rect = rect;
	m_Width = rect.m_Right - rect.m_Left;
	m_Height = rect.m_Bottom - rect.m_Top;

	m_Buffer = buffer;
	m_RowSize = m_Width * 4; // 4 bytes per pixel
	m_EffectiveHeight = m_Height;

#if FORCE_POWER_OF_2_TEXTURES
 	m_Width = RoundUpToPowerOf2(m_Width);
 	m_Height = RoundUpToPowerOf2(m_Height);
#endif

	m_LastFrame = 0;
	m_CurrentFrame = currentFrame;

	m_Dirty = false;
	m_Visible = true;
}

void Tile::clear()
{
 	setDirty();
}

void Tile::setDirty()
{
	m_Dirty = true;
}

bool Tile::isDirty(unsigned short& dirtyFrame)
{
	bool isDirty = m_Dirty;

	Assert(m_CurrentFrame);
	if(m_CurrentFrame)
	{
		unsigned short currentFrame = *m_CurrentFrame;
		dirtyFrame = currentFrame;
		if(!isDirty)
		{
			isDirty = m_LastFrame != currentFrame;
		}
	}

	return isDirty;
}

void Tile::setLastUpdatedFrame(unsigned short frame)
{
	m_LastFrame = frame;
	m_Dirty = false;
}

unsigned TiledBuffer::getDirtyTileBufferSize()
{
	const unsigned maxTiles = 128 * 128;
	const unsigned dirtyTileMem = maxTiles * sizeof(unsigned short);
	return dirtyTileMem;
}

TiledBuffer::TiledBuffer(unsigned int width, unsigned int height)
{
	// TODO: NS - need the id of the window
	unsigned int windowId = 1;

	// make the name of the shared memory buffer different each time we create it,
	// since the subprocess will still be using the old shared buffer for a short
	// period of time before it gets the message to stop using it and switch over
	// to the new buffer. The IPC message to close the shared memory buffer used
	// to be synchronous, but this was changed to use an async IPC message to
	// avoid a deadlock when receiving a synchronous message at the same time.
	static unsigned instance = 0;
	instance++;

	rtry
	{
		DWORD pid = GetCurrentProcessId();
		rage::formatf(m_SharedMemoryName, "rgsc_window_%u_mem_%x_inst_%u", windowId, pid, instance);

		m_SharedMemory = rage_new ipcbase::SharedMemory();
		rverify(m_SharedMemory, catchall, );
		rverify(m_SharedMemory->Create(m_SharedMemoryName, false, false, (width * height * 4) + getDirtyTileBufferSize()), catchall, );
		rverify(m_SharedMemory->Map(m_SharedMemory->max_size()), catchall, );
		m_Buffer = (unsigned char*)m_SharedMemory->memory();
		rverify(m_Buffer, catchall, );
		m_Buffer = m_Buffer + getDirtyTileBufferSize();

		m_Width = width;
		m_Height = height;

		CreateTiles();

		clear();
	}
	rcatchall
	{

	}
}

TiledBuffer::~TiledBuffer()
{
	if(m_SharedMemory != NULL)
	{
		m_SharedMemory->Close();
		m_SharedMemory = NULL;
	}

	m_Buffer = NULL;

	if(m_Tiles)
	{
		delete [] m_Tiles;
		m_Tiles = NULL;
	}
}

void TiledBuffer::CalculateTileSize(const unsigned int width, const unsigned int height, unsigned int &bestTileWidth, unsigned int &bestTileHeight)
{
	const unsigned int desiredNumberOfTiles = 20;
	const unsigned int minTileWidth = 64;
	const unsigned int maxTileWidth = 512;	// optimized tile sizes for 4k monitors so that notifications only require 2 tile updates
	const unsigned int maxTileHeight = 512;

	bestTileWidth = 0;
	bestTileHeight = 0;
	unsigned int bestNumTiles = 0;

	unsigned int testTileWidth = minTileWidth;

	while(testTileWidth <= maxTileWidth)
	{
		unsigned int testTileHeight = minTileWidth;

		while(testTileHeight <= maxTileHeight)
		{
			unsigned int numCols = (unsigned int)ceil((float)width / (float)testTileWidth);
			unsigned int numRows = (unsigned int)ceil((float)height / (float)testTileHeight);
			unsigned int numTiles = numCols * numRows;

			//Displayf("a tile size of %d, %d would give us %d tiles\n", testTileWidth, testTileHeight, numTiles);

			// prefer the tiles to be wider than tall, since that will reduce the number of memcpy's when updating the textures
			// don't enforce this rule if the window has an aspect ratio that is narrower than 1:1 (i.e. portrait mode monitors)
			bool desiredMinAspect = (m_Width >= m_Height) ? (testTileWidth >= testTileHeight) : true;

			if((bestNumTiles == 0) || (desiredMinAspect &&
				(abs((int)desiredNumberOfTiles - (int)numTiles) - abs((int)desiredNumberOfTiles - (int)bestNumTiles)) <= 0))
			{
				//Displayf("best so far\n");
				bestTileWidth = testTileWidth;
				bestTileHeight = testTileHeight;
				bestNumTiles = numTiles;
			}
			else
			{
				//Displayf("not as good as best so far\n");
			}

			testTileHeight = RoundUpToPowerOf2(testTileHeight + 1);
		}

		testTileWidth = RoundUpToPowerOf2(testTileWidth + 1);
	}

	Displayf("Num Tiles: %d, Tile Size: %d x %d", bestNumTiles, bestTileWidth, bestTileHeight);
}

void TiledBuffer::CreateTiles()
{
#if 0
	struct tempTest 
	{
		unsigned int width;
		unsigned int height;
	};

	// test min, max resolution, and all of the most popular resolutions according to the Steam hardware survey
	tempTest tests[] = {{800, 600}, {1024, 768}, {1024, 576}, {1280, 720}, {1280, 800}, {1920, 1080}, {1920, 1200}, {1680, 1050}, {1440, 900}, {2560, 1600},
						{1152, 864}, {1280, 768}, {1280, 960}, {1280, 1024}, {1360, 768}, {1366, 768}, {1600, 900}, {1600, 1200}, {1920, 1200}, {3840, 2160}};

	for(unsigned i = 0; i < (sizeof(tests) / sizeof(tests[0])); i++)
	{
		unsigned int tileWidth = 0;
		unsigned int tileHeight = 0;
		CalculateTileSize(tests[i].width, tests[i].height, tileWidth, tileHeight);

		m_NumCols = (unsigned int)ceil((float)tests[i].width / (float)tileWidth);
		m_NumRows = (unsigned int)ceil((float)tests[i].height / (float)tileHeight);
		m_NumTiles = m_NumCols * m_NumRows;

		Displayf("Best tile size for %d, %d is %d x %d (%d tiles)\n", tests[i].width, tests[i].height, tileWidth, tileHeight, m_NumTiles);
		Displayf("\n");
	}
#endif

	// figure out a good tile width and height
	unsigned int desiredTileWidth = 0;
	unsigned int desiredTileHeight = 0;
	CalculateTileSize(m_Width, m_Height, desiredTileWidth, desiredTileHeight);
	
	m_NumCols = (unsigned int)ceil((float)m_Width / (float)desiredTileWidth);
	m_NumRows = (unsigned int)ceil((float)m_Height / (float)desiredTileHeight);
	m_NumTiles = m_NumCols * m_NumRows;

	m_Tiles = rage_new Tile[m_NumTiles];

	unsigned short* dirtyTileMem = (unsigned short*)m_SharedMemory->memory();

	int totalHeight = 0;

	for(unsigned int i = 0; i < m_NumRows; i++)
	{
		unsigned int tileWidth = desiredTileWidth;
		unsigned int tileHeight = desiredTileHeight;

		totalHeight += tileHeight;
		if(totalHeight > (int)m_Height)
		{
			totalHeight -= tileHeight;
			tileHeight = m_Height - totalHeight;
			totalHeight = m_Height;
		}

		// columns start right to left. We're attempting to minimize the number of tiles that
		// need to be updated when notification appear on screen. Notifications appear in the
		// top-right of the screen. Prefer to have full tiles at the right side of the screen
		// and partial tiles at the left side.
		int totalWidth = m_Width;
		for(int j = m_NumCols - 1; j >= 0; j--)
		{
			totalWidth -= tileWidth;
			if(totalWidth < 0)
			{
				totalWidth += tileWidth;
				tileWidth = totalWidth;
				totalWidth = 0;
			}

			unsigned int bufferOffset = (totalWidth + ((totalHeight - tileHeight)) * m_Width) * 4;
			unsigned char* buffer = m_Buffer + bufferOffset;

			Assert(buffer <= (m_Buffer + (m_Width * m_Height * 4)));
			Assert(buffer >= m_Buffer);


			TileRect rect;
			rect.m_Left = totalWidth;
			rect.m_Right = rect.m_Left + tileWidth;
			rect.m_Top = totalHeight - tileHeight;
			rect.m_Bottom = rect.m_Top + tileHeight;

			unsigned int tileIndex = j + (i * m_NumCols);

			//Displayf("tile[%d, %d] (tile index %d) = %d x %d\n\trect = top: %d, left: %d, bottom: %d, right: %d\n", i, j, tileIndex, tileWidth, tileHeight, rect.m_Top, rect.m_Left, rect.m_Bottom, rect.m_Right);

			m_Tiles[tileIndex].Init(rect, buffer, &dirtyTileMem[tileIndex]);
		}
		//Displayf("\n");
	}
}

void TiledBuffer::clear()
{
	if(m_SharedMemory)
	{
		unsigned char* buffer = (unsigned char*)m_SharedMemory->memory();

		if(buffer)
		{
			// TODO: NS - should send a message to the subprocess to do this instead of writing to the buffer in the DLL
			memset(buffer, 0, m_SharedMemory->max_size());

			for(unsigned int i = 0; i < m_NumTiles; i++)
			{
				m_Tiles[i].clear();
			}
		}
	}
}

unsigned char* TiledBuffer::getBuffer()
{
	return m_Buffer;
}

unsigned int TiledBuffer::getBufferSize()
{
	return m_Width * m_Height * 4;
}

bool TileRect::IsIntersecting(const TileRect& rect) const
{
	return (((m_Left > rect.m_Left ? m_Left : rect.m_Left) < 
		     (m_Right < rect.m_Right ? m_Right : rect.m_Right)) &&
			((m_Top > rect.m_Top ? m_Top : rect.m_Top) < 
			(m_Bottom < rect.m_Bottom ? m_Bottom : rect.m_Bottom)));
}

unsigned int TiledBuffer::getNumTiles()
{
	return m_NumTiles;
}

Tile* TiledBuffer::getTile(unsigned int index)
{
	if(m_Tiles && (index < getNumTiles()))
	{
		return &m_Tiles[index];
	}
	return NULL;
}

Tile* TiledBuffer::getTile(unsigned int row, unsigned int col)
{
	return getTile(col + (row * m_NumCols));
}

} // namespace rgsc
