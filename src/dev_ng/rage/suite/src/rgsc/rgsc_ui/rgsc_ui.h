#ifndef RGSC_UI_H
#define RGSC_UI_H

#include "rgsc_ui_interface.h"
#include "rgsc_common.h"
#include "file/limits.h"
#include "graphics.h"
#include "activation.h"
#include "patching.h"
#include "atl/map.h"
#include "file/packfile.h"
#include "system/criticalsection.h"
#include "net/status.h"
#include "net/time.h"
#include "vector/color32.h"
#include "types.h"

#include "ipc.h"
#include <string>
#include <vector>

namespace IPC
{
	class Message;
}

namespace rgsc
{

class RgscWindow;
class RgscErrorDelegate;

// error C4265: class has virtual functions, but destructor is not virtual
// the binary interface for virtual destructors isn't standardized, so don't make the destructor virtual
#pragma warning(push)
#pragma warning(disable: 4265)

#define USE_RPF_FOR_OFFLINE_ASSETS 0
#define USE_ZIP_FOR_OFFLINE_ASSETS 1

class RgscUi : public IRgscUiLatestVersion
{
public: 
	
	// IRgscUnknown
	RGSC_HRESULT RGSC_CALL QueryInterface(RGSC_REFIID riid, void** ppvObject);

	// IRgscUiV1
	void RGSC_CALL Render();
	bool RGSC_CALL IsVisible();
	bool RGSC_CALL IsOfflineMode();
	RGSC_HRESULT RGSC_CALL OnCreateDevice(void* d3d, void* params);
	RGSC_HRESULT RGSC_CALL OnLostDevice();
	RGSC_HRESULT RGSC_CALL OnResetDevice(void* params);
	bool RGSC_CALL IsReadyToAcceptCommands();
	void RGSC_CALL EnableHotkey(bool enable);
	RGSC_HRESULT RGSC_CALL ShowUi();
	RGSC_HRESULT RGSC_CALL ShowSignInUi();
	RGSC_HRESULT RGSC_CALL ShowGamerProfileUi(unsigned int profileId);
	IActivation* RGSC_CALL GetActivationSystem();
	IPatching* RGSC_CALL GetPatchingSystem();

	// IRgscUiV2
	RGSC_HRESULT RGSC_CALL SetRenderRect(unsigned int top, unsigned int left, unsigned int bottom, unsigned int right);
	RGSC_HRESULT RGSC_CALL CloseUi();

	// IRgscUiV3
	RGSC_HRESULT RGSC_CALL SendMessageToScui(const char* jsonMessage);
	RGSC_HRESULT RGSC_CALL ShowAccountLinkingUi(const AccountLinkPlatform platform, AccountLinkingCallback callback);
	void RGSC_CALL SendVirtualKeyboardResult(const wchar_t* text);
	void RGSC_CALL EnableClosingUi(bool enable);
	void RGSC_CALL ShowVirtualKeyboard(const char* initialText, bool bIsPassword, int maxNumChars);
	InputMethod RGSC_CALL GetInputMethod();
	void RGSC_CALL ForwardExternalUrls(bool enable);
	bool RGSC_CALL IsInFailState();
	bool RGSC_CALL ReloadUi(bool bOfflineOnly);
	void RGSC_CALL SetUiScale(float scale);

	// IRgscUiV4
	bool RGSC_CALL IsHotkeyEnabled();
	bool RGSC_CALL IsClosingEnabled();
	bool RGSC_CALL IsReloadingUi() { return m_ReloadingUi; }
	bool RGSC_CALL IsReloadingUiSignedIn() { return m_SignInOnReload; }
	bool RGSC_CALL IsReloadingUiOnline() { return m_SignOnlineOnReload; }
	bool RGSC_CALL SetMessageHandlers(IRgscPlatformMessageHandler** handlers, const unsigned numHandlers);

	// IRgscUiV5
	bool RGSC_CALL IsOverridingCursor();
	void RGSC_CALL SetWindowMargins(int top, int left, int bottom, int right);

	// IRgscUiV6
	void RGSC_CALL SetIsModal(bool isModal);
	bool RGSC_CALL IsModal();

	// IRgscUiV7
	RGSC_HRESULT RGSC_CALL CreateInWindow(IRgscUiWindowConfiguration* config);
	void RGSC_CALL SetWindowDimensions(int x, int y, int width, int height);
	virtual void RGSC_CALL AddCaptionExclusion(int x, int y, int width, int height, int referencePoint);
	virtual void RGSC_CALL ClearCaptionExclusion();
	virtual void RGSC_CALL DestroyWindow();
	virtual bool RGSC_CALL IsWindowDestroyed();
	virtual void RGSC_CALL SetWindowFocus(bool bEnabled);
	virtual void RGSC_CALL BeginShutdown(int timeOutMs);

	// IRgscUiV8
	virtual void RGSC_CALL Hibernate();
	virtual bool RGSC_CALL IsHibernating();
	virtual void RGSC_CALL WakeUp();
	virtual WindowHandle RGSC_CALL GetBrowserHandle(); 
	virtual WindowHandle RGSC_CALL GetWindowHandle();
	virtual RGSC_HRESULT RGSC_CALL CreatePopupWindow(IRgscUiWindowConfiguration* config);
	virtual RGSC_HRESULT RGSC_CALL CreateBorderlessWindow(IRgscUiWindowConfiguration* config);
	virtual void RGSC_CALL SetWindowCanClose(bool bCanClose);
	virtual void RGSC_CALL SetWindowModalBlocker(WindowHandle handle);
	virtual RGSC_HRESULT RGSC_CALL SetBrowserKeyEventCallback(BrowserKeyEventCallback);

	// IRgscUiV9
	virtual RGSC_HRESULT RGSC_CALL SendMessageToBrowser(CustomWindowMessages msg, RGSC_WPARAM wParam, RGSC_LPARAM lParam);

	// IRgscUiV10
	virtual void RGSC_CALL SetDeviceContext(void* d3dDeviceContext);
	virtual void RGSC_CALL SetKeyboardEventForwarding(bool bEnabled);
	virtual void RGSC_CALL SetMouseEventForwarding(bool bEnabled);
	virtual void RGSC_CALL SetVirtualCursor(bool bEnabled);
	virtual void RGSC_CALL SetModalBackgroundColor(u8 red, u8 green, u8 blue, u8 alpha);

	// IRgscUiV11
	virtual bool RGSC_CALL IsJavascriptReadyToAcceptCommands();

public: // accessible from anywhere in the rgsc.dll

	RgscUi();
	~RgscUi() {}

	RGSC_HRESULT Init();
	void Shutdown();
	void Update();

	RGSC_HRESULT SignIn(const char* jsonProfile);
	void FinishSignIn(const char* jsonResult);
	void FinishModifyProfile(const char* jsonResult);
	
	void OnSignInError();
	bool HadSignInError() { return m_SignInError; }
	void ClearSignInError();
	bool IsWaitingToShutdown() { return m_bWaitingForScuiShutdown; }

	void SetJavascriptReadyToAcceptCommands();

	void JavascriptFunctionDoesntExist(const char* functionName);

	RGSC_HRESULT SendNotification(const char* jsonNotification);

	void SetTextBoxHasFocus(const char* jsonHasFocus);

	void NetworkChange(const char* jsonNetworkChange);

	const char* GetOnlineWebSiteUrl(char (&urlBuf)[RGSC_MAX_URL_BUF_SIZE]);
	const char* GetOfflineWebSiteUrl();

	void SetOnlineUrl(const char* scuiUrl);
	const char* GetCurrentUrl();
	
	RgscWindowType GetWindowType() const;

	RGSC_HRESULT RequestReloadUi(const char* jsonReloadRequest);
	void ReloadUiInternal(bool bOfflineOnly);
	void ClearReloadingUi();

	enum UiState
	{
		UI_STATE_NONE,
		UI_STATE_MAIN_MENU,
		UI_STATE_SIGN_IN,
		UI_STATE_ACHIEVEMENTS,
		UI_STATE_ACTIVATION,
		UI_STATE_PATCHING,
		UI_STATE_FRIENDS,
		UI_STATE_PLAYERS,
		UI_STATE_COMMERCE,
		UI_STATE_ACCOUNT_LINKING,
	};

	enum UiSubState
	{
		UI_SUBSTATE_NONE,
		UI_SUBSTATE_ACTIVATION_MAIN,
		UI_SUBSTATE_ACTIVATION_CORRUPTED,
		UI_SUBSTATE_ACTIVATION_EXPIRATION,
		UI_SUBSTATE_ACTIVATION_RECOVERY,
		UI_SUBSTATE_PATCHING_MESSAGE,
		UI_SUBSTATE_PATCHING_DIALOG,
		UI_SUBSTATE_FRIEND_REQUEST,
		UI_SUBSTATE_PROFILE,
		UI_SUBSTATE_REDEMPTION_CODE,
		UI_SUBSTATE_LINK_STEAM_ACCOUNT,
		UI_SUBSTATE_LINK_FACEBOOK_ACCOUNT,
		UI_SUBSTATE_LINK_GOOGLE_ACCOUNT,
		UI_SUBSTATE_LINK_TWITCHTV_ACCOUNT,
		UI_SUBSTATE_FRIEND_SEARCH
	};

	enum MouseEnterExitEventType
	{
		MOUSE_ENTERED,
		MOUSE_EXITED
	};

	void HandleControllerInput(const u32 pressedButtons, const u32 releasedButtons, const u32 heldButtons);
	bool LaunchExternalWebBrowser(const char* jsonRequest);
	bool LaunchExternalUrl(const char* url);
	bool RequestExternalUrl(const char* url);
	bool C2JsRequestUiState(bool visible, UiState state, UiSubState subState, void* data);
	bool Js2CUiStateResponse(const char* jsonResponse);
	void Js2CRequestUiState(const char* jsonRequest);
	void C2JsUiStateResponse(const char* jsonResponse);
	void PageLoadComplete(int errorCode);
	void SetReadyToAcceptCommands();
	void SetNotificationVisible(const char* jsonVisible);
	void RaiseUiEvent(const char* jsonEvent);
	void RaiseDebugEvent(const wchar_t* jsonEvent);
	void RaiseRetailOutputEvent(const char* jsonEvent);
	void RaiseImeEvent(const char* jsonEvent);

	Activation* _GetActivationSystem();
	Patching* _GetPatchingSystem();
	Ipc* _GetIpc();

	void Send(IPC::Message* message);
	void SetOsCursor();
	void SetOsCursorShape();
	void SetOsCursorClip();
	RGSC_HRESULT HideUi();
	void SetWelcomeNotificationEnabled(bool bEnabled) { m_SendWelcomeNotification = bEnabled; }
	bool SendScuiPing();
	void OnPongReceived();
	void OnPageLoadError(const char* jsonReason);
	void OnWindowCreated(u64 browserHandle, u64 parentWindowHandle);
	void OnWindowSizeChanged(int width, int height);
	void OnWindowClosing();
	void OnRendererCrashed();
	void OnMouseEnterExitEvent(const char* jsonReason, MouseEnterExitEventType eventType);
	void OnKeyEvent(s64 msg, s64 wParam, s64 lParam, s64 scKeyMods);
	void OnShutdownComplete();
	void OnWindowStateChanged(WindowState windowState);

	LRESULT TryForwardRgscPlatformMessage(WNDPROC originalProc, WindowHandle hwnd, RgscPlatformMessageHandler::RgscPlatformMsg rgscMsg, UINT msg, WPARAM wParam, LPARAM lParam);

	const Rgsc_Graphics::RenderRect& GetRenderRect() {return m_RenderRect;}

	void AcountLinkResult(const char* jsonResponse);

	rage::fiPackfile* GetOfflinePackFile() {
#if USE_RPF_FOR_OFFLINE_ASSETS
		return &m_OfflinePackFile;
#else
		return NULL;
#endif
	}

private: // only accessible from rgsc_ui.cpp
	enum State
	{
		STATE_INVALID = -1,
		STATE_LOADING,
		STATE_WAITING_FOR_PAGE_LOAD_COMPLETION,
		STATE_WAITING_FOR_JAVASCRIPT_BINDINGS,
		STATE_WAITING_FOR_JAVASCRIPT_PONG,
		STATE_WAITING_FOR_JAVASCRIPT_READY,
		STATE_JAVASCRIPT_READY,
		STATE_WAITING_FOR_RESTART,
		STATE_START_SIGN_IN,
		STATE_SIGNING_IN,
		STATE_FINISHED,
		STATE_FAILED,
		STATE_NUM_STATES
	};

	enum UiLoadingStatus
	{
		LS_NOT_LOADED,
		LS_LOADING_ONLINE,
		LS_LOADING_OFFLINE,
		LS_LOADED_ONLINE,
		LS_LOADED_OFFLINE,
		LS_LOADING_FAILED
	};

	enum UiPageTimeouts
	{
		TIMEOUT_WAITING_PAGE_LOAD = -1000,
		TIMEOUT_WAITING_JS_BINDINGS = -1001,
		TIMEOUT_WAITING_JS_PONG = -1002,
		TIMEOUT_WAITING_JS_READY = -1003,
		TIMEOUT_ERROR_SCUI_PAGE_LOAD = -1004
	};

	void StartState(State state);
	void UpdateState();
	void UpdateShutdown();

	bool InitBrowserCache();

	// Platform Message Handlers
	void InitDefaultMessageHandlers();

	void InsertMessageHandler(RgscPlatformMessageHandler::ResponseBehaviour scuiVisibleBehaviour, 
								RgscPlatformMessageHandler::ResponseBehaviour scuiHiddenBehaviour, 
								RgscPlatformMessageHandler::RgscPlatformMsg platformMsg,  
								s64 scuiVisibleReturnValue = 0, 
								s64 scuiHiddenReturnValue = 0);

	void InsertMessageRangeHandler(RgscPlatformMessageHandler::ResponseBehaviour scuiVisibleBehaviour, 
									RgscPlatformMessageHandler::ResponseBehaviour scuiHiddenBehaviour, 
									RgscPlatformMessageHandler::RgscPlatformMsg start, 
									RgscPlatformMessageHandler::RgscPlatformMsg end, 
									s64 scuiVisibleReturnValue = 0, 
									s64 scuiHiddenReturnValue = 0);

	rage::atMap<RgscPlatformMessageHandler::RgscPlatformMsg, RgscPlatformMessageHandler*> m_MsgMap;
	rage::atMap<RgscPlatformMessageHandler::RgscPlatformMsg, RgscPlatformMessageHandler*> m_MsgRangeMap;

	void ShowUiInternal();
	void HideUiInternal(bool scUiWasVisible);
	bool LoadUrl(const char* url);
	RGSC_HRESULT CreateWindowInternal(IRgscUiWindowConfiguration* config);
	bool CreateUiWindow(int width, int height);
	void CleanupWindow();
	bool LoadUi(bool bOffline);
	bool LoadUi(bool bOffline, int width, int height);
	void SetupScriptCommands();
	bool DoesTextBoxHaveFocus();
	void SendWelcomeNotification();
	void SendConnectionLostNotification();
	u32 GetStateTimeout(State state);
	bool IsTimeoutErrorCode(int errorCode);

	void GetAchievementImageIpc(unsigned int achievementId, std::vector<char>* achievementImage);
	void CreateIpcChannel(std::string* ipcChannelName);
	RGSC_HRESULT InitSubProcess();
	RGSC_HRESULT StartSubProcess(const char* cachePath, const char* chromeCommandLine);
	void UpdateSubprocess();
	void StartIpc();
	void SubProcessDebugOutput(int severity, std::string message);
	void OnMessageReceived(const IPC::Message& message);
	float GetScaleBasedOnResolution(unsigned int width, unsigned int height);
	bool ReloadUi(bool bOfflineOnly, bool showUiAfterSiteLoad);

	bool m_Initialized;
	rage::sysCriticalSectionToken m_Cs;
	rage::netStatus m_AsyncStatus;
	bool m_HotkeyEnabled;
	bool m_ClosingEnabled;
	bool m_TextboxHasFocus;
	bool m_ForwardExternalUrls;
	bool m_ReloadingUi;
	bool m_SignInOnReload;
	bool m_SignOnlineOnReload;
	State m_State;
	UiLoadingStatus m_LoadingStatus;
	RgscWindow* m_Window;
	RgscWindowType m_WindowType;
	WindowHandle m_BrowserHandle;
	WindowHandle m_WindowHandle;
	Rgsc_Graphics* m_Device;
	Rgsc_Graphics::RenderRect m_RenderRect;
	float m_UiScaleFactor;
	bool m_UserDefinedScaled;
	HCURSOR m_Cursor;
	CURSORINFO m_OldCursorInfo;
	RgscErrorDelegate* m_ErrorDelegate;
	bool m_JavascriptReadyToAcceptCommands;
	bool m_ReadyToAcceptCommands;
	bool m_WasUiJustShown;
	bool m_WasUiJustHidden;
	bool m_WaitingForUiStateResponse;
	bool m_WaitingForUiToBecomeVisible;
	bool m_SendWelcomeNotification;
	bool m_SendConnectionLostNotification;
	bool m_PongReceived;
	bool m_ScuiPageLoadError;
	unsigned m_NumPageReloadAttempts;
	Activation m_ActivationSystem;
	Patching m_PatchingSystem;	
	AccountLinkingCallback m_AccountLinkingCallback;
	rage::netTimeout m_StateLoadTimeout;
	rage::Color32 m_ModalColor;
	bool m_bIsModal;
	bool m_bIsWindowDestroyed;
	u32 m_uShutdownTimeoutMs;
	u32 m_uShutdownStartTime;
	bool m_bWaitingForScuiShutdown;
	bool m_bHibernating;
	BrowserKeyEventCallback m_KeyCallback;

	IRgscUiWindowConfiguration* m_WindowConfig;
	IRgscUiWindowConfigurationV1* m_WindowConfigV1;
	IRgscUiWindowConfigurationV2* m_WindowConfigV2;

	Ipc::Delegate m_IpcDelegate;
	Ipc m_Ipc;

	bool m_TransitioningToOnline;
	bool m_ScUiVisible;
	int m_NumNotificationsVisible;
	u32 m_AutoSignInStartTime;
	u32 m_LastJavascriptPingTime;
	bool m_SignInError;
	bool m_NonScuiUrl;
	bool m_ShowUiAfterSiteLoad;
	PROCESS_INFORMATION m_SubprocessInfo;
	unsigned m_uLastSubprocessRebootTime;
	int m_UiVisibleOnSubprocessReboot;

	char m_OnlineUrl[RGSC_MAX_URL_BUF_SIZE];
	char m_CurrentUrl[RGSC_MAX_URL_BUF_SIZE];

#if USE_RPF_FOR_OFFLINE_ASSETS
	rage::fiPackfile m_OfflinePackFile;
#endif
};

#pragma warning(pop)

} // namespace rgsc

#endif //RGSC_UI_H
