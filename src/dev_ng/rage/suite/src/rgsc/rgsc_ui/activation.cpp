// 
// activation.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "activation.h"
#include "diag/seh.h"
#include "json.h"
#include "rgsc.h"
#include "script.h"

using namespace rage;

namespace rgsc
{

RGSC_HRESULT Activation::QueryInterface(RGSC_REFIID riid, LPVOID* ppvObj)
{
	IRgscUnknown *pUnknown = NULL;

	if(ppvObj == NULL)
	{
		return RGSC_INVALIDARG;
	}

	if(riid == IID_IRgscUnknown)
	{
		pUnknown = static_cast<IActivation*>(this);
	}
	else if(riid == IID_IActivationV1)
	{
		pUnknown = static_cast<IActivationV1*>(this);
	}
	else if(riid == IID_IActivationV2)
	{
		pUnknown = static_cast<IActivationV2*>(this);
	}

	*ppvObj = pUnknown;
	if(pUnknown == NULL)
	{
		return RGSC_NOINTERFACE;
	}

	return RGSC_OK;
}

Activation::Activation()
{
}

Activation::~Activation()
{
}

bool Activation::Init()
{
	ClearCallbacks();
	return true;
}

void Activation::Update()
{

}

void Activation::Shutdown()
{
	ClearCallbacks();
}

void Activation::ClearCallbacks()
{
	m_ActivationCodeCallback = NULL;
	m_CorruptLicenseCallback = NULL;
	m_LicenseRecoveryCallback = NULL;
	m_ExpirationCodeCallback = NULL;
	m_SteamLinkingCallback = NULL;
}

static json_char* json_as_string_or_null(json_const JSONNODE* node)
{
	char* str = json_as_string(node);
	return (_stricmp("null", str) != 0) ? str : NULL;
}

//////////////////////////////////////////////////////////////////////////
// Corrupt License
//////////////////////////////////////////////////////////////////////////

RGSC_HRESULT Activation::ShowCorruptLicenseUi(StringId message, CorruptLicenseCallback callback)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(callback, catchall, hr = RGSC_INVALIDARG);

		JSONNODE *n = json_new(JSON_NODE);

		json_set_name(n, "Data");
		json_push_back(n, json_new_i("Message", message));

		rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(true,
																				 RgscUi::UI_STATE_ACTIVATION,
																				 RgscUi::UI_SUBSTATE_ACTIVATION_CORRUPTED,
																				 n),
																				 catchall,
																				 hr = RGSC_FAIL);

		m_CorruptLicenseCallback = callback;

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

void Activation::CorruptLicenseResponse()
{
	if(AssertVerify(m_CorruptLicenseCallback))
	{
		m_CorruptLicenseCallback();
	}
}

//////////////////////////////////////////////////////////////////////////
// Activation Code
//////////////////////////////////////////////////////////////////////////

RGSC_HRESULT Activation::ShowActivationCodeUi(const int contentId,
										 const char* currentSerialNumber,
										 const bool isAlreadyActivated,
										 ActivationCodeCallback callback)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(callback, catchall, hr = RGSC_INVALIDARG);

		JSONNODE *n = json_new(JSON_NODE);

		json_set_name(n, "Data");

		json_push_back(n, json_new_i("ContentId", contentId));
		json_push_back(n, json_new_a("CurrentSerialNumber", currentSerialNumber));
		json_push_back(n, json_new_b("IsAlreadyActivated", isAlreadyActivated));

		rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(true,
																				 RgscUi::UI_STATE_ACTIVATION,
																				 RgscUi::UI_SUBSTATE_ACTIVATION_MAIN,
																				 n),
																				 catchall,
																				 hr = RGSC_FAIL);

		m_ActivationCodeCallback = callback;

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT Activation::ActivationCodeResult(const int contentId, const int result, StringId message)
{
	JSONNODE *n = json_new(JSON_NODE);

	json_set_name(n, "ActivationResult");

	json_push_back(n, json_new_i("ContentId", contentId));
	json_push_back(n, json_new_i("Result", result));
	json_push_back(n, json_new_i("Message", message));

	json_char *jc = json_write_formatted(n);

	json_delete(n);

	rlDebug2("%s\n", jc);

	bool succeeded = false;
	rgsc::Script::JsActivationResult(&succeeded, jc);
	return succeeded;
}

void Activation::ActivationCodeResponse(const char* jsonResponse)
{
	if(AssertVerify(m_ActivationCodeCallback))
	{
		int contentId = 0;
		char* activationCode = NULL;
		int action = 0;

		JSONNODE *n = json_parse(jsonResponse);

		if(!AssertVerify(n != NULL))
		{
			return;
		}

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "ContentId") == 0)
			{
				contentId = json_as_int(*i);
			}
			else if(_stricmp(node_name, "ActivationCode") == 0)
			{
				activationCode = json_as_string_or_null(*i);
			}
			else if(_stricmp(node_name, "Action") == 0)
			{
				action = json_as_int(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);

		m_ActivationCodeCallback(contentId, activationCode, (ActivationAction)action);

		json_free_safe(activationCode);
	}
}

//////////////////////////////////////////////////////////////////////////
// License Recovery
//////////////////////////////////////////////////////////////////////////

RGSC_HRESULT Activation::ShowLicenseRecoveryUi(const int contentId,
										  const char* recoveryRequestCode,
										  LicenseRecoveryCallback callback)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(callback, catchall, hr = RGSC_INVALIDARG);

		JSONNODE *n = json_new(JSON_NODE);

		json_set_name(n, "Data");

		json_push_back(n, json_new_i("ContentId", contentId));
		json_push_back(n, json_new_a("RequestCode", recoveryRequestCode));

		rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(true,
																				 RgscUi::UI_STATE_ACTIVATION,
																				 RgscUi::UI_SUBSTATE_ACTIVATION_RECOVERY,
																				 n),
																				 catchall,
																				 hr = RGSC_FAIL);

		m_LicenseRecoveryCallback = callback;

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT Activation::LicenseRecoveryResult(const int contentId, const int result, StringId message)
{
	JSONNODE *n = json_new(JSON_NODE);

	json_set_name(n, "LicenseRecoveryResult");

	json_push_back(n, json_new_i("ContentId", contentId));
	json_push_back(n, json_new_i("Result", result));
	json_push_back(n, json_new_i("Message", message));

	json_char *jc = json_write_formatted(n);

	json_delete(n);

	rlDebug2("%s\n", jc);

	bool succeeded = false;
	rgsc::Script::JsLicenseExpirationResult(&succeeded, jc);
	return succeeded;
}

void Activation::LicenseRecoveryResponse(const char* jsonResponse)
{
	if(AssertVerify(m_LicenseRecoveryCallback))
	{
		int contentId = 0;
		char* requestCode = NULL;
		char* responseCode = NULL;
		int action = 0;

		JSONNODE *n = json_parse(jsonResponse);

		if(!AssertVerify(n != NULL))
		{
			return;
		}

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "ContentId") == 0)
			{
				contentId = json_as_int(*i);
			}
			else if(_stricmp(node_name, "RequestCode") == 0)
			{
				requestCode = json_as_string_or_null(*i);
			}
			else if(_stricmp(node_name, "ResponseCode") == 0)
			{
				responseCode = json_as_string_or_null(*i);
			}
			else if(_stricmp(node_name, "Action") == 0)
			{
				action = json_as_int(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);

		m_LicenseRecoveryCallback(contentId, requestCode, responseCode, (ActivationAction)action);

		json_free_safe(requestCode);
		json_free_safe(responseCode);
	}
}

//////////////////////////////////////////////////////////////////////////
// Expiration Code
//////////////////////////////////////////////////////////////////////////

RGSC_HRESULT Activation::ShowExpirationCodeUi(const int contentId,
										 const char* currentSerialNumber,
										 ExpirationCodeCallback callback)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(callback, catchall, hr = RGSC_INVALIDARG);

		JSONNODE *n = json_new(JSON_NODE);

		json_set_name(n, "Data");

		json_push_back(n, json_new_i("ContentId", contentId));
		json_push_back(n, json_new_a("CurrentSerialNumber", currentSerialNumber));

		rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(true,
																				 RgscUi::UI_STATE_ACTIVATION,
																				 RgscUi::UI_SUBSTATE_ACTIVATION_EXPIRATION,
																				 n),
																				 catchall,
																				 hr = RGSC_FAIL);

		m_ExpirationCodeCallback = callback;

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT Activation::ExpirationCodeResult(const int contentId,
										 const int result,
										 StringId message)
{
	JSONNODE *n = json_new(JSON_NODE);

	json_set_name(n, "ActivationResult");

	json_push_back(n, json_new_i("ContentId", contentId));
	json_push_back(n, json_new_i("Result", result));
	json_push_back(n, json_new_i("Message", message));

	json_char *jc = json_write_formatted(n);

	json_delete(n);

	rlDebug2("%s\n", jc);

	bool succeeded = false;
	rgsc::Script::JsLicenseExpirationResult(&succeeded, jc);
	return succeeded;
}

void Activation::ExpirationCodeResponse(const char* jsonResponse)
{
	if(AssertVerify(m_ExpirationCodeCallback))
	{
		int contentId = 0;
		char* activationCode = NULL;
		int action = 0;

		JSONNODE *n = json_parse(jsonResponse);

		if(!AssertVerify(n != NULL))
		{
			return;
		}

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "ContentId") == 0)
			{
				contentId = json_as_int(*i);
			}
			else if(_stricmp(node_name, "ActivationCode") == 0)
			{
				activationCode = json_as_string_or_null(*i);
			}
			else if(_stricmp(node_name, "Action") == 0)
			{
				action = json_as_int(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);

		m_ExpirationCodeCallback(contentId, activationCode, (ActivationAction)action);

		json_free_safe(activationCode);
	}
}

RGSC_HRESULT Activation::ShowSteamLinkingUi(SteamLinkingCallback callback)
{
	RGSC_HRESULT hr = RGSC_FAIL;

	rtry
	{
		rverify(callback, catchall, hr = RGSC_INVALIDARG);

		JSONNODE *n = json_new(JSON_NODE);

		json_set_name(n, "Data");

		// to keep the interface the same as activation for the UI implementor, I'm hard coding the message id here.
		json_push_back(n, json_new_i("Message", 42));

		// we're re-using the activation UI for Steam account linking
		rverify(GetRgscConcreteInstance()->_GetUiInterface()->C2JsRequestUiState(true,
																				 RgscUi::UI_STATE_ACTIVATION,
																				 RgscUi::UI_SUBSTATE_LINK_STEAM_ACCOUNT,
																				 n),
																				 catchall,
																				 hr = RGSC_FAIL);

		m_SteamLinkingCallback = callback;

		hr = RGSC_OK;
	}
	rcatchall
	{

	}

	return hr;
}

RGSC_HRESULT Activation::SteamLinkingResult(const bool linkSucceeded,
									   StringId message)
{
	JSONNODE *n = json_new(JSON_NODE);

	json_set_name(n, "SteamLinkingResult");

	json_push_back(n, json_new_i("Result", linkSucceeded ? 1 : 0));

	// 0 means use the default messages for Steam linking
	if(message == 0)
	{
		// these magic numbers have to match up with the ids in the localization files provided by the UI implementor.
		message = linkSucceeded ? 43 : 44;
	}

	json_push_back(n, json_new_i("Message", message));

	json_char *jc = json_write_formatted(n);

	json_delete(n);

	rlDebug2("%s\n", jc);

	// we're re-using the activation UI for Steam account linking
	bool succeeded = false;
	rgsc::Script::JsActivationResult(&succeeded, jc);
	return succeeded;
}

void Activation::SteamLinkingResponse(const char* jsonResponse)
{
	if(AssertVerify(m_SteamLinkingCallback))
	{
		int action = 0;

		JSONNODE *n = json_parse(jsonResponse);

		if(!AssertVerify(n != NULL))
		{
			return;
		}

		JSONNODE_ITERATOR i = json_begin(n);
		while(i != json_end(n))
		{
			if(!AssertVerify(*i != NULL))
			{
				return;
			}

			// get the node name and value as a string
			json_char *node_name = json_name(*i);

			if(_stricmp(node_name, "Action") == 0)
			{
				action = json_as_int(*i);
			}

			// cleanup and increment the iterator
			json_free_safe(node_name);
			++i;
		}

		json_delete(n);

		Assert(action != 0);

		m_SteamLinkingCallback((ActivationAction)action);
	}
}

} // namespace rgsc
