The Rockstar Games Social Club User Interface, provides the UI for Rockstar's PC Platform.

It is implemented as a DLL project that uses an open source project called Berkelium (http://berkelium.org/). 
Berkelium is a library that provides off-screen HTML/CSS/Flash/Javascript rendering via Google's open source Chromium web browser.
It basically acts as a middle layer between the socialclub.dll and Chrome that renders web pages to a memory buffer, allows us to programatically inject mouse and keyboard input, etc.

IMPORTANT: the Berkelium project submitted into Rage will not compile as is. It relies on Chromium source code which is too large to submit into Rage.

The following are instructions on how to obtain the Chromium source code for those who need to make changes to our (modified) branch of Berkelium.

Most of this information can also be found in the documentation that comes with Berkelium.

============================================================================
Requirements
============================================================================

Windows SDK 7.1
	If installation fails, look at the log:
	If it mentions vc_redist.exe failed to install, uninstall VC++ Redistributables (x86 and x64) and try installing the Windows SDK again. 
	If it mentions an error installing the VC compilers, there might be a newer version of the compiler already installed. In that case, re-install the Windows SDK and unselect the VC++ compiler checkbox.
	After installing the SDK, you can go to Start Menu->Microsoft Windows SDK 7.1->Visual Studio Registration->Windows SDK Configuration Tool. Then select v7.1 and choose the Visual Studio version you're using (i.e. 2008). Click Make Current.
		Note: if this option doesn't appear in your Start menu, you may need to manually configure it in the registry: HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Microsoft SDKs\Windows\

May need this hotfix if you get an error linking: https://connect.microsoft.com/VisualStudio/Downloads/DownloadDetails.aspx?DownloadID=11399

============================================================================
Getting the Chromium Source Code
============================================================================
1. In the folder that contains the Berkelium folder (i.e. rage\3rdParty\), create a folder called 'chromium'.
	Note: Make sure you have at least 20GB available for the Chromium directory.
	You can create a symbolic link to another drive instead, if necessary (i.e. mklink /D rage\3rdParty\chromium d:\projects\chromium).
	I used this to move my Chromium directory off of my SSD. You probably won't need to build Chromium that often (maybe only once) so it's not worth wasting 20GB of fast SSD storage.
2. Download the depot_tools zip file from http://www.chromium.org/developers/how-tos/install-depot-tools
3. Extract the zip file to the chromium folder you created in step 1. You should now have chromium\depot_tools. Make sure it didn't extract to chromium\depot_tools\depot_tools.
3.5 Add the path to depot_tools to your PATH environment variable.
4. In the chromium folder you created in step 1, create another folder call 'chromium' (so now you will have rage\3rdParty\chromium\chromium)
5. Open a dos shell and cd to the rage\3rdParty\chromium\chromium directory
6. Enter 'gclient config http://src.chromium.org/svn/releases/8.0.552.237'. Note, this version number comes from the VERSION.txt file included with Berkelium. A future version of Berkelium may support a higher version of Chromium. See rage\3rdParty\Berkelium\VERSION.txt.
	This will install any necessary tools and creates a .gclient file.
7. Next, open rage\3rdParty\chromium\chromium\.gclient in a text editor.
Change:
-------------------------------------------------------------------------------------
    "custom_deps" : {
    },
-------------------------------------------------------------------------------------
To:
-------------------------------------------------------------------------------------
  "custom_deps": {      
    "src/third_party/WebKit/LayoutTests": None,
    "src/chrome/tools/test/reference_build/chrome": None,
    "src/chrome_frame/tools/test/reference_build/chrome": None,
    "src/chrome/tools/test/reference_build/chrome_linux": None,
    "src/chrome/tools/test/reference_build/chrome_mac": None,
    "src/third_party/hunspell_dictionaries": None,
  },
-------------------------------------------------------------------------------------
Save the file and close it.
This will reduce the amount of data that will be synced from the subversion repository in the next step.
8. Warning: this step will take a long time. It syncs the Chromium source and all related data.
	In your command shell (make sure the current directory is still rage\3rdParty\chromium\chromium), type 'gclient sync'.

============================================================================
Apply Patches
============================================================================
9. Download Patch for Windows: http://gnuwin32.sourceforge.net/packages/patch.htm. You just need the Setup package, not the source code. 
10. Install Patch to the default location. Take note of the installation directory.
11. Add the path to Patch.exe to your PATH environment variable. For example: "C:\Program Files (x86)\GnuWin32\bin".
12. Make sure your current working directory is rage\suite\src\rgsc\rgsc_ui\. Run applyChromiumPatches.bat.
	If all goes well, it should successfully apply all of the Chromium source code patches supplied by Berkelium.
	Backups of the original files will be copied to rage\3rdParty\Berkelium\UnpatchedChromiumBackups in case the originals are ever needed.

============================================================================
Build Chromium
============================================================================
13. Set up your build environment. Follow the "Build environment" instructions here: http://www.chromium.org/developers/how-tos/build-instructions-windows
	In particular, you will need:
		VS 2008 with SP1 installed
		All of the hotfixes listed on that page.
		Windows SDK v7.1 (v6.1 won't work).
		DirectX SDK
		You don't need Cygwin.
14. Open rage\3rdParty\chromium\chromium\src\chrome\chrome.sln
15. Choose either a Debug or Release build from the configurations drop-down. I recommend Release unless you're trying to modify Chromium internals.
16. You don't need to build the whole solution, just the chrome project. In the solution explorer, there will be many folders listed. (apps), (base), (chrome), etc. Under (chrome) is a project called chrome. Right-click it and build (using IncrediBuild with about 20 computers, it took 15 minutes).
	If everything went well, you should be able to launch Chrome from rage\3rdParty\chromium\chromium\src\chrome\Release\chrome.exe

============================================================================
Build Berkelium
============================================================================
17. Open rage\3rdParty\Berkelium\win32\berkelium.sln. Choose Debug or Release and build the solution.
	If you want to run one of the berkelium demo projects, you will first need to copy the following files from chromium\src\chrome\Release\ or chromium\src\chrome\Debug\ to berkelium\win32:
        avcodec-52.dll
        avformat-52.dll
        avutil-50.dll
        icudt42.dll
        wow_helper.exe

============================================================================
Build Rgsc
============================================================================
18. Open \rage\suite\src\rgsc\rgsc\rgsc.sln. Choose Debug or Release and build the solution.
	A post-build event will copy all of the necessary binaries to %ProgramFiles(x86)%/Rockstar Games/Social Club/ or %ProgramFiles(x86)%/Rockstar Games/Social Club Debug/. This is the location from which our games load the DLLs.





============================================================================
Building the full Social Club SDK
============================================================================
The SDK is assembled by the collect_rgsc_sdk.bat script.
Before running the script, build sample_session and rgsc projects (debug, release, win32, and x64).

Requirements for building the full SDK:

TortoiseSVN.
	SVN url: https://rsgedisvn1.rockstar.t2.corp/svn/scblades/trunk.
	Drops of the Social Club SDK are LZMA compressed, encrypted with a password, and uploaded to the SVN.
	These SDK drops are accessible from multiple R* studios and external contractors working on the Social Club UI.
	Passwords have been generated by the guidgen.exe tool installed by Visual Studio 2008. It normally resides in Program Files (x86)\Microsoft Visual Studio 9.0\Common7\Tools.
	
7-zip.
	SDK archives use LZMA compression to keep file sizes low.
	http://www.7-zip.org/download.html

Microsoft signtool.
	Signs executables and dlls. Our Rockstar key file is in perforce.
	This gets installed with Visual Studio 2008.
	
Doxygen.
	Generates documentation for the Social Club SDK in html and .chm format.
	http://www.stack.nl/~dimitri/doxygen/download.html#latestsrc.
	Just download the binaries, source code not required.
	Version tested: v1.8.2

Graphvis.
	Used by doxygen to generates graph images for the documentation.
	http://www.graphviz.org/Download_windows.php
	Version tested: v2.28
	NOTE: Add the path to the directory that contains dot.exe (eg. C:\Program Files (x86)\Graphviz2.38\bin) to your *system* PATH environment variable - not the user env vars 

Microsoft Help Compiler.
	Used by doxygen to compile the documentation to a .chm file.
	http://www.microsoft.com/en-us/download/details.aspx?id=21138
	Just download htmlhelp.exe.
	Version tested: v1.32

 Nullsoft Scriptable Install System Unicode (aka NSIS Unicode).
	Builds the installer.
	Make sure you get the Unicode version.
	http://www.scratchpaper.com/
	Version tested: v2.46.5
