#ifndef _RGSC_WEAK_STRING_H_
#define _RGSC_WEAK_STRING_H_

namespace rgsc {

// Simple POD string class. Data must be owned by caller.
template <class CharType>
struct WeakString {
    const CharType* mData;
    size_t mLength;
	unsigned char mAllocatedOnHeap;

    typedef CharType Type;

    inline const CharType* data() const {
        return mData;
    }

    inline size_t length() const {
        return mLength;
    }

    inline size_t size() const {
        return mLength;
    }

	inline bool isAllocatedOnHeap() const {
		return mAllocatedOnHeap != 0;
	}

    template <class StrType>
    inline StrType& get(StrType& ret) const {
        if (!mData || !mLength) {
            ret = StrType();
        }
        ret = StrType(mData, mLength);
        return ret;
    }

    template <class StrType>
    inline StrType get() const {
        if (!mData || !mLength) {
            return StrType();
        }
        return StrType(mData, mLength);
    }

    template <class StrType>
    inline static WeakString<CharType> point_to(const StrType&input) {
        WeakString<CharType> ret;
        ret.mData = input.data();
        ret.mLength = input.length();
		ret.mAllocatedOnHeap = false;
        return ret;
    }

    inline static WeakString<CharType> point_to(const CharType*input_data,
                                                size_t input_length,
												bool input_allocatedOnHeap = false) {
        WeakString<CharType> ret;
        ret.mData = input_data;
        ret.mLength = input_length;
		ret.mAllocatedOnHeap = input_allocatedOnHeap;
        return ret;
    }

    inline static WeakString<CharType> point_to(const CharType *input_data,
												bool input_allocatedOnHeap = false) {
        WeakString<CharType> ret;
        ret.mData = input_data;
		ret.mAllocatedOnHeap = input_allocatedOnHeap;
        for (ret.mLength = 0; (input_data != NULL) && input_data[ret.mLength]; ++ret.mLength) {
        }
        return ret;
    }

    inline static WeakString<CharType> empty() {
        WeakString<CharType> ret;
        ret.mData = NULL;
        ret.mLength = 0;
		ret.mAllocatedOnHeap = false;
        return ret;
    }
};

template <class StrType, class CharType>
inline StrType &operator+(const StrType&lhs, const WeakString<CharType>&rhs) {
    StrType temp;
    return lhs + rhs.get(temp);
}

template <class StrType, class CharType>
inline StrType &operator+=(StrType&lhs, const WeakString<CharType>&rhs) {
    StrType temp;
    return lhs += rhs.get(temp);
}

template <class OstreamType, class CharType>
inline OstreamType &operator<< (OstreamType&lhs, const WeakString<CharType>&rhs) {
    size_t length = rhs.length();
    const CharType *data = rhs.data();
    for (size_t i = 0; i < length; i++) {
        lhs << data[i];
    }
    return lhs;
}

typedef WeakString<char> URLString;
typedef WeakString<char> UTF8String;
typedef WeakString<wchar_t> WideString;
typedef WeakString<wchar_t> FileString;

}

#endif // _RGSC_WEAK_STRING_H_

