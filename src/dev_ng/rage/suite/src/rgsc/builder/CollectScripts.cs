﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Social_Club_SDK
{
    public class CollectScripts
    {
        private static string ProtectGsmlPath = "";

        public static void Init()
        {
            Program.OutputHeader("- Initializing Collection Scripts -");

            if (!Directory.Exists(Settings.DEST_DIR_ROOT))
            {
                Program.Output(String.Format("Collect script output directory {0} does not exist, creating.", Settings.DEST_DIR_ROOT));
                Directory.CreateDirectory(Settings.DEST_DIR_ROOT);
            }

            if (!Directory.Exists(Settings.DEST_DIR_SDK_GUARDIFACTS))
            {
                Program.Output(String.Format("Collect script output directory {0} does not exist, creating.", Settings.DEST_DIR_SDK_GUARDIFACTS));
                Directory.CreateDirectory(Settings.DEST_DIR_SDK_GUARDIFACTS);
            }


            if (!Directory.Exists(Settings.DEST_DIR_SDK_GUARDIFACTS32))
            {
                Program.Output(String.Format("Collect script output directory {0} does not exist, creating.", Settings.DEST_DIR_SDK_GUARDIFACTS32));
                Directory.CreateDirectory(Settings.DEST_DIR_SDK_GUARDIFACTS32);
            }

            CodeCleanup();

            Program.Report("Collection scripts initialized successfully");
        }

        public static void CodeCleanup()
        {
            // Clear Debug/Release x86 Folders
            if (Directory.Exists(Settings.RGSC_CLEANUP_DIR_DEBUG))
            {
                Program.Output(String.Format("Cleanup debug lib/obj folder", Settings.RGSC_CLEANUP_DIR_DEBUG));
                FileHelper.ClearFolder(Settings.RGSC_CLEANUP_DIR_DEBUG, false);
            }
            if (Directory.Exists(Settings.RGSC_CLEANUP_DIR_RELEASE))
            {
                Program.Output(String.Format("Cleanup release lib/obj folder", Settings.RGSC_CLEANUP_DIR_RELEASE));
                FileHelper.ClearFolder(Settings.RGSC_CLEANUP_DIR_RELEASE, false);
            }

            // Clear Debug/Release x64 Folders
            if (Directory.Exists(Settings.RGSC_CLEANUP_DIR_DEBUG_X64))
            {
                Program.Output(String.Format("Cleanup debug lib/obj folder", Settings.RGSC_CLEANUP_DIR_DEBUG_X64));
                FileHelper.ClearFolder(Settings.RGSC_CLEANUP_DIR_DEBUG_X64, false);
            }
            if (Directory.Exists(Settings.RGSC_CLEANUP_DIR_RELEASE_X64))
            {
                Program.Output(String.Format("Cleanup release lib/obj folder", Settings.RGSC_CLEANUP_DIR_RELEASE_X64));
                FileHelper.ClearFolder(Settings.RGSC_CLEANUP_DIR_RELEASE_X64, false);
            }

            File.Delete(Settings.RGSC_CLEANUP_SDF);
        }

        public static void Collect()
        {
            // Debug -> SAMPLE SESSION + DLLS + Metadata
            
            // TODO: win32 temporarily not included
            // FileHelper.FileCopy(Settings.SAMPLE_DBG_SESSION_DIR + "\\sample_session_win32_debug.exe", Settings.DEST_DBG_DIR_TEST_APP);
            // FileHelper.FileCopy(Settings.SAMPLE_DBG_SESSION_DIR + "\\sample_session_win32_debug.pdb", Settings.DEST_DBG_DIR_TEST_APP);
            // FileHelper.FileCopy(Settings.SAMPLE_DBG_SESSION_DIR + "\\sample_session_win32_debug.map", Settings.DEST_DBG_DIR_TEST_APP);
            Directory.CreateDirectory(Settings.DEST_DBG_DIR_TEST_APP_64);
            Directory.CreateDirectory(Settings.DEST_DBG_DIR_TEST_APP);
            FileHelper.FileCopy(Settings.SAMPLE_DBG_SESSION_64_DIR + "\\sample_session_win64_debug.exe", Settings.DEST_DBG_DIR_TEST_APP_64);
            FileHelper.FileCopy(Settings.SAMPLE_DBG_SESSION_64_DIR + "\\sample_session_win64_debug.pdb", Settings.DEST_DBG_DIR_TEST_APP_64);
            FileHelper.FileCopy(Settings.SAMPLE_DBG_SESSION_64_DIR + "\\sample_session_win64_debug.map", Settings.DEST_DBG_DIR_TEST_APP_64);
            FileHelper.FileCopy(Settings.SHADOWLIBDLL, Settings.DEST_DBG_DIR_TEST_APP_64);
            FileHelper.FileCopy(Settings.GFSDKTXAADLL, Settings.DEST_DBG_DIR_TEST_APP_64);
            FileHelper.FileCopy(Settings.ALPHARESOLVEDLL, Settings.DEST_DBG_DIR_TEST_APP_64);
            FileHelper.FileCopy(Settings.METADATA_DIR + "\\metadata.dat", Settings.DEST_DBG_DIR_TEST_APP);
            FileHelper.FileCopy(Settings.METADATA_DIR + "\\metadata.dat", Settings.DEST_DBG_DIR_TEST_APP_64);

            // Release -> SAMPLE SESSION + DLLS + Metadata
            // TODO: Release temporarily not included
            // FileHelper.FileCopy(Settings.SAMPLE_SESSION_DIR + "\\sample_session_win32_final_2008.exe", Settings.DEST_DIR_TEST_APP);
            // FileHelper.FileCopy(Settings.SAMPLE_SESSION_DIR + "\\sample_session_win32_final_2008.pdb", Settings.DEST_DIR_TEST_APP);
            // FileHelper.FileCopy(Settings.SAMPLE_SESSION_DIR + "\\sample_session_win32_final_2008.map", Settings.DEST_DIR_TEST_APP);
            // FileHelper.FileCopy(Settings.SAMPLE_SESSION_64_DIR + "\\sample_session_win64_final_2008.exe", Settings.DEST_DIR_TEST_APP_64);
            // FileHelper.FileCopy(Settings.SAMPLE_SESSION_64_DIR + "\\sample_session_win64_final_2008.pdb", Settings.DEST_DIR_TEST_APP_64);
            // FileHelper.FileCopy(Settings.SAMPLE_SESSION_64_DIR + "\\sample_session_win64_final_2008.map", Settings.DEST_DIR_TEST_APP_64);

            // Public Interface Headers
            FileHelper.FileCopy(Settings.PUBLIC_INTERFACE_HEADERS, Settings.DEST_DIR_SDK_INCLUDES);

            // Erase Old Guardifacts folders
            if (Settings.EnableProtection)
            {
                FileHelper.ClearFolder(Settings.DEST_DIR_SDK_GUARDIFACTS, true);
                DirectoryInfo folder = new DirectoryInfo(Settings.DEST_DIR_SDK_GUARDIFACTS);
                foreach (DirectoryInfo dir in folder.GetDirectories())
                {
                    Directory.Delete(dir.FullName);
                }

                FileHelper.ClearFolder(Settings.DEST_DIR_SDK_GUARDIFACTS32, true);
                DirectoryInfo folder32 = new DirectoryInfo(Settings.DEST_DIR_SDK_GUARDIFACTS32);
                foreach (DirectoryInfo dir in folder32.GetDirectories())
                {
                    Directory.Delete(dir.FullName);
                }
            }

            Program.SetProgress(65);

            // Protect DLL parse strings
            if (Settings.EnableProtection)
            {
                string protectionPath = Settings.RGSC_PROTECTION_ROOT + "\\rttihandling\\parseForStrings.rb";
                string protectionDll = Settings.RGSC_DIR_X64 + "\\socialclub.dll";
                string protectionArgs = String.Format("--path \"{0}\"", protectionDll);
                RunProtectScript(protectionPath, protectionArgs);
                ReadProtectLog(protectionDll, ".stringWarnings.log");
            }

            // Sign DLL
            string signtool = Settings.SIGN_TOOL_ROOT + "\\signtool.exe";
            string signFile = Settings.RGSC_DIR_X64 + "\\socialclub.dll";
            TrySignFile(signtool, Settings.SIGNING_KEY, Settings.SIGNPW, Settings.SIGNENDPOINT, signFile, "");

            // Debug Cef Files
            Directory.CreateDirectory(Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\d3dcompiler_43.dll", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\d3dcompiler_47.dll", Settings.DEST_DBG_DIR_RGSC);
            //FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\ffmpegsumo.dll", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\icudtl.dat", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\cef.pak", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\cef_100_percent.pak", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\cef_200_percent.pak", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\natives_blob.bin", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\snapshot_blob.bin", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\widevinecdmadapter.dll", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\locales", Settings.DEST_DBG_DIR_RGSC + "\\locales");
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\libcef.dll", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\libEGL.dll", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\libGLESv2.dll", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\socialclub.dll", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\socialclub.pdb", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\socialclub.map", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\SocialClubHelper.exe", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\SocialClubHelper.pdb", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\SocialClubHelper.map", Settings.DEST_DBG_DIR_RGSC);
            //FileHelper.FileCopy(Settings.RGSC_DBG_DIR + "\\steam_api.dll", Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.ScuiAssetsPath, Settings.DEST_DBG_DIR_RGSC);
            FileHelper.FileCopy(Settings.UNINSTALLER_EXE_DEBUG, Settings.DEST_DBG_DIR_RGSC);

            // Debug Cef Files x64
            Directory.CreateDirectory(Settings.DEST_DBG_DIR_RGSC_X64);
            Directory.CreateDirectory(Settings.DEST_DBG_DIR_RGSC_X64 + "\\swiftshader");
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\swiftshader\\libEGL.dll", Settings.DEST_DBG_DIR_RGSC_X64 + "\\swiftshader");
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\swiftshader\\libGLESv2.dll", Settings.DEST_DBG_DIR_RGSC_X64 + "\\swiftshader");
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\chrome_elf.dll", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\d3dcompiler_43.dll", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\d3dcompiler_47.dll", Settings.DEST_DBG_DIR_RGSC_X64);
            //FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\ffmpegsumo.dll", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\icudtl.dat", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\cef.pak", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\cef_100_percent.pak", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\cef_200_percent.pak", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\devtools_resources.pak", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\natives_blob.bin", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\snapshot_blob.bin", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\v8_context_snapshot.bin", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\widevinecdmadapter.dll", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\locales", Settings.DEST_DBG_DIR_RGSC_X64 + "\\locales");
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\libcef.dll", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\libEGL.dll", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\libGLESv2.dll", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\socialclub.dll", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\socialclub.pdb", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\socialclub.map", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\SocialClubHelper.exe", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\SocialClubHelper.pdb", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\SocialClubHelper.map", Settings.DEST_DBG_DIR_RGSC_X64);
            //FileHelper.FileCopy(Settings.RGSC_DBG_DIR_X64 + "\\steam_api64.dll", Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.ScuiAssetsPath, Settings.DEST_DBG_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.UNINSTALLER_EXE_DEBUG, Settings.DEST_DBG_DIR_RGSC_X64);

            // Cef Files Release
            Directory.CreateDirectory(Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\d3dcompiler_43.dll", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\d3dcompiler_47.dll", Settings.DEST_DIR_RGSC);
            //FileHelper.FileCopy(Settings.RGSC_DIR + "\\ffmpegsumo.dll", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\icudtl.dat", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\cef.pak", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\cef_100_percent.pak", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\cef_200_percent.pak", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\natives_blob.bin", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\snapshot_blob.bin", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\widevinecdmadapter.dll", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\locales", Settings.DEST_DIR_RGSC + "\\locales");
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\libcef.dll", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\libEGL.dll", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\libGLESv2.dll", Settings.DEST_DIR_RGSC);
            //FileHelper.FileCopy(Settings.RGSC_DIR + "\\socialclub.dll", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\SocialClubHelper.exe", Settings.DEST_DIR_RGSC);
            //FileHelper.FileCopy(Settings.RGSC_DIR + "\\steam_api.dll", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.ScuiAssetsPath, Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.UNINSTALLER_EXE, Settings.DEST_DIR_RGSC);

            // Release Cef Files x64
            Directory.CreateDirectory(Settings.DEST_DIR_RGSC_X64);
            Directory.CreateDirectory(Settings.DEST_DIR_RGSC_X64 + "\\swiftshader");
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\swiftshader\\libEGL.dll", Settings.DEST_DIR_RGSC_X64 + "\\swiftshader");
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\swiftshader\\libGLESv2.dll", Settings.DEST_DIR_RGSC_X64 + "\\swiftshader");
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\chrome_elf.dll", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\d3dcompiler_43.dll", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\d3dcompiler_47.dll", Settings.DEST_DIR_RGSC_X64);
            //FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\ffmpegsumo.dll", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\icudtl.dat", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\cef.pak", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\cef_100_percent.pak", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\cef_200_percent.pak", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\locales", Settings.DEST_DIR_RGSC_X64 + "\\locales");
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\natives_blob.bin", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\snapshot_blob.bin", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\v8_context_snapshot.bin", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\widevinecdmadapter.dll", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\libcef.dll", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\libEGL.dll", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\libGLESv2.dll", Settings.DEST_DIR_RGSC_X64);
            // FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\socialclub.dll", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\SocialClubHelper.exe", Settings.DEST_DIR_RGSC_X64);
            //FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\steam_api64.dll", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.ScuiAssetsPath, Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.UNINSTALLER_EXE, Settings.DEST_DIR_RGSC_X64);

            Program.SetProgress(70);

            // Guard IT / Protect IT - x64
            {
                string protectIT = Settings.RGSC_PROTECTION_ROOT + "\\ProtectIT.rb";
                string protectArgs = "-t -a x64 -r release -g \"" + Settings.RGSC_PROTECTION_ROOT + "\\rgsc.gsml\" -c -i \"" + Settings.RGSC_DIR_X64 + "\\socialclub.dll\"";
                RunProtectScript(protectIT, protectArgs);

                // Artifacts
                string artifactor = Settings.RGSC_PROTECTION_ROOT + "\\artifactCollector\\artifactCollector.rb";
                string artifactorDst = "-s \"" + Settings.DEST_DIR_SDK_GUARDIFACTS + "\"";
                RunProtectScript(artifactor, artifactorDst);
            }

            // Guard IT / Protect IT - x86
            {
                string protectIT = Settings.RGSC_PROTECTION_ROOT + "\\ProtectIT.rb";
                string protectArgs = "-t -a x86 -r release -g \"" + Settings.RGSC_PROTECTION_ROOT + "\\rgsc32.gsml\" -c -i \"" + Settings.RGSC_DIR + "\\socialclub.dll\"";
                RunProtectScript(protectIT, protectArgs);

                // Artifacts
                string artifactor = Settings.RGSC_PROTECTION_ROOT + "\\artifactCollector\\artifactCollector.rb";
                string artifactorDst = "-s \"" + Settings.DEST_DIR_SDK_GUARDIFACTS32 + "\"";
                RunProtectScript(artifactor, artifactorDst);
            }

            Program.SetProgress(75);

            // Sign Directories
            SignAllInFolder(Settings.DEST_DIR_RGSC, ".exe", signtool, Settings.SIGNING_KEY, Settings.SIGNPW, Settings.SIGNENDPOINT, "steam");
            SignAllInFolder(Settings.DEST_DIR_RGSC, ".dll", signtool, Settings.SIGNING_KEY, Settings.SIGNPW, Settings.SIGNENDPOINT, "steam");
            SignAllInFolder(Settings.DEST_DIR_RGSC_X64, ".exe", signtool, Settings.SIGNING_KEY, Settings.SIGNPW, Settings.SIGNENDPOINT, "steam");
            SignAllInFolder(Settings.DEST_DIR_RGSC_X64, ".dll", signtool, Settings.SIGNING_KEY, Settings.SIGNPW, Settings.SIGNENDPOINT, "steam");

            Program.SetProgress(80);

            // Build Documentation
            Directory.CreateDirectory(Settings.DEST_DIR_SDK_DOCUMENTATION);
            BuildDocumentation();

            Program.SetProgress(85);

            // These files can not be included in the installer
            if (File.Exists(Settings.DEST_DIR_RGSC + "\\socialclub.pdb"))
                Program.ErrorAndExit("Fatal: PDB file found in destination directory.");
            if (File.Exists(Settings.DEST_DIR_RGSC + "\\socialclub.map"))
                Program.ErrorAndExit("Fatal: map file found in destination directory.");
            if (File.Exists(Settings.DEST_DIR_RGSC + "\\SocialClubHelper.pdb"))
                Program.ErrorAndExit("Fatal: PDB file found in destination directory.");
            if (File.Exists(Settings.DEST_DIR_RGSC + "\\SocialClubHelper.map"))
                Program.ErrorAndExit("Fatal: map file found in destination directory.");
            if (File.Exists(Settings.DEST_DIR_RGSC_X64 + "\\socialclub.pdb"))
                Program.ErrorAndExit("Fatal: PDB file found in destination directory.");
            if (File.Exists(Settings.DEST_DIR_RGSC_X64 + "\\socialclub.map"))
                Program.ErrorAndExit("Fatal: map file found in destination directory.");
            if (File.Exists(Settings.DEST_DIR_RGSC_X64 + "\\SocialClubHelper.pdb"))
                Program.ErrorAndExit("Fatal: PDB file found in destination directory.");
            if (File.Exists(Settings.DEST_DIR_RGSC_X64 + "\\SocialClubHelper.map"))
                Program.ErrorAndExit("Fatal: map file found in destination directory.");

            // Build the Social Club Installer
            // Note: make sure there are no trailing slashes at the end of the arguments to BuildRGSC.exe or it won't work.
            string buildPath = FileHelper.ConvertPath("%RGSC_RAGE_DIR%\\suite\\src\\rgsc\\installer\\BuildRGSC.exe");
            string nsisPath = ConfigurationManager.AppSettings["NSISPath"];
            string workingDir = Settings.RGSC_RAGE_DIR + "\\suite\\src\\rgsc\\installer";
            string x86Dest = FileHelper.ConvertPath("%DEST_DIR_ROOT%\\x86\\Social Club");
            string x64Dest = FileHelper.ConvertPath("%DEST_DIR_ROOT%\\x64\\Social Club");
            string installerPath = FileHelper.ConvertPath("%DEST_DIR_SDK%\\installer");
            BuildInstaller(workingDir, buildPath, nsisPath, x86Dest, x64Dest, installerPath, false);

            // Build the Social Club DEBUG Installer
            // Note: make sure there are no trailing slashes at the end of the arguments to BuildRGSC.exe or it won't work.
            x86Dest = FileHelper.ConvertPath("%DEST_DIR_ROOT%\\x86\\Social Club Debug");
            x64Dest = FileHelper.ConvertPath("%DEST_DIR_ROOT%\\x64\\Social Club Debug");
            BuildInstaller(workingDir, buildPath, nsisPath, x86Dest, x64Dest, installerPath, true);

            Program.SetProgress(90);

            // Sign the Social Club Installer
            if (Settings.EnableInstaller)
            {
                string signAllFolder = Settings.DEST_DIR_SDK + "\\installer";
                SignAllInFolder(signAllFolder, ".exe", signtool, Settings.SIGNING_KEY, Settings.SIGNPW, Settings.SIGNENDPOINT, "steam");
            }

            // REM make sure to copy the pdbs to the release directory *after* building the installer so it doesn't get included as part of the installer!
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\socialclub.pdb", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\socialclub.map", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\SocialClubHelper.pdb", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\SocialClubHelper.map", Settings.DEST_DIR_RGSC);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\socialclub.pdb", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\socialclub.map", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\SocialClubHelper.pdb", Settings.DEST_DIR_RGSC_X64);
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\SocialClubHelper.map", Settings.DEST_DIR_RGSC_X64);

            // Copy over the original binaries after the installer's been made so that developers can run the game/apps in debug mode
            FileHelper.FileCopy(Settings.RGSC_DIR_X64 + "\\socialclub.dll", Settings.DEST_DIR_RGSC_X64, false);
            FileHelper.FileCopy(Settings.RGSC_DIR + "\\socialclub.dll", Settings.DEST_DIR_RGSC, false);

            // Validate DLLs exist after protection
            ValidateFileExists(Settings.DEST_DIR_RGSC + "\\socialclub.dll");
            ValidateFileExists(Settings.DEST_DIR_RGSC_X64 + "\\socialclub.dll");
            ValidateFileExists(Settings.DEST_DBG_DIR_RGSC + "\\socialclub.dll");
            ValidateFileExists(Settings.DEST_DBG_DIR_RGSC_X64 + "\\socialclub.dll");

            Program.SetProgress(95);

            if(Settings.EnableProtection)
            {
                // x64
                {
                    // 7Zip the protection folder
                    Zip7Folder(Settings.DEST_DIR_SDK_GUARDIFACTS, Settings.DEST_DIR_SDK_GUARDIFACTS_ZIP);
                    FileHelper.ClearFolder(Settings.DEST_DIR_SDK_GUARDIFACTS, false);
                    Directory.Delete(Settings.DEST_DIR_SDK_GUARDIFACTS, true);

                    // Copy to N drive
                    string ArxMapPath = ConfigurationManager.AppSettings["ArxMapLocation"];
                    Directory.CreateDirectory(String.Format("{0}\\{1}", ArxMapPath, Settings.CurrentSdkBuild));
                    string ArxMapPathSdk = String.Format("{0}\\{1}\\guardifacts.zip", ArxMapPath, Settings.CurrentSdkBuild);
                    FileHelper.FileCopy(Settings.DEST_DIR_SDK_GUARDIFACTS_ZIP, ArxMapPathSdk);
                }

                // x86
                {
                    // 7Zip the protection folder
                    Zip7Folder(Settings.DEST_DIR_SDK_GUARDIFACTS32, Settings.DEST_DIR_SDK_GUARDIFACTS32_ZIP);
                    FileHelper.ClearFolder(Settings.DEST_DIR_SDK_GUARDIFACTS32, false);
                    Directory.Delete(Settings.DEST_DIR_SDK_GUARDIFACTS32, true);

                    // Copy to N drive
                    string ArxMapPath = ConfigurationManager.AppSettings["ArxMapLocation"];
                    Directory.CreateDirectory(String.Format("{0}\\{1}", ArxMapPath, Settings.CurrentSdkBuild));
                    string ArxMapPathSdk = String.Format("{0}\\{1}\\guardifacts32.zip", ArxMapPath, Settings.CurrentSdkBuild);
                    FileHelper.FileCopy(Settings.DEST_DIR_SDK_GUARDIFACTS32_ZIP, ArxMapPathSdk);
                }
            }

            // Copy the folder to the Archives folder
            Directory.CreateDirectory(Settings.SDK_ARCHIVES_DIR);
            string archiveZip = Settings.SDK_ARCHIVES_DIR + "\\" + Settings.CurrentSdkBuild + ".zip";
            Zip7Folder(Settings.DEST_DIR_ROOT, archiveZip);

            Program.SetProgress(100);

            Program.Report("Collection scripts executed successfully");
        }

        private static void BuildInstaller(string workingDir, string buildPath, string nsisPath, string x86Dest, string x64Dest, string retailDest, bool bDebugBuild)
        {
            if (!Settings.EnableInstaller)
            {
                Program.Output("Skipping installer build due to settings");
                return;
            }

            //"%RGSC_RAGE_DIR%\suite\src\rgsc\installer\BuildRGSC.exe" "%PROGRAMFILES(X86)%\\NSIS\\Unicode\\MakeNSIS.exe" "%DEST_DIR_ROOT%\x86\Social Club" "%DEST_DIR_ROOT%\x64\Social Club Retail" "%DEST_DIR_SDK%\installer\Retail"
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = buildPath;
            startInfo.WorkingDirectory = workingDir;
            startInfo.Arguments = String.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\"{4}", nsisPath, x86Dest, x64Dest, retailDest, bDebugBuild ? " -debug" : "");
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            Program.Output("Running NSIS installer: " + startInfo.Arguments);

            using (Process process = Process.Start(startInfo))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Program.Output(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    Program.Output(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {

                }
            }

            if (bDebugBuild)
            {
                string outPath = String.Format("{0}\\Debug\\Social-Club-Debug-Setup.exe", retailDest);
                ValidateFileExists(outPath);
            }
            else
            {
                string outPath = String.Format("{0}\\Social-Club-Setup.exe", retailDest);
                ValidateFileExists(outPath);
            }
        }

        private static void BuildDocumentation()
        {           
            Program.OutputHeader("Building Documentation");
            if (!Settings.EnableDocumentation)
            {
                Program.Output("Skipping documentation build due to settings");
                return;
            }

            string HtmlHelpPath = ConfigurationManager.AppSettings["HtmlHelpPath"];
            string DoxygenPath = ConfigurationManager.AppSettings["DoxygenPath"];

            string cmd = FileHelper.ConvertPath("(type \"%RGSC_RAGE_DIR%\\suite\\src\\rgsc\\documentation\\Doxyfile\" && echo OUTPUT_DIRECTORY = \"%RGSC_RAGE_DIR%\\suite\\src\\rgsc\\documentation\" && echo HHC_LOCATION = \"" + HtmlHelpPath + "\" && echo CHM_FILE = \"%DEST_DIR_SDK_DOCUMENTATION%\\social club.chm\") | \"" + DoxygenPath + "\" -");
            ProcessStartInfo startInfo = new ProcessStartInfo("cmd.exe", "/c " + cmd);
            startInfo.WorkingDirectory = FileHelper.ConvertPath("%RGSC_RAGE_DIR%\\suite\\src\\rgsc\\documentation");
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                process.WaitForExit();
                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {

                }
            }
        }

        private static void SignAllInFolder(string folder, string match, string tool, string key, string pw, string endpoint, string exclusionFilter)
        {
            if (!Settings.EnableSigning)
                return;

            DirectoryInfo dir = new DirectoryInfo(folder);
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                if (file.Name.Contains(match))
                {
                    TrySignFile(tool, key, pw, endpoint, file.FullName, exclusionFilter);
                }
            }

            DirectoryInfo[] subDirs = dir.GetDirectories();
            foreach (DirectoryInfo subDir in subDirs)
            {
                SignAllInFolder(subDir.FullName, match, tool, key, pw, endpoint, exclusionFilter);
            }
        }

        private static void TrySignFile(string signtool, string signKey, string signPw, string signEndPoint, string file, string exclusionFilter)
        {
            if (!String.IsNullOrEmpty(exclusionFilter) && file.Contains(exclusionFilter))
                return;

            FileInfo f = new FileInfo(file);
            if (f.IsReadOnly)
            {
                File.SetAttributes(f.FullName, FileAttributes.Normal);
            }

            bool bSigned = SignFile(signtool, signKey, signPw, signEndPoint, file);
            if (bSigned) return;

            // retry - connection issue?
            Program.Output("Error signing file '{0}', retrying in 1 second");
            System.Threading.Thread.Sleep(1000);
            bSigned = SignFile(signtool, signKey, signPw, signEndPoint, file);
            if (bSigned) return;

            // final retry
            Program.Output("Error signing file '{0}', retrying in 3 seconds");
            System.Threading.Thread.Sleep(3000);
            bSigned = SignFile(signtool, signKey, signPw, signEndPoint, file);
            if (bSigned) return;

            if (Settings.TerminateOnSignFailure)
            {
                Program.ErrorAndExit(String.Format("Failed to digital sign file: '{0}' , aborting.", file));
            }
        }

        private static bool SignFile(string signtool, string signKey, string signPw, string signEndPoint, string file)
        {
            bool bResult = true;

            Program.OutputHeader(String.Format("Signing file: {0}", file));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = signtool;
            startInfo.Arguments = String.Format("sign /f {0} /p {1} /t {2} /v \"{3}\"", signKey, signPw, signEndPoint, file);
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                //
                // Read in all the text from the process with the StreamReader.
                //
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    int numSuccess = 0, numWarning = 0, numErrors = 0;

                    Match sMatch = Regex.Match(result, @"Number of files successfully Signed: (\d)");
                    if (sMatch.Groups.Count > 1)
                    {
                        numSuccess = Convert.ToInt32(sMatch.Groups[1].Value);
                    }

                    Match wMatch = Regex.Match(result, @"Number of warnings: (\d)");
                    if (wMatch.Groups.Count > 1)
                    {
                        numWarning = Convert.ToInt32(wMatch.Groups[1].Value);
                        if (numWarning > 0)
                        {
                            string err = String.Format("Failed to sign {0}, {1} warnings found.", file, numWarning);
                            Program.Error(err);
                            bResult = false;
                        }
                    }

                    Match eMatch = Regex.Match(result, @"Number of errors: (\d)");
                    if (eMatch.Groups.Count > 1)
                    {
                        numErrors = Convert.ToInt32(eMatch.Groups[1].Value);
                        if (numErrors > 0)
                        {
                            string err = String.Format("Failed to sign {0}, {1} errors found.", file, numErrors);
                            Program.Error(err);
                            bResult = false;
                        }
                    }

                    Program.Output(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    Program.Output(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {

                }
            }

            return bResult;
        }

        private static void ValidateFileExists(string path)
        {
            if (!File.Exists(path))
            {
                string err = String.Format("ValidateFileExists: {0} is MISSING", path);
                if (Settings.TerminateOnMissingDlls)
                {
                    Program.ErrorAndExit(err);
                }
                else
                {
                    Program.Error(err);
                }
            }
        }

        private static void RunProtectScript(string protectionPath, string path)
        {
            if (!Settings.EnableProtection)
                return;

            string outputHeader = String.Format("Running protection script: ruby {0} {1}", protectionPath, path);
            Program.OutputHeader(outputHeader);

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "ruby.exe";
            startInfo.Arguments = String.Format("{0} {1}", protectionPath, path);
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                //
                // Read in all the text from the process with the StreamReader.
                //
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    string regexToMatch = @"generated\/(.*).gsml";
                    Match eMatch = Regex.Match(result, regexToMatch);
                    if (eMatch.Groups.Count > 1)
                    {
                        ProtectGsmlPath = eMatch.Groups[1].Value;
                    }

                    Program.Output(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    Program.Output(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {
                    Program.Output(String.Format("Successfully ran protection script: ruby {0} {1}", protectionPath, path));
                }
                else
                {
                    string err = String.Format("Failed to run protection script: ruby {0} {1}\nError: {2}", protectionPath, path, ResultCode);
                    if (Settings.TerminateOnProtectionFailure)
                    {
                        Program.ErrorAndExit(err);
                    }
                    else
                    {
                        Program.Error(err);
                    }
                }
            }
        }

        private static void ReadProtectLog(string protectionDll, string logName)
        {
            FileInfo f = new FileInfo(protectionDll);
            string dirInfo = f.DirectoryName;
            string path = Path.GetFileNameWithoutExtension(protectionDll);
            string logPath = Path.Combine(dirInfo, path);
            string txt = File.ReadAllText(logPath + logName);
            Program.Output(txt);
        }

        public static void Zip7Folder(string folder, string zip)
        {
            if (!Settings.EnableZip7)
                return;

            string outputHeader = String.Format("Running 7zip for folder: '{0}' to '{1}'", folder, zip);
            Program.OutputHeader(outputHeader);

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = Settings.Zip7Path;
            startInfo.Arguments = String.Format("a \"{0}\" \"{1}\\*\"", zip, folder);
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                //
                // Read in all the text from the process with the StreamReader.
                //
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Program.Output(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    Program.Output(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {
                    Program.Output(String.Format("Successfully 7zip for folder: '{0}' to '{1}'", folder, zip));
                }
                else
                {
                    string err = String.Format("Failed 7zip for folder: '{0}' to '{1}', code: {2}", folder, zip, ResultCode);
                    if (Settings.TerminateOnZip7Error)
                    {
                        Program.ErrorAndExit(err);
                    }
                    else
                    {
                        Program.Error(err);
                    }
                }
            }
        }
    }
}
