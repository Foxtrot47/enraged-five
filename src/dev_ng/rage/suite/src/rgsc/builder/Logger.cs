﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Social_Club_SDK
{
    public class Logger
    {
        static StreamWriter Sw = null;
        static FileStream Fs = null;
        public static string Seconds;
        private static Object _lock = new Object();
        private static string LogPath = "";

        public static void Init()
        {
            try
            {
                DateTime date = DateTime.UtcNow;
                DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                TimeSpan diff = date.ToUniversalTime() - origin;
                Seconds = ((int)Math.Floor(diff.TotalSeconds)).ToString();

                string jobSuffix = Program.FindArg("-job=");

                LogPath = String.Format("BuildLog_{0}{1}.log", Seconds, jobSuffix);

                LogCleanup("*BuildLog*");
                LogCleanup("*BuildResults*");

                // This could be optimised to prevent opening and closing the file for each write
                Fs = File.Open(LogPath, FileMode.Create, FileAccess.Write);
            }
            catch (System.Exception ex)
            {
                Program.HandleException(ex);
            }
        }

        private const int MAX_LOGS = 200;

        private static void LogCleanup(string search)
        {
            // Log Cleanup
            DirectoryInfo d = new DirectoryInfo(Directory.GetCurrentDirectory());
            FileInfo[] files = d.GetFiles(search).OrderBy(p => p.LastWriteTime).ToArray();
            for (int i = MAX_LOGS; i < files.Length; i++)
            {
                Log("Trimming log files to " + MAX_LOGS + ", deleting: " + files[i].Name);
                File.Delete(files[i].FullName);
            }
        }

        public static void Shutdown()
        {
            if (Fs != null)
            {
                Fs.Flush();
                Fs.Close();
                Fs = null;
                Sw = null;
            }
        }

        public static void Log(string log)
        {
            lock (_lock)
            {
                if (Fs == null)
                {
                    Fs = File.Open(LogPath, FileMode.Append, FileAccess.Write);
                }

                if (Fs == null)
                    return;

                using (Sw = new StreamWriter(Fs))
                {
                    string dtStr = String.Format("[{0}] {1}", Program.GetTimestampStr(), log);

                    Sw.WriteLine(dtStr);

                    Sw.Flush();
                    Sw = null;
                }

                Fs = null;
            }
        }
    }
}
