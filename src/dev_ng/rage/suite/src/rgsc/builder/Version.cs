﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Threading.Tasks;

namespace Social_Club_SDK
{
    public class Version
    {
        public static void Init()
        {
            string NextVersionPath = Settings.NextVersionPath;
            P4.RevertFile(NextVersionPath);
            Settings.SdkVersion = VersionFromFile(NextVersionPath);
        }

        public static string VersionFromFile(string path)
        {
            string version = Settings.SdkVersion;
            if (String.IsNullOrEmpty(version))
            {
                if (File.Exists(path))
                {
                    version = File.ReadAllText(path);
                    string[] spl = version.Split('.');
                    int intV = Convert.ToInt32(spl[0]) * 1000 + Convert.ToInt32(spl[1]) * 100 + Convert.ToInt32(spl[2]) * 10 + Convert.ToInt32(spl[3]);
                    intV++;
                    version = intV.ToString();
                    version = version.Insert(3, ".");
                    version = version.Insert(2, ".");
                    version = version.Insert(1, ".");
                }
            }

            return version;
        }

        public static void Setup()
        {
            Init();
            
            if (String.IsNullOrEmpty(Settings.SdkVersion))
            {
                Program.ErrorAndExit("Could not generate new SDK version");
            }

            string clName = String.Format("Social Club SDK {0} - Update version number", Settings.SdkVersion);
            string changeList = P4.FindOrCreateChangelist(clName);
            if (String.IsNullOrEmpty(changeList))
            {
                string err = "Failed to create changelist for Resource file updates.";
                if (Settings.TerminateOnResourceChangelistFailure)
                {
                    Program.ErrorAndExit(err);
                }
                else
                {
                    Program.Error(err);
                }
            }

            P4.CheckoutFile(changeList, Settings.NextVersionPath);
            File.WriteAllText(Settings.NextVersionPath, Settings.SdkVersion);

            string RgscResourcePath = FileHelper.ConvertPath(ConfigurationManager.AppSettings["RgscResourcePath"]);
            if (!P4.CheckoutFile(changeList, RgscResourcePath))
            {
                string err = String.Format("Failed to check out file: {0}", RgscResourcePath);
                if (Settings.TerminateOnResourceCheckoutFailure)
                {
                    Program.ErrorAndExit(err);
                }
                else
                {
                    Program.Error(err);
                }
            }
            string SubprocessResourcePath = FileHelper.ConvertPath(ConfigurationManager.AppSettings["SubprocessResourcePath"]);
            if (!P4.CheckoutFile(changeList, SubprocessResourcePath))
            {
                string err = String.Format("Failed to check out file: {0}", SubprocessResourcePath);
                if (Settings.TerminateOnResourceCheckoutFailure)
                {
                    Program.ErrorAndExit(err);
                }
                else
                {
                    Program.Error(err);
                }
            }

            string CommonInterfacePath = FileHelper.ConvertPath(ConfigurationManager.AppSettings["CommonInterfacePath"]);
            if (!P4.CheckoutFile(changeList, CommonInterfacePath))
            {
                string err = String.Format("Failed to check out file: {0}", CommonInterfacePath);
                if (Settings.TerminateOnResourceCheckoutFailure)
                {
                    Program.ErrorAndExit(err);
                }
                else
                {
                    Program.Error(err);
                }
            }
        }

        public static void Update(string version)
        {
            if (string.IsNullOrEmpty(version))
            {
                Program.ErrorAndExit("Invalid version!");
            }

            string RgscResourcePath = FileHelper.ConvertPath(ConfigurationManager.AppSettings["RgscResourcePath"]);
            string SubprocessResourcePath = FileHelper.ConvertPath(ConfigurationManager.AppSettings["SubprocessResourcePath"]);
            string CommonInterfacePath = FileHelper.ConvertPath(ConfigurationManager.AppSettings["CommonInterfacePath"]);

            ReplaceVersion(RgscResourcePath, version);
            ReplaceVersion(SubprocessResourcePath, version);
            ReplaceInterfaceVersion(CommonInterfacePath, version);

            Settings.CurrentSdkBuild = version;
        }

        public static void ReplaceVersion(string path, string version)
        {
            string rgscRs = File.ReadAllText(path);
            string result = Regex.Replace(rgscRs, "[0-9][.][0-9][.][0-9][.][0-9]", version);
            string versionWithCommas = version.Replace(".", ",");
            result = Regex.Replace(result, "[0-9][,][0-9][,][0-9][,][0-9]", versionWithCommas);
            File.WriteAllText(path, result);
        }

        public static void ReplaceInterfaceVersion(string path, string version)
        {
            string newVersion = String.Format("#define RGSC_SDK_VERSION ({0})", version.Replace(".", ""));
            string rgscRs = File.ReadAllText(path);
            string result = Regex.Replace(rgscRs, "#define RGSC_SDK_VERSION \\([0-9][0-9][0-9][0-9]\\)", newVersion);
            File.WriteAllText(path, result);
        }
    }
}
