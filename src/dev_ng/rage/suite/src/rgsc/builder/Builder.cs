﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Social_Club_SDK
{
    public class Builder
    {
        public static void Build()
        {
            Program.OutputHeader("- Building Solutions -");

            int numConfigurations = Convert.ToInt32(ConfigurationManager.AppSettings["NumConfigurations"]);
            int numSuccess = 0, numFail = 0, numSkipped = 0;

            bool retry = true;

            for (int i = 1; i <= numConfigurations; i++)
            {
                try
                {
                    string config = FileHelper.ConvertPath(ConfigurationManager.AppSettings[String.Format("Configuration{0}Name", i)]);
                    string path = FileHelper.ConvertPath(ConfigurationManager.AppSettings[String.Format("Configuration{0}Path", i)]);
                    string args = FileHelper.ConvertPath(ConfigurationManager.AppSettings[String.Format("Configuration{0}Args", i)]);

                    // Update Version/Resource Files
                    Version.Update(Settings.SdkVersion);

                    if (!Settings.EnableBuilds)
                    {
                        numSkipped++;
                    }
                    else if (string.IsNullOrEmpty(config))
                    {
                        string err = String.Format("Config{0} was empty", i);
                        if (Settings.TerminateOnEmptyBuildConfig)
                        {
                            Program.ErrorAndExit(err);
                        }
                        else
                        {
                            Program.Error(err);
                        }

                        numSkipped++;
                    }
                    else
                    {
                        string result = "";
                        bool bFailedThisRun = false;

                        for (int j = 0; j < 2; j++)
                        {
                            string fullArgs = args;

                            // clean first
                            if (j == 0)
                            {
                                fullArgs = fullArgs.Replace("/build", "/Clean");
                            }

                            Program.Output(Environment.NewLine);
                            Program.Output(String.Format("Building sln: {0}", config));
                            Program.Output(String.Format("{0} {1}", path, fullArgs));
                            ProcessStartInfo startInfo = new ProcessStartInfo();
                            startInfo.FileName = "BuildConsole.exe";
                            startInfo.Arguments = String.Format("{0} {1}", path, fullArgs);
                            startInfo.RedirectStandardOutput = true;
                            startInfo.RedirectStandardError = true;
                            startInfo.UseShellExecute = false;
                            startInfo.CreateNoWindow = true;

                            using (Process process = Process.Start(startInfo))
                            {
                                //
                                // Read in all the text from the process with the StreamReader.
                                //
                                using (StreamReader reader = process.StandardOutput)
                                {
                                    result += reader.ReadToEnd();
                                    Logger.Log(result);
                                }

                                using (StreamReader reader = process.StandardError)
                                {
                                    result += reader.ReadToEnd();
                                    Logger.Log(result);
                                }

                                int ResultCode = process.ExitCode;
                                if (ResultCode == 0)
                                {
                                    Program.Output(String.Format("Successfully {0} {1}", (j==0) ? "cleaned" : "built", config));

                                    if (j == 1)
                                    {
                                        numSuccess++;
                                        retry = true;
                                    }
                                }
                                else
                                {
                                    bFailedThisRun = true;
                                    break;
                                }
                            }
                        }

                        if (bFailedThisRun)
                        {
                            if (retry)
                            {
                                retry = false;
                                i--;
                                continue;
                            }

                            numFail++;
                            throw new Exception(String.Format("\n{1}!! Error building {0} !!\n{2}", config, Environment.NewLine, result));
                        }
                    }

                    if (Program.ConsoleAllocated)
                    {
                        Console.Title = String.Format("Build Process {0} / {1} [{2} succeeded] [{3} failed] [{4} skipped]", i, numConfigurations, numSuccess, numFail, numSkipped);
                    }

                    Program.SetProgress(30 + (5 * (i + 1)));
                }
                catch (System.Exception ex)
                {
                    Program.HandleException(ex);
                }
            }

            Program.Report(String.Format("Build Complete: {0} configurations, {1} succeeded, {2} failed, {3} skipped.", numConfigurations, numSuccess, numFail, numSkipped));
            if (Program.EnableUi && numFail > 0)
            {
                throw new Exception(String.Format("{0} Build(s) failed", numFail));
            }
            else
            {
                CopyDlls();
            }
        }

        private static void CopyDlls()
        {
            Program.SetProgress(60);
            if (Settings.EnableCollecting)
            {
                CollectScripts.Collect();
            }
            
            PrepareForSubmit();
        }

        public static void CheckVersionHistory()
        {
            P4.SyncPath(Settings.RGSC_MAIN_P4);

            string[] patchNotes = File.ReadAllLines(Settings.RGSC_MAIN_TXT);

            bool bFoundThisVersion = false;
            foreach (string str in patchNotes)
            {
                if (str.Contains(Settings.SdkVersion))
                {
                    Program.Output("Version history contained notes for " + Settings.SdkVersion);
                    bFoundThisVersion = true;
                    break;
                }
            }

            if (!bFoundThisVersion)
            {
                Program.ErrorAndExit(String.Format("Version History at \"{0}\" did not contain notes for SDK version {1}", Settings.RGSC_MAIN_TXT, Settings.SdkVersion));
            }
        }

        public static void CheckSymbols(string file, string folder)
        {
            string symChk = ConfigurationManager.AppSettings["SymChkPath"];

            string fullPath = Path.Combine(folder, file);

            Program.OutputHeader(String.Format("Checking Symbols file: {0}\\{1}", folder, file));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = symChk;
            startInfo.Arguments = String.Format("\"{0}\" /s \"{1}", fullPath, folder);
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Program.Output(result);

                    if (!result.Contains("SYMCHK: FAILED files = 0"))
                    {
                        Program.ErrorAndExit("SYMCHK found errors with: " + fullPath);
                    }
                    else if (!result.Contains("SYMCHK: PASSED + IGNORED files = 1"))
                    {
                        Program.ErrorAndExit("SYMCHK did not PASS with this file: " + fullPath);
                    }
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    Program.Output(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {

                }
            }
        }

        private static void PrepareForSubmit()
        {
            // Delete Rockstar Games Folder
            string SDKRoot = Settings.P4SDKRoot; 
            string SDK_RSGames = Path.Combine(SDKRoot, "Rockstar Games");

            // Delete Folder
            FileHelper.ClearFolder(SDK_RSGames, false);
            Directory.Delete(SDK_RSGames, true);
            Directory.CreateDirectory(SDK_RSGames);

            // Copy from Desktop
            FileHelper.FolderCopy(Settings.DEST_DIR_ROOT, SDK_RSGames);

            // P4 Changelist
            string clName = String.Format("Social Club SDK {0} - Binaries, etc", Settings.SdkVersion);
            string changelist = P4.FindOrCreateChangelist(clName);

            // add
            string excludeFilters = "preload";
            P4.AddAllFilesInFolder(changelist, SDK_RSGames, excludeFilters);

            // checkout
            P4.CheckoutAllFilesInFolder(changelist, SDK_RSGames, excludeFilters);
            P4.RevertUnchanged(changelist);

            Program.Report("Prepared for submit successful");
        }
    }
}
