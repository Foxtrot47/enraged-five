﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Social_Club_SDK
{
    public class FileHelper
    {
        public static string ConvertPath(string str)
        {
            str = str.Replace("%RGSC_RAGE_DIR%", Settings.RGSC_RAGE_DIR);
            str = str.Replace("%RGSC_TOOLS_ROOT%", Settings.RGSC_TOOLS_ROOT);
            str = str.Replace("%PROGRAMFILES(X86)%", Settings.PROGRAM_FILES);
            str = str.Replace("%DESKTOP_DIR%", Settings.DESKTOP_DIR);
            str = str.Replace("%DEST_DIR_SDK%", Settings.DEST_DIR_SDK);
            str = str.Replace("%DEST_DIR_SDK_SAMPLES%", Settings.DEST_DIR_SDK_SAMPLES);
            str = str.Replace("%ProgramW6432%", Settings.PROGRAM_FILES_X64);
            str = str.Replace("%DEST_DIR_SDK%", Settings.DEST_DIR_SDK);
            str = str.Replace("%DEST_DIR_ROOT%", Settings.DEST_DIR_ROOT);
            str = str.Replace("%DEST_DBG_DIR_RGSC%", Settings.DEST_DBG_DIR_RGSC);
            str = str.Replace("%DEST_DIR_RGSC%", Settings.DEST_DIR_RGSC);
            str = str.Replace("%DEST_DBG_DIR_RGSC_X64%", Settings.DEST_DBG_DIR_RGSC_X64);
            str = str.Replace("%DEST_DIR_RGSC_X64%", Settings.DEST_DIR_RGSC_X64);
            str = str.Replace("%DEST_DIR_SDK_DOCUMENTATION%", Settings.DEST_DIR_SDK_DOCUMENTATION);
            str = str.Replace("%BuildBranch%", Settings.BuildBranch);
            str = str.Replace("%TitleBranch%", Settings.TitleBranch);
            str = str.Replace("%SourceTitle%", Settings.SourceTitle);
            str = str.Replace("%SourceBranch%", Settings.SourceBranch);

            if (str.Contains("%") && Settings.TerminateOnPathConversionFailure)
            {
                Program.ErrorAndExit("Couldn't convert path: " + str);
            }

            return str;
        }

        public static bool DoesDirectoryContainFiles(string path)
        {
            try
            {
                foreach (string dir in Directory.GetDirectories(path))
                {
                    if (DoesDirectoryContainFiles(dir))
                        return true;
                }

                bool HasFiles = Directory.EnumerateFiles(path).Any();
                return HasFiles;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void FileCopy(string src, string dst)
        {
            FileCopy(src, dst, true);
        }

        public static void FileCopy(string src, string dst, bool bNewerOnly)
        {
            // Remove %PATH%s
            src = ConvertPath(src);
            dst = ConvertPath(dst);

            // If copying directories, call FolderCopy instead
            if (Directory.Exists(src) && Directory.Exists(dst))
            {
                FolderCopy(src, dst);
                return;
            }
            // If source is directory, create the destination
            else if (Directory.Exists(src))
            {
                Directory.CreateDirectory(dst);
                FolderCopy(src, dst);
                return;
            }

            // If destination is a directory, create the full path
            if (Directory.Exists(dst))
            {
                FileInfo f = new FileInfo(src);
                dst = Path.Combine(dst, f.Name);
            }

            DateTime srctime = DateTime.MaxValue;
            DateTime destTime = DateTime.MinValue;

            if (File.Exists(src))
            {
                FileInfo f = new FileInfo(src);
                srctime = f.LastWriteTime;
            }

            // Clear read-only flags
            if (File.Exists(dst))
            {
                FileInfo f = new FileInfo(dst);
                if (f.IsReadOnly)
                {
                    File.SetAttributes(f.FullName, FileAttributes.Normal);
                }

                destTime = f.LastWriteTime;
            }

            if (srctime > destTime || !bNewerOnly)
            {
                File.Copy(src, dst, true);
            }
            else if (bNewerOnly && srctime < destTime)
            {
                Logger.Log(String.Format("Src file '{0}' older than dest file '{1}', ignoring.", src, dst));
            }
        }

        public static void FolderCopy(string src, string dest)
        {
            DirectoryInfo srcDir = new DirectoryInfo(src);
            DirectoryInfo destDir = new DirectoryInfo(dest);

            Program.Output(String.Format("Copying folder from {0} to {1}", src, dest));

            if (!destDir.Exists)
            {
                Directory.CreateDirectory(dest);
            }

            DirectoryInfo folder = new DirectoryInfo(dest);
            if ((folder.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
            {
                folder.Attributes ^= FileAttributes.ReadOnly;
            }

            FileInfo[] files = srcDir.GetFiles();
            foreach (FileInfo file in files)
            {
                FileCopy(file.FullName, dest);
            }

            DirectoryInfo[] subDirs = srcDir.GetDirectories();
            foreach (DirectoryInfo subDir in subDirs)
            {
                string path = Path.Combine(dest, subDir.Name);
                FolderCopy(subDir.FullName, path);
            }
        }

        public static void ClearFolder(string path, bool outputSubfolders)
        {
            try
            {

                if (outputSubfolders)
                {
                    Program.Output(String.Format("Deleting files and subfolders from path: {0}", path));
                }
                else
                {
                    Logger.Log(String.Format("Deleting files and subfolders from path: {0}", path));
                }

                DirectoryInfo folder = new DirectoryInfo(path);
                if ((folder.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                {
                    folder.Attributes ^= FileAttributes.ReadOnly;
                }

                foreach (FileInfo file in folder.GetFiles())
                {
                    if (file.IsReadOnly)
                    {
                        File.SetAttributes(file.FullName, FileAttributes.Normal);
                        Logger.Log(String.Format(" Deleting Read-only File: {0}", file.FullName));
                        file.Delete();
                    }
                    else
                    {
                        Logger.Log(String.Format(" Deleting File: {0}", file.FullName));
                        file.Delete();
                    }
                }

                foreach (DirectoryInfo dir in folder.GetDirectories())
                {
                    Logger.Log(String.Format(" Deleting Folder: {0}", dir.FullName));
                    ClearFolder(dir.FullName, false);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
                throw ex;
            }
        }
    }
}
