﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Social_Club_SDK
{
    public static class Program
    {
        public static bool EnableJobMode = false;
        public static bool EnableUi = Convert.ToBoolean(ConfigurationManager.AppSettings["UiEnabled"]);

        public static string[] Args = null;

        public static SDKResults Results = new SDKResults();
        private static DateTime Start = DateTime.Now;

        public static bool ConsoleAllocated = false;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static int Main(string[] args)
        {
            Args = args;
            int result = 0;

            Start = DateTime.Now;

            Logger.Init();

            string jobModeArg = FindArg("-job=");
            if (!string.IsNullOrEmpty(jobModeArg))
            {
                EnableJobMode = true;
                EnableUi = false;
            }

            if (EnableJobMode)
            {
                string[] jobs = jobModeArg.Split(',');
                foreach (string j in jobs)
                {
                    result = JobSystem.ExecuteJob(jobModeArg);
                    if (result != 0)
                        break;
                }
            }
            else if (EnableUi)
            {
                Application.ApplicationExit += new EventHandler(OnProcessExit);
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new BuilderUi());
            }
            else
            {
                AllocConsole();
                ConsoleAllocated = true;

                Console.CancelKeyPress += new ConsoleCancelEventHandler(OnProcessExit);

                try
                {
                    Settings.Init();
                    Settings.SetupSymLinks(Settings.SourceTitle, Settings.SourceBranch);
                    P4.Login();
                    CollectScripts.Init();
                    P4.SyncPerforce();
                    SetupVerification.Verify();
                    Version.Setup();
                    Builder.Build();

                    Results.Report(String.Format("Time elapsed: {0}", GetTimestampStr()));
                    Results.GenerateReport();
                }
                catch (System.Exception ex)
                {
                    HandleException(ex);
                }
            }

            Logger.Shutdown();

            return result;
        }

        public static string FindArg(string s)
        {
            string result = "";

            foreach (string arg in Args)
            {
                if (arg.StartsWith(s))
                {
                    result = arg.Substring(s.Length);
                    break;
                }
            }

            return result;
        }

        private static void SendEmailResult(bool success)
        {
            bool emailEnabled = Settings.GetBoolSetting("EmailsEnabled");
            if (!emailEnabled)
                return;

            string EmailSender = ConfigurationManager.AppSettings["EmailSender"]; // "john.moore@rockstartoronto.com"
            string SmtpServer = ConfigurationManager.AppSettings["EmailServer"]; // "smtp-na.rockstar.t2.corp"

            MailMessage msg = new MailMessage();
            msg.To.Add(new MailAddress("john.moore@rockstartoronto.com", "John Moore"));
            msg.To.Add(new MailAddress("nick.snell@rockstartoronto.com", "Nick Snell"));
            msg.From = new MailAddress(EmailSender, "RGSC SDK AutoBuilder");
            msg.Subject = "RGSC SDK AutoBuilder " + (success ? "Succeeded" : "Failed");
            msg.IsBodyHtml = true;

            SmtpClient client = new SmtpClient(SmtpServer, 25);
            client.UseDefaultCredentials = true;

            string reportString = Results.GetReportAsHtml();
            msg.Body = reportString;

            try
            {
                client.Send(msg);
                Logger.Log("Email result sent successfully");
            }
            catch (Exception ex)
            {
               Logger.Log("Email result sent failed: " + ex.Message);
            }
        }

        public static void HandleException(System.Exception ex)
        {
            string msg = ex.StackTrace + Environment.NewLine + ex.Message;

            Program.Output(String.Format("\n\nException Triggered!\n{0}", msg));
            Program.ErrorAndExit(ex.StackTrace + Environment.NewLine + ex.Message);
        }

        static void OnProcessExit(object sender, EventArgs e)
        {
            Logger.Shutdown();
        }

        public static void SetProgress(int progress)
        {
            if (EnableUi)
            {
                BuilderUi.Instance.SetProgress(progress);
            }
        }

        public static string GetTimestampStr()
        {
            DateTime End = DateTime.Now;
            TimeSpan t = TimeSpan.FromSeconds((End - Start).TotalSeconds);
            return String.Format("{0:D2}:{1:D2}:{2:D3}", t.Minutes, t.Seconds, t.Milliseconds);
        }

        static bool firstHeader = true;
        public static void OutputHeader(string header)
        {
            if (!firstHeader && !Program.EnableUi)
            {
                Output(Environment.NewLine);
            }

            Logger.Log("*********************************************************");
            Output(header);
            Logger.Log("*********************************************************");

            firstHeader = false;

            if (!Program.EnableUi && Program.ConsoleAllocated)
            {
                Console.Title = header;
            }
        }

        public static void Output(string output)
        {
            Logger.Log(output);

            if (Program.EnableUi)
            {
                BuilderUi.Instance.OutputText(output);
            }

            Console.WriteLine(output);
        }

        public static void Report(string str)
        {
            if (!Program.EnableUi)
            {
                Output(str);
            }
            
            Results.Report(str);
        }

        public static void ErrorAndExit(string error)
        {
            if (!Program.EnableUi)
            {
                Error(error);
                Program.Output(Environment.NewLine);
                Program.Output("Press enter to terminate and check logs.");
                WaitForUserInput(true);

                Logger.Shutdown();
                Application.Exit();
                Environment.Exit(-1);
            }
            else { throw new Exception(error); }
        }

        public static void Error(string error)
        {
            Logger.Log(error);

            if (Program.EnableUi)
            {
                BuilderUi.Instance.ErrorText(error);
            }

            // Retrieve original color, set current color to red.
            ConsoleColor originalColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;

            // Write error
            Console.Error.WriteLine(error);
            System.Diagnostics.Debug.WriteLine(error);

            // Reset color
            Console.ForegroundColor = originalColor;
        }

        public static void BringConsoleToFront()
        {
            SetForegroundWindow(GetConsoleWindow());
        }

        public static void WaitForUserInput()
        {
            WaitForUserInput(false);
        }

        public static void WaitForUserInput(bool anyInput)
        {
            if (Program.ConsoleAllocated)
            {
                Program.BringConsoleToFront();

                bool bExit = false;
                while (!bExit)
                {
                    string userVal = Console.ReadLine();

                    if (anyInput)
                    {
                        bExit = true;
                    }
                    else if (userVal.ToLower() == "y")
                    {
                        Logger.Log("WaitForUserInput, user entered '" + userVal + "', continuing");
                        bExit = true;
                    }
                    else if (userVal.ToLower() == "n")
                    {
                        Logger.Log("WaitForUserInput, user entered '" + userVal + "', terminating");
                        Program.ErrorAndExit("User termination");
                        bExit = true;
                    }
                    else
                    {
                        Logger.Log("WaitForUserInput, user entered '" + userVal + "', ignoring");
                    }
                }
            }
        }

        #region Externs
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern bool AllocConsole();

        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        public static extern IntPtr GetConsoleWindow();

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);
        #endregion
    }

    public class SDKResults
    {
        public string P4User;
        public string P4SyncChangelist;
        public string SDKVersion;

        private List<String> ReportStrings = new List<String>();

        public void Clear()
        {
            ReportStrings.Clear();
        }

        public void Report(string str)
        {
            string dtStr = String.Format("[{0}] {1}", Program.GetTimestampStr(), str);
            ReportStrings.Add(dtStr);
        }

        public void GenerateReport()
        {
            string logPath = String.Format("BuildResults_{0}.log", Logger.Seconds);

            // This could be optimised to prevent opening and closing the file for each write
            List<String> Result = new List<String>();
            Result.Add("Build Results:");
            Result.Add(String.Format(" P4 User {0} synced to changelist {1}", P4User, P4SyncChangelist));

            var finalResult = Result.Concat(ReportStrings).ToList();
            File.WriteAllLines(logPath, finalResult.ToArray());
        }

        public string GetReportAsHtml()
        {
            string result = "";
            foreach (string str in ReportStrings)
            {
                result += str;
                result += "</br>";
            }

            result = result.Replace("\r\n", "\n");
            result = result.Replace("\n", "<BR>");
            return result;
        }
    }
}
