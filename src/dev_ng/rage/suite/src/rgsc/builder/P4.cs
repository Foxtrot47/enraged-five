﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Social_Club_SDK
{
    public class P4
    {
        public static void Login()
        {
            Program.OutputHeader("- P4 Login -");

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "p4.exe";
            startInfo.Arguments = "login";

            if (string.IsNullOrEmpty(Settings.P4Password) && !Program.ConsoleAllocated)
            {
                Program.ErrorAndExit("No P4Password specified for UI mode. Add this to the app.config settings.");
                return;
            }

            if (!String.IsNullOrEmpty(Settings.P4Password) || !Program.ConsoleAllocated)
            {
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.RedirectStandardInput = true;
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;
            }

            using (Process process = Process.Start(startInfo))
            {
                if (!String.IsNullOrEmpty(Settings.P4Password))
                {
                    using (StreamWriter writer = process.StandardInput)
                    {
                        writer.WriteLine(Settings.P4Password);
                    }

                    using (StreamReader reader = process.StandardOutput)
                    {
                        string result = reader.ReadToEnd();
                        Logger.Log(result);
                    }

                    using (StreamReader reader = process.StandardError)
                    {
                        string result = reader.ReadToEnd();
                        HandleSessionExpiry(result);
                        Logger.Log(result);
                    }
                }
                else
                {
                    process.WaitForExit();
                }
            }
        }

        public static void SyncPerforce()
        {
            Program.OutputHeader("- SyncPerforce -");

            Program.Results.P4SyncChangelist = GetSyncedChangelist(Settings.P4SyncPath);

            if (!Settings.EnableP4Sync)
            {
                Program.Output("Perforce syncing is disabled, skipping...");
                return;
            }

            string ragePath = Settings.P4SyncPath;
            SyncPath(ragePath);

            string dataPath = Settings.P4SyncPathData;
            SyncPath(dataPath);

            string thirdPartyPath = Settings.P4ThirdParty;
            SyncPath(thirdPartyPath);

            Program.Results.P4SyncChangelist = GetSyncedChangelist(ragePath);
        }

        public static string GetSyncedChangelist(string path)
        {
            string syncCl = "";

            Program.Output(String.Format("Retrieving synced changelist for {0}", path));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "p4.exe";
            startInfo.Arguments = String.Format("changes -m1 \"{0}#have\"", path);
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                //
                // Read in all the text from the process with the StreamReader.
                //
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    string[] results = result.Split(' ');
                    if (results.Length > 3)
                    {
                        syncCl = results[1];
                    }
                    Logger.Log(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    HandleSessionExpiry(result);
                    Logger.Log(result);
                }

                if (syncCl.Length > 0)
                {
                    Program.Output("Retrieved p4 changelist successfully.");
                    Program.Output("Synced to changelist " + syncCl);
                }
                else
                {
                    Program.Output("Retrieving p4 changelist failed");
                }
            }

            Program.Report("P4 Sync changelist retrieval was successful");
            return syncCl;
        }

        public static string GetP4User()
        {
            string p4User = "";
            Program.Output(String.Format("Retrieving P4 User"));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "p4.exe";
            startInfo.Arguments = String.Format("user -o");
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                //
                // Read in all the text from the process with the StreamReader.
                //
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Logger.Log(result);

                    Match m = Regex.Match(result, "User:\t.*\r\n");
                    if (!m.Success)
                    {
                        string err = "Could not read P4 User";
                        if (Settings.TerminateOnP4UserFailure)
                        {
                            Program.ErrorAndExit(err);
                        }
                        else
                        {
                            Program.Error(err);
                        }
                    }
                    p4User = m.Value.Substring("User:".Length);
                    p4User = p4User.Replace("\t", "").Replace("\r", "").Replace("\n", ""); // strip formatting
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    HandleSessionExpiry(result);
                    Logger.Log(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {

                }

                return p4User;
            }
        }

        public static string FindOrCreateChangelist(string desc)
        {
            // Retrieve P4 User
            Program.Results.P4User = P4.GetP4User();
            if (String.IsNullOrEmpty(Program.Results.P4User))
            {
                string err = "Failed to find P4 User";
                if (Settings.TerminateOnP4UserFailure)
                {
                    Program.ErrorAndExit(err);
                }
                else
                {
                    Program.Error(err);
                }
            }

            string cl = P4.FindChangelistByDesc(Program.Results.P4User, desc);
            if (String.IsNullOrEmpty(cl))
            {
                cl = P4.CreateChangeList(desc);
            }

            if (String.IsNullOrEmpty(cl))
            {
                string err = String.Format("Failed to create or find changelist: {0}", desc);
                if (Settings.TerminateOnP4ChangelistError)
                {
                    Program.ErrorAndExit(err);
                }
                else
                {
                    Program.Error(err);
                }
            }

            return cl;
        }

        public static string FindChangelistByDesc(string user, string desc)
        {
            string clNum = "";
            Program.Output(String.Format("Searching for P4 changelist with desc '{0}'", desc));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "p4.exe";
            startInfo.Arguments = String.Format("changes -l -s pending -u {0}", user);
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                //
                // Read in all the text from the process with the StreamReader.
                //
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    string[] results = result.Split(new string[] { "Change" }, 256, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string cl in results)
                    {
                        if (cl.Contains(desc))
                        {
                            Program.Output(String.Format("Found delete assets changelist: {0}", cl));

                            string[] clSplit = cl.Split(' ');
                            if (clSplit.Length >= 3)
                            {
                                clNum = clSplit[1];
                            }

                            break;
                        }
                    }
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    HandleSessionExpiry(result);
                    Logger.Log(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {

                }

                return clNum;
            }
        }

        public static string CreateChangeList(string desc)
        {
            string clNum = "";
            Program.Output(String.Format("Create P4 changelist with desc '{0}'", desc));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "p4.exe";
            startInfo.Arguments = String.Format("change -i");
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardInput = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                process.StandardInput.WriteLine("Change: new");
                process.StandardInput.WriteLine("Description: " + desc);
                process.StandardInput.Close();

                //
                // Read in all the text from the process with the StreamReader.
                //
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    string[] results = result.Split(' ');
                    if (results.Length >= 3)
                    {
                        clNum = results[1];
                    }
                    Logger.Log(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    HandleSessionExpiry(result);
                    Logger.Log(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {
                    
                }

                return clNum;
            }
        }

        public static bool CheckoutFile(string changelist, string file)
        {
            Program.Output(String.Format("Checking out P4 file {0} in CL#{1}", file, changelist));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "p4.exe";
            startInfo.Arguments = String.Format("edit -c {0} \"{1}\"", changelist, file);
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardInput = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Logger.Log(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    HandleSessionExpiry(result);
                    Logger.Log(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {
                    return true;
                }

                return false;
            }
        }

        public static bool CheckoutFolder(string changelist, string folder)
        {
            Program.Output(String.Format("Checking out P4 folder {0} in CL#{1}", folder, changelist));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "p4.exe";
            startInfo.Arguments = String.Format("edit -c {0} ...", changelist);
            startInfo.WorkingDirectory = folder;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardInput = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Logger.Log(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    HandleSessionExpiry(result);
                    Logger.Log(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {
                    return true;
                }

                return false;
            }
        }

        public static bool AddFolder(string changelist, string folder)
        {           

            Program.Output(String.Format("Adding P4 folder {0} in CL#{1}", folder, changelist));
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "p4.exe";
            startInfo.Arguments = String.Format("add -c {0} *", changelist);
            startInfo.WorkingDirectory = folder;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Logger.Log(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    HandleSessionExpiry(result);
                    Logger.Log(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {
                    return true;
                }

                return true;
            }
        }

        public static bool AddFile(string changelist, string file)
        {
            Program.Output(String.Format("Adding P4 file {0} in CL#{1}", file, changelist));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "p4.exe";
            startInfo.Arguments = String.Format("add -c {0} -f \"{1}\"", changelist, file);
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Logger.Log(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                  string result = reader.ReadToEnd();
                  HandleSessionExpiry(result);
                  Logger.Log(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {
                    return true;
                }

                return true;
            }
        }

        public static bool SyncPath(string path)
        {
            Program.Output(String.Format("Syncing p4 path: {0}", path));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "p4.exe";
            startInfo.Arguments = String.Format("sync \"{0}\"", path);
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                //
                // Read in all the text from the process with the StreamReader.
                //
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    if (result.Length > 255)
                    {
                        Program.Output(" Output truncated, see output log.");
                    }
                    else
                    {
                        Program.Output(result);
                    }

                    Logger.Log(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    if (result.Length > 100)
                    {
                        Program.Output(" Output truncated, see output log.");
                    }
                    else
                    {
                        Program.Output(result);
                    }

                    HandleSessionExpiry(result);
                    Logger.Log(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {
                    Program.Output(String.Format("Synced {0} successfully", path));
                    return true;
                }

                return false;
            }
        }

        public static bool Submit(string changeList)
        {
            Program.Output(String.Format("Submit perforce changelist:", changeList));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "p4.exe";
            startInfo.Arguments = String.Format("submit -c {0} {1}");
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Logger.Log(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    Logger.Log(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {
                    return true;
                }

                return false;
            }
        }

        public static string DeletePath(string changelist, string path)
        {
            string clNum = "";
            Program.Output(String.Format("Deleting perforce file(s): {0}", path));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "p4.exe";
            startInfo.Arguments = String.Format("delete -c {0} \"{1}\"", changelist, path);
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Logger.Log(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    Logger.Log(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {

                }

                return clNum;
            }
        }

        private static void HandleSessionExpiry(string result)
        {
            if (result.Contains("Your session has expired.\r\n"))
            {
                string err = "Your P4 session has expired. Please reconnect to P4 in P4V (the best P4 client).";
                if (Settings.TerminateOnP4SessionExpiry)
                {
                    Program.ErrorAndExit(err);
                }
                else
                {
                    Program.Error(err);
                }
            }
        }

        public static void CheckoutAllFilesInFolder(string changelist, string folder)
        {
            CheckoutAllFilesInFolder(changelist, folder, "");
        }

        public static void CheckoutAllFilesInFolder(string changelist, string folder, string excludeFilters)
        {
            List<String> FilesToAdd = new List<String>();
            GetAllInSubfolder(folder, ref FilesToAdd);

            // filter
            string[] filters = excludeFilters.Split(';');
            foreach (string fltr in filters)
            {
                if (!String.IsNullOrEmpty(fltr))
                {
                    FilesToAdd.RemoveAll(item => item.Contains(fltr));
                }
            }

            Parallel.ForEach(FilesToAdd, current =>
            {
                P4.CheckoutFile(changelist, current);
            });
        }

        public static void AddAllFilesInFolder(string changelist, string folder)
        {
            AddAllFilesInFolder(changelist, folder, "");
        }

        public static void AddAllFilesInFolder(string changelist, string folder, string excludeFilters)
        {
            List<String> FilesToAdd = new List<String>();
            DirectoryInfo srcDir = new DirectoryInfo(folder);
            FileInfo[] files = srcDir.GetFiles();
            foreach (FileInfo file in files)
            {
                FilesToAdd.Add(file.FullName);
            }

            List<String> Folders = new List<String>();
            GetAllSubfolders(folder, ref Folders);

            // filter
            string[] filters = excludeFilters.Split(';');
            foreach (string fltr in filters)
            {
                if (!String.IsNullOrEmpty(fltr))
                {
                    Folders.RemoveAll(item => item.Contains(fltr));
                    FilesToAdd.RemoveAll(item => item.Contains(fltr));
                }
            }

            Parallel.ForEach(Folders, current =>
            {
                P4.AddFolder(changelist, current);
            });

            Parallel.ForEach(FilesToAdd, current =>
            {
                P4.AddFile(changelist, current);
            });
        }

        private static void GetAllSubfolders(string folder, ref List<string> FilesToAdd)
        {
            DirectoryInfo srcDir = new DirectoryInfo(folder);
            DirectoryInfo[] subDirs = srcDir.GetDirectories();
            foreach (DirectoryInfo subDir in subDirs)
            {
                FilesToAdd.Add(subDir.FullName);
                GetAllSubfolders(subDir.FullName, ref FilesToAdd);
            }
        }

        private static void GetAllInSubfolder(string folder, ref List<string> FilesToAdd)
        {
            DirectoryInfo srcDir = new DirectoryInfo(folder);

            FileInfo[] files = srcDir.GetFiles();
            foreach (FileInfo file in files)
            {
                FilesToAdd.Add(file.FullName);
            }

            DirectoryInfo[] subDirs = srcDir.GetDirectories();
            foreach (DirectoryInfo subDir in subDirs)
            {
                GetAllInSubfolder(subDir.FullName, ref FilesToAdd);
            }
        }

        public static void RevertFile(string file)
        {
            Program.Output(String.Format("Reverting file: {0}", file));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "p4.exe";
            startInfo.Arguments = String.Format("revert {0}", file);
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Logger.Log(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    Logger.Log(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {

                }
            }
        }

        public static void RevertUnchanged(string changelist)
        {
            Program.Output(String.Format("Reverting perforce unchanged in CL#{0}", changelist));

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "p4.exe";
            startInfo.Arguments = String.Format("revert -a -c {0}", changelist);
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            using (Process process = Process.Start(startInfo))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Logger.Log(result);
                }

                using (StreamReader reader = process.StandardError)
                {
                    string result = reader.ReadToEnd();
                    Logger.Log(result);
                }

                int ResultCode = process.ExitCode;
                if (ResultCode == 0)
                {

                }
            }
        }
    }
}
