﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Text;
using System.IO;
using System.Security;
using System.Security.AccessControl;
using System.Security.Permissions;

namespace Social_Club_SDK
{
    public class SetupVerification
    {
        public static void Verify()
        {
            Program.OutputHeader("- SetupVerification -");

            // Check for Arxan Map write privileges
            string ArxMapPath = ConfigurationManager.AppSettings["ArxMapLocation"];
            CheckFolder(ArxMapPath, FileSystemRights.Write);
            CheckFolder(ArxMapPath, FileSystemRights.Read);

            // Check for Resource files
            string RgscResourcePath = FileHelper.ConvertPath(ConfigurationManager.AppSettings["RgscResourcePath"]);
            CheckFolder(RgscResourcePath, FileSystemRights.Read);
            string SubprocessResourcePath = FileHelper.ConvertPath(ConfigurationManager.AppSettings["SubprocessResourcePath"]);
            CheckFolder(SubprocessResourcePath, FileSystemRights.Read);

            // Check for tools
            string doxygen = ConfigurationManager.AppSettings["DoxygenPath"];
            string DoxygenInstallPath = FileHelper.ConvertPath(ConfigurationManager.AppSettings["DoxygenInstallPath"]);
            if (!File.Exists(doxygen))
            {
                Program.ErrorAndExit(String.Format("Doxygen installation at '{0}' could not be found.\nPlease install from: {1}", doxygen, DoxygenInstallPath));
            }

            string nsis = ConfigurationManager.AppSettings["NSISPath"];
            string NSISInstallPath = FileHelper.ConvertPath(ConfigurationManager.AppSettings["NSISInstallPath"]);
            if (!File.Exists(nsis))
            {
                Program.ErrorAndExit(String.Format("NSIS installation at '{0}' could not be found.\nPlease install from: {1}", nsis, NSISInstallPath));
            }

            string htmlHelp = ConfigurationManager.AppSettings["HtmlHelpPath"];
            string HtmlHelpInstallPath = FileHelper.ConvertPath(ConfigurationManager.AppSettings["HtmlHelpInstallPath"]);
            if (!File.Exists(htmlHelp))
            {
                Program.ErrorAndExit(String.Format("HTML Help installation at '{0}' could not be found.\nPlease install from: {1}", htmlHelp, HtmlHelpInstallPath));
            }

            string symChk = ConfigurationManager.AppSettings["SymChkPath"];
            string symChkInstallPath = FileHelper.ConvertPath(ConfigurationManager.AppSettings["SymChkPathInstallPath"]);
            if (!File.Exists(symChk))
            {
                Program.ErrorAndExit(String.Format("SymChk installation at '{0}' could not be found.\nPlease install from: {1}", symChk, symChkInstallPath));
            }

            string destDir = Settings.DEST_DIR_ROOT;
            if (Directory.Exists(destDir) && FileHelper.DoesDirectoryContainFiles(destDir))
            {
                Program.ErrorAndExit(String.Format("The destination directory \"{0}\" already exists and is not empty! We purposely avoid automatic deletion of this directory.", destDir));
            }

            // does installer for this version exist in P4
            string installerPath = String.Format(Settings.P4SDKRoot + "Rockstar Games\\Social Club SDK\\installer\\Social-Club-Setup.exe");
            string installerDebugPath = String.Format(Settings.P4SDKRoot + "Rockstar Games\\Social Club SDK\\installer\\Debug\\Social-Club-Debug-Setup.exe");
            if (File.Exists(installerPath) || File.Exists(installerDebugPath))
            {
                Program.ErrorAndExit(String.Format("Installer for SDK version '{0}' already exists:\n{1}", Settings.SdkVersion, installerPath));
            }

            Program.Report("Setup verification was successful");
        }

        private static void CheckFolder(string path, FileSystemRights priv)
        {
            if (!path.Contains("."))
            {
                if (!Directory.Exists(path))
                {
                    Program.Output(String.Format("Setup Verification: Path '{0}' did not exist...", path));
                }
            }
            else if (!File.Exists(path))
            {
                Program.Output(String.Format("File could not be verified: '{0}'", path));
            }

            if (!CheckFolderPermission(path, priv))
            {
                Program.Error(String.Format("You don't have {1} permissions for {0}", path, priv.ToString()));
            }
            else
            {
                Program.Output(String.Format("Verified {1} permissions for {0}", path, priv.ToString()));
            }
        }

        public static bool CheckFolderPermission(string path, FileSystemRights priv)
        {
            try
            {
                var writeAllow = false;
                var writeDeny = false;
                var accessControlList = Directory.GetAccessControl(path);
                if (accessControlList == null)
                    return false;
                var accessRules = accessControlList.GetAccessRules(true, true,
                                            typeof(System.Security.Principal.SecurityIdentifier));
                if (accessRules == null)
                    return false;

                foreach (FileSystemAccessRule rule in accessRules)
                {
                    if ((priv & rule.FileSystemRights) != priv)
                        continue;

                    if (rule.AccessControlType == AccessControlType.Allow)
                        writeAllow = true;
                    else if (rule.AccessControlType == AccessControlType.Deny)
                        writeDeny = true;
                }

                return writeAllow && !writeDeny;
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
