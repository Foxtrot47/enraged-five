﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices; 

namespace Social_Club_SDK
{
    public class Settings
    {
        #region Properties

        // Title/Build Config
        public static string TitleBranch;
        public static string BuildBranch;
        public static string SourceTitle;
        public static string SourceBranch;

        public static string NextVersionPath;
        public static string SdkVersion;
        public static string ScuiAssetsPath;
        public static string CurrentSdkBuild;

        // Functionality Toggles
        public static bool EnableP4Sync;
        public static bool EnableAssetUpdate;
        public static bool EnableBuilds;
        public static bool EnableCollecting;
        public static bool EnableSigning;
        public static bool EnableDocumentation;
        public static bool EnableInstaller;
        public static bool EnableProtection;
        public static bool EnableZip7;

        // Rgsc and System Paths
        public static string PROGRAM_FILES;
        public static string PROGRAM_FILES_X64;
        public static string RGSC_RAGE_DIR;
        public static string DESKTOP_DIR;

        // Patch Notes
        public static string RGSC_MAIN_TXT;
        public static string RGSC_MAIN_P4;

        // Termination
        public static bool TerminateOnEmptyBuildConfig;
        public static bool TerminateOnPathConversionFailure;
        public static bool TerminateOnMissingAppSetting;
        public static bool TerminateOnDuplicateAppSetting;
        public static bool TerminateOnSignFailure;
        public static bool TerminateOnProtectionFailure;
        public static bool TerminateOnP4UserFailure;
        public static bool TerminateOnP4SessionExpiry;
        public static bool TerminateOnP4ChangelistError;
        public static bool TerminateOnP4AssetChangelist;
        public static bool TerminateOnVersionDuplicate;
        public static bool TerminateOnVersionDeltaPlusOne;
        public static bool TerminateOnResourceChangelistFailure;
        public static bool TerminateOnResourceCheckoutFailure;
        public static bool TerminateOnMissingDlls;
        public static bool TerminateOnZip7Error;

        // Collect Scripts
        public static string OPTIONS;
        public static string DATELESS_OPTIONS;
        public static string SAMPLE_DBG_SESSION_DIR;
        public static string SAMPLE_SESSION_DIR;
        public static string SAMPLE_DBG_SESSION_64_DIR;
        public static string SAMPLE_SESSION_64_DIR;
        public static string SHADOWLIBDLL;
        public static string ALPHARESOLVEDLL;
        public static string GFSDKTXAADLL;
        public static string UNINSTALLER_EXE;
        public static string UNINSTALLER_EXE_DEBUG;
        public static string SIGN_TOOL_ROOT;
        public static string METADATA_DIR;
        public static string RGSC_DBG_DIR;
        public static string RGSC_DIR;
        public static string RGSC_DBG_DIR_X64;
        public static string RGSC_DIR_X64;
        public static string RGSC_TOOLS_ROOT;
        public static string RGSC_PROTECTION_ROOT;
        public static string PUBLIC_INTERFACE_HEADERS;
        public static string DEST_DIR_ROOT;
        public static string DEST_DIR_SDK;
        public static string DEST_DIR_SDK_SAMPLES;
        public static string DEST_DIR_SDK_DOCUMENTATION;
        public static string DEST_DIR_SDK_INCLUDES;
        public static string DEST_DIR_SDK_GUARDIFACTS;
        public static string DEST_DIR_SDK_GUARDIFACTS32;
        public static string DEST_DIR_SDK_GUARDIFACTS_ZIP;
        public static string DEST_DIR_SDK_GUARDIFACTS32_ZIP;
        public static string DEST_DBG_DIR_TEST_APP;
        public static string DEST_DIR_TEST_APP;
        public static string DEST_DBG_DIR_TEST_APP_64;
        public static string DEST_DIR_TEST_APP_64;
        public static string DEST_DBG_DIR_RGSC;
        public static string DEST_DIR_RGSC;
        public static string DEST_DBG_DIR_RGSC_X64;
        public static string DEST_DIR_RGSC_X64;
        public static string Zip7Path;
        public static string SDK_ARCHIVES_DIR;

        // cleanup
        public static string RGSC_CLEANUP_DIR_DEBUG;
        public static string RGSC_CLEANUP_DIR_RELEASE;
        public static string RGSC_CLEANUP_DIR_DEBUG_X64;
        public static string RGSC_CLEANUP_DIR_RELEASE_X64;
        public static string RGSC_CLEANUP_SDF;

        // signing
        public static string SIGNING_KEY;
        public static string SIGNPW;
        public static string SIGNENDPOINT;

        // P4
        public static string P4User;
        public static string P4Password;
        public static string P4SyncPath;
        public static string P4SyncPathData;
        public static string P4ThirdParty;
        public static string P4SDKRoot;

        #endregion

        static HashSet<string> DupeTester = new HashSet<string>();

        public static void Clear()
        {
            DupeTester.Clear();
        }

        public static string GetSetting(string setting, bool convertPath = true)
        {
            string result = ConfigurationManager.AppSettings[setting];
            if (convertPath)
            {
                result = FileHelper.ConvertPath(result);
            }

            if (result == null)
            {
                string err = "App Setting could not be found: " + setting;
                if (Settings.TerminateOnMissingAppSetting)
                {
                    Program.ErrorAndExit(err);
                }
                else
                {
                    Program.Error(err);
                }
            }

            if (result != "" &&
                    DupeTester.Contains(result) &&
                    result != "true" && result != "false" &&
                    setting != "RGSC_DIR_X64")
            {
                string err = "Duplicate app setting: " + setting + ", value: " + result;
                if (Settings.TerminateOnDuplicateAppSetting)
                {
                    Program.ErrorAndExit(err);
                }
                else
                {
                    Program.Error(err);
                }
            }

            DupeTester.Add(result);
            return result;
        }

        public static bool GetBoolSetting(string setting)
        {
            try
            {
                return Convert.ToBoolean(GetSetting(setting));
            }
            catch (Exception ex)
            {
                Program.Error(ex.Message);
                return false;
            }
        }

        public static void Init()
        {
            Program.OutputHeader("- Initializing Settings -");

            TerminateOnEmptyBuildConfig = GetBoolSetting("TerminateOnEmptyBuildConfig");
            TerminateOnPathConversionFailure = GetBoolSetting("TerminateOnPathConversionFailure");
            TerminateOnMissingAppSetting = GetBoolSetting("TerminateOnMissingAppSetting");
            TerminateOnDuplicateAppSetting = GetBoolSetting("TerminateOnDuplicateAppSetting");
            TerminateOnSignFailure = GetBoolSetting("TerminateOnSignFailure");
            TerminateOnProtectionFailure = GetBoolSetting("TerminateOnProtectionFailure");
            TerminateOnP4UserFailure = GetBoolSetting("TerminateOnP4UserFailure");
            TerminateOnP4SessionExpiry = GetBoolSetting("TerminateOnP4SessionExpiry");
            TerminateOnP4ChangelistError = GetBoolSetting("TerminateOnP4ChangelistError");
            TerminateOnP4AssetChangelist = GetBoolSetting("TerminateOnP4AssetChangelist");
            TerminateOnVersionDuplicate = GetBoolSetting("TerminateOnVersionDuplicate");
            TerminateOnVersionDeltaPlusOne = GetBoolSetting("TerminateOnVersionDeltaPlusOne");
            TerminateOnResourceChangelistFailure = GetBoolSetting("TerminateOnResourceChangelistFailure");
            TerminateOnResourceCheckoutFailure = GetBoolSetting("TerminateOnResourceCheckoutFailure");
            TerminateOnMissingDlls = GetBoolSetting("TerminateOnMissingDlls");
            TerminateOnZip7Error = GetBoolSetting("TerminateOnZip7Error");

            EnableP4Sync = GetBoolSetting("EnableP4Sync");
            EnableAssetUpdate = GetBoolSetting("EnableAssetUpdate");
            EnableBuilds = GetBoolSetting("EnableBuilds");
            EnableCollecting = GetBoolSetting("EnableCollecting");
            EnableSigning = GetBoolSetting("EnableSigning");
            EnableDocumentation = GetBoolSetting("EnableDocumentation");
            EnableInstaller = GetBoolSetting("EnableInstaller");
            EnableProtection = GetBoolSetting("EnableProtection");
            EnableZip7 = GetBoolSetting("EnableZip7");

            BuildBranch = GetSetting("BuildBranch");
            TitleBranch = GetSetting("TitleBranch");

            SourceTitle = Program.FindArg("-title=");
            if (string.IsNullOrEmpty(SourceTitle))
            {
                SourceTitle = GetSetting("SourceTitle");
            }

            SourceBranch = Program.FindArg("-branch=");
            if (string.IsNullOrEmpty(SourceBranch))
            {
                SourceBranch = GetSetting("SourceBranch");
            }

            NextVersionPath = GetSetting("NextVersionPath");
            ScuiAssetsPath = GetSetting("ScuiAssetsPath");

            RGSC_RAGE_DIR = Environment.GetEnvironmentVariable(GetSetting("RGSC_RAGE_DIR"), EnvironmentVariableTarget.User);
            RGSC_TOOLS_ROOT = Environment.GetEnvironmentVariable(GetSetting("RGSC_TOOLS_ROOT"), EnvironmentVariableTarget.User);
            DESKTOP_DIR = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Desktop";
            PROGRAM_FILES = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
            PROGRAM_FILES_X64 = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);

            RGSC_MAIN_TXT = GetSetting("RGSC_MAIN_TXT");
            RGSC_MAIN_P4 = GetSetting("RGSC_MAIN_P4");

            OPTIONS = GetSetting("OPTIONS");
            DATELESS_OPTIONS = GetSetting("DATELESS_OPTIONS");
            SAMPLE_DBG_SESSION_DIR = GetSetting("SAMPLE_DBG_SESSION_DIR");
            SAMPLE_SESSION_DIR = GetSetting("SAMPLE_SESSION_DIR");
            SAMPLE_DBG_SESSION_64_DIR = GetSetting("SAMPLE_DBG_SESSION_64_DIR");
            SAMPLE_SESSION_64_DIR = GetSetting("SAMPLE_SESSION_64_DIR");
            SHADOWLIBDLL = GetSetting("SHADOWLIBDLL");
            ALPHARESOLVEDLL = GetSetting("ALPHARESOLVEDLL");
            GFSDKTXAADLL = GetSetting("GFSDKTXAADLL");
            UNINSTALLER_EXE = GetSetting("UNINSTALLER_EXE");
            UNINSTALLER_EXE_DEBUG = GetSetting("UNINSTALLER_EXE_DEBUG");
            SIGN_TOOL_ROOT = GetSetting("SIGN_TOOL_ROOT");
            METADATA_DIR = GetSetting("METADATA_DIR");
            RGSC_DBG_DIR = GetSetting("RGSC_DBG_DIR");
            RGSC_DIR = GetSetting("RGSC_DIR");
            RGSC_DBG_DIR_X64 = GetSetting("RGSC_DBG_DIR_X64");
            RGSC_DIR_X64 = GetSetting("RGSC_DIR_X64");
            RGSC_PROTECTION_ROOT = GetSetting("RGSC_PROTECTION_ROOT");
            PUBLIC_INTERFACE_HEADERS = GetSetting("PUBLIC_INTERFACE_HEADERS");
            DEST_DIR_ROOT = GetSetting("DEST_DIR_ROOT");
            DEST_DIR_SDK = GetSetting("DEST_DIR_SDK");
            DEST_DIR_SDK_SAMPLES = GetSetting("DEST_DIR_SDK_SAMPLES");
            DEST_DIR_SDK_DOCUMENTATION = GetSetting("DEST_DIR_SDK_DOCUMENTATION");
            DEST_DIR_SDK_INCLUDES = GetSetting("DEST_DIR_SDK_INCLUDES");
            DEST_DIR_SDK_GUARDIFACTS = GetSetting("DEST_DIR_SDK_GUARDIFACTS");
            DEST_DIR_SDK_GUARDIFACTS_ZIP = GetSetting("DEST_DIR_SDK_GUARDIFACTS_ZIP");
            DEST_DIR_SDK_GUARDIFACTS32 = GetSetting("DEST_DIR_SDK_GUARDIFACTS32");
            DEST_DIR_SDK_GUARDIFACTS32_ZIP = GetSetting("DEST_DIR_SDK_GUARDIFACTS32_ZIP");
            DEST_DBG_DIR_TEST_APP = GetSetting("DEST_DBG_DIR_TEST_APP");
            DEST_DIR_TEST_APP = GetSetting("DEST_DIR_TEST_APP");
            DEST_DBG_DIR_TEST_APP_64 = GetSetting("DEST_DBG_DIR_TEST_APP_64");
            DEST_DIR_TEST_APP_64 = GetSetting("DEST_DIR_TEST_APP_64");
            DEST_DBG_DIR_RGSC = GetSetting("DEST_DBG_DIR_RGSC");
            DEST_DIR_RGSC = GetSetting("DEST_DIR_RGSC");
            DEST_DBG_DIR_RGSC_X64 = GetSetting("DEST_DBG_DIR_RGSC_X64");
            DEST_DIR_RGSC_X64 = GetSetting("DEST_DIR_RGSC_X64");
            Zip7Path = GetSetting("Zip7Path");
            SDK_ARCHIVES_DIR = GetSetting("SDK_ARCHIVES_DIR");

            // cleanup
            RGSC_CLEANUP_DIR_DEBUG = GetSetting("RGSC_CLEANUP_DIR_DEBUG");
            RGSC_CLEANUP_DIR_RELEASE = GetSetting("RGSC_CLEANUP_DIR_RELEASE");
            RGSC_CLEANUP_DIR_DEBUG_X64 = GetSetting("RGSC_CLEANUP_DIR_DEBUG_X64");
            RGSC_CLEANUP_DIR_RELEASE_X64 = GetSetting("RGSC_CLEANUP_DIR_RELEASE_X64");
            RGSC_CLEANUP_SDF = GetSetting("RGSC_CLEANUP_SDF");

            SIGNING_KEY = GetSetting("SIGNING_KEY");
            SIGNPW = GetSetting("SIGNPW", false);
            SIGNENDPOINT = GetSetting("SIGNENDPOINT");

            P4User = GetSetting("P4User");
            P4Password = GetSetting("P4Password");
            P4SyncPath = GetSetting("P4SyncPath");
            P4SyncPathData = GetSetting("P4SyncPathData");
            P4SDKRoot = GetSetting("P4SDKRoot");
            P4ThirdParty = GetSetting("P4ThirdParty");

            P4.Login();
            Version.Init();

            Program.Report(String.Format("Title Branch\t: {0}", Settings.TitleBranch));
            Program.Report(String.Format("Title Branch\t: {0}", Settings.TitleBranch));
            Program.Report(String.Format("Build Branch\t: {0}", Settings.BuildBranch));
            Program.Report(String.Format("SdkVersion\t\t: {0}", Settings.SdkVersion));
            Program.Report(String.Format("ScuiAssetsPath\t: {0}", Settings.ScuiAssetsPath));

            Program.Report(String.Format("RGSC_RAGE_DIR\t: {0}", Settings.RGSC_RAGE_DIR));
            Program.Report(String.Format("RGSC_TOOLS_ROOT\t: {0}", Settings.RGSC_TOOLS_ROOT));
            Program.Report(String.Format("DESKTOP_DIR\t\t: {0}", Settings.DESKTOP_DIR));

            Program.Report(String.Format("P4 Syncing\t\t: {0}", Settings.EnableP4Sync));
            Program.Report(String.Format("Asset Update:\t: {0}", Settings.EnableAssetUpdate));
            Program.Report(String.Format("Run Builds\t\t: {0}", Settings.EnableBuilds));
            Program.Report(String.Format("Collect Script\t\t: {0}", Settings.EnableCollecting));
            Program.Report(String.Format("Sign Exes \t\t: {0}", Settings.EnableSigning));
            Program.Report(String.Format("Build Docs\t\t: {0}", Settings.EnableDocumentation));
            Program.Report(String.Format("Build Installer\t: {0}", Settings.EnableInstaller));
            Program.Report(String.Format("Protection\t\t: {0}", Settings.EnableProtection));

            Program.Report("Settings load was successful");
        }

        const int SYMBOLIC_LINK_FLAG_DIRECTORY = 1;

        [DllImport("kernel32.dll")]
        static extern bool CreateSymbolicLink(string lpSymlinkFileName, string lpTargetFileName, int dwFlags);

        [DllImport("kernel32.dll")]
        static extern bool RemoveDirectory(string lpPathName);

        public static bool SetupSymLinks(string title, string branch)
        {
            // root path
            string symLinkPath = String.Format(@"x:\{0}\",TitleBranch);
            if (Directory.Exists(symLinkPath) && !RemoveDirectory(symLinkPath))
                return false;

            string newSymLinkPath = String.Format(@"x:\{0}", title);
            if (!CreateSymbolicLink(symLinkPath, newSymLinkPath, SYMBOLIC_LINK_FLAG_DIRECTORY))
                return false;

            // src/dev
            symLinkPath = String.Format(@"X:\{0}\src\{1}", TitleBranch, BuildBranch);
            if (Directory.Exists(symLinkPath) && !RemoveDirectory(symLinkPath))
                return false;

            newSymLinkPath = String.Format(@"x:\{0}\src\{1}", title, branch);
            if (!CreateSymbolicLink(symLinkPath, newSymLinkPath, SYMBOLIC_LINK_FLAG_DIRECTORY))
                return false;

            // build/dev
            symLinkPath = String.Format(@"X:\{0}\titleupdate\{1}", TitleBranch, BuildBranch);
            if (Directory.Exists(symLinkPath) && !RemoveDirectory(symLinkPath))
                return false;

            newSymLinkPath = String.Format(@"x:\{0}\titleupdate\{1}", title, branch);
            if (!CreateSymbolicLink(symLinkPath, newSymLinkPath, SYMBOLIC_LINK_FLAG_DIRECTORY))
                return false;

            return true;
        }
    }
}
