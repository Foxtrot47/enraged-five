﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Social_Club_SDK
{
    public class JobSystem
    {
        enum JobType
        {
            Invalid,
            Sample,
            SetupSymLink,
            ClearFolder,
            CodeClean,
            P4Login,
            Setup,
            P4Sync,
            Version,
            Build,
            SymChk
        }
        
        public static int ExecuteJob(string jobIdentifier)
        {
            JobType t = LookupJob(jobIdentifier);
            return RunJobInternal(t);
        }

        static JobType LookupJob(string jobIdentifier)
        {
            switch(jobIdentifier)
            {
                case "sample":
                    return JobType.Sample;
                 case "setupsymlink":
                    return JobType.SetupSymLink;
                case "clearfolder":
                    return JobType.ClearFolder;
                case "codeclean":
                    return JobType.CodeClean;
                case "p4login":
                    return JobType.P4Login;
                case "setup":
                    return JobType.Setup;
                case "p4sync":
                    return JobType.P4Sync;
                case "version":
                    return JobType.Version;
                case "build":
                    return JobType.Build;
                case "symchk":
                    return JobType.SymChk;
                default:
                    return JobType.Invalid;
            }
        }

        static int RunJobInternal(JobType t)
        {
            Job j = null;

            switch (t)
            {
                case JobType.Sample:
                    j = new SampleJob();
                    break;
                case JobType.SetupSymLink:
                    j = new SymLinkJob();
                    break;
                case JobType.P4Login:
                    j = new P4LoginJob();
                    break;
                case JobType.ClearFolder:
                    j = new ClearFolderJob();
                    break;
                case JobType.CodeClean:
                    j = new CodeCleanJob();
                    break;
                case JobType.Setup:
                    j = new SetupJob();
                    break;
                case JobType.P4Sync:
                    j = new P4SyncJob();
                    break;
                case JobType.Version:
                    j = new VersionJob();
                    break;
                case JobType.Build:
                    j = new BuildJob();
                    break;
                case JobType.SymChk:
                    j = new SymChkJob();
                    break;
                case JobType.Invalid:
                default:
                    return -1;
            }

            int result = j.RunJob();
            return result;
        }
    };

    public abstract class Job
    {
        public abstract int RunJob();
    };

    public class SampleJob : Job
    {
        public override int RunJob()
        {
            Thread.Sleep(1000);
            return 0;
        }
    }

    public class SymLinkJob : Job
    {
        public override int RunJob()
        {
            Settings.Init();
            bool bResult = Settings.SetupSymLinks(Settings.SourceTitle, Settings.SourceBranch);
            return bResult ? 0 : 1;
        }
    }

    public class ClearFolderJob : Job
    {
        public override int RunJob()
        {
            Settings.Init();
            FileHelper.ClearFolder(Settings.DEST_DIR_ROOT, true);

            string installerPath = String.Format(Settings.P4SDKRoot + "Rockstar Games");
            FileHelper.ClearFolder(installerPath, true);
            return 0;
        }
    }

    public class CodeCleanJob : Job
    {
        public override int RunJob()
        {
            Settings.Init();
            CollectScripts.CodeCleanup();

            return 0;
        }
    }

    public class P4LoginJob : Job
    {
        public override int RunJob()
        {
            Settings.Init();
            P4.Login();
            return 0;
        }
    }

    public class SetupJob : Job
    {
        public override int RunJob()
        {
            Settings.Init();
            CollectScripts.Init();
            SetupVerification.Verify();
            return 0;
        }
    }

    public class P4SyncJob : Job
    {
        public override int RunJob()
        {
            Settings.Init();
            P4.SyncPerforce();
            return 0;
        }
    }

    public class VersionJob : Job
    {
        public override int RunJob()
        {
            Program.Results.P4SyncChangelist = P4.GetSyncedChangelist(Settings.P4SyncPath);
            Settings.Init();
            Version.Setup();
            return 0;
        }
    }

    public class BuildJob : Job
    {
        public override int RunJob()
        {
            Program.Results.P4SyncChangelist = P4.GetSyncedChangelist(Settings.P4SyncPath);
            Settings.Init();
            Version.Setup();

            Builder.Build();

            return 0;
        }
    }

    public class SymChkJob : Job
    {
        public override int RunJob()
        {
            Program.Results.P4SyncChangelist = P4.GetSyncedChangelist(Settings.P4SyncPath);
            Settings.Init();
            Version.Setup();

            // Check symbols for SocialClubHelper.exe
            Builder.CheckSymbols("SocialClubHelper.exe", Settings.DEST_DBG_DIR_RGSC);
            Builder.CheckSymbols("SocialClubHelper.exe", Settings.DEST_DIR_RGSC);
            Builder.CheckSymbols("SocialClubHelper.exe", Settings.DEST_DBG_DIR_RGSC_X64);
            Builder.CheckSymbols("SocialClubHelper.exe", Settings.DEST_DIR_RGSC_X64);

            // Check symbols for socialclub.dll
            Builder.CheckSymbols("socialclub.dll", Settings.DEST_DBG_DIR_RGSC);
            Builder.CheckSymbols("socialclub.dll", Settings.DEST_DIR_RGSC);
            Builder.CheckSymbols("socialclub.dll", Settings.DEST_DBG_DIR_RGSC_X64);
            Builder.CheckSymbols("socialclub.dll", Settings.DEST_DIR_RGSC_X64);

            return 0;
        }
    }

    /*
     *                  Settings.Clear();
                        Results.Clear();

                        Settings.Init();
                        Settings.SetupSymLinks(Settings.SourceTitle, Settings.SourceBranch);
                        FileHelper.ClearFolder(Settings.DEST_DIR_ROOT, true);
                        P4.Login();
                        CollectScripts.Init();
                        P4.SyncPerforce();
                        SetupVerification.Verify();
                        Version.Setup();
                        Builder.Build();

                        Results.Report(String.Format("Build successful, next auto build in {0} hours", SleepTime / 1000 / 60 / 60));

                        SendEmailResult(true);
     */
}
