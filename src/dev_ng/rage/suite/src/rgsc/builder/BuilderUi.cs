﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Social_Club_SDK
{
    public partial class BuilderUi : Form
    {
        public static BuilderUi Instance;

        public BuilderUi()
        {
            if (Instance == null)
            {
                Instance = this;
            }

            InitializeComponent();

            Settings.Init();

            SDKVersion.Text = Settings.SdkVersion;
            SettingsP4SyncBox.Checked = Settings.EnableP4Sync;
            SettingsAssetBox.Checked = Settings.EnableAssetUpdate;
            SettingsBuildBox.Checked = Settings.EnableBuilds;
            SettingsCollectionBox.Checked = Settings.EnableCollecting;
            SettingsSigningBox.Checked = Settings.EnableSigning;
            SettingsDocumentationBox.Checked = Settings.EnableDocumentation;
            SettingsInstallerBox.Checked = Settings.EnableInstaller;
            SettingsProtectionBox.Checked = Settings.EnableProtection;
            SettingsZip7Box.Checked = Settings.EnableZip7;
            TitleComboBox.SelectedIndex = 0;
            BranchComboBox.SelectedIndex = 0;
        }

        public void SetProgress(int progress)
        {
            OverallProgressBar.Value = progress;
        }

        public void OutputText(string text)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<string>(OutputText), new object[] { text });
                return;
            }

            OutputResults.AppendText(text);
            OutputResults.AppendText(Environment.NewLine);
            OutputResults.SelectionStart = BuilderUi.Instance.OutputResults.Text.Length;
            OutputResults.ScrollToCaret();
        }

        public void ErrorText(string text)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<string>(ErrorText), new object[] { text });
                return;
            }

            OutputResults.SelectionColor = Color.Red;
            OutputResults.AppendText(text);
            OutputResults.AppendText(Environment.NewLine);
            OutputResults.SelectionColor = Color.Black;
            OutputResults.SelectionStart = BuilderUi.Instance.OutputResults.Text.Length;
            OutputResults.ScrollToCaret();
        }

        private void BuildButton_Click(object sender, EventArgs e)
        {
            string text = SDKVersion.Text;
            Match eMatch = Regex.Match(text, "[0-9][.][0-9][.][0-9][.][0-9]");
            if (!eMatch.Success || text == "000.000.000.000")
            {
                SDKVersion.Text = "000.000.000.000";
                SDKVersion.Focus();
                return;
            }

            BuildButton.Enabled = false;

            try
            {
                string symLinkTitle = TitleComboBox.SelectedItem.ToString();
                string symLinkBranch = BranchComboBox.SelectedItem.ToString();
                Settings.SetupSymLinks(symLinkTitle, symLinkBranch);

                P4.Login();
                Version.Init();
                CollectScripts.Init();
                Builder.CheckVersionHistory();

                Settings.SdkVersion = SDKVersion.Text;
                Settings.EnableP4Sync = SettingsP4SyncBox.Checked;
                Settings.EnableAssetUpdate = SettingsAssetBox.Checked;
                Settings.EnableBuilds = SettingsBuildBox.Checked;
                Settings.EnableCollecting = SettingsCollectionBox.Checked;
                Settings.EnableSigning = SettingsSigningBox.Checked;
                Settings.EnableDocumentation = SettingsDocumentationBox.Checked;
                Settings.EnableInstaller = SettingsInstallerBox.Checked;
                Settings.EnableProtection = SettingsProtectionBox.Checked;
                Settings.EnableZip7 = SettingsZip7Box.Checked;

                foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcessesByName("explorer"))
                {
                    if (p.MainWindowTitle.ToLower().Contains("rockstar games"))
                        p.Kill();
                    else if (p.MainWindowTitle.ToLower().Contains("social club"))
                        p.Kill();
                }

                SetupVerification.Verify();

                Program.SetProgress(0);
                P4.SyncPerforce();
                Program.SetProgress(10);
                SetupVerification.Verify();
                Program.SetProgress(15);
                Version.Setup();
                Program.SetProgress(30);
                Builder.Build();

                MessageBox.Show("Build Completed");
                Program.OutputHeader("Completed");
            }
            catch(Exception ex)
            {
                Program.Error(ex.Message);
            }

            BuildButton.Enabled = true;
        }

        private void SDKVersion_Leave(object sender, EventArgs e)
        {
            string text = SDKVersion.Text;
            Match eMatch = Regex.Match(text, "[0-9][.][0-9][.][0-9][.][0-9]");
            if (!eMatch.Success)
            {
                SDKVersion.Text = "000.000.000.000";
            }
        }

        private void TitleComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            BranchComboBox.Items.Clear();
            if (TitleComboBox.SelectedItem.ToString() == "gta5")
            {
                BranchComboBox.Items.Add("dev_ng");
                BranchComboBox.Items.Add("dev_temp");
                BranchComboBox.SelectedIndex = 0;
            }
            else if (TitleComboBox.SelectedItem.ToString() == "rdr3")
            {
                BranchComboBox.Items.Add("dev");
                BranchComboBox.Items.Add("dev_migrate");
                BranchComboBox.SelectedIndex = 0;
            }
            else
            {
                BranchComboBox.Items.Add("unknown");
                BranchComboBox.SelectedIndex = 0;
            }
        }
    }
}
