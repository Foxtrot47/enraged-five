﻿namespace Social_Club_SDK
{
    partial class BuilderUi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SDKVersion = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SettingsSigningBox = new System.Windows.Forms.CheckBox();
            this.SettingsZip7Box = new System.Windows.Forms.CheckBox();
            this.SettingsProtectionBox = new System.Windows.Forms.CheckBox();
            this.SettingsInstallerBox = new System.Windows.Forms.CheckBox();
            this.SettingsDocumentationBox = new System.Windows.Forms.CheckBox();
            this.SettingsCollectionBox = new System.Windows.Forms.CheckBox();
            this.SettingsBuildBox = new System.Windows.Forms.CheckBox();
            this.SettingsAssetBox = new System.Windows.Forms.CheckBox();
            this.SettingsP4SyncBox = new System.Windows.Forms.CheckBox();
            this.OverallProgressBar = new System.Windows.Forms.ProgressBar();
            this.BuildButton = new System.Windows.Forms.Button();
            this.OutputResults = new System.Windows.Forms.RichTextBox();
            this.TitleComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BranchComboBox = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SDKVersion
            // 
            this.SDKVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SDKVersion.Location = new System.Drawing.Point(92, 12);
            this.SDKVersion.Name = "SDKVersion";
            this.SDKVersion.Size = new System.Drawing.Size(122, 24);
            this.SDKVersion.TabIndex = 1;
            this.SDKVersion.Text = "000.000.000.000";
            this.SDKVersion.Leave += new System.EventHandler(this.SDKVersion_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "SDK Version:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.SettingsSigningBox);
            this.panel1.Controls.Add(this.SettingsZip7Box);
            this.panel1.Controls.Add(this.SettingsProtectionBox);
            this.panel1.Controls.Add(this.SettingsInstallerBox);
            this.panel1.Controls.Add(this.SettingsDocumentationBox);
            this.panel1.Controls.Add(this.SettingsCollectionBox);
            this.panel1.Controls.Add(this.SettingsBuildBox);
            this.panel1.Controls.Add(this.SettingsAssetBox);
            this.panel1.Controls.Add(this.SettingsP4SyncBox);
            this.panel1.Location = new System.Drawing.Point(15, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(552, 77);
            this.panel1.TabIndex = 3;
            // 
            // SettingsSigningBox
            // 
            this.SettingsSigningBox.AutoSize = true;
            this.SettingsSigningBox.Location = new System.Drawing.Point(159, 27);
            this.SettingsSigningBox.Name = "SettingsSigningBox";
            this.SettingsSigningBox.Size = new System.Drawing.Size(87, 17);
            this.SettingsSigningBox.TabIndex = 8;
            this.SettingsSigningBox.Text = "Sign Binaries";
            this.SettingsSigningBox.UseVisualStyleBackColor = true;
            // 
            // SettingsZip7Box
            // 
            this.SettingsZip7Box.AutoSize = true;
            this.SettingsZip7Box.Location = new System.Drawing.Point(309, 50);
            this.SettingsZip7Box.Name = "SettingsZip7Box";
            this.SettingsZip7Box.Size = new System.Drawing.Size(101, 17);
            this.SettingsZip7Box.TabIndex = 7;
            this.SettingsZip7Box.Text = "Create Archives";
            this.SettingsZip7Box.UseVisualStyleBackColor = true;
            // 
            // SettingsProtectionBox
            // 
            this.SettingsProtectionBox.AutoSize = true;
            this.SettingsProtectionBox.Location = new System.Drawing.Point(309, 27);
            this.SettingsProtectionBox.Name = "SettingsProtectionBox";
            this.SettingsProtectionBox.Size = new System.Drawing.Size(108, 17);
            this.SettingsProtectionBox.TabIndex = 6;
            this.SettingsProtectionBox.Text = "Apply Protections";
            this.SettingsProtectionBox.UseVisualStyleBackColor = true;
            // 
            // SettingsInstallerBox
            // 
            this.SettingsInstallerBox.AutoSize = true;
            this.SettingsInstallerBox.Location = new System.Drawing.Point(309, 4);
            this.SettingsInstallerBox.Name = "SettingsInstallerBox";
            this.SettingsInstallerBox.Size = new System.Drawing.Size(93, 17);
            this.SettingsInstallerBox.TabIndex = 5;
            this.SettingsInstallerBox.Text = "Build Installers";
            this.SettingsInstallerBox.UseVisualStyleBackColor = true;
            // 
            // SettingsDocumentationBox
            // 
            this.SettingsDocumentationBox.AutoSize = true;
            this.SettingsDocumentationBox.Location = new System.Drawing.Point(159, 50);
            this.SettingsDocumentationBox.Name = "SettingsDocumentationBox";
            this.SettingsDocumentationBox.Size = new System.Drawing.Size(124, 17);
            this.SettingsDocumentationBox.TabIndex = 4;
            this.SettingsDocumentationBox.Text = "Build Documentation";
            this.SettingsDocumentationBox.UseVisualStyleBackColor = true;
            // 
            // SettingsCollectionBox
            // 
            this.SettingsCollectionBox.AutoSize = true;
            this.SettingsCollectionBox.Location = new System.Drawing.Point(159, 4);
            this.SettingsCollectionBox.Name = "SettingsCollectionBox";
            this.SettingsCollectionBox.Size = new System.Drawing.Size(116, 17);
            this.SettingsCollectionBox.TabIndex = 3;
            this.SettingsCollectionBox.Text = "Run Collect Scripts";
            this.SettingsCollectionBox.UseVisualStyleBackColor = true;
            // 
            // SettingsBuildBox
            // 
            this.SettingsBuildBox.AutoSize = true;
            this.SettingsBuildBox.Location = new System.Drawing.Point(4, 50);
            this.SettingsBuildBox.Name = "SettingsBuildBox";
            this.SettingsBuildBox.Size = new System.Drawing.Size(79, 17);
            this.SettingsBuildBox.TabIndex = 2;
            this.SettingsBuildBox.Text = "Build SDKs";
            this.SettingsBuildBox.UseVisualStyleBackColor = true;
            // 
            // SettingsAssetBox
            // 
            this.SettingsAssetBox.AutoSize = true;
            this.SettingsAssetBox.Location = new System.Drawing.Point(4, 27);
            this.SettingsAssetBox.Name = "SettingsAssetBox";
            this.SettingsAssetBox.Size = new System.Drawing.Size(123, 17);
            this.SettingsAssetBox.TabIndex = 1;
            this.SettingsAssetBox.Text = "Update SCUI Assets";
            this.SettingsAssetBox.UseVisualStyleBackColor = true;
            // 
            // SettingsP4SyncBox
            // 
            this.SettingsP4SyncBox.AutoSize = true;
            this.SettingsP4SyncBox.Location = new System.Drawing.Point(4, 4);
            this.SettingsP4SyncBox.Name = "SettingsP4SyncBox";
            this.SettingsP4SyncBox.Size = new System.Drawing.Size(93, 17);
            this.SettingsP4SyncBox.TabIndex = 0;
            this.SettingsP4SyncBox.Text = "Sync Perforce";
            this.SettingsP4SyncBox.UseVisualStyleBackColor = true;
            // 
            // OverallProgressBar
            // 
            this.OverallProgressBar.Location = new System.Drawing.Point(15, 359);
            this.OverallProgressBar.Name = "OverallProgressBar";
            this.OverallProgressBar.Size = new System.Drawing.Size(474, 23);
            this.OverallProgressBar.TabIndex = 4;
            // 
            // BuildButton
            // 
            this.BuildButton.Location = new System.Drawing.Point(495, 359);
            this.BuildButton.Name = "BuildButton";
            this.BuildButton.Size = new System.Drawing.Size(75, 23);
            this.BuildButton.TabIndex = 5;
            this.BuildButton.Text = "Build";
            this.BuildButton.UseVisualStyleBackColor = true;
            this.BuildButton.Click += new System.EventHandler(this.BuildButton_Click);
            // 
            // OutputResults
            // 
            this.OutputResults.Location = new System.Drawing.Point(15, 127);
            this.OutputResults.Name = "OutputResults";
            this.OutputResults.Size = new System.Drawing.Size(552, 226);
            this.OutputResults.TabIndex = 6;
            this.OutputResults.Text = "";
            // 
            // TitleComboBox
            // 
            this.TitleComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleComboBox.FormattingEnabled = true;
            this.TitleComboBox.Items.AddRange(new object[] {
            "gta5",
            "rdr3"});
            this.TitleComboBox.Location = new System.Drawing.Point(262, 12);
            this.TitleComboBox.Name = "TitleComboBox";
            this.TitleComboBox.Size = new System.Drawing.Size(121, 26);
            this.TitleComboBox.TabIndex = 7;
            this.TitleComboBox.SelectedValueChanged += new System.EventHandler(this.TitleComboBox_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(231, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Title:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(399, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Branch:";
            // 
            // BranchComboBox
            // 
            this.BranchComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BranchComboBox.FormattingEnabled = true;
            this.BranchComboBox.Items.AddRange(new object[] {
            "dev",
            "dev_ng",
            "dev_temp"});
            this.BranchComboBox.Location = new System.Drawing.Point(449, 12);
            this.BranchComboBox.Name = "BranchComboBox";
            this.BranchComboBox.Size = new System.Drawing.Size(121, 26);
            this.BranchComboBox.TabIndex = 10;
            // 
            // BuilderUi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 394);
            this.Controls.Add(this.BranchComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TitleComboBox);
            this.Controls.Add(this.OutputResults);
            this.Controls.Add(this.BuildButton);
            this.Controls.Add(this.OverallProgressBar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SDKVersion);
            this.Name = "BuilderUi";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox SDKVersion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox SettingsP4SyncBox;
        private System.Windows.Forms.CheckBox SettingsZip7Box;
        private System.Windows.Forms.CheckBox SettingsProtectionBox;
        private System.Windows.Forms.CheckBox SettingsInstallerBox;
        private System.Windows.Forms.CheckBox SettingsDocumentationBox;
        private System.Windows.Forms.CheckBox SettingsCollectionBox;
        private System.Windows.Forms.CheckBox SettingsBuildBox;
        private System.Windows.Forms.CheckBox SettingsAssetBox;
        private System.Windows.Forms.ProgressBar OverallProgressBar;
        private System.Windows.Forms.Button BuildButton;
        private System.Windows.Forms.CheckBox SettingsSigningBox;
        public System.Windows.Forms.RichTextBox OutputResults;
        private System.Windows.Forms.ComboBox TitleComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox BranchComboBox;


    }
}

