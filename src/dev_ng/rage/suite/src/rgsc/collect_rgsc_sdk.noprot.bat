@echo off

set RS_PROJECT=gta5
set RS_PROJROOT=x:\gta5
set RS_CODEBRANCH=x:\gta5\src\dev_ng
set RS_BUILDBRANCH=x:\gta5\titleupdate\dev_ng
set RS_TOOLSROOT=x:\gta5\tools_ng
set RAGE_DIR=%RS_CODEBRANCH%\rage
set RUBYLIB=%RS_TOOLSROOT%\lib
set PATH=%RS_TOOLSROOT%\bin;%RS_TOOLSROOT%\bin\ruby\bin;%PATH%
call "%RS_TOOLSROOT%\bin\setenv.bat"

set RGSC_DIR_X64=%ProgramW6432%\Rockstar Games\Social Club
set RGSC_TOOLS_ROOT=%RS_TOOLSROOT%
set RGSC_PROTECTION_ROOT=%RGSC_TOOLS_ROOT%\script\coding\protection

REM Running RTTI Script; this will copy the original binary to socialclub64.unpatchedstrings.dll
ruby %RGSC_PROTECTION_ROOT%\rttihandling\parseForStrings.rb --path "%RGSC_DIR_X64%\socialclub.dll"

pause
REM Running GuardIT
ruby %RGSC_PROTECTION_ROOT%\ProtectIT.rb -t -r release -g %RGSC_PROTECTION_ROOT%\rgsc_nodebug.gsml -f

pause

