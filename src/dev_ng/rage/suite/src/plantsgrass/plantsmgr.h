//
// plantsgrass/plantsmgr.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef PLANTSGRASS_PLANTSMGR_H
#define PLANTSGRASS_PLANTSMGR_H

#include "atl/bitset.h"
#include "grcore/effect_config.h"
#include "phcore/materialmgr.h"
#include "phbound/boundgeom.h"
#include "physics/inst.h"
#include "system/memops.h"
#include "vector/vector4.h"
#include "vector/matrix34.h"

// if 0, then disables loading of models&textures and Render()
// Update() still works (as its results are required for procedural objects)
#define PNG_PLANTMGR_ENABLED					(1)
// PSN: if 1, then SPU renderer is used:
#define PNG_PLANTSMGR_USE_SPU_RENDERING			(0 && (__PS3) /*&& !SPU_GCM_FIFO*/)
// max poly points in phPolygon (taken from phBound, etc.):
#define PNG_PLANT_MAX_POLY_POINTS				(4)
// number of grass GroupModels with "big" textures:
// each group consist of 4 models + 1 "big" texture;
#define PNG_PLANT_NUM_PLANT_SLOTS				(4)
// num of models per slot:
#define PNG_PLANT_SLOT_NUM_MODELS				(4)
// num of textures per slot:
#define PNG_PLANT_SLOT_NUM_TEXTURES				(8)
// radius of test sphere used to detect Instances located close enough:
#define PNG_PLANTMGR_COL_TEST_RADIUS			(80.0f)			//(60.0f)
#define PNG_PLANTMGR_COL_TEST_RADIUS_SQR		(PNG_PLANTMGR_COL_TEST_RADIUS*PNG_PLANTMGR_COL_TEST_RADIUS)
// distance where TriLocs are considered to be used as base for plant sectors:
#define PNG_PLANT_TRILOC_FAR_DIST				(85.0f)
#define PNG_PLANT_TRILOC_FAR_DIST_SQR			(PNG_PLANT_TRILOC_FAR_DIST*PNG_PLANT_TRILOC_FAR_DIST)
// LOD0:
#define PNG_PLANT_LOD0_ALPHA_CLOSE_DIST			(22.0f)		// LOD0: fade start range
#define PNG_PLANT_LOD0_ALPHA_FAR_DIST			(32.0f)		// LOD0: fade stop range
#define PNG_PLANT_LOD0_FAR_DIST					(32.0f)		// LOD0: where LOD0 is culled
// LOD1:
#define PNG_PLANT_LOD1_CLOSE_DIST				(15.0f)		// LOD1: where LOD1 starts
#define PNG_PLANT_LOD1_ALPHA0_CLOSE_DIST		(15.0f)		// LOD1: alpha0 fade start
#define PNG_PLANT_LOD1_ALPHA0_FAR_DIST			(25.0f)		// LOD1: alpha0 fade stop
#define PNG_PLANT_LOD1_ALPHA1_CLOSE_DIST		(40.0f)		// LOD1: alpha1 fade start
#define PNG_PLANT_LOD1_ALPHA1_FAR_DIST			(55.0f)		// LOD1: alpha1 fade stop
#define PNG_PLANT_LOD1_FAR_DIST					(55.0f)		// LOD1: where LOD1 is culled
// LOD2:
#define PNG_PLANT_LOD2_CLOSE_DIST				(30.0f)		// LOD1: where LOD1 starts
#define PNG_PLANT_LOD2_ALPHA0_CLOSE_DIST		(30.0f)		// LOD1: alpha0 fade start
#define PNG_PLANT_LOD2_ALPHA0_FAR_DIST			(45.0f)		// LOD1: alpha0 fade stop
#define PNG_PLANT_LOD2_ALPHA1_CLOSE_DIST		(55.0f)		// LOD1: alpha1 fade start
#define PNG_PLANT_LOD2_ALPHA1_FAR_DIST			(85.0f)		// LOD1: alpha1 fade stop
#define PNG_PLANT_LOD2_FAR_DIST					(85.0f)		// LOD1: where LOD1 is culled
// number of entities hold in our internal CEntity cache:
#define PNG_PLANT_COL_ENTITY_CACHE_SIZE			(32+8)
// update cache every 32th frame (must be power-of-2):
#define PNG_PLANT_COL_ENTITY_UPDATE_CACHE		(32)
// max number of triangles kept in loc triangle cache:
#define PNG_PLANT_MAX_CACHE_LOC_TRIS_NUM		(512)
// process every 8th TriLoc (must be power-of-2):
#define PNG_PLANT_ENTRY_TRILOC_PROCESS_UPDATE	(8)
// unique mask value to mark case, when we want all LocTris to be processed:
#define PNG_PLANT_ENTRY_TRILOC_PROCESS_ALWAYS	(0x7A7A7A7A)	// changed as it is compared with a signed value
// per-vertex scale:
// <+0; 5> - scales XY+Z
// <-5; 0) - scales Z only
#define MIN_PV_SCALE							(-5.0f)
#define MAX_PV_SCALE							(5.0f)

#if PNG_PLANTSMGR_USE_SPU_RENDERING
#include "plantsgrass/plantsgrassrendererspu.h"
#endif // PNG_PLANTSMGR_USE_SPU_RENDERING

namespace rage
{

const u8 PV_SCALE_ONE = u8(((1.0f - MIN_PV_SCALE) / (MAX_PV_SCALE - MIN_PV_SCALE)) * 255.0f); // scale=1.0f as encoded u8
#if !__FINAL
extern u8 gbPlantMgrActive;	// JB : I need to be able to turn the plantmgr off when exporting navmesh data
#endif // __BANK

class Color32;
class Vector2;
class Vector3;
class grcTexture;
class grmGeometry;
class Matrix34;
class phBound;
class phBoundGeometry;
class phInst;
class phPolygon;
class rmcDrawable;

struct pngGrassModel
{
	pngGrassModel() { sysMemSet(this, 0x00, sizeof(pngGrassModel)); }

	rmcDrawable		*m_pDrawable;		// higher level drawable, used only for memory management
	grmGeometry		*m_pGeometry;		// plant geometry
	Vector4			m_BoundingSphere;	// model bound sphere
	Vector2			m_dimensionLOD2;	// dimensions (w&h) of LOD2 (stored in LOD1)

#if PNG_PLANTSMGR_USE_SPU_RENDERING
	u32				m_IdxOffset;
	u16				m_IdxLocation;		// LOCATION_LOCAL, LOCATION_MAIN
	u16				m_IdxCount;
	u16				m_DrawMode;			// CELL_GCM_PRIMITIVE_TRIANGLES
	u16				m_pad0;
	u32				m_pad1;
	u32				m_GeometryCmd0[PNG_PLANTSMGR_LOCAL_GEOMETRY_MAXCMDSIZE];
#endif // PNG_PLANTSMGR_USE_SPU_RENDERING
};

// "Plant Location" triangle:
// it's generated from ColModel data;
class pngPlantLocTri
{
	friend class pngPlantMgr;
public:
	pngPlantLocTri*	Add(const Vector3& v1, const Vector3& v2, const Vector3& v3, phMaterialMgr::Id nSurfaceType, u8 nLighting, bool bCreatesPlants, bool bCreatesObjects/*, CEntity* pParentEntity*/);
	void Release();

	inline static u8 pvPackScale(float scale);
	inline static float pvUnpackScale(u8 scale8);

	Vector3				m_V1, m_V2, m_V3;								// 3 vertices of triangle
	Vector3				m_Center;
	float				m_SphereRadius;									// radius of the sphere containing triangle
	u32					m_Seed;											// constant seed value for the triangle
	DEV_ONLY(u32		m_nMaxNumPlants;)								// debug info only: max number of plants to generate for this TriLoc
	float				m_triArea;										// area of LocTri (together with density used to compute no of drawn plants)
	// local per-vertex variation data: ground color(RGB)+scale(A):
	Color32				m_GroundColorV1, m_GroundColorV2, m_GroundColorV3;
	phMaterialMgr::Id	m_nSurfaceType;									// copied from CColTriangle;
//	CEntity				*m_pParentEntity;
	u8					m_nLighting;									// lighting (calculated from CColTriangle);
	bool				m_bCreatesPlants	: 1;
	bool				m_bCreatesObjects	: 1;
	bool				m_bCreatedObjects	: 1;
	u8					m_pad0				: 5;
private:
	u16					m_NextTri;
	u16					m_PrevTri;
};

// 1 collsion bound entry in internal "entity" cache:
class pngPlantColBoundEntry
{
	friend class pngPlantMgr;
public:
	pngPlantColBoundEntry*	AddEntry(phInst* physicsInst);
	void					ReleaseEntry();
	phInst*					GetPhysInst();
	const Matrix34*			GetBoundMatrix();
	phBoundGeometry*		GetBound();

	Matrix34				m_BoundMat;			// local copy of above matrix (sometimes phInst, which matrix we use, doesn't exist after some time)
	bool					m_bBoundMatIdentity;// true if BoundMat is identity matrix (no transformation required for LocTris then)
	phInst*					m_pPhysicsInst;
	atBitSet				m_BoundMatProps;	// bitfield to store bCreatesPlants|bCreatesObjects material flags
	u16*					m_LocTriArray;		// array of size [phBound->GetNumPolygons()],
												// it holds pointers to pngPlantLocTri for every phPolygon in the phBound;
	u16						m_nNumTris;			// size of above array
	u16						m_nScancode;		// scancode (timestamp) of current frame
private:
	u16						m_NextEntry;
	u16						m_PrevEntry;
	
};

#if !__SPU
inline phInst* pngPlantColBoundEntry::GetPhysInst()
{ 
	return m_pPhysicsInst;
}

inline const Matrix34* pngPlantColBoundEntry::GetBoundMatrix()	
{
	return GetPhysInst() ? &RCC_MATRIX34(GetPhysInst()->GetMatrix()) : 0;
}

inline phBoundGeometry* pngPlantColBoundEntry::GetBound()
{
	phArchetype* archetype = m_pPhysicsInst->GetArchetype();
	phBound* bound = archetype->GetBound();
	if (bound->GetType() == phBound::GEOMETRY)
	{
		return static_cast<phBoundGeometry*>(static_cast<phBoundPolyhedron*>(bound));
	}
	else
	{
		return NULL;
	}
}
#endif // !__SPU

class pngPlantMgrBase
{
	// this is the data required by the SPU update process
public:
	u16							m_UnusedColEntListHead;
	u16							m_CloseColEntListHead;
	u16							m_UnusedLocTriListHead;
	u16							m_CloseLocTriListHead[PNG_PLANT_NUM_PLANT_SLOTS];	// each grass GroupModel has its own TriLoc close list;
	pngPlantColBoundEntry		m_ColEntCacheTab[PNG_PLANT_COL_ENTITY_CACHE_SIZE];
	pngPlantLocTri				m_LocTrisTab[PNG_PLANT_MAX_CACHE_LOC_TRIS_NUM];
	Vector3						m_CameraPos;
};

struct pngProcObjectCreationInfo;
//struct ProcTriRemovalInfo;

class CPlantsMgrUpdateSPU
{
public:
	pngPlantMgrBase*			m_pPlantsMgr;
	Vector3						m_camPos;
	u32							m_iTriProcessSkipMask;
	Color32						m_defaultGroundColor;
	u32							m_maxAdd;
	u32							m_addBufSize;
	pngProcObjectCreationInfo*	m_pAddList;
	u32							m_maxRemove;
	u32							m_removeBufSize;
	struct ResultSize
	{
		u32						m_numAdd;
		u32						m_numRemove;
	} ;
	ResultSize*					m_pResultSize;
	pngPlantLocTri*				m_LocTrisRenderTab;

};

class pngPlantMgr : public pngPlantMgrBase
{
	friend class pngPlantLocTri;
	friend class pngPlantColBoundEntry;
public:
	pngPlantMgr();

public:
	static bool	Initialise();
	static void	Shutdown();
	static void	ShutdownLevel();

	bool		ReloadConfig();

#if __BANK
	static bool	AddBankWidgets(bkBank& bank);
#endif

public:
//	bool		UpdateBegin(const Vector3& camPos);
	bool		UpdateBegin(const Vector3& cameraPos, const Vector3& vecPlayerPos, const Matrix34& mat, const Vector3& bboxmin, const Vector3& bboxmax, float groundZ);
	void		UpdateEnd();
	void		UpdateTask();
	void		UpdateRenderBuffer();
	static bool	PreUpdateOnceForNewCameraPos(const Vector3& newCamPos);
	static bool	Render();
	bool		RenderInternal();
#if __DEV
	bool		RenderDebugStuff();
	// Statistics Functions for Performance Stats (DHM 23 Jan 2008)
	// These are similar to the other debug functions but available in Release Build
	void		DbgGetCPlantMgrInfo( u32* nNumLocTrisDrawn, u32* nNumPlantsDrawn );
	void		DbgStatsCountCachedBounds( u32* pCountAll );
	void		DbgStatsCountLocTrisAndPlants( u32 groupID, u32* pCountLocTris, u32* pCountPlants );
#endif // __DEV

	// helper functions to pick the right buffer:
	u32			GetUpdateRTBufferID() const { return m_RTbufferID; }
	u32			GetRenderRTBufferID() const { return (m_RTbufferID + 1) & 1; }

private:
	bool					InitialiseInternal();
	void					ShutdownInternal();
	bool					PreUpdateOnceForNewCameraPosInternal(const Vector3& newCameraPos);
	void					AdvanceCurrentScanCode() { m_scanCode++; }
	u16						GetCurrentScanCode() { return(m_scanCode); }
	float					CalculateWindBending();
	bool					_ColBoundCache_Update(const Vector3& cameraPos, bool bQuickUpdate=false);
	bool					_ColBoundCache_ProcessBound(phInst *pInst, phBound *pBound, const u16 nCurrentScanCode);
	pngPlantColBoundEntry*	_ColBoundCache_FindInCache(phInst* physicsInst);
	pngPlantColBoundEntry*	_ColBoundCache_Add(phInst* physicsInst, bool bCheckCacheFirst=false);
	void					_ColBoundCache_Remove(phInst* physicsInst);
	bool					IsBoundGeomPlantFriendly(phBoundGeometry *pBound);
	bool					UpdateAllLocTris(const Vector3& camPos, int iTriProcessSkipMask);
	bool					_ProcessEntryCollisionData(pngPlantColBoundEntry *pEntry, const Vector3& camPos, int iTriProcessSkipMask);
	void					_ProcessEntryCollisionData_GenerateNewLocTri(int indexLocTriArray, pngPlantColBoundEntry *pEntry, phBoundGeometry *pBound, const Vector3& camPos,
									const phPolygon& poly, const int polyIndex0, const int polyIndex1, const int polyIndex2,
									const bool bCreatesPlants, const bool bCreatesObjects, phMaterialMgr::Id actualMaterialID);
	bool					DbgPrintCPlantMgrInfo(u32 *nNumLocTrisDrawn, u32 *nNumPlantsLOD0Drawn, u32 *nNumPlantsLOD1Drawn, u32 *nNumPlantsLOD2Drawn);
	bool					DbgCountCachedBounds(u32 *pCountAll);
	bool					DbgCountLocTrisAndPlants(u32 groupID, u32 *pCountLocTris, u32 *pCountPlants);
	void					DbgDrawTriangleArrays(atArray<Vector3> &triangleVertices, atArray<Color32>& triangleColors);
	pngPlantColBoundEntry*	MoveColEntToList(u16*ppCurrentList, u16*ppNewList, pngPlantColBoundEntry *pEntry);
	pngPlantLocTri*			MoveLocTriToList(u16*ppCurrentList, u16*ppNewList, pngPlantLocTri *pTri);
	void					SwapRTBuffer()				{ m_RTbufferID++; m_RTbufferID&=0x01;	}
	bool					LoadPlantModel(const char* filename, pngGrassModel *pGrassModel);
	void					UnloadPlantModel(pngGrassModel *model);
	bool					LoadPlantModelLOD2(const char* filename, pngGrassModel *pGrassModel);
	bool					CreateGeometryLOD2();

	// RenderThread stuff: Render() is callled from RT and has to have all necessary data double-buffered:
	u16						m_CloseLocTriListRenderHead[2][PNG_PLANT_NUM_PLANT_SLOTS];
	pngPlantLocTri			m_LocTrisRenderTab[2][PNG_PLANT_MAX_CACHE_LOC_TRIS_NUM];

	u32						m_RTbufferID;
	u16						m_scanCode;
#if PNG_PLANTMGR_ENABLED
	grcTexture*				m_PlantTextureTab0[PNG_PLANT_SLOT_NUM_TEXTURES*2];		// LOD0 texture + LOD1 texture
	grcTexture*				m_PlantTextureTab1[PNG_PLANT_SLOT_NUM_TEXTURES*2];
	grcTexture*				m_PlantTextureTab2[PNG_PLANT_SLOT_NUM_TEXTURES*2];
	grcTexture*				m_PlantTextureTab3[PNG_PLANT_SLOT_NUM_TEXTURES*2];

	grcTexture**			m_PlantSlotTextureTab[PNG_PLANT_NUM_PLANT_SLOTS];
	pngGrassModel			m_PlantModelsTab0[PNG_PLANT_SLOT_NUM_MODELS*2] ALIGNED(128);	// LOD0 geometry + LOD1 geometry
	pngGrassModel			m_PlantModelsTab1[PNG_PLANT_SLOT_NUM_MODELS*2] ALIGNED(128);
	pngGrassModel			m_PlantModelsTab2[PNG_PLANT_SLOT_NUM_MODELS*2]	ALIGNED(128);
	pngGrassModel			m_PlantModelsTab3[PNG_PLANT_SLOT_NUM_MODELS*2] ALIGNED(128);
	pngGrassModel*			m_PlantModelSlotTab[PNG_PLANT_NUM_PLANT_SLOTS];
#endif // PNG_PLANTMGR_ENABLED

#if __DEV
	float				m_fdevTriLocFarDistSqr;
	float				m_fdevAlphaCloseDist;
	float				m_fdevAlphaFarDist;
#endif // __DEV
};

extern pngPlantMgr gPlantMgr;

// packs float scale <0.0f; 5.0f> into byte <0; 255>
inline u8 pngPlantLocTri::pvPackScale(float scale)
{
	const float s0 = Min(scale,	MAX_PV_SCALE);
	const float s1 = Max(s0,	MIN_PV_SCALE);
	const float s2 = (s1-MIN_PV_SCALE) * (1.0f/(MAX_PV_SCALE-MIN_PV_SCALE));
	return( u8(s2 * 255.0f) );
}

// unpacks scale as u8 into float:
inline float pngPlantLocTri::pvUnpackScale(u8 scale8)
{
	const float s0 = float(scale8)*(1.0f/255.0f);
	const float s1 = (s0 * (MAX_PV_SCALE-MIN_PV_SCALE)) + MIN_PV_SCALE;
	return( s1 );
}

} // namespace rage

#endif // PLANTSGRASS_PLANTSMGR_H
