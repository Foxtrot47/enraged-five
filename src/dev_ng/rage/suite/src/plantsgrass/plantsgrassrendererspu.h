//
// plantsgrass/plantsgrassrendererspu.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef __PLANTSGRASSRENDERERSPU_H__
#define __PLANTSGRASSRENDERERSPU_H__

// Tasks should only depend on taskheader.h, not task.h (which is the dispatching system)
#include "system/taskheader.h"
#include "Debug/debug.h"
#include "Utils/MemoryMgr.h"

DECLARE_TASK_INTERFACE(PlantsGrassRendererSPU);
void spuDrawTriPlants(::rage::sysTaskParameters & );

class PPTriPlant;
struct pngGrassModel;

#define PNG_PLANTSMGR_RSX_LABELI1			(0)
#define PNG_PLANTSMGR_RSX_LABELI2			(1)
#define PNG_PLANTSMGR_RSX_LABELI3			(2)
#define PNG_PLANTSMGR_RSX_LABELI4			(3)
#define PNG_PLANTSMGR_RSX_LABELI5			(4)

//#define PNG_PLANTSMGR_RSXMARKER_NONE		(0xFFFFFFFF)
//#define PNG_PLANTSMGR_RSXMARKER_READY		(0x2222)	// RSX ready for getting new commands
//#define PNG_PLANTSMGR_RSXMARKER_LOCKBUF1	(1)
//#define PNG_PLANTSMGR_RSXMARKER_UNLOCKBUF1	(0)
//#define PNG_PLANTSMGR_RSXMARKER_LOCKBUF2	(3)
//#define PNG_PLANTSMGR_RSXMARKER_UNLOCKBUF2	(2)
//#define PNG_PLANTSMGR_RSXMARKER_LOCKBUF3	(5)
//#define PNG_PLANTSMGR_RSXMARKER_UNLOCKBUF3	(4)

#define PNG_PLANTSMGR_SPU_USE_BIG_HEAP					(1)
#define PNG_PLANTSMGR_BIG_HEAP_SIZE						(512*1024)	// 512KB for external RSX commandbuffer

#define PNG_PLANTSMGR_LOCAL_HEAP_SIZE					(8192)		// local SPU fifo size
#define PNG_PLANTSMGR_LOCAL_HEAP_GAP_SIZE				(4096)		// gap between buffers (min. 4KB is recommended)

#define PNG_PLANTSMGR_LOCAL_GRASSMODEL_MAX_VERTS		(64)
#define PNG_PLANTSMGR_MAX_NUM_OF_RENDER_JOBS			(8)			// max num of simultaneous render jobs launched

#define PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE			(20)		// it usually takes 19+1 commands to set the whole texturestate
#define PNG_PLANTSMGR_LOCAL_GEOMETRY_MAXCMDSIZE			(80)
#define PNG_PLANTSMGR_LOCAL_BINDSHADERLOD0_MAXCMDSIZE	(104+4)
#define PNG_PLANTSMGR_LOCAL_BINDSHADERLOD1_MAXCMDSIZE	(64+8)
#define PNG_PLANTSMGR_LOCAL_BINDSHADERLOD2_MAXCMDSIZE	(72+4)

// size (in bytes) of one full slot for all textureCmds and grassModels:
#define PNG_PLANTSMGR_LOCAL_SLOTSIZE_TEXTURE	(PNG_PLANT_SLOT_NUM_TEXTURES*2*PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE*sizeof(u32))
#define PNG_PLANTSMGR_LOCAL_SLOTSIZE_MODEL		(PNG_PLANT_SLOT_NUM_MODELS*2*sizeof(pngGrassModel))	


//
//
//
//
struct grassVertexSPU
{
	float	_pos[3];		// defined as float[3] not Vector3 to avoid strange alignment
	float	_normal[3];
	Color32	_cpv;
	float	_uv[2];
};
const int grassVertexSPUSize4	= 9;		// 9 words = 36 bytes


enum { spuCLIP_PLANE_NEAR=0, spuCLIP_PLANE_LEFT, spuCLIP_PLANE_RIGHT, spuCLIP_PLANE_FAR, spuCLIP_PLANE_TOP, spuCLIP_PLANE_BOTTOM };


//
//
//
//
struct spuGrassParamsStruct
{
	u32					m_bDummyJob;					// if true, then SPU will exit without processing - small trick to better allocate jobs in SPURS
	int				m_numVertsGrassModelAllocated;	// no of verts in plant model (used for DMA'ing: usually multiply of 64)
	u32					m_slotID;						// main slotID used by this batch
	u32					m_pad00;

	PPTriPlant			*m_pTriPlant;					// 1st ptr to PPTriPlant
	u32					m_numTriPlant;					// no of triplants to render
	u32					m_TriPlantDmaSize;				// size for DMA
	u32					pad_02;

	u32					pad0;
	u32					pad1;
	u32					pad2;
	u32					pad3;

#if __DEV
	u32					m_nLocTriPlantsSlotID;
	u32					pad5a[3];
#else
	u32					pad5a[4];
#endif
	u32					pad6a[4];
	u32					pad7a[4];
	u32					pad8a[4];
	u32					pad9a[4];
	// SPU: size of this struct must be dma'able:
	#define PSN_SIZEOF_SPUGRASSPARAMSTRUCT		(128)
};


//
//
//
//
struct spuGrassParamsStructMaster
{
	u32					m_nNumGrassJobsLaunched;
	spuGrassParamsStruct	*m_inSpuGrassStructTab;
	u32					m_DeviceFrameCounter;			// GRCDEVICE.GetFrameCounter()
	u32					m_DecrementerTicks;				// amount of ticks used to run SPU task

	u32					m_heapBegin;
	u32					m_heapBeginOffset;
	u32					m_rsxLabel5_CurrentTaskID;		// value of wait tag in Label4 for current job
	u32					m_pad00;

	u32					m_eaMainJTS;					// EA of JTS in startupCB blocking jump to big heap 
	u32					m_ReturnOffsetToMainCB;			// return offset from big heap to main CB
	u32					m_AmountOfBigHeapConsumed;		// out: how much space consumed in big buffer
	u32					m_AmountOfBigHeapOverfilled;	// out: how much big buffer was overfilled

	u32					m_rsxLabelIndices[5];			// index of RSX Label1
	u32					m_eaRsxLabel[5];				// ea of RSX Label1
	float					m_LOD0FarDist2;
	float					m_LOD1CloseDist2;
	
	float					m_LOD1FarDist2;
	float					m_LOD2CloseDist2;
	float					m_LOD2FarDist2;
	float					m_windBending;

	u32					*m_BindShaderLOD0CmdBuf;		// 1 x Bind Shader LOD0
	u32					*m_BindShaderLOD1CmdBuf;		// 1 x Bind Shader LOD1
	u32					*m_BindShaderLOD2CmdBuf;		// 1 x Bind Shader LOD2
	u32					*m_GeometryCmdLOD2;				// geometry cmds for LOD2

	pngGrassModel				*m_PlantModelsTab[PNG_PLANT_NUM_PLANT_SLOTS];	// geometries for all 4 slots
	u32					*m_PlantTexturesTab[PNG_PLANT_NUM_PLANT_SLOTS];// textures for all 4 slots

	Vector4					m_vecCameraPos;
	
	Vector4					m_vecFrustumPlanes[6];			//	6 clipplanes

#if __DEV	// __BANK is always 0 on all SPU builds 
	// statistics & debug:
	u32					m_LocTriPlantsLOD0DrawnPerSlot[PNG_PLANT_NUM_PLANT_SLOTS];	// out
	u32					m_LocTriPlantsLOD1DrawnPerSlot[PNG_PLANT_NUM_PLANT_SLOTS];	// out
	u32					m_LocTriPlantsLOD2DrawnPerSlot[PNG_PLANT_NUM_PLANT_SLOTS];	// out

	u32					m_bGeomCullingTestEnabled	: 1;
	u32					m_bPlantsFlashLOD0			: 1;
	u32					m_bPlantsFlashLOD1			: 1;
	u32					m_bPlantsFlashLOD2			: 1;
	u32					__pad00						: 28;
	u32					__pad01[3];
#endif //__DEV...
};



#if !__SPU
/*
struct grassModelSPU
{
public:
	grassModelSPU() :	m_numVerts(0), m_verts(NULL) 		{ }
	grassModelSPU(int s)									{ Allocate(s);	}
	~grassModelSPU()										{ Destroy();	}
	
	bool Allocate(int size)								{ Assert(size<=PNG_PLANTSMGR_LOCAL_GRASSMODEL_MAX_VERTS); m_verts=(grassVertexSPU*)GtaMallocAlign(PNG_PLANTSMGR_LOCAL_GRASSMODEL_MAX_VERTS*sizeof(grassVertexSPU), 128); m_numVerts=size; return(m_verts!=NULL);	}
	void Destroy()											{ if(m_verts) {GtaFreeAlign(m_verts);m_verts=NULL;} m_numVerts=0;									}

	int			GetNumVerts()							{ return(m_numVerts);							}
	int			GetNumVertsAllocated()					{ return(PNG_PLANTSMGR_LOCAL_GRASSMODEL_MAX_VERTS);	}
	grassVertexSPU*	GetVertexPtr(int i=0)					{ return &m_verts[i];							}

public:
	int			m_numVerts;
	grassVertexSPU*	m_verts;
};
*/
//
//
// works like grassModelSPU, but keeps data of 4 geometries at once in common aligned memory
// (for easier dma'ing data over to SPU);
// every model occupies max. 64 verts in the table:
//
/*
struct grassModelSPU4
{
private:
	enum {maxNumModels=4};

public:
	grassModelSPU4() :	m_verts(NULL) 						{ for(int i=0;i<maxNumModels;i++) m_numVerts[i]=0; }
	~grassModelSPU4()										{ Destroy();	}
	
	bool Allocate()											{ m_verts=(grassVertexSPU*)GtaMallocAlign(PNG_PLANTSMGR_LOCAL_GRASSMODEL_MAX_VERTS*maxNumModels*sizeof(grassVertexSPU), 128); return(m_verts!=NULL);	}
	void Destroy()											{ if(m_verts) {GtaFreeAlign(m_verts);m_verts=NULL;} for(int i=0;i<maxNumModels;i++) m_numVerts[i]=0;;												}	

	void			SetNumVerts(int modelIndex, int n)	{ Assert(modelIndex<maxNumModels); m_numVerts[modelIndex]=n; }
	int			GetMaxNumVerts(int modelIndex)		{ Assert(modelIndex<maxNumModels); return(PNG_PLANTSMGR_LOCAL_GRASSMODEL_MAX_VERTS);	} //modelIndex; ???  DW - quick hack to make this header compile.
	int			GetNumVerts(int modelIndex)			{ Assert(modelIndex<maxNumModels); return(m_numVerts[modelIndex]);					}

	int			GetNumVertsAllocated()					{ return(PNG_PLANTSMGR_LOCAL_GRASSMODEL_MAX_VERTS*maxNumModels);	}
	grassVertexSPU*	GetVertexPtr(int modelIndex, int i)	{ Assert(modelIndex<maxNumModels); return &m_verts[i+(PNG_PLANTSMGR_LOCAL_GRASSMODEL_MAX_VERTS*modelIndex)];							}

	void			SetGeometry(int modelIndex, grmGeometry *g)	{ Assert(modelIndex<maxNumModels); m_Geometries[modelIndex]=g;		}
	grmGeometry*	GetGeometry(int modelIndex)					{ Assert(modelIndex<maxNumModels); return m_Geometries[modelIndex]; }
	
public:
	int			m_numVerts[maxNumModels];
	grassVertexSPU*	m_verts;

	grmGeometry*	m_Geometries[maxNumModels];
};
*/
#endif //!__SPU...


#endif //__PLANTSGRASSRENDERERSPU_H__...

