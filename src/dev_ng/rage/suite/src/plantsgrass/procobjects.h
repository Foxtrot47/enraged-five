//
// plantsgrass/procobjects.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef PLANTSGRASS_PROCOBJECTS_H
#define	PLANTSGRASS_PROCOBJECTS_H

#include "phcore/isect.h"
#include "phcore/segment.h"
#include "plantsgrass/proceduralinfo.h"
#include "plantsgrass/vfxlist.h"
#include "vector/vector3.h"

#define	MAX_PROC_MATERIAL_INFOS	(256)
#define	MAX_ENTITY_ITEMS		(1024)
#define	MAX_ALLOCATED_MATRICES	(512)
#define	MAX_SEGMENTS			(64)

namespace rage
{

class pngPlantLocTri;

class pngEntityItem : public pngListItem
{		
	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

public: ///////////////////////////

//	CEntity* 			m_pData;

	pngPlantLocTri* 		m_pParentTri;
	//CEntity* 			m_pParentEntity;

	bool				m_allocatedMatrix;

#if __BANK
	float				triVert1[3];
	float				triVert2[3];
	float				triVert3[3];
#endif // __BANK

}; // pngEntityItem

struct pngProcObjectCreationInfo
{
	Vector3 pos;
	Vector3 normal;
	pngProcObjInfo* pProcObjInfo;
	pngPlantLocTri* pLocTri;
};

struct pngProcTriRemovalInfo
{
	pngPlantLocTri* pLocTri;
	s32 procTagId;
};

class pngProcObjectMan
{	
public:
	pngProcObjectMan				();
	~pngProcObjectMan				();

	// 
	bool				InitLevel					();
	void				ShutdownLevel				(bool shutdownBank=true);

	void 				Update						();	

	s32				ProcessTriangleAdded		(pngPlantLocTri* pLocTri);
	void				ProcessTriangleRemoved		(pngPlantLocTri* pLocTri, s32 procTagId);

	s32				ProcessEntityAdded			(/*CEntity* pEntity*/);
	void				ProcessEntityRemoved		(/*CEntity* pEntity*/);

	pngEntityItem*		AddObject					(const Vector3& pos, const Vector3& normal, pngProcObjData* pProcObjData, pngList* pList/*, CEntity* pParentEntity*//*, uint8 lighting*/);	
	void				RemoveObject				(pngEntityItem* pEntityItem, pngList* pList);

	pngEntityItem*		GetEntityFromPool			();
	void				ReturnEntityToPool			(pngEntityItem* pEntityItem);

	void				AddTriToRemoveList			(pngPlantLocTri* pLocTri);
	void				LockListAccess				();
	void				UnlockListAccess			();

	s32				GetNumActiveObjects			();

#if __DEV
	bool				IsAllowedToDelete			();
#endif

#if __BANK
	//		void				DisplayDebugInfo			();
	void				RenderDebug					();
	void				RenderDebugEntityZones		();
	static	void				ReloadData					();
#endif // __BANK


private: //////////////////////////

	s32 				AddObjects					(pngProcObjInfo* pProcObjInfo, pngPlantLocTri* pLocTri);
	s32				AddObjects					(/*CEntity* pEntity, C2dEffect* p2dEffect, */s32 numObjsToCreate, phIntersection* pIntersections);
	void				AddObjectToAddList			(Vector3::Param pos, Vector3::Param normal, pngProcObjInfo* pProcObjInfo, pngPlantLocTri* pLocTri);
	void				ProcessAddAndRemoveLists	();	
	bool				IsPtInTriangle2D			(float x, float y, const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& normal, float* z);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////		

public: ///////////////////////////

	s32 				m_numAllocatedMatrices;
	s32				m_numProcObjObjects;								// as opposed to buildings

#if __DEV
	bool				m_removingProcObject;
#endif

#if __BANK
	bool				m_bankInitialised;

	bool				m_disableEntityObjects;
	bool				m_disableCollisionObjects;
	bool				m_renderDebugPolys;
	bool				m_renderDebugZones;
	bool				m_ignoreMinDist;
	bool				m_ignoreSeeding;
	bool				m_forceOneObjPerTri;
	s32				m_numActiveProcObjs;
	s32				m_numProcObjBuildings;
	s32				m_maxProcObjObjects;
	s32				m_numProcObjsToAdd;
	s32				m_numProcObjsToAddSize;
	s32				m_numTrisToRemove;
	s32				m_numTrisToRemoveSize;
#endif // __BANK

private: //////////////////////////

	// pool and list of entity items
	pngEntityItem			m_entityItems				[MAX_ENTITY_ITEMS];
	pngList				m_entityPool;

	// list of entities created by objects (2d effects)
	pngList				m_objectEntityList;

	phSegment			m_segments[64];
	phSegment*			m_pSegments[64];
	phIntersection		m_intersections[64];
	phIntersection*		m_pIntersections[64];

	atArray<pngProcObjectCreationInfo> m_objectsToCreate;
	atArray<pngProcTriRemovalInfo> m_trisToRemove;
};

extern pngProcObjectMan	g_procObjMan;

} // namespace rage

#endif // PLANTSGRASS_PROCOBJECTS_H
