//
// plantsgrass/vfxlist.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "plantsgrass/vfxlist.h"

// system
//#include <stdio.h>

namespace rage
{

// This is a simple class which must be inherited by any class which
// wants to be used as a linked list.
// It only contains two pointers: one to the previous item in the list; 
// and another to the next item in the list. These are both set to NULL
// on creation of an pngListItem object.

pngListItem::pngListItem()
{
	m_pNext = NULL;

#ifndef USE_BASIC_ONLY
	m_pPrev = NULL;
#endif // USE_BASIC_ONLY
}

// This class is where the linked list structure is implemented. It 
// contains a pointer to the head of the list which gives us access to 
// the items within the list. Only classes which inherit pngListItem can
// be stored in the list.
//
// The class contains functions to add, remove and iterate through the
// items stored in it.
//
// A cast must be performed on the returned pngListItem to get access 
// to the data within it.
//
// A destroy function is also included which will delete all of the 
// entries within the list. Individual removal of items will not 
// delete the item.

pngList::pngList()
{
	m_numItems = 0;
	m_pHead = NULL;

#ifndef USE_BASIC_ONLY
	m_pTail		= NULL;
#endif // USE_BASIC_ONLY
}

s32 pngList::AddItem(pngListItem* pItem)
{	
#ifdef USE_BASIC_ONLY

	item->SetNext(m_pHead);
	m_pHead = pItem;

	// one more item in list
	return m_numItems++;

#else // USE_BASIC_ONLY

	// store where the head currently points to
	pngListItem* headItem = m_pHead;

	// set up the ptrs for the new item
	m_pHead = pItem;
	pItem->SetPrev(NULL);
	pItem->SetNext(headItem);

	// set up the ptrs for the old head
	if (headItem)
	{
		headItem->SetPrev(pItem);
	}
	else
	{
		m_pTail = pItem;
	}

	// one more item in list
	return m_numItems++;

#endif // USE_BASIC_ONLY
}

s32 pngList::RemoveItem(pngListItem* pItem)
{
#ifdef USE_BASIC_ONLY
	// find the item in the list
	pngListItem* pCurrItem = m_pHead;
	pngListItem* pPrevItem = NULL;

	while (pCurrItem)
	{
		if (item == pCurrItem)
		{
			break;
		}	

		pPrevItem = pCurrItem;
		pCurrItem = pCurrItem->GetNext();
	}

	// remove if found
	if (pCurrItem)
	{
		if (pPrevItem == NULL)
		{
			m_pHead = pCurrItem->GetNext();
		}
		else
		{
			pPrevItem->SetNext(pCurrItem->GetNext());	
		}

		// one less item in list
		return m_numItems--;
	}
	else
	{
		return m_numItems;
	}

#else // USE_BASIC_ONLY
	// store this items next and prev items
	pngListItem* nextItem = pItem->GetNext();
	pngListItem* prevItem = pItem->GetPrev();

	// update ptrs 
	if (nextItem)
	{
		nextItem->SetPrev(prevItem);
	}
	else
	{
		m_pTail = pItem->GetPrev();
	}

	if (prevItem)
	{
		prevItem->SetNext(nextItem);
	}
	else
	{
		m_pHead = pItem->GetNext();
	}

	// one less item in list
	return m_numItems--;

#endif // USE_BASIC_ONLY
}

pngListItem* pngList::RemoveHead()
{
#ifdef USE_BASIC_ONLY

	pngListItem* pReturnItem = NULL;

	if (m_pHead)
	{
		pReturnItem = m_pHead;

		m_pHead = m_pHead->GetNext();

		m_numItems--;

		return pReturnItem;
	}
	else
	{
		return pReturnItem;
	}

#else // USE_BASIC_ONLY

	pngListItem* pReturnItem = NULL;

	if (m_pHead)
	{
		pReturnItem = m_pHead;

		if (m_pHead == m_pTail)
		{
			// it's the only item in the list
			m_pHead = m_pTail = NULL;
		}
		else
		{
			// pt the head to the second item
			m_pHead = m_pHead->GetNext();	

			if (m_pHead)
			{
				m_pHead->SetPrev(NULL);
			}
		}

		m_numItems--;
		return pReturnItem;
	}
	else
	{
		return pReturnItem;
	}

#endif // USE_BASIC_ONLY
}

s32 pngList::RemoveAll()
{
	m_numItems = 0;
	m_pHead = NULL;

#ifndef USE_BASIC_ONLY
	m_pTail = NULL;
#endif // USE_BASIC_ONLY

	return 0;
}


#ifndef USE_BASIC_ONLY
s32 pngList::AppendItem(pngListItem* pItem)
{
	// store where the tail currently points to
	pngListItem* tailItem = m_pTail;

	// set up ptrs for the new item
	m_pTail = pItem;
	pItem->SetPrev(tailItem);
	pItem->SetNext(NULL);

	// set up ptrs for the old tail
	if (tailItem)
	{
		tailItem->SetNext(pItem);
	}
	else
	{
		m_pHead = pItem;
	}

	// one more item in list
	return m_numItems++;
}

s32 pngList::InsertAfterItem(pngListItem* pItem, pngListItem* pHere)
{
	// find the item in the list
	pngListItem* pCurrItem = m_pHead;

	while (pCurrItem)
	{
		if (pHere == pCurrItem)
		{
			break;
		}	

		pCurrItem = pCurrItem->GetNext();
	}

	// insert new item after this one
	if (pCurrItem)
	{
		pngListItem* nextItem = pCurrItem->GetNext();

		// set up ptrs for this item
		pCurrItem->SetNext(pItem);

		// set up ptrs for the new item
		pItem->SetPrev(pCurrItem);
		pItem->SetNext(nextItem);

		// set up ptrs for the next item
		if (nextItem)
		{
			nextItem->SetPrev(pItem);
		}
		else
		{
			m_pTail = pItem;
		}
	}

	// one more item in list
	return m_numItems++;
}

s32 pngList::InsertBeforeItem(pngListItem* pItem, pngListItem* pHere)
{
	// find the item in the list
	pngListItem* pCurrItem = m_pHead;

	while (pCurrItem)
	{
		if (pHere == pCurrItem)
		{
			break;
		}	

		pCurrItem = pCurrItem->GetNext();
	}

	// insert new item before this one
	if (pCurrItem)
	{
		pngListItem* prevItem = pCurrItem->GetPrev();

		// set up ptrs for this item
		pCurrItem->SetPrev(pItem);

		// set up ptrs for the new item
		pItem->SetPrev(prevItem);
		pItem->SetNext(pCurrItem);

		// set up ptrs for the prev item
		if (prevItem)
		{
			prevItem->SetNext(pItem);
		}
		else
		{
			m_pHead = pItem;
		}
	}

	// one more item in list
	return m_numItems++;
}

pngListItem* pngList::RemoveTail()
{
	pngListItem* pReturnItem = NULL;

	if (m_pTail)
	{
		pReturnItem = m_pTail;

		// pt the tail to the second last item
		m_pTail = m_pTail->GetPrev();
		m_pTail->SetNext(NULL);	

		m_numItems--;

		return pReturnItem;
	}
	else
	{
		return pReturnItem;
	}
}
#endif // USE_BASIC_ONLY

#if __ASSERT
void pngList::CheckListIntegrity(char* txt)
{
	s32 numItemsInList = 0;
	pngListItem* pCurrItem = m_pHead;
	while (pCurrItem)
	{
		numItemsInList++;
		pCurrItem = pCurrItem->GetNext();
	}	

	Assertf(numItemsInList==m_numItems, "NumActualItems=%d NumItem=%d - %s", numItemsInList, m_numItems, txt);
}
#endif

} // namespace rage
