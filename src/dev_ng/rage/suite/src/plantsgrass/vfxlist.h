//
// plantsgrass/vfxlist.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

//	Linked lists are implemented via two classes:
//
//  1. pngListItem
//	
//		  This is a simple class which must be inherited by any class which
//		wants to be used as a linked list.
//		
//		  It only contains two pointers: one to the previous item in the list; 
//		and another to the next item in the list. These are both set to NULL
//		on creation of an pngListItem object.
//
//	2. pngList
//
//		  This class is where the linked list structure is implemented. It 
//		contains a pointer to the head of the list which gives us access to 
//		the items within the list. Only classes which inherit pngListItem can
//		be stored in the list.
//
//		  The class contains functions to add, remove and iterate through the
//		items stored in it.
//
//		  A cast must be performed on the returned pngListItem to get access 
//		to the data within it.
//
//		  A destroy function is also included which will delete all of the 
//		entries within the list. Individual removal of items will not 
//		delete the item.

#ifndef	PLANTSGRASS_VFX_LIST_H
#define PLANTSGRASS_VFX_LIST_H

//#define USE_BASIC_ONLY // gets rid of prev and tail ptrs and related functions if defined

namespace rage
{

class pngListItem
{
public:
	// constructor / destructor
	pngListItem();

	// access functions
	pngListItem* GetPrev() { return m_pPrev; }
	pngListItem* GetNext() { return m_pNext; }
	void SetPrev(pngListItem* pItem) { m_pPrev = pItem; }
	void SetNext(pngListItem* pItem) { m_pNext = pItem; }

private:
	pngListItem*			m_pNext; // ptr to the prev item

#ifndef USE_BASIC_ONLY
	pngListItem*			m_pPrev; // ptr to the next item
#endif // USE_BASIC_ONLY
};

class pngList
{
public:
	pngList();
	s32 AddItem(pngListItem* pItem);			
	s32 RemoveItem(pngListItem* pItem);			
	pngListItem* RemoveHead();
	s32 RemoveAll();							
	pngListItem* GetHead() { return m_pHead; }
	pngListItem* GetNext(pngListItem* pItem) { return pItem->GetNext(); }
	s32 GetNumItems() const { return m_numItems; }

#ifndef USE_BASIC_ONLY
	// list control
	s32 AppendItem(pngListItem* pItem);			
	s32 InsertAfterItem(pngListItem* pItem, pngListItem* pHere);
	s32 InsertBeforeItem(pngListItem* pItem, pngListItem* pHere);
	pngListItem* RemoveTail();							

	// list access
	pngListItem* GetTail() { return m_pTail; }
	pngListItem* GetPrev(pngListItem* pItem) { return pItem->GetPrev(); }

#endif // USE_BASIC_ONLY

#if __ASSERT
	void CheckListIntegrity(char* txt);
#endif

private:
	s32				m_numItems; // number of items in the list
	pngListItem*	m_pHead; // ptr to the head of the list

#ifndef USE_BASIC_ONLY
	pngListItem*	m_pTail; // ptr to the tail of the list
#endif // USE_BASIC_ONLY
};

} // namespace rage

#endif // PLANTSGRASS_VFX_LIST_H
