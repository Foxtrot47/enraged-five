//
// plantsgrass/procobjects.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#if 0
#include "plantsgrass/procobjects.h"

// rage
#include "bank/bkmgr.h"
#include "math/random.h"
#include "physics/iterator.h"
#include "system/criticalsection.h"

// game
// #include "Camera/CamInterface.h"
// #include "Debug/DebugDraw.h"
// #include "Debug/TimeBars.h"
// #include "ModelInfo/ModelInfo.h"
// #include "Network/NetworkInterface.h"
// #include "Objects/Object.h"
// #include "Physics/GtaInst.h"
// #include "Physics/GtaMaterialManager.h"
// #include "Physics/Physics.h"
// #include "Renderer/PlantsMgr.h"
// #include "Scene/Building.h"
// #include "Scene/FileLoader.h"
// #include "Scene/World/GameWorld.h"
// #include "Streaming/Streaming.h"

namespace rage
{

pngProcObjectMan	g_procObjMan;
sysCriticalSectionToken listAccessToken;

pngProcObjectMan::pngProcObjectMan			()
{
#if __BANK
	m_bankInitialised = false;
#endif
}

pngProcObjectMan::~pngProcObjectMan			()
{
}

bool				pngProcObjectMan::InitLevel					()
{
	// fill up the object pool
	for (s32 i=0; i<MAX_ENTITY_ITEMS; i++)
	{
		//m_entityItems[i].m_pData = NULL;
		m_entityItems[i].m_pParentTri = NULL;
//		m_entityItems[i].m_pParentEntity = NULL;
		m_entityItems[i].m_allocatedMatrix = false;

		m_entityPool.AddItem(&m_entityItems[i]);
	}

	m_numAllocatedMatrices = 0;
	m_numProcObjObjects = 0;

	// initialise the segments and intersections
	for (s32 i=0; i<MAX_SEGMENTS; i++)
	{
		m_pSegments[i] = &m_segments[i];
		m_pIntersections[i] = &m_intersections[i];
	}

	// set the size of the add and remove arrays
	m_objectsToCreate.Reserve(MAX_ENTITY_ITEMS);

#if __DEV
	m_removingProcObject = false;
#endif

#if __BANK
	if (!m_bankInitialised)
	{
		m_disableEntityObjects = false;
		m_disableCollisionObjects = false;
		m_renderDebugPolys = false;
		m_renderDebugZones = false;
		m_ignoreMinDist = false;
		m_ignoreSeeding = false;
		m_forceOneObjPerTri = false;
		m_numProcObjBuildings = 0;
		m_maxProcObjObjects = 0;

/* // TJ
		char txt[128];
		CObject::ms_pBank->PushGroup("Procedural Objects", false);
		CObject::ms_pBank->AddToggle("Disable Entity Objects ", &m_disableEntityObjects);
		CObject::ms_pBank->AddToggle("Disable Collision Objects ", &m_disableCollisionObjects);
		CObject::ms_pBank->AddToggle("Render Debug Polys", &m_renderDebugPolys);
		CObject::ms_pBank->AddToggle("Render Debug Zones", &m_renderDebugZones);
		CObject::ms_pBank->AddToggle("Ignore Min Dist", &m_ignoreMinDist);
		CObject::ms_pBank->AddToggle("Ignore Seeding", &m_ignoreSeeding);
		CObject::ms_pBank->AddToggle("Force One Obj Per Tri", &m_forceOneObjPerTri);
		sprintf(txt, "Active Objects (%d)", MAX_ENTITY_ITEMS);
		CObject::ms_pBank->AddSlider(txt, &m_numActiveProcObjs, 0, MAX_ENTITY_ITEMS, 0);
		CObject::ms_pBank->AddSlider("Num Buildings", &m_numProcObjBuildings, 0, MAX_ENTITY_ITEMS, 0);
		CObject::ms_pBank->AddSlider("Num Objects", &m_numProcObjObjects, 0, MAX_ENTITY_ITEMS, 0);
		CObject::ms_pBank->AddSlider("Max Objects", &m_maxProcObjObjects, 0, MAX_ENTITY_ITEMS, 0);
		CObject::ms_pBank->AddSlider("Num To Add", &m_numProcObjsToAdd, 0, MAX_ENTITY_ITEMS, 0);
		CObject::ms_pBank->AddSlider("Num To Add Size", &m_numProcObjsToAddSize, 0, MAX_ENTITY_ITEMS, 0);
		CObject::ms_pBank->AddSlider("Num To Remove", &m_numTrisToRemove, 0, MAX_ENTITY_ITEMS, 0);
		CObject::ms_pBank->AddSlider("Num To Remove Size", &m_numTrisToRemoveSize, 0, MAX_ENTITY_ITEMS, 0);
		sprintf(txt, "Allocated Matrices (%d)", MAX_ALLOCATED_MATRICES);
		CObject::ms_pBank->AddSlider(txt, &m_numAllocatedMatrices, 0, MAX_ALLOCATED_MATRICES, 0);
		CObject::ms_pBank->AddButton("Reload Data File", ReloadData);
		CObject::ms_pBank->PopGroup();
*/

		m_bankInitialised = true;
	}
#endif // __BANK

	return true;
}


///////////////////////////////////////////////////////////////////////////////
//  Shutdown
///////////////////////////////////////////////////////////////////////////////

void				pngProcObjectMan::ShutdownLevel				(bool shutdownBank)
{
	// shutdown the triangle entities
	for (s32 i=0; i<g_procInfo.m_numProcObjInfos; i++)
	{
		pngEntityItem* pEntityItem = (pngEntityItem*)g_procInfo.m_procObjInfos[i].m_entityList.GetHead();

		while (pEntityItem)
		{
			pngEntityItem* pNextEntityItem = (pngEntityItem*)g_procInfo.m_procObjInfos[i].m_entityList.GetNext(pEntityItem);

			g_procObjMan.RemoveObject(pEntityItem, &g_procInfo.m_procObjInfos[i].m_entityList);

			pEntityItem = pNextEntityItem;
		}

		Assert(g_procInfo.m_procObjInfos[i].m_entityList.GetNumItems()==0);
	}

	// shutdown the object entities
	pngEntityItem* pEntityItem = (pngEntityItem*)m_objectEntityList.GetHead();

	while (pEntityItem)
	{
		pngEntityItem* pNextEntityItem = (pngEntityItem*)m_objectEntityList.GetNext(pEntityItem);

		RemoveObject(pEntityItem, &m_objectEntityList);

		pEntityItem = pNextEntityItem;
	}

	Assert(m_objectEntityList.GetNumItems()==0);

	// remove entities from the pool
	Assert(m_entityPool.GetNumItems() == MAX_ENTITY_ITEMS);
	m_entityPool.RemoveAll();

	Assert(m_numAllocatedMatrices==0);

	m_objectsToCreate.Reset();
	m_trisToRemove.Reset();

#if __BANK
	m_maxProcObjObjects = 0;
#endif

	if (shutdownBank)
	{
#if __BANK
		if (shutdownBank && m_bankInitialised)
		{
			m_bankInitialised = false;
		}
#endif // __BANK
	}
}


///////////////////////////////////////////////////////////////////////////////
//  Update
///////////////////////////////////////////////////////////////////////////////

void				pngProcObjectMan::Update						()
{
#if __BANK
// TJ
// 	if(CTimer::IsGamePaused())
// 		return;

	m_numActiveProcObjs = MAX_ENTITY_ITEMS-m_entityPool.GetNumItems();
	if (m_numProcObjObjects>m_maxProcObjObjects)
	{
		m_maxProcObjObjects = m_numProcObjObjects;
	}

	m_numProcObjsToAdd = m_objectsToCreate.GetCount();
	m_numProcObjsToAddSize = m_objectsToCreate.GetCapacity();

	m_numTrisToRemove = m_trisToRemove.GetCount();
	m_numTrisToRemoveSize = m_trisToRemove.GetCapacity();

	if (m_numActiveProcObjs==MAX_ENTITY_ITEMS)
	{
		Warningf("Too many procedural objects in this area\n");
	}
#endif // __BANK

	// go through the objects created by other objects to see if the parent has been deleted
#if 0
	pngEntityItem* pEntityItem = (pngEntityItem*)m_objectEntityList.GetHead();
	while (pEntityItem)
	{
		pngEntityItem* pNextEntityItem = (pngEntityItem*)m_objectEntityList.GetNext(pEntityItem);

// TJ
// 		if (pEntityItem->m_pParentEntity == NULL)
// 		{
// 			RemoveObject(pEntityItem, &m_objectEntityList);
// 		}

		pEntityItem = pNextEntityItem;
	}
#endif // 0

	// advance the scan code so we can tell which entities are visible this frame
//	CGameWorld::AdvanceCurrentScanCode();
#if 0
	// get all of the insts within 30m of the camera
#define MAX_ENTITIES_TO_PROCESS (128)
	s32 numEntitiesToProcess = 0;
	CEntity* pEntityProcessList[MAX_ENTITIES_TO_PROCESS];
	phIterator it;
	it.InitCull_Sphere(camInterface::GetPos(), 30.0f);
	it.SetStateIncludeFlags(phLevelBase::STATE_FLAGS_ALL);
	u16 curObjectLevelIndex = CPhysics::GetLevel()->GetFirstCulledObject(it);
	while (curObjectLevelIndex!=phInst::INVALID_INDEX && numEntitiesToProcess<MAX_ENTITIES_TO_PROCESS)
	{
		phInst& culledInst = *CPhysics::GetLevel()->GetInstance(curObjectLevelIndex);

		// get the entity from the inst
		CEntity* pEntity = CPhysics::GetEntityFromInst(&culledInst);
#if __DEBUG
		Assert(pEntity);
#endif
		if (pEntity && pEntity->GetModelIndex()>-1)
		{
			// check if this object has a procedural object 2d effect
			bool createsProcObjects = false;
			CBaseModelInfo* pModelInfo = CModelInfo::GetModelInfo(pEntity->GetModelIndex());
			s32 num2dEffects = pModelInfo->GetNum2dEffects();

			for (s32 i=0; i<num2dEffects; i++)
			{
				C2dEffect* pEffect = pModelInfo->Get2dEffect(i);
				if (pEffect->GetType() == ET_PROCEDURALOBJECTS)
				{
					createsProcObjects = true;
					break;
				}
			}

			if (createsProcObjects)
			{
				// mark this entity as being valid this frame
				pEntity->SetScanCode(CGameWorld::GetCurrentScanCode());

				// stop proc objs being created around broken pieces of fragments
				bool bSkipThisEntity = false;
				if(pEntity->GetIsTypeObject() && ((CObject*)pEntity)->GetPhysInst() && ((CObject*)pEntity)->GetPhysInst()->GetClassType()==PH_INST_FRAG_CACHE_OBJECT)
					bSkipThisEntity = true;

				// check if this entity has created objects already
				if (!pEntity->m_nFlags.bCreatedProcObjects && !bSkipThisEntity)
				{
					// object is in range and it hasn't created procedural objects yet - create some
					pEntityProcessList[numEntitiesToProcess] = pEntity;
					numEntitiesToProcess++;
				}
			}
		}

		curObjectLevelIndex = CPhysics::GetLevel()->GetNextCulledObject(it);
	}

	// process the entities
	for (s32 i=0; i<numEntitiesToProcess; i++)
	{
		//ProcessEntityAdded(pEntityProcessList[i]);
	}
#endif // 0
#if 0
	// go through the entity list to see what needs removed this frame
	pEntityItem = (pngEntityItem*)m_objectEntityList.GetHead();
	while (pEntityItem)
	{
		pngEntityItem* pNextEntityItem = (pngEntityItem*)m_objectEntityList.GetNext(pEntityItem);

// TJ
// 		if (pEntityItem->m_pParentEntity->GetScanCode() != CGameWorld::GetCurrentScanCode())
// 		{
// 			RemoveObject(pEntityItem, &m_objectEntityList);
// 		}

		pEntityItem = pNextEntityItem;
	}
#endif // 0

	ProcessAddAndRemoveLists();
}

s32 pngProcObjectMan::ProcessTriangleAdded(pngPlantLocTri* pLocTri)
{
	Assert(pLocTri);

#if __BANK
	if (m_disableCollisionObjects)
	{
		return 0;
	}
#endif // __BANK

// 	if(NetworkInterface::IsGameInProgress())
// 	{
// 		return 0;
// 	}

	s32 numObjectsAdded = 0;

	// add an object here
	s32 procTagId = PGTAMATERIALMGR->UnpackProcId(pLocTri->m_nSurfaceType);
	s32 procObjInfoIndex = g_procInfo.m_procTagTable[procTagId].procObjIndex;
	Assert(g_procInfo.m_procObjInfos[procObjInfoIndex].m_procTagId==procTagId);

	while (g_procInfo.m_procObjInfos[procObjInfoIndex].m_procTagId==procTagId && procObjInfoIndex<g_procInfo.m_numProcObjInfos)
	{
		numObjectsAdded += AddObjects(&g_procInfo.m_procObjInfos[procObjInfoIndex], pLocTri);

		procObjInfoIndex++;
	}

	return numObjectsAdded;
}

void pngProcObjectMan::ProcessTriangleRemoved(pngPlantLocTri* pLocTri, s32 procTagId)
{
	Assert(pLocTri);

	// remove all objects associated with this triangle
	//s32 procTagId = PGTAMATERIALMGR->UnpackProcId(pLocTri->m_nSurfaceType);
	s32 procObjInfoIndex = g_procInfo.m_procTagTable[procTagId].procObjIndex;
	Assert(g_procInfo.m_procObjInfos[procObjInfoIndex].m_procTagId==procTagId);
	while (g_procInfo.m_procObjInfos[procObjInfoIndex].m_procTagId==procTagId && procObjInfoIndex<g_procInfo.m_numProcObjInfos)
	{
		pngEntityItem* pEntityItem = (pngEntityItem*)g_procInfo.m_procObjInfos[procObjInfoIndex].m_entityList.GetHead();

		while (pEntityItem)
		{
			pngEntityItem* pNextEntityItem = (pngEntityItem*)g_procInfo.m_procObjInfos[procObjInfoIndex].m_entityList.GetNext(pEntityItem);

			// check if this entity was created on this triangle
			if (pEntityItem->m_pParentTri == pLocTri)
			{
				// Can't trust the triangle pointer anymore so set it to NULL
				pEntityItem->m_pParentTri = NULL;
				RemoveObject(pEntityItem, &g_procInfo.m_procObjInfos[procObjInfoIndex].m_entityList);
			}

			pEntityItem = pNextEntityItem;
		}

		procObjInfoIndex++;
	}
}


///////////////////////////////////////////////////////////////////////////////
//  ProcessEntityAdded
///////////////////////////////////////////////////////////////////////////////

s32				pngProcObjectMan::ProcessEntityAdded			(/*CEntity* pEntity*/)
{
#if 0
	Assert(pEntity);

#if __BANK
	if (m_disableEntityObjects)
	{
		return 0;
	}
#endif // __BANK

	if(NetworkInterface::IsGameInProgress())
	{
		return 0;
	}

	s32 numObjectsAdded = 0;

	// go through the 2d effects looking for info about procedural object creation
	CBaseModelInfo* pModelInfo = CModelInfo::GetModelInfo(pEntity->GetModelIndex());
	s32 num2dEffects = pModelInfo->GetNum2dEffects();

	// pre process the data to find out how many objects we need to place for this object
	Assert(num2dEffects<128);
	s32 numObjsToCreate[128];

	// calc each segment info
	s32 currSegment = 0;
	Vector3 vecSegsMin, vecSegsMax;
	vecSegsMin.Zero();
	vecSegsMax.Zero();

	for (s32 i=0; i<num2dEffects; i++)
	{
		C2dEffect* pEffect = pModelInfo->Get2dEffect(i);
		if (pEffect->GetType() == ET_PROCEDURALOBJECTS)
		{
			CProcObjAttr* po = pEffect->GetProcObj();

			// calc how many objects to create
			float zoneArea = 2.0f * PI * po->radiusOuter;//(po->radiusOuter - po->radiusInner);
			numObjsToCreate[i] = static_cast<s32>(zoneArea * (1.0f/(po->spacing*po->spacing)));

			// check we have room to store them
			Assert(currSegment+numObjsToCreate[i]<MAX_SEGMENTS);

			// calc the segment coords
			for (s32 j=0; j<numObjsToCreate[i]; j++)
			{
				// get a random angle and radius
				float angle = g_DrawRand.GetRanged(-PI, PI);
				float radius = g_DrawRand.GetRanged(po->radiusInner, po->radiusOuter);

				// set the drop position (A)
				Vector3 posA;
				pEffect->GetPos(posA);
				posA.x += CMaths::Sin(angle)*radius;
				posA.y += CMaths::Cos(angle)*radius;

				// set the dropped position (B)
				Vector3 posB = posA;
				posB.z -= 2.0f;

				// set the segment up
				m_segments[currSegment++].Set(posA, posB);

				if(posA.x > vecSegsMax.x)	vecSegsMax.x = posA.x;
				else if(posA.x < vecSegsMin.x)	vecSegsMin.x = posA.x;
				if(posA.y > vecSegsMax.y)	vecSegsMax.y = posA.y;
				else if(posA.y < vecSegsMin.y)	vecSegsMin.y = posA.y;
				if(posA.z > vecSegsMax.z)	vecSegsMax.z = posA.z;
				else if(posA.z < vecSegsMin.z)	vecSegsMin.z = posA.z;

				if(posB.x > vecSegsMax.x)	vecSegsMax.x = posB.x;
				else if(posB.x < vecSegsMin.x)	vecSegsMin.x = posB.x;
				if(posB.y > vecSegsMax.y)	vecSegsMax.y = posB.y;
				else if(posB.y < vecSegsMin.y)	vecSegsMin.y = posB.y;
				if(posB.z > vecSegsMax.z)	vecSegsMax.z = posB.z;
				else if(posB.z < vecSegsMin.z)	vecSegsMin.z = posB.z;
			}
		}
		else
		{
			numObjsToCreate[i] = 0;
		}
	}

	if(currSegment > 0)
	{
		for(int i=0; i<currSegment; i++)
		{
			m_pIntersections[i]->Reset();
			m_pSegments[i]->Transform(pEntity->GetMatrix());
		}

		// do the line tests on the segments to find the ground intersections
		Vector3 vecSegsCentre = 0.5f * (vecSegsMax + vecSegsMin);

		Matrix34 matSegBox = pEntity->GetMatrix();
		matSegBox.d = (vecSegsMax - vecSegsCentre) * 1.1f;

		pEntity->GetMatrix().Transform(vecSegsCentre);

		TB_ADD_PROBES(currSegment);
		CPhysics::GetLevel()->TestProbeBatch(currSegment, (const phSegment**)m_pSegments, vecSegsCentre, matSegBox.d.Mag(), matSegBox, &m_pIntersections[0], NULL);
	}

	// add the actual objects
	s32 currIndex = 0;
	for (s32 i=0; i<num2dEffects; i++)
	{
		if (numObjsToCreate[i]>0)
		{
			numObjectsAdded += AddObjects(pEntity, pModelInfo->Get2dEffect(i), numObjsToCreate[i], m_pIntersections[currIndex]);
			currIndex += numObjsToCreate[i];
		}
	}

	pEntity->m_nFlags.bCreatedProcObjects = true;
	return numObjectsAdded;
#endif // 0
}


///////////////////////////////////////////////////////////////////////////////
//  ProcessEntityRemoved
///////////////////////////////////////////////////////////////////////////////

void				pngProcObjectMan::ProcessEntityRemoved		(/*CEntity* pEntity*/)
{
#if 0
	Assert(pEntity);

	// remove all objects associated with this object
	pngEntityItem* pEntityItem = (pngEntityItem*)m_objectEntityList.GetHead();

	while (pEntityItem)
	{
		pngEntityItem* pNextEntityItem = (pngEntityItem*)m_objectEntityList.GetNext(pEntityItem);

		// check if this entity was created on this triangle
		if (pEntityItem->m_pParentEntity == pEntity)
		{
			RemoveObject(pEntityItem, &m_objectEntityList);
		}

		pEntityItem = pNextEntityItem;
	}

	pEntity->m_nFlags.bCreatedProcObjects = false;
#endif // 0
}


///////////////////////////////////////////////////////////////////////////////
//  AddObjects
///////////////////////////////////////////////////////////////////////////////

s32				pngProcObjectMan::AddObjects					(pngProcObjInfo* pProcObjInfo, pngPlantLocTri* pLocTri)
{
	// check if the triangle is out of range
	float distTriToCamSq = (camInterface::GetPos()-pLocTri->m_Center).Mag2();

	if (distTriToCamSq<pProcObjInfo->m_data.minDistSq)
	{
#if __BANK
		if (!g_procObjMan.m_ignoreMinDist)
#endif // __BANK
		{
			return 0;
		}
	}

	// get the vectors along the sides of the triangle
	Vector3 p1p2 = pLocTri->m_V2-pLocTri->m_V1;
	Vector3 p2p3 = pLocTri->m_V3-pLocTri->m_V2;
	Vector3 p1p3 = pLocTri->m_V3-pLocTri->m_V1;
	Vector3 normal;
	normal.Cross(p1p2, p1p3);

	// keep track of how many objects are added
	s32 numObjectsAdded = 0;

	// check if they should be placed in a regular grid formation
	if (pProcObjInfo->m_data.useGrid)
	{
		// placed in regular grid - find the min and max axis aligned bounding box of this triangle
		float minX = CMaths::Min(pLocTri->m_V1.x, pLocTri->m_V2.x);		minX = CMaths::Min(minX, pLocTri->m_V3.x);
		float minY = CMaths::Min(pLocTri->m_V1.y, pLocTri->m_V2.y);		minY = CMaths::Min(minY, pLocTri->m_V3.y);
		float maxX = CMaths::Max(pLocTri->m_V1.x, pLocTri->m_V2.x);		maxX = CMaths::Max(maxX, pLocTri->m_V3.x);
		float maxY = CMaths::Max(pLocTri->m_V1.y, pLocTri->m_V2.y);		maxY = CMaths::Max(maxY, pLocTri->m_V3.y);

		// calculate the grid start and end loops
		float startX = (s32)(minX/pProcObjInfo->m_data.spacing)*pProcObjInfo->m_data.spacing;
		float endX =   ((s32)(maxX/pProcObjInfo->m_data.spacing)+1)*pProcObjInfo->m_data.spacing;
		float startY = (s32)(minY/pProcObjInfo->m_data.spacing)*pProcObjInfo->m_data.spacing;
		float endY =   ((s32)(maxY/pProcObjInfo->m_data.spacing)+1)*pProcObjInfo->m_data.spacing;

		normal.Normalize();

		for (float i=startX; i<endX; i+=pProcObjInfo->m_data.spacing)
		{
			for (float j=startY; j<endY; j+=pProcObjInfo->m_data.spacing)
			{
				float z;
				if (IsPtInTriangle2D(i, j, pLocTri->m_V1, pLocTri->m_V2, pLocTri->m_V3, normal, &z))
				{
					Vector3 currPos(i, j, z);
					AddObjectToAddList(currPos, normal, pProcObjInfo, pLocTri);
					numObjectsAdded++;
					/*					pngEntityItem* pEntityItem = g_procObjMan.AddObject(currPos, normal, &pProcObjInfo->m_data, &pProcObjInfo->m_entityList, const_cast<CEntity*>(pLocTri->m_pParentEntity));

					if (pEntityItem)
					{
					numObjectsAdded++;
					pEntityItem->m_pParentTri = pLocTri;
					#if __BANK
					pEntityItem->triVert1[0] = pLocTri->m_V1.x;
					pEntityItem->triVert1[1] = pLocTri->m_V1.y;
					pEntityItem->triVert1[2] = pLocTri->m_V1.z;

					pEntityItem->triVert2[0] = pLocTri->m_V2.x;
					pEntityItem->triVert2[1] = pLocTri->m_V2.y;
					pEntityItem->triVert2[2] = pLocTri->m_V2.z;

					pEntityItem->triVert3[0] = pLocTri->m_V3.x;
					pEntityItem->triVert3[1] = pLocTri->m_V3.y;
					pEntityItem->triVert3[2] = pLocTri->m_V3.z;
					#endif // __BANK
					}	*/
				}
			}
		}
	}
	else
	{
		// randomly placed - calculate the area of the triangle
		float triArea = normal.Mag() * 0.5f;
		float numObjects = triArea * pProcObjInfo->m_data.density;

#if __BANK
		if (g_procObjMan.m_forceOneObjPerTri)
		{
			numObjects = 1.0f;
		}
#endif

		// set the seed for this triangle
		s32 storedSeed = 0;
#if __BANK
		if (pProcObjInfo->m_data.useSeed && g_procObjMan.m_ignoreSeeding==false)
#else
		if (pProcObjInfo->m_data.useSeed)
#endif
		{
			float seedF = (pLocTri->m_V1.x + pLocTri->m_V2.y + pLocTri->m_V3.z + pProcObjInfo->m_data.modelIndex) * pLocTri->m_V1.x * pProcObjInfo->m_data.modelIndex;
			//			float seedF = (pLocTri->m_V1.x + pLocTri->m_V2.y + pLocTri->m_V3.z) * pLocTri->m_V3.z;
			us32 seed = (us32)seedF;
			storedSeed = g_DrawRand.GetSeed();
			g_DrawRand.Reset(seed);
		}

		// try to place the objects
		while (numObjects>0)
		{
			float chance = 1.0f;
			if (numObjects<1.0f)
			{
				chance = g_DrawRand.GetRanged(0.0f, 1.0f);
			}

			if (chance<=numObjects)
			{
				float rand1 = g_DrawRand.GetRanged(0.0f, 1.0f);
				float rand2 = g_DrawRand.GetRanged(0.0f, 1.0f);

				if (rand1+rand2>1.0f)
				{
					rand1 = 1.0f-rand1;
					rand2 = 1.0f-rand2;
				}

				Assert(rand1+rand2<=1.0f);

				Vector3 pos = pLocTri->m_V1 + rand1*p1p2 + rand2*p1p3;

				normal.Normalize();
				AddObjectToAddList(pos, normal, pProcObjInfo, pLocTri);
				numObjectsAdded++;
				/*				pngEntityItem* pEntityItem = g_procObjMan.AddObject(pos, normal, &pProcObjInfo->m_data, &pProcObjInfo->m_entityList, const_cast<CEntity*>(pLocTri->m_pParentEntity));

				if (pEntityItem)
				{
				numObjectsAdded++;
				pEntityItem->m_pParentTri = pLocTri;
				#if __BANK
				pEntityItem->triVert1[0] = pLocTri->m_V1.x;
				pEntityItem->triVert1[1] = pLocTri->m_V1.y;
				pEntityItem->triVert1[2] = pLocTri->m_V1.z;

				pEntityItem->triVert2[0] = pLocTri->m_V2.x;
				pEntityItem->triVert2[1] = pLocTri->m_V2.y;
				pEntityItem->triVert2[2] = pLocTri->m_V2.z;

				pEntityItem->triVert3[0] = pLocTri->m_V3.x;
				pEntityItem->triVert3[1] = pLocTri->m_V3.y;
				pEntityItem->triVert3[2] = pLocTri->m_V3.z;
				#endif // __BANK
				}*/
			}

			numObjects -= 1.0f;
		}

#if __BANK
		if (pProcObjInfo->m_data.useSeed && g_procObjMan.m_ignoreSeeding==false)
#else
		if (pProcObjInfo->m_data.useSeed)
#endif
		{
			g_DrawRand.Reset(storedSeed);
		}
	}

#if __BANK
	if (g_procObjMan.m_forceOneObjPerTri)
	{
		Assert(numObjectsAdded==1);
	}
#endif

	return numObjectsAdded;
}

//
// name:		pngProcObjectMan::AddObjectToAddList
// description:
void				pngProcObjectMan::AddObjectToAddList			(Vector3::Param pos, Vector3::Param normal, pngProcObjInfo* pProcObjInfo, pngPlantLocTri* pLocTri)
{
	if (m_objectsToCreate.GetCount()<MAX_ENTITY_ITEMS)
	{
		ProcObjectCreationInfo info;
		info.pos = pos;
		info.normal = normal;
		info.pos.uw = (u32)pProcObjInfo;	
		//info.pProcObjInfo = pProcObjInfo;
		info.normal.uw = (u32)pLocTri;
		//info.pLocTri = pLocTri;
		m_objectsToCreate.Push(info);
	}
}

//
// name:		pngProcObjectMan::AddTriToRemoveList
// description:
void				pngProcObjectMan::AddTriToRemoveList			(pngPlantLocTri* pLocTri)
{
	ProcTriRemovalInfo info;
	info.pLocTri = pLocTri;
	info.procTagId = PGTAMATERIALMGR->UnpackProcId(pLocTri->m_nSurfaceType);
	m_trisToRemove.PushAndGrow(info);
	Assert(m_trisToRemove.GetCapacity()<=5*MAX_ENTITY_ITEMS);
}

//
// name:		pngProcObjectMan::ProcessAddAndRemovalLists
// description:	Process the list of entities to add and the list of triangles to remove
void				pngProcObjectMan::ProcessAddAndRemoveLists	()
{
	LockListAccess();

	// Process remove list
	// Process this first as triangle may have been reused and we don't want to remove objects we have just generated
	for(s32 i=0; i<m_trisToRemove.GetCount(); i++)
	{
		ProcessTriangleRemoved(m_trisToRemove[i].pLocTri, m_trisToRemove[i].procTagId);
	}

	// Process add list
	for(s32 i=0; i<m_objectsToCreate.GetCount(); i++)
	{
		Vector3& pos = m_objectsToCreate[i].pos;
		Vector3& normal = m_objectsToCreate[i].normal;
		pngProcObjInfo* pProcObjInfo = (pngProcObjInfo*)pos.uw;
		pngPlantLocTri* pLocTri = (pngPlantLocTri*)normal.uw;
		// 		pngProcObjInfo* pProcObjInfo = m_objectsToCreate[i].pProcObjInfo;
		// 		pngPlantLocTri* pLocTri = m_objectsToCreate[i].pLocTri;

		if(pLocTri->m_pParentEntity)
		{
			pngEntityItem* pEntityItem = AddObject(pos, normal, &pProcObjInfo->m_data, &pProcObjInfo->m_entityList, const_cast<CEntity*>(pLocTri->m_pParentEntity));

			if (pEntityItem)
			{
				pEntityItem->m_pParentTri = pLocTri;
#if __BANK
				pEntityItem->triVert1[0] = pLocTri->m_V1.x;
				pEntityItem->triVert1[1] = pLocTri->m_V1.y;
				pEntityItem->triVert1[2] = pLocTri->m_V1.z;

				pEntityItem->triVert2[0] = pLocTri->m_V2.x;
				pEntityItem->triVert2[1] = pLocTri->m_V2.y;
				pEntityItem->triVert2[2] = pLocTri->m_V2.z;

				pEntityItem->triVert3[0] = pLocTri->m_V3.x;
				pEntityItem->triVert3[1] = pLocTri->m_V3.y;
				pEntityItem->triVert3[2] = pLocTri->m_V3.z;
#endif // __BANK

			}
		}
	}

	// reset lists
	m_objectsToCreate.ResetCount();
	m_trisToRemove.ResetCount();

	UnlockListAccess();
}

void				pngProcObjectMan::LockListAccess ()
{
	listAccessToken.Lock();
}
void				pngProcObjectMan::UnlockListAccess ()
{
	listAccessToken.Unlock();
}

///////////////////////////////////////////////////////////////////////////////
//  AddObjects
///////////////////////////////////////////////////////////////////////////////

s32				pngProcObjectMan::AddObjects					(/*CEntity* pEntity, C2dEffect* p2dEffect, */s32 numObjsToCreate, phIntersection* pIntersections)
{
	// set up the data
	pngProcObjData procObjInfo;

#if 0
	CProcObjAttr* po = p2dEffect->GetProcObj();

	if (CModelInfo::GetModelInfoFromHashKey(po->objHash, &procObjInfo.modelIndex)==NULL)
	{
#if __ASSERT
		CBaseModelInfo* pModelInfo = CModelInfo::GetModelInfo(pEntity->GetModelIndex());
		Assert(pModelInfo);

		char txt[256];
		sprintf(txt, "Cannot find procedural object attached to %s", pModelInfo->GetModelName());
		Assertf(0, txt);
#endif // __ASSERT
		return 0;
	}
#endif // 0

	// we don't want to add tree objects in this way - make sure we aren't trying to
	CBaseModelInfo* pBaseModelInfo = CModelInfo::GetModelInfo(procObjInfo.modelIndex);
	if (pBaseModelInfo->GetModelType()!=MI_TYPE_BASE)
	{
		Warningf("Trying to add a procedural tree object on another object\n");
		Assert(0);
	}

	procObjInfo.spacing		= po->spacing;
	procObjInfo.density		= 1.0f/(procObjInfo.spacing*procObjInfo.spacing);
	procObjInfo.minDistSq	= 0.0f;
	procObjInfo.minXRot		= 0.0f;
	procObjInfo.maxXRot		= 0.0f;
	procObjInfo.minYRot		= 0.0f;
	procObjInfo.maxYRot		= 0.0f;
	procObjInfo.minZRot		= -180.0f;
	procObjInfo.maxZRot		= 180.0f;
	procObjInfo.minScale	= po->minScale;
	procObjInfo.maxScale	= po->maxScale;
	procObjInfo.minScaleZ	= po->minScaleZ;
	procObjInfo.maxScaleZ	= po->maxScaleZ;
	procObjInfo.zOffMin		= po->zOffsetMin;
	procObjInfo.zOffMax		= po->zOffsetMax;
	procObjInfo.alignObj	= po->isAligned;
	procObjInfo.useGrid		= false;
	procObjInfo.useSeed		= false;
	procObjInfo.isFloating	= false;

	// temp hack to half the density as artists are going wild with these!
	procObjInfo.density *= 0.5f;

	// create the objects
	s32 numObjectsAdded = 0;

	for (s32 i=0; i<numObjsToCreate; i++)
	{
		if (pIntersections[i].GetInstance())
		{
			pngEntityItem* pEntityItem = g_procObjMan.AddObject(pIntersections[i].GetPosition(), pIntersections[i].GetNormal(), &procObjInfo, &m_objectEntityList/*, pEntity*//*, 255*/);

			if (pEntityItem)
			{
				numObjectsAdded++;
				//pEntityItem->m_pParentEntity = pEntity;
#if __BANK
				pEntityItem->triVert1[0] = 0.0f;
				pEntityItem->triVert1[1] = 0.0f;
				pEntityItem->triVert1[2] = 0.0f;

				pEntityItem->triVert2[0] = 0.0f;
				pEntityItem->triVert2[1] = 0.0f;
				pEntityItem->triVert2[2] = 0.0f;

				pEntityItem->triVert3[0] = 0.0f;
				pEntityItem->triVert3[1] = 0.0f;
				pEntityItem->triVert3[2] = 0.0f;
#endif // __BANK

#if __DEV
				//pEntityItem->m_pData->m_nFlags.bIsEntityProcObject = true;
#endif //  __DEV
			}
		}
	}

	return numObjectsAdded;
}


///////////////////////////////////////////////////////////////////////////////
//  AddObject
///////////////////////////////////////////////////////////////////////////////

pngEntityItem*		pngProcObjectMan::AddObject					(const Vector3& pos, const Vector3& normal, pngProcObjData* pProcObjData, pngList* pList, /*CEntity* pParentEntity*//*, uint8 UNUSED_PARAM(lighting)*/)
{
#define NORMAL_Z_THRESH (0.97f)

	if (NetworkInterface::IsGameInProgress())
	{
		return NULL;
	}

	// try to get an object from the pool
	pngEntityItem* pEntityItem = (pngEntityItem*)g_procObjMan.GetEntityFromPool();
#if 0
	if (pEntityItem)
	{
		Assert(pEntityItem->m_pData==NULL);

		// if it needs aligned then check we have a spare matrix
		if (pProcObjData->alignObj && normal.z<NORMAL_Z_THRESH)
		{
			if (g_procObjMan.m_numAllocatedMatrices>=MAX_ALLOCATED_MATRICES)
			{
				// no spare matrices - don't create
				// 				Warningf("Cannot create procedural object aligned to triangle - no matrices left\n");
				g_procObjMan.ReturnEntityToPool(pEntityItem);
				return NULL;
			}
		}

		// we've got one - create the entity
		CBaseModelInfo* pBaseModelInfo = CModelInfo::GetModelInfo(pProcObjData->modelIndex);
		if (pBaseModelInfo==NULL)
		{
			g_procObjMan.ReturnEntityToPool(pEntityItem);
			return NULL;
		}

		us32 isTypeObject = pBaseModelInfo->GetIsTypeObject();

		if (pBaseModelInfo->GetModelType()==MI_TYPE_BASE)
		{
			if (isTypeObject)
			{
				// If we only have 50 objects left then don't create procedural objects
				if(CObject::GetPool()->GetNoOfFreeSpaces() < 50)
				{
					g_procObjMan.ReturnEntityToPool(pEntityItem);
					return NULL;
				}
				if (m_numProcObjObjects>=200)
				{
					g_procObjMan.ReturnEntityToPool(pEntityItem);
					return NULL;
				}
				// it's a game object - create as object and set up object specific data
				//			CPools::GetObjectPool().SetCanDealWithNoMemory(true);
				pEntityItem->m_pData = CObject::Create(pProcObjData->modelIndex, GAME_OBJECT, false);
				if (pEntityItem->m_pData == NULL)
				{
					//				CPools::GetObjectPool().SetCanDealWithNoMemory(false);
					g_procObjMan.ReturnEntityToPool(pEntityItem);
					return NULL;
				}

				m_numProcObjObjects++;
			}
			else
			{
				// If we only have 500 buildings left then don't create procedural objects
				if(CBuilding::GetPool()->GetNoOfFreeSpaces() < 500)
				{
					g_procObjMan.ReturnEntityToPool(pEntityItem);
					return NULL;
				}
				// it's not a game object - create as building and set up building specific data
				//			CPools::GetBuildingPool().SetCanDealWithNoMemory(true);
				pEntityItem->m_pData = rage_new CBuilding();
				if (pEntityItem->m_pData == NULL)
				{
					//				CPools::GetBuildingPool().SetCanDealWithNoMemory(false);
					g_procObjMan.ReturnEntityToPool(pEntityItem);
					return NULL;
				}
				pEntityItem->m_pData->SetModelIndex(pProcObjData->modelIndex);
				//			CPools::GetBuildingPool().SetCanDealWithNoMemory(false);
#if __BANK
				m_numProcObjBuildings++;
#endif
			}
		}

		// check that we've create the object ok
		if (pEntityItem->m_pData==NULL)
		{
			Assertf(0, "Cannot create procedural object\n");
			g_procObjMan.ReturnEntityToPool(pEntityItem);
			return NULL;
		}

		pEntityItem->m_pData->m_nFlags.bIsProcObject = true;
		if (pProcObjData->isFloating)
		{
			pEntityItem->m_pData->m_nFlags.bIsFloatingProcObject = true;
		}
		// procedural objects are considered unimportant when streaming
		pEntityItem->m_pData->m_nFlags.bUnimportantStream = true;

		// Force ambient scale use
		CBaseModelInfo *modelinfo = CModelInfo::GetModelInfo(pProcObjData->modelIndex);
		modelinfo->SetUseAmbientScale(true);

		// calculate the random info
		float scale  = g_DrawRand.GetRanged(pProcObjData->minScale,  pProcObjData->maxScale);
		float scaleZ = g_DrawRand.GetRanged(pProcObjData->minScaleZ, pProcObjData->maxScaleZ);
		float xRot   = g_DrawRand.GetRanged(pProcObjData->minXRot,   pProcObjData->maxXRot);
		float yRot   = g_DrawRand.GetRanged(pProcObjData->minYRot,   pProcObjData->maxYRot);
		float zRot   = g_DrawRand.GetRanged(pProcObjData->minZRot,   pProcObjData->maxZRot);
		float zOff	 = g_DrawRand.GetRanged(pProcObjData->zOffMin,   pProcObjData->zOffMax);

		// calculate the z pos
		Vector3 bbMin = modelinfo->GetBoundingBoxMin();
		if (pProcObjData->alignObj && normal.z<NORMAL_Z_THRESH)
		{
			Assert(g_procObjMan.m_numAllocatedMatrices<MAX_ALLOCATED_MATRICES);

			// this object wants aligned to the normal
			Matrix34 objMat;
			Vector3 xVec;
			xVec.x = CMaths::Cos(zRot);
			xVec.y = CMaths::Sin(zRot);
			xVec.z = 0.0f;
			Vector3 yAxis;
			yAxis.Cross(normal, xVec);
			yAxis.Normalize();
			Vector3 xAxis;
			xAxis.Cross(yAxis, normal);
			xAxis.Normalize();

			objMat.a = xAxis;
			objMat.b = yAxis;
			objMat.c = normal;
			objMat.d = pos + (normal*(zOff-bbMin.z));

			pEntityItem->m_pData->AllocateStaticMatrix();
			pEntityItem->m_pData->SetMatrix(objMat);

			pEntityItem->m_allocatedMatrix = true;
			g_procObjMan.m_numAllocatedMatrices++;
		}
		else if (xRot>0.0f || yRot>0.0f)
		{
			pEntityItem->m_pData->AllocateStaticMatrix();
			pEntityItem->m_pData->SetPosition(Vector3(pos.x, pos.y, pos.z - bbMin.z + zOff));
			pEntityItem->m_pData->SetOrientation(xRot, yRot, zRot);

			pEntityItem->m_allocatedMatrix = true;
			g_procObjMan.m_numAllocatedMatrices++;
		}
		else
		{
			pEntityItem->m_pData->SetPosition(Vector3(pos.x, pos.y, pos.z - bbMin.z + zOff));
			pEntityItem->m_pData->SetOrientation(xRot, yRot, zRot);

			pEntityItem->m_allocatedMatrix = false;
		}

		// scale static objects if required
		if (scale!=1.0f || scaleZ!=1.0f)
		{
			if (!isTypeObject)
			{
				if (g_procObjMan.m_numAllocatedMatrices<MAX_ALLOCATED_MATRICES)
				{
					Matrix34& objMat = pEntityItem->m_pData->GetMatrix();
					objMat.Scale(scale, scale, scaleZ);
					float zOffset = (bbMin.z*scaleZ) - bbMin.z;
					objMat.Translate(0.0f, 0.0f, -zOffset);

					// we need to allocate this matrix if it hasn't been already
					if (pEntityItem->m_allocatedMatrix==false)
					{
						pEntityItem->m_pData->AllocateStaticMatrix();
						pEntityItem->m_pData->SetMatrix(objMat);
						pEntityItem->m_pData->m_nFlags.bUsesFullMatrix = TRUE;

						pEntityItem->m_allocatedMatrix = true;
						g_procObjMan.m_numAllocatedMatrices++;
					}
				}
			}
			else
			{
				Warningf("WARNING: Trying to scale a procedurally generated game object\n");
			}
		}

// 		pEntityItem->m_pData->m_nFlags.bDontCastShadows = true;
// 		pEntityItem->m_pData->m_nFlags.bUsesScreenDoorFade = true;

		// add to the world and to our list
// 		if (pParentEntity->GetModelIndex() > -1){
// 			pEntityItem->m_pData->CopyInteriorDataFrom(pParentEntity);
// 		} else {
// 			pEntityItem->m_pData->ForceWorldInsertToOutside();
// 		}
// 		CGameWorld::Add(pEntityItem->m_pData);

		pList->AddItem(pEntityItem);

		return pEntityItem;
	}

	return NULL;
#else
	return pEntityItem;
#endif // 0
}


///////////////////////////////////////////////////////////////////////////////
//  RemoveObject
///////////////////////////////////////////////////////////////////////////////

void				pngProcObjectMan::RemoveObject				(pngEntityItem* pEntityItem, pngList* pList)
{
#if 0
#if __DEV
	m_removingProcObject = true;
#endif
	Assert(pEntityItem->m_pData);

	if (pEntityItem->m_pParentEntity)
	{
		pEntityItem->m_pParentEntity->m_nFlags.bCreatedProcObjects = false;
	}

	if (pEntityItem->m_pData->GetIsTypeBuilding())
	{
#if __BANK
		m_numProcObjBuildings--;
#endif
	}
	else
	{
		Assert(pEntityItem->m_pData->GetIsTypeObject());
		m_numProcObjObjects--;
	}

	CGameWorld::Remove(pEntityItem->m_pData);

#if __DEV
	CObject::bDeletingProcObject = true;
#endif

	delete pEntityItem->m_pData;

#if __DEV
	CObject::bDeletingProcObject = false;
#endif

	pEntityItem->m_pData = NULL;

	if (pEntityItem->m_allocatedMatrix)
	{
		m_numAllocatedMatrices--;
		pEntityItem->m_allocatedMatrix = false;
	}
#endif // 0

#if __ASSERT
	us32 numItemsInList = pList->GetNumItems();
#endif
	pList->RemoveItem(pEntityItem);
	Assert(numItemsInList-1 == pList->GetNumItems());

	g_procObjMan.ReturnEntityToPool(pEntityItem);

#if __DEV
	m_removingProcObject = false;
#endif
}


///////////////////////////////////////////////////////////////////////////////
//  GetEntityFromPool
///////////////////////////////////////////////////////////////////////////////

pngEntityItem*		pngProcObjectMan::GetEntityFromPool			()
{
	pngEntityItem* pEntity = (pngEntityItem*)m_entityPool.RemoveHead();

	if (pEntity)
	{
		Assert(pEntity->m_pData == NULL);
		Assert(pEntity->m_pParentTri == NULL);
		Assert(pEntity->m_pParentEntity == NULL);
		Assert(pEntity->m_allocatedMatrix == false);
	}

	return pEntity;
}


///////////////////////////////////////////////////////////////////////////////
//  ReturnEntityToPool
///////////////////////////////////////////////////////////////////////////////

void				pngProcObjectMan::ReturnEntityToPool			(pngEntityItem* pEntityItem)
{
	pEntityItem->m_pData = NULL;
	pEntityItem->m_pParentTri = NULL;
	pEntityItem->m_pParentEntity = NULL;
	m_entityPool.AddItem(pEntityItem);
}

///////////////////////////////////////////////////////////////////////////////
//  GetNumActiveObjects
///////////////////////////////////////////////////////////////////////////////

s32				pngProcObjectMan::GetNumActiveObjects			()
{
	return ( MAX_ENTITY_ITEMS - this->m_entityPool.GetNumItems() );
}


///////////////////////////////////////////////////////////////////////////////
//  IsPtInTriangle2D
///////////////////////////////////////////////////////////////////////////////

bool				pngProcObjectMan::IsPtInTriangle2D			(float x, float y, const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& normal, float* z)
{
	// calc the vectors from the point to each vertex
	float dot1 = (v1.x-x)*(v2.y-v1.y) + (v1.y-y)*(v1.x-v2.x);
	float dot2 = (v2.x-x)*(v3.y-v2.y) + (v2.y-y)*(v2.x-v3.x);
	float dot3 = (v3.x-x)*(v1.y-v3.y) + (v3.y-y)*(v3.x-v1.x);

	// check if the point is inside the 2d triangle
	if (dot1>=0.0f && dot2>=0.0f && dot3>=0.0f)
	{
		// we're inside - calculate the z of the point
		float d = -(normal.x*v1.x + normal.y*v1.y + normal.z*v1.z);
		*z = (-normal.x*x - normal.y*y - d) / normal.z;

		return true;
	}

	// we're outside
	return false;
}

#if __DEV

///////////////////////////////////////////////////////////////////////////////
//  IsAllowedToDelete
///////////////////////////////////////////////////////////////////////////////

bool				pngProcObjectMan::IsAllowedToDelete()
{
	return m_removingProcObject;
}

#endif





#if __BANK

///////////////////////////////////////////////////////////////////////////////
//  RenderDebug
///////////////////////////////////////////////////////////////////////////////

void				pngProcObjectMan::RenderDebug				()
{
	if (m_renderDebugPolys)
	{
		for (s32 i=0; i<g_procInfo.m_numProcObjInfos; i++)
		{
			grcState::Default();
			grcState::Default();

			pngEntityItem* pEntityItem = (pngEntityItem*)g_procInfo.m_procObjInfos[i].m_entityList.GetHead();

			while (pEntityItem)
			{
				if( pEntityItem->m_pData )
				{
					DebugDraw::Axis(pEntityItem->m_pData->GetMatrix(),0.5f);
				}

				if( pEntityItem->m_pParentEntity )
				{
					DebugDraw::Axis(pEntityItem->m_pParentEntity->GetMatrix(),1.0f);
				}

				grcBegin(drawTris, 3);

				float seed = pEntityItem->triVert1[0] + pEntityItem->triVert1[1] + pEntityItem->triVert1[2] +
					pEntityItem->triVert2[0] + pEntityItem->triVert2[1] + pEntityItem->triVert2[2] +
					pEntityItem->triVert3[0] + pEntityItem->triVert3[1] + pEntityItem->triVert3[2];
				seed -= static_cast<s32>(seed);

				uint8 col = static_cast<uint8>(255*seed);

				grcColor(Color32(col, col, 128, 255));

				grcVertex3f(pEntityItem->triVert1[0], pEntityItem->triVert1[1], pEntityItem->triVert1[2] + 0.05f);
				grcVertex3f(pEntityItem->triVert2[0], pEntityItem->triVert2[1], pEntityItem->triVert2[2] + 0.05f);
				grcVertex3f(pEntityItem->triVert3[0], pEntityItem->triVert3[1], pEntityItem->triVert3[2] + 0.05f);

				grcEnd();

				pEntityItem = (pngEntityItem*)g_procInfo.m_procObjInfos[i].m_entityList.GetNext(pEntityItem);
			}
		}
	}

	// render debug entity zone
	if (m_renderDebugZones)
	{
		RenderDebugEntityZones();
	}

	// render debug segments
	//	Color32 col(1.0f, 1.0f, 0.0f, 1.0f);
	//	for (s32 i=0; i<MAX_SEGMENTS; i++)
	//	{
	//		DebugDraw::Line(m_segments[i].A, m_segments[i].B, col, col);
	//	}
}


///////////////////////////////////////////////////////////////////////////////
//  RenderDebugEntityZones
///////////////////////////////////////////////////////////////////////////////

void				pngProcObjectMan::RenderDebugEntityZones		()
{
	grcState::Default();
	grcWorldMtx(M34_IDENTITY);
	grcBindTexture(NULL);
	

	// get all of the insts within 15m of the camera
	phIterator it;
	it.InitCull_Sphere(camInterface::GetPos(), 15.0f);
	it.SetStateIncludeFlags(phLevelBase::STATE_FLAGS_ALL);
	u16 curObjectLevelIndex = CPhysics::GetLevel()->GetFirstCulledObject(it);
	while (curObjectLevelIndex != phInst::INVALID_INDEX)
	{
		phInst& culledInst = *CPhysics::GetLevel()->GetInstance(curObjectLevelIndex);

		// get the entity from the inst
		CEntity* pEntity = CPhysics::GetEntityFromInst(&culledInst);
#if __DEBUG
		Assert(pEntity);
#endif
		if (pEntity && pEntity->GetModelIndex()>-1)
		{
			// check if this object has a procedural object 2d effect
			CBaseModelInfo* pModelInfo = CModelInfo::GetModelInfo(pEntity->GetModelIndex());
			s32 num2dEffects = pModelInfo->GetNum2dEffects();

			for (s32 i=0; i<num2dEffects; i++)
			{
				C2dEffect* pEffect = pModelInfo->Get2dEffect(i);
				if (pEffect->GetType() == ET_PROCEDURALOBJECTS)
				{
					CProcObjAttr* po = pEffect->GetProcObj();

					Vector3 PosSrc;
					pEffect->GetPos(PosSrc);
					Vector3 pos;// = pEntity->GetMatrix() * PosSrc;
					pEntity->GetMatrix().Transform(PosSrc, pos);

					grcColor(Color32(255, 255, 0, 255));
					grcDrawCircle(po->radiusOuter, pos, XAXIS, YAXIS, 20, false, true);

					pos.z += 0.02f;
					grcColor(Color32(255, 0, 0, 255));
					grcDrawCircle(po->radiusInner, pos, XAXIS, YAXIS, 20, false, true);
				}
			}
		}

		curObjectLevelIndex = CPhysics::GetLevel()->GetNextCulledObject(it);
	}
}


///////////////////////////////////////////////////////////////////////////////
//  ReloadData
///////////////////////////////////////////////////////////////////////////////

void				pngProcObjectMan::ReloadData					()
{
	// create a list of any triangles currently used
#define MAX_STORED_TRIS (1024)
	s32 numStoredTris = 0;
	pngPlantLocTri* pStoredTris[MAX_STORED_TRIS];

	for (s32 i=0; i<g_procInfo.m_numProcObjInfos; i++)
	{
		pngEntityItem* pEntityItem = (pngEntityItem*)g_procInfo.m_procObjInfos[i].m_entityList.GetHead();

		while (pEntityItem)
		{
			// search for this tri in the list already
			bool foundInList = false;
			for (s32 j=0; j<numStoredTris; j++)
			{
				if (pEntityItem->m_pParentTri == pStoredTris[j])
				{
					foundInList = true;
					break;
				}
			}

			// add to the list if it wasn't in there already
			if (foundInList==false)
			{
				Assert(numStoredTris<MAX_STORED_TRIS);
				pStoredTris[numStoredTris++] = pEntityItem->m_pParentTri;
			}

			pEntityItem = (pngEntityItem*)g_procInfo.m_procObjInfos[i].m_entityList.GetNext(pEntityItem);
		}
	}

	g_procObjMan.ShutdownLevel(false);
	g_procObjMan.InitLevel();

	g_procInfo.InitLevel();

	// restore any triangles that were populated previously
	for (s32 i=0; i<numStoredTris; i++)
	{
		g_procObjMan.ProcessTriangleAdded(pStoredTris[i]);
	}
}

} // namespace rage

#endif // __BANK

#endif // 0