//
// plantsgrass/plantsmgrspu.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#if 0
#if __SPU

// Rage:
#include "system/taskheaderspu.h"
#include "basetypes.h"
#include "atl\bitset.h"
#include "phbound/boundgeom.h"
#include "physics/inst.h"

// Game:
#include "renderer\Renderer.h"			// JW- had to move this up here to get a successful compile...

#include "math/random.cpp"
#include "maths/maths.h"
#include "maths/Maths.h"
#include "maths/vector.h"
#include "maths/Vector.h"
#include "Objects/ProceduralInfo.h"
#include "Objects/ProcObjects.h"
#include "Renderer/PlantsMgr.h"
#include "scene/CEntity.h"
#include "system/dmaspu.h"


mthRandom* g_pDrawRand;
#define g_DrawRand (*g_pDrawRand)

pngPlantMgr* g_pPlantMgr;
#define gPlantMgr (*g_pPlantMgr)

CProceduralInfo* g_pProcInfo;
#define g_procInfo (*g_pProcInfo)

CPlantsMgrUpdateSPU* g_jobParams;
ProcObjectCreationInfo* g_pAddList;
ProcObjectCreationInfo* g_pAddListEnd;
ProcTriRemovalInfo* g_pRemoveList;
ProcTriRemovalInfo* g_pRemoveListEnd;
CPlantsMgrUpdateSPU::CResultSize* g_pResultSize;

// don't add objects if there are less than this many free slots in the AddList
static const u32 THRESHOLD_DONT_ADD_OBJECTS = 0x300;

inline int UnpackProcId(phMaterialMgr::Id packedId)
{
	return ((packedId>>8) & 0xff);
}

bool IsPtInTriangle2D(float x, float y, const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& normal, float* z)
{
	// calc the vectors from the point to each vertex
	float dot1 = (v1.x-x)*(v2.y-v1.y) + (v1.y-y)*(v1.x-v2.x);
	float dot2 = (v2.x-x)*(v3.y-v2.y) + (v2.y-y)*(v2.x-v3.x);
	float dot3 = (v3.x-x)*(v1.y-v3.y) + (v3.y-y)*(v3.x-v1.x);

	// check if the point is inside the 2d triangle
	if (dot1>=0.0f && dot2>=0.0f && dot3>=0.0f)
	{
		// we're inside - calculate the z of the point
		float d = -(normal.x*v1.x + normal.y*v1.y + normal.z*v1.z);
		*z = (-normal.x*x - normal.y*y - d) / normal.z;

		return true;
	}

	// we're outside
	return false;
}

void AddObjectToAddList(Vector3 pos, Vector3 normal, CProcObjInfo* pProcObjInfo, pngPlantLocTri* pLocTri)
{
	AssertVerify(g_jobParams->m_maxAdd--);
	ProcObjectCreationInfo& info = *g_pAddList++;
	pos.uw = ((u8*)pProcObjInfo - (u8*)&g_procInfo.m_procObjInfos[0]);
	normal.uw = (u32)((u8*)pLocTri - (u8*)&gPlantMgr.m_LocTrisTab[0] + (u8*)&g_jobParams->m_pPlantsMgr->m_LocTrisTab[0]);
	info.pos = pos;
	info.normal = normal;
	//Displayf("AddObjectToAddList (%f %f %f) (%f %f %f) %p %p", info.pos.x, info.pos.y, info.pos.z, info.normal.x, info.normal.y, info.normal.z, pProcObjInfo, pLocTri);
	if (g_pAddList == g_pAddListEnd)
	{
		// handle list overflow
		g_pResultSize->m_numAdd += g_jobParams->m_addBufSize;
		g_pAddList -= g_jobParams->m_addBufSize;
		DmaLargePut(g_pAddList, (u64)g_jobParams->m_pAddList, (u8*)g_pAddListEnd - (u8*)g_pAddList);
		g_jobParams->m_pAddList += g_jobParams->m_addBufSize;
		DmaWait();
	}
}

int AddObjects(CProcObjInfo* pProcObjInfo, pngPlantLocTri* pLocTri)
{
	//Displayf("CProcObjectMan::AddObjects %p %p", pProcObjInfo, pLocTri);

	// check if the triangle is out of range
	float distTriToCamSq = (g_jobParams->m_camPos - pLocTri->m_Center).Mag2();

	if (distTriToCamSq<pProcObjInfo->m_data.minDistSq)
	{
#if __BANK
		if (!g_procObjMan.m_ignoreMinDist)
		{
			return 0;
		}
#else // __BANK
		return 0;
#endif // __BANK
	}

	// get the vectors along the sides of the triangle
	Vector3 p1p2 = pLocTri->m_V2-pLocTri->m_V1;
	Vector3 p2p3 = pLocTri->m_V3-pLocTri->m_V2;
	Vector3 p1p3 = pLocTri->m_V3-pLocTri->m_V1;
	Vector3 normal;
	normal.Cross(p1p2, p1p3);

	// keep track of how many objects are added
	int numObjectsAdded = 0;

	// check if they should be placed in a regular grid formation
	if (pProcObjInfo->m_data.useGrid)
	{
		// placed in regular grid - find the min and max axis aligned bounding box of this triangle
		float minX = Min(pLocTri->m_V1.x, pLocTri->m_V2.x); minX = Min(minX, pLocTri->m_V3.x);
		float minY = Min(pLocTri->m_V1.y, pLocTri->m_V2.y);	minY = Min(minY, pLocTri->m_V3.y);
		float maxX = Max(pLocTri->m_V1.x, pLocTri->m_V2.x);	maxX = Max(maxX, pLocTri->m_V3.x);
		float maxY = Max(pLocTri->m_V1.y, pLocTri->m_V2.y);	maxY = Max(maxY, pLocTri->m_V3.y);

		// calculate the grid start and end loops
		float startX = (int)(minX/pProcObjInfo->m_data.spacing)*pProcObjInfo->m_data.spacing;
		float endX =   ((int)(maxX/pProcObjInfo->m_data.spacing)+1)*pProcObjInfo->m_data.spacing;
		float startY = (int)(minY/pProcObjInfo->m_data.spacing)*pProcObjInfo->m_data.spacing;
		float endY =   ((int)(maxY/pProcObjInfo->m_data.spacing)+1)*pProcObjInfo->m_data.spacing;

		normal.Normalize();

		for (float i=startX; i<endX; i+=pProcObjInfo->m_data.spacing)
		{
			for (float j=startY; j<endY; j+=pProcObjInfo->m_data.spacing)
			{
				float z;
				if (IsPtInTriangle2D(i, j, pLocTri->m_V1, pLocTri->m_V2, pLocTri->m_V3, normal, &z))
				{
					Vector3 currPos(i, j, z);
					AddObjectToAddList(currPos, normal, pProcObjInfo, pLocTri);
					numObjectsAdded++;
				}
			}
		}
	}
	else
	{
		// randomly placed - calculate the area of the triangle
		float triArea = normal.Mag() * 0.5f;
		float numObjects = triArea * pProcObjInfo->m_data.density;

#if __BANK
		if (g_procObjMan.m_forceOneObjPerTri)
		{
			numObjects = 1.0f;
		}
#endif

		// set the seed for this triangle
		int storedSeed = 0;
#if __BANK
		if (pProcObjInfo->m_data.useSeed && g_procObjMan.m_ignoreSeeding==false)
#else
		if (pProcObjInfo->m_data.useSeed)
#endif
		{
			float seedF = (pLocTri->m_V1.x + pLocTri->m_V2.y + pLocTri->m_V3.z + pProcObjInfo->m_data.modelIndex) * pLocTri->m_V1.x * pProcObjInfo->m_data.modelIndex;
			u32 seed = (u32)seedF;
			storedSeed = g_DrawRand.GetSeed();
			g_DrawRand.Reset(seed);
		}

		// try to place the objects
		while (numObjects>0)
		{
			float chance = 1.0f;
			if (numObjects<1.0f)
			{
				chance = g_DrawRand.GetRanged(0.0f, 1.0f);
			}

			if (chance<=numObjects)
			{
				float rand1 = g_DrawRand.GetRanged(0.0f, 1.0f);
				float rand2 = g_DrawRand.GetRanged(0.0f, 1.0f);

				if (rand1+rand2>1.0f)
				{
					rand1 = 1.0f-rand1;
					rand2 = 1.0f-rand2;
				}

				Assert(rand1+rand2<=1.0f);

				Vector3 pos = pLocTri->m_V1 + rand1*p1p2 + rand2*p1p3;

				normal.Normalize();
				AddObjectToAddList(pos, normal, pProcObjInfo, pLocTri);
				numObjectsAdded++;
			}

			numObjects -= 1.0f;
		}

#if __BANK
		if (pProcObjInfo->m_data.useSeed && g_procObjMan.m_ignoreSeeding==false)
#else
		if (pProcObjInfo->m_data.useSeed)
#endif
		{
			g_DrawRand.Reset(storedSeed);
		}
	}

#if __BANK
	if (g_procObjMan.m_forceOneObjPerTri)
	{
		Assert(numObjectsAdded==1);
	}
#endif

	return numObjectsAdded;
}

int ProcessTriangleAdded(pngPlantLocTri* pLocTri)
{
	Assert(pLocTri);
	int numObjectsAdded = 0;

	if (g_jobParams->m_maxAdd < THRESHOLD_DONT_ADD_OBJECTS)
		return 0;

	// add an object here
	int procTagId = UnpackProcId(pLocTri->m_nSurfaceType);
	int procObjInfoIndex = g_procInfo.m_procTagTable[procTagId].procObjIndex;
	Assert(g_procInfo.m_procObjInfos[procObjInfoIndex].m_procTagId==procTagId);

	while (g_procInfo.m_procObjInfos[procObjInfoIndex].m_procTagId==procTagId && procObjInfoIndex<g_procInfo.m_numProcObjInfos)
	{
		numObjectsAdded += AddObjects(&g_procInfo.m_procObjInfos[procObjInfoIndex], pLocTri);
		procObjInfoIndex++;
	}

	// assert if bail out threshold is too low for this triangle
	Assert(numObjectsAdded <= (int)THRESHOLD_DONT_ADD_OBJECTS);

	return numObjectsAdded;
}

void AddTriToRemoveList(pngPlantLocTri* pLocTri)
{
	Assert(g_jobParams->m_maxRemove--);
	ProcTriRemovalInfo& info = *g_pRemoveList++;
	info.pLocTri = (pngPlantLocTri*)((u8*)pLocTri - (u8*)&gPlantMgr.m_LocTrisTab[0] + (u8*)&g_jobParams->m_pPlantsMgr->m_LocTrisTab[0]);
	info.procTagId = UnpackProcId(pLocTri->m_nSurfaceType);
	if (g_pRemoveList == g_pRemoveListEnd)
	{
		// handle list overflow
		g_pResultSize->m_numRemove += g_jobParams->m_removeBufSize;
		g_pRemoveList -= g_jobParams->m_removeBufSize;
		DmaLargePut(g_pRemoveList, (u64)g_jobParams->m_pRemoveList, (u8*)g_pRemoveListEnd - (u8*)g_pRemoveList);
		g_jobParams->m_pRemoveList += g_jobParams->m_removeBufSize;
		DmaWait();
	}
}

pngPlantLocTri* pngPlantMgr::MoveLocTriToList(u16*ppCurrentList, u16*ppNewList, pngPlantLocTri *pTri)
{
	Assertf(*ppCurrentList, "CPlant::MoveLocTriToList(): m_CurrentList==NULL!");

	// First - Cut out of old list
	if(!pTri->m_PrevTri)
	{
		// if at head of old list
		*ppCurrentList = pTri->m_NextTri;
		if(*ppCurrentList)	// might have been only 1 entry in list
			m_LocTrisTab[*ppCurrentList-1].m_PrevTri = 0;
	}
	else if(!pTri->m_NextTri)
	{
		// if at tail of old list
		m_LocTrisTab[pTri->m_PrevTri-1].m_NextTri = 0;
	}
	else
	{	// else if in middle of old list
		m_LocTrisTab[pTri->m_NextTri-1].m_PrevTri = pTri->m_PrevTri;
		m_LocTrisTab[pTri->m_PrevTri-1].m_NextTri = pTri->m_NextTri;
	}

	// Second - Insert at start of new list
	pTri->m_NextTri	= *ppNewList;
	pTri->m_PrevTri	= 0;
	*ppNewList = pTri - m_LocTrisTab + 1;
	
	if(pTri->m_NextTri)
		m_LocTrisTab[pTri->m_NextTri-1].m_PrevTri = *ppNewList;

	return(pTri);

}// end of pngPlantMgr::MoveLocTriToList()...

void pngPlantLocTri::Release()
{
	m_triArea = 0.0f;
	if (m_bCreatedObjects)
	{
		AddTriToRemoveList(this);
	}

	if (m_bCreatesObjects && !m_bCreatesPlants)
	{
		gPlantMgr.MoveLocTriToList(&gPlantMgr.m_CloseLocTriListHead[3], &gPlantMgr.m_UnusedLocTriListHead, this);
		m_nSurfaceType			= 0xFE;		// simple tag to show who released this
	}
	else
	{
		int procTagId = UnpackProcId(m_nSurfaceType);
		int plantInfoIndex = g_procInfo.m_procTagTable[procTagId].plantIndex;
		const u32 slotID = g_procInfo.m_plantInfos[plantInfoIndex].m_nPlantSlotID;
		gPlantMgr.MoveLocTriToList(&gPlantMgr.m_CloseLocTriListHead[slotID], &gPlantMgr.m_UnusedLocTriListHead, this);
		m_nSurfaceType			= 0xFF;		// simple tag to show who released this
	}
	
	m_bCreatesPlants = false;
	m_bCreatesObjects = false;
	m_bCreatedObjects = false;
	m_pParentEntity = NULL;
}

static float _CalcLocTriArea(pngPlantLocTri *pLocTri)
{
	const Vector3 V1(pLocTri->m_V2 - pLocTri->m_V1);
	const Vector3 V2(pLocTri->m_V3 - pLocTri->m_V1);
	const Vector3 c = CrossProduct(V1, V2);
	const float area2 = c.Mag() * 0.5f;

	return(area2);
}

pngPlantLocTri* pngPlantLocTri::Add(const Vector3& v1, const Vector3& v2, const Vector3& v3, phMaterialMgr::Id nSurfaceType, u8 nLighting, bool bCreatesPlants, bool bCreatesObjects, CEntity* pParentEntity)
{
	Assert(pParentEntity);
	Assert(gPlantMgr.m_UnusedLocTriListHead);

	m_V1 = v1;
	m_V2 = v2;
	m_V3 = v3;

	m_nSurfaceType	= nSurfaceType;
	m_nLighting		= nLighting;
	
	m_bCreatesPlants	= bCreatesPlants;
	m_bCreatesObjects	= bCreatesObjects;
	m_bCreatedObjects	= false;

	const float _1_3 = 1.0f / 3.0f;
	m_Center = (v1) + (v2) + (v3);
	m_Center *= _1_3;

	Vector3 vecSphRadius(m_Center - m_V1);
	m_SphereRadius = vecSphRadius.Mag();
	m_SphereRadius *= 1.75f;	// make sphere radius 75% bigger (for better sphere visibility detection)
	m_pParentEntity = pParentEntity;


	// FIXME():
	// test: localized per-vertex ground color(RGB) + scale(A):
	this->m_GroundColorV1	= g_jobParams->m_defaultGroundColor;
	this->m_GroundColorV2	= g_jobParams->m_defaultGroundColor;
	this->m_GroundColorV3	= g_jobParams->m_defaultGroundColor;

	if(m_bCreatesObjects && !m_bCreatesPlants)
	{
		// deal with ONLY procedurally generated objects
		gPlantMgr.MoveLocTriToList(&gPlantMgr.m_UnusedLocTriListHead, &gPlantMgr.m_CloseLocTriListHead[3], this);
		return(this);
	}
	else
	{		
		int procTagId = UnpackProcId(nSurfaceType);
		int plantInfoIndex = g_procInfo.m_procTagTable[procTagId].plantIndex;

		const float triArea	= _CalcLocTriArea(this);

		const float numPlants = triArea * g_procInfo.m_plantInfos[plantInfoIndex].m_fDensity;

		// only update the list if Density > 0.0f:
		if (numPlants>0.05f)
		{
			m_triArea = triArea;
			m_Seed = g_DrawRand.GetInt();

			const u32 slotID = g_procInfo.m_plantInfos[plantInfoIndex].m_nPlantSlotID;
			gPlantMgr.MoveLocTriToList(&gPlantMgr.m_UnusedLocTriListHead, &gPlantMgr.m_CloseLocTriListHead[slotID], this);
			return(this);
		}
		else if (m_bCreatesObjects)
		{
			m_bCreatesPlants = false;
			gPlantMgr.MoveLocTriToList(&gPlantMgr.m_UnusedLocTriListHead, &gPlantMgr.m_CloseLocTriListHead[3], this);
			return(this);
		}

		return(NULL);
	}
	
}// end of pngPlantLocTri::Add()...

template<class T>
inline T* FetchSmallUnaligned(void* pBuf, const T* pSrc, u32 tag=0)
{
	DmaGet(pBuf, ((u64)pSrc)&~15, 32, tag);
	return (T*)(((u8*)pBuf)+(((u64)pSrc)&15));
}

void pngPlantMgr::_ProcessEntryCollisionData_GenerateNewLocTri(int indexLTA, 
					pngPlantColBoundEntry *pEntry, phBoundGeometry *pBound, const Vector3& camPos,
					const phPolygon& poly, const int polyIndex0, const int polyIndex1, const int polyIndex2,
					const bool bCreatesPlants, const bool bCreatesObjects, phMaterialMgr::Id actualMaterialID)
{
	// generate new LocTri for this polygon (if necessary):
	Vector3 tv[3];
	const phPolygon::Index vIndex0 = poly.GetVertexIndex(/*0*/polyIndex0);
	const phPolygon::Index vIndex1 = poly.GetVertexIndex(/*1*/polyIndex1);
	const phPolygon::Index vIndex2 = poly.GetVertexIndex(/*2*/polyIndex2);
	const CompressedVertexType* pVerts = pBound->GetCompressedVertexPointer();
	qword buf[3][2];
	const CompressedVertexType* v0 = FetchSmallUnaligned(buf[0], &pVerts[vIndex0*3]);
	const CompressedVertexType* v1 = FetchSmallUnaligned(buf[1], &pVerts[vIndex1*3]);
	const CompressedVertexType* v2 = FetchSmallUnaligned(buf[2], &pVerts[vIndex2*3]);
	DmaWait();
	tv[0] = pBound->DecompressVertex(v0);
	tv[1] = pBound->DecompressVertex(v1);
	tv[2] = pBound->DecompressVertex(v2);

	// transform points if required
	if(pEntry->GetPhysInst())
	{
		if(!pEntry->m_bBoundMatIdentity)
		{
			pEntry->m_BoundMat.Transform(tv[0]);
			pEntry->m_BoundMat.Transform(tv[1]);
			pEntry->m_BoundMat.Transform(tv[2]);
		}
	}
	Vector3 colCenter((tv[0]+tv[1]+tv[2]) / 3.0f);

#ifdef USE_COLLISION_2D_DIST
	Vector3 _colDistV1 = camPos - tv[0];
	Vector3 _colDistV2 = camPos - tv[1];
	Vector3 _colDistV3 = camPos - tv[2];
	Vector3 _colDistV4 = camPos - colCenter;
	Vector2 colDistV1(_colDistV1.x, _colDistV1.y);
	Vector2 colDistV2(_colDistV2.x, _colDistV2.y);
	Vector2 colDistV3(_colDistV3.x, _colDistV3.y);
	Vector2 colDistV4(_colDistV4.x, _colDistV4.y);
#else
	Vector3 colDistV1 = camPos - tv[0];
	Vector3 colDistV2 = camPos - tv[1];
	Vector3 colDistV3 = camPos - tv[2];
	Vector3 colDistV4 = camPos - colCenter;
#endif
	const float colDistSqr1 = colDistV1.Mag2();
	const float colDistSqr2 = colDistV2.Mag2();
	const float colDistSqr3 = colDistV3.Mag2();
	const float colDistSqr4 = colDistV4.Mag2();	

	// calc middle points distances (distance between camera and tri edges' middle points):
#ifdef USE_COLLISION_2D_DIST
	const Vector3 _colDistMV12( (_colDistV1+_colDistV2) * 0.5f );	// V1-2
	const Vector3 _colDistMV23( (_colDistV2+_colDistV3) * 0.5f );	// V2-3
	const Vector3 _colDistMV31( (_colDistV3+_colDistV1) * 0.5f );	// V3-1
	const Vector2 colDistMV12(_colDistMV12.x, _colDistMV12.y);
	const Vector2 colDistMV23(_colDistMV23.x, _colDistMV23.y);
	const Vector2 colDistMV31(_colDistMV31.x, _colDistMV31.y);
#else
	const Vector3 colDistMV12( (colDistV1+colDistV2) * 0.5f );	// V1-2
	const Vector3 colDistMV23( (colDistV2+colDistV3) * 0.5f );	// V2-3
	const Vector3 colDistMV31( (colDistV3+colDistV1) * 0.5f );	// V3-1
#endif
	const float colDistSqr5 = colDistMV12.Mag2();
	const float colDistSqr6 = colDistMV23.Mag2();
	const float colDistSqr7 = colDistMV31.Mag2();

	if( (colDistSqr1 < PNG_PLANT_TRILOC_FAR_DIST_SQR)	||
		(colDistSqr2 < PNG_PLANT_TRILOC_FAR_DIST_SQR)	||
		(colDistSqr3 < PNG_PLANT_TRILOC_FAR_DIST_SQR)	||
		(colDistSqr4 < PNG_PLANT_TRILOC_FAR_DIST_SQR)	||		
		(colDistSqr5 < PNG_PLANT_TRILOC_FAR_DIST_SQR)	||
		(colDistSqr6 < PNG_PLANT_TRILOC_FAR_DIST_SQR)	||
		(colDistSqr7 < PNG_PLANT_TRILOC_FAR_DIST_SQR)	)
	{
		if(bCreatesPlants || bCreatesObjects)
		{
			// this triangle creates plants or objects - add it to the list
			int loctri = m_UnusedLocTriListHead;
			pngPlantLocTri *pLocTri = &m_LocTrisTab[loctri-1];

			if(pLocTri->Add(tv[0], tv[1], tv[2], actualMaterialID, 255/*pColTri->m_nLighting*/, bCreatesPlants, bCreatesObjects, pEntry->m_pEntity))
			{
				// it's added to the list
				Assert(pEntry->m_LocTriArray[indexLTA]==0);
				pEntry->m_LocTriArray[indexLTA] = loctri;


				if (pLocTri->m_bCreatesObjects)
				{
					// try to create procedural objects on this triangle 
					ProcessTriangleAdded(pLocTri);

					// mark as having procedural objects created (even if it hasn't had) 
					// so the triangle is kept and not thrown away and reprocessed next frame
					pLocTri->m_bCreatedObjects = true;
				}
				else
				{
					Assert(pLocTri->m_bCreatesPlants);
				}
			}//if(pLocTri->Add(...)...
		}//if(bCreatesPlants || bCreatesObjects)...
	}// if(colDistSqrXX < PNG_PLANT_TRILOC_FAR_DIST_SQR))...
}// end of GenerateNewLocTri()....

class atBitSetSpu : public atBitSet
{
public:
	u32 Size() {return m_Size;}
	void Fetch(unsigned* pMem)
	{
		DmaLargeGet(pMem, ((u64)m_Bits)&~15, ((m_Size+7) * sizeof(unsigned))&~15);
		m_Bits = &pMem[(((u64)m_Bits)&15)/4];
	}
};

template<class T> void Fetch(const T*& ptr, u32 count, T* dst) {DmaLargeGet(dst, (u64)ptr, count * sizeof(T)); ptr = dst;}
template<class T> void Fetch(T*& ptr, u32 count, T* dst) {DmaLargeGet(dst, (u64)ptr, count * sizeof(T));	ptr = dst;}

bool pngPlantMgr::_ProcessEntryCollisionData(pngPlantColBoundEntry *pEntry, const Vector3& camPos, int iTriProcessSkipMask)
{
	phBoundGeometry *pBound	= pEntry->GetBound();
	Assert(pBound);
	if(!pBound)
		return(false);

	Assertf(pEntry->m_nNumTris > 0, "Not valid number of triangles in entry!");
	// sometimes collision data is not streamed in (???):
	//Assertf(pColModel->m_nNoOfTriangles == pEntry->m_nNumTris, "Different number of ColTris in CEntity & cached entry!");
	if(pBound->GetNumPolygons() != (pEntry->m_nNumTris/2))
	{
		//DEBUGLOG("\n [*]_ProcessEntryCollisionData(): entry was skipped because no collision data was found!\n"); 
		return(false);
	}

const int count = pEntry->m_nNumTris;

	atBitSetSpu& boundMatProps = *Alloca(atBitSetSpu,1);
	sysMemCpy(&boundMatProps, &pEntry->m_BoundMatProps, sizeof(atBitSet));	
	boundMatProps.Fetch(Alloca(unsigned, boundMatProps.Size()+8));
	
	u16* pPpuLocTriArray = pEntry->m_LocTriArray;
	pEntry->m_LocTriArray = Alloca(u16, count);
	DmaLargeGet(pEntry->m_LocTriArray, (u64)pPpuLocTriArray, count * sizeof(u16));

	Fetch(pBound->m_MaterialIds, pBound->m_NumMaterials, Alloca(phMaterialMgr::Id, pBound->m_NumMaterials));

#if POLY_MAX_VERTICES==3
	Fetch(pBound->m_PolyMatIndexList, pBound->m_NumPolygons, Alloca(u8, pBound->m_NumPolygons));
#endif

	DmaWait();

	const int count_1_8	= count / PNG_PLANT_ENTRY_TRILOC_PROCESS_UPDATE;	// 1/8 of everything to process
	int startCount0	= 0;
	int stopCount0	= 0;
	if(count_1_8 && (iTriProcessSkipMask!=PNG_PLANT_ENTRY_TRILOC_PROCESS_ALWAYS))
	{
		startCount0	= iTriProcessSkipMask * count_1_8;
		stopCount0	= (iTriProcessSkipMask!=PNG_PLANT_ENTRY_TRILOC_PROCESS_UPDATE-1)?((iTriProcessSkipMask+1)*count_1_8):(count);
	}
	else
	{	// case when count < PNG_PLANT_ENTRY_TRILOC_PROCESS_UPDATE:
		startCount0	= 0;
		stopCount0	= count;
	}
	const int startCount	= startCount0;
	const int stopCount	= stopCount0;
	for(int i=startCount; i<stopCount; i++)
	{
		int loctri = pEntry->m_LocTriArray[i];
		if(loctri)
		{	
			pngPlantLocTri* pLocTri = &m_LocTrisTab[loctri-1];
			// check if this triangle creates objects but hasn't created any yet and re process
			if(pLocTri->m_bCreatesObjects && (!pLocTri->m_bCreatedObjects))
			{
				Assert(pLocTri->m_bCreatesPlants);
				if (ProcessTriangleAdded(pLocTri))					
				{
					// objects have been created on this triangle ok
					pLocTri->m_bCreatedObjects = true;					
				}
			}		

			// check if LocTri is in range:
			// if not, remove it:
			
#ifdef USE_COLLISION_2D_DIST
			const Vector3 _colDistV1(camPos - pLocTri->m_V1);
			const Vector3 _colDistV2(camPos - pLocTri->m_V2);
			const Vector3 _colDistV3(camPos - pLocTri->m_V3);
			const Vector3 _colDistV4(camPos - pLocTri->m_Center);
			const Vector2 colDistV1(_colDistV1.x, _colDistV1.y);
			const Vector2 colDistV2(_colDistV2.x, _colDistV2.y);
			const Vector2 colDistV3(_colDistV3.x, _colDistV3.y);
			const Vector2 colDistV4(_colDistV4.x, _colDistV4.y);
#else
			const Vector3 colDistV1(camPos - pLocTri->m_V1);
			const Vector3 colDistV2(camPos - pLocTri->m_V2);
			const Vector3 colDistV3(camPos - pLocTri->m_V3);
			const Vector3 colDistV4(camPos - pLocTri->m_Center);
#endif

			const float colDistSqr1 = colDistV1.Mag2();
			const float colDistSqr2 = colDistV2.Mag2();
			const float colDistSqr3 = colDistV3.Mag2();
			const float colDistSqr4 = colDistV4.Mag2();

			// calc middle points distances (distance between camera and tri edges' middle points):
#ifdef USE_COLLISION_2D_DIST
			const Vector3 _colDistMV12( (_colDistV1+_colDistV2) * 0.5f );	// V1-2
			const Vector3 _colDistMV23( (_colDistV2+_colDistV3) * 0.5f );	// V2-3
			const Vector3 _colDistMV31( (_colDistV3+_colDistV1) * 0.5f );	// V3-1
			const Vector2 colDistMV12(_colDistMV12.x, _colDistMV12.y);
			const Vector2 colDistMV23(_colDistMV23.x, _colDistMV23.y);
			const Vector2 colDistMV31(_colDistMV31.x, _colDistMV31.y);
#else
			const Vector3 colDistMV12( (colDistV1+colDistV2) * 0.5f );	// V1-2
			const Vector3 colDistMV23( (colDistV2+colDistV3) * 0.5f );	// V2-3
			const Vector3 colDistMV31( (colDistV3+colDistV1) * 0.5f );	// V3-1
#endif
			const float colDistSqr5 = colDistMV12.Mag2();
			const float colDistSqr6 = colDistMV23.Mag2();
			const float colDistSqr7 = colDistMV31.Mag2();				

			if( (colDistSqr1 >= PNG_PLANT_TRILOC_FAR_DIST_SQR) &&
				(colDistSqr2 >= PNG_PLANT_TRILOC_FAR_DIST_SQR) &&
				(colDistSqr3 >= PNG_PLANT_TRILOC_FAR_DIST_SQR) &&
				(colDistSqr4 >= PNG_PLANT_TRILOC_FAR_DIST_SQR) &&
				
				(colDistSqr5 >= PNG_PLANT_TRILOC_FAR_DIST_SQR) &&
				(colDistSqr6 >= PNG_PLANT_TRILOC_FAR_DIST_SQR) &&
				(colDistSqr7 >= PNG_PLANT_TRILOC_FAR_DIST_SQR)	)
			{
				pLocTri->Release();
				pEntry->m_LocTriArray[i] = 0;
			}
		}
		else
		{
			if(!(i&0x01))							// process only even entries ("1st cells")
			if(m_UnusedLocTriListHead)				// is there any space in trilist?
			if(m_LocTrisTab[m_UnusedLocTriListHead-1].m_NextTri)	// is there space for 2 tris (quad)?
			{				
				const bool bCreatesPlants  = boundMatProps.IsSet(i);
				const bool bCreatesObjects = boundMatProps.IsSet(i+1);

				if(!bCreatesPlants && !bCreatesObjects)
					continue;
				
				const phPolygon *pPoly = &pBound->GetPolygon(i>>1);
				phPolygon poly;
				DmaGet(&poly, (u64)pPoly, sizeof(phPolygon));
				DmaWait();
#if POLY_MAX_VERTICES==3
				const phBound::MaterialIndex boundMaterialID = pBound->GetPolygonMaterialIndex(i>>1);
#else
				const int	boundMaterialID	= poly.GetMaterialIndex();
#endif
				const u32 actualMaterialID = pBound->GetMaterialId(boundMaterialID);

				const int numPolyPoints = poly.GetNumVerts();
				Assert(numPolyPoints <= PNG_PLANT_MAX_POLY_POINTS);

				if(numPolyPoints == 3)
				{	// process tri (1 triangle only):
					_ProcessEntryCollisionData_GenerateNewLocTri(i, pEntry, pBound, camPos, poly, 0, 1, 2,
							bCreatesPlants, bCreatesObjects, actualMaterialID);
					Assert(pEntry->m_LocTriArray[i+1]==0); // this cell should always be empty
				}
				else if(numPolyPoints == 4)
				{	// process quad (2 triangles):
					// 1. tri0 (even):
					_ProcessEntryCollisionData_GenerateNewLocTri(i, pEntry, pBound, camPos, poly, 0, 1, 2,
							bCreatesPlants, bCreatesObjects, actualMaterialID);
					// 2. tri1 (odd) - make sure it's actually not already created:
					if(!pEntry->m_LocTriArray[i+1])
					{
						_ProcessEntryCollisionData_GenerateNewLocTri(i+1, pEntry, pBound, camPos, poly, 0, 2, 3,
							bCreatesPlants, bCreatesObjects, actualMaterialID);
					}
				}
			}//if(m_UnusedLocTriListHead)...
		}
	}//for(int i=0; i<colModel.m_nNoOfTriangles; i++)...

	DmaLargePut(pEntry->m_LocTriArray, (u64)pPpuLocTriArray, count * sizeof(u16));
	DmaWait();
	pEntry->m_LocTriArray = pPpuLocTriArray;

	return(true);
}// end of pngPlantMgr::_ProcessEntryCollsionData()...


bool pngPlantMgr::UpdateAllLocTris(const Vector3& camPos, int iTriProcessSkipMask)
{
	int entry = m_CloseColEntListHead;
	while(entry)
	{
		pngPlantColBoundEntry *pEntry = &m_ColEntCacheTab[entry-1];
		_ProcessEntryCollisionData(pEntry, camPos, iTriProcessSkipMask);
		entry = pEntry->m_NextEntry;
	}

	return(true);
}// end of pngPlantMgr::_UpdateLocTris()...

void pngPlantMgr::UpdateTask()
{
	UpdateAllLocTris(m_CameraPos, g_jobParams->m_iTriProcessSkipMask);
}

phInst* pngPlantColBoundEntry::GetPhysInst()
{ 
	if (!m_pEntity)
		return 0;
	return (phInst*)DmaGetU32((u64)&m_pEntity->m_pPhysInst);
}

phBoundGeometry* g_pCachedBoundGeom;

//
//
//
//
phBoundGeometry* pngPlantColBoundEntry::GetBound()
{
	phInst* pPpuInst = GetPhysInst();
	if (!pPpuInst)
		return 0;
	// shouldn't really dma across the whole phInst & phArchetype here..
	phInst* pInst = Alloca(phInst, 1);
	DmaGet(pInst, (u64)pPpuInst, sizeof(phInst));
	DmaWait();
	phArchetype* pArchetype = Alloca(phArchetype, 1);
	DmaGet(pArchetype, (u64)pInst->GetArchetype(), sizeof(phArchetype));
	DmaWait();
	DmaGet(g_pCachedBoundGeom, (u64)pArchetype->GetBound(), sizeof(phBoundGeometry));
	DmaWait();
	return g_pCachedBoundGeom;
}



//
//
//
//
//
void PlantsMgrUpdateSPU(sysTaskContext& c)
{
 	g_jobParams = c.GetUserDataAs<CPlantsMgrUpdateSPU>();
	g_pPlantMgr = static_cast<pngPlantMgr*>(c.GetInputAs<pngPlantMgrBase>());
 	g_pProcInfo = c.GetInputAs<CProceduralInfo>();
	g_pAddList = c.GetInputAs<ProcObjectCreationInfo>(g_jobParams->m_addBufSize);
	g_pRemoveList = c.GetInputAs<ProcTriRemovalInfo>(g_jobParams->m_removeBufSize);
	g_pResultSize = c.GetInputAs<CPlantsMgrUpdateSPU::CResultSize>();
	g_pResultSize->m_numAdd = 0;
	g_pResultSize->m_numRemove = 0;
	g_pAddListEnd = g_pAddList + g_jobParams->m_addBufSize;
	g_pRemoveListEnd = g_pRemoveList + g_jobParams->m_removeBufSize;
	g_pCachedBoundGeom = Alloca(phBoundGeometry, 1);
	mthRandom rand(spu_read_decrementer());
	g_pDrawRand = &rand;
	ProcObjectCreationInfo* pAddList = g_pAddList;
	ProcTriRemovalInfo* pRemoveList = g_pRemoveList;

	g_pPlantMgr->UpdateTask();

	g_pResultSize->m_numAdd += g_pAddList - pAddList;
	g_pResultSize->m_numRemove = g_pRemoveList - pRemoveList;
	DmaLargePut(g_pPlantMgr, (u64)g_jobParams->m_pPlantsMgr, sizeof(pngPlantMgrBase), c.DmaTag());
	if (g_pAddList != pAddList)
		DmaLargePut(pAddList, (u64)g_jobParams->m_pAddList, (u8*)g_pAddList - (u8*)pAddList, c.DmaTag());
	if (g_pRemoveList != pRemoveList)
		DmaLargePut(pRemoveList, (u64)g_jobParams->m_pRemoveList, (u8*)g_pRemoveList - (u8*)pRemoveList, c.DmaTag());
	DmaPut(g_pResultSize, (u64)g_jobParams->m_pResultSize, sizeof(CPlantsMgrUpdateSPU::CResultSize), c.DmaTag());
	// update render buffer.. could probably dbl buf instead of triple on ps3
	DmaLargePut(g_pPlantMgr->m_LocTrisTab, (u64)g_jobParams->m_LocTrisRenderTab, sizeof(g_pPlantMgr->m_LocTrisTab), c.DmaTag());
}

#endif //#if __SPU...
#endif // 0
