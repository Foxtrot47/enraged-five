//
// plantsgrass/proceduralinfo.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "plantsgrass/plantsmgr.h" // CPLANT_SLOT_NUM_TEXTURES
#include "plantsgrass/proceduralInfo.h"

namespace rage
{

#if __BANK
char pngProceduralInfo::m_procTagNames[MAX_PROCEDURAL_TAGS][MAX_PROCEDURAL_TAG_LENGTH];
#endif

pngProceduralInfo g_procInfo;

bool pngProceduralInfo::InitLevel()
{
	const char* fileName = "common:/DATA/MATERIALS/PROCEDURAL.DAT";
/*
	const CDataFileMgr::DataFile* pData = DATAFILEMGR.GetFirstFile(CDataFileMgr::PROCOBJ_FILE);
	while(DATAFILEMGR.IsValid(pData))
	{
		fileName = pData->m_filename;
		pData = DATAFILEMGR.GetNextFile(pData);
	}
*/

	fiStream* stream = fiStream::Open(fileName, true);
	if (stream)
	{
		// initialise the tokens
		fiAsciiTokenizer token;
		token.Init("procedural", stream);

		// initialise variables
		m_numProcTags = 0;
		m_numProcObjInfos = 0;
		m_numPlantInfos = 0;

#if __DEV
		s32 newPlantSlotId = false;
		s32 currPlantSlotId = 0;
#endif
		s32 currProcTagId = 0;
		char currProcTagName[128];

		char procTagNames[MAX_PROCEDURAL_TAGS][MAX_PROCEDURAL_TAG_LENGTH];
		for (s32 i=0; i<MAX_PROCEDURAL_TAGS; i++)
		{
#if __BANK
			m_procTagNames[i][0] = '\0'; 
#endif
			procTagNames[i][0] = '\0'; 
			m_procTagTable[i].procObjIndex = -1;
			m_procTagTable[i].plantIndex = -1;
		}

		s32 phase = -1;	
		char charBuff[128];

		// read in the version
		m_version = token.GetFloat();

		while (1)
		{
			token.GetToken(charBuff, 128);

			// check for commented lines
			if (charBuff[0]=='#')
			{
				token.SkipToEndOfLine();
				continue;
			}

			// check for change of phase
			if (stricmp(charBuff, "PROCOBJ_DATA_START")==0)
			{
				// proc objs have tags 1-127 (m_numProcTags get incremented before storing)
				Assert(m_numProcTags==0);
				strcpy(currProcTagName, "invalid");
				phase = 0;
				continue;
			}
			else if (stricmp(charBuff, "PROCOBJ_DATA_END")==0)
			{
				phase = -1;
				continue;
			}
			else if (stricmp(charBuff, "PLANT_DATA_START")==0)
			{
				// plants have tags 128-255 (m_numProcTags get incremented before storing)
				Assert(m_numProcTags<=127);
				m_numProcTags=127;
				strcpy(currProcTagName, "invalid");
				phase = 1;
				continue;
			}
			else if (stricmp(charBuff, "PLANT_DATA_END")==0)
			{
				break;
			}

			// phase 0 - material effect table
			if (phase==0)
			{
				Assertf(m_numProcObjInfos<MAX_PROC_OBJ_INFOS, "Not enough space for proc material info\n");

				// procedural tag
				if (strcmp(charBuff, currProcTagName))
				{
					// new tag is different to previous tag - find the new tag id
					currProcTagId = ProcessProcTag(procTagNames, charBuff, currProcTagName, 0);
				}

				m_procObjInfos[m_numProcObjInfos].m_procTagId = currProcTagId;

				// object name
				token.GetToken(charBuff, 128);

				// get the model index
// 				CBaseModelInfo* pModelInfo = CModelInfo::GetModelInfo(charBuff, &m_procObjInfos[m_numProcObjInfos].m_data.modelIndex);
// 				if (pModelInfo==NULL)
// 				{
// 					Assertf(0, "Object referenced in procedural.dat does not exist (%s)", charBuff);
// 					return false;
// 				}
// 
// 				CBaseModelInfo* pBaseModelInfo = CModelInfo::GetModelInfo(m_procObjInfos[m_numProcObjInfos].m_data.modelIndex);
// 				Assertf(pBaseModelInfo->GetModelType()==MI_TYPE_BASE || pBaseModelInfo->GetModelType()==MI_TYPE_TREE, "Trying to load in a procedural object that isn't an object, a building or a tree\n");

				// other data
				float spacing = token.GetFloat();
				m_procObjInfos[m_numProcObjInfos].m_data.spacing = Max(spacing, 2.0f);		// don't let the spacing get below 2.0
				m_procObjInfos[m_numProcObjInfos].m_data.density = 1.0f/(spacing*spacing);

				float minDist = token.GetFloat();
				minDist = Min(minDist, 80.0f);
				m_procObjInfos[m_numProcObjInfos].m_data.minDistSq = minDist*minDist;

				if (m_version>=3.0f)
				{
					m_procObjInfos[m_numProcObjInfos].m_data.minXRot = token.GetFloat()/180.0f * PI;
					m_procObjInfos[m_numProcObjInfos].m_data.maxXRot = token.GetFloat()/180.0f * PI;
					m_procObjInfos[m_numProcObjInfos].m_data.minYRot = token.GetFloat()/180.0f * PI;
					m_procObjInfos[m_numProcObjInfos].m_data.maxYRot = token.GetFloat()/180.0f * PI;
				}
				else
				{
					m_procObjInfos[m_numProcObjInfos].m_data.minXRot = 0.0f;
					m_procObjInfos[m_numProcObjInfos].m_data.maxXRot = 0.0f;
					m_procObjInfos[m_numProcObjInfos].m_data.minYRot = 0.0f;
					m_procObjInfos[m_numProcObjInfos].m_data.maxYRot = 0.0f;
				}

				m_procObjInfos[m_numProcObjInfos].m_data.minZRot = token.GetFloat()/180.0f * PI;
				m_procObjInfos[m_numProcObjInfos].m_data.maxZRot = token.GetFloat()/180.0f * PI;
				m_procObjInfos[m_numProcObjInfos].m_data.minScale = token.GetFloat();
				m_procObjInfos[m_numProcObjInfos].m_data.maxScale = token.GetFloat();
				m_procObjInfos[m_numProcObjInfos].m_data.minScaleZ = token.GetFloat();
				m_procObjInfos[m_numProcObjInfos].m_data.maxScaleZ = token.GetFloat();
				m_procObjInfos[m_numProcObjInfos].m_data.zOffMin = token.GetFloat();
				m_procObjInfos[m_numProcObjInfos].m_data.zOffMax = token.GetFloat();

				s32 intTemp = token.GetInt();
				m_procObjInfos[m_numProcObjInfos].m_data.alignObj = intTemp>0;
				intTemp = token.GetInt();
				m_procObjInfos[m_numProcObjInfos].m_data.useGrid = intTemp>0;
				intTemp = token.GetInt();
				m_procObjInfos[m_numProcObjInfos].m_data.useSeed = intTemp>0;
				intTemp = token.GetInt();
				m_procObjInfos[m_numProcObjInfos].m_data.isFloating = intTemp>0;

// 				if (m_procObjInfos[m_numProcObjInfos].m_data.isFloating)
// 				{
// 					Assert(pBaseModelInfo->GetModelType()==MI_TYPE_BASE);
// 					Assert(pBaseModelInfo->GetIsTypeObject());
// 				}

				// force any tree variables to be correct
// 				if (pBaseModelInfo->GetModelType()==MI_TYPE_TREE)
// 				{
// 					m_procObjInfos[m_numProcObjInfos].m_data.alignObj	 = false;
// 				}

				// if the object has collision then make sure that the scales are 1.0 - we can't scale collision correctly
// 				if (pModelInfo->GetPhysicsDictionary()!=-1 || pModelInfo->GetFragType())
// 				{
// 					Assertf(m_procObjInfos[m_numProcObjInfos].m_data.minScale==1.0f	&&
// 						m_procObjInfos[m_numProcObjInfos].m_data.maxScale==1.0f	&&
// 						m_procObjInfos[m_numProcObjInfos].m_data.minScaleZ==1.0f &&
// 						m_procObjInfos[m_numProcObjInfos].m_data.maxScaleZ==1.0f, 
// 						"Proc Object %s has collision and scale - this is not allowed", charBuff);
// 					//					Printf("\nxxxProc Object %s has collision and scale - this is not allowed", charBuff);
// 
// 					m_procObjInfos[m_numProcObjInfos].m_data.minScale	= 1.0f;
// 					m_procObjInfos[m_numProcObjInfos].m_data.maxScale	= 1.0f;
// 					m_procObjInfos[m_numProcObjInfos].m_data.minScaleZ	= 1.0f;
// 					m_procObjInfos[m_numProcObjInfos].m_data.maxScaleZ	= 1.0f;
// 				}

				m_numProcObjInfos++;
			}
			else if (phase==1)	// phase 1: Plant data parser:
			{
				Assertf(m_numPlantInfos<MAX_PLANT_INFOS, "Not enough space for plant info\n");

				// procedural tag
				if (strcmp(charBuff, currProcTagName))
				{
					// new tag is different to previous tag - find the new tag id
					currProcTagId = ProcessProcTag(procTagNames, charBuff, currProcTagName, 1);
#if __DEV	
					newPlantSlotId = true;
#endif
				}

				pngPlantInfo *pPlantInfo	= &m_plantInfos[m_numPlantInfos]; 

				pPlantInfo->m_procTagId	= currProcTagId;

				// the rest of the plant data
				s32 tempInt = token.GetInt();
				pPlantInfo->m_nPlantSlotID	= static_cast<u16>(tempInt);

#if __DEV
				if (newPlantSlotId)
				{
					currPlantSlotId = tempInt;
					newPlantSlotId = false;
				}
				else
				{
					Assert(currPlantSlotId==tempInt);
				}
#endif

				tempInt = token.GetInt();
				pPlantInfo->m_nModelID		= static_cast<u16>(tempInt);

				tempInt = token.GetInt();
				pPlantInfo->m_nTextureID	= static_cast<u16>(tempInt);
				Assertf(pPlantInfo->m_nTextureID < PNG_PLANT_SLOT_NUM_TEXTURES, "Procedural Plant Data uses too high textureID!");

				tempInt = token.GetInt();
				pPlantInfo->m_rgbaColor.SetRed(tempInt);
				tempInt = token.GetInt();
				pPlantInfo->m_rgbaColor.SetGreen(tempInt);
				tempInt = token.GetInt();
				pPlantInfo->m_rgbaColor.SetBlue(tempInt);
				tempInt = token.GetInt();
				pPlantInfo->m_rgbaColor.SetAlpha(tempInt);

				tempInt = token.GetInt();
				pPlantInfo->m_nIntensity = static_cast<u8>(tempInt);
				tempInt = token.GetInt();
				pPlantInfo->m_nIntensityVar = static_cast<u8>(tempInt);

				pPlantInfo->m_fScaleXY = token.GetFloat();
				pPlantInfo->m_fScaleVarXY = token.GetFloat();
				pPlantInfo->m_fScaleZ = token.GetFloat();
				pPlantInfo->m_fScaleVarZ = token.GetFloat();

				pPlantInfo->m_fUmScaleH = token.GetFloat();
				pPlantInfo->m_fUmScaleV = token.GetFloat();
				pPlantInfo->m_fUmFreqH = token.GetFloat();
				pPlantInfo->m_fUmFreqV = token.GetFloat();

				pPlantInfo->m_fWindBendScale = token.GetFloat();
				pPlantInfo->m_fWindBendVar = token.GetFloat();

				pPlantInfo->m_fCollRadiusScale = token.GetFloat();

				pPlantInfo->m_fDensity = token.GetFloat();
				Assertf(pPlantInfo->m_fDensity > 0.0f, "Procedural Plant Data has a zero density - not allowed!");

				tempInt = token.GetInt();
				pPlantInfo->m_nLODmask = static_cast<u8>(tempInt);

				m_numPlantInfos++;
			}
		}

		stream->Close();
	}

#if __BANK
//	PGTAMATERIALMGR->GetDebugInterface().Update();
#endif

	return true;
}

void pngProceduralInfo::ShutdownLevel()
{
}

s32 pngProceduralInfo::ProcessProcTag(char procTagNames[MAX_PROCEDURAL_TAGS][MAX_PROCEDURAL_TAG_LENGTH], char* newProcTag, char* currProcTag, s32 type)
{
	// set the current tag name
	strcpy(currProcTag, newProcTag);

	// search for this proc tag in the table
	for (s32 i=1; i<=m_numProcTags; i++)
	{
		if (strcmp(procTagNames[i], newProcTag)==0)
		{
			// found in list already - make sure it hasn't been set already for this type
			if (type==0)
			{
				Assert(m_procTagTable[i].procObjIndex==-1);
				m_procTagTable[i].procObjIndex = m_numProcObjInfos;
			}	
			else if (type==1)
			{
				Assert(m_procTagTable[i].plantIndex==-1);
				m_procTagTable[i].plantIndex = m_numPlantInfos;
			}
			else
			{
				Assert(0);
			}

			return i;
		}
	}

	// proc tag hasn't been found in the table - create a new one
	m_numProcTags++;
	Assert(m_numProcTags<=MAX_PROCEDURAL_TAGS);

	strcpy(procTagNames[m_numProcTags], newProcTag);
#if __BANK
	strcpy(m_procTagNames[m_numProcTags], newProcTag);
#endif
	if (type==0)
	{
		Assert(m_procTagTable[m_numProcTags].procObjIndex==-1);
		m_procTagTable[m_numProcTags].procObjIndex = m_numProcObjInfos;
	}	
	else if (type==1)
	{
		Assert(m_procTagTable[m_numProcTags].plantIndex==-1);
		m_procTagTable[m_numProcTags].plantIndex = m_numPlantInfos;
	}
	else
	{
		Assert(0);
	}

	return m_numProcTags;
}

bool pngProceduralInfo::CreatesProcObjects(s32 procTagId)
{
	Assert(procTagId<=m_numProcTags); 
	return m_procTagTable[procTagId].procObjIndex>-1;
}

bool pngProceduralInfo::CreatesPlants(s32 procTagId)
{
	Assert(procTagId<=m_numProcTags); 
	return m_procTagTable[procTagId].plantIndex>-1;
}

} // namespace rage