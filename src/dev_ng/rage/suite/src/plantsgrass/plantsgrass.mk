PROJECT = plantsgrass

INCLUDES = -I ..\..\..\..\ragestlport\STLport-5.0RC5\stlport -I .. -I ..\..\..\base\src
DEFINES = \

OBJS = \
	$(INTDIR)/plantsmgrupdatespu.job.obj	\
	$(INTDIR)/plantsmgrspu.job.obj	\
	$(INTDIR)/plantsgrassrendererspu.job.obj	\
	$(INTDIR)/vfxlist.obj	\
	$(INTDIR)/proceduralinfo.obj	\
	$(INTDIR)/procobjects.obj	\
	$(INTDIR)/plantsmgrspu.obj	\
	$(INTDIR)/plantsmgr.obj	\
	$(INTDIR)/plantsgrassrendererspu.obj	\
	$(INTDIR)/plantsgrassrenderer.obj	\


include ..\..\..\..\rage\build\Makefile.template

$(INTDIR)/plantsmgrupdatespu.job.obj: ./plantsmgrupdatespu.job
	call ..\..\..\..\rage\build\make_spurs_job_pb $(subst /,\,$(abspath $(subst \,/,.))) $(INTDIR) $(basename plantsmgrupdatespu.job) "$(INTDIR)"

$(INTDIR)/plantsmgrspu.job.obj: ./plantsmgrspu.job
	call ..\..\..\..\rage\build\make_spurs_job_pb $(subst /,\,$(abspath $(subst \,/,.))) $(INTDIR) $(basename plantsmgrspu.job) "$(INTDIR)"

$(INTDIR)/plantsgrassrendererspu.job.obj: ./plantsgrassrendererspu.job
	call ..\..\..\..\rage\build\make_spurs_job_pb $(subst /,\,$(abspath $(subst \,/,.))) $(INTDIR) $(basename plantsgrassrendererspu.job) "$(INTDIR)"

$(INTDIR)/vfxlist.obj: ./vfxlist.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/proceduralinfo.obj: ./proceduralinfo.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/procobjects.obj: ./procobjects.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/plantsmgrspu.obj: ./plantsmgrspu.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/plantsmgr.obj: ./plantsmgr.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/plantsgrassrendererspu.obj: ./plantsgrassrendererspu.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/plantsgrassrenderer.obj: ./plantsgrassrenderer.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

HEADERS: $(HEADERS)
