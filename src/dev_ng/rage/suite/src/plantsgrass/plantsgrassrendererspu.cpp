//
// plantsgrass/plantsgrassrendererspu.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#if __SPU

// missing preprocessor symbols:
// this is defined in PlantsMgr.h and maybe missing here:
#define PNG_PLANTMGR_SPU_ENABLED					(0)

#if PNG_PLANTMGR_SPU_ENABLED


//#define SPU_DEBUGF(X)				X
#define SPU_DEBUGF(x)

#define SPU_DEBUGF1(X)				X




//
// private tagID to use with all DMA stuff here:
//
#define PLANTS_DMATAGID				(22)	// main dma tag
#define PLANTS_DMATAGID1			(23)	// local context0
#define PLANTS_DMATAGID2			(24)	// local context1

// useful shortcut for debug prints, etc.
#define SPUTASKID					(inMasterGrassParams->m_rsxLabel5_CurrentTaskID)


#include <cell/gcm_spu.h>
using namespace cell::Gcm;
#include "math/intrinsics.h"
#include "vector/vector3_consts_spu.cpp"
#include "vector/vector3.h"
#include "vector/matrix34.h"
#include "vector/color32.h"
#include "math/float16.h"
#include "phcore/materialmgr.h"
#include "physics/inst.h"
#include "atl/bitset.h"
#include "math/Random.h"

#include <string.h>
#include <spu_printf.h>
#include <math.h>
#include <cell/dma.h>


// SPU helper headers:
#include "..\basetypes.h"
#include "..\maths\RandomSPU.h"		// CRandom()...
#include "..\maths\MathsSPU.h"		// CMaths()...
#include "..\system\DmaSPU.h"		// CSpuDma()...
#include "..\system\DmaSPU.cpp"

#include "..\Renderer\PlantsGrassRenderer.h"		// struct PPTriPlant{}
#include "..\Renderer\PlantsGrassRendererSPU.h"
#include "..\shader_source\grass_regs.h"

static spuGrassParamsStructMaster	*inMasterGrassParams;
static spuGrassParamsStructMaster	*outMasterGrassParams;

static u32 sm_nNumGrassJobsLaunched=0;

static spuGrassParamsStruct inGrassParamsT		ALIGNED(128);
static spuGrassParamsStruct outGrassParamsT		ALIGNED(128);


#define globalLOD0FarDist2		(inMasterGrassParams->m_LOD0FarDist2)

#define globalLOD1CloseDist2	(inMasterGrassParams->m_LOD1CloseDist2)
#define globalLOD1FarDist2		(inMasterGrassParams->m_LOD1FarDist2)

#define globalLOD2CloseDist2	(inMasterGrassParams->m_LOD2CloseDist2)
#define globalLOD2FarDist2		(inMasterGrassParams->m_LOD2FarDist2)

#define globalWindBending		(inMasterGrassParams->m_windBending)

#if __DEV
	#define GRCDEVICE_GetFrameCounter		(inMasterGrassParams->m_DeviceFrameCounter)
	#define bGeomCullingTestEnabled			(inMasterGrassParams->m_bGeomCullingTestEnabled)
	#define gbPlantsFlashLOD0				(inMasterGrassParams->m_bPlantsFlashLOD0)
	#define gbPlantsFlashLOD1				(inMasterGrassParams->m_bPlantsFlashLOD1)
	#define gbPlantsFlashLOD2				(inMasterGrassParams->m_bPlantsFlashLOD2)
	#define pLocTriPlantsLOD0DrawnPerSlot	(&outMasterGrassParams->m_LocTriPlantsLOD0DrawnPerSlot[inGrassParams->m_nLocTriPlantsSlotID])
	#define pLocTriPlantsLOD1DrawnPerSlot	(&outMasterGrassParams->m_LocTriPlantsLOD1DrawnPerSlot[inGrassParams->m_nLocTriPlantsSlotID])
	#define pLocTriPlantsLOD2DrawnPerSlot	(&outMasterGrassParams->m_LocTriPlantsLOD2DrawnPerSlot[inGrassParams->m_nLocTriPlantsSlotID])
#else
	#define bGeomCullingTestEnabled			(true)
#endif	//__DEV...


static  u8				_gCameraPos[1*sizeof(Vector3)];
#define globalCameraPos		(*((Vector3*)_gCameraPos))

//static u8		_gAlphaFade2[1*sizeof(Vector2)];
//#define gAlphaFade2	((Vector2*)_gAlphaFade2)

static u8 _gFrustumClipPlanes[6*sizeof(Vector4)];				// used for sphere tests
#define gFrustumClipPlanes	((Vector4*)_gFrustumClipPlanes)	// PIC files don't support global constructors

// Transposed clip plane equations (for better vectorization):
static u8			_gFrustumTPlane0[4*sizeof(Vector4)];
#define gFrustumTPlane0	((Vector4*)_gFrustumTPlane0)

static u8			_gFrustumTPlane1[4*sizeof(Vector4)];
#define gFrustumTPlane1	((Vector4*)_gFrustumTPlane1)



// localGrassModel defined like this, because PIC files don't support global constructors:
u8 _localTriPlantTab[PPTRIPLANT_BUFFER_SIZE*sizeof(PPTriPlant)]	ALIGNED(128);
#define localTriPlantTab		((PPTriPlant*)&_localTriPlantTab[0]) 

u8 _localPlantModelsSlot0[PNG_PLANT_SLOT_NUM_MODELS*2*sizeof(pngGrassModel)]	ALIGNED(128);
u8 _localPlantModelsSlot1[PNG_PLANT_SLOT_NUM_MODELS*2*sizeof(pngGrassModel)]	ALIGNED(128);
u8 _localPlantModelsSlot2[PNG_PLANT_SLOT_NUM_MODELS*2*sizeof(pngGrassModel)]	ALIGNED(128);
u8 _localPlantModelsSlot3[PNG_PLANT_SLOT_NUM_MODELS*2*sizeof(pngGrassModel)]	ALIGNED(128);
#define localPlantModelsSlot0	((pngGrassModel*)&_localPlantModelsSlot0[0])
#define localPlantModelsSlot1	((pngGrassModel*)&_localPlantModelsSlot1[0])
#define localPlantModelsSlot2	((pngGrassModel*)&_localPlantModelsSlot2[0])
#define localPlantModelsSlot3	((pngGrassModel*)&_localPlantModelsSlot3[0])
pngGrassModel	*localPlantModelsTab[PNG_PLANT_NUM_PLANT_SLOTS];

u32		localGeometryCmdsLOD2[PNG_PLANTSMGR_LOCAL_GEOMETRY_MAXCMDSIZE] ALIGNED(128);

u32		localPlantTexturesSlot0[PNG_PLANT_SLOT_NUM_TEXTURES*2*PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE] ALIGNED(128);
u32		localPlantTexturesSlot1[PNG_PLANT_SLOT_NUM_TEXTURES*2*PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE] ALIGNED(128);
u32		localPlantTexturesSlot2[PNG_PLANT_SLOT_NUM_TEXTURES*2*PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE] ALIGNED(128);
u32		localPlantTexturesSlot3[PNG_PLANT_SLOT_NUM_TEXTURES*2*PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE] ALIGNED(128);
u32		*localPlantTexturesTab[PNG_PLANT_NUM_PLANT_SLOTS];


u32 localBindShaderLOD0Cmd[PNG_PLANTSMGR_LOCAL_BINDSHADERLOD0_MAXCMDSIZE]				ALIGNED(128);
u32 localBindShaderLOD1Cmd[PNG_PLANTSMGR_LOCAL_BINDSHADERLOD1_MAXCMDSIZE]				ALIGNED(128);
u32 localBindShaderLOD2Cmd[PNG_PLANTSMGR_LOCAL_BINDSHADERLOD2_MAXCMDSIZE]				ALIGNED(128);


u8 _g_PlantsRendRand[sizeof(mthRandom)];
#define g_PlantsRendRand	(*((mthRandom*)_g_PlantsRendRand))


u32	eaHeapBegin				= 0;	// big buffer ea
u32	heapBeginOffset			= 0;	// big buffer offset

u32	eaHeapBeginCurrent		= 0;	// current dest ea in big buffer
u32	heapBeginOffsetCurrent	= 0;	// big buffer offset

// amount of bytes big heap is too small to fit all commands we want
#define eaHeapOverfilled		(outMasterGrassParams->m_AmountOfBigHeapOverfilled)


//u32	bufferID				= 0;	// 0-3: current PPU bufferID SPU is generating
//u32	eaHeapBuffers[4]		= {0};
//u32  heapBufferOffsets[4]	= {0};

u32	spuBufferID				= 0;	// 0-1: current internal SPU buffer (localHeap)

u32	_localSolidHeap0[PNG_PLANTSMGR_LOCAL_HEAP_SIZE/sizeof(u32)]		ALIGNED(128);
u32	_localSolidHeap1[PNG_PLANTSMGR_LOCAL_HEAP_SIZE/sizeof(u32)]		ALIGNED(128);
u32*	localSolidHeap[2] = {0};

u32	localDmaTagID[2] = {0};



u32	eaRsxLabel[4],		eaRsxLabel5;
u32	rsxLabelIndex[4],	rsxLabelIndex5;

// values of rsx labels used by dma get/put:
u32	_rsxLabel1 ALIGNED(128) =-1;
u32	_rsxLabel2 ALIGNED(128) =-1;
u32	_rsxLabel3 ALIGNED(128) =-1;
u32	_rsxLabel4 ALIGNED(128) =-1;
u32*	rsxLabels[4];

u32	rsxLabel5 ALIGNED(128) =-1;

//u32 offsetJTS[4] = {0};



static
void SetupContextData32(CellGcmContextData *context, const uint32_t *addr, const uint32_t size, CellGcmContextCallback callback)
{
	context->begin		= (uint32_t*)addr;
	context->current	= (uint32_t*)addr;
	context->end		= (uint32_t*)addr + (size/sizeof(uint32_t) - 32);	// leave 32 words at the end
	context->callback	= callback;
}

static
void SetupContextData0(CellGcmContextData *context, const uint32_t *addr, const uint32_t size, CellGcmContextCallback callback)
{
	context->begin		= (uint32_t*)addr;
	context->current	= (uint32_t*)addr;
	context->end		= (uint32_t*)addr + size/sizeof(uint32_t) - 1;	// leave 1 extra word at the end
	context->callback	= callback;
}

#define SPU_IABS(A)		(((A)>0)?(A):(-A))


static
void spuSleep(u32 v)
{
	u32 v0 = spu_read_decrementer();
	while(1)
	{
		u32 v1 = spu_read_decrementer();
		// if decrementer overflows, then (v0-v1) will be really big number (so spuSleep() will exit immediately)
		// otherwise it works as expected:
		if(/*SPU_IABS*/(v0-v1) > v)
			break;
	}
}


static
void spuWaitForLabel(u32 *pRsxLabel, u32 eaRsxLabel, u32 expectedValue)
{
	*(pRsxLabel) = -1;
	cellDmaSmallGet(pRsxLabel, eaRsxLabel, 4, PLANTS_DMATAGID, 0, 0);
	cellDmaWaitTagStatusAll(1 << PLANTS_DMATAGID);

//	SPU_DEBUGF(spu_printf("\n SPU %X: spuWaitForLabel start: %X, value=%X, expected=%X", SPUTASKID, eaRsxLabel, *pRsxLabel, expectedValue));

	while(*(pRsxLabel) != expectedValue)
	{
		spuSleep(100);	//10
		cellDmaSmallGet(pRsxLabel, eaRsxLabel, 4, PLANTS_DMATAGID, 0, 0);
		cellDmaWaitTagStatusAll(1 << PLANTS_DMATAGID);
	}

//	SPU_DEBUGF(spu_printf("\n SPU %X: spuWaitForLabel end: %X", SPUTASKID, eaRsxLabel));
}


u32 _context[8] ALIGNED(128) ={0};


static
void spuClearJTSinBuffer(u32 offsetJTS, u32 where2JumpOffset)
{
	CellGcmContext con;
	SetupContextData0(&con, &_context[0], 8*sizeof(u32), NULL);

	if(where2JumpOffset)
		con.SetJumpCommand(where2JumpOffset);	// insert jump
	else
		con.SetNopCommand(1);					// insert NOP

	cellDmaSmallPut(&_context[0], offsetJTS, 4, PLANTS_DMATAGID, 0, 0);
	cellDmaWaitTagStatusAll(1 << PLANTS_DMATAGID);

//	SPU_DEBUGF(spu_printf("\n SPU %X: spuClearJTSinBuffer(%X, %X)", SPUTASKID, offsetJTS, where2JumpOffset));
}



static bool bCustomGcmReserveFailedProlog	= false;	// prolog flag
static bool bCustomGcmReserveFailedEpilog	= false;	// epilog flag

//
//
//
//
#if 1
static
int32_t CustomGcmReserveFailed(CellGcmContextData *context, uint32_t /*count*/)
{
	//	SPU_DEBUGF(spu_printf("\n SPU %X: kicking commands: bufID=%d, context: begin: %X, current: %X, end: %X", SPUTASKID, bufferID, (u32)context->begin, (u32)context->current, (u32)context->end));
//	SPU_DEBUGF(spu_printf("\n SPU %X: kicking commands: bufID=%d, context: begin: %X, current: %X, end: %X", SPUTASKID, bufferID, (u32)context->begin, (u32)context->current, (u32)context->end));

	CellGcmContext *curCon		= (CellGcmContext*)context;
	// store curCon end pointer to avoid recurrent calling of this function
	// to itself when setting commands witch context.SetCommandXXX():
//	const u32* oldCurConDataEnd = context->end;
	context->end += 32;	// we still have some space (32 words) before end of real memory buffer...


	if(bCustomGcmReserveFailedEpilog)
	{	// epilog: nothing to dma, only wait for prev buffer dma
		bCustomGcmReserveFailedEpilog = false;
	}
	else
	{
		// align current buffer to 16 bytes
		u32 offsetJTS0 = ((u8*)curCon->current) - ((u8*)curCon->begin);
		while( offsetJTS0 & 0x0000000f )	// fix pointer to be 16 byte aligned
		{
			*(curCon->current++) = 0x00000000;	// NOP
			offsetJTS0 = ((u8*)curCon->current) - ((u8*)curCon->begin);
		}

		// jump to next segment:
		heapBeginOffsetCurrent += PNG_PLANTSMGR_LOCAL_HEAP_SIZE;
		//Assertf(heapBeginOffsetCurrent < (heapBeginOffset+PNG_PLANTSMGR_BIG_HEAP_SIZE), "GrassRendererSPU: Big buffer overflown!");
		if(heapBeginOffsetCurrent < (heapBeginOffset+PNG_PLANTSMGR_BIG_HEAP_SIZE))
		{
			cellGcmSetJumpCommandUnsafe(curCon, heapBeginOffsetCurrent);
		}
		else
		{	// too small output buffer?
			// emergency: jump back to main CB:
			cellGcmSetJumpCommandUnsafe(curCon, inMasterGrassParams->m_ReturnOffsetToMainCB);
			//SPU_DEBUGF1(spu_printf("\n SPU %X: emergency: jumping back to main CB!", SPUTASKID));
		}


		//Assertf(eaHeapBeginCurrent < (eaHeapBegin+PNG_PLANTSMGR_BIG_HEAP_SIZE), "GrassRendererSPU: Big buffer overflown!");
		// start dmaing current buffer only if enough space:
		if(eaHeapBeginCurrent < (eaHeapBegin+PNG_PLANTSMGR_BIG_HEAP_SIZE))
		{	// start copying current buffer:
			const u32 dmatagID = localDmaTagID[spuBufferID];
			cellDmaPut(localSolidHeap[spuBufferID], eaHeapBeginCurrent, PNG_PLANTSMGR_LOCAL_HEAP_SIZE, dmatagID, 0, 0);	// copy buffer
			//		SPU_DEBUGF(spu_printf("\n SPU %X: dma to eaHeapBeginCurrent=0x%X, spuBufferID=%X", SPUTASKID, eaHeapBeginCurrent, spuBufferID));
		}
		else
		{
			eaHeapOverfilled += PNG_PLANTSMGR_LOCAL_HEAP_SIZE;
			// SPU_DEBUGF(spu_printf("\n SPU %X: emergency: no dma!", SPUTASKID));
		}
		
		// increase big buffer ptr:
		eaHeapBeginCurrent += PNG_PLANTSMGR_LOCAL_HEAP_SIZE;
	}


	if(bCustomGcmReserveFailedProlog)
	{	// prolog: 1st dma ever started, not necessary to wait for prev buffer's dma
		bCustomGcmReserveFailedProlog = false;
	}
	else
	{
		// wait for previous buffer's dma:
		u32 prevSpuBufferID		= (spuBufferID-1)&0x00000001;
		const u32 prevDmatagID	= localDmaTagID[prevSpuBufferID];
		cellDmaWaitTagStatusAll(1 << prevDmatagID);
	}


	
	spuBufferID++;
	spuBufferID &= 0x00000001;

	// restart local context:
	SetupContextData32(context, localSolidHeap[spuBufferID], PNG_PLANTSMGR_LOCAL_HEAP_SIZE, &CustomGcmReserveFailed);
//	SPU_DEBUGF(spu_printf("\n SPU %X: commands kicked!", SPUTASKID));

	return(CELL_OK);
}
#else
//
//
//
//
static
int32_t CustomGcmReserveFailed(CellGcmContextData *context, uint32_t /*count*/)
{
	//	SPU_DEBUGF(spu_printf("\n SPU %X: kicking commands: bufID=%d, context: begin: %X, current: %X, end: %X", SPUTASKID, bufferID, (u32)context->begin, (u32)context->current, (u32)context->end));

	CellGcmContext *curCon		= (CellGcmContext*)context;
	// store curCon end pointer to avoid recurrent calling of this function
	// to itself when setting commands witch context.SetCommandXXX():
	//	const u32* oldCurConDataEnd = context->end;
	context->end += 32;	// we still have some space (32 words) before end of real memory buffer...

	uint32_t ea=0;



	if(bCustomGcmReserveFailedEpilog)
	{	// this is epilog: we need only to wait for last buffer's dma to finish and do its ClearJTS():
		bCustomGcmReserveFailedEpilog=false;
	}
	else
	{
		// close current buffer:
		cellGcmSetWriteCommandLabelUnsafe(curCon, rsxLabelIndex[bufferID], 0);			// RSX: Unlock1():

		u32 offsetJTS0 = ((u8*)curCon->current) - ((u8*)curCon->begin);
		while( offsetJTS0 & 0x0000000f )	// fix pointer to be 16 byte aligned
		{
			*(curCon->current++) = 0x00000000;	// NOP
			offsetJTS0 = ((u8*)curCon->current) - ((u8*)curCon->begin);
		}
		offsetJTS[bufferID] = offsetJTS0;
		cellGcmSetJumpCommandUnsafe(curCon, heapBufferOffsets[bufferID] + offsetJTS[bufferID]);	// RSX: JumpToSelf();

		ea = eaHeapBuffers[bufferID];

		//	SPU_DEBUGF(spu_printf("\n SPU %X: kicking commandsJTS: bufID=%d, context: begin: %X, current: %X, end: %X", SPUTASKID, bufferID, (u32)context->begin, (u32)context->current, (u32)context->end));

		// has RSX finished with current buffer?
		spuWaitForLabel(rsxLabels[bufferID], eaRsxLabel[bufferID], 0);
		// start copying current buffer:
		const u32 dmatagID = localDmaTagID[spuBufferID];
		cellDmaPut(localSolidHeap[spuBufferID], ea, PNG_PLANTSMGR_LOCAL_HEAP_SIZE, dmatagID, 0, 0);	// copy buffer
	}


	if(bCustomGcmReserveFailedProlog)
	{	// this is prolog: buffer0 is only dma'ed and nothing else
		bCustomGcmReserveFailedProlog = false;
	}
	else
	{
		u32 prevSpuBufferID		= (spuBufferID-1)&0x00000001;
		const u32 prevDmatagID	= localDmaTagID[prevSpuBufferID];

		// wait for previous buffer's copy:
		cellDmaWaitTagStatusAll(1 << prevDmatagID);

		// for previous buffer: clear JTS in prev-prev buffer and jump to prev buffer:
		u32 prevBufferID		= (bufferID-1)		& 0x00000003;
		u32 prevPrevBufferID = (prevBufferID-1)	& 0x00000003;

		spuClearJTSinBuffer(eaHeapBuffers[prevPrevBufferID]+offsetJTS[prevPrevBufferID], heapBufferOffsets[prevBufferID]);
	}



	bufferID++;
	bufferID &= 0x00000003;

	spuBufferID++;
	spuBufferID &= 0x00000001;


	// restart local context:
	SetupContextData32(context, localSolidHeap[spuBufferID], PNG_PLANTSMGR_LOCAL_HEAP_SIZE, &CustomGcmReserveFailed);

	// RSX: Lock() current buffer:
	cellGcmSetWriteCommandLabelUnsafe(curCon, rsxLabelIndex[bufferID], 1);		// RSX: Lock1():

	//	SPU_DEBUGF(spu_printf("\n SPU %X: commands kicked!", SPUTASKID));

	return(CELL_OK);
}
#endif //#if 0...


//
//
// copies given set of commands into context:
//
static
void CopyLocalCmdsToContext(CellGcmContext *ctx, u32 *cmd, u32 cmdSize)
{
CellGcmContextData *conData = (CellGcmContextData*)ctx;

// check if enough space for copying commands:
	if((conData->current + cmdSize) >= conData->end)
	{
		conData->callback(conData, 0);
	}
	
	for(u32 i=0; i<cmdSize; i++)
	{
		*(conData->current++) = *(cmd++);
	}
}


//
//
// SPU version of GRCDEVICE.SetVertexShaderConstant():
//
inline void GRCDEVICE_SetVertexShaderConstant(CellGcmContext *gcmCon, int startRegister, const float *data, int regCount)
{
	gcmCon->SetVertexProgramConstants(startRegister,regCount<<2,data);
}

inline void GRCDEVICE_DrawIndexedPrimitive(CellGcmContext *gcmCon, pngGrassModel *model)
{
	gcmCon->SetDrawIndexArray(
				model->m_DrawMode,				//modeMap[dm],
				model->m_IdxCount,			//indexCount,
				CELL_GCM_DRAW_INDEX_ARRAY_TYPE_16,
				model->m_IdxLocation,		//gcm::IsLocalPtr(ptr)?CELL_GCM_LOCATION_LOCAL:CELL_GCM_LOCATION_MAIN,
				model->m_IdxOffset		//offset
				);
}

inline void GRCDEVICE_DrawPrimitive(CellGcmContext *gcmCon, u32 primitive, u32 startVertex, u32 vertexCount)
{
	gcmCon->SetDrawArrays(primitive, startVertex, vertexCount);
}


//
//
// stolen from grcViewport::IsSphereVisibleInline():
//
inline
bool _IsSphereVisibleInline(Vector4::Vector4Param sphere)
{
#if 1
Vector4::Vector4Param m_Plane0x = gFrustumTPlane0[0];
Vector4::Vector4Param m_Plane0y = gFrustumTPlane0[1];
Vector4::Vector4Param m_Plane0z = gFrustumTPlane0[2];
Vector4::Vector4Param m_Plane0w = gFrustumTPlane0[3];
Vector4::Vector4Param m_Plane1x = gFrustumTPlane1[0];
Vector4::Vector4Param m_Plane1y = gFrustumTPlane1[1];
Vector4::Vector4Param m_Plane1z = gFrustumTPlane1[2];
Vector4::Vector4Param m_Plane1w = gFrustumTPlane1[3];

	__vector4 xxxx = __vspltw(sphere,0);	// replicate .x through a vector
	__vector4 yyyy = __vspltw(sphere,1);	// ...and y, etc
	__vector4 zzzz = __vspltw(sphere,2);
	__vector4 wwww = __vspltw(sphere,3);
	__vector4 sum0 = __vaddfp(__vmaddfp(xxxx, m_Plane0x, __vmaddfp(yyyy, m_Plane0y, __vmaddfp(zzzz, m_Plane0z, m_Plane0w))), wwww);
	__vector4 sum1 = __vaddfp(__vmaddfp(xxxx, m_Plane1x, __vmaddfp(yyyy, m_Plane1y, __vmaddfp(zzzz, m_Plane1z, m_Plane1w))), wwww);
	_uvector4 cmp0 = __vcmpgefp(sum0, _vzerofp);	// Planes that pass visibility will have 0xFFFFFFFF in their column.
	_uvector4 cmp1 = __vcmpgefp(sum1, _vzerofp);
	_uvector4 final = __vand(cmp0,cmp1);				// Intersect the two sets of frustum clip planes.

	// Replicate nearclip distance across all registers so that __stvewx always gets a valid value.
//	__stvewx(__vspltw(sum0,0),&outZ,0);

	// __vspltisw sign-extends, giving us 0xFFFFFFFF in all four columns
	// Copy appropriate bit out of condition code which is nonzero if all four columns matched the test vector.
	return _vequal(final, _vall1);
#else
	// test near plane only
	Vector4::Vector4Param pe = gFrustumClipPlanes[spuCLIP_PLANE_NEAR];
	float test = pe.x*sphere.x + pe.y*sphere.y + pe.z*sphere.z + pe.w + sphere.w;

	// return the distance from the near clipping plane
//	outZ = test;

	// if it is already behind the near clipping plane break out ...
	if (test < 0.0f)
		return false;

	// then check the other planes
	for(int i=spuCLIP_PLANE_LEFT; i<=spuCLIP_PLANE_BOTTOM; i++) 
	{
		// Get the plane coefficients (layout: Vector4(Normal, D))
		// D - with a normalized normal vector, D expresses the 
		// Euclidian distance by which the plane is translated in the direction 
		// of the normal vector from the origin
		Vector4::Vector4Param pei = gFrustumClipPlanes[i];

		// This code checks if a sphere is outside the frustum
		// It works like this
		// Distance = DotProduct(NormalOfPlane, CenterOfSphere) + D + SphereRadius < 0.0f
		if (pei.x*sphere.x + pei.y*sphere.y + pei.z*sphere.z + pei.w + sphere.w < 0.0f)
			return false;
	}
	return true;
#endif
} // end of _IsSphereVisibleInline()...





//
//
// translated from Andrez's VU code...
//
// Based on Turk's algorithm:
//
// s = rand[0; 1]
// t = rand[0; 1]
//
inline
void _GenPointInTriangle(Vector3 *pRes, const Vector3 *V1, const Vector3 *V2, const Vector3 *V3, float s, float t)
{
	if((s+t) > 1.0f)
	{
		s = 1.0f - s;
		t = 1.0f - t;
	}
	
	const float a = 1.0f - s - t;
	const float b = s;
	const float c = t;

	pRes->x = a*V1->x + b*V2->x + c*V3->x;
	pRes->y = a*V1->y + b*V2->y + c*V3->y;
	pRes->z = a*V1->z + b*V2->z + c*V3->z;
}

inline
void _GenPointInTriangle(Vector4 *pRes, const Vector4 *V1, const Vector4 *V2, const Vector4 *V3, float s, float t)
{
	if((s+t) > 1.0f)
	{
		s = 1.0f - s;
		t = 1.0f - t;
	}

	const float a = 1.0f - s - t;
	const float b = s;
	const float c = t;

	pRes->x = a*V1->x + b*V2->x + c*V3->x;
	pRes->y = a*V1->y + b*V2->y + c*V3->y;
	pRes->z = a*V1->z + b*V2->z + c*V3->z;
	pRes->w = a*V1->w + b*V2->w + c*V3->w;
}


// 360: Leaving it disabled for now, because this doesn't help much on Xenos for some reason
//      and total GPU execution time/CB size is longer/bigger when enabled (?!?);
//      probably D3D does similiar caching tricks under the hood when calling SetVertexShaderConstant();
//
#define OPTIMIZED_VS_GRASS_SETUP	(1 && (__PPU||__SPU))

//
//
//
//
struct instGrassDataDrawStruct
{
	Matrix44				m_WorldMatrix;
	Vector4					m_PlantColor;
	Vector4					m_GroundColor;

	///////////////////////////////////////////////////////
#if OPTIMIZED_VS_GRASS_SETUP
	Matrix34				m_prevWorldMatrix;
#endif

public:
	inline void Reset()		{
#if OPTIMIZED_VS_GRASS_SETUP
		m_prevWorldMatrix.a =
			m_prevWorldMatrix.b =
			m_prevWorldMatrix.c =
			m_prevWorldMatrix.d = Vector3(-1,-1,-1);
#endif //OPTIMIZED_VS_GRASS_SETUP...
	}

	inline bool UseOptimizedVsSetup(const Matrix34& m34)
	{
#if OPTIMIZED_VS_GRASS_SETUP
		const bool bDifferent =	m_prevWorldMatrix.a.IsNotEqual(m34.a) ||
			m_prevWorldMatrix.b.IsNotEqual(m34.b) ||
			m_prevWorldMatrix.c.IsNotEqual(m34.c);
		if(bDifferent)
		{
			m_prevWorldMatrix.a = m34.a;
			m_prevWorldMatrix.b = m34.b;
			m_prevWorldMatrix.c = m34.c;
			return(false);
		}
		return(true);
#else //OPTIMIZED_VS_GRASS_SETUP
		return(false);
#endif //OPTIMIZED_VS_GRASS_SETUP
	}

	// full setup: 6 registers
	inline int			GetRegisterCountFull() const	{ return( (sizeof(m_WorldMatrix)/16) + (sizeof(m_PlantColor)/16) + (sizeof(m_GroundColor)/16) );}
	inline const float*		GetRegisterPointerFull() const	{ return( (const float*)&m_WorldMatrix	);														}

	// optimized setup: 3 registers (only position from matrix is taken):
	inline int			GetRegisterCountOpt() const		{ return( (sizeof(m_WorldMatrix.d)/16) + (sizeof(m_PlantColor)/16) + (sizeof(m_GroundColor)/16) );}
	inline const float*		GetRegisterPointerOpt() const	{ return( (const float*)&m_WorldMatrix.d	);														}
};



//
//
// process all PPTriPlants in given batch
//
static
void processGrassTris(spuGrassParamsStruct *inGrassParams, CellGcmContext *gcmCon)
{
const u32 slotID = inGrassParams->m_slotID;

// models and textures for this slot:
pngGrassModel	*localPlantModels	= localPlantModelsTab[slotID];
u32		*localPlantTextures = localPlantTexturesTab[slotID];


	const int numTriPlants = inGrassParams->m_numTriPlant;
	for(int i=0; i<numTriPlants; i++)
	{
		PPTriPlant *pCurrTriPlant = &localTriPlantTab[i];

		const Vector3 *v1 = &pCurrTriPlant->V1;
		const Vector3 *v2 = &pCurrTriPlant->V2;
		const Vector3 *v3 = &pCurrTriPlant->V3;

		// want same values for plant positions each time...
		g_PlantsRendRand.Reset(pCurrTriPlant->seed);

instGrassDataDrawStruct	customInstDraw;
Matrix34				plantMatrix;
Vector4					plantColourf;

				customInstDraw.Reset();

				// intensity support:  final_I = I + VarI*rand01()
				const int16 intensityVar = pCurrTriPlant->intensity_var;
				u16 intensity = pCurrTriPlant->intensity + u16(float(intensityVar)*g_PlantsRendRand.GetRanged(0.0f, 1.0f));
				intensity = MIN(intensity, 255);

				Color32 col32;
				col32.SetAlpha(	pCurrTriPlant->color.GetAlpha());
	#define CALC_COLOR_INTENSITY(COLOR, I)			u8((u16(COLOR)*I) >> 8)
				col32.SetRed(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetRed(),		intensity));
				col32.SetGreen(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetGreen(),	intensity));
				col32.SetBlue(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetBlue(),	intensity));
	#undef CALC_COLOR_INTENSITY

				plantColourf.x = col32.GetRedf();
				plantColourf.y = col32.GetGreenf();
				plantColourf.z = col32.GetBluef();
				plantColourf.w = col32.GetAlphaf();


	//			grcTexture *pPlantTextureLOD0 = pCurrTriPlant->texture_ptr;
	//			Assert(pPlantTextureLOD0);
				u32 *pTextureCmdsLOD0		= &localPlantTextures[pCurrTriPlant->texture_id*PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE];
				//SPU_DEBUGF(spu_printf("\nSPU %X: setting texture: %d", SPUTASKID, textureID));
	//			grcTexture *pPlantTextureLOD1 = pCurrTriPlant->textureLOD1_ptr;
	//			Assert(pPlantTextureLOD1);
				u32 *pTextureCmdsLOD1		= &localPlantTextures[pCurrTriPlant->textureLOD1_id*PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE];

	//			grmGeometry *pGeometryLOD0		= plantModelsTab[pCurrTriPlant->model_id].m_pGeometry;
	//			const Vector4 boundSphereLOD0	= plantModelsTab[pCurrTriPlant->model_id].m_BoundingSphere; 
				const u32 modelID			= (u32)pCurrTriPlant->model_id;
				pngGrassModel *pPlantModelLOD0		= &localPlantModels[modelID];
				u32 *pGeometryCmdsLOD0		= &pPlantModelLOD0->m_GeometryCmd0[0];
				const Vector4 boundSphereLOD0	= pPlantModelLOD0->m_BoundingSphere;
			
	//			grmGeometry *pGeometryLOD1		= plantModelsTab[pCurrTriPlant->model_id+PNG_PLANT_SLOT_NUM_MODELS].m_pGeometry;
	//			const Vector4 boundSphereLOD1	= plantModelsTab[pCurrTriPlant->model_id+PNG_PLANT_SLOT_NUM_MODELS].m_BoundingSphere; 
				pngGrassModel *pPlantModelLOD1		= &localPlantModels[modelID+PNG_PLANT_SLOT_NUM_MODELS];
				u32 *pGeometryCmdsLOD1		= &pPlantModelLOD1->m_GeometryCmd0[0];
				const Vector4 boundSphereLOD1	= pPlantModelLOD1->m_BoundingSphere;

//				const Vector2 GeometryDimLOD2	= plantModelsTab[pCurrTriPlant->model_id+PNG_PLANT_SLOT_NUM_MODELS].m_dimensionLOD2;
				const Vector2 GeometryDimLOD2	= pPlantModelLOD1->m_dimensionLOD2;
				const Vector4 boundSphereLOD2(0,0,0,1.0f);


				

				// set color for whole pTriLoc:
	//			m_Shader->SetVar(m_shdPlantColorID,		plantColourf);

				// set global micro-movement params: [ scaleH | scaleV | freqH | freqV ]
	//			m_Shader->SetVar(m_shdUMovementParamsID, pCurrTriPlant->um_param);

				// set global collision radius scale:
	//			m_Shader->SetVar(m_shdCollParamsID,		pCurrTriPlant->coll_params);

				// LOD2 billboard dimensions:
	//			Vector4 dimLOD2;
	//			dimLOD2.x = GeometryDimLOD2.x;
	//			dimLOD2.y = GeometryDimLOD2.y;
	//			dimLOD2.z = 0.0f;
	//			dimLOD2.w = 0.0f;
	//			m_Shader->SetVar(m_shdDimensionLOD2ID,	dimLOD2);


				// local per-vertex data: ground color(XYZ)+scale(W):
				Vector4 groundColorV1, groundColorV2, groundColorV3;
				Vector4 groundColorf;

				groundColorV1.x = pCurrTriPlant->groundColorV1.GetRedf();
				groundColorV1.y = pCurrTriPlant->groundColorV1.GetGreenf();
				groundColorV1.z = pCurrTriPlant->groundColorV1.GetBluef();
				groundColorV1.w = pngPlantLocTri::pvUnpackScale(pCurrTriPlant->groundColorV1.GetAlpha());

				groundColorV2.x = pCurrTriPlant->groundColorV2.GetRedf();
				groundColorV2.y = pCurrTriPlant->groundColorV2.GetGreenf();
				groundColorV2.z = pCurrTriPlant->groundColorV2.GetBluef();
				groundColorV2.w = pngPlantLocTri::pvUnpackScale(pCurrTriPlant->groundColorV2.GetAlpha());

				groundColorV3.x = pCurrTriPlant->groundColorV3.GetRedf();
				groundColorV3.y = pCurrTriPlant->groundColorV3.GetGreenf();
				groundColorV3.z = pCurrTriPlant->groundColorV3.GetBluef();
				groundColorV3.w = pngPlantLocTri::pvUnpackScale(pCurrTriPlant->groundColorV3.GetAlpha());



				///////////// Render LOD0: /////////////////////////////////////////////////////////////////////////
#if __DEV
				if((pCurrTriPlant->LODmask&0x01) && (!gbPlantsFlashLOD0 || (GRCDEVICE_GetFrameCounter&0x01)))
#else
				if(pCurrTriPlant->LODmask&0x01)
#endif
				{
					// want same values for plant positions each time...
					g_PlantsRendRand.Reset(pCurrTriPlant->seed);

					// bind plant shader			
					//	BindPlantShader(m_shdDeferredLOD0TechniqueID);
					CopyLocalCmdsToContext(gcmCon, localBindShaderLOD0Cmd, PNG_PLANTSMGR_LOCAL_BINDSHADERLOD0_MAXCMDSIZE);
					customInstDraw.Reset();

					// after binding shader - set other VS local variables now:

					// set color for whole pTriLoc:
					// m_Shader->SetVar(m_shdPlantColorID,		plantColourf);
					GRCDEVICE_SetVertexShaderConstant(gcmCon, GRASS_REG_PLANTCOLOR, (const float*)&plantColourf, 1);

					// set global micro-movement params: [ scaleH | scaleV | freqH | freqV ]
					// m_Shader->SetVar(m_shdUMovementParamsID, pCurrTriPlant->um_param);
					GRCDEVICE_SetVertexShaderConstant(gcmCon, GRASS_REG_UMPARAMS, (const float*)&pCurrTriPlant->um_param, 1);

					// set global collision radius scale:
					// m_Shader->SetVar(m_shdCollParamsID,		pCurrTriPlant->coll_params);
					GRCDEVICE_SetVertexShaderConstant(gcmCon, GRASS_REG_PLAYERCOLLPARAMS, (const float*)&pCurrTriPlant->coll_params, 1);

					// LOD2 billboard dimensions:
					Vector4 dimLOD2;
					dimLOD2.x = 1.0f;	//GeometryDimLOD2.x;
					dimLOD2.y = 1.0f;	//GeometryDimLOD2.y;
					dimLOD2.z = 0.0f;
					dimLOD2.w = 0.0f;
					// m_Shader->SetVar(m_shdDimensionLOD2ID,	dimLOD2);
					GRCDEVICE_SetVertexShaderConstant(gcmCon, GRASS_REG_DIMENSIONLOD2, (const float*)&dimLOD2, 1);

					// setup texture:
					//m_Shader->SetVar(m_shdTextureID,		pPlantTextureLOD0);
					CopyLocalCmdsToContext(gcmCon, pTextureCmdsLOD0, PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE);


					// Setup the vertex and index buffers
	//				grcIndexBuffer* pIndexBuffer			= pGeometryLOD0->GetIndexBuffer(true);
	//				grcVertexBuffer* pVertexBuffer			= pGeometryLOD0->GetVertexBuffer(true);
	//				const u16 nPrimType						= pGeometryLOD0->GetPrimitiveType();
	//				const u32 nIdxCount						= pIndexBuffer->GetIndexCount();
	//				grcVertexDeclaration* vertexDeclaration	= pGeometryLOD0->GetDecl();
	//				GRCDEVICE.SetVertexDeclaration(vertexDeclaration);
	//				GRCDEVICE.SetIndices(*pIndexBuffer);
	//				grcCurrentIndexBuffer = reinterpret_cast<const grcIndexBufferGCM*>(&pBuffer);
	//				GRCDEVICE.SetStreamSource(0, *pVertexBuffer, 0, pVertexBuffer->GetVertexStride());
					CopyLocalCmdsToContext(gcmCon, pGeometryCmdsLOD0, PNG_PLANTSMGR_LOCAL_GEOMETRY_MAXCMDSIZE);
				

					//
					// render a single triPlant full of the desired model
					const int count = pCurrTriPlant->num_plants;
					for(int i=0; i<count; i++)
					{
						const float s = g_PlantsRendRand.GetRanged(0.0f, 1.0f);
						const float t = g_PlantsRendRand.GetRanged(0.0f, 1.0f);
						Vector3 posPlant;
						_GenPointInTriangle(&posPlant, v1, v2, v3, s, t);

						// calculate LOD0 distance:
						Vector3 LodDistV = posPlant - globalCameraPos; 
						const float fLodDist2 = LodDistV.Mag2();
						const bool bLodVisible = (fLodDist2 < globalLOD0FarDist2);

						// early skip if current geometry not visible as LOD0:
						if(!bLodVisible)
						{
							g_PlantsRendRand.GetInt();
							g_PlantsRendRand.GetInt();
							g_PlantsRendRand.GetInt();
							g_PlantsRendRand.GetInt();
							if(intensityVar)
								g_PlantsRendRand.GetInt();

							continue;
						}


						// interpolate ground color(XYZ)+scale(W):
						_GenPointInTriangle(&groundColorf, &groundColorV1, &groundColorV2, &groundColorV3, s, t);
						const float groundScaleVar0 = groundColorf.w;
						groundColorf.w = 1.0f;

						// per-vertex scale:
						// <-5; 0) - scales Z only
						// <+0; 5> - scales XYZ
						const float groundScaleVarXY= (groundScaleVar0>0.0f) ? groundScaleVar0 : 1.0f;
						const float groundScaleVarZ	= Abs(groundScaleVar0);


						// ---- apply various amounts of scaling to the matrix -----
						// calculate an x/y scaling value and apply to matrix

						// final_SclXY = SclXY + SclVarXY*rand01()
						const float	scvariationXY	= pCurrTriPlant->scale_var_xy;
						const float	scaleXY			= (pCurrTriPlant->scale.x + scvariationXY*g_PlantsRendRand.GetRanged(0.0f, 1.0f))*groundScaleVarXY;

						// calculate a z scaling value and apply to matrix
						const float	scvariationZ	= pCurrTriPlant->scale_var_z;
						const float	scaleZ			= (pCurrTriPlant->scale.y + scvariationZ*g_PlantsRendRand.GetRanged(0.0f, 1.0f))*groundScaleVarZ;
						// ----- end of scaling stuff ------


						// choose bigger scale for bound frustum check:
						const float boundRadiusScale = (scaleXY > scaleZ)? (scaleXY) : (scaleZ);

						Vector4 sphere;
						sphere.Set(boundSphereLOD0);
						sphere.AddVector3XYZ(posPlant);
						sphere.w *= boundRadiusScale;

						// skip current geometry if not visible:
						if(bGeomCullingTestEnabled && (!_IsSphereVisibleInline(sphere)))
						{
							g_PlantsRendRand.GetInt();
							g_PlantsRendRand.GetInt();
							if(intensityVar)
								g_PlantsRendRand.GetInt();

							continue;
						}

						plantMatrix.MakeRotateZ(g_PlantsRendRand.GetRanged(0.0f, 1.0f)*2.0f*PI);// overwrites a,b,c
						plantMatrix.MakeTranslate(posPlant);									// overwrites d
						plantMatrix.Scale(scaleXY, scaleXY, scaleZ);	// scale in XY+Z


						// ---- muck about with the matrix to make atomic look blown by the wind --------
						// use here wind_bend_scale & wind_bend_var
						//	final_bend = [ 1.0 + (bend_var*rand(-1,1)) ] * [ global_wind * bend_scale ]
						const float variationBend = pCurrTriPlant->wind_bend_var;
						const float bending =	(1.0f + (variationBend*g_PlantsRendRand.GetRanged(0.0f, 1.0f))) *
							(globalWindBending * pCurrTriPlant->wind_bend_scale);
						plantMatrix.c.x = bending;
						plantMatrix.c.y = bending;
						// ----- end of wind stuff -----


						// color variation:
						if(intensityVar)
						{
							// intensity support:  final_I = I + VarI*rand01()
							u16 intensity = pCurrTriPlant->intensity + u16(float(intensityVar)*g_PlantsRendRand.GetRanged(0.0f, 1.0f));
							intensity = MIN(intensity, 255);

							Color32 col32;
	#define CALC_COLOR_INTENSITY(COLOR, I)			u8((u16(COLOR)*I) >> 8)
							col32.SetRed(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetRed(),		intensity));
							col32.SetGreen(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetGreen(),	intensity));
							col32.SetBlue(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetBlue(),	intensity));
	#undef CALC_COLOR_INTENSITY

							plantColourf.x = col32.GetRedf();
							plantColourf.y = col32.GetGreenf();
							plantColourf.z = col32.GetBluef();
						} // if(intensityVar)...


						customInstDraw.m_WorldMatrix.FromMatrix34(plantMatrix);
						customInstDraw.m_PlantColor.Set(plantColourf);
						customInstDraw.m_GroundColor.Set(groundColorf);

						const bool bOptimizedSetup = customInstDraw.UseOptimizedVsSetup(plantMatrix);
						if(bOptimizedSetup)
						{	// optimized setup: 3 registers:
							GRCDEVICE_SetVertexShaderConstant(gcmCon, 67, customInstDraw.GetRegisterPointerOpt(), customInstDraw.GetRegisterCountOpt());
						}
						else
						{	// full setup: 6 registers:
							GRCDEVICE_SetVertexShaderConstant(gcmCon, 64, customInstDraw.GetRegisterPointerFull(), customInstDraw.GetRegisterCountFull());
						}
						GRCDEVICE_DrawIndexedPrimitive(gcmCon, pPlantModelLOD0);
#if __DEV
						// rendering stats:
						(*pLocTriPlantsLOD0DrawnPerSlot)++;
#endif //__DEV
					}//for(int i=0; i<count; i++)...

//					UnBindPlantShader();

				}// render LOD0...
				////////////////////////////////////////////////////////////////////////////////////////////////////


				///////////// Render LOD1: /////////////////////////////////////////////////////////////////////////
	#if __DEV
				if((pCurrTriPlant->LODmask&0x02) && (!gbPlantsFlashLOD1 || (GRCDEVICE_GetFrameCounter&0x01)))
	#else
				if(pCurrTriPlant->LODmask&0x02)
	#endif
				{
					g_PlantsRendRand.Reset(pCurrTriPlant->seed);

					// Bind the grass shader
//					BindPlantShader(m_shdDeferredLOD1TechniqueID);
					CopyLocalCmdsToContext(gcmCon, localBindShaderLOD1Cmd, PNG_PLANTSMGR_LOCAL_BINDSHADERLOD1_MAXCMDSIZE);
					customInstDraw.Reset();

					// after binding shader - set other VS local variables now:

					// set color for whole pTriLoc:
					// m_Shader->SetVar(m_shdPlantColorID,		plantColourf);
					GRCDEVICE_SetVertexShaderConstant(gcmCon, GRASS_REG_PLANTCOLOR, (const float*)&plantColourf, 1);

					// set global micro-movement params: [ scaleH | scaleV | freqH | freqV ]
					// m_Shader->SetVar(m_shdUMovementParamsID, pCurrTriPlant->um_param);
					GRCDEVICE_SetVertexShaderConstant(gcmCon, GRASS_REG_UMPARAMS, (const float*)&pCurrTriPlant->um_param, 1);

					// set global collision radius scale:
					// m_Shader->SetVar(m_shdCollParamsID,		pCurrTriPlant->coll_params);
					GRCDEVICE_SetVertexShaderConstant(gcmCon, GRASS_REG_PLAYERCOLLPARAMS, (const float*)&pCurrTriPlant->coll_params, 1);

					// LOD2 billboard dimensions:
					Vector4 dimLOD2;
					dimLOD2.x = 1.0f;	//GeometryDimLOD2.x;
					dimLOD2.y = 1.0f;	//GeometryDimLOD2.y;
					dimLOD2.z = 0.0f;
					dimLOD2.w = 0.0f;
					// m_Shader->SetVar(m_shdDimensionLOD2ID,	dimLOD2);
					GRCDEVICE_SetVertexShaderConstant(gcmCon, GRASS_REG_DIMENSIONLOD2, (const float*)&dimLOD2, 1);

					// setup texture:
					// m_Shader->SetVar(m_shdTextureID,	pPlantTextureLOD1);
					CopyLocalCmdsToContext(gcmCon, pTextureCmdsLOD1, PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE);


					// Setup the vertex and index buffers
//					grcIndexBuffer* pIndexBuffer			= pGeometryLOD1->GetIndexBuffer(true);
//					grcVertexBuffer* pVertexBuffer			= pGeometryLOD1->GetVertexBuffer(true);
//					const u16 nPrimType						= pGeometryLOD1->GetPrimitiveType();
//					const u32 nIdxCount						= pIndexBuffer->GetIndexCount();
//					grcVertexDeclaration* vertexDeclaration	= pGeometryLOD1->GetDecl();

//					GRCDEVICE.SetVertexDeclaration(vertexDeclaration);
//					GRCDEVICE.SetIndices(*pIndexBuffer);
//					GRCDEVICE.SetStreamSource(0, *pVertexBuffer, 0, pVertexBuffer->GetVertexStride());
					CopyLocalCmdsToContext(gcmCon, pGeometryCmdsLOD1, PNG_PLANTSMGR_LOCAL_GEOMETRY_MAXCMDSIZE);


					//
					// render a single triPlant full of the desired model
					const int count = pCurrTriPlant->num_plants;
					for(int i=0; i<count; i++)
					{
						const float s = g_PlantsRendRand.GetRanged(0.0f, 1.0f);
						const float t = g_PlantsRendRand.GetRanged(0.0f, 1.0f);
						Vector3 posPlant;
						_GenPointInTriangle(&posPlant, v1, v2, v3, s, t);

						// calculate LOD1 distance:
						Vector3 LodDistV = posPlant - globalCameraPos; 
						const float fLodDist2 = LodDistV.Mag2();
						const bool bLodVisible = (fLodDist2 > globalLOD1CloseDist2)	&& (fLodDist2 < globalLOD1FarDist2);

						// early skip if current geometry not visible as LOD1:
						if(!bLodVisible)
						{
							g_PlantsRendRand.GetInt();
							g_PlantsRendRand.GetInt();
							g_PlantsRendRand.GetInt();
							g_PlantsRendRand.GetInt();
							if(intensityVar)
								g_PlantsRendRand.GetInt();

							continue;
						}

						// interpolate ground color(XYZ)+scale(W):
						_GenPointInTriangle(&groundColorf, &groundColorV1, &groundColorV2, &groundColorV3, s, t);
						const float groundScaleVar0 = groundColorf.w;
						groundColorf.w = 1.0f;

						// per-vertex scale:
						// <-5; 0) - scales Z only
						// <+0; 5> - scales XYZ
						const float groundScaleVarXY= (groundScaleVar0>0.0f) ? groundScaleVar0 : 1.0f;
						const float groundScaleVarZ	= Abs(groundScaleVar0);


						// ---- apply various amounts of scaling to the matrix -----
						// calculate an x/y scaling value and apply to matrix

						// final_SclXY = SclXY + SclVarXY*rand01()
						const float	scvariationXY	= pCurrTriPlant->scale_var_xy;
						const float	scaleXY			= (pCurrTriPlant->scale.x + scvariationXY*g_PlantsRendRand.GetRanged(0.0f, 1.0f))*groundScaleVarXY;

						// calculate a z scaling value and apply to matrix
						const float	scvariationZ	= pCurrTriPlant->scale_var_z;
						const float	scaleZ			= (pCurrTriPlant->scale.y + scvariationZ*g_PlantsRendRand.GetRanged(0.0f, 1.0f))*groundScaleVarZ;
						// ----- end of scaling stuff ------


						// choose bigger scale for bound frustum check:
						const float boundRadiusScale = (scaleXY > scaleZ)? (scaleXY) : (scaleZ);

						Vector4 sphere;
						sphere.Set(boundSphereLOD1);
						sphere.AddVector3XYZ(posPlant);
						sphere.w *= boundRadiusScale;

						// skip current geometry if not visible:
						if(bGeomCullingTestEnabled && (!_IsSphereVisibleInline(sphere)))
						{
							g_PlantsRendRand.GetInt();
							g_PlantsRendRand.GetInt();
							if(intensityVar)
								g_PlantsRendRand.GetInt();

							continue;
						}

						plantMatrix.MakeRotateZ(g_PlantsRendRand.GetRanged(0.0f, 1.0f)*2.0f*PI);// overwrites a,b,c
						plantMatrix.MakeTranslate(posPlant);									// overwrites d
						plantMatrix.Scale(scaleXY, scaleXY, scaleZ);	// scale in XY+Z


						// ---- muck about with the matrix to make atomic look blown by the wind --------
						g_PlantsRendRand.GetInt();
						// ----- end of wind stuff -----

						// color variation:
						if(intensityVar)
						{
							// intensity support:  final_I = I + VarI*rand01()
							u16 intensity = pCurrTriPlant->intensity + u16(float(intensityVar)*g_PlantsRendRand.GetRanged(0.0f, 1.0f));
							intensity = MIN(intensity, 255);

							Color32 col32;
	#define CALC_COLOR_INTENSITY(COLOR, I)			u8((u16(COLOR)*I) >> 8)
							col32.SetRed(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetRed(),		intensity));
							col32.SetGreen(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetGreen(),	intensity));
							col32.SetBlue(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetBlue(),	intensity));
	#undef CALC_COLOR_INTENSITY

							plantColourf.x = col32.GetRedf();
							plantColourf.y = col32.GetGreenf();
							plantColourf.z = col32.GetBluef();
						} // if(intensityVar)...


						customInstDraw.m_WorldMatrix.FromMatrix34(plantMatrix);
						customInstDraw.m_PlantColor.Set(plantColourf);
						customInstDraw.m_GroundColor.Set(groundColorf);

						const bool bOptimizedSetup = customInstDraw.UseOptimizedVsSetup(plantMatrix);
						if(bOptimizedSetup)
						{	// optimized setup: 3 registers:
							GRCDEVICE_SetVertexShaderConstant(gcmCon, 67, customInstDraw.GetRegisterPointerOpt(), customInstDraw.GetRegisterCountOpt());
						}
						else
						{	// full setup: 6 registers:
							GRCDEVICE_SetVertexShaderConstant(gcmCon, 64, customInstDraw.GetRegisterPointerFull(), customInstDraw.GetRegisterCountFull());
						}
						GRCDEVICE_DrawIndexedPrimitive(gcmCon, pPlantModelLOD1);
#if __DEV
						// rendering stats:
						(*pLocTriPlantsLOD1DrawnPerSlot)++;
#endif //__DEV
					}//for(int i=0; i<count; i++)...
					//////////////////////////////////////////////////////////////////

//					UnBindPlantShader();

				}// renderLOD1...
				////////////////////////////////////////////////////////////////////////////////////////////////////


				///////////// Render LOD2: /////////////////////////////////////////////////////////////////////////
	#if __DEV
				if((pCurrTriPlant->LODmask&0x04) && (!gbPlantsFlashLOD2 || (GRCDEVICE_GetFrameCounter&0x01)))
	#else
				if(pCurrTriPlant->LODmask&0x04)
	#endif
				{
					g_PlantsRendRand.Reset(pCurrTriPlant->seed);


					// Bind the grass shader
					//BindPlantShader(m_shdDeferredLOD2TechniqueID);
					CopyLocalCmdsToContext(gcmCon, localBindShaderLOD2Cmd, PNG_PLANTSMGR_LOCAL_BINDSHADERLOD2_MAXCMDSIZE);
					customInstDraw.Reset();

					// after binding shader - set other VS local variables now:

					// set color for whole pTriLoc:
					// m_Shader->SetVar(m_shdPlantColorID,		plantColourf);
					GRCDEVICE_SetVertexShaderConstant(gcmCon, GRASS_REG_PLANTCOLOR, (const float*)&plantColourf, 1);

					// set global micro-movement params: [ scaleH | scaleV | freqH | freqV ]
					// m_Shader->SetVar(m_shdUMovementParamsID, pCurrTriPlant->um_param);
					GRCDEVICE_SetVertexShaderConstant(gcmCon, GRASS_REG_UMPARAMS, (const float*)&pCurrTriPlant->um_param, 1);

					// set global collision radius scale:
					// m_Shader->SetVar(m_shdCollParamsID,		pCurrTriPlant->coll_params);
					GRCDEVICE_SetVertexShaderConstant(gcmCon, GRASS_REG_PLAYERCOLLPARAMS, (const float*)&pCurrTriPlant->coll_params, 1);

					// LOD2 billboard dimensions:
					Vector4 dimLOD2;
					dimLOD2.x = 1.0f;	//GeometryDimLOD2.x;
					dimLOD2.y = 1.0f;	//GeometryDimLOD2.y;
					dimLOD2.z = 0.0f;
					dimLOD2.w = 0.0f;
					// m_Shader->SetVar(m_shdDimensionLOD2ID,	dimLOD2);
					GRCDEVICE_SetVertexShaderConstant(gcmCon, GRASS_REG_DIMENSIONLOD2, (const float*)&dimLOD2, 1);

					// setup texture:
					// m_Shader->SetVar(m_shdTextureID,	pPlantTextureLOD1);
					CopyLocalCmdsToContext(gcmCon, pTextureCmdsLOD1, PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE);
		

					// Setup the vertex and index buffers
					//grcVertexBuffer* pVertexBuffer			= ms_plantLOD2VertexBuffer;
					//grcVertexDeclaration* vertexDeclaration	= ms_plantLOD2VertexDecl;
					//GRCDEVICE.SetVertexDeclaration(vertexDeclaration);
					//GRCDEVICE.SetStreamSource(0, *pVertexBuffer, 0, pVertexBuffer->GetVertexStride());
					CopyLocalCmdsToContext(gcmCon, localGeometryCmdsLOD2, PNG_PLANTSMGR_LOCAL_GEOMETRY_MAXCMDSIZE);
					

					//
					// render a single triPlant full of the desired model
					const int count = pCurrTriPlant->num_plants;
					for(int i=0; i<count; i++)
					{
						const float s = g_PlantsRendRand.GetRanged(0.0f, 1.0f);
						const float t = g_PlantsRendRand.GetRanged(0.0f, 1.0f);
						Vector3 posPlant;
						_GenPointInTriangle(&posPlant, v1, v2, v3, s, t);

						// calculate LOD2 distance:
						Vector3 LodDistV = posPlant - globalCameraPos; 
						const float fLodDist2 = LodDistV.Mag2();
						const bool bLodVisible = (fLodDist2 > globalLOD2CloseDist2)	&& (fLodDist2 < globalLOD2FarDist2);

						// early skip if current geometry not visible as LOD2:
						if(!bLodVisible)
						{
							g_PlantsRendRand.GetInt();
							g_PlantsRendRand.GetInt();
							g_PlantsRendRand.GetInt();
							g_PlantsRendRand.GetInt();
							if(intensityVar)
								g_PlantsRendRand.GetInt();

							continue;
						}

						// interpolate ground color(XYZ)+scale(W):
						_GenPointInTriangle(&groundColorf, &groundColorV1, &groundColorV2, &groundColorV3, s, t);
						//				const float groundScaleVar0 = groundColorf.w;
						groundColorf.w = 1.0f;

						// per-vertex scale:
						// <-5; 0) - scales Z only
						// <+0; 5> - scales XYZ
						//				const float groundScaleVarXY= (groundScaleVar0>0.0f) ? groundScaleVar0 : 1.0f;
						//				const float groundScaleVarZ	= Abs(groundScaleVar0);


						// ---- apply various amounts of scaling to the matrix -----
						// calculate an x/y scaling value and apply to matrix

						// final_SclXY = SclXY + SclVarXY*rand01()
						//				const float	scvariationXY	= pCurrTriPlant->scale_var_xy;
						//				const float	scaleXY			= (pCurrTriPlant->scale.x + scvariationXY*g_PlantsRendRand.GetRanged(0.0f, 1.0f))*groundScaleVarXY;
						g_PlantsRendRand.GetInt();

						// calculate a z scaling value and apply to matrix
						//				const float	scvariationZ	= pCurrTriPlant->scale_var_z;
						//				const float	scaleZ			= (pCurrTriPlant->scale.y + scvariationZ*g_PlantsRendRand.GetRanged(0.0f, 1.0f))*groundScaleVarZ;
						g_PlantsRendRand.GetInt();
						// ----- end of scaling stuff ------


						// choose bigger scale for bound frustum check:
						//				const float boundRadiusScale = (scaleXY > scaleZ)? (scaleXY) : (scaleZ);

						
						Vector4 sphere;
						sphere.Set(boundSphereLOD2);
						sphere.AddVector3XYZ(posPlant);
						//sphere.w *= boundRadiusScale;

						// skip current geometry if not visible:
						if(bGeomCullingTestEnabled && (!_IsSphereVisibleInline(sphere)))
						{
							g_PlantsRendRand.GetInt();
							g_PlantsRendRand.GetInt();
							if(intensityVar)
								g_PlantsRendRand.GetInt();

							continue;
						}

						//				plantMatrix.MakeRotateZ(g_PlantsRendRand.GetRanged(0.0f, 1.0f)*2.0f*PI);// overwrites a,b,c
						g_PlantsRendRand.GetInt();
						plantMatrix.Identity3x3();
						plantMatrix.MakeTranslate(posPlant);									// overwrites d
						//				plantMatrix.Scale(scaleXY, scaleXY, scaleZ);	// scale in XY+Z

						// ---- muck about with the matrix to make atomic look blown by the wind --------
						g_PlantsRendRand.GetInt();
						// ----- end of wind stuff -----

						// color variation:
						if(intensityVar)
						{
	#if 1
							// intensity support:  final_I = I + VarI*rand01()
							u16 intensity = pCurrTriPlant->intensity + u16(float(intensityVar)*g_PlantsRendRand.GetRanged(0.0f, 1.0f));
							intensity = MIN(intensity, 255);

							Color32 col32;
	#define CALC_COLOR_INTENSITY(COLOR, I)			u8((u16(COLOR)*I) >> 8)
							col32.SetRed(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetRed(),		intensity));
							col32.SetGreen(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetGreen(),	intensity));
							col32.SetBlue(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetBlue(),	intensity));
	#undef CALC_COLOR_INTENSITY

							plantColourf.x = col32.GetRedf();
							plantColourf.y = col32.GetGreenf();
							plantColourf.z = col32.GetBluef();
	#else
							g_PlantsRendRand.GetInt();
	#endif
						} // if(intensityVar)...


						customInstDraw.m_WorldMatrix.FromMatrix34(plantMatrix);
						customInstDraw.m_PlantColor.Set(plantColourf);
						customInstDraw.m_GroundColor.Set(groundColorf);

						const bool bOptimizedSetup = customInstDraw.UseOptimizedVsSetup(plantMatrix);
						if(bOptimizedSetup)
						{	// optimized setup: 3 registers:
							GRCDEVICE_SetVertexShaderConstant(gcmCon, 67, customInstDraw.GetRegisterPointerOpt(), customInstDraw.GetRegisterCountOpt());
							GRCDEVICE_DrawPrimitive(gcmCon, CELL_GCM_PRIMITIVE_QUADS, /*startVertex*/0, /*vertexCount*/4 );
						}
						else
						{	// full setup: 6 registers:
							GRCDEVICE_SetVertexShaderConstant(gcmCon, 64, customInstDraw.GetRegisterPointerFull(), customInstDraw.GetRegisterCountFull());
							GRCDEVICE_DrawPrimitive(gcmCon, CELL_GCM_PRIMITIVE_QUADS, /*startVertex*/0, /*vertexCount*/4 );
						}
	#if __DEV
						// rendering stats:
						(*pLocTriPlantsLOD2DrawnPerSlot)++;
	#endif //__DEV
					}//for(int i=0; i<count; i++)...
					//////////////////////////////////////////////////////////////////

					// UnBindPlantShader();

				}// renderLOD2's...
				////////////////////////////////////////////////////////////////////////////////////////////////////

	}// for(int i=0; i<numTriPlants; i++)...


//	SPU_DEBUGF(spu_printf("\n SPU %X: numPlants=%d, drawn=%d", SPUTASKID, numPlants, numPlantsDrawn));

}// end of processGrassTris()...




//
//
//
//
void spuDrawTriPlants(::rage::sysTaskParameters &taskParams)
{
	SPU_DEBUGF(Assert(sizeof(PPTriPlant)==PSN_SIZEOF_PPTRIPLANT));
	SPU_DEBUGF(Assert(sizeof(grassVertexSPU)==36));

	// Prolog:
	inMasterGrassParams		= (spuGrassParamsStructMaster*)taskParams.Input.Data;
	Assert(inMasterGrassParams);
	outMasterGrassParams	= (spuGrassParamsStructMaster*)taskParams.Output.Data;
	Assert(outMasterGrassParams);

	// synchronize out data:
	sysMemCpy(outMasterGrassParams, inMasterGrassParams, sizeof(spuGrassParamsStructMaster));

	outMasterGrassParams->m_DecrementerTicks = spu_read_decrementer();


	//	spu_write_decrementer(0x08000000);	// initialize decrementer to some known value
	g_PlantsRendRand.Reset(spu_read_decrementer());


	// copy shader bind commands to local store:
	CSpuDma::GetQuick(&localBindShaderLOD0Cmd[0], inMasterGrassParams->m_BindShaderLOD0CmdBuf,
		PNG_PLANTSMGR_LOCAL_BINDSHADERLOD0_MAXCMDSIZE*sizeof(u32), PLANTS_DMATAGID);
	CSpuDma::GetQuick(&localBindShaderLOD1Cmd[0], inMasterGrassParams->m_BindShaderLOD1CmdBuf,
		PNG_PLANTSMGR_LOCAL_BINDSHADERLOD1_MAXCMDSIZE*sizeof(u32), PLANTS_DMATAGID);
	CSpuDma::GetQuick(&localBindShaderLOD2Cmd[0], inMasterGrassParams->m_BindShaderLOD2CmdBuf,
		PNG_PLANTSMGR_LOCAL_BINDSHADERLOD2_MAXCMDSIZE*sizeof(u32), PLANTS_DMATAGID);


	Assert(PNG_PLANT_NUM_PLANT_SLOTS==4);
	Assert(sizeof(_localPlantModelsSlot0)	== PNG_PLANTSMGR_LOCAL_SLOTSIZE_MODEL);
	Assert(sizeof(_localPlantModelsSlot1)	== PNG_PLANTSMGR_LOCAL_SLOTSIZE_MODEL);
	Assert(sizeof(_localPlantModelsSlot2)	== PNG_PLANTSMGR_LOCAL_SLOTSIZE_MODEL);
	Assert(sizeof(_localPlantModelsSlot3)	== PNG_PLANTSMGR_LOCAL_SLOTSIZE_MODEL);
	Assert(sizeof(localPlantTexturesSlot0)	== PNG_PLANTSMGR_LOCAL_SLOTSIZE_TEXTURE);
	Assert(sizeof(localPlantTexturesSlot1)	== PNG_PLANTSMGR_LOCAL_SLOTSIZE_TEXTURE);
	Assert(sizeof(localPlantTexturesSlot2)	== PNG_PLANTSMGR_LOCAL_SLOTSIZE_TEXTURE);
	Assert(sizeof(localPlantTexturesSlot3)	== PNG_PLANTSMGR_LOCAL_SLOTSIZE_TEXTURE);

	// grab all models:
	CSpuDma::GetQuick(&localPlantModelsSlot0[0], inMasterGrassParams->m_PlantModelsTab[0],
				PNG_PLANTSMGR_LOCAL_SLOTSIZE_MODEL, PLANTS_DMATAGID);
	CSpuDma::GetQuick(&localPlantModelsSlot1[0], inMasterGrassParams->m_PlantModelsTab[1],
				PNG_PLANTSMGR_LOCAL_SLOTSIZE_MODEL, PLANTS_DMATAGID);
	CSpuDma::GetQuick(&localPlantModelsSlot2[0], inMasterGrassParams->m_PlantModelsTab[2],
				PNG_PLANTSMGR_LOCAL_SLOTSIZE_MODEL, PLANTS_DMATAGID);
	CSpuDma::GetQuick(&localPlantModelsSlot3[0], inMasterGrassParams->m_PlantModelsTab[3],
				PNG_PLANTSMGR_LOCAL_SLOTSIZE_MODEL, PLANTS_DMATAGID);

	// grab LOD2 geometry:
	CSpuDma::GetQuick(&localGeometryCmdsLOD2[0], inMasterGrassParams->m_GeometryCmdLOD2,
				PNG_PLANTSMGR_LOCAL_GEOMETRY_MAXCMDSIZE*sizeof(u32), PLANTS_DMATAGID);

	// grab all textures:
	CSpuDma::GetQuick(&localPlantTexturesSlot0[0], inMasterGrassParams->m_PlantTexturesTab[0],
				PNG_PLANTSMGR_LOCAL_SLOTSIZE_TEXTURE, PLANTS_DMATAGID);
	CSpuDma::GetQuick(&localPlantTexturesSlot1[0], inMasterGrassParams->m_PlantTexturesTab[1],
				PNG_PLANTSMGR_LOCAL_SLOTSIZE_TEXTURE, PLANTS_DMATAGID);
	CSpuDma::GetQuick(&localPlantTexturesSlot2[0], inMasterGrassParams->m_PlantTexturesTab[2],
				PNG_PLANTSMGR_LOCAL_SLOTSIZE_TEXTURE, PLANTS_DMATAGID);
	CSpuDma::GetQuick(&localPlantTexturesSlot3[0], inMasterGrassParams->m_PlantTexturesTab[3],
				PNG_PLANTSMGR_LOCAL_SLOTSIZE_TEXTURE, PLANTS_DMATAGID);

	// resolve pointers:
	localPlantModelsTab[0]	= &localPlantModelsSlot0[0];
	localPlantModelsTab[1]	= &localPlantModelsSlot1[0];
	localPlantModelsTab[2]	= &localPlantModelsSlot2[0];
	localPlantModelsTab[3]	= &localPlantModelsSlot3[0];
	localPlantTexturesTab[0]= &localPlantTexturesSlot0[0];
	localPlantTexturesTab[1]= &localPlantTexturesSlot1[0];
	localPlantTexturesTab[2]= &localPlantTexturesSlot2[0];
	localPlantTexturesTab[3]= &localPlantTexturesSlot3[0];




	sm_nNumGrassJobsLaunched = inMasterGrassParams->m_nNumGrassJobsLaunched;
	SPU_DEBUGF(spu_printf("\n SPU %X: taskid=%X", SPUTASKID, SPUTASKID));
//	SPU_DEBUGF(spu_printf("\n SPU %X: numjobs=%d", SPUTASKID, sm_nNumGrassJobsLaunched));

	eaRsxLabel[0]	= inMasterGrassParams->m_eaRsxLabel[0];
	eaRsxLabel[1]	= inMasterGrassParams->m_eaRsxLabel[1];
	eaRsxLabel[2]	= inMasterGrassParams->m_eaRsxLabel[2];
	eaRsxLabel[3]	= inMasterGrassParams->m_eaRsxLabel[3];
	eaRsxLabel5		= inMasterGrassParams->m_eaRsxLabel[4];

	rsxLabelIndex[0]= inMasterGrassParams->m_rsxLabelIndices[0];
	rsxLabelIndex[1]= inMasterGrassParams->m_rsxLabelIndices[1];
	rsxLabelIndex[2]= inMasterGrassParams->m_rsxLabelIndices[2];
	rsxLabelIndex[3]= inMasterGrassParams->m_rsxLabelIndices[3];
	rsxLabelIndex5	= inMasterGrassParams->m_rsxLabelIndices[4];

	rsxLabels[0]	= &_rsxLabel1;
	rsxLabels[1]	= &_rsxLabel2;
	rsxLabels[2]	= &_rsxLabel3;
	rsxLabels[3]	= &_rsxLabel4;


	eaHeapBegin				= inMasterGrassParams->m_heapBegin;
	heapBeginOffset			= inMasterGrassParams->m_heapBeginOffset;

	eaHeapBeginCurrent		= eaHeapBegin;
	heapBeginOffsetCurrent	= heapBeginOffset;

	eaHeapOverfilled		= 0;

//	const u32 heapGap= PNG_PLANTSMGR_LOCAL_HEAP_GAP_SIZE;	// 4KB gap between RSX buffers
//	eaHeapBuffers[0]	= heapBegin;
//	eaHeapBuffers[1]	= heapBegin			+ (PNG_PLANTSMGR_LOCAL_HEAP_SIZE + heapGap)*1;	// 4KB space between them
//	eaHeapBuffers[2]	= heapBegin			+ (PNG_PLANTSMGR_LOCAL_HEAP_SIZE + heapGap)*2;	// 4KB space between them
//	eaHeapBuffers[3]	= heapBegin			+ (PNG_PLANTSMGR_LOCAL_HEAP_SIZE + heapGap)*3;	// 4KB space between them

//	heapBufferOffsets[0]= heapBeginOffset;
//	heapBufferOffsets[1]= heapBeginOffset	+ (PNG_PLANTSMGR_LOCAL_HEAP_SIZE + heapGap)*1;
//	heapBufferOffsets[2]= heapBeginOffset	+ (PNG_PLANTSMGR_LOCAL_HEAP_SIZE + heapGap)*2;
//	heapBufferOffsets[3]= heapBeginOffset	+ (PNG_PLANTSMGR_LOCAL_HEAP_SIZE + heapGap)*3;

//	offsetJTS[0]	= 0;
//	offsetJTS[1]	= 0;
//	offsetJTS[2]	= 0;
//	offsetJTS[3]	= 0;
	
	localSolidHeap[0] = &_localSolidHeap0[0];
	localSolidHeap[1] = &_localSolidHeap1[0];

	localDmaTagID[0]	= PLANTS_DMATAGID1;
	localDmaTagID[1]	= PLANTS_DMATAGID2;


	globalCameraPos.x	= inMasterGrassParams->m_vecCameraPos.x;
	globalCameraPos.y	= inMasterGrassParams->m_vecCameraPos.y;
	globalCameraPos.z	= inMasterGrassParams->m_vecCameraPos.z;
	globalCameraPos.w	= 0.0f;


	// copy frustum planes:
	for(int i=0; i<6; i++)
	{
		gFrustumClipPlanes[i] = inMasterGrassParams->m_vecFrustumPlanes[i];
	}

	gFrustumTPlane0[0].Set(gFrustumClipPlanes[spuCLIP_PLANE_NEAR].x,gFrustumClipPlanes[spuCLIP_PLANE_LEFT].x,gFrustumClipPlanes[spuCLIP_PLANE_RIGHT].x,0.0f);
	gFrustumTPlane0[1].Set(gFrustumClipPlanes[spuCLIP_PLANE_NEAR].y,gFrustumClipPlanes[spuCLIP_PLANE_LEFT].y,gFrustumClipPlanes[spuCLIP_PLANE_RIGHT].y,0.0f);
	gFrustumTPlane0[2].Set(gFrustumClipPlanes[spuCLIP_PLANE_NEAR].z,gFrustumClipPlanes[spuCLIP_PLANE_LEFT].z,gFrustumClipPlanes[spuCLIP_PLANE_RIGHT].z,0.0f);
	gFrustumTPlane0[3].Set(gFrustumClipPlanes[spuCLIP_PLANE_NEAR].w,gFrustumClipPlanes[spuCLIP_PLANE_LEFT].w,gFrustumClipPlanes[spuCLIP_PLANE_RIGHT].w,0.0f);

	gFrustumTPlane1[0].Set(gFrustumClipPlanes[spuCLIP_PLANE_FAR].x,gFrustumClipPlanes[spuCLIP_PLANE_TOP].x,gFrustumClipPlanes[spuCLIP_PLANE_BOTTOM].x,0.0f);
	gFrustumTPlane1[1].Set(gFrustumClipPlanes[spuCLIP_PLANE_FAR].y,gFrustumClipPlanes[spuCLIP_PLANE_TOP].y,gFrustumClipPlanes[spuCLIP_PLANE_BOTTOM].y,0.0f);
	gFrustumTPlane1[2].Set(gFrustumClipPlanes[spuCLIP_PLANE_FAR].z,gFrustumClipPlanes[spuCLIP_PLANE_TOP].z,gFrustumClipPlanes[spuCLIP_PLANE_BOTTOM].z,0.0f);
	gFrustumTPlane1[3].Set(gFrustumClipPlanes[spuCLIP_PLANE_FAR].w,gFrustumClipPlanes[spuCLIP_PLANE_TOP].w,gFrustumClipPlanes[spuCLIP_PLANE_BOTTOM].w,0.0f);


	// finish all loading to LS:
	CSpuDma::Wait(PLANTS_DMATAGID);


	// setup main local context:
	CellGcmContext		localCon;
	SetupContextData32(&localCon, localSolidHeap[0], PNG_PLANTSMGR_LOCAL_HEAP_SIZE, &CustomGcmReserveFailed);
	//bufferID	= 0;
	spuBufferID	= 0;
	bCustomGcmReserveFailedProlog	= true;	// prolog flag

//	localCon.SetWriteCommandLabel(rsxLabelIndex[0], 1);	// RSX: Lock1();


//////////////////////////////////////////////////////////////////
	for(u32 jobID=0; jobID<sm_nNumGrassJobsLaunched; jobID++)
	{
		// execute single job here:
		CSpuDma::GetQuick(&inGrassParamsT, inMasterGrassParams->m_inSpuGrassStructTab+jobID, sizeof(spuGrassParamsStruct), PLANTS_DMATAGID);
		CSpuDma::Wait(PLANTS_DMATAGID);

		spuGrassParamsStruct *inGrassParams		= &inGrassParamsT;
		spuGrassParamsStruct *outGrassParams	= &outGrassParamsT;

		// copy PPTriPlants struct to local store:
		CSpuDma::GetQuick(&localTriPlantTab[0], inGrassParams->m_pTriPlant, inGrassParams->m_TriPlantDmaSize*inGrassParams->m_numTriPlant, PLANTS_DMATAGID);
		CSpuDma::Wait(PLANTS_DMATAGID);


		// first wait for RSX to get where we expect it in CB:
//		spuWaitForLabel(&rsxLabel5, eaRsxLabel5, inMasterGrassParams->m_rsxLabel5_CurrentTaskID);
		//SPU_DEBUGF(spu_printf("\n SPU %X: Waiting for label5 finished!", SPUTASKID));

		processGrassTris(inGrassParams, &localCon);

	}//	for(u32 jobID=0; jobID<sm_nNumGrassJobsLaunched; jobID++)...
////////////////////////////////////////////////////////////////////////


	// Epilog:
	//localCon.SetWriteCommandLabel(rsxLabelIndex5, SPUTASKID+0x1000000);	// write debug value so we know RSX finished executing and returned to main CB
	localCon.SetJumpCommand(inMasterGrassParams->m_ReturnOffsetToMainCB);	// jump back to main CB
	CustomGcmReserveFailed((CellGcmContextData*)&localCon, 0);	// start copying current buffer

	bCustomGcmReserveFailedEpilog	= true;	// this is last call
	CustomGcmReserveFailed((CellGcmContextData*)&localCon, 0);	// wait for end of current buffer's dma copy

	// synchronize with RSX:
//	u32 timeWaitForRSX = spu_read_decrementer();
//	{
//		spuWaitForLabel(&rsxLabel5, eaRsxLabel5, inMasterGrassParams->m_rsxLabel5_CurrentTaskID);
//		SPU_DEBUGF(spu_printf("\n SPU %X: Waiting for label5 finished!", SPUTASKID));
//	}
//	timeWaitForRSX -= spu_read_decrementer();

	if(eaHeapOverfilled)
	{	// clear to NOP and do not execute (possibly incomplete) this big buffer:
		spuClearJTSinBuffer(inMasterGrassParams->m_eaMainJTS, 0x00000000);
	}
	else
	{	// clear JTS + jump to our big buffer:
		spuClearJTSinBuffer(inMasterGrassParams->m_eaMainJTS, heapBeginOffset);
	}

	SPU_DEBUGF(spu_printf("\n SPU %X: end: returnOffset=0x%X, eaMainJTS=0x%X", SPUTASKID, inMasterGrassParams->m_ReturnOffsetToMainCB,inMasterGrassParams->m_eaMainJTS));
	SPU_DEBUGF(spu_printf("\n SPU %X: end: BigBuffer: PPU: s=0x%X, c=0x%X, e=0x%X", SPUTASKID, eaHeapBegin,eaHeapBeginCurrent,eaHeapBegin+PNG_PLANTSMGR_BIG_HEAP_SIZE));
	SPU_DEBUGF(spu_printf("\n SPU %X: end: BigBuffer: RSX: s=0x%X, c=0x%X, e=0x%X", SPUTASKID, heapBeginOffset,heapBeginOffsetCurrent,heapBeginOffset+PNG_PLANTSMGR_BIG_HEAP_SIZE));

#if 0
	sys_time_get_timebase_frequency();
	Anyway I tried on 085 and 092 with this code in an SPU job:
	// Wait 5 seconds at 80 MHz
	uint32_t start = spu_readch(SPU_RdDec);
	uint32_t time;
	do {
		time = spu_readch(SPU_RdDec);
	} while(start-time<79800000*5);
	and the job takes exactly 5 seconds
#endif

	// amount of consumed bytes in big buffer:
	outMasterGrassParams->m_AmountOfBigHeapConsumed = eaHeapBeginCurrent - eaHeapBegin;

	// number of ticks it took to execute this job:
	outMasterGrassParams->m_DecrementerTicks = outMasterGrassParams->m_DecrementerTicks - spu_read_decrementer();

	SPU_DEBUGF(spu_printf("\n SPU%X: waited for RSX for %d ticks (%.2f%% of total job time %d ticks).", SPUTASKID, timeWaitForRSX, float(timeWaitForRSX)/float(outMasterGrassParams->m_DecrementerTicks)*100.0f, outMasterGrassParams->m_DecrementerTicks));

	SPU_DEBUGF(spu_printf("\n SPU%X: consumed %dKB", SPUTASKID, (eaHeapBeginCurrent-eaHeapBegin)/1024));
	if(eaHeapOverfilled)
		SPU_DEBUGF1(spu_printf("\n SPU%X: overfilled by %dKB!", SPUTASKID, eaHeapOverfilled/1024));
	
	return;
}// end of spuDrawTriPlants()...

#else //PNG_PLANTMGR_SPU_ENABLED...
void spuDrawTriPlants(::rage::sysTaskParameters &)
{
	return;
}
#endif //PNG_PLANTMGR_SPU_ENABLED...
#endif //__SPU....
