//
// plantsgrass/proceduralinfo.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef PLANTSGRASS_PROCEDURALINFO_H
#define	PLANTSGRASS_PROCEDURALINFO_H

#include "plantsgrass/vfxlist.h"
#include "vector/color32.h"

#define MAX_PROCEDURAL_TAGS			(255)
#define MAX_PROCEDURAL_TAG_LENGTH	(64)
#define	MAX_PROC_OBJ_INFOS			(256)
#define MAX_PLANT_INFOS				(128)

namespace rage
{

struct pngProcObjData
{
	s32				modelIndex;
	float 			spacing;
	float 			density;
	float			minDistSq;
	float			minXRot;
	float			maxXRot;
	float			minYRot;
	float			maxYRot;
	float			minZRot;
	float			maxZRot;
	float			minScale;
	float			maxScale;
	float			minScaleZ;
	float			maxScaleZ;
	float			zOffMin;
	float			zOffMax;
	bool			alignObj;
	bool			useGrid;
	bool			useSeed;
	bool			isFloating;
};


struct pngProcTagLookup
{
	s32				procObjIndex;
	s32				plantIndex;

};

struct pngProcObjInfo
{
	s32				m_procTagId;
	pngProcObjData	m_data;
	pngList			m_entityList;
};

class pngPlantInfo 
{
	friend class pngProceduralInfo;
	friend class pngPlantMgr;
	friend class pngPlantLocTri;
private:
	s32 	m_procTagId;
	Color32	m_rgbaColor;		// R, G, B, A
	float	m_fScaleXY;			// scale XY
	float	m_fScaleZ;			// scale Z
	float	m_fScaleVarXY;		// scale variation XY
	float	m_fScaleVarZ;		// scale variation Z
	float	m_fUmScaleH;		// micro-movements: global horizontal scale
	float	m_fUmScaleV;		// micro-movements: global vertical scale
	float	m_fUmFreqH;			// micro-movements: global horizontal frequency
	float	m_fUmFreqV;			// micro-movements: global vertical frequency
	float	m_fWindBendScale;	// wind bend scale
	float	m_fWindBendVar;		// wind bend variation
	float	m_fCollRadiusScale;	// collision radius scale
	float	m_fDensity;			// num of plants per sqm
	u16		m_nPlantSlotID;		// defines model set + "big" texture ID
	u16		m_nModelID;			// model_id of the plant
	u16		m_nTextureID;		// texture number to use for the model (was UV offset for PS2)
	u8		m_nLODmask;			// bitmask for enabled LODs
	u8		m_nIntensity;		// Intensity
	u8		m_nIntensityVar;	// Intensity Variation
};

class pngProceduralInfo
{
	friend class pngProcObjectMan;
	friend class pngPlantMgr;
	friend class pngPlantLocTri;
public:
	bool				InitLevel					();
	void				ShutdownLevel				();

	bool				CreatesProcObjects			(s32 procTagId);
	bool				CreatesPlants				(s32 procTagId);

#if __BANK
	char*				GetProcTagName				(s32 index)					{return m_procTagNames[index];}
#endif

#if !__SPU
private:
#endif

	s32 ProcessProcTag (char procTagNames[MAX_PROCEDURAL_TAGS][MAX_PROCEDURAL_TAG_LENGTH], char* newProcTag, char* currProcTag, s32 type);

	float				m_version;
	s32					m_numProcObjInfos;
	pngProcObjInfo		m_procObjInfos[MAX_PROC_OBJ_INFOS];
	s32					m_numPlantInfos;
	pngPlantInfo		m_plantInfos[MAX_PLANT_INFOS];
	s32					m_numProcTags;
	pngProcTagLookup	m_procTagTable[MAX_PROCEDURAL_TAGS];

#if __BANK
	// static so it doesn't affect SPU
	static char			m_procTagNames				[MAX_PROCEDURAL_TAGS][MAX_PROCEDURAL_TAG_LENGTH];
#endif

} ;

extern pngProceduralInfo g_procInfo;

} // namespace rage

#endif // PLANTSGRASS_PROCEDURALINFO_H
