//
// plantsgrass/plantsgrassrenderer.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef PLANTSGRASS_RENDERER_H
#define PLANTSGRASS_RENDERER_H

#include "plantsgrass/plantsmgr.h"

#define PPTRIPLANT_MODELS_TAB_SIZE		(PNG_PLANT_NUM_PLANT_SLOTS)
#if PNG_PLANTSMGR_USE_SPU_RENDERING
#define	PPTRIPLANT_BUFFER_SIZE		(128)
#else
#define	PPTRIPLANT_BUFFER_SIZE		(32)
#endif

namespace rage
{

// fake typedef
SPU_ONLY(typedef u32 grcTexture;)

struct PPTriPlant
{
public:
    Vector3		V1, V2, V3;			// 3 vertices
	Vector3		center;				// center of the plant triangle		

    u16		model_id;			// model_id used to calculate offset address for model in VU mem
	u16		num_plants;			// num plants to generate

    Vector2		scale;				// x=SxSy, y=Sz

#if PNG_PLANTSMGR_USE_SPU_RENDERING
	u8		texture_id;			// number of texture to use
	u8		textureLOD1_id;		// number of LOD1 texture to use
	u8		__pad001;
	u8		__pad002;
#else
	grcTexture	*texture_ptr;		// ptr to texture to use
	grcTexture	*textureLOD1_ptr;	// ptr to textureLOD1 to use
#endif

    Color32		color;				// color of the model
    
    u8		intensity;			// scale value for the colour
    u8		intensity_var;		// variation of intensity value
	u8		LODmask;			// bitmask for enabled LODs
	u8		__pad00;

    u32		seed;				// seed starting value for this triangle;
    float		scale_var_xy;		// scale variation XY
    float		scale_var_z;		// scale variation Z

	Vector4		um_param;			// micro-movement params: [ scaleH | scaleV | freqH | freqV ]
	Vector2		coll_params;		// collision params:  [radiusSqr | invRadiusSqr]

	float		wind_bend_scale;	// wind bending scale
	float		wind_bend_var;		// wind bending variation

	Color32		groundColorV1, groundColorV2, groundColorV3;

#if __SPU || PNG_PLANTSMGR_USE_SPU_RENDERING
	// SPU: size of this struct must be dma'able
	#define PSN_SIZEOF_PPTRIPLANT			(160)
#endif //PNG_PLANTSMGR_USE_SPU_RENDERING...
};

#if !__SPU
class pngPPTriPlantBuffer;

class pngGrassRenderer
{
	friend class pngPPTriPlantBuffer;
public:
	pngGrassRenderer();
	~pngGrassRenderer();

	static bool 			Initialise();
	static void 			Shutdown();

#if	PNG_PLANTSMGR_USE_SPU_RENDERING
	static bool				SpuInitialisePerRenderFrame();
	static bool				SpuFinalisePerRenderFrame();
	static bool				SpuRecordGrassTexture(u32 slotID, u32 texIndex, grcTexture *pTexture);
	static bool				SpuRecordGeometries();
	static bool				SpuRecordGeometry(void *con1, grmGeometry *pGeometry, u32 *dstCmd, u32 maxCmdSize, u32 numExtraNOPs);
	static bool				SpuRecordGeometry(void *con1, grcVertexBuffer* pVertexBuffer, grcVertexDeclaration* pVertexDeclaration, u32 *dstCmd, u32 maxCmdSize, u32 numExtraNOPs);
	static bool				SpuRecordShaders();
	static bool				SpuRecordShaderBind(void *con1, grcEffectTechnique forcedTech, u32 *dstCmd, u32 maxCmdSize, u32 numExtraNOPs);
	static bool				SpuRecordShaderUnBind(void *con1);
#else
	static bool				SpuInitialisePerRenderFrame()															{ return(true); }
	static bool				SpuFinalisePerRenderFrame()																{ return(true);	}
	static bool				SpuRecordGrassTexture(u32,u32,grcTexture*)										{ return(true); }
	static bool				SpuRecordGeometries()																	{ return(true);	}
	static bool				SpuRecordGeometry(void*,grmGeometry*,u32*,u32,u32)								{ return(true);	}
	static bool				SpuRecordGeometry(void*,grcVertexBuffer*,grcVertexDeclaration*,u32*,u32,u32)	{ return(true);	}
	static bool				SpuRecordShaders()																		{ return(true);	}
	static bool				SpuRecordShaderBind(void*, grcEffectTechnique, u32*, u32, u32)					{ return(true);	}
	static bool				SpuRecordShaderUnBind(void*)															{ return(true);	}
#endif

	static void				AddTriPlant(PPTriPlant *pPlant, u32 ePlantModelSet);
	static void				FlushTriPlantBuffer();

	inline void static		GenPointInTriangle(Vector3 *pRes, const Vector3 *V1, const Vector3 *V2, const Vector3 *V3, float s, float t);
	inline void static		GenPointInTriangle(Vector4 *pRes, const Vector4 *V1, const Vector4 *V2, const Vector4 *V3, float s, float t);

	static bool				SetPlantModelsTab(u32 index, pngGrassModel *plantModels);
	static pngGrassModel*		GetPlantModelsTab(u32 index);

	static void				SetGlobalCameraPos(const Vector3& camPos);
	static const Vector3&	GetGlobalCameraPos();
	
	static void				SetGlobalPlayerPos(const Vector3& playerPos);
	static const Vector3&	GetGlobalPlayerPos();
	
	static void				UpdateGlobalCameraPos();
	static void				UpdateGlobalPlayerPos();

	static void				SetGlobalVehCollisionParams(bool bEnable, const Vector3& vecB, const Vector3& vecM, float radius, float groundZ);
	static void				GetGlobalVehCollisionParams(bool *bEnable, Vector4 *vecB, Vector4 *vecM, Vector4 *vecR);

	static void				SetGlobalWindBending(float bending);
	static float			GetGlobalWindBending();

	static void				SetCurrentScanCode(u16 currScanCode);
	static void				SetGeometryLOD2(grcVertexBuffer	*vb, grcVertexDeclaration *decl);

	static grmShaderGroup*		GetGrassShaderGroup();
	static grmShader*			GetGrassShader();

private:
#if PNG_PLANTSMGR_USE_SPU_RENDERING
	static bool				DrawTriPlantsSPU(PPTriPlant *triPlants, int numTriPlants, u32 slotID);
#else
	static bool 			DrawTriPlants(PPTriPlant *triPlants, int numTris, pngGrassModel* plantModelsTab);
#endif //PNG_PLANTSMGR_USE_SPU_RENDERING...
	static void				BindPlantShader(grcEffectTechnique forcedTech);
	static void				UnBindPlantShader();

	static Vector3			sm_vecCameraPos[2];
	static Vector3			sm_vecPlayerPos[2];

	static bool				sm_bVehCollisionEnabled[2];
	static Vector4			sm_vecVehCollisionB[2];
	static Vector4			sm_vecVehCollisionM[2];
	static Vector4			sm_vecVehCollisionR[2];

	static float			sm_windBending[2];

	// LOD2 vertex buffer:
	static grcVertexBuffer	*sm_plantLOD2VertexBuffer;
	static grcVertexDeclaration	*sm_plantLOD2VertexDecl;
};

enum
{
	PPPLANTBUF_MODEL_SET0	= 0,
	PPPLANTBUF_MODEL_SET1	= 1,
	PPPLANTBUF_MODEL_SET2	= 2,
	PPPLANTBUF_MODEL_SET3	= 3	
};

class pngPPTriPlantBuffer
{
public:
	pngPPTriPlantBuffer();

public:
	void Flush();
	PPTriPlant* GetPPTriPlantPtr(int amountToAdd = 1);
	void ChangeCurrentPlantModelsSet(int newSet);
	void IncreaseBufferIndex(int pipeMode, int amount = 1);
	//
	// access to m_pPlantModelsTab[]:
	//
#if PNG_PLANTSMGR_USE_SPU_RENDERING
	bool AllocateTextureBuffers();
	void DestroyTextureBuffers();
	bool SetPlantTexturesCmd(u32 slotid, u32 index, u32 *cmd);
	u32* GetPlantTexturesCmd(u32 slotid, u32 index);
#endif //PNG_PLANTSMGR_USE_SPU_RENDERING...
	bool SetPlantModelsTab(u32 index, pngGrassModel* pPlantModels);
	pngGrassModel* GetPlantModelsTab(u32 index);

	//
	// current plant models set:
	//
	inline void SetPlantModelsSet(int set)
	{
		m_plantModelsSet = set;
	}
	inline int GetPlantModelsSet() const
	{
		return(this->m_plantModelsSet);
	}
	inline pngGrassModel* GetPlantModels(u32 i)
	{
		Assert(i < PPTRIPLANT_MODELS_TAB_SIZE);
		return m_pPlantModelsTab[i];
	}

private:
	int m_currentIndex;
	PPTriPlant m_Buffer[PPTRIPLANT_BUFFER_SIZE];

	int m_plantModelsSet;

#if PNG_PLANTSMGR_USE_SPU_RENDERING
	u32* m_pPlantsTexturesCmd0;
	u32* m_pPlantsTexturesCmd1;
	u32* m_pPlantsTexturesCmd2;
	u32* m_pPlantsTexturesCmd3;
	u32* m_pPlantsTexturesCmdTab[PPTRIPLANT_MODELS_TAB_SIZE];
#endif //PNG_PLANTSMGR_USE_SPU_RENDERING...
	pngGrassModel* m_pPlantModelsTab[PPTRIPLANT_MODELS_TAB_SIZE];
};

//
//
// translated from Andrez's VU code...
//
// Based on Turk's algorithm:
//
// s = rand[0; 1]
// t = rand[0; 1]
//
inline
void pngGrassRenderer::GenPointInTriangle(Vector3 *pRes, const Vector3 *V1, const Vector3 *V2, const Vector3 *V3, float s, float t)
{
	if((s+t) > 1.0f)
	{
		s = 1.0f - s;
		t = 1.0f - t;
	}
	
	const float a = 1.0f - s - t;
	const float b = s;
	const float c = t;

	pRes->x = a*V1->x + b*V2->x + c*V3->x;
	pRes->y = a*V1->y + b*V2->y + c*V3->y;
	pRes->z = a*V1->z + b*V2->z + c*V3->z;
}

inline void pngGrassRenderer::GenPointInTriangle(Vector4 *pRes, const Vector4 *V1, const Vector4 *V2, const Vector4 *V3, float s, float t)
{
	if((s+t) > 1.0f)
	{
		s = 1.0f - s;
		t = 1.0f - t;
	}

	const float a = 1.0f - s - t;
	const float b = s;
	const float c = t;

	pRes->x = a*V1->x + b*V2->x + c*V3->x;
	pRes->y = a*V1->y + b*V2->y + c*V3->y;
	pRes->z = a*V1->z + b*V2->z + c*V3->z;
	pRes->w = a*V1->w + b*V2->w + c*V3->w;
}
#endif // !__SPU

} // namespace rage

#endif // PLANTSGRASS_RENDERER_H
