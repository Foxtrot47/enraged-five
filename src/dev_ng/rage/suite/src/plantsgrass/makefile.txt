Project plantsgrass

IncludePath $(RAGE_DIR)\base\src

Files {
	plantsgrassrenderer.cpp
	plantsgrassrenderer.h
	plantsgrassrendererspu.cpp
	plantsgrassrendererspu.h
	plantsmgr.cpp
	plantsmgr.h
	plantsmgrspu.cpp
	procobjects.cpp
	procobjects.h
	proceduralinfo.cpp
	proceduralinfo.h
	vfxlist.cpp
	vfxlist.h
}
Custom {
	plantsgrassrendererspu.job
	plantsmgrspu.job
	plantsmgrupdatespu.job
}
