//
// plantsgrass/plantsgrassrenderer.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "grcore/grcorespu.h"
#include "grcore/im.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"
#include "grcore/viewport.h"
#include "grcore/viewport_inline.h"
#include "grcore/wrapper_gcm.h"
#include "grmodel/geometry.h"
#include "grmodel/shader.h"
#include "math/random.h"
#include "phbound/boundgeom.h"
#include "physics/inst.h"
#include "plantsgrass/plantsgrassrenderer.h"
#include "rmcore/drawable.h"
#include "rmcore/instance.h"
#include "system/task.h"
#include "system/timer.h"

#if PNG_PLANTSMGR_USE_SPU_RENDERING
#include "plantsgrass/plantsgrassrendererspu.h"
#include "grcore/wrapper_gcm.h"
#include "grmodel/modelfactory.h"

using namespace cell::Gcm;
#endif // PNG_PLANTSMGR_USE_SPU_RENDERING

#define TweakBool bool

CompileTimeAssert(SPU_GCM_FIFO >= 1); // always make sure we have SPU_GCM_FIFO

namespace rage
{

Vector3					pngGrassRenderer::sm_vecCameraPos[2]			= { Vector3(0,0,0) };
Vector3					pngGrassRenderer::sm_vecPlayerPos[2]			= { Vector3(0,0,0) };
float					pngGrassRenderer::sm_windBending[2]				= { 0.0f };
bool					pngGrassRenderer::sm_bVehCollisionEnabled[2]	= { false };
Vector4					pngGrassRenderer::sm_vecVehCollisionB[2]		= { Vector4(0,0,0,0) };
Vector4					pngGrassRenderer::sm_vecVehCollisionM[2]		= { Vector4(0,0,0,0) };
Vector4					pngGrassRenderer::sm_vecVehCollisionR[2]		= { Vector4(0,0,0,0) };
grcVertexBuffer*		pngGrassRenderer::sm_plantLOD2VertexBuffer		= NULL;
grcVertexDeclaration*	pngGrassRenderer::sm_plantLOD2VertexDecl		= NULL;

static pngPPTriPlantBuffer	s_PPTriPlantBuffer;
static grmShaderGroup*		m_ShaderGroup					= NULL;
static grmShader*			m_Shader						= NULL;
static grcEffectVar			m_shdTextureID					= grcevNONE;
static grcEffectGlobalVar	m_shdCameraPosID				= grcegvNONE;
static grcEffectGlobalVar	m_shdPlayerPosID				= grcegvNONE;
static grcEffectVar			m_shdCollParamsID				= grcevNONE;
static grcEffectGlobalVar	m_shdVehCollisionEnabledID		= grcegvNONE;
static grcEffectGlobalVar	m_shdVehCollisionBID			= grcegvNONE;
static grcEffectGlobalVar	m_shdVehCollisionMID			= grcegvNONE;
static grcEffectGlobalVar	m_shdVehCollisionRID			= grcegvNONE;
static grcEffectVar			m_shdFadeAlphaDistUmTimerID		= grcevNONE;
static grcEffectVar			m_shdFadeAlphaLOD1DistID		= grcevNONE;
static grcEffectVar			m_shdFadeAlphaLOD2DistID		= grcevNONE;
static grcEffectVar			m_shdPlantColorID				= grcevNONE;
static grcEffectVar			m_shdUMovementParamsID			= grcevNONE;
static grcEffectVar			m_shdDimensionLOD2ID			= grcevNONE;
static grcEffectTechnique	m_shdDeferredLOD0TechniqueID	= grcetNONE;
static grcEffectTechnique	m_shdDeferredLOD1TechniqueID	= grcetNONE;
static grcEffectTechnique	m_shdDeferredLOD2TechniqueID	= grcetNONE;
// static u16 					m_currScanCode					= 0;

extern mthRandom g_PlantsRendRand;
extern pngPlantMgr gPlantMgr;

#if __DEV
extern u32 *g_LocTriPlantsLOD0DrawnPerSlot;
extern u32 *g_LocTriPlantsLOD1DrawnPerSlot;
extern u32 *g_LocTriPlantsLOD2DrawnPerSlot;
extern u32 nLocTriPlantsSlotID;
extern u32 nLocTriPlantsLOD0Drawn[PNG_PLANT_NUM_PLANT_SLOTS];
extern u32 nLocTriPlantsLOD1Drawn[PNG_PLANT_NUM_PLANT_SLOTS];
extern u32 nLocTriPlantsLOD2Drawn[PNG_PLANT_NUM_PLANT_SLOTS];
extern u32 nPlantsSpuBigBufferConsumed;
extern u32 nPlantsSpuBigBufferOverfilled;
#endif // __DEV

#if __BANK
extern u8 gbPlantsFlashLOD0;
extern u8 gbPlantsFlashLOD1;
extern u8 gbPlantsFlashLOD2;
extern float gbPlantsLOD0AlphaCloseDist;
extern float gbPlantsLOD0AlphaFarDist;
extern float gbPlantsLOD0FarDist;
extern float gbPlantsLOD1CloseDist;
extern float gbPlantsLOD1FarDist;
extern float gbPlantsLOD1Alpha0CloseDist;
extern float gbPlantsLOD1Alpha0FarDist;
extern float gbPlantsLOD1Alpha1CloseDist;
extern float gbPlantsLOD1Alpha1FarDist;
extern float gbPlantsLOD2CloseDist;
extern float gbPlantsLOD2FarDist;
extern float gbPlantsLOD2Alpha0CloseDist;
extern float gbPlantsLOD2Alpha0FarDist;
extern float gbPlantsLOD2Alpha1CloseDist;
extern float gbPlantsLOD2Alpha1FarDist;
extern u8 gbPlantsGeometryCulling;
#endif // __BANK

// if 1, then commandbuffers are in VRAM:
#define GCMFIFO_IN_VRAM				(0)
#define HEAP_SIZE					((48 + 4) * 1024) // 4 buffers+gap: (8+4) * 4 + 4= 52KB
#define STARTUP_HEAP_SIZE			(256)			// 256 bytes
// use 2D collsion calculation for LocTris:
#define USE_COLLISION_2D_DIST		(1)
// 360: Leaving it disabled for now, because this doesn't help much on Xenos for some reason
//      and total GPU execution time/CB size is longer/bigger when enabled (?!?);
//      probably D3D does similiar caching tricks under the hood when calling SetVertexShaderConstant();
#define OPTIMIZED_VS_GRASS_SETUP	(1 && __PPU)

// few helper allocators to access & map memory,
// depending where CBs are located:
#if GCMFIFO_IN_VRAM
	#define PLANTS_MALLOC_ALIGN(size, alignment)	physical_new(size, alignment)
	#define PLANTS_FREE_ALIGN(ptr)					::gcm::FreePtr(ptr)
	#define PLANTS_GCM_OFFSET(ptr)					::gcm::LocalOffset(ptr)
#else
	#define PLANTS_MALLOC_ALIGN(size, alignment)	rage_aligned_new (alignment) u8[size]
	#define PLANTS_FREE_ALIGN(ptr)					::gcm::FreePtr(ptr)
	#define PLANTS_GCM_OFFSET(ptr)					::gcm::MainOffset(ptr)
#endif // GCMFIFO_IN_VRAM

#if PNG_PLANTSMGR_USE_SPU_RENDERING
static int						s_NumGrassJobsAdded		= 0;
static sysTaskHandle			s_GrassTaskHandle		= NULL;
static u32						s_BigHeapId				= 0;
static u32*						s_BigHeap[2]			= { NULL };
static u32						s_BigHeapOffset[2]		= { 0 };
static u32*						s_Heap					= NULL;
static u32						s_HeapOffset			= 0;
static u32*						s_StartupHeapPtr[2]		= { NULL };
static u32						s_StartupHeapOffset[2]	= { 0 };
static grcVertexDeclaration*	s_VertexDecl			= NULL;
static u32						s_RsxLabelIndices[5]	= { -1 }; // indices of RSX label1
static volatile u32*			s_RsxLabelPtrs[5]		= { NULL }; // RSX: lock/unlock() buf1

// private aligned PPTriPlant space for SPU job to dma from:
static PPTriPlant				s_SpuTriPlantTab[PNG_PLANTSMGR_MAX_NUM_OF_RENDER_JOBS][PPTRIPLANT_BUFFER_SIZE]	ALIGNED(128); // 128*32*16 = 64KB
static spuGrassParamsStruct		s_InSpuGrassStructTab[PNG_PLANTSMGR_MAX_NUM_OF_RENDER_JOBS]						ALIGNED(128);
static spuGrassParamsStruct		s_OutSpuGrassStructTab[PNG_PLANTSMGR_MAX_NUM_OF_RENDER_JOBS]					ALIGNED(128);

// every next task waits for bigger value, so many SPU tasks can be synchronized during the same frame
static u32						s_RsxLabel5_CurrentTaskID = 0;
#endif // PNG_PLANTSMGR_USE_SPU_RENDERING


struct pngGrassInstance
{
	Matrix44				m_WorldMatrix;
	Vector4					m_PlantColor;
	Vector4					m_GroundColor;

#if OPTIMIZED_VS_GRASS_SETUP
	Matrix34				m_prevWorldMatrix;
#endif // OPTIMIZED_VS_GRASS_SETUP

public:
	inline void Reset()
	{
#if OPTIMIZED_VS_GRASS_SETUP
		m_prevWorldMatrix.a =
		m_prevWorldMatrix.b =
		m_prevWorldMatrix.c =
		m_prevWorldMatrix.d = Vector3(-1,-1,-1);
#endif // OPTIMIZED_VS_GRASS_SETUP
	}

#if OPTIMIZED_VS_GRASS_SETUP
	inline bool UseOptimizedVsSetup(const Matrix34& m34)
	{
		const bool bDifferent =	m_prevWorldMatrix.a.IsNotEqual(m34.a) ||
								m_prevWorldMatrix.b.IsNotEqual(m34.b) ||
								m_prevWorldMatrix.c.IsNotEqual(m34.c);
		if(bDifferent)
		{
			m_prevWorldMatrix.a = m34.a;
			m_prevWorldMatrix.b = m34.b;
			m_prevWorldMatrix.c = m34.c;
			return(false);
		}
		return true;
	}
#else //OPTIMIZED_VS_GRASS_SETUP
	inline bool UseOptimizedVsSetup(const Matrix34& ) {return(false);}
#endif //OPTIMIZED_VS_GRASS_SETUP

	// full setup: 6 registers
	inline int GetRegisterCountFull() const
	{
		return sizeof(m_WorldMatrix) / 16 + sizeof(m_PlantColor) / 16 + sizeof(m_GroundColor) / 16;
	}
	inline const float* GetRegisterPointerFull() const
	{
		return (const float*)&m_WorldMatrix;
	}

	// optimized setup: 3 registers (only position from matrix is taken):
	inline int  GetRegisterCountOpt() const
	{
		return sizeof(m_WorldMatrix.d) / 16 + sizeof(m_PlantColor) / 16 + sizeof(m_GroundColor) / 16;
	}
	inline const float* GetRegisterPointerOpt() const
	{
		return (const float*)&m_WorldMatrix.d;
	}
};

pngGrassRenderer::pngGrassRenderer()
{
}

pngGrassRenderer::~pngGrassRenderer()
{
}

bool pngGrassRenderer::Initialise()
{
	m_ShaderGroup = grmShaderFactory::GetInstance().CreateGroup();
	Assert(m_ShaderGroup);

	m_Shader = grmShaderFactory::GetInstance().Create();
	Assert(m_Shader);
	Verifyf(m_Shader->Load("rage_gtagrass"), "Error loading 'grass' shader!");
	m_ShaderGroup->InitCount(1);
	m_ShaderGroup->Add(m_Shader);

	m_shdDeferredLOD0TechniqueID = m_Shader->LookupTechnique("deferredLOD0_draw", true);
	Assert(m_shdDeferredLOD0TechniqueID);

	m_shdDeferredLOD1TechniqueID = m_Shader->LookupTechnique("deferredLOD1_draw", true);
	Assert(m_shdDeferredLOD1TechniqueID);

	m_shdDeferredLOD2TechniqueID = m_Shader->LookupTechnique("deferredLOD2_draw", true);
	Assert(m_shdDeferredLOD2TechniqueID);

	m_shdTextureID = m_Shader->LookupVar("grassTexture0", true);
	Assert(m_shdTextureID);

	m_shdCameraPosID = grcEffect::LookupGlobalVar("vecCameraPos0", true);
	Assert(m_shdCameraPosID);

	m_shdPlayerPosID = grcEffect::LookupGlobalVar("vecPlayerPos0", true);
	Assert(m_shdPlayerPosID);

	m_shdCollParamsID = m_Shader->LookupVar("vecCollParams0", true);
	Assert(m_shdCollParamsID);

	m_shdVehCollisionEnabledID = grcEffect::LookupGlobalVar("bVehCollEnabled0", true);
	Assert(m_shdVehCollisionEnabledID);

	m_shdVehCollisionBID = grcEffect::LookupGlobalVar("vecVehCollB0", true);
	Assert(m_shdVehCollisionBID);

	m_shdVehCollisionMID = grcEffect::LookupGlobalVar("vecVehCollM0", true);
	Assert(m_shdVehCollisionMID);

	m_shdVehCollisionRID = grcEffect::LookupGlobalVar("vecVehCollR0", true);
	Assert(m_shdVehCollisionRID);

	m_shdFadeAlphaDistUmTimerID = m_Shader->LookupVar("fadeAlphaDistUmTimer0", true);
	Assert(m_shdFadeAlphaDistUmTimerID);

	m_shdFadeAlphaLOD1DistID = m_Shader->LookupVar("fadeAlphaLOD1Dist0", true);
	Assert(m_shdFadeAlphaLOD1DistID);

	m_shdFadeAlphaLOD2DistID = m_Shader->LookupVar("fadeAlphaLOD2Dist0", true);
	Assert(m_shdFadeAlphaLOD2DistID);

	m_shdUMovementParamsID = m_Shader->LookupVar("uMovementParams0", true);
	Assert(m_shdUMovementParamsID);

	m_shdPlantColorID = m_Shader->LookupVar("plantColor0");
	Assert(m_shdPlantColorID);

	m_shdDimensionLOD2ID = m_Shader->LookupVar("dimensionLOD20", true);
	Assert(m_shdDimensionLOD2ID);

#if PNG_PLANTSMGR_USE_SPU_RENDERING
	// make sure both enums match each other:
#if __ASSERT
	// stupid gcc doesn't allow to compare enums:
	int plane0;
	plane0 = spuCLIP_PLANE_NEAR;	Assert(grcViewport::CLIP_PLANE_NEAR		==plane0);
	plane0 = spuCLIP_PLANE_LEFT;	Assert(grcViewport::CLIP_PLANE_LEFT		==plane0);
	plane0 = spuCLIP_PLANE_RIGHT;	Assert(grcViewport::CLIP_PLANE_RIGHT	==plane0);
	plane0 = spuCLIP_PLANE_FAR;		Assert(grcViewport::CLIP_PLANE_FAR		==plane0);
	plane0 = spuCLIP_PLANE_TOP;		Assert(grcViewport::CLIP_PLANE_TOP		==plane0);
	plane0 = spuCLIP_PLANE_BOTTOM;	Assert(grcViewport::CLIP_PLANE_BOTTOM	==plane0);
#endif // __ASSERT

	s_PPTriPlantBuffer.AllocateTextureBuffers();

	s_BigHeap[0]			= (u32*)PLANTS_MALLOC_ALIGN(PNG_PLANTSMGR_BIG_HEAP_SIZE, 128);
	s_BigHeap[1]			= (u32*)PLANTS_MALLOC_ALIGN(PNG_PLANTSMGR_BIG_HEAP_SIZE, 128);
	s_BigHeapOffset[0]	= PLANTS_GCM_OFFSET(s_BigHeap[0]);
	s_BigHeapOffset[1]	= PLANTS_GCM_OFFSET(s_BigHeap[1]);
	Assert(s_BigHeap[0]);
	Assert(s_BigHeap[1]);
	Printf("\n PlantsMgr: BigHeap0=%X, BigHeapOffset0=%X", (u32)s_BigHeap[0], (u32)s_BigHeapOffset[0]);
	Printf("\n PlantsMgr: BigHeap1=%X, BigHeapOffset1=%X", (u32)s_BigHeap[1], (u32)s_BigHeapOffset[1]);

	s_Heap = (u32*)PLANTS_MALLOC_ALIGN(HEAP_SIZE, 128);
	s_HeapOffset = PLANTS_GCM_OFFSET(s_Heap);	// all main memory is mapped to RSX already...
	Assert(s_Heap);
	
	s_StartupHeapPtr[0]		= (u32*)PLANTS_MALLOC_ALIGN(STARTUP_HEAP_SIZE, 128);
	s_StartupHeapPtr[1]		= (u32*)PLANTS_MALLOC_ALIGN(STARTUP_HEAP_SIZE, 128);
	s_StartupHeapOffset[0]	= PLANTS_GCM_OFFSET(s_StartupHeapPtr[0]);
	s_StartupHeapOffset[1]	= PLANTS_GCM_OFFSET(s_StartupHeapPtr[1]);
	Assert(s_StartupHeapPtr[0]);
	Assert(s_StartupHeapPtr[1]);

#if __ASSERT
	if(sizeof(PPTriPlant)!=PSN_SIZEOF_PPTRIPLANT)
	{
		Printf("sizeof(PPTriPlant)!=%d: actual sizeof=%d.", PSN_SIZEOF_PPTRIPLANT, sizeof(PPTriPlant));
	}
	if(sizeof(spuGrassParamsStruct)!=PSN_SIZEOF_SPUGRASSPARAMSTRUCT)
	{
		Printf("sizeof(spuGrassParamsStruct)!=%d: actual sizeof=%d.", PSN_SIZEOF_SPUGRASSPARAMSTRUCT, sizeof(spuGrassParamsStruct));
	}
	Assert(sizeof(PPTriPlant)==PSN_SIZEOF_PPTRIPLANT);
	Assert(sizeof(spuGrassParamsStruct)==PSN_SIZEOF_SPUGRASSPARAMSTRUCT);
#endif // __ASSERT

	// allocate RSX labels:
	for(int i=0; i<5; i++)
	{
		s_RsxLabelIndices[i]		= gcm::RsxSemaphoreRegistrar::Allocate();			// index of RSX label1
		s_RsxLabelPtrs[i]			= cellGcmGetLabelAddress(s_RsxLabelIndices[i]);		// ptr of the label in RSX memory
		Assert(s_RsxLabelPtrs[i]);
	}

	*(s_RsxLabelPtrs[PNG_PLANTSMGR_RSX_LABELI5]) = 0xFF000000|(s_BigHeapId<<8)|((s_RsxLabel5_CurrentTaskID-2)&0xFF);

	if(!s_VertexDecl)
	{	// custom vertexbuffer stuff:
		grcFvf fvf;
		fvf.SetPosChannel(true, grcFvf::grcdsFloat4);			// POSITION
		fvf.SetNormalChannel(true, grcFvf::grcdsFloat4);		// NORMAL
		fvf.SetDiffuseChannel(true, grcFvf::grcdsColor);		// COLOR0

		s_VertexDecl = grmModelFactory::BuildDeclarator(&fvf, NULL, NULL, NULL);
		Assert(s_VertexDecl);
	}
#endif // PNG_PLANTSMGR_USE_SPU_RENDERING

	return true;
}

grmShaderGroup* pngGrassRenderer::GetGrassShaderGroup()
{
	return m_ShaderGroup;
}

grmShader* pngGrassRenderer::GetGrassShader()
{
	return m_Shader;
}

void pngGrassRenderer::Shutdown()
{
#define DELETE_PTR(PTR) { if(PTR) { delete(PTR); PTR=NULL; } }
	m_Shader->SetVar(m_shdTextureID, (grcTexture*)NULL);
	DELETE_PTR(m_ShaderGroup);
#undef DELETE_PTR

#if PNG_PLANTSMGR_USE_SPU_RENDERING
	s_PPTriPlantBuffer.DestroyTextureBuffers();

	PLANTS_FREE_ALIGN(s_BigHeap[0]);
	PLANTS_FREE_ALIGN(s_BigHeap[1]);
	s_BigHeap[0] = NULL;
	s_BigHeap[1] = NULL;
	s_BigHeapOffset[0] = 0;
	s_BigHeapOffset[1] = 0;

	PLANTS_FREE_ALIGN(s_Heap);
	s_Heap = NULL;
	s_HeapOffset = 0;

	PLANTS_FREE_ALIGN(s_StartupHeapPtr[0]);
	PLANTS_FREE_ALIGN(s_StartupHeapPtr[1]);
	s_StartupHeapPtr[0] = NULL;
	s_StartupHeapPtr[1] = NULL;
	s_StartupHeapOffset[0] = 0;	
	s_StartupHeapOffset[1] = 0;	
#endif // PNG_PLANTSMGR_USE_SPU_RENDERING
}

void pngGrassRenderer::SetCurrentScanCode(u16 /*currScanCode*/)
{
	// m_currScanCode = currScanCode;
}

void pngGrassRenderer::AddTriPlant(PPTriPlant *pSrcPlant, u32 ePlantModelSet)
{
	s_PPTriPlantBuffer.ChangeCurrentPlantModelsSet(ePlantModelSet);

	PPTriPlant *pDstPlant = s_PPTriPlantBuffer.GetPPTriPlantPtr(1);

	sysMemCpy(pDstPlant, pSrcPlant, sizeof(PPTriPlant));;
	
	s_PPTriPlantBuffer.IncreaseBufferIndex(ePlantModelSet, 1);

}

void pngGrassRenderer::FlushTriPlantBuffer()
{
	s_PPTriPlantBuffer.Flush();
}

bool pngGrassRenderer::SetPlantModelsTab(u32 index, pngGrassModel* plantModels)
{
	return(s_PPTriPlantBuffer.SetPlantModelsTab(index, plantModels));
}

pngGrassModel* pngGrassRenderer::GetPlantModelsTab(u32 index)
{
	return(s_PPTriPlantBuffer.GetPlantModelsTab(index));
}

void pngGrassRenderer::SetGeometryLOD2(grcVertexBuffer *vb, grcVertexDeclaration *decl)
{
	if(sm_plantLOD2VertexBuffer)
	{
		// destroy this
		delete sm_plantLOD2VertexBuffer;
		sm_plantLOD2VertexBuffer = NULL;
	}

	if(sm_plantLOD2VertexDecl)
	{
		// destroy this
		sm_plantLOD2VertexDecl->Release();
		sm_plantLOD2VertexDecl = NULL;
	}

	sm_plantLOD2VertexBuffer	= vb;
	sm_plantLOD2VertexDecl		= decl;
}


//
//
//
// updates global camera pos; should be called every frame;
//
void pngGrassRenderer::SetGlobalCameraPos(const Vector3& camPos)
{
//	Assert(CSystem::IsThisThreadId(SYS_THREAD_UPDATE));
	const int idx = gPlantMgr.GetUpdateRTBufferID();
	sm_vecCameraPos[idx] = camPos;
}

const Vector3& pngGrassRenderer::GetGlobalCameraPos()
{
//	Assert(CSystem::IsThisThreadId(SYS_THREAD_RENDER));
	const int idx = gPlantMgr.GetRenderRTBufferID();
	return sm_vecCameraPos[idx];
}


void pngGrassRenderer::SetGlobalPlayerPos(const Vector3& playerPos)
{
//	Assert(CSystem::IsThisThreadId(SYS_THREAD_UPDATE));
	const int idx = gPlantMgr.GetUpdateRTBufferID();
	sm_vecPlayerPos[idx] = playerPos;
}

const Vector3& pngGrassRenderer::GetGlobalPlayerPos()
{
//	Assert(CSystem::IsThisThreadId(SYS_THREAD_RENDER));
	const int idx = gPlantMgr.GetRenderRTBufferID();
	return sm_vecPlayerPos[idx];
}

void pngGrassRenderer::UpdateGlobalCameraPos()
{
	grcEffect::SetGlobalVar(m_shdCameraPosID, RCC_VEC3V(GetGlobalCameraPos()));
}

void pngGrassRenderer::UpdateGlobalPlayerPos()
{
	grcEffect::SetGlobalVar(m_shdPlayerPosID, RCC_VEC3V(GetGlobalPlayerPos()));
}

void pngGrassRenderer::SetGlobalVehCollisionParams(bool bEnable, const Vector3& vecB, const Vector3& vecM, float radius, float groundZ)
{
//	Assert(CSystem::IsThisThreadId(SYS_THREAD_UPDATE));
	const int idx = gPlantMgr.GetUpdateRTBufferID();
//	float4 vecVehCollB : vecVehCollB0 = float4(0,0,0,0);	// [ segment start point B.xyz				| 0				]
//	float4 vecVehCollM : vecVehCollM0 = float4(0,0,0,0);	// [ segment vector M.xyz					| 1.0f/dot(M,M) ]
//	float4 vecVehCollR : vecVehCollR0 = float4(0,0,0,0);	// [ R	      | R*R          | 1.0f/(R*R)   | worldPosZ		]

	sm_bVehCollisionEnabled[idx] = bEnable;

	Vector4 &vehCollisionB = sm_vecVehCollisionB[idx]; 
	vehCollisionB.x = vecB.x;
	vehCollisionB.y = vecB.y;
	vehCollisionB.z = vecB.z;
	vehCollisionB.w = 0.0f;

	Vector2 vecM2;
	vecM2.x = vecM.x;
	vecM2.y = vecM.y;
	float dotMM = vecM2.Dot(vecM2);

	Vector4 &vehCollisionM = sm_vecVehCollisionM[idx];
	vehCollisionM.x = vecM.x;
	vehCollisionM.y = vecM.y;
	vehCollisionM.z = vecM.z;
	vehCollisionM.w = (dotMM>0.0f)? (1.0f/dotMM) : (0.0f);

	Vector4 &vehCollisionR = sm_vecVehCollisionR[idx];
	vehCollisionR.x = radius;
	vehCollisionR.y = radius * radius;
	vehCollisionR.z = (radius>0.0f)? (1.0f/(radius * radius)) : (0.0f);
	vehCollisionR.w = groundZ;
}

void pngGrassRenderer::GetGlobalVehCollisionParams(bool *pbEnable, Vector4 *pVecB, Vector4 *pVecM, Vector4 *pVecR)
{
//	Assert(CSystem::IsThisThreadId(SYS_THREAD_RENDER));

	Assert(pbEnable);
	Assert(pVecB);
	Assert(pVecM);
	Assert(pVecR);

	const int idx = gPlantMgr.GetRenderRTBufferID();

	*pbEnable	= sm_bVehCollisionEnabled[idx];
	*pVecB		= sm_vecVehCollisionB[idx];
	*pVecM		= sm_vecVehCollisionM[idx];
	*pVecR		= sm_vecVehCollisionR[idx];
}

void pngGrassRenderer::SetGlobalWindBending(float bending)
{
//	Assert(CSystem::IsThisThreadId(SYS_THREAD_UPDATE));
	const int idx = gPlantMgr.GetUpdateRTBufferID();
	sm_windBending[idx] = bending;
}

float pngGrassRenderer::GetGlobalWindBending()
{
//	Assert(CSystem::IsThisThreadId(SYS_THREAD_RENDER));
	const int idx = gPlantMgr.GetRenderRTBufferID();
	return sm_windBending[idx];
}

inline const grcViewport* GetCurrentViewport()
{
	const grcViewport *pViewport = grcViewport::GetCurrent();
	//	const grcViewport *pViewport = &CRenderPhase::GetCurrent()->GetGrcViewport();
	return(pViewport);
}

#if PNG_PLANTSMGR_USE_SPU_RENDERING
static void SetupContextData(CellGcmContextData *context, const u32 *addr, const u32 size, CellGcmContextCallback callback)
{
	context->begin		= (u32*)addr;
	context->current	= (u32*)addr;
	context->end		= (u32*)addr + size/sizeof(u32) - 1;
	context->callback	= callback;
}

static int CustomGcmReserveFailed(CellGcmContextData* ASSERT_ONLY(context), u32 ASSERT_ONLY(count))
{
#if __ASSERT
	Errorf("\nCustom context 0x%p overfilled! CurrentSize=%d (maxSize=%d). Count=%d.",
				context, context->current - context->begin, context->end - context->begin, count);
	Assert(false);
#endif
	return(CELL_OK);
}

// SetupGrassTextureForSPU():
// - generates small command buffer to set particular texture on SPU
// - most of this textrue stuff is stolen from grcFragmentProgram::SetParameter();
bool pngGrassRenderer::SpuRecordGrassTexture(u32 slotID, u32 texIndex, grcTexture *data)
{
	const int _cmdBufSize = 128;
	u32 _cmdBuf[_cmdBufSize]={0};	// fill with NOPs
	CellGcmContext		con1;
#if __ASSERT
	CellGcmContextData	*conData1 = (CellGcmContextData*)&con1;
#endif
	SetupContextData(&con1, _cmdBuf, _cmdBufSize*sizeof(u32), &CustomGcmReserveFailed);

	Assert(data);

	const u32 texUnit = 0;	// check shader source to know which texunit it uses
	const CellGcmTexture *gcmTex = reinterpret_cast<const CellGcmTexture*>(data->GetTexturePtr());

	con1.SetTexture(texUnit,gcmTex);

	const u8 anisotropy = CELL_GCM_TEXTURE_MAX_ANISO_1;
	union { int i; float f; } x;
	x.i = 0;	//states[grcessMIPMAPLODBIAS];
	int fixedBias = int(x.f * 256.0f) & 0x1fff;	// gcm doesn't mask this unless CELL_GCM_BITFIELD is set!

	// controls anisotropic filtering for texture sampler 1
	u32 minMipLevelIndex = static_cast<u32>(gcmTex->mipmap - 1);
	u32 maxMipLevelIndex = Min<u32>(minMipLevelIndex, 0/*MAXMIPLEVEL*/) << 8;
	minMipLevelIndex = Min<u32>(minMipLevelIndex, 12/*grcessMINMIPLEVEL]*/) << 8;

	con1.SetTextureControl(texUnit, CELL_GCM_TRUE, // disable / enable texture sampler
		maxMipLevelIndex, minMipLevelIndex, anisotropy); 

	con1.SetTextureFilter(texUnit, fixedBias,
			CELL_GCM_TEXTURE_LINEAR_LINEAR,	//remapMin[states[grcessMIPFILTER]][states[grcessMINFILTER]],
			CELL_GCM_TEXTURE_LINEAR,	//remapMag[states[grcessMAGFILTER]],
			CELL_GCM_TEXTURE_CONVOLUTION_QUINCUNX);	//remapConv[Max(states[grcessMINFILTER],states[grcessMAGFILTER])]));

	// texture address mode is wrap ...
	con1.SetTextureAddress(texUnit,
				/*grcessADDRESSU*/CELL_GCM_TEXTURE_WRAP,
				/*grcessADDRESSV*/CELL_GCM_TEXTURE_WRAP,
				/*grcessADDRESSW*/CELL_GCM_TEXTURE_WRAP,
				CELL_GCM_TEXTURE_UNSIGNED_REMAP_NORMAL,
				// specify the depth texture test method used when applying the shadow map
				// CELL_GCM_TEXTURE_ZFUNC_GREATER worked well in the past with a depth test of _LESS
				// switches on and off hardware comparison and hardware PCF filtering
				CELL_GCM_TEXTURE_ZFUNC_ALWAYS, 
				gcmTex->_padding); // sRGB reverse conversion

	// Just to be safe - I'm not sure what version this function was actually added to the Cell SDK
	// "brilinear" optimization
	con1.SetTextureOptimization(texUnit, 8/*states[grcessTRILINEARTHRESHOLD]*/, CELL_GCM_TEXTURE_ISO_HIGH, CELL_GCM_TEXTURE_ANISO_HIGH);
	// texture border color
	con1.SetTextureBorderColor(texUnit, 0);

	con1.SetNopCommand(1);	// fillup to 20 commands...

#if __ASSERT
	const u32 cbSize = ((u8*)conData1->current) - ((u8*)conData1->begin);
	Assert((cbSize/4) == PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE);
#endif

	s_PPTriPlantBuffer.SetPlantTexturesCmd(slotID, texIndex, _cmdBuf);

	return true;
}

namespace rage
{
	extern __THREAD CellGcmContextData *g_CurrentGcmContext;
}

static u32 gpShaderLOD0Cmd0[PNG_PLANTSMGR_LOCAL_BINDSHADERLOD0_MAXCMDSIZE] ALIGNED(128);
static u32 gpShaderLOD1Cmd0[PNG_PLANTSMGR_LOCAL_BINDSHADERLOD1_MAXCMDSIZE] ALIGNED(128);
static u32 gpShaderLOD2Cmd0[PNG_PLANTSMGR_LOCAL_BINDSHADERLOD2_MAXCMDSIZE] ALIGNED(128);
static u32 gpPlantLOD2GeometryCmd0[PNG_PLANTSMGR_LOCAL_GEOMETRY_MAXCMDSIZE] ALIGNED(128);

//
// useful routine to print any given memory as RSX dump buffer:
//		[000] 00000330:00000207  00000334:00000000  00000338:000000ff  00000330:00000207
//		[004] 00000334:00000000  00000338:000000ff  00001d6c:00000440  00001d74:00000000
//		[008] 00001d7c:00000001  00001efc:00000021  00001f00:3d808081  00001f04:41287aa8
//		[00c] 00001f08:3f7eff00  00001f0c:3eea4f76  00001d6c:00000440  00001d74:35082214
//
static void DumpMemoryAsRSXDump(const char *dumpName, u32 *startPtr, u32 sizeInWords)
{
u32 *endPtr = startPtr + sizeInWords;
#define GETP(PTR) (PTR<endPtr?(*(PTR)):(0x0000BACA))

	Printf("\n xxxxx %s:Begin", dumpName);

const u32 countW = (sizeInWords+8)/8;
u32 *p=startPtr;

	u32 i=0;
	for(; i<(countW-1); i++)
	{
		Printf("\n [%03x] %08x:%08x  %08x:%08x  %08x:%08x  %08x:%08x", i*4,
				p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]); 
		p += 8;
	}

	// last line:
	Printf("\n [%03x] %08x:%08x  %08x:%08x  %08x:%08x  %08x:%08x", i*4,
				GETP(&p[0]), GETP(&p[1]), GETP(&p[2]), GETP(&p[3]), GETP(&p[4]), GETP(&p[5]), GETP(&p[6]), GETP(&p[7])); 


	Printf("\n xxxxx %s:End", dumpName);
}

bool pngGrassRenderer::SpuRecordShaderBind(void *_con1, grcEffectTechnique forcedTech, u32 *dstCmd, u32 maxCmdSize, u32 /*numExtraNOPs*/)
{
CellGcmContext		&con1		= *((CellGcmContext*)_con1);
CellGcmContextData	*conData1	= (CellGcmContextData*)&con1;

	conData1->current = conData1->begin;	// restart context

	// Bind technique:
	ASSERT_ONLY(int numPasses =) m_Shader->BeginDraw(grmShader::RMC_DRAW, /*restoreState*/false, /*techOverride*/forcedTech);
	Assert(numPasses>0);
	const int pass=0;
	m_Shader->Bind(pass);

	const u32 cbSize4 = (((u8*)conData1->current) - ((u8*)conData1->begin))/4;
	if(cbSize4 < maxCmdSize)
	{	// fill CB with extra nops:
		int numNops = maxCmdSize - cbSize4;
		con1.SetNopCommand(numNops);
	}

#if __ASSERT
	{
		const u32 cbSize = ((u8*)conData1->current) - ((u8*)conData1->begin);
		if((cbSize/4) != maxCmdSize)
			Printf("\n SpuRecordShader: Bind: cbSize=%d, cbSize4=%d, maxCmdSize=%d", cbSize, cbSize/4, maxCmdSize);
		Assert((cbSize/4) == maxCmdSize);
	}
#endif

	sysMemCpy(dstCmd, conData1->begin, maxCmdSize*sizeof(u32));

	return true;
}

bool pngGrassRenderer::SpuRecordShaderUnBind(void *_con1)
{
CellGcmContext		&con1		= *((CellGcmContext*)_con1);
CellGcmContextData	*conData1 = (CellGcmContextData*)&con1;
	conData1->current = conData1->begin;

	// Unbind:
	GRCDEVICE.ClearStreamSource(0);
	m_Shader->UnBind();
	m_Shader->EndDraw();

#if __ASSERT
	{
		const u32 cbSize = ((u8*)conData1->current) - ((u8*)conData1->begin);
		if((cbSize/4) != 0)
		{
			Printf("\n SpuRecordShader: UnBind: cbSize=%d, cbSize4=%d, maxCmdSize=%d", cbSize, cbSize/4, 0);
		}
	}
#endif
	return true;
}

bool pngGrassRenderer::SpuRecordGeometry(void *con, grmGeometry *pGeometry, u32 *dstCmd, u32 maxCmdSize, u32 numExtraNOPs)
{
	grcVertexBuffer* pVB		= pGeometry->GetVertexBuffer(true);
	grcVertexDeclaration* pVDecl= pGeometry->GetDecl();

	return SpuRecordGeometry(con, pVB, pVDecl, dstCmd, maxCmdSize, numExtraNOPs);
}

bool pngGrassRenderer::SpuRecordGeometry(void *_con1, grcVertexBuffer* pVertexBuffer, grcVertexDeclaration* pVertexDeclaration,
														u32 *dstCmd, u32 maxCmdSize, u32 numExtraNOPs)
{
CellGcmContext		&con1		= *((CellGcmContext*)_con1);
CellGcmContextData	*conData1 = (CellGcmContextData*)&con1;
conData1->current = conData1->begin;
	
	Assert(pVertexBuffer);
	Assert(pVertexDeclaration);

	GRCDEVICE.SetVertexDeclaration(pVertexDeclaration);
	GRCDEVICE.SetStreamSource(0, *pVertexBuffer, 0, pVertexBuffer->GetVertexStride());

	con1.SetNopCommand(numExtraNOPs);	// fillup buffer to 80 words

#if __ASSERT
	const u32 cbSize = ((u8*)conData1->current) - ((u8*)conData1->begin);
	if((cbSize/4) != maxCmdSize)
		Printf("\n SpuRecordShaders: SetStreamSource: geom: cbSize=%d, cbSize4=%d, maxCmdSize=%d", cbSize, cbSize/4, maxCmdSize);
	Assert((cbSize/4) == maxCmdSize);
#endif

	sysMemCpy(dstCmd, conData1->begin, maxCmdSize*sizeof(u32));

	return true;
}

bool pngGrassRenderer::SpuRecordGeometries()
{
	const int _cmdBufSize = 256;
	u32 _cmdBuf[_cmdBufSize]={0};	// fill with NOPs
	CellGcmContext		con1;
	SetupContextData(&con1, _cmdBuf, _cmdBufSize*sizeof(u32), &CustomGcmReserveFailed);

	// save orig Rage context:
	CellGcmContextData *oldRageContext = g_CurrentGcmContext;
	Printf("\n SpuRecordGeometries: oldRageContext=0x%p", oldRageContext);
	g_CurrentGcmContext = (CellGcmContextData*)&con1;

	// Bind LOD0 technique:
	SpuRecordShaderBind(&con1, m_shdDeferredLOD0TechniqueID, &gpShaderLOD0Cmd0[0], PNG_PLANTSMGR_LOCAL_BINDSHADERLOD0_MAXCMDSIZE, 3);
	DumpMemoryAsRSXDump("BindShaderLOD0", gpShaderLOD0Cmd0,  PNG_PLANTSMGR_LOCAL_BINDSHADERLOD0_MAXCMDSIZE);
	{
		// 2. VertexDeclaration & SetStreamSource:
		// relies on g_VertexShaderInputs set in grcVertexProgram::Bind()
		for(int slotID=0; slotID<PNG_PLANT_NUM_PLANT_SLOTS; slotID++)
		{
			pngGrassModel *pPlantModelsTab = s_PPTriPlantBuffer.GetPlantModels(slotID);
			for(int i=0; i<PNG_PLANT_SLOT_NUM_MODELS; i++)
			{
				// LOD0 geometries only:
				pngGrassModel *pPlantModel = &pPlantModelsTab[i];

				grmGeometry *pGeometry = pPlantModel->m_pGeometry;
				Assert(pGeometry);
				grcIndexBufferGCM* pIndexBuffer	= (grcIndexBufferGCM*)pGeometry->GetIndexBuffer(true);
				Assert(pIndexBuffer);

				u16* ptr = pIndexBuffer->GetGCMBuffer();
				pPlantModel->m_IdxOffset	= pIndexBuffer->GetGCMOffset();
				// IsLocalPtr() may not always work at startup - depends on rage::g_LocalAddress:
				pPlantModel->m_IdxLocation	= gcm::IsLocalPtr(ptr)?CELL_GCM_LOCATION_LOCAL:CELL_GCM_LOCATION_MAIN;
				pPlantModel->m_IdxCount		= pIndexBuffer->GetIndexCount();
				pPlantModel->m_DrawMode		= CELL_GCM_PRIMITIVE_TRIANGLES;

				SpuRecordGeometry(&con1, pGeometry, &pPlantModel->m_GeometryCmd0[0], PNG_PLANTSMGR_LOCAL_GEOMETRY_MAXCMDSIZE, 8);
			}
		}
	}
	// Unbind LOD0:
	SpuRecordShaderUnBind(&con1);

	// Bind LOD1 technique:
	SpuRecordShaderBind(&con1, m_shdDeferredLOD1TechniqueID, &gpShaderLOD1Cmd0[0], PNG_PLANTSMGR_LOCAL_BINDSHADERLOD1_MAXCMDSIZE, 1);
	DumpMemoryAsRSXDump("BindShaderLOD1", gpShaderLOD1Cmd0,  PNG_PLANTSMGR_LOCAL_BINDSHADERLOD1_MAXCMDSIZE);
	{
		// 2. VertexDeclaration & SetStreamSource:
		// relies on g_VertexShaderInputs set in grcVertexProgram::Bind()
		for(int slotID=0; slotID<PNG_PLANT_NUM_PLANT_SLOTS; slotID++)
		{
			pngGrassModel *pPlantModelsTab = s_PPTriPlantBuffer.GetPlantModels(slotID);
			for(int i=0; i<PNG_PLANT_SLOT_NUM_MODELS; i++)
			{
				// LOD1 geometries only:
				pngGrassModel *pPlantModel = &pPlantModelsTab[i+PNG_PLANT_SLOT_NUM_MODELS];

				grmGeometry *pGeometry = pPlantModel->m_pGeometry;
				Assert(pGeometry);
				grcIndexBufferGCM* pIndexBuffer	= (grcIndexBufferGCM*)pGeometry->GetIndexBuffer(true);
				Assert(pIndexBuffer);

				u16* ptr = pIndexBuffer->GetGCMBuffer();
				pPlantModel->m_IdxOffset	= pIndexBuffer->GetGCMOffset();
				// IsLocalPtr() may not always work at startup - depends on rage::g_LocalAddress:
				pPlantModel->m_IdxLocation	= gcm::IsLocalPtr(ptr)?CELL_GCM_LOCATION_LOCAL:CELL_GCM_LOCATION_MAIN;
				pPlantModel->m_IdxCount		= pIndexBuffer->GetIndexCount();
				pPlantModel->m_DrawMode		= CELL_GCM_PRIMITIVE_TRIANGLES;

				SpuRecordGeometry(&con1, pGeometry, &pPlantModel->m_GeometryCmd0[0], PNG_PLANTSMGR_LOCAL_GEOMETRY_MAXCMDSIZE, 8);
			}
		}
	}
	// Unbind LOD1
	SpuRecordShaderUnBind(&con1);

	// Bind LOD2 technique:
	SpuRecordShaderBind(&con1, m_shdDeferredLOD2TechniqueID, &gpShaderLOD2Cmd0[0], PNG_PLANTSMGR_LOCAL_BINDSHADERLOD2_MAXCMDSIZE, 3);
	DumpMemoryAsRSXDump("BindShaderLOD2", gpShaderLOD2Cmd0,  PNG_PLANTSMGR_LOCAL_BINDSHADERLOD2_MAXCMDSIZE);

	// there's only one LOD2 geometry to deal with:
	SpuRecordGeometry(&con1, sm_plantLOD2VertexBuffer, sm_plantLOD2VertexDecl, &gpPlantLOD2GeometryCmd0[0], PNG_PLANTSMGR_LOCAL_GEOMETRY_MAXCMDSIZE, 8);
	DumpMemoryAsRSXDump("LOD2 geometry", gpPlantLOD2GeometryCmd0, PNG_PLANTSMGR_LOCAL_GEOMETRY_MAXCMDSIZE);

	// Unbind LOD2
	SpuRecordShaderUnBind(&con1);

	// restore orig Rage context:
	g_CurrentGcmContext = oldRageContext;

	return true;
}

bool pngGrassRenderer::SpuRecordShaders()
{
	const int _cmdBufSize = 256;
	u32 _cmdBuf[_cmdBufSize]={0};	// fill with NOPs
	CellGcmContext		con1;
	SetupContextData(&con1, _cmdBuf, _cmdBufSize*sizeof(u32), &CustomGcmReserveFailed);

	// save orig Rage context:
	CellGcmContextData *oldRageContext = g_CurrentGcmContext;
	g_CurrentGcmContext = (CellGcmContextData*)&con1;

	// 1. Records LOD0 technique:
	SpuRecordShaderBind(&con1, m_shdDeferredLOD0TechniqueID, &gpShaderLOD0Cmd0[0], PNG_PLANTSMGR_LOCAL_BINDSHADERLOD0_MAXCMDSIZE, 3);
	SpuRecordShaderUnBind(&con1);

	// 2. Record LOD1 technique:
	SpuRecordShaderBind(&con1, m_shdDeferredLOD1TechniqueID, &gpShaderLOD1Cmd0[0], PNG_PLANTSMGR_LOCAL_BINDSHADERLOD1_MAXCMDSIZE, 1);
	SpuRecordShaderUnBind(&con1);

	// 3. Records LOD2 technique:
	SpuRecordShaderBind(&con1, m_shdDeferredLOD2TechniqueID, &gpShaderLOD2Cmd0[0], PNG_PLANTSMGR_LOCAL_BINDSHADERLOD2_MAXCMDSIZE, 3);
	SpuRecordShaderUnBind(&con1);

	return true;
}

bool pngGrassRenderer::DrawTriPlantsSPU(PPTriPlant *triPlants, int numTriPlants, u32 slotID)
{
	// enough space for new jobs data?
	if(s_NumGrassJobsAdded >= PNG_PLANTSMGR_MAX_NUM_OF_RENDER_JOBS)
	{
		Assert(false);	// should it EVER happen during 1 frame?
		//Printf("\n GrassRendererSPU: Max num of jobs (%d) already running! Freeing job=%d.", PNG_PLANTSMGR_MAX_NUM_OF_RENDER_JOBS, jobIndex0);

		// launch and wait for some launched jobs to finish first:
		pngGrassRenderer::SpuFinalisePerRenderFrame();

		rage::sysTaskManager::Wait(s_GrassTaskHandle);
		s_GrassTaskHandle = NULL;
		s_NumGrassJobsAdded = 0;
	}

	const u32 jobIndex = s_NumGrassJobsAdded;

	PPTriPlant *pCurrTriPlant	= triPlants;

	
	// copy data to aligned structures
	// todo: dma this instead?
	PPTriPlant *spuTriPlantPtr0		= &s_SpuTriPlantTab[jobIndex][0];
	sysMemCpy(spuTriPlantPtr0, pCurrTriPlant, sizeof(PPTriPlant)*numTriPlants);


	// setup data for this job:
	spuGrassParamsStruct *inSpuGrassStructPtr	= &s_InSpuGrassStructTab[jobIndex];
	spuGrassParamsStruct &inSpuGrassStruct		= *inSpuGrassStructPtr;
	spuGrassParamsStruct *outSpuGrassStructPtr	= &s_OutSpuGrassStructTab[jobIndex];
	spuGrassParamsStruct &outSpuGrassStruct		= *outSpuGrassStructPtr;
		
	inSpuGrassStruct.m_bDummyJob					= false;
	inSpuGrassStruct.m_slotID						= slotID;

	inSpuGrassStruct.m_pTriPlant					= spuTriPlantPtr0;
	Assert((spuTriPlantPtr0 & 127) == 0);
	inSpuGrassStruct.m_TriPlantDmaSize				= sizeof(PPTriPlant);
	CompileTimeAssertAligned(sizeof(PPTriPlant), 16);
	inSpuGrassStruct.m_numTriPlant					= numTriPlants;
	Assert(numTriPlants <= PPTRIPLANT_BUFFER_SIZE);

#if __DEV
	// stats only: to know which slot we're rendering
	inSpuGrassStruct.m_nLocTriPlantsSlotID = nLocTriPlantsSlotID;
#endif

	AssertAligned(&inSpuGrassStruct, 128);
	CompileTimeAssertAligned(sizeof(inSpuGrassStruct), 16);
	AssertAligned(&outSpuGrassStruct, 128);
	CompileTimeAssertAligned(sizeof(outSpuGrassStruct), 16);

	s_NumGrassJobsAdded++;

	return true;
}

static spuGrassParamsStructMaster	inSpuParamsMaster		ALIGNED(128);
static spuGrassParamsStructMaster	outSpuParamsMaster		ALIGNED(128);

// launches master job to draw grass:
bool pngGrassRenderer::SpuFinalisePerRenderFrame()
{
	// is there anything to do?
	if(!s_NumGrassJobsAdded)
		return(false);

	Assert(s_GrassTaskHandle==NULL);

	bool bVehCollisionEnabled=false;
	Vector4 vecVehCollisionB, vecVehCollisionM, vecVehCollisionR;

	GetGlobalVehCollisionParams(&bVehCollisionEnabled, &vecVehCollisionB, &vecVehCollisionM, &vecVehCollisionR);

	UpdateGlobalCameraPos();
	UpdateGlobalPlayerPos();

	grcEffect::SetGlobalVar(m_shdVehCollisionEnabledID, bVehCollisionEnabled);
	grcEffect::SetGlobalVar(m_shdVehCollisionBID, vecVehCollisionB);
	grcEffect::SetGlobalVar(m_shdVehCollisionMID, vecVehCollisionM);
	grcEffect::SetGlobalVar(m_shdVehCollisionRID, vecVehCollisionR);

	const grcViewport *pViewport = GetCurrentViewport();
	Assert(pViewport);

#if __BANK
	TweakBool bGeomCullingTestEnabled = gbPlantsGeometryCulling;
#else
	TweakBool bGeomCullingTestEnabled = true;
#endif

#if __BANK
	const float	globalLOD0AlphaCloseDist	= gbPlantsLOD0AlphaCloseDist;
	const float	globalLOD0AlphaFarDist		= gbPlantsLOD0AlphaFarDist;
	const float	globalLOD0FarDist			= gbPlantsLOD0FarDist;
	const float	globalLOD1CloseDist			= gbPlantsLOD1CloseDist;
	const float	globalLOD1FarDist			= gbPlantsLOD1FarDist;
	const float	globalLOD1Alpha0CloseDist	= gbPlantsLOD1Alpha0CloseDist;
	const float	globalLOD1Alpha0FarDist		= gbPlantsLOD1Alpha0FarDist;
	const float	globalLOD1Alpha1CloseDist	= gbPlantsLOD1Alpha1CloseDist;
	const float	globalLOD1Alpha1FarDist		= gbPlantsLOD1Alpha1FarDist;
	const float	globalLOD2CloseDist			= gbPlantsLOD2CloseDist;
	const float	globalLOD2FarDist			= gbPlantsLOD2FarDist;
	const float	globalLOD2Alpha0CloseDist	= gbPlantsLOD2Alpha0CloseDist;
	const float	globalLOD2Alpha0FarDist		= gbPlantsLOD2Alpha0FarDist;
	const float	globalLOD2Alpha1CloseDist	= gbPlantsLOD2Alpha1CloseDist;
	const float	globalLOD2Alpha1FarDist		= gbPlantsLOD2Alpha1FarDist;
#else
	const float	globalLOD0AlphaCloseDist	= PNG_PLANT_LOD0_ALPHA_CLOSE_DIST;
	const float	globalLOD0AlphaFarDist		= PNG_PLANT_LOD0_ALPHA_FAR_DIST;
	const float	globalLOD0FarDist			= PNG_PLANT_LOD0_FAR_DIST;
	const float	globalLOD1CloseDist			= PNG_PLANT_LOD1_CLOSE_DIST;
	const float	globalLOD1FarDist			= PNG_PLANT_LOD1_FAR_DIST;
	const float	globalLOD1Alpha0CloseDist	= PNG_PLANT_LOD1_ALPHA0_CLOSE_DIST;
	const float	globalLOD1Alpha0FarDist		= PNG_PLANT_LOD1_ALPHA0_FAR_DIST;
	const float	globalLOD1Alpha1CloseDist	= PNG_PLANT_LOD1_ALPHA1_CLOSE_DIST;
	const float	globalLOD1Alpha1FarDist		= PNG_PLANT_LOD1_ALPHA1_FAR_DIST;
	const float	globalLOD2CloseDist			= PNG_PLANT_LOD2_CLOSE_DIST;
	const float	globalLOD2FarDist			= PNG_PLANT_LOD2_FAR_DIST;
	const float	globalLOD2Alpha0CloseDist	= PNG_PLANT_LOD2_ALPHA0_CLOSE_DIST;
	const float	globalLOD2Alpha0FarDist		= PNG_PLANT_LOD2_ALPHA0_FAR_DIST;
	const float	globalLOD2Alpha1CloseDist	= PNG_PLANT_LOD2_ALPHA1_CLOSE_DIST;
	const float	globalLOD2Alpha1FarDist		= PNG_PLANT_LOD2_ALPHA1_FAR_DIST;
#endif
	const float	globalLOD0FarDist2			= globalLOD0FarDist*globalLOD0FarDist;
	const float	globalLOD1CloseDist2		= globalLOD1CloseDist*globalLOD1CloseDist;
	const float	globalLOD1FarDist2			= globalLOD1FarDist*globalLOD1FarDist;
	const float	globalLOD2CloseDist2		= globalLOD2CloseDist*globalLOD2CloseDist;
	const float	globalLOD2FarDist2			= globalLOD2FarDist*globalLOD2FarDist;


	// LOD0 alpha, umTimer:
	{
		Vector4 vecAlphaFade4;	// [XY=alphaFade01 | Z=umTimer | ?]
#if 1
		// square distance approach:
		const float closeDist	=	globalLOD0AlphaCloseDist;
		const float farDist		=	globalLOD0AlphaFarDist; 
		const float alphaDenom	= 1.0f / (farDist*farDist - closeDist*closeDist);
		vecAlphaFade4.x = (farDist*farDist) * alphaDenom; // x = (f2)  / (f2-c2)
		vecAlphaFade4.y = (-1.0f) * alphaDenom;		 // y = (-1.0)/ (f2-c2) 
#else
		// linear distance approach:
		const float alphaDenom = 1.0f / (m_farDist - m_closeDist);
		vecAlphaFade4.x = (m_farDist) * alphaDenom; // x = (f2)  / (f2-c2)
		vecAlphaFade4.y = (-1.0f) * alphaDenom;		 // y = (-1.0)/ (f2-c2) 
#endif
		vecAlphaFade4.z = (float)(2.0*PI*(double(sysTimer::GetTicks()*sysTimer::GetTicksToMilliseconds())/1000.0));
		vecAlphaFade4.w = 0.0f;
		m_Shader->SetVar(m_shdFadeAlphaDistUmTimerID, vecAlphaFade4);
	}

	// LOD1 alpha:
	{
		Vector4 vecAlphaFade4;	// [XY=alpha0Fade01 | ZW=alpha1Fade01]

		const float closeDist0	= globalLOD1Alpha0CloseDist;
		const float farDist0	= globalLOD1Alpha0FarDist;
		const float alphaDenom0 = 1.0f / (farDist0*farDist0 - closeDist0*closeDist0);
		vecAlphaFade4.x = (farDist0*farDist0) * alphaDenom0;	// x = (f2)  / (f2-c2)
		vecAlphaFade4.y = (-1.0f) * alphaDenom0;				// y = (-1.0)/ (f2-c2) 

		const float closeDist1	=	globalLOD1Alpha1CloseDist;
		const float farDist1	=	globalLOD1Alpha1FarDist;
		const float alphaDenom1 = 1.0f / (farDist1*farDist1 - closeDist1*closeDist1);
		vecAlphaFade4.z = (farDist1*farDist1) * alphaDenom1;	// x = (f2)  / (f2-c2)
		vecAlphaFade4.w = (-1.0f) * alphaDenom1;			// y = (-1.0)/ (f2-c2) 

		m_Shader->SetVar(m_shdFadeAlphaLOD1DistID, vecAlphaFade4);
	}

	// LOD2 alpha:
	{
		Vector4 vecAlphaFade4;	// [XY=alpha0Fade01 | ZW=alpha1Fade01]

		const float closeDist0	= globalLOD2Alpha0CloseDist;
		const float farDist0	= globalLOD2Alpha0FarDist;
		const float alphaDenom0 = 1.0f / (farDist0*farDist0 - closeDist0*closeDist0);
		vecAlphaFade4.x = (farDist0*farDist0) * alphaDenom0; // x = (f2)  / (f2-c2)
		vecAlphaFade4.y = (-1.0f) * alphaDenom0;		 // y = (-1.0)/ (f2-c2) 

		const float closeDist1	=	globalLOD2Alpha1CloseDist;
		const float farDist1	=	globalLOD2Alpha1FarDist;
		const float alphaDenom1 = 1.0f / (farDist1*farDist1 - closeDist1*closeDist1);
		vecAlphaFade4.z = (farDist1*farDist1) * alphaDenom1; // x = (f2)  / (f2-c2)
		vecAlphaFade4.w = (-1.0f) * alphaDenom1;		 // y = (-1.0)/ (f2-c2) 

		m_Shader->SetVar(m_shdFadeAlphaLOD2DistID, vecAlphaFade4);
	}

	// record all shaders for SPU:
	pngGrassRenderer::SpuRecordShaders();


	PUSH_TIMER("PlantsMgr::Render::WaitForRSX");
	// make sure RSX is not executing our previous buffer:
	// (another way to check this is to set label after RSX has returned to main CB
	// and read it here, however this way seem quicker):
	if(1)
	{
		CellGcmControl volatile *control = cellGcmGetControlRegister();
		u32 get = control->get;
		u32 deadman=20;

		while(((get >= s_BigHeapOffset[s_BigHeapId])	&& (get < (s_BigHeapOffset[s_BigHeapId]+PNG_PLANTSMGR_BIG_HEAP_SIZE)))		
			|| ((get >= s_StartupHeapOffset[s_BigHeapId])	&& (get < (s_StartupHeapOffset[s_BigHeapId]+STARTUP_HEAP_SIZE)))				
			)
		{
			sys_timer_usleep(30);
			get = control->get;
			if(!(--deadman))
			{
				deadman=20;
				Printf("\n PlantsMgrSPU: Task %X has to wait - RSX is still executing previous command buffer!", s_RsxLabel5_CurrentTaskID);
			}
		}
	}

	// make sure RSX really executed prevPrev command buffer:
	static u32 nSkipFirstFrames=2;	//skip first 2 frames
	if(nSkipFirstFrames)
	{
		nSkipFirstFrames--;
	}
	else
	{
		const u32 prevPrevTaskID = 0xFF000000|(((s_BigHeapId  )&0x1)<<8)|((s_RsxLabel5_CurrentTaskID-2)&0xFF);
		const u32 prevTaskID0	= 0xFF000000|(((s_BigHeapId-1)&0x1)<<8)|((s_RsxLabel5_CurrentTaskID-1)&0xFF);
		const u32 prevTaskID1	= ((s_RsxLabel5_CurrentTaskID-1)&0xFF);

		u32 label5 = *s_RsxLabelPtrs[PNG_PLANTSMGR_RSX_LABELI5];
		u32 deadman=20;
		while((label5 != prevPrevTaskID) && (label5 != prevTaskID0) && (label5 != prevTaskID1))
		{
			sys_timer_usleep(30);
			if(!(--deadman))
			{
				deadman=20;
				Printf("\n PlantsMgrSPU: PrevPrevJob %X still not executed by RSX [label5=%08X]!", prevPrevTaskID,label5);
			}
			label5 = *s_RsxLabelPtrs[PNG_PLANTSMGR_RSX_LABELI5];
		}
	}
	POP_TIMER(); //WaitForRSX()...
	

	BindPlantShader(m_shdDeferredLOD0TechniqueID);

	// set waiting marker for SPU:
	CellGcmContext		startupCon;
	CellGcmContextData*	startupConData = (CellGcmContextData*)&startupCon;
	SetupContextData(&startupCon, (u32*)s_StartupHeapPtr[s_BigHeapId], STARTUP_HEAP_SIZE, &CustomGcmReserveFailed);
	startupCon.SetWriteCommandLabel(s_RsxLabelIndices[PNG_PLANTSMGR_RSX_LABELI5], s_RsxLabel5_CurrentTaskID);

	// RSX: reset all labels to known state:
	startupCon.SetWriteCommandLabel(s_RsxLabelIndices[PNG_PLANTSMGR_RSX_LABELI1], 0);
	startupCon.SetWriteCommandLabel(s_RsxLabelIndices[PNG_PLANTSMGR_RSX_LABELI2], 0);
	startupCon.SetWriteCommandLabel(s_RsxLabelIndices[PNG_PLANTSMGR_RSX_LABELI3], 0);
	startupCon.SetWriteCommandLabel(s_RsxLabelIndices[PNG_PLANTSMGR_RSX_LABELI4], 0);

	// JTS in startupCon (will be patched to jump to big heap):
	u32 offsetJTS0 = ((u8*)startupConData->current) - ((u8*)startupConData->begin);
	while( offsetJTS0 & 0x0000000f )	// fix pointer to be 16 byte aligned
	{
		startupCon.SetNopCommand(1);
		offsetJTS0 = ((u8*)startupConData->current) - ((u8*)startupConData->begin);
	}
	AssertAligned(offsetJTS0, 16);

	u32 eaMainJTS = u32(s_StartupHeapPtr[s_BigHeapId]) + offsetJTS0;

	startupCon.SetJumpCommand(u32(s_StartupHeapOffset[s_BigHeapId]) + offsetJTS0);	// JTS
	startupCon.SetNopCommand(3);
	startupCon.SetNopCommand(4);

	const u32 rsxReturnOffsetToMainCB = (u32)PLANTS_GCM_OFFSET((u8*)startupCon.current);
	startupCon.SetNopCommand(8);
	startupCon.SetWriteCommandLabel(s_RsxLabelIndices[PNG_PLANTSMGR_RSX_LABELI5], 0xFF000000|(s_BigHeapId<<8)|s_RsxLabel5_CurrentTaskID);

	// jump from Rage's GCM FIFO to plants' startup FIFO and patch jump back:
	SPU_COMMAND(grcDevice__Jump, 0);
	// Local address of the jump target
	cmd->jumpLocalAddr	= PLANTS_GCM_OFFSET(startupCon.begin);
	// PPU address of the return jump
	cmd->jumpBack		= startupCon.current;
	AssertAligned(startupCon.current, 16);

	sysTaskParameters p;
	sysMemSet(&p,0,sizeof(p));

	// "master" job data:
	inSpuParamsMaster.m_nNumGrassJobsLaunched	= s_NumGrassJobsAdded;
	inSpuParamsMaster.m_inSpuGrassStructTab		= &s_InSpuGrassStructTab[0];
	
	inSpuParamsMaster.m_heapBegin				= (u32)s_BigHeap[s_BigHeapId];
	inSpuParamsMaster.m_heapBeginOffset			= s_BigHeapOffset[s_BigHeapId];
	inSpuParamsMaster.m_ReturnOffsetToMainCB	= rsxReturnOffsetToMainCB;
	inSpuParamsMaster.m_eaMainJTS				= eaMainJTS;
	
	inSpuParamsMaster.m_rsxLabel5_CurrentTaskID	= s_RsxLabel5_CurrentTaskID;		// value of wait tag in Label4 for current job
	inSpuParamsMaster.m_DeviceFrameCounter		= GRCDEVICE.GetFrameCounter();

	for(int i=0; i<5; i++)
	{
		inSpuParamsMaster.m_rsxLabelIndices[i]	= s_RsxLabelIndices[i];
		inSpuParamsMaster.m_eaRsxLabel[i]		= (u32)s_RsxLabelPtrs[i];
	}

	const Vector3 vecCameraPos = pngGrassRenderer::GetGlobalCameraPos();
	inSpuParamsMaster.m_vecCameraPos.SetVector3(vecCameraPos);
	inSpuParamsMaster.m_vecCameraPos.w			= 0.0f;
	inSpuParamsMaster.m_windBending				= GetGlobalWindBending();

	inSpuParamsMaster.m_LOD0FarDist2			= globalLOD0FarDist2;
	inSpuParamsMaster.m_LOD1CloseDist2			= globalLOD1CloseDist2;
	inSpuParamsMaster.m_LOD1FarDist2			= globalLOD1FarDist2;
	inSpuParamsMaster.m_LOD2CloseDist2			= globalLOD2CloseDist2;
	inSpuParamsMaster.m_LOD2FarDist2			= globalLOD2FarDist2;

	inSpuParamsMaster.m_BindShaderLOD0CmdBuf	= gpShaderLOD0Cmd0;
	inSpuParamsMaster.m_BindShaderLOD1CmdBuf	= gpShaderLOD1Cmd0;
	inSpuParamsMaster.m_BindShaderLOD2CmdBuf	= gpShaderLOD2Cmd0;
	inSpuParamsMaster.m_GeometryCmdLOD2			= gpPlantLOD2GeometryCmd0;

	for(int slotID=0; slotID<PNG_PLANT_NUM_PLANT_SLOTS; slotID++)
	{
		inSpuParamsMaster.m_PlantTexturesTab[slotID]= s_PPTriPlantBuffer.GetPlantTexturesCmd(slotID, 0);
		inSpuParamsMaster.m_PlantModelsTab[slotID]	= s_PPTriPlantBuffer.GetPlantModelsTab(slotID);
		AssertAligned(inSpuParamsMaster.m_PlantTexturesTab[slotID], 128);
		AssertAligned(inSpuParamsMaster.m_PlantModelsTab[slotID], 128);
	}
	CompileTimeAssertAligned(PNG_PLANTSMGR_LOCAL_SLOTSIZE_TEXTURE, 16);
	CompileTimeAssertAligned(PNG_PLANTSMGR_LOCAL_SLOTSIZE_MODEL, 16);

	inSpuParamsMaster.m_DecrementerTicks		= 0;
#if __DEV
	// statistics & debug:
	inSpuParamsMaster.m_bPlantsFlashLOD0		= gbPlantsFlashLOD0;
	inSpuParamsMaster.m_bPlantsFlashLOD1		= gbPlantsFlashLOD1;
	inSpuParamsMaster.m_bPlantsFlashLOD2		= gbPlantsFlashLOD2;
	inSpuParamsMaster.m_bGeomCullingTestEnabled	= bGeomCullingTestEnabled;

	for(int i=0; i<PNG_PLANT_NUM_PLANT_SLOTS; i++)
	{
		inSpuParamsMaster.m_LocTriPlantsLOD0DrawnPerSlot[i]	= 0;
		inSpuParamsMaster.m_LocTriPlantsLOD1DrawnPerSlot[i]	= 0;
		inSpuParamsMaster.m_LocTriPlantsLOD2DrawnPerSlot[i]	= 0;
	}
#endif //__BANK

	// get 6 viewport planes:
	const grcViewport *pViewport = GetCurrentViewport();
	Assert(pViewport);
	for(int i=0; i<6; i++)
	{
		inSpuParamsMaster.m_vecFrustumPlanes[i] = pViewport->GetFrustumClipPlane(i);
	}

	AssertAligned(&inSpuParamsMaster, 128);
	AssertAligned(&outSpuParamsMaster, 128);
	CompileTimeAssertAligned(sizeof(inSpuParamsMaster), 16);
	CompileTimeAssertAligned(sizeof(outSpuParamsMaster), 16);

	p.Input.Data	= (void*)&inSpuParamsMaster;
	p.Input.Size	= sizeof(inSpuParamsMaster);
	p.Output.Data	= (void*)&outSpuParamsMaster;
	p.Output.Size	= sizeof(outSpuParamsMaster);
	p.SpuStackSize	= 16 * 1024;	// allocate 16KB for stack

	// run the category update job
	sys_lwsync();
	s_GrassTaskHandle = sysTaskManager::Create(TASK_INTERFACE(PlantsGrassRendererSPU),p, sysTaskManager::SCHEDULER_GRAPHICS_OTHER);
	Assertf(s_GrassTaskHandle, "PSN: Error creating SPU grass job!");

	s_BigHeapId++;
	s_BigHeapId &= 0x01;

	s_RsxLabel5_CurrentTaskID++;
	s_RsxLabel5_CurrentTaskID &= 0xFF;

	UnBindPlantShader();

	return true;
}

bool pngGrassRenderer::SpuInitialisePerRenderFrame()
{
	if(s_GrassTaskHandle)
	{
		rage::sysTaskManager::Wait(s_GrassTaskHandle);
		s_GrassTaskHandle=NULL;
	}

	// reset num jobs launched:
	s_NumGrassJobsAdded = 0;

#if __DEV
	// rendering stats:
	for(int i=0; i<PNG_PLANT_NUM_PLANT_SLOTS; i++)
	{
		nLocTriPlantsLOD0Drawn[i] = outSpuParamsMaster.m_LocTriPlantsLOD0DrawnPerSlot[i];
		nLocTriPlantsLOD1Drawn[i] = outSpuParamsMaster.m_LocTriPlantsLOD1DrawnPerSlot[i];
		nLocTriPlantsLOD2Drawn[i] = outSpuParamsMaster.m_LocTriPlantsLOD2DrawnPerSlot[i];
	}

	nPlantsSpuBigBufferConsumed = outSpuParamsMaster.m_AmountOfBigHeapConsumed;
	nPlantsSpuBigBufferOverfilled = outSpuParamsMaster.m_AmountOfBigHeapOverfilled;
#endif //__DEV...

	return true;
}

#else // PNG_PLANTSMGR_USE_SPU_RENDERING

bool pngGrassRenderer::DrawTriPlants(PPTriPlant *triPlants, int numTriPlants, pngGrassModel* plantModelsTab)
{
	Assert(triPlants);
	Assert(plantModelsTab);

	const float		globalWindBending = GetGlobalWindBending();
	bool bVehCollisionEnabled=false;
	Vector4 vecVehCollisionB, vecVehCollisionM, vecVehCollisionR;

	GetGlobalVehCollisionParams(&bVehCollisionEnabled, &vecVehCollisionB, &vecVehCollisionM, &vecVehCollisionR);

	UpdateGlobalCameraPos();
	UpdateGlobalPlayerPos();

	grcEffect::SetGlobalVar(m_shdVehCollisionEnabledID, bVehCollisionEnabled);
	grcEffect::SetGlobalVar(m_shdVehCollisionBID, VECTOR4_TO_VEC4V(vecVehCollisionB));
	grcEffect::SetGlobalVar(m_shdVehCollisionMID, VECTOR4_TO_VEC4V(vecVehCollisionM));
	grcEffect::SetGlobalVar(m_shdVehCollisionRID, VECTOR4_TO_VEC4V(vecVehCollisionR));

	const grcViewport *pViewport = GetCurrentViewport();
	Assert(pViewport);

#if __BANK
	TweakBool bGeomCullingTestEnabled = gbPlantsGeometryCulling != 0;
#else
	TweakBool bGeomCullingTestEnabled = true;
#endif

#if __BANK
	const float	globalLOD0AlphaCloseDist	= gbPlantsLOD0AlphaCloseDist;
	const float	globalLOD0AlphaFarDist		= gbPlantsLOD0AlphaFarDist;
	const float	globalLOD0FarDist			= gbPlantsLOD0FarDist;
	const float	globalLOD1CloseDist			= gbPlantsLOD1CloseDist;
	const float	globalLOD1FarDist			= gbPlantsLOD1FarDist;
	const float	globalLOD1Alpha0CloseDist	= gbPlantsLOD1Alpha0CloseDist;
	const float	globalLOD1Alpha0FarDist		= gbPlantsLOD1Alpha0FarDist;
	const float	globalLOD1Alpha1CloseDist	= gbPlantsLOD1Alpha1CloseDist;
	const float	globalLOD1Alpha1FarDist		= gbPlantsLOD1Alpha1FarDist;
	const float	globalLOD2CloseDist			= gbPlantsLOD2CloseDist;
	const float	globalLOD2FarDist			= gbPlantsLOD2FarDist;
	const float	globalLOD2Alpha0CloseDist	= gbPlantsLOD2Alpha0CloseDist;
	const float	globalLOD2Alpha0FarDist		= gbPlantsLOD2Alpha0FarDist;
	const float	globalLOD2Alpha1CloseDist	= gbPlantsLOD2Alpha1CloseDist;
	const float	globalLOD2Alpha1FarDist		= gbPlantsLOD2Alpha1FarDist;
#else
	const float	globalLOD0AlphaCloseDist	= PNG_PLANT_LOD0_ALPHA_CLOSE_DIST;
	const float	globalLOD0AlphaFarDist		= PNG_PLANT_LOD0_ALPHA_FAR_DIST;
	const float	globalLOD0FarDist			= PNG_PLANT_LOD0_FAR_DIST;
	const float	globalLOD1CloseDist			= PNG_PLANT_LOD1_CLOSE_DIST;
	const float	globalLOD1FarDist			= PNG_PLANT_LOD1_FAR_DIST;
	const float	globalLOD1Alpha0CloseDist	= PNG_PLANT_LOD1_ALPHA0_CLOSE_DIST;
	const float	globalLOD1Alpha0FarDist		= PNG_PLANT_LOD1_ALPHA0_FAR_DIST;
	const float	globalLOD1Alpha1CloseDist	= PNG_PLANT_LOD1_ALPHA1_CLOSE_DIST;
	const float	globalLOD1Alpha1FarDist		= PNG_PLANT_LOD1_ALPHA1_FAR_DIST;
	const float	globalLOD2CloseDist			= PNG_PLANT_LOD2_CLOSE_DIST;
	const float	globalLOD2FarDist			= PNG_PLANT_LOD2_FAR_DIST;
	const float	globalLOD2Alpha0CloseDist	= PNG_PLANT_LOD2_ALPHA0_CLOSE_DIST;
	const float	globalLOD2Alpha0FarDist		= PNG_PLANT_LOD2_ALPHA0_FAR_DIST;
	const float	globalLOD2Alpha1CloseDist	= PNG_PLANT_LOD2_ALPHA1_CLOSE_DIST;
	const float	globalLOD2Alpha1FarDist		= PNG_PLANT_LOD2_ALPHA1_FAR_DIST;
#endif
	const float	globalLOD0FarDist2			= globalLOD0FarDist*globalLOD0FarDist;
	const float	globalLOD1CloseDist2		= globalLOD1CloseDist*globalLOD1CloseDist;
	const float	globalLOD1FarDist2			= globalLOD1FarDist*globalLOD1FarDist;
	const float	globalLOD2CloseDist2		= globalLOD2CloseDist*globalLOD2CloseDist;
	const float	globalLOD2FarDist2			= globalLOD2FarDist*globalLOD2FarDist;

	// LOD0 alpha, umTimer:
	{
		Vector4 vecAlphaFade4;	// [XY=alphaFade01 | Z=umTimer | ?]
#if 1
		// square distance approach:
		const float closeDist	=	globalLOD0AlphaCloseDist;
		const float farDist		=	globalLOD0AlphaFarDist; 
		const float alphaDenom	= 1.0f / (farDist*farDist - closeDist*closeDist);
		vecAlphaFade4.x = (farDist*farDist) * alphaDenom; // x = (f2)  / (f2-c2)
		vecAlphaFade4.y = (-1.0f) * alphaDenom;		 // y = (-1.0)/ (f2-c2) 
#else
		// linear distance approach:
		const float alphaDenom = 1.0f / (m_farDist - m_closeDist);
		vecAlphaFade4.x = (m_farDist) * alphaDenom; // x = (f2)  / (f2-c2)
		vecAlphaFade4.y = (-1.0f) * alphaDenom;		 // y = (-1.0)/ (f2-c2) 
#endif
		vecAlphaFade4.z = (float)(2.0*PI*(double(sysTimer::GetTicks()*sysTimer::GetTicksToMilliseconds())/1000.0));
		vecAlphaFade4.w = 0.0f;
		m_Shader->SetVar(m_shdFadeAlphaDistUmTimerID, vecAlphaFade4);
	}

	// LOD1 alpha:
	{
		Vector4 vecAlphaFade4;	// [XY=alpha0Fade01 | ZW=alpha1Fade01]
		
		const float closeDist0	= globalLOD1Alpha0CloseDist;
		const float farDist0	= globalLOD1Alpha0FarDist;
		const float alphaDenom0 = 1.0f / (farDist0*farDist0 - closeDist0*closeDist0);
		vecAlphaFade4.x = (farDist0*farDist0) * alphaDenom0;	// x = (f2)  / (f2-c2)
		vecAlphaFade4.y = (-1.0f) * alphaDenom0;				// y = (-1.0)/ (f2-c2) 
		
		const float closeDist1	=	globalLOD1Alpha1CloseDist;
		const float farDist1	=	globalLOD1Alpha1FarDist;
		const float alphaDenom1 = 1.0f / (farDist1*farDist1 - closeDist1*closeDist1);
		vecAlphaFade4.z = (farDist1*farDist1) * alphaDenom1;	// x = (f2)  / (f2-c2)
		vecAlphaFade4.w = (-1.0f) * alphaDenom1;			// y = (-1.0)/ (f2-c2) 
		
		m_Shader->SetVar(m_shdFadeAlphaLOD1DistID, vecAlphaFade4);
	}

	// LOD2 alpha:
	{
		Vector4 vecAlphaFade4;	// [XY=alpha0Fade01 | ZW=alpha1Fade01]

		const float closeDist0	= globalLOD2Alpha0CloseDist;
		const float farDist0	= globalLOD2Alpha0FarDist;
		const float alphaDenom0 = 1.0f / (farDist0*farDist0 - closeDist0*closeDist0);
		vecAlphaFade4.x = (farDist0*farDist0) * alphaDenom0; // x = (f2)  / (f2-c2)
		vecAlphaFade4.y = (-1.0f) * alphaDenom0;		 // y = (-1.0)/ (f2-c2) 

		const float closeDist1	=	globalLOD2Alpha1CloseDist;
		const float farDist1	=	globalLOD2Alpha1FarDist;
		const float alphaDenom1 = 1.0f / (farDist1*farDist1 - closeDist1*closeDist1);
		vecAlphaFade4.z = (farDist1*farDist1) * alphaDenom1; // x = (f2)  / (f2-c2)
		vecAlphaFade4.w = (-1.0f) * alphaDenom1;		 // y = (-1.0)/ (f2-c2) 

		m_Shader->SetVar(m_shdFadeAlphaLOD2DistID, vecAlphaFade4);
	}

	pngGrassInstance customInstDraw;
	Matrix34 plantMatrix;
	Vector4 plantColourf;

	customInstDraw.Reset();

	int TriIdx = 0;

	//do for all the TriPlants in the buffer
	while(numTriPlants > TriIdx)
	{	
		PPTriPlant *pCurrTriPlant = &triPlants[TriIdx];

		const Vector3 *v1 = (Vector3*)&(pCurrTriPlant->V1);
		const Vector3 *v2 = (Vector3*)&(pCurrTriPlant->V2);
		const Vector3 *v3 = (Vector3*)&(pCurrTriPlant->V3);

		// want same values for plant positions each time...
		g_PlantsRendRand.Reset(pCurrTriPlant->seed);

		// intensity support:  final_I = I + VarI*rand01()
		const s16 intensityVar = pCurrTriPlant->intensity_var;
		u16 intensity = pCurrTriPlant->intensity + u16(float(intensityVar)*g_PlantsRendRand.GetRanged(0.0f, 1.0f));
		intensity = Min<u16>(intensity, 255);

		Color32 col32;
		col32.SetAlpha(	pCurrTriPlant->color.GetAlpha());
		#define CALC_COLOR_INTENSITY(COLOR, I)			u8((u16(COLOR)*I) >> 8)
			col32.SetRed(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetRed(),		intensity));
			col32.SetGreen(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetGreen(),	intensity));
			col32.SetBlue(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetBlue(),	intensity));
		#undef CALC_COLOR_INTENSITY

		plantColourf.x = col32.GetRedf();
		plantColourf.y = col32.GetGreenf();
		plantColourf.z = col32.GetBluef();
		plantColourf.w = col32.GetAlphaf();


		grcTexture *pPlantTextureLOD0 = pCurrTriPlant->texture_ptr;
		Assert(pPlantTextureLOD0);
		grcTexture *pPlantTextureLOD1 = pCurrTriPlant->textureLOD1_ptr;
		Assert(pPlantTextureLOD1);

		grmGeometry *pGeometryLOD0		= plantModelsTab[pCurrTriPlant->model_id].m_pGeometry;
		const Vector4 boundSphereLOD0	= plantModelsTab[pCurrTriPlant->model_id].m_BoundingSphere; 
		grmGeometry *pGeometryLOD1		= plantModelsTab[pCurrTriPlant->model_id+PNG_PLANT_SLOT_NUM_MODELS].m_pGeometry;
		const Vector4 boundSphereLOD1	= plantModelsTab[pCurrTriPlant->model_id+PNG_PLANT_SLOT_NUM_MODELS].m_BoundingSphere; 
		const Vector2 GeometryDimLOD2	= plantModelsTab[pCurrTriPlant->model_id+PNG_PLANT_SLOT_NUM_MODELS].m_dimensionLOD2;
		const Vector4 boundSphereLOD2(0,0,0,1.0f);


		// set color for whole pTriLoc:
		m_Shader->SetVar(m_shdPlantColorID,		plantColourf);

		// set global micro-movement params: [ scaleH | scaleV | freqH | freqV ]
		m_Shader->SetVar(m_shdUMovementParamsID, pCurrTriPlant->um_param);

		// set global collision radius scale:
		m_Shader->SetVar(m_shdCollParamsID,		pCurrTriPlant->coll_params);

		// LOD2 billboard dimensions:
		Vector4 dimLOD2;
		dimLOD2.x = GeometryDimLOD2.x;
		dimLOD2.y = GeometryDimLOD2.y;
		dimLOD2.z = 0.0f;
		dimLOD2.w = 0.0f;
		m_Shader->SetVar(m_shdDimensionLOD2ID,	dimLOD2);

		// local per-vertex data: ground color(XYZ)+scale(W):
		Vector4 groundColorV1, groundColorV2, groundColorV3;
		Vector4 groundColorf;

		groundColorV1.x = pCurrTriPlant->groundColorV1.GetRedf();
		groundColorV1.y = pCurrTriPlant->groundColorV1.GetGreenf();
		groundColorV1.z = pCurrTriPlant->groundColorV1.GetBluef();
		groundColorV1.w = pngPlantLocTri::pvUnpackScale(pCurrTriPlant->groundColorV1.GetAlpha());

		groundColorV2.x = pCurrTriPlant->groundColorV2.GetRedf();
		groundColorV2.y = pCurrTriPlant->groundColorV2.GetGreenf();
		groundColorV2.z = pCurrTriPlant->groundColorV2.GetBluef();
		groundColorV2.w = pngPlantLocTri::pvUnpackScale(pCurrTriPlant->groundColorV2.GetAlpha());

		groundColorV3.x = pCurrTriPlant->groundColorV3.GetRedf();
		groundColorV3.y = pCurrTriPlant->groundColorV3.GetGreenf();
		groundColorV3.z = pCurrTriPlant->groundColorV3.GetBluef();
		groundColorV3.w = pngPlantLocTri::pvUnpackScale(pCurrTriPlant->groundColorV3.GetAlpha());

	// Render LOD0:
#if __BANK
	if((pCurrTriPlant->LODmask&0x01) && (!gbPlantsFlashLOD0 || (GRCDEVICE.GetFrameCounter()&0x01)))
#else
	if(pCurrTriPlant->LODmask&0x01)
#endif
	{
		// want same values for plant positions each time...
		g_PlantsRendRand.Reset(pCurrTriPlant->seed);

		m_Shader->SetVar(m_shdTextureID,		pPlantTextureLOD0);

		// bind plant shader			
		BindPlantShader(m_shdDeferredLOD0TechniqueID);
		customInstDraw.Reset();

		// Setup the vertex and index buffers
		grcIndexBuffer* pIndexBuffer			= pGeometryLOD0->GetIndexBuffer(true);
		grcVertexBuffer* pVertexBuffer			= pGeometryLOD0->GetVertexBuffer(true);
		const u16 nPrimType						= pGeometryLOD0->GetPrimitiveType();
		const u32 nIdxCount						= pIndexBuffer->GetIndexCount();
		grcVertexDeclaration* vertexDeclaration	= pGeometryLOD0->GetDecl();

		GRCDEVICE.SetVertexDeclaration(vertexDeclaration);
		GRCDEVICE.SetIndices(*pIndexBuffer);
		GRCDEVICE.SetStreamSource(0, *pVertexBuffer, 0, pVertexBuffer->GetVertexStride());

		// render a single triPlant full of the desired model
		const int count = pCurrTriPlant->num_plants;
		for(int i=0; i<count; i++)
		{
			const float s = g_PlantsRendRand.GetRanged(0.0f, 1.0f);
			const float t = g_PlantsRendRand.GetRanged(0.0f, 1.0f);
			Vector3 posPlant;
			pngGrassRenderer::GenPointInTriangle(&posPlant, v1, v2, v3, s, t);

			// calculate LOD0 distance:
			Vector3 LodDistV = posPlant - GetGlobalCameraPos(); 
			const float fLodDist2 = LodDistV.Mag2();
			const bool bLodVisible = (fLodDist2 < globalLOD0FarDist2);

			// early skip if current geometry not visible as LOD0:
			if(!bLodVisible)
			{
				g_PlantsRendRand.GetInt();
				g_PlantsRendRand.GetInt();
				g_PlantsRendRand.GetInt();
				g_PlantsRendRand.GetInt();
				if(intensityVar)
					g_PlantsRendRand.GetInt();

				continue;
			}

			// interpolate ground color(XYZ)+scale(W):
			pngGrassRenderer::GenPointInTriangle(&groundColorf, &groundColorV1, &groundColorV2, &groundColorV3, s, t);
			const float groundScaleVar0 = groundColorf.w;
			groundColorf.w = 1.0f;

			// per-vertex scale:
			// <-5; 0) - scales Z only
			// <+0; 5> - scales XYZ
			const float groundScaleVarXY= (groundScaleVar0>0.0f) ? groundScaleVar0 : 1.0f;
			const float groundScaleVarZ	= Abs(groundScaleVar0);

			// ---- apply various amounts of scaling to the matrix -----
			// calculate an x/y scaling value and apply to matrix

			// final_SclXY = SclXY + SclVarXY*rand01()
			const float	scvariationXY	= pCurrTriPlant->scale_var_xy;
			const float	scaleXY			= (pCurrTriPlant->scale.x + scvariationXY*g_PlantsRendRand.GetRanged(0.0f, 1.0f))*groundScaleVarXY;

			// calculate a z scaling value and apply to matrix
			const float	scvariationZ	= pCurrTriPlant->scale_var_z;
			const float	scaleZ			= (pCurrTriPlant->scale.y + scvariationZ*g_PlantsRendRand.GetRanged(0.0f, 1.0f))*groundScaleVarZ;
			// ----- end of scaling stuff ------

			// choose bigger scale for bound frustum check:
			const float boundRadiusScale = (scaleXY > scaleZ)? (scaleXY) : (scaleZ);

			Vector4 sphere;
			sphere.Set(boundSphereLOD0);
			sphere.AddVector3XYZ(posPlant);
			sphere.w *= boundRadiusScale;

			// skip current geometry if not visible:
			if(bGeomCullingTestEnabled && (!pViewport->grcViewport::IsSphereVisibleInline(VECTOR4_TO_VEC4V(sphere))))
			{
				g_PlantsRendRand.GetInt();
				g_PlantsRendRand.GetInt();
				if(intensityVar)
					g_PlantsRendRand.GetInt();
				
				continue;
			}

			plantMatrix.MakeRotateZ(g_PlantsRendRand.GetRanged(0.0f, 1.0f)*2.0f*PI);// overwrites a,b,c
			plantMatrix.MakeTranslate(posPlant);									// overwrites d
			plantMatrix.Scale(scaleXY, scaleXY, scaleZ);	// scale in XY+Z

			// ---- muck about with the matrix to make atomic look blown by the wind --------
			const float variationBend = pCurrTriPlant->wind_bend_var;
			const float bending =	(1.0f + (variationBend*g_PlantsRendRand.GetRanged(0.0f, 1.0f))) *
									(globalWindBending * pCurrTriPlant->wind_bend_scale);
			plantMatrix.c.x = bending;
			plantMatrix.c.y = bending;
			// ----- end of wind stuff -----

			// color variation:
			if(intensityVar)
			{
				// intensity support:  final_I = I + VarI*rand01()
				u16 intensity = pCurrTriPlant->intensity + u16(float(intensityVar)*g_PlantsRendRand.GetRanged(0.0f, 1.0f));
				intensity = Min<u16>(intensity, 255);

				Color32 col32;
				#define CALC_COLOR_INTENSITY(COLOR, I)			u8((u16(COLOR)*I) >> 8)
					col32.SetRed(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetRed(),		intensity));
					col32.SetGreen(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetGreen(),	intensity));
					col32.SetBlue(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetBlue(),	intensity));
				#undef CALC_COLOR_INTENSITY

				plantColourf.x = col32.GetRedf();
				plantColourf.y = col32.GetGreenf();
				plantColourf.z = col32.GetBluef();
			}

			customInstDraw.m_WorldMatrix.FromMatrix34(plantMatrix);
			customInstDraw.m_PlantColor.Set(plantColourf);
			customInstDraw.m_GroundColor.Set(groundColorf);

			const bool bOptimizedSetup = customInstDraw.UseOptimizedVsSetup(plantMatrix);
			if(bOptimizedSetup)
			{	// optimized setup: 3 registers:
				GRCDEVICE.SetVertexShaderConstant(67, customInstDraw.GetRegisterPointerOpt(), customInstDraw.GetRegisterCountOpt());
				GRCDEVICE.DrawIndexedPrimitive((grcDrawMode)nPrimType, 0, nIdxCount);
			}
			else
			{	// full setup: 6 registers:
				GRCDEVICE.SetVertexShaderConstant(64, customInstDraw.GetRegisterPointerFull(), customInstDraw.GetRegisterCountFull());
				GRCDEVICE.DrawIndexedPrimitive((grcDrawMode)nPrimType, 0, nIdxCount);
			}
#if __DEV
			(*g_LocTriPlantsLOD0DrawnPerSlot)++;
#endif //__DEV
		}

		UnBindPlantShader();
	}

	// Render LOD1:
#if __BANK
	if((pCurrTriPlant->LODmask&0x02) && (!gbPlantsFlashLOD1 || (GRCDEVICE.GetFrameCounter()&0x01)))
#else
	if(pCurrTriPlant->LODmask&0x02)
#endif
	{
			g_PlantsRendRand.Reset(pCurrTriPlant->seed);

			m_Shader->SetVar(m_shdTextureID,	pPlantTextureLOD1);

			// Bind the grass shader
			BindPlantShader(m_shdDeferredLOD1TechniqueID);
			customInstDraw.Reset();

			// Setup the vertex and index buffers
			grcIndexBuffer* pIndexBuffer			= pGeometryLOD1->GetIndexBuffer(true);
			grcVertexBuffer* pVertexBuffer			= pGeometryLOD1->GetVertexBuffer(true);
			const u16 nPrimType						= pGeometryLOD1->GetPrimitiveType();
			const u32 nIdxCount						= pIndexBuffer->GetIndexCount();
			grcVertexDeclaration* vertexDeclaration	= pGeometryLOD1->GetDecl();

			GRCDEVICE.SetVertexDeclaration(vertexDeclaration);
			GRCDEVICE.SetIndices(*pIndexBuffer);
			GRCDEVICE.SetStreamSource(0, *pVertexBuffer, 0, pVertexBuffer->GetVertexStride());

			// render a single triPlant full of the desired model
			const int count = pCurrTriPlant->num_plants;
			for(int i=0; i<count; i++)
			{
				const float s = g_PlantsRendRand.GetRanged(0.0f, 1.0f);
				const float t = g_PlantsRendRand.GetRanged(0.0f, 1.0f);
				Vector3 posPlant;
				pngGrassRenderer::GenPointInTriangle(&posPlant, v1, v2, v3, s, t);

				// calculate LOD1 distance:
				Vector3 LodDistV = posPlant - GetGlobalCameraPos(); 
				const float fLodDist2 = LodDistV.Mag2();
				const bool bLodVisible = (fLodDist2 > globalLOD1CloseDist2)	&& (fLodDist2 < globalLOD1FarDist2);
	
				// early skip if current geometry not visible as LOD1:
				if(!bLodVisible)
				{
					g_PlantsRendRand.GetInt();
					g_PlantsRendRand.GetInt();
					g_PlantsRendRand.GetInt();
					g_PlantsRendRand.GetInt();
					if(intensityVar)
						g_PlantsRendRand.GetInt();

					continue;
				}

				// interpolate ground color(XYZ)+scale(W):
				pngGrassRenderer::GenPointInTriangle(&groundColorf, &groundColorV1, &groundColorV2, &groundColorV3, s, t);
				const float groundScaleVar0 = groundColorf.w;
				groundColorf.w = 1.0f;

				// per-vertex scale:
				// <-5; 0) - scales Z only
				// <+0; 5> - scales XYZ
				const float groundScaleVarXY= (groundScaleVar0>0.0f) ? groundScaleVar0 : 1.0f;
				const float groundScaleVarZ	= Abs(groundScaleVar0);

				// ---- apply various amounts of scaling to the matrix -----
				// calculate an x/y scaling value and apply to matrix

				// final_SclXY = SclXY + SclVarXY*rand01()
				const float	scvariationXY	= pCurrTriPlant->scale_var_xy;
				const float	scaleXY			= (pCurrTriPlant->scale.x + scvariationXY*g_PlantsRendRand.GetRanged(0.0f, 1.0f))*groundScaleVarXY;

				// calculate a z scaling value and apply to matrix
				const float	scvariationZ	= pCurrTriPlant->scale_var_z;
				const float	scaleZ			= (pCurrTriPlant->scale.y + scvariationZ*g_PlantsRendRand.GetRanged(0.0f, 1.0f))*groundScaleVarZ;
				// ----- end of scaling stuff ------

				// choose bigger scale for bound frustum check:
				const float boundRadiusScale = (scaleXY > scaleZ)? (scaleXY) : (scaleZ);

				Vector4 sphere;
				sphere.Set(boundSphereLOD1);
				sphere.AddVector3XYZ(posPlant);
				sphere.w *= boundRadiusScale;

				// skip current geometry if not visible:
				if(bGeomCullingTestEnabled && (!pViewport->grcViewport::IsSphereVisibleInline(VECTOR4_TO_VEC4V(sphere))))
				{
					g_PlantsRendRand.GetInt();
					g_PlantsRendRand.GetInt();
					if(intensityVar)
						g_PlantsRendRand.GetInt();
					
					continue;
				}

				plantMatrix.MakeRotateZ(g_PlantsRendRand.GetRanged(0.0f, 1.0f)*2.0f*PI);// overwrites a,b,c
				plantMatrix.MakeTranslate(posPlant);									// overwrites d
				plantMatrix.Scale(scaleXY, scaleXY, scaleZ);	// scale in XY+Z

				// ---- muck about with the matrix to make atomic look blown by the wind --------
				g_PlantsRendRand.GetInt();
				// ----- end of wind stuff -----

				// color variation:
				if(intensityVar)
				{
					// intensity support:  final_I = I + VarI*rand01()
					u16 intensity = pCurrTriPlant->intensity + u16(float(intensityVar)*g_PlantsRendRand.GetRanged(0.0f, 1.0f));
					intensity = Min<u16>(intensity, 255);

					Color32 col32;
#define CALC_COLOR_INTENSITY(COLOR, I)			u8((u16(COLOR)*I) >> 8)
					col32.SetRed(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetRed(),		intensity));
					col32.SetGreen(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetGreen(),	intensity));
					col32.SetBlue(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetBlue(),	intensity));
#undef CALC_COLOR_INTENSITY

					plantColourf.x = col32.GetRedf();
					plantColourf.y = col32.GetGreenf();
					plantColourf.z = col32.GetBluef();
				}

				customInstDraw.m_WorldMatrix.FromMatrix34(plantMatrix);
				customInstDraw.m_PlantColor.Set(plantColourf);
				customInstDraw.m_GroundColor.Set(groundColorf);

				const bool bOptimizedSetup = customInstDraw.UseOptimizedVsSetup(plantMatrix);
				if(bOptimizedSetup)
				{	// optimized setup: 3 registers:
					GRCDEVICE.SetVertexShaderConstant(67, customInstDraw.GetRegisterPointerOpt(), customInstDraw.GetRegisterCountOpt());
					GRCDEVICE.DrawIndexedPrimitive((grcDrawMode)nPrimType, 0, nIdxCount);
				}
				else
				{	// full setup: 6 registers:
					GRCDEVICE.SetVertexShaderConstant(64, customInstDraw.GetRegisterPointerFull(), customInstDraw.GetRegisterCountFull());
					GRCDEVICE.DrawIndexedPrimitive((grcDrawMode)nPrimType, 0, nIdxCount);
				}
#if __DEV
				(*g_LocTriPlantsLOD1DrawnPerSlot)++;
#endif //__DEV
			}

			UnBindPlantShader();
		
		}

		// Render LOD2:
#if __BANK
		if((pCurrTriPlant->LODmask&0x04) && (!gbPlantsFlashLOD2 || (GRCDEVICE.GetFrameCounter()&0x01)))
#else
		if(pCurrTriPlant->LODmask&0x04)
#endif
		{
			g_PlantsRendRand.Reset(pCurrTriPlant->seed);

			m_Shader->SetVar(m_shdTextureID,	pPlantTextureLOD1);

			// Bind the grass shader
			BindPlantShader(m_shdDeferredLOD2TechniqueID);
			customInstDraw.Reset();

		
			// Setup the vertex and index buffers
			grcVertexBuffer* pVertexBuffer			= sm_plantLOD2VertexBuffer;
			grcVertexDeclaration* vertexDeclaration	= sm_plantLOD2VertexDecl;

			GRCDEVICE.SetVertexDeclaration(vertexDeclaration);
			GRCDEVICE.SetStreamSource(0, *pVertexBuffer, 0, pVertexBuffer->GetVertexStride());

			//
			// render a single triPlant full of the desired model
			const int count = pCurrTriPlant->num_plants;
			for(int i=0; i<count; i++)
			{
				const float s = g_PlantsRendRand.GetRanged(0.0f, 1.0f);
				const float t = g_PlantsRendRand.GetRanged(0.0f, 1.0f);
				Vector3 posPlant;
				pngGrassRenderer::GenPointInTriangle(&posPlant, v1, v2, v3, s, t);

				// calculate LOD2 distance:
				Vector3 LodDistV = posPlant - GetGlobalCameraPos(); 
				const float fLodDist2 = LodDistV.Mag2();
				const bool bLodVisible = (fLodDist2 > globalLOD2CloseDist2)	&& (fLodDist2 < globalLOD2FarDist2);

				// early skip if current geometry not visible as LOD2:
				if(!bLodVisible)
				{
					g_PlantsRendRand.GetInt();
					g_PlantsRendRand.GetInt();
					g_PlantsRendRand.GetInt();
					g_PlantsRendRand.GetInt();
					if(intensityVar)
						g_PlantsRendRand.GetInt();

					continue;
				}

				// interpolate ground color(XYZ)+scale(W):
				pngGrassRenderer::GenPointInTriangle(&groundColorf, &groundColorV1, &groundColorV2, &groundColorV3, s, t);
				groundColorf.w = 1.0f;

				// ---- apply various amounts of scaling to the matrix -----
				// calculate an x/y scaling value and apply to matrix
				g_PlantsRendRand.GetInt();

				// calculate a z scaling value and apply to matrix
				g_PlantsRendRand.GetInt();
				// ----- end of scaling stuff ------

				Vector4 sphere;
				sphere.Set(boundSphereLOD2);
				sphere.AddVector3XYZ(posPlant);

				// skip current geometry if not visible:
				if(bGeomCullingTestEnabled && (!pViewport->grcViewport::IsSphereVisibleInline(VECTOR4_TO_VEC4V(sphere))))
				{
					g_PlantsRendRand.GetInt();
					g_PlantsRendRand.GetInt();
					if(intensityVar)
						g_PlantsRendRand.GetInt();

					continue;
				}

				g_PlantsRendRand.GetInt();
				plantMatrix.Identity3x3();
				plantMatrix.MakeTranslate(posPlant);									// overwrites d

				// ---- muck about with the matrix to make atomic look blown by the wind --------
				g_PlantsRendRand.GetInt();
				// ----- end of wind stuff -----

				// color variation:
				if(intensityVar)
				{
					// intensity support:  final_I = I + VarI*rand01()
					u16 intensity = pCurrTriPlant->intensity + u16(float(intensityVar)*g_PlantsRendRand.GetRanged(0.0f, 1.0f));
					intensity = Min<u16>(intensity, 255);

					Color32 col32;
#define CALC_COLOR_INTENSITY(COLOR, I)			u8((u16(COLOR)*I) >> 8)
					col32.SetRed(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetRed(),		intensity));
					col32.SetGreen(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetGreen(),	intensity));
					col32.SetBlue(	CALC_COLOR_INTENSITY(pCurrTriPlant->color.GetBlue(),	intensity));
#undef CALC_COLOR_INTENSITY

					plantColourf.x = col32.GetRedf();
					plantColourf.y = col32.GetGreenf();
					plantColourf.z = col32.GetBluef();
				}

				customInstDraw.m_WorldMatrix.FromMatrix34(plantMatrix);
				customInstDraw.m_PlantColor.Set(plantColourf);
				customInstDraw.m_GroundColor.Set(groundColorf);

				const bool bOptimizedSetup = customInstDraw.UseOptimizedVsSetup(plantMatrix);
				if(bOptimizedSetup)
				{	// optimized setup: 3 registers:
					GRCDEVICE.SetVertexShaderConstant(67, customInstDraw.GetRegisterPointerOpt(), customInstDraw.GetRegisterCountOpt());
					GRCDEVICE.DrawPrimitive((grcDrawMode)drawQuads, /*startVertex*/0, /*vertexCount*/4 );
				}
				else
				{	// full setup: 6 registers:
					GRCDEVICE.SetVertexShaderConstant(64, customInstDraw.GetRegisterPointerFull(), customInstDraw.GetRegisterCountFull());
					GRCDEVICE.DrawPrimitive((grcDrawMode)drawQuads, /*startVertex*/0, /*vertexCount*/4 );
				}
#if __DEV
				// rendering stats:
				(*g_LocTriPlantsLOD2DrawnPerSlot)++;
#endif //__DEV
			}

			UnBindPlantShader();

		}
		TriIdx++;
	}

    return true;
}
#endif // PNG_PLANTSMGR_USE_SPU_RENDERING

void pngGrassRenderer::BindPlantShader(grcEffectTechnique forcedTech)
{
	// Bind the grass shader
	ASSERT_ONLY(int numPasses =) m_Shader->BeginDraw(grmShader::RMC_DRAW, /*restoreState*/false, /*techOverride*/forcedTech);
	Assert(numPasses>0);
	const int pass=0;

#if GRCORE_ON_SPU
	// MIGRATE_FIXME
//	static TweakBool bForceOld = false;
	// bind shader on SPU
//	if(g_SetShaderLocalVarsForSpu && !bForceOld)
//	{
//		m_Shader->BindOnSPU(pass, grmShader::RMC_DRAW);
//	}
//	else
	{
		m_Shader->Bind(pass);
	}
#else //GRCORE_ON_SPU...
	m_Shader->Bind(pass);
#endif //GRCORE_ON_SPU...
}

void pngGrassRenderer::UnBindPlantShader()
{
	GRCDEVICE.ClearStreamSource(0);

	// Unbind the shader
#if GRCORE_ON_SPU
// MIGRATE_FIXME
//	const int pass=0;
//	static TweakBool bForceOld = false;
//	if(g_SetShaderLocalVarsForSpu && !bForceOld)
//	{
//		m_Shader->UnBindOnSPU(pass, grmShader::RMC_DRAW);
//	}
//	else
	{
		m_Shader->UnBind();
	}
#else //GRCORE_ON_SPU
	m_Shader->UnBind();
#endif //GRCORE_ON_SPU...
	m_Shader->EndDraw();
}

pngPPTriPlantBuffer::pngPPTriPlantBuffer()
: m_currentIndex(0)
{
	this->SetPlantModelsSet(PPPLANTBUF_MODEL_SET0);
	
	for(int i=0; i<PPTRIPLANT_MODELS_TAB_SIZE; i++)
	{
		m_pPlantModelsTab[i] = NULL;
	}
}

// flush the buffer:
void pngPPTriPlantBuffer::Flush()
{
	if(m_currentIndex>0)
	{
		pngGrassModel*	plantModels = NULL;

		switch(this->m_plantModelsSet)
		{
			case(PPPLANTBUF_MODEL_SET0):	plantModels = m_pPlantModelsTab[0];		break;
			case(PPPLANTBUF_MODEL_SET1):	plantModels = m_pPlantModelsTab[1];		break;
			case(PPPLANTBUF_MODEL_SET2):	plantModels = m_pPlantModelsTab[2];		break;
			case(PPPLANTBUF_MODEL_SET3):	plantModels = m_pPlantModelsTab[3];		break;
			default:						plantModels = NULL;						break;
		}
		Assertf(plantModels, "Unknown PlantModelsSet!");

		// save current seed:
		const u32 storedSeed = g_PlantsRendRand.GetSeed();

#if PNG_PLANTSMGR_USE_SPU_RENDERING
		{
			const u32 slotID = this->m_plantModelsSet;
			pngGrassRenderer::DrawTriPlantsSPU(m_Buffer, m_currentIndex, slotID);
			m_currentIndex = 0;
		}
#else //PNG_PLANTSMGR_USE_SPU_RENDERING...
		{
			pngGrassRenderer::DrawTriPlants(m_Buffer, m_currentIndex, plantModels);
			m_currentIndex = 0;
		}
#endif //PNG_PLANTSMGR_USE_SPU_RENDERING...

		// restore randomness:
		g_PlantsRendRand.Reset(storedSeed);
	}
}

PPTriPlant* pngPPTriPlantBuffer::GetPPTriPlantPtr(int amountToAdd)
{
	if((this->m_currentIndex+amountToAdd) > PPTRIPLANT_BUFFER_SIZE)
	{
		this->Flush();
	}

	return(&this->m_Buffer[m_currentIndex]);
}

void pngPPTriPlantBuffer::ChangeCurrentPlantModelsSet(int newSet)
{
	// different modes of pipeline?
	if(this->GetPlantModelsSet() != newSet)
	{
		// flush contents of old pipeline mode:
		this->Flush();
		
		// set new pipeline mode:
		this->SetPlantModelsSet(newSet);
	}
}

void pngPPTriPlantBuffer::IncreaseBufferIndex(int pipeMode, int amount)
{
	if(this->GetPlantModelsSet() != pipeMode)
	{
		// incompatible pipeline modes!
		Assertf(false, "Incompatible pipeline modes!");
		return;
	}

	this->m_currentIndex += amount;
	if(this->m_currentIndex >= PPTRIPLANT_BUFFER_SIZE)
	{
		this->Flush();
	}
}

bool pngPPTriPlantBuffer::SetPlantModelsTab(u32 index, pngGrassModel* pPlantModels)
{
	if(index >= PPTRIPLANT_MODELS_TAB_SIZE)
		return(false);
		
	this->m_pPlantModelsTab[index] = pPlantModels;

	return true;
}

pngGrassModel* pngPPTriPlantBuffer::GetPlantModelsTab(u32 index)
{
	if(index >= PPTRIPLANT_MODELS_TAB_SIZE)
		return(NULL);
	pngGrassModel* plantModels = this->m_pPlantModelsTab[index];
	return(plantModels);
}

#if PNG_PLANTSMGR_USE_SPU_RENDERING
// internal buffers for storing texture command buffers:
bool pngPPTriPlantBuffer::SetPlantTexturesCmd(u32 slotID, u32 index, u32 *cmd)
{
	Assert(slotID >= 0);
	Assert(slotID < PPTRIPLANT_MODELS_TAB_SIZE);
	Assert(index >= 0);
	Assert(index < PNG_PLANT_SLOT_NUM_TEXTURES*2);	// base textures + LOD textures

	u32 *dst0 = m_pPlantsTexturesCmdTab[slotID];
	u32 *dst = dst0 + index*PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE;

	::sysMemCpy(dst, cmd, PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE*sizeof(u32));

	return true;
}

u32* pngPPTriPlantBuffer::GetPlantTexturesCmd(u32 slotID, u32 index)
{
	u32 *dst0 = m_pPlantsTexturesCmdTab[slotID];
	u32 *dst = dst0 + index*PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE;
	return(dst);
}

bool pngPPTriPlantBuffer::AllocateTextureBuffers()
{
	m_pPlantsTexturesCmd0 = rage_aligned_new (128) u32[PNG_PLANT_SLOT_NUM_TEXTURES*2*PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE];
	Assert(m_pPlantsTexturesCmd0);
	m_pPlantsTexturesCmd1 = rage_aligned_new (128) u32[PNG_PLANT_SLOT_NUM_TEXTURES*2*PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE];
	Assert(m_pPlantsTexturesCmd1);
	m_pPlantsTexturesCmd2 = rage_aligned_new (128) u32[PNG_PLANT_SLOT_NUM_TEXTURES*2*PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE];
	Assert(m_pPlantsTexturesCmd2);
	m_pPlantsTexturesCmd3 = rage_aligned_new (128) u32[PNG_PLANT_SLOT_NUM_TEXTURES*2*PNG_PLANTSMGR_LOCAL_TEXTURE_MAXCMDSIZE];
	Assert(m_pPlantsTexturesCmd3);

	m_pPlantsTexturesCmdTab[0] = m_pPlantsTexturesCmd0;
	m_pPlantsTexturesCmdTab[1] = m_pPlantsTexturesCmd1;
	m_pPlantsTexturesCmdTab[2] = m_pPlantsTexturesCmd2;
	m_pPlantsTexturesCmdTab[3] = m_pPlantsTexturesCmd3;

	return true;
}

void pngPPTriPlantBuffer::DestroyTextureBuffers()
{
	delete [] m_pPlantsTexturesCmd0;
	m_pPlantsTexturesCmd0 = NULL;
	delete [] m_pPlantsTexturesCmd1;
	m_pPlantsTexturesCmd1 = NULL;
	delete [] m_pPlantsTexturesCmd2;
	m_pPlantsTexturesCmd2 = NULL;
	delete [] m_pPlantsTexturesCmd3;
	m_pPlantsTexturesCmd3 = NULL;
}
#endif // PNG_PLANTSMGR_USE_SPU_RENDERING

} // namespace rage
