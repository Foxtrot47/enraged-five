//
// plantsgrass/plantsmgr.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "grcore/indexbuffer.h"
//#include "grcore/state.h"
#include "grcore/texture.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grmodel/geometry.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "math/random.h"
#include "phbound/bound.h"
#include "phbound/boundbox.h"
#include "phbound/boundgeom.h"
#include "physics/inst.h"
#include "physics/levelnew.h"
#include "plantsgrass/plantsgrassrenderer.h"
#include "plantsgrass/plantsmgr.h"
#include "rmcore/drawable.h"
#include "spatialdata/aabb.h"
#include "grprofile/pix.h"
#include "system/task.h"
#include "system/timemgr.h"
#include "system/xtl.h"
#include "vector/matrix34.h"
#include "vector/vector3.h"

#define DEFERRED_MATERIAL_DEFAULT 0x7
#define DEFERRED_MATERIAL_GRASS 0x25

#define TweakFloat float
#define TweakBool bool

#if PNG_PLANTSMGR_USE_SPU_RENDERING
#include "plantsgrass/plantsgrassrendererspu.h"
#endif // PNG_PLANTSMGR_USE_SPU_RENDERING

#define PSN_ALPHATOMASK_PASS		(0 && __PPU)
#define SPU_PLANTMGR_UPDATE			(0 && __PPU) // TJ

namespace rage
{

// per-vertex ground color (RGB) + scale variation (A)
const Color32 DEFAULT_GROUND_COLOR	= Color32(111, 110, 92, PV_SCALE_ONE);
static Color32 gbDefaultGroundColor = DEFAULT_GROUND_COLOR;

#if __BANK
// stuff for dbg VarConsole:
#include "bank/bank.h"
#include "bank/bkmgr.h"
static u8	gbDisplaypngPlantMgrInfo		= false;
static u8	gbShowpngPlantMgrPolys 			= false;
static u8	gbShowpngPlantMgrGroundColor	= false;

static u8		gbForceGroundColor			= false;
static Color32	gbGroundColorV1(255,0,  0,  255);
static Color32	gbGroundColorV2(0,  255,0,  255);
static Color32	gbGroundColorV3(0,  0,  255,255);
static float	gbGroundScaleV1				= 1.0f;
static float	gbGroundScaleV2				= 1.0f;
static float	gbGroundScaleV3				= 1.0f; 

u8	gbPlantsFlashLOD0						= false;
u8	gbPlantsFlashLOD1						= false;
u8	gbPlantsFlashLOD2						= false;

// LOD0:
float	gbPlantsLOD0AlphaCloseDist		= PNG_PLANT_LOD0_ALPHA_CLOSE_DIST;
float	gbPlantsLOD0AlphaFarDist		= PNG_PLANT_LOD0_ALPHA_FAR_DIST;
float	gbPlantsLOD0FarDist				= PNG_PLANT_LOD0_FAR_DIST;

// LOD1:
float	gbPlantsLOD1CloseDist			= PNG_PLANT_LOD1_CLOSE_DIST;
float	gbPlantsLOD1FarDist				= PNG_PLANT_LOD1_FAR_DIST;
float	gbPlantsLOD1Alpha0CloseDist		= PNG_PLANT_LOD1_ALPHA0_CLOSE_DIST;
float	gbPlantsLOD1Alpha0FarDist		= PNG_PLANT_LOD1_ALPHA0_FAR_DIST;
float	gbPlantsLOD1Alpha1CloseDist		= PNG_PLANT_LOD1_ALPHA1_CLOSE_DIST;
float	gbPlantsLOD1Alpha1FarDist		= PNG_PLANT_LOD1_ALPHA1_FAR_DIST;

// LOD2:
float	gbPlantsLOD2CloseDist			= PNG_PLANT_LOD2_CLOSE_DIST;
float	gbPlantsLOD2FarDist				= PNG_PLANT_LOD2_FAR_DIST;
float	gbPlantsLOD2Alpha0CloseDist		= PNG_PLANT_LOD2_ALPHA0_CLOSE_DIST;
float	gbPlantsLOD2Alpha0FarDist		= PNG_PLANT_LOD2_ALPHA0_FAR_DIST;
float	gbPlantsLOD2Alpha1CloseDist		= PNG_PLANT_LOD2_ALPHA1_CLOSE_DIST;
float	gbPlantsLOD2Alpha1FarDist		= PNG_PLANT_LOD2_ALPHA1_FAR_DIST;

static u8	gbDoubleSidedCulling		= false;
u8			gbPlantsGeometryCulling		= true;
#endif // __BANK

#if !__FINAL
u8 gbPlantMgrActive						= true;
#endif // !__FINAL

extern u32 g_AllowVertexBufferVramLocks;

// use 2D collsion calculation for LocTris:
#define USE_COLLISION_2D_DIST					(1)

#if __DEV
#define	DBG_COUNT_DRAWN_PLANTS (1)
// static u32 g_LocTriDrawn[PNG_PLANT_NUM_PLANT_SLOTS] = {0};
u32 g_LocTriPlantsLOD0Drawn[PNG_PLANT_NUM_PLANT_SLOTS]={0};
u32 g_LocTriPlantsLOD1Drawn[PNG_PLANT_NUM_PLANT_SLOTS]={0};
u32 g_LocTriPlantsLOD2Drawn[PNG_PLANT_NUM_PLANT_SLOTS]={0};
u32 *g_LocTriPlantsLOD0DrawnPerSlot = &g_LocTriPlantsLOD0Drawn[0];
u32 *g_LocTriPlantsLOD1DrawnPerSlot = &g_LocTriPlantsLOD1Drawn[0];
u32 *g_LocTriPlantsLOD2DrawnPerSlot = &g_LocTriPlantsLOD2Drawn[0];
u32 g_LocTriPlantsSlotID = 0;

#if	PNG_PLANTSMGR_USE_SPU_RENDERING
u32 g_PlantsSpuBigBufferConsumed = 0;
u32 g_PlantsSpuBigBufferOverfilled = 0;
#endif // PNG_PLANTSMGR_USE_SPU_RENDERING
#endif //__DEV

// private random generator for PlantsMgr (it's used during RenderThread)
// (g_DrawRand is used by stuff in UpdateThread and can't be shared)
mthRandom g_PlantsRendRand;
pngPlantMgr gPlantMgr;

pngPlantMgr::pngPlantMgr()
{
	m_scanCode = 0;

	sysMemSet(m_CloseLocTriListRenderHead, 0, sizeof(m_CloseLocTriListRenderHead));
	m_RTbufferID=0;

#if PNG_PLANTMGR_ENABLED
#if PNG_PLANTSMGR_USE_SPU_RENDERING
	sysMemSet(m_PlantTextureTab0, 0, sizeof(m_PlantTextureTab0));
	sysMemSet(m_PlantTextureTab1, 0, sizeof(m_PlantTextureTab1));
	sysMemSet(m_PlantTextureTab2, 0, sizeof(m_PlantTextureTab2));
	sysMemSet(m_PlantTextureTab3, 0, sizeof(m_PlantTextureTab3));
	sysMemSet(m_PlantSlotTextureTab, 0, sizeof(m_PlantSlotTextureTab));
#else
	sysMemSet(m_PlantModelsTab0, 0, sizeof(m_PlantModelsTab0));
	sysMemSet(m_PlantModelsTab1, 0, sizeof(m_PlantModelsTab1));
	sysMemSet(m_PlantModelsTab2, 0, sizeof(m_PlantModelsTab2));
	sysMemSet(m_PlantModelsTab3, 0, sizeof(m_PlantModelsTab3));
#endif // PNG_PLANTSMGR_USE_SPU_RENDERING
	sysMemSet(m_PlantModelSlotTab, 0, sizeof(m_PlantModelSlotTab));
#endif // PNG_PLANTMGR_ENABLED

#if __DEV
	m_fdevTriLocFarDistSqr	= 45.0f*45.0f;
	m_fdevAlphaCloseDist	= 20.0f;
	m_fdevAlphaFarDist		= 45.0f;
#endif // __DEV
}

#if PNG_PLANTMGR_ENABLED
static grcTexture* LoadTexture(char *texname)
{
	//grcTexture *pTexture = gpPlantMgrTextureDictionary->Lookup(texname);
	grcTexture *pTexture = grcTextureFactory::GetInstance().CreateTexture(texname);
	Assertf(pTexture, "Cannot find plant texture '%s'!", texname);
	pTexture->AddRef();

#if __ASSERT
	// check plant texture dimensions:
	const int texWidth = pTexture->GetWidth();
	const int texHeight= pTexture->GetHeight();

	if( (texWidth==8)	||	(texWidth==16)	||	(texWidth==32)	||	(texWidth==64)	||	(texWidth==128)	||	(texWidth==256)	||	(texWidth==512)	||
		(texHeight==8)	||	(texHeight==16)	||	(texHeight==32)	||	(texHeight==64)	||	(texHeight==128)||	(texHeight==256)||	(texHeight==512)
	)	
	{
		// do nothing - dimensions are OK
	}
	else
	{
		Warningf("PlantsMgr: texture '%s': %dx%d/n", texname, texWidth, texHeight);
		Assertf(false, "PlantsMgr: texture '%s': %dx%d/n", texname, texWidth, texHeight);
	}

	if((texWidth > 512) || (texHeight > 512))
		Assertf(false, "PlantsMgr: texture '%s': %dx%d/n", texname, texWidth, texHeight);
#endif // __ASSERT

	return(pTexture);
}

bool pngPlantMgr::LoadPlantModel(const char* filename, pngGrassModel *pPlantModel)
{
	Assert(pPlantModel);

	rmcDrawable* pDrawable = rage_new rmcDrawable();
	// TODO: support loading from resources
	pDrawable->Load(filename);

	// create new shadergroup with one shader:
	grmShaderGroup *pNewShaderGroup = grmShaderFactory::GetInstance().CreateGroup();
	Assert(pNewShaderGroup);

	// TODO: Load grass shader here
	grmShader *pGrassShader = pngGrassRenderer::GetGrassShader();
	Assert(pGrassShader);
	pNewShaderGroup->InitCount(1);
	pNewShaderGroup->Add(pGrassShader);

	pDrawable->SetShaderGroup(pNewShaderGroup);

	rmcLodGroup *pLodGroup = &pDrawable->GetLodGroup();
	Assert(pLodGroup);
	rmcLod *pLod = &pLodGroup->GetLod(0);
	Assert(pLod);
	grmModel *pModel = pLod->GetModel(0);
	Assert(pModel);

	const int geomCount	= pModel->GetGeometryCount();
	Assert(geomCount > 0);
	
	// setup model shader indices:
	for(int i=0; i<geomCount; i++)
	{
		pModel->SetShaderIndex(i, 0);

#if __ASSERT
		// verify boundSphere radius:
		Vector4 boundSphere = pModel->GetAABBs()[i].GetBoundingSphere();
		Assert(boundSphere.w >= 0.01f);
		// verify vertex stream format available in this geometry:
		grmGeometry *pGeometry = &pModel->GetGeometry(i);
		Assert(pGeometry);
		Assert(pGeometry->GetType() != grmGeometry::GEOMETRYEDGE);
		const grcFvf *pFvf = pGeometry->GetFvf();
		Assert(pFvf);
		Assertf(pFvf->GetPosChannel(),		"PlantsMgr: Geometry is missing POSITION channel!");
		Assertf(pFvf->GetDiffuseChannel(),	"PlantsMgr: Geometry is missing COLOR0 channel!");
		// TJ
		//Assertf(pFvf->GetSpecularChannel(),	"PlantsMgr: Geometry is missing COLOR1 channel!");
		Assertf(pFvf->GetTextureChannel(0),	"PlantsMgr: Geometry is missing TEXCOORD0 channel!");
#endif // __ASSERT
	}

	// return plantModel:
	pPlantModel->m_pDrawable = pDrawable;
	Assert(pPlantModel->m_pDrawable);
	pPlantModel->m_pGeometry = &pModel->GetGeometry(0);
	Assert(pPlantModel->m_pGeometry);
	pPlantModel->m_BoundingSphere = pModel->GetAABBs()[0].GetBoundingSphere();
	Assert(pPlantModel->m_BoundingSphere.w > 0.0f);
	
	return true;
}

void pngPlantMgr::UnloadPlantModel(pngGrassModel *model)
{
	rmcDrawable *pDrawable = model->m_pDrawable;
	if (pDrawable)
	{
		// Get shader group
		grmShaderGroup &shadergroup = pDrawable->GetShaderGroup();
		// Remove reference to the shaders : they're owned by the grassRenderer, and probably
		// already deleted by now. (only one by now).
		for(int i=0;i<shadergroup.GetCount();i++)
		{
			shadergroup.Remove(shadergroup.GetShaderPtr(i),false);
		}
		
		//pDrawable->SetShaderGroup(NULL);
		delete pDrawable;
		pDrawable = NULL;
	}

	model->m_pDrawable = NULL;
	model->m_pGeometry = NULL;
	model->m_BoundingSphere.Set(0);
}

bool pngPlantMgr::LoadPlantModelLOD2(const char* /*filename*/, pngGrassModel* pPlantModel)
{
	Assert(pPlantModel);

	// TODO: load from filename
	rmcDrawable* pDrawable = NULL;

	rmcLodGroup *pLodGroup	= &pDrawable->GetLodGroup();
	Assert(pLodGroup);
	rmcLod		*pLod		= &pLodGroup->GetLod(0);
	Assert(pLod);
	grmModel	*pModel		= pLod->GetModel(0);
	Assert(pModel);

#if __ASSERT
	const int geomCount	= pModel->GetGeometryCount();
	Assertf(geomCount==1, "PlantsMgr: LOD2: Only simple billboard geometries allowed");
#endif // __ASSERT

	grmGeometry *pGeometry = &pModel->GetGeometry(0);
	Assert(pGeometry);
	grcVertexBuffer *pVB	= pGeometry->GetVertexBuffer();
	Assert(pVB);
	
	g_AllowVertexBufferVramLocks++;	// PSN: attempting to edit VBs in VRAM

	grcVertexBufferEditor vbEditor(pVB);

#if __ASSERT
	grcIndexBuffer *pIB		= pGeometry->GetIndexBuffer();
	Assert(pIB);

	const int indexCount	= pIB->GetIndexCount();
	const int primCount	= pGeometry->GetPrimitiveCount();
	Assert(indexCount == 6);
	Assert(primCount*3 == indexCount);	// make sure it's tri list (drawTris);

	const u16* pIndices	= pIB->LockRO();
	Printf("/n indexCount=%d, primCount=%d", indexCount, primCount);
	for(int i=0; i<indexCount; i++)
	{
		const int idx = pIndices[i];
		Vector3	pos		= vbEditor.GetPosition(idx);
		Printf("/n %d: index=%d: xyz=%.2f %.2f %.2f", i, idx, pos.x, pos.y, pos.z);
	}
#endif // __ASSERT

	Vector3 v0 = vbEditor.GetPosition(0);
	Vector3 v1 = vbEditor.GetPosition(1);
	Vector3 v2 = vbEditor.GetPosition(2);
	Vector3 v3 = vbEditor.GetPosition(3);

	
	float width = Abs(v0.x) + Abs(v2.x);
	float height= Abs(v0.z) + Abs(v1.z);

	// second try (try to pick different verts to calculate dimensions):
	if(width <= 0.0f)	width = Abs(v0.x) + Abs(v1.x);
	if(height <= 0.0f)	height= Abs(v0.z) + Abs(v2.z);

	Assert(width  > 0.0f);
	Assert(height > 0.0f);

#if __ASSERT
	Printf("/nPlantsMgr: LOD2 detected billboard W=%.2f and H=%.2f.", width, height);
#endif // __ASSERT

	g_AllowVertexBufferVramLocks++;	// PSN: finished with editing VBs in VRAM

	// return results:
	pPlantModel->m_dimensionLOD2.x = width;
	pPlantModel->m_dimensionLOD2.y = height;

	return true;
}
#endif // PNG_PLANTMGR_ENABLED

bool pngPlantMgr::Initialise()
{
	return gPlantMgr.InitialiseInternal();
}

bool pngPlantMgr::InitialiseInternal()
{
		if(!pngPlantMgr::ReloadConfig())
		{
			return false;
		}

#if PNG_PLANTMGR_ENABLED
		if(!pngGrassRenderer::Initialise())
		{
			return false;
		}

		m_PlantTextureTab0[0+0] = LoadTexture("txgrass0_0");
		m_PlantTextureTab0[0+1] = LoadTexture("txgrass0_1");
		m_PlantTextureTab0[0+2] = LoadTexture("txgrass0_2");
		m_PlantTextureTab0[0+3] = LoadTexture("txgrass0_3");
		m_PlantTextureTab0[0+4] = LoadTexture("txgrass0_4");
		m_PlantTextureTab0[0+5] = LoadTexture("txgrass0_5");
		m_PlantTextureTab0[0+6] = LoadTexture("txgrass0_6");
		m_PlantTextureTab0[0+7] = LoadTexture("txgrass0_7");
		m_PlantTextureTab0[8+0] = LoadTexture("txgrass0_0lod1");
		m_PlantTextureTab0[8+1] = LoadTexture("txgrass0_1lod1");
		m_PlantTextureTab0[8+2] = LoadTexture("txgrass0_2lod1");
		m_PlantTextureTab0[8+3] = LoadTexture("txgrass0_3lod1");
		m_PlantTextureTab0[8+4] = LoadTexture("txgrass0_4lod1");
		m_PlantTextureTab0[8+5] = LoadTexture("txgrass0_5lod1");
		m_PlantTextureTab0[8+6] = LoadTexture("txgrass0_6lod1");
		m_PlantTextureTab0[8+7] = LoadTexture("txgrass0_7lod1");

		m_PlantTextureTab1[0+0] = LoadTexture("txgrass1_0");
		m_PlantTextureTab1[0+1] = LoadTexture("txgrass1_1");
		m_PlantTextureTab1[0+2] = LoadTexture("txgrass1_2");
		m_PlantTextureTab1[0+3] = LoadTexture("txgrass1_3");
		m_PlantTextureTab1[0+4] = LoadTexture("txgrass1_4");
		m_PlantTextureTab1[0+5] = LoadTexture("txgrass1_5");
		m_PlantTextureTab1[0+6] = LoadTexture("txgrass1_6");
		m_PlantTextureTab1[0+7] = LoadTexture("txgrass1_7");
		m_PlantTextureTab1[8+0] = LoadTexture("txgrass1_0lod1");
		m_PlantTextureTab1[8+1] = LoadTexture("txgrass1_1lod1");
		m_PlantTextureTab1[8+2] = LoadTexture("txgrass1_2lod1");
		m_PlantTextureTab1[8+3] = LoadTexture("txgrass1_3lod1");
		m_PlantTextureTab1[8+4] = LoadTexture("txgrass1_4lod1");
		m_PlantTextureTab1[8+5] = LoadTexture("txgrass1_5lod1");
		m_PlantTextureTab1[8+6] = LoadTexture("txgrass1_6lod1");
		m_PlantTextureTab1[8+7] = LoadTexture("txgrass1_7lod1");

		m_PlantTextureTab2[0+0] = LoadTexture("txgrass2_0");
		m_PlantTextureTab2[0+1] = LoadTexture("txgrass2_1");
		m_PlantTextureTab2[0+2] = LoadTexture("txgrass2_2");
		m_PlantTextureTab2[0+3] = LoadTexture("txgrass2_3");
		m_PlantTextureTab2[0+4] = LoadTexture("txgrass2_4");
		m_PlantTextureTab2[0+5] = LoadTexture("txgrass2_5");
		m_PlantTextureTab2[0+6] = LoadTexture("txgrass2_6");
		m_PlantTextureTab2[0+7] = LoadTexture("txgrass2_7");
		m_PlantTextureTab2[8+0] = LoadTexture("txgrass2_0lod1");
		m_PlantTextureTab2[8+1] = LoadTexture("txgrass2_1lod1");
		m_PlantTextureTab2[8+2] = LoadTexture("txgrass2_2lod1");
		m_PlantTextureTab2[8+3] = LoadTexture("txgrass2_3lod1");
		m_PlantTextureTab2[8+4] = LoadTexture("txgrass2_4lod1");
		m_PlantTextureTab2[8+5] = LoadTexture("txgrass2_5lod1");
		m_PlantTextureTab2[8+6] = LoadTexture("txgrass2_6lod1");
		m_PlantTextureTab2[8+7] = LoadTexture("txgrass2_7lod1");

		m_PlantTextureTab3[0+0] = LoadTexture("txgrass3_0");
		m_PlantTextureTab3[0+1] = LoadTexture("txgrass3_1");
		m_PlantTextureTab3[0+2] = LoadTexture("txgrass3_2");
		m_PlantTextureTab3[0+3] = LoadTexture("txgrass3_3");
		m_PlantTextureTab3[0+4] = LoadTexture("txgrass3_4");
		m_PlantTextureTab3[0+5] = LoadTexture("txgrass3_5");
		m_PlantTextureTab3[0+6] = LoadTexture("txgrass3_6");
		m_PlantTextureTab3[0+7] = LoadTexture("txgrass3_7");
		m_PlantTextureTab3[8+0] = LoadTexture("txgrass3_0lod1");
		m_PlantTextureTab3[8+1] = LoadTexture("txgrass3_1lod1");
		m_PlantTextureTab3[8+2] = LoadTexture("txgrass3_2lod1");
		m_PlantTextureTab3[8+3] = LoadTexture("txgrass3_3lod1");
		m_PlantTextureTab3[8+4] = LoadTexture("txgrass3_4lod1");
		m_PlantTextureTab3[8+5] = LoadTexture("txgrass3_5lod1");
		m_PlantTextureTab3[8+6] = LoadTexture("txgrass3_6lod1");
		m_PlantTextureTab3[8+7] = LoadTexture("txgrass3_7lod1");

		m_PlantSlotTextureTab[0] = &m_PlantTextureTab0[0];
		m_PlantSlotTextureTab[1] = &m_PlantTextureTab1[0];
		m_PlantSlotTextureTab[2] = &m_PlantTextureTab2[0];
		m_PlantSlotTextureTab[3] = &m_PlantTextureTab3[0];
	
		// setup texture commands for SPU (all base and LOD textures):
		for(int slot=0; slot<PNG_PLANT_NUM_PLANT_SLOTS; slot++)
		{
			for(int i=0; i<(PNG_PLANT_SLOT_NUM_TEXTURES*2); i++)
			{
				pngGrassRenderer::SpuRecordGrassTexture(slot, i, m_PlantSlotTextureTab[slot][i]);
			}
		}

		LoadPlantModel("grass0_0",			&m_PlantModelsTab0[0+0]);
#if 0 // TJ - we only have the above model right now
		LoadPlantModel("grass0_1",			&m_PlantModelsTab0[0+1]);
		LoadPlantModel("grass0_2",			&m_PlantModelsTab0[0+2]);
		LoadPlantModel("grass0_3",			&m_PlantModelsTab0[0+3]);
		LoadPlantModel("grass0_0lod1",		&m_PlantModelsTab0[4+0]);
		LoadPlantModel("grass0_1lod1",		&m_PlantModelsTab0[4+1]);
		LoadPlantModel("grass0_2lod1",		&m_PlantModelsTab0[4+2]);
		LoadPlantModel("grass0_3lod1",		&m_PlantModelsTab0[4+3]);
		LoadPlantModelLOD2("grass0_0lod2",	&m_PlantModelsTab0[4+0]);
		LoadPlantModelLOD2("grass0_1lod2",	&m_PlantModelsTab0[4+1]);
		LoadPlantModelLOD2("grass0_2lod2",	&m_PlantModelsTab0[4+2]);
		LoadPlantModelLOD2("grass0_3lod2",	&m_PlantModelsTab0[4+3]);

		LoadPlantModel("grass1_0",			&m_PlantModelsTab1[0+0]);
		LoadPlantModel("grass1_1",			&m_PlantModelsTab1[0+1]);
		LoadPlantModel("grass1_2",			&m_PlantModelsTab1[0+2]);
		LoadPlantModel("grass1_3",			&m_PlantModelsTab1[0+3]);
		LoadPlantModel("grass1_0lod1",		&m_PlantModelsTab1[4+0]);
		LoadPlantModel("grass1_1lod1",		&m_PlantModelsTab1[4+1]);
		LoadPlantModel("grass1_2lod1",		&m_PlantModelsTab1[4+2]);
		LoadPlantModel("grass1_3lod1",		&m_PlantModelsTab1[4+3]);
		LoadPlantModelLOD2("grass1_0lod2",	&m_PlantModelsTab1[4+0]);
		LoadPlantModelLOD2("grass1_1lod2",	&m_PlantModelsTab1[4+1]);
		LoadPlantModelLOD2("grass1_2lod2",	&m_PlantModelsTab1[4+2]);
		LoadPlantModelLOD2("grass1_3lod2",	&m_PlantModelsTab1[4+3]);

		LoadPlantModel("grass2_0",			&m_PlantModelsTab2[0+0]);
		LoadPlantModel("grass2_1",			&m_PlantModelsTab2[0+1]);
		LoadPlantModel("grass2_2",			&m_PlantModelsTab2[0+2]);
		LoadPlantModel("grass2_3",			&m_PlantModelsTab2[0+3]);
		LoadPlantModel("grass2_0lod1",		&m_PlantModelsTab2[4+0]);
		LoadPlantModel("grass2_1lod1",		&m_PlantModelsTab2[4+1]);
		LoadPlantModel("grass2_2lod1",		&m_PlantModelsTab2[4+2]);
		LoadPlantModel("grass2_3lod1",		&m_PlantModelsTab2[4+3]);
		LoadPlantModelLOD2("grass2_0lod2",	&m_PlantModelsTab2[4+0]);
		LoadPlantModelLOD2("grass2_1lod2",	&m_PlantModelsTab2[4+1]);
		LoadPlantModelLOD2("grass2_2lod2",	&m_PlantModelsTab2[4+2]);
		LoadPlantModelLOD2("grass2_3lod2",	&m_PlantModelsTab2[4+3]);

		LoadPlantModel("grass3_0",			&m_PlantModelsTab3[0+0]);
		LoadPlantModel("grass3_1",			&m_PlantModelsTab3[0+1]);
		LoadPlantModel("grass3_2",			&m_PlantModelsTab3[0+2]);
		LoadPlantModel("grass3_3",			&m_PlantModelsTab3[0+3]);
		LoadPlantModel("grass3_0lod1",		&m_PlantModelsTab3[4+0]);
		LoadPlantModel("grass3_1lod1",		&m_PlantModelsTab3[4+1]);
		LoadPlantModel("grass3_2lod1",		&m_PlantModelsTab3[4+2]);
		LoadPlantModel("grass3_3lod1",		&m_PlantModelsTab3[4+3]);
		LoadPlantModelLOD2("grass3_0lod2",	&m_PlantModelsTab3[4+0]);
		LoadPlantModelLOD2("grass3_1lod2",	&m_PlantModelsTab3[4+1]);
		LoadPlantModelLOD2("grass3_2lod2",	&m_PlantModelsTab3[4+2]);
		LoadPlantModelLOD2("grass3_3lod2",	&m_PlantModelsTab3[4+3]);
#endif // 0

		m_PlantModelSlotTab[0] = m_PlantModelsTab0;
		m_PlantModelSlotTab[1] = m_PlantModelsTab1;
		m_PlantModelSlotTab[2] = m_PlantModelsTab2;
		m_PlantModelSlotTab[3] = m_PlantModelsTab3;

		// setup geometry buffers for GrassRenderer:
		pngGrassRenderer::SetPlantModelsTab(PPPLANTBUF_MODEL_SET0,	m_PlantModelSlotTab[0]);
		pngGrassRenderer::SetPlantModelsTab(PPPLANTBUF_MODEL_SET1,	m_PlantModelSlotTab[1]);
		pngGrassRenderer::SetPlantModelsTab(PPPLANTBUF_MODEL_SET2,	m_PlantModelSlotTab[2]);
		pngGrassRenderer::SetPlantModelsTab(PPPLANTBUF_MODEL_SET3,	m_PlantModelSlotTab[3]);

		if(!CreateGeometryLOD2())
		{
			return false;
		}

		if(!pngGrassRenderer::SpuRecordGeometries())
		{
			return false;
		}
#endif // PNG_PLANTMGR_ENABLED

		return true;
}

// Create LOD2 vertexbuffer and declaration:
bool pngPlantMgr::CreateGeometryLOD2()
{
	grcVertexElement elements[1] =
	{
		grcVertexElement( 0, grcVertexElement::grcvetTexture, 0, grcFvf::GetDataSizeFromType(grcFvf::grcdsFloat2), grcFvf::grcdsFloat2 , grcFvf::grcsfmModulo, 4 )
	};

	Vector2 quadVerts[4] = 
	{
		Vector2(1.0f, 0.0f), 
		Vector2(0.0f, 0.0f), 
		Vector2(0.0f, 1.0f),
		Vector2(1.0f, 1.0f) 
	};

	// set up quad vertex buffer for instancing.
	const int repeatAmount = 4;
	const int channel = 0;
		
	grcFvf fvf0;
	fvf0.SetTextureChannel( channel, true, grcFvf::grcdsFloat2);
	fvf0.SetPosChannel( false );
	grcVertexBuffer* buf = grcVertexBuffer::Create(repeatAmount, fvf0,  false);
	Assert(buf);

	grcVertexBufferEditor editor(buf);
	for(int j=0; j<repeatAmount; j++ )
	{
		int index = j;
		editor.SetUV(index, channel, quadVerts[index], false );
	}

	grcVertexDeclaration	*decl = GRCDEVICE.CreateVertexDeclaration( elements, NELEM(elements) );
	Assert(decl);

	pngGrassRenderer::SetGeometryLOD2(buf, decl);

	return true;
}

#if __BANK
static void ReloadDataCB()
{
//	g_procObjMan.ReloadData();
	gPlantMgr.ReloadConfig();
}

// debug widgets:
bool pngPlantMgr::AddBankWidgets(bkBank& bank)
{
	bank.PushGroup("PlantsMgr", false);
		bank.AddToggle("pngPlantMgr active",				&gbPlantMgrActive,			0xFF);
		bank.AddToggle("Display Debug Info",			&gbDisplaypngPlantMgrInfo,	0xFF);
		bank.AddToggle("Show LocTri Polys",				&gbShowpngPlantMgrPolys,		0xFF);
		bank.AddToggle("LOD0: Flash Instances",			&gbPlantsFlashLOD0,			0xFF);
		bank.AddToggle("LOD1: Flash Instances",			&gbPlantsFlashLOD1,			0xFF);
		bank.AddToggle("LOD2: Flash Instances",			&gbPlantsFlashLOD2,			0xFF);

		bank.PushGroup("LOD Fading Control", false);
			bank.AddSlider("LOD0: Alpha Close Distance:",	&gbPlantsLOD0AlphaCloseDist,0.0f, PNG_PLANT_TRILOC_FAR_DIST, 0.25f);
			bank.AddSlider("LOD0: Alpha Far Distance:",		&gbPlantsLOD0AlphaFarDist,	0.0f, PNG_PLANT_TRILOC_FAR_DIST, 0.25f);
			bank.AddSlider("LOD0: Far Distance:",			&gbPlantsLOD0FarDist,		0.0f, PNG_PLANT_TRILOC_FAR_DIST, 0.25f);

			bank.AddSlider("LOD1: Close Distance:",				&gbPlantsLOD1CloseDist,		0.0f, PNG_PLANT_TRILOC_FAR_DIST, 0.25f);
			bank.AddSlider("LOD1: Far Distance:",				&gbPlantsLOD1FarDist,		0.0f, PNG_PLANT_TRILOC_FAR_DIST, 0.25f);
			bank.AddSlider("LOD1: Close Alpha Close Distance:",	&gbPlantsLOD1Alpha0CloseDist,0.0f,PNG_PLANT_TRILOC_FAR_DIST, 0.25f);
			bank.AddSlider("LOD1: Close Alpha Far Distance:",	&gbPlantsLOD1Alpha0FarDist,	0.0f, PNG_PLANT_TRILOC_FAR_DIST, 0.25f);
			bank.AddSlider("LOD1: Far Alpha Close Distance:",	&gbPlantsLOD1Alpha1CloseDist,0.0f,PNG_PLANT_TRILOC_FAR_DIST, 0.25f);
			bank.AddSlider("LOD1: Far Alpha Far Distance:",		&gbPlantsLOD1Alpha1FarDist,	0.0f, PNG_PLANT_TRILOC_FAR_DIST, 0.25f);
			
			bank.AddSlider("LOD2: Close Distance:",				&gbPlantsLOD2CloseDist,		0.0f, PNG_PLANT_TRILOC_FAR_DIST, 0.25f);
			bank.AddSlider("LOD2: Far Distance:",				&gbPlantsLOD2FarDist,		0.0f, PNG_PLANT_TRILOC_FAR_DIST, 0.25f);
			bank.AddSlider("LOD2: Close Alpha Close Distance:",	&gbPlantsLOD2Alpha0CloseDist,0.0f,PNG_PLANT_TRILOC_FAR_DIST, 0.25f);
			bank.AddSlider("LOD2: Close Alpha Far Distance:",	&gbPlantsLOD2Alpha0FarDist,	0.0f, PNG_PLANT_TRILOC_FAR_DIST, 0.25f);
			bank.AddSlider("LOD2: Far Alpha Close Distance:",	&gbPlantsLOD2Alpha1CloseDist,0.0f,PNG_PLANT_TRILOC_FAR_DIST, 0.25f);
			bank.AddSlider("LOD2: Far Alpha Far Distance:",		&gbPlantsLOD2Alpha1FarDist,	0.0f, PNG_PLANT_TRILOC_FAR_DIST, 0.25f);
		bank.PopGroup();

		bank.PushGroup("Per-vertex Local Variation Data", false);	
			bank.AddToggle("Show per-vertex Ground Colors",	&gbShowpngPlantMgrGroundColor,0xFF);
			bank.AddColor( "Default Ground Color",			&gbDefaultGroundColor);
			bank.AddToggle("Force per-vertex Ground Colors",&gbForceGroundColor,		0xFF);
			bank.AddColor( "Ground Color at vertex#0",		&gbGroundColorV1);
			bank.AddColor( "Ground Color at vertex#1",		&gbGroundColorV2);
			bank.AddColor( "Ground Color at vertex#2",		&gbGroundColorV3);
			bank.AddSlider("Ground Scale at vertex#0",		&gbGroundScaleV1, MIN_PV_SCALE, MAX_PV_SCALE, 0.1f);
			bank.AddSlider("Ground Scale at vertex#1",		&gbGroundScaleV2, MIN_PV_SCALE, MAX_PV_SCALE, 0.1f);
			bank.AddSlider("Ground Scale at vertex#2",		&gbGroundScaleV3, MIN_PV_SCALE, MAX_PV_SCALE, 0.1f);
		bank.PopGroup();
		
		bank.AddToggle("Geometry frustum culling",		&gbPlantsGeometryCulling,	0xFF);
		bank.AddToggle("Double-sided culling",			&gbDoubleSidedCulling,		0xFF);
		bank.AddButton("Reload \"procedural.dat\" file",ReloadDataCB,				"Reloads \"procedural.dat\" and resets CPlantMgr.");
	bank.PopGroup();

	return true;
}
#endif // __BANK

// loads surface properties, initializes internal pngPlantMgr lists:
bool pngPlantMgr::ReloadConfig()
{
	for(int i=0; i<PNG_PLANT_NUM_PLANT_SLOTS; i++)
	{
		m_CloseLocTriListHead[i]	= 0;
	}
	
	m_UnusedLocTriListHead	= 1;

	for(int i=0; i<PNG_PLANT_MAX_CACHE_LOC_TRIS_NUM; i++)
	{
		pngPlantLocTri *pLocTri = &m_LocTrisTab[i];

		pLocTri->m_V1			= Vector3(0, 0, 0);
		pLocTri->m_V2			= Vector3(0, 0, 0);
		pLocTri->m_V3			= Vector3(0, 0, 0);
		pLocTri->m_Center		= Vector3(0, 0, 0);

		pLocTri->m_nSurfaceType			= 0;

		pLocTri->m_triArea = 0.0f;

		if(i==0)
			m_LocTrisTab[i].m_PrevTri = 0;
		else
			m_LocTrisTab[i].m_PrevTri = (u16)i;
		
		if(i==PNG_PLANT_MAX_CACHE_LOC_TRIS_NUM-1)
			m_LocTrisTab[i].m_NextTri = 0;
		else
			m_LocTrisTab[i].m_NextTri = (u16)(i+2);
	}

	m_CloseColEntListHead	= 0;
	m_UnusedColEntListHead	= 1;

	for(int i=0; i<PNG_PLANT_COL_ENTITY_CACHE_SIZE; i++)
	{
		pngPlantColBoundEntry *pEntry = &m_ColEntCacheTab[i];

		pEntry->m_pPhysicsInst = NULL;
		pEntry->m_LocTriArray = NULL;
		pEntry->m_nNumTris = 0;
		pEntry->m_BoundMatProps.Kill();

		if(i==0)
			m_ColEntCacheTab[i].m_PrevEntry = 0;
		else
			m_ColEntCacheTab[i].m_PrevEntry = (u16)i;
		
		if(i==PNG_PLANT_COL_ENTITY_CACHE_SIZE-1)
			m_ColEntCacheTab[i].m_NextEntry = 0;
		else
			m_ColEntCacheTab[i].m_NextEntry = (u16)(i+2);
	}

	return true;
}

void pngPlantMgr::ShutdownLevel()
{
	// clear whole BoundsCache:
	{
		int entry = gPlantMgr.m_CloseColEntListHead;
		while(entry)
		{
			pngPlantColBoundEntry *pEntry = &gPlantMgr.m_ColEntCacheTab[entry-1];
			entry = pEntry->m_NextEntry;
			pEntry->ReleaseEntry();
		}
	}
}

void pngPlantMgr::Shutdown()
{
	gPlantMgr.ShutdownInternal();
}

void pngPlantMgr::ShutdownInternal()
{
	pngPlantMgr::ShutdownLevel();
#if PNG_PLANTMGR_ENABLED
	pngGrassRenderer::Shutdown();

	// destroy allocated textures:
#define DESTROY_TEXTURE(TEXTURE)		{ if(TEXTURE) {TEXTURE->Release(); TEXTURE=NULL;} }
	for(int i=0; i<PNG_PLANT_SLOT_NUM_TEXTURES; i++)
	{
		DESTROY_TEXTURE(m_PlantTextureTab0[i]);
		DESTROY_TEXTURE(m_PlantTextureTab1[i]);
		DESTROY_TEXTURE(m_PlantTextureTab2[i]);
		DESTROY_TEXTURE(m_PlantTextureTab3[i]);
	}
#undef DESTROY_TEXTURE

#if PNG_PLANTSMGR_USE_SPU_RENDERING
	UnloadPlantModel(m_PlantModelsTab0);
	UnloadPlantModel(m_PlantModelsTab1);
	UnloadPlantModel(m_PlantModelsTab2);
	UnloadPlantModel(m_PlantModelsTab3);
#else // PNG_PLANTSMGR_USE_SPU_RENDERING
	UnloadPlantModel(&m_PlantModelsTab0[0]);
	UnloadPlantModel(&m_PlantModelsTab0[1]);
	UnloadPlantModel(&m_PlantModelsTab0[2]);
	UnloadPlantModel(&m_PlantModelsTab0[3]);
	UnloadPlantModel(&m_PlantModelsTab1[0]);
	UnloadPlantModel(&m_PlantModelsTab1[1]);
	UnloadPlantModel(&m_PlantModelsTab1[2]);
	UnloadPlantModel(&m_PlantModelsTab1[3]);
	UnloadPlantModel(&m_PlantModelsTab2[0]);
	UnloadPlantModel(&m_PlantModelsTab2[1]);
	UnloadPlantModel(&m_PlantModelsTab2[2]);
	UnloadPlantModel(&m_PlantModelsTab2[3]);
	UnloadPlantModel(&m_PlantModelsTab3[0]);
	UnloadPlantModel(&m_PlantModelsTab3[1]);
	UnloadPlantModel(&m_PlantModelsTab3[2]);
	UnloadPlantModel(&m_PlantModelsTab3[3]);
#endif // PNG_PLANTSMGR_USE_SPU_RENDERING
#endif // PNG_PLANTMGR_ENABLED
}

// name:		pngPlantMgr::UpdateTask
// description:	Task to do most of the plant mgr update
void pngPlantMgr::UpdateTask()
{
	// update LocTris in CEntities in _ColEntityCache:
	static u8 nLocTriSkipCounter=0;

	const int iTriProcessSkipMask = (nLocTriSkipCounter++)&(PNG_PLANT_ENTRY_TRILOC_PROCESS_UPDATE-1);
	UpdateAllLocTris(m_CameraPos, iTriProcessSkipMask);

	UpdateRenderBuffer();
}

void pngPlantMgr::UpdateRenderBuffer()
{
	// RT stuff: Render() is called from RT and has to have all necessary data double-buffered:
	const u32 bufferID = GetUpdateRTBufferID();

	u16* dstListHeadTab					= &m_CloseLocTriListRenderHead[bufferID][0];
	pngPlantLocTri* dstLocTri				= &m_LocTrisRenderTab[bufferID][0];
	u32 dstTriIndex=0;

	// copy current pLocTris to cache render buffer:
	for(int slotID=0; slotID<PNG_PLANT_NUM_PLANT_SLOTS; slotID++)
	{
		dstListHeadTab[slotID] = 0;
		u16 pSrcLocTri = m_CloseLocTriListHead[slotID];

		while(pSrcLocTri)
		{
			if(m_LocTrisTab[pSrcLocTri-1].m_bCreatesPlants)	// store only if it creates plants
			{
				Assert(dstTriIndex < PNG_PLANT_MAX_CACHE_LOC_TRIS_NUM);
				pngPlantLocTri* pDstLocTri = &dstLocTri[dstTriIndex];

				sysMemCpy(pDstLocTri, &m_LocTrisTab[pSrcLocTri-1], sizeof(pngPlantLocTri));

				// fixup next pointer:
				pDstLocTri->m_NextTri = dstListHeadTab[slotID];	// Render() uses only next pointer
				pDstLocTri->m_PrevTri = 0;
				dstListHeadTab[slotID] = (u16)(dstTriIndex+1);
				dstTriIndex++;
			}

			pSrcLocTri = m_LocTrisTab[pSrcLocTri-1].m_NextTri;
		}
	}

	SwapRTBuffer();	// swap buffer for 360 task
}

#if !SPU_PLANTMGR_UPDATE // TJ
// static void UpdatePlantMgrTask(const CTaskParams& c)
// {
// 	PIXBegin(1, "PlantsMgr");
// 		gPlantMgr.UpdateTask();
// 	PIXEnd();
// }
#endif // !SPU_PLANTMGR_UPDATE 

#if SPU_PLANTMGR_UPDATE
DECLARE_TASK_INTERFACE(PlantsMgrUpdateSPU);
static sysTaskHandle					s_PlantMgrTaskHandle = 0;
//static ProcObjectCreationInfo			s_SpuProcObjectCreationInfo[512*3];
//static ProcTriRemovalInfo				s_SpuProcTriRemovalInfo[512] ;
static CPlantsMgrUpdateSPU::CResultSize	s_PlantMgrResultSize;
#endif // SPU_PLANTMGR_UPDATE

bool pngPlantMgr::UpdateBegin(const Vector3& cameraPos, const Vector3& vecPlayerPos, const Matrix34& mat, const Vector3& bboxmin, const Vector3& bboxmax, float groundZ)
{
#if !__FINAL
	if(!gbPlantMgrActive)	// skip updating, if varconsole switch is off
		return false;
#endif // !__FINAL

	pngPlantMgr::AdvanceCurrentScanCode();
	pngGrassRenderer::SetCurrentScanCode(pngPlantMgr::GetCurrentScanCode());

	m_CameraPos = cameraPos;
	pngGrassRenderer::SetGlobalCameraPos(cameraPos);

#if 0 // TJ
	// get current player pos:
	Vector3 vecPlayerPos;

	CPlayerPed* pPlayer = CGameWorld::FindLocalPlayer();
	Assert(pPlayer);
	CVehicle *pVehicle=NULL;

	if (pPlayer->m_PedAiFlags.bInVehicle)
		pVehicle = pPlayer->GetMyVehicle();

	if (pVehicle)
	{
		vecPlayerPos = pVehicle->GetPosition();
		
		// grab unmodified (e.g. by opened cardoors, etc.) vehicle's bbox:
		const int MI = pVehicle->GetModelIndex();
		const Vector3 bboxmin = CModelInfo::GetModelInfo(MI)->GetBoundingBoxMin(); //pVehicle->GetBoundingBoxMin();
		const Vector3 bboxmax = CModelInfo::GetModelInfo(MI)->GetBoundingBoxMax(); //pVehicle->GetBoundingBoxMax();
//		Printf("/n cars bboxmin: %.2f, %.2f, %.2f", bboxmin.x, bboxmin.y, bboxmin.z);
//		Printf("/n cars bboxmax: %.2f, %.2f, %.2f", bboxmax.x, bboxmax.y, bboxmax.z);
		
//		Vector3 bboxminW = pVehicle->GetMatrix() * bboxmin;
//		Vector3 bboxmaxW = pVehicle->GetMatrix() * bboxmax;
//		Printf("/n cars bboxminW: %.2f, %.2f, %.2f", bboxminW.x, bboxminW.y, bboxminW.z);
//		Printf("/n cars bboxmaxW: %.2f, %.2f, %.2f", bboxmaxW.x, bboxmaxW.y, bboxmaxW.z);
	
//		example:
//		cars bboxmin0:   -1.07,   -2.99,  -0.83
//		cars bboxmax0:    1.07,    2.53,   0.76
//		cars bboxmin: -1232.78, -970.34, 398.32
//		cars bboxmax: -1230.58, -964.85, 399.89
#endif // 0

		// two points: on the front and back of the car:
		float middleX = bboxmin.x + (bboxmax.x - bboxmin.x)*0.5f;
		float middleZ = bboxmin.z + (bboxmax.z - bboxmin.z)*0.5f;

		static TweakFloat radiusPerc = 1.05f;	// how much make bounding shape "shorter" to fit better around vehicle

		float   Radius = (bboxmax.x - bboxmin.x)*0.5f;	// radius
		Vector3 ptb(middleX, bboxmax.y - Radius*radiusPerc, middleZ);
		Vector3 pta(middleX, bboxmin.y + Radius*radiusPerc, middleZ);

		// first point on segment:
// 		Vector3 ptB = pVehicle->GetMatrix() * ptb;
// 		// last point on segment:
// 		Vector3 ptA = pVehicle->GetMatrix() * pta;
		Vector3 ptB = Dot(ptb, mat);
		// last point on segment:
		Vector3 ptA = Dot(pta, mat);
		// segment vector:
		Vector3 vecM = ptA - ptB;

//		Printf("/n ptB:  %.2f, %.2f, %2.f", ptB.x, ptB.y, ptB.z);
//		Printf("/n ptA:  %.2f, %.2f, %2.f", ptA.x, ptA.y, ptA.z);
//		Printf("/n vecM: %.2f, %.2f, %2.f; radius=%.2f", vecM.x, vecM.y, vecM.z, Radius);

		// detect groundZ pos (from wheel hit pos):	
// 		Vector3 wheelHitPos(0,0,0);
// 		const int numWheels = pVehicle->GetNumWheels();
// 		for(int i=0; i<numWheels; i++)
// 		{
// 			if(pVehicle->GetWheel(i))
// 			{
// 				wheelHitPos = pVehicle->GetWheel(i)->GetHitPos();
// 				break; // first detected wheel is fine
// 			}
// 		}
		
//		static TweakBool bOverrideZ = false;
// 		static float overrideZ = 398.44f; // test for testbed
// 		float groundZ = bOverrideZ? overrideZ : wheelHitPos.z;

		static TweakFloat tweakRadiusScale = 1.0f;		// v1 collision
//		static TweakFloat tweakRadiusScale = 1.25f;	// v2 collision
		pngGrassRenderer::SetGlobalVehCollisionParams(true, ptB, vecM, Radius*tweakRadiusScale, groundZ);
// 	}
// 	else
// 	{
// 		pngGrassRenderer::SetGlobalVehCollisionParams(false, Vector3(0,0,0), Vector3(0,0,0), 0, 0);
// 		vecPlayerPos = pPlayer->GetPosition();
// 	}
	pngGrassRenderer::SetGlobalPlayerPos(vecPlayerPos);

	const float bending = pngPlantMgr::CalculateWindBending();
	pngGrassRenderer::SetGlobalWindBending(bending);

	//
	// update ColEntityCache:
	//
	static u8 nUpdateEntCache = 0;

	//
	// do not update this stuff too often:
	//
	if( !((++nUpdateEntCache)&(PNG_PLANT_COL_ENTITY_UPDATE_CACHE-1)) )
	{
		// full update of entry cache (FULL update):
		_ColBoundCache_Update(m_CameraPos);
	}
	else
	{
		// quick update:
		_ColBoundCache_Update(m_CameraPos, true);
	}

#if SPU_PLANTMGR_UPDATE
	// launch SPU task:
	sysTaskContext c(TASK_INTERFACE(PlantsMgrUpdateSPU), 0, 0, 96 * 1024);
	c.SetInputOutput();
	c.AddInput(this, sizeof(pngPlantMgrBase));
	c.AddInput(&g_procInfo, sizeof(g_procInfo));
	CPlantsMgrUpdateSPU& i = *c.AllocUserDataAs<CPlantsMgrUpdateSPU>();
	i.m_pPlantsMgr = this;
	i.m_camPos = cameraPos;
	i.m_defaultGroundColor = gbDefaultGroundColor;
	//i.m_addBufSize = 256;
	//c.AddOutput(i.m_addBufSize * sizeof(ProcObjectCreationInfo));
	//i.m_removeBufSize = 256;
	//c.AddOutput(i.m_removeBufSize * sizeof(ProcTriRemovalInfo));
	c.AddOutput(sizeof(CPlantsMgrUpdateSPU::CResultSize));
	//i.m_maxAdd = NELEM(s_SpuProcObjectCreationInfo);
	//i.m_maxRemove = NELEM(s_SpuProcTriRemovalInfo);
	//i.m_pAddList = &s_SpuProcObjectCreationInfo[0];
	//i.m_pRemoveList = &s_SpuProcTriRemovalInfo[0];
	i.m_pResultSize = &s_PlantMgrResultSize;
	i.m_iTriProcessSkipMask = CTimer::GetFrameCount() & (PNG_PLANT_ENTRY_TRILOC_PROCESS_UPDATE-1);
	const u32 bufferID = GetUpdateRTBufferID();
	i.m_LocTrisRenderTab = &m_LocTrisRenderTab[bufferID][0];

	Assertf(s_PlantMgrTaskHandle == 0, "plantmgr update task already running!");
	s_PlantMgrTaskHandle = c.Start(sysTaskManager::SCHEDULER_GRAPHICS_OTHER);

#else
	// add update plantmgr task
#if 0 // TJ
 	CTaskParams params;
 	CTaskScheduler::AddTask(UpdatePlantMgrTask, params);
 	CTaskScheduler::Flush();
#else
	gPlantMgr.UpdateTask();
#endif // 0
#endif // SPU_PLANTMGR_UPDATE

	return true;
}

void pngPlantMgr::UpdateEnd()
{
#if !__FINAL
	if(!gbPlantMgrActive)	// skip updating, if varconsole switch is off
		return;
#endif // !__FINAL

// 	if (NetworkInterface::IsNetworkOpen())
//         return;

#if SPU_PLANTMGR_UPDATE	
	sysTaskManager::Wait(s_PlantMgrTaskHandle);
	s_PlantMgrTaskHandle = 0;

	// process SPU generated lists of objects to add/remove
#if 0 // TJ
	const ProcTriRemovalInfo* pRemove		= s_SpuProcTriRemovalInfo;
	const ProcTriRemovalInfo* pRemoveEnd	= pRemove + s_PlantMgrResultSize.m_numRemove;
	for(; pRemove != pRemoveEnd; ++pRemove)
		g_procObjMan.ProcessTriangleRemoved(pRemove->pLocTri, pRemove->procTagId);

	const ProcObjectCreationInfo* pAdd		= s_SpuProcObjectCreationInfo;
	const ProcObjectCreationInfo* pAddEnd	= pAdd + s_PlantMgrResultSize.m_numAdd;
	for(; pAdd != pAddEnd; ++pAdd)
	{		
		CProcObjInfo* pProcObjInfo = (CProcObjInfo*)(pAdd->pos.uw + (u8*)&g_procInfo.m_procObjInfos[0]);
		pngPlantLocTri* pLocTri = (pngPlantLocTri*)pAdd->normal.uw;
		//if (pLocTri->m_pParentEntity)
		{
			CEntityItem* pEntityItem = g_procObjMan.AddObject(pAdd->pos, pAdd->normal, 
				&pProcObjInfo->m_data, &pProcObjInfo->m_entityList, /*const_cast<CEntity*>(pLocTri->m_pParentEntity)*/);
			if (pEntityItem)
			{
				pEntityItem->m_pParentTri = pLocTri;
			#if __BANK
				pEntityItem->triVert1[0] = pLocTri->m_V1.x;
				pEntityItem->triVert1[1] = pLocTri->m_V1.y;
				pEntityItem->triVert1[2] = pLocTri->m_V1.z;

				pEntityItem->triVert2[0] = pLocTri->m_V2.x;
				pEntityItem->triVert2[1] = pLocTri->m_V2.y;
				pEntityItem->triVert2[2] = pLocTri->m_V2.z;

				pEntityItem->triVert3[0] = pLocTri->m_V3.x;
				pEntityItem->triVert3[1] = pLocTri->m_V3.y;
				pEntityItem->triVert3[2] = pLocTri->m_V3.z;
			#endif // __BANK
			}
		}
	}	
#endif // 0

	const u32 bufferID = GetUpdateRTBufferID();
	sysMemCpy(&m_CloseLocTriListRenderHead[bufferID][0], &m_CloseLocTriListHead[0], PNG_PLANT_NUM_PLANT_SLOTS * sizeof(u16));
	SwapRTBuffer();	// swap buffer for SPU task

#if __DEV && 0
	static u32 maxAdd = 0;
	if (s_PlantMgrResultSize.m_numAdd > maxAdd)
	{
		maxAdd = s_PlantMgrResultSize.m_numAdd;
		Displayf("SPU Plant Mgr most added = %i", maxAdd);
	}
	static u32 maxRemove = 0;
	if (s_PlantMgrResultSize.m_numRemove > maxRemove)
	{
		maxRemove = s_PlantMgrResultSize.m_numRemove;
		Displayf("SPU Plant Mgr most removed = %i", maxRemove);
	}
#endif // __DEV
#endif // SPU_PLANTMGR_UPDATE
}

// this function helps to avoid ugly visual "rebuilding" of plants while
// camera is teleported into new remote location;
// to be called ONCE before teleporting camera / player to new far position:
bool pngPlantMgr::PreUpdateOnceForNewCameraPos(const Vector3& newCameraPos)
{
	return gPlantMgr.PreUpdateOnceForNewCameraPosInternal(newCameraPos);
}

bool pngPlantMgr::PreUpdateOnceForNewCameraPosInternal(const Vector3& newCameraPos)
{
// 	g_procObjMan.ShutdownLevel(false);
// 	g_procObjMan.InitLevel();

	pngPlantMgr::AdvanceCurrentScanCode();
	pngGrassRenderer::SetCurrentScanCode(pngPlantMgr::GetCurrentScanCode());
	
	pngGrassRenderer::SetGlobalCameraPos(newCameraPos);

	float bending = pngPlantMgr::CalculateWindBending();
	pngGrassRenderer::SetGlobalWindBending(bending);


	//
	// empty + update ColEntityCache:
	//
	// force updating all EntityCache:
	_ColBoundCache_Update(newCameraPos);
	_ColBoundCache_Update(newCameraPos);
	
	//
	// update ALL LocTris in CEntities in _ColEntityCache:
	//
	UpdateAllLocTris(newCameraPos, PNG_PLANT_ENTRY_TRILOC_PROCESS_ALWAYS);
	UpdateAllLocTris(newCameraPos, PNG_PLANT_ENTRY_TRILOC_PROCESS_ALWAYS);

	return true;
}

bool pngPlantMgr::_ColBoundCache_Update(const Vector3& UNUSED_PARAM(cameraPos), bool bQuickUpdate)
{
	//const u16 nCurrentScanCode = pngPlantMgr::GetCurrentScanCode();

	if(bQuickUpdate)
	{
		// Quick Update:
		if(m_CloseColEntListHead)
		{
			u16 entry = m_CloseColEntListHead;
			while(entry)
			{
				pngPlantColBoundEntry *pEntry = &m_ColEntCacheTab[entry-1];
				entry = pEntry->m_NextEntry;
				// not valid pointer to pEntity?
				if(!pEntry || !pEntry->GetPhysInst())
				{
					pEntry->ReleaseEntry();
				}				
			}
		}
	}
	else // if(bQuickUpdate)...
	{
#if 0 // TJ
		// Full update: 
		phLevelNew * phLevel = CPhysics::GetLevel();
		Assert(phLevel);

		phIterator iterator;
		// Use this option to get all objects matching the req'd state
		iterator.InitCull_Sphere(m_CameraPos, PNG_PLANTMGR_COL_TEST_RADIUS);			// ::InitCull_All();
		iterator.SetStateIncludeFlags(phLevelNew::STATE_FLAG_FIXED);// | phLevelNew::STATE_FLAG_ACTIVE | phLevelNew::STATE_FLAG_INACTIVE);

		u16 iIndexInLevel = phLevel->GetFirstCulledObject(iterator);
		while(iIndexInLevel != phInst::INVALID_INDEX)
		{
			phInst *pInstance = phLevel->GetInstance(iIndexInLevel);
			if(pInstance)
			{
				phArchetype *pArchetype = pInstance->GetArchetype();
				Assert(pArchetype);

				phBound *pBound = pArchetype->GetBound();
				Assert(pBound);

				_ColBoundCache_ProcessBound(pInstance, pBound, nCurrentScanCode);
				
			}

			iIndexInLevel = phLevel->GetNextCulledObject(iterator);
		}

		// go through BoundCache and remove Entrys with no current scancode
		//  (probably they are too far from iterator):
		if(m_CloseColEntListHead)
		{
			int entry = m_CloseColEntListHead;
			while(entry)
			{
				pngPlantColBoundEntry *pEntry = &m_ColEntCacheTab[entry-1];
				entry = pEntry->m_NextEntry;

				if(	(pEntry->m_nScancode != nCurrentScanCode)					||
					// not valid pointer to pEntity?
					(!pEntry->m_pEntity) || (!pEntry->m_pEntity->GetPhysInst())	)
				{
					pEntry->ReleaseEntry();
				}
			}
		}
#endif // 0
	}


	return true;

}

bool pngPlantMgr::_ColBoundCache_ProcessBound(phInst *pInst, phBound *pBound, const u16 nCurrentScanCode)
{
	// composite bounds not supported:
//	Assertf(pBound->GetType() != phBound::COMPOSITE, "PlantsMgr: Composite bounds not supported!"); 
	if(pBound->GetType() == phBound::BVH || pBound->GetType() == phBound::GEOMETRY)
	{
		phBoundGeometry *pBoundGeom = (phBoundGeometry*)pBound;
		if(pngPlantMgr::IsBoundGeomPlantFriendly(pBoundGeom))
		{
			//CEntity* pEntity = CPhysics::GetEntityFromInst(pInst);
			//Assert(pEntity);
			pngPlantColBoundEntry *pEntry = _ColBoundCache_FindInCache(pInst/*pEntity*/);
			if(!pEntry)
			{
				pEntry = _ColBoundCache_Add(pInst);
			}

			if(pEntry)
			{
				pEntry->m_nScancode = nCurrentScanCode;		// update scanecode to indicate this bound is colliding with iterator
			}
		}
	}

	return true;
}

// checks if given Bound is already in BoundCache:
pngPlantColBoundEntry*	pngPlantMgr::_ColBoundCache_FindInCache(phInst* pPhysicsInst)
{
	if(m_CloseColEntListHead)
	{
		int entry = m_CloseColEntListHead;
		while(entry)
		{
			pngPlantColBoundEntry *pEntry = &m_ColEntCacheTab[entry-1];

			if(pEntry->m_pPhysicsInst == pPhysicsInst)
			{
				return(pEntry);
			}

			entry = pEntry->m_NextEntry;
		}
	}
		
	return(NULL);
}

pngPlantColBoundEntry* pngPlantMgr::_ColBoundCache_Add(phInst* pPhysicsInst, bool bCheckCacheFirst)
{
	pngPlantColBoundEntry *pEntry=NULL;
	
	if(bCheckCacheFirst)
	{
		pEntry = _ColBoundCache_FindInCache(pPhysicsInst);
		if(pEntry)
		{
			// seems that this CEntity if already in the cache:
			return(pEntry);
		}
	}

	if(m_UnusedColEntListHead)
	{
		pEntry = &m_ColEntCacheTab[m_UnusedColEntListHead-1];
		if(pEntry->AddEntry(pPhysicsInst))
		{
			return(pEntry);
		}
	}

	return(NULL);
}

void pngPlantMgr::_ColBoundCache_Remove(phInst *pPhysicsInst)
{
	pngPlantColBoundEntry *pEntry = _ColBoundCache_FindInCache(pPhysicsInst);
	if(pEntry)
	{
		pEntry->ReleaseEntry();
	}
}

bool pngPlantMgr::IsBoundGeomPlantFriendly(phBoundGeometry *pBound)
{
	Assert(pBound);
	
	const int numMaterials = pBound->GetNumMaterials();
	if(numMaterials <= 0)
	{
		return false;
	}

#if 0 // TJ
	// check all materials in given bound if they're "plant friendly":
	for(int i=0; i<numMaterials; i++)
	{
		int procTagId = PGTAMATERIALMGR->UnpackProcId(pBound->GetMaterialId(i));
		if (procTagId>0)
		{
			if (g_procInfo.CreatesPlants(procTagId) || g_procInfo.CreatesProcObjects(procTagId))
			{
				return true;
			}
		}
	}

	return false;
#else
	return true;
#endif // 0
}

// goes through active LocTris list, tries to reject
// some located far away and generate new LocTris from ColModel's triangles:
bool pngPlantMgr::UpdateAllLocTris(const Vector3& camPos, int iTriProcessSkipMask)
{
	//g_procObjMan.LockListAccess(); // TJ

	int entry = m_CloseColEntListHead;
	while(entry)
	{
		pngPlantColBoundEntry *pEntry = &m_ColEntCacheTab[entry-1];
		_ProcessEntryCollisionData(pEntry, camPos, iTriProcessSkipMask);
		entry = pEntry->m_NextEntry;
	}

	//g_procObjMan.UnlockListAccess(); // TJ

	return true;
}

// Quads support:
// - each pEntry has 2 times bigger pLocTriArray (to allow space for poly quads (1 quad=2 tris));
// - pLocTriArray[] looks like follows:
//
//		[0] Poly0 tri0
//		[1] Poly0 tri1 (if exists = poly was a quad)
//		[2] Poly1 tri0
//		[3]	Poly1 tri1 (if exists = poly was a quad)
//		[4] Poly2 tri0
//		[5] Poly2 tri1 (if exists = poly was a quad)
//		etc. etc. etc.
//
//	- if poly is triangle, then only 1st cell is occupied
//	- if poly ia quad, then both cells are occupied
bool pngPlantMgr::_ProcessEntryCollisionData(pngPlantColBoundEntry *pEntry, const Vector3& camPos, int iTriProcessSkipMask)
{
	phBoundGeometry *pBound	= pEntry->GetBound();
	Assert(pBound);
	if(!pBound)
		return false;

	Assertf(pEntry->m_nNumTris > 0, "Not valid number of triangles in entry!");

	//
	// sometimes collision data is not streamed in (???):
	//
	//Assertf(pColModel->m_nNoOfTriangles == pEntry->m_nNumTris, "Different number of ColTris in CEntity & cached entry!");
	if(pBound->GetNumPolygons() != (pEntry->m_nNumTris/2))
	{
		//DEBUGLOG("/n [*]_ProcessEntryCollisionData(): entry was skipped because no collision data was found!/n"); 
		return false;
	}

	const int count = pEntry->m_nNumTris;

	const int count_1_8	= count / PNG_PLANT_ENTRY_TRILOC_PROCESS_UPDATE;	// 1/8 of everything to process
	int startCount0	= 0;
	int stopCount0	= 0;
	if(count_1_8 && (iTriProcessSkipMask!=PNG_PLANT_ENTRY_TRILOC_PROCESS_ALWAYS))
	{
		startCount0	= iTriProcessSkipMask * count_1_8;
		stopCount0	= (iTriProcessSkipMask!=PNG_PLANT_ENTRY_TRILOC_PROCESS_UPDATE-1)?((iTriProcessSkipMask+1)*count_1_8):(count);
	}
	else
	{	// case when count < PNG_PLANT_ENTRY_TRILOC_PROCESS_UPDATE:
		startCount0	= 0;
		stopCount0	= count;
	}
	const int startCount	= startCount0;
	const int stopCount	= stopCount0;
	for(int i=startCount; i<stopCount; i++)
	{

		if(pEntry->m_LocTriArray[i])
		{
			pngPlantLocTri *pLocTri = &m_LocTrisTab[pEntry->m_LocTriArray[i]-1];
		
			// check if this triangle creates objects but hasn't created any yet and re process
			if(pLocTri->m_bCreatesObjects && (!pLocTri->m_bCreatedObjects))
			{
				Assert(pLocTri->m_bCreatesPlants);
//				if (g_procObjMan.ProcessTriangleAdded(pLocTri)) // TJ
				{
					// objects have been created on this triangle ok
					pLocTri->m_bCreatedObjects = true;
				}
			}		
		}
		
		if(pEntry->m_LocTriArray[i])
		{
			//
			// check if LocTri is in range:
			// if not, remove it:
			//
			pngPlantLocTri *pLocTri = &m_LocTrisTab[pEntry->m_LocTriArray[i]-1];
			
#ifdef USE_COLLISION_2D_DIST
			const Vector3 _colDistV1(camPos - pLocTri->m_V1);
			const Vector3 _colDistV2(camPos - pLocTri->m_V2);
			const Vector3 _colDistV3(camPos - pLocTri->m_V3);
			const Vector3 _colDistV4(camPos - pLocTri->m_Center);
			const Vector2 colDistV1(_colDistV1.x, _colDistV1.y);
			const Vector2 colDistV2(_colDistV2.x, _colDistV2.y);
			const Vector2 colDistV3(_colDistV3.x, _colDistV3.y);
			const Vector2 colDistV4(_colDistV4.x, _colDistV4.y);
#else
			const Vector3 colDistV1(camPos - pLocTri->m_V1);
			const Vector3 colDistV2(camPos - pLocTri->m_V2);
			const Vector3 colDistV3(camPos - pLocTri->m_V3);
			const Vector3 colDistV4(camPos - pLocTri->m_Center);
#endif // USE_COLLISION_2D_DIST

			const float colDistSqr1 = colDistV1.Mag2();
			const float colDistSqr2 = colDistV2.Mag2();
			const float colDistSqr3 = colDistV3.Mag2();
			const float colDistSqr4 = colDistV4.Mag2();

			// calc middle points distances (distance between camera and tri edges' middle points):
#ifdef USE_COLLISION_2D_DIST
			const Vector3 _colDistMV12( (_colDistV1+_colDistV2) * 0.5f );	// V1-2
			const Vector3 _colDistMV23( (_colDistV2+_colDistV3) * 0.5f );	// V2-3
			const Vector3 _colDistMV31( (_colDistV3+_colDistV1) * 0.5f );	// V3-1
			const Vector2 colDistMV12(_colDistMV12.x, _colDistMV12.y);
			const Vector2 colDistMV23(_colDistMV23.x, _colDistMV23.y);
			const Vector2 colDistMV31(_colDistMV31.x, _colDistMV31.y);
#else
			const Vector3 colDistMV12( (colDistV1+colDistV2) * 0.5f );	// V1-2
			const Vector3 colDistMV23( (colDistV2+colDistV3) * 0.5f );	// V2-3
			const Vector3 colDistMV31( (colDistV3+colDistV1) * 0.5f );	// V3-1
#endif // USE_COLLISION_2D_DIST

			const float colDistSqr5 = colDistMV12.Mag2();
			const float colDistSqr6 = colDistMV23.Mag2();
			const float colDistSqr7 = colDistMV31.Mag2();

			if( (colDistSqr1 >= PNG_PLANT_TRILOC_FAR_DIST_SQR) &&
				(colDistSqr2 >= PNG_PLANT_TRILOC_FAR_DIST_SQR) &&
				(colDistSqr3 >= PNG_PLANT_TRILOC_FAR_DIST_SQR) &&
				(colDistSqr4 >= PNG_PLANT_TRILOC_FAR_DIST_SQR) &&
				
				(colDistSqr5 >= PNG_PLANT_TRILOC_FAR_DIST_SQR) &&
				(colDistSqr6 >= PNG_PLANT_TRILOC_FAR_DIST_SQR) &&
				(colDistSqr7 >= PNG_PLANT_TRILOC_FAR_DIST_SQR)	)
			{
				pLocTri->Release();
				pEntry->m_LocTriArray[i] = 0;
			}
		}
		else
		{
			if(!(i&0x01))							// process only even entries ("1st cells")
			if(m_UnusedLocTriListHead)				// is there any space in trilist?
			if(m_LocTrisTab[m_UnusedLocTriListHead-1].m_NextTri)	// is there space for 2 tris (quad)?
			{				
				
				Assert((i&0x01) == 0);	// process only even entries in the array


#if 0			
				// 1. old version (slow cache accesses to poly in material index):
				const phPolygon& poly = pBound->GetPolygon(i>>1);
				const int numPolyPoints = poly.GetNumVerts();
				Assert(numPolyPoints <= PNG_PLANT_MAX_POLY_POINTS);
				// check surface type of polygon:
				const int	boundMaterialID	= poly.GetMaterialIndex();
				const u32	actualMaterialID = pBound->GetMaterialId(boundMaterialID);

				int procTagId = PGTAMATERIALMGR->UnpackProcId(actualMaterialID);
				const bool bCreatesPlants  = g_procInfo.CreatesPlants(procTagId);
				const bool bCreatesObjects = g_procInfo.CreatesProcObjects(procTagId);
				if((!bCreatesPlants) &&	(!bCreatesObjects))
				{
					continue;
				}
#else			
				// 2. new version (cached material props used):
				// use here: atBitSet	*m_BoundMatProps;	// bitfield to store bCreatesPlants|bCreatesObjects material flags
				const bool bCreatesPlants  = pEntry->m_BoundMatProps.IsSet((i>>1)*2+0);
				const bool bCreatesObjects = pEntry->m_BoundMatProps.IsSet((i>>1)*2+1);

				phMaterialMgr::Id actualMaterialID = 0;
				const phPolygon *pPoly = NULL;
				if(bCreatesPlants || bCreatesObjects)
				{
					pPoly = &pBound->GetPolygon(i>>1);
					const int boundMaterialID	= pBound->GetPolygonMaterialIndex(i>>1);
					actualMaterialID = pBound->GetMaterialId(boundMaterialID);
				}
				else
				{
					continue;
				}

				const phPolygon& poly = *pPoly;
				const int numPolyPoints = poly.GetNumVerts();
				Assert(numPolyPoints <= PNG_PLANT_MAX_POLY_POINTS);
#endif // 0


				if(numPolyPoints == 3)
				{	// process tri (1 triangle only):
					_ProcessEntryCollisionData_GenerateNewLocTri(i, pEntry, pBound, camPos, poly, 0, 1, 2,
							bCreatesPlants, bCreatesObjects, actualMaterialID);
					Assert(pEntry->m_LocTriArray[i+1]==0); // this cell should always be empty
				}
				else if(numPolyPoints == 4)
				{	// process quad (2 triangles):
					// 1. tri0 (even):
					_ProcessEntryCollisionData_GenerateNewLocTri(i, pEntry, pBound, camPos, poly, 0, 1, 2,
							bCreatesPlants, bCreatesObjects, actualMaterialID);
					// 2. tri1 (odd) - make sure it's actually not already created:
					if(pEntry->m_LocTriArray[i+1]==0)
					{
						_ProcessEntryCollisionData_GenerateNewLocTri(i+1, pEntry, pBound, camPos, poly, 0, 2, 3,
							bCreatesPlants, bCreatesObjects, actualMaterialID);
					}
				}
			}
		}
	}

	return true;
}

void pngPlantMgr::_ProcessEntryCollisionData_GenerateNewLocTri(int indexLTA, 
					pngPlantColBoundEntry *pEntry, phBoundGeometry *pBound, const Vector3& camPos,
					const phPolygon& poly, const int polyIndex0, const int polyIndex1, const int polyIndex2,
					const bool bCreatesPlants, const bool bCreatesObjects, phMaterialMgr::Id actualMaterialID)
{
	// generate new LocTri for this polygon (if necessary):
	Vector3 tv[3];
	const phPolygon::Index vIndex0 = poly.GetVertexIndex(/*0*/polyIndex0);
	const phPolygon::Index vIndex1 = poly.GetVertexIndex(/*1*/polyIndex1);
	const phPolygon::Index vIndex2 = poly.GetVertexIndex(/*2*/polyIndex2);
	tv[0] = pBound->GetVertex(vIndex0);
	tv[1] = pBound->GetVertex(vIndex1);
	tv[2] = pBound->GetVertex(vIndex2);

	// transform points if required
	if(pEntry->GetBoundMatrix())
	{
		if(!pEntry->m_bBoundMatIdentity)
		{
			pEntry->m_BoundMat.Transform(tv[0]);
			pEntry->m_BoundMat.Transform(tv[1]);
			pEntry->m_BoundMat.Transform(tv[2]);
		}
	}
	Vector3 colCenter((tv[0]+tv[1]+tv[2]) / 3.0f);

#ifdef USE_COLLISION_2D_DIST
	Vector3 _colDistV1 = camPos - tv[0];
	Vector3 _colDistV2 = camPos - tv[1];
	Vector3 _colDistV3 = camPos - tv[2];
	Vector3 _colDistV4 = camPos - colCenter;
	Vector2 colDistV1(_colDistV1.x, _colDistV1.y);
	Vector2 colDistV2(_colDistV2.x, _colDistV2.y);
	Vector2 colDistV3(_colDistV3.x, _colDistV3.y);
	Vector2 colDistV4(_colDistV4.x, _colDistV4.y);
#else
	Vector3 colDistV1 = camPos - tv[0];
	Vector3 colDistV2 = camPos - tv[1];
	Vector3 colDistV3 = camPos - tv[2];
	Vector3 colDistV4 = camPos - colCenter;
#endif // USE_COLLISION_2D_DIST
	const float colDistSqr1 = colDistV1.Mag2();
	const float colDistSqr2 = colDistV2.Mag2();
	const float colDistSqr3 = colDistV3.Mag2();
	const float colDistSqr4 = colDistV4.Mag2();
				

	// calc middle points distances (distance between camera and tri edges' middle points):
#ifdef USE_COLLISION_2D_DIST
	const Vector3 _colDistMV12( (_colDistV1+_colDistV2) * 0.5f );	// V1-2
	const Vector3 _colDistMV23( (_colDistV2+_colDistV3) * 0.5f );	// V2-3
	const Vector3 _colDistMV31( (_colDistV3+_colDistV1) * 0.5f );	// V3-1
	const Vector2 colDistMV12(_colDistMV12.x, _colDistMV12.y);
	const Vector2 colDistMV23(_colDistMV23.x, _colDistMV23.y);
	const Vector2 colDistMV31(_colDistMV31.x, _colDistMV31.y);
#else
	const Vector3 colDistMV12( (colDistV1+colDistV2) * 0.5f );	// V1-2
	const Vector3 colDistMV23( (colDistV2+colDistV3) * 0.5f );	// V2-3
	const Vector3 colDistMV31( (colDistV3+colDistV1) * 0.5f );	// V3-1
#endif // USE_COLLISION_2D_DIST
	const float colDistSqr5 = colDistMV12.Mag2();
	const float colDistSqr6 = colDistMV23.Mag2();
	const float colDistSqr7 = colDistMV31.Mag2();


	if( (colDistSqr1 < PNG_PLANT_TRILOC_FAR_DIST_SQR)	||
		(colDistSqr2 < PNG_PLANT_TRILOC_FAR_DIST_SQR)	||
		(colDistSqr3 < PNG_PLANT_TRILOC_FAR_DIST_SQR)	||
		(colDistSqr4 < PNG_PLANT_TRILOC_FAR_DIST_SQR)	||
					
		(colDistSqr5 < PNG_PLANT_TRILOC_FAR_DIST_SQR)	||
		(colDistSqr6 < PNG_PLANT_TRILOC_FAR_DIST_SQR)	||
		(colDistSqr7 < PNG_PLANT_TRILOC_FAR_DIST_SQR)	)
	{

		if(bCreatesPlants || bCreatesObjects)
		{
			// this triangle creates plants or objects - add it to the list
			int loctri = m_UnusedLocTriListHead;
			pngPlantLocTri *pLocTri = &m_LocTrisTab[loctri-1];

			if(pLocTri->Add(tv[0], tv[1], tv[2], actualMaterialID, 255/*pColTri->m_nLighting*/, bCreatesPlants, bCreatesObjects/*, pEntry->m_pEntity*/)) // TJ
			{
				// it's added to the list
				Assert(pEntry->m_LocTriArray[indexLTA]==0);
				pEntry->m_LocTriArray[indexLTA] = (u16)loctri;


				if (pLocTri->m_bCreatesObjects)
				{
					// try to create procedural objects on this triangle 
	//								g_procObjMan.ProcessTriangleAdded(pLocTri); // TJ

					// mark as having procedural objects created (even if it hasn't had) 
					// so the triangle is kept and not thrown away and reprocessed next frame
					pLocTri->m_bCreatedObjects = true;

					// this triangle creates objects - process it to see if any get created 
// TJ
//					if (g_procObjMan.ProcessTriangleAdded(pLocTri))
//					{
//						// objects have been created on this triangle ok
//						pLocTri->m_bCreatedObjects = true;
//					}
//					else if (!pLocTri->m_bCreatesPlants)
//					{
//						// no objects have been created and no plants need created - release the triangle
//						pLocTri->Release();
//						pEntry->m_LocTriArray[indexLTA] = NULL;
//					}
				}
				else
				{
					Assert(pLocTri->m_bCreatesPlants);
				}
			}
		}	
	}
	}

// stolen from CEntity::ModifyMatrixForTreeInWind():
float pngPlantMgr::CalculateWindBending()
{
//	Assert(CSystem::IsThisThreadId(SYS_THREAD_UPDATE)); // TJ

//	extern float WindTable[];	// defined in Entity.cpp

//	static u32 RandomSeed = g_DrawRand.GetInt();	// this is update thread

	float bending = 0.0f;

	if (true) //(g_weather.GetWind() < 0.5f) // Calm weather // TJ
	{
		//if(g_weather.GetWind() < 0.2f) // TJ
		//	bending = 0.005f * Sinf( (CTimer::GetTimeInMilliseconds()&4095) * (6.28f/4096.0f) );
		//else 
			//m_mat.xz = 0.02f * Sinf( ((CTimer::GetTimeInMilliseconds()+((u32)this))&4095) * (6.28f/4096.0f) );
			bending = 0.008f * Sinf( ((u32)TIME.GetElapsedTime()&4095) * (6.28f/4096.0f) );
	}
	else
	{
		// TJ
//		u32	TableEntry65536	= (((u32)TIME.GetElapsedTime()<<3) + RandomSeed)&0x0ffff;	// LC was <<4
//		u32	TableEntry16	= (TableEntry65536>>12)&0x0f;
		
//		float	Inter		= (TableEntry65536 & 4095) / 4096.0f;
//		float	WindValue	= 1.0f + (1.0f-Inter) * WindTable[TableEntry16] + Inter * WindTable[(TableEntry16+1)&15];

		//bending = 0.015f * WindValue * g_weather.GetWind(); // LC was 0.008 // TJ
	}

	// palms are bend pernamently:
	//atX += -0.07f * g_weather.Wind;
	return(bending);
}

#if 0
static inline bool IsLocTriVisibleByCamera(pngPlantLocTri *pLocTri)
{
	// is sphere containing triangle visible:?
	const grcViewport *pViewport = grcViewport::GetCurrent();	// RT version
	//const grcViewport *pViewport = &CRenderPhase::GetCurrent()->GetGrcViewport(); // non-RT version
	return pViewport->IsSphereVisible(pLocTri->m_Center.x,pLocTri->m_Center.y,pLocTri->m_Center.z, pLocTri->m_SphereRadius) != cullOutside;
}
#endif

#if PSN_ALPHATOMASK_PASS
// PSN render pass#1:
// draw only to color0 , depth and stencil mask
static void BeginGrassAlphaToMaskPass1()
{
	// render only to color0+depth/stencil:
#if 0 //__XENON
	grcState::SetState(grcsColorWrite,  grccwRGBA);
	grcState::SetState(grcsColorWrite1, grccwNone);
	grcState::SetState(grcsColorWrite2, grccwNone);
#else
	//CDeferredLightingHelper::UnlockGBufferTargets();
	//CDeferredLightingHelper::LockSingleGBufferTarget(0, true);
#endif

static TweakBool debugEnableAlphaToMask1	= true;
static TweakBool debugUseSolidAlphaToMask1	= false;
	grcState::SetAlphaToMask(debugEnableAlphaToMask1);
	grcState::SetAlphaToMaskOffsets(debugUseSolidAlphaToMask1 ? grcamoSolid : grcamoDithered);
	//CShaderLib::SetGlobalDeferredMaterial(DEFERRED_MATERIAL_GRASS);
}

static void EndGrassAlphaToMaskPass1()
{
	grcState::SetAlphaToMask(false);
	//CShaderLib::SetGlobalDeferredMaterial(DEFERRED_MATERIAL_DEFAULT);

#if 0 //__XENON
	grcState::SetState(grcsColorWrite,  grccwRGBA);
	grcState::SetState(grcsColorWrite1, grccwRGBA);
	grcState::SetState(grcsColorWrite2, grccwRGBA);
#else
	//CDeferredLightingHelper::UnlockSingleGBufferTarget(0);
	//CDeferredLightingHelper::LockGBufferTargets();
#endif
}

// PSN render pass#2:
// fullscreen blit to color1 (faked normals) and color2 (spec params)
static void RenderGrassAlphaToMaskPass2()
{
	grcState::SetState(grcsColorWrite,  grccwNone);
	grcState::SetState(grcsColorWrite1, grccwRGBA);
	grcState::SetState(grcsColorWrite2, grccwRGBA);

	grcState::SetDepthTest(false);
	grcState::SetDepthWrite(false);


	// stencil state:
	grcState::SetStencilEnable(true);
	grcState::SetStencilRef(DEFERRED_MATERIAL_GRASS);
	grcState::SetStencilFunc(grccfEqual);

	grcState::SetStencilPass(grcsoZero);	// reset grass material to default
	grcState::SetStencilFail(grcsoKeep);
	grcState::SetStencilZFail(grcsoKeep);

	grcState::SetStencilMask(0xFFFFFFFF);
	grcState::SetStencilWriteMask(0xFFFFFFFF);

	grmShader *shader = pngGrassRenderer::GetGrassShader();
	Assert(shader);
	grcEffectTechnique tech = pngGrassRenderer::GetFakedGBufTechniqueID();
	Assert(tech);

	shader->TWODBlit(	-1.0f,-1.0f, 1.0f,1.0f, 0.0f,
						0.0f,0.0f, 1.0f,1.0f, Color32(255,255,255,255), tech);

	// cleanup:
	grcState::SetStencilEnable(false);
	grcState::SetStencilRef(DEFERRED_MATERIAL_DEFAULT);
	grcState::SetStencilFunc(grccfAlways);
	grcState::SetDepthTest(true);
	grcState::SetDepthWrite(true);
	grcState::SetState(grcsColorWrite,  grccwRGBA);
	grcState::SetState(grcsColorWrite1, grccwRGBA);
	grcState::SetState(grcsColorWrite2, grccwRGBA);
}
#endif // PSN_ALPHATOMASK_PASS

bool pngPlantMgr::Render()
{
	return gPlantMgr.RenderInternal();
}

bool pngPlantMgr::RenderInternal()
{
//	Assert(CSystem::IsThisThreadId(SYS_THREAD_RENDER));
#if !__FINAL
	if(!gbPlantMgrActive)	// skip updating, if varconsole switch is off
		return false;
#endif

#if __DEV
	pngPlantMgr::RenderDebugStuff();
#endif

#if PNG_PLANTMGR_ENABLED

	PIXBegin(0, "PlantsMgr::Render");
//	PUSH_TIMER("PlantsMgr::Render");
	
	grcState::Default();

	// setup render state:
	grcBindTexture(NULL);
	grcWorldIdentity();

#if __BANK
	const bool sm_bEnableCulling = gbDoubleSidedCulling != 0;
#else
	static TweakBool sm_bEnableCulling = true;
#endif // __BANK
	grcState::SetCullMode(sm_bEnableCulling?grccmBack:grccmNone);
//	grcState::SetCullMode(grccmNone);

	grcState::SetFillMode(grcfmSolid);
	grcState::SetAlphaTest(false);

	grcState::SetDepthTest(true);
	grcState::SetDepthWrite(true);
	grcState::SetAlphaBlend(false);
	grcState::SetBlendSet(grcbsNormal);		// Standard srcAlpha, 1-srcAlpha blend

	//	grcLightingMode oldLightingMode = grcState::GetLightingMode();
	grcState::SetLightingMode(grclmDirectional/*grclmNone*/);

	// set the hdr value
//	const float hdrVal = g_timeCycle.GetCurrRenderColourSet().GetVar(CSVAR_LIGHT_DIR_MULT);
//	CShaderLib::SetGlobalHDRIntensityMult(hdrVal);

#if __XENON
	static TweakBool debugHiZEnable = true;
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_HIZENABLE,		debugHiZEnable?D3DHIZ_AUTOMATIC:D3DHIZ_DISABLE);
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_HIZWRITEENABLE,D3DHIZ_AUTOMATIC);

	static TweakBool debugEnableAlphaToMask1	= true;
	static TweakBool debugUseSolidAlphaToMask1	= false;
	grcState::SetAlphaToMask(debugEnableAlphaToMask1);
	grcState::SetAlphaToMaskOffsets(debugUseSolidAlphaToMask1 ? grcamoSolid : grcamoDithered);
	//CShaderLib::SetDeferredAlphaEnable(true);
#endif // __XENON

#if PSN_ALPHATOMASK_PASS
	BeginGrassAlphaToMaskPass1();
#endif // PSN_ALPHATOMASK_PASS

//	CDeferredLightingHelper::PrepareCutoutPass();

	pngGrassRenderer::SpuInitialisePerRenderFrame();

	const int storedSeed = g_PlantsRendRand.GetSeed();

	//
	// render all close lists:
	//
#if 0 // TJ
	for(int slotID=0; slotID<PNG_PLANT_NUM_PLANT_SLOTS; slotID++)
	{
		const u32 bufferID = GetRenderRTBufferID();
		int loctri = m_CloseLocTriListRenderHead[bufferID][slotID];

#if __DEV
		// rendering stats:
		g_LocTriPlantsSlotID				= slotID;
		g_LocTriPlantsLOD0DrawnPerSlot	= &g_LocTriPlantsLOD0Drawn[slotID];
		g_LocTriPlantsLOD1DrawnPerSlot	= &g_LocTriPlantsLOD1Drawn[slotID];
		g_LocTriPlantsLOD2DrawnPerSlot	= &g_LocTriPlantsLOD2Drawn[slotID];
#endif // __DEV

		#if PNG_PLANTSMGR_USE_SPU_RENDERING
			// do nothing	
		#else
			grcTexture **ppGrassTextureTab = m_PlantSlotTextureTab[slotID];
		#endif
		
		u32 PlantModelSetID = PPPLANTBUF_MODEL_SET0;
		switch(slotID)
		{
			case(0):	PlantModelSetID = PPPLANTBUF_MODEL_SET0;	break;
			case(1):	PlantModelSetID = PPPLANTBUF_MODEL_SET1;	break;
			case(2):	PlantModelSetID = PPPLANTBUF_MODEL_SET2;	break;
			case(3):	PlantModelSetID	= PPPLANTBUF_MODEL_SET3;	break;
		}

		while(loctri)
		{
			const u32 bufferID = GetRenderRTBufferID();
			pngPlantLocTri *pLocTri = &m_LocTrisRenderTab[bufferID][loctri-1];

			// check if pLocTri inside current View Frustum:
			if(IsLocTriVisibleByCamera(pLocTri))
			{
				if(pLocTri->m_bCreatesPlants)	// draw only if it creates plants
				{
					DEV_ONLY(pLocTri->m_nMaxNumPlants = 0;)
					int procTagId = PGTAMATERIALMGR->UnpackProcId(pLocTri->m_nSurfaceType);
					int plantInfoIndex = g_procInfo.m_procTagTable[procTagId].plantIndex;
					Assert(g_procInfo.m_plantInfos[plantInfoIndex].m_procTagId==procTagId);

					for( ; (g_procInfo.m_plantInfos[plantInfoIndex].m_procTagId==procTagId) && (plantInfoIndex < g_procInfo.m_numPlantInfos); plantInfoIndex++)
					{
						CPlantInfo *pPlantInfo = &g_procInfo.m_plantInfos[plantInfoIndex];
						
						// is modelID valid for this plant cover definition?
//							if (g_procInfo.m_plantInfos[plantInfoIndex].m_nModelID != PNG_PLANT_SURF_PROP_INVALID_MODELID)
						{
							PPTriPlant triPlant;
							triPlant.V1				=	pLocTri->m_V1;
							triPlant.V2				=	pLocTri->m_V2;
							triPlant.V3				=	pLocTri->m_V3;
							triPlant.center			=	pLocTri->m_Center;

							triPlant.model_id		=	pPlantInfo->m_nModelID;

							g_PlantsRendRand.Reset(pLocTri->m_Seed);

							//
							// max number of plants to generate for this TriLoc
							// this is connected with triangle area + surface type:
							//
							float numPlants = pLocTri->m_triArea * pPlantInfo->m_fDensity;

							int nMaxNumPlants = (int)numPlants;
							float remainder = numPlants - nMaxNumPlants;
							if (g_PlantsRendRand.GetRanged(0.0f, 1.0f)<remainder)
							{
								nMaxNumPlants++;
							}		

							if (nMaxNumPlants==0)
							{
							#if __DEV
								//Printf("/n PlantsMgr: maxNumPlants is 0 for this SurfaceType (%X)!", procTagId);
							#endif
								continue;
							}
		
							DEV_ONLY(pLocTri->m_nMaxNumPlants += nMaxNumPlants;)
							// num of plants to generate (must be multiple of 8):
							triPlant.num_plants		=	(u16)nMaxNumPlants;//8 + (nMaxNumPlants)&(~0x07);

							triPlant.scale.x		=	pPlantInfo->m_fScaleXY;	// scale in XY
							triPlant.scale.y		=	pPlantInfo->m_fScaleZ;	// scale in Z
							
							//triPlant.uv_offset.x	=	0.0f;	//g_procInfo.m_plantInfos[plantInfoIndex].m_vecOffsetUV.x;
							//triPlant.uv_offset.y	=	0.0f;	//g_procInfo.m_plantInfos[plantInfoIndex].m_vecOffsetUV.y;
#if PNG_PLANTSMGR_USE_SPU_RENDERING
							triPlant.texture_id		=	pPlantInfo->m_nTextureID;							// no of texture to use
							triPlant.textureLOD1_id	=	pPlantInfo->m_nTextureID+PNG_PLANT_SLOT_NUM_TEXTURES;	// LOD1 texture
#else
							triPlant.texture_ptr	=	ppGrassTextureTab[pPlantInfo->m_nTextureID];							// texture ptr to use
							triPlant.textureLOD1_ptr=	ppGrassTextureTab[pPlantInfo->m_nTextureID+PNG_PLANT_SLOT_NUM_TEXTURES];	// textureLOD1 ptr to use
#endif // PNG_PLANTSMGR_USE_SPU_RENDERING

							triPlant.color			=	pPlantInfo->m_rgbaColor;

							triPlant.intensity		=	pPlantInfo->m_nIntensity;
							triPlant.intensity_var	=	pPlantInfo->m_nIntensityVar;

							triPlant.seed			=	pLocTri->m_Seed + (plantInfoIndex*0x33);	// g_PlantsRendRand.GetRanged(0.0f, 1.0f)
							triPlant.scale_var_xy	=	pPlantInfo->m_fScaleVarXY;					// scale xyz variation
							triPlant.scale_var_z	=	pPlantInfo->m_fScaleVarZ;

							triPlant.um_param.x		=	pPlantInfo->m_fUmScaleH;		// micro-movements: global horizontal scale
							triPlant.um_param.y		=	pPlantInfo->m_fUmScaleV;		// micro-movements: global vertical scale
							triPlant.um_param.z		=	pPlantInfo->m_fUmFreqH;			// micro-movements: global horizontal frequency
							triPlant.um_param.w		=	pPlantInfo->m_fUmFreqV;			// micro-movements: global vertical frequency

							triPlant.wind_bend_scale=	pPlantInfo->m_fWindBendScale;
							triPlant.wind_bend_var	=	pPlantInfo->m_fWindBendVar;

							// set global collision radius scale:
							static TweakFloat fPlayerDefaultCollRadius = 1.0f;	//0.75f;	// radius of influence
							
							// x = collision radius sqr:
							const float collRadiusSqr = fPlayerDefaultCollRadius*fPlayerDefaultCollRadius * pPlantInfo->m_fCollRadiusScale*pPlantInfo->m_fCollRadiusScale;
							triPlant.coll_params.x	= collRadiusSqr;
							// y = inv collision radius sqr:
							triPlant.coll_params.y	= 1.0f / (collRadiusSqr);

							triPlant.LODmask = pPlantInfo->m_nLODmask;

#if __BANK
							// forced ground color:
							if(gbForceGroundColor)
							{
								triPlant.groundColorV1 = gbGroundColorV1;
								triPlant.groundColorV1.SetAlpha(pngPlantLocTri::pvPackScale(gbGroundScaleV1));
							
								triPlant.groundColorV2 = gbGroundColorV2;
								triPlant.groundColorV2.SetAlpha(pngPlantLocTri::pvPackScale(gbGroundScaleV2));
								
								triPlant.groundColorV3 = gbGroundColorV3;
								triPlant.groundColorV3.SetAlpha(pngPlantLocTri::pvPackScale(gbGroundScaleV3));
							}
							else
#endif //__BANK
							{
								triPlant.groundColorV1 = pLocTri->m_GroundColorV1;
								triPlant.groundColorV2 = pLocTri->m_GroundColorV2;
								triPlant.groundColorV3 = pLocTri->m_GroundColorV3;
							}


							pngGrassRenderer::AddTriPlant(&triPlant, PlantModelSetID);

#ifdef DBG_COUNT_DRAWN_PLANTS
							g_LocTriDrawn[slotID] += 1;
#endif // DBG_COUNT_DRAWN_PLANTS
						}
					}
				}
			}

			loctri = pLocTri->m_NextTri;
		}

		pngGrassRenderer::FlushTriPlantBuffer();
	}
#endif // 0 // TJ

	pngGrassRenderer::SpuFinalisePerRenderFrame();

	// Restore original state.
#if PSN_ALPHATOMASK_PASS
	EndGrassAlphaToMaskPass1();
#endif // PSN_ALPHATOMASK_PASS
#if __XENON
	grcState::SetAlphaToMask(false);
//	CShaderLib::SetDeferredAlphaEnable(false);

	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_HIZENABLE,		D3DHIZ_AUTOMATIC );
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_HIZWRITEENABLE,D3DHIZ_AUTOMATIC );
#endif // __XENON

//	CDeferredLightingHelper::FinishCutoutPass();

#if PSN_ALPHATOMASK_PASS
	RenderGrassAlphaToMaskPass2();
#endif // PSN_ALPHATOMASK_PASS

//	CShaderLib::SetGlobalHDRIntensityMult(1.0f);
	grcState::Default();

	//POP_TIMER();
	PIXEnd();
	
	g_PlantsRendRand.Reset(storedSeed);

#endif // PNG_PLANTMGR_ENABLED
	return true;
}

#if __DEV
bool pngPlantMgr::RenderDebugStuff()
{
//	DbgPrintpngPlantMgrInfo(g_LocTriDrawn, g_LocTriPlantsLOD0Drawn, g_LocTriPlantsLOD1Drawn, g_LocTriPlantsLOD2Drawn);

#ifdef DBG_COUNT_DRAWN_PLANTS
	for(int i=0; i<PNG_PLANT_NUM_PLANT_SLOTS; i++)
	{
		// g_LocTriDrawn[i]				= 0;	// reset statistics
		g_LocTriPlantsLOD0Drawn[i]	= 0;	// reset statistics
		g_LocTriPlantsLOD1Drawn[i]	= 0;	// reset statistics
		g_LocTriPlantsLOD2Drawn[i]	= 0;	// reset statistics
	}
#endif // DBG_COUNT_DRAWN_PLANTS

#if __BANK
//	static TweakFloat dbgHdrVal = 1.0f;
	//CShaderLib::SetGlobalHDRIntensityMult(dbgHdrVal);

	// debug render of all plant polygons:
	if(gbShowpngPlantMgrPolys)
	{
		atArray<Vector3>		dbgTriangleVertices;
		atArray<Color32>		dbgTriangleColors;

		dbgTriangleVertices.Reset();
		dbgTriangleVertices.Reserve(64);
		dbgTriangleColors.Reset();
		dbgTriangleColors.Reserve(64);

		Color32 testColor0(255, 255, 255, 255);
		Color32 testColor1(255, 0,   0,   255);
		Color32 testColor2(0,   0,   255, 255);

		// render all close lists:
		for(int slotID=0; slotID<PNG_PLANT_NUM_PLANT_SLOTS; slotID++)
		{
			int loctri = m_CloseLocTriListHead[slotID];
			while(loctri)
			{
				pngPlantLocTri *pLocTri = &m_LocTrisTab[loctri-1];
				Vector3 *pVert0 = &pLocTri->m_V1;
				Vector3 *pVert1 = &pLocTri->m_V2;
				Vector3 *pVert2 = &pLocTri->m_V3;
					dbgTriangleVertices.PushAndGrow(*pVert0);
					dbgTriangleColors.PushAndGrow(testColor0);
					dbgTriangleVertices.PushAndGrow(*pVert1);
					dbgTriangleColors.PushAndGrow(testColor1);
					dbgTriangleVertices.PushAndGrow(*pVert2);
					dbgTriangleColors.PushAndGrow(testColor2);
				loctri = pLocTri->m_NextTri;
			}
		}

		// setup render state:
		{
			grcWorldIdentity();
			grcBindTexture(NULL);

			grcState::SetCullMode(grccmNone);
			grcState::SetFillMode(grcfmSolid);
			grcState::SetAlphaTest(false);
			grcState::SetAlphaBlend(false);	//true);
			grcState::SetBlendSet(grcbsNormal);		// Standard srcAlpha, 1-srcAlpha blend
			grcState::SetDepthTest(true);
			grcState::SetDepthWrite(true);	//false);
			grcLightState::SetEnabled(false);
			
			grcState::Default();	// otherwise immediate triangles aren't drawn correctly

			DbgDrawTriangleArrays(dbgTriangleVertices, dbgTriangleColors);

			grcState::Default();
		}

	}

	// debug render of all plant polygons:
	if(gbShowpngPlantMgrGroundColor)
	{
		atArray<Vector3>		dbgTriangleVertices;
		atArray<Color32>		dbgTriangleColors;

		dbgTriangleVertices.Reset();
		dbgTriangleVertices.Reserve(64);
		dbgTriangleColors.Reset();
		dbgTriangleColors.Reserve(64);

		// render all close lists:
		for(int slotID=0; slotID<PNG_PLANT_NUM_PLANT_SLOTS; slotID++)
		{
			int loctri = m_CloseLocTriListHead[slotID];
			while(loctri)
			{
				pngPlantLocTri *pLocTri = &m_LocTrisTab[loctri-1];

				Vector3 *pVert0 = &pLocTri->m_V1;
				Vector3 *pVert1 = &pLocTri->m_V2;
				Vector3 *pVert2 = &pLocTri->m_V3;

				Color32 colorV1, colorV2, colorV3; 
				if(gbForceGroundColor)
				{	// assign debug colors:
					colorV1 = gbGroundColorV1;
					colorV2 = gbGroundColorV2;
					colorV3 = gbGroundColorV3;
				}
				else
				{
					colorV1	= pLocTri->m_GroundColorV1;
					colorV2 = pLocTri->m_GroundColorV2;
					colorV3 = pLocTri->m_GroundColorV3;
				}

				dbgTriangleVertices.PushAndGrow(*pVert0);
				dbgTriangleColors.PushAndGrow(colorV1);
				dbgTriangleVertices.PushAndGrow(*pVert1);
				dbgTriangleColors.PushAndGrow(colorV2);
				dbgTriangleVertices.PushAndGrow(*pVert2);
				dbgTriangleColors.PushAndGrow(colorV3);
				
				loctri = pLocTri->m_NextTri;
			}

		}

		// setup render state:
		{
			grcWorldIdentity();
			grcBindTexture(NULL);

			grcState::SetCullMode(grccmNone);
			grcState::SetFillMode(grcfmSolid);
			grcState::SetAlphaTest(false);
			grcState::SetAlphaBlend(false);
			grcState::SetBlendSet(grcbsNormal);
			grcState::SetDepthTest(true);
			grcState::SetDepthWrite(true);
			grcLightState::SetEnabled(false);
			
			grcState::Default();

			DbgDrawTriangleArrays(dbgTriangleVertices, dbgTriangleColors);

			grcState::Default();
		}
	}
#endif // __BANK

	return true;
}
#endif // __DEV

pngPlantColBoundEntry* pngPlantColBoundEntry::AddEntry(phInst* pPhysicsInst)
{
	Assert(gPlantMgr.m_UnusedColEntListHead);

	m_pPhysicsInst = pPhysicsInst; // this assigment must be first, otherwise all Get...() methods below won't work...

	phBoundGeometry* pBound = this->GetBound();
	Assert(pBound); // should it ever happen?
	if(!pBound)
	{
		return(NULL); // nothing added...
	}

	const Matrix34 *pBoundMat = this->GetBoundMatrix();
	if(pBoundMat)
	{
		this->m_BoundMat.Set(*pBoundMat);
		// check whether BoundMat is an identity matrix (so no transformations are required for LocTris):
		this->m_bBoundMatIdentity = (!pBoundMat->IsNotEqual(M34_IDENTITY));
	}
	else
	{	// should it ever happen?
		// idea: maybe flag this situation and get matrix later?
		Assertf(false, "Bound matrix not available!");
		this->m_BoundMat.Set(M34_IDENTITY);
		this->m_bBoundMatIdentity = true;
	}

	if(pBound->GetNumPolygons() > 0)
	{
		const int numPolys= pBound->GetNumPolygons();
		const int numTris = numPolys*2;				// allocate twice the size (must have enough space for quads)
		this->m_nNumTris 	= (u16)numTris;
		this->m_LocTriArray	= rage_new u16[numTris];

		for(int i=0; i<numTris; i++)
		{
			this->m_LocTriArray[i] = 0;
		}

#if 0 // TJ
		// initialize material props for all polys of the bound:
		this->m_BoundMatProps.Init(numPolys*2);
		for(int i=0; i<numPolys; i++)
		{
			// check surface type of polygon:
			const int boundMaterialID	= pBound->GetPolygonMaterialIndex(i);
			const phMaterialMgr::Id	actualMaterialID = pBound->GetMaterialId(boundMaterialID);

			int procTagId = PGTAMATERIALMGR->UnpackProcId(actualMaterialID);
			bool bCreatesPlants  = g_procInfo.CreatesPlants(procTagId);
			bool bCreatesObjects = g_procInfo.CreatesProcObjects(procTagId);

			this->m_BoundMatProps.Set(i*2+0, bCreatesPlants); 
			this->m_BoundMatProps.Set(i*2+1, bCreatesObjects);
		}
#endif // 0

		gPlantMgr.MoveColEntToList(&gPlantMgr.m_UnusedColEntListHead, &gPlantMgr.m_CloseColEntListHead, this);
		return(this);
	}

	return(NULL);
}

void pngPlantColBoundEntry::ReleaseEntry()
{
	if(this->m_LocTriArray)
	{
		// release LocTris first:
		for(int i=0; i<this->m_nNumTris; i++)
		{
			int tri = m_LocTriArray[i];
			if (tri)
			{
				pngPlantLocTri *pTri = &gPlantMgr.m_LocTrisTab[tri-1];
				pTri->Release();	
			}
		}
	
		delete[] this->m_LocTriArray;
		this->m_LocTriArray = NULL;
		this->m_BoundMatProps.Kill();
		this->m_nNumTris	= 0;
	}

// TJ
// 	if(this->m_pEntity)
// 	{
// 		TIDYREF((this->m_pEntity), (&this->m_pEntity));
// 	}
// 	this->m_pEntity		= NULL;

	gPlantMgr.MoveColEntToList(&gPlantMgr.m_CloseColEntListHead, &gPlantMgr.m_UnusedColEntListHead, this);
}

pngPlantLocTri* pngPlantMgr::MoveLocTriToList(u16*ppCurrentList, u16*ppNewList, pngPlantLocTri *pTri)
{

	Assertf(*ppCurrentList, "CPlant::MoveLocTriToList(): m_CurrentList==NULL!");


	// First - Cut out of old list
	if(!pTri->m_PrevTri)
	{
		// if at head of old list
		*ppCurrentList = pTri->m_NextTri;
		if(*ppCurrentList)	// might have been only 1 entry in list
			m_LocTrisTab[*ppCurrentList-1].m_PrevTri = 0;
	}
	else if(!pTri->m_NextTri)
	{
		// if at tail of old list
		m_LocTrisTab[pTri->m_PrevTri-1].m_NextTri = 0;
	}
	else
	{	// else if in middle of old list
		m_LocTrisTab[pTri->m_NextTri-1].m_PrevTri = pTri->m_PrevTri;
		m_LocTrisTab[pTri->m_PrevTri-1].m_NextTri = pTri->m_NextTri;
	}

	// Second - Insert at start of new list
	pTri->m_NextTri	= *ppNewList;
	pTri->m_PrevTri	= 0;
	*ppNewList = (u16)(pTri - m_LocTrisTab + 1);
	
	if(pTri->m_NextTri)
		m_LocTrisTab[pTri->m_NextTri-1].m_PrevTri = *ppNewList;

	return(pTri);

}

pngPlantColBoundEntry* pngPlantMgr::MoveColEntToList(u16*ppCurrentList, u16*ppNewList, pngPlantColBoundEntry *pEntry)
{
	Assertf(*ppCurrentList, "CPlant::MoveColEntToList(): m_CurrentList==NULL!");

	// First - Cut out of old list
	if(pEntry->m_PrevEntry==0)
	{
		// if at head of old list
		*ppCurrentList = pEntry->m_NextEntry;
		if(*ppCurrentList)	// might have been only 1 entry in list
			m_ColEntCacheTab[(*ppCurrentList)-1].m_PrevEntry = 0;
	}
	else if(pEntry->m_NextEntry==0)
	{
		// if at tail of old list
		m_ColEntCacheTab[pEntry->m_PrevEntry-1].m_NextEntry = 0;
	}
	else
	{	// else if in middle of old list
		m_ColEntCacheTab[pEntry->m_NextEntry-1].m_PrevEntry = pEntry->m_PrevEntry;
		m_ColEntCacheTab[pEntry->m_PrevEntry-1].m_NextEntry = pEntry->m_NextEntry;
	}

	// Second - Insert at start of new list
	pEntry->m_NextEntry	= *ppNewList;
	pEntry->m_PrevEntry	= 0;
	*ppNewList = (u16)(pEntry - m_ColEntCacheTab + 1);
	
	if(pEntry->m_NextEntry)
		m_ColEntCacheTab[pEntry->m_NextEntry-1].m_PrevEntry = *ppNewList;

	return(pEntry);
}

//
// 1) way no. 1:
// Area = |P1xP2 + P2xP3 + P3xP1| / 2;
//
//
// 2) way no. 2:
// V1 = P2-P1, V2 = P3-P1
// Area = |V1xV2| / 2;
//
//
//
#if 0 // TJ
static float _CalcLocTriArea(pngPlantLocTri *pLocTri)
{
	const Vector3 V1(pLocTri->m_V2 - pLocTri->m_V1);
	const Vector3 V2(pLocTri->m_V3 - pLocTri->m_V1);
	Vector3 c;
	c.Cross(V1, V2);
	const float area2 = c.Mag() * 0.5f;

	return(area2);
}
#endif // 0

pngPlantLocTri* pngPlantLocTri::Add(const Vector3& v1, const Vector3& v2, const Vector3& v3, phMaterialMgr::Id nSurfaceType, u8 nLighting, bool bCreatesPlants, bool bCreatesObjects/*, CEntity* pParentEntity*/)
{
//	Assert(pParentEntity);
	Assert(gPlantMgr.m_UnusedLocTriListHead);

	this->m_V1 = v1;
	this->m_V2 = v2;
	this->m_V3 = v3;

	this->m_nSurfaceType	= nSurfaceType;
	this->m_nLighting		= nLighting;
	
	this->m_bCreatesPlants	= bCreatesPlants;
	this->m_bCreatesObjects	= bCreatesObjects;
	this->m_bCreatedObjects	= false;

	const float _1_3 = 1.0f / 3.0f;
	this->m_Center = (v1) + (v2) + (v3);
	this->m_Center *= _1_3;

	Vector3 vecSphRadius(this->m_Center - this->m_V1);
	this->m_SphereRadius = vecSphRadius.Mag();
	this->m_SphereRadius *= 1.75f;	// make sphere radius 75% bigger (for better sphere visibility detection)
	//this->m_pParentEntity = pParentEntity;

	// FIXME():
	// test: localized per-vertex ground color(RGB) + scale(A):
	this->m_GroundColorV1	= gbDefaultGroundColor;
	this->m_GroundColorV2	= gbDefaultGroundColor;
	this->m_GroundColorV3	= gbDefaultGroundColor;

	if(m_bCreatesObjects && !m_bCreatesPlants)
	{
		// deal with ONLY procedurally generated objects
		gPlantMgr.MoveLocTriToList(&gPlantMgr.m_UnusedLocTriListHead, &gPlantMgr.m_CloseLocTriListHead[3], this);
		return(this);
	}
	else
	{		
#if 0 // TJ
		int procTagId = PGTAMATERIALMGR->UnpackProcId(nSurfaceType);
		int plantInfoIndex = g_procInfo.m_procTagTable[procTagId].plantIndex;

		const float triArea	= _CalcLocTriArea(this);

		const float numPlants = triArea * g_procInfo.m_plantInfos[plantInfoIndex].m_fDensity;

		// only update the list if Density > 0.0f:
		if (numPlants>0.05f)
		{
			this->m_triArea = triArea;
			this->m_Seed = g_DrawRand.GetInt();	// this is update thread

			const u32 slotID = g_procInfo.m_plantInfos[plantInfoIndex].m_nPlantSlotID;
			gPlantMgr.MoveLocTriToList(&gPlantMgr.m_UnusedLocTriListHead, &gPlantMgr.m_CloseLocTriListHead[slotID], this);
			return(this);
		}
		else if (m_bCreatesObjects)
		{
			this->m_bCreatesPlants = false;
			gPlantMgr.MoveLocTriToList(&gPlantMgr.m_UnusedLocTriListHead, &gPlantMgr.m_CloseLocTriListHead[3], this);
			return(this);
		}
#endif // 0

		return(NULL);
	}
}

void pngPlantLocTri::Release()
{
	this->m_triArea = 0.0f;

//	if (m_bCreatedObjects)
//	{
//		g_procObjMan.AddTriToRemoveList(this);
//	}

	if (m_bCreatesObjects && !m_bCreatesPlants)
	{
		gPlantMgr.MoveLocTriToList(&gPlantMgr.m_CloseLocTriListHead[3], &gPlantMgr.m_UnusedLocTriListHead, this);
		this->m_nSurfaceType			= 0xFE;		// simple tag to show who released this
	}
#if 0 // TJ
	else
	{
		int procTagId = PGTAMATERIALMGR->UnpackProcId(m_nSurfaceType);
		int plantInfoIndex = g_procInfo.m_procTagTable[procTagId].plantIndex;
		const u32 slotID = g_procInfo.m_plantInfos[plantInfoIndex].m_nPlantSlotID;
		gPlantMgr.MoveLocTriToList(&gPlantMgr.m_CloseLocTriListHead[slotID], &gPlantMgr.m_UnusedLocTriListHead, this);
		this->m_nSurfaceType			= 0xFF;		// simple tag to show who released this
	}
#endif // 0
	
	this->m_bCreatesPlants = false;
	this->m_bCreatesObjects = false;
	this->m_bCreatedObjects = false;

//	this->m_pParentEntity = NULL;
}

#if __DEV
#if 0 // TJ
bool pngPlantMgr::DbgPrintpngPlantMgrInfo(u32 *BANK_ONLY(nNumLocTrisDrawn) , u32 *BANK_ONLY(nNumPlantsLOD0Drawn),  u32 *BANK_ONLY(nNumPlantsLOD1Drawn), u32 *BANK_ONLY(nNumPlantsLOD2Drawn))
{
#if __BANK
	Assert(nNumLocTrisDrawn);
	Assert(nNumPlantsLOD0Drawn);
	Assert(nNumPlantsLOD1Drawn);
	Assert(nNumPlantsLOD2Drawn);

	// print some debug/stats info:
	if(gbDisplaypngPlantMgrInfo)
	{
		u32 nNumCachedBounds=0;
		u32 nNumLocTris[PNG_PLANT_NUM_PLANT_SLOTS]={0}, nNumPlants[PNG_PLANT_NUM_PLANT_SLOTS]={0};

		DbgCountCachedBounds(&nNumCachedBounds);

		for(int groupID=0; groupID<PNG_PLANT_NUM_PLANT_SLOTS; groupID++)
		{
			DbgCountLocTrisAndPlants(groupID, &nNumLocTris[groupID], &nNumPlants[groupID]);
		}

// 		char buf[256];
// 		const int StartX = 34;
// 		const int StartY = 42;

		//::sprintf(buf, "pngPlantMgr::Info:");
		//CDebug::PrintToScreenCoors(buf, StartX+0, StartY+0);
		//::sprintf(buf, "----------------");
		//CDebug::PrintToScreenCoors(buf, StartX+0, StartY+1);

		//::sprintf(buf, "Bounds: All=%d", nNumCachedBounds);
		//CDebug::PrintToScreenCoors(buf, StartX+0, StartY+2);

		int slotID=0;
		//::sprintf(buf, "LocTris[0]: All=%d (dr=%d) Plants=%d (LOD0: dr=%d (%.2f%%), LOD1: dr=%d (%.2f%%), LOD2: dr=%d (%.2f%%))", nNumLocTris[slotID], nNumLocTrisDrawn[slotID], nNumPlants[slotID], nNumPlantsLOD0Drawn[slotID], 100.0f*float(nNumPlantsLOD0Drawn[slotID])/float(nNumPlants[slotID]+1), nNumPlantsLOD1Drawn[slotID], 100.0f*float(nNumPlantsLOD1Drawn[slotID])/float(nNumPlants[slotID]+1), nNumPlantsLOD2Drawn[slotID], 100.0f*float(nNumPlantsLOD2Drawn[slotID])/float(nNumPlants[slotID]+1));
		//CDebug::PrintToScreenCoors(buf, StartX+0, StartY+3);
		
		slotID=1;
		//::sprintf(buf, "LocTris[1]: All=%d (dr=%d) Plants=%d (LOD0: dr=%d (%.2f%%), LOD1: dr=%d (%.2f%%), LOD2: dr=%d (%.2f%%))", nNumLocTris[slotID], nNumLocTrisDrawn[slotID], nNumPlants[slotID], nNumPlantsLOD0Drawn[slotID], 100.0f*float(nNumPlantsLOD0Drawn[slotID])/float(nNumPlants[slotID]+1), nNumPlantsLOD1Drawn[slotID], 100.0f*float(nNumPlantsLOD1Drawn[slotID])/float(nNumPlants[slotID]+1), nNumPlantsLOD2Drawn[slotID], 100.0f*float(nNumPlantsLOD2Drawn[slotID])/float(nNumPlants[slotID]+1));
		//CDebug::PrintToScreenCoors(buf, StartX+0, StartY+4);
		
		slotID=2;
		//::sprintf(buf, "LocTris[2]: All=%d (dr=%d) Plants=%d (LOD0: dr=%d (%.2f%%), LOD1: dr=%d (%.2f%%), LOD2: dr=%d (%.2f%%))", nNumLocTris[slotID], nNumLocTrisDrawn[slotID], nNumPlants[slotID], nNumPlantsLOD0Drawn[slotID], 100.0f*float(nNumPlantsLOD0Drawn[slotID])/float(nNumPlants[slotID]+1), nNumPlantsLOD1Drawn[slotID], 100.0f*float(nNumPlantsLOD1Drawn[slotID])/float(nNumPlants[slotID]+1), nNumPlantsLOD2Drawn[slotID], 100.0f*float(nNumPlantsLOD2Drawn[slotID])/float(nNumPlants[slotID]+1));
		//CDebug::PrintToScreenCoors(buf, StartX+0, StartY+5);
		
		slotID=3;
		//::sprintf(buf, "LocTris[3]: All=%d (dr=%d) Plants=%d (LOD0: dr=%d (%.2f%%), LOD1: dr=%d (%.2f%%), LOD2: dr=%d (%.2f%%))", nNumLocTris[slotID], nNumLocTrisDrawn[slotID], nNumPlants[slotID], nNumPlantsLOD0Drawn[slotID], 100.0f*float(nNumPlantsLOD0Drawn[slotID])/float(nNumPlants[slotID]+1), nNumPlantsLOD1Drawn[slotID], 100.0f*float(nNumPlantsLOD1Drawn[slotID])/float(nNumPlants[slotID]+1), nNumPlantsLOD2Drawn[slotID], 100.0f*float(nNumPlantsLOD2Drawn[slotID])/float(nNumPlants[slotID]+1));
		//CDebug::PrintToScreenCoors(buf, StartX+0, StartY+6);

#if __DEV
		//::sprintf(buf, "TriLocFarDist: %.1fm", Sqrt(PNG_PLANT_TRILOC_FAR_DIST_SQR));
		//CDebug::PrintToScreenCoors(buf, StartX+0, StartY+8);
		//::sprintf(buf, "AlphaClose/FarDist: %.1fm/%.1fm", PNG_PLANT_LOD0_ALPHA_CLOSE_DIST, PNG_PLANT_LOD0_ALPHA_FAR_DIST);
		//CDebug::PrintToScreenCoors(buf, StartX+0, StartY+9);
#endif

#if PNG_PLANTSMGR_USE_SPU_RENDERING
		//::sprintf(buf, "SPU BigBuffer: %dKB, consumed: %dKB (%.2f%%), overfilled: %dKB", PNG_PLANTSMGR_BIG_HEAP_SIZE/1024, g_PlantsSpuBigBufferConsumed/1024, float(g_PlantsSpuBigBufferConsumed)/float(PNG_PLANTSMGR_BIG_HEAP_SIZE)*100.0f,g_PlantsSpuBigBufferOverfilled/1024);
		//CDebug::PrintToScreenCoors(buf, StartX+0, StartY+10);
#endif
	}
#endif //__BANK...

	return true;
}
#endif // 0

bool pngPlantMgr::DbgCountCachedBounds(u32 *DEV_ONLY(pCountAll) )
{
#if __DEV
	u32 nCountEnt = 0;

	int entry = m_CloseColEntListHead;
	while(entry)
	{
		pngPlantColBoundEntry *pEntry = &m_ColEntCacheTab[entry-1];
		Assert(pEntry->GetBound());
		nCountEnt++;

		entry = pEntry->m_NextEntry;
	}
	
	if(pCountAll)
	{
		*pCountAll = nCountEnt;
	}

#endif // __DEV
	return true;
}

bool pngPlantMgr::DbgCountLocTrisAndPlants(u32 DEV_ONLY(groupID) , u32 *DEV_ONLY(pCountLocTris) , u32 *DEV_ONLY(pCountPlants) )
{
#if __DEV
	int	nCountTris		= 0,
	nCountPlants	= 0;

	const u32 bufferID = GetUpdateRTBufferID();	// read previous buffer for stats

	int locTri = m_CloseLocTriListRenderHead[bufferID][groupID];
	while(locTri)
	{
		pngPlantLocTri *pLocTri = &m_LocTrisRenderTab[bufferID][locTri-1];
		nCountTris++;
		nCountPlants += pLocTri->m_nMaxNumPlants;
		locTri = pLocTri->m_NextTri;
	}//while(pLocTri)...


	if(pCountLocTris)
	{
		*pCountLocTris = nCountTris;
	}
	
	if(pCountPlants)
	{
		*pCountPlants = nCountPlants;
	}
#endif // __DEV

	return true;
}

void pngPlantMgr::DbgDrawTriangleArrays(atArray<Vector3> &DEV_ONLY(triangleVertices) , atArray<Color32>& DEV_ONLY(triangleColors) )
{
#if __DEV
//	const	u32 nNumVerts= triangleVertices.size();
	const	u32 nNumTris	= triangleVertices.GetCount() / 3;
	Assert(triangleVertices.GetCount() == triangleColors.GetCount());

#define Z_OFFSET		(0.25f)

	int numTrisLeftToDraw = nNumTris;
	int actualVertIndex = 0;

	while(numTrisLeftToDraw > 0)
	{
		int trisToDraw = numTrisLeftToDraw;

		if((numTrisLeftToDraw*3) > grcBeginMax)
		{
			trisToDraw			=  grcBeginMax/3;
			numTrisLeftToDraw	-= grcBeginMax/3;
		}
		else
		{
			trisToDraw			 = numTrisLeftToDraw;
			numTrisLeftToDraw	-= trisToDraw;
		}
		
		grcNormal3f(0.0f, 0.0f, 1.0f);
		grcTexCoord2f(0.0f, 0.0f);

		grcBegin(drawTris, trisToDraw*3);
		for(int v=0; v<trisToDraw; v++)
		{
			Color32	*pCol0	= &triangleColors	[actualVertIndex];
			Vector3 *pVert0 = &triangleVertices	[actualVertIndex];
			actualVertIndex++;

			Color32	*pCol1	= &triangleColors	[actualVertIndex];
			Vector3 *pVert1 = &triangleVertices	[actualVertIndex];
			actualVertIndex++;
			
			Color32	*pCol2	= &triangleColors	[actualVertIndex];
			Vector3 *pVert2 = &triangleVertices	[actualVertIndex];
			actualVertIndex++;

			//grcTexCoord2f(0.0f, 0.0f);
			grcColor(*pCol0);
			grcVertex3f(pVert0->x, pVert0->y, pVert0->z + Z_OFFSET);
			//grcTexCoord2f(1.0f, 0.0f);
			grcColor(*pCol1);
			grcVertex3f(pVert1->x, pVert1->y, pVert1->z + Z_OFFSET);
			//grcTexCoord2f(0.0f, 1.0f);
			grcColor(*pCol2);
			grcVertex3f(pVert2->x, pVert2->y, pVert2->z + Z_OFFSET);
		}
		grcEnd();
	}
#endif // __DEV
}

void pngPlantMgr::DbgStatsCountCachedBounds( u32* pCountAll )
{
	u32 nCountEnt = 0;

	int entry = m_CloseColEntListHead;
	while ( entry )
	{
		pngPlantColBoundEntry *pEntry = &m_ColEntCacheTab[entry-1];
		Assert(pEntry->GetBound());
		nCountEnt++;

		entry = pEntry->m_NextEntry;
	}

	if(pCountAll)
	{
		*pCountAll = nCountEnt;
	}
}

void pngPlantMgr::DbgStatsCountLocTrisAndPlants( u32 groupID , u32* pCountLocTris, u32* pCountPlants )
{
	int nCountTris = 0;
	int nCountPlants = 0;

	int locTri = m_CloseLocTriListHead[groupID];
	while ( locTri )
	{
		pngPlantLocTri *pLocTri = &m_LocTrisTab[locTri-1];
		nCountTris++;
		nCountPlants += pLocTri->m_nMaxNumPlants;
		locTri = pLocTri->m_NextTri;
	}

	if(pCountLocTris)
	{
		*pCountLocTris = nCountTris;
	}

	if(pCountPlants)
	{
		*pCountPlants = nCountPlants;
	}
}
#endif // __DEV

} // namespace rage
