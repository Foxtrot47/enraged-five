// 
// sample_snet/app.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "app.h"
#include "livemanager.h"

#include "input/input.h"
#include "grcore/device.h"
#include "grcore/font.h"
#include "grcore/im.h"
#include "grcore/image.h"
#include "math/amath.h"
#include "system/nelem.h"
#include "system/timer.h"
#include "vector/colors.h"
 
#include "data/rson.h"
#include "file/asset.h"
#include "net/nethardware.h"
#include "net/http.h"
#include "net/task.h"
#include "paging/streamer.h"
#include "rline/rlachievement.h"
#include "rline/cloud/rlcloud.h"
#include "rline/cloudsave/rlcloudsave.h"
#include "rline/scachievements/rlscachievements.h"
#include "rline/socialclub/rlsocialclub.h"
#include "rline/ugc/rlugc.h"
#include "rline/unlocks/rlunlocks.h"
#include "rline/entitlement/rlrosentitlement.h"
#include "rline/facebook/rlfacebookcommon.h"
#include "rline/facebook/rlfacebook.h"
#include "rline/ros/rlros.h"
#include "rline/rlfriendsmanager.h"
#include "string/string.h"
#include "system/service.h"
#include "system/userlist.h"
#include "rline/rlsystemui.h"
#include "rline/ugc/rlugccommon.h"
#include "rline/scfriends/rlscfriends.h"
#include "rline/rltitleid.h"
#include "rline/youtube/rlyoutube.h"
#include "rline/rltelemetry.h"

#if RSG_PC
#include "mmsystem.h"
#endif

#include "rline/rlhttptask.h"

#include <time.h>

#if RSG_DURANGO
#include "durango/Events_durango.h"
#endif

#if LIVESTREAM_ENABLED
#include "grcore/device.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "grcore/texturepc.h"
#include "grcore/texturedefault.h"
#include <d3dx11.h>
#endif

namespace rage
{
	extern const rlTitleId* g_rlTitleId;
#if RSG_PC && !__FINAL
	XPARAM(takehometest);
#endif
	XPARAM(scnoui);
}

// hack - so we can link..
namespace rage
{
	u32 GetStrIndexFromHandle(const pgStreamer::Handle & /*handle*/)
	{
		return 0;
	}
}

static bool rlAchievementInited = false;
static rlAchievement sm_rlAchievement;
static int sm_NumAchRead = 0;

static rlAchievementInfo sm_AchievementsInfo[256];
static rlAchievementInfoEx sm_AchievementsInfoEx[256];

static rlUgcMetadata g_UgcMetadata;
const unsigned T_MAX_RESULTS = 200;
const unsigned T_MAX_PLAYERS = 1;
//static rlUgcQueryContentResults g_UgcResults;
//static rlUgcQueryContentResult<T_MAX_PLAYERS> g_UgcResultRows[T_MAX_RESULTS];
//static rlUgcQueryContentResult<T_MAX_PLAYERS> g_UgcResultRow;

const unsigned MAX_CONTENT_CREATORS = 10;
rlUgcContentCreatorInfo g_UgcContentCreators[MAX_CONTENT_CREATORS];

char s_AutoTestString[512] = {0};

unsigned g_UgcCount;
int g_UgcTotal;
int g_UgcHash = 0;

const unsigned MAX_ALTERNATE_NICKNAMES = 5;
char g_AlternateNicknames[MAX_ALTERNATE_NICKNAMES][RLSC_MAX_NICKNAME_CHARS];
unsigned g_NumAlternateNicknames;

char scAuthToken[RLSC_MAX_SCAUTHTOKEN_CHARS] = {0};

ioVirtualKeyboard App::sm_VirtualKeyboard;

#if RSG_PC
rgsc::ICloudSaveManifest* App::rgscManifest = NULL;
rgsc::ICloudSaveBackupInfo* App::rgscBackupInfo = NULL;
#endif

#if __LIVE
#include "system/xtl.h"
#include <xparty.h>
#endif  //__LIVE

#if RSG_PC
#include "rline/rlpc.h"
#endif

#if __WIN32PC && __DEV
XPARAM(allowbackgroundinput);
#endif

PARAM(sampleSlots, "Render the pubic/private/reserved slot information");

namespace rage
{
	XPARAM(noMarshalInput);
}

PARAM(notimeouts, "[network] Disable timeouts for network sessions");

// Command Line Parameters
PARAM(twitchUsername, "Sets the Twitch.TV username for live streaming");
PARAM(twitchPassword, "Sets the Twitch.TV password for live streaming");

using namespace rage;

NET_MESSAGE_IMPL(MigrateHostMsg);
NET_MESSAGE_IMPL(SwingMsg);
NET_MESSAGE_IMPL(StartMatchMsg);
NET_MESSAGE_IMPL(EndMatchMsg);

float g_Progress = 0.0f;

s16 g_SocialClubTermsOfServiceVersion;
rlScCountries* g_SocialClubCountries;

#if RSG_PC

rgsc::RgscGamepad* m_ScuiPads[ioPad::MAX_PADS];
void InputInit()
{
	if (PARAM_noMarshalInput.Get())
		return;

	// Verify that the gamepad manager in the SDK is valid before attempting to setup
	if (g_rlPc.GetGamepadManager() == NULL)
		return;

	// Enumerate through the pads array, allocating a RgscGamepad for each slot
	int numPads = 0;
	for (int i = 0; i < COUNTOF(m_ScuiPads) && i < ioPad::MAX_PADS; i++)
	{
		m_ScuiPads[numPads] = rage_new rgsc::RgscGamepad();
		if (m_ScuiPads[numPads])
		{
			m_ScuiPads[numPads]->ClearInputs();
			m_ScuiPads[numPads]->SetPadIndex(numPads);
			numPads++;
		}
	}

	// Forward the gamepad array to the SDK GamePadManager
	g_rlPc.GetGamepadManager()->SetMarshalledGamepads((rgsc::IGamepad**)&m_ScuiPads[0], numPads);
}

void InputUpdate()
{
	if (PARAM_noMarshalInput.Get())
		return;

	// Enumerate through our pads
	for (int i = 0; i < ioPad::MAX_PADS; i++)
	{
		ioPad& pad = ioPad::GetPad(i);

		// SCUI input cannot be updated until the rlPC GamePad Manager is initialized
		if (g_rlPc.GetGamepadManager() != NULL)
		{
			// Update the SCUI pad's connection state with our own
			m_ScuiPads[i]->SetIsConnected(pad.IsConnected());

			// Prepare for an update by clearing all inputs (pushes previous input into the 'last' state)
			// m_ScuiPads[i]->ClearInputs();

			// Only update the pad if it is connected
			if (pad.IsConnected())
			{
				// Pull directly from the underlying pad, since the CPad doesn't update when
				// the SCUI is on screen.
				m_ScuiPads[i]->SetButtons(pad.GetButtons());
				m_ScuiPads[i]->SetAxis(0, (rgsc::u8)pad.GetLeftX());
				m_ScuiPads[i]->SetAxis(1, (rgsc::u8)pad.GetLeftY());
				m_ScuiPads[i]->SetAxis(2, (rgsc::u8)pad.GetRightX());
				m_ScuiPads[i]->SetAxis(3, (rgsc::u8)pad.GetRightY());
				m_ScuiPads[i]->SetHasInput();
			}
		}
	}
}

#endif // RSG_PC

void 
App::OnUgcQueryContent(const int OUTPUT_ONLY(index),
                       const char* OUTPUT_ONLY(contentId),
                       const rlUgcMetadata* OUTPUT_ONLY(metadata),
                       const rlUgcRatings* OUTPUT_ONLY(ratings),
                       const char* OUTPUT_ONLY(statsJson),
                       const unsigned OUTPUT_ONLY(numPlayers),
                       const unsigned OUTPUT_ONLY(playerIndex),
                       const rlUgcPlayer* OUTPUT_ONLY(player))
{
#if !__NO_OUTPUT
    char clap[RLUGC_MAX_CLOUD_ABS_PATH_CHARS];
    clap[0] = '\0';

    if (metadata != NULL) metadata->ComposeCloudAbsPath(0, clap, sizeof(clap));

    rlDebug("OnUgcQueryContent:");
    rlDebug("   total        = %d", g_UgcTotal);
    rlDebug("   hash         = %d", g_UgcHash);
    rlDebug("   index        = %d", index);
    rlDebug("   contentId    = %s", contentId);
    rlDebug("   metadata     = %s", (metadata != NULL) ? "NOT NULL" : "NULL");
    rlDebug("     clap 0     = %s", clap);
    rlDebug("   ratings      = %s", (ratings != NULL) ? "NOT NULL" : "NULL");
    rlDebug("   stats        = %s", (statsJson != NULL) ? statsJson : "NULL");
    rlDebug("   numPlayers   = %u", numPlayers);
    rlDebug("   playerIndex  = %u", playerIndex);

    if (player != NULL)
    {
        rlDebug("   playerUserId = %s", player->GetUserId());
    }
#endif
}

App::App()
    : m_LiveMgr(NULL)
    , m_Gamer(NULL)
    , m_NetMode(RL_NETMODE_INVALID)
    , m_NumSearchResults(0)
    , m_LbReadId(~0u)
    , m_ActiveFriend(-1)
    , m_ActiveClan(NULL)
    , m_PrimaryClanCount(0)
    , m_LeavingByChoice(false)
	, m_CrashTime(0)
    , m_Done(false)
    , m_Initialized(false)
    , m_NumLocalProfileStats(0)
#if RSG_PC
	, m_RgscStatus(NULL)
#endif
{
    //Bind delegates
	m_RosDlgt.Bind(this, &App::OnRosEvent);
    m_PresenceDlgt.Bind(this, &App::OnPresenceEvent);
    m_SessionDlgt.Bind(this, &App::OnSessionEvent);
    m_CxnMgrDlgt.Bind(this, &App::OnCxnEvent);
	m_FriendDlgt.Bind(this, &App::OnFriendEvent);
#if LIVESTREAM_ENABLED
	m_LiveStreamDlgt.Bind(this, &App::OnLiveStreamEvent);
#endif

    //Init UI pages
	::memset(m_Pages, 0, sizeof(m_Pages));

    m_Pages[STATE_FRONT_END]                = &m_UiFrontEnd;
    m_Pages[STATE_MULTIPLAYER_MAIN_MENU]    = &m_UiMultiplayerMainMenu;
    m_Pages[STATE_CONFIGURING_HOST]         = &m_UiHostConfig;
    m_Pages[STATE_CONFIGURING_QUERY]        = &m_UiQueryConfig;
    m_Pages[STATE_SELECTING_SESSION]        = &m_UiSessionSelect;
    m_Pages[STATE_VIEWING_SESSION_DETAILS]  = &m_UiSessionDetails;
    m_Pages[STATE_LOBBY]                    = &m_UiLobby;
    m_Pages[STATE_GAME]                     = &m_UiGame;
    m_Pages[STATE_CONFIGURING_ATTRIBUTES]   = &m_UiConfigureAttrs;
    m_Pages[STATE_VIEWING_FRIENDS_LIST]     = &m_UiFriendsList;
    m_Pages[STATE_VIEWING_INVITES_RECEIVED] = &m_UiInvitesReceived;
    m_Pages[STATE_LEADERBOARDS_MAIN_MENU]   = &m_UiLeaderboardMainMenu;
    m_Pages[STATE_LEADERBOARDS2_WRITE_MENU] = &m_UiLeaderboard2WriteMenu;
    m_Pages[STATE_LEADERBOARDS2_READ_MENU]  = &m_UiLeaderboard2ReadMenu;
    m_Pages[STATE_LEADERBOARDS_PLAYER_LB_MENU]  = &m_UiLeaderboardMenu;
    m_Pages[STATE_LEADERBOARDS_GROUP_LB_MENU]   = &m_UiLeaderboardMenu;
    m_Pages[STATE_LEADERBOARDS_GROUP_MEMBER_LB_MENU]  = &m_UiLeaderboardMenu;
    m_Pages[STATE_INBOX_MAIN_MENU]          = &m_UiInboxMenu;
    m_Pages[STATE_SOCIAL_CLUB_MAIN_MENU]    = &m_UiSocialClubMainMenu;
	m_Pages[STATE_COMMERCE_AND_ENTITLEMENT_MAIN_MENU] = &m_UiCommerceAndEntitlementMainMenu; 
	m_Pages[STATE_FACEBOOK_MAIN_MENU]		= &m_UiFacebookMainMenu;
    m_Pages[STATE_CLOUD_MAIN_MENU]          = &m_UiCloudMainMenu;
    m_Pages[STATE_UGC_MAIN_MENU]            = &m_UiUgcMainMenu;
    m_Pages[STATE_UGC_CONTENT_LIST]         = &m_UiUgcContentList;
    m_Pages[STATE_UGC_CONTENT_DETAILS]      = &m_UiUgcContentDetails;
    m_Pages[STATE_UGC_PLAYER_LIST]          = &m_UiUgcPlayerList;
    m_Pages[STATE_PRESENCE_MAIN_MENU]       = &m_UiPresenceMainMenu;
	m_Pages[STATE_RICHPRESENCE_MAIN_MENU]	= &m_UiRichPresenceMainMenu;
	m_Pages[STATE_ACHIEVEMENTS_MAIN_MENU]   = &m_UiAchievementsMainMenu;
	m_Pages[STATE_CLANS_MAIN_MENU]          = &m_UiClansMainMenu;
    m_Pages[STATE_CLANS_ALL_MENU]           = &m_UiClansAllMenu;
    m_Pages[STATE_CLANS_MEMBERS_MENU]       = &m_UiClanMembersMenu;
    m_Pages[STATE_CLANS_RANKS_MENU]         = &m_UiClanRanksMenu;
    m_Pages[STATE_CLANS_REQUESTS_MENU]      = &m_UiClanRequestsMenu;
    m_Pages[STATE_CLANS_SENT_REQUESTS_MENU] = &m_UiClanSentRequestsMenu;
    m_Pages[STATE_CLANS_INVITES_MENU]       = &m_UiClanInvitesMenu;
    m_Pages[STATE_CLANS_INVITE_FRIEND_MENU] = &m_UiClanInviteFriendMenu;
    m_Pages[STATE_CLANS_WALL_MESSAGE_MENU]  = &m_UiClanWallMessagesMenu;
    m_Pages[STATE_GENERIC_RESULT]           = &m_UiGenericResult;
    m_Pages[STATE_PROFILE_STATS_MAIN_MENU]  = &m_UiProfileStatsMainMenu;
	m_Pages[STATE_PROFILE_STATS_RESET_BY_GROUP_MENU] = &m_uiProfileStatsResetByGroupMenu;
    m_Pages[STATE_COMMUNITY_STATS_MAIN_MENU]  = &m_UiCommunityStatsMainMenu;
	m_Pages[STATE_PARTY_MAIN_MENU]			= &m_UiPartyMainMenu;
	m_Pages[STATE_ENTITLEMENT]				= &m_UiEntitlement;
	m_Pages[STATE_LIVESTREAM]				= &m_LiveStreamMenu;
	m_Pages[STATE_YOUTUBE]					= &m_YoutubeMenu;
    m_Pages[STATE_SEGUE]                    = &m_UiSegue;

    m_UiSegue.SetXY(200, 200);
}

bool 
MyNameFilter(const char* src,
             char* dest,
             const unsigned destSize)
{
    size_t len = src ? strlen(src) : 0;

    memset(dest, 0, destSize);

    for(size_t i = 0; (i < len); i++)
    {
        dest[i] = (char)toupper(src[i]);
    }
    
    return true;
}

void 
App::Init(sysMemAllocator* allocator,
            netConnectionManager* cxnMgr,
            void* voiceChatDevice)
{
    Assert(!m_Initialized);

	cxnMgr->GetSocketManager()->GetMainSocket()->RegisterPacketRecorder(&m_BandwidthRecorder);
    m_BandwidthRecorder.SetSampleInterval(5000);

    //Uncomment to test rlGamerInfo name filtering.
    //rlGamerInfo::SetDisplayNameFilter(MyNameFilter);

    snSessionOwner sessionOwner;

    sessionOwner.GetGamerData.Bind(this, &App::HandleGetGamerData);
    sessionOwner.HandleJoinRequest.Bind(this, &App::HandleJoinRequest);

    m_LiveMgr = rage_new LiveManager();
    m_LiveMgr->Init(allocator, cxnMgr, sessionOwner, voiceChatDevice);
    m_PeerMgr.Init(m_LiveMgr->GetCxnMgr(), LiveManager::GAME_CHANNEL_ID);
    
    m_UgcDlgt.Bind(this, &App::OnUgcQueryContent);

    rlProfileStats::EventDelegate dlgt;
    dlgt.Bind(this, &App::OnProfileStatsEvent);
    AssertVerify(rlProfileStats::Init(dlgt, 
                                      MAX_SUBMISSION_SIZE));

    this->PushState(STATE_FRONT_END);

#if __WIN32PC
	// PC Downloader Pipe
	g_rlPc.ConnectDownloaderPipe("\\\\.\\pipe\\GTAVLauncher_Pipe");
#endif

	rlRos::AddDelegate(&m_RosDlgt);
    rlPresence::AddDelegate(&m_PresenceDlgt);
    m_LiveMgr->GetSession()->AddDelegate(&m_SessionDlgt);
    m_LiveMgr->GetCxnMgr()->AddChannelDelegate(&m_CxnMgrDlgt, LiveManager::GAME_CHANNEL_ID);
	rlFriendsManager::AddDelegate(&m_FriendDlgt);

	// Livestream
#if LIVESTREAM_ENABLED
	rlLiveStream::AddDelegate(&m_LiveStreamDlgt);
	rlLiveStream::sm_VideoCaptureCallback = &App::VideoCaptureCallback;
#endif

#if RSG_PC
	InputInit();
#endif

	// Set up a system service delegate
	m_ServiceDelegate.Bind(this, &App::OnServiceEvent);
	g_SysService.AddDelegate(&m_ServiceDelegate);

	if(PARAM_notimeouts.Get())
	{
		netConnectionPolicies policies;
		m_LiveMgr->GetCxnMgr()->GetDefaultConnectionPolicies(LiveManager::GAME_CHANNEL_ID, &policies);
		policies.SetTimeout(0);
		m_LiveMgr->GetCxnMgr()->SetDefaultConnectionPolicies(policies);
		m_LiveMgr->GetCxnMgr()->GetDefaultConnectionPolicies(LiveManager::SESSION_CHANNEL_ID, &policies);
		policies.SetTimeout(0);
		m_LiveMgr->GetCxnMgr()->SetDefaultConnectionPolicies(policies);
	}

    Gamer* gamer = 0;

    for(unsigned i = 0; i < COUNTOF(m_LocalGamers); ++i)
    {
        if(rlPresence::IsSignedIn(i))
        {
            rlGamerInfo gamerInfo;
            rlPresence::GetGamerInfo(i, &gamerInfo);
            m_LocalGamers[i].Reset(gamerInfo);
            if(!gamer)
            {
                gamer = &m_LocalGamers[i];
            }
        }
        else
        {
            m_LocalGamers[i].Clear();
        }
    }

    this->SetGamer(gamer);

    m_UiGenericResult.Bind(this, &App::OnUiEvent);
    m_UiFrontEnd.Bind(this, &App::OnFeEvent);
    m_UiMultiplayerMainMenu.Bind(this, &App::OnUiEvent);
    m_UiHostConfig.Bind(this, &App::OnHostConfigEvent);
    m_UiQueryConfig.Bind(this, &App::OnQueryConfigEvent);
    m_UiSessionSelect.Bind(this, &App::OnSesionSelectEvent);
    m_UiSessionDetails.Bind(this, &App::OnSessionDetailsEvent);
    m_UiLobby.Bind(this, &App::OnLobbyEvent);
    m_UiGame.Bind(this, &App::OnGameEvent);
    m_UiConfigureAttrs.Bind(this, &App::OnConfigureAttrsEvent);
    m_UiFriendsList.Bind(this, &App::OnFriendsListEvent);
    m_UiInvitesReceived.Bind(this, &App::OnInvitesReceivedEvent);
    m_UiLeaderboardMainMenu.Bind(this, &App::OnUiEvent);
    m_UiLeaderboard2WriteMenu.Bind(this, &App::OnUiEvent);
    m_UiLeaderboard2ReadMenu.Bind(this, &App::OnUiEvent);
    m_UiLeaderboardMenu.Bind(this, &App::OnUiEvent);
    m_UiInboxMenu.Bind(this, &App::OnUiEvent);
    m_UiSocialClubMainMenu.Bind(this, &App::OnUiEvent);
	m_UiCommerceAndEntitlementMainMenu.Bind(this, &App::OnUiEvent);
	m_UiFacebookMainMenu.Bind(this, &App::OnUiEvent);
    m_UiCloudMainMenu.Bind(this, &App::OnUiEvent);
    m_UiUgcMainMenu.Bind(this, &App::OnUgcEvent);
    m_UiUgcContentList.Bind(this, &App::OnUgcEvent);
    m_UiUgcContentDetails.Bind(this, &App::OnUgcEvent);
    m_UiUgcPlayerList.Bind(this, &App::OnUgcEvent);
    m_UiPresenceMainMenu.Bind(this, &App::OnUiEvent);
	m_UiRichPresenceMainMenu.Bind(this, &App::OnUiEvent);
	m_UiAchievementsMainMenu.Bind(this, &App::OnUiEvent);
	m_UiProfileStatsMainMenu.Bind(this, &App::OnUiEvent);
	m_uiProfileStatsResetByGroupMenu.Bind(this, &App::OnUiEvent);
    m_UiCommunityStatsMainMenu.Bind(this, &App::OnUiEvent);
    m_UiClansMainMenu.Bind(this, &App::OnUiEvent);
    m_UiClansAllMenu.Bind(this, &App::OnUiEvent);
    m_UiClanRequestsMenu.Bind(this, &App::OnUiEvent);
    m_UiClanSentRequestsMenu.Bind(this, &App::OnUiEvent);
    m_UiClanMembersMenu.Bind(this, &App::OnUiEvent);
    m_UiClanRanksMenu.Bind(this, &App::OnUiEvent);
    m_UiClanInvitesMenu.Bind(this, &App::OnUiEvent);
    m_UiClanWallMessagesMenu.Bind(this, &App::OnUiEvent);
    m_UiClanInviteFriendMenu.Bind(this, &App::OnUiEvent);
	m_UiPartyMainMenu.Bind(this, &App::OnUiEvent);
	m_UiEntitlement.Bind(this, &App::OnUiEvent);
	m_LiveStreamMenu.Bind(this, &App::OnUiEvent);
	m_YoutubeMenu.Bind(this, &App::OnUiEvent);

    m_UiLobby.Init(this, m_LiveMgr);
    m_UiGame.Init(this);
    m_UiMultiplayerMainMenu.Init(m_LiveMgr->GetSession(), &m_LiveMgr->GetVoice());

    AssertVerify(g_SystemUi.Init());

    m_LbReadId = ~0u;

	m_Initialized = true;
}

void 
App::Shutdown()
{
    if(m_Initialized)
    {
        rlProfileStats::Shutdown();

        m_PeerMgr.Shutdown();
        g_SystemUi.Shutdown();

        m_UiFrontEnd.Shutdown();
        m_UiHostConfig.Shutdown();
        m_UiQueryConfig.Shutdown();
        m_UiSessionSelect.Shutdown();
        m_UiSessionDetails.Shutdown();
        m_UiLobby.Shutdown();
        m_UiGame.Shutdown();
        m_UiLeaderboardMainMenu.Shutdown();
        m_UiLeaderboard2WriteMenu.Shutdown();
        m_UiLeaderboard2ReadMenu.Shutdown();
        m_UiLeaderboardMenu.Shutdown();
		m_UiFriendsList.Shutdown();

        m_LiveMgr->GetCxnMgr()->RemoveChannelDelegate(&m_CxnMgrDlgt);
        m_LiveMgr->GetSession()->RemoveDelegate(&m_SessionDlgt);
		rlRos::RemoveDelegate(&m_RosDlgt);
        rlPresence::RemoveDelegate(&m_PresenceDlgt);
		rlFriendsManager::RemoveDelegate(&m_FriendDlgt);
#if LIVESTREAM_ENABLED
		rlLiveStream::RemoveDelegate(&m_LiveStreamDlgt);
#endif

		// Remove the service delegate
		g_SysService.RemoveDelegate(&m_ServiceDelegate);

        for(unsigned i = 0; i < COUNTOF(m_LocalGamers); ++i)
        {
            m_LocalGamers[i].Clear();
        }
		
		m_LiveMgr->GetCxnMgr()->GetSocketManager()->GetMainSocket()->UnregisterPacketRecorder(&m_BandwidthRecorder);

		m_LiveMgr->Shutdown();
        delete m_LiveMgr;
        m_LiveMgr = NULL;
        m_Gamer = NULL;

        m_LbReadId = ~0u;

        m_Initialized = false;

        m_StateStack.Reset();
    }
}

#if __WIN32PC
static bool IsForegroundProcess()
{
	DWORD pid = GetCurrentProcessId();
	HWND hwnd = GetForegroundWindow();
	if (hwnd == NULL) return false;

	DWORD foregroundPid;
	if (GetWindowThreadProcessId(hwnd, &foregroundPid) == 0) return false;

	return (foregroundPid == pid);
}
#endif

#if RSG_PC && !__FINAL
bool
App::UpdateAutomatedTest()
{
	enum State
	{
		STATE_WAITING_FOR_ONLINE,
		STATE_WAITING_FOR_START_TIME,
		STATE_NEXT_ACTION,
		STATE_WAIT_FOR_NEXT_ACTION,
		STATE_SEARCHING,
		STATE_SESSION_QUERYING_CONFIG,
		STATE_JOINING,
		STATE_HOSTING,
		STATE_LEAVING,
		STATE_QUIT
	};

	static State s_State = STATE_WAITING_FOR_ONLINE;
	static u64 s_StartTime = 0;
	static u64 s_EndTime = 0;
	static u64 s_NextActionTime = 0;
	static netRandom s_Rng((int)GetCurrentProcessId() + (int)timeGetTime());
	u64 curPosixTime = rlGetPosixTime();
	s_AutoTestString[0] = '\0';

	if((s_StartTime == 0) || (s_EndTime == 0))
	{
		const char* timeParam = "";
		PARAM_takehometest.Get(timeParam);
		if(sscanf(timeParam, "%llu,%llu", &s_StartTime, &s_EndTime) != 2)
		{
			s_StartTime = 1;
			s_EndTime = 0xFFFFFFFFFFFFFFFE;
			//s_State = STATE_QUIT;
			s_State = STATE_WAITING_FOR_ONLINE;
		}
		else
		{
			if(s_StartTime == 0 || s_EndTime == 0 || s_StartTime >= s_EndTime || s_EndTime <= curPosixTime)
			{
				s_State = STATE_QUIT;
			}
			else
			{
				s_State = STATE_WAITING_FOR_ONLINE;
			}
		}
	}

	if((s_EndTime != 0) && (curPosixTime >= s_EndTime))
	{
		MessageBoxA(NULL, "Test is complete. Please collect your log.", "Sample Session", 0);
		return true;
	}

	switch(s_State)
	{
		case STATE_WAITING_FOR_ONLINE:
		{
			formatf(s_AutoTestString, "Take Home Test: Waiting for sign in.");
			if(g_rlPc.IsUiAcceptingCommands())
			{
				if(g_rlPc.GetProfileManager() && g_rlPc.GetProfileManager()->IsOnline())
				{
					formatf(s_AutoTestString, "Take Home Test: Waiting for relay connection.");

					if(rlRos::IsAnyOnline() && netRelay::IsConnectedToRelay())
					{
						s_State = STATE_WAITING_FOR_START_TIME;
					}
				}
			}
		}
		break;
		case STATE_WAITING_FOR_START_TIME:
		{
			formatf(s_AutoTestString, "Take Home Test: Will start in %u seconds", s_StartTime - curPosixTime);

			if(curPosixTime >= s_StartTime)
			{
				s_State = STATE_NEXT_ACTION;
			}
		}
		break;
		case STATE_NEXT_ACTION:
		{
			if(!m_LiveMgr->GetSession()->Exists())
			{
				this->PopAllStates();

				s_NextActionTime = curPosixTime + (unsigned)s_Rng.GetRanged(10 / 2, 30 / 2); // next action in 10-30 seconds
			}
			else if(m_LiveMgr->GetSession()->IsHost())
			{
				s_NextActionTime = curPosixTime + (unsigned)s_Rng.GetRanged(60 * 2, 60 * 5); // next action in 1-5 mins
			}
			else
			{
				s_NextActionTime = curPosixTime + (unsigned)s_Rng.GetRanged(60 / 5, 60 * 5 / 5); // next action in 1-5 mins
			}

			s_State = STATE_WAIT_FOR_NEXT_ACTION;
		}
		break;
		case STATE_WAIT_FOR_NEXT_ACTION:
		{
			formatf(s_AutoTestString, "Take Home Test: Next action in %u seconds. Test will end in %u seconds.", s_NextActionTime - curPosixTime, s_EndTime - curPosixTime);

			if(curPosixTime >= s_NextActionTime)
			{
				if(m_LiveMgr->GetSession()->Exists())
				{
				    m_LeavingByChoice = true;

					if(m_LiveMgr->GetSession()->Exists())
					{
						if(m_LiveMgr->GetSession()->Leave(m_Gamer->GetLocalIndex(), &m_MyStatus))
						{
							this->PushSegue(SEGUE_SESSION_LEAVING);
							s_State = STATE_LEAVING;
						}
						else
						{
							//this->PushResult("Error leaving session");
							s_State = STATE_NEXT_ACTION;
						}
					}
					else
					{
						s_State = STATE_NEXT_ACTION;
					}
				}
				else
				{
					if(m_LiveMgr->FindSessions(RL_NETMODE_ONLINE,
												m_Gamer,
												1,
												m_UiQueryConfig.GetFilter(),
												m_SessionDetails,
												MAX_SEARCH_RESULTS,
												0,
												&m_NumSearchResults,
												&m_MyStatus))
					{
						this->PushSegue(SEGUE_SESSION_SEARCHING);
						s_State = STATE_SEARCHING;
					}
					else
					{
						s_State = STATE_NEXT_ACTION;
					}
				}
			}
		}
		break;
		case STATE_SEARCHING:
		{
			if(!m_MyStatus.Pending())
			{
				this->PopSegue();

				if(m_MyStatus.Failed())
				{
					if(m_LiveMgr->HostSession(m_Gamer,
													RL_NETMODE_ONLINE,
													m_UiHostConfig.GetNumPublicSlots(),
													m_UiHostConfig.GetNumPrivateSlots(),
													m_UiHostConfig.GetAttrs(),
													m_UiHostConfig.GetCreateFlags(),
													&m_MyStatus))
					{
						this->PushSegue(SEGUE_SESSION_HOSTING);
						s_State = STATE_HOSTING;
					}
					else
					{
						//this->PushResult("Error hosting session");
						s_State = STATE_NEXT_ACTION;
					}
				}
				if(m_MyStatus.Succeeded())
				{
					if(m_NumSearchResults == 0)
					{
						if(m_LiveMgr->HostSession(m_Gamer,
														RL_NETMODE_ONLINE,
														m_UiHostConfig.GetNumPublicSlots(),
														m_UiHostConfig.GetNumPrivateSlots(),
														m_UiHostConfig.GetAttrs(),
														m_UiHostConfig.GetCreateFlags(),
														&m_MyStatus))
						{
							this->PushSegue(SEGUE_SESSION_HOSTING);
							s_State = STATE_HOSTING;
						}
						else
						{
							//this->PushResult("Error hosting session");
							s_State = STATE_NEXT_ACTION;
						}
					}
					else
					{
						const int sessionIdx = (unsigned)s_Rng.GetRanged(0, m_NumSearchResults - 1);
						if(sessionIdx >= 0 && sessionIdx < (int) m_NumSearchResults)
						{
							const rlSessionDetail& detail = m_SessionDetails[sessionIdx];

							if(STATE_MULTIPLAYER_MAIN_MENU != this->GetState())
							{
								this->PopAllStates();
								this->PushState(STATE_MULTIPLAYER_MAIN_MENU);
							}

							m_Joiner = m_Gamer;
							m_SlotType = RL_SLOT_PUBLIC;
							m_JoinSessionInfo = detail.m_SessionInfo;
							m_NetMode = detail.m_SessionConfig.m_NetworkMode;
							m_JoinSessionDetail.Clear();

							if(rlSessionManager::QueryDetail(RL_NETMODE_ONLINE,
																	LiveManager::SESSION_CHANNEL_ID,
																	rlSessionManager::HOST_QUERY_DEFAULT_TIMEOUT_MS,
																	rlSessionManager::HOST_QUERY_DEFAULT_MAX_ATTEMPTS,
																	rlSessionManager::DEFAULT_NUM_QUERY_RESULTS_BEFORE_EARLY_OUT,
																	true,
																	&m_JoinSessionInfo,
																	1,
																	&m_JoinSessionDetail,
																	&m_NumJoinSessionDetailResults,
																	&m_MyStatus))
							{
								this->PushSegue(SEGUE_SESSION_QUERYING_CONFIG);
								s_State = STATE_SESSION_QUERYING_CONFIG;
							}
							else
							{
								s_State = STATE_NEXT_ACTION;
							}

						}
					}
				}
			}
		}
		break;
		case STATE_SESSION_QUERYING_CONFIG:
		{
			if(!m_MyStatus.Pending())
			{
				this->PopSegue();

				if(m_MyStatus.Succeeded())
				{
					const unsigned flags = 0;

					if(m_JoinSessionDetail.m_SessionConfig.m_SessionId == 0)
					{
						s_State = STATE_NEXT_ACTION;
					}
					else if(m_LiveMgr->JoinSession(m_Joiner,
													m_JoinSessionInfo,
													m_JoinSessionDetail.m_SessionConfig.m_NetworkMode,
													m_SlotType,
													flags,
													&m_MyStatus))
					{
						this->PushSegue(SEGUE_SESSION_JOINING);
						s_State = STATE_JOINING;
					}
					else
					{
						//this->PushResult("Error joining session");
						s_State = STATE_NEXT_ACTION;
					}
				}
				else
				{
					s_State = STATE_NEXT_ACTION;
				}
			}
		}
		break;
		case STATE_JOINING:
		{
			if(!m_MyStatus.Pending())
			{
				this->PopSegue();
				if(m_MyStatus.Succeeded())
				{
					this->PushState(STATE_LOBBY);
					s_State = STATE_NEXT_ACTION;
				}
				else
				{
					s_State = STATE_NEXT_ACTION;
				}
			}
		}
		break;
		case STATE_HOSTING:
		{
			if(!m_MyStatus.Pending())
			{
				this->PopSegue();

				if(m_MyStatus.Succeeded())
				{
					this->PushState(STATE_LOBBY);
					s_State = STATE_NEXT_ACTION;
				}
				else
				{
					s_State = STATE_NEXT_ACTION;
				}
			}
		}
		break;
		case STATE_LEAVING:
		{
			if(!m_MyStatus.Pending())
			{
				rlTelemetry::Flush();

				this->PopSegue();

				if(m_Gamer)
				{
					m_Gamer->ClearStats();
				}

				this->DestroySession();

				this->PopAllStates();

				s_State = STATE_NEXT_ACTION;
			}
		}
		break;
		case STATE_QUIT:
		{
			MessageBoxA(NULL, "Error Occurred. Please collect your log.", "Sample Session", 0);
			return true;
		}
		break;
	}

	return false;
}
#endif

bool
App::Update()
{
    unsigned curTime = sysTimer::GetSystemMsTime();

#if RSG_PC && !__FINAL
	if(PARAM_takehometest.Get())
	{
		m_Done = UpdateAutomatedTest();
	}
#endif

#if RSG_PC
	static bool showScui = true;
	if(showScui)
	{
		if(g_rlPc.IsUiAcceptingCommands())
		{
			if(!g_rlPc.IsUiShowing() &&
				!PARAM_scnoui.Get() &&
				g_rlPc.GetProfileManager()->IsSigningIn() == false &&
				g_rlPc.GetProfileManager()->IsSignedIn() == false)
			{
				g_rlPc.ShowSigninUi();
			}
						
			showScui = false;
		}
	}
#endif

    m_LiveMgr->Update(curTime);

    //Update core systems
	g_SystemUi.Update();
	g_SysService.UpdateClass();

    rlProfileStats::Update();
    m_BandwidthRecorder.Update(curTime);

	sm_rlAchievement.Update();

	sysUserList::GetInstance().Update();

#if __STEAM_BUILD
	SteamAPI_RunCallbacks();
#endif

    //Handle input events
	bool ignoreInput = false;

#if __WIN32PC
	ignoreInput = g_SystemUi.IsUiShowing();
#endif

	rage::INPUT.Update(false, ignoreInput, ignoreInput);

#if RSG_PC
	InputUpdate();
#endif // RSG_PC

	if(m_LiveMgr->GetSession()->Exists())
    {
#if RSG_ORBIS
		g_rlNp.GetNpAuth().NotifyPremiumFeature(m_Gamer->GetLocalIndex(), rlRealtimeMultiplayerProperty::Property_None);
#endif

        const snSession* session = m_LiveMgr->GetSession();

        m_UiConfigureAttrs.SetChangeable(session->IsHost());
        m_UiLobby.SetInvitable(session->IsInvitable());
		m_UiLobby.SetBecomeHostOptionEnabled(session->IsHost() == false);
        m_UiLobby.SetInviteToggleEnabled(session->IsHost()
                                        && session->IsOnline());
        m_UiLobby.SetBandwidth(m_BandwidthRecorder.GetInboundBandwidth(),
                               m_BandwidthRecorder.GetInboundPacketFreq(),
                               m_BandwidthRecorder.GetOutboundBandwidth(),
                               m_BandwidthRecorder.GetOutboundPacketFreq());
    }

    const State state = this->GetState();
    UiElement* page = state >= 0 ? m_Pages[state] : 0;

	bool isForegroundProcess = true;
#if __WIN32PC && __DEV
// 	if(!PARAM_allowbackgroundinput.Get())
// 	{
		isForegroundProcess = IsForegroundProcess();
//	}
#endif

	static bool reverseInput = false;
	if(ioKeyboard::KeyPressed(KEY_Z))
	{
		reverseInput = !reverseInput;
	}

    if(page && ((isForegroundProcess && (reverseInput == false)) || ((isForegroundProcess == false) && (reverseInput == true))))
    {
        u32 nPressed = ioPad::GetPad(0).GetPressedButtons();
		if (ignoreInput)
			nPressed = 0;

        if((nPressed & ioPad::LUP) || ioKeyboard::KeyPressed(KEY_UP))
        {
            page->OnUp();
        }
        else if((nPressed & ioPad::LRIGHT) || ioKeyboard::KeyPressed(KEY_RIGHT))
        {
            page->OnRight();
        }
        else if((nPressed & ioPad::LDOWN) || ioKeyboard::KeyPressed(KEY_DOWN))
        {
            page->OnDown();
        }
        else if((nPressed & ioPad::LLEFT) || ioKeyboard::KeyPressed(KEY_LEFT))
        {
			page->OnLeft();
        }
        else if((nPressed & ioPad::RDOWN) || ioKeyboard::KeyPressed(KEY_RETURN))
        {
            page->OnAction();
			
        }
        else if(nPressed & ioPad::RRIGHT || ioKeyboard::KeyPressed(KEY_ESCAPE))
        {
            page->OnBack();
    	}
		else if (nPressed & ioPad::L2 || ioKeyboard::KeyPressed(KEY_NUMPAD0))
		{
			page->OnPageLeft();
		}
		else if (nPressed & ioPad::R2 || ioKeyboard::KeyPressed(KEY_NUMPAD1))
		{
			page->OnPageRight();
		}

#if RSG_DURANGO
		// Suspend Mode
		u32 nHeld = ioPad::GetPad(0).GetButtons();
		if (ignoreInput)
			nHeld = 0;

		if (nHeld & ioPad::L2 && nHeld & ioPad::R2)
		{
			sysServiceEvent e(sysServiceEvent::SUSPENDED);
			g_SysService.TriggerEvent(&e);
		}
		else if (nHeld & ioPad::L1 && nHeld & ioPad::R1)
		{
			sysServiceEvent e(sysServiceEvent::RESUMING);
			g_SysService.TriggerEvent(&e);
		}
#endif
	}

#if RSG_PC
	// tab toggles between voice activated and push-to-talk
	// in push-to-talk mode, tilde (~) is the push-to-talk key
	static VoiceChat::CaptureMode mode = VoiceChat::CAPTURE_MODE_VOICE_ACTIVATED;
	if(ioKeyboard::KeyPressed(KEY_TAB))
	{
		if(mode == VoiceChat::CAPTURE_MODE_VOICE_ACTIVATED)
		{
			mode = VoiceChat::CAPTURE_MODE_PUSH_TO_TALK;
		}
		else
		{
			mode = VoiceChat::CAPTURE_MODE_VOICE_ACTIVATED;
		}
		m_LiveMgr->GetVoice().SetCaptureMode(mode);
	}

	if(mode == VoiceChat::CAPTURE_MODE_PUSH_TO_TALK)
	{
		// push tilde to talk
		m_LiveMgr->GetVoice().SetPushToTalk(ioKeyboard::KeyUp(KEY_GRAVE) == 0);
	}

	if (g_rlPc.HasUserRequestedShutdownViaScui())
	{
		m_Done = true;
	}
#endif

    const bool hasNetwork = rlPeerAddress::HasNetwork();
    const bool isSignedIn = m_Gamer && m_Gamer->IsSignedIn();
    const bool isOnline = m_Gamer && m_Gamer->IsOnline() && 
							((netRelay::GetMyRelayAddress().IsValid() && (netHardware::GetNatType() != NET_NAT_UNKNOWN)) || netNatDetector::HasNatTypeDiscoveryFailed());
    const bool hasInvites = isOnline && m_UiInvitesReceived.HasInvites();

    m_UiFrontEnd.UpdateEnabled(hasNetwork, isSignedIn, isOnline, hasInvites);
    m_UiMultiplayerMainMenu.UpdateEnabled(hasNetwork, isSignedIn, isOnline);
	m_UiFriendsList.Update();

    const Segue segue = this->GetSegue();

#if RSG_PC && !__FINAL
	if(PARAM_takehometest.Get())

	{
		return m_Done;
	}
#endif

    if(STATE_MULTIPLAYER_MAIN_MENU == state
        || STATE_LOBBY == state
        || STATE_GAME == state)
    {
        m_UiFriendsList.SetAllowInvites(m_LiveMgr->GetSession()->IsInvitable());
    }


    switch(int(segue))
    {
    case SEGUE_GENERIC:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();
            m_UiGenericResult.AddText(m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
            this->PushState(STATE_GENERIC_RESULT);
        }
        break;

    case SEGUE_SESSION_HOSTING:
 		if(!m_MyStatus.Pending())
         {
			 this->PopSegue();
 
             if(m_MyStatus.Succeeded())
             {
                 //Go to lobby
                 this->PushState(STATE_LOBBY);
             }
         }
        break;
    case SEGUE_SESSION_SEARCHING:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                m_UiSessionSelect.Clear();
                
                for(int i = 0; i < (int) m_NumSearchResults; ++i)
                {
                    m_UiSessionSelect.AddSession(i, m_SessionDetails[i].m_HostName);
                }

                this->PushState(STATE_SELECTING_SESSION);
            }
        }
        break;
    case SEGUE_SESSION_QUERYING_CONFIG:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                const unsigned flags = 0;

                Assert(m_Joiner && m_Joiner->IsSignedIn());

				if(m_JoinSessionDetail.m_SessionConfig.m_SessionId == 0)
				{
					this->PushResult("Error joining session");
				}
                else if(m_LiveMgr->JoinSession(m_Joiner,
                                                m_JoinSessionInfo,
                                                m_JoinSessionDetail.m_SessionConfig.m_NetworkMode,
                                                m_SlotType,
                                                flags,
                                                &m_MyStatus))
                {
                    this->PushSegue(SEGUE_SESSION_JOINING);
                }
                else
                {
                    this->PushResult("Error joining session");
                }
            }
            else
            {
                this->PushResult("Failure");
            }
        }
        break;
    case SEGUE_SESSION_JOINING:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();
            if(STATE_SELECTING_SESSION == this->GetState())
            {
                this->PopState();
            }

            if(m_MyStatus.Succeeded())
            {
                this->PushState(STATE_LOBBY);
            }
        }
        break;
	case SEGUE_SESSION_JVP_LOOKUP:
		if (!m_MyStatus.Pending())
		{
			this->PopSegue();

			if (m_MyStatus.Succeeded() && m_NumSessionFound > 0 && m_SessionQuery.m_SessionInfo.IsValid())
			{
				this->JoinSession(m_Gamer, m_SessionQuery.m_SessionInfo, RL_NETMODE_ONLINE, RL_SLOT_PUBLIC);
			}
		}
		break;
    case SEGUE_SESSION_CHANGING_ATTRIBUTES:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();
        }
        break;
    case SEGUE_SESSION_TOGGLING_INVITABLE:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();
        }
        break;
    case SEGUE_SESSION_LEAVING:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_Gamer)
            {
                m_Gamer->ClearStats();
            }

            this->DestroySession();
        }
        break;
    case SEGUE_SESSION_LEAVING_TO_JOIN_ANOTHER:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();
            this->JoinSession(m_Gamer, m_JoinSessionInfo, m_NetMode, m_SlotType);
        }
        break;
    case SEGUE_PARTY_LEAVING_GAME_TO_JOIN_ANOTHER:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();
            this->JoinSession(m_Gamer, m_JoinSessionInfo, m_NetMode, m_SlotType);
        }
        break;
    case SEGUE_SESSION_DESTROYING:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_Gamer)
            {
                m_Gamer->ClearStats();
            }

            Assert(m_MyStatus.Succeeded());

            //FIXME (KB) - this is a hack
            //If we destroyed ourselves in response to being
            //kicked then reset the peer manager.
            m_PeerMgr.Shutdown();
            m_PeerMgr.Init(m_LiveMgr->GetCxnMgr(), LiveManager::GAME_CHANNEL_ID);
        }
        break;
    case SEGUE_SESSION_STARTING:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                this->PushState(STATE_GAME);
            }
        }
        break;
    case SEGUE_SESSION_ENDING:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();
            this->PopState();

            m_Gamer->ClearStats();
        }
        break;
    case SEGUE_GETTING_LEADERBOARD_SIZE:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                Displayf("Got size of leaderboard 0x%08x: %d rows", 
                         m_LbReadId,
                         m_TotalLbRows);

                char buf[256];
                formatf(buf, sizeof(buf), "Size of leaderboard 0x%08x: %d rows", 
                        m_LbReadId,
                        m_TotalLbRows);

                this->PushResult(buf);
            }
            else
            {
                this->PushResult("Error getting leaderboard size");
            }
        }
        break;

    case SEGUE_WRITING_LEADERBOARD2:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                this->PushResult("Write leaderboard succeeded");
            }
            else
            {
                this->PushResult("Error writing leaderboard");
            }
        }
        break;

    case SEGUE_READING_LEADERBOARD2:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                //this->PushResult("Read leaderboard succeeded");
                LeaderboardInfo lbInfo;
                LeaderboardInfo::GetLeaderboardInfo(m_LbReadId, &lbInfo);

                Displayf("Found %d rows", m_NumLbRowsReturned);

                m_UiGenericResult.ClearText();

                for(int i = 0; i < (int) m_NumLbRowsReturned; i++)
                {
                    const rlLeaderboard2Row& r = m_Lb2Rows[i];
                    char label[256] = {""};
                    char buf[256] = {""};
                    /*if(r.m_GroupSelector.IsValid())
                    {
                        formatf(label, "%d. %-20s (%s)%-20s %4s",
                                r.Rank,
                                r.GamerDisplayName,
                                r.RowId.GroupHandle.Handle,
                                r.GroupDisplayName,
                                r.GroupDisplayTag);
                    }
                    else*/
                    {
                        formatf(buf, "%s[%s]",
                                r.m_GamerDisplayName,
                                r.m_ClanName);
                        formatf(label, "%d. %-20s",
                                r.m_Rank, buf);
                    }

                    //We mixed up the columns when we performed the read
                    //by adding 3 and modding by the number of columns.
                    //Do the same when displaying the LBs.

                    switch(m_LbReadId)
                    {
                    case Leaderboard_SwingsByStrikes2::LEADERBOARD_ID:
                        formatf(buf, sizeof(buf), "%s (SW=%" I64FMT "u SK=%" I64FMT "u 1B=%" I64FMT "u 2B=%" I64FMT "u 3B=%" I64FMT "u HR=%" I64FMT "u)",
                                label,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_SwingsByStrikes::COLUMN_ID_SWINGS)+3)%lbInfo.NumColumns].Int64Val,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_SwingsByStrikes::COLUMN_ID_STRIKES)+3)%lbInfo.NumColumns].Int64Val,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_SwingsByStrikes::COLUMN_ID_SINGLES)+3)%lbInfo.NumColumns].Int64Val,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_SwingsByStrikes::COLUMN_ID_DOUBLES)+3)%lbInfo.NumColumns].Int64Val,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_SwingsByStrikes::COLUMN_ID_TRIPLES)+3)%lbInfo.NumColumns].Int64Val,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_SwingsByStrikes::COLUMN_ID_HOME_RUNS)+3)%lbInfo.NumColumns].Int64Val);
                        break;
                    case Leaderboard_Hits::LEADERBOARD_ID:
                        formatf(buf, sizeof(buf), "%s (strikes=%" I64FMT "u  maxstrikes=%" I64FMT "u)", 
                                label,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_Hits::COLUMN_ID_HITS)+3)%lbInfo.NumColumns].Int64Val,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_Hits::COLUMN_ID_MAX_HITS)+3)%lbInfo.NumColumns].Int64Val);
                        break;
                    case Leaderboard_ArbHits::LEADERBOARD_ID:
                        formatf(buf, sizeof(buf), "%s (strikes=%" I64FMT "u  maxstrikes=%" I64FMT "u)", 
                                label,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_ArbHits::COLUMN_ID_HITS)+3)%lbInfo.NumColumns].Int64Val,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_ArbHits::COLUMN_ID_MAX_HITS)+3)%lbInfo.NumColumns].Int64Val);
                        break;
                    case SkillLeaderboard_ExhibitionRanked::LEADERBOARD_ID:
                        formatf(buf, sizeof(buf), "%s (skill=%" I64FMT "u gamesplayed=%" I64FMT "u mu=%f sigma=%f)", 
                                label,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionRanked::COLUMN_ID_SKILL)+3)%lbInfo.NumColumns].Int64Val,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionRanked::COLUMN_ID_GAMESPLAYED)+3)%lbInfo.NumColumns].Int64Val,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionRanked::COLUMN_ID_MU)+3)%lbInfo.NumColumns].DoubleVal,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionRanked::COLUMN_ID_SIGMA)+3)%lbInfo.NumColumns].DoubleVal);
                        break;
                    case SkillLeaderboard_ExhibitionStandard::LEADERBOARD_ID:
                        formatf(buf, sizeof(buf), "%s (skill=%" I64FMT "u gamesplayed=%" I64FMT "u mu=%f sigma=%f)", 
                                label,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionStandard::COLUMN_ID_SKILL)+3)%lbInfo.NumColumns].Int64Val,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionStandard::COLUMN_ID_GAMESPLAYED)+3)%lbInfo.NumColumns].Int64Val,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionStandard::COLUMN_ID_MU)+3)%lbInfo.NumColumns].DoubleVal,
                                r.m_ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionStandard::COLUMN_ID_SIGMA)+3)%lbInfo.NumColumns].DoubleVal);
                        break;
                    }

                    m_UiGenericResult.AddText(buf);
                    //Displayf(buf);
                }

                this->PushState(STATE_GENERIC_RESULT);
            }
            else
            {
                this->PushResult("Error reading leaderboard");
            }
        }
        break;

    case SEGUE_READING_STATS:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                LeaderboardInfo lbInfo;
                LeaderboardInfo::GetLeaderboardInfo(m_LbReadId, &lbInfo);

                Displayf("Found %d rows", m_NumLbRowsReturned);

                m_UiGenericResult.ClearText();

                for(int i = 0; i < (int) m_NumLbRowsReturned; i++)
                {
                    const rlLeaderboardRow& r = m_LbRows[i];
                    char label[256] = {""};
                    char buf[256] = {""};
                    if(r.RowId.GroupHandle.IsValid())
                    {
                        formatf(label, "%d. %-20s (%s)%-20s %4s",
                                r.Rank,
                                r.GamerDisplayName,
                                r.RowId.GroupHandle.Handle,
                                r.GroupDisplayName,
                                r.GroupDisplayTag);
                    }
                    else
                    {
                        formatf(label, "%d. %-20s",
                                r.Rank,
                                r.GamerDisplayName);
                    }

                    //We mixed up the columns when we performed the read
                    //by adding 3 and modding by the number of columns.
                    //Do the same when displaying the LBs.

                    switch(m_LbReadId)
                    {
                    case Leaderboard_SwingsByStrikes::LEADERBOARD_ID:
                        formatf(buf, sizeof(buf), "%s (SW=%" I64FMT "u SK=%" I64FMT "u 1B=%" I64FMT "u 2B=%" I64FMT "u 3B=%" I64FMT "u HR=%" I64FMT "u)",
                                label,
                                r.ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_SwingsByStrikes::COLUMN_ID_SWINGS)+3)%lbInfo.NumColumns].Int64Val,
                                r.ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_SwingsByStrikes::COLUMN_ID_STRIKES)+3)%lbInfo.NumColumns].Int64Val,
                                r.ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_SwingsByStrikes::COLUMN_ID_SINGLES)+3)%lbInfo.NumColumns].Int64Val,
                                r.ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_SwingsByStrikes::COLUMN_ID_DOUBLES)+3)%lbInfo.NumColumns].Int64Val,
                                r.ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_SwingsByStrikes::COLUMN_ID_TRIPLES)+3)%lbInfo.NumColumns].Int64Val,
                                r.ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_SwingsByStrikes::COLUMN_ID_HOME_RUNS)+3)%lbInfo.NumColumns].Int64Val);
                        break;
                    case Leaderboard_Hits::LEADERBOARD_ID:
                        formatf(buf, sizeof(buf), "%s (strikes=%" I64FMT "u  maxstrikes=%" I64FMT "u)", 
                                label,
                                r.ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_Hits::COLUMN_ID_HITS)+3)%lbInfo.NumColumns].Int64Val,
                                r.ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_Hits::COLUMN_ID_MAX_HITS)+3)%lbInfo.NumColumns].Int64Val);
                        break;
                    case Leaderboard_ArbHits::LEADERBOARD_ID:
                        formatf(buf, sizeof(buf), "%s (strikes=%" I64FMT "u  maxstrikes=%" I64FMT "u)", 
                                label,
                                r.ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_ArbHits::COLUMN_ID_HITS)+3)%lbInfo.NumColumns].Int64Val,
                                r.ColumnValues[(lbInfo.GetColumnIndex(Leaderboard_ArbHits::COLUMN_ID_MAX_HITS)+3)%lbInfo.NumColumns].Int64Val);
                        break;
                    case SkillLeaderboard_ExhibitionRanked::LEADERBOARD_ID:
                        formatf(buf, sizeof(buf), "%s (skill=%" I64FMT "u gamesplayed=%" I64FMT "u mu=%f sigma=%f)", 
                                label,
                                r.ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionRanked::COLUMN_ID_SKILL)+3)%lbInfo.NumColumns].Int64Val,
                                r.ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionRanked::COLUMN_ID_GAMESPLAYED)+3)%lbInfo.NumColumns].Int64Val,
                                r.ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionRanked::COLUMN_ID_MU)+3)%lbInfo.NumColumns].DoubleVal,
                                r.ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionRanked::COLUMN_ID_SIGMA)+3)%lbInfo.NumColumns].DoubleVal);
                        break;
                    case SkillLeaderboard_ExhibitionStandard::LEADERBOARD_ID:
                        formatf(buf, sizeof(buf), "%s (skill=%" I64FMT "u gamesplayed=%" I64FMT "u mu=%f sigma=%f)", 
                                label,
                                r.ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionStandard::COLUMN_ID_SKILL)+3)%lbInfo.NumColumns].Int64Val,
                                r.ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionStandard::COLUMN_ID_GAMESPLAYED)+3)%lbInfo.NumColumns].Int64Val,
                                r.ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionStandard::COLUMN_ID_MU)+3)%lbInfo.NumColumns].DoubleVal,
                                r.ColumnValues[(lbInfo.GetColumnIndex(SkillLeaderboard_ExhibitionStandard::COLUMN_ID_SIGMA)+3)%lbInfo.NumColumns].DoubleVal);
                        break;
                    }

                    m_UiGenericResult.AddText(buf);
                    //Displayf(buf);
                }

                this->PushState(STATE_GENERIC_RESULT);
            }
            else
            {
                this->PushResult("Error reading stats");
            }
        }
        break;
    case SEGUE_READING_INBOX_MESSAGES:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(!m_MyStatus.Succeeded())
            {
                this->PushResult("Failed to read inbox messages");
            }
            else
            {
                m_UiInboxMenu.ClearMessages();

                const char* msg;
                const char* msgId;
                u64 createPosixTime, readPosixTime;
                while(m_InboxMessageIter.NextMessage(&msg, &msgId, &createPosixTime, &readPosixTime))
                {
                    m_UiInboxMenu.AddMessage(createPosixTime, msg);
                }
            }
        }
        break;
    case SEGUE_PARTY_HOSTING:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(!m_MyStatus.Succeeded())
            {
                this->PushResult("Failed to host party");
            }
#if RSG_NP
            else
            {
                rlPresence::SetStatusString("In a party");
            }
#endif
        }
        break;
    case SEGUE_PARTY_JOINING:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(!m_MyStatus.Succeeded())
            {
                this->PushResult("Failed to join party");
            }
#if RSG_NP
            else
            {
                rlPresence::SetStatusString("In a party");
            }
#endif
        }
        break;
    case SEGUE_PARTY_LEAVING:
    case SEGUE_PARTY_LEAVING_TO_LEAVE_GAME:
    case SEGUE_PARTY_LEAVING_TO_HOST_GAME:
        if(!m_MyStatus.Pending())
        {
            const Segue segue = this->GetSegue();
            this->PopSegue();
#if RSG_NP
            rlPresence::SetStatusString(NULL);
#endif

            if(SEGUE_PARTY_LEAVING_TO_LEAVE_GAME == segue)
            {
                this->LeaveSession(m_Gamer);
            }
            else if(SEGUE_PARTY_LEAVING_TO_HOST_GAME == segue)
            {
                this->HostSession();
            }
            else if(!m_MyStatus.Succeeded())
            {
                this->PushResult("Failed to leave party");
            }
        }
        break;
    case SEGUE_SOCIAL_CLUB_GET_TERMS_OF_SERVICE:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                char buf[1024];
                formatf(buf, sizeof(buf), "Retrieved Social Club Terms of Service version %d (see debug output)", g_SocialClubTermsOfServiceVersion);
                this->PushResult(buf);
                Displayf("Terms of Service version %d", g_SocialClubTermsOfServiceVersion);
            }
            else
            {
                char buf[256];
                formatf(buf, sizeof(buf), "Failed to retrieve Social Club Terms of Service (result code %d)", m_MyStatus.GetResultCode());
                this->PushResult(buf);
            }
        }
        break;
    case SEGUE_SOCIAL_CLUB_LINK_OR_UNLINK_ACCOUNT:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                if(rlSocialClub::IsConnectedToSocialClub(m_Gamer->GetLocalIndex()))
                {
                    this->PushResult("Social Club account linked. You are now logged in.");
                }
                else
                {
                    this->PushResult("Social Club account unlinked.");
                }

            }
            else
            {
                char buf[256];

                if(m_MyStatus.GetResultCode() == RLSC_ERROR_AUTHENTICATIONFAILED_TICKET)
                {
                    formatf(buf, sizeof(buf), "Social Club account linking failed (bad credentials).");
                }
                else
                {
                    formatf(buf, sizeof(buf), "Social Club account linking failed (result code %d)", m_MyStatus.GetResultCode());
                }

                this->PushResult(buf);
            }
        }
        break;
    case SEGUE_SOCIAL_CLUB_CREATE_ACCOUNT:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                this->PushResult("Social Club account creation succeeded. You are now logged in.");
            }
            else
            {
                char buf[256];

                if(m_MyStatus.GetResultCode() == RLSC_ERROR_PROFANE_NICKNAME)
                {
                    formatf(buf, sizeof(buf), "Social Club account creation failed (nickname contains inappropriate language).");
                }
                else if(m_MyStatus.GetResultCode() == RLSC_ERROR_INVALIDARGUMENT_NICKNAME)
                {
                    formatf(buf, sizeof(buf), "Social Club account creation failed (invalid nickname).");
                }
                else if(m_MyStatus.GetResultCode() == RLSC_ERROR_ALREADYEXISTS_NICKNAME)
                {
                    formatf(buf, sizeof(buf), "Social Club account creation failed (nickname is already taken).");
                }
                else if(m_MyStatus.GetResultCode() == RLSC_ERROR_INVALIDARGUMENT_EMAIL)
                {
                    formatf(buf, sizeof(buf), "Social Club account creation failed (invalid e-mail address).");
                }
                else if(m_MyStatus.GetResultCode() == RLSC_ERROR_ALREADYEXISTS_EMAIL)
                {
                    formatf(buf, sizeof(buf), "Social Club account creation failed (e-mail address already exists).");
                }
                else
                {
                    formatf(buf, sizeof(buf), "Social Club account creation failed (result code %d)", m_MyStatus.GetResultCode());
                }
                this->PushResult(buf);
            }
        }
        break;

    case SEGUE_SOCIAL_CLUB_ACCEPT_TERMS_OF_SERVICE:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                char buf[128];
                formatf(buf, sizeof(buf), "Successfully accepted Social Club Terms of Service version %d", g_SocialClubTermsOfServiceVersion);
                this->PushResult(buf);
            }
            else
            {
                char buf[128];
                formatf(buf, sizeof(buf), "Failed to accept Social Club Terms of Service (result code %d)", m_MyStatus.GetResultCode());
                this->PushResult(buf);
            }
        }
        break;

    case SEGUE_SOCIAL_CLUB_RESET_PASSWORD:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                this->PushResult("A reset password email has been sent to the specified e-mail address.");
            }
            else
            {
                char buf[256];

                if(m_MyStatus.GetResultCode() == RLSC_ERROR_DOESNOTEXIST_ROCKSTARACCOUNT)
                {
                    formatf(buf, sizeof(buf), "Error: No Social Club account exists with the specified e-mail address.");
                }
                else if(m_MyStatus.GetResultCode() == RLSC_ERROR_NOTALLOWED_MAXEMAILSREACHED)
                {
                    formatf(buf, sizeof(buf), "Error: The maximum number of password reset e-mails has already been sent.");
                }
                else
                {
                    formatf(buf, sizeof(buf), "Social Club account creation failed (result code %d)", m_MyStatus.GetResultCode());
                }
                this->PushResult(buf);
            }
        }
        break;
    case SEGUE_SOCIAL_CLUB_GET_COUNTRIES:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            char buf[128];
            if(m_MyStatus.Succeeded())
            {
                formatf(buf, sizeof(buf), "Retrieved %d Social Club countries (see debug output)", g_SocialClubCountries->m_List.size());
                this->PushResult(buf);
                
#if !__NO_OUTPUT
                rlScCountries::iterator it = g_SocialClubCountries->m_List.begin();
                rlScCountries::const_iterator stop = g_SocialClubCountries->m_List.end();
                for(unsigned i = 1; stop != it; ++i, ++it)
                {
#if !__NO_OUTPUT
                    rlScCountries::Country *country = *it;
                    Displayf("   Country #%d Code:%s, Name:%s", i, country->m_Code, country->m_Name);
#endif
                }
#endif

                delete g_SocialClubCountries;
            }
            else
            {
                formatf(buf, sizeof(buf), "Failed to retrieve Social Club countries (result code %d)", m_MyStatus.GetResultCode());
                this->PushResult(buf);
            }
        }
        break;

    case SEGUE_SOCIAL_CLUB_UPDATE_PASSWORD:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                this->PushResult("Password updated successfully");
            }
            else
            {
                this->PushResult("Failed to update password");
            }
        }
        break;

	case SEGUE_SOCIAL_CLUB_POST_USER_FEED_ACTIVITY:
		if(!m_MyStatus.Pending())
		{
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                this->PushResult("Feed activity posted successfully");
            }
            else
            {
				char buf[128];
				s32 res = m_MyStatus.GetResultCode();
				if (RLSC_ERROR_NOTALLOWED_PRIVACY == res)
				{
					formatf(buf, "Failed to post feed activity: User privacy setting does not allow posting from the game.");
				}
				else
				{
					formatf(buf, "Failed to post feed activity. rlScErrorCode: %d", res);
				}

                this->PushResult(buf);
            }
		}
		break;

	case SEGUE_FACEBOOK_GET_ACCESS_TOKEN:
		if(!m_MyStatus.Pending())
		{
			this->PopSegue();

			char buf[RL_FACEBOOK_TOKEN_MAX_LENGTH+128];
			if (m_MyStatus.Succeeded())
			{
				formatf(buf, "Facebook get access token succeeded. Please check logs for the value.");
			}
			else
			{
				s32 res = m_MyStatus.GetResultCode();

				if (RL_FB_GET_TOKEN_FAILED_UNKNOWN == res)
				{
					formatf(buf, "Facebook get access token failed: %s", "Unknown Error.");
				}
				else if (RL_FB_GET_TOKEN_FAILED_USER_CANCELLED == res)
				{
					formatf(buf, "Facebook get access token failed: %s", "User cancelled.");
				}
				else if (RL_FB_GET_TOKEN_FAILED_NOT_CONFIGURED == res)
				{
					formatf(buf, "Facebook get access token failed: %s", "Social sharing is not configured (please show dialog).");
				}
				else if (RL_FB_GET_TOKEN_FAILED_NOT_ALLOWED == res)
				{
					formatf(buf, "Facebook get access token failed: %s", "Social sharing is explicitly disabled by the user.");
				}
			}
			this->PushResult(buf);
		}
		break;

	case SEGUE_FACEBOOK_POST_OPEN_GRAPH:
		if(!m_MyStatus.Pending())
		{
			this->PopSegue();

			char buf[1024];
			if (m_MyStatus.Succeeded())
			{
				formatf(buf, "Facebook open graph post succeeded:\n\n%s", m_FacebookOGPostId);
			}
			else
			{
				formatf(buf, "Facebook open graph post failed: %d", m_MyStatus.GetResultCode());
			}
			this->PushResult(buf);
		}
		break;
	case SEGUE_FACEBOOK_VALIDATE_APP_PERMISSIONS:
		if(!m_MyStatus.Pending())
		{
			this->PopSegue();

			char buf[1024];
			if (m_MyStatus.Succeeded())
			{
				formatf(buf, "Facebook app validation succeeded:\n\n");
			}
			else
			{
				formatf(buf, "Facebook app validation failed: %d", m_MyStatus.GetResultCode());
			}
			this->PushResult(buf);
		}
		break;
    case SEGUE_READING_SC_FRIENDS:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                this->PushResult("Read succeeded.  See output for details.");
            }
            else
            {
                this->PushResult("Read failed.  See output for error details.");
            }
        }
        break;

    case SEGUE_CLOUD_DOWNLOAD:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                m_Gb.Append("\0", 1);
                this->PushResult((const char*) m_Gb.GetBuffer());
            }
            else
            {
                this->PushResult("Failed to download from cloud");
            }
        }
        break;

    case SEGUE_CLOUD_UPLOAD:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                this->PushResult("Successfully uploaded to cloud");
            }
            else
            {
                this->PushResult("Failed to upload to cloud");
            }
        }
        break;

    case SEGUE_CLOUD_DELETE:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                this->PushResult("Successfully deleted file from cloud");
            }
            else
            {
                this->PushResult("Failed to delete file from cloud");
            }
        }
        break;
#if RSG_PC
	case SEGUE_CLOUD_SAVE_IDENTIFY_CONFLICTS:
	case SEGUE_CLOUD_SAVE_GET_FILE:
	case SEGUE_CLOUD_SAVE_POST_FILE:
	case SEGUE_CLOUD_DELETE_FILES:
	case SEGUE_CLOUD_QUERY_BETA:
	case SEGUE_CLOUD_BACKUP_CONFLICTS:
	case SEGUE_CLOUD_SEARCH_BACKUPS:
	case SEGUE_CLOUD_RESTORE_BACKUP:
	case SEGUE_CLOUD_GET_METADATA:
		if(!m_RgscStatus->Pending())
		{
			this->PopSegue();

			if(m_RgscStatus->Succeeded())
			{
				this->PushResult("Cloud Save Operation Succeeded");
			}
			else
			{
				this->PushResult("Cloud Save Operation Failed");
			}

			m_RgscStatus->Release();
			m_RgscStatus = NULL;
		}
		break;
	case SEGUE_CLOUD_MERGE_FILES:
		if(!m_RgscStatus->Pending())
		{
			this->PopSegue();

			if(m_RgscStatus->Succeeded())
			{
				this->PushResult("Cloud Save Operation Succeeded");
			}
			else
			{
				this->PushResult("Cloud Save Operation Failed");
			}

			rgsc::ICloudSaveManifestV2* manifestV2 = NULL;
			HRESULT hr = rgscManifest->QueryInterface(rgsc::IID_ICloudSaveManifestV2, (void**)&manifestV2);
			if (SUCCEEDED(hr))
			{
				manifestV2->Save();
			}

			m_RgscStatus->Release();
			m_RgscStatus = NULL;
		}
		break;
	case SEGUE_CLOUD_REGISTER_FILE:
	case SEGUE_CLOUD_UNREGISTER_FILE:
		if(!m_MyStatus.Pending())
		{
			this->PopSegue();

			if(m_MyStatus.Succeeded())
			{
				this->PushResult("Cloud Save Operation Succeeded");
			}
			else
			{
				this->PushResult("Cloud Save Operation Failed");
			}
		}
		break;
#endif
    case SEGUE_UGC_COPY_CONTENT:
    case SEGUE_UGC_CREATE_CONTENT:
    case SEGUE_UGC_UPDATE_CONTENT:
    case SEGUE_UGC_VERSION_CONTENT:
    case SEGUE_UGC_PUBLISH_CONTENT:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                m_UiUgcContentDetails.SetContent(g_UgcMetadata);
                this->PushState(STATE_UGC_CONTENT_DETAILS);
            }
            else
            {
                this->PushResult("Failed");
            }
        }
        break;

    case SEGUE_UGC_QUERY_CONTENT:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if (m_MyStatus.Succeeded())
            {
                //m_UiUgcContentList.SetContent(g_UgcResults);
                this->PushState(STATE_UGC_CONTENT_LIST);
            }
            else
            {
                this->PushResult("Failed");
            }            
        }
        break;
		
    case SEGUE_UGC_CREATE_VIDEO_CONTENT:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                static u8 s_VideoData[20*1024*1024];
                FILE* fp = ::fopen("C:\\MyTemp\\TestLlnwCloudStorage\\FIB 4 - Prep 1.mp4", "rb");
                if(!fp)
                {
                    this->PushResult("'C:\\MyTemp\\TestLlnwCloudStorage\\FIB 4 - Prep 1.mp4' doesn't exist");
                }
                else
                {
                    const size_t dataLen = ::fread(s_VideoData, 1, sizeof(s_VideoData), fp);
                    ::fclose(fp);

                    rlUgcGta5VideoMetadataData vmd;
                    vmd.SetMetadata(g_UgcMetadata);
                    auto task = rlGetTaskManager()->CreateTask<rlHttpLlnwUploadVideoTask>();
                    rlTaskBase::Configure(task,
                                        vmd.GetStagingName(),
                                        vmd.GetStagingUrl(),
                                        vmd.GetStagingAuthToken(),
                                        vmd.GetStagingId(),
                                        0,
                                        s_VideoData,
                                        (unsigned)dataLen,
                                        &m_MyStatus);
                    /*rlTaskBase::Configure(task,
                                        vmd.GetStagingName(),
                                        vmd.GetStagingUrl(),
                                        "XXXX",
                                        "XXXX",
                                        vmd.GetStagingId(),
                                        s_VideoData,
                                        0,
                                        (unsigned)dataLen,
                                        &m_MyStatus);*/
                    rlGetTaskManager()->AddParallelTask(task);
                    this->PushSegue(SEGUE_UGC_UPLOADING_VIDEO);
                }
            }
            else
            {
                this->PushResult("Failed");
            }
        }
        break;

    case SEGUE_UGC_UPLOADING_VIDEO:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if (m_MyStatus.Succeeded())
            {
                if(rlUgc::Publish(m_Gamer->GetLocalIndex(),
                                RLUGC_CONTENT_TYPE_GTA5VIDEO,
                                g_UgcMetadata.GetContentId(),
                                g_UgcMetadata.GetContentId(),
                                NULL,
								NULL,
                                &m_MyStatus))
                {
                    this->PushSegue(SEGUE_UGC_PUBLISH_CONTENT);
                }
                else
                {
                    PushResult("Failed to start task");
                }
            }
            else
            {
                this->PushResult("Video upload failed");
            }            
        }
        break;

    case SEGUE_PRESENCE_QUERY:
    case SEGUE_PRESENCE_QUERY_COUNT:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                this->PushResult("Presence query succeeded");
            }
            else
            {
                this->PushResult("Presence query failed");
            }
        }
        break;

	case SEGUE_ACHIEVEMENTS_AWARD_ACHIEVEMENT:
		if(!m_MyStatus.Pending())
		{
			this->PopSegue();

			if(m_MyStatus.Succeeded())
			{
				this->PushResult("Achievement was awarded successfully");
			}
			else
			{
				this->PushResult("Failed to award achievement");
			}
		}
		break;

	case SEGUE_ACHIEVEMENTS_READ_ACHIEVEMENTS:
		if(!m_MyStatus.Pending())
		{
			this->PopSegue();

			if(m_MyStatus.Succeeded())
			{
				u32 totalPoints = 0;
				u32 totalPossiblePoints = 0;

				for(int i = 0; i < sm_NumAchRead; i++)
				{
#if RSG_PC
					rlAchievementInfoEx& a = sm_AchievementsInfoEx[i];
					totalPossiblePoints += a.GetCred();

					if(a.IsAchieved() == false)
					{
						continue;
					}

					// convert from UTC time to local time
					time_t utcTime = a.GetAchievedTime();
					char tm[26] = {0};
					safecpy(tm, "<N/A>");	

					if(utcTime > 0)
					{
						struct tm* lt = localtime(&utcTime);
						struct tm ltCopy = *lt;
						struct tm ltCopyNoDst = *lt;
						ltCopyNoDst.tm_isdst = 0;

						time_t tmp = mktime(&ltCopy);
						time_t tmp2 = mktime(&ltCopyNoDst);
						time_t dstDiff = tmp2 - tmp;

						time_t badTime = mktime(gmtime(&utcTime));
						badTime -= dstDiff;
						time_t diff = badTime - utcTime;
						time_t localTime = utcTime - diff;

						const char* ctm = ctime(&localTime);
						if(ctm != NULL)
						{
							safecpy(tm, ctm);
							tm[strlen(tm) - 1] = '\0';
						}
					}

					totalPoints += a.GetCred();

					grcImage* image = a.GetImage();
					u32 width = 0;
					u32 height = 0;
					if(image)
					{
						width = image->GetWidth();
						height = image->GetHeight();
					}

					rlDebug("Achievement Id:%d, Type:%d, Title:%s, Description:%s, Unachieved String:%s, Image Size:%dx%d, Points:%d, Show Unachieved:%s, Achieved:%s, Time Achieved:%s", 
						a.GetId(), a.GetType(), a.GetLabel(), a.GetDescription(), a.GetUnachievedString(), width, height, a.GetCred(), a.ShowUnachieved() ? "yes" : "no", a.IsAchieved() ? "yes" : "no", tm);
#else
					rlAchievementInfo& a = sm_AchievementsInfo[i];
					totalPossiblePoints += a.GetCred();

					if (a.IsAchieved())
					{
						totalPoints += a.GetCred();
					}

					rlDebug("Achievement Id:%d, Description:%s, Points:%d, Achieved:%s", a.GetId(), a.GetDescription(), a.GetCred(), a.IsAchieved() ? "yes" : "no");
#endif
				}

				rlDebug("Achievement points earned: %d out of a possible %d", totalPoints, totalPossiblePoints);

				this->PushResult("Achievements were read successfully. See debug output.");
			}
			else
			{
				this->PushResult("Failed to read achievements.");
			}
		}
		break;

	case SEGUE_ACHIEVEMENTS_DELETE_ACHIEVEMENTS:
		if(!m_MyStatus.Pending())
		{
			this->PopSegue();

			if(m_MyStatus.Succeeded())
			{
				this->PushResult("Achievements deleted successfully.");
			}
			else
			{
#if RSG_PC && __DEV
				this->PushResult("Failed to delete achievements.");
#else
				this->PushResult("Failed to delete achievements. This operation is currently only supported on RSG_PC && __DEV builds.");
#endif
			}
		}
		break;

    case SEGUE_ACHIEVEMENTS_GET_ROS_ACHIEVEMENTS:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                this->PushResult("Achievements read successfully");
            }
            else
            {
                this->PushResult("Failed to read achievements");
            }
        }
        break;

    case SEGUE_ACHIEVEMENTS_GET_ROS_ACHIEVEMENT_DEFINITIONS:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                this->PushResult("Achievement definitions read successfully");
            }
            else
            {
                this->PushResult("Failed to read achievement definitions");
            }
        }
        break;

    case SEGUE_CLAN_UPDATING:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                if (!RetrieveThenPushClanState(this->GetState()))
                {
                    this->PushResult("Updated clan");
                }
            }
            else
            {
                this->PushResult("Failed to update clan");   
            }
        }
        break;
    case SEGUE_CLAN_RANK_UPDATING:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                if (!RetrieveThenPushClanState(this->GetState()))
                {
                    this->PushResult("Updated clan rank");
                }
            }
            else
            {
                this->PushResult("Failed to update clan rank");   
            }
        }
        break;
    case SEGUE_CLAN_MEMBER_UPDATING:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                if (!RetrieveThenPushClanState(this->GetState()))
                {
                    this->PushResult("Updated clan member");
                }
            }
            else
            {
                this->PushResult("Failed to update clan member");   
            }
        }
        break;
    case SEGUE_CLAN_INVITE_UPDATING:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                if (!RetrieveThenPushClanState(this->GetState()))
                {
                    this->PushResult("Updating clan invitation");
                }
            }
            else
            {
                this->PushResult("Failed to update clan invitation");   
            }
        }
        break;
    case SEGUE_CLAN_MESSAGE_UPDATING:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                if (!RetrieveThenPushClanState(this->GetState()))
                {
                    this->PushResult("Updating clan messages");
                }
            }
            else
            {
                this->PushResult("Failed to update clan messages");   
            }
        }
        break;
    case SEGUE_CLAN_INVITE_TO_CLAN:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(STATE_CLANS_INVITE_FRIEND_MENU == this->GetState())
            {
                this->PopState();
            }

            if(m_MyStatus.Succeeded())
            {
                this->PushResult("Friend invited to clan");
            }
            else
            {
                this->PushResult("Failed to invite friend to clan");   
            }
        }
        break;
    case SEGUE_CLAN_JOIN_REQUEST:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(STATE_CLANS_ALL_MENU == this->GetState())
            {
                this->PopState();
            }

            if(m_MyStatus.Succeeded())
            {
                this->PushResult("Join request sent to clan");
            }
            else
            {
                this->PushResult("Failed to send join request to clan");   
            }
        }
        break;
    case SEGUE_CLAN_RETRIEVING_MINE:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                m_UiClansMainMenu.Populate(m_MyClanMembership, m_NumMyClanMembership);
                if(STATE_CLANS_MAIN_MENU != this->GetState())
                {
                    this->PushState(STATE_CLANS_MAIN_MENU);
                }
            }
            else
            {
                this->PushResult("Failed to retrieve my clans");
            }
        }
        break;
    case SEGUE_CLAN_RETRIEVING_PRIMARY_CLAN:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                char buf[256];
                sprintf(buf, "Primary Clan: %s", m_PrimaryClan.m_MemberClanInfo.m_Clan.m_ClanName);
                this->PushResult(buf);
            }
            else
            {
                this->PushResult("Failed to retrieve my clans");
            }
        }
        break;
    case SEGUE_CLAN_RETRIEVING_MINE_TO_INVITE:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                m_UiClanInviteFriendMenu.Populate(m_MyClanMembership, m_NumMyClanMembership);
                if(STATE_CLANS_INVITE_FRIEND_MENU != this->GetState())
                {
                    this->PushState(STATE_CLANS_INVITE_FRIEND_MENU);
                }
            }
            else
            {
                this->PushResult("Failed to retrieve my clans");
            }
        }
        break;
    case SEGUE_CLAN_RETRIEVING_ALL:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                rlClanId requestIds[10];
                for (unsigned i = 0; i < m_NumAllClans; ++i)
                {
                    requestIds[i] = m_AllClans[i].m_Id;
                }

                if (m_NumAllClans > 0)
                {
                    if (rlClan::GetLeadersForClans(
                        m_Gamer->m_GamerInfo.GetLocalIndex(), 
                        &requestIds[0], 
                        m_NumAllClans, 
                        m_ClanLeaders, 
                        &m_NumClanLeaders,
                        &m_MyStatus))
                    {
                        this->PushSegue(SEGUE_CLAN_RETRIEVING_ALL_LEADERS);
                    }
                    else
                    {
                        this->PushResult("Failed to retrieve all leaders");
                    }
                }
            }
            else
            {
                this->PushResult("Failed to retrieve all clans");
            }
        }
        break;
    case SEGUE_CLAN_RETRIEVING_ALL_LEADERS:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                m_UiClansAllMenu.Populate(m_AllClans, m_ClanLeaders, m_NumAllClans);
                if(STATE_CLANS_ALL_MENU != this->GetState())
                {
                    this->PushState(STATE_CLANS_ALL_MENU);
                }
            }
            else
            {
                this->PushResult("Failed to retrieve all leaders");
            }
        }
        break;
    case SEGUE_CLAN_RETRIEVING_MEMBERS:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                m_UiClanMembersMenu.Populate(*m_ActiveClan, 
                    m_MyClanMembership, m_NumMyClanMembership,
                    m_ClanMembers, m_NumClanMembers);
                if(STATE_CLANS_MEMBERS_MENU != this->GetState())
                {
                    this->PushState(STATE_CLANS_MEMBERS_MENU);
                }
            }
            else
            {
                this->PushResult("Failed to retrieve members");
            }
        }
        break;
    case SEGUE_CLAN_RETRIEVING_RANKS:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                m_UiClanRanksMenu.Populate(*m_ActiveClan, 
                    m_MyClanMembership, m_NumMyClanMembership,
                    m_ClanRanks, m_NumClanRanks);
                if(STATE_CLANS_RANKS_MENU != this->GetState())
                {
                    this->PushState(STATE_CLANS_RANKS_MENU);
                }
            }
            else
            {
                this->PushResult("Failed to retrieve clan ranks");
            }
        }
        break;
    case SEGUE_CLAN_RETRIEVING_REQUESTS:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                m_UiClanRequestsMenu.Populate(*m_ActiveClan, m_ClanRequests, m_NumClanRequests);
                if(STATE_CLANS_REQUESTS_MENU != this->GetState())
                {
                    this->PushState(STATE_CLANS_REQUESTS_MENU);
                }
            }
            else
            {
                this->PushResult("Failed to retrieve clan requests");
            }
        }
        break;
    case SEGUE_CLAN_RETRIEVING_SENT_REQUESTS:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                m_UiClanSentRequestsMenu.Populate(m_ClanSentRequests, m_NumClanSentRequests);
                if(STATE_CLANS_SENT_REQUESTS_MENU != this->GetState())
                {
                    this->PushState(STATE_CLANS_SENT_REQUESTS_MENU);
                }
            }
            else
            {
                this->PushResult("Failed to retrieve clan sent requests");
            }
        }
        break;
    case SEGUE_CLAN_RETRIEVING_INVITES:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                m_UiClanInvitesMenu.Populate(m_ClanInvites, m_NumClanInvites);
                if(STATE_CLANS_INVITES_MENU != this->GetState())
                {
                    this->PushState(STATE_CLANS_INVITES_MENU);
                }
            }
            else
            {
                this->PushResult("Failed to retrieve clan invites");
            }
        }
        break;
    case SEGUE_CLAN_RETRIEVING_WALL_MESSAGES:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                m_UiClanWallMessagesMenu.Populate(*m_ActiveClan,
                    m_MyClanMembership, m_NumMyClanMembership,
                    m_ClanMessags, m_NumClanMessages);
                if(STATE_CLANS_WALL_MESSAGE_MENU != this->GetState())
                {
                    this->PushState(STATE_CLANS_WALL_MESSAGE_MENU);
                }
            }
            else
            {
                this->PushResult("Failed to retrieve clan messages");
            }
        }
        break;
    case SEGUE_CLAN_RETRIEVING_METADATA_ENUMS:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                static char message[256];
                message[0] = 0;
                safecat(message, "METADATA=|", 256);
                for (unsigned i = 0; i < m_NumClanEnums; ++i)
                {
                    safecat(message, m_ClanEnums[i].m_EnumName, 256);
                    safecat(message, "|", 256);
                }
                this->PushResult(message);
            }
            else
            {
                this->PushResult("Failed to retrieve clan messages");
            }
        }
        break;
    case SEGUE_CLAN_RETRIEVING_FEUD_STAT:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();

            if(m_MyStatus.Succeeded())
            {
                static char message[1024];
                static char num[20];

                message[0] = 0;
                safecat(message, "FEUD=|", 1024);
                for (unsigned i = 0; i < m_NumClanFeuds; ++i)
                {
                    safecat(message, m_ClanFeuds[i].m_ClanName, 1024);
                    formatf(num, 20, "%d", m_ClanFeuds[i].m_TotalFeuds);
                    safecat(message, ";", 1024);
                    safecat(message, num, 1024);
                    safecat(message, "|", 1024);
                }
                this->PushResult(message);
            }
            else
            {
                this->PushResult("Failed to retrieve clan messages");
            }
        }
        break;
    case SEGUE_PROFILE_STATS_SYNCHRONIZE:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();
            PushResult(m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
        }
        break;
    case SEGUE_PROFILE_STATS_READ_BY_GAMER:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();
            if (m_MyStatus.Succeeded())
            {
                char display[1024];

                safecpy(display, "Succeeded.  Set output for results.");

                //safecatf(display, "Rows[%d]: ", m_StatsResult.GetNumRows());
                //safecatf(display, "Columns[%d]", m_StatsResult.GetNumStatIds());
                PushResult(display);

                //char gamerHandleStr[RL_MAX_HANDLE_CHARS];    
                //for (unsigned i = 0; i < m_StatsResult.GetNumRows(); ++i)
                //{
                //    display[0] = '\0';

                //    m_StatsResult.GetGamerHandle(i)->ToString(gamerHandleStr, sizeof(gamerHandleStr));
                //    safecatf(display, "Row[%d]: <%s>: [", i, gamerHandleStr);

                //    for (unsigned j = 0; j < m_StatsResult.GetNumStatIds(); ++j)
                //    {
                //        if (0 != j) safecat(display, ",");

                //        char buf[256];
                //        char buf2[256];
                //        formatf(buf, sizeof(buf), "%d=%s", 
                //                m_StatsResult.GetStatId(j), 
                //                m_StatsResult.GetValue(i, j)->ToString(buf2, sizeof(buf2)));

                //        safecatf(display, buf);
                //    }
                //    safecatf(display, "]");
                //    m_UiGenericResult.AddText(display);
                //}
            }
            else
            {
                PushResult("Failed");
            }
        }
        break;
	case SEGUE_PROFILE_STATS_RESET_BY_GROUP:
		if(!m_MyStatus.Pending())
		{
			this->PopSegue();
			PushResult(m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
		}
		break;
    case SEGUE_PROFILE_STATS_FLUSH:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();
            PushResult(m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
        }
        break;
    case SEGUE_COMMUNITY_STATS_READ:
        if(!m_MyStatus.Pending())
        {
            this->PopSegue();
            if (m_MyStatus.Succeeded())
            {
                char display[1024];

                safecpy(display, "Succeeded: ");

                //safecatf(display, "Rows[%d]: ", m_StatsResult.GetNumRows());
                //safecatf(display, "Columns[%d]", STATS_COLUMN_COUNT);
                PushResult(display);

                //char gamerHandle[RL_MAX_HANDLE_CHARS];    
                //for (unsigned i = 0; i < m_StatsResult.GetNumRows(); ++i)
                //{
                //    const rlProfileStatsRecordBase& r = m_StatsResult.GetRow(i);
                //    r.GetGamerHandle().ToString(gamerHandle, sizeof(gamerHandle));
                //    display[0] = '\0';
                //    safecatf(display, "Row[%d]: <%s>: [", i, gamerHandle);
                //    for (unsigned j = 0; j < r.GetNumStats(); ++j)
                //    {
                //        if (0 != j)
                //        {
                //            safecat(display, ",");
                //        }
                //        const rlProfileStatMetadata* stat = psm.GetStatById(m_StatIds[j]);
                //        Assert(stat);
                //        const rlProfileStatsValue& val = r.GetStatValue(j);
                //        Assert(stat->GetType() == val.GetType());
                //        switch (stat->GetType())
                //        {
                //        case RL_PROFILESTATS_TYPE_FLOAT:
                //            safecatf(display, "%d:%f", m_StatIds[j], val.GetFloat());
                //            break;
                //        case RL_PROFILESTATS_TYPE_INT32:
                //            safecatf(display, "%d:%d", m_StatIds[j], val.GetInt32());
                //            break;
                //        case RL_PROFILESTATS_TYPE_INT64:
                //            safecatf(display, "%d:%" I64FMT "d", m_StatIds[j], val.GetInt64());
                //            break;
                //        default:
                //            safecat(display, "%d:Invalid", m_StatIds[j]);
                //            break;
                //        }
                //    }
                //    safecatf(display, "]");
                //    m_UiGenericResult.AddText(display);
                //}
            }
            else
            {
                PushResult("Failed");
            }
        }
        break;
    }

	if (sm_VirtualKeyboard.IsPending())
	{
		sm_VirtualKeyboard.Update();
	}

	if (GRCDEVICE.QueryCloseWindow())
	{
		m_Done = true;
	}

#define SUPPORT_READY_TO_SHUTDOWN 0
	if (m_CrashTime != 0
#if RSG_PC && SUPPORT_READY_TO_SHUTDOWN
		&& g_rlPc.GetRgscInterface()->IsReadyToShutdown()
#endif
		)
	{
		m_Done = true;
	}

    return m_Done; //false == not done
}

void
App::Draw()
{
    const State state = this->GetState();

    if(state >= 0)
    {
        m_Pages[state]->Draw();

        rage::grcColor(Color_white);
        float y = UI_TITLE_Y - (8*UI_GAP);
        u64 peerId;
        rlPeerInfo::GetLocalPeerId(&peerId);
        char buf[512 + (netSocketAddress::MAX_STRING_BUF_SIZE * 2)];

        const char* gamerName = "";
        int localGamerIndex = -1;
        if(m_Gamer)
        {
            gamerName = m_Gamer->m_GamerInfo.GetName();
            localGamerIndex = m_Gamer->GetGamerInfo().GetLocalIndex();
        }

		time_t secs = rlGetPosixTime();
		struct tm* t = localtime(&secs);

#if RSG_PC && !__FINAL
		if(PARAM_takehometest.Get())
		{
			formatf(buf, "Name:     %s(%d) %s", gamerName, localGamerIndex, s_AutoTestString);
		}
		else
#endif
		{
			formatf(buf, "Name:     %s(%d) Time: %u/%u %u:%02u:%02u%s", gamerName, localGamerIndex, t->tm_mon + 1, t->tm_mday, ((t->tm_hour % 12) == 0) ? 12 : (t->tm_hour % 12), t->tm_min, t->tm_sec, (t->tm_hour < 12) ? "am" : "pm");
		}

        rage::grcDraw2dText(200, y, buf);
        y += UI_GAP;

        char ghStr[RL_MAX_GAMER_HANDLE_CHARS] = {""};
        if(m_Gamer)
        {
            m_Gamer->m_GamerInfo.GetGamerHandle().ToString(ghStr);
        }

        formatf(buf, "Handle:   %s", ghStr);
        rage::grcDraw2dText(200, y, buf);
        y += UI_GAP;

        const char* scNickname = "";
        char ridStr[32] = {""};
        if(m_Gamer)
        {
            const rlRosCredentials& creds =
                rlRos::GetCredentials(m_Gamer->GetLocalIndex());
            if(creds.GetRockstarAccount().m_RockstarId > 0)
            {
                scNickname = creds.GetRockstarAccount().m_Nickname;
                formatf(ridStr, "%" I64FMT "d", creds.GetRockstarId());
            }
        }
        formatf(buf, "SC Nick:  %s", scNickname);
        rage::grcDraw2dText(200, y, buf);
        y += UI_GAP;
        formatf(buf, "R* ID:    %s", ridStr);
        rage::grcDraw2dText(200, y, buf);
        y += UI_GAP;

        formatf(buf, "Peer Id:  0x%" I64FMT "x", peerId);
        rage::grcDraw2dText(200, y, buf);
        y += UI_GAP;

		char addrBuf[netSocketAddress::MAX_STRING_BUF_SIZE];

		// Presence
		{
			// on PC we connect to the presence server via the SC DLL
			// which doesn't expose the presence server details for UI
#if !RSG_PC
			if(netRelay::GetPresenceServerAddress().IsValid())
			{
				netRelay::GetPresenceServerAddress().Format(addrBuf, true);
				if(netRelay::IsUsingSecurePresence())
				{
					if(netRelay::IsConnectedToPresence())
					{
						formatf(buf, "Presence: %s (secure) - connected", addrBuf);
					}
					else
					{
						formatf(buf, "Presence: %s (secure) - not connected", addrBuf);
					}
				}
				else
				{
					formatf(buf, "Presence: %s (non-secure)", addrBuf);
				}
			}
			else
			{
				formatf(buf, "Presence: Address not yet discovered");
			}
			rage::grcDraw2dText(200, y, buf);
			y += UI_GAP;
#endif
		}

		// Relay
		{
			if(netRelay::GetMyRelayAddress().IsValid())
			{
				netRelay::GetMyRelayAddress().GetProxyAddress().Format(addrBuf, true);
				if(netRelay::IsUsingSecureRelays())
				{
					if(netRelay::IsConnectedToRelay())
					{
						formatf(buf, "Relay:    %s (secure) - connected", addrBuf);
					}
					else
					{
						formatf(buf, "Relay:    %s (secure) - not connected", addrBuf);
					}
				}
				else
				{
					formatf(buf, "Relay:    %s (non-secure)", addrBuf);
				}
			}
			else
			{
				formatf(buf, "Relay:    Address not yet discovered");
			}
			rage::grcDraw2dText(200, y, buf);
			y += UI_GAP;
		}

		netNatType natType = netHardware::GetNatType();
		char natTypeStr[32] = "Unknown";
		switch(natType)
		{
			case NET_NAT_OPEN: safecpy(natTypeStr, "Open"); break;
			case NET_NAT_MODERATE: safecpy(natTypeStr, "Moderate"); break;
			case NET_NAT_STRICT: safecpy(natTypeStr, "Strict"); break;
			default: safecpy(natTypeStr, "Unknown"); break;
		}

		// Private address / Public address
		{
			m_LiveMgr->GetCxnMgr()->GetSocketManager()->GetMainSocket()->GetAddress().Format(addrBuf, true);

			if(netRelay::GetMyRelayAddress().IsValid())
			{
				char publicAddrStr[netSocketAddress::MAX_STRING_BUF_SIZE];
				netRelay::GetMyRelayAddress().GetTargetAddress().Format(publicAddrStr, true);
				formatf(buf, "Addr:     %s/%s. NAT Type: %s", addrBuf, publicAddrStr, natTypeStr);
			}
			else
			{
				formatf(buf, "Addr:     %s. NAT Type: %s", addrBuf, natTypeStr);
			}
			rage::grcDraw2dText(200, y, buf);
			y += UI_GAP;
		}

		if (PARAM_sampleSlots.Get())
		{
			snSession* p = m_LiveMgr->GetSession();
			if (p && p->IsEstablished())
			{
				char sessionStatus[256];
				formatf(sessionStatus, "Pub Slots: %d , Private Slots: %d, Reserved Slots: %d", 
					p->GetEmptySlots(RL_SLOT_PUBLIC),
					p->GetEmptySlots(RL_SLOT_PRIVATE),
					p->NumReservedSlots(RL_SLOT_PUBLIC));
				rage::grcDraw2dText(500, y - (4*UI_GAP), sessionStatus);
			}
		}
    }
}

void
App::SetGamer(Gamer* gamer)
{
    if(m_Gamer != gamer)
    {
        m_Gamer = gamer;

        const bool hasNetwork = rlPeerAddress::HasNetwork();
        const bool isSignedIn = m_Gamer && m_Gamer->IsSignedIn();
        const bool isOnline = m_Gamer && m_Gamer->IsOnline();
        const bool hasInvites = isOnline && m_UiInvitesReceived.HasInvites();

        m_UiFrontEnd.UpdateEnabled(hasNetwork, isSignedIn, isOnline, hasInvites);
    }

#if RSG_DURANGO || RSG_ORBIS || RSG_PC
	if (gamer && gamer->IsSignedIn())
	{
		rlPresence::SetActingUserIndex(gamer->GetLocalIndex());
	}
	else
	{
		rlPresence::SetActingUserIndex(-1);
	}
#endif

	if (gamer && gamer->IsOnline())
	{
		rlFriendsManager::Activate(gamer->GetLocalIndex());
	}
	else
	{
		rlFriendsManager::Deactivate();
	}
}

const Gamer*
App::GetGamer() const
{
    return m_Gamer;
}

unsigned
App::GetGamers(Gamer* gamers[], const unsigned maxGamers)
{
    return m_PeerMgr.GetGamers(gamers, maxGamers);
}

LiveManager*
App::GetLiveMgr()
{
    return m_LiveMgr;
}

//private:

Gamer*
App::GetLocalGamerById(const rlGamerId& gamerId)
{
    Gamer* gamer = 0;

    for(unsigned i = 0; i < COUNTOF(m_LocalGamers); ++i)
    {
        if(m_LocalGamers[i].IsValid()
            && m_LocalGamers[i].m_GamerInfo.GetGamerId() == gamerId)
        {
            gamer = &m_LocalGamers[i];
            break;
        }
    }

    return gamer;
}

Gamer*
App::GetLocalGamerByIndex(const int index)
{
    return AssertVerify(index >= 0 && (unsigned)index < COUNTOF(m_LocalGamers))
            ? &m_LocalGamers[index]
            : 0;
}

void 
App::OnFeEvent(UiElement* el, const int evtId, void* arg)
{
    if (&m_UiFrontEnd != el)
    {
        Assert(false);
    }

    if(UIEVENT_ACTION == evtId)
    {
        if(UiFrontEnd::FE_MULTIPLAYER == (size_t)arg)
        {
            if(m_Gamer)
            {
                this->PushState(STATE_MULTIPLAYER_MAIN_MENU);
            }
            else
            {
                this->ShowSigninUi();
            }
        }
        else if(UiFrontEnd::FE_FRIENDS_LIST == (size_t)arg)
        {
            if(m_Gamer && m_Gamer->IsOnline())
            {
                this->PushState(STATE_VIEWING_FRIENDS_LIST);
            }
            else
            {
                this->ShowSigninUi();
            }
        }
        else if(UiFrontEnd::FE_FRIEND_INVITES == (size_t)arg)
        {
            if(m_Gamer && m_Gamer->IsOnline())
            {
                this->PushState(STATE_VIEWING_INVITES_RECEIVED);
            }
            else
            {
                this->ShowSigninUi();
            }
        }
        else if(UiFrontEnd::FE_LEADERBOARDS == (size_t)arg)
        {
            if(m_Gamer && m_Gamer->IsOnline())
            {
                this->PushState(STATE_LEADERBOARDS_MAIN_MENU);
            }
            else
            {
                this->ShowSigninUi();
            }
        }
        else if(UiFrontEnd::FE_INBOX == (size_t)arg)
        {
            if(m_Gamer && m_Gamer->IsOnline())
            {
                this->PushState(STATE_INBOX_MAIN_MENU);
            }
            else
            {
                this->ShowSigninUi();
            }
        }
        else if(UiFrontEnd::FE_SOCIAL_CLUB == (size_t)arg)
        {
#if RLROS_SC_PLATFORM
            this->PushState(STATE_SOCIAL_CLUB_MAIN_MENU);
#else
            if(m_Gamer && m_Gamer->IsOnline())
            {
                this->PushState(STATE_SOCIAL_CLUB_MAIN_MENU);
            }
            else
            {
                this->ShowSigninUi();
            }
#endif
        }
		else if(UiFrontEnd::FE_COMMERCE_AND_ENTITLEMENT == (size_t)arg)
		{
			if (m_Gamer && m_Gamer->IsOnline())
			{
				this->PushState(STATE_COMMERCE_AND_ENTITLEMENT_MAIN_MENU);
			}
			else
			{
				this->ShowSigninUi();
			}
		}
		else if(UiFrontEnd::FE_FACEBOOK == (size_t)arg)
		{
			if (m_Gamer && m_Gamer->IsOnline())
			{
				this->PushState(STATE_FACEBOOK_MAIN_MENU);
			}
			else
			{
				this->ShowSigninUi();
			}
		}
        else if(UiFrontEnd::FE_CLOUD == (size_t)arg)
        {
			if(m_Gamer && m_Gamer->IsSignedIn())
			{
				this->PushState(STATE_CLOUD_MAIN_MENU);
			}
			else
			{
				this->ShowSigninUi();
			}
        }
        else if(UiFrontEnd::FE_UGC == (size_t)arg)
        {
            if(m_Gamer && m_Gamer->IsSignedIn())
            {
                this->PushState(STATE_UGC_MAIN_MENU);
            }
            else
            {
                this->ShowSigninUi();
            }
        }
        else if(UiFrontEnd::FE_PRESENCE == (size_t)arg)
        {
			if(m_Gamer && m_Gamer->IsSignedIn())
			{
				this->PushState(STATE_PRESENCE_MAIN_MENU);
			}
			else
			{
				this->ShowSigninUi();
			}
        }
		else if (UiFrontEnd::FE_RICHPRESENCE == (size_t)arg)
		{
			if(m_Gamer && m_Gamer->IsSignedIn())
			{
				this->PushState(STATE_RICHPRESENCE_MAIN_MENU);
			}
			else
			{
				this->ShowSigninUi();
			}
		}
		else if(UiFrontEnd::FE_ACHIEVEMENTS == (size_t)arg)
		{
			if(m_Gamer && m_Gamer->IsSignedIn())
			{
				this->PushState(STATE_ACHIEVEMENTS_MAIN_MENU);
			}
			else
			{
				this->ShowSigninUi();
			}
		}
		else if(UiFrontEnd::FE_CLANS == (size_t)arg)
        {
            if(m_Gamer && m_Gamer->IsOnline() && 
                rlClan::IsServiceReady(m_Gamer->m_GamerInfo.GetLocalIndex()))
            {
                RetrieveThenPushClanState(STATE_CLANS_MAIN_MENU);
            }
            else
            {
                this->ShowSigninUi();
            }
        }
        else if(UiFrontEnd::FE_PROFILE_STATS == (size_t)arg)
        {
            if(m_Gamer)
            {
                this->PushState(STATE_PROFILE_STATS_MAIN_MENU);
            }
            else
            {
                this->ShowSigninUi();
            }
        }
        else if(UiFrontEnd::FE_COMMUNITY_STATS == (size_t)arg)
        {
            if(m_Gamer)
            {
                this->PushState(STATE_COMMUNITY_STATS_MAIN_MENU);
            }
            else
            {
                this->ShowSigninUi();
            }
        }
		else if(UiFrontEnd::FE_DISPLAY_PARTY == (size_t)arg)
		{
			if(m_Gamer)
			{
				this->PushState(STATE_PARTY_MAIN_MENU);
			}
			else
			{
				this->ShowSigninUi();
			}
		}
        else if(UiFrontEnd::FE_DISPLAY_UNLOCKS == (size_t)arg)
        {
            if(m_Gamer)
            {
                if (rlUnlocks::IsUnlocksValid(m_Gamer->GetLocalIndex()))
                {
                    int nCount;
                    const u8* unlockBits = rlUnlocks::GetUnlockBits(m_Gamer->GetLocalIndex(), &nCount);

                    char unlockStr[1+RL_UNLOCKS_BYTES*2];
                    int binI;
                    int strI;
                    char nibOne;
                    char nibTwo;
                    for (binI = RL_UNLOCKS_BYTES-1, strI=0; binI>=0; --binI,strI+=2)
                    {
                        nibOne = (unlockBits[binI] & 0xF0) >> 4;
                        nibTwo = (unlockBits[binI] & 0xF);
                        unlockStr[strI] = (nibOne < 10) ? (nibOne + '0') : (nibOne - 10 + 'A');
                        unlockStr[strI+1] = (nibTwo < 10) ? (nibTwo + '0') : (nibTwo - 10 + 'A');
                    }
                    unlockStr[RL_UNLOCKS_BYTES*2]='\0';

                    char buf[256];
                    formatf(buf, sizeof(buf), "Unlock bits: 0x%s", unlockStr);
                    PushResult(buf);
                }
                else
                {
                    PushResult("Unlocks invalid.");
                }
            }
            else
            {
                this->ShowSigninUi();
            }
        }
		else if(UiFrontEnd::FE_ENTITLEMENT == (size_t)arg)
		{
			if(m_Gamer)
			{
				this->PushState(STATE_ENTITLEMENT);
			}
			else
			{
				this->ShowSigninUi();
			}
		}
        else if(UiFrontEnd::FE_SHOW_SIGNIN_UI == (size_t)arg)
        {
            this->ShowSigninUi();
        }
		else if(UiFrontEnd::FE_ADDFRIEND == (size_t)arg)
		{
			if(m_Gamer && m_Gamer->IsSignedIn())
			{
				rlGamerHandle gh;
				g_SystemUi.ShowAddFriendUI(m_Gamer->GetLocalIndex(), gh, "");
			}
			else
			{
				this->ShowSigninUi();
			}
		}
		else if (UiFrontEnd::FE_SHOW_APP_HELP == (size_t)arg)
		{
			if(m_Gamer && m_Gamer->IsSignedIn())
			{
				g_SystemUi.ShowAppHelpMenu(m_Gamer->GetLocalIndex());
			}
			else
			{
				this->ShowSigninUi();
			}
		}
		else if(UiFrontEnd::FE_LIVESTREAM == (size_t)arg)
		{
			if(m_Gamer && m_Gamer->IsSignedIn())
			{
				this->PushState(STATE_LIVESTREAM);
			}
			else
			{
				this->ShowSigninUi();
			}
		}
		else if (UiFrontEnd::FE_YOUTUBE == (size_t)arg)
		{
			if(m_Gamer && m_Gamer->IsSignedIn())
			{
				this->PushState(STATE_YOUTUBE);
			}
			else
			{
				this->ShowSigninUi();
			}
		}
		else if(UiFrontEnd::FE_QUIT == (size_t)arg)
        {
            m_Done = true;
        }
		else if(UiFrontEnd::FE_CRASH == (size_t)arg)
		{
#define SUPPORT_ON_GAME_CRASH 0
#if RSG_PC && SUPPORT_ON_GAME_CRASH
			u64 actionToTake = rgsc::OGC_UNADVERTISE_ALL_MATCHES | rgsc::OGC_PRESENCE_SIGNOUT;
			g_rlPc.GetRgscInterface()->OnGameCrashed(actionToTake);
#endif
			m_CrashTime = sysTimer::GetSystemMsTime();
		}
    }
}

void 
App::OnUiEvent(UiElement* el, const int evtId, void* arg)
{
    if(UIEVENT_ACTION != evtId)
    {
        return;
    }

    if(ACTION_BACK == (size_t)arg)
    {
        this->PopState();
    }

    if(&m_UiMultiplayerMainMenu == el)
    {
        if(!m_Gamer || !m_Gamer->IsSignedIn())
        {
            this->ShowSigninUi();
        }
        else if(UiMultiplayerMainMenu::ACTION_HOST_ONLINE == (size_t)arg)
        {
            if(m_Gamer && m_Gamer->IsOnline())
            {
                m_NetMode = RL_NETMODE_ONLINE;
                this->PushState(STATE_CONFIGURING_HOST);
            }
            else
            {
                this->ShowSigninUi();
            }
        }
        else if(UiMultiplayerMainMenu::ACTION_SEARCH_ONLINE == (size_t)arg)
        {
            if(m_Gamer && m_Gamer->IsOnline())
            {
                m_NetMode = RL_NETMODE_ONLINE;
                this->PushState(STATE_CONFIGURING_QUERY);
            }
            else
            {
                this->ShowSigninUi();
            }
        }
        else if(UiMultiplayerMainMenu::ACTION_HOST_LAN == (size_t)arg)
        {
            if(m_Gamer && m_Gamer->IsSignedIn())
            {
                m_NetMode = RL_NETMODE_LAN;
                this->PushState(STATE_CONFIGURING_HOST);
            }
            else
            {
                this->ShowSigninUi();
            }
        }
        else if(UiMultiplayerMainMenu::ACTION_SEARCH_LAN == (size_t)arg)
        {
            if(m_Gamer && m_Gamer->IsSignedIn())
            {
                m_NetMode = RL_NETMODE_LAN;
                this->PushState(STATE_CONFIGURING_QUERY);
            }
            else
            {
                this->ShowSigninUi();
            }
        }
        else if(UiMultiplayerMainMenu::ACTION_HOST_OFFLINE == (size_t)arg)
        {
            if(m_Gamer && m_Gamer->IsSignedIn())
            {
                m_NetMode = RL_NETMODE_OFFLINE;
                this->PushState(STATE_CONFIGURING_HOST);
            }
            else
            {
                this->ShowSigninUi();
            }
        }
        else if(UiMultiplayerMainMenu::ACTION_FRIENDS_LIST == (size_t)arg)
        {
            this->PushState(STATE_VIEWING_FRIENDS_LIST);
        }
    }
    else if(&m_UiLeaderboardMainMenu == el)
    {
        if(!m_Gamer || !m_Gamer->IsOnline())
        {
            this->ShowSigninUi();
        }
        else if(UiLeaderboardMainMenu::ACTION_SHOW_LEADERBOARD2_WRITE_MENU == (size_t)arg)
        {
            this->PushState(STATE_LEADERBOARDS2_WRITE_MENU);
        }
        else if(UiLeaderboardMainMenu::ACTION_SHOW_LEADERBOARD2_READ_MENU == (size_t)arg)
        {
            this->PushState(STATE_LEADERBOARDS2_READ_MENU);
        }
        else if(UiLeaderboardMainMenu::ACTION_SHOW_SWINGS_BY_STRIKE_LB_MENU == (size_t)arg)
        {
            m_LbReadId = (unsigned) Leaderboard_SwingsByStrikes::LEADERBOARD_ID;
            m_UiLeaderboardMenu.SetTitle("SWINGS BY STRIKE LEADERBOARD");
            this->PushState(STATE_LEADERBOARDS_PLAYER_LB_MENU);
        }
        else if(UiLeaderboardMainMenu::ACTION_SHOW_CLAN_SWINGS_BY_STRIKE_LB_MENU == (size_t)arg)
        {
            m_LbReadId = (unsigned) Leaderboard_SwingsByStrikes::LEADERBOARD_ID;
            m_UiLeaderboardMenu.SetTitle("CLAN SWINGS BY STRIKE LEADERBOARD");
            this->PushState(STATE_LEADERBOARDS_GROUP_LB_MENU);
        }
        else if(UiLeaderboardMainMenu::ACTION_SHOW_CLAN_MEMBER_SWINGS_BY_STRIKE_LB_MENU == (size_t)arg)
        {
            m_LbReadId = (unsigned) Leaderboard_SwingsByStrikes::LEADERBOARD_ID;
            m_UiLeaderboardMenu.SetTitle("CLAN MEMBER SWINGS BY STRIKE LEADERBOARD");
            this->PushState(STATE_LEADERBOARDS_GROUP_MEMBER_LB_MENU);
        }
        else if(UiLeaderboardMainMenu::ACTION_SHOW_HITS_LB_MENU == (size_t)arg)
        {
            m_LbReadId = (unsigned) Leaderboard_Hits::LEADERBOARD_ID;
            m_UiLeaderboardMenu.SetTitle("HITS LEADERBOARD");
            this->PushState(STATE_LEADERBOARDS_PLAYER_LB_MENU);
        }
        else if(UiLeaderboardMainMenu::ACTION_SHOW_CLAN_HITS_LB_MENU == (size_t)arg)
        {
            m_LbReadId = (unsigned) Leaderboard_Hits::LEADERBOARD_ID;
            m_UiLeaderboardMenu.SetTitle("CLAN HITS LEADERBOARD");
            this->PushState(STATE_LEADERBOARDS_GROUP_LB_MENU);
        }
        else if(UiLeaderboardMainMenu::ACTION_SHOW_CLAN_MEMBER_HITS_LB_MENU == (size_t)arg)
        {
            m_LbReadId = (unsigned) Leaderboard_Hits::LEADERBOARD_ID;
            m_UiLeaderboardMenu.SetTitle("CLAN MEMBER HITS LEADERBOARD");
            this->PushState(STATE_LEADERBOARDS_GROUP_MEMBER_LB_MENU);
        }
        else if(UiLeaderboardMainMenu::ACTION_SHOW_ARB_HITS_LB_MENU == (size_t)arg)
        {
            m_LbReadId = (unsigned) Leaderboard_ArbHits::LEADERBOARD_ID;
            m_UiLeaderboardMenu.SetTitle("ARB HITS LEADERBOARD");
            this->PushState(STATE_LEADERBOARDS_PLAYER_LB_MENU);
        }
        else if(UiLeaderboardMainMenu::ACTION_SHOW_SKILL_EXHIB_MENU == (size_t)arg)
        {
            m_LbReadId = (unsigned) SkillLeaderboard_ExhibitionStandard::LEADERBOARD_ID;
            m_UiLeaderboardMenu.SetTitle("EXHIB SKILL LEADERBOARD");
            this->PushState(STATE_LEADERBOARDS_PLAYER_LB_MENU);
        }
        else if(UiLeaderboardMainMenu::ACTION_SHOW_SKILL_RANKED_EXHIB_MENU == (size_t)arg)
        {
            m_LbReadId = (unsigned) SkillLeaderboard_ExhibitionRanked::LEADERBOARD_ID;
            m_UiLeaderboardMenu.SetTitle("RANKED SKILL LEADERBOARD");
            this->PushState(STATE_LEADERBOARDS_PLAYER_LB_MENU);
        }
    }
    else if(&m_UiLeaderboard2WriteMenu == el)
    {
        if(!m_Gamer || !m_Gamer->IsOnline())
        {
            this->ShowSigninUi();
        }
        else if(UiLeaderboard2WriteMenu::ACTION_SUBMIT == (size_t)arg)
        {
            WriteLeaderboard2();
        }
    }
    else if(&m_UiLeaderboard2ReadMenu == el)
    {
        if(!m_Gamer || !m_Gamer->IsOnline())
        {
            this->ShowSigninUi();
        }
        else if(UiLeaderboard2ReadMenu::ACTION_READ_BY_RANK == (size_t)arg)
        {
            ReadLeaderboard2(UiLeaderboard2ReadMenu::ACTION_READ_BY_RANK);
        }
        else if(UiLeaderboard2ReadMenu::ACTION_READ_BY_ROW == (size_t)arg)
        {
            ReadLeaderboard2(UiLeaderboard2ReadMenu::ACTION_READ_BY_ROW);
        }
        else if(UiLeaderboard2ReadMenu::ACTION_READ_BY_SCORE == (size_t)arg)
        {
            ReadLeaderboard2(UiLeaderboard2ReadMenu::ACTION_READ_BY_SCORE);
        }
        else if(UiLeaderboard2ReadMenu::ACTION_READ_NEAR == (size_t)arg)
        {
            ReadLeaderboard2(UiLeaderboard2ReadMenu::ACTION_READ_NEAR);
        }
    }
    else if(&m_UiLeaderboardMenu == el)
    {
        if(!m_Gamer || !m_Gamer->IsOnline())
        {
            this->ShowSigninUi();
        }
        else if(UiLeaderboardMenu::ACTION_GET_LEADERBOARD_SIZE == (size_t)arg)
        {
            GetLeaderboardSize(m_LbReadId);
        }
        else if(UiLeaderboardMenu::ACTION_READ_BY_RANK == (size_t)arg)
        {
            ReadLeaderboardByRank(m_LbReadId);
        }
        else if(UiLeaderboardMenu::ACTION_READ_BY_ROW == (size_t)arg)
        {
            ReadLeaderboardByRow(m_LbReadId);
        }
        else if(UiLeaderboardMenu::ACTION_READ_MEMBER_BY_GROUPS == (size_t)arg)
        {
            ReadLeaderboardMemberByGroups(m_LbReadId);
        }
        else if(UiLeaderboardMenu::ACTION_READ_BY_RADIUS == (size_t)arg)
        {
            ReadLeaderboardByRadius(m_LbReadId);
        }
#if __DEV && __LIVE
        else if(UiLeaderboardMenu::ACTION_CLEAR_LEADERBOARD == (size_t)arg)
        {
            rlStats::ResetAllUsersStats(m_LbReadId);
        }
#endif
    }
    else if(&m_UiInboxMenu == el)
    {
        if(!m_Gamer || !m_Gamer->IsOnline())
        {
            this->ShowSigninUi();
        }
        else if(UiInboxMenu::ACTION_GET_MESSAGES == (size_t)arg)
        {
            InboxGetMessages();
        }
        else if(UiInboxMenu::ACTION_GET_NEW_MESSAGES == (size_t)arg)
        {
            InboxGetNewMessages();
        }
        else if(UiInboxMenu::ACTION_POST_MESSAGE == (size_t)arg)
        {
            InboxPostMessage();
        }
        else if(UiInboxMenu::ACTION_POST_MESSAGE_TO_FRIENDS == (size_t)arg)
        {
            InboxPostMessageToFriends();
        }
        else if(UiInboxMenu::ACTION_POST_MESSAGE_TO_CREW == (size_t)arg)
        {
            InboxPostMessageToCrew();
        }
    }
    else if(&m_UiGenericResult == el)
    {
    }
    else if(&m_UiSocialClubMainMenu == el)
    {
        if((UiSocialClubMainMenu::ACTION_GET_TERMS_OF_SERVICE == (size_t)arg) && m_Gamer)
        {
            AssertVerify(rlSocialClub::GetAcceptedLegalPolicyVersion(
                m_Gamer->GetLocalIndex(), 
                "TOS",
                &g_SocialClubTermsOfServiceVersion, 
                &m_MyStatus));

            this->PushSegue(SEGUE_SOCIAL_CLUB_GET_TERMS_OF_SERVICE);
        }
        else if(UiSocialClubMainMenu::ACTION_LINK_OR_UNLINK_ACCOUNT == (size_t)arg)
        {
#if !RLROS_SC_PLATFORM
            if(m_Gamer)
            {
                if(rlSocialClub::IsConnectedToSocialClub(m_Gamer->GetLocalIndex()))
                {
                    AssertVerify(rlSocialClub::UnlinkAccount(m_Gamer->GetLocalIndex(), &m_MyStatus));
                }
                else
                {
                    char email[256];
                    const char* nickname = m_Gamer->GetGamerInfo().GetName();
                    formatf(email, COUNTOF(email), "%s@example.com", m_Gamer->GetGamerInfo().GetName());

                    AssertVerify(rlSocialClub::LinkAccount(m_Gamer->GetLocalIndex(), email, nickname, "password", &m_MyStatus));
                }
                this->PushSegue(SEGUE_SOCIAL_CLUB_LINK_OR_UNLINK_ACCOUNT);
            }
#endif
        }
        else if((UiSocialClubMainMenu::ACTION_CREATE_ACCOUNT == (size_t)arg) && m_Gamer)
        {
            // this would normally come from the list returned from rlSocialClub::GetCountries()
            rlScCountries::Country country;
            country.Reset("US", "United States");

            char email[256];
            const char* nickname = m_Gamer->GetGamerInfo().GetName();
            formatf(email, COUNTOF(email), "%s@example.com", m_Gamer->GetGamerInfo().GetName());

            //AssertVerify(rlSocialClub::CreateAccount(0, "email", NULL, "password", 30, country, "90210", &m_MyStatus));
            AssertVerify(rlSocialClub::CreateAccount(m_Gamer->GetLocalIndex(),
                                                      true,             //acceptNewsletter
                                                      NULL,             //avatarUrl
                                                      country.m_Code,   //countryCode 
                                                      "1-1-1971",       //dob
                                                      email, //email
                                                      true,             //isApproxDob
                                                      NULL,             //languageCode
                                                      nickname,         //nickname
                                                      "password",       //password
                                                      NULL,             //phone
                                                      "90210",          //zipCode
                                                      &m_MyStatus));

            this->PushSegue(SEGUE_SOCIAL_CLUB_CREATE_ACCOUNT);
        }
        else if((UiSocialClubMainMenu::ACTION_ACCEPT_TERMS_OF_SERVICE == (size_t)arg) && m_Gamer)
        {
            AssertVerify(rlSocialClub::AcceptLegalPolicy(
                m_Gamer->GetLocalIndex(), 
                "TOS",
                g_SocialClubTermsOfServiceVersion,
                &m_MyStatus));

            this->PushSegue(SEGUE_SOCIAL_CLUB_ACCEPT_TERMS_OF_SERVICE);
        }
        else if((UiSocialClubMainMenu::ACTION_RESET_PASSWORD == (size_t)arg) && m_Gamer)
        {
            char email[256];
            formatf(email, COUNTOF(email), "%s@example.com", m_Gamer->GetGamerInfo().GetName());

            AssertVerify(rlSocialClub::RequestResetPassword(email, &m_MyStatus));

            this->PushSegue(SEGUE_SOCIAL_CLUB_RESET_PASSWORD);
        }
        else if((UiSocialClubMainMenu::ACTION_GET_COUNTRIES == (size_t)arg) && m_Gamer)
        {
            g_SocialClubCountries = rage_new rlScCountries();
            AssertVerify(rlSocialClub::GetCountries(m_Gamer->GetLocalIndex(), 
                                                     *g_SocialClubCountries, 
                                                     &m_MyStatus));

            this->PushSegue(SEGUE_SOCIAL_CLUB_GET_COUNTRIES);
        }
        else if((UiSocialClubMainMenu::ACTION_UPDATE_PASSWORD == (size_t)arg) && m_Gamer)
        {
            AssertVerify(rlSocialClub::UpdatePassword(m_Gamer->GetLocalIndex(), 
                "password",
                "password",
                &m_MyStatus));

            this->PushSegue(SEGUE_SOCIAL_CLUB_UPDATE_PASSWORD);
        }
		else if((UiSocialClubMainMenu::ACTION_POST_USER_FEED_ACTIVITY == (size_t)arg) && m_Gamer)
		{
            AssertVerify(rlSocialClub::PostUserFeedActivity(m_Gamer->GetLocalIndex(), 
                         "SHARE_SPOTIFY_SONG",
						 0,
						 "1",
						 NULL));

            this->PushSegue(SEGUE_SOCIAL_CLUB_POST_USER_FEED_ACTIVITY);
		}
        else if((UiSocialClubMainMenu::ACTION_GET_SCAUTHTOKEN == (size_t)arg) && m_Gamer)
        {
            static char s_Buf[RLSC_MAX_SCAUTHTOKEN_CHARS];

            if (rlSocialClub::GetScAuthToken(m_Gamer->GetLocalIndex(), 
                                             s_Buf,
                                             sizeof(s_Buf),
                                             &m_MyStatus))
            {
                this->PushSegue(SEGUE_READING_SC_FRIENDS);
            }
            else
            {
                this->PushResult("Failed to start (invalid ROS credentials?)");
            }
        }
        else if((UiSocialClubMainMenu::ACTION_COUNT_ALL_FRIENDS == (size_t)arg) && m_Gamer)
        {
            static int s_NumBlocked = 0;
            static int s_NumFriends = 0;
            static int s_NumInvitesReceived = 0;
            static int s_NumInvitesSent = 0;

            if (rlScFriends::CountAll(m_Gamer->GetLocalIndex(), 
                                      &s_NumBlocked,
                                      &s_NumFriends,
                                      &s_NumInvitesReceived,
                                      &s_NumInvitesSent,
                                      &m_MyStatus))
            {
                this->PushSegue(SEGUE_READING_SC_FRIENDS);
            }
            else
            {
                this->PushResult("Failed to start (invalid ROS credentials?)");
            }
        }
        else if((UiSocialClubMainMenu::ACTION_READ_FRIENDS == (size_t)arg) && m_Gamer)
        {
            static rlScFriend s_ScFriends[300];
            static unsigned s_Count;
            static unsigned s_Total;

            if (rlScFriends::GetFriends(m_Gamer->GetLocalIndex(), 
                                        0, 
                                        NELEM(s_ScFriends),
                                        s_ScFriends,
                                        sizeof(rlScFriend),
                                        &s_Count,
                                        &s_Total,
                                        &m_MyStatus))
            {
                this->PushSegue(SEGUE_READING_SC_FRIENDS);
            }
            else
            {
                this->PushResult("Failed to start reading (invalid ROS credentials?)");
            }
        }
        else if((UiSocialClubMainMenu::ACTION_READ_FRIENDS_AND_INVITES_SENT == (size_t)arg) && m_Gamer)
        {
            static rlScFriend s_ScFriends[500];
            static unsigned s_Count;
            static unsigned s_Total;

            if(rlScFriends::GetFriendsAndInvitesSent(m_Gamer->GetLocalIndex(), 
                                                     0, 
                                                     NELEM(s_ScFriends),
                                                     s_ScFriends,
                                                     sizeof(rlScFriend),
                                                     &s_Count,
                                                     &s_Total,
                                                     &m_MyStatus))
            {
                this->PushSegue(SEGUE_READING_SC_FRIENDS);
            }
            else
            {
                this->PushResult("Failed to start reading (invalid ROS credentials?)");
            }
        }
        else if((UiSocialClubMainMenu::ACTION_READ_INVITES_SENT == (size_t)arg) && m_Gamer)
        {
            static rlScFriendInvite s_ScFriends[300];
            static unsigned s_Count;
            static unsigned s_Total;

            if(rlScFriends::GetInvitesSent(m_Gamer->GetLocalIndex(), 
                                           0, 
                                           NELEM(s_ScFriends),
                                           s_ScFriends,
                                           sizeof(rlScFriendInvite),
                                           &s_Count,
                                           &s_Total,
                                           &m_MyStatus))
            {
                this->PushSegue(SEGUE_READING_SC_FRIENDS);
            }
            else
            {
                this->PushResult("Failed to start reading (invalid ROS credentials?)");
            }
        }
        else if((UiSocialClubMainMenu::ACTION_READ_INVITES_RECEIVED == (size_t)arg) && m_Gamer)
        {
            static rlScFriendInvite s_ScFriends[300];
            static unsigned s_Count;
            static unsigned s_Total;

            if(rlScFriends::GetInvitesReceived(m_Gamer->GetLocalIndex(), 
                                               0, 
                                               NELEM(s_ScFriends),
                                               s_ScFriends,
                                               sizeof(rlScFriendInvite),
                                               &s_Count,
                                               &s_Total,
                                               &m_MyStatus))
            {
                this->PushSegue(SEGUE_READING_SC_FRIENDS);
            }
            else
            {
                this->PushResult("Failed to start reading (invalid ROS credentials?)");
            }
        }
        else if((UiSocialClubMainMenu::ACTION_READ_BLOCKED == (size_t)arg) && m_Gamer)
        {
            static rlScBlockedPlayer s_ScFriends[300];
            static unsigned s_Count;
            static unsigned s_Total;

            if(rlScFriends::GetBlocked(m_Gamer->GetLocalIndex(), 
                                       0, 
                                       NELEM(s_ScFriends),
                                       s_ScFriends,
                                       sizeof(rlScBlockedPlayer),
                                       &s_Count,
                                       &s_Total,
                                       &m_MyStatus))
            {
                this->PushSegue(SEGUE_READING_SC_FRIENDS);
            }
            else
            {
                this->PushResult("Failed to start reading (invalid ROS credentials?)");
            }
        }
        else if((UiSocialClubMainMenu::ACTION_CHECK_EMAIL == (size_t)arg) && m_Gamer)
        {
            if(rlSocialClub::CheckEmail(m_Gamer->GetLocalIndex(),
                                        "rtrickey@rockstarsandiego.com",
                                        &m_MyStatus))
            {
                this->PushSegue(SEGUE_GENERIC);
            }
            else
            {
                this->PushResult("Failed to start reading (invalid ROS credentials?)");
            }
        }
        else if((UiSocialClubMainMenu::ACTION_CHECK_NICKNAME == (size_t)arg) && m_Gamer)
        {
            if(rlSocialClub::GetAlternateNicknames(m_Gamer->GetLocalIndex(),
                                                   "rtrickey",
                                                   g_AlternateNicknames,
                                                   NELEM(g_AlternateNicknames),
                                                   &g_NumAlternateNicknames,
                                                   &m_MyStatus))
            {
                this->PushSegue(SEGUE_GENERIC);
            }
            else
            {
                this->PushResult("Failed to start reading (invalid ROS credentials?)");
            }
        }
#if RSG_PC
		else if((UiSocialClubMainMenu::ACTION_GO_ONLINE == (size_t)arg))
		{
			g_rlPc.GetUiInterface()->ReloadUi(false);
		}
		else if((UiSocialClubMainMenu::ACTION_GO_OFFLINE == (size_t)arg))
		{
			g_rlPc.GetUiInterface()->ReloadUi(true);
		}
		else if((UiSocialClubMainMenu::ACTION_POST_SC_TELEMETRY == (size_t)arg))
		{
			if (g_rlPc.GetTelemetry()->HasMemoryForMetric())
			{
				char buf[255];
				RsonWriter rw;
				rw.Init(buf, sizeof(buf), RSON_FORMAT_JSON);
				rw.WriteInt("a", 1);
				rw.WriteString("b", "test");
				g_rlPc.GetTelemetry()->Write("sctest", rw.ToString());
			}
		}
		else if((UiSocialClubMainMenu::ACTION_DEVICE_LOST_RESET == (size_t)arg))
		{
#if !__FINAL
			g_rlPc.SimluateDeviceLost();
			g_rlPc.SimluateDeviceReset();
#endif
		}
#endif
		else if((UiSocialClubMainMenu::ACTION_CREATE_SC_AUTH_TOKEN == (size_t)arg))
		{
			const rlRosCredentials& cred = rlRos::GetCredentials(m_Gamer->GetLocalIndex());
			if(cred.IsValid())
			{
				if(rlSocialClub::CreateScAuthToken(m_Gamer->GetLocalIndex(), cred.GetTicket(), scAuthToken, RLSC_MAX_SCAUTHTOKEN_CHARS, "Game", (30 * 60 * 24) /* 30 days */, NULL, &m_MyStatus))
				{
					this->PushSegue(SEGUE_GENERIC);
				}
				else
				{
					this->PushResult("Failed to start reading (invalid ROS credentials?)");
				}
			}
		}
		else if (UiSocialClubMainMenu::ACTION_CREATE_SIGNIN_TRANSFER == (size_t)arg)
		{
#if RSG_PC
			g_rlPc.GetProfileManager()->CreateSigninTransferProfile();
#endif
		}
    }
	else if(&m_UiCommerceAndEntitlementMainMenu == el)
	{
		if((UiCommerceAndEntitlementsMainMenu::ACTION_START_COMMERCE == (size_t)arg) && m_Gamer)
		{
#if RSG_PC
			g_rlPc.GetCommerceManager()->InitData();
#endif
		}
		else if((UiCommerceAndEntitlementsMainMenu::ACTION_GET_COMMERCE_DATA == (size_t)arg) && m_Gamer)
		{
#if RSG_PC
			g_rlPc.GetCommerceManager()->StartDataFetch("{\"languageCode\":\"en\"}");
#endif
		}
		else if((UiCommerceAndEntitlementsMainMenu::ACTION_DUMP_COMMERCE == (size_t)arg) && m_Gamer)
		{
#if RSG_PC
			g_rlPc.GetCommerceManager()->GetDataAsJson();
			g_rlPc.GetCommerceManager()->GetItemDataAsJson("Cash");
			g_rlPc.GetCommerceManager()->GetItemDataAsJson("collectors_ed");

#endif
		}
		else if((UiCommerceAndEntitlementsMainMenu::ACTION_SHUTDOWN_COMMERCE == (size_t)arg) && m_Gamer)
		{
#if RSG_PC
			g_rlPc.GetCommerceManager()->DestroyData();
#endif
		}
		else if ((UiCommerceAndEntitlementsMainMenu::ACTION_SUBMIT_TEST_VOUCHER_FOR_CONTENT == (size_t)arg) && m_Gamer)
		{
#if RSG_PC
			g_rlPc.GetCommerceManager()->RequestVoucherContent("{\"voucherCode\":\"TEST-GTAV-SHARK-MULTI\"}");
#endif
		}
		else if ((UiCommerceAndEntitlementsMainMenu::ACTION_CONSUME_PREVIEWED_VOUCHER == (size_t)arg) && m_Gamer)
		{
#if RSG_PC
			g_rlPc.GetCommerceManager()->RequestVoucherConsumption("{\"voucherCode\":\"TEST-GTAV-SHARK-MULTI\"}");
#endif
		}
		else if ((UiCommerceAndEntitlementsMainMenu::ACTION_TEST_CHECKOUT_URL_FETCH == (size_t)arg) && m_Gamer)
		{
#if RSG_PC
			g_rlPc.GetCommerceManager()->RequestUrlForCheckout("{\"productId\":\"GTA5-PC-SharkCard\"}");
#endif
		}
		else if ((UiCommerceAndEntitlementsMainMenu::ACTION_TEST_AGE_FOR_STORE == (size_t)arg) && m_Gamer)
		{
#if RSG_PC
			g_rlPc.GetCommerceManager()->GetUserAgeAppropriateData();
#endif
		}
		else if ((UiCommerceAndEntitlementsMainMenu::ACTION_START_STEAM_TEST_PURCHASE == (size_t)arg) && m_Gamer)
		{
#if __STEAM_BUILD
			g_rlPc.GetCommerceManager()->StartSteamPurchase("{\"productId\":\"GTA5-PC-SharkCard\"}");
#endif
		}
		else if ((UiCommerceAndEntitlementsMainMenu::ACTION_REQUEST_ENTITLEMENT_DATA == (size_t)arg) && m_Gamer)
		{
#if RSG_PC
			g_rlPc.GetCommerceManager()->RequestEntitlementData("Wibble","en");
#endif
		}
		else if ((UiCommerceAndEntitlementsMainMenu::ACTION_DUMP_ENTITLEMENT_DATA == (size_t)arg) && m_Gamer)
		{
#if RSG_PC
			g_rlPc.GetCommerceManager()->GetEntitlementDataAsJson();
#endif
		}
	}
	else if(&m_UiFacebookMainMenu == el)
	{
		if((UiFacebookMainMenu::ACTION_GETACCESSTOKEN_UI == (size_t)arg) && m_Gamer)
		{
#if RL_FACEBOOK_ENABLED
			if (rlFacebook::GetPlatformAccessTokenWithGui(m_Gamer->GetLocalIndex(), &m_MyStatus))
			{
				this->PushSegue(SEGUE_FACEBOOK_GET_ACCESS_TOKEN);
			}
			else
#endif // RL_FACEBOOK_ENABLED
			{
				this->PushResult("Get access token failed");
			}
		}
		else if ((UiFacebookMainMenu::ACTION_GETACCESSTOKEN_NO_UI == (size_t)arg) && m_Gamer)
		{
#if RL_FACEBOOK_ENABLED
			if (rlFacebook::GetPlatformAccessTokenNoGui(m_Gamer->GetLocalIndex(), &m_MyStatus))
			{
				this->PushSegue(SEGUE_FACEBOOK_GET_ACCESS_TOKEN);
			}
			else
#endif // #if RL_FACEBOOK_ENABLED
			{
				this->PushResult("Get access token failed");
			}
		}
		else if ((UiFacebookMainMenu::ACTION_POSTOPENGRAPH == (size_t)arg) && m_Gamer)
		{
#if RL_FACEBOOK_ENABLED
			// Facebook code
			if (rlFacebook::PostOpenGraph(m_Gamer->GetLocalIndex(),  "create", "character", "gtaonline", NULL, m_FacebookOGPostId, 64, &m_MyStatus))
			{
				this->PushSegue(SEGUE_FACEBOOK_POST_OPEN_GRAPH);
			}
			else
#endif // #if RL_FACEBOOK_ENABLED
			{
				this->PushResult("Failed to post facebook open graph.");
			}
		}
	}
    else if(&m_UiCloudMainMenu == el)
    {
        if(m_Gamer
            && (UiCloudMainMenu::ACTION_MEMBER_SC_DOWNLOAD == (size_t)arg
                || UiCloudMainMenu::ACTION_MEMBER_NATIVE_DOWNLOAD == (size_t)arg
                || UiCloudMainMenu::ACTION_TITLE_DOWNLOAD == (size_t)arg))
        {
            m_Gb.Clear();

            rlCloudOnlineService onlineSvc;
            const char* path;
            if(UiCloudMainMenu::ACTION_TITLE_DOWNLOAD == (size_t)arg)
            {
                path = "lipsum.txt";

                AssertVerify(rlCloud::GetTitleFile(path,
                                                    RLROS_SECURITY_DEFAULT,
                                                    0,
                                                    NET_HTTP_OPTIONS_NONE,
                                                    m_Gb.GetFiDevice(),
                                                    m_Gb.GetFiHandle(),
                                                    &m_FileInfo,
                                                    NULL,   //allocator
                                                    &m_MyStatus));
            }
            else
            {
                if(UiCloudMainMenu::ACTION_MEMBER_SC_DOWNLOAD == (size_t)arg)
                {
                    onlineSvc = RL_CLOUD_ONLINE_SERVICE_SC;
                }
                else
                {
                    onlineSvc = RL_CLOUD_ONLINE_SERVICE_NATIVE;
                }

                switch((int) m_UiCloudMainMenu.GetAccessType())
                {
                case UiCloudMainMenu::ACCESS_SECURE:
                    path = "secure/mp3/rosgamerssettings.json";
                    break;
                case UiCloudMainMenu::ACCESS_SHARE:
                    path = "share/samplesession/lipsum.txt";
                    break;
                case UiCloudMainMenu::ACCESS_PUBLISH:
                    path = "publish/samplesession/lipsum.txt";
                    break;
                default:
                    path = "samplesession/lipsum.txt";
                    break;
                }

                /*const fiDeviceCloud& dc = fiDeviceCloud::GetInstance();
                m_fiCloudHandle = dc.GetFile(m_Gamer->GetLocalIndex(),
                                                RL_CLOUD_NAMESPACE_MEMBERS,
                                                onlineSvc,
                                                path);
                Assert(fiHandleInvalid != m_fiCloudHandle);*/

                AssertVerify(rlCloud::GetMemberFile(m_Gamer->GetLocalIndex(),
                                                    rlCloud::INVALID_MEMBER_ID,
                                                    onlineSvc,
                                                    path,
                                                    RLROS_SECURITY_DEFAULT,
                                                    0,
                                                    NET_HTTP_OPTIONS_NONE,
                                                    m_Gb.GetFiDevice(),
                                                    m_Gb.GetFiHandle(),
                                                    &m_FileInfo,
                                                    NULL,   //allocator
                                                    &m_MyStatus));
            }

            this->PushSegue(SEGUE_CLOUD_DOWNLOAD);
        }
        else if(m_Gamer
            && (UiCloudMainMenu::ACTION_MEMBER_SC_UPLOAD == (size_t)arg
                || UiCloudMainMenu::ACTION_MEMBER_SC_APPEND == (size_t)arg
                || UiCloudMainMenu::ACTION_MEMBER_NATIVE_UPLOAD == (size_t)arg
                || UiCloudMainMenu::ACTION_MEMBER_NATIVE_APPEND == (size_t)arg))
        {
            static const char lipsum[] =
            {
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut vulputate tortor. Aenean nulla nisl, ornare in tincidunt sed, vestibulum quis tortor. Duis sed nulla vitae sem vestibulum blandit. Sed malesuada metus eu diam mollis consectetur. Nulla eu nibh quis libero dictum molestie. Nullam tempor elit ac dui molestie dictum vitae a diam. Fusce interdum tristique fringilla. Pellentesque luctus placerat massa vitae mollis. Vivamus interdum neque in leo venenatis hendrerit. Donec gravida turpis nec arcu aliquet fringilla. Mauris eleifend suscipit venenatis."
            };
            m_Gb.Clear();
            rlCloud::PostType postType;
            if(UiCloudMainMenu::ACTION_MEMBER_SC_APPEND == (size_t)arg
                || UiCloudMainMenu::ACTION_MEMBER_NATIVE_APPEND == (size_t)arg)
            {
                postType = rlCloud::POST_APPEND;
            }
            else
            {
                postType = rlCloud::POST_REPLACE;
            }

            rlCloudOnlineService onlineSvc;
            if(UiCloudMainMenu::ACTION_MEMBER_SC_UPLOAD == (size_t)arg
                || UiCloudMainMenu::ACTION_MEMBER_SC_APPEND == (size_t)arg)
            {
                onlineSvc = RL_CLOUD_ONLINE_SERVICE_SC;
            }
            else
            {
                onlineSvc = RL_CLOUD_ONLINE_SERVICE_NATIVE;
            }

            const char* path;
            switch((int) m_UiCloudMainMenu.GetAccessType())
            {
            case UiCloudMainMenu::ACCESS_SHARE:
                path = "share/samplesession/lipsum.txt";
                break;
            case UiCloudMainMenu::ACCESS_PUBLISH:
                path = "publish/samplesession/lipsum.txt";
                break;
            default:
                path = "samplesession/lipsum.txt";
                break;
            }

            static rlCloudFileInfo s_FileInfo;
            AssertVerify(rlCloud::PostMemberFile(m_Gamer->GetLocalIndex(),
                                                onlineSvc,
                                                path,
                                                lipsum,
                                                (unsigned int)strlen(lipsum),
                                                postType,
                                                &s_FileInfo,
                                                NULL,   //allocator
                                                &m_MyStatus));

            this->PushSegue(SEGUE_CLOUD_UPLOAD);
        }
        else if(m_Gamer
            && (UiCloudMainMenu::ACTION_MEMBER_SC_DELETE == (size_t)arg
                || UiCloudMainMenu::ACTION_MEMBER_NATIVE_DELETE == (size_t)arg))
        {
            rlCloudOnlineService onlineSvc;
            if(UiCloudMainMenu::ACTION_MEMBER_SC_DELETE == (size_t)arg)
            {
                onlineSvc = RL_CLOUD_ONLINE_SERVICE_SC;
            }
            else
            {
                onlineSvc = RL_CLOUD_ONLINE_SERVICE_NATIVE;
            }

            const char* path;
            switch((int) m_UiCloudMainMenu.GetAccessType())
            {
            case UiCloudMainMenu::ACCESS_SHARE:
                path = "share/samplesession/lipsum.txt";
                break;
            case UiCloudMainMenu::ACCESS_PUBLISH:
                path = "publish/samplesession/lipsum.txt";
                break;
            default:
                path = "samplesession/lipsum.txt";
                break;
            }

            m_Gb.Clear();
            AssertVerify(rlCloud::DeleteMemberFile(m_Gamer->GetLocalIndex(),
                                                onlineSvc,
                                                path,
                                                NULL,
                                                &m_MyStatus));

            this->PushSegue(SEGUE_CLOUD_DELETE);
        }
#if RSG_PC && 1
		else if (m_Gamer &&
			(UiCloudMainMenu::ACTION_SPSAVES_INIT == (size_t) arg ||
			UiCloudMainMenu::ACTION_SPSAVES_IDENTIFY_CONFLICTS == (size_t)arg || 
			UiCloudMainMenu::ACTION_SPSAVES_REGISTER_FILE == (size_t)arg ||
			UiCloudMainMenu::ACTION_SPSAVES_GET_FILE == (size_t)arg || 
			UiCloudMainMenu::ACTION_SPSAVES_POST_FILE == (size_t)arg) ||
			UiCloudMainMenu::ACTION_SPSAVES_MERGE_FILES == (size_t)arg ||
			UiCloudMainMenu::ACTION_SPSAVES_UNREGISTER_FILE == (size_t)arg ||
			UiCloudMainMenu::ACTION_SPSAVES_DELETE_FILES == (size_t)arg ||
			UiCloudMainMenu::ACTION_SPSAVES_QUERY_BETA == (size_t)arg ||
			UiCloudMainMenu::ACTION_SPSAVES_WRITE_TELEMETRY == (size_t)arg ||
			UiCloudMainMenu::ACTION_SPSAVES_BACKUP_CONFLICTS == (size_t)arg ||
			UiCloudMainMenu::ACTION_SPSAVES_SEARCH_BACKUPS == (size_t)arg ||
			UiCloudMainMenu::ACTION_SPSAVES_GET_BACKUP_INFO == (size_t)arg ||
			UiCloudMainMenu::ACTION_SPSAVES_RESTORE_BACKUP == (size_t)arg ||
			UiCloudMainMenu::ACTION_SPSAVES_GET_METADATA == (size_t)arg)
		{
			class SampleSessionConfiguration : public rgsc::ICloudSaveTitleConfigurationV1
			{
			public:
				rgsc::RGSC_HRESULT RGSC_CALL QueryInterface(rgsc::RGSC_REFIID riid, void** ppvObj)
				{
					IRgscUnknown *pUnknown = NULL;

					if(ppvObj == NULL)
					{
						return rgsc::RGSC_INVALIDARG;
					}

					if(riid == rgsc::IID_IRgscUnknown)
					{
						pUnknown = static_cast<ICloudSaveTitleConfiguration*>(this);
					}
					else if(riid == rgsc::IID_ICloudSaveTitleConfigurationV1)
					{
						pUnknown = static_cast<ICloudSaveTitleConfigurationV1*>(this);
					}

					*ppvObj = pUnknown;
					if(pUnknown == NULL)
					{
						return rgsc::RGSC_NOINTERFACE;
					}

					return rgsc::RGSC_OK;
				}

				const char* RGSC_CALL GetTitleName()
				{
					return "gta5";
				}

				u32 RGSC_CALL GetNumCloudFiles()
				{
					return 16;
				}

				const char* RGSC_CALL GetFileName(unsigned index)
				{
					switch (index)
					{
					case  0: return "SGTA50000";
					case  1: return "SGTA50001";
					case  2: return "SGTA50002";
					case  3: return "SGTA50003";
					case  4: return "SGTA50004";
					case  5: return "SGTA50005";
					case  6: return "SGTA50006";
					case  7: return "SGTA50007";
					case  8: return "SGTA50008";
					case  9: return "SGTA50009";
					case 10: return "SGTA50010";
					case 11: return "SGTA50011";
					case 12: return "SGTA50012";
					case 13: return "SGTA50013";
					case 14: return "SGTA50014";
					case 15: return "SGTA50015";
					default: return NULL;
					}
				}

				const char* RGSC_CALL GetSaveDirectory()
				{
					static char savePath[rgsc::RGSC_MAX_PATH];
					g_rlPc.GetFileSystem()->GetTitleProfileDirectory(savePath, true);
					return savePath;
				}

				const char* RGSC_CALL GetHardwareId()
				{
					static char machineName[MAX_COMPUTERNAME_LENGTH + 1] = {0};
					DWORD machineNameBufSize = COUNTOF(machineName);
					GetComputerNameA(machineName, &machineNameBufSize);
					return machineName;
				}
			};

			static rgsc::ICloudSaveFile* getFile = NULL;
			static rgsc::ICloudSaveFile* postFile = NULL;
			static SampleSessionConfiguration ssConfig;

			if (UiCloudMainMenu::ACTION_SPSAVES_INIT == (size_t)arg)
			{
				rgscManifest = g_rlPc.GetCloudSaveManager()->RegisterTitle(&ssConfig);
				rgsc::ICloudSaveManifestV2* manifestV2 = NULL;
				HRESULT hr = rgscManifest->QueryInterface(rgsc::IID_ICloudSaveManifestV2, (void**)&manifestV2);
				
				// load the local manifest
				manifestV2->Load();
				if (SUCCEEDED(hr) && manifestV2->GetCloudSaveEnabled() == rgsc::ICloudSaveManifestV2::CloudSavesEnabledState::UNKNOWN)
				{
					manifestV2->SetCloudSavesEnabled(rgsc::ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES);
					manifestV2->Save();
				}

				m_UiCloudMainMenu.EnableCloudSaveOperations();
			}
			else if (UiCloudMainMenu::ACTION_SPSAVES_IDENTIFY_CONFLICTS == (size_t)arg)
			{
				if (rgscManifest)
				{
					rgsc::ICloudSaveManifestV2* manifestV2 = NULL;
					if (SUCCEEDED(rgscManifest->QueryInterface(rgsc::IID_ICloudSaveManifestV2, (void**)&manifestV2)))
					{
						manifestV2->Clear();
					}
										
					manifestV2->SetCloudSavesEnabled(rgsc::ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES);

					if (m_RgscStatus == NULL && SUCCEEDED(g_rlPc.GetTaskManager()->CreateAsyncStatus(((rgsc::IAsyncStatus**)&m_RgscStatus))))
					{
						g_rlPc.GetCloudSaveManager()->IdentifyConflicts(rgscManifest, m_RgscStatus);
						this->PushSegue(SEGUE_CLOUD_SAVE_IDENTIFY_CONFLICTS);
					}
				}
			}
			else if (UiCloudMainMenu::ACTION_SPSAVES_REGISTER_FILE == (size_t)arg)
			{
				char path[RAGE_MAX_PATH];
				safecpy(path, ssConfig.GetSaveDirectory());
				safecat(path, "SGTA50000");

				const fiDevice* device = fiDevice::GetDevice(path);

				static u64 TicksPerSecond = 10000000;
				u64 fileTime = device->GetFileTime(path);
				fileTime += (TicksPerSecond * 60 * 5); // add 5 minutes to the file time
				device->SetFileTime(path, fileTime);

				g_rlPc.GetCloudSaveManager()->RegisterFileForUpload(rgscManifest, "SGTA50000", "{\"Autosave\":0,\"Slot\":3,\"LastMission\":\"In the beginning\",\"completionPct\":2.39130,\"PosixTime\":1459360993,\"playerCharacterIndex\":0}");
				this->PushSegue(SEGUE_CLOUD_REGISTER_FILE);
				m_MyStatus.SetPending();
				m_MyStatus.SetSucceeded();
			}
			else if (UiCloudMainMenu::ACTION_SPSAVES_GET_FILE == (size_t)arg)
			{
				if (rgscManifest)
				{
					rgsc::ICloudSaveManifestV2* manifestV2 = NULL;
					if (SUCCEEDED(rgscManifest->QueryInterface(rgsc::IID_ICloudSaveManifestV2, (void**)&manifestV2)))
					{
						manifestV2->SetCloudSavesEnabled(rgsc::ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES);

						for (unsigned i = 0; i < manifestV2->GetNumFiles(); i++)
						{
							rgsc::ICloudSaveFile* f = manifestV2->GetFile(i);
							rgsc::ICloudSaveFileV2* fV2;
							if (SUCCEEDED(f->QueryInterface(rgsc::IID_ICloudSaveFileV2, (void**)&fV2)))
							{
								if (fV2->GetConflictState() == rgsc::ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict ||
									fV2->GetConflictState() == rgsc::ICloudSaveFileV2::ConflictState::CS_ServerHasNewerWithConflict ||
									fV2->GetConflictState() == rgsc::ICloudSaveFileV2::ConflictState::CS_FileDeletedWithConflict)
								{
									fV2->SetResolveType(rgsc::ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptRemote);
								}
							}
						}
					}

					if (m_RgscStatus == NULL && SUCCEEDED(g_rlPc.GetTaskManager()->CreateAsyncStatus(((rgsc::IAsyncStatus**)&m_RgscStatus))))
					{
						g_rlPc.GetCloudSaveManager()->GetUpdatedFiles(rgscManifest, m_RgscStatus);
						this->PushSegue(SEGUE_CLOUD_SAVE_GET_FILE);
					}
				}
			}
			else if (UiCloudMainMenu::ACTION_SPSAVES_POST_FILE == (size_t)arg)
			{
				if (rgscManifest)
				{
					rgsc::ICloudSaveManifestV2* manifestV2 = NULL;
					if (SUCCEEDED(rgscManifest->QueryInterface(rgsc::IID_ICloudSaveManifestV2, (void**)&manifestV2)))
					{
						manifestV2->SetCloudSavesEnabled(rgsc::ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES);

						for (unsigned i = 0; i < manifestV2->GetNumFiles(); i++)
						{
							rgsc::ICloudSaveFile* f = manifestV2->GetFile(i);
							rgsc::ICloudSaveFileV2* fV2;
							if (SUCCEEDED(f->QueryInterface(rgsc::IID_ICloudSaveFileV2, (void**)&fV2)))
							{
								if (fV2->GetConflictState() == rgsc::ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict ||
									fV2->GetConflictState() == rgsc::ICloudSaveFileV2::ConflictState::CS_ServerHasNewerWithConflict ||
									fV2->GetConflictState() == rgsc::ICloudSaveFileV2::ConflictState::CS_FileDeletedWithConflict)
								{
									fV2->SetResolveType(rgsc::ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptLocal);
								}
							}
						}
					}

					if (m_RgscStatus == NULL && SUCCEEDED(g_rlPc.GetTaskManager()->CreateAsyncStatus(((rgsc::IAsyncStatus**)&m_RgscStatus))))
					{
						g_rlPc.GetCloudSaveManager()->PostUpdatedFiles(rgscManifest, m_RgscStatus);
						this->PushSegue(SEGUE_CLOUD_SAVE_POST_FILE);
					}
				}
			}
			else if (UiCloudMainMenu::ACTION_SPSAVES_MERGE_FILES == (size_t)arg)
			{
				if (rgscManifest)
				{
					if (m_RgscStatus == NULL && SUCCEEDED(g_rlPc.GetTaskManager()->CreateAsyncStatus(((rgsc::IAsyncStatus**)&m_RgscStatus))))
					{
						g_rlPc.GetCloudSaveManager()->CopyUpdatedFiles(rgscManifest, m_RgscStatus);
						this->PushSegue(SEGUE_CLOUD_MERGE_FILES);
					}
				}
			}
			else if (UiCloudMainMenu::ACTION_SPSAVES_UNREGISTER_FILE == (size_t)arg)
			{
				char path[RAGE_MAX_PATH];
				safecpy(path, ssConfig.GetSaveDirectory());
				safecat(path, "SGTA50000");

				const fiDevice* device = fiDevice::GetDevice(path);
				device->Delete(path);

				g_rlPc.GetCloudSaveManager()->UnregisterFile(rgscManifest, "SGTA50000");
				this->PushSegue(SEGUE_CLOUD_UNREGISTER_FILE);
				m_MyStatus.SetPending();
				m_MyStatus.SetSucceeded();
			}
			else if (UiCloudMainMenu::ACTION_SPSAVES_DELETE_FILES == (size_t)arg)
			{
				if (rgscManifest)
				{
					rgsc::ICloudSaveManifestV2* manifestV2 = NULL;
					if (SUCCEEDED(rgscManifest->QueryInterface(rgsc::IID_ICloudSaveManifestV2, (void**)&manifestV2)))
					{
						manifestV2->SetCloudSavesEnabled(rgsc::ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES);

						for (unsigned i = 0; i < manifestV2->GetNumFiles(); i++)
						{
							rgsc::ICloudSaveFile* f = manifestV2->GetFile(i);
							rgsc::ICloudSaveFileV2* fV2;
							if (SUCCEEDED(f->QueryInterface(rgsc::IID_ICloudSaveFileV2, (void**)&fV2)))
							{
								if (fV2->GetConflictState() == rgsc::ICloudSaveFileV2::ConflictState::CS_ClientHasNewerWithConflict ||
									fV2->GetConflictState() == rgsc::ICloudSaveFileV2::ConflictState::CS_ServerHasNewerWithConflict ||
									fV2->GetConflictState() == rgsc::ICloudSaveFileV2::ConflictState::CS_FileDeletedWithConflict)
								{
									fV2->SetResolveType(rgsc::ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptLocal);
								}
							}
						}
					}

					if (m_RgscStatus == NULL && SUCCEEDED(g_rlPc.GetTaskManager()->CreateAsyncStatus(((rgsc::IAsyncStatus**)&m_RgscStatus))))
					{
						g_rlPc.GetCloudSaveManager()->DeleteUpdatedfiles(rgscManifest, m_RgscStatus);
						this->PushSegue(SEGUE_CLOUD_DELETE_FILES);
					}
				}
			}
			else if (UiCloudMainMenu::ACTION_SPSAVES_QUERY_BETA == (size_t)arg)
			{
				static bool outResult = false;
				if (m_RgscStatus == NULL && SUCCEEDED(g_rlPc.GetTaskManager()->CreateAsyncStatus(((rgsc::IAsyncStatus**)&m_RgscStatus))))
				{
					g_rlPc.GetCloudSaveManager()->HasBetaAccessAsync("test/csbcheck.json", &outResult, m_RgscStatus);
					this->PushSegue(SEGUE_CLOUD_QUERY_BETA);
				}
			}
			else if (UiCloudMainMenu::ACTION_SPSAVES_WRITE_TELEMETRY == (size_t)arg)
			{
				g_rlPc.GetCloudSaveManager()->WriteConflictTelemetry(rgscManifest);
				g_rlPc.GetTelemetry()->Flush(true);
			}
			else if (UiCloudMainMenu::ACTION_SPSAVES_BACKUP_CONFLICTS == (size_t)arg)
			{
				if (rgscManifest)
				{
					rgsc::ICloudSaveManifestV2* manifestV2 = NULL;
					if (SUCCEEDED(rgscManifest->QueryInterface(rgsc::IID_ICloudSaveManifestV2, (void**)&manifestV2)))
					{
						for (unsigned i = 0; i < manifestV2->GetNumFiles(); i++)
						{
							rgsc::ICloudSaveFile* f = manifestV2->GetFile(i);
							rgsc::ICloudSaveFileV2* fV2;
							if (SUCCEEDED(f->QueryInterface(rgsc::IID_ICloudSaveFileV2, (void**)&fV2)))
							{
								fV2->SetConflictState(rgsc::ICloudSaveFileV2::ConflictState::CS_ServerHasNewerWithConflict);
								fV2->SetResolveType(rgsc::ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptLocal);
							}
						}

						if (m_RgscStatus == NULL && SUCCEEDED(g_rlPc.GetTaskManager()->CreateAsyncStatus(((rgsc::IAsyncStatus**)&m_RgscStatus))))
						{
							g_rlPc.GetCloudSaveManager()->BackupConflictedFiles(rgscManifest, m_RgscStatus);
							this->PushSegue(SEGUE_CLOUD_BACKUP_CONFLICTS);
						}
					}
				}
			}
			else if(UiCloudMainMenu::ACTION_SPSAVES_SEARCH_BACKUPS == (size_t)arg)
			{
				if (rgscManifest)
				{
					if (m_RgscStatus == NULL && SUCCEEDED(g_rlPc.GetTaskManager()->CreateAsyncStatus(((rgsc::IAsyncStatus**)&m_RgscStatus))))
					{
						g_rlPc.GetCloudSaveManager()->SearchForBackups(rgscManifest, m_RgscStatus);
						this->PushSegue(SEGUE_CLOUD_SEARCH_BACKUPS);
					}
				}
			}
			else if(UiCloudMainMenu::ACTION_SPSAVES_GET_BACKUP_INFO == (size_t)arg)
			{
				if (rgscManifest)
				{
					Assert(rgscBackupInfo == NULL);
					rgscBackupInfo = g_rlPc.GetCloudSaveManager()->AllocateBackupInfo();
					g_rlPc.GetCloudSaveManager()->GetBackupInfo(rgscManifest, 0, 0, rgscBackupInfo);
					g_rlPc.GetCloudSaveManager()->FreeBackupInfo(rgscBackupInfo);
					rgscBackupInfo = NULL;
				}
			}
			else if(UiCloudMainMenu::ACTION_SPSAVES_RESTORE_BACKUP == (size_t)arg)
			{
				if (rgscManifest)
				{
					if (m_RgscStatus == NULL && SUCCEEDED(g_rlPc.GetTaskManager()->CreateAsyncStatus(((rgsc::IAsyncStatus**)&m_RgscStatus))))
					{
						g_rlPc.GetCloudSaveManager()->RestoreBackup(rgscManifest, 0, 0, m_RgscStatus);
						this->PushSegue(SEGUE_CLOUD_RESTORE_BACKUP);
					}
				}
			}
			else if(UiCloudMainMenu::ACTION_SPSAVES_GET_METADATA == (size_t)arg)
			{
				if (rgscManifest)
				{
					rgsc::ICloudSaveManifestV2* manifestV2 = NULL;
					if (SUCCEEDED(rgscManifest->QueryInterface(rgsc::IID_ICloudSaveManifestV2, (void**)&manifestV2)))
					{
						for (unsigned i = 0; i < manifestV2->GetNumFiles(); i++)
						{
							rgsc::ICloudSaveFile* f = manifestV2->GetFile(i);
							rgsc::ICloudSaveFileV2* fV2;
							if (SUCCEEDED(f->QueryInterface(rgsc::IID_ICloudSaveFileV2, (void**)&fV2)))
							{
								fV2->SetConflictState(rgsc::ICloudSaveFileV2::ConflictState::CS_ServerHasNewerWithConflict);
								fV2->SetResolveType(rgsc::ICloudSaveFileV2::ConflictResolutionType::CRT_AcceptLocal);
							}
						}

						if (m_RgscStatus == NULL && SUCCEEDED(g_rlPc.GetTaskManager()->CreateAsyncStatus(((rgsc::IAsyncStatus**)&m_RgscStatus))))
						{
							g_rlPc.GetCloudSaveManager()->GetConflictMetadata(rgscManifest, m_RgscStatus);
							this->PushSegue(SEGUE_CLOUD_GET_METADATA);
						}
					}
				}
			}
		}
#endif // RSG_PC
    }
    else if(&m_UiPresenceMainMenu == el)
    {
        if((UiPresenceMainMenu::ACTION_PUBLISH_TO_FRIENDS == (size_t)arg) && m_Gamer)
        {
			rlPresence::PublishToManyFriends(m_Gamer->GetLocalIndex(), "{\"msg\":\"Hola Amigos!!!\"}");
        }
        else if((UiPresenceMainMenu::ACTION_PUBLISH_TO_CREW == (size_t)arg) && m_Gamer)
		{
            rlPresence::PublishToCrew(m_Gamer->GetLocalIndex(), "{\"msg\":\"Hola Crewmigos!!!\"}");
        }
        else if((UiPresenceMainMenu::ACTION_EXECUTE_QUERY == (size_t)arg) && m_Gamer)
        {
            char qparams[64];
            const char* query = m_UiPresenceMainMenu.GetSelectedQuery();

            if(AssertVerify(query)
                && AssertVerify(m_UiPresenceMainMenu.GetSelectedQueryParams(m_Gamer->GetLocalIndex(),
                                                                            qparams)))
            {
                AssertVerify(rlPresence::Query(m_Gamer->GetLocalIndex(),
                                                query,
                                                qparams,
                                                0,
                                                COUNTOF(m_ScPresQueryResults),
                                                m_ScPresQueryResultsBuf,
                                                sizeof(m_ScPresQueryResultsBuf),
                                                m_ScPresQueryResults,
                                                &m_NumScPresQueryResultsRetrieved,
                                                &m_NumScPresQueryResults,
                                                &m_MyStatus));

                this->PushSegue(SEGUE_PRESENCE_QUERY);
            }
        }
        else if((UiPresenceMainMenu::ACTION_QUERY_COUNT == (size_t)arg) && m_Gamer)
        {
            char qparams[64];
            const char* query = m_UiPresenceMainMenu.GetSelectedQuery();

            if(AssertVerify(query)
                && AssertVerify(m_UiPresenceMainMenu.GetSelectedQueryParams(m_Gamer->GetLocalIndex(),
                                                                            qparams)))
            {
                AssertVerify(rlPresence::QueryCount(m_Gamer->GetLocalIndex(),
                                                    query,
                                                    qparams,
                                                    &m_NumScPresQueryResults,
                                                    &m_MyStatus));

                this->PushSegue(SEGUE_PRESENCE_QUERY_COUNT);
            }
        }
    }
	else if(&m_UiRichPresenceMainMenu == el)
	{
		if ((UiRichPresenceMainMenu::ACTION_SET_RICH_PRESENCE == (size_t)arg) && m_Gamer)
		{
#if RSG_ORBIS
			rlPresence::SetStatusString("Idle");
#elif RSG_DURANGO
			events_durango::WriteTestEvent(m_Gamer->GetLocalIndex(), m_UiRichPresenceMainMenu.GetFieldString());
			g_rlXbl.GetPresenceManager()->SetRichPresence(m_Gamer->GetGamerInfo(), 
															m_UiRichPresenceMainMenu.GetPresenceString(), 
															&m_RichPresenceStatus);
#endif
		}
		else if ((UiRichPresenceMainMenu::ACTION_GET_RICH_PRESENCE == (size_t)arg) && m_Gamer)
		{
#if RSG_DURANGO
			g_rlXbl.GetPresenceManager()->GetRichPresence(m_Gamer->GetGamerInfo().GetLocalIndex(), m_Gamer->GetGamerHandle().GetXuid(), &m_RichPresenceStatus);
#endif
		}
	}
	else if(&m_UiAchievementsMainMenu == el)
	{
		if((UiAchievementsMainMenu::ACTION_AWARD_ACHIEVEMENT == (size_t)arg) && m_Gamer)
		{
			if(rlAchievementInited == false)
			{
				sm_rlAchievement.Init(1);
				rlAchievementInited = true;
			}

			int s_AchievementIdToAward = UiAchievementsMainMenu::GetAchievementIndex();
			AssertVerify(sm_rlAchievement.Write(m_Gamer->GetGamerInfo(),
												s_AchievementIdToAward,
												&m_MyStatus));
			this->PushSegue(SEGUE_ACHIEVEMENTS_AWARD_ACHIEVEMENT);
		}
		else if((UiAchievementsMainMenu::ACTION_READ_ACHIEVEMENTS == (size_t)arg) && m_Gamer)
		{
			if(rlAchievementInited == false)
			{
				sm_rlAchievement.Init(1);
				rlAchievementInited = true;
			}

#if RSG_PC
			rlDebug("sizeof(sm_AchievementsInfoEx): %" SIZETFMT "d", sizeof(sm_AchievementsInfoEx));
#else
			rlDebug("sizeof(sm_AchievementsInfo): %" SIZETFMT "d", sizeof(sm_AchievementsInfo));
#endif

 			rlGamerHandle gamerHandle = m_Gamer->GetGamerHandle();

// 			static bool s_ReadFriendAchievements = false;
// 			if(s_ReadFriendAchievements && (m_NumFriends > 0))
// 			{
// 				const rlFriend& f = m_Friends[0];
// 				f.GetGamerHandle(&gamerHandle);
// 			}

			//static int detailFlags = 0;

			rlAchievement::DetailFlags details = (rlAchievement::DetailFlags)(rlAchievement::ACHIEVEMENT_DETAILS_ALL);
#if RSG_PC
			AssertVerify(sm_rlAchievement.Read(m_Gamer->GetGamerInfo(), 
				gamerHandle,
				details,
				&sm_AchievementsInfoEx[0],
				0,
				COUNTOF(sm_AchievementsInfoEx), 
				&sm_NumAchRead, 
				&m_MyStatus));

#else
			AssertVerify(sm_rlAchievement.Read(m_Gamer->GetGamerInfo(), 
											   gamerHandle,
											   details,
											   &sm_AchievementsInfo[0],
											   0,
											   COUNTOF(sm_AchievementsInfo), 
											   &sm_NumAchRead, 
											   &m_MyStatus));
#endif
			//detailFlags++;

			this->PushSegue(SEGUE_ACHIEVEMENTS_READ_ACHIEVEMENTS);
		}
		else if((UiAchievementsMainMenu::ACTION_DELETE_ACHIEVEMENT == (size_t)arg) && m_Gamer)
		{
#if RSG_PC && __DEV
			if(rlAchievementInited == false)
			{
				sm_rlAchievement.Init(1);
				rlAchievementInited = true;
			}

			AssertVerify(sm_rlAchievement.DeleteAllAchievements(m_Gamer->GetGamerInfo(),
																&m_MyStatus));
#else
			m_MyStatus.SetPending();
			m_MyStatus.SetFailed();
#endif

			this->PushSegue(SEGUE_ACHIEVEMENTS_DELETE_ACHIEVEMENTS);
		}
        else if((UiAchievementsMainMenu::ACTION_GET_ROS_ACHIEVEMENTS == (size_t)arg) && m_Gamer)
        {
            static rlScAchievementInfo s_Achievements[10];
            static unsigned s_AchievementsCount;
            static unsigned s_AchievementsTotal;

            AssertVerify(rlScAchievements::GetAchievementsByGamer(
                m_Gamer->GetGamerInfo().GetLocalIndex(),
                m_Gamer->GetGamerHandle(),
                s_Achievements,
                0,
                NELEM(s_Achievements),
                &s_AchievementsCount,
                &s_AchievementsTotal,
                &m_MyStatus));

            this->PushSegue(SEGUE_ACHIEVEMENTS_GET_ROS_ACHIEVEMENTS);
        }
        else if((UiAchievementsMainMenu::ACTION_GET_ROS_ACHIEVEMENT_DEFINITIONS == (size_t)arg) && m_Gamer)
        {
            static rlScAchievementDefinitionInfo s_AchievementDefinitions[10];
            static int s_AchDefPageIndex = 0;
            static unsigned s_AchDefCount;
            static unsigned s_AchDefTotal;

            AssertVerify(rlScAchievements::GetAchievementDefinitions(
                m_Gamer->GetGamerInfo().GetLocalIndex(),
                s_AchievementDefinitions,
                s_AchDefPageIndex++,
                NELEM(s_AchievementDefinitions),
                &s_AchDefCount,
                &s_AchDefTotal,
                &m_MyStatus));

            this->PushSegue(SEGUE_ACHIEVEMENTS_GET_ROS_ACHIEVEMENT_DEFINITIONS);
        }
	}
    else if(&m_UiProfileStatsMainMenu == el)
    {
        if(m_Gamer)
        {
            if(UiProfileStatsMainMenu::ACTION_SYNCHRONIZE == (size_t)arg)
            {
                if(rlProfileStats::Synchronize(m_Gamer->GetLocalIndex(), &m_MyStatus))
                {
                    this->PushSegue(SEGUE_PROFILE_STATS_SYNCHRONIZE);
                }
                else
                {
                    PushResult("Failed (probably do not have a ticket yet)");
                }
            }
            else if(UiProfileStatsMainMenu::ACTION_READ_BY_GAMER == (size_t)arg)
            {
                if(this->ReadProfileStatsByGamer())
                {
                    this->PushSegue(SEGUE_PROFILE_STATS_READ_BY_GAMER);
                }
                else
                {
                    PushResult("Failed");
                }
            }
            else if(UiProfileStatsMainMenu::ACTION_SET_DIRTY == (size_t)arg)
            {
                //Set our stats to random values, and dirty them at priorities based on stat ID.
                unsigned numDirtied = 0;
                for(unsigned i = 0; i < m_NumLocalProfileStats; i++)
                {
                    if ((rand() % 100) <= PROFILE_STAT_DIRTY_PCT)
                    {
                        int newVal = rand() % 100;

                        rlProfileStatsValue& val = m_LocalProfileStats[i].m_StatValue;

                        switch(val.GetType())
                        {
                        case RL_PROFILESTATS_TYPE_FLOAT: val.SetFloat((float)newVal); break;
                        case RL_PROFILESTATS_TYPE_INT32: val.SetInt32((int)newVal); break;
                        case RL_PROFILESTATS_TYPE_INT64: val.SetInt64((s64)newVal); break;
                        
                        default: val.Clear(); break;
                        }

                        int statId = m_LocalProfileStats[i].m_StatId;
                        m_LocalProfileStats[i].m_bDirty = true;

                        m_LocalProfileStats[i].m_Priority = (rlProfileStatsPriority)(Abs(statId) % RL_PROFILESTATS_MAX_PRIORITY);

#if !__NO_OUTPUT
                        char buf[64];
                        rlDebug3("Dirtied stat id=%d  val=%s  priority=%d",
                                 statId, val.ToString(buf, sizeof(buf)), m_LocalProfileStats[i].m_Priority);
#endif

                        ++numDirtied;
                    }
                }

                char buf[128];
                formatf(buf, sizeof(buf), "Dirtied %u stats", numDirtied);
                PushResult(buf);
            }
            else if(UiProfileStatsMainMenu::ACTION_FLUSH == (size_t)arg)
            {
                //This isn't very efficient. To flush, we build a copy of all dirty stats,
                //sort them by priority, and then flush the first MAX_DIRTY_PROFILE_STATS
                //of them
                unsigned numDirtyProfileStats = 0;
                rlProfileDirtyStat dirtyStats[MAX_PROFILE_STATS_TRACKED];
                for(unsigned i = 0; i < m_NumLocalProfileStats; i++)
                {
                    if (numDirtyProfileStats < COUNTOF(dirtyStats) &&
                        m_LocalProfileStats[i].m_bDirty)
                    {
                        dirtyStats[numDirtyProfileStats].Reset(m_LocalProfileStats[i].m_StatId, m_LocalProfileStats[i].m_StatValue, m_LocalProfileStats[i].m_Priority);
                        numDirtyProfileStats++;
                    }
                }

                class FlushIterator : public rlProfileStatsDirtyIterator
                {
                public:
                    FlushIterator(rlProfileDirtyStat *dirtyStats, unsigned dirtyStatsCount)
                    : m_DirtyStats(dirtyStats)
                    , m_DirtyStatsCount(dirtyStatsCount)
                    , m_CurIndex(0)
                    { }

                    virtual bool GetCurrentStat(rlProfileDirtyStat& stat) const
                    {
                        if (!AtEnd())
                        {
                            stat = m_DirtyStats[m_CurIndex];
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }

                    virtual void Next()
                    {
                        if (!AtEnd())
                        {
                            m_CurIndex++;
                        }
                    }

                    virtual bool AtEnd() const
                    {
                        return m_CurIndex >= m_DirtyStatsCount;
                    }

                    static int ComparePriority(const void* t1, const void* t2)
                    {
                        const rlProfileDirtyStat* stat1 = (const rlProfileDirtyStat*)t1;
                        const rlProfileDirtyStat* stat2 = (const rlProfileDirtyStat*)t2;

                        if (stat1->GetPriority() < stat2->GetPriority()) return 1;
                        if (stat1->GetPriority() > stat2->GetPriority()) return -1;
                        return 0;
                    }

                private:
                    rlProfileDirtyStat *m_DirtyStats;
                    const unsigned m_DirtyStatsCount;
                    unsigned m_CurIndex;
                };

                qsort(dirtyStats, numDirtyProfileStats, sizeof(dirtyStats[0]), &FlushIterator::ComparePriority);

                FlushIterator flushIt(dirtyStats, Min<unsigned>(numDirtyProfileStats, MAX_DIRTY_PROFILE_STATS));
                if(rlProfileStats::Flush(flushIt, m_Gamer->GetLocalIndex(), &m_MyStatus))
                {
                    this->PushSegue(SEGUE_PROFILE_STATS_FLUSH);
                }
                else
                {
                    PushResult("Failed (probably too soon for next write)");
                }
            }
			else if(UiProfileStatsMainMenu::ACTION_SHOW_RESET_BY_GROUP_MENU == (size_t)arg)
			{
				m_UiLeaderboardMenu.SetTitle("RESET STATS BY GROUP");
				this->PushState(STATE_PROFILE_STATS_RESET_BY_GROUP_MENU);
			}
        }
        else
        {
            this->ShowSigninUi();
        }
    }
	else if(&m_uiProfileStatsResetByGroupMenu == el)
	{
		if(!m_Gamer || !m_Gamer->IsOnline())
		{
			this->ShowSigninUi();
		}
		else if(UiProfileStatsResetByGroupMenu::ACTION_RESET == (size_t)arg)
		{
			if(this->ResetProfileStatsByGroup())
			{
			    this->PushSegue(SEGUE_PROFILE_STATS_RESET_BY_GROUP);
			}
			else
			{
				PushResult("Failed");
			}
		}
	}
    else if(&m_UiCommunityStatsMainMenu == el)
    {
        if(m_Gamer)
        {
#if 0
            if(UiCommunityStatsMainMenu::ACTION_READ == (size_t)arg)
            {
                static int s_StatIds[] = { (unsigned)3 };
                static unsigned s_Count = 1;
                static float s_Result[] = { 0 };

                if(rlCommunityStats::ReadStats(m_Gamer->GetLocalIndex(),
                                               s_StatIds, s_Count, s_Result,
                                               &m_MyStatus))
                {
                    this->PushSegue(SEGUE_COMMUNITY_STATS_READ);
                }
                else
                {
                    PushResult("Failed");
                }
            }
            else if(UiCommunityStatsMainMenu::ACTION_READ_HISTORY == (size_t)arg)
            {
                static unsigned s_StatId = 3;
                static unsigned s_Count = 3;
                static float s_Result[] = { 0, 0, 0 };

                if(rlCommunityStats::ReadStatHistory(m_Gamer->GetLocalIndex(),
                                               s_StatId, s_Count, s_Result,
                                               &m_MyStatus))
                {
                    this->PushSegue(SEGUE_COMMUNITY_STATS_READ);
                }
                else
                {
                    PushResult("Failed");
                }
            }
            else if(UiCommunityStatsMainMenu::ACTION_READ_HISTORY_SET == (size_t)arg)
            {
                if (!m_HistoryResult.Reset(2, &m_HistoryRows[0]))
                {
                    PushResult("Failed");
                }
                else
                {
                    int statIds[] = { 2u, 3u };

                    if(rlCommunityStats::ReadStatHistorySet(m_Gamer->GetLocalIndex(),
                        statIds, 2, m_HistoryResult.GetMaxDepth(), &m_HistoryResult,
                        &m_MyStatus))
                    {
                        this->PushSegue(SEGUE_COMMUNITY_STATS_READ);
                    }
                    else
                    {
                        PushResult("Failed");
                    }
                }
            }
#endif
        }
        else
        {
            this->ShowSigninUi();
        }
    }
    else if(&m_UiClansMainMenu == el)
    {
        if(UiClansMainMenu::ACTION_CREATE == (size_t)arg)
        {
            if(m_Gamer)
            {
                char clanName[RL_CLAN_NAME_MAX_CHARS];
                char clanTag[RL_CLAN_TAG_MAX_CHARS];

                unsigned curTime = sysTimer::GetSystemMsTime();
                formatf((char*)clanName, RL_CLAN_NAME_MAX_CHARS, "0x%08x Clan",
                    curTime);
                formatf((char*)clanTag, RL_CLAN_TAG_MAX_CHARS, "%s", clanName);

                if(AssertVerify(rlClan::Create(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    clanName, clanTag, false, &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_UPDATING);
                }
            }
        }
        else if(UiClansMainMenu::ACTION_LEAVE == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClansMainMenu.GetSelectedIndex();

                if (AssertVerify(rlClan::Leave(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    m_MyClanMembership[idx].m_Clan.m_Id, &m_MyStatus))) 
                {
                    this->PushSegue(SEGUE_CLAN_UPDATING);
                }
            }
        }
        else if(UiClansMainMenu::ACTION_SET_PRIMARY == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClansMainMenu.GetSelectedIndex();

                if(AssertVerify(rlClan::SetPrimaryClan(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    m_MyClanMembership[idx].m_Clan.m_Id, &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_UPDATING);
                }
            }
        }
        else if(UiClansMainMenu::ACTION_GET_PRIMARY == (size_t)arg)
        {
            if (m_Gamer)
            {
                const rlGamerHandle& gamerHandle = m_Gamer->m_GamerInfo.GetGamerHandle();

                if(AssertVerify(rlClan::GetPrimaryClans(
                    m_Gamer->m_GamerInfo.GetLocalIndex(),
                    &gamerHandle, 1, 
                    &m_PrimaryClan, &m_PrimaryClanCount, NULL,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_RETRIEVING_PRIMARY_CLAN);
                }
            }
        }
        else if(UiClansMainMenu::ACTION_SHOW_SENT_REQUESTS == (size_t) arg)
        {
            if (m_Gamer)
            {
                RetrieveThenPushClanState(STATE_CLANS_SENT_REQUESTS_MENU);
            }
        }
        else if(UiClansMainMenu::ACTION_SHOW_ALL == (size_t) arg)
        {
            if (m_Gamer)
            {
                RetrieveThenPushClanState(STATE_CLANS_ALL_MENU);
            }
        }
        else if(UiClansMainMenu::ACTION_SHOW_MEMBERS == (size_t)arg)
        {
            if (m_Gamer)
            {
                const int idx = m_UiClansMainMenu.GetSelectedIndex();

                m_ActiveClan = &(m_MyClanMembership[idx].m_Clan);

                RetrieveThenPushClanState(STATE_CLANS_MEMBERS_MENU);
            }
        }
        else if(UiClansMainMenu::ACTION_SHOW_REQUESTS == (size_t) arg)
        {
            if (m_Gamer)
            {
                const int idx = m_UiClansMainMenu.GetSelectedIndex();
                m_ActiveClan = &(m_MyClanMembership[idx].m_Clan);

                RetrieveThenPushClanState(STATE_CLANS_REQUESTS_MENU);
            }
        }
        else if(UiClansMainMenu::ACTION_SHOW_RANKS == (size_t) arg)
        {
            if (m_Gamer)
            {
                const int idx = m_UiClansMainMenu.GetSelectedIndex();
                m_ActiveClan = &(m_MyClanMembership[idx].m_Clan);

                RetrieveThenPushClanState(STATE_CLANS_RANKS_MENU);
            }
        }
        else if(UiClansMainMenu::ACTION_SHOW_MESSAGES == (size_t)arg)
        {
            if (m_Gamer)
            {
                const int idx = m_UiClansMainMenu.GetSelectedIndex();
                m_ActiveClan = &(m_MyClanMembership[idx].m_Clan);

                RetrieveThenPushClanState(STATE_CLANS_WALL_MESSAGE_MENU);
            }
        }
        else if(UiClansMainMenu::ACTION_SHOW_METADATA == (size_t)arg)
        {
            if (m_Gamer)
            {
                const int idx = m_UiClansMainMenu.GetSelectedIndex();
                m_ActiveClan = &(m_MyClanMembership[idx].m_Clan);

                RetrieveThenPushClanState(STATE_CLANS_GET_METADATA);
            }
        }
        else if(UiClansMainMenu::ACTION_SHOW_FEUD_STAT == (size_t)arg)
        {
            if (m_Gamer)
            {
                const int idx = m_UiClansMainMenu.GetSelectedIndex();
                m_ActiveClan = &(m_MyClanMembership[idx].m_Clan);

                RetrieveThenPushClanState(STATE_CLANS_GET_FEUD_STAT);
            }
        }
        else if(UiClansMainMenu::ACTION_SHOW_INVITES == (size_t)arg)
        {
            if(m_Gamer)
            {
                RetrieveThenPushClanState(STATE_CLANS_INVITES_MENU);
            }
        }
    }
    else if(&m_UiClanInviteFriendMenu == el)
    {
        if(UiClanInviteFriendMenu::ACTION_INVITE == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanInviteFriendMenu.GetSelectedIndex();
                m_ActiveClan = &(m_MyClanMembership[idx].m_Clan);
                const rlFriend* f = m_UiFriendsList.GetSelectedFriend();
                if(AssertVerify(f))
                {
                    rlGamerHandle invitee;
                    f->GetGamerHandle(&invitee);

                    if(AssertVerify(rlClan::Invite(m_Gamer->m_GamerInfo.GetLocalIndex(), 
                                                   m_ActiveClan->m_Id,
                                                   -1,
                                                   false,
                                                   invitee,
                                                   &m_MyStatus)))
                    {
                        this->PushSegue(SEGUE_CLAN_INVITE_TO_CLAN);
                    }
                }
            }
        }
    }
    else if(&m_UiClansAllMenu == el)
    {
        if(UiClansAllMenu::ACTION_SHOW_MEMBERS == (size_t)arg)
        {
            if (m_Gamer)
            {
                const int idx = m_UiClansAllMenu.GetSelectedIndex();
                m_ActiveClan = &(m_AllClans[idx]);

                RetrieveThenPushClanState(STATE_CLANS_MEMBERS_MENU);
            }
        }
        else if(UiClansAllMenu::ACTION_SHOW_RANKS == (size_t)arg)
        {
            if (m_Gamer)
            {
                const int idx = m_UiClansAllMenu.GetSelectedIndex();
                m_ActiveClan = &(m_AllClans[idx]);

                RetrieveThenPushClanState(STATE_CLANS_RANKS_MENU);
            }
        }
        else if(UiClansAllMenu::ACTION_SHOW_MESSAGES == (size_t)arg)
        {
            if (m_Gamer)
            {
                const int idx = m_UiClansAllMenu.GetSelectedIndex();
                m_ActiveClan = &(m_AllClans[idx]);

                RetrieveThenPushClanState(STATE_CLANS_WALL_MESSAGE_MENU);
            }
        }
        else if(UiClansAllMenu::ACTION_SHOW_METADATA == (size_t)arg)
        {
            if (m_Gamer)
            {
                const int idx = m_UiClansAllMenu.GetSelectedIndex();
                m_ActiveClan = &(m_AllClans[idx]);

                RetrieveThenPushClanState(STATE_CLANS_GET_METADATA);
            }
        }
        else if(UiClansAllMenu::ACTION_JOIN_REQUEST == (size_t) arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClansAllMenu.GetSelectedIndex();
                m_ActiveClan = &(m_AllClans[idx]);

                if(AssertVerify(rlClan::JoinRequest(m_Gamer->m_GamerInfo.GetLocalIndex(), 
                    m_ActiveClan->m_Id,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_JOIN_REQUEST);
                }
            }
        }
        else if(UiClansAllMenu::ACTION_SHOW_FEUD == (size_t) arg)
        {
            if (m_Gamer)
            {
                const int idx = m_UiClansAllMenu.GetSelectedIndex();
                m_ActiveClan = &(m_AllClans[idx]);

                RetrieveThenPushClanState(STATE_CLANS_GET_FEUD_STAT);
            }
        }
    }
    else if(&m_UiClanRequestsMenu == el)
    {
        if(UiClanRequestsMenu::ACTION_DELETE == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanRequestsMenu.GetSelectedIndex();
                const rlClanRequestId requestId = m_ClanRequests[idx].m_RequestId;

                if(AssertVerify(rlClan::DeleteJoinRequest(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    requestId,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_JOIN_REQUEST_DELETE);
                }
            }
        }        
    }
    else if(&m_UiClanSentRequestsMenu == el)
    {
        if(UiClanSentRequestsMenu::ACTION_DELETE == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanSentRequestsMenu.GetSelectedIndex();
                const rlClanRequestId requestId = m_ClanSentRequests[idx].m_RequestId;

                if(AssertVerify(rlClan::DeleteJoinRequest(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    requestId,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_JOIN_REQUEST_DELETE);
                }
            }
        }        
    }
    else if(&m_UiClanMembersMenu == el)
    {
        if(UiClanMembersMenu::ACTION_KICK == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanMembersMenu.GetSelectedIndex();

                if (AssertVerify(rlClan::Kick(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    m_ClanMembers[idx].m_MemberClanInfo.m_Clan.m_Id,
                    m_ClanMembers[idx].m_MemberInfo.m_GamerHandle,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_MEMBER_UPDATING);
                }
            }
        }
        else if(UiClanMembersMenu::ACTION_PROMOTE == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanMembersMenu.GetSelectedIndex();

                if (AssertVerify(rlClan::MemberUpdateRankId(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    m_ClanMembers[idx].m_MemberInfo.m_GamerHandle,
                    m_ClanMembers[idx].m_MemberClanInfo.m_Clan.m_Id,
                    true,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_MEMBER_UPDATING);
                }
            }
        }        
        else if(UiClanMembersMenu::ACTION_DEMOTE == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanMembersMenu.GetSelectedIndex();

                if (AssertVerify(rlClan::MemberUpdateRankId(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    m_ClanMembers[idx].m_MemberInfo.m_GamerHandle,
                    m_ClanMembers[idx].m_MemberClanInfo.m_Clan.m_Id,
                    false,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_MEMBER_UPDATING);
                }
            }
        }
    }
    else if(&m_UiClanInvitesMenu == el)
    {
        if(ACTION_BACK == (size_t)arg)
        {
            // Attempt to refresh the clan menu.
            RetrieveThenPushClanState(this->GetState());
        }
        else if(UiClanInvitesMenu::ACTION_ACCEPT_INVITE == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanInvitesMenu.GetSelectedIndex();
                const rlClanId clanId = m_ClanInvites[idx].m_Clan.m_Id;

                if(AssertVerify(rlClan::Join(m_Gamer->m_GamerInfo.GetLocalIndex(), clanId,
                                             &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_INVITE_UPDATING);
                }
            }
        }        
        else if(UiClanInvitesMenu::ACTION_DELETE_INVITE == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanInvitesMenu.GetSelectedIndex();
                const rlClanInviteId inviteId = m_ClanInvites[idx].m_Id;

                if(AssertVerify(rlClan::DeleteInvite(m_Gamer->m_GamerInfo.GetLocalIndex(), 
                    inviteId,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_INVITE_UPDATING);
                }
            }
        }
    }
    else if(&m_UiClanRanksMenu == el)
    {
        if(UiClanRanksMenu::ACTION_CREATE == (size_t)arg)
        {
            if(m_Gamer)
            {
                const rlClanId clanId = m_ActiveClan->m_Id;

                char rankName[RL_CLAN_RANK_NAME_MAX_CHARS];

                unsigned curTime = sysTimer::GetSystemMsTime();
                formatf((char*)rankName, RL_CLAN_RANK_NAME_MAX_CHARS, "Rank 0x%08x",
                    curTime);

                if(AssertVerify(rlClan::RankCreate(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    clanId, rankName, 0,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_RANK_UPDATING);
                }
            }
        }        
        else if(UiClanRanksMenu::ACTION_DELETE == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanRanksMenu.GetSelectedIndex();
                const rlClanId clanId = m_ActiveClan->m_Id;
                const rlClanRankId rankId = m_ClanRanks[idx].m_Id;

                if(AssertVerify(rlClan::RankDelete(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    clanId, rankId,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_RANK_UPDATING);
                }
            }
        }        
        else if(UiClanRanksMenu::ACTION_MOVE_UP == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanRanksMenu.GetSelectedIndex();
                const rlClanId clanId = m_ActiveClan->m_Id;
                const rlClanRankId rankId = m_ClanRanks[idx].m_Id;

                if(AssertVerify(rlClan::RankUpdate(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    clanId, rankId, -1, NULL, -1,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_RANK_UPDATING);
                }
            }
        }        
        else if(UiClanRanksMenu::ACTION_MOVE_DOWN == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanRanksMenu.GetSelectedIndex();
                const rlClanId clanId = m_ActiveClan->m_Id;
                const rlClanRankId rankId = m_ClanRanks[idx].m_Id;

                if(AssertVerify(rlClan::RankUpdate(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    clanId, rankId, +1, NULL, -1,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_RANK_UPDATING);
                }
            }
        }        
        else if(UiClanRanksMenu::ACTION_TOGGLE_DESCRUCTIVE == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanRanksMenu.GetSelectedIndex();
                const rlClanId clanId = m_ActiveClan->m_Id;
                const rlClanRankId rankId = m_ClanRanks[idx].m_Id;
                s64 sysFlags = m_ClanRanks[idx].m_SystemFlags;

                if (sysFlags & RL_CLAN_PERMISSION_DISBAND)
                {
                    sysFlags &= ~RL_CLAN_PERMISSION_DISBAND;
                    sysFlags &= ~RL_CLAN_PERMISSION_KICK;
                }
                else
                {
                    sysFlags |= RL_CLAN_PERMISSION_DISBAND;
                    sysFlags |= RL_CLAN_PERMISSION_KICK;
                }

                if(AssertVerify(rlClan::RankUpdate(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    clanId, rankId, 0, NULL, sysFlags,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_RANK_UPDATING);
                }
            }
        }        
        else if(UiClanRanksMenu::ACTION_TOGGLE_MANAGER == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanRanksMenu.GetSelectedIndex();
                const rlClanId clanId = m_ActiveClan->m_Id;
                const rlClanRankId rankId = m_ClanRanks[idx].m_Id;
                s64 sysFlags = m_ClanRanks[idx].m_SystemFlags;

                if (sysFlags & RL_CLAN_PERMISSION_RANKMANAGER)
                {
                    sysFlags &= ~RL_CLAN_PERMISSION_RANKMANAGER;
                    sysFlags &= ~RL_CLAN_PERMISSION_PROMOTE;
                    sysFlags &= ~RL_CLAN_PERMISSION_DEMOTE;
                    sysFlags &= ~RL_CLAN_PERMISSION_DELETEFROMWALL;
                    sysFlags &= ~RL_CLAN_PERMISSION_WRITEONWALL;
                }
                else
                {
                    sysFlags |= RL_CLAN_PERMISSION_RANKMANAGER;
                    sysFlags |= RL_CLAN_PERMISSION_PROMOTE;
                    sysFlags |= RL_CLAN_PERMISSION_DEMOTE;
                    sysFlags |= RL_CLAN_PERMISSION_DELETEFROMWALL;
                    sysFlags |= RL_CLAN_PERMISSION_WRITEONWALL;
                }

                if(AssertVerify(rlClan::RankUpdate(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    clanId, rankId, 0, NULL, sysFlags,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_RANK_UPDATING);
                }
            }
        }        
        else if(UiClanRanksMenu::ACTION_TOGGLE_RECRUITER == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanRanksMenu.GetSelectedIndex();
                const rlClanId clanId = m_ActiveClan->m_Id;
                const rlClanRankId rankId = m_ClanRanks[idx].m_Id;
                s64 sysFlags = m_ClanRanks[idx].m_SystemFlags;

                if (sysFlags & RL_CLAN_PERMISSION_INVITE)
                {
                    sysFlags &= ~RL_CLAN_PERMISSION_INVITE;
                    sysFlags &= ~RL_CLAN_PERMISSION_CREWEDIT;
                    sysFlags &= ~RL_CLAN_PERMISSION_WRITEONWALL;
                }
                else
                {
                    sysFlags |= RL_CLAN_PERMISSION_INVITE;
                    sysFlags |= RL_CLAN_PERMISSION_CREWEDIT;
                    sysFlags |= RL_CLAN_PERMISSION_WRITEONWALL;
                }

                if(AssertVerify(rlClan::RankUpdate(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    clanId, rankId, 0, NULL, sysFlags,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_RANK_UPDATING);
                }
            }
        }        
    }
    else if(&m_UiClanWallMessagesMenu == el)
    {
        if(UiClanWallMessagesMenu::ACTION_WRITE_MESSAGE == (size_t)arg)
        {
            if(m_Gamer)
            {
                const rlClanId clanId = m_ActiveClan->m_Id;

                char message[RL_CLAN_WALL_MSG_MAX_CHARS];

                unsigned curTime = sysTimer::GetSystemMsTime();
                formatf((char*)message, RL_CLAN_WALL_MSG_MAX_CHARS, "Random Message 0x%08x",
                    curTime);

                if(AssertVerify(rlClan::WriteWallMessage(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    clanId, message,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_MESSAGE_UPDATING);
                }
            }
        }        
        else if(UiClanWallMessagesMenu::ACTION_DELETE_MESSAGE == (size_t)arg)
        {
            if(m_Gamer)
            {
                const int idx = m_UiClanWallMessagesMenu.GetSelectedIndex();
                const rlClanId clanId = m_ActiveClan->m_Id;
                const rlClanWallMessageId msgId = m_ClanMessags[idx].m_Id;

                if(AssertVerify(rlClan::DeleteWallMessage(m_Gamer->m_GamerInfo.GetLocalIndex(),
                    clanId, msgId,
                    &m_MyStatus)))
                {
                    this->PushSegue(SEGUE_CLAN_MESSAGE_UPDATING);
                }
            }
        }        
    }
	else if (&m_UiPartyMainMenu == el)
	{
		if (UiPartyMainMenu::ACTION_INVITETOPARTY == (size_t)arg)
		{
#if RSG_ORBIS
			int localGamerIndex = m_Gamer->m_GamerInfo.GetLocalIndex();
			g_rlNp.GetNpParty().ShowInvitationList(localGamerIndex);
#elif RSG_DURANGO
			int localGamerIndex = m_Gamer->m_GamerInfo.GetLocalIndex();
			g_rlXbl.GetSystemUi()->ShowSendInvitesUi(localGamerIndex);
#endif
		}
		else if (UiPartyMainMenu::ACTION_CREATEPARTY == (size_t)arg)
		{
#if RSG_ORBIS
			int localGamerIndex = m_Gamer->m_GamerInfo.GetLocalIndex();
			g_rlNp.GetNpParty().CreateParty(localGamerIndex);
#elif RSG_DURANGO
			int localGamerIndex = m_Gamer->m_GamerInfo.GetLocalIndex();
			if (!m_SystemPartyStatus.Pending())
			{
				g_rlXbl.GetPartyManager()->AddLocalUsersToParty(localGamerIndex, &m_SystemPartyStatus);
			}
#endif
		}
#if RSG_DURANGO
		else if (UiPartyMainMenu::ACTION_SETACTIVETITLE == (size_t)arg)
		{
			int localGamerIndex = m_Gamer->m_GamerInfo.GetLocalIndex();
			if (!m_SystemPartyStatus.Pending())
			{
				g_rlXbl.GetPartyManager()->SwitchPartyTitle(localGamerIndex, &m_SystemPartyStatus);
			}
		}
#endif
	}
	else if (&m_UiEntitlement == el)
	{
#if RSG_PC && 0
		if (UiEntitlementMenu::ACTION_SAVE_ENTITLEMENT == (size_t)arg)
		{
			static u8 entitlementBuf[] = { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10};
			rlRosEntitlement::SaveOfflineEntitlement(entitlementBuf, sizeof(entitlementBuf));
		}
		else if (UiEntitlementMenu::ACTION_LOAD_ENTITLEMENT == (size_t)arg)
		{
			rlRosEntitlement::LoadOfflineEntitlement();
		}
		else if (UiEntitlementMenu::ACTION_CLEAR_ENTITLEMENT == (size_t)arg)
		{
			rlRosEntitlement::ClearOfflineEntitlement();
		}

		else if (UiEntitlementMenu::ACTION_SAVE_ENTITLEMENT == (size_t)arg)
		{
			static u8 entitlementBuf[] = { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10};
			g_rlPc.GetCommerceManager()->SaveOfflineEntitlement(entitlementBuf, sizeof(entitlementBuf));
		}
		else if (UiEntitlementMenu::ACTION_LOAD_ENTITLEMENT == (size_t)arg)
		{
			g_rlPc.GetCommerceManager()->LoadOfflineEntitlement();
		}
		else if (UiEntitlementMenu::ACTION_CLEAR_ENTITLEMENT == (size_t)arg)
		{
			g_rlPc.GetCommerceManager()->ClearOfflineEntitlement();
		}
#endif
	}
	else if (&m_LiveStreamMenu == el)
	{
#if LIVESTREAM_ENABLED
		if (UiLiveStreamMenu::ACTION_INIT == (size_t)arg)
		{
			rlLiveStream::InitClass();
		}
		else if (UiLiveStreamMenu::ACTION_AUTHENTICATE == (size_t)arg)
		{
			const char* userName;
			const char* passWord;
			if (PARAM_twitchUsername.Get(userName) && PARAM_twitchPassword.Get(passWord))
			{
				rlLiveStream::Authenticate(userName, passWord);
			}
		}
		else if (UiLiveStreamMenu::ACTION_START_STREAMING == (size_t)arg)
		{
			int index = m_LiveStreamMenu.GetIngestServerIndex();
			int width = m_LiveStreamMenu.GetTargetWidth();
			int height = m_LiveStreamMenu.GetTargetHeight();
			int targetFPS = m_LiveStreamMenu.GetTargetFPS();

			rlLiveStream::StartStreaming(index, width, height, targetFPS);
		}
		else if (UiLiveStreamMenu::ACTION_STOP_STREAMING == (size_t)arg)
		{
			rlLiveStream::StopStreaming();
		}
		else if (UiLiveStreamMenu::ACTION_PAUSE_STREAMING == (size_t)arg)
		{
			rlLiveStream::TogglePaused();
		}
		else if (UiLiveStreamMenu::ACTION_SHUTDOWN == (size_t)arg)
		{
			rlLiveStream::ShutdownClass();
		}
#endif
	}
	else if (&m_YoutubeMenu == el)
	{
		if (m_Gamer && !m_YoutubeMenu.GetStatus()->Pending())
		{
			if (UiYoutubeMenu::ACTION_LINK_ACCOUNT == (size_t)arg)
			{
				rlYoutube::ShowAccountLinkUi(m_Gamer ? m_Gamer->GetLocalIndex() : 0, "gtav", "replay", m_YoutubeMenu.GetStatus());
			}
			else if (UiYoutubeMenu::ACTION_GET_OAUTH == (size_t)arg)
			{
				m_YoutubeMenu.SetTitle("Youtube - Getting Auth Token");
				rlYoutube::GetOAuthToken(m_Gamer->GetLocalIndex(), m_YoutubeMenu.GetUpload(), m_YoutubeMenu.GetStatus());
			}
			else if (UiYoutubeMenu::ACTION_GET_URL == (size_t)arg)
			{
				m_YoutubeMenu.SetTitle("Youtube - Getting Upload Url");
				rlYoutube::GetResumableUploadUrl(m_YoutubeMenu.GetUpload(), m_YoutubeMenu.GetStatus());
			}
			else if (UiYoutubeMenu::ACTION_UPLOAD == (size_t)arg)
			{
				if (m_YoutubeMenu.GetUploadData() != NULL)
				{
					m_YoutubeMenu.SetTitle("Youtube - Uploading Video");
					rlYoutube::Upload(m_YoutubeMenu.GetUpload(), m_YoutubeMenu.GetUploadData() , m_YoutubeMenu.GetStatus());
				}
			}
			else if (UiYoutubeMenu::ACTION_GET_CHANNEL_INFO == (size_t)arg)
			{
				rlYoutube::GetChannelInfo(m_YoutubeMenu.GetUpload(), m_YoutubeMenu.GetStatus());
			}
			else if (UiYoutubeMenu::ACTION_GET_VIDEO_INFO == (size_t)arg)
			{
				rlYoutube::GetVideoInfo(m_YoutubeMenu.GetUpload(), m_YoutubeMenu.GetStatus());
			}
			else if (UiYoutubeMenu::ACTION_CLEAR_URL == (size_t)arg)
			{
				m_YoutubeMenu.GetUpload()->UploadUrl = "";
			}
			else if(UiYoutubeMenu::ACTION_LOAD_VIDEO == (size_t)arg)
			{
				if (m_YoutubeMenu.GetUploadData() == NULL)
				{
					char moviePath[RAGE_MAX_PATH] = {0};
					formatf(moviePath, "%ssample_snet/daybreak.mp4", ASSET.GetPath());
					fiStream* pStream = ASSET.Open(moviePath, "", false, true);
					if (pStream)
					{
						m_YoutubeMenu.GetUpload()->UploadSize = pStream->Size();
						if (m_YoutubeMenu.AllocateData())
						{
							m_YoutubeMenu.SetTitle("Youtube - Loading Video");
							pStream->Read(m_YoutubeMenu.GetUploadData(), (int)m_YoutubeMenu.GetUpload()->UploadSize);
						}
					}
				}
			}
		}
	}
}

void 
App::OnFriendsListEvent(UiElement* el, const int evtId, void* arg)
{
    if (&m_UiFriendsList != el)
    {
        Assert(false);
    }

    if(UIEVENT_ACTION == evtId)
    {
        if(ACTION_BACK == (size_t)arg)
        {
            this->PopState();
        }
        else if(UiFriendsList::ACTION_INVITE_TO_SESSION == (size_t)arg)
        {
            const rlFriend* f = m_UiFriendsList.GetSelectedFriend();
            if(AssertVerify(f))
            {
                rlGamerHandle hGamer;
                f->GetGamerHandle(&hGamer);
                if(m_LiveMgr->GetSession()->Exists()
                    && m_LiveMgr->GetSession()->IsInvitable())
                {
                    m_LiveMgr->GetSession()->SendInvites(&hGamer, 1, "", "Join me!", NULL);
                }
            }
        }
        else if(UiFriendsList::ACTION_INVITE_TO_CLAN == (size_t)arg)
        {
            if(m_Gamer && m_Gamer->IsOnline() && 
                rlClan::IsServiceReady(m_Gamer->m_GamerInfo.GetLocalIndex()))
            {
                const int idx = m_UiFriendsList.GetFriendIndex();
                m_ActiveFriend = idx;

                RetrieveThenPushClanState(STATE_CLANS_INVITE_FRIEND_MENU);
            }
        }
        else if(UiFriendsList::ACTION_SHOW_PROFILE == (size_t)arg)
        {
            const rlFriend* f = m_UiFriendsList.GetSelectedFriend();
            if(AssertVerify(f))
            {
                rlGamerHandle hGamer;
                f->GetGamerHandle(&hGamer);

                g_SystemUi.ShowGamerProfileUi(m_Gamer->GetLocalIndex(), hGamer);
            }
        }
		else if (UiFriendsList::ACTION_CHECK_IS_FRIEND == (size_t)arg)
		{
			const rlFriend* f = m_UiFriendsList.GetSelectedFriend();
			if(AssertVerify(f))
			{
				rlGamerHandle hGamer;
				f->GetGamerHandle(&hGamer);

#if !__NO_OUTPUT
				bool isFriend = rlFriendsManager::IsFriendsWith(m_Gamer->GetLocalIndex(), hGamer);
				rlDebug3("Check is Friend with %s: %s", f->GetName(), isFriend ? "TRUE" : "FALSE");
#endif
			}
		}
        else if (UiFriendsList::ACTION_JOIN_SESSION == (size_t)arg)
        {
            rlSessionInfo sessionInfo;

            if(m_UiFriendsList.GetJoinSessionInfo(&sessionInfo))
            {
                this->JoinSession(m_Gamer,
                                sessionInfo,
                                RL_NETMODE_ONLINE,
                                RL_SLOT_PUBLIC);
            }
			else if (!m_MyStatus.Pending())
			{
				m_SessionQuery.Clear();
				m_NumSessionFound = 0;

				rlGamerInfo gamerInfo;
				if (AssertVerify(rlPresence::GetGamerInfo(m_Gamer->GetLocalIndex(), &gamerInfo)))
				{
					static rlGamerHandle jipGamerHandle;
					m_UiFriendsList.GetSelectedFriend()->GetGamerHandle(&jipGamerHandle);

					if (rlPresenceQuery::GetSessionByGamerHandle(m_Gamer->GetLocalIndex(), &jipGamerHandle, 1, &m_SessionQuery, 1, &m_NumSessionFound, &m_MyStatus))
					{
						this->PushSegue(SEGUE_SESSION_JVP_LOOKUP);
					}
				}
			}
        }
		else if (UiFriendsList::ACTION_SEND_MESSAGE == (size_t)arg)
		{
			const rlFriend* f = m_UiFriendsList.GetSelectedFriend();
			if(AssertVerify(f))
			{
				rlGamerHandle hGamer;
				f->GetGamerHandle(&hGamer);

				g_SystemUi.ShowMessageComposeUi(m_Gamer->GetLocalIndex(), &hGamer, 1, "Test Subject", "Test Salutation");
			}
		}
		else if (UiFriendsList::ACTION_REMOVE_FRIEND == (size_t)arg)
		{
			const rlFriend* f = m_UiFriendsList.GetSelectedFriend();
			if(AssertVerify(f))
			{
				rlGamerHandle hGamer;
				f->GetGamerHandle(&hGamer);

				g_SystemUi.ShowAddFriendUI(m_Gamer->GetLocalIndex(), hGamer, "Test Salutation");
			}
		}
#if RSG_DURANGO
		else if (UiFriendsList::ACTION_RTA_SUBSCRIBE == (size_t)arg)
		{
			if(AssertVerify(m_UiFriendsList.GetSelectedFriend()))
			{
				atArray<rlFriendsReference*> friendArray;
				rlFriendsReference ref;
				ref.m_Id.m_Xuid = m_UiFriendsList.GetSelectedFriend()->GetXuid();
				friendArray.PushAndGrow(&ref);
				g_rlXbl.GetRealtimeActivityManager()->SubscribeToFriendsPresence(m_Gamer->GetLocalIndex(), &friendArray);
			}
		}
		else if (UiFriendsList::ACTION_RTA_UNSUBSCRIBE == (size_t)arg)
		{
			if(AssertVerify(m_UiFriendsList.GetSelectedFriend()))
			{
				atArray<rlFriendsReference*> friendArray;
				rlFriendsReference ref;
				ref.m_Id.m_Xuid = m_UiFriendsList.GetSelectedFriend()->GetXuid();
				friendArray.PushAndGrow(&ref);
				g_rlXbl.GetRealtimeActivityManager()->UnsubscribeToFriendsPresence(m_Gamer->GetLocalIndex(), &friendArray);
			}
		}
		else if(UiFriendsList::ACTION_GET_SESSIONS == (size_t)arg)
		{
			static netStatus s_GetSessionStatus;
			if (!s_GetSessionStatus.Pending())
			{
				g_rlXbl.GetPlayerManager()->GetFriendsPresenceSessions(0, m_UiFriendsList.GetFriends(), m_UiFriendsList.GetNumFriends(), &s_GetSessionStatus);
			}
		}
#endif
		else if (UiFriendsList::ACTION_GET_DISPLAYNAMES == (size_t)arg)
		{
			static netStatus s_GetDisplayNameStatus;
			if (!s_GetDisplayNameStatus.Pending())
			{
				m_UiFriendsList.GetDisplayNames(m_Gamer->GetLocalIndex(), &s_GetDisplayNameStatus);
			}
		}
		else if (UiFriendsList::ACTION_REFRESH_LIST == (size_t)arg)
		{
#if RSG_ORBIS
			rlFriendsManager::Reactivate();
#else
			m_UiFriendsList.RequestNewFriendsPage(m_Gamer->GetLocalIndex());
#endif
		}
		else if (UiFriendsList::ACTION_SEARCH_FRIENDS == (size_t)arg)
		{
			g_SystemUi.ShowFriendSearchUi(m_Gamer->GetLocalIndex());
		}
    }
}

void 
App::OnInvitesReceivedEvent(UiElement* el, const int evtId, void* arg)
{
    if (&m_UiInvitesReceived != el)
    {
        Assert(false);
    }

    if(UIEVENT_ACTION == evtId)
    {
        if(ACTION_BACK == (size_t)arg)
        {
            this->PopState();
        }
        else if(UiInvitesReceived::ACTION_ACCEPT == (size_t)arg)
        {
            //Start the joining process with the selected session info
            const rlSessionInfo* sessionInfo = m_UiInvitesReceived.GetSelectedSession();

            if(sessionInfo)
            {
                this->JoinSession(m_Gamer,
                                *sessionInfo,
                                RL_NETMODE_ONLINE,
                                RL_SLOT_PRIVATE);
            }
        }
        else if(UiInvitesReceived::ACTION_DECLINE == (size_t)arg)
        {
            //Nothing to do currently
        }
        else if(UiInvitesReceived::ACTION_SHOW_PENDING_MP_INVITES == (size_t)arg)
        {
#if RSG_NP
			int localGamerIndex = m_Gamer->m_GamerInfo.GetLocalIndex();
           g_rlNp.GetBasic().ShowAcceptMultiplayerInviteUi(localGamerIndex);
#endif
        }
    }
}

void 
App::OnHostConfigEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg)
{
    Assert(&m_UiHostConfig == el);

    if(UIEVENT_ACTION == evtId)
    {
        if(UiHostConfig::ACTION_CREATE == (size_t)arg)
        {
            this->HostSession();
        }
        else if(ACTION_BACK == (size_t)arg)
        {
            this->PopState();
        }
    }
}

void 
App::OnQueryConfigEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg)
{
    Assert(&m_UiQueryConfig == el);

    if(UIEVENT_ACTION == evtId)
    {
        if(UiQueryConfig::ACTION_SEARCH == (size_t)arg)
        {
            this->FindSessions();
        }
        else if(ACTION_BACK == (size_t)arg)
        {
            this->PopState();
        }
    }
}

void 
App::OnSesionSelectEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg)
{
    Assert(&m_UiSessionSelect == el);

    if(UIEVENT_ACTION == evtId)
    {
        if(ACTION_BACK == (size_t)arg)
        {
            this->PopState();
        }
        else if(UiSessionSelect::ACTION_VIEW_DETAILS == (size_t)arg)
        {
            const int sessionIdx = m_UiSessionSelect.GetSessionIndex();
            if(sessionIdx >= 0 && sessionIdx < (int) m_NumSearchResults)
            {
                m_UiSessionDetails.SetSessionDetail(m_SessionDetails[sessionIdx]);
                this->PushState(STATE_VIEWING_SESSION_DETAILS);
            }
        }
        else if(UiSessionSelect::ACTION_JOIN == (size_t)arg)
        {
            const int sessionIdx = m_UiSessionSelect.GetSessionIndex();
            if(sessionIdx >= 0 && sessionIdx < (int) m_NumSearchResults)
            {
                const rlSessionDetail& detail = m_SessionDetails[sessionIdx];

                this->JoinSession(m_Gamer,
                                    detail.m_SessionInfo,
                                    detail.m_SessionConfig.m_NetworkMode,
                                    RL_SLOT_PUBLIC);
            }
        }
    }
}

void 
App::OnSessionDetailsEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg)
{
    Assert(&m_UiSessionDetails == el);

    if(UIEVENT_ACTION == evtId)
    {
        if(ACTION_BACK == (size_t)arg)
        {
            this->PopState();
        }
        else if(UiSessionDetails::ACTION_JOIN == (size_t)arg)
        {
            const int sessionIdx = m_UiSessionSelect.GetSessionIndex();
            if(sessionIdx >= 0 && sessionIdx < (int) m_NumSearchResults)
            {
                const rlSessionDetail& detail = m_SessionDetails[sessionIdx];

                this->JoinSession(m_Gamer,
                                    detail.m_SessionInfo,
                                    detail.m_SessionConfig.m_NetworkMode,
                                    RL_SLOT_PUBLIC);
            }
        }
    }
}

void 
App::OnLobbyEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg)
{
    Assert(&m_UiLobby == el);

    if(UIEVENT_ACTION == evtId)
    {
        if(UiLobby::ACTION_LEAVE == (size_t)arg)
        {
            this->PopState();
            this->LeaveSession(m_Gamer);
        }
		if(UiLobby::ACTION_MIGRATE_HOST == (size_t)arg)
		{
			this->MigrateHostToMyself();
		}
		else if(UiLobby::ACTION_START == (size_t)arg)
        {
            if(m_LiveMgr->GetSession()->IsHost())
            {
                this->StartMatch();
            }
        }
        else if(UiLobby::ACTION_FRIENDS_LIST == (size_t)arg)
        {
            this->PushState(STATE_VIEWING_FRIENDS_LIST);
        }
        else if(UiLobby::ACTION_CONFIGURE_ATTRIBUTES == (size_t)arg)
        {
            this->PushState(STATE_CONFIGURING_ATTRIBUTES);
        }
        else if(UiLobby::ACTION_TOGGLE_INVITABLE == (size_t)arg)
        {
            this->ToggleInvitable();
        }
#if RSG_ORBIS
		else if (UiLobby::ACTION_GET_SESSION_DATA == (size_t)arg)
		{
			static netStatus sessionDataStatus;
			static rlSessionInfo sessionDataInfo;
			if (!sessionDataStatus.Pending())
			{
				g_rlNp.GetWebAPI().GetSessionData(m_Gamer->GetLocalIndex(), m_LiveMgr->GetSession()->GetWebApiSessionId(), &sessionDataInfo, &sessionDataStatus);
			}
		}
		else if (UiLobby::ACTION_GET_CHANGEABLE_DATA == (size_t)arg)
		{
			static netStatus sessionDataStatus;
			static rlSessionInfo sessionDataInfo;
			if (!sessionDataStatus.Pending())
			{
				g_rlNp.GetWebAPI().GetChangeableSessionData(m_Gamer->GetLocalIndex(), m_LiveMgr->GetSession()->GetWebApiSessionId(), &sessionDataInfo, &sessionDataStatus);
			}
		}
#endif
		else if (UiLobby::ACTION_SHOW_PROFILE == (size_t)arg)
		{
			rlGamerInfo pInfo = m_UiLobby.GetSelection();
			g_SystemUi.ShowGamerProfileUi(m_Gamer->GetLocalIndex(), pInfo.GetGamerHandle());
		}
		else if (UiLobby::ACTION_ADD_FRIEND == (size_t)arg)
		{
			rlGamerInfo pInfo = m_UiLobby.GetSelection();
			g_SystemUi.ShowAddFriendUI(m_Gamer->GetLocalIndex(), pInfo.GetGamerHandle(), "Lets be friends!");
		}
		else if (UiLobby::ACTION_BAIL == (size_t)arg)
		{
			this->PopState();

			m_PeerMgr.Shutdown();
			m_PeerMgr.Init(m_LiveMgr->GetCxnMgr(), LiveManager::GAME_CHANNEL_ID);

			if(m_Gamer)
			{
				m_Gamer->ClearStats();
			}

			if(m_LiveMgr->GetSession()->Exists())
			{
				m_LiveMgr->GetSession()->Shutdown(true, -1);
				m_LiveMgr->InitSession();
			}
		}
    }
}

void 
App::OnGameEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg)
{
    Assert(&m_UiGame == el);

    if(UIEVENT_ACTION == evtId)
    {
        if(UiGame::ACTION_LEAVE == (size_t)arg)
        {
            this->PopState();
            this->PopState();
            this->LeaveSession(m_Gamer);
        }
        else if(UiGame::ACTION_END == (size_t)arg)
        {
            if(m_LiveMgr->GetSession()->IsHost())
            {
                this->EndMatch();
            }
        }
        else if(UiGame::ACTION_SWING == (size_t)arg)
        {
            this->Swing();
        }
    }
}

void 
App::OnConfigureAttrsEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg)
{
    Assert(&m_UiConfigureAttrs == el);

    if(UIEVENT_ACTION == evtId)
    {
        if(ACTION_BACK == (size_t)arg)
        {
            this->PopState();
        }
        else if(UiConfigureAttrs::ACTION_COMMIT == (size_t)arg)
        {
            this->ChangeAttributes();
        }
    }
}

void
App::DoRateContent(const float /*rating*/)
{
    /*const rlUgcQueryContentResultBase* result = g_UgcResults.GetResult(m_UiUgcContentList.GetSelectedContentIndex());

    if(rlUgc::SetPlayerData(m_Gamer->GetLocalIndex(), 
                            RLUGC_CONTENT_TYPE_GTA5MISSION, 
                            result->GetContentInfo().GetMetadata().GetContentId(), 
                            NULL,
                            &rating, 
                            &m_MyStatus))
    {
        this->PushSegue(SEGUE_GENERIC);
    }
    else
    {
        PushResult("Failed to start task");
    }*/
}

void 
App::OnUgcEvent(UiElement* el, const int evtId, void* arg)
{
    if(UIEVENT_ACTION == evtId)
    {
        if(ACTION_BACK == (ptrdiff_t)arg)
        {
            this->PopState();
        }
        else if (el == &m_UiUgcMainMenu)
        {
            if(UiUgcMainMenu::ACTION_CREATE_VIDEO_CONTENT == (ptrdiff_t)arg)
            {
                if(rlUgc::CreateContent(m_Gamer->GetLocalIndex(),
                                        RLUGC_CONTENT_TYPE_GTA5VIDEO,
                                        "Some video",
                                        "{\"test\":1}", //datajson
                                        "This is a video", //description
                                        (rlUgc::SourceFileInfo*)NULL, //files
                                        0, //numFiles
                                        RLSC_LANGUAGE_ENGLISH,
                                        NULL, //tagCsv
                                        false, //publish?
                                        &g_UgcMetadata,
                                        NULL,
                                        &m_MyStatus))
                {
                    this->PushSegue(SEGUE_UGC_CREATE_VIDEO_CONTENT);
                }
                else
                {
                    PushResult("Failed to start task");
                }
                
            }
        }
    }
/*
    if(UIEVENT_ACTION == evtId)
    {
        if(ACTION_BACK == (ptrdiff_t)arg)
        {
            this->PopState();
        }
        else if (el == &m_UiUgcMainMenu)
        {
            if(UiUgcMainMenu::ACTION_CREATE_CONTENT == (ptrdiff_t)arg)
            {
                if(rlUgc::CreateContent(m_Gamer->GetLocalIndex(),
                                        RLUGC_CONTENT_TYPE_GTA5MISSION,
                                        "Rob's & \"totally awesome\" test mission",
                                        "{test:1}", //datajson
                                        "Holy cow! & This mission is 'the' \"bomb\"", //description
                                        NULL, //files
                                        0, //numFiles
                                        RLSC_LANGUAGE_ENGLISH,
                                        NULL, //tagCsv
                                        true, //publish?
                                        &g_UgcMetadata,
                                        &m_MyStatus))
                {
                    this->PushSegue(SEGUE_UGC_CREATE_CONTENT);
                }
                else
                {
                    PushResult("Failed to start task");
                }
                
            }
            else if(UiUgcMainMenu::ACTION_GET_BOOKMARKED == (ptrdiff_t)arg)
            {
                Assert(RLUGC_CATEGORY_UNKNOWN == rlUgcCategoryFromString(rlUgcCategoryToString(RLUGC_CATEGORY_UNKNOWN)));
                Assert(RLUGC_CATEGORY_NONE == rlUgcCategoryFromString(rlUgcCategoryToString(RLUGC_CATEGORY_NONE)));
                Assert(RLUGC_CATEGORY_ROCKSTAR_CREATED == rlUgcCategoryFromString(rlUgcCategoryToString(RLUGC_CATEGORY_ROCKSTAR_CREATED)));
                Assert(RLUGC_CATEGORY_ROCKSTAR_CREATED_CANDIDATE == rlUgcCategoryFromString(rlUgcCategoryToString(RLUGC_CATEGORY_ROCKSTAR_CREATED_CANDIDATE)));
                Assert(RLUGC_CATEGORY_ROCKSTAR_VERIFIED == rlUgcCategoryFromString(rlUgcCategoryToString(RLUGC_CATEGORY_ROCKSTAR_VERIFIED)));
                Assert(RLUGC_CATEGORY_ROCKSTAR_VERIFIED_CANDIDATE == rlUgcCategoryFromString(rlUgcCategoryToString(RLUGC_CATEGORY_ROCKSTAR_VERIFIED_CANDIDATE)));

                Assert(RLUGC_CATEGORY_ROCKSTAR_CREATED_CANDIDATE == rlUgcCategoryFromString(" RSTARcan"));
                Assert(RLUGC_CATEGORY_ROCKSTAR_CREATED_CANDIDATE == rlUgcCategoryFromString(" RSTARcan  "));
                Assert(RLUGC_CATEGORY_ROCKSTAR_CREATED_CANDIDATE == rlUgcCategoryFromString("RSTARcan  "));

                Assert(RLUGC_CONTENT_TYPE_UNKNOWN == rlUgcContentTypeFromString(rlUgcContentTypeToString(RLUGC_CONTENT_TYPE_UNKNOWN)));
                Assert(RLUGC_CONTENT_TYPE_GTA5MISSION == rlUgcContentTypeFromString(rlUgcContentTypeToString(RLUGC_CONTENT_TYPE_GTA5MISSION)));
                Assert(RLUGC_CONTENT_TYPE_GTA5PLAYLIST == rlUgcContentTypeFromString(rlUgcContentTypeToString(RLUGC_CONTENT_TYPE_GTA5PLAYLIST)));
                Assert(RLUGC_CONTENT_TYPE_LIFEINVADERPOST == rlUgcContentTypeFromString(rlUgcContentTypeToString(RLUGC_CONTENT_TYPE_LIFEINVADERPOST)));
                Assert(RLUGC_CONTENT_TYPE_GTA5PHOTO == rlUgcContentTypeFromString(rlUgcContentTypeToString(RLUGC_CONTENT_TYPE_GTA5PHOTO)));
                Assert(RLUGC_CONTENT_TYPE_GTA5CHALLENGE == rlUgcContentTypeFromString(rlUgcContentTypeToString(RLUGC_CONTENT_TYPE_GTA5CHALLENGE)));

                Assert(RLUGC_CONTENT_TYPE_GTA5CHALLENGE == rlUgcContentTypeFromString("   challenge  "));
                Assert(RLUGC_CONTENT_TYPE_GTA5CHALLENGE == rlUgcContentTypeFromString("challenge  "));
                Assert(RLUGC_CONTENT_TYPE_GTA5CHALLENGE == rlUgcContentTypeFromString(" cHallENge"));

                g_UgcResults.Reset(NELEM(g_UgcResultRows), g_UgcResultRows, sizeof(rlUgcQueryContentResult<T_MAX_PLAYERS>));

                if(rlUgc::QueryContent(m_Gamer->GetLocalIndex(), 
                                       RLUGC_CONTENT_TYPE_GTA5MISSION, 
                                       "GetMyBookmarkedContent",
                                       NULL, //queryParams,
                                       0, 
                                       T_MAX_RESULTS,
                                       &g_UgcTotal,
                                       &g_UgcHash,
                                       m_UgcDlgt,
                                       &m_MyStatus))
                {
                    this->PushSegue(SEGUE_UGC_QUERY_CONTENT);
                }
                else
                {
                    PushResult("Failed to start task");
                }
            }
            else if(UiUgcMainMenu::ACTION_GET_FRIENDS == (ptrdiff_t)arg)
            {
                g_UgcResults.Reset(NELEM(g_UgcResultRows), g_UgcResultRows, sizeof(rlUgcQueryContentResult<T_MAX_PLAYERS>));

                if(rlUgc::QueryContent(m_Gamer->GetLocalIndex(), 
                                       RLUGC_CONTENT_TYPE_GTA5MISSION, 
                                       "GetFriendContent",
                                       NULL,
                                       0,
                                       T_MAX_RESULTS,
                                       &g_UgcTotal,
                                       &g_UgcHash,
                                       m_UgcDlgt,
                                       &m_MyStatus))
                {
                    this->PushSegue(SEGUE_UGC_QUERY_CONTENT);
                }
                else
                {
                    PushResult("Failed to start task");
                }
            }
            else if(UiUgcMainMenu::ACTION_GET_MINE == (ptrdiff_t)arg)
            {
                g_UgcResults.Reset(NELEM(g_UgcResultRows), g_UgcResultRows, sizeof(rlUgcQueryContentResult<T_MAX_PLAYERS>));

                if(rlUgc::QueryContent(m_Gamer->GetLocalIndex(), 
                                       RLUGC_CONTENT_TYPE_GTA5MISSION, 
                                       "GetMyContent",
                                       NULL,
                                       0, 
                                       T_MAX_RESULTS,
                                       &g_UgcTotal,
                                       &g_UgcHash,
                                       m_UgcDlgt,
                                       &m_MyStatus))
                {
                    this->PushSegue(SEGUE_UGC_QUERY_CONTENT);
                }
                else
                {
                    PushResult("Failed to start task");
                }
            }
            else if(UiUgcMainMenu::ACTION_GET_ROCKSTAR_CREATED == (size_t)arg)
            {
                g_UgcResults.Reset(NELEM(g_UgcResultRows), g_UgcResultRows, sizeof(rlUgcQueryContentResult<T_MAX_PLAYERS>));

                char queryParams[128];
                formatf(queryParams, sizeof(queryParams), 
                        "{category:\"%s\", hash:%d}", 
                        rlUgcCategoryToString(RLUGC_CATEGORY_ROCKSTAR_CREATED),
                        g_UgcHash);

                if(rlUgc::QueryContent(m_Gamer->GetLocalIndex(), 
                                       RLUGC_CONTENT_TYPE_GTA5MISSION,   
                                       "GetContentByCategory",
                                       queryParams,
                                       0, 
                                       T_MAX_RESULTS,
                                       &g_UgcTotal,
                                       &g_UgcHash,
                                       m_UgcDlgt,
                                       &m_MyStatus))
                {
                    this->PushSegue(SEGUE_UGC_QUERY_CONTENT);
                }
                else
                {
                    PushResult("Failed to start task");
                }
            }
            else if(UiUgcMainMenu::ACTION_GET_TOP_RATED == (size_t)arg)
            {
                g_UgcResults.Reset(NELEM(g_UgcResultRows), g_UgcResultRows, sizeof(rlUgcQueryContentResult<T_MAX_PLAYERS>));

                if(rlUgc::QueryContent(m_Gamer->GetLocalIndex(), 
                                       RLUGC_CONTENT_TYPE_GTA5MISSION, 
                                       "GetTopRatedContent",
                                       NULL,
                                       0,
                                       T_MAX_RESULTS,
                                       &g_UgcTotal,
                                       &g_UgcHash,
                                       m_UgcDlgt,
                                       &m_MyStatus))
                {
                    this->PushSegue(SEGUE_UGC_QUERY_CONTENT);
                }
                else
                {
                    PushResult("Failed to start task");
                }
            }
            else if(UiUgcMainMenu::ACTION_GET_TOP_RATED_CREATORS == (size_t)arg)
            {
                //if(rlUgc::QueryContentCreators(m_Gamer->GetLocalIndex(), 
                //                               RLUGC_CONTENT_TYPE_GTA5MISSION, 
                //                               "GetTopRated",
                //                                0,
                //                                NULL, //queryParams,
                //                                g_UgcContentCreators, 
                //                                NELEM(g_UgcContentCreators),
                //                                &g_UgcCount,
                //                                &g_UgcTotal,
                //                                &m_MyStatus))
                //{
                //    this->PushSegue(SEGUE_GENERIC);
                //}
                //else
                //{
                //    PushResult("Failed to start task");
                //}
            }
        }
        else if (el == &m_UiUgcContentList)
        {
            const rlUgcQueryContentResultBase* result = g_UgcResults.GetResult(m_UiUgcContentList.GetSelectedContentIndex());
            const rlUgcContentInfo& c = result->GetContentInfo();
            
            if(UiUgcContentList::ACTION_DETAILS == (ptrdiff_t)arg)
            {
                m_UiUgcContentDetails.SetContent(result);
                this->PushState(STATE_UGC_CONTENT_DETAILS);
            }
            else if(UiUgcContentList::ACTION_BOOKMARK == (ptrdiff_t)arg)
            {
                if(rlUgc::SetBookmarked(m_Gamer->GetLocalIndex(), 
                                        RLUGC_CONTENT_TYPE_GTA5MISSION, 
                                        result->GetContentInfo().GetMetadata().GetContentId(), 
                                        true,
                                        &m_MyStatus))
                {
                    this->PushSegue(SEGUE_GENERIC);
                }
                else
                {
                    PushResult("Failed to start task");
                }
            }
            else if(UiUgcContentList::ACTION_COPY_CONTENT == (ptrdiff_t)arg)
            {
                if(rlUgc::CopyContent(m_Gamer->GetLocalIndex(),
                                      RLUGC_CONTENT_TYPE_GTA5MISSION,
                                      c.GetMetadata().GetContentId(),
                                      &g_UgcMetadata,
                                      &m_MyStatus))
                {
                    this->PushSegue(SEGUE_UGC_COPY_CONTENT);
                }
                else
                {
                    PushResult("Failed to start task");
                }
            }
            else if(UiUgcContentList::ACTION_DELETE == (ptrdiff_t)arg)
            {
                if(rlUgc::SetDeleted(m_Gamer->GetLocalIndex(), 
                                     RLUGC_CONTENT_TYPE_GTA5MISSION, 
                                     result->GetContentInfo().GetMetadata().GetContentId(), 
                                     true, 
                                     &m_MyStatus))
                {
                    this->PushSegue(SEGUE_GENERIC);
                }
                else
                {
                    PushResult("Failed to start task");
                }
            }
            else if(UiUgcContentList::ACTION_PLAYERS == (ptrdiff_t)arg)
            {
                m_UiUgcPlayerList.SetContent(result);
                this->PushState(STATE_UGC_PLAYER_LIST);
            }
            else if(UiUgcContentList::ACTION_RATE_CONTENT_0PCT == (ptrdiff_t)arg)
            {
                DoRateContent(0.0f);
            }
            else if(UiUgcContentList::ACTION_RATE_CONTENT_25PCT == (size_t)arg)
            {
                DoRateContent(0.25f);
            }
            else if(UiUgcContentList::ACTION_RATE_CONTENT_50PCT == (size_t)arg)
            {
                DoRateContent(0.50f);
            }
            else if(UiUgcContentList::ACTION_RATE_CONTENT_75PCT == (size_t)arg)
            {
                DoRateContent(0.75f);
            }
            else if(UiUgcContentList::ACTION_RATE_CONTENT_100PCT == (size_t)arg)
            {
                DoRateContent(1.0f);
            }
            else if(UiUgcContentList::ACTION_SET_PLAYER_DATA == (ptrdiff_t)arg)
            {
                if(rlUgc::SetPlayerData(m_Gamer->GetLocalIndex(), 
                                        RLUGC_CONTENT_TYPE_GTA5MISSION, 
                                        result->GetContentInfo().GetMetadata().GetContentId(), 
                                        "{foo:1}", 
                                        NULL,
                                        &m_MyStatus))
                {
                    this->PushSegue(SEGUE_GENERIC);
                }
                else
                {
                    PushResult("Failed to start task");
                }
            }
            else if(UiUgcContentList::ACTION_CLEAR_PLAYER_DATA == (ptrdiff_t)arg)
            {
                if(rlUgc::SetPlayerData(m_Gamer->GetLocalIndex(), 
                                        RLUGC_CONTENT_TYPE_GTA5MISSION, 
                                        result->GetContentInfo().GetMetadata().GetContentId(), 
                                        NULL, 
                                        NULL,
                                        &m_MyStatus))
                {
                    this->PushSegue(SEGUE_GENERIC);
                }
                else
                {
                    PushResult("Failed to start task");
                }
            }
            else if(UiUgcContentList::ACTION_UPDATE_CONTENT == (ptrdiff_t)arg)
            {
                if(rlUgc::UpdateContent(m_Gamer->GetLocalIndex(),
                                        RLUGC_CONTENT_TYPE_GTA5MISSION,
                                        c.GetMetadata().GetContentId(),
                                        "Updated Name",   //contentName
                                        NULL,   //dataJson
                                        "Updated description",   //description
                                        NULL, //files
                                        0, //numFiles
                                        NULL,   //tagCsv
                                        &g_UgcMetadata,
                                        &m_MyStatus))
                {
                    this->PushSegue(SEGUE_UGC_UPDATE_CONTENT);
                }
                else
                {
                    PushResult("Failed to start task");
                }
            }
        }
    }
*/
}

void App::OnFriendEvent(const rlFriendEvent* evt)
{
	const unsigned evtId = evt->GetId();
	if (RLFRIEND_EVENT_READY == evtId || RLFRIEND_EVENT_LIST_CHANGED == evtId || RLFRIEND_EVENT_STATUS_CHANGED == evtId || 
		RLFRIEND_EVENT_TITLE_CHANGED == evtId || RLFRIEND_EVENT_SESSION_CHANGED == evtId || RLFRIEND_EVENT_UPDATED == evtId)
	{
		m_UiFriendsList.QueueRefreshFriendsList();
	}
}

#if LIVESTREAM_ENABLED
void App::VideoCaptureCallback(unsigned width, unsigned height, unsigned char* destBuffer)
{
	if (!destBuffer)
	{
		return;
	}

	// Get DXD Context
	ID3D11DeviceContext* context = g_grcCurrentContext;

	grcTexture* pBackBuffer = grcTextureFactory::GetInstance().GetBackBuffer(true);

	if (pBackBuffer)
	{
		ID3D11Resource* backbufferTexturePtr = static_cast<ID3D11Resource*>(pBackBuffer->GetTexturePtr()); 

		if (backbufferTexturePtr)	
		{
			grcTextureFactory::TextureCreateParams oParams( grcTextureFactory::TextureCreateParams::STAGING,  
				grcTextureFactory::TextureCreateParams::LINEAR, 
				grcsWrite|grcsRead, 
				NULL,  
				grcTextureFactory::TextureCreateParams::NORMAL,  
				grcTextureFactory::TextureCreateParams::MSAA_NONE);

			grcTexture* pictureTexture = grcTextureFactoryPC::GetInstance().Create(width, height, (u32)grctfA8R8G8B8, NULL, 1U, &oParams);
			ID3D11Resource* pictureCachedTexturePtr = (ID3D11Resource*)(pictureTexture->GetCachedTexturePtr());

			if (pictureTexture)
			{
				if (pBackBuffer->GetMSAA() != grcDevice::MSAA_None)
				{
					ID3D11Resource* backbufferTexturePtr = (pBackBuffer)->GetTexturePtr();
					context->ResolveSubresource(pictureCachedTexturePtr, 0, backbufferTexturePtr, 0, DXGI_FORMAT_R8G8B8A8_UNORM);
				}
				else
				{
					ID3D11Resource* backbufferTexturePtr = pBackBuffer->GetTexturePtr();
					context->CopyResource(pictureCachedTexturePtr, backbufferTexturePtr);
				}

				((grcTextureD3D11*)pictureTexture)->UpdateCPUCopy();

				grcTextureLock oLock;
				if (SUCCEEDED(pictureTexture->LockRect(0,0,oLock))) 
				{
					u32 pitch = oLock.Pitch;
					u32 *s = (u32*) oLock.Base;

					// Transfer Texture to Image
					u32 *d = (u32*) destBuffer;
					for (unsigned row = 0; row < height; row++,s = (u32*)((char*)s + pitch),d += width) 
					{
						for (unsigned col = 0; col < width; col++)
						{
							Color32 oColor = Color32(s[col]);
							{
								d[col] = 0xFF000000 | oColor.GetRed() << 16 | oColor.GetGreen() << 8 | oColor.GetBlue();
							}
						}
					}
					pictureTexture->UnlockRect(oLock);

					rlLiveStream::SubmitVideoFrame(destBuffer);
				}

				pictureTexture->Release();
				return;
			}
		}
	}
}

void 
App::OnLiveStreamEvent(const rlLiveStreamEvent* evt)
{
	const unsigned evtId = evt->GetId();
	if (evtId == RLSTREAM_EVENT_INITIALIZED)
	{
		m_LiveStreamMenu.UpdateEnabled();
	}
	else if (evtId == RLSTREAM_EVENT_AUTHENTICATING)
	{
		m_LiveStreamMenu.UpdateEnabled();
	}
	else if (evtId == RLSTREAM_EVENT_AUTHENTICATED)
	{
		m_LiveStreamMenu.UpdateEnabled();
	}
	else if (evtId == RLSTREAM_EVENT_FOUND_INGEST_SERVER)
	{
		m_LiveStreamMenu.UpdateIngestionServers();
	}
	else if (evtId == RLSTREAM_EVENT_STARTED_STREAMING)
	{
		m_LiveStreamMenu.UpdateEnabled();
	}
	else if (evtId == RLSTREAM_EVENT_STOPPED_STREAMING)
	{
		m_LiveStreamMenu.UpdateEnabled();
	}
	else if (evtId == RLSTREAM_EVENT_PAUSED_STREAMING)
	{
		m_LiveStreamMenu.UpdateEnabled();
	}
	else if (evtId == RLSTREAM_EVENT_PAUSED_STREAMING)
	{
		m_LiveStreamMenu.UpdateEnabled();
	}
	else if (evtId == RLSTREAM_EVENT_UNINITIALIZED)
	{
		m_LiveStreamMenu.UpdateEnabled();
	}
}
#endif // LIVESTREAM_ENABLED

void 
App::OnServiceEvent(sysServiceEvent* evt)
{
	if (evt == NULL)
	{
		return;
	}

	Displayf("App::OnServiceEvent for type %s", evt->GetDebugName());

	if(evt->GetType() == sysServiceEvent::SUSPENDED)
	{
		rlGetTaskManager()->CancelAll();
		netTask::CancelAll();
	}
}

void App::OnRosEvent(const rlRosEvent& evt)
{
	const unsigned evtId = evt.GetId();

	switch (evtId)
	{
	case RLROS_EVENT_RETRIEVED_GEOLOC_INFO:
		{
			const rlRosEventRetrievedGeoLocInfo& locEvt = static_cast<const rlRosEventRetrievedGeoLocInfo&>(evt);
			if (strcmp(locEvt.m_GeoLocInfo.m_CountryCode, "CN") == 0)
			{
#if RL_FACEBOOK_ENABLED
				rlFacebook::SetKillSwitchEnabled(true);
#endif // #if RL_FACEBOOK_ENABLED

				const bool hasNetwork = rlPeerAddress::HasNetwork();
				const bool isSignedIn = m_Gamer && m_Gamer->IsSignedIn();
				const bool isOnline = m_Gamer && m_Gamer->IsOnline();
				const bool hasInvites = isOnline && m_UiInvitesReceived.HasInvites();

				m_UiFrontEnd.UpdateEnabled(hasNetwork, isSignedIn, isOnline, hasInvites);
			}
		}
		break;
	}
}

void 
App::OnPresenceEvent(const rlPresenceEvent* evt)
{
    const unsigned evtId = evt->GetId();

    if(PRESENCE_EVENT_INVITE_RECEIVED == evtId)
    {
        rlPresenceEventInviteReceived* ie = (rlPresenceEventInviteReceived*)evt;

        Displayf("App: received an invite from %s, salutation=%s", ie->m_InviterName, ie->m_Salutation);

        m_UiInvitesReceived.AddInvite(ie->m_InviterName, ie->m_SessionInfo);
    }
    else if(PRESENCE_EVENT_SIGNIN_STATUS_CHANGED == evtId)
    {
        const int gamerIdx = evt->m_GamerInfo.GetLocalIndex();
        if(AssertVerify(gamerIdx >= 0 && (unsigned)gamerIdx < COUNTOF(m_LocalGamers)))
        {
            Gamer* gamer = &m_LocalGamers[gamerIdx];
            //Assert(!gamer->IsValid()
            //        || evt->m_GamerInfo.GetGamerId() == gamer->GetGamerId());

            gamer->m_GamerInfo = evt->m_GamerInfo;

            if(evt->m_SigninStatusChanged->SignedIn()
                || evt->m_SigninStatusChanged->SignedOnline()
                || evt->m_SigninStatusChanged->SignedOffline())
            {
                if(!m_Gamer || gamer == m_Gamer)
                {
                    //Refresh the UI to match the gamer state.
                    this->SetGamer(gamer);
                }
            }
            else if(evt->m_SigninStatusChanged->SignedOut())
            {
                if(gamer == m_Gamer)
                {
                    m_LeavingByChoice = true;

                    this->DestroySession();

                    this->SetGamer(NULL);

                    m_LeavingByChoice = false;
                }
            }
        }
    }
    else if(PRESENCE_EVENT_NETWORK_STATUS_CHANGED == evtId)
    {
        const int gamerIdx = evt->m_GamerInfo.GetLocalIndex();
        if(AssertVerify(gamerIdx >= 0 && (unsigned)gamerIdx < COUNTOF(m_LocalGamers)))
		{
			Gamer* gamer = &m_LocalGamers[gamerIdx];
            Assert(!gamer->IsValid()
                    || evt->m_GamerInfo.GetGamerId() == gamer->GetGamerId());

            gamer->m_GamerInfo = evt->m_GamerInfo;
        }
    }
	else if (PRESENCE_EVENT_GAME_SESSION_READY == evt->GetId())
	{
#if __ASSERT
		Gamer* gamer = this->GetLocalGamerById(evt->m_GamerInfo.GetGamerId());
		Assert(gamer && gamer->IsOnline());
#endif

		m_UiInvitesReceived.AddInvite("", evt->m_GameSessionReady->m_SessionInfo);
	}
    else if(PRESENCE_EVENT_INVITE_ACCEPTED == evt->GetId()
            || PRESENCE_EVENT_JOINED_VIA_PRESENCE == evt->GetId())
    {
        //FIXME (KB) - If there is a session create pending we must
        //defer until the create completes.

        Gamer* gamer = this->GetLocalGamerById(evt->m_GamerInfo.GetGamerId());
        Assert(gamer && gamer->IsOnline());

        rlSlotType slotType;

        if(PRESENCE_EVENT_INVITE_ACCEPTED == evt->GetId())
        {
            //If we're invited then join in a private slot.
            slotType = RL_SLOT_PRIVATE;
        }
        else
        {
            slotType = RL_SLOT_PUBLIC;
        }

        this->JoinSession(gamer,
                          evt->m_InviteAccepted->m_SessionInfo,
                          RL_NETMODE_ONLINE,
                          slotType);
    }
    else if(PRESENCE_EVENT_MSG_RECEIVED == evtId)
    {
#if !__NO_OUTPUT
        rlPresenceEventMsgReceived* fr = (rlPresenceEventMsgReceived*)evt;
		Displayf("App: received a message from %s: %s", fr->m_SenderName, (const char*)fr->m_Data);
#endif
	}
	else if (PRESENCE_EVENT_PARTY_CHANGED == evtId)
	{
		m_UiPartyMainMenu.RefreshParty();
	}
	else if (PRESENCE_EVENT_PARTY_MEMBERS_AVAILABLE == evtId)
	{
#if RSG_DURANGO
		rlPresenceEventPartyMembersAvailable* pmaEvent = (rlPresenceEventPartyMembersAvailable*)evt;
		if (m_LiveMgr->GetSession()->Exists() &&
			m_LiveMgr->GetSession()->IsPresenceEnabled() &&
			m_LiveMgr->GetSession()->GetSessionInfo().IsValid() &&
			m_LiveMgr->GetSession()->IsHost())
		{
			rlGamerHandle groupMembers[RL_MAX_GAMERS_PER_SESSION];
			for (int i = 0; i < pmaEvent->m_NumAvailablePlayers; i++)
			{
				groupMembers[i].ResetXbl(pmaEvent->m_AvailablePlayers[i].GetXuid());
			}

			m_LiveMgr->GetSession()->ReserveSlots(groupMembers, pmaEvent->m_NumAvailablePlayers, RL_SLOT_PUBLIC, snSession::SLOT_RESERVATION_TTL_MS, false);
		}
#endif
	}
}

void 
App::OnSessionEvent(snSession* /*session*/, const snEvent* evt)
{
    const unsigned eid = evt->GetId();

    if(SNET_EVENT_SESSION_HOSTED == eid
            || SNET_EVENT_SESSION_JOINED == eid)
    {
    }
    else if(SNET_EVENT_JOIN_FAILED == eid)
    {
    }
	else if(SNET_EVENT_ADDING_GAMER == eid)
	{
		const snEventAddingGamer* ag = evt->m_AddingGamer;
		m_PeerMgr.AddTemporaryPlayer(ag->m_GamerInfo,
									ag->m_EndpointId,
									ag->m_SizeofData,
									ag->m_Data);
	}
    else if(SNET_EVENT_ADDED_GAMER == eid)
    {
        Gamer* gamer = 0;

        const snEventAddedGamer* ag = evt->m_AddedGamer;
        if(ag->m_GamerInfo.IsRemote())
        {
            gamer = m_PeerMgr.AddRemoteGamer(ag->m_GamerInfo, ag->m_EndpointId, ag->m_SizeofData, ag->m_Data);
            Assert(gamer);
        }
        else
        {
            for(unsigned i = 0; i < COUNTOF(m_LocalGamers); ++i)
            {
                if(m_LocalGamers[i].m_GamerInfo == ag->m_GamerInfo)
                {
                    gamer = &m_LocalGamers[i];
                }
            }

            Assert(gamer);

            AssertVerify(m_PeerMgr.AddLocalGamer(gamer,
                                                ag->m_SizeofData,
                                                ag->m_Data));
        }

        if(gamer)
        {
            if(m_LiveMgr->GetSession()->IsHost())
            {
                //The host assigns team ids.
                //These values will be serialized and sent to remote
                //gamers.  See SNET_EVENT_GET_GAMER_DATA.

                //Generate a random unique team id.

                bool haveUnique;
                int teamId = -1;
                Gamer* gamers[RL_MAX_GAMERS_PER_SESSION];

                const unsigned numGamers =
                    m_PeerMgr.GetGamers(gamers, COUNTOF(gamers));

                do
                {
                    static const int MAX_POSITIVE_INT = int(~0u >> 1);

                    haveUnique = true;
                    teamId = m_Rng.GetRanged(int(0), MAX_POSITIVE_INT);

                    for(unsigned i = 0; i < numGamers; ++i)
                    {
                        if(teamId == gamers[i]->m_TeamId)
                        {
                            haveUnique = false;
                            break;
                        }
                    }
                }
                while(!haveUnique);

                Assert(teamId >= 0);

                gamer->m_TeamId = teamId;

				// inform of match start
				if (GetState() == STATE_GAME)
				{
					Peer* peer = m_PeerMgr.GetPeer(gamer->GetPeerInfo().GetPeerId());
					StartMatchMsg msg;
					m_LiveMgr->GetCxnMgr()->Send(peer->GetCxnId(), msg, NET_SEND_RELIABLE, NULL);
				}
            }
        }
    }
    else if(SNET_EVENT_REMOVED_GAMER == eid)
    {
        AssertVerify(m_PeerMgr.RemoveGamer(evt->m_RemovedGamer->m_GamerInfo.GetGamerId()));

        if(m_Gamer
            && evt->m_RemovedGamer->m_GamerInfo.GetGamerId() ==
            m_Gamer->m_GamerInfo.GetGamerId())
        {
            if(!m_LeavingByChoice)
            {
                this->DestroySession();
            }

            m_LeavingByChoice = false;
        }
    }
    else if(SNET_EVENT_SESSION_MIGRATE_START == eid)
    {
    }
    else if(SNET_EVENT_SESSION_MIGRATE_END == eid)
    {
		UpdatePresenceBlob();
    }
    else if(SNET_EVENT_SESSION_ATTRS_CHANGED == eid)
    {
        const rlSessionConfig& config =
            m_LiveMgr->GetSession()->GetConfig();

        m_UiConfigureAttrs.SetAttrs(config.m_Attrs,
            config.m_MaxPublicSlots,
            config.m_MaxPrivateSlots);

		UpdatePresenceBlob();
    }
	else if(SNET_EVENT_DISPLAY_NAME_RETRIEVED)
	{
#if RSG_DURANGO
		Gamer* g = m_PeerMgr.GetGamerById(evt->m_SessionDisplayNameRetrieved->m_GamerInfo.GetGamerId());
		if (g)
		{
			rlDisplayName* d = (rlDisplayName*)evt->m_SessionDisplayNameRetrieved->m_GamerInfo.GetDisplayName();
			g->m_GamerInfo.SetDisplayName(d);
		}
#endif
	}
    else if(SNET_EVENT_SESSION_DESTROYED == eid)
    {
    }
}

void 
App::OnCxnEvent(netConnectionManager* /*cxnMgr*/,
                const netEvent* evt)
{
    unsigned msgId;

    if(NET_EVENT_FRAME_RECEIVED == evt->GetId()
       && evt->m_CxnId >= 0
       && netMessage::GetId(&msgId,
                              evt->m_FrameReceived->m_Payload,
                              evt->m_FrameReceived->m_SizeofPayload))
    {
        const netEventFrameReceived* fr = evt->m_FrameReceived;

        if(SwingMsg::MSG_ID() == msgId)
        {
            SwingMsg swing;

            AssertVerify(swing.Import(fr->m_Payload, fr->m_SizeofPayload));

//            Gamer* gamer = this->GetLocalGamerById(swing.m_GamerId);
            Gamer* gamer = m_PeerMgr.GetGamerById(swing.m_GamerId);
            
            if(gamer)
            {
                Displayf("Swing message from \"%s\"", gamer->GetName());
                this->RecordSwing(gamer, swing);
            }
            else
            {
                Displayf("Swing message from unknown gamer");
            }
        }
        else if(EndMatchMsg::MSG_ID() == msgId)
        {
			Displayf("Received End command from:" NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender));
			PopState();
        }
		else if(StartMatchMsg::MSG_ID() == msgId)
		{
			Displayf("Received Start command from:" NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender));
			PushState(STATE_GAME);
		}
		else if(MigrateHostMsg::MSG_ID() == msgId)
		{
			Displayf("Received migrate host command from:" NET_ADDR_FMT "",
				NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender));

			MigrateHostMsg msg;
			AssertVerify(msg.Import(fr->m_Payload, fr->m_SizeofPayload));
			MigrateHost(msg.m_PeerId);
		}
    }
}

void 
App::HostSession()
{
    if (!AssertVerify(m_Gamer && m_Gamer->IsSignedIn()))
	{
		return;
	}

	if(m_LiveMgr->HostSession(m_Gamer,
                                    m_NetMode,
                                    m_UiHostConfig.GetNumPublicSlots(),
                                    m_UiHostConfig.GetNumPrivateSlots(),
                                    m_UiHostConfig.GetAttrs(),
                                    m_UiHostConfig.GetCreateFlags(),
                                    &m_MyStatus))
    {
        this->PushSegue(SEGUE_SESSION_HOSTING);
    }
    else
    {
        this->PushResult("Error hosting session");
    }
}

void
App::JoinSession(Gamer* gamer,
                const rlSessionInfo& sessionInfo,
                const rlNetworkMode netMode,
                const rlSlotType desiredSlotType)
{
    Assert(gamer && gamer->IsSignedIn());

	if(STATE_MULTIPLAYER_MAIN_MENU != this->GetState())
	{
		this->PopAllStates();
		this->PushState(STATE_MULTIPLAYER_MAIN_MENU);
	}

	m_Joiner = gamer;
	m_SlotType = desiredSlotType;
	m_JoinSessionInfo = sessionInfo;
	m_NetMode = netMode;
	m_JoinSessionDetail.Clear();

	if(m_LiveMgr->GetSession()->Exists())
	{
		this->PushSegue(SEGUE_SESSION_LEAVING_TO_JOIN_ANOTHER);
		this->LeaveSession(gamer);
	}
	else if(rlSessionManager::QueryDetail(netMode,
		LiveManager::SESSION_CHANNEL_ID,
		rlSessionManager::HOST_QUERY_DEFAULT_TIMEOUT_MS,
		rlSessionManager::HOST_QUERY_DEFAULT_MAX_ATTEMPTS,
		rlSessionManager::DEFAULT_NUM_QUERY_RESULTS_BEFORE_EARLY_OUT,
		true,
		&m_JoinSessionInfo,
		1,
		&m_JoinSessionDetail,
		&m_NumJoinSessionDetailResults,
		&m_MyStatus))
	{
		this->PushSegue(SEGUE_SESSION_QUERYING_CONFIG);
	}
	else
	{
		this->PushResult("Error joining session");
	}
}

void
App::MigrateHostToMyself()
{
	if(m_LiveMgr->GetSession()->Exists() && m_LiveMgr->GetSession()->IsMigrating() == false)
	{
		//Candidates for new host.
		rlPeerInfo candidates[RL_MAX_GAMERS_PER_SESSION];

		u32 numCandidates = 0;

		if(m_LiveMgr->GetSession()->IsHost())
		{
			// we're already the host
		}
		else
		{
			candidates[numCandidates++] = m_LiveMgr->GetSession()->GetLocalPeerInfo();

			//There should always be at least one candidate - us.
			Assert(numCandidates);

			AssertVerify(m_LiveMgr->GetSession()->Migrate());

			MigrateHostMsg msg(candidates[0].GetPeerId());

			Peer* peers[PeerManager::MAX_PEERS];
			const unsigned numPeers = m_PeerMgr.GetPeers(peers, COUNTOF(peers));

			for(unsigned i = 0; i < numPeers; ++i)
			{
				if(Verifyf(!peers[i]->IsLocal(), "Peer list should not contain local peer"))
				{
					m_LiveMgr->GetCxnMgr()->Send(peers[i]->GetCxnId(),
												 msg,
												 NET_SEND_RELIABLE,
												 NULL);
				}
			}
		}
	}
}

void
App::MigrateHost(u64 peerId)
{
	if(m_LiveMgr->GetSession()->Exists() && m_LiveMgr->GetSession()->IsMigrating() == false)
    {
		//Candidates for new host.
		rlPeerInfo candidates[RL_MAX_GAMERS_PER_SESSION];

		u32 numCandidates = 0;

		Gamer* gamers[PeerManager::MAX_PEERS];
		const unsigned numPeers = m_PeerMgr.GetGamers(gamers, COUNTOF(gamers));
		for(unsigned i = 0; i < numPeers; ++i)
		{
			if(gamers[i]->GetPeerInfo().GetPeerId() == peerId)
			{
				candidates[numCandidates++] = gamers[i]->GetPeerInfo();
				break;
			}
		}

		//There should always be at least one candidate - us.
		Assert(numCandidates);

		AssertVerify(m_LiveMgr->GetSession()->Migrate());
    }
}

void
App::LeaveSession(Gamer* gamer)
{
    Assert(gamer && gamer->IsSignedIn());

    Assert(!m_LeavingByChoice);
    m_LeavingByChoice = true;

    if(m_LiveMgr->GetSession()->Exists())
    {
        if(m_LiveMgr->GetSession()->Leave(gamer->GetLocalIndex(), &m_MyStatus))
        {
            this->PushSegue(SEGUE_SESSION_LEAVING);
        }
        else
        {
            this->PushResult("Error leaving session");
        }
    }

#if RSG_NP || __STEAM_BUILD
    //Clear our presence data.
    rlPresence::SetBlob(0, 0);
#endif
}

void 
App::DestroySession()
{
	if (m_MyStatus.Pending())
		return;

    if(m_LiveMgr->GetSession()->Exists())
    {
        if(m_LiveMgr->GetSession()->Destroy(&m_MyStatus))
        {
            this->PushSegue(SEGUE_SESSION_DESTROYING);
        }
        else
        {
            this->PushResult("Error destroying session");
        }
    }
}

void 
App::FindSessions()
{
    Assert(m_Gamer && m_Gamer->IsSignedIn());

    if(m_LiveMgr->FindSessions(m_NetMode,
                                m_Gamer,
                                1,
                                m_UiQueryConfig.GetFilter(),
                                m_SessionDetails,
                                MAX_SEARCH_RESULTS,
								0,
                                &m_NumSearchResults,
                                &m_MyStatus))
    {
        this->PushSegue(SEGUE_SESSION_SEARCHING);
    }
    else
    {
        this->PushResult("Error finding sessions");
    }
}

void
App::ChangeAttributes()
{
    rlMatchingAttributes attrs;
    unsigned numPubSlots, numPrivSlots;

    m_UiConfigureAttrs.GetAttrs(&attrs, &numPubSlots, &numPrivSlots);
    if(m_LiveMgr->GetSession()->ChangeAttributes(attrs, &m_MyStatus))
    {
        this->PushSegue(SEGUE_SESSION_CHANGING_ATTRIBUTES);
    }
    else
    {
        this->PushResult("Error changing attributes");
    }
}

void 
App::UpdatePresenceBlob()
{
#if RSG_NP || __STEAM_BUILD
	//Put our session info in our presence data, so friends can join via presence.
	//NOTE: Sessions created with the PRIVATE flag should NOT advertise their presence,
	//      as they are intended to only be joined via invites.
	//if(!m_LiveMgr->GetSession()->IsPrivate())
	//The session might not exist at this point if we've destroyed
	//it and we're just passing through the lobby on our way out to the
	//front end.
	if(m_LiveMgr->GetSession()->Exists())
	{
		const rlSessionInfo& sessionInfo = m_LiveMgr->GetSession()->GetSessionInfo();

		u8 data[RL_PRESENCE_MAX_BUF_SIZE];
		unsigned dataSize = 0;
#if RSG_NP
		if(sessionInfo.ExportForNpPresence(data, sizeof(data), &dataSize))
#else
		if(sessionInfo.ToString(data, &dataSize))
#endif
		{
			rlPresence::SetBlob(data, dataSize);
		}		
	}
#endif  //RSG_NP
}

void
App::ToggleInvitable()
{
    unsigned flags = m_LiveMgr->GetSession()->GetPresenceFlags();
    if(m_LiveMgr->GetSession()->IsInvitable())
    {
        flags |= RL_SESSION_PRESENCE_FLAG_INVITABLE;
    }
    else
    {
        flags &= ~RL_SESSION_PRESENCE_FLAG_INVITABLE;
    }

    if(m_LiveMgr->GetSession()->ModifyPresenceFlags(flags, &m_MyStatus))
    {
        this->PushSegue(SEGUE_SESSION_TOGGLING_INVITABLE);
    }
    else
    {
        this->PushResult("Error toggling invitable");
    }
}

void 
App::StartMatch()
{
	Assert(m_LiveMgr->GetSession()->IsHost());

	Peer* peers[PeerManager::MAX_PEERS];
	const unsigned numPeers = m_PeerMgr.GetPeers(peers, COUNTOF(peers));

	StartMatchMsg msg;

	for(unsigned i = 0; i < numPeers; ++i)
	{
		if(Verifyf(!peers[i]->IsLocal(), "Peer list should not contain local peer"))
		{
			m_LiveMgr->GetCxnMgr()->Send(peers[i]->GetCxnId(),
				msg,
				NET_SEND_RELIABLE,
				NULL);
		}
	}

	this->PushSegue(App::SEGUE_SESSION_STARTING);
}

void 
App::EndMatch()
{
    Assert(m_LiveMgr->GetSession()->IsHost());

	Peer* peers[PeerManager::MAX_PEERS];
	const unsigned numPeers = m_PeerMgr.GetPeers(peers, COUNTOF(peers));

	EndMatchMsg msg;

	for(unsigned i = 0; i < numPeers; ++i)
	{
		if(Verifyf(!peers[i]->IsLocal(), "Peer list should not contain local peer"))
		{
			m_LiveMgr->GetCxnMgr()->Send(peers[i]->GetCxnId(),
				msg,
				NET_SEND_RELIABLE,
				NULL);
		}
	}

	this->PushSegue(SEGUE_SESSION_ENDING);
}

void 
App::Swing()
{
    Assert(m_Gamer);

    Peer* peers[PeerManager::MAX_PEERS];
    const unsigned numPeers = m_PeerMgr.GetPeers(peers, COUNTOF(peers));
    
    SwingMsg msg;
    msg.m_GamerId = m_Gamer->GetGamerId();

    const bool strike = (sysTimer::GetSystemMsTime() & 0x7) < 5;

    if(strike)
    {
        msg.m_HitResult = HIT_NONE;
    }
    else
    {
        msg.m_HitResult = sysTimer::GetSystemMsTime() % HIT_NUM_RESULTS;
    }

    RecordSwing(m_Gamer, msg);

    for(unsigned i = 0; i < numPeers; ++i)
    {
        if(Verifyf(!peers[i]->IsLocal(), "Peer list should not contain local peer"))
        {
            m_LiveMgr->GetCxnMgr()->Send(peers[i]->GetCxnId(),
                                         msg,
                                         NET_SEND_RELIABLE,
                                         NULL);
        }
    }
}

void
App::PushSegue(const Segue segue)
{
    if(m_SegueStack.GetCapacity() == 0)
    {
        m_SegueStack.Reserve(16);
    }

    Segue newSegue = segue;

    switch(int(segue))
    {
    case SEGUE_GENERIC:
        m_UiSegue.SetText("Request pending...");
        break;
    case SEGUE_SESSION_HOSTING:
        m_UiSegue.SetText("Creating Host Session...");
        break;
    case SEGUE_SESSION_SEARCHING:
        m_UiSegue.SetText("Searching for sessions...");
        break;
    case SEGUE_SESSION_QUERYING_CONFIG:
	case SEGUE_SESSION_JVP_LOOKUP:
        m_UiSegue.SetText("Querying session config..");
        break;
    case SEGUE_SESSION_JOINING:
        m_UiSegue.SetText("Joining session...");
        break;
    case SEGUE_SESSION_CHANGING_ATTRIBUTES:
        m_UiSegue.SetText("Changing attributes...");
        break;
    case SEGUE_SESSION_TOGGLING_INVITABLE:
        m_UiSegue.SetText("Toggling invitable...");
        break;
    case SEGUE_SESSION_LEAVING:
    case SEGUE_SESSION_LEAVING_TO_JOIN_ANOTHER:
    case SEGUE_PARTY_LEAVING_GAME_TO_JOIN_ANOTHER:
        m_UiSegue.SetText("Leaving...");
//#if RSG_NP
//        //Clear our presence data.
//        rlPresence::SetPresence(0, 0);
//#endif
        break;
    case SEGUE_SESSION_DESTROYING:
        m_UiSegue.SetText("Destroying session...");
        break;
    case SEGUE_SESSION_STARTING:
        m_UiSegue.SetText("Starting session...");
        break;
    case SEGUE_SESSION_ENDING:
        m_UiSegue.SetText("Ending session...");
        break;
    case SEGUE_GETTING_LEADERBOARD_SIZE:
        m_UiSegue.SetText("Getting leaderboard size...");
        break;
    case SEGUE_WRITING_LEADERBOARD2:
        m_UiSegue.SetText("Writing leaderboard...");
        break;
    case SEGUE_READING_LEADERBOARD2:
        m_UiSegue.SetText("Reading leaderboard...");
        break;
    case SEGUE_READING_STATS:
        m_UiSegue.SetText("Reading stats...");
        break;
    case SEGUE_READING_INBOX_MESSAGES:
        m_UiSegue.SetText("Reading inbox messages...");
        break;
    case SEGUE_PARTY_HOSTING:
        m_UiSegue.SetText("Hosting Party..");
        break;
    case SEGUE_PARTY_JOINING:
        m_UiSegue.SetText("Joining Party...");
        break;
    case SEGUE_PARTY_LEAVING:
    case SEGUE_PARTY_LEAVING_TO_LEAVE_GAME:
    case SEGUE_PARTY_LEAVING_TO_HOST_GAME:
        m_UiSegue.SetText("Leaving Party...");
        break;
    case SEGUE_SOCIAL_CLUB_GET_TERMS_OF_SERVICE:
        m_UiSegue.SetText("Retrieving Terms of Service from the Social Club...");
        break;
    case SEGUE_SOCIAL_CLUB_LINK_OR_UNLINK_ACCOUNT:
        m_UiSegue.SetText("Updating link...");
        break;
    case SEGUE_SOCIAL_CLUB_CREATE_ACCOUNT:
        m_UiSegue.SetText("Creating a new Social Club account...");
        break;
    case SEGUE_SOCIAL_CLUB_ACCEPT_TERMS_OF_SERVICE:
        m_UiSegue.SetText("Accepting Social Club Terms of Service...");
        break;
    case SEGUE_SOCIAL_CLUB_RESET_PASSWORD:
        m_UiSegue.SetText("Requesting reset password e-mail from the Social Club...");
        break;
    case SEGUE_SOCIAL_CLUB_GET_COUNTRIES:
        m_UiSegue.SetText("Retrieving Social Club Countries...");
        break;
    case SEGUE_SOCIAL_CLUB_UPDATE_PASSWORD:
        m_UiSegue.SetText("Updating password...");
		break;
	case SEGUE_SOCIAL_CLUB_POST_USER_FEED_ACTIVITY:
		m_UiSegue.SetText("Posting to feed...");
		break;
	case SEGUE_FACEBOOK_GET_ACCESS_TOKEN:
		m_UiSegue.SetText("Getting FB Access token...");
        break;
	case SEGUE_FACEBOOK_POST_OPEN_GRAPH:
		m_UiSegue.SetText("Posting to Facebook...");
        break;
	case SEGUE_FACEBOOK_VALIDATE_APP_PERMISSIONS:
		m_UiSegue.SetText("Checking app permissions...");
		break;
    case SEGUE_READING_SC_FRIENDS:
        m_UiSegue.SetText("Reading...");
        break;
    case SEGUE_CLOUD_DOWNLOAD:
        m_UiSegue.SetText("Downloading...");
        break;
    case SEGUE_CLOUD_UPLOAD:
        m_UiSegue.SetText("Uploading...");
        break;
    case SEGUE_CLOUD_DELETE:
        m_UiSegue.SetText("Deleting...");
        break;
	case SEGUE_CLOUD_SAVE_IDENTIFY_CONFLICTS:
		m_UiSegue.SetText("Cloud Save - Get Manifest...");
		break;
	case SEGUE_CLOUD_SAVE_GET_FILE:
		m_UiSegue.SetText("Cloud Save - Get File...");
		break;
	case SEGUE_CLOUD_SAVE_POST_FILE:
		m_UiSegue.SetText("Cloud Save - Post File...");
		break;
	case SEGUE_CLOUD_REGISTER_FILE:
		m_UiSegue.SetText("Cloud Save - Register File");
		break;
	case SEGUE_CLOUD_UNREGISTER_FILE:
		m_UiSegue.SetText("Cloud Save - Unregister File");
		break;
	case SEGUE_CLOUD_MERGE_FILES:
		m_UiSegue.SetText("Cloud Save - Merge Files");
		break;
	case SEGUE_CLOUD_DELETE_FILES:
		m_UiSegue.SetText("Cloud Save - Delete Files");
		break;
	case SEGUE_CLOUD_QUERY_BETA:
		m_UiSegue.SetText("Cloud Save - Query Beta Access");
		break;
	case SEGUE_CLOUD_BACKUP_CONFLICTS:
		m_UiSegue.SetText("Cloud Save - Backup Conflicts");
		break;
	case SEGUE_CLOUD_SEARCH_BACKUPS:
		m_UiSegue.SetText("Cloud Save - Search Backups");
		break;
	case SEGUE_CLOUD_RESTORE_BACKUP:
		m_UiSegue.SetText("Cloud Save - Restore Backup");
		break;
	case SEGUE_CLOUD_GET_METADATA:
		m_UiSegue.SetText("Cloud Save - Get Metadata");
		break;
    case SEGUE_PRESENCE_QUERY:
    case SEGUE_PRESENCE_QUERY_COUNT:
        m_UiSegue.SetText("Querying presence...");
        break;
    case SEGUE_ACHIEVEMENTS_GET_ROS_ACHIEVEMENTS:
        m_UiSegue.SetText("Reading ROS achievements...");
        break;
    case SEGUE_ACHIEVEMENTS_GET_ROS_ACHIEVEMENT_DEFINITIONS:
        m_UiSegue.SetText("Reading ROS achievement definitions...");
        break;
    case SEGUE_ACHIEVEMENTS_AWARD_ACHIEVEMENT:
		m_UiSegue.SetText("Awarding an achievement...");
		break;
	case SEGUE_ACHIEVEMENTS_READ_ACHIEVEMENTS:
		m_UiSegue.SetText("Reading Achievements...");
		break;
	case SEGUE_ACHIEVEMENTS_DELETE_ACHIEVEMENTS:
		m_UiSegue.SetText("Deleting Achievements...");
		break;
	case SEGUE_CLAN_RETRIEVING_MINE:
        m_UiSegue.SetText("Retrieving my clan information...");
        break;
    case SEGUE_CLAN_RETRIEVING_MINE_TO_INVITE:
        m_UiSegue.SetText("Retrieving my clan information...");
        break;
    case SEGUE_CLAN_RETRIEVING_ALL:
        m_UiSegue.SetText("Retrieving clans...");
        break;
    case SEGUE_CLAN_RETRIEVING_ALL_LEADERS:
        m_UiSegue.SetText("Retrieving leaders...");
        break;
    case SEGUE_CLAN_RETRIEVING_MEMBERS:
        m_UiSegue.SetText("Retrieving clan members...");
        break;
    case SEGUE_CLAN_RETRIEVING_RANKS:
        m_UiSegue.SetText("Retrieving clan ranks...");
        break;
    case SEGUE_CLAN_RETRIEVING_INVITES:
        m_UiSegue.SetText("Retrieving clan invitations...");
        break;
    case SEGUE_CLAN_RETRIEVING_WALL_MESSAGES:
        m_UiSegue.SetText("Retrieving clan messages...");
        break;
    case SEGUE_CLAN_RETRIEVING_PRIMARY_CLAN:
        m_UiSegue.SetText("Retrieving primary clan...");
        break;
    case SEGUE_CLAN_RETRIEVING_FEUD_STAT:
        m_UiSegue.SetText("Retrieving feud stat...");
        break;
    case SEGUE_CLAN_UPDATING:
        m_UiSegue.SetText("Updating clan...");
        break;
    case SEGUE_CLAN_RANK_UPDATING:
        m_UiSegue.SetText("Updating clan rank information...");
        break;
    case SEGUE_CLAN_MEMBER_UPDATING:
        m_UiSegue.SetText("Updating clan membership...");
        break;
    case SEGUE_CLAN_INVITE_UPDATING:
        m_UiSegue.SetText("Updating clan invites...");
        break;
    case SEGUE_CLAN_JOIN_REQUEST_DELETE:
        m_UiSegue.SetText("Delete join request...");
        break;
    case SEGUE_CLAN_MESSAGE_UPDATING:
        m_UiSegue.SetText("Updating clan messages...");
        break;
    case SEGUE_CLAN_INVITE_TO_CLAN:
        m_UiSegue.SetText("Inviting friend to clan...");
        break;
    case SEGUE_PROFILE_STATS_SYNCHRONIZE:
        m_UiSegue.SetText("Reading all profile stats for gamer...");
        break;
    case SEGUE_PROFILE_STATS_READ_BY_GAMER:
        m_UiSegue.SetText("Reading profile stats by gamer...");
        break;
    case SEGUE_PROFILE_STATS_FLUSH:
        m_UiSegue.SetText("Writing profile stats...");
        break;
    case SEGUE_COMMUNITY_STATS_READ:
        m_UiSegue.SetText("Reading community stats...");
        break;
    case SEGUE_UGC_COPY_CONTENT: m_UiSegue.SetText("Copying content..."); break;
    case SEGUE_UGC_CREATE_CONTENT: m_UiSegue.SetText("Creating content..."); break;
    case SEGUE_UGC_UPDATE_CONTENT: m_UiSegue.SetText("Updating content..."); break;
    case SEGUE_UGC_VERSION_CONTENT: m_UiSegue.SetText("Versioning content..."); break;
    case SEGUE_UGC_QUERY_CONTENT: m_UiSegue.SetText("Querying..."); break;
    case SEGUE_UGC_PUBLISH_CONTENT: m_UiSegue.SetText("Publishing..."); break;
    case SEGUE_UGC_CREATE_VIDEO_CONTENT: m_UiSegue.SetText("Creating video..."); break;
    case SEGUE_UGC_UPLOADING_VIDEO: m_UiSegue.SetText("Uploading video..."); break;
    case SEGUE_CLAN_JOIN_REQUEST:
        m_UiSegue.SetText("Sending join request...");
        break;
    case SEGUE_CLAN_RETRIEVING_SENT_REQUESTS:
        m_UiSegue.SetText("Retrieving sent requests...");
        break;
    case SEGUE_CLAN_RETRIEVING_REQUESTS:
        m_UiSegue.SetText("Retrieving recieved requests...");
        break;
	case SEGUE_PROFILE_STATS_RESET_BY_GROUP:
		m_UiSegue.SetText("Resetting stats by group...");
		break;
    default:
        Assert(false);
        newSegue = SEGUE_INVALID;
        break;
    }

    if(SEGUE_INVALID != newSegue)
    {
        if(STATE_SEGUE != this->GetState())
        {
            this->PushState(STATE_SEGUE);
        }

        m_SegueStack.Push(newSegue);
    }
}

void
App::PopSegue()
{
    if(AssertVerify(STATE_SEGUE == this->GetState()))
    {
        Assert(m_SegueStack.GetCount() > 0);

        m_SegueStack.Pop();

        if(SEGUE_INVALID == this->GetSegue())
        {
            this->PopState();
        }
    }
}

App::Segue
App::GetSegue() const
{
    return m_SegueStack.GetCount() > 0 ? m_SegueStack.Top() : SEGUE_INVALID;
}

void 
App::SetState(const State state)
{
    if(this->GetState() >= 0)
    {
        m_Pages[this->GetState()]->OnFocusChange(false);
    }

    switch(int(state))
    {
    case STATE_LOBBY:
		{
			UpdatePresenceBlob();
#if RSG_NP || __STEAM_BUILD
			rlPresence::SetStatusString("In the lobby");
#endif
		}
        break;

    case STATE_GAME:
#if RSG_NP
        rlPresence::SetStatusString("In a match");
#endif
        break;

    case STATE_CONFIGURING_ATTRIBUTES:
        {
            const rlSessionConfig& config =
                m_LiveMgr->GetSession()->GetConfig();
            m_UiConfigureAttrs.SetAttrs(config.m_Attrs,
                                        config.m_MaxPublicSlots,
                                        config.m_MaxPrivateSlots);
        }
        break;
    }

    m_Pages[state]->OnFocusChange(true);
}

void
App::PushState(const State state)
{
    if(m_StateStack.GetCapacity() == 0)
    {
        m_StateStack.Reserve(16);
    }

    for(int i = 0; i < m_StateStack.GetCount(); ++i)
    {
        Assert(state != m_StateStack[i]);
    }

    this->SetState(state);
    m_StateStack.Push(state);
}

void
App::PushResult(const char* resultText)
{
    m_UiGenericResult.SetText(resultText);
    this->PushState(STATE_GENERIC_RESULT);
}

void
App::PopState()
{
    Assert(m_StateStack.GetCount() > 1);

    //We should pop out of STATE_SEGUE only
    //when we're not showing a segue
    Assert(STATE_SEGUE != this->GetState()
            || SEGUE_INVALID == this->GetSegue());

    m_StateStack.Pop();
    this->SetState(m_StateStack.Top());
}

void
App::PopStateTo(const State state)
{
    if(AssertVerify(this->InState(state)))
    {
        while(this->GetState() != state)
        {
            this->PopState();
        }
    }
}

void
App::PopAllStates()
{
    while(m_StateStack.GetCount() > 1)
    {
        this->PopState();
    }
}

App::State
App::GetState() const
{
    return m_StateStack.GetCount() > 0 ? m_StateStack.Top() : STATE_INVALID;
}

bool
App::InState(const State state) const
{
    for(int i = 0; i < m_StateStack.GetCount(); ++i)
    {
        if(state == m_StateStack[i])
        {
            return true;
        }
    }

    return false;
}

void
App::HandleGetGamerData(snSession* /*session*/,
                        const rlGamerInfo& gamerInfo,
                        snGetGamerData* gamerData) const
{
    const Gamer* gamer =
        m_PeerMgr.GetGamer(gamerInfo.GetGamerId());
    if(gamer)
    {
        AssertVerify(gamer->ExportData(gamerData->m_Data,
                                        sizeof(gamerData->m_Data),
                                        &gamerData->m_SizeofData));
    }
	else
	{
		const Gamer* tempGamer = m_PeerMgr.GetTempPlayer(gamerInfo.GetGamerId());
		if (tempGamer)
		{
			AssertVerify(tempGamer->ExportData(gamerData->m_Data, sizeof(gamerData->m_Data), &gamerData->m_SizeofData));
		}
	}
}

bool
App::HandleJoinRequest(snSession* /*session*/,
                        const rlGamerInfo& /*gamerInfo*/,
                        snJoinRequest* /*joinRequest*/) const
{
    return true;
}

ProfileStatData*
App::GetLocalStat(const int statId)
{
    for(unsigned i = 0; i < m_NumLocalProfileStats; i++)
    {
        if (m_LocalProfileStats[i].m_StatId == statId)
        {
            return &m_LocalProfileStats[i];
        }
    }

    return NULL;
}

rlProfileStatsValue* 
App::GetLocalValue(const int statId)
{
    ProfileStatData *statData = GetLocalStat(statId);

    return statData ? &statData->m_StatValue : NULL;
}

void
App::OnProfileStatsEvent(const rlProfileStatsEvent* e)
{
    switch(e->GetId())
    {
    //case RL_PROFILESTATS_EVENT_STATUS_CHANGED:
    //    {
    //        const rlProfileStatsStatusChangedEvent* evt = (const rlProfileStatsStatusChangedEvent*)e;
    //        
    //        rlDebug3("rlProfileStatsStatusChangedEvent: Profile stats are now %s", 
    //                 evt->m_IsAvailable ? "available" : "unavailable");
    //    }
    //    break;

    case RL_PROFILESTATS_EVENT_GET_LOCAL_VALUE:
        {
            //This is called whenever the profile stats system needs to query the local value.
            //If the value is not tracked by the client, it is legal to simply clear the value.
            const rlProfileStatsGetLocalValueEvent* evt = (const rlProfileStatsGetLocalValueEvent*)e;

            rlProfileStatsValue* val = GetLocalValue(evt->m_StatId);
            if (val)
            {
                *evt->m_Value = *val;
            }
            else if (m_NumLocalProfileStats < MAX_PROFILE_STATS_TRACKED)
            {
                // Add stats almost at random so that we have a chance
                // of using them for flush.
                m_LocalProfileStats[m_NumLocalProfileStats].m_StatId = evt->m_StatId;
                switch (rand() % 3)
                {
                case 2:
                    m_LocalProfileStats[m_NumLocalProfileStats].m_StatValue.SetFloat(0);
                    break;
                case 1:
                    m_LocalProfileStats[m_NumLocalProfileStats].m_StatValue.SetInt64(0);
                    break;
                default:
                    m_LocalProfileStats[m_NumLocalProfileStats].m_StatValue.SetInt32(0);
                    break;
                }

                *evt->m_Value = m_LocalProfileStats[m_NumLocalProfileStats].m_StatValue;

                ++m_NumLocalProfileStats;
            }
            else
            {
                evt->m_Value->Clear();
            }
        }
        break;

    case RL_PROFILESTATS_EVENT_SYNCHRONIZE_STAT:
        {
            //This is called when the value of a backend stat differs from the local value.
            //The client is expected to either mark the stat as dirty (if it is client-authoritative)
            //or store the backend value (if it is server-authoritative).
            const rlProfileStatsSynchronizeStatEvent* evt = (const rlProfileStatsSynchronizeStatEvent*)e;

#if !__NO_OUTPUT
            char buf[64];
            rlDebug3("rlProfileStatsSynchronizeStatEvent: id=%d, val=%s",
                     evt->m_StatId,
                     evt->m_Value->ToString(buf, sizeof(buf)));
#endif

            //We'll pretend that all the stats are server-authoritative.
            //Store their value (up to our limit), and make sure they're marked clean 
            //since we're up-to-date with the backend.
            ProfileStatData* stat = GetLocalStat(evt->m_StatId);
            if (stat)
            {
                stat->m_StatValue = *evt->m_Value;
            }
            else if (m_NumLocalProfileStats < MAX_PROFILE_STATS_TRACKED)
            {
                stat = &m_LocalProfileStats[m_NumLocalProfileStats];
                stat->m_StatId = evt->m_StatId;
                stat->m_StatValue = *evt->m_Value;

                ++m_NumLocalProfileStats;
            }

            if (stat)
            {
                stat->m_bDirty = false;
            }
        }
        break;

    case RL_PROFILESTATS_EVENT_FLUSH_STAT:
        {
            // Clear the dirty flag since this stat was published
            const rlProfileStatsFlushStatEvent* evt = (const rlProfileStatsFlushStatEvent*)e;

            ProfileStatData* stat = GetLocalStat(evt->m_StatId);
            if (stat)
            {
                stat->m_bDirty = false;

#if !__NO_OUTPUT
                char buf[64];
                rlDebug3("rlProfileStatsFlushStatEvent: id=%d, value=%s",
                         evt->m_StatId,
                         stat->m_StatValue.ToString(buf, sizeof(buf)));
#endif
            }
        }

        break;
    }
}

//static const rlLeaderboardService LBSVC = RL_LBSERVICE_DEFAULT;
static const rlLeaderboardService LBSVC = RL_LEADERBOARD_SERVICE_ROS;

void
App::RecordSwing(Gamer* gamer, const SwingMsg& swing)
{
    ++gamer->m_NumSwings;
    if(HIT_NONE != swing.m_HitResult)
    {
        ++gamer->m_HitResultCounts[swing.m_HitResult];
    }

    //Test rlMatch's synthesis of team ids.
    //const int teamId = gamer->m_TeamId;
    // const int teamId = -1;

	//snSession* session = m_LiveMgr->GetSession();

    rlLeaderboardUpdate update;

    update.LeaderboardId = Leaderboard_Hits::LEADERBOARD_ID;
    update.InputValues[0].InputId = Leaderboard_Hits::FIELD_ID_HITS;

    update.InputValues[0].Value.Int64Val = gamer->SwingCount();
    update.NumValues = 1;
    update.LeaderboardType = RL_LEADERBOARD_TYPE_PLAYER;
    update.LeaderboardService = LBSVC;
    update.GroupHandle.Clear();

    Displayf("Writing:%d swings for gamer:\"%s\"", 
             gamer->SwingCount(), 
             gamer->GetName());

    // TODO JRM:
	// AssertVerify(session->WriteStats(gamer->GetGamerId(), teamId, update));

    if(rlClan::HasPrimaryClan(gamer->GetLocalIndex()))
    {
        Displayf("Writing:%d swings for gamer:\"%s\" to clan leaderboard", 
                 gamer->SwingCount(), 
                 gamer->GetName());

        const rlClanDesc& clanDesc = rlClan::GetPrimaryClan(gamer->GetLocalIndex());
        update.LeaderboardType = RL_LEADERBOARD_TYPE_GROUP;
        update.LeaderboardService = RL_LEADERBOARD_SERVICE_ROS;
        update.GroupHandle.Init(clanDesc.m_Id);
		// TODO JRM:
        // AssertVerify(session->WriteStats(gamer->GetGamerId(), teamId, update));
    }

    update.InputValues[0].InputId = Leaderboard_SwingsByStrikes::FIELD_ID_SWINGS;
    update.InputValues[0].Value.Int64Val = gamer->SwingCount();
    update.InputValues[1].InputId = Leaderboard_SwingsByStrikes::FIELD_ID_SWING_STRIKE;
    update.InputValues[1].Value.Int64Val = gamer->StrikeCount();
    update.InputValues[2].InputId = Leaderboard_SwingsByStrikes::FIELD_ID_SWING_SINGLES;
    update.InputValues[2].Value.Int64Val = gamer->HitResultCount(HIT_SINGLE);
    update.InputValues[3].InputId = Leaderboard_SwingsByStrikes::FIELD_ID_SWING_DOUBLES;
    update.InputValues[3].Value.Int64Val = gamer->HitResultCount(HIT_DOUBLE);
    update.InputValues[4].InputId = Leaderboard_SwingsByStrikes::FIELD_ID_SWING_TRIPLES;
    update.InputValues[4].Value.Int64Val = gamer->HitResultCount(HIT_TRIPLE);
    update.InputValues[5].InputId = Leaderboard_SwingsByStrikes::FIELD_ID_SWING_HOMERUN;
    update.InputValues[5].Value.Int64Val = gamer->HitResultCount(HIT_HOME_RUN);

    update.NumValues = 6;
    update.LeaderboardId = Leaderboard_SwingsByStrikes::LEADERBOARD_ID;
    update.LeaderboardType = RL_LEADERBOARD_TYPE_PLAYER;
    update.LeaderboardService = LBSVC;
    update.GroupHandle.Clear();

    Displayf("Writing batting stats for gamer:\"%s\"", gamer->GetName());

	// TODO JRM:
    // AssertVerify(session->WriteStats(gamer->GetGamerId(), teamId, update));

    if(rlClan::HasPrimaryClan(gamer->GetLocalIndex()))
    {
        Displayf("Writing batting stats for gamer:\"%s\" to clan leaderboard", 
                    gamer->GetName());

        const rlClanDesc& clanDesc = rlClan::GetPrimaryClan(gamer->GetLocalIndex());
        update.LeaderboardType = RL_LEADERBOARD_TYPE_GROUP;
        update.LeaderboardService = RL_LEADERBOARD_SERVICE_ROS;
        update.GroupHandle.Init(clanDesc.m_Id);

		// TODO JRM
        // AssertVerify(session->WriteStats(gamer->GetGamerId(), teamId, update));
    }

    //Report score for everyone, regardless of arbitration.
    //Report score.
    // TODO JRM: 
	// AssertVerify(session->WriteScore(gamer->GetGamerId(), teamId, gamer->SwingCount()));
}

static const int s_GroupType = 0;

bool
App::WriteLeaderboard2()
{
    static const int LEADERBOARD_ID = Leaderboard_SwingsByStrikes2::LEADERBOARD_ID;

    m_LbReadId = LEADERBOARD_ID;

    bool success = false;

    if(m_Gamer)
    {
        rlLeaderboardInputValue inputValues[6];
        rlLeaderboard2Update update;
        
        update.Init(inputValues, 6);

        int groupIdx = 0;
        if(strlen(m_UiLeaderboard2WriteMenu.GetCategory1()))
        {
            safecpy(update.m_GroupSelector.m_Group[groupIdx].m_Category, m_UiLeaderboard2WriteMenu.GetCategory1());
            formatf(update.m_GroupSelector.m_Group[groupIdx].m_Id, "%d", m_UiLeaderboard2WriteMenu.GetGroup1Id());
            ++groupIdx;
        }

        if(m_UiLeaderboard2WriteMenu.GetCategory2())
        {
            safecpy(update.m_GroupSelector.m_Group[groupIdx].m_Category, m_UiLeaderboard2WriteMenu.GetCategory2());
            formatf(update.m_GroupSelector.m_Group[groupIdx].m_Id, "%d", m_UiLeaderboard2WriteMenu.GetGroup2Id());
            ++groupIdx;
        }

        update.m_GroupSelector.m_NumGroups = groupIdx;
        update.m_LeaderboardId = LEADERBOARD_ID;
        update.m_ClanId = rlClan::GetPrimaryClan(m_Gamer->GetLocalIndex()).m_Id;

        update.m_InputValues[0].InputId = Leaderboard_SwingsByStrikes::FIELD_ID_SWINGS;
        update.m_InputValues[0].Value.Int64Val = m_UiLeaderboard2WriteMenu.GetSwings();
        update.m_InputValues[1].InputId = Leaderboard_SwingsByStrikes::FIELD_ID_SWING_STRIKE;
        update.m_InputValues[1].Value.Int64Val = m_UiLeaderboard2WriteMenu.GetStrikes();
        update.m_InputValues[2].InputId = Leaderboard_SwingsByStrikes::FIELD_ID_SWING_SINGLES;
        update.m_InputValues[2].Value.Int64Val = m_UiLeaderboard2WriteMenu.GetSingles();
        update.m_InputValues[3].InputId = Leaderboard_SwingsByStrikes::FIELD_ID_SWING_DOUBLES;
        update.m_InputValues[3].Value.Int64Val = m_UiLeaderboard2WriteMenu.GetDoubles();
        update.m_InputValues[4].InputId = Leaderboard_SwingsByStrikes::FIELD_ID_SWING_TRIPLES;
        update.m_InputValues[4].Value.Int64Val = m_UiLeaderboard2WriteMenu.GetTriples();
        update.m_InputValues[5].InputId = Leaderboard_SwingsByStrikes::FIELD_ID_SWING_HOMERUN;
        update.m_InputValues[5].Value.Int64Val = m_UiLeaderboard2WriteMenu.GetHomers();

        const rlLeaderboard2Update* updates[] = {&update};
        success = rlLeaderboard2PlayerStats::Write(m_Gamer->GetLocalIndex(), updates, 1, &m_MyStatus);

        if(success)
        {
            this->PushSegue(SEGUE_WRITING_LEADERBOARD2);
        }
    }

    return success;
}

bool
App::ReadLeaderboard2(const int readType)
{
    static const int LEADERBOARD_ID = Leaderboard_SwingsByStrikes2::LEADERBOARD_ID;
    static const int PIVOT_SCORE    = 20;

    m_LbReadId = LEADERBOARD_ID;
    m_NumLbRowsReturned = 0;
    m_TotalLbRows = 0;
    //This can't be deallocated while operation is in progress.
    static rlLeaderboardColumnInfo columns[RL_MAX_LEADERBOARD_COLUMNS];

    bool success = false;

    if(m_Gamer)
    {
        //Get the column info.
        LeaderboardInfo lbInfo;
        LeaderboardInfo::GetLeaderboardInfo(LEADERBOARD_ID, &lbInfo);
        for(int i = 0; i < (int)lbInfo.NumColumns; ++i)
        {
            //Shift the requested columns around a bit to test that they're
            //properly retrieved.
            const int idx = (i+3)%lbInfo.NumColumns;

            columns[idx].Id = lbInfo.ColumnIds[i];
            columns[idx].Type = RL_STAT_TYPE_FROM_ID(lbInfo.ColumnInputIds[i]);
        }

        //Initialize the rows.
        for(int i = 0; i < COUNTOF(m_Lb2Rows); ++i)
        {
            m_Lb2Rows[i].Init(m_StatValueHeap[i], COUNTOF(m_StatValueHeap[i]));
            m_Lb2RowPtrs[i] = &m_Lb2Rows[i];
        }

        rlLeaderboard2GroupSelector groupSelector;
        int groupIdx = 0;
        if(strlen(m_UiLeaderboard2ReadMenu.GetCategory1()))
        {
            safecpy(groupSelector.m_Group[groupIdx].m_Category, m_UiLeaderboard2ReadMenu.GetCategory1());
            const int groupId =  m_UiLeaderboard2ReadMenu.GetGroup1Id();
            if(groupId >= 0)
            {
                formatf(groupSelector.m_Group[groupIdx].m_Id, "%d", groupId);
            }
            else
            {
                //Wildcard
                groupSelector.m_Group[groupIdx].m_Id[0] = '\0';
            }
            ++groupIdx;
        }

        if(m_UiLeaderboard2ReadMenu.GetCategory2())
        {
            safecpy(groupSelector.m_Group[groupIdx].m_Category, m_UiLeaderboard2ReadMenu.GetCategory2());
            const int groupId =  m_UiLeaderboard2ReadMenu.GetGroup2Id();
            if(groupId >= 0)
            {
                formatf(groupSelector.m_Group[groupIdx].m_Id, "%d", groupId);
            }
            else
            {
                //Wildcard
                groupSelector.m_Group[groupIdx].m_Id[0] = '\0';
            }
            ++groupIdx;
        }

        groupSelector.m_NumGroups = groupIdx;

        const rlLeaderboard2Type lbType = m_UiLeaderboard2ReadMenu.GetLeaderboardType();
        const rlClanId clanId = rlClan::GetPrimaryClan(m_Gamer->GetLocalIndex()).m_Id;

        if(RL_LEADERBOARD2_TYPE_PLAYER == lbType)
        {
            switch(readType)
            {
            case UiLeaderboard2ReadMenu::ACTION_READ_BY_RANK:
                success = rlLeaderboard2PlayerStats::ReadByRank(m_Gamer->GetLocalIndex(),
                                                    LEADERBOARD_ID,
                                                    groupSelector,
                                                    1,
                                                    COUNTOF(m_Lb2RowPtrs),
                                                    columns,
                                                    lbInfo.NumColumns,
                                                    m_Lb2RowPtrs,
                                                    &m_NumLbRowsReturned,
                                                    &m_TotalLbRows,
                                                    &m_MyStatus);
                break;
            case UiLeaderboard2ReadMenu::ACTION_READ_BY_ROW:
                success = rlLeaderboard2PlayerStats::ReadByGamer(m_Gamer->GetLocalIndex(),
                                                    LEADERBOARD_ID,
                                                    groupSelector,
                                                    &m_Gamer->GetGamerHandle(),
                                                    1,
                                                    columns,
                                                    lbInfo.NumColumns,
                                                    m_Lb2RowPtrs,
                                                    &m_NumLbRowsReturned,
                                                    &m_TotalLbRows,
                                                    &m_MyStatus);
                break;
            case UiLeaderboard2ReadMenu::ACTION_READ_BY_SCORE:
                success = rlLeaderboard2PlayerStats::ReadByScoreInt(m_Gamer->GetLocalIndex(),
                                                        LEADERBOARD_ID,
                                                        groupSelector,
                                                        PIVOT_SCORE,
                                                        COUNTOF(m_Lb2RowPtrs),
                                                        columns,
                                                        lbInfo.NumColumns,
                                                        m_Lb2RowPtrs,
                                                        &m_NumLbRowsReturned,
                                                        &m_TotalLbRows,
                                                        &m_MyStatus);
                break;
            case UiLeaderboard2ReadMenu::ACTION_READ_NEAR:
                success = rlLeaderboard2PlayerStats::ReadByRadius(m_Gamer->GetLocalIndex(),
                                                        LEADERBOARD_ID,
                                                        groupSelector,
                                                        m_Gamer->GetGamerHandle(),
                                                        COUNTOF(m_Lb2RowPtrs),
                                                        columns,
                                                        lbInfo.NumColumns,
                                                        m_Lb2RowPtrs,
                                                        &m_NumLbRowsReturned,
                                                        &m_TotalLbRows,
                                                        &m_MyStatus);
                break;
            }
        }
        else if(RL_LEADERBOARD2_TYPE_CLAN_MEMBER == lbType)
        {
            switch(readType)
            {
            case UiLeaderboard2ReadMenu::ACTION_READ_BY_RANK:
                success =
                    rlLeaderboard2ClanMemberStats::ReadByRank(m_Gamer->GetLocalIndex(),
                                                    LEADERBOARD_ID,
                                                    groupSelector,
                                                    clanId,
                                                    1,
                                                    COUNTOF(m_Lb2RowPtrs),
                                                    columns,
                                                    lbInfo.NumColumns,
                                                    m_Lb2RowPtrs,
                                                    &m_NumLbRowsReturned,
                                                    &m_TotalLbRows,
                                                    &m_MyStatus);
                break;
            case UiLeaderboard2ReadMenu::ACTION_READ_BY_ROW:
                success =
                    rlLeaderboard2ClanMemberStats::ReadByMember(m_Gamer->GetLocalIndex(),
                                                    LEADERBOARD_ID,
                                                    groupSelector,
                                                    clanId,
                                                    &m_Gamer->GetGamerHandle(),
                                                    1,
                                                    columns,
                                                    lbInfo.NumColumns,
                                                    m_Lb2RowPtrs,
                                                    &m_NumLbRowsReturned,
                                                    &m_TotalLbRows,
                                                    &m_MyStatus);
                break;
            case UiLeaderboard2ReadMenu::ACTION_READ_BY_SCORE:
                success =
                    rlLeaderboard2ClanMemberStats::ReadByScoreInt(m_Gamer->GetLocalIndex(),
                                                    LEADERBOARD_ID,
                                                    groupSelector,
                                                    clanId,
                                                    PIVOT_SCORE,
                                                    COUNTOF(m_Lb2RowPtrs),
                                                    columns,
                                                    lbInfo.NumColumns,
                                                    m_Lb2RowPtrs,
                                                    &m_NumLbRowsReturned,
                                                    &m_TotalLbRows,
                                                    &m_MyStatus);
                break;
            case UiLeaderboard2ReadMenu::ACTION_READ_NEAR:
                success =
                    rlLeaderboard2ClanMemberStats::ReadByRadius(m_Gamer->GetLocalIndex(),
                                                    LEADERBOARD_ID,
                                                    groupSelector,
                                                    clanId,
                                                    m_Gamer->GetGamerHandle(),
                                                    COUNTOF(m_Lb2RowPtrs),
                                                    columns,
                                                    lbInfo.NumColumns,
                                                    m_Lb2RowPtrs,
                                                    &m_NumLbRowsReturned,
                                                    &m_TotalLbRows,
                                                    &m_MyStatus);
                break;
            }
        }
        else if(RL_LEADERBOARD2_TYPE_CLAN == lbType)
        {
            switch(readType)
            {
            case UiLeaderboard2ReadMenu::ACTION_READ_BY_RANK:
                success = rlLeaderboard2ClanStats::ReadByRank(m_Gamer->GetLocalIndex(),
                                                    LEADERBOARD_ID,
                                                    groupSelector,
                                                    1,
                                                    COUNTOF(m_Lb2RowPtrs),
                                                    columns,
                                                    lbInfo.NumColumns,
                                                    m_Lb2RowPtrs,
                                                    &m_NumLbRowsReturned,
                                                    &m_TotalLbRows,
                                                    &m_MyStatus);
                break;
            case UiLeaderboard2ReadMenu::ACTION_READ_BY_ROW:
                success = rlLeaderboard2ClanStats::ReadByClan(m_Gamer->GetLocalIndex(),
                                                    LEADERBOARD_ID,
                                                    groupSelector,
                                                    &clanId,
                                                    1,
                                                    columns,
                                                    lbInfo.NumColumns,
                                                    m_Lb2RowPtrs,
                                                    &m_NumLbRowsReturned,
                                                    &m_TotalLbRows,
                                                    &m_MyStatus);
                break;
            case UiLeaderboard2ReadMenu::ACTION_READ_BY_SCORE:
                success = rlLeaderboard2ClanStats::ReadByScoreInt(m_Gamer->GetLocalIndex(),
                                                    LEADERBOARD_ID,
                                                    groupSelector,
                                                    PIVOT_SCORE,
                                                    COUNTOF(m_Lb2RowPtrs),
                                                    columns,
                                                    lbInfo.NumColumns,
                                                    m_Lb2RowPtrs,
                                                    &m_NumLbRowsReturned,
                                                    &m_TotalLbRows,
                                                    &m_MyStatus);
                break;
            case UiLeaderboard2ReadMenu::ACTION_READ_NEAR:
                success = rlLeaderboard2ClanStats::ReadByRadius(m_Gamer->GetLocalIndex(),
                                                    LEADERBOARD_ID,
                                                    groupSelector,
                                                    clanId,
                                                    COUNTOF(m_Lb2RowPtrs),
                                                    columns,
                                                    lbInfo.NumColumns,
                                                    m_Lb2RowPtrs,
                                                    &m_NumLbRowsReturned,
                                                    &m_TotalLbRows,
                                                    &m_MyStatus);
                break;
            }
        }
        else if(RL_LEADERBOARD2_TYPE_GROUP == lbType)
        {
            switch(readType)
            {
            case UiLeaderboard2ReadMenu::ACTION_READ_BY_RANK:
                success = rlLeaderboard2GroupStats::ReadByRank(m_Gamer->GetLocalIndex(),
                                                    LEADERBOARD_ID,
                                                    groupSelector,
                                                    1,
                                                    COUNTOF(m_Lb2RowPtrs),
                                                    columns,
                                                    lbInfo.NumColumns,
                                                    m_Lb2RowPtrs,
                                                    &m_NumLbRowsReturned,
                                                    &m_TotalLbRows,
                                                    &m_MyStatus);
                break;
            case UiLeaderboard2ReadMenu::ACTION_READ_BY_ROW:
                success = rlLeaderboard2GroupStats::ReadByGroup(m_Gamer->GetLocalIndex(),
                                                    LEADERBOARD_ID,
                                                    &groupSelector,
                                                    1,
                                                    columns,
                                                    lbInfo.NumColumns,
                                                    m_Lb2RowPtrs,
                                                    &m_NumLbRowsReturned,
                                                    &m_TotalLbRows,
                                                    &m_MyStatus);
                break;
            case UiLeaderboard2ReadMenu::ACTION_READ_NEAR:
                success = rlLeaderboard2GroupStats::ReadByRadius(m_Gamer->GetLocalIndex(),
                                                    LEADERBOARD_ID,
                                                    groupSelector,
                                                    COUNTOF(m_Lb2RowPtrs),
                                                    columns,
                                                    lbInfo.NumColumns,
                                                    m_Lb2RowPtrs,
                                                    &m_NumLbRowsReturned,
                                                    &m_TotalLbRows,
                                                    &m_MyStatus);
                break;
            }
        }

        if(success)
        {
            this->PushSegue(SEGUE_READING_LEADERBOARD2);
        }
    }

    return success;
}

bool
App::GetLeaderboardSize(const unsigned lbId)
{
    m_TotalLbRows = 0;

    bool success = false;

    if(InState(STATE_LEADERBOARDS_PLAYER_LB_MENU))
    {
        success =
            rlPlayerStats::GetLeaderboardSize(lbId,
                                            LBSVC,
                                            &m_TotalLbRows,
                                            &m_MyStatus);
    }
    else if(InState(STATE_LEADERBOARDS_GROUP_LB_MENU))
    {
        success =
            rlGroupStats::GetLeaderboardSize(lbId,
                                            s_GroupType,
                                            &m_TotalLbRows,
                                            &m_MyStatus);
    }
    else if(InState(STATE_LEADERBOARDS_GROUP_MEMBER_LB_MENU))
    {
        if(!rlClan::HasPrimaryClan(m_Gamer->GetLocalIndex()))
        {
            this->PushResult("Player has no primary clan");
        }
        else
        {
            const s64 clanId = rlClan::GetPrimaryClan(m_Gamer->GetLocalIndex()).m_Id;
            success =
                rlGroupMemberStats::GetLeaderboardSize(lbId,
                                                        s_GroupType,
                                                        rlLeaderboardGroupHandle(clanId),
                                                        &m_TotalLbRows,
                                                        &m_MyStatus);
        }
    }

    if(success)
    {
        this->PushSegue(SEGUE_GETTING_LEADERBOARD_SIZE);
        return true;
    }
    else if(!InState(STATE_GENERIC_RESULT))
    {
        this->PushResult("Error reading leaderboard size");
    }

    return false;
}

bool 
App::ReadLeaderboardByRank(const unsigned lbId)
{
    bool success = false;

    LeaderboardInfo lbInfo;
    LeaderboardInfo::GetLeaderboardInfo(lbId, &lbInfo);
    rlLeaderboardColumnInfo columns[RL_MAX_LEADERBOARD_COLUMNS];
    for(int i = 0; i < (int)lbInfo.NumColumns; ++i)
    {
        //Shift the requested columns around a bit to test that they're
        //properly retrieved.
        const int idx = (i+3)%lbInfo.NumColumns;

        columns[idx].Id = lbInfo.ColumnIds[i];
        columns[idx].Type = RL_STAT_TYPE_FROM_ID(lbInfo.ColumnInputIds[i]);
    }

    if(InState(STATE_LEADERBOARDS_PLAYER_LB_MENU))
    {
        success = rlPlayerStats::ReadByRank(lbId,
                                            LBSVC,
                                            1,
                                            10,
                                            m_LbRows,
                                            &m_NumLbRowsReturned,
                                            &m_TotalLbRows,
                                            columns,
                                            lbInfo.NumColumns,
                                            &m_MyStatus);
    }
    else if(InState(STATE_LEADERBOARDS_GROUP_LB_MENU))
    {
        success = rlGroupStats::ReadByRank(lbId,
                                            s_GroupType,
                                            1,
                                            10,
                                            m_LbRows,
                                            &m_NumLbRowsReturned,
                                            &m_TotalLbRows,
                                            columns,
                                            lbInfo.NumColumns,
                                            &m_MyStatus);
    }
    else if(InState(STATE_LEADERBOARDS_GROUP_MEMBER_LB_MENU))
    {
        const s64 clanId = rlClan::GetPrimaryClan(m_Gamer->GetLocalIndex()).m_Id;
        if(!rlClan::HasPrimaryClan(m_Gamer->GetLocalIndex()))
        {
            this->PushResult("Player has no primary clan");
        }
        else
        {
            success = rlGroupMemberStats::ReadByRank(lbId,
                                                    s_GroupType,
                                                    rlLeaderboardGroupHandle(clanId),
                                                    1,
                                                    10,
                                                    m_LbRows,
                                                    &m_NumLbRowsReturned,
                                                    &m_TotalLbRows,
                                                    columns,
                                                    lbInfo.NumColumns,
                                                    &m_MyStatus);
        }
    }

    if(success)
    {
        this->PushSegue(SEGUE_READING_STATS);
    }
    else if(!InState(STATE_GENERIC_RESULT))
    {
        this->PushResult("Error reading stats");
    }

    return success;
}

bool 
App::ReadLeaderboardByRow(const unsigned lbId)
{
    bool success = false;

    LeaderboardInfo lbInfo;
    LeaderboardInfo::GetLeaderboardInfo(lbId, &lbInfo);
    rlLeaderboardColumnInfo columns[RL_MAX_LEADERBOARD_COLUMNS];
    for(int i = 0; i < (int)lbInfo.NumColumns; ++i)
    {
        //Shift the requested columns around a bit to test that they're
        //properly retrieved.
        const int idx = (i+3)%lbInfo.NumColumns;

        columns[idx].Id = lbInfo.ColumnIds[i];
        columns[idx].Type = RL_STAT_TYPE_FROM_ID(lbInfo.ColumnInputIds[i]);
    }

    if(InState(STATE_LEADERBOARDS_PLAYER_LB_MENU))
    {
        success =
            AssertVerify(m_Gamer)
            && rlPlayerStats::ReadByGamers(lbId,
                                        LBSVC,
                                        &m_Gamer->m_GamerInfo.GetGamerHandle(),
                                        1,
                                        m_LbRows,
                                        &m_NumLbRowsReturned,
                                        &m_TotalLbRows,
                                        columns,
                                        lbInfo.NumColumns,
                                        &m_MyStatus);
    }
    else if(InState(STATE_LEADERBOARDS_GROUP_LB_MENU))
    {
        if(!rlClan::HasPrimaryClan(m_Gamer->GetLocalIndex()))
        {
            this->PushResult("Player has no primary clan");
        }
        else
        {
            const s64 clanId = rlClan::GetPrimaryClan(m_Gamer->GetLocalIndex()).m_Id;
            const rlLeaderboardGroupHandle groupHandle(clanId);

            success = rlGroupStats::ReadByGroups(lbId,
                                                s_GroupType,
                                                &groupHandle,
                                                1,
                                                m_LbRows,
                                                &m_NumLbRowsReturned,
                                                &m_TotalLbRows,
                                                columns,
                                                lbInfo.NumColumns,
                                                &m_MyStatus);
        }
    }
    else if(InState(STATE_LEADERBOARDS_GROUP_MEMBER_LB_MENU) && AssertVerify(m_Gamer))
    {
        if(!rlClan::HasPrimaryClan(m_Gamer->GetLocalIndex()))
        {
            this->PushResult("Player has no primary clan");
        }
        else
        {
            const s64 clanId = rlClan::GetPrimaryClan(m_Gamer->GetLocalIndex()).m_Id;
            const rlLeaderboardGroupHandle groupHandle(clanId);

            success = rlGroupMemberStats::ReadByMembers(lbId,
                                                        s_GroupType,
                                                        groupHandle,
                                                        &m_Gamer->GetGamerHandle(),
                                                        1,
                                                        m_LbRows,
                                                        &m_NumLbRowsReturned,
                                                        &m_TotalLbRows,
                                                        columns,
                                                        lbInfo.NumColumns,
                                                        &m_MyStatus);
        }
    }

    if(success)
    {
        this->PushSegue(SEGUE_READING_STATS);
    }
    else if(!InState(STATE_GENERIC_RESULT))
    {
        this->PushResult("Error reading stats");
    }

    return success;
}

bool 
App::ReadLeaderboardMemberByGroups(const unsigned lbId)
{
    bool success = false;

    LeaderboardInfo lbInfo;
    LeaderboardInfo::GetLeaderboardInfo(lbId, &lbInfo);
    rlLeaderboardColumnInfo columns[RL_MAX_LEADERBOARD_COLUMNS];
    for(int i = 0; i < (int)lbInfo.NumColumns; ++i)
    {
        //Shift the requested columns around a bit to test that they're
        //properly retrieved.
        const int idx = (i+3)%lbInfo.NumColumns;

        columns[idx].Id = lbInfo.ColumnIds[i];
        columns[idx].Type = RL_STAT_TYPE_FROM_ID(lbInfo.ColumnInputIds[i]);
    }

    if(!InState(STATE_LEADERBOARDS_GROUP_MEMBER_LB_MENU))
    {
        this->PushResult("Operation not allowed except for group member leaderboards");
    }
    else if(AssertVerify(m_Gamer))
    {
        if(!rlClan::HasPrimaryClan(m_Gamer->GetLocalIndex()))
        {
            this->PushResult("Player has no primary clan");
        }
        else
        {
#if 1
            const rlLeaderboardGroupHandle groupHandle(rlClan::GetPrimaryClan(m_Gamer->GetLocalIndex()).m_Id);

            success = rlGroupMemberStats::ReadMemberByGroups(lbId,
                                                            s_GroupType,
                                                            m_Gamer->m_GamerInfo.GetGamerHandle(),
                                                            &groupHandle,
                                                            1,
                                                            m_LbRows,
                                                            &m_NumLbRowsReturned,
                                                            columns,
                                                            lbInfo.NumColumns,
                                                            &m_MyStatus);
#else
            rlLeaderboardGroupHandle groupHandles[2];
            groupHandles[0].Init((s64)6285);
            groupHandles[1].Init((s64)6286);

            success = rlGroupMemberStats::ReadMemberByGroups(lbId,
                                                            s_GroupType,
                                                            m_Gamer->m_GamerInfo.GetGamerHandle(),
                                                            groupHandles,
                                                            2,
                                                            m_LbRows,
                                                            &m_NumLbRowsReturned,
                                                            columns,
                                                            lbInfo.NumColumns,
                                                            &m_MyStatus);
#endif

        }
    }

    if(success)
    {
        this->PushSegue(SEGUE_READING_STATS);
    }
    else if(!InState(STATE_GENERIC_RESULT))
    {
        this->PushResult("Error reading stats");
    }

    return success;
}

bool 
App::ReadLeaderboardByRadius(const unsigned lbId)
{
    bool success = false;

    const unsigned READ_RADIUS = 10;

    LeaderboardInfo lbInfo;
    LeaderboardInfo::GetLeaderboardInfo(lbId, &lbInfo);
    rlLeaderboardColumnInfo columns[RL_MAX_LEADERBOARD_COLUMNS];
    for(int i = 0; i < (int)lbInfo.NumColumns; ++i)
    {
        //Shift the requested columns around a bit to test that they're
        //properly retrieved.
        const int idx = (i+3)%lbInfo.NumColumns;

        columns[idx].Id = lbInfo.ColumnIds[i];
        columns[idx].Type = RL_STAT_TYPE_FROM_ID(lbInfo.ColumnInputIds[i]);
    }

    if(InState(STATE_LEADERBOARDS_PLAYER_LB_MENU))
    {
        success =
            AssertVerify(m_Gamer)
            && rlPlayerStats::ReadByRadius(lbId,
                                            LBSVC,
                                            m_Gamer->m_GamerInfo.GetGamerHandle(),
                                            READ_RADIUS,
                                            m_LbRows,
                                            &m_NumLbRowsReturned,
                                            &m_TotalLbRows,
                                            columns,
                                            lbInfo.NumColumns,
                                            &m_MyStatus);
    }
    else if(InState(STATE_LEADERBOARDS_GROUP_LB_MENU))
    {
        if(!rlClan::HasPrimaryClan(m_Gamer->GetLocalIndex()))
        {
            this->PushResult("Player has no primary clan");
        }
        else
        {
            const s64 clanId =rlClan::GetPrimaryClan(m_Gamer->GetLocalIndex()).m_Id;

            success = rlGroupStats::ReadByRadius(lbId,
                                                s_GroupType,
                                                rlLeaderboardGroupHandle(clanId),
                                                READ_RADIUS,
                                                m_LbRows,
                                                &m_NumLbRowsReturned,
                                                &m_TotalLbRows,
                                                columns,
                                                lbInfo.NumColumns,
                                                &m_MyStatus);
        }
    }
    else if(InState(STATE_LEADERBOARDS_GROUP_MEMBER_LB_MENU))
    {
        this->PushResult("Can't call ReadByRadius for group member leaderboards");
        return success;
    }

    if(success)
    {
        this->PushSegue(SEGUE_READING_STATS);
    }
    else if(!InState(STATE_GENERIC_RESULT))
    {
        this->PushResult("Error reading stats");
    }

    return success;
}

bool 
App::InboxGetMessages()
{
    bool success = false;

    if(m_Gamer)
    {
        success = rlInbox::GetMessages(m_Gamer->GetLocalIndex(),
                                        m_UiInboxMenu.GetOffset(),
                                        m_UiInboxMenu.GetCount(),
                                        (const char**)NULL, //tagsCsv
                                        0,                  //numTags
                                        &m_InboxMessageIter,
                                        &m_MyStatus);
    }

    if(success)
    {
        this->PushSegue(SEGUE_READING_INBOX_MESSAGES);
    }
    else if(!InState(STATE_GENERIC_RESULT))
    {
        this->PushResult("Error reading inbox messages");
    }

    return success;
}

bool 
App::InboxGetNewMessages()
{
    bool success = false;

    if(m_Gamer)
    {
        success = rlInbox::GetUnreadMessages(m_Gamer->GetLocalIndex(),
                                            m_UiInboxMenu.GetOffset(),
                                            m_UiInboxMenu.GetCount(),
                                            (const char**)NULL, //tagsCsv
                                            0,                  //numTags
                                            &m_InboxMessageIter,
                                            &m_MyStatus);
    }

    if(success)
    {
        this->PushSegue(SEGUE_READING_INBOX_MESSAGES);
    }
    else if(!InState(STATE_GENERIC_RESULT))
    {
        this->PushResult("Error reading new inbox messages");
    }

    return success;
}

bool 
App::InboxPostMessage()
{
    bool success = false;

    if(m_Gamer)
    {
        success = rlInbox::PostMessage(m_Gamer->GetLocalIndex(),
                                        &m_Gamer->GetGamerHandle(),
                                        1,
                                        "Fart Bag",
                                        NULL,
                                        0,
                                        0);
    }

    if(success)
    {
        this->PushResult("Posted inbox message");
    }
    else if(!InState(STATE_GENERIC_RESULT))
    {
        this->PushResult("Error posting inbox message");
    }

    return success;
}

bool 
App::InboxPostMessageToFriends()
{
    bool success = false;

    if(m_Gamer)
    {
		success = rlInbox::PostMessageToManyFriends(m_Gamer->GetLocalIndex(), "Mis amigos - yo te amo!!!", NULL, 0, 300); // 5m ttl
    }

    if(success)
    {
        this->PushResult("Posted inbox message to friends");
    }
    else if(!InState(STATE_GENERIC_RESULT))
    {
        this->PushResult("Error posting inbox message to friends");
    }

    return success;
}

bool 
App::InboxPostMessageToCrew()
{
    bool success = false;

    if(m_Gamer)
    {
        const rlClanDesc& clanDesc = rlClan::GetPrimaryClan(m_Gamer->GetLocalIndex());
        if(clanDesc.IsValid())
        {
            success = rlInbox::PostMessageToClan(m_Gamer->GetLocalIndex(),
                                                clanDesc.m_Id,
                                                "I love love love my crew!!!",
                                                NULL,
                                                0,
                                                0);
        }
    }

    if(success)
    {
        this->PushResult("Posted inbox message to crew");
    }
    else if(!InState(STATE_GENERIC_RESULT))
    {
        this->PushResult("Error posting inbox message to crew");
    }

    return success;
}

void
App::ClearGamerStats()
{
    Gamer* gamers[RL_MAX_GAMERS_PER_SESSION];
    unsigned numGamers = m_PeerMgr.GetGamers(gamers, COUNTOF(gamers));

    for(unsigned i = 0; i < numGamers; i++)
    {
        gamers[i]->ClearStats();
    }
}

bool 
App::RetrieveThenPushClanState(State clanState)
{
    switch (clanState)
    {
    case STATE_CLANS_MAIN_MENU:
        m_NumMyClanMembership = COUNTOF(m_MyClanMembership);
        if(rlClan::GetMine(
            m_Gamer->m_GamerInfo.GetLocalIndex(),
            m_MyClanMembership,
            m_NumMyClanMembership,
            &m_NumMyClanMembership, NULL,
            &m_MyStatus))
        {
            this->PushSegue(SEGUE_CLAN_RETRIEVING_MINE);
            return true;
        }
        else
        {
            this->PushResult("Failed to retrieve my clans");
        }
        break;
    case STATE_CLANS_ALL_MENU:
        m_NumAllClans = COUNTOF(m_AllClans);
        if(rlClan::GetAll(
            m_Gamer->m_GamerInfo.GetLocalIndex(),
            0, 
            m_AllClans,
            m_NumAllClans,
            &m_NumAllClans, NULL,
            &m_MyStatus))
        {
            this->PushSegue(SEGUE_CLAN_RETRIEVING_ALL);
            return true;
        }
        else
        {
            this->PushResult("Failed to retrieve all clans");
        }
        break;    
    case STATE_CLANS_MEMBERS_MENU:
        m_NumClanMembers = COUNTOF(m_ClanMembers);
        if(rlClan::GetMembersByClanId(
            m_Gamer->m_GamerInfo.GetLocalIndex(),
            0, 
            m_ActiveClan->m_Id,
            m_ClanMembers,
            m_NumClanMembers,
            &m_NumClanMembers, NULL,
            &m_MyStatus))
        {
            this->PushSegue(SEGUE_CLAN_RETRIEVING_MEMBERS);
            return true;
        }
        else
        {
            this->PushResult("Failed to retrieve members");
        }
        break;
    case STATE_CLANS_RANKS_MENU:
        m_NumClanRanks = COUNTOF(m_ClanRanks);
        if(rlClan::GetRanks(
            m_Gamer->m_GamerInfo.GetLocalIndex(),
            0, 
            m_ActiveClan->m_Id,
            m_ClanRanks,
            m_NumClanRanks,
            &m_NumClanRanks, NULL,
            &m_MyStatus))
        {
            this->PushSegue(SEGUE_CLAN_RETRIEVING_RANKS);
            return true;
        }
        else
        {
            this->PushResult("Failed to retrieve clan ranks");
        }
        break;
    case STATE_CLANS_SENT_REQUESTS_MENU:
        m_NumClanSentRequests = COUNTOF(m_ClanRequests);
        if(rlClan::GetSentJoinRequests(
            m_Gamer->m_GamerInfo.GetLocalIndex(),
            0,
            m_ClanSentRequests,
            m_NumClanSentRequests,
            &m_NumClanSentRequests, NULL,
            &m_MyStatus))
        {
            this->PushSegue(SEGUE_CLAN_RETRIEVING_SENT_REQUESTS);
            return true;
        }
        else
        {
            this->PushResult("Failed to retrieve clan requests");
        }
        break;
    case STATE_CLANS_REQUESTS_MENU:
        m_NumClanRequests = COUNTOF(m_ClanRequests);
        if(rlClan::GetRecievedJoinRequests(
            m_Gamer->m_GamerInfo.GetLocalIndex(),
            0,
            m_ActiveClan->m_Id,
            m_ClanRequests,
            m_NumClanRequests,
            &m_NumClanRequests, NULL,
            &m_MyStatus))
        {
            this->PushSegue(SEGUE_CLAN_RETRIEVING_REQUESTS);
            return true;
        }
        else
        {
            this->PushResult("Failed to retrieve clan requests");
        }
        break;
    case STATE_CLANS_INVITES_MENU:
        m_NumClanInvites = COUNTOF(m_ClanInvites);
        if(rlClan::GetInvites(
            m_Gamer->m_GamerInfo.GetLocalIndex(),
            0,
            m_ClanInvites,
            m_NumClanInvites,
            &m_NumClanInvites, NULL,
            &m_MyStatus))
        {
            this->PushSegue(SEGUE_CLAN_RETRIEVING_INVITES);
            return true;
        }
        else
        {
            this->PushResult("Failed to retrieve clan invites");
        }
        break;
    case STATE_CLANS_INVITE_FRIEND_MENU:
        m_NumMyClanMembership = COUNTOF(m_MyClanMembership);
        if(rlClan::GetMine(
            m_Gamer->m_GamerInfo.GetLocalIndex(),
            m_MyClanMembership,
            m_NumMyClanMembership,
            &m_NumMyClanMembership,
            NULL,
            &m_MyStatus))
        {
            this->PushSegue(SEGUE_CLAN_RETRIEVING_MINE_TO_INVITE);
            return true;
        }
        else
        {
            this->PushResult("Failed to retrieve my clans");
        }
        break;
    case STATE_CLANS_WALL_MESSAGE_MENU:
        m_NumClanMessages = COUNTOF(m_ClanMessags);
        if(rlClan::GetWallMessages(
            m_Gamer->m_GamerInfo.GetLocalIndex(),
            0, 
            m_ActiveClan->m_Id,
            m_ClanMessags,
            m_NumClanMessages,
            &m_NumClanMessages, NULL,
            &m_MyStatus))
        {
            this->PushSegue(SEGUE_CLAN_RETRIEVING_WALL_MESSAGES);
            return true;
        }
        else
        {
            this->PushResult("Failed to retrieve clan messages");
        }
        break;
    case STATE_CLANS_GET_METADATA:
        m_NumClanEnums = COUNTOF(m_ClanEnums);
        if(AssertVerify(rlClan::GetMetadataForClan(
            m_Gamer->m_GamerInfo.GetLocalIndex(),
            0, 
            m_ActiveClan->m_Id,
            m_ClanEnums,
            m_NumClanEnums,
            &m_NumClanEnums, NULL,
            &m_MyStatus)))
        {
            this->PushSegue(SEGUE_CLAN_RETRIEVING_METADATA_ENUMS);
            return true;
        }    
        break;
    case STATE_CLANS_GET_FEUD_STAT:
        m_NumClanFeuds = COUNTOF(m_ClanFeuds);
        if(AssertVerify(rlClan::GetFeudStats(
            m_Gamer->m_GamerInfo.GetLocalIndex(),
            m_ActiveClan->m_Id, RL_CLAN_FEUD_SINCE_ALL_TIME,
            m_NumClanFeuds,
            m_ClanFeuds,
            &m_NumClanFeuds,
            &m_MyStatus)))
        {
            this->PushSegue(SEGUE_CLAN_RETRIEVING_FEUD_STAT);
            return true;
        }    
        break;
    default:
        break;
    }
    return false;
}

bool
App::ReadProfileStatsByGamer()
{
    if (!AssertVerify(m_Gamer)) return false;
    if (!m_NumLocalProfileStats) return false;

    //Randomly select stat IDs to read.
    for (int i = 0; i < NELEM(m_ReadProfileStatsIds); ++i)
    {
        int statId = m_LocalProfileStats[rand() % m_NumLocalProfileStats].m_StatId;
        m_ReadProfileStatsIds[i] = statId;
    }

    //Setup results object.
    if (!AssertVerify(m_ReadProfileStatsResult.Reset(m_ReadProfileStatsIds, 
                                                     NELEM(m_ReadProfileStatsIds),
                                                     NELEM(m_ReadProfileStatsRecords),
                                                     m_ReadProfileStatsRecords,
                                                     sizeof(rlProfileStatsFixedRecord<MAX_PROFILE_STATS_TRACKED>))))
    {
        return false;
    }

    //Setup gamer handles to fetch stats for.
    rlGamerInfo gamerInfo;
    if (!AssertVerify(rlPresence::GetGamerInfo(m_Gamer->GetLocalIndex(), &gamerInfo)))
    {
        return false;
    }
    rlGamerHandle gamerHandles[READ_STATS_MAX_RECORDS];
    gamerHandles[0] = gamerInfo.GetGamerHandle();

    //Perform request.
    return rlProfileStats::ReadStatsByGamer(m_Gamer->GetLocalIndex(), 
                                            gamerHandles, 
                                            NELEM(gamerHandles), 
                                            &m_ReadProfileStatsResult, 
                                            &m_MyStatus);
}


bool
App::ResetProfileStatsByGroup()
{  
	if (!AssertVerify(m_Gamer)) return false;

	//Perform request.
	return rlProfileStats::ResetStatsByGroup(m_Gamer->GetLocalIndex(), 
                                             m_uiProfileStatsResetByGroupMenu.GetGroupId(),
		                                     &m_MyStatus);
}

void
App::ShowSigninUi()
{
    g_SystemUi.ShowSigninUi();
}

bool
App::IsSystemUiShowing() const
{
    return g_SystemUi.IsUiShowing();
}

