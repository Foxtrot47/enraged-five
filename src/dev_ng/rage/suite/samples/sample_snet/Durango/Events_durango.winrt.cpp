// 
// events_durango.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#if RSG_DURANGO

// game includes
#include "diag/seh.h"
#include "events_durango.h"
#include "Events.man.h"
#include "rline/durango/rlxbl_interface.h"
#include "rline/durango/private/rlxblcommon.h"
#include "rline/rldiag.h"
#include "string/unicode.h"

#include <xdk.h>

#pragma comment(lib, "EtwPlus.lib")

using namespace Platform;
using namespace Windows::Foundation;

using namespace rage;

Functor2Ret<ULONG, PCWSTR,LPCGUID> events_durango::m_fAchievementEvents[XB1_ACHIEVEMENT_EVENTS] =
{
	MakeFunctorRet(&EventWriteACH01_Event),
	MakeFunctorRet(&EventWriteACH02_Event),
	MakeFunctorRet(&EventWriteACH03_Event),
	MakeFunctorRet(&EventWriteACH04_Event),
	MakeFunctorRet(&EventWriteACH05_Event),
	MakeFunctorRet(&EventWriteACH06_Event),
	MakeFunctorRet(&EventWriteACH07_Event),
	MakeFunctorRet(&EventWriteACH08_Event),
	MakeFunctorRet(&EventWriteACH09_Event),
	MakeFunctorRet(&EventWriteACH10_Event),
	MakeFunctorRet(&EventWriteACH11_Event),
	MakeFunctorRet(&EventWriteACH12_Event),
	MakeFunctorRet(&events_durango::NoOp),						// aggregate achievement
	MakeFunctorRet(&EventWriteACH14_Event),
	MakeFunctorRet(&EventWriteACH15_Event),
	MakeFunctorRet(&EventWriteACH16_Event),
	MakeFunctorRet(&EventWriteACH17_Event),
	MakeFunctorRet(&EventWriteACH18_Event),
	MakeFunctorRet(&EventWriteACH19_Event),
	MakeFunctorRet(&EventWriteACH20_Event),
	MakeFunctorRet(&EventWriteACH21_Event),
	MakeFunctorRet(&EventWriteACH22_Event),
	MakeFunctorRet(&EventWriteACH23_Event),
	MakeFunctorRet(&EventWriteACH24_Event),
	MakeFunctorRet(&EventWriteACH25_Event),
	MakeFunctorRet(&EventWriteACH26_Event),
	MakeFunctorRet(&events_durango::NoOp),						// aggregate achievement
	MakeFunctorRet(&EventWriteACH28_Event),
	MakeFunctorRet(&EventWriteACH29_Event),
	MakeFunctorRet(&EventWriteACH30_Event),
	MakeFunctorRet(&EventWriteACH31_Event),
	MakeFunctorRet(&EventWriteACH32_Event),
	MakeFunctorRet(&EventWriteACH33_Event),
	MakeFunctorRet(&events_durango::NoOp),						// aggregate achievement
	MakeFunctorRet(&events_durango::NoOp),						// aggregate achievement
	MakeFunctorRet(&events_durango::NoOp),						// aggregate achievement
	MakeFunctorRet(&EventWriteACH37_Event),
	MakeFunctorRet(&events_durango::NoOp),						// aggregate achievement
	MakeFunctorRet(&events_durango::NoOp),						// aggregate achievement
	MakeFunctorRet(&EventWriteACH40_Event),
	MakeFunctorRet(&EventWriteACH41_Event),
	MakeFunctorRet(&EventWriteACH42_Event),
	MakeFunctorRet(&events_durango::NoOp),						// aggregate achievement
	MakeFunctorRet(&events_durango::NoOp),						// aggregate achievement
	MakeFunctorRet(&EventWriteACH45_Event),
	MakeFunctorRet(&EventWriteACH46_Event),
	MakeFunctorRet(&EventWriteACH47_Event),
	MakeFunctorRet(&EventWriteACH48_Event),
	MakeFunctorRet(&EventWriteACH49_Event)
};

bool events_durango::sm_Initialized = false;
events_durango::EventRegistrationWorker events_durango::sm_Worker;

bool events_durango::Init()
{
	g_rlXbl.GetAchivementManager()->SetAchievementEventFunc(MakeFunctorRet(&WriteAchievementEvent));
	bool success = false;

	rtry
	{
		rverify(!sm_Initialized, catchall,);
		rverify(sm_Worker.Start(1), catchall, rlError("Error starting event registration worker"));
		
		sm_Worker.Wakeup();

		rlDebug("Started event registration worker");

		success = sm_Initialized = true;
	}
	rcatchall
	{
	}

	return success;
}

bool events_durango::Shutdown()
{
	ULONG result = EventUnregisterRKTR_39F35803();
	return result == ERROR_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////
// rlAchievement::Worker
///////////////////////////////////////////////////////////////////////////////

events_durango::EventRegistrationWorker::EventRegistrationWorker()
{
}

bool events_durango::EventRegistrationWorker::Start(const int cpuAffinity)
{
	const bool success = this->rlWorker::Start("[GTAV] Event Registration", sysIpcMinThreadStackSize, cpuAffinity, true);
	return success;
}

bool events_durango::EventRegistrationWorker::Stop()
{
	const bool success = this->rlWorker::Stop();
	return success;
}

void events_durango::EventRegistrationWorker::Perform()
{
	result = EventRegisterRKTR_39F35803();
}

bool events_durango::WriteAchievementEvent(int achId, int localGamerIndex, int )
{
	rtry
	{
		rcheck(sm_Initialized, catchall, rlError("events_durango not initialized"));
		rcheck(achId < XB1_ACHIEVEMENT_EVENTS, catchall, rlError("Invalid achievement ID passed to WriteAchievementEvent"));

		PCWSTR playerId = GetUserId(localGamerIndex);
		GUID playerSessionId;
		ZeroMemory( &playerSessionId, sizeof(GUID) );

		ULONG response = m_fAchievementEvents[achId](playerId, &playerSessionId);
		if (response == ERROR_SUCCESS)
		{
			return true;
		}
	}
	rcatchall
	{
		
	}

	return false;
}

bool events_durango::WriteTestEvent(int localGamerIndex, const char * statAttr)
{
	rtry
	{
		rcheck(sm_Initialized, catchall, rlError("events_durango not initialized"));

		PCWSTR playerId = GetUserId(localGamerIndex);
		GUID playerSessionId;
		ZeroMemory( &playerSessionId, sizeof(GUID) );

		wchar_t wStatAttr[256];
		Utf8ToWide((char16*)wStatAttr, statAttr, 256);

		ULONG response = EventWriteCT_SP_MISSIONS(playerId, &playerSessionId, wStatAttr);
		if (response == ERROR_SUCCESS)
		{
			return true;
		}
	}
	rcatchall
	{

	}

	return false;
}

PCWSTR events_durango::GetUserId(int localGamerIndex)
{
	u64 xuid = g_rlXbl.GetPresenceManager()->GetXboxUserId(localGamerIndex);
	String^ userId = rlXblCommon::XuidToWinRTString(xuid);

	return userId->Data();
}

#endif  //RSG_DURANGO
