// 
// events_durango.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EVENTS_DURANGO_H
#define EVENTS_DURANGO_H

#if RSG_DURANGO

#pragma warning(push)
#pragma warning(disable:4273)
#include "atl/functor.h"
#include "rline/rlworker.h"
#include "vectormath/vec3v.h"
#include <wtypes.h>
#pragma warning(pop)

using namespace rage;

class events_durango
{
public:

	static bool Init();
	static bool Shutdown();

	static bool WriteAchievementEvent(int achId, int localGamerIndex, int achProgress);
	static bool WriteTestEvent(int localGamerIndex, const char * statAttr);

private:

	static PCWSTR GetUserId(int localGamerIndex);
	static ULONG NoOp(PCWSTR,LPCGUID) { return 0; }

	struct EventRegistrationWorker : public rlWorker
	{
		EventRegistrationWorker();
		bool Start(const int cpuAffinity);
		bool Stop();

	private:

		//Make Start() un-callable
		bool Start(const char* /*name*/,
			const unsigned /*stackSize*/,
			const int /*cpuAffinity*/)
		{
			FastAssert(false);
			return false;
		}

		virtual void Perform();

		ULONG result;
	};

	static const int XB1_ACHIEVEMENT_EVENTS = 59;
	static Functor2Ret<ULONG, PCWSTR,LPCGUID> m_fAchievementEvents[XB1_ACHIEVEMENT_EVENTS];
	static EventRegistrationWorker sm_Worker;
	static bool sm_Initialized;
};

#endif  //RSG_DURANGO

#endif  //EVENTS_DURANGO_H
