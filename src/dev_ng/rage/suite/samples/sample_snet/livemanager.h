// 
// sample_snet/livemanager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef LIVEMANAGER_H
#define LIVEMANAGER_H

#include "atl/inmap.h"
#include "atl/inlist.h"
#include "avchat/voicechat.h"
#include "rline/rlsessionfinder.h"
#include "snet/session.h"

#include "diag/output.h"

#if !__NO_OUTPUT

#define lmDebug1(a)     SpewDebug(1, ::rage::snDebugLevel(), "lmgr") a
#define lmDebug2(a)     SpewDebug(2, ::rage::snDebugLevel(), "lmgr") a
#define lmDebug3(a)     SpewDebug(3, ::rage::snDebugLevel(), "lmgr") a
#define lmWarning(a)    SpewWarning("lmgr") a
#define lmError(a)      SpewError("lmgr") a

#else

#define lmDebug1(a)
#define lmDebug2(a)
#define lmDebug3(a)
#define lmWarning(a)
#define lmError(a)

#endif  //__NO_OUTPUT

namespace rage
{

class netConnectionManager;
class Gamer;

class LiveManager
{
public:

    enum
    {
        SESSION_CHANNEL_ID          = 6,
        GAME_CHANNEL_ID             = 7,
        VOICE_CHANNEL_ID            = 8,
    };

    enum
    {
        //CPU on which rline worker threads will run.
        RLINE_CPU_AFFINITY      = 1,
    };

    LiveManager();

    void Init(sysMemAllocator* allocator,
                netConnectionManager* cxnMgr,
                const snSessionOwner& owner,
                void* voiceChatDevice);

    void Shutdown();

	// PURPOSE
	//	Initializes the Session
	void InitSession();

    //PURPOSE
    //  Returns a pointer to the session.
    snSession* GetSession();
    const snSession* GetSession() const;

    //PURPOSE
    //  Returns a pointer to the voice object.
    VoiceChat& GetVoice() { return m_VoiceChat; }
    const VoiceChat& GetVoice() const { return m_VoiceChat; }

    //PURPOSE
    //  Returns a pointer to the connection manager.
    netConnectionManager* GetCxnMgr();

    void ResetSession();

    //PURPOSE
    //  Hosts a session.
    //PARAMS
    //  localGamer      - Local gamer that owns the session.
    //  netMode         - ONLINE or LAN.
    //  maxPubSlots     - Maximum number of public slots.
    //  maxPrivSlots    - Maximum number of private slots.
    //  attrs           - Matching attributes.
    //  createFlags     - Flags from rlSession::CreateFlags enum.
    //  status          - Optional status object that can be polled for
    //                    completion.
    //NOTES
    //  The attrs parameter should be an instance of rlSessionAttrs
    //  created using the template constructor with the parameter
    //  being an instance of a Match schema generated from a .xlast
    //  file (see sample_snet.schema.h).
    bool HostSession(Gamer* localGamer,
                    const rlNetworkMode netMode,
                    const int maxPubSlots,
                    const int maxPrivSlots,
                    const rlMatchingAttributes& attrs,
                    const unsigned createFlags,
                    netStatus* status);

    //PURPOSE
    //  Joins a session.
    //PARAMS
    //  localGamer      - Local gamer that owns the local instance of
    //                    the session.
    //  netMode         - ONLINE or LAN.
    //  gameType        - RANKED or STANDARD.
    //  slotType        - PRIVATE or PUBLIC.
    //  createFlags     - Flags from rlSession::CreateFlags enum.  These
    //                    should match the flags used by the host to create
    //                    the session.
    //  status          - Optional status object that can be polled for
    //                    completion.
    //NOTES
    //  This is an asynchronous operation.  Poll the status object
    //  to determine when the operation completes.
    bool JoinSession(Gamer* localGamer,
                    const rlSessionInfo& sessionInfo,
                    const rlNetworkMode netMode,
                    const rlSlotType slotType,
                    const unsigned createFlags,
                    netStatus* status);

    //PURPOSE
    //  Finds sessions.
    //PARAMS
    //  netMode         - NETMODE_ONLINE or NETMODE_LAN.
    //  requester       - Local gamer requesting the query.
    //  numLocalGamers  - Number of local gamers that want to join a session.
    //  filter          - Contains conditions used to filter potential matches.
    //  results         - Upon completion will contain results of the search.
    //  maxResults      - Maximum number of results.
    //  numResults      - Upon completion will contain the number of results.
    //  status          - Optional status object that can be polled for
    //                    completion.
    //NOTES
    //  This is an asynchronous operation.  Poll the status object
    //  to determine when the operation completes.
    bool FindSessions(const rlNetworkMode netMode,
                        const Gamer* requester,
                        const int numLocalGamers,
                        const rlMatchingFilter& filter,
                        rlSessionDetail* results,
                        const unsigned maxResults,
						const unsigned numResultsBeforeEarlyOut,
                        unsigned* numResults,
                        netStatus* status);

    //PURPOSE
    //  Call this on a regular interval.
    void Update(const unsigned curTime);

private:

    void OnSessionEvent(snSession* session,
                        const snEvent* evt);

    snSession::Delegate m_SessionDlgt;

    enum State
    {
        STATE_NONE,
        STATE_CREATING_SESSION,
        STATE_JOINING_SESSION,
        STATE_FINDING_SESSIONS,
    };

    sysMemAllocator* m_Allocator;
    netConnectionManager* m_CxnMgr;
	snSessionOwner m_Owner;

    VoiceChat m_VoiceChat;

    snSession m_Session;

    netStatus m_MyStatus;
    netStatus* m_PendingStatus;

    union
    {
        int m_State;
        State m_StateCode;
    };

    bool m_Initialized  : 1;
};

}   //namespace rage

#endif  //LIVEMANAGER_H
