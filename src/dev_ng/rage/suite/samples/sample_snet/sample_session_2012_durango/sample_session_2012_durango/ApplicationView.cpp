//
// Application.cpp -
//

#include "pch.h"
#include "ApplicationView.h"
#include <..\..\..\..\rage\base\src\input\keyboard_durango_winrt_side.h>
#include <..\..\..\..\rage\base\src\grcore\device_d3d11_winrt_side.h>

#include <string.h>
#include <stdio.h>


extern bool CommonMain_Prologue_DurangoWrapper(int argc,char **argv);
extern bool CommonMain_OneLoopIteration_DurangoWrapper();
extern void CommonMain_Epilogue_DurangoWrapper();

using namespace Windows::Foundation;
using namespace Windows::ApplicationModel;
using namespace Windows::ApplicationModel::Core;
using namespace Windows::ApplicationModel::Activation;
using namespace Windows::UI::Core;
using namespace Windows::Xbox::Networking;

ApplicationView::ApplicationView()
{
}


// Called by the system.  Perform application initialization here, 
// hooking application wide events, etc.
void ApplicationView::Initialize(CoreApplicationView^ applicationView)
{
    applicationView->Activated += ref new TypedEventHandler<CoreApplicationView^, IActivatedEventArgs^>(this, &ApplicationView::OnActivated);
}



// Called when we are provided a window.
void ApplicationView::SetWindow(CoreWindow^ window)
{
	m_Window = window;
	device_d3d11_winrt_side::SetCoreWindow(reinterpret_cast< IUnknown* >(CoreWindow::GetForCurrentThread()));
}


void ApplicationView::Load(Platform::String^ entryPoint)
{
	/*
    m_game = ref new Game();
    m_game->Initialize(CoreWindow::GetForCurrentThread());
	*/
}

Windows::Xbox::Networking::SecureDeviceAssociationTemplate^ g_SecureDeviceAssociationTemplate;

// Called by the system after initialization is complete.  This
// implements the traditional game loop
void ApplicationView::Run()
{
	char *pArgv[3];
	// DURANGO TODO:- Ip address etc.
	pArgv[0] = "Durango";
	pArgv[1] = "@x:\args.txt";
	pArgv[2] = NULL;

// 	try
// 	{
// 		// TODO: NS - not sure where to put this
// 		g_SecureDeviceAssociationTemplate = SecureDeviceAssociationTemplate::GetTemplateByName(
// 			"PeerTraffic"  // the name of a SecureDeviceAssociationTemplate in networkmanifest.xml
// 			);
// 	}
// 	catch (Platform::Exception^ e)
// 	{
// 	}

	CommonMain_Prologue_DurangoWrapper(2, pArgv);

	for (;;)
    {
        // ProcessEvents will throw if the process is exiting, allowing us to
        // break out of the loop.  This will be cleaned up when we get proper
        // process lifetime management online in a future release.
        CoreWindow::GetForCurrentThread()->Dispatcher->ProcessEvents(CoreProcessEventsOption::ProcessAllIfPresent);

        //m_game->Tick();
		CommonMain_OneLoopIteration_DurangoWrapper();
    }

	CommonMain_Epilogue_DurangoWrapper();
}


void ApplicationView::Uninitialize()
{}


// Called when the application is activated.  For now, there is just one activation
// kind - Launch.
void ApplicationView::OnActivated(CoreApplicationView^ applicationView, IActivatedEventArgs^ args)
{
    CoreWindow::GetForCurrentThread()->Activate();
}


// Implements a IFrameworkView factory.
IFrameworkView^ ApplicationViewSource::CreateView()
{
    return ref new ApplicationView();
}


// Application entry point
[Platform::MTAThread]
int main(Platform::Array<Platform::String^>^ args)
{
    auto applicationViewSource = ref new ApplicationViewSource();

    CoreApplication::Run(applicationViewSource);

    return 0;
}
