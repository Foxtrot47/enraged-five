// 
// sample_snet/oobtransactiontask.h
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef OOBTRANSACTIONTASK_H
#define OOBTRANSACTIONTASK_H

#include "net/connectionmanager.h"
#include "net/status.h"
#include "net/transaction.h"
#include "net/tunneler.h"
#include "rline/rlsessioninfo.h"
#include "rline/rltask.h"

using namespace rage;

//PURPOSE
//  This convenience class automates tunneling to a peer and sending an OOB request.
//  It was developed as a proof-of-concept for RDR2 to use during session roaming,
//  where the host of the current session must send a request to another session to
//  reserve slots.  It can be used for any request-response transaction, though.
template<class T_REQUEST, class T_RESPONSE>
class OobTransactionTask : public rlTask<netConnectionManager>
{
public:
    OobTransactionTask()
    : m_State(STATE_INVALID)
    , m_TxId(NET_INVALID_TRANSACTION_ID)
    , m_RequestCount(0)
    {
        m_RespHandler.Bind(this, &OobTransactionTask::OnResponse);
    }

    virtual ~OobTransactionTask() {}

    virtual const char* GetTaskName() const { return "OobTransactionTask"; }

    bool Configure(netConnectionManager* ctx,  
                   const rlSessionInfo* sessionInfo,
                   const rlPeerAddress& peerAddr,
                   const rlNetworkMode netMode,
                   const unsigned channelId,
                   const T_REQUEST* request,
                   T_RESPONSE* response,
                   netStatus* status)
    {
        Assert(peerAddr.IsValid());

        rlDebug2("OobTransactionTask(%s, %s): Configuring", 
                 m_Request->GetMsgName(),
                 m_Response->GetMsgName());

        bool success = false;

        m_ChannelId = channelId;
        m_Request = request;
        m_Response = response;
        m_NetAddr.Clear();
        m_Transactor.Init(ctx);
        m_RequestCount = 0;

        if(AssertVerify(ctx->OpenTunnel(sessionInfo.GetToken(),
                                        peerAddr,
                                        (RL_NETMODE_LAN == netMode) ? NET_TUNNELTYPE_LAN : NET_TUNNELTYPE_ONLINE,
                                        &m_TunnelRqst,
                                        &m_MyStatus)))
        {
            m_State = STATE_WAIT_FOR_TUNNEL;
            success = true;
        }
        else
        {
            rlError(("Failed to open tunnel with peer; possible cause is cookie already in use"));
        }

        return success && rlTask<netConnectionManager>::Configure(ctx, status);      
    }

    virtual void Start()
    {
        rlDebug2("OobTransactionTask(%s, %s): Starting", 
                 m_Request->GetMsgName(),
                 m_Response->GetMsgName());

        rlTask<netConnectionManager>::Start();
    }

    virtual void Finish(const bool success, const int resultCode = 0)
    {
        rlDebug2("OobTransactionTask(%s, %s): %s", 
                 m_Request->GetMsgName(),
                 m_Response->GetMsgName(),
                 success ? "succeeded" : "failed");

        if(!success)
        {
            m_Ctx->CancelTunnelRequest(&m_TunnelRqst);
        }

        if(m_RespHandler.Pending())
        {
            m_RespHandler.Cancel();
        }

        m_Transactor.Shutdown();

        rlTask<netConnectionManager>::Finish(success, resultCode);
    }

    virtual void Update(const unsigned timeStep)
    {
        rlTask<netConnectionManager>::Update(timeStep);

        m_Transactor.Update(timeStep);

        if(IsActive())
        {
            switch(m_State)
            {
            case STATE_WAIT_FOR_TUNNEL:
                if(m_MyStatus.Succeeded())
                {
                    rlDebug2("OobTransactionTask(%s, %s): Tunnel opened", 
                             m_Request->GetMsgName(),
                             m_Response->GetMsgName());

                    m_NetAddr = m_TunnelRqst.GetNetAddress();
                    m_State = STATE_SEND_REQUEST;
                }
                else if(!m_MyStatus.Pending())
                {
                    rlError("Failed to negotiate NAT");

                    //Generate fake IPs in the range 127.0.0.4 to 127.255.255.255
                    static unsigned s_NextFakeIp = 0;
                    const unsigned fakeIp = 0x7F000010 | (s_NextFakeIp & 0x00FFFFFF);

                    ++s_NextFakeIp;

                    m_NetAddr.Init(fakeIp, 0xFFFF);
                    m_State = STATE_SEND_REQUEST;
                }
                break;

            case STATE_SEND_REQUEST:
                if(!m_RespHandler.Pending())
                {
                    if(m_RequestCount < MAX_ATTEMPTS)
                    {
                        if(!this->SendRequest())
                        {
                            this->Finish(false);
                        }
                    }
                    else
                    {
                        rlDebug2("OobTransactionTask(%s, %s): Max attempts failed", 
                                 m_Request->GetMsgName(),
                                 m_Response->GetMsgName());
                        this->Finish(false);
                    }
                }
            }
        }
    }

private:
    bool SendRequest()
    {
        bool success = false;

        if(m_RequestCount)
        {
            success = m_Transactor.ResendRequest(m_NetAddr,
                                                 m_ChannelId,
                                                 m_TxId,
                                                 *m_Request,
                                                 QUERY_TIMEOUT,
                                                 &m_RespHandler);
        }
        else
        {
            success = m_Transactor.SendRequest(m_NetAddr,
                                               m_ChannelId,
                                               &m_TxId,
                                               *m_Request,
                                               QUERY_TIMEOUT,
                                               &m_RespHandler);
        }

        ++m_RequestCount;

        if(success)
        {
            rlDebug2("OobTransactionTask(%s, %s): Sent request (%d of %d tries)", 
                     m_Request->GetMsgName(),
                     m_Response->GetMsgName(),
                     m_RequestCount,
                     MAX_ATTEMPTS);
        }

        return success;
    }

    void OnResponse(netTransactor* /*transactor*/,
                    netResponseHandler* /*handler*/,
                    const netResponse* resp)
    {
        rlDebug2("OobTransactionTask(%s, %s): Received response", 
                 m_Request->GetMsgName(),
                 m_Response->GetMsgName());

        if(resp->Complete() && resp->Answered())
        {
            if(!AssertVerify(m_Response->Import(resp->m_Data, resp->m_SizeofData)))
            {
                rlError("Failed to import %s", m_Response->GetMsgName());
                Finish(false);
            }
            else
            {
                rlDebug2("OobTransactionTask(%s, %s): Successfully imported response", 
                         m_Request->GetMsgName(),
                         m_Response->GetMsgName());
                Finish(true);
            }
        }
        else
        {
            rlDebug2("%s %d.%d.%d.%d:%d timed out or failed",
                     m_Request->GetMsgName(),
                     NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr));
            Finish(false);
        }
    }

    enum
    {
        QUERY_TIMEOUT   = 3000,
        MAX_ATTEMPTS    = 10
    };

    enum State
    {
        STATE_INVALID = -1,
        STATE_WAIT_FOR_TUNNEL,
        STATE_SEND_REQUEST,

        NUM_STATES
    };

    int m_State;

    const T_REQUEST* m_Request;
    T_RESPONSE* m_Response;

    netTunnelRequest m_TunnelRqst;
    netAddress m_NetAddr;
    unsigned m_ChannelId;

    netTransactor m_Transactor; 
    unsigned m_RequestCount;
    unsigned m_TxId;
    netResponseHandler m_RespHandler;

    netStatus m_MyStatus;
};

#endif
