// 
// sample_snet/peermanager.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PEERMANAGER_H
#define PEERMANAGER_H

// rage core
#include "atl/inmap.h"
#include "atl/inlist.h"

// rage net
#include "net/connectionmanager.h"
#include "rline/rlpresence.h"
#include "snet/session.h"

namespace rage
{

class netConnectionManager;

enum SwingResult2
{
    SWING2_STRIKE,
    SWING2_HIT,

    SWING2_NUM_RESULTS
};

enum HitResult
{
    HIT_NONE    = -1,
    HIT_SINGLE,
    HIT_DOUBLE,
    HIT_TRIPLE,
    HIT_HOME_RUN,
    HIT_FLY_OUT,

    HIT_NUM_RESULTS
};

class Gamer
{
public:
    Gamer()
    {
        this->Clear();
    }

    void Reset(const rlGamerInfo& gamerInfo)
    {
        this->Clear();
        m_GamerInfo = gamerInfo;
    }

    void Clear()
    {
        m_GamerInfo.Clear();
        m_TeamId = -1;
        this->ClearStats();
    }

    void ClearStats()
    {
        m_NumSwings = 0;
        for(int i = 0; i < HIT_NUM_RESULTS; ++i)
        {
            m_HitResultCounts[i] = 0;
        }
    }

    bool IsValid() const
    {
        return m_GamerInfo.IsValid();
    }

    const rlGamerInfo& GetGamerInfo() const
    {
        return m_GamerInfo;
    }

    int GetLocalIndex() const
    {
        return m_GamerInfo.GetLocalIndex();
    }

    const char* GetName() const
    {
        return m_GamerInfo.GetName();
    }

    const rlGamerId& GetGamerId() const
    {
        return m_GamerInfo.GetGamerId();
    }

    const rlGamerHandle& GetGamerHandle() const
    {
        return m_GamerInfo.GetGamerHandle();
    }

    const rlPeerInfo& GetPeerInfo() const
    {
        return m_GamerInfo.GetPeerInfo();
    }

    bool IsLocal() const
    {
        return m_GamerInfo.IsLocal();
    }

    bool IsRemote() const
    {
        return m_GamerInfo.IsRemote();
    }

    bool IsSignedIn() const
    {
        return m_GamerInfo.IsSignedIn();
    }

    bool IsOnline() const
    {
        return m_GamerInfo.IsOnline();
    }

    int SwingCount() const
    {
        return m_NumSwings;
    }

    int HitCount() const
    {
        int numHits = 0;
        for(int i = 0; i < HIT_NUM_RESULTS; ++i)
        {
            numHits += m_HitResultCounts[i];
        }

        return numHits;
    }

    int StrikeCount() const
    {
        return SwingCount() - HitCount();
    }

    int HitResultCount(HitResult hitResult) const
    {
        return m_HitResultCounts[hitResult];
    }

    bool ImportData(const void* data,
                    const unsigned sizeofData)
    {
        datBitBuffer bb;
        bb.SetReadOnlyBytes(data, sizeofData);

        bool success = AssertVerify(bb.ReadInt(m_TeamId, 32))
                        && AssertVerify(bb.ReadInt(m_NumSwings, 32));
        if(success)
        {
            for(int i = 0; i < HIT_NUM_RESULTS; ++i)
            {
                if(!AssertVerify(bb.ReadInt(m_HitResultCounts[i], 32)))
                {
                    success = false;
                    break;
                }
            }
        }

        return success;
    }

    bool ExportData(void* data,
                    const unsigned maxSizeofData,
                    unsigned* sizeofData) const
    {
        datBitBuffer bb;
        bb.SetReadWriteBytes(data, maxSizeofData);

        bool success = AssertVerify(bb.WriteInt(m_TeamId, 32))
                        && AssertVerify(bb.WriteInt(m_NumSwings, 32));
        if(success)
        {
            for(int i = 0; i < HIT_NUM_RESULTS; ++i)
            {
                if(!AssertVerify(bb.WriteInt(m_HitResultCounts[i], 32)))
                {
                    success = false;
                    break;
                }
            }
        }

        if(success && sizeofData){*sizeofData = bb.GetNumBytesWritten();}

        return success;
    }

    rlGamerInfo m_GamerInfo;
    //Host-assigned team id.
    int m_TeamId;
    //Statistics we accumulate during a session.
    int m_NumSwings;
    int m_HitResultCounts[HIT_NUM_RESULTS];

    inlist_node<Gamer> m_ListLink;
    inmap_node<rlGamerId, Gamer> m_ByIdLink;
};

class Peer
{
    friend class PeerManager;

public:

    int GetCxnId() const
    {
        return m_CxnId;
    }

	EndpointId GetEndpointId() const
	{
		return m_EndpointId;
	}

    bool IsLocal() const
    {
        return m_PeerInfo.IsLocal();
    }

	bool HasGamer(const rlGamerInfo& info) const
	{
		for(int i = 0; i < m_NumGamers; ++i)
		{
			if(m_Gamers[i]->GetGamerHandle() == info.GetGamerHandle())
			{
				return true;
			}
		}
		return false;
	}

private:

    Peer()
        : m_NumGamers(0)
    {
        this->Clear();
    }

    void Clear()
    {
		m_CxnId = -1;
        m_EndpointId = NET_INVALID_ENDPOINT_ID;

        while(m_NumGamers)
        {
            this->RemoveGamer(m_Gamers[0]->m_GamerInfo.GetGamerId());
        }
    }

    bool AddGamer(Gamer* gamer)
    {
		// check if gamer exists for peer, from the temporary pool
		for (int i = 0; i < m_NumGamers; i++)
		{
			if (m_Gamers[i]->GetGamerId() == gamer->GetGamerId())
			{
				// update gamer
				m_Gamers[i] = gamer;
				return true;
			}
		}

        bool success = false;
        if(AssertVerify(m_NumGamers < RL_MAX_LOCAL_GAMERS))
        {
            m_Gamers[m_NumGamers] = gamer;
            ++m_NumGamers;
            success = true;
        }

        return success;
    }

    bool RemoveGamer(const rlGamerId& gamerId)
    {
        bool success = false;
        for(int i = 0; i < m_NumGamers; ++i)
        {
            if(gamerId == m_Gamers[i]->m_GamerInfo.GetGamerId())
            {
                m_Gamers[i] = m_Gamers[m_NumGamers-1];
                --m_NumGamers;
                success = true;
                break;
            }
        }

        return success;
    }

	int m_CxnId;
    EndpointId m_EndpointId;

    rlPeerInfo m_PeerInfo;
    Gamer* m_Gamers[RL_MAX_LOCAL_GAMERS];
    int m_NumGamers;

    inmap_node<u64, Peer> m_ByIdLink;
    inmap_node<EndpointId, Peer> m_ByAddrLink;
    inlist_node<Peer> m_ListLink;
};

class PeerManager
{
public:

    enum
    {
        MAX_PEERS           = RL_MAX_GAMERS_PER_SESSION,
        MAX_REMOTE_GAMERS   = RL_MAX_GAMERS_PER_SESSION,
    };

    PeerManager();
    ~PeerManager();

    void Init(netConnectionManager* cxnMgr,
            const unsigned channelId);

    void Shutdown();

    Gamer* AddRemoteGamer(const rlGamerInfo& gamerInfo,
							const EndpointId endpointId,
                            const unsigned sizeofData,
                            const void* data);

    bool AddLocalGamer(Gamer* gamer,
                        const unsigned sizeofData,
                        const void* data);

    bool RemoveGamer(const rlGamerId& gamerId);

    unsigned GetGamerCount() const;

    unsigned GetPeerCount() const;

    //PURPOSE
    //  Retrieves the list of gamers.
    //PARAMS
    //  gamers      - Array of gamer pointers to be populated.
    //  maxGamers   - Maximum size of the array.
    //RETURNS
    //  Number of existing gamers.  This may be more or less than maxGamers
    //  If less than maxGamers then the gamers array will contain all gamers.
    //  If more than maxGamers then the gamers array will contain no gamers.
    //  The caller should re-call GetGamers() with an array of appropriate size.
    unsigned GetGamers(Gamer** gamers, const unsigned maxGamers);

    //PURPOSE
    //  Returns the gamer associated with the gamer id.
    Gamer* GetGamerById(const rlGamerId& gamerId);

    //PURPOSE
    //  Retrieves the list of peers.
    //PARAMS
    //  peers       - Array of peer pointers to be populated.
    //  maxPeers    - Maximum size of the array.
    //RETURNS
    //  Number of existing peers.  This may be more or less than maxPeers
    //  If less than maxPeers then the peers array will contain all peers.
    //  If more than maxPeers then the peers array will contain no peers.
    //  The caller should re-call GetPeers() with an array of appropriate size.
    unsigned GetPeers(Peer** peers, const unsigned maxPeers);

    Gamer* GetGamer(const rlGamerId& gamerId);
    const Gamer* GetGamer(const rlGamerId& gamerId) const;

    Peer* GetPeer(const EndpointId endpointId);
    const Peer* GetPeer(const EndpointId endpointId) const;

    Peer* GetPeer(const u64 peerId);
    const Peer* GetPeer(const u64 peerId) const;


	Gamer* AddTemporaryPlayer(const rlGamerInfo &gamerInfo, const EndpointId endpointId, unsigned sizeofData, const void* data);
	void RemoveTemporaryPlayer(const rlGamerId& gamerId);
	Gamer* GetTempPlayer(const rlGamerId& id);
	const Gamer* GetTempPlayer(const rlGamerId& gamerId) const;

private:

    Peer* AddPeer(const rlPeerInfo& peerInfo,
				const EndpointId endpointId);

    bool RemovePeer(Peer* peer);
    bool RemovePeer(const u64 peerId);
    bool RemovePeer(const EndpointId endpointId);

    netConnectionManager* m_CxnMgr;

    unsigned m_ChannelId;

    typedef inmap<EndpointId, Peer, &Peer::m_ByAddrLink> PeersByEndpointId;
    typedef inmap<u64, Peer, &Peer::m_ByIdLink> PeersById;
    typedef inlist<Peer, &Peer::m_ListLink> PeerList;

    typedef inmap<rlGamerId, Gamer, &Gamer::m_ByIdLink> GamersById;
    typedef inlist<Gamer, &Gamer::m_ListLink> GamerList;

    PeersByEndpointId m_PeersByEndpointId;
    PeersById m_PeersById;
    PeerList m_PeerPool;

    GamersById m_GamersById;
    GamerList m_GamerPool;

    Peer m_PeerPile[MAX_PEERS];
    Gamer m_GamerPile[MAX_REMOTE_GAMERS];

    Peer m_LocalPeer;

    bool m_Initialized  : 1;

	// temporary players for async joins
	Gamer m_TempPlayerPile[MAX_REMOTE_GAMERS];
	GamerList m_TempPlayers;
	GamerList m_TempPlayerPool;
};

}   //namespace rage

#endif  //PEERMANAGER_H
