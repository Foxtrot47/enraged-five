// 
// sample_snet/app.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef APP_H
#define APP_H

#include "livemanager.h"
#include "peermanager.h"
#include "ui.h"
#include "sample_session.schema.h"

#include "atl/array.h"
#include "data/growbuffer.h"
#include "input/virtualkeyboard.h"
#include "math/random.h"
#include "net/bandwidth.h"
#include "rline/livestream/rllivestream.h"
#include "rline/rlfriend.h"
#include "rline/rlfriendsreader.h"
#include "rline/clan/rlclan.h"
#include "rline/cloud/rlcloud.h"
#include "rline/profilestats/rlprofilestats.h"
#include "rline/facebook/rlfacebookcommon.h"
#include "rline/inbox/rlinbox.h"
#include "rline/presence/rlpresencequery.h"
#include "rline/ugc/rlugc.h"
#include "rline/rlstats.h"
#include "rline/rlsystemui.h"
#include "rline/rltitlestorage.h"
#include "system/service.h"

#if __LIVE
#include "rline/rlxlsp.h"
#endif

#if RSG_DURANGO
#include "rline/durango/rlxbl_interface.h"
#endif

#if RSG_PC
#include "rline/rlpc.h"
#endif

using namespace rage;

//From the schema.h file.
using namespace player_schema;

struct MigrateHostMsg
{
	NET_MESSAGE_DECL(MigrateHostMsg, netMessageId::MSG_ID_NUM_MESSAGE_IDS + 1);

	MigrateHostMsg()
		: m_PeerId(0)
	{
	}

	MigrateHostMsg(u64 peerId)
		: m_PeerId(peerId)
	{
	}

	NET_MESSAGE_SER(bb, msg)
	{
		return bb.SerUns(msg.m_PeerId, 64);
	}

	u64 m_PeerId;
};

struct SwingMsg
{
    NET_MESSAGE_DECL(SwingMsg, netMessageId::MSG_ID_NUM_MESSAGE_IDS + 2);

    SwingMsg()
        : m_HitResult(HIT_NONE)
    {
    }

    NET_MESSAGE_SER(bb, msg)
    {
		// test maximum message sizes by filling the buffer
		u8 filler[netMessage::MAX_BYTE_SIZEOF_PAYLOAD] = {0};
        return AssertVerify(bb.SerInt(msg.m_HitResult, 32)
            && bb.SerBytes(&msg.m_GamerId, sizeof(msg.m_GamerId))
			&& bb.SerBytes(filler, netMessage::MAX_BYTE_SIZEOF_PAYLOAD - 4 - 8)); // 4 for hit result, 8 for gamerid
    }

    int m_HitResult;
    rlGamerId m_GamerId;
};

struct StartMatchMsg
{
	NET_MESSAGE_DECL(StartMatchMsg, netMessageId::MSG_ID_NUM_MESSAGE_IDS + 3);

	NET_MESSAGE_SER(/*bb*/, /*msg*/)
	{
		return true;
	}
};


struct EndMatchMsg
{
    NET_MESSAGE_DECL(EndMatchMsg, netMessageId::MSG_ID_NUM_MESSAGE_IDS + 4);

    NET_MESSAGE_SER(/*bb*/, /*msg*/)
    {
        return true;
    }
};

//Holds all data for local stats that we track
struct ProfileStatData
{
    int m_StatId;
    rlProfileStatsValue m_StatValue;
    rlProfileStatsPriority m_Priority;
    bool m_bDirty:1;

    ProfileStatData()
    : m_StatId(0)
    , m_StatValue()
    , m_Priority(RL_PROFILESTATS_MIN_PRIORITY)
    , m_bDirty(false)
    {}
};

class App
{
    enum State
    {
        STATE_INVALID           = -1,
        STATE_FRONT_END,

        STATE_MULTIPLAYER_MAIN_MENU,

        STATE_CONFIGURING_HOST,
        STATE_CONFIGURING_QUERY,
        STATE_SELECTING_SESSION,
        STATE_VIEWING_SESSION_DETAILS,
        STATE_LOBBY,
        STATE_GAME,
        STATE_CONFIGURING_ATTRIBUTES,
        STATE_VIEWING_FRIENDS_LIST,
        STATE_VIEWING_INVITES_RECEIVED,
        STATE_SOCIAL_CLUB_MAIN_MENU,
		STATE_COMMERCE_AND_ENTITLEMENT_MAIN_MENU,
		STATE_FACEBOOK_MAIN_MENU,
        STATE_CLOUD_MAIN_MENU,
        STATE_PRESENCE_MAIN_MENU,
		STATE_RICHPRESENCE_MAIN_MENU,
		STATE_ACHIEVEMENTS_MAIN_MENU,
		STATE_PROFILE_STATS_MAIN_MENU,
		STATE_PROFILE_STATS_RESET_BY_GROUP_MENU,
        STATE_COMMUNITY_STATS_MAIN_MENU,

        STATE_LEADERBOARDS_MAIN_MENU,
        STATE_LEADERBOARDS2_WRITE_MENU,
        STATE_LEADERBOARDS2_READ_MENU,
        STATE_LEADERBOARDS_PLAYER_LB_MENU,
        STATE_LEADERBOARDS_GROUP_LB_MENU,
        STATE_LEADERBOARDS_GROUP_MEMBER_LB_MENU,

        STATE_INBOX_MAIN_MENU,

        STATE_CLANS_MAIN_MENU,
        STATE_CLANS_MEMBERS_MENU,
        STATE_CLANS_INVITES_MENU,
        STATE_CLANS_RANKS_MENU,
        STATE_CLANS_REQUESTS_MENU,
        STATE_CLANS_SENT_REQUESTS_MENU,
        STATE_CLANS_ALL_MENU,
        STATE_CLANS_INVITE_FRIEND_MENU,
        STATE_CLANS_WALL_MESSAGE_MENU,

        STATE_CLANS_GET_METADATA,
        STATE_CLANS_GET_FEUD_STAT,

		STATE_PARTY_MAIN_MENU,

        STATE_UGC_MAIN_MENU,
        STATE_UGC_CONTENT_LIST,
        STATE_UGC_CONTENT_DETAILS,
        STATE_UGC_PLAYER_LIST,

		STATE_ENTITLEMENT,
		
		STATE_LIVESTREAM,
		STATE_YOUTUBE,

        STATE_GENERIC_RESULT,

        STATE_SEGUE,

        NUM_STATES
    };

    enum Segue
    {
        SEGUE_INVALID           = -1,
        SEGUE_GENERIC,
        SEGUE_QOS,
        SEGUE_GETTING_LEADERBOARD_SIZE,
        SEGUE_WRITING_LEADERBOARD2,
        SEGUE_READING_LEADERBOARD2,
        SEGUE_READING_STATS,
        SEGUE_READING_INBOX_MESSAGES,

        SEGUE_SESSION_SEARCHING,
        SEGUE_SESSION_HOSTING,
        SEGUE_SESSION_QUERYING_CONFIG,
        SEGUE_SESSION_JOINING,
        SEGUE_SESSION_CHANGING_ATTRIBUTES,
        SEGUE_SESSION_TOGGLING_INVITABLE,
        SEGUE_SESSION_LEAVING,
        SEGUE_SESSION_LEAVING_TO_JOIN_ANOTHER,
        SEGUE_SESSION_DESTROYING,
        SEGUE_SESSION_STARTING,
        SEGUE_SESSION_ENDING,
		SEGUE_SESSION_JVP_LOOKUP,

        SEGUE_PARTY_HOSTING,
        SEGUE_PARTY_JOINING,
        SEGUE_PARTY_LEAVING,
        SEGUE_PARTY_LEAVING_GAME_TO_JOIN_ANOTHER,
        SEGUE_PARTY_LEAVING_TO_LEAVE_GAME,
        SEGUE_PARTY_LEAVING_TO_HOST_GAME,

        SEGUE_SOCIAL_CLUB_GET_TERMS_OF_SERVICE,
        SEGUE_SOCIAL_CLUB_ACCEPT_TERMS_OF_SERVICE,
        SEGUE_SOCIAL_CLUB_LINK_OR_UNLINK_ACCOUNT,
        SEGUE_SOCIAL_CLUB_CREATE_ACCOUNT,
        SEGUE_SOCIAL_CLUB_RESET_PASSWORD,
        SEGUE_SOCIAL_CLUB_GET_COUNTRIES,
        SEGUE_SOCIAL_CLUB_UPDATE_PASSWORD,
		SEGUE_SOCIAL_CLUB_POST_USER_FEED_ACTIVITY,
        SEGUE_CLOUD_DOWNLOAD,
        SEGUE_CLOUD_UPLOAD,
        SEGUE_CLOUD_DELETE,

		SEGUE_CLOUD_SAVE_IDENTIFY_CONFLICTS,
		SEGUE_CLOUD_SAVE_GET_FILE,
		SEGUE_CLOUD_SAVE_POST_FILE,
		SEGUE_CLOUD_REGISTER_FILE,
		SEGUE_CLOUD_MERGE_FILES,
		SEGUE_CLOUD_UNREGISTER_FILE,
		SEGUE_CLOUD_DELETE_FILES,
		SEGUE_CLOUD_QUERY_BETA,
		SEGUE_CLOUD_BACKUP_CONFLICTS,
		SEGUE_CLOUD_SEARCH_BACKUPS,
		SEGUE_CLOUD_RESTORE_BACKUP,
		SEGUE_CLOUD_GET_METADATA,

		SEGUE_FACEBOOK_GET_ACCESS_TOKEN,
		SEGUE_FACEBOOK_POST_OPEN_GRAPH,
		SEGUE_FACEBOOK_VALIDATE_APP_PERMISSIONS,

		SEGUE_PRESENCE_QUERY,
        SEGUE_PRESENCE_QUERY_COUNT,

		SEGUE_ACHIEVEMENTS_AWARD_ACHIEVEMENT,
		SEGUE_ACHIEVEMENTS_READ_ACHIEVEMENTS,
		SEGUE_ACHIEVEMENTS_DELETE_ACHIEVEMENTS,
        SEGUE_ACHIEVEMENTS_GET_ROS_ACHIEVEMENTS,
        SEGUE_ACHIEVEMENTS_GET_ROS_ACHIEVEMENT_DEFINITIONS,

		SEGUE_CLAN_RETRIEVING_MINE,
        SEGUE_CLAN_RETRIEVING_ALL,
        SEGUE_CLAN_RETRIEVING_ALL_LEADERS,
        SEGUE_CLAN_RETRIEVING_MEMBERS,
        SEGUE_CLAN_RETRIEVING_RANKS,
        SEGUE_CLAN_RETRIEVING_REQUESTS,
        SEGUE_CLAN_RETRIEVING_SENT_REQUESTS,
        SEGUE_CLAN_RETRIEVING_INVITES,
        SEGUE_CLAN_RETRIEVING_MINE_TO_INVITE,
        SEGUE_CLAN_RETRIEVING_WALL_MESSAGES,
        SEGUE_CLAN_RETRIEVING_METADATA_ENUMS,
        SEGUE_CLAN_RETRIEVING_FEUD_STAT,
        SEGUE_CLAN_RETRIEVING_PRIMARY_CLAN,
        SEGUE_CLAN_UPDATING,
        SEGUE_CLAN_RANK_UPDATING,
        SEGUE_CLAN_MEMBER_UPDATING,
        SEGUE_CLAN_INVITE_UPDATING,
        SEGUE_CLAN_MESSAGE_UPDATING,
        SEGUE_CLAN_INVITE_TO_CLAN,
        SEGUE_CLAN_JOIN_REQUEST,
        SEGUE_CLAN_JOIN_REQUEST_DELETE,

        SEGUE_PROFILE_STATS_SYNCHRONIZE,
        SEGUE_PROFILE_STATS_READ_BY_GAMER,
        SEGUE_PROFILE_STATS_FLUSH,
		SEGUE_PROFILE_STATS_RESET_BY_GROUP,

        SEGUE_COMMUNITY_STATS_READ,

        SEGUE_READING_SC_FRIENDS,

        SEGUE_UGC_CREATE_CONTENT,
        SEGUE_UGC_QUERY_CONTENT,
        SEGUE_UGC_COPY_CONTENT,
        SEGUE_UGC_UPDATE_CONTENT,
        SEGUE_UGC_VERSION_CONTENT,
        SEGUE_UGC_PUBLISH_CONTENT,

        SEGUE_UGC_CREATE_VIDEO_CONTENT,
        SEGUE_UGC_UPLOADING_VIDEO,

        NUM_SEGUES
    };

public:
    //Aliases for leaderboards.
    typedef Leaderboard_Hits                    LbNonArb;
    typedef Leaderboard_ArbHits                 LbArb;
    typedef SkillLeaderboard_ExhibitionRanked   LbSkillRanked;
    typedef SkillLeaderboard_ExhibitionStandard LbSkillStandard;

    App();

    void Init(sysMemAllocator* allocator,
            netConnectionManager* cxnMgr,
            void* voiceChatDevice);
    void Shutdown();
    bool Update();
	bool UpdateAutomatedTest();

    void Draw();

    void SetGamer(Gamer* gamer);

    const Gamer* GetGamer() const;

    unsigned GetGamers(Gamer* gamers[],
                        const unsigned maxGamers);

    LiveManager* GetLiveMgr();
	PeerManager* GetPeerManager() {return &m_PeerMgr;}


	static ioVirtualKeyboard* GetVirtualKeyboard() { return &sm_VirtualKeyboard; }

private:

    Gamer* GetLocalGamerById(const rlGamerId& gamerId);

    Gamer* GetLocalGamerByIndex(const int gamerIndex);

    void OnFeEvent(UiElement* el, const int evtId, void* arg);
    void OnFriendsListEvent(UiElement* el, const int evtId, void* arg);
    void OnInvitesReceivedEvent(UiElement* el, const int evtId, void* arg);
    void OnHostConfigEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg);
    void OnQueryConfigEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg);
    void OnSesionSelectEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg);
    void OnSessionDetailsEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg);
    void OnLobbyEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg);
    void OnGameEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg);
    void OnConfigureAttrsEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg);
    void OnUgcEvent(UiElement* ASSERT_ONLY(el), const int evtId, void* arg);

    void OnUiEvent(UiElement* el, const int evtId, void* arg);

	// System Events
	ServiceDelegate m_ServiceDelegate;
	void OnServiceEvent(sysServiceEvent* evt);

	void OnRosEvent(const rlRosEvent& evt);
    void OnPresenceEvent(const rlPresenceEvent* evt);
    void OnSessionEvent(snSession* session, const snEvent* evt);
    void OnCxnEvent(netConnectionManager* cxnMgr, const netEvent* evt);
	void OnFriendEvent(const rlFriendEvent* evt);

	// Livestream
#if LIVESTREAM_ENABLED
	static void VideoCaptureCallback(unsigned width, unsigned height, unsigned char* destBuffer);
	void OnLiveStreamEvent(const rlLiveStreamEvent* evt);
#endif

    void OnUgcQueryContent(const int index,                 //Index into results
                           const char* contentId,           //Current content being read
                           const rlUgcMetadata* metadata,   //Metadata for this content, or NULL if hash passed by client matched server
                           const rlUgcRatings* ratings,     //Ratings for this content
                           const char* statsJson,           //Stats JSON for this content
                           const unsigned numPlayers,       //Number of players returned for this piece of content
                           const unsigned playerIndex,      //Index of current player being returned
                           const rlUgcPlayer* player);      //Current player being read

    
    void DoRateContent(const float rating);

    void HostSession();
    void JoinSession(Gamer* gamer,
                    const rlSessionInfo& sessionInfo,
                    const rlNetworkMode netMode,
                    const rlSlotType slotType);
    void LeaveSession(Gamer* gamer);
	void MigrateHostToMyself();
	void MigrateHost(u64 peerId);
    void DestroySession();
	void UpdatePresenceBlob();

    void FindSessions();
    void ChangeAttributes();
    void ToggleInvitable();
    void StartMatch();
    void EndMatch();

    void Swing();

    void PushSegue(const Segue segue);
    void PopSegue();
    Segue GetSegue() const;
    
    void SetState(const State state);
    void PushState(const State state);
    void PushResult(const char* resultText);
    void PopState();
    void PopStateTo(const State state);
    void PopAllStates();
    State GetState() const;
    bool InState(const State state) const;

    void HandleGetGamerData(snSession* session,
                            const rlGamerInfo& gamerInfo,
                            snGetGamerData* gamerData) const;

    bool HandleJoinRequest(snSession* session,
                            const rlGamerInfo& gamerInfo,
                            snJoinRequest* joinRequest) const;

    void RecordSwing(Gamer* gamer, const SwingMsg& swing);
    bool WriteLeaderboard2();
    bool ReadLeaderboard2(const int readType);
    bool GetLeaderboardSize(const unsigned lbId);
    bool ReadLeaderboardByRank(const unsigned lbId);
    bool ReadLeaderboardByRow(const unsigned lbId);
    bool ReadLeaderboardMemberByGroups(const unsigned lbId);
    bool ReadLeaderboardByRadius(const unsigned lbId);
	bool ResetProfileStatsByGroup();

    //Inbox
    bool InboxGetMessages();
    bool InboxGetNewMessages();
    bool InboxPostMessage();
    bool InboxPostMessageToFriends();
    bool InboxPostMessageToCrew();

    void ClearGamerStats();

    bool ReadProfileStatsByGamer();

    bool RetrieveThenPushClanState(State clanState);

    void ShowSigninUi();

    bool IsSystemUiShowing() const;

    ProfileStatData* GetLocalStat(const int statId);
    rlProfileStatsValue* GetLocalValue(const int statId);

    atArray<State> m_StateStack;
    atArray<Segue> m_SegueStack;

    LiveManager* m_LiveMgr;
    PeerManager m_PeerMgr;
    Gamer* m_Gamer;
    datGrowBuffer m_Gb;
    rlCloudFileInfo m_FileInfo;
    netStatus m_MyStatus;

#if RSG_PC
	rgsc::IAsyncStatusLatestVersion* m_RgscStatus;
#endif

    //Profile stats-related constants
    enum 
    {
        //Max stats and records to read per ReadStatsByGamer call.
        MAX_PROFILE_STATS_TRACKED = 1000,
        READ_STATS_MAX_RECORDS = 1,

        //Max dirty stats to track. This is passed to rlProfileStats::Init().
        //NOTE: We only track a fraction of the total, to be able to test 
        //      situations when we have more dirty than we can track.
        MAX_DIRTY_PROFILE_STATS = MAX_PROFILE_STATS_TRACKED / 4,

        //Max size of a submission. This is passed to rlProfileStats::Init().
        //Note that the backend may further lower this, but can never raise it.
        MAX_SUBMISSION_SIZE = 1024,

        //Percent chance we'll dirty a profile stat when Set Dirty is selected in the UI.
        PROFILE_STAT_DIRTY_PCT = 10,
    };

    //Holds the local values of stats we track
    unsigned m_NumLocalProfileStats;
    ProfileStatData m_LocalProfileStats[MAX_PROFILE_STATS_TRACKED];

    //Holds data for profile stats ReadStatsByGamer calls
    s32 m_ReadProfileStatsIds[MAX_PROFILE_STATS_TRACKED];
    rlProfileStatsFixedRecord<MAX_PROFILE_STATS_TRACKED> m_ReadProfileStatsRecords[READ_STATS_MAX_RECORDS];
    rlProfileStatsReadResults m_ReadProfileStatsResult;

    netBandwidthRecorder m_BandwidthRecorder;

#if LIVESTREAM_ENABLED
	rlLiveStream::Delegate m_LiveStreamDlgt;
#endif
	rlRos::Delegate m_RosDlgt;
	rlFriendsManager::Delegate m_FriendDlgt;
    rlPresence::Delegate m_PresenceDlgt;
    snSession::Delegate m_SessionDlgt;
    netConnectionManager::Delegate m_CxnMgrDlgt;
    rlUgc::QueryContentDelegate m_UgcDlgt;

    Gamer m_LocalGamers[RL_MAX_LOCAL_GAMERS];

    enum{ MAX_SEARCH_RESULTS = 20 };

    rlSessionDetail m_SessionDetails[MAX_SEARCH_RESULTS];
    unsigned m_NumSearchResults;

    UiLabel                 m_UiSegue;
    UiFrontEnd              m_UiFrontEnd;
    UiMultiplayerMainMenu   m_UiMultiplayerMainMenu;
    UiHostConfig            m_UiHostConfig;
    UiQueryConfig           m_UiQueryConfig;
    UiSessionSelect         m_UiSessionSelect;
    UiSessionDetails        m_UiSessionDetails;
    UiLobby                 m_UiLobby;
    UiGame                  m_UiGame;
    UiConfigureAttrs        m_UiConfigureAttrs;
    UiFriendsList           m_UiFriendsList;
    UiInvitesReceived       m_UiInvitesReceived;
    UiLeaderboardMainMenu   m_UiLeaderboardMainMenu;
    UiLeaderboard2WriteMenu m_UiLeaderboard2WriteMenu;;
    UiLeaderboard2ReadMenu  m_UiLeaderboard2ReadMenu;
    UiLeaderboardMenu       m_UiLeaderboardMenu;
    UiInboxMenu             m_UiInboxMenu;
    UiGenericResult         m_UiGenericResult;
    UiSocialClubMainMenu    m_UiSocialClubMainMenu;
	UiCommerceAndEntitlementsMainMenu    m_UiCommerceAndEntitlementMainMenu;
    UiCloudMainMenu         m_UiCloudMainMenu;
	UiFacebookMainMenu		m_UiFacebookMainMenu;
    UiUgcMainMenu           m_UiUgcMainMenu;
    UiUgcContentList        m_UiUgcContentList;
    UiUgcContentDetails     m_UiUgcContentDetails;
    UiUgcPlayerList         m_UiUgcPlayerList;
    UiPresenceMainMenu      m_UiPresenceMainMenu;
	UiRichPresenceMainMenu  m_UiRichPresenceMainMenu;
	UiAchievementsMainMenu  m_UiAchievementsMainMenu;
	UiProfileStatsMainMenu  m_UiProfileStatsMainMenu;
	UiProfileStatsResetByGroupMenu m_uiProfileStatsResetByGroupMenu;
    UiCommunityStatsMainMenu  m_UiCommunityStatsMainMenu;
    UiClansMainMenu         m_UiClansMainMenu;
    UiClansAllMenu          m_UiClansAllMenu;
    UiClanMembersMenu       m_UiClanMembersMenu;
    UiClanRanksMenu         m_UiClanRanksMenu;
    UiClanRequestsMenu      m_UiClanRequestsMenu;
    UiClanSentRequestsMenu  m_UiClanSentRequestsMenu;
    UiClanInvitesMenu       m_UiClanInvitesMenu;
    UiClanInviteFriendMenu  m_UiClanInviteFriendMenu;
    UiClanWallMessagesMenu  m_UiClanWallMessagesMenu;
	UiPartyMainMenu			m_UiPartyMainMenu;
	UiEntitlementMenu		m_UiEntitlement;
	UiLiveStreamMenu		m_LiveStreamMenu;
	UiYoutubeMenu			m_YoutubeMenu;

    UiElement* m_Pages[NUM_STATES];

    //Host uses this to assign team ids.
    mthRandom m_Rng;

    Gamer* m_Joiner;
    rlSlotType m_SlotType;
    rlSessionInfo m_JoinSessionInfo;
    rlSessionDetail m_JoinSessionDetail;
    unsigned m_NumJoinSessionDetailResults;

    netStatus m_PartyStatus;
	netStatus m_RichPresenceStatus;

    rlNetworkMode m_NetMode;

    void OnProfileStatsEvent(const rlProfileStatsEvent* e);

    unsigned m_LbReadId;    //Id of leaderboard we're going to read.

    static const int MAX_NUM_ROWS   = 10;

    rlLeaderboardRow m_LbRows[MAX_NUM_ROWS];
    rlLeaderboard2Row m_Lb2Rows[MAX_NUM_ROWS];
    rlLeaderboard2Row* m_Lb2RowPtrs[MAX_NUM_ROWS];
    rlStatValue m_StatValueHeap[MAX_NUM_ROWS][32];
    unsigned m_NumLbRowsReturned;
    unsigned m_TotalLbRows;

    rlInboxMessageIterator m_InboxMessageIter;

    char m_ScPresQueryResultsBuf[1024];
    char* m_ScPresQueryResults[100];
    unsigned m_NumScPresQueryResultsRetrieved;
    unsigned m_NumScPresQueryResults;

    int m_ActiveFriend;
    rlClanDesc* m_ActiveClan;
    rlClanMember m_PrimaryClan;
    u32          m_PrimaryClanCount;

    rlClanMembershipData m_MyClanMembership[10];
    unsigned m_NumMyClanMembership;

    rlClanDesc m_AllClans[10];
    unsigned m_NumAllClans;

    rlClanLeader m_ClanLeaders[10];
    unsigned m_NumClanLeaders;

    rlClanMember m_ClanMembers[50];
    unsigned m_NumClanMembers;

    rlClanRank m_ClanRanks[RL_CLAN_MAX_INVITES];
    unsigned m_NumClanRanks;

    rlClanJoinRequestRecieved m_ClanRequests[RL_CLAN_MAX_INVITES];
    unsigned m_NumClanRequests;

    rlClanJoinRequestSent m_ClanSentRequests[RL_CLAN_MAX_INVITES];
    unsigned m_NumClanSentRequests;

    rlClanInvite m_ClanInvites[RL_CLAN_MAX_INVITES];
    unsigned m_NumClanInvites;

    rlClanWallMessage m_ClanMessags[20];
    unsigned m_NumClanMessages;

    rlClanMetadataEnum m_ClanEnums[20];
    unsigned m_NumClanEnums;
		
    rlClanFeuder m_ClanFeuds[20];
    unsigned m_NumClanFeuds;

	char m_FacebookOGPostId[64];

#if RSG_DURANGO || RSG_PC
	bool m_FBPermissions;
#endif

#if !RSG_ORBIS
	u8 m_FacebookWorkBuffer[1024*8];
#endif

#if RSG_DURANGO
	netStatus m_SystemPartyStatus;
#endif

#if RSG_PC
	static rgsc::ICloudSaveManifest* rgscManifest;
	static rgsc::ICloudSaveBackupInfo* rgscBackupInfo;
#endif

	// JVP
	rlSessionQueryData m_SessionQuery;
	unsigned m_NumSessionFound;
	rlGamerHandle* m_JipGamerHandle;

	static ioVirtualKeyboard sm_VirtualKeyboard;

	u32 m_CrashTime;
    bool m_LeavingByChoice              : 1;
    bool m_Done                         : 1;
    bool m_Initialized                  : 1;
};

#endif
