// 
// sample_snet/ui.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_SNET_UI_H
#define SAMPLE_SNET_UI_H

#include "atl/delegate.h"
#include "atl/inlist.h"
#include "input/virtualkeyboard.h"
#include "net/status.h"
#include "rline/rlfriend.h"
#include "rline/rlfriendsmanager.h"
#include "rline/rlsessionfinder.h"
#include "rline/rlschema.h"
#include "rline/ugc/rlugc.h"
#include "sample_session.schema.h"
#include "vector/color32.h"
#include "rline/ugc/rlugccommon.h"
#include "rline/scpresence/rlscpresence.h"
#include "rline/youtube/rlyoutube.h"

#define UI_GAP  (2.0f * grcFont::GetCurrent().GetHeight())

#define UI_TITLE_X      200
#define UI_TITLE_Y      200
#define UI_CONTENT_X    100
#define UI_CONTENT_Y    (UI_TITLE_Y + (4*UI_GAP))

#define UI_LEFT_CTRL_X      50
#define UI_LEFT_CTRL_Y      400
#define UI_RIGHT_CTRL_X     300
#define UI_RIGHT_CTRL_Y     400

#define	UI_FOOTER_X		100
#define UI_FOOTER_Y		600

class App;

namespace rage
{

class rlSessionQueryResult;
class rlClanLeader;
class rlClanDesc;
class rlClanInvite;
class rlClanMember;
class rlClanMembershipData;
class rlClanRank;
class rlClanWallMessage;
class snSession;
class VoiceChat;

class UiElement;
class LiveManager;

enum UiEvent
{
    UIEVENT_ACTION,
    UIEVENT_BACK,
    UIEVENT_SELECT,
};

enum UiAction
{
    ACTION_BACK     = 0x7FFF0000
};

class UiContainer;

class UiElement : public atDelegate<void (UiElement*, const int, void*)>
{
    friend class UiContainer;
public:

    UiElement();

    virtual ~UiElement();

    virtual void Shutdown();

	virtual void Update() {}
    virtual void Draw() {}

    virtual bool OnLeft() {return m_Focus ? m_Focus->OnLeft() : false;}
    virtual bool OnRight() {return m_Focus ? m_Focus->OnRight() : false;}
    virtual bool OnUp() {return m_Focus ? m_Focus->OnUp() : false;}
    virtual bool OnDown() {return m_Focus ? m_Focus->OnDown() : false;}
    virtual bool OnAction() {return (m_Focus && m_Focus->IsEnabled()) ? m_Focus->OnAction() : false;}
	virtual bool OnPageLeft() { return m_Focus ? m_Focus->OnPageLeft() : false; }
	virtual bool OnPageRight() { return m_Focus ? m_Focus->OnPageRight() : false; }

    virtual bool OnBack()
    {
        if(m_Focus && m_Focus->IsEnabled())
        {
            m_Focus->FireEvent(UIEVENT_BACK);
        }
        else
        {
            this->FireEvent(UIEVENT_BACK);
        }

        return true;
    }
    virtual bool OnFocusChange(const bool fc) {return m_Focus ? m_Focus->OnFocusChange(fc) : false;}

    void SetColor(const Color32& color) {m_Color = color;}
    const Color32& GetColor() const {return m_Color;}

    void SetFocus(UiElement* focus) {m_Focus = focus;}
    UiElement* GetFocus() {return m_Focus;}
    const UiElement* GetFocus() const {return m_Focus;}

    virtual void SetXY(const float x, const float y) {m_X = x; m_Y = y;}
    float GetX() const {return m_X;}
    float GetY() const {return m_Y;}

    void SetEnabled(const bool b);
    bool IsEnabled() const {return m_Enabled;}

    void FireEvent(const int evtId, void* arg = 0)
    {
        if(this->IsBound())
        {
            this->Invoke(this, evtId, arg);
        }
    }

	static const char* GetPageLeftText()
	{
#if RSG_PC
		return "NUM0";
#else
		return "LT";
#endif
	}

	static const char* GetPageRightText()
	{
#if RSG_PC
		return "NUM1";
#else
		return "RT";
#endif
	}

protected:
    Color32 m_Color;
    float m_X, m_Y;

private:

    UiElement* m_Focus;

    inlist_node<UiElement> m_ListLink;

    UiContainer* m_Container;

    bool m_Enabled  : 1;
};

class UiLabel : public UiElement
{
public:

    enum
    {
        MAX_LABEL_LENGTH    = 127
    };

    UiLabel(const char* text = "*L*A*B*E*L*");

    void SetText(const char* text);

    void SetText(const char* text,
                const int length);

	void SetScale(float scaleX, float scaleY);

    const char* GetText() const;

    virtual void Draw();

    virtual bool OnFocusChange(const bool haveFocus);

private:

    char m_Text[MAX_LABEL_LENGTH+1];
	float m_ScaleX;
	float m_ScaleY;
};

class UiContainer : public UiElement
{
public:

    virtual ~UiContainer();

    virtual void Draw();

    virtual void AddElement(UiElement* el);

    virtual void RemoveElement(UiElement* el);

    virtual void RemoveAllElements();

    virtual void OnChildEnabled(UiElement* child,
                                const bool enabled);

    int GetElementCount() const;

protected:
    typedef inlist<UiElement, &UiElement::m_ListLink> ElList;

    ElList m_Children;
};

class UiList : public UiContainer
{
public:

    UiList();
    
    virtual void AddElement(UiElement* el);

    virtual void RemoveElement(UiElement* el);

    virtual bool OnUp();

    virtual bool OnDown();

    virtual bool OnLeft();

    virtual bool OnRight();

    virtual bool OnAction();

    virtual void OnChildEnabled(UiElement* child,
                                const bool enabled);

    UiElement* GetSelection();
	virtual void ResetFocus();

    const UiElement* GetSelection() const;

    void SetEnabled(UiElement* el,
                    const bool b);

    bool IsEnabled(const UiElement* el) const;

    void SetEnabled(const bool b) {this->UiContainer::SetEnabled(b);}
    bool IsEnabled() const {return this->UiContainer::IsEnabled();}

    void Select(UiElement* el);

    virtual void Next();

    virtual void Prev();

protected:

    ElList::iterator m_ItFocus;

    bool m_SettingEnabled   : 1;
};

class UiMultiValue : public UiList
{
public:

    void SetText(const char* text);

    const char* GetLabel() const;

	UiLabel* GetLabelElement() { return &m_Label; }

    virtual void AddElement(UiElement* el);

    virtual void SetXY(const float x, const float y);

    virtual void Draw();

    virtual bool OnUp();
    virtual bool OnDown();

    virtual bool OnLeft();

    virtual bool OnRight();

    virtual bool OnFocusChange(const bool haveFocus);

private:

    void UpdateChildPositions();

    UiLabel m_Label;
};

class UiSpinner : public UiElement
{
public:

    UiSpinner();

    void SetText(const char* text);

    void SetMin(const int minValue);

    void SetMax(const int maxValue);

    void SetLimits(const int minValue, const int maxValue);

    void SetIncrement(const int increment);

    void SetValue(const int value);

    int GetValue() const;

    int GetMax() const;

    int GetMin() const;

    int GetIncrement() const;

    const char* GetLabel() const;

    virtual void SetXY(const float x, const float y);

    virtual void Draw();

    virtual bool OnUp();
    virtual bool OnDown();

    virtual bool OnLeft();

    virtual bool OnRight();

    virtual bool OnFocusChange(const bool haveFocus);

private:

    void UpdateChildPositions();

    int m_Value;
    int m_MaxValue;
    int m_MinValue;
    int m_Increment;
    UiLabel m_Label;
    UiLabel m_ValueLabel;
};

class UiGenericResult : public UiElement
{
public:
    enum { MAX_LINES = 30 };

    UiGenericResult();
    virtual ~UiGenericResult();

    void ClearText();
    void AddText(const char* text);
    void AddText(const char* text,
                const int length);

    //Convenience function: does a ClearText() and an AddText()
    void SetText(const char* text);

    virtual void Draw();

private:

    void OnEvent(UiElement* el, const int evtId, void*);

#if !RSG_ORBIS
    int m_ReturnState;
#endif

    unsigned m_NumLines;

    UiLabel m_Lines[MAX_LINES];
    UiLabel m_Back;
    
    UiList m_Selections;
};

class UiFrontEnd : public UiElement
{
public:
    enum
    {
        FE_MULTIPLAYER,
        FE_FRIENDS_LIST,
        FE_FRIEND_INVITES,
        FE_LEADERBOARDS,
        FE_INBOX,
		FE_SOCIAL_CLUB,
		FE_COMMERCE_AND_ENTITLEMENT,
		FE_FACEBOOK,
		FE_CLOUD,
        FE_UGC,
		FE_PRESENCE,
		FE_RICHPRESENCE,
		FE_ACHIEVEMENTS,
		FE_PROFILE_STATS,
        FE_COMMUNITY_STATS,
		FE_CLANS,
		FE_DISPLAY_UNLOCKS,
		FE_DISPLAY_PARTY,
		FE_ENTITLEMENT,
		FE_ADDFRIEND,
        FE_SHOW_SIGNIN_UI,
		FE_SHOW_APP_HELP,
		FE_LIVESTREAM,
		FE_YOUTUBE,
		FE_CRASH,
        FE_QUIT
    };

    UiFrontEnd();
    virtual ~UiFrontEnd();

    virtual void Draw();

    void UpdateEnabled(const bool hasNetwork,
                       const bool isSignedIn,
                       const bool isOnline,
                       const bool hasInvites);

private:

    void OnEvent(UiElement* el, const int evtId, void*);
	virtual bool OnLeft();
	virtual bool OnRight();

	bool m_Crash;

    UiLabel m_Multiplayer;
    UiLabel m_FriendsList;
    UiLabel m_InvitesReceived;
    UiLabel m_Leaderboards;
    UiLabel m_Inbox;
	UiLabel m_SocialClub;
	UiLabel m_CommerceAndEntitlement;
	UiLabel m_Facebook;
	UiLabel m_Cloud;
    UiLabel m_Ugc;
	UiLabel m_Presence;
	UiLabel m_RichPresence;
	UiLabel m_Achievements;
	UiLabel m_ProfileStats;
    UiLabel m_CommunityStats;
	UiLabel m_Clans;
	UiLabel m_Parties;
	UiLabel m_DisplayUnlocks;
	UiLabel m_Entitlement;
	UiLabel m_ShowSigninUi;
	UiLabel m_AddFriendUi;
	UiLabel m_ShowAppHelpMenu;
	UiLabel m_LiveStream;
	UiLabel m_YoutubeMenu;
	UiLabel m_Quit;
    
    UiList m_Selections;
};

//////////////////////////////////////////////////////////////////////////
//  Leaderboards
//////////////////////////////////////////////////////////////////////////
class UiLeaderboardMainMenu : public UiElement
{
public:
    enum
    {
        ACTION_SHOW_LEADERBOARD2_WRITE_MENU,
        ACTION_SHOW_LEADERBOARD2_READ_MENU,
        ACTION_SHOW_SWINGS_BY_STRIKE_LB_MENU,
        ACTION_SHOW_CLAN_SWINGS_BY_STRIKE_LB_MENU,
        ACTION_SHOW_CLAN_MEMBER_SWINGS_BY_STRIKE_LB_MENU,
        ACTION_SHOW_HITS_LB_MENU,
        ACTION_SHOW_CLAN_HITS_LB_MENU,
        ACTION_SHOW_CLAN_MEMBER_HITS_LB_MENU,
        ACTION_SHOW_ARB_HITS_LB_MENU,
        ACTION_SHOW_SKILL_EXHIB_MENU,
        ACTION_SHOW_SKILL_RANKED_EXHIB_MENU,
    };

    UiLeaderboardMainMenu();
    virtual ~UiLeaderboardMainMenu();

    virtual void Draw();

private:

    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_Leaderboard2Write;
    UiLabel m_Leaderboard2Read;
    UiLabel m_SwingsByStrikeLb;
    UiLabel m_ClanSwingsByStrikeLb;
    UiLabel m_ClanMemberSwingsByStrikeLb;
    UiLabel m_HitsLb;
    UiLabel m_ClanHitsLb;
    UiLabel m_ClanMemberHitsLb;
    UiLabel m_ArbHitsLb;
    UiLabel m_ExhibSkill;
    UiLabel m_RankedExhibSkill;
    UiLabel m_Back;
    
    UiList m_Selections;
};

class UiLeaderboardMenu : public UiElement
{
public:
    enum
    {
        ACTION_GET_LEADERBOARD_SIZE,
        ACTION_READ_BY_RANK,
        ACTION_READ_BY_ROW,
        ACTION_READ_MEMBER_BY_GROUPS,
        ACTION_READ_BY_RADIUS,
#if __DEV
        ACTION_CLEAR_LEADERBOARD,
#endif
    };

    UiLeaderboardMenu();
    virtual ~UiLeaderboardMenu();

    virtual void Draw();

    void SetTitle(const char* title);

private:

    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_GetLeaderboardSize;
    UiLabel m_ReadByRank;
    UiLabel m_ReadByRow;
    UiLabel m_ReadMemberByGroups;
    UiLabel m_ReadByRadius;
#if __DEV
    UiLabel m_ClearLb;
#endif
    UiLabel m_Back;
    
    UiList m_Selections;
};

class UiLeaderboard2WriteMenu : public UiContainer
{
public:
    enum
    {
        ACTION_SUBMIT,
    };
    UiLeaderboard2WriteMenu();
    virtual ~UiLeaderboard2WriteMenu();

    const char* GetCategory1() const
    {
        return ((const UiLabel*)m_Category1.GetSelection())->GetText();
    }
    const char* GetCategory2() const
    {
        return ((const UiLabel*)m_Category2.GetSelection())->GetText();
    }

    int GetGroup1Id() const{ return m_GroupId1.GetValue(); }
    int GetGroup2Id() const{ return m_GroupId2.GetValue(); }

    int GetSwings() const{ return m_Swings.GetValue(); }
    int GetStrikes() const{ return m_Strikes.GetValue(); }
    int GetSingles() const{ return m_Singles.GetValue(); }
    int GetDoubles() const{ return m_Doubles.GetValue(); }
    int GetTriples() const{ return m_Triples.GetValue(); }
    int GetHomers() const{ return m_Homers.GetValue(); }
    int GetFlyouts() const{ return m_Flyouts.GetValue(); }

private:

    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;

    UiLabel m_Cat1None;
    UiLabel m_Cat1Mission;
    UiLabel m_Cat1Challenge;
    UiMultiValue m_Category1;
    UiSpinner m_GroupId1;

    UiLabel m_Cat2None;
    UiLabel m_Cat2Mission;
    UiLabel m_Cat2Challenge;
    UiMultiValue m_Category2;
    UiSpinner m_GroupId2;

    UiSpinner m_Swings;
    UiSpinner m_Strikes;
    UiSpinner m_Singles;
    UiSpinner m_Doubles;
    UiSpinner m_Triples;
    UiSpinner m_Homers;
    UiSpinner m_Flyouts;

    UiLabel m_Swing;

    UiLabel m_Submit;

    UiLabel m_Reset;

    UiLabel m_Back;
    
    UiList m_Selections;
};

class UiLeaderboard2ReadMenu : public UiContainer
{
public:
    enum
    {
        ACTION_READ_BY_RANK,
        ACTION_READ_BY_ROW,
        ACTION_READ_BY_SCORE,
        ACTION_READ_NEAR,
    };

    UiLeaderboard2ReadMenu();
    virtual ~UiLeaderboard2ReadMenu();

    const char* GetCategory1() const
    {
        return ((const UiLabel*)m_Category1.GetSelection())->GetText();
    }
    const char* GetCategory2() const
    {
        return ((const UiLabel*)m_Category2.GetSelection())->GetText();
    }

    int GetGroup1Id() const{ return m_GroupId1.GetValue(); }
    int GetGroup2Id() const{ return m_GroupId2.GetValue(); }

    rlLeaderboard2Type GetLeaderboardType() const;

private:

    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;

    UiLabel m_Cat1None;
    UiLabel m_Cat1Mission;
    UiLabel m_Cat1Challenge;
    UiMultiValue m_Category1;
    UiSpinner m_GroupId1;

    UiLabel m_Cat2None;
    UiLabel m_Cat2Mission;
    UiLabel m_Cat2Challenge;
    UiMultiValue m_Category2;
    UiSpinner m_GroupId2;

    UiLabel m_LbTypePlayer;
    UiLabel m_LbTypeCrewMember;
    UiLabel m_LbTypeCrew;
    UiLabel m_LbTypeGroupMember;
    UiLabel m_LbTypeGroup;
    UiMultiValue m_LbType;

    UiLabel m_ReadByRank;
    UiLabel m_ReadByRow;
    UiLabel m_ReadByScore;
    UiLabel m_ReadNear;

    UiLabel m_Back;
    
    UiList m_Selections;
};

//////////////////////////////////////////////////////////////////////////
//  Inbox
//////////////////////////////////////////////////////////////////////////
class UiInboxMenu : public UiContainer
{
public:
    enum
    {
        ACTION_GET_MESSAGES,
        ACTION_GET_NEW_MESSAGES,
        ACTION_POST_MESSAGE,
        ACTION_POST_MESSAGE_TO_FRIENDS,
        ACTION_POST_MESSAGE_TO_CREW,
    };

    UiInboxMenu();
    virtual ~UiInboxMenu();

    int GetOffset() const
    {
        return m_Offset.GetValue();
    }

    int GetCount() const
    {
        return m_Count.GetValue();
    }

    void ClearMessages();

    void AddMessage(const u64 timestamp, const char* message);

private:

    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;

    UiSpinner m_Offset;
    UiSpinner m_Count;
    UiLabel m_GetMessages;
    UiLabel m_GetNewMessages;
    UiLabel m_PostMessage;
    UiLabel m_PostMessageToFriends;
    UiLabel m_PostMessageToCrew;

    UiList m_MessageList;
    UiContainer m_Messages[10];
    UiLabel m_MessageTimestamps[10];
    UiLabel m_MessageContents[10];
    unsigned m_NumMessages;

    UiLabel m_Back;
    
    UiList m_Selections;
};

//////////////////////////////////////////////////////////////////////////
// Friends
//////////////////////////////////////////////////////////////////////////
class UiFriendsList : public UiElement
{
public:
    enum
    {
        ACTION_SHOW_PROFILE,
        ACTION_INVITE_TO_SESSION,
        ACTION_INVITE_TO_CLAN,
        ACTION_JOIN_SESSION,
		ACTION_SEND_MESSAGE,
		ACTION_RTA_SUBSCRIBE,
		ACTION_RTA_UNSUBSCRIBE,
		ACTION_GET_SESSIONS,
		ACTION_GET_DISPLAYNAMES,
		ACTION_REFRESH_LIST,
		ACTION_REMOVE_FRIEND,
		ACTION_CHECK_IS_FRIEND,
		ACTION_SEARCH_FRIENDS
    };

    //NOTE: MAX_FRIENDS (plural) has a conflict; it compiles, but at runtime 
    //      the compiler uses the MAX_FRIENDS from rlFriend, which is 50,
    //      causing a crash (even though the debugger shows MAX_FRIENDS is 20).  
    //      Qualifying this with UiFriendsList doesn't work, either.
    enum
    {
        MAX_FRIEND = 8
    };

    UiFriendsList();
    virtual ~UiFriendsList();
	virtual void Shutdown();

	virtual void Update();
    virtual void Draw();

	void QueueRefreshFriendsList();
	bool RefreshFriendsList();
	void RequestNewFriendsPage(const int localGamerIndex);

    void SetAllowInvites(const bool allowInvites);
    bool InvitesAllowed() const;

    int GetFriendIndex() const;

    const rlFriend* GetSelectedFriend() const;

    bool GetJoinSessionInfo(rlSessionInfo *info) const;

	rlFriend* GetFriends() { return m_FriendPage.m_Friends; }
	unsigned GetNumFriends() { return m_FriendPage.m_NumFriends; }

	virtual bool OnPageLeft();
	virtual bool OnPageRight();

	bool GetDisplayNames(const int localGamerIndex, netStatus* status);

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_Back;

	UiLabel m_CurrentPage;
	UiLabel m_PrevPage;
	UiLabel m_NextPage;
	UiLabel m_Segue;

    UiLabel m_InviteToSession[MAX_FRIEND];
    UiLabel m_InviteToClan[MAX_FRIEND];
    UiLabel m_Profile[MAX_FRIEND];
    UiLabel m_JoinSession[MAX_FRIEND];
	UiLabel m_SendMessage[MAX_FRIEND];
	UiLabel m_SubscribeToRTA[MAX_FRIEND];
	UiLabel m_UnsubscribeToRTA[MAX_FRIEND];
	UiLabel m_IsFriend[MAX_FRIEND];
	UiLabel m_RemoveFriend[MAX_FRIEND];
	UiMultiValue m_FriendLabels[MAX_FRIEND];

	UiLabel m_GetSessions;
	UiLabel m_GetDisplayNames;
	UiLabel m_Refresh;
	UiLabel m_SearchFriends;

    UiList  m_Selections;

	rlFriendsPage m_FriendPage;

	netStatus m_GetFriendsStatus;

    bool m_AllowInvites : 1;
	bool m_bRefreshingFriends;
	bool m_bQueueRefreshFriendsList;
};

class UiInvitesReceived : public UiElement
{
public:

    enum
    {
        ACTION_ACCEPT,
        ACTION_DECLINE,
        ACTION_SHOW_PENDING_MP_INVITES
    };

    enum
    {
        MAX_INVITES    = 20
    };

    UiInvitesReceived();
    virtual ~UiInvitesReceived();

    virtual void Draw();

    void Clear();

    void AddInvite(const char* sessionHostName,
                   const rlSessionInfo& info);

    void SetPendingMpInviteUiEnabled(const bool enabled);

    void RemoveInvite(const rlSessionInfo& info);

    bool HasInvites() const;

    const rlSessionInfo* GetSelectedSession() const;

private:
    class Invite
    {
    public:
        Invite() {m_InviterName[0] = '\0';}

        char m_InviterName[RL_MAX_NAME_BUF_SIZE];
        rlSessionInfo m_SessionInfo;
        
        UiLabel m_AcceptLabel;
        UiLabel m_DeclineLabel;
        UiMultiValue m_InviteMv;

        inlist_node<Invite> m_ListLink;
    };

    typedef inlist<Invite, &Invite::m_ListLink> ListType;

    void RefreshUi();
    const Invite* GetSelectedInvite() const;
    bool HasInviteFromSession(const rlSessionInfo& info);
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_Back;
    UiList m_InviteList;
    UiLabel m_ShowPendingMpInvites;

    Invite      m_Pile[MAX_INVITES];    //Source of instances we can put on lists
    ListType    m_FreeList;             //List of instances currently not used
    ListType    m_ActiveList;           //List of instances currently in use

    bool m_Dirty;
    bool m_PendingMpInviteUiEnabled;
};

class UiMultiplayerMainMenu : public UiElement
{
public:
    enum
    {
        ACTION_HOST_ONLINE,
        ACTION_SEARCH_ONLINE,
        ACTION_HOST_LAN,
        ACTION_SEARCH_LAN,
        ACTION_HOST_OFFLINE,
        ACTION_FRIENDS_LIST,
        ACTION_HOST_PARTY,
        ACTION_LEAVE_PARTY
    };

    UiMultiplayerMainMenu();
    virtual ~UiMultiplayerMainMenu();

    void Init(snSession* session,
                const VoiceChat* voiceChat);

    virtual void Shutdown();

    virtual void Draw();

    void UpdateEnabled(const bool hasNetwork,
                      const bool isSignedIn,
                      const bool isOnline);

private:

    void OnEvent(UiElement* el, const int evtId, void*);

    snSession* m_Session;
    const VoiceChat* m_VoiceChat;

    UiLabel m_Title;
    UiLabel m_HostOnline;
    UiLabel m_SearchOnline;
    UiLabel m_HostLan;
    UiLabel m_SearchLan;
    UiLabel m_HostOffline;
    UiLabel m_FriendsList;
    UiLabel m_HostParty;
    UiLabel m_LeaveParty;
    UiLabel m_Back;
    
    UiList m_Selections;
};

class UiHostConfig : public UiElement
{
public:
    enum
    {
        ACTION_CREATE,
    };

    UiHostConfig();
    virtual ~UiHostConfig();

    virtual void Draw();

    const rlMatchingAttributes& GetAttrs() const;

    unsigned GetNumPublicSlots() const { return m_NumPublicSlots; }
    unsigned GetNumPrivateSlots() const { return m_NumPrivateSlots; }
    unsigned GetCreateFlags() const {return m_CreateFlags;}

private:
    
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;

    UiLabel m_GtStandard;
    UiLabel m_GtRanked;
    UiMultiValue m_GameType;

    UiLabel m_GmExhib;
    UiLabel m_GmTourn;
    UiMultiValue m_GameMode;

    UiLabel m_NumPublicSlotLabels[RL_MAX_GAMERS_PER_SESSION + 1]; // + 1 for 0 public slots
	UiMultiValue m_NumPublicSlotsMv;

    UiLabel m_NumPrivateSlotLabels[5];
    UiMultiValue m_NumPrivateSlotsMv;

    unsigned m_NumPublicSlots;
    unsigned m_NumPrivateSlots;

    UiLabel m_Gpm3;
    UiLabel m_Gpm5;
    UiMultiValue m_GamesPerMatch;

    UiLabel m_Mpl7;
    UiLabel m_Mpl11;
    UiMultiValue m_MatchPointLimit;

    UiLabel m_JipEnabled;
    UiLabel m_JipDisabled;
    UiMultiValue m_Jip;

    UiLabel m_Back;
    UiLabel m_Create;

    UiList m_Selections;

    rlMatchingAttributes m_Attrs;

    unsigned m_CreateFlags;
};

class UiQueryConfig : public UiElement
{
public:
    enum
    {
        ACTION_SEARCH,
    };

    UiQueryConfig();
    virtual ~UiQueryConfig();

    virtual void Draw();

    const rlMatchingFilter& GetFilter() const;

    bool IsRanked() const;

private:

    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;

    UiLabel m_GtStandard;
    UiLabel m_GtRanked;
    UiMultiValue m_GameType;

    UiLabel m_GmExhib;
    UiLabel m_GmTourn;
    UiMultiValue m_GameMode;

    UiLabel m_GpmAny;
    UiLabel m_Gpm3;
    UiLabel m_Gpm5;
    UiMultiValue m_GamesPerMatch;

    UiLabel m_MplAny;
    UiLabel m_Mpl7;
    UiLabel m_Mpl11;
    UiMultiValue m_MatchPointLimit;

    UiLabel m_Back;
    UiLabel m_Search;

    UiList m_Selections;

    rlMatchingFilter m_Filter;
};

class UiConfigureAttrs : public UiElement
{
public:
    enum
    {
        ACTION_COMMIT,
    };

    UiConfigureAttrs();
    virtual ~UiConfigureAttrs();

    virtual void Draw();

    void SetChangeable(const bool changeable);

    void SetAttrs(const rlMatchingAttributes& attrs,
                const unsigned numPubSlots,
                const unsigned numPrivSlots);

    void GetAttrs(rlMatchingAttributes* attrs,
                unsigned* numPubSlots,
                unsigned* numPrivSlots) const;

    unsigned GetNumPublicSlots() const { return m_NumPublicSlots; }
    unsigned GetNumPrivateSlots() const { return m_NumPrivateSlots; }

private:

    void RefreshItems();

    void RefreshCommittable();
    
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;

    UiLabel m_GmExhib;
    UiLabel m_GmTourn;
    UiMultiValue m_GameMode;

    UiLabel m_NumPublicSlots0;
    UiLabel m_NumPublicSlots1;
    UiLabel m_NumPublicSlots2;
    UiLabel m_NumPublicSlots4;
	UiLabel m_NumPublicSlots16;
	UiLabel m_NumPublicSlots32;

	UiMultiValue m_NumPublicSlotsMv;

    UiLabel m_NumPrivateSlots0;
    UiLabel m_NumPrivateSlots1;
    UiLabel m_NumPrivateSlots2;
    UiLabel m_NumPrivateSlots4;
    UiMultiValue m_NumPrivateSlotsMv;

    unsigned m_NumPublicSlots;
    unsigned m_NumPrivateSlots;

    UiLabel m_Gpm3;
    UiLabel m_Gpm5;
    UiMultiValue m_GamesPerMatch;

    UiLabel m_Mpl7;
    UiLabel m_Mpl11;
    UiMultiValue m_MatchPointLimit;

    UiLabel m_Back;
    UiLabel m_Commit;

    UiList m_Selections;

    rlMatchingAttributes m_Attrs;

    bool m_Changeable   : 1;
};

class UiSessionSelect : public UiElement
{
public:

    enum
    {
        ACTION_JOIN,
        ACTION_VIEW_DETAILS,
    };

    enum
    {
        MAX_SESSIONS    = 20
    };

    UiSessionSelect();
    virtual ~UiSessionSelect();

    virtual void Draw();

    void AddSession(const int index, const char* text);

    void Clear();

    int GetSessionIndex() const;

private:

    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiList m_SessionList;
    UiLabel m_Join[MAX_SESSIONS];
    UiLabel m_View[MAX_SESSIONS];
    UiMultiValue m_Sessions[MAX_SESSIONS];
    UiLabel m_Back;
    int m_Count;
    int m_SessionIndices[MAX_SESSIONS];
};

class UiSessionDetails : public UiElement
{
public:
    enum
    {
        ACTION_JOIN,
    };

    UiSessionDetails();
    virtual ~UiSessionDetails();

    void SetSessionDetail(const rlSessionDetail& detail);

    virtual void Draw();

private:

    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;

    UiLabel m_GtLabel, m_GameType;
    UiLabel m_GmLabel, m_GameMode;
    UiLabel m_GpmLabel, m_GamesPerMatch;
    UiLabel m_MplLabel, m_MatchPointLimit;

    UiLabel m_Back;
    UiLabel m_Join;

    UiList m_Selections;
    UiList m_Details;
};

class UiLobby : public UiElement
{
public:
    enum
    {
        ACTION_LEAVE,
		ACTION_MIGRATE_HOST,
        ACTION_START,
        ACTION_FRIENDS_LIST,
        ACTION_CONFIGURE_ATTRIBUTES,
        ACTION_TOGGLE_INVITABLE,
		ACTION_GET_SESSION_DATA,
		ACTION_GET_CHANGEABLE_DATA,
		ACTION_SHOW_PROFILE,
		ACTION_ADD_FRIEND,
		ACTION_BAIL
    };

    UiLobby();
    virtual ~UiLobby();

    void Init(App* app, LiveManager* liveMgr);

    virtual void Shutdown();

    virtual void Draw();

	void SetBecomeHostOptionEnabled(const bool enabled);

    void SetInvitable(const bool invitable);

    void SetInviteToggleEnabled(const bool enabled);

    void SetBandwidth(const unsigned inboundBytesPerSecond,
                      const unsigned inboundPacketsPerSecond,
                      const unsigned outboundBytesPerSecond,
                      const unsigned outboundPacketsPerSecond);

	virtual bool OnPageLeft();
	virtual bool OnPageRight();

	rlGamerInfo GetSelection();

private:

    void OnEvent(UiElement* el, const int evtId, void*);

	App* m_App;
    LiveManager* m_LiveMgr;
    UiLabel m_Title;
    UiList m_GamerList;
    UiLabel m_GamerNames[RL_MAX_GAMERS_PER_SESSION];
    UiList m_Selections;
    UiLabel m_Leave;
	UiLabel m_MigrateHost;
	UiLabel m_FriendsList;
    UiLabel m_Start;
    UiLabel m_ConfigureAttrs;
    UiLabel m_Invitable;
    UiLabel m_Bandwidth;
	UiLabel m_GetSessionData;
	UiLabel m_GetChangeableData;
	UiLabel m_ShowProfile;
	UiLabel m_AddFriend;
	UiLabel m_Bail;

	int m_PlayerIndex;

    bool m_IsInvitable  : 1;
};

class UiGame : public UiElement
{
public:
    enum
    {
        ACTION_LEAVE,
        ACTION_END,
        ACTION_SWING,
    };

    UiGame();
    virtual ~UiGame();

    void Init(App* app);

    virtual void Shutdown();

    virtual void Draw();

private:

    void OnEvent(UiElement* el, const int evtId, void*);

    App* m_App;
    UiLabel m_Title;
    UiLabel m_Header;
    UiList m_GamerList;
    UiLabel m_GamerNames[RL_MAX_GAMERS_PER_SESSION];
    UiList m_Selections;
    UiLabel m_Leave;
    UiLabel m_End;
    UiLabel m_Swing;
};

//////////////////////////////////////////////////////////////////////////
// Social Club
//////////////////////////////////////////////////////////////////////////
class UiSocialClubMainMenu : public UiElement
{
public:
	enum
	{
		ACTION_GET_TERMS_OF_SERVICE,
        ACTION_ACCEPT_TERMS_OF_SERVICE,
		ACTION_LINK_OR_UNLINK_ACCOUNT,
		ACTION_CREATE_ACCOUNT,
		ACTION_RESET_PASSWORD,
		ACTION_GET_COUNTRIES,
        ACTION_UPDATE_PASSWORD,
		ACTION_POST_USER_FEED_ACTIVITY,
        ACTION_GET_SCAUTHTOKEN,
        ACTION_COUNT_ALL_FRIENDS,
        ACTION_READ_FRIENDS,
        ACTION_READ_FRIENDS_AND_INVITES_SENT,
        ACTION_READ_INVITES_SENT,
        ACTION_READ_INVITES_RECEIVED,
        ACTION_READ_BLOCKED,
        ACTION_CHECK_EMAIL,
        ACTION_CHECK_NICKNAME,
		ACTION_GO_ONLINE,
		ACTION_GO_OFFLINE,
		ACTION_POST_SC_TELEMETRY,
		ACTION_DEVICE_LOST_RESET,
		ACTION_CREATE_SC_AUTH_TOKEN,
		ACTION_CREATE_SIGNIN_TRANSFER,
	};

	UiSocialClubMainMenu();
	virtual ~UiSocialClubMainMenu();
	virtual void Draw();

private:
	void OnEvent(UiElement* el, const int evtId, void*);

	UiLabel m_Title;
	UiLabel m_GetTermsOfService;
    UiLabel m_AcceptTermsOfService;
	UiLabel m_LinkOrUnlinkAccount;
    UiLabel m_CheckEmail;
    UiLabel m_CheckNickname;
	UiLabel m_CreateAccount;
	UiLabel m_ResetPassword;
	UiLabel m_GetCountries;
    UiLabel m_UpdatePassword;
	UiLabel m_PostUserFeedActivity;
    UiLabel m_GetScAuthToken;
    UiLabel m_CountAllFriends;   
    UiLabel m_ReadFriends;
    UiLabel m_ReadFriendsAndInvitesSent;
    UiLabel m_ReadInvitesSent;
    UiLabel m_ReadInvitesReceived;
    UiLabel m_ReadBlocked;
	UiLabel m_GoOnline;
	UiLabel m_Goffline;
	UiLabel m_PostScTelemetry;
	UiLabel m_DeviceLostReset;
	UiLabel m_CreateScAuthToken;
	UiLabel m_CreateSignInTransfer;

	UiLabel m_Back;

	UiList m_Selections;
};

//////////////////////////////////////////////////////////////////////////
// Commerce and entitlements
//////////////////////////////////////////////////////////////////////////
class UiCommerceAndEntitlementsMainMenu : public UiElement
{
public:
	enum
	{
		ACTION_START_COMMERCE,
		ACTION_GET_COMMERCE_DATA,
		ACTION_DUMP_COMMERCE,
		ACTION_SHUTDOWN_COMMERCE,
		ACTION_SUBMIT_TEST_VOUCHER_FOR_CONTENT,
		ACTION_CONSUME_PREVIEWED_VOUCHER,
		ACTION_TEST_CHECKOUT_URL_FETCH,
		ACTION_TEST_AGE_FOR_STORE,
		ACTION_START_STEAM_TEST_PURCHASE,
		ACTION_REQUEST_ENTITLEMENT_DATA,
		ACTION_DUMP_ENTITLEMENT_DATA
	};

	UiCommerceAndEntitlementsMainMenu();
	virtual ~UiCommerceAndEntitlementsMainMenu();
	virtual void Draw();

private:
	void OnEvent(UiElement* el, const int evtId, void*);

	UiLabel m_Title;
	UiLabel m_StartCommerce;
	UiLabel m_GetCommerceData;
	UiLabel m_DumpCommerceData;
	UiLabel m_ShutdownCommerce;
	UiLabel m_SubmitTestVoucherForContent;
	UiLabel m_ConsumeTestVoucherForContent;
	UiLabel m_TestFetchCheckoutUrl;
	UiLabel m_TestGetAgeAppropriateForStore;
	UiLabel m_StartSteamTestPurchase;
	UiLabel m_RequestEntitlementData;
	UiLabel m_DumpEntitlementData;


	UiLabel m_Back;

	UiList m_Selections;
};

//////////////////////////////////////////////////////////////////////////
// Cloud
//////////////////////////////////////////////////////////////////////////
class UiCloudMainMenu : public UiElement
{
public:
    enum
    {
        ACTION_MEMBER_SC_DOWNLOAD,
        ACTION_MEMBER_SC_UPLOAD,
        ACTION_MEMBER_SC_APPEND,
        ACTION_MEMBER_SC_DELETE,
        ACTION_MEMBER_NATIVE_DOWNLOAD,
        ACTION_MEMBER_NATIVE_UPLOAD,
        ACTION_MEMBER_NATIVE_APPEND,
        ACTION_MEMBER_NATIVE_DELETE,
        ACTION_TITLE_DOWNLOAD,

		// SP Saves
		ACTION_SPSAVES_INIT,
		ACTION_SPSAVES_IDENTIFY_CONFLICTS,
		ACTION_SPSAVES_REGISTER_FILE,
		ACTION_SPSAVES_UNREGISTER_FILE,
		ACTION_SPSAVES_GET_FILE,
		ACTION_SPSAVES_POST_FILE,
		ACTION_SPSAVES_MERGE_FILES,
		ACTION_SPSAVES_DELETE_FILES,
		ACTION_SPSAVES_QUERY_BETA,
		ACTION_SPSAVES_WRITE_TELEMETRY,
		ACTION_SPSAVES_BACKUP_CONFLICTS,
		ACTION_SPSAVES_SEARCH_BACKUPS,
		ACTION_SPSAVES_RESTORE_BACKUP,
		ACTION_SPSAVES_GET_BACKUP_INFO,
		ACTION_SPSAVES_GET_METADATA
    };

    enum AccessType
    {
        ACCESS_PRIVATE,
        ACCESS_SECURE,  //read only
        ACCESS_SHARE,
        ACCESS_PUBLISH
    };

    UiCloudMainMenu();
    virtual ~UiCloudMainMenu();

    virtual void Draw();

    AccessType GetAccessType() const;

	void EnableCloudSaveOperations();

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;

    UiLabel m_MembersLabel;

    UiLabel m_PrivateFolder;
    UiLabel m_SecureFolder;
    UiLabel m_ShareFolder;
    UiLabel m_PublishFolder;
    UiMultiValue m_AccessFolder;

    UiLabel m_MembersScLabel;
    UiLabel m_MembersScDownload;
    UiLabel m_MembersScUpload;
    UiLabel m_MembersScAppend;
    UiLabel m_MembersScDelete;
    UiLabel m_MembersNativeLabel;
    UiLabel m_MembersNativeDownload;
    UiLabel m_MembersNativeUpload;
    UiLabel m_MembersNativeAppend;
    UiLabel m_MembersNativeDelete;

    UiLabel m_TitlesLabel;
    UiLabel m_TitlesDownload;

	UiLabel m_SpCloudLabel;
	UiLabel m_SpCloudInit;
	UiLabel m_SpCloudIdentifyConflicts;
	UiLabel m_SpCloudRegisterFile;
	UiLabel m_SpCloudUnregisterFile;
	UiLabel m_SpCloudGetFile;
	UiLabel m_SpCloudPostFile;
	UiLabel m_SpCloudBackupConflicts;
	UiLabel m_SpCloudSearchBackups;
	UiLabel m_SpCloudGetBackupInfo;
	UiLabel m_SpCloudRestoreBackup;
	UiLabel m_SpCloudMergeFile;
	UiLabel m_SpCloudDeleteFiles;
	UiLabel m_SpCloudQueryBeta;
	UiLabel m_SpCloudTelemetryEvent;
	UiLabel m_SpCloudGetMetadata;

    UiLabel m_Back;

    UiList m_Selections;
};

//////////////////////////////////////////////////////////////////////////
// Ugc
//////////////////////////////////////////////////////////////////////////
class UiUgcMainMenu : public UiElement
{
public:
    enum
    {
        ACTION_CREATE_VIDEO_CONTENT,
        ACTION_CREATE_CONTENT,
        ACTION_GET_BOOKMARKED,
        ACTION_GET_FRIENDS,
        ACTION_GET_MINE,
        ACTION_GET_ROCKSTAR_CREATED,
        ACTION_GET_TOP_RATED,
        ACTION_GET_TOP_RATED_CREATORS
    };

    UiUgcMainMenu();
    virtual ~UiUgcMainMenu();

    virtual void Draw();

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_CreateVideoContent;
    UiLabel m_CreateContent;
    UiLabel m_GetBookmarked;
    UiLabel m_GetFriends;
    UiLabel m_GetMine;
    UiLabel m_GetRockstarCreated;
    UiLabel m_GetTopRated;
    UiLabel m_GetTopRatedCreators;
    UiLabel m_Back;

    UiList m_Selections;
};

//////////////////////////////////////////////////////////////////////////
// UgcContentList
//////////////////////////////////////////////////////////////////////////
class UiUgcContentList : public UiElement
{
public:
    enum
    {
        ACTION_BOOKMARK,
        ACTION_COPY_CONTENT,
        ACTION_DELETE,
        ACTION_DETAILS,
        ACTION_PLAYERS,
        ACTION_RATE_CONTENT_0PCT,
        ACTION_RATE_CONTENT_25PCT,
        ACTION_RATE_CONTENT_50PCT,
        ACTION_RATE_CONTENT_75PCT,
        ACTION_RATE_CONTENT_100PCT,
        ACTION_SET_PLAYER_DATA,
        ACTION_CLEAR_PLAYER_DATA,
        ACTION_SET_PUBLISHED,
        ACTION_UPDATE_CONTENT,
        ACTION_VERSION_CONTENT,
    };

    UiUgcContentList();
    virtual ~UiUgcContentList();

    //void SetContent(const rlUgcQueryContentResults& results);
    int GetSelectedContentIndex() const;

    virtual void Draw();

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    enum { MAX_CONTENT = 20 };

    UiLabel m_Title;
    UiLabel m_Bookmark[MAX_CONTENT];
    UiLabel m_CopyContent[MAX_CONTENT];
    UiLabel m_Delete[MAX_CONTENT];
    UiLabel m_Details[MAX_CONTENT];
    UiLabel m_Players[MAX_CONTENT];
    UiLabel m_Rate0[MAX_CONTENT];
    UiLabel m_Rate25[MAX_CONTENT];
    UiLabel m_Rate50[MAX_CONTENT];
    UiLabel m_Rate75[MAX_CONTENT];
    UiLabel m_Rate100[MAX_CONTENT];
    UiLabel m_SetPlayerData[MAX_CONTENT];
    UiLabel m_ClearPlayerData[MAX_CONTENT];
    UiLabel m_SetPublished[MAX_CONTENT];
    UiLabel m_UpdateContent[MAX_CONTENT];
    UiLabel m_VersionContent[MAX_CONTENT];
    UiMultiValue m_Content[MAX_CONTENT];
    UiList  m_Selections;

    UiLabel m_Back;
};

//////////////////////////////////////////////////////////////////////////
// UgcPlayerList
//////////////////////////////////////////////////////////////////////////
class UiUgcPlayerList : public UiElement
{
public:
    enum
    {
        ACTION_DETAILS
    };

    UiUgcPlayerList();
    virtual ~UiUgcPlayerList();

    //void SetContent(const rlUgcQueryContentResultBase* result);
    int GetSelectedPlayerIndex() const;

    virtual void Draw();

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    enum { MAX_PLAYERS = 10 };

    UiLabel m_Title;
    UiLabel m_Players[MAX_PLAYERS];
    UiList  m_Selections;

    UiLabel m_Back;
};

//////////////////////////////////////////////////////////////////////////
// UiUgcContentDetails
//////////////////////////////////////////////////////////////////////////
class UiUgcContentDetails : public UiElement
{
public:
    UiUgcContentDetails();
    virtual ~UiUgcContentDetails();

    //void SetContent(const rlUgcQueryContentResultBase* result);
    void SetContent(const rlUgcMetadata& m);

    virtual void Draw();

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_AccountId;
    UiLabel m_Category;
    UiLabel m_CloudAbsPath;
    UiLabel m_ContentId;
    UiLabel m_ContentName;
    UiLabel m_CreatedDate;
    UiLabel m_Data;
    UiLabel m_Description;
    UiLabel m_IsPublished;
    UiLabel m_RockstarId;
    UiLabel m_Stats;
    UiLabel m_UserId;
    UiLabel m_UserName;
    UiLabel m_Version;
    UiList  m_Selections;

    UiLabel m_Back;
};

//////////////////////////////////////////////////////////////////////////
// Presence
//////////////////////////////////////////////////////////////////////////
class UiPresenceMainMenu : public UiElement
{
public:
    enum
    {
        ACTION_PUBLISH_TO_FRIENDS,
        ACTION_PUBLISH_TO_CREW,
        ACTION_EXECUTE_QUERY,
        ACTION_QUERY_COUNT
    };

    UiPresenceMainMenu();
    virtual ~UiPresenceMainMenu();

    const char* GetSelectedQuery() const;
    bool GetSelectedQueryParams(const int localGamerIndex,
                                char* buf,
                                size_t sizeofBuf) const;
    template<size_t SIZE>
    bool GetSelectedQueryParams(const int localGamerIndex,
                                char (&buf)[SIZE]) const
    {
        return GetSelectedQueryParams(localGamerIndex, buf, SIZE);
    }

    virtual void Draw();

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    enum
    {
        MAX_QUERIES = 10
    };

    UiLabel m_Title;

    UiLabel m_PublishToFriends;
    UiLabel m_PublishToCrew;
    UiMultiValue m_ExecuteQuery;
    UiMultiValue m_QueryCount;

    UiLabel m_ExecuteQueryNames[MAX_QUERIES];
    UiLabel m_QueryCountNames[MAX_QUERIES];

    UiLabel m_Back;

    UiList m_Selections;

#if !RSG_ORBIS
    char m_QueryParamBuf[128];
#endif
};

//////////////////////////////////////////////////////////////////////////
// Rich Presence
//////////////////////////////////////////////////////////////////////////
#define MAX_FIELDID 5
#define MAX_PRESENCEID 7

class UiRichPresenceMainMenu : public UiElement
{
public:
	enum
	{
		ACTION_SET_RICH_PRESENCE,
		ACTION_GET_RICH_PRESENCE
	};

	UiRichPresenceMainMenu();
	virtual ~UiRichPresenceMainMenu();

	virtual void Draw();

	char m_PresenceBuf[RLSC_PRESENCE_STRING_MAX_SIZE];
	char m_FieldBuf[RLSC_PRESENCE_STRING_MAX_SIZE];

	const char * GetFieldString()
	{
		return m_FieldBuf;
	}

	const char * GetPresenceString() 
	{
		return m_PresenceBuf;
	}

private:
	
	void OnEvent(UiElement* el, const int evtId, void*);

	UiLabel m_Title;
	UiLabel m_SetString;
	UiLabel m_GetString;
	UiLabel m_PresenceString;
	UiLabel m_FieldString;

	UiLabel m_Back;
	UiList m_Selections;

	bool m_bUpdatingPresenceStr;
	bool m_bUpdatingFieldStr;

	char m_VirtualKeyboardResult[ioVirtualKeyboard::MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD * 3]; // (max number of utf8 chars from utf16)
};

//////////////////////////////////////////////////////////////////////////
// Achievements
//////////////////////////////////////////////////////////////////////////
class UiAchievementsMainMenu : public UiElement
{
public:
	enum
	{
		ACTION_AWARD_ACHIEVEMENT,
		ACTION_READ_ACHIEVEMENTS,
		ACTION_DELETE_ACHIEVEMENT,
        ACTION_GET_ROS_ACHIEVEMENTS,
        ACTION_GET_ROS_ACHIEVEMENT_DEFINITIONS
	};

	UiAchievementsMainMenu();
	virtual ~UiAchievementsMainMenu();
	virtual void Draw();

	virtual bool OnLeft();
	virtual bool OnRight();

	static int GetAchievementIndex() { return m_AchievementIndex; }

private:
	void OnEvent(UiElement* el, const int evtId, void*);

	UiLabel m_Title;
	UiLabel m_AwardAchievement;
	UiLabel m_ReadAchievements;
	UiLabel m_DeleteAchievements;
    UiLabel m_GetRosAchievements;
    UiLabel m_GetRosAchievementDefinitions;

#if RSG_DURANGO
	UiLabel m_SetStat;
	UiLabel m_ReadStat;
#endif

	UiLabel m_Back;
	UiList m_Selections;

	static const int MAX_ACHIEVEMENTS = 100;
	static int m_AchievementIndex;
	bool m_bWaitingForKeyboard;

#if RSG_DURANGO
	int m_IntStat;
	netStatus statStatus;

	char m_Stat[256];
	char sm_VirtualKeyboardResult[ioVirtualKeyboard::MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD * 3]; // (max number of utf8 chars from utf16)
#endif
};

//////////////////////////////////////////////////////////////////////////
// Profile Stats
//////////////////////////////////////////////////////////////////////////
class UiProfileStatsMainMenu : public UiElement
{
public:
    enum
    {
        ACTION_SYNCHRONIZE,
        ACTION_READ_BY_GAMER,
        ACTION_SET_DIRTY,
        ACTION_FLUSH,
		ACTION_SHOW_RESET_BY_GROUP_MENU,
    };

    UiProfileStatsMainMenu();
    virtual ~UiProfileStatsMainMenu();

    virtual void Draw();

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_ProfileStatsSynchronize;
    UiLabel m_ProfileStatsReadByGamer;
    UiLabel m_ProfileStatsSetDirty;
    UiLabel m_ProfileStatsFlush;
	UiLabel m_ProfileStatsResetByGroup;
    UiLabel m_Back;

    UiList m_Selections;
};

class UiProfileStatsResetByGroupMenu : public UiContainer
{
public:
	enum
	{
		ACTION_GROUP,
		ACTION_RESET,
	};

	UiProfileStatsResetByGroupMenu();
	virtual ~UiProfileStatsResetByGroupMenu();

	virtual void Draw();

	int GetGroupId()
	{
		return m_GroupNumberToReset.GetValue();
	}

private:
	void OnEvent(UiElement* el, const int evtId, void*);

	UiLabel m_Title;

	UiSpinner m_GroupNumberToReset;
	UiLabel m_ResetStatsGroupForPlayer;
	UiLabel m_Back;

	UiList m_Selections;
};

//////////////////////////////////////////////////////////////////////////
// Community Stats
//////////////////////////////////////////////////////////////////////////
class UiCommunityStatsMainMenu : public UiElement
{
public:
    enum
    {
        ACTION_READ,
        ACTION_READ_HISTORY,
        ACTION_READ_HISTORY_SET,
    };

    UiCommunityStatsMainMenu();
    virtual ~UiCommunityStatsMainMenu();

    virtual void Draw();

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_ReadStats;
    UiLabel m_ReadHistory;
    UiLabel m_ReadHistorySet;
    UiLabel m_Back;

    UiList m_Selections;
};

//////////////////////////////////////////////////////////////////////////
// Clans
//////////////////////////////////////////////////////////////////////////

class UiClansMainMenu : public UiElement
{
public:
    enum
    {
        ACTION_LEAVE,
        ACTION_SHOW_MEMBERS,
        ACTION_SHOW_RANKS,
        ACTION_SHOW_REQUESTS,
        ACTION_SHOW_SENT_REQUESTS,
        ACTION_SHOW_INVITES,
        ACTION_SHOW_MESSAGES,
        ACTION_SHOW_METADATA,
        ACTION_SHOW_FEUD_STAT,
        ACTION_SHOW_ALL,
        ACTION_SET_PRIMARY,
        ACTION_GET_PRIMARY,
        ACTION_CREATE,
    };

    enum
    {
        MAX_MEMBERSHIP_PER_PAGE  = 25
    };

    UiClansMainMenu();
    virtual ~UiClansMainMenu();

    virtual void Draw();

    void Populate(const rlClanMembershipData* mine, 
                  const unsigned myCount);

    int GetSelectedIndex() const;

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_Back;

    UiMultiValue m_Clans[MAX_MEMBERSHIP_PER_PAGE];
    UiLabel m_Leave[MAX_MEMBERSHIP_PER_PAGE];
    UiLabel m_ShowMembers[MAX_MEMBERSHIP_PER_PAGE];
    UiLabel m_ShowRanks[MAX_MEMBERSHIP_PER_PAGE];
    UiLabel m_ShowRequests[MAX_MEMBERSHIP_PER_PAGE];
    UiLabel m_ShowMessages[MAX_MEMBERSHIP_PER_PAGE];
    UiLabel m_ShowMetadata[MAX_MEMBERSHIP_PER_PAGE];
    UiLabel m_SetPrimary[MAX_MEMBERSHIP_PER_PAGE];
    UiLabel m_FeudStat[MAX_MEMBERSHIP_PER_PAGE];
    UiLabel m_ShowInvites;
    UiLabel m_ShowSentRequests;
    UiLabel m_ShowAll;
    UiLabel m_Create;
    UiLabel m_GetPrimary;

    UiList m_Selections;
};

class UiClanInviteFriendMenu : public UiElement
{
public:
    enum
    {
        ACTION_INVITE,
    };

    enum
    {
        MAX_MEMBERSHIP_PER_PAGE  = 25
    };

    UiClanInviteFriendMenu();
    virtual ~UiClanInviteFriendMenu();

    virtual void Draw();

    void Populate(const rlClanMembershipData* mine, 
                  const unsigned myCount);

    int GetSelectedIndex() const;

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_Back;

    UiMultiValue m_Clans[MAX_MEMBERSHIP_PER_PAGE];
    UiLabel m_Invite[MAX_MEMBERSHIP_PER_PAGE];
    UiLabel m_Create;

    UiList m_Selections;
};

class UiClansAllMenu : public UiElement
{
public:
    enum
    {
        ACTION_SHOW_MEMBERS,
        ACTION_SHOW_RANKS,
        ACTION_SHOW_MESSAGES,
        ACTION_SHOW_METADATA,
        ACTION_SHOW_FEUD,
        ACTION_JOIN_REQUEST,
    };

    enum
    {
        MAX_CLAN_PER_PAGE  = 25
    };

    UiClansAllMenu();
    virtual ~UiClansAllMenu();

    virtual void Draw();

    void Populate(const rlClanDesc* clans,
                  const rlClanLeader* leaders,
                  const unsigned clanCount);

    int GetSelectedIndex() const;

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_Back;

    UiMultiValue m_Clans[MAX_CLAN_PER_PAGE];
    UiLabel m_ShowMembers[MAX_CLAN_PER_PAGE];
    UiLabel m_ShowRanks[MAX_CLAN_PER_PAGE];
    UiLabel m_ShowMessages[MAX_CLAN_PER_PAGE];
    UiLabel m_ShowMetadata[MAX_CLAN_PER_PAGE];
    UiLabel m_JoinRequest[MAX_CLAN_PER_PAGE];
    UiLabel m_FeudStat[MAX_CLAN_PER_PAGE];

    UiList m_Selections;
};

class UiClanMembersMenu : public UiElement
{
public:
    enum
    {
        ACTION_KICK,
        ACTION_PROMOTE,
        ACTION_DEMOTE,
    };

    enum
    {
        MAX_MEMBERS_PER_PAGE  = 25
    };

    UiClanMembersMenu();
    virtual ~UiClanMembersMenu();

    virtual void Draw();

    void Populate(const rlClanDesc& activeClan,
                  const rlClanMembershipData* mine,
                  const unsigned myCount,
                  const rlClanMember* members,
                  const unsigned numMembers);

    int GetSelectedIndex() const;

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_Back;

    UiMultiValue m_Members[MAX_MEMBERS_PER_PAGE];
    UiLabel m_Kick[MAX_MEMBERS_PER_PAGE];
    UiLabel m_Promote[MAX_MEMBERS_PER_PAGE];
    UiLabel m_Demote[MAX_MEMBERS_PER_PAGE];

    UiList  m_Selections;
};

class UiClanInvitesMenu : public UiElement
{
public:
    enum
    {
        ACTION_ACCEPT_INVITE,
        ACTION_DELETE_INVITE,
    };

    enum
    {
        MAX_INVITES_PER_PAGE  = 10
    };

    UiClanInvitesMenu();
    virtual ~UiClanInvitesMenu();

    virtual void Draw();

    void Populate(const rlClanInvite* invites,
                  const unsigned numInvites);

    int GetSelectedIndex() const;

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_Back;

    UiMultiValue m_Invites[MAX_INVITES_PER_PAGE];
    UiLabel m_Accept[MAX_INVITES_PER_PAGE];
    UiLabel m_Delete[MAX_INVITES_PER_PAGE];

    UiList m_Selections;
};

class UiClanWallMessagesMenu : public UiElement
{
public:
    enum
    {
        ACTION_WRITE_MESSAGE,
        ACTION_DELETE_MESSAGE,
    };

    enum
    {
        MAX_MESSAGES_PER_PAGE  = 10
    };

    UiClanWallMessagesMenu();
    virtual ~UiClanWallMessagesMenu();

    virtual void Draw();

    void Populate(const rlClanDesc& activeClan,
                  const rlClanMembershipData* mine,
                  const unsigned myCount,
                  const rlClanWallMessage* messages,
                  const unsigned numMessages);

    int GetSelectedIndex() const;

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_Back;

    UiMultiValue m_Messages[MAX_MESSAGES_PER_PAGE];
    UiLabel m_Delete[MAX_MESSAGES_PER_PAGE];
    UiLabel m_WriteMessage;

    UiList m_Selections;
};

class UiClanRanksMenu : public UiElement
{
public:
    enum
    {
        ACTION_CREATE,
        ACTION_DELETE,
        ACTION_MOVE_UP,
        ACTION_MOVE_DOWN,
        ACTION_TOGGLE_MANAGER,
        ACTION_TOGGLE_RECRUITER,
        ACTION_TOGGLE_DESCRUCTIVE,
    };

    enum
    {
        MAX_RANKS_PER_PAGE  = 10
    };

    UiClanRanksMenu();
    virtual ~UiClanRanksMenu();

    virtual void Draw();

    void Populate(const rlClanDesc& activeClan,
                  const rlClanMembershipData* mine,
                  const unsigned myCount,
                  const rlClanRank* ranks,
                  const unsigned numRanks);

    int GetSelectedIndex() const;

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_Back;

    UiMultiValue m_Ranks[MAX_RANKS_PER_PAGE];
    UiLabel m_Delete[MAX_RANKS_PER_PAGE];
    UiLabel m_MoveUp[MAX_RANKS_PER_PAGE];
    UiLabel m_MoveDown[MAX_RANKS_PER_PAGE];
    UiLabel m_ToggleManager[MAX_RANKS_PER_PAGE];
    UiLabel m_ToggleRecruiter[MAX_RANKS_PER_PAGE];
    UiLabel m_ToggleDestructive[MAX_RANKS_PER_PAGE];
    UiLabel m_Create;


    UiList m_Selections;
};

class UiClanRequestsMenu : public UiElement
{
public:
    enum
    {
        ACTION_DELETE
    };

    enum
    {
        MAX_REQUESTS_PER_PAGE  = 10
    };

    UiClanRequestsMenu();
    virtual ~UiClanRequestsMenu();

    virtual void Draw();

    void Populate(const rlClanDesc& activeClan,
                  const rlClanJoinRequestRecieved* requests,
                  const unsigned numRequests);

    int GetSelectedIndex() const;

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_Back;

    UiMultiValue m_Requests[MAX_REQUESTS_PER_PAGE];
    UiLabel m_Delete[MAX_REQUESTS_PER_PAGE];

    UiList m_Selections;
};

class UiClanSentRequestsMenu : public UiElement
{
public:
    enum
    {
        ACTION_DELETE
    };

    enum
    {
        MAX_REQUESTS_PER_PAGE  = 10
    };

    UiClanSentRequestsMenu();
    virtual ~UiClanSentRequestsMenu();

    virtual void Draw();

    void Populate(const rlClanJoinRequestSent* requests,
                  const unsigned numRequests);

    int GetSelectedIndex() const;

private:
    void OnEvent(UiElement* el, const int evtId, void*);

    UiLabel m_Title;
    UiLabel m_Back;

    UiMultiValue m_Requests[MAX_REQUESTS_PER_PAGE];
    UiLabel m_Delete[MAX_REQUESTS_PER_PAGE];

    UiList m_Selections;
};
//////////////////////////////////////////////////////////////////////////
// Facebook
//////////////////////////////////////////////////////////////////////////
class UiFacebookMainMenu : public UiElement
{
public:
	enum
	{
		ACTION_GETACCESSTOKEN_UI,
		ACTION_GETACCESSTOKEN_NO_UI,
		ACTION_POSTOPENGRAPH
	};

	UiFacebookMainMenu();
	virtual ~UiFacebookMainMenu();
	virtual void Draw();

private:
	void OnEvent(UiElement* el, const int evtId, void*);

	UiLabel m_Title;
	UiLabel m_GetAccessTokenUi;
	UiLabel m_GetAccessTokenNoUi;
	UiLabel m_PostOpenGraph;
	UiLabel m_CheckPermissions;
	UiLabel m_Browser;

	UiLabel m_Back;

	UiList m_Selections;
};

//////////////////////////////////////////////////////////////////////////
// System Parties
//////////////////////////////////////////////////////////////////////////

class UiPartyMainMenu : public UiElement
{
public:
	enum
	{
		ACTION_SETACTIVETITLE,
		ACTION_INVITETOPARTY,
		ACTION_CREATEPARTY
	};

	enum
	{
		MAX_PARTY = 32
	};

	UiPartyMainMenu();
	virtual ~UiPartyMainMenu();
	virtual void Draw();

	void RefreshParty();

private:
	void OnEvent(UiElement* el, const int evtId, void*);

	UiLabel m_Title;
	UiLabel m_Back;

#if RSG_DURANGO
	UiLabel m_SetActiveTitle;
#endif

	UiLabel m_CreateParty;
	UiLabel m_InviteFriendToParty;

	UiLabel m_PartyMembers[MAX_PARTY];

	UiList m_Selections;

	int m_PartySize;
};

//////////////////////////////////////////////////////////////////////////
// Entitlement
//////////////////////////////////////////////////////////////////////////

class UiEntitlementMenu : public UiElement
{
public:
	enum
	{
		ACTION_LOAD_ENTITLEMENT,
		ACTION_SAVE_ENTITLEMENT,
		ACTION_CLEAR_ENTITLEMENT,
		ACTION_LOAD_ENTITLEMENT_DLL,
		ACTION_SAVE_ENTITLEMENT_DLL,
		ACTION_CLEAR_ENTITLEMENT_DLL
	};

	UiEntitlementMenu();
	virtual ~UiEntitlementMenu();
	virtual void Draw();

private:
	void OnEvent(UiElement* el, const int evtId, void*);

	UiLabel m_Title;
	UiLabel m_Back;

	UiLabel m_SaveEntitlement;
	UiLabel m_LoadEntitlement;
	UiLabel m_ClearEntitlement;

	UiLabel m_SaveEntitlementDll;
	UiLabel m_LoadEntitlementDll;
	UiLabel m_ClearEntitlementDll;

	UiList m_Selections;
};

//////////////////////////////////////////////////////////////////////////
// Live Stream
//////////////////////////////////////////////////////////////////////////

class UiLiveStreamMenu : public UiElement
{
public:
	enum
	{
		ACTION_INIT,
		ACTION_SHUTDOWN,
		ACTION_AUTHENTICATE,
		ACTION_START_STREAMING,
		ACTION_STOP_STREAMING,
		ACTION_PAUSE_STREAMING,

		MAX_INGEST_SERVERS = 20
	};

	UiLiveStreamMenu();
	virtual ~UiLiveStreamMenu();
	virtual void Draw();

	virtual void UpdateEnabled();
	virtual void UpdateIngestionServers();

	int GetIngestServerIndex();
	int GetTargetWidth();
	int GetTargetHeight();
	int GetTargetFPS();

private:
	void OnEvent(UiElement* el, const int evtId, void*);

	UiLabel m_Title;

	UiLabel m_Init;
	UiLabel m_Shutdown;
	UiLabel m_Authenticate;

	UiLabel m_StreamSettings;

	UiLabel m_ServerNames[MAX_INGEST_SERVERS];
	UiMultiValue m_IngestServers;

	UiLabel m_StartStreaming;
	UiLabel m_StopStreaming;
	UiLabel m_PauseStreaming;

	UiLabel m_Back;
	UiList m_Selections;

};


//////////////////////////////////////////////////////////////////////////
// YouTube Uploading
//////////////////////////////////////////////////////////////////////////

class UiYoutubeMenu : public UiElement
{
public:
	enum
	{
		ACTION_INIT,
		ACTION_LINK_ACCOUNT,
		ACTION_LOAD_VIDEO,
		ACTION_GET_OAUTH,
		ACTION_GET_URL,
		ACTION_CLEAR_URL,
		ACTION_UPLOAD,
		ACTION_GET_CHANNEL_INFO,
		ACTION_GET_VIDEO_INFO
	};

	UiYoutubeMenu();
	virtual ~UiYoutubeMenu();
	virtual void Draw();

	void SetTitle(const char * title) { m_Title.SetText(title); }
	netStatus* GetStatus() { return &m_YoutubeStatus; }
	rlYoutubeUpload* GetUpload() { return &m_YoutubeUpload; }
	bool AllocateData();
	u8* GetUploadData() { return m_Data; }

private:
	void OnEvent(UiElement* el, const int evtId, void*);

	UiLabel m_Title;

	UiLabel m_AccountLink;
	UiLabel m_LoadVideo;
	UiLabel m_OAuth;
	UiLabel m_GetUrl;
	UiLabel m_ClearUrl;
	UiLabel m_Upload;
	UiLabel m_ChannelInfo;
	UiLabel m_VideoInfo;

	UiLabel m_Back;
	UiList m_Selections;

	rlYoutubeUpload m_YoutubeUpload;
	netStatus m_YoutubeStatus;
	u8* m_Data;
};


}   //namespage rage

#endif  //SAMPLE_SNET_UI_H
