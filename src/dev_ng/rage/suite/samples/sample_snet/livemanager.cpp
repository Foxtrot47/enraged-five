// 
// sample_snet/livemanager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "livemanager.h"

#include "peermanager.h"
#include "net/net.h"

#if __XENON
#include "system/xtl.h"
#include <XAudio2.h>
#endif

#if RSG_DURANGO
#include "rline/durango/rlxbl_interface.h"
#include "durango/Events_durango.h"
#endif

#if RSG_ORBIS
#include "file/asset.h"
#include "rline/rlnp.h"
#endif

namespace rage
{

LiveManager::LiveManager()
    : m_Allocator(0)
    , m_CxnMgr(0)
    , m_PendingStatus(0)
    , m_State(STATE_NONE)
    , m_Initialized(false)
{
    m_SessionDlgt.Bind(this, &LiveManager::OnSessionEvent);
}

void
LiveManager::Init(sysMemAllocator* allocator,
                    netConnectionManager* cxnMgr,
                    const snSessionOwner& owner,
                    void* voiceChatDevice)
{
    Assert(allocator);
    Assert(cxnMgr);

    if(AssertVerify(!m_Initialized))
    {
        m_Allocator = allocator;
        m_CxnMgr = cxnMgr;
		m_Owner = owner;

		// initialize the session
		InitSession();

		m_Session.SetPresenceAttributeNames(rlScAttributeId::GameSessionToken.Name,
													  rlScAttributeId::GameSessionId.Name,
													  rlScAttributeId::GameSessionInfo.Name,
													  rlScAttributeId::IsGameHost.Name,
													  rlScAttributeId::IsGameJoinable.Name,
													  false);

        m_Session.AddDelegate(&m_SessionDlgt);

        //Make sure the voice channel is plaintext.
        netChannelPolicies channelPolicies;
        cxnMgr->GetChannelPolicies(VOICE_CHANNEL_ID, &channelPolicies);
        channelPolicies.m_CompressionEnabled = false;
        cxnMgr->SetChannelPolicies(VOICE_CHANNEL_ID, channelPolicies);


        AssertVerify(m_VoiceChat.Init(VoiceChatTypes::MAX_LOCAL_TALKERS,
                                        VoiceChatTypes::MAX_REMOTE_TALKERS,
                                        voiceChatDevice,
                                        RLINE_CPU_AFFINITY));

#if __BANK
		m_VoiceChat.InitWidgets();
#endif

        AssertVerify(m_VoiceChat.InitNetwork(cxnMgr, VOICE_CHANNEL_ID));

#if RSG_PC
		// apply preferences
		bool voiceChatEnabled = true;
		bool talkingEnabled = true;
		u32 outputVolume = 100;
		u32 microphoneVolume = 100;
		u32 outputDeviceId = 0; // default device as set up in Windows
		u32 inputDeviceId = 0; // default device as set up in Windows

		if(voiceChatEnabled)
		{
			m_VoiceChat.SetPlaybackDeviceById32(outputDeviceId);

			if(talkingEnabled)
			{
				m_VoiceChat.SetCaptureDeviceById32(inputDeviceId);
			}
		}

		m_VoiceChat.SetPlaybackDeviceVolume((float)outputVolume / 100.0f);
		m_VoiceChat.SetCaptureDeviceVolume((float)microphoneVolume / 100.0f);

		m_VoiceChat.SetVpdThreshold(0);
		m_VoiceChat.SetVpdThresholdPTT(0);
		m_VoiceChat.SetCaptureMode(VoiceChat::CAPTURE_MODE_VOICE_ACTIVATED);
#endif

#if RSG_ORBIS
		char iconPath[RAGE_MAX_PATH] = {0};
		formatf(iconPath, "%ssample_snet/icon.jpg", ASSET.GetPath());
		g_rlNp.SetDefaultSessionImage(iconPath);
#endif

#if RSG_DURANGO
		events_durango::Init();
		g_rlXbl.GetSessionManager()->SetTimeoutPolicies(ISessions::DEFAULT_ASYNC_SESSION_TIMEOUT_MS, ISessions::DEFAULT_MEMBER_RESERVED_TIMEOUT_MS,
			ISessions::DEFAULT_MEMBER_INACTIVE_TIMEOUT_MS, ISessions::DEFAULT_MEMBER_READY_TIMEOUT_MS, ISessions::DEFAULT_SESSION_EMPTY_TIMEOUT_MS);
#endif

        m_PendingStatus = 0;

        m_State = STATE_NONE;

        m_Initialized = true;
    }
}

void
LiveManager::Shutdown()
{
    if(m_Initialized)
    {
        m_Session.RemoveDelegate(&m_SessionDlgt);
		
		m_VoiceChat.ShutdownNetwork();
		m_VoiceChat.Shutdown();

		m_Session.Shutdown(false, -1);

        m_CxnMgr = 0;
        m_Allocator = 0;
        m_PendingStatus = 0;
        m_State = STATE_NONE;

        m_Initialized = false;
    }
}

void LiveManager::InitSession()
{
	m_Session.Init(m_Allocator, m_Owner, m_CxnMgr, SESSION_CHANNEL_ID,"GAME");
}

snSession*
LiveManager::GetSession()
{
    return &m_Session;
}

const snSession*
LiveManager::GetSession() const
{
    return &m_Session;
}

netConnectionManager*
LiveManager::GetCxnMgr()
{
    return m_CxnMgr;
}

bool
LiveManager::HostSession(Gamer* localGamer,
                        const rlNetworkMode netMode,
                        const int maxPubSlots,
                        const int maxPrivSlots,
                        const rlMatchingAttributes& attrs,
                        const unsigned createFlags,
                        netStatus* status)
{
    Assert(STATE_NONE == m_State);

    bool success = false;

    m_PendingStatus = status;

    u8 gamerData[SNET_MAX_SIZEOF_GAMER_DATA];
    unsigned sizeofGamerData;

    success =
        localGamer->ExportData(gamerData, sizeof(gamerData), &sizeofGamerData)
        && m_Session.Host(localGamer->m_GamerInfo.GetLocalIndex(),
                         netMode,
                         maxPubSlots,
                         maxPrivSlots,
                         attrs,
                         createFlags,
                         gamerData,
                         sizeofGamerData,
                         &m_MyStatus);

    if(success)
    {
        m_State = STATE_CREATING_SESSION;
        if(m_PendingStatus){m_PendingStatus->SetPending();}
    }
    else
    {
        m_PendingStatus = 0;
    }

    return success;
}

bool
LiveManager::JoinSession(Gamer* localGamer,
                        const rlSessionInfo& sessionInfo,
                        const rlNetworkMode netMode,
                        const rlSlotType slotType,
                        const unsigned createFlags,
                        netStatus* status)
{
    Assert(STATE_NONE == m_State);

    bool success = false;

    m_PendingStatus = status;

    u8 gamerData[SNET_MAX_SIZEOF_GAMER_DATA];
    unsigned sizeofGamerData;

	unsigned numPartyMembers = 0;
	rlGamerHandle aGroup[RL_MAX_GAMERS_PER_SESSION];

#if RSG_DURANGO
	numPartyMembers = rlGetSystemPartySize();
	for (unsigned i = 0; i < numPartyMembers; i++)
	{
		if (g_rlXbl.GetPartyManager()->GetPartyMember(i))
		{
			aGroup[i] = g_rlXbl.GetPartyManager()->GetPartyMember(i)->m_GamerHandle;
		}
	}
#endif

    success =
        localGamer->ExportData(gamerData, sizeof(gamerData), &sizeofGamerData)
        && m_Session.Join(localGamer->m_GamerInfo.GetLocalIndex(),
                            sessionInfo,
                            netMode,
                            slotType,
                            createFlags,
                            gamerData,
                            sizeofGamerData,
                            aGroup,
                            numPartyMembers,
                            &m_MyStatus);

    if(success)
    {
        m_State = STATE_JOINING_SESSION;
        if(m_PendingStatus){m_PendingStatus->SetPending();}
    }
    else
    {
        m_PendingStatus = 0;
    }

    return success;
}

bool
LiveManager::FindSessions(const rlNetworkMode netMode,
                            const Gamer* requester,
                            const int numLocalGamers,
                            const rlMatchingFilter& filter,
                            rlSessionDetail* results,
                            const unsigned maxResults,
							const unsigned numResultsBeforeEarlyOut,
                            unsigned* numResults,
                            netStatus* status)
{
    Assert(STATE_NONE == m_State);

    m_PendingStatus = status;

	// add party members
#if RSG_DURANGO
	int numGamers = numLocalGamers;
	if (rlGetSystemPartySize() > 0)
	{
		numGamers = rlGetSystemPartySize() - 1; // subtract 1 for requester who must be in system party
	}
#else
	int numGamers = numLocalGamers;
#endif

    const bool success =
        rlSessionFinder::Find(requester->GetLocalIndex(),
                            netMode,
                            SESSION_CHANNEL_ID,
                            numGamers,
                            filter,
                            results,
                            maxResults,
							numResultsBeforeEarlyOut,
                            numResults,
                            &m_MyStatus);

    if(success)
    {
        m_State = STATE_FINDING_SESSIONS;
        if(m_PendingStatus){m_PendingStatus->SetPending();}
    }
    else
    {
        m_PendingStatus = 0;
    }

    return success;
}

void
LiveManager::Update(const unsigned curTime)
{
    m_Session.Update(curTime);
    m_VoiceChat.Update();

    if(STATE_NONE != m_State && !m_MyStatus.Pending())
    {
        if(m_PendingStatus)
        {
            m_PendingStatus->SetStatus(m_MyStatus.GetStatus());
            m_PendingStatus = 0;
        }

        m_State = STATE_NONE;
    }
}

//private:

void
LiveManager::OnSessionEvent(snSession* /*session*/, const snEvent* evt)
{
    if(SNET_EVENT_ADDED_GAMER == evt->GetId())
    {
#if RSG_DURANGO
		if (evt->m_AddedGamer->m_GamerInfo.IsLocal())
		{
			m_VoiceChat.AddLocalChatUser(evt->m_AddedGamer->m_GamerInfo.GetLocalIndex());
		}
		else
		{
			m_VoiceChat.AddRemoteChatUser(evt->m_AddedGamer->m_GamerInfo, evt->m_AddedGamer->m_EndpointId);
		}
#endif // RSG_DURANGO
        m_VoiceChat.AddTalker(evt->m_AddedGamer->m_GamerInfo, evt->m_AddedGamer->m_EndpointId);
    }
    else if(SNET_EVENT_REMOVED_GAMER == evt->GetId())
    {
        m_VoiceChat.RemoveTalker(evt->m_RemovedGamer->m_GamerInfo.GetGamerId());
#if RSG_DURANGO
		if (evt->m_RemovedGamer->m_GamerInfo.IsLocal())
		{
			m_VoiceChat.RemoveLocalChatUser(evt->m_RemovedGamer->m_GamerInfo.GetLocalIndex());
		}
		else
		{
			m_VoiceChat.RemoveRemoteChatUser(evt->m_RemovedGamer->m_GamerInfo.GetGamerHandle());
		}
#endif // RSG_DURANGO
    }
}


}   //namespace rage
