// 
// sample_snet/ui.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "ui.h"
#include "app.h"

#include "diag/seh.h"
#include "grcore/im.h"
#include "grcore/font.h"
#include "rline/livestream/rllivestream.h"
#include "rline/rl.h"
#include "rline/rlfriend.h"
#include "rline/rlfriendsmanager.h"
#include "rline/entitlement/rlrosentitlement.h"
#include "rline/rlsessionfinder.h"
#include "rline/socialclub/rlsocialclub.h"
#include "rline/facebook/rlfacebook.h"
#include "livemanager.h"
#include "math/amath.h"
#include "snet/session.h"
#include "system/nelem.h"
#include "system/timer.h"

#if RSG_PC
#include "rline/rlpc.h"
#endif

#if RSG_ORBIS
#include "rline/rlnpparty.h"
#include "rline/rlnp.h"
#endif

#include <time.h>

namespace rage
{

#if RSG_PC && !__FINAL
XPARAM(takehometest);
#endif

//Convenience macro for adding an element to m_Selections
#define ADD_SELECTION(elem, text, x, y) \
    elem.SetText(text);\
    elem.SetXY(x, y);\
    m_Selections.AddElement(&elem);

static const Color32 COLOR_GRAY(0.3f, 0.3f, 0.3f);
static const Color32 COLOR_YELLOW(0.6f, 0.6f, 0.0f);
static const Color32 COLOR_WHITE(1.0f, 1.0f, 1.0f);

static const u16 RL_GAMETYPE_STANDARD = 1;
static const u16 RL_GAMETYPE_RANKED = 2;

///////////////////////////////////////////////////////////////////////////////
//  UiElement
///////////////////////////////////////////////////////////////////////////////
UiElement::UiElement()
    : m_Color(1.0f, 1.0f, 1.0f, 1.0f)
    , m_X(0)
    , m_Y(0)
    , m_Focus(0)
    , m_Container(NULL)
    , m_Enabled(true)
{
}

UiElement::~UiElement()
{
    this->Shutdown();
}

void
UiElement::Shutdown()
{
    this->Unbind();
    if(m_Container)
    {
        m_Container->RemoveElement(this);
    }
}

void
UiElement::SetEnabled(const bool b)
{
    m_Enabled = b;
    if(m_Container)
    {
        m_Container->OnChildEnabled(this, b);
    }
}
    
///////////////////////////////////////////////////////////////////////////////
//  UiLabel
///////////////////////////////////////////////////////////////////////////////
UiLabel::UiLabel(const char* text)
{
    safecpy(m_Text, text, sizeof(m_Text));
	m_ScaleX = 1.0f;
	m_ScaleY = 1.0f;
}

void
UiLabel::SetText(const char* text)
{
    safecpy(m_Text, text, sizeof(m_Text));
}

void
UiLabel::SetText(const char* text, const int length)
{
    const int len = sizeof(m_Text) > length ? sizeof(m_Text) : length;
    safecpy(m_Text, text, len);
}

void
UiLabel::SetScale(float scaleX, float scaleY)
{
	m_ScaleX = scaleX;
	m_ScaleY = scaleY;
}

const char*
UiLabel::GetText() const
{
    return m_Text;
}

void
UiLabel::Draw()
{
	
    this->UiElement::Draw();
    grcColor(this->IsEnabled() ? m_Color : COLOR_GRAY);
    grcDraw2dText(m_X, m_Y, m_Text, false, m_ScaleX, m_ScaleY);
}

bool
UiLabel::OnFocusChange(const bool haveFocus)
{
    const Color32 c = haveFocus ? COLOR_YELLOW : COLOR_WHITE;
    this->SetColor(c);
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//  UiContainer
///////////////////////////////////////////////////////////////////////////////
UiContainer::~UiContainer()
{
    this->RemoveAllElements();
}

void
UiContainer::Draw()
{
    this->UiElement::Draw();
    ElList::iterator it = m_Children.begin();
    ElList::const_iterator stop = m_Children.end();
    for(; stop != it; ++it)
    {
        (*it)->Draw();
    }
}

void
UiContainer::AddElement(UiElement* el)
{
    m_Children.push_back(el);
    el->m_Container = this;
}

void
UiContainer::RemoveElement(UiElement* el)
{
    m_Children.erase(el);
    el->m_Container = NULL;
}

void
UiContainer::RemoveAllElements()
{
    while(!m_Children.empty())
    {
        this->RemoveElement(*m_Children.begin());
    }
}

void
UiContainer::OnChildEnabled(UiElement* /*child*/, const bool /*enabled*/)
{
}


int
UiContainer::GetElementCount() const
{
    return (int) m_Children.size();
}

///////////////////////////////////////////////////////////////////////////////
//  UiList
///////////////////////////////////////////////////////////////////////////////
UiList::UiList()
: m_SettingEnabled(false)
{
}

void
UiList::AddElement(UiElement* el)
{
    this->UiContainer::AddElement(el);
    if(1 == this->GetElementCount())
    {
        m_ItFocus = m_Children.begin();
        this->Select(el);
    }
}

void
UiList::RemoveElement(UiElement* el)
{
    if(this->GetElementCount())
    {
        if(1 == this->GetElementCount())
        {
            if(el->IsEnabled())
            {
                Assert(*m_ItFocus == el);
                (*m_ItFocus)->OnFocusChange(false);
            }
        }
        else if(*m_ItFocus == el)
        {
            this->Next();
            if(*m_ItFocus == el)
            {
                m_ItFocus = ElList::iterator();
            }
        }
        this->UiContainer::RemoveElement(el);
    }
}

bool
UiList::OnUp()
{
    this->Prev();
    return true;
}

bool
UiList::OnDown()
{
    this->Next();
    return true;
}

bool
UiList::OnLeft()
{
    if(this->GetElementCount())
    {
        (*m_ItFocus)->OnLeft();
    }
    return true;
}

bool
UiList::OnRight()
{
    if(this->GetElementCount())
    {
        (*m_ItFocus)->OnRight();
    }
    return true;
}

bool
UiList::OnAction()
{
    if(this->IsEnabled() && this->GetElementCount())
    {
        this->FireEvent(UIEVENT_ACTION);
    }
    return true;
}

UiElement*
UiList::GetSelection()
{
    return this->GetElementCount() ? *m_ItFocus : 0;
}

void
UiList::ResetFocus()
{
	if (this->GetElementCount())
	{
		m_ItFocus = m_Children.begin();
		this->Select(*m_ItFocus);
	}
}

const UiElement*
UiList::GetSelection() const
{
    return this->GetElementCount() ? *m_ItFocus : 0;
}

void
UiList::Select(UiElement* el)
{
    if(el->IsEnabled())
    {
        ElList::iterator it = m_Children.begin();
        ElList::const_iterator stop = m_Children.end();
        for(; stop != it; ++it)
        {
            if(*it == el)
            {
                (*m_ItFocus)->OnFocusChange(false);
                m_ItFocus = it;
                (*m_ItFocus)->OnFocusChange(true);
                this->FireEvent(UIEVENT_SELECT);
                break;
            }
        }
    }
}

void
UiList::OnChildEnabled(UiElement* child, const bool enabled)
{
    if(!m_SettingEnabled)
    {
        this->SetEnabled(child, enabled);
    }
}

void
UiList::SetEnabled(UiElement* el, const bool b)
{
    m_SettingEnabled = true;

    ElList::iterator it = m_Children.begin();
    ElList::const_iterator stop = m_Children.end();
    for(; stop != it; ++it)
    {
        if(*it == el)
        {
            el->SetEnabled(b);
            if(!b && this->GetSelection() == el)
            {
                this->Next();
            }
            break;
        }
    }

    m_SettingEnabled = false;
}

bool
UiList::IsEnabled(const UiElement* el) const
{
    bool b = false;
    ElList::const_iterator it = m_Children.begin();
    ElList::const_iterator stop = m_Children.end();
    for(; stop != it; ++it)
    {
        if(*it == el)
        {
            b = el->IsEnabled();
            break;
        }
    }

    return b;
}

void
UiList::Next()
{
    int count = (int) m_Children.size();
    ElList::iterator it = m_ItFocus;

    for(++it; it != m_ItFocus && count > 0; ++it, --count)
    {
        if(m_Children.end() == it)
        {
            it = m_Children.begin();
        }

        if((*it)->IsEnabled())
        {
            this->Select(*it);
            break;
        }
    }
}

void
UiList::Prev()
{
    int count = (int) m_Children.size();
    ElList::iterator it = m_ItFocus;

    if(m_Children.begin() == it)
    {
        it = m_Children.end();
    }

    for(--it; it != m_ItFocus && count > 0; --it, --count)
    {
        if((*it)->IsEnabled())
        {
            this->Select(*it);
            break;
        }

        if(m_Children.begin() == it)
        {
            it = m_Children.end();
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiMultiValue
///////////////////////////////////////////////////////////////////////////////
void
UiMultiValue::SetText(const char* text)
{
    m_Label.SetText(text);
    this->UpdateChildPositions();
}

const char*
UiMultiValue::GetLabel() const
{
    return m_Label.GetText();
}

void
UiMultiValue::AddElement(UiElement* el)
{
    this->UiList::AddElement(el);
    const float w =
        grcFont::GetCurrent().GetWidth() * float(strlen(m_Label.GetText()) + 1);
    el->SetXY(this->GetX() + w, this->GetY());
}

void
UiMultiValue::SetXY(const float x, const float y)
{
    this->UiList::SetXY(x, y);
    m_Label.SetXY(x, y);

    this->UpdateChildPositions();
}

void
UiMultiValue::Draw()
{
    this->UiElement::Draw();

    m_Label.Draw();

    if(this->GetElementCount())
    {
        (*m_ItFocus)->OnFocusChange(false);
        (*m_ItFocus)->Draw();
    }
}

bool
UiMultiValue::OnUp()
{
    return true;
}

bool
UiMultiValue::OnDown()
{
    return true;
}

bool
UiMultiValue::OnLeft()
{
    this->UiList::OnUp();
    return true;
}

bool
UiMultiValue::OnRight()
{
    this->UiList::OnDown();
    return true;
}

bool
UiMultiValue::OnFocusChange(const bool haveFocus)
{
    this->UiList::OnFocusChange(haveFocus);
    m_Label.OnFocusChange(haveFocus);
    return true;
}

//private:

void
UiMultiValue::UpdateChildPositions()
{
    const float w =
        grcFont::GetCurrent().GetWidth() * float(strlen(m_Label.GetText()) + 1);
    const float x = this->GetX() + w;
    const float y = this->GetY();

    ElList::iterator it = m_Children.begin();
    ElList::const_iterator stop = m_Children.end();
    for(; stop != it; ++it)
    {
        (*it)->SetXY(x, y);
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiSpinner
///////////////////////////////////////////////////////////////////////////////
UiSpinner::UiSpinner()
    : m_MinValue(-INT_MAX)
    , m_MaxValue(INT_MAX)
    , m_Increment(1)
    , m_Value(0)
{
    this->SetValue(m_Value);
}

void
UiSpinner::SetText(const char* text)
{
    m_Label.SetText(text);
    this->UpdateChildPositions();
}

void
UiSpinner::SetMin(const int minValue)
{
    SetLimits(minValue, m_MaxValue);
}

void
UiSpinner::SetMax(const int maxValue)
{
    SetLimits(m_MinValue, maxValue);
}

void
UiSpinner::SetLimits(const int minValue, const int maxValue)
{
    Assert(minValue <= maxValue);
    m_MinValue = minValue;
    m_MaxValue = maxValue;
}

void
UiSpinner::SetIncrement(const int increment)
{
    m_Increment = increment;
}

void
UiSpinner::SetValue(const int value)
{
    if(value < m_MinValue)
    {
        m_Value = m_MinValue;
    }
    else if(value > m_MaxValue)
    {
        m_Value = m_MaxValue;
    }
    else
    {
        m_Value = value;
    }
    char buf[128];
    formatf(buf, "%d", m_Value);
    m_ValueLabel.SetText(buf);
}

int
UiSpinner::GetValue() const
{
    return m_Value;
}

int
UiSpinner::GetMax() const
{
    return m_MaxValue;
}

int
UiSpinner::GetMin() const
{
    return m_MinValue;
}

int
UiSpinner::GetIncrement() const
{
    return m_Increment;
}

const char*
UiSpinner::GetLabel() const
{
    return m_Label.GetText();
}

void
UiSpinner::SetXY(const float x, const float y)
{
    this->UiElement::SetXY(x, y);
    m_Label.SetXY(x, y);

    this->UpdateChildPositions();
}

void
UiSpinner::Draw()
{
    this->UiElement::Draw();

    m_Label.Draw();
    m_ValueLabel.Draw();
}

bool
UiSpinner::OnUp()
{
    return true;
}

bool
UiSpinner::OnDown()
{
    return true;
}

bool
UiSpinner::OnLeft()
{
    this->SetValue(m_Value - m_Increment);
    return true;
}

bool
UiSpinner::OnRight()
{
    this->SetValue(m_Value + m_Increment);
    return true;
}

bool
UiSpinner::OnFocusChange(const bool haveFocus)
{
    m_Label.OnFocusChange(haveFocus);
    return true;
}

//private:

void
UiSpinner::UpdateChildPositions()
{
    const float w =
        grcFont::GetCurrent().GetWidth() * float(strlen(m_Label.GetText()) + 1);
    const float x = this->GetX() + w;
    const float y = this->GetY();

    m_ValueLabel.SetXY(x, y);
}

///////////////////////////////////////////////////////////////////////////////
//UiGenericResult
///////////////////////////////////////////////////////////////////////////////
UiGenericResult::UiGenericResult()
: m_NumLines(0)
{
    m_Selections.Bind(this, &UiGenericResult::OnEvent);

    float y = 0;
    for(unsigned i = 0; i < MAX_LINES; i++, y += UI_GAP)
    {
        m_Lines[i].SetText("");
        m_Lines[i].SetXY(UI_CONTENT_X, UI_CONTENT_Y + y);
    }

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, y);
    m_Selections.AddElement(&m_Back);

    this->SetFocus(&m_Selections);
}

UiGenericResult::~UiGenericResult()
{
    m_Selections.RemoveAllElements();
}

void
UiGenericResult::Draw()
{
    this->UiElement::Draw();
    m_Selections.Draw();

    for(unsigned i =0; i < m_NumLines; i++)
    {
        m_Lines[i].Draw();
    }
}

void
UiGenericResult::ClearText()
{
    for(unsigned i = 0; i < MAX_LINES; i++)
    {
        m_Lines[i].SetText("");
    }
    m_NumLines = 0;
}

void
UiGenericResult::AddText(const char* text)
{
    if(m_NumLines < MAX_LINES)
    {
        m_Lines[m_NumLines].SetText(text);
        ++m_NumLines;
    }
}

void
UiGenericResult::AddText(const char* text, const int length)
{
    if(m_NumLines < MAX_LINES)
    {
        m_Lines[m_NumLines].SetText(text, length);
        ++m_NumLines;
    }
}

void
UiGenericResult::SetText(const char* text)
{
    ClearText();
    if(text)
    {
        const int len = (int)strlen(text);
        const int numLines = (len + UiLabel::MAX_LABEL_LENGTH - 1) / UiLabel::MAX_LABEL_LENGTH;
        int offset = 0;
        for(int i = 0; i < numLines; ++i, offset += UiLabel::MAX_LABEL_LENGTH)
        {
            Assert(offset < len);
            AddText(&text[offset], UiLabel::MAX_LABEL_LENGTH);
        }
    }
}

void
UiGenericResult::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
        {
            const UiElement* sel = m_Selections.GetSelection();
                
            if(&m_Back == sel)
            {
                this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
            }
        }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiFrontEnd
///////////////////////////////////////////////////////////////////////////////
UiFrontEnd::UiFrontEnd()
	: m_Crash(false)
{
    m_Selections.Bind(this, &UiFrontEnd::OnEvent);

    float y = UI_CONTENT_Y;

#if RSG_PC && !__FINAL
	if(!PARAM_takehometest.Get())
#endif
	{
		ADD_SELECTION(m_Multiplayer,"Multiplayer", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_FriendsList,"Friends List", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_InvitesReceived,"Invites Received", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_Leaderboards,"Leaderboards", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_Inbox,"Inbox", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_SocialClub,"Social Club", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_CommerceAndEntitlement,"Commerce and Entitlement", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_Facebook,"Facebook", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_Cloud,"Cloud", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_Ugc,"Ugc", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_Presence,"Presence", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_RichPresence,"Rich Presence", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_Achievements,"Achievements", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_ProfileStats,"Profile Stats", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_CommunityStats,"Community Stats", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_Clans,"Clans", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_Parties, "System Party", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_DisplayUnlocks,"Display Unlocks", UI_CONTENT_X, y); y += UI_GAP;
#if RSG_PC
		ADD_SELECTION(m_Entitlement, "Entitlement", UI_CONTENT_X, y); y += UI_GAP;
#endif
		ADD_SELECTION(m_ShowSigninUi,"Show Signin", UI_CONTENT_X, y); y += UI_GAP;

		if (g_SystemUi.CanAddFriendUI())
		{
			ADD_SELECTION(m_AddFriendUi, "Add Friend", UI_CONTENT_X, y); y += UI_GAP;
		}

#if RSG_DURANGO
		ADD_SELECTION(m_ShowAppHelpMenu, "Show App Help", UI_CONTENT_X, y); y += UI_GAP;
#endif

#if LIVESTREAM_ENABLED
		ADD_SELECTION(m_LiveStream, "Live Streaming", UI_CONTENT_X, y); y += UI_GAP;
#endif

		ADD_SELECTION(m_YoutubeMenu, "Youtube Uploads", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_Quit,"Quit", UI_CONTENT_X, y); y += UI_GAP;
	}	

    this->SetFocus(&m_Selections);
}

UiFrontEnd::~UiFrontEnd()
{
    m_Selections.RemoveAllElements();
}

void
UiFrontEnd::Draw()
{
    this->UiElement::Draw();
    m_Selections.Draw();
}

void 
UiFrontEnd::UpdateEnabled(const bool /*hasNetwork*/,
                          const bool isSignedIn,
                          const bool isOnline,
                          const bool hasInvites)
{
    m_Selections.SetEnabled(&m_Multiplayer,             isSignedIn);
    m_Selections.SetEnabled(&m_FriendsList,             isOnline);
    m_Selections.SetEnabled(&m_InvitesReceived,         hasInvites);
    m_Selections.SetEnabled(&m_Leaderboards,            isOnline);
    m_Selections.SetEnabled(&m_Inbox,                   isOnline);
#if RLROS_SC_PLATFORM
    m_Selections.SetEnabled(&m_SocialClub,              true);
#else
    m_Selections.SetEnabled(&m_SocialClub,              isOnline);
#endif
	m_Selections.SetEnabled(&m_CommerceAndEntitlement,	isOnline);
	m_Selections.SetEnabled(&m_Facebook,			    isOnline && RL_FACEBOOK_SWITCH(!rlFacebook::IsKillSwitchEnabled(), false));
    m_Selections.SetEnabled(&m_Cloud,                   isOnline);
    m_Selections.SetEnabled(&m_Ugc,                     isOnline);
    m_Selections.SetEnabled(&m_Presence,                isOnline);
	m_Selections.SetEnabled(&m_RichPresence,			isOnline);
	m_Selections.SetEnabled(&m_Achievements,            isSignedIn);
	m_Selections.SetEnabled(&m_Clans,                   isOnline);
	m_Selections.SetEnabled(&m_Parties,					isOnline);
    m_Selections.SetEnabled(&m_ProfileStats,            true); //To test graceful handling of errors
    m_Selections.SetEnabled(&m_CommunityStats,          true); //To test graceful handling of errors
    m_Selections.SetEnabled(&m_DisplayUnlocks,          true); //To test graceful handling of errors

#if RSG_PC
    m_Selections.SetEnabled(&m_ShowSigninUi,            !isSignedIn && g_rlPc.IsUiAcceptingCommands());
#else
	m_Selections.SetEnabled(&m_ShowSigninUi,            true);
#endif

	if (g_SystemUi.CanAddFriendUI())
	{
		m_Selections.SetEnabled(&m_AddFriendUi,				isSignedIn);
	}

#if RSG_DURANGO
	m_Selections.SetEnabled(&m_ShowAppHelpMenu, true);
#endif

#if LIVESTREAM_ENABLED
	m_Selections.SetEnabled(&m_LiveStream, isOnline);
#endif

	m_Selections.SetEnabled(&m_YoutubeMenu, isOnline);

	m_Selections.SetEnabled(&m_Quit,                    true);
}

//private:

bool UiFrontEnd::OnLeft()
{
	if (m_Selections.GetSelection() == &m_Quit)
	{
		m_Crash = !m_Crash;
		char textBuf[256] = {0};
		safecpy(textBuf, m_Crash ? "Game Crash" : "Quit");
		m_Quit.SetText(&textBuf[0], 256);
	}

	return true;
}

bool UiFrontEnd::OnRight()
{
	if (m_Selections.GetSelection() == &m_Quit)
	{
		m_Crash = !m_Crash;
		char textBuf[256] = {0};
		safecpy(textBuf, m_Crash ? "Game Crash" : "Quit");
		m_Quit.SetText(&textBuf[0], 256);
	}

	return true;
}

void
UiFrontEnd::OnEvent(UiElement* ASSERT_ONLY(el), const int evtId, void*)
{
    Assert(&m_Selections == el);

    if(UIEVENT_ACTION == evtId)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Multiplayer == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)FE_MULTIPLAYER);
        }
        else if(&m_FriendsList == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)FE_FRIENDS_LIST);
        }
        else if(&m_InvitesReceived == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)FE_FRIEND_INVITES);
        }
        else if(&m_Leaderboards == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)FE_LEADERBOARDS);
        }
        else if(&m_Inbox == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)FE_INBOX);
        }
        else if(&m_SocialClub == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)FE_SOCIAL_CLUB);
        }
		else if(&m_CommerceAndEntitlement == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)FE_COMMERCE_AND_ENTITLEMENT);
		}
		else if(&m_Facebook == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)FE_FACEBOOK);
		}
        else if(&m_Cloud == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)FE_CLOUD);
        }
        else if(&m_Ugc == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)FE_UGC);
        }
        else if(&m_Presence == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)FE_PRESENCE);
        }
		else if (&m_RichPresence == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)FE_RICHPRESENCE);
		}
		else if(&m_Achievements == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)FE_ACHIEVEMENTS);
		}
		else if(&m_Clans == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)FE_CLANS);
        }
        else if(&m_ProfileStats == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)FE_PROFILE_STATS);
        }
        else if(&m_CommunityStats == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)FE_COMMUNITY_STATS);
        }
        else if(&m_DisplayUnlocks == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)FE_DISPLAY_UNLOCKS);
        }
		else if(&m_Entitlement == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)FE_ENTITLEMENT);
		}
		else if(&m_Parties == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)FE_DISPLAY_PARTY);
		}
        else if(&m_ShowSigninUi == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)FE_SHOW_SIGNIN_UI);
        }
		else if(&m_ShowAppHelpMenu == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)FE_SHOW_APP_HELP);
		}
		else if(&m_AddFriendUi == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)FE_ADDFRIEND);
		}
		else if(&m_LiveStream == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)FE_LIVESTREAM);
		}
		else if(&m_YoutubeMenu == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)FE_YOUTUBE);
		}
		else if(&m_Quit == sel)
        {
			if (m_Crash)
			{
				this->FireEvent(UIEVENT_ACTION, (void*)FE_CRASH);
			}
			else
			{
				this->FireEvent(UIEVENT_ACTION, (void*)FE_QUIT);	
			}
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiLeaderboardMainMenu
///////////////////////////////////////////////////////////////////////////////
UiLeaderboardMainMenu::UiLeaderboardMainMenu()
{
    m_Title.SetText("LEADERBOARDS");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiLeaderboardMainMenu::OnEvent);

    float y = UI_CONTENT_Y;

    ADD_SELECTION(m_Leaderboard2Write, "Leaderboard 2 Write", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_Leaderboard2Read, "Leaderboard 2 Read", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_SwingsByStrikeLb, "Swings by Strike", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ClanSwingsByStrikeLb, "Clan Swings by Strike", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ClanMemberSwingsByStrikeLb, "Clan Member Swings by Strike", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_HitsLb, "Hits", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ClanHitsLb, "Clan Hits", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ClanMemberHitsLb, "Clan Member Hits", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ArbHitsLb, "Hits (arb)", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ExhibSkill, "Exhibition Skill", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_RankedExhibSkill, "Exhibition Skill (ranked)", UI_CONTENT_X, y); y += UI_GAP;
    
    ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

    this->SetFocus(&m_Selections);
}

UiLeaderboardMainMenu::~UiLeaderboardMainMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiLeaderboardMainMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void
UiLeaderboardMainMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();

        if(&m_Leaderboard2Write == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_LEADERBOARD2_WRITE_MENU);
        }
        else if(&m_Leaderboard2Read == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_LEADERBOARD2_READ_MENU);
        }
        else if(&m_SwingsByStrikeLb == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_SWINGS_BY_STRIKE_LB_MENU);
        }
        else if(&m_ClanSwingsByStrikeLb == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_CLAN_SWINGS_BY_STRIKE_LB_MENU);
        }
        else if(&m_ClanMemberSwingsByStrikeLb == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_CLAN_MEMBER_SWINGS_BY_STRIKE_LB_MENU);
        }
        else if(&m_HitsLb == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_HITS_LB_MENU);
        }
        else if(&m_ClanHitsLb == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_CLAN_HITS_LB_MENU);
        }
        else if(&m_ClanMemberHitsLb == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_CLAN_MEMBER_HITS_LB_MENU);
        }
        else if(&m_ArbHitsLb == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_ARB_HITS_LB_MENU);
        }
        else if(&m_ExhibSkill == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_SKILL_EXHIB_MENU);
        }
        else if(&m_RankedExhibSkill == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_SKILL_RANKED_EXHIB_MENU);
        }
        else if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiLeaderboardMenu
///////////////////////////////////////////////////////////////////////////////
UiLeaderboardMenu::UiLeaderboardMenu()
{
    m_Title.SetText("LEADERBOARD");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiLeaderboardMenu::OnEvent);

    float y = UI_CONTENT_Y;

    ADD_SELECTION(m_GetLeaderboardSize, "Get Leaderboard Size", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ReadByRank, "Read by Rank", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ReadByRow, "Read by Row", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ReadMemberByGroups, "Read Member by Groups", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ReadByRadius, "Read by Radius", UI_CONTENT_X, y); y += UI_GAP;
#if __DEV
    ADD_SELECTION(m_ClearLb, "Clear Leaderboard", UI_CONTENT_X, y); y += UI_GAP;
#endif
    
    ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

    this->SetFocus(&m_Selections);
}

UiLeaderboardMenu::~UiLeaderboardMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiLeaderboardMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void
UiLeaderboardMenu::SetTitle(const char* title)
{
    m_Title.SetText(title);
}

void
UiLeaderboardMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
            
        if(&m_GetLeaderboardSize == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_LEADERBOARD_SIZE);
        }
        else if(&m_ReadByRow == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_BY_ROW);
        }
        else if(&m_ReadMemberByGroups == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_MEMBER_BY_GROUPS);
        }
        else if(&m_ReadByRadius == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_BY_RADIUS);
        }
        else if(&m_ReadByRank == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_BY_RANK);
        }
#if __DEV
        else if(&m_ClearLb == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CLEAR_LEADERBOARD);
        }
#endif
        else if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

//////////////////////////////////////////////////////////////////////////
//  UiLeaderboard2WriteMenu
//////////////////////////////////////////////////////////////////////////
UiLeaderboard2WriteMenu::UiLeaderboard2WriteMenu()
{
    m_Cat1None.SetText("");
    m_Cat1Mission.SetText("Mission");
    m_Cat1Challenge.SetText("Challenge");
    m_Category1.AddElement(&m_Cat1None);
    m_Category1.AddElement(&m_Cat1Mission);
    m_Category1.AddElement(&m_Cat1Challenge);
    m_Category1.Select(&m_Cat1Mission);

    m_Cat2None.SetText("");
    m_Cat2Mission.SetText("Mission");
    m_Cat2Challenge.SetText("Challenge");
    m_Category2.AddElement(&m_Cat2None);
    m_Category2.AddElement(&m_Cat2Mission);
    m_Category2.AddElement(&m_Cat2Challenge);
    m_Category2.Select(&m_Cat2Challenge);

    m_GroupId1.SetMin(0);
    m_GroupId2.SetMin(0);

    m_Swings.SetText("Swings");
    m_Strikes.SetText("Strikes");
    m_Singles.SetText("Singles");
    m_Doubles.SetText("Doubles");
    m_Triples.SetText("Triples");
    m_Homers.SetText("Homers");
    m_Flyouts.SetText("Flyouts");

    m_Title.SetText("LEADERBOARD WRITE");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiLeaderboard2WriteMenu::OnEvent);

    float y = UI_CONTENT_Y;

    ADD_SELECTION(m_Category1, "Category 1", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GroupId1, "Group ID 1", UI_CONTENT_X + UI_GAP, y); y += UI_GAP;
    ADD_SELECTION(m_Category2, "Category 2", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GroupId2, "Group ID 2", UI_CONTENT_X + UI_GAP, y); y += UI_GAP;

    y += UI_GAP;

    ADD_SELECTION(m_Swing, "Swing", UI_CONTENT_X, y); y += UI_GAP;
    y += UI_GAP;
    ADD_SELECTION(m_Submit, "Submit", UI_CONTENT_X, y); y += UI_GAP;
    y += UI_GAP;
    ADD_SELECTION(m_Reset, "Reset", UI_CONTENT_X, y); y += UI_GAP;

    y += UI_GAP*2;
    const float SCORES_X  = UI_CONTENT_X + (5*UI_GAP);

    m_Swings.SetXY(SCORES_X, y); y += UI_GAP;
    this->AddElement(&m_Swings);
    m_Strikes.SetXY(SCORES_X, y); y += UI_GAP;
    this->AddElement(&m_Strikes);
    m_Singles.SetXY(SCORES_X, y); y += UI_GAP;
    this->AddElement(&m_Singles);
    m_Doubles.SetXY(SCORES_X, y); y += UI_GAP;
    this->AddElement(&m_Doubles);
    m_Triples.SetXY(SCORES_X, y); y += UI_GAP;
    this->AddElement(&m_Triples);
    m_Homers.SetXY(SCORES_X, y); y += UI_GAP;
    this->AddElement(&m_Homers);
    m_Flyouts.SetXY(SCORES_X, y); y += UI_GAP;
    this->AddElement(&m_Flyouts);
    
    ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

    this->AddElement(&m_Title);
    this->AddElement(&m_Selections);

    this->SetFocus(&m_Selections);
}

UiLeaderboard2WriteMenu::~UiLeaderboard2WriteMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiLeaderboard2WriteMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
            
        if(&m_Reset == sel)
        {
            m_Swings.SetValue(0);
            m_Strikes.SetValue(0);
            m_Singles.SetValue(0);
            m_Doubles.SetValue(0);
            m_Triples.SetValue(0);
            m_Homers.SetValue(0);
            m_Flyouts.SetValue(0);
        }
        else if(&m_Swing == sel)
        {
            m_Swings.SetValue(m_Swings.GetValue()+1);
            const bool strike = (sysTimer::GetSystemMsTime() & 0x7) < 5;

            if(strike)
            {
                m_Strikes.SetValue(m_Strikes.GetValue()+1);
            }
            else
            {
                const int hitResult = sysTimer::GetSystemMsTime() % 5;
                switch(hitResult)
                {
                case 0:
                    m_Singles.SetValue(m_Singles.GetValue()+1);
                    break;
                case 1:
                    m_Doubles.SetValue(m_Doubles.GetValue()+1);
                    break;
                case 2:
                    m_Triples.SetValue(m_Triples.GetValue()+1);
                    break;
                case 3:
                    m_Homers.SetValue(m_Homers.GetValue()+1);
                    break;
                case 4:
                    m_Flyouts.SetValue(m_Flyouts.GetValue()+1);
                    break;
                }
            }
        }
        else if(&m_Submit == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SUBMIT);
        }
        else if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

//////////////////////////////////////////////////////////////////////////
//  UiLeaderboard2ReadMenu
//////////////////////////////////////////////////////////////////////////
UiLeaderboard2ReadMenu::UiLeaderboard2ReadMenu()
{
    m_Cat1None.SetText("");
    m_Cat1Mission.SetText("Mission");
    m_Cat1Challenge.SetText("Challenge");
    m_Category1.AddElement(&m_Cat1None);
    m_Category1.AddElement(&m_Cat1Mission);
    m_Category1.AddElement(&m_Cat1Challenge);
    m_Category1.Select(&m_Cat1Mission);

    m_Cat2None.SetText("");
    m_Cat2Mission.SetText("Mission");
    m_Cat2Challenge.SetText("Challenge");
    m_Category2.AddElement(&m_Cat2None);
    m_Category2.AddElement(&m_Cat2Mission);
    m_Category2.AddElement(&m_Cat2Challenge);
    m_Category2.Select(&m_Cat2Challenge);

    //We'll treat -1 as a wildcard
    m_GroupId1.SetMin(-1);
    m_GroupId2.SetMin(-1);

    m_LbTypePlayer.SetText("Player");
    m_LbTypeCrewMember.SetText("Crew Member");
    m_LbTypeCrew.SetText("Crew");
    m_LbTypeGroupMember.SetText("Group Member");
    m_LbTypeGroup.SetText("Group");
    m_LbType.AddElement(&m_LbTypePlayer);
    m_LbType.AddElement(&m_LbTypeCrewMember);
    m_LbType.AddElement(&m_LbTypeCrew);
    m_LbType.AddElement(&m_LbTypeGroupMember);
    m_LbType.AddElement(&m_LbTypeGroup);

    m_Title.SetText("LEADERBOARD READ");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiLeaderboard2ReadMenu::OnEvent);

    float y = UI_CONTENT_Y;

    ADD_SELECTION(m_Category1, "Category 1", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GroupId1, "Group ID 1", UI_CONTENT_X + UI_GAP, y); y += UI_GAP;
    ADD_SELECTION(m_Category2, "Category 2", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GroupId2, "Group ID 2", UI_CONTENT_X + UI_GAP, y); y += UI_GAP;
    ADD_SELECTION(m_LbType, "Leaderboard Type", UI_CONTENT_X, y); y += UI_GAP;

    y += UI_GAP;
    ADD_SELECTION(m_ReadByRank, "Read by Rank", UI_CONTENT_X, y); y += UI_GAP;
    y += UI_GAP;
    ADD_SELECTION(m_ReadByRow, "Read by Row", UI_CONTENT_X, y); y += UI_GAP;
    y += UI_GAP;
    ADD_SELECTION(m_ReadByScore, "Read by Score", UI_CONTENT_X, y); y += UI_GAP;
    y += UI_GAP;
    ADD_SELECTION(m_ReadNear, "Read Near", UI_CONTENT_X, y); y += UI_GAP;
    
    ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

    this->AddElement(&m_Title);
    this->AddElement(&m_Selections);

    this->SetFocus(&m_Selections);
}

UiLeaderboard2ReadMenu::~UiLeaderboard2ReadMenu()
{
    m_Selections.RemoveAllElements();
}

rlLeaderboard2Type
UiLeaderboard2ReadMenu::GetLeaderboardType() const
{
    const UiElement* sel = m_LbType.GetSelection();
    if(&m_LbTypePlayer == sel)
    {
        return RL_LEADERBOARD2_TYPE_PLAYER;
    }
    else if(&m_LbTypeCrew == sel)
    {
        return RL_LEADERBOARD2_TYPE_CLAN;
    }
    else if(&m_LbTypeCrewMember == sel)
    {
        return RL_LEADERBOARD2_TYPE_CLAN_MEMBER;
    }
    else if(&m_LbTypeGroup == sel)
    {
        return RL_LEADERBOARD2_TYPE_GROUP;
    }
    else if(&m_LbTypeGroupMember == sel)
    {
        return RL_LEADERBOARD2_TYPE_GROUP_MEMBER;
    }

    return RL_LEADERBOARD2_TYPE_INVALID;
}

void
UiLeaderboard2ReadMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
            
        if(&m_ReadByRank == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_BY_RANK);
        }
        else if(&m_ReadByRow == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_BY_ROW);
        }
        else if(&m_ReadByScore == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_BY_SCORE);
        }
        else if(&m_ReadNear == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_NEAR);
        }
        else if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

//////////////////////////////////////////////////////////////////////////
//  UiInboxMenu
//////////////////////////////////////////////////////////////////////////
UiInboxMenu::UiInboxMenu()
    : m_NumMessages(0)
{
    m_Title.SetText("INBOX");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiInboxMenu::OnEvent);

    float y = UI_CONTENT_Y;

    m_Offset.SetMin(0);
    m_Count.SetMin(0);
    m_Offset.SetIncrement(10);
    m_Count.SetValue(10);

    ADD_SELECTION(m_Offset, "Offset", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_Count, "Count", UI_CONTENT_X, y); y += UI_GAP;
    y += UI_GAP;
    ADD_SELECTION(m_GetMessages, "Get Messages", UI_CONTENT_X, y); y += UI_GAP;
    y += UI_GAP;
    ADD_SELECTION(m_GetNewMessages, "Get New Messages", UI_CONTENT_X, y); y += UI_GAP;
    y += UI_GAP;
    ADD_SELECTION(m_PostMessage, "Post Message", UI_CONTENT_X, y); y += UI_GAP;
    y += UI_GAP;
    ADD_SELECTION(m_PostMessageToFriends, "Post To Friends", UI_CONTENT_X, y); y += UI_GAP;
    y += UI_GAP;
    ADD_SELECTION(m_PostMessageToCrew, "Post To Crew", UI_CONTENT_X, y); y += UI_GAP;

    for(int i = 0; i < COUNTOF(m_Messages); ++i)
    {
        y += UI_GAP;
        m_MessageTimestamps[i].SetXY(UI_CONTENT_X+UI_GAP, y);
        m_MessageContents[i].SetXY(UI_CONTENT_X, y);
        m_Messages[i].AddElement(&m_MessageTimestamps[i]);
        m_Messages[i].AddElement(&m_MessageContents[i]);
    }

    y += UI_GAP;
    m_MessageList.SetXY(UI_CONTENT_X, y);
    this->AddElement(&m_MessageList);

    ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

    this->AddElement(&m_Title);
    this->AddElement(&m_Selections);

    this->SetFocus(&m_Selections);
}

UiInboxMenu::~UiInboxMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiInboxMenu::ClearMessages()
{
    m_MessageList.RemoveAllElements();
    m_NumMessages = 0;
}

void
UiInboxMenu::AddMessage(const u64 timestamp, const char* message)
{
    if(m_NumMessages < COUNTOF(m_Messages))
    {
        const time_t t = (time_t)timestamp;
        m_MessageTimestamps[m_NumMessages].SetText(ctime(&t));
        m_MessageContents[m_NumMessages].SetText(message);

        const float w =
            grcFont::GetCurrent().GetWidth() * float(strlen(m_MessageTimestamps[m_NumMessages].GetText()) + 1);
        const float x = m_MessageTimestamps[m_NumMessages].GetX() + w;
        const float y = m_MessageTimestamps[m_NumMessages].GetY();
        m_MessageContents[m_NumMessages].SetXY(x, y);

        m_MessageList.AddElement(&m_Messages[m_NumMessages]);
        ++m_NumMessages;
    }
}

void
UiInboxMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
            
        if(&m_GetMessages == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_MESSAGES);
        }
        else if(&m_GetNewMessages == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_NEW_MESSAGES);
        }
        else if(&m_PostMessage == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_POST_MESSAGE);
        }
        else if(&m_PostMessageToFriends == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_POST_MESSAGE_TO_FRIENDS);
        }
        else if(&m_PostMessageToCrew == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_POST_MESSAGE_TO_CREW);
        }
        else if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiFriendsList
///////////////////////////////////////////////////////////////////////////////
UiFriendsList::UiFriendsList()
: m_AllowInvites(false)
{
	m_bQueueRefreshFriendsList = false;
	m_bRefreshingFriends = false;
	m_FriendPage.Init(MAX_FRIEND);

    m_Title.SetText("FRIENDS LIST");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiFriendsList::OnEvent);
	
	m_Segue.SetText("Loading more friends...");
	m_Segue.SetScale(3.0f, 3.0f);
	m_Segue.SetXY(UI_CONTENT_X, UI_CONTENT_Y);

	float curGap = 0;
    for(unsigned i = 0; i < UiFriendsList::MAX_FRIEND; i++)
    {
		curGap += UI_GAP;

        m_InviteToSession[i].SetText("Invite to Session");
        m_InviteToClan[i].SetText("Invite to Clan");
        m_Profile[i].SetText("Profile");
        m_JoinSession[i].SetText("Join Session");
		m_SendMessage[i].SetText("Send Message");
		m_SubscribeToRTA[i].SetText("Subscribe to RTA");
		m_UnsubscribeToRTA[i].SetText("Unsubscribe from RTA");
		m_IsFriend[i].SetText("Check Is Friend");
		m_RemoveFriend[i].SetText("Remove Friend");

        m_FriendLabels[i].Bind(this, &UiFriendsList::OnEvent);
        m_FriendLabels[i].SetText("");
        m_FriendLabels[i].SetXY(UI_CONTENT_X, UI_CONTENT_Y + curGap);
    }

	// add some space...
	curGap += (5 * UI_GAP);

#if RSG_DURANGO
	m_GetSessions.SetText("Get Friends Sessions");
	m_GetSessions.SetXY(UI_CONTENT_X, UI_CONTENT_Y + curGap);
	m_Selections.AddElement(&m_GetSessions);
	curGap += UI_GAP;
#endif

#if RSG_ORBIS || RSG_DURANGO
	m_GetDisplayNames.SetText("Get Display Names");
	m_GetDisplayNames.SetXY(UI_CONTENT_X, UI_CONTENT_Y + curGap);
	m_Selections.AddElement(&m_GetDisplayNames);
	curGap += UI_GAP;
#endif

	m_Refresh.SetText("Refresh List");
	m_Refresh.SetXY(UI_CONTENT_X, UI_CONTENT_Y + curGap);
	m_Selections.AddElement(&m_Refresh);
	curGap += UI_GAP;

	m_SearchFriends.SetText("Search Friends");
	m_SearchFriends.SetXY(UI_CONTENT_X, UI_CONTENT_Y + curGap);
	m_Selections.AddElement(&m_SearchFriends);
	curGap += UI_GAP;
	
	char buf[255];
	formatf(buf, "Start Index: %d", m_FriendPage.m_StartIndex);
	m_CurrentPage.SetText(buf);
	m_CurrentPage.SetXY(UI_FOOTER_X, UI_FOOTER_Y);
	
	sysMemSet(buf, 0, sizeof(buf));
	formatf(buf, "Prev Page (%s)", GetPageLeftText());
	m_PrevPage.SetText(buf);
	m_PrevPage.SetXY(UI_FOOTER_X, UI_FOOTER_Y + (UI_GAP));

	sysMemSet(buf, 0, sizeof(buf));
	formatf(buf, "Next Page (%s)", GetPageRightText());
	m_NextPage.SetText(buf);
	m_NextPage.SetXY(UI_FOOTER_X, UI_FOOTER_Y + (2* UI_GAP));

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);
    m_Selections.AddElement(&m_Back);

    this->SetFocus(&m_Selections);
}

void
UiFriendsList::Shutdown()
{
	m_FriendPage.Shutdown();
}

UiFriendsList::~UiFriendsList()
{
    m_Selections.RemoveAllElements();
	m_FriendPage.Shutdown();
}

void 
UiFriendsList::QueueRefreshFriendsList()
{
	m_bQueueRefreshFriendsList = true;
}

void 
UiFriendsList::Update()
{
	int numFriends = rlFriendsManager::GetTotalNumFriends(rlPresence::GetActingUserIndex());
	int startIndex = m_FriendPage.m_StartIndex;
	char buf[256] = {0};
	int numPages = (numFriends / MAX_FRIEND) + (numFriends % MAX_FRIEND == 0 ? 0 : 1);
	int curPage = (startIndex / MAX_FRIEND) + 1;
	formatf(buf, "FRIENDS LIST (Page %d / %d)", curPage, numPages);
	m_Title.SetText(buf);

	if (m_bQueueRefreshFriendsList)
	{
		RefreshFriendsList();
		m_bQueueRefreshFriendsList = false;
	}

	if (m_bRefreshingFriends)
	{
		if (!m_GetFriendsStatus.Pending())
		{
			m_Selections.RemoveAllElements();

			for(int i=0; i < UiFriendsList::MAX_FRIEND; i++)
			{
				m_FriendLabels[i].RemoveAllElements();

				if(i < (int)m_FriendPage.m_NumFriends)
				{
					const rlFriend& f = m_FriendPage.m_Friends[i];

					char buf[128];
					bool isInSession = false;
#if RSG_NP
					u8 data[RL_PRESENCE_MAX_BUF_SIZE];
					unsigned dataSize = 0;

					AssertVerify(f.GetPresenceBlob(rlPresence::GetActingUserIndex(), data, &dataSize, sizeof(data)));

					isInSession = dataSize > 0;
#elif RSG_PC || RSG_DURANGO
					isInSession = f.IsInSession();
#endif

					formatf(buf, sizeof(buf), "%s [%s]%s:", 
						f.GetName(),
						f.IsOnline() ? "ONLINE" : "OFFLINE",
						isInSession ? " [IN_SESSION]" : "");

					if(isInSession)
					{
						m_FriendLabels[i].AddElement(&m_JoinSession[i]);
					}

					if(m_AllowInvites)
					{
						m_FriendLabels[i].AddElement(&m_InviteToSession[i]);
					}

					m_FriendLabels[i].AddElement(&m_InviteToClan[i]);
					m_FriendLabels[i].AddElement(&m_SendMessage[i]);

#if RSG_DURANGO
					m_FriendLabels[i].AddElement(&m_SubscribeToRTA[i]);
					m_FriendLabels[i].AddElement(&m_UnsubscribeToRTA[i]);
#endif

					m_FriendLabels[i].AddElement(&m_Profile[i]);
					m_FriendLabels[i].AddElement(&m_IsFriend[i]);
					m_FriendLabels[i].AddElement(&m_RemoveFriend[i]);

					m_FriendLabels[i].SetText(buf);
					m_Selections.AddElement(&m_FriendLabels[i]);
				}
				else
				{
					m_FriendLabels[i].SetText("");
				}
			}

			m_bRefreshingFriends = false;

			m_Selections.AddElement(&m_Back);
#if RSG_DURANGO
			m_Selections.AddElement(&m_GetSessions);
#endif

#if RSG_DURANGO || RSG_ORBIS
			m_Selections.AddElement(&m_GetDisplayNames);
#endif
			m_Selections.AddElement(&m_Refresh);
#if RSG_PC
			m_Selections.AddElement(&m_SearchFriends);
#endif
		}
	}
}

void
UiFriendsList::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
	m_CurrentPage.Draw();
	m_PrevPage.Draw();
	m_NextPage.Draw();
}

bool 
UiFriendsList::RefreshFriendsList()
{
	rtry
	{
		int friendFlags = rlFriendsReader::FRIENDS_ALL | rlFriendsReader::FRIENDS_PRESORT_ID | rlFriendsReader::FRIENDS_PRESORT_ONLINE;
		rcheck(!m_GetFriendsStatus.Pending(), catchall, );
		rverify(rlFriendsManager::GetFriends(rlPresence::GetActingUserIndex(), &m_FriendPage, friendFlags, &m_GetFriendsStatus), catchall, );
		m_Selections.RemoveAllElements();
		m_Selections.AddElement(&m_Segue);
		m_bRefreshingFriends = true;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool UiFriendsList::OnPageLeft()
{
	rtry
	{
		rcheck(!m_GetFriendsStatus.Pending(), catchall,);
		int newIndex = m_FriendPage.m_StartIndex - m_FriendPage.m_MaxFriends;
		if (newIndex < 0)
		{
			int totalFriends = rlFriendsManager::GetTotalNumFriends(rlPresence::GetActingUserIndex());
			int overflow = totalFriends % m_FriendPage.m_MaxFriends;
			if (overflow == 0)
			{
				overflow = m_FriendPage.m_MaxFriends;
			}

			newIndex = totalFriends - overflow;
		}

		if (m_FriendPage.m_StartIndex == newIndex)
		{
			return true;
		}

		m_FriendPage.m_StartIndex = newIndex;
		int friendFlags = rlFriendsReader::FRIENDS_ALL | rlFriendsReader::FRIENDS_PRESORT_ID | rlFriendsReader::FRIENDS_PRESORT_ONLINE;
		rverify(rlFriendsManager::GetFriends(rlPresence::GetActingUserIndex(), &m_FriendPage, friendFlags, &m_GetFriendsStatus), catchall, );
		m_Selections.RemoveAllElements();
		m_Selections.AddElement(&m_Segue);
		m_bRefreshingFriends = true;

		char buf[255];
		formatf(buf, "Start Index: %d", m_FriendPage.m_StartIndex);
		m_CurrentPage.SetText(buf);
		m_CurrentPage.SetXY(UI_FOOTER_X, UI_FOOTER_Y);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool UiFriendsList::OnPageRight()
{
	rtry
	{
		rcheck(!m_GetFriendsStatus.Pending(), catchall,);
		int newIndex = m_FriendPage.m_StartIndex + m_FriendPage.m_MaxFriends;
		int totalFriends = rlFriendsManager::GetTotalNumFriends(rlPresence::GetActingUserIndex());
		if (newIndex >= totalFriends)
		{
			newIndex = 0;
		}

		// If we're not requesting new data
		if (m_FriendPage.m_StartIndex == newIndex)
		{
			return true;
		}

		m_FriendPage.m_StartIndex = newIndex;
		int friendFlags = rlFriendsReader::FRIENDS_ALL | rlFriendsReader::FRIENDS_PRESORT_ID | rlFriendsReader::FRIENDS_PRESORT_ONLINE;
		rverify(rlFriendsManager::GetFriends(rlPresence::GetActingUserIndex(), &m_FriendPage, friendFlags, &m_GetFriendsStatus), catchall, );
		m_Selections.RemoveAllElements();
		m_Selections.AddElement(&m_Segue);
		m_bRefreshingFriends = true;

		char buf[255];
		formatf(buf, "Start Index: %d", m_FriendPage.m_StartIndex);
		m_CurrentPage.SetText(buf);
		m_CurrentPage.SetXY(UI_FOOTER_X, UI_FOOTER_Y);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool UiFriendsList::GetDisplayNames(const int localGamerIndex, netStatus* status)
{
#if RSG_DURANGO
	static rlDisplayName displayNames[MAX_FRIEND];
	static rlGamertag gamerTags[MAX_FRIEND];
	static rlGamerHandle gamerHandles[MAX_FRIEND];
	for (unsigned i = 0; i < m_FriendPage.m_NumFriends; i++)
	{
		gamerHandles[i].ResetXbl(m_FriendPage.m_Friends[i].GetXuid());
	}

	g_rlXbl.GetProfileManager()->GetPlayerNames(localGamerIndex, gamerHandles, m_FriendPage.m_NumFriends, displayNames, gamerTags, status);
#elif RSG_ORBIS
	static rlDisplayName displayNames[MAX_FRIEND];
	static rlGamertag gamerTags[MAX_FRIEND];

	rlGamerHandle* handles = (rlGamerHandle*)alloca(sizeof(rlGamerHandle) * m_FriendPage.m_NumFriends);
	if (!handles)
	{
		return false;
	}
	
	for (unsigned i = 0; i < m_FriendPage.m_NumFriends; i++)
	{
		m_FriendPage.m_Friends[i].GetGamerHandle(&handles[i]);
	}

	g_rlNp.GetWebAPI().GetPlayerNames(localGamerIndex, handles, m_FriendPage.m_NumFriends, &displayNames[0], &gamerTags[0], true, status);
#else
	(void)localGamerIndex;
	(void)status;
#endif
	
	return true;
}

void
UiFriendsList::RequestNewFriendsPage(const int localGamerIndex)
{
	if (rlFriendsManager::CanQueueFriendsRefresh(localGamerIndex))
	{
		rlFriendsManager::RequestRefreshFriendsPage(localGamerIndex, &m_FriendPage, m_FriendPage.m_StartIndex, m_FriendPage.m_NumFriends, true);
	}
}

void
UiFriendsList::SetAllowInvites(const bool allowInvites)
{
    m_AllowInvites = allowInvites;

    for(unsigned i=0; i < UiFriendsList::MAX_FRIEND; i++)
    {
        m_FriendLabels[i].RemoveAllElements();

		bool isInSession = false;
		const rlFriend& f = m_FriendPage.m_Friends[i];

#if RSG_NP
		u8 data[RL_PRESENCE_MAX_BUF_SIZE];
		unsigned dataSize = 0;

		AssertVerify(f.GetPresenceBlob(rlPresence::GetActingUserIndex(), data, &dataSize, sizeof(data)));

		isInSession = dataSize > 0;
#elif RSG_PC || RSG_DURANGO
		isInSession = f.IsInSession();
#endif

		if(isInSession)
		{
			m_FriendLabels[i].AddElement(&m_JoinSession[i]);
		}

        if(m_AllowInvites)
        {
            m_FriendLabels[i].AddElement(&m_InviteToSession[i]);
        }

        m_FriendLabels[i].AddElement(&m_InviteToClan[i]);
		m_FriendLabels[i].AddElement(&m_SendMessage[i]);

#if RSG_DURANGO
		m_FriendLabels[i].AddElement(&m_SubscribeToRTA[i]);
		m_FriendLabels[i].AddElement(&m_UnsubscribeToRTA[i]);
#endif

        m_FriendLabels[i].AddElement(&m_Profile[i]);
		m_FriendLabels[i].AddElement(&m_IsFriend[i]);
		m_FriendLabels[i].AddElement(&m_RemoveFriend[i]);
    }
}

bool
UiFriendsList::InvitesAllowed() const
{
    return m_AllowInvites;
}

int
UiFriendsList::GetFriendIndex() const
{
    int idx = -1;
    const UiElement* sel = m_Selections.GetSelection();

    if(sel >= &m_FriendLabels[0] && sel < &m_FriendLabels[MAX_FRIEND])
    {
        idx = ptrdiff_t_to_int((const UiMultiValue*) sel - &m_FriendLabels[0]);
    }

    return idx;
}

const rlFriend*
UiFriendsList::GetSelectedFriend() const
{
    const int idx = this->GetFriendIndex();
    return (idx >= 0) ? &m_FriendPage.m_Friends[idx] : NULL;
}

void
UiFriendsList::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
		else if (&m_GetSessions == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_SESSIONS);
		}
		else if (&m_GetDisplayNames == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_DISPLAYNAMES);
		}
		else if (&m_Refresh == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_REFRESH_LIST);
		}
		else if(&m_SearchFriends == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SEARCH_FRIENDS);
		}
        else
        {
            const int idx = this->GetFriendIndex();
            if(idx >= 0)
            {
                const UiElement* choice = m_FriendLabels[idx].GetSelection();
                if(&m_InviteToSession[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_INVITE_TO_SESSION);
                }
                else if(&m_InviteToClan[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_INVITE_TO_CLAN);
                }
                else if(&m_Profile[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_PROFILE);
                }
				else if (&m_IsFriend[idx] == choice)
				{
					this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CHECK_IS_FRIEND);
				}
                else if(&m_JoinSession[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_JOIN_SESSION);
                }
				else if (&m_SendMessage[idx] == choice)
				{
					this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SEND_MESSAGE);
				}
				else if (&m_SubscribeToRTA[idx] == choice)
				{
					this->FireEvent(UIEVENT_ACTION, (void*)ACTION_RTA_SUBSCRIBE);
				}
				else if (&m_UnsubscribeToRTA[idx] == choice)
				{
					this->FireEvent(UIEVENT_ACTION, (void*)ACTION_RTA_UNSUBSCRIBE);
				}
				else if (&m_RemoveFriend[idx] == choice)
				{
					this->FireEvent(UIEVENT_ACTION, (void*)ACTION_REMOVE_FRIEND);
				}
            }
            /*for(unsigned i=0; i < UiFriendsList::MAX_FRIEND; i++)
            {
                if(sel == &m_FriendLabels[i])
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)FRIENDSLIST_JOIN_SESSION);
                    break;
                }
            }*/
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

bool
#if RSG_NP
UiFriendsList::GetJoinSessionInfo(rlSessionInfo *info) const
#else
UiFriendsList::GetJoinSessionInfo(rlSessionInfo *) const
#endif
{
#if RSG_NP
    const UiElement* sel = m_Selections.GetSelection();

    if(sel)
    {
        for(unsigned i=0; i < UiFriendsList::MAX_FRIEND; i++)
        {
            if(sel == &m_FriendLabels[i])
            {
                const rlFriend& f = m_FriendPage.m_Friends[i];

                char data[RL_PRESENCE_MAX_BUF_SIZE];
                unsigned dataSize = 0;

                AssertVerify(f.GetPresenceBlob(0, data, &dataSize, sizeof(data)));

                if(dataSize)
                {
                    if(info->ImportFromNpPresence(data, dataSize))
                    {
                        return true;
                    }
                    else
                    {
                        rlError("Failed to import sessioninfo from presence");
                    }
                }

                break;
            }
        }
    }
#elif RSG_PC && 0
	// TODO: NS - need to use GetAttributesForGamer to query presence for session info struct
    const UiElement* sel = m_Selections.GetSelection();

    if(sel)
    {
        for(unsigned i=0; i < UiFriendsList::MAX_FRIEND; i++)
        {
            if(sel == &m_FriendLabels[i])
            {
                const rlFriend& f = m_Friends[i];
                *info = f.GetSessionInfo();
                return true;
            }
        }
    }
#endif

    return false;
}

///////////////////////////////////////////////////////////////////////////////
//  UiInvitesReceived
///////////////////////////////////////////////////////////////////////////////
UiInvitesReceived::UiInvitesReceived()
: m_Dirty(true)
, m_PendingMpInviteUiEnabled(false)
{
    m_InviteList.Bind(this, &UiInvitesReceived::OnEvent);

    m_Title.SetText("INVITES RECEIVED");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);

    for(unsigned i = 0; i < MAX_INVITES; i++)
    {
        Invite *inv = &m_Pile[i];

        inv->m_AcceptLabel.SetText("Accept");
        inv->m_DeclineLabel.SetText("Decline");
        inv->m_InviteMv.AddElement(&inv->m_AcceptLabel);
        inv->m_InviteMv.AddElement(&inv->m_DeclineLabel);

        m_FreeList.push_back(inv);
    }

    m_ShowPendingMpInvites.SetText("Show Multiplayer Invites");
}

UiInvitesReceived::~UiInvitesReceived()
{
    for(unsigned i = 0; i < MAX_INVITES; i++)
    {
        Invite *inv = &m_Pile[i];
        inv->m_InviteMv.RemoveAllElements();
    }
}

void
UiInvitesReceived::Draw()
{
    if(m_Dirty)
    {
        RefreshUi();
        m_Dirty = false;
    }

    this->UiElement::Draw();
    m_Title.Draw();
    m_InviteList.Draw();
}

void
UiInvitesReceived::AddInvite(const char* inviterName,
                           const rlSessionInfo& info)
{
    if(HasInviteFromSession(info))
    {        
        return; //Redundant, ignore.
    }

    if(!m_FreeList.empty())
    {
        Invite* tmp = *m_FreeList.begin();

        m_FreeList.pop_front();       

        safecpy(tmp->m_InviterName, inviterName, sizeof(tmp->m_InviterName));
        tmp->m_SessionInfo = info;

        m_ActiveList.push_back(tmp);

        m_Dirty = true;
    }
}

void
UiInvitesReceived::SetPendingMpInviteUiEnabled(const bool enabled)
{
    m_PendingMpInviteUiEnabled = enabled;
    RefreshUi();
}

void
UiInvitesReceived::RemoveInvite(const rlSessionInfo& info)
{
    if(!m_ActiveList.empty())
    {
        ListType::iterator it = m_ActiveList.begin();
        while(it != m_ActiveList.end())
        {
            Invite* tmp = *it;

            if(tmp->m_SessionInfo == info)
            {
                m_ActiveList.erase(tmp);
                m_FreeList.push_back(tmp);
                m_Dirty = true;
                return;
            }

            it++;
        }
    }
}

bool
UiInvitesReceived::HasInvites() const
{
    return (0 != m_ActiveList.size()) || m_PendingMpInviteUiEnabled;
}

void
UiInvitesReceived::Clear()
{
    while(!m_ActiveList.empty())
    {
        RemoveInvite((*(m_ActiveList.begin()))->m_SessionInfo);
    }

    m_Dirty = true;
}

const UiInvitesReceived::Invite* 
UiInvitesReceived::GetSelectedInvite() const
{
    const UiElement* sel = m_InviteList.GetSelection();

    if(sel && !m_ActiveList.empty())
    {
        ListType::const_iterator it = m_ActiveList.begin();
        while(it != m_ActiveList.end())
        {
            if(sel == &((*it)->m_InviteMv))
            {
                return *it;
            }
            it++;
        }
    }

    return 0;
}

const rlSessionInfo* 
UiInvitesReceived::GetSelectedSession() const
{
    const Invite *inv = GetSelectedInvite();

    if(inv)
    {
        return &inv->m_SessionInfo;
    }

    return 0;
}

bool
UiInvitesReceived::HasInviteFromSession(const rlSessionInfo& info)
{
    if(!m_ActiveList.empty())
    {
        ListType::iterator it = m_ActiveList.begin();
        while(it != m_ActiveList.end())
        {
            Invite* tmp = *it;

            if(tmp->m_SessionInfo == info)
            {                
                return true;
            }

            it++;
        }
    }

    return false;
}

void
UiInvitesReceived::RefreshUi()
{
    m_InviteList.RemoveAllElements();

    float y = UI_CONTENT_Y;

    if(!m_ActiveList.empty())
    {
        ListType::iterator it = m_ActiveList.begin();
        while(it != m_ActiveList.end())
        {
            Invite* tmp = *it;

            char buf[128];
            formatf(buf, sizeof(buf), "%s:", tmp->m_InviterName);

            tmp->m_InviteMv.SetText(buf);
            tmp->m_InviteMv.SetXY(UI_CONTENT_X, y);           

            m_InviteList.AddElement(&tmp->m_InviteMv);

            y += UI_GAP;
            it++;
        }
    }

    if(m_PendingMpInviteUiEnabled)
    {
        m_ShowPendingMpInvites.SetXY(UI_CONTENT_X, y);
        m_InviteList.AddElement(&m_ShowPendingMpInvites);
    }

    m_InviteList.AddElement(&m_Back);

    this->SetFocus(&m_InviteList);
}

void
UiInvitesReceived::OnEvent(UiElement* el, const int evtId, void*)
{
    Assert(&m_InviteList == el);

    if(UIEVENT_ACTION == evtId && &m_InviteList == el)
    {
        const UiElement* sel = m_InviteList.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else if(m_InviteList.GetSelection() == &m_ShowPendingMpInvites)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_PENDING_MP_INVITES);
            this->SetPendingMpInviteUiEnabled(false);
        }
        else
        {
            const Invite* inv = GetSelectedInvite();
            if(inv)
            {
                if(&inv->m_DeclineLabel == inv->m_InviteMv.GetSelection())
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_DECLINE);
                    RemoveInvite(inv->m_SessionInfo);
                }
                else if(&inv->m_AcceptLabel == inv->m_InviteMv.GetSelection())
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_ACCEPT);
                    RemoveInvite(inv->m_SessionInfo);
                }
            }
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiMultiplayerMainMenu
///////////////////////////////////////////////////////////////////////////////
UiMultiplayerMainMenu::UiMultiplayerMainMenu()
: m_Session(NULL)
, m_VoiceChat(NULL)
{
    m_Title.SetText("MULTIPLAYER");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    float y = UI_CONTENT_Y;

    m_Selections.Bind(this, &UiMultiplayerMainMenu::OnEvent);

    y = UI_CONTENT_Y;

    ADD_SELECTION(m_HostOnline, "Host Online", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_SearchOnline, "Search Online", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_HostLan, "Host LAN", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_SearchLan, "Search LAN", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_HostOffline, "Host Offline", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_FriendsList,"Friends List", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_HostParty, "Host Party", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_LeaveParty, "Leave Party", UI_CONTENT_X, y); y += UI_GAP;
    
    ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

    this->SetFocus(&m_Selections);
}

UiMultiplayerMainMenu::~UiMultiplayerMainMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiMultiplayerMainMenu::Init(snSession* session, const VoiceChat* voiceChat)
{
    m_Session = session;
    m_VoiceChat = voiceChat;
}

void
UiMultiplayerMainMenu::Shutdown()
{
    m_Session = NULL;
    m_VoiceChat = NULL;
    UiElement::Shutdown();
}

void
UiMultiplayerMainMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();

    m_Selections.Draw();
}

void
UiMultiplayerMainMenu::UpdateEnabled(const bool hasNetwork,
                                      const bool isSignedIn,
                                      const bool isOnline)
{
    m_Selections.SetEnabled(&m_HostOnline,              isOnline);
    m_Selections.SetEnabled(&m_SearchOnline,            isOnline);
    m_Selections.SetEnabled(&m_HostLan,                 hasNetwork && isSignedIn && SUPPORT_LAN_MULTIPLAYER);
    m_Selections.SetEnabled(&m_SearchLan,               hasNetwork && isSignedIn && SUPPORT_LAN_MULTIPLAYER);
    m_Selections.SetEnabled(&m_HostOffline,             isSignedIn);
    m_Selections.SetEnabled(&m_FriendsList,             isOnline);
    m_Selections.SetEnabled(&m_HostParty,               isOnline);
    m_Selections.SetEnabled(&m_LeaveParty,              isOnline);
}

void
UiMultiplayerMainMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
            
        if(&m_HostOnline == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_HOST_ONLINE);
        }
        else if(&m_SearchOnline == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SEARCH_ONLINE);
        }
        else if(&m_HostLan == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_HOST_LAN);
        }
        else if(&m_SearchLan == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SEARCH_LAN);
        }
        else if(&m_HostOffline == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_HOST_OFFLINE);
        }
        else if(&m_FriendsList == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_FRIENDS_LIST);
        }
        else if(&m_HostParty == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_HOST_PARTY);
        }
        else if(&m_LeaveParty == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_LEAVE_PARTY);
        }
        else if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiHostConfig
///////////////////////////////////////////////////////////////////////////////
UiHostConfig::UiHostConfig()
: m_NumPublicSlots(32)
, m_NumPrivateSlots(0)
, m_CreateFlags(0)
{
    m_Attrs.Reset(MatchingAttributes());

    m_Title.SetText("HOST CONFIGURATION");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiHostConfig::OnEvent);

    m_GtStandard.SetText("Standard");
    m_GtRanked.SetText("Ranked");
    m_GameType.Bind(this, &UiHostConfig::OnEvent);
    m_GameType.SetText("Game Type: ");
    m_GameType.AddElement(&m_GtStandard);
    m_GameType.AddElement(&m_GtRanked);
    m_GameType.Select(&m_GtStandard);
    m_Attrs.SetSessionPurpose(RL_GAMETYPE_STANDARD);

    m_GmExhib.SetText("Exhibition");
    m_GmTourn.SetText("Tournament");
    m_GameMode.Bind(this, &UiHostConfig::OnEvent);
    m_GameMode.SetText("Game Mode: ");
    m_GameMode.AddElement(&m_GmExhib);
    m_GameMode.AddElement(&m_GmTourn);
    m_GameMode.Select(&m_GmExhib);
    m_Attrs.SetGameMode(MatchingAttributes::GAME_MODE_EXHIBITION);

    m_NumPublicSlotsMv.Bind(this, &UiHostConfig::OnEvent);
    m_NumPublicSlotsMv.SetText("Public Slots:");
    for(int i = 0; i < COUNTOF(m_NumPublicSlotLabels); ++i)
    {
        char buf[8];
        formatf(buf, "%d", i);
        m_NumPublicSlotLabels[i].SetText(buf);
	    m_NumPublicSlotsMv.AddElement(&m_NumPublicSlotLabels[i]);
    }

    m_NumPublicSlotsMv.Select(&m_NumPublicSlotLabels[32]);

    m_NumPrivateSlotsMv.Bind(this, &UiHostConfig::OnEvent);
    m_NumPrivateSlotsMv.SetText("Private Slots:");
    for(int i = 0; i < COUNTOF(m_NumPrivateSlotLabels); ++i)
    {
        char buf[8];
        formatf(buf, "%d", i);
        m_NumPrivateSlotLabels[i].SetText(buf);
	    m_NumPrivateSlotsMv.AddElement(&m_NumPrivateSlotLabels[i]);
    }

    m_NumPrivateSlotsMv.Select(&m_NumPrivateSlotLabels[0]);

    m_Gpm3.SetText("3");
    m_Gpm5.SetText("5");
    m_GamesPerMatch.Bind(this, &UiHostConfig::OnEvent);
    m_GamesPerMatch.SetText("Games Per Match: ");
    m_GamesPerMatch.AddElement(&m_Gpm3);
    m_GamesPerMatch.AddElement(&m_Gpm5);
    m_GamesPerMatch.Select(&m_Gpm3);
    m_Attrs.SetValueById(MatchingAttributes::FIELD_ID_GAMES_PER_MATCH,
                        MatchingAttributes::GAMES_PER_MATCH_BEST_OF_3);

    m_Mpl7.SetText("7");
    m_Mpl11.SetText("11");
    m_MatchPointLimit.Bind(this, &UiHostConfig::OnEvent);
    m_MatchPointLimit.SetText("Match Point Limit: ");
    m_MatchPointLimit.AddElement(&m_Mpl7);
    m_MatchPointLimit.AddElement(&m_Mpl11);
    m_MatchPointLimit.Select(&m_Mpl7);
    m_Attrs.SetValueById(MatchingAttributes::FIELD_ID_MATCH_POINT_LIMIT, 7);
    
    m_JipEnabled.SetText("Enabled");
    m_JipDisabled.SetText("Disabled");
    m_Jip.Bind(this, &UiHostConfig::OnEvent);
    m_Jip.SetText("Join In Progress: ");
    m_Jip.AddElement(&m_JipEnabled);
    m_Jip.AddElement(&m_JipDisabled);
    m_Jip.Select(&m_JipEnabled);

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);

    m_Create.SetText("Create");
    m_Create.SetXY(UI_RIGHT_CTRL_X, UI_RIGHT_CTRL_Y);

    UiElement* elm[] =
    {
        &m_GameType,
        &m_GameMode,
        &m_NumPublicSlotsMv,
        &m_NumPrivateSlotsMv,
        &m_GamesPerMatch,
        &m_MatchPointLimit,
        &m_Jip,
    };

    const float h = UI_GAP;

    for(unsigned i = 0; i < COUNTOF(elm); ++i)
    {
        elm[i]->SetXY(UI_CONTENT_X, UI_CONTENT_Y + i*h);
        m_Selections.AddElement(elm[i]);
    }

    m_Selections.AddElement(&m_Back);
    m_Selections.AddElement(&m_Create);

    m_Selections.Select(&m_Create);

    this->SetFocus(&m_Selections);
}

UiHostConfig::~UiHostConfig()
{
    m_Selections.RemoveAllElements();

    m_GameType.RemoveAllElements();
    m_GameMode.RemoveAllElements();
    m_NumPublicSlotsMv.RemoveAllElements();
    m_NumPrivateSlotsMv.RemoveAllElements();
    m_GamesPerMatch.RemoveAllElements();
    m_MatchPointLimit.RemoveAllElements();
}

void
UiHostConfig::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

const rlMatchingAttributes&
UiHostConfig::GetAttrs() const
{
    return m_Attrs;
}

//private:

void
UiHostConfig::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else if(&m_Create == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CREATE);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
    else if(UIEVENT_SELECT == evtId)
    {
        if(&m_GameType == el)
        {
            const UiElement* sel = m_GameType.GetSelection();
            if(&m_GtStandard == sel)
            {
                m_Attrs.SetSessionPurpose(RL_GAMETYPE_STANDARD);
            }
            else if(&m_GtRanked == sel)
            {
                m_Attrs.SetSessionPurpose(RL_GAMETYPE_RANKED);
            }
        }
        else if(&m_GameMode == el)
        {
            const UiElement* sel = m_GameMode.GetSelection();
            if(&m_GmExhib == sel)
            {
                m_Attrs.SetGameMode(MatchingAttributes::GAME_MODE_EXHIBITION);
            }
            else if(&m_GmTourn == sel)
            {
                m_Attrs.SetGameMode(MatchingAttributes::GAME_MODE_TOURNAMENT);
            }
        }
        else if(&m_NumPublicSlotsMv == el)
        {
            const UiElement* sel = m_NumPublicSlotsMv.GetSelection();
            m_NumPublicSlots = ptrdiff_t_to_int(((UiLabel*)sel) - m_NumPublicSlotLabels);
            m_Selections.SetEnabled(&m_Create,
                                    m_NumPublicSlots || m_NumPrivateSlots);
        }
        else if(&m_NumPrivateSlotsMv == el)
        {
            const UiElement* sel = m_NumPrivateSlotsMv.GetSelection();
            m_NumPrivateSlots = ptrdiff_t_to_int(((UiLabel*)sel) - m_NumPrivateSlotLabels);
            m_Selections.SetEnabled(&m_Create,
                                    m_NumPublicSlots || m_NumPrivateSlots);
        }
        else if(&m_GamesPerMatch == el)
        {
            const UiElement* sel = m_GamesPerMatch.GetSelection();
            if(&m_Gpm3 == sel)
            {
                m_Attrs.SetValueById(MatchingAttributes::FIELD_ID_GAMES_PER_MATCH,
                                    MatchingAttributes::GAMES_PER_MATCH_BEST_OF_3);
            }
            else if(&m_Gpm5 == sel)
            {
                m_Attrs.SetValueById(MatchingAttributes::FIELD_ID_GAMES_PER_MATCH,
                                    MatchingAttributes::GAMES_PER_MATCH_BEST_OF_5);
            }
        }
        else if(&m_MatchPointLimit == el)
        {
            const UiElement* sel = m_MatchPointLimit.GetSelection();
            if(&m_Mpl7 == sel)
            {
                m_Attrs.SetValueById(MatchingAttributes::FIELD_ID_MATCH_POINT_LIMIT,
                                    7);
            }
            else if(&m_Mpl11 == sel)
            {
                m_Attrs.SetValueById(MatchingAttributes::FIELD_ID_MATCH_POINT_LIMIT,
                                    11);
            }
        }
        else if(&m_Jip == el)
        {
            const UiElement* sel = m_Jip.GetSelection();
            if(&m_JipEnabled == sel)
            {
                m_CreateFlags &= ~RL_SESSION_CREATE_FLAG_JOIN_IN_PROGRESS_DISABLED;
            }
            else if(&m_JipDisabled == sel)
            {
                m_CreateFlags |= RL_SESSION_CREATE_FLAG_JOIN_IN_PROGRESS_DISABLED;
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiQueryConfig
///////////////////////////////////////////////////////////////////////////////
UiQueryConfig::UiQueryConfig()
{
    m_Filter.Reset(MatchingFilter_MAIN_QUERY());

    m_Title.SetText("QUERY CONFIGURATION");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiQueryConfig::OnEvent);

    m_GtStandard.SetText("Standard");
    m_GtRanked.SetText("Ranked");
    m_GameType.Bind(this, &UiQueryConfig::OnEvent);
    m_GameType.SetText("Game Type: ");
    m_GameType.AddElement(&m_GtStandard);
    m_GameType.AddElement(&m_GtRanked);
    m_GameType.Select(&m_GtStandard);
    m_Filter.SetSessionPurpose(RL_GAMETYPE_STANDARD);

    m_GmExhib.SetText("Exhibition");
    m_GmTourn.SetText("Tournament");
    m_GameMode.Bind(this, &UiQueryConfig::OnEvent);
    m_GameMode.SetText("Game Mode: ");
    m_GameMode.AddElement(&m_GmExhib);
    m_GameMode.AddElement(&m_GmTourn);
    m_GameMode.Select(&m_GmExhib);
    m_Filter.SetGameMode(MatchingAttributes::GAME_MODE_EXHIBITION);

    m_GpmAny.SetText("Any");
    m_Gpm3.SetText("3");
    m_Gpm5.SetText("5");
    m_GamesPerMatch.Bind(this, &UiQueryConfig::OnEvent);
    m_GamesPerMatch.SetText("Games Per Match: ");
    m_GamesPerMatch.AddElement(&m_GpmAny);
    m_GamesPerMatch.AddElement(&m_Gpm3);
    m_GamesPerMatch.AddElement(&m_Gpm5);
    m_GamesPerMatch.Select(&m_GpmAny);

    m_MplAny.SetText("Any");
    m_Mpl7.SetText("7");
    m_Mpl11.SetText("11");
    m_MatchPointLimit.Bind(this, &UiQueryConfig::OnEvent);
    m_MatchPointLimit.SetText("Match Point Limit: ");
    m_MatchPointLimit.AddElement(&m_MplAny);
    m_MatchPointLimit.AddElement(&m_Mpl7);
    m_MatchPointLimit.AddElement(&m_Mpl11);
    m_MatchPointLimit.Select(&m_MplAny);

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);

    m_Search.SetText("Search");
    m_Search.SetXY(UI_RIGHT_CTRL_X, UI_RIGHT_CTRL_Y);

    UiElement* elm[] =
    {
        &m_GameType,
        &m_GameMode,
        &m_GamesPerMatch,
        &m_MatchPointLimit,
    };

    const float h = UI_GAP;

    for(unsigned i = 0; i < COUNTOF(elm); ++i)
    {
        elm[i]->SetXY(UI_CONTENT_X, UI_CONTENT_Y + i*h);
        m_Selections.AddElement(elm[i]);
    }

    m_Selections.AddElement(&m_Back);
    m_Selections.AddElement(&m_Search);

    m_Selections.Select(&m_Search);

    this->SetFocus(&m_Selections);
}

UiQueryConfig::~UiQueryConfig()
{
    m_Selections.RemoveAllElements();

    m_GameType.RemoveAllElements();
    m_GameMode.RemoveAllElements();
    m_GamesPerMatch.RemoveAllElements();
    m_MatchPointLimit.RemoveAllElements();
}

void
UiQueryConfig::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

const rlMatchingFilter&
UiQueryConfig::GetFilter() const
{
    return m_Filter;
}

bool
UiQueryConfig::IsRanked() const
{
    return false; // return (m_Filter.GetGameType() == RL_GAMETYPE_RANKED);
}

//private:

void
UiQueryConfig::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else if(&m_Search == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SEARCH);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
    else if(UIEVENT_SELECT == evtId)
    {
        if(&m_GameType == el)
        {
            const UiElement* sel = m_GameType.GetSelection();
            if(&m_GtStandard == sel)
            {
                m_Filter.SetSessionPurpose(RL_GAMETYPE_STANDARD);
            }
            else if(&m_GtRanked == sel)
            {
                m_Filter.SetSessionPurpose(RL_GAMETYPE_RANKED);
            }
        }
        else if(&m_GameMode == el)
        {
            const UiElement* sel = m_GameMode.GetSelection();
            if(&m_GmExhib == sel)
            {
                m_Filter.SetGameMode(MatchingAttributes::GAME_MODE_EXHIBITION);
            }
            else if(&m_GmTourn == sel)
            {
                m_Filter.SetGameMode(MatchingAttributes::GAME_MODE_TOURNAMENT);
            }
        }
        else if(&m_GamesPerMatch == el)
        {
            const UiElement* sel = m_GamesPerMatch.GetSelection();
            if(&m_GpmAny == sel)
            {
                m_Filter.ClearValue(1);
            }
            else if(&m_Gpm3 == sel)
            {
                m_Filter.SetValue(1, MatchingAttributes::GAMES_PER_MATCH_BEST_OF_3);
            }
            else if(&m_Gpm5 == sel)
            {
                m_Filter.SetValue(1, MatchingAttributes::GAMES_PER_MATCH_BEST_OF_5);
            }
        }
        else if(&m_MatchPointLimit == el)
        {
            const UiElement* sel = m_MatchPointLimit.GetSelection();
            if(&m_MplAny == sel)
            {
                m_Filter.ClearValue(0);
            }
            else if(&m_Mpl7 == sel)
            {
                m_Filter.SetValue(0, 7);
            }
            else if(&m_Mpl11 == sel)
            {
                m_Filter.SetValue(0, 11);
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiConfigureAttrs
///////////////////////////////////////////////////////////////////////////////
UiConfigureAttrs::UiConfigureAttrs()
: m_NumPublicSlots(32)
, m_NumPrivateSlots(0)
, m_Changeable(true)
{
    m_Attrs.Reset(MatchingAttributes());

    m_Title.SetText("ATTRIBUTES");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiConfigureAttrs::OnEvent);

    m_GmExhib.SetText("Exhibition");
    m_GmTourn.SetText("Tournament");
    m_GameMode.Bind(this, &UiConfigureAttrs::OnEvent);
    m_GameMode.SetText("Game Mode: ");
    m_GameMode.AddElement(&m_GmExhib);
    m_GameMode.AddElement(&m_GmTourn);

    m_NumPublicSlotsMv.Bind(this, &UiConfigureAttrs::OnEvent);
    m_NumPublicSlotsMv.SetText("Public Slots:");
    m_NumPublicSlots0.SetText("0"); 
    m_NumPublicSlots1.SetText("1"); 
    m_NumPublicSlots2.SetText("2");
    m_NumPublicSlots4.SetText("4");
	m_NumPublicSlots16.SetText("16");
	m_NumPublicSlots32.SetText("32");

	m_NumPublicSlotsMv.AddElement(&m_NumPublicSlots32);
	m_NumPublicSlotsMv.AddElement(&m_NumPublicSlots16);
	m_NumPublicSlotsMv.AddElement(&m_NumPublicSlots4);
	m_NumPublicSlotsMv.AddElement(&m_NumPublicSlots2);
	m_NumPublicSlotsMv.AddElement(&m_NumPublicSlots1);
	m_NumPublicSlotsMv.AddElement(&m_NumPublicSlots0);

    m_NumPrivateSlotsMv.Bind(this, &UiConfigureAttrs::OnEvent);
    m_NumPrivateSlotsMv.SetText("Private Slots:");
    m_NumPrivateSlots0.SetText("0"); 
    m_NumPrivateSlots1.SetText("1"); 
    m_NumPrivateSlots2.SetText("2");
    m_NumPrivateSlots4.SetText("4");
    m_NumPrivateSlotsMv.AddElement(&m_NumPrivateSlots0);
    m_NumPrivateSlotsMv.AddElement(&m_NumPrivateSlots1);
    m_NumPrivateSlotsMv.AddElement(&m_NumPrivateSlots2);
    m_NumPrivateSlotsMv.AddElement(&m_NumPrivateSlots4);

    m_Gpm3.SetText("3");
    m_Gpm5.SetText("5");
    m_GamesPerMatch.Bind(this, &UiConfigureAttrs::OnEvent);
    m_GamesPerMatch.SetText("Games Per Match: ");
    m_GamesPerMatch.AddElement(&m_Gpm3);
    m_GamesPerMatch.AddElement(&m_Gpm5);

    m_Mpl7.SetText("7");
    m_Mpl11.SetText("11");
    m_MatchPointLimit.Bind(this, &UiConfigureAttrs::OnEvent);
    m_MatchPointLimit.SetText("Match Point Limit: ");
    m_MatchPointLimit.AddElement(&m_Mpl7);
    m_MatchPointLimit.AddElement(&m_Mpl11);

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);

    m_Commit.SetText("Commit");
    m_Commit.SetXY(UI_RIGHT_CTRL_X, UI_RIGHT_CTRL_Y);

    UiElement* elm[] =
    {
        &m_GameMode,
        &m_NumPublicSlotsMv,
        &m_NumPrivateSlotsMv,
        &m_GamesPerMatch,
        &m_MatchPointLimit,
    };

    const float h = UI_GAP;

    for(unsigned i = 0; i < COUNTOF(elm); ++i)
    {
        elm[i]->SetXY(UI_CONTENT_X, UI_CONTENT_Y + i*h);
        m_Selections.AddElement(elm[i]);
    }

    m_Selections.AddElement(&m_Back);
    m_Selections.AddElement(&m_Commit);

    m_Selections.Select(&m_Commit);

    this->SetChangeable(false);

    this->SetFocus(&m_Selections);
}

UiConfigureAttrs::~UiConfigureAttrs()
{
    m_Selections.RemoveAllElements();

    m_GameMode.RemoveAllElements();
    m_NumPublicSlotsMv.RemoveAllElements();
    m_NumPrivateSlotsMv.RemoveAllElements();
    m_GamesPerMatch.RemoveAllElements();
    m_MatchPointLimit.RemoveAllElements();
}

void
UiConfigureAttrs::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void
UiConfigureAttrs::SetChangeable(const bool changeable)
{
    if(changeable)
    {
        m_Title.SetText("ATTRIBUTES (changeable)");
    }
    else
    {
        m_Title.SetText("ATTRIBUTES (not changeable)");
    }

    m_Changeable = changeable;

    this->RefreshItems();
}

void
UiConfigureAttrs::SetAttrs(const rlMatchingAttributes& attrs,
                            const unsigned numPubSlots,
                            const unsigned numPrivSlots)
{
    m_Attrs = attrs;
    m_NumPublicSlots = numPubSlots;
    m_NumPrivateSlots = numPrivSlots;

    this->RefreshItems();
}

void
UiConfigureAttrs::GetAttrs(rlMatchingAttributes* attrs,
                            unsigned* numPubSlots,
                            unsigned* numPrivSlots) const
{
    *attrs = m_Attrs;
    *numPubSlots = m_NumPublicSlots;
    *numPrivSlots = m_NumPrivateSlots;
}

//private:

void
UiConfigureAttrs::RefreshItems()
{
    switch(m_Attrs.GetGameMode())
    {
    case MatchingAttributes::GAME_MODE_EXHIBITION: m_GameMode.Select(&m_GmExhib); break;
    case MatchingAttributes::GAME_MODE_TOURNAMENT: m_GameMode.Select(&m_GmTourn); break;
    default:
        Assert(false);
    }

    switch(m_NumPublicSlots)
    {
    case 0: m_NumPublicSlotsMv.Select(&m_NumPublicSlots0); break;
    case 1: m_NumPublicSlotsMv.Select(&m_NumPublicSlots1); break;
    case 2: m_NumPublicSlotsMv.Select(&m_NumPublicSlots2); break;
    case 4: m_NumPublicSlotsMv.Select(&m_NumPublicSlots4); break;
	case 16: m_NumPublicSlotsMv.Select(&m_NumPublicSlots16); break;
	case 32: m_NumPublicSlotsMv.Select(&m_NumPublicSlots32); break;
	default:
        Assert(false);
    }

    switch(m_NumPrivateSlots)
    {
    case 0: m_NumPrivateSlotsMv.Select(&m_NumPrivateSlots0); break;
    case 1: m_NumPrivateSlotsMv.Select(&m_NumPrivateSlots1); break;
    case 2: m_NumPrivateSlotsMv.Select(&m_NumPrivateSlots2); break;
    case 4: m_NumPrivateSlotsMv.Select(&m_NumPrivateSlots4); break;
    default:
        Assert(false);
    }

    const unsigned* val =
        m_Attrs.GetValueById(MatchingAttributes::FIELD_ID_GAMES_PER_MATCH);

    if(AssertVerify(val))
    {
        switch(*val)
        {
        case MatchingAttributes::GAMES_PER_MATCH_BEST_OF_3: m_GamesPerMatch.Select(&m_Gpm3); break;
        case MatchingAttributes::GAMES_PER_MATCH_BEST_OF_5: m_GamesPerMatch.Select(&m_Gpm5); break;
        default:
            Assert(false);
        }
    }

    val = m_Attrs.GetValueById(MatchingAttributes::FIELD_ID_MATCH_POINT_LIMIT);

    if(AssertVerify(val))
    {
        switch(*val)
        {
        case 7: m_MatchPointLimit.Select(&m_Mpl7); break;
        case 11: m_MatchPointLimit.Select(&m_Mpl11); break;
        default:
            Assert(false);
        }
    }

    m_Selections.SetEnabled(&m_GameMode, m_Changeable);
    m_Selections.SetEnabled(&m_NumPublicSlotsMv, m_Changeable);
    m_Selections.SetEnabled(&m_NumPrivateSlotsMv, m_Changeable);
    m_Selections.SetEnabled(&m_GamesPerMatch, m_Changeable);
    m_Selections.SetEnabled(&m_MatchPointLimit, m_Changeable);

    this->RefreshCommittable();
}

void
UiConfigureAttrs::RefreshCommittable()
{
    m_Selections.SetEnabled(&m_Commit,
                            m_Changeable && (m_NumPublicSlots || m_NumPrivateSlots));
}

void
UiConfigureAttrs::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else if(&m_Commit == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_COMMIT);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
    else if(UIEVENT_SELECT == evtId)
    {
        if(&m_GameMode == el)
        {
            const UiElement* sel = m_GameMode.GetSelection();
            if(&m_GmExhib == sel)
            {
                m_Attrs.SetGameMode(MatchingAttributes::GAME_MODE_EXHIBITION);
            }
            else if(&m_GmTourn == sel)
            {
                m_Attrs.SetGameMode(MatchingAttributes::GAME_MODE_TOURNAMENT);
            }
        }
        else if(&m_NumPublicSlotsMv == el)
        {
            const UiElement* sel = m_NumPublicSlotsMv.GetSelection();
            if(&m_NumPublicSlots0 == sel)
            {
                m_NumPublicSlots = 0;
            }
            else if(&m_NumPublicSlots1 == sel)
            {
                m_NumPublicSlots = 1;
            }
            else if(&m_NumPublicSlots2 == sel)
            {
                m_NumPublicSlots = 2;
            }
            else if(&m_NumPublicSlots4 == sel)
            {
                m_NumPublicSlots = 4;
            }
			else if(&m_NumPublicSlots16 == sel)
			{
				m_NumPublicSlots = 16;
			}
			else if(&m_NumPublicSlots32 == sel)
			{
				m_NumPublicSlots = 32;
			}

            this->RefreshCommittable();
        }
        else if(&m_NumPrivateSlotsMv == el)
        {
            const UiElement* sel = m_NumPrivateSlotsMv.GetSelection();
            if(&m_NumPrivateSlots0 == sel)
            {
                m_NumPrivateSlots = 0;
            }
            else if(&m_NumPrivateSlots1 == sel)
            {
                m_NumPrivateSlots = 1;
            }
            else if(&m_NumPrivateSlots2 == sel)
            {
                m_NumPrivateSlots = 2;
            }
            else if(&m_NumPrivateSlots4 == sel)
            {
                m_NumPrivateSlots = 4;
            }

            this->RefreshCommittable();
        }
        else if(&m_GamesPerMatch == el)
        {
            const UiElement* sel = m_GamesPerMatch.GetSelection();
            if(&m_Gpm3 == sel)
            {
                m_Attrs.SetValueById(MatchingAttributes::FIELD_ID_GAMES_PER_MATCH,
                                    MatchingAttributes::GAMES_PER_MATCH_BEST_OF_3);
            }
            else if(&m_Gpm5 == sel)
            {
                m_Attrs.SetValueById(MatchingAttributes::FIELD_ID_GAMES_PER_MATCH,
                                    MatchingAttributes::GAMES_PER_MATCH_BEST_OF_5);
            }
        }
        else if(&m_MatchPointLimit == el)
        {
            const UiElement* sel = m_MatchPointLimit.GetSelection();
            if(&m_Mpl7 == sel)
            {
                m_Attrs.SetValueById(MatchingAttributes::FIELD_ID_MATCH_POINT_LIMIT, 7);
            }
            else if(&m_Mpl11 == sel)
            {
                m_Attrs.SetValueById(MatchingAttributes::FIELD_ID_MATCH_POINT_LIMIT, 11);
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiSessionSelect
///////////////////////////////////////////////////////////////////////////////
UiSessionSelect::UiSessionSelect()
{
    const float h = UI_GAP;
    float y = UI_CONTENT_Y;
    for(int i = 0; i < MAX_SESSIONS; ++i)
    {
        m_Join[i].SetText("Join");
        m_View[i].SetText("View");
        m_Sessions[i].AddElement(&m_Join[i]);
        m_Sessions[i].AddElement(&m_View[i]);
        m_Sessions[i].SetXY(UI_CONTENT_X, y);
        y += h;
    }

    m_SessionList.Bind(this, &UiSessionSelect::OnEvent);

    m_Title.SetText("SESSIONS");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);

    this->SetFocus(&m_SessionList);
}

UiSessionSelect::~UiSessionSelect()
{
    for(int i = 0; i < MAX_SESSIONS; ++i)
    {
        m_Sessions[i].RemoveAllElements();
    }
}

void
UiSessionSelect::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_SessionList.Draw();
}

void
UiSessionSelect::AddSession(const int index, const char* text)
{
    if(m_Count < MAX_SESSIONS)
    {
        char buf[128];
        formatf(buf, sizeof(buf), "%s:", text);

        m_Sessions[m_Count].SetText(buf);
        m_SessionIndices[m_Count] = index;
        m_SessionList.AddElement(&m_Sessions[m_Count]);

        if(0 == m_Count)
        {
            m_SessionList.Select(&m_Sessions[m_Count]);
        }

        ++m_Count;
    }
}

void
UiSessionSelect::Clear()
{
    m_SessionList.RemoveAllElements();
    m_SessionList.AddElement(&m_Back);

    for(unsigned i = 0; i < MAX_SESSIONS; i++)
    {
        m_SessionIndices[i] = -1;
    }

    m_Count = 0;
}

int
UiSessionSelect::GetSessionIndex() const
{
    const UiElement* sel = m_SessionList.GetSelection();
    const int selected = sel ? ptrdiff_t_to_int((const UiMultiValue*) sel - m_Sessions) : -1;
    
    if(selected >= 0 && selected < MAX_SESSIONS)
    {
        return m_SessionIndices[selected];
    }
    else
    {
        return -1;
    }
}

//private:

void
UiSessionSelect::OnEvent(UiElement* el, const int evtId, void*)
{
    Assert(&m_SessionList == el);

    if(UIEVENT_ACTION == evtId && &m_SessionList == el)
    {
        const UiElement* sel = m_SessionList.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else if(this->GetSessionIndex() >= 0)
        {
            const int idx = this->GetSessionIndex();
            if(&m_View[idx] == m_Sessions[idx].GetSelection())
            {
                this->FireEvent(UIEVENT_ACTION, (void*)ACTION_VIEW_DETAILS);
            }
            else if(&m_Join[idx] == m_Sessions[idx].GetSelection())
            {
                this->FireEvent(UIEVENT_ACTION, (void*)ACTION_JOIN);
            }
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiSessionDetails
///////////////////////////////////////////////////////////////////////////////
UiSessionDetails::UiSessionDetails()
{
    UiElement* elm[] =
    {
        &m_GameType,
        &m_GameMode,
        &m_GamesPerMatch,
        &m_MatchPointLimit,
    };

    const float h = UI_GAP;

    for(unsigned i = 0; i < COUNTOF(elm); ++i)
    {
        elm[i]->SetXY(UI_CONTENT_X, UI_CONTENT_Y + i*h);
        m_Details.AddElement(elm[i]);
    }

    m_Selections.Bind(this, &UiSessionDetails::OnEvent);

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);

    m_Join.SetText("Join");
    m_Join.SetXY(UI_RIGHT_CTRL_X, UI_RIGHT_CTRL_Y);

    m_Selections.AddElement(&m_Back);
    m_Selections.AddElement(&m_Join);

    m_Selections.Select(&m_Join);

    this->SetFocus(&m_Selections);
}

UiSessionDetails::~UiSessionDetails()
{
    m_Selections.RemoveAllElements();
    m_Details.RemoveAllElements();
}

void
UiSessionDetails::SetSessionDetail(const rlSessionDetail& detail)
{
    char buf[128];
    formatf(buf, sizeof(buf), "SESSION DETAILS: %s", detail.m_HostName);
    m_Title.SetText(buf);
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    const rlMatchingAttributes& attrs = detail.m_SessionConfig.m_Attrs;

    formatf(buf, sizeof(buf), "Game Type: Standard");
    m_GameType.SetText(buf);

    formatf(buf,
            sizeof(buf),
            "Game Mode: %s",
            attrs.GetGameMode() == MatchingAttributes::GAME_MODE_EXHIBITION
                ? "Exhibition"
                : "Tournament");
    m_GameMode.SetText(buf);

    const u32* val = attrs.GetValueById(MatchingAttributes::FIELD_ID_GAMES_PER_MATCH);
    Assert(val);

    formatf(buf,
            sizeof(buf),
            "Games per Match: %s",
            *val == MatchingAttributes::GAMES_PER_MATCH_BEST_OF_3 ? "3" : "5");
    m_GamesPerMatch.SetText(buf);

    val = attrs.GetValueById(MatchingAttributes::FIELD_ID_MATCH_POINT_LIMIT);
    Assert(val);

    formatf(buf, sizeof(buf), "Match Point Limit: %d", *val);
    m_MatchPointLimit.SetText(buf);
}

void
UiSessionDetails::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Details.Draw();
    m_Selections.Draw();
}

//private:

void
UiSessionDetails::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else if(&m_Join == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_JOIN);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiLobby
///////////////////////////////////////////////////////////////////////////////
UiLobby::UiLobby()
    : m_LiveMgr(0)
    , m_IsInvitable(true)
	, m_PlayerIndex(0)
	, m_App(NULL)
{
    m_Title.SetText("LOBBY");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    const float h = UI_GAP;
    float x = 400;
    float y = UI_CONTENT_Y;
    for(int i = 0; i < COUNTOF(m_GamerNames); ++i)
    {
        m_GamerNames[i].SetXY(x, y);
        y += h;
    }

    m_Selections.Bind(this, &UiLobby::OnEvent);

    y = UI_CONTENT_Y;

#if RSG_PC && !__FINAL
	if(!PARAM_takehometest.Get())
#endif
	{
		ADD_SELECTION(m_Leave, "Leave", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_MigrateHost, "Become Host", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_FriendsList, "Friends", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_Start, "Start", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_ConfigureAttrs, "Attributes", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_Invitable, "Invitable", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_Bandwidth, "", UI_CONTENT_X, y); y += UI_GAP;

#if RSG_ORBIS
		ADD_SELECTION(m_GetSessionData, "GET SessionData", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_GetChangeableData, "GET Changeable Data", UI_CONTENT_X, y); y += UI_GAP;
#endif

		ADD_SELECTION(m_ShowProfile, "Show Profile", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_AddFriend, "Add Friend", UI_CONTENT_X, y); y += UI_GAP;
		ADD_SELECTION(m_Bail, "Bail", UI_CONTENT_X, y); y += UI_GAP;
	}

    m_Selections.Select(&m_Start);

    this->SetFocus(&m_Selections);
}

UiLobby::~UiLobby()
{
    m_Selections.RemoveAllElements();
}

void
UiLobby::Init(App* app, LiveManager* liveMgr)
{
    Assert(!m_LiveMgr);
    m_LiveMgr = liveMgr;
	m_App = app;
}

void
UiLobby::Shutdown()
{
    m_LiveMgr = 0;
    m_GamerList.RemoveAllElements();
    UiElement::Shutdown();
}

void
UiLobby::Draw()
{
    this->UiElement::Draw();

    m_Title.Draw();

    m_GamerList.RemoveAllElements();

    snSession* session = m_LiveMgr->GetSession();
    rlGamerInfo gamers[RL_MAX_GAMERS_PER_SESSION];

    const int gamerCount = session->GetGamers(gamers, COUNTOF(gamers));

	Peer* peers[PeerManager::MAX_PEERS];
	const unsigned numPeers = m_App->GetPeerManager()->GetPeers(peers, COUNTOF(peers));

    u64 hostPeerId;
    m_LiveMgr->GetSession()->GetHostPeerId(&hostPeerId);

	enum CxnQuality
	{
		CXN_QUAL_DIRECT,
		CXN_QUAL_PEER_RELAY_2_HOPS,
		CXN_QUAL_PEER_RELAY_3_HOPS,
		CXN_QUAL_PEER_RELAY_4_HOPS,
		CXN_QUAL_PEER_RELAY_5_HOPS,
		CXN_QUAL_PEER_RELAY_MORE_HOPS,
		CXN_QUAL_RELAY_SERVER,
		NUM_CXN_QUALITIES
	};

	static Color32 cxnQualityColours[NUM_CXN_QUALITIES] = {
																Color32(255, 255, 255), // direct
																Color32(158, 249, 255), // peer relay, 2 hops
																Color32(96, 212, 255), // peer relay, 3 hops
																Color32(0, 187, 255), // peer relay, 4 hops
																Color32(0, 140, 255), // peer relay, 5 hops
																Color32(0, 140, 255), // peer relay, 6+ hops
																Color32(255, 170, 215), // relay server
														  };	


    for(int i = 0; i < gamerCount; ++i)
    {
        bool hasHeadset = gamers[i].IsLocal()
                            && m_LiveMgr->GetVoice().IsInitialized()
                            && m_LiveMgr->GetVoice().HasHeadset(gamers[i].GetLocalIndex());
        bool isSpeaking = m_LiveMgr->GetVoice().IsInitialized()
                            && m_LiveMgr->GetVoice().IsTalking(gamers[i].GetGamerId());

		char szVolume[11] = {0};

		if (gamers[i].IsLocal())
		{
			float volume = m_LiveMgr->GetVoice().GetLoudness(gamers[i].GetLocalIndex());

			// exaggerate the volume for the UI since loudness is logarithmic
			volume = sqrt(volume);

			char szBars[11] = "||||||||||";

			unsigned numBars = (unsigned)((volume * 100) / 10);
			safecpy(szVolume, szBars, (int)numBars + 1);
		}

		EndpointId endpointId = NET_INVALID_ENDPOINT_ID;
		for(unsigned j = 0; j < numPeers; ++j)
		{
			if(peers[j]->HasGamer(gamers[i]))
			{
				endpointId = peers[j]->GetEndpointId();
				break;
			}
		}
		
		const netAddress& addr = NET_IS_VALID_ENDPOINT_ID(endpointId) ? m_LiveMgr->GetCxnMgr()->GetAddress(endpointId) : netAddress::INVALID_ADDRESS;

		char natTypeStr[32] = "Unknown NAT Type";
		netNatType natType = gamers[i].GetPeerInfo().GetPeerAddress().GetNatType();
		switch(natType)
		{
			case NET_NAT_OPEN: safecpy(natTypeStr, "Open"); break;
			case NET_NAT_MODERATE: safecpy(natTypeStr, "Moderate"); break;
			case NET_NAT_STRICT: safecpy(natTypeStr, "Strict"); break;
			default: safecpy(natTypeStr, "Unknown NAT Type"); break;
		}

		CxnQuality cxnQuality = CXN_QUAL_DIRECT;

		char addrBuf[netAddress::MAX_STRING_BUF_SIZE + 128] = {0};
		if(addr.IsValid())
		{
			addr.FormatAttemptIpV4(addrBuf);

			if(addr.IsRelayServerAddr())
			{
				cxnQuality = CXN_QUAL_RELAY_SERVER;
			}
			else if(addr.IsPeerRelayAddr())
			{
				unsigned hopCount = m_LiveMgr->GetCxnMgr()->GetHopCount(endpointId);
				Assert(hopCount >= 2);
				cxnQuality = (CxnQuality)Min((int)(CXN_QUAL_PEER_RELAY_MORE_HOPS), (int)(CXN_QUAL_PEER_RELAY_2_HOPS + (hopCount - 2)));
				safecatf(addrBuf, " HopCount: %u", hopCount);
			}
		}
		else
		{
			if(!gamers[i].IsLocal())
			{
				safecpy(addrBuf, "Not connected");
			}
		}

        char buf[256];
        formatf(buf, COUNTOF(buf), "%s (%s) %s %s %s %s",
#if RSG_DURANGO
                gamers[i].GetDisplayName(),
#else
				gamers[i].GetName(),
#endif
                gamers[i].IsLocal() ? "local" : "remote",
                gamers[i].GetPeerInfo().GetPeerId() == hostPeerId ? "(host)" : "",
                hasHeadset ? "(headset)" : "",
				addrBuf,			
                isSpeaking ? szVolume : "");

		if(gamers[i].IsRemote())
		{
			m_GamerNames[i].SetColor(cxnQualityColours[cxnQuality]);
		}

        m_GamerNames[i].SetText(buf);
        m_GamerList.AddElement(&m_GamerNames[i]);
    }

	// for leavers
	if (m_PlayerIndex >= m_GamerList.GetElementCount())
	{
		m_PlayerIndex = 0;
		m_GamerList.ResetFocus();
	}

	// set the relevant focus
	for (int i = 0; i < m_PlayerIndex; i++)
	{
		m_GamerList.Next();
	}

    m_GamerList.Draw();
    m_Selections.Draw();
}


bool 
UiLobby::OnPageLeft()
{
	m_PlayerIndex--;
	if (m_PlayerIndex < 0)
	{
		m_PlayerIndex = m_GamerList.GetElementCount() > 0 ? m_GamerList.GetElementCount() - 1 : 0;
	}
	
	return true;
}

bool
UiLobby::OnPageRight()
{
	m_PlayerIndex++;
	if (m_PlayerIndex >= m_GamerList.GetElementCount())
	{
		m_PlayerIndex = 0;
	}

	return true;
}

rlGamerInfo
UiLobby::GetSelection()
{
	rlGamerInfo gamers[RL_MAX_GAMERS_PER_SESSION];
	snSession* session = m_LiveMgr->GetSession();
	session->GetGamers(gamers, COUNTOF(gamers));
	return gamers[m_PlayerIndex];
}

void
UiLobby::SetInvitable(const bool invitable)
{
    m_Invitable.SetText(invitable ? "Invitable" : "Not invitable");
    m_IsInvitable = invitable;
}

void
UiLobby::SetBecomeHostOptionEnabled(const bool enabled)
{
    m_Selections.SetEnabled(&m_MigrateHost, enabled);
}

void
UiLobby::SetInviteToggleEnabled(const bool enabled)
{
    m_Selections.SetEnabled(&m_Invitable, enabled);
}

void
UiLobby::SetBandwidth(const unsigned inboundBytesPerSecond,
                      const unsigned inboundPacketsPerSecond,
                      const unsigned outboundBytesPerSecond,
                      const unsigned outboundPacketsPerSecond)
{
    char buf[128];
    formatf(buf, sizeof(buf), 
            "Bandwidth Bps(Pps):  in=%u(%u) out=%u(%u)",
            inboundBytesPerSecond,
            inboundPacketsPerSecond,
            outboundBytesPerSecond,
            outboundPacketsPerSecond);

    m_Bandwidth.SetText(buf);
}

//private:

void
UiLobby::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Leave == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_LEAVE);
        }
		if(&m_MigrateHost == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_MIGRATE_HOST);
		}
		else if(&m_Start == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_START);
        }
        else if(&m_FriendsList == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_FRIENDS_LIST);
        }
        else if(&m_ConfigureAttrs == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CONFIGURE_ATTRIBUTES);
        }
        else if(&m_Invitable == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_TOGGLE_INVITABLE);
        }
		else if(&m_GetSessionData == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_SESSION_DATA);
		}
		else if(&m_GetChangeableData == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_CHANGEABLE_DATA);
		}
		else if(&m_ShowProfile == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_PROFILE);
		}
		else if(&m_AddFriend == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_ADD_FRIEND);
		}
		else if(&m_Bail == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BAIL);
		}
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiGame
///////////////////////////////////////////////////////////////////////////////
UiGame::UiGame()
    : m_App(NULL)
{
    m_Title.SetText("GAME");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    const float h = UI_GAP;
    float x = 400;
    float y = UI_CONTENT_Y;
    m_Header.SetXY(x, y);
    y += h;

    char buf[256];
    formatf(buf,
            sizeof(buf),
            "%-30s%8s%8s%8s%8s%8s%8s%8s%8s",
            "",
            "Swings",
            "Strikes",
            "Singles",
            "Doubles",
            "Triples",
            "Homers",
            "FlyOuts",
            "B/A");
    m_Header.SetText(buf);

    for(int i = 0; i < COUNTOF(m_GamerNames); ++i)
    {
        m_GamerNames[i].SetXY(x, y);
        y += h;
    }

    m_Selections.Bind(this, &UiGame::OnEvent);

    x = UI_CONTENT_X;
    y = UI_CONTENT_Y;

    m_Leave.SetText("Leave");
    m_Leave.SetXY(x, y);
    y += h;

    m_Swing.SetText("Swing");
    m_Swing.SetXY(x, y);
    y += h;

    m_End.SetText("End");
    m_End.SetXY(x, y);
    y += h;

    m_Selections.AddElement(&m_Leave);
    m_Selections.AddElement(&m_Swing);
    m_Selections.AddElement(&m_End);

    m_Selections.Select(&m_End);

    this->SetFocus(&m_Selections);
}

UiGame::~UiGame()
{
    m_Selections.RemoveAllElements();
}

void
UiGame::Init(App* app)
{
    Assert(!m_App);
    m_App = app;
}

void
UiGame::Shutdown()
{
    m_App = NULL;
    m_GamerList.RemoveAllElements();
    UiElement::Shutdown();
}

void
UiGame::Draw()
{
    this->UiElement::Draw();

    m_Title.Draw();
    m_Header.Draw();

    m_GamerList.RemoveAllElements();

    Gamer* gamers[RL_MAX_GAMERS_PER_SESSION] = {0};

    const unsigned gamerCount = m_App->GetGamers(gamers, COUNTOF(gamers));

    u64 hostPeerId;
    m_App->GetLiveMgr()->GetSession()->GetHostPeerId(&hostPeerId);

    for(unsigned i = 0; i < gamerCount; ++i)
    {
        char name[128];
        formatf(name, COUNTOF(name), "%s (%s) %s",
                gamers[i]->GetName(),
                gamers[i]->IsLocal() ? "local" : "remote",
                gamers[i]->GetPeerInfo().GetPeerId() == hostPeerId ? "(host)" : "");

        char buf[256];

    formatf(buf,
                sizeof(buf),
                "%-30s%8d%8d%8d%8d%8d%8d%8d%8d",
                name,
                gamers[i]->SwingCount(),
                gamers[i]->StrikeCount(),
                gamers[i]->HitResultCount(HIT_SINGLE),
                gamers[i]->HitResultCount(HIT_DOUBLE),
                gamers[i]->HitResultCount(HIT_TRIPLE),
                gamers[i]->HitResultCount(HIT_HOME_RUN),
                gamers[i]->HitResultCount(HIT_FLY_OUT),
                0   //B/A
                );

        m_GamerNames[i].SetText(buf);
        m_GamerList.AddElement(&m_GamerNames[i]);
    }

    m_GamerList.Draw();
    m_Selections.Draw();
}

//private:

void
UiGame::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Leave == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_LEAVE);
        }
        else if(&m_End == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_END);
        }
        else if(&m_Swing == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SWING);
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiSocialClubMainMenu
///////////////////////////////////////////////////////////////////////////////
UiSocialClubMainMenu::UiSocialClubMainMenu()
{
    m_Title.SetText("Social Club");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiSocialClubMainMenu::OnEvent);

    float y = UI_CONTENT_Y;

    ADD_SELECTION(m_LinkOrUnlinkAccount, "Link Account", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_CreateAccount, "Create Account", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GetCountries, "Get Countries", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GetTermsOfService, "Get Accepted Terms of Service Version", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_AcceptTermsOfService, "Accept Terms of Service", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ResetPassword, "Request Reset Password E-mail", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_UpdatePassword, "Update Password", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_PostUserFeedActivity, "Post User Feed Activity (SHARE_SPOTIFY_SONG)", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GetScAuthToken, "Get SC Auth Token", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_CountAllFriends, "Count All Friends", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ReadFriends, "Read Friends", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ReadFriendsAndInvitesSent, "Read Friends & Invites Sent", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ReadInvitesSent, "Read Invites Sent", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ReadInvitesReceived, "Read Invites Received", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ReadBlocked, "Read Blocked", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_CheckEmail, "Check Email", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_CheckNickname, "Check Nickname", UI_CONTENT_X, y); y += UI_GAP;

#if RSG_PC
	ADD_SELECTION(m_GoOnline, "Go Online", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_Goffline, "Go Offline", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_PostScTelemetry, "Post SC Telemetry Msg", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_DeviceLostReset, "Device Lost/Reset", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_CreateScAuthToken, "Create SC Auth Token", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_CreateSignInTransfer, "Create Signin Transfer", UI_CONTENT_X, y); y += UI_GAP;
#endif

    ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

    this->SetFocus(&m_Selections);
}

UiSocialClubMainMenu::~UiSocialClubMainMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiSocialClubMainMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();

#if !RLROS_SC_PLATFORM
    if(rlSocialClub::IsConnectedToSocialClub(0))
    {
        m_LinkOrUnlinkAccount.SetText("Unlink Account");
    }
    else
    {
        m_LinkOrUnlinkAccount.SetText("Link Account");
    }
#endif

    m_Selections.Draw();
}

void
UiSocialClubMainMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();

        if(&m_GetTermsOfService == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_TERMS_OF_SERVICE);
        }
        else if(&m_LinkOrUnlinkAccount == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_LINK_OR_UNLINK_ACCOUNT);
        }
        else if(&m_CreateAccount == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CREATE_ACCOUNT);
        }
        else if(&m_AcceptTermsOfService == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_ACCEPT_TERMS_OF_SERVICE);
        }
        else if(&m_ResetPassword == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_RESET_PASSWORD);
        }
        else if(&m_GetCountries == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_COUNTRIES);
        }
        else if(&m_UpdatePassword == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_UPDATE_PASSWORD);
        }
		else if(&m_PostUserFeedActivity == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_POST_USER_FEED_ACTIVITY);
		}
        else if(&m_GetScAuthToken == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_SCAUTHTOKEN);
        }
        else if(&m_CountAllFriends == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_COUNT_ALL_FRIENDS);
        }
        else if(&m_ReadFriends == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_FRIENDS);
        }
        else if(&m_ReadFriendsAndInvitesSent == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_FRIENDS_AND_INVITES_SENT);
        }
        else if(&m_ReadInvitesSent == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_INVITES_SENT);
        }
        else if(&m_ReadInvitesReceived == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_INVITES_RECEIVED);
        }
        else if(&m_ReadBlocked == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_BLOCKED);
        }
        else if(&m_CheckEmail == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CHECK_EMAIL);
        }
        else if(&m_CheckNickname == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CHECK_NICKNAME);
        }
#if RSG_PC
		else if(&m_GoOnline == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GO_ONLINE);
		}
		else if(&m_Goffline == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GO_OFFLINE);
		}
		else if(&m_PostScTelemetry == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_POST_SC_TELEMETRY);
		}
		else if(&m_DeviceLostReset == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_DEVICE_LOST_RESET);
		}
		else if(&m_CreateScAuthToken == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CREATE_SC_AUTH_TOKEN);
		}
		else if (&m_CreateSignInTransfer == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CREATE_SIGNIN_TRANSFER);
		}
#endif
		else if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
		

    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiCommerceAndEntitlementsMainMenu
///////////////////////////////////////////////////////////////////////////////
UiCommerceAndEntitlementsMainMenu::UiCommerceAndEntitlementsMainMenu()
{
	m_Title.SetText("Commerce and Entitlements");
	m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

	m_Selections.Bind(this, &UiCommerceAndEntitlementsMainMenu::OnEvent);

	float y = UI_CONTENT_Y;

	ADD_SELECTION(m_StartCommerce, "Start Commerce", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_GetCommerceData, "Get Commerce Data", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_DumpCommerceData, "Dump Commerce Data", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_ShutdownCommerce, "Shutdown Commerce", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_SubmitTestVoucherForContent, "Get test voucher content", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_ConsumeTestVoucherForContent, "Consume previously fetched voucher content", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_TestFetchCheckoutUrl, "Get a checkout url for test product", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_TestGetAgeAppropriateForStore, "Get data for whether current user is old enough to access store", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_StartSteamTestPurchase, "Start steam test purchase", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_RequestEntitlementData, "Start Entitlement Data Fetch", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_DumpEntitlementData, "Dump Entitlement Data", UI_CONTENT_X, y); y += UI_GAP;




	ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

	this->SetFocus(&m_Selections);
}

UiCommerceAndEntitlementsMainMenu::~UiCommerceAndEntitlementsMainMenu()
{
	m_Selections.RemoveAllElements();
}

void
	UiCommerceAndEntitlementsMainMenu::Draw()
{
	this->UiElement::Draw();
	m_Title.Draw();

	m_Selections.Draw();
}

void
	UiCommerceAndEntitlementsMainMenu::OnEvent(UiElement* el, const int evtId, void*)
{
	if(UIEVENT_ACTION == evtId && &m_Selections == el)
	{
		const UiElement* sel = m_Selections.GetSelection();

		if(&m_StartCommerce == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_START_COMMERCE);
		}
		else if(&m_GetCommerceData == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_COMMERCE_DATA);
		}
		else if(&m_DumpCommerceData == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_DUMP_COMMERCE);
		}
		else if(&m_ShutdownCommerce == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHUTDOWN_COMMERCE);
		}
		else if(&m_SubmitTestVoucherForContent == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SUBMIT_TEST_VOUCHER_FOR_CONTENT);
		}
		else if(&m_ConsumeTestVoucherForContent == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CONSUME_PREVIEWED_VOUCHER);
		}
		else if(&m_TestFetchCheckoutUrl == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_TEST_CHECKOUT_URL_FETCH);
		}
		else if(&m_TestGetAgeAppropriateForStore == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_TEST_AGE_FOR_STORE);
		}
		else if (&m_StartSteamTestPurchase == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_START_STEAM_TEST_PURCHASE);
		}
		else if (&m_RequestEntitlementData == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_REQUEST_ENTITLEMENT_DATA);
		}
		else if (&m_DumpEntitlementData == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_DUMP_ENTITLEMENT_DATA);
		}
		else if(&m_Back == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
		}
	}
	else if(UIEVENT_BACK == evtId)
	{
		this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
	}
}

///////////////////////////////////////////////////////////////////////////////
//UiAchievementsMainMenu
///////////////////////////////////////////////////////////////////////////////
int UiAchievementsMainMenu::m_AchievementIndex = 0;
UiAchievementsMainMenu::UiAchievementsMainMenu()
{
	m_Title.SetText("Achievements");
	m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

	m_Selections.Bind(this, &UiAchievementsMainMenu::OnEvent);

	float y = UI_CONTENT_Y;

	ADD_SELECTION(m_AwardAchievement, "Award Achievement (0)", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_ReadAchievements, "Read Achievements", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_DeleteAchievements, "Delete Achievements", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GetRosAchievements, "Get ROS Achievements", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GetRosAchievementDefinitions, "Get ROS Achievement Definitions", UI_CONTENT_X, y); y += 2 * UI_GAP;

#if RSG_DURANGO	
	safecpy(m_Stat, "ACH01Event");

	char statBuf[256] = {0};
	formatf(statBuf, "Read Stat (Current: %s)", m_Stat);

	ADD_SELECTION(m_ReadStat, statBuf, UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_SetStat, "Set Stat", UI_CONTENT_X, y); y += UI_GAP;
#endif

	ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

	m_AchievementIndex = 0;
	m_bWaitingForKeyboard = false;

#if RSG_DURANGO
	sm_VirtualKeyboardResult[0] = '\0';
#endif

	this->SetFocus(&m_Selections);
}

UiAchievementsMainMenu::~UiAchievementsMainMenu()
{
	m_Selections.RemoveAllElements();
}

void
UiAchievementsMainMenu::Draw()
{
	this->UiElement::Draw();
	m_Title.Draw();
	m_Selections.Draw();

#if RSG_DURANGO
	if (m_bWaitingForKeyboard)
	{
		if (App::GetVirtualKeyboard()->Succeeded())
		{
			char statBuf[256] = {0};
			sysMemSet(&m_Stat, 0, sizeof(m_Stat));

			char16* kbdData = App::GetVirtualKeyboard()->GetResult();
			WideToUtf8(m_Stat, kbdData, 256);

			formatf(statBuf, "Read Stat (Current: %s)", m_Stat);
			m_ReadStat.SetText(&statBuf[0], 256);

			m_bWaitingForKeyboard = false;
		}
	}
#endif
}

bool UiAchievementsMainMenu::OnLeft()
{
	if (m_Selections.GetSelection() == &m_AwardAchievement)
	{
		m_AchievementIndex--;
		if (m_AchievementIndex < 0)
			m_AchievementIndex = MAX_ACHIEVEMENTS;

		char textBuf[256] = {0};
		formatf(textBuf, "Award Achievement (%d)", m_AchievementIndex);
		m_AwardAchievement.SetText(&textBuf[0], 256);
	}

	return true;
}

bool UiAchievementsMainMenu::OnRight()
{
	if (m_Selections.GetSelection() == &m_AwardAchievement)
	{
		m_AchievementIndex++;
		if (m_AchievementIndex >= MAX_ACHIEVEMENTS)
			m_AchievementIndex = 0;

		char textBuf[256] = {0};
		formatf(textBuf, "Award Achievement (%d)", m_AchievementIndex);
		m_AwardAchievement.SetText(&textBuf[0], 256);
	}

	return true;
}

void
UiAchievementsMainMenu::OnEvent(UiElement* el, const int evtId, void*)
{
	if(UIEVENT_ACTION == evtId && &m_Selections == el)
	{
		const UiElement* sel = m_Selections.GetSelection();

		if(&m_AwardAchievement == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_AWARD_ACHIEVEMENT);
		}
		else if(&m_ReadAchievements == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_ACHIEVEMENTS);
		}
		else if(&m_DeleteAchievements == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_DELETE_ACHIEVEMENT);
		}
        else if(&m_GetRosAchievements == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_ROS_ACHIEVEMENTS);
        }
        else if(&m_GetRosAchievementDefinitions == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_ROS_ACHIEVEMENT_DEFINITIONS);
        }
		else if(&m_Back == sel)
		{
			m_bWaitingForKeyboard = false;
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
		}
#if RSG_DURANGO
		else if (&m_SetStat == sel)
		{
			ioVirtualKeyboard::Params params;
			char16 defaultText[25];
			Utf8ToWide(defaultText, sm_VirtualKeyboardResult, 25);

			params.m_InitialValue = defaultText;
			params.m_KeyboardType = ioVirtualKeyboard::kTextType_DEFAULT;
			params.m_Description = _C16("Sample");
			params.m_Title = _C16("Sample");
			params.m_MaxLength = 25;
			params.m_PlayerIndex = 0;

			App::GetVirtualKeyboard()->Show(params);
			m_bWaitingForKeyboard = true;
		}
		else if (&m_ReadStat == sel)
		{
			g_rlXbl.GetAchivementManager()->ReadStatInt(0, &m_Stat[0], strlen(m_Stat), &m_IntStat, &statStatus);
		}
#endif
	}
	else if(UIEVENT_BACK == evtId)
	{
		m_bWaitingForKeyboard = false;
		this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
	}
}

///////////////////////////////////////////////////////////////////////////////
//UiProfileStatsMainMenu
///////////////////////////////////////////////////////////////////////////////
UiProfileStatsMainMenu::UiProfileStatsMainMenu()
{
    m_Title.SetText("Profile Stats");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiProfileStatsMainMenu::OnEvent);

    float y = UI_CONTENT_Y;

    ADD_SELECTION(m_ProfileStatsSynchronize,"Synchronize", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ProfileStatsReadByGamer,"Read By Gamer", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ProfileStatsSetDirty,"Set Dirty", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ProfileStatsFlush,"Flush", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_ProfileStatsResetByGroup,"Reset Stats By Group", UI_CONTENT_X, y); y += UI_GAP;

    ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

    this->SetFocus(&m_Selections);
}

UiProfileStatsMainMenu::~UiProfileStatsMainMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiProfileStatsMainMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void
UiProfileStatsMainMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();

        if(&m_ProfileStatsSynchronize == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SYNCHRONIZE);
        }
        else if(&m_ProfileStatsReadByGamer == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_BY_GAMER);
        }
        else if(&m_ProfileStatsSetDirty == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SET_DIRTY);
        }
        else if(&m_ProfileStatsFlush == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_FLUSH);
        }
		else if(&m_ProfileStatsResetByGroup == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_RESET_BY_GROUP_MENU);
		}
        else if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiProfileStatsResetByGamerMenu
///////////////////////////////////////////////////////////////////////////////
UiProfileStatsResetByGroupMenu::UiProfileStatsResetByGroupMenu()
{
	m_Title.SetText("PROFILE STATS RESET STATS BY GAMER");
	m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

	m_Selections.Bind(this, &UiProfileStatsResetByGroupMenu::OnEvent);

	float y = UI_CONTENT_Y;

	m_GroupNumberToReset.SetMin(0);
	m_GroupNumberToReset.SetIncrement(1);

	ADD_SELECTION(m_GroupNumberToReset,"Group To Reset", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_ResetStatsGroupForPlayer,"Reset Stats for Group", UI_CONTENT_X, y); y += UI_GAP;

	ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

	this->AddElement(&m_Title);
	this->AddElement(&m_Selections);

	this->SetFocus(&m_Selections);
}

UiProfileStatsResetByGroupMenu::~UiProfileStatsResetByGroupMenu()
{
	m_Selections.RemoveAllElements();
}

void
UiProfileStatsResetByGroupMenu::Draw()
{
	this->UiElement::Draw();
	m_Title.Draw();
	m_Selections.Draw();
}

void
UiProfileStatsResetByGroupMenu::OnEvent(UiElement* el, const int evtId, void*)
{
	if(UIEVENT_ACTION == evtId && &m_Selections == el)
	{
		const UiElement* sel = m_Selections.GetSelection();

		if(&m_ResetStatsGroupForPlayer == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_RESET);
		}
		else if(&m_Back == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
		}
	}
	else if(UIEVENT_BACK == evtId)
	{
		this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
	}
}


///////////////////////////////////////////////////////////////////////////////
//UiCommunityStatsMainMenu
///////////////////////////////////////////////////////////////////////////////
UiCommunityStatsMainMenu::UiCommunityStatsMainMenu()
{
    m_Title.SetText("Community Stats");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiCommunityStatsMainMenu::OnEvent);

    float y = UI_CONTENT_Y;

    ADD_SELECTION(m_ReadStats,"Read Stats", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ReadHistory,"Read Stat History", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ReadHistorySet,"Read Stat History Set", UI_CONTENT_X, y); y += UI_GAP;

    ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

    this->SetFocus(&m_Selections);
}

UiCommunityStatsMainMenu::~UiCommunityStatsMainMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiCommunityStatsMainMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void
UiCommunityStatsMainMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();

        if(&m_ReadStats == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ);
        }
        else if(&m_ReadHistory == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_HISTORY);
        }
        else if(&m_ReadHistorySet == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_READ_HISTORY_SET);
        }
        else if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiClansMainMenu
///////////////////////////////////////////////////////////////////////////////
UiClansMainMenu::UiClansMainMenu()
{
    m_Title.SetText("Clans");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiClansMainMenu::OnEvent);

    const float h = UI_GAP;
    for(unsigned i = 0; i < UiClansMainMenu::MAX_MEMBERSHIP_PER_PAGE; i++)
    {
        m_Leave[i].SetText("Leave");
        m_ShowMembers[i].SetText("Members");
        m_ShowRanks[i].SetText("Ranks");
        m_ShowRequests[i].SetText("Requests");
        m_ShowMessages[i].SetText("Messages");
        m_ShowMetadata[i].SetText("Metadata");
        m_SetPrimary[i].SetText("Set Primary");
        m_FeudStat[i].SetText("Feud Stat");

        m_Clans[i].AddElement(&m_Leave[i]);
        m_Clans[i].AddElement(&m_ShowMembers[i]);
        m_Clans[i].AddElement(&m_ShowRanks[i]);
        m_Clans[i].AddElement(&m_ShowRequests[i]);
        m_Clans[i].AddElement(&m_ShowMessages[i]);
        m_Clans[i].AddElement(&m_ShowMetadata[i]);
        m_Clans[i].AddElement(&m_SetPrimary[i]);
        m_Clans[i].AddElement(&m_FeudStat[i]);
        m_Clans[i].Bind(this, &UiClansMainMenu::OnEvent);
        m_Clans[i].SetText("");
        m_Clans[i].SetXY(UI_CONTENT_X, UI_CONTENT_Y + i*h);
    }

    m_ShowInvites.SetText("Invites");
    m_ShowInvites.SetXY(UI_RIGHT_CTRL_X, UI_RIGHT_CTRL_Y + 0 * h);
    m_Selections.AddElement(&m_ShowInvites);

    m_ShowSentRequests.SetText("Sent Requests");
    m_ShowSentRequests.SetXY(UI_RIGHT_CTRL_X, UI_RIGHT_CTRL_Y + 1 * h);
    m_Selections.AddElement(&m_ShowSentRequests);

    m_ShowAll.SetText("List All");
    m_ShowAll.SetXY(UI_RIGHT_CTRL_X, UI_RIGHT_CTRL_Y + 2 * h);
    m_Selections.AddElement(&m_ShowAll);

    m_Create.SetText("Create Clan");
    m_Create.SetXY(UI_RIGHT_CTRL_X, UI_RIGHT_CTRL_Y + 3 * h);
    m_Selections.AddElement(&m_Create);

    m_GetPrimary.SetText("Get Primary");
    m_GetPrimary.SetXY(UI_RIGHT_CTRL_X, UI_RIGHT_CTRL_Y + 4 * h);
    m_Selections.AddElement(&m_GetPrimary);

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);
    m_Selections.AddElement(&m_Back);

    this->SetFocus(&m_Selections);
}

UiClansMainMenu::~UiClansMainMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiClansMainMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void 
UiClansMainMenu::Populate(const rlClanMembershipData* mine,
                          const unsigned myCount)
{
    m_Selections.RemoveAllElements();

    char clanLine[256];

    for(unsigned i=0; i < UiClansMainMenu::MAX_MEMBERSHIP_PER_PAGE; i++)
    {
        if(i < myCount)
        {
            formatf(clanLine, COUNTOF(clanLine), "%s %s:%" I64FMT "d [%s] (%s[%d])",
                mine[i].m_IsPrimary ? "*" : "",
                mine[i].m_Clan.m_ClanName,
                mine[i].m_Clan.m_Id,
                mine[i].m_Clan.m_ClanTag,
                mine[i].m_Rank.m_RankName,
                mine[i].m_Rank.m_RankOrder);

            m_Clans[i].SetText(clanLine);

            m_Selections.AddElement(&m_Clans[i]);
        }
        else
        {
            m_Clans[i].SetText("");
        }
    }

    m_Selections.AddElement(&m_ShowInvites);
    m_Selections.AddElement(&m_ShowSentRequests);
    m_Selections.AddElement(&m_ShowAll);
    m_Selections.AddElement(&m_Create);
    m_Selections.AddElement(&m_GetPrimary);
    m_Selections.AddElement(&m_Back);
}

int
UiClansMainMenu::GetSelectedIndex() const
{
    int idx = -1;
    const UiElement* sel = m_Selections.GetSelection();

    if(sel >= &m_Clans[0] && sel < &m_Clans[MAX_MEMBERSHIP_PER_PAGE])
    {
        idx = ptrdiff_t_to_int((const UiMultiValue*) sel - &m_Clans[0]);
    }

    return idx;
}

void
UiClansMainMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else if(&m_Create == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CREATE);
        }
        else if(&m_GetPrimary == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_PRIMARY);
        }
        else if(&m_ShowInvites == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_INVITES);
        }
        else if(&m_ShowSentRequests == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_SENT_REQUESTS);
        }
        else if(&m_ShowAll == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_ALL);
        }
        else
        {
            const int idx = this->GetSelectedIndex();
            if(idx >= 0)
            {
                const UiElement* choice = m_Clans[idx].GetSelection();
                if(&m_Leave[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_LEAVE);
                }
                else if(&m_SetPrimary[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SET_PRIMARY);
                }
                else if(&m_ShowMembers[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_MEMBERS);
                }
                else if(&m_ShowRanks[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_RANKS);
                }
                else if(&m_ShowRequests[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_REQUESTS);
                }
                else if(&m_ShowMessages[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_MESSAGES);
                }
                else if(&m_ShowMetadata[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_METADATA);
                }
                else if(&m_FeudStat[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_FEUD_STAT);
                }
            }
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiClanInviteFriendMenu
///////////////////////////////////////////////////////////////////////////////
UiClanInviteFriendMenu::UiClanInviteFriendMenu()
{
    m_Title.SetText("Clans");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiClanInviteFriendMenu::OnEvent);

    const float h = UI_GAP;
    for(unsigned i = 0; i < UiClanInviteFriendMenu::MAX_MEMBERSHIP_PER_PAGE; i++)
    {
        m_Invite[i].SetText("Invite");

        m_Clans[i].AddElement(&m_Invite[i]);
        m_Clans[i].Bind(this, &UiClanInviteFriendMenu::OnEvent);
        m_Clans[i].SetText("");
        m_Clans[i].SetXY(UI_CONTENT_X, UI_CONTENT_Y + i*h);
    }

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);
    m_Selections.AddElement(&m_Back);

    this->SetFocus(&m_Selections);
}

UiClanInviteFriendMenu::~UiClanInviteFriendMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiClanInviteFriendMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void 
UiClanInviteFriendMenu::Populate(const rlClanMembershipData* mine,
                                 const unsigned myCount)
{
    m_Selections.RemoveAllElements();

    char clanLine[256];

    for(unsigned i=0; i < UiClanInviteFriendMenu::MAX_MEMBERSHIP_PER_PAGE; i++)
    {
        if(i < myCount)
        {
            formatf(clanLine, COUNTOF(clanLine), "%s[%s] (%s[%d])",
                mine[i].m_Clan.m_ClanName,
                mine[i].m_Clan.m_ClanTag,
                mine[i].m_Rank.m_RankName,
                mine[i].m_Rank.m_RankOrder);

            m_Clans[i].SetText(clanLine);
            m_Clans[i].SetEnabled((mine[i].m_Rank.m_SystemFlags & RL_CLAN_PERMISSION_INVITE) != 0);
            m_Selections.AddElement(&m_Clans[i]);
        }
        else
        {
            m_Clans[i].SetText("");
        }
    }

    m_Selections.AddElement(&m_Back);
}

int
UiClanInviteFriendMenu::GetSelectedIndex() const
{
    int idx = -1;
    const UiElement* sel = m_Selections.GetSelection();

    if(sel >= &m_Clans[0] && sel < &m_Clans[MAX_MEMBERSHIP_PER_PAGE])
    {
        idx = ptrdiff_t_to_int((const UiMultiValue*) sel - &m_Clans[0]);
    }

    return idx;
}

void
UiClanInviteFriendMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else
        {
            const int idx = this->GetSelectedIndex();
            if(idx >= 0)
            {
                const UiElement* choice = m_Clans[idx].GetSelection();
                if(&m_Invite[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_INVITE);
                }
            }
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiClansAllMenu
///////////////////////////////////////////////////////////////////////////////
UiClansAllMenu::UiClansAllMenu()
{
    m_Title.SetText("Clans");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiClansAllMenu::OnEvent);

    const float h = UI_GAP;
    for(unsigned i = 0; i < UiClansAllMenu::MAX_CLAN_PER_PAGE; i++)
    {
        m_JoinRequest[i].SetText("Join Request");
        m_ShowMembers[i].SetText("Members");
        m_ShowRanks[i].SetText("Ranks");
        m_ShowMessages[i].SetText("Messages");
        m_ShowMetadata[i].SetText("Metadata");
        m_FeudStat[i].SetText("Feud Stat");

        m_Clans[i].AddElement(&m_JoinRequest[i]);
        m_Clans[i].AddElement(&m_ShowMembers[i]);
        m_Clans[i].AddElement(&m_ShowRanks[i]);
        m_Clans[i].AddElement(&m_ShowMessages[i]);
        m_Clans[i].AddElement(&m_ShowMetadata[i]);
        m_Clans[i].AddElement(&m_FeudStat[i]);
        m_Clans[i].Bind(this, &UiClansAllMenu::OnEvent);
        m_Clans[i].SetText("");
        m_Clans[i].SetXY(UI_CONTENT_X, UI_CONTENT_Y + i*h);
    }

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);
    m_Selections.AddElement(&m_Back);

    this->SetFocus(&m_Selections);
}

UiClansAllMenu::~UiClansAllMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiClansAllMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void 
UiClansAllMenu::Populate(const rlClanDesc* clans,
                         const rlClanLeader* leaders,
                         const unsigned clanCount)
{
    m_Selections.RemoveAllElements();

    char clanLine[256];

    for(unsigned i=0; i < UiClansAllMenu::MAX_CLAN_PER_PAGE; i++)
    {
        if(i < clanCount)
        {
            unsigned leaderId = i;
            unsigned iteration = 0;
            while (iteration < clanCount && leaders[leaderId].m_ClanId != clans[i].m_Id)
            {
                iteration++;
                leaderId = (leaderId + 1) % clanCount;
            }

            if (leaders[leaderId].m_ClanId == clans[i].m_Id)
            {
                formatf(clanLine, COUNTOF(clanLine), "%s[%s](%s)",
                    clans[i].m_ClanName,
                    clans[i].m_ClanTag,
                    leaders[leaderId].m_MemberInfo.m_Gamertag);
            }
            else
            {
                formatf(clanLine, COUNTOF(clanLine), "%s[%s]()",
                    clans[i].m_ClanName,
                    clans[i].m_ClanTag);
            }

            m_Clans[i].SetText(clanLine);

            m_Selections.AddElement(&m_Clans[i]);
        }
        else
        {
            m_Clans[i].SetText("");
        }
    }

    m_Selections.AddElement(&m_Back);
}

int
UiClansAllMenu::GetSelectedIndex() const
{
    int idx = -1;
    const UiElement* sel = m_Selections.GetSelection();

    if(sel >= &m_Clans[0] && sel < &m_Clans[MAX_CLAN_PER_PAGE])
    {
        idx = ptrdiff_t_to_int((const UiMultiValue*) sel - &m_Clans[0]);
    }

    return idx;
}

void
UiClansAllMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else
        {
            const int idx = this->GetSelectedIndex();
            if(idx >= 0)
            {
                const UiElement* choice = m_Clans[idx].GetSelection();
                if(&m_ShowMembers[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_MEMBERS);
                }
                else if(&m_ShowRanks[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_RANKS);
                }
                else if(&m_ShowMessages[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_MESSAGES);
                }
                else if(&m_ShowMetadata[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_METADATA);
                }
                else if(&m_JoinRequest[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_JOIN_REQUEST);
                }
                else if(&m_FeudStat[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHOW_FEUD);
                }
            }
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiClanMembersMenu
///////////////////////////////////////////////////////////////////////////////
UiClanMembersMenu::UiClanMembersMenu()
{
    m_Title.SetText("CLAN MEMBERS");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiClanMembersMenu::OnEvent);

    const float h = UI_GAP;
    for(unsigned i = 0; i < UiClanMembersMenu::MAX_MEMBERS_PER_PAGE; i++)
    {
        m_Kick[i].SetText("Kick");
        m_Promote[i].SetText("Promote");
        m_Demote[i].SetText("Demote");

        m_Members[i].AddElement(&m_Kick[i]);
        m_Members[i].AddElement(&m_Promote[i]);
        m_Members[i].AddElement(&m_Demote[i]);
        m_Members[i].Bind(this, &UiClanMembersMenu::OnEvent);
        m_Members[i].SetText("");
        m_Members[i].SetXY(UI_CONTENT_X, UI_CONTENT_Y + i*h);
    }

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);
    m_Selections.AddElement(&m_Back);

    this->SetFocus(&m_Selections);
}

UiClanMembersMenu::~UiClanMembersMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiClanMembersMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void 
UiClanMembersMenu::Populate(const rlClanDesc& activeClan,
                            const rlClanMembershipData* mine,
                            const unsigned myCount,
                            const rlClanMember* members,
                            const unsigned numMembers)
{
    m_Selections.RemoveAllElements();

    char memberLine[256];

    formatf(memberLine, COUNTOF(memberLine), "CLAN MEMBERS: %s",
        activeClan.m_ClanName);

    m_Title.SetText(memberLine);

    unsigned int i;
    s64 mySystemFlags = 0;

    for (i = 0; i < myCount; ++i)
    {
        if (mine[i].m_Clan.m_Id == activeClan.m_Id)
        {
            mySystemFlags = mine[i].m_Rank.m_SystemFlags;
            break;
        }
    }

    for(i = 0; i < UiClanMembersMenu::MAX_MEMBERS_PER_PAGE; i++)
    {
        if(i < numMembers)
        {
            formatf(memberLine, COUNTOF(memberLine), "%s (%s)",
                members[i].m_MemberInfo.m_Gamertag,
                members[i].m_MemberClanInfo.m_Rank.m_RankName);
            m_Members[i].SetText(memberLine);

            m_Kick[i].SetEnabled((mySystemFlags & RL_CLAN_PERMISSION_KICK) != 0);
            m_Promote[i].SetEnabled((mySystemFlags & RL_CLAN_PERMISSION_PROMOTE) != 0);
            m_Demote[i].SetEnabled((mySystemFlags & RL_CLAN_PERMISSION_DEMOTE) != 0);

            m_Selections.AddElement(&m_Members[i]);
        }
        else
        {
            m_Members[i].SetText("");
        }
    }

    m_Selections.AddElement(&m_Back);
}

int
UiClanMembersMenu::GetSelectedIndex() const
{
    int idx = -1;
    const UiElement* sel = m_Selections.GetSelection();

    if(sel >= &m_Members[0] && sel < &m_Members[MAX_MEMBERS_PER_PAGE])
    {
        idx = ptrdiff_t_to_int((const UiMultiValue*) sel - &m_Members[0]);
    }

    return idx;
}

void
UiClanMembersMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else
        {
            const int idx = this->GetSelectedIndex();
            if(idx >= 0)
            {
                const UiElement* choice = m_Members[idx].GetSelection();
                if(&m_Kick[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_KICK);
                }
                else if(&m_Promote[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_PROMOTE);
                }
                else if(&m_Demote[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_DEMOTE);
                }
            }
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiClanInvitesMenu
///////////////////////////////////////////////////////////////////////////////
UiClanInvitesMenu::UiClanInvitesMenu()
{
    m_Title.SetText("CLAN INVITES");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiClanInvitesMenu::OnEvent);

    const float h = UI_GAP;
    for(unsigned i = 0; i < UiClanInvitesMenu::MAX_INVITES_PER_PAGE; i++)
    {
        m_Accept[i].SetText("Accept");
        m_Delete[i].SetText("Delete");

        m_Invites[i].AddElement(&m_Accept[i]);
        m_Invites[i].AddElement(&m_Delete[i]);
        m_Invites[i].Bind(this, &UiClanInvitesMenu::OnEvent);
        m_Invites[i].SetText("");
        m_Invites[i].SetXY(UI_CONTENT_X, UI_CONTENT_Y + i*h);
    }

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);
    m_Selections.AddElement(&m_Back);

    this->SetFocus(&m_Selections);
}

UiClanInvitesMenu::~UiClanInvitesMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiClanInvitesMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void 
UiClanInvitesMenu::Populate(const rlClanInvite* invites,
                            const unsigned numInvites)
{
    m_Selections.RemoveAllElements();

    char inviteLine[256];
    for(unsigned i=0; i < UiClanInvitesMenu::MAX_INVITES_PER_PAGE; i++)
    {
        if(i < numInvites)
        {
            formatf(inviteLine, COUNTOF(inviteLine), "from: %s to: %s about: %s",
                invites[i].m_Inviter.m_Gamertag,
                invites[i].m_Invitee.m_Gamertag,
                invites[i].m_Clan.m_ClanName);

            m_Invites[i].SetText(inviteLine);
            m_Selections.AddElement(&m_Invites[i]);
        }
        else
        {
            m_Invites[i].SetText("");
        }
    }

    m_Selections.AddElement(&m_Back);
}

int
UiClanInvitesMenu::GetSelectedIndex() const
{
    int idx = -1;
    const UiElement* sel = m_Selections.GetSelection();

    if(sel >= &m_Invites[0] && sel < &m_Invites[MAX_INVITES_PER_PAGE])
    {
        idx = ptrdiff_t_to_int((const UiMultiValue*) sel - &m_Invites[0]);
    }

    return idx;
}

void
UiClanInvitesMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else
        {
            const int idx = this->GetSelectedIndex();
            if(idx >= 0)
            {
                const UiElement* choice = m_Invites[idx].GetSelection();
                if(&m_Accept[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_ACCEPT_INVITE);
                }
                else if(&m_Delete[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_DELETE_INVITE);
                }
            }
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiClanRequestsMenu
///////////////////////////////////////////////////////////////////////////////
UiClanRequestsMenu::UiClanRequestsMenu()
{
    m_Title.SetText("CLAN REQUESTS RECEIVED");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiClanRequestsMenu::OnEvent);

    const float h = UI_GAP;
    for(unsigned i = 0; i < UiClanRequestsMenu::MAX_REQUESTS_PER_PAGE; i++)
    {
        m_Delete[i].SetText("Delete");

        m_Requests[i].AddElement(&m_Delete[i]);
        m_Requests[i].Bind(this, &UiClanRequestsMenu::OnEvent);
        m_Requests[i].SetText("");
        m_Requests[i].SetXY(UI_CONTENT_X, UI_CONTENT_Y + i*h);
    }

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);
    m_Selections.AddElement(&m_Back);

    this->SetFocus(&m_Selections);
}

UiClanRequestsMenu::~UiClanRequestsMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiClanRequestsMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void 
UiClanRequestsMenu::Populate(const rlClanDesc& activeClan, const rlClanJoinRequestRecieved* requests, const unsigned numRequests)
{
    m_Selections.RemoveAllElements();

    char requestLine[256];

    formatf(requestLine, COUNTOF(requestLine), "CLAN REQUESTS: %s",
        activeClan.m_ClanName);

    m_Title.SetText(requestLine);

    unsigned i;
    for(i = 0; i < UiClanRequestsMenu::MAX_REQUESTS_PER_PAGE; i++)
    {
        if(i < numRequests)
        {
            formatf(requestLine, COUNTOF(requestLine), "%s(%s)",
                requests[i].m_Player.m_Gamertag,
                requests[i].m_Player.m_Nickname);

            m_Delete[i].SetEnabled(true);

            m_Requests[i].SetText(requestLine);
            m_Selections.AddElement(&m_Requests[i]);
        }
        else
        {
            m_Requests[i].SetText("");
        }
    }

    m_Selections.AddElement(&m_Back);
}

int
UiClanRequestsMenu::GetSelectedIndex() const
{
    int idx = -1;
    const UiElement* sel = m_Selections.GetSelection();

    if(sel >= &m_Requests[0] && sel < &m_Requests[MAX_REQUESTS_PER_PAGE])
    {
        idx = ptrdiff_t_to_int((const UiMultiValue*) sel - &m_Requests[0]);
    }

    return idx;
}

void
UiClanRequestsMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else
        {
            const int idx = this->GetSelectedIndex();
            if(idx >= 0)
            {
                const UiElement* choice = m_Requests[idx].GetSelection();
                if(&m_Delete[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_DELETE);
                }
            }
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiClanSentRequestsMenu
///////////////////////////////////////////////////////////////////////////////
UiClanSentRequestsMenu::UiClanSentRequestsMenu()
{
    m_Title.SetText("CLAN REQUESTS SENT");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiClanSentRequestsMenu::OnEvent);

    const float h = UI_GAP;
    for(unsigned i = 0; i < UiClanSentRequestsMenu::MAX_REQUESTS_PER_PAGE; i++)
    {
        m_Delete[i].SetText("Delete");

        m_Requests[i].AddElement(&m_Delete[i]);
        m_Requests[i].Bind(this, &UiClanSentRequestsMenu::OnEvent);
        m_Requests[i].SetText("");
        m_Requests[i].SetXY(UI_CONTENT_X, UI_CONTENT_Y + i*h);
    }

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);
    m_Selections.AddElement(&m_Back);

    this->SetFocus(&m_Selections);
}

UiClanSentRequestsMenu::~UiClanSentRequestsMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiClanSentRequestsMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void 
UiClanSentRequestsMenu::Populate(const rlClanJoinRequestSent* requests, const unsigned numRequests)
{
    m_Selections.RemoveAllElements();

    m_Title.SetText("CLAN SENT REQUESTS");

    char requestLine[256];

    unsigned i;
    for(i = 0; i < UiClanRequestsMenu::MAX_REQUESTS_PER_PAGE; i++)
    {
        if(i < numRequests)
        {
            formatf(requestLine, COUNTOF(requestLine), "%s(%s)",
                requests[i].m_Clan.m_ClanName,
                requests[i].m_Clan.m_ClanTag);

            m_Delete[i].SetEnabled(true);

            m_Requests[i].SetText(requestLine);
            m_Selections.AddElement(&m_Requests[i]);
        }
        else
        {
            m_Requests[i].SetText("");
        }
    }

    m_Selections.AddElement(&m_Back);
}

int
UiClanSentRequestsMenu::GetSelectedIndex() const
{
    int idx = -1;
    const UiElement* sel = m_Selections.GetSelection();

    if(sel >= &m_Requests[0] && sel < &m_Requests[MAX_REQUESTS_PER_PAGE])
    {
        idx = ptrdiff_t_to_int((const UiMultiValue*) sel - &m_Requests[0]);
    }

    return idx;
}

void
UiClanSentRequestsMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else
        {
            const int idx = this->GetSelectedIndex();
            if(idx >= 0)
            {
                const UiElement* choice = m_Requests[idx].GetSelection();
                if(&m_Delete[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_DELETE);
                }
            }
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiClanRanksMenu
///////////////////////////////////////////////////////////////////////////////
UiClanRanksMenu::UiClanRanksMenu()
{
    m_Title.SetText("CLAN RANKS");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiClanRanksMenu::OnEvent);

    const float h = UI_GAP;
    for(unsigned i = 0; i < UiClanRanksMenu::MAX_RANKS_PER_PAGE; i++)
    {
        m_MoveUp[i].SetText("Up");
        m_MoveDown[i].SetText("Down");
        m_ToggleManager[i].SetText("Manager");
        m_ToggleRecruiter[i].SetText("Recruiter");
        m_ToggleDestructive[i].SetText("Destructive");
        m_Delete[i].SetText("Delete");

        m_Ranks[i].AddElement(&m_MoveUp[i]);
        m_Ranks[i].AddElement(&m_MoveDown[i]);
        m_Ranks[i].AddElement(&m_ToggleManager[i]);
        m_Ranks[i].AddElement(&m_ToggleRecruiter[i]);
        m_Ranks[i].AddElement(&m_ToggleDestructive[i]);
        m_Ranks[i].AddElement(&m_Delete[i]);
        m_Ranks[i].Bind(this, &UiClanRanksMenu::OnEvent);
        m_Ranks[i].SetText("");
        m_Ranks[i].SetXY(UI_CONTENT_X, UI_CONTENT_Y + i*h);
    }

    m_Create.SetText("Create Rank");
    m_Create.SetXY(UI_RIGHT_CTRL_X, UI_RIGHT_CTRL_Y);
    m_Selections.AddElement(&m_Create);

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);
    m_Selections.AddElement(&m_Back);

    this->SetFocus(&m_Selections);
}

UiClanRanksMenu::~UiClanRanksMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiClanRanksMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void 
UiClanRanksMenu::Populate(const rlClanDesc& activeClan,
                          const rlClanMembershipData* mine,
                          const unsigned myCount,
                          const rlClanRank* ranks,
                          const unsigned numRanks)
{
    m_Selections.RemoveAllElements();

    char rankLine[256];

    formatf(rankLine, COUNTOF(rankLine), "CLAN RANKS: %s",
        activeClan.m_ClanName);

    m_Title.SetText(rankLine);

    unsigned i;
    s64 mySystemFlags = 0;

    for (i = 0; i < myCount; ++i)
    {
        if (mine[i].m_Clan.m_Id == activeClan.m_Id)
        {
            mySystemFlags = mine[i].m_Rank.m_SystemFlags;
            break;
        }
    }
    for(i = 0; i < UiClanRanksMenu::MAX_RANKS_PER_PAGE; i++)
    {
        if(i < numRanks)
        {
            formatf(rankLine, COUNTOF(rankLine), "%s(%d) [%s/%s/%s]",
                ranks[i].m_RankName,
                ranks[i].m_RankOrder,
                ((ranks[i].m_SystemFlags & RL_CLAN_PERMISSION_RANKMANAGER) != 0) ? "M" : "-",
                ((ranks[i].m_SystemFlags & RL_CLAN_PERMISSION_INVITE) != 0) ? "R" : "-");

            m_Delete[i].SetEnabled((mySystemFlags & RL_CLAN_PERMISSION_RANKMANAGER) != 0);
            m_MoveUp[i].SetEnabled((mySystemFlags & RL_CLAN_PERMISSION_RANKMANAGER) != 0);
            m_MoveDown[i].SetEnabled((mySystemFlags & RL_CLAN_PERMISSION_RANKMANAGER) != 0);
            m_ToggleDestructive[i].SetEnabled((mySystemFlags & RL_CLAN_PERMISSION_RANKMANAGER) != 0);
            m_ToggleManager[i].SetEnabled((mySystemFlags & RL_CLAN_PERMISSION_RANKMANAGER) != 0);
            m_ToggleRecruiter[i].SetEnabled((mySystemFlags & RL_CLAN_PERMISSION_RANKMANAGER) != 0);

            m_Ranks[i].SetText(rankLine);
            m_Selections.AddElement(&m_Ranks[i]);
        }
        else
        {
            m_Ranks[i].SetText("");
        }
    }

    m_Create.SetEnabled((mySystemFlags & RL_CLAN_PERMISSION_RANKMANAGER) != 0);

    m_Selections.AddElement(&m_Create);
    m_Selections.AddElement(&m_Back);
}

int
UiClanRanksMenu::GetSelectedIndex() const
{
    int idx = -1;
    const UiElement* sel = m_Selections.GetSelection();

    if(sel >= &m_Ranks[0] && sel < &m_Ranks[MAX_RANKS_PER_PAGE])
    {
        idx = ptrdiff_t_to_int((const UiMultiValue*) sel - &m_Ranks[0]);
    }

    return idx;
}

void
UiClanRanksMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else if(&m_Create == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CREATE);
        }
        else
        {
            const int idx = this->GetSelectedIndex();
            if(idx >= 0)
            {
                const UiElement* choice = m_Ranks[idx].GetSelection();
                if(&m_Delete[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_DELETE);
                }
                else if(&m_MoveUp[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_MOVE_UP);
                }
                else if(&m_MoveDown[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_MOVE_DOWN);
                }
                else if(&m_ToggleDestructive[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_TOGGLE_DESCRUCTIVE);
                }
                else if(&m_ToggleManager[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_TOGGLE_MANAGER);
                }
                else if(&m_ToggleRecruiter[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_TOGGLE_RECRUITER);
                }
            }
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//  UiClanWallMessagesMenu
///////////////////////////////////////////////////////////////////////////////
UiClanWallMessagesMenu::UiClanWallMessagesMenu()
{
    m_Title.SetText("CLAN WALL");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiClanWallMessagesMenu::OnEvent);

    const float h = UI_GAP;
    for(unsigned i = 0; i < UiClanWallMessagesMenu::MAX_MESSAGES_PER_PAGE; i++)
    {
        m_Delete[i].SetText("Delete");

        m_Messages[i].AddElement(&m_Delete[i]);
        m_Messages[i].Bind(this, &UiClanWallMessagesMenu::OnEvent);
        m_Messages[i].SetText("");
        m_Messages[i].SetXY(UI_CONTENT_X, UI_CONTENT_Y + i*h);
    }

    m_WriteMessage.SetText("Write Message");
    m_WriteMessage.SetXY(UI_RIGHT_CTRL_X, UI_RIGHT_CTRL_Y);
    m_Selections.AddElement(&m_WriteMessage);

    m_Back.SetText("Back");
    m_Back.SetXY(UI_LEFT_CTRL_X, UI_LEFT_CTRL_Y);
    m_Selections.AddElement(&m_Back);

    this->SetFocus(&m_Selections);
}

UiClanWallMessagesMenu::~UiClanWallMessagesMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiClanWallMessagesMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void 
UiClanWallMessagesMenu::Populate(const rlClanDesc& activeClan,
                                 const rlClanMembershipData* mine,
                                 const unsigned myCount,
                                 const rlClanWallMessage* messages,
                                 const unsigned numMessages)
{
    m_Selections.RemoveAllElements();

    unsigned i;
    s64 mySystemFlags = 0;

    for (i = 0; i < myCount; ++i)
    {
        if (mine[i].m_Clan.m_Id == activeClan.m_Id)
        {
            mySystemFlags = mine[i].m_Rank.m_SystemFlags;
            break;
        }
    }

    char messageLine[256];
    for(unsigned i=0; i < UiClanWallMessagesMenu::MAX_MESSAGES_PER_PAGE; i++)
    {
        if(i < numMessages)
        {
            formatf(messageLine, COUNTOF(messageLine), "from: %s \"%s\"",
                messages[i].m_Writer.m_Gamertag,
                messages[i].m_Message);

            m_Messages[i].SetText(messageLine);

            //m_Delete[i].SetEnabled((messages[i].m_Writer.m_GamerHandle == myGamerHandle) || ((mySystemFlags & RL_CLAN_PERMISSION_DELETEFROMWALL) != 0));

            m_Selections.AddElement(&m_Messages[i]);
        }
        else
        {
            m_Messages[i].SetText("");
        }
    }

    m_WriteMessage.SetEnabled((mySystemFlags & RL_CLAN_PERMISSION_WRITEONWALL) != 0);

    m_Selections.AddElement(&m_WriteMessage);
    m_Selections.AddElement(&m_Back);
}

int
UiClanWallMessagesMenu::GetSelectedIndex() const
{
    int idx = -1;
    const UiElement* sel = m_Selections.GetSelection();

    if(sel >= &m_Messages[0] && sel < &m_Messages[MAX_MESSAGES_PER_PAGE])
    {
        idx = ptrdiff_t_to_int((const UiMultiValue*) sel - &m_Messages[0]);
    }

    return idx;
}

void
UiClanWallMessagesMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else if(&m_WriteMessage == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_WRITE_MESSAGE);
        }
        else
        {
            const int idx = this->GetSelectedIndex();
            if(idx >= 0)
            {
                const UiElement* choice = m_Messages[idx].GetSelection();
                if(&m_Delete[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_DELETE_MESSAGE);
                }
            }
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiCloudMainMenu
///////////////////////////////////////////////////////////////////////////////
UiCloudMainMenu::UiCloudMainMenu()
{
    m_Title.SetText("Cloud");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiCloudMainMenu::OnEvent);

    float y = UI_CONTENT_Y;

    m_PrivateFolder.SetText("Private");
    m_SecureFolder.SetText("Secure (read only)");
    m_ShareFolder.SetText("Share");
    m_PublishFolder.SetText("Publish");
    m_AccessFolder.AddElement(&m_PrivateFolder);
    m_AccessFolder.AddElement(&m_SecureFolder);
    m_AccessFolder.AddElement(&m_ShareFolder);
    m_AccessFolder.AddElement(&m_PublishFolder);

	float x = UI_CONTENT_X;
    ADD_SELECTION(m_MembersLabel,           "Members Namespace", x, y); y += UI_GAP;
    ADD_SELECTION(m_AccessFolder,           "  AccessFolder", x, y); y += UI_GAP;
    ADD_SELECTION(m_MembersScLabel,         "  SC", x, y); y += UI_GAP;
    ADD_SELECTION(m_MembersScDownload,      "    Download", x, y); y += UI_GAP;
    ADD_SELECTION(m_MembersScUpload,        "    Upload", x, y); y += UI_GAP;
    ADD_SELECTION(m_MembersScAppend,        "    Append", x, y); y += UI_GAP;
    ADD_SELECTION(m_MembersScDelete,        "    Delete", x, y); y += UI_GAP;
    ADD_SELECTION(m_MembersNativeLabel,     "  Native", x, y); y += UI_GAP;
    ADD_SELECTION(m_MembersNativeDownload,  "    Download", x, y); y += UI_GAP;
    ADD_SELECTION(m_MembersNativeUpload,    "    Upload", x, y); y += UI_GAP;
    ADD_SELECTION(m_MembersNativeAppend,    "    Append", x, y); y += UI_GAP;
    ADD_SELECTION(m_MembersNativeDelete,    "    Delete", x, y); y += UI_GAP;
    ADD_SELECTION(m_TitlesLabel,            "Titles Namespace", x, y); y += UI_GAP;
    ADD_SELECTION(m_TitlesDownload,         "  Download", x, y); y += UI_GAP;
	ADD_SELECTION(m_SpCloudLabel,			"SP Cloud Saves", x, y); y += UI_GAP;
	ADD_SELECTION(m_SpCloudInit,			"  Init", x, y); y += UI_GAP;
	
	float startY = y;
	ADD_SELECTION(m_SpCloudIdentifyConflicts,"  Identify Conflicts", x, y); y += UI_GAP;
	ADD_SELECTION(m_SpCloudRegisterFile,	"  Register File", x, y); y+= UI_GAP;
	ADD_SELECTION(m_SpCloudUnregisterFile,	"  Unregister File", x, y); y += UI_GAP;
	ADD_SELECTION(m_SpCloudDeleteFiles,		"  Delete Files", x, y); y += UI_GAP;
	ADD_SELECTION(m_SpCloudGetFile,			"  Get Files", x, y); y += UI_GAP;
	ADD_SELECTION(m_SpCloudPostFile,		"  Post Files", x, y); y += UI_GAP;
	ADD_SELECTION(m_SpCloudBackupConflicts, "  Backup Conflicts", x, y); y += UI_GAP;

	x += (2 * UI_CONTENT_X);
	y = startY;
	ADD_SELECTION(m_SpCloudSearchBackups,	"  Search Backups", x, y); y += UI_GAP;
	ADD_SELECTION(m_SpCloudGetBackupInfo,	"  Get Backup Info", x, y); y += UI_GAP;
	ADD_SELECTION(m_SpCloudRestoreBackup,	"  Restore Backup", x, y); y += UI_GAP;
	ADD_SELECTION(m_SpCloudMergeFile,		"  Merge Files", x, y); y += UI_GAP;
	ADD_SELECTION(m_SpCloudQueryBeta,		"  Query Beta", x, y); y += UI_GAP;
	ADD_SELECTION(m_SpCloudTelemetryEvent,	"  Write Telemetry", x, y); y += UI_GAP;
	ADD_SELECTION(m_SpCloudGetMetadata,		"  Get Metadata", x, y); y += UI_GAP;

    m_MembersLabel.SetEnabled(false);
    m_MembersScLabel.SetEnabled(false);
    m_MembersNativeLabel.SetEnabled(false);
    m_TitlesLabel.SetEnabled(false);
	m_SpCloudLabel.SetEnabled(false);
	m_SpCloudIdentifyConflicts.SetEnabled(false);
	m_SpCloudRegisterFile.SetEnabled(false);
	m_SpCloudUnregisterFile.SetEnabled(false);
	m_SpCloudDeleteFiles.SetEnabled(false);
	m_SpCloudGetFile.SetEnabled(false);
	m_SpCloudPostFile.SetEnabled(false);
	m_SpCloudBackupConflicts.SetEnabled(false);
	m_SpCloudSearchBackups.SetEnabled(false);
	m_SpCloudGetBackupInfo.SetEnabled(false);
	m_SpCloudRestoreBackup.SetEnabled(false);
	m_SpCloudMergeFile.SetEnabled(false);
	m_SpCloudQueryBeta.SetEnabled(false);
	m_SpCloudTelemetryEvent.SetEnabled(false);
	m_SpCloudGetMetadata.SetEnabled(false);

    ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

    this->SetFocus(&m_Selections);
}

UiCloudMainMenu::~UiCloudMainMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiCloudMainMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void UiCloudMainMenu::EnableCloudSaveOperations()
{
	m_SpCloudIdentifyConflicts.SetEnabled(true);
	m_SpCloudRegisterFile.SetEnabled(true);
	m_SpCloudUnregisterFile.SetEnabled(true);
	m_SpCloudDeleteFiles.SetEnabled(true);
	m_SpCloudGetFile.SetEnabled(true);
	m_SpCloudPostFile.SetEnabled(true);
	m_SpCloudBackupConflicts.SetEnabled(true);
	m_SpCloudSearchBackups.SetEnabled(true);
	m_SpCloudGetBackupInfo.SetEnabled(true);
	m_SpCloudRestoreBackup.SetEnabled(true);
	m_SpCloudMergeFile.SetEnabled(true);
	m_SpCloudQueryBeta.SetEnabled(true);
	m_SpCloudTelemetryEvent.SetEnabled(true);
	m_SpCloudGetMetadata.SetEnabled(true);

	m_SpCloudInit.SetEnabled(false);
}

UiCloudMainMenu::AccessType
UiCloudMainMenu::GetAccessType() const
{
    if(&m_SecureFolder == m_AccessFolder.GetSelection())
    {
        return ACCESS_SECURE;
    }
    else if(&m_ShareFolder == m_AccessFolder.GetSelection())
    {
        return ACCESS_SHARE;
    }
    else if(&m_PublishFolder == m_AccessFolder.GetSelection())
    {
        return ACCESS_PUBLISH;
    }

    return ACCESS_PRIVATE;
}

void
UiCloudMainMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();

        if(&m_MembersScDownload == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_MEMBER_SC_DOWNLOAD);
        }
        else if(&m_MembersScUpload == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_MEMBER_SC_UPLOAD);
        }
        else if(&m_MembersScAppend == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_MEMBER_SC_APPEND);
        }
        else if(&m_MembersScDelete == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_MEMBER_SC_DELETE);
        }
        else if(&m_MembersNativeDownload == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_MEMBER_NATIVE_DOWNLOAD);
        }
        else if(&m_MembersNativeUpload == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_MEMBER_NATIVE_UPLOAD);
        }
        else if(&m_MembersNativeAppend == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_MEMBER_NATIVE_APPEND);
        }
        else if(&m_MembersNativeDelete == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_MEMBER_NATIVE_DELETE);
        }
        else if(&m_TitlesDownload == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_TITLE_DOWNLOAD);
        }
		else if(&m_SpCloudInit == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_INIT);
		}
		else if(&m_SpCloudIdentifyConflicts == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_IDENTIFY_CONFLICTS);
		}
		else if(&m_SpCloudRegisterFile == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_REGISTER_FILE);
		}
		else if(&m_SpCloudUnregisterFile == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_UNREGISTER_FILE);
		}
		else if(&m_SpCloudGetFile == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_GET_FILE);
		}
		else if(&m_SpCloudPostFile == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_POST_FILE);
		}
		else if(&m_SpCloudBackupConflicts == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_BACKUP_CONFLICTS);
		}
		else if(&m_SpCloudSearchBackups == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_SEARCH_BACKUPS);
		}
		else if(&m_SpCloudGetBackupInfo == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_GET_BACKUP_INFO);
		}
		else if(&m_SpCloudRestoreBackup == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_RESTORE_BACKUP);
		}
		else if(&m_SpCloudMergeFile == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_MERGE_FILES);
		}
		else if(&m_SpCloudDeleteFiles == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_DELETE_FILES);
		}
		else if(&m_SpCloudQueryBeta == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_QUERY_BETA);
		}
		else if(&m_SpCloudTelemetryEvent == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_WRITE_TELEMETRY);
		}
		else if(&m_SpCloudGetMetadata == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SPSAVES_GET_METADATA);
		}
        else if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiUgcMainMenu
///////////////////////////////////////////////////////////////////////////////
UiUgcMainMenu::UiUgcMainMenu()
{
    m_Title.SetText("Ugc");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiUgcMainMenu::OnEvent);

    float y = UI_CONTENT_Y;

    ADD_SELECTION(m_CreateVideoContent, "Create Video Content", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_CreateContent, "Create Content", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GetBookmarked, "Get Bookmarked", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GetFriends, "Get by Friends", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GetMine, "Get Mine", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GetRockstarCreated, "Get Rockstar Created", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GetTopRated, "Get Top Rated", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_GetTopRatedCreators, "Get Top Rated Creators", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

    this->SetFocus(&m_Selections);
}

UiUgcMainMenu::~UiUgcMainMenu()
{
    m_Selections.RemoveAllElements();
}

void
UiUgcMainMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void
UiUgcMainMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();

        if(&m_CreateVideoContent == sel) this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CREATE_VIDEO_CONTENT);
        else if(&m_CreateContent == sel) this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CREATE_CONTENT);
        else if(&m_GetBookmarked == sel) this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_BOOKMARKED);
        else if(&m_GetFriends == sel) this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_FRIENDS);
        else if(&m_GetMine == sel) this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_MINE);
        else if(&m_GetRockstarCreated == sel) this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_ROCKSTAR_CREATED);
        else if(&m_GetTopRated == sel) this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_TOP_RATED);
        else if(&m_GetTopRatedCreators == sel) this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_TOP_RATED_CREATORS);
        else if(&m_Back == sel) this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiUgcContentList
///////////////////////////////////////////////////////////////////////////////
UiUgcContentList::UiUgcContentList()
{
    m_Title.SetText("Ugc");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiUgcContentList::OnEvent);

    float y = UI_CONTENT_Y;

    for(unsigned i = 0; i < MAX_CONTENT; i++)
    {
        m_Bookmark[i].SetText("Bookmark");
        m_CopyContent[i].SetText("Copy Content");
        m_Delete[i].SetText("DELETE");
        m_Details[i].SetText("Details");
        m_Players[i].SetText("Players");
        m_Rate0[i].SetText("Rate 0%");
        m_Rate25[i].SetText("Rate 25%");
        m_Rate50[i].SetText("Rate 50%");
        m_Rate75[i].SetText("Rate 75%");
        m_Rate100[i].SetText("Rate 100%");
        m_SetPlayerData[i].SetText("Set Player Data");
        m_ClearPlayerData[i].SetText("Clear Player Data");
        m_SetPublished[i].SetText("Set Published");
        m_UpdateContent[i].SetText("Update Content");
        m_VersionContent[i].SetText("Version Content");
        
        m_Content[i].Bind(this, &UiUgcContentList::OnEvent);
        m_Content[i].SetText("");
        m_Content[i].SetXY(UI_CONTENT_X, y); y += UI_GAP;
    }

    ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

    this->SetFocus(&m_Selections);
}

UiUgcContentList::~UiUgcContentList()
{
    m_Selections.RemoveAllElements();
}

void
UiUgcContentList::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

//void 
//UiUgcContentList::SetContent(const rlUgcQueryContentResults& results)
//{
//    m_Selections.RemoveAllElements();
//
//    for(unsigned i=0; i < results.GetNumResults(); i++)
//    {
//        if(i < (int)MAX_CONTENT)
//        {
//            m_Content[i].RemoveAllElements();
//
//            const rlUgcQueryContentResultBase* r = results.GetResult(i);
//            const rlUgcContentInfo& c = r->GetContentInfo();
//            const rlUgcMetadata& m = c.GetMetadata();
//
//            m_SetPublished[i].SetText(m.IsPublished() ? "Set Unpublished" : "Set Published");
//
//            m_Content[i].AddElement(&m_Details[i]);
//            m_Content[i].AddElement(&m_Players[i]);
//            m_Content[i].AddElement(&m_Bookmark[i]);
//            m_Content[i].AddElement(&m_CopyContent[i]);
//            m_Content[i].AddElement(&m_Rate0[i]);
//            m_Content[i].AddElement(&m_Rate25[i]);
//            m_Content[i].AddElement(&m_Rate50[i]);
//            m_Content[i].AddElement(&m_Rate75[i]);
//            m_Content[i].AddElement(&m_Rate100[i]);
//            m_Content[i].AddElement(&m_SetPlayerData[i]);
//            m_Content[i].AddElement(&m_ClearPlayerData[i]);
//            m_Content[i].AddElement(&m_SetPublished[i]);
//            m_Content[i].AddElement(&m_UpdateContent[i]);
//            m_Content[i].AddElement(&m_VersionContent[i]);
//            m_Content[i].AddElement(&m_Delete[i]);
//
//            char buf[128];
//            formatf(buf, sizeof(buf), "%s v%d:", m.GetContentName(), m.GetVersion());
//
//            m_Content[i].SetText(buf);
//            m_Selections.AddElement(&m_Content[i]);
//        }
//    }
//
//    m_Selections.AddElement(&m_Back);
//}

int
UiUgcContentList::GetSelectedContentIndex() const
{
    int idx = -1;
    const UiElement* sel = m_Selections.GetSelection();

    if(sel >= &m_Content[0] && sel < &m_Content[MAX_CONTENT])
    {
        idx = ptrdiff_t_to_int((const UiMultiValue*) sel - &m_Content[0]);
    }

    return idx;
}

void
UiUgcContentList::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
        else
        {
            const int idx = this->GetSelectedContentIndex();
            if(idx >= 0)
            {
                const UiElement* choice = m_Content[idx].GetSelection();
                if(&m_Details[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_DETAILS);
                }
                else if(&m_Bookmark[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BOOKMARK);
                }
                else if(&m_CopyContent[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_COPY_CONTENT);
                }
                else if(&m_Delete[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_DELETE);
                }
                else if(&m_Players[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_PLAYERS);
                }
                else if(&m_Rate0[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_RATE_CONTENT_0PCT);
                }
                else if(&m_Rate25[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_RATE_CONTENT_25PCT);
                }
                else if(&m_Rate50[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_RATE_CONTENT_50PCT);
                }
                else if(&m_Rate75[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_RATE_CONTENT_75PCT);
                }
                else if(&m_Rate100[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_RATE_CONTENT_100PCT);
                }
                else if(&m_SetPlayerData[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SET_PLAYER_DATA);
                }
                else if(&m_ClearPlayerData[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CLEAR_PLAYER_DATA);
                }
                else if(&m_SetPublished[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SET_PUBLISHED);
                }
                else if(&m_UpdateContent[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_UPDATE_CONTENT);
                }
                else if(&m_VersionContent[idx] == choice)
                {
                    this->FireEvent(UIEVENT_ACTION, (void*)ACTION_VERSION_CONTENT);
                }
            }
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiUgcPlayerList
///////////////////////////////////////////////////////////////////////////////
UiUgcPlayerList::UiUgcPlayerList()
{
    m_Title.SetText("Ugc Players");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiUgcPlayerList::OnEvent);

    float y = UI_CONTENT_Y;

    for(unsigned i = 0; i < MAX_PLAYERS; i++)
    {
        m_Players[i].Bind(this, &UiUgcPlayerList::OnEvent);
        m_Players[i].SetText("");
        m_Players[i].SetXY(UI_CONTENT_X, y); y += UI_GAP;
    }

    ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

    this->SetFocus(&m_Selections);
}

UiUgcPlayerList::~UiUgcPlayerList()
{
    m_Selections.RemoveAllElements();
}

void
UiUgcPlayerList::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

//void 
//UiUgcPlayerList::SetContent(const rlUgcQueryContentResultBase* result)
//{
//    m_Selections.RemoveAllElements();
//
//    for(unsigned i=0; i < result->GetNumPlayers(); i++)
//    {
//        if(i < (int)MAX_PLAYERS)
//        {
//            const rlUgcPlayer* p = result->GetPlayer(i);
//
//            char buf[128];
//            formatf(buf, sizeof(buf), "%s (bkmk:%s,  rt:%0.2f)", 
//                    p->GetUserId(), 
//                    p->IsBookmarked() ? "true" : "false",
//                    p->HasRating() ? p->GetRating() : -1.0f);
//
//            m_Players[i].SetText(buf);
//            m_Selections.AddElement(&m_Players[i]);
//        }
//        else
//        {
//            m_Players[i].SetText("");
//        }
//    }
//
//    m_Selections.AddElement(&m_Back);
//}

int
UiUgcPlayerList::GetSelectedPlayerIndex() const
{
    int idx = -1;
    const UiElement* sel = m_Selections.GetSelection();

    if(sel >= &m_Players[0] && sel < &m_Players[MAX_PLAYERS])
    {
        idx = ptrdiff_t_to_int((const UiLabel*) sel - &m_Players[0]);
    }

    return idx;
}

void
UiUgcPlayerList::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiUgcContentDetails
///////////////////////////////////////////////////////////////////////////////
UiUgcContentDetails::UiUgcContentDetails()
{
    m_Title.SetText("Ugc");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiUgcContentDetails::OnEvent);

    float y = UI_CONTENT_Y;

    ADD_SELECTION(m_AccountId, "AccountId", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_Category, "Category", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_CloudAbsPath, "CloudAbsPath", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ContentId, "ContentId", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ContentName, "Content Name", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_CreatedDate, "CreatedDate", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_Data, "Data", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_Description, "Description", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_IsPublished, "IsPublished", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_RockstarId, "RockstarId", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_Stats, "Stats", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_UserId, "UserId", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_UserName, "UserName", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_Version, "Version", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_Back, "Back", UI_CONTENT_X, y+UI_GAP*2);

    this->SetFocus(&m_Selections);
}

UiUgcContentDetails::~UiUgcContentDetails()
{
    m_Selections.RemoveAllElements();
}

void
UiUgcContentDetails::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

//void 
//UiUgcContentDetails::SetContent(const rlUgcQueryContentResultBase* result)
//{
//    SetContent(result->GetContentInfo().GetMetadata());
//}

void 
UiUgcContentDetails::SetContent(const rlUgcMetadata& m)
{
    char buf[64];

    formatf(buf, sizeof(buf), "Account ID: %d", m.GetAccountId());
    m_AccountId.SetText(buf);

    formatf(buf, sizeof(buf), "Category: %s", rlUgcCategoryToString(m.GetCategory()));
    m_Category.SetText(buf);

    char clap[RLUGC_MAX_CLOUD_ABS_PATH_CHARS];
    if (m.ComposeCloudAbsPath(0, clap, sizeof(clap)))
    {
        formatf(buf, sizeof(buf), "Cloud Abs Path: %s", clap);
        m_CloudAbsPath.SetText(buf);
    }
    else
    {
        formatf(buf, sizeof(buf), "Cloud Abs Path: [Unknown Format]");
        m_CloudAbsPath.SetText(buf);
    }

    formatf(buf, sizeof(buf), "Content ID: %s", m.GetContentId());
    m_ContentId.SetText(buf);

    formatf(buf, sizeof(buf), "Content Name: %s", m.GetContentName());
    m_ContentName.SetText(buf);

    formatf(buf, sizeof(buf), "Created Date (POSIX): %d", m.GetCreatedDate());
    m_CreatedDate.SetText(buf);

    formatf(buf, sizeof(buf), "Data: %s", m.GetData());
    m_Data.SetText(buf);

    formatf(buf, sizeof(buf), "Description: %s", m.GetDescription());
    m_Description.SetText(buf);

    formatf(buf, sizeof(buf), "IsPublished: %s", m.IsPublished() ? "true" : "false");
    m_IsPublished.SetText(buf);

    formatf(buf, sizeof(buf), "Rockstar ID: %d", m.GetRockstarId());
    m_RockstarId.SetText(buf);

    formatf(buf, sizeof(buf), "Stats: [none]");
    m_Stats.SetText(buf);

    formatf(buf, sizeof(buf), "User ID: %s", m.GetUserId());
    m_UserId.SetText(buf);

    formatf(buf, sizeof(buf), "User Name: %s", m.GetUsername());
    m_UserName.SetText(buf);

    formatf(buf, sizeof(buf), "Version: %d", m.GetVersion());
    m_Version.SetText(buf);
}

void
UiUgcContentDetails::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();
        if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}

///////////////////////////////////////////////////////////////////////////////
//UiPresenceMainMenu
///////////////////////////////////////////////////////////////////////////////
static const char* s_PresenceQueryNames[] =
{
    "OnlinePlayers",
    "OnlineScPlayers",
    "OnlineCrewMates",
    "PlayingMp",
    "InParty",
	"SessionByGamerHandle",
};

UiPresenceMainMenu::UiPresenceMainMenu()
{
    m_Title.SetText("Presence");
    m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

    m_Selections.Bind(this, &UiPresenceMainMenu::OnEvent);

    for(int i = 0; i < COUNTOF(s_PresenceQueryNames); ++i)
    {
        m_ExecuteQueryNames[i].SetText(s_PresenceQueryNames[i]);
        m_ExecuteQuery.AddElement(&m_ExecuteQueryNames[i]);

        m_QueryCountNames[i].SetText(s_PresenceQueryNames[i]);
        m_QueryCount.AddElement(&m_QueryCountNames[i]);
    }

    float y = UI_CONTENT_Y;

    ADD_SELECTION(m_PublishToFriends, "Publish To Friends", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_PublishToCrew, "Publish To Crew", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_ExecuteQuery, "Execute Query", UI_CONTENT_X, y); y += UI_GAP;
    ADD_SELECTION(m_QueryCount, "Query Count", UI_CONTENT_X, y); y += UI_GAP;

    ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

    this->SetFocus(&m_Selections);
}

UiPresenceMainMenu::~UiPresenceMainMenu()
{
    m_Selections.RemoveAllElements();
}

const char*
UiPresenceMainMenu::GetSelectedQuery() const
{
    const UiElement* sel = m_Selections.GetSelection();
    const char* qstr = NULL;

    if(&m_ExecuteQuery == sel || &m_QueryCount == sel)
    {
        const UiMultiValue* mv = (const UiMultiValue*)sel;
        qstr = ((UiLabel*)mv->GetSelection())->GetText();
    }

    return qstr;
}

bool
UiPresenceMainMenu::GetSelectedQueryParams(const int localGamerIndex,
                                            char* buf,
                                            size_t sizeofBuf) const
{
    bool success = false;

    const UiElement* sel = m_Selections.GetSelection();

    if(&m_ExecuteQuery == sel || &m_QueryCount == sel)
    {
        const UiMultiValue* mv = (const UiMultiValue*)sel;
        const char* q = ((UiLabel*)mv->GetSelection())->GetText();
        if(!strcmp("OnlinePlayers", q))
        {
            buf[0] = '\0';
            success = true;
        }
        else if(!strcmp("OnlineCrewMates", q))
        {
            s64 clanId = 0;
            if(rlClan::HasPrimaryClan(localGamerIndex))
            {
                clanId = rlClan::GetPrimaryClan(localGamerIndex).m_Id;
            }

            formatf(buf, sizeofBuf, "@crewid,%" I64FMT "d", clanId);
            success = true;
        }
        else if(!strcmp("OnlineScPlayers", q))
        {
            buf[0] = '\0';
            success = true;
        }
        else if(!strcmp("PlayingMp", q))
        {
            buf[0] = '\0';
            success = true;
        }
        else if(!strcmp("InParty", q))
        {
            buf[0] = '\0';
            success = true;
        }
		else if(!strcmp("SessionByGamerHandle", q))
		{
			unsigned totalNumFriends = rlFriendsManager::GetTotalNumFriends(localGamerIndex);
			for (int i = totalNumFriends-1; i >= 0; i--)
			{
				if(rlFriendsManager::IsFriendOnline(i) && rlFriendsManager::IsFriendInSameTitle(i))
				{
					rlGamerHandle gh;
					rlFriendsManager::GetGamerHandle(i, &gh);

					if (gh.IsValid())
					{
						char ghBuf[RL_MAX_GAMER_HANDLE_CHARS] = {};

						if(gh.ToString(ghBuf, sizeof(ghBuf)))
						{
							formatf(buf, sizeofBuf, "@ghandle,\"%s\"", ghBuf);
							break;
						}
					}
				}
			}

			success = true;
		}
        else
        {
            buf[0] = '\0';
        }
    }

    return success;
}

void
UiPresenceMainMenu::Draw()
{
    this->UiElement::Draw();
    m_Title.Draw();
    m_Selections.Draw();
}

void
UiPresenceMainMenu::OnEvent(UiElement* el, const int evtId, void*)
{
    if(UIEVENT_ACTION == evtId && &m_Selections == el)
    {
        const UiElement* sel = m_Selections.GetSelection();

        if(&m_PublishToFriends == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_PUBLISH_TO_FRIENDS);
        }
        else if(&m_PublishToCrew == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_PUBLISH_TO_CREW);
        }
        else if(&m_ExecuteQuery == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_EXECUTE_QUERY);
        }
        else if(&m_QueryCount == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_QUERY_COUNT);
        }
        else if(&m_Back == sel)
        {
            this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
        }
    }
    else if(UIEVENT_BACK == evtId)
    {
        this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
    }
}


///////////////////////////////////////////////////////////////////////////////
//UiRichPresenceMainMenu
///////////////////////////////////////////////////////////////////////////////
UiRichPresenceMainMenu::UiRichPresenceMainMenu()
{
	safecpy(m_PresenceBuf, "PRESENCE_PRES_8");
	safecpy(m_FieldBuf, "SPMG_30");
	m_VirtualKeyboardResult[0] = '\0';

	m_Title.SetText("Rich Presence");
	m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);
	m_Selections.Bind(this, &UiRichPresenceMainMenu::OnEvent);
	
	float y = UI_CONTENT_Y;

	// Presence ID
	char pBuf[64];
	formatf(pBuf, "Presence Str (Current: %s)", m_PresenceBuf);
	ADD_SELECTION(m_PresenceString, pBuf, UI_CONTENT_X, y); y+= UI_GAP;

	// Field ID
	char fBuf[64];
	formatf(fBuf, "Field Str (Current: %s)", m_FieldBuf);
	ADD_SELECTION(m_FieldString, fBuf, UI_CONTENT_X, y); y+= UI_GAP;

	// set Presence status
	ADD_SELECTION(m_SetString, "Set Rich Presence", UI_CONTENT_X, y); y+= UI_GAP*2;
	ADD_SELECTION(m_GetString, "Get Rich Presence", UI_CONTENT_X, y); y+= UI_GAP*2;

	// Back
	ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

	m_bUpdatingPresenceStr = false;
	m_bUpdatingFieldStr = false;

	this->SetFocus(&m_Selections);
}

UiRichPresenceMainMenu::~UiRichPresenceMainMenu()
{
	m_Selections.RemoveAllElements();
}

void UiRichPresenceMainMenu::Draw()
{
	this->UiElement::Draw();
	m_Title.Draw();
	m_Selections.Draw();

	if (m_bUpdatingPresenceStr)
	{
		if (App::GetVirtualKeyboard()->Succeeded())
		{
			sysMemSet(&m_PresenceBuf, 0, sizeof(m_PresenceBuf));

			char16* kbdData = App::GetVirtualKeyboard()->GetResult();
			WideToUtf8(m_PresenceBuf, kbdData, 256);

			char buf[256] = {0};
			formatf(buf, "Presence Str (Current: %s)", m_PresenceBuf);
			m_PresenceString.SetText(&buf[0], 256);

			m_bUpdatingPresenceStr = false;
		}
		else if (!App::GetVirtualKeyboard()->IsPending())
		{
			m_bUpdatingPresenceStr = false;
		}
	}
	else if (m_bUpdatingFieldStr)
	{
		if (App::GetVirtualKeyboard()->Succeeded())
		{
			sysMemSet(&m_FieldBuf, 0, sizeof(m_FieldBuf));

			char16* kbdData = App::GetVirtualKeyboard()->GetResult();
			WideToUtf8(m_FieldBuf, kbdData, 256);

			char buf[256] = {0};
			formatf(buf, "Field Str (Current: %s)", m_FieldBuf);
			m_FieldString.SetText(&buf[0], 256);

			m_bUpdatingFieldStr = false;
		}
		else if (!App::GetVirtualKeyboard()->IsPending())
		{
			m_bUpdatingFieldStr = false;
		}
	}
}

void UiRichPresenceMainMenu::OnEvent(UiElement* el, const int evtId, void*)
{
	if(UIEVENT_ACTION == evtId && &m_Selections == el)
	{
		const UiElement* sel = m_Selections.GetSelection();
		if(&m_Back == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
		}
		else if(&m_SetString == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SET_RICH_PRESENCE);
		}
		else if(&m_GetString == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_RICH_PRESENCE);
		}
		else if(&m_PresenceString == sel)
		{
			ioVirtualKeyboard::Params params;
			char16 defaultText[25];
			Utf8ToWide(defaultText, m_PresenceBuf, 25);

			params.m_InitialValue = defaultText;
			params.m_KeyboardType = ioVirtualKeyboard::kTextType_DEFAULT;
			params.m_Description = _C16("Sample");
			params.m_Title = _C16("Sample");
			params.m_MaxLength = 25;
			params.m_PlayerIndex = 0;

			App::GetVirtualKeyboard()->Show(params);
			m_bUpdatingPresenceStr = true;
		}
		else if(&m_FieldString == sel)
		{
			ioVirtualKeyboard::Params params;
			char16 defaultText[25];
			Utf8ToWide(defaultText, m_FieldBuf, 25);

			params.m_InitialValue = defaultText;
			params.m_KeyboardType = ioVirtualKeyboard::kTextType_DEFAULT;
			params.m_Description = _C16("Sample");
			params.m_Title = _C16("Sample");
			params.m_MaxLength = 25;
			params.m_PlayerIndex = 0;

			App::GetVirtualKeyboard()->Show(params);
			m_bUpdatingFieldStr = true;
		}
	}
	else if(UIEVENT_BACK == evtId)
	{
		this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
	}
}

///////////////////////////////////////////////////////////////////////////////
//UiSocialClubMainMenu
///////////////////////////////////////////////////////////////////////////////
UiFacebookMainMenu::UiFacebookMainMenu()
{
	m_Title.SetText("Facebook");
	m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

	m_Selections.Bind(this, &UiFacebookMainMenu::OnEvent);

	float y = UI_CONTENT_Y;

	ADD_SELECTION(m_GetAccessTokenUi, "Get AccessToken - Show GUI", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_GetAccessTokenNoUi, "Get AccessToken - No GUI", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_PostOpenGraph, "Post OpenGraph", UI_CONTENT_X, y); y += UI_GAP;

	ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP*2);

	this->SetFocus(&m_Selections);
}

UiFacebookMainMenu::~UiFacebookMainMenu()
{
	m_Selections.RemoveAllElements();
}

void
UiFacebookMainMenu::Draw()
{
	this->UiElement::Draw();
	m_GetAccessTokenUi.Draw();
	m_GetAccessTokenNoUi.Draw();
	m_PostOpenGraph.Draw();
	m_Back.Draw();

	m_Selections.Draw();
}

void
UiFacebookMainMenu::OnEvent(UiElement* el, const int evtId, void*)
{
	if(UIEVENT_ACTION == evtId && &m_Selections == el)
	{
		const UiElement* sel = m_Selections.GetSelection();

		if(&m_GetAccessTokenUi == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GETACCESSTOKEN_UI);
		}
		else if (&m_GetAccessTokenNoUi == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GETACCESSTOKEN_NO_UI);
		}
		else if(&m_PostOpenGraph == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_POSTOPENGRAPH);
		}
		else if(&m_Back == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
		}
	}
	else if(UIEVENT_BACK == evtId)
	{
		this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
	}
}

///////////////////////////////////////////////////////////////////////////////
//UiPartyMainMenu
///////////////////////////////////////////////////////////////////////////////
UiPartyMainMenu::UiPartyMainMenu()
{
	m_Title.SetText("System Party");
	m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

	m_Selections.Bind(this, &UiPartyMainMenu::OnEvent);

	float y = UI_CONTENT_Y;

#if RSG_DURANGO
	ADD_SELECTION(m_SetActiveTitle, "Set Active Title", UI_CONTENT_X, y); y += UI_GAP;
#endif

	ADD_SELECTION(m_CreateParty, "Create Party", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_InviteFriendToParty, "Invite to System Party", UI_CONTENT_X, y); y += UI_GAP;
	
	y += UI_GAP;

	for(unsigned i = 0; i < RL_MAX_PARTY_SIZE; i++)
	{
		m_PartyMembers[i].SetText("");
		m_PartyMembers[i].SetXY(UI_CONTENT_X, y);
		y += UI_GAP;
	}

	ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP);

	this->SetFocus(&m_Selections);
}

UiPartyMainMenu::~UiPartyMainMenu()
{
	m_Selections.RemoveAllElements();
}

void UiPartyMainMenu::Draw()
{
	this->UiElement::Draw();

#if RSG_DURANGO
	m_SetActiveTitle.Draw();
#endif

	m_CreateParty.Draw();
	m_InviteFriendToParty.Draw();

	m_Back.Draw();

	m_Selections.Draw();
}

void UiPartyMainMenu::OnEvent(UiElement* el, const int evtId, void*)
{
	if(UIEVENT_ACTION == evtId && &m_Selections == el)
	{
		const UiElement* sel = m_Selections.GetSelection();

		if (&m_InviteFriendToParty == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_INVITETOPARTY);
		}
		else if (&m_CreateParty == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CREATEPARTY);
		}
#if RSG_DURANGO
		else if (&m_SetActiveTitle == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SETACTIVETITLE);
		}
#endif
		else if(&m_Back == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
		}
	}
	else if(UIEVENT_BACK == evtId)
	{
		this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
	}
}

void UiPartyMainMenu::RefreshParty()
{
	m_Selections.RemoveAllElements();

#if RSG_DURANGO
	m_Selections.AddElement(&m_SetActiveTitle);
#endif

	m_Selections.AddElement(&m_CreateParty);
	m_Selections.AddElement(&m_InviteFriendToParty);

#if RSG_ORBIS
	rlNpPartyMemberInfo partyMembers[RL_MAX_GAMERS_PER_SESSION];
	m_PartySize = g_rlNp.GetNpParty().GetPartyMembers(partyMembers);

	for (int i = 0; i < m_PartySize; i++)
	{
		m_PartyMembers[i].SetText(partyMembers[i].m_SceNpPartyMemberInfo.npId.handle.data);
		m_Selections.AddElement(&m_PartyMembers[i]);
	}
#elif RSG_DURANGO
	m_PartySize = g_rlXbl.GetPartyManager()->GetPartySize();

	for(int i=0; i < m_PartySize; i++)
	{
		rlXblPartyMemberInfo* pInfo = g_rlXbl.GetPartyManager()->GetPartyMember(i);
		m_PartyMembers[i].SetText(pInfo->m_GamerName);
		m_Selections.AddElement(&m_PartyMembers[i]);
	}
#endif

	m_Selections.AddElement(&m_Back);
}


///////////////////////////////////////////////////////////////////////////////
// UiEntitlementMenu
///////////////////////////////////////////////////////////////////////////////
UiEntitlementMenu::UiEntitlementMenu()
{
	m_Title.SetText("Entitlement");
	m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

	m_Selections.Bind(this, &UiEntitlementMenu::OnEvent);

	float y = UI_CONTENT_Y;

	ADD_SELECTION(m_SaveEntitlement, "Save Entitlement", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_LoadEntitlement, "Load Entitlement", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_ClearEntitlement, "Clear Entitlement", UI_CONTENT_X, y); y += UI_GAP;

	y+= UI_GAP;

	ADD_SELECTION(m_SaveEntitlementDll, "Save Entitlement (DLL)", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_LoadEntitlementDll, "Load Entitlement  (DLL)", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_ClearEntitlementDll, "Clear Entitlement  (DLL)", UI_CONTENT_X, y); y += UI_GAP;


	m_SaveEntitlement.SetEnabled(false);
	m_LoadEntitlement.SetEnabled(false);
	m_ClearEntitlement.SetEnabled(false);
	m_SaveEntitlementDll.SetEnabled(false);
	m_LoadEntitlementDll.SetEnabled(false);
	m_ClearEntitlementDll.SetEnabled(false);

	y += UI_GAP;

	ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP);

	this->SetFocus(&m_Selections);
}

UiEntitlementMenu::~UiEntitlementMenu()
{
	m_Selections.RemoveAllElements();
}

void UiEntitlementMenu::Draw()
{
	this->UiElement::Draw();

#if RSG_PC
	if (rlRosEntitlement::IsEntitlementIOInProgress() || g_rlPc.GetCommerceManager()->IsEntitlementIOInProgress())
	{
		m_LoadEntitlement.SetEnabled(false);
		m_SaveEntitlement.SetEnabled(false);
		m_ClearEntitlement.SetEnabled(false);

		m_LoadEntitlementDll.SetEnabled(false);
		m_SaveEntitlementDll.SetEnabled(false);
		m_ClearEntitlementDll.SetEnabled(false);
	}
	else if (rlRosEntitlement::HasOfflineEntitlement() || g_rlPc.GetCommerceManager()->HasOfflineEntitlement())
	{
		m_LoadEntitlement.SetEnabled(false);
		m_SaveEntitlement.SetEnabled(false);
		m_ClearEntitlement.SetEnabled(true);

		m_LoadEntitlementDll.SetEnabled(false);
		m_SaveEntitlementDll.SetEnabled(false);
		m_ClearEntitlementDll.SetEnabled(true);
	}
	else if (rlRosEntitlement::IsInitialized())
	{
		m_LoadEntitlement.SetEnabled(true);
		m_SaveEntitlement.SetEnabled(true);
		m_ClearEntitlement.SetEnabled(false);

		m_LoadEntitlementDll.SetEnabled(true);
		m_SaveEntitlementDll.SetEnabled(true);
		m_ClearEntitlementDll.SetEnabled(false);
	}
#endif

	m_Back.Draw();

	m_Selections.Draw();
}

void UiEntitlementMenu::OnEvent(UiElement* el, const int evtId, void*)
{
	if(UIEVENT_ACTION == evtId && &m_Selections == el)
	{
		const UiElement* sel = m_Selections.GetSelection();

		if (&m_SaveEntitlement == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SAVE_ENTITLEMENT);
		}
		else if (&m_LoadEntitlement == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_LOAD_ENTITLEMENT);
		}
		else if (&m_ClearEntitlement == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CLEAR_ENTITLEMENT);
		}
		else if (&m_LoadEntitlementDll == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SAVE_ENTITLEMENT_DLL);
		}
		else if (&m_SaveEntitlementDll == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SAVE_ENTITLEMENT_DLL);
		}
		else if (&m_ClearEntitlementDll == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SAVE_ENTITLEMENT_DLL);
		}
		else if(&m_Back == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
		}
	}
	else if(UIEVENT_BACK == evtId)
	{
		this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
	}
}

///////////////////////////////////////////////////////////////////////////////
// UiLiveStreamMenu
///////////////////////////////////////////////////////////////////////////////
UiLiveStreamMenu::UiLiveStreamMenu()
{
	m_Title.SetText("Live Streaming");
	m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

	m_Selections.Bind(this, &UiLiveStreamMenu::OnEvent);

	float y = UI_CONTENT_Y;

	y += UI_GAP;

	ADD_SELECTION(m_Init, "Init", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_Authenticate, "Authenticate", UI_CONTENT_X, y); y += UI_GAP;

	ADD_SELECTION(m_IngestServers, "Ingest Servers: ", UI_CONTENT_X, y); y += UI_GAP;

	ADD_SELECTION(m_StartStreaming, "Start Streaming", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_StopStreaming, "Stop Streaming", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_PauseStreaming, "Pause Streaming", UI_CONTENT_X, y); y += UI_GAP;
	ADD_SELECTION(m_Shutdown, "Shutdown", UI_CONTENT_X, y); y += UI_GAP;

	ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP);

	UpdateEnabled();

	this->SetFocus(&m_Selections);
}

UiLiveStreamMenu::~UiLiveStreamMenu()
{
	m_Selections.RemoveAllElements();
}

void UiLiveStreamMenu::Draw()
{
	this->UiElement::Draw();

	m_Title.Draw();
	m_Back.Draw();

	m_Selections.Draw();
}

void UiLiveStreamMenu::UpdateIngestionServers()
{
#if LIVESTREAM_ENABLED
	int numIngestServers = rlLiveStream::GetNumIngestServers();
	m_IngestServers.RemoveAllElements();

	for (int i = 0; i < numIngestServers && i < MAX_INGEST_SERVERS; i++)
	{
		const char* server = rlLiveStream::GetIngestServer(i);
		m_ServerNames[i].SetText(server);
		m_IngestServers.AddElement(&m_ServerNames[i]);
	}
#endif
}

int UiLiveStreamMenu::GetIngestServerIndex()
{
	const UiElement* sel = m_IngestServers.GetSelection();
	const int selected = sel ? ptrdiff_t_to_int((const UiLabel*) sel - m_ServerNames) : -1;
	return selected;
}

int UiLiveStreamMenu::GetTargetWidth()
{
	// TODO: MultiValue selector
	return 1280;
}

int UiLiveStreamMenu::GetTargetHeight()
{
	// TODO: MultiValue selector
	return 720;
}

int UiLiveStreamMenu::GetTargetFPS()
{
	// TODO: MultiValue selector
	return 30;
}

void UiLiveStreamMenu::UpdateEnabled()
{
#if LIVESTREAM_ENABLED
	bool bInitialized = rlLiveStream::IsInitialized();
	bool bAuthenticating = rlLiveStream::IsAuthenticating();
	bool bAuthenticated = rlLiveStream::IsAuthenticated();
	bool bStreaming = rlLiveStream::IsStreaming();
	bool bPaused = rlLiveStream::IsPaused();

	m_Selections.SetEnabled(&m_Init, !bInitialized);
	m_Selections.SetEnabled(&m_Authenticate, bInitialized && !bAuthenticating && !bAuthenticated);
	m_IngestServers.GetLabelElement()->SetEnabled(bAuthenticated && !bStreaming && !bPaused);
	m_Selections.SetEnabled(&m_IngestServers, bAuthenticated);
	m_Selections.SetEnabled(&m_StartStreaming, bAuthenticated && !bStreaming && !bPaused);
	m_Selections.SetEnabled(&m_StopStreaming, bAuthenticated && bStreaming);
	m_Selections.SetEnabled(&m_PauseStreaming, bAuthenticated && (bStreaming || bPaused));
	m_Selections.SetEnabled(&m_Shutdown, bInitialized);

	if (bPaused)
	{
		m_PauseStreaming.SetText("Unpause");
	}
	else
	{
		m_PauseStreaming.SetText("Pause");
	}

	m_Title.SetText(rlLiveStream::GetState());
#endif
}

void UiLiveStreamMenu::OnEvent(UiElement* el, const int evtId, void*)
{
	if(UIEVENT_ACTION == evtId && &m_Selections == el)
	{
		const UiElement* sel = m_Selections.GetSelection();

		if(&m_Back == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
		}
		else if (&m_Init == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_INIT);
		}
		else if (&m_Shutdown == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_SHUTDOWN);
		}
		else if (&m_Authenticate == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_AUTHENTICATE);
		}
		else if (&m_StartStreaming == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_START_STREAMING);
		}
		else if (&m_StopStreaming == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_STOP_STREAMING);
		}
		else if (&m_PauseStreaming == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_PAUSE_STREAMING);
		}
	}
	else if(UIEVENT_BACK == evtId)
	{
		this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
	}
}

/////////////////////////////////////////////////////////////////////
// UiYoutubeMenu
/////////////////////////////////////////////////////////////////////
UiYoutubeMenu::UiYoutubeMenu()
	: m_Data(NULL)
{
	m_Title.SetText("Youtube Uploads");
	m_Title.SetXY(UI_TITLE_X, UI_TITLE_Y);

	m_Selections.Bind(this, &UiYoutubeMenu::OnEvent);

	rlYoutubeVideoInfo* pInfo = &m_YoutubeUpload.VideoInfo;
	safecpy(pInfo->Title, "Rockfoo Test Upload");
	safecpy(pInfo->Description, "This test upload has a test description");
	pInfo->AddTag("rockfoo");
	pInfo->AddTag("test");
	pInfo->AddTag("upload");
	pInfo->AddTag("kcXDfPZj44fKBj9XHfL1");
	pInfo->Privacy = rlYoutubeVideoInfo::PRIVATE;
	pInfo->Embeddable = false;

	float y = UI_CONTENT_Y;

	y += UI_GAP;

	ADD_SELECTION(m_AccountLink, "Link Account", UI_CONTENT_X, y); y+= UI_GAP;
	ADD_SELECTION(m_LoadVideo, "Load Video", UI_CONTENT_X, y); y+= UI_GAP;
	ADD_SELECTION(m_OAuth, "OAuth Token", UI_CONTENT_X, y); y+= UI_GAP;
	ADD_SELECTION(m_GetUrl, "Get Upload URL", UI_CONTENT_X, y); y+= UI_GAP;
	ADD_SELECTION(m_ClearUrl, "Clear Upload URL", UI_CONTENT_X, y); y+= UI_GAP;
	ADD_SELECTION(m_Upload, "Upload Video", UI_CONTENT_X, y); y+= UI_GAP;
	ADD_SELECTION(m_ChannelInfo, "Get Channel Info", UI_CONTENT_X, y); y+= UI_GAP;

	const char* HARD_CODED_VIDEO_ID = "ER5z4AE3XGk";
	char videoinfobuf[256] = {0};
	formatf(videoinfobuf, "Get Video Info (ID: %s)", HARD_CODED_VIDEO_ID);
	ADD_SELECTION(m_VideoInfo, videoinfobuf, UI_CONTENT_X, y); y+= UI_GAP;
	pInfo->VideoId = HARD_CODED_VIDEO_ID;

	ADD_SELECTION(m_Back, "Back", UI_LEFT_CTRL_X, y+UI_GAP);

	this->SetFocus(&m_Selections);
}

UiYoutubeMenu::~UiYoutubeMenu()
{
	m_Selections.RemoveAllElements();

	if (m_YoutubeStatus.Pending())
	{
		netTask::Cancel(&m_YoutubeStatus);
	}

	if (m_Data)
	{
		delete m_Data;
		m_Data = NULL;
	}
}

void UiYoutubeMenu::Draw()
{
	if (m_YoutubeStatus.Pending())
	{
		m_LoadVideo.SetEnabled(false);
		m_OAuth.SetEnabled(false);
		m_GetUrl.SetEnabled(false);
		m_ClearUrl.SetEnabled(false);
		m_Upload.SetEnabled(false);
		m_ChannelInfo.SetEnabled(false);
		m_VideoInfo.SetEnabled(false);
	}
	else if (m_Data == NULL)
	{
		m_LoadVideo.SetEnabled(true);
		m_OAuth.SetEnabled(false);
		m_GetUrl.SetEnabled(false);
		m_ClearUrl.SetEnabled(false);
		m_Upload.SetEnabled(false);
		m_ChannelInfo.SetEnabled(false);
		m_VideoInfo.SetEnabled(false);
	}
	else if (m_YoutubeUpload.AccessToken.length() == 0)
	{
		m_Title.SetText("Youtube Uploads");
		m_LoadVideo.SetEnabled(false);
		m_OAuth.SetEnabled(true);
		m_GetUrl.SetEnabled(false);
		m_ClearUrl.SetEnabled(false);
		m_Upload.SetEnabled(false);
		m_ChannelInfo.SetEnabled(false);
		m_VideoInfo.SetEnabled(false);
	}
	else if (m_YoutubeUpload.UploadUrl.length() == 0)
	{
		m_Title.SetText("Youtube Uploads");
		m_LoadVideo.SetEnabled(false);
		m_OAuth.SetEnabled(false);
		m_GetUrl.SetEnabled(true);
		m_ClearUrl.SetEnabled(false);
		m_Upload.SetEnabled(false);
		m_ChannelInfo.SetEnabled(true);
		m_VideoInfo.SetEnabled(true);
	}
	else
	{
		m_Title.SetText("Youtube Uploads");
		m_LoadVideo.SetEnabled(false);
		m_OAuth.SetEnabled(false);
		m_GetUrl.SetEnabled(false);
		m_ClearUrl.SetEnabled(true);
		m_Upload.SetEnabled(true);
		m_ChannelInfo.SetEnabled(true);
		m_VideoInfo.SetEnabled(true);
	}

	
	this->UiElement::Draw();

	m_Title.Draw();
	
	m_Back.Draw();

	m_Selections.Draw();
}

bool UiYoutubeMenu::AllocateData() 
{ 
	if (m_Data)
	{
		delete m_Data;
		m_Data = NULL;
	}

	m_Data = rage_new u8[m_YoutubeUpload.UploadSize];
	return m_Data != NULL;
}

void UiYoutubeMenu::OnEvent(UiElement* el, const int evtId, void*)
{
	if(UIEVENT_ACTION == evtId && &m_Selections == el)
	{
		const UiElement* sel = m_Selections.GetSelection();

		if(&m_Back == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
		}
		else if(&m_AccountLink == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_LINK_ACCOUNT);
		}
		else if(&m_LoadVideo == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_LOAD_VIDEO);
		}
		else if(&m_OAuth == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_OAUTH);
		}
		else if(&m_GetUrl == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_URL);
		}
		else if(&m_ClearUrl == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_CLEAR_URL);
		}
		else if(&m_Upload == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_UPLOAD);
		}
		else if(&m_ChannelInfo == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_CHANNEL_INFO);
		}
		else if(&m_VideoInfo == sel)
		{
			this->FireEvent(UIEVENT_ACTION, (void*)ACTION_GET_VIDEO_INFO);
		}
	}
	else if(UIEVENT_BACK == evtId)
	{
		this->FireEvent(UIEVENT_ACTION, (void*)ACTION_BACK);
	}
}

}   //namespace rage
