@echo off

setlocal

set SOURCE=.\assets

pushd %SOURCE%

REM echo.
REM echo Generating .xzp file...
REM echo.

REM %XEDK%\bin\win32\xuipkg /NOLOGO /O assets.xzp *.xui
REM %XEDK%\bin\win32\xuipkg /NOLOGO /A assets.xzp *.png
REM %XEDK%\bin\win32\xuipkg /NOLOGO /A assets.xzp *.ttf

popd

REM move /Y %SOURCE%\ponglive.xzp .

REM echo.
REM echo Generating UI header files...
REM echo.

REM for %%i in ( %SOURCE%\*.xui ) do (
REM     %XEDK%\bin\win32\xui2bin /NOLOGO /HEADER %%i
REM )

REM move /Y %SOURCE%\*.h .

echo.
echo Generating .spa and .h files from .xlast files...
echo.

if "%RAGE_DIR%"=="" goto no_ragedir

for %%i in ( %SOURCE%\*.xlast ) do (
    %XEDK%\bin\win32\spac -nologo -noh %%i
    %RAGE_DIR%\tools\base\exes\xlast2c %%i
)

REM for %%i in ( *.spa ) do copy /Y %%i spa.bin

goto end

:no_ragedir

echo RAGE_DIR is not defined
goto end

:end
pause