@echo off
setlocal
cd /d "X:\gta5\src\dev_ng\rage\suite\samples\sample_snet"
for /f "usebackq delims==" %%v in (`set rs_`) do set %%v=
for /f "usebackq delims==" %%v in (`set rage_`) do set %%v=
set RS_PROJECT=gta5
set RS_PROJROOT=x:\gta5
set RS_CODEBRANCH=x:\gta5\src\dev_ng
set RS_BUILDBRANCH=x:\gta5\titleupdate\dev_ng
set RS_TOOLSROOT=x:\gta5\tools_ng
set RAGE_DIR=%RS_CODEBRANCH%\rage
set RUBYLIB=%RS_TOOLSROOT%\lib
set PATH=%RS_TOOLSROOT%\bin;%RS_TOOLSROOT%\bin\ruby\bin;%PATH%
call "%RS_TOOLSROOT%\bin\setenv.bat"
set SCE_PS3_ROOT=x:/ps3sdk/dev/usr/local/430_001/cell
set _NO_DEBUG_HEAP=1
echo Visual Studio environment:
echo RAGE_DIR:      %RAGE_DIR%
echo SCE_PS3_ROOT   %SCE_PS3_ROOT%

set orbissdk=%SCE_ROOT_DIR%\\ORBIS SDKs\\1.700
if not exist "%orbissdk%" set orbissdk=%PROGRAMFILES(X86)%\\SCE\\ORBIS SDKs\\1.700
if not exist "%orbissdk%" set orbissdk=C:\\Program Files (x86)\\SCE\\ORBIS SDKs\\1.700
if not exist "%orbissdk%" set orbissdk=C:\\Orbis\\1.700
if exist "%orbissdk%" set SCE_ORBIS_SDK_DIR=%orbissdk%

if not exist "%orbissdk%" (
echo.&&echo WARNING:
echo You do not appear to have the correct PS4 SDK installed.
echo The custom PS4 SDK 1.7 is required to build this project.
echo This consists of SDK 1.700.081 plus three patches.
echo You may have trouble building PS4 code without this,
echo but the solution will still load.
echo Please contact your IT dept for more details.
pause
)
set orbissdk=

start "" sample_session_2012.sln
