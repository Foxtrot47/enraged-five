set orbissdk=%SCE_ROOT_DIR%\\ORBIS SDKs\\1.700
if not exist "%orbissdk%" set orbissdk=%PROGRAMFILES(X86)%\\SCE\\ORBIS SDKs\\1.700
if not exist "%orbissdk%" set orbissdk=C:\\Program Files (x86)\\SCE\\ORBIS SDKs\\1.700
if not exist "%orbissdk%" set orbissdk=C:\\Orbis\\1.700
if exist "%orbissdk%" set SCE_ORBIS_SDK_DIR=%orbissdk%

if not exist "%orbissdk%" (
echo.&&echo WARNING:
echo You do not appear to have the correct PS4 SDK installed.
echo The custom PS4 SDK 1.7 is required to build this project.
echo This consists of SDK 1.700.081 plus three patches.
echo You may have trouble building PS4 code without this,
echo but the solution will still load.
echo Please contact your IT dept for more details.
pause
)
set orbissdk=

cd /d x:\gta5\src\dev_ng\rage\base\src\shaders 
start "" x:\gta5\tools_ng\script\coding\shaders\makeshader.bat -platform orbis rage_im.fx
start "" x:\gta5\tools_ng\script\coding\shaders\makeshader.bat -platform durango rage_im.fx

pause
