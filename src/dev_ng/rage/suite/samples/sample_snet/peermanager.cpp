// 
// sample_snet/peermanager.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "peermanager.h"

#include "system/nelem.h"
#include "system/param.h"

namespace rage
{

RAGE_DECLARE_CHANNEL(peermgr)
RAGE_DEFINE_CHANNEL(peermgr)

#define pmDebug1(fmt, ...)  RAGE_DEBUGF1(peermgr, fmt, __VA_ARGS__)
#define pmDebug2(fmt, ...)  RAGE_DEBUGF2(peermgr, fmt, __VA_ARGS__)
#define pmDebug3(fmt, ...)  RAGE_DEBUGF3(peermgr, fmt, __VA_ARGS__)
#define pmWarning(fmt, ...) RAGE_WARNINGF(peermgr, fmt, __VA_ARGS__)
#define pmError(fmt, ...)   RAGE_ERRORF(peermgr, fmt, __VA_ARGS__)

PeerManager::PeerManager()
    : m_CxnMgr(0)
    , m_ChannelId(NET_INVALID_CHANNEL_ID)
    , m_Initialized(false)
{
    for(unsigned i = 0; i < COUNTOF(m_GamerPile); ++i)
    {
        m_GamerPile[i].Clear();
        m_GamerPool.push_back(&m_GamerPile[i]);
    }

    for(unsigned i = 0; i < COUNTOF(m_PeerPile); ++i)
    {
        m_PeerPile[i].Clear();
        m_PeerPool.push_back(&m_PeerPile[i]);
    }

	for(unsigned i = 0; i < COUNTOF(m_TempPlayerPile); ++i)
	{
		m_TempPlayerPool.push_back(&m_TempPlayerPile[i]);
	}
}

PeerManager::~PeerManager()
{
    m_PeerPool.clear();
    m_GamerPool.clear();

	Assert(m_TempPlayerPool.size() == COUNTOF(m_TempPlayerPile));
	m_TempPlayerPool.clear();
	m_TempPlayers.clear();
}

void
PeerManager::Init(netConnectionManager* cxnMgr,
                const unsigned channelId)
{
    if(AssertVerify(!m_Initialized))
    {
        Assert(m_PeersByEndpointId.empty());
        Assert(m_PeersById.empty());
        Assert(m_GamersById.empty());

        Assert(m_GamerPool.size() == COUNTOF(m_GamerPile));
        Assert(m_PeerPool.size() == COUNTOF(m_PeerPile));

        Assert(0 == m_LocalPeer.m_NumGamers);

        m_CxnMgr = cxnMgr;
        m_ChannelId = channelId;

        m_Initialized = true;
    }
}

void
PeerManager::Shutdown()
{
    if(m_Initialized)
    {
        while(!m_GamersById.empty())
        {
            this->RemoveGamer(m_GamersById.begin()->second->m_GamerInfo.GetGamerId());
        }

		while(!m_TempPlayers.empty())
		{
			this->RemoveGamer(m_TempPlayers.front()->GetGamerId());
		}

        Assert(m_GamersById.empty());
        Assert(m_GamerPool.size() == COUNTOF(m_GamerPile));

		Assert(m_GamersById.empty());
		Assert(m_GamerPool.size() == COUNTOF(m_GamerPile));

        Assert(m_PeersById.empty());
        Assert(m_PeersByEndpointId.empty());
        Assert(m_PeerPool.size() == COUNTOF(m_PeerPile));

		Assert(m_TempPlayers.empty());
		Assert(m_TempPlayerPool.size() == COUNTOF(m_TempPlayerPile));

        Assert(0 == m_LocalPeer.m_NumGamers);

        m_LocalPeer.Clear();

        m_CxnMgr = 0;
        m_ChannelId = NET_INVALID_CHANNEL_ID;

        m_Initialized = false;
    }
}

Gamer*
PeerManager::AddRemoteGamer(const rlGamerInfo& gamerInfo,
							const EndpointId endpointId,
                            const unsigned sizeofData,
                            const void* data)
{
    Assert(gamerInfo.IsRemote());

    pmDebug2("Adding remote gamer:%s...", gamerInfo.GetName());

    Gamer* gamer = 0;

    if(AssertVerify(!m_GamerPool.empty())
        && AssertVerify(!this->GetGamer(gamerInfo.GetGamerId())))
    {
        const rlPeerInfo& peerInfo = gamerInfo.GetPeerInfo();
        Peer* peer = this->GetPeer(peerInfo.GetPeerId());
        bool createdPeer = false;

        if(!peer)
        {
            peer = this->AddPeer(peerInfo, endpointId);
            createdPeer = (0 != peer);
        }

        if(peer)
        {
            gamer = *m_GamerPool.begin();

            gamer->Reset(gamerInfo);

            if(AssertVerify(gamer->ImportData(data, sizeofData))
                && AssertVerify(peer->AddGamer(gamer)))
            {
                m_GamerPool.pop_front();
                m_GamersById.insert(gamer->GetGamerId(), gamer);

                pmDebug2("Added remote gamer:%s, team:%d",
                            gamerInfo.GetName(),
                            gamer->m_TeamId);
            }
            else
            {
                gamer->Clear();
                gamer = 0;

                if(createdPeer)
                {
                    this->RemovePeer(peer);
                }
            }
        }
    }

	// remove any temporary players
	RemoveTemporaryPlayer(gamerInfo.GetGamerId());

    return gamer;
}

bool
PeerManager::AddLocalGamer(Gamer* gamer,
                            const unsigned sizeofData,
                            const void* data)
{
    Assert(gamer->IsLocal());

    pmDebug2("Adding local gamer:%s...", gamer->GetName());

    const bool success = AssertVerify(gamer->ImportData(data, sizeofData))
                        && AssertVerify(m_LocalPeer.AddGamer(gamer));

    if(success)
    {
        m_GamersById.insert(gamer->GetGamerId(), gamer);
    }

    return success;
}

bool
PeerManager::RemoveGamer(const rlGamerId& gamerId)
{
    bool success = false;
    Gamer* gamer = this->GetGamer(gamerId);

    if(gamer)
    {
        pmDebug2("Removing %s gamer:%s...",
                    gamer->IsLocal() ? "LOCAL" : "REMOTE",
                    gamer->GetName());

        m_GamersById.erase(gamerId);

        if(gamer->IsLocal())
        {
            success = m_LocalPeer.RemoveGamer(gamerId);
        }
        else
        {
            const u64 peerId = gamer->GetPeerInfo().GetPeerId();
            Peer* peer = this->GetPeer(peerId);
            if(AssertVerify(peer))
            {
                success = peer->RemoveGamer(gamerId);
                Assert(success);

                if(!peer->m_NumGamers)
                {
                    AssertVerify(this->RemovePeer(peerId));
                }
            }

            gamer->Clear();
            m_GamerPool.push_back(gamer);
        }
    }

	// possible is a temp gamer?
	if (!success)
	{
		Gamer* player = GetTempPlayer(gamerId);
		if (player)
		{
			RemoveTemporaryPlayer(gamerId);
			success = true;
		}
	}

    return success;
}

unsigned
PeerManager::GetGamerCount() const
{
    return (unsigned)m_GamersById.size();
}

unsigned
PeerManager::GetPeerCount() const
{
    return (unsigned)m_PeersByEndpointId.size();
}

unsigned
PeerManager::GetGamers(Gamer** gamers, const unsigned maxGamers)
{
    if(maxGamers >= m_GamersById.size())
    {
        GamersById::iterator it = m_GamersById.begin();
        GamersById::const_iterator stop = m_GamersById.end();

        for(int i = 0; stop != it; ++it, ++i)
        {
            gamers[i] = it->second;
        }
    }

    return (unsigned)m_GamersById.size();
}

Gamer*
PeerManager::GetGamerById(const rlGamerId& gamerId)
{
    GamersById::iterator it = m_GamersById.find(gamerId);
    return (m_GamersById.end() == it) ? 0 : it->second;
}

unsigned
PeerManager::GetPeers(Peer** peers, const unsigned maxPeers)
{
    if(maxPeers >= m_PeersByEndpointId.size())
    {
        PeersByEndpointId::iterator it = m_PeersByEndpointId.begin();
        PeersByEndpointId::const_iterator stop = m_PeersByEndpointId.end();

        for(int i = 0; stop != it; ++it, ++i)
        {
            peers[i] = it->second;
        }
    }

    return (unsigned)m_PeersByEndpointId.size();
}

Gamer*
PeerManager::GetGamer(const rlGamerId& gamerId)
{
    GamersById::iterator it = m_GamersById.find(gamerId);

    return m_GamersById.end() != it ? it->second : 0;
}

const Gamer*
PeerManager::GetGamer(const rlGamerId& gamerId) const
{
    return const_cast<PeerManager*>(this)->GetGamer(gamerId);
}

Peer*
PeerManager::GetPeer(const EndpointId endpointId)
{
    PeersByEndpointId::iterator it = m_PeersByEndpointId.find(endpointId);

    return (m_PeersByEndpointId.end() != it) ? it->second : 0;
}

const Peer*
PeerManager::GetPeer(const EndpointId endpointId) const
{
    return const_cast<PeerManager*>(this)->GetPeer(endpointId);
}

Peer*
PeerManager::GetPeer(const u64 peerId)
{
    PeersById::iterator it = m_PeersById.find(peerId);

    return (m_PeersById.end() != it) ? it->second : 0;
}

const Peer*
PeerManager::GetPeer(const u64 peerId) const
{
    return const_cast<PeerManager*>(this)->GetPeer(peerId);
}

//private:

Peer*
PeerManager::AddPeer(const rlPeerInfo& peerInfo, const EndpointId endpointId)
{
    Assert(peerInfo.IsRemote());

    //Make sure it's a valid address.
    Assert(endpointId != NET_INVALID_ENDPOINT_ID);

    Peer* peer = 0;

    pmDebug2("Adding peer:%s...", NET_ADDR_FOR_PRINTF(m_CxnMgr->GetAddress(endpointId)));

    if(AssertVerify(!m_PeerPool.empty())
        && AssertVerify(!this->GetPeer(endpointId)))
    {
        Peer* tmp = *m_PeerPool.begin();
        tmp->Clear();

        tmp->m_CxnId = m_CxnMgr->OpenConnection(endpointId,
                                                m_ChannelId,
                                                NULL,
                                                0,
                                                NULL);

        if(AssertVerify(tmp->m_CxnId >= 0))
        {
            peer = tmp;
        }
    }

    if(peer)
    {
        peer->m_PeerInfo = peerInfo;
        peer->m_EndpointId = endpointId;
        m_PeerPool.pop_front();
        m_PeersById.insert(peerInfo.GetPeerId(), peer);
        m_PeersByEndpointId.insert(endpointId, peer);
        pmDebug2("Added peer:%s", NET_ADDR_FOR_PRINTF(m_CxnMgr->GetAddress(endpointId)));
    }

    return peer;
}

bool
PeerManager::RemovePeer(Peer* peer)
{
    Assert(peer >= m_PeerPile && peer < &m_PeerPile[MAX_PEERS]);

    return this->RemovePeer(peer->m_EndpointId);
}

bool
PeerManager::RemovePeer(const u64 peerId)
{
    PeersById::iterator it = m_PeersById.find(peerId);
    return m_PeersById.end() != it
            && this->RemovePeer(it->second->m_EndpointId);
}

bool
PeerManager::RemovePeer(const EndpointId endpointId)
{
    bool success = false;

    pmDebug2("Removing peer:%s...", NET_ADDR_FOR_PRINTF(m_CxnMgr->GetAddress(endpointId)));

    PeersByEndpointId::iterator it = m_PeersByEndpointId.find(endpointId);

    if(AssertVerify(m_PeersByEndpointId.end() != it))
    {
        Peer* peer = it->second;
        if(!m_CxnMgr->IsClosed(peer->m_CxnId))
        {
            m_CxnMgr->CloseConnection(peer->m_CxnId, NET_CLOSE_GRACEFULLY);
        }

        m_PeersByEndpointId.erase(it);
        m_PeersById.erase(peer->m_PeerInfo.GetPeerId());
        peer->Clear();
        m_PeerPool.push_back(peer);
        success = true;
    }

    return success;
}

Gamer* PeerManager::AddTemporaryPlayer(const rlGamerInfo &gamerInfo, const EndpointId endpointId, const unsigned sizeofData,const void* data)
{
	Assert(gamerInfo.IsRemote());
	pmDebug2("Adding temporary remote gamer:%s...", gamerInfo.GetName());

	Gamer* gamer = NULL;

	if(AssertVerify(!m_TempPlayerPool.empty())
		&& AssertVerify(!this->GetTempPlayer(gamerInfo.GetGamerId())))
	{
		Gamer* gamer = *m_TempPlayerPool.begin();

		const rlPeerInfo& peerInfo = gamerInfo.GetPeerInfo();
		Peer* peer = this->GetPeer(peerInfo.GetPeerId());
		bool createdPeer = false;

		if(!peer)
		{
			peer = this->AddPeer(peerInfo, endpointId);
			createdPeer = (0 != peer);
		}

		if(peer)
		{
			gamer = *m_TempPlayerPool.begin();

			gamer->Reset(gamerInfo);

			if(AssertVerify(gamer->ImportData(data, sizeofData))
				&& AssertVerify(peer->AddGamer(gamer)))
			{
				m_TempPlayerPool.pop_front();
				m_TempPlayers.push_back(gamer);

				pmDebug2("Added temp remote gamer:%s, team:%d", gamerInfo.GetName(), gamer->m_TeamId);
			}
			else
			{
				gamer->Clear();
				gamer = 0;

				if(createdPeer)
				{
					this->RemovePeer(peer);
				}
			}
		}
	}

	return gamer;
}

void PeerManager::RemoveTemporaryPlayer(const rlGamerId& gamerId)
{
	Gamer* pPlayer = GetTempPlayer(gamerId);
	if(pPlayer)
	{
		pmDebug2("Removing temp remote gamer: %" I64FMT "d", gamerId.GetId());

		pPlayer->Clear();

		m_TempPlayers.erase(pPlayer);
		m_TempPlayerPool.push_back(pPlayer);
	}
}

// PURPOSE
// Retrieves a netPlayer from the temporary array
Gamer* PeerManager::GetTempPlayer(const rlGamerId& id)
{
	Gamer* pPlayer = NULL;

	GamerList::const_iterator existIt   = m_TempPlayers.begin();
	GamerList::const_iterator existStop = m_TempPlayers.end();
	for(; existIt != existStop; ++existIt)
	{
		Gamer* player = (Gamer*)*existIt;

		if(player->GetGamerId() == id)
		{
			pPlayer = player;
			break;
		}
	}

	return pPlayer;
}

const Gamer*
PeerManager::GetTempPlayer(const rlGamerId& gamerId) const
{
	return const_cast<PeerManager*>(this)->GetTempPlayer(gamerId);
}

}   //namespace rage
