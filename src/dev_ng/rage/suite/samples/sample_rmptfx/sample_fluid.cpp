// 
// sample_ptcore/sample_fluid.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Fluid
//	PURPOSE:
//		This sample demonstrates how to use the particles to simulate fluids.


#include "atl/array.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "math/random.h"
#include "grcore/im.h"
#include "math/simplemath.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "ptcore/draw.h"
#include "sample_physics/sample_physics.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/colors.h"


using namespace rage;

PARAM(file, "Reserved for future expansion, not currently hooked up");


class ptcFluidParticle
{
public:
	const Vector3& GetPosition () const
	{
		return m_Matrix.d;
	}

	void SetPosition (const Vector3& position)
	{
		DebugAssert(position.Mag2()<1.0e12f);
		m_Matrix.d.Set(position);
		m_PrevPosition.Set(position);
	}

	void MovePosition (const Vector3& displacement)
	{
		DebugAssert(displacement.Mag2()<1.0e12f);
		m_Matrix.d.Add(displacement);
	}
	
	Vector3 UpdateVelocity (const Vector3& gravity, float damping, float timeStep)
	{
		m_Velocity.AddScaled(gravity,timeStep);
		m_Velocity.Scale(1.0f-damping*timeStep);
		Vector3 displacement(m_Velocity);
		displacement.Scale(timeStep);
		return displacement;
	}

	void ComputeVelocity (float invTimeStep)
	{
		m_Velocity.Subtract(m_Matrix.d,m_PrevPosition);
		m_Velocity.Scale(invTimeStep);
		m_PrevPosition.Set(m_Matrix.d);
		DebugAssert(m_Velocity.Mag2()<1.0e12f);
		DebugAssert(m_PrevPosition.Mag2()<1.0e12f);
	}

	ptcFluidParticle* GetNextInCell ()
	{
		return m_NextInCell;
	}

	void SetNextInCell (ptcFluidParticle* nextParticle)
	{
		m_NextInCell = nextParticle;
	}

public:
	float m_Life;
	float m_SizeX;
	float m_SizeY;
	u32 m_TileId;
	Vector3 m_Velocity;
	Vector3 m_AngularVelocity;
	Matrix34 m_Matrix;
	Vector3 m_PrevPosition;

protected:
	ptcFluidParticle* m_NextInCell;
};


class ptcSpatialCell
{
public:
	void Init ()
	{
		m_FirstParticle = NULL;
		m_FrameCount = BAD_INDEX;
	}

	void AddParticle (ptcFluidParticle& particle, int frameCount)
	{
		// If this cell is up to date (if it has already been used on this frame), then append any particles already here to the new particle.
		particle.SetNextInCell(m_FrameCount==frameCount ? m_FirstParticle : NULL);

		// Set the frame count.
		m_FrameCount = frameCount;

		// Make the given particle the first particle in this cell.
		m_FirstParticle = &particle;
	}

	ptcFluidParticle* GetFirstParticle (int frameCount)
	{
		return (frameCount==m_FrameCount ? m_FirstParticle : NULL);
	}

protected:
	ptcFluidParticle* m_FirstParticle;
	int m_FrameCount;
};


class ptcSpatialGrid
{
public:
	void Init (float gridSize, u32 gridSizeInCells)
	{
		// Set the total number of cells, and create and initialize the array of cells.
		int numCells = gridSizeInCells*gridSizeInCells*gridSizeInCells;
		m_SpatialCells.Resize(numCells);
		for (int cellIndex=0; cellIndex<numCells; cellIndex++)
		{
			m_SpatialCells[cellIndex].Init();
		}

		// Set the grid size and inverse cell size.
		m_GridSize = gridSize;
		m_InvCellSize = (float)gridSizeInCells/m_GridSize;
		Assert(GetNextPow2(gridSizeInCells-1)==gridSizeInCells && "The fluid spatial grid must have a power of 2 size in cells.");
		m_GridSizeInCells = gridSizeInCells;

		// Compute the dimension bit mask, which masks the lower bits of an unsigned integer to wrap around the number of cells along each dimension,
		// and compute the number of dimension bits in the mask, which is used to shift bits to translate between cell index along a dimension and
		// cell index in the linear list of cells.
		Assert(m_GridSizeInCells>1);
		m_DimensionBitMask = BIT(0);
		m_DimensionNumBits = 1;
		u32 power2 = 2;
		while (power2<m_GridSizeInCells)
		{
			m_DimensionBitMask |= BIT(m_DimensionNumBits);
			m_DimensionNumBits++;
			power2 *= 2;
		}

		// Make sure that three cell index numbers fit into a u32.
		Assert(m_DimensionNumBits<10);
	}

	void AddParticle (ptcFluidParticle& particle, int frameCount)
	{
		ptcSpatialCell& cell = GetCell(particle.GetPosition());
		cell.AddParticle(particle,frameCount);
	}

	ptcSpatialCell& GetCell (const Vector3& position)
	{
		// Convert each of the three position coordinates into a cell index along each dimension.
		u32 cellX = (u32)(fmodf(position.x,m_GridSize)*m_InvCellSize);
		u32 cellY = (u32)(fmodf(position.y,m_GridSize)*m_InvCellSize);
		u32 cellZ = (u32)(fmodf(position.z,m_GridSize)*m_InvCellSize);

		// Combine the three cell indices into a single u12, for a spatial grid with 4k entries.
		// Along each dimension, particles in cells beyond 16 loop back into repeating cells.
		u32 cellIndex = cellX & m_DimensionBitMask;
		cellIndex += (cellY & m_DimensionBitMask) << m_DimensionNumBits;
		cellIndex += (cellZ & m_DimensionBitMask) << m_DimensionNumBits << m_DimensionNumBits;

		// Return the cell containing the specified position.
		return m_SpatialCells[cellIndex];
	}
	
	void FindNeighbors (const ptcFluidParticle& particle, int frameCount, float interactionDistance2, atArray<ptcFluidParticle*>& neighbors, atArray<Vector3>& offsets,
						atArray<float>& distances)
	{
		// Clear out the given arrays of neighbors, relative positions and distances.
		neighbors.clear();
		offsets.clear();
		distances.clear();

		// Convert each of the three position coordinates into a cell index along each dimension, subtracting 1 to get a neighbor cell.
		Vector3 position(particle.GetPosition());
		u32 cellX = (u32)(fmodf(position.x,m_GridSize)*m_InvCellSize-1.0f);
		u32 cellY = (u32)(fmodf(position.y,m_GridSize)*m_InvCellSize-1.0f);
		u32 cellZ = (u32)(fmodf(position.z,m_GridSize)*m_InvCellSize-1.0f);

		// Loop over the cell and its 26 neighbors.
		Vector3 offset;
		int indexX,indexY,indexZ;
		u32 cellIndex;
		for (indexX=0; indexX<3; indexX++)
		{
			for (indexY=0; indexY<3; indexY++)
			{
				for (indexZ=0; indexZ<3; indexZ++)
				{
					cellIndex = cellX & m_DimensionBitMask;
					cellIndex += (cellY & m_DimensionBitMask) << m_DimensionNumBits;
					cellIndex += (cellZ & m_DimensionBitMask) << m_DimensionNumBits << m_DimensionNumBits;
					ptcFluidParticle* otherParticle = m_SpatialCells[cellIndex].GetFirstParticle(frameCount);
					while (otherParticle)
					{
						if (otherParticle!=&particle)
						{
							offset.Subtract(otherParticle->GetPosition(),position);
							float distance2 = offset.Mag2();
							if (distance2<interactionDistance2)
							{
								neighbors.Append() = otherParticle;
								offsets.Append() = offset;
								distances.Append() = sqrtf(distance2);
								if (neighbors.GetCount()==neighbors.GetCapacity())
								{
									// The maximum number of neighbors was hit, so stop collecting neighbors.
									return;
								}
							}
						}

						otherParticle = otherParticle->GetNextInCell();
					}

					cellZ = (cellZ<m_GridSizeInCells-1 ? cellZ+1 : 0);
				}

				cellY = (cellY<m_GridSizeInCells-1 ? cellY+1 : 0);
			}

			cellX = (cellX<m_GridSizeInCells-1 ? cellX+1 : 0);
		}
	}

protected:
	atArray<ptcSpatialCell> m_SpatialCells;
	float m_GridSize;
	float m_InvCellSize;
	u32 m_GridSizeInCells;
	u32 m_DimensionBitMask;
	u32 m_DimensionNumBits;
};


class ptcSampleFluid : public ragesamples::physicsSampleManager
{
protected:

	int ComputeNumToActivate (float timeStep)
	{
		float numParticles = m_ProductionRate*(m_SpareTime+timeStep);
		m_SpareTime = fmodf(numParticles,1.0f)/m_ProductionRate;
		return (int)numParticles;
	}
	
	void InitClient ()
	{
		m_DrawMode = SPHERE;
		physicsSampleManager::InitClient();

		ragesamples::phDemoWorld& waterWorld = MakeNewDemoWorld("water");
		Vector3 position(0.0f,0.2f,0.0f);
		bool alignBottom = false;
		Vector3 rotation(0.0f,0.0f,0.5f*PI);
		waterWorld.CreateFixedObject("cinderblock",position,alignBottom,rotation);

		// STEP #1. Allocate space to hold your particle points
		//-- If all you need are screen aligned billboards, then you only need a position.  However, we
		// want to demonstrate non-screen aligned particles as well, so we'll allocate storage to hold
		// a complete matrix.

		int maxNumParticles = 1000;
		m_FluidParticles.Resize(maxNumParticles);
		m_ParticleRadius = 0.04f;
		m_InteractionDistance = 0.2f;
		m_ProductionRate = 50.0f;
		m_PressureConstant = 1.0f;
		m_RestDensity = 0.02f;

		// Initialize the spatial grid for sorting particles.
		const float gridSize = 3.2f;		// 3.2m on a side (20cm per cell)
		const int gridSizeInCells = 16;		// 16 cells on a side (4096 total cells)
		m_SpatialGrid.Init(gridSize,gridSizeInCells);

		// Make the lifespan just long enough that the first particle expires about the same time the initial pool of inactive particles runs out.
		m_LifeSpan = (float)maxNumParticles/m_ProductionRate;
		m_SpareTime = 0.0f;
		m_FirstInactiveIndex = 0;
		m_FirstActiveIndex = 0;

		// Initialize all the particles as inactive.
		int particleIndex;
		for (particleIndex=0; particleIndex<m_FluidParticles.GetCount(); particleIndex++)
		{
			m_FluidParticles[particleIndex].m_Life = -1.0f;
		}

		// Create an initial production time worth of particles. Their lifespans are adjusted so that they phase out at the production rate.
		const float initialProductionTime = 1.0f;
		int numStartingPtx = ComputeNumToActivate(initialProductionTime);
		float lifeSpan = m_LifeSpan-initialProductionTime;
		float deltaLifeSpan = 1.0f/m_ProductionRate;
		int frameCount = TIME.GetFrameCount();
		for (int i = 0; i < numStartingPtx; ++i)
		{
			ActivateParticle(lifeSpan,frameCount);
			lifeSpan += deltaLifeSpan;
		}
		
		// STEP #2. Load any textures needed by the particles
		//-- We'll load a simple R* texture for now
		m_ParticleTex = grcTextureFactory::GetInstance().Create("rs");
		
		// -STOP
	}

	void ShutdownClient ()
	{
		if (m_ParticleTex)
		{
			m_ParticleTex->Release();
		}
	}


	void UpdateClient ()
	{
		physicsSampleManager::UpdateClient();
		
		// Step #3.  Update the particles.

		// Decrement the lifetime of all active particles, and update all active particles.
		const float timeStep = TIME.GetSeconds();
		Vector3 gravity(PHSIM->GetGravity());
		Vector3 position,nextPosition,displacement;
		const float damping = 0.5f;
		int frameCount = TIME.GetFrameCount();
		int maxParticleIndex = m_FluidParticles.GetCount()-1;
		int particleIndex = m_FirstActiveIndex;
		DebugAssert(particleIndex!=m_FirstInactiveIndex);
		while (particleIndex!=m_FirstInactiveIndex)
		{
			// Decrease the particle's remaining lifetime and see if it is over.
			ptcFluidParticle& particle = m_FluidParticles[particleIndex];
			particle.m_Life -= timeStep;
			if (particle.m_Life>0.f)
			{
				// Update the particle's velocity and find its displacement.
				displacement.Set(particle.UpdateVelocity(gravity,damping,timeStep));

				// See if the particle hits anything on this frame.
				phIntersection intersection;
				position.Set(particle.GetPosition());
				nextPosition.Add(position,displacement);
				DebugAssert(position.Mag2()<1.0e12f);
				DebugAssert(nextPosition.Mag2()<1.0e12f);
				Assert(PHLEVEL);
				if (PHLEVEL->TestSweptSphere(position,nextPosition,m_ParticleRadius,&intersection))
				{
					// The particle collided, so stop its motion on the collision plane.
					Vector3 normal(intersection.GetNormal());
					displacement.AddScaled(normal,(intersection.GetT()-1.0f)*displacement.Dot(normal));
				}
				else if (PHLEVEL->TestSphere(position,m_ParticleRadius,&intersection))
				{
					// The particle's trajectory did not collide, but its current position collides.
					Vector3 normal(intersection.GetNormal());
					displacement.AddScaled(normal,intersection.GetDepth()-displacement.Dot(normal));
				}

				// Set the particle's new position.
				particle.MovePosition(displacement);

				// Update the particle in the spatial grid. It will be removed from its old cell in the grid only when that cell is used and updated.
				m_SpatialGrid.AddParticle(particle,frameCount);
			}

			// Increment the particle index, looping back to zero from the top.
			particleIndex = (particleIndex<maxParticleIndex ? particleIndex+1 : 0);

			// Make sure that if the particle counter goes around the horn then the first inactive index is less than the first active index.
			Assert(particleIndex!=0 || m_FirstInactiveIndex<=m_FirstActiveIndex);
		}

		// Move the first active particle marker ahead of all the particles whose lives have expired.
		while (m_FluidParticles[m_FirstActiveIndex].m_Life<=0.0f)
		{
			m_FirstActiveIndex++;
			DebugAssert(m_FirstActiveIndex!=m_FirstInactiveIndex);
			if (m_FirstActiveIndex==m_FluidParticles.GetCapacity())
			{
				m_FirstActiveIndex = 0;
			}
		}

		// Compute the density and pressure on all active particles, and displace particles in pairs from the pressure.
		Vector3 unitOffset;
		float invInteraction = 1.0f/m_InteractionDistance;
		float timeStep2 = square(timeStep);
		const int maxNumNeighbors = 32;
		atArray<ptcFluidParticle*> neighbors;
		neighbors.Reserve(maxNumNeighbors);
		atArray<float> distances;
		distances.Reserve(maxNumNeighbors);
		atArray<Vector3> offsets;
		offsets.Reserve(maxNumNeighbors);
		particleIndex = m_FirstActiveIndex;
		DebugAssert(particleIndex!=m_FirstInactiveIndex);
		int numNeighbors,neighborIndex;
		while (particleIndex!=m_FirstInactiveIndex)
		{
			// Fill in the list of neighbor particles.
			ptcFluidParticle& particle = m_FluidParticles[particleIndex];
			m_SpatialGrid.FindNeighbors(particle,frameCount,m_InteractionDistance,neighbors,offsets,distances);

			// Find the density at this particle from its neighbors.
			float density = 0.0f;
			numNeighbors = neighbors.GetCount();
			if (numNeighbors>0)
			{
				for (neighborIndex=0; neighborIndex<numNeighbors; neighborIndex++)
				{
					density += square(1.0f-distances[neighborIndex]*invInteraction);
				}

				// Find the pressure from the density.
				float pressure = m_PressureConstant*(density-m_RestDensity);

				// Displace this particle and its neighbors from the pressure.
				for (neighborIndex=0; neighborIndex<numNeighbors; neighborIndex++)
				{
					unitOffset.InvScale(offsets[neighborIndex],distances[neighborIndex]);
					displacement.Scale(unitOffset,pressure*timeStep2*(1.0f-distances[neighborIndex]*invInteraction));
					Assert(neighbors[neighborIndex]);
					neighbors[neighborIndex]->MovePosition(displacement);
					particle.MovePosition(-displacement);
				}
			}

			// Increment the particle index, looping back to zero from the top.
			particleIndex = (particleIndex<maxParticleIndex ? particleIndex+1 : 0);

			// Make sure that if the particle counter goes around the horn then the first inactive index is less than the first active index.
			Assert(particleIndex!=0 || m_FirstInactiveIndex<=m_FirstActiveIndex);
		}

		// Set the particle velocities from the position changes on this frame.
		float invTimeStep = 1.0f/timeStep;
		particleIndex = m_FirstActiveIndex;
		while (particleIndex!=m_FirstInactiveIndex)
		{
			// Set the particle's velocity for this frame from its position change.
			ptcFluidParticle& particle = m_FluidParticles[particleIndex];
			particle.ComputeVelocity(invTimeStep);

			// Increment the particle index, looping back to zero from the top.
			particleIndex = (particleIndex<maxParticleIndex ? particleIndex+1 : 0);

			// Make sure that if the particle counter goes around the horn then the first inactive index is less than the first active index.
			Assert(particleIndex!=0 || m_FirstInactiveIndex<=m_FirstActiveIndex);
		}

		// Activate more particles.
		int numToActivate = ComputeNumToActivate(timeStep);
		for (int index=0; index<numToActivate; index++)
		{
			if (!ActivateParticle(m_LifeSpan,frameCount))
			{
				// Activation failed, so save the available activation time for the next frame.
				m_SpareTime += timeStep;
			}
		}

		// -STOP
	}

	void DrawClient ()
	{
		physicsSampleManager::DrawClient();

		// STEP #4.  Prepare for rendering particles
		//-- Ideally, particles will be rendered in a batch (i.e. no other render calls but particles).
		//	You will want to call the following code once per batched render of particles.
		// <b>NOTE:</b> It is very <b>important</b> that you call ptcBuildRotTable after every
		//	camera change, since this is what is used for billboarded particles. 
		
		//-- This call is used to inform the particle engine how many unique particle "tiles" are
		// contained within one texture.  This setting may change outside of a Begin/End group.
		// In our case, we're using a texture broken into 3x3 tiles
		ptcSetRes(3);
		
		//-- Also need to bind the texture to use.  This setting may also change outside of a 
		// Begin/End group.
		grcBindTexture( m_ParticleTex);

		// -STOP
		
		// Misc. other renderstates
		grcState::SetAlphaBlend(true);
		grcState::SetAlphaRef(40);
			
		// @@@@@@@@@@@This should be combined with rendering in sample_testmanager @@@@@@@@@@@@@
		// STEP #5.  Render the particles
		//-- The RAGE base particle system can only handle a fixed number of particles at any given
		// time, so we need to continue iterating through our list until all particles are rendered.
		// Note how the calls are similar to the immediate mode render calls (i.e. Begin/Draw/End)
/*		const int maxRenderCount = (m_DrawMode == BILLBOARD) ? ptcBeginMax : ptcBeginQuadsMax;
		const int particlesToDraw = m_FluidParticles.GetCount();
		int particlesDrawn = 0;
		while( particlesDrawn != particlesToDraw ) {
			// Tell the particle engine how many particles to render in this iteration
			const int loopCount = Min(maxRenderCount, particlesToDraw - particlesDrawn);
			// We currently use 2 different draw methods based on how you want the particles displayed
			if ( m_DrawMode == BILLBOARD ) {
				ptcBegin(loopCount);
				for (int i = 0; i < loopCount; ++i) {
					const ptcFluidParticle &pt = m_FluidParticles[particlesDrawn++];
					// Arbitrarily choose X angular velocity for rotation
					ptcDraw(pt.m_TileId, pt.m_AngularVelocity.x*pt.m_Life, pt.m_Matrix.d.x, pt.m_Matrix.d.y,pt.m_Matrix.d.z, 
								pt.m_SizeX, pt.m_SizeY, Color32(1.f,1.f,1.f,Min(pt.m_Life*2.f,1.f)));
				}

				// Send these particles down the pipe and prepare for next batch if needed
				ptcEnd();
			} 
			else if ( m_DrawMode == FLAT ) {
				ptcBeginQuads(loopCount);
				for (int i = 0; i < loopCount; ++i) {
					const ptcFluidParticle &pt = m_FluidParticles[particlesDrawn++];
					
					// We're going to use the x-y plane of the matrix for each quad
					
					ptcDrawQuad(pt.m_TileId, pt.m_Matrix.d, pt.m_Matrix.b, pt.m_Matrix.a,
						pt.m_SizeX, pt.m_SizeY, Color32(1.f,1.f,1.f,Min(pt.m_Life*2.f,1.f)));
				}

				// Send these particles down the pipe and prepare for next batch if needed
				ptcEnd();
			}
			else if (m_DrawMode == SPHERE)
			{
				const int steps = 4;
				const bool longitudinalCircles = false;
				const bool solid = true;
				grcState::Default();	
				
				grcWorldIdentity();
				grcColor(Color_MediumTurquoise);
				for (int particleIndex=0; particleIndex<loopCount; particleIndex++)
				{
					const Vector3& center = m_FluidParticles[particlesDrawn++].m_Matrix.d;
					grcDrawSphere(m_ParticleRadius, center, steps, longitudinalCircles, solid);
				}
			}
		}

		// STEP #6.  Inform rage that we are no longer rendering particles
		//-- This needs to be done before any other type of rendering is done
		ptcMicrocode(false);*/
		
		// -STOP
	}

	bool ActivateParticle (float lifeSpan, int frameCount)
	{
		if (m_FirstInactiveIndex!=(m_FirstActiveIndex>0 ? m_FirstActiveIndex-1 : m_FluidParticles.GetCount()-1))
		{
			// There is more than one inactive particle, so activate one. At least one particle is always kept inactive just to make the accounting easier,
			// so that m_FirstInactiveIndex doesn't have to be equal to m_FirstActiveIndex or BAD_INDEX.
			ptcFluidParticle& outPoint = m_FluidParticles[m_FirstInactiveIndex];
			DebugAssert(outPoint.m_Life<=0.0f);

			// Determine a max range based on a desired density
			const float ptxDensity = 1000.0f;		// particles per meter
			const float ptxSpacing = (float) m_FluidParticles.GetCount() / ptxDensity * 0.5f;	// cut it in half since it's +/-
			outPoint.m_Matrix.Identity3x3();
			// Since particles have no impact on replay, use drawrand
			Vector3 position;
			position.x = g_DrawRand.GetVaried(0.5f)*ptxSpacing;
			position.y = 1.0f+g_DrawRand.GetVaried(0.5f)*ptxSpacing;
			position.z = g_DrawRand.GetVaried(0.5f)*ptxSpacing;
			outPoint.SetPosition(position);
		
			// Init velocity
			const float ptxSpeed = 0.1f;			// meters per second
			float x,y,z;
			x = g_DrawRand.GetVaried(2.f)*ptxSpeed;
			y = g_DrawRand.GetVaried(2.f)*ptxSpeed;
			z = g_DrawRand.GetVaried(2.f)*ptxSpeed;
			outPoint.m_Velocity.Set(x, y, z);
		
			// Init angular velocity
			const float ptxSpinRate = 2.0f * PI;	// radians per second		
			x = g_DrawRand.GetVaried(2.f)*ptxSpinRate;
			y = g_DrawRand.GetVaried(2.f)*ptxSpinRate;
			z = g_DrawRand.GetVaried(2.f)*ptxSpinRate;
			outPoint.m_AngularVelocity.Set(x, y, z);

			// Init life
			outPoint.m_Life = lifeSpan;
		
			// Init size
			const float ptxMaxSize = 0.2f;		// meters
			outPoint.m_SizeX = g_DrawRand.GetFloat() * ptxMaxSize;
			outPoint.m_SizeY = outPoint.m_SizeX;
			
			// Init which tile id to use (we're using the first 6 R* logos)
			outPoint.m_TileId = g_DrawRand.GetRanged(0, 5);

			// Put the particle in the spatial grid.
			m_SpatialGrid.AddParticle(outPoint,frameCount);

			m_FirstInactiveIndex++;
			DebugAssert(m_FirstActiveIndex!=m_FirstInactiveIndex);
			DebugAssert(m_FirstInactiveIndex<=m_FluidParticles.GetCount());
			if (m_FirstInactiveIndex>=m_FluidParticles.GetCount())
			{
				m_FirstInactiveIndex = 0;
			}

			return true;
		}

		return false;
	}
	
	void InitCamera(){
		grcSampleManager::InitSampleCamera(Vector3(0.f, 3.f, 5.f), Vector3(0.f,0.f,0.f));
	}

#if __BANK
	void AddWidgetsClient() {
		const char *types[DRAWMODE_COUNT] = {"Billboard", "Quad", "Sphere"};

		bkBank &bk = bkManager::GetInstance().CreateBank("Particle Sample");
		bk.AddCombo("Render Mode", (int*) &m_DrawMode, DRAWMODE_COUNT, types);
	}
#endif
	
	grcTexture *m_ParticleTex;
	atArray<ptcFluidParticle> m_FluidParticles;

	ptcSpatialGrid m_SpatialGrid;

	float m_ParticleRadius;
	float m_InteractionDistance;
	float m_PressureConstant;
	float m_RestDensity;

	// PURPOSE: particles per second produced, if available
	float m_ProductionRate;

	// PURPOSE: the time each particle is active before being destroyed
	float m_LifeSpan;

	// PURPOSE: unused time interval (fraction of a frame) for particle production on the next frame
	float m_SpareTime;

	// PURPOSE: The index number of the next particle to spawn.
	int m_FirstInactiveIndex;

	// PURPOSE: The index number of the first active particle.
	int m_FirstActiveIndex;

	enum ptcDrawMode { BILLBOARD, FLAT, SPHERE, DRAWMODE_COUNT };
	ptcDrawMode m_DrawMode;
	
};


// main application
int Main()
{
	ptcSampleFluid samplePtc;
	samplePtc.Init();

	samplePtc.UpdateLoop();

	samplePtc.Shutdown();

	return 0;
}
