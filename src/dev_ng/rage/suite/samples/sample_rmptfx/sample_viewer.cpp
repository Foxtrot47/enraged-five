// 
// sample_rmptfx/sample_viewer.cpp
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
//	Hint: Run with the -demo flag on the command line if you just want to make sure something works.

#include "atl/array.h"
#include "atl/atfunctor.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/console.h"
#include "file/asset.h"
//#include "gpuptfx/ptxgpuparticle.h"
#include "grcore/effect.h"
#include "grcore/effect_values.h"
#include "grcore/im.h"
#include "grcore/state.h"
#include "grmodel/shadergroup.h"
#include "math/random.h"
#include "pheffects/wind.h"
#include "pheffects/winddisturbancebox.h"
#include "pheffects/winddisturbancecyclone.h"
#include "rmptfx/ptxbehaviours.h"
#include "rmptfx/ptxkeyframe.h"
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxemitinstiter.h"
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxvolume.h"
#include "rmptfx/ptxfxlist.h"
#include "sample_rmcore/sample_rmcore.h"
#include "system/main.h"
#include "system/param.h"
#include "system/platform.h"
#include "system/timemgr.h"
#include "vectormath/vectortypes.h"

PARAM(file,"[ptxviewer]");
PARAM(demo,"Demo mode");
PARAM(dictionary, "[ptxviewer] will load resourced rule dictionary ");
//PARAM(gpuParticle,"[ptxviewer]");
PARAM(launch,"[ptxviewer]");
PARAM(launchcount,"[ptxviewer]");
PARAM(launchperframe,"[ptxviewer]");
PARAM(capture, "[ptxviewer]");
PARAM(fixedframe, "[ptxviewer]");
PARAM(randseed, "[ptxviewer]");
PARAM(maxeffects, "[ptxviewer]");
PARAM(evoval, "[ptxviewer]");
PARAM(mt,"[multithreaded]");
PARAM(testevos, "");
XPARAM(hdr);

#if __XENON
#include "system/xtl.h"
#include <tracerecording.h>
#pragma comment (lib, "tracerecording.lib")
#endif

#define __RMPTFX_DOWNSAMPLEBUFFERS (0)
#define MAX_WIND_CYCLONES (5)
#define MAX_WIND_BOXES (5)

//--------- Example Game Configuration

class ShowTightBounds : public ptxRenderSetup
{
public:
	virtual	bool PreDraw( const ptxEffectInst*, const ptxEmitterInst*, Vec3V_In vMin_In, Vec3V_In vMax_In, u32) const 
	{
		Vec3V vMin = vMin_In;
		Vec3V vMax = vMax_In;

		grcDrawBox(RCC_VECTOR3(vMin), RCC_VECTOR3(vMax) , Color32( 0.2f, 1.0f, 0.4f) );
		return true;
	}
};


//class ShowTightBounds : public ptxVisibilty
//{
//public:		
//	virtual	void VisibiltyTest( u32 viewportId, const EffectInstance& inst, float& doUpdateAmt, float& doDrawAmt , bool& killIt) const 
//	{
//		grcDrawBox( min, max , Color32( 0.2f, 1.0f, 0.4f) );
//		Vector3 amb = GetAmbient( (min + max ) * 0.5f );
//		SetAmbientGlobalVariable( amb );
//
//		CullSphere.radius += inst.Velocity.Length()
//
//		doUpdateAmt = 1.0f;
//		doDraw = 1.0f;
//		if ( culled )
//		{
//
//			doUpdateAmt = 0.99999f * HashTable[inst + viewportID ];
//			doDraw = 0.0f
//		}
//		if ( distance > VERY_FAR_DISTANCE )
//		{
//			killIt = true;
//		}
//	}
//}
typedef atMap<ConstString, ptxFxList*> FxListMap;
FxListMap g_LoadedFxLists;

////////////////////////////////////////
// Console commands
#if __BANK
void Reset(const char** /*argv*/, int /*argc*/, bkConsole::OutputFunctor /*cout*/)
{
	RMPTFXMGR.Reset();
}

void LoadFxList(const char** argv, int /*argc*/, bkConsole::OutputFunctor cout)
{
	if (g_LoadedFxLists.Access(argv[0]))
	{
		char buf[256];
		formatf(buf, 256, "Already loaded fx list %s (%x)", argv[0], *g_LoadedFxLists.Access(argv[0]));
		cout(buf);
		return;
	}

	ptxFxList* list = RMPTFXMGR.LoadFxList(argv[0], 0);
	if (list)
	{
		char buf[256];
		formatf(buf, 256, "Loaded fx list %s (%x)", argv[0], list);
		cout(buf);
		g_LoadedFxLists.Insert(ConstString(argv[0]), list);
	}
	else
	{
		char buf[256];
		formatf(buf, 256, "Couldn't load fx list %s", argv[0], list);
		cout(buf);
	}
	return;
}

void PrintLoadedFxLists(const char** /*argv*/, int /*argc*/, bkConsole::OutputFunctor cout)
{
	for(FxListMap::Iterator i = g_LoadedFxLists.CreateIterator(); !i.AtEnd(); i.Next())
	{
		ptxFxList* list = (ptxFxList*)(i.GetData());
		char buf[256];
#if 0
		formatf(buf, 256, "%s\t\t%x\t%d\n", i.GetKey().c_str(), list, list->GetUseCount() );
#else
		formatf(buf, 256, "%s\t\t%x\t%d\n", i.GetKey().c_str(), list);
#endif
		cout(buf);
	}
}

#if __DEV
void PrintInstances(const char** /*argv*/, int /*argc*/, bkConsole::OutputFunctor /*cout*/)
{
	RMPTFXMGR.PrintInstanceList();
}

void PrintEmitRules(const char** /*argv*/, int /*argc*/, bkConsole::OutputFunctor cout)
{
	ptxEmitInstIter iter(RMPTFXMGR);
	int i = 0;
	while(iter.MoveNext())
	{
		char buf[256];
		formatf(buf, 256, "%d: %s %d\n", i, iter.GetEffectInst()->GetName(), iter.GetEventNum());
		cout(buf);
		ptxEmitterInst* inst = iter.GetEmitterInst();
		if (inst)
		{
			
			formatf(buf, 256, "  emitter: <%f,%f,%f>\n", inst->m_LastPos.GetXf(), inst->m_LastPos.GetYf(), inst->m_LastPos.GetZf());
			cout(buf);
			i++;
		}
	}
}
#endif

void UnloadFxList(const char** argv, int /*argc*/, bkConsole::OutputFunctor cout)
{
	if (!g_LoadedFxLists.Access(argv[0]))
	{
		char buf[256];
		formatf(buf, 256, "FxList %s isn't loaded", argv[0]);
		cout(buf);
		return;
	}

	RMPTFXMGR.UnloadFxList(g_LoadedFxLists[ConstString(argv[0])], false);
	char buf[256];
	formatf(buf, 256, "Unloaded fx list %s", argv[0]);
	cout(buf);
	g_LoadedFxLists.Delete(argv[0]);
}

void SpawnEffect(const char** argv, int argc, bkConsole::OutputFunctor cout)
{
	ptxEffectInst* inst = RMPTFXMGR.GetEffectInst(argv[0]);
	if (!inst)
	{
		char buf[256];
		formatf(buf, 256, "Couldn't find instance named %s", argv[0]);
		cout(buf);
		return;
	}

	Vec3V pos(V_ZERO);
	if (argc == 4)
	{
		pos.SetXf( (float)atof(argv[1]) );
		pos.SetYf( (float)atof(argv[2]) );
		pos.SetZf( (float)atof(argv[3]) );
	}

	inst->SetPosVector3(pos);
	inst->Start();
}
#endif

static volatile bool m_sDoUpdateFlag = false;
static volatile bool m_sFinishedUpdateFlag = false;
static volatile bool m_sQuit = false;
/***************************************************************/
class gtaSample : public ragesamples::rmcSampleManager {
public:
	Vector4								m_BackgroundColor;
	bool								m_DrawBackground;
	bool								m_DrawGrid;
	bool								m_Draw3dGrid;
	ptxFxList*							m_TestResource;
	atPtr<ptxRenderSetup>				m_exampleRenderSetup;
	bool								m_useRenderSetup;

	float								m_TotalPtxDrawTime;
	float								m_TotalPtxUpdateTime;

// 	ptxGpuParticle						m_gpuParticleSystem;
// 	ptxSimpleEmitter					m_gpuEmitter;
// 	ptxgpuRenderSettings				m_renderSettings;

	const char*							m_EffectName;
	int									m_LaunchAtStartNum;
	int									m_LaunchPerFrameNum;

	float								m_FrameStep;
	u32									m_CaptureFrame;

	float								m_EvoVals[ptxEvolutionList::kMaxNumEvolutions];



#if !__WIN32PC
	grcRenderTarget*					m_SmallColorRenderTarget;
	grcRenderTarget*					m_SmallDepthRenderTarget;
	
	grcCompositeRenderTarget*			m_CompositeRenderTarget;
	grcEffectVar m_TexelSizeId;
	XENON_ONLY(grcEffectVar m_DiffuseTextureId);
	u8 m_OldDepthTextureFormat;
#endif //!_WIN32


	phWindCycloneGroup*	m_pCycloneGroup;
	phWindBoxGroup*		m_pBoxGroup;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	bool CompareResourceExtension(const char* fileExtn, const char* codeExtn)
	{
		while(*fileExtn && *codeExtn)
		{
			if (!((*codeExtn == '#' && *fileExtn == g_sysPlatform) ||
				*codeExtn == *fileExtn))
			{
				return false;
			}
			++fileExtn;
			++codeExtn;
		}
		return *fileExtn == 0 && *codeExtn == 0;
	}


#if __BANK
	void SpawnN(const char** argv, int /*argc*/, bkConsole::OutputFunctor /*cout*/)
	{
		const char* oldEffectName = m_EffectName;
		m_EffectName = argv[0];
		int count = atoi(argv[1]);
		LaunchEffects(count);
		m_EffectName = oldEffectName;
	}
#endif

	void LaunchEffects(int count=1)
	{
		for(int i = 0; i < count; i++)
		{
			Vector3 pos;
			pos.x = g_DrawRand.GetRanged(-10.0f, 10.0f);
			pos.y = g_DrawRand.GetRanged(-10.0f, 10.0f);
			pos.z = g_DrawRand.GetRanged(-10.0f, 10.0f);

			ptxEffectInst* inst = RMPTFXMGR.GetEffectInst(m_EffectName);
			if (inst)
			{
				inst->SetPosVector3(RCC_VEC3V(pos));
				for(int i = 0; i < ptxEvolutionList::kMaxNumEvolutions; i++)
				{
					inst->SetEvolutionTime(i, m_EvoVals[i]);
				}
				inst->Start();
			}
		}
	}

	static void DefaultOnStartFunctor(ptxEffectInst * /*inst*/)
	{
	}

	static void DefaultOnStopFunctor(ptxEffectInst * /*inst*/)
	{
	}

	static void DefaultOnRecycleFunctor(ptxEffectInst * /*inst*/)
	{
	}

	static void DefaultOnCullFunctor(ptxEffectInst * inst)
	{
		//inst->GetDataUpdate()->m_CullState = CullState_NotCulled;
		inst->GetDataDBMT(RMPTFXTHREADER.GetUpdateBufferId())->m_CullState = CullState_NotCulled;
	}

	static void DefaultOnPostUpdateFunctor(ptxEffectInst * /*inst*/)
	{
	}

	static void DefaultOnPreDrawFunctor(ptxEffectInst * /*inst*/)
	{
	}


	static DECLARE_THREAD_FUNC(RMPTFX_MT_UpdateThread)
	{
		if(ptr == 0) return;
		((gtaSample*)ptr)->RMPTFX_MT_UpdateLoop();
	}

	void RMPTFX_MT_UpdateLoop()
	{
		while(!m_sQuit)
		{
			if(m_sDoUpdateFlag)
			{
				//printf("RMPTFX Threaded Update Happening\n");
				m_sFinishedUpdateFlag = false;
				RMPTFXMGR.Update();
				m_sDoUpdateFlag = false;
				m_sFinishedUpdateFlag = true;
			}
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void CreateDownsampleBuffers()
	{
#if !__WIN32PC

		grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
		grcRenderTarget * bigColorRenderTarget = textureFactory.GetBackBuffer(false);
		grcTextureFactory::CreateParams params;

#if __XENON
		params.HasParent = true;
		params.Parent = NULL;
#endif // __XENON
		u32 colorBpp = 32;
#if __PS3
		if (PARAM_hdr.Get())
		{
			params.Format = grctfA16B16G16R16F;
			colorBpp = 64;
		}
#endif // __PS3

		//Create the small render target
		const u32 smallSizeShift = 1;
		u32 smallWidth = bigColorRenderTarget->GetWidth() >> smallSizeShift;
		u32 smallHeight = bigColorRenderTarget->GetHeight() >> smallSizeShift;
		m_SmallColorRenderTarget = textureFactory.CreateRenderTarget("Particle downsample buffer (color)", grcrtPermanent, smallWidth, smallHeight, colorBpp, &params);
#if __XENON
		params.Parent = m_SmallColorRenderTarget;
		params.IsResolvable = false;
#endif // __XENON

		m_SmallDepthRenderTarget = textureFactory.CreateRenderTarget("Particle downsample buffer (depth)", grcrtDepthBuffer, smallWidth, smallHeight, 32, &params);

		grcCompositeRenderTarget::CreateParams crtCreateParams;

		// Customize the shaders/vars used by grcCompositeRenderTarget 
		grcEffect& defaultEffect = GRCDEVICE.GetDefaultEffect();
		m_TexelSizeId = defaultEffect.LookupVar("TexelSize");
		XENON_ONLY(m_DiffuseTextureId = defaultEffect.LookupVar("DiffuseTex"));
		grcEffectTechnique compositeTechnique = defaultEffect.LookupTechnique("CopyTransparentEdgeBlur");
		crtCreateParams.compositeTechnique = &compositeTechnique;
		grcEffectTechnique blitTechnique = defaultEffect.LookupTechnique("BlitTransparentEdgeBlur");
		crtCreateParams.blitTechnique = &blitTechnique;
		grcEffectVar blitSrcMapId = defaultEffect.LookupVar("TransparentSrcMap");
		crtCreateParams.blitSrcMapVar = &blitSrcMapId;

		crtCreateParams.srcColor = m_SmallColorRenderTarget; // Doesn't matter, this gets overidden
		crtCreateParams.srcDepth = m_SmallDepthRenderTarget; // Doesn't matter, this gets overidden
		crtCreateParams.dstColor = m_SmallColorRenderTarget;
		crtCreateParams.dstDepth = m_SmallDepthRenderTarget;

		m_CompositeRenderTarget = rage_new grcCompositeRenderTarget(crtCreateParams);

#endif // !__WIN32PC
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void InitClient() 
	{
		m_sDoUpdateFlag = false;
		m_sFinishedUpdateFlag = false;
		m_sQuit = false;
		if(PARAM_mt.Get())
			sysIpcCreateThread( gtaSample::RMPTFX_MT_UpdateThread, this, 32*1024, PRIO_NORMAL, "[RAGE] RMPTFXUpdateThread", 1);

		EnableLogoDraw(true);
		EnableBackgroundDraw(true);
		m_BackgroundColor.Set(0.0f,0.0f,0.197f,1.0f);
		//m_BackgroundColor.Set(GetBackGroundColor().GetRedf(),GetBackGroundColor().GetGreenf(),GetBackGroundColor().GetBluef(),GetBackGroundColor().GetAlphaf());
		m_DrawBackground=true;
		m_DrawGrid=true;
		m_Draw3dGrid=false;

		m_TotalPtxUpdateTime = 0.0f;
		m_TotalPtxDrawTime = 0.0f;

#if __PFDRAW
		GetRageProfileDraw().Init(2000000, true, grcBatcher::BUF_FULL_IGNORE);
		GetRageProfileDraw().SetEnabled(true);
#endif

		int maxeffects = 1000;
		PARAM_maxeffects.Get(maxeffects);

		// Init wind.
		phWind::InitClass();

		// Init some wind settings.
		WIND.SetFocalPoint( Vec3V(V_ZERO) ); // Centers the grid at the origin.
		WIND.SetWaterLevel( 0.0f ); // Get rid of the water level, effectively.
		WIND.SetAngle(WIND_TYPE_WATER, PI);
		WIND.SetSpeed(WIND_TYPE_WATER, 0.8f );
		WIND.SetAngle(WIND_TYPE_AIR, 2.0f*PI );
		WIND.SetSpeed(WIND_TYPE_AIR, 0.8f );

		// Create an auto-managed cyclone disturbance group, specifying some auto-manage parameters.
		phCycloneAutoData cycAutoData;
		cycAutoData.type = WIND_DIST_AIR;
		cycAutoData.createChance = 0.3f;
		cycAutoData.rangeMin = 5.0f;
		cycAutoData.rangeMax = 15.0f;
		cycAutoData.strengthMin = 0.5f;
		cycAutoData.strengthMax = 3.0f;
		cycAutoData.strengthDelta = 0.3f;
		cycAutoData.shrinkChance = 0.1f;
		cycAutoData.forceMult = 10.0f;
		m_pCycloneGroup = rage_new phWindCycloneGroup();
		m_pCycloneGroup->Init(MAX_WIND_CYCLONES);
		m_pCycloneGroup->SetAutoData(cycAutoData);
		m_pCycloneGroup->SetAutomated(true);

		// Create an auto-managed box disturbance group, specifying some auto-manage parameters.
#if 0
		// phBoxAutoData was removed
		phBoxAutoData boxAutoData;
		boxAutoData.type = WIND_DIST_AIR
		boxAutoData.createChance = 0.3f;
		boxAutoData.sizeMin = 3.0f;
		boxAutoData.sizeMax = 7.0f;
		boxAutoData.strengthMin = 5.0f;
		boxAutoData.strengthMax = 10.0f;
#endif
		m_pBoxGroup	= rage_new phWindBoxGroup();
		m_pBoxGroup->Init(MAX_WIND_BOXES);
		//m_pBoxGroup->SetAutoData(boxAutoData);
		m_pBoxGroup->SetAutomated(true);

		// Add the disturbance groups.
		WIND.AddDisturbanceGroup(m_pCycloneGroup);
		WIND.AddDisturbanceGroup(m_pBoxGroup);

		// Start up wind.
		WIND.InitSession();


		// Init particle system.
		rmPtfxManager::InitClass();
		RMPTFXMGR.CreatePointPool(10000);
		RMPTFXMGR.CreateEffectInstPool(maxeffects);
		RMPTFXMGR.CreateDrawList(); 
#if !__WIN32PC && __RMPTFX_DOWNSAMPLEBUFFERS
		RMPTFXMGR.CreateDrawList("LowRez Test"); 
#endif
		RMPTFXMGR.SetDefaultFxInstOnStartFunctor(MakeFunctor(DefaultOnStartFunctor));
		RMPTFXMGR.SetDefaultFxInstOnStopFunctor(MakeFunctor(DefaultOnStopFunctor));
		RMPTFXMGR.SetDefaultFxInstOnRecycleFunctor(MakeFunctor(DefaultOnRecycleFunctor));
		RMPTFXMGR.SetDefaultFxInstOnCullFunctor(MakeFunctor(DefaultOnCullFunctor));
		RMPTFXMGR.SetDefaultFxInstPostUpdateFunctor(MakeFunctor(DefaultOnPostUpdateFunctor));
		RMPTFXMGR.SetDefaultFxInstPreDrawFunctor(MakeFunctor(DefaultOnPreDrawFunctor));
		RMPTFXMGR.AddGameSpecificDataEffectRuleFlagString("GameSpecificFlag_01");
		RMPTFXMGR.AddGameSpecificDataEffectRuleFlagString("GameSpecificFlag_02");
#if __PPU
		// PS3 PPU callback to help load wind data to SPU so we can query main memory wind field from SPU.
	RMPTFXMGR.SetAddAirSpeedDataForSpuUpdate(MakeFunctor(&rage::PtxAddAirSpeedDataForSpu));
#endif

		
#if !__WIN32PC
		CreateDownsampleBuffers();
#endif //!__WIN32PC
		
		
		

		const char* rawFileName = NULL;
		char fileName[128];
		fileName[0]=0;

		
		for(int i = 0; i < ptxEvolutionList::kMaxNumEvolutions; i++)
		{
			m_EvoVals[i] = 0.0f;
		}
		PARAM_evoval.GetArray(m_EvoVals, 5);

		PARAM_file.Get(rawFileName);
		if (rawFileName)
		{
			const char* extn = 	ASSET.FindExtensionInPath(rawFileName);
			ASSET.RemoveExtensionFromPath(fileName,128,rawFileName);

			if (!strcmp(extn, ".fxlist"))
			{
				RMPTFXMGR.LoadFxList(rawFileName, 0);
			}
			else if (CompareResourceExtension(extn, ".#pfl"))
			{
				RMPTFXMGR.LoadFxList(rawFileName, 0);
			}
			else if (!strcmp(extn, ".effectrule"))
			{
				RMPTFXMGR.LoadEffectRule(fileName);
			}
		}

		if(PARAM_demo.Get())
		{
			RMPTFXMGR.LoadEffectRule("rmptfx_demo");
			ptxEffectInst * inst = RMPTFXMGR.GetEffectInst("rmptfx_demo");
			if(inst)
			{
				inst->SetPosVector3( Vec3V(V_ZERO) );
				inst->Start();
			}
		}

		if (PARAM_testevos.Get())
		{
			RMPTFXMGR.LoadEffectRule("SpriteColumn_3Evo");
			RMPTFXMGR.LoadEffectRule("SpriteColumn_BlueEvo");
			RMPTFXMGR.LoadEffectRule("SpriteColumn_RedEvo");
			RMPTFXMGR.LoadEffectRule("SpriteColumn_GreenEvo");

			Vec3V pos(0.0f, 0.0f, 0.0f);
			Vec3V xincrement(4.0f, 0.0f, 0.0f);

			for(int i = 0; i < 5; i++)
			{
				ptxEffectInst* inst = RMPTFXMGR.GetEffectInst("SpriteColumn_BlueEvo");
				inst->SetPosVector3(pos);
				inst->Start();
				pos += xincrement;
			}

			for(int i = 0; i < 5; i++)
			{
				ptxEffectInst* inst = RMPTFXMGR.GetEffectInst("SpriteColumn_3Evo");
				inst->SetPosVector3(pos);
				inst->Start();
				pos += xincrement;
			}

			pos = Vec3V(0.0f, 0.0f, 4.0f);

			for(int i = 0; i < 5; i++)
			{
				ptxEffectInst* inst = RMPTFXMGR.GetEffectInst("SpriteColumn_BlueEvo");
				inst->SetPosVector3(pos);
				inst->SetEvolutionTime("Tint", 1.0f);
				inst->Start();
				pos += xincrement;
			}

			for(int i = 0; i < 5; i++)
			{
				ptxEffectInst* inst = RMPTFXMGR.GetEffectInst("SpriteColumn_3Evo");
				inst->SetPosVector3(pos);
				inst->SetEvolutionTime("Blueness", 1.0f);
				inst->Start();
				pos += xincrement;
			}

			pos = Vec3V(0.0f, 0.0f, 8.0f);

			for(int i = 0; i < 5; i++)
			{
				ptxEffectInst* inst = RMPTFXMGR.GetEffectInst("SpriteColumn_RedEvo");
				inst->SetPosVector3(pos);
				inst->SetEvolutionTime("Tint", 1.0f);
				inst->Start();
				pos += xincrement;
			}

			for(int i = 0; i < 5; i++)
			{
				ptxEffectInst* inst = RMPTFXMGR.GetEffectInst("SpriteColumn_3Evo");
				inst->SetPosVector3(pos);
				inst->SetEvolutionTime("Redness", 1.0f);
				inst->Start();
				pos += xincrement;
			}


			pos = Vec3V(0.0f, 0.0f, 12.0f);

			for(int i = 0; i < 5; i++)
			{
				ptxEffectInst* inst = RMPTFXMGR.GetEffectInst("SpriteColumn_GreenEvo");
				inst->SetPosVector3(pos);
				inst->SetEvolutionTime("Tint", 1.0f);
				inst->Start();
				pos += xincrement;
			}

			for(int i = 0; i < 5; i++)
			{
				ptxEffectInst* inst = RMPTFXMGR.GetEffectInst("SpriteColumn_3Evo");
				inst->SetPosVector3(pos);
				inst->SetEvolutionTime("Greenness", 1.0f);
				inst->Start();
				pos += xincrement;
			}

		}
		
		// Example of configuring the setup for rendering particle rules
		// this is done at the lowest level ( ptxRule ) and is very useful
		// for setting up game specific lighting.
		m_useRenderSetup = false;
		m_exampleRenderSetup = rage_new ShowTightBounds();

// 		WIN32PC_ONLY(PARAM_gpuParticle.Set(NULL));
// 		if ( PARAM_gpuParticle.Get() )
// 		{
// 			// NOTE since atPtr's are stored for the shaders it allows for pointers to be passed in
// 			// the reason for this is to allow game teams to override the renderShader and Update shader
// 			// with there own effect specific suff.
// 
// 			const int NumParticles = 32*128;
// 			m_gpuParticleSystem.Init(NumParticles,
// 										rage_new ptxGpuUpdateShader( "gpuptfx_update" ),
// 										rage_new ptxGpuRenderShader( "gpuptfx_simplerender" ),
// 										false, // allocate at start of VRAM (360 only)
// 										true); // use hit plane (PS3 only)
// 
// 			m_gpuEmitter =  ptxSimpleEmitter::GetRainStyleEmitter();
// 
// 			m_renderSettings = ptxgpuRenderSettings::GetRainStyleSettings();
// 			m_renderSettings.diffuseTex = grcTextureFactory::GetInstance().Create("tune\\rmptfx\\textures\\rain#1.dds");
// 			m_renderSettings.diffuseTex2 = grcTextureFactory::GetInstance().Create("tune\\rmptfx\\textures\\rainanim#1.dds");
// 
// 			m_gpuParticleSystem.SetGravity( Vector3( 0.0f, -30.8f, 0.0f ));  // could put it in the
// 
// 			if ( GetZUp() )
// 			{
// 				m_gpuParticleSystem.GetUpdateShader()->SetTechnique( "drawPlaneGusts" );	
// 			}
// 			else
// 			{
// 				m_gpuParticleSystem.GetUpdateShader()->SetTechnique( "drawPlaneGustsYUp" );
// 			}
// 			m_gpuParticleSystem.GetRenderShader()->SetTechnique( "draw_Rain" );
// 			
// 		}

		m_EffectName = NULL;
		PARAM_launch.Get(m_EffectName);

		m_LaunchAtStartNum = 0;
		PARAM_launchcount.Get(m_LaunchAtStartNum);

		m_LaunchPerFrameNum = 0;
		PARAM_launchperframe.Get(m_LaunchPerFrameNum);

		if (m_EffectName)
		{
			int randseed = 12345;
			PARAM_randseed.Get(randseed);
			g_DrawRand.Reset(randseed);

			LaunchEffects(m_LaunchAtStartNum);
		}

		m_FrameStep = 0.0f;
		PARAM_fixedframe.Get(m_FrameStep);

		m_CaptureFrame = 0;
		PARAM_capture.Get(m_CaptureFrame);
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void InitShaderSystems()
	{
		rmcSampleManager::InitShaderSystems();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void ShutdownClient() 
	{
		m_sQuit=true;
#if !__WIN32PC
		LastSafeRelease( m_SmallColorRenderTarget );
		LastSafeRelease( m_SmallDepthRenderTarget );
#endif //!__WIN32PC

#if __PFDRAW
		GetRageProfileDraw().Shutdown();
#endif
		rmPtfxManager::ShutdownClass();
// 		if ( PARAM_gpuParticle.Get() )
// 		{
// 			m_gpuParticleSystem.Shutdown();
// 			m_renderSettings.Shutdown();
// 		}

		phWind::ShutdownClass();
		delete m_pCycloneGroup;
		delete m_pBoxGroup;
	}

	sysTimer m_Timer;

	void BeginTrace(const char* NOTFINAL_ONLY(XENON_ONLY(filename)))
	{
		if (PARAM_capture.Get() && TIME.GetFrameCount() == m_CaptureFrame)
		{
#if __XENON && !__FINAL
			XTraceStartRecording(filename);
#endif
		}
		m_Timer.Reset();
	}

	bool EndTrace(float* timeAccumulator)
	{
		if (PARAM_capture.Get() && TIME.GetFrameCount() == m_CaptureFrame)
		{
#if __XENON && !__FINAL
			XTraceStopRecording();
#endif
			return true;
		}
		else if (timeAccumulator)
		{
			*timeAccumulator += m_Timer.GetUsTime();
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void UpdateClient() 
	{
		static int frameCount = 0;
		frameCount++;

		int capture = -1;
		PARAM_capture.Get(capture);

		RMPTFXMGR.BeginFrame();

		SetClearColor(Color32(m_BackgroundColor.x,m_BackgroundColor.y,m_BackgroundColor.z,m_BackgroundColor.w));
		rmcSampleManager::UpdateClient();


		if (m_EffectName)
		{
			LaunchEffects(m_LaunchPerFrameNum);
		}

		float frameStep = m_FrameStep > 0.0f ? m_FrameStep : TIME.GetSeconds();

		BeginTrace("devkit:\\rmptfx_update.pix2");

//		WIND.Update(TIME.GetSeconds(), (u32)(TIME.GetUnpausedElapsedTime()*1000.0f), TIME.GetFrameCount());

		if(!PARAM_mt.Get())
			RMPTFXMGR.Update(frameStep);

		EndTrace(&m_TotalPtxUpdateTime);
	}
	void PreDrawClient()
	{
// 		if ( PARAM_gpuParticle.Get() )
// 		{
// 			static sysTimer	currentTime;
// 			static float lastTime = 0.0f;
// 
// 			m_gpuParticleSystem.SetEmitter( m_gpuEmitter );
// 			// At the moment we just use a hardcoded ground height of 0.0f
// 			//m_gpuParticleSystem.GetUpdateShader()->SetHeightMap(...);
// 			m_gpuParticleSystem.Update( grcViewport::GetCurrent() , currentTime.GetTime() - lastTime );
// 			//m_gpuParticleSystem.GetUpdateShader()->SetHeightMap(NULL);
// 			lastTime = currentTime.GetTime();
// 		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void DrawClient() 
	{
		
		grcViewport::GetCurrent()->SetWorldIdentity();

//		if ( PARAM_gpuParticle.Get() )
//		{
//			//m_gpuParticleSystem.Render( grcViewport::GetCurrent(), m_renderSettings , m_exampleRenderSetup);		// migTODO: Fix me!
//		}

		

		if(PARAM_mt.Get())
		{
			m_sFinishedUpdateFlag = false;
			m_sDoUpdateFlag = true;
		}
		BeginTrace("devkit:\\rmptfx_draw.pix2");

		grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
		grcRenderTarget* bigDepthRenderTarget = textureFactory.GetBackBufferDepth(__XENON);
		grcRenderTarget* bigColorRenderTarget = textureFactory.GetBackBuffer(__XENON);
		RMPTFXMGR.SetFrameBuffer(bigColorRenderTarget);
		RMPTFXMGR.SetDepthBuffer(bigDepthRenderTarget);
		RMPTFXMGR.Draw(0);
		
#if !__WIN32PC
		//Set lowrez render target
#if __RMPTFX_DOWNSAMPLEBUFFERS
		SetLowRezBuffer();
#endif
		RMPTFXMGR.Draw(1); // Downsampled
#if __RMPTFX_DOWNSAMPLEBUFFERS
		CompositeLowrezBuffer();
#endif 
#endif //!__WIN32PC
		
		//Restore

		if(PARAM_mt.Get())
		{
			while(!m_sFinishedUpdateFlag)
			{
				sysIpcSleep(1);
			}
		}

		if (EndTrace(&m_TotalPtxDrawTime))
		{
			Printf("Update time: %fus\n", m_TotalPtxUpdateTime);
			Printf("Draw time: %fus\n", m_TotalPtxDrawTime);
		}

	
#if __PFDRAW
		grcBindTexture(NULL);
		grcState::Default();
		grcLightState::SetEnabled(true);

		const Matrix34 & cameraMtx = GetCamMgr().GetWorldMtx();
		GetRageProfileDraw().SendDrawCameraForServer(cameraMtx);
		GetRageProfileDraw().Render(true);

		// View wind.
		grcLightState::SetEnabled(false);
		WIND.DebugDraw();
#endif
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if __RMPTFX_DOWNSAMPLEBUFFERS
	void SetLowRezBuffer()
	{
#if !__WIN32PC

		//Resove the big buffer and set it as the src for the compisite target
		grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
		grcRenderTarget* bigColorRenderTarget = textureFactory.GetBackBuffer(__XENON);
		grcRenderTarget* bigDepthRenderTarget = textureFactory.GetBackBufferDepth(__XENON);

		m_CompositeRenderTarget->SetSrcColorRenderTarget(bigColorRenderTarget);
		m_CompositeRenderTarget->SetSrcDepthRenderTarget(bigDepthRenderTarget);


		grcRenderTarget* smallColorRenderTarget = m_CompositeRenderTarget->GetDstColorRenderTarget();
		grcRenderTarget* smallDepthRenderTarget = m_CompositeRenderTarget->GetDstDepthRenderTarget();

		//Tell the particle system which textures to use for frame and depth (for soft/heathaze type effects)
		RMPTFXMGR.SetFrameBuffer(smallColorRenderTarget);
		RMPTFXMGR.SetDepthBuffer(smallDepthRenderTarget);

		grcEffect& defaultEffect = GRCDEVICE.GetDefaultEffect();
		float floatWidth = static_cast<float>(smallColorRenderTarget->GetWidth());
		float floatHeight = static_cast<float>(smallColorRenderTarget->GetHeight());
		Vector4 texelSize(1.0f / floatWidth, 1.0f / floatHeight, floatWidth, floatHeight);
		defaultEffect.SetVar(m_TexelSizeId, texelSize);
#if __XENON
		defaultEffect.SetVar(m_DiffuseTextureId, bigColorRenderTarget);
#endif // __XENON

#if __PPU
		m_OldDepthTextureFormat = GRCDEVICE.PatchShadowToDepthBuffer(smallDepthRenderTarget, false);
#endif // __PPU
		rmPtfxShaderTemplateList::SetGlobalShaderVars();

		//Set the composite as the render target
		m_CompositeRenderTarget->BeginRenderingDst(true);

#endif //!__WIN32PC
	}
#endif // __RMPTFX_DOWNSAMPLEBUFFERS
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if __RMPTFX_DOWNSAMPLEBUFFERS
	void CompositeLowrezBuffer()
	{
#if !__WIN32PC

		//Reset the frame as the render target
		m_CompositeRenderTarget->EndRenderingDst();
#if __PPU
		grcRenderTarget* smallDepthRenderTarget = m_CompositeRenderTarget->GetDstDepthRenderTarget();
		GRCDEVICE.PatchDepthToShadowBuffer(smallDepthRenderTarget , m_OldDepthTextureFormat, false);
#endif // __PPU

		// Do the final composite over our full screen render target
		m_CompositeRenderTarget->CompositeDstToSrc();


#if __XENON
		grcEffect& defaultEffect = GRCDEVICE.GetDefaultEffect();
		defaultEffect.SetVar(m_DiffuseTextureId, (grcTexture*)NULL);
#endif // __XENON

#endif //!__WIN32PC
	}
#endif // __RMPTFX_DOWNSAMPLEBUFFERS
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void InitCamera()
	{
		rmcSampleManager::InitCamera();
		rmcSampleManager::InitSampleCamera(Vector3(0.f, 4.f,-15.f), Vector3(0.f,0.f,0.f));
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if __BANK
	void OnDrawBackgroundChanged()
	{
		EnableBackgroundDraw(m_DrawBackground);
	}
	void OnDrawGridChanged()
	{
		EnableGridDraw(m_DrawGrid);
	}
	void OnDraw3DGridChanged()
	{
		EnableGrid3DDraw(m_Draw3dGrid);
	}
	void OnRenderSetupChanged()
	{
		RMPTFXMGR.SetRenderSetup( m_useRenderSetup ?  m_exampleRenderSetup : 0 );
	}

	template<void(*_Fn)(const char**, int, bkConsole::OutputFunctor)>
	void AddCommand(const char* name)
	{
		bkConsole::CommandFunctor fc;
		fc.Reset<_Fn>();
		bkConsole::AddCommand(name, fc);
	}

	void AddWidgetsClient() 
	{
		AddCommand<&SpawnEffect>("spawn");
		AddCommand<&LoadFxList>("loadfx");
		AddCommand<&UnloadFxList>("unloadfx");
		AddCommand<&PrintLoadedFxLists>("printfx");
		AddCommand<&Reset>("reset");
#if __DEV
		AddCommand<&PrintEmitRules>("printemitters");
		AddCommand<&PrintInstances>("printinstances");
#endif
		bkConsole::CommandFunctor spawnN;
		spawnN.Reset<gtaSample, &gtaSample::SpawnN>(this);
		bkConsole::AddCommand("spawnn", spawnN);

		bkBank* bank =&BANKMGR.CreateBank("GTA",false);
		//bank->AddToggle("Multithreading",&(RMPTFXTHREADER.m_PtxMultithreaded));
		//background seems to not work
		//bank->AddToggle("DrawBackground", &m_DrawBackground, datCallback(MFA(gtaSample::OnDrawBackgroundChanged),this));
		bank->AddToggle("DrawGrid", &m_DrawGrid, datCallback(MFA(gtaSample::OnDrawGridChanged),this));
		bank->AddToggle("Draw3dGrid", &m_Draw3dGrid, datCallback(MFA(gtaSample::OnDraw3DGridChanged),this));
		bank->AddColor("Background Color",&m_BackgroundColor);
		bank->AddToggle("Use Render Setup", &m_useRenderSetup , datCallback(MFA(gtaSample::OnRenderSetupChanged),this));

		bank->PushGroup( "Wind" );
		WIND.AddWidgets( *bank );
		bank->PopGroup();

// 		if ( PARAM_gpuParticle.Get() )
// 		{
// 			m_gpuParticleSystem.AddWidgets(*bank );
// 			bank->PushGroup( "Gpu Emitter" );
// 			m_gpuEmitter.AddWidgets( *bank, true  );
// 			bank->PopGroup();
// 
// 			bank->PushGroup( "Render Settings" );
// 			m_renderSettings.AddWidgets( *bank, true );
// 			m_gpuParticleSystem.GetRenderShader()->AddWidgets( *bank  );
// 
// 			bank->PopGroup();
// 	
// 		}

		RMPTFXMGR.AddWidgets();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// main application
int Main()
{
	gtaSample sample;
	//sample.SetZUp(true);
	//g_UnitUp = ZAXIS;

	sample.Init();
#if __XENON
	GRCDEVICE.SetSize(1280,720);
#else
	GRCDEVICE.SetSize(1024,760);
#endif

#if __WIN32PC
	GRCDEVICE.SetBlockOnLostFocus(false);
#endif

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}

