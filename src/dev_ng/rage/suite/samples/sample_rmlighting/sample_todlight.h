// 
// sample_rmlighting/sample_todlight.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//
// PURPOSE:
//		This class defines a time of day directional "sun" shadowcasting light -controlling orbit position and keyframed colors

#ifndef SAMPLE_RMLIGHTING_SAMPLE_TODLIGHT_H
#define SAMPLE_RMLIGHTING_SAMPLE_TODLIGHT_H

#include "vector/vector4.h"
#include "vector/matrix34.h"
#include "bank/Bank.h"
#include "data/base.h"
#include "grcore/effect.h"

namespace rage {

class grcTexture;
class rmlLightSource;
class fiAsciiTokenizer;
class grcRenderTarget;

/***** rmlSampleLightKeyFrame **********************************/
/***************************************************************/
class rmlSampleLightKeyFrame
{
public:
	rmlSampleLightKeyFrame();

	Vector4		m_KeyColor;
	Vector4		m_KeyAmbLight;
	Vector4		m_KeyShadow;
	float		m_KeyOrbitX;
	float		m_KeyBlur;
	float		m_KeyZOffset;
	
    void Load(fiAsciiTokenizer*token);

#if __BANK
	void AddWidgets(bkBank&bank,const char *name);
	void Save(fiAsciiTokenizer*token);
#endif
};

/***** rmcSampleTODLight ***************************************/
/***************************************************************/
class rmcSampleTODLight : public datBase
{
public:
		rmcSampleTODLight();
		~rmcSampleTODLight();

		void Update();
		void Init(const char*name = "TODSun");
		void SetFromTime(float tod);
		void Draw();
		void Load();
		void SetDefaults();

		bool		m_Enabled;
		Vector4		m_LightColor;
		Vector4		m_ShadowColor;
		Vector4		m_AmbientColor;
		Vector2		m_OrbitRotation;
		float		m_OrbitVelocity;
		float		m_Distance;
		float		m_ShadowBlur;
		float		m_ShadowZOffset;
		Matrix34	m_Matrix;
		rmlLightSource* m_LightInfo;
		char		m_Name[256];
		
		//Time of day Specific Vars
		float		m_TimeOfDay;
		float		m_DayOrbitSpeed;
		float		m_DayOrbitSpeed2;
		float		m_NightOrbitSpeed;
		float		m_RateChangeTime;
		

		//Keyframe Vars
		rmlSampleLightKeyFrame	m_Keyframes[5];
		bool					m_KeyframeEnable;

		//Vars to visulize the light
		bool		m_Draw;
		float		m_Radius;
		float		m_Alpha;
		float		m_GlowAlpha;
		grcTexture* m_Texture;

		//Shader var ids
		grcEffectGlobalVar m_DepthTextureID;
		grcEffectGlobalVar m_ShadowColorID;
		grcEffectGlobalVar m_LightMatrixID;
		grcEffectGlobalVar m_PixelBlurID;
		grcEffectGlobalVar m_ZOffsetID;
		grcRenderTarget*	m_LightDepthMap;




#if __BANK
		void AddWidgets(bkBank&bank,const char * name);
		void Save();
#endif
};
}	// namespace rage
#endif //SAMPLE_RMLIGHTING_SAMPLE_TODLIGHT_H
