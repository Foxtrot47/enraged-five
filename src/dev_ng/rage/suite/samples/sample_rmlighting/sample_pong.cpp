// 
// sample_rmlighting/sample_pong.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// PURPOSE:
//		This sample demonstrates how to set up the lighting system to use shadowmaps in Pong

#include "sample_rmcore/sample_rmcore.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "crskeleton/skeletondata.h"
#include "crskeleton/skeleton.h"
#include "file/token.h"
#include "grcore/im.h"
#include "grmodel/setup.h"
#include "grmodel/shadergroup.h"
#include "rmcore/drawable.h"
#include "rmlighting/lightingmgr.h"
#include "rmlighting/lightproxy.h"
#include "rmlighting/lightsource.h"
#include "rmlighting/shadowmap.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/vector4.h"

#include "sample_todlight.h"

using namespace rage;

int perryint=0;
PARAM(file,"[sample_shadowmap]");


class rmcSamplePong;


//Must overide the rmlLightProxy to further communication between lighting system and game

/***** rmcSampleLightProxy *************************************/
/***************************************************************/
class rmcSampleLightProxy : public rmlLightProxy
{	
public:
	rmcSampleLightProxy();
	int BuildVisibleListForViewport( const grcViewport *viewport, u32 flags, const rmlLightSource *source );
	virtual void DrawVisibleItemsForViewport( const grcViewport * /*viewport*/, u32 /*flags*/ ){ Warningf("Fix Me Chris"); }
	rmcSamplePong * m_GamePointer;
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/***** rmcDrawableObject **************************************/
/**************************************************************/
class rmcDrawableObject
{
public:
	rmcDrawableObject()
	{
		m_Drawable = 0;
		m_Skeleton = 0;
		m_Matrix.Identity();
		m_IsVisible=true;
		m_Rotation.Zero();
	};
	~rmcDrawableObject()
	{
		delete m_Drawable;
		delete m_Skeleton;
		m_Drawable=0;
		m_Skeleton=0;
	}
	rmcDrawable*  m_Drawable;
	crSkeleton*   m_Skeleton;
	Matrix34      m_Matrix;
	bool		  m_IsVisible;
	Vector3		  m_Rotation;
};

/***** rmcSamplePong **************************************/
/***************************************************************/
class rmcSamplePong : public ragesamples::rmcSampleManager
{
	friend class rmcSampleLightProxy;
public:

	rmcSampleTODLight				m_TODSun;

	rmlLightingMgr					m_LightingMgr;
	rmcSampleLightProxy				m_LightProxy;
	atArray<rmcDrawableObject>		m_Objects;
	float							m_NearClip;
	float							m_FarClip;
	float							m_FovX;
	float							m_FovY;
	Matrix34						m_CameraMatrix;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	rmcSamplePong() : rmcSampleManager()
	{
		SetMinFiles(1);
		SetMaxFiles(5);
		SetDefaultFileName(0, "slice03");
		m_Objects.Reserve(5);
		GetFileList();
		m_NearClip = 0.00f;
		m_FarClip  = 100.0f;
		m_FovX = 60.0f;
		m_FovY = 30.0f;
		m_CameraMatrix.Identity();
		
		TurnOffTimeOfDay();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
protected:
	void InitShaderSystems()
	{
		// Have to init lighting manager here, so that it can init any shader technique groups and/or
		//	global variables it may need
		// Hook game into proxy
		m_LightProxy.m_GamePointer = this;
		m_LightProxy.Init(4,5);
		// Hook proxy into lighting manager
		m_LightingMgr.Init(&m_LightProxy);

		m_LightingMgr.InitShaderSystem();
		rmcSampleManager::InitShaderSystems();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void InitCamera()
	{
		m_CamMgr.Init(Vector3(0,5.0f,10.0f),Vector3(0,0,0));
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void InitClient()
	{
		ragesamples::rmcSampleManager::InitClient();
		EnableLogoDraw(false);
		EnableBackgroundDraw(false);
		SetClearColor(Color32(0,0,0));
		m_DrawLights=false;

		m_TODSun.Init("TODSun");

		//Load up all the models
		for(int i=0;i<GetNumFileNames();i++)
		{
			ASSET.PushFolder(GetFileName(i));

			rmcDrawable *drawable = rage_new rmcDrawable();
			if(!drawable->Load(GetFileName(i)))
			{
				delete drawable;			
				continue;
			}

			//Load attached skeleton (if it has one)
			crSkeleton * skeleton=0;
			crSkeletonData *skeldata;
			skeldata = drawable->GetSkeletonData();
			if (skeldata)
			{
				skeleton = rage_new crSkeleton();
				skeleton->Init(*skeldata,NULL);
			}

			rmcDrawableObject & newObj = m_Objects.Append();
			newObj.m_Drawable = drawable;
			newObj.m_Skeleton = skeleton;
			newObj.m_Matrix.Identity();

			ASSET.PopFolder();
		}

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void ShutdownClient()
	{
		rmcSampleManager::ShutdownClient();
		m_LightingMgr.Shutdown();
		m_Objects.Reset();
	}
#if __BANK
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void AddWidgetsPasses()
	{
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void AddWidgetsLights()
	{
	}
#endif
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void InitLights()
	{
		ragesamples::rmcSampleManager::InitLights();
		//Reserve one lightsource for the TODSun
		m_TODSun.m_LightInfo = m_LightingMgr.CreateLightSource(true);
		if(m_TODSun.m_LightInfo)
			m_TODSun.m_LightInfo->SetDirectionalType();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void UpdateLightSource(rmcSampleTODLight * light)
	{
		rmlLightSource * info = light->m_LightInfo;
		Vector3 color(light->m_LightColor.x,light->m_LightColor.y,light->m_LightColor.z);
		color.Scale(light->m_LightColor.w);
		info->SetLightColor(color);
		info->SetPosition(light->m_Matrix.d);
		info->SetDirection(-light->m_Matrix.c);
		info->SetShadowColor(light->m_ShadowColor);
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void UpdateLights()
	{
		m_LightingMgr.Update();


		//Support toggle lights on/off
		if(m_TODSun.m_LightInfo)
		{
			if(m_TODSun.m_Enabled)
				m_LightingMgr.ActivateLightSource(m_TODSun.m_LightInfo);
			else
			{
				m_LightingMgr.DeactivateLightSource(m_TODSun.m_LightInfo);
			}
		}
		m_TODSun.Update();
		//Tell the lighting manager where the active lights are / color changes etc...
		UpdateLightSource(&m_TODSun);

		m_LightingMgr.SetAmbientColor(m_TODSun.m_AmbientColor);
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void UpdateClient()
	{
		//repose skeletons
		for(int i=0;i<m_Objects.GetCount();i++)
		{
			m_Objects[i].m_Matrix.RotateX(m_Objects[i].m_Rotation.x);
			m_Objects[i].m_Matrix.RotateY(m_Objects[i].m_Rotation.y);
			m_Objects[i].m_Matrix.RotateZ(m_Objects[i].m_Rotation.z);
			if(m_Objects[i].m_Skeleton)
			{
				m_Objects[i].m_Skeleton->SetParentMtx(&m_Objects[i].m_Matrix);
				m_Objects[i].m_Skeleton->Update();
			}
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void DrawClient()
	{
		m_CameraMatrix.Set(grcViewport::GetCurrent()->GetCameraMtx());

		m_LightingMgr.DrawScene();

		m_TODSun.Draw();

	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	void TurnOnTimeOfDay()
	{
		m_TODSun.m_Enabled = true;
		m_TODSun.m_KeyframeEnable=true;
		m_TODSun.m_DayOrbitSpeed  =  0.02f;
		m_TODSun.m_NightOrbitSpeed = 0.4f;
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void TurnOffTimeOfDay()
	{
		m_TODSun.m_KeyframeEnable=false;
		m_TODSun.m_DayOrbitSpeed  =  0.0f;
		m_TODSun.m_NightOrbitSpeed = 0.0f;
		m_TODSun.m_TimeOfDay=0.0f;
		m_TODSun.SetDefaults();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if __BANK
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void AddWidgetsClient()
	{
		rmcSampleManager::AddWidgetsClient();
		bkBank *bank = &BANKMGR.CreateBank("Lights");
		bank->AddButton("Turn on  Time of Day Test", datCallback(MFA(rmcSamplePong::TurnOnTimeOfDay),this));
		bank->AddButton("Turn Off Time of Day Test", datCallback(MFA(rmcSamplePong::TurnOffTimeOfDay),this));
		m_TODSun.AddWidgets(*bank,"TODSun");
		bank->AddSlider("FOVX",&m_FovX,0.0f,100.0f,0.01f);
		bank->AddSlider("FOVY",&m_FovY,0.0f,100.0f,0.01f);
		bank->AddSlider("NearClip",&m_NearClip,-1000.0f,1000.0f,0.1f);
		bank->AddSlider("FarClip",&m_FarClip,-1000.0f,1000.0f,0.1f);


		m_LightingMgr.AddWidgets(BANKMGR.CreateBank("Lighting Mgr"));
		bank = &BANKMGR.CreateBank("Models");
		for(int i=0;i<m_Objects.GetCount();i++)
		{
			bank->PushGroup(GetFileName(i),false);
			bank->AddToggle("Visible",&m_Objects[i].m_IsVisible);
			bank->AddSlider("Position",&m_Objects[i].m_Matrix.d,-1000.0f,1000.0f,0.1f);
			bank->AddSlider("Rotation",&m_Objects[i].m_Rotation,-1000.0f,1000.0f,0.1f);
			// m_Objects[i].m_Drawable->GetShaderData()->AddWidgets(*bank);
			bank->PopGroup();
		}
	}
#endif
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void DrawGrid()
	{
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
};

/***** rmcSampleLightProxy *************************************/
/***************************************************************/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rmcSampleLightProxy::rmcSampleLightProxy() : rmlLightProxy()
{
	m_GamePointer = 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int rmcSampleLightProxy::BuildVisibleListForViewport( const grcViewport * /*viewport*/, u32 /*flags*/, const rmlLightSource * /*source*/ )
{
	if(!m_GamePointer)
		return 0;

	//NOTE: Don't forget to clear the render instance list!
	m_VisibleInstances.Resize(0);
	int cnt=0;

	//here is where we'd do all our fancy viewport culling and bucket filtering
	//this simple example will just add all the models loaded
	for(int i=0;i<m_GamePointer->m_Objects.GetCount();i++)
	{
		rmcDrawableObject * curObject = &m_GamePointer->m_Objects[i];
		if(curObject->m_IsVisible)
		{
			RegisterRenderInstance(curObject->m_Drawable,curObject->m_Matrix,curObject->m_Skeleton, 0);
			cnt++;
		}
	}

	return cnt;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// main application
int Main()
{	
#if __XENON
	GRCDEVICE.SetSize(640,480);
#else
	GRCDEVICE.SetSize(1024,768);
#endif

	rmcSamplePong sampleRmc;
	sampleRmc.Init(RAGE_ASSET_ROOT "sample_rmlighting/");

	sampleRmc.UpdateLoop();

	sampleRmc.Shutdown();

	return 0;
}
