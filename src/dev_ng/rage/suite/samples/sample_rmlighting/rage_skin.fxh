// rage commonly used functions and defines related to skin rendering

//------------------------------------
shared const float4x3 gBoneMtx[55] : WorldMatrixArray;

//----------------------
// common rage structs:
//		** Put skinning related structs here that can be shared
struct rageSkinVertexInput {
	// This covers the default rage skinned vertex
    float3 pos            : POSITION;
	float2 weight		  : BLENDWEIGHT;
	float4 blendindices	  : BLENDINDICES;
    float2 texCoord0      : TEXCOORD0;
    float3 normal		  : NORMAL;
};

struct rageSkinVertexInputBump {
	// This covers the default rage skinned vertex
    float3 pos            : POSITION;
	float2 weight		  : BLENDWEIGHT;
	float4 blendindices	  : BLENDINDICES;
    float2 texCoord0      : TEXCOORD0;
    float3 normal		  : NORMAL;
	float3 tangent		  : TANGENT0;
};


//----------------------
// common rage functions:
//		** Put skinning related functions here that can be shared

// Compute a composite matrix using 3 weights for a given bone
float4x3 ComputeSkinMtx(int4 indicies, float2 weights)
{
	// Use this to get the posed mtx for use by verts & normals
	float4x3 skinMtx = gBoneMtx[indicies.z] * weights.x;
	skinMtx += gBoneMtx[indicies.y] * weights.y;
	skinMtx += gBoneMtx[indicies.x] * (1 - weights.x - weights.y);
	return skinMtx;
}

