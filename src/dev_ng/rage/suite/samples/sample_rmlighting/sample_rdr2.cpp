// 
// sample_rmlighting/sample_rdr2.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// PURPOSE:
//		This sample demonstrates how to set up the lighting system to use shadowmaps in Pong

#include "sample_rmcore/sample_rmcore.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "crskeleton/skeletondata.h"
#include "crskeleton/skeleton.h"
#include "file/token.h"
#include "grcore/im.h"
#include "grmodel/setup.h"
#include "grmodel/shadergroup.h"
#include "math/random.h"
#include "rmcore/drawable.h"
#include "rmlighting/lightingmgr.h"
#include "rmlighting/lightproxy.h"
#include "rmlighting/lightsource.h"
#include "rmlighting/shadowmap.h"
#include "sample_rmlighting/rdr2_shadowmap.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/vector4.h"

#include "sample_todlight.h"

using namespace rage;

int perryint=0;
PARAM(file,"[sample_shadowmap]");


class rmcSampleRdr2;



//Must overide the rmlLightProxy to further communication between lighting system and game


/***** rmcSamplePong **************************************/
/***************************************************************/
class rmcSampleRdr2 : public ragesamples::rmcSampleManager
{
public:


	rdr2LightingMgr		m_LightingMgr;
	rdr2LightProxy		m_LightProxy;

	
	float				m_NearClip;
	float				m_FarClip;
	float				m_FovX;
	float				m_FovY;
	Matrix34			m_CameraMatrix;
	bool				m_ShowOrtho;
	atArray<rmcDrawableObject>	m_Objects;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	rmcSampleRdr2() : rmcSampleManager()
	{
		SetMinFiles(1);
		SetMaxFiles(10);
		SetDefaultFileName(0, "rock_01x");
		GetFileList();
		m_NearClip = 0.00f;
		m_FarClip  = 100.0f;
		m_FovX = 60.0f;
		m_FovY = 30.0f;
		m_CameraMatrix.Identity();
		m_ShowOrtho=false;
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
protected:
	void InitShaderSystems()
	{
		rmcSampleManager::InitShaderSystems();

		m_LightProxy.Init(4,5);
		m_LightingMgr.Init(&m_LightProxy);
		m_LightingMgr.InitShaderSystem();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void InitCamera()
	{
		m_CamMgr.Init(Vector3(0,5.0f,10.0f),Vector3(0,0,0),dcamCamMgr::CAM_MAYA);
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void InitClient()
	{
		ragesamples::rmcSampleManager::InitClient();
		EnableLogoDraw(false);
		EnableBackgroundDraw(true);
		SetClearColor(Color32(0,0,0));
		m_DrawLights=false;

		m_Objects.Reserve(2000);
		//Load up all the models
		for(int i=0;i<GetNumFileNames();i++)
		{
			ASSET.PushFolder(GetFileName(i));

			for(int k=0;k<20;k++)
			{
				rmcDrawable *drawable = rage_new rmcDrawable();
				if(!drawable->Load("entity"))
				{
					delete drawable;			
					continue;
				}

				//Load attached skeleton (if it has one)
				crSkeleton * skeleton=0;
				crSkeletonData *skeldata;
				skeldata = drawable->GetSkeletonData();
				if (skeldata)
				{
					skeleton = rage_new crSkeleton();
					skeleton->Init(*skeldata,NULL);
				}

				Matrix34 matrix;
				matrix.Identity();
				m_LightingMgr.AddShadowCastingObject(drawable,skeleton,&matrix);

				rmcDrawableObject & newObj = m_Objects.Append();
				newObj.m_Drawable = drawable;
				newObj.m_Skeleton = skeleton;
				//Random position
				float xx = g_DrawRand.GetRanged(-5.0f,5.0f);
				float yy = g_DrawRand.GetRanged(-5.0f,5.0f);
				float zz = g_DrawRand.GetRanged(-5.0f,5.0f);
				newObj.m_Matrix.Set(matrix);
				newObj.m_Matrix.Scale(g_DrawRand.GetRanged(0.5f,3.0f));
				newObj.m_Matrix.d.Set(xx,yy,zz);
				xx = g_DrawRand.GetRanged(-0.05f,0.05f);
				yy = g_DrawRand.GetRanged(-0.05f,0.05f);
				zz = g_DrawRand.GetRanged(-0.05f,0.05f);
				newObj.m_Rotation.Set(xx,yy,zz);
			}
			ASSET.PopFolder();

		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void ShutdownClient()
	{
		rmcSampleManager::ShutdownClient();
		m_LightingMgr.Shutdown();
		m_Objects.Reset();
	}
#if __BANK
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void AddWidgetsPasses()
	{
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void AddWidgetsLights()
	{
	}
#endif
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void InitLights()
	{
		//ragesamples::rmcSampleManager::InitLights();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void UpdateLights()
	{
		//ragesamples::rmcSampleManager::UpdateLights();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void UpdateClient()
	{
		m_LightingMgr.Update();

		//repose skeletons
		for(int i=0;i<m_Objects.GetCount();i++)
		{
			m_Objects[i].m_Matrix.RotateX(m_Objects[i].m_Rotation.x);
			m_Objects[i].m_Matrix.RotateY(m_Objects[i].m_Rotation.y);
			m_Objects[i].m_Matrix.RotateZ(m_Objects[i].m_Rotation.z);
			if(m_Objects[i].m_Skeleton)
			{
				m_Objects[i].m_Skeleton->SetParentMtx(&m_Objects[i].m_Matrix);
				m_Objects[i].m_Skeleton->Update();
			}
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void DrawClient()
	{
		m_CameraMatrix.Set(grcViewport::GetCurrent()->GetCameraMtx());

		//m_CameraMatrix.d.Print("here i am");
		//m_CameraMatrix.c.Print("looking (+c)");

		//m_LightingMgr.DoShadowPass();

		grcState::Default();
		grcState::Default();
		
		//grcState::SetBestLightingMode(grclmDirectional);
		//grcLightState::SetEnabled(true);
		

		//

		//Note: Objects are always drawn by the Lighting manager 
		m_LightingMgr.DrawScene();

	//	grcState::SetBestLightingMode(grclmDirectional);
	//	grcLightState::SetEnabled(true);
		
	


		/*grcViewport *oldVp = grcViewport::GetCurrent();
		grcViewport vp;
		
		if(m_ShowOrtho)
		{
			((rdr2ShadowMap*)m_LightingMgr.GetShadowMapSystem())->CalcOrthoViewport(&vp);
			grcViewport::SetCurrent(&vp);
		
		}		
		*/

		//grcViewport::SetCurrentWorldIdentity();

		

		//Draw all the objects
		/*for (u32 bucket=0; bucket<20; bucket++)
		{
			for(int o=0;o<m_Objects.GetCount();o++)
			{
				if(m_Objects[o].m_Skeleton) {
					m_Objects[o].m_Drawable->DrawSkinned(*m_Objects[o].m_Skeleton,bucket,0);
				}
				else {
					m_Objects[o].m_Drawable->Draw(m_Objects[o].m_Matrix,bucket,0);
				}
			}
		}
		*/
		//grcViewport::SetCurrent(oldVp);
		//grcWorldIdentity();

		m_LightingMgr.DrawSun();
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
#if __BANK
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void AddWidgetsClient()
	{
		rmcSampleManager::AddWidgetsClient();
		//bkBank *bank = &BANKMGR.CreateBank("Lights");
		//bank->AddButton("Turn on  Time of Day Test", datCallback(MFA(rmcSampleRdr2::TurnOnTimeOfDay),this));
		//bank->AddButton("Turn Off Time of Day Test", datCallback(MFA(rmcSampleRdr2::TurnOffTimeOfDay),this));
		//m_TODSun.AddWidgets(*bank,"TODSun");
		//bank->AddSlider("FOVX",&m_FovX,0.0f,100.0f,0.01f);
		//bank->AddSlider("FOVY",&m_FovY,0.0f,100.0f,0.01f);
		//bank->AddSlider("NearClip",&m_NearClip,-1000.0f,1000.0f,0.1f);
		//bank->AddSlider("FarClip",&m_FarClip,-1000.0f,1000.0f,0.1f);


		bkBank *bank = &BANKMGR.CreateBank("Lighting Mgr");
		bank->AddToggle("Show Ortho",&m_ShowOrtho);

		m_LightingMgr.AddWidgets(*bank);
		bank = &BANKMGR.CreateBank("Models");
		for(int i=0;i<m_Objects.GetCount();i++)
		{
			//bank->PushGroup(GetFileName(i),false);
			bank->AddToggle("Visible",&m_Objects[i].m_IsVisible);
			bank->AddSlider("Position",&m_Objects[i].m_Matrix.d,-1000.0f,1000.0f,0.1f);
			bank->AddSlider("Rotation",&m_Objects[i].m_Rotation,-1000.0f,1000.0f,0.1f);
			// m_Objects[i].m_Drawable->GetShaderData()->AddWidgets(*bank);
			//bank->PopGroup();
		}
	}
#endif
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void DrawGrid()
	{
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// main application
int Main()
{	
#if __XENON
	GRCDEVICE.SetSize(640,480);
#else
	GRCDEVICE.SetSize(1024,768);
#endif

	rmcSampleRdr2 sampleRmc;
	sampleRmc.Init(RAGE_ASSET_ROOT "sample_rmlighting/");

	sampleRmc.UpdateLoop();

	sampleRmc.Shutdown();

	return 0;
}
