set ARCHIVE=sample_rmlighting
set FILES= sample_todLight 
set TESTERS=sample_pong sample_rdr2 
set SAMPLE_LIBS=sample_rmcore %RAGE_SAMPLE_GRCORE_LIBS% sample_rmlighting parser parsercore
set LIBS=%SAMPLE_LIBS% %RAGE_GFX_LIBS% rmlighting devcam %RAGE_CORE_LIBS%
set XPROJ=%RAGE_DIR%\suite\src %RAGE_DIR%\suite\samples %RAGE_DIR%\base\src %RAGE_DIR%\base\samples
