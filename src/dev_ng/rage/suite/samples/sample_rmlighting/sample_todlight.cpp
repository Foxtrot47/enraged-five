// sample_rmlighting/sample_todlight.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//
// PURPOSE:
//		This class defines a time of day directional "sun" shadowcasting light -controlling orbit position and keyframed colors


#include "sample_todlight.h"

#include "file/asset.h"
#include "file/token.h"
#include "grcore/im.h"
#include "grcore/state.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"
#include "system/param.h"
#include "system/timemgr.h"

using namespace rage;


PARAM(nokeyframe, "[sample_rmlighting] Disable keyframes");


/***** rmlSampleLightKeyFrame **********************************/
/***************************************************************/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rmlSampleLightKeyFrame::rmlSampleLightKeyFrame()
{
	m_KeyColor.Zero();
	m_KeyAmbLight.Zero();
	m_KeyShadow.Zero();
	m_KeyOrbitX=0.0f;
	m_KeyBlur = 1.00f;
	m_KeyZOffset = -0.001f;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlSampleLightKeyFrame::Load(fiAsciiTokenizer*token)
{
	if(token->CheckToken("LightColor:"))
		token->GetVector(m_KeyColor);
	if(token->CheckToken("AmbColor:"))
		token->GetVector(m_KeyAmbLight);
	if(token->CheckToken("ShadowColor:"))
		token->GetVector(m_KeyShadow);
	if(token->CheckToken("OrbitPos:"))
		m_KeyOrbitX = token->GetFloat();
	if(token->CheckToken("Blur:"))
		m_KeyBlur = token->GetFloat();
	if(token->CheckToken("ZOffset:"))
		m_KeyZOffset = token->GetFloat();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if __BANK
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlSampleLightKeyFrame::AddWidgets(bkBank&bank,const char *name)
{
	bank.PushGroup(name,false);
	bank.AddSlider("Orbit X Pos",&m_KeyOrbitX,-1000.0f,1000.0f,0.001f);
	bank.AddSlider("Light Color",&m_KeyColor,-100.0f,100.0f,0.001f);
	bank.AddSlider("Ambient Color",&m_KeyAmbLight,-100.0f,100.0f,0.001f);
	bank.AddSlider("Shadow Color",&m_KeyShadow,-100.0f,100.0f,0.001f);
	bank.AddSlider("Blur",&m_KeyBlur,-100.0f,100.0f,0.001f);
	bank.AddSlider("ZOffset",&m_KeyZOffset,-100.0f,100.0f,0.001f);
	bank.PopGroup();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlSampleLightKeyFrame::Save(fiAsciiTokenizer*token)
{
	token->PutDelimiter("\nLightColor: ");
	token->Put(m_KeyColor);
	token->PutDelimiter("\nAmbColor: ");
	token->Put(m_KeyAmbLight);
	token->PutDelimiter("\nShadowColor: ");
	token->Put(m_KeyShadow);
	token->PutDelimiter("\nOrbitPos: ");
	token->Put(m_KeyOrbitX);
	token->PutDelimiter("\nBlur: ");
	token->Put(m_KeyBlur);
	token->PutDelimiter("\nZOffset: ");
	token->Put(m_KeyZOffset);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif //BANK
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



/***** rmlSampleLightKeyFrame **********************************/
/***************************************************************/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rmcSampleTODLight::rmcSampleTODLight()
{
	m_Enabled=	true;
	m_LightColor.Set(1.0f,0.804f,0.741f,1.0f);
	m_ShadowColor.Set(1.f,1.0f,1.0f,0.788f);
	m_AmbientColor.Set(0.245f,0.205f,0.145f,1.0f);
	m_Distance = 85.0f;
	m_OrbitRotation.Zero();
	m_OrbitVelocity = 0.0f;
	m_LightInfo = 0;
	m_ShadowBlur = 0.002f;
	m_ShadowZOffset = -0.006f;
	
	m_Matrix.Identity();

	m_TimeOfDay = 0.0f;
	m_DayOrbitSpeed = 0.0f;
	m_DayOrbitSpeed2 = 0.0f;
	m_RateChangeTime = 2.0f;
	m_NightOrbitSpeed =0.0f;

	sprintf(m_Name,"TODSun");

	//Vars to visulize the light
	m_Draw= true;
	m_Radius= 10.0f;
	m_Alpha= 1.0f;
	m_GlowAlpha=1.5f;
	m_Texture =(grcTexture*)grcTexture::None;

	//Keyframe Vars
	m_KeyframeEnable = !PARAM_nokeyframe.Get();

	//Shader Vars
	m_DepthTextureID=grcegvNONE;
	m_ShadowColorID=grcegvNONE;
	m_LightMatrixID=grcegvNONE;
	m_PixelBlurID=grcegvNONE;
	m_ZOffsetID=grcegvNONE;
	
	m_LightDepthMap=0;

	
	SetDefaults();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rmcSampleTODLight::~rmcSampleTODLight()
{
	if(m_Texture)
		m_Texture->Release();
	
	if(m_LightDepthMap)
		m_LightDepthMap->Release();
	m_LightDepthMap=0;

	m_Texture=0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmcSampleTODLight::Init(const char*name)
{
	//Load sun texture
	ASSET.PushFolder("textures");
	m_Texture = grcTextureFactory::GetInstance().Create("SunGlowTexture");
	ASSET.PopFolder();

	sprintf(m_Name,"%s",name);
	//Load tuning 
	Load();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmcSampleTODLight::SetDefaults()
{
	m_OrbitRotation.Zero();
	m_LightColor.Set(1.0f,0.804f,0.741f,1.0f);
	m_ShadowColor.Set(1.f,1.0f,1.0f,0.788f);
	m_AmbientColor.Set(0.245f,0.205f,0.145f,1.0f);
	m_ShadowBlur = 1.00f;
	m_ShadowZOffset = -0.001f;

	//Set default keyframe colors for dawn,noon,dusk and midnight
	m_Keyframes[0].m_KeyOrbitX=1.50f;
	m_Keyframes[1].m_KeyOrbitX=0.00f;
	m_Keyframes[2].m_KeyOrbitX=-1.50f;
	m_Keyframes[3].m_KeyOrbitX=-3.00f;

	m_Keyframes[0].m_KeyColor.Set(1.0f,0.718f,0.741f,1.0f);
	m_Keyframes[1].m_KeyColor.Set(1.0f,0.948f,0.939f,1.0f);
	m_Keyframes[2].m_KeyColor.Set(1.0f,0.804f,0.741f,1.0f);
	m_Keyframes[3].m_KeyColor.Set(0.753f,0.657f,1.0f,1.0f);

	m_Keyframes[0].m_KeyAmbLight.Set(0.165f,0.122f,0.132f,1.0f);
	m_Keyframes[1].m_KeyAmbLight.Set(0.294f,0.232f,0.163f,1.0f);
	m_Keyframes[2].m_KeyAmbLight.Set(0.243f,0.150f,0.095f,1.0f);
	m_Keyframes[3].m_KeyAmbLight.Set(0.161f,0.144f,0.214f,1.0f);

	m_Keyframes[0].m_KeyShadow.Set(1.0f,1.0f,1.0,0.397f);
	m_Keyframes[1].m_KeyShadow.Set(1.f,1.f,1.f,0.550f);
	m_Keyframes[2].m_KeyShadow.Set(1.f,1.0f,1.0f,0.388f);
	m_Keyframes[3].m_KeyShadow.Set(1.f,1.f,1.f,0.5f);

	m_Keyframes[0].m_KeyBlur=1.00f;
	m_Keyframes[1].m_KeyBlur=1.00f;
	m_Keyframes[2].m_KeyBlur=1.00f;
	m_Keyframes[3].m_KeyBlur=1.00f;

	m_Keyframes[0].m_KeyZOffset=-0.001f;
	m_Keyframes[1].m_KeyZOffset=-0.001f;
	m_Keyframes[2].m_KeyZOffset=-0.001f;
	m_Keyframes[3].m_KeyZOffset=-0.001f;

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmcSampleTODLight::Update()
{
	if(m_KeyframeEnable)
	{
		m_OrbitVelocity=0.0f;
		
		//Night is twice as fast as day
		if(m_TimeOfDay>0.55f)
			m_TimeOfDay+=m_NightOrbitSpeed*TIME.GetSeconds();
		else
		{
			if(m_TimeOfDay>m_RateChangeTime)
				m_TimeOfDay+=m_DayOrbitSpeed2*TIME.GetSeconds();
			else
				m_TimeOfDay+=m_DayOrbitSpeed*TIME.GetSeconds();
		}

		if(m_TimeOfDay>=1.0f)
			m_TimeOfDay=0.0f;
		if(m_TimeOfDay<0.0f)
			m_TimeOfDay=0.999f;
		
		SetFromTime(m_TimeOfDay);
	}
	else
	{
		m_OrbitRotation.x+=(m_OrbitVelocity*TIME.GetSeconds());
		SetFromTime(0.0f);
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmcSampleTODLight::SetFromTime(float tod)
{
	Vector3 pos(0.0f,m_Distance,0.0f);

	if(m_KeyframeEnable)
	{
		float keyTime[5]={0.0f,0.25f,0.5f,0.75f,1.0f};
		int startKF;
		int endKF;
		int i=0;
		//figure out which keyframe we're in
		while((tod>=keyTime[i]))
			i++;
		startKF=i-1;
		endKF=i;

		float ftime = (tod-keyTime[startKF]);
		ftime/=(keyTime[endKF]-keyTime[startKF]);

		//Phony 5th keyframe for easy looping (matches 0 keyframe)
		m_Keyframes[4].m_KeyOrbitX=m_Keyframes[0].m_KeyOrbitX-6.3f;
		m_Keyframes[4].m_KeyColor.Set(m_Keyframes[0].m_KeyColor);
		m_Keyframes[4].m_KeyAmbLight.Set(m_Keyframes[0].m_KeyAmbLight);
		m_Keyframes[4].m_KeyShadow.Set(m_Keyframes[0].m_KeyShadow);
		m_Keyframes[4].m_KeyBlur = m_Keyframes[0].m_KeyBlur;
		m_Keyframes[4].m_KeyZOffset = m_Keyframes[0].m_KeyZOffset;


		//Position Sun and colors
		m_OrbitRotation.x = Lerp(ftime,m_Keyframes[startKF].m_KeyOrbitX,m_Keyframes[endKF].m_KeyOrbitX);
		m_LightColor.x=(Lerp(ftime,m_Keyframes[startKF].m_KeyColor.x,m_Keyframes[endKF].m_KeyColor.x));
		m_LightColor.y=(Lerp(ftime,m_Keyframes[startKF].m_KeyColor.y,m_Keyframes[endKF].m_KeyColor.y));
		m_LightColor.z=(Lerp(ftime,m_Keyframes[startKF].m_KeyColor.z,m_Keyframes[endKF].m_KeyColor.z));
		m_LightColor.w=(Lerp(ftime,m_Keyframes[startKF].m_KeyColor.w,m_Keyframes[endKF].m_KeyColor.w));
		m_AmbientColor.x=(Lerp(ftime,m_Keyframes[startKF].m_KeyAmbLight.x,m_Keyframes[endKF].m_KeyAmbLight.x));
		m_AmbientColor.y=(Lerp(ftime,m_Keyframes[startKF].m_KeyAmbLight.y,m_Keyframes[endKF].m_KeyAmbLight.y));
		m_AmbientColor.z=(Lerp(ftime,m_Keyframes[startKF].m_KeyAmbLight.z,m_Keyframes[endKF].m_KeyAmbLight.z));
		m_AmbientColor.w=(Lerp(ftime,m_Keyframes[startKF].m_KeyAmbLight.w,m_Keyframes[endKF].m_KeyAmbLight.w));
		m_ShadowColor.x=(Lerp(ftime,m_Keyframes[startKF].m_KeyShadow.x,m_Keyframes[endKF].m_KeyShadow.x));
		m_ShadowColor.y=(Lerp(ftime,m_Keyframes[startKF].m_KeyShadow.y,m_Keyframes[endKF].m_KeyShadow.y));
		m_ShadowColor.z=(Lerp(ftime,m_Keyframes[startKF].m_KeyShadow.z,m_Keyframes[endKF].m_KeyShadow.z));
		m_ShadowColor.w=(Lerp(ftime,m_Keyframes[startKF].m_KeyShadow.w,m_Keyframes[endKF].m_KeyShadow.w));
		m_ShadowBlur=(Lerp(ftime,m_Keyframes[startKF].m_KeyBlur,m_Keyframes[endKF].m_KeyBlur));
		m_ShadowZOffset=(Lerp(ftime,m_Keyframes[startKF].m_KeyZOffset,m_Keyframes[endKF].m_KeyZOffset));
	}

	pos.RotateX(m_OrbitRotation.x);
	pos.RotateZ(m_OrbitRotation.y);

	
	//Look at origin
	m_Matrix.Identity();
	m_Matrix.d.Set(pos);
	m_Matrix.LookAt(Vector3(0.0f,0.0f,0.0f), Vector3(0.0f, 1.0f, 0.0f));
	
	Vector3 A,B,C(pos);
	A.Set(0.0f,0.0f,-1.0f);
	C.Normalize();
	B.Cross(A,C);
	A.Cross(C,B);
	m_Matrix.a.Set(A);
	m_Matrix.b.Set(B);
	m_Matrix.c.Set(C);
	m_Matrix.d.Set(pos);
	
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmcSampleTODLight::Draw()
{
	if(!(m_Enabled && m_Draw) )
		return;

	//DrawSun
	const Matrix34 & matrix = grcViewport::GetCurrent()->GetCameraMtx();
	Vector3 A,B,C,D;
	float radius=m_Radius*3;

	A.Set(m_Matrix.d);
	B.Set(m_Matrix.d);
	C.Set(m_Matrix.d);
	D.Set(m_Matrix.d);


	A.AddScaled(matrix.a,radius);
	A.AddScaled(matrix.b,-radius);
	B.AddScaled(matrix.a,radius);
	B.AddScaled(matrix.b,radius);
	C.AddScaled(matrix.a,-radius);
	C.AddScaled(matrix.b,-radius);
	D.AddScaled(matrix.a,-radius);
	D.AddScaled(matrix.b,radius);
    
	Vector3 ambColor(m_AmbientColor.x,m_AmbientColor.y,m_AmbientColor.z);
	ambColor.Scale(m_AmbientColor.w);
	Color32 c = Color32(ambColor.x,ambColor.y,ambColor.z,m_GlowAlpha);

	grcState::Default();
	grcState::Default();
	grcViewport::SetCurrentWorldIdentity();
	grcState::SetAlphaFunc(grcafAlways);
	grcState::SetDepthWrite(false);
	grcState::SetAlphaBlend(true);
	grcState::SetAlphaTest(false);
	grcState::SetBlendSet(grcbsAdd);
	grcLightState::SetEnabled(false);
	grcBindTexture( m_Texture);
	
	grcBegin(drawTriStrip,4);
	grcVertex(A.x,A.y,A.z,0,0,0,c,0.0f,0.0f);
	grcVertex(B.x,B.y,B.z,0,0,0,c,1.0f,0.0f);
	grcVertex(C.x,C.y,C.z,0,0,0,c,0.0f,1.0f);
	grcVertex(D.x,D.y,D.z,0,0,0,c,1.0f,1.0f);
	grcEnd();

	radius=m_Radius;
	A.Set(m_Matrix.d);
	B.Set(m_Matrix.d);
	C.Set(m_Matrix.d);
	D.Set(m_Matrix.d);


	A.AddScaled(matrix.a,radius);
	A.AddScaled(matrix.b,-radius);
	B.AddScaled(matrix.a,radius);
	B.AddScaled(matrix.b,radius);
	C.AddScaled(matrix.a,-radius);
	C.AddScaled(matrix.b,-radius);
	D.AddScaled(matrix.a,-radius);
	D.AddScaled(matrix.b,radius);

	Vector3 lColor(m_LightColor.x,m_LightColor.y,m_LightColor.z);
	lColor.Scale(m_LightColor.w);
	c = Color32(lColor.x,lColor.y,lColor.z,m_Alpha);

	grcState::SetDepthWrite(false);
	grcState::SetBlendSet(grcbsAdd);

	grcBindTexture( m_Texture);
	
	grcBegin(drawTriStrip,4);
	grcVertex(A.x,A.y,A.z,1.0f,1.0f,1.0f,c,0.0f,0.0f);
	grcVertex(B.x,B.y,B.z,1.0f,1.0f,1.0f,c,1.0f,0.0f);
	grcVertex(C.x,C.y,C.z,1.0f,1.0f,1.0f,c,0.0f,1.0f);
	grcVertex(D.x,D.y,D.z,1.0f,1.0f,1.0f,c,1.0f,1.0f);
	grcEnd();

	grcBegin(drawTriStrip,4);
	grcVertex(A.x,A.y,A.z,1.0f,1.0f,1.0f,c,0.0f,0.0f);
	grcVertex(B.x,B.y,B.z,1.0f,1.0f,1.0f,c,1.0f,0.0f);
	grcVertex(C.x,C.y,C.z,1.0f,1.0f,1.0f,c,0.0f,1.0f);
	grcVertex(D.x,D.y,D.z,1.0f,1.0f,1.0f,c,1.0f,1.0f);
	grcEnd();
	grcLightState::SetEnabled(true);
	grcState::SetDepthWrite(true);
	grcState::SetBlendSet(grcbsNormal);
	grcState::Default();
	
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmcSampleTODLight::Load()
{
	ASSET.PushFolder("tune/lighting");
	fiStream *S = ASSET.Open(m_Name, "txt");
	if ( S ) 
	{
		fiAsciiTokenizer token;
		token.Init("TODColors", S);

		for(int i=0;i<4;i++)
			m_Keyframes[i].Load(&token);

		if(token.CheckToken("Rate0:"))
			m_DayOrbitSpeed = token.GetFloat();
		if(token.CheckToken("Rate1:"))
			m_DayOrbitSpeed2 = token.GetFloat();
		if(token.CheckToken("RateChange:"))
			m_RateChangeTime = token.GetFloat();



		S->Close();
	}
	ASSET.PopFolder();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if __BANK
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmcSampleTODLight::AddWidgets(bkBank&bank,const char * name)
{
	bank.PushGroup(name,false);
	bank.AddToggle("Enabled",&m_Enabled);
	
	bank.PushGroup("Colors",false);
	bank.AddSlider("Light Color Red",&m_LightColor.x,-100.0f,100.0f,0.01f);
	bank.AddSlider("Light Color Green",&m_LightColor.y,-100.0f,100.0f,0.01f);
	bank.AddSlider("Light Color Blue",&m_LightColor.z,-100.0f,100.0f,0.01f);
	bank.AddSlider("Light Color Intensity",&m_LightColor.w,-100.0f,100.0f,0.01f);
	bank.AddSlider("Ambient Color Red",&m_AmbientColor.x,-100.0f,100.0f,0.01f);
	bank.AddSlider("Ambient Color Green",&m_AmbientColor.y,-100.0f,100.0f,0.01f);
	bank.AddSlider("Ambient Color Blue",&m_AmbientColor.z,-100.0f,100.0f,0.01f);
	bank.AddSlider("Ambient Color Intensity",&m_AmbientColor.w,-100.0f,100.0f,0.01f);
	bank.AddSlider("Shadow Color Red",&m_ShadowColor.x,-100.0f,100.0f,0.01f);
	bank.AddSlider("Shadow Color Green",&m_ShadowColor.y,-100.0f,100.0f,0.01f);
	bank.AddSlider("Shadow Color Blue",&m_ShadowColor.z,-100.0f,100.0f,0.01f);
	bank.AddSlider("Shadow Color Intensity",&m_ShadowColor.w,-100.0f,100.0f,0.01f);
	bank.PopGroup();

	bank.AddSlider("Distance",&m_Distance,-1000.0f,1000.0f,1.0f);
	bank.AddSlider("Orbit X",&m_OrbitRotation.x,-100.0f,100.0f,0.01f);
	bank.AddSlider("Orbit Z",&m_OrbitRotation.y,-100.0f,100.0f,0.01f);
	bank.AddToggle("Draw",&m_Draw);
	bank.AddSlider("Blur",&m_ShadowBlur,0.0f,1000.0f,0.001f);
	bank.AddSlider("ZOffset",&m_ShadowZOffset,-100.0f,1000.0f,0.001f);
	bank.AddSlider("Radius",&m_Radius,0.0f,100.0f,0.01f);
	bank.AddSlider("Alpha",&m_Alpha,0.0f,100.0f,0.01f);
	bank.AddSlider("GlowAlpha",&m_GlowAlpha,0.0f,100.0f,0.01f);
	bank.AddSlider("Time Of Day",&m_TimeOfDay,0.0f,1.0f,0.01f);
	bank.AddSlider("DayTimeRate",&m_DayOrbitSpeed,-100.0f,100.0f,0.0001f);
	bank.AddSlider("DayTimeRate2",&m_DayOrbitSpeed2,-100.0f,100.0f,0.0001f);
	bank.AddSlider("TimeToSwitchToRate2",&m_RateChangeTime,-100.0f,100.0f,0.001f);
	bank.AddSlider("NightTimeShift",&m_NightOrbitSpeed,-100.0f,100.0f,0.01f);
	bank.PushGroup("Keyframes",false);
	bank.AddToggle("Enable Keyframes",&m_KeyframeEnable);
	bank.AddButton("Save", datCallback(MFA(rmcSampleTODLight::Save), this));
	bank.AddButton("Load", datCallback(MFA(rmcSampleTODLight::Load), this));
	m_Keyframes[0].AddWidgets(bank,"Dawn");
	m_Keyframes[1].AddWidgets(bank,"Noon");
	m_Keyframes[2].AddWidgets(bank,"Dusk");
	m_Keyframes[3].AddWidgets(bank,"Midnight");
	bank.PopGroup();

	bank.PopGroup();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmcSampleTODLight::Save()
{		
	ASSET.PushFolder("tune/lighting");
	fiStream *S = ASSET.Create(m_Name, "txt");
	if ( S ) 
	{
		fiAsciiTokenizer token;
		token.Init("TODColors", S);
		for(int i=0;i<4;i++)
			m_Keyframes[i].Save(&token);

		token.PutDelimiter("\nRate0: ");
		token.Put(m_DayOrbitSpeed);
		token.PutDelimiter("\nRate1: ");
		token.Put(m_DayOrbitSpeed2);
		token.PutDelimiter("\nRateChange: ");
		token.Put(m_RateChangeTime);

		S->Close();
	}
	ASSET.PopFolder();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif // __BANK
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
