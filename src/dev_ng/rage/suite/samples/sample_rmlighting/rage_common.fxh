// rage commonly used functions and defines

//------------------------------------
shared float4x4 gWorld : World;
shared float4x4 gWorldInverse : WorldInverse;
shared float4x4 gWorldView : WorldView;
shared float4x4 gWorldViewInverse : WorldViewInverse;
shared float4x4 gWorldViewProj : WorldViewProjection;
shared float4x4 gWorldInverseTranspose : WorldInverseTranspose;
shared float4x4 gViewInverse : ViewInverse;
shared float4x3 gTextureMtx0 : TextureMatrix;

#define MAX_LIGHTS 3

#define LT_POINT 0
#define LT_DIR 1
#define LT_COUNT 2

shared float4 gLightPosDir[MAX_LIGHTS] : Position
<
	string Object = "PointDirLight";
	string Space = "World";
> = { {1403.0f, 1441.0f, 1690.0f, 0.0f}, {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0} };

shared float4 gLightColor[MAX_LIGHTS] : Diffuse
<
    string UIName = "Diffuse Light Color";
    string Object = "LightPos";
> = { {1.0f, 1.0f, 1.0f, 1.0f}, {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0} };

shared int gLightTypeCounts[LT_COUNT] : LightTypeCounts
<
	string UIName = "Count of each type of light used";
> = { MAX_LIGHTS - 1, 1 };

shared float4 gLightAmbient : Ambient
<
    string UIWidget = "Ambient Light Color";
    string Space = "material";
> = {0.0f, 0.0f, 0.0f, 1.0f};


// common rage structs:
struct rageVertexInput {
	// This covers the default rage vertex format (non-skinned)
    float3 pos			: POSITION;
	float3 diffuse		: COLOR0;
    float2 texCoord0	: TEXCOORD0;
    float2 texCoord1	: TEXCOORD1;
    float3 normal		: NORMAL;
};

struct rageVertexInputBump {
	// This covers the default rage vertex format (non-skinned)
    float3 pos			: POSITION;
	float3 diffuse		: COLOR0;
    float2 texCoord0	: TEXCOORD0;
    float2 texCoord1	: TEXCOORD1;
    float3 normal		: NORMAL;
	float3 tangent		: TANGENT0;
};

struct rageVertexOutputPassThrough {
	// This is used for the simple vertex and pixel shader routines
    float4 pos			: POSITION;
    float3 diffuse		: COLOR0;
    float2 texCoord0	: TEXCOORD0;
	float2 texCoord1	: TEXCOORD1;
	float3 normal		: COLOR1;
};

struct rageLightOutput {
	float4 lightPosDir[MAX_LIGHTS];			// Light position/directions
};


// common rage functions:
// Simple passthrough vertex shader - useful for operations that are primarily per-pixel
rageVertexOutputPassThrough VS_ragePassThrough(rageVertexInput IN)
{
	rageVertexOutputPassThrough OUT;
	OUT.pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
    OUT.diffuse = IN.diffuse;
    OUT.texCoord0 = IN.texCoord0;
    OUT.texCoord1 = IN.texCoord1;
    OUT.normal = mul(float4(IN.normal, 1.0), gWorldInverseTranspose).xyz;
    return OUT;
};

// Even simpler passthrough vertex shader - this one assumes positions are pretransformed
//	Good for screen space blits.
rageVertexOutputPassThrough VS_ragePassThroughNoXform(rageVertexInput IN)
{
	rageVertexOutputPassThrough OUT;
	OUT.pos = float4(IN.pos, 1.0);
    OUT.diffuse = IN.diffuse;
    OUT.texCoord0 = IN.texCoord0;
    OUT.texCoord1 = IN.texCoord1;
    OUT.normal = float3(0.0, 0.0, -1.0);	// Force normal to face camera
    return OUT;
};


// Lighting function helpers
rageLightOutput rageComputeLightData(float3 inWorldPos)
{
	rageLightOutput OUT;
	int index = 0;
	for (int i = 0; i < MAX_LIGHTS; ++i) {
		if ( i < gLightTypeCounts[LT_POINT] ) {
			OUT.lightPosDir[index].xyz = (gLightPosDir[index] - inWorldPos).xyz;
			OUT.lightPosDir[index].w = max(distance(gLightPosDir[index], inWorldPos), 0.01);
			OUT.lightPosDir[index].xyz /= OUT.lightPosDir[index].w;
			OUT.lightPosDir[index].w = 1.0 / OUT.lightPosDir[index].w;
		}
		else {
			// We're going to negate the directional light so that the
			//	math more closely follows that of the point light
			OUT.lightPosDir[index].xyz = -gLightPosDir[index].xyz;
			OUT.lightPosDir[index].w = 100.0;
		}
		index++;
	}
// Had to remove this for loop & collapse into above with if/else
//	 in order to get around shader compiler errors.. !?!?
//	for (int j = 0; j < gLightTypeCounts[LT_DIR]; ++j) {
//		lightPosDir[index].xyz = -gLightPosDir[index].xyz;
//		lightPosDir[index].w = 100.0;
//		index++;
//	}
	return OUT;
}

// This mimics the intrinsic "lit" function, but uses less instructions (faster also??)
float4 rageGetLitFactors( float NdotL, float NdotH, float power )
{
	float4 OUT;
	OUT.y = max(NdotL, 0.0);
	if ( OUT.y <= 0 )
		OUT.z = 0.0;
	else
		OUT.z = pow((max(NdotH, 0.0)),power);
	OUT.xw = 1.0;
// NOTE: Had to rework to reduce instruction count (just the lit
//		command alone puts us over the top)
//    float4 OUT = lit( dot(N, L), dot(N, H), specularColor.w );
//    OUT.y *= IN.lightDistDir0.w;
//	  OUT.y = min(light0.y, 1.0);

	return OUT;
}

