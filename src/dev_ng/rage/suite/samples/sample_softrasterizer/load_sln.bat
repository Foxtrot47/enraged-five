@echo off

setlocal
pushd "%~dp0"

set RS_BUILDBRANCH=x:\gta5\build\dev
set RS_CODEBRANCH=x:\gta5\src\dev
set RS_PROJECT=gta5
set RS_PROJROOT=x:\gta5
set RS_TOOLSROOT=x:\gta5\tools
set RAGE_DIR=x:\gta5\src\dev\rage\

CALL setenv.bat

set BUILD_FOLDER=%RS_BUILDBRANCH%
set RAGE_DIR=x:\gta5\src\dev\rage\
set SCE_PS3_ROOT=X:/ps3sdk/dev/usr/local/430_001/cell

ECHO LOAD_SLN ENVIRONMENT
ECHO BUILD_FOLDER: 	%BUILD_FOLDER%
ECHO RAGE_DIR: 		%RAGE_DIR%
ECHO SCE_PS3_ROOT: 	%SCE_PS3_ROOT%
ECHO END LOAD_SLN ENVIRONMENT

start "C:\Program Files (x86)\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe" %cd%\sample_rasterizer_2008.sln
popd
