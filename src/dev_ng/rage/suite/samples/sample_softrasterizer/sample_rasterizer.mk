top: bottom

include ..\..\..\..\rage\build\Makefile.template

.PHONY: FORCE
TARGET = sample_rasterizer
ELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.elf
SELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.self

bottom: $(SELF)

$(SELF): $(ELF)
	make_fself $(ELF) $(SELF)

LIBS += ..\..\..\..\rage\base\samples\sample_grcore/$(INTDIR)/sample_grcore.lib

..\..\..\..\rage\base\samples\sample_grcore/$(INTDIR)/sample_grcore.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\samples\sample_grcore -f sample_grcore.mk

LIBS += ..\..\..\..\rage\base\src\init/$(INTDIR)/init.lib

..\..\..\..\rage\base\src\init/$(INTDIR)/init.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\init -f init.mk

LIBS += ..\..\..\..\rage\base\samples\sample_file/$(INTDIR)/sample_file.lib

..\..\..\..\rage\base\samples\sample_file/$(INTDIR)/sample_file.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\samples\sample_file -f sample_file.mk

LIBS += ..\..\..\..\rage\base\src\devcam/$(INTDIR)/devcam.lib

..\..\..\..\rage\base\src\devcam/$(INTDIR)/devcam.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\devcam -f devcam.mk

LIBS += ..\..\..\..\rage\base\src\gizmo/$(INTDIR)/gizmo.lib

..\..\..\..\rage\base\src\gizmo/$(INTDIR)/gizmo.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\gizmo -f gizmo.mk

LIBS += ..\..\..\..\rage\base\src\grcustom/$(INTDIR)/grcustom.lib

..\..\..\..\rage\base\src\grcustom/$(INTDIR)/grcustom.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\grcustom -f grcustom.mk

LIBS += ..\..\..\..\rage\base\samples\sample_rmcore/$(INTDIR)/sample_rmcore.lib

..\..\..\..\rage\base\samples\sample_rmcore/$(INTDIR)/sample_rmcore.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\samples\sample_rmcore -f sample_rmcore.mk

LIBS += ..\..\..\..\rage\base\src\shaders/$(INTDIR)/shaders.lib

..\..\..\..\rage\base\src\shaders/$(INTDIR)/shaders.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\shaders -f shaders.mk

LIBS += ..\..\..\..\rage\base\src\grcore/$(INTDIR)/grcore.lib

..\..\..\..\rage\base\src\grcore/$(INTDIR)/grcore.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\grcore -f grcore.mk

LIBS += ..\..\..\..\rage\base\src\shaderlib/$(INTDIR)/shaderlib.lib

..\..\..\..\rage\base\src\shaderlib/$(INTDIR)/shaderlib.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\shaderlib -f shaderlib.mk

LIBS += ..\..\..\..\rage\base\src\input/$(INTDIR)/input.lib

..\..\..\..\rage\base\src\input/$(INTDIR)/input.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\input -f input.mk

LIBS += ..\..\..\..\rage\base\src\jpeg/$(INTDIR)/jpeg.lib

..\..\..\..\rage\base\src\jpeg/$(INTDIR)/jpeg.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\jpeg -f jpeg.mk

LIBS += ..\..\..\..\rage\base\src\vcproj\RageCore\$(INTDIR)\RageCore.lib

..\..\..\..\rage\base\src\vcproj\RageCore\$(INTDIR)\RageCore.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\vcproj\RageCore -f RageCore.mk

LIBS += ..\..\..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib

..\..\..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\stlport\STLport-5.0RC5\src -f stlport.mk

LIBS += ..\..\..\..\rage\base\src\vcproj\RageGraphics\$(INTDIR)\RageGraphics.lib

..\..\..\..\rage\base\src\vcproj\RageGraphics\$(INTDIR)\RageGraphics.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\vcproj\RageGraphics -f RageGraphics.mk

LIBS += ..\..\..\..\rage\suite\src\softrasterizer/$(INTDIR)/softrasterizer.lib

..\..\..\..\rage\suite\src\softrasterizer/$(INTDIR)/softrasterizer.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\suite\src\softrasterizer -f softrasterizer.mk

INCLUDES = -I ..\..\..\..\rage\stlport\STLport-5.0RC5\stlport -I ..\..\..\..\rage\base\src -I ..\..\..\..\rage\base\samples -I ..\..\..\..\rage\suite\samples -I ..\..\..\..\rage\suite\src

$(INTDIR)\sample_rasterizer.obj: sample_rasterizer.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(ELF): $(LIBS) $(INTDIR)\sample_rasterizer.obj
	ppu-lv2-gcc -o $(ELF) $(INTDIR)\sample_rasterizer.obj -Wl,--start-group $(LIBS) -Wl,--end-group $(LLIBS)
