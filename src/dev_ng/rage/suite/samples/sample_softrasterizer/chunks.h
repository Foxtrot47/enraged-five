
#ifndef _CHUNK_H_
#define _CHUNK_H_

#include "vectormath/legacyconvert.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"
#include "grmodel/shader.h"

namespace rage
{

typedef int (*GenFunc)(int xpos, int ypos, int width, Vec4V* __restrict verts, int vstride );



struct GeoChunk
{
	
	grcVertexBuffer*			m_vertexBuffer;
	grcIndexBuffer**			m_indexBuffers;

	int							m_x,m_y;
	GenFunc						m_genFunc;
public:


	static const int OccluderDepth = 8;
#if __ASSERT
	static const int			MaxDepth = 7;
#else
	static const int			MaxDepth = 7;
#endif
	
	static const int			ChunkWidth = 1<<MaxDepth;
	static const int			NumChunkVerts = ChunkWidth * ChunkWidth;
	static const int			NumChunkIndices = (ChunkWidth-1) * (ChunkWidth-1)*6;

	Vec3V						m_bmin;
	Vec3V						m_bmax;
	Vec3V*						m_occludeVerts;
	u8*							m_occIndices;
	int							m_numOccludeIndices;
	int							m_numOccludeVerts;

	GeoChunk() : m_vertexBuffer(0),m_occludeVerts(0) {}

	const grcVertexBuffer* GetVertexBuffer()
	{
		return m_vertexBuffer;
	}

	void Create( int x, int y, GenFunc func, grcIndexBuffer** buffers, u8* occIndices, int numIndices );
	void Generate( grcFvf& fvf )
	{		
		ActualCreate( m_x, m_y, m_genFunc, fvf );
	}
	void ActualCreate( int x, int y, GenFunc func, grcFvf& fvf );
	void Draw(  int lod ) const;
	void CalcBounds( Vec3V_InOut rbmin, Vec3V_InOut rbmax, Vec4V* __restrict verts, int amt );
	void FreeHiModel();
	~GeoChunk();
};


class ProceduralStreamer
{

	atArray<GeoChunk*>  m_unloadedList;
	atArray<std::pair<float, GeoChunk*> >	 m_streamqueue;

	float									m_LoadThreshold;
	float									m_UnloadThreshold;
	int										m_maxStreamJobs;

	int										m_streamqueueSize;
	int										m_numUnloaded;

	void AddStreamJob( GeoChunk* chunk, float area);
public:
	ProceduralStreamer();
	
	void Create( int size);
	void Add( GeoChunk* chunk);

	void StreamOutAssets(atArray<GeoChunk*>& actualList );

	void FindNewAssestToGenerate();
	// execute stream jobs
	void ExectuteAnyPendingJobs( atArray<GeoChunk*>& actualList, grcFvf& fvf);

#if __BANK
	void AddWidgets( bkBank& bank);
#endif
};


struct VisObj
{
	const   GeoChunk*	chunk;
	int		lod;
};


int rstCreateVisibleList(  GeoChunk**  clist, VisObj* __restrict vislist, int amt,	const Vec4V* HiZ,
						 ScalarV_In smallThreshold, float lodFactor, bool useOcclusion, Vec3V* rbmin, Vec3V *rbmax );

int rstSelectOccluders( GeoChunk** __restrict clist,  GeoChunk** __restrict occlist, u8* isClipped, int amt, 
					   int maxOccluders, ScalarV_In threshold );




class GeoChunkManager
{	

	grcIndexBuffer*				m_indexBuffers[GeoChunk::MaxDepth];
	grcIndexBuffer*				m_stripIndexBuffers[GeoChunk::MaxDepth];
	int							m_numOccIndices;
	u8*							m_occIndices;
	grmShader*					m_terraShader;
	int							m_Lod;
	grcVertexDeclaration*		m_vertexDeclaration;
	grcFvf						m_fvf;

	void CreateOccPatchIndices();
	grcIndexBuffer* CreateIndexBuffer( int size, int maxsize );
	grcIndexBuffer* CreateStripIndexBuffer( int size, int maxsize );

public:
	static const int StripLength = 8;

	GeoChunkManager() : m_vertexDeclaration(0), m_Lod(2){}

	grcFvf& GetFvf(){ return m_fvf; }

	void Setup(); 

	grcIndexBuffer** GetParametricIndexBuffers() { return m_indexBuffers; }
	grcIndexBuffer** GetStripIndexBuffers() { return m_stripIndexBuffers; }
	u8*			 GetParametricOccluderIndexBuffer() { return m_occIndices; }
	int				 GetNumOccluderIndices() { return m_numOccIndices; }


	~GeoChunkManager();

	void Draw( VisObj* chunks, int amt );
	void Draw( grmShader* shader,  grcEffectTechnique tech, int /*lod*/,  VisObj* chunks, int amt  );

#if __BANK
	void AddWidgets(bkBank& bank);
#endif
};



int NoiseTerrain(int xpos, int ypos, int width, Vec4V* __restrict verts, int vstride  );
int NoiseRock(int xpos, int ypos, int width, Vec4V* __restrict verts, int vstride );
int NoiseGrass(int xpos, int ypos, int width, Vec4V* __restrict verts, int /*vstride*/ );

Vec4V_Out Fbm2V( Vec4V_In  px, Vec4V_In py, Vec4V_In persistence, int Octaves );
};

#endif