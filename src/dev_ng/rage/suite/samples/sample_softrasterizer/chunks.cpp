


#include "chunks.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shader.h"
#include "softrasterizer/scan.h"
#include "softrasterizer/softrasterizer.h"
#include "grcore/viewport.h"
#include "math/random.h"

namespace rage {
	PS3_ONLY(extern u32 g_AllowVertexBufferVramLocks );
};


namespace rage
{
	

template< int SA>
Vec4V V4VShiftLeft( Vec4V_In a )
{
#if __WIN32PC
	__m128i b = *(reinterpret_cast<const __m128i*>(&a));
	b = _mm_slli_epi32( b, SA );
	return Vec4V(*(reinterpret_cast<__m128*>(&b)));
#else
	Vec4V SAV = Vec4V(Vec::V4VConstant<SA, SA, SA, SA>());
#if __PPU
	return  Vec4V((Vec::Vector_4V)vec_sl( (vec_uint4)a.GetIntrin128(), (vec_uint4)SAV.GetIntrin128()));
#else
	return Vec4V(__vslw(a.GetIntrin128(), SAV.GetIntrin128() ));
#endif

#endif
}
template< int SA>
Vec4V V4VShiftRight( Vec4V_In a )
{
#if __WIN32PC
	__m128i b = *(reinterpret_cast<const __m128i*>(&a));
	b = _mm_srli_epi32( b, SA );
	return Vec4V(*(reinterpret_cast<__m128*>(&b)));
#else
	Vec4V SAV = Vec4V(Vec::V4VConstant<SA, SA, SA, SA>());

#if __PPU
	return  Vec4V((Vec::Vector_4V)vec_sr( (vec_uint4)a.GetIntrin128(), (vec_uint4)SAV.GetIntrin128()));
#else
	return Vec4V(__vsrw(a.GetIntrin128(), SAV.GetIntrin128() ));
#endif

#endif
}

__forceinline Vec4V_Out AddIntV( Vec4V_In a, Vec4V_In b  ) 
{
	return Vec4V(Vec::V4AddInt(a.GetIntrin128(),b.GetIntrin128()));
}
__forceinline Vec4V_Out VFloor( Vec4V_In a ) 
{
	return RoundToNearestIntZero(a); //RoundToNearestIntNegInf(a);
}
//-------------------------------------
// Fast Noise
//----------------------------------------
__forceinline Vec4V_Out TEA( Vec4V_In v0i, Vec4V_In v1i  ) 
{
	Vec4V delta= Vec4V(Vec::V4VConstant<0x9e3779b9, 0x9e3779b9, 0x9e3779b9, 0x9e3779b9>());
	Vec4V delta2= Vec4V(Vec::V4VConstant<0x3c6ef372, 0x3c6ef372, 0x3c6ef372, 0x3c6ef372>());
	Vec4V delta3= Vec4V(Vec::V4VConstant<0xdaa66d2b, 0xdaa66d2b, 0xdaa66d2b, 0xdaa66d2b>());
	Vec4V k0 = Vec4V(Vec::V4VConstant<0xA341316C, 0xA341316C, 0xA341316C, 0xA341316C>());
	Vec4V k1 = Vec4V(Vec::V4VConstant<0xC8013EA4, 0xC8013EA4, 0xC8013EA4, 0xC8013EA4>());
	Vec4V k2 = Vec4V(Vec::V4VConstant<0xAD90777D, 0xAD90777D, 0xAD90777D, 0xAD90777D>());
	Vec4V k3 = Vec4V(Vec::V4VConstant<0x7E95761E, 0x7E95761E, 0x7E95761E, 0x7E95761E>());
	Vec4V mask = Vec4V(Vec::V4VConstant<0x0000002F, 0x0000002F, 0x0000002F, 0x0000002F>());


	// TODO : use 16 bit add to half instruction cost
	Vec4V v0 = AddIntV( v0i, V4VShiftLeft<16>(v0i));
	Vec4V v1 = AddIntV( v1i, V4VShiftLeft<16>(v1i));

	v0 = AddIntV( v0, AddIntV(V4VShiftLeft<4>(v1),k0) ^ AddIntV(v1,delta) ^ AddIntV( V4VShiftRight<5>(v1),k1) );
	v1 = AddIntV( v1, AddIntV(V4VShiftLeft<4>(v0),k2) ^ AddIntV(v0,delta) ^ AddIntV( V4VShiftRight<5>(v0),k3) );

	v0 = AddIntV( v0, AddIntV(V4VShiftLeft<4>(v1),k0) ^ AddIntV(v1,delta2)^ AddIntV( V4VShiftRight<5>(v1),k1) );
	v1 = AddIntV( v1, AddIntV(V4VShiftLeft<4>(v0),k2) ^ AddIntV(v0,delta2) ^ AddIntV( V4VShiftRight<5>(v0),k3) );

	v0 = AddIntV( v0, AddIntV(V4VShiftLeft<4>(v1),k0) ^ AddIntV(v1,delta3)^ AddIntV( V4VShiftRight<5>(v1),k1) );

	return IntToFloatRaw<5>( v1& mask) - Vec4V(V_ONE);
}
//-------------------------------------
// Fast Noise
//----------------------------------------
__forceinline Vec4V_Out TEA( Vec4V_In ai, Vec4V_In bi,  Vec4V_In ci  ) 
{
	Vec4V delta= Vec4V(Vec::V4VConstant<0x9e3779b9, 0x9e3779b9, 0x9e3779b9, 0x9e3779b9>());
	Vec4V delta2= Vec4V(Vec::V4VConstant<0x3c6ef372, 0x3c6ef372, 0x3c6ef372, 0x3c6ef372>());
	Vec4V delta3= Vec4V(Vec::V4VConstant<0xdaa66d2b, 0xdaa66d2b, 0xdaa66d2b, 0xdaa66d2b>());
	Vec4V k0 = Vec4V(Vec::V4VConstant<0xA341316C, 0xA341316C, 0xA341316C, 0xA341316C>());
	Vec4V k1 = Vec4V(Vec::V4VConstant<0xC8013EA4, 0xC8013EA4, 0xC8013EA4, 0xC8013EA4>());
	Vec4V k2 = Vec4V(Vec::V4VConstant<0xAD90777D, 0xAD90777D, 0xAD90777D, 0xAD90777D>());
	Vec4V k3 = Vec4V(Vec::V4VConstant<0x7E95761E, 0x7E95761E, 0x7E95761E, 0x7E95761E>());
	Vec4V mask = Vec4V(Vec::V4VConstant<0x0000002F, 0x0000002F, 0x0000002F, 0x0000002F>());


	// TODO : use 16 bit add to half instruction cost
	Vec4V v0 = AddIntV( ai, V4VShiftLeft<16>(ci));
	Vec4V v1 = AddIntV( bi, V4VShiftLeft<16>(ci));

	v0 = AddIntV( v0, AddIntV(V4VShiftLeft<4>(v1),k0) ^ AddIntV(v1,delta) ^ AddIntV( V4VShiftRight<5>(v1),k1) );
	v1 = AddIntV( v1, AddIntV(V4VShiftLeft<4>(v0),k2) ^ AddIntV(v0,delta) ^ AddIntV( V4VShiftRight<5>(v0),k3) );

	v0 = AddIntV( v0, AddIntV(V4VShiftLeft<4>(v1),k0) ^ AddIntV(v1,delta2)^ AddIntV( V4VShiftRight<5>(v1),k1) );
	v1 = AddIntV( v1, AddIntV(V4VShiftLeft<4>(v0),k2) ^ AddIntV(v0,delta2) ^ AddIntV( V4VShiftRight<5>(v0),k3) );

	v0 = AddIntV( v0, AddIntV(V4VShiftLeft<4>(v1),k0) ^ AddIntV(v1,delta3)^ AddIntV( V4VShiftRight<5>(v1),k1) );

	return IntToFloatRaw<5>( v1& mask) - Vec4V(V_ONE);
}
__forceinline Vec4V_Out Mad(  Vec4V_In x, Vec4V_In y, Vec4V_In z ) 
{
	return AddScaled( z, x, y);
}
__forceinline Vec3V_Out Mad(  Vec3V_In x, Vec3V_In y, Vec3V_In z ) 
{
	return AddScaled( z, x, y);
}
__forceinline Vec4V_Out cellNoise2V(  Vec4V_In x, Vec4V_In y) 
{
	Vec4V i = VFloor(x);
	Vec4V j = VFloor(y);
	return TEA( FloatToIntRaw<0>(i), FloatToIntRaw<0>(j) );
}
__forceinline Vec4V_Out noise2V(  Vec4V_In x, Vec4V_In y ) 
{
	static Vec4V thirty = Vec4V(ScalarVFromF32(30.0f));
	static Vec4V two = Vec4V(ScalarVFromF32(2.0f));
	static Vec4V ten = Vec4V(ScalarVFromF32(10.0f));
	static Vec4V neg15 = Vec4V(ScalarVFromF32(-15.0f));
	static Vec4V six = Vec4V(ScalarVFromF32(6.0f));

	Vec4V one=Vec4V(Vec::V4VConstant<1, 1, 1, 1>());
	Vec4V onef = Vec4V(V_ONE);
	Vec4V i = VFloor(x);
	Vec4V j = VFloor(y);
	Vec4V u = x -i;
	Vec4V v = y -j;

	i =  FloatToIntRaw<0>(i);
	j =  FloatToIntRaw<0>(j);
	Vec4V u2 = u*u;
	Vec4V v2 = v *v;
	u = u2*u*Mad(u,Mad(u,six, neg15),ten);
	v = v2*v*Mad(v,Mad(v,six, neg15),ten);

	const Vec4V i1 = AddIntV( i ,one);
	const Vec4V j1 = AddIntV( j ,one);

	const Vec4V a = TEA( i, j );
	const Vec4V b = TEA( i1, j );
	const Vec4V c = TEA( i, j1 );
	const Vec4V d = TEA( i1, j1 );

	const Vec4V k0 =   a;
	const Vec4V k1 =   b - a;
	const Vec4V k2 =   c - a;
	const Vec4V k4 =   a - b - c + d;

	return k0 + k1*u + k2*v + k4*u*v;
}
__forceinline Vec4V_Out noise3V(  Vec4V_In x, Vec4V_In y , Vec4V_In z) 
{
	static Vec4V thirty = Vec4V(ScalarVFromF32(30.0f));
	static Vec4V two = Vec4V(ScalarVFromF32(2.0f));
	static Vec4V ten = Vec4V(ScalarVFromF32(10.0f));
	static Vec4V neg15 = Vec4V(ScalarVFromF32(-15.0f));
	static Vec4V six = Vec4V(ScalarVFromF32(6.0f));

	Vec4V one=Vec4V(Vec::V4VConstant<1, 1, 1, 1>());
	Vec4V onef = Vec4V(V_ONE);
	Vec4V i = VFloor(x);
	Vec4V j = VFloor(y);
	Vec4V k = VFloor(z);
	Vec4V u = x -i;
	Vec4V v = y -j;
	Vec4V w = z -k;

	i =  FloatToIntRaw<0>(i);
	j =  FloatToIntRaw<0>(j);
	k =  FloatToIntRaw<0>(k);

	Vec4V u2 = u*u;
	Vec4V v2 = v *v;
	Vec4V w2 = w *w;
	u = u2*u*Mad(u,Mad(u,six, neg15),ten);
	v = v2*v*Mad(v,Mad(v,six, neg15),ten);
	w = w2*w*Mad(w,Mad(w,six, neg15),ten);

	const Vec4V i_1 = AddIntV( i ,one);
	const Vec4V j_1 = AddIntV( j ,one);
	const Vec4V k_1 = AddIntV( k ,one);

	const Vec4V a = TEA( i,   j ,  k );
	const Vec4V b = TEA( i_1, j,   k );
	const Vec4V c = TEA( i,   j_1, k );
	const Vec4V d = TEA( i_1, j_1, k );
	const Vec4V e = TEA( i,   j ,  k_1 );
	const Vec4V f = TEA( i_1, j,   k_1 );
	const Vec4V g = TEA( i,   j_1, k_1 );
	const Vec4V h = TEA( i_1, j_1, k_1 );

	const Vec4V k0 =   a;
	const Vec4V k1 =   b - a;
	const Vec4V k2 =   c - a;
	const Vec4V k3 =   e - a;
	const Vec4V k4 =   a - b - c + d;
	const Vec4V k5 =   a - c - e + g;
	const Vec4V k6 =   a - b - e + f;
	const Vec4V k7 = - a + b + c - d + e - f - g + h;

	// Mad formulation may be faster
	return k0 + k1*u + k2*v + k3*w + k4*u*v + k5*v*w + k6*w*u + k7*u*v*w;
}
__forceinline Vec4V_Out dnoise2V(  Vec4V_In x, Vec4V_In y, Vec4V_InOut rdu, Vec4V_InOut rdv ) 
{
	static Vec4V thirty = Vec4V(ScalarVFromF32(30.0f));
	static Vec4V two = Vec4V(ScalarVFromF32(2.0f));
	static Vec4V ten = Vec4V(ScalarVFromF32(10.0f));
	static Vec4V neg15 = Vec4V(ScalarVFromF32(-15.0f));
	static Vec4V six = Vec4V(ScalarVFromF32(6.0f));

	Vec4V one=Vec4V(Vec::V4VConstant<1, 1, 1, 1>());
	Vec4V onef = Vec4V(V_ONE);
	Vec4V i = VFloor(x);
	Vec4V j = VFloor(y);
	Vec4V u = x -i;
	Vec4V v = y -j;

	i =  FloatToIntRaw<0>(i);
	j =  FloatToIntRaw<0>(j);
	Vec4V u2 = u*u;
	Vec4V v2 = v *v;
	const Vec4V du = thirty*u2*Mad(u,(u-two),onef);
	const Vec4V dv = thirty*v2*Mad(v,(v-two),onef);

	u = u2*u*Mad(u,Mad(u,six, neg15),ten);
	v = v2*v*Mad(v,Mad(v,six, neg15),ten);

	const Vec4V i1 = AddIntV( i ,one);
	const Vec4V j1 = AddIntV( j ,one);

	const Vec4V a = TEA( i, j );
	const Vec4V b = TEA( i1, j );
	const Vec4V c = TEA( i, j1 );
	const Vec4V d = TEA( i1, j1 );

	const Vec4V k0 =   a;
	const Vec4V k1 =   b - a;
	const Vec4V k2 =   c - a;
	const Vec4V k4 =   a - b - c + d;

	rdu = du * Mad(k4,v, k1);
	rdv = dv * Mad(k4,u, k2);
	return k0 + k1*u + k2*v + k4*u*v;
}
Vec4V_Out Fbm2V( Vec4V_In  px, Vec4V_In py, Vec4V_In persistence, int Octaves )
{
	Vec4V weight = persistence;
	Vec4V x = px;
	Vec4V y = py;
	Vec4V separation = Vec4V(V_TWO);
	Vec4V accumulator = noise2V( x, y);

	for(int i = 0; i < Octaves-1; i++)
	{
		x *= separation;
		y *= separation;
		accumulator += noise2V( x, y) * weight;
		weight *= persistence;
	}
	return accumulator;
}
template<int Octaves>
Vec4V_Out Fbm3V( Vec4V_In  px, Vec4V_In py, Vec4V_In pz, Vec4V_In persistence)
{
	Vec4V weight = persistence;
	Vec4V x = px;
	Vec4V y = py;
	Vec4V z = pz;
	Vec4V separation = Vec4V(V_TWO);
	Vec4V accumulator = noise3V( x, y, z);

	for(int i = 0; i < Octaves-1; i++)
	{
		x *= separation;
		y *= separation;
		z *= separation;
		accumulator += noise3V( x, y, z) * weight;
		weight *= persistence;
	}
	return accumulator;
}
template<int Octaves>
Vec4V_Out Fbm2NormV( Vec4V_In  px, Vec4V_In py, Vec4V_In persistence,
					Vec4V_InOut nx, Vec4V_InOut nz )
{
	Vec4V weight = persistence;
	Vec4V x = px;
	Vec4V y = py;
	Vec4V separation = Vec4V(V_TWO);
	Vec4V accumulator = noise2V( x, y);

	Vec4V du = Vec4V(V_ZERO);
	Vec4V dv = Vec4V(V_ZERO);

	for(int i = 0; i < Octaves-1; i++)
	{
		x *= separation;
		y *= separation;

		Vec4V ddu, ddv;
		accumulator += dnoise2V( x, y, ddu, ddv) * weight;
		du += ddu * weight;
		dv += ddv * weight;
		weight *= persistence;
	}

	nx = du;
	nz = dv;
	return accumulator;
}


void GeoChunk::Create( int x, int y, GenFunc func, grcIndexBuffer** buffers, u8* occIndices, int numIndices )
{
	m_x = x;
	m_y = y;
	m_genFunc = func;

	// calculate approximate bounds
	Vec4V verts[4*4*2];
	ASSERT_ONLY(int amt =)  func( x*(ChunkWidth), y*(ChunkWidth), 4, verts,2 );
	Assert( amt == 4*4*2);
	CalcBounds( m_bmin, m_bmax, verts, 4* 4);
	m_indexBuffers = buffers;

	m_occludeVerts = 0;
	m_numOccludeIndices = 0;
	m_occIndices= 0;
	if ( occIndices )
	{
		int occDepth = OccluderDepth; 
		m_occludeVerts = rage_new Vec3V[occDepth*occDepth];
		Vec4V* verts = rage_new Vec4V[occDepth*occDepth*2];

		ASSERT_ONLY(int amt =) func( x*(ChunkWidth), y*(ChunkWidth), occDepth, verts,2 );			
		Assert( amt == occDepth*occDepth*2);
		for (int i =0; i < occDepth*occDepth; i++)
		{
			m_occludeVerts[i]=verts[i*2].GetXYZ();
		}
		// get better bounds
		CalcBounds( m_bmin, m_bmax, verts, occDepth*occDepth);
		m_numOccludeIndices = numIndices;
		for (int i =0; i < numIndices; i++)
			Assign(m_occIndices, occIndices);
		m_numOccludeVerts = occDepth*occDepth;

		delete [] verts;
	}
}

void GeoChunk::ActualCreate( int x, int y, GenFunc func, grcFvf& fvf )
{
	m_vertexBuffer = grcVertexBuffer::Create( NumChunkVerts, fvf, true,false,NULL);
	if (AssertVerify(m_vertexBuffer))
	{
		PS3_ONLY( g_AllowVertexBufferVramLocks = true; )

			m_vertexBuffer->LockRW();
		Vec4V* verts = reinterpret_cast<Vec4V*>( m_vertexBuffer->GetLockPtr() );

		if ( AssertVerify( verts))
		{
			int amt =func( x*(ChunkWidth), y*(ChunkWidth), ChunkWidth, verts,2 );
			Assert( amt == NumChunkVerts*2);
			CalcBounds( m_bmin, m_bmax, verts, amt/2);
		}
		m_vertexBuffer->UnlockRW();
		PS3_ONLY( g_AllowVertexBufferVramLocks = false; )

	}
}

void GeoChunk::Draw( int lod ) const
{
	if (AssertVerify(m_vertexBuffer))
	{
		Assert( lod >=0 && lod <GeoChunk::MaxDepth-2);

		GRCDEVICE.SetIndices(*m_indexBuffers[lod]);			
		GRCDEVICE.SetStreamSource(0,*m_vertexBuffer, 0, m_vertexBuffer->GetVertexStride() );
		GRCDEVICE.DrawIndexedPrimitive( drawTris, 0,m_indexBuffers[lod]->GetIndexCount());
	}
}

void GeoChunk::CalcBounds( Vec3V_InOut rbmin, Vec3V_InOut rbmax, Vec4V* __restrict verts, int amt )
{
	Vec4V bmin = Vec4V(V_FLT_MAX);
	Vec4V bmax = Vec4V(V_NEG_FLT_MAX);
	for (int i =0; i < amt; i++)
	{
		bmin = Min( verts[i*2], bmin);
		bmax = Max( verts[i*2], bmax);
	}
	rbmin = bmin.GetXYZ();
	rbmax = bmax.GetXYZ();
}

void GeoChunk::FreeHiModel()
{
	delete m_vertexBuffer;
	m_vertexBuffer = 0;
}

GeoChunk::~GeoChunk()
{
	FreeHiModel();
	delete [] m_occludeVerts;
}

void ProceduralStreamer::AddStreamJob( GeoChunk* chunk, float area )
{
	for (int i = 0; i < m_streamqueue.GetCount(); i++)
	{
		if ( m_streamqueue[i].first > area)
		{
			m_streamqueue.Insert( i) = std::make_pair(area, chunk);
			return;
		}
	}
	m_streamqueue.Append()= std::make_pair(area, chunk);
}

ProceduralStreamer::ProceduralStreamer() : m_LoadThreshold(1.0f), m_UnloadThreshold(0.8f), m_maxStreamJobs(1)
{
	m_streamqueueSize = 0;
	m_numUnloaded = 0;
}

void ProceduralStreamer::Create( int size )
{
	m_unloadedList.Reserve(size);
	m_streamqueue.Reserve(size);
}

void ProceduralStreamer::Add( GeoChunk* chunk )
{
	m_unloadedList.Append()=chunk;
}

void ProceduralStreamer::StreamOutAssets( atArray<GeoChunk*>& actualList )
{
	rstSplatMat44V splatMtx;

	Vec3V minBounds = Vec3V(V_ZERO);
	Vec3V maxBounds;
	ScalarV rescaleDepth = rstCreateCurrentSplatMat44V( grcViewport::GetCurrent(), splatMtx, maxBounds);

	int cnt = actualList.GetCount();
	for (int i = 0; i < cnt; )
	{
		Vec3V scbmin;
		Vec3V scbmax;

		rstCalculateScreenSpaceBound(	splatMtx, rescaleDepth,
			actualList[i]->m_bmin,actualList[i]->m_bmax,scbmin, scbmax);  // pipeline these two			

		// clip to screen first
		scbmax = Clamp( scbmax, minBounds, maxBounds);
		scbmin = Clamp( scbmin, minBounds, maxBounds);

		VecBoolV isVis =  VecBoolV(V_T_T_T_T) ;//rstScanQueryHiZLarge( HiZ, scbmin, scbmax );

		// calculate lod
		Vec3V diff = scbmax - scbmin;
		ScalarV area = diff.GetX() + diff.GetY();

		// do selection on a threshold on m_unloadList; 
		if ( Unlikely(area.Getf()  < m_UnloadThreshold ) )
		{

			actualList[i]->FreeHiModel();
			m_unloadedList.Append() = actualList[i];
			actualList.DeleteFast(i);
			cnt--;
		}
		else
		{
			i++;
		}
	}
}

void ProceduralStreamer::FindNewAssestToGenerate()
{
	rstSplatMat44V splatMtx;
	Vec3V maxBounds;
	Vec3V minBounds = Vec3V(V_ZERO);
	ScalarV rescaleDepth = rstCreateCurrentSplatMat44V( grcViewport::GetCurrent(), splatMtx, maxBounds);
	//ScalarV	loadThreshold = ScalarVFromF32(m_LoadThreshold);

	int cnt = m_unloadedList.GetCount();
	for (int i = 0; i < cnt; )
	{
		Vec3V scbmin;
		Vec3V scbmax;

		rstCalculateScreenSpaceBound(	splatMtx, rescaleDepth,
			m_unloadedList[i]->m_bmin,m_unloadedList[i]->m_bmax,scbmin, scbmax);  // pipeline these two			
		VecBoolV isVis =  VecBoolV(V_T_T_T_T) ;//rstScanQueryHiZLarge( HiZ, scbmin, scbmax );

		// clip to screen first
		scbmax = Clamp( scbmax, minBounds, maxBounds);
		scbmin = Clamp( scbmin, minBounds, maxBounds);

		// calculate lod
		Vec3V diff = scbmax - scbmin;
		ScalarV area = diff.GetX() + diff.GetY();

		// do selection on a threshold on m_unloadList; 
		if ( Unlikely(area.Getf()  > m_LoadThreshold ) )
		{
			AddStreamJob( m_unloadedList[i], area.Getf() );
			m_unloadedList.DeleteFast(i);
			cnt--;
		}
		else
		{
			i++;
		}
	}
}

void ProceduralStreamer::ExectuteAnyPendingJobs( atArray<GeoChunk*>& actualList, grcFvf& fvf )
{
	FindNewAssestToGenerate();


	m_streamqueueSize = m_streamqueue.GetCount();
	m_numUnloaded = m_unloadedList.GetCount();

	int numJobs = Min( m_streamqueue.GetCount(), m_maxStreamJobs);

	int start = m_streamqueue.GetCount() - numJobs;
	for (int i = start; i< m_streamqueue.GetCount(); i++)  // TODO : add a task
	{
		m_streamqueue[i].second->Generate( fvf);
	}
	// clear it out
	for (int i = 0; i< numJobs; i++)
	{
		actualList.Append()= m_streamqueue.back().second;
		m_streamqueue.Pop();
	}
}
#if __BANK
void ProceduralStreamer::AddWidgets( bkBank& bank )
{
	bank.PushGroup("Procedural Streaming");
	bank.AddSlider("m_maxStreamJobs", &m_maxStreamJobs, 0,30, 1);
	bank.AddSlider("m_UnloadThreshold", &m_UnloadThreshold, 0.0f, 100.0f, 0.01f);
	bank.AddSlider("m_LoadThreshold", &m_LoadThreshold, 0.0f, 100.0f, 0.01f);

	bank.AddSlider("Number Unloaded", &m_numUnloaded, 0,10000,1);
	bank.AddSlider("m_streamqueueSize", &m_streamqueueSize,  0,10000,1);
	bank.PopGroup();
}
#endif

int rstCreateVisibleList(  GeoChunk**  clist, VisObj* __restrict vislist, int amt,	const Vec4V* HiZ,
						 ScalarV_In smallThreshold, float lodFactor, bool useOcclusion, Vec3V* rbmin, Vec3V *rbmax )

{
	rstSplatMat44V splatMtx;
	Vec3V maxBounds;
	ScalarV rescaleDepth = rstCreateCurrentSplatMat44V( grcViewport::GetCurrent(), splatMtx,maxBounds);
	int cnt =  0;
	Vec3V minBounds(0.f,0.f,-100.f);
	ScalarV zero(V_ZERO);

	for (int i = 0; i < amt; i++)
	{
		Vec3V scbmin;
		Vec3V scbmax;

		rstCalculateScreenSpaceBound(	splatMtx, rescaleDepth,clist[i]->m_bmin,clist[i]->m_bmax,scbmin, scbmax);  // pipeline these two
		vislist[cnt].chunk = clist[i];
		VecBoolV isVis(V_T_T_T_T);

		if ( useOcclusion)
			isVis = VecBoolV(rstScanQueryCountHiZLarge( HiZ, scbmin, scbmax ) > ScalarV(V_ZERO));


		isVis = isVis | VecBoolV( scbmin.GetZ() < zero ); // if clip front plane depth will always be in front
		isVis = isVis & VecBoolV( scbmax.GetZ() > zero); // but if behind front plane then chuck it

		// clip to screen first
		scbmax = Clamp( scbmax, minBounds, maxBounds);
		scbmin = Clamp( scbmin, minBounds, maxBounds);

		// calculate lod
		Vec3V diff = scbmax - scbmin;
		ScalarV area = diff.GetX() + diff.GetY();
		isVis = isVis & VecBoolV(area >  smallThreshold);

		vislist[cnt].lod = (int)(Max( 0.0f, floor( (float)(GeoChunk::MaxDepth-2) - area.Getf()/ lodFactor)));


		int r = IsEqualIntAll( isVis,  VecBoolV(V_T_T_T_T) );

		rbmin[i]=scbmin;
		rbmax[i]=scbmax;
		rbmax[i].SetWi(r);
		cnt += r;
	}
	return cnt;
}

int rstSelectOccluders( GeoChunk** __restrict clist,  GeoChunk** __restrict occlist, u8* isClipped, int amt, 
					   int maxOccluders, ScalarV_In threshold )
{
	rstSplatMat44V splatMtx;
	Vec3V maxBounds;
	Vec3V minBounds(0.f,0.f,-100.f);
	ScalarV rescaleDepth = rstCreateCurrentSplatMat44V( grcViewport::GetCurrent(), splatMtx, maxBounds);
	int cnt =  0;
	float eps = 0.001f;
	ScalarV clipPlane = ScalarVFromF32(eps);

	for (int i = 0; i < amt; i++)
	{
		Vec3V scbmin;
		Vec3V scbmax;

		rstCalculateScreenSpaceBound(	splatMtx, rescaleDepth,clist[i]->m_bmin,
			clist[i]->m_bmax,scbmin, scbmax);  // pipeline these two

		// clip to screen first
		scbmax = Clamp( scbmax, minBounds, maxBounds);
		scbmin = Clamp( scbmin, minBounds, maxBounds);

		// calculate lod
		Vec3V diff = scbmax - scbmin;
		isClipped[cnt] = (u8)IsLessThanOrEqualAll( scbmin.GetZ(), clipPlane);
		ScalarV area = diff.GetX() * diff.GetY();
		occlist[cnt] = clist[i];
		cnt += IsTrue( area > threshold)&& clist[i]->m_numOccludeIndices>0;		
		cnt = Min( cnt, maxOccluders);		
	}
	return cnt;
}

void GeoChunkManager::CreateOccPatchIndices()
{
	int size = GeoChunk::OccluderDepth;
	int quads = size-1;
	m_numOccIndices = quads * quads*6;
	m_occIndices = rage_new u8[m_numOccIndices];

	u8* buffer = m_occIndices;
	int idx = 0;
	for (int u = 0; u < quads; u++)
	{
		for(int v = 0; v < quads; v++)
		{
			// add quad
			u8 qidx = (u8)(u  + v*size);

			buffer[idx++]= qidx;				
			buffer[idx++]= qidx+(u8)size;
			buffer[idx++]= qidx+1;

			buffer[idx++]= qidx+(u8)size;				
			buffer[idx++]= qidx+(u8)size+1;
			buffer[idx++]= qidx+1;
		}

	}
	Assert( idx == m_numOccIndices);
}

grcIndexBuffer* GeoChunkManager::CreateIndexBuffer( int size, int maxsize )
{
	int quads = size;
	u16	 stepSize = (u16) maxsize/(u16) quads;
	bool shorten = true;
	u16 shortenX = 1;
	if ( size == maxsize)
	{
		quads = size-1;
		stepSize = 1;
		shorten = false;
		shortenX = 0;
	}

	int numIndices = quads * quads*6;
	grcIndexBuffer* idxBuffer = grcIndexBuffer::Create( numIndices );
	u16* buffer = idxBuffer->LockRW();

	u16					stepx = (u16)stepSize;
	int idx =0;

	for (int u = 0; u < quads; u++)
	{

		u16		stepy = (u16)stepSize*(u16) maxsize;
		u16		istepy = stepy;
		if ( u == quads-1 && shorten)
		{
			istepy =  (u16)(stepSize-1)*(u16) maxsize;
		}
		for(int v = 0; v < quads-1; v++)
		{
			// add quad
			u16 qidx = (u16)(u * stepy) + (u16)v*stepx;

			buffer[idx++]= qidx;				
			buffer[idx++]= qidx+istepy;
			buffer[idx++]= qidx+stepx;

			buffer[idx++]= qidx+istepy;				
			buffer[idx++]= qidx+istepy+stepx;
			buffer[idx++]= qidx+stepx;
		}
		u16 qidx = (u16)(u * stepy) + (u16)(quads-1)*stepx;
		buffer[idx++]= qidx;				
		buffer[idx++]= qidx+istepy;
		buffer[idx++]= qidx+stepx-shortenX;

		buffer[idx++]= qidx+istepy;				
		buffer[idx++]= qidx+istepy+stepx-shortenX;
		buffer[idx++]= qidx+stepx-shortenX;
	}
	Assert( idx == numIndices);
	idxBuffer->UnlockRW();
	return idxBuffer;
}

grcIndexBuffer* GeoChunkManager::CreateStripIndexBuffer( int size, int maxsize )
{
	int curveHieght = Max( (StripLength*size )/maxsize,2);
	int numStrips = size * 4;
	int numIndices = numStrips * (curveHieght-1)*6;
	grcIndexBuffer* idxBuffer = grcIndexBuffer::Create( numIndices );
	u16* buffer = idxBuffer->LockRW();

	u16				stepx = (u16)((int)StripLength/curveHieght) *2;
	int idx =0;

	for (int u = 0; u < numStrips; u++)
	{
		u16 istepy = 1;
		u16 stepy = ((StripLength)*2);
		for(int v = 0; v < curveHieght-1; v++)
		{
			// add quad
			int acc = Min( v * stepx, (StripLength-1)*2);
			u16 qidx = (u16)(u * stepy) + (u16)acc;

			buffer[idx++]= qidx;				
			buffer[idx++]= qidx+istepy;
			buffer[idx++]= qidx+stepx;

			buffer[idx++]= qidx+istepy;				
			buffer[idx++]= qidx+istepy+stepx;
			buffer[idx++]= qidx+stepx;
		}			
	}
	Assert( idx == numIndices);
	idxBuffer->UnlockRW();
	return idxBuffer;
}

void GeoChunkManager::Setup()
{
	PS3_ONLY( g_AllowVertexBufferVramLocks = true; )

		m_fvf.SetPosChannel( true, grcFvf::grcdsFloat4);
	m_fvf.SetDiffuseChannel(false);
	m_fvf.SetTextureChannel( 0, true, grcFvf::grcdsFloat4);

	grcVertexElement elements[] =
	{
		grcVertexElement( 0, grcVertexElement::grcvetPosition, 0, sizeof(float) * 4, grcFvf::grcdsFloat4), 
		grcVertexElement( 0, grcVertexElement::grcvetTexture, 0, sizeof(float) * 4, grcFvf::grcdsFloat4)
	};
	m_vertexDeclaration = GRCDEVICE.CreateVertexDeclaration( elements, NELEM( elements ) );

	for (int i =0; i < GeoChunk::MaxDepth-1; i++)
	{
		m_indexBuffers[i]=CreateIndexBuffer( 1<<(GeoChunk::MaxDepth -i), 1<<GeoChunk::MaxDepth);
		m_stripIndexBuffers[i] = CreateStripIndexBuffer(1<<(GeoChunk::MaxDepth -i), 1<<GeoChunk::MaxDepth);

	}	
	CreateOccPatchIndices();

	m_terraShader = grmShaderFactory::GetInstance().Create();
	Assert(m_terraShader );
	ASSERT_ONLY(bool res = ) m_terraShader->Load("rage_terrain");
	Assert( res);		

	PS3_ONLY( g_AllowVertexBufferVramLocks = false; )
}

GeoChunkManager::~GeoChunkManager()
{
#if !__WIN32PC
	if ( m_vertexDeclaration )
		grmModelFactory::FreeDeclarator( m_vertexDeclaration );

	for (int i = 0; i < GeoChunk::MaxDepth-1; i++)
	{
		delete m_indexBuffers[i];
	}

	delete m_terraShader;
#endif
}

void GeoChunkManager::Draw( VisObj* chunks, int amt )
{
	Draw( m_terraShader, grcetNONE, m_Lod, chunks, amt);
}

void GeoChunkManager::Draw( grmShader* shader, grcEffectTechnique tech, int /*lod*/, VisObj* chunks, int amt )
{
	if (!shader->BeginDraw(grmShader::RMC_DRAW, true, tech)) 
	{
		shader->EndDraw();
		return;
	}
	shader->Bind();
	GRCDEVICE.SetVertexDeclaration( m_vertexDeclaration );

	for(int i = 0; i< amt; i++)
	{
		int aclod = chunks[i].lod;
		chunks[i].chunk->Draw(aclod); 
	}

	GRCDEVICE.ClearStreamSource(0);
	shader->UnBind();		
	shader->EndDraw();
}

#if __BANK
void GeoChunkManager::AddWidgets( bkBank& bank )
{
	bank.AddSlider("Lod", &m_Lod, 0,GeoChunk::MaxDepth-2,1);
}
#endif

Vec4V_Out GetTerrainHeight( Vec4V_In px, Vec4V_In py, Vec4V_InOut nx, Vec4V_InOut nz )
{
	static Vec4V persistence = Vec4V( ScalarVFromF32(0.55f) );
	static Vec4V nscale = Vec4V( ScalarVFromF32(1.0f/20.0f) );
	static Vec4V hieght = Vec4V(ScalarVFromF32(4.0f));
	//	return Fbm2V<8>( px * nscale,py* nscale, persistence) * hieght;
	Vec4V pz = Fbm2NormV<8>( px * nscale,py* nscale, Vec4V(persistence), nx, nz);
	return pz*hieght;

}



int NoiseTerrain(int xpos, int ypos, int width, Vec4V* __restrict verts, int vstride  )
{
	ScalarV persistence = ScalarVFromF32(0.55f);
	ScalarV hieght = ScalarVFromF32(4.0f);
	ScalarV nscale = ScalarVFromF32(1.0f/20.0f) ;

	ScalarV invhieght = ScalarVFromF32(1.0f/4.0f);
	Vec4V pscale = Vec4V(ScalarVFromF32(1.0f/4.0f)) ;
	Vec4V pscalesStep = Vec4V(ScalarVFromF32(32.0f/(float)(width-1)));


	float stx = 128.0f/(float)(width-1);
	Vec4V py = Vec4V(ScalarVFromF32((float)ypos)) * pscale ;
	Vec4V sx = Vec4V(ScalarVFromF32((float)xpos), ScalarVFromF32((float)xpos+stx*1.f),
		ScalarVFromF32((float)xpos+stx*2.f), ScalarVFromF32((float)xpos+stx*3.f)) ;

	sx *= pscale;

	Vec4V low =  Vec4V(0.2f, 0.1f, 0.01f,1.0f);
	Vec4V high =  Vec4V(0.8f, 0.9f, 0.7f,1.0f);
	Vec4V cliff =  Vec4V(0.4f, 0.4f, 0.5f,1.0f);
	Vec4V colorScale = Vec4V(ScalarVFromF32(100.0f));
	Vec4V colorWieght  = Vec4V(ScalarVFromF32(0.1f));

	for(int i = 0; i <width; i++)
	{

		Vec4V px = sx ;
		int idx = i * width*vstride;
		for (int j =0;j < width; j+=4)
		{
			Vec4V nx,nz;
			//Vec4V pz = Fbm2NormV<8>( px * nscale,py* nscale, Vec4V(persistence), nx, nz);
			Vec4V pz = GetTerrainHeight( px,py, nx,nz);

			Vec4V hc =Saturate(pz * invhieght * Vec4V(V_HALF) + Vec4V(V_HALF));
			//pz *= hieght;

			nx *= Vec4V(V_HALF);
			nz *= Vec4V(V_HALF);

			Vec4V ny = SqrtFast(Saturate( Vec4V(V_ONE) - ( nx * nx + nz * nz)));
			Vec4V c = ny;


			c = Saturate( c  + noise2V(  px * colorScale, py* colorScale ) * colorWieght ) ;

			// sizzle from 4x4 to one by one
			Vec4V t0 = MergeXY(px, py);
			Vec4V t1 = MergeXY(pz, Vec4V(V_ZERO));
			Vec4V t2 = MergeZW(px, py);
			Vec4V t3 = MergeZW(pz, Vec4V(V_ZERO));
			verts[idx +0] = MergeXY(t0, t1);
			verts[idx +2] = MergeZW(t0, t1);
			verts[idx +4] = MergeXY(t2, t3);
			verts[idx +6] = MergeZW(t2, t3);

			verts[idx +1] = Lerp( c.GetX(), cliff, Lerp( hc.GetX(), low, high));
			verts[idx +3] = Lerp( c.GetY(), cliff, Lerp( hc.GetY(), low, high));
			verts[idx +5] = Lerp( c.GetZ(), cliff, Lerp( hc.GetZ(), low, high));
			verts[idx +7] = Lerp( c.GetW(), cliff, Lerp( hc.GetW(), low, high));

			idx +=4*vstride;
			px += Vec4V(V_FOUR) * pscalesStep;

		}
		py += pscalesStep;
	}
	return width* width*vstride;

}


// a lot less along the horizontal axis
// Could use strip format  from the arc and curve constants

int NoiseRock(int xpos, int ypos, int width, Vec4V* __restrict verts, int vstride )
{
	ScalarV persistence = ScalarVFromF32(0.48f);
	Vec4V hieght = Vec4V( ScalarVFromF32(0.25f));
	Vec4V nscale = Vec4V( ScalarVFromF32(1.0f));

	Vec4V pscale = Vec4V(ScalarVFromF32(1.0f/4.0f)) ;
	Vec4V alpha = Vec4V(ScalarVFromF32(-PI/2.0f)) ;

	// calculate position as jittered in a grid
	
	
	// todo add option to indices to wrap on an axis
	Vec4V stepTheta = Vec4V( ScalarVFromF32(4.0f*2.0f*PI/(float)(width-1)) );
	Vec4V stepAlpha = Vec4V( ScalarVFromF32(PI/(float)(width-1)) );
	
	Vec4V low =  Vec4V(0.3f, 0.25f, 0.2f,1.0f);
	Vec4V high =  Vec4V(0.9f, 0.9f, 0.9f,1.0f);

	Vec4V radius = Vec4V(ScalarVFromF32(0.5f));
	Vec4V offset = Vec4V(ScalarVFromF32(257.0f)  );
	Vec4V basePos = Vec4V( (float)xpos/4.0f, (float)ypos/4.0f ,0.0f,0.0f );
	Vec4V offsetX = offset + Vec4V(basePos.GetX());
	Vec4V offsetY = offset + Vec4V(basePos.GetY());

	Vec4V randomOffset = cellNoise2V( basePos.Get<Vec::X,Vec::Y,Vec::X,Vec::Y>(),
										basePos.Get<Vec::Y,Vec::X,Vec::Y,Vec::X>());

	Vec4V halfWidth = ScalarVFromF32((float)128.0f/2.0f) * pscale;
	basePos += Mad(randomOffset, halfWidth ,halfWidth);

	Vec4V nx,nz;
	Vec4V basePosZ = GetTerrainHeight( Vec4V(basePos.GetX()), Vec4V(basePos.GetY()), nx,nz) + radius;

	Vec4V colorScale = Vec4V(ScalarVFromF32(2.0f));

	for(int i = 0; i <width; i++)
	{
		int idx = i * width*vstride;

		Vec4V pz = Sin( alpha);
		Vec4V cosAlpha = Cos( alpha);
		Vec4V theta = stepTheta * Vec4V(0.0f, 1.0f/4.0f,2.0f/4.0f,3.0f/4.0f);
		// make fatter at bottom
		Vec4V rad = radius * Mad( pz, Vec4V(V_ONE), Vec4V(V_ONE) );

		for (int j =0;j < width; j+=4)
		{			
			
			Vec4V rpx = Cos( theta) * cosAlpha ;
			Vec4V rpy = Sin( theta) * cosAlpha ;
			Vec4V rpz = pz ;

			Vec4V d = Fbm3V<7>( Mad(rpx, nscale, offsetX), Mad(rpy, nscale, offsetY), 
									Mad(rpz, nscale, offsetX),  Vec4V(persistence));

			Vec4V hc =Saturate( Mad( d , Vec4V(V_HALF) ,Vec4V(V_HALF)));

					
			
			d = Mad( d, hieght, rad );
			rpx = Mad( rpx, d, Vec4V(basePos.GetX()));
			rpy = Mad( rpy, d, Vec4V(basePos.GetY()));
			rpz = Mad(rpz, d , basePosZ);

			/*Vec4V hc2 = noise3V(  rpx * colorScale + offsetX, rpy* colorScale+ offsetX, rpz *colorScale+ offsetX ) * Vec4V(V_HALF) +   Vec4V(V_HALF);
			hc2 *= Max( pz, Vec4V(V_ZERO));

			hc = hc2;*/
			theta += stepTheta;

			// sizzle from 4x4 to one by one
			Vec4V t0 = MergeXY(rpx, rpy);
			Vec4V t1 = MergeXY(rpz, Vec4V(V_ZERO));
			Vec4V t2 = MergeZW(rpx, rpy);
			Vec4V t3 = MergeZW(rpz, Vec4V(V_ZERO));
			verts[idx +0] = MergeXY(t0, t1);
			verts[idx +2] = MergeZW(t0, t1);
			verts[idx +4] = MergeXY(t2, t3);
			verts[idx +6] = MergeZW(t2, t3);

			verts[idx +1] = Lerp( hc.GetX(), low, high);
			verts[idx +3] = Lerp( hc.GetY(), low, high);
			verts[idx +5] = Lerp( hc.GetZ(), low, high);
			verts[idx +7] = Lerp( hc.GetW(), low, high);

			idx +=4*vstride;
		}
		alpha += stepAlpha;
	}
	return width* width*vstride;
}


int NoiseGrass(int xpos, int ypos, int width, Vec4V* __restrict verts, int /*vstride*/ )
{
	// TODO - do four at a pop?
	mthRandom rnd;

	int	numBlades = width== 4 ? 4 : width * 8;
	int numSamples = width == 4 ? 2 : GeoChunkManager::StripLength;

	Vec3V pscale = Vec3V(ScalarVFromF32(1.0f/8.0f));
	Vec3V basePos = Vec3V( (float)xpos, 0.0f,(float)ypos ) * pscale;
	Vec3V psize =  Vec3V(ScalarVFromF32((float)GeoChunk::ChunkWidth)) * pscale;

	psize.SetY(ScalarV(V_ZERO));

	Vec3V vsize = Vec3V(ScalarVFromF32(0.3f));
	Vec3V vbase = Vec3V(ScalarVFromF32(0.0f));
	vsize.SetYf(0.2f);
	vbase.SetYf(0.4f);

	vbase -= vsize * Vec3V(V_HALF);

	Vec3V cbase = Vec3V(0.1f,0.2f, 0.15f );
	Vec3V csize = Vec3V(0.1f,0.2f, 0.15f );

	ScalarV startWidth = ScalarVFromF32(0.2f);
	ScalarV widthStep = ScalarVFromF32(0.2f/(float)(GeoChunkManager::StripLength-1));

	Vec3V gravity = Vec3V(0.0f,-0.05f, 0.0f );
	ScalarV floppiness = ScalarVFromF32(1.0f);
	Vec3V windVel = Vec3V(-0.001f,0.0f, -0.001f ) * floppiness;

	int idx = 0;
	Vec3V accel = gravity + windVel;

	Vec4V nx,nz;

	for(int i=0;i<numBlades;i++)
	{
		Vec3V	p = Mad(rnd.Get3FloatsV(), psize, basePos);
		Vec3V	vel = Mad(rnd.Get3FloatsV(), vsize, vbase);

		Vec3V	tangent = Normalize( Cross( Vec3V(V_Y_AXIS_WONE), Normalize(vel)) );

		p.SetY( GetTerrainHeight( Vec4V(p.GetX()), Vec4V(p.GetZ()),nx,nz).GetX());

		Vec3V gcol= Mad(rnd.Get3FloatsV(), Vec3V(0.8f,0.35f, 0.3f), Vec3V( 0.15f,0.65f, 0.2f));
		Vec3V sgcol= Mad(rnd.Get3FloatsV(), Vec3V(0.2f,0.3f, 0.1f), Vec3V( 0.0f,0.1f, 0.0f));

		gcol -= sgcol;
		gcol *= ScalarVFromF32(1.0f/(float)numSamples);


		ScalarV grassWidth= startWidth;

		// TODO - darken at the bottom5
		for(int j=0; j<numSamples; j++)
		{
			Vec3V acc = tangent * grassWidth;
			verts[idx] = Vec4V(p + acc,ScalarV(V_ONE));
			verts[idx+2]= Vec4V( p - acc,ScalarV(V_ONE));
			verts[idx+1] =Vec4V(sgcol,ScalarV(V_ONE));
			verts[idx+3] =Vec4V(sgcol,ScalarV(V_ONE));

			p += vel;
			vel += accel;
			grassWidth -= widthStep;		
			sgcol += gcol;

			idx +=4;
		}		
	}
	return idx;
}
}