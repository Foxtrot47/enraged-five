// 
// sample_cutscene/miscentity.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_CUTSCENE_MISCENTITY_H
#define SAMPLE_CUTSCENE_MISCENTITY_H

#include "sample_cutscene.h"

#include "atl/map.h"
#include "audioengine/entity.h"
#include "audiosoundtypes/sound.h"
#include "cutscene/cutsaudioentity.h"
#include "cutscene/cutsboundsentity.h"
#include "cutscene/cutsentity.h"
#include "script/thread.h"

using namespace rage;

namespace rage
{
    class audEntity;
}

namespace ragesamples
{

//##############################################################################

// PURPOSE: A class for an audio entity
class SampleCutsceneAudioEntity : public cutsAudioEntity
{
public:
    SampleCutsceneAudioEntity();
    SampleCutsceneAudioEntity( const cutfObject* pObject );
    virtual ~SampleCutsceneAudioEntity() {}

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL );

protected:
    // PURPOSE:  Returns the current audio time in seconds
    // PARAMS:
    //    fSeconds - the seconds that have elapsed since Prepare was called.
    //    iBuffer - the buffer currently playing
    // RETURNS: the play time in seconds, or -1.0f on error or if no audio is playing
    virtual float GetPlayTime( float fSeconds, int iBuffer );

    // PURPOSE:  Asynchronous call to request the slot acquisition and preloading of a cutscene audio asset.
    // PARAMS:
    //    pName - the name of the cut scene/audio stream
    //    fStartOffset - the offset, in seconds, at which to begin playback
    //    iBuffer - the buffer to prepare
    // RETURNS: The prepare state.
    virtual audPrepareState Prepare( const char *pName, float fStartOffset, int iBuffer );

    // PURPOSE:  Start the audio
    // PARAMS:
    //    iBuffer - the buffer to play
    // NOTES:  Calling code must ensure that the asset is prepared (ie Prepare() returns AUD_PREPARED)
    virtual void Play( int iBuffer );

    // PURPOSE:  Stop the audio and free resources.
    // PARAMS:
    //    iBuffer - the buffer to stop
    virtual void Stop( int iBuffer );

#if __BANK
    // PURPOSE: Mute the audio
    // PARAMS:
    //    iBuffer - the buffer to mute
    virtual void Mute( int iBuffer );

    // PURPOSE: Unmute the audio
    // PARAMS:
    //    iBuffer - the buffer to unmute
    virtual void Unmute( int iBuffer );
#endif // __BANK

private:
    audEntity m_audEntity;
    audSound *m_pSound[c_numBuffers];

#if __BANK
    bool m_bIsMuted;
#endif
    
    static u32 s_iStreamingBucketId;
};

//##############################################################################

class SampleCutsceneSubtitleEntity : public cutsSingletonEntity
{
public:
    SampleCutsceneSubtitleEntity();
    SampleCutsceneSubtitleEntity( atArray<const cutfObject*>& pObjectList );
    virtual ~SampleCutsceneSubtitleEntity() {}

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL );    
};

//##############################################################################

class SampleCutsceneScreenFadeEntity : public cutsUniqueEntity
{
public:
    SampleCutsceneScreenFadeEntity();
    SampleCutsceneScreenFadeEntity( const cutfObject* pObject );
    virtual ~SampleCutsceneScreenFadeEntity() {}

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL );    
};

//##############################################################################

class SampleCutsceneEventEntity : public cutsUniqueEntity
{
public:
    SampleCutsceneEventEntity();
    SampleCutsceneEventEntity( const cutfObject* pObject );
    virtual ~SampleCutsceneEventEntity() {}

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL );    

private:
    atMap<ConstString, scrThreadId> m_scriptFileToThreadIdMap;
};

//##############################################################################

class SampleCutsceneBlockingBoundsEntity : public cutsBoundsEntity
{
public:
    SampleCutsceneBlockingBoundsEntity( SampleBlockingBounds *pBlockingBounds, const cutfObject* pObject );
    virtual ~SampleCutsceneBlockingBoundsEntity() {}

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL );

private:
    SampleBlockingBounds *m_pBlockingBounds;
};

//##############################################################################

class SampleCutsceneRemovalBoundsEntity : public cutsBoundsEntity
{
public:
    SampleCutsceneRemovalBoundsEntity( SampleRemovalBounds *pRemovalBounds, const cutfObject* pObject );
    virtual ~SampleCutsceneRemovalBoundsEntity() {}

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL );

private:
    SampleRemovalBounds *m_pRemovalBounds;
};

//##############################################################################

} // namespace ragesamples

#endif // SAMPLE_CUTSCENE_MISCENTITY_H
