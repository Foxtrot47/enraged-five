// 
// sample_cutscene/sample_cutscene.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_CUTSCENE_SAMPLE_CUTSCENE_H
#define SAMPLE_CUTSCENE_SAMPLE_CUTSCENE_H

#include "cutscenemanager.h"

#include "atl/array.h"
#include "atl/functor.h"
#include "atl/map.h"
#include "atl/queue.h"
#include "audioengine/controller.h"
#include "cutscene/cutsmanager.h"
#include "grcore/light.h"
#include "sample_simpleworld/sample_simpleworld.h"
#include "text/language.h"
#include "vector/matrix34.h"

namespace rage
{
    class bkCombo;
    class crCreatureComponentCamera;
    class crCreatureComponentLight;
    class crSkeleton;
    class RvAnimMgr;
    class txtFormattedStringTex;

#if __BANK
    struct SEditCutfLightInfo;
#endif // __BANK
}

namespace ragesamples
{

//##############################################################################

class SampleGameEntity
{
public:
    SampleGameEntity();
    SampleGameEntity( const char *pName );
    virtual ~SampleGameEntity() = 0;

    const char* GetName() const;
    void SetName( const char *pName );

    bool IsActive() const;
    void SetIsActive( bool bActive );

    Matrix34& GetTransform();
    
    virtual void Update() = 0;

#if __BANK
    virtual void AddWidgets( bkBank &bank );
#endif

protected:
    Matrix34 m_transform;

private:
    char *m_pName;
    bool m_bIsActive;
};

inline const char* SampleGameEntity::GetName() const
{
    return m_pName;
}

inline bool SampleGameEntity::IsActive() const
{
    return m_bIsActive;
}

inline void SampleGameEntity::SetIsActive( bool bActive )
{
    m_bIsActive = bActive;
}

inline Matrix34& SampleGameEntity::GetTransform()
{
    return m_transform;
}

//#############################################################################

class SampleBlockingBounds : public SampleGameEntity
{
public:
    SampleBlockingBounds( const char *pName );
    virtual ~SampleBlockingBounds();

    virtual void Update();
};

//#############################################################################

class SampleRemovalBounds : public SampleBlockingBounds
{
public:
    SampleRemovalBounds( const char *pName );
    virtual ~SampleRemovalBounds();
};

//#############################################################################

class SampleAnimCreature : public SampleGameEntity
{
public:
    SampleAnimCreature( const char *pName );
    virtual ~SampleAnimCreature();
   
    RvAnimMgr* GetAnimMgr();

    virtual void Update();

#if __BANK
    virtual void AddWidgets( bkBank &bank );
#endif

protected:
    crCreature *m_pCreature;
    RvAnimMgr *m_pAnimMgr;
};

inline RvAnimMgr* SampleAnimCreature::GetAnimMgr()
{
    return m_pAnimMgr;
}

//##############################################################################

class SampleAnimCamera : public SampleAnimCreature
{
public:
    SampleAnimCamera();
    SampleAnimCamera( const char *pName );
    virtual ~SampleAnimCamera();
      
    const grcViewport& GetViewport() const;

    void SetFaceZoomEnabled( bool bEnable );

    void SetFaceZoomMatrix( const Matrix34 &m, float fNearDrawDistance, float fFarDrawDistance );

    bool IsDepthOfFieldEnabled() const;
    void SetDepthOfFieldEnabled( bool bEnable );

    virtual void Update();

private:
    grcViewport m_viewport;
    crCreatureComponentCamera *m_pComponentCamera;

    bool m_bFaceZoomEnabled;
    grcViewport m_faceZoomViewport;

    bool m_bDepthOfFieldEnabled;
};

inline const grcViewport& SampleAnimCamera::GetViewport() const
{
    return m_bFaceZoomEnabled ? m_faceZoomViewport : m_viewport;
}

inline void SampleAnimCamera::SetFaceZoomEnabled( bool bEnable )
{
    m_bFaceZoomEnabled = bEnable;
}

inline bool SampleAnimCamera::IsDepthOfFieldEnabled() const
{
    return m_bDepthOfFieldEnabled;
}

inline void SampleAnimCamera::SetDepthOfFieldEnabled( bool bEnable )
{
    m_bDepthOfFieldEnabled = bEnable;
}

//##############################################################################

class SampleAnimLight : public SampleAnimCreature
{
public:
    SampleAnimLight();
    SampleAnimLight( const char *pName );
    virtual ~SampleAnimLight();

    const grcLightGroup::grcLightSource& GetLight() const;

    virtual void Update();

#if __BANK
    void SetEditLightInfo( SEditCutfLightInfo *pEditLightInfo );

    virtual void AddWidgets( bkBank &bank );
#endif

private:
    grcLightGroup::grcLightSource m_light;
    crCreatureComponentLight *m_pComponentLight;

#if __BANK
    SEditCutfLightInfo *m_pEditLightInfo;
#endif
};

inline const grcLightGroup::grcLightSource& SampleAnimLight::GetLight() const
{
    return m_light;
}

#if __BANK

inline void SampleAnimLight::SetEditLightInfo( SEditCutfLightInfo *pEditLightInfo )
{
    m_pEditLightInfo = pEditLightInfo;
}

#endif // __BANK

//##############################################################################

class SampleScreenFadeManager : public datBase
{
public:
    SampleScreenFadeManager();
    ~SampleScreenFadeManager();

    void FadeIn( float fCurrentTime, const Color32 &color, float fDuration );

    void FadeOut( float fCurrentTime, const Color32 &color, float fDuration );

    void Pause();

    void Play();

    void Stop();

    bool IsActive() const;
    void SetIsActive( bool bIsActive );

    void SetCurrentTime( float fTime );

    void Update();

    void Draw();

private:
    enum EFadeState
    {
        NOT_FADING,
        FADING_IN,
        FADING_OUT
    };

    void Fade( EFadeState state, float fCurrentTime, const Color32 &color, float fDuration );

    bool m_bPlay;

    EFadeState m_fadeState;

    float m_fCurrentTime;
    float m_fStartTime;
    float m_fEndTime;

    Color32 m_targetColor;
    Color32 m_currentColor;    

#if __BANK
    Color32 m_fadeColor;
    float m_fFadeDuration;

public:
    void AddWidgets( bkBank &bank );

private:
    void BankFadeInCallback();
    void BankFadeOutCallback();
    void BankPlayCallback();
    void BankPauseCallback();
    void BankStopCallback();
#endif // __BANK
};

inline void SampleScreenFadeManager::FadeIn( float fCurrentTime, const Color32 &color, float fDuration )
{
    Fade( FADING_IN, fCurrentTime, color, fDuration );
}

inline void SampleScreenFadeManager::FadeOut( float fCurrentTime, const Color32 &color, float fDuration )
{
    Fade( FADING_OUT, fCurrentTime, color, fDuration );
}

inline void SampleScreenFadeManager::Pause()
{
    m_bPlay = false;
}

inline void SampleScreenFadeManager::Play()
{
    m_bPlay = true;
}

inline void SampleScreenFadeManager::SetCurrentTime( float fTime )
{
    m_fCurrentTime = fTime;
}


//##############################################################################

class SampleCutscene : public rmcSampleSimpleWorld
{
public:
    SampleCutscene();
    virtual ~SampleCutscene();

    virtual void InitClient();
    virtual void ShutdownClient();

    virtual void UpdateClient();
    virtual void UpdateLights();

    virtual void DrawClient();

    virtual void SetupCamera();

    SampleAnimCamera* CreateAnimCamera( const char *pName );
    SampleAnimCamera* GetAnimCamera( const char *pName );
    void DeleteAnimCamera( const char *pName );

    SampleAnimLight* CreateAnimLight( const char *pName );
    SampleAnimLight* GetAnimLight( const char *pName );
    void DeleteAnimLight( const char* pName );

    SampleScreenFadeManager& GetScreenFadeManager();

    SampleBlockingBounds* CreateBlockingBounds( const char* pName );
    SampleBlockingBounds* GetBlockingBounds( const char* pName );
    void DeleteBlockingBounds( const char* pName );
    
    SampleRemovalBounds* CreateRemovalBounds( const char* pName );
    SampleRemovalBounds* GetRemovalBounds( const char* pName );
    void DeleteRemovalBounds( const char* pName );

    txtLanguage::eLanguage GetCurrentLanguage() const;
    bool LoadSubtitles( const char* pFilename );
    void UnloadSubtitles( const char* pFilename );
    bool ShowSubtitle( const char* pIdentifier, int iTransition, float fDuration );
    bool HideSubtitle( const char* pIdentifier, int iTransition, float fDuration );
    void HideSubtitles();

    typedef Functor0 BatchCompleteFunctor;

    void StartEntityBatchLoad( float fTimeInterval );
    void AddEntityFilenameToBatch( const char* pFilename );
    void EndEntityBatchLoad( BatchCompleteFunctor functor );
    void CancelEntityBatchLoad();

    void WaitForUserInput();
    void SetWaitForUserInputMessage( const char *pMessage );

protected:
    virtual const char* GetSampleName() const;

    SampleAnimCamera m_animCamera;
    SampleAnimLight m_animLights[grcLightGroup::DEFAULT_LIGHT_COUNT];
    SampleScreenFadeManager m_screenFadeManager;
    SampleCutsceneManager m_cutsceneManager;

    float m_fBatchLoaderTime;
    float m_fBatchLoaderTimeInterval;
    bool m_bBatchIsLoading;
    Functor0 m_batchCompleteFunctor;
    atQueue<atString, 16> m_batchFilenames;

    atArray<atString> m_currentSubtitleIdentifiers;
    atArray<txtFormattedStringTex *> m_currentSubtitles;

    atMap<ConstString, SampleBlockingBounds *> m_blockingBounds;
    atMap<ConstString, SampleRemovalBounds *> m_removalBounds;

    bool m_bWaitingForUserInput;
    const char *m_pWaitingForUserInputMessage;
    ioValue m_continueCutSceneKey;

    audController m_AudioController;

#if !__FINAL
    ioValue m_rewind;
    ioValue m_fastForward;
    ioValue m_restart;
    ioValue m_playResume;
    ioValue m_previousFaceZoom;
    ioValue m_nextFaceZoom;
#endif // !__FINAL

#if __BANK
    bkCombo *m_pCutfilesCombo;
    int m_iSelectedCutSceneIndex;    
    bool m_bPlayNow;
    int m_fadeOutGameAtBeginning;
    int m_fadeInCutsceneAtBeginning;
    int m_fadeOutCutsceneAtEnd;
    int m_fadeInGameAtEnd;

    virtual void AddWidgetsClient();
    void BankRefreshCutfilesComboCallback();
    void BankStartEndCutsceneCallback();
#endif // __BANK
};

inline const char* SampleCutscene::GetSampleName() const
{
    return "sample_cutscene";
}

inline SampleScreenFadeManager& SampleCutscene::GetScreenFadeManager()
{
    return m_screenFadeManager;
}

inline void SampleCutscene::SetWaitForUserInputMessage( const char *pMessage )
{
    m_pWaitingForUserInputMessage = pMessage;
}

//##############################################################################

} // namespace ragesamples

#endif // SAMPLE_CUTSCENE_SAMPLE_CUTSCENE_H
