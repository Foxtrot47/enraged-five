// 
// sample_cutscene/sample_cutscene.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_cutscene.h"
#include "sampleEventDefs.h"

#include "audioengine/engine.h"
#include "audioengine/engineutil.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/combo.h"
#include "creature/creature.h"
#include "creature/componentcamera.h"
#include "creature/componentlight.h"
#include "creature/componentmover.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/motiontreescheduler.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "cutfile/cutfevent.h"
#include "file/asset.h"
#include "grcore/device.h"
#include "grcore/font.h"
#include "grcore/im.h"
#include "grcore/quads.h"
#include "grpostfx/postfx.h"
#include "input/pad.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "text/cursor.h"
#include "text/language.h"
#include "text/stringtable.h"
#include "text/stringtex.h"
#include "vector/vector4.h"

namespace asset 
{
    XPARAM( path );
}

namespace ragesamples
{

PARAM( cutfile, "[sample_cutscene] The cut file to play" );

//##############################################################################

void FindCutFilesCallback(const fiFindData &data, void *userArg)
{
    atArray<atString>& filenames = *((atArray<atString>*)userArg);

    const char* pConstExtension = ASSET.FindExtensionInPath( data.m_Name );    
    if ( pConstExtension != NULL )
    {
        char *pExtension = const_cast<char *>( pConstExtension ) + 1;
        if ( (stricmp( pExtension, PI_CUTSCENE_XMLFILE_EXT ) == 0) || (stricmp( pExtension, PI_CUTSCENE_BINFILE_EXT ) == 0) )
        {
            char cFilename[RAGE_MAX_PATH];
            ASSET.RemoveExtensionFromPath( cFilename, sizeof(cFilename), data.m_Name );

            atString strFilename(cFilename);

            if ( filenames.Find( strFilename ) == -1 )
            {
                filenames.PushAndGrow( strFilename );
            }
        }
    }
}

//#############################################################################

SampleGameEntity::SampleGameEntity()
: m_bIsActive(false)
{
    m_pName = NULL;
    m_transform.Identity();
}

SampleGameEntity::SampleGameEntity( const char *pName )
: m_bIsActive(false)
{
    m_pName = StringDuplicate( pName );
    m_transform.Identity();
}

SampleGameEntity::~SampleGameEntity()
{
    if ( m_pName != NULL )
    {
        StringFree( m_pName );
    }
}

void SampleGameEntity::SetName( const char *pName )
{
    StringFree( m_pName );
    m_pName = StringDuplicate( pName );
}

#if __BANK

void SampleGameEntity::AddWidgets( bkBank &bank )
{
    bank.AddToggle( "Is Active", &m_bIsActive );
    bank.AddMatrix( "Transformation", &m_transform, -10000.0f, 10000.0f, 1.0f );
}

#endif // __BANK

//#############################################################################

SampleBlockingBounds::SampleBlockingBounds( const char *pName )
: SampleGameEntity(pName)
{    

}

SampleBlockingBounds::~SampleBlockingBounds()
{
    
}

void SampleBlockingBounds::Update()
{

}

//#############################################################################

SampleRemovalBounds::SampleRemovalBounds( const char *pName )
: SampleBlockingBounds( pName )
{

}

SampleRemovalBounds::~SampleRemovalBounds()
{

}

//#############################################################################

SampleAnimCreature::SampleAnimCreature( const char *pName )
: SampleGameEntity(pName)
, m_pCreature(NULL)
, m_pAnimMgr(NULL)
{
    m_pCreature = rage_new crCreature( 1 );

    m_pAnimMgr = rage_new RvAnimMgr;
    m_pAnimMgr->Init( *m_pCreature );
}

SampleAnimCreature::~SampleAnimCreature()
{
    if ( m_pAnimMgr )
    {
        delete m_pAnimMgr;
        m_pAnimMgr = NULL;
    }

    if ( m_pCreature )
    {
        delete m_pCreature;
        m_pCreature = NULL;
    }
}

void SampleAnimCreature::Update()
{
    if ( m_pAnimMgr )
    {
        // update the animation
        m_pAnimMgr->Update();
    }
}

#if __BANK

void SampleAnimCreature::AddWidgets( bkBank &bank )
{
    SampleGameEntity::AddWidgets( bank );

    if ( m_pAnimMgr )
    {
        bank.PushGroup( "Animation Control" );
        {
            m_pAnimMgr->AddWidgets( bank );
        }
        bank.PopGroup();
    }
}

#endif // __BANK

//##############################################################################

SampleAnimCamera::SampleAnimCamera()
: SampleAnimCreature(NULL)
, m_bFaceZoomEnabled(false)
, m_bDepthOfFieldEnabled(true)
{
    m_pComponentCamera = rage_new crCreatureComponentCamera( *m_pCreature, &m_transform );
    m_pCreature->AddComponent( *m_pComponentCamera );
}

SampleAnimCamera::SampleAnimCamera( const char *pName )
: SampleAnimCreature(pName)
, m_bFaceZoomEnabled(false)
, m_bDepthOfFieldEnabled(true)
{
    m_pComponentCamera = rage_new crCreatureComponentCamera( *m_pCreature, &m_transform );
    m_pCreature->AddComponent( *m_pComponentCamera );
}

SampleAnimCamera::~SampleAnimCamera()
{
    m_pComponentCamera = NULL;
}

void SampleAnimCamera::Update()
{
    if ( m_pComponentCamera )
    {
        // update the parent matrix with the current transformation
        m_pComponentCamera->SetParentMatrix( m_transform );
    }

    SampleAnimCreature::Update();

    if ( m_bFaceZoomEnabled )
    {
        if ( IsActive() && m_bDepthOfFieldEnabled )
        {
            const Vector4 &vCurrentDofParams = GRPOSTFX->GetDofParams();
            Vector4 vDofParams( vCurrentDofParams[0], 0.5f, 15.0f, vCurrentDofParams[3] );
            GRPOSTFX->SetDofParams( vDofParams );
        }
    }
    else if ( m_pComponentCamera )
    {
        // grab the current viewport
        m_pComponentCamera->ConfigureViewport( m_viewport );

        // only update the depth of field if we're active
        if ( IsActive() && m_bDepthOfFieldEnabled )
        {
            const Vector4 &vCurrentDofParams = GRPOSTFX->GetDofParams();
            Vector4 vDofParams( vCurrentDofParams[0], m_pComponentCamera->GetNearDepthOfField(),
                m_pComponentCamera->GetFarDepthOfField(), vCurrentDofParams[3] );
            GRPOSTFX->SetDofParams( vDofParams );
        }
    }
}

void SampleAnimCamera::SetFaceZoomMatrix( const Matrix34 &m, float fNearDrawDistance, float fFarDrawDistance )
{
    if ( fNearDrawDistance <= 0.0f )
    {
        fNearDrawDistance = 0.05f;
    }

    if ( fFarDrawDistance <= 0.0f )
{
        fFarDrawDistance = 1000.0f;
    }

    m_faceZoomViewport.Perspective( 65.0f, 0.0f, fNearDrawDistance, fFarDrawDistance );
    m_faceZoomViewport.SetCameraMtx( m );
    m_faceZoomViewport.SetWorldMtx( M34_IDENTITY );
}

//##############################################################################

SampleAnimLight::SampleAnimLight()
: SampleAnimCreature(NULL)
#if __BANK
, m_pEditLightInfo(NULL)
#endif // __BANK
{
    m_pComponentLight = rage_new crCreatureComponentLight( *m_pCreature, &m_transform );
    m_pCreature->AddComponent( *m_pComponentLight );
}

SampleAnimLight::SampleAnimLight( const char *pName )
: SampleAnimCreature(pName)
#if __BANK
, m_pEditLightInfo(NULL)
#endif // __BANK
{
    m_pComponentLight = rage_new crCreatureComponentLight( *m_pCreature, &m_transform );
    m_pCreature->AddComponent( *m_pComponentLight );
}

SampleAnimLight::~SampleAnimLight()
{
    m_pComponentLight = NULL;
}

void SampleAnimLight::Update()
{
    if ( m_pComponentLight )
    {
        // update the parent matrix with the current transformation
        m_pComponentLight->SetParentMatrix( m_transform );
    }

    SampleAnimCreature::Update();

    if ( m_pComponentLight )
    {
        // grab the current light
        m_pComponentLight->ConfigureLight( m_light );

#if __BANK
        if ( m_pEditLightInfo != NULL )   
        {
            // feed the current values back in as the original values
            m_pEditLightInfo->vOriginalColor = m_light.m_Color;
            m_pEditLightInfo->fOriginalConeAngle = m_light.m_ConeAngle * RtoD;
            m_pEditLightInfo->vOriginalDirection = m_light.m_Dir;
            m_pEditLightInfo->fOriginalFallOff = m_light.m_Falloff;
            m_pEditLightInfo->fOriginalIntensity = m_light.m_Intensity;
            m_pEditLightInfo->vOriginalPosition = m_light.m_Pos;

            // override each value, if needed
            if ( m_pEditLightInfo->bOverrideColor )
            {
                m_light.m_Color = m_pEditLightInfo->vColor;
            }

            if ( m_pEditLightInfo->bOverrideConeAngle )
            {
                m_light.m_ConeAngle = m_pEditLightInfo->fConeAngle * DtoR;
            }

            if ( m_pEditLightInfo->bOverrideDirection )
            {
                m_light.m_Dir = m_pEditLightInfo->vDirection;
            }

            if ( m_pEditLightInfo->bOverrideFallOff )
            {
                m_light.m_Falloff = m_pEditLightInfo->fFallOff;
            }

            if ( m_pEditLightInfo->bOverrideIntensity )
            {
                m_light.m_Intensity = m_pEditLightInfo->fIntensity;
            }

            if ( m_pEditLightInfo->bOverridePosition )
            {
                m_light.m_Pos = m_pEditLightInfo->vPosition;
            }
        }
#endif // __BANK
    }
}

#if __BANK

void SampleAnimLight::AddWidgets( bkBank &bank )
{
    SampleAnimCreature::AddWidgets( bank );
}

#endif // __BANK

//##############################################################################

SampleScreenFadeManager::SampleScreenFadeManager()
#if __BANK
: m_fFadeDuration(CUTSCENE_FADE_TIME)
#endif // __BANK
{
#if __BANK
    m_fadeColor.Set( 0, 0, 0, 255 );
#endif // __BANK

    Stop();
}

SampleScreenFadeManager::~SampleScreenFadeManager()
{

}

void SampleScreenFadeManager::Stop()
{
    m_bPlay = false;
    m_fadeState = NOT_FADING;
    m_targetColor.Set( 0, 0, 0, 0 );
    m_currentColor.Set( 0, 0, 0, 0 );
    m_fCurrentTime = 0.0f;
    m_fStartTime = 0.0f;
    m_fEndTime = 0.0f;
}

void SampleScreenFadeManager::Update()
{
    if ( m_bPlay )
    {
        m_fCurrentTime += TIME.GetSeconds();
    }
    
    if ( m_fCurrentTime >= m_fEndTime )
    {
        m_fadeState = NOT_FADING;
    }

    float fPhase = 0.0f;
    if ( m_fEndTime > m_fStartTime )
    {
        fPhase = (m_fCurrentTime - m_fStartTime) / (m_fEndTime - m_fStartTime);
    }

    switch ( m_fadeState )
    {
    case FADING_IN:
        {
            float fAlpha = m_targetColor.GetAlphaf() * (1.0f - fPhase);
            if ( fAlpha < 0.0f )
            {
                fAlpha = 0.0f;
            }

            if ( fAlpha > 1.0f )
            {
                fAlpha = 1.0f;
            }

            m_currentColor.Setf( m_currentColor.GetRedf(), m_currentColor.GetGreenf(), m_currentColor.GetBluef(), fAlpha );
        }
        break;
    case FADING_OUT:
        {
            float fAlpha = m_targetColor.GetAlphaf() * fPhase;
            if ( fAlpha < 0.0f )
            {
                fAlpha = 0.0f;
            }

            if ( fAlpha > 1.0f )
            {
                fAlpha = 1.0f;
            }

            m_currentColor.Setf( m_currentColor.GetRedf(), m_currentColor.GetGreenf(), m_currentColor.GetBluef(), fAlpha );
        }
        break;
    default:
        break;
    }
}

void SampleScreenFadeManager::Draw()
{
    // By checking the alpha instead of the fade state, we ensure that if we've faded out we stay out until we are canceled or faded in.
    if ( m_currentColor.GetAlpha() > 0 )
    {
        grcState::SetState( grcsDepthFunc, grccfAlways );
        grcState::SetState( grcsDepthWrite, false );
        grcState::SetState( grcsAlphaBlend, true );

        float height = (float)GRCDEVICE.GetHeight();
        float width = (float)GRCDEVICE.GetWidth();

        PUSH_DEFAULT_SCREEN();
        grcBeginQuads( 1 );
        grcDrawQuadf( 0.0f, 0.0f, width, height, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, m_currentColor );
        grcEndQuads();
        POP_DEFAULT_SCREEN();
    }
}

void SampleScreenFadeManager::Fade( EFadeState state, float fCurrentTime, const Color32 &color, float fDuration )
{
    m_fadeState = state;

    m_fCurrentTime = fCurrentTime;
    m_fStartTime = fCurrentTime;
    m_fEndTime = fCurrentTime + fDuration;

    m_currentColor = color;
    m_targetColor = color;
    
    if ( state == FADING_OUT )
    {
        m_currentColor.SetAlpha( 0 );
    }

    Play();
}

#if __BANK

void SampleScreenFadeManager::AddWidgets( bkBank &bank )
{
    bank.AddColor( "Fade Color", &m_fadeColor );
    bank.AddColor( "Current Color", &m_currentColor );
    bank.AddSlider( "Fade Duration", &m_fFadeDuration, 0.0f, 10.0f, 0.1f );
    bank.AddButton( "Fade In", datCallback(MFA(SampleScreenFadeManager::BankFadeInCallback),this) );
    bank.AddButton( "Fade Out", datCallback(MFA(SampleScreenFadeManager::BankFadeOutCallback),this) );
    bank.AddButton( "Play", datCallback(MFA(SampleScreenFadeManager::BankPlayCallback),this) );
    bank.AddButton( "Pause", datCallback(MFA(SampleScreenFadeManager::BankPauseCallback),this) );
    bank.AddButton( "Stop", datCallback(MFA(SampleScreenFadeManager::BankStopCallback),this) );
}

void SampleScreenFadeManager::BankFadeInCallback()
{
    FadeIn( m_fCurrentTime, m_fadeColor, m_fFadeDuration );
}

void SampleScreenFadeManager::BankFadeOutCallback()
{
    FadeOut( m_fCurrentTime, m_fadeColor, m_fFadeDuration );
}

void SampleScreenFadeManager::BankPlayCallback()
{
    Play();
}

void SampleScreenFadeManager::BankPauseCallback()
{
    Pause();
}

void SampleScreenFadeManager::BankStopCallback()
{
    Stop();
}

#endif // __BANK

//##############################################################################

SampleCutscene::SampleCutscene()
: m_fBatchLoaderTime(0.0f)
, m_fBatchLoaderTimeInterval(0.0f)
, m_bBatchIsLoading(false)
#if __BANK
, m_pCutfilesCombo(NULL)
, m_iSelectedCutSceneIndex( -1 )
, m_bPlayNow(true)
, m_fadeOutGameAtBeginning(cutsManager::DEFAULT_FADE)
, m_fadeInCutsceneAtBeginning(cutsManager::DEFAULT_FADE)
, m_fadeOutCutsceneAtEnd(cutsManager::DEFAULT_FADE)
, m_fadeInGameAtEnd(cutsManager::DEFAULT_FADE)
, m_bWaitingForUserInput(false)
, m_pWaitingForUserInputMessage(NULL)
#endif // __BANK
{

}

SampleCutscene::~SampleCutscene()
{

}

void SampleCutscene::InitClient()
{
    rmcSampleSimpleWorld::InitClient();

#if !__FINAL
    // Setup keyboard hot-keys
    m_Mapper.Map( IOMS_KEYBOARD, KEY_LEFT, m_rewind );
    m_Mapper.Map( IOMS_KEYBOARD, KEY_RIGHT, m_fastForward );
    m_Mapper.Map( IOMS_KEYBOARD, KEY_UP, m_restart );
    m_Mapper.Map( IOMS_KEYBOARD, KEY_DOWN, m_playResume );
    m_Mapper.Map( IOMS_KEYBOARD, KEY_F9, m_previousFaceZoom );
    m_Mapper.Map( IOMS_KEYBOARD, KEY_F10, m_nextFaceZoom );
#endif // !__FINAL

#if __WIN32PC
    ASSET.PushFolder( "T:/rage/audio/sample_audio/output/PC" );
#elif __XENON
    ASSET.PushFolder( "T:/rage/audio/sample_audio/output/XBOX360" );
#elif __PPU
    ASSET.PushFolder( "T:/rage/audio/sample_audio/output/PS3" );
#endif

    if ( !InitializeAudio( m_AudioController, sysMemAllocator::GetMaster().GetAllocator(1), sysMemAllocator::GetMaster().GetAllocator(0) ) )
    {
        AssertMsg(0, "Audio Engine failed to initialise!");

#if __WIN32PC || __XENON || __PPU
        ASSET.PopFolder();
#endif
    }
    else
    {
#if __WIN32PC || __XENON || __PPU
        ASSET.PopFolder();
#endif

        AUDIOENGINE.GetEffectManager().StartUpdatingEffects();

        // enable a listener
        AUDIOENGINE.GetEnvironment().SetListenerContribution(1.0f, 0);
        AUDIOENGINE.GetEnvironment().CommitListenerSettings(0);
    }

    m_cutsceneManager.SetSampleCutscene( this );

    char assetDir[RAGE_MAX_PATH];
    sprintf( assetDir, "%s/sample_cutscene", ASSET.GetPath() );
    m_cutsceneManager.SetAssetDir( assetDir );

    m_ViewMode = 0;
    m_DrawHelp = false;
    m_DrawWorldAxes = false;
    m_DrawLogo = false;
    m_DrawCam = false;

#if __WIN32PC
    GRPOSTFX->SetPPTechniqueName( "HDR" );
#else
    GRPOSTFX->SetPPTechniqueName( "HDRDepthofField" );
#endif

    m_Mapper.Map( IOMS_PAD_DIGITALBUTTON, ioPad::RDOWN, m_continueCutSceneKey );

    // initialize the static members of the cuts and cutf classes
    cutsManager::InitClass();

    // register our custom events
    cutfEvent::SetEventIdDisplayNameFunc( MakeFunctorRet( &GetCutsceneCustomEventIdDisplayName ) );
    cutfEvent::SetEventIdRankFunc( MakeFunctorRet( &GetCutsceneCustomEventIdRank ) );
    cutfEvent::SetOppositeEventIdFunc( MakeFunctorRet( &GetCutsceneCustomOppositeEventId ) );
    cutfEventArgs::SetEventArgsTypeNameFunc( MakeFunctorRet( &GetCutsceneCustomEventArgsTypeName ) );
}

void SampleCutscene::ShutdownClient()
{
    m_cutsceneManager.Unload();

    UnloadSubtitles( NULL );

    // shutdown the static members of the cuts and cutf classes
    cutsManager::ShutdownClass();

#if __BANK
    m_cutsceneManager.RemoveWidgets();
#endif // __BANK

    AUDIOENGINE.Shutdown();

    rmcSampleSimpleWorld::ShutdownClient();
}

void SampleCutscene::UpdateClient()
{
    u32 now = audEngineUtil::GetCurrentTimeInMilliseconds();

    rmcSampleSimpleWorld::UpdateClient();

#if !__FINAL
    if ( m_rewind.IsPressed() )
    {
        if ( m_cutsceneManager.IsPaused() )
        {
            m_cutsceneManager.StepBackward();
        }
        else if ( m_cutsceneManager.IsPlaying() )
        {
            m_cutsceneManager.Rewind();
        }
    }
    else if ( m_fastForward.IsPressed() )
    {
        if ( m_cutsceneManager.IsPaused() )
        {
            m_cutsceneManager.StepForward();
        }
        else if ( m_cutsceneManager.IsPlaying() )
        {
            m_cutsceneManager.FastForward();
        }
    }
    else if ( m_restart.IsPressed() )
    {
        m_cutsceneManager.Restart();
    }
    else if ( m_playResume.IsPressed() )
    {
        m_cutsceneManager.PauseResume();
    }
    else if ( m_previousFaceZoom.IsPressed() )
    {
        m_cutsceneManager.PreviousFace();
    }
    else if ( m_nextFaceZoom.IsPressed() )
    {
        m_cutsceneManager.NextFace();
    }
#endif // !__FINAL

    if ( m_bBatchIsLoading )
    {
        m_fBatchLoaderTime += TIME.GetSeconds();

        if ( m_fBatchLoaderTime >= m_fBatchLoaderTimeInterval )
        {
            if ( m_batchFilenames.GetCount() > 0 )
            {
                atString filename = m_batchFilenames.Pop();
                LoadEntity( filename.c_str() );
                m_fBatchLoaderTime = 0.0f;
            }
            else
            {
                m_bBatchIsLoading = false;
                m_batchCompleteFunctor();
            }
        }
    }

    m_animCamera.Update();

    for ( int i = 0; i < grcLightGroup::DEFAULT_LIGHT_COUNT; ++i )
    {
        m_animLights[i].Update();
    }

    m_screenFadeManager.Update();

    atMap<ConstString, SampleBlockingBounds *>::Iterator blockingBoundsIter = m_blockingBounds.CreateIterator();
    for ( blockingBoundsIter.Start(); !blockingBoundsIter.AtEnd(); blockingBoundsIter.Next() )
    {
        blockingBoundsIter.GetData()->Update();
    }

    atMap<ConstString, SampleRemovalBounds *>::Iterator removalBoundsIter = m_removalBounds.CreateIterator();
    for ( removalBoundsIter.Start(); !removalBoundsIter.AtEnd(); removalBoundsIter.Next() )
    {
        removalBoundsIter.GetData()->Update();
    }

    if ( m_bWaitingForUserInput && m_continueCutSceneKey.IsPressed() )
    {
        m_bWaitingForUserInput = false;
        m_pWaitingForUserInputMessage = NULL;
        m_cutsceneManager.Play();
    }
    
    m_AudioController.PreUpdate(now);
    m_AudioController.Update(now);

    // Update the CategoryManager, so any settings changed will take effect.
    AUDIOENGINE.GetCategoryManager().CommitCategorySettings();

    audWaveSlot::UpdateSlots();

    m_cutsceneManager.PreSceneUpdate();
}

void SampleCutscene::UpdateLights()
{
    int iActiveCount = 0;
    for ( int i = 0; i < grcLightGroup::DEFAULT_LIGHT_COUNT; ++i )
    {
        if ( m_animLights[i].IsActive() )
        {
            const grcLightGroup::grcLightSource &light = m_animLights[i].GetLight();

            // copy the light data to the light group
            m_Lights.SetColor( i, light.m_Color );
            m_Lights.SetConeAngle( i, light.m_ConeAngle );
            m_Lights.SetDirection( i, light.m_Dir );
            m_Lights.SetFalloff( i, light.m_Falloff );
            m_Lights.SetIntensity( i, light.m_Intensity );
            m_Lights.SetLightEnabled( i, true );
            m_Lights.SetLightType( i, (grcLightGroup::grcLightType)light.m_Type );
            m_Lights.SetNegative( i, light.m_Negative ? true : false );
            m_Lights.SetOversaturate( i, light.m_Oversaturate ? true : false );
            m_Lights.SetPosition( i, light.m_Pos );

            // update the light manipulator
            m_LightManipulators[i].lightSrcMtx.Identity();
            m_LightManipulators[i].lightSrcMtx.d = light.m_Pos;
            m_LightManipulators[i].lightTgtMtx.Identity();

            float t;
            Vector3 dir = light.m_Dir;
            Vector3 pos = light.m_Pos;

            //If possible try to initially position the light target on the ground plane
            geomSegments::CollideRayPlane( pos, dir, Vector4(0.0f,1.0f,0.0f,0.0f), &t );
            Vector3 targetPos = pos;
            if ( t <= 0.0f )
            {
                //No intersection, so just place it 2 units out along the direction vector
                targetPos.AddScaled( dir, 2.0f );
            }
            else
            {
                targetPos.AddScaled( dir, t );
            }

            m_LightManipulators[i].lightTgtMtx.d = targetPos;

            if ( (light.m_Type == grcLightGroup::LTTYPE_DIR) && m_DirLightOrbitTarget )
            {
                m_LightManipulators[i].lightTgtMtx.LookAt( m_LightManipulators[i].lightTgtMtx.d, m_LightManipulators[i].lightSrcMtx.d );
            }
            
            ++iActiveCount;
        }
        else
        {
            UpdateLight( i );
        }
    }

    if ( iActiveCount > 0 )
    {
        m_Lights.SetActiveCount( iActiveCount );
    }
}

void SampleCutscene::DrawClient()
{
    rmcSampleSimpleWorld::DrawClient();
 
#if __BANK
    // Would make sense to put this in DrawHelpClient(), except that we've turned off help.
    m_cutsceneManager.DrawDebugText();

    m_cutsceneManager.DebugDraw();
#endif // __BANK

    PUSH_DEFAULT_SCREEN();

    m_screenFadeManager.Draw();

    float x = GRCDEVICE.GetWidth() / 2.0f;
    float safeZoneHeight = (float)GRCDEVICE.GetHeight() * 4.0f / 5.0f;
    float safeZoneBottomY = (((float)GRCDEVICE.GetHeight() - safeZoneHeight) / 2.0f) + safeZoneHeight;
    for ( int i = 0; i < m_currentSubtitles.GetCount(); ++i )
    {
        m_currentSubtitles[i]->Draw( x, safeZoneBottomY );
    }

    if ( m_bWaitingForUserInput && (m_pWaitingForUserInputMessage != NULL) )
    {
        float y = (float)(GRCDEVICE.GetHeight() - (grcFont::GetCurrent().GetHeight() * 2));
        float width = (float)strlen( m_pWaitingForUserInputMessage ) * grcFont::GetCurrent().GetWidth();
        float x = (GRCDEVICE.GetWidth() / 2.0f) - (width / 2.0f);

        SetDrawStringPos( x, y );
        DrawString( m_pWaitingForUserInputMessage, false );
    }    

    POP_DEFAULT_SCREEN();

    m_cutsceneManager.PostSceneUpdate( TIME.GetSeconds() );
}

void SampleCutscene::SetupCamera()
{
    if ( m_animCamera.IsActive() )
    {
        grcViewport::SetCurrent( &(m_animCamera.GetViewport()) );
    }
    else
    {
        rmcSampleSimpleWorld::SetupCamera();
    }
}

SampleAnimCamera* SampleCutscene::CreateAnimCamera( const char *pName )
{
    m_animCamera.SetName( pName );

    return &m_animCamera;
}

SampleAnimCamera* SampleCutscene::GetAnimCamera( const char* /*pName*/ )
{
    return &m_animCamera;
}

void SampleCutscene::DeleteAnimCamera( const char* /*pName*/ )
{
    m_animCamera.SetName( NULL );
}

SampleAnimLight* SampleCutscene::CreateAnimLight( const char *pName )
{
    for ( int i = 0; i < grcLightGroup::DEFAULT_LIGHT_COUNT; ++i )
    {
        if ( m_animLights[i].GetName() == NULL )
        {
            m_animLights[i].SetName( pName );
            return &m_animLights[i];
        }
        else if ( strcmp( m_animLights[i].GetName(), pName ) == 0 )
        {
            return &m_animLights[i];
        }
    }

    return NULL;
}

SampleAnimLight* SampleCutscene::GetAnimLight( const char *pName )
{
    for ( int i = 0; i < grcLightGroup::DEFAULT_LIGHT_COUNT; ++i )
    {
        if ( (m_animLights[i].GetName() != NULL) && (strcmp( m_animLights[i].GetName(), pName ) == 0) )
        {
            return &m_animLights[i];
        }
    }

    return NULL;
}

void SampleCutscene::DeleteAnimLight( const char* pName )
{
    for ( int i = 0; i < grcLightGroup::DEFAULT_LIGHT_COUNT; ++i )
    {
        if ( (m_animLights[i].GetName() != NULL) && (strcmp( m_animLights[i].GetName(), pName ) == 0) )
        {            
            m_animLights[i].SetName( NULL );
            break;
        }
    }
}

SampleBlockingBounds* SampleCutscene::CreateBlockingBounds( const char* pName )
{
    SampleBlockingBounds *pBlockingBounds = rage_new SampleBlockingBounds( pName );
    m_blockingBounds.Insert( ConstString(pName), pBlockingBounds );
    return pBlockingBounds;
}

SampleBlockingBounds* SampleCutscene::GetBlockingBounds( const char* pName )
{
    SampleBlockingBounds **ppBlockingBounds = m_blockingBounds.Access( pName );
    if ( ppBlockingBounds != NULL )
    {
        return *ppBlockingBounds;
    }

    return NULL;
}

void SampleCutscene::DeleteBlockingBounds( const char* pName )
{
    SampleBlockingBounds *pBlockingBounds = GetBlockingBounds( pName );
    if ( pBlockingBounds != NULL )
    {
        m_blockingBounds.Delete( pName );
        delete pBlockingBounds;
    }
}

SampleRemovalBounds* SampleCutscene::CreateRemovalBounds( const char* pName )
{
    SampleRemovalBounds *pRemovalBounds = rage_new SampleRemovalBounds( pName );
    m_removalBounds.Insert( ConstString(pName), pRemovalBounds );
    return pRemovalBounds;
}

SampleRemovalBounds* SampleCutscene::GetRemovalBounds( const char* pName )
{
    SampleRemovalBounds **ppRemovalBounds = m_removalBounds.Access( pName );
    if ( ppRemovalBounds != NULL )
    {
        return *ppRemovalBounds;
    }

    return NULL;
}

void SampleCutscene::DeleteRemovalBounds( const char* pName )
{
    SampleRemovalBounds *pRemovalBounds = GetRemovalBounds( pName );
    if ( pRemovalBounds != NULL )
    {
        m_removalBounds.Delete( pName );
        delete pRemovalBounds;
    }
}

txtLanguage::eLanguage SampleCutscene::GetCurrentLanguage() const
{
    // FIXME: We should get the language from the regional settings
    return txtLanguage::kEn;
}

bool SampleCutscene::LoadSubtitles( const char* pFilename )
{
    UnloadSubtitles( NULL );
    
    if ( STRINGTABLE.Load( pFilename, GetCurrentLanguage(), 0xffffffff, false, false ) )
    {
        Displayf( "Loaded subtitle file '%s'.", pFilename );
    }
    else
    {
        Errorf( "Unable to load subtitle file '%s'.", pFilename );
    }

    return true;
}

void SampleCutscene::UnloadSubtitles( const char* /*pFilename*/ )
{
    HideSubtitles();

    if ( STRINGTABLE.IsValid() )
    {
        STRINGTABLE.Kill();
    }
}

bool SampleCutscene::ShowSubtitle( const char* pIdentifier, int /*iTransition*/, float /*fDuration*/ )
{
    atString id( pIdentifier );
    int indexOf = m_currentSubtitleIdentifiers.Find( id );
    if ( indexOf == -1 )
    {
        if ( STRINGTABLE.Exists( pIdentifier ) )
        {
            txtFormattedStringTex *pFormattedStringText = rage_new txtFormattedStringTex( pIdentifier );

            m_currentSubtitleIdentifiers.PushAndGrow( id );
            m_currentSubtitles.PushAndGrow( pFormattedStringText );

            return true;
        }
    }

    return false;
}

bool SampleCutscene::HideSubtitle( const char* pIdentifier, int /*iTransition*/, float /*fDuration*/ )
{
    atString id( pIdentifier );
    int indexOf = m_currentSubtitleIdentifiers.Find( id );
    if ( indexOf != -1 )
    {
        delete m_currentSubtitles[indexOf];

        m_currentSubtitleIdentifiers.Delete( indexOf );
        m_currentSubtitles.Delete( indexOf );

        return true;
    }

    return false;
}

void SampleCutscene::HideSubtitles()
{
    for ( int i = 0; i < m_currentSubtitles.GetCount(); ++i )
    {
        delete m_currentSubtitles[i];
    }

    m_currentSubtitles.Reset();
    m_currentSubtitleIdentifiers.Reset();
}

void SampleCutscene::StartEntityBatchLoad( float fTimeInterval )
{
    m_bBatchIsLoading = false;
    m_fBatchLoaderTimeInterval = fTimeInterval;
}

void SampleCutscene::AddEntityFilenameToBatch( const char* pFilename )
{
    // see if the entity already exists
    int count = SMPWORLDEMGR.GetEntityCount();
    for ( int i = 0; i < count; ++i )
    {
        SimpleWorldEntity *pSimpleWorldEntity = SMPWORLDEMGR.GetEntity( i );
        if ( stricmp( pSimpleWorldEntity->GetSourceFilePath().c_str(), pFilename ) == 0 )
        {
            return;
        }
    }

    m_batchFilenames.Push( atString( pFilename ) );
}

void SampleCutscene::EndEntityBatchLoad( BatchCompleteFunctor functor )
{
    m_batchCompleteFunctor = functor;
    m_fBatchLoaderTime = 0.0f;
    m_bBatchIsLoading = true;
}

void SampleCutscene::CancelEntityBatchLoad()
{
    m_bBatchIsLoading = false;
    m_batchFilenames.Reset();
}

void SampleCutscene::WaitForUserInput()
{
    m_cutsceneManager.Pause();
    m_bWaitingForUserInput = true;
}

#if __BANK

void SampleCutscene::AddWidgetsClient()
{
    rmcSampleSimpleWorld::AddWidgetsClient();

    bkBank &bank = BANKMGR.CreateBank( "rage - SampleCutscene" );
    {         
        bkBank *pCameraBank = BANKMGR.FindBank( "rage - Camera" );
        if ( pCameraBank != NULL )
        {
            pCameraBank->PushGroup( "Animation" );
            {
                m_animCamera.AddWidgets( *pCameraBank );
            }
            pCameraBank->PopGroup();
        }

        bkBank *pLightsBank = BANKMGR.FindBank( "rage - Lights" );
        if ( pLightsBank != NULL )
        {
            bkWidget *pChild = pLightsBank->GetChild();
            for ( int i = 0; i < grcLightGroup::DEFAULT_LIGHT_COUNT; ++i )
            {
                char name[32];
                sprintf( name, "Light %d", i );
                while ( (pChild != NULL) && (strcmp( pChild->GetTitle(), name ) != 0) )
                {
                    pChild = pChild->GetNext();
                }

                if ( pChild != NULL )
                {
                    pLightsBank->SetCurrentGroup( *(dynamic_cast<bkGroup *>( pChild )) );
                    {
                        pLightsBank->PushGroup( "Animation" );
                        {            
                            m_animLights[i].AddWidgets( *pLightsBank );
                        }
                        pLightsBank->PopGroup();  
                    }
                    pLightsBank->UnSetCurrentGroup( *(dynamic_cast<bkGroup *>( pChild )) );
                }
            }
        }

        bkBank *pGraphicsBank = BANKMGR.FindBank( "rage - Gfx" );
        if ( pGraphicsBank != NULL )
        {
            pGraphicsBank->PushGroup( "Screen Fades" );
            {
                m_screenFadeManager.AddWidgets( *pGraphicsBank );
            }
            pGraphicsBank->PopGroup();
        }
        
        m_pCutfilesCombo = dynamic_cast<bkCombo *>( bank.AddCombo( "Cutscene", &m_iSelectedCutSceneIndex, 0, NULL, 0 ) );
        bank.AddButton( "Refresh Cutscenes", datCallback(MFA(SampleCutscene::BankRefreshCutfilesComboCallback),this) );
        bank.AddToggle( "Play Now", &m_bPlayNow );

        const char* pFadeList[] = { "Default", "Force", "Skip" };
        bank.AddCombo( "Fade Out Game", &m_fadeOutGameAtBeginning, 3, pFadeList, 0, NullCallback, "Override the fade out game behavior at the beginning of the scene." );
        bank.AddCombo( "Fade In Cutscene", &m_fadeInCutsceneAtBeginning, 3, pFadeList, 0, NullCallback, "Override the fade in cutscene behavior at the beginning of the scene." );
        bank.AddCombo( "Fade Out Cutscene", &m_fadeOutCutsceneAtEnd, 3, pFadeList, 0, NullCallback, "Override the fade out cutscene behavior at the end of the scene." );
        bank.AddCombo( "Fade In Game", &m_fadeInGameAtEnd, 3, pFadeList, 0, NullCallback, "Override the fade in game behavior at the end of the scene." );

        bank.AddButton( "Start/End Cutscene", datCallback(MFA(SampleCutscene::BankStartEndCutsceneCallback),this) );

        BankRefreshCutfilesComboCallback();
        
        m_cutsceneManager.AddWidgets( bank );
    }

    const char* pCutfile;
    if ( PARAM_cutfile.Get( pCutfile ) )
    {
        m_cutsceneManager.Load( pCutfile );
    }
}

void SampleCutscene::BankRefreshCutfilesComboCallback()
{
    char currentCutfile[RAGE_MAX_PATH];
    if ( m_iSelectedCutSceneIndex > -1 )
    {
        safecpy( currentCutfile, m_pCutfilesCombo->GetString( m_iSelectedCutSceneIndex ), sizeof(RAGE_MAX_PATH) );
    }
    else
    {
        currentCutfile[0] = 0;
    }
    
    char cutfileDir[RAGE_MAX_PATH];
    sprintf( cutfileDir, "%scuts", ASSET.GetPath() );

    atArray<atString> filenames;
    int iHandle = ASSET.EnumFiles( cutfileDir, FindCutFilesCallback, &filenames );
    if ( iHandle )
    {
        m_iSelectedCutSceneIndex = -1;

        m_pCutfilesCombo->UpdateCombo( m_pCutfilesCombo->GetTitle(), m_pCutfilesCombo->GetValuePtr(), filenames.GetCount(), NULL, 
            m_pCutfilesCombo->GetValueOffset(), m_pCutfilesCombo->GetCallback() ? *(m_pCutfilesCombo->GetCallback()) : NullCallback, 
            m_pCutfilesCombo->GetTooltip() );

        for ( int i = 0; i < filenames.GetCount(); ++i )
        {
            m_pCutfilesCombo->SetString( i, filenames[i].c_str() );

            if ( stricmp( currentCutfile, filenames[i].c_str() ) == 0 )
            {
                m_iSelectedCutSceneIndex = i;
            }
        }
    }
}

void SampleCutscene::BankStartEndCutsceneCallback()
{
    if ( m_cutsceneManager.IsRunning() )
    {
        m_cutsceneManager.Stop();
    }
    else if ( m_iSelectedCutSceneIndex > -1 )
    {
        char cutfile[RAGE_MAX_PATH];
        sprintf( cutfile, "%s/cuts/%s", ASSET.GetPath(), m_pCutfilesCombo->GetString( m_iSelectedCutSceneIndex ) );

        m_cutsceneManager.Load( cutfile, NULL, m_bPlayNow, 
            (cutsManager::EScreenFadeOverride)m_fadeOutGameAtBeginning, (cutsManager::EScreenFadeOverride)m_fadeInCutsceneAtBeginning,
            (cutsManager::EScreenFadeOverride)m_fadeOutCutsceneAtEnd, (cutsManager::EScreenFadeOverride)m_fadeInGameAtEnd );
    }
}

#endif // __BANK

//##############################################################################

} // namespace ragesamples
