// 
// sample_cutscene/sample_cinematic.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "sample_cutscene.h"
#include "system/param.h"

using namespace rage;
using namespace ragesamples;

namespace asset 
{
    XPARAM( path );
}

int Main()
{
    SampleCutscene sampleCutscene;

#if __WIN32PC
    asset::PARAM_path.Set( "T:/rage/assets;T:/rage/audio/sample_audio/output/PC;T:/rage/assets/sample_cutscene/subtitles" );
#elif __XENON
    asset::PARAM_path.Set( "T:/rage/assets;T:/rage/audio/sample_audio/output/XBOX360;T:/rage/assets/sample_cutscene/subtitles" );
#elif __PPU
    asset::PARAM_path.Set( "T:/rage/assets;T:/rage/audio/sample_audio/output/PS3;T:/rage/assets/sample_cutscene/subtitles" );
#endif

    sampleCutscene.Init();

    sampleCutscene.UpdateLoop();

    sampleCutscene.Shutdown();

    return 0;
}
