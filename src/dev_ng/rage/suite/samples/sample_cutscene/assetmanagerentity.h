// 
// sample_cutscene/assetmanagerentity.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_CUTSCENE_ASSETMANAGERENTITY_H
#define SAMPLE_CUTSCENE_ASSETMANAGERENTITY_H

#include "file/limits.h"
#include "cutscene/cutsentity.h"

using namespace rage;

namespace ragesamples
{

class SampleCutsceneManager;

//##############################################################################

// PURPOSE: A class for an audio entity
class SampleCutsceneAssetManagerEntity : public cutsUniqueEntity
{
public:
    SampleCutsceneAssetManagerEntity();
    SampleCutsceneAssetManagerEntity( const cutfObject* pObject );
    virtual ~SampleCutsceneAssetManagerEntity() {}

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL );

protected:
    void EntityBatchComplete();

    SampleCutsceneManager *m_pManager;
};

//##############################################################################

} // namespace ragesamples

#endif // SAMPLE_CUTSCENE_ASSETMANAGERENTITY_H
