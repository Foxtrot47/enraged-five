// 
// sample_cutscene/animentity.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_CUTSCENE_ANIMENTITY_H
#define SAMPLE_CUTSCENE_ANIMENTITY_H

#include "cutscene/cutsanimentity.h"
#include "vector/matrix34.h"

using namespace rage;

namespace ragesamples
{

class SampleAnimCamera;
class SampleAnimLight;
class SimpleWorldDrawableEntity;
class SimpleWorldRmptfxEmitter;

//##############################################################################

// PURPOSE: A sample Animation Manager Entity that implements the load
//  and unload functions.
class SampleCutsceneAnimationManagerEntity : public cutsAnimationManagerEntity
{
public:
    SampleCutsceneAnimationManagerEntity( const cutfObject *pObject );
    ~SampleCutsceneAnimationManagerEntity();

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL );

protected:
    // PURPOSE: Loads the indicated dictionary into the specified buffer
    // PARAMS:
    //    pManager - the cutsManager (provides the scene file name)
    //    iObjectId - the object that will do the loading
    //    pName - the name of the dictionary to load
    //    iBuffer - the buffer to load the dictionary into
    virtual bool LoadDictionary( cutsManager *pManager, s32 iObjectId, const char* pName, int iBuffer );

    // PURPOSE: Unloads the dictionary from the specified buffer.
    // PARAMS:
    //    pManager - the cutsManager
    //    iBuffer - the buffer to unload the dictionary from
    // RETURNS: true if we unloaded (or started to unloading)
    // NOTES: Override to implement your own dictionary unloading scheme.
    virtual bool UnloadDictionary( cutsManager *pManager, int iBuffer );

    // PURPOSE: Cancels any streaming or pending dictionary loads.  
    // PARAMS:
    //    pManager - the cutsManager (provides the API for retrieving the cutsEntity and cutfObject)
    //    pObject - the cut scene object that is associated with this entity.
    // NOTES: Streaming is not included with this module, so users should override the function appropriately.  This
    //    function will just set the "is loaded" flag to true.
    virtual bool CancelLoads( cutsManager *pManager, const cutfObject* pObject );

    // PURPOSE: Dispatch the appropriate Set Animation event to the specified object.
    // PARAMS:
    //    pManager - the cutsManager (provides the API for retrieving the cutsEntity and cutfObject)
    //    iObjectId - the object id to dispatch the event to
    //    fSectionTime - the time in the animation relative to the section start time
    //    fStartTime - the start time of the clip in terms of progress through the scene
    //    pAnimName1 - the name of the primary animation
    //    pAnimName2 - the name of the secondary animation, if any
    virtual bool SetEntityAnimation( cutsManager *pManager, s32 iObjectId, float fSectionTime, float fStartTime, 
        const char *pAnimName1, const char* pAnimName2=NULL );
};

//##############################################################################

// PURPOSE: A class for a camera entity
class SampleCutsceneAnimatedCameraEntity : public cutsAnimatedCameraEntity
{
public:
    SampleCutsceneAnimatedCameraEntity( SampleAnimCamera *pAnimCamera, const cutfObject* pObject, crmtMotionTree *pMotionTree, 
        crmtMotionTreeScheduler *pMotionTreeScheduler=NULL );
    virtual ~SampleCutsceneAnimatedCameraEntity();

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL );    

protected:
    SampleAnimCamera *m_pAnimCamera;
};

//##############################################################################

// PURPOSE: A class for an animated model entity
class SampleCutsceneAnimatedModelEntity : public cutsMotionTreeEntity
{
public:
    SampleCutsceneAnimatedModelEntity( SimpleWorldDrawableEntity *pSimpleWorldDrawableEntity, const cutfObject* pObject, 
        crmtMotionTree *pMotionTree, crmtMotionTreeScheduler *pMotionTreeScheduler=NULL );
    virtual ~SampleCutsceneAnimatedModelEntity();

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL );

#if !__FINAL
    // PURPOSE: Sets the value of debug draw.
    // PARAMS:
    //    bDraw - to draw or not to draw
    virtual void SetDebugDraw( bool bDraw );

    virtual int GetMemoryUsage() const;
#endif // !__FINAL

    // PURPOSE: Retrieves the simple world entity associated with this cut scene entity
    // RETURNS: The associated drawable entity.
    SimpleWorldDrawableEntity* GetSimpleWorldDrawableEntity() const;
    
protected:
#if !__FINAL
    int m_iMemoryUsage;
#endif // !__FINAL

    SimpleWorldDrawableEntity *m_pSimpleWorldDrawableEntity;
};

inline SimpleWorldDrawableEntity* SampleCutsceneAnimatedModelEntity::GetSimpleWorldDrawableEntity() const
{
    return m_pSimpleWorldDrawableEntity;
}

#if !__FINAL

inline int SampleCutsceneAnimatedModelEntity::GetMemoryUsage() const
{
    return m_iMemoryUsage;
}

#endif // !__FINAL

//##############################################################################

// PURPOSE: A class for a light entity
class SampleCutsceneAnimatedLightEntity : public cutsAnimatedLightEntity
{
public:
    SampleCutsceneAnimatedLightEntity( SampleAnimLight *pAnimLight, const cutfObject* pObject, crmtMotionTree *pMotionTree, 
        crmtMotionTreeScheduler *pMotionTreeScheduler=NULL );
    virtual ~SampleCutsceneAnimatedLightEntity();

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL );    

    // PURPOSE: Returns the light index so we know what grcLightGroup::grcLightSource we are
    // RETURNS: The light index.
    int GetLightIndex() const;

protected:
    SampleAnimLight *m_pAnimLight;
};

//##############################################################################

// PURPOSE: A class for an animated particle effect entity
class SampleCutsceneAnimatedParticleEffectEntity : public cutsAnimatedParticleEffectEntity
{
public:
    SampleCutsceneAnimatedParticleEffectEntity( SimpleWorldRmptfxEmitter *pSimpleWorldRmptfxEmitter, ptxEffectInst *pEventInst,
        const cutfObject* pObject, crmtMotionTree *pMotionTree, crmtMotionTreeScheduler *pMotionTreeScheduler=NULL );
    virtual ~SampleCutsceneAnimatedParticleEffectEntity();

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL );

#if !__FINAL
    virtual int GetMemoryUsage() const;
#endif // !__FINAL

    // PURPOSE: Retrieves the simple world entity associated with this cut scene entity
    // RETURNS: The associated emitter entity.
    SimpleWorldRmptfxEmitter* GetSimpleWorldRmptfxEmitter() const;

protected:
#if !__FINAL
    int m_iMemoryUsage;
#endif // !__FINAL

    SimpleWorldRmptfxEmitter *m_pSimpleWorldRmptfxEmitter;
};

inline SimpleWorldRmptfxEmitter* SampleCutsceneAnimatedParticleEffectEntity::GetSimpleWorldRmptfxEmitter() const
{
    return m_pSimpleWorldRmptfxEmitter;
}

#if !__FINAL

inline int SampleCutsceneAnimatedParticleEffectEntity::GetMemoryUsage() const
{
    return m_iMemoryUsage;
}

#endif // !__FINAL

//##############################################################################

} // namespace ragesamples

#endif // SAMPLE_CUTSCENE_ANIMENTITY_H
