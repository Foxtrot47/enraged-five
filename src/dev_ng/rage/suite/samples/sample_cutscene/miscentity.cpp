// 
// sample_cutscene/miscentity.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "miscentity.h"

#include "cutscenemanager.h"
#include "sample_cutscene.h"
#include "sampleEventDefs.h"
#include "samplecutscenechannel.h"
#include "audioengine/entity.h"
#include "audiosoundtypes/streamingsound.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfobject.h"
#include "cutscene/cutsevent.h"
#include "cutscene/cutseventargs.h"
#include "grcore/im.h"
#include "scriptgui/debugger.h"

namespace ragesamples
{

//##############################################################################

u32 SampleCutsceneAudioEntity::s_iStreamingBucketId = ~0U;

SampleCutsceneAudioEntity::SampleCutsceneAudioEntity()
#if __BANK
: m_bIsMuted(false)
#endif // __BANK
{
    for ( int i = 0; i < c_numBuffers; ++i )
    {
        m_pSound[i] = NULL;
    }

    m_audEntity.Init();

    // For our purposes in this sample, we'll just grab the next available bucket in the pool.
    if ( s_iStreamingBucketId == ~0U )
    {
        s_iStreamingBucketId = audSound::GetStaticPool().GetNextReservedBucketId();
    }
}

SampleCutsceneAudioEntity::SampleCutsceneAudioEntity( const cutfObject* pObject )
: cutsAudioEntity( pObject )
#if __BANK
, m_bIsMuted(false)
#endif // __BANK
{
    for ( int i = 0; i < c_numBuffers; ++i )
    {
        m_pSound[i] = NULL;
    }

    m_audEntity.Init();

    // For our purposes in this sample, we'll just grab the next available bucket in the pool.
    if ( s_iStreamingBucketId == ~0U )
    {
        s_iStreamingBucketId = audSound::GetStaticPool().GetNextReservedBucketId();
    }
}

void SampleCutsceneAudioEntity::DispatchEvent( cutsManager* pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs )
{
    switch ( iEventId )
    {
    case CUTSCENE_LOAD_AUDIO_EVENT:
        {
            const cutfNameEventArgs *pNameEventArgs = dynamic_cast<const cutfNameEventArgs *>( pEventArgs );
            if ( pNameEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to start at %f seconds.", pManager->GetDisplayTime(), 
                    cutfEvent::GetDisplayName( iEventId ), pNameEventArgs->GetName(), pManager->GetAudioStartTime() );
            }
        }
        break;
    case CUTSCENE_UNLOAD_AUDIO_EVENT:
        {
            const cutfNameEventArgs *pNameEventArgs = dynamic_cast<const cutfNameEventArgs *>( pEventArgs );
            if ( pNameEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pNameEventArgs->GetName() );
            }
        }
        break;
    case CUTSCENE_PLAY_AUDIO_EVENT:
        {
            const cutfNameEventArgs *pNameEventArgs = dynamic_cast<const cutfNameEventArgs *>( pEventArgs );
            if ( pNameEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pNameEventArgs->GetName() );
            }
        }
        break;
    case CUTSCENE_STOP_AUDIO_EVENT:
        {
            const cutfNameEventArgs *pNameEventArgs = dynamic_cast<const cutfNameEventArgs *>( pEventArgs );
            if ( pNameEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pNameEventArgs->GetName() );
            }
        }
        break;
    case CUTSCENE_PLAY_EVENT:
        {
            if ( WasPaused() )
            {
                sampleCutsDisplayf( "%s Re-buffering audio '%s' to start at %f seconds.", 
                    pManager->GetDisplayTime(), GetAudioName(), pManager->GetAudioSeconds() );
            }
        }
        break;
#if __BANK
    case CUTSCENE_MUTE_AUDIO_EVENT:
    case CUTSCENE_UNMUTE_AUDIO_EVENT:
        {
            sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );
        }
        break;
#endif // __BANK
    case CUTSCENE_START_OF_SCENE_EVENT:
    case CUTSCENE_END_OF_SCENE_EVENT:
    case CUTSCENE_STOP_EVENT:
    case CUTSCENE_UPDATE_EVENT:
    case CUTSCENE_UPDATE_LOADING_EVENT:
    case CUTSCENE_UPDATE_UNLOADING_EVENT:
    case CUTSCENE_UNLOADED_EVENT:
    case CUTSCENE_PAUSE_EVENT:
    case CUTSCENE_CANCEL_LOAD_EVENT:
    case CUTSCENE_SCENE_ORIENTATION_CHANGED_EVENT:
    case CUTSCENE_STEP_BACKWARD_EVENT:
    case CUTSCENE_STEP_FORWARD_EVENT:
    case CUTSCENE_REWIND_EVENT:
    case CUTSCENE_FAST_FORWARD_EVENT:
    case CUTSCENE_RESTART_EVENT:
        {
            // handle silently
        }
        break;
    default:
        {
            sampleCutsDisplayf( "%s Event '%s' (id=%d) not supported on entity type '%s'.", pManager->GetDisplayTime(), 
                cutfEvent::GetDisplayName( iEventId ), iEventId, pObject->GetTypeName() );
        }
        break;
    }

    cutsAudioEntity::DispatchEvent( pManager, pObject, iEventId, pEventArgs );
}

float SampleCutsceneAudioEntity::GetPlayTime( float fSeconds, int iBuffer )
{
    if ( m_pSound[iBuffer] != NULL )
    {
        audStreamingSound *pStreamingSound = dynamic_cast<audStreamingSound *>( m_pSound[iBuffer] );
        return pStreamingSound->GetCurrentPlayTimeOfWave( (int)(fSeconds * 1000.0f) ) / 1000.0f;
    }

    return -1.0f;
}

audPrepareState SampleCutsceneAudioEntity::Prepare( const char *pName, float fStartOffset, int iBuffer )
{
    if ( m_pSound[iBuffer] == NULL )
    {
        audSoundInitParams initParams;
        initParams.StartOffset = (int)(fStartOffset * 1000.0f);
        Assign(initParams.BucketId, s_iStreamingBucketId);

#if __BANK
        if ( m_bIsMuted )
        {
            initParams.Volume = -100.0f;
        }
        else
        {
            initParams.Volume = 0.0f;
        }
#else
        initParams.Volume = 0.0f;
#endif // __BANK

        m_audEntity.CreateSound_PersistentReference( pName, &(m_pSound[iBuffer]), &initParams );
    }
    
    if ( m_pSound[iBuffer] != NULL )
    {
        audWaveSlot *slot = audWaveSlot::FindWaveSlot( "STREAM_SLOT" );
        if ( !sampleCutsVerifyf( slot, "Could not find the Wave Slot named 'STREAM_SLOT'." ) )
        {
            return AUD_PREPARE_FAILED;
        }

        return m_pSound[iBuffer]->Prepare( slot, true );
    }

    return AUD_PREPARE_FAILED;
}

void SampleCutsceneAudioEntity::Play( int iBuffer )
{
    if ( m_pSound[iBuffer] != NULL )
    {
        m_pSound[iBuffer]->Play();
    }
}

void SampleCutsceneAudioEntity::Stop( int iBuffer )
{
    if ( m_pSound[iBuffer] != NULL )
    {
        m_pSound[iBuffer]->StopAndForget();
        m_pSound[iBuffer] = NULL;
    }
}

#if __BANK

void SampleCutsceneAudioEntity::Mute( int iBuffer )
{
    if ( !m_bIsMuted && (m_pSound[iBuffer] != NULL) )
    {
        m_pSound[iBuffer]->SetRequestedVolume( -100.0f );
    }

    m_bIsMuted = true;
}

void SampleCutsceneAudioEntity::Unmute( int iBuffer )
{
    if ( m_bIsMuted && (m_pSound[iBuffer] != NULL) )
    {
        m_pSound[iBuffer]->SetRequestedVolume( 0.0f );
    }

    m_bIsMuted = false;
}

#endif // __BANK

//##############################################################################

SampleCutsceneSubtitleEntity::SampleCutsceneSubtitleEntity()
{

}

SampleCutsceneSubtitleEntity::SampleCutsceneSubtitleEntity( atArray<const cutfObject*>& pObjectList )
: cutsSingletonEntity(pObjectList)
{

}

void SampleCutsceneSubtitleEntity::DispatchEvent( cutsManager *pManager, const cutfObject* NOTFINAL_ONLY( pObject ), 
                                                 s32 iEventId, const cutfEventArgs* pEventArgs )
{
    SampleCutsceneManager *pCutsceneMgr = dynamic_cast<SampleCutsceneManager *>( pManager );
    NOTFINAL_ONLY( const cutfSubtitleObject *pSubtitleObject = dynamic_cast<const cutfSubtitleObject *>( pObject ); )

    switch ( iEventId )
    {
    case CUTSCENE_SHOW_SUBTITLE_EVENT:
        {
            const cutfSubtitleEventArgs *pSubtitleEventArgs = dynamic_cast<const cutfSubtitleEventArgs *>( pEventArgs );
            if ( (pSubtitleEventArgs->GetLanguageID() == -1)
                || (pSubtitleEventArgs->GetLanguageID() == pCutsceneMgr->GetSampleCutscene()->GetCurrentLanguage()) )
            {
                if ( !pCutsceneMgr->GetSampleCutscene()->ShowSubtitle( 
                    pSubtitleEventArgs->GetName(), pSubtitleEventArgs->GetTransitionIn(), pSubtitleEventArgs->GetTransitionInDuration() ) )
                {
                    sampleCutsWarningf( "%s Subtitle '%s' for %s not found.", pManager->GetDisplayTime(), pSubtitleEventArgs->GetName(),
                        pSubtitleObject->GetName() );
                }
                else
                {
                    sampleCutsDisplayf( "%s %s %s: '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                        pSubtitleObject->GetName(), pSubtitleEventArgs->GetName() );
                }
            }
        }
        break;
    case CUTSCENE_HIDE_SUBTITLE_EVENT:
        {
            const cutfSubtitleEventArgs *pSubtitleEventArgs = dynamic_cast<const cutfSubtitleEventArgs *>( pEventArgs );
            if ( (pSubtitleEventArgs->GetLanguageID() == -1) 
                || (pSubtitleEventArgs->GetLanguageID() == pCutsceneMgr->GetSampleCutscene()->GetCurrentLanguage()) )
            {
                if ( !pCutsceneMgr->GetSampleCutscene()->HideSubtitle( 
                    pSubtitleEventArgs->GetName(), pSubtitleEventArgs->GetTransitionOut(), pSubtitleEventArgs->GetTransitionOutDuration() ) )
                {
                    sampleCutsWarningf( "%s Subtitle '%s' for %s not found.", pManager->GetDisplayTime(), pSubtitleEventArgs->GetName(),
                        pSubtitleObject->GetName() );
                }
                else
                {
                    sampleCutsDisplayf( "%s %s %s: '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                        pSubtitleObject->GetName(), pSubtitleEventArgs->GetName() );
                }
            }
        }
        break;
    case CUTSCENE_END_OF_SCENE_EVENT:
    case CUTSCENE_RESTART_EVENT:
    case CUTSCENE_STOP_EVENT:
        {
            pCutsceneMgr->GetSampleCutscene()->HideSubtitles();
        }
        break;
    case CUTSCENE_START_OF_SCENE_EVENT:
    case CUTSCENE_PLAY_EVENT:
    case CUTSCENE_UPDATE_EVENT:
    case CUTSCENE_UPDATE_LOADING_EVENT:
    case CUTSCENE_UPDATE_UNLOADING_EVENT:
    case CUTSCENE_PAUSE_EVENT:
    case CUTSCENE_SCENE_ORIENTATION_CHANGED_EVENT:
    case CUTSCENE_STEP_BACKWARD_EVENT:
    case CUTSCENE_STEP_FORWARD_EVENT:
    case CUTSCENE_REWIND_EVENT:
    case CUTSCENE_FAST_FORWARD_EVENT:
        {
            // handle silently
        }
        break;
    default:
        {
            sampleCutsDisplayf( "%s Event '%s' (id=%d) not supported on entity type '%s'.", pManager->GetDisplayTime(), 
                cutfEvent::GetDisplayName( iEventId ), iEventId, pObject->GetTypeName() );
        }
        break;
    }
}

//##############################################################################

SampleCutsceneScreenFadeEntity::SampleCutsceneScreenFadeEntity()
{

}

SampleCutsceneScreenFadeEntity::SampleCutsceneScreenFadeEntity( const cutfObject* pObject )
: cutsUniqueEntity(pObject)
{

}

void SampleCutsceneScreenFadeEntity::DispatchEvent( cutsManager *pManager, const cutfObject* NOTFINAL_ONLY( pObject ), 
                                                   s32 iEventId, const cutfEventArgs* pEventArgs )
{
    SampleCutsceneManager *pCutsceneMgr = dynamic_cast<SampleCutsceneManager *>( pManager );

    switch ( iEventId )
    {
    case CUTSCENE_FADE_OUT_EVENT:
        {
            const cutfScreenFadeEventArgs *pScreenFadeEventArgs = dynamic_cast<const cutfScreenFadeEventArgs *>( pEventArgs );
            if ( pScreenFadeEventArgs != NULL )
            {
                pCutsceneMgr->GetSampleCutscene()->GetScreenFadeManager().FadeOut( pManager->GetSeconds(),
                    pScreenFadeEventArgs->GetColor(), pScreenFadeEventArgs->GetFloat1() );

                pCutsceneMgr->GetSampleCutscene()->GetScreenFadeManager().Pause();

                sampleCutsDisplayf( "%s %s %f seconds to (%d,%d,%d,%d).", 
                    pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pScreenFadeEventArgs->GetFloat1(), pScreenFadeEventArgs->GetColor().GetRed(), 
                    pScreenFadeEventArgs->GetColor().GetGreen(), pScreenFadeEventArgs->GetColor().GetBlue(), 
                    pScreenFadeEventArgs->GetColor().GetAlpha() );
            }
        }
        break;
    case CUTSCENE_FADE_IN_EVENT:
        {
            const cutfScreenFadeEventArgs *pScreenFadeEventArgs = dynamic_cast<const cutfScreenFadeEventArgs *>( pEventArgs );
            if ( pScreenFadeEventArgs != NULL )
            {
                pCutsceneMgr->GetSampleCutscene()->GetScreenFadeManager().FadeIn( pManager->GetSeconds(),
                    pScreenFadeEventArgs->GetColor(), pScreenFadeEventArgs->GetFloat1() );

                if ( pManager->IsRunning() )
                {
                    pCutsceneMgr->GetSampleCutscene()->GetScreenFadeManager().Pause();
                }
                else
                {
                    // if this is a game fade in, we need to tell it to update itself
                    pCutsceneMgr->GetSampleCutscene()->GetScreenFadeManager().Play();
                }

                sampleCutsDisplayf( "%s %s %f seconds from (%d,%d,%d,%d).", 
                    pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pScreenFadeEventArgs->GetFloat1(), pScreenFadeEventArgs->GetColor().GetRed(), 
                    pScreenFadeEventArgs->GetColor().GetGreen(), pScreenFadeEventArgs->GetColor().GetBlue(), 
                    pScreenFadeEventArgs->GetColor().GetAlpha() );
            }
        }
        break;
    case CUTSCENE_RESTART_EVENT:
        {
            pCutsceneMgr->GetSampleCutscene()->GetScreenFadeManager().Stop();
        }
        break;
    case CUTSCENE_PAUSE_EVENT:
        {
            pCutsceneMgr->GetSampleCutscene()->GetScreenFadeManager().Pause();
        }
        break;
    case CUTSCENE_UPDATE_FADING_OUT_AT_BEGINNING_EVENT:
    case CUTSCENE_UPDATE_EVENT:
        {
            const cutsUpdateEventArgs *pUpdateEventArgs = dynamic_cast<const cutsUpdateEventArgs *>( pEventArgs );
            if ( pEventArgs != NULL )
            {
                pCutsceneMgr->GetSampleCutscene()->GetScreenFadeManager().SetCurrentTime( pUpdateEventArgs->GetSeconds() );
            }
        }
        break;
    case CUTSCENE_START_OF_SCENE_EVENT:
    case CUTSCENE_END_OF_SCENE_EVENT:
    case CUTSCENE_UPDATE_LOADING_EVENT:
    case CUTSCENE_UPDATE_UNLOADING_EVENT:
    case CUTSCENE_UNLOADED_EVENT:
    case CUTSCENE_PLAY_EVENT:
    case CUTSCENE_STOP_EVENT:
    case CUTSCENE_CANCEL_LOAD_EVENT:
    case CUTSCENE_SCENE_ORIENTATION_CHANGED_EVENT:
    case CUTSCENE_STEP_BACKWARD_EVENT:
    case CUTSCENE_STEP_FORWARD_EVENT:
    case CUTSCENE_REWIND_EVENT:
    case CUTSCENE_FAST_FORWARD_EVENT:
        {
            // handle silently
        }
        break;
    default:
        {
            sampleCutsDisplayf( "%s Event '%s' (id=%d) not supported on entity type '%s'.", pManager->GetDisplayTime(), 
                cutfEvent::GetDisplayName( iEventId ), iEventId, pObject->GetTypeName() );
        }
        break;
    }
}

//##############################################################################

SampleCutsceneEventEntity::SampleCutsceneEventEntity()
{

}

SampleCutsceneEventEntity::SampleCutsceneEventEntity( const cutfObject* pObject )
: cutsUniqueEntity(pObject)
{

}

void SampleCutsceneEventEntity::DispatchEvent( cutsManager *pManager, const cutfObject* /*pObject*/, s32 iEventId, const cutfEventArgs* pEventArgs )
{
    SampleCutsceneManager *pSampleCutsceneManager = dynamic_cast<SampleCutsceneManager *>( pManager );

    switch ( iEventId )
    {
    case CUTSCENE_WAIT_FOR_USER_INPUT_EVENT:
        {
            pSampleCutsceneManager->GetSampleCutscene()->WaitForUserInput();

            sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );
        }
        break;
    case CUTSCENE_DISPLAY_USER_INPUT_MESSAGE_EVENT:
        {
            const ICutsceneUserInputMessageCustomEventArgs *pUserInputMessageEventArgs 
                = static_cast<const ICutsceneUserInputMessageCustomEventArgs *>( pEventArgs );
            pSampleCutsceneManager->GetSampleCutscene()->SetWaitForUserInputMessage( pUserInputMessageEventArgs->GetText() );

            sampleCutsDisplayf( "%s %s '%s'", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                pUserInputMessageEventArgs->GetText() );
        }
        break;
    case CUTSCENE_START_SCRIPT_EVENT:
        {
            const ICutsceneScriptCustomEventArgs *pScriptEventArgs = static_cast<const ICutsceneScriptCustomEventArgs *>( pEventArgs );
            char cScriptFile[RAGE_MAX_PATH];
            fiAssetManager::RemoveExtensionFromPath( cScriptFile, sizeof(cScriptFile), pScriptEventArgs->GetName() );    

            sampleCutsDisplayf( "%s %s '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                pScriptEventArgs->GetName() );

            scrThreadId id = scrThread::CreateThread( cScriptFile );
            if ( id != 0 )
            {
#if __BANK
                scrDebugger::CreateDebugger( cScriptFile, id );
#endif

                // NOTE: Normally, a game-team knows exactly which script contains the global variables and loads that first,
                // but for this viewer and subsequent samples, let's make it easy on the user.
                if ( ASSET.Exists( cScriptFile, "sgv" ) )
                {
                    scrThread::LoadGlobalVariableInfo( cScriptFile );
                }

                m_scriptFileToThreadIdMap.Insert( ConstString(pScriptEventArgs->GetName()), id );
            }
            else
            {
                sampleCutsWarningf( "%s Unable to start the script '%s'.", pManager->GetDisplayTime(), pScriptEventArgs->GetName() );
            }
        }
        break;
    case CUTSCENE_STOP_SCRIPT_EVENT:
        {
            const ICutsceneScriptCustomEventArgs *pScriptEventArgs = static_cast<const ICutsceneScriptCustomEventArgs *>( pEventArgs );
            
            sampleCutsDisplayf( "%s %s '%s'", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                pScriptEventArgs->GetName() );

            scrThreadId *pId = m_scriptFileToThreadIdMap.Access( pScriptEventArgs->GetName() );
            if ( pId == NULL )
            {
                sampleCutsWarningf( "%s The script '%s' has already been stopped or was never started.", 
                    pManager->GetDisplayTime(), pScriptEventArgs->GetName() );
            }
            else
            {
                m_scriptFileToThreadIdMap.Delete( pScriptEventArgs->GetName() );

#if __BANK
                scrDebugger::KillDebugger( *pId );
#endif // __BANK
                scrThread::KillThread( *pId );
            }
        }
        break;
    default:
        {
            // handle silently
        }
        break;
    }
}

//#############################################################################

SampleCutsceneBlockingBoundsEntity::SampleCutsceneBlockingBoundsEntity( SampleBlockingBounds *pBlockingBounds, const cutfObject* pObject )
: cutsBoundsEntity(pObject)
, m_pBlockingBounds(pBlockingBounds)
{

}

void SampleCutsceneBlockingBoundsEntity::DispatchEvent( cutsManager* pManager, const cutfObject* pObject,  
                                                       s32 iEventId, const cutfEventArgs* pEventArgs )
{
    switch ( iEventId )
    {
    case CUTSCENE_START_OF_SCENE_EVENT:
        {
            Matrix34 m;
            pManager->GetSceneOrientationMatrix( m );
            m_pBlockingBounds->GetTransform().Set( m );
        }
        break;
    case CUTSCENE_ACTIVATE_BLOCKING_BOUNDS_EVENT:
        {
            m_pBlockingBounds->SetIsActive( true );

            sampleCutsDisplayf( "%s %s '%s'", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                pObject->GetDisplayName().c_str() );
        }
        break;
    case CUTSCENE_DEACTIVATE_BLOCKING_BOUNDS_EVENT:
        {
            m_pBlockingBounds->SetIsActive( false );

            sampleCutsDisplayf( "%s %s '%s'", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                pObject->GetDisplayName().c_str() );
        }
        break;
    case CUTSCENE_END_OF_SCENE_EVENT:
        {
            m_pBlockingBounds->SetIsActive( false );
        }
        break;
    case CUTSCENE_SCENE_ORIENTATION_CHANGED_EVENT:
        {
            Matrix34 m;
            pManager->GetSceneOrientationMatrix( m );
            m_pBlockingBounds->GetTransform().Set( m );
        }
        break;
    default:
        {
            // handle silently
        }
        break;
    }

    cutsBoundsEntity::DispatchEvent( pManager, pObject, iEventId, pEventArgs );
}

//##############################################################################

SampleCutsceneRemovalBoundsEntity::SampleCutsceneRemovalBoundsEntity( SampleRemovalBounds *pRemovalBounds, const cutfObject* pObject )
: cutsBoundsEntity(pObject)
, m_pRemovalBounds(pRemovalBounds)
{

}

void SampleCutsceneRemovalBoundsEntity::DispatchEvent( cutsManager* pManager, const cutfObject* pObject, 
                                                      s32 iEventId, const cutfEventArgs* pEventArgs )
{
    switch ( iEventId )
    {
    case CUTSCENE_START_OF_SCENE_EVENT:
        {
            Matrix34 m;
            pManager->GetSceneOrientationMatrix( m );
            m_pRemovalBounds->GetTransform().Set( m );
            
            m_pRemovalBounds->SetIsActive( true );
        }
        break;
    case CUTSCENE_END_OF_SCENE_EVENT:
        {
            m_pRemovalBounds->SetIsActive( false );
        }
        break;
    case CUTSCENE_SCENE_ORIENTATION_CHANGED_EVENT:
        {
            Matrix34 m;
            pManager->GetSceneOrientationMatrix( m );
            m_pRemovalBounds->GetTransform().Set( m );
        }
        break;
    default:
        {
            // handle silently
        }
        break;
    }

    cutsBoundsEntity::DispatchEvent( pManager, pObject, iEventId, pEventArgs );
}

//##############################################################################

} // namespace ragesamples
