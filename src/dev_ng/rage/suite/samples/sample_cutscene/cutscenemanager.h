// 
// sample_cutscene/cutscenemanager.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_CUTSCENE_CUTSCENEMANAGER_H
#define SAMPLE_CUTSCENE_CUTSCENEMANAGER_H

#include "cutscene/cutsmanager.h"
#include "grcore/light.h" // for grcLightGroup::MAX_LIGHTS

using namespace rage;

namespace ragesamples
{

class SampleCutscene;
class SampleCutsceneSubtitleEntity;

class SampleCutsceneManager : public cutsManager
{
public:
    SampleCutsceneManager();
    ~SampleCutsceneManager();

    SampleCutscene* GetSampleCutscene() const;

    void SetSampleCutscene( SampleCutscene *pSampleCutscene );

    const char* GetAssetDir() const;

    void SetAssetDir( const char* pAssetDir );

protected:
    // PURPOSE: Reserves an entity for use by the cut scene manager that corresponds 
    //    to the given cut scene object.
    // PARAMS:
    //    pObject - the object to return a cut scene entity for
    // RETURNS: A cut scene entity.
    // NOTES: Depending on your setup, you may be creating an entity here, or merely 
    //    returning the pointer to one you've already created.  In addition, a single
    //    cut scene entity can be responsible for receiving the events for more than
    //    one cut scene object.
    virtual cutsEntity* ReserveEntity( const cutfObject* pObject );

    // PURPOSE: Releases an entity that the cut scene manager no longer needs
    // PARAMS:
    //    pEntity - the entity to release.  For the case of "singleton"
    //      entities, the entity itself should never be deleted until the last 
    //      cut scene object that is owns is released.
    //    pObject - the entity's associated object.  
    virtual void ReleaseEntity( cutsEntity *pEntity, const cutfObject* pObject );

#if __BANK

    // PURPOSE: Retrieves a new Hidden Object SEditCutfObjectLocationInfo for the currently selected item in the game world.
    // RETURNS: An SEditCutfObjectLocationInfo instance, or NULL.
    virtual SEditCutfObjectLocationInfo* GetHiddenObjectInfo();

    // PURPOSE: Retrieves a new Fixup Object SEditCutfObjectLocationInfo for the currently selected item in the game world.
    // RETURNS: An SEditCutfObjectLocationInfo instance, or NULL.
    virtual SEditCutfObjectLocationInfo* GetFixupObjectInfo();

#endif // __BANK

private:
    SampleCutscene *m_pSampleCutscene;
    char m_cAssetDir[RAGE_MAX_PATH];

    SampleCutsceneSubtitleEntity *m_pSubtitleEntity;
};

inline SampleCutscene* SampleCutsceneManager::GetSampleCutscene() const
{
    return m_pSampleCutscene;
}

inline void SampleCutsceneManager::SetSampleCutscene( SampleCutscene *pSampleCutscene )
{
    m_pSampleCutscene = pSampleCutscene;
}

inline const char* SampleCutsceneManager::GetAssetDir() const
{
    return m_cAssetDir;
}

inline void SampleCutsceneManager::SetAssetDir( const char* pAssetDir )
{
    if ( pAssetDir != NULL )
    {
        safecpy( m_cAssetDir, pAssetDir, sizeof(m_cAssetDir) );
    }
    else
    {
        m_cAssetDir[0] = 0;
    }
}

} // namespace ragesamples

#endif // SAMPLE_CUTSCENE_CUTSCENEMANAGER_H
