// 
// sample_cutscene/animentity.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "animentity.h"

#include "sample_cutscene.h"
#include "samplecutscenechannel.h"

#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "creature/componentcamera.h"
#include "cutscene/cutsevent.h"
#include "cutscene/cutseventargs.h"
#include "cutscene/cutsmanager.h"
#include "rmptfx/ptxeffectinst.h"
#include "sample_simpleworld/animmgr.h"

using namespace rage;

namespace ragesamples
{

//##############################################################################

SampleCutsceneAnimationManagerEntity::SampleCutsceneAnimationManagerEntity( const cutfObject *pObject )
: cutsAnimationManagerEntity(pObject)
{

}

SampleCutsceneAnimationManagerEntity::~SampleCutsceneAnimationManagerEntity()
{

}

void SampleCutsceneAnimationManagerEntity::DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, 
                                                         const cutfEventArgs* pEventArgs )
{
    // This is just for the purposes of reporting
    switch ( iEventId )
    {
    case CUTSCENE_LOAD_ANIM_DICT_EVENT:
    case CUTSCENE_UNLOAD_ANIM_DICT_EVENT:
    case CUTSCENE_SET_ANIM_EVENT:
    case CUTSCENE_CLEAR_ANIM_EVENT:
    case CUTSCENE_START_OF_SCENE_EVENT:
    case CUTSCENE_END_OF_SCENE_EVENT:
    case CUTSCENE_PLAY_EVENT:
    case CUTSCENE_UPDATE_EVENT:
    case CUTSCENE_UPDATE_LOADING_EVENT:
    case CUTSCENE_UPDATE_UNLOADING_EVENT:
    case CUTSCENE_UNLOADED_EVENT:
    case CUTSCENE_PAUSE_EVENT:
    case CUTSCENE_STOP_EVENT:
    case CUTSCENE_CANCEL_LOAD_EVENT:
    case CUTSCENE_STEP_BACKWARD_EVENT:
    case CUTSCENE_STEP_FORWARD_EVENT:
    case CUTSCENE_REWIND_EVENT:
    case CUTSCENE_FAST_FORWARD_EVENT:
    case CUTSCENE_RESTART_EVENT:
    case CUTSCENE_SCENE_ORIENTATION_CHANGED_EVENT:
        {
            // handle silently
        }
        break;
    default:
        {
            sampleCutsDisplayf( "%s Event '%s' (id=%d) not supported on entity type '%s'.", 
                pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ), iEventId, pObject->GetTypeName() );
        }
        break;
    }

    cutsAnimationManagerEntity::DispatchEvent( pManager, pObject, iEventId, pEventArgs );
}

bool SampleCutsceneAnimationManagerEntity::LoadDictionary( cutsManager *pManager, s32 iObjectId, const char* pName, int iBuffer )
{
    if ( !cutsAnimationManagerEntity::LoadDictionary( pManager, iObjectId, pName, iBuffer ) )
    {
        sampleCutsWarningf( "%s Could not load the animation dictionary '%s'.", pManager->GetDisplayTime(), pName );
        return false;
    }

    sampleCutsDisplayf( "%s Load animation dictionary '%s'.", pManager->GetDisplayTime(), pName );
    return true;
}

bool SampleCutsceneAnimationManagerEntity::UnloadDictionary( cutsManager *pManager, int iBuffer )
{
    char cAnimDictName[CUTSCENE_OBJNAMELEN];
    safecpy( cAnimDictName, m_dictionaryData[iBuffer].cName, sizeof(cAnimDictName) );

    if ( !cutsAnimationManagerEntity::UnloadDictionary( pManager, iBuffer ) )
    {
        sampleCutsWarningf( "%s Could not unload the animation dictionary '%s'.", pManager->GetDisplayTime(), cAnimDictName );
        return false;
    }

    sampleCutsDisplayf( "%s Unload animation dictionary '%s'.", pManager->GetDisplayTime(), cAnimDictName );
    return true;
}

bool SampleCutsceneAnimationManagerEntity::CancelLoads( cutsManager *pManager, const cutfObject* pObject )
{
    if ( cutsAnimationManagerEntity::CancelLoads( pManager, pObject ) )
    {
        sampleCutsDisplayf( "%s Cancelled Anim Dictionary load(s).", pManager->GetDisplayTime() );
        return true;
    }

    return false;
}

bool SampleCutsceneAnimationManagerEntity::SetEntityAnimation( cutsManager *pManager, s32 iObjectId, float fSectionTime, float fStartTime,
                                                              const char *pAnimName1, const char* pAnimName2 )
{
    if ( !cutsAnimationManagerEntity::SetEntityAnimation( pManager, iObjectId, fSectionTime, fStartTime, pAnimName1, pAnimName2 ) )
    {
        if ( (pAnimName2 != NULL) && (strlen( pAnimName2 ) > 0) )
        {
            sampleCutsWarningf( "Could not locate a clip or animation named '%s' or '%s'.", pAnimName1, pAnimName2 );
        }
        else
        {
            sampleCutsWarningf( "Could not locate a clip or animation named '%s'.", pAnimName1 );
        }

        return false;
    }

    return true;
}

//##############################################################################

SampleCutsceneAnimatedCameraEntity::SampleCutsceneAnimatedCameraEntity( SampleAnimCamera *pAnimCamera, const cutfObject* pObject, 
                                                                       crmtMotionTree *pMotionTree, 
                                                                       crmtMotionTreeScheduler *pMotionTreeScheduler )
: cutsAnimatedCameraEntity( pObject, pMotionTree, pMotionTreeScheduler )
, m_pAnimCamera( pAnimCamera )
{

}

SampleCutsceneAnimatedCameraEntity::~SampleCutsceneAnimatedCameraEntity()
{

}

void SampleCutsceneAnimatedCameraEntity::DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, 
                                                       const cutfEventArgs* pEventArgs )
{
    switch ( iEventId )
    {
    case CUTSCENE_START_OF_SCENE_EVENT:
        {
            Matrix34 m;
            pManager->GetSceneOrientationMatrix( m );
            m_pAnimCamera->GetTransform().Set( m );

            m_pAnimCamera->GetAnimMgr()->SetPlayMode( RvAnimMgr::ANIM_PAUSE );
            m_pAnimCamera->SetIsActive( true );
            m_pAnimCamera->SetDepthOfFieldEnabled( pManager->IsDepthOfFieldEnabled() );
            m_pAnimCamera->SetFaceZoomEnabled( false );
        }
        break;
    case CUTSCENE_END_OF_SCENE_EVENT:
        {
            m_pAnimCamera->SetIsActive( false );
            m_pAnimCamera->SetFaceZoomEnabled( false );
        }
        break;
    case CUTSCENE_SET_DRAW_DISTANCE_EVENT:
        {
            const cutfTwoFloatValuesEventArgs *pTwoFloatsEventArgs = dynamic_cast<const cutfTwoFloatValuesEventArgs *>( pEventArgs );
            if ( pEventArgs != NULL )
            {
                if ( m_pCameraComponent != NULL )
                {
                    // only spew TTY when it changes because in __BANK mode, this event is received every frame for editing purposes.
                    if ( (m_pCameraComponent->GetNearClip() != pTwoFloatsEventArgs->GetFloat1()) 
                        || (m_pCameraComponent->GetFarClip() != pTwoFloatsEventArgs->GetFloat2()) )
                    {
                        sampleCutsDisplayf( "%s %s (%.03f,%.03f).", pManager->GetDisplayTime(),
                            cutfEvent::GetDisplayName( iEventId ), pTwoFloatsEventArgs->GetFloat1(), pTwoFloatsEventArgs->GetFloat2() );
                    }
                }
            }
        }
        break;
#if !__FINAL
    case CUTSCENE_SET_FACE_ZOOM_EVENT:
        {
            const cutfObjectIdEventArgs *pObjectIdEventArgs = dynamic_cast<const cutfObjectIdEventArgs *>( pEventArgs );
            if ( pObjectIdEventArgs != NULL )
            {
                cutsEntity *pEntity = pManager->GetEntityByObjectId( pObjectIdEventArgs->GetObjectId() );
                if ( pEntity != NULL )
                {                    
                    const SampleCutsceneAnimatedModelEntity *pModelEntity 
                        = dynamic_cast<const SampleCutsceneAnimatedModelEntity *>( pEntity );
                    if ( pModelEntity && pModelEntity->GetSimpleWorldDrawableEntity() && pModelEntity->GetSimpleWorldDrawableEntity()->GetSkeleton() )
                    {
                        SetFaceZoomSkeleton( pModelEntity->GetSimpleWorldDrawableEntity()->GetSkeleton() );
                        m_pAnimCamera->SetFaceZoomEnabled( true );
                        
                        sampleCutsDisplayf( "%s %s '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                            pModelEntity->GetSimpleWorldDrawableEntity()->GetIdentifier().c_str() );
                    }
                }
            }
        }
        break;
    case CUTSCENE_CLEAR_FACE_ZOOM_EVENT:
        {
            SetFaceZoomSkeleton( NULL );
            m_pAnimCamera->SetFaceZoomEnabled( false );

            sampleCutsDisplayf( "%s %s.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );
        }
        break;
#endif // !__FINAL
    case CUTSCENE_SCENE_ORIENTATION_CHANGED_EVENT:
        {
            Matrix34 m;
            pManager->GetSceneOrientationMatrix( m );
            m_pAnimCamera->GetTransform().Set( m );
        }
        break;
    case CUTSCENE_SET_ANIMATION_EVENT:
        {
            const cutsAnimationEventArgs *pAnimEventArgs = dynamic_cast<const cutsAnimationEventArgs *>( pEventArgs );
            if ( pAnimEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pAnimEventArgs->GetAnimation()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_ANIMATION_EVENT:
        {
            const cutsDualAnimationEventArgs *pDualAnimEventArgs = dynamic_cast<const cutsDualAnimationEventArgs *>( pEventArgs );
            if ( pDualAnimEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s' and '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pDualAnimEventArgs->GetAnimation()->GetName(), pDualAnimEventArgs->GetAnimation2()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_CLIP_EVENT:
        {
            const cutsClipEventArgs *pClipEventArgs = dynamic_cast<const cutsClipEventArgs *>( pEventArgs );
            if ( pClipEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pClipEventArgs->GetClip()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_CLIP_EVENT:
        {
            const cutsDualClipEventArgs *pDualClipEventArgs = dynamic_cast<const cutsDualClipEventArgs *>( pEventArgs );
            if ( pDualClipEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s' and '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pDualClipEventArgs->GetClip()->GetName(),
                     pDualClipEventArgs->GetClip2()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_CLIP_ANIM_EVENT:
        {           
            const cutsDualClipAnimEventArgs *pDualClipAnimEventArgs = dynamic_cast<const cutsDualClipAnimEventArgs *>( pEventArgs );
            if ( pDualClipAnimEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s' and '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pDualClipAnimEventArgs->GetClip()->GetName(),
                    pDualClipAnimEventArgs->GetAnimation()->GetName() );
            }
        }
        break;
    case CUTSCENE_CLEAR_ANIM_EVENT:
        {
            sampleCutsDisplayf( "%s %s '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                pObject->GetDisplayName().c_str() );
        }
        break;
    case CUTSCENE_UPDATE_EVENT:
        {
#if !__FINAL
            if ( IsFaceZoomEnabled() )
            {
                m_pAnimCamera->SetFaceZoomMatrix( GetFaceZoomMatrix(), GetFaceZoomNearDrawDistance(), GetFaceZoomFarDrawDistance() );
            }
#endif // !__FINAL
        }
        break;
    case CUTSCENE_CAMERA_CUT_EVENT:
        {
            const cutfCameraCutEventArgs *pCameraCutEventArgs = dynamic_cast<const cutfCameraCutEventArgs *>( pEventArgs );
            if ( pCameraCutEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s'", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pCameraCutEventArgs->GetName() );
            }
            else
            {
                sampleCutsWarningf( "%s %s. (Old Format.  Please re-export your cutscenes.)", pManager->GetDisplayTime(), 
                    cutfEvent::GetDisplayName( iEventId ) );
            }
        }
        break;
    case CUTSCENE_ENABLE_DEPTH_OF_FIELD_EVENT:
        {
            sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );

            m_pAnimCamera->SetDepthOfFieldEnabled( true );
        }
        break;
    case CUTSCENE_DISABLE_DEPTH_OF_FIELD_EVENT:
        {
            sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );

            m_pAnimCamera->SetDepthOfFieldEnabled( false );
        }
        break;
    case CUTSCENE_PLAY_EVENT:
    case CUTSCENE_UPDATE_LOADING_EVENT:
    case CUTSCENE_UPDATE_UNLOADING_EVENT:
    case CUTSCENE_PAUSE_EVENT:
    case CUTSCENE_STOP_EVENT:
    case CUTSCENE_STEP_BACKWARD_EVENT:
    case CUTSCENE_STEP_FORWARD_EVENT:
    case CUTSCENE_REWIND_EVENT:
    case CUTSCENE_FAST_FORWARD_EVENT:
    case CUTSCENE_RESTART_EVENT:
    case CUTSCENE_SET_FACE_ZOOM_DISTANCE_EVENT:
    case CUTSCENE_SET_FACE_ZOOM_NEAR_DRAW_DISTANCE_EVENT:
    case CUTSCENE_SET_FACE_ZOOM_FAR_DRAW_DISTANCE_EVENT:
    case CUTSCENE_SHOW_DEBUG_LINES_EVENT:
    case CUTSCENE_HIDE_DEBUG_LINES_EVENT:
        {
            // handle silently
        }
        break;
    default:
        {
            sampleCutsDisplayf( "%s Event '%s' (id=%d) not supported on entity type '%s'.", 
                pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ), iEventId, pObject->GetTypeName() );
        }
        break;
    }

    cutsAnimatedCameraEntity::DispatchEvent( pManager, pObject, iEventId, pEventArgs );
}

//##############################################################################

SampleCutsceneAnimatedModelEntity::SampleCutsceneAnimatedModelEntity( SimpleWorldDrawableEntity *pSimpleWorldDrawableEntity, 
                                                                     const cutfObject* pObject, crmtMotionTree *pMotionTree, 
                                                                     crmtMotionTreeScheduler *pMotionTreeScheduler )
: cutsMotionTreeEntity( pObject, pMotionTree, pMotionTreeScheduler )
, m_pSimpleWorldDrawableEntity(pSimpleWorldDrawableEntity)
{
#if !__FINAL
    m_iMemoryUsage = m_pSimpleWorldDrawableEntity->GetMemoryUsed();
#endif // !__FINAL
}

SampleCutsceneAnimatedModelEntity::~SampleCutsceneAnimatedModelEntity()
{
    m_pSimpleWorldDrawableEntity = NULL;
}

void SampleCutsceneAnimatedModelEntity::DispatchEvent( cutsManager* pManager, const cutfObject* pObject, 
                                                      s32 iEventId, const cutfEventArgs* pEventArgs )
{
    switch ( iEventId )
    {
    case CUTSCENE_START_OF_SCENE_EVENT:
        {
            Matrix34 m;
            pManager->GetSceneOrientationMatrix( m );

            m_pSimpleWorldDrawableEntity->GetAnimMgr()->SetPlayMode( RvAnimMgr::ANIM_PAUSE );
            m_pSimpleWorldDrawableEntity->SetTransform( m );
        }
        break;
    case CUTSCENE_SCENE_ORIENTATION_CHANGED_EVENT:
        {
            Matrix34 m;
            pManager->GetSceneOrientationMatrix( m );
            m_pSimpleWorldDrawableEntity->SetTransform( m );
        }
        break;
    case CUTSCENE_SET_ANIMATION_EVENT:
        {
            const cutsAnimationEventArgs *pAnimEventArgs = dynamic_cast<const cutsAnimationEventArgs *>( pEventArgs );
            if ( pAnimEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pAnimEventArgs->GetAnimation()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_ANIMATION_EVENT:
        {
            const cutsDualAnimationEventArgs *pDualAnimEventArgs = dynamic_cast<const cutsDualAnimationEventArgs *>( pEventArgs );
            if ( pDualAnimEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s' and '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pDualAnimEventArgs->GetAnimation()->GetName(), pDualAnimEventArgs->GetAnimation2()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_CLIP_EVENT:
        {
            const cutsClipEventArgs *pClipEventArgs = dynamic_cast<const cutsClipEventArgs *>( pEventArgs );
            if ( pClipEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pClipEventArgs->GetClip()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_CLIP_EVENT:
        {
            const cutsDualClipEventArgs *pDualClipEventArgs = dynamic_cast<const cutsDualClipEventArgs *>( pEventArgs );
            if ( pDualClipEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s' and '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pDualClipEventArgs->GetClip()->GetName(),
                    pDualClipEventArgs->GetClip2()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_CLIP_ANIM_EVENT:
        {           
            const cutsDualClipAnimEventArgs *pDualClipAnimEventArgs = dynamic_cast<const cutsDualClipAnimEventArgs *>( pEventArgs );
            if ( pDualClipAnimEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s' and '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pDualClipAnimEventArgs->GetClip()->GetName(),
                    pDualClipAnimEventArgs->GetAnimation()->GetName() );
            }
        }
        break;
    case CUTSCENE_CLEAR_ANIM_EVENT:
        {
            sampleCutsDisplayf( "%s %s '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                pObject->GetDisplayName().c_str() );
        }
        break;
    case CUTSCENE_PLAY_EVENT:
    case CUTSCENE_UPDATE_EVENT:
    case CUTSCENE_UPDATE_LOADING_EVENT:
    case CUTSCENE_UPDATE_UNLOADING_EVENT:
    case CUTSCENE_PAUSE_EVENT:
    case CUTSCENE_STOP_EVENT:
    case CUTSCENE_STEP_BACKWARD_EVENT:
    case CUTSCENE_STEP_FORWARD_EVENT:
    case CUTSCENE_REWIND_EVENT:
    case CUTSCENE_FAST_FORWARD_EVENT:
    case CUTSCENE_RESTART_EVENT:
    case CUTSCENE_SHOW_DEBUG_LINES_EVENT:
    case CUTSCENE_HIDE_DEBUG_LINES_EVENT:
        {
            // handle silently
        }
        break;
    default:
        {
            sampleCutsDisplayf( "%s Event '%s' (id=%d) not supported on entity type '%s'.", 
                pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ), iEventId, pObject->GetTypeName() );
        }
        break;
    }

    cutsMotionTreeEntity::DispatchEvent( pManager, pObject, iEventId, pEventArgs );
}

#if !__FINAL

void SampleCutsceneAnimatedModelEntity::SetDebugDraw( bool bDraw )
{
    cutsMotionTreeEntity::SetDebugDraw( bDraw );

    if ( m_pSimpleWorldDrawableEntity != NULL )
    {
        m_pSimpleWorldDrawableEntity->SetProfileDrawEnabled( bDraw );
    }
}

#endif // !__FINAL

//##############################################################################

SampleCutsceneAnimatedLightEntity::SampleCutsceneAnimatedLightEntity( SampleAnimLight *pAnimLight, const cutfObject* pObject, 
                                                                     crmtMotionTree *pMotionTree,  crmtMotionTreeScheduler *pMotionTreeScheduler )
                                                                     : cutsAnimatedLightEntity( pObject, pMotionTree, pMotionTreeScheduler )
                                                                     , m_pAnimLight(pAnimLight)
{

}

SampleCutsceneAnimatedLightEntity::~SampleCutsceneAnimatedLightEntity()
{
    m_pAnimLight = NULL;
}

void SampleCutsceneAnimatedLightEntity::DispatchEvent( cutsManager* pManager, const cutfObject* pObject, 
                                                      s32 iEventId, const cutfEventArgs* pEventArgs )
{
    switch ( iEventId )
    {
    case CUTSCENE_START_OF_SCENE_EVENT:
        {
            Matrix34 m;
            pManager->GetSceneOrientationMatrix( m );

            m_pAnimLight->GetAnimMgr()->SetPlayMode( RvAnimMgr::ANIM_PAUSE );
            m_pAnimLight->SetIsActive( true );
        }
        break;
    case CUTSCENE_END_OF_SCENE_EVENT:
        {
            m_pAnimLight->SetIsActive( false );
        }
        break;
    case CUTSCENE_SCENE_ORIENTATION_CHANGED_EVENT:
        {
            Matrix34 m;
            pManager->GetSceneOrientationMatrix( m );
            m_pAnimLight->GetTransform().Set( m );
        }
        break;
    case CUTSCENE_SET_ANIMATION_EVENT:
        {
            const cutsAnimationEventArgs *pAnimEventArgs = dynamic_cast<const cutsAnimationEventArgs *>( pEventArgs );
            if ( pAnimEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pAnimEventArgs->GetAnimation()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_ANIMATION_EVENT:
        {
            const cutsDualAnimationEventArgs *pDualAnimEventArgs = dynamic_cast<const cutsDualAnimationEventArgs *>( pEventArgs );
            if ( pDualAnimEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s' and '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pDualAnimEventArgs->GetAnimation()->GetName(), pDualAnimEventArgs->GetAnimation2()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_CLIP_EVENT:
        {
            const cutsClipEventArgs *pClipEventArgs = dynamic_cast<const cutsClipEventArgs *>( pEventArgs );
            if ( pClipEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pClipEventArgs->GetClip()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_CLIP_EVENT:
        {
            const cutsDualClipEventArgs *pDualClipEventArgs = dynamic_cast<const cutsDualClipEventArgs *>( pEventArgs );
            if ( pDualClipEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s' and '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pDualClipEventArgs->GetClip()->GetName(),
                    pDualClipEventArgs->GetClip2()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_CLIP_ANIM_EVENT:
        {           
            const cutsDualClipAnimEventArgs *pDualClipAnimEventArgs = dynamic_cast<const cutsDualClipAnimEventArgs *>( pEventArgs );
            if ( pDualClipAnimEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s' and '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pDualClipAnimEventArgs->GetClip()->GetName(),
                    pDualClipAnimEventArgs->GetAnimation()->GetName() );
            }
        }
        break;
    case CUTSCENE_CLEAR_ANIM_EVENT:
        {
            sampleCutsDisplayf( "%s %s '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                pObject->GetDisplayName().c_str() );
        }
        break;
    case CUTSCENE_PLAY_EVENT:
    case CUTSCENE_UPDATE_EVENT:
    case CUTSCENE_UPDATE_LOADING_EVENT:
    case CUTSCENE_UPDATE_UNLOADING_EVENT:
    case CUTSCENE_PAUSE_EVENT:
    case CUTSCENE_STOP_EVENT:
    case CUTSCENE_STEP_BACKWARD_EVENT:
    case CUTSCENE_STEP_FORWARD_EVENT:
    case CUTSCENE_REWIND_EVENT:
    case CUTSCENE_FAST_FORWARD_EVENT:
    case CUTSCENE_RESTART_EVENT:
    case CUTSCENE_SHOW_DEBUG_LINES_EVENT:
    case CUTSCENE_HIDE_DEBUG_LINES_EVENT:
        {
            // handle silently
        }
        break;
    default:
        {
            sampleCutsDisplayf( "%s Event '%s' (id=%d) not supported on entity type '%s'.", pManager->GetDisplayTime(), 
                cutfEvent::GetDisplayName( iEventId ), iEventId, pObject->GetTypeName() );
        }
        break;
    }

    cutsAnimatedLightEntity::DispatchEvent( pManager, pObject, iEventId, pEventArgs );

#if __BANK
    m_pAnimLight->SetEditLightInfo( m_pEditLightInfo );
#endif // __BANK
}

//##############################################################################

SampleCutsceneAnimatedParticleEffectEntity::SampleCutsceneAnimatedParticleEffectEntity( SimpleWorldRmptfxEmitter *pSimpleWorldRmptfxEmitter, 
    ptxEffectInst *pEventInst, const cutfObject* pObject, crmtMotionTree *pMotionTree, crmtMotionTreeScheduler *pMotionTreeScheduler )
: cutsAnimatedParticleEffectEntity( pEventInst, pObject, pMotionTree, pMotionTreeScheduler )
, m_pSimpleWorldRmptfxEmitter(pSimpleWorldRmptfxEmitter)
{
#if !__FINAL
    m_iMemoryUsage = m_pSimpleWorldRmptfxEmitter->GetMemoryUsed();
#endif // !__FINAL
}

SampleCutsceneAnimatedParticleEffectEntity::~SampleCutsceneAnimatedParticleEffectEntity()
{
    m_pSimpleWorldRmptfxEmitter = NULL;
}

void SampleCutsceneAnimatedParticleEffectEntity::DispatchEvent( cutsManager* pManager, const cutfObject* pObject, 
                                                      s32 iEventId, const cutfEventArgs* pEventArgs )
{
    switch ( iEventId )
    {
    case CUTSCENE_START_OF_SCENE_EVENT:
        {
            Matrix34 m;
            pManager->GetSceneOrientationMatrix( m );

            m_pSimpleWorldRmptfxEmitter->GetAnimMgr()->SetPlayMode( RvAnimMgr::ANIM_PAUSE );
            m_pSimpleWorldRmptfxEmitter->SetTransform( m );
        }
        break;
    case CUTSCENE_SCENE_ORIENTATION_CHANGED_EVENT:
        {
            Matrix34 m;
            pManager->GetSceneOrientationMatrix( m );
            m_pSimpleWorldRmptfxEmitter->SetTransform( m );
        }
        break;
    case CUTSCENE_SET_ANIMATION_EVENT:
        {
            const cutsAnimationEventArgs *pAnimEventArgs = dynamic_cast<const cutsAnimationEventArgs *>( pEventArgs );
            if ( pAnimEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pAnimEventArgs->GetAnimation()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_ANIMATION_EVENT:
        {
            const cutsDualAnimationEventArgs *pDualAnimEventArgs = dynamic_cast<const cutsDualAnimationEventArgs *>( pEventArgs );
            if ( pDualAnimEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s' and '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pDualAnimEventArgs->GetAnimation()->GetName(), pDualAnimEventArgs->GetAnimation2()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_CLIP_EVENT:
        {
            const cutsClipEventArgs *pClipEventArgs = dynamic_cast<const cutsClipEventArgs *>( pEventArgs );
            if ( pClipEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pClipEventArgs->GetClip()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_CLIP_EVENT:
        {
            const cutsDualClipEventArgs *pDualClipEventArgs = dynamic_cast<const cutsDualClipEventArgs *>( pEventArgs );
            if ( pDualClipEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s' and '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pDualClipEventArgs->GetClip()->GetName(),
                    pDualClipEventArgs->GetClip2()->GetName() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_CLIP_ANIM_EVENT:
        {           
            const cutsDualClipAnimEventArgs *pDualClipAnimEventArgs = dynamic_cast<const cutsDualClipAnimEventArgs *>( pEventArgs );
            if ( pDualClipAnimEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s '%s' to '%s' and '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                    pObject->GetDisplayName().c_str(), pDualClipAnimEventArgs->GetClip()->GetName(),
                    pDualClipAnimEventArgs->GetAnimation()->GetName() );
            }
        }
        break;
    case CUTSCENE_CLEAR_ANIM_EVENT:
        {
            sampleCutsDisplayf( "%s %s '%s'.", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                pObject->GetDisplayName().c_str() );
        }
        break;
    case CUTSCENE_PLAY_EVENT:
    case CUTSCENE_UPDATE_EVENT:
    case CUTSCENE_UPDATE_LOADING_EVENT:
    case CUTSCENE_UPDATE_UNLOADING_EVENT:
    case CUTSCENE_PAUSE_EVENT:
    case CUTSCENE_STOP_EVENT:
    case CUTSCENE_STEP_BACKWARD_EVENT:
    case CUTSCENE_STEP_FORWARD_EVENT:
    case CUTSCENE_REWIND_EVENT:
    case CUTSCENE_FAST_FORWARD_EVENT:
    case CUTSCENE_RESTART_EVENT:
        {
            // handle silently
        }
        break;
    default:
        {
            sampleCutsDisplayf( "%s Event '%s' (id=%d) not supported on entity type '%s'.", pManager->GetDisplayTime(), 
                cutfEvent::GetDisplayName( iEventId ), iEventId, pObject->GetTypeName() );
        }
        break;
    }

    cutsAnimatedParticleEffectEntity::DispatchEvent( pManager, pObject, iEventId, pEventArgs );
}

//##############################################################################

} // namespace ragesamples
