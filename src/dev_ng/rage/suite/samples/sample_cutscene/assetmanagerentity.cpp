// 
// sample_cutscene/assetmanagerentity.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "assetmanagerentity.h"

#include "cutscenemanager.h"
#include "sample_cutscene.h"
#include "samplecutscenechannel.h"
#include "atl/string.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfobject.h"
#include "cutscene/cutsevent.h"
#include "file/asset.h"
#include "sample_simpleworld/animmgr.h"
#include "string/string.h"

namespace ragesamples
{

//##############################################################################

SampleCutsceneAssetManagerEntity::SampleCutsceneAssetManagerEntity()
: m_pManager(NULL)
{

}

SampleCutsceneAssetManagerEntity::SampleCutsceneAssetManagerEntity( const cutfObject* pObject )
: cutsUniqueEntity( pObject )
, m_pManager(NULL)
{

}

void SampleCutsceneAssetManagerEntity::DispatchEvent( cutsManager* pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs )
{
    SampleCutsceneManager *pCutsceneMgr = dynamic_cast<SampleCutsceneManager *>( pManager );

    switch ( iEventId )
    {
    case CUTSCENE_LOAD_SCENE_EVENT:
        {
            sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );
        }
        break;
    case CUTSCENE_UNLOAD_SCENE_EVENT:
        {
            sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );
        }
        break;
    case CUTSCENE_LOAD_MODELS_EVENT:
        {
            const cutfObjectIdListEventArgs *pObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEventArgs );
            if ( pObjectIdListEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );

                pCutsceneMgr->GetSampleCutscene()->StartEntityBatchLoad( 0.25f );

                const atArray<s32> &objectIdList = pObjectIdListEventArgs->GetObjectIdList();
                for ( int i = 0; i < objectIdList.GetCount(); ++i )
                {
                    const cutfObject *pLoadObject = pManager->GetObjectById( objectIdList[i] );
                    if ( pLoadObject != NULL )
                    {
                        char cFilename[RAGE_MAX_PATH];

                        // see if we specified an alternate model
                        const parAttribute *pAtt = pLoadObject->GetAttributeList().FindAttribute( "AltModel" );
                        if ( pAtt != NULL )
                        {
                            sprintf( cFilename, "%s/%s/entity.type", pCutsceneMgr->GetAssetDir(), pAtt->GetStringValue() );
                        }
                        else
                        {
                            sprintf( cFilename, "%s/%s/entity.type", pCutsceneMgr->GetAssetDir(), pLoadObject->GetDisplayName().c_str() );
                        }
                           
                        pCutsceneMgr->GetSampleCutscene()->AddEntityFilenameToBatch( cFilename );
                    }
                }

                m_pManager = pCutsceneMgr;
                pCutsceneMgr->GetSampleCutscene()->EndEntityBatchLoad( 
                    MakeFunctor( *this, &SampleCutsceneAssetManagerEntity::EntityBatchComplete ) );
                pCutsceneMgr->SetIsLoading( pObject->GetObjectId(), true );
            }
        }
        break;
    case CUTSCENE_UNLOAD_MODELS_EVENT:
        {
            const cutfObjectIdListEventArgs *pObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEventArgs );
            if ( pObjectIdListEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );

                const atArray<s32> &objectIdList = pObjectIdListEventArgs->GetObjectIdList();
                for ( int i = 0; i < objectIdList.GetCount(); ++i )
                {
                    const cutfObject *pUnloadObject = pManager->GetObjectById( objectIdList[i] );
                    if ( pUnloadObject != NULL )
                    {
                        char cFilename[RAGE_MAX_PATH];

                        // see if we specified an alternate model
                        const parAttribute *pAtt = pUnloadObject->GetAttributeList().FindAttribute( "AltModel" );
                        if ( pAtt != NULL )
                        {
                            sprintf( cFilename, "%s/%s/entity.type", pCutsceneMgr->GetAssetDir(), pAtt->GetStringValue() );
                        }
                        else
                        {
                            sprintf( cFilename, "%s/%s/entity.type", pCutsceneMgr->GetAssetDir(), pUnloadObject->GetDisplayName().c_str() );
                        }

                        pCutsceneMgr->GetSampleCutscene()->UnloadEntity( cFilename );
                    }
                }
            }
        }
        break;
    case CUTSCENE_LOAD_PARTICLE_EFFECTS_EVENT:
        {
            const cutfObjectIdListEventArgs *pObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEventArgs );
            if ( pObjectIdListEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );

                const atArray<s32> &objectIdList = pObjectIdListEventArgs->GetObjectIdList();
                for ( int i = 0; i < objectIdList.GetCount(); ++i )
                {
                    const cutfObject *pLoadObject = pManager->GetObjectById( objectIdList[i] );
                    if ( pLoadObject != NULL )
                    {
                        char filename[RAGE_MAX_PATH];                       
                        sprintf( filename, "$/tune/rmptfx/effectrules/%s", pLoadObject->GetDisplayName().c_str() );

                        char fullFilename[RAGE_MAX_PATH];
                        ASSET.FullPath( fullFilename, sizeof(fullFilename), filename, NULL );

                        pCutsceneMgr->GetSampleCutscene()->LoadParticleEffect( fullFilename );
                    }
                }                
            }
        }
        break;
    case CUTSCENE_UNLOAD_PARTICLE_EFFECTS_EVENT:
        {
            const cutfObjectIdListEventArgs *pObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEventArgs );
            if ( pObjectIdListEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );

                const atArray<s32> &objectIdList = pObjectIdListEventArgs->GetObjectIdList();
                for ( int i = 0; i < objectIdList.GetCount(); ++i )
                {
                    const cutfObject *pUnloadObject = pManager->GetObjectById( objectIdList[i] );
                    if ( pUnloadObject != NULL )
                    {
                        char filename[RAGE_MAX_PATH];                       
                        sprintf( filename, "$/tune/rmptfx/effectrules/%s", pUnloadObject->GetDisplayName().c_str() );

                        char fullFilename[RAGE_MAX_PATH];
                        ASSET.FullPath( fullFilename, sizeof(fullFilename), filename, NULL );

                        pCutsceneMgr->GetSampleCutscene()->UnloadEntity( fullFilename );
                    }
                }
            }
        }
        break;
    case CUTSCENE_LOAD_SUBTITLES_EVENT:
        {
            const cutfNameEventArgs *pNameEventArgs = dynamic_cast<const cutfNameEventArgs *>( pEventArgs );
            if ( pNameEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );

                char cFilename[RAGE_MAX_PATH];                       
                sprintf( cFilename, "%s/subtitles/%s.strtbl", pCutsceneMgr->GetAssetDir(), pNameEventArgs->GetName() );

                pCutsceneMgr->GetSampleCutscene()->LoadSubtitles( cFilename );
            }
        }
        break;
    case CUTSCENE_UNLOAD_SUBTITLES_EVENT:
        {
            const cutfNameEventArgs *pNameEventArgs = dynamic_cast<const cutfNameEventArgs *>( pEventArgs );
            if ( pNameEventArgs != NULL )
            {
                sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );

                char cFilename[RAGE_MAX_PATH];                       
                sprintf( cFilename, "%s/subtitles/%s.strtbl", pCutsceneMgr->GetAssetDir(), pNameEventArgs->GetName() );

                pCutsceneMgr->GetSampleCutscene()->UnloadSubtitles( cFilename );
            }
        }
        break;
    case CUTSCENE_LOAD_OVERLAYS_EVENT:
    case CUTSCENE_UNLOAD_OVERLAYS_EVENT:
        {
            sampleCutsDisplayf( "%s Event '%s' (id=%d) not yet supported.", pManager->GetDisplayTime(), 
                cutfEvent::GetDisplayName( iEventId ), iEventId );
        }
        break;

    case CUTSCENE_HIDE_OBJECTS_EVENT:
        {
            const cutfObjectIdListEventArgs *pObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEventArgs );
            if ( pObjectIdListEventArgs != NULL )
            {
                const atArray<s32> &objectIdList = pObjectIdListEventArgs->GetObjectIdList();
                for ( int i = 0; i < objectIdList.GetCount(); ++i )
                {
                    const cutfObject *pObj = pCutsceneMgr->GetObjectById( objectIdList[i] );
                    if ( pObj != NULL )
                    {
                        NOTFINAL_ONLY( const cutfHiddenModelObject *pHiddenModelObject = dynamic_cast<const cutfHiddenModelObject *>( pObj ); )
                        sampleCutsDisplayf( "%s %s '%s' within %f units of (%f,%f,%f).", 
                            pCutsceneMgr->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                            pHiddenModelObject->GetName(), pHiddenModelObject->GetRadius(),
                            pHiddenModelObject->GetPosition().GetX(), pHiddenModelObject->GetPosition().GetY(), 
                            pHiddenModelObject->GetPosition().GetZ() );
                    }
                }
            }
        }
        break;
    case CUTSCENE_SHOW_OBJECTS_EVENT:
        {
            const cutfObjectIdListEventArgs *pObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEventArgs );
            if ( pObjectIdListEventArgs != NULL )
            {
                const atArray<s32> &objectIdList = pObjectIdListEventArgs->GetObjectIdList();
                for ( int i = 0; i < objectIdList.GetCount(); ++i )
                {
                    const cutfObject *pObj = pCutsceneMgr->GetObjectById( objectIdList[i] );
                    if ( pObj != NULL )
                    {
                        NOTFINAL_ONLY( const cutfHiddenModelObject *pHiddenModelObject = dynamic_cast<const cutfHiddenModelObject *>( pObj ); )
                        sampleCutsDisplayf( "%s %s '%s' within %f units of (%f,%f,%f).", 
                            pCutsceneMgr->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                            pHiddenModelObject->GetName(), pHiddenModelObject->GetRadius(),
                            pHiddenModelObject->GetPosition().GetX(), pHiddenModelObject->GetPosition().GetY(), 
                            pHiddenModelObject->GetPosition().GetZ() );
                    }
                }
            }
        }
        break;
    case CUTSCENE_FIXUP_OBJECTS_EVENT:
        {
            const cutfObjectIdListEventArgs *pObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEventArgs );
            if ( pObjectIdListEventArgs != NULL )
            {
                const atArray<s32> &objectIdList = pObjectIdListEventArgs->GetObjectIdList();
                for ( int i = 0; i < objectIdList.GetCount(); ++i )
                {
                    const cutfObject *pObj = pCutsceneMgr->GetObjectById( objectIdList[i] );
                    if ( pObj != NULL )
                    {
                        NOTFINAL_ONLY( const cutfFixupModelObject *pFixupModelObject = dynamic_cast<const cutfFixupModelObject *>( pObj ); )
                        sampleCutsDisplayf( "%s %s '%s' within %f units of (%f,%f,%f).", 
                            pCutsceneMgr->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                            pFixupModelObject->GetName(), pFixupModelObject->GetRadius(),
                            pFixupModelObject->GetPosition().GetX(), pFixupModelObject->GetPosition().GetY(), 
                            pFixupModelObject->GetPosition().GetZ() );
                    }
                }
            }
        }
        break;
    case CUTSCENE_REVERT_FIXUP_OBJECTS_EVENT:
        {
            const cutfObjectIdListEventArgs *pObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEventArgs );
            if ( pObjectIdListEventArgs != NULL )
            {
                const atArray<s32> &objectIdList = pObjectIdListEventArgs->GetObjectIdList();
                for ( int i = 0; i < objectIdList.GetCount(); ++i )
                {
                    const cutfObject *pObj = pCutsceneMgr->GetObjectById( objectIdList[i] );
                    if ( pObj != NULL )
                    {
                        NOTFINAL_ONLY( const cutfFixupModelObject *pFixupModelObject = dynamic_cast<const cutfFixupModelObject *>( pObj ); )
                        sampleCutsDisplayf( "%s %s '%s' within %f units of (%f,%f,%f).", 
                            pCutsceneMgr->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ),
                            pFixupModelObject->GetName(), pFixupModelObject->GetRadius(),
                            pFixupModelObject->GetPosition().GetX(), pFixupModelObject->GetPosition().GetY(), 
                            pFixupModelObject->GetPosition().GetZ() );
                    }
                }
            }
        }
        break;
    case CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT:
        {
            const cutfObjectIdListEventArgs *pObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEventArgs );
            if ( pObjectIdListEventArgs != NULL )
            {
                const atArray<s32> &objectIdList = pObjectIdListEventArgs->GetObjectIdList();
                for ( int i = 0; i < objectIdList.GetCount(); ++i )
                {
                    const cutfObject *pObj = pCutsceneMgr->GetObjectById( objectIdList[i] );
                    if ( pObj != NULL )
                    {
                        const cutfBlockingBoundsObject *pBlockingBoundsObject = dynamic_cast<const cutfBlockingBoundsObject *>( pObj );

                        pCutsceneMgr->GetSampleCutscene()->CreateBlockingBounds( pBlockingBoundsObject->GetDisplayName().c_str() );
                    }
                }
            }
        }
        break;
    case CUTSCENE_REMOVE_BLOCKING_BOUNDS_EVENT:
        {
            const cutfObjectIdListEventArgs *pObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEventArgs );
            if ( pObjectIdListEventArgs != NULL )
            {
                const atArray<s32> &objectIdList = pObjectIdListEventArgs->GetObjectIdList();
                for ( int i = 0; i < objectIdList.GetCount(); ++i )
                {
                    const cutfObject *pObj = pCutsceneMgr->GetObjectById( objectIdList[i] );
                    if ( pObj != NULL )
                    {
                        pCutsceneMgr->GetSampleCutscene()->DeleteBlockingBounds( pObj->GetDisplayName().c_str() );
                    }
                }
            }
        }
        break;
    case CUTSCENE_ADD_REMOVAL_BOUNDS_EVENT:
        {
            const cutfObjectIdListEventArgs *pObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEventArgs );
            if ( pObjectIdListEventArgs != NULL )
            {
                const atArray<s32> &objectIdList = pObjectIdListEventArgs->GetObjectIdList();
                for ( int i = 0; i < objectIdList.GetCount(); ++i )
                {
                    const cutfObject *pObj = pCutsceneMgr->GetObjectById( objectIdList[i] );
                    if ( pObj != NULL )
                    {
                        const cutfRemovalBoundsObject *pRemovalBoundsObject = dynamic_cast<const cutfRemovalBoundsObject *>( pObj );

                        pCutsceneMgr->GetSampleCutscene()->CreateRemovalBounds( pRemovalBoundsObject->GetDisplayName().c_str() );
                    }
                }
            }
        }
        break;
    case CUTSCENE_REMOVE_REMOVAL_BOUNDS_EVENT:
        {
            const cutfObjectIdListEventArgs *pObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEventArgs );
            if ( pObjectIdListEventArgs != NULL )
            {
                const atArray<s32> &objectIdList = pObjectIdListEventArgs->GetObjectIdList();
                for ( int i = 0; i < objectIdList.GetCount(); ++i )
                {
                    const cutfObject *pObj = pCutsceneMgr->GetObjectById( objectIdList[i] );
                    if ( pObj != NULL )
                    {
                        pCutsceneMgr->GetSampleCutscene()->DeleteRemovalBounds( pObj->GetDisplayName().c_str() );
                    }
                }
            }
        }
        break;
    case CUTSCENE_START_OF_SCENE_EVENT:
    case CUTSCENE_END_OF_SCENE_EVENT:
    case CUTSCENE_UNLOADED_EVENT:
    case CUTSCENE_PLAY_EVENT:
    case CUTSCENE_PAUSE_EVENT:
    case CUTSCENE_STOP_EVENT:
    case CUTSCENE_STEP_BACKWARD_EVENT:
    case CUTSCENE_STEP_FORWARD_EVENT:
        {
            sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );
        }
        break;
    case CUTSCENE_REWIND_EVENT:
    case CUTSCENE_FAST_FORWARD_EVENT:
        {
            const cutfFloatValueEventArgs *pFloatEventArgs = dynamic_cast<const cutfFloatValueEventArgs *>( pEventArgs );
            if ( pFloatEventArgs != NULL )
            {
                char playbackRateText[32];

                if ( pFloatEventArgs->GetFloat1() > 1.0f )
                {
                    sprintf( playbackRateText, "%dx", (int)pFloatEventArgs->GetFloat1() );
                }
                else if ( pFloatEventArgs->GetFloat1() < -1.0f )
                {
                    sprintf( playbackRateText, "%dx", (int)fabsf(pFloatEventArgs->GetFloat1()) );
                }
                else if ( (pFloatEventArgs->GetFloat1() > 0.0f) && (pFloatEventArgs->GetFloat1() < 1.0f) )
                {
                    sprintf( playbackRateText, "%d fps", (int)(pFloatEventArgs->GetFloat1() * 30.0f) );
                }
                else if ( (pFloatEventArgs->GetFloat1() > -1.0f) && (pFloatEventArgs->GetFloat1() < 0.0f) )
                {
                    sprintf( playbackRateText, "%d fps", (int)fabsf(pFloatEventArgs->GetFloat1() * 30.0f) );
                }
                else
                {
                    playbackRateText[0] = 0;
                }

                sampleCutsDisplayf( "%s %s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ), playbackRateText );
            }
        }
        break;
    case CUTSCENE_CANCEL_LOAD_EVENT:
        {            
            sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );

            pCutsceneMgr->GetSampleCutscene()->CancelEntityBatchLoad();
            pManager->SetIsLoading( pObject->GetObjectId(), false );
        }
        break;
    case CUTSCENE_RESTART_EVENT:
        {
            // FIXME: may need to reload some things

            sampleCutsDisplayf( "%s %s", pManager->GetDisplayTime(), cutfEvent::GetDisplayName( iEventId ) );
        }
        break;
    case CUTSCENE_UPDATE_EVENT:
    case CUTSCENE_UPDATE_LOADING_EVENT:
    case CUTSCENE_UPDATE_UNLOADING_EVENT:
    case CUTSCENE_SCENE_ORIENTATION_CHANGED_EVENT:
        {
            // handle silently
        }
        break;
    default:
        {
            sampleCutsDisplayf( "%s Event '%s' (id=%d) not supported on entity type '%s'.", pManager->GetDisplayTime(), 
                cutfEvent::GetDisplayName( iEventId ), iEventId, pObject->GetTypeName() );
        }
        break;
    }
}

void SampleCutsceneAssetManagerEntity::EntityBatchComplete()
{
    if ( m_pManager != NULL )
    {
        m_pManager->SetIsLoaded( m_pObject->GetObjectId(), true );
    }
}

//##############################################################################

} // namespace ragesamples
