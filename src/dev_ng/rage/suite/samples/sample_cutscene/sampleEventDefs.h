//
// sample_cutscene/sampleEventDefs.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef SAMPLE_CUTSCENE_SAMPLEEVENTDEFS_H
#define SAMPLE_CUTSCENE_SAMPLEEVENTDEFS_H

#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"

using namespace rage;

namespace ragesamples {

//##############################################################################

enum ECutsceneCustomEvent {
	CUTSCENE_WAIT_FOR_USER_INPUT_EVENT = CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE,
	CUTSCENE_DISPLAY_USER_INPUT_MESSAGE_EVENT,
	CUTSCENE_START_SCRIPT_EVENT,
	CUTSCENE_LAST_CUSTOM_EVENT,
	CUTSCENE_STOP_SCRIPT_EVENT = CUTSCENE_LAST_CUSTOM_EVENT
};

//##############################################################################

static const char* s_cutsceneEventIdDisplayNames[] = {
	"Wait For User Input",
	"Display User Input Message",
	"Start Script",
	"Stop Script"
};

// PURPOSE: Pass this function in to cutfEvent::AddEventIdDisplayNameFunc(...).
static inline const char* GetCutsceneCustomEventIdDisplayName( s32 iEventId )
{
	if ( (iEventId >= CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE) && (iEventId <= CUTSCENE_LAST_CUSTOM_EVENT) )
	{
		int iEventIdOffset = iEventId - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;
		return s_cutsceneEventIdDisplayNames[iEventIdOffset];
	}

	return "";
}

//##############################################################################

static const s32 c_cutsceneCustomEventSortingRanks[] = {
	CUTSCENE_LAST_EVENT_RANK + 1, // CUTSCENE_WAIT_FOR_USER_INPUT_EVENT
	CUTSCENE_LAST_EVENT_RANK, // CUTSCENE_DISPLAY_USER_INPUT_MESSAGE_EVENT
	CUTSCENE_LAST_EVENT_RANK + 1, // CUTSCENE_START_SCRIPT_EVENT
	CUTSCENE_LAST_EVENT_RANK, // CUTSCENE_STOP_SCRIPT_EVENT
};

// PURPOSE: Pass this function in to cutfEvent::AddEventIdRankFunc(...).
static inline s32 GetCutsceneCustomEventIdRank( s32 iEventId )
{
	if ( (iEventId >= CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE) && (iEventId <= CUTSCENE_LAST_CUSTOM_EVENT) )
	{
		int iEventIdOffset = iEventId - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;
		return c_cutsceneCustomEventSortingRanks[iEventIdOffset];
	}

	return CUTSCENE_LAST_UNLOAD_EVENT_RANK;
}

//##############################################################################

static const s32 c_cutsceneCustomOppositeEventIds[] = {
	CUTSCENE_NO_OPPOSITE_EVENT, // CUTSCENE_WAIT_FOR_USER_INPUT_EVENT
	CUTSCENE_NO_OPPOSITE_EVENT, // CUTSCENE_DISPLAY_USER_INPUT_MESSAGE_EVENT
	CUTSCENE_STOP_SCRIPT_EVENT,
	CUTSCENE_START_SCRIPT_EVENT,
};

// PURPOSE: Pass this function in to cutfEvent::AddOppositeEventIdFunc(...).
static inline s32 GetCutsceneCustomOppositeEventId( s32 iEventId )
{
	if ( (iEventId >= CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE) && (iEventId <= CUTSCENE_LAST_CUSTOM_EVENT) )
	{
		int iEventIdOffset = iEventId - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;
		return c_cutsceneCustomOppositeEventIds[iEventIdOffset];
	}

	return CUTSCENE_NO_OPPOSITE_EVENT;
}

//##############################################################################

enum ECutsceneCustomEventArgsType {
	CUTSCENE_GENERIC_EVENT_ARGS_TYPE = CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE,
	CUTSCENE_USERINPUTMESSAGE_EVENT_ARGS_TYPE,
	CUTSCENE_LAST_CUSTOM_EVENT_ARGS_TYPE,
	CUTSCENE_SCRIPT_EVENT_ARGS_TYPE = CUTSCENE_LAST_CUSTOM_EVENT_ARGS_TYPE
};

//##############################################################################

static const char* s_cutsceneEventArgsTypeNames[] = {
	"Generic",
	"UserInputMessage",
	"Script",
};

// PURPOSE: Pass this function in to cutfEventArgs::AddEventArgsTypeNameFunc(...).
static inline const char* GetCutsceneCustomEventArgsTypeName( s32 iEventArgsType )
{
	if ( (iEventArgsType >= CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE) && (iEventArgsType <= CUTSCENE_LAST_CUSTOM_EVENT_ARGS_TYPE) )
	{
		int iEventArgsTypeOffset = iEventArgsType - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE;
		return s_cutsceneEventArgsTypeNames[iEventArgsTypeOffset];
	}

	return "";
}

//##############################################################################

class ICutsceneGenericCustomEventArgs : public cutfEventArgs {
public:
	ICutsceneGenericCustomEventArgs() { }
	~ICutsceneGenericCustomEventArgs() { }
};

//##############################################################################

class ICutsceneUserInputMessageCustomEventArgs : public cutfEventArgs {
public:
	ICutsceneUserInputMessageCustomEventArgs() { }
	~ICutsceneUserInputMessageCustomEventArgs() { }

	const char* GetText() const
	{
		static char cBuffer[256];
		return m_attributeList.FindAttributeStringValue( "Text", "(null)", cBuffer, sizeof(cBuffer) );
	}
};

//##############################################################################

class ICutsceneScriptCustomEventArgs : public cutfEventArgs {
public:
	ICutsceneScriptCustomEventArgs() { }
	~ICutsceneScriptCustomEventArgs() { }

	const char* GetName() const
	{
		static char cBuffer[256];
		return m_attributeList.FindAttributeStringValue( "Name", "(null)", cBuffer, sizeof(cBuffer) );
	}
};

//##############################################################################

} // namespace ragesamples

//##############################################################################

#endif // SAMPLE_CUTSCENE_SAMPLEEVENTDEFS_H
