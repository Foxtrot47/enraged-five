// 
// sample_cutscene/cutscenemanager.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "cutscenemanager.h"

#include "animentity.h"
#include "assetmanagerentity.h"
#include "miscentity.h"
#include "sample_cutscene.h"
#include "samplecutscenechannel.h"
#include "cutfile/cutfobject.h"
#include "parsercore/attribute.h"
#include "sample_simpleworld/entitymgr.h"
#include "system/param.h"

RAGE_DEFINE_CHANNEL( SampleCuts );

namespace rage
{
    XPARAM( noaudio );
}

namespace ragesamples
{

SampleCutsceneManager::SampleCutsceneManager()
: m_pSampleCutscene(NULL)
, m_pSubtitleEntity(NULL)
{
    m_cAssetDir[0] = 0;
}

SampleCutsceneManager::~SampleCutsceneManager()
{

}

cutsEntity* SampleCutsceneManager::ReserveEntity( const cutfObject* pObject )
{
    cutsEntity *pEntity = NULL;

    switch ( pObject->GetType() )
    {
    case CUTSCENE_ASSET_MANAGER_OBJECT_TYPE:
        {
            pEntity = rage_new SampleCutsceneAssetManagerEntity( pObject );
        }
        break;
    case CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE:
        {
            pEntity = rage_new SampleCutsceneAnimationManagerEntity( pObject );
        }
        break;
    case CUTSCENE_MODEL_OBJECT_TYPE:
        {
            const cutfModelObject *pModelObject = dynamic_cast<const cutfModelObject *>( pObject );
            if ( pModelObject != NULL )
            {
                SimpleWorldEntity *pSimpleWorldEntity = NULL;

                // see if we specified an alternate model
                const parAttribute *pAtt = pModelObject->GetAttributeList().FindAttribute( "AltModel" );
                if ( pAtt != NULL )
                {
                    pSimpleWorldEntity = SMPWORLDEMGR.GetEntity( pAtt->GetStringValue() );
                }
                else
                {
                    pSimpleWorldEntity = SMPWORLDEMGR.GetEntity( pModelObject->GetDisplayName().c_str() );
                }

                if ( pSimpleWorldEntity != NULL  )
                {
                    SimpleWorldDrawableEntity *pSimpleWorldDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity *>( pSimpleWorldEntity );
                    if ( pSimpleWorldDrawableEntity != NULL )
                    {
                        switch ( pModelObject->GetModelType() )
                        {
                        case CUTSCENE_PED_MODEL_TYPE:
                        case CUTSCENE_VEHICLE_MODEL_TYPE:
                        case CUTSCENE_PROP_MODEL_TYPE:
                            {
                                // Create the animated model entity.  Do NOT pass in the scheduler.  We'll let the entity's
                                // update function take care of that.
                                pEntity = rage_new SampleCutsceneAnimatedModelEntity( pSimpleWorldDrawableEntity,
                                    pObject, pSimpleWorldDrawableEntity->GetAnimMgr()->GetMotionTree() );
                            }
                            break;
                        default:
                            {
                                sampleCutsDisplayf( "Model objects of type '%s' are not yet supported.", pObject->GetTypeName() );
                            }
                            break;
                        }
                    }
                    else
                    {
                        sampleCutsWarningf( "The entity we found for '%s' is not a SimpleWorldDrawableEntity.", 
                            pModelObject->GetDisplayName().c_str() );
                    }
                }
                else
                {
                    sampleCutsWarningf( "Could not locate entity '%s'", pModelObject->GetDisplayName().c_str() );
                }
            }
        }
        break;
    case CUTSCENE_CAMERA_OBJECT_TYPE:
        {            
            // Create the animated camera entity.  Do NOT pass in the scheduler.  We'll let the camera's
            // RvAnimMgr function take care of that.
            SampleAnimCamera *pAnimCamera = m_pSampleCutscene->CreateAnimCamera( pObject->GetDisplayName().c_str() );
            if ( pAnimCamera != NULL )
            {
                pEntity = rage_new SampleCutsceneAnimatedCameraEntity( pAnimCamera, pObject, pAnimCamera->GetAnimMgr()->GetMotionTree() );
            }
        }
        break;
    case CUTSCENE_AUDIO_OBJECT_TYPE:
        {
            if ( !PARAM_noaudio.Get())
            {
                pEntity = rage_new SampleCutsceneAudioEntity( pObject );
            }
            else
            {
                sampleCutsDisplayf( "-noaudio used.  Skip creating SampleCutsceneAudioEntity." );
                return NULL;
            }
        }
        break;        
    case CUTSCENE_LIGHT_OBJECT_TYPE:
        {
            // Create an animated light entity.  Do NOT pass in the scheduler.  We'll let the camera's
            // RvAnimMgr function take care of that.
            SampleAnimLight *pAnimLight = m_pSampleCutscene->CreateAnimLight( pObject->GetDisplayName().c_str() );
            if ( pAnimLight != NULL )
            {
                pEntity = rage_new SampleCutsceneAnimatedLightEntity( pAnimLight, pObject, pAnimLight->GetAnimMgr()->GetMotionTree() );
            }
            else
            {
                sampleCutsWarningf( "Unable to create more light entities.  Max is %d.", grcLightGroup::MAX_LIGHTS );
                return NULL;
            }
        }
        break;
    case CUTSCENE_ANIMATED_PARTICLE_EFFECT_OBJECT_TYPE:
        {
            SimpleWorldEntity *pSimpleWorldEntity = SMPWORLDEMGR.GetEntity( pObject->GetDisplayName().c_str() );
            if ( pSimpleWorldEntity != NULL  )
            {
                SimpleWorldRmptfxEmitter *pSimpleWorldRmptfxEmitter = dynamic_cast<SimpleWorldRmptfxEmitter *>( pSimpleWorldEntity );
                if ( pSimpleWorldRmptfxEmitter != NULL )
                {
                    pEntity = rage_new SampleCutsceneAnimatedParticleEffectEntity( pSimpleWorldRmptfxEmitter, 
                        pSimpleWorldRmptfxEmitter->GetEffectInst(), pObject, pSimpleWorldRmptfxEmitter->GetAnimMgr()->GetMotionTree() );
                }
            }
        }
        break;
    case CUTSCENE_SUBTITLE_OBJECT_TYPE:
        {
            if ( m_pSubtitleEntity == NULL )
            {
                m_pSubtitleEntity = rage_new SampleCutsceneSubtitleEntity;
            }

            m_pSubtitleEntity->AddObject( pObject );
            pEntity = m_pSubtitleEntity;
        }
        break;
    case CUTSCENE_SCREEN_FADE_OBJECT_TYPE:
        {
            pEntity = rage_new SampleCutsceneScreenFadeEntity( pObject );
        }
        break;
    case CUTSCENE_HIDDEN_MODEL_OBJECT_TYPE:
    case CUTSCENE_FIXUP_MODEL_OBJECT_TYPE:
        {
            // handle silently
        }
        return NULL;
    case CUTSCENE_EVENTS_OBJECT_TYPE:
        {
            pEntity = rage_new SampleCutsceneEventEntity( pObject );
        }
        break;
    case CUTSCENE_BLOCKING_BOUNDS_OBJECT_TYPE:
        {
            const cutfBlockingBoundsObject *pBlockingBoundsObject = dynamic_cast<const cutfBlockingBoundsObject *>( pObject );

            SampleBlockingBounds *pBlockingBounds = m_pSampleCutscene->GetBlockingBounds( pBlockingBoundsObject->GetDisplayName().c_str() );
            if ( pBlockingBounds != NULL )
            {
                pEntity = rage_new SampleCutsceneBlockingBoundsEntity( pBlockingBounds, pObject );
            }
        }
        break;
    case CUTSCENE_REMOVAL_BOUNDS_OBJECT_TYPE:
        {
            const cutfRemovalBoundsObject *pRemovalBoundsObject = dynamic_cast<const cutfRemovalBoundsObject *>( pObject );

            SampleRemovalBounds *pRemovalBounds = m_pSampleCutscene->GetRemovalBounds( pRemovalBoundsObject->GetDisplayName().c_str() );
            if ( pRemovalBounds != NULL )
            {
                pEntity = rage_new SampleCutsceneRemovalBoundsEntity( pRemovalBounds, pObject );
            }
        }
        break;
    default:
        break;
    }

    if ( pEntity != NULL )
    {
        sampleCutsDisplayf( "Created '%s' entity for object '%s'", pObject->GetTypeName(), pObject->GetDisplayName().c_str() );
    }
    else
    {
        sampleCutsDisplayf( "Objects of type '%s' are not yet supported.", pObject->GetTypeName() );
    }

    return pEntity;
}

void SampleCutsceneManager::ReleaseEntity( cutsEntity *pEntity, const cutfObject* pObject )
{
    // This is weird doing this here.  Perhaps we should add events to reserve/release the game camera and lights.  That way the 
    // game has forewarning of what items are going to be used.
    switch ( pObject->GetType() )
    {
    case CUTSCENE_CAMERA_OBJECT_TYPE:
        {
            m_pSampleCutscene->DeleteAnimCamera( pObject->GetDisplayName().c_str() );
        }
        break;    
    case CUTSCENE_LIGHT_OBJECT_TYPE:
        {
            m_pSampleCutscene->DeleteAnimLight( pObject->GetDisplayName().c_str() );
        }
        break;
    default:
        break;
    }

    switch ( pEntity->GetType() )
    {
    case CUTSCENE_UNIQUE_ENTITY_TYPE:
        {
            sampleCutsDisplayf( "Released '%s' entity for object '%s'", pObject->GetTypeName(), pObject->GetDisplayName().c_str() );
            delete pEntity;
        }
        break;
    case CUTSCENE_SINGLETON_ENTITY_TYPE:
        {
            cutsSingletonEntity *pSingletonEntity = dynamic_cast<cutsSingletonEntity *>( pEntity );
            pSingletonEntity->RemoveObject( pObject );
            sampleCutsDisplayf( "Removed object '%s' from entity '%s'", pObject->GetDisplayName().c_str(), pObject->GetTypeName() );

            if ( pSingletonEntity->GetObjectList().GetCount() == 0 )
            {
                if ( pEntity == m_pSubtitleEntity )
                {
                    m_pSubtitleEntity = NULL;
                }

                sampleCutsDisplayf( "Released '%s' entity for object '%s'", pObject->GetTypeName(), pObject->GetDisplayName().c_str() );
                delete pSingletonEntity;
            }
        }
        break;
    default:
        {
            sampleCutsDisplayf( "Unknown entity type id %d for entity '%s', object '%s'.", pEntity->GetType(), 
                pObject->GetTypeName(), pObject->GetDisplayName().c_str() );
        }
        break;
    }    
}

#if __BANK

SEditCutfObjectLocationInfo* SampleCutsceneManager::GetHiddenObjectInfo()
{
    // FIXME: For now, just generate a random hidden object
    static int iIndex = 0;
    
    SEditCutfObjectLocationInfo *pEditObjectLocationInfo = rage_new SEditCutfObjectLocationInfo;
    sprintf( pEditObjectLocationInfo->cName, "hiddenObject_%d", iIndex );
    pEditObjectLocationInfo->vPosition.Set( (iIndex % 2) * 1.0f, (iIndex % 3) * 1.0f, (iIndex % 2) * 1.0f );
    pEditObjectLocationInfo->fRadius = ((iIndex % 2) * 1.0f) + 0.25f;

    ++iIndex;
    
    return pEditObjectLocationInfo;
}

SEditCutfObjectLocationInfo* SampleCutsceneManager::GetFixupObjectInfo()
{
    // FIXME: For now, just generate a random fixup object
    static int iIndex = 0;

    SEditCutfObjectLocationInfo *pEditObjectLocationInfo = rage_new SEditCutfObjectLocationInfo;
    sprintf( pEditObjectLocationInfo->cName, "fixupObject_%d", iIndex );
    pEditObjectLocationInfo->vPosition.Set( (iIndex % 2) * 1.0f, (iIndex % 3) * 1.0f, (iIndex % 2) * 1.0f );
    pEditObjectLocationInfo->fRadius = ((iIndex % 2) * 1.0f) + 0.25f;

    ++iIndex;

    return pEditObjectLocationInfo;
}

#endif // __BANK

} // namespace rage
