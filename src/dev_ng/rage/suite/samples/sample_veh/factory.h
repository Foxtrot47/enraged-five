// 
// sample_veh/factory.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_VEH_FACTORY_H
#define SAMPLE_VEH_FACTORY_H

#include "data.h"
#include "vehicle.h"

#include "string/string.h"
#include "vehbase/factory.h"
#include "vehdyna/auto.h"


namespace rage {

class sampleVehicle;
class sampleVehicleType;
class vehEntity;
class vehInput;
class vehSim;
class vehModel;
class vehDamage;
class vehAudio;
class vehFeedback;
class Vector3;

/* Purpose: Vehicle Factory used to create a vehicle of a specified type. 
   Usage: Call sampleVehicleFactory::Constuct( vehicleType ) to allocate
      and initialize a vehicle of type 'vehicleType'.
*/
class sampleVehicleFactory : public vehFactory {
public:
							sampleVehicleFactory();				// Default constructor
	virtual					~sampleVehicleFactory();			// Destructor

	// PURPOSE: Manage vehicle pointer
	sampleVehicle*			GetEntity()	const					{ return static_cast<sampleVehicle*>(m_EntityRef); }	// Return pointer to vehicle
	void					SetEntity(sampleVehicle* entity)	{ m_EntityRef = entity; }								// Set pointer to vehicle

	// PURPOSE: Manage pointer to type data
	sampleVehicleType*		GetType() const						{ return static_cast<sampleVehicleType*>(m_TypeRef); }	// Return pointer to type
	void					SetType(sampleVehicleType* type)	{ m_TypeRef = type; }									// Set pointer to type

	sampleVehicle*			Construct(sampleVehicleType*);		// Allocate and initialize a vehicle given a pointer to the type data

protected:
	// PURPOSE: Factory methods to allocate and initialize a vehicle by component
	virtual void			MakeEntity();						// Factory method - vehicle entity
	virtual void			MakeInput();						// Factory method - vehicle input
	virtual void			MakeSim();							// Factory method - vehicle sim
	virtual void			MakeModel();						// Factory method - vehicle model
	virtual void			MakeDamage();						// Factory method - vehicle damage
	virtual void			MakeAudio();						// Factory method - vehicle audio
	virtual void			MakeGyro();							// Factory method - vehicle gyro
	virtual void			MakeStuck();						// Factory method - vehicle stuck
	virtual void			MakeFeedback();						// Factory method - vehicle controller feedback
	virtual void			MakeTelemetry();					// Factory method - vehicle telemetry (development)

private:
};

} // namespace rage

#endif	// SAMPLE_VEH_FACTORY_H
