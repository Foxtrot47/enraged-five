// 
// sample_veh/drawable.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "drawable.h"

#include "atl/bintree_rsc.h"
#include "file/token.h"
#include "phbound/bound.h"
#include "rmcore/typefileparser.h"

using namespace rage;

sampleDrawable::sampleDrawable()
	: rmcDrawable(),
	m_Bound(NULL)
{
}

sampleDrawable::~sampleDrawable() 
{
	if (m_Bound && phConfig::IsRefCountingEnabled())
	{
		m_Bound->Release();
	}
	else
	{
		delete m_Bound;
	}
}

void sampleDrawable::LoadBoneTags(rmcTypeFileCbData *data)
{
	Locator loc;

	loc.BoneIndex = data->m_T->GetInt();

	m_Locators.AddLocator(data->m_EntityName, loc);
}

void sampleDrawable::LoadBound(rmcTypeFileCbData *data)
{
	Assertf(m_Bound==NULL,"Only one bound allowed, use a composite bound");

	fiTokenizer *T = data->m_T;
	char buff[128];
	T->GetToken(buff, sizeof(buff)); 

	if(stricmp(buff,"none"))
	{
		m_Bound = phBound::Load(buff);
	}
}


void sampleDrawable::LoadLocators(rmcTypeFileCbData *data)
{
	Locator loc;

	data->m_T->MatchToken( "boneindex" );
	loc.BoneIndex = data->m_T->GetInt();
	data->m_T->MatchToken( "offset" );
	data->m_T->GetVector( loc.Offset );
	data->m_T->MatchToken( "eulers" );
	data->m_T->GetVector( loc.Eulers );

	if (data->m_T->CheckToken( "[", false ) )
	{
		data->m_T->CheckToken( "[" );

		while ( data->m_T->CheckToken( "]", false ) == false )
		{
			// Currently, we're just reading in the keys and values, and ignoring them
			const int MAX_KEY_AND_VALUE_LENGTH = 256;
			char keyValue[MAX_KEY_AND_VALUE_LENGTH];
			data->m_T->GetToken( keyValue, MAX_KEY_AND_VALUE_LENGTH );
		}

		data->m_T->CheckToken( "]" );
	}

	m_Locators.AddLocator( data->m_EntityName, loc );
}

const sampleDrawable::Locator* sampleDrawable::GetLocator(const char* name) const
{
	const Locator* locator = m_Locators.GetLocator( name );
	return locator;
}

bool sampleDrawable::Load(const char *basename, rmcTypeFileParser *parser, bool configParser)
{
	return rmcDrawable::Load(basename, parser, configParser);
}

bool sampleDrawable::Load(fiTokenizer &T, rmcTypeFileParser *parser, bool configParser)
{	//:bLoadBaseDataSet
	//	This flag indicates whether to load the structural information (locators / skeletontype
	static rmcTypeFileParser defaultParser;
	if ( !parser )
	{
		parser = &defaultParser;
		parser->Reset();
	}

	if ( configParser )
	{
		parser->RegisterLoader( "bound", "all", datCallback(MFA1(sampleDrawable::LoadBound), this, 0, true));
		parser->RegisterLoader( "locator", "all", datCallback(MFA1(sampleDrawable::LoadLocators), this, 0, true) );
		parser->RegisterLoader( "bonetag", "all", datCallback(MFA1(sampleDrawable::LoadBoneTags), this, 0, true) );

		parser->RegisterLoader( "shadinggroup", "shadinggroup", datCallback(MFA1(rmcDrawable::LoadShader), this, 0, true) );
		parser->RegisterLoader( "lodgroup", "mesh", datCallback(MFA1(rmcDrawable::LoadMesh), this, 0, true) );
		parser->RegisterLoader( "edge", "edge", datCallback(MFA1(rmcDrawable::LoadEdgeModel), this, 0, true) );

		parser->RegisterLoader( "skel", "skel", datCallback(MFA1(rmcDrawable::LoadSkel), this, 0, true) );
	}

	parser->SetWarningSpew(false);

	// Call the base class Load(), while we handle any callbacks.
	bool retval = rmcDrawable::Load(T, parser, false);

	return retval;
}
