// 
// sample_veh/data.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_VEH_DATA_H
#define SAMPLE_VEH_DATA_H

#include "vector/vector3.h"
#include "vehbase/type.h"

namespace rage {

class sampleDrawable;
class crSkeletonData;
class phBound;
class phMaterial;
class vehChassisTune;
class vehGyroTune;
class vehStuckTune;
class vehInputTune;
class vehDriverTune;
class bkBank;

/* Purpose: vehCarType holds classes that are not duplicated for each car instance, such as
			skeleton data, tuning parameters, models and physical bounds. */
class sampleVehicleType : public vehType
{
public:
	// PURPOSE: Allocate and initialize a vehicle type data object
	// INPUTS: name - Name of vehicle type file (.type)
	static sampleVehicleType* Construct(const char * name);

public:
	virtual				~sampleVehicleType();		// Destructor

	// PURPOSE: Manage vehicle drawable
	sampleDrawable*		GetDrawable()				{ return m_Drawable; }					// Return pointer to drawable
	void				SetDrawable(sampleDrawable* skel) { m_Drawable = skel; }			// Set pointer to drawable
	bool				MakeDrawable();														// Allocate and initialize the drawable

	// PURPOSE: Manage vehicle skeleton
	crSkeletonData*		GetSkeleton()				{ return m_Skeleton; }					// Return pointer to skeleton
	void				SetSkeleton(crSkeletonData* skel) { m_Skeleton = skel; }			// Set pointer to skeleton
	bool				MakeSkeleton();														// Allocate and initialize the skeleton

	// PURPOSE: Manage vehicle bound
	phBound*			GetBound()					{ return m_Bound; }						// Return pointer to bound
	void				SetBound(phBound* pBound)	{ m_Bound = pBound; }					// Set pointer to bound
	bool				MakeBound();														// Allocate and initialize the bound

	// PURPOSE: Manage sim tune data pointer
	vehChassisTune*		GetSimTune()				{ return m_SimTune; }					// Return pointer to simulation tune data
	void				SetSimTune(vehChassisTune* pSimTune) { m_SimTune = pSimTune; }		// Set pointer to simulation tune data
	
	// PURPOSE: Manage gyro tune data pointer
	vehGyroTune*		GetGyroTune()				{ return m_GyroTune; }					// Return pointer to gyro tune data
	void				SetGyroTune(vehGyroTune* pGyroTune) { m_GyroTune = pGyroTune; }		// Set pointer to gyro tune data

	// PURPOSE: Manage stuck tune data pointer
	vehStuckTune*		GetStuckTune()				{ return m_StuckTune; }					// Return pointer to stuck tune data
	void				SetStuckTune(vehStuckTune* pStuckTune) { m_StuckTune = pStuckTune; }// Set pointer to stuck tune data

	// PURPOSE: Manage input tune data pointer
	vehInputTune*		GetInputTune()				{ return m_InputTune; }					// Return pointer to input tune data
	void				SetInputTune(vehInputTune* pInputTune) { m_InputTune = pInputTune; }// Set pointer to input tune data

	// PURPOSE: Manage driver tune data pointer
	vehDriverTune*		GetDriverTune()				{ return m_DriverTune; }				// Return pointer to driver tune data
	void				SetDriverTune(vehDriverTune* pDriverTune) { m_DriverTune = pDriverTune; } // Set pointer to driver tune data

	// PURPOSE: Factory method to create tune data
	bool				MakeTune();															// Allocate and initialize tune data

#if __BANK
	// PURPOSE: Add widgets to the bank
	void				AddWidgets(bkBank&);
#endif

protected:
	sampleVehicleType(const char * name);			// Constructor

	sampleDrawable*		m_Drawable;					// Pointer to drawable
	crSkeletonData*		m_Skeleton;					// Pointer to skeleton
	phBound*			m_Bound;					// Pointer to physics bound
	vehChassisTune*		m_SimTune;					// Pointer to sim tune data
	vehGyroTune*		m_GyroTune;					// Pointer to gyro tune data
	vehStuckTune*		m_StuckTune;				// Pointer to stuck tune data
	vehInputTune*		m_InputTune;				// Pointer to input tune data
	vehDriverTune*		m_DriverTune;				// Pointer to driver tune data
//	vehCarAudioData*	m_AudioTune;				// Pointer to audio tune data

private:

};

} // namespace rage

#endif	// SAMPLE_VEH_DATA_H
