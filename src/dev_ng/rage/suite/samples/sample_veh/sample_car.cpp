// 
// sample_veh/sample_car.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "data.h"
#include "factory.h"

#include "bank/bkmgr.h"
#include "devcam/cammgr.h"
#include "devcam/polarcam.h"
#include "input/keys.h"
#include "phcore/phmath.h"
#include "physics/constraint.h"
#include "sample_physics/demoobject.h"
#include "sample_physics/sample_physics.h"
#include "system/main.h"
#include "system/param.h"
#include "vehdyna/auto.h"
#include "vehdyna/automgr.h"

#define MAX_NUM_VEHICLES	64

PARAM(asset, "Vehicle asset directory");
PARAM(folder, "Vehicle folder subdirectory");
PARAM(file, "Vehicle type file name (.type)");

namespace ragesamples {

using namespace rage;

/* Purpose: Allocate and insert a vehicle into the tester */
class vehicleSampleManager : public physicsSampleManager
{
public:
	// PURPOSE: Construct a vehicle manager with a max of 5 vehicles
	vehicleSampleManager()
		: m_VehicleData(NULL)
		, m_VehicleList(0,MAX_NUM_VEHICLES)
		, m_VehicleResetList(0,MAX_NUM_VEHICLES)
		, m_VehicleWorldList(0,MAX_NUM_VEHICLES)
	{
	}

	void CreateVehicle (Vector3::Param position=ORIGIN, Vector3::Param rotation=ORIGIN)
	{
		// Create the vehicle.
		Assert(m_VehicleList.GetCount()<MAX_NUM_VEHICLES);
		sampleVehicleFactory factory;
		sampleVehicle* vehicle = factory.Construct(m_VehicleData);

		// Set its matrix.
		Matrix34& reset = m_VehicleResetList.Append();
		reset.Set(CreateRotatedMatrix(position,rotation));
		vehicle->GetInstance()->SetMatrix(RCC_MAT34V(reset));
		vehicle->GetSim()->GetCollider()->SetColliderMatrixFromInstance();

		// Put the vehicle in the vehicle array.
		m_VehicleList.Push(vehicle);
		m_VehicleWorldList.Push(m_Demos.GetCurrentWorld());
	}

	// PURPOSE: Create a physics world with one vehicle. Allocate
	// a vehicle by creating a vehicle type and using that type
	// to create one or more vehicle instances.
	virtual void InitClient()
	{
		// Set the world up direction.
		SetUnitUp(m_UseZUp ? ZAXIS : YAXIS);
		vehWheel::s_WheelBoundRotationZ = false;

		// Get the asset path, folder name and vehicle name.
		const char* assetDir = "t:/rage/assets";
		PARAM_asset.Get(assetDir);
		const char* folderName = "physics";
		PARAM_folder.Get(folderName);
		const char* vehicleName = "vp_mustang";
		//const char* vehicleName = "vp_motorcycle";
		//const char* vehicleName = "vp_elise_04";
		PARAM_file.Get(vehicleName);

		INIT_PARSER;
		Vector3 position,rotation;

		physicsSampleManager::InitClient();

		m_Mapper.Reset();
		m_Mapper.Map(IOMS_KEYBOARD, KEY_F4, m_Reset);

		// Create the vehicle data.
		ASSET.SetPath(assetDir);
		ASSET.PushFolder(folderName);
		m_VehicleData = sampleVehicleType::Construct(vehicleName);
		m_VehicleData->AddRef();
		ASSET.PopFolder();

		phDemoWorld& carOnPlaneWorld = *(rage_new phDemoWorld("car on plane"));
		m_Demos.AddDemo(&carOnPlaneWorld);
		carOnPlaneWorld.Init(1000);
		carOnPlaneWorld.CreateFixedObject("plane_bumpy");
		carOnPlaneWorld.CreateFixedObject("plane_bvh");
		//carOnPlaneWorld.ConstructTerrainPlane();
	//	position.Set(0.0f,3.0f,0.0f);
	//	rotation.Set(0.0f,0.0f,0.5f*PI);
	//	float radius = 0.5f;
	//	float height = 0.5f;
	//	float ringCurveRadius = 0.6f;
	//	float capsCurveRadius = 0.8f;
	//	carOnPlaneWorld.CreateCurvedGeometryCylinder(position,rotation,radius,height,ringCurveRadius,capsCurveRadius);
		position.Set(0.0f,3.0f,0.0f);
		rotation.Set(0.8f,0.0f,0.0f);
	//	carOnPlaneWorld.CreateObject("wheel_pair",position,false,rotation);
		CreateVehicle();

		phDemoWorld& mcTestWorld = *(rage_new phDemoWorld("MC test"));
		m_Demos.AddDemo(&mcTestWorld);
		mcTestWorld.Init(1000);
		mcTestWorld.CreateFixedObject("plane_bvh");
		//mcTestWorld.ConstructPyramid(Vector3(4.0f,0.0f,0.0f),3,true);
		//mcTestWorld.ConstructPyramid(Vector3(4.0f,0.0f,-5.0f),3,true);
		//mcTestWorld.ConstructPyramid(Vector3(-4.0f,0.0f,0.0f),3,true);
		//mcTestWorld.ConstructPyramid(Vector3(-4.0f,0.0f,-5.0f),3,true);
		//mcTestWorld.ConstructTable(Vector3(0.0f,0.0f,-3.0f));
		//mcTestWorld.ConstructTable(Vector3(0.0f,0.0f,-5.0f));
		//mcTestWorld.ConstructTable(Vector3(0.0f,0.0f,-7.0f));
		//mcTestWorld.ConstructTable(Vector3(0.0f,0.0f,-9.0f));
		//mcTestWorld.ConstructTable(Vector3(0.0f,0.0f,-11.0f));
		position.Set(0.0f,2.0f,-6.0f);
		phInst* gate = mcTestWorld.CreateObject("box_gate",position)->GetPhysInst();
		Vector3 halfWidth(gate->GetArchetype()->GetBound()->GetBoundingBoxMax());
		position.x += halfWidth.x;
		position.y -= halfWidth.y;
		mcTestWorld.AttachObjectToWorld(position,gate);
		position.y += 2.0f*halfWidth.y;
		mcTestWorld.AttachObjectToWorld(position,gate);
		phConstraint* constraint = mcTestWorld.AttachObjectToWorld(position,gate);
		constraint->SetFixedRotation();
		constraint->SetDegreesConstrained(1,YAXIS);
		constraint->SetHardLimitMax(0.25f*PI);
		constraint->SetHardLimitMin(-PI);

		CreateVehicle();

		int startingDemo = 0;
		m_Demos.SetCurrentDemo(startingDemo);
		m_Demos.GetCurrentWorld()->Activate();

		ResetVehicles();

#if __BANK
		int numVehicles = m_VehicleList.GetCount();
		m_VehicleData->AddWidgets(BANKMGR.CreateBank("Vehicle Data"));
		for (int vehicleIndex=0; vehicleIndex<numVehicles; vehicleIndex++)
		{
			char vehicleName[64];
			formatf(vehicleName,"Vehicle %i Inst",vehicleIndex);
			m_VehicleList[vehicleIndex]->AddWidgets(BANKMGR.CreateBank(vehicleName));
		}
#endif

	}

	virtual void InitCamera ()
	{
		Vector3 lookFrom(0.0f,5.0f,10.0f);
		lookFrom.Add(phDemoObject::GetWorldOffset());
		Vector3 lookTo(0.0f,0.0f,0.0f);
		lookTo.Add(phDemoObject::GetWorldOffset());
		InitSampleCamera(lookFrom,lookTo);
		m_CamMgr.SetCamera(dcamCamMgr::CAM_POLAR);
	}

	virtual void ShutdownClient()
	{
		vehAutoMgr::Destruct();

		for(int i=m_VehicleList.GetCount()-1; i>=0; i--)
		{
			delete m_VehicleList[i];
		}
		m_VehicleList.Reset();

		if(m_VehicleData->Release() == 0)
		{
			m_VehicleData = NULL;
		}

		SHUTDOWN_PARSER;

		physicsSampleManager::ShutdownClient();
	}

	void ResetVehicles()
	{
		int numVehicles = m_VehicleList.GetCount();
		for (int vehicleIndex=0; vehicleIndex<numVehicles; vehicleIndex++)
		{
			m_VehicleList[vehicleIndex]->Reset();
			m_VehicleList[vehicleIndex]->GetInstance()->SetMatrix(RCC_MAT34V(m_VehicleResetList[vehicleIndex]));
			m_VehicleList[vehicleIndex]->GetSim()->GetCollider()->SetColliderMatrixFromInstance();
		}
	}

	virtual void Reset()
	{
		ResetVehicles();
		physicsSampleManager::Reset();
	}

	virtual void Update()
	{
		if (m_Reset.IsPressed())
		{
			Reset();
		}

		physicsSampleManager::Update();

		const phDemoWorld* activeWorld = m_Demos.GetCurrentWorld();
		int numVehicles = m_VehicleList.GetCount();
		for (int vehicleIndex=0; vehicleIndex<numVehicles; vehicleIndex++)
		{
			if (m_VehicleWorldList[vehicleIndex]==activeWorld)
			{
				m_VehicleList[vehicleIndex]->GetSim()->ProfileDraw();
				if (activeWorld->ShouldUpdate())
				{
					m_VehicleList[vehicleIndex]->Iterate();
				}
			}
		}

		// Sync polar camera to position of vehicle
		if(m_VehicleList[0] && m_CamMgr.GetCurrentCamera() == dcamCamMgr::CAM_POLAR)
		{
			dcamPolarCam& camPolar = static_cast<dcamPolarCam&>(m_CamMgr.GetCamera(dcamCamMgr::CAM_POLAR));
			Vector3 camOffset = camPolar.GetOffset();
			camOffset = RCC_MATRIX34(m_VehicleList[0]->GetInstance()->GetMatrix()).d;
			camPolar.SetOffset(camOffset);
		}
	}

	virtual void DrawClient()
	{
		// TODO: draw the car?

		physicsSampleManager::DrawClient();
	}

private:
	sampleVehicleType*	m_VehicleData;
	atArray<Matrix34> m_VehicleResetList;
	atArray<sampleVehicle*>	m_VehicleList;
	atArray<phDemoWorld*> m_VehicleWorldList;
};

} // namespace ragesamples

// main application
int Main()
{
	{
		ragesamples::vehicleSampleManager samplePhysics;
		samplePhysics.Init();

		samplePhysics.UpdateLoop();

		samplePhysics.Shutdown();
	}

	return 0;
}
