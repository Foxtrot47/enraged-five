// 
// sample_veh/data.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "data.h"

#include "drawable.h"

#include "bank/bank.h"
#include "crskeleton/skeletondata.h"
#include "file/asset.h"
#include "phbound/bound.h"
#include "phcore/material.h"
#include "rmcore/drawable.h"
#include "string/string.h"
#include "vehdyna/chassis.h"
#include "vehdyna/driver.h"
#include "vehdyna/gyro.h"
#include "vehdyna/playerinput.h"
#include "vehdyna/stuck.h"


namespace rage {

#define INERTIA_SIZE_SCALAR	1.25f

sampleVehicleType* sampleVehicleType::Construct(const char * name)
{
	sampleVehicleType* type = rage_new sampleVehicleType(name);
	type->MakeDrawable();
	type->MakeSkeleton();
	type->MakeBound();
	type->MakeTune();
	return type;
}


sampleVehicleType::sampleVehicleType(const char * name)
	: vehType(name)
{
	m_Skeleton = NULL;
	m_Bound = NULL;
	m_SimTune = NULL;
	m_GyroTune = NULL;
	m_StuckTune = NULL;
	m_InputTune = NULL;
	m_DriverTune = NULL;
}

sampleVehicleType::~sampleVehicleType()
{
//	Assert(m_Skeleton->GetRefCount() == 1);
	m_Skeleton->Release();
	if(phConfig::IsRefCountingEnabled())
	{
		Assert(m_Bound->GetRefCount() == 1);
		m_Bound->Release();
	}
	else
	{
		delete m_Bound;
	}
	delete m_SimTune;
	delete m_GyroTune;
	delete m_StuckTune;
	delete m_InputTune;
	delete m_DriverTune;
}

bool sampleVehicleType::MakeDrawable()
{
	m_Drawable = rage_new sampleDrawable();

	ASSET.PushFolder(m_Name);
	if (!m_Drawable->Load("entity"))
	{
		delete m_Drawable;
		m_Drawable = NULL;
	}
	ASSET.PopFolder();

	return true;
}

bool sampleVehicleType::MakeSkeleton()
{
	if(m_Drawable)
	{
		m_Skeleton = m_Drawable->GetSkeletonData();
		m_Skeleton->AddRef();
	}
	else
	{
		ASSET.PushFolder(m_Name);
		m_Skeleton = crSkeletonData::AllocateAndLoad(m_Name);
		ASSET.PopFolder();
	}

	return true;
}

bool sampleVehicleType::MakeBound()
{
	// Initialize bound
	if(m_Drawable)
	{
		m_Bound = m_Drawable->GetBound();
		m_Bound->AddRef();
	}
	else
	{
		m_Bound = phBound::Load(m_Name);
	}

	return true;
}

bool sampleVehicleType::MakeTune()
{
	bool parSetting0 = PARSER.Settings().GetFlag(parSettings::LOAD_UNKNOWN_TYPES);
	PARSER.Settings().SetFlag(parSettings::LOAD_UNKNOWN_TYPES, true);

	m_InputTune = rage_new vehInputTune();
	m_InputTune->Allocate();
	m_InputTune->LoadData(m_Name,"vehInput");

	m_SimTune = rage_new vehChassisTune();
	m_SimTune->Allocate();
	m_SimTune->LoadData(m_Name,"vehSim");		  
													   
	m_SimTune->GetBoundFriction();
	m_SimTune->GetBoundElasticity();

	// Better defaults based on type data
	Vector3 size;
	Assertf(GetBound(),"No bound assigned");
	size.Subtract(GetBound()->GetBoundingBoxMax(),GetBound()->GetBoundingBoxMin());
	size.y=GetBound()->GetBoundingBoxMax().y;
	Vector3 Size=m_SimTune->GetSize();
	if(Size.x == 0.0f)
		Size.x = size.x;
	if(Size.y == 0.0f)
		Size.y = size.y;
	if(Size.z == 0.0f)
		Size.z = size.z;
	m_SimTune->SetSize(Size);

	// Initialize m_pICS
	Vector3 InertiaBox=m_SimTune->GetInertiaBox();
	if(InertiaBox.IsZero()) {
		InertiaBox.Scale(m_SimTune->GetSize(),INERTIA_SIZE_SCALAR);
		m_SimTune->SetInertiaBox(InertiaBox);
	}

	m_GyroTune = rage_new vehGyroTune();
	m_GyroTune->Allocate();
	m_GyroTune->LoadData(m_Name,"vehGyro");

	m_StuckTune = rage_new vehStuckTune();
	m_StuckTune->Allocate();
	m_StuckTune->LoadData(m_Name,"vehStuck");

//	m_DriverTune = new vehDriverTune();
//	m_DriverTune->Allocate();
//	m_DriverTune->Load(m_Name,"vehDriver");

//	m_AudioTune = new vehCarAudioData();
//	m_AudioTune->Allocate();
//	m_AudioTune->Load(m_Name,"vehAudio");

	PARSER.Settings().SetFlag(parSettings::LOAD_UNKNOWN_TYPES, parSetting0);

	return true;
}

#if __BANK
void sampleVehicleType::AddWidgets(bkBank& B)
{
//	B.PushGroup("Type",false);
		B.PushGroup("Tune",false);
		if(m_InputTune) {
			m_InputTune->AddWidgets(B);
		}
		if(m_SimTune) {
			m_SimTune->AddWidgets(B);
		}
		if(m_GyroTune) {
			m_GyroTune->AddWidgets(B);
		}
		if(m_StuckTune) {
			m_StuckTune->AddWidgets(B);
		}
		if(m_DriverTune) {
			m_DriverTune->AddWidgets(B);
		}
//		if(m_AudioTune) {
//			m_AudioTune->AddWidgets(B);
//		}
		B.PopGroup();
//		if(m_BonyData) {
//			m_BonyData->AddWidgets(B);
//		}
//	B.PopGroup();
}
#endif

} // namespace rage
