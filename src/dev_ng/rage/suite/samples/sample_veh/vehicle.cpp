// 
// sample_veh/vehicle.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "vehicle.h"

#if __BANK
#include "bank/bank.h"
#include "bank/bkmgr.h"
#endif

#include "crskeleton/skeleton.h"

sampleVehicle::sampleVehicle()
	: vehAuto()
{
	m_Data = NULL;
	m_Skeleton = NULL;
}

sampleVehicle::~sampleVehicle()
{
	delete m_Skeleton;
	m_Skeleton = NULL;

	m_Data->Release();
}

#if __BANK
void sampleVehicle::AddWidgets(bkBank &bank)
{
	vehAuto::AddWidgets(bank);

}
#endif
