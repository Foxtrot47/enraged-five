// 
// sample_veh/drawable.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_VEH_DRAWABLE_H
#define SAMPLE_VEH_DRAWABLE_H
#include "atl/bintree.h"
#include "rmcore/drawable.h"
#include "string/string.h"

namespace rage {

class rmcTypeFileParser;
class fiTokenizer;
class phBound;
class datResource;
class grmShaderGroup;


class sampleDrawable : public rmcDrawable
{
public:
	struct Locator
	{
		int BoneIndex;
		Vector3 Offset;
		Vector3 Eulers;

		Locator() : BoneIndex(-1), Offset(0.0f,0.0f,0.0f), Eulers(0.0f,0.0f,0.0f) {}
		Locator( datResource& UNUSED_PARAM(rsc) ) {}
	};

public:
	sampleDrawable();
	sampleDrawable(class datResource&);
	sampleDrawable(class datResource&, grmShaderGroup*);

	~sampleDrawable();

	bool Load(const char *basename = "entity", rmcTypeFileParser *parser = 0, bool configParser = true);
	bool Load(fiTokenizer &T, rmcTypeFileParser *parser = 0, bool configParser = true);

	void LoadBoneTags(rmcTypeFileCbData *data);
	void LoadBound(rmcTypeFileCbData *data);
	void LoadLocators(rmcTypeFileCbData *data);

	phBound* GetBound() const						{ return m_Bound; }
	void SetBound(phBound* bound)					{ m_Bound = bound; }

	const Locator* GetLocator( const char* name ) const;

protected:
	class LocatorData
	{
	public:
		LocatorData() { }

		~LocatorData() { m_Locators.DeleteAll(); }

		LocatorData(class datResource& rsc);

		void AddLocator( const char *name, const Locator& info )	{ m_Locators.Insert( name, info ); }
		const Locator* GetLocator( const char *name ) const { return m_Locators.Access( name ); }

		int GetLocatorCount() const { return m_Locators.GetNumNodes(); }

	private:
		atBinTree<ConstString,Locator> m_Locators;
	};

	phBound*		m_Bound;
	LocatorData		m_Locators;

private:
};

} // namespace rage

#endif	// SAMPLE_VEH_DRAWABLE_H
