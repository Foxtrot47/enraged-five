// 
// sample_veh/vehicle.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_VEH_VEHICLE_H
#define SAMPLE_VEH_VEHICLE_H

#include "data.h"
#include "vehdyna/auto.h"
#include "vehdyna/playerinput.h"

namespace rage {

class crSkeleton;


class sampleVehicle : public vehAuto
{
public:
	sampleVehicle();				// Default constructor
	virtual ~sampleVehicle();		// Destructor

	// PURPOSE: Manage pointer to user input
	void				SetInput(vehPlayerInput* input)	{ m_Input=input; }									// Set pointer to user input
	vehPlayerInput*		GetPlayerInput() const			{ return dynamic_cast<vehPlayerInput*>(m_Input); }	// Return pointer to vehPlayerInput::

	// PURPOSE: Manage pointer to vehicle type data
	void					SetData(const sampleVehicleType* data)	{ m_Data = data; m_Data->AddRef(); }	// Set pointer to type data
	const sampleVehicleType* GetData() const						{ return m_Data; }						// Return pointer to type data

	// PURPOSE: Manage pointer to skeleton instance
	crSkeleton*				GetSkeleton()							{ return m_Skeleton; }					// Set pointer to skeleton
	void					SetSkeleton(crSkeleton* skeleton)		{ m_Skeleton = skeleton; }				// Return pointer to skeleton

//	const Matrix34& ReadLocalBoneMatrix( int boneIdx ) const
//	{
//		Assert("Invalid bone index" && boneIdx >= 0 && boneIdx < GetModel()->GetModel()->GetBoneCount());
//		return m_Skeleton->GetBone( boneIdx ).GetLocalMtx();
//	};

//	Matrix34* GetLocalBoneMatrix( int boneIdx )
//	{ 
//		Assert("Invalid bone index" && boneIdx >= 0 && boneIdx < GetModel()->GetModel()->GetBoneCount());
//		return &m_Skeleton->GetBone( boneIdx ).GetLocalMtx();
//	}

//	Matrix34* GetWorldBoneMatrix( int boneIdx )
//	{ 
//		Assert("Invalid bone index" && boneIdx >= 0 && boneIdx < GetModel()->GetModel()->GetBoneCount());
//		return &m_Skeleton->GetGlobalMtx( boneIdx );
//	}

#if __BANK
	// PURPOSE: Add widgets to the bank
	void AddWidgets(bkBank &bk);
#endif

protected:
	// PURPOSE: Copy constructor & assignment operator
	const sampleVehicle *operator=(const sampleVehicle &);			// Not implemented.
	sampleVehicle(const sampleVehicle &);							// Not implemented.

	crSkeleton* m_Skeleton;											// Pointer to skeleton instance data

	const sampleVehicleType* m_Data;								// Pointer to vehicle type data
};

} // namespace rage

#endif	// SAMPLE_VEH_VEHICLE_H
