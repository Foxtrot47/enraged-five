// 
// sample_veh/factory.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "factory.h"

#include "data.h"
#include "vehicle.h"

#include "crskeleton/bonedata.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "diag/memstats.h"
#include "string/string.h"
#include "vehbase/entity.h"
#include "vehbase/factory.h"
#include "vehbase/feedback.h"
#include "vehbase/input.h"
#include "vehbase/model.h"
#include "vehbase/sim.h"
#include "vehdyna/chassis.h"
#include "vehdyna/gyro.h"
#include "vehdyna/instance.h"
#include "vehdyna/playerinput.h"
#include "vehdyna/stuck.h"
#include "vehdyna/telemetry.h"


namespace rage {

/*
 *	sampleVehicleFactory()
 */

sampleVehicleFactory::sampleVehicleFactory()
{
}


/*
 *	~sampleVehicleFactory()
 */

sampleVehicleFactory::~sampleVehicleFactory()
{
}

/*
 *	ConstructEntity()
 */

sampleVehicle* sampleVehicleFactory::Construct(sampleVehicleType* type) {
	m_TypeRef = type;
	m_TypeRef->AddRef();

	MakeEntity();
	diagLogMemory("MakeEntity");

	MakeSim();
	diagLogMemory("MakeSim");

	MakeInput();
	diagLogMemory("MakeInput");

	MakeModel();
	diagLogMemory("MakeModel");

	MakeDamage();
	diagLogMemory("MakeDamage");

	MakeStuck();
	diagLogMemory("MakeStuck");

	MakeGyro();
	diagLogMemory("MakeGyro");

	MakeAudio();
	diagLogMemory("MakeAudio");

	MakeFeedback();
	diagLogMemory("MakeFeedback");

	MakeTelemetry();
	diagLogMemory("MakeTelemetry");

	return GetEntity();
}

void sampleVehicleFactory::MakeEntity()
{
	sampleVehicle* entity = rage_new sampleVehicle();
	entity->SetData(static_cast<const sampleVehicleType*>(m_TypeRef));

	// Make the car's physics instance a vehInstance (defined in vehdyna/auto).
	vehInstance* pInstance = rage_new vehInstance();
	pInstance->SetVehicle(entity);	// Attach the car to its instance
	entity->SetInstance(pInstance);

	// Instanciate the skeleton given the skel data and instance matrix
	crSkeleton* skeleton = rage_new crSkeleton();
	skeleton->Init(*GetType()->GetSkeleton(), &entity->GetInstance()->GetMatrix());
	entity->SetSkeleton(skeleton);

	SetEntity( entity );
}

/*
 *	MakeInput()
 */

void sampleVehicleFactory::MakeInput() {
	vehPlayerInput* pInput = rage_new vehPlayerInput();

	pInput->Init(static_cast<vehChassis*>(GetEntity()->GetSim()),GetType()->GetInputTune());
	pInput->SetIndex(0);
	pInput->EnableAutoReverse(true);

	GetEntity()->SetInput( pInput );
}

/*
 *	MakeSim()
 */

void sampleVehicleFactory::MakeSim() {
	vehChassis* pSim = rage_new vehChassis();

	pSim->SetTune(GetType()->GetSimTune());

	pSim->SetVehicle(GetEntity());

	pSim->MakeCollider(GetType()->GetName(), GetEntity()->GetInstance(), GetType()->GetBound());

	pSim->MakeAero(GetType()->GetName());

	pSim->MakeFluid(GetType()->GetName());

	pSim->MakeTransmission(GetType()->GetName());

	pSim->MakeEngine(GetType()->GetName());

	pSim->MakeWheels(GetType()->GetName(), GetEntity()->GetSkeleton(), "wheel_");

	pSim->MakeDrivetrains(GetType()->GetName());

	pSim->MakeAxles(GetType()->GetName(), GetEntity()->GetSkeleton(), "axle_");

	pSim->MakeSuspensions(GetType()->GetName(), GetEntity()->GetSkeleton(), "arm_", "shock_");

	pSim->ConfigureDrivetrain();

	pSim->ComputeConstants();

	pSim->SetResetPos(ORIGIN);

	GetEntity()->SetSim(pSim);
}

/*
 *	MakeModel()
 */

void sampleVehicleFactory::MakeModel() {
	vehModel* pModel = 0;

	GetEntity()->SetModel( pModel );
}

/*
 *	MakeDamage()
 */

void sampleVehicleFactory::MakeDamage() {
	vehDamage* pDamage = 0;

	GetEntity()->SetDamage( pDamage );
}

/*
 *	MakeStuck();
 */

void sampleVehicleFactory::MakeStuck() {
	vehStuck* pStuck = rage_new vehStuck();

	pStuck->Init(GetEntity()->GetSim(),GetType()->GetStuckTune());

	GetEntity()->SetStuck( pStuck );
}

/*
 *	MakeGyro();
 */

void sampleVehicleFactory::MakeGyro() {
	vehGyro* pGyro=NULL;

	pGyro = rage_new vehGyro();

	pGyro->Init(GetEntity()->GetSim(),GetType()->GetGyroTune());
	pGyro->SetLeanable(true);

	GetEntity()->SetGyro( pGyro );
}

/*
 *	MakeAudio()
 */

void sampleVehicleFactory::MakeAudio() {
	vehAudio* pAudio = 0;

	GetEntity()->SetAudio( pAudio );
}


/*
*	MakeFeedback()
*/

void sampleVehicleFactory::MakeFeedback() {
	vehFeedback* pFeedback = 0;

	GetEntity()->SetFeedback( pFeedback );
}

/*
*	MakeTelemetry()
*/

void sampleVehicleFactory::MakeTelemetry() {
	vehTelemetry* pTelemetry = rage_new vehTelemetry();
	pTelemetry->Init(GetEntity()->GetSim(), GetType()->GetName());

	GetEntity()->SetTelemetry( pTelemetry );
}

} // namespace rage
