// 
// sample_mt/sample_mt_creature.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Creature motion tree sample
// PURPOSE: This sample demonstrates a motion tree being used to update a creature.  



#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/framefilters.h"
#include "creature/creature.h"
#include "crextra/expressions.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeanimation.h"
#include "crmotiontree/observer.h"
#include "crmotiontree/requestanimation.h"
#include "crmotiontree/requestblend.h"
#include "crmotiontree/requestcapture.h"
#include "crmotiontree/requestexpression.h"
#include "crmotiontree/requestfilter.h"
#include "crmotiontree/requestframe.h"
#include "crmotiontree/requestmerge.h"
#include "crmotiontree/requestpm.h"
#include "crparameterizedmotion/parameter.h"
#include "crparameterizedmotion/parameterizedmotion.h"
#include "crparameterizedmotion/parameterizedmotionsimple.h"
#include "grmodel/shadergroup.h"
#include "input/pad.h"
#include "rmcore/drawable.h"
#include "sample_motiontree/sample_motiontree.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "zlib/zlib.h"

using namespace rage;

//#define DEFAULT_FILE "$/tempTestAssets/MC4_Karol/entity.type"
#define DEFAULT_FILE "$/fragments/cowboyNM/entity.type"
XPARAM(file);

//#define DEFAULT_ANIM "$/tempTestAssets/MC4_Karol/Karol_PinkSlip_Tweaked02.anim"
#define DEFAULT_ANIM ""
PARAM(anim, "[sample_mt_creature] Animation file to load (default is \"" DEFAULT_ANIM "\")");

//#define DEFAULT_ANIMMASKREMAP "$/tempTestAssets/MC4_Karol/animNrmMap_remap.txt"
#define DEFAULT_ANIMMASKREMAP ""
PARAM(animmaskremap, "[sample_mt_creature] Remap file for animated normal map masks");

#define DEFAULT_EXPR ""
PARAM(expr, "[sample_mt_creature] Expression file to load (default is \"" DEFAULT_EXPR "\")");

#define DEFAULT_PM ""
PARAM(pm, "[sample_mt_creature] Parameterized motion file to load (default is \"" DEFAULT_PM "\")");

PARAM(verbose, "[sample_mt_creature] Verbose output [and level] (default is none)");

#define PROFILE_MOTION_TREE (0)

namespace ragesamples 
{

class crMotionTreeSampleCreature : public ragesamples::crMotionTreeSampleManager
{
public:
	crMotionTreeSampleCreature() 
		: ragesamples::crMotionTreeSampleManager()
		, m_Animation(NULL)
		, m_Expressions(NULL)
		, m_Pm(NULL)
		, m_RageSummitDemo(false)
		, m_RageSummitDemo_UseSixaxis(false)
	{
		m_Filter.AddRef();
		m_AnimationObserver.AddRef();
		m_PmObserver.AddRef();
	}

protected:

	void InitClient()
	{
		crMotionTreeSampleManager::InitClient();
		crExpressions::InitClass();

		const char* animName = DEFAULT_ANIM;
		if(PARAM_anim.Get())
		{
			PARAM_anim.Get(animName);
		}

		if(animName && animName[0])
		{
			m_Animation = crAnimation::AllocateAndLoad(animName);
			if(!m_Animation)
			{
				Quitf("ERROR - failed to load animation file '%s'", animName);
			}
		}

		const char* exprName = DEFAULT_EXPR;
		if(PARAM_expr.Get())
		{
			PARAM_expr.Get(exprName);
		}

		const bool pack = true;
		if(exprName && exprName[0])
		{
			m_Expressions = crExpressions::AllocateAndLoad(exprName, pack);

			if(!m_Expressions)
			{
				Quitf("ERROR - failed to load expression file '%s'", exprName);
			}
			m_Expressions->AddRef();
			m_Expressions->InitializeCreature(GetCreature());
		}

		const char* pmName = DEFAULT_PM;
		if(PARAM_pm.Get())
		{
			PARAM_pm.Get(pmName);
		}

		if(pmName && pmName[0])
		{
			crpmParameterizedMotionSimpleData* spmData = crpmParameterizedMotionSimpleData::AllocateAndLoad(pmName);
			if(!spmData)
			{
				Quitf("ERROR - failed to load parameterized motion file '%s'", pmName);
			}

			crpmParameterizedMotionSimple* spm = rage_new crpmParameterizedMotionSimple;
			spm->Init(*spmData);
			spm->SetLooped(true);

			m_Pm = spm;
			m_Parameter.Init(m_Pm->GetNumParameterDimensions());
		}

		GetCreature().InitDofs(m_FrameData);

		m_FrameIn.Init(m_FrameData);
		m_FrameOut.Init(m_FrameData);

		GetCreature().Identity(m_FrameIn);

#if PROFILE_MOTION_TREE
		crmtRequestAnimation reqAnim(m_Animation);
		crmtRequestPm reqPm(m_Pm);
		reqAnim.SetLooping(true, true);
		crmtRequestMerge reqMerge(reqPm, reqAnim);
		crmtRequestExpression reqExpression(reqMerge, m_Expressions);

		GetMotionTree().Request(reqExpression);
#else // PROFILE_MOTION_TREE
		crmtRequestFrame reqFrame(&m_FrameIn);
		crmtRequestPm reqPm(m_Pm);
		crmtRequestAnimation reqAnim(m_Animation);
		reqAnim.SetLooping(true, true);
		crmtRequestMerge reqMerge1(reqPm, reqAnim);
		crmtRequestMerge reqMerge2(reqFrame, reqMerge1);
		crmtRequestExpression reqExpression(reqMerge2, m_Expressions);
		crmtRequestCapture reqCapture(reqExpression, &m_FrameOut);
		crmtRequestFilter reqFilter(reqCapture, &m_Filter);

		GetMotionTree().Request(reqFilter);
#endif // PROFILE_MOTION_TREE

		m_AnimationObserver.Attach(reqAnim.GetObserver());
		m_PmObserver.Attach(reqPm.GetObserver());

		if(PARAM_verbose.Get())
		{
			int verbosity = 2;
			PARAM_verbose.Get(verbosity);

			GetMotionTree().Dump(verbosity);
			if(m_Expressions)
			{
				m_Expressions->Dump(verbosity);
			}
		}

#if __BANK
		AddWidgetsCreature();
#else // __BANK
		if(m_Animation)
		{
			m_FrameIn.Invalidate();
		}
#endif // __BANK
	}

	void ShutdownClient()
	{
		if(m_Animation)
		{
			delete m_Animation;
			m_Animation = NULL;
		}
		if(m_Expressions)
		{
			delete m_Expressions;
			m_Expressions = NULL;
		}
		if(m_Pm)
		{
			delete m_Pm;
			m_Pm = NULL;
		}
		crExpressions::ShutdownClass();
		crMotionTreeSampleManager::ShutdownClient();
	}

	void DrawClient()
	{
		crMotionTreeSampleManager::DrawClient();
	}

	void UpdateClient()
	{
		crMotionTreeSampleManager::UpdateClient();

#if __BANK
		UpdateFrameInOutWidgets();

		if(m_Pm)
		{
			m_Pm->SetParameter(m_Parameter);
		}
#endif //__BANK

		if (m_RageSummitDemo)
		{
			Vec3V jawPosition;
			Vec3V eyePosition;
			Vec3V browLeftCircle;
			Vec3V browRightCircle;

			ioPad& pad = ioPad::GetPad(0);
			browLeftCircle[1] = pad.GetNormAnalogButton(ioPad::L2_INDEX) * 0.5f;
			browLeftCircle[0] = browLeftCircle[2] = 0.0f;
			browRightCircle[1] = pad.GetNormAnalogButton(ioPad::R2_INDEX) * 0.5f;
			browRightCircle[0] = browRightCircle[2] = 0.0f;
			float leftBlink = pad.GetNormAnalogButton(ioPad::L1_INDEX);
			float rightBlink = pad.GetNormAnalogButton(ioPad::R1_INDEX);
#if __PPU
			if (m_RageSummitDemo_UseSixaxis)
			{
				eyePosition[0] = pad.GetNormSensorAxis(ioPad::SENSOR_X) * 0.75f;
				eyePosition[1] = pad.GetNormSensorAxis(ioPad::SENSOR_Y) * 0.75f;
			}
			else
#endif // __PPU
			{
				m_RageSummitDemo_UseSixaxis = false;
				eyePosition[0] = pad.GetNormLeftX() * 0.75f;
				eyePosition[1] = pad.GetNormLeftY() * 0.5f;
			}
			eyePosition[2] = 0.0f;
			jawPosition[0] = pad.GetNormRightX();
			jawPosition[1] = pad.GetNormRightY();
			jawPosition[2] = 0.0f;

			m_FrameIn.SetVector3(kTrackFacialTranslation, 36436, jawPosition);
			m_FrameIn.SetVector3(kTrackFacialTranslation, 25902, eyePosition);
			m_FrameIn.SetVector3(kTrackFacialTranslation, 27604, browLeftCircle);
			m_FrameIn.SetVector3(kTrackFacialTranslation, 42334, browRightCircle);
			m_FrameIn.SetVector3(kTrackFacialTranslation, 38462, browLeftCircle);
			m_FrameIn.SetVector3(kTrackFacialTranslation, 58113, browRightCircle);
			m_FrameIn.SetFloat(kTrackFacialControl, 33887, leftBlink);
			m_FrameIn.SetFloat(kTrackFacialControl, 11155, rightBlink);
		}
		GetCamMgr().SetAcceptInput(!m_RageSummitDemo);
	}

	void CreateCreature(crCreature*& creature)
 	{
		creature = rage_new crCreature;
		creature->Init(m_FrameDataFactory, m_FrameAccelerator, &GetSkeleton(), &GetMoverMtx());	
	}

	virtual void UpdateFragment ()
	{
		crFragSampleManager::UpdateFragment();
	}

	void InitMover()
	{
		// suppress creation of a mover inst
	}

	const char* GetDefaultFile()
	{
		const char* defaultFile = DEFAULT_FILE;
		PARAM_file.Get(defaultFile);
		return defaultFile;
	}

#if PROFILE_MOTION_TREE
	void UpdateMotionTree()
	{
		sysTimer timer;

#define USE_SCHEDULER (1)
#if USE_SCHEDULER
		crMotionTreeSampleManager::UpdateMotionTree();
#else // USE_SCHEDULER
		GetMotionTree().Update(TIME.GetSeconds());
#endif // USE_SCHEDULER

		float time = timer.GetTime();

		static float totalTime = 0.f;
		totalTime += time;

		static float minTime = FLT_MAX;
		minTime = Min(time, minTime);

		static float maxTime = 0.f;
		maxTime = Max(time, maxTime);

		static int totalPasses = 0;
		totalPasses++;

		if(!(totalPasses%1000))
		{
			Printf("motion tree update time: average %f ms, min %f ms, max %f ms\n", 1000.f*totalTime/float(totalPasses), 1000.f*minTime, 1000.f*maxTime);
			totalTime = 0.f;
			minTime = FLT_MAX;
			maxTime = 0.f;
			totalPasses = 0;
		}
	}
#endif // PROFILE_MOTION_TREE

#if __BANK
	void AddWidgetsCreature()
	{
		bkBank &creatureBank=BANKMGR.CreateBank("Creature",50,500);
		creatureBank.AddToggle("RAGE Summit demo", &m_RageSummitDemo);
		creatureBank.AddToggle("RAGE Summit demo (use Sixaxis?)", &m_RageSummitDemo_UseSixaxis);
		creatureBank.AddToggle("Allow bones", &m_Filter.m_AllowBones);
		creatureBank.AddToggle("Allow blend shapes", &m_Filter.m_AllowBlendShapes);
		creatureBank.AddToggle("Allow animated normal maps", &m_Filter.m_AllowAnimatedNormalMaps);

		creatureBank.PushGroup("Materials");
		GetDrawable().GetShaderGroup().AddWidgets(creatureBank);
		creatureBank.PopGroup();

		creatureBank.AddButton("Reset Input", datCallback(MFA(crMotionTreeSampleCreature::ResetFrameInOutWidgets),this));
		creatureBank.AddButton("Dump Output", datCallback(MFA(crMotionTreeSampleCreature::DumpOut),this));

		creatureBank.PushGroup("Input frame");
		m_FrameInWidgets.Init(m_FrameIn, &GetSkeleton().GetSkeletonData());
		m_FrameInWidgets.AddWidgets(creatureBank);
		creatureBank.PopGroup();

		creatureBank.PushGroup("Output frame");
		m_FrameOutWidgets.Init(m_FrameOut, &GetSkeleton().GetSkeletonData());
		m_FrameOutWidgets.AddWidgets(creatureBank);
		creatureBank.PopGroup();

		ResetFrameInOutWidgets();

		if(m_Animation && m_AnimationObserver.IsAttached())
		{
			crmtNodeAnimation* nodeAnimation = static_cast<crmtNodeAnimation*>(m_AnimationObserver.GetNode());
			nodeAnimation->GetAnimPlayer().AddWidgets(creatureBank);
		}

		if(m_Pm && m_PmObserver.IsAttached())
		{
			creatureBank.PushGroup("Simple Parameterized Motion");

			crpmParameter minParameter(m_Parameter.GetNumDimensions());
			crpmParameter maxParameter(m_Parameter.GetNumDimensions());

			m_Pm->GetParameterBoundaries(minParameter, maxParameter);

			m_Parameter.AddWidgets(creatureBank, &minParameter, &maxParameter);

			creatureBank.PopGroup();
		}
	}

	void UpdateFrameInOutWidgets()
	{
		m_FrameInWidgets.UpdateFrame();

		m_FrameOutWidgets.ResetWidgets();
	}

	void ResetFrameInOutWidgets()
	{
		GetCreature().Identity(m_FrameIn);

		m_FrameInWidgets.ResetWidgets();
		if(m_Animation)
		{
			m_FrameIn.Invalidate();

			m_FrameInWidgets.ResetWidgets();
		}
	}

	void DumpOut()
	{
		m_FrameOut.Dump();
	}

	FrameEditorWidgets m_FrameInWidgets;
	FrameEditorWidgets m_FrameOutWidgets;
#endif // __BANK

	class CreatureFrameFilter : public crFrameFilter
	{
	public:

		CreatureFrameFilter()
			: m_AllowBones(true)
			, m_AllowBlendShapes(true)
			, m_AllowAnimatedNormalMaps(true)
		{
		}
		
		virtual ~CreatureFrameFilter() 
		{
		}

		virtual bool FilterDof(u8 track, u16, float&)
		{
			switch(track)
			{
			case kTrackBoneTranslation:
			case kTrackBoneRotation:
			case kTrackBoneScale:
				return m_AllowBones;

			case kTrackBlendShape:
				return m_AllowBlendShapes;

			case kTrackAnimatedNormalMaps:
				return m_AllowAnimatedNormalMaps;

			default:
				return true;
			}
		}

		virtual u32 GetSignature() const
		{
			u32 signature = u32(m_AllowBones) | (u32(m_AllowBlendShapes)<<1) | (u32(m_AllowAnimatedNormalMaps)<<2);
			crc32(signature, (const u8*)&sm_Hash, sizeof(u32));

			return signature;
		}

		static u32 sm_Hash;

		bool m_AllowBones;
		bool m_AllowBlendShapes;
		bool m_AllowAnimatedNormalMaps;
	};

private:
	crFrame m_FrameIn;
	crFrame m_FrameOut;
	crFrameData m_FrameData;
	crAnimation* m_Animation;
	crExpressions* m_Expressions;
	crpmParameterizedMotion* m_Pm;
	crpmParameter m_Parameter;
	CreatureFrameFilter m_Filter;
	crmtObserver m_AnimationObserver;
	crmtObserver m_PmObserver;
	bool m_RageSummitDemo;
	bool m_RageSummitDemo_UseSixaxis;
};

u32 crMotionTreeSampleCreature::CreatureFrameFilter::sm_Hash = atHash_const_char("CreatureFrameFilter");


} // namespace ragesamples

// main application
int Main()
{
	ragesamples::crMotionTreeSampleCreature sampleMotionTreeCreature;
	sampleMotionTreeCreature.Init();

	sampleMotionTreeCreature.UpdateLoop();

	sampleMotionTreeCreature.Shutdown();

	return 0;
}
