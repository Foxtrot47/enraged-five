set ARCHIVE=sample_aicreature
set FILES=luxo
set TESTERS=sample_falling sample_luxo
set SAMPLE_LIBS=sample_motiontree sample_rmcore sample_cranimation sample_physics sample_crfragment %RAGE_SAMPLE_LIBS%
set LIBS=%SAMPLE_LIBS% %RAGE_CORE_LIBS% %RAGE_GFX_LIBS%
set LIBS=%LIBS% %RAGE_CR_LIBS% %RAGE_SUITE_CR_LIBS% curve
set LIBS=%LIBS% %RAGE_PH_LIBS% fragment breakableglass phglass cloth grrope event pheffects spatialdata
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples %RAGE_DIR%\suite\src
set XDEFINE=USING_RAGE
