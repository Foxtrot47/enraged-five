//
// sample_aicreature/luxo.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "luxo.h"

#include "pharticulated/articulatedcollider.h"
#include "phbound/boundcapsule.h"
#include "phbound/boundcomposite.h"
#include "physics/sleep.h"
#include "system/timemgr.h"


namespace ragesamples {

////////////////////////////////////////////////////////////////


phLuxoObject::phLuxoObject ()
{
	m_MinTargetHeight = 0.0f;
	m_MaxTargetHeight = 4.0f;
	m_MinTargetDistance = 0.4f;
	m_MaxTargetDistance = 2.0f;
	m_NumViewStates = 0;
	m_NumVertStates = 0;
	m_NumHorzStates = 0;
	m_NumBendStates = 0;
	m_NumTurnStates = 0;
	m_NumTargetBendStates = 0;
	m_NumTargetTurnStates = 0;
}


phLuxoObject::~phLuxoObject()
{
}


void phLuxoObject::InitFromBound (phDemoWorld& demoWorld, const char* name, phJointCore::JointType* jointTypeList, const Vector3& position, const Vector3& rotation, bool allToRoot)
{
	phArticulatedObject::InitFromBound(demoWorld,name,jointTypeList,position,rotation,allToRoot);
	Assert(m_PhysInst->GetArchetype()->GetBound()->GetType()==phBound::COMPOSITE);
	const phBoundComposite& luxoBound = *static_cast<const phBoundComposite*>(m_PhysInst->GetArchetype()->GetBound());
	Assert(luxoBound.GetNumBounds()==4);
	const phBound& baseBound = *luxoBound.GetBound(0);
	m_BaseThickness = baseBound.GetBoundingBoxMax().GetYf()-baseBound.GetBoundingBoxMin().GetYf();
	Assert(luxoBound.GetBound(1)->GetType()==phBound::CAPSULE);
	const phBoundCapsule& humerusBound = *static_cast<const phBoundCapsule*>(luxoBound.GetBound(1));
	m_HumerusLength = humerusBound.GetLength();
	Assert(luxoBound.GetBound(2)->GetType()==phBound::CAPSULE);
	const phBoundCapsule& ulnaBound = *static_cast<const phBoundCapsule*>(luxoBound.GetBound(2));
	m_UlnaLength = ulnaBound.GetLength();
}


void phLuxoObject::SetMuscles (const Vector3& baseTargetAngle, float elbowTargetAngle, const Vector3& neckTargetAngle)
{
	// Get a reference to the body.
	phArticulatedBody& body = *m_Collider->GetBody();

	// Set the base joint muscle.
	phJoint3Dof& baseJoint = body.GetJoint3Dof(0);
	float muscleAngleStrength = 300.0f;
	float muscleSpeedStrength = 30.0f;
	baseJoint.GetCore().SetDriveState(phJointCore::DRIVE_STATE_ANGLE_AND_SPEED);
	baseJoint.SetMuscleAngleStrength(muscleAngleStrength);
	baseJoint.SetMuscleSpeedStrength(muscleSpeedStrength);
	baseJoint.SetMuscleTargetAngle(baseTargetAngle);
	baseJoint.SetMuscleTargetSpeed(ORIGIN);

	// Set the elbow joint muscle.
	phJoint1Dof& elbowJoint = body.GetJoint1Dof(1);
	elbowJoint.GetCore().SetDriveState(phJointCore::DRIVE_STATE_ANGLE_AND_SPEED);
	elbowJoint.SetMuscleAngleStrength(muscleAngleStrength);
	elbowJoint.SetMuscleSpeedStrength(muscleSpeedStrength);
	elbowJoint.SetMuscleTargetAngle(elbowTargetAngle-0.05f*PI);
	elbowJoint.SetMuscleTargetSpeed(0.0f);

	// Set the neck joint muscle.
	phJoint3Dof& neckJoint = body.GetJoint3Dof(2);
	Vector3 neckTarget(neckTargetAngle);
	neckTarget.x += 0.1f*PI;
	neckJoint.GetCore().SetDriveState(phJointCore::DRIVE_STATE_ANGLE_AND_SPEED);
	neckJoint.SetMuscleAngleStrength(muscleAngleStrength);
	neckJoint.SetMuscleSpeedStrength(muscleSpeedStrength);
	neckJoint.SetMuscleTargetAngle(neckTarget);
	neckJoint.SetMuscleTargetSpeed(ORIGIN);
}


void phLuxoObject::LearnViews ()
{
	// Loop over the entire range of target heights, distances and angles.
	for (int targetHeight=0; targetHeight<QUANTIZATION_STEPS; targetHeight++)
	{
		float height = UnquantizeHeight(targetHeight);
		for (int targetDistance=0; targetDistance<QUANTIZATION_STEPS; targetDistance++)
		{
			float distance = UnquantizeDistance(targetDistance);
			for (int targetAngle=0; targetAngle<QUANTIZATION_STEPS; targetAngle++)
			{
				float angle = UnquantizeAngle(targetAngle);

				// Try to find a set of bend and turn angles to view this target state.
				int baseBend,elbowBend,neckBend,baseTurn,neckTurn;
				if (ComputeViewState(height,distance,angle,&baseBend,&elbowBend,&neckBend,&baseTurn,&neckTurn))
				{
					// A successful pose was found to view this target position. Add the new bend state.
					int bendStateIndex = AddNewBendState(baseBend,elbowBend,neckBend);

					// Add the new turn state.
					int turnStateIndex = AddNewTurnState(baseTurn,neckTurn);

					// Add the new target bend state.
					int targetBendStateIndex = AddNewTargetBendState(targetHeight,targetDistance);

					// Add the new target turn state.
					int targetTurnStateIndex = AddNewTargetTurnState(targetAngle);

					// Add the new vertical view state.
					int vertStateIndex = AddNewVerticalViewState(bendStateIndex,targetBendStateIndex);

					// Add the new horizontal view state.
					int horzStateIndex = AddNewHorizontalViewState(turnStateIndex,targetTurnStateIndex);

					// Add the new view state.
					AddNewViewState(vertStateIndex,horzStateIndex);
				}
			}
		}
	}
}


void phLuxoObject::LearnJumpUp ()
{
	// Get references to the three joints.
	phArticulatedBody& body = *m_Collider->GetBody();
	phJoint3Dof& baseJoint = body.GetJoint3Dof(0);
	phJoint1Dof& elbowJoint = body.GetJoint1Dof(1);
	phJoint3Dof& neckJoint = body.GetJoint3Dof(2);

	m_BaseFrequency = PI;
	ScalarV timeStep = ScalarVFromF32(TIME.GetSeconds());
	m_RestTorque[0] = baseJoint.ComputeMuscleTorque(timeStep).GetZf();
	m_RestTorque[1] = elbowJoint.ComputeMuscleTorque(timeStep).Getf();
	m_RestTorque[2] = neckJoint.ComputeMuscleTorque(timeStep).GetZf();
}


bool phLuxoObject::ComputeViewState (float targetHeight, float targetDistance, float targetAngle, int* baseBend, int* elbowBend, int* neckBend, int* baseTurn, int* neckTurn)
{
	// Set the base and elbow bend to 45 degrees.
	float baseBendAngle = 0.0f*PI;
	(*baseBend) = QuantizeAngle(baseBendAngle);

	// Set the elbow bend angle to twice the base bend angle.
	float elbowBendAngle = 0.0f*PI;
	(*elbowBend) = QuantizeAngle(elbowBendAngle);

	// Compute the neck bend angle to view the target.
	float heightAtNeck = m_BaseThickness + m_HumerusLength*sinf(baseBendAngle+0.25f*3.14159f) + m_UlnaLength*sinf(elbowBendAngle-baseBendAngle+0.25f*3.14159f);
	float heightDifference = targetHeight - heightAtNeck;
	float invToTarget = invsqrtf(square(heightDifference)+square(targetDistance));
	float sine = heightDifference*invToTarget;
	float cosine = targetDistance*invToTarget;
	float neckBendAngle = ArcTangent(sine,cosine);
	(*neckBend) = QuantizeAngle(neckBendAngle);

	// Split the turn angle in half between the base and the neck.
	int halfTurnQuantized = QuantizeAngle(0.5f*targetAngle);
	(*baseTurn) = halfTurnQuantized;
	(*neckTurn) = halfTurnQuantized;

	// Return true to indicate that a successful view state was found.
	return true;
}


int phLuxoObject::AddNewBendState (int baseBend, int elbowBend, int neckBend)
{
	// Search through the existing bend states to see if the given bend state is already known.
	int bendStateIndex;
	for (bendStateIndex=0; bendStateIndex<m_NumBendStates; bendStateIndex++)
	{
		if (m_BendState[bendStateIndex][0]==baseBend && m_BendState[bendStateIndex][1]==elbowBend && m_BendState[bendStateIndex][2]==neckBend)
		{
			// The given state was already found, so return its index.
			return bendStateIndex;
		}
	}

	// The given state was not found, so add it.
	Assert(bendStateIndex==m_NumBendStates && m_NumBendStates<MAX_BEND_STATES);
	m_BendState[bendStateIndex][0] = (u8)baseBend;
	m_BendState[bendStateIndex][1] = (u8)elbowBend;
	m_BendState[bendStateIndex][2] = (u8)neckBend;

	// Increment the number of bend states.
	m_NumBendStates++;

	// Return the index number of the new state.
	return bendStateIndex;
}


int phLuxoObject::AddNewTurnState (int baseTurn, int neckTurn)
{
	// Search through the existing turn states to see if the given turn state is already known.
	int turnStateIndex;
	for (turnStateIndex=0; turnStateIndex<m_NumTurnStates; turnStateIndex++)
	{
		if (m_TurnState[turnStateIndex][0]==baseTurn && m_TurnState[turnStateIndex][1]==neckTurn)
		{
			// The given state was already found, so return its index.
			return turnStateIndex;
		}
	}

	// The given state was not found, so add it.
	Assert(turnStateIndex==m_NumTurnStates && m_NumTurnStates<MAX_TURN_STATES);
	m_TurnState[turnStateIndex][0] = (u8)baseTurn;
	m_TurnState[turnStateIndex][1] = (u8)neckTurn;

	// Increment the number of turn states.
	m_NumTurnStates++;

	// Return the index number of the new state.
	return turnStateIndex;
}


int phLuxoObject::AddNewTargetBendState (int targetHeight, int targetDistance)
{
	// Search through the existing target bend states to see if the given target bend state is already known.
	int targetBendStateIndex;
	for (targetBendStateIndex=0; targetBendStateIndex<m_NumTargetBendStates; targetBendStateIndex++)
	{
		if (m_TargetBendState[targetBendStateIndex][0]==targetHeight && m_TargetBendState[targetBendStateIndex][1]==targetDistance)
		{
			// The given state was already found, so return its index.
			return targetBendStateIndex;
		}
	}

	// The given state was not found, so add it.
	Assert(targetBendStateIndex==m_NumTargetBendStates && m_NumTargetBendStates<MAX_BEND_STATES);
	m_TargetBendState[targetBendStateIndex][0] = (u8)targetHeight;
	m_TargetBendState[targetBendStateIndex][1] = (u8)targetDistance;
//	m_TargetBendState[targetBendStateIndex][2] = (u8)tiltAngle;

	// Increment the number of target bend states.
	m_NumTargetBendStates++;

	// Return the index number of the new state.
	return targetBendStateIndex;
}


int phLuxoObject::AddNewTargetTurnState (int targetAngle)
{
	// Search through the existing target turn states to see if the given target turn state is already known.
	int targetTurnStateIndex;
	for (targetTurnStateIndex=0; targetTurnStateIndex<m_NumTargetTurnStates; targetTurnStateIndex++)
	{
		if (m_TargetTurnState[targetTurnStateIndex][0]==targetAngle)
		{
			// The given state was already found, so return its index.
			return targetTurnStateIndex;
		}
	}

	// The given state was not found, so add it.
	Assert(targetTurnStateIndex==m_NumTargetTurnStates && m_NumTargetTurnStates<MAX_TURN_STATES);
	m_TargetTurnState[targetTurnStateIndex][0] = (u8)targetAngle;
//	m_TargetTurnState[targetTurnStateIndex][1] = (u8)targetTurnDirection;

	// Increment the number of target bend states.
	m_NumTargetTurnStates++;

	// Return the index number of the new state.
	return targetTurnStateIndex;
}


int phLuxoObject::AddNewVerticalViewState (int bendStateIndex, int targetBendStateIndex)
{
	// Search through the existing vertical view states to see if the given vertical view state is already known.
	int vertStateIndex;
	for (vertStateIndex=0; vertStateIndex<m_NumVertStates; vertStateIndex++)
	{
		if (m_VerticalViewState[vertStateIndex][0]==bendStateIndex && m_VerticalViewState[vertStateIndex][1]==targetBendStateIndex)
		{
			// The given state was already found, so return its index.
			return vertStateIndex;
		}
	}

	// The given state was not found, so add it.
	Assert(vertStateIndex==m_NumVertStates && m_NumVertStates<MAX_VERT_STATES);
	m_VerticalViewState[vertStateIndex][0] = (u8)bendStateIndex;
	m_VerticalViewState[vertStateIndex][1] = (u8)targetBendStateIndex;

	// Increment the number of vertical view states.
	m_NumVertStates++;

	// Return the index number of the new state.
	return vertStateIndex;
}


int phLuxoObject::AddNewHorizontalViewState (int turnStateIndex, int targetTurnStateIndex)
{
	// Search through the existing horizontal view states to see if the given horizontal view state is already known.
	int horzStateIndex;
	for (horzStateIndex=0; horzStateIndex<m_NumHorzStates; horzStateIndex++)
	{
		if (m_HorizontalViewState[horzStateIndex][0]==turnStateIndex && m_HorizontalViewState[horzStateIndex][1]==targetTurnStateIndex)
		{
			// The given state was already found, so return its index.
			return horzStateIndex;
		}
	}

	// The given state was not found, so add it.
	Assert(horzStateIndex==m_NumHorzStates && m_NumHorzStates<MAX_HORZ_STATES);
	m_HorizontalViewState[horzStateIndex][0] = (u8)turnStateIndex;
	m_HorizontalViewState[horzStateIndex][1] = (u8)targetTurnStateIndex;

	// Increment the number of horizontal view states.
	m_NumHorzStates++;

	// Return the index number of the new state.
	return horzStateIndex;
}


void phLuxoObject::AddNewViewState (int vertStateIndex, int horzStateIndex)
{
	// Search through the existing view states to see if the given view state is already known.
	int viewStateIndex;
	for (viewStateIndex=0; viewStateIndex<m_NumViewStates; viewStateIndex++)
	{
		if (m_ViewState[viewStateIndex][0]==vertStateIndex && m_ViewState[viewStateIndex][1]==horzStateIndex)
		{
			// The given state was already found, so don't add it.
			return;
		}
	}

	// The given state was not found, so add it.
	Assert(viewStateIndex==m_NumViewStates && m_NumViewStates<MAX_VIEW_STATES);
	m_ViewState[viewStateIndex][0] = (u8)vertStateIndex;
	m_ViewState[viewStateIndex][1] = (u8)horzStateIndex;

	// Increment the number of horizontal view states.
	m_NumViewStates++;
}


bool phLuxoObject::FindStateToViewTarget (float targetHeight, float targetDistance, float targetAngle, int* bendStateIndex, int* turnStateIndex)
{
	// Find the target bend state from the given target height and distance.
	int targetHeightQuantized = QuantizeHeight(targetHeight);
	int targetDistanceQuantized = QuantizeDistance(targetDistance);
	int targetBendStateIndex;
	for (targetBendStateIndex=0; targetBendStateIndex<m_NumTargetBendStates; targetBendStateIndex++)
	{
		if (m_TargetBendState[targetBendStateIndex][0]==targetHeightQuantized && m_TargetBendState[targetBendStateIndex][1]==targetDistanceQuantized)
		{
			break;
		}
	}

	// Find the target turn state from the given target angle.
	int targetAngleQuantized = QuantizeAngle(targetAngle);
	int targetTurnStateIndex;
	for (targetTurnStateIndex=0; targetTurnStateIndex<m_NumTargetTurnStates; targetTurnStateIndex++)
	{
		if (m_TargetTurnState[targetTurnStateIndex][0]==targetAngleQuantized)
		{
			break;
		}
	}

	// Find the vertical view state from the target bend state.
	int vertStateIndex;
	for (vertStateIndex=0; vertStateIndex<m_NumVertStates; vertStateIndex++)
	{
		if (m_VerticalViewState[vertStateIndex][1]==targetBendStateIndex)
		{
			break;
		}
	}

	// Find the horizontal view state from the target turn state.
	int horzStateIndex;
	for (horzStateIndex=0; horzStateIndex<m_NumHorzStates; horzStateIndex++)
	{
		if (m_HorizontalViewState[horzStateIndex][1]==targetTurnStateIndex)
		{
			break;
		}
	}

	// Find the view state from the vertical and horizontal view states.
	int viewStateIndex;
	for (viewStateIndex=0; viewStateIndex<m_NumViewStates; viewStateIndex++)
	{
		if (m_ViewState[viewStateIndex][0]==vertStateIndex && m_ViewState[viewStateIndex][1]==horzStateIndex)
		{
			break;
		}
	}

	// See if a view state was found.
	if (viewStateIndex<m_NumViewStates)
	{
		// A view state was found, so use it to set the bend and turn states.
		(*bendStateIndex) = m_VerticalViewState[vertStateIndex][0];
		(*turnStateIndex) = m_HorizontalViewState[horzStateIndex][0];

		// Return true to indicate that a view state was found for the given target.
		return true;
	}

	// Return false to indicate that no view state was found for the given target.
	return false;
}


void phLuxoObject::ViewTarget (const Vector3& worldPosition)
{
	const phBoundComposite& luxoBound = *static_cast<const phBoundComposite*>(m_PhysInst->GetArchetype()->GetBound());
	const phBound& baseBound = *luxoBound.GetBound(0);
	Vector3 baseCenter = RCC_VECTOR3(m_PhysInst->GetPosition());
	baseCenter.y += baseBound.GetBoundingBoxMin().GetYf();
	float targetHeight = worldPosition.y - baseCenter.y;
	Vector3 toTarget = worldPosition-baseCenter;
	toTarget.y = 0.0f;
	float targetDistance = toTarget.FlatMag();
	float cosine = -toTarget.Dot(VEC3V_TO_VECTOR3(m_PhysInst->GetMatrix().GetCol0()))/targetDistance;
	float sine = toTarget.Dot(VEC3V_TO_VECTOR3(m_PhysInst->GetMatrix().GetCol2()))/targetDistance;
	float targetAngle = ArcTangent(sine,cosine);

	// Get references to the three joints.
	phArticulatedBody& body = *m_Collider->GetBody();
	phJoint3Dof& baseJoint = body.GetJoint3Dof(0);
	phJoint1Dof& elbowJoint = body.GetJoint1Dof(1);
	phJoint3Dof& neckJoint = body.GetJoint3Dof(2);

	int bendStateIndex,turnStateIndex;
	if (FindStateToViewTarget(targetHeight,targetDistance,targetAngle,&bendStateIndex,&turnStateIndex))
	{
		// Get the bend and turn states.
		u8* bendState = &m_BendState[bendStateIndex][0];
		u8* turnState = &m_TurnState[turnStateIndex][0];

		// Set the base joint muscle.
		Vector3 baseAngle;
		baseAngle.x = UnquantizeAngle(bendState[0]);
		baseAngle.y = 0.0f;
		baseAngle.z = UnquantizeAngle(turnState[0]);
		baseJoint.SetMuscleTargetAngle(baseAngle);

		// Set the elbow joint muscle.
		float elbowAngle = UnquantizeAngle(bendState[1])-0.05f*PI;
		elbowJoint.SetMuscleTargetAngle(elbowAngle);

		// Set the neck joint muscle.
		Vector3 neckAngle;
		neckAngle.x = UnquantizeAngle(bendState[2])+0.1f*PI;
		neckAngle.y = UnquantizeAngle(turnState[1]);
		neckAngle.z = 0.0f;
		neckJoint.SetMuscleTargetAngle(neckAngle);
	}
	else
	{
		// Set the relaxed standing pose target angles.
		Vector3 muscleTargetAngle(0.0f*PI,0.0f,0.0f);
		baseJoint.SetMuscleTargetAngle(muscleTargetAngle);
		elbowJoint.SetMuscleTargetAngle(0.0f*PI);
		muscleTargetAngle.Set(0.0f*PI,0.0f,0.0f);
		neckJoint.SetMuscleTargetAngle(muscleTargetAngle);
	}
}


void phLuxoObject::JumpUp ()
{
	m_ActionStartTime = TIME.GetSeconds();
	Vector3 baseTarget(0.25f*PI,0.0f,0.0f);
	float elbowTarget = -0.25f*PI;
	Vector3 neckTarget(0.25f*PI,0.0f,0.0f);
	SetMuscles(baseTarget,elbowTarget,neckTarget);
}


phLuxoObject* phLuxoObject::CreateLuxoObject (phDemoWorld& demoWorld, const Vector3& position, const Vector3& rotation)
{
	const bool allToRoot = false;
	phLuxoObject* luxoObject = new phLuxoObject;
	phJointCore::JointType jointTypeList[] = {phJointCore::JNT_3DOF, phJointCore::JNT_1DOF, phJointCore::JNT_3DOF};
	luxoObject->InitFromBound(demoWorld,"luxo",jointTypeList,position,rotation,allToRoot);
	luxoObject->SetMuscles();
	phSleep* sleep = luxoObject->GetCollider().GetSleep();
	sleep->SetVelTolerance2(0.0f);
	sleep->SetAngVelTolerance2(0.0f);
	return luxoObject;
}


// Training procedure:
//	Cycle through target height, horizontal distance and tilt angle, and for each combination make a m_TargetBendState (set of 3 quantized numbers).
//	Cycle through target horizontal angle and turn direction, and for each combination make a m_TargetTurnState (set of 2 quantized numbers).
//	For each m_TargetBendState and m_TargetTurnState combination, find a m_BendState and a m_TurnState that results in a successful view. If there
//	are none, remove the m_TargetBendState and m_TargetTurnState from the lists. If there are multiple, choose one.
//	When there is a combination m_TargetBendState, m_TargetTurnState, m_BendState and m_TurnState that results in a successful view, record the
//	state index numbers in m_VerticalViewState and m_HorizontalViewState. Record those index numbers in m_ViewState.

// Motion procedure:
//	Find the target height, horizontal distance, tilt angle, horizontal angle and turn direction, and quantize them all. Search the m_TargetBendState and
//	m_TargetTurnState lists for matching sets of quantized numbers. If no set is found, quit. When a set is found, search m_VerticalViewState and
//	m_HorizontalViewState for the index numbers of the sets. If none are found, quit. When they are found, search m_ViewState for the index numbers where they
//	are found. If not found, quit. When an m_ViewState is found, get the m_BendState and m_TurnState from the m_VerticalViewState and the m_HorizontalViewState.
//	From the m_BendState and m_TurnState, compute the bend angles for the base, elbow and head, and the turn angles for the base and head.


} // namespace ragesamples

