// 
// sample_veh/sample_car.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "luxo.h"

#include "bank/bkmgr.h"
#include "sample_physics/demoobject.h"
#include "sample_physics/sample_physics.h"
#include "system/main.h"
#include "system/param.h"

PARAM(file, "unused");

namespace ragesamples {

using namespace rage;


class luxoSampleManager : public physicsSampleManager
{
public:

	virtual void InitClient()
	{
		physicsSampleManager::InitClient();

		// common variables
		Vector3 position,rotation;

		////////////////
		// 0: Luxo world
		phDemoWorld& luxoWorld = MakeNewDemoWorld("Luxo");
		luxoWorld.SetFramerate(30.0f);
		position.Set(0.0f,1.0f,0.0f);
		rotation.Set(0.0f,0.0f,0.0f);
		m_Luxo = phLuxoObject::CreateLuxoObject(luxoWorld,position,rotation);
		Assert(m_Luxo);
		m_Luxo->LearnViews();
		m_ViewTarget.Identity3x3();
		m_ViewTarget.d.Set(-2.0f,1.0f,0.0f);
		luxoWorld.AddTranslationGizmo(m_ViewTarget);
		m_InitialViewTarget.Set(m_ViewTarget);
	}

	virtual void InitCamera ()
	{
		physicsSampleManager::InitCamera();
	}

	virtual void ShutdownClient ()
	{
		physicsSampleManager::ShutdownClient();
	}

	virtual void Reset ()
	{
		m_ViewTarget.Set(m_InitialViewTarget);
		m_Luxo->SetMuscles();
		physicsSampleManager::Reset();
	}

	virtual void Update ()
	{
		physicsSampleManager::Update();
	}

	void ViewTarget ()
	{
		m_Luxo->ViewTarget(m_ViewTarget.d);
	}

	void JumpUp ()
	{
		m_Luxo->JumpUp();
	}

	void LearnJumpUp ()
	{
		m_Luxo->LearnJumpUp();
	}

#if __BANK
	virtual void AddWidgetsClient ()
	{
		bkBank& bank = BANKMGR.CreateBank("Sample Luxo");
		bank.AddButton("View Target",datCallback(MFA(luxoSampleManager::ViewTarget),this));
		bank.AddButton("Jump Up",datCallback(MFA(luxoSampleManager::JumpUp),this));
		bank.AddButton("Learn Jump Up",datCallback(MFA(luxoSampleManager::LearnJumpUp),this));
		bank.Show();

		physicsSampleManager::AddWidgetsClient();
	}
#endif


private:
	phLuxoObject* m_Luxo;
	Matrix34 m_ViewTarget,m_InitialViewTarget;
};

} // namespace ragesamples

// main application
int Main()
{
	{
		ragesamples::luxoSampleManager sampleLuxo;
		sampleLuxo.Init();

		sampleLuxo.UpdateLoop();

		sampleLuxo.Shutdown();
	}

	return 0;
}
