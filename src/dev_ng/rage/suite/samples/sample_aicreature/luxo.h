// 
// sample_aicreature/luxo.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_AICREATURE_LUXO_H
#define SAMPLE_AICREATURE_LUXO_H

#include "sample_physics/demoobject.h"

#define QUANTIZATION_STEPS	16

#define QUANT_STEPS			(float)QUANTIZATION_STEPS
#define INV_QUANT_STEPS		1.0f/QUANT_STEPS

#define MAX_BEND_STATES		QUANTIZATION_STEPS*QUANTIZATION_STEPS
#define MAX_TURN_STATES		QUANTIZATION_STEPS

#define MAX_VERT_STATES		QUANTIZATION_STEPS*QUANTIZATION_STEPS
#define MAX_HORZ_STATES		QUANTIZATION_STEPS

#define MAX_VIEW_STATES	4096


#define	LUXO_NUM_WAVES	8

namespace ragesamples {

using namespace rage;


////////////////////////////////////////////////////////////////
// phLuxoObject

class phLuxoObject : public phArticulatedObject
{
public:
	phLuxoObject();
	virtual ~phLuxoObject();

	virtual void InitFromBound (phDemoWorld& demoWorld, const char* name, phJointCore::JointType* jointTypeList, const Vector3& position, const Vector3& rotation=ORIGIN, bool allToRoot=true);

	// PURPOSE: Set the joint muscle torques required for relaxed standing.
	void SetMuscles (const Vector3& baseTargetAngle=ORIGIN, float elbowTargetAngle=0.0f, const Vector3& neckTargetAngle=ORIGIN);

	// PURPOSE: Create a memory set of joint angles to view a set of target positions and orientations.
	void LearnViews ();

	void LearnJumpUp ();

	// PURPOSE: Compute the set of joint angles to view the given position.
	// PARAMS:
	//	targetHeight - the altitude of the target position
	//	targetDistance - the horizontal distance of the target position
	// RETURN:	true if a successful view state was found, false if not
	bool ComputeViewState (float targetHeight, float targetDistance, float targetAngle, int* baseBend, int* elbowBend, int* neckBend, int* baseTurn, int* neckTurn);

	// PURPOSE: Add a new bend state with the given quantized bend angles, if they are not already in an existing state.
	// PARAMS:
	//	baseBend - the quantized bend angle of the base joint
	//	elbowBend - the quantized bend angle of the elbow joint
	//	neckBend - the quantized bend angle of the neck joint.
	// RETURN:	the index number of the bend state
	int AddNewBendState (int baseBend, int elbowBend, int neckBend);

	// PURPOSE: Add a new turn state with the given quantized turn angles, if they are not already in an existing state.
	// PARAMS:
	//	baseTurn - the quantized turn angle of the base joint
	//	neckTurn - the quantized turn angle of the neck joint.
	// RETURN:	the index number of the turn state
	int AddNewTurnState (int baseTurn, int neckTurn);

	int AddNewTargetBendState (int targetHeight, int targetDistance);

	int AddNewTargetTurnState (int targetAngle);

	int AddNewVerticalViewState (int bendStateIndex, int targetBendStateIndex);

	int AddNewHorizontalViewState (int turnStateIndex, int targetTurnStateIndex);

	void AddNewViewState (int vertStateIndex, int horzStateIndex);

	bool FindStateToViewTarget (float targetHeight, float targetDistance, float targetAngle, int* bendStateIndex, int* turnStateIndex);

	void ViewTarget (const Vector3& worldPosition);

	void JumpUp ();

	static phLuxoObject* CreateLuxoObject (phDemoWorld& demoWorld, const Vector3& position=ORIGIN, const Vector3& rotation=ORIGIN);
	
	static int QuantizeAngle (float angle);

	static float UnquantizeAngle (int quantizedAngle);

	int QuantizeHeight (float height);

	float UnquantizeHeight (int quantizedHeight);

	int QuantizeDistance (float distance);

	float UnquantizeDistance (int quantizedDistance);

protected:
	// PURPOSE: list of combined vertical and horizontal view states
	u16 m_ViewState[MAX_VIEW_STATES][2];
	int m_NumViewStates;

	// PURPOSE: list of combined bend states and target bend states
	u8 m_VerticalViewState[MAX_VERT_STATES][2];
	int m_NumVertStates;

	// PURPOSE: list of combined turn states and target turn states
	u8 m_HorizontalViewState[MAX_HORZ_STATES][2];
	int m_NumHorzStates;

	// PURPOSE: list of bend states, each with 3 quantized bend angles for the base, elbow and head
	u8 m_BendState[MAX_BEND_STATES][3];
	int m_NumBendStates;

	// PURPOSE: list of turn states, each with 2 quantized turn angles for the base and head
	u8 m_TurnState[MAX_TURN_STATES][2];
	int m_NumTurnStates;

	// PURPOSE: list of target bend states, each with quantized height, horizontal distance and tilt angle
	u8 m_TargetBendState[MAX_BEND_STATES][3];
	int m_NumTargetBendStates;

	// PURPOSE: list of target turn states, each with quantized horizontal angle and horizontal turn direction
	u8 m_TargetTurnState[MAX_TURN_STATES][2];
	int m_NumTargetTurnStates;

	float m_MinTargetHeight,m_MaxTargetHeight,m_MinTargetDistance,m_MaxTargetDistance;

	float m_BaseThickness,m_HumerusLength,m_UlnaLength;

	float m_ActionStartTime;


	// Jumping parameters:
	float m_RestTorque[3];
	float m_Amplitude[3][LUXO_NUM_WAVES];
	float m_BaseFrequency;
};

inline int phLuxoObject::QuantizeAngle (float angle)
{
	angle = Clamp(angle,-0.5f*PI,0.5f*PI);
	angle += 0.5f*PI;
	return (int)(QUANT_STEPS*angle/PI);
}

inline float phLuxoObject::UnquantizeAngle (int quantizedAngle)
{
	return ((float)quantizedAngle*INV_QUANT_STEPS-0.5f)*PI;
}

inline int phLuxoObject::QuantizeHeight (float height)
{
	height = Clamp(height,m_MinTargetHeight,m_MaxTargetHeight);
	return (int)(QUANT_STEPS*(height-m_MinTargetHeight)/(m_MaxTargetHeight-m_MinTargetHeight)+0.5f);
}

inline float phLuxoObject::UnquantizeHeight (int quantizedHeight)
{
	return (float)quantizedHeight*INV_QUANT_STEPS*(m_MaxTargetHeight-m_MinTargetHeight)+m_MinTargetHeight;
}

inline int phLuxoObject::QuantizeDistance (float distance)
{
	distance = Clamp(distance,m_MinTargetDistance,m_MaxTargetDistance);
	return  (int)(QUANT_STEPS*(distance-m_MinTargetDistance)/(m_MaxTargetDistance-m_MinTargetDistance)+0.5f);
}

inline float phLuxoObject::UnquantizeDistance (int quantizedDistance)
{
	return (float)quantizedDistance*INV_QUANT_STEPS*(m_MaxTargetDistance-m_MinTargetDistance)+m_MinTargetDistance;
}

} // namespace ragesamples

#endif
