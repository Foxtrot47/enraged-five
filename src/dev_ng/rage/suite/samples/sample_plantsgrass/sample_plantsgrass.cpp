//
// sample_plantsgrass/sample_plantsgrass.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "sample_plantsgrass.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "file/asset.h"
#include "grcore/channel.h"
#include "grmodel/setup.h"
#include "grmodel/shader.h"
#include "grmodel/shaderfx.h"
#include "grmodel/shadergroup.h"
#include "system/param.h"

#include <stdio.h>

using namespace rage;

//PARAM(file,"[sample_plantsgrass] File to load (default is \"entity.type\")");
PARAM(shaderlibs,"[sample_plantsgrass] Set shader library path (default is \"tune/shaders/lib\")");
PARAM(shaderdb,"[sample_plantsgrass] Set shader database path (default is \"tune/shaders/db\")");
PARAM(shaderpath,"[sample_plantsgrass] Set shader root path (tune/shaders/lib and tune/shaders/db are appended)");
PARAM(autotexdict, "[sample_plantsgrass] Automatically create a texture dictionary for textures loaded without one");

PARAM(gta,"[sample_plantsgrass] Use GTA path setup");
PARAM(mc4,"[sample_plantsgrass] Use MC4 path setup");
PARAM(rdr2,"[sample_plantsgrass] Use RDR2 path setup");
PARAM(mp3,"[sample_plantsgrass] Use MP3 path setup");

// NOTE: This is XPARAM'd, so that each sample can provide its own documentation as to what this is for
XPARAM(file);

namespace ragesamples {
XPARAM(zup);
}
namespace asset {
XPARAM(path);
}

namespace ragesamples {

vegSampleManager::vegSampleManager() : grcSampleManager(), m_NumFiles(0), m_MinFiles(1), m_MaxFiles(1)
{
	using namespace asset;

	if (PARAM_gta.Get()) {
		PARAM_zup.Set("");
		PARAM_path.Set("x:/gta/build");
		PARAM_shaderlibs.Set("x:/gta/build/common/shaders");
		PARAM_shaderdb.Set("x:/gta/build/common/shaders/db");
	}
	else if (PARAM_mc4.Get()) {
		PARAM_path.Set("t:/mc4/assets");
		// MC4 has two shader paths, not sure what to do here...
	}
	else if (PARAM_rdr2.Get()) {
		// Are these correct?
		PARAM_path.Set("t:/rdr2/assets");
		PARAM_shaderdb.Set("t:/rdr2/tune/shaders/db");
		PARAM_shaderlibs.Set("t:/rdr2/tune/shaders/lib");
	}
	else if (PARAM_mp3.Get()) {
		// No clue, fill this in pls.
	}

	for (int i = 0; i < m_MinFiles; ++i)
	{
		m_DefaultFileNames[i] = "entity.type";
	}
	for (int i = m_MinFiles; i < m_MaxFiles; ++i)
	{
		m_DefaultFileNames[i] = "unknown.type";
	}
}

grcSetup& vegSampleManager::AllocateSetup()
{
	return *(rage_new grmSetup());
}

void vegSampleManager::CreateGfxFactories()
{
	const char* libPathPtr;
	if (PARAM_shaderlibs.Get(libPathPtr))
		grcEffect::SetDefaultPath(libPathPtr);

	grcSampleManager::CreateGfxFactories();

	// We always want auto tex dict's now because the shader system
	// no longer owns texture references.
	grmShaderGroup::SetAutoTexDict(true);

#if __WIN32 && __D3D
	grmShaderFx::RegisterTechniqueGroup("normals");
#endif
	RegisterTechniqueGroups();
	InitShaderSystems();
}

void vegSampleManager::InitShaderSystems() 
{
	char fullShaderLibPath[RAGE_MAX_PATH*4];	// potentially the path is several path's appended together with ; as a seperator
	char fullShaderDbPath[RAGE_MAX_PATH*4];		// potentially the path is several path's appended together with ; as a seperator

	const char* cmdLineShaderPath;
	if(PARAM_shaderpath.Get(cmdLineShaderPath))
	{
		sprintf(fullShaderLibPath,"%s/%s", cmdLineShaderPath, "tune/shaders/lib");
		sprintf(fullShaderDbPath,"%s/%s", cmdLineShaderPath, "tune/shaders/db");
	}
	else
	{
		//-shaderpath wasn't specified on the command line, and there wasn't an appropriate setting in the module settings file
		//so default the asset path + "/tune/shaders/lib"
		sprintf(fullShaderLibPath,"%s/%s", GetFullAssetPath(), "tune/shaders/lib");

		// This utterly fails on !__WIN32PC because GetModuleSetting always returns false!
		//No command line option was given specifying a preset directory, and no setting was available in the moduleSettings file
		//so don't load any shader presets
		sprintf(fullShaderDbPath,"%s/%s", GetFullAssetPath(), "tune/shaders/db");

	}

	//Check to see if the user overrode the -shaderpath setting on the command line, or what was in the module settings file by using
	//the -shaderlibs cmd line option
	const char* cmdLineShaderLibsPath;
	if(PARAM_shaderlibs.Get(cmdLineShaderLibsPath))
	{
		grcDisplayf("Overriding Module Setting for 'PathToCompiledShaders' with -shaderlib cmd line specification");
		strcpy(fullShaderLibPath, cmdLineShaderLibsPath);
	}

	//Load the shaders... multiple paths can be separ
	char * pPathName = &fullShaderLibPath[0];
	while(pPathName && *pPathName)
	{
		char * pSeperator = strchr(pPathName, ';');
		if (pSeperator)
			*pSeperator++ = 0;
		//Load the shaders...
		grcDisplayf("Loading shaders from : '%s'", pPathName);
		grmShaderFactory::GetInstance().PreloadShaders(pPathName);
		if (pSeperator)
		{
			pSeperator[-1] = ';';
			pPathName = pSeperator;
		}
		else
			pPathName = NULL;
	}	

	//Check to see if the user overrode the -shaderpath setting on the command line, or what was in the module settings file by using
	//the -shaderdb cmd line option
	const char* cmdLineShaderDbPath;
	if(PARAM_shaderdb.Get(cmdLineShaderDbPath))
	{
		Printf("Overriding Module Setting for 'PathToShaderDatabase' with -shaderdb cmd line specification\n");
		strcpy(fullShaderDbPath, cmdLineShaderDbPath);
	}

	m_MtlLib = grcMaterialLibrary::Preload(fullShaderDbPath);
	grcMaterialLibrary::SetCurrent(m_MtlLib);
}

void vegSampleManager::DestroyGfxFactories()
{
	grcMaterialLibrary::SetCurrent(NULL);
	if (m_MtlLib) 
	{
		m_MtlLib->Release();
		m_MtlLib = NULL;
	}
	grcSampleManager::DestroyGfxFactories();
}

void vegSampleManager::Init(const char* path)
{
	GetFileList();

	grcSampleManager::Init(path);	

	for (int i=0;i<NUM_BUCKETS;i++)
		m_BucketEnable[i]=true;
}


void vegSampleManager::GetFileList()
{
	const char* filePath=NULL;

	// get all file names:
	m_NumFiles=0;
	m_NumFiles=PARAM_file.GetArray(m_FileNames,MAX_FILES,m_ParamNameBuf,sizeof(m_ParamNameBuf));
	if (m_NumFiles < m_MinFiles)
	{
		for (int i = m_NumFiles; i < m_MinFiles; ++i) {
			m_FileNames[i] = m_DefaultFileNames[i];
		}
		m_NumFiles = m_MinFiles;
	}

	for (int i=0;i<m_NumFiles;i++)
	{
		// find the path separator:
		char *fp = (char*)m_FileNames[i] + strlen(m_FileNames[i]) - 1;
		while (fp > m_FileNames[i] && *fp != '/' && *fp != '\\')
			--fp;

		// if we 've found it, we grab the path from the file name:
		if (*fp == '/' || *fp == '\\') {
			filePath=m_FileNames[i];
			strcpy(m_FileNameBuf[i],fp+1);
			m_FileNames[i] = m_FileNameBuf[i];
			fp[1] = 0; // zero out path separator
			SetFullAssetPath(filePath);
			ASSET.PushFolder(filePath);
		}
	}
}

#if __BANK
static bool sRenderNormals = false;
void RenderNormals() {
#if __WIN32 && __D3D
	if ( sRenderNormals ) {
		int groupId = rage::grmShaderFx::FindTechniqueGroupId("normals");
		rage::grmShaderFx::SetForcedTechniqueGroupId(groupId);
	}
	else {
		rage::grmShaderFx::SetForcedTechniqueGroupId(-1);
	}
#endif // 
}

void vegSampleManager::AddWidgetsClient()
{
	AddWidgetsBuckets();
	AddWidgetsPasses();
	if (m_MtlLib)
		m_MtlLib->AddWidgets(BANKMGR.CreateBank("rage - Shader Database"));
	AddWidgetsDebugDraw();
}

void vegSampleManager::AddWidgetsBuckets()
{
	// Bucket Widgets:
	bkBank &bankBuckets = BANKMGR.CreateBank("rage - Buckets");
	for (int i=0;i<NUM_BUCKETS;i++)
	{
		char bucketName[32];
		sprintf(bucketName,"Bucket %d",i);
		bankBuckets.AddToggle(bucketName,&m_BucketEnable[i]);
	}
}

void vegSampleManager::AddWidgetsPasses()
{
	// passes widgets:
	bkBank &bankPasses = BANKMGR.CreateBank("rage - Passes");
	bankPasses.AddSlider("Max Passes",&grmShader::GetMaxPasses(),0,8,1);
}

void vegSampleManager::AddWidgetsDebugDraw()
{
	bkBank &bk = BANKMGR.CreateBank("rage - Debug Draw");
	bk.AddToggle("Render Normals", &sRenderNormals, CFA(RenderNormals));
	bk.AddButton("Reload Shaders", datCallback(grcEffect::ReloadAll));
    bk.AddToggle("Draw Grid", &m_DrawGrid);
    bk.AddToggle("Draw Help", &m_DrawHelp);
    bk.AddToggle("Draw World Axes", &m_DrawWorldAxes);
    bk.AddToggle("Draw Logo", &m_DrawLogo);
}
#endif // __BANK

} // namespace ragesamples
