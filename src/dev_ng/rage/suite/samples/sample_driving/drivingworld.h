// 
// sample_aiworld/drivingWorld.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_AIWORLD_drivingWorld_H
#define SAMPLE_AIWORLD_drivingWorld_H

#include "drivingcar.h"

#include "ainav/path.h"
#include "sample_simpleworld/sample_simpleworld.h"

namespace ragesamples
{

/*
PURPOSE
	drivingWorld is a simple demonstration of pathfinding in a "real-world"
	setting.  It is capable of loading visible geometry and physics bounds
	from entity.type files or from RDR-style rsChild.txt files.  It can load
	navigation data from .nav files, navigation meshes grouped into a nav_main.xml
	file and from streaming navigation mesh resource files.
*/
class drivingWorld : public rmcSampleSimpleWorld
{
public:
	/* drivingWorld interface. */

	// PURPOSE:	Constructor.
	drivingWorld();

	// PURPOSE:	Initialize the navigation system.
	// PARAMS:	useSeparateThread	- True to run the navigation system in
	//								  its own thread.
	void InitNavigation(bool useSeparateThread);

	// PURPOSE:	Update the navigation system, to be done once a frame.
	void UpdateNavigation();

	// PURPOSE:	Shut down the navigation system.
	void ShutdownNavigation();

	// PURPOSE: Responds to clicks to select and position an actor or to set
	// the actor's pathfinding goal.
	void UpdateMouse();

	// PURPOSE: Move all actors along their path.
	void UpdateActors();

	// PURPOSE: Draw all the actors.
	void DrawActors();

	// PURPOSE: Stop all the actors from pathfinding.
	void StopActors();

	/* grcSampleManager interface. */
	virtual void InitClient();
	virtual void ShutdownClient();
	virtual void UpdateClient();
	virtual void DrawClient();
	virtual const char* GetSampleName() const { return "Driving Nav Test"; }

	// PURPOSE: Loads a .nav file containing the navigation mesh.
	void LoadNavLevel(const char* fileName);

	// PURPOSE: Loads a .bnd file containing physics bounds.
	void LoadBound(const char* fileName);

	void UnloadAllBounds();

	// PURPOSE: Loads a navigation level from a resource file.
	void LoadNavRsc(const char* fileName);

	// A bunch of helper functions for scripts.
	void SetNumActors(int n);
	void SetActorPosition(int actor, const Vector3& pos, bool probe);
	void GetActorPosition(int actor, Vector3& pos) const;
	void SetActorGoal(int actor, const Vector3& pos, bool probe);
	void SetAllActorsPaused(bool pause);
	void SetActorFlagPref(int actor, const char* flagName);
	void SetActorFlagWeight(int actor, float weight);
	void SetActorWidth(int actor, float width);
	void SetActorSpeed(int actor, float speed);
	void SelectActor(int actor);
	void SetCameraFollow(bool follow)			{	m_CameraFollowActor = follow;	}
	void SetCameraOffset(const Vector3& offset)	{	m_CameraOffset = offset;	}
	void SetCameraPosition(const Vector3& pos);
	void SetCameraEulersInDegrees(const Vector3& eulers);
	void SetCameraLookAt(const Vector3& pos, float slerp);
	bool IsActorAtGoal(int actor) const;
	void SubmitActorQuery(int actor);
	void SetDrawOptions(bool enabled, float radius, const char* flagName);

	// A bunch of helper functions for scripts.
	void RegisterScriptingCommands();
	static void ScriptSetNumActors(int n);
	static void ScriptSetActorPosition(int actor, const Vector3& pos, bool probe);
	static void ScriptGetActorPosition(int actor, Vector3& pos);
	static void ScriptSetActorGoal(int actor, const Vector3& pos, bool probe);
	static void ScriptSetAllActorsPaused(bool pause);
	static void ScriptSetActorFlagPref(int actor, const char* flagName);
	static void ScriptSetActorFlagWeight(int actor, float weight);
	static void ScriptSetActorWidth(int actor, float width);
	static void ScriptSetActorSpeed(int actor, float width);
	static void ScriptSelectActor(int actor);
	static void ScriptSetCameraFollow(bool follow);
	static void ScriptSetCameraOffset(const Vector3& offset);
	static void ScriptSetCameraPosition(const Vector3& pos);
	static void ScriptSetCameraEulersInDegrees(const Vector3& eulers);
	static void ScriptSetCameraLookAt(const Vector3& pos, float slerp);
	static bool ScriptIsActorAtGoal(int actor);
	static void ScriptSubmitActorQuery(int actor);
	static void ScriptSetDrawOptions(bool enabled, float radius, const char* flagName);

#if __BANK
	// A bunch of helper functions for bank button clicks.
	void AddWidgetsClient();
	void BankLoadEntity();
	void BankUnloadEntity();
	void BankLoadBound();
	void BankLoadNavLevel();
	void BankLoadNavRsc();
	void BankUnloadNavLevel();
	void BankChangedNumActors();
	void BankDefaultDimensionsChanged();
	void BankAllPausedChanged();
#endif

protected:
	/* drivingWorld internal interface. */

	// PURPOSE: Static pointer to the AI sample world instance used by the
	// scripting commands.
	static drivingWorld*		sm_ScriptTargetWorld;

	// PURPOSE:	An owned path waypoint pool.
	rage::ai::navPathGroup*		m_PathGroup;

	// PURPOSE:	True if the mesh is currently being drawn.
	bool						m_DrawMesh;

	// PURPOSE: True if we should draw all actors's paths.
	bool						m_DrawPaths;

	// PURPOSE: The maximum distance from the camera to draw the mesh.
	float						m_DrawRadius;

	// PURPOSE: True if the camera should follow the actor.
	bool						m_CameraFollowActor;

	// PURPOSE: Offset from the selected actor for the camera to follow
	// when m_CameraFollowActor is true.
	Vector3						m_CameraOffset;

	// PURPOSE: The number of actors before the most recent change.
	int							m_PrevNumActors;

	// PURPOSE: The current number of actors, set in a widget.
	int							m_NumActors;

	// PURPOSE: The maximum number of actors this sample supports.
	static const int			sm_MaxActors = 1;

	// PURPOSE: The actors.
	drivingCar				m_Actors[sm_MaxActors];

#if __BANK
	// PURPOSE: Used to reload the navigation bank when a level is loaded.
	rage::bkBank*				m_NavBank;

	// PURPOSE: Used to reload the actor bank when a new actor is selected.
	rage::bkBank*				m_ActorBank;
#endif

	// PURPOSE: The currently selected actor.
	drivingCar*				m_SelectedActor;

	// PURPOSE: Modifier key bound to move camera, used here to prevent
	// registering clicks intended for camera movement.  Usually ALT.
	rage::ioValue				m_MoveCameraKey;

	// PURPOSE: Modifier key which makes right mouse clicks teleport
	// the actor instead of setting the goal.  Usually CTRL.
	rage::ioValue				m_TeleportKey;

	// PURPOSE: Modifier key which makes teleporting the actor not interrupt
	// the current pathfinding operation.  Usually Shift.
	rage::ioValue				m_TeleportQuietKey;

	// PURPOSE: Default actor radius, used by navLevel debug drawing.
	float						m_DefaultRadius;

	// PURPOSE: Not sure this does anything.
	float						m_DefaultHeight;

	// PURPOSE: If set, all actors are paused.
	bool						m_AllPaused;

	// PURPOSE: Array used to look up flags from indicies.  Not owned.
	rage::phMaterialFlags*		m_MaterialFlags;

	// PURPOSE: The index into m_MaterialFlags of the flags required
	// for a polygon in the navigation mesh to be drawn.
	int							m_DrawFlagIndex;

	atFixedArray<phDemoObject*, 32> m_SimplePhysObjs;
};

} // namespace ragesamples

#endif // ifndef SAMPLE_AIWORLD_drivingWorld_H
