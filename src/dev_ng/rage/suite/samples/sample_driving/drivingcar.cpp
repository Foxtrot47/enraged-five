// 
// sample_aiworld/drivingCar.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "drivingCar.h"

#include "aicore/debugdraw.h"
#include "ainavigation/levelsimplemesh.h"
#include "ainavigation/parsable.h"
#include "ainavigation/searchhierarchy.h"
#include "ainav/locus.h"
#include "ainav/parsable.h"
#include "ainav/path.h"
#include "ainav/pathexec.h"
#include "ainav/spaceiter.h"
#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "crskeleton/skeleton.h"
#include "input/mapper.h"
#include "input/mouse.h"
#include "input/pad.h"
#include "phbound/boundcapsule.h"
#include "physics/levelnew.h"
#include "phcore/materialmgrflag.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/geometry.h"

using namespace rage;
using namespace ai;
using namespace ragesamples;

drivingCar::drivingCar()
: m_Query(NULL), m_SteerSTRL(m_DrivePathMulti), m_SteerMulti(m_SteerSTRL)
{
}

void drivingCar::Init(const phMaterialFlags* materialFlags)
{
	Assert(!m_Query);

	m_Query = rage_new navQueryQuickestPath;

	m_Color.Set(rand() & 0xFF, rand() & 0xFF, rand() & 0xFF);

	m_Speed = 50.f;
	m_Radius = 0.25f;
	m_Height = 1.75f;
	m_MaxStepUp = 0.51f;

	m_Paused = false;

	m_Capabilities.AddRef();
	m_PathFilter.AddRef();

	m_Capabilities.GetDimensions().SetWidth(m_Radius * 2.f);
	m_Capabilities.GetDimensions().SetHeight(m_Height);
	m_Capabilities.SetMaterialFilter(&m_PathFilter);

	m_CurrentPosition.Zero();
	m_GoalPosition.Zero();

	m_MaterialFlags = materialFlags;
	m_PreferredFlagIndex = 0;

	m_CurrentHeading.Set(XAXIS);
	m_GoalHeading.Set(XAXIS);
}

void drivingCar::Shutdown()
{
	delete m_Query;
	m_Query = NULL;

	m_Capabilities.SetMaterialFilter(NULL);

	m_Capabilities.RemoveRef();
	m_PathFilter.RemoveRef();
}

void drivingCar::Draw(bool drawPath, bool selected)
{
#if NAV_DEBUGDRAW
	if (drawPath && m_Query->PathFound())
	{
		navPath& path = m_Query->PathGet();
		path.DebugDraw(m_StartLocus.GetPosition(), path.GetHandleFirst(), 0.1f);
	}
#endif

#if __DEV
	Matrix34 world(M34_IDENTITY);
	world.d.y = 0.2f;
	grcWorldMtx(world);
	m_DrivePathTurns.DrawDebug(100.f);
	world.d.y = 0.3f;
	grcWorldMtx(world);
	m_SteerMulti.DrawDebug(100.f);
#endif

	drawPath = false;

	grcColor(selected ? Color_red : m_Color);

	Vector3 center(m_CurrentPosition.x, m_CurrentPosition.y + m_Height * 0.5f, m_CurrentPosition.z);

	grcWorldIdentity();
	grcDrawCapsule(m_Height, m_Radius, center, 16, true);
	grcDrawSphere(m_Radius, m_GoalLocus.GetPosition(), 16, true, true);
}

bool drivingCar::MoveTowards(const Vector3& dest)
{
	Vector3 prevPosition(m_CurrentPosition);

	float dist = sqrtf(square(dest.x - m_CurrentPosition.x) + square(dest.z - m_CurrentPosition.z));

	float speed = m_Speed * TIME.GetSeconds();

	if(speed > dist)
		m_CurrentPosition = dest;
	else
		m_CurrentPosition.Lerp(speed / dist, m_CurrentPosition, dest);

	phSegment segment;
	phIntersection isect;

	for (int n = 0; ; n++)
	{
		segment.Set(Vector3(prevPosition.x, prevPosition.y + m_MaxStepUp, prevPosition.z),
			Vector3(m_CurrentPosition.x, prevPosition.y + m_MaxStepUp, m_CurrentPosition.z));

		if (!PHLEVEL->TestProbe(segment, &isect))
			break;

		Vector3 delta = m_CurrentPosition - RCC_VECTOR3(isect.GetPosition());

		m_CurrentPosition.SubtractScaled(RCC_VECTOR3(isect.GetNormal()), delta.Dot(RCC_VECTOR3(isect.GetNormal())) * 1.001f);

		// Check if stuck in a corner
		if (n == 1)
			return true;
	}

	const float probeBelow = 1000.f;
	bool foundGround = false;

	// This mover checks 4 points spaced evenly around the circle at half the
	// actors radius.

	m_CurrentPosition.y = prevPosition.y - probeBelow;

	segment.A.Set(m_CurrentPosition.x, prevPosition.y + m_MaxStepUp, m_CurrentPosition.z);
	segment.B.Set(m_CurrentPosition.x, prevPosition.y - probeBelow, m_CurrentPosition.z);

	segment.A.x -= 0.5f * m_Radius;
	segment.B.x -= 0.5f * m_Radius;
	if (PHLEVEL->TestProbe(segment, &isect))
		foundGround = true, m_CurrentPosition.y = Max(m_CurrentPosition.y, isect.GetPosition().GetYf());

	segment.A.x += m_Radius;
	segment.B.x += m_Radius;
	if (PHLEVEL->TestProbe(segment, &isect))
		foundGround = true, m_CurrentPosition.y = Max(m_CurrentPosition.y, isect.GetPosition().GetYf());

	segment.A.x -= 0.5f * m_Radius;
	segment.B.x -= 0.5f * m_Radius;
	segment.A.z -= 0.5f * m_Radius;
	segment.B.z -= 0.5f * m_Radius;
	if (PHLEVEL->TestProbe(segment, &isect))
		foundGround = true, m_CurrentPosition.y = Max(m_CurrentPosition.y, isect.GetPosition().GetYf());

	segment.A.z += m_Radius;
	segment.B.z += m_Radius;
	if (PHLEVEL->TestProbe(segment, &isect))
		foundGround = true, m_CurrentPosition.y = Max(m_CurrentPosition.y, isect.GetPosition().GetYf());

	if (!foundGround)
		m_CurrentPosition.y = prevPosition.y;

	// Check if the stuck on a wall
	return (prevPosition.Dist2(m_CurrentPosition) < square(Min(speed, dist) * 0.1f));
}

bool drivingCar::Update()
{
	if (m_Paused)
		return false;

	if (m_DrivePathTurns.GetNumSpans() == 0 && m_Query->IsInitialized() && 
		navLevel::GetInstance().Receive(*m_Query) &&
		m_Query->IsStateDone() && m_Query->PathFound())
	{
		const navPath &path = m_Query->PathGet();

		for (navPathActionHandle handle = path.GetHandleFirst(); handle.IsValid(path); handle.Next(path))
		{
			const navPathAction& action = path.ActionGet(handle);

			if (action.GetType() == navPathAction::kActionTypeGoWithinSpan)
			{
				Vector3 left = action.GetGoWithinSpan().m_Left;
				Vector3 right = action.GetGoWithinSpan().m_Right;

				Vector3 delta;
				delta.Subtract(right, left);
				delta.Normalize();

				left.AddScaled(delta, 1.f);
				right.AddScaled(delta, -1.f);

				m_DrivePathTurns.AddSpans(1, &left, &right);

				if (m_DrivePathTurns.GetNumSpans() == m_DrivePathTurns.GetMaxSpans())
					break;
			}
		}
	}

	m_DrivePathTurns.CalcTurnSections();
	m_SteerMulti.SetStart(m_CurrentPosition, m_CurrentHeading);
	m_SteerMulti.SetTarget(m_GoalPosition, m_GoalHeading);
	m_SteerMulti.SetDrivePath(m_DrivePathTurns);
	m_SteerMulti.Calculate();

	return false;
}

void drivingCar::Submit()
{
	// STEP #5. Populate and submit a navigation query.
	navLevelSimpleMesh &level = static_cast<navLevelSimpleMesh&>(navLevelSimpleMesh::GetInstance());
	if (!level.IsReady())
		return;

	m_DrivePathTurns.SetNumSpans(0);

	// -- The navigation system may currently be processing this query.
	//    Before we can submit it again, we need to make sure to tell
	//    navigation system that we are not interested in continuing
	//    processing the previous query.
	level.Cancel(*m_Query);

	// -- We are about to call Init() on the query, but calls to Shutdown()
	//    and Init() have to match, so we need to make sure Shutdown() was
	//    called if the query is currently initialized. For convenience,
	//    navQuery has an IsInitialized() function so the user doesn't need
	//    to remember.
	if(m_Query->IsInitialized())
		m_Query->Shutdown();

	// -- Set up the navLocus objects. In this example, these are simple
	//    points, but the same query can work with for example sets of
	//    points (using navLocusUnion) as the start and/or destination.
	m_StartLocus.SetPosition(m_CurrentPosition);
	m_GoalLocus.SetPosition(m_GoalPosition);

	m_Capabilities.SetMaterialFilter(m_PathFilter.m_PreferredFlags ? &m_PathFilter : NULL);

	// -- Initialize the navQueryQuickestPath object, specifying the start
	//    locus and the destination locus to use, and telling it how fast
	//    this agent can move and what capabilities it has.
	m_Query->Init(m_StartLocus, m_GoalLocus, m_Speed, &m_Capabilities);

	// -- Call Submit(). This will add the query to a queue that the
	//    navigation system has. By using this interface, the navLevel
	//    internally can spread the computations across multiple frames
	//    or do processing in a separate thread, transparent to the user.
	level.Submit(*m_Query);

	// STOP.
}

void drivingCar::Stop()
{
	Assert(m_Query);

	// STEP #6. Cancel, shut down, and destroy the navQuery object when you
	// don't need it anymore.

	// -- Make sure to cancel the query if it has been submitted to the query
	//    manager, i.e. is currently being processed by the navigation system.
	//    Shutdown() doesn't currently do this for us.
	navLevel::GetInstance().Cancel(*m_Query);

	// -- Call Shutdown() on the query. The class will assert internally that
	//    calls to Init() and Shutdown() are matched.
	if(m_Query->IsInitialized())
		m_Query->Shutdown();

	// STOP.
}

void drivingCar::SetCurrentPosition(const Vector3& pos, bool probe)
{
	m_CurrentPosition = pos;

	if (probe)
	{
		phSegment segment;
		phIntersection isect;

		segment.A.Set(pos.x, 1000, pos.z);
		segment.B.Set(pos.x, -1000, pos.z);

		if (PHLEVEL->TestProbe(segment, &isect))
			m_CurrentPosition = RCC_VECTOR3(isect.GetPosition());
	}
}

void drivingCar::SetGoal(const Vector3& pos, bool probe)
{
	m_GoalPosition = pos;

	if (probe)
	{
		phSegment segment;
		phIntersection isect;

		segment.A.Set(pos.x, 1000, pos.z);
		segment.B.Set(pos.x, -1000, pos.z);

		if (PHLEVEL->TestProbe(segment, &isect))
			m_GoalPosition = RCC_VECTOR3(isect.GetPosition());
	}
}

void drivingCar::SetFlagPref(const char* flagName)
{
	m_PreferredFlagIndex = 0;

	if (!m_MaterialFlags || !flagName)
		return;

	phMaterialFlags flag = MATERIALMGRFLAG.GetFlagByName(flagName);

	for (int i = 0, n = MATERIALMGRFLAG.GetNumFlags(); i < n; i++)
	{
		if (m_MaterialFlags[i] == flag)
		{
			m_PreferredFlagIndex = i;
			break;
		}
	}

	m_PathFilter.m_PreferredFlags = m_MaterialFlags[m_PreferredFlagIndex];
}

bool drivingCar::IsAtGoal(float epsilon) const
{
	return (GetCurrentPosition().FlatDist2(m_GoalPosition) < epsilon);
}

#if __BANK

void drivingCar::AddWidgets(bkBank& bank)
{
	bank.AddSlider("Height", &m_Height, 0.f, 2.f, 0.1f, datCallback(MFA(drivingCar::BankDimensionsChanged),this));
	bank.AddSlider("Radius", &m_Radius, 0.f, 2.f, 0.1f, datCallback(MFA(drivingCar::BankDimensionsChanged),this));
	bank.AddSlider("Speed", &m_Speed, 0.f, 200.f, 10.f, datCallback(MFA(drivingCar::BankDimensionsChanged),this));
	bank.AddSlider("Max Step Up", &m_MaxStepUp, 0.1f, 1.f, .1f, datCallback(MFA(drivingCar::BankDimensionsChanged),this));
	bank.AddToggle("Paused", &m_Paused);

	const char** flagNames = Alloca(const char*, MATERIALMGRFLAG.GetNumFlags() + 1);
	flagNames[0] = "All Materials";

	int n = 1;
	for (atMap<ConstString, phMaterialFlags>::ConstIterator i = MATERIALMGRFLAG.GetFlagIter(); i; ++i)
		flagNames[n++] = i.GetKey();

	bank.AddCombo("Preference", &m_PreferredFlagIndex, MATERIALMGRFLAG.GetNumFlags() + 1, flagNames, datCallback(MFA(drivingCar::BankDimensionsChanged),this));
	bank.AddSlider("Pref. Weight", &m_PathFilter.m_Weight, 0.1f, 10.f, 1.f, datCallback(MFA(drivingCar::BankDimensionsChanged),this));
}

void drivingCar::BankDimensionsChanged()
{
	m_Capabilities.GetDimensions().SetWidth(m_Radius * 2.f);
	m_Capabilities.GetDimensions().SetHeight(m_Height);
	m_PathFilter.m_PreferredFlags = m_MaterialFlags[m_PreferredFlagIndex];

	Submit();
}

#endif // __BANK
