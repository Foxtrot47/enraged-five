// 
// sample_ai/drivingWorld.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// PURPOSE
//	This application is a tester for the 'ainav' and 'ainavigation' modules.
//	It demonstrates the search portion of the nav code, by submitting queries to the
//	navLevel. This is the primary way for games to interface with
//	the RAGE navigation system.

#define STATIC_HEAP_SIZE 10000000

#include "drivingworld.h"
#include "drivingmaterial.h"

#include "aicore/debugdraw.h"
#include "ainavigation/levelsimplemesh.h"
#include "ainavigation/parsable.h"
#include "ainavigation/searchhierarchy.h"
#include "ainav/locus.h"
#include "ainav/parsable.h"
#include "ainav/path.h"
#include "ainav/pathexec.h"
#include "ainav/spaceiter.h"
#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "crskeleton/skeleton.h"
#include "input/mapper.h"
#include "input/mouse.h"
#include "input/pad.h"
#include "phbound/boundcapsule.h"
#include "phcore/materialmgrflag.h"
#include "physics/levelnew.h"
#include "physics/shapetest.h"
#include "physics/simulator.h"
#include "script/wrapper.h"
#include "system/timemgr.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/geometry.h"
#include "vectormath/mat34v.h"

using namespace rage;
using namespace ai;
using namespace ragesamples;

//-----------------------------------------------------------------------------

PARAM(path,			"The base path to assets.");
PARAM(threadnav,	"Execute pathfinding in a seperate thread.");
PARAM(level,		"[sample_driving] The path to an entity.type file to load visible geometry and physics bounds from");
PARAM(nav,			"[sample_driving] The path to a .nav file to load navigation data from");
PARAM(bound,		"[sample_driving] The path to a .bnd file to load physics bounds");
PARAM(navrsc,		"[sample_driving] The path to a navigation resource file");

//-----------------------------------------------------------------------------

drivingWorld::drivingWorld()
	: m_PathGroup(NULL)
	, m_MaterialFlags(NULL)
{
}

void drivingWorld::InitNavigation(bool useSeparateThread)
{
	// STEP #1. Set up a pool of path waypoints.

	// -- Create a navPathGroup object, which is a pool of path
	//    waypoints that various navPath objects will use.
	//    1024 here is the total number of path waypoints that
	//    everybody has to share. These objects are fairly small,
	//    currently 36 or so bytes each. The -1 means that there
	//    is no limitation for the number of objects used by each
	//    agent. Note that if a separate thread is going to be used
	//    by the navigation system, navPathGroup needs to be created in
	//    a thread-safe mode.
	Assert(!m_PathGroup);
	m_PathGroup = rage_new navPathGroup(sm_MaxActors * 100, -1, useSeparateThread);

	// -- There can be many navPathGroup objects in a game, so we
	//    need to tell the navQuery classes which one they are supposed
	//    to use. This does not transfer ownership of the object.
	navQuery::PathGroupUse(*m_PathGroup);

	// STEP #2. Set up a navLevel object.

	// -- Set the resolution we need for vertices in navSpaceMesh. If a game
	//    uses small mesh chunks, they can reduce this value and thus improve the
	//    precision for each vertex position.
	aiStoredFloat8<navActorCapabilities>::ClassSetResolution(0.2f);
	aiStoredFloat8<navSpaceMesh>::ClassSetResolution(4.0f);
	aiStoredFloat<navSpaceMesh>::ClassSetResolution(0.05f);

	// -- Create a navLevelSimpleMesh. This is a navLevel subclass that uses
	//    hierarchical pathfinding with three subspaces (levels in the
	//    hierarchy). The top level is a coarse grid, in which each cell
	//    contains a navigation mesh chunk. Each polygon in the mesh chunk
	//    can potentially be subdivided in a fine rectangular grid as needed,
	//    for pathfinding around dynamic obstacles.
	//    Note that calling CreateInstance also sets navLevel::sm_Instance,
	//    making it the one and only navLevel instance in the game.
	navLevelSimpleMesh &lvl = navLevelSimpleMesh::CreateInstance();

	// -- To avoid dynamic memory allocation, and for other reasons, there
	//    is a limit to how many navigation nodes can be a part of any given
	//    A* search. There is a default number, but in this sample we want
	//    to be able to demonstrate and test longer searches, and different
	//    games will probably want to set this according to their needs and
	//    resources.
	lvl.SetMaxNumSearchNodes(4096);

	// -- If running with a separate thread dedicated to the navigation system,
	//    let navLevel know that that's what you want.
	//    Note: in a similar way, you could configure the navLevel object here
	//    to enable streaming of data.
	if(useSeparateThread)
	{
		lvl.SetUseThreading(true);
	}

	// -- Call Init() on the navLevel. This creates various internal objects,
	//    but doesn't actually load any navigation data.
	lvl.Init();

	// -- Allow an A* search to take no more than 3 seconds in total.
	//    The default is currently 0.5 seconds and isn't always enough for
	//    searches across long distances.
	//    Note that this currently has to be done after calling Init().
	lvl.SetMaxPathfinderTotalSearchTime(3.0f);	// MAGIC!

	// Disable some things we don't use when driving
	lvl.SetAllowCornerRounding(false);
	lvl.SetAllowPathPostprocessing(false);
	lvl.SetAllowPathObjects(false);
	lvl.SetAllowDirectPath(false);

	// -STOP.

	m_MaterialFlags = rage_new rage::phMaterialFlags[MATERIALMGRFLAG.GetNumFlags() + 1];
	m_MaterialFlags[0] = 0;
	m_DrawFlagIndex = 0;
	m_CameraFollowActor = false;
	m_CameraOffset.Zero();

	int n = 1;
	for (atMap<ConstString, phMaterialFlags>::ConstIterator i = MATERIALMGRFLAG.GetFlagIter(); i; ++i)
		m_MaterialFlags[n++] = *i;
}

void drivingWorld::UpdateNavigation()
{
	// STEP #4. On every frame, make sure the navigation system gets updated.

	navLevelSimpleMesh& level = static_cast<navLevelSimpleMesh&>(navLevel::GetInstance());

	// -- Call Update() once per frame from the main thread, even if the navigation
	//    system is running in its own thread.
	level.Update();

	// -STOP.
}

void drivingWorld::UpdateActors()
{
	for (int i = 0; i < m_NumActors; i++)
		m_Actors[i].Update();
}

void drivingWorld::DrawActors()
{
	bool oldLighting = grcLighting(grclmNone);

	for (int i = 0; i < m_NumActors; i++)
		m_Actors[i].Draw(m_DrawPaths, m_SelectedActor == &m_Actors[i]);

	grcLighting(oldLighting);
}

void drivingWorld::StopActors()
{
	for (int i = 0; i < m_NumActors; i++)
		m_Actors[i].Stop();
}

void drivingWorld::ShutdownNavigation()
{
	// STEP #7. Shut down the navigation system.

	// -- Call Shutdown() on the navLevel instance, so that there are matching
	//    Init() and Shutdown() calls.
	navLevel::GetInstance().Shutdown();

	// -- Destroy the instance. This matches the call to CreateInstance(),
	//    and sets navLevel::sm_Instance back to NULL.
	navLevel::DestroyInstance();

	// STEP #8. Shut down the path waypoint pool.

	// -- Tell navQuery that it can no longer use pathGroup. Note that
	//    a navPathGroup is a reference-counted object with a specific
	//    owner, which means that if we had forgotten to do this, an
	//    assert would fail when we try to destroy the object, as
	//    navQuery would still have a reference to it.
	navQuery::PathGroupRelease();

	// -- We created the navPathGroup and are responsible for deleting
	//    it like this.
	Assert(m_PathGroup);
	delete m_PathGroup;
	m_PathGroup = NULL;

	delete m_MaterialFlags;
	m_MaterialFlags = NULL;

	// -STOP.
}

void drivingWorld::InitClient()
{
	rmcSampleSimpleWorld::InitClient();

	drivingMaterial::RegisterMaterialFlags();
	RegisterScriptingCommands();

	// Initialize flags.
	m_DrawMesh = true;
	m_DrawPaths = true;
	m_DrawRadius = 400.f;
	m_NumActors = m_PrevNumActors = 1;

	// Set up the asset path.
	const char* pathName = RAGE_ASSET_ROOT;
	PARAM_path.Get(pathName);
	SetFullAssetPath(pathName);

	// Register the parsable classes.
	REGISTER_PARSABLE_CLASS(spdGrid2D);
	RegisterParsableClassesAiNav();
	RegisterParsableClassesAiNavigation();

	// Check if we're going to run in multi-threaded mode.
	bool useSeparateThread = PARAM_threadnav.Get();

	InitNavigation(useSeparateThread);

	for (int i = 0; i < sm_MaxActors; i++)
		m_Actors[i].Init(m_MaterialFlags);

	m_Mapper.Map(IOMS_KEYBOARD, KEY_ALT, m_MoveCameraKey);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_CONTROL, m_TeleportKey);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_SHIFT, m_TeleportQuietKey);

	navActorDimensions& dim = navActorCapabilities::GetDefault().GetDimensions();
	m_DefaultRadius = dim.GetRadius();
	m_DefaultHeight = dim.GetHeight();

	m_AllPaused = false;

	BANK_ONLY(m_NavBank = m_ActorBank = NULL);

	if (PARAM_navrsc.Get(pathName))
		LoadNavRsc(pathName);

	if (PARAM_bound.Get(pathName))
		LoadBound(pathName);

	if (PARAM_nav.Get(pathName))
		LoadNavLevel(pathName);

	if (PARAM_level.Get(pathName))
		LoadFragDrawableEntity(pathName);

	for (int i = 0; i < m_NumActors; i++)
		m_Actors[i].SetCurrentPosition(VEC3_ZERO, true);

	m_SelectedActor = NULL;
}

void drivingWorld::ShutdownClient()
{
	for (int i = 0; i < m_NumActors; i++)
		m_Actors[i].Stop();

	for (int i = 0; i < sm_MaxActors; i++)
		m_Actors[i].Shutdown();

	ShutdownNavigation();

	UnloadAllBounds();

	rmcSampleSimpleWorld::ShutdownClient();
}

void drivingWorld::UpdateClient()
{
	rmcSampleSimpleWorld::UpdateClient();

	if (!m_SelectedActor)
		SelectActor(0);

	UpdateNavigation();

	UpdateMouse();

	if (m_SelectedActor && m_CameraFollowActor)
		m_CameraOffset = GetCamMgr().GetWorldMtx().d - m_SelectedActor->GetCurrentPosition();

	UpdateActors();

	if (m_SelectedActor && m_CameraFollowActor)
	{
		Matrix34 m;
		m.Set3x3(GetCamMgr().GetWorldMtx());
		m.d = m_SelectedActor->GetCurrentPosition() + m_CameraOffset;
		m.LookDown(-m_CameraOffset);
		GetCamMgr().SetWorldMtx(m);
	}
}

void drivingWorld::UpdateMouse()
{
	if (m_MoveCameraKey.IsReleased())
	{
		Vector3 v = GetCamMgr().GetWorldMtx().d;
		Printf("Setting camera position to << %0.2f, %0.2f, %0.2f >>\n", v.x, v.y, v.z);
		GetCamMgr().GetWorldMtx().ToEulersXYZ(v);
		v *= RtoD;
		Printf("Setting camera eulers (in degrees) to << %0.2f, %0.2f, %0.2f >>\n", v.x, v.y, v.z);
	}

	if (m_MoveCameraKey.IsDown() || !grcViewport::GetCurrent())
		return;
	
	if (ioMouse::GetButtons() & ioMouse::MOUSE_LEFT)
	{
		phSegment seg;

		grcViewport::GetCurrent()->ReverseTransform(float(ioMouse::GetX()), float(ioMouse::GetY()),	seg.A, seg.B);

		drivingCar* newSelection = NULL;
		float maxDepth = 0;

		phBoundCapsule capsule;
		phIntersection isect;

		for (int i = 0; i < m_NumActors; i++)
		{
			capsule.SetCapsuleSize(m_Actors[i].GetRadius(),
								   m_Actors[i].GetHeight());
			capsule.SetCentroidOffset(m_Actors[i].GetCurrentPosition() +
				Vector3(0.f, m_Actors[i].GetHeight() * 0.5f, 0.f));

			phShapeTest<phShapeProbe> shapeTest;
			shapeTest.InitProbe(seg,&isect);

			if (shapeTest.TestOneObject(capsule) && isect.GetDepth() > maxDepth)
				newSelection = &m_Actors[i], maxDepth = isect.GetDepth();
		}

		capsule.Release(false);

		if (PHLEVEL->TestProbe(seg, &isect))
		{
			for (int i = 0; i < m_NumActors; i++)
			{
				Vector3 currentHeading;
				currentHeading.Subtract(RCC_VECTOR3(isect.GetPosition()), m_Actors[i].GetCurrentPosition());
				if (fabsf(currentHeading.y) < 0.5f && currentHeading.FlatMag2() < 100.f)
				{
					currentHeading.y = 0;
					currentHeading.NormalizeSafe(XAXIS);
					m_Actors[i].SetCurrentHeading(currentHeading);
				}

				Vector3 goalHeading;
				goalHeading.Subtract(RCC_VECTOR3(isect.GetPosition()), m_Actors[i].GetGoal());
				if (fabsf(goalHeading.y) < 0.5f && goalHeading.FlatMag2() < 100.f)
				{
					goalHeading.y = 0;
					goalHeading.NormalizeSafe(XAXIS);
					m_Actors[i].SetGoalHeading(goalHeading);
				}
			}
		}

#if __BANK
		if (newSelection && m_SelectedActor != newSelection)
		{
			m_ActorBank->Truncate(0);
			m_SelectedActor = newSelection;
			m_SelectedActor->AddWidgets(*m_ActorBank);
		}
#endif
	}
	else if ((ioMouse::GetPressedButtons() & ioMouse::MOUSE_RIGHT) &&  m_NumActors > 0)
	{
		phSegment seg;

		grcWorldIdentity();
		grcViewport::GetCurrent()->ReverseTransform(float(ioMouse::GetX()), float(ioMouse::GetY()),	seg.A, seg.B);

		phIntersection isect;

		if (PHLEVEL->TestProbe(seg, &isect))
		{
			drivingCar* selected = m_SelectedActor ? m_SelectedActor : m_Actors;

			if (m_TeleportKey.IsDown())
			{
				selected->SetCurrentPosition(RCC_VECTOR3(isect.GetPosition()), false);

				Printf("Setting actor position to << %0.2f, %0.2f, %0.2f >>\n", isect.GetPosition().GetXf(), isect.GetPosition().GetYf(), isect.GetPosition().GetZf());

				if (m_TeleportQuietKey.IsUp())
					selected->Submit();
			}
			else
			{
				Printf("Setting actor goal to << %0.2f, %0.2f, %0.2f >>\n", isect.GetPosition().GetXf(), isect.GetPosition().GetYf(), isect.GetPosition().GetZf());

				selected->SetGoal(RCC_VECTOR3(isect.GetPosition()), false);
				selected->Submit();
			}
		}
	}
}

void drivingWorld::DrawClient()
{
#if __DEV
	grcLighting(grclmNone);

	navLevelSimpleMesh& level = static_cast<navLevelSimpleMesh&>(navLevel::GetInstance());

	// Allow the navigation system to draw debugging info.
	level.DebugDraw();

	// Draw the mesh.
	if (level.IsReady() && m_DrawMesh)
	{
		const navSpaceMesh* mesh = level.GetMesh();
		Assert(mesh);

		const Vector3& center = GetCameraMatrix().d;

		drivingMaterialFilter filter(m_MaterialFlags[m_DrawFlagIndex]);

		for (int i = 0, n = mesh->GetNumChunks(); i < n; i++)
			if (mesh->GetChunkMatrix(i).d.FlatDist2(center) < square(m_DrawRadius))
				mesh->DrawChunk(i, navNodeDefs::kNodeDrawTypeGeneric, &filter);
	}

	DrawActors();
#endif

	rmcSampleSimpleWorld::DrawClient();
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Physics bound loading

void drivingWorld::LoadBound(const char* fileName)
{
	if (!m_pFragWorld)
		return;

	phBound* bnd = phBound::Load(fileName);
	if (!bnd)
		return;

	phArchetype* arch = rage_new phArchetype;
	arch->SetBound(bnd);

	phInst* inst = rage_new phInst;
	inst->SetArchetype(arch);
	inst->SetMatrix(Mat34V(V_IDENTITY));

	phDemoObject* obj = m_SimplePhysObjs.Append() = rage_new phDemoObject;
	obj->SetInst(inst);

	m_pFragWorld->AddFixedObject(obj);
}

void drivingWorld::UnloadAllBounds()
{
	for (int i = 0, n = m_SimplePhysObjs.GetCount(); i < n; i++)
	{
		phDemoObject* obj = m_SimplePhysObjs[i];
		obj->GetPhysInst()->GetArchetype()->GetBound()->Release(true);
		m_pFragWorld->GetSimulator()->DeleteObject(obj->GetLevelIndex());
		m_pFragWorld->RemoveObject(obj);
		delete obj;
	}

	m_SimplePhysObjs.Resize(0);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Navigation data loading

void drivingWorld::LoadNavLevel(const char* fileName)
{
#if __BANK
	if (m_NavBank)
		m_NavBank->Truncate(0);
#endif

	if (navLevel::GetInstance().IsReady())
		static_cast<navLevelSimpleMesh&>(navLevel::GetInstance()).UnloadData();

	navLevelSimpleMesh& level = static_cast<navLevelSimpleMesh&>(navLevel::GetInstance());
	level.LoadData(fileName, NULL);
	if (level.GetMesh())
		level.GetMesh()->SetMaxNumCulledNodes(10240);

#if __BANK
	if (m_NavBank)
		navLevel::GetInstance().AddWidgets(*m_NavBank);
#endif
}

void drivingWorld::LoadNavRsc(const char* )
{
#if 0
#if __BANK
	if (m_NavBank)
		m_NavBank->Truncate(0);
#endif

	if (navLevel::GetInstance().IsReady())
		static_cast<navLevelSimpleMesh&>(navLevel::GetInstance()).UnloadData();

	navLevelSimpleMesh& level = static_cast<navLevelSimpleMesh&>(navLevel::GetInstance());
	level.StreamingEnable();
	level.LoadData(NULL, fileName);
	if (level.GetMesh())
		level.GetMesh()->SetMaxNumCulledNodes(10240);

#if __BANK
	if (m_NavBank)
		navLevel::GetInstance().AddWidgets(*m_NavBank);
#endif
#endif
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Scripting functions

drivingWorld* drivingWorld::sm_ScriptTargetWorld = NULL;

void drivingWorld::ScriptSetNumActors(int n)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SetNumActors(n);	}
void drivingWorld::ScriptGetActorPosition(int actor, Vector3& pos)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->GetActorPosition(actor, pos);	}
void drivingWorld::ScriptSetActorPosition(int actor, const Vector3& pos, bool probe)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SetActorPosition(actor, pos, probe);	}
void drivingWorld::ScriptSetActorGoal(int actor, const Vector3& pos, bool probe)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SetActorGoal(actor, pos, probe);	}
void drivingWorld::ScriptSetAllActorsPaused(bool pause)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SetAllActorsPaused(pause);	}
void drivingWorld::ScriptSetActorFlagPref(int actor, const char* flagName)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SetActorFlagPref(actor, flagName);	}
void drivingWorld::ScriptSetActorFlagWeight(int actor, float weight)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SetActorFlagWeight(actor, weight);	}
void drivingWorld::ScriptSetActorWidth(int actor, float width)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SetActorWidth(actor, width);	}
void drivingWorld::ScriptSetActorSpeed(int actor, float speed)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SetActorSpeed(actor, speed);	}
void drivingWorld::ScriptSelectActor(int actor)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SelectActor(actor);	}
void drivingWorld::ScriptSetCameraFollow(bool follow)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SetCameraFollow(follow);	}
void drivingWorld::ScriptSetCameraOffset(const Vector3& offset)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SetCameraOffset(offset);	}
bool drivingWorld::ScriptIsActorAtGoal(int actor)
{	Assert(sm_ScriptTargetWorld); return sm_ScriptTargetWorld->IsActorAtGoal(actor);	}
void drivingWorld::ScriptSetCameraPosition(const Vector3& pos)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SetCameraPosition(pos);	}
void drivingWorld::ScriptSetCameraEulersInDegrees(const Vector3& eulers)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SetCameraEulersInDegrees(eulers);	}
void drivingWorld::ScriptSetCameraLookAt(const Vector3& pos, float slerp)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SetCameraLookAt(pos, slerp);	}
void drivingWorld::ScriptSubmitActorQuery(int actor)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SubmitActorQuery(actor);	}
void drivingWorld::ScriptSetDrawOptions(bool enabled, float radius, const char* flagName)
{	Assert(sm_ScriptTargetWorld); sm_ScriptTargetWorld->SetDrawOptions(enabled, radius, flagName);	}

void drivingWorld::RegisterScriptingCommands()
{
	Assert(sm_ScriptTargetWorld == NULL);
	sm_ScriptTargetWorld = this;

	SCR_REGISTER_SECURE(SET_NUM_ACTORS, 0xefade0f,			drivingWorld::ScriptSetNumActors );
	SCR_REGISTER_SECURE(SET_ALL_ACTORS_PAUSED, 0x6f892f26,	drivingWorld::ScriptSetAllActorsPaused );

	SCR_REGISTER_SECURE(GET_ACTOR_POSITION, 0xbb447f36,		drivingWorld::ScriptGetActorPosition );
	SCR_REGISTER_SECURE(SET_ACTOR_POSITION, 0xcb786d24,		drivingWorld::ScriptSetActorPosition );
	SCR_REGISTER_SECURE(SET_ACTOR_GOAL, 0x5b815f07,			drivingWorld::ScriptSetActorGoal );
	SCR_REGISTER_SECURE(SET_ACTOR_WIDTH, 0x43bebdfc,			drivingWorld::ScriptSetActorWidth );
	SCR_REGISTER_SECURE(SET_ACTOR_SPEED, 0x9d78931,			drivingWorld::ScriptSetActorSpeed );
	SCR_REGISTER_SECURE(SET_ACTOR_FLAG_PREF, 0xed5ba17d,		drivingWorld::ScriptSetActorFlagPref );
	SCR_REGISTER_SECURE(SET_ACTOR_FLAG_WEIGHT, 0x969f9a7d,	drivingWorld::ScriptSetActorFlagWeight );
	SCR_REGISTER_SECURE(IS_ACTOR_AT_GOAL, 0xd340f097,			drivingWorld::ScriptIsActorAtGoal );
	SCR_REGISTER_SECURE(SUBMIT_ACTOR_QUERY, 0x6cddec9,		drivingWorld::ScriptSubmitActorQuery );

	SCR_REGISTER_SECURE(SELECT_ACTOR, 0xb12cf2,				drivingWorld::ScriptSelectActor );
	SCR_REGISTER_SECURE(SET_CAMERA_FOLLOW, 0xdab3705a,		drivingWorld::ScriptSetCameraFollow );
	SCR_REGISTER_SECURE(SET_CAMERA_OFFSET, 0xb675fcc7,		drivingWorld::ScriptSetCameraOffset );

	SCR_REGISTER_SECURE(SET_CAMERA_POSITION, 0xb12cd8c,		drivingWorld::ScriptSetCameraPosition );
	SCR_REGISTER_SECURE(SET_CAMERA_ORIENTATION, 0x486f4461,	drivingWorld::ScriptSetCameraEulersInDegrees );
	SCR_REGISTER_SECURE(SET_CAMERA_LOOK_AT, 0x1a48a0e8,		drivingWorld::ScriptSetCameraLookAt );
	SCR_REGISTER_SECURE(SET_DRAW_OPTIONS, 0x6b552f44,			drivingWorld::ScriptSetDrawOptions );
}

void drivingWorld::SetCameraPosition(const Vector3& pos)
{
	Matrix34 m;
	m.Set3x3(GetCamMgr().GetWorldMtx());
	m.d = pos;
	GetCamMgr().SetWorldMtx(m);
}

void drivingWorld::SetCameraEulersInDegrees(const Vector3& eulers)
{
	Matrix34 m;
	Vector3 tmp;
	tmp.Scale(eulers, DtoR);
	m.FromEulersXYZ(tmp);
	m.d = GetCamMgr().GetWorldMtx().d;
	GetCamMgr().SetWorldMtx(m);
}

void drivingWorld::SetCameraLookAt(const Vector3& pos, float slerp)
{
	Matrix34 m;
	m.d = GetCamMgr().GetWorldMtx().d;
	m.LookAt(pos);

	if (slerp < 1.0f - SMALL_FLOAT)
		m.Interpolate(GetCamMgr().GetWorldMtx(), m, slerp);

	GetCamMgr().SetWorldMtx(m);
}

void drivingWorld::SetNumActors(int n)
{
	m_SelectedActor = NULL;
	m_NumActors = n;

	for (int i = m_PrevNumActors; i < m_NumActors; i++)
	{
		m_Actors[i].SetCurrentPosition(VEC3_ZERO, true);
		m_Actors[i].SetPaused(m_AllPaused);
	}

	for (int i = m_NumActors; i < m_PrevNumActors; i++)
		m_Actors[i].Stop();

	m_PrevNumActors = m_NumActors;
}

void drivingWorld::SetActorPosition(int actor, const Vector3& pos, bool probe)
{
	Assert(actor >= 0 && actor < sm_MaxActors);
	m_Actors[actor].SetCurrentPosition(pos, probe);
}

void drivingWorld::GetActorPosition(int actor, Vector3& pos) const
{
	Assert(actor >= 0 && actor < sm_MaxActors);
	pos = m_Actors[actor].GetCurrentPosition();
}

void drivingWorld::SetActorGoal(int actor, const Vector3& pos, bool probe)
{
	Assert(actor >= 0 && actor < sm_MaxActors);
	m_Actors[actor].SetGoal(pos, probe);
}

void drivingWorld::SetAllActorsPaused(bool paused)
{
	m_AllPaused = paused;

	for (int i = 0; i < m_NumActors; i++)
		m_Actors[i].SetPaused(m_AllPaused);
}

void drivingWorld::SetActorFlagPref(int actor, const char* flagName)
{
	Assert(actor >= 0 && actor < sm_MaxActors);
	m_Actors[actor].SetFlagPref(flagName);
}

void drivingWorld::SetActorFlagWeight(int actor, float weight)
{
	Assert(actor >= 0 && actor < sm_MaxActors);
	m_Actors[actor].SetFlagWeight(weight);
}

void drivingWorld::SetActorWidth(int actor, float width)
{
	Assert(actor >= 0 && actor < sm_MaxActors);
	m_Actors[actor].SetWidth(width);
}

void drivingWorld::SetActorSpeed(int actor, float speed)
{
	Assert(actor >= 0 && actor < sm_MaxActors);
	m_Actors[actor].SetSpeed(speed);
}

void drivingWorld::SelectActor(int actor)
{
	drivingCar* newSelection = (actor >= 0 && actor < m_NumActors) ? m_Actors + actor : NULL;

#if __BANK
	if (newSelection && m_SelectedActor != newSelection)
	{
		m_ActorBank->Truncate(0);
		newSelection->AddWidgets(*m_ActorBank);
	}
#endif

	m_SelectedActor = newSelection;
}

bool drivingWorld::IsActorAtGoal(int actor) const
{
	Assert(actor >= 0 && actor < sm_MaxActors);
	return m_Actors[actor].IsAtGoal(0.01f);
}

void drivingWorld::SubmitActorQuery(int actor)
{
	Assert(actor >= 0 && actor < sm_MaxActors);
	return m_Actors[actor].Submit();
}

void drivingWorld::SetDrawOptions(bool enabled, float radius, const char* flagName)
{
	m_DrawMesh = enabled;
	m_DrawRadius = radius;

	m_DrawFlagIndex = 0;

	if (!m_MaterialFlags || !flagName)
		return;

	phMaterialFlags flag = MATERIALMGRFLAG.GetFlagByName(flagName);

	for (int i = 0, n = MATERIALMGRFLAG.GetNumFlags(); i < n; i++)
	{
		if (m_MaterialFlags[i] == flag)
		{
			m_DrawFlagIndex = i;
			return;
		}
	}
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Bank functions

#if __BANK

void drivingWorld::AddWidgetsClient()
{	
	// Let the navLevel add widgets to a new bank.
	m_NavBank = &BANKMGR.GetInstance().CreateBank("rage - Navigation Level");
	navLevel::GetInstance().AddWidgets(*m_NavBank);

	bkBank& bank = BANKMGR.GetInstance().CreateBank("Sample Driving");

	bank.AddButton("Load Entity", datCallback(MFA(drivingWorld::BankLoadEntity),this));
	bank.AddButton("Unload Entity", datCallback(MFA(drivingWorld::BankUnloadEntity),this));

	bank.AddButton("Load Nav Mesh", datCallback(MFA(drivingWorld::BankLoadNavLevel),this));
	bank.AddButton("Load Nav Resource", datCallback(MFA(drivingWorld::BankLoadNavRsc),this));
	bank.AddButton("Unload Nav Mesh", datCallback(MFA(drivingWorld::BankUnloadNavLevel),this));

	bank.AddButton("Load Bound File", datCallback(MFA(drivingWorld::BankLoadBound),this));
	bank.AddButton("Unload All Bounds", datCallback(MFA(drivingWorld::UnloadAllBounds),this));

	bank.AddToggle("Draw Nav Level", &m_DrawMesh);
	bank.AddSlider("Draw Radius", &m_DrawRadius, 0.f, 1000.f, 10.f);

	const char** flagNames = Alloca(const char*, MATERIALMGRFLAG.GetNumFlags() + 1);
	flagNames[0] = "Draw All";

	int n = 1;
	for (atMap<ConstString, phMaterialFlags>::ConstIterator i = MATERIALMGRFLAG.GetFlagIter(); i; ++i)
		flagNames[n++] = i.GetKey();

	bank.AddCombo("Draw Flag Only", &m_DrawFlagIndex, MATERIALMGRFLAG.GetNumFlags() + 1, flagNames);
	
	bank.AddToggle("Draw Paths", &m_DrawPaths);

	bank.AddSlider("Number of Actors", &m_NumActors, 0, sm_MaxActors, 1, datCallback(MFA(drivingWorld::BankChangedNumActors),this));

	bank.AddToggle("All Actors Paused", &m_AllPaused, datCallback(MFA(drivingWorld::BankAllPausedChanged),this));

	bank.AddToggle("Camera Follow Actor", &m_CameraFollowActor);

	bank.AddSlider("Draw Actor Radius", &m_DefaultRadius, 0.f, 10.f, 1.f, datCallback(MFA(drivingWorld::BankDefaultDimensionsChanged),this));

	m_ActorBank = &BANKMGR.GetInstance().CreateBank("Selected Car");
}

void drivingWorld::BankDefaultDimensionsChanged()
{
	navActorDimensions& dim = navActorCapabilities::GetDefault().GetDimensions();
	dim.SetHeight(m_DefaultHeight);
	dim.SetWidth(m_DefaultRadius * 2.0f);
}

void drivingWorld::BankAllPausedChanged()
{
	for (int i = 0; i < m_NumActors; i++)
		m_Actors[i].SetPaused(m_AllPaused);
}

void drivingWorld::BankChangedNumActors()
{
	SetNumActors(m_NumActors);
}

void drivingWorld::BankLoadNavLevel()
{
	char fileName[256];
	memset(fileName, 0, 256);

	if(BANKMGR.OpenFile(fileName, 256, "*.nav", false, "Navigation Level (*.nav)"))
	{
		LoadNavLevel(fileName);
	}
}

void drivingWorld::BankLoadBound()
{
	char fileName[256];
	memset(fileName, 0, 256);

	if(BANKMGR.OpenFile(fileName, 256, "*.bnd", false, "Physics Bound (*.bnd)"))
	{
		LoadBound(fileName);
	}
}

void drivingWorld::BankLoadNavRsc()
{
	char fileName[256];
	memset(fileName, 0, 256);

#if __XENON
	if(BANKMGR.OpenFile(fileName, 256, "*.xnm", false, "Navigation Resource (*.xsm)"))
		LoadNavRsc(fileName);
#elif __WIN32
	if(BANKMGR.OpenFile(fileName, 256, "*.wnm", false, "Navigation Resource (*.wsm)"))
		LoadNavRsc(fileName);
#endif
}

void drivingWorld::BankUnloadNavLevel()
{
	StopActors();

	static_cast<navLevelSimpleMesh&>(navLevel::GetInstance()).UnloadData();
}

void drivingWorld::BankLoadEntity()
{
	if (m_pLastFragDrawableEntityLoaded)
		SMPWORLDEMGR.RemoveEntity(m_pLastFragDrawableEntityLoaded);

	rmcSampleSimpleWorld::BankLoadFragDrawable();
}

void drivingWorld::BankUnloadEntity()
{
	if (m_pLastFragDrawableEntityLoaded)
		SMPWORLDEMGR.RemoveEntity(m_pLastFragDrawableEntityLoaded);

	m_pLastFragDrawableEntityLoaded = NULL;
}

#endif
