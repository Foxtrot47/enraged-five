// 
// sample_driving/sample_driving.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "drivingworld.h"
#include "system/main.h"

/*
PURPOSE
	sample_aiworld is a simple demonstration of pathfinding in a "real-world"
	setting.  It is capable of loading visible geometry and physics bounds
	from entity.type files or from RDR-style rsChild.txt files.  It can load
	navigation data from .nav files, navigation meshes grouped into a nav_main.xml
	file and from streaming navigation mesh resource files.

	Pathfinding is carried out by a variable number of simple actors that
	wander aimlessly.  Actors are drawn as capsules and their goals as spheres.
	You may click on an actor to select him, and then may right-click to set a
	pathfinding goal for the actor.  Control-right-clicking teleports the actor
	to the place you click.  If no actor is selected it will affect actor 0,
	which makes it easy to find an actor to play with, given that they spawn at
	random locations in the level.  Middle-click to deselect the actor and have
	it start wandering again.
	
	The "Sample AIWorld" bank provides buttons to load various types of
	visible and navigation data and widgets to control debug drawing.
	
	If an actor is selected, you may change its	properties in the
	"Sample AIWorld Selected Actor" bank.

	Various navigation parameters and drawing options can be fiddled with in the
	"rage - Navigation Level" bank once a navigation level has been loaded.

	Scripting is supported to demonstrate basic navmesh functionality.  Scripts
	can be run through the "rage - Script" bank.  Some sample scripts intended
	to run in the aiPlayground (using the second example command line) are in
	t:\rage\assets\sample_aiworld\scripts

	Command line options:

	-path		The asset root.

	-threadnav	Enable running navigation in its own thread.  This is a big performance win on
				a dual-core/hyperthreaded PCs and on Xenon.

	-level		The path to an entity.type file to load visible geometry and physics bounds.

	-nav		The path to a .nav file to load navigation data from.

	-navrsc		The path to a .xsm or .wsm file from which to load navigation data.
*/

int Main()
{
	ragesamples::drivingWorld* drivingWorld = rage_new ragesamples::drivingWorld;

	drivingWorld->Init();
	drivingWorld->UpdateLoop();
	drivingWorld->Shutdown();

	delete drivingWorld;

	return 0;
}
