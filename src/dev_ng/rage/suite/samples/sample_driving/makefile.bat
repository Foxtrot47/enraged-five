set ARCHIVE=driving
set FILES=drivingcar drivingworld drivingmaterial
set TESTERS=sample_driving

set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples
set XPROJ=%XPROJ% %RAGE_DIR%\suite\src %RAGE_DIR%\suite\samples %RAGE_DIR%\script\src %RAGE_DIR%\suite\tools %RAGE_DIR%\suite\tools\cli

set SAMPLE_LIBS=sample_file %RAGE_SAMPLE_GRCORE_LIBS% gizmo
set LIBS=%SAMPLE_LIBS% devcam %RAGE_CORE_LIBS% %RAGE_GFX_LIBS% vieweraudio cliptools

set LIBS_AUDIO=%RAGE_AUD_LIBS% vieweraudio
set LIBS_CREATURE=%RAGE_CR_LIBS% %RAGE_SUITE_CR_LIBS%
set LIBS_PHYSICS=%RAGE_PH_LIBS%
set LIBS_GRAPHICS=devcam %RAGE_GFX_LIBS%
set LIBS_CORE=%RAGE_CORE_LIBS% spatialdata %RAGE_SAMPLE_GRCORE_LIBS%
set LIBS_BASE_AI=
set LIBS_SIMPLEWORLD=sample_simpleworld sample_physics sample_fragment sample_rmcore init rmptfx grshadowmap breakableglass phglass fragment 
set LIBS_SIMPLEWORLD=%LIBS_SIMPLEWORLD% cloth grrope grpostfx event pheffects eventtypes script scriptgui cliptools

set LIBS_sample_driving=%LIBS_CORE% %LIBS_GRAPHICS% %LIBS_BASE_AI% %LIBS_CREATURE% %LIBS_PHYSICS% %LIBS_SIMPLEWORLD% %LIBS_AUDIO%
