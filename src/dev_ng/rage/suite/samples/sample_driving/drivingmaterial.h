// 
// drivingWorld/drivingMaterial.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_AIWORLD_drivingMaterial_H
#define SAMPLE_AIWORLD_drivingMaterial_H

#include "ainav/materialflagged.h"
#include "ainavigation/materialmanager.h"

namespace rage
{
	class fiAsciiTokenizer;
	class phMaterial;
}

//-----------------------------------------------------------------------------

namespace ragesamples
{

/*
PURPOSE
	This class implements the navMaterial interface for per-polygon
	materials in the navigation mesh.  This specific class doesn't
	really add any functionality to navMaterialFlagged other than
	defining the enumeration for the flags used by the RDR sample
	assets.
*/
class drivingMaterial : public rage::ai::navMaterialFlagged
{
public:
	// PURPOSE: Defines the flag name tokens which are then
	// changed into enumerations and string constants with
	// some macro magic below.
	#define SAG_MATERIAL_FLAGS				\
		SAG_MATERIAL_FLAG(GENERICSPAWN)		\
		SAG_MATERIAL_FLAG(HORSESPAWN)		\
		SAG_MATERIAL_FLAG(ANIMALSPAWN)		\
		SAG_MATERIAL_FLAG(PEDSPAWN)			\
		SAG_MATERIAL_FLAG(NOSPAWN)			\
		SAG_MATERIAL_FLAG(PEDPATH)			\
		SAG_MATERIAL_FLAG(HORSEPATH)		\
		SAG_MATERIAL_FLAG(VEHICLEPATH)		\
		SAG_MATERIAL_FLAG(GENERICPATH)		\
		SAG_MATERIAL_FLAG(ANIMALPATH)

	// PURPOSE: Macro magic that assigns bit indicies to the flags above.
	#define SAG_MATERIAL_FLAG(x) FLAG_AI_ ## x ## _BIT,
		enum { SAG_MATERIAL_FLAGS_FIRST_BIT = -1, SAG_MATERIAL_FLAGS };
	#undef SAG_MATERIAL_FLAG

	// PURPOSE: Macro magic that assigns bit masks to the bit indicies above.
	#define SAG_MATERIAL_FLAG(x) FLAG_AI_ ## x = (1 << FLAG_AI_ ## x ## _BIT),
		enum { SAG_MATERIAL_FLAGS };
	#undef SAG_MATERIAL_FLAG

	// PURPOSE: Uses macro magic to map string constants to the bit masks above.
	static void RegisterMaterialFlags();

	// PURPOSE: Our simpe-RTTI bitmask.
	static const rage::u32 kMaterialTypeSample = kMaterialTypeFlagged;

	// PURPOSE: Sets up the simple-RTTI, allowing derived classes to specify their
	// extra bits.
	drivingMaterial(rage::u32 typeFlags = 0) : navMaterialFlagged(typeFlags | kMaterialTypeSample) { }

	// PURPOSE: Resource constructor that uses the simple-RTTI to verify that
	// we're constructing the right type of object.
	drivingMaterial(rage::datResource& rsc) : navMaterialFlagged(rsc)
	{	Assert(GetTypeFlags() & kMaterialTypeSample);	}
};

// PURPOSE: A factory for drivingMaterials.
class drivingMaterialManager : public rage::ai::navGDFMaterialManager
{
public:
	// PURPOSE: Default constructor creates default material with drivingMaterial default constructor.
	drivingMaterialManager();

	// PURPOSE: Clones the given material to use as the default material.
	drivingMaterialManager(const rage::ai::navMaterial* defaultMtl);

	// PURPOSE: Resource constructor properly fixes up the vtable for all of the contained drivingMaterials.
	drivingMaterialManager(rage::datResource &rsc);

	// PURPOSE: Deletes the default material.
	virtual ~drivingMaterialManager();

	// PURPOSE: Creates an AI material using the information we care about in the physics
	// material.  In this sample we just use the flags, although we could use any of the
	// physics properties in the material.
	virtual int CreateMaterial(const rage::phMaterial &physMtrl, rage::phMaterialFlags flags);

	// PURPOSE: Returns the material index for the material, cloning it and adding it to
	// the material manager if not already present.
	virtual int CreateMaterial(const rage::ai::navMaterial &src);

	// PURPOSE: Clones the material.
	virtual rage::ai::navMaterial* ConstructMaterial(const rage::ai::navMaterial& mtrl) const;

	// PURPOSE: Loads the material section from a nav mesh ASCII file.
	virtual void LoadMaterial(rage::fiAsciiTokenizer &tok);

	// PURPOSE: Saves the material section to a nav mesh ASCII file.
	virtual void SaveMaterial(rage::fiAsciiTokenizer &tok, int i) const;

	// PURPOSE: Tests for material equality.  In this case we just check the flags.
	virtual bool CompareMaterials(const rage::ai::navMaterial &m1, const rage::ai::navMaterial &m2) const;

	// PURPOSE: Clones the material manager and all contained materials.  Used during
	// resource construction to consolidate materials loaded from multiple ASCII files
	// into a single resource heap.
	virtual navMaterialManager* Clone() const;
};

}

#endif // ifndef SAMPLE_AIWORLD_drivingMaterial_H
