// 
// drivingWorld/drivingMaterial.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "drivingMaterial.h"

#include "file/token.h"
#include "phcore/materialmgrflag.h"

using namespace rage;
using namespace ai;
using namespace ragesamples;


//-----------------------------------------------------------------------------


void drivingMaterial::RegisterMaterialFlags()
{
#define SAG_MATERIAL_FLAG(x) MATERIALMGRFLAG.RegisterFlag("ai" #x, FLAG_AI_ ## x);
	SAG_MATERIAL_FLAGS
#undef SAG_MATERIAL_FLAG
}


//-----------------------------------------------------------------------------

drivingMaterialManager::drivingMaterialManager()
{
	m_DefaultMaterial = rage_new drivingMaterial;
}

drivingMaterialManager::drivingMaterialManager(const navMaterial* defaultMtl)
{
	m_DefaultMaterial = defaultMtl ? ConstructMaterial(*defaultMtl) : NULL;
}

drivingMaterialManager::drivingMaterialManager(datResource &rsc) :
	navGDFMaterialManager(rsc, false)
{
	for(int i = 0; i < m_Materials.GetCount(); i++)
	{
		rsc.PointerFixup(m_Materials[i].ptr);
		new (m_Materials[i]) drivingMaterial(rsc);
	}
}

drivingMaterialManager::~drivingMaterialManager()
{
	delete m_DefaultMaterial;

	// Note: this member variable is in the base class, so clearing out the
	// variable after deleting is probably a good idea in this case. /FF
	m_DefaultMaterial = NULL;
}


navMaterialManager* drivingMaterialManager::Clone() const
{
	drivingMaterialManager* cloned = rage_new drivingMaterialManager(m_DefaultMaterial);
	cloned->CloneMaterials(*this);
	return cloned;
}


int drivingMaterialManager::CreateMaterial(const phMaterial &, phMaterialFlags flags)
{
	drivingMaterial m;
	m.SetFlags(flags);
	return CreateMaterial(m);
}

int drivingMaterialManager::CreateMaterial(const ai::navMaterial &src)
{
	int index = FindMaterial(src);
	if(index >= 0)
		return index;
	return AddMaterial(src);
}

ai::navMaterial* drivingMaterialManager::ConstructMaterial(const ai::navMaterial &mtrl) const
{
	Assert(mtrl.GetTypeFlags() & drivingMaterial::kMaterialTypeSample);
	return rage_new drivingMaterial(static_cast<const drivingMaterial&>(mtrl));
}

bool drivingMaterialManager::CompareMaterials(const ai::navMaterial &m1, const ai::navMaterial &m2) const
{
	Assert(m1.GetTypeFlags() & drivingMaterial::kMaterialTypeSample);
	Assert(m2.GetTypeFlags() & drivingMaterial::kMaterialTypeSample);
	return (static_cast<const drivingMaterial&>(m1) == static_cast<const drivingMaterial&>(m2));
}

void drivingMaterialManager::LoadMaterial(fiAsciiTokenizer &tok)
{
	// We use SAGMaterial token here because we're using RDR nav meshes
	// which are built using SAGMaterials.
	tok.MatchToken("SAGMaterial");

	drivingMaterial *mtrl = rage_new drivingMaterial;
	mtrl->Load(tok);
	m_Materials.Append() = mtrl;
}

void drivingMaterialManager::SaveMaterial(fiAsciiTokenizer &tok, int i) const
{
	// We use SAGMaterial token here because we're using RDR nav meshes
	// which are built using SAGMaterials.
	tok.StartLine();
	tok.PutStr("SAGMaterial");
	tok.EndLine();

	Assert(m_Materials[i]);

	Assert(m_Materials[i]->GetTypeFlags() & drivingMaterial::kMaterialTypeSample);
	static_cast<const drivingMaterial*>(m_Materials[i].ptr)->Save(tok);
}
