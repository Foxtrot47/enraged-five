// 
// sample_aiworld/drivingCar.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_AIWORLD_drivingCar_H
#define SAMPLE_AIWORLD_drivingCar_H

#include "ainav/actorcap.h"
#include "ainav/locus.h"
#include "ainav/pathexec.h"
#include "ainav/materialflagged.h"
#include "aiveh/drivepathturns.h"
#include "aiveh/steerstrl.h"
#include "aiveh/strlmulti.h"
#include "data/base.h"
#include "system/typeinfo.h"
#include "vector/color32.h"

namespace rage 
{ 
	namespace ai 
	{ 
		class navQueryQuickestPath;
		class navSpaceMesh;
		class navLevelSimpleMesh; 
	} 
}

namespace ragesamples
{

/*
PURPOSE:
	A material filter lets you specify which materials should be considered
	navigable by the pathfinding engine as well as any preferred materials
	and their weight (scale value when calculating the distance to cross).

	This is a simple example that only allows you to specify two sets of
	material flags, one for the required flags and one for the preferred flags,
	and a single weight for the non-preferred materials.  You could write your own
	that has separate weights for different materials.
 */

struct drivingMaterialFilter : public rage::ai::navMaterialFilter
{
	// PURPOSE: Initializes the filter with the preferred and required
	// set of material flags and the weight for non-preferred flags.
	drivingMaterialFilter(rage::phMaterialFlags reqFlags = 0, 
		rage::phMaterialFlags prefFlags = 0, float weight = 1.0f) :
		m_RequiredFlags(reqFlags), m_PreferredFlags(prefFlags), m_Weight(weight) { }

	/*
	PURPOSE
		This function should return 'true' if the filter should accept
		the material, 'false' otherwise.
	PARAMS
		mtrl		- An AI material. The pointer may be NULL.
	RETURNS
		'true' if the filter lets the material through.
	*/
	virtual bool AcceptMaterial(const rage::ai::navMaterial *mtrl) const
	{
		return (!mtrl || m_RequiredFlags == 0 || (mtrl->GetTypeFlags() & rage::ai::navMaterialFlagged::kMaterialTypeFlagged) == 0 ||
			    SafeCast(const rage::ai::navMaterialFlagged, mtrl)->CheckFlags(m_RequiredFlags));
	}

	/*
	PURPOSE
		This function should return 'true' if the material is preferred.
	PARAMS
	mtrl		- An AI material. The pointer may be NULL.
	RETURNS
		'true' if the filter prefers this material.
	NOTES
		This is used by the path postprocessor when trying to find shortcuts
		between nodes on the path.  The postprocessor will not take shortcuts
		that causes the actor to leave a preferred material earlier or enter
		a preferred material later than the original path.
	*/
	virtual bool IsPreferred(const rage::ai::navMaterial *mtrl) const
	{
		return (mtrl && (mtrl->GetTypeFlags() & rage::ai::navMaterialFlagged::kMaterialTypeFlagged) &&
			SafeCast(const rage::ai::navMaterialFlagged, mtrl)->CheckFlags(m_PreferredFlags));
	}

	/*
	PURPOSE
		This function returns a weighting factor used to scale the cost
		associated with traversal between two navSpaceNodes.
	PARAMS
		mtrl		- An AI material. The pointer may be NULL.
	RETURNS
		A distance scale factor greater than 1 for non-preferred
		materials and a value of 1 for preferred materials.
	NOTE
		Returning a value less than 1 may cause A* to return a non-optimal
		path.
	*/
	virtual float GetMaterialWeight(const rage::ai::navMaterial *mtrl) const
	{
		if (!mtrl || m_PreferredFlags == 0 || (mtrl->GetTypeFlags() & rage::ai::navMaterialFlagged::kMaterialTypeFlagged) == 0 ||
			SafeCast(const rage::ai::navMaterialFlagged, mtrl)->CheckFlags(m_PreferredFlags))
			return 1.0f;
		else
			return m_Weight;
	}

	// PURPOSE: A set of flags, at least one of which is required for the
	// actor to consider crossing.
	rage::phMaterialFlags m_RequiredFlags;

	// PURPOSE: A set of flags, at least one of which is required for the
	// actor to consider the material preferred.
	rage::phMaterialFlags m_PreferredFlags;

	// PURPOSE: The weight given for materials not matching the preffered flags.
	// If you assign a value of 10, then the pathfinder will consider the
	// non-preferred materials to cost 10 times as much to traverse.
	float m_Weight;
};

	
/*
PURPOSE
	A simple pathfinding agent that wanders around the navigation level.  It
	performs rudimentary collision checks to prevent walking through walls
	and it probes vertically to stick to the ground or go over short obstacles.
*/

struct drivingCar : public rage::datBase
{
	// PURPOSE: Default constructor.
	drivingCar();

	// PURPOSE: Initialize the actor, creating a navQuery object.
	void Init(const rage::phMaterialFlags* materialFlags);

	// PURPOSE: Shutdown the actor, canceling any queries and destroying
	// the navQuery object.
	void Shutdown();

	// PURPOSE: Call each frame to move along path and collide with stuff.
	bool Update();

	// PURPOSE: Stop the query.
	void Stop();

	// PURPOSE:	(Re-)submit the query.
	void Submit();

	// PURPOSE: Draw the actor and its goal as boring debug primatives.
	void Draw(bool drawPath, bool selected);

	// PURPOSE: Move the actor along its path.
	void UpdateActor(float timeStep);

	// PURPOSE: Helper function to move towards a point with collisions.
	bool MoveTowards(const rage::Vector3& dest);

	// PURPOSE: Sets the current position.
	// PARAMS
	//		probe - If true will probe the physics bound to set the Y coordinate on the ground.
	void SetCurrentPosition(const rage::Vector3& pos, bool probe);

	// PURPOSE: Returns the current positions.
	const rage::Vector3& GetCurrentPosition() const
	{	return m_CurrentPosition;	}

	// PURPOSE: Sets the goal position.
	// PARAMS
	//		probe - If true will probe the physics bound to set the Y coordinate on the ground.
	void SetGoal(const rage::Vector3& pos, bool probe);

	const rage::Vector3& GetGoal() const
	{	return m_GoalPosition;	}

	// PURPOSE: Pauses/unpauses the actor.
	void SetPaused(bool pause)
	{	m_Paused = pause;	}

	// PURPOSE: Sets the preferred material by name for the actor to path on.
	void SetFlagPref(const char* flagName);

	// PURPOSE: Sets the scale factor for the distance calculated by the
	// pathfinder when crossing non-preferred materials.
	void SetFlagWeight(float weight)
	{	m_PathFilter.m_Weight = weight;	}

	// PURPOSE: Sets the actor's width.
	void SetWidth(float width)
	{	m_Capabilities.GetDimensions().SetWidth(width);	}

	// PURPOSE: Sets the actor's speed.
	void SetSpeed(float speed)
	{	m_Speed = speed;	}

	// PURPOSE: Returns true if the actor is within epsilon distance of its goal.
	bool IsAtGoal(float epsilon) const;

	// PURPOSE: Returns the actor's height.
	float GetHeight() const
	{	return m_Capabilities.GetDimensions().GetHeight();	}

	// PURPOSE: Returns the actor's width.
	float GetWidth() const
	{	return m_Capabilities.GetDimensions().GetWidth();	}

	// PURPOSE: Returns the actor's radius.
	float GetRadius() const
	{	return m_Capabilities.GetDimensions().GetRadius();	}

	void SetCurrentHeading(const rage::Vector3& heading)
	{	m_CurrentHeading = heading;	}

	void SetGoalHeading(const rage::Vector3& heading)
	{	m_GoalHeading = heading;	}

#if __BANK
	void AddWidgets(rage::bkBank& bank);
	void BankDimensionsChanged();
#endif

private:
	// PURPOSE:	Query object to submit to the navigation system.
	rage::ai::navQueryQuickestPath*	m_Query;

	// PURPOSE:	navLocus representing the start of the search.
	rage::ai::navPoint				m_StartLocus;

	// PURPOSE:	navLocus representing the goal of the search.
	rage::ai::navPoint				m_GoalLocus;

	// PURPOSE: The current position of this actor.
	rage::Vector3					m_CurrentPosition;

	// PURPOSE: The current position of this actor.
	rage::Vector3					m_GoalPosition;

	rage::Vector3					m_CurrentHeading;
	rage::Vector3					m_GoalHeading;

	// PURPOSE: The color to draw this actor.
	rage::Color32					m_Color;

	// PURPOSE: Stores driving path spans.
	rage::aivDrivePathWithTurns		m_DrivePathTurns;
	rage::aivDrivePathSTRLMultiSeg	m_DrivePathMulti;

	// PURPOSE: Calculates race line.
	rage::aivSteerSTRL			m_SteerSTRL;
	rage::aivSteerMulti			m_SteerMulti;

	// PURPOSE: The speed in m/s this actor moves.
	float							m_Speed;

	// PURPOSE: The radius of this actor.
	float							m_Radius;

	// PURPOSE: The height of this actor.
	float							m_Height;

	// PURPOSE: Stores the radius and height for the nav system to use.
	rage::ai::navActorCapabilities	m_Capabilities;

	// PURPOSE: The maximum change in elevation the simple mover will step up.
	float							m_MaxStepUp;

	// PURPOSE: When paused the actor stops following its path.
	bool							m_Paused;

	// PURPOSE: The material filter used when pathfinding.
	drivingMaterialFilter			m_PathFilter;

	// PURPOSE: Array used to look up flags from indicies.  Not owned.
	const rage::phMaterialFlags*	m_MaterialFlags;

	// PURPOSE: The index into m_MaterialFlags of the flags required
	// for the actor to consider a material preferred.
	int								m_PreferredFlagIndex;

private:
	// Unimplemented.
	drivingCar(const drivingCar&);
	drivingCar& operator=(const drivingCar&);
};

} // namespace ragesamples

#endif // ifndef SAMPLE_AIWORLD_drivingCar_H
