@echo off

if not defined RS_TOOLSCONFIG (
	@echo.
	call setenv
	@echo.
)

call %RS_TOOLSROOT%\script\util\projGen\sync.bat
call %RS_TOOLSROOT%\script\util\projGen\generate.bat %RS_TOOLSCONFIG%\projgen\unittest.build %cd%\sample_netunittest.makefile.txt
call %RS_TOOLSROOT%\script\util\projGen\generate.bat %RS_TOOLSCONFIG%\projgen\unittest.build %cd%\sample_netunittestconsole.makefile.txt
call %RS_TOOLSROOT%\script\util\projgen\rebuildGameLibrary.bat %cd%\sample_netunittest.slndef


pause
