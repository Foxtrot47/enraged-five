

#include "netUnitTestFramework.h"

#if UNITTEST_ENABLE_RAGE_UTF8

#include "data/rson.h"
//#include "string/utf8.h"
#include "system/nelem.h"

#include <string>

using namespace rage;

NET_TEST_CLASS(Test_rageUtf8)
{
	NET_TEST_INIT(Init_rageUtf8)
	{

	}

	NET_TEST_METHOD(TestUtf8Functions)
	{
		/*const char* values[] = {
			"abcdef",
			"abcde\xc3\xb1",
			"abcde\xe2\x82\xa1",
			"abcde\xf0\x90\x8c\xbc"
		};*/

		//netTestAssert(unicode::Utf8_Strlen(values[0]) == 6);
		//netTestAssert(unicode::Utf8_Strlen(values[1]) == 6);
		//netTestAssert(unicode::Utf8_Strlen(values[2]) == 6);
		//netTestAssert(unicode::Utf8_Strlen(values[3]) == 6);

		//netTestAssert(unicode::Utf8_ByteLength(values[0]) == 6);
		//netTestAssert(unicode::Utf8_ByteLength(values[1]) == 7);
		//netTestAssert(unicode::Utf8_ByteLength(values[2]) == 8);
		//netTestAssert(unicode::Utf8_ByteLength(values[3]) == 9);

		/*int pos = 0;
		char buf[255] = {0};
		int numBytes = 0;

		safecat(buf, "a");
		safecat(buf, "\xc3\xb1");
		safecat(buf, "\xe2\x82\xa1");
		safecat(buf, "\xf0\x90\x8c\xbc");*/

		// strlen + byte length
		/*netTestAssert(unicode::Utf8_Strlen(buf) == 4);
		netTestAssert(unicode::Utf8_ByteLength(buf) == 10);*/

		// next
		//numBytes = unicode::Utf8_Next(buf, &pos);
		//netTestAssert(numBytes == 1);
		//netTestAssert(pos == 1);
		////numBytes = unicode::Utf8_Next(buf, &pos);
		//netTestAssert(numBytes == 2);
		//netTestAssert(pos == 3);
		////numBytes = unicode::Utf8_Next(buf, &pos);
		//netTestAssert(numBytes == 3);
		//netTestAssert(pos == 6);
		////numBytes = unicode::Utf8_Next(buf, &pos);
		//netTestAssert(numBytes == 4);
		//netTestAssert(pos == 10);
		////numBytes = unicode::Utf8_Next(buf, &pos);
		//netTestAssert(numBytes == 0);
		//netTestAssert(pos == 10);

		//// prev
		////numBytes = unicode::Utf8_Previous(buf, &pos);
		//netTestAssert(numBytes == 4);
		//netTestAssert(pos == 6);
		////numBytes = unicode::Utf8_Previous(buf, &pos);
		//netTestAssert(numBytes == 3);
		//netTestAssert(pos == 3);
		////numBytes = unicode::Utf8_Previous(buf, &pos);
		//netTestAssert(numBytes == 2);
		//netTestAssert(pos == 1);
		////numBytes = unicode::Utf8_Previous(buf, &pos);
		//netTestAssert(numBytes == 1);
		//netTestAssert(pos == 0);

		//// next utf32
		////unicode::utf32 c;
		////c = unicode::Utf8_NextUtf32(buf, &pos);
		////netTestAssert(c == 0x61); // "a" code point is 97 == 0x61
		//netTestAssert(pos == 1);
		////c = unicode::Utf8_NextUtf32(buf, &pos);
		////netTestAssert(c == 0xF1); // "\xc3\xb1"code point is 241 == F1
		//netTestAssert(pos == 3);
		////c = unicode::Utf8_NextUtf32(buf, &pos);
		////netTestAssert(c == 0x20A1); // "\xe2\x82\xa1" code point is 8353 == 20A1
		//netTestAssert(pos == 6);
		////c = unicode::Utf8_NextUtf32(buf, &pos);
		////netTestAssert(c == 0x1033C); // "\xf0\x90\x8c\xbc" code point is 66364 == 1033C
		//netTestAssert(pos == 10);

		//// strchr
		//pos = 0;
		////unicode::Utf8_Strchr(buf, 0x20A1, &pos);
		//netTestAssert(pos == 2);
		//
		//// add more
		//safecat(buf, "\xe2\x82\xa1");
		//safecat(buf, "\xe2\x82\xa1");
		////netTestAssert(unicode::Utf8_Strlen(buf) == 6);

		//char searchBuf[10] = {0};
		//safecat(searchBuf, "\xe2\x82\xa1");
		//safecat(searchBuf, "\xf0\x90\x8c\xbc");
		////netTestAssert(unicode::Utf8_Strlen(searchBuf) == 2);

		//// strstr
		//int charPos = 0;
		////const char* result = unicode::Utf8_Strstr(buf, searchBuf, &charPos);
		////netTestAssert(result != NULL);
		//netTestAssert(charPos == 2);
		//netTestAssert(unicode::Utf8_Strlen(result) == 4);

		// get character
		//netTestAssert(unicode::Utf8_GetUtf32(buf, 0) == 0x61);
		//netTestAssert(unicode::Utf8_GetUtf32(buf, 1) == 0xF1);
		/*netTestAssert(unicode::Utf8_GetUtf32(buf, 2) == 0x20A1);
		netTestAssert(unicode::Utf8_GetUtf32(buf, 3) == 0x1033C);
		netTestAssert(unicode::Utf8_GetUtf32(buf, 4) == 0x20A1);
		netTestAssert(unicode::Utf8_GetUtf32(buf, 5) == 0x20A1);
		netTestAssert(unicode::Utf8_GetUtf32(buf, 6) == 0);
		netTestAssert(unicode::Utf8_GetUtf32(buf, 10000) == 0);*/
	}
	NET_TEST_METHOD_END

//	NET_TEST_METHOD(TestUtf8NullsAndEmpty)
//	{
//#if !USE_MS_UNIT_TEST_FRAMEWORK
//		netUnitTest::sm_IsExpectingFailures = true;
//#endif
//
//		int num = 0, delta = 0, length = 0;
//		//const char* result;
//		//unicode::utf32 c;
//
//		//netTestAssert(unicode::Utf8_IsValid(""));
//
//		/*c = unicode::Utf8_NextUtf32("", &num);
//		netTestAssert(c == 0);
//		c = unicode::Utf8_NextUtf32(NULL, &num);
//		netTestAssert(c == 0);
//		c = unicode::Utf8_NextUtf32(NULL, NULL);
//		netTestAssert(c == 0);
//
//		c = unicode::Utf8_GetUtf32("", 0);
//		netTestAssert(c == 0);
//		c = unicode::Utf8_NextUtf32(NULL, &num);
//		netTestAssert(c == 0);
//		c = unicode::Utf8_NextUtf32(NULL, NULL);
//		netTestAssert(c == 0);
//*/
//		num = 0;
//		//delta = unicode::Utf8_Previous("", &num);
//		netTestAssert(delta == 0);
//		netTestAssert(num == 0);
//		num = 0;
//		//delta = unicode::Utf8_Previous(NULL, &num);
//		netTestAssert(delta == 0);
//		netTestAssert(num == 0);
//		num = 0;
//		//delta = unicode::Utf8_Previous(NULL, NULL);
//		netTestAssert(delta == 0);
//		netTestAssert(num == 0);
//
//		num = 0;
//		//delta = unicode::Utf8_Next("", &num);
//		netTestAssert(delta == 0);
//		netTestAssert(num == 0);
//		num = 0;
//		//delta = unicode::Utf8_Next(NULL, &num);
//		netTestAssert(delta == 0);
//		netTestAssert(num == 0);
//		num = 0;
//		//delta = unicode::Utf8_Next(NULL, NULL);
//		netTestAssert(delta == 0);
//		netTestAssert(num == 0);
//
//		//length = unicode::Utf8_ByteLength("");
//		netTestAssert(length == 0);
//		//length = unicode::Utf8_ByteLength(NULL);
//		netTestAssert(length == 0);
//
//		num = 0;
//		//result = unicode::Utf8_Strchr("", 0, &num);
//		netTestAssert(num == 0);
//		//netTestAssert(result == 0);
//		num = 0;
//		//result = unicode::Utf8_Strchr(NULL, 0, &num);
//		netTestAssert(num == 0);
//		netTestAssert(result == 0);
//		num = 0;
//		//result = unicode::Utf8_Strchr(NULL, 0, NULL);
//		netTestAssert(num == 0);
//		netTestAssert(result == 0);
//
//		const char* str = "test";
//		num = 0;
//		//result = unicode::Utf8_Strstr(str, "", &num);
//		netTestAssert(num == 0);
//		netTestAssert(result == str);
//		num = 0;
//		//result = unicode::Utf8_Strstr("", "", &num);
//		netTestAssert(num == 0);
//		netTestAssert(result == 0);
//		num = 0;
//		//result = unicode::Utf8_Strstr(NULL, "", &num);
//		netTestAssert(num == 0);
//		netTestAssert(result == 0);
//		num = 0;
//		//result = unicode::Utf8_Strstr(NULL, "", NULL);
//		netTestAssert(num == 0);
//		netTestAssert(result == 0);
//
//#if !USE_MS_UNIT_TEST_FRAMEWORK
//		netUnitTest::sm_IsExpectingFailures = false;
//#endif
//	}
//	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestUtf8ValidCharacters)
	{
		//const char* values[] = {
		//	"a",						// 0 - 'Valid ASCII'								
		//	"\xc3\xb1",					// 1 - 'Valid 2 Octet Sequence'					
		//	"\xc3\x28",					// 2 - 'Invalid 2 Octet Sequence'					
		//	"\xa0\xa1",					// 3 - 'Invalid Sequence Identifier'				
		//	"\xe2\x82\xa1",				// 4 - 'Valid 3 Octet Sequence'					
		//	"\xe2\x28\xa1",				// 5 - 'Invalid 3 Octet Sequence (in 2nd Octet)'	
		//	"\xe2\x82\x28",				// 6 - 'Invalid 3 Octet Sequence (in 3rd Octet)'	
		//	"\xf0\x90\x8c\xbc",			// 7 - 'Valid 4 Octet Sequence'					
		//	"\xf0\x28\x8c\xbc",			// 8 - 'Invalid 4 Octet Sequence (in 2nd Octet)'	
		//	"\xf0\x90\x28\xbc",			// 9 - 'Invalid 4 Octet Sequence (in 3rd Octet)'	
		//	"\xf0\x28\x8c\x28",			// 10 - 'Invalid 4 Octet Sequence (in 4th Octet)'	
		//};

		/*netTestAssert(unicode::Utf8_IsValid(values[0]));
		netTestAssert(unicode::Utf8_IsValid(values[1]));
		netTestAssert(!unicode::Utf8_IsValid(values[2]));
		netTestAssert(!unicode::Utf8_IsValid(values[3]));
		netTestAssert(unicode::Utf8_IsValid(values[4]));
		netTestAssert(!unicode::Utf8_IsValid(values[5]));
		netTestAssert(!unicode::Utf8_IsValid(values[6]));
		netTestAssert(unicode::Utf8_IsValid(values[7]));
		netTestAssert(!unicode::Utf8_IsValid(values[8]));
		netTestAssert(!unicode::Utf8_IsValid(values[9]));
		netTestAssert(!unicode::Utf8_IsValid(values[10]));*/
	}
	NET_TEST_METHOD_END

}
NET_TEST_CLASS_END(Test_rageUtf8)

#endif // UNITTEST_ENABLE_RAGE_RSON