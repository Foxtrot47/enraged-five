

#include "netUnitTestFramework.h"

#include "diag/seh.h"
#include "net/net.h"
#include "net/http.h"
#include "string/stringutil.h"

using namespace rage;

NET_TEST_CLASS(Test_netHttpRequest)
{
    NET_TEST_INIT(Init_netHttpRequest)
    {

    }


    // HTTP status code parsing
    NET_TEST_METHOD(ParseStatusCode)
    {
        //bool result = false;
        //const char* header = "HTTP/1.1 200 Ok" CRLF;
        //netHttpStatusCode statusCode = NET_HTTPSTATUS_INVALID;
		
        //result = netHttpRequest::ParseStatusCodeFromHeader(header, strlen(header), &statusCode);

        //netTestAssert(result);
        //netTestAssert(statusCode == NET_HTTPSTATUS_OK);

        //header = "HTTP/1.1 500 Internal Server Error" CRLF;
        //statusCode = NET_HTTPSTATUS_INVALID;

        //result = netHttpRequest::ParseStatusCodeFromHeader(header, strlen(header), &statusCode);

        //netTestAssert(result);
        //netTestAssert(statusCode == NET_HTTPSTATUS_INTERNAL_SERVER_ERROR);

        //// Test garbage header
        //header = "This is not a valid http header" CRLF;
        //statusCode = NET_HTTPSTATUS_INVALID;

        //result = netHttpRequest::ParseStatusCodeFromHeader(header, strlen(header), &statusCode);

        //netTestAssert(!result);
        //netTestAssert(statusCode == NET_HTTPSTATUS_INVALID);


        //// Test another garbage header, without CRLF
        //header = "This is not a valid http header";
        //statusCode = NET_HTTPSTATUS_INVALID;

        //result = netHttpRequest::ParseStatusCodeFromHeader(header, strlen(header), &statusCode);

        //netTestAssert(!result);
        //netTestAssert(statusCode == NET_HTTPSTATUS_INVALID);
    }
    NET_TEST_METHOD_END

	NET_TEST_METHOD(UrlEncodingSpaces)
	{
		//char bufA[1024];
		//char bufB[sizeof(bufA) + 1];
		//char bufC[3 * sizeof(bufA)];
		//char dstBuf[3 * sizeof(bufA)];
		//char bufSmall[16];

		//unsigned numConsumed = 0;
		//unsigned dstBufLen, lenB, lenSmall, lenC;

		//////////////////////////////////////////
		//// one fish+two fish+red fish+blue fish
		//////////////////////////////////////////
		//safecpy(bufA, "https://localhost/one fish+two fish+red fish+blue fish");

		//lenB = sizeof(bufB);
		//dstBufLen = sizeof(dstBuf);
		////netTestAssert(netHttpRequest::UrlEncode(dstBuf, &dstBufLen, bufA, istrlen(bufA), &numConsumed, ":/", true));
		//netTestAssert(netHttpRequest::UrlDecode(bufB, &lenB, dstBuf, istrlen(dstBuf), &numConsumed));
		//netTestAssert(stricmp(bufA, bufB) == 0);

		//lenB = sizeof(bufB);
		//dstBufLen = sizeof(dstBuf);
		////netTestAssert(netHttpRequest::UrlEncode(dstBuf, &dstBufLen, bufA, istrlen(bufA), &numConsumed, ":/", false));
		//netTestAssert(netHttpRequest::UrlDecode(bufB, &lenB, dstBuf, istrlen(dstBuf), &numConsumed));
		//netTestAssert(stricmp(bufA, bufB) == 0);

		//////////////////////////////////////////
		//// trim
		//////////////////////////////////////////
		//safecpy(bufA, "      test string       ");

		//lenB = sizeof(bufB);
		//dstBufLen = sizeof(dstBuf);
		////netTestAssert(netHttpRequest::UrlEncode(dstBuf, &dstBufLen, bufA, istrlen(bufA), &numConsumed, NULL, true));
		//netTestAssert(netHttpRequest::UrlDecode(bufB, &lenB, dstBuf, istrlen(dstBuf), &numConsumed));
		//netTestAssert(stricmp(bufA, bufB) == 0);

		//StringTrim(bufA, bufA, sizeof(bufA));

		//lenB = sizeof(bufB);
		//dstBufLen = sizeof(dstBuf);
		////netTestAssert(netHttpRequest::UrlEncode(dstBuf, &dstBufLen, bufA, istrlen(bufA), &numConsumed, NULL, true));
		//netTestAssert(netHttpRequest::UrlDecode(bufB, &lenB, dstBuf, istrlen(dstBuf), &numConsumed));
		//netTestAssert(stricmp(bufA, bufB) == 0);

		//////////////////////////////////////////
		//// small buf - should assert
		//////////////////////////////////////////
		//safecpy(bufA, "This is a string longer than 16 characters.");

		//lenSmall = sizeof(bufSmall);

		//netTestBeginExpectAssert();
		//netTestAssert(netHttpRequest::UrlEncode(bufSmall, &lenSmall, bufA, istrlen(bufA), &numConsumed, NULL));
		//netTestEndExpectAssert(1);

		//// Ensure it failed
		//netTestAssert(stricmp(bufA, bufSmall) != 0);
		//netTestAssert(istrlen(bufSmall) == lenSmall);
		//netTestAssert(lenSmall + 1 == sizeof(bufSmall));

		//////////////////////////////////////////
		//// double encode
		//////////////////////////////////////////
		//safecpy(bufA, "https://localhost/one fish+two fish+red fish+blue fish");

		//lenB = sizeof(bufB);
		//lenC = sizeof(bufC);
		//dstBufLen = sizeof(dstBuf);
		////netTestAssert(netHttpRequest::UrlEncode(bufB, &lenB, bufA, istrlen(bufA), &numConsumed, ":/", true));
		////netTestAssert(netHttpRequest::UrlEncode(dstBuf, &dstBufLen, bufB, lenB, &numConsumed, ":/", true));
		//lenB = sizeof(bufB);
		//netTestAssert(netHttpRequest::UrlDecode(bufB, &lenB, dstBuf, dstBufLen, &numConsumed));
		//netTestAssert(netHttpRequest::UrlDecode(bufC, &lenC, bufB, lenB, &numConsumed));
		//netTestAssert(stricmp(bufA, bufC) == 0);

		//lenB = sizeof(bufB);
		//lenC = sizeof(bufC);
		//dstBufLen = sizeof(dstBuf);
		////netTestAssert(netHttpRequest::UrlEncode(bufB, &lenB, bufA, istrlen(bufA), &numConsumed, ":/", false));
		////netTestAssert(netHttpRequest::UrlEncode(dstBuf, &dstBufLen, bufB, lenB, &numConsumed, ":/", false));
		//lenB = sizeof(bufB);
		//netTestAssert(netHttpRequest::UrlDecode(bufB, &lenB, dstBuf, dstBufLen, &numConsumed));
		//netTestAssert(netHttpRequest::UrlDecode(bufC, &lenC, bufB, lenB, &numConsumed));
		//netTestAssert(stricmp(bufA, bufC) == 0);

		//lenB = sizeof(bufB);
		//lenC = sizeof(bufC);
		//dstBufLen = sizeof(dstBuf);
		////netTestAssert(netHttpRequest::UrlEncode(bufB, &lenB, bufA, istrlen(bufA), &numConsumed, ":/", false));
		////netTestAssert(netHttpRequest::UrlEncode(dstBuf, &dstBufLen, bufB, lenB, &numConsumed, ":/", true));
		//lenB = sizeof(bufB);
		//netTestAssert(netHttpRequest::UrlDecode(bufB, &lenB, dstBuf, dstBufLen, &numConsumed));
		//netTestAssert(netHttpRequest::UrlDecode(bufC, &lenC, bufB, lenB, &numConsumed));
		//netTestAssert(stricmp(bufA, bufC) == 0);

		//lenB = sizeof(bufB);
		//lenC = sizeof(bufC);
		//dstBufLen = sizeof(dstBuf);
		////netTestAssert(netHttpRequest::UrlEncode(bufB, &lenB, bufA, istrlen(bufA), &numConsumed, ":/", true));
		////netTestAssert(netHttpRequest::UrlEncode(dstBuf, &dstBufLen, bufB, lenB, &numConsumed, ":/", false));
		//lenB = sizeof(bufB);
		//netTestAssert(netHttpRequest::UrlDecode(bufB, &lenB, dstBuf, dstBufLen, &numConsumed));
		//netTestAssert(netHttpRequest::UrlDecode(bufC, &lenC, bufB, lenB, &numConsumed));
		//netTestAssert(stricmp(bufA, bufC) == 0);

		////////////////////////////////////////
		////////////////////////////////////////
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(BatchUrlEncoding)
	{
		char bufA[512], bufB[sizeof(bufA)+1];
		char dstBuf[3*sizeof(bufA)];
		netRandom rng(1);
		unsigned lenA, lenB, numConsumed;

		bool allSucceeded = true;

		for(int i = 0; i < 1000; ++i)
		{
			rtry
			{
				for(int j = 0; j < sizeof(bufA); ++j)
				{
					// Handle spaces in another test.
					do 
					{
						bufA[j] = (char)(rng.GetInt() & 0xFF);
					} while (bufA[j] == ' ');
				}

				lenA = sizeof(dstBuf);
				rverify(netHttpRequest::UrlEncode(dstBuf, &lenA, bufA, sizeof(bufA), &numConsumed, NULL), catchall, );
				rverify(numConsumed == sizeof(bufA), catchall, );
				lenB = sizeof(bufB);
				rverify(netHttpRequest::UrlDecode(bufB, &lenB, dstBuf, lenA, &numConsumed), catchall, );
				rverify(numConsumed == lenB, catchall, );
				rverify(!memcmp(bufA, bufB, sizeof(bufA)), catchall, );
			}
			rcatchall
			{
				allSucceeded = false;
				break;
			}
		}

		netTestAssert(allSucceeded);
	}
	NET_TEST_METHOD_END
}
NET_TEST_CLASS_END(Test_netHttpRequest)
