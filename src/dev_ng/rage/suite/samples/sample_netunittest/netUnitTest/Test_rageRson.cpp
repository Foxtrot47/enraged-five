

#include "netUnitTestFramework.h"

#if UNITTEST_ENABLE_RAGE_RSON

#include "data/rson.h"
#include "system/nelem.h"

using namespace rage;

NET_TEST_CLASS(Test_rageRson)
{
	NET_TEST_INIT(Init_rageRson)
	{

	}

	NET_TEST_METHOD(TestJsonWriter)
	{
		char buf[1024];
		RsonWriter rw(buf, COUNTOF(buf), RSON_FORMAT_JSON);

		u8 binval[] = {'f','o','o','b','a','r'};
		int ints[] = {1,2,3,4};
		unsigned uns[] = {1,2,3,4};
		float floats[] = {1.5f,1.25f,1.125f,1.0625f};
		double doubles[] = {1234.0000000000000009,2.234577000001,1.30000030000457,1.200003400057};

		bool success = rw.Begin(NULL, NULL);
		netTestAssert(success);
		success = rw.WriteDouble("double", 12340000000000000009.0E-016);
		netTestAssert(success);
		success = rw.WriteArray("doubles", doubles, 4);
		netTestAssert(success);
		success = rw.End();
		netTestAssert(0 == strcmp(rw.ToString(), "{\"double\":1234.00000,\"doubles\":[1234.00000,2.23458,1.30000,1.20000]}"));
		rw.Reset();

		success = rw.Begin(NULL, NULL);
		netTestAssert(success);
		success = rw.WriteInt("int", 123);
		netTestAssert(success);
		success = rw.WriteUns("uns", 0xFFFF1230);
		netTestAssert(success);
		success = rw.WriteFloat("float", 1.334455f);
		netTestAssert(success);
		success = rw.WriteHex("hex", 0xABCDEF);
		netTestAssert(success);
		success = rw.WriteHex64("hex64", 0x123456789ABCDEFull);
		netTestAssert(success);
		success = rw.WriteBinary("foobar", binval, COUNTOF(binval));
		netTestAssert(success);
		success = rw.WriteBool("bool", true);
		netTestAssert(success);
		success = rw.WriteString("string", "testoff");
		netTestAssert(success);
		success = rw.WriteString("string2", "test off");
		netTestAssert(success);
		success = rw.WriteString("string3", "\"test off\"");
		netTestAssert(success);
		success = rw.WriteArray("ints", ints, 4);
		netTestAssert(success);
		success = rw.WriteArray("uns", uns, 4);
		netTestAssert(success);
		success = rw.WriteArray("floats", floats, 4);
		netTestAssert(success);
		success = rw.End();
		netTestAssert(success);

		netTestAssert(0 == strcmp(rw.ToString(),
			"{\"int\":123,\"uns\":4294906416,\"float\":1.33446,\"hex\":\"0xABCDEF\",\"hex64\":\"0x123456789ABCDEF\",\"foobar\":\"Zm9vYmFy\",\"bool\":true,\"string\":\"testoff\",\"string2\":\"test off\",\"string3\":\"\\\"test off\\\"\",\"ints\":[1,2,3,4],\"uns\":[1,2,3,4],\"floats\":[1.50000,1.25000,1.12500,1.06250]}"));

		//Test calling End() when the buffer has been reset to continue
		//and nothing further has been added.  This occurs when the
		//underlying memory buffer was exhausted and the writer was reset
		//to continue, but there was no further data to write.
		rw.Reset();
		success = rw.Begin(NULL, NULL);
		netTestAssert(success);
		success = rw.WriteInt("int", 123);
		netTestAssert(success);
		rw.ResetToContinue();
		success = rw.End();
		netTestAssert(success);

		netTestAssert(0 == strcmp(rw.ToString(), "}"));
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestJsonReader)
	{
		const char* rsonTests[] =
		{
			//Value is an escaped string
			"{\"astring\":\"\\\"thestring\\\"\"}",
			//Lots of things
			"{\"vers\":123,\"foo\":\"bar\",\"user\":{\"onname\":\"Your Test\",\"id\":\"0x43dc7a128add9087\"},\"time\":1234567,\"microtime\":34359738368}",
			//Extra double quotes
			"{\"foo\" : \"bubba\"\",\"bar\":\"hubba\"}",
			//Misplaced }
			"{\"foo\" : \"bubba\"},\"bar\":\"hubba\"}",
			//No {}
			"\"foo\" : \"bubba\",\"bar\":\"hubba\"",
			//No {}
			"{\"b0\":\"Zg\",\"d0\":\"f\",\"b1\":\"Zm8\",\"d1\":\"fo\",\"b2\":\"Zm9v\",\"d2\":\"foo\",\"b3\":\"Zm9vYg\",\"d3\":\"fooob\",\"b4\":\"Zm9vYmE\",\"d4\":\"foooba\",\"b5\":\"Zm9vYmFy\",\"d5\":\"fooobar\"}",
			//Arrays of int, unsigned, float
			"{\"ints\":[1,2,3,4],\"uns\":[-1,-2,-3,-4],\"floats\":[1.5,1.25,1.125,1.0625]}",
			"{\"doubles\":[9.0E-016,2.234567890123456,1.3345678901234559,0.0000000000009]}",
			"{\"Success\":true}",
			"{\"Failure\":false}",
			//Array of objects.  JSON can have spaces in the name.
			"[{\"a a\":1},{\"b b\":2},{\"c c\":3}]",
		};

		RsonReader rr, tmp;
		char str[128];
		u64 id, microtime;
		unsigned time, u;

		bool success = rr.Init(rsonTests[0], 0, (unsigned) strlen(rsonTests[0]));
		netTestAssert(success);
		success = rr.ReadString("astring", str, sizeof(str));
		netTestAssert(success);
		netTestAssert(0 == strcmp(str, "\"thestring\""));

		success = rr.Init(rsonTests[1], 0, (unsigned) strlen(rsonTests[1]));
		netTestAssert(success);
		success = rr.GetMember("user", &tmp);
		netTestAssert(success);
		success = tmp.ReadString("onname", str, COUNTOF(str));
		netTestAssert(success && !strcmp("Your Test", str));
		success = tmp.ReadUns64("id", id);
		netTestAssert(success && 0x43dc7a128add9087ull == id);
		success = rr.ReadUns("time", time);
		netTestAssert(success && 1234567 == time);
		success = rr.ReadUns64("microtime", microtime);
		netTestAssert(success && 34359738368ull == microtime);
		success = rr.ReadUns("foo", u);
		netTestAssert(!success);
		success = rr.ReadString("foo", str, COUNTOF(str));
		netTestAssert(success && !strcmp("bar", str));
		success = rr.ReadUns("bar", u);
		netTestAssert(!success);

		success = rr.Init(rsonTests[2], 0, (unsigned) strlen(rsonTests[2]));
		netTestAssert(!success);

		success = rr.Init(rsonTests[3], 0, (unsigned) strlen(rsonTests[3]));
		success = rr.ReadString("foo", str, COUNTOF(str));
		netTestAssert(success && !strcmp("bubba", str));
		success = rr.ReadString("bar", str, COUNTOF(str));
		netTestAssert(!success);

		success = rr.Init(rsonTests[4], 0, (unsigned) strlen(rsonTests[4]));
		success = rr.ReadString("foo", str, COUNTOF(str));
		netTestAssert(!success);
		success = rr.ReadString("bar", str, COUNTOF(str));
		netTestAssert(!success);
		success = rr.AsString(str, COUNTOF(str));
		netTestAssert(success && !strcmp("bubba", str));

		u8 bv[128];
		int len;
		success = rr.Init(rsonTests[5], 0, (unsigned) strlen(rsonTests[5]));
		success = rr.ReadBinary("b0", bv, COUNTOF(bv), &len);
		netTestAssert(success && 1 == len && 'f' == bv[0]);
		success = rr.ReadBinary("b1", bv, COUNTOF(bv), &len);
		netTestAssert(success && 2 == len && 'f' == bv[0] && 'o' == bv[1]);
		success = rr.ReadBinary("b2", bv, COUNTOF(bv), &len);
		netTestAssert(success && 3 == len && 'f' == bv[0] && 'o' == bv[1] && 'o' == bv[2]);
		success = rr.ReadBinary("b3", bv, COUNTOF(bv), &len);
		netTestAssert(success && 4 == len && 'f' == bv[0] && 'o' == bv[1] && 'o' == bv[2] && 'b' == bv[3]);
		success = rr.ReadBinary("b4", bv, COUNTOF(bv), &len);
		netTestAssert(success && 5 == len && 'f' == bv[0] && 'o' == bv[1] && 'o' == bv[2] && 'b' == bv[3] && 'a' == bv[4]);
		success = rr.ReadBinary("b5", bv, COUNTOF(bv), &len);
		netTestAssert(success && 6 == len && 'f' == bv[0] && 'o' == bv[1] && 'o' == bv[2] && 'b' == bv[3] && 'a' == bv[4] && 'r' == bv[5]);

		int ints[4];
		unsigned uns[4];
		float floats[4];
		double doubles[4];

		rr.Init(rsonTests[6], 0, (unsigned) strlen(rsonTests[6]));
		int numRead = rr.ReadArray("ints", ints, 4);
		netTestAssert(4 == numRead);
		netTestAssert(1==ints[0] && 2==ints[1] && 3==ints[2] && 4==ints[3]);
		numRead = rr.ReadArray("uns", uns, 4);
		netTestAssert(4 == numRead);
		netTestAssert(unsigned(-1)==uns[0] && unsigned(-2)==uns[1] && unsigned(-3)==uns[2] && unsigned(-4)==uns[3]);
		numRead = rr.ReadArray("floats", floats, 4);
		netTestAssert(4 == numRead);
		netTestAssert(1.5f==floats[0] && 1.25f==floats[1] && 1.125f==floats[2] && 1.0625f==floats[3]);

		rr.Init(rsonTests[7], 0, (unsigned) strlen(rsonTests[7]));
		numRead = rr.ReadArray("doubles", doubles, 4);
		netTestAssert(4 == numRead);
		netTestAssert(9e-016==doubles[0] && 2.2345678901234560==doubles[1] && 1.3345678901234559==doubles[2] && 9e-13==doubles[3]);

		bool b;
		rr.Init(rsonTests[8], 0, (unsigned) strlen(rsonTests[8]));
		success = rr.ReadBool("Success", b);
		netTestAssert(success);
		netTestAssert(b);

		rr.Init(rsonTests[9], 0, (unsigned) strlen(rsonTests[9]));
		success = rr.ReadBool("Failure", b);
		netTestAssert(success);
		netTestAssert(!b);

		RsonReader readers[10];
		const char* names[] = {"a a", "b b", "c c"};
		rr.Init(rsonTests[10], 0, (unsigned) strlen(rsonTests[10]));
		numRead = rr.AsArray(readers, COUNTOF(readers));
		netTestAssert(COUNTOF(names) == numRead);
		for(int i = 0; i < COUNTOF(names); ++i)
		{
			success = readers[i].GetFirstMember(&rr);
			netTestAssert(success);
			success = rr.GetName(str, sizeof(str));
			netTestAssert(success);
			netTestAssert(!strcmp(str, names[i]));
			int val;
			success = rr.AsInt(val);
			netTestAssert(success);
			netTestAssert(i+1 == val);
		}
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestLuaWriter)
	{
		char buf[1024];
		RsonWriter rw(buf, COUNTOF(buf), RSON_FORMAT_LUA);

		u8 binval[] = {'f','o','o','b','a','r'};
		int ints[] = {1,2,3,4};
		unsigned uns[] = {1,2,3,4};
		float floats[] = {1.5f,1.25f,1.125f,1.0625f};
		double doubles[] = {1234.0000000000000009,2.234577000001,1.30000030000457,1.200003400057};

		bool success = rw.Begin(NULL, NULL);
		netTestAssert(success);
		success = rw.WriteDouble("double", 12340000000000000009.0E-016);
		netTestAssert(success);
		success = rw.WriteArray("doubles", doubles, 4);
		netTestAssert(success);
		success = rw.End();
		netTestAssert(0 == strcmp(rw.ToString(), "{double=1234.00000,doubles={1234.00000,2.23458,1.30000,1.20000}}"));
		rw.Reset();

		success = rw.Begin(NULL, NULL);
		netTestAssert(success);
		success = rw.WriteInt("int", 123);
		netTestAssert(success);
		success = rw.WriteUns("uns", 0xFFFF1230);
		netTestAssert(success);
		success = rw.WriteFloat("float", 1.334455f);
		netTestAssert(success);
		success = rw.WriteHex("hex", 0xABCDEF);
		netTestAssert(success);
		success = rw.WriteHex64("hex64", 0x123456789ABCDEFull);
		netTestAssert(success);
		success = rw.WriteBinary("foobar", binval, COUNTOF(binval));
		netTestAssert(success);
		success = rw.WriteBool("bool", true);
		netTestAssert(success);
		success = rw.WriteString("string", "testoff");
		netTestAssert(success);
		success = rw.WriteString("string2", "test off");
		netTestAssert(success);
		success = rw.WriteString("string3", "\"test off\"");
		netTestAssert(success);
		success = rw.WriteArray("ints", ints, 4);
		netTestAssert(success);
		success = rw.WriteArray("uns", uns, 4);
		netTestAssert(success);
		success = rw.WriteArray("floats", floats, 4);
		netTestAssert(success);
		success = rw.End();
		netTestAssert(success);

		netTestAssert(0 == strcmp(rw.ToString(),
			"{int=123,uns=4294906416,float=1.33446,hex=0xABCDEF,hex64=0x123456789ABCDEF,foobar=Zm9vYmFy,bool=true,string=testoff,string2=\"test off\",string3=\"\\\"test off\\\"\",ints={1,2,3,4},uns={1,2,3,4},floats={1.50000,1.25000,1.12500,1.06250}}"));

		//Test calling End() when the buffer has been reset to continue
		//and nothing further has been added.  This occurs when the
		//underlying memory buffer was exhausted and the writer was reset
		//to continue, but there was no further data to write.
		rw.Reset();
		success = rw.Begin(NULL, NULL);
		netTestAssert(success);
		success = rw.WriteInt("int", 123);
		netTestAssert(success);
		rw.ResetToContinue();
		success = rw.End();
		netTestAssert(success);

		netTestAssert(0 == strcmp(rw.ToString(), "}"));
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestLuaReader)
	{
		const char* rsonTests[] =
		{
			//Value is an escaped string
			"{\"astring\":\"\\\"thestring\\\"\"}",
			//Lots of things
			"{vers=123,foo=bar,user={onname=\"Your Test\",id=0x43dc7a128add9087},time=1234567,microtime=34359738368}",
			//Extra double quotes
			"{foo = \"bubba\"\",bar=hubba}",
			//Misplaced }
			"{foo = \"bubba\"},bar=hubba}",
			//No {}
			"foo = \"bubba\",bar=hubba",
			//No {}
			"{b0=Zg,d0=f,b1=Zm8,d1=fo,b2=Zm9v,d2=foo,b3=Zm9vYg,d3=fooob,b4=Zm9vYmE,d4=foooba,b5=Zm9vYmFy,d5=fooobar}",
			//Arrays of int, unsigned, float
			"{ints={1,2,3,4},uns={-1,-2,-3,-4},floats={1.5,1.25,1.125,1.0625}}",
			"{doubles={9.0E-016,2.234567890123456,1.3345678901234559,0.0000000000009}}",
			"{Success=true}",
			"{Failure=false}",
			//Array of objects, with spaces in the names
			"{{\"a a\"=1},{\"b b\"=2},{\"c c\"=3}}",
		};

		RsonReader rr, tmp;
		char str[128];
		u64 id, microtime;
		unsigned time, u;

		bool success = rr.Init(rsonTests[0], 0, (unsigned) strlen(rsonTests[0]));
		netTestAssert(success);
		success = rr.ReadString("astring", str, sizeof(str));
		netTestAssert(success);
		netTestAssert(0 == strcmp(str, "\"thestring\""));

		success = rr.Init(rsonTests[1], 0, (unsigned) strlen(rsonTests[1]));
		netTestAssert(success);
		success = rr.GetMember("user", &tmp);
		netTestAssert(success);
		success = tmp.ReadString("onname", str, COUNTOF(str));
		netTestAssert(success && !strcmp("Your Test", str));
		success = tmp.ReadUns64("id", id);
		netTestAssert(success && 0x43dc7a128add9087ull == id);
		success = rr.ReadUns("time", time);
		netTestAssert(success && 1234567 == time);
		success = rr.ReadUns64("microtime", microtime);
		netTestAssert(success && 34359738368ull == microtime);
		success = rr.ReadUns("foo", u);
		netTestAssert(!success);
		success = rr.ReadString("foo", str, COUNTOF(str));
		netTestAssert(success && !strcmp("bar", str));
		success = rr.ReadUns("bar", u);
		netTestAssert(!success);

		success = rr.Init(rsonTests[2], 0, (unsigned) strlen(rsonTests[2]));
		netTestAssert(!success);

		success = rr.Init(rsonTests[3], 0, (unsigned) strlen(rsonTests[3]));
		success = rr.ReadString("foo", str, COUNTOF(str));
		netTestAssert(success && !strcmp("bubba", str));
		success = rr.ReadString("bar", str, COUNTOF(str));
		netTestAssert(!success);

		success = rr.Init(rsonTests[4], 0, (unsigned) strlen(rsonTests[4]));
		success = rr.ReadString("foo", str, COUNTOF(str));
		netTestAssert(!success);
		success = rr.ReadString("bar", str, COUNTOF(str));
		netTestAssert(!success);
		success = rr.AsString(str, COUNTOF(str));
		netTestAssert(success && !strcmp("bubba", str));

		u8 bv[128];
		int len;
		success = rr.Init(rsonTests[5], 0, (unsigned) strlen(rsonTests[5]));
		success = rr.ReadBinary("b0", bv, COUNTOF(bv), &len);
		netTestAssert(success && 1 == len && 'f' == bv[0]);
		success = rr.ReadBinary("b1", bv, COUNTOF(bv), &len);
		netTestAssert(success && 2 == len && 'f' == bv[0] && 'o' == bv[1]);
		success = rr.ReadBinary("b2", bv, COUNTOF(bv), &len);
		netTestAssert(success && 3 == len && 'f' == bv[0] && 'o' == bv[1] && 'o' == bv[2]);
		success = rr.ReadBinary("b3", bv, COUNTOF(bv), &len);
		netTestAssert(success && 4 == len && 'f' == bv[0] && 'o' == bv[1] && 'o' == bv[2] && 'b' == bv[3]);
		success = rr.ReadBinary("b4", bv, COUNTOF(bv), &len);
		netTestAssert(success && 5 == len && 'f' == bv[0] && 'o' == bv[1] && 'o' == bv[2] && 'b' == bv[3] && 'a' == bv[4]);
		success = rr.ReadBinary("b5", bv, COUNTOF(bv), &len);
		netTestAssert(success && 6 == len && 'f' == bv[0] && 'o' == bv[1] && 'o' == bv[2] && 'b' == bv[3] && 'a' == bv[4] && 'r' == bv[5]);

		int ints[4];
		unsigned uns[4];
		float floats[4];
		double doubles[4];

		rr.Init(rsonTests[6], 0, (unsigned) strlen(rsonTests[6]));
		int numRead = rr.ReadArray("ints", ints, 4);
		Assert(4 == numRead);
		Assert(1==ints[0] && 2==ints[1] && 3==ints[2] && 4==ints[3]);
		numRead = rr.ReadArray("uns", uns, 4);
		netTestAssert(4 == numRead);
		netTestAssert(unsigned(-1)==uns[0] && unsigned(-2)==uns[1] && unsigned(-3)==uns[2] && unsigned(-4)==uns[3]);
		numRead = rr.ReadArray("floats", floats, 4);
		netTestAssert(4 == numRead);
		netTestAssert(1.5f==floats[0] && 1.25f==floats[1] && 1.125f==floats[2] && 1.0625f==floats[3]);

		rr.Init(rsonTests[7], 0, (unsigned) strlen(rsonTests[7]));
		numRead = rr.ReadArray("doubles", doubles, 4);
		netTestAssert(4 == numRead);
		netTestAssert(9e-016==doubles[0] && 2.2345678901234560==doubles[1] && 1.3345678901234559==doubles[2] && 9e-13==doubles[3]);

		bool b;
		rr.Init(rsonTests[8], 0, (unsigned) strlen(rsonTests[8]));
		success = rr.ReadBool("Success", b);
		netTestAssert(success);
		netTestAssert(b);

		rr.Init(rsonTests[9], 0, (unsigned) strlen(rsonTests[9]));
		success = rr.ReadBool("Failure", b);
		netTestAssert(success);
		netTestAssert(!b);

		RsonReader readers[10];
		const char* names[] = {"a a", "b b", "c c"};
		(void) names; // MS compilers in final: error C4189: 'names' : local variable is initialized but not referenced ... wat?

		rr.Init(rsonTests[10], 0, (unsigned) strlen(rsonTests[10]));
		numRead = rr.AsArray(readers, COUNTOF(readers));
		netTestAssert(COUNTOF(names) == numRead);
		for(int i = 0; i < COUNTOF(names); ++i)
		{
			success = readers[i].GetFirstMember(&rr);
			Assert(success);
			success = rr.GetName(str, sizeof(str));
			Assert(success);
			Assert(!strcmp(str, names[i]));
			int val;
			success = rr.AsInt(val);
			netTestAssert(success);
			netTestAssert(i+1 == val);
		}
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestRsonHandle)
	{
		// create a test string with many children
		//const char* rsonTest = "{ \"child1\":  { \"child2\" : { \"child3\" :  { \"child4\" :  { \"intval\" : 5, \"stringval\" : \"Test\" } } } } }";

		//// initialize the rson reader
		//RsonReader rr;
		//bool success = rr.Init(rsonTest, StringLength(rsonTest) + 1); // null terminator
		//netTestAssert(success);

		//// create a handle to the rson reader, and validate it
		//RsonHandle handle(&rr);
		//netTestAssert(handle.IsValid());

		//// extract the first child by name
		//RsonHandle firstchild = handle.GetMember("child1");
		//netTestAssert(firstchild.IsValid());
		//
		//// extract the 'child4' member, from both the 'handle' (root) and the firstChild (first child)
		//RsonHandle intVal = firstchild.GetMember("child2").GetMember("child3").GetMember("child4").GetMember("intval");
		//RsonHandle intValLongPath = handle.GetMember("child1").GetMember("child2").GetMember("child3").GetMember("child4").GetMember("intval");
		//// validate that both Handles returned a valid reader
		//netTestAssert(intVal.IsValid());
		//netTestAssert(intValLongPath.IsValid());

		//// extract the first integer value
		//int i;
		//netTestAssert(intVal.Reader()->AsInt(i));
		//netTestAssertEqual(i, 5);

		//// extract the second integer value (using the longer path)
		//int iLongPath;
		//netTestAssert(intValLongPath.Reader()->AsInt(iLongPath));
		//netTestAssertEqual(iLongPath, 5);

		//// validate that both methods read the same integer
		//netTestAssertEqual(i, iLongPath);

		//// attempt to find an 'invalid' member. Validate that 'isValid' returns FALSE and Reader() returns NULL
		//RsonHandle invalidHandle = handle.GetMember("child1").GetMember("invalid");
		//netTestAssert(invalidHandle.IsValid() == false);
		//netTestAssert(invalidHandle.Reader() == NULL);

		//// attempt to find a child from the invalid member
		//RsonHandle invalidChildHandle = invalidHandle.GetMember("invalid");
		//netTestAssert(invalidChildHandle.IsValid() == false);
		//netTestAssert(invalidChildHandle.Reader() == NULL);

		//// read the 'stringval' from the 4th child, as seen from the 1st child
		//RsonHandle strVal = firstchild.GetMember("child2").GetMember("child3").GetMember("child4").GetMember("stringval");
		//netTestAssert(strVal.IsValid());

		//// extract and validate the string value
		//char buf[256];
		//netTestAssert(strVal.Reader()->AsString(buf));
		//netTestAssert(stricmp(buf, "Test") == 0);
	}
	NET_TEST_METHOD_END
}
NET_TEST_CLASS_END(Test_rageRson)

#endif // UNITTEST_ENABLE_RAGE_RSON