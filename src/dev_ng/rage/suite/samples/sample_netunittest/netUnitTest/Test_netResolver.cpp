

#include "netUnitTestFramework.h"

#if UNITTEST_ENABLE_NET_RESOLVER

#include "net/resolver.h"
#include "net/status.h"
#include "net/netaddress.h"
//#include "net/task2.h"
#include "system/timer.h"

using namespace rage;

NET_TEST_CLASS(Test_netResolver)
{
	NET_TEST_INIT(Init_netTask2)
	{

	}


	NET_TEST_METHOD(TestResolve)
	{
		//netResolver::Init();

		//netStatus resolverStatus;
		//netIpAddress ipAddress;

		//const char* hostname = "dev.ros.rockstargames.com";
		//netTestAssert(netResolver::ResolveHost(hostname, &ipAddress, &resolverStatus));

		//while(resolverStatus.Pending())
		//{
		//	netTask2::Update();
		//	netResolver::Update();
		//}

		//netTestAssert(resolverStatus.Succeeded());

		//netResolver::Shutdown();
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestResolveFailure)
	{
		/*netResolver::Init();

		netStatus resolverStatus;
		netIpAddress ipAddress;

		const char* hostname = "dev.ros.rockstargames.coom";
		
#if !USE_MS_UNIT_TEST_FRAMEWORK
		netUnitTest::sm_IsExpectingFailures = true;
#endif

		netTestAssert(netResolver::ResolveHost(hostname, &ipAddress, &resolverStatus));
		while(resolverStatus.Pending())
		{
			netTask2::Update();
			netResolver::Update();
		}

#if !USE_MS_UNIT_TEST_FRAMEWORK
		netUnitTest::sm_IsExpectingFailures = true;
#endif

		netTestAssert(resolverStatus.Failed());

		netResolver::Shutdown();
		*/
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestResolveCombine)
	{
//		netResolver::Init();
//
//		// The resolvers support up to 16 simultaneous resolves, so we'll try with 17.
//		const int NUM_RESOLVERS = 17;
//		netStatus resolverStatus[NUM_RESOLVERS];
//		netIpAddress ipAddress[NUM_RESOLVERS];
//
//		const char* hostname = "dev.ros.rockstargames.com";
//
//		// First off the first 16 resolves, expecting all to succeed
//		int i = 0;
//		for (int i = 0; i < NUM_RESOLVERS - 1; i++)
//		{
//			netTestAssert(netResolver::ResolveHost(hostname, &ipAddress[i], &resolverStatus[i]));
//		}
//
//		// Expect the 17th to fail
//#if !USE_MS_UNIT_TEST_FRAMEWORK
//		netUnitTest::sm_IsExpectingFailures = true;
//#endif
//		netTestAssertEqual(netResolver::ResolveHost(hostname, &ipAddress[i], &resolverStatus[i]), false);
//
//#if !USE_MS_UNIT_TEST_FRAMEWORK
//		netUnitTest::sm_IsExpectingFailures = false;
//#endif
//
//		// Run while any task is pending
//		bool isAnyPending = true;
//		while(isAnyPending)
//		{
//			netTask2::Update();
//			netResolver::Update();
//
//			isAnyPending = false;
//			for (int i = 0; i < NUM_RESOLVERS; i++)
//			{
//				if (resolverStatus[i].Pending())
//				{
//					isAnyPending = true;
//					break;
//				}
//			}
//		}
//
//		// The net resolver pool is 16, so the first 16 should succeed
//		i = 0;
//		for (; i < NUM_RESOLVERS - 1; i++)
//			netTestAssert(resolverStatus[i].Succeeded());
//
//		// The net resolver pool is 16, so the 17th should fail
//		netTestAssert(resolverStatus[i].None());
//
//		netResolver::Shutdown();
	}
	NET_TEST_METHOD_END
		
	NET_TEST_METHOD(TestResolveCombineTwice)
	{
//		netResolver::Init();
//
//		// The resolvers support up to 16 simultaneous resolves, so we'll try with 17.
//		const int NUM_RESOLVERS = 17;
//		netStatus resolverStatus[NUM_RESOLVERS];
//		netIpAddress ipAddress[NUM_RESOLVERS];
//
//		const char* hostname = "dev.ros.rockstargames.com";
//
//		// First off the first 16 resolves, expecting all to succeed
//		int i = 0;
//		for (int i = 0; i < NUM_RESOLVERS - 1; i++)
//		{
//			netTestAssert(netResolver::ResolveHost(hostname, &ipAddress[i], &resolverStatus[i]));
//		}
//
//		// Expect the 17th to fail
//#if !USE_MS_UNIT_TEST_FRAMEWORK
//		netUnitTest::sm_IsExpectingFailures = true;
//#endif
//		netTestAssertEqual(netResolver::ResolveHost(hostname, &ipAddress[i], &resolverStatus[i]), false);
//
//#if !USE_MS_UNIT_TEST_FRAMEWORK
//		netUnitTest::sm_IsExpectingFailures = false;
//#endif
//
//		// Run while any task is pending
//		bool isAnyPending = true;
//		while(isAnyPending)
//		{
//			netTask2::Update();
//			netResolver::Update();
//
//			isAnyPending = false;
//			for (int i = 0; i < NUM_RESOLVERS; i++)
//			{
//				if (resolverStatus[i].Pending())
//				{
//					isAnyPending = true;
//					break;
//				}
//			}
//		}
//
//		// The net resolver pool is 16, so the first 16 should succeed
//		i = 0;
//		for (; i < NUM_RESOLVERS - 1; i++)
//			netTestAssert(resolverStatus[i].Succeeded());
//
//		// The net resolver pool is 16, so the 17th should fail
//		netTestAssert(resolverStatus[i].None());
//
//		/////////////////////// ITERATION TWO ////////////////////////////////
//		netResolver::Shutdown();
//		netResolver::Init();
//
//		// First off the first 16 resolves, expecting all to succeed
//		i = 0;
//		for (int i = 0; i < NUM_RESOLVERS - 1; i++)
//		{
//			ipAddress[i].Clear();
//			netTestAssert(netResolver::ResolveHost(hostname, &ipAddress[i], &resolverStatus[i]));
//		}
//
//		// Expect the 17th to fail
//#if !USE_MS_UNIT_TEST_FRAMEWORK
//		netUnitTest::sm_IsExpectingFailures = true;
//#endif
//		ipAddress[i].Clear();
//		netTestAssertEqual(netResolver::ResolveHost(hostname, &ipAddress[i], &resolverStatus[i]), false);
//
//#if !USE_MS_UNIT_TEST_FRAMEWORK
//		netUnitTest::sm_IsExpectingFailures = false;
//#endif
//
//		// Run while any task is pending
//		isAnyPending = true;
//		while(isAnyPending)
//		{
//			netTask2::Update();
//			netResolver::Update();
//
//			isAnyPending = false;
//			for (int i = 0; i < NUM_RESOLVERS; i++)
//			{
//				if (resolverStatus[i].Pending())
//				{
//					isAnyPending = true;
//					break;
//				}
//			}
//		}
//
//		// The net resolver pool is 16, so the first 16 should succeed
//		i = 0;
//		for (; i < NUM_RESOLVERS - 1; i++)
//			netTestAssert(resolverStatus[i].Succeeded());
//
//		// The net resolver pool is 16, so the 17th should fail
//		netTestAssert(resolverStatus[i].None());
//
//		netResolver::Shutdown();
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestResolveCancel)
	{
//		netResolver::Init();
//
//		// The resolvers support up to 16 simultaneous resolves, so we'll try with 17.
//		const int NUM_RESOLVERS = 17;
//
//		// We want to cancel the 3rd request
//		const int CANCEL_IDX = 3;
//
//		netStatus resolverStatus[NUM_RESOLVERS];
//		netIpAddress ipAddress[NUM_RESOLVERS];
//
//		const char* hostname = "dev.ros.rockstargames.com";
//
//		// First off the first 16 resolves, expecting all to succeed
//		int i = 0;
//		for (int i = 0; i < NUM_RESOLVERS - 1; i++)
//		{
//			netTestAssert(netResolver::ResolveHost(hostname, &ipAddress[i], &resolverStatus[i]));
//		}
//
//		// Expect the 17th to fail
//#if !USE_MS_UNIT_TEST_FRAMEWORK
//		netUnitTest::sm_IsExpectingFailures = true;
//#endif
//
//		netTestAssertEqual(netResolver::ResolveHost(hostname, &ipAddress[i], &resolverStatus[i]), false);
//
//#if !USE_MS_UNIT_TEST_FRAMEWORK
//		netUnitTest::sm_IsExpectingFailures = false;
//#endif
//
//		// cancel the third request
//		netResolver::Cancel(&resolverStatus[CANCEL_IDX]);
//
//		bool isAnyPending = true;
//		while(isAnyPending)
//		{
//			netTask2::Update();
//			netResolver::Update();
//
//			isAnyPending = false;
//			for (int i = 0; i < NUM_RESOLVERS; i++)
//			{
//				if (resolverStatus[i].Pending())
//				{
//					isAnyPending = true;
//					break;
//				}
//			}
//		}
//
//		// The net resolver pool is 16, so the first 16 should succeed
//		i = 0;
//		for (; i < NUM_RESOLVERS - 1; i++)
//		{
//			// the third should be cancelled, the rest should succeed
//			if ( i == CANCEL_IDX)
//			{
//				netTestAssert(resolverStatus[i].Canceled());
//			}
//			else
//			{
//				netTestAssert(resolverStatus[i].Succeeded());
//			}
//		}
//
//		// The net resolver pool is 16, so the 17th should fail
//		netTestAssert(resolverStatus[i].None());
//
//		netResolver::Shutdown();
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestResolveFull)
	{
//		netResolver::Init();
//
//		// net resolver supports 8 cache slots - test with 9 to ensure 8 succeed and 1 fail
//		const int MAX_CACHED_RESOLVERS = 9;
//		netStatus resolverStatus[MAX_CACHED_RESOLVERS];
//		netIpAddress ipAddress[MAX_CACHED_RESOLVERS];
//
//		const char* hostNames[MAX_CACHED_RESOLVERS] = 
//		{ 
//			"dev.ros.rockstargames.com", 
//			"dev.sc.rockstargames.com",
//			"beta.sc.rockstargames.com",
//			"hub.rockstargames.com",
//			"dev.scadmin.rockstargames.com",
//			"rockstargames.com",
//			"socialclub.rockstargames.com",
//			"scadmin.rockstargames.com",
//			"who.cares.will.fail",
//		};
//
//		// the first 8 should succeed
//		int i = 0;
//		for (i = 0; i < MAX_CACHED_RESOLVERS - 1; i++)
//		{
//			netTestAssert(netResolver::ResolveHost(hostNames[i], &ipAddress[i], &resolverStatus[i]));
//		}
//
//		// the 9th should fail
//#if !USE_MS_UNIT_TEST_FRAMEWORK
//		netUnitTest::sm_IsExpectingFailures = true;
//#endif
//
//		netTestAssertEqual(netResolver::ResolveHost(hostNames[i], &ipAddress[i], &resolverStatus[i]), false);
//
//#if !USE_MS_UNIT_TEST_FRAMEWORK
//		netUnitTest::sm_IsExpectingFailures = false;
//#endif
//
//		// Run while any task is pending
//		bool isAnyPending = true;
//		while(isAnyPending)
//		{
//			netTask2::Update();
//			netResolver::Update();
//
//			isAnyPending = false;
//			for (int i = 0; i < MAX_CACHED_RESOLVERS; i++)
//			{
//				if (resolverStatus[i].Pending())
//				{
//					isAnyPending = true;
//					break;
//				}
//			}
//		}
//
//		// The net resolver cache pool is 8, so the first 8 should succeed
//		for (i = 0; i < MAX_CACHED_RESOLVERS - 1; i++)
//			netTestAssert(resolverStatus[i].Succeeded());
//
//		// The net resolver cache pool is 8, so the 9th should succeed
//		netTestAssert(resolverStatus[i].None());
//
//		netResolver::Shutdown();
	}
	NET_TEST_METHOD_END

#if UNITTEST_ENABLE_LONG_BLOCKING_TASKS
	NET_TEST_METHOD(TestResolveTimeout)
	{
		netResolver::Init();

		netStatus resolverStatus;
		netIpAddress ipAddress;

		const char* hostname = "dev.ros.rockstargames.com";
		netTestAssert(netResolver::ResolveHost(hostname, &ipAddress, &resolverStatus));

		unsigned startTime = sysTimer::GetSystemMsTime();

		while(resolverStatus.Pending())
		{
			netTask2::Update();
			netResolver::Update();
		}

		netTestAssert(resolverStatus.Succeeded());

		// wait until 2 minutes has elapsed
		while(resolverStatus.Pending() || !sysTimer::HasElapsedIntervalMs(startTime, 2 * 60 * 1000))
		{
			netTask2::Update();
			netResolver::Update();
		}

		ipAddress.Clear();

		// succeeds from cache
		netTestAssert(netResolver::ResolveHost(hostname, &ipAddress, &resolverStatus));
		netTestAssert(resolverStatus.Succeeded());

		// wait until 4 minutes has elapsed
		while(resolverStatus.Pending() || !sysTimer::HasElapsedIntervalMs(startTime, 4 * 60 * 1000))
		{
			netTask2::Update();
			netResolver::Update();
		}

		// pending
		netTestAssert(netResolver::ResolveHost(hostname, &ipAddress, &resolverStatus));
		netTestAssert(resolverStatus.Pending());

		// wait for success
		while(resolverStatus.Pending())
		{
			netTask2::Update();
			netResolver::Update();
		}

		netTestAssert(resolverStatus.Succeeded());

		netResolver::Shutdown();
	}
	NET_TEST_METHOD_END
#endif // UNITTEST_ENABLE_LONG_BLOCKING_TASKS
}
NET_TEST_CLASS_END(Test_netResolver)

#endif // UNITTEST_ENABLE_NET_RESOLVER