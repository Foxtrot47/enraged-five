

#include "netUnitTestFramework.h"

#if UNITTEST_ENABLE_NET_CRYPTO

#include "net/net.h"
#include "net/crypto.h"
#include "net/netdiag.h"
#include "system/timer.h"

#include "system/nelem.h"
#include "system/simpleallocator.h"

#if RSG_DURANGO
// for debug memory to work
#pragma comment(lib, "PIXEvt.lib")
#endif

using namespace rage;

NET_TEST_CLASS(Test_netCrypto)
{
	NET_TEST_INIT(Init_netCrypto)
	{
	}

	NET_TEST_METHOD(TestP2pCryptSimple)
	{
		/*netP2pCrypt::Init();

		u8 hmacKey[32] = {0xF1, 0x1D, 0x88, 0x31, 0xA0, 0x81, 0x1E, 0xFF,
						  0xB3, 0x12, 0x12, 0x21, 0xAC, 0xDD, 0x19, 0x24,
						  0xE7, 0x36, 0x98, 0x0A, 0x76, 0xA8, 0x7F, 0xF7,
						  0x11, 0x00, 0xDF, 0x3C, 0x15, 0x61, 0x55, 0x4D};
		u8 hmacSalt[16] = {0x16, 0x35, 0x01, 0x7B, 0xB2, 0x7C, 0xAF, 0xE9,
						   0xB0, 0x5F, 0x13, 0x52, 0x7A, 0x21, 0x87, 0xB2};

		netP2pCrypt::SetHmacKeyAndSalt(hmacKey, sizeof(hmacKey), hmacSalt, sizeof(hmacSalt));

		netP2pCrypt::Key p2pKey;
		netP2pCrypt::GenerateRandomKey(p2pKey);

		const char* data = "Two things are infinite: the universe and human stupidity; and I'm not sure about the universe.";
		const unsigned sizeOfData = (unsigned)strlen(data) + 1;
		unsigned sizeOfEncodedData = sizeOfData + MAX_SIZEOF_P2P_CRYPT_OVERHEAD;
		u8* encodedData = (u8*)Alloca(u8, sizeOfEncodedData);

		bool success = netP2pCrypt::Encrypt(p2pKey, (const u8*)data, sizeOfData, encodedData, sizeOfEncodedData);
		netTestAssert(success);

		u8* buf = encodedData;
		unsigned sizeOfBuf = sizeOfEncodedData;
		success = netP2pCrypt::Decrypt(p2pKey, &buf, sizeOfBuf);
		netTestAssert(success);
		netTestAssert(sizeOfBuf == sizeOfData);
		netTestAssert(memcmp(data, buf, sizeOfData) == 0);

		netP2pCrypt::Shutdown();*/
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestP2pCryptStress)
	{
		//netP2pCrypt::Init();

		//netRandom r(sysTimer::GetSystemMsTime());

		//bool success = true;
		//for(unsigned i = 1; i < 10240; ++i)
		//{
		//	// test random length HMAC key and salt
		//	unsigned keyLen = r.GetRanged(1, netP2pCrypt::HMAC_KEY_MAX_LEN);
		//	unsigned saltLen = r.GetRanged(1, netP2pCrypt::HMAC_SALT_MAX_LEN);

		//	// test random values of HMAC key and salt
		//	u8 hmacKey[netP2pCrypt::HMAC_KEY_MAX_LEN] = {0};
		//	u8 hmacSalt[netP2pCrypt::HMAC_SALT_MAX_LEN] = {0};
		//	netP2pCrypt::GenerateRandomBytes(hmacKey, keyLen);
		//	netP2pCrypt::GenerateRandomBytes(hmacSalt, saltLen);

		//	netP2pCrypt::SetHmacKeyAndSalt(hmacKey, keyLen, hmacSalt, saltLen);

		//	// test every plaintext length from 1 byte to N bytes
		//	const unsigned sizeOfData = i;
		//	u8* data = new u8[sizeOfData];

		//	netP2pCrypt::GenerateRandomBytes(data, sizeOfData);

		//	unsigned sizeOfEncodedData = sizeOfData + MAX_SIZEOF_P2P_CRYPT_OVERHEAD;
		//	u8* encodedData = new u8[sizeOfEncodedData];

		//	// test random keys
		//	netP2pCrypt::Key p2pKey;
		//	netP2pCrypt::GenerateRandomKey(p2pKey);

		//	success = netP2pCrypt::Encrypt(p2pKey, data, sizeOfData, encodedData, sizeOfEncodedData);

		//	u8* decryptedData = encodedData;
		//	unsigned sizeOfDecryptedData = sizeOfEncodedData;
		//	success = success && netP2pCrypt::Decrypt(p2pKey, &decryptedData, sizeOfDecryptedData);
		//	success = success && (sizeOfDecryptedData == sizeOfData);
		//	success = success && (memcmp(data, decryptedData, sizeOfData) == 0);

		//	delete[] data;
		//	delete[] encodedData;

		//	if(!success)
		//	{
		//		break;
		//	}
		//}

		//netTestAssert(success);

		//netP2pCrypt::Shutdown();
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestP2pCryptSpeed)
	{
		//// measure how fast we can encrypt/decrypt

		//netP2pCrypt::Init();
		//
		//// test random length HMAC key and salt
		//unsigned keyLen = 16;
		//unsigned saltLen = 16;

		//// test random values of HMAC key and salt
		//u8 hmacKey[netP2pCrypt::HMAC_KEY_MAX_LEN] = {0};
		//u8 hmacSalt[netP2pCrypt::HMAC_SALT_MAX_LEN] = {0};
		//netP2pCrypt::GenerateRandomBytes(hmacKey, keyLen);
		//netP2pCrypt::GenerateRandomBytes(hmacSalt, saltLen);

		//netP2pCrypt::SetHmacKeyAndSalt(hmacKey, keyLen, hmacSalt, saltLen);

		//u8 data[1024];
		//u8 encodedData[sizeof(data) + MAX_SIZEOF_P2P_CRYPT_OVERHEAD];
		////u8 decryptedData[sizeof(data) + MAX_SIZEOF_P2P_CRYPT_OVERHEAD];

		//netP2pCrypt::GenerateRandomBytes(data, sizeof(data));

		//unsigned sizeOfData = sizeof(data);

		//netP2pCrypt::Key p2pKey;
		//netP2pCrypt::GenerateRandomKey(p2pKey);
		//
		//const unsigned numIterations = 100000;

		//bool success = true;
		//unsigned startTime = sysTimer::GetSystemMsTime();
		//unsigned sizeOfEncodedData = sizeof(encodedData);

		//for(unsigned i = 0; (i < numIterations) && success; ++i)
		//{
		//	sizeOfEncodedData = sizeof(encodedData);
		//	success = netP2pCrypt::Encrypt(p2pKey, data, sizeOfData, encodedData, sizeOfEncodedData);
		//}

		//unsigned elasped = sysTimer::GetSystemMsTime() - startTime;
		//netTestAssert(success);
		//float msPerOp = (float)elasped / numIterations;
		//Displayf("TestP2pCryptSpeed took %u ms to encrypt %u plaintexts of length %u bytes. %.4f ms per encrypt.", elasped, numIterations, (unsigned)sizeof(data), msPerOp);
		//netTestAssert(msPerOp < 0.1);

		//success = true;
		//startTime = sysTimer::GetSystemMsTime();
 	//	for(unsigned i = 0; (i < numIterations) && success; ++i)
 	//	{
		//	// we need to copy the encrypted data each iteration so the speed test will include the copying time
		//	u8 decryptedData[sizeof(encodedData)];
		//	u8* buf = decryptedData;
		//	sysMemCpy(buf, encodedData, sizeOfEncodedData);
		//	unsigned sizeOfBuf = sizeOfEncodedData;
 	//		success = netP2pCrypt::Decrypt(p2pKey, &buf, sizeOfBuf);
 	//	}

		//elasped = sysTimer::GetSystemMsTime() - startTime;
		//netTestAssert(success);
		//msPerOp = (float)elasped / numIterations;
		//Displayf("TestP2pCryptSpeed took %u ms to decrypt %u ciphertexts of length %u bytes. %.4f ms per decrypt.", elasped, numIterations, (unsigned)sizeof(data), msPerOp);
		//netTestAssert(msPerOp < 0.1);

		//netP2pCrypt::Shutdown();
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestP2pCryptTamperDetection)
	{
		//netP2pCrypt::Init();

		//u8 hmacKey[netP2pCrypt::HMAC_KEY_MAX_LEN] = {0};
		//u8 hmacSalt[netP2pCrypt::HMAC_SALT_MAX_LEN] = {0};
		//unsigned keyLen = sizeof(hmacKey);
		//unsigned saltLen = sizeof(hmacSalt);
		//netP2pCrypt::GenerateRandomBytes(hmacKey, keyLen);
		//netP2pCrypt::GenerateRandomBytes(hmacSalt, saltLen);
		//netP2pCrypt::SetHmacKeyAndSalt(hmacKey, keyLen, hmacSalt, saltLen);

		//const unsigned sizeOfData = 10240;
		//u8 data[sizeOfData];

		//u8 encodedData[sizeof(data) + MAX_SIZEOF_P2P_CRYPT_OVERHEAD];
		//unsigned sizeOfEncodedData = sizeof(encodedData);

		//netP2pCrypt::GenerateRandomBytes(data, sizeof(data));

		//netP2pCrypt::Key p2pKey;
		//netP2pCrypt::GenerateRandomKey(p2pKey);

		//bool success = netP2pCrypt::Encrypt(p2pKey, data, sizeOfData, encodedData, sizeOfEncodedData);
		//netTestAssert(success);


		//netRandom r(sysTimer::GetSystemMsTime());

		//success = true;

		//// test changing each byte in turn and make sure we detect the modification
		//for(unsigned i = 0; (i < sizeof(data)) && success; ++i)
		//{
		//	u8 encodedDataCopy[sizeof(encodedData)];
		//	sysMemCpy(encodedDataCopy, encodedData, sizeOfEncodedData);

		//	u8* decryptedData = encodedDataCopy;
		//	unsigned sizeOfDecryptedData = sizeOfEncodedData;

		//	// damage one byte
		//	while(encodedDataCopy[i] == encodedData[i])
		//	{
		//		encodedDataCopy[i] = (u8)r.GetRanged(0, 255);
		//	}

		//	// make sure we detect the modification
		//	sizeOfDecryptedData = sizeOfEncodedData;
 	//		success = success && netP2pCrypt::Decrypt(p2pKey, &decryptedData, sizeOfDecryptedData) == false;
 	//		success = success && (memcmp(data, decryptedData, sizeOfData) != 0);

		//	// repair the damage and retest
		//	sysMemCpy(encodedDataCopy, encodedData, sizeOfEncodedData);
		//	decryptedData = encodedDataCopy;
		//	sizeOfDecryptedData = sizeOfEncodedData;
 	//		success = success && netP2pCrypt::Decrypt(p2pKey, &decryptedData, sizeOfDecryptedData);
 	//		success = success && (sizeOfDecryptedData == sizeOfData);
 	//		success = success && (memcmp(data, decryptedData, sizeOfData) == 0);
		//}

		//netTestAssert(success);

		//netP2pCrypt::Shutdown();
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestDiffieHellman)
	{
		/*netP2pCrypt::Init();*/

		/*class DhPeer
		{
		public:
			bool Init()
			{
				return dh.InitDefault1024BitGroup();
			}

			bool GenerateKeys()
			{
				bool success = Init();
				if(!success) return false;

				success = dh.GenerateKeyPair();
				if(!success) return false;

				dhSizeOfPublicKey = sizeof(dhPublicKey);
				return dh.ExportPublicKey(dhPublicKey, &dhSizeOfPublicKey);
			}

			bool Agree(const u8* otherPublicKey, const unsigned otherPublicKeySize)
			{
				dhSizeOfAgreeKey = sizeof(dhAgreeKey);
				return dh.Agree(otherPublicKey, otherPublicKeySize, dhAgreeKey, &dhSizeOfAgreeKey);
			}

			u8 dhPublicKey[netCrypto::DiffieHellman::MAX_PUBLIC_KEY_LENGTH_IN_BYTES];
			unsigned dhSizeOfPublicKey;

			u8 dhAgreeKey[netCrypto::DiffieHellman::MAX_AGREE_KEY_LENGTH_IN_BYTES];
			unsigned dhSizeOfAgreeKey;

			netCrypto::DiffieHellman dh;
		};

		const unsigned numIterations = 256;*/

		// test for correctness
		{
			/*bool success = true;
			for(unsigned i = 0; (i < numIterations) && success; ++i)
			{
				DhPeer alice;
				DhPeer bob;

				success = success && alice.GenerateKeys();
				success = success && bob.GenerateKeys();

				success = success && alice.Agree(bob.dhPublicKey, bob.dhSizeOfPublicKey);
				success = success && bob.Agree(alice.dhPublicKey, alice.dhSizeOfPublicKey);

				success = success && (alice.dhSizeOfAgreeKey == bob.dhSizeOfAgreeKey);
				success = success && (memcmp(alice.dhAgreeKey, bob.dhAgreeKey, alice.dhSizeOfAgreeKey) == 0);
			}

			netTestAssert(success);*/
		}
		
		// speed test Init function
		{
//			bool success = true;
//			DhPeer alice;
//            OUTPUT_ONLY(unsigned startTime = sysTimer::GetSystemMsTime();)
//			for(unsigned i = 0; (i < numIterations) && success; ++i)
//			{
//				success = success && alice.Init();
//            }
//            netTestAssert(success);
//
//#if !__NO_OUTPUT
//            unsigned elasped = sysTimer::GetSystemMsTime() - startTime;
//            float msPerOp = (float)elasped / numIterations;
//            Displayf("TestDiffieHellman - Init took %u ms for %u iterations. %.4f ms per iteration.", elasped, numIterations, msPerOp);
//#endif //  !__NO_OUTPUT

		}

		// speed test GenerateKeys function
		{
//            bool success = true;
//            OUTPUT_ONLY(unsigned startTime = sysTimer::GetSystemMsTime();)
//			for(unsigned i = 0; (i < numIterations) && success; ++i)
//			{
//				DhPeer alice;
//				success = success && alice.GenerateKeys();
//            }
//            netTestAssert(success);
//
//#if !__NO_OUTPUT
//			unsigned elasped = sysTimer::GetSystemMsTime() - startTime;
//			float msPerOp = (float)elasped / numIterations;
//			Displayf("TestDiffieHellman - GenerateKeys took %u ms to generate %u keys. %.4f ms per iteration.", elasped, numIterations, msPerOp);
//#endif //  !__NO_OUTPUT
		}

		// speed test Agree function
		{
//			DhPeer bob;
//			bool success = bob.GenerateKeys();
//
//			DhPeer alice;
//			success = success && alice.GenerateKeys();
//
//            OUTPUT_ONLY(unsigned startTime = sysTimer::GetSystemMsTime();)
//			for(unsigned i = 0; (i < numIterations) && success; ++i)
//			{
//				success = success && alice.Agree(bob.dhPublicKey, bob.dhSizeOfPublicKey);
//            }
//            netTestAssert(success);
//
//#if !__NO_OUTPUT
//			unsigned elasped = sysTimer::GetSystemMsTime() - startTime;
//			float msPerOp = (float)elasped / numIterations;
//			Displayf("TestDiffieHellman - Agree took %u ms to agree on %u keys. %.4f ms per iteration.", elasped, numIterations, msPerOp);
//#endif //  !__NO_OUTPUT
		}

		/*netP2pCrypt::Shutdown();*/
	}
	NET_TEST_METHOD_END

}
NET_TEST_CLASS_END(Test_netCrypto)

#endif // UNITTEST_ENABLE_NET_CRYPTO
