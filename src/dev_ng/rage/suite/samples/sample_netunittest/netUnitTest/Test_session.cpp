
#include "netUnitTestFramework.h"
#include "snet/session.h"
#include "system/simpleallocator.h"
#include "rline/rltitleid.h"
#include "net/nethardware.h"


#if RSG_PC && !__NO_OUTPUT
namespace rage {
	XPARAM(processinstance);
	XPARAM(scDll);
	XPARAM(scemail);
	XPARAM(scpassword);
}
#endif // __WIN32PC && !__FINAL

enum class ChannelIds : unsigned
{
	NetReserved	  = 1,
	NatTraversal  = 10,
	NetConnection = 13
};

using namespace rage;

NET_TEST_CLASS(Test_snSession)
{
	NET_TEST_METHOD(InitAndShutdown)
	{
		bool result = false;
		
		// Setup
		const unsigned heapSize = 1024 * 1024;
		u8* netHeap = rage_new u8[heapSize];
		sysMemSimpleAllocator netAllocator(netHeap, heapSize, sysMemSimpleAllocator::HEAP_NET);
		//net can handle out of memory situations so configure the
		//allocator to not Quitf() on failure.
		netAllocator.SetQuitOnFail(false);

		Assert(netInit(&netAllocator, 0, 0));
		
		// don't load the Social Club DLL
		PARAM_scDll.Set("none");

		PARAM_scemail.Set("netUnitTestSc1@rockfoo.com");
		PARAM_scpassword.Set("Password1");

		Assert(netRelay::IsInitialized());
		Assert(netRelay::GetSocket());
		netSocket* sock = netRelay::GetSocket();

		rlTitleId* titleId = nullptr;
		{
			Assert(titleId == nullptr);

			const char* ROS_TITLE_NAME = "GTA5";
			const char* TITLE_DIRECTORY_NAME = "GTA5QA";
			int ROS_SC_VERSION = 11;
			int ROS_TITLE_VERSION = 11;
			const char* ROS_TITLE_SECRETS = "C4pWJwWIKGUxcHd69eGl2AOwH2zrmzZAoQeHfQFcMelybd32QFw9s10px6k0o75XZeB5YsI9Q9TdeuRgdbvKsxc=";
			const rlRosEnvironment ROS_ENV = RLROS_ENV_DEV;

			rlRosTitleId rosTitleId(ROS_TITLE_NAME, TITLE_DIRECTORY_NAME, ROS_SC_VERSION, ROS_TITLE_VERSION, ROS_TITLE_SECRETS, ROS_ENV, NULL, 0);
			titleId = rage_new rlTitleId(rosTitleId);

			Assert(titleId);	
		}

		// Initialise rline
		result = rlInit(&netAllocator, sock, titleId, 0);
		Assert(result);

		snSessionOwner owner;
		owner.GetGamerData.Bind(
			[](snSession*, const rlGamerInfo&, snGetGamerData*) -> void 
			{
				// garbage do-nothing layabout lambda
			});

		owner.HandleJoinRequest.Bind(
			[](snSession*, const rlGamerInfo&, snJoinRequest*) -> bool
			{
				return true;
			});

		netConnectionManager cxnMgr;
		Assert(cxnMgr.Init(&netAllocator, 
						   1, 
						   1, 
						   sock, 
						   (unsigned)ChannelIds::NetReserved, 
						   (unsigned)ChannelIds::NatTraversal, 
						   0));
		Assert(cxnMgr.IsInitialized());

		// Set up session
		snSession session;
		result = session.Init(&netAllocator, 
							  owner, 
							  &cxnMgr, 
							  0, 
							  OUTPUT_ONLY("TEST_SESSION"));

		netTestAssert(result);

		// Attempt to host a session.
		{
			const int localGamerIndex = 1;
			const int maxPubSlots = 32;
			const int maxPrivSlots = 0;
			const unsigned createFlags = 0;
			const rlNetworkMode networkMode = rlNetworkMode::RL_NETMODE_OFFLINE; 
			rlMatchingAttributes attrs;
			u8 gamerData[10];
			unsigned sizeofGamerData = sizeof(gamerData);
			netStatus status;

			result = session.Host(localGamerIndex, 
								  networkMode, 
								  maxPubSlots, 
								  maxPrivSlots, 
								  attrs, 
								  createFlags, 
								  gamerData, 
								  sizeofGamerData, 
								  &status);
	
			netTestAssert(result);

			// More tests for successful hosting will go here...
		
			// Close session...

			// Session closing tests...
		}

		session.Shutdown(false);

		// Tests for successful session cleanup will go here...
		
		cxnMgr.Shutdown();

		rlShutdown();

		netHardware::DestroySocket(sock);

		netShutdown();

		// Clean up
		delete titleId;
		delete[] netHeap;

	}
	NET_TEST_METHOD_END
}
NET_TEST_CLASS_END(Test_snSession)