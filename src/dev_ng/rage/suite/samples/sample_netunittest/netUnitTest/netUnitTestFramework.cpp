
#include "netUnitTestFramework.h"


#if USE_MS_UNIT_TEST_FRAMEWORK


#include "system/main.h"

int Main()
{
	return 0;
}

XPARAM(nographics);

#else

#if RSG_PC || RSG_DURANGO
#pragma warning(push)
#pragma warning(disable: 4668)
#include <windows.h>
#pragma warning(pop)
#endif

#include <iostream>

#include "input/keyboard.h"
#include "system/param.h"

using namespace rage;

PARAM(failsOnly, "Only the failed tests are displayed, passed tests are silent");
PARAM(breakOnRageAssert, "Break if a rage assert is hit (if the debugger is attached ofc)");
XPARAM(nographics);

/////////////////////////////////////////////////////////////////////////////////////////////////// Static members



static int currentTestIndex = 0;

bool netUnitTest::sm_hasFailed = false;
bool netUnitTest::sm_IsExpectingFailures = false;
static int sm_assertFiredCount = 0;

bool netUnitTest::sm_ignoreRageAssert = false;



std::vector<netUnitTest*>& TestList()
{
	static std::vector<netUnitTest*> sm_Tests;
	return sm_Tests;
}

void netUnitTest::registerTest(netUnitTest* test)
{
	TestList().push_back(test);
}

void netUnitTest::prepareTests()
{
#if __ASSERT
	//diagSetLogCallback(netUnitTest::diagLogUnitTest);
#endif // __ASSERT

	std::vector<netUnitTest*> testList = TestList();
	for(auto test : testList)
	{
		test->prepare();
	}
}

bool netUnitTest::runNext()
{
	// reset sm_IsExpectingFailures
	netUnitTest::sm_IsExpectingFailures = false;

	std::vector<netUnitTest*> testList = TestList();
	if(currentTestIndex < testList.size())
	{
		netUnitTest* test = testList[currentTestIndex];
		if(!test->runNextMethod()) // returns false if we ran all the methods in this test
		{
			currentTestIndex++;
		}
		return true;
	}
	return false;
}

//void Draw2dText(sga::Font* font, float x,float y,const char *s, Color32 color, float scale)
//{
//	font->DrawString(*sga::g_GraphicsContext,x,y,0,scale,color.GetColor(), s);
//}

//float netUnitTest::draw(sga::Font* font, float yOffset)
//{
//	float y = TEST_MARGIN_TOP + 50.0f + yOffset;
//
//	std::vector<netUnitTest*> testList = TestList();
//	for(auto test : testList)
//	{
//		if(PARAM_failsOnly.Get() && !test->hasFailed())
//			continue;
//
//		// Display the method name
//		Draw2dText(font, TEST_MARGIN_LEFT , y, test->getTestName(), COLOR_WHITE, SUBTITLE_FONT_SIZE);
//		y+=30.0f;
//
//		for(auto testMethod : test->m_testMethods)
//		{
//			if(PARAM_failsOnly.Get() && !testMethod.hasFailed())
//				continue;
//
//			// Display the method name
//			Draw2dText(font, TEST_MARGIN_LEFT , y, testMethod.m_name.c_str(), COLOR_GRAY, TEST_FONT_SIZE);
//			Draw2dText(font, TEST_MARGIN_LEFT +TITLE_OFFSET_X , y, "[", COLOR_GRAY, TEST_FONT_SIZE);
//
//			int i=0;
//			for(i=0; i<testMethod.m_result.size(); i++)
//			{
//				char r = testMethod.m_result.at(i);
//				if(r == '-')
//				{
//					Draw2dText(font, TEST_MARGIN_LEFT+TITLE_OFFSET_X + (i+1)*OFFSET_X , y, TEST_CHECK_SUCCESS_CHARACTER, COLOR_GREEN, TEST_FONT_SIZE);
//				}
//				else
//				{
//					Draw2dText(font, TEST_MARGIN_LEFT+TITLE_OFFSET_X + (i+1)*OFFSET_X , y, TEST_CHECK_FAILURE_CHARACTER, COLOR_RED, TEST_FONT_SIZE);
//				}
//			}
//
//			Draw2dText(font, TEST_MARGIN_LEFT+TITLE_OFFSET_X + (i+1)*OFFSET_X , y, "]", COLOR_GRAY, TEST_FONT_SIZE);
//
//			if(testMethod.m_errors.size())
//			{
//				y+=20.0f;
//				for(i=0; i<testMethod.m_errors.size(); i++)
//				{
//					if(testMethod.m_errors[i].m_type == NET_TEST_RAGE_ASSERT)
//					{
//						Draw2dText(font, TEST_MARGIN_LEFT+ERROR_MARGIN_LEFT , y, testMethod.m_errors[i].m_str.c_str(), COLOR_PURPLE, TEST_FONT_SIZE);
//					}
//					if(testMethod.m_errors[i].m_type == NET_TEST_TEST_CHECK)
//					{
//						Draw2dText(font, TEST_MARGIN_LEFT+ERROR_MARGIN_LEFT , y, testMethod.m_errors[i].m_str.c_str(), COLOR_RED, TEST_FONT_SIZE);
//					}
//					y+=20.0f;
//				}
//			}
//			y+=20.0f;
//		}
//		y+=10.0f;
//	}
//
//	return y - yOffset;
//}

/////////////////////////////////////////////////////////////////////////////////////////////////// Instances members

netUnitTest::netUnitTest()
	: m_nextMethodToRun(0)
{
	netUnitTest::registerTest(this);
}

netUnitTest::~netUnitTest()
{

}

void netUnitTest::addTestMethod(const std::string& name, TestMethod method)
{
	TestMethodInfo tmi;
	tmi.m_name = name;
	tmi.m_test = method;

	m_testMethods.push_back(tmi);
}

bool netUnitTest::runNextMethod()
{
	if(m_nextMethodToRun < m_testMethods.size())
	{
		m_testMethods[m_nextMethodToRun].m_test();
		m_nextMethodToRun++;
		return true;
	}
	return false;
}

bool netUnitTest::testsAllOk()
{
	return !sm_hasFailed;
}

bool netUnitTest::testAssert(bool cond, const char* fmt, ...)
{
	if(!cond)
	{
		m_testMethods[m_nextMethodToRun].m_result += "X";
		char message[512] = { 0 } ;
		va_list args;
		va_start(args, fmt);
		rage::vformatf_n(message, 512, fmt, args);
		va_end(args);

		TestError error;
		error.m_type = NET_TEST_TEST_CHECK;
		error.m_str = message;
		m_testMethods[m_nextMethodToRun].m_errors.push_back(error);

		sm_hasFailed = true;
		return true;
	}
	else
	{
		m_testMethods[m_nextMethodToRun].m_result += "-";
	}
	return false;
}


void netUnitTest::startExpectAsserts()
{
	sm_ignoreRageAssert = true; 
	sm_assertFiredCount = 0;
}

void netUnitTest::checkAssertCountAndClean(int count)
{
	(void) count;
#if __ASSERT
	if(sm_assertFiredCount != count)
	{
		std::vector<netUnitTest*> testList = TestList();
		if(currentTestIndex < testList.size())
		{
			netUnitTest* test = testList[currentTestIndex];
			if(test->m_nextMethodToRun < test->m_testMethods.size())
			{
				char message[512] = { 0 } ;
				rage::formatf(message, "Expected asserts : %d but %d fired", count, sm_assertFiredCount);

				TestError error;
				error.m_type = netUnitTest::NET_TEST_RAGE_ASSERT;
				error.m_str = message;
				test->m_testMethods[test->m_nextMethodToRun].m_errors.push_back(error);
			}
		}
	}
#endif  // __ASSERT
	sm_assertFiredCount = 0;
	sm_ignoreRageAssert = false; 
}

void netUnitTest::testLog(const char* fmt, ...)
{
	char message[512] = { 0 } ;
	va_list args;
	va_start(args, fmt);
	rage::vformatf_n(message, 512, fmt, args);
	va_end(args);
	std::cout << message << std::endl;
}

bool netUnitTest::hasFailed() const
{
	for(auto testMethod : m_testMethods)
	{
		if(testMethod.m_errors.size() > 0)
			return true;
	}
	return false;
}

#if __ASSERT
bool netUnitTest::diagLogUnitTest(const rage::diagChannel &channel, rage::diagSeverity severity, rage::u64 quitfFlags,const char *file,int line,const char *fmt, va_list args)
{
	(void) channel;
	(void) quitfFlags;
	(void) file;
	(void) line;
	(void) fmt;
	(void) args;

	if( severity > rage::DIAG_SEVERITY_ERROR)
	{
		char message[512] = { 0 } ;
		rage::vformatf_n(message, 512, fmt, args);
		std::cout << message << std::endl;

#if RSG_PC || RSG_DURANGO
		if (IsDebuggerPresent())
		{
			OutputDebugStringA(message);
			OutputDebugStringA("\n");
		}
#endif

		return true;
	}

	if (netUnitTest::sm_IsExpectingFailures || sm_ignoreRageAssert)
	{
		char message[512] = { 0 } ;
		rage::vformatf_n(message, 512, fmt, args);
		std::cout << message << std::endl;

#if RSG_PC || RSG_DURANGO
		if (IsDebuggerPresent())
		{
			OutputDebugStringA(message);
			OutputDebugStringA("\n");
		}
#endif
		sm_assertFiredCount++;
		return true;
	}

	if(PARAM_breakOnRageAssert.Get())
	{
		// Hey, some assert fired from Rage :(
		__debugbreak();
	}

	std::vector<netUnitTest*> testList = TestList();
	if(currentTestIndex < testList.size())
	{
		netUnitTest* test = testList[currentTestIndex];
		if(test->m_nextMethodToRun < test->m_testMethods.size())
		{
			char message[512] = { 0 } ;
			rage::vformatf_n(message, 512, fmt, args);

			std::cout << "ASSERT FAILED " << message << std::endl;

#if RSG_PC || RSG_DURANGO
			if (IsDebuggerPresent())
			{
				OutputDebugStringA(message);
				OutputDebugStringA("\n");
			}

#endif

			TestError error;
			error.m_type = netUnitTest::NET_TEST_RAGE_ASSERT;
			error.m_str = "RAGE_ASSERT: ";//+std::string(message);
			test->m_testMethods[test->m_nextMethodToRun].m_errors.push_back(error);
		}
	}

	return true;
}
#endif // __ASSERT

#endif // USE_MS_UNIT_TEST_FRAMEWORK