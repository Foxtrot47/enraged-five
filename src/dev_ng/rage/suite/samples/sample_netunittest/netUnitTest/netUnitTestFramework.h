#pragma once

#include "testmatrix.h"

// Display constants

#if RSG_ORBIS
#define TEST_MARGIN_LEFT		100.0f
#define ERROR_MARGIN_LEFT		60.0f
#define TEST_MARGIN_TOP			65.0f
#else
#define TEST_MARGIN_LEFT		60.0f
#define ERROR_MARGIN_LEFT		20.0f
#define TEST_MARGIN_TOP			25.0f
#endif

#define TITLE_FONT_SIZE			1.8f
#define SUBTITLE_FONT_SIZE		1.4f
#define TEST_FONT_SIZE			1.0f

#define OFFSET_X				10.0f
#define TITLE_OFFSET_X			300.0f

#define TEST_CHECK_SUCCESS_CHARACTER "-"
#define TEST_CHECK_FAILURE_CHARACTER "X"

///////////////////////////////////////////////////////////////////////////////////////////////////// MACROS WIZARDRY

// If we're using MS framework, our macros will just match MS macros
// If not, the macros will create netUnitTest classes

#if USE_MS_UNIT_TEST_FRAMEWORK


#include "CppUnitTest.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;


#define NET_TEST_CLASS(name)					TEST_CLASS(name) 
#define NET_TEST_CLASS_END(name)				;
		
#define NET_TEST_INIT(name)						TEST_CLASS_INITIALIZE(name) 

#define NET_TEST_METHOD(name)					public: TEST_METHOD(name)
#define NET_TEST_METHOD_END						;
		

#define netTestLog(fmt, ...)					char str[128]; Logger::WriteMessage(formatf(str, fmt, ##__VA_ARGS__));

#define netTestAssert							Assert::IsTrue 
#define netTestAssertEqual						Assert::AreEqual
#define netTestAssertNotEqual					Assert::AreEqual
#define netTestAssertExpectAssert				Assert::IsTrue

#define netTestBeginExpectAssert()				;
#define netTestEndExpectAssert(count)			;

#else

#include <functional>
#include <vector>

#include "input/input.h"
//#include "sga/sga_font.h"
//#include "sga/sga_image.h"
//#include "sga/sga_setup.h"
//#include "sga/sga_debug_renderer.h"
#include "diag/channel.h"

#define NET_TEST_CLASS(name)					\
class name : public netUnitTest					\
{												\
public :										\
	static name sm_instance;					\
	const char* getTestName() { return #name; }	\
	void prepare()								\
	{											

#define NET_TEST_CLASS_END(name)				\
	}											\
};												\
name sm_TestInstance_##name;


#define NET_TEST_INIT(name)						;

// this macro will wrap your test code in a lambda function and push it into the test queue for this test class.
#define NET_TEST_METHOD(name)					addTestMethod(#name, [this]() mutable
// your test code is gonna be here 
#define NET_TEST_METHOD_END						);


#define netTestLog(fmt, ...)					netUnitTest::testLog(fmt, ##__VA_ARGS__)

// Asserts macros
#define netTestAssert(a)						if(testAssert(a, "Line %d : Assert(%s) ", __LINE__, #a)) __debugbreak();
#define netTestAssertEqual(a, b)				if(testAssert(a == b, "Line %d : AssertEqual(%s, %s) ", __LINE__, #a, #b)) __debugbreak();
#define netTestAssertNotEqual(a, b)				if(testAssert(a != b, "Line %d : AssertNotEqual(%s, %s) ", __LINE__, #a, #b)) __debugbreak();

#define netTestAssertExpectAssert(a)			netUnitTest::sm_ignoreRageAssert = true; if(testAssert(a, "Line %d : Assert(%s) ", __LINE__, #a)) __debugbreak(); netUnitTest::sm_ignoreRageAssert = false;

#define netTestBeginExpectAssert()				startExpectAsserts()
#define netTestEndExpectAssert(count)			checkAssertCountAndClean(count)





//static const rage::Color32 COLOR_WHITE(1.0f, 1.0f, 1.0f);
//static const rage::Color32 COLOR_GRAY(0.3f, 0.3f, 0.3f);
//static const rage::Color32 COLOR_GREEN(0.0f, 1.0f, 0.0f);
//static const rage::Color32 COLOR_RED(1.0f, 0.0f, 0.0f);
//static const rage::Color32 COLOR_LIGHT_RED(1.0f, 0.1f, 0.1f);
//static const rage::Color32 COLOR_PURPLE(0.61f,0.19f,1.00f);





class netUnitTest
{
	typedef std::function<void()> TestMethod;

public:

	static void prepareTests();
	static void registerTest(netUnitTest* test);
	static bool runNext();
	//static float draw(rage::sga::Font* font, float yOffset);

	//! Returns true if all the tests have succeeded
	static bool testsAllOk();

	netUnitTest();
	virtual ~netUnitTest();

	//! prepare will fill the TestMethod list
	virtual void prepare() = 0;
	virtual const char* getTestName() = 0;



	//! Returns true if the test instance has failed (meaning at least one of its test method failed)
	bool hasFailed() const;

	//! Logs to stdout
	static void testLog(const char* fmt, ...);

		static bool sm_ignoreRageAssert;

protected:

	void addTestMethod(const std::string& name, TestMethod method);

	bool testAssert(bool cond, const char* fmt, ...);

	//! Sometimes you might want to check a failure case and expect your code to assert, surround it with the macros block
	void startExpectAsserts();
	//! When you use the macros to expect asserts to fire, we need to check that the correct amount of expected asserts fired, then clean the counter
	void checkAssertCountAndClean(int count);

#if __ASSERT
	//! redirect asserts to our function since RFS wont probably be here
	static bool diagLogUnitTest(const rage::diagChannel &channel, rage::diagSeverity severity, rage::u64 quitfFlags,const char *file,int line,const char *fmt, va_list args);
#endif // __ASSERT

	static bool sm_hasFailed;
	static bool sm_IsExpectingFailures;

private:

	static const int NET_TEST_TEST_CHECK = 0;
	static const int NET_TEST_RAGE_ASSERT = 1;

	bool runNextMethod();
	int m_nextMethodToRun;


	struct TestError
	{
		int m_type;
		std::string m_str;
	};

	//! A test method is composed of a name, a lambda to run, and a result.
	struct TestMethodInfo
	{
		std::string m_name;
		TestMethod m_test;
		std::string m_result;
		std::vector<TestError> m_errors;

		bool hasFailed() const
		{
			return m_errors.size() > 0;
		}
	};

	//! this is the list of tests methods that will be run sequentially.
	//! each call to runNextMethod will run the next method in the list, and increment the iterator.
	std::vector<TestMethodInfo> m_testMethods;

};


#endif // USE_MS_UNIT_TEST_FRAMEWORK