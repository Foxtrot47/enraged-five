

#include "netUnitTestFramework.h"

//#include "net/uri.h"

using namespace rage;

NET_TEST_CLASS(Test_netUri)
{
	NET_TEST_INIT(Init_netUri)
	{

	}

	// Uri parsing, extracting scheme, host port and path from a uri
	NET_TEST_METHOD(ParseUri)
	{
		//netUri infos;
		//bool result;
  //      char rebuiltUri[512];

		//// Uri with only a host
		//const char* uri = "ws://localhost";
		//result = netUri::Parse(uri, infos);

		//netTestAssert(result);
		//netTestAssert(strcmp(infos.m_Host, "localhost") == 0);
		//netTestAssertEqual(infos.m_Port, (short)0);
		//netTestAssert(infos.m_Scheme == URISCHEME_WS);
  //      netTestAssert(strcmp(infos.m_Path, "") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);

  //      infos.GetCompleteUri(rebuiltUri);
  //      netTestAssert(strcmp(uri, rebuiltUri) == 0);

		//// Uri with only a host and an empty path
		//infos.Reset();
		//uri = "ws://localhost/";
		//result = netUri::Parse(uri, infos);

		//netTestAssert(result);
		//netTestAssert(strcmp(infos.m_Host, "localhost") == 0);
		//netTestAssertEqual(infos.m_Port, (short)0);
		//netTestAssert(infos.m_Scheme == URISCHEME_WS);
  //      netTestAssert(strcmp(infos.m_Path, "/") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);

  //      infos.GetCompleteUri(rebuiltUri);
  //      netTestAssert(strcmp(uri, rebuiltUri) == 0);

		//// Uri with host and port
		//infos.Reset();
		//uri = "ws://localhost:8181";
		//result = netUri::Parse(uri, infos);

		//netTestAssert(result);
		//netTestAssert(strcmp(infos.m_Host, "localhost") == 0);
		//netTestAssertEqual(infos.m_Port, (short)8181);
		//netTestAssert(infos.m_Scheme == URISCHEME_WS);
  //      netTestAssert(strcmp(infos.m_Path, "") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);

  //      infos.GetCompleteUri(rebuiltUri);
  //      netTestAssert(strcmp(uri, rebuiltUri) == 0);

		//// Uri with host, port and more after
		//infos.Reset();
		//uri = "wss://localhost:8181/path/after/host";
		//result = netUri::Parse(uri, infos);

		//netTestAssert(result);
		//netTestAssert(strcmp(infos.m_Host, "localhost") == 0);
		//netTestAssertEqual(infos.m_Port, (short)8181);
		//netTestAssert(infos.m_Scheme == URISCHEME_WSS);
  //      netTestAssert(strcmp(infos.m_Path, "/path/after/host") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);

  //      infos.GetCompleteUri(rebuiltUri);
  //      netTestAssert(strcmp(uri, rebuiltUri) == 0);

		//// Uri without a port
		//infos.Reset();
		//uri = "https://localhost/someMorePath";
		//result = netUri::Parse(uri, infos);

		//netTestAssert(result);
		//netTestAssert(strcmp(infos.m_Host, "localhost") == 0);
		//netTestAssertEqual(infos.m_Port, (short)0);
		//netTestAssert(infos.m_Scheme == URISCHEME_HTTPS);
  //      netTestAssert(strcmp(infos.m_Path, "/someMorePath") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);

  //      infos.GetCompleteUri(rebuiltUri);
  //      netTestAssert(strcmp(uri, rebuiltUri) == 0);


		//// Path with extra spaces
		//infos.Reset();
		//uri = "https://localhost/someMorePath/     ";
		//result = netUri::Parse(uri, infos);

		//netTestAssert(result);
		//netTestAssert(strcmp(infos.m_Host, "localhost") == 0);
		//netTestAssertEqual(infos.m_Port, (short)0);
		//netTestAssert(infos.m_Scheme == URISCHEME_HTTPS);
  //      netTestAssert(strcmp(infos.m_Path, "/someMorePath/") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);

  //      // Path with extra spaces
  //      infos.Reset();
  //      uri = "https://localhost/some More Path with spaces/     ";
  //      result = netUri::Parse(uri, infos);

  //      netTestAssert(result);
  //      netTestAssert(strcmp(infos.m_Host, "localhost") == 0);
  //      netTestAssertEqual(infos.m_Port, (short)0);
  //      netTestAssert(infos.m_Scheme == URISCHEME_HTTPS);
  //      netTestAssert(strcmp(infos.m_Path, "/some+More+Path+with+spaces/") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);


		//// Uri with a wrong port
		//infos.Reset();
		//uri = "http://localhost:notAPort";
		//netTestBeginExpectAssert();
		//result = netUri::Parse(uri, infos);
		//netTestEndExpectAssert(2); // 2 because rverify throws two diagError

		//netTestAssert(!result);
		//netTestAssert(strcmp(infos.m_Host, "") == 0);
		//netTestAssertEqual(infos.m_Port, (short)0);
		//netTestAssert(infos.m_Scheme == URISCHEME_INVALID);
  //      netTestAssert(strcmp(infos.m_Path, "") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);


  //      // More tests using URI from the game
  //      infos.Reset();
  //      uri = "https://rest.bugstar.rockstargames.com:8443/";
  //      result = netUri::Parse(uri, infos);

  //      netTestAssert(result);
  //      netTestAssert(strcmp(infos.m_Host, "rest.bugstar.rockstargames.com") == 0);
  //      netTestAssertEqual(infos.m_Port, (short)8443);
  //      netTestAssert(infos.m_Scheme == URISCHEME_HTTPS);
  //      netTestAssert(strcmp(infos.m_Path, "/") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);

  //      infos.GetCompleteUri(rebuiltUri);
  //      netTestAssert(strcmp(uri, rebuiltUri) == 0);

  //      infos.Reset();
  //      uri = "http://dev.ros.rockstargames.com/cloud";
  //      result = netUri::Parse(uri, infos);

  //      netTestAssert(result);
  //      netTestAssert(strcmp(infos.m_Host, "dev.ros.rockstargames.com") == 0);
  //      netTestAssertEqual(infos.m_Port, (short)0);
  //      netTestAssert(infos.m_Scheme == URISCHEME_HTTP);
  //      netTestAssert(strcmp(infos.m_Path, "/cloud") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);

  //      infos.GetCompleteUri(rebuiltUri);
  //      netTestAssert(strcmp(uri, rebuiltUri) == 0);


  //      infos.Reset();
  //      uri = "http://dev.cloud.rockstargames.com/titles";
  //      result = netUri::Parse(uri, infos);

  //      netTestAssert(result);
  //      netTestAssert(strcmp(infos.m_Host, "dev.cloud.rockstargames.com") == 0);
  //      netTestAssertEqual(infos.m_Port, (short)0);
  //      netTestAssert(infos.m_Scheme == URISCHEME_HTTP);
  //      netTestAssert(strcmp(infos.m_Path, "/titles") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);

  //      infos.GetCompleteUri(rebuiltUri);
  //      netTestAssert(strcmp(uri, rebuiltUri) == 0);


  //      infos.Reset();
  //      uri = "http://10.24.22.146:7890";
  //      result = netUri::Parse(uri, infos);

  //      netTestAssert(result);
  //      netTestAssert(strcmp(infos.m_Host, "10.24.22.146") == 0);
  //      netTestAssertEqual(infos.m_Port, (short)7890);
  //      netTestAssert(infos.m_Scheme == URISCHEME_HTTP);
  //      netTestAssert(strcmp(infos.m_Path, "") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);

  //      infos.GetCompleteUri(rebuiltUri);
  //      netTestAssert(strcmp(uri, rebuiltUri) == 0);

	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(ParseUri2)
	{
		//netUri infos;
  //      bool result;
  //      char rebuiltUri[512];

  //      // String query
  //      infos.Reset();
  //      const char* uri = "https://www.google.co.uk/search?q=uri";
  //      result = netUri::Parse(uri, infos);

  //      netTestAssert(result);
  //      netTestAssert(strcmp(infos.m_Host, "www.google.co.uk") == 0);
  //      netTestAssertEqual(infos.m_Port, (short)0);
  //      netTestAssert(infos.m_Scheme == URISCHEME_HTTPS);
  //      netTestAssert(strcmp(infos.m_Path, "/search") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "?q=uri") == 0);

  //      infos.GetCompleteUri(rebuiltUri);
  //      netTestAssert(strcmp(uri, rebuiltUri) == 0);

  //      // Empty string query
  //      infos.Reset();
  //      uri = "https://www.google.co.uk/?";
  //      result = netUri::Parse(uri, infos);

  //      netTestAssert(result);
  //      netTestAssert(strcmp(infos.m_Host, "www.google.co.uk") == 0);
  //      netTestAssertEqual(infos.m_Port, (short)0);
  //      netTestAssert(infos.m_Scheme == URISCHEME_HTTPS);
  //      netTestAssert(strcmp(infos.m_Path, "/") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "?") == 0);

  //      infos.GetCompleteUri(rebuiltUri);
  //      netTestAssert(strcmp(uri, rebuiltUri) == 0);

  //      // Full Uri
  //      infos.Reset();
  //      uri = "https://hostname:1337/this/is/the/path?andThis=isTheQueryString&with=parameter";
  //      result = netUri::Parse(uri, infos);

  //      netTestAssert(result);
  //      netTestAssert(strcmp(infos.m_Host, "hostname") == 0);
  //      netTestAssertEqual(infos.m_Port, (short)1337);
  //      netTestAssert(infos.m_Scheme == URISCHEME_HTTPS);
  //      netTestAssert(strcmp(infos.m_Path, "/this/is/the/path") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "?andThis=isTheQueryString&with=parameter") == 0);

  //      infos.GetCompleteUri(rebuiltUri);
  //      netTestAssert(strcmp(uri, rebuiltUri) == 0);

  //      // Full Uri
  //      infos.Reset();
  //      uri = "https://hostname:1337/this/is/the/path/?QueryString";
  //      result = netUri::Parse(uri, infos);

  //      netTestAssert(result);
  //      netTestAssert(strcmp(infos.m_Host, "hostname") == 0);
  //      netTestAssertEqual(infos.m_Port, (short)1337);
  //      netTestAssert(infos.m_Scheme == URISCHEME_HTTPS);
  //      netTestAssert(strcmp(infos.m_Path, "/this/is/the/path/") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "?QueryString") == 0);

  //      infos.GetCompleteUri(rebuiltUri);
  //      netTestAssert(strcmp(uri, rebuiltUri) == 0);



  //      // Insanely long Uris
  //      infos.Reset();
  //      uri = "https://omg_look_at_this_i_have_never_seen_an_uri_that_long_Lorem_ipsum_dolor_sit_amet__consectetur_adipiscing_elit__sed_do_eiusmod_tempor_incididunt_ut_labore_et_dolore_magna_aliqua._Ut_enim_ad_minim_veniam__quis_nostrud_exercitation_ullamco_laboris_nisi_ut_aliquip_ex_ea_commodo_consequat._Duis_aute_irure_dolor_in_reprehenderit_in_voluptate_velit_esse_cillum_dolore_eu_fugiat_nulla_pariatur._Excepteur_sint_occaecat_cupidatat_non_proident__sunt_in_culpa_qui_officia_deserunt_mollit_anim_id_est_laborum._Lorem_ipsum_dolor_sit_amet__consectetur_adipiscing_elit__sed_do_eiusmod_tempor_incididunt_ut_labore_et_dolore_magna_aliqua:1337/this/is/the/path/?QueryString";

  //      netTestBeginExpectAssert();
  //      result = netUri::Parse(uri, infos);
  //      netTestEndExpectAssert(2); // 2 because rverify throws two diagError

  //      netTestAssert(!result);
  //      netTestAssert(strcmp(infos.m_Host, "") == 0);
  //      netTestAssertEqual(infos.m_Port, (short)0);
  //      netTestAssert(infos.m_Scheme == URISCHEME_INVALID);
  //      netTestAssert(strcmp(infos.m_Path, "") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);

  //      infos.Reset();
  //      uri = "https://hostname/Really/really/really/loooooooong/path/Lorem/ipsum/dolor/sit/amet//consectetur/adipiscing/elit//sed/do/eiusmod/tempor/incididunt/ut/labore/et/dolore/magna/aliqua./Ut/enim/ad/minim/veniam//quis/nostrud/exercitation/ullamco/laboris/nisi/ut/aliquip/ex/ea/commodo/consequat./Duis/aute/irure/dolor/in/reprehenderit/in/voluptate/velit/esse/cillum/dolore/eu/fugiat/nulla/pariatur./Excepteur/sint/occaecat/cupidatat/non/proident//sunt/in/culpa/qui/officia/deserunt/mollit/anim/id/est/laborum./Lorem/ipsum/dolor/sit/amet//consectetur/adipiscing/elit//sed/do/eiusmod/tempor/incididunt/ut/labore/et/dolore/magna/aliqua";

  //      netTestBeginExpectAssert();
  //      result = netUri::Parse(uri, infos);
  //      netTestEndExpectAssert(2); // 2 because rverify throws two diagError

  //      netTestAssert(!result);
  //      netTestAssert(strcmp(infos.m_Host, "") == 0);
  //      netTestAssertEqual(infos.m_Port, (short)0);
  //      netTestAssert(infos.m_Scheme == URISCHEME_INVALID);
  //      netTestAssert(strcmp(infos.m_Path, "") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);


  //      // Null Uri
  //      infos.Reset();
  //      uri = nullptr;

  //      netTestBeginExpectAssert();
  //      result = netUri::Parse(uri, infos);
  //      netTestEndExpectAssert(2); // 2 because rverify throws two diagError

  //      netTestAssert(!result);
  //      netTestAssert(strcmp(infos.m_Host, "") == 0);
  //      netTestAssertEqual(infos.m_Port, (short)0);
  //      netTestAssert(infos.m_Scheme == URISCHEME_INVALID);
  //      netTestAssert(strcmp(infos.m_Path, "") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);

  //      // Malformed Uris
  //      infos.Reset();
  //      uri = "https://hostn/ame:1337/Hey_theres_a_slash_in_the_hostname";

  //      netTestBeginExpectAssert();
  //      result = netUri::Parse(uri, infos);
  //      netTestEndExpectAssert(2); // 2 because rverify throws two diagError

  //      netTestAssert(!result);
  //      netTestAssert(strcmp(infos.m_Host, "") == 0);
  //      netTestAssertEqual(infos.m_Port, (short)0);
  //      netTestAssert(infos.m_Scheme == URISCHEME_INVALID);
  //      netTestAssert(strcmp(infos.m_Path, "") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);

  //      
  //      infos.Reset();
  //      uri = "https://hostname:133700/Hey_the_port_is_invalid";

  //      netTestBeginExpectAssert();
  //      result = netUri::Parse(uri, infos);
  //      netTestEndExpectAssert(2); // 2 because rverify throws two diagError

  //      netTestAssert(!result);
  //      netTestAssert(strcmp(infos.m_Host, "") == 0);
  //      netTestAssertEqual(infos.m_Port, (short)0);
  //      netTestAssert(infos.m_Scheme == URISCHEME_INVALID);
  //      netTestAssert(strcmp(infos.m_Path, "") == 0);
  //      netTestAssert(strcmp(infos.m_QueryString, "") == 0);

	}
	NET_TEST_METHOD_END
}
NET_TEST_CLASS_END(Test_netUri)
