
#include "netUnitTestFramework.h"

#include "rline/rlpcpipe.h"
#include "system/simpleallocator.h"

using namespace rage;

#if UNITTEST_ENABLE_RLINE_PC_PIPE
#if RSG_PC

namespace rage
{
	extern sysMemAllocator* g_rlAllocator;
}

NET_TEST_CLASS(Test_rlPcPipe)
{

	NET_TEST_INIT(Init_rlPcPipe)
	{
		// Put your initialization code here
	}

	// Uri parsing, extracting scheme, host port and path from a uri
	NET_TEST_METHOD(TestMessages)
	{
		// Download Progress
		{
			//char* msg = "PROG:MP3:55";
			//rlPcPipeMessageBase b(msg);
			/*netTestAssert(b.IsA<rlPcPipeDownloadProgressMessage>());

			rlPcPipeDownloadProgressMessage m;
			netTestAssert(m.Import(msg, istrlen(msg)));

			char buf[255];
			netTestAssert(m.Export(buf, COUNTOF(buf)));
			netTestAssert(!stricmp(msg, buf));*/
		}

		// Download Progress - Buffer too small
		{
			/*char* msg = "PROG:MP3:55";
			rlPcPipeMessageBase b(msg);
			netTestAssert(b.IsA<rlPcPipeDownloadProgressMessage>());

			rlPcPipeDownloadProgressMessage m;
			netTestAssert(m.Import(msg, istrlen(msg)));

			char buf[32];
			netTestBeginExpectAssert();
			netTestAssert(!m.Export(buf, COUNTOF(buf)));
			netTestEndExpectAssert(1);*/
		}

		// Download Complete
		{
			/*char* msg = "COMP:MP3";
			rlPcPipeMessageBase b(msg);
			netTestAssert(b.IsA<rlPcPipeDownloadCompletionMessage>());

			rlPcPipeDownloadCompletionMessage m;
			netTestAssert(m.Import(msg, istrlen(msg)));

			char buf[255];
			netTestAssert(m.Export(buf, COUNTOF(buf)));
			netTestAssert(!stricmp(msg, buf));*/
		}

		// Download Error
		{
			/*char* msg = "ERRO:MP3:There was an error with your download.";
			rlPcPipeMessageBase b(msg);
			netTestAssert(b.IsA<rlPcPipeDownloadErrorMessage>());

			rlPcPipeDownloadErrorMessage m;
			netTestAssert(m.Import(msg, istrlen(msg)));

			char buf[512];
			netTestAssert(m.Export(buf, COUNTOF(buf)));
			netTestAssert(!stricmp(msg, buf));*/
		}

		// Download Pause
		{
			//char* msg = "PAUSE";
			//rlPcPipeMessageBase b(msg);
			//netTestAssert(b.IsA<rlPcPipeDownloadPauseMessage>());

			//rlPcPipeDownloadPauseMessage m;

			//// Pause does not import
			//netTestBeginExpectAssert();
			//netTestAssert(!m.Import(msg, istrlen(msg)));
			//netTestEndExpectAssert(1);

			//char buf[255];
			//netTestAssert(m.Export(buf, COUNTOF(buf)));
			//netTestAssert(!stricmp(msg, buf));
		}

		// Download Resume
		{
			//char* msg = "RESUME";
			//rlPcPipeMessageBase b(msg);
			//netTestAssert(b.IsA<rlPcPipeDownloadResumeMessage>());

			//rlPcPipeDownloadResumeMessage m;

			//// Resume does not import
			//netTestBeginExpectAssert();
			//netTestAssert(!m.Import(msg, istrlen(msg)));
			//netTestEndExpectAssert(1);

			//char buf[255];
			//netTestAssert(m.Export(buf, COUNTOF(buf)));
			//netTestAssert(!stricmp(msg, buf));
		}

		// Ping
		{
			//char* msg = "PING";
			//rlPcPipeMessageBase b(msg);
			//netTestAssert(b.IsA<rlPcPipePingMessage>());

			//rlPcPipePingMessage m;

			//// Ping does not import
			//netTestBeginExpectAssert();
			//netTestAssert(!m.Import(msg, istrlen(msg)));
			//netTestEndExpectAssert(1);

			//char buf[255];
			//netTestAssert(m.Export(buf, COUNTOF(buf)));
			//netTestAssert(!stricmp(msg, buf));
		}

		// Standard User
		{
			//char* msg = "UACE";
			//rlPcPipeMessageBase b(msg);
			//netTestAssert(b.IsA<rlPcPipeStandardUserMessage>());

			//rlPcPipeStandardUserMessage m;

			//// Standard User does not import
			//netTestBeginExpectAssert();
			//netTestAssert(!m.Import(msg, istrlen(msg)));
			//netTestEndExpectAssert(1);

			//char buf[255];
			//netTestAssert(m.Export(buf, COUNTOF(buf)));
			//netTestAssert(!stricmp(msg, buf));
		}

		// Restart
		{
			//char* msg = "RESTART";
			//rlPcPipeMessageBase b(msg);
			//netTestAssert(b.IsA<rlPcPipeRestartGameMessage>());

			//rlPcPipeRestartGameMessage m;

			//// Restart Game does not import
			//netTestBeginExpectAssert();
			//netTestAssert(!m.Import(msg, istrlen(msg)));
			//netTestEndExpectAssert(1);

			//char buf[255];
			//netTestAssert(m.Export(buf, COUNTOF(buf)));
			//netTestAssert(!stricmp(msg, buf));
		}

		// Download with token
		{
			//char* msg = "DOWN:BOB:ABCDEF";
			//rlPcPipeMessageBase b(msg);
			//netTestAssert(b.IsA<rlPcPipeDownloadWithTokenMessage>());

			//rlPcPipeDownloadWithTokenMessage m;

			//// rlPcPipeDownloadWithTokenMessage does not import
			// netTestBeginExpectAssert();
			//netTestAssert(!m.Import(msg, istrlen(msg)));
			//netTestEndExpectAssert(1);

			//m.Init("BOB", "ABCDEF");

			//char buf[512];
			//netTestAssert(m.Export(buf, COUNTOF(buf)));
			//netTestAssert(!stricmp(msg, buf));
		}

		// Social Club Sign Out
		{
			//char* msg = "SCSO";
			//rlPcPipeMessageBase b(msg);
			//netTestAssert(b.IsA<rlPcPipeSignOutMessage>());

			//rlPcPipeSignOutMessage m;

			//// Social Club Sign Out does not import
			//netTestBeginExpectAssert();
			//netTestAssert(!m.Import(msg, istrlen(msg)));
			//netTestEndExpectAssert(1);

			//char buf[255];
			//netTestAssert(m.Export(buf, COUNTOF(buf)));
			//netTestAssert(!stricmp(msg, buf));
		}
		
		// Social Club Sign In
		{
			/*char* msg = "SCSI:123456890";
			rlPcPipeMessageBase b(msg);
			netTestAssert(b.IsA<rlPcPipeSignInMessage>());

			rlPcPipeSignInMessage m;
			netTestAssert(m.Import(msg, istrlen(msg)));

			char buf[255];
			netTestAssert(m.Export(buf, COUNTOF(buf)));
			netTestAssert(!stricmp(msg, buf));*/
		}

		// Social Club Ticket Changed
		/*sysMemAllocator* prevAllocator = g_rlAllocator;
		int heapSize = 1024 * 1024;
		u8* heap = rage_new u8[heapSize];
		sysMemSimpleAllocator* allocator = rage_new sysMemSimpleAllocator(heap, heapSize, sysMemSimpleAllocator::HEAP_NET);
		g_rlAllocator = allocator;
		{
			char* msg = "SCTC:123456890:AABBCCDDEEFF11223344556677889900";
			rlPcPipeMessageBase b(msg);
			netTestAssert(b.IsA<rlPcPipeTicketChangedMessage>());
			
			rlPcPipeTicketChangedMessage m;
			netTestAssert(m.Import(msg, istrlen(msg)));

			char buf[255];
			netTestAssert(m.Export(buf, COUNTOF(buf)));
			netTestAssert(!stricmp(msg, buf));
		}
		delete[] heap;
		delete allocator;
		g_rlAllocator = prevAllocator;*/
	}
	NET_TEST_METHOD_END

}
NET_TEST_CLASS_END(Test_rlPcPipe)

#endif // RSG_PC
#endif // UNITTEST_ENABLE_RLINE_PC_PIPE