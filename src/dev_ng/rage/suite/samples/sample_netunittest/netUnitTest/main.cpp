
#include "system/main.h"

namespace rage
{
#if RSG_ORBIS

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-writable-strings"
#if SCE_ORBIS_SDK_VERSION >= 0x02500000
#pragma GCC diagnostic ignored "-Wwritable-strings"
#endif
const char *XEX_TITLE_ID = "CUSA00411";	// default to european, gets overridden later
#pragma GCC diagnostic pop

#endif // RSG_ORBIS
} // rage

using namespace rage;

#include "input/input.h"
#include "paging/streamer.h"

#include "grmodel/setup.h"
#include "grcore/device.h"
#include "grcore/font.h"
#include "grcore/im.h"
#include "file/asset.h"
#if __D3D11
#include "shader_rage_im_win32_40.h"
#else
#include "shader_rage_im_win32_30.h"
#endif

//#include "netUnitTest_fnt.h"
//#include "netUnitTest_fnt_dds.h"
#include "netUnitTestFramework.h"

#if RSG_ORBIS
#include <libime.h>
#include <libsysmodule.h>
#pragma comment (lib, "libSceIme_stub_weak.a")
#endif

PARAM(autoquit, "PC only : Closes the application after the test are run, returning EXIT_SUCCESS or EXIT failure depending on the tests results");
PARAM(autoquitOnSuccess, "PC only : Closes the application after the test are run is successful, otherwise leave it open so you can see the results");

// hack - so we can link..
namespace rage
{
	u32 GetStrIndexFromHandle(const unsigned & /*handle*/)
	{
		return 0;
	}
}

//sga::Font* sm_Font;
//float s_curY;

//void Draw2dText(float x,float y,const char *s, Color32 color, float scale)
//{
	//rage::grcDraw2dText(x, y, s, false, scale, scale); 
	//sm_Font->DrawString(*sga::g_GraphicsContext,x,y,0,scale,color.GetColor(), s);
//}

//void Draw()
//{  
	//sga::Setup::BeginDrawBasic(*sga::g_GraphicsContext,nullptr);
	//float color[4] = { 0, 0, 0, 1.0f };
	//sga::g_GraphicsContext->ClearColorOnly(color);
	//sga::g_GraphicsContext->SetBlendState(sga::BS_Normal);
	//sga::g_GraphicsContext->ResetViewport();
	// This won't be necessary after lockdown is lifted and I can submit my fixed version of sga_font.cpp:
	//sga::g_GraphicsContext->SetRasterizerState(sga::RS_NoBackfaceCull);

	//Draw2dText(TEST_MARGIN_LEFT, TEST_MARGIN_TOP + s_curY, "===== NET UNIT TESTS ===== ", COLOR_WHITE, TITLE_FONT_SIZE);

//#if RSG_PC || RSG_ORBIS
	// Quick and dirty scrolling.
	//float maxY = netUnitTest::draw(sm_Font, s_curY);
	/*rage::INPUT.Update(false, false, false);
	if (rage::ioKeyboard::KeyDown(rage::KEY_DOWN)) s_curY -= 8;
	else if (rage::ioKeyboard::KeyDown(rage::KEY_UP)) s_curY += 8;
	maxY = -maxY + 300; 
	if (s_curY <= maxY)
		s_curY = maxY;
	else if (s_curY >= 0)
		s_curY = 0;*/
//#else
//	netUnitTest::draw(sm_Font, s_curY);
//#endif

	//sga::Setup::EndDrawBasic(*sga::g_GraphicsContext);
//}

//void KeyboardUpdateThread(void* /*param*/)
//{
//#if RSG_PC
//	for(;;)
//	{
//		ioKeyboard::RecaptureLostDevices();
//		sysIpcSleep(30);
//	}
//#endif
//}


int Main()
{
	grmSetup setup;

	const char* defaultEffectName = GRCDEVICE.GetDefaultEffectName();
	char shaderDir[RAGE_MAX_PATH] = {0};
	ASSET.RemoveNameFromPath(shaderDir, sizeof(shaderDir),defaultEffectName);
#if !RSG_PC
	if(ASSET.Exists(shaderDir, "") == false)
#endif
	{
		// sample_session is distributed to everyone testing the Social Club UI. These people may not have the rage/assets directory containing the rage_im.fxc shader.
		// fallback to 'embedded' shader
#if __D3D11
		char memFileName[RAGE_MAX_PATH];
		fiDevice::MakeMemoryFileName(memFileName, RAGE_MAX_PATH, embedded_rage_im_win32_40_data, sizeof(embedded_rage_im_win32_40_data), false, "\\rage_im");
		GRCDEVICE.SetDefaultEffectName(memFileName);
#else
		char memFileName[RAGE_MAX_PATH];
		fiDevice::MakeMemoryFileName(memFileName, RAGE_MAX_PATH, embedded_rage_im_win32_30_data, sizeof(embedded_rage_im_win32_30_data), false, "\\rage_im");
		GRCDEVICE.SetDefaultEffectName(memFileName);
#endif
	}

	char titleBuf[32];
	formatf(titleBuf, "RageNet Unit Test");
	setup.Init(RAGE_ASSET_ROOT, titleBuf);
	setup.BeginGfx(true /*!PARAM_fullscreen.Get()*/);
	setup.CreateDefaultFactories();

	//rage::INPUT.Begin(true, false);

	//// currently using InputMono with faux bold
	////u32 swizzle;
	//char fntFile[256], ddsFile[256];
	//fiDevice::MakeMemoryFileName(fntFile, (int)sizeof(fntFile), netUnitTest_fnt_data, sizeof(netUnitTest_fnt_data), false, "sample session font file");
	//fiDevice::MakeMemoryFileName(ddsFile, (int)sizeof(ddsFile), netUnitTest_fnt_dds_data, sizeof(netUnitTest_fnt_dds_data), false, "sample session font dds file");
	//sga::Image* image = sga::Image::LoadFromDDS(ddsFile, swizzle);
	//sm_Font = sga::Font::CreateFromFontFile(fntFile, image);
	//image->Release();

	netUnitTest::prepareTests();


	while(netUnitTest::runNext())
	{
		// Temp fix, not sure why rendering doesnt work as expected on pc if I render every frame :(
		// So we'll just render when we have the final results
//#if !RSG_PC
		//Draw();
//#endif // !RSG_PC
	}

#if RSG_PC

	if(PARAM_autoquit.Get())
	{
		// use _exit instead of exit so no cleanup is performed (causing a crash in in graphics shutdown)
		if(netUnitTest::testsAllOk()) 
		{
			_exit(EXIT_SUCCESS);
		}
		else
		{
			_exit(EXIT_FAILURE); 
		}
	}

	if(PARAM_autoquitOnSuccess.Get() && netUnitTest::testsAllOk())
	{
		_exit(EXIT_SUCCESS);
	}

#endif // RSG_PC

//#if RSG_PC
	//sysIpcCreateThread(KeyboardUpdateThread, NULL, 32*1024, PRIO_HIGHEST, "[RAGE] KeyboardUpdateThread", 2, "KeyboardUpdateThread");
//#endif

	// Loop forever on the results
	while(!setup.WantExit())
	{
		setup.BeginUpdate();
		setup.EndUpdate();

		setup.BeginDraw();
		setup.EndDraw();

		//rage::sysIpcSleep(16);
	}

	setup.DestroyFactories();
	setup.EndGfx();
	setup.Shutdown();

	return 0; 
}
