

#include "netUnitTestFramework.h"

#if UNITTEST_ENABLE_NET_TASK2

//#include "net/task2.h"
#include "net/status.h"
#include "system/timer.h"

using namespace rage;

//sysCriticalSectionToken s_TestCs;

NET_TEST_CLASS(Test_netTask2)
{
	NET_TEST_INIT(Init_netTask2)
	{
		//netTask2::Init();
	}

	NET_TEST_METHOD(TestChild)
	{
		//int state = 0;

		////Start a task that will have a child task.
		////The parent task will not continue until the child task completes.

		//auto parentTask = netTask2::ScheduleTask([&state]()
		//{
		//	AUTO_CANCEL();

		//	int childState;

		//	switch(state)
		//	{
		//	case 0:
		//		state = 1;
		//		childState = 0;

		//		//Create a child task.
		//		//We'll go to sleep until the child task completes.

		//		//Declaring the lambda "mutable" allows us to modify captured state.
		//		//The captured variable "childState" becomes similar to a member
		//		//variable for the lambda.  It maintains state between lambda invocations.
		//		netTask2::ScheduleChildTask([childState]() mutable
		//		{
		//			if(netTask2::IsCancelRequested())
		//			{
		//				return netTaskState::Canceled;
		//			}

		//			while(childState < 5)
		//			{
		//				++childState;
		//				return netTaskState::Pending;
		//			}

		//			return netTaskState::Succeeded;
		//		}, NULL, TASK2_DECL(TestChild, ragenet_task2));

		//		return netTaskState::Pending;
		//		break;
		//	case 1:
		//		//If we reach this state then the child task completed.
		//		++state;
		//		return netTaskState::Pending;
		//		break;
		//	case 2:
		//		++state;
		//		return netTaskState::Pending;
		//		break;
		//	case 3:
		//		++state;
		//		return netTaskState::Succeeded;
		//		break;
		//	}

		//	return netTaskState::Succeeded;
		//});

		//while(!parentTask.IsComplete())
		//{
		//	netTask2::Update();
		//}

		//netTestAssertEqual(4, state);
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestContinuation)
	{
		//int x = 0, y = 0, z = 0;

		////Create task A.

		//auto A = netTask2::ScheduleTask([&x]()
		//{
		//	AUTO_CANCEL();

		//	while(x < 10)
		//	{
		//		++x;
		//		return netTaskState::Pending;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestContinuationA, ragenet_task2));

		////Add a continuation to task A, then add a continuation to that.
		////C will refer to the third task in the chain.
		////The continuation between A and C is unnamed.
		////A -> ___ -> C.
		////When C completes then both A and the unnamed tasks will have completed.

		//auto C =

		////Unnamed continuation
		//A.Then([&y]()
		//{
		//	AUTO_CANCEL();

		//	while(y < 15)
		//	{
		//		++y;
		//		return netTaskState::Pending;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestContinuationB, ragenet_task2))
		////Continuation that's assigned to C.
		//.Then([&z]()
		//{
		//	AUTO_CANCEL();

		//	while(z < 20)
		//	{
		//		++z;
		//		return netTaskState::Pending;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestContinuationC, ragenet_task2));

		//while(!C.IsComplete())
		//{
		//	netTask2::Update();
		//}

		//netTestAssert(A.IsComplete());
		//netTestAssertEqual(10, x);
		//netTestAssertEqual(15, y);
		//netTestAssertEqual(20, z);

	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestGroup)
	{
		//int x = 0, y = 0, z = 0;

		////Create a dummy task as the grouper.
		////The dummy task won't run until all child tasks
		////complete, at which time the dummy task will complete.
		//auto G = netTask2::CreateTask([]()
		//{
		//	return netTaskState::Succeeded;
		//});

		////Create child tasks A, B, C.

		//auto A = netTask2::CreateTask([&x]()
		//{
		//	AUTO_CANCEL();

		//	while(x < 10)
		//	{
		//		++x;
		//		return netTaskState::Pending;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestGroupA, ragenet_task2));

		//auto B = netTask2::CreateTask([&y]()
		//{
		//	AUTO_CANCEL();

		//	while(y < 15)
		//	{
		//		++y;
		//		return netTaskState::Pending;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestGroupB, ragenet_task2));

		//auto C = netTask2::CreateTask([&z]()
		//{
		//	AUTO_CANCEL();

		//	while(z < 20)
		//	{
		//		++z;
		//		return netTaskState::Pending;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestGroupC, ragenet_task2));

		////Add the child tasks to the group (parent) task.
		//G.AddChildTask(A);
		//G.AddChildTask(B);
		//G.AddChildTask(C);

		////Schedule the group task to run.
		//netTask2::ScheduleTask(G);

		////Run until G completes, implying A, B, and C have also competed.
		//while(!G.IsComplete())
		//{
		//	netTask2::Update();
		//}

		//netTestAssert(A.IsComplete());
		//netTestAssert(B.IsComplete());
		//netTestAssert(C.IsComplete());
		//netTestAssertEqual(10, x);
		//netTestAssertEqual(15, y);
		//netTestAssertEqual(20, z);
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestWait)
	{
		//int state = 0;

		////Start a task that will wait for another task.
		////The waiter task will not continue until the awaited task completes.

		//auto waiterTask = netTask2::ScheduleTask([&state]()
		//{
		//	AUTO_CANCEL();

		//	int awaitedState;

		//	switch(state)
		//	{
		//	case 0:
		//		state = 1;
		//		awaitedState = 0;

		//		//Create a task that will be awaited.
		//		//We'll go to sleep until the awaited task completes.
		//		{
		//			auto awaitedTask = netTask2::ScheduleTask([awaitedState]() mutable
		//			{
		//				AUTO_CANCEL();

		//				while(awaitedState < 5)
		//				{
		//					++awaitedState;
		//					return netTaskState::Pending;
		//				}

		//				return netTaskState::Succeeded;
  //              }, NULL, TASK2_DECL(TestWaitA, ragenet_task2));

		//			netTask2::Wait(awaitedTask);
		//		}

		//		return netTaskState::Pending;
		//		break;
		//	default:
		//		++state;
		//		break;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestWaitB, ragenet_task2));

		//while(!waiterTask.IsComplete())
		//{
		//	netTask2::Update();
		//}

		//netTestAssertEqual(2, state);
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestWhenAll)
	{
  //      const int numTasks = 10;
		//netTask2 tasks[numTasks];
		//int states[numTasks] = {0};

		//for(int i = 0; i < numTasks; ++i)
		//{
		//	states[i] = 0;

		//	tasks[i] = netTask2::ScheduleTask([&states, i]()
		//	{
		//		SYS_CS_SYNC(s_TestCs);

		//		AUTO_CANCEL();

		//		if(++states[i] == (i+1)*10)
		//		{
		//			return netTaskState::Succeeded;
		//		}

		//		return netTaskState::Pending;
		//	}, NULL, TASK2_DECLT(TestWhenAllA, ragenet_task2));
		//}

		//bool waiterRan = false;

		//auto waiterFunc = [&waiterRan]()
		//{
		//	AUTO_CANCEL();

		//	waiterRan = true;

		//	return netTaskState::Succeeded;
		//};

		//netTask2 waiterTask = netTask2::WhenAll(tasks, numTasks, waiterFunc);

		////Test canceling the waiter.  The waitables should not have been canceled.
		//waiterTask.Cancel();

		////The waiter won't realize it's been canceled until it has a chance to run,
		////which is only after all waitables have completed.
		//while(!waiterTask.IsComplete())
		//{
		//	netTask2::Update();
		//}

		////The waiter was canceled before it could complete.
		//netAssert(!waiterRan);

		//// Run the waiters to completion
		//bool allComplete = false;
		//while(!allComplete)
		//{
		//	netTask2::Update();

		//	allComplete = true;
		//	for (int i = 0; i < numTasks; i++)
		//	{
		//		if (!tasks[i].IsComplete())
		//			allComplete = false;
		//	}
		//}

		////The waitables should have run to completion.
		//for(int i = 0; i < numTasks; ++i)
		//{
		//	netTestAssert(tasks[i].IsComplete());
		//	netTestAssertEqual(states[i], (i+1)*10);
		//}

		////Recreate the waitable tasks.
		//for(int i = 0; i < numTasks; ++i)
		//{
		//	states[i] = 0;

		//	tasks[i] = netTask2::ScheduleTask([&states, i]()
		//	{
		//		AUTO_CANCEL();

		//		if(++states[i] == (i+1)*10)
		//		{
		//			return netTaskState::Succeeded;
		//		}

		//		return netTaskState::Pending;
		//	}, NULL, TASK2_DECL(TestWhenAllB, ragenet_task2));
		//}

		////Recreate the waiter and let it run to completion
		//waiterTask = netTask2::WhenAll(tasks, numTasks, waiterFunc);

		//while(!waiterTask.IsComplete())
		//{
		//	netTask2::Update();
		//}

		////The waiter should have run to completion.
		//netAssert(waiterRan);

		////The waitables should have all run to completion.
		//for(int i = 0; i < numTasks; ++i)
		//{
		//	netTestAssert(tasks[i].IsComplete());
		//	netTestAssert(states[i] == (i+1)*10);
		//}
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestWhenAny)
	{
		//const int numTasks = 10;
		//netTask2 tasks[numTasks];
		//int states[numTasks] = {0};
		//for(int i = 0; i < numTasks; ++i)
		//{
		//	tasks[i] = netTask2::ScheduleTask([&states, i]()
		//	{
		//		AUTO_CANCEL();

		//		if(++states[i] == (i+1)*10)
		//		{
		//			return netTaskState::Succeeded;
		//		}

		//		return netTaskState::Pending;
		//	}, NULL, TASK2_DECL(TestWhenAnyA, ragenet_task2));
		//}

		//bool waiterRan = false;

		//auto waiterFunc = [&waiterRan]()
		//{
		//	AUTO_CANCEL();

		//	waiterRan = true;

		//	return netTaskState::Succeeded;
		//};

		//netTask2 waiterTask = netTask2::WhenAny(tasks, numTasks, waiterFunc);

		////Test canceling the waiter.  The waitables should not have been canceled.
		//waiterTask.Cancel();

		////The waiter won't realize it's been canceled until it has a chance to run,
		////which is only after one of the waitables has completed.
		//while(!waiterTask.IsComplete())
		//{
		//	netTask2::Update();
		//}

		////The waiter was canceled before it could complete.
		//netTestAssert(!waiterRan);

		////Task 0 completed, the rest didn't
		//netTestAssert(tasks[0].IsComplete());
		//netTestAssert(states[0] == 10);

		//for(int i = 1; i < numTasks; ++i)
		//{
		//	netAssert(!tasks[i].IsComplete());
		//	int expected = 10;
		//	//One extra iteration was run before the waiter realized it was canceled.
		//	expected += 1;
		//	netAssert(states[i] == expected);
		//}

		////Let the waitable tasks run to completion.
		//bool complete = false;
		//while(!complete)
		//{
		//	netTask2::Update();
		//	complete = true;
		//	for(int i = 0; i < numTasks && complete; ++i)
		//	{
		//		complete = complete && tasks[i].IsComplete();
		//	}
		//}

		//for(int i = 0; i < numTasks; ++i)
		//{
		//	netTestAssert(tasks[i].IsComplete());
		//	int expected = (i+1)*10;
		//	netTestAssert(states[i] == expected);
		//}

		////Recreate the waitable tasks.
		//for(int i = 0; i < numTasks; ++i)
		//{
		//	states[i] = 0;

		//	tasks[i] = netTask2::ScheduleTask([&states, i]()
		//	{
		//		AUTO_CANCEL();

		//		if(++states[i] == (i+1)*10)
		//		{
		//			return netTaskState::Succeeded;
		//		}

		//		return netTaskState::Pending;
		//	}, NULL, TASK2_DECL(TestWhenAnyB, ragenet_task2));
		//}

		////Recreate the waiter and let it run to completion
		//waiterTask = netTask2::WhenAny(tasks, numTasks, waiterFunc);

		//while(!waiterTask.IsComplete())
		//{
		//	netTask2::Update();
		//}

		////The waiter should have run to completion.
		//netTestAssert(waiterRan);

		////Task 0 completed, the rest didn't
		//netTestAssert(tasks[0].IsComplete());
		//netTestAssert(states[0] == 10);

		//for(int i = 1; i < numTasks; ++i)
		//{
		//	netAssert(!tasks[i].IsComplete());
		//	int expected = 10;
		//	//One extra iteration was run before the waiter completed.
		//	expected += 1;
		//	netTestAssert(states[i] == expected);
		//}

		////Let the waitable tasks run to completion.
		//complete = false;
		//while(!complete)
		//{
		//	netTask2::Update();
		//	complete = true;
		//	for(int i = 0; i < numTasks && complete; ++i)
		//	{
		//		complete = complete && tasks[i].IsComplete();
		//	}
		//}

		//for(int i = 0; i < numTasks; ++i)
		//{
		//	netTestAssert(tasks[i].IsComplete());
		//	int expected = (i+1)*10;
		//	netTestAssert(states[i] == expected);
		//}
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestCancelWithChild)
	{
		//Start a task with a child task.
		//Cancel the parent task.  The child task
		//should also be canceled.

		//int state = 0;
  //  int x = 0, y = 0;
  //  bool parentCanceled = false, childCanceled = false;

		//auto parentTask = netTask2::ScheduleTask([&state, &x, &y, &parentCanceled, &childCanceled]()
		//{
		//	if(netTask2::IsCancelRequested())
		//	{
		//		parentCanceled = true;
		//		return netTaskState::Canceled;
		//	}

		//	switch(state)
		//	{
		//	case 0:
		//		state = 1;

		//		netTask2::ScheduleChildTask([&y, &childCanceled]()
		//		{
		//			if(netTask2::IsCancelRequested())
		//			{
		//				childCanceled = true;
		//				return netTaskState::Canceled;
		//			}

		//			while(y < 10)
		//			{
		//				++y;
		//				return netTaskState::Pending;
		//			}

		//			return netTaskState::Succeeded;
		//		}, NULL, TASK2_DECL(TestCancelChild, ragenet_task2));

		//		return netTaskState::Pending;
		//		break;
		//	case 1:
		//		//If we reach this state then the child task completed.
		//		while(x < 10)
		//		{
		//			++x;
		//			return netTaskState::Pending;
		//		}
		//		++state;
		//		break;
		//	case 2:
		//		break;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestCancelParent, ragenet_task2));

		////After 5 iterations cancel the parent.
		////The child should also be canceled as a result.
		//while(!parentTask.IsComplete())
		//{
		//	for(int i = 0; i < 5; ++i)
		//	{
		//		netTask2::Update();
		//	}

		//	parentTask.Cancel();
		//}

		//netTestAssert(parentCanceled);
		//netTestAssert(childCanceled);
		//netTestAssertEqual(1, state);
		//netTestAssertEqual(0, x);
		//netTestAssertEqual(4, y);  //Child was canceled before y reached 10
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestCancelWithContinuation)
	{
  //  int x = 0, y = 0, z = 0;

		////Start a task and add continuations.
		////Cancel the antecedent task.
		////The continuations should also have been canceled.

  //  bool canceledA = false, canceledB = false, canceledC = false;

		//auto A = netTask2::ScheduleTask([&x, &canceledA]()
		//{
		//	if(netTask2::IsCancelRequested())
		//	{
		//		canceledA = true;
		//		return netTaskState::Canceled;
		//	}

		//	while(x < 10)
		//	{
		//		++x;
		//		return netTaskState::Pending;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestCancelA, ragenet_task2));

		//auto B = A.Then([&y, &canceledB]()
		//{
		//	if(netTask2::IsCancelRequested())
		//	{
		//		canceledB = true;
		//		return netTaskState::Canceled;
		//	}

		//	while(y < 15)
		//	{
		//		++y;
		//		return netTaskState::Pending;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestCancelB, ragenet_task2));

		//auto C = B.Then([&z, &canceledC]()
		//{
		//	if(netTask2::IsCancelRequested())
		//	{
		//		canceledC = true;
		//		return netTaskState::Canceled;
		//	}

		//	while(z < 20)
		//	{
		//		++z;
		//		return netTaskState::Pending;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestCancelC, ragenet_task2));

		////After 5 iterations cancel A.
		////B and C should never run, except to recognize they've been canceled
		//while(!A.IsComplete())
		//{
		//	for(int i = 0; i < 5; ++i)
		//	{
		//		netTask2::Update();
		//	}

		//	A.Cancel();
		//}

		//netTestAssert(A.IsComplete());
		//netTestAssert(B.IsComplete());
		//netTestAssert(C.IsComplete());
		//netTestAssert(canceledA);
		//netTestAssert(canceledB);
		//netTestAssert(canceledC);
		//netTestAssertEqual(5, x);
		//netTestAssertEqual(0, y);
		//netTestAssertEqual(0, z);
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestCancelWithGroup)
	{
  //  int x = 0, y = 0, z = 0;
  //  bool canceledA = false, canceledB = false, canceledC = false;

		////Create a group of tasks and cancel the group.
		////All tasks in the group should also be canceled.

		//auto G = netTask2::CreateTask([]()
		//{
		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestCancelGroup, ragenet_task2));

		//auto A = netTask2::CreateTask([&x, &canceledA]()
		//{
		//	if(netTask2::IsCancelRequested())
		//	{
		//		canceledA = true;
		//		return netTaskState::Canceled;
		//	}

		//	while(x < 10)
		//	{
		//		++x;
		//		return netTaskState::Pending;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestCancelGroupA, ragenet_task2));

		//auto B = netTask2::CreateTask([&y, &canceledB]()
		//{
		//	if(netTask2::IsCancelRequested())
		//	{
		//		canceledB = true;
		//		return netTaskState::Canceled;
		//	}

		//	while(y < 15)
		//	{
		//		++y;
		//		return netTaskState::Pending;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestCancelGroupB, ragenet_task2));

		//auto C = netTask2::CreateTask([&z, &canceledC]()
		//{
		//	if(netTask2::IsCancelRequested())
		//	{
		//		canceledC = true;
		//		return netTaskState::Canceled;
		//	}

		//	while(z < 20)
		//	{
		//		++z;
		//		return netTaskState::Pending;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(TestCancelGroupC, ragenet_task2));

		//G.AddChildTask(A);
		//G.AddChildTask(B);
		//G.AddChildTask(C);

		//netTask2::ScheduleTask(G);

		////After 5 iterations cancel G.
		////A, B, and C should have also run for 5 iterations.
		//while(!G.IsComplete())
		//{
		//	for(int i = 0; i < 5; ++i)
		//	{
		//		netTask2::Update();
		//	}

		//	G.Cancel();
		//}

		//netTestAssert(G.IsComplete());
		//netTestAssert(A.IsComplete());
		//netTestAssert(B.IsComplete());
		//netTestAssert(C.IsComplete());
		//netTestAssert(canceledA);
		//netTestAssert(canceledB);
		//netTestAssert(canceledC);
		//netTestAssertEqual(5, x);
		//netTestAssertEqual(5, y);
		//netTestAssertEqual(5, z);
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestThreadPool)
	{
		//const unsigned TASK_DURATION = 1000;
		//const unsigned CANCEL_TIME = TASK_DURATION / 2;

		//netStatus sa, sb, sc;

		//auto blockingTask = netTask2::ScheduleTask([&TASK_DURATION]()
		//{
		//	sysIpcSleep(TASK_DURATION);
		//	return netTaskState::Succeeded;
		//}, &sa, TASK2_DECLT(ThreadPoolTask1,ragenet_task2))
		//	.Then([&TASK_DURATION]()
		//{
		//	sysIpcSleep(TASK_DURATION);
		//	return netTaskState::Succeeded;
		//}, &sc, TASK2_DECL(ThreadPoolTask2,ragenet_task2));

		//auto blockingTaskCancel = netTask2::ScheduleTask([&TASK_DURATION]()
		//{
		//	AUTO_CANCEL();
		//	sysIpcSleep(TASK_DURATION);
		//	netTask2Debug("BlockingTask Sleep #1 completed.");

		//	AUTO_CANCEL();
		//	sysIpcSleep(TASK_DURATION);
		//	netTask2Debug("BlockingTask Sleep #2 completed.");

		//	return netTaskState::Succeeded;
		//}, &sb, TASK2_DECLT(ThreadPoolTask3,ragenet_task2));

		//u32 startTime = sysTimer::GetSystemMsTime();

		//// Wait for both tasks to complete
		//while (sb.Pending() || sa.Pending() || sc.Pending())
		//{
		//	netTask2::Update();

		//	// Cancel the second task half-way through.
		//	if (sysTimer::HasElapsedIntervalMs(startTime, CANCEL_TIME))
		//	{
		//		blockingTaskCancel.Cancel();
		//	}
		//}

		//// the first task should have succeeded, the second was canceled
		//netTestAssert(blockingTask.IsSucceeded());
		//netTestAssert(blockingTaskCancel.IsCanceled());
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestThreadPoolWait)
	{
		//int state = 0;

		////Start a task that will wait for another task.
		////The waiter task will not continue until the awaited task completes.

		//auto waiterTask = netTask2::ScheduleTask([&state]()
		//{
		//	AUTO_CANCEL();

		//	int awaitedState;

		//	switch(state)
		//	{
		//	case 0:
		//		state = 1;
		//		awaitedState = 0;

		//		//Create a task that will be awaited.
		//		//We'll go to sleep until the awaited task completes.
		//		{
		//			auto awaitedTask = netTask2::ScheduleTask([awaitedState]() mutable
		//			{
		//				AUTO_CANCEL();

		//				while(awaitedState < 3)
		//				{
		//					++awaitedState;
		//					sysIpcSleep(100);
		//					return netTaskState::Pending;
		//				}

		//				return netTaskState::Succeeded;
		//			}, NULL, TASK2_DECLT(ThreadPoolWaited, ragenet_task2));

		//			netTask2::Wait(awaitedTask);
		//		}

		//		return netTaskState::Pending;
		//		break;
		//	default:
		//		++state;
		//		break;
		//	}

		//	return netTaskState::Succeeded;
		//}, NULL, TASK2_DECL(ThreadPoolWaiter, ragenet_task2));

		//while(!waiterTask.IsComplete())
		//{
		//	netTask2::Update();
		//}

		//netTestAssertEqual(2, state);
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestNetStatus)
	{
		/*netStatus sa, sb, sc, sd;

		auto ta = netTask2::ScheduleTask([]()
		{
			return netTaskState::Succeeded;
		}, &sa)
		.Then([]()
		{
			return netTaskState::Succeeded;
		}, &sb);

		auto tb = netTask2::ScheduleTask([]()
		{
			return netTaskState::Failed;
		}, &sc);

		auto tc = netTask2::ScheduleTask([]()
		{
			return netTaskState::Canceled;
		}, &sd);

		while(sa.Pending() || sb.Pending() || sc.Pending() || sd.Pending())
		{
			netTask2::Update();
		}

		netTestAssert(sa.Succeeded());
		netTestAssert(sb.Succeeded());
		netTestAssert(sc.Failed());
		netTestAssert(sd.Canceled());*/
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestLogging)
	{
		/*netStatus status;

		auto a = netTask2::ScheduleTask([]()
		{
		netTask2Debug("Channeled Task Output");
		return netTaskState::Succeeded;
		}, &status, TASK2_DECL(TestLogging,ragenet_task2));

		while(status.Pending())
		{
		netTask2::Update();
		}

		netTestAssert(a.IsSucceeded() && status.Succeeded());*/
	}
	NET_TEST_METHOD_END


}
NET_TEST_CLASS_END(Test_netTask2)

#endif // #if UNITTEST_ENABLE_NET_TASK2