
#include "netUnitTestFramework.h"

#if UNITTEST_ENABLE_NET_CXNMGR

#include "diag/seh.h"
#include "net/connectionmanager.h"
#include "net/net.h"
#include "net/natdetector.h"
#include "net/netdiag.h"
#include "net/netfuncprofiler.h"
#include "net/nethardware.h"
#include "net/relay.h"

#include "rline/rl.h"
#include "rline/ros/rlroscommon.h"
#include "rline/rltitleid.h"
#include "rline/rlpc.h"
#include "rline/rlpresence.h"

#include "system/timer.h"
#include "system/nelem.h"
#include "system/simpleallocator.h"
#include "system/xtl.h"

using namespace rage;

#if RSG_PC && !__NO_OUTPUT
namespace rage {
	XPARAM(processinstance);
	XPARAM(scDll);
	XPARAM(scemail);
	XPARAM(scpassword);
}
#endif // __WIN32PC && !__FINAL

PARAM(netTestHostPeerAddr, "Peer address of the host, base64 encoded");
PARAM(netTestParentPid, "The process id of the parent process");

enum ChannelIds
{
	NETWORK_RESERVED_CHANNEL_ID			= 1,
	NAT_TRAVERSAL_CHANNEL_ID			= 10,
	NET_CONNECTION_CHANNEL_ID			= 13,
};

class Test_netCxnMgrApp
{
public:
	Test_netCxnMgrApp()
	: m_State(STATE_WAIT_FOR_ONLINE)
	, m_TestPassed(false)
	, m_NetAllocator(NULL)
	, m_Socket(NULL)
	, m_TitleId(NULL)
	, m_CxnMgr(NULL)
	, m_ParentPid(0)
	, m_IsHost(false)
	, m_EndTest(false)
	{

	}

	virtual ~Test_netCxnMgrApp()
	{

	}

	bool CreateSubprocess()
	{
		rtry
		{
			char subprocessPath[RAGE_MAX_PATH] = {0};	
			rverify(GetModuleFileName(NULL, subprocessPath, COUNTOF(subprocessPath)) != 0, catchall, )

			DWORD childProcessCreationFlags = 0;

			STARTUPINFO si;
			ZeroMemory(&si, sizeof(si));
			si.cb = sizeof(si);
			si.dwFlags = STARTF_USESHOWWINDOW;
			si.wShowWindow = SW_SHOW;

			ZeroMemory(&m_SubprocessInfo, sizeof(m_SubprocessInfo));

 			char commandLine[4096] = {0};

			char peerAddrStr[netPeerAddress::TO_STRING_BUFFER_SIZE] = {0};
			netPeerAddress peerAddr;
			rverify(netPeerAddress::GetLocalPeerAddress(&peerAddr), catchall, );
			rverify(peerAddr.ToString(peerAddrStr, sizeof(peerAddrStr)) != NULL, catchall, );
			formatf(commandLine, "\"%s\" -netUnitTestsByName=Test_netCxnMgr -processinstance=2 -netTestHostPeerAddr=%s -netTestParentPid=%u \
								 -net_log=debug3 -snet_log=debug3 -rline_log=debug3 -ragenet_log=debug3 \
								 -logfile=X:\\gta5\\src\\dev_ng\\rage\\suite\\samples\\sample_netUnitTest\\console2.log", 
								 subprocessPath, peerAddrStr, GetCurrentProcessId());
			Displayf("Creating subprocess: %s", commandLine);

			rverify(CreateProcess(subprocessPath,
									commandLine,
									NULL,
									NULL,
									FALSE,
									childProcessCreationFlags,
									NULL,
									NULL,
									&si,
									&m_SubprocessInfo) != 0, catchall, Displayf("CreateProcess failed."));

			Displayf("Subprocess created successfully.");

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void InitTitleId()
	{
		const char* ROS_TITLE_NAME = "GTA5";
		const char* TITLE_DIRECTORY_NAME = "GTA5QA";
		int ROS_SC_VERSION = 11;
		int ROS_TITLE_VERSION = 11;
		const char* ROS_TITLE_SECRETS = "C4pWJwWIKGUxcHd69eGl2AOwH2zrmzZAoQeHfQFcMelybd32QFw9s10px6k0o75XZeB5YsI9Q9TdeuRgdbvKsxc=";
		const rlRosEnvironment ROS_ENV = RLROS_ENV_DEV;

		rlRosTitleId rosTitleId(ROS_TITLE_NAME, TITLE_DIRECTORY_NAME, ROS_SC_VERSION, ROS_TITLE_VERSION, ROS_TITLE_SECRETS, ROS_ENV, NULL, 0);
		m_TitleId = new rlTitleId(rosTitleId);
	}

	void InitNetwork()
	{
		const unsigned heapSize = 1024 * 1024;
		u8* m_NetHeap = new u8[heapSize];
		m_NetAllocator = new sysMemSimpleAllocator(m_NetHeap, heapSize, 1);
		m_NetAllocator->BeginLayer();

		// don't load the Social Club DLL
		PARAM_scDll.Set("none");

		// these DEV Social Club accounts were created specifically for the unit test framework
		if(PARAM_netTestParentPid.Get())
		{
			// child process
			PARAM_scemail.Set("netUnitTestSc2@rockfoo.com");
		}
		else
		{
			// parent process
			PARAM_scemail.Set("netUnitTestSc1@rockfoo.com");
		}

		PARAM_scpassword.Set("Password1");

		netInit(m_NetAllocator, 0, 0);

		m_Socket = netRelay::GetSocket();
		Assert(m_Socket);

		InitTitleId();
		rlInit(m_NetAllocator, m_Socket, m_TitleId, 0);

		// initialize the net func profiler
		NOTFINAL_ONLY(netFunctionProfiler::Init();)

		m_CxnMgr = new netConnectionManager();
		m_CxnMgr->Init(m_NetAllocator, 1, 1, m_Socket, NETWORK_RESERVED_CHANNEL_ID, NAT_TRAVERSAL_CHANNEL_ID, 0);
	}

	void Init()
	{
		InitTitleId();
		InitNetwork();

		m_State = STATE_WAIT_FOR_ONLINE;
	}

	void NetUpdate()
	{
		m_NetAllocator->SanityCheck();

		const unsigned curTime = sysTimer::GetSystemMsTime();

		netUpdate();

		rlUpdate();

		m_CxnMgr->Update(curTime);

		sysIpcSleep(15);
	}

	virtual void InitTest() = 0;
	virtual void UpdateTest() = 0;

	void UpdateLoop()
	{
		rlDebug("UpdateLoop");

		while(!m_EndTest)
		{
			NOTFINAL_ONLY(rage::netFunctionProfiler::GetProfiler().StartFrame());

			NetUpdate();

			// if our parent process terminates, then we terminate
			if(m_ParentPid != 0)
			{
				HANDLE process = OpenProcess(SYNCHRONIZE, FALSE, m_ParentPid);
				DWORD ret = WaitForSingleObject(process, 0);
				CloseHandle(process);
				if(ret != WAIT_TIMEOUT)
				{
					// parent process isn't running, bail
					TerminateProcess(NULL, 0);
				}
			}

			switch(m_State)
			{
			case Test_netCxnMgrApp::STATE_WAIT_FOR_ONLINE:
				if(netRelay::IsConnectedToRelay() && netNatDetector::GetLocalNatType() != NET_NAT_UNKNOWN)
				{
					Displayf("Ready for multiplayer");

					// TODO: NS - use correct user index
					rlPresence::SetActingUserIndex(0);
									
					u32 pid = 0;
					PARAM_netTestParentPid.Get(pid);
					m_ParentPid = pid;

					const char* peerAddrStr = NULL;
					if(!PARAM_netTestHostPeerAddr.Get(peerAddrStr))
					{
						Displayf("Taking host path");

						// we're the host - we pass our peer address via command line to the subprocess
						m_IsHost = true;
						m_State = STATE_CREATE_SUBPROCESS;
						InitTest();

						// need to listen for incoming connection requests
						m_CxnMgr->StartListening(NET_CONNECTION_CHANNEL_ID);
					}
					else
					{
						Displayf("Taking client path");

						// we're the client - we receive the peer address via command line from our parent process
						m_IsHost = false;
						m_PeerAddr.FromString(peerAddrStr);
						InitTest();
						m_State = STATE_RUN_TEST;
					}
				}
				break;
			case Test_netCxnMgrApp::STATE_CREATE_SUBPROCESS:
				{
					CreateSubprocess();
					m_State = STATE_RUN_TEST;
				}
				break;
			case STATE_RUN_TEST:
				{
					UpdateTest();
				}
				break;
			default:
				break;
			}

			NOTFINAL_ONLY(rage::netFunctionProfiler::GetProfiler().TerminateFrame());
		}

		// Close the child process after the end of the test. (Doesn't work very gracefully)
		if (!m_IsHost) 
		{
			rlDisplay("Closing child process");
			_exit(EXIT_SUCCESS);
		}
		// if our parent process terminates, then we terminate
	}

	void Shutdown()
	{
		NOTFINAL_ONLY(rage::netFunctionProfiler::Deinit(););
	}
	
	void SetTestPassed()
	{
		m_TestPassed = true;
	}

	bool GetTestPassed()
	{
		return m_TestPassed;
	}

	enum State
	{
		STATE_WAIT_FOR_ONLINE,
		STATE_CREATE_SUBPROCESS,
		STATE_RUN_TEST
	};

	State m_State;
	bool m_TestPassed;
	sysMemSimpleAllocator* m_NetAllocator;
	netSocket* m_Socket;
	PROCESS_INFORMATION m_SubprocessInfo;
	rlTitleId* m_TitleId;
	netConnectionManager* m_CxnMgr;
	netPeerAddress m_PeerAddr;
	DWORD m_ParentPid;
	bool m_IsHost;
	bool m_EndTest;
};

class TestUnsolicitedConnection : public Test_netCxnMgrApp
{
public:
	TestUnsolicitedConnection()
	: m_State(STATE_WAIT_FOR_CONNECTION_REQUEST)
	{

	}

	bool OpenTunnel()
	{
		Assert(m_PeerAddr.IsValid());

		const netTunnelSecurityParams secParams(false, 0, 0);
		
		return m_CxnMgr->OpenTunnel(m_PeerAddr,
									netAddress(),
									secParams,
									NET_TUNNELTYPE_ONLINE,
									NET_TUNNEL_REASON_P2P,
									0, 
									true,
									&m_TunnelRequest,
									&m_ConnectionStatus);
	}

	bool OpenConnection()
	{
		rtry
		{
			char peerAddrStr[netPeerAddress::TO_STRING_BUFFER_SIZE] = {0};
			netPeerAddress peerAddr;

			rverify(netPeerAddress::GetLocalPeerAddress(&peerAddr), catchall, );
			rverify(peerAddr.ToString(peerAddrStr, sizeof(peerAddrStr)) != NULL, catchall, );

			m_CxnId = m_CxnMgr->OpenConnection(m_PeerAddr,
									 m_Addr,
									 netAddress(), 
									 NET_CONNECTION_CHANNEL_ID,
									 peerAddrStr,
									 (unsigned)strlen(peerAddrStr) + 1,
									 &m_ConnectionStatus);

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void Close()
	{
		if(m_TunnelRequest.Pending())
		{
			m_CxnMgr->CancelTunnelRequest(&m_TunnelRequest);
		}

		if(m_CxnMgrDelegate.IsRegistered())
		{
			m_CxnMgr->RemoveDelegate(&m_CxnMgrDelegate);
		}

		m_PeerAddr.Clear();

		m_State = STATE_CLOSED;

		
	}

	void InitTest()
	{
		m_CxnMgrDelegate.Bind(this, &TestUnsolicitedConnection::OnConnectionEvent);
		m_CxnMgr->AddDelegate(&m_CxnMgrDelegate, NET_CONNECTION_CHANNEL_ID); 
		if(m_IsHost)
		{
			m_State = STATE_WAIT_FOR_CONNECTION_REQUEST;
		}
		else
		{
			m_State = STATE_OPEN_TUNNEL;
		}

		Displayf("State: %d", m_State);
	}

	void OnConnectionEvent(netConnectionManager* OUTPUT_ONLY(cxnMgr), const netEvent* evt)
	{
		netDebug2("OnConnectionEvent :: %s, State: %d", evt->GetAutoIdNameFromId(evt->GetId()), m_State);

		if(evt->GetId() == NET_EVENT_CONNECTION_REQUESTED && (m_State == STATE_WAIT_FOR_CONNECTION_REQUEST))
		{
			netEventConnectionRequested* cxnRqst = evt->m_CxnRequested;
			char peerAddrStr[netPeerAddress::TO_STRING_BUFFER_SIZE] = {0};
			safecpy(peerAddrStr, (char*)cxnRqst->m_Payload);

			if(!m_PeerAddr.FromString(peerAddrStr))
			{
				netDebug2("OnConnectionEvent :: Mismatched peer address: %s", m_PeerAddr.ToString());
				m_PeerAddr.Clear();
			}
			else
			{
				rlDebug("Accepting connection from " NET_ADDR_FMT "...",
					NET_ADDR_FOR_PRINTF(cxnRqst->m_Sender));

				m_Addr = cxnRqst->m_Sender;
				m_CxnId = m_CxnMgr->AcceptConnection(m_PeerAddr, cxnRqst->m_Sender, netAddress(), cxnRqst->m_ChannelId, cxnRqst->m_Sequence, &m_ConnectionStatus);

				if(m_CxnId != NET_INVALID_CXN_ID)
				{
					rlDebug("Accepted connection from " NET_ADDR_FMT ", Id: 0x%08x",
						NET_ADDR_FOR_PRINTF(m_Addr), 
						m_CxnId);
					m_State = STATE_OPENING_CONNECTION;
				}
				else
				{
					rlDebug("Failed to accept connection from " NET_ADDR_FMT "...",
						NET_ADDR_FOR_PRINTF(m_Addr));
				}
			}
		}
		else if(evt->m_CxnId != m_CxnId)
		{
			// not for this connection
			return;
		}

		switch(evt->GetId())
		{
		case NET_EVENT_CONNECTION_ERROR:
		case NET_EVENT_CONNECTION_CLOSED:
			{
	#if !__NO_OUTPUT
				static const char* szCxnErr[] =
				{
					"TimedOut",		// CXNERR_TIMED_OUT
					"SendError",	// CXNERR_SEND_ERROR
					"Terminated",	// CXNERR_TERMINATED
				};

				if(evt->GetId() == NET_EVENT_CONNECTION_ERROR)
					rlDebug("OnConnectionEvent :: Error [0x%08x] :: Addr: %s, Reason: %s", evt->m_CxnId, cxnMgr->GetAddress(evt->m_CxnId).ToString(), szCxnErr[evt->m_CxnError->m_Code]);
				else if(evt->GetId() == NET_EVENT_CONNECTION_CLOSED)
					rlDebug("OnConnectionEvent :: Closed [0x%08x] :: Addr: %s", evt->m_CxnId, cxnMgr->GetAddress(evt->m_CxnId).ToString());
	#endif

				if(m_ConnectionStatus.Pending())
				{
					m_ConnectionStatus.SetFailed();
				}

				Displayf("Connection terminated");
				Close();
			}
			break;

		default:
			break; 
		}
	}

	virtual void UpdateTest()
	{
		rlDebug("UpdateTest");

		switch(m_State)
		{
		case STATE_WAIT_FOR_CONNECTION_REQUEST:
			break;
		case STATE_CLOSED:
			m_EndTest = true;
			break;
		case STATE_OPEN_TUNNEL:
			rlDebug("Opening tunnel to %s...", m_PeerAddr.ToString());
			if(OpenTunnel())
			{
				m_State = STATE_OPENNING_TUNNEL;
			}
			else
			{
				rlError("Failed to open tunnel to %s...", m_PeerAddr.ToString());
				Close();
			}
			break;
		case STATE_OPENNING_TUNNEL:
			if(m_ConnectionStatus.Succeeded())
			{
				m_Addr = m_TunnelRequest.GetNetAddress();
				m_State = STATE_OPEN_CONNECTION;
			}
			else if(!m_ConnectionStatus.Pending())
			{
				rlError("Failed to open tunnel to %s...", m_PeerAddr.ToString());
				Close();
			}
			break;
		case STATE_OPEN_CONNECTION:
			rlDebug("Opening connection to " NET_ADDR_FMT "...",
						NET_ADDR_FOR_PRINTF(m_PeerAddr));

			rlDebug("Opening connection to " NET_ADDR_FMT "...",
					NET_ADDR_FOR_PRINTF(m_Addr));

			if(OpenConnection())
			{
				m_State = STATE_OPENING_CONNECTION;
			}
			else
			{
				rlError("Failed to open connection to " NET_ADDR_FMT "...",
					NET_ADDR_FOR_PRINTF(m_Addr));
				Close();
			}
			break;
		case STATE_OPENING_CONNECTION:
			if(m_ConnectionStatus.Succeeded())
			{
				rlDebug("Connected to " NET_ADDR_FMT "...",
					NET_ADDR_FOR_PRINTF(m_Addr));

				m_State = STATE_OPEN;
			}
			else if(!m_ConnectionStatus.Pending())
			{
				rlError("Failed to open connection to %s...", m_PeerAddr.ToString());
				Close();
			}
			break;
		case STATE_OPEN:
			rlDebug("m_State = STATE_OPEN, test passed and ending test");
			SetTestPassed();
			m_EndTest = true;
			Close();
			break;
		}
	}

	enum State
	{
		STATE_CLOSED,
		STATE_WAIT_FOR_CONNECTION_REQUEST,
		STATE_OPEN_TUNNEL,
		STATE_OPENNING_TUNNEL,
		STATE_OPEN_CONNECTION,
		STATE_OPENING_CONNECTION,
		STATE_OPEN
	};

	State m_State;
	netConnectionManager::Delegate m_CxnMgrDelegate;
	netStatus m_ConnectionStatus;
	netTunnelRequest m_TunnelRequest;
	netAddress m_Addr;
	int m_CxnId; 
};

class TestZeroLoss : public TestUnsolicitedConnection
{
public:
	enum
	{
		NET_CHANNEL_12 = 12,
	};

	TestZeroLoss()
	: m_Next(0)
	{

	}

	void SendNext()
	{
		Displayf("TestZeroLoss %s sent %u", m_IsHost ? "Host" : "Client", m_Next);
		//m_CxnMgr->Send(m_EndpointId, NET_CHANNEL_12, &m_Next, sizeof(m_Next), 0, NULL);
	}

	void OnConnectionEvent(netConnectionManager* cxnMgr, const netEvent* evt)
	{
		TestUnsolicitedConnection::OnConnectionEvent(cxnMgr, evt);

		//if(evt->GetEndpointId() != m_EndpointId)
		//{
		//	// not for this endpoint
		//	return;
		//}

		switch(evt->GetId())
		{
		case NET_EVENT_FRAME_RECEIVED:
			{
				netEventFrameReceived* fr = evt->m_FrameReceived;
				Assert(fr->m_SizeofPayload == sizeof(m_Next));

				u32 val = 0;
				memcpy(&val, fr->m_Payload, sizeof(val));

				Displayf("TestZeroLoss %s received %u", m_IsHost ? "Host" : "Client", val);

				Assert(val == (m_Next - 1));

				SendNext();
				m_Next += 2;
			}
			break;

		default:
			break; 
		}
	}

	void InitTest()
	{
		TestUnsolicitedConnection::InitTest();
		m_CxnMgrDelegate.Bind(this, &TestZeroLoss::OnConnectionEvent);
		//m_CxnMgr->AddChannelDelegate(&m_CxnMgrDelegate, NET_CHANNEL_12); 

		m_Next = m_IsHost ? 0 : 1;
	}

	virtual void UpdateTest()
	{
		TestUnsolicitedConnection::UpdateTest();
		if(TestUnsolicitedConnection::m_State == STATE_OPEN)
		{
			if(m_IsHost)
			{
				if((m_Next == 0))
				{
					SendNext();
					m_Next += 2;
				}

				if(m_Next >= 100)
				{
					m_EndTest = true;
				}
			}
		}
	}

	unsigned m_Next;
	netConnectionManager::Delegate m_CxnMgrDelegate;
};

NET_TEST_CLASS(Test_netCxnMgr)
{
	NET_TEST_INIT(Init_netCxnMgr)
	{

	}

	NET_TEST_METHOD(Connect)
	{
		/*TestUnsolicitedConnection app;
		app.Init();
		app.UpdateLoop();
		netTestAssert(app.GetTestPassed());
		app.Shutdown();*/
	}
	NET_TEST_METHOD_END

	NET_TEST_METHOD(TestZeroLoss)
	{
		//netTestAssert(app.GetTestPassed());
// 		TestZeroLoss app;
// 		app.Init();
// 		app.UpdateLoop();
// 		netTestAssert(app.GetTestPassed());
// 		app.Shutdown();
	}
	NET_TEST_METHOD_END
}
NET_TEST_CLASS_END(Test_netCxnMgr)

#endif // UNITTEST_ENABLE_NET_CXNMGR
