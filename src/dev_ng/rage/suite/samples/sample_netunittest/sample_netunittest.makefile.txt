Project netUnitTest
RootDirectory netUnitTest

ConfigurationType dll

IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\base\samples
IncludePath netUnitTest
IncludePath $(VCInstallDir)UnitTest\include

Files {
	Package.Appxmanifest
    Test_example.cpp
    Test_websocket.cpp
	Test_netCxnMgr.cpp
	Test_netCrypto.cpp
	Test_netHttpRequest.cpp
    test_netTask2.cpp
	Test_netResolver.cpp
	Test_rageRson.cpp
	Test_netUri.cpp
	Test_session.cpp
	Test_rageUtf8.cpp
	Test_rlPcPipe.cpp
	testmatrix.h
	netUnitTest_fnt.h
	netUnitTest_fnt_dds.h
    netUnitTestFramework.h
    netUnitTestFramework.cpp
}


Libraries { 
 ..\..\..\base\src\vcproj\RageCore\RageCore
 ..\..\..\base\src\vcproj\RageCreature\RageCreature
 ..\..\..\base\src\vcproj\RageGraphics\RageGraphics
 ..\..\..\base\src\vcproj\RageNet\RageNet
 ..\..\..\base\src\vcproj\RageAudio\RageAudio
 %RAGE_DIR%\suite\src\snet\snet
 %RAGE_DIR%\suite\src\avchat\avchat
}