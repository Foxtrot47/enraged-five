// 
// sample_net_physics/netsupport_host.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "netsupport_host.h"

#include "system/ipc.h"

using namespace ragenetsamples;
using namespace ragesamples;
using namespace rage;

// TITLE: Host
// PURPOSE:
//		Host side logic for a simple server-client based game.

//how often the stats are outputed onto the console, in seconds.
const float hostAppController::K_STATS_UPDATE_HOST_RATE = 1.f;

void peerSession::OnEnter ( int /*oldState*/ )
{
    switch(GetCurrentState()){
                case APP_SESSION_STARTING: 
                    //start downloading the level
                    OnTransition(APP_SESSION_DOWNLOADING);
                    break;
                case APP_SESSION_CONNECTED: break;
                case APP_SESSION_DOWNLOADING: 
                    //to download the level we send the peer..
                    //0)start scene download msg
                    m_pHostApp->SendToReliable(m_ConnectionID, netMessage( AppProtocolMsg( AppProtocolMsg::FP_START_SCENE_DOWNLOAD )));
                    //1)the current scene
                    m_pHostApp->SendSceneToPeer(m_ConnectionID);
                    //2)stop scene download msg
                    m_pHostApp->SendToReliable(m_ConnectionID, netMessage( AppProtocolMsg( AppProtocolMsg::FP_SCENE_DOWNLOAD_FINISH )));
                    break;
                case APP_SESSION_DOWNLOADING_OBJECT:
                    //3)start objects download msg
                    m_pHostApp->SendToReliable(m_ConnectionID, netMessage( AppProtocolMsg( AppProtocolMsg::FP_START_OBJECT_DOWNLOAD )));
                    //4)all the objects in the world 
                    m_pHostApp->SendObjectsToPeer(m_ConnectionID);
                    //5)stop objects download msg
                    m_pHostApp->SendToReliable(m_ConnectionID, netMessage( AppProtocolMsg( AppProtocolMsg::FP_OBJECT_DOWNLOAD_FINISH )));

                    //upon the completion of step 5 on the remote side, the 
                    //peer should respond with a protocol msg indicating they have finish
                    //insantating the scene, we can then allow the peer to enter the game proper..
                    break;
                case APP_SESSION_IN_GAME: 
                    //tell the peer to enter the game
                    m_pHostApp->SendToReliable(m_ConnectionID, netMessage( AppProtocolMsg( AppProtocolMsg::FP_START_ENTER_GAME )));

                    //we now consider all objects bound to this connection, use the sync manager
                    //to add thie object to the set of managed object for this connectionID
                    //                    m_pHostApp->GetGameInstance()->BindAllObjects(m_ConnectionID,0);
                    break;
                case APP_SESSION_EXITING: 
                    break;
    }
}

//////////////////////////////////////////////////////////////////////////

//
//PURPOSE:  dtor for hostAppController, make sure to destroy the sessions associated with the host..
//
hostAppController::~hostAppController(){

    for (int ndx = 0; m_SessionContainer.GetCount() < ndx; ndx++) 
    {
        if (m_SessionContainer[ndx] != NULL)
        {
            delete m_SessionContainer[ndx];
            m_SessionContainer[ndx] = NULL;
        }
    }
}

//  STEP #1. Setup the host statemachine.

//entering the m_CurrentAppState
void  hostAppController::OnEnter             ( int /*oldState*/ ){

    Assert(GetGameInstance()!=NULL);

    //Odd a null game instance...
    if (GetGameInstance()==NULL){
        return;
    }

    switch(GetCurrentState()){

        //--
        //<I>On Startup</I><P>
        //The host logic is for the most part encasupalted within a single transition function
        //the OnEnter. Using a switch statement we can perform operations on entering a particualr state.
        //This function encodes the overall host behavior.<P>
        //Starting up we'll need to set the policies
            case APP_HOST_STARTING: 
                    OnTransition(APP_HOST_NET_START);
#if __DEV
                    m_JoinPolicies.SetKeepAliveInterval( 0 );
                    m_JoinPolicies.SetQoSInterval( 0 );
                    m_JoinPolicies.SetConnectTimeout( 10*1000 );
#endif
                    m_JoinPolicies.SetMaxRetries( 100 );
                    m_JoinPolicies.SetRetransTimeout( 10*1000 );

#if __DEV
                    m_NormalPolicies.SetKeepAliveInterval( 0 );
                    m_NormalPolicies.SetQoSInterval( 0 );
                    m_NormalPolicies.SetConnectTimeout( 1*1000 );
#endif
                    m_NormalPolicies.SetMaxRetries( 100 );

                    m_StatsTimmer.Start(K_STATS_UPDATE_HOST_RATE);

                    break;
                    //--
                    //<I>Init World</I><P>
                case APP_HOST_WORLD_START: 
                    if (GetGameInstance()->InitWorldPhysics() && GetGameInstance()->CreateDemoWorld()){
                        OnTransition(APP_HOST_RUNNING);
                    } else {
                        OnTransition(APP_HOST_SHUTDOWN);
                    }
                    break;
                    //--
                    //<I>Init Network</I><P>
                case APP_HOST_NET_START: 
                    if (GetGameInstance()->InitNetwork()){

                        // Set the polciy in normal mode..
                        GetGameInstance()->GetNetManager().SetPolicies( m_NormalPolicies );

                        // Bind the callback function for processing the net messages
                        GetGameInstance()->GetNetMessageHanlder().Reset< hostAppController, 
                            &hostAppController::HandleMessage >( this );

                        OnTransition(APP_HOST_WORLD_START);
                    } else {
                        OnTransition(APP_HOST_SHUTDOWN);
                    }
                    break;
                    //--
                    //<I>Stable State</I><P>
                case APP_HOST_RUNNING: 
                    break;
                    //--
                    //<I>Shutting Down</I><P>
                case APP_HOST_SHUTDOWN:
                    break;
                     //-STOP
    }
}

//
//PURPOSE:  refresh logic for each state..
//
void hostAppController::Refresh ( void ){
    switch(GetCurrentState()){
                case APP_HOST_STARTING: 
                case APP_HOST_WORLD_START: 
                case APP_HOST_NET_START: 
                    break;
                    //While the game is running, the host is refreshed. When the host is in
                    //the stable state (APP_HOST_RUNNING), it waits for incoming connections.
                case APP_HOST_RUNNING: 
                    if (GetGameInstance())
                    {
                        GetGameInstance()->Refresh();

                        //output our stats if they are enabled..
                        if (GetGameInstance()->IsNetStatsEnabled() && m_StatsTimmer.Check())
                        {
                            for (int ndx = 0;ndx < m_SessionContainer.GetCount(); ndx++) 
                            {
                                if (m_SessionContainer[ndx] != NULL)
                                {
                                    GetGameInstance()->OutputNetStats(m_SessionContainer[ndx]->GetConnectionID());
                                }
                            }

                            m_StatsTimmer.Next(K_STATS_UPDATE_HOST_RATE);
                        }

                        //we call this 2x once in the gameinstance refresh loop and once
                        //again here, to send off any pending messages, to reduce the transctional
                        //latency..
                        GetGameInstance()->GetNetManager().Update();
                    }
                    break;
                case APP_HOST_SHUTDOWN: 
                    break;
    }
}

//  STEP #2. Process incoming client.

//
//PURPOSE:  Callback that handles messages dispatched from the connection manager.
//
unsigned hostAppController::HandleMessage ( const netAddress& sender, const int connectionId, const netMessage& msg ){
    unsigned retval = 0;

    if (GetGameInstance() == NULL){
        return retval;
    }

    //////////////////////////////////////////////////////////////////////////
    //Handle connection requests
    if (connectionId<0){
        //--
        //Upon a successful connection attempt by the client, we're notified by the low level network system
        //through a RequestConnectMsg net message. This message is a user defined message, the first sent
        //by the connecting peer.

        if( msg.GetId() == RequestConnectMsg::GetMessageId() )
        {
            //Accept the connection.
            const int conn =
                GetGameInstance()->GetNetManager().AcceptConnection( sender,
                netMessage( AppProtocolMsg(AppProtocolMsg::FP_ACKNOWLEDGE_CONNECT) ) );

            Assert(conn>=0);
            Assert(GetGameInstance()!=NULL);

            //--
            //Every connecting peer is managed by a peerSession state object which controls its host side
            //micro logic.

            peerSession* pNewSession = rage_new peerSession(conn,this);

            Assert(pNewSession);

            pNewSession->OnStart();

            //-STOP

            bool CreateNewSlot = true;

            //see if we can reuse a session slot
            for (int ndx = 0;ndx < m_SessionContainer.GetCount(); ndx++) 
            {
                if (m_SessionContainer[ndx] == NULL)
                {
                    m_SessionContainer[ndx] = pNewSession;
                    CreateNewSlot = false;
                    break;
                }
            }

            if (CreateNewSlot){
                m_SessionContainer.PushAndGrow(pNewSession,1);
            }

            //set the net policy so its more friendly for joining players
            //once they complete the join process the policy will return to normal
            m_NumJoiners++;

            GetGameInstance()->GetNetManager().SetPolicies(m_JoinPolicies);

            retval |= K_RTVAL_HANDLED;
        }
    }

    //////////////////////////////////////////////////////////////////////////
    //Handle protocol messages
    if( msg.GetId() == AppProtocolMsg::GetMessageId() )
    {
        //Protocol message is used for peers and host to talk to each other.

        //Retrieve the message data from the message
        AppProtocolMsg RealMsg;

        msg.Deserialize( &RealMsg );

        Assert(!RealMsg.m_ForClient);

        if (!RealMsg.m_ForClient){

   
            switch (RealMsg.m_ProtocolEnum){
                //--
                //The host and client communcaite through the generic AppProtocolMsg net message. This message
                //is used to cooridnate host/client behavior.<P>
                //<I>Scene Sycning</I><P>Once the peer finsihes creating the scene we can start the object download
                case AppProtocolMsg::FH_PEER_FINISH_CREATE_SCENE:
                    {
                        peerSession* pSession = FindSessionByConnection(connectionId);

                        if (pSession){
                            pSession->StartObjectDownload();
                        } else {
                            Assertf(pSession!=NULL,"Could not find valid connection for given ID %d",connectionId);
                        }
                    }
                    break;
                    //--
                    //<I>Object Sycning</I><P>Once the clients finishes the object download we can notify the 
                    //net object manager of the joining peer. This will complete the client
                    //initial synchronization process.. Note the objects downloaded are not network object, they are
                    //external game objects used outside of the network object manager. The synchorinzaiton of 
                    //the network objects starts when we notify the network object manager of the incoming peer.
                case AppProtocolMsg::FH_PEER_FINISH_CREATE_OBJECTS: 
                    {
                        peerSession* pSession = FindSessionByConnection(connectionId);

                        if (pSession){
                            pSession->EnterGame();
                        } else {
                            Assertf(pSession!=NULL,"Could not find valid connection for given ID %d",connectionId);
                        }

                        m_NumJoiners--;

                        if (m_NumJoiners==0){
                            GetGameInstance()->GetNetManager().SetPolicies(m_NormalPolicies);
                        }

                        //--
                        //Notify the object manager of the joining peer. By notifying the object manager, the processes
                        //is started to synchonize the net objects. This is an involved process starting with peer ID
                        //negoitaiton, syncing of net tokens and eventually instanting the active net objects.

                        GetGameInstance()->GetNetObjManager().OnPeerJoin( (nsynConnectionId)connectionId );

                        //-STOP
                    }
                    break;

                    //--
                    //<I>Exiting</I><P>When the client wishes to exit, we are notified by AppProtocolMsg.
                case AppProtocolMsg::FH_CLIENT_SHUTDOWN: 
                    //we proably should let the net object manager complete its exiting process for
                    //the given connection before we attempt to close the connection itself.. TODO(ddn)
                    
                    //--
                    //Be sure to notify the object manager of the exiting peer so we can clean up its state.
                    GetGameInstance()->GetNetObjManager().OnPeerExit( (nsynConnectionId)connectionId );

                    UnBindConnection( connectionId );
                    
                    GetGameInstance()->GetNetManager().CloseConnection( connectionId );
                    break;
                    //-STOP
            }

            retval |= K_RTVAL_HANDLED;
        }
    }
    
    //--
    //<I>Irregular Exit</I><P>If the client leaves the game irregularly, we are notified by the low level net system through
    //the netNotifyDropMessage net message. 

    if ( msg.GetId() == netNotifyDropMessage::GetMessageId())
    {   
        //Retrieve the message data from the message
        netNotifyDropMessage RealMsg;

        msg.Deserialize( &RealMsg );

        //--
        //We have to notify the object manager of the exiting peer to clean up its state.

        GetGameInstance()->GetNetObjManager().OnPeerExit( (nsynConnectionId)connectionId );

        //-STOP

        UnBindConnection( connectionId );

        GetGameInstance()->GetNetManager().CloseConnection( connectionId );
    }

    return retval;
}


//
//PURPOSE:  sends the scene info to the peer
//
void    hostAppController::SendSceneToPeer ( const int connID ){

    Assert(GetGameInstance()!=NULL);

    if (GetGameInstance()==NULL){
        return;
    }

    const netSceneObject& rScene = GetGameInstance()->GetSceneData();

    CreateSceneMsg Msg(rScene);

    SendToReliable(connID,netMessage(Msg));
}

//
//PURPOSE:  sends all the active objects to the peer
//
void    hostAppController::SendObjectsToPeer ( const int /*connID*/ ){

    //    Assert(GetGameInstance()!=NULL);
    //
    //    if (GetGameInstance()==NULL){
    //        return;
    //    }
    //
    //    gameInstance::ObjectIterator iter = GetGameInstance()->GetObjectIterator();
    //
    //    //for each object in the container, we send a create message for it..
    //    iter.Start();
    //
    //#if __DEV
    //    int counter = 0;
    //#endif
    //
    //    while (!iter.AtEnd()){
    //        netObjecControllerBase* pObject = iter.GetData();
    //
    //        //send off the creation message for this object
    //        CreateObjectMsg Msg(*pObject);
    //
    //        SendToReliable(connID,netMessage(Msg));
    //
    //#if     __DEV
    //    counter++;
    //
    //    //flush the message queue at interveals for testing purposes
    //    if ((counter%63)==0){
    //        GetGameInstance()->GetNetManager().Update();
    //
    //        //wait 1 sec between send blocks, we do this to prevent the 
    //        //client side from losing packets due to CPU load..
    //        sysIpcSleep( 1000 );
    //    }
    //#endif
    //
    //        iter.Next();
    //    }
}

//
//PURPOSE:  removes the connection object from the sync mngr
//
void    hostAppController::UnBindConnection (int connID){
    Assert(GetGameInstance()!=NULL);

    if (GetGameInstance()==NULL){
        return;
    }

    //    gameInstance::ObjectIterator iter = GetGameInstance()->GetObjectIterator();
    //
    //    //for each object in the container, we unbind it from that connection..
    //
    //    iter.Start();
    //
    //    while (!iter.AtEnd()){
    //        netObjecControllerBase* pObject = iter.GetData();
    //
    //        GetGameInstance()->GetSyncManager().UnbindMaster( pObject, connID );
    //
    //        iter.Next();
    //    }

    //Remove the connection form the session container
    for (int ndx = 0;ndx < m_SessionContainer.GetCount(); ndx++) 
    {
        if (m_SessionContainer[ndx] != NULL  && 
            m_SessionContainer[ndx]->GetConnectionID() == connID)
        {
            delete m_SessionContainer[ndx];

            m_SessionContainer[ndx]=NULL;

            break;
        }
    }

}

//
//PURPOSE:  finds a peerSession for the given connection ID, can return NULL
//
peerSession*    hostAppController::FindSessionByConnection  ( const int connID ){

    //linear search, this could be a bottleneck if we have many sessions, better to use a map|hash table
    //then if we are managing that many sessions..

    for (int ndx = 0;ndx < m_SessionContainer.GetCount(); ndx++) 
    {
        if (m_SessionContainer[ndx] != NULL  && 
            m_SessionContainer[ndx]->GetConnectionID() == connID)
        {
            return m_SessionContainer[ndx];
        }
    }

    return NULL;
}

//
//PURPOSE:  sends a message to all connected peers, reliably
//
void hostAppController::SendAllPeersReliable    (const netMessage& rMsg){
    for (int ndx = 0;ndx < m_SessionContainer.GetCount(); ndx++) 
    {
        if (m_SessionContainer[ndx] != NULL)
        {
            SendToReliable(m_SessionContainer[ndx]->GetConnectionID(), rMsg);
        }
    }
}
