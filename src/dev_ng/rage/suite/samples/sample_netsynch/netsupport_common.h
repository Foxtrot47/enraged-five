// 
// sample_net_physics/netsupport_common.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_NET_PHYSICS_NETSUPPORT_COMMON_H
#define SAMPLE_NET_PHYSICS_NETSUPPORT_COMMON_H

#include "atl/ptr.h"
#include "atl/referencecounter.h"
#include "atl/string.h"
#include "math/random.h"
#include "net/netconnectionmanager.h"
#include "net/nethoster.h"
#include "net/netstats.h"
#include "system/timemgr.h"
#include "system/timer.h"

#include "netsynch/objectmanager.h"
#include "netsynch/manager.h"
#include "netsynch/base.h"
#include "netsynch/event.h"

#include "sample_physics/demoworld.h"
#include "sample_physics/demoobject.h"
#include "sample_physics/sample_physics.h"
#include "netsupport_messages.h"

using namespace rage;

template < typename C, typename T > C ConvertIntergral(const T& rVal){
    //hmmm.. need a way to handle sign/unsigned compare without causing comiler to barff..TODO(DDN)
    AssertMsg(rVal==(T)((C)rVal),"conversion results in data loss");
    return (C) rVal;
}

//partial specialzied version..
template < typename T > float ConvertIntergral(const T& rVal){
    netDebug3(("conversion might result in data loss"));
    return (float) rVal;
}

//specialzied version..
template <> int ConvertIntergral<int,float>(const float& rVal);

//helpful auto-checking macros for conversion from intergral types..
//in release/final we can define this to be straight up type conversion..
#define NET_TO_U32(X)   (ConvertIntergral<u32>(X))
#define NET_TO_S32(X)   (ConvertIntergral<s32>(X))
#define NET_TO_U16(X)   (ConvertIntergral<u16>(X))
#define NET_TO_S16(X)   (ConvertIntergral<s16>(X))
#define NET_TO_U8(X)    (ConvertIntergral<u8>(X))
#define NET_TO_S8(X)    (ConvertIntergral<s8>(X))
#define NET_TO_INT(X)   (ConvertIntergral<int>(X))

namespace ragenetsamples 
{
    extern const unsigned K_RTVAL_HANDLED;

    class rage::nsynObjectManager;

    struct SampleSetup{   

        typedef short SAMPLE_OBJECT_TYPE;

        SampleSetup();

        void PrintSetupInfo(void);

        bool ExtractFromCommandline();
        int m_UseSimulatedLag;
        int m_MessageHeapSize;
        int m_NetTimePingBuckets;
        float m_UpdateFreqSecs;
        float m_UpdateVariance;
        int m_NumObjectsCreate;
        float m_ObjectDistrib;
        float m_ObjectRotVariance;
        SAMPLE_OBJECT_TYPE m_SyncObjectType;
        const char* m_ObjectTypeToCreate;
        int m_Mode; 
        int m_FrameSleep;
        float m_TimeScale;
        bool m_UseForcedSync;
        int m_WorldConstruct;
        bool m_SpawnAwake;
    };

    //////////////////////////////////////////////////////////////////////////
    //unique ID used for network purposes.
    //////////////////////////////////////////////////////////////////////////
    struct netId {
        netId(const int in=-1): m_ID(in){}
        int		m_ID;			//synchronized ID across the network to refer to an object
    };

    //////////////////////////////////////////////////////////////////////////
    //simple timmer object which makes the management of the updates timmer alittle easier.
    //////////////////////////////////////////////////////////////////////////
    class TimeHelperObject{
    public:

        void Start( float offset ){
            m_nextTime = TIME.GetElapsedTime() + offset;
        }

        bool Check(void){
            if (TIME.GetElapsedTime() > m_nextTime){
                return true;
            }

            return false;
        }

        void Next(float offset){
            m_nextTime = TIME.GetElapsedTime() + offset;
            //m_nextTime += offset;
        }

        void Update(void){
            m_nextTime = TIME.GetElapsedTime() ;
        }

        float Delta(void){
            return (TIME.GetElapsedTime() - m_nextTime);
        }

    private:

        float   m_nextTime;     //next time this object will update in seconds, 0 = uninitlaizied

    };

  
    // PURPOSE
    //  internal manager events, used to notify parts of the application 
    //  about manager related funcitonality.
    class nsynEvtGame : public rage::nsynEvtBase 
    {
        NET_EVENT_DECL(nsynEvtGame);
    public:

        enum nsynEvtGameEvent
        {   
            EVT_EVENT_NULL = 0,
            EVT_EVENT_RESET_WORLD,          //command to reset the world
            EVT_EVENT_RESET_OBJECT,
        };

        nsynEvtGame()
        {
            m_Event = EVT_EVENT_NULL;
        } 

        nsynEvtGame(nsynEvtGameEvent event)
        {
            m_Event = event;
        }

        nsynEvtGame(nsynEvtGameEvent event, const Matrix34& mtx )
        {
            m_Event = event;
            m_Mtx   = mtx;
        }

        nsynEvtGameEvent GetEvent() const
        {
            return (nsynEvtGameEvent)m_Event;
        }

        const Matrix34& GetMatrix() const 
        {
            return m_Mtx;
        }

    private:

        SERALIZE_N(1, Matrix34,            m_Mtx);
        SERALIZE_N_END(2, int,             m_Event);

    };

    // PURPOSE
    //  Simple game object to demonstrate the usage of the object manager. The game object contains a message
    //  hanlding function which recives all network events associated with this object. The game object also
    //  demonstrates several concepts of dynamic binding of the managed object and controller/reciver.
    class SimpleGameObj : public phInst, public rage::nsynMsgHandler
    {
    public:

        SimpleGameObj(){
            m_NetObj = NULL;
        }

        virtual ~SimpleGameObj(){m_PriorityOverride = 0.f;};
       
        virtual void PreComputeImpacts (phImpact& UNUSED_PARAM(impactList), phInst* UNUSED_PARAM(hitInst));

        virtual void NotifyDeactivate();
       
        //
        // PURPOSE
        //  Callback into the network object upon receiving RcvApplyImpulse in phInst.
        // PARAMS
        //	impulse		- the impulse to apply
        //	position	- the position in world space at which to apply the impulse
        //	component	- optional composite bound part number (only used when the instance has a composite bound)
        //	push		- optional push to apply
        //
        virtual void    ApplyImpulse                 (const Vector3&  UNUSED_PARAM(impulse), const Vector3&  UNUSED_PARAM(position), int  UNUSED_PARAM(component), const Vector3*  UNUSED_PARAM(push));

        //
        // PURPOSE
        //  Callback into the network object upon receiving RcvApplyImpulse in phInst.
        // PARAMS
        //	impulse		- the impulse to apply
        //
        virtual void    ApplyImpulse                 (const Vector3&  UNUSED_PARAM(impulse));

        //
        // PURPOSE
        //  Callback into the network object upon receiving RcvApplyForce in phInst.
        // PARAMS
        //	force		- the force to apply
        //	position	- the position in world space at which to apply the force
        //	component	- optional composite bound part number (only used when the instance has a composite bound)
        //
        virtual void    ApplyForce                   (const Vector3& UNUSED_PARAM(force), const Vector3&  UNUSED_PARAM(position), int  UNUSED_PARAM(component))
        ;

        //
        // PURPOSE
        //  Callback into the network object upon receiving RcvPrepareForActivation in phInst.
        // PARAMS
        //  colliderToUse   - ???(undocumented in physics system)
        //  impactList      - ???(undocumented in physics system)
        //  impactData      - ???(undocumented in physics system)
        //  contactGroup    - ???(undocumented in physics system)
        // RETURNS
        //  phInst          - instance to activate ( presumably m_pInstance )
        // NOTES
        //  If activation is not possible or not permitted, it should return NULL.
        //
        virtual phInst *PrepareForActivation         (phCollider ** UNUSED_PARAM(colliderToUse), phImpact * UNUSED_PARAM(impactList),phImpactData * UNUSED_PARAM(impactData), const phContactMgr::Iterator * UNUSED_PARAM(contactGroup));

        //
        // PURPOSE
        //  Handles events for this object, implements object specific event handling logic ,overloads base class HandleEvent.
        // PARAMS
        //  --
        // RETURNS
        //  --
        virtual EventReturnType           HandleEvent ( const nsynEvtBase& event );

        float                   m_PriorityOverride;
        rage::nsynObjId             m_NetId;
        rage::nsynBase*      m_NetObj;
    };

    class gameInstance;
   
    //
    // PURPOSE
    // Encapsulates the creation of objects locally and remotely. It registers the bind function to the netObjManager
    //  which then manages the objects. An objects binding function is identified through the class Id of the types
    //  of objects it creates.
    //
    struct SimpleGameObjectNetFactory : public rage::nsynFactory  {
    public:
        NET_CLASS_DECL(SimpleGameObjectNetFactory);
    public:
        typedef atPtr<SimpleGameObjectNetFactory>        SPtr;        //Smartpointer to a SimpleGameObjectNetFactory
        typedef atPtr<const SimpleGameObjectNetFactory>  SPtrToConst; //Smartpointer to a const SimpleGameObjectNetFactory
    public:

        struct CmpObj {
            CmpObj (netBitBuffer* buffer, int mode, Vector3& val)
            {
                Assert(buffer!=NULL);
                serialize::Bind(buffer,mode,val);
            }
        };

        struct SetupData
        {
            SERALIZE_N(1, rage::atString,                  m_ObjectType);              //object type
            SERALIZE_N_CUSTOM(2, rage::Vector3,            m_Position, CmpObj);        //position
            SERALIZE_N_CUSTOM(3, rage::Vector3,            m_Rotation, CmpObj);        //rotation
            SERALIZE_N(4, bool,                            m_AlignBottom);             //flag to align the bottom of the object
            SERALIZE_N(5, bool,                            m_AlwaysActive);            //flag to set the object to be always active
            SERALIZE_N_END_NO_VIR(6, bool,                 m_StartActive);             //flag to start the object in active mode
        };

        SimpleGameObjectNetFactory(gameInstance* game){
            m_Game = game;
        }
        
    public:

        //Game specific impl

        bool                    RegisterGameObject                  ( SimpleGameObj* gameObj, SetupData& setup );
       
    public:

        //Net callbacks

        virtual rage::nsynBase*                  CreateSyncObject            ( const nsynToken* token );
        virtual bool                                CreateRemote                ( const nsynToken* token );
        virtual rage::nsynControllerBase*        CreateController            ( rage::nsynBase* netMngdObj );
        virtual rage::nsynReciverBase*           CreateReciver               ( rage::nsynBase* netMngdObj );
        virtual void                                RegisterSyncObject          ( rage::nsynBase* netMngdObj );
     virtual void                                ReleaseSyncObject           ( rage::nsynBase* netMngObject );

    private:

        gameInstance* m_Game;

    };

    //////////////////////////////////////////////////////////////////////////
    //base network specific data and interfaces associated with a physical object..
    //////////////////////////////////////////////////////////////////////////
    class nsynBaseOld : public phInst {
    public:
        NET_SYNC_CLASS_DECL(nsynBaseOld);
    public:
        nsynBaseOld(){
            m_pDemoObject	= NULL;
            m_pNetTimeMngr	= NULL;
            m_AlignBottom	= false;
            m_AlwaysActive	= false;
            m_StartActive	= false;
        }

        virtual ~nsynBaseOld(){
            m_pDemoObject = NULL;
        }

        virtual	void		Init			( bool /*IsHost*/ ){}
        virtual void		Refresh			( float /*timeSecsDelta*/ ){}
        virtual void        Update          ( void ){}
        virtual rage::u32	SyncSerialize	( rage::netBitBuffer* sync, const unsigned flags) const = 0;
        virtual void		SyncDeserialize	( const rage::netBitBuffer& sync ) = 0;
        virtual void        ForceSync       ( void ){}
        virtual void        ForceReset      ( void ){}

        ragesamples::phDemoObject*	m_pDemoObject;		//the logical physical object as known to the physics system
        nsynTimeManager*		    m_pNetTimeMngr;		//network time manager assocaited with this object
        rage::atString				m_ObjectType;		//object type
        Vector3						m_Position;			//inital position of the object
        Vector3						m_Rotation;			//inital rotation of the object
        int                         m_MngdObjectType;    //network object type
        netId						m_NetID;			//the network ID which is consistent on host/peer
        bool						m_AlignBottom;		//flag to align the bottom of the object
        bool						m_AlwaysActive;		//flag to set the object to be always active
        bool						m_StartActive;		//flag to start the object in active mode
    };

    //////////////////////////////////////////////////////////////////////////
    //thin network specific data associated with a scene..
    //////////////////////////////////////////////////////////////////////////
    class netSceneObject{
    public:
        bool                m_SimulatePhysics;      //turn physics on 
        Vector3             m_WorldOffset;          //global world offset

        int                 m_SyncObjectType;       //type of sync object were using on the remote side

        int                 m_MaxOctreeNodes;       //max octree nodes
        int                 m_MaxActiveObjects;     //max active objects physics system will manage
        int                 m_MaxObjects;           //max objects physics system will manage
        int                 m_MaxBehaviors;         //???

        Vector3             m_WorldMin;             //bounds of the world min
        Vector3             m_WorldMax;             //bounds of the world max

        rage::atString      m_SceneName;            //name/resoruce of the scene to load
        Vector3             m_Position;             //inital position of the scene 
        float               m_TimeScale;            //time scaling to use for this scene
        int                 m_FixedFrameMode;        //using fixed frame updates

    };


    //////////////////////////////////////////////////////////////////////////
    //This is our gameInstance class. The gameInstance contains several main 
    //	components which needs to be instanitated for the system to work correctly, this object resides
    //	both on the peer and host. The app controller implements the speicifc logic for the host and peer.
    //		(world\physics component::responsible for rendering, input, physics)
    //		(network component::responsible for creating/maintaining netconnections and netmngdects..)
    //The game instance is commonality code between the host/peer.
    //////////////////////////////////////////////////////////////////////////
    class gameInstance : public nsynMsgHandler
    {
    public:

        typedef	atMap<int,nsynBaseOld*>::Iterator						ObjectIterator;
        typedef atMap<int,nsynBaseOld*>::ConstIterator				ConstObjectIterator;

    public:

        gameInstance( bool isHost, bool collectNetStats = false);
        ~gameInstance( void );

        //Init functions for the world and physics components
        bool						InitWorldPhysics		( void );

        //Init functions for the network components
        bool						InitNetwork				( void );

        //Checks to see if netstats are enabled.
        bool						IsNetStatsEnabled		( void ){return m_CollectNetStats;}

        //State check for high level app to see if the game wants to exit
        bool						WantsToExit				( void );

        //Refresh function for the game
        void						Refresh					( void );

        //Cleansup all the necessary systems
        void						ShutDown				( void );

        //Resets the world
        void                        ResetWorld              ( void );

        //Notifciation of a world reset from demoworld
        void                        ResetWorldNotify        ( void );

        //Creates a scene given the setup information
        bool						CreateScene				( const netSceneObject& rSceneData );

        //Creates a demoworld on the host only
        bool						CreateDemoWorld			( void );

        //Creation function for physical object, if netId == -1, then then a unique ID is created..
        SimpleGameObj*              CreateObject		    ( const char* file, const Vector3& position, bool alignBottom=false, 
            const Vector3& rotation=ORIGIN,bool alwaysActive=false, bool startActive=true, bool registerObj=false);

        //Deletes an object from the world TODO(DDN)
        void						DeleteObject	        ( SimpleGameObj* object );

        //Deletes an object from the world TODO(DDN)
        void						DeleteObjectBynetID		( const netId& /*rNetID*/ ){}
        
        //Output netstats for the given connection
        void						OutputNetStats			( const int connectID );

        //Gets the network time manager
        nsynTimeManager*				GetNetTimeSync			( void ) {return m_NetObjMngr.GetNetTimeManager(); }

        //Gets the elaspsed time in the physics system..
        float						GetElapsedPhysicsTime	( void ){
            if (m_pPhySampleMngr==NULL || m_pPhySampleMngr->GetCurrentWorld() == NULL){
                return 0.f;
            }

            return m_pPhySampleMngr->GetCurrentWorld()->GetElapsedTime();
        }

        rage::nsynBase*          BindGameObjectToNet     ( rage::nsynObjId id );

        virtual EventReturnType     HandleEvent             ( const nsynEvtBase& event );

        SimpleGameObj*              GetSimpleGameObject     ( rage::nsynObjId id );
    
        void                        RegisterGameObject      ( SimpleGameObj* gameObj );

        void                        TakeControl             ( phInst* inst );

        bool                        TakeControlAlt          ( phInst* inst );

        bool                        RecordApplyForce        ( int levelIndex, const Vector3& force, const Vector3& position, int component=0);

        bool                        RecordApplyImpulse      ( int levelIndex, const Vector3& impulse, const Vector3& position, int component=0);

        void                        ReleaseOwnership        ( phInst* inst );
    
        void                        ResetObject             ( phInst* inst );

    public:	

        //////////////////////////////////////////////////////////////////////////
        //Interface for accessing the scene, object container and update 

        //Gets the scene data associated with the game
        const netSceneObject&		GetSceneData			( void ){return m_SceneData; }


    public:

        //////////////////////////////////////////////////////////////////////////
        //Interface for accessing various network functionality

        //Gets the Hoster associated with this game..
        rage::netHoster&			GetHoster				( void ){return m_Hoster; }

        //Gets the netConnectionManager
        rage::netConnectionManager& GetNetManager			( void ){return m_NetMgr; }

        //Get the MessageHandler
        rage::netConnectionManager::MessageHandler&     GetNetMessageHanlder ( void ) {return m_NetMsgHandler;}

        //Get the SyncManager
        rage::nsynHitchManager&						    GetSyncManager(void){
            return *m_NetObjMngr.GetNetSyncMngr(); /*yes ,this is bad but this is refacotred code..*/}

        rage::nsynObjectManager&                         GetNetObjManager(void){return m_NetObjMngr;}
        
        rage::nsynScopeId                                GetGameScopeLow ( void )
        {
            return m_GameScopeLow;
        }

        rage::nsynScopeId                                GetGameScopeHigh ( void )
        {
            return m_GameScopeHigh;
        }

    private:

        //Refreshes all the netMngds contained by the this game instance
        void						                    RefreshMngdObjects( void );

    private:

        typedef atPtr<SimpleGameObj>            SimpleGameObjSPtr;

    private:

        int                                             m_SyncObjectType;
        rage::netStats									m_NetStats;			//This will be used to collect network statistics.
        rage::netHoster									m_Hoster;			//??? need to ask Kevin..
        rage::netConnectionManager						m_NetMgr;			//net mngr is responsible for ???
        rage::netConnectionManager::MessageHandler		m_NetMsgHandler;	//net msg handler is responsible for ???
        ragesamples::physicsSampleManager*				m_pPhySampleMngr;	//physics sample manager..
        bool											m_Init;				//variable used to init the setup object
        bool											m_IsHost;			//flag to indicate if were the host.
        bool											m_RunPhySim;		//flag to indicate if we can run the physics system..
        bool											m_CollectNetStats;	//flag to indicate if we are collecting net stats
        bool											m_UseFakeLag;		//flag to turn on/off fake lag	
        bool                                            m_IsShutdown;
        bool                                            m_CanTakeControl;   
        netSceneObject									m_SceneData;		//data for setting up the current scene.
        ::rage::nsynObjectManager                        m_NetObjMngr;       //managages the net objects

        ::rage::nsynScopeId                              m_GameScopeLow;     //our game scope used to control the updates for objects
        ::rage::nsynScopeId                              m_GameScopeHigh;    //our game scope used to update high importance objects

        SimpleGameObjectNetFactory::SPtr                m_SimpleFactory;    //factory object for syncing simple game objects across the network
        rage::atMap<nsynObjIdType,SimpleGameObjSPtr>        m_GameObjects;      //all game objects indiex using their network ID
        rage::atSList<ragesamples::phDemoObject*>       m_DemoObjects;      //by nessecity we have to keep a cpy of this list to we can clear out the inst..

    private:	

        gameInstance ( const gameInstance& );
        gameInstance& operator=( const gameInstance& );
    };

    //////////////////////////////////////////////////////////////////////////
    //simple state machine class to manage the states of the peers, host and peer sessions 
    //////////////////////////////////////////////////////////////////////////
    class simpleStateMachine{
    public:

        simpleStateMachine(){ m_CurrentState = -1; }
        virtual ~simpleStateMachine(){ m_CurrentState = -1; }

        //Checks the validity of the state, subclass dependent
        virtual bool		IsValidStateEnum	( int state ) = 0;

        //Special state function for initialzing the state machine
        virtual void		OnStart				( void  ){}

        //Shutdown function
        virtual void		OnShutdown			( void  ){}

        //Handles the transition of states
        virtual void		OnTransition		( int newState ){

            if (!IsValidStateEnum(newState)){
                return;
            }

            if (newState == m_CurrentState){
                return;
            }

            OnExit(newState);

            int oldState = m_CurrentState;

            m_CurrentState = newState;

            OnEnter(oldState);
        }

        //Gets the current state of the machine
        int					GetCurrentState ( void ) {return m_CurrentState;}

    protected:

        //Called on exiting of the currentState
        virtual void		OnExit			( int /*NewState*/ ){}

        //Called upon entering of the currentState
        virtual void		OnEnter			( int /*OldState*/ ){}

        //Internal start function used by OnStart
        void				OnStartInternal	( int state  ){
            m_CurrentState = state;
            OnEnter(state);
        }

    private:	

        int					m_CurrentState;			//current state
    };

    //////////////////////////////////////////////////////////////////////////
    //this object encapsualtes the logic associated with a particualr app type (peer or host)
    //this also includes logic with state transitions and message mediation between peer and host..
    //////////////////////////////////////////////////////////////////////////
    class netAppController : public simpleStateMachine {
    public:

        netAppController(gameInstance* pGameInstance) : 
          simpleStateMachine(),  m_pGameInstance(pGameInstance), m_HostConnectionID(-1)
          {}

          virtual ~netAppController(){ m_pGameInstance = NULL; }

          virtual void        ResetWorld          ( void ){
              //no-op
          }

          //Checks the game if it wants to exit
          virtual bool		WantsToExit			( void ){
              if (GetGameInstance() == NULL){
                  return false;
              }

              return GetGameInstance()->WantsToExit();
          }

          //Shutdown functionality
          virtual void		OnShutdown			( void  ){
              if (GetGameInstance()){
                  GetGameInstance()->ShutDown();
              }

              m_HostConnectionID = -1;

              simpleStateMachine::OnShutdown();
          }

          //Gets the Host Address
          rage::netAddress	GetHostAddress		( void ) {
              if (GetGameInstance()){
                  return GetGameInstance()->GetHoster().GetHostInfo( 0 )->address;
              }
              rage::netAddress Empty;
              return Empty;
          }

          //Sends to the host, reliably.
          void				SendHostReliable	( const rage::netMessage& rMsg ){

              Assert(m_HostConnectionID!=-1);
              Assert(GetGameInstance()!=NULL);

              if (m_HostConnectionID==-1 || GetGameInstance() == NULL){
                  return;
              }

              GetGameInstance()->GetNetManager().QueueOutgoing(
                  m_HostConnectionID, rMsg, netMessage::SEND_RELIABLE, NULL );
          }

          //Sends to the host, unreliably.
          void				SendHostUnreliable	( const rage::netMessage& rMsg ){

              Assert(m_HostConnectionID!=-1);
              Assert(GetGameInstance()!=NULL);

              if (m_HostConnectionID==-1 || GetGameInstance() == NULL){
                  return;
              }

              GetGameInstance()->GetNetManager().QueueOutgoing(
                  m_HostConnectionID, rMsg, 0, NULL );
          }

          //Sends to a target, reliably.
          void				SendToReliable( const int ConnectID, const rage::netMessage& rMsg ){

              Assert(ConnectID!=-1);
              Assert(GetGameInstance()!=NULL);

              if (ConnectID==-1 || GetGameInstance() == NULL){
                  return;
              }

              GetGameInstance()->GetNetManager().QueueOutgoing(
                  ConnectID, rMsg, netMessage::SEND_RELIABLE, NULL );
          }

          //Sends to a target, unreliably.
          void				    SendToUnreliable( const int ConnectID, const rage::netMessage& rMsg ){

              Assert(ConnectID!=-1);
              Assert(GetGameInstance()!=NULL);

              if (ConnectID==-1 || GetGameInstance() == NULL){
                  return;
              }

              GetGameInstance()->GetNetManager().QueueOutgoing(
                  ConnectID, rMsg, 0, NULL );
          }

          //Refresh function called every update
          virtual void		    Refresh			( void ) = 0;

          //Gets the current game instance associated with this APP
          gameInstance*		    GetGameInstance ( void ) {return m_pGameInstance;}

          //Gets the current net time sync object
          nsynTimeManager*		GetNetTimeSync	( void ) {
              if (GetGameInstance()==NULL){
                  return NULL;
              }

              return GetGameInstance()->GetNetTimeSync();
          }

          //Is true if this net app is slave to an external timmer
          virtual bool		IsRemoteTimePeer		( void ){return true;}

          //Returns the remote time soruce connection ID
          virtual int	    GetTimeSourceConnectID	( void ){return m_HostConnectionID;}

          float				GetElapsedPhysicsTime	( void ){
              if (m_pGameInstance==NULL){
                  return 0.f;
              }

              return m_pGameInstance->GetElapsedPhysicsTime();
          }

    protected:

        //Set the host connection ID
        void				SetHostConnectionID		( int ConnID ){ m_HostConnectionID = ConnID; }

        //Get the host connection ID
        int					GetHostConnectionID		( void ){return m_HostConnectionID;}

    private:	

        gameInstance*		m_pGameInstance;					//the game object itself..
        int					m_HostConnectionID;					//connection ID of the host, if we are not one..

    };

}   //namespace ragesamples

extern ragenetsamples::netAppController*    GetNetApp();
extern ragenetsamples::gameInstance*        GetGame();

#endif  //NETSUPPORT_COMMON_H
