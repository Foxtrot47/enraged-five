// 
// sample_net_physics/netsupport_peer.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#include "netsupport_peer.h"

using namespace ragenetsamples;

// TITLE: Client
// PURPOSE:
//		Client side logic for a simple server-client based game.

//  STEP #1. Setup the client statemachine.

//entering the m_CurrentAppState
void        peerAppController::OnEnter         ( int /*oldState*/ ){

    //Odd a null game instance...
    if (GetGameInstance()==NULL){
        Assert(GetGameInstance()!=NULL);
        return;
    }

    switch(GetCurrentState()){
        //--
        //Like the host, most of the client logic resides in its state transition function the OnEnter.
        //<P><I>Startup</I><P>
            case APP_PEER_STARTING:
                //do our init stuff here
                //transition to startup the network
                OnTransition(APP_PEER_NET_START);

#if __DEV
                m_JoinPolicies.SetKeepAliveInterval( 0 );
                m_JoinPolicies.SetQoSInterval( 0 );
                m_JoinPolicies.SetConnectTimeout( 10*1000 );
#endif
                m_JoinPolicies.SetMaxRetries( 100 );
                m_JoinPolicies.SetRetransTimeout( 1000 );

#if __DEV
                m_NormalPolicies.SetKeepAliveInterval( 0 );
                m_NormalPolicies.SetQoSInterval( 0 );
                m_NormalPolicies.SetConnectTimeout( 10*1000 );
#endif
                m_NormalPolicies.SetMaxRetries( 100 );

                m_StatsTimmer.Start(1.f);

                break;
                //--
                //<I>World Startup</I><P>Note this state is empty because the world initialization is purely driven by
                //the host.
            case APP_PEER_WORLD_START:
                break;
                //--
                //<I>Init Network</I><P>
            case APP_PEER_NET_START:
                //--the peer is different from the host in that they startup the network first
                //and then once they finish connecting, they init the world..
                if (GetGameInstance()->InitNetwork()){
                    //start looking for a host
                    GetGameInstance()->GetHoster().StartQuery();

                    // Bind the callback function for processing the net messages
                    GetGameInstance()->GetNetMessageHanlder().Reset< peerAppController, 
                        &peerAppController::HandleMessage >( this );

                    //go to looking state.
                    OnTransition(APP_PEER_LOOKING_HOST);
                } else {
                    OnTransition(APP_PEER_SHUTDOWN);
                }
                break;
                //--
                //<I>Looking for host</I><P>
            case APP_PEER_LOOKING_HOST:
                //wait to find a host to connect too..
                break;
                //--
                //<I>Connecting</I><P>
            case APP_PEER_CONNECTING:
                //found a host were connecting now.. 
                //the host protocol message will drive our state now..
                GetGameInstance()->GetNetManager().SetPolicies(m_JoinPolicies);
                break;
                //--
                //<I>Synching to host</I><P>
            case APP_PEER_SYNCING:
                //--we have received a reply from the host to start synchronizing into a existing
                //scene.. the steps are :<P>
                //Init the world<P>
                if (GetGameInstance()->InitWorldPhysics()){
                    SendHostReliable( netMessage ( AppProtocolMsg ( AppProtocolMsg::FH_PEER_FINISH_CREATE_SCENE)));
                } else {
                    OnTransition(APP_PEER_SHUTDOWN);
                }
                //Create the scene<P>
                //Create the objects<P>
                //Reply to host upon finishing of object/scene creation<P>
                //Host message then will transition us to APP_PEER_RUNNING<P>
                break;
                //--
                //<I>Stable state</I><P>
            case APP_PEER_RUNNING:
                GetGameInstance()->GetNetManager().SetPolicies(m_NormalPolicies);
                break;
                //--
                //<I>Shutdown</I><P>
            case APP_PEER_SHUTDOWN:
                SendHostReliable( netMessage( AppProtocolMsg( AppProtocolMsg::FH_CLIENT_SHUTDOWN )));
                break;
                //-STOP
    }
}

//  STEP #2. Syncing with host.

// Callback that handles messages dispatched from the connection manager.
unsigned peerAppController::HandleMessage( const netAddress& , const int connectionId, const netMessage& msg ){
    unsigned retval = 0;

    if (connectionId < 0 || GetGameInstance() == NULL){
        return retval;
    }


    //////////////////////////////////////////////////////////////////////////
    //Handle protocol messages

    //--
    //Upon successfully connecting to a host, we will start receiving AppProtocolMsg which will coordinate
    //the communication between the host and client.

    //Protocol message are used for peers and host to talk to each other.
    if( msg.GetId() == AppProtocolMsg::GetMessageId() )
    {
        //Retrieve the message data from the message
        AppProtocolMsg RealMsg;

        msg.Deserialize( &RealMsg );

        Assert(RealMsg.m_ForClient);

        if (RealMsg.m_ForClient){
            //--
            //The message protocol between the client and host is encapsulated within this switch statement.
            switch (RealMsg.m_ProtocolEnum){
                        case AppProtocolMsg::FP_ACKNOWLEDGE_CONNECT: 
                            {
                                OnTransition(APP_PEER_CONNECTING);
                                nsynConnectionId conn = (nsynConnectionId)connectionId;
                                GetGameInstance()->GetNetObjManager().OnPeerConnect( conn );
                            }
                            break;
                        case AppProtocolMsg::FP_START_SCENE_DOWNLOAD: 
                            OnTransition(APP_PEER_SYNCING);
                            break;
                        case AppProtocolMsg::FP_SCENE_DOWNLOAD_FINISH: 
                            SendHostReliable(
                                netMessage( AppProtocolMsg( AppProtocolMsg::FH_PEER_FINISH_CREATE_SCENE )));
                            break;
                        case AppProtocolMsg::FP_START_OBJECT_DOWNLOAD: 
                            //no-op
                            break;
                        case AppProtocolMsg::FP_OBJECT_DOWNLOAD_FINISH: 
                            //                            GetGameInstance()->SlaveAllObjects(GetHostConnectionID());
                            SendHostReliable(
                                netMessage( AppProtocolMsg( AppProtocolMsg::FH_PEER_FINISH_CREATE_OBJECTS ) ) );
                            break;
                        case AppProtocolMsg::FP_START_ENTER_GAME: 
                            OnTransition(APP_PEER_RUNNING);
                            break;
                        case AppProtocolMsg::FP_RESET_WORLD:
                            GetGameInstance()->ResetWorld();
                            break;
                        case AppProtocolMsg::FP_HOST_SHUTDOWN:
                            m_ForceExit = true;
                            break;
            }
            //-STOP

            retval |= K_RTVAL_HANDLED;
        }
    }

    //////////////////////////////////////////////////////////////////////////
    //Handle scene create message

    //--
    //Once we're connected, we are sent a message about the current scene, which we use to instigate our world
    if( msg.GetId() == CreateSceneMsg::GetMessageId() )
    {
        //Retrieve the message data from the message
        CreateSceneMsg RealMsg;

        msg.Deserialize( &RealMsg );

        netSceneObject SceneData;

        RealMsg.Set(&SceneData);

        GetGameInstance()->CreateScene(SceneData);

        retval |= K_RTVAL_HANDLED;
    }
   
    //--
    //Once we're connected, game objects which need to be created are sent through CreateObjectMsg message. 

    //////////////////////////////////////////////////////////////////////////
    //Handle object create message
    if( msg.GetId() == CreateObjectMsg::GetMessageId() )
    {
        //Retrieve the message data from the message
        CreateObjectMsg RealMsg;

        msg.Deserialize( &RealMsg );

        GetGameInstance()->CreateObject(
            RealMsg.m_ObjectType, RealMsg.m_Position, RealMsg.m_AlignBottom,
            RealMsg.m_Rotation, RealMsg.m_AlwaysActive, RealMsg.m_StartActive );

        retval |= K_RTVAL_HANDLED;
    }
    
    //--
    //Once we've connected, game objects are deleted using the DeleteObjectMsg message.

    //////////////////////////////////////////////////////////////////////////
    //Handle object delete message
    if( msg.GetId() == DeleteObjectMsg::GetMessageId() )
    {
        //Retrieve the message data from the message
        DeleteObjectMsg RealMsg;

        msg.Deserialize( &RealMsg );

        netId NetID;
        NetID.m_ID = RealMsg.m_NetID;

        GetGameInstance()->DeleteObjectBynetID( NetID );

        retval |= K_RTVAL_HANDLED;
    }

    //-STOP

    return retval;
}
