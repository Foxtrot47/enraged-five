// 
// sample_net_physics/netsupport_host.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_NET_PHYSICS_NETSUPPORT_HOST_H
#define SAMPLE_NET_PHYSICS_NETSUPPORT_HOST_H

#include "netsupport_common.h"

#include "atl/array.h"

namespace ragenetsamples
{
    class peerSession;

    /////////////////////////
    /////////////////////////////////////////////////
    //hostAppController: class used to manage the state\logic of the host
    //////////////////////////////////////////////////////////////////////////
    class hostAppController : public netAppController {
    public:

        static const float K_STATS_UPDATE_HOST_RATE;

        enum APPSTATE_HOST{
            APP_HOST_STARTING,              //inital state, temporary, used to setup stuff
            APP_HOST_WORLD_START,           //state which init the world/physics system
            APP_HOST_NET_START,             //state which init the network components
            APP_HOST_RUNNING,               //state after everythign is inited
            APP_HOST_SHUTDOWN               //shutdown state, do cleanup here.
        };

        hostAppController(gameInstance* pGameInstance) : netAppController(pGameInstance), m_NumJoiners(0) {}
        ~hostAppController();

        virtual void        Refresh                 ( void );

        virtual void        ResetWorld              ( void ){
            SendAllPeersReliable( rage::netMessage( AppProtocolMsg( AppProtocolMsg::FP_RESET_WORLD )) );
        }

        // Callback that handles messages dispatched from the connection manager.
        unsigned            HandleMessage           ( const netAddress& sender, const int connectionId, const netMessage& msg );
        void                SendSceneToPeer         ( const int connID );
        void                SendObjectsToPeer       ( const int connID );
        peerSession*        FindSessionByConnection ( const int connID );

        virtual bool        IsValidStateEnum    ( int state ){
            if (state < APP_HOST_STARTING || state > APP_HOST_SHUTDOWN){
                return false;
            }

            return true;
        }

        virtual void        OnStart             ( void  ){OnStartInternal(APP_HOST_STARTING);}

        virtual void        OnShutdown          ( void  ){
            SendAllPeersReliable( rage::netMessage( AppProtocolMsg( AppProtocolMsg::FP_HOST_SHUTDOWN )) );
            netAppController::OnShutdown();
        }

        //Is true if this net app is slave to an external timmer
        virtual bool        IsRemoteTimePeer        ( void ){return false;}

        //Returns the remote time soruce connection ID
        virtual int         GetTimeSourceConnectID  ( void ){return -1;}

    protected:

        //entering the m_CurrentAppState
        virtual void        OnEnter             ( int /*oldState*/ );

        void                UnBindConnection        (int connectionID);

        void                SendAllPeersReliable    (const netMessage& rMsg);

    private:

        TimeHelperObject                    m_StatsTimmer;              //timmer for stats 
        rage::atArray<peerSession*>         m_SessionContainer;         //active sessions for this host
        netPolicies                         m_NormalPolicies;           //use this policy normally
        netPolicies                         m_JoinPolicies;             //use this policy when we have joining player
        int                                 m_NumJoiners;               //number of active joiners
    };

    //////////////////////////////////////////////////////////////////////////
    //PeerSession : state and logic associated with a single peer on the host side
    //////////////////////////////////////////////////////////////////////////
    class peerSession : public simpleStateMachine {
    public:

        enum APPSTATE_PEER_SESSION{
            APP_SESSION_STARTING,
            APP_SESSION_CONNECTED,
            APP_SESSION_DOWNLOADING,
            APP_SESSION_DOWNLOADING_OBJECT,
            APP_SESSION_IN_GAME,
            APP_SESSION_EXITING,
        };

        peerSession(const int connectID, hostAppController* pHostApp) :
            simpleStateMachine (), m_ConnectionID(connectID), m_pHostApp(pHostApp) {
                Assert(connectID>=0);
                Assert(pHostApp!=NULL);
            }

            virtual ~peerSession(){}

            virtual void        Refresh             ( void ){
                switch(GetCurrentState()){
                case APP_SESSION_STARTING: 
                case APP_SESSION_CONNECTED: 
                case APP_SESSION_DOWNLOADING: 
                case APP_SESSION_IN_GAME: 
                case APP_SESSION_EXITING: 
                    break;
                }
            }

            virtual bool        IsValidStateEnum    ( int state ){
                if (state < APP_SESSION_STARTING || state > APP_SESSION_EXITING){
                    return false;
                }

                return true;
            }

            virtual void    OnStart             ( void ){OnStartInternal(APP_SESSION_STARTING);}
            void            EnterGame           ( void ){OnTransition(APP_SESSION_IN_GAME);}
            void            StartObjectDownload ( void ){OnTransition(APP_SESSION_DOWNLOADING_OBJECT);} 
            int             GetConnectionID     ( void ){return m_ConnectionID;}

    protected:

        //entering the m_CurrentAppState
        virtual void        OnEnter             ( int /*oldState*/ );

    private:

        int                 m_ConnectionID;             //the connection ID for this peer
        hostAppController*  m_pHostApp;                 //the app controller assocaited with this peer
    };

}   //namespace ragesamples

#endif //NETSUPPORT_HOST_H
