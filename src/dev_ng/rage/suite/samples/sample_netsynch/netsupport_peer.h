// 
// sample_net_physics/netsupport_peer.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_NET_PHYSICS_NETSUPPORT_PEER_H
#define SAMPLE_NET_PHYSICS_NETSUPPORT_PEER_H

#include "netsupport_common.h"

#include "netsynch/manager.h"

namespace ragenetsamples
{
    //////////////////////////////////////////////////////////////////////////
    //peerAppController: class used to manage the state\logic of the peer
    //////////////////////////////////////////////////////////////////////////
    class peerAppController : public netAppController {
    public:

        enum APPSTATE_PEER{
            APP_PEER_STARTING,          //Initial transitory state, used to setup things, immediately transitions to APP_PEER_NET_START
            APP_PEER_WORLD_START,       //After we've connected to the host successfully, we init our world object
            APP_PEER_NET_START,         //Initalize the net subsystem so we can start looking for a host
            APP_PEER_LOOKING_HOST,      //After we have initialized the net subsystem, we go into this state to look for a host
            APP_PEER_CONNECTING,        //After we've found a host, we enter this state
            APP_PEER_SYNCING,           //After the host has acknowledged our connection and begins the synchronization phase we enter this state
            APP_PEER_RUNNING,           //After we have completed session synchronization we enter this state and is essentially in the simulation
            APP_PEER_SHUTDOWN           //Exit state, shuts down subsystems and sends off farewell msg to host.
        };

        peerAppController(gameInstance* pGameInstance) : netAppController(pGameInstance), m_ForceExit(false) {}
        ~peerAppController(){}
        
        virtual bool        WantsToExit         ( void ){
            if (m_ForceExit){
                return true;
            }
            return netAppController::WantsToExit();
        }

        virtual void        Refresh             ( void ){
            switch(GetCurrentState()){
             case APP_PEER_STARTING: 
             case APP_PEER_NET_START: 
             case APP_PEER_WORLD_START:
                 break;

             case APP_PEER_LOOKING_HOST: 
                 if (GetGameInstance()){
                     if (GetGameInstance()->GetHoster().GetHostCount()!=0){
                         //once we've found a host we can stop looking.
                         GetGameInstance()->GetHoster().StopQuery();

                         //send off connect request message..
                         int connID = GetGameInstance()->GetNetManager().RequestConnection( 
                             GetGameInstance()->GetHoster().GetHostInfo( 0 )->address, netMessage( RequestConnectMsg() ) );

                         SetHostConnectionID(connID);

                         //transition to connecting state.. I wonder if we should timeout if we can't connect
                         //in a reasonable time??
                         OnTransition(APP_PEER_CONNECTING);
                     } else {
                         GetGameInstance()->GetHoster().Update();
                         GetGameInstance()->GetNetManager().Update();
                     }
                 }
                 break;

             case APP_PEER_CONNECTING: 
             case APP_PEER_SYNCING: 
             case APP_PEER_RUNNING: 
                 if (GetGameInstance() && GetNetTimeSync()){
                     GetGameInstance()->Refresh();
                     if (m_StatsTimmer.Check())
                     {
                        GetGameInstance()->OutputNetStats(GetHostConnectionID());
                        m_StatsTimmer.Next(1.f);
                     }
                     
                 }
                 break;
             
             case APP_PEER_SHUTDOWN: 
                 break;
         }
        }

        virtual bool        IsValidStateEnum    ( int state ){
            if (state < APP_PEER_STARTING || state > APP_PEER_SHUTDOWN){
                return false;
         }

            return true;
        }

        virtual void        OnStart             ( void  ){OnStartInternal(APP_PEER_STARTING);}

        // Callback that handles messages dispatched from the connection manager.
        unsigned HandleMessage( const netAddress& , const int connectionId, const netMessage& msg );
        
        virtual void        OnShutdown          ( void  ){
            if (!m_ForceExit)
            {
                SendHostReliable( rage::netMessage( AppProtocolMsg( AppProtocolMsg::FH_CLIENT_SHUTDOWN )) );
            }
            netAppController::OnShutdown();
        }

    protected:

        //entering the m_CurrentAppState
        virtual void        OnEnter         ( int /*oldState*/ );

        private:
            TimeHelperObject                    m_StatsTimmer;              //timmer for stats 
            bool                                m_ForceExit;                //flag to force the client to shutdown
            netPolicies                         m_NormalPolicies;           //use this policy normally use when the game is running
            netPolicies                         m_JoinPolicies;             //use this policy when we have joining player( more lax on resends, etc.. )
    };

}   //namespace ragesamples

#endif  //NETSUPPORT_PEER_H
