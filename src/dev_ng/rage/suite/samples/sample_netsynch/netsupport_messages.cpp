// 
// sample_net_physics/netsupport_messages.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "netsupport_messages.h"
#include "netsupport_common.h"

using namespace ragenetsamples;
using namespace ragesamples;
using namespace rage;

NET_MESSAGE_IMPL( RequestConnectMsg );
NET_MESSAGE_IMPL( AppProtocolMsg );
NET_MESSAGE_IMPL( CreateSceneMsg );
NET_MESSAGE_IMPL( CreateObjectMsg );
NET_MESSAGE_IMPL( DeleteObjectMsg );

//////////////////////////////////////////////////////////////////////////

CreateSceneMsg::CreateSceneMsg(const netSceneObject& rScene){
    m_FixedFrameMode    = rScene.m_FixedFrameMode;
    m_MaxActiveObjects  = rScene.m_MaxActiveObjects;
    m_MaxBehaviors      = rScene.m_MaxBehaviors;
    m_MaxObjects        = rScene.m_MaxObjects;
    m_SyncObjectType    = rScene.m_SyncObjectType;
    m_MaxOctreeNodes    = rScene.m_MaxOctreeNodes;
    m_Position          = rScene.m_Position;
    m_SceneName         = rScene.m_SceneName;
    m_WorldMax          = rScene.m_WorldMax;
    m_WorldMin          = rScene.m_WorldMin;
    m_WorldOffset       = rScene.m_WorldOffset;
    m_SimulatePhysics   = rScene.m_SimulatePhysics;
    m_TimeScale         = rScene.m_TimeScale;
}

void CreateSceneMsg::Set( netSceneObject* pScene ){
    pScene->m_FixedFrameMode    = m_FixedFrameMode;
    pScene->m_MaxActiveObjects  = m_MaxActiveObjects;
    pScene->m_MaxBehaviors      = m_MaxBehaviors;
    pScene->m_MaxObjects        = m_MaxObjects;
    pScene->m_SyncObjectType    = m_SyncObjectType;
    pScene->m_MaxOctreeNodes    = m_MaxOctreeNodes;
    pScene->m_Position          = m_Position;
    pScene->m_SceneName         = m_SceneName;
    pScene->m_WorldMax          = m_WorldMax;
    pScene->m_WorldMin          = m_WorldMin;
    pScene->m_WorldOffset       = m_WorldOffset;
    pScene->m_SimulatePhysics   = m_SimulatePhysics;
    pScene->m_TimeScale         = m_TimeScale;
}

//////////////////////////////////////////////////////////////////////////

CreateObjectMsg::CreateObjectMsg(const nsynBaseOld& rObject){
    m_ObjectType    = rObject.m_ObjectType; 
    m_Position      = rObject.m_Position;       
    m_Rotation      = rObject.m_Rotation;       
    m_NetID         = rObject.m_NetID.m_ID;     
    m_AlignBottom   = rObject.m_AlignBottom;    
    m_AlwaysActive  = rObject.m_AlwaysActive;   
    m_StartActive   = rObject.m_StartActive;    
    m_MngdObjectType = rObject.m_MngdObjectType;
}

