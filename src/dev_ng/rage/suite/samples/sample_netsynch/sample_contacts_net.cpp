//
// sample_physics\sample_contacts.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//


#include "math/random.h"
#include "phcore/config.h"
#include "physics/simulator.h"
#include "system/main.h"
#include "system/param.h"

#include "sample_physics/demoobject.h"
#include "sample_physics/sample_physics.h"
#include "netsupport_common.h"
#include "netsupport_host.h"
#include "netsupport_peer.h"

using namespace ragenetsamples;
using namespace ragesamples;
using namespace rage;


//PARAM(world, "The demo world to initialize when the demo boots");
PARAM(file, "Reserved for future expansion, not currently hooked up");
PARAM(peerType, "\"guest\" to run as guest, \"host\" to run as host." );
    
#define DEFAULT_PEER_TYPE  "host"

#define ENABLE_NET_STATS	1

namespace rage {
	EXT_PFD_DECLARE_GROUP(Physics);
	EXT_PFD_DECLARE_GROUP(Bounds);
	EXT_PFD_DECLARE_ITEM(Active);
	EXT_PFD_DECLARE_ITEM(Inactive);
	EXT_PFD_DECLARE_ITEM(Fixed);
}

namespace {
    ragenetsamples::netAppController*  gNetApp = NULL;
}

ragenetsamples::netAppController*  GetNetApp(void){
    return gNetApp;
}

// main application
int Main()
{	
    //determine if were running as host or peer
	const char* peerType = PARAM_peerType.GetByName( "peerType", DEFAULT_PEER_TYPE );

	peerType = peerType ? peerType : DEFAULT_PEER_TYPE;

	const bool isHost = ( 0 == ::strcmp( "host", peerType ) );

	//instantiate the appropriate app controller for our type
	netAppController*	pAppController = NULL;
	atPtr<gameInstance> Game = rage_new gameInstance(isHost,ENABLE_NET_STATS);

	if (isHost){
		pAppController = rage_new hostAppController(Game);
	} else {
		pAppController = rage_new peerAppController(Game);	
	}

    gNetApp = pAppController;
 
	// The first state is special as it has no previous vaild state
	//to get around this problem we can OnStart.. instead of the ususal
	//OnTransition function..
	pAppController->OnStart();

	// Run the game until the app wants to exit.
	while(!pAppController->WantsToExit()){
		pAppController->Refresh();
	}

	// Shutdown all the objects
	pAppController->OnShutdown();

	// Dont forget to delete the app controller..
	delete pAppController;

    Game    = NULL;
    gNetApp = NULL;

	return 0;
}
