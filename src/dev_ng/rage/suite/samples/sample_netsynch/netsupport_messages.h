// 
// sample_net_physics/netsupport_messages.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_NET_PHYSICS_NETSUPPORT_MESSAGES_H
#define SAMPLE_NET_PHYSICS_NETSUPPORT_MESSAGES_H

#include "atl/string.h"
#include "net/net.h"
#include "net/netMessage.h"
#include "net/netpacket.h"
#include "netsynch/object.h"
#include "string/string.h"
#include "system/nelem.h"
#include "system/timemgr.h"
#include "vector/vector3.h"

#include "sample_physics/demoobject.h"
#include "sample_physics/sample_physics.h"

namespace ragenetsamples
{
    enum {
        MAX_NET_STRING_SIZE = 256,
    };

    class netSceneObject;
    class nsynBaseOld;

    
    //////////////////////////////////////////////////////////////////////////
    //MsgHelper : small helper class to handle read/write of composiste types..
    //////////////////////////////////////////////////////////////////////////
    struct MsgHelper{
        static void ReadVector3     ( rage::Vector3& rVector, const rage::netMessage& rMsg ){
            rMsg.ReadFloat(&rVector.x);
            rMsg.ReadFloat(&rVector.y);
            rMsg.ReadFloat(&rVector.z);
        }
        static void WriteVector3    ( const rage::Vector3& rVector, rage::netMessage& rMsg ){
            rMsg.WriteFloat(rVector.x);
            rMsg.WriteFloat(rVector.y);
            rMsg.WriteFloat(rVector.z);
        }

        static void ReadMatrix34    ( rage::Matrix34& rMatrix, const rage::netMessage& rMsg ){
            MsgHelper::ReadVector3(rMatrix.GetVector(0),rMsg);
            MsgHelper::ReadVector3(rMatrix.GetVector(1),rMsg);
            MsgHelper::ReadVector3(rMatrix.GetVector(2),rMsg);
            MsgHelper::ReadVector3(rMatrix.GetVector(3),rMsg);
        }

        static void WriteMatrix34   ( const rage::Matrix34& rMatrix, rage::netMessage& rMsg ){
            MsgHelper::WriteVector3(rMatrix.GetVector(0),rMsg);
            MsgHelper::WriteVector3(rMatrix.GetVector(1),rMsg);
            MsgHelper::WriteVector3(rMatrix.GetVector(2),rMsg);
            MsgHelper::WriteVector3(rMatrix.GetVector(3),rMsg);
        }
        
        static void ReadVector3     ( rage::Vector3& rVector, const rage::netBitBuffer& rData ){
            rData.ReadFloat(&rVector.x);
            rData.ReadFloat(&rVector.y);
            rData.ReadFloat(&rVector.z);
        }
        static void WriteVector3    ( const rage::Vector3& rVector, rage::netBitBuffer& rData ){
            rData.WriteFloat(rVector.x);
            rData.WriteFloat(rVector.y);
            rData.WriteFloat(rVector.z);
        }
        
        static void ReadQuaternion  ( rage::Quaternion& rQuat, const rage::netBitBuffer& rData ){
            rData.ReadFloat(&rQuat.x);
            rData.ReadFloat(&rQuat.y);
            rData.ReadFloat(&rQuat.z);
            rData.ReadFloat(&rQuat.w);
        }
        static void WriteQuaternion ( const rage::Quaternion& rQuat, rage::netBitBuffer& rData ){
            rData.WriteFloat(rQuat.x);
            rData.WriteFloat(rQuat.y);
            rData.WriteFloat(rQuat.z);
            rData.WriteFloat(rQuat.w);
        }

        static void ReadMatrix34    ( rage::Matrix34& rMatrix, const rage::netBitBuffer& rData ){
            MsgHelper::ReadVector3(rMatrix.GetVector(0),rData);
            MsgHelper::ReadVector3(rMatrix.GetVector(1),rData);
            MsgHelper::ReadVector3(rMatrix.GetVector(2),rData);
            MsgHelper::ReadVector3(rMatrix.GetVector(3),rData);
        }

        static void WriteMatrix34   ( const rage::Matrix34& rMatrix, rage::netBitBuffer& rData ){
            MsgHelper::WriteVector3(rMatrix.GetVector(0),rData);
            MsgHelper::WriteVector3(rMatrix.GetVector(1),rData);
            MsgHelper::WriteVector3(rMatrix.GetVector(2),rData);
            MsgHelper::WriteVector3(rMatrix.GetVector(3),rData);
        }

        static void ReadString ( rage::atString& rString, const rage::netMessage& rMsg ){
            char BufferTmp[MAX_NET_STRING_SIZE];
            rMsg.ReadStr(BufferTmp,MAX_NET_STRING_SIZE);
            rString = BufferTmp;
        }

        static void WriteString ( const rage::atString& rString, rage::netMessage& rMsg ){
            char BufferTmp[MAX_NET_STRING_SIZE];

            if (rString.GetLength() > MAX_NET_STRING_SIZE){
                Assertf(0,"tried to pass in string longer than tmp buffer size %d", MAX_NET_STRING_SIZE);
                rMsg.WriteStr("Error",5);
                return;
            }

            memcpy(BufferTmp,(const char*)rString,rString.GetLength());

            BufferTmp[rString.GetLength()] = '\0';

            rMsg.WriteStr(BufferTmp,MAX_NET_STRING_SIZE);
        }
    };

    //////////////////////////////////////////////////////////////////////////
    //netTimeMsg : msg used by the netTimeObject to sync clocks 
    //////////////////////////////////////////////////////////////////////////
    struct netTime {
        netTime(rage::u32 time){ m_Time = time; }

        netTime (void)
        {
            Clear();
        }

        void    Clear       (void)
        {
            m_Time = 0;
        }

        void    Decrement   (rage::u32 val)
        {
            m_Time -= val;
        }

        int     CalculateDifference  (netTime& rhs)
        {
            if (rhs.m_Time > m_Time){
                return 0;
            }

            return m_Time-rhs.m_Time;
        }

        bool operator < ( const netTime& rhs ) const 
        {
            return m_Time < rhs.m_Time;
        }

        bool operator == ( const netTime& rhs ) const 
        {
            return m_Time == rhs.m_Time;
        }

        bool operator > ( const netTime& rhs ) const 
        {
            return !(*this < rhs);
        }

        void Read( const rage::netMessage& msg )
        {
            msg.Read(&m_Time);
        }

        void Write( rage::netMessage* msg ) const
        {
            msg->Write(m_Time);
        }

        u32 m_Time; //raw time value
    };

    //////////////////////////////////////////////////////////////////////////
    //RequestConnectMsg : inital message sent to host to request a connect
    //////////////////////////////////////////////////////////////////////////
    class RequestConnectMsg
    {
        NET_MESSAGE_DECL( RequestConnectMsg );

        void Read( const rage::netMessage& /*msg*/ ){}
        void Write( rage::netMessage* /*msg*/ ) const{}
    };

    //////////////////////////////////////////////////////////////////////////
    //AppProtocolMsg : generic 2 way communcation message used by host-peer
    //////////////////////////////////////////////////////////////////////////
    struct AppProtocolMsg
    {
        NET_MESSAGE_DECL( AppProtocolMsg );

        enum FOR_PEER_ENUMS
        {
            FP_ACKNOWLEDGE_CONNECT,
            FP_START_SCENE_DOWNLOAD,
            FP_SCENE_DOWNLOAD_FINISH,
            FP_START_OBJECT_DOWNLOAD,
            FP_OBJECT_DOWNLOAD_FINISH,
            FP_START_ENTER_GAME,
            FP_RESET_WORLD,
            FP_HOST_SHUTDOWN,
        };

        enum FOR_HOST_ENUMS
        {
            FH_PEER_FINISH_CREATE_SCENE,
            FH_PEER_FINISH_CREATE_OBJECTS,
            FH_CLIENT_SHUTDOWN
        };
        
        AppProtocolMsg(FOR_PEER_ENUMS msgEnum){
            m_ForClient = true;
            m_ProtocolEnum = (rage::s32) msgEnum;
        }
        
        AppProtocolMsg(FOR_HOST_ENUMS msgEnum){
            m_ForClient = false;
            m_ProtocolEnum = (rage::s32) msgEnum;
        }

        AppProtocolMsg(void){
            m_ForClient = false;
            m_ProtocolEnum = -1;
        }

        void Read( const rage::netMessage& msg )
        {
            msg.Read( &m_ProtocolEnum );
            msg.ReadBool( &m_ForClient );
        }

        void Write( rage::netMessage* msg ) const
        {
            msg->Write( m_ProtocolEnum );
            msg->WriteBool( m_ForClient );
        }

        rage::s32       m_ProtocolEnum;         //the message enum (this is a bidirectional msg which is used for host-peer to talk to each other)
        bool            m_ForClient;            //flag to denote if this message is carrying a peer or host enum
    };

    //////////////////////////////////////////////////////////////////////////
    //CreateSceneMsg : inital message sent to host to request a connect
    //TODO(DDN):switch over to fixed size buffers for string 
    //////////////////////////////////////////////////////////////////////////
    class CreateSceneMsg
    {
        NET_MESSAGE_DECL( CreateSceneMsg );

        CreateSceneMsg(){}

        CreateSceneMsg(const netSceneObject& rScene);

        void Set( netSceneObject* pScene );

        void Read( const rage::netMessage& msg )
        {
            msg.ReadBool(&m_SimulatePhysics);
            msg.ReadInt(&m_FixedFrameMode,32);         
            msg.ReadFloat(&m_TimeScale);

            MsgHelper::ReadVector3(m_WorldOffset,msg);
            MsgHelper::ReadVector3(m_WorldMin,msg);
            MsgHelper::ReadVector3(m_WorldMax,msg);
            MsgHelper::ReadVector3(m_Position,msg);

            msg.Read(&m_SyncObjectType);
            msg.Read(&m_MaxOctreeNodes);
            msg.Read(&m_MaxActiveObjects);
            msg.Read(&m_MaxObjects);
            msg.Read(&m_MaxBehaviors);

            MsgHelper::ReadString(m_SceneName,msg);
        }

        void Write( rage::netMessage* msg ) const
        {
            msg->WriteBool(m_SimulatePhysics);
            msg->WriteInt(m_FixedFrameMode,32);
            msg->WriteFloat(m_TimeScale);

            MsgHelper::WriteVector3(m_WorldOffset,*msg);
            MsgHelper::WriteVector3(m_WorldMin,*msg);
            MsgHelper::WriteVector3(m_WorldMax,*msg);
            MsgHelper::WriteVector3(m_Position,*msg);

            msg->Write(m_SyncObjectType);
            msg->Write(m_MaxOctreeNodes);
            msg->Write(m_MaxActiveObjects);
            msg->Write(m_MaxObjects);
            msg->Write(m_MaxBehaviors);

            MsgHelper::WriteString(m_SceneName,*msg);
        }

        float               m_TimeScale;
        bool                m_SimulatePhysics;
        int                 m_FixedFrameMode;
        rage::Vector3       m_WorldOffset;
        rage::Vector3       m_WorldMin;
        rage::Vector3       m_WorldMax;
        rage::Vector3       m_Position;

        rage::s32           m_SyncObjectType;
        rage::s32           m_MaxOctreeNodes;
        rage::s32           m_MaxActiveObjects;
        rage::s32           m_MaxObjects;
        rage::s32           m_MaxBehaviors;

        rage::atString      m_SceneName;
    };

    //////////////////////////////////////////////////////////////////////////
    //CreateObjectMsg : inital message sent to host to request a connect
    //TODO(DDN):switch over to fixed size buffers for string 
    //////////////////////////////////////////////////////////////////////////
    class CreateObjectMsg
    {
        NET_MESSAGE_DECL( CreateObjectMsg );
        
        CreateObjectMsg(){}

        CreateObjectMsg(const nsynBaseOld& rObject);

        void Read( const rage::netMessage& msg )
        {
            msg.ReadInt(&m_MngdObjectType, sizeof(m_MngdObjectType)*8);
            MsgHelper::ReadVector3(m_Position,msg);
            MsgHelper::ReadVector3(m_Rotation,msg);
        
            msg.Read(&m_NetID);
            msg.ReadBool(&m_AlignBottom);
            msg.ReadBool(&m_AlwaysActive);
            msg.ReadBool(&m_StartActive);

            MsgHelper::ReadString(m_ObjectType,msg);
        }

        void Write( rage::netMessage* msg ) const
        {
            msg->WriteInt(m_MngdObjectType, sizeof(m_MngdObjectType)*8);
            MsgHelper::WriteVector3(m_Position,*msg);
            MsgHelper::WriteVector3(m_Rotation,*msg);
    
            msg->Write(m_NetID);
            msg->WriteBool(m_AlignBottom);
            msg->WriteBool(m_AlwaysActive);
            msg->WriteBool(m_StartActive);

            MsgHelper::WriteString(m_ObjectType,*msg);
        }

        int                         m_MngdObjectType;   //our net object type.
        rage::atString              m_ObjectType;       //object type
        rage::Vector3               m_Position;         //position
        rage::Vector3               m_Rotation;         //rotation
        rage::s32                   m_NetID;            //the network ID which is consistent on host/peer
        bool                        m_AlignBottom;      //flag to align the bottom of the object
        bool                        m_AlwaysActive;     //flag to set the object to be always active
        bool                        m_StartActive;      //flag to start the object in active mode
    };

    //////////////////////////////////////////////////////////////////////////
    //sent when we want to delete an object on a peer ( host should never recive this )
    //////////////////////////////////////////////////////////////////////////
    class DeleteObjectMsg
    {
        NET_MESSAGE_DECL( DeleteObjectMsg );

        void Read( const rage::netMessage& msg )
        {
            msg.Read(&m_NetID);
        }

        void Write( rage::netMessage* msg ) const
        {
            msg->Write(m_NetID);
        }

        rage::s32                   m_NetID;            //the network ID which is consistent on host/peer
    };

}   //namespace ragesamples


#endif
