// 
// sample_net_physics/netsupport_common.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "netsupport_common.h"

#include "atl/slist.h"
#include "math/random.h"
#include "net/net.h"
#include "net/netpacket.h"
#include "physics/archmgr.h"
#include "physics/simulator.h"
#include "physics/levelnew.h"
#include "system/ipc.h"
#include "system/param.h"

#include "netsynch/objectmanager.h"
#include "netsynch/rigidbody.h"

using namespace ragenetsamples;
using namespace ragesamples;
using namespace rage;
using namespace rage::nsynPhysicsEnum;

// TITLE: Game
// PURPOSE:
//		Setup the network and game world.

//use this to scale the physics simulation and network interpolation code in time..
float gTimeScale = 1.f;

SampleSetup gSetUpParams;

gameInstance* g_GameInstance = NULL;

template <> int ConvertIntergral<int,float>(const float& rVal){
    netDebug3(("conversion might result in data loss"));
    return (int) rVal;
}

bool gExtUpdateFlag = false;

//////////////////////////////////////////////////////////////////////////
//network specific defines..

#if __XENON
#define PORT    1000
#else
#define PORT    5677
#endif

#define MAX_CONNECTIONS         32
#define MAX_MSG_QUEUE           1024
#define MAX_BINDINGS            1024 * 10
#define APP_ID                  0xBEEF

//Turns on/off simulated lag
#define USE_SIMULATED_LAG       0

//Size of heap used by net messages.
#define MESSAGE_HEAP_SIZE       ((1024 * 1024)*20)

//the number of pings the time peers will collect until it can make
//an effective attempt at time synchronization, this and the rate at which
//it sends the pings determine how long after a peer connects until it can
//make an effective estimate of the remote time.
#define NET_TIME_PING_BUCKETS   25

//how often we update the objects manually
//NOTE: when the framerate goes below the update frequency, it is not  that the 
//objects can be updated evenly at that point in time, need to fix this problem with a more
//intelligent update trimmer. The effective update rate is (UPDATE_FREQ_SECS + UPDATE_VARIANCE*UPDATE_FREQ_SECS)/2
//when u consider variance.
#define UPDATE_FREQ_SECS        .1f

//how much variance we apply to the update frequency, as defined by UPDATE_FREQ_SECS
#define UPDATE_VARIANCE         1.f

//how much random sleep time to add to each frame, causes frame rate fluctuation.
#define MAX_FRAME_SLEEP         1

//define this if we own the net physics objects vs the physics system..
#define NETP_OWNS_NET_OBJECTS   0

//number of objects to create
#define NUM_OBJECTS_CREATE      50

//how widely are the objects distributed when they are created ( uses this as bounds to rand func )
#define OBJECT_DISTRIB          1.f

//how much variation in terms of rotation to apply to the objects upon creation..
#define OBJECT_ROT_VARIANCE     2.f

//defines what type of sync object to use, see header for detail explanation of each..
#define SYNC_OBJECT_TYPE        3

//how high to put each object. Its height is a function of its creation order (higher number = higher up)
#define HEIGHT_SCALING_VALUE  .15f

#define WORLD_TO_CONSTRUCT      0

#define USE_FORCE_SYNC          0

//SYNC_OBJECT_TYPE available.
//  0   netPhyObjectSimple
//  1   netPhyObjectStaticBlend 
//  2   netPhyObjectProjectedBlend  
//  3   netPhyObjectProjectedBlendTimeCorrected
//  4   netPhyObjectSimpleSim                   (TODO)
//  5   netPhyObjectSimpleSimSync            (TODO)
//  6   netPhyObjectSimpleSimSyncOptimized   (TODO)

//object type to create
#define OBJECT_TYPE_TO_CREATE   "capsule"

//flag to determine if we should spawn the objects awake.
#define SPAWN_AWAKE             0

//OBJECT_TYPE_TO_CREATE available
//"barbell""
//"big_plane""
//"big_wall""
//"BLT""
//"box_222""
//"box_arm""
//"box_chain""
//"box_elbow""
//"box_flat""
//"box_geom""
//"box_geom_5""
//"box_med""
//"box_stair""
//"capsule""
//"capsule_double""
//"capsule_fat""
//"capsule_small""
//"capsule_tee""
//"capsule_thin""
//"capsule_torso""
//"capsule_upper_arm""
//"chandelier""
//"cinderblock""
//"crate""
//"crate_big""
//"crate_flat""
//"elbow""
//"escalade""
//"hotdog""
//"humanoid""
//"icosahedron""
//"lamborghini""
//"longer_crate""
//"long_crate""
//"male_torso_lod01""
//"mannequin""
//"materials""
//"mill""
//"park""
//"pingpongtable""
//"ponytail""
//"rope""
//"shaders""
//"shirt""
//"shirt_lod01""
//"shirt_lod02""
//"shorts_lod01""
//"shorts_lod02""
//"shoulder""
//"small_cloth""
//"small_plane""
//"small_wall""
//"sphere_007""
//"sphere_020""
//"sphere_056""
//"sphere_1""
//"sphere_122""
//"sphere_227""
//"spider""
//"splash""
//"splashot""
//"stair""
//"table_leg""
//"table_top""
//"torso_poly""
//"torso_spheres""
//"train_log_car""
//"train_rock_car""
//"train_tracks""
//"tune""
//"vehcamworld""
//"vp_elise_04""
//"wall""

//////////////////////////////////////////////////////////////////////////

//handy constant used by the handlemessage functions
const unsigned ragenetsamples::K_RTVAL_HANDLED = (netConnectionManager::HANDLER_HANDLED | netConnectionManager::HANDLER_STOP);

NET_EVENT_IMPL(nsynEvtGame);

//////////////////////////////////////////////////////////////////////////
//Use anonymous namespace to place utility classes and functions which we 
// don't want to expose to global scope
PARAM(simLag, "\"0\" artificial lag off, \"1\" artificial lag on." );
PARAM(msgHeapSizeKB, "\"[number]\" size of message heap the network can use." );
PARAM(pingBuckets, "\"[number]\" number of buckets used by the net time system." );
PARAM(updateFreq, "\"[float]\" time between updates in secs, update rate is 1/updateFreq." );
PARAM(updateVar, "\"[float]\" random scaling factor added to update time." );
PARAM(numObjects, "\"[number]\" number of objects to create" );
PARAM(objDistrib, "\"[float]\" how widely the objects are created apart" );
PARAM(objRotVar, "\"[float]\" how much random rotation to give the objects." );
PARAM(objSyncType, "\"0\" simple, \"1\" static blend, \"2\" projected blend, \"3\" projected time corrected blend, \"4\" simple sim" );
PARAM(objPhyType, "\"[string]\" physics object type, look in physics/assets folder for types." );
PARAM(frameMode, "\"0\" fixed frame, \"1\" fixed timestep, \"2\" realtime" );
PARAM(frameSleep, "\"[number]\" maximum random sleep time per frame. Causes frame rate fluxiation." );
PARAM(timeScale, "\"[float]\" scales the time, affects both physics and network interpolation." );
PARAM(startWorld, "\"[number]\" world to start with." );
PARAM(spawnAwake, "\"0\" spawns asleep, \"1\" spawns awake." );

namespace{


    enum {
           MODE_FIXED_FRAME,
           MODE_FIXED_TIMESTEP,
           MODE_REALTIME,
    };

    const float   OVERRRIDE_THRESHOLD = .5f;
    const float   PRIOIRTY_SCALE      =  .8f;
    const float   PROIRTY_MAX         =  2.f;

    //
    //PURPOSE:  Creates a new instance of a network synchronization object from the given type ID
    //
    nsynBaseOld*  CreateSyncObjectFromTypeID(int /*TypeID*/){

        nsynBaseOld* pNewNetObj = NULL;

//        switch(TypeID){
//        case    0:  pNewNetObj = new netPhyObjectSimple;                                break;
//        case    1:  pNewNetObj = new netPhyObjectStaticBlend;                           break;
//        case    2:  pNewNetObj = new netPhyObjectProjectedBlend;                        break;
//        case    3:  pNewNetObj = new netPhyObjectProjectedBlendTimeCorrected;           break;
//        case    4:  pNewNetObj = new netPhyObjectSimpleSim;                             break;
//        case    5:  pNewNetObj = new netPhyObjectSimpleSimTimeCorrected;                break;
//        case    6:  pNewNetObj = new netPhyObjectConstraintSimTimeCorrected;            break;
//        case    7:  pNewNetObj = new netPhyObjectSimple2SimTimeCorrected;               break;
//        case    8:  pNewNetObj = new netPhyObjectMixSimTimeCorrected;                   break;
//        case    9:  pNewNetObj = new netPhyObjectMixProjSimTimeCorrected;               break;
//        case    10: pNewNetObj = new netPhyObjectMixProjRealSimTimeCorrected;           break;
//        default:
//            AssertMsg(0,"Error, undefined net object type.");
//            break;
//        }

        return pNewNetObj;
    }

    bool CanRemoteRunPhysics( int ObjType ){
        if (ObjType>=4){
            return true;
        }

        return false;
    }

    bool ShouldUseForceUpdate( int ObjType ){
        if (ObjType == 8 || ObjType == 9 || ObjType == 10)
        {
            return true;
        }

        return false;
    }

    void ConstructPyramid (const Vector3& pos, int height, bool zRotate, const char* archName,
        bool zGravity , gameInstance* pGame)
    {
        if ( archName == NULL )
        {
            archName = "crate";
        }

        // TODO: make this get the archetype by calling CreateObject instead of using the archetype manager
        char boundFilePath[256];
        strcpy(boundFilePath, "physics\\");
        strcat(boundFilePath, archName);
        strcat(boundFilePath, "\\bound");
        const phArchetype* archetype = ARCHMGR.GetArchetype(boundFilePath, true, 0, phArchetype::ARCHETYPE_DAMP);
        Assert(archetype);
        const phBound* bound = archetype->GetBound();
        Assert(bound);

        const Vector3 boxMax(bound->GetBoundingBoxMax());
        const Vector3 boxMin(bound->GetBoundingBoxMin());
        const Vector3 boxOffset(bound->GetCentroidOffset());
        float size = boxMax.y - boxMin.y;
        float halfSize = 0.5f*size;
        float width = boxMax.x - boxMin.x;
        Vector3 boxPosition;
        float posWide,posHigh,posDeep=0.0f;
        for (int i=0; i<height; i++)
        {
            for (int j=0; j<height-i;j++)
            {
                posWide = float(j) * width - (height - i - 1) * width * 0.5f;
                posHigh = halfSize + float(i) * size;
                if (zGravity)
                {
                    if (zRotate)
                    {
                        boxPosition.Set(posWide,posDeep,posHigh);
                    }
                    else
                    {
                        boxPosition.Set(posDeep,posWide,posHigh);
                    }
                }
                else
                {
                    if (zRotate)
                    {
                        boxPosition.Set(posWide,posHigh,posDeep);
                    }
                    else if (!zGravity)
                    {
                        boxPosition.Set(posDeep,posHigh,posWide);
                    }
                }

                boxPosition.Add(pos);
                boxPosition.Subtract(boxOffset);

                pGame->CreateObject(archName, boxPosition, false, ORIGIN, false, gSetUpParams.m_SpawnAwake,true);
            }
        }
    }

    void ConstructTable (const Vector3& position, const Vector3& rotation, gameInstance* pGame)
    {
        Matrix34 tableMtx(CreateRotatedMatrix(position,rotation));
        float legHorzOffset = 0.6f;
        float legVertOffset = 0.4f;
        Matrix34 objectMtx(tableMtx);
        Vector3 objectPos(0,legVertOffset*2.f,0);
        tableMtx.Transform(objectPos,objectMtx.d);
        pGame->CreateObject("table_top", objectMtx.d, false, objectMtx.GetEulers(),false,gSetUpParams.m_SpawnAwake,true);
        objectPos.Set(-legHorzOffset,legVertOffset,-legHorzOffset);
        tableMtx.Transform(objectPos,objectMtx.d);
        pGame->CreateObject("table_leg", objectMtx.d, false, objectMtx.GetEulers(),false,gSetUpParams.m_SpawnAwake,true);
        objectPos.Set(legHorzOffset,legVertOffset,-legHorzOffset);
        tableMtx.Transform(objectPos,objectMtx.d);
        pGame->CreateObject("table_leg", objectMtx.d, false, objectMtx.GetEulers(),false,gSetUpParams.m_SpawnAwake,true);
        objectPos.Set(-legHorzOffset,legVertOffset,legHorzOffset);
        tableMtx.Transform(objectPos,objectMtx.d);
        pGame->CreateObject("table_leg", objectMtx.d, false, objectMtx.GetEulers(),false,gSetUpParams.m_SpawnAwake,true);
        objectPos.Set(legHorzOffset,legVertOffset,legHorzOffset);
        tableMtx.Transform(objectPos,objectMtx.d);
        pGame->CreateObject("table_leg", objectMtx.d, false, objectMtx.GetEulers(),false,gSetUpParams.m_SpawnAwake,true);
        objectPos.Set(0.0f,2.0f*legVertOffset+0.05f,0.0f);
        tableMtx.Transform(objectPos,objectMtx.d);
        pGame->CreateObject("table_leg", objectMtx.d, false, objectMtx.GetEulers(),false,gSetUpParams.m_SpawnAwake,true);
    }

    void ConstructJenga(const Vector3& pos, int height, int levelSize, const char* archName, gameInstance* pGame)
    {
        if ( archName == NULL )
        {
            archName = "long_crate";
        }

        // TODO: make this get the archetype by calling CreateObject instead of using the archetype manager
        char boundFilePath[256];
        strcpy(boundFilePath, "physics\\");
        strcat(boundFilePath, archName);
        strcat(boundFilePath, "\\bound");
        const phArchetype* archetype = ARCHMGR.GetArchetype(boundFilePath, true, 0, phArchetype::ARCHETYPE_DAMP);
        Assert(archetype);
        const phBound* bound = archetype->GetBound();
        Assert(bound);

        const Vector3 boxMax(bound->GetBoundingBoxMax());
        const Vector3 boxMin(bound->GetBoundingBoxMin());
        float boxHeight = boxMax.y - boxMin.y;
        float boxWidth = (boxMax.z - boxMin.z);// * 0.5f;
        const Vector3 boxOffset(bound->GetCentroidOffset());

        Matrix34 straight;
        straight.Identity3x3();

        Matrix34 rotated;
        rotated.MakeRotateY(PI * 0.5f);

        for (int level = 0; level < height; ++level)
        {
            for (int block = 0; block < levelSize; ++block)
            {
                float displacement = (block - levelSize * 0.5f + 0.5f);

                straight.d = pos;
                straight.d.y += boxHeight * 2 * level;
                straight.d.z = pos.z + boxWidth * displacement;

                pGame->CreateObject(archName, straight.d, false, straight.GetEulers(),false,gSetUpParams.m_SpawnAwake,true);

                rotated.d = pos;
                rotated.d.y += boxHeight * (2 * level + 1);
                rotated.d.x = pos.x + boxWidth * displacement;

                pGame->CreateObject(archName, rotated.d, false, Vector3(0,PI * 0.5f,0),false,gSetUpParams.m_SpawnAwake,true);
            }
        }
    }

    void ConstructJumble(int numObjects, gameInstance* pGame)
    {
        mthRandom random;

        for (int objects = 0; objects < numObjects; ++objects)
        {

            char* pObj = "capsule";

            switch(objects%26){
                case 0: pObj =  "barbell"; break;
                case 1: pObj =  "sphere_122"; break;
                case 2: pObj =  "table_leg"; break;
                case 3: pObj =  "sphere_056"; break;
                case 4: pObj =  "box_222"; break;
                case 5: pObj =  "box_arm"; break;
                case 6: pObj =  "box_chain"; break;
                case 7: pObj =  "box_elbow"; break;
                case 8: pObj =  "box_flat"; break;
                case 9: pObj =  "box_geom"; break;
                case 10: pObj =  "box_geom_5"; break;
                case 11: pObj =  "box_med"; break;
                case 12: pObj =  "humanoid"; break;
                case 13: pObj =  "capsule"; break;
                case 14: pObj =  "capsule_double"; break;
                case 15: pObj =  "capsule_fat"; break;
                case 16: pObj =  "capsule_small"; break;
                case 17: pObj =  "capsule_tee"; break;
                case 18: pObj =  "capsule_thin"; break;
                case 19: pObj =  "capsule_torso"; break;
                case 20: pObj =  "capsule_upper_arm"; break;
                case 21: pObj =  "chandelier"; break;
                case 22: pObj =  "cinderblock"; break;
                case 23: pObj =  "crate"; break;
                case 24: pObj =  "crate_big"; break;
                case 25: pObj =  "crate_flat"; break;
            }
           
            Vector3 Pos(random.GetRanged(-gSetUpParams.m_ObjectDistrib, gSetUpParams.m_ObjectDistrib), 1.0f + objects * HEIGHT_SCALING_VALUE,
                random.GetRanged(-gSetUpParams.m_ObjectDistrib, gSetUpParams.m_ObjectDistrib));

            Vector3 Rot(random.GetRanged(-gSetUpParams.m_ObjectRotVariance, gSetUpParams.m_ObjectRotVariance), 1.0f + objects * 0.15f,
                random.GetRanged(-gSetUpParams.m_ObjectRotVariance, gSetUpParams.m_ObjectRotVariance));

            pGame->CreateObject(pObj,Pos,true,Rot,false,gSetUpParams.m_SpawnAwake,true);
        }
    }

    void ConstructSame(int numObjects, gameInstance* pGame)
    {
        mthRandom random;

        for (int objects = 0; objects < numObjects; ++objects)
        {
            const char* pObj = static_cast<const char*>(gSetUpParams.m_ObjectTypeToCreate);

            Vector3 Pos(random.GetRanged(-gSetUpParams.m_ObjectDistrib, gSetUpParams.m_ObjectDistrib), 1.0f + objects * HEIGHT_SCALING_VALUE,
                random.GetRanged(-gSetUpParams.m_ObjectDistrib, gSetUpParams.m_ObjectDistrib));

            Vector3 Rot(random.GetRanged(-gSetUpParams.m_ObjectRotVariance, gSetUpParams.m_ObjectRotVariance), 1.0f + objects * 0.15f,
                random.GetRanged(-gSetUpParams.m_ObjectRotVariance, gSetUpParams.m_ObjectRotVariance));

            pGame->CreateObject(pObj,Pos,true,Rot,false,gSetUpParams.m_SpawnAwake,true);
        }
    }

    struct NetCreatorClass{
        phInst* CreateFunc(void){
            return m_pNetPhyObj;
        }
        phInst* m_pNetPhyObj;
    };
}


ragenetsamples::gameInstance* GetGame()
{
    return g_GameInstance;
}

//////////////////////////////////////////////////////////////////////////

SampleSetup::SampleSetup(){
    m_UseSimulatedLag          =USE_SIMULATED_LAG;
    m_MessageHeapSize          =MESSAGE_HEAP_SIZE;
    m_NetTimePingBuckets       =NET_TIME_PING_BUCKETS;
    m_UpdateFreqSecs           =UPDATE_FREQ_SECS;
    m_UpdateVariance           =UPDATE_VARIANCE;
    m_NumObjectsCreate         =NUM_OBJECTS_CREATE;
    m_ObjectDistrib            =OBJECT_DISTRIB;
    m_ObjectRotVariance        =OBJECT_ROT_VARIANCE;
    m_SyncObjectType           =(SAMPLE_OBJECT_TYPE)(SYNC_OBJECT_TYPE);
    m_ObjectTypeToCreate       = OBJECT_TYPE_TO_CREATE;
    m_Mode                     = MODE_REALTIME;
    m_FrameSleep               = MAX_FRAME_SLEEP;
    m_TimeScale                = gTimeScale;
    m_UseForcedSync            = USE_FORCE_SYNC;
    m_WorldConstruct           = WORLD_TO_CONSTRUCT;
    m_SpawnAwake               = SPAWN_AWAKE;
}

bool SampleSetup::ExtractFromCommandline(){
    const char* simLag          = PARAM_simLag.GetByName( "simLag", NULL);   
    const char* msgHeapSizeKB   = PARAM_msgHeapSizeKB.GetByName( "msgHeapSizeKB", NULL ); 
    const char* pingBuckets     = PARAM_pingBuckets.GetByName( "pingBuckets", NULL );   
    const char* updateFreq      = PARAM_updateFreq.GetByName( "updateFreq", NULL );  
    const char* updateVar       = PARAM_updateVar.GetByName( "updateVar", NULL ); 
    const char* numObjects      = PARAM_numObjects.GetByName( "numObjects", NULL );  
    const char* objDistrib      = PARAM_objDistrib.GetByName( "objDistrib", NULL );  
    const char* objRotVar       = PARAM_objRotVar.GetByName( "objRotVar", NULL );
    const char* objSyncType     = PARAM_objSyncType.GetByName( "objSyncType", NULL );
    const char* objPhyType      = PARAM_objPhyType.GetByName( "objPhyType", NULL );
    const char* frameMode       = PARAM_objPhyType.GetByName( "frameMode", NULL );
    const char* frameSleep      = PARAM_objPhyType.GetByName( "frameSleep", NULL );
    const char* timeScale       = PARAM_objPhyType.GetByName( "timeScale", NULL );
    const char* startWorld      = PARAM_objPhyType.GetByName( "startWorld", NULL );
    const char* spawnAwake      = PARAM_objPhyType.GetByName( "spawnAwake", NULL );

    if (simLag){
        if (::strcmp( "1", simLag ) == 0){
            m_UseSimulatedLag = true;
        } else {
            m_UseSimulatedLag = false;
        }
    }

    if (spawnAwake){
        if (::strcmp( "1", spawnAwake ) == 0){
            m_SpawnAwake = true;
        } else {
            m_SpawnAwake = false;
        }
    }

    if (msgHeapSizeKB){
        if (::atoi(msgHeapSizeKB)>0){
            m_MessageHeapSize = ::atoi(msgHeapSizeKB)*1024;
        }
    }

    if (pingBuckets){
        if (::atoi(pingBuckets)>0){
            m_NetTimePingBuckets = ::atoi(pingBuckets);
        }
    }

    if (updateFreq){
        if (::atof(updateFreq)>0){
            m_UpdateFreqSecs = (float)::atof(updateFreq);
        }
    }

    if (updateVar){
        m_UpdateVariance = (float)::atof(updateVar);
    }

    if (numObjects){
        if (::atoi(numObjects)>0){
            m_NumObjectsCreate = ::atoi(numObjects);
        }
    }

    if (objDistrib){
        m_ObjectDistrib = (float)::atof(objDistrib);
    }

    if (objRotVar){
        m_ObjectRotVariance = (float)::atof(objRotVar);
    }

    if (objSyncType){
        m_SyncObjectType = (SAMPLE_OBJECT_TYPE)(::atoi(objSyncType));
    }

    if (objPhyType){
        m_ObjectTypeToCreate = objPhyType;
    }

    if (frameMode){
        switch(::atoi(frameMode))
        {
        case 0: m_Mode = MODE_FIXED_FRAME;     break;
        case 1: m_Mode = MODE_FIXED_TIMESTEP;  break;
        case 2: m_Mode = MODE_REALTIME;        break;
        default:
            Warningf("Unrecognized frame mode type");
            break;
        }
    }

    if (frameSleep){
        if (::atoi(frameSleep)>0){
            m_FrameSleep = ::atoi(frameSleep);
        }
    }

    if (timeScale){
        m_TimeScale = (float)::atof(timeScale);
    }

    m_UseForcedSync = ShouldUseForceUpdate(m_SyncObjectType);

    if (timeScale){
        m_TimeScale = (float)::atof(timeScale);
    }

    if (startWorld){
        m_WorldConstruct = ::atoi(startWorld);

        if (m_WorldConstruct==0 || m_WorldConstruct == 2)
        {
            m_NumObjectsCreate = 50;
        }
    }

    return true;
}

//////////////////////////////////////////////////////////////////////////

void SimpleGameObj::PreComputeImpacts (phImpact& UNUSED_PARAM(impactList), phInst* hitInst)
{
    if (m_PriorityOverride>OVERRRIDE_THRESHOLD)
    {
        if (GetGame()->TakeControlAlt(hitInst))
        { 
            netDebug1(("m_PriorityOverride %f , taking control", m_PriorityOverride));
            m_PriorityOverride =  Clamp(m_PriorityOverride*PRIOIRTY_SCALE,FLT_MIN,PROIRTY_MAX);
        }
    }
}

 void    SimpleGameObj::ApplyImpulse (const Vector3&  UNUSED_PARAM(impulse), const Vector3&  UNUSED_PARAM(position), int  UNUSED_PARAM(component), const Vector3*  UNUSED_PARAM(push))
{
    HandleEvent(nsynEvtPhysics(NET_PHY_EVT_APPLY_IMPULSE));
}

 void    SimpleGameObj::ApplyImpulse (const Vector3&  UNUSED_PARAM(impulse))
 {
    HandleEvent(nsynEvtPhysics(NET_PHY_EVT_APPLY_IMPULSE));
}

 void    SimpleGameObj::ApplyForce (const Vector3& UNUSED_PARAM(force), const Vector3&  UNUSED_PARAM(position), int  UNUSED_PARAM(component))
 {
     HandleEvent(nsynEvtPhysics(NET_PHY_EVT_APPLY_FORCE));
 }

 void    SimpleGameObj::NotifyDeactivate ()
 {
     HandleEvent(nsynEvtPhysics(NET_PHY_EVT_SLEEP));
 }

 phInst *SimpleGameObj::PrepareForActivation (phCollider ** UNUSED_PARAM(colliderToUse), phImpact * UNUSED_PARAM(impactList),phImpactData * UNUSED_PARAM(impactData), const phContactMgr::Iterator * UNUSED_PARAM(contactGroup))
 {
     EventReturnType val = HandleEvent(nsynEvtPhysics(NET_PHY_EVT_PREPARE_ACTIVATE));

     // If the event is blocked on the non-authoritive machine, return NULL to prevent activation.
     if (val == rage::EVT_HANDLED_NAUTH_BLOCK)
     {
         return NULL;
     }

     // All other conditions return this.
     return this;
 }


//////////////////////////////////////////////////////////////////////////
//contactsSampleManager : sample physics manager used for our test purposes..

class networkSampleManager : public physicsSampleManager
{
public:
    //
    //PURPOSE:  Initialize the world and physics system.
    //
    virtual void InitClient(){

        phConfig::EnableRefCounting(); 

        physicsSampleManager::InitClient();
    }
};

//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////


//
//PURPOSE:  dtor for gameInstance, makes sure to destroy the m_pPhySampleMngr and 
//clear out the network object container, proably also should make sure we shutdown 
//the network if we haven't already..
//
gameInstance::~gameInstance( void ){

    ShutDown();

    if (m_pPhySampleMngr)
    {
        delete m_pPhySampleMngr;
    }

    m_SimpleFactory = NULL;
    
    //unregister our message message handler
    m_NetMgr.RemoveMessageHandler( &m_NetMsgHandler );

    g_GameInstance = NULL;
}

// STEP #1. Instantiate the network

//
//PURPOSE:  Initialize the network component, needs to be host/peer aware.
//
bool gameInstance::InitNetwork( void ){

    netDebug1(( "Running as %s", m_IsHost ? "HOST" : "GUEST" ));

    if (m_IsHost){
        netDebug1(("Update Frequency is %3.3f ups with a variance of %3.3f", 
            1.f/gSetUpParams.m_UpdateFreqSecs, gSetUpParams.m_UpdateVariance));
    }

    //Setup the default network policy

    netPolicies DefaultPolicy;

#if __DEV
    DefaultPolicy.SetKeepAliveInterval( 0 );
    DefaultPolicy.SetQoSInterval( 0 );
    DefaultPolicy.SetConnectTimeout( 10*1000 );
#endif

    //--
    //Setup the net manager and its policies

    m_NetMgr.Init( PORT, MAX_CONNECTIONS, MAX_MSG_QUEUE, APP_ID );
    m_NetMgr.SetPolicies( DefaultPolicy );

    //--
    //Stats are optional..

    if (m_CollectNetStats)
    {
        // Init the netstats system
        m_NetStats.Init( MAX_CONNECTIONS );

        // Enable stats collection for all stat types.
        m_NetStats.SetStatEnabled( netStats::STAT_ALL, true );

        //Register the stats collector with the connection manager.
        m_NetMgr.SetStatsCollector( &m_NetStats );
    }

    //--
    //Init the netHoster

    m_Hoster.Init( &m_NetMgr );

    //--
    //Push our netMsgHanlder to the active queue, this will allow us to recive net messages from
    //the network.

    m_NetMgr.PushMessageHandler( &m_NetMsgHandler );

    //--
    //Enable the hoster as needed

    if( m_IsHost )
    {
        m_Hoster.SetHostingEnabled( true );
    }
    else
    {
        m_Hoster.SetHostingEnabled( false );
    }

    //--
    //Simulated lag is optional, use the commandline -simLag 1 to enbale it.

    netSocket* pSocket = m_NetMgr.GetDataSocket();

    if (pSocket && m_UseFakeLag){

        netSocket::FakeLatency LatencyPolicy;

        //Baseline latency.
        LatencyPolicy.m_BaselineLatency = 150;
        //Max extra latency during a latency burst.
        LatencyPolicy.m_MaxExtraBurstLatency = LatencyPolicy.m_BaselineLatency/2;
        //Min time between latency bursts.
        LatencyPolicy.m_MinTimeBetweenBursts = 1000;
        //Max time between latency bursts.
        LatencyPolicy.m_MaxTimeBetweenBursts = 5000;
        //Min duration of a latency burst.
        LatencyPolicy.m_MinBurstDuration = 100;
        //Max duration of a latency burst.
        LatencyPolicy.m_MaxBurstDuration = 1000;

        pSocket->SetFakeLatency( LatencyPolicy );

        netSocket::FakeDrop DropPolicy;

        //Baseline percent packet drop (0 - 100).
        DropPolicy.m_BaselineDropPcnt = 2;
        //Maximum percent added to baseline during a burst.
        DropPolicy.m_MaxExtraBurstDropPcnt = 8;
        //Min time between drop bursts.
        DropPolicy.m_MinTimeBetweenBursts = 1000;
        //Max time between drop bursts.
        DropPolicy.m_MaxTimeBetweenBursts = 5000;
        //Min duration of a drop burst.
        DropPolicy.m_MinBurstDuration = 100;
        //Max duration of a drop burst.
        DropPolicy.m_MaxBurstDuration = 1000;

        pSocket->SetFakeDrop( DropPolicy );
    }

    //--
    //Init the network object manager
    
    AssertMsg(0,"This sample is broken until we convert over the netsynch manager to use the netroute module -ddn");

    //m_NetObjMngr.Init(&m_NetMgr,m_IsHost,gSetUpParams.m_Mode == MODE_FIXED_FRAME);

    //--
    //Setup our update scope, they're used to control the updates of the network objects.
    //SEE (rage::nsynUpdateScope)

    nsynUpdateScope scopeLow (gSetUpParams.m_UpdateFreqSecs);
    nsynUpdateScope scopeHigh(gSetUpParams.m_UpdateFreqSecs*2.5f);

    m_GameScopeLow  = m_NetObjMngr.AddUpdateScope( scopeLow );
    m_GameScopeHigh = m_NetObjMngr.AddUpdateScope( scopeHigh );

    Assert(m_GameScopeHigh != INVALID_SCOPE_ID && m_GameScopeLow != INVALID_SCOPE_ID);

    //--
    //Register this as an external net event message handler, this will allow us to recive
    //net events targeted toward this peer. The net events and the net messages are two different
    //data exchange subsystems. Net Messages are purely network related data whose use is usually
    //constrained to the network subsystems. The net events are applicaiton level network transparent 
    //objects, which work within the netsynch framework. SEE (rage::netMessage, rage::nsynEvtBase)

    m_NetObjMngr.RegisterExternalEvtHandler(
        MAKE_NET_FUNCTOR(gameInstance,HandleEvent,this));

    //-STOP 

    m_IsShutdown = false;

    return true;
}

// STEP #2. Instantiate the game world

//
//PURPOSE:  Instantiates the physics system, which inturn creates a backdrop for our tests.
//

bool gameInstance::InitWorldPhysics( void ){

    //--
    //Init the physics subsystem. In this case were using a derived network sample physics manager.

    Assert(m_pPhySampleMngr==NULL);

    m_pPhySampleMngr = rage_new networkSampleManager;

    Assert(m_pPhySampleMngr!=NULL);
 
    m_pPhySampleMngr->Init(NULL);

    //-STOP

    return true;
}

//
//PURPOSE:  Creates a demo world on the host side
//

bool    gameInstance::CreateDemoWorld ( void ){

    Assert(m_IsHost);

    if (!m_IsHost)
    {
        return false;
    }
 
    //--
    //Create the demoworld. We are using the physics demoworld project to manage the phyiscal objects.
    //Fill out the scene data

    netSceneObject newSceneData;

    newSceneData.m_WorldOffset      = Vector3(0.0f,0.0f,0.0f);
    newSceneData.m_MaxOctreeNodes   = 1000;
    newSceneData.m_MaxActiveObjects = gSetUpParams.m_NumObjectsCreate+1;
    newSceneData.m_MaxObjects       = gSetUpParams.m_NumObjectsCreate+1;
    newSceneData.m_MaxBehaviors     = 0;
    
    newSceneData.m_SyncObjectType   = 0;//m_SyncObjectType;

    newSceneData.m_WorldMin         = Vector3(-999.0f,-999.0f,-999.0f);
    newSceneData.m_WorldMax         = Vector3(999.0f,999.0f,999.0f);

    newSceneData.m_SceneName        = "Park";
    newSceneData.m_Position         = Vector3(17.5f, 0.0f, 14.5f);
    newSceneData.m_SimulatePhysics  = true;//CanRemoteRunPhysics(gSetUpParams.m_SyncObjectType);
    newSceneData.m_TimeScale        = gTimeScale;
    newSceneData.m_FixedFrameMode   = gSetUpParams.m_Mode;
    
    //--
    //Note the create scene call. This setups the world with the given parameters in newSceneData

    CreateScene(newSceneData);

    //-STOP

    return true;
}

//
//PURPOSE:	resets the world
//
void gameInstance::ResetWorld ( void ){
    m_CanTakeControl = false;

//    for (int i=0; i<m_GameObjects.GetNumSlots(); i++) {
//        rage::atMap<nsynObjIdType,SimpleGameObjSPtr>::Entry *e = m_GameObjects.GetEntry(i);
//        while (e) {
//
//            if (e->data!=NULL && e->data->m_NetObj != NULL)
//            {
//                m_NetObjMngr.ReleaseSyncObject( e->data->m_NetObj );
//            }
//
//            e = e->next;
//        }
//    }

    if (m_pPhySampleMngr->GetCurrentWorld())
    {
        m_pPhySampleMngr->GetCurrentWorld()->Reset();
    }

    m_CanTakeControl = true;
}

//
//PURPOSE:  shutsdown the game
//
void gameInstance::ShutDown  ( void ){

    if (!m_IsShutdown)
    {
        //have to manually clear out the phinst as we share it with the demoworld, who will
        //delete it upon exit. Since our phinst is now a counted object we can't let demoworld
        //explicity delete it.
        atSNode<phDemoObject*>*  iter = m_DemoObjects.GetHead();

        if (iter)
        {  
            for (;iter!=NULL;iter = iter->GetNext())
            {
                iter->Data->SetPhysInst(NULL);
            }
        }

        m_DemoObjects.DeleteAll();

        m_NetObjMngr.UnregisterExternalEvtHandler(
            MAKE_NET_FUNCTOR(gameInstance,HandleEvent,this));

        //send off our last msgs before we shut it down..
        m_NetMgr.Update();

        m_Hoster.Shutdown();

        m_NetObjMngr.Shutdown();

        m_NetMgr.Shutdown();

        netShutdown(); 

        if (m_pPhySampleMngr)
        {
            m_pPhySampleMngr->Shutdown();
        }

        m_GameObjects.Kill();

        m_IsShutdown = true;
    }
}

//  STEP #3. Refresh the game.

//
//PURPOSE:  Refresh the game, this includes network and world/physics components
//
void gameInstance::Refresh ( void ){

    //--
    //After successfully setting up the network and the world we will need to refresh it
    //periodically. In this case every frame.<P>
    //Refresh the physics manager.
    if (m_pPhySampleMngr)
    {
        m_pPhySampleMngr->UpdateStep();
    }

    //--
    //Refresh the hoster, allows incoming peers to make connections to us.
    if( m_IsHost )
    {
        m_Hoster.Update();
    } 

    //--
    //The object manager needs the elapsed physics time per frame if we are 
    //using a relative time mode such as fixed frame mode.
    m_NetObjMngr.SetElapsedPhysicsTime( GetElapsedPhysicsTime() );

    //--
    //Refresh the object manager
    m_NetObjMngr.Update();
    
    //--
    //Refresh the net manager. This flushes outgoing and processes incoming net
    //messages for use.
    m_NetMgr.Update();

    //-STOP

    unsigned int SleepTime = 1;

#if __DEV
    //fluctuate the framerate to see what happens..
    mthRandom random;
    SleepTime = random.GetRanged(1,gSetUpParams.m_FrameSleep);
#endif

    //--
    //Give other threads CPU time
    sysIpcSleep( SleepTime );

    //-STOP
}

//
//PURPOSE:  outputs the net stats for the given connection, if it's enabled
//
void gameInstance::OutputNetStats ( const int connectID ){

    if (!m_CollectNetStats)
    {
        Printf( "Netstats not Enabled." );
        return;
    }

    const netStats::StatsRecord* statsRec = m_NetStats.GetStats( connectID );

    if (statsRec==NULL)
    {
        Printf( "StatsRecord for connection %d is NULL.",connectID );
        return;
    }

    Printf( "*****CONNECTION %d*****",connectID );
    Printf( "    Latency:%d\n", m_NetMgr.GetLatency( connectID ) );
    Printf( "    INB:%f, OUTB:%f, INBPP:%f, OUTBPP:%f\n",
        statsRec->m_InboundBytesPerSec.GetRate(),
        statsRec->m_OutboundBytesPerSec.GetRate(),
        statsRec->m_InboundPacketsPerSec.GetRate() == 0.f ? 
        0.f : statsRec->m_InboundBytesPerSec.GetRate()/statsRec->m_InboundPacketsPerSec.GetRate(),
        statsRec->m_OutboundPacketsPerSec.GetRate() == 0.f ?
        0.f : statsRec->m_OutboundBytesPerSec.GetRate()/statsRec->m_OutboundPacketsPerSec.GetRate());
    Printf( "    INP:%f, OUTP:%f\n",
        statsRec->m_InboundPacketsPerSec.GetRate(),
        statsRec->m_OutboundPacketsPerSec.GetRate() );
    Printf( "    INM:%f, OUTM:%f\n",
        statsRec->m_InboundMsgsPerSec.GetRate(),
        statsRec->m_OutboundMsgsPerSec.GetRate() );
}

//
//PURPOSE:  checks to see if the game wants to exit
//
bool gameInstance::WantsToExit ( void ){
    if (m_pPhySampleMngr)
    {
        return m_pPhySampleMngr->WantsExit();
    } 
    else 
    {
        return false;
    }
}

//
//PURPOSE:  creates the scene and stores away the pertinent information so it 
//can be propagated to the peers..
//
bool gameInstance::CreateScene  ( const netSceneObject& rSceneData ){
    
    Assert(m_pPhySampleMngr!=NULL);

    if (m_pPhySampleMngr==NULL)
    {
        return false;
    }

    // Use this to offset all objects in the world, to test physics behavior with roundoff errors far from the origin.
    phDemoObject::SetWorldOffset(rSceneData.m_WorldOffset);

    // Set the physics level parameters.

    Vector3 worldMin(rSceneData.m_WorldMin);
    Vector3 worldMax(rSceneData.m_WorldMax);

    worldMin.Add(phDemoObject::GetWorldOffset());
    worldMax.Add(phDemoObject::GetWorldOffset());

    // Setup complicated terrain
    m_pPhySampleMngr->GetWorldContainer().AddDemo(rage_new phDemoWorld(rSceneData.m_SceneName));

    Assert(m_pPhySampleMngr->GetCurrentWorld()!=NULL);

    if (m_pPhySampleMngr->GetCurrentWorld()==NULL)
    {
        return false;
    }

    m_pPhySampleMngr->GetCurrentWorld()->Init(
        rSceneData.m_MaxOctreeNodes,
        rSceneData.m_MaxActiveObjects,
        rSceneData.m_MaxObjects,
        rSceneData.m_WorldMin,
        rSceneData.m_WorldMax,
        rSceneData.m_MaxBehaviors);

    m_SyncObjectType = rSceneData.m_SyncObjectType;

    //until the geometry is fixed we will have to use a stand in.. -ddn 8/23/05
    //m_pPhySampleMngr->GetCurrentWorld()->CreateFixedObject(
    //  rSceneData.m_SceneName,rSceneData.m_Position);

    // Standin world.. 
    m_pPhySampleMngr->GetCurrentWorld()->ConstructTerrainPlane();
   

    // Start with our default world, since we'll only support 1 for now.
    m_pPhySampleMngr->GetWorldContainer().SetCurrentDemo(0);
    m_pPhySampleMngr->GetCurrentWorld()->Activate();
   
    m_SceneData = rSceneData;
 
    // Should be better way to do this to stop its updates.. -ddn
    if (!rSceneData.m_SimulatePhysics && !m_IsHost)
    {
        m_pPhySampleMngr->GetCurrentWorld()->Pause();
    }

    gTimeScale = rSceneData.m_TimeScale;

    if (m_IsHost){
        switch(gSetUpParams.m_WorldConstruct)
        {
        case 0: ConstructJenga(Vector3(0.0f, 0.0f, 0.0f), 2, 3, "longer_crate", this);  break;
        case 1: ConstructJumble( gSetUpParams.m_NumObjectsCreate, this );               break;
        case 2: ConstructPyramid(Vector3(0.0f, 0.0f, -2.0f),9,true,NULL,false,this);     break;
        case 3: ConstructSame( gSetUpParams.m_NumObjectsCreate, this );                 break;
        default:
                ConstructJenga(Vector3(0.0f, 0.0f, 0.0f), 2, 3, "longer_crate", this);  break;
        }
    }
    //set it to realtime mode..
    m_pPhySampleMngr->GetCurrentWorld()->SetFixedFrame(
        rSceneData.m_FixedFrameMode == MODE_FIXED_FRAME, 
        rSceneData.m_FixedFrameMode == MODE_FIXED_TIMESTEP);

    //register our control clbk
    m_pPhySampleMngr->GetCurrentWorld()->RegisterResetWorldClbk(
        MakeFunctor(*this,&gameInstance::ResetWorldNotify));

    m_pPhySampleMngr->GetCurrentWorld()->RegisterTakeControlClbk(
        MakeFunctor(*this,&gameInstance::TakeControl));

    m_pPhySampleMngr->GetCurrentWorld()->RegisterRecordApplyForceClbk (
        MakeFunctor(*this,&gameInstance::RecordApplyForce));

    m_pPhySampleMngr->GetCurrentWorld()->RegisterRecordApplyImpulseClbk(
        MakeFunctor(*this,&gameInstance::RecordApplyImpulse));

    m_pPhySampleMngr->GetCurrentWorld()->RegisterReleaseOwnershipClbk(
        MakeFunctor(*this,&gameInstance::ReleaseOwnership));
  
    m_pPhySampleMngr->GetCurrentWorld()->RegisterResetObjectClbk(
        MakeFunctor(*this,&gameInstance::ResetObject));

    m_NetObjMngr.SetFixedFrameMode( rSceneData.m_FixedFrameMode == MODE_FIXED_FRAME );
    
    return true;
}

//
//PURPOSE:  wraps the object creation so we can store network specific data for object propagation..
//
SimpleGameObj* gameInstance::CreateObject ( const char* file, const Vector3& position, bool alignBottom, 
                                             const Vector3& rotation,bool alwaysActive, bool startActive, bool registerObject)
{    
    phDemoObject*   pDemoObject = NULL;
    NetCreatorClass createInst;

    SimpleGameObj* newGameObj = rage_new SimpleGameObj;

    Assert(newGameObj!=NULL);

    createInst.m_pNetPhyObj = newGameObj;

    AssertMsg(m_pPhySampleMngr!=NULL,"m_pPhySampleMngr is NULL, should not be null at this point.");

    if (m_pPhySampleMngr && m_pPhySampleMngr->GetCurrentWorld() != NULL)
    {
        //passing in a functor to create our phInst object in the physics system
        //allows us to specialize the object without creating dependicies on the physics 
        //system and the network system..
        pDemoObject =  m_pPhySampleMngr->GetCurrentWorld()->CreateObject(
            file,position,alignBottom,rotation,alwaysActive,startActive,
            MakeFunctorRet(createInst,&NetCreatorClass::CreateFunc));
    }

    if (pDemoObject==NULL)
    {
        AssertMsg(0,"could not create demo object");
        
        if (newGameObj!=NULL){
            delete newGameObj;
        }

        return NULL;
    }

    atSNode<phDemoObject*>  *node = rage_new atSNode<phDemoObject*>(pDemoObject);

    m_DemoObjects.Append( *node );

    if (registerObject)
    {
        SimpleGameObjectNetFactory::SetupData data;

        data.m_ObjectType = file;       
        data.m_Position = position;         
        data.m_Rotation = rotation;                  
        data.m_AlignBottom = alignBottom;      
        data.m_AlwaysActive = alwaysActive;     
        data.m_StartActive = startActive;   

        if (!m_SimpleFactory->RegisterGameObject(newGameObj,data))
        {
            Warningf("Could not register game object to net object manager.");
        }
            
        RegisterGameObject(newGameObj);
     
        //////////////////////////////////////////////////////////////////////////
        //TEST CODE : testing out event sending, send an empty event to the object and
        //see what happens

        nsynEvtPhysics tmp;

        m_NetObjMngr.SendTo( tmp, newGameObj->m_NetId );

        //////////////////////////////////////////////////////////////////////////
    }

    return newGameObj;
}

void gameInstance::ResetWorldNotify ( void )
{
    nsynEvtGame evt(nsynEvtGame::EVT_EVENT_RESET_WORLD);

    m_NetObjMngr.SendToAllPeers(evt);

//    for (int i=0; i<m_GameObjects.GetNumSlots(); i++) {
//        rage::atMap<nsynObjIdType,SimpleGameObjSPtr>::Entry *e = m_GameObjects.GetEntry(i);
//        while (e) {
//
//            if (e->data!=NULL && e->data->m_NetObj != NULL)
//            {
//                m_NetObjMngr.ReleaseSyncObject( e->data->m_NetObj );
//            }
//
//            e = e->next;
//        }
//    } 
//    
//    for (int i=0; i<m_GameObjects.GetNumSlots(); i++) {
//        rage::atMap<nsynObjIdType,SimpleGameObjSPtr>::Entry *e = m_GameObjects.GetEntry(i);
//        while (e) {
//
//            if (e->data!=NULL && e->data->m_NetObj != NULL)
//            {
//                m_NetObjMngr.AcquireAuthority( e->data->m_NetId );
//            }
//
//            e = e->next;
//        }
//    }
}

void gameInstance::ResetObject ( phInst* inst )
{
    //slow but we don't do this enough to worry about it, if we did we can use our own
    //type checking..
    SimpleGameObj* gameObj = dynamic_cast<SimpleGameObj*>(inst);

    if (gameObj==NULL)
    {
        return;
    }

    if (gameObj->m_NetObj != NULL && gameObj->m_NetObj->IsAuthoritve())
    {
        nsynEvtGame evt(
            nsynEvtGame::EVT_EVENT_RESET_OBJECT,
            inst->GetMatrix());

        m_NetObjMngr.BroadcastFor(evt,gameObj->m_NetId);
    }

    gameObj->NotifyDeactivate();
}

void gameInstance::ReleaseOwnership ( phInst* inst )
{ 
    //slow but we don't do this enough to worry about it, if we did we can use our own
    //type checking..
    SimpleGameObj* gameObj = dynamic_cast<SimpleGameObj*>(inst);

    if (gameObj==NULL)
    {
        return;
    }

    if (gameObj->m_NetObj != NULL)
    {
        gameObj->m_NetObj->ReleaseOwnership();
    }

    return;
}
//alternate take control, we dont increase the prioirty..
bool  gameInstance::TakeControlAlt ( phInst* inst )
{  
    if (!m_CanTakeControl)
    {
        return false;
    }

    //slow but we don't do this enough to worry about it, if we did we can use our own
    //type checking..
    SimpleGameObj* gameObj = dynamic_cast<SimpleGameObj*>(inst);

    if (gameObj==NULL)
    {
        return false;
    }

    if (gameObj->m_NetObj!=NULL && !gameObj->m_NetObj->IsAuthoritve())
    {    
        m_NetObjMngr.AcquireAuthority(gameObj->m_NetId);
        m_NetObjMngr.ActivateController(gameObj->m_NetObj->GetToken());
    }

    return true;
}

bool gameInstance::RecordApplyForce  (int levelIndex, const Vector3& force, const Vector3& position, int component)
{
    //slow but we don't do this enough to worry about it, if we did we can use our own
    //type checking..
    SimpleGameObj* gameObj = dynamic_cast<SimpleGameObj*>(PHLEVEL->GetInstance(levelIndex));

    if (gameObj == NULL)
    {
        return false;
    }

    if (gameObj->m_NetObj==NULL)
    {
        return false;
    }

    if (gameObj->m_NetObj->CanAcquireAuthority() || gameObj->m_NetObj->IsAuthoritve())
    {
        return false;
    }

    nsynEvtPhysicsApplyRemote event( NET_PHY_APPLY_FORCE, levelIndex, force, position, component );

    gameObj->m_NetObj->SendToOwner(event);

    return true;
}

bool gameInstance::RecordApplyImpulse (int levelIndex, const Vector3& impulse, const Vector3& position, int component)
{
    //slow but we don't do this enough to worry about it, if we did we can use our own
    //type checking..
    SimpleGameObj* gameObj = dynamic_cast<SimpleGameObj*>(PHLEVEL->GetInstance(levelIndex));

    if (gameObj == NULL)
    {
        return false;
    }

    if (gameObj->m_NetObj==NULL)
    {
        return false;
    }

    if (gameObj->m_NetObj->CanAcquireAuthority() || gameObj->m_NetObj->IsAuthoritve())
    {
        return false;
    }

    nsynEvtPhysicsApplyRemote event( NET_PHY_APPLY_IMUPLSE, levelIndex, impulse, position, component );

    gameObj->m_NetObj->SendToOwner(event);

    return true;
}


void gameInstance::RegisterGameObject( SimpleGameObj* gameObj )
{
    if (gameObj==NULL)
    {
        return;
    }

    m_GameObjects.Insert(gameObj->m_NetId,gameObj);
}

rage::nsynBase* gameInstance::BindGameObjectToNet ( rage::nsynObjId id )
{
    return m_NetObjMngr.CreateSyncObject( id );
}

void gameInstance::DeleteObject	( SimpleGameObj* /*object*/ )
{
    //TODO
}

SimpleGameObj* gameInstance::GetSimpleGameObject ( rage::nsynObjId id )
{
    SimpleGameObjSPtr* gameObj =  m_GameObjects.Access(id);

    if (gameObj==NULL)
    {
        return NULL;
    }

    return *gameObj;
}
