// 
// /swap_bink_bytes.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 
#include "file/stream.h"

void Swap_u32(unsigned *buffer,int count) {
	while (count--) {
		unsigned v = *buffer;
		*buffer++ = (v>>24)|(v<<24)|((v>>8)&0xFF00)|((v&0xFF00)<<8);
	}
}

int main(int argc, char **argv)
{
	using namespace rage;

	if (argc != 3) {
		printf("usage: swap_bink_bytes infile outfile\n");
		return 1;
	}
	fiStream *S = fiStream::Open(argv[1]);
	if (!S) {
		printf("cannot open input file\n");
		return 1;
	}
	int fileSize = S->Size();
	char *bytes = new char[fileSize];
	S->Read(bytes, fileSize);
	S->Close();

	Swap_u32((unsigned*)bytes,fileSize/4);

	S = fiStream::Create(argv[2]);
	if (!S) {
		printf("cannot create output file\n");
		return 1;
	}
	S->Write(bytes, fileSize);
	S->Close();

	return 0;
}