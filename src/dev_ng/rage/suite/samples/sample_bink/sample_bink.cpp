// 
// /sample_bink.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// We don't patch bink into our filesystem support, so bink movies must be accessible
// by the underlying console's native filesystem, not in an archive or over RFS.
// This app will work whether or not RFS is disabled because we use explicit paths.
// #define DISABLE_RFS

#define SIMPLE_HEAP_SIZE (64 * 1024) 

#include "system/main.h"
#include "system/param.h"
#include "file/asset.h"
#include "sample_rmcore/sample_rmcore.h"
#include "system/xtl.h"

#include "bink/movie.h"

using namespace rage;

PARAM(file, "Movie to play");

class grcSampleBink : public ragesamples::rmcSampleManager
{
public:
	grcSampleBink()
	{
		m_pMovie = 0;
		m_DrawBackground = false;
	}

	~grcSampleBink()
	{
	}

	void InitClient()
	{
		// Open the movie
#if __XENON
		const char* pMovie = "game:\\france_flythru.bik";
#elif __PPU
		const char* pMovie = "/app_home/t:/rage/assets/sample_bink/france_flythru.bik";
#else
		const char* pMovie = "t:\\rage\\assets\\sample_bink\\france_flythru.bik";
#endif
		PARAM_file.Get(pMovie);
		
		m_pStream = ASSET.Open(pMovie, "bik");
		if (!m_pStream)
			Quitf("Unable to open sample movie '%s'",pMovie);
		m_pMovie = bwMovie::Create();			
		m_pMovie->SetMovie(m_pStream);

		m_pMovie->SetRect(Vector3(-2.0f, 2.0f, 0.0f), Vector3(2, 2, 0.0f), Vector3(-2.0f, -2.0f, 0.0f), Vector3(2.0f, -2.0f, 0.0f));
		m_pMovie->Play();
	}

	void UpdateClient()
	{
		if( m_pMovie )
		{
			// Update the movie
			m_pMovie->Update();
		}
	}

	void DrawClient()
	{
		if( m_pMovie )
		{
			grcViewport::SetCurrentWorldIdentity();

			// Draw the movie
			m_pMovie->Draw();

			// Close the movie if its done and exit
			//m_WantsExit = !m_pMovie->IsPlaying();
		}
	}

	void ShutdownClient()
	{
		delete m_pMovie;
		m_pStream->Close();
	}

protected:
	fiStream*		m_pStream;
	bwMovie*		m_pMovie;
};

int Main()
{
	grcSampleBink sample;
	sample.Init("sample_bink");
	sample.UpdateLoop();
	sample.Shutdown();
	return 0;
}
