//
// sample_cranimation/sample_anim.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: Animation
// PURPOSE:
//		This sample shows how to load an animation, pose a skeleton, and render a skinned model.


using namespace rage;


#include "atl/creator.h"
#include "atl/functor.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/animplayer.h"
#include "crskeleton/skeletondata.h"
#include "event/instance.h"
#include "event/player.h"
#include "event/timeline.h"
#include "event/type.h"
#include "event/manager.h"
#include "eventtypes/debug.h"
#include "eventtypes/sound.h"
#include "parser/manager.h"
#include "profile/profiler.h"
#include "grprofile/drawmanager.h"
#include "rmcore/drawable.h"
#include "sample_cranimation/sample_cranimation.h"
#include "sample_rmcore/sample_rmcore.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

#define DEFAULT_ANIM "T:\\rageassets\\assets\\animation\\motion\\male_wlk_m.anim"
PARAM(anim,"[sample_anim] Animation file to load (default is \"" DEFAULT_ANIM "\")");

class evtSampleAnimation : public ragesamples::craSampleManager
{
public:
	evtSampleAnimation() : ragesamples::craSampleManager()
	{
	}

protected:
	void InitClient()
	{
		GetRageProfiler().Init();

		INIT_PARSER;
		INIT_EVENTMGR;

		evtAnimPlayer::RegisterPlayerClass();

		EVENTMGR.RegisterTypeInstancePair<evtTypePrintf, evtInstancePrintf>("Debug", "printf");
		EVENTMGR.RegisterTypeInstancePair<evtTypeDrawTick, evtInstanceDrawTick>("Debug", "Draw Tick Mark");
		EVENTMGR.RegisterTypeInstancePair<evtTypeAudOneShot, evtInstanceAudOneShot>("AudOneShot", "AudOneShot");

		REGISTER_PARSABLE_CLASS(evtTimeline);

		m_EventPlayer = rage_new evtAnimPlayer;
		m_EventPlayer->CreateParameterList();

		craSampleManager::InitClient();

		// STEP #1. Load our animation.
		const char *animName = DEFAULT_ANIM;
		PARAM_anim.Get(animName);

		// -- Call <c>crAnimation::AllocateAndLoad()</c> with the animation name and it'll
		// return you a loaded animation.
		m_Animation=crAnimation::AllocateAndLoad(animName);
        Assert( m_Animation );
		m_AnimPlayer=rage_new crAnimPlayer(m_Animation);
		m_AnimPlayer->SetLooped(true, true);

		ASSET.PushFolder("$/../../sample_event");
		//PARSER.LoadObjectPtr(animName, "evt", m_EventTimeline);
		ASSET.PopFolder();
		m_EventTimeline = rage_new evtTimeline;
		m_EventTimeline->SortByTime();

		Assert(m_EventTimeline);
		bool controlPlayhead = true; // Since we only have one player, it should control the playhead
		m_EventPlayer->SetTimeline(*m_EventTimeline, controlPlayhead);
		m_EventPlayer->GetSkeleton() = const_cast<const crSkeleton*>(&GetSkeleton());

#if __BANK
		bkBank& eventBank=bkManager::GetInstance().CreateBank("Event Bank");

		PARSER.AddWidgets(eventBank, m_EventTimeline);
		m_EventTimeline->AddWidgets(eventBank);

		m_EventTimeline->SetMaxTime(m_Animation->Convert30FrameToTime(m_Animation->GetNum30Frames()-1));

		eventBank.AddButton("Save Timeline", datCallback(MFA(evtSampleAnimation::SaveTimeline), this));

		bkBank& profBank=bkManager::GetInstance().CreateBank("Profile");
		GetRageProfiler().AddWidgets(profBank);
#endif

		m_EventPlayer->Start(0.0f);

		// STEP #2. Initialize our animation frame.
		
	
		//-- If we need to get a single frame from the animation, we must have a data
		// structure that will store it.  We create an instance of <c>crFrame</c>
		// to store this data.
		m_Frame=rage_new crFrame;

		// -STOP

		if( m_Animation )
		{
			//-- Initialize the animation frame based on the degrees of freedom
			// found in the skeleton.
			m_Frame->InitCreateBoneAndMoverDofs(GetSkeleton().GetSkeletonData(), false);

			m_Frame->IdentityFromSkel(GetSkeleton().GetSkeletonData());

			// -STOP
		}
	}

	void SaveTimeline()
	{
		const char *animName = DEFAULT_ANIM;
		PARAM_anim.Get(animName);
		ASSET.PushFolder("$/../../sample_event");
		PARSER.SaveObject(animName, "evt", m_EventTimeline);
		ASSET.PopFolder();
	}

	void ShutdownClient()
	{
		delete m_AnimPlayer;
		delete m_Animation;
		delete m_Frame;
		delete m_EventPlayer;
		delete m_EventTimeline;

		ragesamples::craSampleManager::ShutdownClient();

		SHUTDOWN_EVENTMGR;
		SHUTDOWN_PARSER;

		GetRageProfiler().Shutdown();
	}

	void UpdateClient()
	{	
#if __PROFILE
		GetRageProfiler().UpdateInput();
		GetRageProfiler().BeginFrame();
#endif
#if __PFDRAW
		GetRageProfileDraw().Begin();
#endif

		if (m_Animation)
		{
			// STEP #3. Request frame data from the animation.
			m_AnimPlayer->Composite(*m_Frame);
			
			// STEP #4. Pose the skeleton.

			//-- Now we take the frame data and pose the bones in the skeleton.  To do this, just pass the 
			// a reference to the skeleton to be posed.
			m_Frame->Pose(GetSkeleton());

			// -STOP
		}
		else
		{
			GetSkeleton().Reset();
		}
		
		// STEP #5. Compute the global matrices of the skeleton bones.

		//-- Updating the skeleton will compute the global matrices which will be used for rendering the skinned object.
		GetSkeleton().Update();
		UpdateMatrixSet();

		// -STOP

		if (GetPlay())
		{
			// STEP #6. Goto the next frame.

			//-- Now we goto the next frame.  We do this by multiplying the game clock with the playback rate.
			m_AnimPlayer->SetRate(GetPlaybackRate());
			m_AnimPlayer->Update(TIME.GetSeconds());

			m_EventPlayer->GetPosition() = ORIGIN;
			m_EventPlayer->GetAnimBlendAmout() = 1.0f;
			m_EventPlayer->GetAnimBlendSpeed() = 0.0f;
			m_EventPlayer->GetAnimSpeed() = GetPlaybackRate();
			m_EventPlayer->GetDrawable() = &GetDrawable();

			m_EventPlayer->Update(m_AnimPlayer->GetTime(), true);

			// -STOP
		}

#if __PFDRAW
		GetRageProfileDraw().End();
#endif
#if __PROFILE
		GetRageProfiler().EndFrame();
#endif

	}

	void DrawClient()
	{
		craSampleManager::DrawClient();

#if __PFDRAW
		GetRageProfileDraw().Render();
#endif
#if __PROFILE
		GetRageProfiler().Draw();
		GetRageProfiler().Update();
#endif
	}

private:
		crAnimPlayer*		m_AnimPlayer;
		crAnimation*			m_Animation;
		crFrame*			m_Frame;
		evtTimeline*		m_EventTimeline;
		evtAnimPlayer*			m_EventPlayer;
};

// main application
int Main()
{
	evtSampleAnimation sampleCra;
	sampleCra.Init("sample_rmcore\\drawable");

	sampleCra.UpdateLoop();

	sampleCra.Shutdown();

	return 0;
}
