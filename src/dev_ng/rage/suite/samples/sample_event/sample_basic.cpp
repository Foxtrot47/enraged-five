// 
// /sample_basic.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "atl/creator.h"
#include "atl/functor.h"
#include "event/instance.h"
#include "event/manager.h"
#include "event/player.h"
#include "event/timeline.h"
#include "event/type.h"
#include "eventtypes/debug.h"
#include "eventtypes/sound.h"
#include "parser/manager.h"
#include "system/main.h"

#include "atl/string.h"

using namespace rage;

int Main()
{
	INIT_PARSER;
	INIT_EVENTMGR;

	EVENTMGR.RegisterTypeInstancePair<evtTypePrintf, evtInstancePrintf>("Debug", "printf");
	EVENTMGR.RegisterTypeInstancePair<evtTypeAudOneShot, evtInstanceAudOneShot>("AudOneShot", "AudOneShot");

	REGISTER_PARSABLE_CLASS(evtInstance);
	REGISTER_PARSABLE_CLASS(evtTimeline);
	REGISTER_PARSABLE_CLASS(evtInstancePrintf);
	REGISTER_PARSABLE_CLASS(evtInstanceAudOneShot);

	evtTimeline myTimeline;
	PARSER.LoadObject("sampletimeline", "xml", myTimeline);
	myTimeline.SortByTime();

	evtPlayer myPlayer;
	myPlayer.SetTimeline(myTimeline);

	myPlayer.Start(2.0f);
	myPlayer.Update(2.0f);
	myPlayer.Update(2.0f);
	myPlayer.Stop(2.0f);

	printf("\n--------------- Forwards --------------\n\n");

	myPlayer.Start(0.0f);
	float lastT = 0.0f;
	for(float t = 0.0f; t <= 10.0f; t += 0.5f) {
		printf("start frame %.5f->%.5f\n", lastT, t);
		myPlayer.Update(t);
		printf("end frame %.5f->%.5f\n\n", lastT, t);
		lastT = t;
	}
	myPlayer.Stop(10.0f);

	printf("\n--------------- Backwards --------------\n\n");

	myPlayer.Start(10.0f);
	lastT = 10.0f;
	for(float t = 10.0f; t >= 0.0f; t -= 0.5f) {
		printf("start frame %.5f->%.5f\n", lastT, t);
		myPlayer.Update(t);
		printf("end frame %.5f->%.5f\n\n", lastT, t);
		lastT = t;
	}
	myPlayer.Stop(0.0f);

  	PARSER.SaveObject("outtimeline", "rbf", &myTimeline);

	SHUTDOWN_EVENTMGR;
	SHUTDOWN_PARSER;

//	delete printInstance;
//	delete sndInstance;

	return 0;
}

