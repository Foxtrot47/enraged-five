set TESTERS=sample_expressions
set SAMPLE_LIBS=sample_rmcore sample_cranimation sample_physics sample_crfragment sample_motiontree %RAGE_SAMPLE_LIBS%
set LIBS=%SAMPLE_LIBS% %RAGE_CORE_LIBS% %RAGE_GFX_LIBS%
set LIBS=%LIBS% %RAGE_CR_LIBS% %RAGE_SUITE_CR_LIBS% curve 
set LIBS=%LIBS% %RAGE_PH_LIBS% breakableglass phglass fragment cloth grrope event pheffects spatialdata
set XPROJ=%RAGE_DIR%/base/src %RAGE_DIR%/base/samples %RAGE_DIR%/suite/src %RAGE_DIR%/suite/samples
