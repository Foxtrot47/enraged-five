// 
// /sample_expressions.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Expression system sample
// PURPOSE: Used to test expression system


#include "sample_motiontree/sample_motiontree.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/bkvector.h"
#include "bank/slider.h"
#include "cranimation/animation.h"
#include "cranimation/frameiterators.h"
#include "creature/creature.h"
#include "creature/componentextradofs.h"
#include "crextra/expressions.h"
#include "crmotiontree/requestanimation.h"
#include "crmotiontree/requestcapture.h"
#include "crmotiontree/requestexpression.h"
#include "crmotiontree/requestframe.h"
#include "crmotiontree/requestmerge.h"
#include "system/main.h"
#include "system/param.h"

namespace rage
{
	datCallback g_ClothManagerCallback = NullCB;
}

using namespace rage;

#define DEFAULT_FILE "$/expressions/victor/vict_form/entity.type"
XPARAM(file);

#define DEFAULT_ANIM "$/expressions/victor/vict_form/vict_form_face"
#define DEFAULT_EXPR "$/expressions/victor/vict_form/victor"

PARAM(anim,"[sample_expressions] Animation file to load (default is \"" DEFAULT_ANIM "\")");
PARAM(expr,"[sample_expressions] Expression file to load (default is \"" DEFAULT_EXPR "\")");
PARAM(exprrsc,"[sample_expressions] Expression resource file to load");

namespace ragesamples 
{

#define DEFAULT_LIGHTPRESETS "$/tempTestAssets/MC4_Karol/MC4_Karol.lgroup"
XPARAM(lightpresets);

#define DEFAULT_CAMPRESETS "$/expressions/camera.camset"
XPARAM(campresets);


class crExpressionsSample : public ragesamples::crMotionTreeSampleManager
{
public:
	crExpressionsSample() 
		: ragesamples::crMotionTreeSampleManager()
		, m_Animation(NULL)
		, m_Expressions(NULL)
	{
	}

protected:

	void InitClient()
	{
		if (!PARAM_lightpresets.Get())
		{
			PARAM_lightpresets.Set(DEFAULT_LIGHTPRESETS);
		}

		if (!PARAM_campresets.Get())
		{
			PARAM_campresets.Set(DEFAULT_CAMPRESETS);
		}

		crMotionTreeSampleManager::InitClient();

		crExpressions::InitClass();

		const char* animName = DEFAULT_ANIM;
		if(PARAM_anim.Get())
		{
			PARAM_anim.Get(animName);
		}

		if(animName && animName[0])
		{
			m_Animation = crAnimation::AllocateAndLoad(animName,NULL,true);
			if(!m_Animation)
			{
				Quitf("ERROR - failed to load animation file '%s'", animName);
			}
		}
	
		// load expression file
		const char *exprRsc = NULL;
		PARAM_exprrsc.Get(exprRsc);
		if(exprRsc)
		{
			datResourceMap map;
			datResourceInfo hdr;
			m_Expressions = (crExpressions*) pgRscBuilder::LoadBuild(exprRsc,"#ex",crExpressions::RORC_VERSION,map,hdr);
			datResource rsc(map,exprRsc);
			m_Expressions->Place(m_Expressions,rsc);
		}
		else
		{
			const char* exprName = DEFAULT_EXPR;
			PARAM_expr.Get(exprName);
			m_Expressions = crExpressions::AllocateAndLoad(exprName, true);
		}

		if(!m_Expressions)
		{
			Quitf("ERROR - failed to load expression file");
		}
		m_Expressions->AddRef();
		m_Expressions->InitializeCreature(GetCreature());
		GetCreature().Refresh();
		GetMotionTree().Refresh();

		GetCreature().InitDofs(m_FrameData);
		crCreatureComponentExtraDofs* componentExtraDofs = static_cast<crCreatureComponentExtraDofs*>(GetCreature().FindComponent(crCreatureComponent::kCreatureComponentTypeExtraDofs));
		if(componentExtraDofs)
		{
			componentExtraDofs->GetIdentityFrame().Zero();
		}

		m_FrameIn = rage_new crFrame();
		m_FrameOut = rage_new crFrame();
		m_FrameIn->Init(m_FrameData);
		m_FrameOut->Init(m_FrameData);

		crmtRequestFrame reqFrame(m_FrameIn);
		crmtRequestAnimation reqAnim(m_Animation);
		reqAnim.SetLooping(true, true);
		crmtRequestMerge reqMerge(reqFrame, reqAnim);
		crmtRequestExpression reqExpression(reqMerge, m_Expressions);
		crmtRequestCapture reqCapture(reqExpression, m_FrameOut);

		GetMotionTree().Request(reqCapture);

#if __BANK
		AddWidgetsExpressions();
#endif // __BANK

		GetMotionTree().Dump();
		//m_Expressions->Dump();
	}

	void ShutdownClient()
	{
		if(m_Animation)
		{
			delete m_Animation;
			m_Animation = NULL;
		}
		if(m_Expressions)
		{
			delete m_Expressions;
			m_Expressions = NULL;
		}

		delete m_FrameIn;
		delete m_FrameOut;

		crExpressions::ShutdownClass();

		crMotionTreeSampleManager::ShutdownClient();
	}

	void UpdateClient()
	{
#if __BANK
		UpdateWidgetsExpressions();
#endif // __BANK

		crMotionTreeSampleManager::UpdateClient();
	}

#if __BANK
	void AddWidgetsExpressions()
	{
		bkBank& bank = BANKMGR.CreateBank("Expressions", 50, 500);
		bank.AddButton("Reset Input", datCallback(MFA(crExpressionsSample::ResetWidgetsExpressions),this));
		
		bank.PushGroup("Input frame");
		m_FrameInWidgets.Init(*m_FrameIn, &GetSkeleton().GetSkeletonData());
		m_FrameInWidgets.AddWidgets(bank);
		bank.PopGroup();

		bank.PushGroup("Output frame");
		m_FrameOutWidgets.Init(*m_FrameOut, &GetSkeleton().GetSkeletonData());
		m_FrameOutWidgets.AddWidgets(bank);
		bank.PopGroup();

		ResetWidgetsExpressions();
	}

	void UpdateWidgetsExpressions()
	{
		m_FrameInWidgets.UpdateFrame();

		m_FrameOutWidgets.ResetWidgets();
	}

	void ResetWidgetsExpressions()
	{
		GetCreature().Identity(*m_FrameIn);

		m_FrameInWidgets.ResetWidgets();
		if(m_Animation)
		{
			m_FrameIn->Invalidate();

			m_FrameInWidgets.ResetWidgets();
		}
	}


	FrameEditorWidgets m_FrameInWidgets;
	FrameEditorWidgets m_FrameOutWidgets;
#endif // __BANK

	void InitFragment()
	{
	}

	void InitMover()
	{
	}

	const char* GetDefaultFile()
	{
		const char* defaultFile = DEFAULT_FILE;
		PARAM_file.Get(defaultFile);
		return defaultFile;
	}

private:
	crAnimation* m_Animation;
	crExpressions* m_Expressions;
	crFrame* m_FrameIn;
	crFrame* m_FrameOut;
	crFrameData m_FrameData;
};

} // namespace ragesamples

// main application
int Main()
{
	ragesamples::crExpressionsSample sampleExpressions;
	sampleExpressions.Init("expressions");

	sampleExpressions.UpdateLoop();

	sampleExpressions.Shutdown();

	return 0;
}



