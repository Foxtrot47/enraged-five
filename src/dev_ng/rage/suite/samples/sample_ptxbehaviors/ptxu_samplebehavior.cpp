// 
// rmptfx/ptxu_samplebehavior.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "ptxu_samplebehavior.h"

#include "ptxu_samplebehavior_parser.h"

#include "rmptfx/ptxbehavior.h"
#include "rmptfx/ptxdraw.h"
#include "rmptfx/ptxemitrulestd.h"
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxproplist.h"
#include "rmptfx/ptxrule.h"
#include "parser/visitorutils.h"
#include "system/cache.h"
#include "vector/geometry.h"
#include "vectormath/classes.h"

// Non-SPU #includes the .frag.
#if !__SPU
#include "ptxu_samplebehavior.frag"
#include "ptxu_samplebehavior_load.frag"
#endif


namespace rage
{
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if __RMPTFX_INTERFACE
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Keyframe UI information
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	ptxKeyframeDefn ptxu_SampleBehavior::sm_UIKeyframeMember(
		"Sample KF", ptxKeyframe::KF_VECTOR4,
		-10000.0f, 10000.0f, 0.01f,
		0.0f, 1.0f, 0.01f,
		"X", "Y", "Z", "W"
		);
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif //__RMPTFX_INTERFACE
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	ptxu_SampleBehavior::ptxu_SampleBehavior()
	{
		RMPTFX_INIT_KEYFRAME(ptxu_SampleBehavior,m_KeyframeMember,ScalarV(V_ONE),sm_UIKeyframeMember);
		m_IntMember = 1;
		m_FloatMember = 0.0f;
		m_BoolMember = true;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Resourcing

	ptxu_SampleBehavior::ptxu_SampleBehavior(datResource& rsc)
		: ptxBehavior(rsc)
		, m_KeyframeMember(rsc)
	{
	}

#if __DECLARESTRUCT
	void ptxu_SampleBehavior::DeclareStruct(datTypeStruct& s)
	{
		ptxBehavior::DeclareStruct(s);
		SSTRUCT_BEGIN_BASE(ptxu_SampleBehavior, ptxBehavior)
			SSTRUCT_FIELD(ptxu_SampleBehavior, m_KeyframeMember)
			SSTRUCT_FIELD(ptxu_SampleBehavior, m_IntMember)
			SSTRUCT_FIELD(ptxu_SampleBehavior, m_FloatMember)
			SSTRUCT_FIELD(ptxu_SampleBehavior, m_BoolMember)
			SSTRUCT_IGNORE(ptxu_SampleBehavior, m_Pad)
		SSTRUCT_END(ptxu_SampleBehavior)
	}
#endif

	IMPLEMENT_PLACE(ptxu_SampleBehavior);

	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ptxu_SampleBehavior::SetUpdateFuncParams(ptxBehaviorUpdateFunc& update)
	{
		// We know this.
#if !__PS3
		update.pUpdateFunc = &StaticBehaviorFuncs::ptxu_SampleBehavior_UpdatePoint;
		update.pLoadFunc = &StaticBehaviorFuncs::ptxu_SampleBehavior_Load;
#else
		update.pUpdateFunc = (updateFunc_t*)(FRAG_INTERFACE_START(ptxu_samplebehavior));
		update.pLoadFunc = (loadFunc_t*)(FRAG_INTERFACE_START(ptxu_samplebehavior_load));
#if __PPU
		update.pUpdateFuncPPU = &StaticBehaviorFuncs::ptxu_SampleBehavior_UpdatePoint;
#endif
#endif
		update.behavDataSize = sizeof( ptxu_SampleBehavior );

		// These last 2 are only needed on PS3, for DMA purposes.
#if __PS3
		update.updateFuncSize = (u32)FRAG_INTERFACE_SIZE(ptxu_samplebehavior);
		update.loadFuncSize = (u32)FRAG_INTERFACE_SIZE(ptxu_samplebehavior_load);
#endif
	}

	void ptxu_SampleBehavior::ResetPropBiasLinks(atArray<ptxBiasLink>& /*rBiasLinks*/)
	{
	}

#if __RMPTFX_INTERFACE
	void ptxu_SampleBehavior::WriteTuneableDefToByteBuffer(ptxByteBuff& buff)
	{
		ptxBehavior::WriteBaseDefToByteBuffer(buff);
		buff.Write_u8(4);
		ptxBehavior::WriteInt(buff,"Sample Int",0,10);
		ptxBehavior::WriteFloat(buff,"Sample Float",-10.0f,10.0f);
		buff.Write_const_char("bool");
		buff.Write_const_char("Sample Bool");
		buff.Write_const_char("keyframe");
		buff.Write_const_char("Sample KF");
	}
	void ptxu_SampleBehavior::SetUiData()
	{
		m_KeyframeMember.m_Keyframe.SetUiData(&sm_UIKeyframeMember);
	}
	void ptxu_SampleBehavior::WritePropsToByteBuffer( ptxByteBuff& buff )
	{
		ptxBehavior::WriteBasePropsToByteBuffer(buff);

		SetUiData();
		buff.Write_s32(m_IntMember);
		buff.Write_float(m_FloatMember);
		buff.Write_bool(m_BoolMember);
		m_KeyframeMember.WriteToByteBuffer(buff);
	}
	void ptxu_SampleBehavior::ReadPropsFromByteBuffer( const ptxByteBuff& buff )
	{
		m_IntMember = buff.Read_s32();
		m_FloatMember = buff.Read_float();
		m_BoolMember = buff.Read_bool();
		m_KeyframeMember.ReadFromByteBuffer(buff);
	}

	bool ptxu_SampleBehavior::Validate()
	{
		//See if it has been tuned
		// Copy the behavior, downcast.
		ptxu_SampleBehavior* pBehav = (ptxu_SampleBehavior*)RMPTFXMGR.GetBehavior(this->GetID());

		return !(*pBehav == *this);
	}
#endif // __RMPTFX_INTERFACE

} // namespace rage

