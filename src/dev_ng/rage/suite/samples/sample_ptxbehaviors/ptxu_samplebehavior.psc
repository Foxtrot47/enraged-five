<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<autoregister allInFile="true"/>

<structdef base="::rage::ptxBehavior" type="::rage::ptxu_SampleBehavior">
<struct name="m_KeyframeMember" type="::rage::ptxKeyframeProp"/>
<int name="m_IntMember"/>
<float name="m_FloatMember"/>
<bool name="m_BoolMember"/>
</structdef>

</ParserSchema>