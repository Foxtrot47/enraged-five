// 
// rmptfx/ptxu_samplebehavior.frag 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXU_SAMPLEBEHAVIOR_FRAG
#define RMPTFX_PTXU_SAMPLEBEHAVIOR_FRAG 

#if __SPU
#define __SPUFRAG 1
#include <spu_printf.h>
#include "vector/vector3_consts_spu.cpp"
using namespace rage;
#endif // __SPU

#include "ptxu_samplebehavior.h"
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxrule.h"
#include "rmptfx/ptxbehaviorcommon.h"

namespace rage
{
namespace StaticBehaviorFuncs
{
	// ptxu_SampleBehavior
 	BEHAVIOR_FUNCTION(
		ptxu_SampleBehavior_UpdatePoint,
		void* /*pData*/,
		ptxUpdateBehaviorData* /*pUpdateData*/,
		_PointData* RESTRICT /*pPointData*/,
		ptxEffectInst* RESTRICT /*pEffectInst*/,
		ptxEmitterInst* RESTRICT /*pEmitterInst*/,
		ScalarV_In /*dt*/	)
	{
		// ... do something useful...
	}

} // StaticBehaviorFuncs
} // namespace rage

#endif // RMPTFX_PTXU_SAMPLEBEHAVIOR_FRAG 
