// 
// rmptfx/ptxu_samplebehavior.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXU_SAMPLEBEHAVIOR_H 
#define RMPTFX_PTXU_SAMPLEBEHAVIOR_H 

#include "rmptfx/ptxbehavior.h"
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxrule.h"
#include "rmptfx/ptxshader.h"
#include "parser/manager.h"

#if __PPU
DECLARE_FRAG_INTERFACE(ptxu_samplebehavior);
DECLARE_FRAG_INTERFACE(ptxu_samplebehavior_load);
#endif // __PSN

namespace rage
{


	class ptxu_SampleBehavior : public ptxBehavior
	{
	public:
		ptxu_SampleBehavior();
		const char* GetID()	{ return "ptxu_SampleBehavior"; }

		float GetOrder() { return BEHORDER_SAMPLEBEHAVIOR; }

		bool NeedsToInit()				{ return false; }
		bool NeedsToUpdate()			{ return true; }
		bool NeedsToDraw()				{ return false; }
		bool NeedsToRelease()			{ return false; }

		void SetUpdateFuncParams(ptxBehaviorUpdateFunc& update);

		void ResetPropBiasLinks(atArray<ptxBiasLink>& rBiasLinks);

		explicit ptxu_SampleBehavior(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_SampleBehavior);

#if __RMPTFX_INTERFACE
		const char * GetUIName(){return "Non-RMPTFX Sample Behavior";}
		bool Validate();
		void SetUiData();
		void WritePropsToByteBuffer( ptxByteBuff& buff );
		void ReadPropsFromByteBuffer( const ptxByteBuff& buff );
		void WriteTuneableDefToByteBuffer( ptxByteBuff& buff );
		bool operator==( const ptxu_SampleBehavior& other )
		{
			return
				(m_KeyframeMember == other.m_KeyframeMember)		&&
				(m_IntMember == other.m_IntMember)					&&
				(m_FloatMember == other.m_FloatMember)				&&
				(m_BoolMember == other.m_BoolMember)				;
		}
		// Data specific to this behavior (flags/keyframes)
		static ptxKeyframeDefn sm_UIKeyframeMember;
#endif
	public:
		// Data specific to this behavior (flags/keyframes)
		ptxKeyframeProp		m_KeyframeMember;
		int					m_IntMember;
		float				m_FloatMember;
		bool				m_BoolMember;

		datPadding<7>		m_Pad;

	public:
		PAR_PARSABLE;
	};

} // namespace rage

#endif // RMPTFX_PTXU_SAMPLEBEHAVIOR_H 
