// 
// rmptfx/ptxu_samplebehavior_load.frag 
// 
// Copyright (C) 1999-2008 Rockstar Games. All Rights Reserved.
// 

#ifndef RMPTFX_PTXU_SAMPLEBEHAVIOR_LOAD_FRAG 
#define RMPTFX_PTXU_SAMPLEBEHAVIOR_LOAD_FRAG



#if __SPU
#define __SPUFRAG 1
#include <spu_printf.h>
#include "vector/vector3_consts_spu.cpp"
using namespace rage;
#endif // __SPU

#include "ptxu_samplebehavior.h"

#include "rmptfx/ptxbehaviorfrag.h"
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxspuscratchpad.h"

namespace rage
{
namespace StaticBehaviorFuncs
{

	BEHAVIORFRAG_DECL(
		void, ptxu_SampleBehavior_Load,
		void* pData,
		ptxSpuUtilFuncs* pUtilFuncs,
		ptxLoadBehaviorData* pLoadData,
		ptxEffectInst* pEffectInst,
		ptxScratchPad& scratch
		);

	BEHAVIORFRAG_IMPL(
		void, ptxu_SampleBehavior_Load,
		void* pData,
		ptxSpuUtilFuncs* pUtilFuncs,
		ptxLoadBehaviorData* pLoadData,
		ptxEffectInst* /*pEffectInst*/,
		ptxScratchPad& scratch
		)
	{
		InitLoad(pLoadData);
		ptxSpuLoadInfo info(scratch, pLoadData->m_EvoList);
		
		// Downcast to the type we know it is.
		ptxu_SampleBehavior* pObj = (ptxu_SampleBehavior*)(pData);

		// Load KF's.
		(* pUtilFuncs->kfLoader )( &pObj->m_KeyframeMember, info );
	}






} // StaticBehaviorFuncs
} // namespace rage

#endif // RMPTFX_PTXUPDATEBEH_MATRIXWEIGHT_LOAD_FRAG 
