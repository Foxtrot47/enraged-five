// 
// sample_motiontree/sample_mt_physical.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Secondary motion tree sample
// PURPOSE: This sample demonstrates of a physical expression.  


#include "sample_motiontree.h"

#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "creature/componentphysical.h"
#include "creature/creature.h"
#include "crskeleton/skeletondata.h"
#include "crextra/expressions.h"
#include "crextra/expressionopmacros.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeanimation.h"
#include "crmotiontree/requestanimation.h"
#include "crmotiontree/requestexpression.h"
#include "phcore/phmath.h"
#include "rmcore/drawable.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/colors.h"

using namespace rage;

namespace rage { datCallback g_ClothManagerCallback = NullCB; };

#define DEFAULT_FILE "$/sample_cranimation/mp3skelnew/entity.type"
#define DEFAULT_ANIM "$/animation/pm/walktotarget/walk_target_1.anim" // "$/animation/pm/step/step_high.anim"
#define DEFAULT_EXPR "$/sample_creature/secondarymotion/wobble.expr"

PARAM(expr, "Expression file to load (default is \"" DEFAULT_EXPR "\")");
PARAM(anim, "Animation file to load (default is \"" DEFAULT_ANIM "\")");
XPARAM(file);

const Vector3 OBJECT_DIMENSION(0.1f, 0.5f, 0.1f);

namespace ragesamples 
{

XPARAM(defaultcamera);
XPARAM(zup);

class crSecondaryMotionSample : public ragesamples::crMotionTreeSampleManager
{
public:
	crSecondaryMotionSample() 
		: ragesamples::crMotionTreeSampleManager()
		, m_Animation(NULL)
		, m_Expressions(NULL)
		, m_HasLooped(false)
	{
	}

protected:

	void InitClient()
	{
		crMotionTreeSampleManager::InitClient();
		crExpressions::InitClass();

		PARAM_defaultcamera.Set("MAYA");
		SetZUp(true);
		
		const char* animName = DEFAULT_ANIM;
		if(PARAM_anim.Get())
		{
			PARAM_anim.Get(animName);
		}

		if(animName && animName[0])
		{
			m_Animation = crAnimation::AllocateAndLoad(animName, NULL, true);
			if(!m_Animation)
			{
				Quitf("ERROR - failed to load animation file '%s'", animName);
			}
		}

		LoadExpressions();

		crmtRequestAnimation reqAnim(m_Animation);
		reqAnim.SetLooping(true, true);

		crmtRequestExpression reqExpression(reqAnim, m_Expressions);

		GetMotionTree().Request(reqExpression);

		crmtObserverFunctor* observer = rage_new crmtObserverFunctor;

		// - Attach the newly created observer to the node created by the request.
		observer->Attach(reqAnim.GetObserver());

		// - Register a callback function to process the animation looped event.
		observer->RegisterFunctor(crmtNodeAnimation::kMsgAnimationLooped, 
			MakeFunctor(*this, &crSecondaryMotionSample::Callback));

		CreateBanks();
	}

	void Callback(const crmtMessage&, crmtObserverFunctor&)
	{
		m_HasLooped = true;
	}

	void LoadExpressions()
	{
		const char* exprName = DEFAULT_EXPR;
		if(PARAM_expr.Get())
		{
			PARAM_expr.Get(exprName);
		}

		atString exprStr(exprName);
		if(exprStr.EndsWith("ex"))
		{
			datResourceMap map;
			datResourceInfo hdr;
			m_Expressions = (crExpressions*) pgRscBuilder::LoadBuild(exprName,"#ex",crExpressions::RORC_VERSION,map,hdr);
			datResource rsc(map,exprName);
			m_Expressions->Place(m_Expressions,rsc);
		}
		else
		{
			m_Expressions = crExpressions::AllocateAndLoad(exprName, true);
		}

		if(!m_Expressions)
		{
			Quitf("ERROR - failed to load expression file '%s'", exprName);
		}

		m_Expressions->AddRef();
		m_Expressions->InitializeCreature(GetCreature());

		GetCreature().Refresh();
		GetMotionTree().Refresh();
	}

	void CreateBanks()
	{
#if __BANK
		GetCreature();

		crCreatureComponentPhysical* physicalComponent = GetCreature().FindComponent<crCreatureComponentPhysical>();
		if(!physicalComponent)
			return;

		crCreatureComponentPhysical::Motion* motions = physicalComponent->GetMotions();
		crMotionDescription& descr = motions[0].m_Descr;

		bkBank &bank = BANKMGR.CreateBank("rage - Secondary Motion");

		bank.AddVector("Gravity", (Vec3V*)&descr.m_Gravity, -100.f, 100.f, 0.5f);
		bank.AddVector("Direction", &descr.m_Direction, -100.f, 100.f, 0.5f);

		const float min = -1000.f, max = 1000.f, delta = 5.f;
		bank.PushGroup("Linear",false);
		bank.AddVector("MinConstraint", &descr.m_Linear.m_MinConstraint, min, max, delta);
		bank.AddVector("MaxConstraint", &descr.m_Linear.m_MaxConstraint, min, max, delta);
		bank.AddVector("Stiffness", &descr.m_Linear.m_Strength, min, max, delta);
		bank.AddVector("Damping", &descr.m_Linear.m_Damping, min, max, delta);
		bank.PopGroup();

		bank.PushGroup("Angular",false);
		bank.AddVector("MinConstraint", &descr.m_Angular.m_MinConstraint, min, max, delta);
		bank.AddVector("MaxConstraint", &descr.m_Angular.m_MaxConstraint, min, max, delta);
		bank.AddVector("Stiffness", &descr.m_Angular.m_Strength, min, max, delta);
		bank.AddVector("Damping", &descr.m_Angular.m_Damping, min, max, delta);
		bank.PopGroup();
#endif
	}

	void ShutdownClient()
	{
		delete m_Expressions;
		delete m_Animation;

		crExpressions::ShutdownClass();
		crMotionTreeSampleManager::ShutdownClient();
	}

	void DrawClient()
	{
		craSampleManager::DrawClient();

		// Debug drawing
		bool oldLighting = grcLighting(false);

		// Draw object axis
		crCreatureComponentPhysical* physicalComponent = GetCreature().FindComponent<crCreatureComponentPhysical>();
		if(!physicalComponent)
			return;

		int boneIdx = physicalComponent->GetMotions()[0].m_ChildIdx;

		grcDrawAxis(1.0f, Mat34V(V_IDENTITY));

		Mat34V childMtx;
		GetSkeleton().GetGlobalMtx(boneIdx, childMtx);
		grcDrawAxis(0.5f, childMtx);

		// Draw attached object
		grcDrawBox(OBJECT_DIMENSION, RCC_MATRIX34(childMtx), Color_yellow);

		// Draw reference object axis
		const crBoneData* boneData = GetSkeleton().GetSkeletonData().GetBoneData(boneIdx);
		Mat34V parentMtx;
		GetSkeleton().GetGlobalMtx(boneData->GetParent()->GetIndex(), parentMtx);
		Mat34V localChildMtx;
		Mat34VFromQuatV(localChildMtx, boneData->GetDefaultRotation(), boneData->GetDefaultTranslation());
		Transform(localChildMtx, parentMtx, localChildMtx);
		grcDrawAxis(1.0f, localChildMtx);

		grcLighting(oldLighting);

		if(m_HasLooped)
		{
			crCreatureComponentPhysical* physicalComponent = GetCreature().FindComponent<crCreatureComponentPhysical>();
			if(physicalComponent)
			{
				physicalComponent->ResetMotions();
			}
			m_HasLooped = false;
		}
	}

	const char* GetDefaultFile()
	{
		const char* defaultFile = DEFAULT_FILE;
		PARAM_file.Get(defaultFile);
		return defaultFile;
	}

private:
	crExpressions* m_Expressions;
	crAnimation* m_Animation;
	bool m_HasLooped;
};


} // namespace ragesamples

// main application
int Main()
{
	ragesamples::crSecondaryMotionSample sampleMotionTreeCreature;
	sampleMotionTreeCreature.Init();

	sampleMotionTreeCreature.UpdateLoop();

	sampleMotionTreeCreature.Shutdown();

	return 0;
}
