// 
// sample_mt/sample_motiontree.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_motiontree.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "crclip/clip.h"
#include "creature/creature.h"
#include "crmotiontree/nodeclip.h"
#include "crmotiontree/nodeexpression.h"
#include "crmotiontree/nodeik.h"
#include "crmotiontree/nodepm.h"
#include "crmotiontree/nodestyle.h"
#include "system/param.h"
#include "system/timemgr.h"

using namespace rage;

namespace ragesamples
{

crMotionTreeSampleManager::crMotionTreeSampleManager() 
: m_Logging(false)
, m_Verbosity(3)
{
	m_MotionTree = rage_new crmtMotionTree();
}

crMotionTreeSampleManager::~crMotionTreeSampleManager()
{
}

void crMotionTreeSampleManager::Init(const char* path)
{
	crClip::InitClass();
	crmtMotionTree::InitClass();

	// initialize all the non-core built in nodes
	crmtNodeIk::InitClass();
	crmtNodeClip::InitClass();
	crmtNodePm::InitClass();
	crmtNodeExpression::InitClass();

	crFragSampleManager::Init(path);
}

void crMotionTreeSampleManager::Update()
{
	crFragSampleManager::Update();

	UpdateMotionTree();

	if(m_Logging)
	{
		m_MotionTree->Dump(m_Verbosity);
	}
}

void crMotionTreeSampleManager::Shutdown()
{
	delete m_MotionTree;
	m_MotionTree = NULL;

	crFragSampleManager::Shutdown();

	crmtMotionTree::ShutdownClass();
	crClip::ShutdownClass();
}

void crMotionTreeSampleManager::InitClient()
{
	crFragSampleManager::InitClient();

	// initialize the motion tree:
	InitMotionTree();

#if __BANK
	AddWidgetsMotionTree();
#endif // __BANK
}

void crMotionTreeSampleManager::UpdateClient()
{
	crFragSampleManager::UpdateClient();
}

void crMotionTreeSampleManager::DrawClient()
{
	crFragSampleManager::DrawClient();
}

void crMotionTreeSampleManager::ShutdownClient()
{
	crFragSampleManager::ShutdownClient();
}

void crMotionTreeSampleManager::InitMotionTree()
{
	m_MotionTree->Init(GetCreature());
}

void crMotionTreeSampleManager::UpdateMotionTree()
{
	m_MotionTreeScheduler.Schedule(*m_MotionTree, TIME.GetSeconds());
	m_MotionTreeScheduler.WaitOnAllComplete();

	GetCreature().PostUpdate();
}

#if __BANK
void crMotionTreeSampleManager::AddWidgetsMotionTree()
{
	bkBank& crmtBank = BANKMGR.CreateBank("Motion Tree", 50, 500);
	crmtBank.AddToggle("Enable Logging", &m_Logging);
	crmtBank.AddSlider("Logging Verbosity", &m_Verbosity, 0, 3, 1.f);
}
#endif // __BANK


}; // namespace ragesamples

