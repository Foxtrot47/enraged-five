// 
// sample_mt/sample_mt_manager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#include "sample_motiontree.h"

#include "cranimation/animation.h"
#include "cranimation/framefilters.h"
#include "crmotiontree/managermixer.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/observer.h"
#include "crmotiontree/requestanimation.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

namespace rage
{
	datCallback g_ClothManagerCallback = NullCB;
}

using namespace rage;


#define DEFAULT_ANIM "$/animation/motion/male_wlk_m.anim"
#define DEFAULT_FILE "$/rage_male/rage_male.type"

PARAM(anim,"[sample_mt_manager] Animation file to load (default is \"" DEFAULT_ANIM "\")");
XPARAM(file);

class crMotionTreeSampleManagerSingleState : public ragesamples::crMotionTreeSampleManager
{
public:
	crMotionTreeSampleManagerSingleState() : ragesamples::crMotionTreeSampleManager()
	{
	}

protected:
	void InitClient()
	{
		crMotionTreeSampleManager::InitClient();

		m_Manager = rage_new crmtManagerMixer(GetMotionTree());

		const char* animName = DEFAULT_ANIM;
		PARAM_anim.Get(animName);
/*		
		crAnimation* anim = crAnimation::AllocateAndLoad(animName, NULL, true);
		
		crmtRequestAnimation reqAnim(anim);
		reqAnim.SetLooping(true, true);
		anim->Release();

		m_Manager->InsertChannel(0, reqAnim);
		m_Manager->SetWeight(0, 1.f);
*/
		crAnimation* animL = crAnimation::AllocateAndLoad("$/animation/motion/male_wlk_lft_turn_m.anim", NULL, true);
		crmtRequestAnimation reqAnimL(animL);
		reqAnimL.SetLooping(true, true);
		animL->Release();

		m_Manager->InsertChannel(0, reqAnimL);
		m_Manager->SetWeight(0, 0.75f);

		crAnimation* animR = crAnimation::AllocateAndLoad("$/animation/motion/male_wlk_rgt_turn_m.anim", NULL, true);
		crmtRequestAnimation reqAnimR(animR);
		reqAnimR.SetLooping(true, true);
		animR->Release();

		m_Manager->InsertChannel(0, reqAnimR);
		m_Manager->SetWeight(0, 0.5f);

#if __BANK
		// BANKS?
#endif // __BANK
	}

	

	void ShutdownClient()
	{
		delete m_Manager;
	}

	const char* GetDefaultFile()
	{
		const char* filename = DEFAULT_FILE;
		PARAM_file.Get(filename);
		return filename;
	}

private:
	crmtManagerMixer* m_Manager;
};


// main application
int Main()
{
	crMotionTreeSampleManagerSingleState sampleMotionTreeManager;
	sampleMotionTreeManager.Init("");

	sampleMotionTreeManager.UpdateLoop();

	sampleMotionTreeManager.Shutdown();

	return 0;
}
