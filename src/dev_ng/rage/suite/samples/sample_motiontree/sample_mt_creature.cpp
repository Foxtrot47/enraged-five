// 
// sample_mt/sample_mt_creature.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Creature motion tree sample
// PURPOSE: This sample demonstrates a motion tree being used to update a creature.  


#include "sample_motiontree.h"

#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/framefilters.h"
#include "creature/creature.h"
#include "crextra/expressions.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeanimation.h"
#include "crmotiontree/observer.h"
#include "crmotiontree/requestanimation.h"
#include "crmotiontree/requestblend.h"
#include "crmotiontree/requestcapture.h"
#include "crmotiontree/requestexpression.h"
#include "crmotiontree/requestfilter.h"
#include "crmotiontree/requestframe.h"
#include "crmotiontree/requestmerge.h"
#include "crmotiontree/requestpm.h"
#include "crparameterizedmotion/parameter.h"
#include "crparameterizedmotion/parameterizedmotion.h"
#include "crparameterizedmotion/parameterizedmotionsimple.h"
#include "grmodel/shadergroup.h"
#include "input/pad.h"
#include "rmcore/drawable.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "zlib/zlib.h"

namespace rage
{
	datCallback g_ClothManagerCallback = NullCB;
}

using namespace rage;

#define DEFAULT_FILE "$/tempTestAssets/MC4_Karol/entity.type"
XPARAM(file);

#define DEFAULT_ANIM "$/tempTestAssets/MC4_Karol/Karol_PinkSlip_Tweaked02.anim"
PARAM(anim, "[sample_mt_creature] Animation file to load (default is \"" DEFAULT_ANIM "\")");

#define DEFAULT_ANIMMASKREMAP "$/tempTestAssets/MC4_Karol/animNrmMap_remap.txt"
PARAM(animmaskremap, "[sample_mt_creature] Remap file for animated normal map masks");

#define DEFAULT_EXPR ""
PARAM(expr, "[sample_mt_creature] Expression file to load (default is \"" DEFAULT_EXPR "\")");

#define DEFAULT_PM ""
PARAM(pm, "[sample_mt_creature] Parameterized motion file to load (default is \"" DEFAULT_PM "\")");

namespace ragesamples {
#define DEFAULT_LIGHTPRESETS "$/tempTestAssets/MC4_Karol/MC4_Karol.lgroup"
XPARAM(lightpresets);

#define DEFAULT_CAMPRESETS "$/tempTestAssets/MC4_Karol/MC4_Karol.camset"
XPARAM(campresets);
} // namespace ragesamples

PARAM(verbose, "[sample_mt_creature] Verbose output [and level] (default is none)");

#define PROFILE_MOTION_TREE (0)

namespace ragesamples 
{

class crMotionTreeSampleCreature : public ragesamples::crMotionTreeSampleManager
{
public:
	crMotionTreeSampleCreature() 
		: ragesamples::crMotionTreeSampleManager()
		, m_Animation(NULL)
		, m_Expressions(NULL)
		, m_Pm(NULL)
		, m_RageSummitDemo(false)
		, m_RageSummitDemo_UseSixaxis(false)
		, m_FrameIn(NULL)
		, m_FrameOut(NULL)
	{
		m_AnimationObserver.AddRef();
		m_PmObserver.AddRef();
	}

protected:

	void InitClient()
	{
		// Set some default parameters before grcSampleManager::Init
		if (!PARAM_lightpresets.Get())
		{
			PARAM_lightpresets.Set(DEFAULT_LIGHTPRESETS);
		}
		if (!PARAM_campresets.Get())
		{
			PARAM_campresets.Set(DEFAULT_CAMPRESETS);
		}

		crMotionTreeSampleManager::InitClient();
		crExpressions::InitClass();

		const char* animName = DEFAULT_ANIM;
		if(PARAM_anim.Get())
		{
			PARAM_anim.Get(animName);
		}

		if(animName && animName[0])
		{
			m_Animation = crAnimation::AllocateAndLoad(animName, NULL, true);
			if(!m_Animation)
			{
				Quitf("ERROR - failed to load animation file '%s'", animName);
			}
		}

		const char* exprName = DEFAULT_EXPR;
		if(PARAM_expr.Get())
		{
			PARAM_expr.Get(exprName);
		}

		const bool pack = true;
		if(exprName && exprName[0])
		{
			m_Expressions = crExpressions::AllocateAndLoad(exprName, pack);

			if(!m_Expressions)
			{
				Quitf("ERROR - failed to load expression file '%s'", exprName);
			}
			m_Expressions->AddRef();
			m_Expressions->InitializeCreature(GetCreature());
		}

		const char* pmName = DEFAULT_PM;
		if(PARAM_pm.Get())
		{
			PARAM_pm.Get(pmName);
		}

		if(pmName && pmName[0])
		{
			crpmParameterizedMotionSimpleData* spmData = crpmParameterizedMotionSimpleData::AllocateAndLoad(pmName);
			if(!spmData)
			{
				Quitf("ERROR - failed to load parameterized motion file '%s'", pmName);
			}

			crpmParameterizedMotionSimple* spm = rage_new crpmParameterizedMotionSimple;
			spm->Init(*spmData);
			spm->SetLooped(true);

			m_Pm = spm;
			m_Parameter.Init(m_Pm->GetNumParameterDimensions());
		}

		GetCreature().InitDofs(m_FrameData);

		m_FrameIn = rage_new crFrame();
		m_FrameOut = rage_new crFrame();
		m_FrameIn->Init(m_FrameData);
		m_FrameOut->Init(m_FrameData);

		GetCreature().Identity(*m_FrameIn);

#if PROFILE_MOTION_TREE
		crmtRequestAnimation reqAnim(m_Animation);
		crmtRequestPm reqPm(m_Pm);
		reqAnim.SetLooping(true, true);
		crmtRequestMerge reqMerge(reqPm, reqAnim);
		crmtRequestExpression reqExpression(reqMerge, m_Expressions);

		GetMotionTree().Request(reqExpression);
#else // PROFILE_MOTION_TREE
		crmtRequestFrame reqFrame(m_FrameIn);
		crmtRequestPm reqPm(m_Pm);
		crmtRequestAnimation reqAnim(m_Animation);
		reqAnim.SetLooping(true, true);
		crmtRequestMerge reqMerge1(reqPm, reqAnim);
		crmtRequestMerge reqMerge2(reqFrame, reqMerge1);
		crmtRequestExpression reqExpression(reqMerge2, m_Expressions);
		crmtRequestCapture reqCapture(reqExpression, m_FrameOut);

		GetMotionTree().Request(reqCapture);
#endif // PROFILE_MOTION_TREE

		m_AnimationObserver.Attach(reqAnim.GetObserver());
		m_PmObserver.Attach(reqPm.GetObserver());

		if(PARAM_verbose.Get())
		{
			int verbosity = 2;
			PARAM_verbose.Get(verbosity);

			GetMotionTree().Dump(verbosity);
			if(m_Expressions)
			{
				m_Expressions->Dump(verbosity);
			}
		}

#if __BANK
		AddWidgetsCreature();
#else // __BANK
		if(m_Animation)
		{
			m_FrameIn->Invalidate();
		}
#endif // __BANK
	}

	void ShutdownClient()
	{
		if(m_Animation)
		{
			delete m_Animation;
			m_Animation = NULL;
		}
		if(m_Expressions)
		{
			delete m_Expressions;
			m_Expressions = NULL;
		}
		if(m_Pm)
		{
			delete m_Pm;
			m_Pm = NULL;
		}

		m_FrameIn->Release();
		m_FrameOut->Release();

		crExpressions::ShutdownClass();
		crMotionTreeSampleManager::ShutdownClient();
	}

	void DrawClient()
	{
		crMotionTreeSampleManager::DrawClient();
	}

	void UpdateClient()
	{
		crMotionTreeSampleManager::UpdateClient();

#if __BANK
		UpdateFrameInOutWidgets();

		if(m_Pm)
		{
			m_Pm->SetParameter(m_Parameter);
		}
#endif //__BANK

		if (m_RageSummitDemo)
		{
			Vec3V jawPosition;
			Vec3V eyePosition;
			Vec3V browLeftCircle;
			Vec3V browRightCircle;

			ioPad& pad = ioPad::GetPad(0);
			browLeftCircle[1] = pad.GetNormAnalogButton(ioPad::L2_INDEX) * 0.5f;
			browLeftCircle[0] = browLeftCircle[2] = 0.0f;
			browRightCircle[1] = pad.GetNormAnalogButton(ioPad::R2_INDEX) * 0.5f;
			browRightCircle[0] = browRightCircle[2] = 0.0f;
			float leftBlink = pad.GetNormAnalogButton(ioPad::L1_INDEX);
			float rightBlink = pad.GetNormAnalogButton(ioPad::R1_INDEX);
#if __PPU
			if (m_RageSummitDemo_UseSixaxis)
			{
				eyePosition[0] = pad.GetNormSensorAxis(ioPad::SENSOR_X) * 0.75f;
				eyePosition[1] = pad.GetNormSensorAxis(ioPad::SENSOR_Y) * 0.75f;
			}
			else
#endif // __PPU
			{
				m_RageSummitDemo_UseSixaxis = false;
				eyePosition[0] = pad.GetNormLeftX() * 0.75f;
				eyePosition[1] = pad.GetNormLeftY() * 0.5f;
			}
			eyePosition[2] = 0.0f;
			jawPosition[0] = pad.GetNormRightX();
			jawPosition[1] = pad.GetNormRightY();
			jawPosition[2] = 0.0f;

			m_FrameIn->SetVector3(kTrackFacialTranslation, 36436, jawPosition);
			m_FrameIn->SetVector3(kTrackFacialTranslation, 25902, eyePosition);
			m_FrameIn->SetVector3(kTrackFacialTranslation, 27604, browLeftCircle);
			m_FrameIn->SetVector3(kTrackFacialTranslation, 42334, browRightCircle);
			m_FrameIn->SetVector3(kTrackFacialTranslation, 38462, browLeftCircle);
			m_FrameIn->SetVector3(kTrackFacialTranslation, 58113, browRightCircle);
			m_FrameIn->SetFloat(kTrackFacialControl, 33887, leftBlink);
			m_FrameIn->SetFloat(kTrackFacialControl, 11155, rightBlink);
		}
		GetCamMgr().SetAcceptInput(!m_RageSummitDemo);
	}

	void CreateCreature(crCreature*& creature)
 	{
		creature = rage_new crCreature;
		creature->Init(m_FrameDataFactory, m_FrameAccelerator, &GetSkeleton(), &GetMoverMtx());
	}

	void InitFragment()
	{
		// suppress creation of fragment inst
	}

	void InitMover()
	{
		// suppress creation of a mover inst
	}

	const char* GetDefaultFile()
	{
		const char* defaultFile = DEFAULT_FILE;
		PARAM_file.Get(defaultFile);
		return defaultFile;
	}

#if PROFILE_MOTION_TREE
	void UpdateMotionTree()
	{
		sysTimer timer;

#define USE_SCHEDULER (1)
#if USE_SCHEDULER
		crMotionTreeSampleManager::UpdateMotionTree();
#else // USE_SCHEDULER
		GetMotionTree().Update(TIME.GetSeconds());
#endif // USE_SCHEDULER

		float time = timer.GetTime();

		static float totalTime = 0.f;
		totalTime += time;

		static float minTime = FLT_MAX;
		minTime = Min(time, minTime);

		static float maxTime = 0.f;
		maxTime = Max(time, maxTime);

		static int totalPasses = 0;
		totalPasses++;

		if(!(totalPasses%1000))
		{
			Printf("motion tree update time: average %f ms, min %f ms, max %f ms\n", 1000.f*totalTime/float(totalPasses), 1000.f*minTime, 1000.f*maxTime);
			totalTime = 0.f;
			minTime = FLT_MAX;
			maxTime = 0.f;
			totalPasses = 0;
		}
	}
#endif // PROFILE_MOTION_TREE

#if __BANK
	void AddWidgetsCreature()
	{
		bkBank &creatureBank=BANKMGR.CreateBank("Creature",50,500);
		creatureBank.AddToggle("RAGE Summit demo", &m_RageSummitDemo);
		creatureBank.AddToggle("RAGE Summit demo (use Sixaxis?)", &m_RageSummitDemo_UseSixaxis);

		creatureBank.PushGroup("Materials");
		GetDrawable().GetShaderGroup().AddWidgets(creatureBank);
		creatureBank.PopGroup();

		creatureBank.AddButton("Reset Input", datCallback(MFA(crMotionTreeSampleCreature::ResetFrameInOutWidgets),this));
		creatureBank.AddButton("Dump Output", datCallback(MFA(crMotionTreeSampleCreature::DumpOut),this));

		creatureBank.PushGroup("Input frame");
		m_FrameInWidgets.Init(*m_FrameIn, &GetSkeleton().GetSkeletonData());
		m_FrameInWidgets.AddWidgets(creatureBank);
		creatureBank.PopGroup();

		creatureBank.PushGroup("Output frame");
		m_FrameOutWidgets.Init(*m_FrameOut, &GetSkeleton().GetSkeletonData());
		m_FrameOutWidgets.AddWidgets(creatureBank);
		creatureBank.PopGroup();

		ResetFrameInOutWidgets();

		if(m_Animation && m_AnimationObserver.IsAttached())
		{
			crmtNodeAnimation* nodeAnimation = static_cast<crmtNodeAnimation*>(m_AnimationObserver.GetNode());
			nodeAnimation->GetAnimPlayer().AddWidgets(creatureBank);
		}

		if(m_Pm && m_PmObserver.IsAttached())
		{
			creatureBank.PushGroup("Simple Parameterized Motion");

			crpmParameter minParameter(m_Parameter.GetNumDimensions());
			crpmParameter maxParameter(m_Parameter.GetNumDimensions());

			m_Pm->GetParameterBoundaries(minParameter, maxParameter);

			m_Parameter.AddWidgets(creatureBank, &minParameter, &maxParameter);

			creatureBank.PopGroup();
		}
	}

	void UpdateFrameInOutWidgets()
	{
		m_FrameInWidgets.UpdateFrame();

		m_FrameOutWidgets.ResetWidgets();
	}

	void ResetFrameInOutWidgets()
	{
		GetCreature().Identity(*m_FrameIn);

		m_FrameInWidgets.ResetWidgets();
		if(m_Animation)
		{
			m_FrameIn->Invalidate();

			m_FrameInWidgets.ResetWidgets();
		}
	}

	void DumpOut()
	{
		m_FrameOut->Dump();
	}

	FrameEditorWidgets m_FrameInWidgets;
	FrameEditorWidgets m_FrameOutWidgets;
#endif // __BANK

private:
	crFrameData m_FrameData;
	crFrame* m_FrameIn;
	crFrame* m_FrameOut;
	crAnimation* m_Animation;
	crExpressions* m_Expressions;
	crpmParameterizedMotion* m_Pm;
	crpmParameter m_Parameter;
	crmtObserver m_AnimationObserver;
	crmtObserver m_PmObserver;
	bool m_RageSummitDemo;
	bool m_RageSummitDemo_UseSixaxis;
};

} // namespace ragesamples

// main application
int Main()
{
	ragesamples::crMotionTreeSampleCreature sampleMotionTreeCreature;
	sampleMotionTreeCreature.Init();

	sampleMotionTreeCreature.UpdateLoop();

	sampleMotionTreeCreature.Shutdown();

	return 0;
}
