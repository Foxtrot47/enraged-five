// 
// sample_mt/sample_mt_simple.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Test motion tree sample
// PURPOSE: Used to test performance on simulated trees.


#include "sample_motiontree.h"

#include "cranimation/animation.h"
#include "cranimation/framefilters.h"
#include "creature/creature.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeanimation.h"
#include "crmotiontree/observer.h"
#include "crmotiontree/requestaddsubtract.h"
#include "crmotiontree/requestanimation.h"
#include "crmotiontree/requestblend.h"
#include "crmotiontree/requestfilter.h"
#include "crmotiontree/requestmirror.h"
#include "crskeleton/skeletondata.h"
#include "math/random.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "system/timer.h"

namespace rage
{
	datCallback g_ClothManagerCallback = NullCB;
}

using namespace rage;

#define NUM_ANIMS (3)

#define DEFAULT_ANIM0 "Gent_nor_hnd_for_wlk_7"
#define DEFAULT_ANIM1 "gent_nor_hnd_lft_mid_pck_7"
#define DEFAULT_ANIM2 "gent_nor_hnd_lft_upp_pck_7"

PARAM(anim0,"[sample_mt_test] Animation file 0 to load (default is \"" DEFAULT_ANIM0 "\")");
PARAM(anim1,"[sample_mt_test] Animation file 1 to load (default is \"" DEFAULT_ANIM1 "\")");
PARAM(anim2,"[sample_mt_test] Animation file 2 to load (default is \"" DEFAULT_ANIM2 "\")");


#define ADDITIONAL_TREES (20)

namespace ragesamples 
{

class crMotionTreeSampleTest : public ragesamples::crMotionTreeSampleManager
{
public:
	crMotionTreeSampleTest() 
		: ragesamples::crMotionTreeSampleManager()
	{
		for(int i=0; i<NUM_ANIMS; ++i)
		{
			m_Animations[i] = NULL;
		}
	}

protected:

	void InitClient()
	{
		crMotionTreeSampleManager::InitClient();

		// STEP #1. Load the animations

		// -- This sample uses three animations, which need to be loaded
		// using either their default filenames, or ones provided via the 
		// command line.
		for(int i=0; i<NUM_ANIMS; ++i)
		{
			const char* animName = NULL;
			switch(i)
			{
			case 0: 
				{
					animName = DEFAULT_ANIM0;
					PARAM_anim0.Get(animName);
				}
				break;
			case 1:
				{
					animName = DEFAULT_ANIM1;
					PARAM_anim1.Get(animName);
				}
				break;
			case 2:
				{
					animName = DEFAULT_ANIM2;
					PARAM_anim2.Get(animName);
				}
				break;
			default:
				Assert(0);
			}

			m_Animations[i] = crAnimation::AllocateAndLoad(animName, NULL, true);

			// -- Check that the animations loaded successfully.
			if(!m_Animations[i])
			{
				Quitf("Failed to load animation '%s%s'", ASSET.GetPath(), animName);
			}
		}

		// -STOP


		m_Filter = rage_new crFrameFilterBoneBasic(GetSkeleton().GetSkeletonData());
		m_Filter->AddBone(GetSkeleton().GetSkeletonData().FindBoneData("clavicle_l"), true, 1.f);
		m_Filter->AddBone(GetSkeleton().GetSkeletonData().FindBoneData("hip_l"), true, 1.f);
		m_Filter->AddBone(GetSkeleton().GetSkeletonData().FindBoneData("clavicle_r"), true, 1.f);
		m_Filter->AddBone(GetSkeleton().GetSkeletonData().FindBoneData("hip_r"), true, 1.f);

		m_Filter2 = rage_new crFrameFilterBoneBasic(GetSkeleton().GetSkeletonData());
		m_Filter2->AddBone(GetSkeleton().GetSkeletonData().FindBoneData("clavicle_l"), true, 1.f);
		m_Filter2->AddBone(GetSkeleton().GetSkeletonData().FindBoneData("hip_l"), true, 1.f);

		const int maxRequests = 10; // 100;
		crmtRequest* requests[maxRequests];

#define IDENTICAL_TREES (1)
#if IDENTICAL_TREES 
		g_ReplayRand.SetFullSeed(1);
#endif // IDENTICAL_TREES
		MakeRequest(maxRequests, requests, -1, GetMotionTree());

		GetMotionTree().Dump();

#if ADDITIONAL_TREES > 0
		for(int i=0; i<ADDITIONAL_TREES; ++i)
		{
#if IDENTICAL_TREES
			g_ReplayRand.SetFullSeed(1);
#endif // IDENTICAL_TREES
			m_AdditionalTrees[i] = rage_new crmtMotionTree();
			m_AdditionalSkeletons[i].Init(GetSkeleton().GetSkeletonData());
			m_AdditionalMoverMtxs[i] = Mat34V(V_IDENTITY);
			m_AdditionalCreatures[i].Init(m_FrameDataFactory, m_FrameAccelerator, &m_AdditionalSkeletons[i], &m_AdditionalMoverMtxs[i]);
			m_AdditionalTrees[i]->Init(m_AdditionalCreatures[i]);

			crmtRequest* requests[maxRequests];

			MakeRequest(maxRequests, requests, -1, *m_AdditionalTrees[i]);
		}
#endif // ADDITIONAL_TREES > 0

	}

	void MakeRequest(int depth, crmtRequest** requests, int top, crmtMotionTree& mt)
	{
		if(depth > 0)
		{
			crAnimation* anim = NULL;
			if(g_ReplayRand.GetRanged(0, 2))
			{
				const char* animName = DEFAULT_ANIM0;
				PARAM_anim0.Get(animName);

				anim = crAnimation::AllocateAndLoad(animName, NULL, true);
			}
			crmtRequestAnimation reqAnim(anim);
#if 1

#if 1
			crFrameFilterBoneBasic* filter = rage_new crFrameFilterBoneBasic(GetSkeleton().GetSkeletonData());
			filter->AddBone(GetSkeleton().GetSkeletonData().FindBoneData("clavicle_l"), true, 1.f);
			filter->AddBone(GetSkeleton().GetSkeletonData().FindBoneData("hip_l"), true, 1.f);
			filter->AddBone(GetSkeleton().GetSkeletonData().FindBoneData("clavicle_r"), true, 1.f);
			filter->AddBone(GetSkeleton().GetSkeletonData().FindBoneData("hip_r"), true, g_ReplayRand.GetFloat());
			crmtRequestFilter reqFilter(reqAnim, filter);
			requests[++top] = &reqFilter;
#else
			crmtRequestFilter reqFilter(reqAnim, m_Filter);
			requests[++top] = &reqFilter;
#endif

#else
			requests[++top] = &reqAnim;
#endif
			MakeRequest(depth-1, requests, top, mt);
		}
		else if(top > 1)
		{
			int swaps = g_ReplayRand.GetRanged(0, 10);
			for(int i=0; i<swaps; ++i)
			{
				int idx0 = g_ReplayRand.GetRanged(0, top);
				int idx1 = g_ReplayRand.GetRangedDifferent(idx0, 0, top);
				SwapEm(requests[idx0], requests[idx1]);
			}

			float blend = !g_ReplayRand.GetRanged(0, 4)?(g_ReplayRand.GetRanged(0, 1)?1.f:0.f):g_ReplayRand.GetFloat();
			if(1) // g_ReplayRand.GetRanged(0, 2))
			{
				crmtRequestBlend reqBlend(*requests[top], *requests[top-1], blend);
				requests[--top] = &reqBlend;
				MakeRequest(0, requests, top, mt);
			}
			else
			{
				crmtRequestAddSubtract reqAddSubtract(*requests[top], *requests[top-1], blend);
				requests[--top] = &reqAddSubtract;
				MakeRequest(0, requests, top, mt);
			}
		}
		else
		{
#if 1
		crmtRequestFilter reqFilter(*requests[top], m_Filter2);
		mt.Request(reqFilter);
#else
		mt.Request(*requests[top]);
#endif
		}
	}

	void UpdateMotionTree()
	{
		sysTimer timer;

#if ADDITIONAL_TREES > 0
		for(int i=0; i<ADDITIONAL_TREES; ++i)
		{
			GetMotionTreeScheduler().Schedule(*m_AdditionalTrees[i], TIME.GetSeconds());
		}
#endif // ADDITIONAL_TREES 

		crMotionTreeSampleManager::UpdateMotionTree();

		float time = timer.GetTime();

		static float totalTime = 0.f;
		totalTime += time;

		static float minTime = FLT_MAX;
		minTime = Min(time, minTime);

		static float maxTime = 0.f;
		maxTime = Max(time, maxTime);

		static int totalPasses = 0;
		totalPasses++;

		if(!(totalPasses%500))
		{
			Printf("motion tree update time: average %f ms, min %f ms, max %f ms\n", 1000.f*totalTime/float(totalPasses), 1000.f*minTime, 1000.f*maxTime);
			totalTime = 0.f;
			minTime = FLT_MAX;
			maxTime = 0.f;
			totalPasses = 0;
		}
	}

	void ShutdownClient()
	{
		for(int i=0; i<NUM_ANIMS; ++i)
		{
			if(m_Animations[i])
			{
				m_Animations[i]->Release();
				m_Animations[i] = NULL;
			}
		}

		if(m_Filter)
		{
			m_Filter->Release();
			m_Filter = NULL;
		}
		if(m_Filter2)
		{
			m_Filter2->Release();
			m_Filter2 = NULL;
		}


#if ADDITIONAL_TREES > 0
		for(int i=0; i<ADDITIONAL_TREES; ++i)
		{
			delete m_AdditionalTrees[i];
		}
#endif // ADDITIONAL_TREES 

		crMotionTreeSampleManager::ShutdownClient();
	}

	void InitMover()
	{
		// suppress creation of a mover inst
	}

private:
	crAnimation* m_Animations[NUM_ANIMS];
	crFrameFilterBoneBasic* m_Filter;
	crFrameFilterBoneBasic* m_Filter2;

#if ADDITIONAL_TREES > 0
	crmtMotionTree* m_AdditionalTrees[ADDITIONAL_TREES];
	crCreature m_AdditionalCreatures[ADDITIONAL_TREES];
	crSkeleton m_AdditionalSkeletons[ADDITIONAL_TREES];
	Mat34V m_AdditionalMoverMtxs[ADDITIONAL_TREES];
#endif // ADDITIONAL_TREES > 0
};

} // namespace ragesamples

// main application
int Main()
{
	ragesamples::crMotionTreeSampleTest sampleMotionTreeTest;
	sampleMotionTreeTest.Init("sample_cranimation/newskel");

	sampleMotionTreeTest.UpdateLoop();

	sampleMotionTreeTest.Shutdown();

	return 0;
}
