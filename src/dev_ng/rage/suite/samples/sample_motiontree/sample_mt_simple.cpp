// 
// sample_mt/sample_mt_simple.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Simple motion tree sample
// PURPOSE: This sample demonstrates how to construct and manipulate a 
// very simple motion tree.  
// It demonstrates direct usage of a motion tree and its nodes, not 
// via a manager (see sample_mt_manager for that).  
// It is not designed to be interactive or general purpose, but shows a 
// sequence of operations performed on a motion tree to introduce 
// various techniques, objects and interfaces.
// In this sample, with the default animations, the character will
// take two steps, stop and grab an object at chest height with his
// right hand.


#include "sample_motiontree.h"

#include "cranimation/animation.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeanimation.h"
#include "crmotiontree/observer.h"
#include "crmotiontree/requestanimation.h"
#include "crmotiontree/requestblend.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

#define NUM_ANIMS (3)

#define DEFAULT_ANIM0 "Gent_nor_hnd_for_wlk"
#define DEFAULT_ANIM1 "gent_nor_hnd_lft_mid_pck"
#define DEFAULT_ANIM2 "gent_nor_hnd_lft_upp_pck"

PARAM(anim0,"[sample_mt_simple] Animation file 0 to load (default is \"" DEFAULT_ANIM0 "\")");
PARAM(anim1,"[sample_mt_simple] Animation file 1 to load (default is \"" DEFAULT_ANIM1 "\")");
PARAM(anim2,"[sample_mt_simple] Animation file 2 to load (default is \"" DEFAULT_ANIM2 "\")");

namespace rage
{
#if (__BANK && !__RESOURCECOMPILER)
	datCallback	g_ClothManagerCallback = NullCB;
#endif
}

namespace ragesamples 
{

class crMotionTreeSampleSimple : public ragesamples::crMotionTreeSampleManager
{
public:
	crMotionTreeSampleSimple() 
		: ragesamples::crMotionTreeSampleManager()
	{
		for(int i=0; i<NUM_ANIMS; ++i)
		{
			m_Animations[i] = NULL;
		}
	}

protected:

	void InitClient()
	{
		crMotionTreeSampleManager::InitClient();

		// STEP #1. Load the animations

		// -- This sample uses three animations, which need to be loaded
		// using either their default filenames, or ones provided via the 
		// command line.
		for(int i=0; i<NUM_ANIMS; ++i)
		{
			const char* animName = NULL;
			switch(i)
			{
			case 0: 
				{
					animName = DEFAULT_ANIM0;
					PARAM_anim0.Get(animName);
				}
				break;
			case 1:
				{
					animName = DEFAULT_ANIM1;
					PARAM_anim1.Get(animName);
				}
				break;
			case 2:
				{
					animName = DEFAULT_ANIM2;
					PARAM_anim2.Get(animName);
				}
				break;
			default:
				Assert(0);
			}

			m_Animations[i] = crAnimation::AllocateAndLoad(animName, NULL, true);

			// -- Check that the animations loaded successfully.
			if(!m_Animations[i])
			{
				Quitf("Failed to load animation '%s%s'", ASSET.GetPath(), animName);
			}
		}

		// -STOP


		// STEP #2. Request an animation node.
		
		// -- An animation node is a basic output node in a motion tree,
		// which will play a single animation (and take care of issues like
		// movers and looping etc).  
		// Internally an animation node uses a crAnimPlayer to 
		// track the current time in the animation.
		// Like all nodes, animation nodes are constructed by creating a
		// request, and passing this into the motion tree.

		// - Construct an animation request, initialize with the animation
		// we want the animation we want the node to play
		crmtRequestAnimation reqAnim(m_Animations[0]);

		// - Pass the request into the motion tree.  This will cause an 
		// animation node to be created.
		crmtMotionTree& mt = GetMotionTree();
		mt.Request(reqAnim);

		// -STOP

		
		// STEP #3. Obtain an observer (smart pointer), to the newly constructed node.
		
		// -- Observers are smart pointers that track nodes within a motion
		// tree and receive notification of events on those nodes.
		// Never use ordinary pointers to track node activity in the tree, 
		// nodes can be destroyed without notice, leading to invalid memory accesses.

		// - Construct a functor observer.  
		// A functor observer is a special type of observer that allows 
		// functors to be registered to process specific events that occur on 
		// the observed node.
		crmtObserverFunctor* observer = rage_new crmtObserverFunctor;

		// - Attach the newly created observer to the node created by the request.
		observer->Attach(reqAnim.GetObserver());

		// - Register a callback function to process the animation looped event.
		observer->RegisterFunctor(crmtNodeAnimation::kMsgAnimationLooped, 
			MakeFunctor(*this, &crMotionTreeSampleSimple::Callback));

		// -STOP

#if __BANK
		// BANKS?
#endif // __BANK
	}

	void Callback(const crmtMessage&, crmtObserverFunctor& observer)
	{
		crmtMotionTree& mt = GetMotionTree();

		// STEP #4. Make a more complex, composite request.

		// -- Requests can be combined together to cause the construction
		// of multiple new nodes.

		// - Request two new animation nodes.
		crmtRequestAnimation reqAnim1(m_Animations[1]);
		crmtRequestAnimation reqAnim2(m_Animations[2]);

		// - Override the looping behaviour, don't allow these animations to loop.
		reqAnim1.SetLooping(true, false);
		reqAnim2.SetLooping(true, false);

		// - Request a blend node, use this to blend between the two animations.
		// The first parameter indicates a 50% blend between the two inputs, while
		// the second parameter indicates the blend is constant, unchanging.
		// The default animations are of a character grabbing an object at 
		// waist level and at a very high level.  The blend between the two
		// will result in a character grabbing at chest height.
		crmtRequestBlend reqBlend(reqAnim1, reqAnim2, 0.5f, 0.f);

		// - Request a cross fade, or blend node.
		// The first parameter indicates a blend starting at 0% while
		// the second parameter indicates the blend is variable, and will increase at
		// a rate of 400% per second (ie it will go from 0 -> 100% in 1/4 of a second).
		crmtRequestBlend reqFade(reqBlend, 0.f, 4.f);

		// - Submit this request to the motion tree.
		// Passing in the observer as a second parameter will cause this 
		// request to construct new nodes at that destination in the tree.
		// As the top of the request was a blend node with only one parameter,
		// the node being observed will serve as the other node in the cross fade.
		mt.Request(reqFade, &observer);

		// - Finally, detach the observer from the node, it's no longer needed.
		// Observers are reference counted, and will automatically destroy themselves.
		observer.Detach();

		//-STOP
	}

	void ShutdownClient()
	{
		for(int i=0; i<NUM_ANIMS; ++i)
		{
			if(m_Animations[i])
			{
				m_Animations[i]->Release();
				m_Animations[i] = NULL;
			}
		}

		crMotionTreeSampleManager::ShutdownClient();
	}

	void InitMover()
	{
		// suppress creation of a mover inst
	}

private:
	crAnimation* m_Animations[NUM_ANIMS];
};

} // namespace ragesamples

// main application
int Main()
{
	ragesamples::crMotionTreeSampleSimple sampleMotionTreeSimple;
	sampleMotionTreeSimple.Init("sample_cranimation/newskel");

	sampleMotionTreeSimple.UpdateLoop();

	sampleMotionTreeSimple.Shutdown();

	return 0;
}
