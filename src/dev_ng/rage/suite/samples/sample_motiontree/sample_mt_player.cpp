// 
// sample_mt/sample_mt_player.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_motiontree.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "crmotiontree/complexplayer.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

using namespace rage;

#define DEFAULT_ANIM "Gent_nor_hnd_for_wlk"


class crMotionTreeSampleComplexPlayer : public ragesamples::crMotionTreeSampleManager
{
public:
	crMotionTreeSampleComplexPlayer() 
		: ragesamples::crMotionTreeSampleManager()
		, m_Player(NULL)
		, m_Timeline(0.f)
	{
	}

protected:
	void InitClient()
	{
		crMotionTreeSampleManager::InitClient();

		m_Player = rage_new crmtComplexPlayer(m_FrameDataFactory, m_FrameAccelerator, GetSkeleton(), &GetMoverMtx(), &GetMotionTree());

#if __BANK
		AddWidgetsComplexPlayer();
#endif // __BANK
	}

	void ShutdownClient()
	{
		delete m_Player;
	}

#if __BANK
	void AddWidgetsComplexPlayer()
	{
		bkBank& playerBank = BANKMGR.CreateBank("Complex Player", 50, 500);
		m_Player->AddWidgets(playerBank);
		playerBank.AddButton("Play Animation", datCallback(MFA(crMotionTreeSampleComplexPlayer::PlayAnimation),this));
	}
#endif // __BANK

	void PlayAnimation()
	{
		const char* animFilename = DEFAULT_ANIM;
#if __BANK
		animFilename = BANKMGR.OpenFile("*.anim", false, "Animation (*.anim)");
#endif // __BANK
		crAnimation* anim = crAnimation::AllocateAndLoad(animFilename);
		if(anim)
		{
			m_Player->PlayAnimation(anim);
			anim->Release();
		}
	}

	void UpdateMotionTree()
	{
		m_Player->Update(TIME.GetSeconds());
	}

	// TODO --- SAMPLE SHOULD SHOW PLAYER INTERFACE
	// ABILITY TO PLAY DIFFERENT TYPES OF OBJECTS
	// SCRUB ETC

private:
	crmtComplexPlayer* m_Player;
	float m_Timeline;
};


// main application
int Main()
{
	crMotionTreeSampleComplexPlayer sampleMotionTreePlayer;
	sampleMotionTreePlayer.Init("sample_cranimation\\newskel");

	sampleMotionTreePlayer.UpdateLoop();

	sampleMotionTreePlayer.Shutdown();

	return 0;
}
