// 
// sample_mt/sample_mt_crowd.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_motiontree.h"

#include "cranimation/animation.h"
#include "creature/creature.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/requestanimation.h"
#include "grmodel/matrixset.h"
#include "rmcore/drawable.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "system/timer.h"

using namespace rage;

namespace rage { datCallback g_ClothManagerCallback = NullCB; }

#define GRID_DISTANCE 2.f

#define DEFAULT_FILE "$/rage_male/rage_male.type"
XPARAM(file);

#define DEFAULT_ANIM "$/animation/motion/male_wlk.anim"
PARAM(anim, "[sample_mt_creature_multiple] Animation file to load (default is \"" DEFAULT_ANIM "\")");

#define DEFAULT_COUNT 50
PARAM(count, "[sample_mt_creature_multiple] Number of creature to animated");

namespace ragesamples 
{

class crMotionTreeSampleCrowd : public ragesamples::crMotionTreeSampleManager
{
public:
	crMotionTreeSampleCrowd() 
	: ragesamples::crMotionTreeSampleManager()
	, m_Animation(NULL)
	, m_Drawable(NULL)
	, m_TotalTime(0.f)
	, m_NumCalls(0)
	{
	}

protected:

	void InitClient()
	{
		crAnimation::InitClass();
		crCreature::InitClass();
		crmtMotionTree::InitClass();

		// Load shared data
		const char* animName = DEFAULT_ANIM;
		if(PARAM_anim.Get())
		{
			PARAM_anim.Get(animName);
		}

		if(animName && animName[0])
		{
			m_Animation = crAnimation::AllocateAndLoad(animName, NULL, true);
			if(!m_Animation)
			{
				Quitf("ERROR - failed to load animation file '%s'", animName);
			}
		}
		crmtRequestAnimation reqAnim(m_Animation);
		reqAnim.SetLooping(true, true);

		const char* defaultFile = DEFAULT_FILE;
		char path[256];
		fiAssetManager::RemoveNameFromPath(path, 256, defaultFile);
		ASSET.PushFolder(path);
		m_Drawable = rage_new rmcDrawable();
		if(!m_Drawable->Load(defaultFile))
		{
			Quitf("Couldn't load file '%s'!", defaultFile);
		}
		ASSET.PopFolder();

		// Create instance of creatures
		int count = DEFAULT_COUNT;
		PARAM_count.Get(count);
		m_Instances.Resize(count);
		int columnSize = (int)sqrtf((float)m_Instances.GetCount());
		Vec3V centerPos(float(columnSize)/2,0.f,float(columnSize)/2);
		for(int i=0; i < m_Instances.GetCount(); ++i)
		{
			Instance& instance = m_Instances[i];

			Vec3V pos(float(i/columnSize),0.f,float(i%columnSize));
			pos -= centerPos;
			pos *= ScalarVFromF32(GRID_DISTANCE);
			instance.m_MoverMtx = Mat34V(V_IDENTITY);
			instance.m_MoverMtx.SetCol3(pos);

			instance.m_Skeleton.Init(*m_Drawable->GetSkeletonData());
			instance.m_Skeleton.SetParentMtx(&instance.m_MoverMtx);
			instance.m_Creature.Init(m_FrameDataFactory, m_FrameAccelerator, &instance.m_Skeleton, &instance.m_MoverMtx);
			instance.m_MotionTree.Init(instance.m_Creature);
			instance.m_MotionTree.Request(reqAnim);
			instance.m_MatrixSet = grmMatrixSet::Create(instance.m_Skeleton);
		}
	}

	void ShutdownClient()
	{
		m_Animation->Release();
		for(int i=0; i < m_Instances.GetCount(); ++i)
		{
			delete m_Instances[i].m_MatrixSet;
		}
		delete m_Drawable;
	}

	void DrawClient()
	{
		for(int i=0; i < m_Instances.GetCount(); ++i)
		{
			Instance& instance = m_Instances[i];
			instance.m_Creature.SwapBuffers();
			instance.m_Creature.DrawUpdate();
			instance.m_Creature.PreDraw(true);

			u8 lod;
			if (m_Drawable->IsVisible(RCC_MATRIX34(instance.m_MoverMtx), *grcViewport::GetCurrent(), lod)) 
			{
				float zDist = GetCamMgr().GetWorldMtx().d.Mag();
				grcTextureFactory::GetInstance().SetTextureLod(zDist);

				for (int bucket=0; bucket<NUM_BUCKETS; bucket++)
				{
					if (GetBucketEnable(bucket)) 
					{
						m_Drawable->DrawSkinned(RCC_MATRIX34(instance.m_MoverMtx), *instance.m_MatrixSet, BUCKETMASK_GENERATE(bucket), lod);
					}
				}
			}
		}
	}

	void UpdateClient()
	{

	}

	void UpdateMotionTree()
	{
		// Schedule motion trees
		for(int i=0; i < m_Instances.GetCount(); ++i)
		{
			GetMotionTreeScheduler().Schedule(m_Instances[i].m_MotionTree, TIME.GetSeconds(), 0);
		}

		sysTimer timer;
		GetMotionTreeScheduler().WaitOnAllComplete();

		// Print time waiting
		static const int MAXIMUM_CALL = 100;
		m_TotalTime += timer.GetTime();
		if(++m_NumCalls==MAXIMUM_CALL)
		{
			printf("%f\n", m_TotalTime);
			m_TotalTime = 0.f;
			m_NumCalls = 0;
		}

		// Update matrix set and creature
		for(int i=0; i < m_Instances.GetCount(); ++i)
		{
			Instance& instance = m_Instances[i];
			instance.m_MatrixSet->Update(instance.m_Skeleton, m_Drawable->IsSkinned());
			instance.m_Creature.PostUpdate();
		}
	}

private:
	struct Instance
	{
		Mat34V			m_MoverMtx;
		crSkeleton		m_Skeleton;
		crCreature		m_Creature;
		crmtMotionTree  m_MotionTree;
		grmMatrixSet*	m_MatrixSet;
	};
	atArray<Instance> m_Instances;

	crAnimation* m_Animation;
	rmcDrawable* m_Drawable;
	float m_TotalTime;
	int m_NumCalls;
};

} // namespace ragesamples

// main application
int Main()
{
	ragesamples::crMotionTreeSampleCrowd sample;
	
	sample.Init();
	sample.UpdateLoop();
	sample.Shutdown();

	return 0;
}
