// 
// sample_mt/sample_motiontree.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SAMPLE_CRMOTIONTREE_SAMPLE_CRMOTIONTREE_H
#define SAMPLE_CRMOTIONTREE_SAMPLE_CRMOTIONTREE_H

#include "sample_crfragment/sample_crfragment.h"

#include "crmotiontree/motiontree.h"
#include "crmotiontree/motiontreescheduler.h"

namespace ragesamples 
{

class crMotionTreeSampleManager : public crFragSampleManager
{
public:
	crMotionTreeSampleManager();
	~crMotionTreeSampleManager();

	virtual void Init(const char* path=NULL);
	virtual void Update();
	virtual void Shutdown();

	virtual void InitClient();
	virtual void UpdateClient();
	virtual void DrawClient();
	virtual void ShutdownClient();

	virtual void UpdateFragment() {}

protected:
	// PURPOSE: Override this to gain control of initialization process on the motion tree
	virtual void InitMotionTree();

	// PURPOSE: Override this to gain control of the update process on the motion tree
	virtual void UpdateMotionTree();

#if __BANK
	void AddWidgetsMotionTree();
#endif // __BANK

protected:
	crmtMotionTree& GetMotionTree() { return *m_MotionTree; }
	crmtMotionTreeScheduler& GetMotionTreeScheduler() { return m_MotionTreeScheduler; }

private:
	crmtMotionTree* m_MotionTree;
	crmtMotionTreeScheduler m_MotionTreeScheduler;
	bool m_Logging;
	int m_Verbosity;
};

} // namespace ragesamples

#endif // SAMPLE_CRMOTIONTREE_SAMPLE_CRMOTIONTREE_H
