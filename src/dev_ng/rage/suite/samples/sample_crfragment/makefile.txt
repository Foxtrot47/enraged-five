Project sample_crfragment

IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\base\samples
IncludePath $(RAGE_DIR)\suite\src

Files {
	sample_crfragment.cpp
	sample_crfragment.h
}
