// 
// sample_crfragment/sample_crfragment.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_CRFRAGMENT_SAMPLE_CRFRAGMENT_H
#define SAMPLE_CRFRAGMENT_SAMPLE_CRFRAGMENT_H

#include "sample_cranimation/sample_cranimation.h"
#include "sample_physics/demoworld.h"

// pre declarations
namespace rage
{
	class fragInst;
	class fragType;
	class crMoverInst;
	class grmMatrixSet;
};


namespace ragesamples 
{

using namespace rage;

class crFragSampleManager : public craSampleManager
{
public:
	crFragSampleManager();

	virtual void InitClient();
	virtual void ShutdownClient();
	virtual void UpdateClient();
	virtual void DrawClient();

	virtual bool ShouldFlushPfDraw();
	virtual void CreateDrawableSkeleton(rmcDrawable*& drawable, grbTargetManager*& manager, crSkeleton*& skeleton, Mat34V*& moverMtx, crCreature*& creature, grmMatrixSet*& matrixSet);
	virtual void ShutdownDrawableSkeleton();
	virtual const char* GetDefaultFile();

protected:

	// PURPOSE: Override this to change physics initialization
	virtual void InitPhysics();

	// PURPOSE: Override this to change fragment initialization
	virtual void InitFragment();

	// PURPOSE: Override this to change mover initialization
	virtual void InitMover();

	// PURPOSE: Override this to create your own frag inst (ie for natural motion)
	virtual fragInst* CreateFragInst();

	// PURPOSE: Override to load your own frag type
	virtual fragType* CreateFragType();

	// PURPOSE: Override to change the synchronization between fragment and skeleton
	virtual void UpdateFragment();

	// PURPOSE: Get the fragment instance
	// RETURNS: fragment instance reference
	fragInst& GetFragInst() const { Assert(m_FragInst); return *m_FragInst; }

	// PURPOSE: Get the fragment type
	// RETURNS: fragment type reference
	fragType& GetFragType() const { Assert(m_FragType); return *m_FragType; }

	// PURPOSE: Get the mover instance
	// RETURNS: mover instance reference
	crMoverInst& GetMoverInst() const { Assert(m_MoverInst); return *m_MoverInst; }

private:
	fragInst* m_FragInst;
	fragType* m_FragType;
	crMoverInst* m_MoverInst;
	phDemoMultiWorld m_Demos;
	bool m_UseBaseShutdown;
};

}; // namespace ragesamples

#endif  // SAMPLE_CRFRAGMENT_SAMPLE_CRFRAGMENT_H
