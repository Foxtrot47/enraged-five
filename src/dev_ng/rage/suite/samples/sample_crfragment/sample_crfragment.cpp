// 
// sample_crfragment/sample_crfragment.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_crfragment.h"

#include "creature/creature.h"
#include "crextra/mover.h"
#include "crskeleton/skeletondata.h"
#include "fragment/drawable.h"
#include "fragment/instance.h"
#include "fragment/manager.h"
#include "fragment/type.h"
#include "grmodel/matrixset.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "system/param.h"
#include "vectormath/classes.h"
#include "vectormath/legacyconvert.h"

using namespace rage;

#define DEFAULT_FILE "$/fragments/detectivedc/entity.type"
XPARAM(file);

namespace rage 
{
	EXT_PFD_DECLARE_GROUP(Physics);
	EXT_PFD_DECLARE_GROUP(Bounds);
	EXT_PFD_DECLARE_ITEM(Constraints);
};  // namespace rage

namespace ragesamples
{


crFragSampleManager::crFragSampleManager() 
: m_FragInst(NULL)
, m_FragType(NULL)
, m_MoverInst(NULL)
, m_UseBaseShutdown(false)
{
}

void crFragSampleManager::InitClient()
{
	craSampleManager::InitClient();
}

void crFragSampleManager::CreateDrawableSkeleton(rmcDrawable*& drawable, grbTargetManager*& manager, crSkeleton*& skeleton, Mat34V*& moverMtx, crCreature*& creature, grmMatrixSet*& matrixSet)
{
	// set up the physics
	InitPhysics();

	// set up the fragment (could be ragdoll or NM)
	InitFragment();

	// set up the mover
	InitMover();

	// construct a skeleton and mover to go with the fragment drawable
	if(m_FragType)
	{
		drawable = static_cast<rmcDrawable*>(m_FragType->GetCommonDrawable());
		skeleton = rage_new crSkeleton();
		skeleton->Init(m_FragInst->GetSkeleton()->GetSkeletonData());
		matrixSet = grmMatrixSet::Create(*skeleton);
		moverMtx = rage_new Mat34V(V_IDENTITY);

		skeleton->SetParentMtx(moverMtx);

		creature = rage_new crCreature;
		creature->Init(m_FrameDataFactory, m_FrameAccelerator, skeleton, moverMtx);
	}
	else
	{
		m_UseBaseShutdown = true;
		craSampleManager::CreateDrawableSkeleton(drawable, manager, skeleton, moverMtx, creature, matrixSet);
	}
}

void crFragSampleManager::ShutdownDrawableSkeleton()
{
	if(m_UseBaseShutdown)
	{
		craSampleManager::ShutdownDrawableSkeleton();
	}
	else
	{
		delete &GetSkeleton();
		delete &GetMoverMtx();
		delete &GetCreature();
		delete &GetMatrixSet();
	}
}

void crFragSampleManager::ShutdownClient()
{
	if(FRAGMGR)
	{
		FRAGMGR->Reset();
	}

	if(m_FragInst)
	{
		m_FragInst->Remove();
		delete m_FragInst;
	}
	if(m_FragType)
	{
		delete m_FragType;
	}

	if(FRAGMGR)
	{
		delete FRAGMGR;
	}

	if(m_MoverInst)
	{
		PHSIM->DeleteObject(m_MoverInst,false);
		delete m_MoverInst;
	}

	m_Demos.Shutdown();

	for (int demo = 0; demo < m_Demos.GetNumDemos(); ++demo)
	{
		delete m_Demos[demo];
	}

	phSimulator::ShutdownClass();
	phDemoWorld::ClassShutdown();

	craSampleManager::ShutdownClient();
}

void crFragSampleManager::UpdateClient()
{
	UpdateFragment();

	m_Demos.Update();

	craSampleManager::UpdateClient();
}

void crFragSampleManager::UpdateFragment()
{
	Mat34V& moverMtx = GetMoverMtx();

	crSkeleton& skel = GetSkeleton();
	crSkeleton& fragSkel = *m_FragInst->GetSkeleton();

	if (PHLEVEL->IsActive(m_FragInst->GetLevelIndex()))
	{
		m_FragInst->PoseSkeletonFromBounds();

		sysMemCpy(skel.GetObjectMtxs(), fragSkel.GetObjectMtxs(), sizeof(Mat34V)*skel.GetSkeletonData().GetNumBones());

		moverMtx = m_FragInst->GetMatrix();
		skel.InverseUpdate();
	}
	else
	{
		m_FragInst->SetMatrix(moverMtx);

		sysMemCpy(fragSkel.GetObjectMtxs(), skel.GetObjectMtxs(), sizeof(Mat34V)*skel.GetSkeletonData().GetNumBones());

		fragSkel.InverseUpdate();

		m_FragInst->PoseBoundsFromSkeleton(true,true);
		PHLEVEL->UpdateObjectLocation(m_FragInst->GetLevelIndex());
	}
}

void crFragSampleManager::DrawClient()
{
	craSampleManager::DrawClient();

	m_Demos.UpdateMouse();
	m_Demos.Draw();
}

bool crFragSampleManager::ShouldFlushPfDraw()
{
	return m_Demos.GetCurrentWorld()->ShouldUpdate(); 
}

const char* crFragSampleManager::GetDefaultFile()
{
	const char* file = DEFAULT_FILE;
	PARAM_file.Get(file);
	return file;
}

void crFragSampleManager::InitPhysics()
{
	PFD_GROUP_ENABLE(Physics, true);
	PFD_GROUP_ENABLE(Bounds, true);
	PFD_ITEM_ENABLE(Constraints, true);

	phConfig::EnableRefCounting();

	phSimulator::InitClass();
	phDemoWorld::InitClass();

	m_Demos.AddDemo(rage_new phDemoWorld("sample_crfragment"));
	m_Demos.GetCurrentWorld()->Init(1000, 128, 128, Vec3V(-10000.0f, -10000.0f, -10000.0f),  Vec3V(10000.0f, 10000.0f, 10000.0f));
	m_Demos.GetCurrentWorld()->Activate();
	m_Demos.GetCurrentWorld()->ConstructTerrainPlane();
}

void crFragSampleManager::InitFragment()
{
	rage_new fragManager;

	m_FragType = CreateFragType();
	Assert(m_FragType);

	m_FragInst = CreateFragInst();
	Assert(m_FragInst);

	m_FragInst->Insert(false);
	m_FragInst->SetManualSkeletonUpdate(true);
	m_FragInst->PutIntoCache();
}

void crFragSampleManager::InitMover()
{
	m_MoverInst = rage_new crMoverInst();
	m_MoverInst->Init(0.35f, 1.85f, 0.2f, ORIGIN, 97.f, m_FragInst);
	m_MoverInst->SetMatrix(Mat34V(V_IDENTITY));

	PHSIM->AddActiveObject(m_MoverInst, true);
}

fragInst* crFragSampleManager::CreateFragInst()
{
	return rage_new fragInst(m_FragType, M34_IDENTITY);
}

fragType* crFragSampleManager::CreateFragType()
{
	return fragType::Load(GetDefaultFile());
}


}; // namespace ragesamples
