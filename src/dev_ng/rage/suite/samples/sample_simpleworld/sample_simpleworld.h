// 
// sample_simpleworld/sample_simpleworld.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_SIMPLEWORLD_SAMPLE_SIMPLEWORLD_H
#define SAMPLE_SIMPLEWORLD_SAMPLE_SIMPLEWORLD_H

#include "animmgr.h"
#include "entitymgr.h"
#include "simpleworld.h"

#include "atl/array.h"
#include "atl/map.h"
#include "atl/string.h"
#include "grcore/texture.h"
#include "grcore/texturereference.h"
#include "pheffects/winddisturbancebox.h"
#include "pheffects/winddisturbancecyclone.h"
#include "sample_fragment/fragworld.h"
#include "sample_rmcore/sample_rmcore.h"
#include "script/thread.h"
#include "shaderlib/rage_constants.h"

#define USETRANSPARENTCOMPOSITERENDERTARGET (RAGE_ENCODEOPAQUECOLOR && __PPU)
#define MAX_WIND_CYCLONES 5
#define MAX_WIND_BOXES 5

//-----------------------------------------------------------------------------

namespace rage
{
	class bkBank;
	class crTagEditorWidget;
	class grmShader;
	class grcTextureReference;
	class grcGPRAllocation;
}

namespace rageSimpleScript
{
    class SimpleWorldScript;
}

//-----------------------------------------------------------------------------

using namespace rage;

namespace ragesamples
{
    // convenient #defines for file types and parTreeNode names
    #define FILE_LIST "filist"
    #define ENTITY_LIST "elist"
    #define FRAG_LIST "flist"
    #define FRAG_DRAW_LIST "fdlist"
    #define ANIM_LIST "alist"
    #define CLIP_LIST "clist"

    class sfiFileListLoader;

//-----------------------------------------------------------------------------
class rmcSampleSimpleWorld : public ragesamples::rmcSampleManager
{
public:
	rmcSampleSimpleWorld();
	virtual ~rmcSampleSimpleWorld();

    // must call this right after constructor is called:
    virtual void Init(const char* path=NULL);

    // must call this before the destructor is called:
    virtual void Shutdown();

protected:
	//grcSampleManager 
	virtual const char* GetSampleName() const
	{
		return "sample_simpleWorld";
	}

    virtual void InitFileListLoader();

	virtual void InitClient();
	virtual void InitCamera();
	virtual void InitLights();
	bool		 InitGlobalTextureList(const char* globalTexListPath);

	virtual void Draw();
	virtual void DrawHelpClient();
	virtual void UpdateClient();
	virtual void DrawClient();
	virtual void DrawClientTransparent();
	virtual void DrawClientDebug();
	virtual void RenderIntoShadowMaps();
	virtual void RenderIntoShadowCollector();

	virtual void ShutdownClient();

	virtual void CreateGfxFactories();
	virtual void RegisterTechniqueGroups();
	virtual void DestroyGfxFactories();

	const char* SaveEntityList(const char* filePath);
	bool LoadEntityList(const char* filePath);
	const char* SaveAnimationList(const char* filePath);
	bool LoadAnimationList(const char* filePath);
    const char* SaveClipList(const char* filePath);
    bool LoadClipList(const char* filePath);
	const char* SaveFragmentList(const char* filePath);
	bool LoadFragmentList(const char* filePath);
    const char* SaveFragmentDrawableList(const char* filePath);
    bool LoadFragmentDrawableList(const char* filePath);

    void UpdateClampFPSSettingCallback( bool clamp, float fps, float maxFrameTime );

	void AnimMgrCurrentFrameChanged( RvAnimMgr* pMgr );
	void AnimMgrPlaybackModeChanged( RvAnimMgr* pMgr );
	void AnimMgrAnimIndexChanged( RvAnimMgr* pMgr );
	void AnimMgrClipIndexChanged( RvAnimMgr* pMgr );
	void AnimMgrSequenceModeChanged( RvAnimMgr* pMgr );
	void AnimMgrFrameRangeChanged( RvAnimMgr* pMgr );
	void AnimMgrMoverModeChanged( RvAnimMgr* pMgr );
	void AnimMgrAllowMoverChanged( RvAnimMgr* pMgr );
	void AnimMgrAllowRootTranslationChanged( RvAnimMgr* pMgr);
	void AnimMgrMoverDebugDrawChanged( RvAnimMgr* pMgr );
	void AnimMgrLoopAnimationChanged( RvAnimMgr* pMgr );

	void AnimMgrAnimListDoubleClickHandler( int key );
	void AnimMgrClipListDoubleClickHandler( int key );

	void AnimMgrResetMoverHandler();

	void ClipEditorTimeChangedHandler( crTagEditorWidget* widget, float time );

public:
	bool LoadEntity( const char* filePath );
    bool LoadEntity( const char* filePath, Matrix34 &transform );
	bool LoadFragEntity( const char* filePath );
	bool LoadFragDrawableEntity( const char* filePath );
    bool LoadParticleEffect( const char* filePath );
	bool LoadAnimation(const char* filePath);
    bool LoadClip(const char* filePath);

    void UnloadEntity( const char* filePath );
    void UnloadAnimation( const char* filePath );
    void UnloadClip( const char* filePath );

protected:
	static void DeviceLost();
	static void DeviceReset();

    bool SaveFileList( const char *pFilePath );
    bool LoadFileList( const char *pFilePath );

    bool SaveXmlFile( const char *pFilePath );
    bool LoadXmlFile( const char *pFilePath );

    bool LoadScriptFile( const char *pFilePath );    

    // callbacks for FILELISTLOADER file save/load
    static rage::atString SaveGlobalSearchListPath( void *pObj, const char * ) { return ((rmcSampleSimpleWorld *)pObj)->m_GlobalTexSearchPath; }
    static bool InitGlobalTexSearchPathFunc( void *pObj, const char *pFilePath ) { ((rmcSampleSimpleWorld *)pObj)->m_GlobalTexSearchPath = pFilePath; return true; }

    static rage::atString SaveGlobalTexListPath( void *pObj, const char * ) { return ((rmcSampleSimpleWorld *)pObj)->m_GlobalTexListPath; }
    static bool InitGlobalTexListPathFunc( void *pObj, const char *pFilePath ) { ((rmcSampleSimpleWorld *)pObj)->InitGlobalTextureList(pFilePath); return true; }

    static rage::atString SaveEntityListFile( void *pObj, const char *pFilePath ) { return atString(((rmcSampleSimpleWorld *)pObj)->SaveEntityList(pFilePath)); }
    static bool LoadEntityListFile( void *pObj, const char *pFilePath ) { return ((rmcSampleSimpleWorld *)pObj)->LoadEntityList(pFilePath); }
    static rage::atString SaveAnimationListFile( void *pObj, const char *pFilePath ) { return atString(((rmcSampleSimpleWorld *)pObj)->SaveAnimationList(pFilePath)); }
    static bool LoadAnimationListFile( void *pObj, const char *pFilePath ) { return ((rmcSampleSimpleWorld *)pObj)->LoadAnimationList(pFilePath); }
    static rage::atString SaveClipListFile( void *pObj, const char *pFilePath ) { return atString(((rmcSampleSimpleWorld *)pObj)->SaveClipList(pFilePath)); }
    static bool LoadClipListFile( void *pObj, const char *pFilePath ) { return ((rmcSampleSimpleWorld *)pObj)->LoadClipList(pFilePath); }
    static rage::atString SaveFragmentListFile( void *pObj, const char *pFilePath ) { return atString(((rmcSampleSimpleWorld *)pObj)->SaveFragmentList(pFilePath)); }
    static bool LoadFragmentListFile( void *pObj, const char *pFilePath ) { return ((rmcSampleSimpleWorld *)pObj)->LoadFragmentList(pFilePath); }
    static rage::atString SaveFragmentDrawableListFile( void *pObj, const char *pFilePath ) { return atString(((rmcSampleSimpleWorld *)pObj)->SaveFragmentDrawableList(pFilePath)); }
    static bool LoadFragmentDrawableListFile( void *pObj, const char *pFilePath ) { return ((rmcSampleSimpleWorld *)pObj)->LoadFragmentDrawableList(pFilePath); }
    
    static bool LoadEntityFile( void *pObj, const char *pFilePath ) { return ((rmcSampleSimpleWorld *)pObj)->LoadEntity(pFilePath); }
    static bool LoadFragEntityFile( void *pObj, const char *pFilePath ) { return ((rmcSampleSimpleWorld *)pObj)->LoadFragEntity(pFilePath); }
    static bool LoadFragDrawableEntityFile( void *pObj, const char *pFilePath ) { return ((rmcSampleSimpleWorld *)pObj)->LoadFragDrawableEntity(pFilePath); }
    static bool LoadAnimationFile( void *pObj, const char *pFilePath ) { return ((rmcSampleSimpleWorld *)pObj)->LoadAnimation(pFilePath); }
    static bool LoadClipFile( void *pObj, const char *pFilePath ) { return ((rmcSampleSimpleWorld *)pObj)->LoadClip(pFilePath); }

    static rage::atString SaveLightsFile( void *pObj, const char *pFilePath );
    static bool LoadLightsFile( void *pObj, const char *pFilePath );
    static rage::atString SaveCameraFile( void *pObj, const char *pFilePath );
    static bool LoadCameraFile( void *pObj, const char *pFilePath );

#if USE_RMPTFX
	bool CreateParticleEmitter(const char* filePath);

    static bool LoadParticleEmitterFile( void *pObj, const char* pFilePath ) { return ((rmcSampleSimpleWorld *)pObj)->CreateParticleEmitter(pFilePath); }
#endif //USE_RMPTFX

    // callbacks for FILELISTLOADER parTreeNode save/load
    static bool SaveGlobalTexSearchPathTreeNode( void *pObj, rage::parTreeNode *pTreeNode );
    static bool LoadGlobalTexSearchPathTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );

    static bool SaveGlobalTexListPathTreeNode( void *pObj, rage::parTreeNode *pTreeNode );
    static bool LoadGlobalTexListPathTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );

    static bool SaveEntityListTreeNode( void *pObj, rage::parTreeNode *pTreeNode );
    static bool LoadEntityListTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );
    static bool SaveAnimationListTreeNode( void *pObj, rage::parTreeNode *pTreeNode );
    static bool LoadAnimationListTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );
    static bool SaveClipListTreeNode( void *pObj, rage::parTreeNode *pTreeNode );
    static bool LoadClipListTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );
    static bool SaveFragmentListTreeNode( void *pObj, rage::parTreeNode *pTreeNode );
    static bool LoadFragmentListTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );

    static bool LoadEntityTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );
    static bool LoadFragEntityTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );
    static bool LoadFragDrawableEntityTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );
    static bool LoadAnimationTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );
    static bool LoadClipTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );

    static bool SaveLightsTreeNode( void *pObj, rage::parTreeNode *pTreeNode );
    static bool LoadLightsTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );
    static bool SaveCameraTreeNode( void *pObj, rage::parTreeNode *pTreeNode );
    static bool LoadCameraTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );

#if USE_RMPTFX
    static bool LoadParticleEmitterTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );

    static bool LoadRmPtfxPreLoadTreeNode( void * pObj, const rage::parTreeNode *pTreeNode );
#endif //USE_RMPTFX

    static bool SaveFragDrawableListTreeNode( void *pObj, rage::parTreeNode *pTreeNode );
    static bool LoadFragDrawableListTreeNode( void *pObj, const rage::parTreeNode *pTreeNode );

	void	FocusCamera(const spdSphere& focusSphere);
	void	FrameAllEntites();
	float	DistanceFromCamera(const Vector3& point);
    virtual void OnRemoveEntity( SimpleWorldEntity *pEntity );
	virtual void OnLoadEntity( SimpleWorldEntity * )	{}

	//~R
	int GetTriangleCount(/*const rage::fragShaft& shaft*/);
	int GetVertexCount();
	int GetGeometryCount();


protected:
	int			m_MaxOctreeNodes;
	int			m_MaxActivePhysicsObjects;
	int			m_MaxPhysicsObjects;
	atString	m_GlobalTexSearchPath;
    atString	m_GlobalTexListPath;

	fragWorld*	m_pFragWorld;

	RvAnimMgr	m_globalAnimMgr;

	ioMapper	m_Mapper;
	ioValue		m_HotKeyMayaCam;
	ioValue		m_HotKeyLhCam;
	ioValue		m_HotKeyQuakeCam;
	ioValue		m_HotKeyReset;
	ioValue		m_HotKeyToggleAnim;

	bool		m_bManageParser;
	bool		m_bShowFragmentHUD;
	bool		m_bShowAnimationHUD;

	int			m_viewportRenderMode;

    SimpleWorldDrawableEntity *m_pLastEntityLoaded;
	SimpleWorldFragmentEntity *m_pLastFragEntityLoaded;
	SimpleWorldFragDrawableEntity *m_pLastFragDrawableEntityLoaded;

	grmShader* m_GlassShader;

    // when initializing, we have to remember these presets for later
    bool m_forceInitCamera;
    bool m_initCamera;
    bool m_forceInitLights;
    bool m_initLights;
    grcSampleLightPreset m_lightPreset;
    grcSampleCameraPreset m_cameraPreset;

    bool m_constructTerrainPlane;

    int m_autoPlayAnimation;
    bool m_autoSequenceAnimation;

	phWindCycloneGroup*	m_pCycloneGroup;

#if USETRANSPARENTCOMPOSITERENDERTARGET
	grcCompositeRenderTarget* m_CompositeRenderTarget;
#endif // USETRANSPARENTCOMPOSITERENDERTARGET

#if __BANK
	virtual void BankSaveEntityList();
	virtual void BankLoadEntityList();
	virtual void BankLoadEntity();
	
    virtual void BankSaveAnimationList();
	virtual void BankLoadAnimationList();
	virtual void BankLoadAnimation();

    virtual void BankSaveClipList();
    virtual void BankLoadClipList();
    virtual void BankLoadClip();
	virtual void BankSaveAllClips();

    virtual void BankSaveFragmentList();
	virtual void BankLoadFragmentList();
	virtual void BankLoadFragmentEntity();

    virtual void BankSaveFragDrawableList();
    virtual void BankLoadFragDrawableList();
    virtual void BankLoadFragDrawable();

    virtual void BankSaveFileList();    // still here for compatibility, but otherwise, this function is no longer used
    virtual void BankLoadFileList();

    virtual void BankSaveFileXml();
    virtual void BankLoadFileXml();

    virtual void RefreshSimpleWorld();

	virtual void RenderModeChanged();
	
#if USE_RMPTFX
	virtual void BankCreateParticleEmitter();
#endif //USE_RMPTFX

	//grcSampleManager 
	virtual void AddWidgetsClient();

protected:
	bkBank* m_pGlobalAnimBank;
	bkBank*	m_pSampleBank;
#endif //__BANK

    virtual void ResetSimpleWorld();

    void InsertEntity( SimpleWorldEntity *pEntity, bool bLoadTextures=true );
    void RemoveEntity( SimpleWorldEntity *pEntity );
};

//-----------------------------------------------------------------------------

class SimpleWorldGlobalTexture
{
public:
	SimpleWorldGlobalTexture() 
		: m_pTexture(NULL)
		, m_pTextureReference(NULL) {};
	~SimpleWorldGlobalTexture();
public:
	atString				m_FileName;
	grcTexture*				m_pTexture;
	grcTextureReference*	m_pTextureReference;
};

inline SimpleWorldGlobalTexture::~SimpleWorldGlobalTexture()
{
	if(m_pTexture)
		m_pTexture->Release();
	if(m_pTextureReference)
		delete m_pTextureReference;
}

//-----------------------------------------------------------------------------

class SimpleWorldGlobalTextureMgr
{
public:
	void AddTextureToGlobalList(const char* shaderRefName, const char* fileName);
	bool LoadGlobalList(const char* searchPath);

	const grcTexture*			GetTexture(const char* shaderRefName) const;
	const grcTextureReference*	GetTextureReference(const char* shaderRefName) const;

protected:
	SimpleWorldGlobalTextureMgr() {};

private:
	atMap<atString, SimpleWorldGlobalTexture>	m_TextureTable;
};

//-----------------------------------------------------------------------------

inline const grcTexture* SimpleWorldGlobalTextureMgr::GetTexture(const char* shaderRefName) const
{
	const SimpleWorldGlobalTexture* pGlobalTex = m_TextureTable.Access(shaderRefName);
	if(pGlobalTex)
		return pGlobalTex->m_pTexture;
	else
		return NULL;
}

inline const grcTextureReference* SimpleWorldGlobalTextureMgr::GetTextureReference(const char* shaderRefName) const
{
	const SimpleWorldGlobalTexture* pGlobalTex = m_TextureTable.Access(shaderRefName);
	if(pGlobalTex)
		return pGlobalTex->m_pTextureReference;
	else
		return NULL;
}

//-----------------------------------------------------------------------------

typedef atSingleton<SimpleWorldGlobalTextureMgr> smpWorldGlobalTextureMgrSingleton;

#define SMPWORLDGTEXMGR				::ragesamples::smpWorldGlobalTextureMgrSingleton::InstanceRef()
#define INIT_SMPWORLDGTEXMGR		::ragesamples::smpWorldGlobalTextureMgrSingleton::Instantiate()
#define SHUTDOWN_SMPWORLDGTEXMGR	::ragesamples::smpWorldGlobalTextureMgrSingleton::Destroy()

//-----------------------------------------------------------------------------

//------------------------------ console setup
#if !__FINAL
void SetConsoleSimpleWorld( rmcSampleSimpleWorld* smpwrld ) ; 
#endif


} //End namespace ragesamples

#endif //SAMPLE_SIMPLEWORLD_SAMPLE_SIMPLEWORLD_H
