// 
// sample_simpleworld/simplescript.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "simplescript.h"

#include "devcam/cammgr.h"
#include "devcam/devcam.h"
#include "devcam/flycam.h"
#include "devcam/freecam.h"
#include "devcam/lhcam.h"
#include "devcam/mayacam.h"
#include "devcam/polarcam.h"
#include "devcam/roamcam.h"
#include "devcam/trackcam.h"
#include "devcam/warcam.h"

#include "grpostfx/postfx.h"
#include "input/pad.h"
#include "script/wrapper.h"
#include "vector/vector3.h"
#include "vector/vector4.h"

#include "entitymgr.h"
#include "sample_simpleworld.h"
#include "simpleworld.h"

using namespace rage;
using namespace ragesamples;

namespace rageSimpleScript
{

//#############################################################################

static ragesamples::rmcSampleSimpleWorld* s_pSampleSimpleWorld = NULL;

//#############################################################################

static void DrawString( const char *s, bool fadeWithTime )
{
    s_pSampleSimpleWorld->DrawString( s, fadeWithTime );
}

static void SetDrawStringPos( float x, float y )
{
    s_pSampleSimpleWorld->SetDrawStringPos( x, y );
}

//#############################################################################

enum EScrKey
{
    E_LUP=0,
    E_LDOWN,
    E_LLEFT,
    E_LRIGHT,
    E_RUP,
    E_RDOWN,
    E_RLEFT,
    E_RRIGHT,
    E_START,
    E_SELECT,
    E_LBUTTON,
    E_RBUTTON,
    E_LTRIGGER,
    E_RTRIGGER
};

static bool IsPadKeyDown( int key )
{
    switch ( static_cast<EScrKey>(key) )
    {
    case E_LUP:
        return (ioPad::GetPad(0).GetPressedButtons() & ioPad::LUP) == ioPad::LUP;

    case E_LDOWN:
        return (ioPad::GetPad(0).GetPressedButtons() & ioPad::LDOWN) == ioPad::LDOWN;

    case E_LLEFT:
        return (ioPad::GetPad(0).GetPressedButtons() & ioPad::LLEFT) == ioPad::LLEFT;

    case E_LRIGHT:
        return (ioPad::GetPad(0).GetPressedButtons() & ioPad::LRIGHT) == ioPad::LRIGHT;

    case E_RUP:
        return (ioPad::GetPad(0).GetPressedButtons() & ioPad::RUP) == ioPad::RUP;

    case E_RDOWN:
        return (ioPad::GetPad(0).GetPressedButtons() & ioPad::RDOWN) == ioPad::RDOWN;

    case E_RLEFT:
        return (ioPad::GetPad(0).GetPressedButtons() & ioPad::RLEFT) == ioPad::RLEFT;

    case E_RRIGHT:
        return (ioPad::GetPad(0).GetPressedButtons() & ioPad::RRIGHT) == ioPad::RRIGHT;

    case E_START:
        return (ioPad::GetPad(0).GetPressedButtons() & ioPad::START) == ioPad::START;

    case E_SELECT:
        return (ioPad::GetPad(0).GetPressedButtons() & ioPad::SELECT) == ioPad::SELECT;

    case E_LBUTTON:
        return (ioPad::GetPad(0).GetPressedButtons() & ioPad::L1) == ioPad::L1;

    case E_RBUTTON:
        return (ioPad::GetPad(0).GetPressedButtons() & ioPad::R1) == ioPad::R1;

    case E_LTRIGGER:
        return (ioPad::GetPad(0).GetPressedButtons() & ioPad::L2) == ioPad::L2;

    case E_RTRIGGER:
        return (ioPad::GetPad(0).GetPressedButtons() & ioPad::R2) == ioPad::R2;
    }

    return false;
}

//#############################################################################

static void SetLightDir( int id, const Vector3 &dir )
{
    Vector3 tmpDir = dir;
    tmpDir.Normalize();
    
    Vector3 tgtD;
    tgtD.Add( VEC3V_TO_VECTOR3(s_pSampleSimpleWorld->GetLightManipulators()[id].lightSrcMtx.d()), tmpDir );
    
    s_pSampleSimpleWorld->GetLightManipulators()[id].lightTgtMtx.Setd(VECTOR3_TO_VEC3V(tgtD));

    if ( s_pSampleSimpleWorld->GetLightGroup()->GetActiveCount() <= id )
    {
        s_pSampleSimpleWorld->GetLightGroup()->SetActiveCount( id + 1 );
    }
}

static void SetLightPos( int id, const Vector3 &pos )
{
    s_pSampleSimpleWorld->GetLightManipulators()[id].lightSrcMtx.Setd(VECTOR3_TO_VEC3V(pos));

    if ( s_pSampleSimpleWorld->GetLightGroup()->GetActiveCount() <= id )
    {
        s_pSampleSimpleWorld->GetLightGroup()->SetActiveCount( id + 1 );
    }
}

static void SetLightColor( int id, int r, int g, float b )
{
    s_pSampleSimpleWorld->GetLightGroup()->SetColor( id, r/255.0f, g/255.0f, b/255.0f );

    if ( s_pSampleSimpleWorld->GetLightGroup()->GetActiveCount() <= id )
    {
        s_pSampleSimpleWorld->GetLightGroup()->SetActiveCount( id + 1 );
    }
}

static void SetLightIntensity( int id, float intensity )
{
    s_pSampleSimpleWorld->GetLightGroup()->SetIntensity( id, intensity );

    if ( s_pSampleSimpleWorld->GetLightGroup()->GetActiveCount() <= id )
    {
        s_pSampleSimpleWorld->GetLightGroup()->SetActiveCount( id + 1 );
    }
}

static void SetLightFalloff( int id, float falloff )
{
    s_pSampleSimpleWorld->GetLightGroup()->SetFalloff( id, falloff );

    if ( s_pSampleSimpleWorld->GetLightGroup()->GetActiveCount() <= id )
    {
        s_pSampleSimpleWorld->GetLightGroup()->SetActiveCount( id + 1 );
    }
}

static void SetLightType( int id, int type )
{
    s_pSampleSimpleWorld->GetLightGroup()->SetLightType( id, static_cast<grcLightGroup::grcLightType>(type) );

    if ( s_pSampleSimpleWorld->GetLightGroup()->GetActiveCount() <= id )
    {
        s_pSampleSimpleWorld->GetLightGroup()->SetActiveCount( id + 1 );
    }
}

static void SetLightOversaturate( int id, bool /*oversaturate */ )
{
    //// s_pSampleSimpleWorld->GetLightGroup()->SetOversaturate( id, oversaturate );

    if ( s_pSampleSimpleWorld->GetLightGroup()->GetActiveCount() <= id )
    {
        s_pSampleSimpleWorld->GetLightGroup()->SetActiveCount( id + 1 );
    }
}

//#############################################################################

// general 
static void InitCamera( const Vector3 &pos, const Vector3 &target, int type )
{
    s_pSampleSimpleWorld->GetCamMgr().Init( pos, target, static_cast<dcamCamMgr::eCamera>(type) );
    s_pSampleSimpleWorld->SetViewMode( 0 );
}

static void SetCameraType( int type )
{
    s_pSampleSimpleWorld->GetCamMgr().SetCamera( static_cast<dcamCamMgr::eCamera>(type) );
    s_pSampleSimpleWorld->SetViewMode( 0 );
}

// fly cam
static void SetFlyCamSpeed( float speed )
{
    dcamFlyCam &flyCam = (dcamFlyCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_FLY );
    flyCam.SetSpeed( speed );
}

// free cam
static void SetFreeCamLocation( const Vector3 &loc )
{
    dcamFreeCam &freeCam = (dcamFreeCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_FREE );
    freeCam.SetLocation( loc );
}

static void SetFreeCamOffsetLocation( const Vector3 &offset )
{
    dcamFreeCam &freeCam = (dcamFreeCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_FREE );
    freeCam.OffsetLocation( offset );
}

static void RotateFreeCam( const Vector3 &rot )
{
    dcamFreeCam &freeCam = (dcamFreeCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_FREE );
    freeCam.Rotate( rot );
}

static void SetFreeCamSpeeds( float transSpeed, float rotSpeed )
{
    dcamFreeCam::SetSpeed( transSpeed, rotSpeed );
}

// lh cam

static void SetLHCamFollow( const char *pEntityName )
{
    SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( pEntityName );
    if ( !pEntity )
    {
        Errorf( "SetLHCamFollow error: no entity named %s", pEntityName );
        return;
    }

    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    lhCam.SetFollow( pEntity->GetTransform().d );
}

static void StopLHCamFollow()
{
    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    lhCam.StopFollow();
}

static void SetLHCamOffset( const Vector3 &offset )
{
    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    lhCam.SetOffset( offset );
}

static void SetLHCamFollowOffset( bool useOffset )
{
    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    lhCam.SetFollowOffset( useOffset );
}

static void SetLHCamOrient( Vector3 &orient )
{
    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    lhCam.SetOrient( &orient );
}

static void SetLHCamLinearRate( float rate )
{
    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    lhCam.SetLinearRate( rate );
}

static void SetLHCamAngularRate( float rate )
{
    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    lhCam.SetAngularRate( rate );
}

static void SetLHCamOrientRate( float rate )
{
    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    lhCam.SetOrientRate( rate );
}

static void SetLHCamDistance( float dist )
{
    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    lhCam.SetDistance( dist );
}

static void SetLHCamAzimuth( float azm )
{
    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    lhCam.SetAzimuth( azm );
}

static void SetLHCamIncline( float inc )
{
    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    lhCam.SetIncline( inc );
}

static float GetLHCamDistance()
{
    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    return lhCam.GetDistance();
}

static float GetLHCamAzimuth()
{
    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    return lhCam.GetAzimuth();
}

static float GetLHCamIncline()
{
    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    return lhCam.GetIncline();
}

static void GetLHCamOffset( Vector3 &offset )
{
    dcamLhCam &lhCam = (dcamLhCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_LH );
    offset = lhCam.GetOffset();
}

// maya cam

static float GetMayaCamFocusDistance()
{
    dcamMayaCam &mayaCam = (dcamMayaCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_MAYA );
    return mayaCam.GetFocusDistance();
}

static void SetMayaCamDefaultSpeed( float s )
{
    dcamMayaCam &mayaCam = (dcamMayaCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_MAYA );
    mayaCam.SetDefaultSpeed( s );
}

static void SetMayaCamRotateSpeed( float s )
{
    dcamMayaCam &mayaCam = (dcamMayaCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_MAYA );
    mayaCam.SetRotateSpeed( s );
}

static void SetMayaCamFocusPoint( const Vector3 &p )
{
    dcamMayaCam &mayaCam = (dcamMayaCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_MAYA );
    mayaCam.SetFocusPoint( p );
}

// polar cam

static void SetPolarCamFollow( const char *pEntityName )
{
    SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( pEntityName );
    if ( !pEntity )
    {
        Errorf( "SetPolarCamFollow error: no entity named %s", pEntityName );
        return;
    }

    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    polarCam.SetFollow( pEntity->GetTransform().d );
}

static void StopPolarCamFollow()
{
    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    polarCam.StopFollow();
}

static void SetPolarCamOffset( const Vector3 &offset )
{
    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    polarCam.SetOffset( offset );
}

static void SetPolarCamFollowOffset( bool useOffset )
{
    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    polarCam.SetFollowOffset( useOffset );
}

static void SetPolarCamOrient( Vector3 &orient )
{
    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    polarCam.SetOrient( &orient );
}

static void SetPolarCamLinearRate( float rate )
{
    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    polarCam.SetLinearRate( rate );
}

static void SetPolarCamAngularRate( float rate )
{
    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    polarCam.SetAngularRate( rate );
}

static void SetPolarCamOrientRate( float rate )
{
    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    polarCam.SetOrientRate( rate );
}

static void SetPolarCamDistance( float dist )
{
    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    polarCam.SetDistance( dist );
}

static void SetPolarCamAzimuth( float azm )
{
    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    polarCam.SetAzimuth( azm );
}

static void SetPolarCamIncline( float inc )
{
    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    polarCam.SetIncline( inc );
}

static float GetPolarCamDistance()
{
    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    return polarCam.GetDistance();
}

static float GetPolarCamAzimuth()
{
    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    return polarCam.GetAzimuth();
}

static float GetPolarCamIncline()
{
    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    return polarCam.GetIncline();
}

static void GetPolarCamOffset( Vector3 &offset )
{
    dcamPolarCam &polarCam = (dcamPolarCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_POLAR );
    offset = polarCam.GetOffset();
}

// track cam

static void SetTrackCamAxisOrigin( const Vector3 &origin )
{
    dcamTrackCam &trackCam = (dcamTrackCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_TRACK );
    trackCam.SetAxisOrigin( origin );
}

static void SetTrackCamTopView( float dist )
{
    dcamTrackCam &trackCam = (dcamTrackCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_TRACK );
    trackCam.TopView( dist );
}

static void SetTrackCamReverse( bool rev )
{
    dcamTrackCam &trackCam = (dcamTrackCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_TRACK );
    trackCam.SetReverse( rev );
}

static void SetTrackCamWheelScale( float scale )
{
    dcamTrackCam &trackCam = (dcamTrackCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_TRACK );
    trackCam.SetWheelScale( scale );
}

static void SetTrackCamMouseWindowOverride( float w, float h )
{
    dcamTrackCam &trackCam = (dcamTrackCam &)s_pSampleSimpleWorld->GetCamMgr().GetCamera( dcamCamMgr::CAM_TRACK );
    trackCam.SetMouseWindowOverride( w, h );
}

//#############################################################################

static void SetEntityStringPos( const char* pEntityName, const Vector3 &pos )
{
    SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( pEntityName );
    if ( pEntity )
    {
        Matrix34 m = const_cast<Matrix34 &>( pEntity->GetTransform() );
        m.MakeTranslate( pos );
        pEntity->SetTransform( m );
    }
    else
    {
        Errorf( "SetEntityStringPos error: no entity named %s", pEntityName );
    }
}

static void SetEntityIntPos( int entityId, const Vector3 &pos )
{
    SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( entityId );
    if ( pEntity )
    {
        Matrix34 m = const_cast<Matrix34 &>( pEntity->GetTransform() );
        m.MakeTranslate( pos );
        pEntity->SetTransform( m );
    }
    else
    {
        Errorf( "SetEntityIntPos error: no entity at index %d", entityId );
    }
}

static void GetEntityStringPos( const char *pEntityName, Vector3 &pos )
{
    const SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( pEntityName );
    if ( pEntity )
    {
        pos = pEntity->GetTransform().d;
    }
    else
    {
        Errorf( "GetEntityStringPos error: no entity named %s", pEntityName );
    }
}

static void GetEntityIntPos( int entityId, Vector3 &pos )
{
    const SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( entityId );
    if ( pEntity )
    {
        pos = pEntity->GetTransform().d;
    }
    else
    {
        Errorf( "GetEntityIntPos error: no entity at index %d", entityId );
    }
}

static int LoadEntity( const char* name, const char* filePath )
{
    int count = SMPWORLDEMGR.GetEntityCount();
    if ( s_pSampleSimpleWorld->LoadEntity(filePath) )
    {
        SimpleWorldEntity *pEntity = const_cast<SimpleWorldEntity *>( SMPWORLDEMGR.GetEntity(count) );
        pEntity->SetEntityName( name );
        return count;
    }

    return -1;
}

static int LoadFragEntity( const char* name, const char* filePath )
{
    int count = SMPWORLDEMGR.GetEntityCount();
    if ( s_pSampleSimpleWorld->LoadFragEntity(filePath) )
    {
        SimpleWorldEntity *pEntity = const_cast<SimpleWorldEntity *>( SMPWORLDEMGR.GetEntity(count) );
        pEntity->SetEntityName( name );
        return count;
    }

    return -1;
}

static int LoadFragDrawableEntity( const char* name, const char* filePath )
{
    int count = SMPWORLDEMGR.GetEntityCount();
    if ( s_pSampleSimpleWorld->LoadFragDrawableEntity(filePath) )
    {
        SimpleWorldEntity *pEntity = const_cast<SimpleWorldEntity *>( SMPWORLDEMGR.GetEntity(count) );
        pEntity->SetEntityName( name );
        return count;
    }

    return -1;
}

static void RemoveEntityString( const char* pEntityName )
{
    SMPWORLDEMGR.RemoveEntity( pEntityName );
}

static void RemoveEntityInt( int entityId )
{
    SMPWORLDEMGR.RemoveEntity( entityId );
}

//#############################################################################

static int LoadAnimation( const char* filePath )
{
    int count = RvAnimMgr::GetAnimationCount();
    if ( s_pSampleSimpleWorld->LoadAnimation(filePath) )
    {
        return count;
    }

    return -1;
}

static void PlayEntityStringAnimation( const char* pEntityName, int animId, bool update, bool /*overrideGlobal*/ )
{
    const SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( pEntityName );
    if ( pEntity )
    {
        const SimpleWorldDrawableEntity *pDrawableEntity = dynamic_cast<const SimpleWorldDrawableEntity *>( pEntity );
        if ( pDrawableEntity )
        {
            const_cast<SimpleWorldDrawableEntity *>(pDrawableEntity)->GetAnimMgr()->SetCurrentAnimationId( animId );
            const_cast<SimpleWorldDrawableEntity *>(pDrawableEntity)->SetUpdateAnim( update );
            return;
        }

        Errorf( "PlayEntityStringAnimation error: %s is not a Drawable or a Fragment Entity", pEntityName );
    }
    else
    {
        Errorf( "PlayEntityStringAnimation error: no entity named %s", pEntityName );
    }
}

static void PlayEntityIntAnimation( int entityId, int animId, bool update, bool /*overrideGlobal*/ )
{
    const SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( entityId );
    if ( pEntity )
    {
        const SimpleWorldDrawableEntity *pDrawableEntity = dynamic_cast<const SimpleWorldDrawableEntity *>( pEntity );
        if ( pDrawableEntity )
        {
            const_cast<SimpleWorldDrawableEntity *>(pDrawableEntity)->GetAnimMgr()->SetCurrentAnimationId( animId );
            const_cast<SimpleWorldDrawableEntity *>(pDrawableEntity)->SetUpdateAnim( update );
            return;
        }

        Errorf( "PlayEntityIntAnimation error: entity %d is not a Drawable or a Fragment Entity", entityId );
    }
    else
    {
        Errorf( "PlayEntityIntAnimation error: no entity at index %d", entityId );
    }
}

static void GotoStartOfAnimation( const char* pEntityName )
{
    const SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( pEntityName );
    if ( pEntity )
    {
        const SimpleWorldDrawableEntity *pDrawableEntity = dynamic_cast<const SimpleWorldDrawableEntity *>( pEntity );
        if ( pDrawableEntity )
        {
            const_cast<SimpleWorldDrawableEntity *>(pDrawableEntity)->GetAnimMgr()->BankCbkAnimCtrlGotoStart();
            return;
        }

        Errorf( "GotoStartOfAnimation error: %s is not a Drawable or a Fragment Entity", pEntityName );
    }
    else
    {
        Errorf( "GotoStartOfAnimation error: no entity named %s", pEntityName );
    }
}

static void StepBackInAnimation( const char* pEntityName )
{
    const SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( pEntityName );
    if ( pEntity )
    {
        const SimpleWorldDrawableEntity *pDrawableEntity = dynamic_cast<const SimpleWorldDrawableEntity *>( pEntity );
        if ( pDrawableEntity )
        {
            const_cast<SimpleWorldDrawableEntity *>(pDrawableEntity)->GetAnimMgr()->BankCbkAnimCtrlStepBack();
            return;
        }

        Errorf( "StepBackInAnimation error: %s is not a Drawable or a Fragment Entity", pEntityName );
    }
    else
    {
        Errorf( "StepBackInAnimation error: no entity named %s", pEntityName );
    }
}

static void PlayAnimationForward( const char* pEntityName )
{
    const SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( pEntityName );
    if ( pEntity )
    {
        const SimpleWorldDrawableEntity *pDrawableEntity = dynamic_cast<const SimpleWorldDrawableEntity *>( pEntity );
        if ( pDrawableEntity )
        {
            const_cast<SimpleWorldDrawableEntity *>(pDrawableEntity)->GetAnimMgr()->BankCbkAnimCtrlPlayForward();
            return;
        }

        Errorf( "PlayAnimationForward error: %s is not a Drawable or a Fragment Entity", pEntityName );
    }
    else
    {
        Errorf( "PlayAnimationForward error: no entity named %s", pEntityName );
    }
}

static void PlayAnimationBackward( const char* pEntityName )
{
    const SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( pEntityName );
    if ( pEntity )
    {
        const SimpleWorldDrawableEntity *pDrawableEntity = dynamic_cast<const SimpleWorldDrawableEntity *>( pEntity );
        if ( pDrawableEntity )
        {
            const_cast<SimpleWorldDrawableEntity *>(pDrawableEntity)->GetAnimMgr()->BankCbkAnimCtrlPlayBackward();
            return;
        }

        Errorf( "PlayAnimationBackward error: %s is not a Drawable or a Fragment Entity", pEntityName );
    }
    else
    {
        Errorf( "PlayAnimationBackward error: no entity named %s", pEntityName );
    }
}

static void PauseAnimation( const char* pEntityName )
{
    const SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( pEntityName );
    if ( pEntity )
    {
        const SimpleWorldDrawableEntity *pDrawableEntity = dynamic_cast<const SimpleWorldDrawableEntity *>( pEntity );
        if ( pDrawableEntity )
        {
            const_cast<SimpleWorldDrawableEntity *>(pDrawableEntity)->GetAnimMgr()->BankCbkAnimCtrlPause();
            return;
        }

        Errorf( "PauseAnimation error: %s is not a Drawable or a Fragment Entity", pEntityName );
    }
    else
    {
        Errorf( "PauseAnimation error: no entity named %s", pEntityName );
    }
}

static void StepForwardInAnimation( const char* pEntityName )
{
    const SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( pEntityName );
    if ( pEntity )
    {
        const SimpleWorldDrawableEntity *pDrawableEntity = dynamic_cast<const SimpleWorldDrawableEntity *>( pEntity );
        if ( pDrawableEntity )
        {
            const_cast<SimpleWorldDrawableEntity *>(pDrawableEntity)->GetAnimMgr()->BankCbkAnimCtrlStepForward();
            return;
        }

        Errorf( "StepForwardInAnimation error: %s is not a Drawable or a Fragment Entity", pEntityName );
    }
    else
    {
        Errorf( "StepForwardInAnimation error: no entity named %s", pEntityName );
    }
}

static void GotoEndOfAnimation( const char* pEntityName )
{
    const SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( pEntityName );
    if ( pEntity )
    {
        const SimpleWorldDrawableEntity *pDrawableEntity = dynamic_cast<const SimpleWorldDrawableEntity *>( pEntity );
        if ( pDrawableEntity )
        {
            const_cast<SimpleWorldDrawableEntity *>(pDrawableEntity)->GetAnimMgr()->BankCbkAnimCtrlGotoEnd();
            return;
        }

        Errorf( "GotoEndOfAnimation error: %s is not a Drawable or a Fragment Entity", pEntityName );
    }
    else
    {
        Errorf( "GotoEndOfAnimation error: no entity named %s", pEntityName );
    }
}

//#############################################################################

// dof parameters: 1: focal plane, 2: near focus limit (focal plane relative), 3: far focus limit (focal plane relative) 4: blur clamp (0..1)
static float GetPPP_DOF_focal_plane()
{
    const Vector4 &dofParams = GRPOSTFX->GetDofParams();
    return dofParams[0];
}

static void SetPPP_DOF_focal_plane( float f )
{
    Vector4 &dofParams = const_cast<Vector4 &>( GRPOSTFX->GetDofParams() );
    dofParams[0] = f;
    GRPOSTFX->SetDofParams( dofParams );
}

static float GetPPP_DOF_near_focus_limit()
{
    const Vector4 &dofParams = GRPOSTFX->GetDofParams();
    return dofParams[1];
}

static void SetPPP_DOF_near_focus_limit( float f )
{
    Vector4 &dofParams = const_cast<Vector4 &>( GRPOSTFX->GetDofParams() );
    dofParams[1] = f;
    GRPOSTFX->SetDofParams( dofParams );
}

static float GetPPP_DOF_far_focus_limit()
{
    const Vector4 &dofParams = GRPOSTFX->GetDofParams();
    return dofParams[2];
}

static void SetPPP_DOF_far_focus_limit( float f )
{
    Vector4 &dofParams = const_cast<Vector4 &>( GRPOSTFX->GetDofParams() );
    dofParams[2] = f;
    GRPOSTFX->SetDofParams( dofParams );
}

static float GetPPP_DOF_blur_clamp()
{
    const Vector4 &dofParams = GRPOSTFX->GetDofParams();
    return dofParams[3];
}

static void SetPPP_DOF_blur_clamp( float f )
{
    Vector4 &dofParams = const_cast<Vector4 &>( GRPOSTFX->GetDofParams() );
    dofParams[3] = f;
    GRPOSTFX->SetDofParams( dofParams );
}

// get/set exposure of hdr
// static float GetHDR_exposure()
// {
//     return GRPOSTFX->GetExposure();
// }
// 
// static void SetHDR_exposure( float f )
// {
//     GRPOSTFX->SetExposure( f );
// }

// get/set bright pass threshold of hdr bright-pass filter
static float GetHDR_bright_pass_threshold()
{
    return GRPOSTFX->GetBrightPassThreshold();
}

static void SetHDR_bright_pass_threshold( float f )
{
    GRPOSTFX->SetBrightPassThreshold( f );
}

// get/set bright pass offset of hdr bright-pass filter
static float GetHDR_bright_pass_offset()
{
    return GRPOSTFX->GetBrightPassOffset();
}

static void SetHDR_bright_pass_offset( float f )
{
    GRPOSTFX->SetBrightPassOffset( f );
}

// get/set middle gray of hdr 
static float GetHDR_middle_gray()
{
    return GRPOSTFX->GetMiddleGray();
}

static void SetHDR_middle_gray( float f )
{
    GRPOSTFX->SetMiddleGray( f );
}

// get/set adaption level of light adaption 
static float GetHDR_light_adaptation_level()
{
    return GRPOSTFX->GetAdapt();
}

static void SetHDR_light_adaptation_level( float f )
{
    GRPOSTFX->SetAdapt( f );
}

// get/set bloom intensity of hdr gauss filter 
static float GetHDR_gauss_filter_bloom_intensity()
{
    return GRPOSTFX->GetBloomIntensity();
}

static void SetHDR_gauss_filter_bloom_intensity( float f )
{
    GRPOSTFX->SetBloomIntensity( f );
}

// color correction in post-processing pipeline
static void GetPPP_color_correction( Vector3 &color, float &a )
{
    const Vector4 &rgba = GRPOSTFX->GetColorCorrect();
    color.Set( rgba.x, rgba.y, rgba.z );
    a = rgba.w;
}

static void SetPPP_color_correction_rgba( float r, float g, float b, float a )
{
    GRPOSTFX->SetColorCorrect( r, g, b, a );
}

static void SetPPP_color_correction( Vector3 &color, float a )
{
    Vector4 rgba( color.x, color.y, color.z, a );
    GRPOSTFX->SetColorCorrect( rgba );
}

// add color in post-processing pipeline
static void GetPPP_color_add( Vector3 &color, float &a )
{
    const Vector4 &rgba = GRPOSTFX->GetConstAdd();
    color.Set( rgba.x, rgba.y, rgba.z );
    a = rgba.w;
}

static void SetPPP_color_add_rgba( float r, float g, float b, float a )
{
    GRPOSTFX->SetConstAdd( r, g, b, a );
}

static void SetPPP_color_add( Vector3 &color, float a )
{
    Vector4 rgba( color.x, color.y, color.z, a );
    GRPOSTFX->SetConstAdd( rgba );
}

// radial blur filter parameters
static void GetPPP_radial_blur_scale( Vector3 &scale )
{
    scale = const_cast<Vector3 &>( GRPOSTFX->GetScale() );
}

static void SetPPP_radial_blur_scale_xyz( float x, float y, float z )
{
    GRPOSTFX->SetScale( x, y, z );
}

static void SetPPP_radial_blur_scale( Vector3 &scale )
{
    GRPOSTFX->SetScale( scale );
}

// get/set lower light adaption limit
static float GetHDR_LowerLimitAdaption()
{
	return GRPOSTFX->GetLowerLimitLightAdaption();
}

static void SetHDR_LowerLimitAdaption( float f )
{
	GRPOSTFX->SetLowerLimitLightAdaption( f );
}

// get/set higher light adaption limit
static float GetHDR_HigherLimitAdaption()
{
	return GRPOSTFX->GetHigherLimitLightAdaption();
}

static void SetHDR_HigherLimitAdaption( float f )
{
	GRPOSTFX->SetHigherLimitLightAdaption( f );
}

// get/set white value in hdr pipeline
static float GetHDR_White()
{
    return GRPOSTFX->GetWhite();
}

static void SetHDR_White( float f )
{
    GRPOSTFX->SetWhite( f );
}

// get/set different techniques for the post-processing pipeline
static int GetPPP_technique()
{
    return static_cast<int>( GRPOSTFX->GetPPTechnique() );
}

static void SetPPP_technique( int effect )
{
    GRPOSTFX->SetPPTechnique( effect );
}

// get/set visual render target debugger
static bool IsPPP_showing_render_targets()
{
    return GRPOSTFX->GetShowRenderTargetFlag();
}

static void SetPPP_show_render_targets( bool b )
{
    GRPOSTFX->SetShowRenderTargetFlag( b );
}

// get/set light adaption
static bool IsHDR_light_adaptation_on()
{
    return GRPOSTFX->GetLightAdaptionFlag();
}

static void SetHDR_light_adaptation( bool b )
{
    GRPOSTFX->SetLightAdaptionFlag( b );
}

// get/set contrast
static float GetPPP_contrast()
{
    return GRPOSTFX->GetContrast();
}

static void SetPPP_contrast( float f )
{
    GRPOSTFX->SetContrast( f );
}

// get/set saturation
static float GetPPP_saturation()
{
    return GRPOSTFX->GetSaturation();
}

static void SetPPP_saturation( float f )
{
    GRPOSTFX->SetSaturation( f );
}

//
// police cam parameters
//

static float GetPPP_GetBrightPassThresholdTwo()
{
	return GRPOSTFX->GetBrightPassThresholdTwo();
}

static void SetPPP_SetBrightPassThresholdTwo(float f )
{
	GRPOSTFX->SetBrightPassThresholdTwo(f);
}

static float GetPPP_GetBrightPassOffsetTwo()
{
	return GRPOSTFX->GetBrightPassOffsetTwo();
}

static void SetPPP_SetBrightPassOffsetTwo(float f )
{
	GRPOSTFX->SetBrightPassOffsetTwo(f);
}

static float GetPPP_GetMiddleGrayTwo()
{
	return GRPOSTFX->GetMiddleGrayTwo();
}

static void SetPPP_SetMiddleGrayTwo(float f )
{
	GRPOSTFX->SetMiddleGrayTwo(f);
}

static float GetPPP_GetBrightPassWhiteTwo()
{
	return GRPOSTFX->GetBrightPassWhiteTwo();
}

static void SetPPP_SetBrightPassWhiteTwo(float f )
{
	GRPOSTFX->SetBrightPassWhiteTwo(f);
}

static float GetPPP_GetContrastTwo()
{
	return GRPOSTFX->GetContrastTwo();
}

static void SetPPP_SetContrastTwo(float f )
{
	GRPOSTFX->SetContrastTwo(f);
}

static float GetPPP_GetSaturationTwo()
{
	return GRPOSTFX->GetSaturationTwo();
}

static void SetPPP_SetSaturationTwo(float f)
{
	GRPOSTFX->SetSaturationTwo(f);
}

//
// film grain parameters
//
/*
// get and set sharpness
static float GetPPPSepia_frame_sharpness()
{
    return GRPOSTFX->GetFrameSharpness();
}

static void SetPPPSepia_frame_sharpness( float f )
{
    GRPOSTFX->SetFrameSharpness( f );
}

// get and set shape
static float GetPPPSepia_frame_shape()
{
    return GRPOSTFX->GetFrameShape();
}

static void SetPPPSepia_frame_shape( float f )
{
    GRPOSTFX->SetFrameShape( f );
}

// get and set frame limit
static float GetPPPSepia_frame_limit()
{
    return GRPOSTFX->GetFrameLimit();
}

static void SetPPPSepia_frame_limit( float f )
{
    GRPOSTFX->SetFrameLimit( f );
}


// get and set time limit
static float GetPPPSepia_time_frame()
{
    return GRPOSTFX->GetTimeFrame();
}

static void SetPPPSepia_time_frame( float f )
{
    GRPOSTFX->SetTimeFrame( f );
}

// get and set interference value
static float GetPPPSepia_interference()
{
    return GRPOSTFX->GetInterference();
}

static void SetPPPSepia_interference( float f )
{
    GRPOSTFX->SetInterference( f );
}
*/
// get and set gamma value
static float GetPPP_gamma()
{
    return GRPOSTFX->GetGamma();
}

static void SetPPP_gamma( float f )
{
    GRPOSTFX->SetGamma( f );
}

//#############################################################################

static bool IsScriptValid( int id )
{
    scrThread *pThread = scrThread::GetThread((scrThreadId)(id));

    if ( pThread == NULL )
    {
        return false;
    }

    return (pThread->GetState() != scrThread::ABORTED);
}

//#############################################################################

void Register_SimpleScript( ragesamples::rmcSampleSimpleWorld *pSampleSimpleWorld )
{
    s_pSampleSimpleWorld = pSampleSimpleWorld;

    // draw text to screen
    SCR_REGISTER_SECURE(DRAW_STRING, 0x60822477, DrawString );
    SCR_REGISTER_SECURE(SET_DRAW_STRING_POS, 0x94ed0a91, SetDrawStringPos );

    // read user input
    SCR_REGISTER_SECURE(IS_PAD_KEYDOWN, 0xa0674a2, IsPadKeyDown );

    // light manipulation
    SCR_REGISTER_SECURE(SET_LIGHT_DIR, 0xb1be3b9e, SetLightDir );
    SCR_REGISTER_SECURE(SET_LIGHT_POS, 0x40a581d2, SetLightPos );
    SCR_REGISTER_SECURE(SET_LIGHT_COLOR, 0x65fe5132, SetLightColor );
    SCR_REGISTER_SECURE(SET_LIGHT_INTENSITY, 0x2cc9a71c, SetLightIntensity );
    SCR_REGISTER_SECURE(SET_LIGHT_FALLOFF, 0x4d7f6e03, SetLightFalloff );
    SCR_REGISTER_SECURE(SET_LIGHT_TYPE, 0xcb58679d, SetLightType );
    SCR_REGISTER_SECURE(SET_LIGHT_OVERSATURATE, 0x1f95d97f, SetLightOversaturate );

    // camera
    SCR_REGISTER_SECURE(INIT_CAMERA, 0xdb68dc31, InitCamera );
    SCR_REGISTER_SECURE(SET_CAMERA_TYPE, 0xfd6cf6db, SetCameraType );
    SCR_REGISTER_SECURE(SET_FLYCAM_SPEED, 0x4186f2ed, SetFlyCamSpeed );
    SCR_REGISTER_SECURE(SET_FREECAM_LOCATION, 0xe96db319, SetFreeCamLocation );
    SCR_REGISTER_SECURE(SET_FREECAM_OFFSET_LOCATION, 0xc6811d61, SetFreeCamOffsetLocation );
    SCR_REGISTER_SECURE(ROTATE_FREECAM, 0x2de9085c, RotateFreeCam );
    SCR_REGISTER_SECURE(SET_FREECAM_SPEEDS, 0x4a7a4116, SetFreeCamSpeeds );
    SCR_REGISTER_SECURE(SET_LHCAM_FOLLOW, 0x89baafd6, SetLHCamFollow );
    SCR_REGISTER_SECURE(STOP_LHCAM_FOLLOW, 0x2c8f3921, StopLHCamFollow );
    SCR_REGISTER_SECURE(SET_LHCAM_OFFSET, 0x62fb243f, SetLHCamOffset );
    SCR_REGISTER_SECURE(SET_LHCAM_FOLLOW_OFFSET, 0x93467f4d, SetLHCamFollowOffset );
    SCR_REGISTER_SECURE(SET_LHCAM_ORIENT, 0x7410504f, SetLHCamOrient );
    SCR_REGISTER_SECURE(SET_LHCAM_LINEAR_RATE, 0xcac347e1, SetLHCamLinearRate );
    SCR_REGISTER_SECURE(SET_LHCAM_ANGULATE_RATE, 0x9adaae80, SetLHCamAngularRate );
    SCR_REGISTER_SECURE(SET_LHCAM_ORIENT_RATE, 0xb6f2f26e, SetLHCamOrientRate );
    SCR_REGISTER_SECURE(SET_LHCAM_DISTANCE, 0xca69735a, SetLHCamDistance );
    SCR_REGISTER_SECURE(SET_LHCAM_AZIMUTH, 0x8392d2b2, SetLHCamAzimuth );
    SCR_REGISTER_SECURE(SET_LHCAM_INCLINE, 0xaf5be3bd, SetLHCamIncline );
    SCR_REGISTER_SECURE(GET_LHCAM_DISTANCE, 0x96d12103, GetLHCamDistance );
    SCR_REGISTER_SECURE(GET_LHCAM_AZIMUTH, 0xefbd53e7, GetLHCamAzimuth );
    SCR_REGISTER_SECURE(GET_LHCAM_INCLINE, 0xd07fa5d9, GetLHCamIncline );
    SCR_REGISTER_SECURE(GET_LHCAM_OFFSET, 0xe2be94bb, GetLHCamOffset );
    SCR_REGISTER_SECURE(GET_MAYACAM_FOCUS_DISTANCE, 0x88b6f8be, GetMayaCamFocusDistance );
    SCR_REGISTER_SECURE(SET_MAYACAM_DEFAULT_SPEED, 0x2ea49ff3, SetMayaCamDefaultSpeed );
    SCR_REGISTER_SECURE(SET_MAYACAM_ROTATE_SPEED, 0xdd6202f3, SetMayaCamRotateSpeed );
    SCR_REGISTER_SECURE(SET_MAYACAM_FOCUS_POINT, 0x532bf7df, SetMayaCamFocusPoint );
    SCR_REGISTER_SECURE(SET_POLARCAM_FOLLOW, 0x7ac125ae, SetPolarCamFollow );
    SCR_REGISTER_SECURE(STOP_POLARCAM_FOLLOW, 0xa4d44cff, StopPolarCamFollow );
    SCR_REGISTER_SECURE(SET_POLARCAM_OFFSET, 0xcca84d00, SetPolarCamOffset );
    SCR_REGISTER_SECURE(SET_POLARCAM_FOLLOW_OFFSET, 0x81f643e1, SetPolarCamFollowOffset );
    SCR_REGISTER_SECURE(SET_POLARCAM_ORIENT, 0xd6215246, SetPolarCamOrient );
    SCR_REGISTER_SECURE(SET_POLARCAM_LINEAR_RATE, 0xc7a133f0, SetPolarCamLinearRate );
    SCR_REGISTER_SECURE(SET_POLARCAM_ANGULATE_RATE, 0x29ec7893, SetPolarCamAngularRate );
    SCR_REGISTER_SECURE(SET_POLARCAM_ORIENT_RATE, 0x21ee03e3, SetPolarCamOrientRate );
    SCR_REGISTER_SECURE(SET_POLARCAM_DISTANCE, 0xe881c406, SetPolarCamDistance );
    SCR_REGISTER_SECURE(SET_POLARCAM_AZIMUTH, 0x6f2050b1, SetPolarCamAzimuth );
    SCR_REGISTER_SECURE(SET_POLARCAM_INCLINE, 0x77a5312b, SetPolarCamIncline );
    SCR_REGISTER_SECURE(GET_POLARCAM_DISTANCE, 0xac573242, GetPolarCamDistance );
    SCR_REGISTER_SECURE(GET_POLARCAM_AZIMUTH, 0x16f33525, GetPolarCamAzimuth );
    SCR_REGISTER_SECURE(GET_POLARCAM_INCLINE, 0x58d35b3f, GetPolarCamIncline );
    SCR_REGISTER_SECURE(GET_POLARCAM_OFFSET, 0x8ca07694, GetPolarCamOffset );
    SCR_REGISTER_SECURE(SET_TRACKCAM_AXIS_ORIGIN, 0x9d4b7b87, SetTrackCamAxisOrigin );
    SCR_REGISTER_SECURE(SET_TRACKCAM_TOP_VIEW, 0x95beee20, SetTrackCamTopView );
    SCR_REGISTER_SECURE(SET_TRACKCAM_REVERSE, 0x32774547, SetTrackCamReverse );
    SCR_REGISTER_SECURE(SET_TRACKCAM_WHEEL_SCALE, 0x7fdfea7b, SetTrackCamWheelScale );
    SCR_REGISTER_SECURE(SET_TRACKCAM_MOUSE_WINDOW_OVERRIDE, 0xd2f6bccd, SetTrackCamMouseWindowOverride );

    // entity manipulation
    SCR_REGISTER_SECURE(SET_ENTITY_STRING_POS, 0x5656eaf2, SetEntityStringPos );
    SCR_REGISTER_SECURE(SET_ENTITY_INT_POS, 0xbc2c5354, SetEntityIntPos );
    SCR_REGISTER_SECURE(GET_ENTITY_STRING_POS, 0x5a3037fb, GetEntityStringPos );
    SCR_REGISTER_SECURE(GET_ENTITY_INT_POS, 0x87668773, GetEntityIntPos );
    SCR_REGISTER_SECURE(LOAD_ENTITY, 0xbd646e4d, LoadEntity );
    SCR_REGISTER_SECURE(LOAD_FRAG_ENTITY, 0x7376ebd2, LoadFragEntity );
    SCR_REGISTER_SECURE(LOAD_FRAG_DRAWABLE_ENTITY, 0xa370c55d, LoadFragDrawableEntity );
    SCR_REGISTER_SECURE(REMOVE_ENTITY_STRING, 0xfc6a6b3d, RemoveEntityString );
    SCR_REGISTER_SECURE(REMOVE_ENTITY_INT, 0x69ba468a, RemoveEntityInt );

    // animation
    SCR_REGISTER_SECURE(LOAD_ANIMATION, 0xe726c8a1, LoadAnimation );
    SCR_REGISTER_SECURE(PLAY_ENTITY_STRING_ANIMATION, 0x6ae0681d, PlayEntityStringAnimation );
    SCR_REGISTER_SECURE(PLAY_ENTITY_INT_ANIMATION, 0xe9226f07, PlayEntityIntAnimation );
    SCR_REGISTER_SECURE(GOTO_START_OF_ANIMATION, 0x48e9d15a, GotoStartOfAnimation );
    SCR_REGISTER_SECURE(STEP_BACK_IN_ANIMATION, 0xbb206827, StepBackInAnimation );
    SCR_REGISTER_SECURE(PLAY_ANIMATION_FORWARD, 0x9dc55503, PlayAnimationForward );
    SCR_REGISTER_SECURE(PLAY_ANIMATION_BACKWARD, 0x9da7295f, PlayAnimationBackward );
    SCR_REGISTER_SECURE(PAUSE_ANIMATION, 0xa90b600e, PauseAnimation );
    SCR_REGISTER_SECURE(STEP_FORWARD_IN_ANIMATION, 0xb4745742, StepForwardInAnimation );
    SCR_REGISTER_SECURE(GOTO_END_OF_ANIMATION, 0xfaea3395, GotoEndOfAnimation );

    // postfx

    // DOF parameters: 1: Focal Plane, 2: near focus limit (Focal Plane relative), 3: far Focus limit (Focal Plane relative) 4: blur clamp (0..1)
    SCR_REGISTER_SECURE(GET_PPP_DOF_FOCAL_PLANE, 0xa9490406, GetPPP_DOF_focal_plane );
    SCR_REGISTER_SECURE(SET_PPP_DOF_FOCAL_PLANE, 0x4e0eb4da, SetPPP_DOF_focal_plane );

    SCR_REGISTER_SECURE(GET_PPP_DOF_NEAR_FOCUS_LIMIT, 0x7bd8cbde, GetPPP_DOF_near_focus_limit );
    SCR_REGISTER_SECURE(SET_PPP_DOF_NEAR_FOCUS_LIMIT, 0xb0d65680, SetPPP_DOF_near_focus_limit );

    SCR_REGISTER_SECURE(GET_PPP_DOF_FAR_FOCUS_LIMIT, 0xd7a14853, GetPPP_DOF_far_focus_limit );
    SCR_REGISTER_SECURE(SET_PPP_DOF_FAR_FOCUS_LIMIT, 0xfe4b57de, SetPPP_DOF_far_focus_limit );

    SCR_REGISTER_SECURE(GET_PPP_DOF_BLUR_CLAMP, 0x1047134, GetPPP_DOF_blur_clamp );
    SCR_REGISTER_SECURE(SET_PPP_DOF_BLUR_CLAMP, 0xee99390, SetPPP_DOF_blur_clamp );

//     // get/set exposure of HDR
//     SCR_REGISTER( GET_PPP_HDR_EXPOSURE, GetHDR_exposure );
//     SCR_REGISTER( SET_PPP_HDR_EXPOSURE, SetHDR_exposure );

    // get/set bright pass threshold of HDR bright-pass filter
    SCR_REGISTER_SECURE(GET_PPP_HDR_BRIGHT_PASS_THRESHOLD, 0x15a4e4c6, GetHDR_bright_pass_threshold );
    SCR_REGISTER_SECURE(SET_PPP_HDR_BRIGHT_PASS_THRESHOLD, 0x77b726f4, SetHDR_bright_pass_threshold );

    // get/set bright pass offset of HDR bright-pass filter
    SCR_REGISTER_SECURE(GET_PPP_HDR_BRIGHT_PASS_OFFSET, 0x9d873a1b, GetHDR_bright_pass_offset );
    SCR_REGISTER_SECURE(SET_PPP_HDR_BRIGHT_PASS_OFFSET, 0xecd56e41, SetHDR_bright_pass_offset );

    // get/set middle gray of HDR 
    SCR_REGISTER_SECURE(GET_PPP_HDR_MIDDLE_GRAY, 0x9df68f9, GetHDR_middle_gray );
    SCR_REGISTER_SECURE(SET_PPP_HDR_MIDDLE_GRAY, 0x6f8092b5, SetHDR_middle_gray );

    // get/set adaption level of light adaption 
    SCR_REGISTER_SECURE(GET_PPP_HDR_LIGHT_ADAPTATION_LEVEL, 0xb56f074b, GetHDR_light_adaptation_level );
    SCR_REGISTER_SECURE(SET_PPP_HDR_LIGHT_ADAPTATION_LEVEL, 0x2d7efae9, SetHDR_light_adaptation_level );

    // get/set bloom intensity of HDR Gauss filter 
    SCR_REGISTER_SECURE(GET_PPP_HDR_GAUSS_FILTER_BLOOM_INTENSITY, 0x7414c809, GetHDR_gauss_filter_bloom_intensity );
    SCR_REGISTER_SECURE(SET_PPP_HDR_GAUSS_FILTER_BLOOM_INTENSITY, 0xf97afa13, SetHDR_gauss_filter_bloom_intensity );

    // color correction in post-processing pipeline
    SCR_REGISTER_SECURE(GET_PPP_COLOR_CORRECTION, 0x40b1739a, GetPPP_color_correction );
    SCR_REGISTER_SECURE(SET_PPP_COLOR_CORRECTION_RGBA, 0xe9811dbe, SetPPP_color_correction_rgba );
    SCR_REGISTER_SECURE(SET_PPP_COLOR_CORRECTION, 0x55936842, SetPPP_color_correction );

    // add color in post-processing pipeline
    SCR_REGISTER_SECURE(GET_PPP_COLOR_ADD, 0x41a52dde, GetPPP_color_add );
    SCR_REGISTER_SECURE(SET_PPP_COLOR_ADD_RGBA, 0x43ddc4ec, SetPPP_color_add_rgba );
    SCR_REGISTER_SECURE(SET_PPP_COLOR_ADD, 0x65bf53d2, SetPPP_color_add );

    // radial blur filter parameters
    SCR_REGISTER_SECURE(GET_PPP_RADIAL_BLUR_SCALE, 0x5cffbcd5, GetPPP_radial_blur_scale );
    SCR_REGISTER_SECURE(SET_PPP_RADIAL_BLUR_SCALE_XYZ, 0x6130cc00, SetPPP_radial_blur_scale_xyz );
    SCR_REGISTER_SECURE(SET_PPP_RADIAL_BLUR_SCALE, 0xeb4b3825, SetPPP_radial_blur_scale );

    // get/set white values in HDR pipeline
	SCR_REGISTER_SECURE(GET_PPP_HDR_WHITE, 0xff500938, GetHDR_White );
	SCR_REGISTER_SECURE(SET_PPP_HDR_WHITE, 0xe0c7bdb6, SetHDR_White );

	// get/set lower light adaption limit
	SCR_REGISTER_SECURE(GET_PPP_HDR_LOWERLIMITADAPTION, 0x444451d2, GetHDR_LowerLimitAdaption );
	SCR_REGISTER_SECURE(SET_PPP_HDR_LOWERLIMITADAPTION, 0x9ec0d334, SetHDR_LowerLimitAdaption );

	// get/set higher light adaption limit
	SCR_REGISTER_SECURE(GET_PPP_HDR_HIGHERLIMITADAPTION, 0x346c99aa, GetHDR_HigherLimitAdaption );
	SCR_REGISTER_SECURE(SET_PPP_HDR_HIGHERLIMITADAPTION, 0x5297dcbc, SetHDR_HigherLimitAdaption );

	// get/set different techniques for the post-processing pipeline
    SCR_REGISTER_SECURE(GET_PPP_TECHNIQUE, 0xf0a42f1f, GetPPP_technique );
    SCR_REGISTER_SECURE(SET_PPP_TECHNIQUE, 0x5007fc9e, SetPPP_technique );

    // get/set visual render target debugger
    SCR_REGISTER_SECURE(IS_PPP_SHOWING_RENDER_TARGETS, 0x73d0f83c, IsPPP_showing_render_targets );
    SCR_REGISTER_SECURE(SET_PPP_SHOW_RENDER_TARGETS, 0xb5ebb46, SetPPP_show_render_targets );

    // get/set light adaption
    SCR_REGISTER_SECURE(IS_PPP_HDR_LIGHT_ADAPTATION_ON, 0x6866c434, IsHDR_light_adaptation_on );
    SCR_REGISTER_SECURE(SET_PPP_HDR_LIGHT_ADAPTATION, 0x23e1e3d8, SetHDR_light_adaptation );

    // get/set contrast
    SCR_REGISTER_SECURE(GET_PPP_CONTRAST, 0xedd6a15e, GetPPP_contrast );
    SCR_REGISTER_SECURE(SET_PPP_CONTRAST, 0xb22d2dc6, SetPPP_contrast );

    // get/set saturation
    SCR_REGISTER_SECURE(GET_PPP_SATURATION, 0xe5d5613c, GetPPP_saturation );
    SCR_REGISTER_SECURE(SET_PPP_SATURATION, 0x4e35aa13, SetPPP_saturation );

    // get and set gamma value
    SCR_REGISTER_SECURE(GET_PPP_GAMMA, 0x212aab2c, GetPPP_gamma );
    SCR_REGISTER_SECURE(SET_PPP_GAMMA, 0x80af90a7, SetPPP_gamma );

    //
    // film grain parameters
    //
/*
    // get and set sharpness
    SCR_REGISTER_SECURE(GET_PPP_SEPIA_FRAME_SHARPNESS, 0xc27a51e7, GetPPPSepia_frame_sharpness );
    SCR_REGISTER_SECURE(SET_PPP_SEPIA_FRAME_SHARPNESS, 0xdb253c24, SetPPPSepia_frame_sharpness );

    // get and set shape
    SCR_REGISTER_SECURE(GET_PPP_SEPIA_FRAME_SHAPE, 0x3f072321, GetPPPSepia_frame_shape );
    SCR_REGISTER_SECURE(SET_PPP_SEPIA_FRAME_SHAPE, 0x6f4e33cb, SetPPPSepia_frame_shape );

    // get and set frame limit
    SCR_REGISTER_SECURE(GET_PPP_SEPIA_FRAME_LIMIT, 0x805e2ad9, GetPPPSepia_frame_limit );
    SCR_REGISTER_SECURE(SET_PPP_SEPIA_FRAME_LIMIT, 0x4deb7c3d, SetPPPSepia_frame_limit );

    // get and set time limit
    SCR_REGISTER_SECURE(GET_PPP_SEPIA_TIME_FRAME, 0x8f34f19f, GetPPPSepia_time_frame );
    SCR_REGISTER_SECURE(SET_PPP_SEPIA_TIME_FRAME, 0xcade5ed6, SetPPPSepia_time_frame );

    // get and set interference value
    SCR_REGISTER_SECURE(GET_PPP_SEPIA_INTERFERENCE, 0x11d21472, GetPPPSepia_interference );
    SCR_REGISTER_SECURE(SET_PPP_SEPIA_INTERFERENCE, 0xba5bfeb0, SetPPPSepia_interference );
*/
	SCR_REGISTER_SECURE(GET_PPP_BRIGHTPASSTHRESHOLDTWO, 0x1f9cf8f7, GetPPP_GetBrightPassThresholdTwo );
	SCR_REGISTER_SECURE(SET_PPP_BRIGHTPASSTHRESHOLDTWO, 0x46b3d09f, SetPPP_SetBrightPassThresholdTwo );

	SCR_REGISTER_SECURE(GET_PPP_BRIGHTPASSOFFSETTWO, 0x89bff3d4, GetPPP_GetBrightPassOffsetTwo );
	SCR_REGISTER_SECURE(SET_PPP_BRIGHTPASSOFFSETTWO, 0x9e84decb, SetPPP_SetBrightPassOffsetTwo );

	SCR_REGISTER_SECURE(GET_PPP_MIDDLEGRAYTWO, 0x66bb6a86, GetPPP_GetMiddleGrayTwo );
	SCR_REGISTER_SECURE(SET_PPP_MIDDLEGRAYTWO, 0x8df39323, SetPPP_SetMiddleGrayTwo );

	SCR_REGISTER_SECURE(GET_PPP_BRIGHTPASSWHITETWO, 0xa6787ffc, GetPPP_GetBrightPassWhiteTwo );
	SCR_REGISTER_SECURE(SET_PPP_BRIGHTPASSWHITETWO, 0x7abc07d1, SetPPP_SetBrightPassWhiteTwo );

	SCR_REGISTER_SECURE(GET_PPP_CONTRASTTWO, 0x43f34115, GetPPP_GetContrastTwo );
	SCR_REGISTER_SECURE(SET_PPP_CONTRASTTWO, 0xd12c7791, SetPPP_SetContrastTwo );

	SCR_REGISTER_SECURE(GET_PPP_SATURATIONTWO, 0x4858f86, GetPPP_GetSaturationTwo );
	SCR_REGISTER_SECURE(SET_PPP_SATURATIONTWO, 0x99c15636, SetPPP_SetSaturationTwo );

	// misc
    SCR_REGISTER_SECURE(IS_SCRIPT_VALID, 0x45f7d589, IsScriptValid );
}

//#############################################################################

} // namespace rageSimpleScript
