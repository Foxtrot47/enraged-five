// 
// sample_simpleworld/sample_hdr.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// TITLE: 
//      HDR
// PURPOSE:
//		This sample shows how to use HDR cubemaps and allows manipulation of the various post processing effects.
// NOTES:
//      If no "filist" is provided on the commandline, we default is t:\rage\assets\sample_hdr\sample_hdr.filist
//      Use the "HDR Cube Map" and "PostFx" bank of widgets to manipulate the scene.


#include "sample_hdr.h"
#include "sample_simpleworld.h"

#include "atl/array.h"
#include "bank/bkmgr.h"
#include "sample_file/filelistloader.h"
#include "system/main.h"
#include "system/param.h"

using namespace ragesamples;

XPARAM( filist );

PARAM( cubemaplist, "[sample_hdr] Load a list of Cube Maps to cycle through." );
PARAM( hdrobjlist, "[sample_hdr] Load a list of HDR-enabled objects to cycle through." );

class rmcSampleHdrWorld : public ragesamples::rmcSampleSimpleWorld
{
protected:

    struct EntityStruct
    {
        Matrix34 xForm;
        char fileName[256];
    };

    atArray<EntityStruct> m_cubemapFiles;
    atArray<EntityStruct> m_hdrObjectFiles;

    int m_currentCubemap;
    int m_currentHdrObject;

    SimpleWorldDrawableEntity *m_pCubeMapEntity;
    SimpleWorldDrawableEntity *m_pHdrObjectEntity;

public:
    rmcSampleHdrWorld()
    {       
        m_pCubeMapEntity = m_pHdrObjectEntity = NULL;
    }
    
    virtual ~rmcSampleHdrWorld()
    {
        m_cubemapFiles.clear();
        m_hdrObjectFiles.clear();
    }

protected:

    virtual const char* GetSampleName() const
    {
        return "sample_hdr";
    }

    virtual void InitFileListLoader()
    {
        ragesamples::rmcSampleSimpleWorld::InitFileListLoader();

        FILELISTLOADER->RegisterFile( "cubeMapList", this, MakeFunctorRet(LoadCubemapListFile), FILELISTLOADER->NullSaveFileFunctor );
        FILELISTLOADER->RegisterFile( "hdrObjList", this, MakeFunctorRet(LoadHdrObjectListFile), FILELISTLOADER->NullSaveFileFunctor );
    }

    virtual void InitClient()
    {
        rmcSampleSimpleWorld::InitClient();

        const char *pCubeMapList;
        if ( PARAM_cubemaplist.Get(pCubeMapList) )
        {
            LoadCubemapListFile( this, pCubeMapList );
        }

        const char *pHdrObjList;
        if ( PARAM_hdrobjlist.Get(pHdrObjList) )
        {
            LoadHdrObjectListFile( this, pHdrObjList );
        }

        // try to load the first one from each list
        m_currentCubemap = m_currentHdrObject = 0;

        if ( m_cubemapFiles.GetCount() > 0 )
        {
            LoadEntity( m_cubemapFiles[m_currentCubemap].fileName, m_cubemapFiles[m_currentCubemap].xForm );
            m_pCubeMapEntity = m_pLastEntityLoaded;
        }

        if ( m_hdrObjectFiles.GetCount() > 0 )
        {
            LoadEntity( m_hdrObjectFiles[m_currentHdrObject].fileName, m_hdrObjectFiles[m_currentHdrObject].xForm );
            m_pHdrObjectEntity = m_pLastEntityLoaded;
        }
    }

    static bool LoadCubemapListFile( void *pObj, const char *pFile )
    {
        Assert( pFile );
        fiStream *pStream = ASSET.Open( pFile, ENTITY_LIST );
        if( !pStream )
        {
            Errorf( "Failed to open Cubemap Entity List file : '%s'\n", pFile );
            return false;
        }

        rmcSampleHdrWorld *pHdrWorld = (rmcSampleHdrWorld *)pObj;
    
        if ( pHdrWorld->m_cubemapFiles.GetCount() > 0 )
        {
            pHdrWorld->m_cubemapFiles.clear();
        }

        Printf( "Loading Cubemap Entity List : '%s'\n", pFile );

        //Read in the entity path/transform pairs from the file
        EntityStruct entity;
        while ( fgetline(entity.fileName,256,pStream) )
        {
            float a0, a1, a2;
            float b0, b1, b2;
            float c0, c1, c2;
            float d0, d1, d2;

            char strMatrix[256];
            if ( !fgetline(strMatrix,256,pStream) )
            {
                Errorf( "Error, invalid Cubemap Entity List File.  Missing transform for entity '%s'\n", entity.fileName );
                pStream->Close();
                return false;
            }

            sscanf(strMatrix, "%f %f %f %f %f %f %f %f %f %f %f %f",
                &a0, &a1, &a2,
                &b0, &b1, &b2,
                &c0, &c1, &c2,
                &d0, &d1, &d2);
            entity.xForm.Set(a0,a1,a2,b0,b1,b2,c0,c1,c2,d0,d1,d2);

            pHdrWorld->m_cubemapFiles.PushAndGrow( entity );

            Printf( "Found Cube Map entity '%s'\n", entity.fileName );
            Printf( "\tPositioned at [%f,%f,%f]\n", d0,d1,d2 );
        }

        pStream->Close();
        return true;
    }

    static bool LoadHdrObjectListFile( void *pObj, const char *pFile )
    {
        Assert( pFile );
        fiStream *pStream = ASSET.Open( pFile, ENTITY_LIST );
        if( !pStream )
        {
            Errorf( "Failed to open Hdr Object Entity List file : '%s'\n", pFile );
            return false;
        }

        rmcSampleHdrWorld *pHdrWorld = (rmcSampleHdrWorld *)pObj;

        if ( pHdrWorld->m_hdrObjectFiles.GetCount() > 0 )
        {
            pHdrWorld->m_hdrObjectFiles.clear();
        }

        Printf( "Loading Hdr Object Entity List : '%s'\n", pFile );

        //Read in the entity path/transform pairs from the file
        EntityStruct entity;
        while ( fgetline(entity.fileName,256,pStream) )
        {
            float a0, a1, a2;
            float b0, b1, b2;
            float c0, c1, c2;
            float d0, d1, d2;

            char strMatrix[256];
            if ( !fgetline(strMatrix,256,pStream) )
            {
                Errorf( "Error, invalid Hdr Object Entity List File.  Missing transform for entity '%s'\n", entity.fileName );
                pStream->Close();
                return false;
            }

            sscanf(strMatrix, "%f %f %f %f %f %f %f %f %f %f %f %f",
                &a0, &a1, &a2,
                &b0, &b1, &b2,
                &c0, &c1, &c2,
                &d0, &d1, &d2);
            entity.xForm.Set(a0,a1,a2,b0,b1,b2,c0,c1,c2,d0,d1,d2);

            pHdrWorld->m_hdrObjectFiles.PushAndGrow( entity );

            Printf( "Found Hdr Object entity '%s'\n", entity.fileName );
            Printf( "\tPositioned at [%f,%f,%f]\n", d0,d1,d2 );
        }

        pStream->Close();
        return true;
    }

#if __BANK
    void BankCycleNextCubemap()
    {
        if ( m_cubemapFiles.GetCount() <= 1 )
        {
            return;
        }
        
        // unload current cubemap
        if ( m_pCubeMapEntity )
        {
            SMPWORLDEMGR.RemoveEntity( m_pCubeMapEntity );
        }

        // select next cubemap
        ++m_currentCubemap;
        if ( m_currentCubemap >= m_cubemapFiles.GetCount() )
        {
            m_currentCubemap = 0;
        }

        // load next cubemap
        LoadEntity( m_cubemapFiles[m_currentCubemap].fileName, m_cubemapFiles[m_currentCubemap].xForm );
        m_pCubeMapEntity = m_pLastEntityLoaded;
    }

    void BankCyclePrevCubemap()
    {
        if ( m_cubemapFiles.GetCount() <= 1 )
        {
            return;
        }

        // unload current cubemap
        if ( m_pCubeMapEntity )
        {
            SMPWORLDEMGR.RemoveEntity( m_pCubeMapEntity );
        }

        // select next cubemap
        --m_currentCubemap;
        if ( m_currentCubemap < 0 )
        {
            m_currentCubemap = m_cubemapFiles.GetCount() - 1;
        }

        // load next cubemap
        LoadEntity( m_cubemapFiles[m_currentCubemap].fileName, m_cubemapFiles[m_currentCubemap].xForm );
        m_pCubeMapEntity = m_pLastEntityLoaded;
    }
    
    void BankCycleNextHdrObj()
    {
        if ( m_hdrObjectFiles.GetCount() <= 1 )
        {
            return;
        }

        // unload current cubemap
        if ( m_pHdrObjectEntity )
        {
            SMPWORLDEMGR.RemoveEntity( m_pHdrObjectEntity );
        }

        // select next hdr object
        ++m_currentHdrObject;
        if ( m_currentHdrObject >= m_hdrObjectFiles.GetCount() )
        {
            m_currentHdrObject = 0;
        }

        // load next hdr object
        LoadEntity( m_hdrObjectFiles[m_currentHdrObject].fileName, m_hdrObjectFiles[m_currentHdrObject].xForm );
        m_pHdrObjectEntity = m_pLastEntityLoaded;
    }

    void BankCyclePrevHdrObj()
    {
        if ( m_hdrObjectFiles.GetCount() <= 1 )
        {
            return;
        }

        // unload current hdr object
        if ( m_pHdrObjectEntity )
        {
            SMPWORLDEMGR.RemoveEntity( m_pHdrObjectEntity );
        }

        // select next hdr object
        --m_currentHdrObject;
        if ( m_currentHdrObject < 0 )
        {
            m_currentHdrObject = m_hdrObjectFiles.GetCount() - 1;
        }

        // load next hdr object
        LoadEntity( m_hdrObjectFiles[m_currentHdrObject].fileName, m_hdrObjectFiles[m_currentHdrObject].xForm );
        m_pHdrObjectEntity = m_pLastEntityLoaded;
    }
    
    void BankLoadCubemapEntityList()
    {
        char fullEntityPath[256];
        memset(fullEntityPath, 0, 256);

        if ( BANKMGR.OpenFile(fullEntityPath, 256, "*."ENTITY_LIST, false, "Cubemap Entity List Files (*."ENTITY_LIST")") )
        {
            // unload current cubemap
            if ( m_pHdrObjectEntity )
            {
                SMPWORLDEMGR.RemoveEntity( m_pHdrObjectEntity );
            }

            // load the list
            LoadCubemapListFile( this, fullEntityPath );

            // load the first one in the list
            m_currentCubemap = 0;
            if ( m_cubemapFiles.GetCount() > 0 )
            {
                LoadEntity( m_cubemapFiles[m_currentCubemap].fileName, m_cubemapFiles[m_currentCubemap].xForm );
                m_pCubeMapEntity = m_pLastEntityLoaded;
            }
        }
    }
    
    void BankLoadHdrObjectEntityList()
    {
        char fullEntityPath[256];
        memset(fullEntityPath, 0, 256);

        if ( BANKMGR.OpenFile(fullEntityPath, 256, "*."ENTITY_LIST, false, "HDR Object Entity List Files (*."ENTITY_LIST")") )
        {
            // unload current cubemap
            if ( m_pHdrObjectEntity )
            {
                SMPWORLDEMGR.RemoveEntity( m_pHdrObjectEntity );
            }

            // load the list
            LoadHdrObjectListFile( this, fullEntityPath );

            // load the first on in the list
            m_currentHdrObject = 0;
            if ( m_hdrObjectFiles.GetCount() > 0 )
            {
                LoadEntity( m_hdrObjectFiles[m_currentHdrObject].fileName, m_hdrObjectFiles[m_currentHdrObject].xForm );
                m_pHdrObjectEntity = m_pLastEntityLoaded;
            }
        }
    }

    //grcSampleManager 
    virtual void AddWidgetsClient()
    {
        rmcSampleSimpleWorld::AddWidgetsClient();

        bkBank *pBank = &BANKMGR.CreateBank( "rage - HDR Cube Map" );
        {
            pBank->AddButton( "Cycle next Cube Map", datCallback(MFA(rmcSampleHdrWorld::BankCycleNextCubemap),this), 
                "Load the next Cube Map in the list." );
            pBank->AddButton( "Cycle previous Cube Map", datCallback(MFA(rmcSampleHdrWorld::BankCyclePrevCubemap),this), 
                "Load the previous Cube Map in the list." );
            pBank->AddButton( "Cycle next HDR Object", datCallback(MFA(rmcSampleHdrWorld::BankCycleNextHdrObj),this), 
                "Load the next HDR Object in the list." );
            pBank->AddButton( "Cycle previous HDR Object", datCallback(MFA(rmcSampleHdrWorld::BankCyclePrevHdrObj),this), 
                "Load the previous HDR Object in the list." );
            pBank->AddButton( "Load Cube Map Entity List", datCallback(MFA(rmcSampleHdrWorld::BankLoadCubemapEntityList),this), 
                "Load a ."ENTITY_LIST" of Cube Map entities.");
            pBank->AddButton( "Load HDR Object Entity List", datCallback(MFA(rmcSampleHdrWorld::BankLoadHdrObjectEntityList),this), 
                "Load a ."ENTITY_LIST" of HDR-enabled entities." );
        }
    }
#endif //__BANK
};


int Main()
{
    rmcSampleHdrWorld sampleRmc;
    
    if ( !PARAM_filist.Get() )
    {
        PARAM_filist.Set( "t:/rage/assets/sample_hdr/sample_hdr.filist" );
    }

    sampleRmc.Init( "sample_rmcore\\drawable" );

    sampleRmc.UpdateLoop();

    sampleRmc.Shutdown();

    return 0;
}
