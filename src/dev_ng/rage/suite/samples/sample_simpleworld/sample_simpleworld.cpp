// 
// sample_simpleworld/sample_simpleworld.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "sample_simpleworld.h"
#include "simplescript.h"

#include "diag/xmllog.h"
#include "diag/output.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/console.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crclip/clip.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "file/stream.h"
#include "file/token.h"
#include "fragment/cache.h"
#include "fragment/cachemanager.h"
#include "fragment/drawable.h"
#include "fragment/manager.h"
#include "fragment/fragshaft.h"
#include "fragment/tune.h"
#include "fragment/typechild.h"
#include "gizmo/manager.h"
#include "grcore/image.h"
#include "grpostfx/postfx.h"
#include "paging/streamer.h"
#include "parsercore/attribute.h"
#include "pharticulated/articulatedcollider.h"
#include "pheffects/wind.h"
#include "phsolver/contactmgr.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "grprofile/drawmanager.h"
#include "grprofile/ekg.h"
#include "profile/profiler.h"
#include "grprofile/stats.h"
#include "rmcore/drawable.h"
#include "rmptfx/ptxmanager.h"
#include "sample_file/filelistloader.h"
#include "sample_fragment/fragworld.h"
#include "sample_physics/sample_physics.h"
#include "scriptgui/debugger.h"
#include "script/bankscript.h"
#include "script/program.h"
#include "script/thread.h"
#include "system/param.h"
#include "grprofile/pix.h"
#include "system/timemgr.h"
//~R
#include "grmodel/shaderfx.h"
#include "grmodel/shaderfx.h"
#include "grcore/texture.h"
#include "grmodel/geometry.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"
//rk
#include "vieweraudio\AudioMgr.h"

#if __WIN32PC
#include "embedded_rageViewer_debug_win32_30.h"
#endif
#if __XENON
#include "embedded_rageViewer_debug_fxl_final.h"
#endif
#if __PS3
#include "embedded_rageViewer_debug_psn.h"
#endif

PARAM(filist,"[sample_simpleworld] The filename of the .fiList file (list of entities, fragments, animations, lights, camera, postfx, etc.) to load");
PARAM(fixml,"[sample_simpleworld] The filename of the .xml file containing the assets that need to be loaded and other initial settings");
PARAM(file,"[sample_simpleworld] The filename of the .type file to load");
PARAM(entity,"[sample_simpleworld] The filename of the .type file to load");
PARAM(fragfile,"[sample_simpleworld] The filname of the fragment .type file to load");
PARAM(anim,"[sample_simpleworld] The filename of the animation file to load (default is none)");
PARAM(clip,"[sample_simpleworld] The filename of the clip file to load (default is none)");
PARAM(playanim,"[sample_simpleworld] The index of the animation to play (default is none)");
PARAM(seqanim,"[sample_simpleworld] If specified, animations will be played in sequence");
PARAM(elist,"[sample_simpleworld] The filename of the entity list (.elist) file to load");
PARAM(flist,"[sample_simpleworld] The filename of the fragment entity list (.flist) file to load");
PARAM(fdlist,"[sample_simpleworld] The filename of the fragment drawable entity list (.fdlist) file to load");
PARAM(alist,"[sample_simpleworld] The filename of the animation list (.alist) file to load");
PARAM(gtlistpath,"[sample_simpleworld] The path to a list of global textures to load");
PARAM(gtpath,"[sample_simpleworld] The search path to look for global textures in");
PARAM(rageshaderpath,"[sample_rmcore] Set rage shader root path (tune/shaders/lib and tune/shaders/db are appended)");
PARAM(skelforskinonly, "[sample_simpleworld] Project specific option for RDR2 to make the simpleworld viewer render like sample_drawable did.");
PARAM(attachment, "[sample_simpleworld] Specify an attachment (<parent>,<child>,<parentBone>,<childLoc>).");
//PARAM(autotexdict,"[sample_simpleworld] Automatic texture dictionary creation" );
PARAM(terrainplane,"[sample_simpleworld] Whether or not to construct a terrain plane.  Default is true.");
PARAM(autoframe,"[sample_simpleworld] automatically frames the first entity.");
PARAM(rmptfxloadptxlist, "[sample_simpleworld] Load a list of effect rules.");
PARAM(rmptfxsaveall, "[sample_simpleworld] Re-save ptx assets.");
PARAM(expr, "[sample_simpleworld] The filename of the expression file to load.");

PARAM(takesnapshot,"[sample_simpleworld] takes a screenshot every frame, ends with smoketest, render target based" );
PARAM(takesnapshotset,"[sample_simpleworld] this command line is made specifically for taking city sector snapshots" );
PARAM(snapshotquality,"[sample_simpleworld] sets JPEG quality for takesnapshot (0-100)");
PARAM(shownumverts,"[sample_simpleworld] Shows total vertices in scene");
PARAM(shownumtris,"[sample_simpleworld] Shows total triangles in scene");
PARAM(shownumgeometries,"[sample_simpleworld] Shows total geometries in scene");
PARAM(modeldebugger,"[sample_simpleworld] Run with model debugger");
PARAM(modeldebuggeractive,"[sample_simpleworld] Set model debugger active");
PARAM(modeldebuggermode,"[sample_simpleworld] Sets the technique that the model debugger start up with using the above command line (normalweighted,vertcolor2,vertcoloralpha2,texcoords,texcoord2,texture0,texture1) default is normal");
PARAM(drawboundingbox,"[sample_simpleworld] Draws the bounding box, the last one if there's more than one drawable for now. only works with profiledraw/nonrelease");
PARAM(drawbound,"[sample_simpleworld] Draws the bound, must load file using -fragfile instead of -file");
PARAM(modeldebuggershader,"[sample_simpleworld] Sets a custom shader for the model debugger to use");
PARAM(mdshadertechs,"[sample_simpleworld] techniques to register for the model debugger");
PARAM(modeldebuggertexture,"[sample_simpleworld] Sets a custom texture for the model debugger to use");

PARAM(audioon,"[sample_simpleworld] Don't initialize or play audio");

XPARAM(mrt);
XPARAM(hdr);

PARAM(nopostfx,"[sample_simpleworld] if someone wants to switch off Postfx ... you do not want to do this .. really!.");

PF_PAGE(Rage_SimpleWorld, "RAGE Sample - major systems");
PF_GROUP(Rage_SimpleWorld);
PF_LINK(Rage_SimpleWorld, Rage_SimpleWorld);
PF_TIMER(UpdateWind, Rage_SimpleWorld);
PF_TIMER(UpdatePhysics, Rage_SimpleWorld);
PF_TIMER(UpdateAnimation, Rage_SimpleWorld);
PF_TIMER(UpdateEntities, Rage_SimpleWorld);
PF_TIMER(UpdateAudio, Rage_SimpleWorld);
PF_TIMER(UpdateEffects, Rage_SimpleWorld);
PF_TIMER(UpdateScripts, Rage_SimpleWorld);
PF_TIMER(DrawFragments, Rage_SimpleWorld);
PF_TIMER(DrawWorld, Rage_SimpleWorld);
PF_TIMER(DrawEffects, Rage_SimpleWorld);
PF_TIMER(DrawShadows, Rage_SimpleWorld);
PF_TIMER(DrawPostfx, Rage_SimpleWorld);

using namespace rage;

// overload for vectors
namespace rage
{


XPARAM(nospucollide);

	inline void XMLLogWriteValue( XmlLog& log, const Vector3& val )
	{
		log.NewLine();
		log.Write( "x", val.x );
		log.Write( "y", val.y );
		log.Write( "z", val.z );
		log.NewLine();
	}

datCallback g_ClothManagerCallback = NullCB;

}
namespace ragesamples

{
	class rmcModelDebuggerMode
	{
	public:
		bool m_active;
		int m_currentTechnique;
		int m_currentFillMode;
		int m_viewMode;
	};
	class rmcModelDebugger : rage::datBase
	{
		grmShader*		m_debugShader;
		grmShader*		m_customDebugShader;
		grmShader*		m_oldForceShader;
		int				m_oldForceShaderId;

		grcEffectVar	m_textureId;
		grcEffectVar	m_cameraPos;



		grcTexture * m_tex;

	public:
		const char* m_techniqueNames[64];
	private:
		char* m_customTechniqueNames[64];
		char m_techniqueString[256];
	public:
		const char* m_fillModes[3];

	private:
		int  m_techniqueIds[64];
		int m_currentTechnique;
		int m_currentFillMode;
		grcFillMode m_oldFillMode;
		grcCullMode	m_oldCullMode;

		grcSmallStateStack m_states;

		static rmcModelDebugger *sm_instance;
		rmcModelDebugger(){}
	public:
		bool m_active;
		bool m_currentCullMode;
		Vector3 m_camPos;
		void RegisterTechniques()
		{
			m_techniqueIds[0]=	0;
			m_techniqueIds[1]=	grmShaderFx::RegisterTechniqueGroup("normalweighted");
			m_techniqueIds[2]=	grmShaderFx::RegisterTechniqueGroup("texcoords");
			m_techniqueIds[3]=	grmShaderFx::RegisterTechniqueGroup("texcoords2");
			m_techniqueIds[4]=	grmShaderFx::RegisterTechniqueGroup("vertcolors");
			m_techniqueIds[5]=	grmShaderFx::RegisterTechniqueGroup("vertcolorsalpha");
			m_techniqueIds[6]=	grmShaderFx::RegisterTechniqueGroup("vertcolors2");
			m_techniqueIds[7]=	grmShaderFx::RegisterTechniqueGroup("vertcolorsalpha2");
			m_techniqueIds[8]=	grmShaderFx::RegisterTechniqueGroup("texture1");
			m_techniqueIds[9]=	grmShaderFx::RegisterTechniqueGroup("texture2");
			m_techniqueIds[10]= grmShaderFx::RegisterTechniqueGroup("lt");
			m_techniqueIds[11]= grmShaderFx::RegisterTechniqueGroup("lt2");
			m_techniqueIds[12]= grmShaderFx::RegisterTechniqueGroup("blinnphong");


			const char * customtechniques;
			if(!PARAM_mdshadertechs.Get(customtechniques))
				return;
			strcpy(m_techniqueString,customtechniques);

			int length=strlen(m_techniqueString);
			int pindex=0;
			int techcount=10;
			for(int i=0;i<length;i++)
			{
				if(m_techniqueString[i]==' ')
				{
					m_techniqueString[i]=0;
					m_techniqueIds[techcount]=	grmShaderFx::RegisterTechniqueGroup(m_techniqueString+pindex);
					pindex=i+1;
				}
			}

		}
		void Configure(rmcModelDebuggerMode * mode)
		{
			m_active=mode->m_active;
			m_currentFillMode=mode->m_currentFillMode;
			m_currentTechnique=mode->m_currentTechnique;
			ShaderSwitch();

		}
		void FillModeSwitch(){}
		void ShaderSwitch()
		{
			//if(m_active)	
			//	rage::grmShaderFx::SetForcedTechniqueGroupId(m_techniqueIds[m_currentTechnique]);
		}
	public:
		static bool InitClass()
		{
			if(sm_instance)
			{
				Errorf("Model Debugger alreaedy initialized!");
				return false;
			}
			sm_instance = rage_new rmcModelDebugger();
			//sm_instance->RegisterTechniques();
			return true;
		}
		void AddDebugTechnque( /*const char* techniqueName, const char* debugMenuName*/ )
		{
		}
		bool Init()
		{
			// loads the debug shader
			/*
			char pathname[256];
			ASSET.GetStackedPath(pathname,256,0);
			ASSET.SetPath("T:\\rage\\assets\\tune\\shaders\\lib");	
			ASSET.PopFolder();
			*/
			m_debugShader = grmShaderFactory::GetInstance().Create();
			AssertReturn( m_debugShader /*&& "Shader could not be created"*/ );

			if ( !m_debugShader->Load("rageViewer_debug") )
			{
				AssertReturn( false /*!"Shader could not loaded"*/ );
			}
			// setup the texture id
			const char *texturepath;
			m_tex=0;
			if(PARAM_modeldebuggertexture.Get(texturepath))
			{
				m_tex= grcTextureFactory::GetInstance().CreateTexture(texturepath);
				m_textureId = m_debugShader->LookupVar("DebugTexture");
				m_debugShader->SetVar(m_textureId, m_tex);

			}
/*
#if !__PS3
			else
				m_tex= grcTextureFactory::GetInstance().CreateTexture("C:/debug");
				m_textureId = m_debugShader->LookupVar("DebugTexture");
			m_debugShader->SetVar(m_textureId, m_tex);
#endif
*/
			m_cameraPos=m_debugShader->LookupVar("cameraPos");


			m_active = PARAM_modeldebugger.Get();
			m_currentTechnique=0;

			const char *mode;
			const char * shaderpath;
			if(!PARAM_modeldebuggershader.Get(shaderpath))
				return true;

			m_customDebugShader = grmShaderFactory::GetInstance().Create();
			m_customDebugShader->Load(shaderpath);

#if EFFECT_PRESERVE_STRINGS
			const char * technique;
			char techniqueReg[256];
			for(int i=0;i<m_customDebugShader->GetTechniqueCount();i++)
			{
				technique=m_debugShader->GetTechniqueName(m_debugShader->GetTechniqueByIndex(i));
				if(!strcmp(technique,"draw")||!strcmp(technique,"drawskinned"))
					continue;
				strcpy(techniqueReg,technique);
				for(int i=0;techniqueReg[i]!=0;i++)
				{
					if(techniqueReg[i]=='_')
						techniqueReg[i]=0;
				}
				Displayf(techniqueReg);
			}
#endif
			if(PARAM_modeldebuggermode.GetParameter(mode))
			{
				int techId=grmShaderFx::FindTechniqueGroupId(mode);
				techId=(techId<0)?0:techId;

				grmShaderFx::SetForcedTechniqueGroupId(techId);
			}
			//ASSET.SetPath(pathname);
			return true;
		}
		static rmcModelDebugger &GetInstance()
		{
			return *sm_instance;
		}
		static bool Shutdown()
		{		
			delete sm_instance->m_debugShader;
			if(sm_instance->m_tex)
				sm_instance->m_tex->Release();
			delete sm_instance;

			return true;
		}
		void BeginModelDebugger()
		{
			m_debugShader->SetVar(m_cameraPos, m_camPos);
			// if active 
			// we need to force the shader
			if(!m_active)
				return;

			m_oldForceShader	=grmModel::GetForceShader();
			m_oldForceShaderId	=grmShaderFx::GetForcedTechniqueGroupId();
			m_oldFillMode		=grcState::GetFillMode();
			m_oldCullMode		=grcState::GetCullMode();

			grmModel::SetForceShader( m_debugShader );
			grmShaderFx::SetForcedTechniqueGroupId(m_techniqueIds[m_currentTechnique]);


			if(m_currentFillMode==0)
				grcState::SetFillMode(grcfmSolid);
			if(m_currentFillMode==1)
				grcState::SetFillMode(grcfmWireframe);
			if(m_currentFillMode==2)
				grcState::SetFillMode(grcfmPoint);

			if(!m_currentCullMode)
				grcState::SetCullMode(grccmNone);
		}
		void End()
		{
			if(!m_active) 
				return;
			grcState::SetFillMode(m_oldFillMode);
			grcState::SetCullMode(m_oldCullMode);
			grmModel::SetForceShader( m_oldForceShader );
			grmShaderFx::SetForcedTechniqueGroupId(m_oldForceShaderId);
		}
#if __BANK
		void AddWidgets( bkBank& bank )
		{
			bank.PushGroup( "Model Debugger" );
			m_active=PARAM_modeldebugger.Get();
			bank.AddToggle("Activate", &m_active);

			m_techniqueNames[0]=	"normal";
			m_techniqueNames[1]=	"normalweighted";
			m_techniqueNames[2]=	"texcoords";
			m_techniqueNames[3]=	"texcoords2";
			m_techniqueNames[4]=	"vertcolors";
			m_techniqueNames[5]=	"vertcolorsalpha";
			m_techniqueNames[6]=	"vertcolors2";
			m_techniqueNames[7]=	"vertcolorsalpha2";
			m_techniqueNames[8]=	"texture1";
			m_techniqueNames[9]=	"texture2";
			m_techniqueNames[10]=	"phong";
			m_techniqueNames[11]=	"camera specular";
			m_techniqueNames[12]=	"blinn-phong";

			m_fillModes[0]="solid";
			m_fillModes[1]="wireframe";
			m_fillModes[2]="point";

			m_currentTechnique=0;
			bank.AddCombo("Shader Mode", &m_currentTechnique, 13, m_techniqueNames, 
				datCallback(MFA(rmcModelDebugger::ShaderSwitch),this) );

			m_currentFillMode=0;
			bank.AddCombo("Shader FillMode", &m_currentFillMode, 3, m_fillModes, 
				datCallback(MFA(rmcModelDebugger::FillModeSwitch),this) );

			bank.AddToggle("Backface Culling for Wireframe/PT", &m_currentCullMode);

			bank.PopGroup();
		}
#endif
	};
	rmcModelDebugger *rmcModelDebugger::sm_instance = 0;
static const u32 ARCHETYPE_TYPE_TERRAIN		= 1 << 0;
static const int SIMPLE_WORLD_SCRIPT_SIZE = 20;

//-----------------------------------------------------------------------------
void rmcSampleSimpleWorld::RegisterTechniqueGroups()
{
	//if(PARAM_modeldebugger.Get())
#if !__PS3
		rmcModelDebugger::GetInstance().RegisterTechniques();
#endif
}
int rmcSampleSimpleWorld::GetTriangleCount(/*const rage::fragShaft& shaft*/)
{
	rmcDrawable * drawable=SMPWORLD.GetDrawable();
	if(drawable==NULL)
		return 0;

	rmcLodGroup& lodGroup=drawable->GetLodGroup();
	if(!lodGroup.ContainsLod(0))
		return 0;

	rmcLod &lod=lodGroup.GetLod(0);
	int numTris=0;
	for(int i=0;i<lod.GetCount();i++)
	{
		grmModel * model=lod.GetModel(i);
		if(model==NULL)
			continue;
		for(int j=0;j<model->GetGeometryCount();j++)
		{			
			grmGeometry &geometry=model->GetGeometry(j);
			numTris+=geometry.GetIndexBuffer()->GetIndexCount()/3;
		}
	}
	return numTris;
}
int rmcSampleSimpleWorld::GetVertexCount(/*const rage::fragShaft& shaft*/)
{
	rmcDrawable * drawable=SMPWORLD.GetDrawable();
	if(drawable==NULL)
		return 0;

	rmcLodGroup& lodGroup=drawable->GetLodGroup();
	if(!lodGroup.ContainsLod(0))
		return 0;

	rmcLod &lod=lodGroup.GetLod(0);
	int numVerts=0;
	for(int i=0;i<lod.GetCount();i++)
	{
		grmModel * model=lod.GetModel(i);
		if(model==NULL)
			continue;
		for(int j=0;j<model->GetGeometryCount();j++)
		{			
			grmGeometry &geometry=model->GetGeometry(j);
			numVerts+=geometry.GetVertexBuffer()->GetVertexCount();
		}
	}
	return numVerts;
}
int rmcSampleSimpleWorld::GetGeometryCount(/*const rage::fragShaft& shaft*/)
{
	rmcDrawable * drawable=SMPWORLD.GetDrawable();
	if(drawable==NULL)
		return 0;

	rmcLodGroup& lodGroup=drawable->GetLodGroup();
	if(!lodGroup.ContainsLod(0))
		return 0;

	rmcLod &lod=lodGroup.GetLod(0);

	int numGeos=0;
	for(int i=0;i<lod.GetCount();i++)
	{
		grmModel * model=lod.GetModel(i);
		//how can a model be null!?
		if(model!=NULL)
			numGeos+=model->GetGeometryCount();
	}
	return numGeos;
}
rmcSampleSimpleWorld::rmcSampleSimpleWorld()
	: m_MaxOctreeNodes(1000)
	, m_MaxActivePhysicsObjects(500)
	, m_MaxPhysicsObjects(500)
    , m_constructTerrainPlane(true)
    , m_autoPlayAnimation(-1)
    , m_autoSequenceAnimation(false)
	, m_bShowFragmentHUD(true)
	, m_bShowAnimationHUD(true)
    , m_forceInitCamera(false)
    , m_forceInitLights(false)
    , m_initCamera(false)
    , m_initLights(false)
    , m_pLastEntityLoaded(NULL)
    , m_pLastFragEntityLoaded(NULL)
    , m_pLastFragDrawableEntityLoaded(NULL)
	, m_viewportRenderMode(0)
{

}

//-----------------------------------------------------------------------------

rmcSampleSimpleWorld::~rmcSampleSimpleWorld()
{
}

//-----------------------------------------------------------------------------
void rmcSampleSimpleWorld::Init(const char* path)
{
    rmcShadowList::Instantiate();
    InitFileListLoader();
	//if(PARAM_modeldebugger.Get())
#if !__PS3
	rmcModelDebugger::InitClass();
#endif
    rmcSampleManager::Init( path );
	//if(PARAM_modeldebugger.Get())
	//{
#if !__PS3
	rmcModelDebugger::GetInstance().Init();
#endif
	//}

}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::Shutdown()
{
	//if(PARAM_modeldebugger.Get())
#if !__PS3
	rmcModelDebugger::Shutdown();
#endif

    rmcSampleManager::Shutdown();
 
    FILELISTLOADER->Terminate();
 
    rmcShadowList::Destroy();
	
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::InitFileListLoader()
{
    // Initialize the FILELISTLOADER here, instead of InitClient, so that derived classes will have it available right away
    FILELISTLOADER->Init();

    // Register file save/load callbacks
    FILELISTLOADER->RegisterFile( "file", this, MakeFunctorRet(LoadEntityFile), sfiFileListLoader::NullSaveFileFunctor );
    FILELISTLOADER->RegisterFile( "entity", this, MakeFunctorRet(LoadEntityFile), sfiFileListLoader::NullSaveFileFunctor );
    FILELISTLOADER->RegisterFile( "fragfile", this, MakeFunctorRet(LoadFragEntityFile), sfiFileListLoader::NullSaveFileFunctor );
    FILELISTLOADER->RegisterFile( "fragdrawfile", this, MakeFunctorRet(LoadFragDrawableEntityFile), sfiFileListLoader::NullSaveFileFunctor );
    FILELISTLOADER->RegisterFile( "anim", this, MakeFunctorRet(LoadAnimationFile), sfiFileListLoader::NullSaveFileFunctor );
    FILELISTLOADER->RegisterFile( ENTITY_LIST, this, MakeFunctorRet(LoadEntityListFile), MakeFunctorRet(SaveEntityListFile) );
    FILELISTLOADER->RegisterFile( FRAG_LIST, this, MakeFunctorRet(LoadFragmentListFile), MakeFunctorRet(SaveFragmentListFile) );
    FILELISTLOADER->RegisterFile( ANIM_LIST, this, MakeFunctorRet(LoadAnimationListFile), MakeFunctorRet(SaveAnimationListFile) );
    FILELISTLOADER->RegisterFile( CLIP_LIST, this, MakeFunctorRet(LoadClipListFile), MakeFunctorRet(SaveClipListFile) );
    FILELISTLOADER->RegisterFile( "gtlistpath", this, MakeFunctorRet(InitGlobalTexListPathFunc), MakeFunctorRet(SaveGlobalTexListPath) );
    FILELISTLOADER->RegisterFile( "gtpath", this, MakeFunctorRet(InitGlobalTexSearchPathFunc), MakeFunctorRet(SaveGlobalSearchListPath) );

    FILELISTLOADER->RegisterFile( "lights", this, MakeFunctorRet(LoadLightsFile), MakeFunctorRet(SaveLightsFile) );
    FILELISTLOADER->RegisterFile( "camera", this, MakeFunctorRet(LoadCameraFile), MakeFunctorRet(SaveCameraFile) );
#if USE_RMPTFX
    FILELISTLOADER->RegisterFile( "emitter", this, MakeFunctorRet(LoadParticleEmitterFile), sfiFileListLoader::NullSaveFileFunctor ); // FIXME
#endif
    FILELISTLOADER->RegisterFile( "fragSettings", this, sfiFileListLoader::NullLoadFileFunctor, sfiFileListLoader::NullSaveFileFunctor );  // fixme
    FILELISTLOADER->RegisterFile( "physSettings", this, sfiFileListLoader::NullLoadFileFunctor, sfiFileListLoader::NullSaveFileFunctor );  // fixme
    FILELISTLOADER->RegisterFile( "fogSettings", this, sfiFileListLoader::NullLoadFileFunctor, sfiFileListLoader::NullSaveFileFunctor );  // fixme
#if USE_GRSHADOWMAP
    FILELISTLOADER->RegisterFile( "shadowmap", this, sfiFileListLoader::NullLoadFileFunctor, sfiFileListLoader::NullSaveFileFunctor );  // fixme
#endif 
#if USE_GRCUBESHADOWMAP
    FILELISTLOADER->RegisterFile( "cubeshadowmap", this, sfiFileListLoader::NullLoadFileFunctor, sfiFileListLoader::NullSaveFileFunctor );  // fixme
#endif    

    FILELISTLOADER->RegisterFile( FRAG_DRAW_LIST, this, MakeFunctorRet(LoadFragmentDrawableListFile), MakeFunctorRet(SaveFragmentDrawableListFile) );

    // Register tree node save/load callbacks
    FILELISTLOADER->RegisterTreeNode( "file", this, MakeFunctorRet(LoadEntityTreeNode), sfiFileListLoader::NullSaveTreeNodeFunctor );
    FILELISTLOADER->RegisterTreeNode( "entity", this, MakeFunctorRet(LoadEntityTreeNode), sfiFileListLoader::NullSaveTreeNodeFunctor );
    FILELISTLOADER->RegisterTreeNode( "fragfile", this, MakeFunctorRet(LoadFragEntityTreeNode), sfiFileListLoader::NullSaveTreeNodeFunctor );
    FILELISTLOADER->RegisterTreeNode( "anim", this, MakeFunctorRet(LoadAnimationTreeNode), sfiFileListLoader::NullSaveTreeNodeFunctor );
    FILELISTLOADER->RegisterTreeNode( ENTITY_LIST, this, MakeFunctorRet(LoadEntityListTreeNode), MakeFunctorRet(SaveEntityListTreeNode) );
    FILELISTLOADER->RegisterTreeNode( FRAG_LIST, this, MakeFunctorRet(LoadFragmentListTreeNode), MakeFunctorRet(SaveFragmentListTreeNode) );
    FILELISTLOADER->RegisterTreeNode( ANIM_LIST, this, MakeFunctorRet(LoadAnimationListTreeNode), MakeFunctorRet(SaveAnimationListTreeNode) );
    FILELISTLOADER->RegisterTreeNode( CLIP_LIST, this, MakeFunctorRet(LoadClipListTreeNode), MakeFunctorRet(SaveClipListTreeNode) );
    FILELISTLOADER->RegisterTreeNode( "gtlistpath", this, MakeFunctorRet(LoadGlobalTexListPathTreeNode), MakeFunctorRet(SaveGlobalTexListPathTreeNode) );
    FILELISTLOADER->RegisterTreeNode( "gtpath", this, MakeFunctorRet(LoadGlobalTexSearchPathTreeNode), MakeFunctorRet(SaveGlobalTexSearchPathTreeNode) );

    FILELISTLOADER->RegisterTreeNode( "lights", this, MakeFunctorRet(LoadLightsTreeNode), MakeFunctorRet(SaveLightsTreeNode) );
    FILELISTLOADER->RegisterTreeNode( "camera", this, MakeFunctorRet(LoadCameraTreeNode), MakeFunctorRet(SaveCameraTreeNode) );
#if USE_RMPTFX
    FILELISTLOADER->RegisterTreeNode( "emitter", this, MakeFunctorRet(LoadParticleEmitterTreeNode), sfiFileListLoader::NullSaveTreeNodeFunctor ); // FIXME
    FILELISTLOADER->RegisterTreeNode( "preload_emitters", this, MakeFunctorRet(LoadRmPtfxPreLoadTreeNode), sfiFileListLoader::NullSaveTreeNodeFunctor ); // FIXME
#endif
    FILELISTLOADER->RegisterTreeNode( "fragSettings", this, sfiFileListLoader::NullLoadTreeNodeFunctor, sfiFileListLoader::NullSaveTreeNodeFunctor );  // fixme
    FILELISTLOADER->RegisterTreeNode( "physSettings", this, sfiFileListLoader::NullLoadTreeNodeFunctor, sfiFileListLoader::NullSaveTreeNodeFunctor );  // fixme
    FILELISTLOADER->RegisterTreeNode( "fogSettings", this, sfiFileListLoader::NullLoadTreeNodeFunctor, sfiFileListLoader::NullSaveTreeNodeFunctor );  // fixme
#if USE_GRSHADOWMAP
    FILELISTLOADER->RegisterTreeNode( "shadowmap", this, sfiFileListLoader::NullLoadTreeNodeFunctor, sfiFileListLoader::NullSaveTreeNodeFunctor );  // fixme
#endif
#if USE_GRCUBESHADOWMAP
    FILELISTLOADER->RegisterTreeNode( "cubeshadowmap", this, sfiFileListLoader::NullLoadTreeNodeFunctor, sfiFileListLoader::NullSaveTreeNodeFunctor );  // fixme
#endif

    FILELISTLOADER->RegisterTreeNode( FRAG_DRAW_LIST, this, MakeFunctorRet(LoadFragDrawableListTreeNode), MakeFunctorRet(SaveFragDrawableListTreeNode) );
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::CreateGfxFactories()
{
#if __XENON
	GRCGPRALLOCATION->Init();
#endif

#if USE_GRSHADOWMAP || USE_GRCUBESHADOWMAP
	// 2D screenspace texture that collects the shadows
	GRSHADOWCOLLECTOR->Init();
#endif

	// load techniques and create an instance
	if(!PARAM_nopostfx.Get())
		GRPOSTFX->Init();
	
	if(!PARAM_nopostfx.Get())
	    FILELISTLOADER->RegisterFile( "postfx", GRPOSTFX, MakeFunctorRet(grPostFX::LoadPPPPresetFunc), MakeFunctorRet(grPostFX::SavePPPPresetFunc) );

#if USE_GRSHADOWMAP
	RMCSAMPLESHADOWMAP->Init();
#endif //USE_GRSHADOWMAP

#if USE_GRCUBESHADOWMAP
	RMCCUBESAMPLESHADOWMAP->Init();
#endif //USE_GRSHADOWMAP


	rmcSampleManager::CreateGfxFactories();
	

	char libPath[256];
	const char *assetRoot = GetFullAssetPath();
	PARAM_rageshaderpath.Get(assetRoot);

	sprintf(libPath,"%s/%s",assetRoot,"tune/shaders/lib");

	// load the shader and all the parameters
	if(!PARAM_nopostfx.Get())
		GRPOSTFX->InitShaders(libPath);

#if USE_GRSHADOWMAP
	RMCSAMPLESHADOWMAP->InitShaders(libPath);
#endif //USE_GRSHADOWMAP

#if USE_GRCUBESHADOWMAP
	RMCCUBESAMPLESHADOWMAP->InitShaders(libPath);
#endif //USE_GRSHADOWMAP

#if USE_GRSHADOWMAP || USE_GRCUBESHADOWMAP
	GRSHADOWCOLLECTOR->InitShaders(libPath);
#endif //USE_GRSHADOWMAP

	if(!PARAM_nopostfx.Get())
		GRPOSTFX->CreateRenderTargets(GRCDEVICE.GetWidth(), GRCDEVICE.GetHeight());
	
#if USE_GRSHADOWMAP
	RMCSAMPLESHADOWMAP->CreateRenderTargets();
#endif //USE_GRSHADOWMAP

#if USE_GRCUBESHADOWMAP
	RMCCUBESAMPLESHADOWMAP->CreateRenderTargets();
#endif //USE_GRSHADOWMAP

#if USE_GRSHADOWMAP || USE_GRCUBESHADOWMAP
#if __XENON
	// use compressed shadow collector <-> do not use it if you use cube maps ... in case you do there is no error message and it just won't work
	// anymore
	GRSHADOWCOLLECTOR->CreateRenderTargets(XENON_ONLY(false), NULL, XENON_ONLY( GRCDEVICE.GetHeight() == 600 ), XENON_ONLY(false), false );
#else
	GRSHADOWCOLLECTOR->CreateRenderTargets();
#endif

#endif //USE_GRSHADOWMAP

}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::DestroyGfxFactories()
{
	rmcSampleManager::DestroyGfxFactories();
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::DeviceReset()
{
#if __WIN32PC
	if(!PARAM_nopostfx.Get())
	{
		//GRPOSTFX Render Target Update
		GRPOSTFX->DeleteRenderTargets();
		GRPOSTFX->CreateRenderTargets(GRCDEVICE.GetWidth(),  GRCDEVICE.GetHeight());	

	}
#endif
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::DeviceLost()
{
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::InitClient()
{
	rmcSampleManager::InitClient();

#if __PFDRAW
	GetRageProfileDraw().Init(2000000, true, grcBatcher::BUF_FULL_IGNORE);
	GetRageProfileDraw().SetEnabled(true);
#endif //__PFDRAW

	INIT_PARSER;
/*
	if ( PARAM_autotexdict.Get() )
	{
		grmShaderGroup::SetAutoTexDict( true );
	}
*/	
	INIT_SMPWORLD;
	INIT_SMPWORLDEMGR;
	INIT_SMPWORLDGTEXMGR;
	
	// Init wind.
	phWind::InitClass();

	// Init some wind settings.
	WIND.SetFocalPoint( Vec3V(V_ZERO) ); // Centers the grid at the origin.
	WIND.SetWaterLevel( 0.0f ); // Get rid of the water level, effectively.
	WIND.SetAngle(WIND_TYPE_WATER, PI);
	WIND.SetSpeed(WIND_TYPE_WATER, 0.8f );
	WIND.SetAngle(WIND_TYPE_AIR, 2.0f*PI );
	WIND.SetSpeed(WIND_TYPE_AIR, 0.8f );
// 	WIND.SetUseGridLookup( true );

	// Create an auto-managed cyclone disturbance group, specifying some auto-manage parameters.
	phCycloneAutoData cycAutoData;
	cycAutoData.type = WIND_DIST_GLOBAL;
	cycAutoData.createChance = 0.3f;
	cycAutoData.rangeMin = 5.0f;
	cycAutoData.rangeMax = 15.0f;
	cycAutoData.strengthMin = 0.5f;
	cycAutoData.strengthMax = 3.0f;
	cycAutoData.strengthDelta = 0.3f;
	cycAutoData.shrinkChance = 0.1f;
	cycAutoData.forceMult = 10.0f;
	m_pCycloneGroup = rage_new phWindCycloneGroup();
	m_pCycloneGroup->Init(MAX_WIND_CYCLONES);
	m_pCycloneGroup->SetAutoData(cycAutoData);
	m_pCycloneGroup->SetAutomated(true);

	// Add the disturbance groups.
	WIND.AddDisturbanceGroup(m_pCycloneGroup);

	// Start up wind.
	WIND.InitSession();

#if USE_RMPTFX
	//RMPTFX Initialization
	rmPtfxManager::InitClass();
	RMPTFXMGR.CreatePointPool(8000); 
	RMPTFXMGR.CreateEffectInstPool(2000);

	const char* szPtfxList = NULL;
	if(PARAM_rmptfxloadptxlist.Get(szPtfxList))
	{
		RMPTFXMGR.LoadFxList(szPtfxList, 0);
	}

	if(PARAM_rmptfxsaveall.Get())
	{
#if RMPTFX_EDITOR
		RMPTFXMGR.SaveAll();
#endif // RMPTFX_EDITOR
	}

#if __PPU
	// PS3 PPU callback to help load wind data to SPU so we can query main memory wind field from SPU.
	RMPTFXMGR.SetAddAirSpeedDataForSpuUpdate(MakeFunctor(&PtxAddAirSpeedDataForSpu));
#endif

#if 0
	grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
	grcRenderTarget* bigColorRenderTarget = textureFactory.GetBackBuffer(false);

	grcTextureFactory::CreateParams params;
#if __XENON
	params.HasParent = true;
	params.Parent = NULL;
#endif // __XENON
	u32 colorBpp = 32;
#if __PPU
	if (PARAM_hdr.Get())
	{
		params.Format = grctfA16B16G16R16F;
		colorBpp = 64;
	}
#endif // __PPU

	const u32 smallSizeShift = 1;
	u32 smallWidth = bigColorRenderTarget->GetWidth() >> smallSizeShift;
	u32 smallHeight = bigColorRenderTarget->GetHeight() >> smallSizeShift;
	grcRenderTarget* smallColorRenderTarget = textureFactory.CreateRenderTarget("Particle downsample buffer (color)", grcrtPermanent, smallWidth, smallHeight, colorBpp, &params);
#if __XENON
	params.Parent = smallColorRenderTarget;
	params.IsResolvable = false;
#endif // __XENON
	grcRenderTarget* smallDepthRenderTarget = textureFactory.CreateRenderTarget("Particle downsample buffer (depth)", grcrtDepthBuffer, smallWidth, smallHeight, 32, &params);

	RMPTFXMGR.CreateDrawBucketsForEachQuality();
	RMPTFXMGR.CreateDownsampleBuffers(smallColorRenderTarget, smallDepthRenderTarget);
#else
#endif // __RMPTFX_DOWNSAMPLEBUFFERS

	RMPTFXMGR.CreateDrawList(); 
#endif //USE_RMPTFX

	if(PARAM_skelforskinonly.Get())
	{
		SMPWORLD.SetUseSkelForSkinnedOnly(true);
	}

	//Physics-Fragment Initialization
	phConfig::EnableRefCounting();
	phSimulator::InitClass();
	phDemoWorld::InitClass();

	//Audio Init
	if (PARAM_audioon.Get())
	{
		pgStreamer::InitClass(256);
		vwrAudioManager::InitClass();
	}
	

	m_pFragWorld = rage_new fragWorld("rmcSampleSimpleWorld");

	if(m_UseZUp)
		m_pFragWorld->SetUpAxis(2, GRAVITY);

	m_pFragWorld->Init(m_MaxOctreeNodes, m_MaxActivePhysicsObjects, m_MaxPhysicsObjects, Vec3V(-999.0f, -999.0f, -999.0f), Vec3V(999.0f, 999.0f, 999.0f), 32);
	
    m_pFragWorld->InitUpdateObjects(2);
	m_pFragWorld->Activate();


	RvAnimMgr::InitClass();

	//Entity Managed Initialization
	SMPWORLDEMGR.SetMovableFunctors(MakeFunctor(SMPWORLD, &SimpleWorld::Insert),
									MakeFunctor(SMPWORLD, &SimpleWorld::Remove),
									MakeFunctor(SMPWORLD, &SimpleWorld::Update));

	SMPWORLDEMGR.SetDrawFunctors( MakeFunctorRet(SMPWORLD, &SimpleWorld::AddToDrawBucket) );

	SMPWORLDEMGR.SetCameraFunctors( MakeFunctor(*this, &rmcSampleSimpleWorld::FocusCamera),
									MakeFunctorRet(*this, &rmcSampleSimpleWorld::DistanceFromCamera));
    SMPWORLDEMGR.SetRemoveEntityFunctor( MakeFunctor(*this, &rmcSampleSimpleWorld::OnRemoveEntity) );

	//Setup keyboard hot-keys
	m_Mapper.Map(IOMS_KEYBOARD, KEY_1, m_HotKeyMayaCam);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_2, m_HotKeyLhCam);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_3, m_HotKeyQuakeCam);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_F4, m_HotKeyReset);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_SPACE, m_HotKeyToggleAnim);

	//Default to no render target debug display.
	if(!PARAM_nopostfx.Get())
		GRPOSTFX->SetShowRenderTargetFlag(false);
	
	//Default to disabled post-processing
	if(!PARAM_nopostfx.Get())
		GRPOSTFX->SetDefaultTechnique();

#if __WIN32PC
	Functor0 resetCb = MakeFunctor(rmcSampleSimpleWorld::DeviceReset);
	Functor0 lostCb = MakeFunctor(rmcSampleSimpleWorld::DeviceLost);	
	GRCDEVICE.RegisterDeviceLostCallbacks(lostCb, resetCb);
#endif

    // Initialize the scripting engine
    scrProgram::InitClass();
    scrThread::InitClass( 256 ); 
    scrThread::AllocateThreads( SIMPLE_WORLD_SCRIPT_SIZE );
    
#if __BANK
    scrDebugger::InitClass( SIMPLE_WORLD_SCRIPT_SIZE, true );
#endif

    // Register additional commands
    scrThread::RegisterBuiltinCommands();
    rageBankScriptBindings::Register_BankScript();
    rageSimpleScript::Register_SimpleScript( this );

	//Handle the global texture list loading
	const char* globalTexSearchPath;
	if ( PARAM_gtpath.Get(globalTexSearchPath) )
    {
        m_GlobalTexSearchPath = globalTexSearchPath;
    }

	const char* globalTexListPath;
	if ( PARAM_gtlistpath.Get(globalTexListPath) )
    {
        InitGlobalTextureList( globalTexListPath );
    }

	// Auto-play the specified animation:
	int playAnimIdx = 0;
	if ( PARAM_playanim.Get(playAnimIdx) )
	{
		m_autoPlayAnimation = playAnimIdx;
	}

	// Sequence animations
	if ( PARAM_seqanim.Get() )
	{
		m_autoSequenceAnimation = true;
	}

    const char *pTerrainPlane;
    if ( PARAM_terrainplane.Get(pTerrainPlane) )
    {
        m_constructTerrainPlane = stricmp(pTerrainPlane,"true") == 0;
    }

    if ( m_constructTerrainPlane )
    {
        m_pFragWorld->ConstructTerrainPlane( false, ARCHETYPE_TYPE_TERRAIN, m_UseZUp );
    }

	//Load any animation specified on the command line
	const char*	animFileNames[64];
	char  animParamNameBuf[1024];
	int nAnimFiles = PARAM_anim.GetArray(animFileNames, 64, animParamNameBuf,sizeof(animParamNameBuf));
	for(int animFileIdx=0; animFileIdx < nAnimFiles; animFileIdx++)
	{
		LoadAnimation(animFileNames[animFileIdx]);
	}

	//Load any clip specified on the command line
	const char* clipFileNames[64];
	char clipParamNameBuf[1024];
	int nClipFiles = PARAM_clip.GetArray(clipFileNames, 64, clipParamNameBuf,sizeof(clipParamNameBuf));
	for(int clipFileIdx=0; clipFileIdx < nClipFiles; clipFileIdx++)
	{
		LoadClip(clipFileNames[clipFileIdx]);
	}

	//Load any animation list specified on the command line
	const char* animListFileName;
	if ( PARAM_alist.Get(animListFileName) )
	{
		LoadAnimationList(animListFileName);
	}
	//Load any entity specified on the command line
	const char* entityFileName;
	if ( PARAM_entity.Get(entityFileName) || PARAM_file.Get(entityFileName) )
	{
		LoadEntity(entityFileName);
	}

    //Load any fragment entity specified on the command line
	const char* fragEntityFileName;
	if ( PARAM_fragfile.Get(fragEntityFileName) )
	{
		LoadFragEntity(fragEntityFileName);
	}

	//Load any entity list specified on the command line
	const char* entityListFileName;
	if ( PARAM_elist.Get(entityListFileName) )
	{
		LoadEntityList(entityListFileName);
	}

    //Load any frag entity list specified on the command line
    const char* fragListFileName;
    if ( PARAM_flist.Get(fragListFileName) )
    {
        LoadFragmentList(fragListFileName);
    }

    // Load any world list specified on the command line
    const char *pFileXmlName;
    if ( PARAM_fixml.Get(pFileXmlName) )
    {
#if !__FINAL
		diagContextMessage	mess( "Loading Xml File %s", pFileXmlName );
#endif
        LoadXmlFile(pFileXmlName);
    }

    // Load any world list specified on the command line
    const char *pFileListName;
    if ( PARAM_filist.Get(pFileListName) )
    {
        LoadFileList(pFileListName);
    }

	// Attachments:
	if( PARAM_attachment.Get() )
	{
		const int kNumAttachParams = 4;
		const int kMaxAttachments = 16;
		const char *pAttachParams[kNumAttachParams*kMaxAttachments];
		char buf[1024];
		int numParams = PARAM_attachment.GetArray(pAttachParams, kNumAttachParams*kMaxAttachments, buf, 1024);
		Displayf("numParams: %d", numParams);
		if( numParams%kNumAttachParams == 0 )
		{
			int numAttachments = numParams / kNumAttachParams;
			Displayf("numAttachments : %d", numAttachments);
			for( int i=0; i<numAttachments; ++i )
			{
				int idxBase = i*kNumAttachParams;
				Displayf("Attachment: %s, %s, %s, %s", pAttachParams[idxBase+0], pAttachParams[idxBase+1], pAttachParams[idxBase+2], pAttachParams[idxBase+3]);
				SMPWORLDEMGR.GetAttachmentMgr().Attach(pAttachParams[idxBase+0], pAttachParams[idxBase+1], pAttachParams[idxBase+2], pAttachParams[idxBase+3]);
			}
		}
		else
		{
			Quitf("invalid attachment(s) specified.  Please check your '-attachment' command line");
		}
	}
#if !__FINAL
	SetConsoleSimpleWorld( const_cast<rmcSampleSimpleWorld*>(this) );
#endif

#if USETRANSPARENTCOMPOSITERENDERTARGET
	grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
	grcCompositeRenderTarget::CreateParams params;
	grcRenderTarget* backBuffer = textureFactory.GetBackBuffer(false);
	grcRenderTarget* depthBuffer = textureFactory.GetBackBufferDepth(false);
	params.srcColor = backBuffer;
	params.srcDepth = depthBuffer;
	params.dstColor = backBuffer;
	params.dstDepth = depthBuffer;

	m_CompositeRenderTarget = rage_new grcCompositeRenderTarget(params);
#endif // USETRANSPARENTCOMPOSITERENDERTARGET
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::InitGlobalTextureList(const char* globalTexListPath)
{
	Assert(globalTexListPath);
    bool success = false;

    m_GlobalTexListPath = globalTexListPath;

	ASSET.PushFolder(globalTexListPath);
	fiStream *S = ASSET.Open("globaltex","list");
	if (S) 
	{
		fiTokenizer T(globalTexListPath,S);
		char referenceName[128];
		while (T.GetToken(referenceName,sizeof(referenceName))) 
		{
			char textureName[128];
			T.GetToken(textureName,sizeof(textureName));

			Displayf("Adding global texture reference '%s' for file '%s'", referenceName, textureName);
			SMPWORLDGTEXMGR.AddTextureToGlobalList(referenceName, textureName);
		}
		S->Close();
        success =true;
	}
    else
    {
        m_GlobalTexListPath = "";
    }
	
    ASSET.PopFolder();
    return success;
}
//-----------------------------------------------------------------------------
void rmcSampleSimpleWorld::InitCamera()
{
	rmcSampleManager::InitCamera();

    if ( m_initCamera )
    {
        SetCameraPreset( &m_cameraPreset );
        m_initCamera = false;
    }

    m_forceInitCamera = true;

#if __BANK
	if ( PARAM_autoframe.Get() )
	{
//		if ( SMPWORLDEMGR.GetEntityCount() > 0 )
//		{
//			SMPWORLDEMGR.GetEntity(0)->FrameEntityBankCallback();
//		}
		FrameAllEntites();
	}
#endif

}

//-----------------------------------------------------------------------------
void	rmcSampleSimpleWorld::FrameAllEntites()
{
	if(SMPWORLDEMGR.GetEntityCount() > 0)
	{
		SimpleWorldEntity* pobEntity = SMPWORLDEMGR.GetEntity(0);
		pobEntity->UpdateBoundingSphereAndAABB();
		spdAABB obGlobalBoundingBox = pobEntity->GetAABB();
		obGlobalBoundingBox.Transform(pobEntity->GetTransform());
		for(int e = 1; e<SMPWORLDEMGR.GetEntityCount(); e++)
		{
			pobEntity = SMPWORLDEMGR.GetEntity(e);
			pobEntity->UpdateBoundingSphereAndAABB();
			spdAABB obBoundingBox = pobEntity->GetAABB();
			obBoundingBox.Transform(pobEntity->GetTransform());
			obGlobalBoundingBox.Combine(obBoundingBox);
		}

		switch(m_ViewMode)
		{
		case 0: 
			{
				// Persp, so do nothing
				break;
			}
		case 1: 
			{
				// Top, so zero out x and y aabb data
				Vector3 obCenter = obGlobalBoundingBox.GetCenter();
				if(g_UnitUp == ZAXIS)
				{
					obGlobalBoundingBox.Set(Vector4(obCenter.x, obCenter.y, obCenter.z, 0.25f * Max((obGlobalBoundingBox.GetMax().x - obGlobalBoundingBox.GetMin().x), (obGlobalBoundingBox.GetMax().y - obGlobalBoundingBox.GetMin().y))));
				}
				else
				{
					obGlobalBoundingBox.Set(Vector4(obCenter.x, obCenter.y, obCenter.z, 0.25f * Max((obGlobalBoundingBox.GetMax().x - obGlobalBoundingBox.GetMin().x), (obGlobalBoundingBox.GetMax().z - obGlobalBoundingBox.GetMin().z))));
				}
				break;
			}
		case 2: 
			{
				// Bottom, so zero out x and y aabb data
				Vector3 obCenter = obGlobalBoundingBox.GetCenter();
				if(g_UnitUp == ZAXIS)
				{
					obGlobalBoundingBox.Set(Vector4(obCenter.x, obCenter.y, obCenter.z, 0.25f * Max((obGlobalBoundingBox.GetMax().x - obGlobalBoundingBox.GetMin().x), (obGlobalBoundingBox.GetMax().y - obGlobalBoundingBox.GetMin().y))));
				}
				else
				{
					obGlobalBoundingBox.Set(Vector4(obCenter.x, obCenter.y, obCenter.z, 0.25f * Max((obGlobalBoundingBox.GetMax().x - obGlobalBoundingBox.GetMin().x), (obGlobalBoundingBox.GetMax().z - obGlobalBoundingBox.GetMin().z))));
				}
				break;
			}
		case 3: 
			{
				// Left, so zero out x and y aabb data
				Vector3 obCenter = obGlobalBoundingBox.GetCenter();
				if(g_UnitUp == ZAXIS)
				{
					obGlobalBoundingBox.Set(Vector4(obCenter.x, obCenter.y, obCenter.z, 0.25f * Max((obGlobalBoundingBox.GetMax().z - obGlobalBoundingBox.GetMin().z), (obGlobalBoundingBox.GetMax().y - obGlobalBoundingBox.GetMin().y))));
				}
				else
				{
					obGlobalBoundingBox.Set(Vector4(obCenter.x, obCenter.y, obCenter.z, 0.25f * Max((obGlobalBoundingBox.GetMax().y - obGlobalBoundingBox.GetMin().y), (obGlobalBoundingBox.GetMax().z - obGlobalBoundingBox.GetMin().z))));
				}
				break;
			}
		case 4: 
			{
				// Right, so zero out x and y aabb data
				Vector3 obCenter = obGlobalBoundingBox.GetCenter();
				if(g_UnitUp == ZAXIS)
				{
					obGlobalBoundingBox.Set(Vector4(obCenter.x, obCenter.y, obCenter.z, 0.25f * Max((obGlobalBoundingBox.GetMax().z - obGlobalBoundingBox.GetMin().z), (obGlobalBoundingBox.GetMax().y - obGlobalBoundingBox.GetMin().y))));
				}
				else
				{
					obGlobalBoundingBox.Set(Vector4(obCenter.x, obCenter.y, obCenter.z, 0.25f * Max((obGlobalBoundingBox.GetMax().y - obGlobalBoundingBox.GetMin().y), (obGlobalBoundingBox.GetMax().z - obGlobalBoundingBox.GetMin().z))));
				}
				break;
			}
		case 5: 
			{
				// Front, so zero out x and y aabb data
				Vector3 obCenter = obGlobalBoundingBox.GetCenter();
				if(g_UnitUp == ZAXIS)
				{
					obGlobalBoundingBox.Set(Vector4(obCenter.x, obCenter.y, obCenter.z, 0.25f * Max((obGlobalBoundingBox.GetMax().x - obGlobalBoundingBox.GetMin().x), (obGlobalBoundingBox.GetMax().z - obGlobalBoundingBox.GetMin().z))));
				}
				else
				{
					obGlobalBoundingBox.Set(Vector4(obCenter.x, obCenter.y, obCenter.z, 0.25f * Max((obGlobalBoundingBox.GetMax().x - obGlobalBoundingBox.GetMin().x), (obGlobalBoundingBox.GetMax().y - obGlobalBoundingBox.GetMin().y))));
				}
				break;
			}
		case 6: 
			{
				// Back, so zero out x and y aabb data
				Vector3 obCenter = obGlobalBoundingBox.GetCenter();
				if(g_UnitUp == ZAXIS)
				{
					obGlobalBoundingBox.Set(Vector4(obCenter.x, obCenter.y, obCenter.z, 0.25f * Max((obGlobalBoundingBox.GetMax().x - obGlobalBoundingBox.GetMin().x), (obGlobalBoundingBox.GetMax().z - obGlobalBoundingBox.GetMin().z))));
				}
				else
				{
					obGlobalBoundingBox.Set(Vector4(obCenter.x, obCenter.y, obCenter.z, 0.25f * Max((obGlobalBoundingBox.GetMax().x - obGlobalBoundingBox.GetMin().x), (obGlobalBoundingBox.GetMax().y - obGlobalBoundingBox.GetMin().y))));
				}
				break;
			}
		}

		spdSphere obFocusSphere(VECTOR3_TO_VEC3V(obGlobalBoundingBox.GetCenter()), Mag(obGlobalBoundingBox.GetHalfExtents()));
		SMPWORLDEMGR.FrameCamera(obFocusSphere);
	}
}


//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::FocusCamera(const spdSphere& focusSphere)
{
	if(m_ViewMode == 0)
	{
		if(PARAM_takesnapshotset.Get())
		{
			// m_CamMgr.Init(Vector3(focusSphere.GetRadius(), 0.0f, focusSphere.GetRadius()), focusSphere.GetCenter());
			m_CamMgr.Init(Vector3(2.0f * focusSphere.GetRadiusf(), 4.0f * focusSphere.GetRadiusf(), 0.0f), VEC3V_TO_VECTOR3(focusSphere.GetCenter()), dcamCamMgr::CAM_MAYA);
		}
		m_CamMgr.Frame(focusSphere);
	}
	else
	{
		m_OrthoCam.Frame(focusSphere);
	}
	if(PARAM_takesnapshotset.Get())
	{
		int iViewMode = m_ViewMode;
		for(int i=1;i<8;i++)
		{
			m_ViewMode=i;
			m_OrthoCam.Frame(focusSphere);
			UpdateCamera();
		}
		m_ViewMode=iViewMode;
	}
}

//-----------------------------------------------------------------------------

float rmcSampleSimpleWorld::DistanceFromCamera(const Vector3& point)
{
	//Return the distance from the camera
	const Matrix34& camWrldMtx = m_CamMgr.GetWorldMtx();
	Vector3 ptToCamVec;
	ptToCamVec.Subtract(camWrldMtx.d, point);
	return ptToCamVec.Mag();
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::OnRemoveEntity( SimpleWorldEntity * /*pEntity*/ )
{
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::InitLights()
{
	rmcSampleManager::InitLights();

    if ( m_initLights )
    {
        SetLightPreset( &m_lightPreset );
        m_initLights = false;
    }

    m_forceInitLights = true;
	m_LightMode = grclmPoint;
}
//-----------------------------------------------------------------------------
void rmcSampleSimpleWorld::UpdateClient()
{
	//Keyboard update
	m_Mapper.Update();
	if(m_HotKeyMayaCam.IsPressed())
	{
		m_CamMgr.SetCamera(dcamCamMgr::CAM_MAYA);
	}
	else if(m_HotKeyLhCam.IsPressed())
	{
		m_CamMgr.SetCamera(dcamCamMgr::CAM_LH);
	}
	else if(m_HotKeyQuakeCam.IsPressed())
	{
		m_CamMgr.SetCamera(dcamCamMgr::CAM_QUAKE);
	}
	else if(m_HotKeyReset.IsPressed())
	{
		//Reset the RMPTFX system
		RMPTFXMGR.Reset();
	}
	else if(m_HotKeyToggleAnim.IsPressed())
	{
		m_globalAnimMgr.TogglePlayMode();
	}

	//Base class update 
	rmcSampleManager::UpdateClient();

	{
		PF_FUNC(UpdateWind);
		WIND.Update(TIME.GetSeconds(), (u32)(TIME.GetUnpausedElapsedTime()*1000.0f), TIME.GetFrameCount());
	}

	//Physics update
	{
		PF_FUNC(UpdatePhysics);

		m_pFragWorld->Update();

		if(m_HotKeyReset.IsPressed())
		{
			//Reset all of the fragment entities to the orientation/position specified by the entity
			int entityCount = SMPWORLDEMGR.GetEntityCount();
			for(int i=0; i<entityCount; i++)
			{
				SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity(i);
				SimpleWorldFragmentEntity *pFragEntity = dynamic_cast<SimpleWorldFragmentEntity*>(pEntity);
				if(pFragEntity)
				{
					pFragEntity->Reset();
				}
			}

			m_pFragWorld->Reset();
		}
	}

	//Global animation manager update
	{
		//If an animation was set to auto-play then trigger it on the first time through this update loop
		if ( m_autoPlayAnimation > -1 )
		{
			m_globalAnimMgr.SetCurrentAnimationIndex( m_autoPlayAnimation );
			AnimMgrAnimIndexChanged( &m_globalAnimMgr );
			m_autoPlayAnimation = -1;
		}

		//If we are set to sequence animations then trigger it on the first time through this update loop
		if ( m_autoSequenceAnimation )
        {
            m_globalAnimMgr.SetSequence(m_autoSequenceAnimation);
            m_autoSequenceAnimation = false;
        }

		PF_FUNC(UpdateAnimation);
		m_globalAnimMgr.Update();
		m_globalAnimMgr.UpdateClipEditorCurrentTime();
	}

	//Enity manager update
	{
		PF_FUNC(UpdateEntities);
		SMPWORLDEMGR.Update();
	}

	// Wind update.
	//WIND.Update(TIME.GetSeconds(), TIME.GetUnpausedElapsedTime(), TIME.GetFrameCount());

	//Audio Update
	if (PARAM_audioon.Get())
	{
		PF_FUNC(UpdateAudio);
		vwrAudioManager::Update(GetCameraMatrix(), false);
	}

#if USE_RMPTFX
	{
		PF_FUNC(UpdateEffects);
		RMPTFXMGR.BeginFrame();
		RMPTFXMGR.Update(TIME.GetSeconds());
	}
#endif //USE_RMPTFX
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::DrawHelpClient()
{
	//Animation HUD drawing
	if(m_bShowAnimationHUD)
	{	
		char tmpString[512];
		bool bDrawFame = true;
		float top = 40;
		float left = 210;

		if(m_globalAnimMgr.IsInAnimMode())
		{
			int animIdx = m_globalAnimMgr.GetCurrentAnimationIndex();
			if( animIdx == 0)
			{
				//Animation is in the base pose
				SetDrawStringPos(left, top);
				if(!PARAM_takesnapshotset.Get())
				{
					DrawString("Anim Name : Base Pose", false);
				}
				bDrawFame = false;
			}
			else
			{
				crAnimation* pCurrentAnim = m_globalAnimMgr.GetAnimation(animIdx);
				if(pCurrentAnim)
				{
					const char *szAnimName = pCurrentAnim->GetName();
					formatf(tmpString, sizeof(tmpString), "Anim Name : %s",
						szAnimName ? ASSET.FileName(szAnimName) : "Unknown");
		
					SetDrawStringPos(left, top);
					DrawString(tmpString, false);
				}
			}
		}
		else
		{
			//Playing back a clip
			int clipIdx = m_globalAnimMgr.GetCurrentClipIndex();
			if( clipIdx == 0 )
			{
				//Animation is in the base pose
				SetDrawStringPos(left, top);
				DrawString("Clip Name : Base Pose", false);
				bDrawFame = false;
			}
			else
			{
				crClip* pCurrentClip = m_globalAnimMgr.GetClip(clipIdx);
				if(pCurrentClip)
				{
					const char *szClipName = pCurrentClip->GetName();
					formatf(tmpString, sizeof(tmpString), "Clip Name : %s",
						szClipName ? ASSET.FileName(szClipName) : "Unknown");
		
					SetDrawStringPos(left, top);
					DrawString(tmpString, false);
				}
			}
		}

		if(bDrawFame)
		{
			top += 12.f;

			formatf(tmpString, sizeof(tmpString), "Frame : %.1f", m_globalAnimMgr.GetCurFrame());
			SetDrawStringPos(left, top);
			DrawString(tmpString, false);

			left += 160.f;
			formatf(tmpString, sizeof(tmpString), "Phase : %.2f%%", m_globalAnimMgr.GetCurPhase() * 100.f);
			SetDrawStringPos(left, top);
			DrawString(tmpString, false);
		}
	}

	//Fragment HUD drawing
	if(m_pFragWorld && m_bShowFragmentHUD)
	{
		float textLeft, textTop;
		char tmpString[100];

		textLeft = GRCDEVICE.GetWidth() - 180.0f;
		textTop = 40;
		SetDrawStringPos(textLeft,textTop);

		if(!PARAM_takesnapshotset.Get())
		{
			formatf(tmpString, sizeof(tmpString), "real time: %6.3f", m_pFragWorld->GetFrameTime() * 1000.0f);

			DrawString(tmpString, false);		

			formatf(tmpString,
					sizeof(tmpString),
					"sim time:  %6.3f %s%s",
					m_pFragWorld->GetSimTimeLastFrame() * 1000.0f,
					m_pFragWorld->GetFixedFrame() ? "*" : "",
					m_pFragWorld->GetAltFxiedFrame() ? "+" : "");

			DrawString(tmpString, false);

			formatf(tmpString, sizeof(tmpString), "max sim time: %3.f", m_pFragWorld->GetMaximumFrameTime() * 1000.0f);
			DrawString(tmpString, false);

			DrawString(phMouseInput::GetLeftClickModeString(), false);
			DrawString(phMouseInput::GetScaleString(), false);

			formatf(tmpString, sizeof(tmpString), "timewarp:  %6.3f", m_pFragWorld->GetTimeWarp());
			DrawString(tmpString, false);
					
			formatf(tmpString, sizeof(tmpString), "oversample:  %4d", m_pFragWorld->GetOversampling());
			DrawString(tmpString, false);

			formatf(tmpString, sizeof(tmpString), "gravity:   %6.3f", m_pFragWorld->GetGravityScale() * GRAVITY);
			DrawString(tmpString, false);
		}

		if(PARAM_shownumgeometries.Get())
		{
			//formatf(tmpString, sizeof(tmpString), "verticies:   %4d", grmModel::sm_VertexCount);
			formatf(tmpString, sizeof(tmpString), "geometries:  %4d", GetGeometryCount());

			DrawString(tmpString, false);
		}
		if(PARAM_shownumverts.Get())
		{
			//formatf(tmpString, sizeof(tmpString), "verticies:   %4d", grmModel::sm_VertexCount);
			formatf(tmpString, sizeof(tmpString), "verticies:  %4d", GetVertexCount());

			DrawString(tmpString, false);
		}
		if(PARAM_shownumtris.Get())
		{
			//formatf(tmpString, sizeof(tmpString), "triangles:   %4d", grmModel::sm_TriangleCount);
			formatf(tmpString, sizeof(tmpString), "triangles:  %4d", GetTriangleCount());
			DrawString(tmpString, false);
		}
	}//End if(m_pFragWorld && m_bShowFragmentHUD)
}

//-----------------------------------------------------------------------------
void rmcSampleSimpleWorld::Draw()
{
#if !__PS3
	static int counter=0;
	static bool bDebugDrawActive=true;
	static int iDebugDrawFillMode=0;
	static int iDebugDrawTechnique=0;
	static int iDebugDrawViewMode=0;
	if (!GRCDEVICE.IsCreated())
		return;

	m_Setup->BeginDraw();

	if(PARAM_takesnapshot.Get() || PARAM_takesnapshotset.Get())
		GRCDEVICE.BeginTakeScreenShot();
	if(PARAM_takesnapshotset.Get())
	{
		m_ColorScheme = SCHEME_MAYA;
		if(counter>12)
			counter=12;
		rmcModelDebuggerMode mode;
		mode.m_active = bDebugDrawActive;
		mode.m_currentFillMode = iDebugDrawFillMode;
		mode.m_currentTechnique = iDebugDrawTechnique;
		mode.m_viewMode = iDebugDrawViewMode;
		rmcModelDebugger::GetInstance().Configure(&mode);
		m_ViewMode=iDebugDrawViewMode;
		FrameAllEntites();
		UpdateCamera();
	}
#else
	m_Setup->BeginDraw();
#endif

	// this initializes the camera system
	SetupCamera();

	// in some examples this is procedural texture stuff -> we create procedural textures in render targets
	PreDrawClient();

	//
	// this is the shadow pass == Z pre-pass
	//
	// The shadow-map code can not handle situations in which the viewer frustum is an orthographic projection, so disable the shadowmap rendering
	// when we are in any viewport besides the perspective view.
	if(m_ViewMode == 0)
	{
		PF_FUNC(DrawShadows);

		// render from the lights point of view shadows into the depth buffer
		RenderIntoShadowMaps();

		#if USE_GRSHADOWMAP || USE_GRCUBESHADOWMAP
			GRSHADOWCOLLECTOR->LockShadowCollector();
		#endif

		// render from the viewer's perspective shadow data into a screen-space texture
		RenderIntoShadowCollector();

		#if USE_GRSHADOWMAP || USE_GRCUBESHADOWMAP
			GRSHADOWCOLLECTOR->UnlockShadowCollector();
		#endif

		#if USE_GRSHADOWMAP || USE_GRCUBESHADOWMAP
			GRSHADOWCOLLECTOR->SetShadowCollectorMap();
		#endif
	}

	PostShadowPreDrawClient();

#if __XENON // this is lame ... no idea why we need that clear here ...
	GRCDEVICE.Clear(true, m_Setup->GetClearColor(), true, 1.0f, false);
#endif

	// this is a lame workaround for the PC ... will be removed sometime
#if __WIN32PC
	// Bind render target for PC
	if(!PARAM_nopostfx.Get())
		GRPOSTFX->BindPCRenderTarget(true);
#endif

#if __XENON
	if (m_UseAA)
	{
		grcResolveFlags clearParams;
		clearParams.Color = GetBackGroundColor();
		GRCDEVICE.BeginTiledRendering(NULL,NULL,&clearParams, m_UseAA);
	}
#elif __WIN32PC
	bool bMSAA = false;
	grcTextureFactory::CreateParams params;
	GRCDEVICE.GetMultiSample(*(u32*)&params.Multisample, params.MultisampleQuality);
	if (params.Multisample != 0)
	{
		bMSAA = true;
		grcState::SetState( grcsMultiSample, bMSAA );
	}
#endif
	//
	// if the background is not drawn ... at least clear color and depth
	//
	if(m_DrawBackground)
	{
		DrawBackground();
	}
	else
	{
		// clear render target
		GRCDEVICE.Clear(true, m_Setup->GetClearColor(), true, 1.0f, false);
	}

#if __WIN32
	// Clip planes must be respecified after any camera change because they
	// are transformed by the current composite matrix before being sent
	// down to the hardware.
	if (m_UserClip) {
		GRCDEVICE.SetClipPlaneEnable(1 << 0);
		GRCDEVICE.SetClipPlane(0, m_UserClipPlane);
	}
#endif



	if (m_DrawGrid)
	{
		if (m_DrawThreeGrids)
		{
			DrawGrid(Vector3(1.0f, 0.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f));
			DrawGrid(Vector3(0.0f, 1.0f, 0.0f));
			DrawGrid(Vector3(0.0f, 0.0f, 1.0f));
		}
		if(m_UseZUp)
		{
			DrawGrid(Vector3(0.0f, 0.0f, 1.0f));
		}
		else
		{
			DrawGrid(Vector3(0.0f, 1.0f, 0.0f));
		}
	}


#if __WIN32
	if (m_UserClip) {
		GRCDEVICE.SetClipPlaneEnable(0);
	}
#endif

	// Draw the lights
	DrawLights();

	// Draw the gizmos
	grcBindTexture(NULL);
	GIZMOMGR.Draw();

	SetLights();

	PIXBegin(0, "DrawClient");
	//if(PARAM_modeldebugger.Get())
#if !__PS3
	const Matrix34 &mat=rmcSampleManager::GetCameraMatrix();
	rmcModelDebugger::GetInstance().m_camPos = mat.GetVector(3);
	switch(rmcSampleManager::GetViewMode())
	{
	case 0: break;
	case 1: rmcModelDebugger::GetInstance().m_camPos.z -= 1000.0f; break; // Bottom
	case 2: rmcModelDebugger::GetInstance().m_camPos.z += 1000.0f; break; // Top
	case 3: rmcModelDebugger::GetInstance().m_camPos.x -= 1000.0f; break; // Left
	case 4: rmcModelDebugger::GetInstance().m_camPos.x += 1000.0f; break; // Right
	case 5: rmcModelDebugger::GetInstance().m_camPos.y += 1000.0f; break; // Front
	case 6: rmcModelDebugger::GetInstance().m_camPos.y -= 1000.0f; break; // Back
	}
	rmcModelDebugger::GetInstance().BeginModelDebugger();
#endif

	DrawClient();

#if __PFDRAW
	if(PARAM_drawboundingbox.Get())
	{
		Color32 color(0,255,255);
		rmcDrawable * drawable=SMPWORLD.GetDrawable();
		Matrix34 mat;
		mat.Identity();
		const Matrix34 * matp=&mat;
		if(drawable!=NULL)
			drawable->ProfileDrawBoundingBox(matp,color,false);
	}
	if(PARAM_drawbound.Get()&&PARAM_fragfile.Get()&&SMPWORLDEMGR.GetEntityCount()>0)
	{
		SimpleWorldFragDrawableEntity * entity=reinterpret_cast<SimpleWorldFragDrawableEntity*>(SMPWORLDEMGR.GetEntity(0));
		bool drawbound=entity->DoDrawBound();
		entity->SetDrawBound(true);
		
		entity->DrawBound();

		entity->SetDrawBound(drawbound);
	}
#endif
#if !__PS3
	//if(PARAM_modeldebugger.Get())
	rmcModelDebugger::GetInstance().End();
#endif

#if !USETRANSPARENTCOMPOSITERENDERTARGET
	PIXBegin(0, "DrawClientTransparent");
	DrawClientTransparent();
	PIXEnd();
#endif // !USETRANSPARENTCOMPOSITERENDERTARGET
	PIXEnd();

#if __XENON		
	if (m_UseAA)
		GRCDEVICE.EndTiledRendering();
#elif __WIN32PC
	if (bMSAA)
	{
		bMSAA = false;
		grcState::SetState( grcsMultiSample, bMSAA );
	}
#endif

#if USETRANSPARENTCOMPOSITERENDERTARGET
	PIXBegin(0, "DrawClientTransparent");

	GRCDEVICE.MSAABlitBackToFrontBuffer(true, false);
	m_CompositeRenderTarget->BeginRenderingDst();
	DrawClientTransparent();
	m_CompositeRenderTarget->EndRenderingDst();
	GRCDEVICE.MSAABlitBackToFrontBuffer(true, true);

	//m_CompositeRenderTarget->CompositeDstToSrc();

	PIXEnd();
#endif // USETRANSPARENTCOMPOSITERENDERTARGET

	//
	// the reason why this is here, is that I do not want the 
	// help, world axes and logo rendered in here .. they should not be infected by the post fx
	//
	// run post-processing pipeline
	if(!PARAM_nopostfx.Get())
	{
		PF_FUNC(DrawPostfx);
		GRPOSTFX->Process();
	}

	// Try to recover from shaderfx rendering
	//grcState::Default();	
	
	grcWorldIdentity();

	DrawClientDebug();
	DrawStatsAndInfo();

	m_Setup->EndDraw();
#if !__PS3
	const char * filename=NULL;
	char dest[256];
	int quality=50;
	if(PARAM_takesnapshot.Get(filename) ||PARAM_takesnapshotset.Get(filename)){
		grcImage *sc=GRCDEVICE.EndTakeScreenShot();
		if (sc)
		{
			strcpy(dest,filename);
			if(PARAM_takesnapshotset.Get())
			{
				// strcat(dest,technames[counter]);
				// strcat(dest,".jpg");
				// counter++;
				// sprintf(dest, "%s_Active%d_FillMode%d_Technique%d_ViewMode%d.jpg", filename, bDebugDrawActive, iDebugDrawFillMode, iDebugDrawTechnique, iDebugDrawViewMode);
				switch(iDebugDrawViewMode)
				{
				case 0: strcat(dest, "_persp"); break;
				case 1: strcat(dest, "_bottom"); break;
				case 2: strcat(dest, "_top"); break;
				case 3: strcat(dest, "_left"); break;
				case 4: strcat(dest, "_right"); break;
				case 5: strcat(dest, "_front"); break;
				case 6: strcat(dest, "_back"); break;
				default: strcat(dest, "_unknown"); break;
				}
				if(bDebugDrawActive)
				{
					strcat(dest, "_");
					strcat(dest, rmcModelDebugger::GetInstance().m_techniqueNames[iDebugDrawTechnique]);
					strcat(dest, "_");
					strcat(dest, rmcModelDebugger::GetInstance().m_fillModes[iDebugDrawFillMode]);
				}
				strcat(dest, ".jpg");

				iDebugDrawViewMode++;
				if(iDebugDrawViewMode > 6)
				{
					iDebugDrawViewMode = 0;
					iDebugDrawTechnique++;
					if(iDebugDrawTechnique > 12)
					{
						iDebugDrawTechnique = 0;
						iDebugDrawFillMode++;
						if(iDebugDrawFillMode > 1)
						{
							iDebugDrawFillMode = 0;
							bDebugDrawActive=!bDebugDrawActive;
						}
					}
				}
			}
			if(PARAM_snapshotquality.Get())
				PARAM_snapshotquality.Get(quality);
			sc->SaveJPEG(dest,quality);
			sc->Release();

			if(PARAM_takesnapshotset.Get())
			{
				if(!bDebugDrawActive && iDebugDrawTechnique)
				{
					// Full loop complete, so bail
					m_WantsExit = true;
				}
			}
		}
	}
#endif
}


#if __XENON
static int	OldFormat;
#endif
//-----------------------------------------------------------------------------
void rmcSampleSimpleWorld::RenderIntoShadowMaps()
{
#if USE_GRSHADOWMAP
	// use shadow list
	RMCSAMPLESHADOWMAP->SetObjectList( rmcShadowList::InstancePtr() );

#if __XENON
	// switch off fp Z buffer
	OldFormat = GRCDEVICE.SetfpZBuffer(false);
#endif

	RMCSAMPLESHADOWMAP->RenderIntoShadowMaps();

#if __XENON
	// was the old format a fp Z buffer and are we in the fourth shadow map now?
	if(GRCDEVICE.IsItfpZBuffer(OldFormat))
		// switch on fp Z buffer
		GRCDEVICE.SetfpZBuffer(true);
#endif

#endif //USE_GRSHADOWMAP
#if USE_GRCUBESHADOWMAP
	RMCCUBESAMPLESHADOWMAP->RenderIntoCubeShadowMap();
#endif //USE_GRSHADOWMAP
}

void rmcSampleSimpleWorld::RenderIntoShadowCollector()
{
	// alpha blend the shadow maps in there.
#if USE_GRSHADOWMAP
	// use shadow list
	RMCSAMPLESHADOWMAP->SetObjectList( rmcShadowList::InstancePtr() );

	RMCSAMPLESHADOWMAP->RenderShadowsIntoShadowCollector();
#endif //USE_GRSHADOWMAP
#if USE_GRCUBESHADOWMAP
	RMCCUBESAMPLESHADOWMAP->RenderShadowsIntoShadowCollector();
#endif //USE_GRSHADOWMAP
}

//-----------------------------------------------------------------------------
void rmcSampleSimpleWorld::DrawClient()
{

#if __PFDRAW
	SMPWORLDEMGR.ProfileDraw();
	const Matrix34 & cameraMtx = GetCamMgr().GetWorldMtx();
	GetRageProfileDraw().SendDrawCameraForServer(cameraMtx);
	GetRageProfileDraw().Render(true);
#endif //__PFDRAW

#if USE_GRSHADOWMAP
	// preview ortho viewports
	RMCSAMPLESHADOWMAP->DebugOrthoViewport(false);
#endif
	//Simpleworld drawing
	fragShaft shaft;
	if(m_ViewMode == 0)
	{
		shaft.Set(*m_Viewport, RCC_MATRIX34(m_Viewport->GetCameraMtx()));
	}
	else
	{
		Matrix34 mat;
		mat.Identity();
		mat.d = VEC3V_TO_VECTOR3(m_Viewport->GetCameraPosition());
		shaft.Set(*m_Viewport, mat);
	}

	{
		PF_FUNC(DrawFragments);
		// Fragment drawing
		m_pFragWorld->SetCamera( *m_Viewport, shaft.GetMatrix() );
		m_pFragWorld->UpdateMouse();
		m_pFragWorld->Draw();
	}

	{
		PF_FUNC(DrawWorld);
		SMPWORLD.Draw(shaft);
	}

#if USE_RMPTFX
	//RMPTFX drawing

	XENON_ONLY( if (!m_UseAA ) ) // predicated tiling doesn't work with depth resolves for particles
	{
		PF_FUNC(DrawEffects);
		grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
		grcRenderTarget* bigDepthRenderTarget = textureFactory.GetBackBufferDepth(__XENON);
		grcRenderTarget* bigColorRenderTarget = textureFactory.GetBackBuffer(__XENON);
		RMPTFXMGR.SetFrameBuffer(bigColorRenderTarget);
		RMPTFXMGR.SetDepthBuffer(bigDepthRenderTarget);
		RMPTFXMGR.Draw();//-1, false); // High res
		//RMPTFXMGR.Draw();//-1, true); // Downsampled
	}
#endif //USE_RMPTFX

	{
		PF_FUNC(UpdateScripts);
		// Update the scripts
		scrThread::UpdateAll( 0 );
		BANK_ONLY(scrDebugger::UpdateAll());
	}

#if __BANK && __PFDRAW
	WIND.DebugDraw();
#endif // __BANK
}

void rmcSampleSimpleWorld::DrawClientTransparent()
{
}

void rmcSampleSimpleWorld::DrawClientDebug()
{
#if USE_GRSHADOWMAP
	RMCSAMPLESHADOWMAP->ShowShadowRenderTargets();
#endif

#if USE_GRCUBESHADOWMAP
	RMCCUBESAMPLESHADOWMAP->ShowCubeShadowRenderTargets();
#endif

#if USE_GRSHADOWMAP || USE_GRCUBESHADOWMAP
	GRSHADOWCOLLECTOR->ShowShadowRenderTargets();
#endif
}

//-----------------------------------------------------------------------------
void rmcSampleSimpleWorld::ShutdownClient()
{
#if 0
	RMPTFXMGR.DestroyDownsampleBuffers();
#endif // __RMPTFX_DOWNSAMPLEBUFFERS
    // Scripting engine teardown
#if __BANK
    scrDebugger::KillAllDebuggers();
    scrDebugger::ShutdownClass();
#endif
    scrThread::KillAllThreads();
    scrThread::ShutdownClass();
    scrProgram::ShutdownClass();

#if __PFDRAW
	GetRageProfileDraw().Shutdown();
#endif //__PFDRAW

	//PostFX Shutdown
	if(!PARAM_nopostfx.Get())
		GRPOSTFX->DeleteRenderTargets();
	
	if(!PARAM_nopostfx.Get())
		GRPOSTFX->Terminate();

#if __XENON
	GRCGPRALLOCATION->Terminate();
#endif

	if (PARAM_audioon.Get())
	{
		pgStreamer::ShutdownClass();
		vwrAudioManager::ShutdownClass();
	}

	//Note : This is a touchy shutdown sequence (eg. it will cause a crash if its not done
	//in this order...
	//We have to reset the fragment manager, then shutdown the simpleworld entity manager 
	//which will delete the frag instances it is maintaining, then finally delete the 
	//frag world and shutdown the rest of the physics system.
	FRAGMGR->Reset();

	//Entity Manager Shutdown
	SHUTDOWN_SMPWORLDGTEXMGR;
	SHUTDOWN_SMPWORLDEMGR;
    SHUTDOWN_SMPWORLD;

    RvAnimMgr::ShutdownClass(); // FIXME: If you have any anims playing on an entity, you'll get a crash.

	//Shadow-map Shutdown
#if USE_GRSHADOWMAP
	RMCSAMPLESHADOWMAP->DeleteRenderTargets();
	RMCSAMPLESHADOWMAP->Terminate();
#endif //USE_GRSHADOWMAP

#if USE_GRCUBESHADOWMAP
	RMCCUBESAMPLESHADOWMAP->DeleteRenderTargets();
	RMCCUBESAMPLESHADOWMAP->Terminate();
#endif //USE_GRSHADOWMAP

#if USE_GRSHADOWMAP || USE_GRCUBESHADOWMAP
	GRSHADOWCOLLECTOR->DeleteRenderTargets();
	// 2D screenspace texture that collects the shadows
	GRSHADOWCOLLECTOR->Terminate();
#endif



	//Physics-Fragment Shutdown
	if(m_pFragWorld)
	{
		m_pFragWorld->Shutdown();
		delete m_pFragWorld;
	}

	phSimulator::ShutdownClass();
	phDemoWorld::ClassShutdown();

#if USE_RMPTFX
	//Particle Effects Shutdown
	rmPtfxManager::ShutdownClass();
#endif //USE_RMPTFX

	// Shutdown wind.
	phWind::ShutdownClass();
	delete m_pCycloneGroup;

	//General Shutdown
	SHUTDOWN_PARSER;

#if USETRANSPARENTCOMPOSITERENDERTARGET
    delete m_CompositeRenderTarget;
#endif // USETRANSPARENTCOMPOSITERENDERTARGET
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadEntity( const char* filePath )
{
	Assert(filePath);
	SetFullAssetPath(filePath);

	SimpleWorldDrawableEntity *pDrawableEntity = rage_new SimpleWorldDrawableEntity();
	if ( !pDrawableEntity->LoadEntityFile(filePath,true) )
	{
		delete pDrawableEntity;
		return false;
	}
	else
	{
		const char* szExprFile = NULL;
		if(PARAM_expr.Get(szExprFile))
		{
			pDrawableEntity->LoadAndInitialzeExpressions(szExprFile);
		}

        InsertEntity( pDrawableEntity, true );

        Printf( "Loaded entity '%s' Positioned at [%f,%f,%f]\n", (const char *)pDrawableEntity->GetEntityName(), 
            pDrawableEntity->GetTransform().d.x, pDrawableEntity->GetTransform().d.y, pDrawableEntity->GetTransform().d.z );

        m_pLastEntityLoaded = pDrawableEntity;
	}

	return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadEntity( const char* filePath, Matrix34 &transform )
{
    if ( LoadEntity(filePath) )
    {
        m_pLastEntityLoaded->SetTransform( transform );
        return true;
    }

    return false;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadFragEntity( const char* filePath )
{
	Assert(filePath);
	SetFullAssetPath(filePath);

	SimpleWorldFragmentEntity *pFragEntity = rage_new SimpleWorldFragmentEntity( PHLEVEL, m_pFragWorld->GetSimulator() );
	if ( !pFragEntity->LoadEntityFile(filePath) )
	{
		delete pFragEntity;
		return false;
	}

	pFragEntity->Reset();

	InsertEntity( pFragEntity );


	Printf( "Loaded frag entity '%s' Positioned at [%f,%f,%f]\n", (const char *)pFragEntity->GetEntityName(), 
			pFragEntity->GetTransform().d.x, pFragEntity->GetTransform().d.y, pFragEntity->GetTransform().d.z );

	m_pLastFragEntityLoaded = pFragEntity;

	return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadFragDrawableEntity( const char* filePath )
{
	Assert(filePath);
	SetFullAssetPath(filePath);

	SimpleWorldFragDrawableEntity *pFragDrawableEntity = rage_new SimpleWorldFragDrawableEntity( PHLEVEL, m_pFragWorld->GetSimulator() );
	if ( !pFragDrawableEntity->LoadEntityFile(filePath) )
	{
		delete pFragDrawableEntity;
		return false;
	}
	else
	{
		pFragDrawableEntity->Reset();

		InsertEntity( pFragDrawableEntity );

        Printf( "Loaded frag drawable entity '%s' Positioned at [%f,%f,%f]\n", (const char *)pFragDrawableEntity->GetEntityName(), 
            pFragDrawableEntity->GetTransform().d.x, pFragDrawableEntity->GetTransform().d.y, pFragDrawableEntity->GetTransform().d.z );

		m_pLastFragDrawableEntityLoaded = pFragDrawableEntity;
	}

	return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadParticleEffect( const char* filePath )
{
    return CreateParticleEmitter( filePath );
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadAnimation( const char* filePath )
{
    if( RvAnimMgr::LoadAndInsertAnimation(filePath))
		return true;
	else
		return false;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadClip( const char* filePath )
{
    if(RvAnimMgr::LoadAndInsertClip(filePath))
	{
        return true;
	}
    else
        return false;
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::UnloadEntity( const char* filePath )
{
    int count = SMPWORLDEMGR.GetEntityCount();
    for ( int i = 0; i < count; ++i )
    {
        SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( i );
        if ( stricmp( pEntity->GetSourceFilePath(), filePath ) == 0 )
        {
            RemoveEntity( pEntity );
            break;
        }
    }
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::UnloadAnimation( const char* filePath )
{
    RvAnimMgr::UnloadAndRemoveAnimation( filePath );
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::UnloadClip( const char* filePath )
{
    RvAnimMgr::UnloadAndRemoveClip( filePath );
}

//-----------------------------------------------------------------------------

const char* rmcSampleSimpleWorld::SaveEntityList( const char* filePath )
{
	Assert(filePath);

    parTree *pTree = rage_new parTree;
    parTreeNode *pRoot = pTree->CreateRoot();
    pRoot->GetElement().SetName( ENTITY_LIST );

	Printf("Saving Entity List : '%s'\n", filePath);

    if ( !SaveEntityListTreeNode(this,pRoot) )
    {
        return NULL;
    }
	
    char path[256];
    fiAssetManager::RemoveExtensionFromPath( path, 256, filePath );
    if ( !PARSER.SaveTree(path,"xml",pTree,parManager::XML) )
    {
        Errorf( "SaveEntityList PARSER error on %s\n", filePath );
        return NULL;
    }

    return filePath;
}
//-----------------------------------------------------------------------------
bool rmcSampleSimpleWorld::LoadEntityList(const char* filePath)
{
	Assert(filePath);

    Printf( "Loading entity List : '%s'\n", filePath );

    const char *pExt = fiAssetManager::FindExtensionInPath( filePath );
    if ( strcmp(pExt,".xml") == 0 )
    {
        parTree *pTree = PARSER.LoadTree( filePath, "xml" );
        if ( pTree )
        {
            parTreeNode *pRoot = pTree->GetRoot();
            if ( strcmp(pRoot->GetElement().GetName(),ENTITY_LIST) != 0 )
            {
                Errorf( "%s is not an %s xml file\n", filePath, ENTITY_LIST );
                return false;
            }

            return LoadEntityListTreeNode( this, pRoot );
        }

        Errorf( "LoadEntityList error: PARSER could not load %s\n", filePath );
        return false;
    }

    // backwards compatibility
	fiStream *pStream = ASSET.Open( filePath, ENTITY_LIST );
	if(!pStream)
	{
		Errorf("Failed to open entity list file : '%s'\n", filePath);
		return false;
	}

    int count = 0;
    SimpleWorldDrawableEntity *pDrawableEntity = rage_new SimpleWorldDrawableEntity();
    while ( pDrawableEntity->LoadEntityFromStream(pStream) )
    {
        InsertEntity( pDrawableEntity );

        Printf( "Loaded entity '%s' Positioned at [%f,%f,%f]\n", (const char *)pDrawableEntity->GetEntityName(), 
            pDrawableEntity->GetTransform().d.x, pDrawableEntity->GetTransform().d.y, pDrawableEntity->GetTransform().d.z );

        m_pLastEntityLoaded = pDrawableEntity;

        pDrawableEntity = rage_new SimpleWorldDrawableEntity();
        
        ++count;
    }

    delete pDrawableEntity;

    Printf( "\tLoaded %d entities\n", count );

	pStream->Close();
	return true;
}
//-----------------------------------------------------------------------------
const char* rmcSampleSimpleWorld::SaveFragmentList( const char* filePath )
{
    Assert(filePath);

    parTree *pTree = rage_new parTree;
    parTreeNode *pRoot = pTree->CreateRoot();
    pRoot->GetElement().SetName( FRAG_LIST );

    Printf("Saving Entity List : '%s'\n", filePath);

    if ( !SaveFragmentListTreeNode(this,pRoot) )
    {
        return NULL;
    }

    char path[256];
    fiAssetManager::RemoveExtensionFromPath( path, 256, filePath );
    if ( !PARSER.SaveTree(path,"xml",pTree,parManager::XML) )
    {
        Errorf( "SaveFragmentList PARSER error on %s\n", filePath );
        return NULL;
    }

    return filePath;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadFragmentList( const char* filePath )
{
	Assert(filePath);

    Printf( "Loading frag entity List : '%s'\n", filePath );

    const char *pExt = fiAssetManager::FindExtensionInPath( filePath );
    if ( strcmp(pExt,".xml") == 0 )
    {
        parTree *pTree = PARSER.LoadTree( filePath, "xml" );
        if ( pTree )
        {
            parTreeNode *pRoot = pTree->GetRoot();
            if ( strcmp(pRoot->GetElement().GetName(),FRAG_LIST) != 0 )
            {
                Errorf( "%s is not an %s xml file\n", FRAG_LIST, filePath );
                return false;
            }

            return LoadFragmentListTreeNode( this, pRoot );
        }

        Errorf( "LoadFragmentList error: PARSER could not load %s\n", filePath );
        return false;
    }

    // backwards compatibility
    fiStream *pStream = ASSET.Open( filePath, FRAG_LIST );
	if(!pStream)
	{
		Errorf("Failed to open fragment list file : '%s'\n", filePath);
		return false;
	}

    int count = 0;

    SimpleWorldFragmentEntity *pFragEntity = rage_new SimpleWorldFragmentEntity( PHLEVEL, m_pFragWorld->GetSimulator() );
    while ( pFragEntity->LoadEntityFromStream(pStream) )
    {
        pFragEntity->Reset();

        InsertEntity( pFragEntity );

        Printf( "Loaded frag entity '%s' Positioned at [%f,%f,%f]\n", (const char *)pFragEntity->GetEntityName(), 
            pFragEntity->GetTransform().d.x, pFragEntity->GetTransform().d.y, pFragEntity->GetTransform().d.z );

        pFragEntity = rage_new SimpleWorldFragmentEntity( PHLEVEL, m_pFragWorld->GetSimulator() );

        ++count;
    }

    delete pFragEntity;

    Printf( "\tLoaded %d frag entities\n", count );

    pStream->Close();
	return true;
}

//-----------------------------------------------------------------------------

const char* rmcSampleSimpleWorld::SaveFragmentDrawableList(const char* filePath)
{
    Assert(filePath);

    parTree *pTree = rage_new parTree;
    parTreeNode *pRoot = pTree->CreateRoot();
    pRoot->GetElement().SetName( FRAG_DRAW_LIST );

    Printf( "Saving frag drawable List : '%s'\n", filePath );

    if ( !SaveFragDrawableListTreeNode(this,pRoot) )
    {
        return NULL;
    }

    char path[256];
    fiAssetManager::RemoveExtensionFromPath( path, 256, filePath );
    if ( !PARSER.SaveTree(path,"xml",pTree,parManager::XML) )
    {
        Errorf( "SaveFragmentDrawableList PARSER error on %s\n", filePath );
        return NULL;
    }

    return filePath;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadFragmentDrawableList(const char* filePath)
{
    Assert(filePath);

    Printf( "Loading frag drawable entity List : '%s'\n", filePath );

    const char *pExt = fiAssetManager::FindExtensionInPath( filePath );
    if ( strcmp(pExt,".xml") != 0 )
    {
        Errorf( "LoadFragmentDrawableList error on %s: only .xml files are allowed\n", filePath );
        return false;
    }
    
    parTree *pTree = PARSER.LoadTree( filePath, "xml" );
    if ( pTree )
    {
        parTreeNode *pRoot = pTree->GetRoot();
        if ( strcmp(pRoot->GetElement().GetName(),FRAG_DRAW_LIST) != 0 )
        {
            Errorf( "%s is not an %s xml file\n", filePath, FRAG_LIST );
            return false;
        }

        return LoadFragDrawableEntityTreeNode( this, pRoot );
    }

    Errorf( "LoadFragmentList error: PARSER could not load %s\n", filePath );
    return false;
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::UpdateClampFPSSettingCallback( bool clamp, float fps, float maxFrameTime )
{
    m_pFragWorld->SetFramerate( fps );
    m_pFragWorld->SetMaximumFrameTime( maxFrameTime );
    m_pFragWorld->SetFixedFrame( clamp );
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::AnimMgrCurrentFrameChanged( RvAnimMgr* pMgr )
{
	float curFrame = pMgr->GetCurFrame();
	pMgr->UpdateClipEditorCurrentTime();
	int numEntities = SMPWORLDEMGR.GetEntityCount();
	for( int entityIdx = 0; entityIdx < numEntities; entityIdx++)
	{
		SimpleWorldDrawableEntity* pDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity*>(SMPWORLDEMGR.GetEntity(entityIdx));
		if(pDrawableEntity)
		{
			pDrawableEntity->GetAnimMgr()->SetCurFrame( curFrame );
		}
	}
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::AnimMgrFrameRangeChanged(RvAnimMgr* pMgr )
{
	pMgr->UpdateClipEditorTimeWindow();
	int numEntities = SMPWORLDEMGR.GetEntityCount();
	float startFrame = pMgr->GetPlaybackStartFrame();
	float endFrame = pMgr->GetPlaybackEndFrame();
	for( int entityIdx = 0; entityIdx < numEntities; entityIdx++)
	{
		SimpleWorldDrawableEntity* pDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity*>(SMPWORLDEMGR.GetEntity(entityIdx));
		if(pDrawableEntity)
		{
			pDrawableEntity->GetAnimMgr()->SetPlaybackStartFrame(startFrame);
			pDrawableEntity->GetAnimMgr()->SetPlaybackEndFrame(endFrame);
		}
	}
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::AnimMgrPlaybackModeChanged( RvAnimMgr* pMgr )
{
	RvAnimMgr::AnimPlayMode playMode = pMgr->GetPlayMode();
	int numEntities = SMPWORLDEMGR.GetEntityCount();
	for( int entityIdx = 0; entityIdx < numEntities; entityIdx++)
	{
		SimpleWorldDrawableEntity* pDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity*>(SMPWORLDEMGR.GetEntity(entityIdx));
		if(pDrawableEntity)
		{
			pDrawableEntity->GetAnimMgr()->SetPlayMode( playMode );
		}
	}
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::AnimMgrAnimIndexChanged( RvAnimMgr* pMgr )
{
	int animIndex = pMgr->GetCurrentAnimationIndex();
	int numEntities = SMPWORLDEMGR.GetEntityCount();
	for( int entityIdx = 0; entityIdx < numEntities; entityIdx++)
	{
		SimpleWorldDrawableEntity* pDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity*>(SMPWORLDEMGR.GetEntity(entityIdx));
		if(pDrawableEntity)
		{
			pDrawableEntity->GetAnimMgr()->SetCurrentAnimationIndex( animIndex );
		}
	}
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::AnimMgrClipIndexChanged( RvAnimMgr* pMgr )
{
	int clipIndex = pMgr->GetCurrentClipIndex();
	pMgr->UpdateClipEditorTimeWindow();
	int numEntities = SMPWORLDEMGR.GetEntityCount();
	for( int entityIdx = 0; entityIdx < numEntities; entityIdx++)
	{
		SimpleWorldDrawableEntity* pDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity*>(SMPWORLDEMGR.GetEntity(entityIdx));
		if(pDrawableEntity)
		{
			pDrawableEntity->GetAnimMgr()->SetCurrentClipIndex( clipIndex );
		}
	}
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::AnimMgrSequenceModeChanged( RvAnimMgr* pMgr )
{
	bool bSequence = pMgr->GetSequence();
	int numEntities = SMPWORLDEMGR.GetEntityCount();
	for( int entityIdx = 0; entityIdx < numEntities; entityIdx++)
	{
		SimpleWorldDrawableEntity* pDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity*>(SMPWORLDEMGR.GetEntity(entityIdx));
		if(pDrawableEntity)
		{
			pDrawableEntity->GetAnimMgr()->SetSequence( bSequence );
		}
	}
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::AnimMgrAnimListDoubleClickHandler( int key )
{
	m_globalAnimMgr.SetCurrentAnimationIndex( key );
	AnimMgrAnimIndexChanged( &m_globalAnimMgr );
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::AnimMgrClipListDoubleClickHandler( int key )
{
	m_globalAnimMgr.SetCurrentClipIndex(key);
	AnimMgrClipIndexChanged(&m_globalAnimMgr);
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::AnimMgrLoopAnimationChanged( RvAnimMgr* pMgr )
{
	bool bLoop = pMgr->GetLoopAnimation();
	int numEntities = SMPWORLDEMGR.GetEntityCount();
	for( int entityIdx = 0; entityIdx < numEntities; entityIdx++)
	{
		SimpleWorldDrawableEntity* pDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity*>(SMPWORLDEMGR.GetEntity(entityIdx));
		if(pDrawableEntity)
		{
			pDrawableEntity->GetAnimMgr()->SetLoopAnimation( bLoop );
		}
	}
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::AnimMgrResetMoverHandler( )
{
	int numEntities = SMPWORLDEMGR.GetEntityCount();
	for( int entityIdx = 0; entityIdx < numEntities; entityIdx++)
	{
		SimpleWorldDrawableEntity* pDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity*>(SMPWORLDEMGR.GetEntity(entityIdx));
		if(pDrawableEntity)
		{
			pDrawableEntity->ResetMover();
		}
	}
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::AnimMgrMoverModeChanged( RvAnimMgr* pMgr )
{
	RvAnimMgr::MoverPlayMode mode = pMgr->GetMoverPlayMode();

	int numEntities = SMPWORLDEMGR.GetEntityCount();
	for( int entityIdx = 0; entityIdx < numEntities; entityIdx++)
	{
		SimpleWorldDrawableEntity* pDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity*>(SMPWORLDEMGR.GetEntity(entityIdx));
		if(pDrawableEntity)
		{
			pDrawableEntity->GetAnimMgr()->SetMoverPlayMode( mode );
		}
	}
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::AnimMgrAllowMoverChanged( RvAnimMgr* pMgr )
{
	bool allowMover = pMgr->GetAllowMover();

	int numEntities = SMPWORLDEMGR.GetEntityCount();
	for( int entityIdx = 0; entityIdx < numEntities; entityIdx++)
	{
		SimpleWorldDrawableEntity* pDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity*>(SMPWORLDEMGR.GetEntity(entityIdx));
		if(pDrawableEntity)
		{
			pDrawableEntity->GetAnimMgr()->SetAllowMover( allowMover );
		}
	}
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::AnimMgrAllowRootTranslationChanged( RvAnimMgr* pMgr)
{
	bool allowRootTrans = pMgr->GetAllowRootTranslation();

	int numEntities = SMPWORLDEMGR.GetEntityCount();
	for( int entityIdx = 0; entityIdx < numEntities; entityIdx++)
	{
		SimpleWorldDrawableEntity* pDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity*>(SMPWORLDEMGR.GetEntity(entityIdx));
		if(pDrawableEntity)
		{
			pDrawableEntity->GetAnimMgr()->SetAllowRootTranslation( allowRootTrans );
		}
	}
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::AnimMgrMoverDebugDrawChanged( RvAnimMgr* pMgr )
{
	bool bMoverDebugDraw = pMgr->GetMoverDebugDraw();

	int numEntities = SMPWORLDEMGR.GetEntityCount();
	for( int entityIdx = 0; entityIdx < numEntities; entityIdx++)
	{
		SimpleWorldDrawableEntity* pDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity*>(SMPWORLDEMGR.GetEntity(entityIdx));
		if(pDrawableEntity)
		{
			pDrawableEntity->GetAnimMgr()->SetMoverDebugDraw( bMoverDebugDraw );
		}
	}
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::ClipEditorTimeChangedHandler( crTagEditorWidget* /*widget*/, float time )
{
	m_globalAnimMgr.SetPlayMode(RvAnimMgr::ANIM_PAUSE);
	AnimMgrPlaybackModeChanged(&m_globalAnimMgr);

	m_globalAnimMgr.SetCurFrame( time );
}

//-----------------------------------------------------------------------------

const char* rmcSampleSimpleWorld::SaveAnimationList(const char* filePath)
{
    Assert(filePath);

    parTree *pTree = rage_new parTree;
    parTreeNode *pRoot = pTree->CreateRoot();
    pRoot->GetElement().SetName( ANIM_LIST );

    Printf("Saving Entity List : '%s'\n", filePath);

    if ( !SaveAnimationListTreeNode(this,pRoot) )
    {
        return NULL;
    }

    char path[256];
    fiAssetManager::RemoveExtensionFromPath( path, 256, filePath );
    if ( !PARSER.SaveTree(path,"xml",pTree,parManager::XML) )
    {
        Errorf( "SaveAnimationList PARSER error on %s\n", filePath );
        return NULL;
    }

    return filePath;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadAnimationList(const char* filePath)
{
	Assert(filePath);
    
    Printf( "Loading anim List : '%s'\n", filePath );

    const char *pExt = fiAssetManager::FindExtensionInPath( filePath );
    if ( strcmp(pExt,".xml") == 0 )
    {
        parTree *pTree = PARSER.LoadTree( filePath, "xml" );
        if ( pTree )
        {
            parTreeNode *pRoot = pTree->GetRoot();
            if ( strcmp(pRoot->GetElement().GetName(),ANIM_LIST) != 0 )
            {
                Errorf( "%s is not an %s xml file\n", filePath, ANIM_LIST );
                return false;
            }

            return LoadAnimationListTreeNode( this, pRoot );
        }

        Errorf( "LoadAnimationList error: PARSER could not load %s\n", filePath );
        return false;
    }

    // backwards compatibility
    fiStream *pStream = ASSET.Open( filePath, ANIM_LIST );
	if(!pStream)
	{
		Errorf("Failed to open animation list file : '%s'\n", filePath);
		return false;
	}

	char animPath[256];
	while(fgetline(animPath,256,pStream))
	{
		LoadAnimation(animPath);
		Printf("Loaded Animation : '%s'\n", animPath);
	}
	pStream->Close();
	return true;
}

//-----------------------------------------------------------------------------

const char* rmcSampleSimpleWorld::SaveClipList(const char* filePath)
{
    Assert(filePath);

    parTree *pTree = rage_new parTree;
    parTreeNode *pRoot = pTree->CreateRoot();
    pRoot->GetElement().SetName( CLIP_LIST );

    Printf("Saving Entity List : '%s'\n", filePath);

    if ( !SaveClipListTreeNode(this,pRoot) )
    {
        return NULL;
    }

    char path[256];
    fiAssetManager::RemoveExtensionFromPath( path, 256, filePath );
    if ( !PARSER.SaveTree(path,"xml",pTree,parManager::XML) )
    {
        Errorf( "SaveClipList PARSER error on %s\n", filePath );
        return NULL;
    }

    return filePath;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadClipList(const char* filePath)
{
    Assert(filePath);

    Printf( "Loading anim List : '%s'\n", filePath );

    const char *pExt = fiAssetManager::FindExtensionInPath( filePath );
    if ( strcmp(pExt,".xml") == 0 )
    {
        parTree *pTree = PARSER.LoadTree( filePath, "xml" );
        if ( pTree )
        {
            parTreeNode *pRoot = pTree->GetRoot();
            if ( strcmp(pRoot->GetElement().GetName(),CLIP_LIST) != 0 )
            {
                Errorf( "%s is not an %s xml file\n", filePath, CLIP_LIST );
                return false;
            }

            return LoadClipListTreeNode( this, pRoot );
        }

        Errorf( "LoadClipList error: PARSER could not load %s\n", filePath );
        return false;
    }

    // backwards compatibility
    fiStream *pStream = ASSET.Open( filePath, CLIP_LIST );
    if(!pStream)
    {
        Errorf("Failed to open animation list file : '%s'\n", filePath);
        return false;
    }

    char animPath[256];
    while(fgetline(animPath,256,pStream))
    {
        LoadClip(animPath);
        Printf("Loaded Clip : '%s'\n", animPath);
    }
    pStream->Close();
    return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::SaveFileList( const char *pFilePath )
{
    atString filePath(pFilePath);
    const char* ext = fiAssetManager::FindExtensionInPath((const char*)filePath);
    if ( ext )
    {
        filePath.Truncate( filePath.GetLength() - strlen(ext) );
    }

    return FILELISTLOADER->SaveFile( filePath );
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadFileList( const char *pFilePath )
{
    return FILELISTLOADER->LoadFile( pFilePath );
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::SaveXmlFile( const char *pFilePath )
{
    atString filePath(pFilePath);
    const char* ext = fiAssetManager::FindExtensionInPath((const char*)filePath);
    if ( ext )
    {
        filePath.Truncate( filePath.GetLength() - strlen(ext) );
    }

    return FILELISTLOADER->SaveTree( filePath );
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadXmlFile( const char *pFilePath )
{
    return FILELISTLOADER->LoadTree( pFilePath );
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadScriptFile( const char *pFilePath )
{
    char scriptFile[256];
    fiAssetManager::RemoveExtensionFromPath( scriptFile, 256, pFilePath );    
    //const char *pName = fiAssetManager::FileName( scriptFile );

    scrThreadId id = scrThread::CreateThread( scriptFile );
    if ( id != 0 )
    {
#if __BANK
        scrDebugger::CreateDebugger( scriptFile, id );
#endif
		
		// NOTE: Normally, a game-team knows exactly which script contains the global variables and loads that first,
		// but for this viewer and subsequent samples, let's make it easy on the user.
		if ( ASSET.Exists( scriptFile, "sgv" ) )
		{
			scrThread::LoadGlobalVariableInfo( scriptFile );
		}
        return true;
    }
    
    return false;
}

//-----------------------------------------------------------------------------

rage::atString rmcSampleSimpleWorld::SaveLightsFile( void *pObj, const char *pFilePath )
{
    if ( ((rmcSampleSimpleWorld *)pObj)->SaveLightPreset(pFilePath) )
    {
        atString lightFileName(pFilePath);
        const char* ext = fiAssetManager::FindExtensionInPath((const char*)lightFileName);
        if ( !ext )
        {
            lightFileName += ".lgroup";
        }

        return lightFileName;
    }

	return atString("");
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadLightsFile( void *pObj, const char *pFilePath )
{
    rmcSampleSimpleWorld *pSmplWrld = (rmcSampleSimpleWorld *)pObj;
    
    // save in m_lightPreset. we'll use it later in InitLights()
    if ( pSmplWrld->LoadLightPreset(pFilePath,&pSmplWrld->m_lightPreset) )
    {
        if ( pSmplWrld->m_forceInitLights )
        {
            pSmplWrld->SetLightPreset( &pSmplWrld->m_lightPreset );
        }
        else
        {
            pSmplWrld->m_initLights = true;
        }
        return true;
    }

    return false;
}

//-----------------------------------------------------------------------------

rage::atString rmcSampleSimpleWorld::SaveCameraFile( void *pObj, const char *pFilePath )
{
    if ( ((rmcSampleSimpleWorld *)pObj)->SaveCameraPreset(pFilePath) )
    {
        atString cameraFileName(pFilePath);
        const char* ext = fiAssetManager::FindExtensionInPath((const char*)cameraFileName);
        if ( !ext )
        {
            cameraFileName += ".camset";
        }

        return cameraFileName;
    }

    return atString("");
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadCameraFile( void *pObj, const char *pFilePath )
{
    rmcSampleSimpleWorld *pSmplWrld = (rmcSampleSimpleWorld *)pObj;

    if ( pSmplWrld->LoadCameraPreset(pFilePath,&pSmplWrld->m_cameraPreset) )
    {
        if ( pSmplWrld->m_forceInitCamera )
        {
            pSmplWrld->SetCameraPreset( &pSmplWrld->m_cameraPreset );
        }
        else
        {
            pSmplWrld->m_initCamera = true;
        }
        return true;
    }

    return false;
}

//-----------------------------------------------------------------------------

#if USE_RMPTFX
bool rmcSampleSimpleWorld::CreateParticleEmitter(const char* filePath)
{
	Assert(filePath);
	
	char ruleName[80];
	fiAssetManager::RemoveExtensionFromPath(ruleName, 80, fiAssetManager::FileName(filePath));
	
    int memoryAvailableBefore = sysMemAllocator::GetCurrent().GetMemoryAvailable();

	ptxEffectRule* pRule = RMPTFXMGR.LoadEffectRule(ruleName);
	if(pRule)
	{
		ptxEffectInst *pEmitter = RMPTFXMGR.GetEffectInst(ruleName);
		if(pEmitter)
		{
			char entityName[80];
			sprintf(entityName,"[ptxEmitter] %s", ruleName);
			SimpleWorldRmptfxEmitter *pSmpWorldEmitter = rage_new SimpleWorldRmptfxEmitter(pEmitter);
			pSmpWorldEmitter->SetEntityName(entityName);

            char fullPath[RAGE_MAX_PATH];
            ASSET.FullPath( fullPath, sizeof(fullPath), filePath, NULL );

            pSmpWorldEmitter->SetSourceFilePath( fullPath );
            pSmpWorldEmitter->SetIdentifier( ruleName );

            int memoryAvailableAfter = sysMemAllocator::GetCurrent().GetMemoryAvailable();
            pSmpWorldEmitter->SetMemoryUsed( (memoryAvailableBefore - memoryAvailableAfter) / 1024 );
			
            InsertEntity( pSmpWorldEmitter, false );
			return true;
		}
	}
	return false;
}
#endif //USE_RMPTFX

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::SaveGlobalTexSearchPathTreeNode( void *pObj, rage::parTreeNode *pTreeNode )
{
    rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;
    const char *pStr = (const char *)pSmpSplWrld->m_GlobalTexSearchPath;

    if ( strlen(pStr) > 0 )
    {
        pTreeNode->GetElement().AddAttribute( "content", "ascii", false, false );
        pTreeNode->SetData( pStr, strlen(pStr) );
        return true;
    }

    return false;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadGlobalTexSearchPathTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{
    rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;

    const char *pStr = pTreeNode->GetData();
    if ( pStr )
    {
        pSmpSplWrld->m_GlobalTexSearchPath = pStr;
        return true;
    }

    return false;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::SaveGlobalTexListPathTreeNode( void *pObj, rage::parTreeNode *pTreeNode )
{
    rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;
    const char *pStr = (const char *)pSmpSplWrld->m_GlobalTexListPath;

    if ( strlen(pStr) > 0 )
    {
        pTreeNode->GetElement().AddAttribute( "content", "ascii", false, false );
        pTreeNode->SetData( pStr, strlen(pStr) );
        return true;
    }

    return false;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadGlobalTexListPathTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{
    rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;

    const char *pStr = pTreeNode->GetData();
    if ( pStr )
    {
        pSmpSplWrld->m_GlobalTexListPath = pStr;
        return true;
    }

    return false;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::SaveEntityListTreeNode( void *, rage::parTreeNode *pTreeNode )
{
    int numEntities = SMPWORLDEMGR.GetEntityCount();
    for (int i = 0; i < numEntities; i++)
    {
        SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( i );
        Assert(pEntity);
        SimpleWorldDrawableEntity *pDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity*>( pEntity );
        if ( !pDrawableEntity || !pDrawableEntity->IsExactlySimpleWorldDrawableEntity() )
        {
            continue;
        }

        pDrawableEntity->SaveEntityTreeNode( pTreeNode );
    }

    return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadEntityListTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{  
    // go through every child node of "elist"
    int count = 0;
    parTreeNode *pNode = const_cast<parTreeNode *>(pTreeNode)->GetChild();
    while ( pNode )
    {
        // load it
        if ( !rmcSampleSimpleWorld::LoadEntityTreeNode(pObj,pNode) )
        {
            return false;
        }

        pNode = pNode->GetSibling();

        ++count;
    }

    Printf( "\tLoaded %d entities\n", count );

    return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::SaveAnimationListTreeNode( void *, rage::parTreeNode *pTreeNode )
{
    int count = RvAnimMgr::GetAnimationCount();
    for (int i = 0; i < count; ++i)
    {
        crAnimation *pAnim = RvAnimMgr::GetAnimation( i );
		if(pAnim)
		{
			parTreeNode *pNode = parTreeNode::CreateStdLeaf( "anim", pAnim->GetName() );
			pNode->AppendAsChildOf( pTreeNode );
		}
    }

    return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadAnimationListTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{
    parTreeNode *pNode = pTreeNode->GetChild();
    while ( pNode )
    {
        if ( !rmcSampleSimpleWorld::LoadAnimationTreeNode(pObj,pNode) )
        {
            return false;
        }
        
        pNode = pNode->GetSibling();
    }

    return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::SaveClipListTreeNode( void *, rage::parTreeNode *pTreeNode )
{
    int count = RvAnimMgr::GetClipCount();
    for (int i = 0; i < count; ++i)
    {
        crClip *pClip = RvAnimMgr::GetClip( i );
        if(pClip)
        {
            parTreeNode *pNode = parTreeNode::CreateStdLeaf( "clip", pClip->GetName() );
            pNode->AppendAsChildOf( pTreeNode );
        }
    }

    return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadClipListTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{
    parTreeNode *pNode = pTreeNode->GetChild();
    while ( pNode )
    {
        if ( !rmcSampleSimpleWorld::LoadClipTreeNode(pObj,pNode) )
        {
            return false;
        }

        pNode = pNode->GetSibling();
    }

    return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::SaveFragmentListTreeNode( void *, rage::parTreeNode *pTreeNode )
{
    int	numEntities = SMPWORLDEMGR.GetEntityCount();
    for (int i = 0; i < numEntities; i++)
    {
        SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( i );
        Assert(pEntity);
        SimpleWorldFragmentEntity *pFragEntity = dynamic_cast<SimpleWorldFragmentEntity*>(pEntity);
        if ( !pFragEntity )
        {
            continue;
        }

        pFragEntity->SaveEntityTreeNode( pTreeNode );
    }

    return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadFragmentListTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{
    int count = 0;

    // go through every child node of "flist"
    parTreeNode *pNode = const_cast<parTreeNode *>(pTreeNode)->GetChild();
    while ( pNode )
    {
        // load it
        if ( !rmcSampleSimpleWorld::LoadFragEntityTreeNode(pObj,pNode) )
        {
            return false;
        }

        pNode = pNode->GetSibling();

        ++count;
    }

    Printf( "\tLoaded %d frag entities\n", count );

    return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadEntityTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{
    rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;

    SimpleWorldDrawableEntity *pDrawableEntity = rage_new SimpleWorldDrawableEntity();
    if ( !pDrawableEntity->LoadEntityTreeNode(pTreeNode) )
    {
        delete pDrawableEntity;
        return false;
    }

    pSmpSplWrld->InsertEntity( pDrawableEntity );

    Printf( "Loaded entity '%s' Positioned at [%f,%f,%f]\n", (const char *)pDrawableEntity->GetEntityName(), 
        pDrawableEntity->GetTransform().d.x, pDrawableEntity->GetTransform().d.y, pDrawableEntity->GetTransform().d.z );

    pSmpSplWrld->m_pLastEntityLoaded = pDrawableEntity;

    return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadFragEntityTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{
    rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;

    SimpleWorldFragmentEntity *pFragEntity = rage_new SimpleWorldFragmentEntity( PHLEVEL, pSmpSplWrld->m_pFragWorld->GetSimulator() );
    if ( !pFragEntity->LoadEntityTreeNode(pTreeNode) )
    {
        delete pFragEntity;
        return false;
    }

    pFragEntity->Reset();
    pSmpSplWrld->InsertEntity( pFragEntity );

    Printf( "Loaded frag entity '%s' Positioned at [%f,%f,%f]\n", (const char *)pFragEntity->GetEntityName(), 
        pFragEntity->GetTransform().d.x, pFragEntity->GetTransform().d.y, pFragEntity->GetTransform().d.z );

    return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadFragDrawableEntityTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{
    rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;

    SimpleWorldFragDrawableEntity *pFragDrawableEntity = rage_new SimpleWorldFragDrawableEntity( PHLEVEL, pSmpSplWrld->m_pFragWorld->GetSimulator() );
    if ( !pFragDrawableEntity->LoadEntityTreeNode(pTreeNode) )
    {
        delete pFragDrawableEntity;
        return false;
    }

    pFragDrawableEntity->Reset();
    pSmpSplWrld->InsertEntity( pFragDrawableEntity );

    Printf( "Loaded frag drawable entity '%s' Positioned at [%f,%f,%f]\n", (const char *)pFragDrawableEntity->GetEntityName(), 
        pFragDrawableEntity->GetTransform().d.x, pFragDrawableEntity->GetTransform().d.y, pFragDrawableEntity->GetTransform().d.z );

    return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadAnimationTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{
    rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;

    const char *pAnimName = pTreeNode->GetData();
    if ( pAnimName )
    {
        if ( pSmpSplWrld->LoadAnimation(pAnimName) )
        {
            Displayf( "Loaded animation %s", pAnimName );
            return true;
        }
    }

    return false;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadClipTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{
    rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;

    const char *pClipName = pTreeNode->GetData();
    if ( pClipName )
    {
        if ( pSmpSplWrld->LoadClip(pClipName) )
        {
            Displayf( "Loaded clip %s", pClipName );
            return true;
        }
    }

    return false;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::SaveLightsTreeNode( void *pObj, rage::parTreeNode *pTreeNode )
{
    rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;
    
    // Little hack here: SaveLightPreset() is going to set the name of the parTreeNode we pass in.
    parTreeNode *pNode = rage_new parTreeNode;
    pNode->AppendAsChildOf( pTreeNode );
    
    return pSmpSplWrld->SaveLightPreset(NULL,pNode);
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadLightsTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{
    rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;

	// Reset all the lights before we load then 
	grcLightGroup&	grp = pSmpSplWrld->m_lightPreset.m_LightGroup;
	for ( int i = 0; i < grcLightGroup::MAX_LIGHTS ;i++ )
	{
		grp.Reset( i);
	}

    // remember, pTreeNode is a wrapper around the real guts of our data
    if ( pSmpSplWrld->LoadLightPreset(const_cast<parTreeNode *>(pTreeNode->GetChild()),&pSmpSplWrld->m_lightPreset) )
    {
        if ( pSmpSplWrld->m_forceInitLights )
        {
            pSmpSplWrld->SetLightPreset( &pSmpSplWrld->m_lightPreset );
        }
        else
        {
            pSmpSplWrld->m_initLights = true;
        }
        return true;
    }
    
    return false;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::SaveCameraTreeNode( void *pObj, rage::parTreeNode *pTreeNode )
{
    rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;

    // Little hack here: SaveCameraPreset() is going to set the name of the parTreeNode we pass in.
    parTreeNode *pNode = rage_new parTreeNode;
    pNode->AppendAsChildOf( pTreeNode );

    return pSmpSplWrld->SaveCameraPreset(NULL,pNode);
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadCameraTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{
    rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;

    // remember, pTreeNode is a wrapper around the real guts of our data
    if ( pSmpSplWrld->LoadCameraPreset(const_cast<parTreeNode *>(pTreeNode->GetChild()),&pSmpSplWrld->m_cameraPreset) )
    {
        if ( pSmpSplWrld->m_forceInitCamera )
        {
            pSmpSplWrld->SetCameraPreset( &pSmpSplWrld->m_cameraPreset );
        }
        else
        {
            pSmpSplWrld->m_initCamera = true;
        }
        return true;
    }

    return false;
}

//-----------------------------------------------------------------------------

#if USE_RMPTFX

bool rmcSampleSimpleWorld::LoadParticleEmitterTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{
    rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;

    parAttribute *pAtt = const_cast<parTreeNode *>(pTreeNode)->GetElement().FindAttribute( "rule" );
    return pSmpSplWrld->CreateParticleEmitter( pAtt->GetStringValue() );
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadRmPtfxPreLoadTreeNode( void * /*pObj*/, const rage::parTreeNode *pTreeNode )
{
    //rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;

    int count = 0;
    parTreeNode *pNode = pTreeNode->GetChild();
    while ( pNode )
    {
        const char *pType = pNode->GetElement().GetName();
        if ( strcmp(pType,"emitter_rule") == 0 )
        {
            int num = 0;
            parAttribute *pAtt = pNode->GetElement().FindAttribute( "num" );
            if ( pAtt )
            {
                num = pAtt->GetIntValue();
            }

            if ( RMPTFXMGR.LoadEffectRule(pNode->GetData(),num) )
            {
                Printf( "Loaded effect rule %s\n", pNode->GetData() );
            }
            else
            {
                Printf( "LoadRmPtfxEmittersTreeNode error: %s failed to load\n", pNode->GetData() );
                return false;
            }
        }
        else if ( strcmp(pType,"ptx_rule") == 0 )
        {
            if ( RMPTFXMGR.LoadPtxRule(pNode->GetData()) )
            {
                Printf( "Loaded ptx rule %s\n", pNode->GetData() );
            }
            else
            {
                Printf( "LoadRmPtfxEmittersTreeNode error: %s failed to load\n", pNode->GetData() );
                return false;
            }
        }
        else if ( strcmp(pType,"fx_list") == 0 )
        {
            RMPTFXMGR.LoadFxList( pNode->GetData(), 0 );
            Printf( "Loaded fx list %s\n", pNode->GetData() );
        }
        else
        {
            Printf( "LoadRmPtfxEmittersTreeNode error: unknown rule type %s\n", pType );
            return false;
        }

        ++count;
        pNode = pNode->GetSibling();
    }

    Printf( "\tPreloaded %d emitter rules\n", count );

    return true;
}

#endif //USE_RMPTFX

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::SaveFragDrawableListTreeNode( void * /*pObj*/, rage::parTreeNode *pTreeNode )
{
    // rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;
    
    int count = SMPWORLDEMGR.GetEntityCount();
    for (int i = 0; i < count; ++i)
    {
        SimpleWorldFragDrawableEntity *pFragDrawableEntity = dynamic_cast<SimpleWorldFragDrawableEntity *>( SMPWORLDEMGR.GetEntity(i) );
        if ( pFragDrawableEntity == NULL )
        {
            continue;
        }

        pFragDrawableEntity->SaveEntityTreeNode( pTreeNode );
    }

    return true;
}

//-----------------------------------------------------------------------------

bool rmcSampleSimpleWorld::LoadFragDrawableListTreeNode( void *pObj, const rage::parTreeNode *pTreeNode )
{
    int count = 0;

    // rmcSampleSimpleWorld *pSmpSplWrld = (rmcSampleSimpleWorld *)pObj;

    parTreeNode *pEntityNode = pTreeNode->GetChild();
    while ( pEntityNode )
    {
        if ( !rmcSampleSimpleWorld::LoadFragDrawableEntityTreeNode(pObj,pEntityNode) )
        {
            return false;
        }

        pEntityNode = pEntityNode->GetSibling();

        ++count;
    }

    Printf( "\tLoaded %d frag drawable entities\n", count );

    return true;
}

//-----------------------------------------------------------------------------

#if __BANK

void rmcSampleSimpleWorld::BankLoadEntityList()
{
	char fullListPath[256];
	memset(fullListPath, 0, 256);

	if(BANKMGR.OpenFile(fullListPath, 256, "*."ENTITY_LIST";*.xml", false, "Entity List (*."ENTITY_LIST";*.xml)"))
	{
		LoadEntityList(fullListPath);
	}
}


void rmcSampleSimpleWorld::BankSaveEntityList()
{
	char fullListPath[256];
	memset(fullListPath, 0, 256);

	if(BANKMGR.OpenFile(fullListPath, 256, "*.xml", true, "Entity List (*.xml)"))
	{
		const char* ext = fiAssetManager::FindExtensionInPath(fullListPath);
		if(!ext)
		{
			strcat(fullListPath, ".xml");
		}
			
		SaveEntityList(fullListPath);
	}
}

void rmcSampleSimpleWorld::BankLoadEntity()
{
    char fullEntityPath[256];
    memset(fullEntityPath, 0, 256);

	std::string formats = "*.type;*.xml;*.";
	formats += g_sysPlatform;
	formats += "dr";

	std::string description = "Entity Files (" + formats + ")";

	if(BANKMGR.OpenFile(fullEntityPath, 256, formats.c_str(), false, description.c_str()))
    {
        LoadEntity(fullEntityPath);
    }
}

void rmcSampleSimpleWorld::BankSaveAnimationList()
{
    char fullListPath[256];
    memset(fullListPath, 0, 256);

    if(BANKMGR.OpenFile(fullListPath, 256, "*.xml", true, "Animation List (*.xml)"))
    {
        const char* ext = fiAssetManager::FindExtensionInPath(fullListPath);
        if ( !ext )
        {
            strcat( fullListPath, ".xml" );
        }
        SaveAnimationList(fullListPath);
    }
}

void rmcSampleSimpleWorld::BankLoadAnimationList()
{	
    char fullListPath[256];
    memset(fullListPath, 0, 256);

    if(BANKMGR.OpenFile(fullListPath, 256, "*."ANIM_LIST";*.xml", false, "Animation List (*."ANIM_LIST";*.xml)"))
    {
        LoadAnimationList(fullListPath);
    }
}

void rmcSampleSimpleWorld::BankLoadAnimation()
{
    char fullAnimPath[256];
    memset(fullAnimPath, 0, 256);

    if(BANKMGR.OpenFile(fullAnimPath, 256, "*.anim;*.xml", false, "Animation Files (*.anim;*.xml)"))
    {
        LoadAnimation(fullAnimPath);
    }
}

void rmcSampleSimpleWorld::BankSaveAllClips()
{
	RvAnimMgr::SaveAllClips();
}

void rmcSampleSimpleWorld::BankSaveClipList()
{
    char fullListPath[256];
    memset(fullListPath, 0, 256);

    if(BANKMGR.OpenFile(fullListPath, 256, "*.xml", true, "Clip List (*.xml)"))
    {
        const char* ext = fiAssetManager::FindExtensionInPath(fullListPath);
        if ( !ext )
        {
            strcat( fullListPath, ".xml" );
        }
        SaveClipList(fullListPath);
    }
}

void rmcSampleSimpleWorld::BankLoadClipList()
{	
    char fullListPath[256];
    memset(fullListPath, 0, 256);

    if(BANKMGR.OpenFile(fullListPath, 256, "*."ANIM_LIST";*.xml", false, "Clip List (*."ANIM_LIST";*.xml)"))
    {
        LoadClipList(fullListPath);
    }
}

void rmcSampleSimpleWorld::BankLoadClip()
{
    char fullAnimPath[256];
    memset(fullAnimPath, 0, 256);

    if(BANKMGR.OpenFile(fullAnimPath, 256, "*.clip;*.xml", false, "Clip Files (*.clip;*.xml)"))
    {
        LoadClip(fullAnimPath);
    }
}

void rmcSampleSimpleWorld::BankSaveFragDrawableList()
{
    char fullListPath[256];
    memset(fullListPath, 0, 256);

    if(BANKMGR.OpenFile(fullListPath, 256, "*.xml", true, "Fragment Drawable Entity List (*.xml)"))
    {
        const char* ext = fiAssetManager::FindExtensionInPath(fullListPath);
        if(!ext)
        {
            strcat(fullListPath, ".xml");
        }

        SaveFragmentDrawableList(fullListPath);
    }
}

void rmcSampleSimpleWorld::BankLoadFragDrawableList()
{
    char fullListPath[256];
    memset(fullListPath, 0, 256);

    if(BANKMGR.OpenFile(fullListPath, 256, "*.xml", false, "Fragment Drawable Entity List (*.xml)"))
    {
        LoadFragmentDrawableList(fullListPath);
    }
}

void rmcSampleSimpleWorld::BankLoadFragDrawable()
{
	char fullEntityPath[256];
	memset(fullEntityPath, 0, 256);

	if (BANKMGR.OpenFile(fullEntityPath, 256, "*.type;*.xml", false, "Entity Files (*.type;*.xml)"))
	{
		LoadFragDrawableEntity(fullEntityPath);
	}
}

void rmcSampleSimpleWorld::BankLoadFragmentList()
{
	char fullListPath[256];
	memset(fullListPath, 0, 256);

	if(BANKMGR.OpenFile(fullListPath, 256, "*."FRAG_LIST";*.xml", false, "Fragment List (*."FRAG_LIST";*.xml)"))
	{
		LoadFragmentList(fullListPath);
	}
}

void rmcSampleSimpleWorld::BankSaveFragmentList()
{
	char fullListPath[256];
	memset(fullListPath, 0, 256);

	if(BANKMGR.OpenFile(fullListPath, 256, "*.xml", true, "Fragment List (*.xml)"))
	{
		const char* ext = fiAssetManager::FindExtensionInPath(fullListPath);
		if(!ext)
		{
			strcat(fullListPath, ".xml");
		}
		SaveFragmentList(fullListPath);
	}
}

void rmcSampleSimpleWorld::BankLoadFragmentEntity()
{
	char fullEntityPath[256];
	memset(fullEntityPath, 0, 256);

	if(BANKMGR.OpenFile(fullEntityPath, 256, "*.type;*.xml", false, "Entity Files (*.type;*.xml)"))
	{
		LoadFragEntity(fullEntityPath);
	}
}

#if USE_RMPTFX
void rmcSampleSimpleWorld::BankCreateParticleEmitter()
{
	char fullRulePath[256];
	memset(fullRulePath, 0, 256);

	if(BANKMGR.OpenFile(fullRulePath, 256, "*.effectrule", false, "Effect Rule Files (*.effectrule)"))
	{
		CreateParticleEmitter(fullRulePath);
	}
}
#endif //USE_RMPTFX

void rmcSampleSimpleWorld::BankSaveFileList()
{
    // still here for compatibility, but otherwise, this function is no longer used
    /*
    char fullListPath[256];
    memset( fullListPath, 0, 256 );

    if ( BANKMGR.OpenFile(fullListPath, 256, "*."FILE_LIST, true, "File List (*."FILE_LIST")") )
    {
        const char* ext = fiAssetManager::FindExtensionInPath( fullListPath );
        if( !ext )
        {
            strcat( fullListPath, "."FILE_LIST );
        }
        SaveFileList( fullListPath );
    }
    */
}

void rmcSampleSimpleWorld::BankLoadFileList()
{
    char fullListPath[256];
    memset( fullListPath, 0, 256 );

    if ( BANKMGR.OpenFile(fullListPath, 256, "*."FILE_LIST, true, "File List (*."FILE_LIST")") )
    {
        LoadFileList( fullListPath );
    }
}

void rmcSampleSimpleWorld::BankSaveFileXml()
{
    char fullListPath[256];
    memset( fullListPath, 0, 256 );

    if ( BANKMGR.OpenFile(fullListPath, 256, "*.xml", true, "File List (*.xml)") )
    {
        const char* ext = fiAssetManager::FindExtensionInPath( fullListPath );
        if( !ext )
        {
            strcat( fullListPath, ".xml" );
        }
        SaveXmlFile( fullListPath );
    }
}

void rmcSampleSimpleWorld::BankLoadFileXml()
{
    char fullListPath[256];
    memset( fullListPath, 0, 256 );

    if ( BANKMGR.OpenFile(fullListPath, 256, "*.xml", false, "File List Files (*.xml)") )
    {
        LoadXmlFile( fullListPath );
    }
}

void rmcSampleSimpleWorld::RefreshSimpleWorld()
{
	//TODO : Implement refresh
}

void rmcSampleSimpleWorld::RenderModeChanged()
{
	//0 = "Textured / Shaded"
	//1 = "Wireframe" 
	//2 = "Wireframe / Skeleton"
	//3 = "Skeleton Only"

	int numEntities = SMPWORLDEMGR.GetEntityCount();
	for( int entityIdx = 0; entityIdx < numEntities; entityIdx++)
	{
		SimpleWorldDrawableEntity* pDrawableEntity = dynamic_cast<SimpleWorldDrawableEntity*>(SMPWORLDEMGR.GetEntity(entityIdx));
		if(pDrawableEntity)
		{
			rmcDrawable* pDrawable = pDrawableEntity->GetDrawable();
			grmShaderGroup& shaderGroup = pDrawable->GetShaderGroup();

			int nShaders = shaderGroup.GetCount();
			for(int shaderIdx = 0; shaderIdx < nShaders; shaderIdx++)
			{
				grmShader* pShader = shaderGroup.GetShaderPtr(shaderIdx);
				
				u8 flags = (u8)pShader->GetInstanceData().UserFlags;

				if(m_viewportRenderMode == 0) //Textured and shaded
				{
					flags &= ~grmShader::FLAG_DRAW_WIREFRAME;
					flags &= ~grmShader::FLAG_DISABLE_INSTANCES;
				}
				else if(m_viewportRenderMode == 1 || m_viewportRenderMode == 2) // Wireframe or Wireframe/Skeleton
				{
					flags |= grmShader::FLAG_DRAW_WIREFRAME;
					flags &= ~grmShader::FLAG_DISABLE_INSTANCES;
				} 
				else if(m_viewportRenderMode == 3) //Skeleton only
				{
					flags |= grmShader::FLAG_DISABLE_INSTANCES;
				}
				pShader->GetInstanceData().UserFlags = flags;
			}
		}
	}
}

void rmcSampleSimpleWorld::AddWidgetsClient()
{
	ragesamples::rmcSampleManager::AddWidgetsClient();

	phMouseInput::AddClassWidgets(BANKMGR.CreateBank("rage - phMouseInput"));

    bkBank *pPhysWorldBank = &BANKMGR.CreateBank("rage - Frag World");
    phDemoWorld::AddClassWidgets( *pPhysWorldBank );
    m_pFragWorld->AddWidgets( *pPhysWorldBank );

	bkBank& windBank = BANKMGR.CreateBank("rage - Wind");
	WIND.AddWidgets(windBank);

#if USE_RMPTFX && RMPTFX_BANK
	// add the rmptfx widgets
	RMPTFXMGR.AddWidgets();
#endif //USE_RMPTFX

	// add post-processing pipeline widgets
	if(!PARAM_nopostfx.Get())
	{
		diagContextMessage	mess("Adding PostFx Widgets");
		bkBank &bk = BANKMGR.CreateBank("rage - PostFX");
		GRPOSTFX->AddWidgets(bk);
	}

#if USE_GRSHADOWMAP
	{
		diagContextMessage	mess("Adding Cascaded Shadow Map Widgets");
		// add shadowmap widgets
		bkBank &bk2 = BANKMGR.CreateBank("rage - Cascaded Shadow Map");
		RMCSAMPLESHADOWMAP->AddSaveWidgets(bk2);
		RMCSAMPLESHADOWMAP->AddWidgets(bk2);
	}
#endif //USE_GRSHADOWMAP

#if USE_GRCUBESHADOWMAP
	// add shadowmap widgets
	bkBank &bk3 = BANKMGR.CreateBank("rage - Cubic Shadow Map");
	RMCCUBESAMPLESHADOWMAP->AddSaveWidgets(bk3);
	RMCCUBESAMPLESHADOWMAP->AddWidgets(bk3);
#endif //USE_GRSHADOWMAP

	m_pGlobalAnimBank = &(BANKMGR.CreateBank("rage - GlobalAnimation"));
	Assert(m_pGlobalAnimBank);

	m_globalAnimMgr.InitWidgets( *m_pGlobalAnimBank );
	m_globalAnimMgr.AddWidgets( *m_pGlobalAnimBank );
	m_globalAnimMgr.SetCurrentFrameChangedFtor( MakeFunctor(*this,&rmcSampleSimpleWorld::AnimMgrCurrentFrameChanged) );
	m_globalAnimMgr.SetFrameRangeChangedFtor( MakeFunctor(*this,&rmcSampleSimpleWorld::AnimMgrFrameRangeChanged) );
	m_globalAnimMgr.SetPlaybackModeChangedFtor( MakeFunctor(*this,&rmcSampleSimpleWorld::AnimMgrPlaybackModeChanged) );
	m_globalAnimMgr.SetAnimIndexChangedFtor( MakeFunctor(*this,&rmcSampleSimpleWorld::AnimMgrAnimIndexChanged) );
	m_globalAnimMgr.SetClipIndexChangedFtor( MakeFunctor(*this,&rmcSampleSimpleWorld::AnimMgrClipIndexChanged) );
	m_globalAnimMgr.SetSequenceModeChangedFtor( MakeFunctor(*this,&rmcSampleSimpleWorld::AnimMgrSequenceModeChanged) );
	m_globalAnimMgr.SetAnimSelectedFtor( MakeFunctor(*this,&rmcSampleSimpleWorld::AnimMgrAnimListDoubleClickHandler) );
	m_globalAnimMgr.SetClipSelectedFtor( MakeFunctor(*this,&rmcSampleSimpleWorld::AnimMgrClipListDoubleClickHandler) );

	m_globalAnimMgr.SetLoopAnimationChangedFtor( MakeFunctor(*this,&rmcSampleSimpleWorld::AnimMgrLoopAnimationChanged) );

	m_globalAnimMgr.SetResetMoverFtor( MakeFunctor(*this,&rmcSampleSimpleWorld::AnimMgrResetMoverHandler) );
	m_globalAnimMgr.SetMoverModeChangedFtor( MakeFunctor(*this,&rmcSampleSimpleWorld::AnimMgrMoverModeChanged) );
	m_globalAnimMgr.SetAllowMoverChangedFtor( MakeFunctor(*this,&rmcSampleSimpleWorld::AnimMgrAllowMoverChanged) );
	m_globalAnimMgr.SetAllowRootTranslationChangedFtor( MakeFunctor(*this,&rmcSampleSimpleWorld::AnimMgrAllowRootTranslationChanged) );
	m_globalAnimMgr.SetMoverDebugDrawChangedFtor( MakeFunctor(*this,&rmcSampleSimpleWorld::AnimMgrMoverDebugDrawChanged) );

	// add the simple world widgest
	m_pSampleBank = &(BANKMGR.CreateBank("rage - SampleSimpleWorld"));
	Assert(m_pSampleBank);

#if USE_RMPTFX
    m_pSampleBank->AddButton("Create Particle Effect", datCallback(MFA(rmcSampleSimpleWorld::BankCreateParticleEmitter), this));
#endif //USE_RMPTFX

    m_pSampleBank->PushGroup( "Entity" );
    {
        m_pSampleBank->AddButton("Load Entity", datCallback(MFA(rmcSampleSimpleWorld::BankLoadEntity),this));
	    m_pSampleBank->AddButton("Load Entity List", datCallback(MFA(rmcSampleSimpleWorld::BankLoadEntityList),this));
	    m_pSampleBank->AddButton("Save Entity List", datCallback(MFA(rmcSampleSimpleWorld::BankSaveEntityList), this));
    }
    m_pSampleBank->PopGroup();
	
    m_pSampleBank->PushGroup( "Fragment" );
    {
        m_pSampleBank->AddButton("Load Fragment", datCallback(MFA(rmcSampleSimpleWorld::BankLoadFragmentEntity),this));
	    m_pSampleBank->AddButton("Load Fragment List", datCallback(MFA(rmcSampleSimpleWorld::BankLoadFragmentList), this));
	    m_pSampleBank->AddButton("Save Fragment List", datCallback(MFA(rmcSampleSimpleWorld::BankSaveFragmentList), this));
    }
    m_pSampleBank->PopGroup();

    m_pSampleBank->PushGroup( "Fragment Drawable" );
    {
        m_pSampleBank->AddButton("Load Fragment Drawable", datCallback(MFA(rmcSampleSimpleWorld::BankLoadFragDrawable),this));
        m_pSampleBank->AddButton("Load Fragment Drawable List", datCallback(MFA(rmcSampleSimpleWorld::BankLoadFragDrawableList), this));
        m_pSampleBank->AddButton("Save Fragment Drawable List", datCallback(MFA(rmcSampleSimpleWorld::BankSaveFragDrawableList), this));
    }
    m_pSampleBank->PopGroup();

    m_pSampleBank->PushGroup( "Animation" );
    {
	    m_pSampleBank->AddButton("Load Animation", datCallback(MFA(rmcSampleSimpleWorld::BankLoadAnimation),this));
	    m_pSampleBank->AddButton("Load Animation List", datCallback(MFA(rmcSampleSimpleWorld::BankLoadAnimationList), this));
	    m_pSampleBank->AddButton("Save Animation List", datCallback(MFA(rmcSampleSimpleWorld::BankSaveAnimationList), this));
        m_pSampleBank->AddButton("Load Clip", datCallback(MFA(rmcSampleSimpleWorld::BankLoadClip),this));
        m_pSampleBank->AddButton("Load Clip List", datCallback(MFA(rmcSampleSimpleWorld::BankLoadClipList), this));
        m_pSampleBank->AddButton("Save Clip List", datCallback(MFA(rmcSampleSimpleWorld::BankSaveClipList), this));
		m_pSampleBank->AddButton("Save All Clips", datCallback(MFA(rmcSampleSimpleWorld::BankSaveAllClips), this));
    }
    m_pSampleBank->PopGroup();
	
    m_pSampleBank->PushGroup( "All" );
    {
        m_pSampleBank->AddButton( "Reset Entities", datCallback(MFA(rmcSampleSimpleWorld::ResetSimpleWorld),this));
        
        m_pSampleBank->AddButton( "Load File List", datCallback(MFA(rmcSampleSimpleWorld::BankLoadFileList),this), "This is being deprecated in favor of xml." );

        m_pSampleBank->AddButton( "Load File Xml", datCallback(MFA(rmcSampleSimpleWorld::BankLoadFileXml),this) );
        m_pSampleBank->AddButton( "Save File Xml", datCallback(MFA(rmcSampleSimpleWorld::BankSaveFileXml),this) );
    }
    m_pSampleBank->PopGroup();

	//if(PARAM_modeldebugger.Get())
#if !__PS3
	rmcModelDebugger::GetInstance().AddWidgets( *m_pSampleBank );
#endif

    SMPWORLDEMGR.InitBank();

	//Update the Camera Bank with the option to turn off the fragment HUD
	static const char* renderModeList[] = {"Textured / Shaded", "Wireframe", "Wireframe / Skeleton", "Skeleton Only"};
	
	bkBank *pCameraBank = BANKMGR.FindBank("rage - Camera");
	
	if(pCameraBank)
	{
		pCameraBank->AddToggle("Show Fragment HUD",&m_bShowFragmentHUD); 
		pCameraBank->AddToggle("Show Animation HUD",&m_bShowAnimationHUD);

		pCameraBank->PushGroup("Rendering Mode");
		pCameraBank->AddCombo("Mode", &m_viewportRenderMode, 4, renderModeList, 0, datCallback(MFA(rmcSampleSimpleWorld::RenderModeChanged), this) );
		pCameraBank->PopGroup();
	}
}


#endif //__BANK

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::ResetSimpleWorld()
{
    SMPWORLDEMGR.Reset();
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::InsertEntity( SimpleWorldEntity *pEntity, bool bLoadTextures )
{
    SMPWORLDEMGR.InsertEntity( pEntity );
	OnLoadEntity( pEntity );

    // Load the global texture list textures for this entity
    if ( bLoadTextures )
    {
        if ( m_GlobalTexSearchPath.GetLength() )
        {
            SMPWORLDGTEXMGR.LoadGlobalList( (const char *)m_GlobalTexSearchPath );
        }
        else
        {
            char basePath[256];
            fiAssetManager::RemoveNameFromPath( basePath, 256, (const char *)pEntity->GetSourceFilePath() );
            SMPWORLDGTEXMGR.LoadGlobalList( basePath );
        }
    }
}

//-----------------------------------------------------------------------------

void rmcSampleSimpleWorld::RemoveEntity( SimpleWorldEntity *pEntity )
{
    SMPWORLDEMGR.RemoveEntity( pEntity );
}

//-----------------------------------------------------------------------------

void SimpleWorldGlobalTextureMgr::AddTextureToGlobalList(const char* shaderRefName, const char* fileName)
{
	Assert(shaderRefName);
	Assert(fileName);

	//Does an entry already exist for this name?
	SimpleWorldGlobalTexture* pGlobalTexLookp = m_TextureTable.Access(shaderRefName);
	if(!pGlobalTexLookp)
	{
		SimpleWorldGlobalTexture &globalTex = m_TextureTable[atString(shaderRefName)];
		globalTex.m_FileName = fileName;
		globalTex.m_pTexture = NULL;
		globalTex.m_pTextureReference = rage_new grcTextureReference(fileName, const_cast<grcTexture*>(grcTexture::None));
		grcTextureFactory::GetInstance().RegisterTextureReference(shaderRefName, globalTex.m_pTextureReference);
	}
	else
	{
		Warningf("Duplicate global texture list entry found for entry '%s', referring to file '%s', skipping duplicate entry",
			shaderRefName, fileName);
	}
}

//-----------------------------------------------------------------------------

bool SimpleWorldGlobalTextureMgr::LoadGlobalList(const char* searchPath)
{
	bool bPopPath = false;

	//Do we need to look somplace else besides the folder we are loading the asset from to find the 
	//global textures?
	if(searchPath)
	{
		ASSET.PushFolder(searchPath);
		bPopPath = true;
	}

	//Iterate through the global texture list, and load the textures
	atMap<atString, SimpleWorldGlobalTexture>::Iterator it = m_TextureTable.CreateIterator();
	while(!it.AtEnd())
	{
		SimpleWorldGlobalTexture& pGlbTexture = it.GetData();
		if(pGlbTexture.m_pTexture)
		{
			Displayf("Global texture reference '%s' has already been loaded.", (const char*)it.GetKey());
		}
		else
		{
			//Load the texture
			grcTexture* pTexture = grcTextureFactory::GetInstance().Create(pGlbTexture.m_FileName);
			
			//Set the texture, and the texture reference
			pGlbTexture.m_pTexture = pTexture;
			pGlbTexture.m_pTextureReference->SetReference(pGlbTexture.m_pTexture);
			Displayf("Loaded global texture '%s', set as referent for '%s'", (const char*)(pGlbTexture.m_FileName), (const char*)it.GetKey());
		}
		++it;
	}

	if(bPopPath)
		ASSET.PopFolder();

	return true;
}





#if !__FINAL

rmcSampleSimpleWorld*		g_SampleSimpleWorld = 0;

// -------------------------- console interface   ---------------------
void LoadEntityCommand( const char** args,int numArgs,bkConsole::OutputFunctor output  )
{
	CONSOLE_HELP_TEXT("Loads an Entity file, either resourced or a type file");

	if (numArgs < 2)
	{
		output("Need A File Name and an Identifier Name");
		output("ERROR 1 : IncorrectArguments");
		output("$false");
		return;
	}
	int count = SMPWORLDEMGR.GetEntityCount();
	if ( g_SampleSimpleWorld->LoadEntity(args[0]) )
	{
		SimpleWorldEntity *pEntity = const_cast<SimpleWorldEntity *>( SMPWORLDEMGR.GetEntity(count) );
		pEntity->SetEntityName( args[1] );
		pEntity->SetIdentifier( args[1] );
		output("$true");
		return;
	}
	output("$false");
	return;
}
void RemoveEntityCommand( const char** args,int numArgs,bkConsole::OutputFunctor output  )
{
	CONSOLE_HELP_TEXT("Removes a named entity");

	if (numArgs < 1)
	{
		output("Need an identifier");
		output("ERROR 1 : IncorrectArguments");
		output("$false");
		return;
	}
	bool res = true;
	if ( !stricmp( args[0], "all") )
	{
		int count = SMPWORLDEMGR.GetEntityCount();
		while ( count )
		{
			SMPWORLDEMGR.RemoveEntity( 0 );
			count = SMPWORLDEMGR.GetEntityCount();
		}
	}
	else
	{
		res =SMPWORLDEMGR.RemoveEntity( args[0] );
	}
	output(  res ? "$true" : "$false" );
	return;
}


Vector3 StringToVector3( const char** args,int numArgs , bkConsole::OutputFunctor output  )
{
	if ( numArgs < 3 ) 
	{ 
		output("Error 0: Not Enough args for vector value"); 
		return Vector3( 0.0f, 0.0f, 0.0f);
	}
	return Vector3(  (float)atof( args[0]), (float)atof( args[1]), (float)atof( args[2]) );
}

// russ's xml stuff could come in handy here
void SetEntityState(  SimpleWorldEntity *pEntity , const char* tag, const char** args,int numArgs, bkConsole::OutputFunctor output  )
{
	Assert( pEntity );
	if ( !strcmp( tag, "position" ) )
	{
		Matrix34 mtx = pEntity->GetTransform();
		mtx.d = StringToVector3( args, numArgs, output );
		pEntity->SetTransform( mtx );
	}
	else if ( !strcmp( tag, "name" ) )
	{
		if ( numArgs == 0 ) { output("Error 0: Not Enough args for name"); }
		pEntity->SetEntityName( args[0] );
		pEntity->SetIdentifier( args[0] );
	}
	else if ( !strcmp( tag, "rotation" ) )
	{
		Matrix34 mtx = pEntity->GetTransform();
		mtx.FromEulersXYZ( StringToVector3( args, numArgs, output ) );
		pEntity->SetTransform( mtx );
	}
	else if ( !strcmp( tag, "hidden" ) )
	{
		if ( numArgs == 0 ) { output("Error 0: Not Enough args for boolean value"); }
		pEntity->SetHidden( args[0][0]=='t' );
	}
	else
	{
		output("Error 0: Tag not recognised"); 
	}
}
void SetEntityCommand( const char** args,int numArgs,bkConsole::OutputFunctor output )
{
	CONSOLE_HELP_TEXT("Returns information about the entity in xml form");

	if (numArgs < 2)
	{
		output("Error 0: Need an identifier and a state value to set\n");
		output("ERROR 1 : IncorrectArguments\n");
		return;
	}
	if ( !stricmp( args[0], "all") )
	{
		// output all entities
		int count = SMPWORLDEMGR.GetEntityCount();
		for ( int i = 0; i < count; i++ )
		{
			SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( i );
			SetEntityState( pEntity, args[1], &args[2], numArgs - 2, output );
		}
	}
	else
	{
		SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( args[0] );
		if ( pEntity) 
		{
			SetEntityState( pEntity, args[1], &args[2], numArgs - 2 , output );
		}
	}
	output( "$true" );
	return;
}
void GetEntityState( XmlLog& log,  const SimpleWorldEntity *pEntity )
{
	Assert( pEntity );
	log.GroupStart("Entity");
		log.Write("Name", pEntity->GetEntityName() );
		log.Write("Identifier", pEntity->GetIdentifier() );
		log.Write("Position", pEntity->GetTransform().d );
		log.Write("Rotation", pEntity->GetTransform().GetEulers());
		log.Write("SourceFilePath",pEntity->GetSourceFilePath() );
	log.GroupEnd("Entity");
}
void GetEntityCommand( const char** args,int numArgs,bkConsole::OutputFunctor output )
{
	CONSOLE_HELP_TEXT("Returns information about the entity in xml form");

	if (numArgs < 1)
	{
		output("Error 0: Need an identifier\n");
		output("ERROR 1 : IncorrectArguments\n");
		return;
	}
	XmlStringLog log;
	log.GroupStart("Entities");
	if ( !stricmp( args[0], "all") )
	{
		// output all entities
		int count = SMPWORLDEMGR.GetEntityCount();
		log.Write("Count", count );
		for ( int i = 0; i < count; i++ )
		{
			const SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( i );
			GetEntityState( log, pEntity );
		}
	}
	else
	{
		
		const SimpleWorldEntity *pEntity = SMPWORLDEMGR.GetEntity( args[0] );
		if ( pEntity) 
		{
			log.Write("Count", 1 );
			GetEntityState( log, pEntity );
		}
		else
		{
			output( "Error 0: Entity name %s is incorrect");
		}
		
	}
	log.GroupEnd("Entities");

	output( log.ToString() );

	return;
}

void TakeSnapSnotCommand( const char** args,int numArgs,bkConsole::OutputFunctor output )
{
	CONSOLE_HELP_TEXT("Takes Performance Readings and Screen shot of current game");

	if (numArgs < 1)
	{
		output("Need screen shot path \n");
		return;
	}
	XmlStringLog log;
	g_SampleSimpleWorld->TakeSnapShot( log , args[0] );
	output( log.ToString() );
}

void AddConsoleCommands()
{
	AssertMsg( g_SampleSimpleWorld != 0 , "SimpleWorld is not set" );
	bkConsole::CommandFunctor functor;
	functor.Reset<&LoadEntityCommand>();
	bkConsole::AddCommand("load-entity",functor);

	functor.Reset<&RemoveEntityCommand>();
	bkConsole::AddCommand("remove-entity",functor);

	functor.Reset<&GetEntityCommand>();
	bkConsole::AddCommand("get-entity",functor);

	functor.Reset<&SetEntityCommand>();
	bkConsole::AddCommand("set-entity",functor);

	functor.Reset<&TakeSnapSnotCommand>();
	bkConsole::AddCommand("take-snapshot",functor);
}
void SetConsoleSimpleWorld( rmcSampleSimpleWorld* smpwrld )  
{
	g_SampleSimpleWorld = smpwrld;
	AddConsoleCommands();
}

#endif


//-----------------------------------------------------------------------------

}//end namespace ragesamples
