// 
// sample_simpleworld/entitymgr.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SAMPLE_SIMPLEWORLD_ENTITYMGR_H
#define SAMPLE_SIMPLEWORLD_ENTITYMGR_H

#include "animmgr.h"
#include "frameEditor.h"
#include "simpleworld.h"

#include "atl/array.h"
#include "atl/functor.h"
#include "atl/singleton.h"
#include "atl/string.h"
#include "data/base.h"
#include "fragment/instance.h"
#include "fragment/type.h"
#include "parser/manager.h"
#include "physics/leveldefs.h"
#include "grprofile/drawmanager.h"
#include "fragment/fragshaft.h"
#include "spatialdata/aabb.h"
#include "spatialdata/sphere.h"
#include "vector/matrix34.h"


//-----------------------------------------------------------------------------

namespace rage
{
	class crAnimation;
    class crCreature;
    class crCreatureComponentParticleEffect;
	class crSkeleton;
    class crMoverInst;
	class crExpressions;
    class crmtComplexPlayer;
	class crmtMotionTree;
    class crmtObserver;
    class grbTargetManager;
	class Matrix34;
	class rmcDrawable;
	class bkGroup;
	class bkBank;
	class bkList;
	class bkGroup;
	class bkSlider;
    class bkText;
	class bkToggle;
	class bkButton;
	class gzGizmo;
	class phSimulator;
	class ptxEffectInst;
    class fragDrawable;
	class grmMatrixSet;
}

//-----------------------------------------------------------------------------

using namespace rage;

namespace ragesamples
{

//-----------------------------------------------------------------------------

// PURPOSE: The basic drawable in the sample.
class SimpleWorldEntity : public rage::datBase
{
public:
	SimpleWorldEntity();
	SimpleWorldEntity(const Matrix34& transform);

	virtual ~SimpleWorldEntity();

	virtual bool	AddToDrawList() {return false;}
	virtual void	Draw() {};
	virtual void	Update() {};

    // name derived from file name
	const atString&		GetEntityName() const { return m_EntityName; }
	void				SetEntityName(const char *name) { m_EntityName = name; }

    // entity.type file (optional)
    const atString&		GetSourceFilePath() const { return m_EntityFilePath; }
    void                SetSourceFilePath( const char *path ) { m_EntityFilePath = path; }

    // identifier for use by the "game"
    const atString&     GetIdentifier() const { return m_Identifier; }
    void                SetIdentifier(const char *identifier) { m_Identifier = identifier; }

	const Matrix34&		GetTransform() const { return m_Transform; }
	void				SetTransform(const Matrix34& transform);

	virtual const spdSphere&	GetXFormSphere();
	const spdAABB&			GetAABB(){return m_EntityAABB;}
	void				UpdateBoundingSphereAndAABB();


	virtual bool		DoesCastShadow() const { return false; }

	bool				IsHidden() const { return m_IsHidden; }
	void				SetHidden(bool bHidden) { m_IsHidden = bHidden; }

	virtual void		Remove() {};

	void				SetUserData(u32 userData)	{ m_UserData = userData; }
	u32					GetUserData() const			{ return m_UserData; }	

    // Memory used in kilobytes
    int                 GetMemoryUsed() const           { return m_memoryUsed; }
    void                SetMemoryUsed( int memoryUsed ) { m_memoryUsed = memoryUsed; }

    virtual rmcDrawable* GetDrawable() const { return 0; }

    static const char* TransformToString( Matrix34 *pMatrix );
    static Matrix34* StringToTransform( const char *pStr );

    bool LoadEntityFile( const char* filePath, bool bIsFullAssetPath=true );
    bool LoadEntityTreeNode( const parTreeNode *pTreeNode );
    bool LoadEntityFromStream( fiStream *pStream );

    bool SaveEntityFile( const char* filePath );
    bool SaveEntityTreeNode( parTreeNode *pParentNode );
    bool SaveEntityToStream( fiStream *pStream );

    PAR_PARSABLE;

private:
    // PURPOSE: To initialize the entity after the xml/data has been read
    // PARAMS:
    //      pEntityFile - entity.type file to load
    //      bIsFullAssetPath - whether pEntityFile has the full path or not
    // RETURNS:
    //      true on success, false on failure
    // NOTES:
    bool Init( const char *pEntityFile, bool bIsFullAssetPath );

    static Matrix34 s_TransformFromString;

protected:
    // PURPOSE: Derived entities override this function.  Called from Init(...).
    // PARAMS:
    //      pEntityFile - entity.type file to load
    //      bIsFullAssetPath - whether pEntityFile has the full path or not
    // RETURNS:
    //      true on success, false on failure
    // NOTES:
    virtual bool InitDerived( const char * /*pEntityFile*/, bool /*bIsFullAssetPath*/ ) { return false; }

    void GetEntityNameFromPath( const char *pPath, bool bIsFullAssetPath=true );

    // PURPOSE: Sets up the ASSET folder and assetPath for loading
    // PARAMS:
    //      assetPath - pass by reference.  this will be modified if bIsFullAssetPath is true
    //      pFilePath - original path of file
    //      bIsFullAssetPath - if pFilePath contains the full path or not
    // RETURNS:
    //      true if we need to call ASSET.PopFolder() after loading the assets
    // NOTES:
    bool SetAssetFile( atString &assetFile, const char *pFilePath, bool bIsFullAssetPath );

	virtual void		PosGizmoDraggedCallback() {};
	virtual void		RotGizmoDraggedCallback();

protected:
    static const int   ENTITY_NAME_CHAR_LEN = 64;
	atString	m_EntityName;
    char        m_EntityNameChar[ENTITY_NAME_CHAR_LEN]; // kluge for the PARSER/widgets
    
    static const int   ENTITY_FILE_PATH_CHAR_LEN = 256;
    atString	m_EntityFilePath;
    char        m_EntityFilePathChar[ENTITY_FILE_PATH_CHAR_LEN];   // kluge for PARSER

    static const int   IDENTIFIER_CHAR_LEN = 64;
    atString    m_Identifier;
    char        m_IdentifierChar[IDENTIFIER_CHAR_LEN]; // kluge for the PARSER/widgets
	
    Matrix34	m_Transform;
    Matrix34*   m_pTransform;   // kluge for the PARSER
	Vector3		m_Rotation;		// Stores euler rotation values (in degrees), for use with the roation type-in widget.

	spdSphere	m_XFormSphere;
	spdAABB		m_EntityAABB;
	bool		m_IsHidden;
	
	gzGizmo*	m_pRotGizmo;
	bool		m_bRotGizmoActive;
	gzGizmo*	m_pPosGizmo;
	bool		m_bPosGizmoActive;
	
	u32			m_UserData;
    int         m_memoryUsed;   // in kilobytes
	
#if __PFDRAW
public:
	virtual void		ProfileDraw() {};
#endif

#if __BANK
public:
	void				AddWidgets(bkBank &bk);
	virtual void		RemoveWidgets(bkBank &bk);

	void				RemoveEntityBankCallback();
	void				FrameEntityBankCallback();
	void				ShowPosGizmoBankCallback();
	void				ShowRotGizmoBankCallback();
	
    virtual void		PositionChangedBankCallback() {}
	virtual void		RotationChangedBankCallback();

    void                IdentifierChangedBankCallback();

protected:
    virtual void		AddDerivedWidgets( bkBank& /*bk*/ ) {}

	bkGroup*			m_pBankGroup;
	bkButton*			m_pBankRemoveEntityButton;
    bkText*             m_pBankTextField;
    bkSlider*           m_pBankUserDataSlider;
	bkSlider*			m_pBankPosSlider;
	bkSlider*			m_pBankRotSlider;
	bkToggle*			m_pBankIsHidden;
	bkButton*			m_pBankFrameEntity;
	bkToggle*			m_pBankIsRotGizmoActive;
	bkToggle*			m_pBankIsPosGizmoActive;
#endif //__BANK
};

inline const spdSphere& SimpleWorldEntity::GetXFormSphere()
{
//	m_XFormSphere.SetCenter(m_Transform.d);
	return m_XFormSphere;
}

inline void SimpleWorldEntity::SetTransform(const Matrix34& transform) 
{ 
	m_Transform = transform; 
	m_XFormSphere.Set(RCC_VEC3V(m_Transform.d), m_XFormSphere.GetRadius());

#if __BANK
    PositionChangedBankCallback();
#endif
}

//-----------------------------------------------------------------------------

// PURPOSE: A movable that contains an entity
class EntityMovable : public SimpleWorldMovable
{
public: 
	EntityMovable(SimpleWorldEntity* pEntity) : SimpleWorldMovable(static_cast<void*>(pEntity)) 
	  {};
	virtual ~EntityMovable() {};

	virtual void AddToDrawList(const rage::fragShaft& shaft);
	virtual const rage::spdSphere& GetBoundingSphere() const;
};

//-----------------------------------------------------------------------------

// PURPOSE: An entity that utilizes an EntityMovable
class SimpleWorldMovableEntity : public SimpleWorldEntity
{
public:
	SimpleWorldMovableEntity();
	SimpleWorldMovableEntity(const Matrix34& transform);

	virtual ~SimpleWorldMovableEntity();

	virtual SimpleWorldMovable*	GetMovable() const { return m_Movable; }

    PAR_PARSABLE;

protected:
	EntityMovable*	m_Movable;
};

//-----------------------------------------------------------------------------
// PURPOSE: An entity that represents a drawable.  The internals use the 
//  crCreature system to support blend shapes and animated normal maps, in 
//  addition to regular animation.
class SimpleWorldDrawableEntity : public SimpleWorldMovableEntity
{
public:
    SimpleWorldDrawableEntity();
    SimpleWorldDrawableEntity( rmcDrawable* drawable, crSkeleton* skeleton, const Matrix34& transform );

    virtual ~SimpleWorldDrawableEntity();

    virtual rmcDrawable* GetDrawable() const { return m_pDrawable; }
    virtual crSkeleton*	GetSkeleton() const { return m_pSkeleton; }
    virtual RvAnimMgr* GetAnimMgr() const { return m_pAnimMgr; }
    virtual crCreature* GetCreature() const { return m_pCreature; }
    virtual grbTargetManager* GetBlendTargetManager() const { return m_pBlendTargetManager; }
	virtual crExpressions* GetExpressions() const { return m_pExpressions; }

	virtual void LoadAndInitialzeExpressions( const char* szFilePath );
    
    virtual bool DoesCastShadow() const { return m_bCastsShadow; }
    virtual bool DoesCastCubeShadow() const { return m_bCastsCubeShadow; }

    bool DoUpdateAnim() const { return m_UpdateAnim; }
    void SetUpdateAnim( bool update );

	void ResetMover();

    virtual bool AddToDrawList();
    virtual void Draw();
    virtual void Update();
    virtual void Remove();

    virtual bool IsExactlySimpleWorldDrawableEntity() { return true; }

    void SetProfileDrawEnabled( bool bEnable );

    PAR_PARSABLE;

protected:
    virtual bool InitDerived( const char *pEntityFile, bool bIsFullAssetPath );

    virtual void PostLoad() {}

    rmcDrawable* m_pDrawable;
    crSkeleton*	m_pSkeleton;
    grbTargetManager* m_pBlendTargetManager;
    crCreature* m_pCreature;
    crExpressions* m_pExpressions;
	
    grmMatrixSet* m_pMatrixSet;
    RvAnimMgr* m_pAnimMgr;   
    RvAnimMgr::AnimPlayMode m_lastAnimPlayMode;

	Vector3	m_MoverPosition;
	Vector3 m_MoverRotation;

    bool m_UpdateAnim;
    int	m_AnimId;

    bool m_ProfileDrawEnabled;
    bool m_bCastsShadow;
    bool m_bCastsCubeShadow;

    bool m_bForceLODLevel;
    float m_ForceLODDistance;

#if __PFDRAW
public:
    virtual void ProfileDraw();
#endif // __PFDRAW

    void BankUpdateAnimCallback();

#if __BANK
public:
    virtual void RemoveWidgets( bkBank &bk );

protected:
    virtual void AddDerivedWidgets( bkBank &bk );
	virtual void AddCreatureWidgets( bkBank &bk );

	void UpdateFrameInOutWidgets();
	void ResetFrameInOutWidgets();

    bkToggle* m_pBankForceLODLevel;
    bkSlider* m_pBankForceLODDistance;
    bkToggle* m_pBankCastsShadow;
    bkToggle* m_pBankUpdateAnim;
    bkToggle* m_pBankEnableProfileDraw;
    bkGroup*  m_pBankShaderGroup;
	bkGroup*  m_pBankCreatureGroup;

	FrameEditorWidgets m_FrameInWidgets;
	FrameEditorWidgets m_FrameOutWidgets;
#endif //__BANK
};

inline void SimpleWorldDrawableEntity::SetProfileDrawEnabled( bool bEnable )
{
    m_ProfileDrawEnabled = bEnable;
}

//-----------------------------------------------------------------------------

// PURPOSE: An entity representing a drawable, animatable object that has
//  a bounds.
class SimpleWorldFragDrawableEntity : public SimpleWorldDrawableEntity
{
public:
    // START
    // These 2 values are only null to satisfy the PARSER.  It will assert
    // if either of them are NULL
    SimpleWorldFragDrawableEntity( phLevelNew *pLevelNew=NULL, phSimulator *pSim=NULL );
    // STOP
    
    SimpleWorldFragDrawableEntity( phLevelNew *pLevelNew, phSimulator *pSim, 
        fragDrawable* drawable, crSkeleton* skeleton, const Matrix34& transform );

    virtual ~SimpleWorldFragDrawableEntity();
    
    virtual phInst* GetInst() const { return m_pInst; }    
    virtual int GetLevelIndex() const { FastAssert(m_pInst); return m_pInst->GetLevelIndex(); }

    bool DoDrawBound() const { return m_DrawBound; }
    void SetDrawBound( bool draw ) { m_DrawBound = draw; }

    virtual void DrawBound();

    //SimpleWorldEntity
    virtual void		Draw();
    virtual void		Update();
    virtual void		Remove();
    virtual void		Reset();

    virtual bool IsExactlySimpleWorldDrawableEntity() { return false; }

    PAR_PARSABLE;

protected:
    virtual void	PositionChangedBankCallback();

    // PURPOSE: Loads the drawable, setups the skeleton and spdSphere, adds to shadowmap
    //          In the case of SimpleWorldFragDrawableEntity, also inits it for the
    //          phSimulator
    // PARAMS:
    //      pEntityFile - entity.type file to load
    //      bIsFullAssetPath - whether pEntityFile has the full path or not
    // RETURNS:
    //      true on success, false on failure
    // NOTES:
    virtual bool InitDerived( const char *pEntityFile, bool bIsFullAssetPath );

    virtual void		PosGizmoDraggedCallback();
    virtual void		RotGizmoDraggedCallback();

    phLevelNew*     m_pLevelNew;
    phSimulator*    m_pSim;
    phInst* 	    m_pInst;
    
    bool            m_DrawBound;

#if __BANK
    bkToggle*   m_pBankDrawBoundToggle;
#endif

#if __PFDRAW
public:
    virtual void		ProfileDraw();	
#endif // __PFDRAW

#if __BANK
public:
    virtual void		RemoveWidgets(bkBank &bk);

protected:
    virtual void		AddDerivedWidgets(bkBank &bk);
#endif // __BANK
};

//-----------------------------------------------------------------------------

// PURPOSE: An entity representing a drawable, animatable object that has a 
//  bounds and can be treated as a full fragment (destructable, ragdoll, etc.)
class SimpleWorldFragmentEntity : public SimpleWorldFragDrawableEntity
{
public:
    // START
    // These 2 values are only null to satisfy the PARSER.  It will assert
    // if either of them are NULL
    SimpleWorldFragmentEntity( phLevelNew *pLevelNew=NULL, phSimulator *pSim=NULL );
    // STOP

    SimpleWorldFragmentEntity( phLevelNew *pLevelNew, phSimulator *pSim, 
        fragDrawable* drawable, crSkeleton* skeleton, const Matrix34& transform );

    virtual ~SimpleWorldFragmentEntity();

    virtual rmcDrawable* GetDrawable() const;
    virtual fragType* GetFragType() const { return m_pFragType; }
    virtual crMoverInst* GetMoverInst() const { return m_pMoverInst; }

    bool StartActive() const { return m_startActive; }
    void  SetStartActive( bool startActive ) { m_startActive = startActive; InitialStateChangedBankCallback(); }

    virtual void Update();
    virtual void Draw();
    virtual const spdSphere& GetXFormSphere();
    virtual void Reset();
    virtual void Remove();

#if __PFDRAW
    virtual void ProfileDraw();	
#endif // __PFDRAW

    PAR_PARSABLE;

protected:
    virtual bool InitDerived( const char *pEntityFile, bool bIsFullAssetPath );

    virtual void PosGizmoDraggedCallback();
    virtual void RotGizmoDraggedCallback();

    fragType* m_pFragType;
    crMoverInst* m_pMoverInst;
    bool m_startActive; // what state the object should be in on reset (fixed, active or inactive)

    virtual void PositionChangedBankCallback();

#if __BANK
public:
    virtual void RemoveWidgets( bkBank &bk );

protected:
    virtual void AddDerivedWidgets( bkBank& bk );

    bkToggle* m_pBankActiveToggle;
#endif //__BANK

    virtual void InitialStateChangedBankCallback();
};

//-----------------------------------------------------------------------------

// PURPSOE: A particle effects emitter entity
class SimpleWorldRmptfxEmitter : public SimpleWorldMovableEntity
{
public:
    SimpleWorldRmptfxEmitter();
    SimpleWorldRmptfxEmitter( ptxEffectInst* emitter );
    virtual ~SimpleWorldRmptfxEmitter();

    virtual ptxEffectInst* GetEffectInst() const { return m_pEmitter; }
    virtual RvAnimMgr* GetAnimMgr() const { return m_pAnimMgr; }
    virtual crCreature* GetCreature() const { return m_pCreature; }

    bool DoUpdateAnim() const { return m_UpdateAnim; }
    void SetUpdateAnim( bool update );

    virtual void Draw();
    virtual void Update();

    void SetProfileDrawEnabled( bool bEnable );

    PAR_PARSABLE;

private:
    ptxEffectInst* m_pEmitter;

    crCreature* m_pCreature;
    crCreatureComponentParticleEffect *m_pComponentPtfx;
    RvAnimMgr* m_pAnimMgr;   
    RvAnimMgr::AnimPlayMode m_lastAnimPlayMode;

    bool m_UpdateAnim;
    int	m_AnimId;

    bool m_ProfileDrawEnabled;

#if __PFDRAW
public:
    virtual void ProfileDraw();
#endif // __PFDRAW

    void BankUpdateAnimCallback();

#if __BANK
public:
    virtual void RemoveWidgets(bkBank &bk);

protected:
    virtual void AddDerivedWidgets(bkBank &bk);
    void StartEmitterBankCbk();
    void StopEmitterBankCbk();

    bkButton* m_pBankStart;
    bkButton* m_pBankStop;
    bkToggle* m_pBankEnableProfileDraw;
    bkToggle* m_pBankUpdateAnim;
#endif //__BANK
};

inline void SimpleWorldRmptfxEmitter::SetProfileDrawEnabled( bool bEnable )
{
    m_ProfileDrawEnabled = bEnable;
}

//-----------------------------------------------------------------------------

// PURPOSE: Handles the attachment of one entity to another at a bone joint.
class SimpleWorldAttachmentMgr : public rage::datBase
{
public:
	SimpleWorldAttachmentMgr();
	~SimpleWorldAttachmentMgr();

	void Attach(const char *pParentEntityName, const char *pAttachedEntityName, const char *pParentBoneName, const char *pAttachedLocName);
	void Detach(const char *pParentEntityName, const char *pAttachedEntityName);
	bool IsAttached(const char *pParentEntityName, const char *pAttachedEntityName) const;
	bool IsAttached(const char *pAttachedEntityName) const;

	void Update();

#if __BANK
	void AddWidgets();
	void DebugDraw();
#endif

private:
	class AttachData
	{
	public:
		AttachData(SimpleWorldDrawableEntity *pParentEntity, SimpleWorldFragmentEntity *pAttachedEntity, const char *pParentBoneName, const char *pAttachedLocName);
		~AttachData();

		const char * GetLocName() const
		{
#if __BANK
			if( m_bOverrideLocName )
				return m_OverrideLocName;
#endif
			return m_AttachedLocName;
		}
#if __BANK
		void AddWidgets(bkBank &b);
		bool m_bOverrideLocName;
		char m_OverrideLocName[128];
#endif

		SimpleWorldDrawableEntity *m_pParent;
		SimpleWorldFragmentEntity *m_pChild;
		ConstString m_ParentBoneName;
		ConstString m_AttachedLocName;
	};

	void UpdateAttachment(AttachData &ad);

	atArray<AttachData*> m_Attachments;

#if __BANK
	bool m_bDrawAttachmentBones;
	bool m_bDrawAttachmentLocators;
#endif
};

//-----------------------------------------------------------------------------

// PURPOSE: Manages all of the entities in the sample
class SimpleWorldEntityMgr : public rage::datBase
{
public:
	typedef rage::Functor1<void*>	InsertMovableFunctor;
	typedef rage::Functor1<void*>	RemoveMovableFunctor;
	typedef rage::Functor1<void*>	UpdateMovableFunctor;
	typedef rage::Functor5Ret<bool,
							const rmcDrawable&,
							const Matrix34*,
							const crSkeleton*,
							const grmMatrixSet*,
							float>	AddToDrawBucketFunctor;

	typedef rage::Functor1<const spdSphere&>	FrameCameraFunctor;
	typedef rage::Functor1Ret<float,
							  const Vector3&>	DistanceFromCameraFunctor;
    typedef rage::Functor1<SimpleWorldEntity*>  RemoveEntityFunctor;

public:
	~SimpleWorldEntityMgr();

	void				SetMovableFunctors(InsertMovableFunctor insertFtor,
											RemoveMovableFunctor removeFtor,
											UpdateMovableFunctor updateFtor);
	void				SetDrawFunctors(AddToDrawBucketFunctor addToDrawBucketFtor);
	void				SetCameraFunctors(FrameCameraFunctor frameCameraFtor,
										  DistanceFromCameraFunctor distanceFromCameraFtor);
    void                SetRemoveEntityFunctor( RemoveEntityFunctor removeEntityFtor );

	void				InsertEntity(SimpleWorldEntity* pEntity, bool bInsertMovable = true);
	
    int					GetEntityCount() const;
	
    SimpleWorldEntity*	GetEntity(int entityIdx) const;
    SimpleWorldEntity*  GetEntity(const char *entityId) const;

	bool				RemoveEntity(SimpleWorldEntity* pEntity);
    bool                RemoveEntity(const char *entityId);
	void				RemoveEntity(int entityIdx);

	void				GetShadowCastingEntities(atArray<SimpleWorldEntity*>& retEntities);
	int					GetShadowCastingEntitiesCount() const;

	SimpleWorldAttachmentMgr &
						GetAttachmentMgr() { return m_AttachmentMgr; }

	void				Update();
	void				Reset();

	//SimpleWorldEntityMgr Functors
	void				InsertMovable(void* movableId);
	void				RemoveMovable(void* movableId);
	void				UpdateMovable(void* movableId);
	void				AddToDrawBucket(const rmcDrawable& toDraw,
										const Matrix34* matrix,
										const crSkeleton* skeleton,
										const grmMatrixSet* matrixSet,
										float dist);
	void				FrameCamera(const spdSphere& focusSphere);
	float				DistanceFromCamera(const Vector3& point);

	//Utility methods for initializing defualt dummy functors
	static bool			ReturnFalse(const rmcDrawable&, const Matrix34*, const crSkeleton*, const grmMatrixSet*, float);
	static void*		ReturnZero(void*);
	static void			NullCameraFrame(const spdSphere& sphere);
	static float		ReturnZeroDistance(const Vector3& pos);
	

#if __PFDRAW
	void				ProfileDraw();
#endif //__PFDRAW

#if __BANK
public:
	void				InitBank();
	void				UnhideAllBankCallback();

protected:
	bkBank*				m_pBank;
	bkButton*			m_pBankUnHideAll;
#endif //__BANK

protected:
	SimpleWorldEntityMgr();

private:
	atArray<SimpleWorldEntity*>			m_EntityTable;
	SimpleWorldAttachmentMgr			m_AttachmentMgr;
	
	InsertMovableFunctor				m_InsertMovable;
	RemoveMovableFunctor				m_RemoveMovable;
	UpdateMovableFunctor				m_UpdateMovable;
	AddToDrawBucketFunctor				m_AddToDrawBucketFtor;
	FrameCameraFunctor					m_FrameCameraFtor;
	DistanceFromCameraFunctor			m_DistanceFromCameraFtor;
    RemoveEntityFunctor                 m_RemoveEntityFtor;
};

//-----------------------------------------------------------------------------

typedef atSingleton<SimpleWorldEntityMgr> smpWorldEntityMgrSingleton;

#define SMPWORLDEMGR			::ragesamples::smpWorldEntityMgrSingleton::InstanceRef()
#define INIT_SMPWORLDEMGR		::ragesamples::smpWorldEntityMgrSingleton::Instantiate()
#define SHUTDOWN_SMPWORLDEMGR	::ragesamples::smpWorldEntityMgrSingleton::Destroy()

#if __BANK
#define SMPWORLDEMGR_BANK		"SimpleWorld Entities"
#endif //__BANK

inline int SimpleWorldEntityMgr::GetEntityCount() const
{
	return m_EntityTable.GetCount();
}

inline int SimpleWorldEntityMgr::GetShadowCastingEntitiesCount() const
{
	int w = 0;
	int entityCount = GetEntityCount();
	for(int i=0; i<entityCount; i++)
	{
		SimpleWorldEntity* pEntity = GetEntity(i);
		if(pEntity->DoesCastShadow())
			w++;
	}

	return w;
}

inline SimpleWorldEntity* SimpleWorldEntityMgr::GetEntity(int entityIdx) const
{
	FastAssert(entityIdx < m_EntityTable.GetCount());
	return m_EntityTable[entityIdx];
}

inline SimpleWorldEntity* SimpleWorldEntityMgr::GetEntity(const char *entityId) const
{
    int count = GetEntityCount();
    for (int entityIdx = 0; entityIdx < count; ++entityIdx)
    {
        SimpleWorldEntity* pEntity = GetEntity( entityIdx );
        if ( pEntity->GetIdentifier() == entityId )
        {
            return pEntity;
        }
    }

    return NULL;
}

inline void	SimpleWorldEntityMgr::SetMovableFunctors(InsertMovableFunctor insertFtor,
													RemoveMovableFunctor removeFtor,
													UpdateMovableFunctor updateFtor)
{
	m_InsertMovable = insertFtor;
	m_RemoveMovable = removeFtor;
	m_UpdateMovable = updateFtor;	
}

inline void SimpleWorldEntityMgr::SetDrawFunctors(AddToDrawBucketFunctor addToDrawBucketFtor)
{
	m_AddToDrawBucketFtor = addToDrawBucketFtor;
}

inline void	SimpleWorldEntityMgr::SetCameraFunctors(FrameCameraFunctor frameCameraFtor,
													DistanceFromCameraFunctor distanceFromCameraFtor)
{
	m_FrameCameraFtor = frameCameraFtor;
	m_DistanceFromCameraFtor = distanceFromCameraFtor;
}

inline void SimpleWorldEntityMgr::SetRemoveEntityFunctor(RemoveEntityFunctor removeEntityFtor)
{
    m_RemoveEntityFtor = removeEntityFtor;
}

inline void SimpleWorldEntityMgr::InsertMovable(void* movableId)
{
	m_InsertMovable(movableId);
}

inline void SimpleWorldEntityMgr::RemoveMovable(void* movableId)
{
	m_RemoveMovable(movableId);
}

inline void SimpleWorldEntityMgr::UpdateMovable(void* movableId)
{
	m_UpdateMovable(movableId);
}

inline void SimpleWorldEntityMgr::AddToDrawBucket(const rmcDrawable& toDraw,
												const Matrix34* matrix,
												const crSkeleton* skeleton,
												const grmMatrixSet* matrixSet,
												float dist)
{
	m_AddToDrawBucketFtor(toDraw, matrix, skeleton, matrixSet, dist);
}

inline void SimpleWorldEntityMgr::FrameCamera(const spdSphere& focusSphere)
{
	m_FrameCameraFtor(focusSphere);
}

inline float SimpleWorldEntityMgr::DistanceFromCamera(const Vector3& point)
{
	return m_DistanceFromCameraFtor(point);
}

inline void* SimpleWorldEntityMgr::ReturnZero(void*)
{
	return 0;
}

inline bool SimpleWorldEntityMgr::ReturnFalse(const rmcDrawable&, const Matrix34*, const crSkeleton*, const grmMatrixSet*, float)
{
	return false;
}

inline void SimpleWorldEntityMgr::NullCameraFrame(const spdSphere&)
{
}

inline float SimpleWorldEntityMgr::ReturnZeroDistance(const Vector3&)
{
	return 0.0f;
}

//-----------------------------------------------------------------------------

}//End namespace ragesamples

#endif
