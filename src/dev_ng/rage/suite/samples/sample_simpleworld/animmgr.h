// 
// sample_simpleworld/animmgr.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SAMPLE_SIMPLEWORLD_ANIMMGR_H
#define SAMPLE_SIMPLEWORLD_ANIMMGR_H

#include "atl/array.h"
#include "atl/functor.h"
#include "atl/map.h"
#include "atl/singleton.h"
#include "atl/string.h"
#include "cranimation/animation.h"
#include "cranimation/commonpool.h"
#include "cranimation/framefilters.h"
#include "cranimation/frame.h"
#include "cranimation/framedatafactory.h"
#include "cranimation/frameaccelerator.h"
#include "crmotiontree/motiontreescheduler.h"
#include "crmotiontree/observer.h"
#include "data/base.h"

using namespace rage;

namespace rage
{
    class bkBank;
    class bkButton;
    class bkList;
    class bkSlider;
    class bkToggle;
	class bkCombo;
    class crClip;
    class crmtMotionTree;
    class crmtMotionTreeScheduler;
	class crExpressions;
	class crTagEditorWidget;
	class crClipPropertyEditor;
	class crCreature;
}

namespace ragesamples
{

//-----------------------------------------------------------------------------

class AnimMgrFrameFilter : public crFrameFilter
{
public:
	AnimMgrFrameFilter()
		: crFrameFilter(kFrameFilterTypeProjectFilters)
		, m_AllowMover(false)
		, m_AllowRootTranslation(true)
	{
	}
	
	AnimMgrFrameFilter(datResource& rsc)
		: crFrameFilter(rsc)
	{
	}

	virtual ~AnimMgrFrameFilter() {};

	CR_DECLARE_FRAME_FILTER_TYPE(AnimMgrFrameFilter)

	virtual bool FilterDof(u8 track, u16 id, float&);
	virtual u32 CalcSignature() const;

	static u32 sm_Hash;

	bool m_AllowMover;
	bool m_AllowRootTranslation;
};

//-----------------------------------------------------------------------------

class RvAnimMgr : public rage::datBase
{
public:
	enum AnimPlayMode
	{
		ANIM_FORWARD = 1,
		ANIM_PAUSE,
		ANIM_BACKWARD,
        ANIM_STEP_FORWARD,  // up to the user to set m_PlayMode to ANIM_PAUSE after handling the action
        ANIM_STEP_BACKWARD, // up to the user to set m_PlayMode to ANIM_PAUSE after handling the action
        ANIM_GOTO_START,    // up to the user to set m_PlayMode to ANIM_PAUSE after handling the action
        ANIM_GOTO_END,       // up to the user to set m_PlayMode to ANIM_PAUSE after handling the action
		ANIM_GOTO_START_AND_PLAY_FORWARD,
		ANIM_GOTO_END_AND_PLAY_BACKWARD,
	};

	enum MoverPlayMode
	{
		MOVER_CONTINUOUS,
		MOVER_ABSOLUTE
	};

	typedef rage::Functor1<RvAnimMgr*>	AnimMgrChangeFtor;
	typedef rage::Functor1<int>			AnimMgrSelectedFtor;
	typedef rage::Functor0				AnimMgrCommandFtor;
	
public:
    RvAnimMgr();
	~RvAnimMgr();

	void Init(crCreature* creature);

    static void         InitClass();
    static void         ShutdownClass();

    static void		    Reset();

	//PURPOSE : Inserts an animation into the bank of animations
    static void		    InsertAnimation(crAnimation* pAnimation);

	//PURPOSE : Removes an animation from the bank of animations
    static void         RemoveAnimation(crAnimation* pAnimation);

	//PURPOSE : Loads an animation and inserts it into the bank of animations
    static crAnimation*	LoadAndInsertAnimation(const char* filePath);

	//PURPOSE : Removes an animation from the bank of animations based on a filename
    static void         UnloadAndRemoveAnimation(const char* filePath);

	//PURPOSE : Returns the number of animations
    static int			GetAnimationCount();
	
	//PURPOSE : Returns a pointer the the animation at the supplied index
    static crAnimation*	GetAnimation( int animIdx );

	//PURPOSE : Returns the index of an animation based on the supplied pointer
    static int          GetAnimationIndex( crAnimation* pAnimation );

	//PURPOSE : Returns the index of an animation based on it's internal animation id
    static int          GetAnimationIndex( int animId );
    
	//PURPSOE : Retrieves an animation based on the supplied animation id
    static crAnimation* GetAnimationById( int animId );

	//PURPOSE : Returns the id of the supplied animation (0 of the animation could not be found, 
	//			as id's are assigned starting at 1)
    static int          GetAnimationId( crAnimation* pAnimation );

	//PURPOSE : Returns the id of an animation based on an index into the animation list
    static int          GetAnimationId( int animIdx );
    
	//PURPOSE : Inserts a clip in the the bank of clips
    static void		    InsertClip(crClip* pClip);

	//PURPOSE : Removes a clip
    static void         RemoveClip(crClip* pClip);

	//PURPOSE : Loads a clip from the specified path and inserts it into the bank of clips
    static crClip*  	LoadAndInsertClip(const char* filePath);

	//PURPOSE : Removes a clip from the bank of clips identified by the supplied file path
    static void         UnloadAndRemoveClip(const char* filePath);

    static int			GetClipCount();
    static crClip*	    GetClip( int clipIdx );
    static int          GetClipIndex( crClip* pClip );
    static int          GetClipIndex( int clipId );

    static crClip*      GetClipById( int clipId );
    static int          GetClipId( crClip* pClip );
    static int          GetClipId( int clipIdx );

    float			GetFrameRate() const { return m_FrameRate; }
	void			SetFrameRate( float fr );

    float			GetCurFrame() const { return m_CurrentFrame; }
	void            SetCurFrame( float frame ) { m_CurrentFrame = frame; m_currentFrameChangedFtor(this); }

	float			GetCurTime() const;
    void            SetCurTime( float time );

	float			GetCurPhase() const { return m_Phase; }
	
    bool            IsInAnimMode() const { return m_animMode; }
    bool            IsInClipMode() const { return !m_animMode; }

	crAnimation*	GetCurrentAnimation() const;
    crClip*         GetCurrentClip() const;

    float			GetCurrentAnimStartFrame() const { return m_StartFrame; }
    float			GetCurrentAnimEndFrame() const { return m_EndFrame; }

	float			GetCurrentAnimStartTime() const;
	float			GetCurrentAnimEndTime() const;

    float			GetPlaybackStartFrame() const { return m_PlaybackStartFrame; }
    void			SetPlaybackStartFrame( float startFrame ) { m_PlaybackStartFrame = startFrame; }

    float			GetPlaybackStartTime() const;
	void			SetPlaybackStartTime( float startTime );

    float			GetPlaybackEndTime() const;
    void			SetPlaybackEndTime( float endTime );

    float			GetPlaybackEndFrame() const { return m_PlaybackEndFrame; }
    void			SetPlaybackEndFrame( float endFrame ) { m_PlaybackEndFrame = endFrame; }

    float			GetStepSize() const { return m_StepSize; }
    void			SetStepSize(float stepSize) { m_StepSize = stepSize; }

    AnimPlayMode	GetPlayMode() const { return (AnimPlayMode)m_PlayMode; }
	void			SetPlayMode(AnimPlayMode playMode) { m_PlayMode = playMode; }
	void			TogglePlayMode();

	MoverPlayMode	GetMoverPlayMode() const { return (MoverPlayMode)m_MoverPlayMode; }
	void			SetMoverPlayMode(MoverPlayMode playMode) { m_MoverPlayMode = playMode; }

    crmtMotionTree* GetMotionTree() const { return m_pMotionTree; }
   
	void			Update();

    int             GetCurrentAnimationIndex() const { return m_CurrentAnimIdx; }
    void            SetCurrentAnimationIndex( int animIdx );
    int             GetCurrentAnimationId() const { return GetAnimationId( m_CurrentAnimIdx ); }
    void            SetCurrentAnimationId( int animId );
    
    int             GetCurrentClipIndex() const { return m_CurrentClipIdx; }
    void            SetCurrentClipIndex( int animIdx );
    int             GetCurrentClipId() const { return GetClipId( m_CurrentClipIdx ); }
    void            SetCurrentClipId( int animId );

	bool			GetSequence() const { return m_Sequence; }
    void			SetSequence( bool sequence );

	void			SetLoopAnimation( bool bLoop ) { m_bLoopAnimation = bLoop; }
	bool			GetLoopAnimation() const { return m_bLoopAnimation; }

	void			SetAllowMover( bool bAllow ) { m_Filter.m_AllowMover = bAllow; }
	bool			GetAllowMover() const { return m_Filter.m_AllowMover; }

	void			SetAllowRootTranslation (bool bAllow) { m_Filter.m_AllowRootTranslation = bAllow; }
	bool			GetAllowRootTranslation() const { return m_Filter.m_AllowRootTranslation; }

	void			SetMoverDebugDraw( bool bDraw ) { m_bMoverDebugDraw = bDraw; }
	bool			GetMoverDebugDraw() const { return m_bMoverDebugDraw; }

	void			SetCurrentFrameChangedFtor( AnimMgrChangeFtor ftor ) { m_currentFrameChangedFtor = ftor; }
	void			SetPlaybackModeChangedFtor( AnimMgrChangeFtor ftor ) { m_playModeChangedFtor = ftor; }
	void			SetFrameRangeChangedFtor( AnimMgrChangeFtor ftor ) { m_frameRangeChangedFtor = ftor; }
	void			SetLoopedFtor( AnimMgrChangeFtor ftor ) { m_loopedFtor = ftor; }
	
	void			SetLoopAnimationChangedFtor( AnimMgrChangeFtor ftor) { m_loopAnimationChangedFtor = ftor; }

	void			SetAnimIndexChangedFtor( AnimMgrChangeFtor ftor ) { m_animIndexChangedFtor = ftor; }
	void			SetClipIndexChangedFtor( AnimMgrChangeFtor ftor ) { m_clipIndexChangedFtor = ftor; }
	void			SetSequenceModeChangedFtor( AnimMgrChangeFtor ftor ) { m_sequenceModeChangedFtor = ftor; }
	void			SetResetMoverFtor( AnimMgrCommandFtor ftor ) { m_resetMoverFtor = ftor; }
	void			SetMoverModeChangedFtor( AnimMgrChangeFtor ftor ) { m_moverModeChangedFtor = ftor; }
	void			SetAllowMoverChangedFtor( AnimMgrChangeFtor ftor ) { m_allowMoverChangedFtor = ftor; }
	void			SetAllowRootTranslationChangedFtor ( AnimMgrChangeFtor ftor ) { m_allowRootTranslationChangedFtor = ftor; }
	void			SetMoverDebugDrawChangedFtor ( AnimMgrChangeFtor ftor ) { m_moverDebugDrawChangedFtor = ftor; }

	static void		SetAnimSelectedFtor( AnimMgrSelectedFtor ftor ) { sm_AnimSelectedFtor = ftor; }
	static void		SetClipSelectedFtor( AnimMgrSelectedFtor ftor )	{ sm_ClipSelectedFtor = ftor; }


	// Whichever RvAnimMgr wants to set the editor timelines should call this functions. 
	void			UpdateClipEditorCurrentTime();
	void			UpdateClipEditorTimeWindow();

	void			SetExpressions(crExpressions* pExpr);
	crFrame&		GetFrameIn() { return m_FrameIn; }
	crFrame&		GetFrameOut() { return m_FrameOut; }
	bool			IsFrameInDirty() const { return m_bFrameInDirty; }
	void			CleanFrameIn() { m_bFrameInDirty = false; }

public:
	static crFrameDataFactory sm_FrameDataFactory;
	static crFrameAccelerator sm_FrameAccelerator;
	static crCommonPool sm_CommonPool;

private:
	static crmtMotionTreeScheduler sm_MotionTreeScheduler;

    static atMap<int, crAnimation*> sm_AnimationTable;   // store by animIdx so we can add/remove at will
    static atArray<crAnimation*>    sm_AnimationList;   // for sequencing
    static int                      sm_nextAnimId;

    static atMap<int, crClip*>      sm_ClipTable;   // store by clipIdx so we can add/remove at will
    static atArray<crClip*>         sm_ClipList;   // for sequencing
    static int                      sm_nextClipId;

	static AnimMgrSelectedFtor		sm_AnimSelectedFtor;	//Functor to be called when an animation was picked in the animation list through the UI
	static AnimMgrSelectedFtor		sm_ClipSelectedFtor;	//Functor to be called when a clip was picked in the clip list through the UI

	static bool						sm_ManageEventManager;

#if __BANK
    static bkList*						sm_pBankAnimList;
    static bkList*						sm_pBankClipList;
	static atArray<crTagEditorWidget*>	sm_ClipTagEditors;
	static atArray<crClipPropertyEditor*>	sm_ClipPropertyEditors;
#endif
	
	float						m_FrameRate;
	float						m_CurrentFrame;
	float						m_StartFrame;
	float						m_EndFrame;
	float						m_PlaybackStartFrame;
	float						m_PlaybackEndFrame;
	float						m_StepSize;
    bool                        m_animMode; // true for animation, false for clip
	int							m_CurrentAnimIdx;
    int                         m_CurrentClipIdx;
	bool						m_Sequence;
	int							m_PlayMode;
	int							m_LastPlayDirection;
	bool						m_bLoopAnimation;
	int							m_MoverPlayMode;
	crFrame						m_FrameIn;
	crFrame						m_FrameOut;
	float						m_Phase;
	bool						m_bMoverDebugDraw;
	bool						m_bFrameInDirty;

    crmtMotionTree*             m_pMotionTree;
	crmtObserver*				m_pObserverAnim;
	crmtObserver*				m_pObserverClip;
	crmtObserver*				m_pObserverExpr;
	AnimMgrFrameFilter			m_Filter;

	AnimMgrChangeFtor			m_currentFrameChangedFtor;
	AnimMgrChangeFtor			m_playModeChangedFtor;
	AnimMgrChangeFtor			m_animIndexChangedFtor;
	AnimMgrChangeFtor			m_clipIndexChangedFtor;
	AnimMgrChangeFtor			m_sequenceModeChangedFtor;
	AnimMgrChangeFtor			m_frameRangeChangedFtor;
	AnimMgrChangeFtor			m_loopedFtor;
	AnimMgrChangeFtor			m_moverModeChangedFtor;
	AnimMgrChangeFtor			m_allowMoverChangedFtor;
	AnimMgrChangeFtor			m_allowRootTranslationChangedFtor;
	AnimMgrChangeFtor			m_moverDebugDrawChangedFtor;
	AnimMgrChangeFtor			m_loopAnimationChangedFtor;
	AnimMgrCommandFtor			m_resetMoverFtor;

public:
#if __BANK
    static void     InitWidgets( bkBank &bk );

	void			AddWidgets( bkBank &bk );
    void            RemoveWidgets( bkBank &bk );

	static void		SaveClip(crClip* clip);
	static void		SaveClipAs(crClip* clip);
	static void		SaveClipTemplate(crClip* clip);
	static void		LoadClipTemplate(crClip* clip);
	static void		SaveAllClips();
#endif

	static void		AnimListCbkDoubleClick(s32 key);
	static void		ClipListCbkDoubleClick(s32 key);

	void			BankCbkAnimCtrlCurrentFrameChanged();
	void			BankCbkAnimCtrlPhaseChanged();
    void            BankCbkAnimCtrlAnimOrClipToggleChanged();
    void			BankCbkAnimCtrlAnimIndexChanged();
    void			BankCbkAnimCtrlClipIndexChanged();
	void			BankCbkAnimCtrlGotoStart();
	void			BankCbkAnimCtrlStepBack();
	void			BankCbkAnimCtrlPlayBackward();
	void			BankCbkAnimCtrlPause();
	void			BankCbkAnimCtrlPlayForward();
	void			BankCbkAnimCtrlStepForward();
	void			BankCbkAnimCtrlGotoEnd();
	void			BankCbkSequenceModeChanged();
	void			BankCbkLoopAnimationChanged();
	void			BankCbkMoverModeChanged();
	void			BankCbkResetMover();
	void			BankCbkResetAndPlay();
	void			BankCbkAllowMoverChanged();
	void			BankCbkAllowRootTranslationChanged();
	void			BankCbkMoverDebugDrawChanged();
	void			BankCbkPlaybackStartTimeChanged();
	void			BankCbkPlaybackEndTimeChanged();
	void			BankCbkAnimStartTimeChanged();
	void			BankCbkAnimEndTimeChanged();


#if __BANK
private:
    bkToggle*       m_pAnimOrClipToggle;
    bkSlider*       m_pAnimIdxSlider;
    bkSlider*       m_pClipIdxSlider;
    bkToggle*       m_pSeqToggle;
    bkSlider*       m_pRateSlider;
    bkSlider*       m_pTimeSlider;
	bkSlider*		m_pPhaseSlider;
    bkSlider*       m_pStartTimeSlider;
    bkSlider*       m_pEndTimeSlider;
    bkSlider*       m_pPlaybackStartTimeSlider;
    bkSlider*       m_pPlaybackEndTimeSlider;    
    bkSlider*       m_pStepSizeSlider;
    bkButton*       m_pGotoStartButton;
    bkButton*       m_pStepBackButton;
    bkButton*       m_pPlayBackwardsButton;
    bkButton*       m_pPauseButton;
    bkButton*       m_pPlayForwaredsButton;
    bkButton*       m_pStepForwardsButton;
    bkButton*       m_pGotoEndButton;
	bkToggle*		m_pLoopAnimationToggle;
	bkButton*		m_pResetMoverButton;
	bkCombo*		m_pMoverModeCombo;
	bkToggle*		m_pAllowMoverToggle;
	bkToggle*		m_pAllowRootTranslationToggle;
	bkToggle*		m_pMoverDebugDraw;
#endif //__BANK

};

//-----------------------------------------------------------------------------

}//End namespace ragesamples

#endif //SAMPLE_SIMPLEWORLD_ANIMMGR_H
