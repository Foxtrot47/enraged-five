// 
// sample_simpleworld/bucketcontrol.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "atl\string.h"
#include <map>
#include <vector>
#include <algorithm>

#include "crskeleton/skeleton.h"
#include "grmodel/matrixset.h"
#include "rmcore/drawable.h"
#include "vector/matrix34.h"


namespace rage
{

	

namespace RenderableElement
{
	
	class StackBasedAllocator
	{
	static std::vector<char>& Mem() 
	{
		static std::vector<char>	sm_mem;
		return sm_mem;
	}
	public:
		void* operator new(size_t size  
	#if RAGE_ENABLE_RAGE_NEW
					,const char*,int
	#endif
					)
		{
			Mem().resize( Mem().size() + size + 8  );
			return ((char*)Mem().begin() + (Mem().size() - size ));
		}
		static void Allocate( int memSize )
		{
			Mem().reserve( memSize );
		}
		static void Free()
		{
			Mem().resize(0);
		}
		static char* GetBase() { return (char*)Mem().begin(); }
	};

	class ElementBase : public StackBasedAllocator
	{
	public:
		ElementBase( float keyValue ) : m_key( keyValue )
		{}
		virtual ~ElementBase() { }
		virtual void Draw( int drawbucket ) = 0;
		float Key() const { return m_key;}
	private:
		ElementBase& operator=( const ElementBase& other );
		ElementBase( const ElementBase& other );
		float		m_key;
	};

	class Drawable: public ElementBase
	{
	public:
		Drawable( 	const rmcDrawable& toDraw,
			const Matrix34* matrix,
			int lod,
			float dist ) : 
			m_toDraw( toDraw ),
			m_matrixList( matrix ),
			m_lod( lod ),
			ElementBase( dist )
		{}
		void Draw( int bucket )
		{
			m_toDraw.Draw( *m_matrixList, bucket, m_lod );
		}
	protected:
		const rmcDrawable&	m_toDraw;
		const Matrix34*		m_matrixList;
		int					m_lod;
	};

	class DrawableSkinned: public ElementBase
	{
	public:
		DrawableSkinned(
			const rmcDrawable& toDraw,
			const crSkeleton*  skeleton,
			const grmMatrixSet* matrixSet,
			int lod,
			float dist ) :  
			m_toDraw( toDraw ),
			m_skeleton( skeleton ),
			m_matrixSet( matrixSet ),
			m_lod( lod ),
			ElementBase( dist )
		{
		}

		void Draw( int bucket )
		{		
			m_toDraw.DrawSkinned(RCC_MATRIX34(*m_skeleton->GetParentMtx()) , *m_matrixSet, bucket, m_lod );
		}

	public:
		const rmcDrawable&	m_toDraw;
		const crSkeleton*	m_skeleton;
		const grmMatrixSet*	m_matrixSet;
		int					m_lod;
	};
};

	template<class PTR, class BASE>
	class RelocateablePointer
	{
		char* m_ptr;

		void set( PTR* p )
		{
			m_ptr =  (char*)( ((int)((char*) ( p ) ) )- (int)BASE::GetBase() );
		}
		PTR* get() const
		{
			return (PTR*)( m_ptr +  (int)BASE::GetBase() );
		}
	public:
		RelocateablePointer( PTR* ptr ) 
		{  set( ptr ); }
		RelocateablePointer() : m_ptr(0)
		{}

		const PTR* operator->() const { return get(); }
		const PTR&  operator*() const { return *get(); }

		PTR* operator->() { return get(); }
		PTR&  operator*()  { return *get(); }

		const RelocateablePointer& operator=(const RelocateablePointer& ptr )
		{
			m_ptr = ptr.m_ptr;
			return *this;
		}
	};

	class BucketRenderer
	{
		typedef RelocateablePointer< RenderableElement::ElementBase, RenderableElement::StackBasedAllocator> PtrType;
		typedef std::vector<PtrType>	RenderList;
	public:
		struct SortBackToFront   
		{
			bool operator()( const PtrType a, const PtrType b )
			{
				return a->Key() > b->Key();
			}
		};

		
		void Clear()
		{
			RenderableElement::StackBasedAllocator::Free();
			for ( int i = 0; i < MaxNumberOfBuckets; i++ )
			{
				bucketLists[i].resize(0);
			}
		}
		void Add( int bucket, RenderableElement::ElementBase* renderable )
		{
			bucketLists[bucket].push_back( PtrType( renderable) );
		}

		template<class T> 
		void Sort( int bucketNo, T Sorter )
		{
			std::sort( bucketLists[ bucketNo].begin(), bucketLists[ bucketNo].end(), Sorter );
		}

		void RenderBucket( RenderList& list , int bucket )
		{
			for ( RenderList::iterator itor = list.begin(); itor != list.end();
					++itor )
			{
				(*itor)->Draw( bucket );
			}
		}
		void Render( u32 bucketMask )
		{
			int mask = 1;
			for (int bucket = 0; bucket < MaxNumberOfBuckets; ++bucket, mask <<= 1)
			{
				if ( mask & bucketMask )
				{
					RenderBucket( bucketLists[ bucket], bucket );
				}
			}
		}
	private:
		static const int MaxNumberOfBuckets = 32;
		
		RenderList		bucketLists[ MaxNumberOfBuckets];
	};


};
