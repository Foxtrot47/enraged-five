// 
// sample_simpleworld/sample_multidrawable.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "system/main.h"
#include "sample_simpleworld/sample_simpleworld.h"

using namespace ragesamples;

int Main()
{
	for (int pass=0; pass<1; pass++) {		// Change the 1 to a 2 to test shutdown/restart functionality.
		rmcSampleSimpleWorld sampleRmc;
		sampleRmc.Init("$\\sample_simpleworld");
	
		sampleRmc.UpdateLoop();
	
		sampleRmc.Shutdown();
	}

	return 0;
}
