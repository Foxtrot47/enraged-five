// 
// sample_simpleworld/entitymgr.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "entitymgr.h"
#include "entitymgr_parser.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "bank/list.h"
#include "bank/slider.h"
#include "creature/creature.h"
#include "creature/component.h"
#include "creature/componentmover.h"
#include "creature/componentptfx.h"
#include "crextra/componentevent.h"
#include "crextra/expressions.h"
#include "crextra/mover.h"
#include "crmotiontree/complexplayer.h"
#include "crmotiontree/motiontree.h"
#include "crskeleton/skeletondata.h"
#include "crskeleton/skeleton.h"
#include "fragment/cache.h"
#include "fragment/drawable.h"
#include "fragment/tune.h"
#include "gizmo/manager.h"
#include "gizmo/rotation.h"
#include "gizmo/translation.h"
#include "grblendshapes/manager.h"
#include "crmotiontree/motiontreescheduler.h"
#include "parsercore/attribute.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "grprofile/drawmanager.h"
#include "rmcore/drawable.h"
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxeffectinst.h"
#include "grprofile/pix.h"
#include "system/timemgr.h"
#include "vector/colors.h"

using namespace rage;

namespace ragesamples
{

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

Matrix34 SimpleWorldEntity::s_TransformFromString;

SimpleWorldEntity::SimpleWorldEntity()
	: m_Identifier("")
    , m_pTransform(&m_Transform)
	, m_IsHidden(false)
    , m_bRotGizmoActive(false)
    , m_bPosGizmoActive(false)
    , m_UserData(0)
#if __BANK
    , m_pBankGroup(NULL)
    , m_pBankPosSlider(NULL)
	, m_pBankRotSlider(NULL)
    , m_pBankRemoveEntityButton(NULL)
    , m_pBankTextField(NULL)
    , m_pBankIsHidden(NULL)
    , m_pBankFrameEntity(NULL)
    , m_pBankIsRotGizmoActive(NULL)
    , m_pBankIsPosGizmoActive(NULL)
#endif
{
	m_Transform.Identity();
    
	m_XFormSphere.SetV4(Vec4V(V_ZERO));

    m_EntityAABB.Set(Vec3V(V_ZERO), Vec3V(V_ZERO));

	m_pRotGizmo = rage_new gzRotation(m_Transform, Functor0::NullFunctor(), MakeFunctor(*this, &SimpleWorldEntity::RotGizmoDraggedCallback));
	m_pRotGizmo->Deactivate();	

	m_pPosGizmo = rage_new gzTranslation(m_Transform, Functor0::NullFunctor(), MakeFunctor(*this, &SimpleWorldEntity::PosGizmoDraggedCallback));
	m_pPosGizmo->Deactivate();

	s_TransformFromString.Identity();

	m_Rotation = m_Transform.GetEulers();
	m_Rotation.Scale(RtoD);

    m_EntityNameChar[0] = 0;
    m_EntityFilePathChar[0] = 0;
    m_IdentifierChar[0] = 0;
}

SimpleWorldEntity::SimpleWorldEntity(const Matrix34& transform)
    : m_Identifier("")
    , m_Transform(transform)
    , m_pTransform(&m_Transform)
    , m_IsHidden(false)
    , m_bRotGizmoActive(false)
    , m_bPosGizmoActive(false)
    , m_UserData(0)
    , m_memoryUsed(0)
#if __BANK
    , m_pBankGroup(NULL)
    , m_pBankRemoveEntityButton(NULL)
    , m_pBankTextField(NULL)
    , m_pBankUserDataSlider(NULL)
    , m_pBankPosSlider(NULL)
	, m_pBankRotSlider(NULL)
    , m_pBankIsHidden(NULL)
    , m_pBankFrameEntity(NULL)
    , m_pBankIsRotGizmoActive(NULL)
    , m_pBankIsPosGizmoActive(NULL)
#endif
{
	m_XFormSphere.Set(RCC_VEC3V(m_Transform.d), ScalarV(V_ZERO));

    m_EntityAABB.Set(Vec3V(V_ZERO), Vec3V(V_ZERO));

	m_pRotGizmo = rage_new gzRotation(m_Transform);
	m_pRotGizmo->Deactivate();

	m_pPosGizmo = rage_new gzTranslation(m_Transform);
	m_pPosGizmo->Deactivate();

	s_TransformFromString.Identity();

	m_Rotation = m_Transform.GetEulers();
	m_Rotation.Scale(RtoD);

    m_EntityNameChar[0] = 0;
    m_EntityFilePathChar[0] = 0;
    m_IdentifierChar[0] = 0;
}

SimpleWorldEntity::~SimpleWorldEntity()
{
	if(m_pRotGizmo)
		delete m_pRotGizmo;
	if(m_pPosGizmo)
		delete m_pPosGizmo;
}

const char* SimpleWorldEntity::TransformToString( Matrix34 *pMatrix )
{
    static char str[256];
    sprintf( str, "%f %f %f %f %f %f %f %f %f %f %f %f",
        pMatrix->a.x, pMatrix->a.y, pMatrix->a.z,
        pMatrix->b.x, pMatrix->b.y, pMatrix->b.z,
        pMatrix->c.x, pMatrix->c.y, pMatrix->c.z,
        pMatrix->d.x, pMatrix->d.y, pMatrix->d.z );
    
    return str;
}

Matrix34* SimpleWorldEntity::StringToTransform( const char *pStr )
{
    float a0, a1, a2;
    float b0, b1, b2;
    float c0, c1, c2;
    float d0, d1, d2;
    sscanf( pStr, "%f %f %f %f %f %f %f %f %f %f %f %f",
        &a0, &a1, &a2,
        &b0, &b1, &b2,
        &c0, &c1, &c2,
        &d0, &d1, &d2 );
    
    s_TransformFromString.Identity();
    s_TransformFromString.Set( a0, a1, a2, b0, b1, b2, c0, c1, c2, d0, d1, d2 );

    return &s_TransformFromString;
}

bool SimpleWorldEntity::LoadEntityFile( const char* filePath, bool bIsFullAssetPath )
{
    m_memoryUsed = 0;
    int memoryAvailableBefore = sysMemAllocator::GetCurrent().GetMemoryAvailable();

    bool bRtn = false;

    atString assetFile;
    bool bPopFolder = SetAssetFile( assetFile, filePath, bIsFullAssetPath );

    // check for xml file
    const char *pExt = fiAssetManager::FindExtensionInPath( (const char *)assetFile );
    if ( strcmp(pExt,".xml") == 0 )
    {
        char path[256];
        fiAssetManager::RemoveExtensionFromPath( path, 256, (const char *)assetFile );

        parTree *pTree = PARSER.LoadTree( path, "xml" );
        if ( pTree == NULL )
        {
            Errorf( "SimpleWorldEntity LoadEntityFile failed: PARSER error on %s\n", (const char *)assetFile );
            bRtn = false;
        }
        else
        {
            bRtn = LoadEntityTreeNode( pTree->GetRoot() );
        }
    }
    else
    {
        // START - The following code has been left for compatibility

        //Set the entity name
        GetEntityNameFromPath( filePath, bIsFullAssetPath );

        // copy to id
        m_Identifier = m_EntityName;

        bRtn = Init((const char *)assetFile, !bPopFolder );
        // STOP
    }

    if ( bPopFolder )
    {
        ASSET.PopFolder();
    }

    if ( bRtn )
    {
        int memoryAvailableAfter = sysMemAllocator::GetCurrent().GetMemoryAvailable();
        m_memoryUsed = (memoryAvailableBefore - memoryAvailableAfter) / 1024;
        m_memoryUsed += (sysMemAllocator::GetCurrent().GetSize( this ) / 1024);
    }

    return bRtn;
}

bool SimpleWorldEntity::LoadEntityTreeNode( const parTreeNode *pTreeNode )
{
    m_memoryUsed = 0;
    int memoryAvailableBefore = sysMemAllocator::GetCurrent().GetMemoryAvailable();

    bool bPopFolder = false;
    bool bRtn = false;

    // old xml format
    const char *pNodeName = pTreeNode->GetElement().GetName();
    if ( strcmp(pNodeName,"entity") == 0 )
    {
        m_IdentifierChar[0] = 0;
        parAttribute *pAtt = const_cast<parTreeNode *>(pTreeNode)->GetElement().FindAttribute( "id" );
        if ( pAtt )
        {
            SetIdentifier( pAtt->GetStringValue() );
        }

        // entity to load
        const char *pEntityFile = NULL;
        parTreeNode *pNode = pTreeNode->FindChildWithName( "file" );
        if ( pNode )
        {
            pEntityFile = pNode->GetData();
        }
        else
        {
            Errorf( "SimpleWorldEntity LoadEntityTreeNode failed: we're remissing a file node\n" );
            return false;
        }

        // set entity name
        if ( pEntityFile )
        {
            GetEntityNameFromPath( pEntityFile );
        }

        // transformation
        m_Transform.Identity();
        pNode = pTreeNode->FindChildWithName( "matrix" );
        if ( pNode )
        {
            const char *pMatrixStr = pNode->GetData();
            if ( pMatrixStr )
            {
				m_Transform = *SimpleWorldEntity::StringToTransform(pMatrixStr);
            }
        }

        // userdata
        m_UserData = 0;
        pNode = pTreeNode->FindChildWithName( "userdata" );
        if ( pNode )
        {
            const char* pStr = pNode->GetData();
            if ( pStr )
            {
                sscanf( pStr, "%u", &m_UserData );
            }
        }

        atString assetFile;
        bPopFolder = SetAssetFile( assetFile, pEntityFile, true );

        bRtn = Init( (const char *)assetFile, !bPopFolder);
    }
    // new xml format
    else
    {
        if ( strcmp(pNodeName,parser_GetStructure()->GetName()) != 0 )
        {
            Errorf( "SimpleWorldEntity LoadEntityTreeNode failed: %s is not a new SimpleWorldEntity node\n", pNodeName );
            return false;
        }

        if ( !PARSER.LoadFromStructure(const_cast<parTreeNode *>(pTreeNode),*parser_GetStructure(),this) )
        {
            Errorf( "SimpleWorldDrawableEntity LoadEntityTreeNode failed: PARSER error\n" );
            return false;
        }
        
        atString assetFile;
        bPopFolder = SetAssetFile( assetFile, m_EntityFilePathChar, true );

        bRtn = Init( NULL, true );
    }

    if ( bPopFolder )
    {
        ASSET.PopFolder();
    }

    if ( bRtn )
    {
        int memoryAvailableAfter = sysMemAllocator::GetCurrent().GetMemoryAvailable();
        m_memoryUsed = (memoryAvailableBefore - memoryAvailableAfter) / 1024;
        m_memoryUsed += (sysMemAllocator::GetCurrent().GetSize( this ) / 1024);
    }

    return bRtn;
}

bool SimpleWorldEntity::LoadEntityFromStream( fiStream *pStream )
{
    m_memoryUsed = 0;
    int memoryAvailableBefore = sysMemAllocator::GetCurrent().GetMemoryAvailable();

    // get entity.type file path
    char entityPath[256];
    if ( !fgetline(entityPath,256,pStream) )
    {
        return false;
    }

    // set name
    GetEntityNameFromPath( entityPath, true );

    // copy to id
    m_Identifier = m_EntityName;

    // get transform
    char strMatrix[256];
    if ( !fgetline(strMatrix,256,pStream) )
    {
        Errorf( "LoadEntityFromStream error:  Missing transform for entity '%s'\n", entityPath );
        pStream->Close();
        return false;
    }

	m_Transform = *SimpleWorldEntity::StringToTransform(strMatrix);
	m_Rotation = m_Transform.GetEulers();
	m_Rotation.Scale(RtoD);

    // load
    atString assetFile;
    bool bPopFolder = SetAssetFile( assetFile, entityPath, true );

    bool bRtn = Init( (const char *)assetFile, !bPopFolder );

    if ( bPopFolder )
    {
        ASSET.PopFolder();
    }

    if ( bRtn )
    {
        int memoryAvailableAfter = sysMemAllocator::GetCurrent().GetMemoryAvailable();
        m_memoryUsed = (memoryAvailableBefore - memoryAvailableAfter) / 1024;
        m_memoryUsed += (sysMemAllocator::GetCurrent().GetSize( this ) / 1024);
    }

    return bRtn;
}

bool SimpleWorldEntity::SaveEntityFile( const char* filePath )
{
    // force to xml file to update to new save format
    
    char path[256];
    fiAssetManager::RemoveExtensionFromPath( path, 256, filePath );

    // kluge
    s_TransformFromString = m_Transform;

    bool bRtn = PARSER.SaveObject( path, "xml", this, parManager::XML );
    if ( !bRtn )
    {
        Errorf( "SimpleWorldEntity %s SaveEntityFile failed: PARSER error\n", (const char*)m_EntityName );
    }

    return bRtn;
}

bool SimpleWorldEntity::SaveEntityTreeNode( parTreeNode *pParentNode )
{
    // kluge
    s_TransformFromString = m_Transform;

    parTreeNode *pNode = PARSER.BuildTreeNode( this );
    if ( pNode )
    {
        pNode->AppendAsChildOf( pParentNode );
        return true;
    }
    else
    {
        Printf( "SimpleWorldEntity SaveEntityTreeNode error on %s\n", (const char *)m_EntityName );
        return false;
    }
}

bool SimpleWorldEntity::SaveEntityToStream( fiStream *pStream )
{
    char strBuffer[256];

    sprintf(strBuffer, "%s\r\n", (const char*)m_EntityFilePath );
    pStream->Write( strBuffer, strlen(strBuffer) );

    sprintf( strBuffer, "%f %f %f %f %f %f %f %f %f %f %f %f\r\n",
        m_Transform.a.x, m_Transform.a.y, m_Transform.a.z,
        m_Transform.b.x, m_Transform.b.y, m_Transform.b.z,
        m_Transform.c.x, m_Transform.c.y, m_Transform.c.z,
        m_Transform.d.x, m_Transform.d.y, m_Transform.d.z );
    pStream->Write( strBuffer, strlen(strBuffer) );

    return true;
}

bool SimpleWorldEntity::Init( const char *pEntityFile, bool bIsFullAssetPath )
{
    if ( pEntityFile == NULL )
    {
        // fixup our PARSER kluges
        m_EntityName = m_EntityNameChar;
        m_EntityFilePath = m_EntityFilePathChar;
        m_Identifier = m_IdentifierChar;
    }
    else
    {
        if ( bIsFullAssetPath )
        {
            m_EntityFilePath = pEntityFile;
        }
        else
        {
            char path[256];
            ASSET.GetStackedPath( path, 256 );
            m_EntityFilePath = path;
            m_EntityFilePath += pEntityFile;
        }

        safecpy( m_EntityNameChar, (const char *)m_EntityName, ENTITY_NAME_CHAR_LEN );
        safecpy( m_EntityFilePathChar, (const char *)m_EntityFilePath, ENTITY_FILE_PATH_CHAR_LEN );
        safecpy( m_IdentifierChar, (const char *)m_Identifier, IDENTIFIER_CHAR_LEN );
    }

    // another parser kluge
    m_Transform = SimpleWorldEntity::s_TransformFromString;
	m_Rotation = m_Transform.GetEulers();
	m_Rotation.Scale(RtoD);

    return InitDerived( (const char *)m_EntityFilePath, true );
}

void SimpleWorldEntity::GetEntityNameFromPath( const char *pPath, bool bIsFullAssetPath )
{
    if ( bIsFullAssetPath )
    {
		// Is it a type file?
		const char* ext = fiAssetManager::FindExtensionInPath(pPath);
		if(ext && (strcmpi(ext, ".type")==0))
		{
			// Given the full path to a type file, so....
			// ...Set the name of the entity based on the directory name the .type file is located in
			char tmp[256];
			ASSET.RemoveNameFromPath( tmp, 256, pPath );
			atString assetPath(tmp);
			int strCt = assetPath.GetLength();
			int i;
			for (i=(strCt-1); i>0 ; i--)
			{
				if( (assetPath[i] == '/') || (assetPath[i] == '\\' ) )
				{
					break;
				}
			}
	        
			if( i >= 0 )
			{
				m_EntityName.Set( assetPath, assetPath.GetLength(), i+1, -1 );
			}
		}
		else
		{
			// Not a type file, so use filename without extension
			char acBaseName[256];
			ASSET.BaseName(acBaseName, 256, pPath);
	        m_EntityName = ASSET.FileName(acBaseName);
		}
    }
    else
    {
        //Set the name of the entity to the file it was loaded from
        m_EntityName = pPath;
    }
}

bool SimpleWorldEntity::SetAssetFile( atString &assetFile, const char *pFilePath, bool bIsFullAssetPath )
{
    if ( bIsFullAssetPath )
    {
        char basePath[256];
        fiAssetManager::RemoveNameFromPath( basePath, 256, pFilePath );
        
        ASSET.PushFolder( basePath );
        assetFile = ASSET.FileName( pFilePath );
        return true;
    }
    else
    {
        assetFile = pFilePath;
        return false;
    }
}

void SimpleWorldEntity::RotGizmoDraggedCallback()
{
	m_Rotation = m_Transform.GetEulers();
	m_Rotation.Scale(RtoD);
}

#if __BANK

void SimpleWorldEntity::RotationChangedBankCallback()
{
	Vector3 radVec = m_Rotation;
	radVec.Scale(DtoR);
	m_Transform.FromEulersXYZ(radVec);
}

void SimpleWorldEntity::RemoveEntityBankCallback()
{
	SMPWORLDEMGR.RemoveEntity(this);
}

void SimpleWorldEntity::FrameEntityBankCallback()
{
	SMPWORLDEMGR.FrameCamera(m_XFormSphere);
}

void SimpleWorldEntity::ShowPosGizmoBankCallback()
{
	if(m_bPosGizmoActive)
	{
		m_pPosGizmo->Activate();
		const spdSphere &sp = GetXFormSphere();
		if(sp.GetRadiusf())
			m_pPosGizmo->SetSize(sp.GetRadiusf() * 1.5f);
		else
			m_pPosGizmo->SetSize(1.0f);
	}
	else
		m_pPosGizmo->Deactivate();
}

void SimpleWorldEntity::ShowRotGizmoBankCallback()
{
	if(m_bRotGizmoActive)
	{
		m_pRotGizmo->Activate();
		const spdSphere &sp = GetXFormSphere();
		if(sp.GetRadiusf())
			m_pRotGizmo->SetSize(sp.GetRadiusf() * 1.5f);
		else
			m_pRotGizmo->SetSize(1.0f);
	}
	else
		m_pRotGizmo->Deactivate();
}

void SimpleWorldEntity::IdentifierChangedBankCallback()
{
    m_Identifier = m_IdentifierChar;
}

void SimpleWorldEntity::AddWidgets(bkBank &bk)
{
	m_pBankGroup = bk.PushGroup(m_EntityName);
    {
        m_pBankRemoveEntityButton = bk.AddButton("Remove", datCallback(MFA(SimpleWorldEntity::RemoveEntityBankCallback),this));
        m_pBankTextField = bk.AddText( "Identifier", m_IdentifierChar, 64, datCallback(MFA(SimpleWorldEntity::IdentifierChangedBankCallback),this) );
        m_pBankUserDataSlider = bk.AddSlider( "User Data", &m_UserData, 0, 4294967295U, 1 );
		m_pBankPosSlider = bk.AddSlider("Position", &m_Transform.d, -10000.0f, 10000.0f, 0.1f, datCallback(MFA(SimpleWorldEntity::PositionChangedBankCallback),this));
		m_pBankRotSlider = bk.AddSlider("Rotation", &m_Rotation, -360.0f, 360.0f, 0.1f, datCallback(MFA(SimpleWorldEntity::RotationChangedBankCallback),this));
		m_pBankIsPosGizmoActive = bk.AddToggle("Show Position Gizmo", &m_bPosGizmoActive, datCallback(MFA(SimpleWorldEntity::ShowPosGizmoBankCallback),this));
		m_pBankIsRotGizmoActive = bk.AddToggle("Show Rotation Gizmo", &m_bRotGizmoActive, datCallback(MFA(SimpleWorldEntity::ShowRotGizmoBankCallback),this));
		m_pBankIsHidden = bk.AddToggle("Hidden", &m_IsHidden);
		m_pBankFrameEntity = bk.AddButton("Frame Entity", datCallback(MFA(SimpleWorldEntity::FrameEntityBankCallback), this));
		AddDerivedWidgets(bk);
    }
	bk.PopGroup();
}

void SimpleWorldEntity::RemoveWidgets(bkBank &bk)
{
	if(m_pBankRemoveEntityButton)
	{
		bk.Remove((bkWidget&)*m_pBankRemoveEntityButton);
		m_pBankRemoveEntityButton = NULL;
	}
    if (m_pBankTextField)
    {
        bk.Remove((bkWidget&)*m_pBankTextField);
        m_pBankTextField = NULL;
    }
    if ( m_pBankUserDataSlider )
    {
        bk.Remove((bkWidget&)*m_pBankUserDataSlider);
        m_pBankUserDataSlider = NULL;
    }
	if(m_pBankPosSlider)
	{
		bk.Remove((bkWidget&)*m_pBankPosSlider);
		m_pBankPosSlider = NULL;
	}
	if(m_pBankRotSlider)
	{
		bk.Remove((bkWidget&)*m_pBankRotSlider);
		m_pBankRotSlider = NULL;
	}
	if(m_pBankIsHidden)
	{
		bk.Remove((bkWidget&)*m_pBankIsHidden);
		m_pBankIsHidden = NULL;
	}
	if(m_pBankFrameEntity)
	{
		bk.Remove((bkWidget&)*m_pBankFrameEntity);
		m_pBankFrameEntity = NULL;
	}
	if(m_pBankIsRotGizmoActive)
	{
		bk.Remove((bkWidget&)*m_pBankIsRotGizmoActive);
		m_pBankIsRotGizmoActive = NULL;
	}
	if(m_pBankIsPosGizmoActive)
	{
		bk.Remove((bkWidget&)*m_pBankIsPosGizmoActive);
		m_pBankIsPosGizmoActive = NULL;
	}
	//Remove the group last
	if(m_pBankGroup)
	{
		bk.DeleteGroup(*m_pBankGroup);
		m_pBankGroup = NULL;
	}
	
}
#endif //__BANK

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void EntityMovable::AddToDrawList(const rage::fragShaft& shaft)
{
    SimpleWorldEntity *pEntity = reinterpret_cast<SimpleWorldEntity*>(m_Context);
    if ( shaft.IsVisible( GetBoundingSphere()) )
    {
        pEntity->AddToDrawList();
    }
}

const rage::spdSphere& EntityMovable::GetBoundingSphere() const
{
    SimpleWorldEntity *pEntity = reinterpret_cast<SimpleWorldEntity*>(m_Context);
    return pEntity->GetXFormSphere();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


SimpleWorldMovableEntity::SimpleWorldMovableEntity()
	:SimpleWorldEntity()
{
	//Create a handle for this entity in the world
	m_Movable = rage_new EntityMovable(this);
	Assert(m_Movable);
}

SimpleWorldMovableEntity::SimpleWorldMovableEntity(const Matrix34& transform)
	:SimpleWorldEntity(transform)
{
	//Create a handle for this entity in the world
	m_Movable = rage_new EntityMovable(this);
	Assert(m_Movable);
}

SimpleWorldMovableEntity::~SimpleWorldMovableEntity()
{
	//Remove the movable from the world
	SMPWORLDEMGR.RemoveMovable(m_Movable);

	//Delete the movable handle.
	delete m_Movable;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

SimpleWorldDrawableEntity::SimpleWorldDrawableEntity()
: m_pDrawable(NULL)
, m_pSkeleton(NULL)
, m_pBlendTargetManager(NULL)
, m_pCreature(NULL)
, m_pExpressions(NULL)
, m_pMatrixSet(NULL)
, m_pAnimMgr(NULL)
, m_lastAnimPlayMode(RvAnimMgr::ANIM_PAUSE)
, m_UpdateAnim(true)
, m_AnimId(0)
, m_ProfileDrawEnabled(false)
, m_bCastsShadow(true)
, m_bCastsCubeShadow(true)
, m_bForceLODLevel(false)
, m_ForceLODDistance(0.0f)
, m_MoverPosition(0.0f,0.0f,0.0f)
, m_MoverRotation(0.0f,0.0f,0.0f)
#if __BANK
, m_pBankForceLODLevel(NULL)
, m_pBankForceLODDistance(NULL)
, m_pBankCastsShadow(NULL)
, m_pBankUpdateAnim(NULL)
, m_pBankEnableProfileDraw(NULL)
, m_pBankShaderGroup(NULL)
#endif // __BANK
{
    m_pDrawable = rage_new rmcDrawable;
    
    m_pBlendTargetManager = rage_new grbTargetManager;
    m_pCreature = rage_new crCreature();
	m_pCreature->Init( RvAnimMgr::sm_FrameDataFactory, RvAnimMgr::sm_FrameAccelerator, m_pSkeleton );

	crCreatureComponentEvent* eventComponent = rage_new crCreatureComponentEvent(*m_pCreature);
	m_pCreature->AddComponent(*eventComponent);
	

    //Intialize the bounding sphere to its LOD 0 culling sphere
    Vector3 sphereCenter = m_pDrawable->GetLodGroup().GetCullSphere();
    sphereCenter.Add( m_Transform.d );
    m_XFormSphere.Set( RCC_VEC3V(sphereCenter), ScalarV(m_pDrawable->GetLodGroup().GetCullRadius()) );

    if ( m_pCreature && m_pSkeleton )
    {
        m_pAnimMgr = rage_new RvAnimMgr;
        m_pAnimMgr->Init( m_pCreature );
		m_pAnimMgr->SetResetMoverFtor( MakeFunctor(*this,&SimpleWorldDrawableEntity::ResetMover) );
    }
}

SimpleWorldDrawableEntity::SimpleWorldDrawableEntity( rmcDrawable* drawable, crSkeleton* skeleton, const Matrix34& transform )
: SimpleWorldMovableEntity(transform)
, m_pDrawable(drawable)
, m_pSkeleton(skeleton)
, m_pBlendTargetManager(NULL)
, m_pCreature(NULL)
, m_pMatrixSet(NULL)
, m_pAnimMgr(NULL)
, m_lastAnimPlayMode(RvAnimMgr::ANIM_PAUSE)
, m_UpdateAnim(true)
, m_AnimId(0)
, m_ProfileDrawEnabled(false)
, m_bCastsShadow(true)
, m_bCastsCubeShadow(true)
, m_bForceLODLevel(false)
, m_ForceLODDistance(0.0f)
#if __BANK
, m_pBankForceLODLevel(NULL)
, m_pBankForceLODDistance(NULL)
, m_pBankCastsShadow(NULL)
, m_pBankUpdateAnim(NULL)
, m_pBankEnableProfileDraw(NULL)
, m_pBankShaderGroup(NULL)
#endif // __BANK
{
    if ( m_pSkeleton )
    {
        m_pMatrixSet = grmMatrixSet::Create( *m_pSkeleton );
        m_pSkeleton->SetParentMtx( reinterpret_cast<Mat34V*>(&m_Transform) );
    }

    if ( m_pDrawable )
    {
        m_pBlendTargetManager = rage_new grbTargetManager;
        m_pCreature = rage_new crCreature();
		m_pCreature->Init( RvAnimMgr::sm_FrameDataFactory, RvAnimMgr::sm_FrameAccelerator, m_pSkeleton );

		crCreatureComponentEvent* eventComponent = rage_new crCreatureComponentEvent(*m_pCreature);
		m_pCreature->AddComponent(*eventComponent);

        //Intialize the bounding sphere to its LOD 0 culling sphere
        Vector3 sphereCenter = m_pDrawable->GetLodGroup().GetCullSphere();
        sphereCenter.Add( m_Transform.d );
        m_XFormSphere.Set( RCC_VEC3V(sphereCenter), ScalarV(m_pDrawable->GetLodGroup().GetCullRadius()) );
    }

    if ( !m_pCreature )
    {
		m_pCreature = rage_new crCreature();
		m_pCreature->Init( RvAnimMgr::sm_FrameDataFactory, RvAnimMgr::sm_FrameAccelerator, m_pSkeleton );
	}

    m_pAnimMgr = rage_new RvAnimMgr;
    m_pAnimMgr->Init( m_pCreature );
	m_pAnimMgr->SetResetMoverFtor( MakeFunctor(*this,&SimpleWorldDrawableEntity::ResetMover) );
}

SimpleWorldDrawableEntity::~SimpleWorldDrawableEntity()
{    
    if ( m_pAnimMgr )
    {
        delete m_pAnimMgr;
        m_pAnimMgr = NULL;
    }

    if ( m_pCreature )
    {
        delete m_pCreature;
        m_pCreature = NULL;
    }

    if ( m_pMatrixSet )
    {
        delete m_pMatrixSet;
        m_pMatrixSet = NULL;
    }

    if ( m_pSkeleton )
    {
        delete m_pSkeleton;
        m_pSkeleton = NULL;
    }

    if ( m_pDrawable )
    {
        delete m_pDrawable;
        m_pDrawable = NULL;
    }

	if ( m_pBlendTargetManager )
	{
		m_pBlendTargetManager->Release();
		m_pBlendTargetManager = NULL;
	}

	if (m_pExpressions)
	{
		m_pExpressions->Release();
		m_pExpressions = NULL;
	}
}

void SimpleWorldDrawableEntity::SetUpdateAnim( bool update )
{
    m_UpdateAnim = update;

    BankUpdateAnimCallback();
}

void SimpleWorldDrawableEntity::LoadAndInitialzeExpressions( const char* szFilePath )
{
	Assert(m_pCreature);

	m_pExpressions = crExpressions::AllocateAndLoad(szFilePath);
	if(!m_pExpressions)
	{
		Errorf("Failed to load expression data '%s'", szFilePath);
		return;
	}
	m_pExpressions->AddRef();
	m_pExpressions->InitializeCreature(*m_pCreature);

	crFrameData* frameData = rage_new crFrameData;
	m_pCreature->InitDofs(*frameData);

	m_pAnimMgr->GetFrameIn().Init(*frameData);
	m_pAnimMgr->GetFrameOut().Init(*frameData);

	frameData->Release();

	m_pAnimMgr->SetExpressions( m_pExpressions );
}

bool SimpleWorldDrawableEntity::AddToDrawList()
{
    Draw();
    return true;
}

void SimpleWorldDrawableEntity::ResetMover()
{
	if(m_pCreature)
	{
		crCreatureComponentMover* pMoverComp = static_cast<crCreatureComponentMover*>(m_pCreature->FindComponent(crCreatureComponent::kCreatureComponentTypeMover));
		if(pMoverComp)
		{
			Mat34V lastMoverMtx(V_IDENTITY);
			pMoverComp->SetLastMoverMtx(lastMoverMtx);
			pMoverComp->Reset(false);
		}
	}
}

void SimpleWorldDrawableEntity::Draw()
{
	if ( GetCreature() )
	{
		GetCreature()->PreDraw(!IsHidden());
	}

	if ( !IsHidden() )
    {
        if ( GetDrawable() )
        {
            // We only have a very limited number of time stamps on PS3, so we can't be so
            // liberal with sprinkling PIXBegin/PIXEnd calls everywhere
#if !__PPU
            PIXBegin(0, GetEntityName());
#endif // !__PPU

            if ( m_bForceLODLevel )
            {
                SMPWORLDEMGR.AddToDrawBucket( *GetDrawable(), &m_Transform, GetSkeleton(), m_pMatrixSet, m_ForceLODDistance );  
            }
            else
            {
                float camDist = SMPWORLDEMGR.DistanceFromCamera( m_Transform.d );
                SMPWORLDEMGR.AddToDrawBucket( *GetDrawable(), &m_Transform, GetSkeleton(), m_pMatrixSet, camDist );
            }

#if !__PPU
            PIXEnd();
#endif // !__PPU
        }

        if ( GetCreature() )
        {
            GetCreature()->Draw();
        }
    }
}

void SimpleWorldDrawableEntity::Update()
{
    // update the animation
    if ( GetAnimMgr() )
    {
        GetAnimMgr()->Update();

        m_AnimId = GetAnimMgr()->GetCurrentAnimationId();
    }

    if ( m_pMatrixSet && GetSkeleton() && GetDrawable() )
    {
        // update the matrix set
        m_pMatrixSet->Update( *GetSkeleton(), GetDrawable()->IsSkinned() );
    }

	if(m_pCreature)
	{
		crCreatureComponentMover* pMoverComp = static_cast<crCreatureComponentMover*>(m_pCreature->FindComponent(crCreatureComponent::kCreatureComponentTypeMover));
		if(pMoverComp)
		{
			Matrix34 lastMoverMtx(RCC_MATRIX34(pMoverComp->GetLastMoverMtx()));
			m_MoverRotation = lastMoverMtx.GetEulers();
			m_MoverRotation.Scale( RtoD ); //Convert the mover rotation into degrees
			m_MoverPosition = lastMoverMtx.d;
		}
	}

#if __BANK
	if( m_pAnimMgr && m_pAnimMgr->IsFrameInDirty())
	{
		m_pAnimMgr->CleanFrameIn();
		m_FrameInWidgets.ResetWidgets();
		UpdateFrameInOutWidgets();
	}
#endif //__BANK

    // Update the bounding sphere and the AABB box
	UpdateBoundingSphereAndAABB();
}

void	SimpleWorldEntity::UpdateBoundingSphereAndAABB()
{
    if ( GetDrawable() )
    {
        Vector3 sphereCenter = GetDrawable()->GetLodGroup().GetCullSphere();
        sphereCenter.Add( m_Transform.d );
        m_XFormSphere.Set( RCC_VEC3V(sphereCenter), ScalarV(GetDrawable()->GetLodGroup().GetCullRadius()) );

        // probe if there is AABB data in the file
        Vector3 min, max;
        GetDrawable()->GetLodGroup().GetBoundingBox( min, max );

        m_EntityAABB.Set( RCC_VEC3V(min), RCC_VEC3V(max) );
    }
}


void SimpleWorldDrawableEntity::Remove()
{
    if ( GetDrawable() )
    {
        rmcShadowList::GetInstance().RemoveShadowCastingObject( GetDrawable() );
    }
}

bool SimpleWorldDrawableEntity::InitDerived( const char *pEntityFile, bool /*bIsFullAssetPath*/ )
{
	// build up the drawable resourced name
	std::string name = ".";
	name += g_sysPlatform;
	name += "dr";

	if ( !strcmp( ASSET.FindExtensionInPath(pEntityFile), name.c_str() ) )
    {		
        atString resourceName(pEntityFile);
		resourceName.Set( pEntityFile, strlen( pEntityFile), 0, strlen( pEntityFile) - 4 );

		delete m_pDrawable;
        m_pDrawable = rmcDrawable::LoadResource( resourceName.c_str() );

        if ( !m_pDrawable )
        {
            Errorf( "Failed to load entity resource file : '%s'", pEntityFile );
            return false;
        }
    }
	else if ( !strcmp( ASSET.FindExtensionInPath(pEntityFile), ".dr" ) )
	{		
		atString resourceName(pEntityFile);
		resourceName.Set( pEntityFile, strlen( pEntityFile), 0, strlen( pEntityFile) - 3);

		delete m_pDrawable;
		m_pDrawable = rmcDrawable::LoadResource( resourceName.c_str() ); 

		if ( !m_pDrawable )
		{
			Errorf( "Failed to load entity resource file : '%s'", pEntityFile );
			return false;
		}
	}
    else
    {
        if ( m_pDrawable == NULL )
        {
            m_pDrawable = rage_new rmcDrawable;
        }

        //Load the drawable	
        if ( !m_pDrawable->Load( pEntityFile ) )
        {
            Errorf( "Failed to load entity type file : '%s'", pEntityFile );
            return false;
        }
    }

    // Load the skeleton
    crSkeletonData *skelData = GetDrawable()->GetSkeletonData();
    if ( skelData )
    {
        // (re)init the skeleton
        crSkeleton *pSkeleton = GetSkeleton();
        if ( pSkeleton )
        {
            pSkeleton->Shutdown();
        }
        else
        {
            m_pSkeleton = rage_new crSkeleton;
            pSkeleton = m_pSkeleton;
        }

        pSkeleton->Init( *skelData, NULL );        
        pSkeleton->SetParentMtx( reinterpret_cast<Mat34V*>(&m_Transform) );

        // (re) init the matrix set
        if ( m_pMatrixSet )
        {
            delete m_pMatrixSet;
        }

        m_pMatrixSet = grmMatrixSet::Create( *pSkeleton );

        // (re)init the creature
        crCreature *pCreature = GetCreature();
        if ( pCreature )
        {
            pCreature->Shutdown();
        }
        else
        {
            m_pCreature = rage_new crCreature;
            pCreature = m_pCreature;
        }
        // (re)init the blend target manager
        if ( m_pBlendTargetManager )
        {
            m_pBlendTargetManager->Release();
			m_pBlendTargetManager = NULL;
        }

        m_pBlendTargetManager = rage_new grbTargetManager;
        m_pBlendTargetManager->Load( pEntityFile, m_pDrawable );

        pCreature->Init( RvAnimMgr::sm_FrameDataFactory, RvAnimMgr::sm_FrameAccelerator, GetSkeleton(), reinterpret_cast<Mat34V*>(&m_Transform) );

        // (re)init the motion tree
        RvAnimMgr *pAnimMgr = GetAnimMgr();
        if ( pAnimMgr )
        {
            pAnimMgr->GetMotionTree()->Shutdown();
        }
        else
        {
            pAnimMgr = rage_new RvAnimMgr;
            m_pAnimMgr = pAnimMgr;
			m_pAnimMgr->SetResetMoverFtor( MakeFunctor(*this,&SimpleWorldDrawableEntity::ResetMover) );
        }

        pAnimMgr->Init( pCreature );
        pAnimMgr->SetCurrentAnimationId( m_AnimId );
		
		crFrameData* frameData = rage_new crFrameData;
		pCreature->InitDofs(*frameData);

		pAnimMgr->GetFrameIn().Init(*frameData);
		pAnimMgr->GetFrameOut().Init(*frameData);

		frameData->Release();

		crCreatureComponentEvent* eventComponent = rage_new crCreatureComponentEvent(*pCreature);
		m_pCreature->AddComponent(*eventComponent);
	}
    else
    {
        if ( m_pMatrixSet )
        {
            delete m_pMatrixSet;
            m_pMatrixSet = NULL;
        }

        if ( m_pSkeleton )
        {
            delete m_pSkeleton;
            m_pSkeleton = NULL;
        }

        if ( m_pCreature )
        {
            delete m_pCreature;
            m_pCreature = NULL;
        }

        if ( m_pAnimMgr )
        {
            delete m_pAnimMgr;
            m_pAnimMgr = NULL;
        }

		if (m_pBlendTargetManager)
		{
			m_pBlendTargetManager->Release();
			m_pBlendTargetManager = NULL;
		}
    }
    
    //Add the object as a shadow caster (TODO : This should be optional?)
    if ( m_bCastsShadow && GetDrawable() )
    {
        Assert( rmcShadowList::IsInstantiated() );
        rmcShadowList::GetInstance().AddShadowCastingObject( GetDrawable(), GetSkeleton(), &m_Transform, m_pMatrixSet );
    }

    //Set the Bounding Sphere
    Vector3 sphereCenter = GetDrawable()->GetLodGroup().GetCullSphere();
    sphereCenter.Add( m_Transform.d );
    m_XFormSphere.Set( RCC_VEC3V(sphereCenter), ScalarV(GetDrawable()->GetLodGroup().GetCullRadius()) );

    if ( GetSkeleton() )
    {
        Matrix34 pSkelRootMat;
		GetSkeleton()->GetGlobalMtx( 0, RC_MAT34V(pSkelRootMat) );
        Vec3V center = m_XFormSphere.GetCenter();
        center += RCC_VEC3V(pSkelRootMat.d);
        m_XFormSphere.Set( center, m_XFormSphere.GetRadius() );
    }

    return true;
}

#if __PFDRAW

void SimpleWorldDrawableEntity::ProfileDraw()
{
#if !__FINAL
		if ( GetCreature() && m_pAnimMgr && m_pAnimMgr->GetMoverDebugDraw() )
        {
			crCreatureComponentMover* pMoverComp = static_cast<crCreatureComponentMover*>(m_pCreature->FindComponent(crCreatureComponent::kCreatureComponentTypeMover));
			if(pMoverComp)
			{
				pMoverComp->DebugDraw();
			}	
			//GetCreature()->DebugDraw();
        }
#endif

    if ( m_ProfileDrawEnabled )
    {
        if ( GetDrawable() )
        {
            GetDrawable()->ProfileDraw( GetSkeleton(), &m_Transform );
        }

        if ( GetSkeleton() )
        {
            GetSkeleton()->ProfileDraw();
        }
    }
}

#endif // __PFDRAW

void SimpleWorldDrawableEntity::BankUpdateAnimCallback()
{
    if ( m_pAnimMgr )
    {
        if ( !m_UpdateAnim )
        {
            m_lastAnimPlayMode = m_pAnimMgr->GetPlayMode();
            m_pAnimMgr->SetPlayMode( RvAnimMgr::ANIM_PAUSE );
        }
        else
        {
            m_pAnimMgr->SetPlayMode( m_lastAnimPlayMode );
        }
    }
}

#if __BANK

void SimpleWorldDrawableEntity::RemoveWidgets( bkBank &bk )
{
    if ( m_pAnimMgr )
    {
        m_pAnimMgr->RemoveWidgets( bk );

        if(m_pBankUpdateAnim)
        {
            bk.Remove(*m_pBankUpdateAnim);
            m_pBankUpdateAnim = NULL;
        }
    }

#if USE_GRSHADOWMAP
    if(m_pBankCastsShadow)
    {
        bk.Remove(*m_pBankCastsShadow);
        m_pBankCastsShadow = NULL;
    }
#endif //USE_GRSHADOWMAP

    if(m_pBankEnableProfileDraw)
    {
        bk.Remove(*m_pBankEnableProfileDraw);
        m_pBankEnableProfileDraw = NULL;
    }

    if(m_pBankForceLODLevel)
    {
        bk.Remove(*m_pBankForceLODLevel);
        m_pBankForceLODLevel = NULL;
    }

    if(m_pBankForceLODDistance)
    {
        bk.Remove(*m_pBankForceLODDistance);
        m_pBankForceLODDistance = NULL;
    }

    if(m_pBankShaderGroup)
    {
        bk.DeleteGroup(*m_pBankShaderGroup);
    }

    //Call base class remove widgets after all the this derived classes widgets have been removed
    SimpleWorldMovableEntity::RemoveWidgets(bk);
}

void SimpleWorldDrawableEntity::AddDerivedWidgets( bkBank &bk )
{
	bk.AddSlider("Mover Position", &m_MoverPosition, -10000, 10000, 0.0001f);
	bk.AddSlider("Mover Rotation", &m_MoverRotation, -10000, 10000, 0.0001f);

    m_pBankForceLODLevel = bk.AddToggle("Force LOD Distance", &m_bForceLODLevel);
    m_pBankForceLODDistance = bk.AddSlider("LOD Distance", &m_ForceLODDistance, 0.0f, 10000.0f, 1.0f);

#if USE_GRSHADOWMAP
    m_pBankCastsShadow = bk.AddToggle("Cast Shadow", &m_bCastsShadow);
#endif //USE_GRSHADOWMAP

#if USE_GRCUBESHADOWMAP
    m_pBankCastsShadow = bk.AddToggle("Cast Cube Shadow", &m_bCastsCubeShadow);
#endif //USE_GRCUBESHADOWMAP

    m_pBankEnableProfileDraw = bk.AddToggle("Enable Profile Drawing", &m_ProfileDrawEnabled);

    if ( GetDrawable() )
    {
        grmShaderGroup& shaderGroup = GetDrawable()->GetShaderGroup();
        if(&shaderGroup)
        {
            m_pBankShaderGroup = bk.PushGroup("Shaders", false);
            GetDrawable()->GetShaderGroup().AddWidgets(bk);
            bk.PopGroup();
        }
    }

    if ( m_pAnimMgr )
    {
        bk.PushGroup( "Animation Control" );
        {
            m_pBankUpdateAnim = bk.AddToggle( "Update Animation", &m_UpdateAnim, 
                datCallback(MFA(SimpleWorldDrawableEntity::BankUpdateAnimCallback),this) );

            m_pAnimMgr->AddWidgets( bk );
        }
        bk.PopGroup();
    }

	if( m_pCreature && m_pSkeleton )
	{

		m_pBankCreatureGroup = bk.PushGroup("Creature", false);
		AddCreatureWidgets( bk );
		bk.PopGroup();
	}
}

void SimpleWorldDrawableEntity::UpdateFrameInOutWidgets()
{
	m_FrameInWidgets.UpdateFrame();
	m_FrameOutWidgets.ResetWidgets();
}

void SimpleWorldDrawableEntity::ResetFrameInOutWidgets()
{
	Assert(m_pCreature);

	m_pCreature->Identity(m_pAnimMgr->GetFrameIn());

	m_FrameInWidgets.ResetWidgets();
	if(m_pAnimMgr->GetAnimationById(m_AnimId))
	{
		m_pAnimMgr->GetFrameIn().Invalidate();
		m_FrameInWidgets.ResetWidgets();
	}
}

void SimpleWorldDrawableEntity::AddCreatureWidgets( bkBank &bk )
{
	bk.PushGroup("Input frame");
	m_FrameInWidgets.Init(m_pAnimMgr->GetFrameIn(), &m_pSkeleton->GetSkeletonData());
	m_FrameInWidgets.AddWidgets(bk);
	bk.PopGroup();

	bk.PushGroup("Output frame");
	m_FrameOutWidgets.Init(m_pAnimMgr->GetFrameOut(), &m_pSkeleton->GetSkeletonData());
	m_FrameOutWidgets.AddWidgets(bk);
	bk.PopGroup();

	ResetFrameInOutWidgets();
}

#endif // __BANK

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

SimpleWorldFragDrawableEntity::SimpleWorldFragDrawableEntity( phLevelNew *pLevelNew, phSimulator *pSim )
: SimpleWorldDrawableEntity()
, m_pLevelNew(pLevelNew)
, m_pSim(pSim)
, m_pInst(NULL)
, m_DrawBound(false) 
#if __BANK
, m_pBankDrawBoundToggle(NULL)
#endif
{
    Assert( pLevelNew );
    Assert( pSim );

	delete m_pDrawable;
    m_pDrawable = rage_new fragDrawable;

    m_pInst = rage_new phInst;
}

SimpleWorldFragDrawableEntity::SimpleWorldFragDrawableEntity( phLevelNew *pLevelNew, phSimulator *pSim,
                            fragDrawable* drawable, crSkeleton* skeleton, const Matrix34& transform )
: SimpleWorldDrawableEntity( drawable, skeleton, transform )
, m_pLevelNew(pLevelNew)
, m_pSim(pSim)
, m_pInst(NULL)
, m_DrawBound(false) 
#if __BANK
, m_pBankDrawBoundToggle(NULL)
#endif
{
    m_pInst = rage_new phInst;

    if ( GetDrawable() )
    {
        m_pInst->SetMatrix( RCC_MAT34V(SafeCast(fragDrawable, GetDrawable())->GetBoundMatrix()) );

        phArchetype *pArchetype = rage_new phArchetype;
        pArchetype->SetBound( SafeCast(fragDrawable, GetDrawable())->GetBound() );
        pArchetype->SetFilename( m_EntityFilePath );
        m_pInst->SetArchetype( pArchetype );

        m_pSim->AddFixedObject( m_pInst );
    }
}

SimpleWorldFragDrawableEntity::~SimpleWorldFragDrawableEntity()
{
    if ( m_pInst )
    {
        delete m_pInst;
        m_pInst = NULL;
    }
}

bool SimpleWorldFragDrawableEntity::InitDerived( const char *pEntityFile, bool bIsFullAssetPath )
{
    if ( SimpleWorldDrawableEntity::InitDerived( pEntityFile, bIsFullAssetPath ) )
    {
        if ( GetDrawable() )
        {
            phInst *pInst = GetInst();
            if ( pInst == NULL )
            {
                m_pInst = rage_new phInst;
                pInst = m_pInst;
            }

            pInst->SetMatrix( RCC_MAT34V(SafeCast(fragDrawable, GetDrawable())->GetBoundMatrix()) );

            phArchetype *pArchetype = rage_new phArchetype;
            pArchetype->SetBound( SafeCast(fragDrawable, GetDrawable())->GetBound() );
            pArchetype->SetFilename( m_EntityFilePath );
            pInst->SetArchetype( pArchetype );

            m_pSim->AddFixedObject( pInst );
        }

        return true;
    }
    return false;
}

void SimpleWorldFragDrawableEntity::Reset()
{
    PositionChangedBankCallback();
}

void SimpleWorldFragDrawableEntity::DrawBound()
{
#if __BANK && __PFDRAW
    if ( m_DrawBound && GetInst() )
    {
        grcBindTexture( NULL );
        grcColor( Color_white );
        GetInst()->GetArchetype()->GetBound()->Draw( GetInst()->GetMatrix(), false, false );
    }
#endif
}

void SimpleWorldFragDrawableEntity::Draw()
{
    SimpleWorldDrawableEntity::Draw();
    
    DrawBound();
}

void SimpleWorldFragDrawableEntity::Update()
{
    SimpleWorldDrawableEntity::Update();
}

void SimpleWorldFragDrawableEntity::Remove()
{
    if ( GetInst() && GetInst()->IsInLevel() )
    {
       m_pSim->DeleteObject( GetInst()->GetLevelIndex() );
    }

    SimpleWorldDrawableEntity::Remove();
}

#if __PFDRAW

void SimpleWorldFragDrawableEntity::ProfileDraw()
{
    SimpleWorldDrawableEntity::ProfileDraw();
}

#endif // __PFDRAW

void SimpleWorldFragDrawableEntity::PosGizmoDraggedCallback()
{
    PositionChangedBankCallback();
}

void SimpleWorldFragDrawableEntity::RotGizmoDraggedCallback()
{
    PositionChangedBankCallback();
}

void SimpleWorldFragDrawableEntity::PositionChangedBankCallback()
{
    if ( GetInst() )
    {
        Assert( m_pLevelNew );
        Matrix34 m;
        
        if ( GetDrawable() )
        {
            m.Dot( SafeCast(fragDrawable, GetDrawable())->GetBoundMatrix(), m_Transform );
            m.Normalize();
        }

        if ( GetInst() )
        {
            GetInst()->SetMatrix( RCC_MAT34V(m) );
            m_pLevelNew->UpdateObjectLocation( GetInst()->GetLevelIndex() );
            GetInst()->ReportMovedBySim();
        }
    }
}

#if __BANK

void SimpleWorldFragDrawableEntity::RemoveWidgets(bkBank &bk)
{
    if ( m_pBankDrawBoundToggle )
    {
        bk.Remove( *m_pBankDrawBoundToggle );
        m_pBankDrawBoundToggle = NULL;
    }

    //Call base class remove widgets after all the this derived classes widgets have been removed
    SimpleWorldDrawableEntity::RemoveWidgets( bk );
}

void SimpleWorldFragDrawableEntity::AddDerivedWidgets(bkBank &bk)
{
    m_pBankDrawBoundToggle = bk.AddToggle( "Draw Bound", &m_DrawBound );
    
    SimpleWorldDrawableEntity::AddDerivedWidgets( bk );
}

#endif // __BANK

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

SimpleWorldFragmentEntity::SimpleWorldFragmentEntity( phLevelNew *pLevelNew, phSimulator *pSim )
: SimpleWorldFragDrawableEntity( pLevelNew, pSim )
, m_pFragType(NULL)
, m_pMoverInst(NULL)
, m_startActive(false)
#if __BANK
, m_pBankActiveToggle(NULL)
#endif // __BANK
{
    // we know that m_pInst is going to be created, so let's replace it with our own
    if ( m_pInst )
    {
        delete m_pInst;
    }

    m_pInst = rage_new fragInst;

	delete m_pDrawable;
	m_pDrawable = NULL;
}

SimpleWorldFragmentEntity::SimpleWorldFragmentEntity( phLevelNew *pLevelNew, phSimulator *pSim, 
                           fragDrawable* drawable, crSkeleton* skeleton, const Matrix34& transform )
                           : SimpleWorldFragDrawableEntity( pLevelNew, pSim, drawable, skeleton, transform )
                           , m_pFragType(NULL)
                           , m_pMoverInst(NULL)
                           , m_startActive(false)
#if __BANK
                           , m_pBankActiveToggle(NULL)
#endif // __BANK
{
    // we know that m_pInst is going to be created, so let's replace it with our own
    if ( m_pInst )
    {
        if ( m_pInst->IsInLevel() )
        {
            m_pSim->DeleteObject( m_pInst->GetLevelIndex() );
        }

        delete m_pInst;
    }

    m_pInst = rage_new fragInst;

    // delete the drawable, we will be pulling it from the fragType
	delete m_pDrawable;
	m_pDrawable = NULL;
}

SimpleWorldFragmentEntity::~SimpleWorldFragmentEntity()
{
    if ( m_pFragType )
    {
        delete m_pFragType;
        m_pFragType = NULL;
    }

    if ( m_pMoverInst )
    {
        delete m_pMoverInst;
        m_pMoverInst = NULL;
    }
}

rmcDrawable* SimpleWorldFragmentEntity::GetDrawable() const
{
    if ( m_pFragType )
    {
        return m_pFragType->GetCommonDrawable();
    }

    return NULL;
}

void SimpleWorldFragmentEntity::Update()
{
    // check for attachment
    if ( SMPWORLDEMGR.GetAttachmentMgr().IsAttached( GetEntityName() ) )
    {
        return;
    }

    SimpleWorldFragDrawableEntity::Update();

    // do the physics stuff
    if ( GetInst() )
    {
        fragInst *pFragInst = dynamic_cast<fragInst *>( GetInst() );
        if ( !pFragInst->GetCached() )
        {
            pFragInst->PutIntoCache();
        }

        if ( GetSkeleton() && pFragInst->GetSkeleton() )
        {
            if ( m_pLevelNew->IsActive( pFragInst->GetLevelIndex() ) )
            {
                pFragInst->PoseSkeletonFromBounds();

				sysMemCpy(GetSkeleton()->GetObjectMtxs(), pFragInst->GetSkeleton()->GetObjectMtxs(), sizeof(Matrix34)*GetSkeleton()->GetBoneCount());

                m_Transform = RCC_MATRIX34( pFragInst->GetMatrix() );
                GetSkeleton()->InverseUpdate();
            }
            else
            {
                pFragInst->SetMatrix( RCC_MAT34V( m_Transform ) );

				sysMemCpy(pFragInst->GetSkeleton()->GetObjectMtxs(), GetSkeleton()->GetObjectMtxs(), sizeof(Matrix34)*GetSkeleton()->GetBoneCount());

                pFragInst->GetSkeleton()->InverseUpdate();

				//pFragInst->PoseBoundsFromSkeleton();
				
				if(pFragInst->IsInLevel())
                {
					m_pLevelNew->UpdateObjectLocation( pFragInst->GetLevelIndex() );
				}
            }
        }
    }
}

void SimpleWorldFragmentEntity::Draw()
{
	if ( GetCreature() )
	{
		GetCreature()->PreDraw(!IsHidden());
	}

	if ( !IsHidden() )
    {
        // don't draw the drawable, the physics system will do that

        if ( GetCreature() )
        {
            GetCreature()->Draw();
        }
    }

    DrawBound();
}

const spdSphere& SimpleWorldFragmentEntity::GetXFormSphere()
{
    if ( GetInst() )
    {
        fragInst *pFragInst = dynamic_cast<fragInst *>( GetInst() );
        return pFragInst->GetXFormSphere();
    }
    else
    {
        return m_XFormSphere;
    }
}

void SimpleWorldFragmentEntity::Reset()
{
    Assert( m_pLevelNew );
    Assert( m_pSim );

    if ( GetInst() )
    {
        dynamic_cast<fragInst *>( GetInst() )->Reset();

        int levelIndex = GetLevelIndex();
        if ( levelIndex != phInst::INVALID_INDEX )
        {
            if ( m_startActive )
            {
                // Make sure the object is active.
                if ( !m_pLevelNew->IsActive(levelIndex) )
                {
                    m_pSim->ActivateObject(levelIndex);
                }
            }
        }

		//TODO : Might need to store the original transform of the fragment entity at load time
		//and resore that transform instead of the identity here....
		m_Transform.Identity();
    }
}

void SimpleWorldFragmentEntity::Remove()
{
    if ( fragTune::IsInstantiated() && GetFragType() )
    {
        BANK_ONLY( FRAGTUNE->RemoveArchetypeWidgets( GetFragType() ) );
        BANK_ONLY( FRAGTUNE->RefreshTypeList() );
    }

    if ( GetInst() )
    {
        fragInst *pFragInst = dynamic_cast<fragInst *>( GetInst() );
        pFragInst->Remove();
    }

    // don't call the parent class function, call the grandparent
    SimpleWorldDrawableEntity::Remove();
}

#if __PFDRAW

void SimpleWorldFragmentEntity::ProfileDraw()
{
    if ( m_ProfileDrawEnabled )
    {
#if !__FINAL
        if ( GetCreature() )
        {
            GetCreature()->DebugDraw();
        }
#endif

        if ( GetFragType() && GetInst() )
        {
            fragInst *pFragInst = dynamic_cast<fragInst *>( GetInst() );
            GetFragType()->ProfileDraw( pFragInst->GetCurrentPhysicsLOD(), m_Transform, pFragInst->GetSkeleton() );
        }
    }
}

#endif // __PFDRAW

bool SimpleWorldFragmentEntity::InitDerived( const char *pEntityFile, bool /*bIsFullAssetPath*/ )
{
    fragType *pFragType = GetFragType();
    if ( pFragType != NULL )
    {
        delete pFragType;
    }

    m_pFragType = fragType::Load( pEntityFile );
    if ( m_pFragType )
    {
        phInst *pInst = GetInst();
        if ( pInst != NULL )
        {
            if ( m_pInst->IsInLevel() )
            {
                m_pSim->DeleteObject( m_pInst->GetLevelIndex() );
            }

            delete pInst;
        }

        pInst = rage_new fragInst( m_pFragType, m_Transform );
        m_pInst = pInst;
        
        fragInst *pFragInst = dynamic_cast<fragInst *>( pInst );

        pFragInst->Insert( false );
        pFragInst->SetManualSkeletonUpdate( true );

        if ( !pFragInst->GetCached() )
        {
            pFragInst->PutIntoCache();
        }

        if ( fragTune::IsInstantiated() )
        {
            BANK_ONLY( FRAGTUNE->RefreshTypeList() );
        }

        m_pMoverInst = rage_new crMoverInst();
        m_pMoverInst->Init( 0.35f, 1.85f, 0.2f, ORIGIN, 97.f, pFragInst );
        m_pMoverInst->SetMatrix( RCC_MAT34V( m_Transform ) );
        m_pSim->AddActiveObject( m_pMoverInst );

        if ( pFragInst->GetSkeleton() )
        {
            // (re)init the skeleton
            crSkeleton *pSkeleton = GetSkeleton();
            if ( pSkeleton )
            {
                pSkeleton->Shutdown();
            }
            else
            {
                m_pSkeleton = rage_new crSkeleton;
                pSkeleton = m_pSkeleton;
            }

            pSkeleton->Init( pFragInst->GetSkeleton()->GetSkeletonData() );
            pSkeleton->SetParentMtx( &pFragInst->GetMatrix() );

            // (re) init the matrix set
            if ( m_pMatrixSet )
            {
                delete m_pMatrixSet;
            }

            m_pMatrixSet = grmMatrixSet::Create( *pSkeleton );

            // (re)init the blend target manager
            if (m_pBlendTargetManager)
            {
                m_pBlendTargetManager->Release();
				m_pBlendTargetManager = NULL;
            }
            m_pBlendTargetManager = rage_new grbTargetManager;
            m_pBlendTargetManager->Load( pEntityFile, GetDrawable() );

            // (re)init the creature
            crCreature *pCreature = GetCreature();
            if ( pCreature )
            {
                pCreature->Shutdown();
            }
            else
            {
                m_pCreature = rage_new crCreature;
                pCreature = m_pCreature;
			}

            pCreature->Init( RvAnimMgr::sm_FrameDataFactory, RvAnimMgr::sm_FrameAccelerator, GetSkeleton() );

            // (re)init the motion tree
            RvAnimMgr *pAnimMgr = GetAnimMgr();
            if ( pAnimMgr )
            {
                pAnimMgr->GetMotionTree()->Shutdown();
            }
            else
            {
                pAnimMgr = rage_new RvAnimMgr;
                m_pAnimMgr = pAnimMgr;
            }

            pAnimMgr->Init( pCreature );
            pAnimMgr->SetCurrentAnimationId( m_AnimId );

			crCreatureComponentEvent* eventComponent = rage_new crCreatureComponentEvent(*pCreature);
			m_pCreature->AddComponent(*eventComponent);
        }
		Vector3 sphereCenter = GetDrawable()->GetLodGroup().GetCullSphere();
		sphereCenter.Add( m_Transform.d );
		m_XFormSphere.Set( RCC_VEC3V(sphereCenter),  ScalarV(GetDrawable()->GetLodGroup().GetCullRadius()) );	
        return true;
    }
    else
    {
        Warningf( "Unable to load fragType file '%s'", pEntityFile );
        return false;
    }
}

void SimpleWorldFragmentEntity::PosGizmoDraggedCallback()
{
    PositionChangedBankCallback();
}

void SimpleWorldFragmentEntity::RotGizmoDraggedCallback()
{
    PositionChangedBankCallback();
}

void SimpleWorldFragmentEntity::PositionChangedBankCallback()
{
    Assert( m_pLevelNew );

    if ( GetInst() )
    {
        fragInst *pFragInst = dynamic_cast<fragInst *>( GetInst() );
        pFragInst->SetResetMatrix( m_Transform );
        pFragInst->Reset();
    }
}

#if __BANK

void SimpleWorldFragmentEntity::RemoveWidgets( bkBank &bk )
{
    if ( m_pBankActiveToggle )
    {
        bk.Remove( (bkWidget &)*m_pBankActiveToggle );
        m_pBankActiveToggle = NULL;
    }

    SimpleWorldFragDrawableEntity::RemoveWidgets( bk );
}

void SimpleWorldFragmentEntity::AddDerivedWidgets( bkBank& bk )
{
    m_pBankActiveToggle = bk.AddToggle( "Start Active", &m_startActive, 
        datCallback(MFA(SimpleWorldFragmentEntity::InitialStateChangedBankCallback),this) );
    
    SimpleWorldFragDrawableEntity::AddDerivedWidgets( bk );
}

#endif //__BANK

void SimpleWorldFragmentEntity::InitialStateChangedBankCallback()
{
    int levelIndex = GetLevelIndex();
    if ( m_startActive )
    {
        if ( !m_pLevelNew->IsActive(levelIndex) )
        {
            m_pSim->ActivateObject( levelIndex );
        }
    }
    else
    {
        if ( m_pLevelNew->IsActive(levelIndex) )
        {
            m_pSim->DeactivateObject( levelIndex );
        }
    }
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

SimpleWorldRmptfxEmitter::SimpleWorldRmptfxEmitter(ptxEffectInst* emitter)
: m_pEmitter(emitter)
, m_pCreature(NULL)
, m_pComponentPtfx(NULL)
, m_pAnimMgr(NULL)
, m_lastAnimPlayMode(RvAnimMgr::ANIM_PAUSE)
, m_UpdateAnim(true)
, m_AnimId(0)
, m_ProfileDrawEnabled(false)
#if __BANK
, m_pBankStart(NULL)
, m_pBankStop(NULL)
, m_pBankEnableProfileDraw(NULL)
, m_pBankUpdateAnim(NULL)
#endif // __BANK
{
    //Set the xform sphere to the unit sphere, since we don't
    //know what the bounding sphere would be for the particle system.
    m_XFormSphere.Set(m_XFormSphere.GetCenter(), ScalarV(V_ONE));

    m_pCreature = rage_new crCreature();
    
    m_pComponentPtfx = rage_new crCreatureComponentParticleEffect( *m_pCreature, reinterpret_cast<Mat34V*>(&m_Transform) );
    m_pCreature->AddComponent( *m_pComponentPtfx );

    m_pAnimMgr = rage_new RvAnimMgr;
    m_pAnimMgr->Init( m_pCreature );

    m_memoryUsed = sysMemAllocator::GetCurrent().GetSize( this );
    if ( emitter != NULL )
    {
        m_memoryUsed += sysMemAllocator::GetCurrent().GetSize( emitter );
    }
}

SimpleWorldRmptfxEmitter::SimpleWorldRmptfxEmitter()
: m_pEmitter(NULL)
, m_pCreature(NULL)
, m_pComponentPtfx(NULL)
, m_pAnimMgr(NULL)
, m_lastAnimPlayMode(RvAnimMgr::ANIM_PAUSE)
, m_UpdateAnim(true)
, m_AnimId(0)
, m_ProfileDrawEnabled(false)
#if __BANK
, m_pBankStart(NULL)
, m_pBankStop(NULL)
, m_pBankEnableProfileDraw(NULL)
, m_pBankUpdateAnim(NULL)
#endif // __BANK
{
    //Set the xform sphere to the unit sphere, since we don't
    //know what the bounding sphere would be for the particle system.
	m_XFormSphere.Set(m_XFormSphere.GetCenter(), ScalarV(V_ONE));

    m_pCreature = rage_new crCreature();

    m_pComponentPtfx = rage_new crCreatureComponentParticleEffect( *m_pCreature, reinterpret_cast<Mat34V*>(&m_Transform) );
    m_pCreature->AddComponent( *m_pComponentPtfx );

    m_pAnimMgr = rage_new RvAnimMgr;
    m_pAnimMgr->Init( m_pCreature );
}

SimpleWorldRmptfxEmitter::~SimpleWorldRmptfxEmitter()
{
    if ( m_pAnimMgr )
    {
        delete m_pAnimMgr;
        m_pAnimMgr = NULL;
    }

    if ( m_pCreature )
    {
        delete m_pCreature;
        m_pCreature = NULL;
    }

    m_pComponentPtfx = NULL;

    if ( m_pEmitter )
    {
        m_pEmitter->Stop();
        m_pEmitter = NULL;
    }
}

void SimpleWorldRmptfxEmitter::SetUpdateAnim( bool update )
{
    m_UpdateAnim = update;

    BankUpdateAnimCallback();
}

void SimpleWorldRmptfxEmitter::Draw()
{
    if ( GetCreature() )
    {
        GetCreature()->PreDraw(true);
        GetCreature()->Draw();
    }
}

void SimpleWorldRmptfxEmitter::Update()
{
    if ( m_pComponentPtfx )
    {
        // update the parent matrix with the current transformation
        m_pComponentPtfx->SetParentMatrix( RCC_MAT34V(m_Transform) );
    }

    // update the animation
    if ( GetAnimMgr() )
    {
        GetAnimMgr()->Update();

        m_AnimId = GetAnimMgr()->GetCurrentAnimationId();
    }

    if ( m_pEmitter )
    {
        m_XFormSphere.Set( RCC_VEC3V(m_Transform.d), m_XFormSphere.GetRadius() );

        if ( m_pComponentPtfx )
        {
            // set the emitter's transformation and call Relocate() to update it immediately
            m_pEmitter->SetBaseMtx( m_pComponentPtfx->GetRootMatrix() );
            m_pEmitter->Relocate();

			int numParams = m_pComponentPtfx->GetNumEvolutionParameters();
            for ( int i = 0; i < numParams; ++i )
            {
                m_pEmitter->SetEvoValue( i, m_pComponentPtfx->GetEvolutionParameterValueByIndex( i ) );
            }
        }
        else
        {            
            m_pEmitter->SetBaseMtx( RCC_MAT34V(m_Transform) );            
        }
    }
}

#if __PFDRAW

void SimpleWorldRmptfxEmitter::ProfileDraw()
{
    if ( m_ProfileDrawEnabled )
    {
#if !__FINAL
        if ( GetCreature() )
        {
            GetCreature()->DebugDraw();
        }
#endif
    }
}

#endif // __PFDRAW

void SimpleWorldRmptfxEmitter::BankUpdateAnimCallback()
{
    if ( m_pAnimMgr )
    {
        if ( !m_UpdateAnim )
        {
            m_lastAnimPlayMode = m_pAnimMgr->GetPlayMode();
            m_pAnimMgr->SetPlayMode( RvAnimMgr::ANIM_PAUSE );
        }
        else
        {
            m_pAnimMgr->SetPlayMode( m_lastAnimPlayMode );
        }
    }
}

#if __BANK

void SimpleWorldRmptfxEmitter::AddDerivedWidgets(bkBank &bk)
{
    m_pBankStart = bk.AddButton( "Start", datCallback(MFA(SimpleWorldRmptfxEmitter::StartEmitterBankCbk),this) );
    m_pBankStop = bk.AddButton( "Stop", datCallback(MFA(SimpleWorldRmptfxEmitter::StopEmitterBankCbk),this) );
    m_pBankEnableProfileDraw = bk.AddToggle( "Enable Profile Drawing", &m_ProfileDrawEnabled );

    if ( m_pAnimMgr )
    {
        bk.PushGroup( "Animation Control" );
        {
            m_pBankUpdateAnim = bk.AddToggle( "Update Animation", &m_UpdateAnim, 
                datCallback(MFA(SimpleWorldRmptfxEmitter::BankUpdateAnimCallback),this) );

            m_pAnimMgr->AddWidgets( bk );
        }
        bk.PopGroup();
    }
}

void SimpleWorldRmptfxEmitter::RemoveWidgets( bkBank &bk )
{
    if ( m_pBankStart )
    {
        bk.Remove( (bkWidget&)*m_pBankStart );
        m_pBankStart = NULL;
    }
    
    if ( m_pBankStop )
    {
        bk.Remove( (bkWidget&)*m_pBankStop );
        m_pBankStop = NULL;
    }

    if ( m_pBankEnableProfileDraw )
    {
        bk.Remove( *m_pBankEnableProfileDraw );
        m_pBankEnableProfileDraw = NULL;
    }

    if ( m_pAnimMgr )
    {
        m_pAnimMgr->RemoveWidgets( bk );

        if ( m_pBankUpdateAnim )
        {
            bk.Remove( *m_pBankUpdateAnim );
            m_pBankUpdateAnim = NULL;
        }
    }

    //Call base class remove widgets after all the this derived classes widgets have been removed
    SimpleWorldMovableEntity::RemoveWidgets( bk );
}

void SimpleWorldRmptfxEmitter::StartEmitterBankCbk()
{
    if ( m_pEmitter )
    {
        m_pEmitter->Start();
    }
}

void SimpleWorldRmptfxEmitter::StopEmitterBankCbk()
{
    if ( m_pEmitter )
    {
        m_pEmitter->Stop();
    }
}

#endif // __BANK

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

SimpleWorldAttachmentMgr::AttachData::AttachData(SimpleWorldDrawableEntity *pParentEntity, SimpleWorldFragmentEntity *pAttachedEntity, const char *pParentBoneName, const char *pAttachedLocName) :
m_pParent(pParentEntity),
m_pChild(pAttachedEntity),
m_ParentBoneName(pParentBoneName),
m_AttachedLocName(pAttachedLocName)
{
#if __BANK
    m_bOverrideLocName = false;
    m_OverrideLocName[0] = 0;
#endif
}

SimpleWorldAttachmentMgr::AttachData::~AttachData() {}

#if __BANK

void SimpleWorldAttachmentMgr::AttachData::AddWidgets(bkBank &b)
{
    b.PushGroup(m_pParent ? m_pChild->GetEntityName().c_str() : "?");
    b.AddToggle("OverrideLoc", &m_bOverrideLocName);
    b.AddText("OverrideLocName", m_OverrideLocName, 128);
    b.PopGroup();
}

#endif // _BANK

SimpleWorldAttachmentMgr::SimpleWorldAttachmentMgr()
{
#if __BANK
    m_bDrawAttachmentBones = false;
    m_bDrawAttachmentLocators = false;
#endif
}

SimpleWorldAttachmentMgr::~SimpleWorldAttachmentMgr()
{
    for( int i=0; i<m_Attachments.GetCount(); ++i )
    {
#if __BANK
        // TODO:
        //		if (m_pBank)
        //			pEntity->RemoveWidgets(*m_pBank);
#endif //__BANK
        delete m_Attachments[i];
    }
}

void SimpleWorldAttachmentMgr::Attach(const char *pParentEntityName, const char *pAttachedEntityName, const char *pParentBoneName, const char *pAttachedLocName)
{
    SimpleWorldDrawableEntity *pParent = dynamic_cast<SimpleWorldDrawableEntity*>(SMPWORLDEMGR.GetEntity(pParentEntityName));
    SimpleWorldFragmentEntity *pChild = dynamic_cast<SimpleWorldFragmentEntity*>(SMPWORLDEMGR.GetEntity(pAttachedEntityName));

    if( pParent && pChild )
    {
        SimpleWorldAttachmentMgr::AttachData *pAD = rage_new AttachData(pParent, pChild, pParentBoneName, pAttachedLocName);
        m_Attachments.PushAndGrow(pAD);
        pChild->GetAnimMgr()->SetCurrentAnimationId( 0 );
        pChild->SetUpdateAnim( false );
    }
    else
    {
        if( !pParent )
            Errorf("Couldn't find parent entity %s", pParentEntityName);
        if( !pChild )
            Errorf("Couldn't find attached entity %s", pAttachedEntityName);
    }
}

void SimpleWorldAttachmentMgr::Detach(const char *pParentEntityName, const char *pAttachedEntityName)
{
    for( int i=0; i<m_Attachments.GetCount(); ++i )
    {
        if( !strcmp(m_Attachments[i]->m_pParent->GetEntityName(), pParentEntityName) &&
            !strcmp(m_Attachments[i]->m_pChild->GetEntityName(), pAttachedEntityName) )
        {
            delete m_Attachments[i];
            m_Attachments.Delete(i);
            break;
        }
    }
}

bool SimpleWorldAttachmentMgr::IsAttached(const char *pParentEntityName, const char *pAttachedEntityName) const
{
    for( int i=0; i<m_Attachments.GetCount(); ++i )
    {
        if( !strcmp(m_Attachments[i]->m_pParent->GetEntityName(), pParentEntityName) &&
            !strcmp(m_Attachments[i]->m_pChild->GetEntityName(), pAttachedEntityName) )
        {
            return true;
        }
    }
    return false;
}

bool SimpleWorldAttachmentMgr::IsAttached(const char *pAttachedEntityName) const
{
    for( int i=0; i<m_Attachments.GetCount(); ++i )
    {
        if( !strcmp(m_Attachments[i]->m_pChild->GetEntityName(), pAttachedEntityName) )
        {
            return true;
        }
    }
    return false;
}

void SimpleWorldAttachmentMgr::Update()
{
    for( int i=0; i<m_Attachments.GetCount(); ++i )
    {
        if( m_Attachments[i] )
            UpdateAttachment(*m_Attachments[i]);
    }
}

void SimpleWorldAttachmentMgr::UpdateAttachment(AttachData &ad)
{
    if( !ad.m_pParent || !ad.m_pParent->GetSkeleton() )
        return;

    crSkeleton *pSkel = ad.m_pParent->GetSkeleton();
    const crBoneData *bd = pSkel->GetSkeletonData().FindBoneData(ad.m_ParentBoneName.m_String);
    if( !bd )
        return;
    Matrix34 attachMtx;
	pSkel->GetGlobalMtx(bd->GetIndex(), RC_MAT34V(attachMtx));

    if( ad.GetLocName() )
    {
        fragDrawable *pFD = dynamic_cast<fragDrawable*>(ad.m_pChild->GetDrawable());
        if( pFD )
        {
            const fragDrawable::Locator *pLoc = pFD->GetLocator(ad.GetLocName());
            if( pLoc )
            {
                Matrix34 locMtx(M34_IDENTITY);
                locMtx.FromEulersXYZ(pLoc->Eulers);
                locMtx.d.Set(pLoc->Offset);
                locMtx.FastInverse();
                attachMtx.DotFromLeft(locMtx);
            }
        }
    }

    ad.m_pChild->SetTransform(attachMtx);
}

#if __BANK
void SimpleWorldAttachmentMgr::AddWidgets()
{
    bkBank &b = BANKMGR.CreateBank("Attachment Mgr");
    b.PushGroup("Draw", false);
    b.AddToggle("Attachment Bones", &m_bDrawAttachmentBones);
    b.AddToggle("Attachment Locators", &m_bDrawAttachmentLocators);
    b.PopGroup();
    b.PushGroup("Attachments", false);
    for( int i=0; i<m_Attachments.GetCount(); ++i )
    {
        if( m_Attachments[i] )
            m_Attachments[i]->AddWidgets(b);
    }
    b.PopGroup();
}
#endif

#if __BANK
void SimpleWorldAttachmentMgr::DebugDraw()
{
    grcBindTexture( NULL );
    grcColor( Color_white );

    for( int i=0; i<m_Attachments.GetCount(); ++i )
    {
        if( m_Attachments[i] )
        {
            AttachData &ad = *m_Attachments[i];
            if( m_bDrawAttachmentBones )
            {
                if( !ad.m_pParent || !ad.m_pParent->GetSkeleton() )
                    return;

                crSkeleton *pSkel = ad.m_pParent->GetSkeleton();
                const crBoneData *bd = pSkel->GetSkeletonData().FindBoneData(ad.m_ParentBoneName.m_String);
                if( !bd )
                    return;
                Mat34V attachMtx;
				pSkel->GetGlobalMtx(bd->GetIndex(), attachMtx);

                grcDrawAxis(0.25f, attachMtx);
            }
        }
    }
}
#endif

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

SimpleWorldEntityMgr::SimpleWorldEntityMgr()
{
    //Initlize the functors to dummy functors
    m_InsertMovable = InsertMovableFunctor::NullFunctor();
    m_RemoveMovable = RemoveMovableFunctor::NullFunctor();
    m_UpdateMovable = UpdateMovableFunctor::NullFunctor();
    m_AddToDrawBucketFtor = MakeFunctorRet(ReturnFalse);
    m_FrameCameraFtor = MakeFunctor(NullCameraFrame);
    m_DistanceFromCameraFtor = MakeFunctorRet(ReturnZeroDistance);
    m_RemoveEntityFtor = RemoveEntityFunctor::NullFunctor();

#if __BANK
    m_pBank				= NULL;
    m_pBankUnHideAll	= NULL;
#endif //__BANK
}

SimpleWorldEntityMgr::~SimpleWorldEntityMgr()
{
    Reset();
}

#if __BANK

void SimpleWorldEntityMgr::UnhideAllBankCallback()
{
    int entityCount = GetEntityCount();
    for(int i=0; i<entityCount; i++)
    {
        SimpleWorldEntity* pCurEntity = GetEntity(i);
        pCurEntity->SetHidden(false);
    }
}

void SimpleWorldEntityMgr::InitBank()
{
    if(m_pBank)
        BANKMGR.DestroyBank(*m_pBank);

    m_pBank = &(BANKMGR.CreateBank(SMPWORLDEMGR_BANK));

    m_pBankUnHideAll = m_pBank->AddButton("Unhide All", datCallback(MFA(SimpleWorldEntityMgr::UnhideAllBankCallback),this));

    //Add widgets for any entities that have already been loaded from the command line
    int entityCount = GetEntityCount();
    for(int i=0; i<entityCount; i++)
    {
        SimpleWorldEntity *pEntity = GetEntity(i);
        if (m_pBank)
            pEntity->AddWidgets(*m_pBank);
    }
}
#endif //__BANK

void SimpleWorldEntityMgr::InsertEntity(SimpleWorldEntity *pEntity, bool bInsertMovable)
{
    //Make sure we have a default identifier
    if ( pEntity->GetIdentifier() == "" )
    {
        char id[8];
        sprintf( id, "%d", GetEntityCount() );
        pEntity->SetIdentifier( id );
    }

    //Add the entity to the manager entity table
    m_EntityTable.PushAndGrow(pEntity);

    //Add the entity to the world
    if(bInsertMovable)
    {
        SimpleWorldMovableEntity *pMovableEntity = dynamic_cast<SimpleWorldMovableEntity*>(pEntity);
        if(pMovableEntity)
            InsertMovable(pMovableEntity->GetMovable());
    }

#if __BANK
    if(m_pBank)
        pEntity->AddWidgets(*m_pBank);
#endif //__BANK
}

void SimpleWorldEntityMgr::RemoveEntity(int entityIdx)
{
    Assert(entityIdx < m_EntityTable.GetCount());

    SimpleWorldEntity *pEntity = m_EntityTable[entityIdx];
    pEntity->Remove();

#if __BANK
    if (m_pBank)
        pEntity->RemoveWidgets(*m_pBank);
#endif //__BANK	

    if ( m_RemoveEntityFtor != RemoveEntityFunctor::NullFunctor() )
    {
        m_RemoveEntityFtor( pEntity );
    }

    delete pEntity;
    m_EntityTable.Delete(entityIdx);
}

bool SimpleWorldEntityMgr::RemoveEntity(SimpleWorldEntity* pEntity)
{
    int entityCount = GetEntityCount();
    for(int i=0; i<entityCount; i++)
    {
        SimpleWorldEntity* pCurEntity = GetEntity(i);
        if(pCurEntity == pEntity)
        {
            RemoveEntity(i);
            return true;
        }
    }
    return false;
}

bool SimpleWorldEntityMgr::RemoveEntity(const char *entityId)
{
    SimpleWorldEntity *pEntity = GetEntity( entityId );
    if ( pEntity )
    {
        return RemoveEntity( pEntity );
    }
    return false;
}

void SimpleWorldEntityMgr::Update()
{
    int entityCount = GetEntityCount();
    for(int i=0; i<entityCount; i++)
    {
        SimpleWorldEntity* pEntity = GetEntity(i);
        pEntity->Update();
    }

    m_AttachmentMgr.Update();
}

void SimpleWorldEntityMgr::Reset()
{
    int entityCount = GetEntityCount();
    for(int i=0; i<entityCount; i++)
    {
        SimpleWorldEntity* pEntity = GetEntity(i);
        pEntity->Remove();
#if __BANK
        if (m_pBank)
            pEntity->RemoveWidgets(*m_pBank);
#endif //__BANK
        delete pEntity;
    }
    m_EntityTable.Reset();

    rmcShadowList::GetInstance().RemoveShadowCastingObjects();
}

void SimpleWorldEntityMgr::GetShadowCastingEntities(atArray<SimpleWorldEntity*>& retEntities)
{
    int entityCount = GetEntityCount();
    for(int i=0; i<entityCount; i++)
    {
        SimpleWorldEntity* pEntity = GetEntity(i);
        if(pEntity->DoesCastShadow())
            retEntities.PushAndGrow(pEntity);
    }
}

#if __PFDRAW
void SimpleWorldEntityMgr::ProfileDraw()
{
    int entityCount = GetEntityCount();
    for(int i=0; i<entityCount; i++)
    {
        SimpleWorldEntity* pEntity = GetEntity(i);
        pEntity->ProfileDraw();
    }
}
#endif //__PFDRAW

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

} //End namespace ragesamples
