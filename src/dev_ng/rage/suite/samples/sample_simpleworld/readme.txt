Here is the command line you need for the example.

-rageshaderpath T:\rage\assets -hdr -shaderpath T:\rage\assets

we need a specific rage shader path so that the program finds rage_postfx.fx and the shader path is there that the program can find the shaders of the asset you load. The -hdr switch switches on/off hdr rendering.

Please note that Depth of Field only works with models that have a DOF capable shader applied to. The only DOF capable shader in rage is now

rage_diffuse_ppp.fx

Have fun,
- Wolf
