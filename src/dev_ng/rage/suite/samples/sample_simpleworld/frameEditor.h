// 
// sample_simpleworld/frameEditor.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_SIMPLEWORLD_FRAMEEDITOR_H
#define SAMPLE_SIMPLEWORLD_FRAMEEDITOR_H

#include "atl/array.h"
#include "vector/vector3.h"

using namespace rage;

namespace rage
{
	class crFame;
	class crSkeletonData;
}

#if __BANK

//-----------------------------------------------------------------------------
//PURPOSE : Utility class for managing a set of animation frame widgets
class FrameEditorWidgets
{
public:
	FrameEditorWidgets();
	~FrameEditorWidgets();

	void Init(crFrame& frame, const crSkeletonData* pSkelData);

	void AddWidgets(bkBank& bk);

	void ResetWidgets();

	void UpdateFrame();

private:
	crFrame*	m_pFrame;
	const crSkeletonData*	m_pSkelData;

	atArray<Vector3>	m_WidgetValues;
	atArray<bool>		m_WidgetInvalid;
};
//-----------------------------------------------------------------------------

#endif //__BANK

#endif //SAMPLE_SIMPLEWORLD_FRAMEEDITOR_H