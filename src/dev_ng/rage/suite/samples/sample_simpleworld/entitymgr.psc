<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<structdef autoregister="true" constructable="false" type="ragesamples::SimpleWorldEntity" version="1">
    <string hideWidgets="true" name="m_EntityNameChar" size="64" type="member"/>
    <string hideWidgets="true" name="m_EntityFilePathChar" size="256" type="member"/>
    <string name="m_IdentifierChar" size="64" type="member"/>
    <pointer fromString="ragesamples::SimpleWorldEntity::StringToTransform" hideWidgets="true" name="m_pTransform" policy="external_named" toString="ragesamples::SimpleWorldEntity::TransformToString" type="rage::Matrix34"/>    
    <bool init="true" name="m_IsHidden"/>
    <bool init="false" name="m_bRotGizmoActive"/>
    <bool init="false" name="m_bPosGizmoActive"/>
    <u32 init="0" min="0" name="m_UserData"/>
</structdef>

<structdef autoregister="true" base="ragesamples::SimpleWorldEntity" constructable="false" type="ragesamples::SimpleWorldMovableEntity">
</structdef>

<structdef autoregister="true" base="ragesamples::SimpleWorldMovableEntity" onPostLoad="PostLoad" type="ragesamples::SimpleWorldDrawableEntity">
    <bool init="false" name="m_ProfileDrawEnabled"/>
    <bool init="true" name="m_bCastsShadow"/>
    <bool init="true" name="m_bCastsCubeShadow"/>
    <bool init="true" name="m_UpdateAnim"/>
    <int init="0" name="m_AnimId"/>
    <bool name="m_bForceLODLevel"/>
    <float init="0.000000f" max="10000.000000f" min="0.000000f" name="m_ForceLODDistance" step="1.000000f"/>
</structdef>

<structdef autoregister="true" base="ragesamples::SimpleWorldDrawableEntity" type="ragesamples::SimpleWorldFragDrawableEntity">
    <bool init="false" name="m_DrawBound"/>
</structdef>

<structdef autoregister="true" base="ragesamples::SimpleWorldFragDrawableEntity" type="ragesamples::SimpleWorldFragmentEntity">
    <bool init="false" name="m_startActive"/>
</structdef>

<structdef autoregister="true" base="ragesamples::SimpleWorldMovableEntity" type="ragesamples::SimpleWorldRmptfxEmitter">
    <bool init="false" name="m_ProfileDrawEnabled"/>
    <bool init="true" name="m_UpdateAnim"/>
    <int init="0" name="m_AnimId"/>
</structdef>

</ParserSchema>