#include "../../../base/src/shaderlib/rage_samplers.fxh"
#include "../../../base/src/shaderlib/rage_common.fxh"
/*
struct	VS_INPUT_SKINNED
{
	float4 Position		: DX_POS;
	//float4 Weights		: BLENDWEIGHT;
	//index4 Indices		: BLENDINDICES;	
	float4 diffuse0		: COLOR0;
	float4 diffuse1		: COLOR1;
	float2 texCoord0	: TEXCOORD0;
	float2 texCoord1	: TEXCOORD1;
	float3 normal		: NORMAL;
	float4 tangent		: TANGENT;
};
*/
struct VS_INPUT
{
    float3 pos			: POSITION;
	float4 diffuse0		: COLOR0;
	float4 diffuse1		: COLOR1;
    float2 texCoord0	: TEXCOORD0;
    float2 texCoord1	: TEXCOORD1;
	//float2 texCoord2	: TEXCOORD2;
    //float2 texCoord3	: TEXCOORD3;
	//float2 texCoord4	: TEXCOORD4;
    //float2 texCoord5	: TEXCOORD5;
	//float2 texCoord6	: TEXCOORD6;
    float3 normal		: NORMAL;
	float3 tangent		: TANGENT;
};
struct VS_OUTPUT
{
	float4 pos				: DX_POS;
	float4 color0			: COLOR0;
	float4 color1			: COLOR1;
	float2 tex0				: TEXCOORD0;
	float2 tex1				: TEXCOORD1;
	//float2 tex2			: TEXCOORD2;
	//float2 tex3			: TEXCOORD3;
	//float2 tex4			: TEXCOORD4;
	float3 pos0				: TEXCOORD5;
	float3 normal			: TEXCOORD6;

};
/*
VS_OUTPUT VSSkinned(VS_INPUTS IN){
	rageSkinMtx	BoneMtx = ComputeSkinMtx(IN.Indices, IN.Weights);
	float3 Pos = rageSkinTransform(IN.Position.xyz, BoneMtx);
	float4 Position = mul(float4(Pos, 1.0), gWorldViewProj);
	float3 WorldPosition = mul(float4(Pos, 1.0), gWorld).xyz;

	float3 skinnedNorm = rageSkinRotate(IN.Normal, BoneMtx);
	float3 WorldNormal = normalize(mul(skinnedNorm, (float3x3)gWorld));
	float3 skinnedTangent = rageSkinRotate(IN.Tangent.xyz, BoneMtx);
	float4 WorldTangent = float4(normalize(mul(skinnedTangent, (float3x3)gWorld)), IN.Tangent.w);
	float3 WorldBinormal = rageComputeBinormal(IN.Normal, IN.Tangent);
	WorldBinormal = rageSkinRotate(WorldBinormal, BoneMtx);
	WorldBinormal = normalize(mul(WorldBinormal, (float3x3)gWorld));

	return VS_Common(Position, WorldPosition, WorldNormal, IN.TexCoord0, WorldTangent, WorldBinormal, IN.Diffuse);
}
*/
VS_OUTPUT VertDebug(VS_INPUT IN)
{
	VS_OUTPUT OUT;
	OUT.pos = mul(float4(IN.pos.xyz,1.0f), gWorldViewProj).xyzw;
	OUT.color0 = IN.diffuse0;
	OUT.color1 = IN.diffuse1;
	OUT.normal = IN.normal.xyz;
	OUT.tex0= IN.texCoord0;
	OUT.tex1= IN.texCoord1;
	OUT.pos0= IN.pos;
	//OUT.tex2= IN.texCoord2;
	//OUT.tex3= IN.texCoord3;
	//OUT.tex4= IN.texCoord4;
	//OUT.tex5= IN.texCoord5;
	return OUT;	
}
/*
texture testing	: TESTING;
sampler MySample =
sampler_state
{
Texture = <testing>;
MipFilter = LINEAR;
MinFilter = LINEAR;
MagFilter = LINEAR;
};
*/
BeginSampler(sampler2D, DebugTexture, DebugSampler, DebugTexture)
ContinueSampler(sampler2D, DebugTexture, DebugSampler, DebugTexture)
	MinFilter = Linear;
	MagFilter = Linear;
	MipFilter = Linear;
EndSampler;

float3 cameraPos;

float4 PSNormal(VS_OUTPUT IN) : COLOR
{
	return float4(IN.normal,1.0f);
}
float4 PSNormalWeighted(VS_OUTPUT IN) : COLOR
{
	return float4((IN.normal.xyz+.5)/2,1.0f);
}
float4 PSTexCoords(VS_OUTPUT IN) : COLOR
{
	return  float4(IN.tex0.y,IN.tex0.x,0.0f,1.0f);	
}
float4 PSTexCoords2(VS_OUTPUT IN) : COLOR
{
	return  float4(IN.tex1.y,IN.tex1.x,0.0f, 1.0f);		
}
float4 PSVertColors(VS_OUTPUT IN) : COLOR
{
	return  float4(IN.color0.x,IN.color0.y,IN.color0.z,1.0f);
}
float4 PSVertColorsAlpha(VS_OUTPUT IN) : COLOR
{
	return  float4(IN.color0.a,IN.color0.a,IN.color0.a,1.0f);
}
float4 PSVertColors2(VS_OUTPUT IN) : COLOR
{
	return  float4(IN.color1.x,IN.color1.y,IN.color1.z,1.0f);
}
float4 PSVertColorsAlpha2(VS_OUTPUT IN) : COLOR
{
	return  float4(IN.color1.a,IN.color1.a,IN.color1.a,1.0f);
}
float4 PSTexture1(VS_OUTPUT IN) : COLOR
{
	return tex2D(DebugSampler, IN.tex0);
}
float4 PSTexture2(VS_OUTPUT IN) : COLOR
{
	return tex2D(DebugSampler, IN.tex1);
}
float4 PSLit(VS_OUTPUT IN) : COLOR
{
	float3 lightDir=normalize(float3(.4,-.7,0));
	//float3 color=saturate(dot(normalize(cameraPos-IN.pos0),IN.normal))*.5+IN.color1.a*.5;
	float3 specular=pow(saturate(dot(normalize(cameraPos-IN.pos0),(lightDir-2*dot(IN.normal,lightDir)*IN.normal))),32);
	float3 diffuse=saturate(dot(-lightDir,IN.normal));
	float3 ambient=IN.color1.a;
	float3 retval=.5*specular+(diffuse+.5*ambient)*tex2D(DebugSampler,IN.tex0).xyz;
	return float4(retval,1);	
	//return float4(color,1);
}
float4 PSLit2(VS_OUTPUT IN) : COLOR
{
	float3 specular=saturate(dot(normalize(cameraPos-IN.pos0),IN.normal));
	return float4(specular,1);
}
float4 PSLit3(VS_OUTPUT IN) : COLOR
{
	const float scatterWidth=.5;
	float3 lightDir=normalize(float3(.4,-.7,0));
	//float3 color=saturate(dot(normalize(cameraPos-IN.pos0),IN.normal))*.5+IN.color1.a*.5;
	float3 look=normalize(IN.pos0-cameraPos);
	float3 halfAngle=normalize(lightDir+normalize(look));

	float3 specular=pow(saturate(dot(-halfAngle,IN.normal)),16)*tex2D(DebugSampler,IN.tex0).a;
	float3 diffuse=dot(-lightDir,IN.normal);
	float3 dflw=diffuse*2-1;
	dflw=saturate((diffuse+.8)/1.8);
	float3 scatter = smoothstep(0.0, scatterWidth, dflw) *
                    smoothstep(scatterWidth * 2.0, scatterWidth,
                               dflw);
	float3 ambient=IN.color1.a;
	
	float3 rim=saturate(pow(1-dot(-look,IN.normal),2)*diffuse)*tex2D(DebugSampler,IN.tex0).a;
	diffuse=saturate((diffuse+.8)/1.8);
	scatter=max(scatter,diffuse);
	specular*=2;
	specular+=5*rim;
	float3 retval=float3(.7,.8,1)*specular+(max(.2*scatter,diffuse)+.25)*tex2D(DebugSampler,IN.tex0).xyz;
	return float4(retval,1);
}
//normal_draw

technique draw
{  
	pass p0   
	{  
		//FillMode=Wireframe;
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSNormal();
	} 
}
technique drawskinned
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSNormal();
	} 
}
technique normalweighted_draw
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSNormalWeighted();
	} 
}
technique normalweighted_drawskinned
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSNormalWeighted();
	} 
}
technique texcoords_draw
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSTexCoords();
	} 
}
technique texcoords_drawskinned
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSTexCoords();
	} 
}
technique texcoords2_draw
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSTexCoords2();
	} 
}
technique texcoords2_drawskinned
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSTexCoords2();
	} 
}
technique vertcolors_draw
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSVertColors();
	} 
}
technique vertcolors_drawskinned
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSVertColors();
	} 
}
technique vertcolorsalpha_draw
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSVertColorsAlpha();
	} 
}
technique vertcolorsalpha_drawskinned
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSVertColorsAlpha();
	} 
}
technique vertcolors2_draw
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSVertColors2();
	} 
}
technique vertcolors2_drawskinned
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSVertColors2();
	} 
}
technique vertcolorsalpha2_draw
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSVertColorsAlpha2();
	} 
}
technique vertcolorsalpha2_drawskinned
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSVertColorsAlpha2();
	} 
}
technique texture1_draw
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSTexture1();
	} 
}
technique texture1_drawskinned
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSTexture1();
	} 
}
technique texture2_draw
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSTexture2();
	} 
}
technique texture2_drawskinned
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSTexture2();
	} 
}
technique lt_draw
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSLit();
	} 
}
technique lt_drawskinned
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSLit();
	} 
}
technique lt2_draw
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSLit2();
	} 
}
technique lt2_drawskinned
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSLit2();
	} 
}
technique blinnphong_draw
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSLit3();
	} 
}
technique blinnphong_drawskinned
{  
	pass p0   
	{  
		VertexShader = compile VERTEXSHADER VertDebug();
		PixelShader  = compile PIXELSHADER PSLit3();
	} 
}
