// 
// sample_simpleworld/simpleworld.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_SIMPLEWORLD_SIMPLEWORLD_H
#define SAMPLE_SIMPLEWORLD_SIMPLEWORLD_H

#define USE_RMPTFX 1

// switch cascaded shadow maps on and off 
#define USE_GRSHADOWMAP 0

// switch cube shadow maps on and off 
#if __XENON
#define USE_GRCUBESHADOWMAP 0
#elif __PPU
#define USE_GRCUBESHADOWMAP 0
#else
#define USE_GRCUBESHADOWMAP 0
#endif

#include "atl/array.h"
#include "atl/delegate.h"
#include "atl/dlist.h"
#include "atl/singleton.h"
#include "grcore/viewport.h"

#if USE_GRSHADOWMAP
#include "grshadowmap/cascadedshadowmap.h"
#endif

#if USE_GRCUBESHADOWMAP
#include "grshadowmap/cubeshadowmap.h"
#endif

#include "fragment/fragshaft.h"
#include "spatialdata/sphere.h"
#include "vector/matrix34.h"

#include "bucketcontrol.h"

//-----------------------------------------------------------------------------

namespace rage
{
	class crSkeleton;
	class Matrix34;
	class rmcDrawable;
	class grcViewport;
	class fragShaft;
	class spdSphere;
	class grmMatrixSet;
};


//-----------------------------------------------------------------------------

namespace ragesamples 
{

class SimpleWorldEntity;


/***** rmcDrawableObject **************************************/
/**************************************************************/
class rmcDrawableObject
{
public:
	rmcDrawableObject()
	{
		m_Drawable = 0;
		m_Skeleton = 0;
		m_Matrix = 0;
		m_IsVisible=true;
		m_Rotation.Zero();
	};
	~rmcDrawableObject() {};
	rage::rmcDrawable*  m_Drawable;
	rage::crSkeleton*   m_Skeleton;
	rage::Matrix34*		m_Matrix;
	rage::grmMatrixSet*	m_MatrixSet;
	bool				m_IsVisible;
	rage::Vector3		m_Rotation;
};

// PURPOSE: Class to manage drawing a list of objects with one shader
// REMARKS: This is useful for rendering shadows and collision maps, reflection height maps, etc
//
class rmcShadowObjectList 
{
public:
	rmcShadowObjectList();
	~rmcShadowObjectList() {}

	// add shadow casters
	void AddShadowCastingObject(rage::rmcDrawable*drawable,rage::crSkeleton*skeleton,rage::Matrix34 *matrix,rage::grmMatrixSet *ms);

	void RemoveShadowCastingObjects();
	void RemoveShadowCastingObject(rage::rmcDrawable *drawable);
	void RenderShadowCasters( rage::grcViewport &vp, bool isCasting  );

	typedef rage::atDelegator<void (rage::grcViewport&, bool)> DrawDelegator;
	typedef DrawDelegator::Delegate DrawDelegate;
	void AddNonSkinnedDrawDelegate(DrawDelegate* delegate)
	{
		m_ExternalNonSkinnedDrawDelegator.AddDelegate(delegate);
	}
	void RemoveNonSkinnedDrawDelegate(DrawDelegate* delegate)
	{
		m_ExternalNonSkinnedDrawDelegator.RemoveDelegate(delegate);
	}
	void AddSkinnedDrawDelegate(DrawDelegate* delegate)
	{
		m_ExternalSkinnedDrawDelegator.AddDelegate(delegate);
	}
	void RemoveSkinnedDrawDelegate(DrawDelegate* delegate)
	{
		m_ExternalSkinnedDrawDelegator.RemoveDelegate(delegate);
	}
	void AddSpecialCaseDrawDelegate(DrawDelegate* delegate)
	{
		m_ExternalSpecialCaseDrawDelegator.AddDelegate(delegate);
	}
	void RemoveSpecialCaseDrawDelegate(DrawDelegate* delegate)
	{
		m_ExternalSpecialCaseDrawDelegator.RemoveDelegate(delegate);
	}

	void RenderBounds();
private:
	DrawDelegator m_ExternalNonSkinnedDrawDelegator;
	DrawDelegator m_ExternalSkinnedDrawDelegator;
	DrawDelegator m_ExternalSpecialCaseDrawDelegator;

	rage::u32							m_MaxBuckets;			// Maximum number of buckets needed by game
	rage::atArray<rmcDrawableObject>	m_ShadowCasters;

	// shaft that is used to cull objects against the light camera view frustum
	rage::fragShaft shaft;
};

typedef atSingleton<rmcShadowObjectList> rmcShadowList;


#if USE_GRSHADOWMAP

///***** rmcSampleShadowMap **************************************/
///**************************************************************/
//class rmcSampleShadowMap : public rage::CShadowMap
//{
//public:
//	rmcSampleShadowMap(int numTiles);
//
//	~rmcSampleShadowMap();
//
//	// add shadow casters
//	void AddShadowCastingObject(rage::rmcDrawable*drawable,rage::crSkeleton*skeleton,rage::Matrix34 *matrix);
//
//	void RemoveShadowCastingObjects();
//
//	void RemoveShadowCastingObject(rage::rmcDrawable *drawable);
//
//    void RenderShadowCasters( rage::grcViewport &vp, bool isCasting  );
//
//	// static functions
//	// keep track of the instance
//	static void Init();
//
//	static void Terminate();
//
//	static rmcSampleShadowMap *GetInstance() {return sm_Instance;}
//
//	rage::atDelegator<void (rage::grcViewport&, bool)> m_ExternalNonSkinnedDrawDelegator;
//	rage::atDelegator<void (rage::grcViewport&, bool)> m_ExternalSkinnedDrawDelegator;
//	rage::atDelegator<void (rage::grcViewport&, bool)> m_ExternalSpecialCaseDrawDelegator;
//
//private:
//	rage::u32							m_MaxBuckets;			// Maximum number of buckets needed by game
//	rage::atArray<rmcDrawableObject>	m_ShadowCasters;
//	//rage::atArray<SimpleWorldEntity*>	m_ShadowEntities;
//
//	// shaft that is used to cull objects against the light camera view frustum
//	rage::fragShaft shaft;
//};



/***** rmcSampleShadowMap **************************************/
/**************************************************************/
class rmcSampleShadowMap : public rage::CShadowMap
{
public:
	rmcSampleShadowMap(int numTiles);
	~rmcSampleShadowMap();

	// static functions
	// keep track of the instance
	static void Init();
	static rmcSampleShadowMap *GetInstance() {return sm_Instance;}
	static void Terminate();

	void SetObjectList( rmcShadowObjectList* objList )
	{
		m_objectList = objList;
	}
	 void RenderShadowCasters( rage::grcViewport &vp, bool isCasting  )
	 {
		 FastAssert( m_objectList );
		 if ( m_objectList )
		 {
			m_objectList->RenderShadowCasters( vp, isCasting );
		 }
	 }

private:
	rmcShadowObjectList*		m_objectList;
	static rmcSampleShadowMap *sm_Instance;			// instance of cascaded shadow map
};

#define RMCSAMPLESHADOWMAP (rmcSampleShadowMap::GetInstance())

#endif // USE_GRSHADOWMAP


#if USE_GRCUBESHADOWMAP

/***** rmcSampleCubeShadowMap **************************************/
/**************************************************************/
class rmcSampleCubeShadowMap : public rage::CCubeShadowMap
{
public:
	rmcSampleCubeShadowMap(int numLights);

	~rmcSampleCubeShadowMap();

	// add shadow casters
	void AddShadowCastingObject(rage::rmcDrawable*drawable,rage::crSkeleton*skeleton,rage::Matrix34 *matrix);

	void RemoveShadowCastingObjects();

	void RemoveShadowCastingObject(rage::rmcDrawable *drawable);

	void RenderShadowCasters(rage::grcViewport &vp, bool isCasting);

	// static functions
	// keep track of the instance
	static void Init();

	static void Terminate();

	static rmcSampleCubeShadowMap *GetInstance() {return sm_CubeInstance;}

	rage::atDelegator<void (rage::grcViewport&, bool)> m_ExternalNonSkinnedDrawDelegator;
	rage::atDelegator<void (rage::grcViewport&, bool)> m_ExternalSkinnedDrawDelegator;

private:
	rage::u32							m_MaxBuckets;			// Maximum number of buckets needed by game
	rage::atArray<rmcDrawableObject>	m_ShadowCasters;
	
	// array that holds cube shadow map casters
	rage::atArray<rmcDrawableObject>	m_CubeShadowCasters;

	//rage::atArray<SimpleWorldEntity*>	m_ShadowEntities;

	static rmcSampleCubeShadowMap *sm_CubeInstance;			// instance of cube shadow map

	// shaft that is used to cull objects against the light camera view frustum
	rage::fragShaft shaft;
};

#define RMCCUBESAMPLESHADOWMAP (rmcSampleCubeShadowMap::GetInstance())

#endif // USE_GRCUBESHADOWMAP

//-----------------------------------------------------------------------------

class SimpleWorldMovable
{
friend class SimpleWorld;

public:
	SimpleWorldMovable(void* context);
	virtual ~SimpleWorldMovable() {};

	virtual void					AddToDrawList(const rage::fragShaft& shaft) = 0;
	virtual const rage::spdSphere&	GetBoundingSphere(void) const = 0;

protected:
	void*								m_Context;
	rage::atDNode<SimpleWorldMovable*>	m_InWorldNode;
};

//-----------------------------------------------------------------------------

class SimpleWorld
{
public:
	void	Insert(void* movableId);
	void	Remove(void* movableId);
	void	Update(void* movableId);

	bool AddToDrawBucket(const rage::rmcDrawable& toDraw,
						 const rage::Matrix34* matrix,
						 const rage::crSkeleton* skeleton,
						 const rage::grmMatrixSet* matrixSet,
						 float lod);
	
	// high level
	void Draw(rage::fragShaft& shaft, rage::u32 BucketMask = ~0 );

	// more control
	void DrawFinal( rage::u32 BucketMask);
	void DrawSetup( rage::fragShaft &shaft, rage::u32 BucketMask  = ~0 );

	void SetUseSkelForSkinnedOnly(bool flag) { m_bUseSkelForSkinnedOnly = flag; }
	void Ignore( rage::u32 ingoreBuckets ) { m_IgnoreBuckets = ingoreBuckets; }

	void BeginDraw();
	void EndDraw( rage::u32 BucketMask );

	rage::rmcDrawable* GetDrawable();

protected:
	SimpleWorld() 
		: m_bUseSkelForSkinnedOnly(false),
		  m_IgnoreBuckets( 0 )
	{};

	rage::atDList<SimpleWorldMovable*>	m_Inserted;

	rage::BucketRenderer						m_bucketRenderer;
	rage::u32									m_IgnoreBuckets;
	//This flag is a project specific hack for RDR2 to make the simpleworld viewer
	//render like sample_drawable.
	bool	m_bUseSkelForSkinnedOnly;
};


//-----------------------------------------------------------------------------

typedef atSingleton<SimpleWorld> smpWorldSingleton;

#define SMPWORLD			::ragesamples::smpWorldSingleton::InstanceRef()
#define INIT_SMPWORLD		::ragesamples::smpWorldSingleton::Instantiate()
#define SHUTDOWN_SMPWORLD	::ragesamples::smpWorldSingleton::Destroy()

//-----------------------------------------------------------------------------

}//end namespace ragesamples



#endif //SAMPLE_SIMPLEWORLD_SIMPLEWORLD_H
