// 
// sample_simpleworld/simplescript.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_SIMPLEWORLD_SIMPLESCRIPT_H
#define SAMPLE_SIMPLEWORLD_SIMPLESCRIPT_H

namespace ragesamples
{
    class rmcSampleSimpleWorld;
}

namespace rageSimpleScript
{

void Register_SimpleScript( ragesamples::rmcSampleSimpleWorld *pSampleSimpleWorld );

} // namespace rageSimpleScript

#endif // SAMPLE_SIMPLEWORLD_SIMPLESCRIPT_H
