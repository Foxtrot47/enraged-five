// 
// sample_simpleworld/animmgr.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "animmgr.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "bank/button.h"
#include "bank/list.h"
#include "bank/slider.h"
#include "bank/toggle.h"
#include "bank/widget.h"
#include "cliptools/clipcombinecore.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "crmetadata/tags.h"
#include "crmetadata/tag.h"
#include "creature/creature.h"
#include "crextra/componentevent.h"
#include "crextra/expressions.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/node.h"
#include "crmotiontree/nodeanimation.h"
#include "crmotiontree/nodeclip.h"
#include "crmotiontree/nodeexpression.h"
#include "crmotiontree/nodefilter.h"
#include "crmotiontree/nodeik.h"
#include "crmotiontree/nodepm.h"
#include "crmotiontree/nodestyle.h"
#include "crmotiontree/observer.h"
#include "crmotiontree/requestanimation.h"
#include "crmotiontree/requestblend.h"
#include "crmotiontree/requestcapture.h"
#include "crmotiontree/requestclip.h"
#include "crmotiontree/requestExpression.h"
#include "crmotiontree/requestFrame.h"
#include "crmotiontree/requestFilter.h"
#include "crmotiontree/requestMerge.h"
#include "crskeleton/skeleton.h"
#include "system/timemgr.h"
#include "zlib/zlib.h"

namespace ragesamples
{

//-----------------------------------------------------------------------------

crmtMotionTreeScheduler RvAnimMgr::sm_MotionTreeScheduler;
crFrameDataFactory RvAnimMgr::sm_FrameDataFactory;
crFrameAccelerator RvAnimMgr::sm_FrameAccelerator;
crCommonPool RvAnimMgr::sm_CommonPool;

atMap<int, crAnimation*> RvAnimMgr::sm_AnimationTable;
atArray<crAnimation*> RvAnimMgr::sm_AnimationList;
int RvAnimMgr::sm_nextAnimId = 0;

atMap<int, crClip*> RvAnimMgr::sm_ClipTable;
atArray<crClip*> RvAnimMgr::sm_ClipList;
int RvAnimMgr::sm_nextClipId = 0;

RvAnimMgr::AnimMgrSelectedFtor	RvAnimMgr::sm_AnimSelectedFtor = RvAnimMgr::AnimMgrSelectedFtor::NullFunctor();
RvAnimMgr::AnimMgrSelectedFtor	RvAnimMgr::sm_ClipSelectedFtor = RvAnimMgr::AnimMgrSelectedFtor::NullFunctor();

bool RvAnimMgr::sm_ManageEventManager = false;

u32 AnimMgrFrameFilter::sm_Hash = atHash_const_char("AnimMgrFrameFilter");

#if __BANK
bkList*							RvAnimMgr::sm_pBankAnimList = NULL;
bkList*							RvAnimMgr::sm_pBankClipList = NULL;
atArray<crTagEditorWidget*>		RvAnimMgr::sm_ClipTagEditors;
atArray<crClipPropertyEditor*>	RvAnimMgr::sm_ClipPropertyEditors;
#endif


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CR_IMPLEMENT_FRAME_FILTER_TYPE(AnimMgrFrameFilter, kFrameFilterTypeProjectFilters, crFrameFilter)

bool AnimMgrFrameFilter::FilterDof(u8 track, u16 id, float&)
{
	switch(track)
	{
	case kTrackBoneTranslation:
		{
			if(id == 0)
			{
				return m_AllowRootTranslation;
			}
			else
				return true;
		}
	case kTrackMoverTranslation:
		return m_AllowMover;
	default:
		return true;
	}
}

u32 AnimMgrFrameFilter::CalcSignature() const
{
	u32 signature = u32(m_AllowMover) | (u32(m_AllowRootTranslation)<<1);
	crc32(signature, (const u8*)&sm_Hash, sizeof(u32));

	return signature;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

RvAnimMgr::RvAnimMgr()
	: m_CurrentFrame(0.f)
	, m_Phase(0.f)
	, m_StartFrame(0.f)
	, m_EndFrame(0.f)
	, m_FrameRate(30.f)
	, m_PlaybackStartFrame(0.f)
	, m_PlaybackEndFrame(0.f)
	, m_Sequence(false)
	, m_PlayMode(ANIM_PAUSE)
	, m_LastPlayDirection(0)
	, m_bLoopAnimation(true)
	, m_MoverPlayMode(MOVER_ABSOLUTE)
	, m_bMoverDebugDraw(false)
	, m_StepSize(0.f)
    , m_animMode(true)
	, m_CurrentAnimIdx(0)
    , m_CurrentClipIdx(0)
    , m_pMotionTree(NULL)
	, m_bFrameInDirty(false)
#if __BANK
    , m_pAnimOrClipToggle(NULL)
    , m_pAnimIdxSlider(NULL)
    , m_pClipIdxSlider(NULL)
    , m_pSeqToggle(NULL)
    , m_pRateSlider(NULL)
    , m_pTimeSlider(NULL)
    , m_pStartTimeSlider(NULL)
    , m_pEndTimeSlider(NULL)
    , m_pPlaybackStartTimeSlider(NULL)
    , m_pPlaybackEndTimeSlider(NULL) 
    , m_pStepSizeSlider(NULL)
    , m_pGotoStartButton(NULL)
    , m_pStepBackButton(NULL)
    , m_pPlayBackwardsButton(NULL)
    , m_pPauseButton(NULL)
    , m_pPlayForwaredsButton(NULL)
    , m_pStepForwardsButton(NULL)
    , m_pGotoEndButton(NULL)
#endif
{
	m_Filter.AddRef();

    m_pMotionTree = rage_new crmtMotionTree;

	m_currentFrameChangedFtor = AnimMgrChangeFtor::NullFunctor();
	m_playModeChangedFtor = AnimMgrChangeFtor::NullFunctor();
	m_animIndexChangedFtor = AnimMgrChangeFtor::NullFunctor();
	m_clipIndexChangedFtor = AnimMgrChangeFtor::NullFunctor();
	m_frameRangeChangedFtor = AnimMgrChangeFtor::NullFunctor();
	m_loopedFtor = AnimMgrChangeFtor::NullFunctor();
	m_resetMoverFtor = AnimMgrCommandFtor::NullFunctor();
	m_moverModeChangedFtor = AnimMgrChangeFtor::NullFunctor();
	m_allowMoverChangedFtor = AnimMgrChangeFtor::NullFunctor();
	m_allowRootTranslationChangedFtor = AnimMgrChangeFtor::NullFunctor();
	m_moverDebugDrawChangedFtor = AnimMgrChangeFtor::NullFunctor();
	m_loopAnimationChangedFtor = AnimMgrChangeFtor::NullFunctor();

	m_pObserverClip = rage_new crmtObserver();
	m_pObserverAnim = rage_new crmtObserver();
	m_pObserverExpr = rage_new crmtObserver();
	m_pObserverClip->AddRef();
	m_pObserverAnim->AddRef();
	m_pObserverExpr->AddRef();
}

//-----------------------------------------------------------------------------

RvAnimMgr::~RvAnimMgr()
{
    SetCurrentAnimationId( 0 );
	SetCurrentClipId(0);

	m_pObserverAnim->Release();
	m_pObserverClip->Release();
	m_pObserverExpr->Release();

	delete m_pMotionTree;
    m_pMotionTree = NULL;
}

//-----------------------------------------------------------------------------

void RvAnimMgr::Init(crCreature* creature)
{
	crmtRequestFrame reqFrame(&m_FrameIn);
	crmtRequestAnimation reqAnim;
	crmtRequestClip	reqClip;
	crmtRequestMerge reqMerge1(reqAnim, reqClip);
	crmtRequestMerge reqMerge2(reqFrame, reqMerge1);
	crmtRequestExpression reqExpr(reqMerge2, NULL);
	crmtRequestCapture reqCapture(reqExpr, &m_FrameOut);
	crmtRequestFilter reqFilter(reqCapture, &m_Filter);

	m_pMotionTree->Init(*creature);
	m_pMotionTree->Request( reqFilter );

	m_pObserverAnim->Attach(reqAnim.GetObserver());
	m_pObserverClip->Attach(reqClip.GetObserver());
	m_pObserverExpr->Attach(reqExpr.GetObserver());
}

//-----------------------------------------------------------------------------

void RvAnimMgr::SetExpressions(crExpressions* pExpr)
{
	if(m_pObserverExpr->IsAttached())
	{
		crmtNodeExpression* pNodeExpr = static_cast<crmtNodeExpression*>(m_pObserverExpr->GetNode());
		pNodeExpr->GetExpressionPlayer().SetExpressions(pExpr);
	}
}

//-----------------------------------------------------------------------------

void RvAnimMgr::InitClass()
{
	//Insert a NULL pointer at the 0 index of the animation table
	//to represent the base pose
	sm_AnimationTable.Insert( sm_nextAnimId, NULL );
    ++sm_nextAnimId;

    sm_AnimationList.PushAndGrow( NULL );

    //Insert a NULL pointer at the 0 index of the clip table
    //to represent the base pose
    sm_ClipTable.Insert( sm_nextClipId, NULL );
    ++sm_nextClipId;

    sm_ClipList.PushAndGrow( NULL );

    // Animation Initialization
    crAnimation::InitClass();
    crCreature::InitClass();
    crTag::InitClass();
    crClip::InitClass();
    crmtMotionTree::InitClass();
	crExpressions::InitClass();

#if !INIT_NON_CORE_NODES
    // initialize all the non-core built in nodes
    crmtNodeIk::InitClass();
    crmtNodeClip::InitClass();
    crmtNodePm::InitClass();
    crmtNodeExpression::InitClass();
#endif // !INIT_NON_CORE_NODES

	sm_CommonPool.Init(32, 5*1024);
	sm_FrameDataFactory.Init(sm_CommonPool);
	sm_MotionTreeScheduler.Init();
}

//-----------------------------------------------------------------------------

void RvAnimMgr::ShutdownClass()
{   
    Reset();

#if !INIT_NON_CORE_NODES
    // shutdown all the non-core built in nodes
    crmtNodeIk::ShutdownClass();
    crmtNodeClip::ShutdownClass();
    crmtNodeStyle::ShutdownClass();
    crmtNodePm::ShutdownClass();
    crmtNodeExpression::ShutdownClass();
#endif // !INIT_NON_CORE_NODES

    crmtMotionTree::ShutdownClass();
    crCreature::ShutdownClass();
    crAnimation::ShutdownClass();
    crTag::ShutdownClass();
    crClip::ShutdownClass();

	// Event shutdown
	crCreatureEventPlayer::UnregisterPlayerClass();
	if (sm_ManageEventManager)
	{
		SHUTDOWN_EVENTMGR;
	}
}

//-----------------------------------------------------------------------------

void RvAnimMgr::Reset()
{
    int animCount = sm_AnimationList.GetCount();
    for ( int i = 1; i < animCount; ++i )  // start at 1 because index 0 is our NULL pose
    {
#if __BANK
        sm_pBankAnimList->RemoveItem( GetAnimationId( i ) );
#endif //__BANK

        sm_AnimationList[i]->Release();
    }

    sm_AnimationTable.Kill();
    sm_AnimationList.Reset();
    sm_nextAnimId = 0;

    int clipCount = sm_ClipList.GetCount();
    for ( int i = 1; i < clipCount; ++i )  // start at 1 because index 0 is our NULL pose
    {
#if __BANK
        sm_pBankClipList->RemoveItem( GetClipId( i ) );
		bkBank* clipBank = BANKMGR.FindBank(sm_ClipList[i]->GetName());
		if (clipBank)
		{
			BANKMGR.DestroyBank(*clipBank);
		}
		sm_ClipTagEditors[i] = NULL;
#endif //__BANK

        sm_ClipList[i]->Release();
    }

    sm_ClipTable.Kill();
    sm_ClipList.Reset();
    sm_nextClipId = 0;
#if __BANK
	sm_ClipTagEditors.Reset();
	sm_ClipPropertyEditors.Reset();
#endif
}

//-----------------------------------------------------------------------------

void RvAnimMgr::InsertAnimation(crAnimation *pAnimation)
{
    sm_AnimationTable.Insert( sm_nextAnimId, pAnimation );
    sm_AnimationList.PushAndGrow( pAnimation );

#if __BANK
    if ( sm_pBankAnimList )
    {
        sm_pBankAnimList->AddItem( sm_nextAnimId, 0, sm_nextAnimId );

		char baseName[256];
		ASSET.BaseName(baseName, 256, ASSET.FileName(pAnimation->GetName()));
        sm_pBankAnimList->AddItem( sm_nextAnimId, 1, baseName );
    }
#endif //__BANK

    ++sm_nextAnimId;
}

//-----------------------------------------------------------------------------

void RvAnimMgr::RemoveAnimation(crAnimation* pAnimation)
{
    int animIdx = GetAnimationIndex( pAnimation );
    int animId = GetAnimationId( pAnimation );

    if ( (animIdx != 0) && (animId != 0) )
    {
        sm_AnimationList.Delete( animIdx );
        sm_AnimationTable.Delete( animId );

#if __BANK
        if( sm_pBankAnimList )
        {
            sm_pBankAnimList->RemoveItem( animId );
        }
#endif // __BANK
    }
}

//-----------------------------------------------------------------------------

crAnimation* RvAnimMgr::LoadAndInsertAnimation(const char *filePath)
{
    Assert(filePath);
    crAnimation *pAnimation = crAnimation::AllocateAndLoad( filePath );
    if ( pAnimation == NULL )
    {
        Errorf( "Failed to load animation : '%s'", filePath );
    }
    else
    {
        InsertAnimation( pAnimation );
    }

    return pAnimation;
}

//-----------------------------------------------------------------------------

void RvAnimMgr::UnloadAndRemoveAnimation(const char* filePath)
{
    Assert(filePath);

    int count = sm_AnimationList.GetCount();
    for ( int i = 1; i < count; ++i )   // start at 1 because index 0 is our NULL pose
    {
        if ( stricmp( sm_AnimationList[i]->GetName(), filePath ) == 0 )
        {
            crAnimation *pAnimation = sm_AnimationList[i];
            RemoveAnimation( pAnimation );
            pAnimation->Release();
            break;
        }
    }
}

//-----------------------------------------------------------------------------

void RvAnimMgr::InsertClip(crClip *pClip)
{
    sm_ClipTable.Insert( sm_nextClipId, pClip );
    sm_ClipList.PushAndGrow( pClip );

#if __BANK
    if ( sm_pBankClipList )
    {
        sm_pBankClipList->AddItem( sm_nextClipId, 0, sm_nextClipId );
        sm_pBankClipList->AddItem( sm_nextClipId, 1, pClip->GetName() );
    }

	bkBank& bank = BANKMGR.CreateBank(pClip->GetName());
	bank.AddButton("Save", datCallback(CFA1(SaveClip), pClip));
	bank.AddButton("Save As...", datCallback(CFA1(SaveClipAs), pClip));
	bank.AddButton("Save Template...", datCallback(CFA1(SaveClipTemplate), pClip));
	bank.AddButton("Load Template...", datCallback(CFA1(LoadClipTemplate), pClip));	
#endif //__BANK

    ++sm_nextClipId;
}

//-----------------------------------------------------------------------------

void RvAnimMgr::RemoveClip(crClip* pClip)
{
    int clipIdx = GetClipIndex( pClip );
    int clipId = GetClipId( pClip );
    if ( (clipIdx != 0) && (clipId != 0) )
    {

#if __BANK
        if( sm_pBankClipList )
        {
            sm_pBankClipList->RemoveItem( clipId );
        }

		if (sm_ClipTagEditors[clipIdx])
		{
			bkBank* bank = BANKMGR.FindBank(pClip->GetName());
			if (bank)
			{
				BANKMGR.DestroyBank(*bank);
			}
			sm_ClipTagEditors.Delete(clipIdx);
			sm_ClipPropertyEditors.Delete(clipIdx);
		}
#endif // __BANK

		sm_ClipList.Delete( clipIdx );
		sm_ClipTable.Delete( clipId );
    }
}

//-----------------------------------------------------------------------------

crClip* RvAnimMgr::LoadAndInsertClip(const char *filePath)
{
    Assert(filePath);    
    crClip *pClip = crClip::AllocateAndLoad( filePath );
    if ( pClip == NULL )
    {
        Errorf( "Failed to load clip : '%s'", filePath );
    }
    else
    {
        InsertClip( pClip );
    }

    return pClip;
}

//-----------------------------------------------------------------------------

void RvAnimMgr::UnloadAndRemoveClip(const char* filePath)
{
    Assert(filePath);

    int count = sm_ClipList.GetCount();
    for ( int i = 1; i < count; ++i )   // start at 1 because index 0 is our NULL pose
    {
        if ( stricmp( sm_ClipList[i]->GetName(), filePath ) == 0 )
        {
            crClip *pClip = sm_ClipList[i];
            RemoveClip( pClip );
            pClip->Release();
            break;
        }
    }
}

//-----------------------------------------------------------------------------

int RvAnimMgr::GetAnimationCount()
{
    return sm_AnimationList.GetCount();
}

//-----------------------------------------------------------------------------

crAnimation* RvAnimMgr::GetAnimation( int animIdx )
{
    if ( (animIdx >= 0) && (animIdx < sm_AnimationList.GetCount()) )
    {
        return sm_AnimationList[animIdx];
    }

    return NULL;
}

//-----------------------------------------------------------------------------

int RvAnimMgr::GetAnimationIndex( crAnimation* pAnimation )
{
    int count = sm_AnimationList.GetCount();
    for ( int i = 1; i < count; ++i )   // start at 1 because index 0 is our NULL pose
    {
        if ( sm_AnimationList[i] == pAnimation )
        {
            return i;
        }
    }

    return 0;
}

//-----------------------------------------------------------------------------

int RvAnimMgr::GetAnimationIndex( int animId )
{
    crAnimation **ppAnim = sm_AnimationTable.Access( animId );
    if ( ppAnim )
    {
        return GetAnimationIndex( *ppAnim );
    }

    return 0;
}

//-----------------------------------------------------------------------------

crAnimation* RvAnimMgr::GetAnimationById( int animId )
{
    crAnimation **ppAnimation = sm_AnimationTable.Access( animId );
    if ( ppAnimation != NULL )
    {
        return *ppAnimation;
    }

    return NULL;
}

//-----------------------------------------------------------------------------

int RvAnimMgr::GetAnimationId( crAnimation* pAnimation )
{
    atMap<int, crAnimation *>::Iterator entry = sm_AnimationTable.CreateIterator();
    for ( entry.Start(); !entry.AtEnd(); entry.Next() )
    {
        if ( entry.GetData() == pAnimation )
        {
            return entry.GetKey();
        }
    }

    return 0;
}

//-----------------------------------------------------------------------------

int RvAnimMgr::GetAnimationId( int animIdx )
{
    crAnimation *pAnim = GetAnimation( animIdx );
    if ( pAnim )
    {
        return GetAnimationId( pAnim );
    }

    return 0;
}


//-----------------------------------------------------------------------------

int RvAnimMgr::GetClipCount()
{
    return sm_ClipList.GetCount();
}

//-----------------------------------------------------------------------------

crClip* RvAnimMgr::GetClip( int clipIdx )
{
    if ( (clipIdx >= 0) && (clipIdx < sm_ClipList.GetCount()) )
    {
        return sm_ClipList[clipIdx];
    }

    return NULL;
}

//-----------------------------------------------------------------------------

int RvAnimMgr::GetClipIndex( crClip* pClip )
{
    int count = sm_ClipList.GetCount();
    for ( int i = 1; i < count; ++i )   // start at 1 because index 0 is our NULL pose
    {
        if ( sm_ClipList[i] == pClip )
        {
            return i;
        }
    }

    return 0;
}

//-----------------------------------------------------------------------------

int RvAnimMgr::GetClipIndex( int clipId )
{
    crClip **ppAnim = sm_ClipTable.Access( clipId );
    if ( ppAnim )
    {
        return GetClipIndex( *ppAnim );
    }

    return 0;
}

//-----------------------------------------------------------------------------

crClip* RvAnimMgr::GetClipById( int clipId )
{
    crClip **ppClip = sm_ClipTable.Access( clipId );
    if ( ppClip != NULL )
    {
        return *ppClip;
    }

    return NULL;
}

//-----------------------------------------------------------------------------

int RvAnimMgr::GetClipId( crClip* pClip )
{
    atMap<int, crClip *>::Iterator entry = sm_ClipTable.CreateIterator();
    for ( entry.Start(); !entry.AtEnd(); entry.Next() )
    {
        if ( entry.GetData() == pClip )
        {
            return entry.GetKey();
        }
    }

    return 0;
}

//-----------------------------------------------------------------------------

int RvAnimMgr::GetClipId( int clipIdx )
{
    crClip *pAnim = GetClip( clipIdx );
    if ( pAnim )
    {
        return GetClipId( pAnim );
    }

    return 0;
}

//-----------------------------------------------------------------------------

void RvAnimMgr::SetFrameRate( float fr )
{
    m_FrameRate = fr;

	if ( IsInAnimMode() && m_pObserverAnim->IsAttached() )
    {
		crmtNodeAnimation *pAnimNode = static_cast<crmtNodeAnimation*>(m_pObserverAnim->GetNode());
        pAnimNode->GetAnimPlayer().SetRate( m_FrameRate );
    }
	else if ( IsInClipMode() && m_pObserverClip->IsAttached() )
    {
		crmtNodeClip *pClipNode = static_cast<crmtNodeClip*>(m_pObserverClip->GetNode());
        pClipNode->GetClipPlayer().SetRate( m_FrameRate );
    }

}

//-----------------------------------------------------------------------------

float RvAnimMgr::GetCurTime() const
{
    crAnimation *pAnimation = GetCurrentAnimation();
    if ( pAnimation )
    {
        return pAnimation->Convert30FrameToTime( m_CurrentFrame );
    }
    else
    {
        crClip *pClip = GetCurrentClip();
        if ( pClip )
        {
            return pClip->Convert30FrameToTime( m_CurrentFrame );
        }
    }

    return 0.0f;
}

//-----------------------------------------------------------------------------

void RvAnimMgr::SetCurTime( float time )
{
    crAnimation *pAnimation = GetCurrentAnimation();
    if ( pAnimation )
    {
        m_CurrentFrame = pAnimation->ConvertTimeTo30Frame( time );
    }
    else
    {
        crClip *pClip = GetCurrentClip();
        if ( pClip )
        {
            m_CurrentFrame = pClip->Convert30FrameToTime( time );
        }
    }

	if ( IsInAnimMode() && m_pObserverAnim->IsAttached() )
	{
		crmtNodeAnimation *pAnimNode = static_cast<crmtNodeAnimation*>(m_pObserverAnim->GetNode());
		pAnimNode->GetAnimPlayer().SetTime( m_CurrentFrame );
	}
	else if ( IsInClipMode() && m_pObserverClip->IsAttached() )
	{
		crmtNodeClip *pClipNode = static_cast<crmtNodeClip*>(m_pObserverClip->GetNode());
		pClipNode->GetClipPlayer().SetTime( m_CurrentFrame );
	}
}

//-----------------------------------------------------------------------------

crAnimation* RvAnimMgr::GetCurrentAnimation() const
{
    if ( IsInAnimMode() )
    {
        return GetAnimation( m_CurrentAnimIdx );
    }
    else
    {
        return NULL;
    }
}

crClip* RvAnimMgr::GetCurrentClip() const
{
    if ( IsInClipMode() )
    {
        return GetClip( m_CurrentClipIdx );
    }
    else
    {
        return NULL;
    }
}

//-----------------------------------------------------------------------------

float RvAnimMgr::GetCurrentAnimStartTime() const
{
    crAnimation *pAnimation = GetCurrentAnimation();
    if ( pAnimation )
    {
        return pAnimation->Convert30FrameToTime( m_StartFrame );
    }
    else
    {
        crClip *pClip = GetCurrentClip();
        if ( pClip )
        {
            return pClip->Convert30FrameToTime( m_StartFrame );
        }
    }

    return 0.0f;
}

//-----------------------------------------------------------------------------

float RvAnimMgr::GetCurrentAnimEndTime() const
{
    crAnimation *pAnimation = GetCurrentAnimation();
    if ( pAnimation )
    {
        return pAnimation->Convert30FrameToTime( m_EndFrame );
    }
    else
    {
        crClip *pClip = GetCurrentClip();
        if ( pClip )
        {
            return pClip->Convert30FrameToTime( m_EndFrame );
        }
    }

    return 0.0f;
}

//-----------------------------------------------------------------------------

float RvAnimMgr::GetPlaybackStartTime() const
{
    crAnimation *pAnimation = GetCurrentAnimation();
    if ( pAnimation )
    {
        return pAnimation->Convert30FrameToTime( m_PlaybackStartFrame );
    }
    else
    {
        crClip *pClip = GetCurrentClip();
        if ( pClip )
        {
            return pClip->Convert30FrameToTime( m_PlaybackStartFrame );
        }
    }

    return 0.0f;
}

//-----------------------------------------------------------------------------

void RvAnimMgr::SetPlaybackStartTime( float startTime )
{
    crAnimation *pAnimation = GetCurrentAnimation();
    if ( pAnimation )
    {
        m_PlaybackStartFrame = pAnimation->ConvertTimeTo30Frame( startTime );
    }
    else
    {
        crClip *pClip = GetCurrentClip();
        if ( pClip )
        {
            m_PlaybackStartFrame = pClip->Convert30FrameToTime( startTime );
        }
    }
}

//-----------------------------------------------------------------------------

float RvAnimMgr::GetPlaybackEndTime() const
{
    crAnimation *pAnimation = GetCurrentAnimation();
    if ( pAnimation )
    {
        return pAnimation->Convert30FrameToTime( m_PlaybackEndFrame );
    }
    else
    {
        crClip *pClip = GetCurrentClip();
        if ( pClip )
        {
            return pClip->Convert30FrameToTime( m_PlaybackEndFrame );
        }
    }

    return 0.0f;
}


//-----------------------------------------------------------------------------

void RvAnimMgr::SetPlaybackEndTime( float endTime )
{
    crAnimation *pAnimation = GetCurrentAnimation();
    if ( pAnimation )
    {
        m_PlaybackEndFrame = pAnimation->ConvertTimeTo30Frame( endTime );
    }
    else
    {
        crClip *pClip = GetCurrentClip();
        if ( pClip )
        {
            m_PlaybackEndFrame = pClip->Convert30FrameToTime( endTime );
		}
    }
}

//-----------------------------------------------------------------------------

void RvAnimMgr::TogglePlayMode()
{
	if(m_PlayMode == ANIM_PAUSE)
	{
		if(m_LastPlayDirection < 0)
		{
			m_PlayMode = ANIM_BACKWARD;
		}
		else
		{
			m_PlayMode = ANIM_FORWARD;

		}
		m_LastPlayDirection = 0;
	}
	else if (m_PlayMode == ANIM_FORWARD)
	{
		m_PlayMode = ANIM_PAUSE;
		m_LastPlayDirection = 1;
	}
	else if (m_PlayMode == ANIM_BACKWARD)
	{
		m_PlayMode = ANIM_PAUSE;
		m_LastPlayDirection = -1;
	}

	m_playModeChangedFtor(this);
}

//-----------------------------------------------------------------------------

// NOTE: Using this function instead of crAnimation::Convert30FrameToTime or crClip::Convert30FrameToTime
float FrameToTime(float frame)
{
	return frame / 30.0f;
}

float TimeToFrame(float time)
{
	return time * 30.0f;
}

void RvAnimMgr::Update()
{
	bool hitAnEnd = false;

	float playbackStartTime = GetPlaybackStartTime();
	float playbackEndTime = GetPlaybackEndTime();

	float startTime = FrameToTime(m_CurrentFrame);
	float lastTime = startTime; // to see if the time changed over this update
	float endTime = startTime; // this update should animate from startTime to endTime

	float oneFrame = FrameToTime(1.0f);

	// FrameToTime on m_FrameRate converts frames per second to a unitless scalar
	float framerateScalar = FrameToTime(m_FrameRate);

	crmtNodeAnimation *pAnimNode = NULL;
	crmtNodeClip *pClipNode = NULL;

	if( (m_pMotionTree->GetSkeleton() != NULL) || (m_pMotionTree->GetCreature() != NULL) )
	{
		if ( IsInAnimMode() && m_pObserverAnim->IsAttached() )
		{
			pAnimNode = static_cast<crmtNodeAnimation*>(m_pObserverAnim->GetNode());
			float playerTime = pAnimNode->GetAnimPlayer().GetTime();
			float deltaTime = startTime - playerTime;
			startTime = playerTime;

			if(m_PlayMode == RvAnimMgr::ANIM_PAUSE)
			{
				//We only want to do this when the time was changed by scrubbing the current frame manually...
				pAnimNode->GetAnimPlayer().SetDeltaHiddenSupplement( deltaTime );
			}
			else if(m_PlayMode == RvAnimMgr::ANIM_GOTO_START)
			{
				pAnimNode->GetAnimPlayer().SetDeltaHiddenSupplement( playbackStartTime - playerTime );
			}
			else if(m_PlayMode == RvAnimMgr::ANIM_GOTO_END)
			{
				pAnimNode->GetAnimPlayer().SetDeltaHiddenSupplement( playbackEndTime - playerTime );
			}
		}
		else if ( IsInClipMode() && m_pObserverClip->IsAttached() )
		{
			pClipNode = static_cast<crmtNodeClip*>(m_pObserverClip->GetNode());
			float playerTime = pClipNode->GetClipPlayer().GetTime();
			float deltaTime = startTime - playerTime;
			startTime = playerTime;

			if(m_PlayMode == RvAnimMgr::ANIM_PAUSE)
			{
				//We only want to do this when the time was changed by scrubbing the current frame manually...
				pClipNode->GetClipPlayer().SetDeltaHiddenSupplement( deltaTime );
			}
			else if(m_PlayMode == RvAnimMgr::ANIM_GOTO_START)
			{
				pClipNode->GetClipPlayer().SetDeltaHiddenSupplement( playbackStartTime - playerTime );
			}
			else if(m_PlayMode == RvAnimMgr::ANIM_GOTO_END)
			{
				pClipNode->GetClipPlayer().SetDeltaHiddenSupplement( playbackEndTime - playerTime );
			}
		}
	}

	// Compute the desired start and end times based on the playback mode
	switch(m_PlayMode)
	{
	case RvAnimMgr::ANIM_GOTO_START_AND_PLAY_FORWARD:
		{
			if ( m_pMotionTree->GetSkeleton() != NULL )
			{
				m_pMotionTree->GetSkeleton()->Reset();
			}
			startTime = playbackStartTime;
			endTime = startTime + TIME.GetSeconds() * framerateScalar;
			m_PlayMode = RvAnimMgr::ANIM_FORWARD;
		}
		break;
	case RvAnimMgr::ANIM_GOTO_START:
		{
			startTime = playbackStartTime;
			endTime = playbackStartTime;
			m_PlayMode = RvAnimMgr::ANIM_PAUSE;
		}
		break;
	case RvAnimMgr::ANIM_STEP_BACKWARD:
		{
			endTime = Max<float>(startTime - oneFrame, playbackStartTime);
			m_PlayMode = RvAnimMgr::ANIM_PAUSE;
		}
		break;
	case RvAnimMgr::ANIM_BACKWARD:
		{
			endTime = startTime - TIME.GetSeconds() * framerateScalar;
			
		}
		break;
	case RvAnimMgr::ANIM_PAUSE:
		{
			startTime = FrameToTime(m_CurrentFrame); // maybe it's being overridden via widgets?
			endTime = startTime;
		}
		break;
	case RvAnimMgr::ANIM_FORWARD:
		{
			endTime = startTime + TIME.GetSeconds() * framerateScalar;
		}
		break;
	case RvAnimMgr::ANIM_STEP_FORWARD:
		{
			endTime = Min<float>(startTime + oneFrame, playbackEndTime);
			m_PlayMode = RvAnimMgr::ANIM_PAUSE;
		}
		break;
	case RvAnimMgr::ANIM_GOTO_END:
		{
			startTime = playbackEndTime;
			endTime = playbackEndTime;
			m_PlayMode = RvAnimMgr::ANIM_PAUSE;
		}
		break;
	case RvAnimMgr::ANIM_GOTO_END_AND_PLAY_BACKWARD:
		{
			if ( m_pMotionTree->GetSkeleton() != NULL )
			{
				m_pMotionTree->GetSkeleton()->Reset();
			}
			startTime = playbackEndTime;
			endTime = startTime - TIME.GetSeconds() * framerateScalar;
			m_PlayMode = RvAnimMgr::ANIM_BACKWARD;
		}
	}

	// Clamp the end time to be within the correct range. Indicate whether or not we hit the end point

	// epsilon changes along with frame rate so if we are playing at 1fps the eps. for snapping to the end point
	// is much smaller than if we're at 30fps - so that when playing back slowly we don't get big jumps in the animation
	float epsilon = 0.001f * framerateScalar;


	if (endTime - playbackStartTime < epsilon) // If endTime is within epsilon of playbackEndTime or less, snap it to the start
	{
		if (m_PlayMode == ANIM_BACKWARD)
		{
			hitAnEnd = true;
		}
		endTime = playbackStartTime;
	}
	if (playbackEndTime - endTime < epsilon) // If endTime is within epsilon of playbackEndTime or greater, snap it to the end
	{
		if (m_PlayMode == ANIM_FORWARD)
		{
			hitAnEnd = true;
		}
		endTime = playbackEndTime;
	}

	// Based on the new start and end times, compute a new rate to pass down to the anim player

	float animRate = (endTime - startTime) / TIME.GetSeconds();

	// Set the new times/rates and update the motion tree

	m_CurrentFrame = TimeToFrame(endTime); 
	
	// Update the phase
	if( m_EndFrame - m_StartFrame > 0.f )
	{
		m_Phase = endTime / ((FrameToTime(m_EndFrame) - FrameToTime(m_StartFrame)));
	}
	else
	{
		m_Phase = 0.f;
	}
	
	if (pAnimNode)
	{
		pAnimNode->GetAnimPlayer().SetTime(startTime);
		pAnimNode->GetAnimPlayer().SetRate(animRate);
	}
	else if (pClipNode)
	{
		pClipNode->GetClipPlayer().SetTime(startTime);
		pClipNode->GetClipPlayer().SetRate(animRate);
	}

	if( (m_pMotionTree->GetSkeleton() != NULL) || (m_pMotionTree->GetCreature() != NULL) )
	{
		sm_MotionTreeScheduler.Schedule( *m_pMotionTree, TIME.GetSeconds() );
		sm_MotionTreeScheduler.WaitOnComplete( *m_pMotionTree );

		m_pMotionTree->GetCreature()->PostUpdate();
	}

	if (hitAnEnd)
	{
		if (m_Sequence)
		{
			if (IsInAnimMode())
			{
				int animIdx = Wrap(m_CurrentAnimIdx + (m_PlayMode == ANIM_FORWARD ? 1 : -1), 1, sm_AnimationList.GetCount()-1);
				if (animIdx != m_CurrentAnimIdx)
				{
					SetCurrentAnimationIndex(animIdx);
				}
			}
			else
			{
				int clipIdx = Wrap(m_CurrentClipIdx + (m_PlayMode == ANIM_FORWARD ? 1 : -1), 1, sm_ClipList.GetCount()-1);
				if (clipIdx != m_CurrentClipIdx)
				{
					SetCurrentClipIndex( clipIdx );
				}
			}
		}
		
		if (!m_bLoopAnimation && !m_Sequence)
		{
			m_PlayMode = ANIM_PAUSE;
			m_playModeChangedFtor(this);
		}
		else
		{
			if (m_PlayMode == ANIM_FORWARD)
			{
				m_PlayMode = ANIM_GOTO_START_AND_PLAY_FORWARD;
				
				if(m_MoverPlayMode == MOVER_ABSOLUTE)
					m_resetMoverFtor();
			}
			else 
			{
				m_PlayMode = ANIM_GOTO_END_AND_PLAY_BACKWARD;
				
				if(m_MoverPlayMode == MOVER_ABSOLUTE)
					m_resetMoverFtor();
			}
		}
		
	}

	if ( lastTime != endTime )
	{
		m_currentFrameChangedFtor(this);
	}
}

//-----------------------------------------------------------------------------

void RvAnimMgr::SetCurrentAnimationIndex( int animIdx )
{
    SetCurrentAnimationId( GetAnimationId( animIdx ) );
}

//-----------------------------------------------------------------------------

void RvAnimMgr::SetCurrentAnimationId( int animId )
{
    m_animMode = true;

	crAnimation *pAnimation = GetAnimationById( animId );
	if(m_pObserverAnim->IsAttached())
	{
		crmtNodeAnimation* pNodeAnim = static_cast<crmtNodeAnimation*>(m_pObserverAnim->GetNode());
		pNodeAnim->GetAnimPlayer().SetAnimation(pAnimation);
		pNodeAnim->GetAnimPlayer().SetTime(0.0f);
	}

	//Stop the clip if one is playing...
	if(m_pObserverClip->IsAttached())
	{
		crmtNodeClip* pNodeClip = static_cast<crmtNodeClip*>(m_pObserverClip->GetNode());
		pNodeClip->GetClipPlayer().SetClip(NULL);
	}

	if(pAnimation)
	{
		m_FrameIn.Invalidate();
		m_bFrameInDirty = true;

		m_PlayMode = ANIM_FORWARD;
        m_StartFrame = 0.0f;
        m_PlaybackStartFrame = m_StartFrame;
        m_EndFrame = pAnimation->ConvertTimeTo30Frame( pAnimation->GetDuration() );
        m_PlaybackEndFrame = m_EndFrame;
        m_StepSize = 1.0f;
		m_CurrentFrame = 0.0f;
	}
	else
	{
		m_PlayMode = ANIM_PAUSE;
        m_StartFrame = 0.0f;
        m_PlaybackStartFrame = 0.0f;
        m_EndFrame = 0.0f;
        m_PlaybackEndFrame = 0.0f;
        m_StepSize = 0.0f;
		m_CurrentFrame = 0.0f;
	}

    m_CurrentAnimIdx = GetAnimationIndex( pAnimation );
}

//-----------------------------------------------------------------------------

void RvAnimMgr::SetCurrentClipIndex( int clipIdx )
{
    SetCurrentClipId( GetClipId( clipIdx ) );
}

//-----------------------------------------------------------------------------

void RvAnimMgr::SetCurrentClipId( int clipId )
{
    m_animMode = false;

    crClip *pClip = GetClipById( clipId );

	if(m_pObserverClip->IsAttached())
	{
		crmtNodeClip* pNodeClip = static_cast<crmtNodeClip*>(m_pObserverClip->GetNode());
		pNodeClip->GetClipPlayer().SetClip(pClip);
		pNodeClip->GetClipPlayer().SetTime(0.0f);
	}

	//Stop the animation if one is playing...
	if(m_pObserverAnim->IsAttached())
	{
		crmtNodeAnimation* pNodeAnim = static_cast<crmtNodeAnimation*>(m_pObserverAnim->GetNode());
		pNodeAnim->GetAnimPlayer().SetAnimation(NULL);
	}

	if(pClip)
	{
		m_FrameIn.Invalidate();
		m_bFrameInDirty = true;

		m_PlayMode = ANIM_FORWARD;
        m_StartFrame = 0.0f;
        m_PlaybackStartFrame = m_StartFrame;

        m_EndFrame = pClip->ConvertTimeTo30Frame( pClip->GetDuration() );
        m_PlaybackEndFrame = m_EndFrame;
        m_StepSize = 1.0f;

		m_CurrentFrame = 0.0f;
	}
	else
	{
		m_PlayMode = ANIM_PAUSE;
        m_StartFrame = 0.0f;
        m_PlaybackStartFrame = 0.0f;
        m_EndFrame = 0.0f;
        m_PlaybackEndFrame = 0.0f;
        m_StepSize = 0.0f;
		m_CurrentFrame = 0.0f;
	}

    m_CurrentClipIdx = GetClipIndex( pClip );
}

//-----------------------------------------------------------------------------

void RvAnimMgr::SetSequence(bool sequence)
{
    m_Sequence = sequence;
}

//-----------------------------------------------------------------------------

void RvAnimMgr::BankCbkAnimCtrlCurrentFrameChanged()
{
	m_currentFrameChangedFtor( this );

	//Anytime the user is manually changing the frame (e.g. through scrubbing)
	//we should automatically switch into pause mode
	/*m_PlayMode = RvAnimMgr::ANIM_PAUSE;
	m_playModeChangedFtor(this);*/
}

void RvAnimMgr::BankCbkAnimCtrlPhaseChanged()
{

}

void RvAnimMgr::BankCbkAnimCtrlAnimOrClipToggleChanged()
{
    if ( m_animMode )
    {
        BankCbkAnimCtrlAnimIndexChanged();
    }
    else
    {
        BankCbkAnimCtrlClipIndexChanged();
    }
}

void RvAnimMgr::BankCbkAnimCtrlAnimIndexChanged()
{
    SetCurrentAnimationIndex( m_CurrentAnimIdx );
	m_animIndexChangedFtor( this );
}

void RvAnimMgr::BankCbkAnimCtrlClipIndexChanged()
{
    SetCurrentClipIndex( m_CurrentClipIdx );
	m_clipIndexChangedFtor(this);
}

void RvAnimMgr::BankCbkAnimCtrlGotoStart()
{
	m_PlayMode = ANIM_GOTO_START;
	m_playModeChangedFtor(this);
//	m_currentFrameChangedFtor( this );
}

void RvAnimMgr::BankCbkAnimCtrlStepBack()
{
	m_PlayMode = ANIM_STEP_BACKWARD;
	m_playModeChangedFtor(this);
//	m_currentFrameChangedFtor( this );
}

void RvAnimMgr::BankCbkAnimCtrlPlayBackward()
{
	m_PlayMode = ANIM_BACKWARD;
	m_playModeChangedFtor(this);
}

void RvAnimMgr::BankCbkAnimCtrlPause()
{
	m_PlayMode = ANIM_PAUSE;
	m_playModeChangedFtor(this);
}

void RvAnimMgr::BankCbkAnimCtrlPlayForward()
{
	m_PlayMode = ANIM_FORWARD;
	m_playModeChangedFtor(this);
}

void RvAnimMgr::BankCbkAnimCtrlStepForward()
{
	m_PlayMode = ANIM_STEP_FORWARD;
	m_playModeChangedFtor(this);
	m_currentFrameChangedFtor( this );
}

void RvAnimMgr::BankCbkAnimCtrlGotoEnd()
{
	m_PlayMode = ANIM_GOTO_END;
	m_playModeChangedFtor(this);
	m_currentFrameChangedFtor( this );
}

void RvAnimMgr::BankCbkSequenceModeChanged()
{
	m_sequenceModeChangedFtor(this);
}

void RvAnimMgr::BankCbkLoopAnimationChanged()
{
	m_loopAnimationChangedFtor(this);
}

void RvAnimMgr::BankCbkMoverModeChanged()
{
	m_moverModeChangedFtor(this);
}

void RvAnimMgr::BankCbkResetMover()
{
	m_resetMoverFtor();
}

void RvAnimMgr::BankCbkResetAndPlay()
{
	m_resetMoverFtor();

	m_PlayMode = ANIM_GOTO_START_AND_PLAY_FORWARD;
	m_playModeChangedFtor(this);
	m_currentFrameChangedFtor( this );
}

void RvAnimMgr::BankCbkAllowMoverChanged()
{
	m_allowMoverChangedFtor(this);
}

void RvAnimMgr::BankCbkAllowRootTranslationChanged()
{
	m_allowRootTranslationChangedFtor(this);
}

void RvAnimMgr::BankCbkMoverDebugDrawChanged()
{
	m_moverDebugDrawChangedFtor(this);
}

void RvAnimMgr::BankCbkPlaybackStartTimeChanged()
{
	m_frameRangeChangedFtor(this);
}

void RvAnimMgr::BankCbkPlaybackEndTimeChanged()
{
	m_frameRangeChangedFtor(this);
}

void RvAnimMgr::BankCbkAnimStartTimeChanged()
{

}

void RvAnimMgr::BankCbkAnimEndTimeChanged()
{
	
}

void RvAnimMgr::AnimListCbkDoubleClick(s32 key)
{
	sm_AnimSelectedFtor(key);
}

void RvAnimMgr::ClipListCbkDoubleClick(s32 key)
{
	sm_ClipSelectedFtor(key);
}

void RvAnimMgr::UpdateClipEditorTimeWindow()
{
}

void RvAnimMgr::UpdateClipEditorCurrentTime()
{
}

#if __BANK

void RvAnimMgr::InitWidgets( bkBank &bk )
{
    sm_pBankAnimList = bk.AddList( "Loaded Animations" );

	bkList::ClickItemFuncType animDblClickFtor;
	animDblClickFtor.Reset<&RvAnimMgr::AnimListCbkDoubleClick>();
	sm_pBankAnimList->SetDoubleClickItemFunc( animDblClickFtor );

    sm_pBankAnimList->AddColumnHeader( 0, "Id", bkList::INT );
    sm_pBankAnimList->AddColumnHeader( 1, "Name", bkList::STRING );

    sm_pBankAnimList->AddItem( 0, 0, 0 );
    sm_pBankAnimList->AddItem( 0, 1, "(none) - Base Pose" );

    //Add widgets for any animations that may have already been loaded from the command line
    int animCount = GetAnimationCount();
    for ( int i = 1; i < animCount; ++i )
    {
        crAnimation *pAnim = GetAnimation( i );
        if ( pAnim )
        {
            int key = GetAnimationId( i );
            sm_pBankAnimList->AddItem( key, 0, key );
            sm_pBankAnimList->AddItem( key, 1, pAnim->GetName() );
        }
    }

    sm_pBankClipList = bk.AddList( "Loaded Clips" );

	bkList::ClickItemFuncType clipDblClickFtor;
	clipDblClickFtor.Reset<&RvAnimMgr::ClipListCbkDoubleClick>();
	sm_pBankClipList->SetDoubleClickItemFunc(clipDblClickFtor);

    sm_pBankClipList->AddColumnHeader( 0, "Id", bkList::INT );
    sm_pBankClipList->AddColumnHeader( 1, "Name", bkList::STRING );

    sm_pBankClipList->AddItem( 0, 0, 0 );
    sm_pBankClipList->AddItem( 0, 1, "(none) - Base Pose" );

    //Add widgets for any animations that may have already been loaded from the command line
    int clipCount = GetClipCount();
    for ( int i = 1; i < clipCount; ++i )
    {
        crClip *pClip = GetClip( i );
        if ( pClip )
        {
            int key = GetClipId( i );
            sm_pBankClipList->AddItem( key, 0, key );
            sm_pBankClipList->AddItem( key, 1, pClip->GetName() );
        }
    }
}

void RvAnimMgr::AddWidgets( bkBank &bk )
{
    m_pAnimOrClipToggle = bk.AddToggle( "Use Anim Index", &m_animMode, 
        datCallback(MFA(RvAnimMgr::BankCbkAnimCtrlAnimOrClipToggleChanged),this) );
    m_pAnimIdxSlider = bk.AddSlider("Anim Index", &m_CurrentAnimIdx, 0, 10000, 1, 
        datCallback(MFA(RvAnimMgr::BankCbkAnimCtrlAnimIndexChanged),this) );
    m_pClipIdxSlider = bk.AddSlider("Clip Index", &m_CurrentClipIdx, 0, 10000, 1, 
        datCallback(MFA(RvAnimMgr::BankCbkAnimCtrlClipIndexChanged),this) );
	m_pSeqToggle = bk.AddToggle( "Sequence Animations/Clips", &m_Sequence, datCallback(MFA(RvAnimMgr::BankCbkSequenceModeChanged),this) );
	
    m_pRateSlider = bk.AddSlider( "Frame Rate", &m_FrameRate, 0.0f, 60.0f, 1.0f );
    m_pTimeSlider = bk.AddSlider( "Current Frame", &m_CurrentFrame, 0.0f, 10000.0f, 0.1f,
		datCallback(MFA(RvAnimMgr::BankCbkAnimCtrlCurrentFrameChanged),this) );
	m_pPhaseSlider = bk.AddSlider( "Phase", &m_Phase, 0.0f, 1.0f, 0.01f,
		datCallback(MFA(RvAnimMgr::BankCbkAnimCtrlPhaseChanged),this) );
    m_pStartTimeSlider = bk.AddSlider( "Start Frame", &m_StartFrame, 0.0f, 10000.0f, 0.0f, 
        datCallback(MFA(RvAnimMgr::BankCbkAnimStartTimeChanged),this) );
    m_pEndTimeSlider = bk.AddSlider( "End Frame", &m_EndFrame, 0.0f, 10000.0f, 0.0f, 
        datCallback(MFA(RvAnimMgr::BankCbkAnimEndTimeChanged),this) );
    m_pPlaybackStartTimeSlider = bk.AddSlider( "Playback Start Frame", &m_PlaybackStartFrame, 0.0f, 10000.0f, 0.1f, 
        datCallback(MFA(RvAnimMgr::BankCbkPlaybackStartTimeChanged),this) );
    m_pPlaybackEndTimeSlider = bk.AddSlider( "Playback End Frame", &m_PlaybackEndFrame, 0.0f, 10000.0f, 0.1f, 
        datCallback(MFA(RvAnimMgr::BankCbkPlaybackEndTimeChanged),this) );
    m_pStepSizeSlider = bk.AddSlider( "Step Size", &m_StepSize, 0.01f, 10.0f, 0.01f );

    m_pGotoStartButton = bk.AddButton( "Goto Start", datCallback(MFA(RvAnimMgr::BankCbkAnimCtrlGotoStart),this) );
    m_pStepBackButton = bk.AddButton( "Step Back", datCallback(MFA(RvAnimMgr::BankCbkAnimCtrlStepBack),this) );
    m_pPlayBackwardsButton = bk.AddButton( "Play Backwards", datCallback(MFA(RvAnimMgr::BankCbkAnimCtrlPlayBackward),this) );
    m_pPauseButton = bk.AddButton( "Pause", datCallback(MFA(RvAnimMgr::BankCbkAnimCtrlPause),this) );
    m_pPlayForwaredsButton = bk.AddButton( "Play Forwards", datCallback(MFA(RvAnimMgr::BankCbkAnimCtrlPlayForward),this) );
    m_pStepForwardsButton = bk.AddButton( "Step Forward", datCallback(MFA(RvAnimMgr::BankCbkAnimCtrlStepForward), this) );
    m_pGotoEndButton = bk.AddButton( "Goto End", datCallback(MFA(RvAnimMgr::BankCbkAnimCtrlGotoEnd),this) );

	m_pLoopAnimationToggle = bk.AddToggle("Loop Animation", &m_bLoopAnimation, datCallback(MFA(RvAnimMgr::BankCbkLoopAnimationChanged), this) );

	m_pResetMoverButton = bk.AddButton( "Reset Mover", datCallback(MFA(RvAnimMgr::BankCbkResetMover), this) );

	bk.AddButton("Reset And Play", datCallback(MFA(RvAnimMgr::BankCbkResetAndPlay), this));
	
	static const char* moverModeList[] = {"Continuous","Absolute"};
	m_pMoverModeCombo = bk.AddCombo("Mover Mode", &m_MoverPlayMode, 2, moverModeList,0,datCallback(MFA(RvAnimMgr::BankCbkMoverModeChanged),this));

	m_pAllowMoverToggle = bk.AddToggle("Allow Mover", &m_Filter.m_AllowMover, datCallback(MFA(RvAnimMgr::BankCbkAllowMoverChanged),this) );
	m_pAllowRootTranslationToggle = bk.AddToggle("Allow Root Trans", &m_Filter.m_AllowRootTranslation, datCallback(MFA(RvAnimMgr::BankCbkAllowRootTranslationChanged),this) );
	m_pMoverDebugDraw = bk.AddToggle("Mover Debug Draw", &m_bMoverDebugDraw, datCallback(MFA(RvAnimMgr::BankCbkMoverDebugDrawChanged),this) );
}

void RvAnimMgr::RemoveWidgets( bkBank &bk )
{
    if ( m_pGotoEndButton )
    {
        bk.Remove( *m_pGotoEndButton );
        m_pGotoEndButton = NULL;
    }

    if ( m_pStepForwardsButton )
    {
        bk.Remove( *m_pStepForwardsButton );
        m_pStepForwardsButton = NULL;
    }

    if ( m_pPlayForwaredsButton )
    {
        bk.Remove( *m_pPlayForwaredsButton );
        m_pPlayForwaredsButton = NULL;
    }

    if ( m_pPauseButton )
    {
        bk.Remove( *m_pPauseButton );
        m_pPauseButton = NULL;
    }

    if ( m_pPlayBackwardsButton )
    {
        bk.Remove( *m_pPlayBackwardsButton );
        m_pPlayBackwardsButton = NULL;
    }

    if ( m_pStepBackButton )
    {
        bk.Remove( *m_pStepBackButton );
        m_pStepBackButton = NULL;
    }

    if ( m_pGotoStartButton )
    {
        bk.Remove( *m_pGotoStartButton );
        m_pGotoStartButton = NULL;
    }

    if ( m_pStepSizeSlider )
    {
        bk.Remove( *m_pStepSizeSlider );
        m_pStepSizeSlider = NULL;
    }

    if ( m_pPlaybackEndTimeSlider )
    {
        bk.Remove( *m_pPlaybackEndTimeSlider );
        m_pPlaybackEndTimeSlider = NULL;
    }

    if ( m_pPlaybackStartTimeSlider )
    {
        bk.Remove( *m_pPlaybackStartTimeSlider );
        m_pPlaybackStartTimeSlider = NULL;
    }

    if ( m_pEndTimeSlider )
    {
        bk.Remove( *m_pEndTimeSlider );
        m_pEndTimeSlider = NULL;
    }

    if ( m_pStartTimeSlider )
    {
        bk.Remove( *m_pStartTimeSlider );
        m_pStartTimeSlider = NULL;
    }

    if ( m_pTimeSlider )
    {
        bk.Remove( *m_pTimeSlider );
        m_pTimeSlider = NULL;
    }

    if ( m_pRateSlider )
    {
        bk.Remove( *m_pRateSlider );
        m_pRateSlider = NULL;
    }

    if ( m_pSeqToggle )
    {
        bk.Remove( *m_pSeqToggle );
        m_pSeqToggle = NULL;
    }

    if ( m_pClipIdxSlider )
    {
        bk.Remove( *m_pClipIdxSlider );
        m_pClipIdxSlider = NULL;
    }

    if ( m_pAnimIdxSlider )
    {
        bk.Remove( *m_pAnimIdxSlider );
        m_pAnimIdxSlider = NULL;
    }

    if ( m_pAnimOrClipToggle )
    {
        bk.Remove( *m_pAnimOrClipToggle );
        m_pAnimOrClipToggle = NULL;
    }
}

//-----------------------------------------------------------------------------

void RvAnimMgr::SaveAllClips()
{
	int nClips = GetClipCount();
	for (int clipIdx = 0; clipIdx < nClips; clipIdx++)
	{
		crClip*	pClip = GetClip(clipIdx);
		if(pClip)
		{
			SaveClip(pClip);
		}
	}
}

void RvAnimMgr::SaveClip(crClip* pClip)
{
	pClip->Save(pClip->GetName());
}

void RvAnimMgr::SaveClipAs(crClip* pClip)
{
	char filename[RAGE_MAX_PATH];
	safecpy(filename, pClip->GetName() ? pClip->GetName() : "new_clip");
	if (BANKMGR.OpenFile(filename, RAGE_MAX_PATH, "*.clip", true, "Clip files (.clip)"))
	{
		pClip->Save(filename);
	}
}

void RvAnimMgr::SaveClipTemplate(crClip* pClip)
{
	if (!pClip)
	{
		return;
	}

	char filename[RAGE_MAX_PATH];
	safecpy(filename, pClip->GetName() ? pClip->GetName() : "new_clip");
	atString origName(pClip->GetName());

	char comment[RAGE_MAX_PATH];
	formatf(comment, "Template of %s", origName.c_str());

	if (BANKMGR.OpenFile(filename, RAGE_MAX_PATH, "*.clip", true, "Clip files (.clip)"))
	{
		pClip->Save(filename);
		pClip->SetName(origName);
	}
}

void RvAnimMgr::LoadClipTemplate(crClip* pClip)
{
	char filename[RAGE_MAX_PATH];
	safecpy(filename, "new_template");

	if (BANKMGR.OpenFile(filename, RAGE_MAX_PATH, "*.clip", false, "Clip files (.clip)"))
	{
		crClip* newClip = crClip::AllocateAndLoad(filename);

		atArray<const crClip*> sourceClips;
		sourceClips.PushAndGrow(newClip, 1);

		crClipCombine::CombineClips(sourceClips, pClip);

		sourceClips.Reset();

		delete newClip;
	}
}

#endif //__BANK

//-----------------------------------------------------------------------------

}//End namespace ragesamples
