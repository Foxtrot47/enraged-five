set ARCHIVE=sample_simpleworld
set FILES=sample_simpleworld simpleworld entitymgr simplescript animmgr frameEditor
set TESTERS=sample_multidrawable sample_hdr
set HEADONLY=bucketcontrol
set SAMPLE_LIBS=%RAGE_SAMPLE_LIBS% sample_physics sample_fragment
set LIBS=%SAMPLE_LIBS% %RAGE_GFX_LIBS% %RAGE_CORE_LIBS% %RAGE_CR_LIBS% %RAGE_SUITE_CR_LIBS% %RAGE_AUD_LIBS%
set LIBS=%LIBS% spatialdata grshadowmap cliptools
set LIBS=%LIBS% %RAGE_PH_LIBS% breakableglass phglass fragment cloth grrope rmptfx event eventtypes %RAGE_SCRIPT_LIBS% vieweraudio
set XPROJ=%RAGE_DIR%\suite\src %RAGE_DIR%\suite\samples %RAGE_DIR%\base\src %RAGE_DIR%\base\samples %RAGE_DIR%\script\src %RAGE_DIR%\suite\tools %RAGE_DIR%\suite\tools\cli
set PARSE=entitymgr
set EMBEDDEDSHADERS=rageViewer_debug.fx
set CUSTOM=embedded_rageViewer_debug_win32_30.h embedded_rageViewer_debug_psn.h embedded_rageViewer_debug_fxl_final.h