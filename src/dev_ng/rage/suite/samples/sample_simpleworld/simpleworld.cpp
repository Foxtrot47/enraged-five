// 
// sample_simpleworld/simpleworld.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "simpleworld.h"

#include "entitymgr.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "crskeleton/skeletondata.h"
#include "fragment/fragshaft.h"
#include "grcore/state.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"
#include "grpostfx/postfx.h"
#include "math/amath.h"
#include "grprofile/drawmanager.h"
#include "rmcore/drawable.h"
#include "rmcore/drawable.h"
#include "spatialdata/sphere.h"
#include "system/param.h"
#include "system/timemgr.h"

using namespace rage;

namespace ragesamples
{


rmcShadowObjectList::rmcShadowObjectList()
{
	m_ShadowCasters.Reserve(50);
	m_MaxBuckets = 16; //NUM_BUCKETS;
}

// add shadow casters
void rmcShadowObjectList::AddShadowCastingObject(rmcDrawable*drawable,crSkeleton*skeleton,Matrix34 *matrix,grmMatrixSet *ms)
{
	rmcDrawableObject & newObj = m_ShadowCasters.Grow();
	newObj.m_Drawable = drawable;
	newObj.m_Skeleton = skeleton;
	newObj.m_MatrixSet = ms;
	newObj.m_Matrix = matrix;
}


void rmcShadowObjectList::RemoveShadowCastingObjects()
{
	if(m_ShadowCasters.GetCount())
		m_ShadowCasters.Reset();
}

void rmcShadowObjectList::RemoveShadowCastingObject(rmcDrawable *drawable)
{
	int shadowCasterCnt = m_ShadowCasters.GetCount();
	for(int i=0 ; i<shadowCasterCnt; i++)
	{
		if(m_ShadowCasters[i].m_Drawable == drawable)
		{
			m_ShadowCasters.Delete(i);
			break;
		}
	}
}


void RenderWithCulling( grcViewport& vp, rmcDrawable* drawable,  const Matrix34& mtx, int lodIndex )
{
	if ( drawable->GetBucketMask(lodIndex) != 1 ) // only render opaque objects into the shadow maps
	{
		return; 
	}
	const Matrix34* mtxList = &mtx;
	grcViewport::SetCurrentWorldMtx(RCC_MAT34V(mtxList[0]));
	int lastMtx = -2;

	const rmcLod& lod=drawable->GetLodGroup().GetLod(lodIndex);
	for (int i=lod.GetCount()-1;i>=0;i--)
	{
		const grmModel& model=*lod.GetModel(i);

		int idx = model.GetMatrixIndex();

		if (idx != lastMtx)
		{
			Assert( idx >=0 && idx <= 1024 );
			grcViewport::SetCurrentWorldMtx(RCC_MAT34V(mtxList[lastMtx = idx]));
		}

		const spdAABB *AABBs =  model.GetAABBs();

		if ( AABBs && model.GetGeometryCount() > 1  )
		{
			for (int j=model.GetGeometryCount()-1;j>=0;j--)
			{
				if (vp.IsSphereVisible(AABBs[j + 1].GetBoundingSphere().GetV4()))
				{
					model.Draw(j);
				}
			}
		}
		else
		{
			for (int j=model.GetGeometryCount()-1;j>=0;j--)
			{
				model.Draw(j);
			}
		}
	}
}

const int g_IndexList[64][7] =
{
	{-1,-1,-1,-1,-1,-1,   0}, // 0 inside
	{ 0, 4, 7, 3,-1,-1,   4}, // 1 left
	{ 1, 2, 6, 5,-1,-1,   4}, // 2 right
	{-1,-1,-1,-1,-1,-1,   0}, // 3 -
	{ 0, 1, 5, 4,-1,-1,   4}, // 4 bottom
	{ 0, 1, 5, 4, 7, 3,   6}, // 5 bottom, left
	{ 0, 1, 2, 6, 5, 4,   6}, // 6 bottom, right
	{-1,-1,-1,-1,-1,-1,   0}, // 7 -
	{ 2, 3, 7, 6,-1,-1,   4}, // 8 top
	{ 0, 4, 7, 6, 2, 3,   6}, // 9 top, left
	{ 1, 2, 3, 7, 6, 5,   6}, //10 top, right
	{-1,-1,-1,-1,-1,-1,   0}, //11 -
	{-1,-1,-1,-1,-1,-1,   0}, //12 -
	{-1,-1,-1,-1,-1,-1,   0}, //13 -
	{-1,-1,-1,-1,-1,-1,   0}, //14 -
	{-1,-1,-1,-1,-1,-1,   0}, //15 -
	{ 0, 3, 2, 1,-1,-1,   4}, //16 front
	{ 0, 4, 7, 3, 2, 1,   6}, //17 front, left
	{ 0, 3, 2, 6, 5, 1,   6}, //18 front, right
	{-1,-1,-1,-1,-1,-1,   0}, //19 -
	{ 0, 3, 2, 1, 5, 4,   6}, //20 front, bottom
	{ 1, 5, 4, 7, 3, 2,   6}, //21 front, bottom, left
	{ 0, 3, 2, 6, 5, 4,   6}, //22 front, bottom, right
	{-1,-1,-1,-1,-1,-1,   0}, //23 -
	{ 0, 3, 7, 6, 2, 1,   6}, //24 front, top
	{ 0, 4, 7, 6, 2, 1,   6}, //25 front, top, left
	{ 0, 3, 7, 6, 5, 1,   6}, //26 front, top, right
	{-1,-1,-1,-1,-1,-1,   0}, //27 -
	{-1,-1,-1,-1,-1,-1,   0}, //28 -
	{-1,-1,-1,-1,-1,-1,   0}, //29 -
	{-1,-1,-1,-1,-1,-1,   0}, //30 -
	{-1,-1,-1,-1,-1,-1,   0}, //31 -
	{ 4, 5, 6, 7,-1,-1,   4}, //32 back
	{ 0, 4, 5, 6, 7, 3,   6}, //33 back, left
	{ 1, 2, 6, 7, 4, 5,   6}, //34 back, right
	{-1,-1,-1,-1,-1,-1,   0}, //35 -
	{ 0, 1, 5, 6, 7, 4,   6}, //36 back, bottom
	{ 0, 1, 5, 6, 7, 3,   6}, //37 back, bottom, left
	{ 0, 1, 2, 6, 7, 4,   6}, //38 back, bottom, right
	{-1,-1,-1,-1,-1,-1,   0}, //39 -
	{ 2, 3, 7, 4, 5, 6,   6}, //40 back, top
	{ 0, 4, 5, 6, 2, 3,   6}, //41 back, top, left
	{ 1, 2, 3, 7, 4, 5,   6}, //42 back, top, right
	{-1,-1,-1,-1,-1,-1,   0}, //43 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //44 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //45 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //46 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //47 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //48 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //49 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //50 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //51 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //52 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //53 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //54 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //55 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //56 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //57 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //58 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //59 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //60 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //61 invalid
	{-1,-1,-1,-1,-1,-1,   0}, //62 invalid
	{-1,-1,-1,-1,-1,-1,   0}  //63 invalid
};


//----------------------------------------------------------------------------
// CalculateBoxArea: computes the screen-projected 2D area of an oriented 3D
// bounding box
//----------------------------------------------------------------------------

float CalculateBoxArea(
	const Vector3&		eye,    //eye point (in bbox object coordinates)
	const Vector3&		min,	//3d bbox
	const Vector3&		max,
	const Matrix34&		boxLocalToWorld, //free transformation for bbox
	const Matrix44&		fullViewProjectionScreenMtx) //view volume
{
	//compute 6-bit code to classify eye with respect to the 6 defining planes
	//of the bbox
	int pos = ((eye[0] < min[0]) ?  1 : 0)   // 1 = left
		+ ((eye[0] > max[0]) ?  2 : 0)   // 2 = right
		+ ((eye[1] < min[1]) ?  4 : 0)   // 4 = bottom
		+ ((eye[1] > max[1]) ?  8 : 0)   // 8 = top
		+ ((eye[2] < min[2]) ? 16 : 0)   // 16 = front
		+ ((eye[2] > max[2]) ? 32 : 0);  // 32 = back

	int num = g_IndexList[pos][6]; //look up number of vertices in outline
	if (!num) return -1.0;       //zero indicates invalid case, return -1

	Vector3 vertexBox[8],dst[8];
	//generate 8 corners of the bbox
	vertexBox[0] = Vector3 (min[0],min[1],min[2]); //     7+------+6
	vertexBox[1] = Vector3 (max[0],min[1],min[2]); //     /|     /|
	vertexBox[2] = Vector3 (max[0],max[1],min[2]); //    / |    / |
	vertexBox[3] = Vector3 (min[0],max[1],min[2]); //   / 4+---/--+5
	vertexBox[4] = Vector3 (min[0],min[1],max[2]); // 3+------+2 /    y   z
	vertexBox[5] = Vector3 (max[0],min[1],max[2]); //  | /    | /     |  /
	vertexBox[6] = Vector3 (max[0],max[1],max[2]); //  |/     |/      |/
	vertexBox[7] = Vector3 (min[0],max[1],max[2]); // 0+------+1      *---x

	Matrix44 compositeMtx;
	compositeMtx.Identity();
	compositeMtx.FromMatrix34(boxLocalToWorld);
	compositeMtx.Dot(fullViewProjectionScreenMtx);

	float sum = 0; int i;
	for(i=0; i<num; i++) //transform all outline corners into 2D screen space
	{
		dst[i] = compositeMtx.FullTransform(vertexBox[g_IndexList[pos][i]]); // Project into screen space
	}

	sum = (dst[num-1][0] - dst[0][0]) * (dst[num-1][1] + dst[0][1]);
	for (i=0; i<num-1; i++)
		sum += (dst[i][0] - dst[i+1][0]) * (dst[i][1] + dst[i+1][1]);

	return Abs(sum * 0.5f); //return computed value corrected by 0.5
}

float CalculateSphereArea(Vector3& spherePosition, float sphereRadius, const grcViewport* viewport)
{
	float screenRadius;

	// Access sphere, early out for invalid sphere
	if (sphereRadius <= 0.0f)
	{
		return 0.0f;
	}

	Vector4 sphereRadiusProjected(0.0f, 0.0f, sphereRadius, 1.0f);
	RCC_MATRIX44(viewport->GetProjection()).Transform(sphereRadiusProjected, sphereRadiusProjected);
	RCC_MATRIX44(viewport->GetScreenMtx()).Transform(sphereRadiusProjected, sphereRadiusProjected);
	sphereRadius = sphereRadiusProjected[2];

	// Compute the area of the projected bounding sphere
	if (viewport->IsPerspective())
	{
		const Vector4 c = VEC4V_TO_VECTOR4(viewport->GetViewMtx().c());
		const float zDist = spherePosition[0] * c[0] + spherePosition[1] * c[1] + spherePosition[2] * c[2] + c[3];
		
		if (zDist == 0.0f)
		{
			return 1.0f;
		}

		screenRadius = viewport->GetNearClip() / zDist * sphereRadius;

	}
	else
	{
		screenRadius = sphereRadius;
	}

	const float screenAreaRecip = 1.0f / (viewport->GetWidth() * viewport->GetHeight());
	return screenRadius * screenRadius * PI * screenAreaRecip;
}
void rmcShadowObjectList::RenderBounds()
{
	for ( int i = 0; i < m_ShadowCasters.GetCount(); i++ )
	{
		const rmcLodGroup& lodGroup = m_ShadowCasters[i].m_Drawable->GetLodGroup();
		grcDrawSphere( lodGroup.GetCullRadius(), lodGroup.GetCullSphere() + m_ShadowCasters[i].m_Matrix->d, 8, true );
	}
}

#define USEPROJECTEDAREACULLING (0)

#if USEPROJECTEDAREACULLING
static float g_ScreenSpaceProportion = 0.01f;
#endif // USEPROJECTEDAREACULLING

void rmcShadowObjectList::RenderShadowCasters(grcViewport& vp, bool isCasting )
{
	shaft.Set(vp, RCC_MATRIX34(vp.GetCameraMtx()));

	grmShader *forceShader=grmModel::GetForceShader();
	Assert(forceShader);
	static bool ignoreVisTests = false;
	const int maxLod = LOD_MED;

	int numPasses=0;

	grcLightState::SetEnabled(true);

	const grcViewport* currentViewport = grcViewport::GetCurrent();
	Assert( currentViewport == &vp );

	Matrix44 fullViewProjectionScreenMtx = RCC_MATRIX44(currentViewport->GetViewMtx());
	fullViewProjectionScreenMtx.Dot(RCC_MATRIX44(currentViewport->GetProjection()));
	fullViewProjectionScreenMtx.Dot(RCC_MATRIX44(currentViewport->GetScreenMtx()));

#if USEPROJECTEDAREACULLING
	const float screenSpaceThreshold = currentViewport->GetWidth() * currentViewport->GetHeight() * g_ScreenSpaceProportion;
#endif // USEPROJECTEDAREACULLING

	// draw non-skinned objects:
	numPasses = forceShader->BeginDraw(grmShader::RMC_DRAW, true);
	for (int j=0;j<numPasses;j++)
	{
		forceShader->Bind(j);
		// draw the shadow caster objects
		for(int i=0; i < m_ShadowCasters.GetCount(); i++)
		{
			// Set the Bounding Sphere for frustum culling
			spdSphere sphere;
			
			const rmcLodGroup& lodGroup = m_ShadowCasters[i].m_Drawable->GetLodGroup();

			// this is in model space
			Vector3 sphereCenter = lodGroup.GetCullSphere();
			
			// transform this to world space ... only translate the spherecenter, because there is no concept of rotation for spheres
			sphereCenter.Add(m_ShadowCasters[i].m_Matrix->d);
			
			// sphere center & radius
			sphere.Set(RCC_VEC3V(sphereCenter), ScalarV(lodGroup.GetCullRadius()));

			Vector4 Sphere = VEC3V_TO_VECTOR4(sphere.GetV4());

			//if (vp.IsSphereVisible(Sphere) || ignoreVisTests )
			if ( shaft.IsVisible( sphere ) || ignoreVisTests )
			{
				bool screenSizeTestResult = true;
#if USEPROJECTEDAREACULLING
				Vector3 boxMin;
 				Vector3 boxMax;
 				lodGroup.GetBoundingBox(boxMin, boxMax);
				if (!(boxMin == VEC3_ZERO & boxMax == VEC3_ZERO))  // Filter out sketchy bounding boxes
				{
					const Matrix34& casterLocalToWorld = *m_ShadowCasters[i].m_Matrix;
					Matrix34 casterWorldToLocal;
					casterWorldToLocal.Inverse(casterLocalToWorld);

					Vector3 eyeLocal = currentViewport->GetCameraMtx().d;
					casterWorldToLocal.Transform(eyeLocal);

					screenSizeTestResult = CalculateBoxArea(eyeLocal, boxMin, boxMax, casterLocalToWorld, fullViewProjectionScreenMtx) >= screenSpaceThreshold;
				}
				else
				{
					screenSizeTestResult = CalculateSphereArea(sphereCenter, sphere.GetRadius(), currentViewport) >= screenSpaceThreshold;
				}
#endif // USEPROJECTEDAREACULLING

				if (screenSizeTestResult || ignoreVisTests)
				{
					float camDist = SMPWORLDEMGR.DistanceFromCamera(m_ShadowCasters[i].m_Matrix->d);
					int lod = lodGroup.ComputeLod( camDist );
					if (lodGroup.ContainsLod(lod))
					{
						if (lod > maxLod && lodGroup.ContainsLod(maxLod))
						{
							lod = maxLod;
						}
					}
					
					if (!lodGroup.ContainsLod(lod))
					{
						lod = 0;
					}

					if(m_ShadowCasters[i].m_Skeleton) 
					{
						bool IsSkinned =  lodGroup.GetModel(lod).GetSkinFlag(); // TODO - replace with proper lod
						if ( IsSkinned )
						{
							m_ShadowCasters[i].m_Drawable->DrawNoShadersSkinned(RCC_MATRIX34(*m_ShadowCasters[i].m_Skeleton->GetParentMtx()), *m_ShadowCasters[i].m_MatrixSet, lod, rmcDrawable::RENDER_NONSKINNED);
						}
						else
						{
							RenderWithCulling( vp, m_ShadowCasters[i].m_Drawable, RCC_MATRIX34(*m_ShadowCasters[i].m_Skeleton->GetParentMtx()), lod);
						}
					}
					else
					{
						RenderWithCulling( vp, m_ShadowCasters[i].m_Drawable, *m_ShadowCasters[i].m_Matrix, lod);
					}
				}
			}
		}

		m_ExternalNonSkinnedDrawDelegator.Dispatch(vp, false);

		forceShader->UnBind();
	}
	forceShader->EndDraw();

	// draw skinned objects:
	numPasses = forceShader->BeginDraw(grmShader::RMC_DRAWSKINNED, true);
	for (int i=0;i<numPasses;i++)
	{
		forceShader->Bind(i);
		// draw the shadow caster objects
		for(int i=0; i < m_ShadowCasters.GetCount(); i++)
		{
			// Set the Bounding Sphere for frustum culling
			spdSphere sphere;

			const rmcLodGroup& lodGroup = m_ShadowCasters[i].m_Drawable->GetLodGroup();

			// this is in model space
			Vector3 sphereCenter = lodGroup.GetCullSphere();

			// transform this to world space ... only translate the spherecenter, because there is no concept of rotation for spheres
			sphereCenter.Add(m_ShadowCasters[i].m_Matrix->d);

			// sphere center
			sphere.Set(RCC_VEC3V(sphereCenter), ScalarV(lodGroup.GetCullRadius()));
			Vector4 Sphere = VEC3V_TO_VECTOR4(sphere.GetV4());

			if (m_ShadowCasters[i].m_Skeleton && (vp.IsSphereVisible(VECTOR4_TO_VEC4V(Sphere)) || ignoreVisTests))
			{
				bool screenSizeTestResult = true;
#if USEPROJECTEDAREACULLING
				Vector3 boxMin;
				Vector3 boxMax;
				lodGroup.GetBoundingBox(boxMin, boxMax);
				if (!(boxMin == VEC3_ZERO & boxMax == VEC3_ZERO))  // Filter out sketchy bounding boxes
				{
					const Matrix34& casterLocalToWorld = *m_ShadowCasters[i].m_Matrix;
					Matrix34 casterWorldToLocal;
					casterWorldToLocal.Inverse(casterLocalToWorld);

					Vector3 eyeLocal = currentViewport->GetCameraMtx().d;
					casterWorldToLocal.Transform(eyeLocal);

					screenSizeTestResult = CalculateBoxArea(eyeLocal, boxMin, boxMax, casterLocalToWorld, fullViewProjectionScreenMtx) >= screenSpaceThreshold;
				}
				else
				{
					screenSizeTestResult = CalculateSphereArea(sphereCenter, sphere.GetRadius(), currentViewport) >= screenSpaceThreshold;
				}
#endif // USEPROJECTEDAREACULLING

				if (screenSizeTestResult || ignoreVisTests)
				{
					float camDist = SMPWORLDEMGR.DistanceFromCamera(m_ShadowCasters[i].m_Matrix->d);
					int lod = lodGroup.ComputeLod( camDist );
					if (lodGroup.ContainsLod(lod))
					{
						if (lod > maxLod && lodGroup.ContainsLod(maxLod))
						{
							lod = maxLod;
						}
					}

					if (!lodGroup.ContainsLod(lod))
					{
						lod = 0;
					}

					m_ShadowCasters[i].m_Drawable->DrawNoShadersSkinned(RCC_MATRIX34(*m_ShadowCasters[i].m_Skeleton->GetParentMtx()), *m_ShadowCasters[i].m_MatrixSet, lod, rmcDrawable::RENDER_SKINNED);
				}
			}
		}
	
		m_ExternalSkinnedDrawDelegator.Dispatch(vp, false);

		forceShader->UnBind();
	}
	forceShader->EndDraw();

	if ( isCasting )
	{
		m_ExternalSpecialCaseDrawDelegator.Dispatch( vp, false );
	}

	//grcState::Default();	
	//
}

#if USE_GRSHADOWMAP

rmcSampleShadowMap::rmcSampleShadowMap(int numTiles) : m_objectList(0), CShadowMap(numTiles, false)
{	
}
rmcSampleShadowMap::~rmcSampleShadowMap() {}



// static functions
// keep track of the instance
void rmcSampleShadowMap::Init()
{
	Assert(sm_Instance == NULL);
	sm_Instance = rage_new rmcSampleShadowMap(4);
}

void rmcSampleShadowMap::Terminate()
{
	Assert(sm_Instance);
	delete sm_Instance;
}

rmcSampleShadowMap* rmcSampleShadowMap::sm_Instance;

#endif // USE_GRSHADOWMAP

//----------------------------------------------------------
//
// Cube shadow map
//
//----------------------------------------------------------

#if USE_GRCUBESHADOWMAP

rmcSampleCubeShadowMap::rmcSampleCubeShadowMap(int numLights) : CCubeShadowMap(numLights, true)
{
	m_CubeShadowCasters.Reserve(50);
	m_MaxBuckets = 16; //NUM_BUCKETS;
}

rmcSampleCubeShadowMap::~rmcSampleCubeShadowMap()
{}

// add shadow casters
void rmcSampleCubeShadowMap::AddShadowCastingObject(rmcDrawable*drawable,crSkeleton*skeleton,Matrix34 *matrix)
{
	rmcDrawableObject & newObj = m_CubeShadowCasters.Grow();
	newObj.m_Drawable = drawable;
	newObj.m_Skeleton = skeleton;
	newObj.m_Matrix = matrix;
}


void rmcSampleCubeShadowMap::RemoveShadowCastingObjects()
{
	if(m_CubeShadowCasters.GetCount())
		m_CubeShadowCasters.Reset();
}

void rmcSampleCubeShadowMap::RemoveShadowCastingObject(rmcDrawable *drawable)
{
	int shadowCasterCnt = m_CubeShadowCasters.GetCount();
	for(int i=0 ; i<shadowCasterCnt; i++)
	{
		if(m_CubeShadowCasters[i].m_Drawable == drawable)
		{
			m_CubeShadowCasters.Delete(i);
			break;
		}
	}
}

void rmcSampleCubeShadowMap::RenderShadowCasters(grcViewport& vp, bool /*isCasting*/ )
{
	// create the shaft to cull objects against the camera view frustum
	//	shaft.Set(vp, vp.GetCameraMtx());

	grmShader *forceShader=grmModel::GetForceShader();
	Assert(forceShader);

	int numPasses=0;

	// draw non-skinned objects:
	numPasses = forceShader->BeginDraw(grmShader::RMC_DRAW, true);
	for (int i=0;i<numPasses;i++)
	{
		forceShader->Bind(i);
		// draw the shadow caster objects
		for(int i=0; i < m_CubeShadowCasters.GetCount(); i++)
		{
			// Set the Bounding Sphere for frustum culling
			spdSphere sphere;

			// this is in model space
			Vector3 sphereCenter = m_CubeShadowCasters[i].m_Drawable->GetLodGroup().GetCullSphere();

			// transform this to world space ... only translate the spherecenter, because there is no concept of rotation for spheres
			sphereCenter.Add(m_CubeShadowCasters[i].m_Matrix->d);

			// sphere center
			sphere.SetCenter(sphereCenter);

			// sphere radius
			sphere.SetRadius(m_CubeShadowCasters[i].m_Drawable->GetLodGroup().GetCullRadius());

/*
			// take the biggest lod level ... assuming we do not use a smaller one anyway
			const rmcLod& lod = m_CubeShadowCasters[i].m_Drawable->GetLodGroup().GetLod(0);

			for( int j = 0; j < lod.GetCount(); j++ )
			{
				const grmModel& model=*lod.GetModel(j);

				// get the bounding sphere ... do not forget to set sm_UseGeomBoundSpheres to true
				const Vector4 *Sphere = model.GetBoundSpheres();

				// do the sphere test
				for (int j=model.GetGeometryCount()-1;j>=0;j--)
				{
					const Vector4 *sphere = &Sphere[j];
					grcDrawSphere(sphere->w, Vector3(*sphere));

					//if (vp.IsSphereVisible(Sphere[j]))
					{
						model.Draw(j);
					}
				}
			}
*/



//			sphere.Draw(1.0, 0.0, 0.0);

			Vector4 Sphere = Vector4(sphere.GetCenter().x, sphere.GetCenter().y, sphere.GetCenter().z, sphere.GetRadius());

			if (vp.IsSphereVisible(Sphere))
			{
				if(m_CubeShadowCasters[i].m_Skeleton) 
				{
					m_CubeShadowCasters[i].m_Drawable->DrawNoShadersSkinned(*m_CubeShadowCasters[i].m_Skeleton, 0, rmcDrawable::RENDER_NONSKINNED);
				}
				else
				{
					m_CubeShadowCasters[i].m_Drawable->DrawNoShaders(*m_CubeShadowCasters[i].m_Matrix, 0);
				}
			}

		}

		m_ExternalNonSkinnedDrawDelegator.Dispatch(vp, false);

		forceShader->UnBind();
	}
	forceShader->EndDraw();

	// draw skinned objects:
	numPasses = forceShader->BeginDraw(grmShader::RMC_DRAWSKINNED, true);
	for (int i=0;i<numPasses;i++)
	{
		forceShader->Bind(i);
		// draw the shadow caster objects
		for(int i=0; i < m_CubeShadowCasters.GetCount(); i++)
		{
			// Set the Bounding Sphere for frustum culling
			spdSphere sphere;

			// this is in model space
			Vector3 sphereCenter = m_CubeShadowCasters[i].m_Drawable->GetLodGroup().GetCullSphere();

			// transform this to world space ... only translate the spherecenter, because there is no concept of rotation for spheres
			sphereCenter.Add(m_CubeShadowCasters[i].m_Matrix->d);

			// sphere center
			sphere.SetCenter(sphereCenter);

			// sphere radius
			sphere.SetRadius(m_CubeShadowCasters[i].m_Drawable->GetLodGroup().GetCullRadius());
			Vector4 Sphere = Vector4(sphere.GetCenter().x, sphere.GetCenter().y, sphere.GetCenter().z, sphere.GetRadius());

			if (vp.IsSphereVisible(Sphere))
			{
				if(m_CubeShadowCasters[i].m_Skeleton) 
				{
					m_CubeShadowCasters[i].m_Drawable->DrawNoShadersSkinned(*m_CubeShadowCasters[i].m_Skeleton, 0,rmcDrawable::RENDER_SKINNED);
				}
			}
		}

		m_ExternalSkinnedDrawDelegator.Dispatch(vp, false);

		forceShader->UnBind();
	}
	forceShader->EndDraw();

// 	if ( isCasting )
// 	{
// 		m_ExternalSpecialCaseDrawDelegator.Dispatch(vp, false);
// 	}
	

	//grcState::Default();	
	//
}

// static functions
// keep track of the instance
void rmcSampleCubeShadowMap::Init()
{
	Assert(sm_CubeInstance == NULL);
	sm_CubeInstance = rage_new rmcSampleCubeShadowMap(1);
}

void rmcSampleCubeShadowMap::Terminate()
{
	Assert(sm_CubeInstance);
	delete sm_CubeInstance;
}

rmcSampleCubeShadowMap* rmcSampleCubeShadowMap::sm_CubeInstance;

#endif // USE_GRCUBESHADOWMAP

//-----------------------------------------------------------------------------

SimpleWorldMovable::SimpleWorldMovable(void* context)
	: m_Context(context)
{
	m_InWorldNode.Data = this;
}

//-----------------------------------------------------------------------------

void SimpleWorld::Insert(void* movableId)
{
	SimpleWorldMovable* pMovable = reinterpret_cast<SimpleWorldMovable*>(movableId);
	m_Inserted.Append(pMovable->m_InWorldNode);
}

//-----------------------------------------------------------------------------

void SimpleWorld::Remove(void* movableId)
{
	m_Inserted.PopNode(reinterpret_cast<SimpleWorldMovable*>(movableId)->m_InWorldNode);
}

//-----------------------------------------------------------------------------

void SimpleWorld::Update(void* UNUSED_PARAM(movableId))
{
}

//-----------------------------------------------------------------------------

bool SimpleWorld::AddToDrawBucket(const rmcDrawable& toDraw,
								  const Matrix34* matrix,
								  const crSkeleton* skeleton,
								  const grmMatrixSet* matrixSet,
								  float dist)
{
	u8 lod = static_cast<u8>(toDraw.GetLodGroup().ComputeLod(dist));
	int bucketMask = toDraw.GetBucketMask(lod);
	if (bucketMask == ~0)
	{
		return false;
	}
	
	int mask = 1;
	for (int bucket = 0; bucket < 32; ++bucket, mask <<= 1)
	{
		if ( mask & m_IgnoreBuckets)
		{
			continue;
		}
		if (mask & bucketMask)
		{
			grcTextureFactory::GetInstance().SetTextureLod(dist);
			bool bDrawSkinned =  toDraw.GetLodGroup().GetModel(lod).GetSkinFlag() || toDraw.GetSkeletonData();
			
			if ( skeleton && bDrawSkinned )
			{
				//toDraw.DrawSkinned(*skeleton, bucket, lod);
				m_bucketRenderer.Add( bucket, rage_new RenderableElement::DrawableSkinned( toDraw, skeleton, matrixSet, lod, dist ));
			}
			else
			{
				if(m_bUseSkelForSkinnedOnly)
				{
					//toDraw.Draw(*matrix, bucket, lod);
					m_bucketRenderer.Add( bucket, rage_new RenderableElement::Drawable( toDraw, matrix, lod, dist ));
				}
				else
				{
					if(skeleton)
					{
						//toDraw.Draw(skeleton->GetGlobalMtx(0), bucket, lod);
						m_bucketRenderer.Add( bucket, rage_new RenderableElement::Drawable( toDraw, reinterpret_cast<const Matrix34*>(skeleton->GetObjectMtxs()), lod, dist ));
					}
					else
					{
						//toDraw.Draw(*matrix, bucket, lod);
						m_bucketRenderer.Add( bucket, rage_new RenderableElement::Drawable( toDraw, matrix, lod, dist ));
					}
				}
			}
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

void SimpleWorld::Draw(fragShaft &shaft, u32 BucketMask)
{
	DrawSetup( shaft, BucketMask );	
	DrawFinal( BucketMask);
}

void SimpleWorld::DrawSetup(fragShaft &shaft, u32 BucketMask)
{
	BeginDraw();

	for (const atDNode<SimpleWorldMovable*>* node = m_Inserted.GetHead(); node; node = node->GetNext())
	{
		SimpleWorldMovable* movable = node->Data; Assert(movable);
		movable->AddToDrawList(shaft);
	}
	EndDraw( BucketMask );
}

void SimpleWorld::DrawFinal( u32 BucketMask)
{
	m_bucketRenderer.Render( BucketMask);
}

void SimpleWorld::EndDraw( u32  )
{
	// hard wired transparency for now
	m_bucketRenderer.Sort( 1, BucketRenderer::SortBackToFront() );	
}
// used for vertex/tris count only
rage::rmcDrawable * SimpleWorld::GetDrawable()
{
	rmcDrawable * drawable = 0;
	for (const atDNode<SimpleWorldMovable*>* node = m_Inserted.GetHead(); node; node = node->GetNext())
	{
		SimpleWorldMovable* movable = node->Data;
		Assert(movable);
		SimpleWorldEntity *pEntity = reinterpret_cast<SimpleWorldEntity*>(movable->m_Context);
		if(pEntity==NULL)
		{
			continue;
		}
		drawable=pEntity->GetDrawable();
	}
	return drawable;
}

void SimpleWorld::BeginDraw()
{
	m_bucketRenderer.Clear();
}

//-----------------------------------------------------------------------------

} //End namespace ragesamples
