// 
// sample_simpleworld/frameEditor.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/bkvector.h"
#include "bank/slider.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/frameiterators.h"
#include "creature/creature.h"
#include "crskeleton/skeletondata.h"
#include "vectormath/legacyconvert.h"

#include "frameEditor.h"

using namespace rage;

#if __BANK

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

class AddWidgetIterator : public crFrameIterator<AddWidgetIterator>
{
public:
	AddWidgetIterator(crFrame& frame, atArray<Vector3>& widgetValues, atArray<bool>& widgetInvalid, bkBank& bank, const crSkeletonData* skelData)
		: crFrameIterator<AddWidgetIterator>(frame)
		, m_WidgetValues(&widgetValues)
		, m_WidgetInvalid(&widgetInvalid)
		, m_Bank(&bank)
		, m_SkelData(skelData)
	{
	}

	void IterateDof(const crFrameData::Dof& dof, crFrame::Dof&, float)
	{
		char buf[512];
		sprintf(buf, "track %d id %d ", dof.m_Track, dof.m_Id);

		char buf2[256]; 
		buf2[0] = '\0';
		
		const char* name = crSkeletonData::DebugConvertBoneIdToName(dof.m_Id);
		if(name)
		{
			sprintf(buf2, "name '%s'", name);
		}

		switch(dof.m_Track)
		{
		case kTrackBoneTranslation:
		case kTrackBoneRotation:
		case kTrackBoneScale:
			{
				int boneIdx;
				if(m_SkelData && m_SkelData->ConvertBoneIdToIndex(dof.m_Id, boneIdx))
				{
					const crBoneData* bd = m_SkelData->GetBoneData(boneIdx);
					if(bd)
					{
						switch(dof.m_Track)
						{
						case kTrackBoneTranslation:
							sprintf(buf2, "bone[%d] '%s' translation", bd->GetIndex(), bd->GetName());
							break;

						case kTrackBoneRotation:
							sprintf(buf2, "bone[%d] '%s' rotation", bd->GetIndex(), bd->GetName());
							break;

						case kTrackBoneScale:
							sprintf(buf2, "bone[%d] '%s' scale", bd->GetIndex(), bd->GetName());
							break;
						}
					}
				}
			}
			break;
		}

		strcat(buf, buf2);
		m_Bank->PushGroup(buf, false);

		bool& b = m_WidgetInvalid->Grow();
		b = true;

		m_Bank->AddToggle("invalid", &b);

		Vector3& v = m_WidgetValues->Grow();
		v.Zero();

		switch(dof.m_Type)
		{
		case kFormatTypeQuaternion:
			m_Bank->AddAngle("euler x", &v.x, bkAngleType::RADIANS);
			m_Bank->AddAngle("euler y", &v.y, bkAngleType::RADIANS);
			m_Bank->AddAngle("euler z", &v.z, bkAngleType::RADIANS);
			break;

		case kFormatTypeVector3:
			m_Bank->AddVector("vector xyz", &v, bkVector::FLOAT_MIN_VALUE, bkVector::FLOAT_MAX_VALUE, 0.001f);
			break;

		case kFormatTypeFloat:
			m_Bank->AddSlider("float", &v.x, bkSlider::FLOAT_MIN_VALUE, bkSlider::FLOAT_MAX_VALUE, 0.001f);
			break;
		}

		m_Bank->PopGroup();
	}

	atArray<Vector3>* m_WidgetValues;
	atArray<bool>* m_WidgetInvalid;
	bkBank* m_Bank;
	const crSkeletonData* m_SkelData;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

class ResetWidgetIterator : public crFrameIterator<ResetWidgetIterator>
{
public:
	ResetWidgetIterator(crFrame& frame, atArray<Vector3>& widgetValues, atArray<bool>& widgetInvalid)
		: crFrameIterator<ResetWidgetIterator>(frame)
		, m_WidgetValues(&widgetValues)
		, m_WidgetInvalid(&widgetInvalid)
	{
		m_Index = 0;
	}

	void IterateDof(const crFrameData::Dof& dof, crFrame::Dof& dest, float)
	{
		bool& b = (*m_WidgetInvalid)[m_Index];
		b = dest.IsInvalid();

		Vector3& v = (*m_WidgetValues)[m_Index];
		if(!dest.IsInvalid())
		{
			switch(dof.m_Type)
			{
			case kFormatTypeQuaternion:
				{
					QuatV q;
					q = dest.GetUnsafe<QuatV>();
					RC_VEC3V(v) = QuatVToEulersXYZ(q);
				}
				break;

			case kFormatTypeVector3:
				v = dest.GetUnsafe<Vector3>();
				break;

			case kFormatTypeFloat:
				v.x = dest.GetUnsafe<float>();
				break;
			}
		}

		m_Index++;
	}

	atArray<Vector3>* m_WidgetValues;
	atArray<bool>* m_WidgetInvalid;
	int m_Index;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

class UpdateWidgetIterator : public crFrameIterator<UpdateWidgetIterator>
{
public:
	UpdateWidgetIterator(crFrame& frame, atArray<Vector3>& widgetValues, atArray<bool>& widgetInvalid)
		: crFrameIterator<UpdateWidgetIterator>(frame)
		, m_WidgetValues(&widgetValues)
		, m_WidgetInvalid(&widgetInvalid)
	{
		m_Index = 0;
	}

	void IterateDof(const crFrameData::Dof& dof, crFrame::Dof& dest, float)
	{
		const Vector3& v = (*m_WidgetValues)[m_Index];
		switch(dof.m_Type)
		{
		case kFormatTypeQuaternion:
			{
				QuatV q = QuatVFromEulersXYZ(RCC_VEC3V(v));
				dest.Set<QuatV>(q);
			}
			break;

		case kFormatTypeVector3:
			dest.Set<Vector3>(v);
			break;

		case kFormatTypeFloat:
			dest.Set<float>(v.x);
			break;
		}

		const bool& b = (*m_WidgetInvalid)[m_Index];
		dest.SetInvalid(b);

		m_Index++;
	}

	atArray<Vector3>* m_WidgetValues;
	atArray<bool>* m_WidgetInvalid;
	int m_Index;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

FrameEditorWidgets::FrameEditorWidgets()
	: m_pFrame(NULL)
	, m_pSkelData(NULL)
{
}

//-----------------------------------------------------------------------------

FrameEditorWidgets::~FrameEditorWidgets()
{
}

//-----------------------------------------------------------------------------

void FrameEditorWidgets::Init(crFrame& frame, const crSkeletonData* pSkelData)
{
	Assert(!m_pFrame);
	m_pFrame = &frame;
	m_pSkelData = pSkelData;
}

//-----------------------------------------------------------------------------

void FrameEditorWidgets::AddWidgets(bkBank& bk)
{
	Assert(m_pFrame);

	const int numDofs = m_pFrame->GetNumDofs();

	m_WidgetValues.Reserve(numDofs);
	m_WidgetInvalid.Reserve(numDofs);

	AddWidgetIterator it(*m_pFrame, m_WidgetValues, m_WidgetInvalid, bk, m_pSkelData);
	it.Iterate(NULL, 1.f, false);
}

//-----------------------------------------------------------------------------

void FrameEditorWidgets::ResetWidgets()
{
	Assert(m_pFrame);

	ResetWidgetIterator it(*m_pFrame, m_WidgetValues, m_WidgetInvalid);
	it.Iterate(NULL, 1.f, false);
}

//-----------------------------------------------------------------------------

void FrameEditorWidgets::UpdateFrame()
{
	Assert(m_pFrame);

	UpdateWidgetIterator it(*m_pFrame, m_WidgetValues, m_WidgetInvalid);
	it.Iterate(NULL, 1.f, false);
}

//-----------------------------------------------------------------------------

#endif //__BANK

