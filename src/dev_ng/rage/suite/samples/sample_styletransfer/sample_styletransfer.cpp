// 
// /sample_styletransfer.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_cranimation/sample_cranimation.h"

#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/animplayer.h"
#include "crskeleton/skeletondata.h"
#include "crstyletransfer/style.h"
#include "crstyletransfer/styleplayer.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

using namespace rage;

#define DEFAULT_FILE "$/rage_male/rage_male.type"
XPARAM(file);

namespace ragesamples
{

#define DEFAULT_ANIM "$/animation/style/s056_smooth_turn_slow_neutral_1.anim"
PARAM(anim, "[sample_styletransfer] Animation file to load (default is \"" DEFAULT_ANIM "\")");

#define DEFAULT_STYLE "$/sample_styletransfer/leg.style"
PARAM(style, "[sample_styletransfer] Style transfer file to load (default is \"" DEFAULT_STYLE "\")");

PARAM(blendweight, "[sample_styletransfer] Optional:  The global blend weight to use on the style (default is 1.0)");
PARAM(heuristic, "[sample_styletransfer] Optional:  Flag for enabling/disabling heuristic blending (default is 0)");
PARAM(confidence, "[sample_styletransfer] Optional:  Used with heuristic blending.  Confidence in the system (default is 20)");
PARAM(preference, "[sample_styletransfer] Optional:  Used with heuristic blending.  Preference for stylization (default is 2)");
XPARAM(zup);

class crSampleStyleTransfer : public ragesamples::craSampleManager
{
public:
	crSampleStyleTransfer() : ragesamples::craSampleManager()
		, m_Player(NULL)
		, m_Frame(NULL)
		, m_Style(NULL)
		, m_StylePlayer(NULL)
	{
	}

protected:
	void InitClient()
	{
		craSampleManager::InitClient();

		// find animation filename
		const char *animName = DEFAULT_ANIM;
		PARAM_anim.Get(animName);

		// find blend weight
		float blendWeight = 1.0;
		PARAM_blendweight.Get(blendWeight);

		// load animation
		crAnimation* anim=crAnimation::AllocateAndLoad(animName);

		// setup animation player
		m_Player=rage_new crAnimPlayer(anim);

		// force animation player to continuously loop animation
		m_Player->SetLooped(true, true);

		if(anim)
		{
			// let animation player take over task of destroying animation
			anim->Release();
		}

		// create animation frame
		m_Frame=rage_new crFrame;

		crSkeleton& skel = GetSkeleton();
		const crSkeletonData& skelData = skel.GetSkeletonData();

		// initialize animation frame to contain all degrees of freedom in skeleton
		m_Frame->InitCreateBoneAndMoverDofs(skelData, true);

		// initialize the degrees of freedom to be identity pose
		m_Frame->IdentityFromSkel(skelData);
		m_Frame->SetMoverMatrix(0, Mat34V(V_IDENTITY));

		// find the style name
		const char *styleName = DEFAULT_STYLE;
		PARAM_style.Get(styleName);

		// find the up index
		int upIndex = 1;  // y-axis
		if(PARAM_zup.Get())
		{
			upIndex = 2;  // z-axis
		}

		// find the value of the heuristic flag
		int heuristic = 0;
		PARAM_heuristic.Get(heuristic);

		// find the value of the confidence term
		int conf = 20;
		PARAM_confidence.Get(conf);

		// find the value of the preference term
		int pref = 2;
		PARAM_preference.Get(pref);

		// allocate a new style
		m_Style = rage_new crstStyle();

		// attempt to load the style file
		if(!m_Style->Load(styleName))
		{
			Quitf("Failed to load style file '%s'", styleName);
		}

		// allocate and initialize the style player
		m_StylePlayer = rage_new crstStylePlayer(m_Style, skelData, 
			upIndex, !heuristic, conf, pref);
		m_StylePlayer->SetBlendWeight(blendWeight);

#if __BANK
		AddWidgetsStyleTransfer();
#endif //__BANK
	}

	void ShutdownClient()
	{
		delete m_Player;
		delete m_Frame;
		delete m_Style;
		delete m_StylePlayer;

		craSampleManager::ShutdownClient();
	}

	void UpdateClient()
	{
		crSkeleton& skeleton = GetSkeleton();
		Mat34V& moverMtx = GetMoverMtx();

		// update the degrees of freedom in the frame to contain the current animation pose
		m_Player->Composite(*m_Frame);

		// stylize the contents of the frame
		float timing = m_StylePlayer->Stylize(*m_Frame, m_Player->GetDelta());

		// take the stylized frame and pose it onto the skeleton of the character
		m_Frame->Pose(skeleton);

		// update the mover, get the delta of the mover from the frame, add it to the absolute mover
		Mat34V deltaMtx;
		m_Frame->GetMoverMatrix(deltaMtx);
		Transform(moverMtx, moverMtx, deltaMtx);

		// update the skeleton's world space matrices
		skeleton.Update();

		// update the animation player to the next time step
		m_Player->SetRate(GetPlaybackRate()/timing);
		m_Player->Update(TIME.GetSeconds());

		craSampleManager::UpdateClient();
	}

#if __BANK
	void AddWidgetsStyleTransfer()
	{
	}
#endif // __BANK

	const char* GetDefaultFile()
	{
		const char* defaultFile = DEFAULT_FILE;
		PARAM_file.Get(defaultFile);
		return defaultFile;
	}

private:
	crAnimPlayer* m_Player;
	crFrame* m_Frame;

	crstStyle* m_Style;
	crstStylePlayer* m_StylePlayer;
};

}; // namespace ragesamples

using namespace ragesamples;

// main application
int Main(void)
{
	crSampleStyleTransfer sampleStyleTransfer;
	sampleStyleTransfer.Init();

	sampleStyleTransfer.UpdateLoop();

	sampleStyleTransfer.Shutdown();

	return 0;
}

