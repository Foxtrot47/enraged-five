// 
// sample_skydome/sample_skydome.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_SKYDOME_H
#define SAMPLE_SKYDOME_H

#include "sample_simpleworld/simpleworld.h"
#include "sample_simpleworld/sample_simpleworld.h"
#include "grmodel/shadergroup.h"
#include "shadercontrollers/grcphenomna.h"
#include "shadercontrollers/skyhat.h"

#include "shadercontrollers/skyKeyframed.h"

//-----------------------------------------------------------------------------

namespace rage
{
    class bkBank;
    class ProceduralTextureManager;
    class ProceduralTextureVerletWater;
    class ProceduralTextureSkyhat;
}

//-----------------------------------------------------------------------------
#define NUM_WEATHER_SETTINGS		5

using namespace rage;

namespace ragesamples
{
    //-----------------------------------------------------------------------------

    class rmcSampleSkydomeTest : public ragesamples::rmcSampleSimpleWorld
    {
    public:
        rmcSampleSkydomeTest();
        ~rmcSampleSkydomeTest();

        // Get the current time for Time of Day stuff.
        float GetCurrentTime() const { return m_TimeOfDay; }

    protected:
        //-START overridden from the parent class
        virtual const char* GetSampleName() const
        {
            return "Sample Skydone";
        }

        virtual void InitClient();
        virtual void ShutdownClient();

        virtual void UpdateClient();
        virtual void PreDrawClient();
        virtual void DrawClient();

#if __BANK
        virtual void AddWidgetsClient();
#endif // __BANK
        //-STOP

        // Callback to remove drawawables from the Shader Controller when removing an entity.
  //      virtual void OnRemoveEntity( SimpleWorldEntity *pEntity );

        //-START This callback will fill the Procedural Texture's Rt def and Shader def arrays with client-specific
        // data that the ProcTex will use.
        void SetupSkyhatProceduralTexture( ProceduralTextureSkyhat *pSkyhat );
        //-STOP

        // Update the Time of Day
        float UpdateTimeOfDay( bool& );

		// save our control settings
		void SaveSettings();
		void ResetSettings();

	private:

		SkyHatPerFrameSettings GetGameSettings( float time );


		void CopySettingsFromNiceToKeyed();

#if __BANK
        void BankUpdateTimeOfDay();
#endif


		void SaveWieghts();
		void LoadWieghts();

        bool					m_DrawShadows;
        bool					m_UseBlendedShadows;

        bool                    m_AutoUpdateTOD;
        bool                    m_lockTOD;
        float                   m_unlockTODcountdown;    
        float                   m_TimeOfDay;
        float                   m_timeUpdateRate;

        spdShaft                m_currentShaft;

       

        bool						m_debugRenderTargets;
		SkyDomeProceduralControl	m_SkyControl;
		SkyDome*					m_SkyDome;

		static const int			MaxControls = NUM_WEATHER_SETTINGS;
		SkyDomeProceduralControl	m_SkyControlList[ MaxControls ];
		float						m_wieghts[ MaxControls ];
		
		SkyDomeKeyFramedControl		m_keyedController;
		float						m_wieghtKeyed;

		SkyDomeKeyFrameTimeElements	m_keyedTimeElements;
		bool						m_useKeyedTimeElements;
    };

    //-----------------------------------------------------------------------------
} //End namespace ragesamples

#endif //SAMPLE_SKYDOME_H
