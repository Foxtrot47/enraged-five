
#ifndef SKY_DOME_KEY_FRAMED_CONTROL_H
#define SKY_DOME_KEY_FRAMED_CONTROL_H

#include "rmptfx/keyframe.h"

namespace rage
{
	struct SkyHatPerFrameSettings;
	class parRTStructure;

	class SkyDomeKeyFramedControl : public datBase
	{
	public:
		SkyDomeKeyFramedControl() ;

		void LoadSkyhatTuning();

#if __BANK
		void SaveSkyhatTuning();
		void AddWidgets( bkBank &bank );
#endif
		void CalculateSkySettings( float timeOfDay, SkyHatPerFrameSettings& settings );

	private:

		void Create( parRTStructure& m_parRTS );
		// sky
		ptxKeyframe m_SkyColorKF;	//Keyframed Acceleration
		ptxKeyframe m_AzimuthColorKF;		//Keyframed world space "friction"
		ptxKeyframe m_AzimuthColorEastKF;	//Weight of attached matrix
		ptxKeyframe m_SunsetColorKF;	//Weight of attached matrix
		Vector2			m_SunAxias;


		// clouds
		ptxKeyframe m_CloudColorKF;	//Directional based size scalars
		ptxKeyframe m_CloudThresholdThruCloudShadowStrengthKF;
		ptxKeyframe m_CloudShadowOffsetThruAzymuthStrengthKF;
		ptxKeyframe m_DetailScaleThruDetailStrengthWithEdgeSmoothThicknessKF;

		// top clouds
		ptxKeyframe m_TopCloudColorKF;	
		ptxKeyframe m_TBiasThruTDetailThruThresholdThruHeightKF;

		// Sun
		ptxKeyframe m_SunColorKF;	//Keyframed Acceleration
		ptxKeyframe m_UnStrengthThruCSpeedThruStarBrightnessKF;

		// moon
		ptxKeyframe m_MoonColorKFAndBrightness;	//Keyframed Acceleration
	};
};


#endif
