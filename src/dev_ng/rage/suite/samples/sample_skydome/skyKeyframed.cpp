

#include "shadercontrollers/skyhat.h"

#include "skyKeyFramed.h"
using namespace rage;

void SkyDomeKeyFramedControl::LoadSkyhatTuning()
{		
	parRTStructure		parRTS;		
	Create( parRTS);

	PARSER.LoadFromStructure("SkyHat", "tuning", parRTS);
}

SkyDomeKeyFramedControl::SkyDomeKeyFramedControl() : m_SunAxias( 1.0f, 0.0f )
{}
inline Vector3 GetKey( float t, ptxKeyframe& val )
{
	Vec_xyzw color;
	val.GetKeyframeData(t).GetData(color, t);

	Vector3 res;
	res.Set( color.GetX(), color.GetY(), color.GetZ() );
	return res;
}

void SkyDomeKeyFramedControl::CalculateSkySettings( float timeOfDay, SkyHatPerFrameSettings& settings )
{
	settings.Clear();

	settings.m_skyColor = GetKey( timeOfDay, m_SkyColorKF ); 
	settings.m_AzimuthColor = GetKey( timeOfDay, m_AzimuthColorKF );
	settings.m_AzimuthColorEast = GetKey( timeOfDay, m_AzimuthColorEastKF );
	settings.m_SunsetColor.SetVector3( GetKey( timeOfDay, m_SunsetColorKF ) );

	// clouds
	settings.m_CloudColor = GetKey( timeOfDay, m_CloudColorKF );

	Vec_xyzw cloudThresholdThruCloudShadowStrength;
	m_CloudThresholdThruCloudShadowStrengthKF.GetKeyframeData( timeOfDay).GetData(cloudThresholdThruCloudShadowStrength, timeOfDay);

	settings.m_CloudThreshold = cloudThresholdThruCloudShadowStrength.GetX(); 
	settings.m_CloudBias = cloudThresholdThruCloudShadowStrength.GetY();
	settings.m_CloudThickness = cloudThresholdThruCloudShadowStrength.GetZ();
	settings.m_CloudShadowStrength = cloudThresholdThruCloudShadowStrength.GetW();


	Vec_xyzw cloudShadowOffsetThruAzymuthStrength;
	m_CloudShadowOffsetThruAzymuthStrengthKF.GetKeyframeData( timeOfDay).GetData(cloudShadowOffsetThruAzymuthStrength, timeOfDay);

	settings.m_CloudShadowOffset = cloudShadowOffsetThruAzymuthStrength.GetX(); // m_SkyShader->GetEffect().SetVar(m_CloudShadowOffsetID		,cloudShadowOffsetThruAzymuthStrength.GetX());		
	settings.m_CloudInscatteringRange = cloudShadowOffsetThruAzymuthStrength.GetY(); // m_SkyShader->GetEffect().SetVar(m_CloudInscatteringRangeID	,cloudShadowOffsetThruAzymuthStrength.GetY());
	settings.m_CloudEdgeSmooth = cloudShadowOffsetThruAzymuthStrength.GetZ(); // m_SkyShader->GetEffect().SetVar(m_CloudEdgeSmoothID			,cloudShadowOffsetThruAzymuthStrength.GetZ());
	settings.m_AzimuthStrength = cloudShadowOffsetThruAzymuthStrength.GetW(); // m_SkyShader->GetEffect().SetVar(m_AzymuthStrengthID			,cloudShadowOffsetThruAzymuthStrength.GetW());

	Vec_xyzw detailScaleThruDetailStrength;
	m_DetailScaleThruDetailStrengthWithEdgeSmoothThicknessKF.GetKeyframeData( timeOfDay).GetData(detailScaleThruDetailStrength, timeOfDay);

	settings.m_DetailScale = detailScaleThruDetailStrength.GetX();		// m_SkyShader->GetEffect().SetVar(m_DetailScaleID				,detailScaleThruDetailStrength.GetX());
	settings.m_DetailStrength = detailScaleThruDetailStrength.GetY();	// m_SkyShader->GetEffect().SetVar(m_DetailStrengthID			,detailScaleThruDetailStrength.GetY());
	//	settings.m_CloudThickness = detailScaleThruDetailStrength.GetZ();

	// top clouds
	settings.m_TopCloudColor = GetKey( timeOfDay, m_TopCloudColorKF );

	Vec_xyzw  TBiasThruTDetailThruThresholdThruHeight;
	m_TBiasThruTDetailThruThresholdThruHeightKF.GetKeyframeData( timeOfDay).GetData(TBiasThruTDetailThruThresholdThruHeight, timeOfDay);

	settings.m_TopCloudBias = TBiasThruTDetailThruThresholdThruHeight.GetX();		// m_SkyShader->GetEffect().SetVar(m_DetailScaleID				,detailScaleThruDetailStrength.GetX());
	settings.m_TopCloudDetail = TBiasThruTDetailThruThresholdThruHeight.GetY();	// m_SkyShader->GetEffect().SetVar(m_DetailStrengthID			,detailScaleThruDetailStrength.GetY());
	settings.m_TopCloudThreshold = TBiasThruTDetailThruThresholdThruHeight.GetZ();
	settings.m_TopCloudHeight = TBiasThruTDetailThruThresholdThruHeight.GetZ();

	// Sun
	settings.m_SunColor = GetKey( timeOfDay, m_SunColorKF );
	Vec_xyzw  UnStrengthThruCSpeedThruStarBrightnessKF;
	m_UnStrengthThruCSpeedThruStarBrightnessKF.GetKeyframeData( timeOfDay).GetData(UnStrengthThruCSpeedThruStarBrightnessKF, timeOfDay);

	settings.m_UnderLightStrength = UnStrengthThruCSpeedThruStarBrightnessKF.GetX();		// m_SkyShader->GetEffect().SetVar(m_DetailScaleID				,detailScaleThruDetailStrength.GetX());
	settings.m_CloudSpeed = UnStrengthThruCSpeedThruStarBrightnessKF.GetY();	// m_SkyShader->GetEffect().SetVar(m_DetailStrengthID			,detailScaleThruDetailStrength.GetY());
	settings.m_StarFieldBrightness = UnStrengthThruCSpeedThruStarBrightnessKF.GetZ();

	// mooon
	Vec_xyzw	Moon;
	m_MoonColorKFAndBrightness.GetKeyframeData( timeOfDay).GetData(Moon, timeOfDay);

	settings.m_MoonColor.Set( Moon.GetX(), Moon.GetY(), Moon.GetZ() );
	settings.m_MoonBrightness = Moon.GetW();
}

void SkyDomeKeyFramedControl::Create( parRTStructure& m_parRTS )
{
	m_parRTS.SetName("rmwSkydome");		
	m_parRTS.AddStructureInstance("SkyColorKF",									m_SkyColorKF);
	m_parRTS.AddStructureInstance("AzimuthColorKF",								m_AzimuthColorKF);
	m_parRTS.AddStructureInstance("AzimuthColorKF",								m_AzimuthColorEastKF);
	m_parRTS.AddStructureInstance("SunsetColorKF",								m_SunsetColorKF);
	m_parRTS.AddStructureInstance("CloudColorKF",								m_CloudColorKF);
	m_parRTS.AddStructureInstance("CloudThresholdThruCloudShadowStrengthKF",	m_CloudThresholdThruCloudShadowStrengthKF);
	m_parRTS.AddStructureInstance("CloudShadowOffsetThruAzymuthStrengthKF",		m_CloudShadowOffsetThruAzymuthStrengthKF);
	m_parRTS.AddStructureInstance("DetailScaleThruDetailStrengthWithEdgeSmoothThicknessKF",	m_DetailScaleThruDetailStrengthWithEdgeSmoothThicknessKF);

	m_parRTS.AddStructureInstance("TopCloudColorKF",										m_TopCloudColorKF);
	m_parRTS.AddStructureInstance("TBiasThruTDetailThruThresholdThruHeightKF",				m_TBiasThruTDetailThruThresholdThruHeightKF);

	m_parRTS.AddStructureInstance("SunColorKF",												m_SunColorKF);
	m_parRTS.AddStructureInstance("UnStrengthThruCSpeedThruStarBrightnessKF",				m_UnStrengthThruCSpeedThruStarBrightnessKF);
	m_parRTS.AddStructureInstance("m_MoonColorKFAndBrightness",								m_MoonColorKFAndBrightness);


//	???		m_SunAxias
}
#if __BANK
void SkyDomeKeyFramedControl::SaveSkyhatTuning()
{
	parRTStructure		parRTS;		
	Create( parRTS);

	PARSER.SaveFromStructure("SkyHat", "tuning", parRTS);
}

static void AddKeyedWidget( bkBank &bank, 
						     ptxKeyframe& keyedWidget,
							 const char* name,
							 const char* l1 = 0, const char* l2 = 0, const char* l3 = 0,  const char* l4 = 0)
{
	ptxKeyframeDefn		data;
	ptxKeyframe::eWidgetType		Format = ptxKeyframe::KF_VECTOR4;
	if ( l4)
	{
		data.SetLabels(l1,l2,l3, l4 );
		data.SetShowAsColorGradient( false );
	}
	else if ( l3 )
	{
		data.SetLabels(l1,l2,l3);
		Format = ptxKeyframe::KF_VECTOR3;
		data.SetShowAsColorGradient( true );
	}
	else if ( l2 )
	{
		data.SetLabels(l1,l2 );
		Format = ptxKeyframe::KF_VECTOR2;
		data.SetShowAsColorGradient( false );
	}
	else 
	{
		Assert( l2 == 0 );
		data.SetLabels( l1 );
		Format = ptxKeyframe::KF_VECTOR_X;
		data.SetShowAsColorGradient( false );
	}
	
	data.m_YRangeMinMax.Set(0.0f,1.0f);
	data.m_XRangeMinMax.Set(0.0f,24.0f);
	data.m_DeltaXY.Set(0.01f,0.01f);
	keyedWidget.AddWidgets(bank, name, Format,data);

}
void SkyDomeKeyFramedControl::AddWidgets(bkBank &bank)
{
	bank.PushGroup("Keyed Sky Hat ",false);
		bank.AddButton("Save",datCallback(MFA(SkyDomeKeyFramedControl::LoadSkyhatTuning),this));
		bank.AddButton("Load",datCallback(MFA(SkyDomeKeyFramedControl::SaveSkyhatTuning),this));


		bank.PushGroup( "Sky", false );

			AddKeyedWidget( bank, 
							m_SkyColorKF,
							"Sky Color",
							"SkyColorRed","SkyColorGreen","SkyColorBlue" );


			AddKeyedWidget( bank, 
				m_AzimuthColorKF,
				"Azimuth Color",
				"Azimuth Color Red","Azimuth Color Green","Azimuth Color Blue" );

			AddKeyedWidget( bank, 
				m_AzimuthColorEastKF,
				"Azimuth Color East",
				"Azimuth Color Red","Azimuth Color Green","Azimuth Color Blue" );

			AddKeyedWidget( bank, 
				m_SunsetColorKF,
				"Sunset Color",
				"Sunset Color Red","Sunset Color Green","Sunset Color Blue" );

			bank.AddSlider( "Sun Axias", &m_SunAxias, -1.0f, 1.0f , 0.01f);

		bank.PopGroup();

		// Clouds 
		bank.PushGroup("Clouds", false );

			AddKeyedWidget( bank, 
				m_CloudColorKF,
				"Cloud Color",
				"Cloud Color Red","Cloud Color Green","Cloud Color Blue" );

			// Cloud 2:
			AddKeyedWidget( bank, 
				m_CloudThresholdThruCloudShadowStrengthKF,
				"Cloud 2",
				"Cloud Threshold","Cloud Bias","Clould Thickness", "Shadow Stength" );

			// Cloud 2:
			AddKeyedWidget( bank, 
				m_CloudShadowOffsetThruAzymuthStrengthKF,
				"Cloud 2",
				"Shadow Offset","Inscattering Range","Edge Smooth", "Azymuth Strength" );
			// Cloud 3:
			AddKeyedWidget( bank, 
				m_DetailScaleThruDetailStrengthWithEdgeSmoothThicknessKF,
				"Cloud 2",
				"Detail Scale","Detail Strength" );

			bank.PushGroup("Top Layer", false );

				AddKeyedWidget( bank, 
					m_TopCloudColorKF,
					"Top Layer Color",
					"Top Layer  Red","Top Layer Green","Top Layer  Blue" );

				AddKeyedWidget( bank, 
					m_TBiasThruTDetailThruThresholdThruHeightKF,
					"Top Layer Settings",
					"Top Bias","Top Detail","Top Threshold", "Top Height" );

			bank.PopGroup();

		bank.PopGroup();

		bank.PushGroup( "Sun", false );

			AddKeyedWidget( bank, 
				m_SunColorKF,
				"Sun Color",
				"Sun Red","Sun Green","Sun Blue" );

			AddKeyedWidget( bank, 
				m_UnStrengthThruCSpeedThruStarBrightnessKF,
				"Sun Details",
				"Under Light Strength","Cloud Speed","StarField Brightness " );

			AddKeyedWidget( bank, 
				m_MoonColorKFAndBrightness,
				"Moon Color And Brightness",
				"Moon Red","Moon Blue","Moon Green" , "Moon Brightness");
		bank.PopGroup();

	bank.PopGroup();
}
#endif
