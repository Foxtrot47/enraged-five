set ARCHIVE=sample_skydome
set FILES=sample_skydome
set TESTERS=sample_skydome_test
set SAMPLE_LIBS=sample_rmcore sample_physics sample_fragment sample_simpleworld
set LIBS=%SAMPLE_LIBS% %RAGE_GFX_LIBS% %RAGE_CORE_LIBS% %RAGE_CR_LIBS% %RAGE_SUITE_CR_LIBS% %RAGE_SAMPLE_LIBS% spatialdata shadercontrollers %RAGE_PH_LIBS% %RAGE_SCRIPT_LIBS% %RAGE_AUD_LIBS%
set LIBS=%LIBS% fragment breakableglass phglass cloth grrope rmptfx event eventtypes actor grshadowmap vieweraudio cliptools
set XPROJ=%RAGE_DIR%\suite\src %RAGE_DIR%\suite\samples %RAGE_DIR%\base\src %RAGE_DIR%\base\samples %RAGE_DIR%\script\src %RAGE_DIR%\suite\src  %RAGE_DIR%\suite\tools %RAGE_DIR%\suite\tools\cli
