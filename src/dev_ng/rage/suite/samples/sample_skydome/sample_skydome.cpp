// 
// sample_skydome/sample_skydome.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crskeleton/skeletondata.h"
#include "crskeleton/skeleton.h"
#include "diag/tracker.h"
#include "gizmo/manager.h"
#include "grcore/effect_values.h"
#include "grmodel/shader.h"
#include "grpostfx/postfx.h"
#include "input/pad.h"
#include "grprofile/drawmanager.h"
#include "grprofile/ekg.h"
#include "profile/profiler.h"
#include "grprofile/stats.h"
#include "rmcore/drawable.h"
#include "spatialdata/shaft.h"
#include "system/param.h"
#include "system/timemgr.h"

#include "sample_simpleworld/simpleworld.h"
#include "sample_simpleworld/entitymgr.h"
#include "sample_skydome.h"

#include "shadercontrollers/shaderfragmentcontrol.h"
#include "shadercontrollers/grcphenomna.h"
#include "shadercontrollers/proceduraltexture.h"
#include "shadercontrollers/grcproceduralphenomna.h"

#include "file/stream.h"
using namespace rage;

const char* gSettingNames[ NUM_WEATHER_SETTINGS ] =	{ "Nice", "Cloudy", "Stormy", "Clear", "Foggy" };




namespace ragesamples
{

    rmcSampleSkydomeTest::rmcSampleSkydomeTest()
        : m_DrawShadows(true)
        , m_UseBlendedShadows(false)
        , m_TimeOfDay(16.0f)   
        , m_timeUpdateRate(0.01f)
        , m_AutoUpdateTOD(true)
#if __BANK
        , m_lockTOD(false)
        , m_unlockTODcountdown(0.0f)
#endif
        , m_debugRenderTargets( false )
		, m_wieghtKeyed( 0.0f )
		, m_useKeyedTimeElements( false )
    {
		for ( int i = 0; i < MaxControls; i++ )
		{
			m_wieghts[i] =0.0f;
		}
		m_wieghts[0] = 1.0f;
    }
    //-----------------------------------------------------------------------------

    rmcSampleSkydomeTest::~rmcSampleSkydomeTest()
    {
		delete m_SkyDome;
		m_SkyDome= 0;
    }

    //-----------------------------------------------------------------------------

    void rmcSampleSkydomeTest::InitClient()
    {
		{
			RAGE_TRACK(LeakTestStart);

			SkyDome*	leaky = rage_new SkyDome();
			leaky->Init();
			delete leaky;
		}
		RAGE_TRACK_REPORT_NAME( "CheckForSkyDomeLeaks" );

		//------------- setup the controller for the time of day settings
		m_SkyDome = rage_new SkyDome();
		
		ProceduralTextureSkyhat::Params params( true, false, 128, 128
#if __XENON
			, grctfA2B10G10R10 
#endif
			);

		m_SkyDome->Init(SkyhatMiniNoise::SettingsCallback(), params );
		ResetSettings(); 

        rmcSampleSimpleWorld::InitClient();		
	
#if __PFDRAW
        // don't want to draw physics bounds or other info about drawables
        GetRageProfileDraw().SetEnabled( false );
#endif
	    ioPad::BeginAll();

		if ( GRPOSTFX )
		{
			GRPOSTFX->SetGammaEditable( true);
		}
    }

    //-----------------------------------------------------------------------------

    void rmcSampleSkydomeTest::ShutdownClient()
    {
		delete m_SkyDome;
		m_SkyDome= 0;

		rmcSampleSimpleWorld::ShutdownClient();
     }

	void rmcSampleSkydomeTest::SaveWieghts()
	{
		fiStream*	out = ASSET.Create("wieghts", "xml", false );	
		if ( out )
		{
			out->WriteFloat( m_wieghts, MaxControls );
			out->WriteFloat( &m_wieghtKeyed, 1);
			out->Close();
		}
	}
	void rmcSampleSkydomeTest::LoadWieghts()
	{
		fiStream*	out =ASSET.Open("wieghts", "xml" );
		if ( out )
		{
			out->ReadFloat( m_wieghts, MaxControls );
			out->ReadFloat( &m_wieghtKeyed, 1 );
			out->Close();
		}
	}
    //-----------------------------------------------------------------------------

	//
	// PURPOSE
	//		This creates the settings for the skyhat
	//		This shows an example of blending multiple presets on the fly
	//		This is completely up to the game teams in how they want to do this.
	//	
	SkyHatPerFrameSettings rmcSampleSkydomeTest::GetGameSettings( float time )
	{
		// perform a wieghted blend of the weather settings
		float						wieghtedSum = m_wieghts[0];
		
		for ( int i = 1; i < MaxControls; i++ )
		{
			wieghtedSum +=m_wieghts[i];
		}
		if ( wieghtedSum == 0.0f )
		{
			wieghtedSum = 0.001f;
		}
		// add ones for the game specific control
		SkyHatPerFrameSettings		settings;
		SkyHatPerFrameSettings		weightedSettings;

		
		weightedSettings.Clear();
		for ( int i = 0; i < MaxControls; i++ )
		{
			m_SkyControlList[i].CalculateSkySettings( time, settings );	
			settings *= m_wieghts[i]/wieghtedSum;	// and weight it
			weightedSettings += settings;
		}

		// show how to do a straight lerp
		SkyHatPerFrameSettings		keyedSettings;
		m_keyedController.CalculateSkySettings( time, keyedSettings );

		weightedSettings = Lerp( m_wieghtKeyed, weightedSettings , keyedSettings );
		
		return weightedSettings;
	}
    void rmcSampleSkydomeTest::UpdateClient()
    {
       
        //-START update the TOD, checking if we're cycled around, thus starting a new day  
		bool nextDay;
        float time = UpdateTimeOfDay( nextDay );
		
		m_SkyDome->SetSettings( GetGameSettings( time ) );


		if ( m_useKeyedTimeElements )	// Set time settings
		{	
			m_SkyDome->SetSunDirection( m_keyedTimeElements.GetSunDirection( time ));
		}
		else
		{
			m_SkyDome->SetSunDirection( SkyDomeProceduralControl::GetSunDirection( time ));
		}

		m_SkyDome->Update( time, nextDay );
        //-STOP

        // This will update the entities, particle effects, and fragWorld (physics)
        rmcSampleSimpleWorld::UpdateClient();
    }
	
    //-----------------------------------------------------------------------------

    float rmcSampleSkydomeTest::UpdateTimeOfDay( bool& nextDay )
    {
		float lastTime = GetCurrentTime();
		
        float time = m_TimeOfDay;

        if ( !m_lockTOD )
        {
            if ( m_AutoUpdateTOD )
            {
                time += m_timeUpdateRate;
                if (time > 24.0f)
                {
                    time = 0.0f;
                }
                else if ( time < 0.0f )
                {
                    time = 24.0f;
                }
            }
        }
        else
        {
            m_unlockTODcountdown -= 0.10f;
            if ( m_unlockTODcountdown <= 0.0f )
            {
                m_lockTOD = false;
            }
        }

        m_TimeOfDay = time;

		nextDay = false;
		if ( lastTime > time )
		{
			nextDay = true;
		}

        return time;
    }

    //-----------------------------------------------------------------------------

    void rmcSampleSkydomeTest::PreDrawClient()
    {
        SetupCamera();
		m_SkyDome->DrawProceduralTextures();
    }

    //-----------------------------------------------------------------------------

    void rmcSampleSkydomeTest::DrawClient()
    {
    
		m_SkyDome->Draw();

	    if ( m_debugRenderTargets )
        {
           
			m_SkyDome->Show();
        }
    }
    //-----------------------------------------------------------------------------
	void rmcSampleSkydomeTest::ResetSettings()
	{
		// load up our sky controllers for different weather conditions
		ASSET.PushFolder("sample_skydome");
		LoadWieghts();

		m_SkyDome->Load("dome");

		for ( int i = 0; i < MaxControls; i++ )
		{
			m_SkyControlList[i].Load( gSettingNames[i] );
		}

		m_keyedTimeElements.Load("sunDirection" );
		m_keyedController.Load("keyedWeather");
		ASSET.PopFolder();
	}

#if __BANK


    void rmcSampleSkydomeTest::AddWidgetsClient()
    {
        rmcSampleSimpleWorld::AddWidgetsClient();

        bkBank *pBank = &BANKMGR.CreateBank( "Sample SkyDome" );
        {
            pBank->AddToggle( "Draw Shadows", &m_DrawShadows);

            pBank->AddToggle( "Use Blended Shadows", &m_UseBlendedShadows);		
            pBank->AddToggle( "Show Extra Render Targets", &m_debugRenderTargets );
            pBank->AddSlider( "Time Of Day", &m_TimeOfDay, 0.0f, 24.0f, 0.01f, datCallback(MFA(rmcSampleSkydomeTest::BankUpdateTimeOfDay),this) );
            pBank->AddSlider( "Time Update Rate", &m_timeUpdateRate, -1.0f, 1.0f, 0.01f );
			pBank->AddToggle( "Auto Update Time Of Day", &m_AutoUpdateTOD );

			// add wieghts to wieght weather settings to use
			pBank->PushGroup( "Weather Weights" );
			for ( int i = 0; i < MaxControls; i++ )
			{
				pBank->AddSlider( gSettingNames[i], &m_wieghts[i], 0.0f, 1.0f, 0.01f  );
			}
			
			pBank->AddSlider( "Keyed", &m_wieghtKeyed, 0.0f, 1.0f, 0.01f);
			pBank->PopGroup();

			if ( m_SkyDome )
				m_SkyDome->AddWidgets( *pBank );

			// add ones for the game specific control
			for ( int i = 0; i < MaxControls; i++ )
			{
				pBank->PushGroup( gSettingNames[i] );
				PARSER.AddWidgets( *pBank, &m_SkyControlList[i] );
				pBank->PopGroup();
			}

			// add the keyframed one to show multiple different inputs.
			pBank->PushGroup( "Keyed Settings" );
			//m_keyedController.AddWidgets( *pBank );
			WidgetsAddWithFileIO( *pBank, m_keyedController, "sample_skydome\\keyedWeather" );

			pBank->PopGroup();

			
			m_keyedTimeElements.AddWidgets( *pBank );
			

			pBank->AddToggle( "Use Keyed Sun Direction", &m_useKeyedTimeElements );
			
        }
		pBank->AddButton("Copy Blended Settings From Procedural To Keyed", datCallback(MFA(rmcSampleSkydomeTest::CopySettingsFromNiceToKeyed),this) );
		pBank->AddButton("Reset Settings", datCallback(MFA(rmcSampleSkydomeTest::ResetSettings),this) );
		pBank->AddButton("Save Settings", datCallback(MFA(rmcSampleSkydomeTest::SaveSettings),this) );
	}


	void rmcSampleSkydomeTest::CopySettingsFromNiceToKeyed()
	{
		int maxIndex = 0;
		float maxWieght = m_wieghts[0];
		for ( int i =1; i < MaxControls; i++ )  // chose the control with the largest wieght
		{
			
			if ( m_wieghts[i] > maxWieght )
			{
				maxIndex = i;	
			}
		}
		ConvertFromOneControllerToAnother( m_SkyControlList[maxIndex], m_keyedController , 6 );

		char fullKeyPath[256];
		memset(fullKeyPath, 0, 256);

		if(BANKMGR.OpenFile(fullKeyPath, 256, "*.tuning;*.xml", true, "Keyed Sky Hat Tunning Files (*.tuning;*.xml)"))
		{
			m_keyedController.Save( fullKeyPath );
		}

	}
	void rmcSampleSkydomeTest::SaveSettings()
	{
		ASSET.PushFolder("sample_skydome");

		SaveWieghts();
		m_SkyDome->Save("dome");
		for ( int i = 0; i < MaxControls; i++ )
		{
			m_SkyControlList[i].Save(  gSettingNames[i]);
		}

		m_keyedController.Save( "keyedWeather" );
		m_keyedTimeElements.Save("sunDirection");

		ASSET.PopFolder();
	}
    //-----------------------------------------------------------------------------

    void rmcSampleSkydomeTest::BankUpdateTimeOfDay()
    {
        m_lockTOD = true;
        m_unlockTODcountdown = 3.0f;
    }

#endif //__BANK
    //-----------------------------------------------------------------------------

}//end namespace ragesamples
