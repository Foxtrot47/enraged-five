// 
// sample_skydome/sample_skydome_test.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"
#include "sample_skydome/sample_skydome.h"

using namespace ragesamples;

int Main()
{
    rmcSampleSkydomeTest sampleRmc;
    sampleRmc.Init("");

    sampleRmc.UpdateLoop();
    sampleRmc.Shutdown();

    return 0;
}
