// 
// /sample_clip.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Clip player
// PURPOSE:
//	This sample demonstrates how to play a clip using the clip player.

using namespace rage;

#include "sample_cranimation/sample_cranimation.h"

#include "atl/functor.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/frame.h"
#include "crclip/clip.h"
#include "crclip/clipdictionary.h"
#include "crclip/clipserialanimations.h"
#include "crclip/clipplayer.h"
#include "crextra/expressions.h"
#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "crmetadata/tag.h"
#include "crskeleton/skeletondata.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"

#define DEFAULT_FILE "$/rage_male/rage_male.type"
XPARAM(file);

#define DEFAULT_CLIP "$/sample_clip/test.clip"
PARAM(clip, "[sample_clip] Clip file to load (default is \"" DEFAULT_CLIP "\")");

PARAM(clipdict, "[sample_clip] Optional clip dictionary file where to get the clip");

class crSampleClip : public ragesamples::craSampleManager	
{
public:
	crSampleClip() : ragesamples::craSampleManager()
		, m_Clip(NULL)
		, m_Frame(NULL)
		, m_Player(NULL)
	{
	}

protected:
	void InitClient()
	{
		crClip::InitClass();
		crExpressions::InitClass();

		craSampleManager::InitClient();

		// STEP #1. Load our clip.
		const char *clipName = DEFAULT_CLIP;
		PARAM_clip.Get(clipName);

		const char *clipDictionaryName;
		if(PARAM_clipdict.Get(clipDictionaryName))
		{
			crClipDictionary* clipDictionary = crClipDictionary::LoadResource(clipDictionaryName);
			if(clipDictionary)
			{
				m_Clip = clipDictionary->GetClip(clipName);
			}
		}
		else
		{
			m_Clip = crClip::AllocateAndLoad(clipName);
		}

		// STEP #2. Initialize the clip player.
		m_Player = rage_new crClipPlayer();

		// -- Pass the clip into the player
		m_Player->SetClip(m_Clip);

		// -- Force the player to loop the clip.
		// By default the player will use the clip's own looping settings 
		// to determine the looping status of the clip.
		m_Player->SetLooped(true, true);

		// -STOP


		// STEP #3. Initialize our animation frame.

		// -- If we need to get a single frame from the clip, we must have a data
		// structure that will store it.  We create an instance of <c>crFrame</c>
		// to store this data.
		m_Frame=rage_new crFrame;

		// -STOP

		if(m_Clip != NULL)
		{
			// -- Initialize the animation frame based on the degrees of freedom
			// found in the skeleton data.
			const crSkeletonData& skelData = GetSkeleton().GetSkeletonData();
			m_Frame->InitCreateBoneAndMoverDofs(skelData, false);

			// -- Initialize the values of the degrees of freedom in the newly created 
			// frame to identity.
			m_Frame->IdentityFromSkel(skelData);

			// -STOP

			// STEP #4. Demonstrate the retrieval of a property
			
			// -- Properties are simple meta data that can be attached to any clip,
			// they are useful for storing a range of general and project specific
			// additional data on a per clip basis.

			// -- Try and find the property using the keyword
			const crProperty* prop = m_Clip->HasProperties() ? m_Clip->GetProperties()->FindProperty("Description") : NULL;

			// -- Check property was found and retrieve its string attribute
			if(prop)
			{
				const crPropertyAttribute* attrib = prop->GetAttribute("String");
				if(attrib && attrib->GetType() == crPropertyAttribute::kTypeString)
				{
					const crPropertyAttributeString* attribString = static_cast<const crPropertyAttributeString*>(attrib);
					const char* descriptionString = attribString->GetString().c_str();

					Printf("Clip description '%s'\n", descriptionString);
				}
			}
		}

#if __BANK
		AddWidgetsPlayer();
#endif // __BANK
	}

	void ShutdownClient()
	{
		delete m_Player;
		delete m_Clip;
		delete m_Frame;

		craSampleManager::ShutdownClient();

		crClip::ShutdownClass();
	}

	void UpdateClient()
	{
		crSkeleton& skeleton = GetSkeleton();

		if(m_Clip != NULL)
		{
			// STEP #5. Request frame data from the player.

			// -- To retrieve a frame from the player we call <c>crClipPlayer::Composite()</c>.
			m_Player->Composite(*m_Frame);

			// -STOP


			// STEP #6. Pose the skeleton.

			//-- Now we take the frame data and pose the bones in the skeleton.  
			// To do this, just pass the frame a reference to the skeleton to be posed.  
			// Posing sets the local matrices in skeleton.
			m_Frame->Pose(skeleton);

			// -STOP

		}
		else
		{
			skeleton.Reset();
		}

		// STEP #7. Compute the global matrices of the skeleton bones.

		//-- Updating the skeleton will compute the global matrices 
		// which will be used for rendering the skinned object.
		skeleton.Update();
		UpdateMatrixSet();

		// -STOP

		if (GetPlay() && m_Clip != NULL)
		{
			// STEP #8. Update the player.

			// -- First we ensure that the player is updated with the current playback rate.
			m_Player->SetRate(GetPlaybackRate());

			// -- Now we advance time in the player by passing in the game clock delta.
			// The player automatically takes care of looping the clip for us.
			// Pass in a tag trigger functor; this function will receive a callback
			// if updating the clip causes any tags to be entered or exited.

			float junkUnusedTime;
			bool junkHasLoopedOrEnded;
			crClipPlayer::TagTriggerFunctor tagTriggerFunctor = MakeFunctor(*this, &crSampleClip::TagTriggerCallback);
			m_Player->Update(TIME.GetSeconds(), junkUnusedTime, junkHasLoopedOrEnded, &tagTriggerFunctor);

			// -STOP
		}
	}

	void TagTriggerCallback(const crTag& tag, bool enterOrExit, bool entered, float time)
	{
		// STEP #9. Process the tag trigger functor callback.
		
		// -- Is this an enter or exit event?
		if(enterOrExit)
		{
			if(entered)
			{
				// -- Display some basic tag information on entrance
				Printf("Entered tag '%s' %08x %f seconds ago\n", tag.GetName(), tag.GetKey(), time);
				Printf("Tag start %.3f mid %.3f end %.3f\n", tag.GetStart(), tag.GetMid(), tag.GetEnd());

				// -- Test if the tag is a "label"
				// Labels are a standard tag arrangement, containing 
				// a description string that has logical significance.
				if(tag.GetKey() == crTag::CalcKey("Label"))
				{
					// -- labels contain one string property attribute, also named "label"
					const crProperty& prop = tag.GetProperty();
					const crPropertyAttribute* attrib = prop.GetAttribute("Label");
					if(attrib && attrib->GetType() == crPropertyAttribute::kTypeString)
					{
						const crPropertyAttributeString* attribString = static_cast<const crPropertyAttributeString*>(attrib);
						const char* labelText = attribString->GetString().c_str();

						Printf("Tag contains label '%s'\n", labelText);
					}
				}
			}
			else
			{
				// -- Display some basic tag information on exit
				Printf("Exited tag '%s' %08x %f seconds ago\n", tag.GetName(), tag.GetKey(), time);
			}
		}
		else
		{
			// -- Display some basic tag information on update
			Printf("Update tag '%s' %08x\n", tag.GetName(), tag.GetKey());
		}
		// -STOP
	}

#if __BANK
	void AddWidgetsPlayer()
	{
//		bkBank &playerBank = BANKMGR.CreateBank("Player", 50, 500);
//		m_Player->AddWidgets(playerBank);
	}
#endif // __BANK

	const char* GetDefaultFile()
	{
		const char* defaultFile = DEFAULT_FILE;
		PARAM_file.Get(defaultFile);
		return defaultFile;
	}

private:
	crClip* m_Clip;
	crFrame* m_Frame;
	crClipPlayer* m_Player;
};


// main application
int Main()
{
	crSampleClip sampleClip;
	sampleClip.Init();

	sampleClip.UpdateLoop();

	sampleClip.Shutdown();

	return 0;
}




