// 
// /sample_breakableglass.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_rmcore/sample_rmcore.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "breakableglass/bgdrawable.h"
#include "breakableglass/breakable.h"
#include "breakableglass/crackstemplate.h"
#include "breakableglass/glassmanager.h"
#include "file/asset.h"
#include "grcore/im.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grmodel/setup.h"
#include "grmodel/shaderfx.h"
#include "input/mouse.h"
#include "math/random.h"
#include "phbound/boundbox.h"
#include "phcore/materialmgrimpl.h"
#include "physics/collider.h"
#include "physics/sleep.h"
#include "physics/simulator.h"
#include "system/main.h"
#include "system/namedpipe.h"
#include "system/param.h"
#include "system/timemgr.h"

using namespace rage;

PARAM(pipeclient,"[sample_immediatemode] specify that this instance of the executable will send IM commands");
PARAM(pipeserver,"[sample_immediatemode] specify that this instance of the executable will receive IM commands");
PARAM(perftest,"[sample_immediatemode] repeat render 1000 times for performance testing");
PARAM(file, "[sample_breakableblass] required by sample_rmcore");


class bgSampleManager : public ragesamples::rmcSampleManager
{
public: 
	bgSampleManager() : m_IsServer(false)
		, m_Pipe(NULL)
		, m_pShader(NULL)
		, m_PaneHandle(bgGlassHandle_Invalid)
		, m_modelPitch(0.f)
		, m_modelRoll(0.f)
		, m_modelYaw(0.f)
		, m_uv0(0.f, 0.f)
		, m_uv1(1.f, 1.f)
		, m_phMaterialID(phMaterialMgr::DEFAULT_MATERIAL_ID)
		, m_pModelInfo(NULL)
		, m_GlassTypeOverride(-1)
	{
#if __BANK
		const char* param;
		if (PARAM_pipeclient.Get(param))
		{
			Printf("Connecting to server...");
			m_IsServer=false;
			m_Pipe=rage_new sysNamedPipe;
			while (m_Pipe->Open(bkRemotePacket::GetRagSocketPort(bkRemotePacket::SOCKET_OFFSET_USER0))!=true);
			Displayf("connected.");
		}
# if __WIN32PC
		else if (PARAM_pipeserver.Get(param))
		{
			m_IsServer=true;
			Printf("Waiting for client to connect...");
			m_Pipe=rage_new sysNamedPipe;
			while (m_Pipe->Create(bkRemotePacket::GetRagSocketPort(bkRemotePacket::SOCKET_OFFSET_USER0))!=true);
			GRCDEVICE.SetBlockOnLostFocus(false);
			Displayf("client connected.");
		}
# endif
#endif
	}

#define kPaneSize 5.f
#define kPaneThickness 0.1f
	static const int kMaxActiveGlass = 16;     // # pieces of glass
	static const int kGpuBufferMemSize = 1024; // kilobytes of memory for GPU buffers
	static const int kBreakableHeapSize = 128; // kb for breakable heap
	void InitClient()
	{
		// Base InitClient()
		grcSampleManager::InitClient();

		// create material manager
		rage::phMaterialMgrImpl<rage::phMaterial>::Create();

		// define constraints
		const float kfWorldExtents = 6145.0f;

		const rage::Vector3 worldExtentsMin( -kfWorldExtents, -kfWorldExtents, -kfWorldExtents );
		const rage::Vector3 worldExtentsMax( +kfWorldExtents, +kfWorldExtents, +kfWorldExtents );

		const int numOctreeNodes = 2000;
		const int maxFixedObjects = 2500;
		const int maxActiveObjects = 400;
		const int maxInactiveObjects = 50;
		const int numConstraints = 625;
		const int NUM_CLOTH_BEHAVIOURS = 32;
		const int numInstBehaviors = NUM_CLOTH_BEHAVIOURS;

		// create physics level
		phConfig::EnableRefCounting();
		rage::phLevelNew *physicsLevel = rage_new rage::phLevelNew;
		physicsLevel->SetExtents(VECTOR3_TO_VEC3V(worldExtentsMin), VECTOR3_TO_VEC3V(worldExtentsMax));
		physicsLevel->SetMaxActive(maxActiveObjects);
		physicsLevel->SetNumOctreeNodes(numOctreeNodes);
		physicsLevel->SetMaxObjects(maxFixedObjects + maxActiveObjects + maxInactiveObjects);
#if LEVELNEW_ENABLE_DEFERRED_OCTREE_UPDATE
		const int maxDeferredInstanceCount = 50;
		physicsLevel->SetMaxDeferredInstanceCount(maxDeferredInstanceCount);
#endif
		physicsLevel->Init();
		physicsLevel->SetActiveInstance();

		// create physics simulator
		rage::phSimulator::InitClass();
		rage::phSimulator *physicsSimulator = rage_new rage::phSimulator;
		physicsSimulator->SetActiveInstance();
		phSimulator::InitParams params;
		params.maxInstBehaviors = numInstBehaviors;
		params.maxConstraints = numConstraints;
		params.maxManagedColliders = maxActiveObjects;
		params.scratchpadSize = 6*1024*1024;
		params.maxManifolds = 2048;
		physicsSimulator->Init(physicsLevel, params);

		// Add floor
		m_box.SetBoxSize(Vector3(24.f, 0.5f, 24.f));
		m_archetype.SetBound(&m_box);
		m_archetype.SetMass(1.f);
		m_inst.SetArchetype(&m_archetype);
		Matrix34 transform;
		transform.Identity3x3();
		transform.d = Vector3(0.f, -2.5f, 0.f);
		m_inst.SetMatrix(MATRIX34_TO_MAT34V(transform));

		PHSIM->AddFixedObject(&m_inst);

		m_phMaterialID = phMaterialMgr::DEFAULT_MATERIAL_ID;

		ASSET.PushFolder("sample_breakableglass");
		m_pDiffuse = grcTextureFactory::CreateTexture("GlassDiffuseAlpha");
		m_pSpecular = grcTextureFactory::CreateTexture("GlassSpecular");
		m_pEnvironment = grcTextureFactory::CreateTexture("GlassEnvironment");
		ASSET.PopFolder();

		// map input
		m_Mapper.Map(IOMS_MOUSE_BUTTON,ioMouse::MOUSE_LEFT,m_Shoot);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_CONTROL, m_Ctrl);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_ALT, m_Alt);

		// Create glass geometry
		// first we need a flexible vertex format to match our shader
		grcFvf fvf;
		fvf.SetPosChannel(true, grcFvf::grcdsFloat3);
		fvf.SetDiffuseChannel(true, grcFvf::grcdsColor);
		fvf.SetTextureChannel(0, true, grcFvf::grcdsFloat2);
		fvf.SetNormalChannel(true, grcFvf::grcdsFloat3);
		fvf.SetTangentChannel(0, true, grcFvf::grcdsPackedNormal);
		m_pVertexBuffer = grcVertexBuffer::Create(5, fvf, __PS3);
		m_pIndexBuffer = grcIndexBuffer::Create(6);
		u16* pIndices = m_pIndexBuffer->LockRW();
		pIndices[0] = 0;
		pIndices[1] = 3;
		pIndices[2] = 2;
		pIndices[3] = 0;
		pIndices[4] = 2;
		pIndices[5] = 1;
		m_pIndexBuffer->Unlock();

		m_pShader = rage_new grmShaderFx;
		m_pShader->Load("rage_breakableglass", NULL);

		// set texture vars (normally artist-specified)
		SetShaderTexureVar(m_pDiffuse,     "DiffuseTex");
		SetShaderTexureVar(m_pSpecular,    "SpecularTex");
		SetShaderTexureVar(m_pEnvironment, "EnvironmentTex");

		ASSET.PushFolder("sample_breakableglass");
		bgGlassManager::InitClass(kMaxActiveGlass, kGpuBufferMemSize, kBreakableHeapSize);
		ASSET.PopFolder();
		m_GlassTypeOverride = -1;
		m_transform = M34_IDENTITY;

		m_bRegenerateGlass = true;
		m_bBreakGlass = false;
	}

	void ShutdownClient()
	{
		if (bgGlassHandle_Invalid != m_PaneHandle)
		{
			bgGlassManager::DestroyBreakableGlass(m_PaneHandle);
		}

		bgGlassManager::ShutdownClass();

		delete m_pModelInfo;
		m_pModelInfo = NULL;

		delete m_pShader;
		m_pShader=NULL;

		LastSafeRelease(m_pDiffuse);
		LastSafeRelease(m_pSpecular);
		LastSafeRelease(m_pEnvironment);


		delete m_pVertexBuffer;
		m_pVertexBuffer = 0;
		delete m_pIndexBuffer;
		m_pIndexBuffer = 0;

		PHSIM->DeleteObject(m_inst.GetLevelIndex());

		m_inst.SetArchetype(NULL, false);
		m_archetype.SetBound(NULL);
		m_box.Release(false);

		// clear out the simulator and delete it
		rage::phSimulator *physicsSimulator = PHSIM;
		phSimulator::SetActiveInstance(0);
		delete physicsSimulator;
		phSimulator::ShutdownClass();

		// clear out the level and delete it
		rage::phLevelNew *physicsLevel = PHLEVEL;
		physicsLevel->Shutdown();
		delete physicsLevel;

		// destroy material manager
		MATERIALMGR.Destroy();

		grcSampleManager::ShutdownClient();
	}

	void InitLights()
	{
		m_LightMode = grclmNone;
		grcSampleManager::InitLights();
	}

	void InitCamera()
	{
		grcSampleManager::InitSampleCamera(Vector3(-3.113f, 6.368f, -9.9586f), Vector3(0.f, 0.f, 0.f));
	}

	void InitSetup()
	{
		m_Setup = rage_new grmSetup;
	}

#if __DEV && __BANK
	void AddWidgetsClient()
	{
		m_pShader->AddWidgets(BANKMGR.CreateBank("rage_breakableglass shader"));
		bkBank& bk = BANKMGR.CreateBank("rage - SampleBreakableGlass");
		bk.AddSlider("Pane Type Override", &m_GlassTypeOverride, -1, bgSCracksTemplate::GetInstance().GetGlassTypes().GetCount()-1, 1);
		bk.AddSlider("Model yaw", &m_modelYaw, -3.5f, 3.5f, 0.001f);
		bk.AddSlider("Model pitch", &m_modelPitch, -3.5f, 3.5f, 0.001f);
		bk.AddSlider("Model roll", &m_modelRoll, -3.5f, 3.5f, 0.001f);
		bk.AddSlider("uv0.x", &m_uv0.x, 0.f, 1.f, 0.001f);
		bk.AddSlider("uv0.y", &m_uv0.y, 0.f, 1.f, 0.001f);
		bk.AddSlider("uv1.x", &m_uv1.x, 0.f, 1.f, 0.001f);
		bk.AddSlider("uv1.y", &m_uv1.y, 0.f, 1.f, 0.001f);
		bgBreakable::AddWidgets(bk);
	}
#endif

	void RegenerateGlass()
	{
		if (bgGlassHandle_Invalid != m_PaneHandle)
		{
			bgGlassManager::DestroyBreakableGlass(m_PaneHandle);
#if !__NO_OUTPUT
			size_t used, available, largest;
			bgBreakable::GetPrivateHeapStats(used, available, largest);
			bgDebugf1("No Glass: used: %i, available: %i largest: %i",used, available, largest );
#endif
		}
		atRangeArray<Vec3V, 5> positions;
		positions[0] = Vec3V(-kPaneSize/2.f,  0.f,       0.f);
		positions[1] = Vec3V( kPaneSize/2.f,  0.f,       0.f);
		positions[2] = Vec3V( kPaneSize/2.f,  kPaneSize,       0.f);
		positions[3] = Vec3V(-kPaneSize/2.f,  kPaneSize,       0.f);
		positions[4] = Vec3V(0.f,  0.f,       kPaneThickness);

		Mat34V transform;
		Vec3V angles(m_modelYaw, m_modelPitch, m_modelRoll);
		Mat34VFromEulersXYZ( transform, angles );


		for (int i=0; i<positions.GetMaxCount(); i++)
		{
			positions[i] = Transform(transform, positions[i]);
		}


		{
			grcVertexBufferEditor editor(m_pVertexBuffer, true, false);
			for (int i=0; i<positions.GetMaxCount(); i++)
			{
				editor.SetPosition(i, RCC_VECTOR3(positions[i]));
			}

			editor.SetUV(0, 0, Vector2(m_uv0.x, m_uv0.y));
			editor.SetUV(1, 0, Vector2(m_uv1.x, m_uv0.y));
			editor.SetUV(2, 0, Vector2(m_uv1.x, m_uv1.y));
			editor.SetUV(3, 0, Vector2(m_uv0.x, m_uv1.y));
			editor.SetUV(4, 0, Vector2(0.f, 0.f)); // unused

			Vector3 normal(0.f, 0.f, 1.f);
			editor.SetNormal(0, normal);
			editor.SetNormal(1, normal);
			editor.SetNormal(2, normal);
			editor.SetNormal(3, normal);
			editor.SetNormal(4, normal);
			Vector3 tangent(0.f, 1.f, 0.f);
			editor.SetTangent(0, 0, tangent);
			editor.SetTangent(1, 0, tangent);
			editor.SetTangent(2, 0, tangent);
			editor.SetTangent(3, 0, tangent);
			editor.SetTangent(4, 0, tangent);
		}

		// regenerate model info
		if (m_pModelInfo)
		{
			delete m_pModelInfo;
		}

		//Randomly determine glass type, unless override is specified
		int glassType = (m_random.GetRanged(0, bgSCracksTemplate::GetInstance().GetGlassTypes().GetCount()-1));
		if (m_GlassTypeOverride > -1)
		{
			glassType = m_GlassTypeOverride;
		}

		m_pModelInfo = bgPaneModelInfoBase::CreateModelInfo(
			m_pVertexBuffer, 
			m_pIndexBuffer,
			0, // shaderIndex
			u8(glassType), // glassType
			-1); // boneIndex

		m_pModelInfo->ComputePlane(m_glassPlane);
	}

	void UpdateClient()
	{
		PHSIM->Update(TIME.GetSeconds());

		// Update user input
		m_Mapper.Update();

		if (m_Shoot.IsPressed() && m_Alt.IsUp())
		{
			// Reset glass pane if requested
			if (m_Ctrl.IsDown())
			{
				m_bRegenerateGlass = true;
			}

			grcViewport::GetCurrent()->ResetPerspective();
			// Get the line segment in world coords from the near clip plane to the far clip plane
			// at the current mouse location
			Vector3 mouseNear, mouseFar;
			grcViewport::GetCurrent()->ReverseTransform(
				float(ioMouse::GetX()), 
				float(ioMouse::GetY()),
				mouseNear, mouseFar);

			// intersect that segment with the glass plane
			// @TODO: should fix GetPlane() to transform to world space; 
			// doesn't matter for now since our sample glass uses the identity transform.

			float nearDist = m_glassPlane.DistanceToPlane(mouseNear);
			float farDist = m_glassPlane.DistanceToPlane(mouseFar);
			float denom = 1.f / (nearDist - farDist);
			Vector3 intersect = nearDist * denom * mouseFar - farDist * denom * mouseNear;
			Vector3 impactDir = intersect - mouseNear;
			m_bBreakGlass = true;
			m_impactLoc = RCC_VEC3V(intersect);
			m_impactDir = RCC_VEC3V(impactDir);
		}

		grcSampleManager::UpdateClient();

		if (m_bRegenerateGlass)
		{
			RegenerateGlass();
			m_bRegenerateGlass = false;
			// regenerated glass is invisible by default, so break it in the center to make it visible
			if (!m_bBreakGlass)
			{
				m_bBreakGlass = true;
				Vector3 impactLoc = m_pModelInfo->m_posBase + 0.5 * m_pModelInfo->m_posWidth + 0.5* m_pModelInfo->m_posHeight;
				Vector3 impactDir = m_glassPlane.GetVector3();
				m_impactLoc = RCC_VEC3V(impactLoc);
				m_impactDir = RCC_VEC3V(impactDir);
			}
		}

		if (m_bBreakGlass)
		{
			BreakGlass();
			m_bBreakGlass = false;
		}

		if (bgGlassHandle_Invalid != m_PaneHandle)
		{
			const grcViewport* pViewport = grcViewport::GetCurrent();
			if (NULL != pViewport)
			{
				Vec3V viewportPosition = RCC_VEC3V(pViewport->GetCameraMtx().d);
				int viewportWidth = pViewport->GetWidth();
				float viewportTanHFOV = pViewport->GetTanHFOV();
				bgGlassManager::SetViewportData(
					viewportPosition, 
					viewportWidth, 
					viewportTanHFOV );
				bgGlassManager::UpdateBreakableGlass(m_PaneHandle, 
					RCC_MAT34V(m_transform), 
					TIME.GetSeconds());

			}
			bgGlassManager::UpdateBuffers();
		}

		bgGlassManager::SyncBreakableGlass();
	}

	const grcInstanceData& GetInstanceData(int)
	{
		return m_pShader->GetInstanceData();
	}

	void BreakGlass()
	{
		if (m_PaneHandle == bgGlassHandle_Invalid)
		{
			bgGetInstanceDataFunc getInstanceDataFunc;
			getInstanceDataFunc.Bind(this, &bgSampleManager::GetInstanceData);
			bgGlassManager::CreateBreakableGlass(&m_PaneHandle, 
				RCC_MAT34V(m_transform),
				*m_pModelInfo,
				getInstanceDataFunc,
				NULL,
				m_phMaterialID);
		}
		HitGlass(m_PaneHandle);
	}

	bgGlassImpactFunc GetGlassImpactFunc() const
	{
		bgGlassImpactFunc impactFunc;
		impactFunc.Bind(this, &bgSampleManager::HitGlass);
		return impactFunc;
	}

	void HitGlass(bgGlassHandle in_handle)
	{
		bgGlassManager::HitGlass(in_handle, 0, m_impactLoc, m_impactDir);
	}

	void DrawScene()
	{
		// STEP #1. Set our transformation matrix.

		//-- Set the matrix that gets applied to all subsequent geometry.
		grcViewport::SetCurrentWorldMtx(RCC_MAT34V(m_transform));

		int passCount = PARAM_perftest.Get()? 1000 : 1;
		for (int passes=0; passes<passCount; passes++) 
		{
			// render a nice solid floor
#if __PFDRAW
			m_box.Draw(MAT34V_TO_MATRIX34(m_inst.GetMatrix()), false, false, 0, 0, 0);
#endif

			// render glass
			if (bgGlassHandle_Invalid != m_PaneHandle)
				bgGlassManager::DrawBreakableGlass(m_PaneHandle);
		}

		// -STOP
	}

	void DrawClient()
	{
		DrawScene();
		// needs to be called once/frame from render thread
		bgDrawable::EndFrame();
	}

private:
	// Set the specified texture var and release the texture
	void SetShaderTexureVar(grcTexture* in_pTexture, const char* in_pVarName)
	{
		grcEffectVar var = m_pShader->LookupVar(in_pVarName);
		m_pShader->SetVar(var, in_pTexture);
	}

	rage::mthRandom m_random;

	rage::phBoundBox m_box;
	rage::phArchetypePhys m_archetype;
	rage::phInst m_inst;

	ioMapper		    m_Mapper;
	ioValue			    m_Shoot;  // break the pane
	ioValue             m_Alt;    // ignore mouse click while Alt is down; 
	ioValue				m_Ctrl;   // when pressed while shooting, reset the pane first

	Matrix34 m_transform;

	bgGlassHandle m_PaneHandle; 
	bool m_IsServer;
	sysNamedPipe* m_Pipe;
	grmShaderFx* m_pShader;

	grcVertexBuffer* m_pVertexBuffer;
	grcIndexBuffer* m_pIndexBuffer;

	grcTexture *m_pDiffuse, *m_pSpecular, *m_pEnvironment;

	bool m_bBreakGlass;
	bool m_bRegenerateGlass;
	int m_GlassTypeOverride;
	float m_modelPitch;
	float m_modelYaw;
	float m_modelRoll;
	Vector2 m_uv0;
	Vector2 m_uv1;
	int m_phMaterialID;
	bgPaneModelInfoBase* m_pModelInfo;
	Vector4 m_glassPlane;
	Vec3V m_impactLoc;
	Vec3V m_impactDir;
};

#include "file/remote.h"

// main application
int Main()
{
	bgSampleManager sample;

	sample.SetFullAssetPath(RAGE_ASSET_ROOT);

	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}
