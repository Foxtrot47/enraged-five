// 
// /sample_multicharactercloth.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// This sample has 4 threads.
// Thread0 - Update/Sync Thread
//				This threads job is to update the scene and build the draw list to be rendered next frame
// Thread1 - Draw Thread
//				This threads job is to execute draw calls for the drawlist build last frame
// Thread2 - Character 0 Thread
// Thread3 - Character 1 Thread
//				These two threads update the animation and cloth for each character

// The threads execute as follows each frame:
//		Thread0				Thread1				Thread2				Thread3
//	Build Drawlist0		----------------	Anim/ClothA0		Anim/ClothB0
//	Build Drawlist1		Draw Drawlist0		Anim/ClothA1		Anim/ClothB1
//	Build Drawlist0		Draw Drawlist1		Anim/ClothA0		Anim/ClothB0
//	Build Drawlist1		Draw Drawlist0		Anim/ClothA1		Anim/ClothB1
//	...					...					...					...


//////////////////////////////////////////////////////////////////////////////
// Includes
  

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "cloth/charactercloth.h"
#include "grcore/device.h"
#include "grmodel/geometry.h"
#include "paging/rscbuilder.h"
#include "paging/streamer.h"
#include "phbound/boundtaperedcapsule.h"
#include "phcore/config.h"
#include "phcore/surface.h"
#include "phcore/materialmgrimpl.h"
#include "pheffects/morphgeometry.h"
#include "pheffects/resourceversions.h"
#include "physics/simulator.h"
#include "physics/levelnew.h"
#include "sample_fragment/fragworld.h"
#include "sample_physics/sample_physics.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "system/xtl.h"
#include "vectormath/vec3v.h"

//////////////////////////////////////////////////////////////////////////////
// Defines
#define DRAW_THREAD_STACK		(128 * 1024)
#define CHAR_THREAD_STACK		(512 * 1024)
#define RESOURCE_VERSION		(0 + clthResourceVersion)
#define CHARACTER_RESOURCE_SIZE	(1024 * 1024 * 64)				// 64mb should be enough for now

//////////////////////////////////////////////////////////////////////////////
// Parameters
PARAM(file,		"[sample_multicharactercloth] unused");
PARAM(char0,	"[sample_multicharactercloth] type file name for character 0");
PARAM(char1,	"[sample_multicharactercloth] type file name for character 1");
PARAM(ragdoll0,	"[sample_multicharactercloth] ragdoll type file name for character 0");
PARAM(ragdoll1,	"[sample_multicharactercloth] ragdoll type file name for character 1");
PARAM(anim0,	"[sample_multicharactercloth] animation file name for character 0");
PARAM(anim1,	"[sample_multicharactercloth] animation file name for character 1");
PARAM(regen,	"[sample_multicharactercloth] Force resource regeneration");
namespace rage { XPARAM(d3dmt); }


namespace ragesamples
{

typedef struct _ThreadParams
{
	sysIpcSema	m_eTrigger;
	sysIpcSema	m_eFinished;
	sysIpcSema	m_eKill;
	sysIpcSema	m_eDead;
	
	_ThreadParams()
	{
		m_eTrigger = sysIpcCreateSema(false);
		m_eFinished = sysIpcCreateSema(false);
		m_eKill = sysIpcCreateSema(false);
		m_eDead = sysIpcCreateSema(false);
	}

	~_ThreadParams()
	{
		sysIpcDeleteSema(m_eTrigger);
		sysIpcDeleteSema(m_eFinished);
		sysIpcDeleteSema(m_eKill);
		sysIpcDeleteSema(m_eDead);
	}

	bool IsTriggered()
	{
		return sysIpcPollSema(m_eTrigger);
	}

	bool IsFinished()
	{
		return sysIpcPollSema(m_eFinished);
	}

	bool IsKilled()
	{
		return sysIpcPollSema(m_eKill);
	}

	bool IsDead()
	{
		return sysIpcPollSema(m_eDead);
	}

	void Trigger()
	{
		sysIpcSignalSema(m_eTrigger);
	}

	void SetFinished()
	{
		sysIpcSignalSema(m_eFinished);
	}

	void Finish()
	{
		sysIpcWaitSema(m_eFinished);
	}
	
	void Kill()
	{
		sysIpcSignalSema(m_eKill);
		sysIpcWaitSema(m_eDead);
	}

	void MarkDead()
	{
		sysIpcSignalSema(m_eDead);
	}
} ThreadParams;

typedef struct _ClothParams
{
	ThreadParams		m_ThreadParams;
	characterCloth*		m_Character;
	crAnimation*		m_Animation;
	crFrame			m_AnimFrame;
	crFrame			m_BlendFrame;
	crFrame			m_InitialFrame;
	float				m_AnimPhase;
	float				m_BlendPhase;

	_ClothParams()
	{
		m_Character = 0;
		m_Animation = 0;
		m_AnimPhase = 0.0f;
		m_BlendPhase = 0.0f;
	}
} ClothParams;


PRE_DECLARE_THREAD_FUNC(DrawThreadMain);
PRE_DECLARE_THREAD_FUNC(CharacterThreadMain);
void InitAnimation(ClothParams* pClothParams);
void UpdateAnimation(ClothParams* pClothParams);

class sampleMultiThreadCloth : public physicsSampleManager
{
public:
	sampleMultiThreadCloth()
	{
		m_bCharacter0Enabled = true;
		m_bCharacter1Enabled = true;
		m_LightMode = grclmPoint;
	}

	virtual ~sampleMultiThreadCloth()
	{
	}

	virtual void InitClient()
	{
		physicsSampleManager::InitClient();

		// Create and initialize a simple world.
		fragWorld* clothWorld = rage_new fragWorld("World");
		clothWorld->Init(1000, 500, 500, Vec3V( -999.0f, -999.0f, -999.0f), Vec3V(+999.0f, +999.0f, +999.0f), 9);
		const bool triangulated = true;
		const u32 includeFlags = INCLUDE_FLAGS_ALL;
		clothWorld->ConstructTerrainPlane(triangulated,includeFlags);
		m_Demos.AddDemo(clothWorld);
		m_Demos.SetCurrentDemo(0);
		m_Demos.GetCurrentWorld()->Activate();

		// Init Physics Stuff
		//InitPhysics();

		// Init widgets
#if __BANK
		bkBank* pBank = &BANKMGR.CreateBank("sample_multithreadcloth");
		pBank->AddToggle("Enable Character0", &m_bCharacter0Enabled);
		pBank->AddToggle("Enable Character1", &m_bCharacter1Enabled);
//#if __PFDRAW
//		GetRageProfileDraw().Init(2000000, true, grcBatcher::BUF_FULL_IGNORE);
//		GetRageProfileDraw().SetEnabled(true);
//#endif
#endif

		// Setup character strings from command line parameters
		const char* character0 = "$/fragments/player_mexicanRebel/entity.type";
		const char* character1 = "$/fragments/player_mexicanRebel/entity.type";
		const char* ragdoll0 = "$/fragments/player_mexicanRebel/entity.type";
		const char* ragdoll1 = "$/fragments/player_mexicanRebel/entity.type";
		const char* anim0 = "default.anim";
		const char* anim1 = "default.anim";
		PARAM_char0.Get(character0);
		PARAM_char1.Get(character1);
		PARAM_ragdoll0.Get(ragdoll0);
		PARAM_ragdoll1.Get(ragdoll1);
		PARAM_anim0.Get(anim0);
		PARAM_anim1.Get(anim1);

		// Load character 0
		LoadCharacter(0, character0, ragdoll0, Vector3(2.0f, 0.0f, 0.0f));

		// Load character 1
		LoadCharacter(1, character1, ragdoll1, Vector3(-2.0f, 0.0f, 0.0f));

		// Load the animations
		m_ClothParams[0].m_Animation = crAnimation::AllocateAndLoad(anim0);
		m_ClothParams[1].m_Animation = crAnimation::AllocateAndLoad(anim1);
		InitAnimation(&m_ClothParams[0]);
		InitAnimation(&m_ClothParams[1]);

		// Relinquish control of the graphics device so the draw thread can take over
		GRCDEVICE.ReleaseThreadOwnership();

		// Create the draw thread
		sysIpcCreateThread(DrawThreadMain, this, DRAW_THREAD_STACK,  rage::PRIO_BELOW_NORMAL, "DrawThreadMain", 1);

		// Create the character threads
		sysIpcCreateThread(CharacterThreadMain, &m_ClothParams[0], CHAR_THREAD_STACK, rage::PRIO_BELOW_NORMAL, "CharacterThreadMain [0]", 5);
		sysIpcCreateThread(CharacterThreadMain, &m_ClothParams[1], CHAR_THREAD_STACK, rage::PRIO_BELOW_NORMAL, "CharacterThreadMain [1]", 3);
	}

	virtual void ShutdownClient()
	{
#if __WIN32
		Assert(_heapchk() == _HEAPOK);
#endif

		// Kill the threads
		m_DrawThreadParams.Kill();
		m_ClothParams[0].m_ThreadParams.Kill();
		m_ClothParams[1].m_ThreadParams.Kill();

		// Delete the characters
		if( m_ClothParams[0].m_Character )
			delete m_ClothParams[0].m_Character;
		if( m_ClothParams[1].m_Character )
			delete m_ClothParams[1].m_Character;

		// Delete the animations
		if( m_ClothParams[0].m_Character )
			delete m_ClothParams[0].m_Animation;
		if( m_ClothParams[1].m_Character )
			delete m_ClothParams[1].m_Animation;

		// Cleanup the physics
		delete FRAGMGR;
		delete PHSIM;
		PHLEVEL->Shutdown();
		delete PHLEVEL;
		MATERIALMGR.Destroy();

#if __WIN32
		Assert(_heapchk() == _HEAPOK);
#endif
	}

	virtual void UpdateClient()
	{
		// Wait for the draw thread to be done
		m_DrawThreadParams.Finish();

		// Trigger the character threads
		if( m_bCharacter0Enabled )
			m_ClothParams[0].m_ThreadParams.Trigger();
		if( m_bCharacter1Enabled )
			m_ClothParams[1].m_ThreadParams.Trigger();

#if __WIN32
		// Do the scene updating here
		DWORD dwTicks = GetTickCount();
		while( GetTickCount() - dwTicks < 15 )
		{
		}
#endif

		// Wait for character threads to finish
		if( m_bCharacter0Enabled )
			m_ClothParams[0].m_ThreadParams.Finish();

		if( m_bCharacter1Enabled )
			m_ClothParams[1].m_ThreadParams.Finish();

		// swap the draw buffers
		grmGeometryQB::SwapBuffers();

		// Trigger the draw thread
		m_DrawThreadParams.Trigger();
	}

	virtual void Draw()
	{
		// Override draw so that the main thread doesnt render anything
	}

	void DrawFrame()
	{
		// This is the draw call executed from the draw thread.  
		// Call the base class draw function which will in turn call our DrawClient function
		rmcSampleManager::Draw();
	}

	virtual void DrawClient()
	{
		Matrix34 world = GetCamMgr().GetWorldMtx();

		for( int bucket = 0; bucket < NUM_BUCKETS; bucket++ )
		{
			if( GetBucketEnable(bucket) )
			{
				// Draw character 0
				if( m_bCharacter0Enabled && m_ClothParams[0].m_Character )
				{
					m_ClothParams[0].m_Character->Draw(world, bucket);
				}		

				// Draw character 1
				if( m_bCharacter1Enabled && m_ClothParams[1].m_Character )
				{
					m_ClothParams[1].m_Character->Draw(world, bucket);
				}
			}
		}

#if __PFDRAW
		grcBindTexture(NULL);
		grcState::Default();
		grcLightState::SetEnabled(true);

		
//		m_ClothParams[0].m_Character->m_RagDoll->GetArchetype()->GetBound()->Draw(m_ClothParams[0].m_Character->m_RagDoll->GetMatrix());

		GetRageProfileDraw().Render();
#endif
	}

	void LoadCharacter(int nCharacter, const char* szCharacter, const char* szRagdoll, const Vector3& pos)
	{
#if 0//__PAGING
		// Build the name of the resource
		char szCharacterName[256];
		char szResource[256];
		//strcpy(szCharacterName, fiAssetManager::FileName(szCharacter));
		fiAssetManager::BaseName(szCharacterName, 256, szCharacter);
		sprintf(szResource, "resource\\%s", szCharacterName);

		// Setup custom bound loader for tapered capsules
		phBound::SetCustomResourceConstructor(MakeFunctorRet (phBoundTaperedCapsule::ResourceCreate));
		phBound::SetCustomResourceConstructor(MakeFunctorRet (phMorphGeometry::NewRscTypeSpecific));


		// Load the character from resource
		pgStreamer::InitClass();
		pgStreamerInfo info;
		datResourceMap map;
		if( !pgRscBuilder::BeginStream(szResource, "#char", RESOURCE_VERSION, info, map) )
			Quitf("Cannot stream '%s'", szResource);
		pgRscBuilder::EndStream(m_ClothParams[nCharacter].m_Character, info, map, true /*block*/,true /*close*/, szResource);
		pgStreamer::ShutdownClass();

		m_ClothParams[nCharacter].m_Character->InsertIntoLevel();

#else // __PAGING

		// Paging is turned off so just load from disk normally
		{
			// Create the character cloth object
			m_ClothParams[nCharacter].m_Character = rage_new characterCloth();

			// Load the ragdoll and the character entity
			m_ClothParams[nCharacter].m_Character->Load(szCharacter, szRagdoll);

			// Reinsert into the physics level to match the resource path
			m_ClothParams[nCharacter].m_Character->RemoveFromLevel();
			m_ClothParams[nCharacter].m_Character->InsertIntoLevel();
		}

#endif // __PAGING

		// Set the initial position
		Matrix34 mtx;
		mtx.Identity();
		mtx.Translate(pos);
		m_ClothParams[nCharacter].m_Character->Teleport(mtx);
	}

/*	void InitPhysics()
	{
		phConfig::EnableRefCounting(true);

		// Initialize physics material manager
		ASSET.PushFolder(RAGE_ASSET_ROOT "physics\\materials");
		phMaterialMgrImpl<phSurface>::Create();
		MATERIALMGR.Load(64);
		ASSET.PopFolder();

		// Initialize fragmanager
		rage_new fragManager();

		// Create physics level
		phLevelNew* pLevel = rage_new phLevelNew();
		phLevelNew::SetActiveInstance(pLevel);

		const int maxOctreeNodes    = 1000;
		const int maxActiveObjects  = 5000;
		const int maxObjects        = 5000;
		const float size			= 1000;	 
		const Vec3V minWorld(-size,-size,-size);
		const Vec3V maxWorld( size, size, size);

		pLevel->SetExtents(minWorld, maxWorld);
		pLevel->SetMaxObjects(maxObjects);
		pLevel->SetMaxActive(maxActiveObjects);
		pLevel->SetNumOctreeNodes(maxOctreeNodes);
		pLevel->Init();

		// Create physics simulator
		phSimulator* pSimulator = rage_new rage::phSimulator;
		phSimulator::SetActiveInstance(pSimulator);
		pSimulator = rage::phSimulator::GetActiveInstance();
		pSimulator->SetMaxInstBehaviors(0);
		pSimulator->Init(pLevel, 512);
	}*/


	// Accessors
	ThreadParams*	GetDrawThreadParams()			{ return &m_DrawThreadParams; }

protected:
	ThreadParams		m_DrawThreadParams;
	ClothParams			m_ClothParams[2];

	bool				m_bCharacter0Enabled;
	bool				m_bCharacter1Enabled;
};

DECLARE_THREAD_FUNC(DrawThreadMain)
{
	// Get the character cloth object
	sampleMultiThreadCloth* pCharacterCloth = (sampleMultiThreadCloth*)ptr;

	// Get the draw thread parameters
	ThreadParams* pParams = pCharacterCloth->GetDrawThreadParams();

	// Take control of the graphics device
	GRCDEVICE.AcquireThreadOwnership();

	// Loop until we are told to stop
	while( !pParams->IsKilled() )
	{
		// Check for trigger
		if( pParams->IsTriggered() )
		{
			// Draw this frame
			pCharacterCloth->DrawFrame();
		}

		// Mark that we are done
		pParams->SetFinished();

		// Sleep to let other threads on this processor execute
		sysIpcSleep(0);
	}

	// Thread is done, mark it as dead
	pParams->MarkDead();
}

DECLARE_THREAD_FUNC(CharacterThreadMain)
{
	// Get the cloth parameters
	ClothParams* pParams = (ClothParams*)ptr;

	// Use a constant 60hz timestep
	float fTime = 0.0166666667f;

	// Loop until we are told to stop
	while( !pParams->m_ThreadParams.IsKilled() )
	{
		// Check for trigger
		if( pParams->m_ThreadParams.IsTriggered() )
		{
			// Only process this character if its vaild
			if( pParams->m_Character )
			{
				// Update Animation
				//UpdateAnimation(pParams);

				// Update character cloth
				pParams->m_Character->Update(fTime);
			}

			// Mark that we are done
			pParams->m_ThreadParams.SetFinished();
		}

		// Sleep to let other threads on this processor execute
		sysIpcSleep(0);
	}

	// Thread is done, mark it as dead
	pParams->m_ThreadParams.MarkDead();
}

void InitAnimation(ClothParams* pClothParams)
{
	if( pClothParams->m_Character )
	{
		// Initialize the animation frames with the current skeleton
		crSkeleton* pSkeleton = pClothParams->m_Character->GetSkeleton();
		const crSkeletonData& skeletonData = pSkeleton->GetSkeletonData();

		pClothParams->m_AnimFrame.InitCreateBoneAndMoverDofs(skeletonData, false);
		pClothParams->m_BlendFrame.InitCreateBoneAndMoverDofs(skeletonData, false);
		pClothParams->m_InitialFrame.InitCreateBoneAndMoverDofs(skeletonData, false);

		pClothParams->m_AnimFrame.InversePose(*pSkeleton);		
		pClothParams->m_BlendFrame.InversePose(*pSkeleton);		
		pClothParams->m_InitialFrame.InversePose(*pSkeleton);
	}
}

void UpdateAnimation(ClothParams* pClothParams)
{
	if( pClothParams->m_Character )
	{
		// Build the animation frame
		if( pClothParams->m_BlendPhase >= 1.0f )
		{
			pClothParams->m_Animation->CompositeFrame(pClothParams->m_AnimPhase, pClothParams->m_BlendFrame);
		}
		else
		{
			pClothParams->m_Animation->CompositeFrame(pClothParams->m_AnimPhase, pClothParams->m_AnimFrame);
			pClothParams->m_BlendFrame.Set(pClothParams->m_InitialFrame);
			pClothParams->m_BlendFrame.Blend(pClothParams->m_BlendPhase, pClothParams->m_AnimFrame);

			float blendTime = 0.2f;
			pClothParams->m_BlendPhase += TIME.GetSeconds() / blendTime;

			if (pClothParams->m_BlendPhase > 1.0f)
			{
				pClothParams->m_BlendPhase = 1.0f;
			}
		}

		// Don't apply the animation frame
	//	pClothParams->m_Character->ApplyAnimationFrame(pClothParams->m_BlendFrame);

		// Increment time
		pClothParams->m_AnimPhase += TIME.GetSeconds() * 1.0f;
		if( pClothParams->m_AnimPhase > pClothParams->m_Animation->GetDuration() )
		{
			pClothParams->m_AnimPhase -= pClothParams->m_Animation->GetDuration();
		}
	}
}

}	// namespace ragesamples

// main application
int Main ()
{
//	_CrtSetBreakAlloc(28999);
#if __WIN32PC
	// Make sure we use multi-thread safe d3d implementation
	PARAM_d3dmt.Set("");
#endif

	ragesamples::sampleMultiThreadCloth sampleCloth;
	sampleCloth.Init("sample_multithreadcloth");
	sampleCloth.UpdateLoop();
	sampleCloth.Shutdown();
	return 0;
}
