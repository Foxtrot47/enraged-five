// 
// /sample_cloth.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: sample_cloth
// PURPOSE:
//		This sample shows how to add cloth to the environment.  Demonstrates interaction with a physical rigidbody


#include "bank/bkmgr.h"
#include "cloth/environmentcloth.h"
#include "fragment/cache.h"
#include "fragment/tune.h"
#include "fragment/typecloth.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"
#include "grmodel/geometry.h"
#include "grrope/grroperender.h"
#include "input/keys.h"
#include "phbound/boundsphere.h"
#include "phcore/phmath.h"
#include "pheffects/cloth_verlet.h"
#include "pheffects/clothverletinst.h"
#include "physics/colliderdispatch.h"
#include "physics/simulator.h"
#include "rmcore/drawable.h"
#include "sample_fragment/fragworld.h"
#include "sample_physics/demoobject.h"
#include "sample_physics/sample_physics.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vectormath/vec3v.h"

#include <system/xtl.h>


//	0 - sheet and ball
//	1 - clothesline
//	2 - tent
//	3 - pot on rope
//	4 - curtains with window frame collisions
//	5 - sheet and ball with z-up
#define WORLD_INDEX	0


#define MAX_NUM_ENV_CLOTH	16

PARAM(file, "Reserved for future expansion, not currently hooked up");
PARAM(wind, "strength of the wind vector");


#if !__XENON
namespace rage {
	char *XEX_TITLE_ID;
}
#endif

namespace rage {
EXT_PFD_DECLARE_GROUP(DrawCloth);
EXT_PFD_DECLARE_GROUP(Physics);
EXT_PFD_DECLARE_ITEM(Models);
EXT_PFD_DECLARE_ITEM(Solid);
EXT_PFD_DECLARE_ITEM(Wireframe);

#if (__BANK && !__RESOURCECOMPILER)
datCallback	g_ClothManagerCallback = NullCB;
#endif


#if __PFDRAW
extern void DrawClothWireframe( const Vec3V* RESTRICT vertexBuffer, const phVerletCloth& cloth, void* controllerAddress, const Vector3& offset );
#endif
}


// Cloth class structure used in sample_cloth:
//
//	fragType m_EnvClothType									(member of this sample)
//
//		fragTypeEnvCloth m_EnvCloth							(member of fragType)
//
//			environmentCloth m_Cloth						(member of fragTypeEnvCloth)
//
//				phEnvClothVerletBehavior m_Behavior			(member of environmentCloth)
//
//					phClothVerletBehavior					(base class for phEnvClothVerletBehavior)
//
//						phInstBehavior						(base class for phClothVerletBehavior)
//
//							phInst m_Instance				(phClothVerletInst, member of phInstBehavior)
//
//						phVerletCloth m_Cloth				(member of phClothVerletBehavior)
//
//							phClothData m_ClothData			(member of phVerletCloth)
//
//					environmentCloth m_ClothController		(pointer back up to the owning environmantCloth)
//
//				phInst* m_cthInst							(the same instance as in the phInstBehavior)
//
//				clothController m_ClothController			(member of environmentCloth)
//
//					phVerletCloth m_Cloth					(member of clothController, same as m_Cloth in phClothVerletBehavior)
//
//					clothBridgeSimGfx m_Bridge				(member of clothController)
//
//	fragInst m_EnvClothInst									(member of this sample, same as m_Instance in phInstBehavior)
//
//
//	Classes for character cloth:
//
//		fragTypeCharCloth
//
//			characterCloth m_Cloth							(member of fragTypeCharCloth)
//
//				characterClothController m_ClothControllers	(member array in characterCloth)
//
//					clothController							(base class for characterClothController)
//
//					phVerletCloth m_Cloth					(member of clothController)
//
//					clothBridgeSimGfx m_Bridge				(member of clothController)
//
//			fragInst* m_Ragdoll								(member of characterCloth)


namespace ragesamples {

using namespace rage;

class clothSampleManager : public physicsSampleManager
{
public:

	virtual void InitClient()
	{
		// Initialize the basic physics sample manager.
		physicsSampleManager::InitClient();

	#if __PFDRAW
		// Enable model drawing in the physics drawing system, so that the cloth draws.
		PFD_Models.SetEnabled(true);
	#endif

		m_MultithreadedCloth = false;
		m_PrevMultithreadedCloth = false;
		m_Ball = NULL;

		// Initialize rope rendering.
		RopeModel::InitRopeModel("sample_rope\\p_gen_rope02.dds");

#if WORLD_INDEX==0
		const bool zUp = false;
		m_Demos.AddDemo(CreateSheetWorld(zUp));
#elif WORLD_INDEX==1
		m_Demos.AddDemo(CreateClotheslineWorld());
#elif WORLD_INDEX==2
		m_Demos.AddDemo(CreateTentWorld());
#elif WORLD_INDEX==3
		m_Demos.AddDemo(CreatePotOnRopeWorld());
#elif WORLD_INDEX==4
		m_Demos.AddDemo(CreateWindowCurtainWorld());
#else
		const bool zUp = true;
		m_Demos.AddDemo(CreateSheetWorld(zUp));
#endif

		// Using the demo world factories causes a crash on exit.
	//	m_Demos.AddDemo(phDemoMultiWorld::Factory(this,&clothSampleManager::CreateSheetWorld));
	//	m_Demos.AddDemo(phDemoMultiWorld::Factory(this,&clothSampleManager::CreateClotheslineWorld));

		// Disable wireframe mesh drawing on the cloth.
		// Change this to true to see wireframe instead of the cloth model. It takes a long time to draw.
#if WORLD_INDEX==0 | WORLD_INDEX==5
		PFD_GROUP_ENABLE(DrawCloth,false);
#else
		PFD_GROUP_ENABLE(DrawCloth,true);
#endif

		// Disable bound drawing, because the bound that draws for the cloth is the whole collision sphere.
		// Set this to true to draw the spherical bound that determines when collisions with the cloth will be tested.
		// The mouse grab will register this as a hit, so if you want to grab the ball, you need make sure this cloth bound isn't in the way.
		PFD_GROUP_ENABLE(Physics,false);

		// Turn off solid physics bound drawing, so that when the cloth's sphere is enabled (above), you can still see the cloth inside it.
		PFD_ITEM_ENABLE(Solid,false);
		PFD_ITEM_ENABLE(Wireframe,true);

		// Map the reset key.
		m_Mapper.Map(IOMS_KEYBOARD,KEY_F4,m_Reset);

		// Set the only demo world and activate it.
		m_Demos.SetCurrentDemo(0);
		m_Demos.GetCurrentWorld()->Activate();

		// Set the wind and ball speeds.
		m_WindSpeed = 5.0f;
		PARAM_wind.Get(m_WindSpeed);

		BANK_ONLY(FRAGTUNE->RefreshTypeList());
	}

	phDemoWorld* CreateSheetWorld (bool zUp)
	{
		// Create and initialize a simple world.
		fragWorld* sheetWorld = rage_new fragWorld("Sheet");
		sheetWorld->Init(1000, 500, 500, Vec3V( -999.0f, -999.0f, -999.0f), Vec3V(+999.0f, +999.0f, +999.0f), 9);
		if (zUp)
		{
			SetZUp(true);
			const Vector3 gravity = Vector3(0.0f,0.0f,-9.8f);
			sheetWorld->SetGravity(gravity);
		}

		const bool triangulated = true;
		const u32 includeFlags = INCLUDE_FLAGS_ALL;
		sheetWorld->ConstructTerrainPlane(triangulated,includeFlags,zUp);

		// Make a sphere to throw against the cloth.
		const bool throwBall = true;
		Vector3 position(0.0f,3.75f,-0.75f);
		if (zUp)
		{
			position.Set(0.0f,-0.75,10.0f);
		}

		if (throwBall)
		{
			const bool alignBottom = false;
			m_Ball = sheetWorld->CreateObject("sphere_056",position,alignBottom);
		}
		else
		{
			m_Ball = NULL;
		}

		// Make the cloth as a fragment object, and insert it into the fragment world.
		Vector3 rotation(0.0f,0.25f*PI,0.0f);
		position.Set(0.0f,0.0f,0.0f);
		Matrix34 clothPose(CreateRotatedMatrix(position,rotation));
#if MESH_LIBRARY
		m_EnvClothType[0] = fragType::Load("$/fragments/env_sheet/env_sheet.type");
		Assert(m_EnvClothType[0]);
#endif
		m_EnvClothInst[0] = rage_new fragInst(m_EnvClothType[0],clothPose);
		m_EnvClothInst[0]->Insert(false);
		m_NumEnvCloth = 1;

		//	fragTypeEnvCloth* envCloth = m_EnvClothType[0]->GetTypeEnvCloth(0);
		//	environmentCloth& cloth = envCloth->m_Cloth;
		//	cloth.UnPinAllVerts();
		//	cloth.PinVertex(1056);
		//	cloth.PinVertex(1088);
		m_BallSpeed = 5.0f;
		if (throwBall)
		{
			// Throw the ball at the cloth.
			Vector3 ballVelocity(0.0f,0.0f,m_BallSpeed);
			if (zUp)
			{
				ballVelocity.Set(0.0f,m_BallSpeed,0.0f);
			}

			sheetWorld->GetSimulator()->GetCollider(m_Ball->GetPhysInst())->SetVelocity(ballVelocity);
		}

		return sheetWorld;
	}

	void AddFlag (Vec3V_In flagPosition)
	{
		int flagIndex = m_NumEnvCloth;
		m_EnvClothType[flagIndex] = fragType::Load("$/fragments/tra_rope_test01x_flag01x/entity.type");
		Assert(m_EnvClothType[flagIndex]);

		Mat34V clothPose = Mat34V(V_IDENTITY);
		clothPose.SetCol3(flagPosition);
		m_EnvClothInst[flagIndex] = rage_new fragInst(m_EnvClothType[flagIndex],clothPose);
		m_EnvClothType[flagIndex]->SetAttachBottomEnd();
		m_EnvClothInst[flagIndex]->Insert(false);
		m_NumEnvCloth++;
	}

	phDemoWorld* CreateClotheslineWorld ()
	{
		// Create and initialize a simple world.
		fragWorld* clotheslineWorld = rage_new fragWorld("Clothesline");
		clotheslineWorld->Init(1000, 500, 500, Vec3V( -999.0f, -999.0f, -999.0f), Vec3V(+999.0f, +999.0f, +999.0f), 9);
		clotheslineWorld->GetPhLevel()->SetOctreeNodeCreationParameters(10,9);
		clotheslineWorld->ConstructTerrainPlane();

		// Make the cloth as a fragment object, and insert it into the fragment world.
		Mat34V clothPose = Mat34V(V_IDENTITY);
		Vec3V ropePosition = Add(Scale(Vec3V(V_X_AXIS_WZERO),ScalarVFromF32(-3.0f)),Scale(Vec3V(V_Y_AXIS_WZERO),ScalarVFromF32(3.0f)));
		clothPose.SetCol3(ropePosition);
		m_EnvClothType[0] = fragType::Load("$/fragments/tra_rope_test01x_rope01x/entity.type");
		Assert(m_EnvClothType[0]);
		m_EnvClothType[0]->SetAttachBottomEnd();

		// Add a fixed object to the starting end of the rope.
		clotheslineWorld->CreateFixedObject("sphere_020",VEC3V_TO_VECTOR3(ropePosition));

		// Add a pendant to the end of the rope, to test fragment rope pendant attachment.
		fragTypeEnvCloth* typeEnvCloth = m_EnvClothType[0]->GetTypeEnvCloth(0);
		environmentCloth& envClothRope = typeEnvCloth->m_Cloth;
		Vec3V position = Add(ropePosition,Scale(Vec3V(V_X_AXIS_WZERO),envClothRope.GetCloth()->ComputeRopeLength()));
		clotheslineWorld->CreateObject("sphere_020",VEC3V_TO_VECTOR3(position));

		m_EnvClothInst[0] = rage_new fragInst(m_EnvClothType[0],clothPose);
		m_EnvClothInst[0]->Insert(false);
		m_NumEnvCloth = 1;

		Vec3V flagPosition = Add(Scale(Vec3V(V_X_AXIS_WZERO),ScalarVFromF32(-2.5f)),Scale(Vec3V(V_Y_AXIS_WZERO),ScalarVFromF32(3.05f)));
		AddFlag(flagPosition);
		flagPosition = Add(Scale(Vec3V(V_X_AXIS_WZERO),ScalarVFromF32(-1.0f)),Scale(Vec3V(V_Y_AXIS_WZERO),ScalarVFromF32(3.05f)));
		AddFlag(flagPosition);
		flagPosition = Add(Scale(Vec3V(V_X_AXIS_WZERO),ScalarVFromF32(0.5f)),Scale(Vec3V(V_Y_AXIS_WZERO),ScalarVFromF32(3.05f)));
		AddFlag(flagPosition);
		flagPosition = Add(Scale(Vec3V(V_X_AXIS_WZERO),ScalarVFromF32(2.0f)),Scale(Vec3V(V_Y_AXIS_WZERO),ScalarVFromF32(3.05f)));
		AddFlag(flagPosition);
		flagPosition = Add(Scale(Vec3V(V_X_AXIS_WZERO),ScalarVFromF32(4.0f)),Scale(Vec3V(V_Y_AXIS_WZERO),ScalarVFromF32(3.05f)));
		AddFlag(flagPosition);
		flagPosition = Add(Scale(Vec3V(V_X_AXIS_WZERO),ScalarVFromF32(5.5f)),Scale(Vec3V(V_Y_AXIS_WZERO),ScalarVFromF32(3.05f)));
		AddFlag(flagPosition);
		flagPosition = Add(Scale(Vec3V(V_X_AXIS_WZERO),ScalarVFromF32(7.0f)),Scale(Vec3V(V_Y_AXIS_WZERO),ScalarVFromF32(3.05f)));
		AddFlag(flagPosition);
		flagPosition = Add(Scale(Vec3V(V_X_AXIS_WZERO),ScalarVFromF32(8.5f)),Scale(Vec3V(V_Y_AXIS_WZERO),ScalarVFromF32(3.05f)));
		AddFlag(flagPosition);

		return clotheslineWorld;
	}

	phDemoWorld* CreateTentWorld ()
	{
		// Create and initialize a simple world.
		fragWorld* tentWorld = rage_new fragWorld("Tent");
		tentWorld->Init(1000, 500, 500, Vec3V( -999.0f, -999.0f, -999.0f), Vec3V(+999.0f, +999.0f, +999.0f), 9);
		tentWorld->ConstructTerrainPlane();

		// Make the cloth as a fragment object, and insert it into the fragment world.
		Matrix34 pose(M34_IDENTITY);
		m_EnvClothType[0] = fragType::Load("$/fragments/c_gen_tentCochinay01x/entity.type");
		Assert(m_EnvClothType[0]);
		m_EnvClothInst[0] = rage_new fragInst(m_EnvClothType[0],pose);
		m_EnvClothInst[0]->Insert(false);
		m_NumEnvCloth = 1;

		pose.d.y = 3.0f;
		tentWorld->CreateObject("sphere_056",pose.d);

		return tentWorld;
	}

	phDemoWorld* CreatePotOnRopeWorld ()
	{
		// Create and initialize a simple world.
		fragWorld* potOnRopeWorld = rage_new fragWorld("PotOnRope");
		potOnRopeWorld->Init(1000, 500, 500, Vec3V( -999.0f, -999.0f, -999.0f), Vec3V(+999.0f, +999.0f, +999.0f), 9);
		potOnRopeWorld->ConstructTerrainPlane();

		// Make the cloth as a fragment object, and insert it into the fragment world.
		Matrix34 pose(M34_IDENTITY);
	//	pose.RotateX(PI);
		pose.d.y = 2.87f;
		m_EnvClothType[0] = fragType::Load("$/fragments/c_gen_rope02x/entity.type");
		Assert(m_EnvClothType[0]);
		m_EnvClothInst[0] = rage_new fragInst(m_EnvClothType[0],pose);
		m_EnvClothInst[0]->Insert(false);
		m_NumEnvCloth = 1;

		pose.d.y = 1.16f;
		fragType* potType = fragType::Load("$/fragments/p_gen_potSm01a/entity.type");
		fragInst* potInst = rage_new fragInst(potType,pose);
		potInst->Insert(false);

		return potOnRopeWorld;
	}

	phDemoWorld* CreateWindowCurtainWorld ()
	{
		// Create and initialize a simple world.
		fragWorld* windowWorld = rage_new fragWorld("Window Curtains");
		windowWorld->Init(1000, 500, 500, Vec3V( -999.0f, -999.0f, -999.0f), Vec3V(+999.0f, +999.0f, +999.0f), 9);
		windowWorld->ConstructTerrainPlane();

		// Make the curtains as a fragment object.
		Matrix34 clothPose(M34_IDENTITY);
		clothPose.RotateLocalY(-0.25f*PI);
		clothPose.d.y = 2.0f;
		m_EnvClothType[0] = fragType::Load("$/fragments/c_gen_curtains02x/entity.type");
		Assert(m_EnvClothType[0]);

		// Make the curtain instance and put it in the world.
		m_EnvClothInst[0] = rage_new fragInst(m_EnvClothType[0],clothPose);
		m_EnvClothInst[0]->Insert(false);
		m_NumEnvCloth = 1;

		// Make a window frame.
		Matrix34 windowPose(M34_IDENTITY);
		windowPose.RotateLocalY(-0.25f*PI);
		windowPose.d.Set(0.0f,1.25f,0.0f);
		windowWorld->CreateFixedObject("windowframe",windowPose);

		return windowWorld;
	}

	bool PreUpdate (fragInst* envClothInst)
	{
		// Get the environment cloth from the fragment cache entry.
		environmentCloth* envCloth = envClothInst->GetCacheEntry()->GetHierInst()->envCloth;

		// Get the vertex normals from the drawable.
		// More resistance occurs at a vertex when it's vertex normal is aligned with the wind.
		atArray<Vec3V> vertexNormals;
		int numVertices = envCloth->GetCloth()->GetNumVertices();
		vertexNormals.Resize(numVertices);
		Vector3 windDisplacement(m_WindSpeed*TIME.GetSeconds(),0.0f,0.0f);
		
		if (envCloth->GetCloth()->IsRope())
		{
			Vector3 windDirection(windDisplacement);
			windDirection.NormalizeSafe(ORIGIN);
//			envCloth->GetCloth()->GetVertexNormalsFromRope((Vector3*)&(vertexNormals[0]),numVertices,windDirection);

			// Reduce the wind displacement for rope.
			windDisplacement.Scale(0.25f);
		}
		else
		{
//			envCloth->GetVertexNormalsFromDrawable((Vector3*)&(vertexNormals[0]));
		}

		// Apply air resistance to the cloth.
//		const int normalStride = 1;
//		const float dragCoef = 0.00167f*TIME.GetInvSeconds();
//		envCloth->GetCloth()->ApplyAirResistance(&(vertexNormals[0]),normalStride,dragCoef,windDisplacement);

		// Return true to indicate that the cloth should update.
		// A cloth-in-view check could also be done here, returning false to prevent it from updating when it's out of view.
		return true;
	}

	virtual void Update()
	{	
		if (m_Reset.IsPressed())
		{
			if (m_Ball)
			{
				// Throw the ball at the cloth.
				PHSIM->GetCollider(m_Ball->GetPhysInst())->SetVelocity(Vector3(0.0f,0.0f,m_BallSpeed));
			}
		}

		if (grcViewport::GetCurrent())
		{
			FRAGMGR->SetInterestFrustum(*grcViewport::GetCurrent(), GetCameraMatrix());
		}

		physicsSampleManager::Update();

		PreUpdate( m_EnvClothInst[0] );
		environmentCloth* envCloth =  m_EnvClothInst[0]->GetCacheEntry()->GetHierInst()->envCloth;
	#if 0//__PS3

		sysTaskHandle h = envCloth->UpdateSpu( TIME.GetSeconds() /*, windDisplacement*/);
		sysTaskManager::Wait( h );
		envCloth->UpdatePostSpu();

	#else
		envCloth->UpdateCloth(TIME.GetSeconds(), 1.0f);
	#endif

	#if __PFDRAW
//		DrawClothWireframe( envCloth->GetCloth()->GetClothData().GetVertexPointer(), *envCloth->GetCloth() );
	#endif
	}

	void ShutdownClient()
	{
		for (int clothPiece=0; clothPiece<m_NumEnvCloth; clothPiece++)
		{
			// Avoid an assert failure on exit.
			PHLEVEL->UpdateObjectLocationAndRadius(m_EnvClothInst[clothPiece]->GetLevelIndex(),(Mat34V_Ptr)(NULL));
		}

		FRAGMGR->Reset();

		for (int clothPiece=0; clothPiece<m_NumEnvCloth; clothPiece++)
		{
			m_EnvClothInst[clothPiece]->Remove();
			delete m_EnvClothInst[clothPiece];
			delete m_EnvClothType[clothPiece];
		}

		if (m_Ball)
		{
			m_Demos.GetCurrentWorld()->RemoveObject(m_Ball);
			delete m_Ball;
		}

		RopeModel::Instance().Destroy();

		physicsSampleManager::ShutdownClient();
	}

	void DrawClient()
	{
		physicsSampleManager::DrawClient();

		PreUpdate( m_EnvClothInst[0] );
		environmentCloth* envCloth =  m_EnvClothInst[0]->GetCacheEntry()->GetHierInst()->envCloth;

		for (int bucket = 0; bucket < 8; ++bucket)
		{
			envCloth->GetDrawable()->Draw(M34_IDENTITY, bucket, envCloth->GetDrawable()->GetLodGroup().GetLodIndex() );
		}
	}

	virtual void InitCamera ()
	{
		// Set the camera position and target for a good view of the cloth.
		Vector3 lookFrom(4.0f,8.0f,-6.0f);
		Vector3 lookTo(0.0f,2.0f,0.0f);
		InitSampleCamera(lookFrom,lookTo);
	}

#if __BANK
	virtual void AddWidgetsClient ()
	{
		bkBank& bank = BANKMGR.CreateBank("Sample Cloth");
		bank.AddToggle("Multithreaded Cloth",&m_MultithreadedCloth);
		bank.AddSlider("Wind Speed",&m_WindSpeed,-20.0f,20.0f,1.0f);

		physicsSampleManager::AddWidgetsClient();
	}
#endif


protected:
	fragType* m_EnvClothType[MAX_NUM_ENV_CLOTH];
	fragInst* m_EnvClothInst[MAX_NUM_ENV_CLOTH];
	int m_NumEnvCloth;

	ioValue m_Reset;
	phDemoObject* m_Ball;
	float m_WindSpeed,m_BallSpeed;
	bool m_MultithreadedCloth,m_PrevMultithreadedCloth;
};

} // namespace ragesamples


int Main()
{
	{
		ragesamples::clothSampleManager samplePhysics;
		samplePhysics.Init();

		samplePhysics.UpdateLoop();

		samplePhysics.Shutdown();
	}

	return 0;
}
