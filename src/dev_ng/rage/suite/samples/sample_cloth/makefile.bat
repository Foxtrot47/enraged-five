set TESTERS=sample_cloth sample_ponytail sample_basic_cloth sample_charactercloth sample_multithreadcloth
set LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS% %RAGE_PH_LIBS% %RAGE_SAMPLE_LIBS% %RAGE_AUD_LIBS% %RAGE_CR_LIBS% %RAGE_SUITE_CR_LIBS%
set LIBS=%LIBS% cranimation profile rmptfx
set LIBS=%LIBS% breakableglass phglass fragment cloth grrope spatialdata
set LIBS=%LIBS% event eventtypes 
set LIBS=%LIBS% sample_physics sample_fragment sample_cranimation sample_crfragment
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples %RAGE_DIR%\suite\src %RAGE_DIR%\suite\samples
set XDEFINE=USING_RAGE
