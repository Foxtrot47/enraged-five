// 
// /sample_charactercloth.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


//////////////////////////////////////////////////////////////////////////////
// Includes

#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "cloth/charactercloth.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "fragment/typecloth.h"
#include "grcore/device.h"
#include "grmodel/geometry.h"
#include "paging/streamer.h"
#include "phcore/config.h"
#include "phcore/phmath.h"
#include "phcore/surface.h"
#include "phcore/materialmgrimpl.h"
#include "pheffects/resourceversions.h"
#include "physics/simulator.h"
#include "physics/levelnew.h"
#include "sample_fragment/fragworld.h"
#include "sample_physics/sample_physics.h"
#include "sample_rmcore/sample_rmcore.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "system/xtl.h"
#include "vectormath/vec3v.h"

//////////////////////////////////////////////////////////////////////////////
// Defines

//////////////////////////////////////////////////////////////////////////////
// Parameters
PARAM(file,"[sample_charactercloth] unused");
PARAM(char,"[sample_charactercloth] type file name for character");
PARAM(ragdoll,"[sample_charactercloth] ragdoll type file name for character");
PARAM(anim,"[sample_charactercloth] animation file name for character");
PARAM(wind,"strength of the wind vector");

#define MAX_NUM_CHAR_CLOTH	16

namespace rage {
	EXT_PFD_DECLARE_GROUP(DrawCloth);
	EXT_PFD_DECLARE_GROUP(Physics);
	EXT_PFD_DECLARE_ITEM(Models);
	EXT_PFD_DECLARE_ITEM(Solid);
	EXT_PFD_DECLARE_ITEM(Wireframe);
#if __PFDRAW
	extern void DrawClothWireframe (const phClothData& clothData, const phVerletCloth& cloth, const Vector3* vertexNormals);
#endif
}

namespace ragesamples
{

typedef struct _ClothParams
{
	characterCloth* m_CharacterCloth;
	crAnimation*		m_Animation;
	crFrame			m_AnimFrame;
	crFrame			m_BlendFrame;
	crFrame			m_InitialFrame;
	float				m_AnimPhase;
	float				m_BlendPhase;

	_ClothParams()
	{
		m_CharacterCloth = NULL;
		m_Animation = 0;
		m_AnimPhase = 0.0f;
		m_BlendPhase = 0.0f;
	}
} ClothParams;


void UpdateCloth(ClothParams* pClothParams);
void InitAnimation(ClothParams* pClothParams);
void UpdateAnimation(ClothParams* pClothParams);

class sampleCharacterCloth : public physicsSampleManager
{
public:
	sampleCharacterCloth()
	{
//		m_LightMode = grclmPoint;
	}

	virtual ~sampleCharacterCloth()
	{
	}

	virtual void InitClient()
	{
		physicsSampleManager::InitClient();

	#if __PFDRAW
		// Enable model drawing in the physics drawing system, so that the cloth draws.
		PFD_Models.SetEnabled(true);

		// Disable bound drawing, because the bound that draws for the cloth is the whole collision sphere.
		// Set this to true to draw the spherical bound that determines when collisions with the cloth will be tested.
		// The mouse grab will register this as a hit, so if you want to grab the ball, you need make sure this cloth bound isn't in the way.
		PFD_GROUP_ENABLE(Physics,false);

		// Turn off solid physics bound drawing, so that when the cloth's sphere is enabled (above), you can still see the cloth inside it.
		PFD_ITEM_ENABLE(Solid,false);
		PFD_ITEM_ENABLE(Wireframe,true);
		PFD_GROUP_ENABLE(DrawCloth,true);
	#endif

		m_WindSpeed = 5.0f;
		PARAM_wind.Get(m_WindSpeed);

		m_Demos.AddDemo(CreateWorld());

		// Set the only demo world and activate it.
		m_Demos.SetCurrentDemo(0);
		m_Demos.GetCurrentWorld()->Activate();

		fragManagerBase::ClothInterface::ClothPreUpdateFunc preUpdateCallback(this,&sampleCharacterCloth::PreUpdate);
		fragManagerBase::ClothInterface::SetEnvironmentClothPreUpdateFunc(preUpdateCallback);
	}

	void ShutdownClient()
	{
		for (int clothPiece=0; clothPiece<m_NumCharCloth; clothPiece++)
		{
			// Avoid an assert failure on exit.
			PHLEVEL->UpdateObjectLocationAndRadius(m_CharClothInst[clothPiece]->GetLevelIndex(),(Mat34V_Ptr)(NULL));
		}

		FRAGMGR->Reset();

		for (int clothPiece=0; clothPiece<m_NumCharCloth; clothPiece++)
		{
			m_CharClothInst[clothPiece]->Remove();
			delete m_CharClothInst[clothPiece];
			delete m_CharClothType[clothPiece];
		}

		physicsSampleManager::ShutdownClient();
	}

	phDemoWorld* CreateWorld ()
	{
		// Create and initialize a simple world.
		fragWorld* sheetWorld = rage_new fragWorld("World");
		sheetWorld->Init(1000, 500, 500, Vec3V( -999.0f, -999.0f, -999.0f), Vec3V(+999.0f, +999.0f, +999.0f), 9);
		const bool triangulated = true;
		const u32 includeFlags = INCLUDE_FLAGS_ALL;
		sheetWorld->ConstructTerrainPlane(triangulated,includeFlags);

		// Make the cloth as a fragment object, and insert it into the fragment world.
		Vector3 position(0.0f,2.0f,0.0f);
		Vector3 rotation(0.0f,0.5f*PI,0.0f);
		Matrix34 clothPose(CreateRotatedMatrix(position,rotation));
		m_CharClothType[0] = fragType::Load("$/fragments/c_gen_curtains02x/entity.type");
		Assert(m_CharClothType[0]);
		if (m_CharClothType[0]->GetNumEnvCloths()>0)
		{
			m_CharClothType[0]->GetTypeEnvCloth(0)->m_Cloth.GetClothController()->SetIsUsingSoftPinning(true);
		}

		m_CharClothInst[0] = rage_new fragInst(m_CharClothType[0],clothPose);
		m_CharClothInst[0]->Insert(false);
		m_NumCharCloth = 1;

		return sheetWorld;

/*		// Setup character strings from command line parameters
		const char* character = "$/fragments/player_mexicanRebelNEW/entity.type";
		const char* ragdoll = "$/fragments/player_mexicanRebelNEW/entity.type";
		const char* anim = "$/fragments/player_mexicanRebelNEW/Gent_nor_hnd_for_wlk.anim";
		PARAM_char.Get(character);
		PARAM_ragdoll.Get(ragdoll);
		PARAM_anim.Get(anim);

		// create character
		CreateCharacter(character, ragdoll, Vector3(0.0f, 0.0f, 0.0f));

		// Set the only demo world and activate it.
		m_Demos.SetCurrentDemo(0);
		m_Demos.GetCurrentWorld()->Activate();

		// Load the animations
		m_ClothParams.m_Animation = crAnimation::AllocateAndLoad(anim);
		InitAnimation(&m_ClothParams);*/
	}

	bool PreUpdate (fragInst* envClothInst)
	{
		// Get the environment cloth from the fragment cache entry.
		environmentCloth* envCloth = envClothInst->GetCacheEntry()->GetHierInst()->envCloths[0];

		// Get the vertex normals from the drawable.
		// More resistance occurs at a vertex when it's vertex normal is aligned with the wind.
		atArray<Vector3> vertexNormals;
		int numVertices = envCloth->GetCloth()->GetClothData().GetNumVertices();
		vertexNormals.Resize(numVertices);
		Vector3 windDisplacement(m_WindSpeed*TIME.GetSeconds(),0.0f,0.0f);
		if (envCloth->GetCloth()->IsRope())
		{
			Vector3 windDirection(windDisplacement);
			windDirection.NormalizeSafe(ORIGIN);
			envCloth->GetCloth()->GetVertexNormalsFromRope(&(vertexNormals[0]),numVertices,windDirection);

			// Reduce the wind displacement for rope.
			windDisplacement.Scale(0.25f);
		}
		else
		{
			envCloth->GetVertexNormalsFromDrawable(&(vertexNormals[0]));
		}

		// Apply air resistance to the cloth.
		const int normalStride = 1;
		const float dragCoef = 0.00167f*TIME.GetInvSeconds();
		envCloth->GetCloth()->ApplyAirResistance((Vec3V*)&vertexNormals[0],normalStride,dragCoef,windDisplacement);

		// Return true to indicate that the cloth should update.
		// A cloth-in-view check could also be done here, returning false to prevent it from updating when it's out of view.
		return true;
	}

	virtual void Update()
	{
		if (grcViewport::GetCurrent())
		{
			FRAGMGR->SetInterestFrustum(*grcViewport::GetCurrent(), GetCameraMatrix());
		}

		physicsSampleManager::Update();
	}

	void CreateCharacter(const char* szCharacter, const char* szRagdoll, const Vector3& pos)
	{

		// Create the character cloth object
		m_ClothParams.m_CharacterCloth = rage_new characterCloth();

		// Load the ragdoll and the character entity
		m_ClothParams.m_CharacterCloth->Load(szCharacter, szRagdoll);

		// Set the initial position
		Matrix34 mtx;
		mtx.Identity();
		mtx.Translate(pos);
		m_ClothParams.m_CharacterCloth->Teleport(mtx);
	}

	void InitPhysics()
	{
		phConfig::EnableRefCounting(true);

		// Initialize physics material manager
		ASSET.PushFolder(RAGE_ASSET_ROOT "physics\\materials");
		phMaterialMgrImpl<phSurface>::Create();
		MATERIALMGR.Load(64);
		ASSET.PopFolder();

		// Initialize fragmanager
		rage_new fragManager();

		// Create physics level
		phLevelNew* pLevel = rage_new phLevelNew();
		phLevelNew::SetActiveInstance(pLevel);

		const int maxOctreeNodes    = 1000;
		const int maxActiveObjects  = 5000;
		const int maxObjects        = 5000;
		const float size			= 1000;	 
		const Vec3V minWorld(-size,-size,-size);
		const Vec3V maxWorld( size, size, size);

		pLevel->SetExtents(minWorld, maxWorld);
		pLevel->SetMaxObjects(maxObjects);
		pLevel->SetMaxActive(maxActiveObjects);
		pLevel->SetNumOctreeNodes(maxOctreeNodes);
		pLevel->Init();

		// Create physics simulator
		phSimulator* pSimulator = rage_new rage::phSimulator;
		phSimulator::SetActiveInstance(pSimulator);
		pSimulator = rage::phSimulator::GetActiveInstance();
		phSimulator::InitParams params;
		params.maxManagedColliders = 512;
		pSimulator->Init(pLevel, params);
	}

#if __BANK
	virtual void AddWidgetsClient ()
	{
		bkBank& bank = BANKMGR.CreateBank("Sample Character Cloth");
		bank.AddSlider("Wind Speed",&m_WindSpeed,-20.0f,20.0f,1.0f);
		physicsSampleManager::AddWidgetsClient();
	}
#endif


	// Accessors

protected:
	fragType* m_CharClothType[MAX_NUM_CHAR_CLOTH];
	fragInst* m_CharClothInst[MAX_NUM_CHAR_CLOTH];
	int m_NumCharCloth;

	float m_WindSpeed;
	ClothParams				m_ClothParams;
//	grcSampleLightPreset	m_lightSet;

};


void UpdateCloth(ClothParams* pParams)
{
	// Use a constant 60hz time step
	float fTime = 0.0166666667f;

	// Only process this character if its valid
	if( pParams->m_CharacterCloth )
	{
		// Update Animation
		if (pParams->m_Animation)
		{
			UpdateAnimation(pParams);
		}

		// Update character cloth
		pParams->m_CharacterCloth->Update(fTime);

	#if __PFDRAW
		// Draw the cloth in wire frame (this is normally turned off because it's slow).
		characterClothController* controller = pParams->m_CharacterCloth->GetClothController(0);
		Assert(controller);
		const int lodIndex = controller->GetLOD();
		const phVerletCloth* verletCloth = controller->GetCloth(lodIndex);
		Assert(verletCloth);
		DrawClothWireframe(verletCloth->GetClothData(),*verletCloth,lodIndex);
	#endif

	}
}

void InitAnimation(ClothParams* pClothParams)
{
	if( pClothParams->m_CharacterCloth )
	{
		// Initialize the animation frames with the current skeleton
		crSkeleton* pSkeleton = pClothParams->m_CharacterCloth->GetSkeleton();
		const crSkeletonData& skeletonData = pSkeleton->GetSkeletonData();

		pClothParams->m_AnimFrame.InitCreateBoneAndMoverDofs(skeletonData, false);
		pClothParams->m_BlendFrame.InitCreateBoneAndMoverDofs(skeletonData, false);
		pClothParams->m_InitialFrame.InitCreateBoneAndMoverDofs(skeletonData, false);

		pClothParams->m_AnimFrame.InversePose(*pSkeleton);		
		pClothParams->m_BlendFrame.InversePose(*pSkeleton);		
		pClothParams->m_InitialFrame.InversePose(*pSkeleton);
	}
}

void UpdateAnimation(ClothParams* pClothParams)
{
	if( pClothParams->m_CharacterCloth )
	{
		// Build the animation frame
		if( pClothParams->m_BlendPhase >= 1.0f )
		{
			pClothParams->m_Animation->CompositeFrame(pClothParams->m_AnimPhase, pClothParams->m_BlendFrame);
		}
		else
		{
			pClothParams->m_Animation->CompositeFrame(pClothParams->m_AnimPhase, pClothParams->m_AnimFrame);
			pClothParams->m_BlendFrame.Set(pClothParams->m_InitialFrame);
			pClothParams->m_BlendFrame.Blend(pClothParams->m_BlendPhase, pClothParams->m_AnimFrame);

			float blendTime = 0.2f;
			pClothParams->m_BlendPhase += TIME.GetSeconds() / blendTime;

			if (pClothParams->m_BlendPhase > 1.0f)
			{
				pClothParams->m_BlendPhase = 1.0f;
			}
		}

		// Don't apply the animation frame
//		pClothParams->m_CharacterCloth->ApplyAnimationFrame(pClothParams->m_BlendFrame);

		// Increment time
		pClothParams->m_AnimPhase += TIME.GetSeconds() * 1.0f;
		if( pClothParams->m_AnimPhase > pClothParams->m_Animation->GetDuration() )
		{
			pClothParams->m_AnimPhase -= pClothParams->m_Animation->GetDuration();
		}
	}
}


}	// namespace ragesamples

// main application
int Main ()
{
	ragesamples::sampleCharacterCloth sampleCloth;
	sampleCloth.Init("sample_charactercloth");
	sampleCloth.UpdateLoop();
	sampleCloth.Shutdown();
	return 0;
}
