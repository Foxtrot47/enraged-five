// 
// /sample_basic_cloth.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: sample_basic_cloth
// PURPOSE:
//		This sample shows how to add cloth to the environment without using the fragment system.


#include "bank/bkmgr.h"
#include "cloth/environmentcloth.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"
#include "grmodel/geometry.h"
#include "input/keys.h"
#include "input/pad.h"
#include "phbound/boundsphere.h"
#include "phBound/support.h"
#include "pheffects/cloth_verlet.h"
#include "pheffects/clothverletinst.h"
#include "physics/collider.h"
#include "physics/simulator.h"
#include "rmcore/drawable.h"
#include "sample_physics/sample_physics.h"
#include "sample_physics/demoobject.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vectormath/vec3v.h"

#include "mesh/serialize.h"
#include "cloth/clothbridgesimgfx.h"
#include "cloth/clothcontroller.h"


#if __BANK && !__RESOURCECOMPILER
	#include "input/keyboard.h"
	#include "input/keys.h"
	#include "input/mouse.h"
#endif


#include <system/xtl.h>

PARAM(file, "Reserved for future expansion, not currently hooked up");
PARAM(wind, "strength of the wind vector");



#define		USE_OTHER_CLOTH			0
#define		SOFT_BODY				0
#define		USE_RAG_TO_SWITCHLOD	(__BANK && !__RESOURCECOMPILER)
#define		MULTITHREADED			__PS3


#if SOFT_BODY

#include "gizmo/manager.h"
#include "gizmo/rotation.h"
#include "gizmo/translation.h"

#endif	// SOFT_BODY


#if !__XENON
namespace rage {
	char *XEX_TITLE_ID;
}
#endif

namespace rage {
EXT_PFD_DECLARE_GROUP(DrawCloth);
EXT_PFD_DECLARE_GROUP(Physics);
EXT_PFD_DECLARE_ITEM(Models);
EXT_PFD_DECLARE_ITEM(Solid);
EXT_PFD_DECLARE_ITEM(Wireframe);
#if __PFDRAW
extern void DrawClothWireframe (const phClothData& clothData, const phVerletCloth& cloth, int lodIndex, const Vector3* vertexNormals);
#endif
}

namespace ragesamples {

using namespace rage;


#if SOFT_BODY

PFD_DECLARE_GROUP_ON(DrawSoftBody);
PFD_DECLARE_ITEM_ON( SoftBody, Color_white, DrawSoftBody);

class phSoftBody : public datBase
{	

	float xOriginal, zOriginal;
	float yCoord;


	float gizmoActiveTimer;
	gzGizmo*	pGizmo;
	Matrix34	gizmoMtx;

public:
	int NumVerts[2];
	Vector3* vStreams[2];

	phSoftBody()
	{
		gizmoMtx.Identity();
		pGizmo = rage_new gzTranslation( gizmoMtx );
		pGizmo->Deactivate();
		gizmoActiveTimer = 0.0f;
	}

	~phSoftBody()
	{
	}

	phMorphController*	m_MorphController;
	phDemoMultiWorld* m_Demos;

	void Draw( int numVerts, Vector3* vStream, Color32 color )
	{
		float radius = 0.01f;
		for( int i = 0; i < numVerts; ++i )
		{
			PF_DRAW_SPHERE_COLOR(SoftBody, radius, vStream[i], color);
		}
	}

	phBoundGeometry* GenerateMeshBound( mshMesh* mesh, /*int lodIndex,*/ const Matrix34& mat )
	{
		Assert( mesh );

		mshMaterial* mtl = &mesh->GetMtl(0);
		Assert( mtl );

		int nVerts = mtl->GetVertexCount();
		int nMaterials = 1;
		int generatedPolyCount = mtl->GetTriangleCount();

		sysMemStartTemp();
		phBoundGeometry* meshBound = rage_new phBoundGeometry();
		meshBound->Init(Fourtify(nVerts), nMaterials, generatedPolyCount);

		int i;

		sysMemEndTemp();

#if COMPRESSED_VERTEX_METHOD > 0 && !SUPPORT_UNCOMPRESSED_WITH_COMPRESSED
		Vector3 aabbMin(VEC3_MAX), aabbMax(-VEC3_MAX);
		for (i=0; i<nVerts; ++i)
		{
			Vector3 v( mtl->GetPos(i) );

// NEW: this is the option to transform vertices back to whatever space 
			mat.Transform( v );

			aabbMin.Min(aabbMin, v);
			aabbMax.Max(aabbMax, v);
		}

		aabbMin.Subtract(meshBound->GetMarginV());
		aabbMax.Add(meshBound->GetMarginV());
		meshBound->InitQuantization(aabbMin, aabbMax);
#endif

		for (i=0; i<nVerts; ++i)
		{
			Vector3 v( mtl->GetPos(i) );
			mat.Transform( v );
			meshBound->SetVertex(i, v );
		}
//		sysMemStartTemp();

		Vector3 centerVector;
		centerVector.Average(meshBound->GetBoundingBoxMin(), meshBound->GetBoundingBoxMax());
		int fourtifiedNumVerts = Fourtify(nVerts);
		for (i=nVerts; i<fourtifiedNumVerts; ++i)
		{
			meshBound->SetVertex(i, centerVector);
		}

		meshBound->SetMaterialId(0, 0);

//		sysMemEndTemp();

		phPolygon poly;
		int physPolyCount = 0;
		for(mshMaterial::TriangleIterator ti = mtl->BeginTriangles(); ti != mtl->EndTriangles(); ++ti) 
		{
			int indices[3];
			ti.GetVertIndices(indices[0], indices[1], indices[2]);

			phPolygon::Index i0 = static_cast<phPolygon::Index>( indices[0] );
			phPolygon::Index i1 = static_cast<phPolygon::Index>( indices[1] );
			phPolygon::Index i2 = static_cast<phPolygon::Index>( indices[2] );
			
			poly.InitTriangle(i0, i1, i2, meshBound->GetVertex(i0), meshBound->GetVertex(i1), meshBound->GetVertex(i2));
			meshBound->SetPolygon(physPolyCount++, poly);
		}

		meshBound->PostLoadCompute();				// do post-processing on bound

		Vector3 newCGOffset;
		meshBound->CalcCGOffset(newCGOffset);
		meshBound->SetCGOffset(newCGOffset);

#if !__FINAL// && !__CONSOLE
		meshBound->WeldVertices(1e-3f);
		meshBound->ComputeNeighbors("simcloth");
#endif

		return meshBound;
	}



	bool LoadSBMesh( fiTokenizer &t, const char *match, int lod, const Matrix34& mat )
	{
		bool result = false;

		char buf[128];
		t.MatchToken(match); 
		if (!t.CheckToken("none")) 
		{
			t.GetToken(buf, sizeof(buf));		// support old format i guess
			t.GetToken(buf, sizeof(buf));

			sysMemStartTemp();
			mshMesh* mesh = rage_new mshMesh;
			SerializeFromFile(buf, *mesh, "mesh");
			sysMemEndTemp();

// TODO: non-sense code to please the compiler
//			int nModel = 
				t.GetInt();

			phBoundGeometry* bound = GenerateMeshBound( mesh, /*lod,*/ mat );		

			NumVerts[lod] = bound->GetNumVertices();
			vStreams[lod] = rage_new  Vector3[ NumVerts[lod] ];
			Vector3* vertices = vStreams[lod];
			for(int i = 0; i < NumVerts[lod]; ++i)
			{
				vertices[i].Set(bound->GetVertex(i));
			}

			
			Assert( m_MorphController );			
			int indexCount =  bound->GetNumPolygons() * 3;
			m_MorphController->CreateIndexMap( lod, lod, indexCount );
			m_MorphController->MapData[lod]->m_PolyCount = bound->GetNumPolygons();
			u16* clothIndex = (u16*)m_MorphController->MapData[lod]->m_IMap[lod];
			for(int i = 0; i < bound->GetNumPolygons(); ++i)
			{
				const phPolygon& p = bound->GetPolygon(i);
				*clothIndex++ = p.GetVertexIndex(0);
				*clothIndex++ = p.GetVertexIndex(1);
				*clothIndex++ = p.GetVertexIndex(2);
			}



			sysMemStartTemp();

			Assert( bound );
			Assert( mesh );
			delete bound;
			delete mesh;

			sysMemEndTemp();

			// TODO: clean it up, what was this ?!
			float threshold = t.GetFloat();
			threshold *= 1.0f;

			result = true;
		}

		return result;
	}


	void LoadSB( rmcTypeFileCbData* data )
	{
		fiTokenizer* tokenizer = data->m_T;
		tokenizer->CheckToken("{");
		tokenizer->CheckToken("mesh");

		if( tokenizer->CheckToken("{") )
		{
			//m_ClothController->CreateBridge();		?!?!?

			Matrix34 m;
			m.Identity();

			m_MorphController =  rage_new phMorphController;
			LoadSBMesh( *tokenizer, "high",	LOD_HIGH, m );

			m.d.Set( 0.0f, -0.1f, 0.0f );
			LoadSBMesh( *tokenizer, "med",	LOD_MED, m );


			const int index0 = 0;
			const int index1 = 1;			

			m_MorphController->CreateDeformMapping(	NumVerts[0]
													, (Vec3V*) vStreams[0]
													, (Vec3V*) vStreams[1]
													, index0, index1, phMapData::enWEIGHT_ONE );


			sysMemStartTemp();
			if( m_MorphController->MapData[0] )
			{
				m_MorphController->MapData[0]->m_IMapCount[0] = 0;
				m_MorphController->MapData[0]->DeleteIMap( 0 );				
			}

			if( m_MorphController->MapData[1] )
			{
				m_MorphController->MapData[1]->m_IMapCount[1] = 0;
				m_MorphController->MapData[1]->DeleteIMap( 1 );				
			}
			sysMemEndTemp();		

		}

// TEMP TEMP

		xOriginal = vStreams[1][0].x;
		zOriginal = vStreams[1][0].z;

		yCoord = vStreams[1][0].y;
		
	}


	void Update()
	{

		static float elapsed = 0.0f;
		elapsed += TIME.GetSeconds();
		static float amplitude = 0.1f;
		static float speed = 12.0f;
		float delta = amplitude * FPSin( elapsed * speed );

//		vStreams[1][0].SetX( xOriginal + delta );
//		vStreams[1][0].SetZ( zOriginal + delta );

		vStreams[1][0].SetY( yCoord - delta );
		vStreams[1][1].SetY( yCoord - delta );
		vStreams[1][2].SetY( yCoord - delta );
		vStreams[1][3].SetY( yCoord - delta );

#if __BANK && !__RESOURCECOMPILER

		static int vertexIndex = -1;
		static int lastVertexIndex = -1;
		Assert( m_Demos );
		const phMouseInput::Spring& grabSpring = m_Demos->GetCurrentWorld()->GetGrabSpring();
		if( !grabSpring.inst )
		{
			if ( grcViewport::GetCurrent() )
			{				
				Vector3 mouseScreen, mouseFar;

				grcWorldIdentity();
				grcViewport::GetCurrent()->ReverseTransform( static_cast<float>(ioMouse::GetX()), static_cast<float>(ioMouse::GetY()), mouseScreen, mouseFar );

				vertexIndex = PickVertex( mouseScreen, mouseFar );
			}
		}


		if( vertexIndex > -1 )
		{	
			lastVertexIndex = vertexIndex;
/*	// disabled - use only the gizmo 
			if( grabSpring.inst )
			{
				
				Vector3 v = GetVertexPosition( vertexIndex );

				Vector3 attachementPointVec = grabSpring.attachmentPoint;
				attachementPointVec.Subtract( v );

				static float maxLenClamp = phVerletCloth::PFD_GetLengthClamp();
				const float len = Clamp( attachementPointVec.Mag(), 0.0f, maxLenClamp );
				attachementPointVec.NormalizeSafe();

				const float delta = len * TIME.GetSeconds();
				v.AddScaled( attachementPointVec, delta );
				SetVertexPosition( vertexIndex, v );
				
			}
*/
		}
		else
		{

			if( pGizmo->IsActive() && !(ioMouse::GetButtons() & ioMouse::MOUSE_LEFT) )
			{
				gizmoActiveTimer -= TIME.GetSeconds();
			}

			if( gizmoActiveTimer < 0.0f )
			{
				gizmoActiveTimer = 0.0f;
				pGizmo->Deactivate();
			}

			if( pGizmo->IsActive() )
			{
				SetVertexPosition( lastVertexIndex, gizmoMtx.GetVector(3) );
			}
			else
			{
				lastVertexIndex = -1;
			}
		}
#endif // __BANK && !__RESOURCECOMPILER

	}

	Vector3& GetVertexPosition( int vertexIndex )
	{
		// TODO: set the correct vertex stream
		return vStreams[1][vertexIndex];
	}

	void SetVertexPosition( int vertexIndex, Vector3& pos )
	{
		// TODO: set the correct vertex stream
		vStreams[1][vertexIndex] = pos;
	}


	virtual void Shutdown()
	{
		delete pGizmo;
		pGizmo = NULL;
	}

#if __BANK && !__RESOURCECOMPILER
	int PickVertex( Vector3& mouseScreen, Vector3& mouseFar )
	{

		// TODO: work in progress
		const float closetPtRadius = 0.05f;
		const float closetPtRadiusSQR = closetPtRadius * closetPtRadius;
		Color32 colorSnap = Color_green;

		for(int i = 0; i < NumVerts[1]; ++i )
		{		
			Vector3 vtx = vStreams[1][i];
			Vector3 closestVertex = geomPoints::FindClosestPointSegToPoint( mouseScreen, mouseFar, vtx );
			vtx.Subtract( closestVertex );

			const float closestDistSQR = vtx.Mag2();
			if( closestDistSQR < closetPtRadiusSQR )
			{
//				PF_DRAW_SPHERE_COLOR( SoftBody, 0.05f, vStreams[1][i], colorSnap );
				pGizmo->Activate();
				gizmoMtx.Identity();
				gizmoMtx.Translate( vStreams[1][i] );
				gizmoActiveTimer = 3.0f;
				return i;
			}
		}

		return -1;
	}
#endif

};

phSoftBody*  m_SoftBody;



#endif // SOFT_BODY

#define		MAX_TEMP_INDICES	32

class clothSampleManager : public physicsSampleManager
{
public:

	virtual void InitClient()
	{
		// Initialize the basic physics sample manager.
		physicsSampleManager::InitClient();

	#if __PFDRAW
		// Enable model drawing in the physics drawing system, so that the cloth draws.
		PFD_Models.SetEnabled(true);
	#endif

		m_Demos.AddDemo(CreateSheetWorld());

		// Disable bound drawing, because the bound that draws for the cloth is the whole collision sphere.
		// Set this to true to draw the spherical bound that determines when collisions with the cloth will be tested.
		// The mouse grab will register this as a hit, so if you want to grab the ball, you need make sure this cloth bound isn't in the way.
		PFD_GROUP_ENABLE(Physics,false);

		// Turn off solid physics bound drawing, so that when the cloth's sphere is enabled (above), you can still see the cloth inside it.
		PFD_ITEM_ENABLE(Solid,false);
		PFD_ITEM_ENABLE(Wireframe,true);

		// Set the only demo world and activate it.
		m_Demos.SetCurrentDemo(0);
		m_Demos.GetCurrentWorld()->Activate();

		// Set the wind and ball speeds.
		m_WindSpeed = 5.0f;
		PARAM_wind.Get(m_WindSpeed);

#if __BANK && !__RESOURCECOMPILER
		environmentCloth::InitWidgets();
#endif
	}
	
	void UpdateClient()			
	{ 

#if !SOFT_BODY
		const int lodIndexDrawable = m_EnvCloth->GetDrawable()->GetLodGroup().GetLodIndex();
		phVerletCloth* verletCloth = m_EnvCloth->GetCloth( lodIndexDrawable );

		static float elapsed = 0.0f;
		elapsed += TIME.GetSeconds();
		Vector3 windDisplacement(0.0f, 0.0f, m_WindSpeed * TIME.GetSeconds(), 0.0f );
				
		int numVertices = verletCloth->GetClothData().GetNumVertices();
		atArray<Vector3> vertexNormals;
		vertexNormals.Resize(numVertices);

		// Get the vertex normals from the drawable.			
		m_EnvCloth->GetVertexNormalsFromDrawable( &(vertexNormals[0]) );

		// More resistance occurs at a vertex when it's vertex normal is aligned with the wind.
		// Apply air resistance to the cloth.

// NOTE: make sure the air resistance is applied to the correct simulation mesh !

		const int normalStride = 1;
		const float dragCoef = 0.00167f*TIME.GetInvSeconds();
		m_EnvCloth->GetCloth()->ApplyAirResistance( (Vec3V*)&(vertexNormals[0]),normalStride,dragCoef,windDisplacement);

		if( grcViewport::GetCurrent() )
		{
			const Vec3V camPos = *(Vec3V*)&(grcViewport::GetCurrent()->GetCameraMtx().GetVector(3));
#if !USE_RAG_TO_SWITCHLOD
			m_EnvCloth->CheckDistance( camPos );
#endif
		}		

#if __BANK && !__RESOURCECOMPILER
		phVerletCloth* verletClothSim = m_EnvCloth->GetCloth();
		Assert( verletClothSim );

		static int vertexIndex = -1;
		static int vertIndices[MAX_TEMP_INDICES];			
		static int indexCount = 0;
		const bool inPinMode = ioKeyboard::KeyDown(KEY_CONTROL) ? true: false;

		const phMouseInput::Spring& grabSpring = m_Demos.GetCurrentWorld()->GetGrabSpring();
		if( !grabSpring.inst )
		{
			for( int i = 0; i < MAX_TEMP_INDICES; ++i)
				vertIndices[i] = -1;
			indexCount = 0;

			if ( grcViewport::GetCurrent() )
			{				
				Vector3 mouseScreen, mouseFar;

				grcWorldIdentity();
				grcViewport::GetCurrent()->ReverseTransform( static_cast<float>(ioMouse::GetX()), static_cast<float>(ioMouse::GetY()), mouseScreen, mouseFar );
	
				vertexIndex = verletClothSim->PickVertices( vertIndices, indexCount, MAX_TEMP_INDICES, mouseScreen, mouseFar, inPinMode );
			}
		}

		if( vertexIndex > -1 )
		{		
			if( inPinMode )
			{
/*	
// TODO: analyze connectivity and edge code for dynamic pinning and unpinning - Svetli
// test case 1 - the whole connectivity gets broken when unpinning vertices dynamically
// work in progress
				if( ioKeyboard::KeyPressed(KEY_SPACE) )
				{				
					// unpin vertex
					sysMemStartTemp();
					atArray< int > unpinList;
					unpinList.Resize( 1 );
					sysMemEndTemp();

					unpinList[ 0 ] = vertexIndex;

					atFunctor3< void, int, int, int > funcy;
					funcy.Reset< clothController, &clothController::OnClothVertexSwap >( m_EnvCloth->GetClothController() );

					verletClothSim->UnPinVerts( m_EnvCloth->GetLOD(), unpinList, 0, &funcy );
					//verletClothSim->PinVerts( m_EnvCloth->GetLOD(), unpinList, 0, &funcy );
				}
*/
			}
			else if( grabSpring.inst )
			{
				phClothData& clothData = verletClothSim->GetClothData();
				Vector3 v = clothData.GetVertexPosition( vertexIndex );

				Vector3 attachementPointVec = grabSpring.attachmentPoint;
				attachementPointVec.Subtract( v );

				static float maxLenClamp = phVerletCloth::PFD_GetLengthClamp();
				const float len = Clamp( attachementPointVec.Mag(), 0.0f, maxLenClamp );
				attachementPointVec.NormalizeSafe();

				const float delta = len * TIME.GetSeconds();
				v.AddScaled( attachementPointVec, delta );
				clothData.SetVertexPosition( vertexIndex, v );

				for( int i = 0; i < indexCount; ++i )
				{
					v = clothData.GetVertexPosition( vertIndices[i] );
					v.AddScaled( attachementPointVec, delta );
					clothData.SetVertexPosition( vertIndices[i], v );
				}
			}
		}
#endif // __BANK && !__RESOURCECOMPILER


#if MULTITHREADED

		sysTaskHandle h = m_EnvCloth->UpdateSpu( TIME.GetSeconds() /*, windDisplacement*/);
		sysTaskManager::Wait( h );
		m_EnvCloth->UpdatePostSpu();
		
#else
		m_EnvCloth->Update(TIME.GetSeconds());
#endif

#if __PFDRAW
		DrawClothWireframe( m_EnvCloth->GetCloth()->GetClothData(), *m_EnvCloth->GetCloth(), 0);
#endif

#if !MULTITHREADED
		physicsSampleManager::UpdateClient();
#endif

#else

		static int wWeightLevel = 1;
		static int wWeightLevelSet = 1;

		m_SoftBody->m_MorphController->Morph( 0, 1, (Vec3V*)m_SoftBody->vStreams[0], (Vec3V*)m_SoftBody->vStreams[1], (phMapData::enWEIGHT_LEVEL)wWeightLevel, (phMapData::enWEIGHT_LEVEL) wWeightLevelSet );

		m_SoftBody->Update();
#endif

	}

	void DrawClient()
	{
		physicsSampleManager::DrawClient();
#if !SOFT_BODY
		for (int bucket = 0; bucket < 8; ++bucket)
		{

#if IMMEDIATE_DRAW
			static bool bla = false;
			if(bla)
			{
				m_EnvCloth->DrawImmediate( M34_IDENTITY );
			}
			else
#endif
				m_EnvCloth->GetDrawable()->Draw(M34_IDENTITY, bucket, m_EnvCloth->GetDrawable()->GetLodGroup().GetLodIndex() );
		}
#endif
#if SOFT_BODY

		m_SoftBody->Draw( m_SoftBody->NumVerts[0], m_SoftBody->vStreams[0], Color_red3 );
		m_SoftBody->Draw( m_SoftBody->NumVerts[1], m_SoftBody->vStreams[1], Color_white );

#endif

	}


	phDemoWorld* CreateSheetWorld()
	{
		// Create and initialize a simple world.
		phDemoWorld* demoWld = CreateNewDemoWorld("Sheet");

#if USE_OTHER_CLOTH
		const char* typeFile = "$/fragments/cloth/temp/entity.type";
#else
		const char* typeFile = "$/fragments/env_sheet/env_sheet.type";
#endif
		fiSafeStream     typeStream = ASSET.Open(typeFile, "type", true, true);
		fiAsciiTokenizer typeTok;
		typeTok.Init(typeFile, typeStream);

		rmcDrawable* drawable = rage_new rmcDrawable;

#if USE_OTHER_CLOTH
		ASSET.PushFolder("$/fragments/cloth/temp");
#else
		ASSET.PushFolder("$/fragments/env_sheet");
#endif

		rmcTypeFileParser parser;
		parser.RegisterLoader( "shadinggroup", "shadinggroup", datCallback(MFA1(rmcDrawable::LoadShader), drawable, 0, true) );
		parser.RegisterLoader( "lodgroup", "all", datCallback(MFA1(rmcDrawable::LoadMesh), drawable, 0, true) );
		parser.RegisterLoader( "edge", "all", datCallback(MFA1(rmcDrawable::LoadEdgeModel), drawable, 0, true) );

		// Load the drawable.
		parser.ProcessTypeFile(static_cast<fiTokenizer&>(typeTok), true);

		m_EnvCloth = rage_new environmentCloth;
		m_EnvCloth->SetReferencedDrawable(drawable);

		typeTok.Reset();
		parser.Reset();
		parser.RegisterLoader( "cloth", "all", datCallback(MFA1(environmentCloth::LoadCloth), m_EnvCloth, 0, true) );
		// Load the cloth object.
		parser.ProcessTypeFile(static_cast<fiTokenizer&>(typeTok), true);

		ASSET.PopFolder();

		m_EnvCloth->Init();

		demoWld->GetSimulator()->AddInactiveObject(const_cast<phInst*>(m_EnvCloth->GetInstance()));
		demoWld->GetSimulator()->AddInstBehavior(*m_EnvCloth->GetBehavior());


#if SOFT_BODY

		m_SoftBody = rage_new phSoftBody;

		m_SoftBody->m_Demos = &m_Demos;

		const char* sbTypeFile = "$/fragments/cloth/temp/softbody.type";
		fiSafeStream     typeStreamSB = ASSET.Open(sbTypeFile, "type", true, true);
		fiAsciiTokenizer typeTokSB;
		typeTokSB.Init(sbTypeFile, typeStreamSB);

		ASSET.PushFolder("$/fragments/cloth/temp");

		rmcTypeFileParser parserSB;

		typeTokSB.Reset();
		parserSB.Reset();
		parserSB.RegisterLoader( "cloth", "all", datCallback(MFA1( phSoftBody::LoadSB ), m_SoftBody, 0, true) );

		// Load the softbody  object.
		parserSB.ProcessTypeFile(static_cast<fiTokenizer&>(typeTokSB), true);

#endif

		return demoWld;
	}

	virtual void InitCamera ()
	{
		// Set the camera position and target for a good view of the cloth.
		Vector3 lookFrom(4.0f,8.0f,-6.0f);
		Vector3 lookTo(0.0f,2.0f,0.0f);
		InitSampleCamera(lookFrom,lookTo);
	}

	virtual void Shutdown()
	{
#if SOFT_BODY
		m_SoftBody->Shutdown();
#endif 
		grcSampleManager::Shutdown();
	}


#if __BANK
	virtual void AddWidgetsClient ()
	{
		bkBank& bank = BANKMGR.CreateBank("Sample Cloth");
		bank.AddSlider("Wind Speed",&m_WindSpeed,-40.0f,40.0f,1.0f);

		physicsSampleManager::AddWidgetsClient();
	}
#endif

protected:
	environmentCloth*  m_EnvCloth;
	float              m_WindSpeed;
};

} // namespace ragesamples


int Main()
{
	{
		ragesamples::clothSampleManager samplePhysics;
		samplePhysics.Init();

		samplePhysics.UpdateLoop();

		samplePhysics.Shutdown();
	}

	return 0;
}
