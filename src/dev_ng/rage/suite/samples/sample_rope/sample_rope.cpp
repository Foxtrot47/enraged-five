// 
// sample_rope/sample_rope.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "atl/atfunctor.h"
#include "cloth/environmentcloth.h"
#include "gizmo/manager.h"
#include "gizmo/translation.h"
#include "grcore/im.h"
#include "grmodel/setup.h"
#include "grrope/grroperender.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "pharticulated/articulatedcollider.h"
#include "pharticulated/bodypart.h"
#include "phbound/boundcomposite.h"
#include "phcore/phmath.h"
#include "phbound/boundsphere.h"
#include "pheffects/cloth_verlet.h"
#include "pheffects/rope.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "physics/constraint.h"
#include "sample_physics/demoobject.h"
#include "sample_physics/sample_physics.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "math/random.h"

// RAY - test
//#include "phbound/boundsphere.h"
//#include "curve/curvecatrom.h"
//#include "grmodel/shader.h"
//#include "grmodel/shadergroup.h"
//#include "grcore/vertexbuffer.h"
//#include "grcore/indexbuffer.h"
//#include "grcore/device.h"
//#include "grmodel/modelfactory.h"
//#include "math/amath.h"

// PURPOSE: Define the maximum number of rope pieces in all the physical worlds in this sample.
//			A game-specific manager is required to control rope allocation and reset. Rope update
//			is done through phInstBehavior, so it is automatic for rope placed in the physics level.
// NOTES:
//		Rope does not yet work with the accumulation lcp solver. It will hang objects correctly, but
//		with the accumulation solver turned on, rope will not correctly wrap around objects.
#define	MAX_NUM_ROPES		64
#define MAX_NUM_PENDANTS	16

#define SLIDE_ROPE_THROUGH_RING	0

PARAM(file, "Reserved for future expansion, not currently hooked up");


#if __XENON

void    WINAPI D3DDevice_SetRenderState_ParameterCheck(D3DDevice *UNUSED_PARAM(pDevice), D3DRENDERSTATETYPE UNUSED_PARAM(State), DWORD UNUSED_PARAM(Value)) {}
void    WINAPI D3DDevice_GetRenderState_ParameterCheck(__in D3DDevice* UNUSED_PARAM(pDevice), D3DRENDERSTATETYPE UNUSED_PARAM(State)) {}
void    WINAPI D3DDevice_SetVertexShaderConstantF_ParameterCheck(__in D3DDevice* UNUSED_PARAM(pDevice), UINT UNUSED_PARAM(StartRegister), __in_ecount(4*UNUSED_PARAM(Vector4fCount)) CONST float *UNUSED_PARAM(pConstantData), DWORD UNUSED_PARAM(Vector4fCount)) {}
void    WINAPI D3DDevice_SetPixelShaderConstantF_ParameterCheck(__in D3DDevice* UNUSED_PARAM(pDevice), UINT UNUSED_PARAM(StartRegister), __in_ecount(4*UNUSED_PARAM(Vector4fCount)) CONST float *UNUSED_PARAM(pConstantData), DWORD UNUSED_PARAM(Vector4fCount)) {}
void    WINAPI D3DDevice_SetSamplerState_ParameterCheck(__in D3DDevice *UNUSED_PARAM(pDevice), DWORD UNUSED_PARAM(Sampler), D3DSAMPLERSTATETYPE UNUSED_PARAM(Type), DWORD UNUSED_PARAM(Value)) {}
BOOL D3D__SingleStepper;

#endif


namespace rage {
EXT_PFD_DECLARE_GROUP(DrawCloth);
EXT_PFD_DECLARE_ITEM(Models);

#if __PFDRAW
extern void DrawClothWireframe (const phClothData& clothData, const phVerletCloth& cloth, int lodIndex, const Vector3* vertexNormals);
#endif

#if (__BANK && !__RESOURCECOMPILER)
datCallback	g_ClothManagerCallback = NullCB;
#endif

}


namespace ragesamples {

using namespace rage;


///////////////////////////////////////////////////////////////
// TITLE: sample_rope - Using the rope simulation
// PURPOSE:
//		This sample shows how to use the rope simulation. It also contains
//		multiple pages with rope in various physical situations to test
//		rope when other physics changes are made.


void AddBrokenRopePiece (environmentCloth* UNUSED_PARAM(originalEnvCloth), environmentCloth* newEnvCloth)
{
	phSwingingObject& ropeObject = *rage_new phSwingingObject;
	ropeObject.SetInst(const_cast<phInst*>(newEnvCloth->GetInstance()));
	const float ropeRadius = 0.02f;
	rmcRopeDrawable* ropeDrawable = rage_new rmcRopeDrawable(newEnvCloth->GetCloth(),ropeRadius);
	ropeObject.SetDrawable(ropeDrawable);
	newEnvCloth->SetClonedDrawable(ropeDrawable);
	phDemoWorld* demoWorld = phDemoWorld::GetActiveDemo();
	demoWorld->AddUpdateObject(&ropeObject);
}


class RopeManager : public physicsSampleManager
{
public:


	phDemoWorld& MakeRopeDemoWorld (const char* worldName, const char* terrainName=NULL)
	{
		const Vector3 terrainPosition = ORIGIN;
		int maxOctreeNodes = 1000;
		int maxActiveObjects = 500;
		int maxObjects = 500;
		const Vector3 worldMin = Vector3(-999.0f,-999.0f,-999.0f);
		const Vector3 worldMax = Vector3(999.0f,999.0f,999.0f);
		int maxInstBehaviors = MAX_NUM_ROPES;
		return MakeNewDemoWorld(worldName,terrainName,terrainPosition,maxOctreeNodes,maxActiveObjects,maxObjects,worldMin,worldMax,maxInstBehaviors);
	}


	virtual void InitClient ()
	{
		// STEP #1. Initialize the base physics sample class.
		physicsSampleManager::InitClient();

	#if __PFDRAW
		// Turn off physics drawing because it interferes with rope rendering (the base class InitClient turns it on).
		GetRageProfileDraw().SetEnabled(false);
		PFD_Models.SetEnabled(true);
	#endif

		// Initialize rope rendering.
		RopeModel::InitRopeModel("sample_rope\\p_gen_rope02.dds", NULL, NULL);

		// Create a pool of extra cloth pieces, for breaking rope.
// Not used anymore
// 		phEnvClothPool::CreateEnvClothPool(16);
// 		Functor2<environmentCloth*,environmentCloth*> breakRopeFunction = MakeFunctor(&AddBrokenRopePiece);
// 		ENVCLOTHPOOL->SetTearClothFunction(breakRopeFunction);

		// STEP #2. Initialize parameters that manage the collection of rope pieces.
		// The different rope pieces are not all in the same physical world.
		m_NumRopes = 0;
		int ropeIndex,pendantIndex;
		for (ropeIndex=0; ropeIndex<MAX_NUM_ROPES; ropeIndex++)
		{
			m_RopeObject[ropeIndex] = NULL;
			m_Rope[ropeIndex] = NULL;
			m_IsControlled[ropeIndex] = false;
			m_DemoWorld[ropeIndex] = NULL;
			m_NumPendants[ropeIndex] = 0;
			m_Coiled[ropeIndex] = false;
			for (pendantIndex=0; pendantIndex<MAX_NUM_PENDANTS; pendantIndex++)
			{
				m_PendantConstraint[ropeIndex][pendantIndex] = NULL;
			}
		}

		// STEP #3. Create all the sets of interacting objects, with a sample page for each set.
		//-- "+" and "-" keys control paging through the following set of sample pages.
		// CreateRope in each section initializes each rope piece and places it in the physical world.
		phInst* pendant;
		Vector3 position,rotation,frequency,amplitude,phaseOffset,impulse;
		float ropeLength,ropeRadius,coilRadius;
		int pendantVertex,numEdges,loopVertexToEnd;
		bool alignBottom = false;
		bool pinVertex = true;
		m_RopeInRing = NULL;

		m_VertexIndexList[0] = 0;
		m_VertexIndexList[1] = 5;
		m_VertexIndexList[2] = 10;
		m_VertexIndexList[3] = 15;
		m_VertexIndexList[4] = 20;
		m_VertexIndexList[5] = 25;

		PFD_GROUP_ENABLE(DrawCloth,true);

		////////////////////////
		// Page 0: swinging rope
		phDemoWorld& swingingWorld = MakeRopeDemoWorld("Swinging Rope","big_plane");
		//swingingWorld.SetFramerate(30.0f);
		ropeLength = 6.0f;
		ropeRadius = 0.02f;
		position.Set(-0.5f*ropeLength,ropeLength,0.0f);
		rotation.Set(0.0f,0.0f,-0.5f*PI);
		ropeIndex = CreateRope(swingingWorld,position,rotation,NULL,ropeLength,ropeRadius,34);
		AddSwingingConstraint(ropeIndex,ropeLength);

		//////////////////////////
		// Page 1: telephone poles
		phDemoWorld& poleWorld = MakeRopeDemoWorld("Telephone Poles","big_plane");
		ropeLength = 10.0f;
		float zOffset = 0.0f;
		position.Set(2.0f*ropeLength,0.0f,zOffset);
		rotation.Set(0.0f,0.5f*PI,0.0f);
		poleWorld.CreateFixedObject("capsule_tee_big",position,alignBottom,rotation);
		position.x -= ropeLength;
		phInst* poleRight = poleWorld.CreateObject("capsule_tee_big",position,alignBottom,rotation)->GetPhysInst();
		position.x -= ropeLength;
		phInst* poleCenter = poleWorld.CreateObject("capsule_tee_big",position,alignBottom,rotation)->GetPhysInst();
		position.x -= ropeLength;
		phInst* poleLeft = poleWorld.CreateObject("capsule_tee_big",position,alignBottom,rotation)->GetPhysInst();
		position.x -= ropeLength;
		poleWorld.CreateFixedObject("capsule_tee_big",position,alignBottom,rotation);
		ropeRadius = 0.02f;
		position.Set(-2.0f*ropeLength,4.6f,zOffset+1.0f);
		rotation.Set(0.0f,0.0f,0.0f);
		int pinVertexIndex = 0;
		const float lengthScale = 1.05f;
		ropeIndex = CreateRope(poleWorld,position,rotation,NULL,ropeLength,ropeRadius,18,-1,pinVertexIndex,-1.0f,false,lengthScale);
		AddPendant(ropeIndex,ropeLength,poleLeft);
		position.z -= 2.0f;
		ropeIndex = CreateRope(poleWorld,position,rotation,NULL,ropeLength,ropeRadius,18,-1,pinVertexIndex,-1.0f,false,lengthScale);
		AddPendant(ropeIndex,ropeLength,poleLeft);
		position.x += ropeLength;
		position.z += 2.0f;
		pinVertex = false;
		ropeIndex = CreateRope(poleWorld,position,rotation,NULL,ropeLength,ropeRadius,18,-1,pinVertexIndex,-1.0f,false,lengthScale);
		AddPendant(ropeIndex,ropeLength,poleLeft,0);
		AddPendant(ropeIndex,ropeLength,poleCenter,18,0);
		position.z -= 2.0f;
		ropeIndex = CreateRope(poleWorld,position,rotation,NULL,ropeLength,ropeRadius,18,-1,pinVertexIndex,-1.0f,false,lengthScale);
		AddPendant(ropeIndex,ropeLength,poleLeft,0);
		AddPendant(ropeIndex,ropeLength,poleCenter,18,0);
		position.x += ropeLength;
		position.z += 2.0f;
		ropeIndex = CreateRope(poleWorld,position,rotation,NULL,ropeLength,ropeRadius,18,-1,pinVertexIndex,-1.0f,false,lengthScale);
		AddPendant(ropeIndex,ropeLength,poleCenter,0);
		AddPendant(ropeIndex,ropeLength,poleRight,18,0);
		position.z -= 2.0f;
		ropeIndex = CreateRope(poleWorld,position,rotation,NULL,ropeLength,ropeRadius,18,-1,pinVertexIndex,-1.0f,false,lengthScale);
		AddPendant(ropeIndex,ropeLength,poleCenter,0);
		AddPendant(ropeIndex,ropeLength,poleRight,18,0);
		position.Set(2.0f*ropeLength,4.6f,zOffset+1.0f);
		rotation.Set(0.0f,0.0f,PI);
		pinVertex = true;
		ropeIndex = CreateRope(poleWorld,position,rotation,NULL,ropeLength,ropeRadius,18,-1,pinVertexIndex,-1.0f,false,lengthScale);
		AddPendant(ropeIndex,ropeLength,poleRight);
		position.z -= 2.0f;
		ropeIndex = CreateRope(poleWorld,position,rotation,NULL,ropeLength,ropeRadius,18,-1,pinVertexIndex,-1.0f,false,lengthScale);
		AddPendant(ropeIndex,ropeLength,poleRight);
		position.Set(0.0f,0.5f,-4.0f);
		rotation.Set(0.125f*PI,0.0f,0.0f);
		poleWorld.CreateFixedObject("small_plane",position,alignBottom,rotation);
		position.x += 0.3f;
		position.y += 12.0f;
		poleWorld.CreateObject("sphere_122",position);

		//////////////////////////////////////////
		// Page 2: swinging rope hanging a ragdoll
		phDemoWorld& swingingRagdollWorld = MakeRopeDemoWorld("Swinging Ragdoll","big_plane");
		position.Set(-2.0f,6.0f,0.0f);
		rotation.Set(0.0f,0.0f,0.0f);
		ropeLength = 4.0f;
		ropeRadius = 0.02f;
		ropeIndex = CreateRope(swingingRagdollWorld,position,rotation,NULL,ropeLength,ropeRadius);
		frequency.Set(4.0f,3.0f,2.0f);
		amplitude.Set(1.0f,0.5f,1.0f);
		AddSwingingConstraint(ropeIndex,ropeLength,frequency,amplitude);
		position.Set(2.15f,5.7f,0.0f);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		phArticulatedObject* draggee = phArticulatedObject::CreateHuman(swingingRagdollWorld,position,rotation);
		pendant = draggee->GetPhysInst();
		AddPendant(ropeIndex,ropeLength,pendant);
		Vector3 muscleTargetAngle(0.45f*PI,0.0f,-0.45f*PI);
		float muscleAngleStrength = 400.0f;
		float muscleSpeedStrength = 10.0f;
		phJoint3Dof& rightShoulder = draggee->GetBody().GetJoint3Dof(2);
		rightShoulder.SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
		rightShoulder.SetMuscleAngleStrength(muscleAngleStrength);
		rightShoulder.SetMuscleSpeedStrength(muscleSpeedStrength);
		rightShoulder.SetMuscleTargetAngle(muscleTargetAngle);
		rightShoulder.SetMuscleTargetSpeed(ORIGIN);
		muscleTargetAngle.x = -muscleTargetAngle.x;
		muscleTargetAngle.y = -muscleTargetAngle.y;
		phJoint3Dof& leftShoulder = draggee->GetBody().GetJoint3Dof(4);
		leftShoulder.SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
		leftShoulder.SetMuscleAngleStrength(muscleAngleStrength);
		leftShoulder.SetMuscleSpeedStrength(muscleSpeedStrength);
		leftShoulder.SetMuscleTargetAngle(muscleTargetAngle);
		leftShoulder.SetMuscleTargetSpeed(ORIGIN);
		float elbowTargetAngle = 0.5f*PI;
		phJoint1Dof& rightElbow = draggee->GetBody().GetJoint1Dof(3);
		rightElbow.SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
		rightElbow.SetMuscleAngleStrength(muscleAngleStrength);
		rightElbow.SetMuscleSpeedStrength(muscleSpeedStrength);
		rightElbow.SetMuscleTargetAngle(elbowTargetAngle);
		rightElbow.SetMuscleTargetSpeed(0.0f);
		phJoint1Dof& leftElbow = draggee->GetBody().GetJoint1Dof(5);
		leftElbow.SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
		leftElbow.SetMuscleAngleStrength(muscleAngleStrength);
		leftElbow.SetMuscleSpeedStrength(muscleSpeedStrength);
		leftElbow.SetMuscleTargetAngle(-elbowTargetAngle);
		leftElbow.SetMuscleTargetSpeed(0.0f);

		///////////////////////////////////
		// Page 3: a swinging, coiled lasso
		phDemoWorld& lassoWorld = MakeRopeDemoWorld("lasso","big_plane");
		//position.Set(-0.5f,0.6f,0.0f);
		//rotation.Set(0.4f*PI,0.0f,0.4f*PI);
		//phArticulatedObject::CreateHuman(lassoWorld,position,rotation);
		position.Set(0.0f,3.8f,-0.4f);
		rotation.Set(0.5f*PI,0.0f,0.0f);
		numEdges = 19;
		coilRadius = 0.25f;
		ropeLength = (float)numEdges*2.0f*PI*coilRadius/5.0f;
		ropeRadius = 0.02f;
		loopVertexToEnd = 16;
		pinVertex = false;
		ropeIndex = CreateRope(lassoWorld,position,rotation,NULL,ropeLength,ropeRadius,numEdges,loopVertexToEnd,pinVertex,coilRadius);
		m_Coiled[ropeIndex] = true;
		frequency.Set(4.0f,4.0f,4.0f);
		amplitude.Set(1.4f,1.2f,1.4f);
		phaseOffset.Set(0.5f,0.3f,0.0f);
		AddSwingingConstraint(ropeIndex,ropeLength,frequency,amplitude,phaseOffset);

		//////////////////////
		// Page 4: a rope mesh
		phDemoWorld& hangingRopeWorld = MakeRopeDemoWorld("Rope Mesh","big_plane");
		Vector3 cornerA(2.0f,3.0f,0.0f);
		Vector3 cornerB(-2.0f,3.0f,0.0f);
		Vector3 cornerC(-2.0f,1.0f,0.0f);
		CreateRopeMesh(hangingRopeWorld,cornerA,cornerB,cornerC,4,3,true,true,false,true);

		////////////////////////////////////////////////////////////
		// Page X: swinging rope dragging a ragdoll on rough terrain
		phDemoWorld& draggingRagdollRoughWorld = MakeRopeDemoWorld("draggingRagdollRough","splashot");
		//draggingRagdollRoughWorld.SetFramerate(30.0f);
		position.Set(-2.0f,3.5f,0.0f);
		rotation.Set(0.0f,0.0f,0.0f);
		ropeLength = 12.0f;
		ropeRadius = 0.03f;
		ropeIndex = CreateRope(draggingRagdollRoughWorld,position,rotation,NULL,ropeLength,ropeRadius);
		frequency.Set(0.7f,1.0f,1.0f);
		amplitude.Set(12.0f,0.5f,12.0f);
		AddSwingingConstraint(ropeIndex,ropeLength,frequency,amplitude);
		position.Set(10.15f,3.4f,0.0f);
		rotation.Set(0.0f,-0.5f*PI,0.0f);
		pendant = phArticulatedObject::CreateHuman(draggingRagdollRoughWorld,position,rotation)->GetPhysInst();
		AddPendant(ropeIndex,ropeLength,pendant);

		/////////////////////////////////////////////////
		// Page 5: ragdoll hanging from a constrained box
		phDemoWorld& railWorld = MakeRopeDemoWorld("Hanging From Box","big_plane");
		position.Set(0.0f,6.0f,0.0f);
		rotation.Set(0.0f,0.0f,-0.5f*PI);
		ropeLength = 3.5f;
		ropeRadius = 0.02f;
		ropeIndex = CreateRope(railWorld,position,rotation,NULL,ropeLength,ropeRadius);
		position.Set(0.0f,6.0f,0.0f);
		alignBottom = false;
		rotation.Zero();
		phDemoObject* slider = railWorld.CreateObject("crate",position,alignBottom,rotation);
		position.y += 0.5f;
		phConstraint* translationConstraint = PHSIM->GetConstraintMgr()->AttachObjectToWorld('Sro0', position,*slider->GetPhysInst());
		translationConstraint->SetHardLimitMin(position + Vector3(-100.0f,0.0f,-100.0f));
		translationConstraint->SetHardLimitMax(position + Vector3( 100.0f,0.0f, 100.0f));
		translationConstraint->SetConstraintLimitType(phConstraint::LIMIT_TYPE_AXES);
		pendant = slider->GetPhysInst();
		pendantVertex = 0;
		AddPendant(ropeIndex,ropeLength,pendant,pendantVertex);
		position.Set(0.0f,2.5f,0.15f);
		rotation.Set(0.0f,0.0f,0.0f);
		pendant = phArticulatedObject::CreateHuman(railWorld,position,rotation)->GetPhysInst();
		AddPendant(ropeIndex,ropeLength,pendant);

		//////////////////////////////////////////////
		// Page 6: rope with a fixed capsule and a box
		phDemoWorld& pommelWorld = MakeRopeDemoWorld("pommel_horse","big_plane");
		pommelWorld.SetFramerate(30.0f);
		position.Set(0.0f,2.6f,0.3f);
		rotation.Set(0.5f*PI,-0.25f*PI,0.0f);
		alignBottom = false;
		pendant = pommelWorld.CreateFixedObject("capsule_fat",position,alignBottom,rotation)->GetPhysInst();
		position.Set(-2.0f,0.0f,0.0f);
		rotation.Zero();
//		pommelWorld.CreateObject("crate",position,alignBottom,rotation);
		position.Set(0.0f,5.4f,0.0f);
		rotation.Set(0.0f,0.0f,PI);
		ropeLength = 4.0f;
		ropeRadius = 0.04f;
		const bool breakable = true;
		ropeIndex = CreateRope(pommelWorld,position,rotation,NULL,ropeLength,ropeRadius,18,BAD_INDEX,BAD_INDEX,-1.0f,breakable);
	//	phVerletCloth* cloth = m_Rope[ropeIndex]->GetCloth();
	//	cloth->SetBodyInstanceEnviroCloth(pendant);
	//	cloth->SetBodyCollisionEnviroCloth(0,false);
	//	cloth->SetBodyCollisionEnviroCloth(1,false);
/*		AddGizmo(ropeIndex,ropeLength);*/
		position.Set(-4.035f,5.4f,0.0f);
		pendant = pommelWorld.CreateObject("sphere_007",position)->GetPhysInst();
		AddPendant(ropeIndex,ropeLength,pendant);

		//////////////////////////////////////////
		// Page 7: rope pulling a box over a ledge
		phDemoWorld& ledgeWorld = MakeRopeDemoWorld("ledge","big_plane");
		position.Set(0.0f,2.2f,-3.0f);
		rotation.Set(0.25f,0.0f,0.0f);
		ledgeWorld.CreateFixedObject("box_big",position,alignBottom,rotation);
		position.Set(0.0f,2.0f,-1.0f);
		ledgeWorld.CreateFixedObject("box_big",position);
		position.Set(0.0f,6.0f,1.8f);
		ledgeWorld.CreateObject("sphere_020",position,alignBottom);
		position.Set(2.0f,7.0f,-2.0f);
		rotation.Set(0.0f,0.0f,PI);
		ropeLength = 4.0f;
		ropeRadius = 0.02f;
		ropeIndex = CreateRope(ledgeWorld,position,rotation,NULL,ropeLength,ropeRadius);
		position.Set(2.56f,7.0f,-2.0f);
		rotation.Zero();
		pendant = ledgeWorld.CreateObject("sphere_056",position)->GetPhysInst();
		AddPendant(ropeIndex,ropeLength,pendant,0);
		position.Set(-2.56f,7.0f,-2.0f);
		pendant = ledgeWorld.CreateObject("sphere_056",position)->GetPhysInst();
		AddPendant(ropeIndex,ropeLength,pendant);

		/////////////////////////////////////////////////
		// Page X: two spheres hanging around a fixed box
/*		phDemoWorld& bolaWorld = MakeRopeDemoWorld("bola","big_plane");
		position.Set(2.0f,7.0f,0.0f);
		rotation.Set(0.0f,0.0f,PI);
		ropeLength = 4.0f;
		ropeRadius = 0.02f;
		ropeIndex = CreateRope(bolaWorld,position,rotation,NULL,ropeLength,ropeRadius);
		position.Set(2.56f,7.0f,0.0f);
		rotation.Set(0.0f,0.0f,PI);
		pendant = bolaWorld.CreateObject("sphere_056",position)->GetPhysInst();
		AddPendant(ropeIndex,ropeLength,pendant,0);
		position.Set(-2.56f,7.0f,0.0f);
		pendant = bolaWorld.CreateObject("sphere_056",position)->GetPhysInst();
		AddPendant(ropeIndex,ropeLength,pendant);
		position.Set(0.0f,4.0f,0.0f);
		bolaWorld.CreateFixedObject("box_long",position,alignBottom,rotation);
*/
		///////////////////////////////////////////
		// Page X: a bola wrapping around a ragdoll
	/*	phDemoWorld& bolaRagdollWorld = MakeRopeDemoWorld("bolaRagdoll","big_plane");
		position.Set(-4.0f,1.4f,2.0f);
		rotation.Set(0.5f*PI,0.0f,0.0f);
		ropeLength = 4.0f;
		ropeRadius = 0.02f;
		ropeIndex = CreateRope(bolaRagdollWorld,position,rotation,NULL,ropeLength,ropeRadius);
		position.Set(-4.0f,1.4f,2.035f);
		pendant = bolaRagdollWorld.CreateObject("sphere_007",position)->GetPhysInst();
		AddPendant(ropeIndex,ropeLength,pendant,0);
		impulse.Set(10.0f,0.0f,0.0f);
		bolaRagdollWorld.GetSimulator()->ApplyImpulse(pendant->GetLevelIndex(),impulse);
		position.Set(-4.0f,1.4f,-2.035f);
		pendant = bolaRagdollWorld.CreateObject("sphere_007",position)->GetPhysInst();
		AddPendant(ropeIndex,ropeLength,pendant);
		bolaRagdollWorld.GetSimulator()->ApplyImpulse(pendant->GetLevelIndex(),impulse);
		position.Set(4.0f,1.0f,0.0f);
		phArticulatedObject::CreateHuman(bolaRagdollWorld,position);*/

		/////////////////////////////////////////////////
		// Page 8: wrecking ball with a pyramid of crates
		phDemoWorld& wreckerWorld = MakeRopeDemoWorld("Wrecking Ball","big_plane");
		wreckerWorld.SetFramerate(30.0f);
		position.Set(-4.0f,6.0f,0.0f);
		rotation.Set(0.0f,0.0f,0.0f);
		ropeLength = 4.0f;
		ropeRadius = 0.03f;
		ropeIndex = CreateRope(wreckerWorld,position,rotation,NULL,ropeLength,ropeRadius,18,-1,18);
	//	AddGizmo(ropeIndex,ropeLength,18);
		position.Set(-4.56f,6.0f,0.0f);
		rotation.Zero();
		pendant = wreckerWorld.CreateObject("sphere_056",position)->GetPhysInst();
		AddPendant(ropeIndex,ropeLength,pendant,0,18);
		position.Set(1.0f,0.0f,0.0f);
		int pyramidHeight = 6;
		wreckerWorld.ConstructPyramid(position,pyramidHeight,false,"cube_040");
	#if SLIDE_ROPE_THROUGH_RING
		m_RopeInRing = m_Rope[ropeIndex];
		int middleVertex = m_RopeInRing->GetCloth()->GetNumVertices()/2;
		m_RingPosition = VECTOR3_TO_VEC3V(m_RopeInRing->GetCloth()->GetClothData().GetVertexPosition(middleVertex));
	#endif

		//////////////////////////
		// Page X: hanging ragdoll
/*		phDemoWorld& executionWorld = MakeRopeDemoWorld("execution","big_plane");
		position.Set(0.0f,6.0f,0.0f);
		rotation.Set(0.0f,0.0f,0.0f);
		ropeLength = 4.0f;
		ropeRadius = 0.03f;
		ropeIndex = CreateRope(executionWorld,position,rotation,NULL,ropeLength,ropeRadius);
		AddGizmo(ropeIndex,ropeLength);
		position.Set(4.15f,5.7f,0.0f);
		rotation.Set(0.0f,0.5f*PI,0.0f);
		pendant = phArticulatedObject::CreateHuman(executionWorld,position,rotation)->GetPhysInst();
		AddPendant(ropeIndex,ropeLength,pendant);
		pendantIndex = 0;
		m_Rope[ropeIndex]->GetEnvironmentCloth()->SetPendantNoCollideEdge(pendantIndex);
		m_Rope[ropeIndex]->GetEnvironmentCloth()->SetPendantNoCollidePart(pendantIndex,1);
*/
		//////////////////////////
		// Page X: swinging lasso
	/*	phDemoWorld& lariatWorld = MakeRopeDemoWorld("lariat","big_plane");
		position.Set(2.0f,2.4f,0.0f);
		rotation.Set(0.0f,0.0f,0.0f);
		ropeLength = 4.0f;
		ropeRadius = 0.02f;
		numEdges = 18;
		loopVertexToEnd = 6;
		ropeIndex = CreateRope(lariatWorld,position,rotation,NULL,ropeLength,ropeRadius,numEdges,loopVertexToEnd);
		frequency.Set(3.0f,1.0f,2.0f);
		amplitude.Set(1.0f,0.4f,1.0f);
		phaseOffset.Set(0.5f*PI,PI,0.0f);
		AddSwingingConstraint(ropeIndex,ropeLength,frequency,amplitude,phaseOffset);*/

		// STEP #4. Set and activate the initial page, from the command line parameter "startingDemo" with the first page as default.
		m_Demos.SetCurrentDemo(0);
		m_Demos.GetCurrentWorld()->Activate();
	}

	virtual void InitCamera ()
	{
		// STEP #5. Set the camera position and target for a good view of each rope world.
		Vector3 lookFrom(0.0f,12.0f,12.0f);
		Vector3 lookTo(0.0f,0.0f,0.0f);
		InitSampleCamera(lookFrom,lookTo);
	}

	virtual void Update ()
	{
		physicsSampleManager::Update();

	#if SLIDE_ROPE_THROUGH_RING
		if (m_RopeInRing)
		{
			int edgeIndex;
			ScalarV edgeT;
			phVerletCloth* cloth = m_RopeInRing->GetCloth();
			cloth->ComputeEdgeIndexAndT(m_RingPosition,edgeIndex,edgeT);
			Vec3V positionOnRope = cloth->ComputePositionOnStrand(edgeIndex,edgeT);
			Vec3V displacement = Subtract(m_RingPosition,positionOnRope);
			ScalarV depth = Mag(displacement);
			Vec3V normal = InvScale(displacement,depth);

			phClothData& clothData = cloth->GetClothData();
			Vec3V* vertexPositions = (Vec3V*)clothData.GetVertexPointer();
			const Vec3V* vertexPrevPositions = (const Vec3V*)clothData.GetVertexPrevPointer();

			const phEdgeData& edge = cloth->GetEdgeData(edgeIndex);
			int vertexA = edge.m_vertIndices[0];
			int vertexB = edge.m_vertIndices[1];
			ScalarV distToA2 = MagSquared(Subtract(vertexPositions[vertexA],m_RingPosition));
			ScalarV distToB2 = MagSquared(Subtract(vertexPositions[vertexB],m_RingPosition));
			ScalarV fractionA = InvScale(distToA2,Add(distToA2,distToB2));
			ScalarV fractionB = Subtract(ScalarV(V_ONE),fractionA);
			ScalarV depthA = Scale(depth,fractionA);
			ScalarV depthB = Scale(depth,fractionB);
			cloth->ApplyCollisionBasic(normal,depthA,vertexPositions[vertexA],vertexPrevPositions[vertexA]);
			cloth->SetVertexColliding(vertexA,normal);
			cloth->ApplyCollisionBasic(normal,depthB,vertexPositions[vertexB],vertexPrevPositions[vertexB]);
			cloth->SetVertexColliding(vertexB,normal);
		}
	#endif

		// STEP #6. Update the rope controls. The rope class (phRope) is derived from phInstBehavior, and it is updated when the physics
		// simulator updates all of the instance behaviors.
		for (int ropeIndex=0; ropeIndex<m_NumRopes; ropeIndex++)
		{
			// See if this rope piece is in the currently active physical world.
			if (m_Demos.GetCurrentWorld()==m_DemoWorld[ropeIndex])
			{
				// This rope piece is in the currently active physical world.
				Assert(m_Rope[ropeIndex]);
				phEnvClothVerletBehavior& rope = *m_Rope[ropeIndex];

				rope.GetEnvironmentCloth()->CopyVerletDrawVertsForRope();

				// See if the rope should be teleported.
				if (ioKeyboard::KeyPressed(KEY_T))
				{
					// T was pressed, so teleport and re-orient the rope.
					const Vector3 push(0.0f,0.2f,0.0f);
					const Vector3 turn(0.0f,0.0f,0.25f*PI);
					Matrix34 newWorld(CreateRotatedMatrix(push,turn));
					newWorld.DotFromLeft( RCC_MATRIX34(rope.GetInstance()->GetMatrix()) );
					rope.GetEnvironmentCloth()->TeleportAndOrient(newWorld);
					if (m_IsControlled[ropeIndex])
					{
						m_ControlledMatrix[ropeIndex].d.Set(newWorld.d);
						if (m_RopeConstraint[ropeIndex])
						{
							m_RopeConstraint[ropeIndex]->SetWorldPosition(newWorld.d);
						}
					}
				}

				if (ioKeyboard::KeyPressed(KEY_H))
				{
// 					// H was pressed, so halve the number of rope edges.
// 					int oldNumEdges = rope.GetCloth()->GetNumEdges();
// 					int firstEdge = 0;
// 					int lastEdge = oldNumEdges-1;
// 					int newNumEdges = oldNumEdges/2;
// 					rope.MapToNewRopeEdges(newNumEdges,firstEdge,lastEdge);
				}
				else if (ioKeyboard::KeyPressed(KEY_D))
				{
// 					// D was pressed, so double the number of rope edges.
// 					int oldNumEdges = rope.GetCloth()->GetNumEdges();
// 					int firstEdge = 0;
// 					int lastEdge = oldNumEdges-1;
// 					int newNumEdges = oldNumEdges*2;
// 					rope.MapToNewRopeEdges(newNumEdges,firstEdge,lastEdge);
				}

				// See if this rope piece is controlled by a gizmo or a constraint.
				if (m_IsControlled[ropeIndex])
				{
					if (m_RopeConstraint[ropeIndex])
					{
						// This rope piece is controlled by a constraint, so copy the constraint's new position (gizmo positions update automatically).
						m_ControlledMatrix[ropeIndex].d.Set(m_RopeConstraint[ropeIndex]->GetWorldPosition());
					}

					// Constrain the rope's first vertex to the controlled position and velocity.
// 					const int vertexIndex = 0;
// 					if (!m_Coiled[ropeIndex])
// 					{
// 						m_Rope[ropeIndex]->GetEnvironmentCloth()->ControlVertex(vertexIndex,m_ControlledMatrix[ropeIndex].d);
// 					}
// 					else
// 					{
// 						m_Rope[ropeIndex]->GetEnvironmentCloth()->ControlVertices(m_VertexIndexList,6,m_ControlledMatrix[ropeIndex].d);
// 					}

					m_PrevControlledMatrix[ropeIndex].Set(m_ControlledMatrix[ropeIndex]);
				}

				// See if the rope is controlled by a gizmo or constraint.
				if (m_IsControlled[ropeIndex] && m_NumPendants[ropeIndex]>0 && m_PendantConstraint[ropeIndex][0])
				{
					// This rope piece is controlled by a gizmo or constraint, so set the first pendant's constraint position to the controlled position.
					m_PendantConstraint[ropeIndex][0]->SetWorldPosition(m_ControlledMatrix[ropeIndex].d);
				}

// 			#if __PFDRAW
// 				DrawClothWireframe(rope.GetCloth()->GetClothData(),*rope.GetCloth(), 0);
// 			#endif

			}
		}
	}

	virtual void Reset ()
	{
		// STEP #7. Reset the rope controls.
		for (int ropeIndex=0; ropeIndex<m_NumRopes; ropeIndex++)
		{
			if (m_Demos.GetCurrentWorld()==m_DemoWorld[ropeIndex])
			{

				m_Rope[ropeIndex]->Reset();
				m_Rope[ropeIndex]->GetCloth()->Reset();
				if (m_IsControlled[ropeIndex])
				{
					m_ControlledMatrix[ropeIndex].Set(m_InitialControlledMatrix[ropeIndex]);
					m_PrevControlledMatrix[ropeIndex].Set(m_ControlledMatrix[ropeIndex]);
				}
			}
		}
	}

	virtual grcSetup& AllocateSetup ()
	{
		return *(rage_new grmSetup());
	}

	virtual void ShutdownClient ()
	{
		for (int ropeIndex=0; ropeIndex<m_NumRopes; ropeIndex++)
		{
			// Don't delete the rope objects, because the demo worlds delete them.
			phInst* ropeInstance = m_RopeObject[ropeIndex]->GetPhysInst();
			int levelIndex = ropeInstance->GetLevelIndex();
			if (m_DemoWorld[ropeIndex]->GetPhLevel()->IsInLevel(levelIndex))
			{
				// Remove the instance behavior and its instance from the simulator.
				m_DemoWorld[ropeIndex]->GetSimulator()->RemoveInstanceAndBehavior(*m_Rope[ropeIndex]->GetEnvironmentCloth()->GetBehavior());
			}

			// Remove the rope instance from the demo object, so that the cloth can delete it.
			m_RopeObject[ropeIndex]->SetPhysInst(NULL);

			// Delete the rope cloth type.
			delete m_Rope[ropeIndex]->GetEnvironmentCloth();
		}
// Not used anymore
//		delete ENVCLOTHPOOL;

		physicsSampleManager::ShutdownClient();
	}

	// PURPOSE: Create a piece of rope and add it to the given physics demo world.
	int CreateRope (phDemoWorld& demoWorld, const Vector3& endPosition, const Vector3& rotation, const char* UNUSED_PARAM(boundName)=NULL, float ropeLength=2.0f, float ropeRadius=0.02f,
					int numEdges=18, int loopVertexToEnd=BAD_INDEX, int pinVertexIndex=0, float coilRadius=-1.0f, bool breakable=false, float lengthScale=1.0f)
	{
		Assert(m_NumRopes<MAX_NUM_ROPES);

		// Create a rope and its matrix pose.
		environmentCloth& rope = *rage_new environmentCloth;
		Matrix34 ropePose(CreateRotatedMatrix(endPosition,rotation));

		float edgeLength = ropeLength/(float)numEdges;
		float coilingAngle = 0.0f;
		float coilAnglePerEdge = 0.0f;
		if (coilRadius>0.0f)
		{
			coilAnglePerEdge = edgeLength/coilRadius;
		}

		// Set the rope vertex positions.
		int numVertices = numEdges+1;
		Vector3* vertices = Alloca(Vector3,numVertices);
		Vector3 vertexPosition(ORIGIN);
		for (int vertexIndex=0; vertexIndex<numVertices; vertexIndex++)
		{
			ropePose.Transform(vertexPosition,vertices[vertexIndex]);
			vertexPosition.x += edgeLength*cosf(coilingAngle);
			vertexPosition.y -= edgeLength*sinf(coilingAngle);
			coilingAngle += coilAnglePerEdge;
		}

		// Initialize one-dimensional cloth.
		rope.AllocateClothController();
		phVerletCloth* cloth = rope.GetCloth();
/*		cloth->InitStrand(vertices,numVertices);*/
		cloth->InitEdgeData();
		rope.CreatePhysics(NULL,ropeRadius);
/*		rope.AllocatePendantData(2);*/
		m_Rope[m_NumRopes] = static_cast<phEnvClothVerletBehavior*>(rope.GetBehavior());
		cloth->InitCollisionData();
/*		cloth->InitSimpleData();*/
		if (pinVertexIndex>=0)
		{
			cloth->DynamicPinVertex(pinVertexIndex);
		}

		if (coilRadius>0.0f)
		{
			for (int pinnedVertIndex=0; pinnedVertIndex<6; pinnedVertIndex++)
			{
				if (m_VertexIndexList[pinnedVertIndex]<=numEdges)
				{
					cloth->DynamicPinVertex(m_VertexIndexList[pinnedVertIndex]);
				}
			}
		}

		// just to compile
		loopVertexToEnd += 0;

		phArchetype* ropeArchetype = rope.GetArchetype();
		m_RopeObject[m_NumRopes] = rage_new phSwingingObject;
		phSwingingObject& ropeObject = *m_RopeObject[m_NumRopes];
		Matrix34 matrix(CreateRotatedMatrix(endPosition,rotation));
		matrix.d.AddScaled(matrix.a,0.5f*ropeLength);
		ropeObject.SetInst(const_cast<phInst*>(rope.GetInstance()));
		ropeObject.InitPhys(ropeArchetype,matrix);
		rmcRopeDrawable* ropeDrawable = rage_new rmcRopeDrawable(m_Rope[m_NumRopes]->GetCloth(),ropeRadius);
		ropeObject.SetDrawable(ropeDrawable);
		rope.SetClonedDrawable(ropeDrawable);

		demoWorld.AddInactiveObject(&ropeObject);
		demoWorld.AddUpdateObject(&ropeObject);
		demoWorld.GetSimulator()->AddInstBehavior(*rope.GetBehavior());
		m_IsControlled[m_NumRopes] = false;
		m_DemoWorld[m_NumRopes] = &demoWorld;
		m_RopeConstraint[m_NumRopes] = NULL;

		int levelIndex = ropeObject.GetLevelIndex();
		demoWorld.GetPhLevel()->SetInactiveCollidesAgainstFixed(levelIndex,true);
		demoWorld.GetPhLevel()->SetInactiveCollidesAgainstInactive(levelIndex,true);

// Not used anymore
 		if (breakable)
 		{
// 			rope.GetCloth()->SetBreakable();
// 			rope.SetTearStrength(1.0f);
 		}

		rope.GetCloth()->ScaleRopeLength(lengthScale);

		return (m_NumRopes++);
	}

	int CreateRopeMesh (phDemoWorld& demoWorld, const Vector3& cornerA, const Vector3& cornerB, const Vector3& cornerC, int edgesAB, int edgesBC,
						bool /*outsideEdgeAB*/, bool /*outsideEdgeBC*/, bool /*outsideEdgeCD*/, bool /*outsideEdgeDA*/)
	{
		// Set the rope vertex positions.
		Vector3 ropeEdgeAB(cornerB);
		ropeEdgeAB.Subtract(cornerA);
		ropeEdgeAB.InvScale((float)edgesAB);
		Vector3 ropeEdgeBC(cornerC);
		ropeEdgeBC.Subtract(cornerB);
		ropeEdgeBC.InvScale((float)edgesBC);
		int numVertices = (edgesAB+1)*(edgesBC+1);
		Vector3* vertices = Alloca(Vector3,numVertices);
		Vector3 vertexPosition(cornerA);
		float* compressionWeight = Alloca(float,numVertices);
		int verticesAB = edgesAB+1;
		int verticesBC = edgesBC+1;
		int vertexIndex = 0;
		for (int vertexIndexBC=0; vertexIndexBC<verticesBC; vertexIndexBC++)
		{
			for (int vertexIndexAB=0; vertexIndexAB<verticesAB; vertexIndexAB++)
			{
				compressionWeight[vertexIndex] = 1.0f;
				vertices[vertexIndex++].Set(vertexPosition);
				vertexPosition.Add(ropeEdgeAB);
			}

			vertexPosition.SubtractScaled(ropeEdgeAB,(float)verticesAB);
			vertexPosition.Add(ropeEdgeBC);
		}

		// Initialize two-dimensional cloth.
		Assert(m_NumRopes<MAX_NUM_ROPES);
		environmentCloth& rope = *rage_new environmentCloth;
		rope.AllocateClothController();
		phVerletCloth* cloth = rope.GetCloth();
// 		cloth->InitRopeMesh(vertices,verticesAB,verticesBC,outsideEdgeAB,outsideEdgeBC,outsideEdgeCD,outsideEdgeDA);
		cloth->InitEdgeData(compressionWeight);
		rope.CreatePhysics();
/*		rope.AllocatePendantData(2);*/
		m_Rope[m_NumRopes] = static_cast<phEnvClothVerletBehavior*>(rope.GetBehavior());
		cloth->InitCollisionData();
/*		cloth->InitSimpleData();*/

		cloth->DynamicPinVertex(0);
		cloth->DynamicPinVertex(edgesAB);

		phArchetype* ropeArchetype = rope.GetArchetype();
		m_RopeObject[m_NumRopes] = rage_new phSwingingObject;
		phSwingingObject& ropeObject = *m_RopeObject[m_NumRopes];

		Matrix34 matrix(M34_IDENTITY);
		matrix.d.Set(cornerA);
		ropeObject.SetInst(const_cast<phInst*>(rope.GetInstance()));
		ropeObject.InitPhys(ropeArchetype,matrix);
		const float drawRadius = 0.1f;
		rmcRopeDrawable* ropeDrawable = rage_new rmcRopeDrawable(m_Rope[m_NumRopes]->GetCloth(),drawRadius);
		ropeObject.SetDrawable(ropeDrawable);
		rope.SetClonedDrawable(ropeDrawable);
		demoWorld.AddInactiveObject(&ropeObject);
		demoWorld.AddUpdateObject(&ropeObject);
		demoWorld.GetSimulator()->AddInstBehavior(*rope.GetBehavior());
		m_IsControlled[m_NumRopes] = false;
		m_DemoWorld[m_NumRopes] = &demoWorld;
		m_RopeConstraint[m_NumRopes] = NULL;

		int levelIndex = ropeObject.GetLevelIndex();
		demoWorld.GetPhLevel()->SetInactiveCollidesAgainstFixed(levelIndex,true);
		demoWorld.GetPhLevel()->SetInactiveCollidesAgainstInactive(levelIndex,true);

		return (m_NumRopes++);
	}

	// PURPOSE: Add a physical constraint to a piece of rope that swings it around with sine waves.
	void AddSwingingConstraint (int ropeIndex, float ropeLength, const Vector3& frequency=Vector3(4.0f,3.0f,2.0f), const Vector3& amplitude=Vector3(1.0f,1.0f,1.0f), const Vector3& phaseOffset=ORIGIN)
	{
		Assert(!m_IsControlled[ropeIndex]);
		phConstraintMgr* constraintMgr = m_DemoWorld[ropeIndex]->GetSimulator()->GetConstraintMgr();

		Vector3 ropeEndPosition = m_Rope[ropeIndex]->GetEnvironmentCloth()->GetWorldEndPosition(ropeLength);

		m_RopeConstraint[ropeIndex] = m_RopeObject[ropeIndex]->SetMovingConstraint(ropeEndPosition,constraintMgr);
		m_RopeObject[ropeIndex]->SetFrequency(frequency);
		m_RopeObject[ropeIndex]->SetAmplitude(amplitude);
		m_RopeObject[ropeIndex]->SetPhaseOffset(phaseOffset);
		m_ControlledMatrix[ropeIndex].Identity3x3();
		m_ControlledMatrix[ropeIndex].d.Set(m_RopeConstraint[ropeIndex]->GetWorldPosition());
		m_IsControlled[ropeIndex] = true;
		m_InitialControlledMatrix[ropeIndex].Set(m_ControlledMatrix[ropeIndex]);
		m_PrevControlledMatrix[ropeIndex].Set(m_ControlledMatrix[ropeIndex]);
// 		const int vertexIndex = 0;
// 		if (!m_Coiled[ropeIndex])
// 		{
// 			m_Rope[ropeIndex]->GetEnvironmentCloth()->ControlVertex(vertexIndex,m_ControlledMatrix[ropeIndex].d);
// 		}
// 		else
// 		{
// 			m_Rope[ropeIndex]->GetEnvironmentCloth()->ControlVertices(m_VertexIndexList,6,m_ControlledMatrix[ropeIndex].d);
// 		}
	}

	// PURPOSE: Add user control to the position of the end of a piece of rope with a gizmo, providing mouse control of position.
	void AddGizmo (int ropeIndex, float ropeLength, int/* vertexIndex=0*/)
	{
		Assert(!m_IsControlled[ropeIndex]);
		m_DemoWorld[ropeIndex]->AddTranslationGizmo(m_ControlledMatrix[ropeIndex]);
		m_ControlledMatrix[ropeIndex].Identity3x3();
		m_ControlledMatrix[ropeIndex].d.Set(m_Rope[ropeIndex]->GetEnvironmentCloth()->GetWorldEndPosition(ropeLength));
		m_IsControlled[ropeIndex] = true;
		m_InitialControlledMatrix[ropeIndex].Set(m_ControlledMatrix[ropeIndex]);
		m_PrevControlledMatrix[ropeIndex].Set(m_ControlledMatrix[ropeIndex]);
// 		if (!m_Coiled[ropeIndex])
// 		{
// 			m_Rope[ropeIndex]->GetEnvironmentCloth()->ControlVertex(vertexIndex,m_ControlledMatrix[ropeIndex].d);
// 		}
// 		else
// 		{
// 			m_Rope[ropeIndex]->GetEnvironmentCloth()->ControlVertices(m_VertexIndexList,6,m_ControlledMatrix[ropeIndex].d);
// 		}
	}

	// PURPOSE: Add a hanging attached object at the middle vertex of a piece of rope.
	int AddPendantInMiddle (int ropeIndex, float ropeLength, phInst* pendant)
	{
		int pendantVertex = m_Rope[ropeIndex]->GetEnvironmentCloth()->GetNumRopeVertices()/2;
		return AddPendant(ropeIndex,ropeLength,pendant,pendantVertex);
	}

	// PURPOSE: Add a hanging attached object to the end of a piece of rope, or to the specified vertex.
	int AddPendant (int ropeIndex, float /*ropeLength*/, phInst* /*pendant*/, int pendantVertex=BAD_INDEX, int otherConstrainedVertex=0)
	{
		// Get the pendant index.
		int pendantIndex = m_NumPendants[ropeIndex];
		Assert(pendantIndex<MAX_NUM_PENDANTS);
		Assert(m_Rope[ropeIndex]);
		phEnvClothVerletBehavior& rope = *m_Rope[ropeIndex];
		int numRopeSegments = rope.GetEnvironmentCloth()->GetNumRopeVertices()-1;
		if (pendantVertex==BAD_INDEX)
		{
			// The pendant's attachment vertex was not specified, so make it the last one.
			pendantVertex = numRopeSegments;
		}

		// Set the pendant in the rope.
		int ropePendantIndex = -1; //rope.GetEnvironmentCloth()->SetPendant(pendant,pendantVertex);

		// Find the constraint's position and length (to the rope's first vertex, or to the previous pendant if this is not the first).
		// The cloth constraint computes its own length, but this is set here to work in case the cloth constraint isn't active.
// 		float constraintLength = 1.07f*ropeLength;
// 		phInst* prevPendant = NULL;
		Vec3V constraintPosition;
		if (pendantIndex==0)
		{
			// This is the only pendant so far.
			if (pendantVertex!=0)
			{
				// Make the other end of the constraint the rope's first vertex.
// 				constraintPosition = VECTOR3_TO_VEC3V(rope.GetEnvironmentCloth()->GetVertexWorldPosition(0));
// 				constraintLength *= pendantVertex/(float)numRopeSegments;
			}
		}
		else
		{
			// This is not the first pendant, so make the other end of the constraint the previous pendant.
// 			int prevPendantIndex = pendantIndex-1;
// 			prevPendant = rope.GetEnvironmentCloth()->GetPendant(prevPendantIndex);
// 			Assert(prevPendant);
// 			int prevPendantVertex = rope.GetEnvironmentCloth()->GetPendantVertex(prevPendantIndex);
// 			constraintPosition = VECTOR3_TO_VEC3V(rope.GetEnvironmentCloth()->GetVertexWorldPosition(prevPendantVertex));
// 			constraintLength *= (pendantVertex-rope.GetEnvironmentCloth()->GetPendantVertex(prevPendantIndex))/(float)numRopeSegments;
		}

		phClothConstraint* ropeConstraint = NULL;
		if (pendantIndex!=0 || pendantVertex!=otherConstrainedVertex)
		{
			// Set the pendant's constraint.
			m_PendantConstraint[ropeIndex][pendantIndex] = rage_new phClothConstraint;
			ropeConstraint = m_PendantConstraint[ropeIndex][pendantIndex];
			ropeConstraint->SetRope(m_Rope[ropeIndex]->GetEnvironmentCloth()->GetCloth());

// 			phConstraintMgr* constraintMgr = m_DemoWorld[ropeIndex]->GetSimulator()->GetConstraintMgr();
// 			const int component = 0;
// 			const bool addInitialSep = false;
			if (pendantIndex==0)
			{
// 				// This is the first pendant on this rope.
// 				Vector3 firstVertexPosition(rope.GetEnvironmentCloth()->GetVertexWorldPosition(otherConstrainedVertex));
// 				Vector3 vertexWorldPosition(rope.GetEnvironmentCloth()->GetVertexWorldPosition(pendantVertex));
// 				constraintMgr->AttachObjectToWorld('Sro1', firstVertexPosition,vertexWorldPosition,*pendant,component,constraintLength,addInitialSep,ropeConstraint);
// 				ropeConstraint->SetVertices(pendantVertex,otherConstrainedVertex);
			}
			else
			{
// 				// This is not the first pendant on this rope, so attach it to the previous pendant.
// 				Assert(prevPendant);
// 				Vector3 vertexWorldPosition(rope.GetEnvironmentCloth()->GetVertexWorldPosition(pendantVertex));
// 				constraintMgr->AttachObjects('Sro2', VEC3V_TO_VECTOR3(constraintPosition),vertexWorldPosition,*prevPendant,*pendant,component,component,constraintLength,addInitialSep,ropeConstraint);
// 				ropeConstraint->SetVertices(otherConstrainedVertex,pendantVertex);
			}
		}

		DebugAssert(ropePendantIndex==m_NumPendants[ropeIndex]);
		m_NumPendants[ropeIndex]++;
		return ropePendantIndex;
	}

protected:

	phEnvClothVerletBehavior* m_Rope[MAX_NUM_ROPES];
	phSwingingObject* m_RopeObject[MAX_NUM_ROPES];
	phDemoWorld* m_DemoWorld[MAX_NUM_ROPES];
	Matrix34 m_ControlledMatrix[MAX_NUM_ROPES],m_PrevControlledMatrix[MAX_NUM_ROPES],m_InitialControlledMatrix[MAX_NUM_ROPES];
	phClothConstraint* m_PendantConstraint[MAX_NUM_ROPES][MAX_NUM_PENDANTS];
	int m_NumPendants[MAX_NUM_ROPES];

	int m_VertexIndexList[6];
	bool m_Coiled[MAX_NUM_ROPES];

	// PURPOSE: constraints that attach rope pieces to the world with gizmo control
	// NOTES:	These are not for rope collisions - a rope that has a NULL m_RopeConstraint can still create rope constraints when it collides.
	phConstraint* m_RopeConstraint[MAX_NUM_ROPES];

	phEnvClothVerletBehavior* m_RopeInRing;
	Vec3V m_RingPosition;

	bool m_IsControlled[MAX_NUM_ROPES];
	int m_NumRopes;
};

} // namespace ragesamples


int Main()
{
	{
		ragesamples::RopeManager sampleRope;
		sampleRope.Init();
		sampleRope.UpdateLoop();
		sampleRope.Shutdown();
	}

    return 0;
}
/*
if (collideEdges && 0)
{
	// Look for collisions between the cloth (rope) edges and the tapered capsule bound.
	ScalarV vertexRadius = ScalarVFromF32(vertRad);
	Vec3V unitAxis = boundPose.GetCol1();
	Vec3V boundCentroid = bound->GetWorldCentroid(boundPose);
	ScalarV capsuleLength = ScalarVFromF32(bound->GetLength());
	ScalarV capsuleRadius1 = bound->GetRadius1();
	ScalarV capsuleRadius2 = bound->GetRadius2();
	ScalarV largestCapsuleRad = (IsGreaterThanAll(capsuleRadius1,capsuleRadius2) ? capsuleRadius1 : capsuleRadius2);
	Vec3V end0 = SubtractScaled(boundCentroid,unitAxis,Scale(ScalarV(V_HALF),capsuleLength));
	for (int edgeIndex=0; edgeIndex<m_NumEdges; edgeIndex++)
	{
		const phEdgeData& edge = m_EdgeData[edgeIndex];
		ScalarV edgeLength = Sqrt(ScalarVFromF32(edge.m_EdgeLength2));
		int startVertIndex = edge.m_vertIndices[0];
		int endVertIndex = edge.m_vertIndices[1];
		if (startVertIndex<nPins && endVertIndex<nPins)
		{
			// Both vertices on this edge are pinned, so don't do the collision.
			continue;
		}

		Vec3V edgeStart = VECTOR3_TO_VEC3V(m_ClothData.GetVertexPosition(startVertIndex));
		Vec3V edgeEnd = VECTOR3_TO_VEC3V(m_ClothData.GetVertexPosition(endVertIndex));
		Vec3V edgePosition = Average(edgeStart,edgeEnd);
		ScalarV edgeHalfLength = Scale(edgeLength,ScalarV(V_HALF));
		ScalarV boundRadius = ScalarVFromF32(boundRad);
		ScalarV radiusSum = Add(edgeHalfLength,boundRadius);
		ScalarV squaredRadiusSum = Scale(radiusSum,radiusSum);
		if (IsLessThanAll(DistSquared(edgePosition,boundCentroid),squaredRadiusSum))
		{
			// The cloth edge's bounding sphere intersects the tapered capsule's bounding sphere.
			// Find the relative displacement.
			Vec3V relPos = Subtract(edgePosition,boundCentroid);
			Vec3V edgePrevStart = VECTOR3_TO_VEC3V(m_ClothData.GetVertexPrevPosition(startVertIndex));
			Vec3V edgePrevEnd = VECTOR3_TO_VEC3V(m_ClothData.GetVertexPrevPosition(endVertIndex));
			Vec3V edgePrevPosition = Average(edgePrevStart,edgePrevEnd);
			Mat34V capsulePose = boundPose;
			capsulePose.SetCol3(boundCentroid);
			Vec3V boundPrevCentroid = bound->GetWorldCentroid(boundPrevPose);
			Mat34V capsulePrevPose = boundPrevPose;
			capsulePrevPose.SetCol3(boundPrevCentroid);
			Vector3 relDispOld;
			phImpactSet::GetRelDisp(MAT34V_TO_MATRIX34(capsulePose),MAT34V_TO_MATRIX34(capsulePrevPose),VEC3V_TO_VECTOR3(edgePrevPosition),VEC3V_TO_VECTOR3(relPos),relDispOld);
			Vec3V relDisp = VECTOR3_TO_VEC3V(relDispOld);

			// Find the intersection of the edge with the tapered capsule.
			Vec3V edgeStartToEnd = Subtract(edgeEnd,edgeStart);
			Vec3V capsuleAxis = Scale(unitAxis,capsuleLength);
			float edgeTf,capsuleTf;
			geomTValues::FindTValuesSegToSeg(VEC3V_TO_VECTOR3(edgeStart),VEC3V_TO_VECTOR3(edgeStartToEnd),VEC3V_TO_VECTOR3(end0),VEC3V_TO_VECTOR3(capsuleAxis),&edgeTf,&capsuleTf);
			ScalarV edgeT = Clamp(ScalarVFromF32(edgeTf),ScalarV(V_ZERO),ScalarV(V_ONE));
			ScalarV capsuleT = Clamp(ScalarVFromF32(capsuleTf),ScalarV(V_ZERO),ScalarV(V_ONE));
			edgePosition = AddScaled(edgeStart,edgeStartToEnd,edgeT);
			Vec3V capsulePosition = AddScaled(end0,capsuleAxis,capsuleT);
			ScalarV distanceSquared = DistSquared(edgePosition,capsulePosition);
			radiusSum = Add(vertexRadius,largestCapsuleRad);
			squaredRadiusSum = Scale(radiusSum,radiusSum);
			if (IsLessThanAll(distanceSquared,squaredRadiusSum))
			{
				// The cloth edge intersects the capsule with the larger of the two tapered capsule radii.
				ScalarV distance = SqrtSafe(distanceSquared);
				ScalarV cosine = InvScale(Subtract(capsuleRadius2,capsuleRadius1),capsuleLength);
				ScalarV extraDistance = Scale(distance,InvScale(cosine,SqrtSafe(Subtract(ScalarV(V_ONE),Scale(cosine,cosine)))));
				capsulePosition = AddScaled(capsulePosition,unitAxis,extraDistance);
				distanceSquared = DistSquared(edgePosition,capsulePosition);
				ScalarV taperedRadius = Add(capsuleRadius1,Scale(Subtract(capsuleRadius2,capsuleRadius1),Add(capsuleT,InvScale(extraDistance,capsuleLength))));
				radiusSum = Add(taperedRadius,vertexRadius);
				if (IsLessThanAll(distanceSquared,Scale(radiusSum,radiusSum)))
				{
					// The cloth edge intersects the tapered capsule.
					ScalarV depth = Max(Subtract(radiusSum,Sqrt(distanceSquared)),ScalarV(V_ZERO));
					Vec3V normal = Scale(Subtract(edgePosition,capsulePosition),Invert(Sqrt(distanceSquared)));
					if (IsGreaterThanAll(MagSquared(relDisp),squaredRadiusSum) && IsGreaterThanAll(Dot(normal,relDisp),ScalarV(V_ZERO)))
					{
						// The cloth edge went more than half way through the capsule, so push it out the other side.
						depth = Subtract(Scale(radiusSum,ScalarV(V_TWO)),depth);
						normal = Negate(normal);
					}

					ScalarV weight1 = ScalarV(V_ONE);//(endVertIndex<nPins ? ScalarV(V_ZERO) : edgeT);
					ScalarV weight0 = ScalarV(V_ONE);//(startVertIndex<nPins ? ScalarV(V_ZERO) : Subtract(ScalarV(V_ONE),weight1));
					ScalarV depth0 = Scale(depth,weight0);
					ScalarV depth1 = Scale(depth,weight1);
					ApplyCollisionBasic(normal,depth,*(Vec3V*)&clothVertexPositions[startVertIndex],*(Vec3V*)&prevVertexPositions[startVertIndex],doFriction,moveWithBodyVelocity);
					ApplyCollisionBasic(normal,depth,*(Vec3V*)&clothVertexPositions[endVertIndex],*(Vec3V*)&prevVertexPositions[endVertIndex],doFriction,moveWithBodyVelocity);
					if (m_IsRope)
					{
						SetVertexColliding(startVertIndex,normal,saveCollisions,otherInstance,otherCollider);
						SetVertexColliding(endVertIndex,normal,saveCollisions,otherInstance,otherCollider);
					}
				}
			}
		}
	}
}*/