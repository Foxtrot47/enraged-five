// 
// sample_rope/sample_bending.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "cloth/environmentcloth.h"
#include "gizmo/manager.h"
#include "gizmo/translation.h"
#include "grcore/im.h"
#include "grmodel/setup.h"
#include "grrope/grroperender.h"
#include "pharticulated/articulatedcollider.h"
#include "pharticulated/bodypart.h"
#include "phbound/boundcomposite.h"
#include "phcore/phmath.h"
#include "pheffects/rope.h"
#include "physics/constraint.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "sample_physics/demoobject.h"
#include "sample_physics/sample_physics.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "grmodel/shaderfx.h"

// RAY - test
//#include "phbound/boundsphere.h"
//#include "curve/curvecatrom.h"
//#include "grmodel/shader.h"
//#include "grmodel/shadergroup.h"
//#include "grcore/vertexbuffer.h"
//#include "grcore/indexbuffer.h"
//#include "grcore/device.h"
//#include "grmodel/modelfactory.h"
//#include "math/amath.h"

// PURPOSE: Define the maximum number of foliage pieces in all the physical worlds in this sample.
//			A game-specific manager is required to control rope allocation and reset. Rope update
//			is done through phInstBehavior, so it is automatic for rope placed in the physics level.
#define	MAX_NUM_PIECES		32

PARAM(file, "Reserved for future expansion, not currently hooked up");


namespace rage {
	EXT_PFD_DECLARE_GROUP(DrawCloth);
	EXT_PFD_DECLARE_ITEM(Models);

#if __PFDRAW
	extern void DrawClothWireframe (const phClothData& clothData, const phVerletCloth& cloth, int lodIndex, const Vector3* vertexNormals);
#endif

#if (__BANK && !__RESOURCECOMPILER)
	datCallback	g_ClothManagerCallback = NullCB;
#endif

}


namespace ragesamples {

using namespace rage;


////////////////////////////////////////////////////////////////////////////////////////////////
// TITLE: sample_foliage - Using the rope simulation for flexible objects approximated by a line
// PURPOSE:
//		This sample shows how to use the rope simulation for flexible objects that are appoximated by a line. sample_rope shows how to
//		use the rope simulation for rope.

class FoliageManager : public physicsSampleManager
{
public:

	phDemoWorld& MakeRopeDemoWorld (const char* worldName, const char* terrainName=NULL)
	{
		const Vector3 terrainPosition = ORIGIN;
		int maxOctreeNodes = 1000;
		int maxActiveObjects = 500;
		int maxObjects = 500;
		const Vector3 worldMin = Vector3(-999.0f,-999.0f,-999.0f);
		const Vector3 worldMax = Vector3(999.0f,999.0f,999.0f);
		int maxInstBehaviors = MAX_NUM_PIECES;
		return MakeNewDemoWorld(worldName,terrainName,terrainPosition,maxOctreeNodes,maxActiveObjects,maxObjects,worldMin,worldMax,maxInstBehaviors);
	}

	virtual void InitClient ()
	{
		// STEP #1. Initialize the base physics sample class.
		physicsSampleManager::InitClient();

		grmShaderFx::SetForcedTechniqueGroupId(0);
	#if __PFDRAW
		// Turn off physics drawing (after the base class InitClient turned it on).
		GetRageProfileDraw().SetEnabled(false);
		//PFD_GROUP_ENABLE(DrawCloth,true);
		PFD_Models.SetEnabled(true);
	#endif



		// STEP #1.5: set up the rendering of the curved model use the the RopeBasedModel singleto
		m_leafModel = rage_new rmcDrawable();		// load our model for rendering ropes

		ASSET.PushFolder("sample_rope\\leaf\\");
		m_leafModel->Load("leaf");
		ASSET.PopFolder();

		m_chainModel= rage_new rmcDrawable();		// load our model for rendering ropes

		ASSET.PushFolder("sample_rope\\chain\\");
		m_chainModel->Load("chain");
		ASSET.PopFolder();

		m_model = m_chainModel;

		// STEP #2. Initialize parameters that manage the collection of foliage pieces.
		// The different pieces are not all in the same physical world.
		m_NumPieces = 0;
		int pieceIndex;
		for (pieceIndex=0; pieceIndex<MAX_NUM_PIECES; pieceIndex++)
		{
			m_BendingObject[pieceIndex] = NULL;
			m_Bender[pieceIndex] = NULL;
			m_DemoWorld[pieceIndex] = NULL;
		}

		// STEP #3. Create all the sets of interacting objects, with a sample page for each set.
		//-- "+" and "-" keys control paging through the following set of sample pages.
		// CreateFoliagePiece in each section initializes each piece and places it in the physical world.
		Vector3 position,rotation,frequency,amplitude,phaseOffset;
		float length,radius;//,endRadius;
		bool alignBottom = false;

		///////////////////////////////////////////////
		// Page 0: drooping plants with rolling spheres
		phDemoWorld& forestWorld = MakeRopeDemoWorld("forest");
		SetModelToUse(true);
		length = 2.0f;
		radius = 0.1f;
		position.Set(-1.75f,radius,-1.0f);
		rotation.Set(0.0f,0.0f,0.4f*PI);
		CreateFoliagePiece(forestWorld,position,rotation,length,radius);
		position.Set(-1.7f,radius,0.0f);
		rotation.Set(0.0f,0.0f,0.3f*PI);
		CreateFoliagePiece(forestWorld,position,rotation,length,radius);
		position.Set(-1.6f,radius,1.0f);
		rotation.Set(0.0f,0.0f,0.35f*PI);
		CreateFoliagePiece(forestWorld,position,rotation,length,radius);
		position.Set(1.75f,radius,0.5f);
		rotation.Set(0.0f,0.0f,-1.35f*PI);
		CreateFoliagePiece(forestWorld,position,rotation,length,radius);
		position.Set(1.7f,radius,-0.5f);
		rotation.Set(0.0f,0.0f,-1.4f*PI);
		CreateFoliagePiece(forestWorld,position,rotation,length,radius);
		position.Set(0.0f,0.0f,-10.0f);
		rotation.Set(0.14f,0.0f,0.0f);
		forestWorld.CreateFixedObject("crate_flat",position,alignBottom,rotation);
		position.Set(-0.6f,3.0f,-13.5f);
		rotation.Zero();
		forestWorld.CreateObject("sphere_056",position,alignBottom,rotation);
		position.Set(0.6f,3.0f,-13.0f);
		forestWorld.CreateObject("sphere_056",position,alignBottom,rotation);

		//////////////////
		// Page X: fishing
/*		phDemoWorld& fishingWorld = MakeRopeDemoWorld("fishing");
		position.Set(0.0f,2.0f,0.0f);
		rotation.Set(0.0f,0.0f,-1.4f*PI);
		length = 2.0f;
		radius = 0.1f;
		endRadius = 0.02f;
		SetModelToUse(false);
		CreateFishingPole(fishingWorld,position,rotation);//,length,radius,endRadius);
*/
		///////////////////////////////
		// Page X: user-controlled whip
/*		phDemoWorld& whipWorld = MakeRopeDemoWorld("whip");
		position.Set(0.0f,1.2f,0.0f);
		rotation.Set(0.0f,0.0f,-0.6f*PI);
		length = 4.0f;
		radius = 0.1f;
		endRadius = 0.02f;
		SetModelToUse(false);
		CreateWhip(whipWorld,position,rotation,length,radius,endRadius);
*/
		// STEP #4. Set and activate the initial page, from the command line parameter "startingDemo" with the first page as default.
		m_Demos.SetCurrentDemo(0);
		m_Demos.GetCurrentWorld()->Activate();
	}

	virtual void InitCamera ()
	{
		// STEP #5. Set the camera position and target for a good view of each world.
		Vector3 lookFrom(0.0f,6.0f,10.0f);
		Vector3 lookTo(0.0f,0.0f,0.0f);
		InitSampleCamera(lookFrom,lookTo);
	}

	virtual void Update ()
	{
		physicsSampleManager::Update();

		// STEP #6. Update the controls. The rope class (phRope) is derived from phInstBehavior, and it is updated when the physics
		// simulator updates all of the instance behaviors.
		for (int pieceIndex=0; pieceIndex<m_NumPieces; pieceIndex++)
		{
			// See if this rope piece is in the currently active physical world.
			if (m_Demos.GetCurrentWorld()==m_DemoWorld[pieceIndex])
			{
				// This rope piece is in the currently active physical world.
				Assert(m_Bender[pieceIndex]);
//				phEnvClothVerletBehavior& bender = *m_Bender[pieceIndex];

				// See if this rope piece is controlled by a gizmo or a constraint.
				if (m_IsControlled[pieceIndex])
				{
					if (m_Constraint[pieceIndex])
					{
						// This piece is controlled by a constraint, so copy the constraint's new position (gizmo positions update automatically).
						m_ControlledMatrix[pieceIndex].d.Set(m_Constraint[pieceIndex]->GetWorldPosition());
					}

					// Constrain the rope's first vertex to the controlled position and velocity.
//					const int vertexIndex = 0;
//					bender.GetEnvironmentCloth()->ControlVertex(vertexIndex,m_ControlledMatrix[pieceIndex].d);
					m_PrevControlledMatrix[pieceIndex].Set(m_ControlledMatrix[pieceIndex]);
				}

// 			#if __PFDRAW
// 				DrawClothWireframe(bender.GetCloth()->GetClothData(),*bender.GetCloth(), 0);
// 			#endif

			}
		}
	}

	virtual void Reset ()
	{
		// STEP #7. Reset the controls.
		for (int pieceIndex=0; pieceIndex<m_NumPieces; pieceIndex++)
		{
			if (m_Demos.GetCurrentWorld()==m_DemoWorld[pieceIndex])
			{

				m_Bender[pieceIndex]->Reset();
				m_Bender[pieceIndex]->GetCloth()->Reset();
				if (m_IsControlled[pieceIndex])
				{
					m_ControlledMatrix[pieceIndex].Set(m_InitialControlledMatrix[pieceIndex]);
					m_PrevControlledMatrix[pieceIndex].Set(m_ControlledMatrix[pieceIndex]);
				}
			}
		}
	}

	virtual grcSetup& AllocateSetup ()
	{
		return *(rage_new grmSetup());
	}

	virtual void ShutdownClient ()
	{
		for (int pieceIndex=0; pieceIndex<MAX_NUM_PIECES; pieceIndex++)
		{
			// Don't delete the bending objects, because the demo worlds delete them.
			if (m_BendingObject[pieceIndex])
			{
				phInst* instance = m_BendingObject[pieceIndex]->GetPhysInst();
				int levelIndex = instance->GetLevelIndex();
				if (m_DemoWorld[pieceIndex]->GetPhLevel()->IsInLevel(levelIndex))
				{
					// Remove the instance behavior from the simulator.
					m_DemoWorld[pieceIndex]->GetSimulator()->RemoveInstBehavior(*m_Bender[pieceIndex]->GetEnvironmentCloth()->GetBehavior());

					// Remove the rope instance from the physics level and simulator.
					m_DemoWorld[pieceIndex]->GetSimulator()->DeleteObject(levelIndex);
				}

				// Remove the rope instance from the demo object, so that the cloth can delete it.
				m_BendingObject[pieceIndex]->SetPhysInst(NULL);

				// Delete the rope cloth type.
				delete m_Bender[pieceIndex]->GetEnvironmentCloth();
			}
		}

		physicsSampleManager::ShutdownClient();
	}
	void SetModelToUse( bool  isLeaf )
	{
		if ( isLeaf )
		{
			m_model = m_leafModel;
		}
		else
		{
			m_model = m_chainModel;
		}
	}

	int CreateBender (phDemoWorld& demoWorld, const Vector3& endPosition, const Vector3& rotation, const char* boundName=NULL, float length=2.0f, float startRadius=0.02f, float endRadius=0.01f, int numEdges=12,
						float density=1.0f, float stretchStrength=200.0f, float stretchDamping=20.0f, float airFriction=0.1f, float bendStrength=60.0f, float bendDamping=6.0f, bool held=true)
	{
		Assert(m_NumPieces<MAX_NUM_PIECES);

		// Create a rope and its matrix pose.
		environmentCloth& newBender = *rage_new environmentCloth;
		Matrix34 ropePose(CreateRotatedMatrix(endPosition,rotation));

		// Set the rope vertex positions.
		int numVertices = numEdges+1;
		Vector3* vertices = Alloca(Vector3,numVertices);
		Vector3 vertexPosition(ORIGIN);
		float edgeLength = length/(float)numEdges;
		for (int vertexIndex=0; vertexIndex<numVertices; vertexIndex++)
		{
			ropePose.Transform(vertexPosition,vertices[vertexIndex]);
			vertexPosition.x += edgeLength;
		}

		// Initialize one-dimensional cloth.
		newBender.AllocateClothController();
		phVerletCloth* cloth = newBender.GetCloth();
		atArray<float> bendStrengthList;
		bendStrengthList.Resize(numVertices);
		for (int i=0; i<numVertices; i++) bendStrengthList[i]=bendStrength;
//		cloth->InitStrand(vertices,numVertices,(bendStrength>0.0f ? &bendStrengthList[0] : NULL));
		newBender.CreatePhysics();
		cloth->InitCollisionData();
//		cloth->InitSimpleData();
		cloth->InitEdgeData();
		m_Bender[m_NumPieces] = static_cast<phEnvClothVerletBehavior*>(newBender.GetBehavior());
		if (held)
		{
			cloth->PinVertex(0);
			cloth->PinVertex(1);
		}

		cloth->SetEdgeWeightsFromPinning();

		stretchStrength += 0.0f;
		stretchDamping += 0.0f;
		airFriction += 0.0f;
		bendStrength += 0.0f;
		bendDamping += 0.0f;
		density += 0.0f;
		endRadius += 0.0f;

		phBoundComposite* compositeBound = NULL;
		if (boundName)
		{
			// Load the specified bound file.
			phBound* bound = phBound::Load(boundName);
			if (bound->GetType()==phBound::COMPOSITE)
			{
				compositeBound = static_cast<phBoundComposite*>(bound);
			}
		}

		if (!compositeBound)
		{
			// No bound file was given, or it failed to load, so create a composite bound for the rope.
			compositeBound = &newBender.CreateRopeBound(numEdges,length,startRadius,endRadius);
		}

		phArchetypePhys& archetype = *(rage_new phArchetypePhys);
		archetype.SetBound(compositeBound);

		m_BendingObject[m_NumPieces] = rage_new phSwingingObject;

		phSwingingObject& benderObject = *m_BendingObject[m_NumPieces];

		Matrix34 matrix(CreateRotatedMatrix(endPosition,rotation));

		matrix.d.AddScaled(matrix.a,0.5f*length);
		benderObject.SetInst(const_cast<phInst*>(newBender.GetInstance()));
		benderObject.InitPhys(&archetype,matrix);

		const int bucket = 0;
		benderObject.SetDrawable(rage_new rmcRopeBasedModelDrawable(m_model,m_Bender[m_NumPieces]->GetEnvironmentCloth()->GetCloth(),bucket));

		demoWorld.AddInactiveObject(&benderObject);
		demoWorld.AddUpdateObject(&benderObject);
		demoWorld.GetSimulator()->AddInstBehavior(*newBender.GetBehavior());
		m_IsControlled[m_NumPieces] = false;
		m_DemoWorld[m_NumPieces] = &demoWorld;
		m_Constraint[m_NumPieces] = NULL;
		return (m_NumPieces++);
	}

	// PURPOSE: Create a piece of rope and add it to the given physics demo world.
	int CreateFoliagePiece (phDemoWorld& demoWorld, const Vector3& position, const Vector3& rotation, float length=2.0f, float radius=0.02f, int numEdges=4)
	{
		const float density = 1.0f;
		const float stretchStrength = 200.0f;
		const float stretchDamping = 20.0f;
		const float airFriction = 0.1f;
		const float bendStrength = 1.0f;
		const float bendDamping = 1.0f;
		return CreateBender(demoWorld,position,rotation,NULL,length,radius,radius,numEdges,density,stretchStrength,stretchDamping,airFriction,bendStrength,bendDamping);
	}

	// PURPOSE: Create a piece of rope as a whip and add it to the given physics demo world.
	int CreateWhip (phDemoWorld& demoWorld, const Vector3& position, const Vector3& rotation, float length=2.0f, float startRadius=0.02f, float endRadius=0.01f, int numEdges=12)
	{
		const float density = 1.0f;
		const float stretchStrength = 200.0f;
		const float stretchDamping = 20.0f;
		const float airFriction = 0.1f;
		const float bendStrength = 60.0f;
		const float bendDamping = 6.0f;
		return CreateBender(demoWorld,position,rotation,NULL,length,startRadius,endRadius,numEdges,density,stretchStrength,stretchDamping,airFriction,bendStrength,bendDamping);
	}

	// PURPOSE: Create a piece of rope as a whip and add it to the given physics demo world.
	void CreateFishingPole (phDemoWorld& demoWorld, const Vector3& position, const Vector3& rotation, float length=2.0f, float startRadius=0.02f, float endRadius=0.02f, int numEdges=4)
	{
		// Create a fishing pole.
		float density = 1.0f;
		float stretchStrength = 200.0f;
		float stretchDamping = 20.0f;
		float airFriction = 0.1f;
		float bendStrength = 1.0f;
		float bendDamping = 1.0f;
		int poleIndex = CreateBender(demoWorld,position,rotation,NULL,length,startRadius,endRadius,numEdges,density,stretchStrength,stretchDamping,airFriction,bendStrength,bendDamping);

		// Create a fishing line, starting at the end of the pole.
		Vector3 poleEndPosition(length,0.0f,0.0f);
		Matrix34 polePose(CreateRotatedMatrix(position,rotation));
		polePose.Transform(poleEndPosition);
		Vector3 lineRotation(0.0f,0.0f,PI);
		float lineLength = 3.0f;
		density = 1.0f;
		stretchStrength = 200.0f;
		stretchDamping = 20.0f;
		airFriction = 0.1f;
		bendStrength = 0.0f;
		bendDamping = 0.0f;
		bool held = false;
		int lineIndex = CreateBender(demoWorld,poleEndPosition,lineRotation,NULL,lineLength,endRadius,endRadius,numEdges,density,stretchStrength,stretchDamping,airFriction,bendStrength,bendDamping,held);

		// Constrain the line to the pole.
		phInst* poleInstance = m_BendingObject[poleIndex]->GetPhysInst();
		phInst* lineInstance = m_BendingObject[lineIndex]->GetPhysInst();
		int poleComponent = numEdges-1;
		int lineComponent = 0;
		lineIndex += 0;
		poleIndex += 0;
		m_Constraint[lineIndex] = demoWorld.GetSimulator()->GetConstraintMgr()->AttachObjects('Sbnd', poleEndPosition,lineInstance,poleInstance,lineComponent,poleComponent);
	}

	// PURPOSE: Add a physical constraint to a piece of rope that swings it around with sine waves.
	void AddSwingingConstraint (int pieceIndex, const Vector3& frequency=Vector3(4.0f,3.0f,2.0f), const Vector3& amplitude=Vector3(1.0f,1.0f,1.0f), const Vector3& phaseOffset=ORIGIN)
	{
		Assert(!m_IsControlled[pieceIndex]);
		phConstraintMgr* constraintMgr = m_DemoWorld[pieceIndex]->GetSimulator()->GetConstraintMgr();
		const Vector3 ropePosition = RCC_VECTOR3(m_BendingObject[pieceIndex]->GetPhysInst()->GetPosition());
		m_Constraint[pieceIndex] = m_BendingObject[pieceIndex]->SetMovingConstraint(ropePosition,constraintMgr);
		m_BendingObject[pieceIndex]->SetFrequency(frequency);
		m_BendingObject[pieceIndex]->SetAmplitude(amplitude);
		m_BendingObject[pieceIndex]->SetPhaseOffset(phaseOffset);
		m_ControlledMatrix[pieceIndex].Identity3x3();
		m_ControlledMatrix[pieceIndex].d.Set(m_Constraint[pieceIndex]->GetWorldPosition());
		m_IsControlled[pieceIndex] = true;
		m_InitialControlledMatrix[pieceIndex].Set(m_ControlledMatrix[pieceIndex]);
		m_PrevControlledMatrix[pieceIndex].Set(m_ControlledMatrix[pieceIndex]);
//		const int vertexIndex = 0;
//		m_Bender[pieceIndex]->GetEnvironmentCloth()->ControlVertex(vertexIndex,m_ControlledMatrix[pieceIndex].d);
	}

	// PURPOSE: Add user control to the position of the end of a piece of foliage with a gizmo, providing mouse control of position.
	void AddGizmo (int pieceIndex)
	{
		Assert(!m_IsControlled[pieceIndex]);
		m_DemoWorld[pieceIndex]->AddTranslationGizmo(m_ControlledMatrix[pieceIndex]);
		m_ControlledMatrix[pieceIndex].Identity3x3();
		m_ControlledMatrix[pieceIndex].d.Set(RCC_VECTOR3(m_BendingObject[pieceIndex]->GetPhysInst()->GetPosition()));
		m_IsControlled[pieceIndex] = true;
		m_InitialControlledMatrix[pieceIndex].Set(m_ControlledMatrix[pieceIndex]);
		m_PrevControlledMatrix[pieceIndex].Set(m_ControlledMatrix[pieceIndex]);
//		const int vertexIndex = 0;
//		m_Bender[pieceIndex]->GetEnvironmentCloth()->ControlVertex(vertexIndex,m_ControlledMatrix[pieceIndex].d);
	}


protected:
	phSwingingObject* m_BendingObject[MAX_NUM_PIECES];
	phEnvClothVerletBehavior* m_Bender[MAX_NUM_PIECES];
	phDemoWorld* m_DemoWorld[MAX_NUM_PIECES];
	Matrix34 m_ControlledMatrix[MAX_NUM_PIECES],m_PrevControlledMatrix[MAX_NUM_PIECES],m_InitialControlledMatrix[MAX_NUM_PIECES];

	phConstraint* m_Constraint[MAX_NUM_PIECES];
	bool m_IsControlled[MAX_NUM_PIECES];
	int m_NumPieces;

	// leaf model to use
	rmcDrawable*		m_leafModel;
	rmcDrawable*		m_chainModel;
	rmcDrawable*		m_model;

};

} // namespace ragesamples


int Main()
{
	{
		ragesamples::FoliageManager sampleFoliage;
		sampleFoliage.Init();
		sampleFoliage.UpdateLoop();
		sampleFoliage.Shutdown();
	}

    return 0;
}
