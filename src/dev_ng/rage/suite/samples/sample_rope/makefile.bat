set SAMPLE_LIBS=%RAGE_SAMPLE_GRCORE_LIBS% sample_rmcore sample_physics
set BASE_LIBS=devcam spatialdata shaders
set LIBS=%RAGE_CORE_LIBS% %RAGE_PH_LIBS% %SAMPLE_LIBS% %BASE_LIBS% %RAGE_GFX_LIBS%  grrope cloth fragment breakableglass phglass event cranimation
set TESTERS=sample_rope sample_bending
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples %RAGE_DIR%\suite\samples %RAGE_DIR%\suite\src
