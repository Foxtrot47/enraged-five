// 
// /sample_grass.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_rmcore/sample_rmcore.h"
#include "grass/manager.h"
#include "grass/field.h"
#include "grass/patch.h"
#include "grass/wind.h"
#include "grass/fieldfactory.h"
#include "grass/staticparticles.h"

#include "spatialdata/shaft.h"
#include "system/main.h"
#include "system/param.h"
#include "grcore/im.h"
#include "grcore/effect.h"
#include "bank/bkmgr.h"
#include "bank/bank.h"

#include "system/xtl.h"
#if __WIN32
#include <d3d9.h>
#endif
#include "grcore/device.h"


//////////////////////////////////////////////////////////////////////
// PURPOSE
//	 help function to make adding load / Save buttons easier
//	Assumes that filename is statically created.
//
#if __BANK
template<class T>
inline void WidgetsAddWithFileIO( bkBank& bank, T& object , const char* fileName )
{
	bank.AddButton("Load",datCallback(MFA1(T::Load),&object, (void*)fileName ));
	bank.AddButton("Save",datCallback(MFA1(T::Save),&object, (void*)fileName ));
	object.AddWidgets( bank );
}
#endif


#define WIND_FIELD_RESOLUTION	64

PARAM(file,		"[sample_grass] unused");
PARAM(particlelist,		"[sample_grass] unused");

#if !__XENON
// Use a larger field width because we don't yet support quads or point sprites in the code
#define GRID_WIDTH		6
#else
#define GRID_WIDTH		4
#endif
#define NUM_FIELDS		(GRID_WIDTH * GRID_WIDTH)

namespace ragesamples
{

class sampleGrass : public rmcSampleManager
{
public:
	sampleGrass() : m_StreamingBatchId(0), m_GrassManager(0)
	{
		m_vWindDir.Set(-1.0f, 0.0f, 0.0f);
		m_fWindStr = 1.5f;
		m_nGustChance = 00;
		m_fMinGustStrength = 1.5f;
		m_fMaxGustStrength = 20.0f;
		m_fGustRadius = 1.0f;

		strcpy(m_szGrassLayoutTexture, "$\\sample_grass\\e3demo_grassmap01.dds");
		//strcpy(m_szGrassBillboardTexture, "wheat.dds");
	//	strcpy(m_szGrassBillboardTexture, "thingrass0000.dds");
		strcpy(m_szGrassBillboardTexture, "$\\sample_grass\\grass_atlas.dds");
		m_vExtents.Set(-128.0f, 128.0f, -128.0f, 128.0f);
		m_fGridRes = 1.0f;
		m_vCardSize.Set(3.0f, 3.0f);
		m_vCardSizeVariance.Set(0.5f, 0.5f);

		m_nHorizTexVariants = 8;
		m_nVertTexVariants = 1;

		m_fFadeStart = 500.0f;
		m_fFadeRange = 100.0f;

		m_WindStrength = 0.4f;
		
		if ( m_UseZUp )
		{
			m_WindDirection =  Vector3( 1.0f, 1.0f, 0.0f );
		}
		else
		{
			m_WindDirection =  Vector3( 1.0f, 0.0f, 1.0f );
		}
		grassManager::SetWind( m_WindDirection, m_WindStrength );

		// to check rage version the same as wilderness demo
		pgRscBuilder::AddVersionNumberToExtension();
	}

	virtual void InitClient()
	{
		// Init the grass patch class
		grassPatch::InitClass();

	
		// Init the grass manager
		m_GrassManager = rage_new grassManager();

		grassField::SetupStaticIndexBuffer(NUM_VERTS_PER_PRIMITIVE, 1024);
	
		// Set up the different type shaders
		m_GrassManager->SetPatch("grass", rage_new grassPatchPushable( "rage_grass_pushable") );	// override the default grass  -  "grass_atlas.dds", 8 
		m_GrassManager->SetPatch( "drops", rage_new grassPatchAnimation( "rage_grass_animation") ); // set user defined -  "drop_atlas0.dds", 8 
		m_GrassManager->SetPatch( "rain", rage_new grassPatchMovingAnimation( "rage_grass_rain") ); // "water_drop.dds", 8 

		m_GrassManager->GetLod().useCylinderGrass = __XENON || __PS3;
		m_GrassManager->GetLod().cheapPassOffset = 1;

		PARAM_particlelist.Set("");
		if ( PARAM_particlelist.Get() )
		{
			CreateParticleField( "particle_grass" );
		}
		else
		{
			CreateField();
		}

		m_GrassManager->Load("grass_drawers" );	// load up the games settings for the patches and load there textures.
		m_GrassManager->GetPatch<grassPatchTextured>(atString("grass"))->SetAlphaFades( 70.0f,1.0f/10.0f);
	}

	static const int MaximumBladesPerField = 400;

	/*
		PURPOSE
			Shows how to load up a field list  from a static particle xml file 
	*/
	void CreateParticleField( const char* name )
	{
		atPtr<ParticleList>		particles = ParticleList::Create( name, "grs"); // "grass_5mil.xml");//
		
		if ( particles )	// particles are found.
		{
			for ( Particle* p = particles->begin(); p != particles->end(); p++ )
			{
				// swap z and y 
				std::swap(p->Position.y, p->Position.z );
			}

			FieldList*	 fields = CreateFieldList( particles, MaximumBladesPerField);
			m_GrassManager->Add( fields, m_StreamingBatchId );
		}
	}
	/*
	PURPOSE
		Shows how to load up resourced field list 
	*/
	void CreateResourcedParticleField()
	{
		FieldList*	 fields;
		pgRscBuilder::Load( fields, "particle_grass","#grs", FieldList::RORC_VERSION);
		if ( fields )
		{
			m_GrassManager->Add( fields, m_StreamingBatchId );
		}
	}
	
	/*
	PURPOSE
		Shows how to create a field list from a several textures on the fly
	*/
	void CreateField()
	{
		FieldList* fields = rage_new FieldList;

		fieldFactory::PlacementFieldParams	params;
		params.SetColorPlacementMap( m_szGrassLayoutTexture )
				.SetDiffuseMap( m_szGrassBillboardTexture )
				.SetExtents( m_vExtents )
				.SetGridRes( m_fGridRes )
				.SetCardSize( m_vCardSize )
				.SetCardSizeVariance( m_vCardSizeVariance )
				.SetNumFramesWidth(m_nHorizTexVariants);

		fieldFactory::CreateSubdividedField( *fields, GRID_WIDTH, params );

		fields->SetType("grass");
		m_GrassManager->Add( fields, m_StreamingBatchId );
	}

	void LoadField()
	{
		char fullPresetPath[256];
		memset(fullPresetPath, 0, 256);

#if __BANK
		if( BANKMGR.OpenFile(fullPresetPath, 256, "*.grs", false, "Grass Field (*.grs)") )
		{	
			// Load the field
			CreateParticleField( fullPresetPath );
		}
#endif
	}
	void LoadResourcedField()
	{
		char fullPresetPath[256];
		memset(fullPresetPath, 0, 256);

#if __BANK
		if( BANKMGR.OpenFile(fullPresetPath, 256, "*.?grs", false, "Grass Field Resourced (*.?grs)") )
		{
			// Load the field
			FieldList*	 fields;
			pgRscBuilder::Load( fields, fullPresetPath,"#grs", FieldList::RORC_VERSION);
			m_GrassManager->Add( fields, m_StreamingBatchId );
		}
#endif
	}

	virtual void ShutdownClient()
	{

		delete m_GrassManager;
		
		// Shutdown the grass patch class
		grassPatch::ShutdownClass();


		rmcSampleManager::ShutdownClient();
	}

	virtual void UpdateClient()
	{
		static sysTimer elapsed;

		m_GrassManager->SetTime( elapsed.GetTime() * m_fGustRadius);

		// shows how to modify grassShader patch setttings
		grassPatchTextured* patch = m_GrassManager->GetPatch<grassPatchTextured>(atString("grass"));
		if ( patch )
		{
			patch->SetWind( m_WindDirection, m_WindStrength );
		}
	}

	virtual void DrawClient()
	{
		grcState::SetLightingMode( grclmDirectional );

		// Draw the grass
		grcViewport* viewport = grcViewport::GetCurrent();
		if (viewport)
		{
			spdShaft shaft;
			shaft.Set(*viewport, grcViewport::GetCurrent()->GetCameraMtx());
			m_GrassManager->Draw(shaft);
		}
	}

	void RemoveField()
	{
		m_GrassManager->DestroyFields( m_StreamingBatchId );
	}
#if __BANK
	virtual void AddWidgetsClient()
	{
		bkBank &B = BANKMGR.CreateBank("sample_grass");
		B.AddSlider("Batch Identifier", &m_StreamingBatchId, 0, 120, 1 );
		B.AddButton("Load (XML) ", datCallback(MFA(sampleGrass::LoadField), this));
		B.AddButton("Load Resourced ", datCallback(MFA(sampleGrass::LoadResourcedField), this));
		B.AddButton("Remove", datCallback(MFA(sampleGrass::RemoveField), this));

		if ( m_GrassManager )
		{
			B.PushGroup("Grass Manager" );
			WidgetsAddWithFileIO( B, *m_GrassManager, "grass_drawers" );
			B.PopGroup();
		}
	}
#endif
protected:
	grassManager*		m_GrassManager;
	grcTexture*			m_GroundTexture;

	// Wind
	Vector3				m_vWindDir;
	float				m_fWindStr;
	int					m_nGustChance;
	float				m_fMinGustStrength;
	float				m_fMaxGustStrength;
	float				m_fGustRadius;    

	// Field
	char				m_szGrassLayoutTexture[256];
	char				m_szGrassBillboardTexture[256];
	Vector4				m_vExtents;
	float				m_fGridRes;
	Vector2				m_vCardSize;
	Vector2				m_vCardSizeVariance;
	int					m_nHorizTexVariants;
	int					m_nVertTexVariants;
    Matrix44            m_mColorProjectionMatrix;

	Vector3				m_WindDirection;
	float				m_WindStrength;
	bool				m_cameraFacing;

	//	lod
	float				m_fFadeStart;
	float				m_fFadeRange;

	int					m_StreamingBatchId;
};

} // namespace ragesamples

int Main ()
{
	ragesamples::sampleGrass sample;
	sample.Init("sample_grass");
	sample.UpdateLoop();
	sample.Shutdown();
	return 0;
}
