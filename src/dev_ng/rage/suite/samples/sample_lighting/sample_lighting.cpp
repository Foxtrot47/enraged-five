// 
// sample_lighting/sample_lighting.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Note: creates a 2D screenspace texture that holds all shadow relevant data
//

#define SIMPLE_HEAP_SIZE		( 145* 1024  )
#define SIMPLE_PHYSICAL_SIZE	( 68* 1024  )

#include "system/main.h"
#include "system/param.h"

#include "sample_simpleworld/entitymgr.h"
#include "sample_simpleworld/sample_simpleworld.h"
#include "sample_rmcore/sample_rmcore.h"
#include "shadercontrollers/skyhat.h"
#include "system/timemgr.h"

#include "grshadowmap/spotlightshadowmap.h"
#include "grpostfx/postfx.h"


#include "rmptfx/ptxmanager.h"
#include "manylights/lightgrid.h"
#include "manylights/lightshadercontrollers.h"
#include "manylights/lightfog.h"
#include "animator.h"

XPARAM( file );


PARAM( lightlayers, "Whether to use light layers or not" );
PARAM( eightlights, "Whether to use eight lights per cell or four" );
PARAM( textureLights, "Whether to store lights in a texture or not" );

using namespace ragesamples;


class ScreenDoorTransparency
{
	enum Types
	{
		SDT_OFF = 0,
		SDT_DITHERED,
		SDT_SOLID,
		SDT_ALPHA
	};

	float					m_transparency;
	Types					m_type;
	grcEffectGlobalVar		m_transparencyID;	
public:

	ScreenDoorTransparency() : m_transparency(1.0f), m_type(SDT_OFF),m_transparencyID(grcegvNONE )
	{}
	
	void Begin()
	{
		if ( m_transparencyID == grcegvNONE )
		{
			m_transparencyID = grcEffect::LookupGlobalVar ("GlobalTransparency" , false  );
		}

		switch( m_type )
		{
		case SDT_OFF :
			grcState::SetAlphaBlend( false);
			break;
		case SDT_DITHERED:
			grcState::SetAlphaToMask( true );
			grcState::SetAlphaToMaskOffsets( grcamoDithered );
			break;
		case SDT_SOLID:

			grcState::SetAlphaToMask( true );
			grcState::SetAlphaToMaskOffsets( grcamoSolid );
			break;
		case SDT_ALPHA:
			grcState::SetAlphaBlend( true );
			break;
		}
		grcEffect::SetGlobalVar( m_transparencyID, m_transparency * m_transparency);
	};
	void End()
	{
		grcState::SetAlphaToMask( false );
		grcState::SetAlphaBlend( false );
	}
	
#if __BANK
	void AddWidgets( bkBank& bank )
	{
		const char* Names[ SDT_ALPHA + 1] = {	"Off", "Dithered","Solid","Full Alpha" };

		bank.PushGroup("Screen Door Transparencey");
		bank.AddCombo("Type", (int*)&m_type, SDT_ALPHA + 1, Names) ;
		bank.AddSlider("Transparencey", &m_transparency,0.0f, 1.0f, 0.01f );
		bank.PopGroup();
	}
#endif
};

#if 0 

namespace sv {


template<int N>
class ControlReg
{
	u32 m_c;
	Vector4 m_v;
public:
	ControlReg( u32 c , Vector4 v )	:m_c(c), m_v(v)	{}

	Vector4 GetVector() const { return m_v; }

	bool All()	{ return ( m_c & (1 << 7)) != 0 ; }
	bool Any()	{ return !( m_c& (1 << 6)) != 0 ; }
	bool None()	{ return ( m_c & (1 << 6)) != 0 ; }
};
template<int N>
bool All( ControlReg<N> r )	{ return r.All(); }
template<int N>
bool Any( ControlReg<N> r )	{ return r.Any(); }
template<int N>
bool None( ControlReg<N> r )	{ return r.None(); }

/*
class ComparitorLessThan
{
	Vector4	m_a;
	Vector4 m_b;
public:
	ComparitorLessThan( Vector4 a, Vector4 b ) : m_a(a), m_b(b) {}
	ControlReg GetReg() { u32 r;  Vector4 v = m_a.IsLessThanVR( m_b, r ); return ControlReg(r); }
	Vector4 GetReg() { u32 r;  Vector4 v = m_a.IsLessThanVR( m_b, r ); return ControlReg(r); }
};
*/
template<int N >
class Float
{
public:
	Float() {}
	Float( const Float& other ) : m_v( other.m_v )	{}

	Float( const ControlReg<N> cr ) : m_v( cr.GetVector() ) {}

	template<class a, class b, class c, class d >
	Float<N>( bool )
	{
		static Vector4 sm_value( a, b, c, d );
		m_v = sm_value;
	}
	
	// assignment
	Float<N>& operator=( const Float<N>& other )	{ m_v = other.m_v; return *this; }

	// comparison
	Float<N> operator==( const Float<N>& other )	{ return Float<N>( m_v.IsEqualIV(other.m_v));}
//	Float<N> operator<( const Float<N>& other )	{ return Float<N>( m_v.IsLessThanV(other.m_v));}
//	Float<N> operator>( const Float<N>& other )	{ return Float<N>( m_v.IsGreaterThanV(other.m_v));}

	ControlReg<N> operator<( const Float<N>& other )	{ u32 r;  Vector4 v = m_v.IsLessThanVR(other.m_v, r ); return ControlReg<N>( r ,v );}
	ControlReg<N> operator>( const Float<N>& other )	{ u32 r;  Vector4 v = m_v.IsGreaterThanVR( other.m_v, r ); return ControlReg<N>( r , v);}

	//ControlReg lt( const Float<N>& other )	const { u32 r;  m_v.IsLessThanVR(other.m_v, r ); return ControlReg( r );}
	//ControlReg gt( const Float<N>& other )	const 	{ u32 r;  m_v.IsGreaterThanVR(other.m_v, r );return ControlReg( r );}

	// math
	Float<N>& operator+=( const Float<N>& other )	{ return Float<N>( m_v.Add( other.m_v ));}
	Float<N>& operator-=( const Float<N>& other )	{ return Float<N>( m_v.Subtract(other.m_v));}
	Float<N>& operator*=( const Float<N>& other )	{ return Float<N>( m_v.Multiply(other.m_v));}
	Float<N>& operator/=( const Float<N>& other )	
	{ 
		Vector4 v;
		v.Invert( other.m_v);
		return Float<N>( m_v.Multiply( v ));
	}

	Float<N>	Select(   const Float<N>& a, const Float<N>& b ) const
	{
		Vector4 v = m_v;
		return Float<N>( v.Select( b.m_v, a.m_v ) );
	}

	friend class Float<1>;
	friend class Float<2>;
	friend class Float<3>;

	// conversion
	Float<1> x() {  return Float<1>(m_v); }
	Float<2> xx() { return Float<2>( m_v ); }
	Float<3> xxx() { return Float<3>( m_v ); }
protected: 
	Vector4	m_v;
	explicit Float( const Vector4 v ) : m_v( v )	{}
};


/*
template<int N, int Offset, int Length>
class FloatRef
{
	Float<N>& ref;
	const FloatRef& operator=( const FloatRef& other );

public:
	FloatRef( Float<N> r) : ref( r) {}
	FloatRef( FloatRef r) : ref( r.ref ) {}
	// assignment
	const FloatRef& operator=( const Float<N>& other )	
	{ 
		Vector4 v = ref.m_v;
		for (int i = 0; i < Length; i++ )
		{
			v[ Offset + i] = other.m_v[i];
		}
		ref.m_v = v;
		return *this;
	}

};*/


template<int N>
Float<N> Select(  const ControlReg<N> selector, const Float<N> a, const Float<N> b )
{
	Float<N> sel( selector );
	return sel.Select( a, b );
}

// template specialization
class Float1 : public Float<1>
{
public:
	Float1( const Float<1>& other  )  : Float( other ) {}

	friend	class Float2;
	friend	class Float3;

	explicit Float1( float f ) 
	{
		Set(f);
	}
	void Set( float f )	{ m_v.Set( f); }



protected:
	explicit Float1( Vector4& v ) : Float(v ) {}
};

Vector4 permuteV4( const Vector4& v, int a, int b, int c, int d )
{
	Vector4 res;
	res.x = v[a];
	res.y = v[b];
	res.z = v[c];
	res.w = v[d];
	return res;
}
class Float2 : public Float<2>
{
public:
	friend	class Float1;
	friend	class Float3;

	Float2() {}

	Float2( Float<2> other  )  : Float( other ) {}

	Float2( const Float1& other ) 
		: Float( other.m_v )	{}


	template<class a, class b>
	Float2( bool )
	{
		static Vector4 sm_value( a, b, c, d );
		m_v = sm_value;
	}
	explicit Float2( const Float1& x, const Float1& y ) 
	{
		m_v.MergeXY( x.m_v, y.m_v );
	}

	// conversion
	Float1 x() { Vector4 v; v.SplatX(); return Float1( v ); }
	Float1 y() { Vector4 v; v.SplatY(); return Float1( v ); }
	Float2 xy() { return *this; }

	//FloatRef<2,1,1> SetY() { return FloatRef<2,1,1>( *this ); }

	Float2 xx() { Vector4 v; v.SplatX(); return Float2( v ); }
	Float2 yy() { Vector4 v; v.SplatY(); return Float2( v ); }

	// swizzling
	Float2 yx() { return Float2( permuteV4( m_v, 1, 0, 0, 0 ) ); }

	bool All()	{ return m_v.x != 0.0f && m_v.y != 0.0f; }
	bool Any()  { return m_v.x != 0.0f || m_v.y != 0.0f; }

protected:
	explicit Float2( Vector4 v ) : Float(v ) {}
};
class Float3 : public Float<3>
{
public:
	friend	class Float1;
	friend	class Float2;

	Float3() {}

	Float3( Float<3> other  )  : Float( other ) {}


	Float3( const Float1& other ) 
		: Float( other.m_v )	{}

	explicit Float3( const Float1& x, const Float1& y, const Float1& z ) 
	{
		m_v.MergeXY( x.m_v, y.m_v );
		m_v.z = z.m_v.z;
	}
	explicit Float3( const Float2& xy, const Float1& z ) 
	{
		m_v = xy.m_v;
		m_v.z = z.m_v.z;
	}

	// conversion
	Float1 x() { Vector4 v; v.SplatX(); return Float1( v ); }
	Float1 y() { Vector4 v; v.SplatY(); return Float1( v ); }
	Float1 z() { Vector4 v; v.SplatZ(); return Float1( v ); }
	Float2 xy() { return Float2( m_v ); }
	Float3 xyz() { return *this; }

	Float2 xx() { Vector4 v; v.SplatX(); return Float2( v ); }
	Float2 yy() { Vector4 v; v.SplatY(); return Float2( v ); }
	Float2 zz() { Vector4 v; v.SplatZ(); return Float2( v ); }

	Float3 xxx() { Vector4 v; v.SplatX(); return Float3( v ); }
	Float3 yyy() { Vector4 v; v.SplatY(); return Float3( v ); }
	Float3 zzz() { Vector4 v; v.SplatZ(); return Float3( v ); }

	// swizzling
	Float2 yx() { return Float2( permuteV4( m_v, 1, 0, 0, 0 ) ); }

	Float2 xz() { return Float2( permuteV4( m_v, 0, 2, 0, 0 ) ); }
	Float2 zx() { return Float2( permuteV4( m_v, 2, 0, 0, 0 ) ); }

	Float2 yz() { return Float2( permuteV4( m_v, 1, 2, 0, 0 ) ); }
	Float2 zy() { return Float2( permuteV4( m_v, 2, 1, 0, 0 ) ); }

	// xzy, yzx,  zxy, xzy, zyx
	

protected:
	explicit Float3( Vector4 v ) : Float(v ) {}
};

void UnitTestSvFloats()
{
	Float1	v1( 1.0f );
	Float1	v2( 2.0f );
	Float1	v3( 3.0f );
	Float1	v4( 4.0f );

	Float2	v2_1( v1, v2 );

	Float3	v3_1( v2_1, v3 );
	Float3	v3_2( v3_1 );
	Float3 res = v3_1 == v3_2;

	Float2 res2;
	res2 = Select( v3_1.yx() > v2_1, v3.xx() , v4.xx() );

	Float2 res3 = v3_2.yx() > v2_1;

	Float3 bound1( v3_1 );
	Float3 bound2(v3_2 );

	bool result = Any(  bound1 <  bound2  );
	Assert( result );

}

};

#endif

class HotLoader
{
public:
	HotLoader() : m_path(""), m_timeStamp(0 )
	{}

	HotLoader( const char* path )
		: m_path( path ), m_timeStamp(0 )
	{
	}

	void SetFile( const char* path, const char* suffix = 0 )
	{
		m_path = path;
		if ( suffix )
		{
			m_path+=  atString(".");
			m_path+= atString( suffix );
		}
		m_timeStamp = 0; // lets reload it
	}
	bool IsOutOfDate() const { return GetCurrentTimeStamp() > m_timeStamp; }
	u64	  GetCurrentTimeStamp() const
	{ 
		return fiDevice::GetDevice(m_path)->GetFileTime(m_path);
	}

	bool NeedToLoad()
	{
		if ( m_path == "" )
		{
			return false;
		}
		if ( IsOutOfDate() )
		{
			m_timeStamp = GetCurrentTimeStamp();
			return true;
		}
		return false;
	}
private:
	u64				m_timeStamp;
	atString		m_path;
};

/*

template<class RESOURCE>
class HotLoaderResource : public atReferenceCounter
{
	HotLoader	loader;
	RESOURCE*	resource;
public:
	HotLoaderResource()
};
template<class RESOURCE>
class HotLoaderResourceTable : public HotLoader
{
	std::list<HotLoaderResource*>		tables;
	HotLoaderSubject( const char* path ) : HotLoader
};

*/


PARAM( mayaautoupdate, "Automatically upload the light file to allow for coninutous updateing of the maya file" );
PARAM( cameraautoupdate, "Automatically update the camera from maya" );

//-----------------------------------------------------------------------------
#define NUM_OBJECTS				15
const char*		ObjectList[ NUM_OBJECTS ] = {	
									"ManyLights\\manyLightsCityTex\\manyLightsCityTex.type",
									//"t:\\rage\\assets\\resource\\manylights\\manylightsCityTex\\manyLightsCityTex.dr",
									"ManyLights\\manyLightsCity\\manyLightsCity.type",
								

									"ManyLights\\test_pillars\\test_pillars.type",
									"ManyLights\\test_pillars_anim\\test_pillars_anim.type",
									"ManyLights\\test_awning\\test_awning.type",
									"ManyLights\\test_awning\\test_awning.type",

									"ManyLights\\thirtytwo\\thirtytwo.type",

									"ManyLights\\manylightsShadow\\manylightsShadow.type",
									"ManyLights\\test_character\\test_character.type",
									"ManyLights\\test_character8\\test_character8.type",
								
									"ManyLights\\manyLightsSaloon\\manyLightsSaloon.type",
									"boyo\\boyo.type",
									"canoe\\entity.type",
									"dock\\entity.type",
									"saloon\\entity.type" };
			
const char*		LightFileList[ NUM_OBJECTS ] = {	
											"t:\\rage\\assets\\lightSetups\\manyLightstest",
										//"t:\\rage\\assets\\lightSetups\\manyLightsCityTex",
										"t:\\rage\\assets\\lightSetups\\manyLightsCity",
										"t:\\rage\\assets\\lightSetups\\pillars",
										"t:\\rage\\assets\\lightSetups\\pillars_anim",
										"t:\\rage\\assets\\lightSetups\\awning",
										"t:\\rage\\assets\\lightSetups\\awning8",									

										"t:\\rage\\assets\\lightSetups\\thirtytwo",

										"t:\\rage\\assets\\lightSetups\\manylightsShadow",
										"t:\\rage\\assets\\lightSetups\\character",
										"t:\\rage\\assets\\lightSetups\\character8",
											"prt\\ManyLights\\lightSetups\\manylightsShadow",
											"prt\\ManyLights\\lightSetups\\manylightsShadow",
											"prt\\ManyLights\\lightSetups\\manylightsShadow",
											"prt\\ManyLights\\lightSetups\\manylightsShadow",
											"prt\\ManyLights\\lightSetups\\manylightsShadow" };

class SampleLighting : public ragesamples::rmcSampleSimpleWorld
{
	class RenderShadowCastersFunctor
	{
		SampleLighting& world;

		RenderShadowCastersFunctor&	operator=( RenderShadowCastersFunctor& );
	public:
		explicit RenderShadowCastersFunctor( SampleLighting& w ) : world( w )
		{}

		bool operator()( grcViewport& view )
		{
			world.RenderShadowCasters( view, true );
			return true;
		}
	};

public:
	SampleLighting() : m_objIndex( 0 ), 
						m_showLights ( false ),
						m_showTree( false ),
						m_sphereOfInfluence( 0.5f ),
						m_lightCompaction( 0.1f ),
						m_IntensityBoost( 1.0f ),
						m_cullLights( false ),
						m_alwaysReload( PARAM_mayaautoupdate.Get() ),
						m_rebuildSortTexture( true ),
						m_mapSize( 4 ),
						m_AmbientOcclusionFadeIn( 0.0f ),
						m_bumpiness( 1.0f ),
						m_animate1( "lightAnimate1"),
						m_animate2( "lightAnimate2"),
						m_animate3( "lightAnimate3"),
						m_drawLightFogs( true ),
						m_drawDebugCones( false ),
						m_volumeIntensity( 1.0f ),
						m_volumeTrans( 1.0f),
						m_debugTransparency( 0.5f ),
						m_clipIntensity( 2.0f ),
						m_skyDome( rage_new SkyDome ),
						m_ShadowCache( rage_new CShadowCache ),
						m_sortFillMap( rage_new NearestMap ),
						m_cameraAutoUpdate( PARAM_cameraautoupdate.Get() ),
						m_shadowsInitialized(false ),
						m_time(12.0f)
	{
		 m_sortMap = rage_new NearestMap( PARAM_lightlayers.Get() ? 3 : 1 ,64,  PARAM_textureLights.Get() , PARAM_eightlights.Get() );
	}

	~SampleLighting() {}


protected:
	//grcSampleManager 
	virtual const char* GetSampleName() const
	{
		return "Lighting Demo";
	}

	void LoadLights()
	{
		LightList		lightList;
		PARSER.LoadObject( m_lightFileName.c_str(), "xml", lightList);


		Light::SetSphereOfInfluence( m_sphereOfInfluence );

		//lightList.Scale( 100.0f, m_IntensityBoost );
		lightList.Scale( 1.0f, m_IntensityBoost );
		if ( lightList.Empty())
		{
			return;
		}

		if ( lightList.GetVersion() >= 2 )
		{
			m_volumeLights.Reset();
			for( int i = 0; i < lightList.GetCount(); i++ )
			{
				if ( lightList[i].CastsVolume() )
				{
					m_volumeLights.PushAndGrow( lightList[i] );
					m_volumeLights.back().Update(2);
				}
			}
			m_mayaLights.CompactLights( lightList, m_lightCompaction, 0.000001f );
			m_mayaLights.Update();
		}
		else
		{
			m_mayaLights.CompactLights( lightList, m_lightCompaction, 0.000001f );
			m_mayaLights.Update();
			m_volumeLights = m_mayaLights.lights;
		}
		
		
		
		m_ShadowCache->Reset(); // should really only dirty lights with changed shadow params.
		RebuildShadows();
		RebuildLights();
	}

	void RebuildShadows()
	{
		BuildShadows();
	}
	void BuildShadows()
	{
		m_ShadowCache->UseCachedShadowsWithLightList( m_mayaLights.lights );
	}
	void AddShadowedLight( int i )
	{
		Light&	light= m_mayaLights.lights[i];
		m_ShadowCache->AddSpot( i, light.Position, light.DirectionVector, 
			light.GetAngleRange(), light.GetShadowRange(), 
			light.ShadowSize / m_mapSize,
			light.GetShadowFilterSize() );
		light.ResetDirty();
	}
	void RebuildLights()
	{
		if ( !m_mayaLights.lights.GetCount())
		{
			return;
		}
		m_lightingParamsController.SetLights( m_mayaLights.lights , true );
		m_lightingParamsController.SetLights( m_mayaLights.fillLights , false);
	
		// shadowing
		if ( m_rebuildSortTexture )
		{
			FastLightList<LIGHT_GRID_MAX_LIGHTS> fLights;
			Vector3 diag = m_objAABB.GetMax()  - m_objAABB.GetMin();
			//diag = Vector3( 0.0f, 0.0f, 0.0f );
			fLights.CreateFastLights( m_mayaLights.lights, diag );

			m_sortMap->SetUpLights( m_mayaLights.lights );
			
			m_sortMap->SetFastLights( fLights );
			m_sortMap->Create(  m_objAABB.GetMin(), m_objAABB.GetMax() );		

			m_lightingParamsController.SetSortTexture( m_sortMap->GetGrid(),  m_sortMap->GetGrid2(),   m_sortFillMap->GetGrid(), m_objAABB.GetMin(), m_objAABB.GetMax()  );
			m_lightingParamsController.SetPositionalTexture(  m_sortMap->GetPosition(), m_sortMap->GetColors(), m_sortMap->GetHeights(), m_sortMap->GetPosition2(),m_sortMap->GetColors2() );
		//	m_lightingParamsController.SetHeightMap( m_heightMap.GetTexture() );
			m_lightingParamsController.SetRayTracedReflections( m_heightMap.GetTexture() , m_skyDome->GetProceduralTextures()->GetRenderTarget( ProceduralTextureSkyhat::E_MINI_SKY_RT) );
		}

		m_ShadowController.SetShadows(  m_mayaLights, *m_ShadowCache,  m_AmbientOcclusionFadeIn, m_bumpiness );
	}
	void CullLights()
	{
		grcViewport*	view = grcViewport::GetCurrent();
		m_mayaLights.Cull( view );
		RebuildShadows();
		RebuildLights();
	}
	void InitLights()
	{

		rmcSampleSimpleWorld::InitLights();
		LoadLights();
	}

	void ResetShadows()
	{
		if (m_shadowsInitialized)
		{
			return;
		}
#if __PPU
		CShadowedSpotLight::ShadowType	type = CShadowedSpotLight::STANARD;
#else
		CShadowedSpotLight::ShadowType	type = CShadowedSpotLight::VARIANCE;  // 128
#endif
		m_ShadowCache->Init( 36, 256, type, true );  // 128
		
		m_shadowsInitialized = true;
		if ( !PARAM_file.Get() )
		{
			LoadObject();
		}
		if ( m_mayaLights.lights.GetCount())
		{
			RebuildShadows();
			RebuildLights();
		}
		
	}

	void InitClient()
	{
		//m_UseAA = true;

		
		if ( m_UseZUp)
		{
			g_UnitUp = Vector3( 0.0f, 0.0f, 1.0f );
		}
		ASSET.PushFolder( "$\\prt\\ManyLights\\");
		grmShaderFactoryStandard::GetInstance().PreloadShaders( "lib", false ) ;
		ASSET.PopFolder();

		rmcSampleSimpleWorld::InitClient();

		
		ShaderController::Instantiate();


		//Setup keyboard hot-keys
		m_Mapper.Map(IOMS_KEYBOARD, KEY_N, m_HotKeyNextModel);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_P, m_HotKeyPreviousModel);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_R, m_HotKeyLoadLights);
		
	


		m_heightMap.Init( 128, "HieghtMap" );

		m_sortMap->Init();
		m_sortFillMap->Init();


		ResetShadows();	

		ASSET.PushFolder( "$\\prt\\ManyLights\\");
		m_animate1.Load();
		m_animate2.Load();
		m_animate3.Load();
		ASSET.PopFolder();

		m_lightFogRenderer = rage_new SpotLightFogRenderer();

		GRPOSTFX->SetGammaEditable( true );
		GRPOSTFX->SetGamma( 0.67f );
		GRPOSTFX->LoadPPPPreset( "$\\prt\\ManyLights\\PPP\\manyLightsPresets.ppp" );
		
		
		m_skyDome->Init();

		m_skyDome->SetZUp( m_UseZUp );

		
	}
	void CreateHeightMap( const Vector3& aabbMin, const Vector3& aabbMax )
	{
		if ( !m_heightMap.IsDirty() ) 
		{ 
			return; 
		}
		Assert( aabbMin.IsGreaterThanV( aabbMax).IsZero() );
		Vector3 centre = ( aabbMin + aabbMax ) * 0.5f;
		Vector3 range = ( aabbMax - aabbMin ) * 0.5f;
		Vector3 top( centre.x, centre.y, aabbMax.z );
		Vector3 dir( 0.0f, 0.0f, -1.0f );
		Vector3 rmin( -range.x, -range.y, 0.0f);
		Vector3 rmax( range.x, range.y, range.z * 2.0f);

		m_heightMap.CreateDirectionalLight(top,dir, rmin, rmax  );
		m_heightMap.Begin();

		RenderShadowCasters( m_heightMap.GetViewport() , true);

		m_heightMap.End( false);
	}


	void PreDrawClient()
	{
	
		grcState::SetLightingMode( grclmSpot );

		CreateHeightMap( m_objAABB.GetMin(),  m_objAABB.GetMax() );

		// lets render out our shadow cache textures first
		RenderShadowCastersFunctor	render( *this );
		m_ShadowCache->RenderRequiredShadowMaps( render );

		m_skyDome->DrawProceduralTextures();
	}

#if USE_GRSHADOWMAP
	void RenderShadowCasters( grcViewport&  vp, bool isCaster)
	{
		RMCSAMPLESHADOWMAP->SetObjectList( rmcShadowList::InstancePtr() );
		RMCSAMPLESHADOWMAP->RenderShadowCasters( vp,  isCaster);
	}
#else
	void RenderShadowCasters( grcViewport& , bool /*isCaster*/)
	{
	}
#endif

	//-----------------------------------------------------------------------------
	void DrawClient2()
	{
		//Simpleworld drawing
		spdShaft shaft;
		if(m_ViewMode == 0)
		{
			shaft.Set(*m_Viewport, m_Viewport->GetCameraMtx());
		}
		else
		{
			Matrix34 mat;
			mat.Identity();
			mat.d = m_Viewport->GetCameraMtx().d;
			shaft.Set(*m_Viewport, mat);
		}

		// Fragment drawing
		m_pFragWorld->SetCamera( *m_Viewport, shaft.GetMatrix() );
		m_pFragWorld->UpdateMouse();
		m_pFragWorld->Draw();

		m_lightingParamsController.SetGlobals();
		m_ShadowController.SetGlobals();
		m_CompositeLighting.SetGlobals();

#if __XENON
		// set the pixel shader gprs to 95 -> vertex shader can only use 48 gprs
		GRCGPRALLOCATION->SetGPRAllocation(95);
#endif
		m_fader.Begin();
		
		
		SMPWORLD.Draw(shaft);

		m_fader.End();

#if __XENON
		// set the pixel shader gprs to 80 -> vertex shader can only use 48 gprs
		GRCGPRALLOCATION->SetGPRAllocation(0);
#endif

	}
	void DrawClient()
	{
		ShaderController::GetInstance().UpdateShaders();	
		

		if ( m_showLights )
		{
			m_mayaLights.Draw();
			m_objAABB.Draw( Color32(1.0f, 0.0f,1.0f,1.0f ));

			m_heightMap.Show(90,90,180,180);
		}

		m_ShadowCache->DebugDraw();	// for debugging

		DrawClient2();


		m_skyDome->Draw();
		// do transparent light fogs last
		m_lightFogRenderer->SetClipIntensity( m_clipIntensity );
		if ( m_drawDebugCones )
		{
			m_lightFogRenderer->SetTechnique( "drawDebug" );
			m_lightFogRenderer->CullSortRender( m_mayaLights.lights, m_debugTransparency, 1.0f  );
		}
		if ( m_drawLightFogs )
		{
			m_lightFogRenderer->SetTechnique( "draw" );
			m_lightFogRenderer->CullSortRender( m_volumeLights, m_volumeIntensity , m_volumeTrans);
		}

	}
	void Draw()
	{
		m_Setup->BeginDraw();

		PreDrawClient();

		SetupCamera();

		// The shadow-map code has problems calculating the view frustum when in ortho-mode, so disable the shadowmap rendering
		// when we are in any viewport besides the perspective view.
		GRCDEVICE.Clear(true, Color32(0.0f, 0.0f, 0.0f, 1.0f), true, 1.0f, false);

		// Bind render target for PC
		GRPOSTFX->BindPCRenderTarget(true);

		SetFog();
		

#if __WIN32
		// Clip planes must be respecified after any camera change because they
		// are transformed by the current composite matrix before being sent
		// down to the hardware.
		if (m_UserClip) {
			GRCDEVICE.SetClipPlaneEnable(1 << 0);
			GRCDEVICE.SetClipPlane(0, m_UserClipPlane);
		}
#endif

#if __XENON
		if (m_UseAA)
		{
			grcResolveFlags clearParams;
			clearParams.Color = m_ClearColor;
			GRCDEVICE.BeginTiledRendering(NULL,NULL,&clearParams);
		}
#endif


#if __WIN32
		if (m_UserClip) {
			GRCDEVICE.SetClipPlaneEnable(0);
		}
#endif

		DrawClient();

#if __XENON		
		if (m_UseAA)
			GRCDEVICE.EndTiledRendering();
#endif

		// run post-processing pipeline
		GRPOSTFX->Process();


		// Try to recover from shaderfx rendering
		//grcState::Default();	
		
		grcWorldIdentity();

	
		m_Setup->EndDraw();
	}
	void Next()
	{	

		m_objIndex++;
		LoadObject();
		if ( m_objIndex >= NUM_OBJECTS )
		{
			m_objIndex = 0;
		}
	}
	void Previous()
	{
		m_objIndex--;
		if ( m_objIndex < 0 )
		{
			m_objIndex = NUM_OBJECTS - 1;
		}
		LoadObject();
	}
	void LoadObject()
	{
		std::string path = "t:\\rage\\assets\\prt\\";
		
		if ( SMPWORLDEMGR.GetEntityCount() )
		{	
			rmcDrawable *pDrawable = SMPWORLDEMGR.GetEntity(0)->GetDrawable();
			ShaderController::GetInstance().RemoveShaders( pDrawable );

			SMPWORLDEMGR.RemoveEntity( 0 );
		}

		pgRscBuilder::AddVersionNumberToExtension();

		path += ObjectList[ m_objIndex ];

		AssertVerify( LoadEntityFile( this, path.c_str() )   );
	}
	virtual void OnRemoveEntity( SimpleWorldEntity *pEntity )
	{
		rmcDrawable *pDrawable = pEntity->GetDrawable();
		ShaderController::GetInstance().RemoveShaders( pDrawable );

	}
	virtual void OnLoadEntity( SimpleWorldEntity * pEntity)	
	{

		//rmcDrawable *pDrawable = pEntity->GetDrawable();
		//ShaderController::GetInstance().AddShaders( pDrawable );

		//Vector4 	sphere(  pEntity->GetXFormSphere().GetVector4() );
		rmcDrawable *pDrawable = SMPWORLDEMGR.GetEntity(0)->GetDrawable();
		Vector3 min;
		Vector3 max;
		pDrawable->GetLodGroup().GetBoundingBox( min, max);
		m_objAABB.Set( min, max  );

		std::string path = "t:\\rage\\assets\\LightSetups\\";
		path += fiAssetManager::FileName( (const char *)pEntity->GetSourceFilePath() );
		path = path.substr( 0, path.find('.') );

		m_lightFileName = path;
		m_lightFileWatcher.SetFile( path.c_str(), "xml" );

		if (!m_shadowsInitialized )
		{
			ResetShadows();
		}
		LoadLights();

		m_heightMap.MarkDirty();
	}

	void ShutdownClient()
	{
		delete m_skyDome;
		m_skyDome = 0;
		
		delete m_ShadowCache;
		m_ShadowCache = 0;

		delete m_sortMap;
		m_sortMap = 0;
		delete m_sortFillMap;
		m_sortFillMap = 0;

		delete m_lightFogRenderer;
		m_lightFogRenderer = 0;
		rmcSampleSimpleWorld::ShutdownClient();

		ShaderController::Destroy();
	}

	virtual void UpdateCamera()
	{
		if ( m_cameraAutoUpdate )
		{
			Matrix34 camMtx  = m_mayaLights.GetCamera();
			GetCamMgr().SetWorldMtx( camMtx  );
		}
		else
		{
			rmcSampleSimpleWorld::UpdateCamera();
		}
	}
	virtual void UpdateClient()
	{
		//Keyboard update
		ragesamples::rmcSampleSimpleWorld::UpdateClient();

		SkyHatPerFrameSettings          settings;    
		float time = m_time;//22.0f;	// 10 at night
		m_SkyControl.CalculateSkySettings( time, settings );   // calculate settings using the current weather 
		m_skyDome->SetSettings( settings );                                      // and pass to skydome 

		m_skyDome->SetSunDirection( SkyDomeProceduralControl::GetSunDirection( time ));  // set sun direction 

		static float cloudTime = 0.0f;
		cloudTime += 0.1f;
		//m_skyDome->Update( 24.0f, 0 );
		m_skyDome->Update(  cloudTime, 0 );

		//grmShaderFx::SetForcedTechniqueGroupId(0);

		m_Mapper.Update();
		if(m_HotKeyNextModel.IsPressed())
		{
			Next();
		}
		else if ( m_HotKeyPreviousModel.IsPressed() )
		{
			Previous();
		} 
		else if ( m_HotKeyLoadLights.IsPressed() )
		{
			 LoadLights();
		}

		if ( m_cullLights )
		{
			CullLights();
		}
		
		const bool testShadowRenderingPerformance = false;

		if( testShadowRenderingPerformance)
		{

			for( int i = 0; i <  m_mayaLights.lights.GetCount() ; i++ )
			{
				AddShadowedLight( i);
			}
		}
		bool rebuild = true;
		if (  m_animate1.Apply( m_mayaLights.lights ))		{			rebuild = true;		}
		if (  m_animate2.Apply( m_mayaLights.lights ))		{			rebuild = true;		}
		if (  m_animate3.Apply( m_mayaLights.lights ))		{			rebuild = true;		}

		if ( rebuild )
		{

			BuildShadows();
			RebuildLights();
		}
		m_lightingParamsController.SetLights( m_mayaLights.lights , true );
	
		if ( m_alwaysReload )
		{
			if ( m_lightFileWatcher.NeedToLoad() )
			{
				LoadLights();
			}
		}
	}
	
#if __BANK
	void AddWidgetsClient() 
	{
		rmcSampleSimpleWorld::AddWidgetsClient() ;

		bkBank& bank = BANKMGR.CreateBank("Many Lights");

		bank.PushGroup("Lights");			// add lighting group
		bank.AddSlider("Time", &m_time, 0.0f, 24.0f, 0.01f );

		bank.AddToggle( "Display Sphere of Influence Spheres", &m_showLights );
		bank.AddSlider( "Sphere of Influence", &m_sphereOfInfluence, 0.001f, 60.0f, 0.01f , datCallback(MFA( SampleLighting::LoadLights), this));
		bank.AddSlider( "Light Compaction", &m_lightCompaction, 0.0f, 100.0f, 0.01f , datCallback(MFA( SampleLighting::LoadLights), this));
		bank.AddSlider( "Light Intensity Boost", &m_IntensityBoost, 0.01f, 100.0f, 0.01f , datCallback(MFA( SampleLighting::LoadLights), this));
		bank.AddToggle( "Cull Lights", &m_cullLights );
		bank.AddToggle( "Auto Update", &m_alwaysReload );
		bank.AddToggle( "Camera Auto Update", &m_cameraAutoUpdate );

		bank.AddSlider( "Light Ambient Occlusion Factor", &m_AmbientOcclusionFadeIn, 0.00f, 1.0f, 0.01f , datCallback(MFA( SampleLighting::LoadLights), this));
		bank.AddSlider( "Bumpiness", &m_bumpiness, 0.0f, 20.0f, 0.001f );
		bank.AddToggle( "Rebuild Sort Texture", &m_rebuildSortTexture );

		bank.AddSlider("Shadow Map Scale Down Size", &m_mapSize, 1, 4, 1, datCallback(MFA( SampleLighting::ResetShadows), this));
		bank.PopGroup();

		bank.AddButton( "Next", datCallback(MFA( SampleLighting::Next), this));
		bank.AddButton( "Previous", datCallback(MFA( SampleLighting::Previous), this));
		m_ShadowCache->AddWidgets( bank );

		m_CompositeLighting.AddWidgets( &bank );

		bank.PushGroup("Animations");
		m_animate1.AddWidgets( bank );
		m_animate2.AddWidgets( bank );
		m_animate3.AddWidgets( bank );
		bank.PopGroup();



		bank.PushGroup("Light Volumes");
		bank.AddToggle( "Display", &m_drawLightFogs );
		bank.AddSlider("Intensity", &m_volumeIntensity, 0.0f, 2.0f, 0.01f );
		bank.AddSlider("MinimumIntensity", &m_clipIntensity, 0.0f, 5.0f, 0.01f );
		bank.AddSlider("Transparency", &m_volumeTrans, 0.0f,2.0f, 0.01f );
		
		bank.AddToggle( "Debug Lights", &m_drawDebugCones );
		bank.AddSlider("Transparency", &m_debugTransparency, 0.0f, 1.0f, 0.01f );
		bank.PopGroup();

		m_fader.AddWidgets( bank );
	}

#endif // __BANK
private:
	int						m_objIndex;
	ioMapper				m_Mapper;
	ioValue					m_HotKeyNextModel;
	ioValue					m_HotKeyPreviousModel;
	ioValue					m_HotKeyLoadLights;

	LightList				m_mayaLights;
	atArray<Light>			m_volumeLights;
	FastLighting			m_lightingParamsController;
	ManyShadows				m_ShadowController;
	LightingComposite		m_CompositeLighting;

	bool					m_showLights;
	bool					m_showTree;
	bool					m_cullLights;
	bool					m_alwaysReload;
	bool					m_cameraAutoUpdate;

	bool					m_rebuildSortTexture;
	bool					m_shadowsInitialized;

	float					m_sphereOfInfluence;
	float					m_lightCompaction;
	float					m_IntensityBoost;


	NearestMap*				m_sortMap;
	NearestMap*				m_sortFillMap;
	spdAABB					m_objAABB;


	// shadowing stuff
	CShadowCache*			m_ShadowCache;
	int						m_mapSize;

	// ambient occ
	float					m_AmbientOcclusionFadeIn;
	float					m_bumpiness;

	HotLoader				m_lightFileWatcher;
	std::string				m_lightFileName;

	Animator				m_animate1;
	Animator				m_animate2;
	Animator				m_animate3;

	bool					m_drawLightFogs;
	bool					m_drawDebugCones;
	float					m_volumeIntensity;
	float					m_volumeTrans;
	float					m_clipIntensity;
	float					m_debugTransparency;
	SpotLightFogRenderer	*m_lightFogRenderer;

	float					m_time;
	// skydome
	SkyDome*							m_skyDome;
	rage::SkyDomeProceduralControl  m_SkyControl; // create a controller for controlling the weather 

	CShadowedSpotLight					m_heightMap;
	ScreenDoorTransparency				m_fader;

#if __BANK
	//virtual void AddWidgetsClient() {}
#endif // __BANK

};

int Main()
{
	// do unit tests first as really should be done after libary
	//sv::UnitTestSvFloats();

#if __ASSERT
	UnitTestLightGridMemory();
#endif

	SampleLighting	sample;


	sample.Init("");
	sample.UpdateLoop();
	sample.Shutdown();

	return 0;
}
