set TESTERS=sample_lighting
set SAMPLE_LIBS=sample_rmcore sample_physics sample_fragment sample_simpleworld
set HEADONLY=animator
set LIBS=%SAMPLE_LIBS% %RAGE_SAMPLE_LIBS% %RAGE_SCRIPT_LIBS% %RAGE_GFX_LIBS% %RAGE_CORE_LIBS% %RAGE_CR_LIBS% %RAGE_SUITE_CR_LIBS% %RAGE_AUD_LIBS% %RAGE_PH_LIBS% spatialdata shadercontrollers 
set LIBS=%LIBS% lightshaders breakableglass phglass fragment cloth grrope rmptfx event eventtypes grshadowmap rmlighting manylights vieweraudio cliptools
set XPROJ=%RAGE_DIR%\suite\src %RAGE_DIR%\suite\samples %RAGE_DIR%\base\src %RAGE_DIR%\base\samples %RAGE_DIR%\speedtree\src  %RAGE_DIR%\script\src %RAGE_DIR%\suite\tools %RAGE_DIR%\suite\tools\cli