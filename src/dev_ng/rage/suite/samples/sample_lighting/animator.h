// 
// sample_lighting/animator.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Note: creates a 2D screenspace texture that holds all shadow relevant data
//
#ifndef SAMPLE_LIGHTING_ANIMATOR_H
#define SAMPLE_LIGHTING_ANIMATOR_H

// RAGE_QA:NO_NAMESPACE_RAGE

#include "manylights/simpleLightManager.h"
#include "rmptfx/keyframe.h"
#include "vectormath/legacyconvert.h"


// Animation
class Animator : public datBase 
{
public:
	Animator( const char* fileName  ) : m_duration( 5.0f ),
		m_colorOn( false),
		m_moveOn( false ),
		m_rotOn( false ),
		m_lightNumber( 0 ),
		m_fileName( fileName)
	{}

	void SetFileName( const char* fileName )
	{
		m_fileName = fileName;
	}

	int	GetNumber()	const	{ return m_lightNumber; }
	float GetTime()
	{
		float t = m_time.GetTime() / m_duration;

		t = t - floor( t);
		t *= 2.0f;
		if (t > 1.0f)
		{
			t = 2.0f - t; 
		}
		return t;
	}
	bool Apply( atArray<Light>& lights )
	{
		float t = GetTime();

		if ( lights.GetCount() <= m_lightNumber )
		{
			return false;
		}
		Light& lit = lights[ m_lightNumber ];

		bool doUpdate = false;

		if ( m_colorOn )
		{
			Vector4 val;
			m_Color.GetKeyframeData(t).GetData( RC_VEC4V(val), t );
			lit.Colour.Set( val.x, val.y , val.z );
			doUpdate = true;
		}

		if ( m_moveOn )
		{
			Vector4 val;
			m_Position.GetKeyframeData(t).GetData( RC_VEC4V(val), t );
			lit.Position.Set( val.x, val.y, val.z );
			doUpdate = true;
			lit.MarkDirty();
		}
		if ( m_rotOn )
		{
			Vector4 val;
			m_Rotation.GetKeyframeData(t).GetData( RC_VEC4V(val), t );
			lit.Direction.Set( val.x, val.y, val.z);
			doUpdate = true;
			lit.MarkDirty();
		}		
		if ( doUpdate )
		{
			lit.Update(2);
		}
		return lit.IsDirty();
	}
	void Reset()
	{
		m_time.Reset();
	}
#if __BANK 
#if __BANK_RMPTFX
	void AddWidgets( bkBank& bank )
	{
		bank.PushGroup("Animate");			// add lighting group
		ptxKeyframeDefn*		colData = rage_new ptxKeyframeDefn(
			"Color", ptxKeyframe::KF_VECTOR3,
			0.0f, 1.0f, 0.01f,
			0.0f, 1.0f, 0.01f,
			"Red", "Green", "Blue", NULL, true);

		m_Color.SetUiData(colData);
		m_Color.AddWidgets(bank);
		bank.AddToggle( "Apply Color Change", &m_colorOn );

		ptxKeyframeDefn*		posData = rage_new ptxKeyframeDefn(
			"Position", ptxKeyframe::KF_VECTOR3,
			-100.0f, 100.0f, 0.01f,
			0.0f, 1.0f, 0.01f,
			"X", "Y", "Z");
		m_Position.SetUiData(posData);
		m_Position.AddWidgets( bank );
		bank.AddToggle( "Apply Movement Change", &m_moveOn );

		ptxKeyframeDefn*		rotData = rage_new ptxKeyframeDefn(
			"Rotation", ptxKeyframe::KF_VECTOR3,
			-180.0f, 180.0f, 0.01f,
			0.0f, 1.0f, 0.01f,
			"X", "Y", "Z"
			);
		m_Rotation.SetUiData(rotData);
		m_Rotation.AddWidgets( bank );
		bank.AddToggle( "Apply Rotation Change", &m_rotOn );

		bank.AddSlider( "Duration", &m_duration, 0.001f, 60.0f, 0.01f );
		bank.AddSlider( "lightNumber", &m_lightNumber, 0, 30, 1 );

		bank.AddButton( "Reset", datCallback(MFA( Animator::Reset), this));
		bank.AddButton("Load",datCallback(MFA(Animator::Load),this));
		bank.AddButton("Save",datCallback(MFA(Animator::Save),this));

		bank.PopGroup();
	}
#else
	void AddWidgets( bkBank& bank ) {}
#endif
#endif
	void Create( parRTStructure& m_parRTS )
	{
		m_parRTS.SetName("animate");		
		m_parRTS.AddStructureInstance("Color",									m_Color);
		m_parRTS.AddStructureInstance("Position",								m_Position);
		m_parRTS.AddStructureInstance("Rotation",								m_Rotation);

		m_parRTS.AddSimpleMember("m_duration", NULL, &m_duration , parMember::FLOAT);
		m_parRTS.AddSimpleMember("m_colorOn", NULL, &m_colorOn, parMember::BOOL );
		m_parRTS.AddSimpleMember("m_moveOn", NULL, &m_moveOn , parMember::BOOL);
		m_parRTS.AddSimpleMember("m_rotOn", NULL, &m_rotOn , parMember::BOOL);
		m_parRTS.AddSimpleMember("m_lightNumber", NULL, &m_lightNumber , parMember::INT);
	}
	void Save()
	{
		parRTStructure		parRTS;		
		Create( parRTS);
		PARSER.SaveFromStructure( m_fileName, "xml", parRTS);
	}
	void Load()
	{
		parRTStructure		parRTS;		
		Create( parRTS);
		PARSER.LoadFromStructure( m_fileName, "xml", parRTS);
	}
	void UpdateUI()
	{
#if __BANK && __BANK_RMPTFX
		m_Color.UpdateInterface();
		m_Position.UpdateInterface();
		m_Rotation.UpdateInterface();
#endif
	}

private:
	ptxKeyframe	m_Color;	//Keyframed Acceleration
	ptxKeyframe	m_Position;	//Weight of attached matrix
	ptxKeyframe	m_Rotation;	//Weight of attached matrix

	atString				m_fileName;
	float					m_duration;
	bool					m_colorOn;
	bool					m_moveOn;
	bool					m_rotOn;
	int						m_lightNumber;
	sysTimer				m_time;	
};


#endif
