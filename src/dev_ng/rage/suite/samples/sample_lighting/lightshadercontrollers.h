// 
// sample_lighting/lightshadercontrollers.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Note: creates a 2D screenspace texture that holds all shadow relevant data
//
#ifndef SAMPLE_LIGHTING_LIGHTSHADERCONTROLLERS_H
#define SAMPLE_LIGHTING_LIGHTSHADERCONTROLLERS_H


namespace rage {

class FastSpot4Lighting : public ShaderFragment
{
	struct FastSpot4LightingVarCache : public VarCache
	{
		grcEffectVar	m_positionXID;
		grcEffectVar	m_positionYID;
		grcEffectVar	m_positionZID;

		grcEffectVar	m_colorRID;
		grcEffectVar	m_colorGID;
		grcEffectVar	m_colorBID;

		grcEffectVar	m_DirectionID;
		grcEffectVar	m_FadeScaleID;
		grcEffectVar	m_FadeOffsetID;
		grcEffectVar	m_DropOffID;

		grcEffectVar	m_intensityID;
		grcEffectVar	m_amtID;

		grcEffectVar	m_ShadowMatrix1ID;
		grcEffectVar	m_ShadowTexture1ID;
		grcEffectVar	m_ShadowTexture2ID;
		grcEffectVar	m_ShadowTexture3ID;
		grcEffectVar	m_ShadowTexture4ID;
		grcEffectVar	m_ShadowTexture5ID;
		grcEffectVar	m_ShadowTexture6ID;
		grcEffectVar	m_ShadowTexture7ID;
		grcEffectVar	m_ShadowTexture8ID;

		grcEffectVar	m_ShadowAttenuationID;
		grcEffectVar	m_ShadowVSMEplisionID;
		grcEffectVar	m_ShadowDepthBiasID;
		grcEffectVar	m_AmbientOcclusionFadeInID;
		grcEffectVar	m_MaxShadowFadeID;

		void CacheEffectVars( grmShader& effect )
		{

			m_positionXID = effect.LookupVar( "Light4PosX" );
			m_positionYID = effect.LookupVar( "Light4PosY" );
			m_positionZID = effect.LookupVar( "Light4PosZ" );


			m_colorRID = effect.LookupVar( "Light4ColR" );
			m_colorGID = effect.LookupVar( "Light4ColG" );
			m_colorBID = effect.LookupVar( "Light4ColB" );  

			m_intensityID = effect.LookupVar( "Light4OneOverRange" ); 

			m_DirectionID = effect.LookupVar( "FastSpotDirection" );
			m_FadeScaleID = effect.LookupVar( "FastSpotFadeScale" );
			m_FadeOffsetID = effect.LookupVar( "FastSpotFadeOffset" );  
			m_DropOffID = effect.LookupVar( "FastSpotDropOff" );

			m_ShadowMatrix1ID = effect.LookupVar( "ShadowMatrix" );
			m_ShadowTexture1ID = effect.LookupVar( "Shadow1Texture" );
			m_ShadowTexture2ID = effect.LookupVar( "Shadow2Texture" );
			m_ShadowTexture3ID = effect.LookupVar( "Shadow3Texture" );
			m_ShadowTexture4ID = effect.LookupVar( "Shadow4Texture" );
			m_ShadowTexture5ID = effect.LookupVar( "Shadow5Texture" );
			m_ShadowTexture6ID = effect.LookupVar( "Shadow6Texture" );
			m_ShadowTexture7ID = effect.LookupVar( "Shadow7Texture" );
			m_ShadowTexture8ID = effect.LookupVar( "Shadow8Texture" );


			m_ShadowAttenuationID = effect.LookupVar( "gOneOverLightAttenuationEndx4" );
			m_ShadowVSMEplisionID = effect.LookupVar( "LightVSMEpsilonx4" );
			m_ShadowDepthBiasID = effect.LookupVar( "LightVSMBiasx4" );

			m_AmbientOcclusionFadeInID = effect.LookupVar( "AmbientOcclusionFadeIn");
			m_MaxShadowFadeID = effect.LookupVar("MaxShadowFade");

			//m_amtID = effect.LookupVar( "FastLightNumBatches" );
		}
	};
public:
	FastSpot4Lighting() : m_amtLights(0), m_setup( false )
	{
	}
	bool UsesFragment( grmShader& effect )
	{
		return ( effect.LookupVar( "FastSpotFadeScale" , false ) != 0 );
	}

	VarCache* CreateVariableCache()	{ return rage_new FastSpot4LightingVarCache; }

	void Reset()	{ m_setup = false; }

	void SetupShader( grmShader& effect, VarCache* cVars ) 
	{
		Assert( cVars );
		FastSpot4LightingVarCache* var = reinterpret_cast<FastSpot4LightingVarCache*>(cVars);

		if ( !m_setup )
		{
			return;
		}
		effect.SetVar( var->m_colorRID, m_colR, m_amtLights );
		effect.SetVar( var->m_colorGID, m_colG, m_amtLights  );
		effect.SetVar( var->m_colorBID, m_colB, m_amtLights  );

		effect.SetVar( var->m_intensityID, m_oneOverRangeSq, m_amtLights );
		//		effect.SetVar( var->m_amtID, m_amtLights );

		effect.SetVar( var->m_positionXID, m_positionX, m_amtLights );
		effect.SetVar( var->m_positionYID, m_positionY, m_amtLights  );
		effect.SetVar( var->m_positionZID, m_positionZ, m_amtLights  ); 

		effect.SetVar( var->m_DirectionID, m_direction, 3 * m_amtLights);
		effect.SetVar( var->m_FadeScaleID, m_spotScale,  m_amtLights);
		effect.SetVar( var->m_FadeOffsetID, m_spotOffset,  m_amtLights);
		effect.SetVar( var->m_DropOffID, m_spotDropOff, m_amtLights );

		effect.SetVar( var->m_ShadowMatrix1ID, m_ShadowMtx, 8);
		effect.SetVar( var->m_ShadowTexture1ID, m_ShadowTex[0]);
		effect.SetVar( var->m_ShadowTexture2ID, m_ShadowTex[1]);
		effect.SetVar( var->m_ShadowTexture3ID, m_ShadowTex[2]);
		effect.SetVar( var->m_ShadowTexture4ID, m_ShadowTex[3]);

		effect.SetVar( var->m_ShadowTexture5ID, m_ShadowTex[4]);
		effect.SetVar( var->m_ShadowTexture6ID, m_ShadowTex[5]);
		effect.SetVar( var->m_ShadowTexture7ID, m_ShadowTex[6]);
		effect.SetVar( var->m_ShadowTexture8ID, m_ShadowTex[7]);

		effect.SetVar( var->m_ShadowAttenuationID, m_ShadowAttenutation, Max_Fast_Lights);
		effect.SetVar( var->m_ShadowVSMEplisionID, m_ShadowVSMEplision, Max_Fast_Lights);
		effect.SetVar( var->m_ShadowDepthBiasID, m_ShadowDepthBias, Max_Fast_Lights);

		effect.SetVar( var->m_AmbientOcclusionFadeInID, m_AmbientOcclusionFadeIn );
		effect.SetVar( var->m_MaxShadowFadeID, m_MaxShadowFade );
	}

	void SetupShaderPass() 
	{}

	int MaxFastLights()		{ return Max_Fast_Lights; }

	// public interface
	void SetLightsFast4( atArray<Light>& list, float AmbOccFadeIn  ) 
	{ 
		//Assert( ( list.GetCount() & 0x3 )== 0 );
		m_amtLights = Max_Fast_Lights;

		int loopMax = list.GetCount() >> 2;
		for ( int i = 0, lidx = 0; i < loopMax; i++, lidx +=4 )
		{
			m_positionX[i] = Vector4( list[lidx].Position.x, list[lidx + 1].Position.x, list[lidx + 2].Position.x, list[lidx + 3].Position.x );
			m_positionY[i] = Vector4( list[lidx].Position.y, list[lidx + 1].Position.y, list[lidx + 2].Position.y, list[lidx + 3].Position.y );
			m_positionZ[i] = Vector4( list[lidx].Position.z, list[lidx + 1].Position.z, list[lidx + 2].Position.z, list[lidx + 3].Position.z );

			m_colR[i] = Vector4( list[lidx].Colour.x, list[lidx + 1].Colour.x, list[lidx + 2].Colour.x, list[lidx + 3].Colour.x );
			m_colG[i] = Vector4( list[lidx].Colour.y, list[lidx + 1].Colour.y, list[lidx + 2].Colour.y, list[lidx + 3].Colour.y );
			m_colB[i] = Vector4( list[lidx].Colour.z, list[lidx + 1].Colour.z, list[lidx + 2].Colour.z, list[lidx + 3].Colour.z );

			m_direction[ i * 3 + 0]  = Vector4( list[lidx].DirectionVector.x, list[lidx + 1].DirectionVector.x, list[lidx + 2].DirectionVector.x, list[lidx + 3].DirectionVector.x );
			m_direction[ i * 3 + 1]  = Vector4( list[lidx].DirectionVector.y, list[lidx + 1].DirectionVector.y, list[lidx + 2].DirectionVector.y, list[lidx + 3].DirectionVector.y );
			m_direction[ i * 3 + 2]  = Vector4( list[lidx].DirectionVector.z, list[lidx + 1].DirectionVector.z, list[lidx + 2].DirectionVector.z, list[lidx + 3].DirectionVector.z );


			list[lidx + 0].CalculateSpotScaleBias( m_spotScale[i].x, m_spotOffset[i].x );
			list[lidx + 1].CalculateSpotScaleBias( m_spotScale[i].y, m_spotOffset[i].y );
			list[lidx + 2].CalculateSpotScaleBias( m_spotScale[i].z, m_spotOffset[i].z );
			list[lidx + 3].CalculateSpotScaleBias( m_spotScale[i].w, m_spotOffset[i].w );

			m_spotDropOff[i] = Vector4( list[ lidx].DropOff, list[ lidx + 1 ].DropOff, list[ lidx + 2 ].DropOff, list[ lidx  + 3 ].DropOff );

			m_oneOverRangeSq[i] = Vector4( list[lidx].Intensity, list[lidx + 1].Intensity, list[lidx + 2].Intensity, list[lidx + 3].Intensity);
			m_oneOverRangeSq[i].InvertSafe();
		}

		for ( int i = loopMax; i < Max_Fast_Lights; i++ )
		{
			m_positionX[i] = m_positionY[i]  = m_positionZ[i] = Vector4( 100000.0f, 1000000000.0f, 1000000.0f, 1000000.0f );
			m_colR[i] = m_colG[i] = m_colB[i] = Vector4( 1.0f, 1.0f, 1.0f, 1.0f );
			m_oneOverRangeSq[i] = Vector4( 1.0f, 1.0f, 1.0f, 1.0f );
		}

		m_AmbientOcclusionFadeIn.x = AmbOccFadeIn;
		m_AmbientOcclusionFadeIn.y = 1.0f - AmbOccFadeIn;
	}
	void SetShadows( CShadowCache& shadows, atArray<Light>& list )
	{ 
		int loopMax = Min( list.GetCount() >> 2, Max_Fast_Lights );

		for ( int i = 0, lidx = 0; i < loopMax; i++, lidx +=4 )
		{

			for ( int j =0; j < 4; j++ )
			{
				m_ShadowMtx[ lidx + j ] = shadows[ lidx +j].GetMatrix();
				m_ShadowTex[ lidx + j] = shadows.GetTexture(); //shadows[ lidx +j].GetTexture();
			}
			m_ShadowAttenutation[i].x = 1.0f/shadows[lidx +0].GetRange();
			m_ShadowAttenutation[i].y = 1.0f/shadows[lidx +1].GetRange();
			m_ShadowAttenutation[i].z = 1.0f/shadows[lidx +2].GetRange();
			m_ShadowAttenutation[i].w = 1.0f/shadows[lidx +3].GetRange();

			m_ShadowDepthBias[i].x = m_ShadowAttenutation[i].x * shadows[lidx +0].GetStart() + list[lidx +0].GetShadowBias();
			m_ShadowDepthBias[i].y = m_ShadowAttenutation[i].y * shadows[lidx +1].GetStart() + list[lidx +1].GetShadowBias();
			m_ShadowDepthBias[i].z = m_ShadowAttenutation[i].z * shadows[lidx +2].GetStart() + list[lidx +2].GetShadowBias();
			m_ShadowDepthBias[i].w = m_ShadowAttenutation[i].w * shadows[lidx +3].GetStart() + list[lidx +3].GetShadowBias();

			m_ShadowDepthBias[i] = -m_ShadowDepthBias[i];
			m_ShadowVSMEplision[i] = Vector4( list[lidx +0].GetShadowEplision(), list[lidx +1].GetShadowEplision(), list[lidx +2].GetShadowEplision(), list[lidx +3].GetShadowEplision());
		}

		m_MaxShadowFade = shadows.GetSpotShadowMaxFade();
		m_setup = true;
	}
private:
	static const int Max_Fast_Lights = 3;

	Vector4		m_positionX[ Max_Fast_Lights ];
	Vector4		m_positionY[ Max_Fast_Lights ];
	Vector4		m_positionZ[ Max_Fast_Lights ];

	Vector4		m_colR[ Max_Fast_Lights ];
	Vector4		m_colG[ Max_Fast_Lights ];
	Vector4		m_colB[ Max_Fast_Lights ];

	Vector4		m_direction[ Max_Fast_Lights * 3 ];
	Vector4		m_spotScale[ Max_Fast_Lights ];
	Vector4		m_spotOffset[ Max_Fast_Lights ];
	Vector4		m_spotDropOff[ Max_Fast_Lights ];
	Vector4		m_oneOverRangeSq[ Max_Fast_Lights ];
	int			m_amtLights;


	// variance shadows 
	Matrix44			m_ShadowMtx[ 4* Max_Fast_Lights];
	const grcTexture*	m_ShadowTex[  4 * Max_Fast_Lights];

	Vector4 m_ShadowAttenutation[Max_Fast_Lights];
	Vector4 m_ShadowVSMEplision[Max_Fast_Lights];
	Vector4 m_ShadowDepthBias[Max_Fast_Lights];

	Vector2 m_AmbientOcclusionFadeIn;
	Vector2	m_MaxShadowFade;

	bool	m_setup;
};

#define USE_COMPRESSED_DIRECTION 1 
// 
// /sample_lighting.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

//---- shader controller for fast lighting
class FastLighting : public ShaderFragment
{
	struct FastLightingVarCache : public VarCache
	{
		grcEffectVar	m_positionXID;
		grcEffectVar	m_positionYID;
		grcEffectVar	m_positionZID;

		grcEffectVar	m_colorRID;
		grcEffectVar	m_colorGID;
		grcEffectVar	m_colorBID;

		grcEffectVar	m_intensityID;
		grcEffectVar	m_amtID;

		grcEffectVar	m_kdSplit;
		grcEffectVar	m_kdLeafVals;
		grcEffectVar	m_kdVSLeafVals;

		grcEffectVar	m_positionID;
		grcEffectVar	m_colorID;
		grcEffectVar	m_directionID;

		grcEffectVar	m_sortTextureID;
		grcEffectVar	m_sortTexture2ID;
		grcEffectVar	m_sortScaleID;
		grcEffectVar	m_sortBiasID;

		grcEffectVar	m_posTextureID;
		grcEffectVar	m_posScaleID;
		grcEffectVar	m_posBiasID;
		grcEffectVar	m_colTextureID;

		grcEffectVar	m_positionFillID;
		grcEffectVar	m_colorFillID;
		grcEffectVar	m_directionFillID;
		grcEffectVar	m_sortFillTextureID;

		void CacheEffectVars( grmShader& effect )
		{


			m_positionID = effect.LookupVar( "LightPositions" );
			m_colorID = effect.LookupVar("LightColors");
			m_directionID = effect.LookupVar( "LightDirections" );


			m_positionFillID = effect.LookupVar( "FillLightPositions", false  );
			m_colorFillID = effect.LookupVar("FillLightColors", false  );
			m_directionFillID = effect.LookupVar( "FillLightDirections" , false  );
			m_sortFillTextureID = effect.LookupVar( "FillLightSortTexture" , false  );

			m_sortTextureID = effect.LookupVar ("LightSortTexture" , false  );
			m_sortTexture2ID = effect.LookupVar ("LightSortTexture2" , false  );
			m_posTextureID = effect.LookupVar("PositionTexture", false  );
			m_sortScaleID = effect.LookupVar("LightSortScale", false  );
			m_sortBiasID = effect.LookupVar("LightSortBias", false  );

			m_posBiasID = effect.LookupVar("PositionBias", false  );
			m_posScaleID = effect.LookupVar("PositionScale", false  );
			m_colTextureID = effect.LookupVar("ColorT" , false  );

		}
	};

	struct FastLightingGlobalVarCache
	{
		
		grcEffectGlobalVar	m_positionID;
		grcEffectGlobalVar	m_colorID;
		grcEffectGlobalVar	m_directionID;

		grcEffectGlobalVar	m_sortTextureID;
		grcEffectGlobalVar	m_sortScaleID;
		grcEffectGlobalVar	m_sortBiasID;

		bool				m_set;

		FastLightingGlobalVarCache() : m_set(false)
		{}
		void CacheEffectVars()
		{

			m_positionID = grcEffect::LookupGlobalVar( "LightPositions" );
			m_colorID = grcEffect::LookupGlobalVar("LightColors");
#if !USE_COMPRESSED_DIRECTION
			m_directionID = grcEffect::LookupGlobalVar( "LightDirections" );
#endif

			m_sortTextureID = grcEffect::LookupGlobalVar ("LightSortTexture" , false  );
			m_sortScaleID = grcEffect::LookupGlobalVar("LightSortScale", false  );
			m_sortBiasID = grcEffect::LookupGlobalVar("LightSortBias", false  );
			m_set = true;
		}
		bool IsSet() { return m_set; }
	};

	FastLightingGlobalVarCache  m_GVars;
public:
	FastLighting() : m_amtLights(0), m_sortedTexture(0 ), m_posTexture(0)
	{
	}
	bool UsesFragment( grmShader& /*effect*/ )
	{
		return false;//	return ( effect.LookupVar( "LightDirections" , false ) != 0 );
	}

	VarCache* CreateVariableCache()	{ return rage_new FastLightingVarCache; }

	void SetGlobals()
	{
		if ( ! m_GVars.IsSet() )
		{
			m_GVars.CacheEffectVars();
		}
		grcEffect::SetGlobalVar( m_GVars.m_positionID, vals[0].m_position, vals[0].m_amtNormalLights );
		grcEffect::SetGlobalVar( m_GVars.m_colorID,  vals[0].m_color, vals[0].m_amtNormalLights );
#if !USE_COMPRESSED_DIRECTION
		grcEffect::SetGlobalVar(m_GVars.m_directionID, vals[0].m_directions, vals[0].m_amtNormalLights );
#endif
		grcEffect::SetGlobalVar( m_GVars.m_sortTextureID, m_sortedTexture );
		grcEffect::SetGlobalVar( m_GVars.m_sortScaleID, m_sortScale );
		grcEffect::SetGlobalVar(m_GVars.m_sortBiasID, m_sortBias );
	}


	void SetupShader( grmShader& effect, VarCache* cVars ) 
	{
		Assert( cVars );
		FastLightingVarCache* var = reinterpret_cast<FastLightingVarCache*>(cVars);


		effect.SetVar( var->m_positionID, vals[0].m_position, vals[0].m_amtNormalLights );
		effect.SetVar( var->m_colorID,  vals[0].m_color, vals[0].m_amtNormalLights );
#if !USE_COMPRESSED_DIRECTION
		effect.SetVar( var->m_directionID, vals[0].m_directions, vals[0].m_amtNormalLights );
#endif


		/*effect.SetVar( var->m_positionFillID, vals[1].m_position, vals[1].m_amtNormalLights );
		effect.SetVar( var->m_colorFillID,  vals[1].m_color, vals[1].m_amtNormalLights );
		effect.SetVar( var->m_directionFillID, vals[1].m_directions, vals[1].m_amtNormalLights );
		effect.SetVar( var->m_sortFillTextureID, m_sortedFillTexture ); */

		effect.SetVar( var->m_sortTextureID, m_sortedTexture );
		effect.SetVar( var->m_sortTexture2ID, m_sortedTexture2 );
		effect.SetVar( var->m_sortScaleID, m_sortScale );
		effect.SetVar( var->m_sortBiasID, m_sortBias );

		effect.SetVar( var->m_posTextureID, m_posTexture );
		effect.SetVar( var->m_posScaleID, m_posScale );
		effect.SetVar( var->m_posBiasID, m_posBias );
		effect.SetVar( var->m_colTextureID, m_colTexture );

	}


	void SetSortTexture( grcTexture*	tex, grcTexture* tex2, grcTexture* position, grcTexture* colors, grcTexture* sortedFillTexture, const Vector3& min, const Vector3& max )	
	{ 
		m_sortScale = ( max - min );
		m_sortScale.InvertSafe();
		m_sortBias = -min * m_sortScale;

		m_sortedTexture = tex; 
		m_sortedTexture2 = tex2;
		m_posTexture = position;
		m_colTexture = colors;
		m_sortedFillTexture = sortedFillTexture;
	}

	void SetupShaderPass() 
	{}

	int MaxFastLights()		{ return Max_Fast_Lights; }

	void SetLights( atArray<Light>& list , bool isDirect )
	{
#if USE_COMPRESSED_DIRECTION
		const bool UseCompressedDirection = true;
#else
		const bool UseCompressedDirection = true;
#endif


		LightVals* lgt = &(vals[0]);
		if ( !isDirect )
		{
			lgt = &(vals[1]);
		}
		for ( int i = 0; i < list.GetCount(); i++ )
		{

			lgt->m_position[i].SetVector3( list[i].Position );
			if ( !UseCompressedDirection )
			{
				lgt->m_color[i].SetVector3( list[i].Colour );
				lgt->m_color[i].w = list[i].Intensity;
				lgt->m_directions[i].SetVector3(list[i].DirectionVector );
				lgt->m_directions[i].w = list[i].CalculateConeCosineAngle();
			}
			else
			{
				Vector4 color;
				color.SetVector3( list[i].Colour * 255.0f );
				color.RoundToNearestInt();

				color.AddVector3( list[i].DirectionVector * 0.5f + Vector3( 0.5f, 0.5f, 0.5f) );
				color.w = list[i].Intensity;
				lgt->m_color[i] = color;
			}
			list[i].CalculateSpotScaleBias( lgt->m_directions[i].w , lgt->m_position[i].w);
		}

		// use black light as null light
		lgt->m_position[ list.GetCount()] = Vector4( 1000.0f, 1000000.0f, 10000.0f , 0.0f);
		lgt->m_color[ list.GetCount() ] = Vector4( 0.0f, 0.0f, 1.0f, 1.0f );
		lgt->m_directions[ list.GetCount() ] = Vector4( 0.0f, -1.0f, 0.0f, 0.0f );
		lgt->m_amtNormalLights = list.GetCount() + 1;

		if ( isDirect )
		{

			Vector3 minPos;
			Vector3 maxPos;
			GetBoundBoxPosition( list , minPos, maxPos );

			Vector4 bound = GetBoundSpherePosition( list );
			m_posScale = Vector3( 2.0f * bound.w, 2.0f *bound.w, 2.0f *bound.w );
			m_posBias.Set( bound.x - bound.w, bound.y - bound.w, bound.z - bound.w );
		}
	}

	// public interface
	void SetLightsFast4( atArray<Light>& list  ) 
	{ 
		//Assert( ( list.GetCount() & 0x3 )== 0 );
		m_amtLights = Max_Fast_Lights;

		int loopMax = list.GetCount() >> 2;
		for ( int i = 0, lidx = 0; i < loopMax; i++, lidx +=4 )
		{
			m_positionX[i] = Vector4( list[lidx].Position.x, list[lidx + 1].Position.x, list[lidx + 2].Position.x, list[lidx + 3].Position.x );
			m_positionY[i] = Vector4( list[lidx].Position.y, list[lidx + 1].Position.y, list[lidx + 2].Position.y, list[lidx + 3].Position.y );
			m_positionZ[i] = Vector4( list[lidx].Position.z, list[lidx + 1].Position.z, list[lidx + 2].Position.z, list[lidx + 3].Position.z );

			m_colR[i] = Vector4( list[lidx].Colour.x, list[lidx + 1].Colour.x, list[lidx + 2].Colour.x, list[lidx + 3].Colour.x );
			m_colG[i] = Vector4( list[lidx].Colour.y, list[lidx + 1].Colour.y, list[lidx + 2].Colour.y, list[lidx + 3].Colour.y );
			m_colB[i] = Vector4( list[lidx].Colour.z, list[lidx + 1].Colour.z, list[lidx + 2].Colour.z, list[lidx + 3].Colour.z );

			m_oneOverRangeSq[i] = Vector4( list[lidx].Intensity, list[lidx + 1].Intensity, list[lidx + 2].Intensity, list[lidx + 3].Intensity);
			m_oneOverRangeSq[i].InvertSafe();
		}

		for ( int i = loopMax; i < Max_Fast_Lights; i++ )
		{
			m_positionX[i] = m_positionY[i]  = m_positionZ[i] = Vector4( 100000.0f, 1000000000.0f, 1000000.0f, 1000000.0f );
			m_colR[i] = m_colG[i] = m_colB[i] = Vector4( 1.0f, 1.0f, 1.0f, 1.0f );
			m_oneOverRangeSq[i] = Vector4( 1.0f, 1.0f, 1.0f, 1.0f );
		}
	}

	void SetNodes( const LightList& lights, atArray<kdNode>& nodes, int& cnt, int nodeRef, int numLevels, int offset )
	{
		Assert( numLevels != 0);
		kdNode& n = nodes[ nodeRef ];
		kdNode& l = nodes[ n.left ]; 
		kdNode& r = nodes[ n.right ]; 

		m_kdSplits[ cnt ] = Vector4( n.split, l.split, r.split, 0.0f );

		if ( numLevels == 1 )		// is a leaf node so set correctly
		{
			m_kdSplits[ cnt++ ].w = (float)offset * 4.0f;

			int closestLights[8];
			LightGetStrongestNLightsInBox( lights.lights,  closestLights, l.lbox.GetCentre() , 8, l.lbox.min, l.lbox.max );		// bottom left
			m_kdleafVSLights[ m_leafCount] = Vector4( (float)closestLights[4], (float)closestLights[5], (float)closestLights[6] , (float)closestLights[7] );
			m_kdleafLights[ m_leafCount++] = Vector4( (float)closestLights[0], (float)closestLights[1], (float)closestLights[2] , (float)closestLights[3] );
			LightGetStrongestNLightsInBox( lights.lights,closestLights, l.rbox.GetCentre() , 8, l.rbox.min, l.rbox.max );		// top left
			m_kdleafVSLights[ m_leafCount] = Vector4( (float)closestLights[4], (float)closestLights[5], (float)closestLights[6] , (float)closestLights[7] );
			m_kdleafLights[ m_leafCount++] = Vector4( (float)closestLights[0], (float)closestLights[1], (float)closestLights[2] , (float)closestLights[3] );
			LightGetStrongestNLightsInBox( lights.lights,closestLights, r.lbox.GetCentre() , 8,  r.lbox.min, r.lbox.max );		// bottom right
			m_kdleafVSLights[ m_leafCount] = Vector4( (float)closestLights[4], (float)closestLights[5], (float)closestLights[6] , (float)closestLights[7] );
			m_kdleafLights[ m_leafCount++] = Vector4( (float)closestLights[0], (float)closestLights[1], (float)closestLights[2] , (float)closestLights[3] );
			LightGetStrongestNLightsInBox( lights.lights,closestLights, r.rbox.GetCentre() , 8, r.rbox.min, r.rbox.max );		// top right
			m_kdleafVSLights[ m_leafCount] = Vector4( (float)closestLights[4], (float)closestLights[5], (float)closestLights[6] , (float)closestLights[7] );
			m_kdleafLights[ m_leafCount++] = Vector4( (float)closestLights[0], (float)closestLights[1], (float)closestLights[2] , (float)closestLights[3] );
			return;
		}
		else
		{
			//Vector4 offV = Vector4( 2.0f, 1.0f, 1.0f , 0.0f );
			//offV.Scale((float)( numLevels / 2 ) );
			//offV.w =(float)offset + 1.0f; // not sure why I add to add one
			//m_kdIndices[ cnt++ ] = offV;  

			m_kdSplits[ cnt++ ].w = (float)1.0f;
		}

		numLevels--;
		SetNodes( lights, nodes, cnt, l.left, numLevels , offset );
		SetNodes( lights, nodes, cnt, l.right, numLevels , offset + ( numLevels  ));
		SetNodes( lights, nodes, cnt, r.left, numLevels , offset + 2 * ( numLevels  ) );
		SetNodes( lights, nodes, cnt, r.right, numLevels , offset + 3 * ( numLevels ) );
	}

	void SetKdTree( const LightList& lights, atArray<kdNode>& nodes, int numLevels )
	{
		m_numKdNodes = 0;
		m_leafCount = 0;
		SetNodes( lights, nodes, m_numKdNodes, m_numKdNodes, numLevels , 0  );
	}

private:
	static const int Max_Fast_Lights = 4;
	static const int Max_Normal_Lights = 64;

	Vector4		m_positionX[ Max_Fast_Lights ];
	Vector4		m_positionY[ Max_Fast_Lights ];
	Vector4		m_positionZ[ Max_Fast_Lights ];

	Vector4		m_colR[ Max_Fast_Lights ];
	Vector4		m_colG[ Max_Fast_Lights ];
	Vector4		m_colB[ Max_Fast_Lights ];

	Vector4		m_oneOverRangeSq[ Max_Fast_Lights ];

	struct LightVals
	{
		Vector4		m_position[ Max_Normal_Lights ];
		Vector4		m_color[ Max_Normal_Lights ];
		Vector4		m_directions[ Max_Normal_Lights ];
		int			m_amtNormalLights;
	};

	LightVals	vals[2];

	int			m_amtLights;

	static const int Max_Fast_KdNodes = 16;
	static const int Max_Fast_LeafNodes = 64;

	Vector4		m_kdSplits[ Max_Fast_KdNodes ];
	Vector4		m_kdIndices[ Max_Fast_KdNodes ];
	Vector4		m_kdleafLights[ Max_Fast_LeafNodes ];
	Vector4		m_kdleafVSLights[Max_Fast_LeafNodes ];

	int			m_numKdNodes;
	int			m_leafCount;

	grcTexture*	m_sortedTexture;
	grcTexture*	m_sortedTexture2;
	grcTexture* m_sortedFillTexture;

	Vector3		m_sortScale;
	Vector3		m_sortBias;

	grcTexture*	m_posTexture;
	grcTexture*	m_colTexture;

	bool		m_useShadows;

	// compression stuff
	Vector3 m_posScale;
	Vector3 m_posBias;



};

class ManyShadows: public ShaderFragment
{
	struct ManyShadowsVarCache : public VarCache
	{
		grcEffectVar	ShadowMatrixXID;
		grcEffectVar	ShadowMatrixYID;
		grcEffectVar	ShadowMatrixWID;

		grcEffectVar	ShadowMatCompXID;

		grcEffectVar	ShadowSettingsID;
		grcEffectVar	ShadowTextureID;
		grcEffectVar	ShadowTexScaleID;
		grcEffectVar	m_MaxShadowFadeID;
		grcEffectVar	m_AmbientOcclusionFadeInID;
		grcEffectVar	m_BumpinessID;

		grcEffectVar	m_DirectionDirID;
		grcEffectVar	m_DirectionColID;


		void CacheEffectVars( grmShader& effect )
		{
			ShadowMatrixXID = effect.LookupVar("ShadowMatrixX" );
			ShadowMatrixYID = effect.LookupVar("ShadowMatrixY" );
			//ShadowMatrixWID = effect.LookupVar("ShadowMatrixW" );
			//ShadowMatCompXID = effect.LookupVar("ShadowMatCompX");
			ShadowSettingsID = effect.LookupVar("ShadowSettings" );
			ShadowTextureID =  effect.LookupVar("ShadowAtlasTexture" );
			ShadowTexScaleID = effect.LookupVar("ShadowTexScale" );
			m_MaxShadowFadeID = effect.LookupVar("MaxShadowFade");
			m_AmbientOcclusionFadeInID = effect.LookupVar( "AmbientOcclusionFadeIn");
			m_BumpinessID = effect.LookupVar("Bumpiness");
			m_DirectionDirID = effect.LookupVar("DirectionDir");
			m_DirectionColID = effect.LookupVar("DirectionCol");
		}
	};
	struct ManyShadowsGlobalVarCache
	{
		grcEffectGlobalVar	ShadowMatrixXID;
		grcEffectGlobalVar	ShadowMatrixYID;

		grcEffectGlobalVar	ShadowSettingsID;
		grcEffectGlobalVar	ShadowTextureID;
		grcEffectGlobalVar	m_MaxShadowFadeID;
		bool				m_set;

		ManyShadowsGlobalVarCache() : m_set(false)
		{}
		void CacheEffectVars()
		{
			ShadowMatrixXID = grcEffect::LookupGlobalVar( "manylights_ShadowMatrixX" );
			ShadowMatrixYID = grcEffect::LookupGlobalVar( "manylights_ShadowMatrixY" );
			ShadowSettingsID = grcEffect::LookupGlobalVar( "manylights_ShadowSettings" );
			ShadowTextureID = grcEffect::LookupGlobalVar( "ShadowAtlasTexture" );
			m_MaxShadowFadeID = grcEffect::LookupGlobalVar( "MaxShadowFade");

			m_set = true;
		}
		bool IsSet() { return m_set; }
	};
	ManyShadowsGlobalVarCache	m_GVars;

public:
	ManyShadows() : m_amtLights(0), m_shadowTexture(0)
	{
	}
	bool UsesFragment( grmShader& /*effect*/ )
	{
		return false; //return ( effect.LookupVar( "ShadowSettings" , false ) != 0 );
	}

	VarCache* CreateVariableCache()	{ return rage_new ManyShadowsVarCache; }

	void SetGlobals()
	{
		if ( !m_amtLights )
		{
			return;
		}
		if (!m_GVars.IsSet() )
		{
			m_GVars.CacheEffectVars();
		}
		grcEffect::SetGlobalVar( m_GVars.ShadowMatrixXID, ShadowMatrixX, m_amtLights );
		grcEffect::SetGlobalVar( m_GVars.ShadowMatrixYID,  ShadowMatrixY, m_amtLights );
		grcEffect::SetGlobalVar( m_GVars.ShadowSettingsID, ShadowSettings, m_amtLights );
		grcEffect::SetGlobalVar( m_GVars.ShadowTextureID, m_shadowTexture );
		Vector4 sFade( m_shadowFade.x, m_shadowFade.y, 0.0f, 0.0f );
		grcEffect::SetGlobalVar( m_GVars.m_MaxShadowFadeID, sFade );

	}
	void SetupShader( grmShader& effect, VarCache* cVars ) 
	{
		Assert( cVars );
		ManyShadowsVarCache* var = reinterpret_cast<ManyShadowsVarCache*>(cVars);

		effect.SetVar( var->ShadowMatrixXID, ShadowMatrixX, m_amtLights );
		effect.SetVar( var->ShadowMatrixYID,  ShadowMatrixY, m_amtLights );
		//effect.SetVar( var->ShadowMatrixWID, ShadowMatrixW, m_amtLights );
		effect.SetVar( var->ShadowSettingsID, ShadowSettings, m_amtLights );
		//effect.SetVar( var->ShadowMatCompXID ,ShadowMatCompX, m_amtLights );
		effect.SetVar( var->ShadowTextureID, m_shadowTexture );
		effect.SetVar( var->ShadowTexScaleID, m_shadScale );
		effect.SetVar( var->m_MaxShadowFadeID, m_shadowFade );
		effect.SetVar( var->m_AmbientOcclusionFadeInID, m_AmbientOcclusionFadeIn );
		effect.SetVar( var->m_BumpinessID, m_bump );

		effect.SetVar( var->m_DirectionDirID, m_dir );
		effect.SetVar( var->m_DirectionColID, m_dirCol );

	}

	void SetupShaderPass() 
	{}

	void SetShadows( LightList& lights, CShadowCache& shadows, atArray<Light>& list  , float ambOcc, float bump ) 
	{
		m_shadowTexture = shadows.GetTexture();
		for ( int i = 0; i < list.GetCount(); i++ )
		{
			const CShadowedSpotLight& spotShadow = shadows[i]; 
			Matrix44	mat = spotShadow.GetMatrix();
			mat.Transpose();

			ShadowMatrixX[i] = mat.a;
			ShadowMatrixY[i] = mat.b;
			ShadowMatrixW[i] = mat.d;


			Vector4		settings( 1.0f/ spotShadow.GetRange(), 0.0f, 0.0f, 0.0f);
			settings.y = -(settings.x * spotShadow.GetStart() + list[i].GetShadowBias());
			list[i].CalculateSpotScaleBias( settings.z, settings.w);

			float compZ;
			ShadowMatCompX[i] = spotShadow.GetCompressedSpotMatrix( compZ);
			ShadowSettings[i] = settings;
		}
		m_shadScale = shadows.GetShadowTexScale();
		m_shadowFade = shadows.GetSpotShadowMaxFade();
		m_amtLights = list.GetCount();

		m_AmbientOcclusionFadeIn.x = ambOcc;
		m_AmbientOcclusionFadeIn.y = 1.0f - ambOcc;
		m_bump = bump;

		lights.GetDirectional( m_dir, m_dirCol );

	}


private:
	static const int Max_Normal_Lights = 64;

	Vector4		ShadowMatrixX[ Max_Normal_Lights];
	Vector4		ShadowMatrixY[ Max_Normal_Lights];
	Vector4		ShadowMatrixW[ Max_Normal_Lights];
	Vector4		 ShadowSettings[Max_Normal_Lights];
	Vector2		m_shadScale;
	Vector2		m_shadowFade;
	Vector2		m_AmbientOcclusionFadeIn;

	Vector4		ShadowMatCompX[ Max_Normal_Lights];

	Vector3		m_dir;
	Vector3		m_dirCol;

	const grcTexture*	 m_shadowTexture;
	int			m_amtLights;
	float		m_bump;
};


class LightingComposite: public ShaderFragment
{
	struct LightingCompositeVarCache : public VarCache
	{


		grcEffectVar	AmbientOcclusionScaleID;
		grcEffectVar	BounceStrengthID;
		grcEffectVar	EnvironmentStrengthID;
		grcEffectVar	DirectStrengthID;
		grcEffectVar	DisplayLightingOnlyID;
		grcEffectVar	skyColorID;
		grcEffectVar	grdColorID;
		grcEffectVar	bounceFallOffID;

		void CacheEffectVars( grmShader& effect )
		{
			AmbientOcclusionScaleID = effect.LookupVar("AmbientOcclusionScale" );
			BounceStrengthID = effect.LookupVar("BounceStrength" );
		//	EnvironmentStrengthID = effect.LookupVar("EnvironmentStrength" );
		//	DirectStrengthID =  effect.LookupVar("DirectStrength" );
			DisplayLightingOnlyID = effect.LookupVar("DisplayLightingOnly" );
			skyColorID = effect.LookupVar("SkyColor");
			grdColorID = effect.LookupVar("GroundColor");
		//	bounceFallOffID = effect.LookupVar("BounceFallOff", false);
		}
	};
	struct LightingCompositeGlobalVarCache
	{
		grcEffectGlobalVar	skyColorID;
		grcEffectGlobalVar	grdColorID;

		bool				m_set;

		LightingCompositeGlobalVarCache() : m_set(false)
		{}
		void CacheEffectVars()
		{
			skyColorID = grcEffect::LookupGlobalVar( "manylights_SkyColor" );
			grdColorID = grcEffect::LookupGlobalVar( "manylights_GroundColor" );
			m_set = true;
		}
		bool IsSet() { return m_set; }
	};
	LightingCompositeGlobalVarCache	m_GVars;
public:
	LightingComposite() 
		:	m_ambOccScale( 1.0f, 0.1f ),
		m_bounceStrength( 2.5f),
		m_envStrength( 0.3f ),
		m_direct( 1.0f ),
		m_lightingOnly( false ),
		m_GroundColor(0.02f, 0.02f, 0.2f ),
		m_SkyColor(0.05f, 0.05f, 0.0f )
	{
	}
	bool UsesFragment( grmShader& /*effect*/ )
	{
		return false;//return ( effect.LookupVar( "BounceStrength" , false ) != 0 );
	}

	VarCache* CreateVariableCache()	{ return rage_new LightingCompositeVarCache; }

	void SetGlobals()
	{
		if (!m_GVars.IsSet() )
		{
			m_GVars.CacheEffectVars();	
		}
		Vector4	sky( m_SkyColor.x, m_SkyColor.y, m_SkyColor.z, m_direct);
		Vector4 grd( m_GroundColor.x, m_GroundColor.y, m_GroundColor.z, m_envStrength);
		grcEffect::SetGlobalVar( m_GVars.skyColorID, sky );
		grcEffect::SetGlobalVar( m_GVars.grdColorID, grd );
	}
	void SetupShader( grmShader& effect, VarCache* cVars ) 
	{
		Assert( cVars );
		LightingCompositeVarCache* var = reinterpret_cast<LightingCompositeVarCache*>(cVars);

		effect.SetVar( var->AmbientOcclusionScaleID, m_ambOccScale );
		effect.SetVar( var->BounceStrengthID,  m_bounceStrength );
	//	effect.SetVar( var->EnvironmentStrengthID, m_envStrength );
	//	effect.SetVar( var->DirectStrengthID, m_direct );
		effect.SetVar( var->DisplayLightingOnlyID, m_lightingOnly ? 1.0f : 0.0f );
		effect.SetVar( var->DirectStrengthID, m_direct );
		effect.SetVar( var->DisplayLightingOnlyID, m_lightingOnly ? 1.0f : 0.0f );
		effect.SetVar( var->skyColorID, m_SkyColor );
		effect.SetVar( var->grdColorID, m_GroundColor );
	//	effect.SetVar( var->bounceFallOffID, 1.0f/m_bounceFallOff );
	}


	void SetupShaderPass() 
	{}

#if __BANK
	void AddWidgets(bkBank* bank )	
	{

		bank->PushGroup("Compositing Lighting Layers");			// add lighting group
		bank->AddSlider( "Ambient Occlusion Scale", &m_ambOccScale.x, 0.001f, 2.0f, 0.01f );
		bank->AddSlider( "Ambient Occlusion Offset", &m_ambOccScale.y, 0.0f, 1.0f, 0.01f );
		bank->AddSlider( "Bounce Light Strength", &m_bounceStrength, 0.01f, 5.0f, 0.01f );
		bank->AddSlider( "Environment Light Strength", &m_envStrength, 0.01f, 5.0f, 0.01f );
		bank->AddSlider( "Direct Light Strength", &m_direct, 0.01f, 5.0f, 0.01f );
		bank->AddToggle( "Display Lighting only", &m_lightingOnly );

		bank->AddColor( "Env Sky Color", &m_SkyColor );
		bank->AddColor( "Env Ground Color", &m_GroundColor );

		//bank->AddSlider( "m_bounceFallOff", &m_bounceFallOff, 0.01f, 100.0f, 0.01f );
		
		bank->PopGroup();
	}
#endif

private:
	Vector3	m_SkyColor;
	Vector3 m_GroundColor;
	Vector2	m_ambOccScale;
	float	m_bounceStrength;
	float	m_envStrength;
	float	m_direct;
	bool	m_lightingOnly;

};

};

#endif
