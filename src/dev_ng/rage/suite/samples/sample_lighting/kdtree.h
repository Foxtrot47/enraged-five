// 
// sample_lighting/kdtree.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Note: creates a 2D screenspace texture that holds all shadow relevant data
//

#ifndef SAMPLE_LIGHTING_KDTREE_H
#define SAMPLE_LIGHTING_KDTREE_H

// RAGE_QA:NO_NAMESPACE_RAGE



struct kdNode
{
	float		split;
	int			left;
	int			right;
	AABox		lbox;
	AABox		rbox;

	void Draw()
	{
		DrawAABB( lbox.min, lbox.max, Color32( 0.1f, 0.1f, 1.0f, 1.0f));
		DrawAABB( rbox.min, rbox.max, Color32( 0.1f, 1.0f, 1.0f, 1.0f));
	}
};

template<class T, int AMOUNT>
class SphereTree
{
	atArray<Vector4>		m_spheres;
	atArray<kdNode>			m_nodes;

	float CalculateProperSplitPointMedian( atArray<T>& list, int index, float split )
	{
		const int MaximumLightsSorting = 128;
		KeyIndexPair	MedianList[ MaximumLightsSorting ];
		for ( int i = 0; i < list.GetCount(); i++ )
		{
			MedianList[i] = KeyIndexPair( list[i].GetSphere()[index], i ) ;
		}
		std::sort( MedianList, MedianList + list.GetCount() );
		KeyIndexPair med = MedianList[ list.GetCount()/2 ];

		// decide when side of sphere depending on position of centre split plane
		float radius = list[ med.value() ].GetSphere().w;
		float newsplit;
		if ( med.Key() < split )
		{
			newsplit = med.Key() + radius;		// on left side
		}
		else
		{
			newsplit = med.Key() - radius;		// on right side
		}
		return newsplit;
	}
	float CalculateProperSplitPoint( atArray<T>& list, int index, float split )
	{
		Assert( list.GetCount() );

		float minLeft = FLT_MAX;
		float	  minLeftSplit = 0.0f;
		float minRight = FLT_MAX;
		float  minRightSplit = 0.0f;
		int  leftCount = 0;

		for ( int i = 0; i < list.GetCount(); i++ )
		{
			float c = list[i].GetSphere()[index];
			float d = c - split;
			d *=d;

			if ( c < split )
			{
				leftCount++;
				if ( minLeft > d)
				{
					minLeft = d;
					minLeftSplit = c + list[i].GetSphere().w;
				}
			}
			else
			{
				if ( minRight > d)
				{
					minRight = d;
					minRightSplit = c - list[i].GetSphere().w;
				}
			}
		}
		float newsplit;
		if ( leftCount > (list.GetCount() / 2) ) // more on left side
		{
			newsplit = minLeftSplit;
		}
		else
		{
			newsplit = minRightSplit;
		}
		return newsplit;
	}


	int SplitTree( atArray<T>& list, int depth, int maxDepth, bool splitXAxias, atArray<Vector4>& results, int& nIndex , const AABox& box  )
	{
		//Assert( list.GetCount() > 0 );
		if ( depth == maxDepth ) //|| list.GetCount() == 1 )
		{
			Vector4 bound = box.ToSphere();
			if ( list.GetCount() != 0 )
			{
				bound = GetBoundSphere( list);;
			}
			bound.w += 0.1f;	// gravy for drawing.
			results.Grow() = bound;
			return nIndex;
		}



		AABox bound;
		if ( list.GetCount() )
		{
			bound = GetBoundBox( list  );
		}
		else
		{
			bound = box;
		}

		int index = splitXAxias ? 0 : 2 ;
		float split = bound.GetCentre()[ index ];

		if ( list.GetCount())
		{
			split =CalculateProperSplitPointMedian( list, index, split );
		}

		m_nodes[ nIndex].split = split;

		AABox leftBox;
		AABox rightBox;

		Vector3 maxMid = box.max;
		Vector3 minMid = box.min;
		maxMid[ index ] = split;
		minMid[ index ] = split;
		leftBox.Set( box.min, maxMid);
		rightBox.Set( minMid,  box.max );

		m_nodes[ nIndex ].lbox = leftBox;
		m_nodes[ nIndex ].rbox = rightBox;

		atArray<T>	left;
		atArray<T>	right;
		for ( int i = 0; i < list.GetCount(); i++ )
		{
			if ( list[i].GetSphere()[index] < split )
			{
				left.Grow() = list[i];
			}
			else
			{
				right.Grow() = list[i];
			}
		}

		splitXAxias = !splitXAxias;
		int nodeNum = nIndex;
		nIndex++;
		m_nodes[nodeNum].left = SplitTree( left, depth+ 1 , maxDepth, splitXAxias, results ,  nIndex, leftBox );
		m_nodes[ nodeNum].right = SplitTree( right, depth + 1, maxDepth, splitXAxias, results, nIndex, rightBox );

		return nodeNum;
	}

public:
	void Create( atArray<T>& list )
	{
		m_spheres.clear();

		int i = AMOUNT - 1;
		int depth= 0;
		while( i )
		{
			depth++;
			i >>= 1;
		}
		int nIndex = 0;
		m_nodes.Resize( AMOUNT - 1 );

		AABox bound;
		if ( list.GetCount() )
		{
			GetBoundBox( list, bound.min, bound.max );
			SplitTree(list, 0, depth, true, m_spheres,nIndex, bound );
		}
		//Assert( m_spheres.GetCount() ==  Min( AMOUNT, list.GetCount()) );
	}

	void Draw()
	{
		bool drawSpheres = false;

		if ( drawSpheres )
		{
			for ( int i = 0; i < m_spheres.GetCount() ; i++ )
			{
				DrawSphere( m_spheres[i], Color32( 2.0f, 2.0f, 2.0f, 1.0f ) );
			}
		}

		for ( int i = 0; i < m_nodes.GetCount(); i++ )
		{
			m_nodes[i].Draw();
		}
	}

	atArray<kdNode>& nodes() { return m_nodes; }
};




#endif
