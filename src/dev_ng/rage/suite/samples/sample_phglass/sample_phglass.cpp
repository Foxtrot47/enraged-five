// 
// /sample_breakableglasstest.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#include "system/main.h"

#if 0
#include "sample_rmcore/sample_rmcore.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "breakableglass/breakable.h"
#include "file/asset.h"
#include "grcore/im.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grmodel/setup.h"
#include "grmodel/shaderfx.h"
#include "input/mouse.h"
#include "math/random.h"
#include "phbound/boundbvh.h"
#include "phcore/materialmgrimpl.h"
#include "pheffects/mouseinput.h"
#include "phglass/glass.h"
#include "phglass/glassinstance.h"
#include "physics/inst.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "grprofile/drawmanager.h"
#include "system/namedpipe.h"
#include "system/param.h"
#include "system/timemgr.h"

using namespace rage;

PARAM(pipeclient,"[sample_immediatemode] specify that this instance of the executable will send IM commands");
PARAM(pipeserver,"[sample_immediatemode] specify that this instance of the executable will receive IM commands");
PARAM(perftest,"[sample_immediatemode] repeat render 1000 times for performance testing");
PARAM(file, "[sample_breakableblass] required by sample_rmcore");

namespace rage {
EXT_PFD_DECLARE_GROUP(Physics);
EXT_PFD_DECLARE_GROUP(Bounds);
}

//Demonstrate adjusting properties of the piece
void SetShardTypeFlag(bgPhysics &physics, float &)
{
	//Get the box's size
	Vector3 size(physics.GetSize());
	float volume(size[0] * size[1] * size[2]);

	//Set the mass of the glass equal to ten times its volume
	//(making the density ten, and uniform for all pieces)
	physics.SetMass(volume * 0.01f);

	//Set archetype type flags (this does nothing,
	//but we could set them to something useful)
	if (phCollider *pCollider = physics.GetCollider())
	{
		phArchetype &archetype(*pCollider->GetInstance()->GetArchetype());
		archetype.SetTypeFlag(2, true);
		archetype.SetTypeFlag(1, false);
	}
}

class bgSampleManager : public ragesamples::rmcSampleManager
{
public: 
	bgSampleManager() : m_IsServer(false)
		, m_Pipe(NULL)
		, m_pShader(NULL)
		, m_PaneHandle(-1)
	{
#if __BANK
		const char* param;
		if (PARAM_pipeclient.Get(param))
		{
			Printf("Connecting to server...");
			m_IsServer=false;
			m_Pipe=rage_new sysNamedPipe;
			while (m_Pipe->Open(bkRemotePacket::GetRagSocketPort(bkRemotePacket::SOCKET_OFFSET_USER0))!=true);
			Displayf("connected.");
		}
# if __WIN32PC
		else if (PARAM_pipeserver.Get(param))
		{
			m_IsServer=true;
			Printf("Waiting for client to connect...");
			m_Pipe=rage_new sysNamedPipe;
			while (m_Pipe->Create(bkRemotePacket::GetRagSocketPort(bkRemotePacket::SOCKET_OFFSET_USER0))!=true);
			GRCDEVICE.SetBlockOnLostFocus(false);
			Displayf("client connected.");
		}
# endif
#endif
	}

#define kPaneSize 5.f
#define kPaneThickness 0.1f
	void InitClient()
	{
		// Base InitClient()
		grcSampleManager::InitClient();
		// setup Glass Manager
		SGlassManager::Instantiate();
		SGlassManager::InstanceRef().InitClass(MakeFunctor(&SetShardTypeFlag));
		ASSET.PushFolder("sample_breakableglass");
		m_pDiffuse = grcTextureFactory::CreateTexture("GlassDiffuseAlpha");
		m_pSpecular = grcTextureFactory::CreateTexture("GlassSpecular");
		m_pEnvironment = grcTextureFactory::CreateTexture("GlassEnvironment");
		ASSET.PopFolder();

		// map input
		m_Mapper.Map(IOMS_MOUSE_BUTTON,ioMouse::MOUSE_LEFT,m_Shoot);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_CONTROL, m_Ctrl);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_ALT, m_Alt);

		// Create glass geometry
		// first we need a flexible vertex format to match our shader
		grcFvf fvf;
		fvf.SetPosChannel(true, grcFvf::grcdsFloat3);
		fvf.SetDiffuseChannel(true, grcFvf::grcdsColor);
		fvf.SetTextureChannel(0, true, grcFvf::grcdsFloat2);
		fvf.SetTextureChannel(1, true, grcFvf::grcdsFloat2);
		fvf.SetTextureChannel(2, true, grcFvf::grcdsFloat2);
		fvf.SetNormalChannel(true, grcFvf::grcdsFloat3);
		grcVertexBuffer* pVertexBuffer = grcVertexBuffer::Create(5, fvf, __PS3);
		{
			grcVertexBufferEditor editor(pVertexBuffer);
			Vector3 normal(0.f, 0.f, 1.f);
			editor.SetNormal(0, normal);
			editor.SetNormal(1, normal);
			editor.SetNormal(2, normal);
			editor.SetNormal(3, normal);
			editor.SetNormal(4, normal);
			editor.SetPosition(0, Vector3(-kPaneSize/2.f,  0.f,       0.f));
			editor.SetPosition(1, Vector3( kPaneSize/2.f,  0.f,       0.f));
			editor.SetPosition(2, Vector3( kPaneSize/2.f,  kPaneSize, 0.f));
			editor.SetPosition(3, Vector3(-kPaneSize/2.f,  kPaneSize, 0.f));
			editor.SetPosition(4, Vector3(0.f, 0.f, kPaneThickness)); // just for thickness
			editor.SetUV(0, 0, Vector2(0.f, 0.f));
			editor.SetUV(0, 1, Vector2(0.f, 0.f));
			editor.SetUV(1, 0, Vector2(1.f, 0.f));
			editor.SetUV(1, 1, Vector2(1.f, 0.f));
			editor.SetUV(2, 0, Vector2(1.f, 1.f));
			editor.SetUV(2, 1, Vector2(1.f, 1.f));
			editor.SetUV(3, 0, Vector2(0.f, 1.f));
			editor.SetUV(3, 1, Vector2(0.f, 1.f));
			editor.SetUV(4, 0, Vector2(0.f, 0.f));
			editor.SetUV(4, 1, Vector2(0.f, 0.f));
		}
		grcIndexBuffer* pIndexBuffer = grcIndexBuffer::Create(6);
		u16* pIndices = pIndexBuffer->LockRW();
		pIndices[0] = 0;
		pIndices[1] = 3;
		pIndices[2] = 2;
		pIndices[3] = 0;
		pIndices[4] = 2;
		pIndices[5] = 1;
		pIndexBuffer->Unlock();

		m_pShader = rage_new grmShaderFx;
		m_pShader->Load("rage_breakableglass");

		// set texture vars (normally artist-specified)
		SetShaderTexureVar(m_pDiffuse,     "DiffuseTex");
		SetShaderTexureVar(m_pSpecular,    "SpecularTex");
		SetShaderTexureVar(m_pEnvironment, "EnvironmentTex");

		m_PaneHandle = SGlassManager::InstanceRef().AllocateGlass(0, m_pShader, pVertexBuffer, pIndexBuffer);
		delete pIndexBuffer;
		delete pVertexBuffer;

		SGlassManager::InstanceRef().HitGlass(m_PaneHandle, 0, M34_IDENTITY, Vector3(0.f, kPaneSize/2.f, 0.f), Vector3(0.f, 0.f, 1.f));

		phConfig::EnableRefCounting();

#if __PFDRAW
		const int pfDrawBufferSize = 2000000;
		GetRageProfileDraw().Init(pfDrawBufferSize);
		GetRageProfileDraw().SetEnabled(true);
		PFD_GROUP_ENABLE(Physics, true);
#endif

		phLevelNew::SetActiveInstance(rage_new phLevelNew);

		const Vec3V worldExtentsMin(-999.0f,-999.0f,-999.0f);
		const Vec3V worldExtentsMax(999.0f,999.0f,999.0f);
		PHLEVEL->SetExtents(worldExtentsMin,worldExtentsMax);
		PHLEVEL->SetMaxObjects(500);
		const int maxNumActiveObjects = 100;
		PHLEVEL->SetMaxActive(100);
		PHLEVEL->SetNumOctreeNodes(1000);
		PHLEVEL->Init();

		phSimulator::InitClass();
		phSimulator::SetActiveInstance(rage_new phSimulator);
		PHSIM->Init(PHLEVEL,maxNumActiveObjects);

		ASSET.PushFolder("materials");
		phMaterialMgrImpl<phMaterial>::Create();
		MATERIALMGR.Load();
		ASSET.PopFolder();

#if __BANK
		PHSIM->AddWidgets(BANKMGR.CreateBank("rage - Physics"));
#endif // __BANK

		m_GlassPhysics.SetBreakable(SGlassManager::InstanceRef().GetGlass(m_PaneHandle));
		phBound* bound = m_GlassPhysics.RegenerateBound();
		m_Archetype.SetBound(bound);
		m_Archetype.SetIncludeFlag(u32(-2), false);
//		m_Archetype.SetTypeFlag(2, true);
		m_Inst.SetArchetype(&m_Archetype);
		m_Inst.SetMatrix(Mat34V(V_IDENTITY));
		m_Inst.SetGlass(&m_GlassPhysics);

		m_MouseInput = rage_new phMouseInput;
		m_MouseInput->EnableLeftClick();
		m_MouseInput->SetLeftClickMode(phMouseInput::IMPULSE);

		PHSIM->AddFixedObject(&m_Inst);
	}

	void ShutdownClient()
	{
		delete m_MouseInput;

		delete m_pShader;
		m_pShader=NULL;
		SGlassManager::InstanceRef().ShutdownClass();
		SGlassManager::Destroy();

		PHSIM->DeleteObject(m_Inst.GetLevelIndex());
		m_Inst.SetArchetype(NULL, false);

		phLevelNew* levelNew = PHLEVEL;
		levelNew->Shutdown();
		delete levelNew;

		delete PHSIM;
		phSimulator::SetActiveInstance(NULL);

		MATERIALMGR.Destroy();

#if __PFDRAW
		GetRageProfileDraw().Shutdown();
#endif

		phSimulator::ShutdownClass();

		LastSafeRelease(m_pDiffuse);
		LastSafeRelease(m_pSpecular);
		LastSafeRelease(m_pEnvironment);
		grcSampleManager::ShutdownClient();
	}

	void InitLights()
	{
		m_LightMode = grclmNone;
		grcSampleManager::InitLights();
	}

	void InitCamera()
	{
		grcSampleManager::InitSampleCamera(Vector3(-3.113f, 6.368f, -9.9586f), Vector3(0.f, 0.f, 0.f));
	}

	void InitSetup()
	{
		m_Setup = rage_new grmSetup;
	}

#if __BANK
	void AddWidgetsClient()
	{
		m_pShader->AddWidgets(BANKMGR.CreateBank("rage_breakableglass shader"));
		bkBank& bk = BANKMGR.CreateBank("rage - SampleBreakableGlass");
		SGlassManager::InstanceRef().AddWidgets(bk);
	}
#endif

	void UpdateClient()
	{
		// Update user input
		m_Mapper.Update();

		if (m_Shoot.IsPressed() && m_Alt.IsUp() && (m_PaneHandle >= 0))
		{
			// Reset glass pane if requested
			if (m_Ctrl.IsDown())
			{
				SGlassManager::InstanceRef().ResetGlass(m_PaneHandle);

				// Get the line segment in world coords from the near clip plane to the far clip plane
				// at the current mouse location
				Vector3 mouseNear, mouseFar;
				grcViewport::GetCurrent()->ReverseTransform(
					float(ioMouse::GetX()), 
					float(ioMouse::GetY()),
					mouseNear, mouseFar);
				// intersect that segment with the plane z=0:
				float denom = 1.f / (mouseFar.z - mouseNear.z);
				Vector3 intersect = mouseFar.z * denom * mouseNear - mouseNear.z * denom * mouseFar;

				SGlassManager::InstanceRef().HitGlass(
					m_PaneHandle, 
					0, 
					M34_IDENTITY, 
					intersect, 
					mouseFar - mouseNear);

				m_Archetype.SetBound(NULL);

				phBound* bound = m_GlassPhysics.RegenerateBound();

				m_Archetype.SetBound(bound);
			}

#if 0
			// Get the line segment in world coords from the near clip plane to the far clip plane
			// at the current mouse location
			Vector3 mouseNear, mouseFar;
			grcViewport::GetCurrent()->ReverseTransform(
				static_cast<float>(ioMouse::GetX()), 
				static_cast<float>(ioMouse::GetY()),
				mouseNear, mouseFar);
			// intersect that segment with the plane z=0:
			float denom = 1.f / (mouseFar.z - mouseNear.z);
			Vector3 intersect = mouseFar.z * denom * mouseNear - mouseNear.z * denom * mouseFar;

			SGlassManager::InstanceRef().HitGlass(
				m_PaneHandle, 
				0, 
				M34_IDENTITY, 
				intersect, 
				mouseFar - mouseNear);
#endif
		}

		m_MouseInput->Update(false);

		PHSIM->Update(TIME.GetSeconds());

		grcSampleManager::UpdateClient();
		SGlassManager::InstanceRef().UpdateSynchronize(TIME.GetSeconds());
	}

	void DrawScene()
	{
		// STEP #1. Set our transformation matrix.

		//-- Set the matrix that gets applied to all subsequent geometry.
		grcViewport::SetCurrentWorldIdentity();

		int passCount = PARAM_perftest.Get()? 1000 : 1;
		for (int passes=0; passes<passCount; passes++) 
		{
			// render a nice solid floor

			// render glass
			SGlassManager::InstanceRef().Render();
		}

#if __PFDRAW
		grcBindTexture(NULL);
		grcState::Default();
		grcLightState::SetEnabled(false);

#if 0
		bgBreakable* breakable = SGlassManager::InstanceRef().GetGlass(m_PaneHandle);

		Vector3 topLeft = breakable->GetPaneLeftTopCorner();
		Vector3 bottomRight = breakable->GetPaneRightBottomCorner();
		Vector3 boxMin, boxMax;
		boxMin.Min(topLeft, bottomRight);
		boxMax.Max(topLeft, bottomRight);
		Vector3 boxExtent = boxMax - boxMin;

		mthRandom rand;

		for (int i = 0; i < breakable->m_solidPieces.GetCount(); ++i)
		{
			bgOnePieceSolidGlass& solidPiece = breakable->m_solidPieces[i];

			if (solidPiece.m_state == bgpsOnPane)
			{
				Vector3 center(ORIGIN);
				grcColor3f(Vector3(rand.Get3FloatsV().GetIntrin128()));
				grcBegin(drawTris, solidPiece.m_triangles.GetCount() * 3);
				for (int j = 0; j < solidPiece.m_triangles.GetCount(); ++j)
				{
					Vector3 pos0(Vector3(solidPiece.m_points[solidPiece.m_triangles[j].m_index[0]].m_position, Vector2::kXY) * boxExtent + boxMin);
					grcVertex3f(pos0);
					center += pos0;
					Vector3 pos1(Vector3(solidPiece.m_points[solidPiece.m_triangles[j].m_index[1]].m_position, Vector2::kXY) * boxExtent + boxMin);
					grcVertex3f(pos1);
					center += pos1;
					Vector3 pos2(Vector3(solidPiece.m_points[solidPiece.m_triangles[j].m_index[2]].m_position, Vector2::kXY) * boxExtent + boxMin);
					grcVertex3f(pos2);
					center += pos2;
				}
				grcEnd();

				center.Multiply(center, Vector3(1.0f / (3.0f * solidPiece.m_triangles.GetCount()), 1.0f / (3.0f * solidPiece.m_triangles.GetCount()), 1.0f / (3.0f * solidPiece.m_triangles.GetCount())));

				char label[256];
				formatf(label, "%d", i);
				grcColor(Color32(225, 225, 255, 255));
				grcDrawLabel(center, label);
			}
		}
#endif

		PHSIM->ProfileDraw();
		m_MouseInput->Draw();

		const Matrix34 & cameraMtx = GetCamMgr().GetWorldMtx();
		GetRageProfileDraw().SendDrawCameraForServer(cameraMtx);
		GetRageProfileDraw().Render();
#endif

		// -STOP
	}

	void DrawClient()
	{
		DrawScene();
	}

private:
	// Set the specified texture var and release the texture
	void SetShaderTexureVar(grcTexture* in_pTexture, const char* in_pVarName)
	{
		grcEffectVar var = m_pShader->LookupVar(in_pVarName);
		m_pShader->SetVar(var, in_pTexture);
	}

	ioMapper		    m_Mapper;
	ioValue			    m_Shoot;  // break the pane
	ioValue             m_Alt;    // ignore mouse click while Alt is down; 
	ioValue				m_Ctrl;   // when pressed while shooting, reset the pane first

	int m_PaneHandle; 
	bool m_IsServer;
	sysNamedPipe* m_Pipe;
	grmShaderFx* m_pShader;

	grcTexture *m_pDiffuse, *m_pSpecular, *m_pEnvironment;

	phGlass m_GlassPhysics;
	phGlassInst m_Inst;
	phArchetype m_Archetype;

	phMouseInput* m_MouseInput;
};

#include "file/remote.h"
#endif // 0

// main application
int Main()
{
#if 0
	bgSampleManager sample;
	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();
#endif // 0

	return 0;
}

