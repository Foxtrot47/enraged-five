// 
// sample_audio/sample_music.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#include "audmusic/music_xenon.h"
#include "audmusic/songinfo.h"
#include "system/timer.h"
#include "system/new.h"
#include "system/ipc.h"
#include "system/main.h"

#if __XENON
  #if __OPTIMIZED
    #pragma comment(lib,"xmp.lib")
  #else
	#pragma comment(lib,"xmpd.lib")
  #endif
#endif

using namespace rage;

int Main(void)
{
#if __XENON
	audMusicXenon *pMusic = rage_new audMusicXenon();

	int start = 0;
	int total = 2;

	int numSongInfo;
	audUserPlaylistInfo *paSongInfo = rage_new audUserPlaylistInfo[total];

	pMusic->FindUserMusic(start, total, *paSongInfo, numSongInfo);

	// stop any playing music track
	pMusic->Stop();

	sysTimer timer;
	u32 startTime = 0;

	int idx = 0;
	while (1)
	{
		pMusic->Update();

		if (pMusic->GetState() == audMusic::kStateNone && pMusic->GetPlayState() == audMusic::kIdle)
		{
			pMusic->Play(&paSongInfo[idx++%total]);
			startTime = timer.GetSystemMsTime();
		}
		else if (pMusic->GetPlayState() == audMusic::kPlay)
		{
			u32 elapsedTime = timer.GetSystemMsTime() - startTime;
			if (elapsedTime > (u32)pMusic->GetCurrentSongInfo().SongInfo.dwDuration)
				pMusic->Stop();
		}
	}
#endif // xenon

	return 0;
}

