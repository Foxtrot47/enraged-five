set ARCHIVE=sample_fragment
set FILES=fragworld
set PH_LIBS=%RAGE_PH_LIBS% breakableglass phglass fragment cloth grrope
set SAMPLE_LIBS=%RAGE_SAMPLE_LIBS% parser parsercore sample_physics
set BASE_LIBS=curve devcam spatialdata rmlighting cranimation
set EVENT_LIBS=event eventtypes rmptfx
set LIBS=%RAGE_CORE_LIBS% %PH_LIBS% %SAMPLE_LIBS% %BASE_LIBS% %RAGE_GFX_LIBS% %EVENT_LIBS% %RAGE_AUD_LIBS%
REM LIBS_testername doesn't work on linux build system
set TESTERS=%TESTERS% sample_basic sample_smallcache sample_hangedman
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples %RAGE_DIR%\suite\src
