// 
// sample_physics/fragworld.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_PHYSICS_FRAGWORLD_H
#define SAMPLE_PHYSICS_FRAGWORLD_H

#include "atl/dlist.h"
#include "atl/functor.h"
#include "atl/pool.h"
#include "atl/singleton.h"
#include "sample_physics/demoworld.h"

namespace rage {

class crSkeleton;
class fragInst;
class fragShaft;
class grmShader;
class Matrix34;
class rmcDrawable;
class spdSphere;
class grmMatrixSet;

}

namespace ragesamples {

class fragWorld;

class fragWorldMovable
{
public:
	friend class fragWorld;

	fragWorldMovable(void* context);

private:
	void* m_Context;

	rage::atDNode<fragWorldMovable*> m_InWorldNode;
};

class fragWorld : public phDemoWorld
{
public:
	fragWorld(const char* name = NULL, int cacheEntries = 64);
	~fragWorld();
    virtual void Init(const int knMaxOctreeNodes, int knMaxActiveObjects = 500, int maxObjects = 500, Vec3V_In worldMin=Vec3V(-999.0f,-999.0f,-999.0f), Vec3V_In worldMax=Vec3V(999.0f,999.0f,999.0f), const int maxInstBehaviors=32, int scratchpadSize = 6*1024*1024, int maxManifolds = 1024, int maxExternVelManifolds = 256, int numManagedConstraints = 32);
	virtual void Init(phLevelNew* level, phSimulator* sim, int maxObjects = 500);

	virtual void Reset();
	virtual void Update();
	virtual void Draw();

	// Below is the movables interface, which allows drawing of meshes, sort of like in a real game

	fragWorldMovable* GetMovable(void* movableId);

	bool AddToDrawList(void* context);
	bool AddToDrawBucket(const rage::rmcDrawable& toDraw,
						 const rage::Matrix34& matrix,
						 const rage::crSkeleton * ms,
						 grmMatrixSet* sharedMs,
						 float lod);
	const rage::spdSphere& GetBoundingSphere(void* context);

	void DrawAllMovables(rage::fragShaft& shaft);

	typedef rage::Functor1Ret<bool, void*> AddToDrawListFunctor;
	typedef rage::Functor1Ret<const rage::spdSphere&, void*> GetBoundingSphereFunctor;

	void SetFunctors(AddToDrawListFunctor addToDrawList, GetBoundingSphereFunctor getBoundingSphere);

    void SetCamera( const grcViewport &pViewport, const Matrix34 &pCameraMtx );

#if __BANK
    virtual void AddWidgets(bkBank & bank);
#endif

protected:
	static void PartsBrokeOff(fragInst* oldInst,
							atFixedBitSet<phInstBreakable::MAX_NUM_BREAKABLE_COMPONENTS>& brokenGroups,
							fragInst* newInst);

	fragShaft*					m_Camera;

	grmShader*					m_GlassShader;

	ioValue						m_BreakOnePart;
    ioValue						m_DeleteOnePart;
	ioValue						m_OpenOnePart;
	ioValue						m_CloseOnePart;
	ioValue						m_BreakAll;
	ioValue						m_DamageOnePart;

	rage::atPool<fragWorldMovable> m_Movables;
	rage::atDList<fragWorldMovable*> m_Inserted;

	AddToDrawListFunctor m_AddToDrawList;
	GetBoundingSphereFunctor m_GetBoundingSphere;

	bool						m_bManagePARSER;
	bool						m_bManageEVENTMGR;
};

inline fragWorldMovable* fragWorld::GetMovable(void* movableId)
{
	return reinterpret_cast<fragWorldMovable*>(movableId);
}

inline bool fragWorld::AddToDrawList(void* context)
{
	return m_AddToDrawList(context);
}

inline const rage::spdSphere& fragWorld::GetBoundingSphere(void* context)
{
	return m_GetBoundingSphere(context);
}

inline void fragWorld::SetFunctors(AddToDrawListFunctor addToDrawList, GetBoundingSphereFunctor getBoundingSphere)
{
	m_AddToDrawList = addToDrawList;
	m_GetBoundingSphere = getBoundingSphere;
}

} // namespace ragesamples

#endif // SAMPLE_PHYSICS_FRAGWORLD_H
