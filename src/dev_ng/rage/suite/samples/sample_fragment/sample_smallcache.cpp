//
// sample_fragment\sample_smallcache.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//
// Suggested command lines:
// -file=$\fragments\hotdogred_rex\entity.type
// -file=$\fragments\hotdogred_rex\entity.type -norevive
// -file=$\fragments\chinalmplrg\entity.type -numinsts=30
// -file=$\fragments\chinalmplrg\entity.type -numinsts=30 -norevive
//

// TITLE: Fragment small cache demonstration
// PURPOSE:
//		This sample loads a fragment type, and creates a circle of instances of that type. The fragment cache
//      is deliberately set to a small number of entries to demonstrate the behavior of the system when cache
//      entries start to run out.
// NOTES:
//      - This is a good sample to try out the "DetachInterest" debug drawing. To use this, choose a camera position,
//        then click the DetachInterest widget in the Fragment group. The current camera position is frozen at that
//        position, as far as the fragment manager is aware. The position of the camera is also marked by debug
//        drawing of its frustum at that time. You can then move the camera away, and observe, for example, that the
//        system will prefer to recycle cache entries outside of that frustum.

#define ENABLE_RFS 1

#include "sample_physics/sample_physics.h"

#include "sample_fragment/fragworld.h"

#include "fragment/animation.h"
#include "fragment/cache.h"
#include "fragment/cachemanager.h"
#include "fragment/drawable.h"
#include "fragment/manager.h"
#include "fragment/tune.h"
#include "fragment/typechild.h"

#include "spatialdata/shaft.h"

#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "input/keys.h"
#include "paging/streamer.h"
#include "parser/manager.h"
#include "pharticulated/articulatedcollider.h"
#include "phcore/phmath.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "grprofile/drawmanager.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"	 

PARAM(file,		  "[sample_smallcache] Fragment type to load");
PARAM(anim,       "[sample_smallcache] The animation to play on the character");
PARAM(fragrsc,    "[sample_smallcache] Build resources and use the pgPaging system to load type data");
PARAM(numinsts,   "[sample_smallcache] The number of instances to populate around in a circle");
PARAM(instradius, "[sample_smallcache] The radius of the circle to populate insts around in");
PARAM(cachesize,  "[sample_smallcache] The number of cache entries to allocate");
PARAM(norevive,   "[sample_smallcache] Turn on ""no revive"" on the instances, they will not be revived when their cache entries are lost");
XPARAM(fragcache);

namespace ragesamples {

using namespace rage;

static const u32 DOESNT_COLLIDE_WITH_TERRAIN = 1 << 1;

////////////////////////////////////////////////////////////////
// 

class fragSmallCacheSampleManager : public physicsSampleManager
{
public:
	fragSmallCacheSampleManager()
		: m_Type(NULL)
	{
	}

	virtual void InitClient()
	{
		// Create the physics sample manager
		physicsSampleManager::InitClient();

		// Set the cache size before we create the fragWorld, it will init the fragment cache for us
		if (!PARAM_fragcache.Get())
		{
			PARAM_fragcache.Set("8");
		}

		// Create the fragment world and initialize it, and put in a terrain plane
		m_Demos.AddDemo(rage_new fragWorld("sample_smallcache"));
		m_Demos.GetCurrentWorld()->Init(1000);
		m_Demos.GetCurrentWorld()->Activate();
		m_Demos.GetCurrentWorld()->ConstructTerrainPlane(false, ~DOESNT_COLLIDE_WITH_TERRAIN);

		// Load the type data
		Matrix34 mtx(M34_IDENTITY);

		const char* file = "$/fragments/HotDogRed_rex/entity.type";
		//const char* file = "T:/mc4/assets/props/la_prop_gate_paramount_01x/entity.type";
		//const char* file = "$/fragments/la_prop_bollard_yellow_01x/entity.type";
		PARAM_file.Get(file);
		m_Type = fragType::Load(file);
		Assertf(m_Type, "Fragment type missing: %s", file);

		// Create some instances of the type
		InitInsts();

		// Tell the physics demo system which "demo" to run, we only have one, so...
		int startingDemo = 0;
		m_Demos.SetCurrentDemo(startingDemo);
	}

	void InitInsts()
	{
		int numInsts = 20;
		PARAM_numinsts.Get(numInsts);
		m_Insts.Resize(numInsts);

		// Make the default radius just big enough to fit all the instances around in a circle.
		float radius = (float)numInsts*(m_Type->GetPhys()->GetBound()->GetRadiusAroundCentroid())/(PI);
		PARAM_instradius.Get(radius);

		// Create a bunch of instances in a circle
		for (int inst = 0; inst < numInsts; ++inst)
		{
			float angle = (2.0f * PI * inst) / numInsts;
			Matrix34 mtx(CreateRotatedMatrix(Vector3(radius * sinf(angle), 0.4f, radius * cosf(angle)),
											 Vector3(0.0f, angle, 0.0f)));
			m_Insts[inst] = rage_new fragInst(m_Type, mtx);
			m_Insts[inst]->Insert(false);

			if (PARAM_norevive.Get())
			{
				m_Insts[inst]->SetNoRevive();
			}
		}

		BANK_ONLY(FRAGTUNE->AddArchetypeWidgets(m_Type));
	}

	virtual void InitCamera()
	{
		InitSampleCamera(Vector3(3.0f, 3.0f, 3.0f), ORIGIN);
	}

	virtual void Update()
	{
		// Tell the fragment manager where our camera is
		if (grcViewport::GetCurrent())
		{
			FRAGMGR->SetInterestFrustum(*grcViewport::GetCurrent(), GetCameraMatrix());
		}

		// Update the physics sample manager, this handles mouse interaction, etc.
		physicsSampleManager::Update();

		// If the user wants, reset the simulation
		m_Mapper.Update();
		if (m_Reset.IsPressed())
		{
			FRAGMGR->Reset();

			for (int inst = 0; inst < m_Insts.GetCount(); ++inst)
			{
				m_Insts[inst]->Reset();
			}
		}
	}

	virtual void ShutdownClient()
	{
		// Reset the fragment manager
		FRAGMGR->Reset();

		// Remove all those insts
		for (int inst = 0; inst < m_Insts.GetCount(); ++inst)
		{
			m_Insts[inst]->Remove();
			delete m_Insts[inst];
		}

		// Kill the type
		delete m_Type;

		// Kill the physics demo
		physicsSampleManager::ShutdownClient();
	}

protected:
	virtual const char* GetSampleName() const
	{
		return "sample_smallcache";
	}

private:
	fragType*					m_Type;
	atArray<fragInst*>			m_Insts;

	ioMapper					m_Mapper;
	ioValue						m_Reset;
};

} // namespace ragesamples

// main application
int Main()
{
	{
		ragesamples::fragSmallCacheSampleManager samplePhysics;
		samplePhysics.Init();

		samplePhysics.UpdateLoop();

		samplePhysics.Shutdown();
	}

	return 0;
}





