// 
// sample_physics/fragworld.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "fragworld.h"

#include "bank/bkmgr.h"
#include "breakableglass/glassmanager.h"
#include "cranimation/animation.h"
#include "crskeleton/skeleton.h"
#include "event/manager.h"
#include "event/player.h"
#include "event/set.h"
#include "event/timeline.h"
#include "eventtypes/debug.h"
#include "eventtypes/ptfx.h"
#include "eventtypes/sound.h"
#include "fragment/cachemanager.h"
#include "fragment/eventplayer.h"
#include "fragment/eventtypes.h"
#include "fragment/manager.h"
#include "fragment/tune.h"
#include "fragment/type.h"
#include "fragment/typechild.h"
#include "grmodel/matrixset.h"
#include "input/keys.h"
#include "paging/streamer.h"
#include "parser/manager.h"
#include "physics/levelnew.h"
#include "rmcore/drawable.h"
#include "system/param.h"
#include "system/timemgr.h"

// To use this, you also have to add rmworld to the makefile
#define USE_RMWORLD 0

#if USE_RMWORLD
#include "rmworld/drawable.h"
#include "rmworld/nodemovablemgr.h"
#include "rmworld/world.h"
#endif

PARAM(fragtune,   "[sample_fragment] The path to the fragment.xml");
PARAM(fragcache,  "[sample_fragment] The number of cache entries to allocate");

namespace ragesamples {

using namespace rage;

fragWorld::fragWorld(const char* name, int cacheEntries)
	: phDemoWorld(name)
	, m_Movables(5000)
	, m_bManagePARSER(false)
	, m_bManageEVENTMGR(false)
{
#if USE_RMWORLD
	rage_new rmwFactory();
	rmwWorld::InitClass(1.0f, 0, false);
	INIT_RMWMOVABLEMGR;
#endif

    INIT_PARSER;

	if(!evtTypeManagerSingleton::IsInstantiated())
	{
		INIT_EVENTMGR;
		m_bManageEVENTMGR = true;
	}

	evtAnimPlayer::RegisterPlayerClass();
	fragContinuousEventPlayer::RegisterPlayerClass();
	fragBreakEventPlayer::RegisterPlayerClass();
	fragCollisionEventPlayer::RegisterPlayerClass();

	EVENTMGR.RegisterTypeInstancePair<evtTypeAudOneShot, evtInstanceAudOneShot>("AudOneShot", "AudOneShot", evtTypeManager::ONCOLLIDE_SKIP);
	EVENTMGR.RegisterTypeInstancePair<fragSnuffEventType, fragSnuffEventInstance>("Frag", "Snuff Continuous Events", evtTypeManager::ONCOLLIDE_SKIP);
	EVENTMGR.RegisterTypeInstancePair<fragDamageEventType, fragDamageEventInstance>("Frag", "Cause Damage", evtTypeManager::ONCOLLIDE_SKIP);
	EVENTMGR.RegisterTypeInstancePair<fragAnimEventType, fragAnimEventInstance>("Frag", "Trigger Animation", evtTypeManager::ONCOLLIDE_SKIP);
	EVENTMGR.RegisterTypeInstancePair<fragPaneFrameBrokenEventType, fragPaneFrameBrokenEventInstance>("Frag", "Break Window Pane Frame", evtTypeManager::ONCOLLIDE_SKIP);
	EVENTMGR.RegisterTypeInstancePair<evtTypePtfx, evtInstancePtfx>("Debug", "RmPtfx", evtTypeManager::ONCOLLIDE_SKIP);
	EVENTMGR.RegisterTypeInstancePair<evtTypePrintf, evtInstancePrintf>("Debug", "printf", evtTypeManager::ONCOLLIDE_SKIP);
	EVENTMGR.RegisterTypeInstancePair<evtTypeDrawTick, evtInstanceDrawTick>("Debug", "Draw Tick Mark", evtTypeManager::ONCOLLIDE_SKIP);

	//		EVENTMGR.RegisterEventType("sound", new evtTypeSoundNamed);

	fragTune::Instantiate();

	const char* tunePath = "$/tune/types/fragments";
	PARAM_fragtune.Get(tunePath);

	phConfig::EnableRefCounting();

    if ( fragTune::IsInstantiated() )
    {
        ASSET.PushFolder(tunePath);
	    FRAGTUNE->Load();
	    ASSET.PopFolder();
        
        FRAGTUNE->SetForcedTuningPath(tunePath);
    }

#if __PAGING
	pgStreamer::InitClass(256);
#endif

	PARAM_fragcache.Get(cacheEntries);

	crAnimation::InitClass();

#if USE_RMWORLD
	rage_new fragManager(fragManager::UserDataInit::NullFunctor(),
		MakeFunctor(RMWMOVABLEMGR, &rmwMovableNodeManager::Insert),
		MakeFunctor(RMWMOVABLEMGR, &rmwMovableNodeManager::Remove),
		MakeFunctor(RMWMOVABLEMGR, &rmwMovableNodeManager::Update),
		MakeFunctorRet(RMWMOVABLEMGR, &rmwMovableNodeManager::NewMovable),
		MakeFunctor(RMWMOVABLEMGR, &rmwMovableNodeManager::DeleteMovable),
		MakeFunctorRet(RMWMOVABLEMGR, &rmwMovableNodeManager::AddToDrawBucket),
		MakeFunctorRet(fragManager::ReturnEntityClassZero),
		cacheEntries);
#else
	rage_new fragManager();
#endif

	FRAGMGR->SetPartsBrokeOffFunc(MakeFunctor(PartsBrokeOff));

#if __BANK
    if ( fragTune::IsInstantiated() )
    {
        FRAGTUNE->AddWidgets(BANKMGR.CreateBank("rage - Fragments"));
    }
#endif // __BANK

	FRAGMGR->SetDefaultTypeFlags(TYPE_FLAGS_ALL);

	m_Camera = rage_new fragShaft;

#if 0
	RAGE_TRACK(BreakableGlass);

	ASSET.PushFolder("$\\sample_breakableglass");
	static int s_GlassHeapMem = 127;
	bgGlassManager::InitClass();
	bgGlassManager::Malloc(16,s_GlassHeapMem);
	ASSET.PopFolder();

	{
		RAGE_TRACK(GlassShader);
		m_GlassShader = rage_new grmShader;
		m_GlassShader->Load("rage_breakableglass");
		bgGlassManager::SetGlassShader(m_GlassShader);
	}
#endif
}

void fragWorld::Init(const int knMaxOctreeNodes, int knMaxActiveObjects, int maxObjects, Vec3V_In worldMin, Vec3V_In worldMax, const int maxInstBehaviors, int scratchpadSize, int maxManifolds, int maxExternVelManifolds, int numManagedConstraints)
{
	phDemoWorld::Init(knMaxOctreeNodes, knMaxActiveObjects, maxObjects, worldMin, worldMax, maxInstBehaviors, scratchpadSize, maxManifolds, maxExternVelManifolds, numManagedConstraints);

#if __BANK
	FRAGMGR->AddWidgets(BANKMGR.CreateBank("rage - Fragment Manager"));
#endif

	m_Mapper.Map(IOMS_KEYBOARD, KEY_B, m_BreakOnePart);
    m_Mapper.Map(IOMS_KEYBOARD, KEY_X, m_DeleteOnePart);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_O, m_OpenOnePart);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_C, m_CloseOnePart);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_CONTROL, m_BreakAll);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_D, m_DamageOnePart);
}

void fragWorld::Init(phLevelNew* level, phSimulator* sim, int maxObjects)
{
	phDemoWorld::Init(level, sim, maxObjects);

	m_Mapper.Map(IOMS_KEYBOARD, KEY_B, m_BreakOnePart);
    m_Mapper.Map(IOMS_KEYBOARD, KEY_X, m_DeleteOnePart);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_O, m_OpenOnePart);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_C, m_CloseOnePart);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_CONTROL, m_BreakAll);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_D, m_DamageOnePart);
}

void fragWorld::PartsBrokeOff(fragInst* oldInst,
							  atFixedBitSet<phInstBreakable::MAX_NUM_BREAKABLE_COMPONENTS>& brokenGroups,
							  fragInst* newInst)
{
	// Since FixGrabForBreak() is in RAGE base and doesn't know anything about fragments or groups, we really do have to construct a bit array that refers
	//   to bound components and not fragment groups to pass to it.
	atFixedBitSet<phInstBreakable::MAX_NUM_BREAKABLE_COMPONENTS> brokenChildren;
	for(int curChildIndex = oldInst->GetTypePhysics()->GetNumChildren() - 1; curChildIndex >= 0; --curChildIndex)
	{
		const fragTypeChild *curChild = oldInst->GetTypePhysics()->GetAllChildren()[curChildIndex];
		const int curGroupIndex = curChild->GetOwnerGroupPointerIndex();
		if(brokenGroups.IsSet(curGroupIndex))
		{
			brokenChildren.Set(curChildIndex);
		}
		else
		{
			// Not necessary if the default is off.
			brokenChildren.Clear(curChildIndex);
		}
	}

	phDemoWorld::GetActiveDemo()->FixGrabForBreak(oldInst, brokenChildren, newInst);

	oldInst->PartsBrokeOff(brokenGroups, newInst);
}

fragWorld::~fragWorld()
{
	bgGlassManager::ShutdownClass();

	delete m_GlassShader;

	delete FRAGMGR;

#if __PAGING
	pgStreamer::ShutdownClass();
#endif // __PAGING

    if ( fragTune::IsInstantiated() )
    {
        fragTune::Destroy();
    }

	delete m_Camera;

	evtAnimPlayer::UnregisterPlayerClass();
	fragContinuousEventPlayer::UnregisterPlayerClass();
	fragBreakEventPlayer::UnregisterPlayerClass();
	fragCollisionEventPlayer::UnregisterPlayerClass();

	if(m_bManageEVENTMGR)
		SHUTDOWN_EVENTMGR;

    SHUTDOWN_PARSER;

#if USE_RMWORLD
	SHUTDOWN_RMWMOVABLEMGR;
	rmwWorld::GetInstance().Shutdown();
	rmwWorld::ShutdownClass();

	delete &rmwFactory::GetInstance();
#endif

	crAnimation::ShutdownClass();
}

void fragWorld::Reset()
{
	phDemoWorld::Reset();

	FRAGMGR->Reset();

	for (int i = 0; i < m_NumObjects; ++i)
	{
		if (fragInst* inst = dynamic_cast<fragInst*>(m_Objects[i]->GetPhysInst()))
		{
			if (!inst->GetCached())
			{
				inst->Reset();

				if (inst->ShouldBeInCacheToDraw())
				{
					inst->PutIntoCache();
				}
			}
		}
	}
}

void fragWorld::Update()
{
	phDemoWorld::Update();

    if ( fragTune::IsInstantiated() )
    {
        FRAGTUNE->Update(0, true);
    }
	phMouseInput::Spring& spring = const_cast<phMouseInput::Spring&>(GetGrabSpring());
	phInst* springInst = (PHLEVEL->LegitLevelIndex(spring.instLevelIndex) && PHLEVEL->GetState(spring.instLevelIndex) != phLevelBase::OBJECTSTATE_NONEXISTENT) ? PHLEVEL->GetInstance(spring.instLevelIndex) : NULL;

	if (m_BreakOnePart.IsPressed())
	{
		if (springInst)
		{
			if (fragInst* inst = dynamic_cast<fragInst*>(springInst))
			{
				if (m_BreakAll.IsDown())
				{
					int numComponents = inst->GetTypePhysics()->GetNumChildren();
					for (int component = 0; component < numComponents; ++component)
					{
						inst->BreakOffAbove(component);
					}
				}
				else
				{
					inst->BreakOffAbove(spring.component);
				}
			}
		}
	}

    if (m_DeleteOnePart.IsPressed())
    {
        if (springInst)
        {
            if (fragInst* inst = dynamic_cast<fragInst*>(springInst))
            {
                inst->DeleteAbove(spring.component);
            }
        }
    }

	if (m_OpenOnePart.IsPressed())
	{
		if (springInst)
		{
			if (fragInst* inst = dynamic_cast<fragInst*>(springInst))
			{
				if (m_BreakAll.IsDown())
				{
					int numComponents = inst->GetTypePhysics()->GetNumChildren();
					for (int component = 0; component < numComponents; ++component)
					{
						inst->OpenLatchAbove(component);
					}
				}
				else
				{
					inst->OpenLatchAbove(spring.component);
				}
			}
		}
	}

	if (m_CloseOnePart.IsPressed())
	{
		if (springInst)
		{
			if (fragInst* inst = dynamic_cast<fragInst*>(springInst))
			{
				if (m_BreakAll.IsDown())
				{
					int numComponents = inst->GetTypePhysics()->GetNumChildren();
					for (int component = 0; component < numComponents; ++component)
					{
						inst->CloseLatchAbove(component);
					}
				}
				else
				{
					inst->CloseLatchAbove(spring.component);
				}
			}
		}
	}

	if (m_DamageOnePart.IsPressed())
	{
		if (springInst)
		{
			if (fragInst* inst = dynamic_cast<fragInst*>(springInst))
			{
				fragInst::Collision collision;

				collision.damage = 1e30f;

				if (m_BreakAll.IsDown())
				{
					int numComponents = inst->GetTypePhysics()->GetNumChildren();
					for (int component = 0; component < numComponents; ++component)
					{
						collision.component = component;

						inst->CollisionDamage(collision, NULL);
					}
				}
				else
				{
					collision.component = spring.component;

					inst->CollisionDamage(collision, NULL);
				}
			}
		}
	}

	if (ShouldUpdate())
	{
		FRAGMGR->Update(TIME.GetSeconds());
	}

#if USE_RMWORLD
	m_Camera->Set(*grcViewport::GetCurrent(), GetCameraMatrix());
	RMWORLD.Update(*m_Camera,GetCameraMatrix().d,false);
#endif
}

void fragWorld::Draw()
{
// 	bgGlassManager::UpdateBuffers();
// 	bgGlassManager::SyncBreakableGlass();

	phDemoWorld::Draw();

	if (true || UpdatedLastFrame() || m_FirstFrameSinceActivation)
	{
#if USE_RMWORLD
		RMWORLD.AddToDrawList(*m_Camera);
		RMWORLD.DrawStart(*m_Camera);
		RMWORLD.DrawEnd(*m_Camera);
#else
		DrawAllMovables(*m_Camera);
#endif
	}
}

fragWorldMovable::fragWorldMovable(void* context)
	: m_Context(context)
{
	m_InWorldNode.Data = this;
}

/*
bool rmwMovableObjectNodePooled::AddToDrawList(const fragShaft& UNUSED_PARAM(shaft))
{
	bool added = RMWMOVABLEMGR.AddToDrawList(m_Context);

	if (added)
	{
		m_TimeLastVisible = TIME.GetElapsedTime();
	}

	return added;
}

const spdSphere& rmwMovableObjectNodePooled::GetBoundingSphere(void) const
{
	return RMWMOVABLEMGR.GetBoundingSphere(m_Context);
}
*/

bool fragWorld::AddToDrawBucket(const rmcDrawable& toDraw,
								const Matrix34& matrix,
								const crSkeleton *skeleton,
								grmMatrixSet* sharedMs,
								float dist)
{
	u8 lod = static_cast<u8>(toDraw.GetLodGroup().ComputeLod(dist));
	int bucketMask = toDraw.GetBucketMask(lod);
	if (bucketMask == ~0)
	{
		return false;
	}
	
	int mask = 1;
	for (int bucket = 0; bucket < 32; ++bucket, mask <<= 1)
	{
		if (mask & bucketMask)
		{
			if (skeleton)
			{
				grmMatrixSet *ms = grmMatrixSet::Create(*skeleton);
				ms->Update(*skeleton,toDraw.IsSkinned());
				toDraw.DrawSkinned(matrix, *ms, bucket, lod);
#if __XENON
				// This is terrible, and is only suitable for demo code
				GRCDEVICE.BlockOnFence(GRCDEVICE.InsertFence());
#endif
				delete ms;
			}
			else if (sharedMs)
			{
				// ... but if it's shared, we do need the root matrix
				toDraw.DrawSkinned(matrix, *sharedMs, bucket, lod);
			}
			else
			{
				toDraw.Draw(matrix, bucket, lod);
			}
		}
	}

	return true;
}

void fragWorld::DrawAllMovables(fragShaft& UNUSED_PARAM(shaft))
{
	for (const atDNode<fragWorldMovable*>* node = m_Inserted.GetHead(); node; node = node->GetNext())
	{
		fragWorldMovable* movable = node->Data; Assert(movable);
		AddToDrawList(movable->m_Context);
	}
}

void fragWorld::SetCamera( const grcViewport &pViewport, const Matrix34 &pCameraMtx )
{
    m_Camera->Set( pViewport, pCameraMtx );
    FRAGMGR->SetInterestFrustum( pViewport, pCameraMtx );
}

#if __BANK
void fragWorld::AddWidgets(bkBank& b)
{
    b.AddButton("Reset", datCallback(CFA1(phDemoWorld::ResetFromBankCallback), this));

    b.PushGroup( "Objects" );
    {
        for (int i = 0; i < m_NumObjects; ++i)
        {
            m_Objects[i]->AddWidgets( b );
        }
    }
    b.PopGroup();

    b.PushGroup( "Update Objects" );
    {
        for (int i = 0; i < m_NumUpdateObjects; ++i)
        {
            m_UpdateObjects[i]->AddWidgets( b );
        }
    }
    b.PopGroup();
}
#endif // __BANK

} // namespace ragesamples

