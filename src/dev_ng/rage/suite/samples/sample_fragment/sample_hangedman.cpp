//
// sample_fragment\sample_hangedman.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

#define ENABLE_RFS 1

#include "fragworld.h"

#include "atl/atfunctor.h"
#include "bank/bkmgr.h"
#include "cloth/environmentcloth.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "fragment/cache.h"
#include "fragment/cachemanager.h"
#include "fragment/drawable.h"
#include "fragment/manager.h"
#include "fragment/tune.h"
#include "fragment/typechild.h"
#include "fragment/typecloth.h"
#include "gizmo/rotation.h"
#include "gizmo/translation.h"
#include "input/keys.h"
#include "paging/streamer.h"
#include "parser/manager.h"
#include "pharticulated/articulatedcollider.h"
#include "phcore/phmath.h"
#include "pheffects/rope.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "grprofile/drawmanager.h"
#include "sample_physics/sample_physics.h"
#include "spatialdata/shaft.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"	 

// To use this, you also have to add rmworld to the makefile
#define USE_RMWORLD 0

#if USE_RMWORLD
#include "rmworld/drawable.h"
#include "rmworld/nodemovablemgr.h"
#include "rmworld/world.h"
#endif

PARAM(file,		 "[viewfrag] Fragment type to load for body");
PARAM(ropefile,	 "[viewfrag] Fragment type to load for rope");

bool		  s_AllowFall = false;
gzTranslation* s_Translation = NULL;
gzRotation* s_Rotation = NULL;

namespace ragesamples {

using namespace rage;

static const u32 ARCHETYPE_TYPE_TERRAIN		= 1 << 0;

////////////////////////////////////////////////////////////////
// 

void AddBrokenRopePiece (environmentCloth* UNUSED_PARAM(originalEnvCloth), environmentCloth* UNUSED_PARAM(newEnvCloth))
{
//	fragCacheEntry* cacheEntry = FRAGCACHEMGR->GetNewCacheEntry();
//	phSwingingObject& ropeObject = *rage_new phSwingingObject;
//	ropeObject.SetInst(const_cast<phInst*>(envCloth->GetInstance()));
//	const float ropeRadius = 0.02f;
//	ropeObject.SetDrawable(rage_new rmcRopeDrawable(envCloth->GetBehavior(),ropeRadius));
//	phDemoWorld* demoWorld = phDemoWorld::GetActiveDemo();
//	demoWorld->AddUpdateObject(&ropeObject);
}

////////////////////////////////////////////////////////////////
// 

class hangedManSampleManager : public physicsSampleManager
{
public:
	hangedManSampleManager()
		: m_RopeType(NULL)
		, m_BodyType(NULL)
		, m_Rope(NULL)
	{
	}

	virtual void InitClient()
	{
		physicsSampleManager::InitClient();

		m_Demos.AddDemo(rage_new fragWorld("hangedman"));
		const int maxOctreeNodes = 1000;
		m_Demos.GetCurrentWorld()->Init(maxOctreeNodes);
		m_Demos.GetCurrentWorld()->Activate();
		m_Demos.GetCurrentWorld()->ConstructTerrainPlane(false, ARCHETYPE_TYPE_TERRAIN);

		// Create a pool of extra cloth pieces, for breaking rope.
		phEnvClothPool::CreateEnvClothPool(16);
		Functor2<environmentCloth*,environmentCloth*> breakRopeFunction = MakeFunctor(&AddBrokenRopePiece);
		ENVCLOTHPOOL->SetTearClothFunction(breakRopeFunction);

		// Load the rope type file.
		const char* ropeFile = "fragments/tra_rope_test01x_rope01x/entity";
		PARAM_ropefile.Get(ropeFile);
		m_RopeType = fragType::Load(ropeFile);
		Assert(m_RopeType);
		BANK_ONLY(FRAGTUNE->AddArchetypeWidgets(m_RopeType));

		// Set the rope type's flag to attach to any object its end touches.
		m_RopeType->SetAttachBottomEnd();

		// Make the rope breakable.
		m_RopeType->GetTypeEnvCloth(0)->m_Cloth.GetCloth()->SetBreakable();
		m_RopeType->GetTypeEnvCloth(0)->m_Cloth.SetTearStrength(1.0f);

		// Add a rope instance to the world.
		Vector3 topOfRope(0.0f, 16.0f, 0.0f);
		Matrix34 objectMatrix(CreateRotatedMatrix(topOfRope, ORIGIN));
		m_Rope = rage_new fragInst(m_RopeType, objectMatrix);
		m_Rope->Insert(false);

		// Add a fixed object, from which the rope will hang.
		Vector3 boxExtents(1.0f,1.0f,1.0f);
		m_Demos.GetCurrentWorld()->CreateFixedBox(boxExtents,topOfRope);
		// Load the body type file.
		const char* file = "fragments/detectivedc/entity";
		PARAM_file.Get(file);
		m_BodyType = fragType::Load(file);
		Assert(m_BodyType);
		BANK_ONLY(FRAGTUNE->AddArchetypeWidgets(m_BodyType));
		BANK_ONLY(FRAGTUNE->RefreshTypeList());

		// Put a body instance at the end of the rope.
		Vector3 bodyPosition(topOfRope);
		ScalarV ropeLength = m_Rope->GetType()->GetTypeEnvCloth(0)->m_Cloth.GetCloth()->ComputeRopeLength();
		bodyPosition.x += ropeLength.Getf();
		bodyPosition.y -= 1.5f;
		sm_Body = rage_new fragInst(m_BodyType, CreateRotatedMatrix(bodyPosition, ORIGIN));
		sm_Body->SetInsertionState(phLevelBase::OBJECTSTATE_ACTIVE);
		sm_Body->Insert(false);

		int startingDemo = 0;
		m_Demos.SetCurrentDemo(startingDemo);
	}

	virtual void InitCamera()
	{
		InitSampleCamera(Vector3(3.0f, 3.0f, 3.0f), ORIGIN);
	}

	virtual void Update()
	{
		physicsSampleManager::Update();

		pgQueueScheduler::GetScheduler().Update();
		if (grcViewport::GetCurrent())
		{
			FRAGMGR->SetInterestFrustum(*grcViewport::GetCurrent(), GetCameraMatrix());
		}
	}

	virtual void ShutdownClient()
	{
		sm_Body->Remove();
		delete sm_Body;
		m_Rope->Remove();
		delete m_Rope;

		delete m_RopeType;
		delete m_BodyType;

		physicsSampleManager::ShutdownClient();
	}

protected:
	virtual const char* GetSampleName() const
	{
		return "viewfrag";
	}

private:
	static void PartsBrokeOff(fragInst* oldInst,
							  atFixedBitSet<phInstBreakable::MAX_NUM_BREAKABLE_COMPONENTS>& brokenParts,
							  fragInst* newInst)
	{
		phDemoWorld::GetActiveDemo()->FixGrabForBreak(oldInst, brokenParts, newInst);
	}

	fragType*					m_RopeType;
	fragType*					m_BodyType;
	fragInst*					m_Rope;
	static fragInst*			sm_Body;
};

fragInst* hangedManSampleManager::sm_Body = NULL;

} // namespace ragesamples

// main application
int Main()
{
	{
		ragesamples::hangedManSampleManager samplePhysics;
		samplePhysics.Init();

		samplePhysics.UpdateLoop();

		samplePhysics.Shutdown();
	}

	return 0;
}





