//
// sample_fragment\sample_basic.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//
// Suggested command lines:
// -file=$\fragments\piranha\entity.type
// -file=$\fragments\detectivedc\entity.type
// -file=$\fragments\hotdogred_rex\entity.type

// TITLE: Basic fragments
// PURPOSE:
//		This sample shows how to load a fragment type, and create
//      a fragment instance from it.

#define ENABLE_RFS 1

#include "sample_physics/sample_physics.h"

#include "fragment/type.h"
#include "fragment/manager.h"

#include "pharticulated/articulatedcollider.h"
#include "system/main.h"
#include "system/param.h"

PARAM(file, "[sample_basic] Fragment type to load");

namespace ragesamples {

using namespace rage;

////////////////////////////////////////////////////////////////
// 

class fragmentSampleManager : public physicsSampleManager
{
public:
	fragmentSampleManager()
		: m_Inst(NULL)
	{
	}

	virtual void InitClient()
	{
		// STEP #1. Initialize the physics sample manager and create a demo world

		// -- This is actually several steps rolled into one, but these steps are common to
		// all RAGE physics, so they're not repeated here. If you need help setting up RAGE
		// physics, have a squiz at base/samples/sample_physics/sample_basic.sln

		physicsSampleManager::InitClient();
		m_Demos.AddDemo(rage_new phDemoWorld("basic frag sample"));
		m_Demos.GetCurrentWorld()->Init(1000);
		m_Demos.GetCurrentWorld()->Activate();
		m_Demos.GetCurrentWorld()->ConstructTerrainPlane(false);
		m_Demos.SetCurrentDemo(0);

		// STEP #2. Initialize the fragment manager

		// -- The fragment manager is responsible for a couple of features we're not actually
		// using in this sample. For one, it handles paging of type data, and in this sample we
		// are just going to load the type data directly from disk. The other is that it forwards
		// draw calls from a drawing system to the fragment instances, and in this sample we're
		// not going to draw the fragments themselves. (You will only see their physical forms
		// due to physics debug drawing. Nevertheless, we have to create one because there's a fair
		// bit of code that expects us to.

		rage_new fragManager;

		// STEP #3. Load a fragment type

		// -- All we have to do is call fragType::Load(file) with the location of the entity.type
		// file we're interested in. If we were using the fragment manager's paging system, we would
		// have instead called FRAGMGR->GetType(), which would have given us a fragTypeStub instead
		// of a while fragType. The stub doesn't hold the data, but will inform any instances we create
		// with it when the data becomes available.

		const char* file = "$/fragments/HotDogRed_rex/entity.type";
		PARAM_file.Get(file);

		m_Type = fragType::Load(file);
		Assert(m_Type);

		// STEP #4. Instantiate the instance

		// -- We create a fragInst, passing it the type data that we loaded. If we had only a fragTypeStub
		// at this point, the constructor would look the same. The matrix passed in is the initial position
		// of the instance, and also where it will return to when Reset.

		m_Inst = rage_new fragInst(m_Type, M34_IDENTITY);

		// STEP #5. Insert the instance

		// -- Inserting the instance causes it to try to insert into the physics world. If it doesn't have
		// type data at this point because it was create with a fragTypeStub whose type hasn't paged in
		// yet, it will wait to perform the insert as soon as the type comes in. In this sample, it is
		// inserted immediately.
		//
		// If we were using fragment drawing, Insert would also cause the instance to create a "movable"
		// in the drawing system through the callbacks registered with the fragment manager. That could be
		// used, to hook into some PVS mechanism such as an octree or BSP. In this sample, that doesn't
		// happen because we didn't register any functors with the fragment manager.

		m_Inst->Insert(false);
	}

	virtual void ShutdownClient()
	{
		// STEP #6. Clean up

		// -- Removing the instance deletes it from the physics world, and would also remove it from the
		// graphics culling world if we were using one. 

		m_Inst->Remove();

        // We have to reset the fragment manager before we delete the type, because otherwise the fragment
        // cache could be holding onto references to our type data.
        FRAGMGR->Reset();

        // Then we delete the instance and the type, and
        // dismiss the other systems in the reverse order we created them.
		delete m_Inst;
		delete m_Type;

		delete FRAGMGR;

		physicsSampleManager::ShutdownClient();
	}

protected:
	virtual const char* GetSampleName() const
	{
		return "sample_fragment/sample_basic";
	}

private:
	fragType*					m_Type;
	fragInst*					m_Inst;
};

} // namespace ragesamples

// main application
int Main()
{
	{
		ragesamples::fragmentSampleManager samplePhysics;
		samplePhysics.Init();

		samplePhysics.UpdateLoop();

		samplePhysics.Shutdown();
	}

	return 0;
}





