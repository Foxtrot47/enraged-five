//
// sample_replay/replay_shape_tests.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: replay_shape_tests
// PURPOSE:
//		This sample tests physics level shape tests with replay. A collection of physical objects is set up, shape tests are done, and then
//		it is reset after a certain time and run again.
//		Errors occur if the second run is not the same as the first.
//		Different pages in the sample separate replay testing into sections to isolate problems with different kinds of objects.


#include "curve/curvemgr.h"
#include "devcam/mayacam.h"
#include "devcam/polarcam.h"
#include "fragment/instance.h"
#include "fragment/manager.h"
#include "fragment/type.h"
#include "grcore/im.h"
#include "grmodel/setup.h"
#include "math/random.h"
#include "pharticulated/articulatedcollider.h"
#include "pharticulated/bodypart.h"
#include "phbound/boundcomposite.h"
#include "phcore/phmath.h"
#include "phsolver/contactmgr.h"
#include "physics/constraint.h"
#include "physics/levelnew.h"
#include "physics/shapetest.h"
#include "physics/simulator.h"
#include "physics/sleep.h"
#include "sample_fragment/fragworld.h"
#include "sample_physics/demoobject.h"
#include "sample_physics/sample_physics.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "system/timer.h"
#include "vector/colors.h"

PARAM(file, "Reserved for future expansion, not currently hooked up");

#define REPLAY_MAX_ISECTS	64
namespace ragesamples {

using namespace rage;


// PURPOSE: Create and test physical scenes with replay.
class ShapeTestReplayManager : public physicsSampleManager
{
public:
	virtual void InitClient ()
	{
		// Verify the compile options needed to test replay.
	#if !TEST_REPLAY
		Quitf("Turn on TEST_REPLAY in diag/debuglog.h to test replay.");
	#endif

		if (phSimulator::UsingCollisionTasks())
		{
			Quitf("Turn off PHCOLLISION_USE_TASKS in physics/simulator.cpp to test replay.");
		}

		if (phConfig::GetForceSolverWorkerThreadCount() > 0)
		{
			Quitf("Turn off force solver worker threads to test replay.");
		}

	#if !__DEBUGLOG
		Quitf("Turn on __DEBUGLOG in diag/debuglog.h to test replay.");
	#endif

		// STEP #1. Initialize the base physics sample class.
		physicsSampleManager::InitClient();


		// STEP #2. Create all the sets of interacting objects, with a sample page for each set.
		//-- "+" and "-" keys control paging through the following set of sample pages.
		Vector3 position,rotation,worldAttachPoint,worldAttachPointA,worldAttachPointB;
		m_HotdogCart = NULL;
		m_Ragdoll = NULL;

		////////////////
		// Page 0: boxes
		position.Set(0.0f,-4.0f,0.0f);
		phDemoWorld& stackWorld = MakeNewDemoWorld("Stack",NULL,position);
		Vector3 boxSize(1.0f,1.0f,1.0f);
		int numHigh = 8;
		int iBox;
		for (iBox=0; iBox<numHigh; iBox++)
		{
			stackWorld.CreateBox(boxSize,1.0f,Vector3(0,iBox+position.y,0.2f*iBox),true);
		}

		// Page 1: blocks resting on the ground
		phDemoWorld& blockWorld = MakeNewDemoWorld("blocks");
		blockWorld.CreateObject("crate",Vector3(+2.0f, 0.0f, -1.0f),true);
		blockWorld.CreateObject("cinderblock",Vector3(0.2f, 0.0f, -0.9f),true);
		blockWorld.CreateObject("long_crate",Vector3(-1.0f, 0.0f, -2.0f),true);

		////////////////////
		// Page 2: a ragdoll
		phDemoWorld& ragdollWorld = MakeNewDemoWorld("dragging ragdoll","plane_complex");
		position.Set(-4.0f,1.0f,-2.0f);
		phArticulatedObject::CreateHuman(ragdollWorld,position);

		m_ShapeTestType = 0;
		m_MaxNumIsects = 8;
		m_TestBoxSize = Vec3V(V_TEN);
		m_TestBoxMin = -Vec3V(V_FIVE);
		m_MaxRadius = ScalarVFromF32(5.0f);

		// STEP #3. Set and activate the initial page, from the command line parameter "startingDemo" with the first page as default.
		m_Demos.SetCurrentDemo(0);
		m_Demos.GetCurrentWorld()->Activate();
		m_ReplayTimer.Reset();
		m_ResetOnce = false;
	}


	virtual void InitCamera ()
	{
		Vector3 lookFrom(-1.0f,3.5f,6.0f);
		Vector3 lookTo(0.0f,1.0f,0.0f);
		InitSampleCamera(lookFrom,lookTo);
	}


	virtual void Reset ()
	{
		physicsSampleManager::Reset();
		m_ReplayTimer.Reset();
		TIME.Reset();
		m_Demos.GetCurrentWorld()->Reset();
	}
	
	void TestPoint ()
	{
		phShapeTest<phShapeSphere> pointTest;
		Vec3V worldCenter = AddScaled(m_TestBoxMin,m_TestBoxSize,g_ReplayRand.Get3FloatsV());
		int numIsects = g_ReplayRand.GetRanged(-1,m_MaxNumIsects);
		Assert(numIsects<REPLAY_MAX_ISECTS);
		pointTest.InitPoint(worldCenter,m_IsectList,numIsects);
		pointTest.TestInLevel();
	}

	void TestSphere ()
	{
		phShapeTest<phShapeSphere> sphereTest;
		Vec3V worldCenter = AddScaled(m_TestBoxMin,m_TestBoxSize,g_ReplayRand.Get3FloatsV());
		ScalarV radius = m_MaxRadius*g_ReplayRand.GetFloatV();
		int numIsects = g_ReplayRand.GetRanged(-1,m_MaxNumIsects);
		Assert(numIsects<REPLAY_MAX_ISECTS);
		sphereTest.InitSphere(worldCenter,radius,m_IsectList,numIsects);
		sphereTest.TestInLevel();
	}

	void TestProbe ()
	{
		phShapeTest<phShapeProbe> probeTest;
		Vec3V start = AddScaled(m_TestBoxMin,m_TestBoxSize,g_ReplayRand.Get3FloatsV());
		Vec3V end = AddScaled(m_TestBoxMin,m_TestBoxSize,g_ReplayRand.Get3FloatsV());
		phSegmentV segment;
		segment.Set(start,end);
		int numIsects = g_ReplayRand.GetRanged(-1,m_MaxNumIsects);
		Assert(numIsects<REPLAY_MAX_ISECTS);
		probeTest.InitProbe(segment,m_IsectList,numIsects);
		probeTest.TestInLevel();
	}

	void TestEdge ()
	{
		phShapeTest<phShapeProbe> edgeTest;
		Vec3V start = AddScaled(m_TestBoxMin,m_TestBoxSize,g_ReplayRand.Get3FloatsV());
		Vec3V end = AddScaled(m_TestBoxMin,m_TestBoxSize,g_ReplayRand.Get3FloatsV());
		phSegmentV segment;
		segment.Set(start,end);
		int numIsects = g_ReplayRand.GetRanged(-1,m_MaxNumIsects);
		Assert(numIsects<REPLAY_MAX_ISECTS);
		edgeTest.InitEdge(segment,m_IsectList,numIsects);
		edgeTest.TestInLevel();
	}

	void TestCapsule ()
	{
		phShapeTest<phShapeCapsule> capsuleTest;
		Vec3V start = AddScaled(m_TestBoxMin,m_TestBoxSize,g_ReplayRand.Get3FloatsV());
		Vec3V end = AddScaled(m_TestBoxMin,m_TestBoxSize,g_ReplayRand.Get3FloatsV());
		phSegmentV segment;
		segment.Set(start,end);
		ScalarV radius = m_MaxRadius*g_ReplayRand.GetFloatV();
		int numIsects = g_ReplayRand.GetRanged(-1,m_MaxNumIsects);
		Assert(numIsects<REPLAY_MAX_ISECTS);
		capsuleTest.InitCapsule(segment,radius,m_IsectList,numIsects);
		capsuleTest.TestInLevel();
	}

	void TestSweptSphere ()
	{
		phShapeTest<phShapeCapsule> sweptSphereTest;
		Vec3V start = AddScaled(m_TestBoxMin,m_TestBoxSize,g_ReplayRand.Get3FloatsV());
		Vec3V end = AddScaled(m_TestBoxMin,m_TestBoxSize,g_ReplayRand.Get3FloatsV());
		phSegmentV segment;
		segment.Set(start,end);
		ScalarV radius = m_MaxRadius*g_ReplayRand.GetFloatV();
		int numIsects = g_ReplayRand.GetRanged(-1,m_MaxNumIsects);
		Assert(numIsects<REPLAY_MAX_ISECTS);
		sweptSphereTest.InitSweptSphere(segment,radius,m_IsectList,numIsects);
		sweptSphereTest.TestInLevel();
	}

	void TestObject ()
	{
	}

	void TestBatch ()
	{
	}


	virtual void UpdateClient ()
	{
		const float replayTestTime = 8.0f;
		physicsSampleManager::UpdateClient();
		if (!m_ResetOnce)
		{
			if (m_ReplayTimer.GetTime()>replayTestTime*2.0f)
			{
				// The time limit was hit, so reset.
				// This goes beyond the time limit to avoid a crash in the replay logger when the replay time gets too close to the first run time.
				m_ResetOnce = true;
				Reset();
			}
		}
		else if (m_ReplayTimer.GetTime()>replayTestTime)
		{
			// Replay testing succeeded.
			m_ReplayTimer.Reset();
			if (m_Demos.GetCurrentNumber()<m_Demos.GetNumDemos()-1)
			{
				// Move to the next world and keep testing.
				m_Demos.NextWorld(&m_CamMgr);

				// Reset the random number seed and the already-reset flag.
				g_ReplayRand.Reset(72);
				m_ResetOnce = false;

				// Reset the system timer.
				TIME.Reset();

			#if __DEBUGLOG
				// Reinitialize replay logging.
				const bool replay = false;
				const bool abortOnFailure = true;
				diagDebugLogInit(replay,abortOnFailure);
				g_ReplayRand.SetReplayDebug(true);
			#endif

			}
			else
			{
				// Replay testing succeeded for all the worlds.
				m_WantsExit = true;
				if (m_HotdogCart)
				{
					m_HotdogCart->Remove();
				}

				if (m_Ragdoll)
				{
					m_Ragdoll->Remove();
				}
			}
		}

		// Do a shape test.
		switch (m_ShapeTestType++)
		{
			case 0:
			{
				TestPoint();
				break;
			}
			case 1:
			{
				TestSphere();
				break;
			}
			case 2:
			{
				TestProbe();
				break;
			}
			case 3:
			{
				TestEdge();
				break;
			}
			case 4:
			{
				TestCapsule();
				break;
			}
			case 5:
			{
				TestSweptSphere();
				break;
			}
			case 6:
			{
				TestObject();
				break;
			}
			case 7:
			{
				TestBatch();
				break;
			}
			default:
			{
				// Start over at the beginning.
				m_ShapeTestType = 0;
			}
		}
	}


	virtual void DrawClient ()
	{
		physicsSampleManager::DrawClient();
	}


	virtual grcSetup& AllocateSetup()
	{
		return *(rage_new grmSetup());
	}


protected:
	fragInst* m_HotdogCart;
	fragInst* m_Ragdoll;
	sysTimer m_ReplayTimer;
	bool m_ResetOnce;

	phIntersection m_IsectList[REPLAY_MAX_ISECTS];
	Vec3V m_TestBoxSize,m_TestBoxMin;
	ScalarV m_MaxRadius;
	int m_MaxNumIsects;
	int m_ShapeTestType;
};

} // namespace ragesamples


int Main()
{
	ragesamples::ShapeTestReplayManager sampleShapeTestReplay;
	sampleShapeTestReplay.Init();
	sampleShapeTestReplay.UpdateLoop();
	sampleShapeTestReplay.Shutdown();
    return 0;
}

