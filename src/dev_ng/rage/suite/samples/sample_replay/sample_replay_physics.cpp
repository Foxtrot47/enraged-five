//
// sample_replay/sample_replay_physics.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// TITLE: sample_replay_physics
// PURPOSE:
//		This sample tests physics with replay. A collection of physical objects is set up, run, reset after a certain time and run again.
//		Errors occur if the second run is not the same as the first.
//		Different pages in the sample separate replay testing into sections to isolate problems, such as collisions, constraints, ragdolls,
//		breaking objects, cloth, rope, and water.


#include "curve/curvemgr.h"
#include "devcam/mayacam.h"
#include "devcam/polarcam.h"
#include "fragment/instance.h"
#include "fragment/manager.h"
#include "fragment/type.h"
#include "grcore/im.h"
#include "grmodel/setup.h"
#include "math/random.h"
#include "pharticulated/articulatedcollider.h"
#include "pharticulated/bodypart.h"
#include "phbound/boundcomposite.h"
#include "phcore/phmath.h"
#include "physics/constraint.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "physics/sleep.h"
#include "sample_fragment/fragworld.h"
#include "sample_physics/demoobject.h"
#include "sample_physics/sample_physics.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "system/timer.h"
#include "vector/colors.h"

PARAM(file, "Reserved for future expansion, not currently hooked up");

namespace ragesamples {

using namespace rage;


// PURPOSE: Create and test physical scenes with replay.
class PhysicsReplayManager : public physicsSampleManager
{
public:
	virtual void InitClient ()
	{
		// Verify the compile options needed to test replay.
	#if !TEST_REPLAY
		Quitf("Turn on TEST_REPLAY in diag/debuglog.h to test replay.");
	#endif

		if (phSimulator::UsingCollisionTasks())
		{
			Quitf("Turn off PHCOLLISION_USE_TASKS in physics/simulator.cpp to test replay.");
		}

		if (phConfig::GetForceSolverWorkerThreadCount() > 0)
		{
			Quitf("Turn off force solver worker threads to test replay.");
		}

	#if !__DEBUGLOG
		Quitf("Turn on __DEBUGLOG in diag/debuglog.h to test replay.");
	#endif

		// STEP #1. Initialize the base physics sample class.
		physicsSampleManager::InitClient();


		// STEP #2. Create all the sets of interacting objects, with a sample page for each set.
		//-- "+" and "-" keys control paging through the following set of sample pages.
		phConstraintMgr* constraintMgr;
		phDemoObject* object;
		phDemoObject* crate1;
		phDemoObject* crate2;
		phConstraint* constraint;
		Vector3 position,rotation,worldAttachPoint,worldAttachPointA,worldAttachPointB;
		int component = 0;
		bool alignBottom;
		m_HotdogCart = NULL;
		m_Ragdoll = NULL;

		///////////////////////////
		// Page 0: a stack of boxes
		position.Set(0.0f,-4.0f,0.0f);
		phDemoWorld& stackWorld = MakeNewDemoWorld("Stack",NULL,position);
		Vector3 boxSize(1.0f,1.0f,1.0f);
		int numHigh = 8;
		int iBox;
		for( iBox = 0; iBox < numHigh; iBox++ )
		{
			stackWorld.CreateBox( boxSize, 1.0f, Vector3( 0, iBox+position.y, 0), true );
		}

		//////////////////////////////////////////////////////////////////////////////////////////////
		// Page 1: a row of physically active crates connected by fixed point constraints with length,
		// which allow points on neighboring crates to separate up to a certain distance
		phDemoWorld& lengthWorld = MakeNewDemoWorld("long constraints");
		constraintMgr = lengthWorld.GetSimulator()->GetConstraintMgr();
		position.Set(1.5f,1.0f,0.0f);
		rotation.Zero();
		alignBottom = false;
		crate1 = lengthWorld.CreateObject("crate",position,alignBottom,rotation);
		position.Set(0.0f,1.1f,0.0f);
		crate2 = lengthWorld.CreateObject("crate",position,alignBottom,rotation);
		worldAttachPointA.Set(1.0f, 1.5f, 0.0f);
		worldAttachPointB.Set(0.5f, 1.6f, 0.0f);
		float constraintLength = 0.5f;
		constraintMgr->AttachObjects('Srp0', worldAttachPointA,worldAttachPointB,crate1->GetPhysInst(),crate2->GetPhysInst(),component,component,constraintLength);
		position.Set(-1.5f,1.2f,0.0f);
		phDemoObject* crate3 = lengthWorld.CreateObject("crate",position,alignBottom,rotation);
		worldAttachPointA.Set(-0.5f, 1.6f, 0.0f);
		worldAttachPointB.Set(-1.0f, 1.7f, 0.0f);
		constraintMgr->AttachObjects('Srp1', worldAttachPointA,worldAttachPointB,crate2->GetPhysInst(),crate3->GetPhysInst(),component,component,constraintLength);
		position.Set(-3.0f,1.3f,0.0f);
		phDemoObject* crate4 = lengthWorld.CreateObject("crate",position,alignBottom,rotation);
		worldAttachPointA.Set(-2.0f, 1.7f, 0.0f);
		worldAttachPointB.Set(-2.5f, 1.8f, 0.0f);
		// This attaches crate4 and crate3 together with a point constraint.
		constraintMgr->AttachObjects('Srp2', worldAttachPointA,worldAttachPointB,crate3->GetPhysInst(),crate4->GetPhysInst(),component,component,constraintLength);

		////////////////////////
		// Page 2: hanging signs
		phDemoWorld& signWorld = MakeNewDemoWorld("signs");
		position.Set(-2.0f,2.0f,0.0f);
		rotation.Set(ORIGIN);
		alignBottom = false;
		phInst* sign1 = signWorld.CreateObject("box_flat",position,alignBottom,rotation)->GetPhysInst();
		constraintMgr = signWorld.GetSimulator()->GetConstraintMgr();
		worldAttachPoint.Set(position);
		worldAttachPoint.x -= 0.4f;
		// This attaches sign1 to the world at one point.
		constraintMgr->AttachObjectToWorld('Srp3', worldAttachPoint,sign1);
		worldAttachPoint.z -= 0.4f;
		// This attaches sign1 to the world at another point, so that it can rotate only about one axis.
		constraintMgr->AttachObjectToWorld('Srp4', worldAttachPoint,sign1);
		position.x += 4.0f;
		phDemoObject* sign2 = signWorld.CreateObject("table_top",position,alignBottom,rotation);
		Assert(sign2);
		// This prevents sign2 from translating.
		PHSIM->GetConstraintMgr()->AttachObjectToWorld('Srp5', sign2->GetPhysInst());
		// This constrains sign2 to rotate only about the z axis.
		phConstraint* rotationConstraint = PHSIM->GetConstraintMgr()->AttachObjectToWorld('Srp6', sign2->GetPhysInst());
		rotationConstraint->SetDegreesConstrained(2,ZAXIS);
		position.Set(0.0f,1.4f,2.0f);
		object = signWorld.CreateObject("capsule_fat",position);
		PHSIM->GetConstraintMgr()->AttachObjectToWorld('Srp7', sign2->GetPhysInst());
		rotationConstraint = PHSIM->GetConstraintMgr()->AttachObjectToWorld('Srp8', sign2->GetPhysInst());
		rotationConstraint->SetDegreesConstrained(3,ORIGIN);
		rotationConstraint->SetHardLimitMin(Vector3(-0.5f*PI,-0.5f*PI,-0.5f*PI));
		rotationConstraint->SetHardLimitMax(Vector3( 0.5f*PI, 0.5f*PI, 0.5f*PI));
		Vector3 softLimits(0.3f,0.1f,0.5f);
		rotationConstraint->SetSoftLimitMin(-softLimits);
		rotationConstraint->SetHardLimitMax(softLimits);
		position.Set(0.0f,3.0f,2.0f);
		rotation.Set(0.0f,0.0f,0.0f);
		object = signWorld.CreateObject("box_flat",position,alignBottom,rotation);
		position.x += object->GetPhysInst()->GetArchetype()->GetBound()->GetBoundingBoxMax().x;
		constraintMgr->AttachObjectToWorld('Srp9', position,object->GetPhysInst());
		constraint = constraintMgr->AttachObjectToWorld('SrpA', position,object->GetPhysInst());
		constraint->SetFixedRotation();
		constraint->SetDegreesConstrained(2,ZAXIS);
		position.Set(-2.0f,3.0f,2.0f);
		rotation.Set(0.0f,0.0f,0.0f);
		object = signWorld.CreateObject("box_flat",position,alignBottom,rotation);
		Vector3 halfWidth(object->GetPhysInst()->GetArchetype()->GetBound()->GetBoundingBoxMax());
		position.x += halfWidth.x;
		position.z += halfWidth.z;
		constraintMgr->AttachObjectToWorld('SrpB', position,object->GetPhysInst());
		position.z -= 2.0f*halfWidth.z;
		constraintMgr->AttachObjectToWorld('SrpC', position,object->GetPhysInst());
		constraint = constraintMgr->AttachObjectToWorld('SrpD', position,object->GetPhysInst());
		constraint->SetFixedRotation();
		constraint->SetDegreesConstrained(1,ZAXIS);
		constraint->SetHardLimitMin(0.0f);
		constraint->SetHardLimitMax(PI);
		position.x -= halfWidth.x;
		position.y += 1.0f;
		position.z += halfWidth.z;
		signWorld.CreateObject("sphere_056",position);

		// Page 3: blocks resting on the ground
		phDemoWorld& blockWorld = MakeNewDemoWorld("blocks");
		blockWorld.CreateObject("crate",Vector3(+2.0f, 0.0f, -1.0f),true);
		blockWorld.CreateObject("cinderblock",Vector3(0.2f, 0.0f, -0.9f),true);
		blockWorld.CreateObject("long_crate",Vector3(-1.0f, 0.0f, -2.0f),true);

		////////////////////
		// Page 4: a ragdoll
		phDemoWorld& ragdollWorld = MakeNewDemoWorld("dragging ragdoll","plane_complex");
		position.Set(-4.0f,1.0f,-2.0f);
		phArticulatedObject::CreateHuman(ragdollWorld,position);

		////////////////////////////////////////////////////////////////
		// Page 5: a hotdog cart and a ragdoll using the fragment system
		m_Demos.AddDemo(rage_new fragWorld("replayFragProps"));
		m_Demos.GetCurrentWorld()->Init(1000);
		m_Demos.GetCurrentWorld()->Activate();
		m_Demos.GetCurrentWorld()->ConstructTerrainPlane(false);
		const char* propFile = "$/fragments/HotDogRed_rex/entity.type";
		fragType* propType = fragType::Load(propFile);
		Assertf(propType, "Fragment type missing: %s", propFile);
		Matrix34 cartMatrix(M34_IDENTITY);
		cartMatrix.d.y = 2.0f;
		m_HotdogCart = rage_new fragInst(propType,cartMatrix);
		m_HotdogCart->Insert(false);
		PHSIM->ActivateObject(m_HotdogCart->GetLevelIndex());
		const char* ragdollFile = "$/fragments/detectivedc/entity.type";
		fragType* ragdollType = fragType::Load(ragdollFile);
		Assertf(ragdollType, "Fragment type missing: %s", ragdollFile);
		Matrix34 ragdollMatrix(M34_IDENTITY);
		ragdollMatrix.d.x = 2.0f;
		ragdollMatrix.d.y = 2.0f;
		m_Ragdoll = rage_new fragInst(ragdollType,ragdollMatrix);
		m_Ragdoll->Insert(false);
		PHSIM->ActivateObject(m_Ragdoll->GetLevelIndex());

		// STEP #3. Set and activate the initial page, from the command line parameter "startingDemo" with the first page as default.
		m_Demos.SetCurrentDemo(0);
		m_Demos.GetCurrentWorld()->Activate();
		m_ReplayTimer.Reset();
		m_ResetOnce = false;
	}


	virtual void InitCamera ()
	{
		Vector3 lookFrom(-1.0f,3.5f,6.0f);
		Vector3 lookTo(0.0f,1.0f,0.0f);
		InitSampleCamera(lookFrom,lookTo);
	}


	virtual void Reset ()
	{
		physicsSampleManager::Reset();
		m_ReplayTimer.Reset();
		TIME.Reset();
		m_Demos.GetCurrentWorld()->Reset();
		if (m_Demos.GetCurrentNumber()==m_Demos.GetNumDemos()-1)
		{
			FRAGMGR->Reset();
			if (m_HotdogCart)
			{
				m_HotdogCart->Reset();
				PHSIM->ActivateObject(m_HotdogCart->GetLevelIndex());
			}

			if (m_Ragdoll)
			{
				m_Ragdoll->Reset();
				PHSIM->ActivateObject(m_Ragdoll->GetLevelIndex());
			}
		}
	}


	virtual void UpdateClient ()
	{
		const float replayTestTime = 8.0f;
		physicsSampleManager::UpdateClient();
		if (!m_ResetOnce)
		{
			if (m_ReplayTimer.GetTime()>replayTestTime*2.0f)
			{
				// The time limit was hit, so reset.
				// This goes beyond the time limit to avoid a crash in the replay logger when the replay time gets too close to the first run time.
				m_ResetOnce = true;
				Reset();
			}
		}
		else if (m_ReplayTimer.GetTime()>replayTestTime)
		{
			// Replay testing succeeded.
			m_ReplayTimer.Reset();
			if (m_Demos.GetCurrentNumber()<m_Demos.GetNumDemos()-1)
			{
				// Move to the next world and keep testing.
				m_Demos.NextWorld(&m_CamMgr);

				// Reset the random number seed and the already-reset flag.
				g_ReplayRand.Reset(72);
				m_ResetOnce = false;

				// Reset the system timer.
				TIME.Reset();

			#if __DEBUGLOG
				// Reinitialize replay logging.
				const bool replay = false;
				const bool abortOnFailure = true;
				diagDebugLogInit(replay,abortOnFailure);
				g_ReplayRand.SetReplayDebug(true);
			#endif

			}
			else
			{
				// Replay testing succeeded for all the worlds.
				m_WantsExit = true;
				if (m_HotdogCart)
				{
					m_HotdogCart->Remove();
				}

				if (m_Ragdoll)
				{
					m_Ragdoll->Remove();
				}
			}
		}
	}


	virtual void DrawClient ()
	{
		physicsSampleManager::DrawClient();
	}


	virtual grcSetup& AllocateSetup()
	{
		return *(rage_new grmSetup());
	}


protected:
	fragInst* m_HotdogCart;
	fragInst* m_Ragdoll;
	sysTimer m_ReplayTimer;
	bool m_ResetOnce;
};

} // namespace ragesamples


int Main()
{
	ragesamples::PhysicsReplayManager samplePhysicsReplay;
	samplePhysicsReplay.Init();
	samplePhysicsReplay.UpdateLoop();
	samplePhysicsReplay.Shutdown();
    return 0;
}

