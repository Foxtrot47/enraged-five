// 
// /sample_gta_viewer.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
// 
using namespace rage;

#include "sample_rmcore/sample_rmcore.h"

#include "atl/array.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "grcore/im.h"
#include "grcore/state.h"
#include "grcore/effect.h"
#include "grcore/effect_values.h"
#include "grmodel/shadergroup.h"

#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxvolume.h"
#include "rmptfx/keyframe.h"

#include "system/main.h"
#include "system/param.h"
#include "system/platform.h"
#include "system/timemgr.h"
#include "math/random.h"

PARAM(file,"[gtaviewer]");


/***************************************************************/
class gtaSample : public ragesamples::rmcSampleManager {
public:
	Vector4 m_BackgroundColor;
	bool	m_DrawBackground;
	bool	m_DrawGrid;
	bool	m_Draw3dGrid;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	grcEffectGlobalVar sm_gParticleLightingParams;
	grcEffectGlobalVar sm_gvLightAmbientColour0;
	grcEffectGlobalVar sm_gvLightAmbientColour1;
	grcEffectGlobalVar sm_gvDirectionalLight;
	grcEffectGlobalVar sm_gvDirectionalColor;
	grcEffectGlobalVar sm_gvHDRMultiplier;
	grcEffectGlobalVar sm_gvFogParams;
	grcEffectGlobalVar sm_gvFogColor;

	void InitLights()
	{
		sm_gParticleLightingParams	= grcEffect::LookupGlobalVar("ParticleLightingParams");
		sm_gvLightAmbientColour0	= grcEffect::LookupGlobalVar("LightAmbientColor0");
		sm_gvLightAmbientColour1	= grcEffect::LookupGlobalVar("LightAmbientColor1");
		sm_gvDirectionalLight		= grcEffect::LookupGlobalVar("DirectionalLight");
		sm_gvDirectionalColor		= grcEffect::LookupGlobalVar("DirectionalColour");
		sm_gvHDRMultiplier			= grcEffect::LookupGlobalVar("globalScalars");
		sm_gvFogParams				= grcEffect::LookupGlobalVar("globalFogParams");
		sm_gvFogColor				= grcEffect::LookupGlobalVar("globalFogColor");
	}

	s32 SetupLights()
	{
		Vector4 lightDir(-5000.0f, -10000.0f, 15000.0f, 1.0f);
		lightDir.Normalize();

		grcEffect::SetGlobalVar(sm_gParticleLightingParams,		Vector4(1.0f, 1.0f, 1.0f, 1.0f));
		grcEffect::SetGlobalVar(sm_gvLightAmbientColour0,		Vector4(0.25f, 0.25f, 0.25f, 1.0f));
		grcEffect::SetGlobalVar(sm_gvLightAmbientColour1,		Vector4(-0.1f, -0.1f, -0.1f, 0.0f));
		grcEffect::SetGlobalVar(sm_gvDirectionalLight,			lightDir);
		grcEffect::SetGlobalVar(sm_gvDirectionalColor,			Vector4(0.9f, 0.9f, 0.9f, 1.0f));
		grcEffect::SetGlobalVar(sm_gvHDRMultiplier,				Vector4(1.0f, 1.0f, 1.0f, 1.0f));
		grcEffect::SetGlobalVar(sm_gvFogParams,					Vector4(40.0f*40.0f, 3000.0f*3000.0f, 0.01f, 1.0f));
		grcEffect::SetGlobalVar(sm_gvFogColor,					Vector4(1.0f, 0.0f, 0.0f, 0.0f));

		return 1;
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	bool CompareResourceExtension(const char* fileExtn, const char* codeExtn)
	{
		while(*fileExtn && *codeExtn)
		{
			if (!((*codeExtn == '#' && *fileExtn == g_sysPlatform) ||
				*codeExtn == *fileExtn))
			{
				return false;
			}
			++fileExtn;
			++codeExtn;
		}
		return *fileExtn == 0 && *codeExtn == 0;
	}

	void InitClient() 
	{
		EnableLogoDraw(true);
		EnableBackgroundDraw(true);
		m_BackgroundColor.Set(GetBackGroundColor().GetRedf(),GetBackGroundColor().GetGreenf(),GetBackGroundColor().GetBluef(),GetBackGroundColor().GetAlphaf());
		m_DrawBackground=true;
		m_DrawGrid=true;
		m_Draw3dGrid=false;

#if __PFDRAW
		GetRageProfileDraw().Init(2000000, true, grcBatcher::BUF_FULL_IGNORE);
		GetRageProfileDraw().SetEnabled(true);
#endif

		// For resourced particles rage uses versioning.
		//pgRscBuilder::AddVersionNumberToExtension();	

		rmPtfxManager::InitClass();
		RMPTFXMGR.CreatePointPool(10000);
		RMPTFXMGR.CreateEffectInstPool(5000);
		RMPTFXMGR.CreateDrawBuckets(1);

		const char* fileName = NULL;
		PARAM_file.Get(fileName);
		if (fileName)
		{
			const char* extn = 	ASSET.FindExtensionInPath(fileName);
			if (!strcmp(extn, ".fxlist"))
			{
				RMPTFXMGR.LoadFxList(fileName);
			}
			else if (CompareResourceExtension(extn, ".#pfl"))
			{
				RMPTFXMGR.UseResources();
				RMPTFXMGR.LoadFxList(fileName);
			}
			else if (!strcmp(extn, ".effectrule"))
			{
				RMPTFXMGR.LoadEffectRule(fileName);
			}
		}

		InitLights();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void InitShaderSystems()
	{
		rmcSampleManager::InitShaderSystems();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void ShutdownClient() 
	{
#if __PFDRAW
		GetRageProfileDraw().Shutdown();
#endif
		rmPtfxManager::ShutdownClass();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void UpdateClient() 
	{
		SetClearColor(Color32(m_BackgroundColor.x,m_BackgroundColor.y,m_BackgroundColor.z,m_BackgroundColor.w));
		rmcSampleManager::UpdateClient();
		RMPTFXMGR.Update(TIME.GetSeconds());
		RMPTFXMGR.EndFrame();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void DrawClient() 
	{
		SetupLights();

		RMPTFXMGR.Draw();
		//RMPTFXMGR.EndFrame();

#if __PFDRAW
		grcBindTexture(NULL);
		grcState::Default();
		grcLightState::SetEnabled(true);

		const Matrix34 & cameraMtx = GetCamMgr().GetWorldMtx();
		GetRageProfileDraw().SendDrawCameraForServer(cameraMtx);
		GetRageProfileDraw().Render(true);
#endif
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void InitCamera()
	{
		rmcSampleManager::InitCamera();
		rmcSampleManager::InitSampleCamera(Vector3(0.f, -15.f, 4.f), Vector3(0.f,0.f,0.f));
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if __BANK
	void OnDrawBackgroundChanged()
	{
		EnableBackgroundDraw(m_DrawBackground);
	}
	void OnDrawGridChanged()
	{
		EnableGridDraw(m_DrawGrid);
	}
	void OnDraw3DGridChanged()
	{
		EnableGrid3DDraw(m_Draw3dGrid);
	}
	void AddWidgetsClient() 
	{
		bkBank* bank =&BANKMGR.CreateBank("GTA",false);
		//bank->AddToggle("Multithreading",&(RMPTFXTHREADER.m_PtxMultithreaded));
		//background seems to not work
		//bank->AddToggle("DrawBackground", &m_DrawBackground, datCallback(MFA(gtaSample::OnDrawBackgroundChanged),this));
		bank->AddToggle("DrawGrid", &m_DrawGrid, datCallback(MFA(gtaSample::OnDrawGridChanged),this));
		bank->AddToggle("Draw3dGrid", &m_Draw3dGrid, datCallback(MFA(gtaSample::OnDraw3DGridChanged),this));
		bank->AddColor("Background Color",&m_BackgroundColor);

		
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
};



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// main application
int Main()
{
#if __XENON
	GRCDEVICE.SetSize(1280,720);
#else
	GRCDEVICE.SetSize(1024,760);
#endif

	gtaSample sample;
	sample.SetZUp(true);
	sample.Init();

#ifndef __XENON
	GRCDEVICE.SetBlockOnLostFocus(false);
#endif

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}

