// 
// /sample_gta_viewer.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_rmcore/sample_rmcore.h"

#include "devcam/cammgr.h"
#include "file/asset.h"
#include "file/device_relative.h"
#include "file/packfile.h"

#include "fragment/type.h"

#include "grcore/texture.h"
#include "rmcore/drawable.h"

#include "system/main.h"
#include "system/param.h"
#include "system/typeinfo.h"

using namespace rage;

PARAM(file,"[sample_gta_viewer] Toplevel datafile to load (default: common:/data/gta.dat)");
XPARAM(shaderlibs);
XPARAM(shaderdb);

// PURPOSE:	Given a filename, produce a location key if it's in the right format.
//			This is how we unambiguously identify an object on the disk.
static u32 GetLocation(const char *filename)
{
	if (!strncmp(filename,"stream:/",8))
		return atoi(filename+8);
	else
		return 0;
}

// PURPOSE:	Subclass fiPackfile to extend FindEntry functionality so that a magic stream
//			key filename is translated directly into an entry pointer.
class keyedPackfile: public fiPackfile
{
public:
	const fiPackEntry *FindEntry(const char *name) const
	{
		unsigned location = GetLocation(name);
		if (location)
			return m_Entries + (location & 0xFFFFFF);
		else
			return fiPackfile::FindEntry(name);
	}
};

// PURPOSE:	Singleton which handles all stream:/ device requests.  It knows how to translate
//			a stream:/xxxx key into the originating packfile and entry within that packfile.
class ImgDevice: public fiDevice
{
public:
	ImgDevice() : m_TheDevice(NULL), m_TheHandle(fiHandleInvalid)
	{
	}

	static void AddImgFile(const char *imgFile)

	{
		keyedPackfile *packfile = rage_new keyedPackfile;
		if (!packfile->Init(imgFile,false))
			Quitf("Unable to init packfile '%s'",imgFile);
		packfile->SetRelativePath("");
		sm_Packfiles[sm_PackfileCount++] = packfile;
	}

	static u32 GenerateName(const char *filename)
	{
		for (int i=0; i<sm_PackfileCount; i++)
		{
			const fiPackEntry *entry = sm_Packfiles[i]->FindEntry(filename);
			if (entry)
			{
				u32 offset = entry - sm_Packfiles[i]->FindEntry("/");
				return offset | (i << 24);
			}
		}
		return 0;
	}

	int GetResourceInfo(const char *filename,datResourceInfo &outHeader) const
	{
		u32 location = GetLocation(filename);
		fiPackfile *device = sm_Packfiles[location>>24];
		return device->GetResourceInfo(filename,outHeader);
	}

	fiHandle Open(const char*,bool) const { Assert(0); return fiHandleInvalid; }

	fiHandle Create(const char*) const { Assert(0); return fiHandleInvalid; }

	int Read(fiHandle,void*,int) const { Assert(0); return -1; }

	int Write(fiHandle,const void*,int) const { Assert(0); return -1; }

	int Close(fiHandle) const { Assert(0); return -1; }

	int Seek(fiHandle,int,fiSeekWhence) const { Assert(0); return -1; }

	fiHandle OpenBulk(const char *filename,u64 &outBias) const
	{
		Assert(!m_TheDevice && m_TheHandle==fiHandleInvalid);
		u32 location = GetLocation(filename);
		fiPackfile *device = sm_Packfiles[location>>24];
		m_TheHandle = device->OpenBulk(filename,outBias);
		if (m_TheHandle != fiHandleInvalid)
			m_TheDevice = device;
		return (fiHandle) 0;
	}

	int ReadBulk(fiHandle ASSERT_ONLY(handle),u64 offset,void *outBuffer,int bufferSize) const
	{
		Assert(handle == (fiHandle)0);
		return m_TheDevice->ReadBulk(m_TheHandle,offset,outBuffer,bufferSize);
	}

	int CloseBulk(fiHandle ASSERT_ONLY(handle)) const
	{
		Assert(handle == (fiHandle)0);
		m_TheDevice = NULL;
		m_TheHandle = fiHandleInvalid;
		return 0;
	}

	u64 GetFileTime(const char*) const { Assert(0); return 0; }

	bool SetFileTime(const char*,u64) const { Assert(0); return false; }

	u32 GetAttributes(const char *filename) const
	{
		u32 location = GetLocation(filename);
		fiPackfile *device = sm_Packfiles[location>>24];
		return device->GetAttributes(filename);
	}

	u64 GetFileSize(const char *filename) const
	{
		u32 location = GetLocation(filename);
		fiPackfile *device = sm_Packfiles[location>>24];
		return device->GetFileSize(filename);
	}

private:
	mutable const fiDevice *m_TheDevice;
	mutable fiHandle m_TheHandle;

	static keyedPackfile *sm_Packfiles[256];
	static int sm_PackfileCount;
};

keyedPackfile *ImgDevice::sm_Packfiles[256];
int ImgDevice::sm_PackfileCount;
ImgDevice s_TheImgDevice;

typedef void	(*PlaceFunc)(pgBase*,datResource&);
typedef pgBase* (*SetCurrent)(pgBase*);

// PURPOSE:	A module contains everything we need to know about a particular type of asset; its
//			file extension, expected resource version, and a function to place (resource construct)
//			it once it's resident.  Note that destruction is not necessary here because everything
//			that is "toplevel streamable" derives from pgBase and that has a virtual destructor.
struct Module
{
	const char *	m_Extension;
	int				m_ExpectedVersion;
	PlaceFunc		m_PlaceFunc;
	SetCurrent		m_SetCurrent;

	static Module sm_Modules[];
};

// PURPOSE:	For convenience, explicitly declare the supported streamable types.
//			A real application would be able to extend this at runtime
enum { MODULE_TEXDICT, MODULE_DRAWABLE, MODULE_DRAWABLEDICT, MODULE_FRAGTYPE };

// PURPOSE:	The predefined list of modules, indexed by the MODULE_... enumerants above.
Module Module::sm_Modules[] = 
{
	{ "#td", grcTexture::RORC_VERSION, (PlaceFunc) pgDictionary<grcTexture>::Place, (SetCurrent) pgDictionary<grcTexture>::SetCurrent },
	{ "#dr", rmcDrawable::RORC_VERSION, (PlaceFunc) rmcDrawable::Place, NULL },
	{ "#dd", rmcDrawable::RORC_VERSION, (PlaceFunc) pgDictionary<rmcDrawable>::Place, NULL },
	{ "#ft", fragType::RORC_VERSION, (PlaceFunc) fragType::Place, NULL },
};

/* PURPOSE:	Streamable object.  Each streamable object knows its module index (which also
			defines its type).  It also has a reference count, state, and dependency chain
			pointer, and knows its location on disc, and finally has a pointer to the actual
			object if it's resident. */
class Streamable
{
public:
	enum { NONRESIDENT, PENDING, RESIDENT, LEAVING };

	void Init(int moduleIndex)
	{
		m_Module = moduleIndex;
		m_State = NONRESIDENT;
		m_RefCount = 0;
		m_Parent = NULL;
		m_Pointer = NULL;
		m_Location = 0;
		m_Next = NULL;		// Could have regular separate lists here, but this is simpler.
	}

	static void InitClass()
	{
		sysMemAllocator &alloc = *sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);
		sysMemBuddyAllocator &ba = (sysMemBuddyAllocator&)(alloc);

		sm_HeapBase = ba.GetHeapBase();
		sm_HeapSize = ba.GetHeapSize();
		sm_HeapShift = 12;		// should get this from somewhere, not hard-code it
		sm_HeapRemap = rage_new Streamable*[sm_HeapSize >> sm_HeapShift];
		memset(sm_HeapRemap, 0, (sm_HeapSize >> sm_HeapShift) * sizeof(Streamable*));
	}

	Module& GetModule() const { return Module::sm_Modules[m_Module]; }

	int GetState() const { return m_State; }

	void SetState(int newState) { 
		// Remove self from correct old list
		if (m_State == PENDING)
			RemoveSelfFromList(&sm_FirstPending);
		else if (m_State == RESIDENT)
			RemoveSelfFromList(&sm_FirstResident);
		else if (m_State == LEAVING)
			RemoveSelfFromList(&sm_FirstLeaving);

		m_State = newState; 

		if (newState == PENDING)
		{
			AddSelfToList(&sm_FirstPending);

			// If we are becoming resident, AddRef our parents.
			// Do it now so they cannot accidentally get evicted between when we're pending and resident.
			Streamable *i = GetParent();
			while (i)
			{
				i->AddRef();
				i = i->GetParent();
			}
		}
		else if (newState == RESIDENT)
		{
			AddSelfToList(&sm_FirstResident);
		}
		else if (newState == LEAVING)
		{
			AddSelfToList(&sm_FirstLeaving);

			// Should only be in this state if our refcount is zero
			Assert(m_RefCount == 0);
			// Now it's a countdown before we're actually deleted
			m_RefCount = 3;
		}
		else if (newState == NONRESIDENT)
		{
			// Remove this asset from the heap remap table
			datResourceMap deathMap;
			m_Pointer->RegenerateMap(deathMap);
			for (int i=0; i<deathMap.VirtualCount + deathMap.PhysicalCount; i++)
				SetHeapPageOwner(deathMap.Chunks[i].DestAddr, NULL);

			// There is no "nonresident" list.
			++sysMemIsDeleting;
			delete m_Pointer;
			--sysMemIsDeleting;

			m_Pointer = NULL;

			// If we've been deleted, Release our parents.
			// Only do it now once we're gone so that parents can't get finalized before we do.
			Streamable *i = GetParent();
			while (i)
			{
				i->Release();
				i = i->GetParent();
			}
		}
	}

	void AddRef()
	{
		// If we were on our way out and somebody needs us again, come back!
		if (m_State == LEAVING)
		{
			SetState(RESIDENT);
			m_RefCount = 1;
		}
		else
		{
			Assert(m_State == RESIDENT);
			++m_RefCount;
		}
	}

	void Release()
	{
		Assert(m_RefCount);

		Assert(m_State == RESIDENT || m_State == LEAVING);
		// If we've lost our final reference, enter the leaving state.
		if (--m_RefCount == 0)
		{
			if (m_State == RESIDENT)
				SetState(LEAVING);
			else
				SetState(NONRESIDENT);
		}
	}

	int GetRefCount()
	{
		return m_RefCount;
	}

	void SetParent(Streamable *parent)
	{
		// If there's already a parent, make sure it matches the one we're specifying.
		Assert(!m_Parent || parent==m_Parent);
		m_Parent = parent;
	}

	Streamable* GetParent() const { return m_Parent; }

	void PatchReferences(const datResourcePatch &patch)
	{
		patch.PointerFixup(m_Pointer);
		m_Pointer->PatchReferences(patch);
	}

	pgBase* GetPointer() const { return m_Pointer; }

	void SetPointer(pgBase *p) { m_Pointer = p; }

	u32 GetLocation() const { return m_Location; }

	void SetLocation(u32 l) { m_Location = l; }

	int GetResourceInfo(datResourceInfo &info) const
	{
		char namebuf[32];
		formatf(namebuf,sizeof(namebuf),"stream:/%u",m_Location);
		return s_TheImgDevice.GetResourceInfo(namebuf,info);
	}

	static void SetHeapPageOwner(void *ptr,Streamable *asset)
	{
		Assert(ptr >= sm_HeapBase && ptr < (char*)sm_HeapBase + sm_HeapSize);
		u32 idx = ((char*)ptr - (char*)sm_HeapBase) >> sm_HeapShift;
		Assert(!asset || !sm_HeapRemap[idx] || sm_HeapRemap[idx] == asset);
		sm_HeapRemap[idx] = asset;
	}

	static Streamable* GetHeapPageOwner(void *ptr)
	{
		Assert(ptr >= sm_HeapBase && ptr < (char*)sm_HeapBase + sm_HeapSize);
		return sm_HeapRemap[((char*)ptr - (char*)sm_HeapBase) >> sm_HeapShift];
	}

	void Place(const datResourceMap &map)
	{
		datResource rsc(map,"place",false);
		Module::sm_Modules[m_Module].m_PlaceFunc(m_Pointer = (pgBase*)rsc.GetBase(), rsc);
		m_Pointer->MakeDefragmentable(map,true);
		SetState(RESIDENT);

		// Set up remap table entries
		for (int i=0; i<map.VirtualCount + map.PhysicalCount; i++)
			Streamable::SetHeapPageOwner(map.Chunks[i].DestAddr,this);

	}

	void Defragment(const datResourceMap &map)
	{
		datResource rsc(map,"defrag",true);
		// Invoke the place function again.  We intentionally reassign the pointer here because it
		// may have changed if it was the first virtual block that moved.
		Module::sm_Modules[m_Module].m_PlaceFunc(m_Pointer = (pgBase*)rsc.GetBase(), rsc);
		// Remember the new location of any nodes that moved!
		m_Pointer->MakeDefragmentable(map,false);
	}

	static Streamable* GetFirstResident() { return sm_FirstResident; }

	static Streamable* GetFirstLeaving() { return sm_FirstLeaving; }

	Streamable* GetNext() const { return m_Next; }

private:
	u32 m_Module : 5,		// Type handler
		m_State : 2,		// NONRESIDENT, PENDING, RESIDENT, LEAVING
		m_RefCount : 25;	// Reference count if state != LEAVING.
	Streamable *m_Parent;	// Pointer to dependent object if any (could be a list at some point)
	pgBase *m_Pointer;		// Pointer to object if resident.
	u32 m_Location;			// Location of the object in permanent store

	void AddSelfToList(Streamable** first)
	{
		Assert(m_Next == NULL);
		m_Next = *first;
		*first = this;
	}

	void RemoveSelfFromList(Streamable** first)
	{
		while (*first)
		{
			if (*first == this)
			{
				*first = m_Next;
				m_Next = NULL;
				return;
			}
			first = &(*first)->m_Next;
		}
		Quitf("Corrupted streamer list.");
	}

	Streamable *m_Next;		// Next object in same state as us.
	static Streamable *sm_FirstPending, *sm_FirstResident, *sm_FirstLeaving;

	static void *sm_HeapBase;
	static size_t sm_HeapSize;
	static size_t sm_HeapShift;
	static Streamable **sm_HeapRemap;
};

Streamable *Streamable::sm_FirstPending;
Streamable *Streamable::sm_FirstResident;
Streamable *Streamable::sm_FirstLeaving;
void *Streamable::sm_HeapBase;
size_t Streamable::sm_HeapSize;
size_t Streamable::sm_HeapShift;
Streamable **Streamable::sm_HeapRemap;

/* PURPOSE:	Very simplified abstraction of a storage manager.  This allows objects
			to store an s16 index and also avoids loading duplicate copies of objects. */
class Store
{
public:
	Store(int maxCount,int moduleIndex)
	{
		m_Count = 0;
		m_Streamables = rage_new Streamable[maxCount];
		m_MaxCount = maxCount;
		m_ModuleIndex = moduleIndex;
		for (int i=0; i<maxCount; i++)
			m_Streamables[i].Init(moduleIndex);
	}

	~Store()
	{
		delete[] m_Streamables;
	}

	s16 Add(const char *name)
	{
		char namebuf[32];
		const char *ext = Module::sm_Modules[m_ModuleIndex].m_Extension;
		formatf(namebuf,sizeof(namebuf),"%s.%s",name,ext);
		while (strchr(namebuf,'#'))
			*strchr(namebuf,'#') = g_sysPlatform;
		unsigned location = ImgDevice::GenerateName(namebuf);
		if (!location)
			return -1;
		// Texture dictionaries, among others, may get added more than once.
		for (int i=0; i<m_Count; i++)
			if (m_Streamables[i].GetLocation() == location)
				return (s16) i;
		formatf(namebuf,sizeof(namebuf),"stream:/%u",location);
		datResourceInfo info;
		int actualVersion = s_TheImgDevice.GetResourceInfo(namebuf,info);
		int expectedVersion = Module::sm_Modules[m_ModuleIndex].m_ExpectedVersion;
		if (actualVersion != expectedVersion)
		{
			Errorf("Resource %s out of date, got %d, expected %d",namebuf,actualVersion,expectedVersion);
			return -1;
		}
		if (m_Count == m_MaxCount)
			Quitf("Store is full, raise count above %d",m_MaxCount);
		m_Streamables[m_Count].SetLocation(location);
		Assert(m_Count < 32768);
		return (s16)(m_Count++);
	}

	int GetCount() const
	{
		return m_Count;
	}

	Streamable& GetStreamable(s16 key) const
	{
		Assert(key>=0 && key<m_Count);
		return m_Streamables[key];
	}

protected:
	u32 *m_Keys;
	Streamable *m_Streamables;
	int m_Count, m_MaxCount, m_ModuleIndex;
};

/* PURPOSE:	Adds type safety to the Store class. */
template <class T> class TypedStore: public Store 
{
public:
	TypedStore(int maxCount,int moduleIndex) : Store(maxCount,moduleIndex) { }

	T* operator[](int idx) const
	{
		if (idx == -1)
			return NULL;
		else
			return (T*) m_Streamables[idx].GetPointer();
	}
};

class gtaSample: public ragesamples::rmcSampleManager 
{
	struct Type
	{
		Vector4 Center;
		u32 Hash;
		s16 DrIndex;
		s16 TdIndex;
		s16 DdIndex;
		s16 FtIndex;
	};
	struct Instance
	{
		Matrix34 Mtx;
		int TypeIndex;
	};
public:
	gtaSample()
		: m_TdStore(3000,MODULE_TEXDICT)
		, m_DdStore(600,MODULE_DRAWABLEDICT)
		, m_FtStore(800,MODULE_FRAGTYPE)
		, m_DrStore(22000,MODULE_DRAWABLE)
	{
		// Might as well force the correct GTA shader paths here.
		PARAM_shaderlibs.Set("x:/gta/build/common/shaders");
		PARAM_shaderdb.Set("x:/gta/build/common/shaders/db");

		m_Pending = pgStreamer::Error;
		m_OutOfMemory = false;
	}

	virtual const char *GetSampleName() const
	{
		return "sample_gta_viewer";
	}

	void InitClient()
	{
		// Initialize the streamer
		pgStreamer::InitClass(256);

		// Setup proper GTA relative devices so files load correctly.
		m_Common.Init("x:/gta/build/common/");
		m_Common.MountAs("common:/");

		m_CommonImg.Init("x:/gta/build/common/");
		m_CommonImg.MountAs("commonimg:/");

		m_Platform.Init("x:/gta/build/independent/");
		m_Platform.MountAs("platform:/");

		m_PlatformImg.Init(__PPU? "x:/gta/build/ps3/" : "x:/gta/build/xbox360/");
		m_PlatformImg.MountAs("platformimg:/");

		fiDevice::Mount("stream:/",s_TheImgDevice,true);

		const char *datFile = "common:/data/gta.dat";
		// const char *datFile = "common:/data/animviewer.dat";
		PARAM_file.Get(datFile);

		// Load the toplevel world definition file
		LoadDatFile(datFile);

		Displayf("%d instances, %d types, %d drawables, %d drawable dicts, %d frag types, %d tex dicts loaded",
			m_Instances.GetCount(),m_Types.GetCount(),m_DrStore.GetCount(),m_DdStore.GetCount(),m_FtStore.GetCount(),m_TdStore.GetCount());

		m_LightMode = grclmNone;

		Streamable::InitClass();
	}

	void InitCamera()
	{
		SetZUp(true);
		GetCamMgr().SetUseZUp(true);
		GetCamMgr().SetCamera(dcamCamMgr::CAM_ROAM);
	}

	void LoadDatFile(const char *filename)
	{
		fiSafeStream S(ASSET.Open(filename,"dat"));
		if (!S)
			Quitf("Unable to open '%s'",filename);
		char linebuf[256];
		while (fgetline(linebuf,sizeof(linebuf),S))
		{
			if (linebuf[0]=='#' || linebuf[0]==0)
				continue;
			const char *verb = strtok(linebuf," \t");
			const char *noun = strtok(NULL," \t");
			if (!strcmp(verb,"IMGLIST"))
			{
				fiSafeStream L(ASSET.Open(noun,""));
				if (!L)
					Quitf("Unable to open imglist '%s'",noun);
				char linebuf[256];
				while (fgetline(linebuf,sizeof(linebuf),L))
				{
					if (linebuf[0]=='#' || linebuf[0]==0)
						continue;
					const char *img = strtok(linebuf," \t");
					// ignore the loadflag
					strcat(linebuf,".img");		// cheesy but will work because we already consumed loadFlag
					LoadImgFile(img);
				}
				Displayf("Done processing IMGLIST.");
			}
			else if (!strcmp(verb,"IMG"))
				LoadImgFile(noun);
			else if (!strcmp(verb,"IDE"))
				LoadIdeFile(noun);
			else if (!strcmp(verb,"IPL"))
				LoadIplFile(noun);
			else
				Warningf("Ignoring unknown verb '%s'",verb);
		}
	}

	void LoadImgFile(const char *imgFile)
	{
		ImgDevice::AddImgFile(imgFile);
	}

	void LoadIdeFile(const char *ideFile)
	{
		enum IDELoadingStatus {
			LOADING_NOTHING,
			LOADING_OBJECTS,
			LOADING_MLO,
			LOADING_MLO_INST,
			LOADING_MLO_ROOM,
			LOADING_MLO_ROOM_OBJLIST,
			LOADING_MLO_PORTALS,
			LOADING_TIMEOBJS,
			LOADING_WEAPONS,
			LOADING_CLUMP,
			LOADING_ANIMATED_CLUMP,
			LOADING_VEHICLES,
			LOADING_PEDS,
			LOADING_TREES,
			LOADING_2DEFFECTS,
			LOADING_TXDPARENTS,
			LOADING_PATHNODES,
			LOADING_PATHLINKS,
			LOADING_AUDIO_MATS,
		};

		fiSafeStream S(ASSET.Open(ideFile,""));
		if (!S)
			Quitf("Cannot open IDE file '%s'",ideFile);
		char linebuf[256];
		IDELoadingStatus status = LOADING_NOTHING;
		while (fgetline(linebuf,sizeof(linebuf),S))
		{
			// Remove commas
			char *comma;
			while ((comma = strchr(linebuf,',')) != NULL)
				*comma = ' ';
			// Check for section changes.
			if (!strcmp(linebuf,"end"))
				status = LOADING_NOTHING;
			else if (status == LOADING_NOTHING)
			{
				if (!strcmp(linebuf,"objs"))
					status = LOADING_OBJECTS;
				else if (!strcmp(linebuf,"tobj"))
					status = LOADING_TIMEOBJS;
				else if (!strcmp(linebuf,"txdp"))
					status = LOADING_TXDPARENTS;
			}
			else if (status == LOADING_OBJECTS || status == LOADING_TIMEOBJS)
			{
				const int MODEL_NAME_LEN = 24;
				char name[MODEL_NAME_LEN];
				char txdName[MODEL_NAME_LEN];
				char ddName[MODEL_NAME_LEN];
				float lodDist;
				Vector3 bbMin(0.0f, 0.0f, 0.0f);
				Vector3 bbMax(0.0f, 0.0f, 0.0f);
				Vector3 bsCentre(0.0f, 0.0f, 0.0f);
				float bsRadius = 0.0f;
				u32 flags=0;
				float specialAttribute=0.0f;		// This is a float just now but should be changed into an uint32 once the IDEs have been re-exported.
				int numParams;

				ddName[0] = '\0';

				// Object types now have an extra field 'attribute' that is an exclusive attribute that the specifies whether
				// a type is a garage door/clothing rack in a shop etc. This makes some of the bits in the flags field obsolete.
				// To make sure things still work in the transition phase we deal with 15 AND 16 parameters.
				// (timeobjs have an extra number at the end that we ignore)
				numParams = sscanf(linebuf, "%s %s %f %d %f %f %f %f %f %f %f %f %f %f %f %s",
					&name[0], &txdName[0], &lodDist, &flags, &specialAttribute,
					&bbMin.x, &bbMin.y, &bbMin.z,
					&bbMax.x, &bbMax.y, &bbMax.z,
					&bsCentre.x, &bsCentre.y, &bsCentre.z,
					&bsRadius, &ddName[0]);
				if (numParams != 16)
					Quitf("Old IDE file format found: [%s] %d",linebuf,numParams);
				// Displayf("%s.%s.%s %x %.f",name,txdName,ddName,flags,specialAttribute);

				s16 tdIndex = strcmp(txdName,"null")? m_TdStore.Add(txdName) : -1;
				s16 ddIndex = strcmp(ddName,"null")? m_DdStore.Add(ddName) : -1;
				s16 drIndex = ddIndex==-1? m_DrStore.Add(name) : -1;
				s16 ftIndex = drIndex==-1 && ddIndex==-1? m_FtStore.Add(name) : -1;
				ftIndex = -1;	// HACK - fragtypes don't load right now.
				// Make sure we found either a drawable dict, a drawable, or a fragment.
				/* Assert((ddIndex != -1 && drIndex == -1 && ftIndex == -1)
					|| (ddIndex == -1 && (drIndex != -1 || ftIndex != -1))); */

				Type &type = m_Types.Grow();
				type.Hash = atStringHash(name);
				type.Center.Set(bsCentre.x,bsCentre.y,bsCentre.z,bsRadius);
				type.DrIndex = drIndex;
				type.TdIndex = tdIndex;
				type.DdIndex = ddIndex;
				type.FtIndex = ftIndex;

				Streamable *dependent = NULL;
				if (ddIndex != -1 && tdIndex != -1)
				{
					// Drawable dict depends on tex dict
					m_DdStore.GetStreamable(ddIndex).SetParent(&m_TdStore.GetStreamable(tdIndex));
				}
				if (ddIndex != -1)
					dependent = &m_DdStore.GetStreamable(ddIndex);
				else if (tdIndex != -1)
					dependent = &m_TdStore.GetStreamable(tdIndex);
				if (drIndex != -1)
					m_DrStore.GetStreamable(drIndex).SetParent(dependent);
				if (ftIndex != -1)
					m_FtStore.GetStreamable(ftIndex).SetParent(dependent);
			}
			else if (status == LOADING_TXDPARENTS)
			{
				char txdName[25], txdParentName[25];
				sscanf(linebuf,"%s %s",txdName,txdParentName);
				s16 txd = m_TdStore.Add(txdName);
				if (txd != -1)
				{
					if (strcmp(txdParentName,"root"))
					{
						s16 txdParent = m_TdStore.Add(txdParentName);
						Assert(txdParent != -1);
						m_TdStore.GetStreamable(txd).SetParent(&m_TdStore.GetStreamable(txdParent));
					}
				}
				else
					Errorf("Unable to find texture dictionary '%s' anywhere.",txdName);
			}
		}
	}

	void LoadIplFile(const char *iplFile)
	{
		enum IPLLoadingStatus {
			LOADING_NOTHING,
			LOADING_IGNORE,
			LOADING_INSTANCES,
			LOADING_MULTIBUILDINGS,
			LOADING_ZONES,
			LOADING_MAPZONES,
			LOADING_CULLZONES,
			LOADING_OCCLUSION,
			LOADING_GARAGE,
			LOADING_ENTRYEXIT,
			LOADING_PICKUPS,
			LOADING_CARGENERATORS,
			LOADING_STUNTJUMPS,
			LOADING_TIMECYCLEMODS,
			LOADING_AUDIOZONES,
			LOADING_PATHNODES,
			LOADING_PATHLINKS,
			LOADING_BLOCKS,
			LOADING_MLOAPPEND,
			LOADING_2DEFFECTS
		};

		fiSafeStream S(ASSET.Open(iplFile,""));
		if (!S)
			Quitf("Cannot open IPL file '%s'",iplFile);
		Displayf("Parsing instances from '%s'",iplFile);
		char linebuf[256];
		IPLLoadingStatus status = LOADING_NOTHING;
		while (fgetline(linebuf,sizeof(linebuf),S))
		{
			// Remove commas
			char *comma;
			while ((comma = strchr(linebuf,',')) != NULL)
				strcpy(comma,comma+1);
			// Check for section changes.
			if (!strcmp(linebuf,"end"))
				status = LOADING_NOTHING;
			else if (status == LOADING_NOTHING)
			{
				if (!strcmp(linebuf,"inst"))
					status = LOADING_INSTANCES;
			}
			else if (status == LOADING_INSTANCES)
			{
				char typeName[24];
				int flags;
				Vector3 pos;
				Quaternion quat;
				int lodIndex, blockIndex;
				float lodDistance;
				int numParams = sscanf(linebuf,"%s %d  %f %f %f  %f %f %f %f  %d %d %f",
					typeName,&flags,&pos.x,&pos.y,&pos.z,&quat.x,&quat.y,&quat.z,&quat.w,&lodIndex,&blockIndex,&lodDistance);
				if (numParams != 12)
					Quitf("Old IPL file found: [%s] %d",linebuf,numParams);

				Instance &inst = m_Instances.Grow();
				inst.Mtx.FromQuaternion(quat);
				inst.Mtx.d.Set(pos);
				u32 hash = atStringHash(typeName);
				int i = 0;
				for (i=0; i<m_Types.GetCount(); i++)
					if (m_Types[i].Hash == hash)
						break;
				if (i == m_Types.GetCount())
				{
					Errorf("Unknown instance %s",typeName);
					m_Instances.Pop();
				}
				else
					inst.TypeIndex = i;
			}
		}
	}

	bool FoundOwner(const sysMemDefragmentation &defrag,int i,Streamable *j)
	{
		pgBase *p = j->GetPointer();
		int ptrSlot = p->MapContainsPointer(defrag.Nodes[i].From);
		if (ptrSlot != -1)
		{
			// Regenerate the entire map from compressed store
			datResourceMap defragMap;
			p->RegenerateMap(defragMap);

			// Disconnect the object from any global lists before potentially moving parts of them
			p->BeforeCopy();

			// Fix the destination address of the portion we're moving
			// TODO: Could identify multiple nodes in same allocation and do the work at once.
			defragMap.Chunks[ptrSlot].DestAddr = defrag.Nodes[i].To;
			// Make sure the size we think the node is and the size the buddy heap think it is match.
			Assert(defrag.Nodes[i].Size == defragMap.Chunks[ptrSlot].Size);

			// Perform the copy (intentionally leave the source unmolested)
			memcpy(defrag.Nodes[i].To, defrag.Nodes[i].From, defrag.Nodes[i].Size);

			// Mark previous location of chunk as having no owner
			Streamable::SetHeapPageOwner(defrag.Nodes[i].From, NULL);

			// Mark new location of chunk as having this owner.
			Streamable::SetHeapPageOwner(defrag.Nodes[i].To, j);

			// Reinvoke the resource constructor so that this object knows its new state
			j->Defragment(defragMap);

			// Now go through everything else and patch any outstanding references. This is a heinous N*N loop!
			// You should not patch the object you just defragmented.  It ought to be harmless because
			// the pointer will have already been adjusted out of the source range and will not be
			// adjusted again.  Patching is defined as a separate operation from resource construction because
			// objects typically have many fewer external references than internal pointers.  Also, by defining
			// them as separate objects, non-resourced game-level objects can participate in patching as well.
			datResourcePatch patch(defragMap.Chunks[ptrSlot]);
			for (Streamable *k = Streamable::GetFirstResident(); k; k=k->GetNext())
				if (k != j)
					k->PatchReferences(patch);
			return true;
		}
		return false;
	}

	void UpdateClient()
	{
		// Already something in progress.
		if (m_Pending != pgStreamer::Error)
			return;

		// Locate next best candidate
		const Vector3 &camPos = GetCameraMatrix().d;
		float bestDist2 = 200.0f * 200.0f;		// Don't bother with anything further away than this
		float worstDist2 = 0;
		Streamable *bestAsset = NULL, *worstAsset = NULL;
		for (int i=0; i<m_Instances.GetCount(); i++)
		{
			float dist2 = m_Instances[i].Mtx.d.Dist2(camPos);
			Type &type = m_Types[m_Instances[i].TypeIndex];
			Streamable *asset = (type.DrIndex != -1)? &m_DrStore.GetStreamable(type.DrIndex) : (type.FtIndex != -1)? &m_FtStore.GetStreamable(type.FtIndex) : (type.DdIndex != -1)? &m_DdStore.GetStreamable(type.DdIndex) : NULL;
			if (asset)
			{
				if (dist2 < bestDist2 && asset->GetState() == Streamable::NONRESIDENT)
				{
					bestAsset = asset;
					bestDist2 = dist2;
				}
				else if (m_OutOfMemory && dist2 > worstDist2 && asset->GetState() == Streamable::RESIDENT)
				{
					worstDist2 = dist2;
					worstAsset = asset;
				}
			}
		}

		// If there's something that needs to be resident, schedule it.
		m_OutOfMemory = false;
		if (bestAsset)
		{
			// Identify the lowest-level asset in dependency chain that isn't resident yet.
			while (bestAsset->GetParent() && bestAsset->GetParent()->GetState() == Streamable::NONRESIDENT)
				bestAsset = bestAsset->GetParent();
			datResourceInfo info;
			int rscVer = bestAsset->GetResourceInfo(info);

			if (!pgRscBuilder::ValidateMap("file",m_PendingMap,info))
			{
				sysMemAllocator &alloc = *sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);
				sysMemDefragmentation defrag;
				if (alloc.BeginDefragmentation(defrag))
				{
					for (int i=0; i<defrag.Count; i++)
						AssertVerify(FoundOwner(defrag,i,Streamable::GetHeapPageOwner(defrag.Nodes[i].From)));
					alloc.EndDefragmentation();

					// Try to allocate again after the defrag is done
					m_OutOfMemory = !pgRscBuilder::ValidateMap("file",m_PendingMap,info);
					if (!m_OutOfMemory)
						Displayf("Defragmentation succeeded!  Allocation now worked.");
					else
						Displayf("Defragmentation didn't free up enough memory.");
				}
				else
				{
					Displayf("Out of memory and nothing to defragment!");
					m_OutOfMemory = true;
				}
			}
			if (!m_OutOfMemory)
			{
				char buf[32];
				formatf(buf,sizeof(buf),"stream:/%u",bestAsset->GetLocation());
				m_Pending = pgStreamer::Open(buf, NULL, rscVer);
				Assert(m_Pending != pgStreamer::Error);

				if (pgStreamer::Read(m_Pending, m_PendingMap.Chunks,m_PendingMap.PhysicalCount+m_PendingMap.VirtualCount,
					sizeof(datResourceFileHeader),ReadCompleteStub,this)) 
				{
					m_PendingAsset = bestAsset;
					bestAsset->SetState(Streamable::PENDING);
				}
				else
				{
					pgStreamer::Close(m_Pending);
					m_Pending = pgStreamer::Error;
				}
			}
		}

		// If there's something that is far away, kill it.
		if (worstAsset)
		{
			// Only leaf assets should show up here.
			Assert(worstAsset->GetRefCount() == 0);
			worstAsset->SetState(Streamable::LEAVING);
		}


		// Anything already on the death list will count down before dying.
		Streamable *deathRow = Streamable::GetFirstLeaving();
		while (deathRow)
		{
			// Copy the next ptr before calling Release since the state may change and modify the next ptr.
			Streamable *nextToDie = deathRow->GetNext();
			deathRow->Release();
			deathRow = nextToDie;
		}
	}

	static void ReadCompleteStub(void *userArg,void * /*dest*/,u32 /*offset*/,u32 /*amtRead*/)
	{
		((gtaSample*)userArg)->ReadComplete();
	}

	void ReadComplete()
	{
		// Displayf("streaming operation complete");
		pgStreamer::Close(m_Pending);
		m_Pending = pgStreamer::Error;
		Streamable *dependent = m_PendingAsset->GetParent();
		pgBase *prevState = NULL;
		if (dependent && dependent->GetModule().m_SetCurrent)
			prevState = dependent->GetModule().m_SetCurrent(dependent->GetPointer());
		m_PendingAsset->Place(m_PendingMap);
		if (dependent && dependent->GetModule().m_SetCurrent)
			dependent->GetModule().m_SetCurrent(prevState);
	}

	void DrawDrawable(const Instance &inst,rmcDrawable *drawable)
	{
		if (drawable)
		{
			u8 lod;
			if (drawable->IsVisible(inst.Mtx,*grcViewport::GetCurrent(),lod,NULL))
				drawable->Draw(inst.Mtx,0,lod);
		}
	}

	void DrawClient()
	{
		for (int i=0; i<m_Instances.GetCount(); i++)
		{
			Type &type = m_Types[m_Instances[i].TypeIndex];
			if (type.DrIndex != -1 && m_DrStore.GetStreamable(type.DrIndex).GetState() == Streamable::RESIDENT)
				DrawDrawable(m_Instances[i],m_DrStore[type.DrIndex]);
			else if (type.DdIndex != -1 && m_DdStore.GetStreamable(type.DdIndex).GetState() == Streamable::RESIDENT)
				DrawDrawable(m_Instances[i],m_DdStore[type.DdIndex]->Lookup(type.Hash));
		}
	}

private:
	fiDeviceRelative m_Common, m_CommonImg, m_Platform, m_PlatformImg;

	TypedStore< pgDictionary<grcTexture> > m_TdStore;
	TypedStore< pgDictionary<rmcDrawable> > m_DdStore;
	TypedStore< pgDictionary<fragType> > m_FtStore;
	TypedStore< rmcDrawable > m_DrStore;
	atArray<Type> m_Types;
	atArray<Instance> m_Instances;
	pgStreamer::Handle m_Pending;
	datResourceMap m_PendingMap;
	Streamable *m_PendingAsset;
	bool m_OutOfMemory;
};


int Main()
{
	gtaSample sampleGta;
	sampleGta.Init("x:/gta/build");

	sampleGta.UpdateLoop();

	sampleGta.Shutdown();

	return 0;
}
