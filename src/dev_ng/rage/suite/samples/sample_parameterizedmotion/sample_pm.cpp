// 
// /sample_pm.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


#include "sample_cranimation/sample_cranimation.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cranimation/frame.h"
#include "crclip/clip.h"
#include "crclip/framecompositor.h"
#include "crextra/expressions.h"
#include "crparameterizedmotion/blenddatabase.h"
#include "crparameterizedmotion/parameter.h"
#include "crparameterizedmotion/parameterizedmotionsimple.h"
#include "crparameterizedmotion/parameterizedmotionvariable.h"
#include "crparameterizedmotion/parametersampler.h"
#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "grcore/im.h"
#include "input/pad.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"


using namespace rage;

XPARAM(file);

PARAM(demo, "Parameterized motion demo index");
PARAM(pm, "Parameterized motion file (Example: \"abc.spm\")");
PARAM(sampler, "Optional sampler config string, used for debug rendering (Example: \"locomotion()\" )");
PARAM(expr, "Optional expression file (Example: \"xyz.expr\")");


const char* g_DemoPmFiles[] = 
{
	"$/animation/pm/walk/walk1.spm",
	"$/animation/pm/punch1/punch1.spm",
	"$/animation/pm/aim1hand/aim1.spm",
	"$/animation/pm/pickup/pickup.spm",
	"$/animation/pm/punchcombo/punchcombo.spm",
	"$/animation/pm/aim2hand/aim2.spm",
	"$/animation/pm/fastwalk/fastwalk.spm",
	"$/animation/pm/lookat/lookat.spm",
	"$/animation/pm/run/run.spm",
	"$/animation/pm/walktotarget/walktotarget.spm",
	"$/animation/pm/Step/Step.spm",
	"$/animation/pm/HitReaction/HitReaction.spm",
};

const char* g_DemoSamplerConfigs[] =
{
	"locomotion(samplespeed(false))",
	"spatial(bonename(Char_L_Hand))",
	"aim(aimbonename(Char_R_Hand),basebonename(Char_Spine3),wrap(160))",
	"spatial(bonename(Char_R_Hand),minimal(false),axes(xz))",
	"spatial(bonename(Char_R_Hand),minimal(false),axes(xz))",
	"aim(aimbonename(Char_R_Hand),basebonename(Char_Spine3),wrap(160),negate(1))",
	"locomotion(samplespeed(true))",
	"aim(aimbonename(Char_Head))",
	"locomotion(samplespeed(false))",
	"spatial(bonename(Char_L_Foot),minimal(true),axes(xy))",
	"spatial(bonename(Char_R_Foot),minimal(true),axes(z),majoraxis(y))",
	"container(clipproperty(propertyname(HitHeight)),clipproperty(propertyname(HitDirection)))",
};

const char* g_DemoFiles[] = 
{
	"$/sample_cranimation/mp3skel/entity.type",
	"$/sample_cranimation/mp3skel/entity.type",
	"$/sample_cranimation/mp3skel/entity.type",
	"$/sample_cranimation/mp3skel/entity.type",  // TODO --- make newskel
	"$/sample_cranimation/mp3skel/entity.type",  // TODO --- make newskel
	"$/sample_cranimation/mp3skel/entity.type",  // TODO --- make newskel
	"$/sample_cranimation/mp3skelnew/entity.type",
	"$/sample_cranimation/mp3skelnew/entity.type",
	"$/sample_cranimation/mp3skelnew/entity.type",
	"$/sample_cranimation/mp3skelnew/entity.type",
	"$/sample_cranimation/mp3skelnew/entity.type",
	"$/sample_cranimation/mp3skelnew/entity.type",
};

const char* g_DemoExprFiles[] = 
{
	"",
	"",
	"",
	"",  // TODO --- make newskel
	"",  // TODO --- make newskel
	"",  // TODO --- make newskel
	"$/sample_cranimation/mp3skelnew/AnimExpr.expr",
	"$/sample_cranimation/mp3skelnew/AnimExpr.expr",
	"$/sample_cranimation/mp3skelnew/AnimExpr.expr",
	"$/sample_cranimation/mp3skelnew/AnimExpr.expr",
	"$/sample_cranimation/mp3skelnew/AnimExpr.expr",
	"$/sample_cranimation/mp3skelnew/AnimExpr.expr",
};

class crSampleParameterizedMotion : public ragesamples::craSampleManager
{
public:
	crSampleParameterizedMotion() 
		: ragesamples::craSampleManager()
		, m_Pm(NULL)
		, m_Sampler(NULL)
		, m_Expressions(NULL)
		, m_SnapToEntry(0)
		, m_ResetOnLoop(false)
		, m_FromPad(false)
		, m_DumpBlendWeights(false)
		, m_DumpToViewport(false)
		, m_DumpCollapsed(true)
		, m_U(0.f)
	{
	}

protected:

	void InitClient()
	{
		craSampleManager::InitClient();

		crClip::InitClass();
		crpmParameterSampler::InitClass();
		crExpressions::InitClass();

		crSkeleton& skel = GetSkeleton();
		const crSkeletonData& skelData = skel.GetSkeletonData();

		m_Frame.InitCreateBoneAndMoverDofs(skelData, true);

		int demoIdx = 0;
		PARAM_demo.Get(demoIdx);

		bool defaultPmFileUsed = true;
		const char* defaultPmFile = g_DemoPmFiles[demoIdx];
		if(PARAM_pm.Get(defaultPmFile))
		{
			defaultPmFileUsed = false;
		}

		if(defaultPmFileUsed)
		{
			g_UnitUp = ZAXIS;
			SetZUp(true);
		}

		crpmParameterizedMotionData* pmData = crpmParameterizedMotionData::AllocateAndLoad(defaultPmFile);
		if(!pmData)
		{
			Quitf("Failed to load pm data file '%s'", defaultPmFile);
		}

		m_Pm = crpmParameterizedMotion::AllocateFromData(*pmData);

		m_ResetOnLoop = !m_Pm->IsLooped();
		m_Pm->SetLooped(true);

		const char* samplerConfig = defaultPmFileUsed?g_DemoSamplerConfigs[demoIdx]:NULL;
		if(!PARAM_sampler.Get(samplerConfig))
		{
			if(m_Pm->GetProperties())
			{
				const crProperty* propSampler = m_Pm->GetProperties()->FindProperty("PmSamplerConfig");
				if(propSampler)
				{
					const crPropertyAttribute* attribConfig = propSampler->GetAttribute("PmSamplerConfig");
					if(attribConfig && attribConfig->GetType() == crPropertyAttribute::kTypeString)
					{
						samplerConfig = static_cast<const crPropertyAttributeString*>(attribConfig)->GetString().c_str();
					}
				}
			}
		}

		if(samplerConfig)
		{
			crpmParameterizedMotionSimpleData* spmData = static_cast<crpmParameterizedMotionSimpleData*>(pmData);
			m_Sampler = crpmParameterSampler::BuildSampler(samplerConfig, spmData->GetRegistrationCurveData(), *spmData->GetClips(), skelData, NULL, false);
			if(!m_Sampler)
			{
				Quitf("Bad sampler config string?");
			}
		}
		else
		{
			Warningf("Missing sampler config string, debug rendering disabled");
		}

		int numParamDims = m_Pm->GetNumParameterDimensions();

		m_Parameter.Init(numParamDims);

		crpmParameter minParam(numParamDims);
		crpmParameter maxParam(numParamDims);
		m_Pm->GetParameterBoundaries(minParam, maxParam);

		m_Parameter.Lerp(0.5f, minParam, maxParam, &m_Pm->GetParameterization());
		

		const char* defaultExprFile = g_DemoExprFiles[demoIdx];
		PARAM_expr.Get(defaultExprFile);

		if(defaultExprFile && defaultExprFile[0])
		{
			m_Expressions = crExpressions::AllocateAndLoad(defaultExprFile);
			if(!m_Expressions)
			{
				Quitf("Failed to load expr file '%s'", defaultExprFile);
			}
		}

#if __BANK
		bkBank &bk = BANKMGR.CreateBank("Parameter(s)", 50, 500);

		for(int i=0; i<numParamDims; ++i)
		{
			bk.AddSlider("Parameter", &m_Parameter[i], minParam[i], maxParam[i], 0.01f);
		}

		bk.AddToggle("From Game Pad", &m_FromPad);

		bk.AddSlider("Snap to Entry", &m_SnapToEntry, 0, 1000, 1.f, datCallback(MFA(crSampleParameterizedMotion::SnapToEntryCB), this));

		bk.AddToggle("Dump Blend Weights", &m_DumpBlendWeights);
		bk.AddToggle("Dump to Viewport", &m_DumpToViewport);
		bk.AddToggle("Dump Collapsed", &m_DumpCollapsed);

//		bk.AddSlider("U", &m_U, 0.f, 1.f, 0.001f);

		if(pmData->GetType() == crpmParameterizedMotionData::kPmDataTypeVariable)
		{
			crpmParameterizedMotionVariableData* vpmData = static_cast<crpmParameterizedMotionVariableData*>(pmData);

			const int numVariations = vpmData->GetNumVariations();
			m_Variations.Resize(numVariations);

			for(int i=0; i<numVariations; ++i)
			{
				m_Variations[i] = 0;

				char buf[64];
				sprintf(buf, "Variation[%d]", i);
				bk.AddSlider(buf, &(m_Variations[i]), 0, vpmData->GetNumOptions(i)-1, 1.f, datCallback(MFA(crSampleParameterizedMotion::UpdateVariationsCB), this));
			}
		}
#endif // __BANK

		pmData->Release();
	}

#if __BANK
	void SnapToEntryCB()
	{
		const crpmBlendDatabase& bd = *static_cast<crpmParameterizedMotionSimple*>(m_Pm)->GetSimpleData()->GetBlendDatabase();

		crpmParameter parameter(bd.GetNumParameterDimensions(), 0.f);
		crpmWeights weights(bd.GetNumWeightDimensions(), 0.f);
		crpmVariations variations;

		bd.GetEntry(m_SnapToEntry, parameter, weights, variations);
		if(IsClose(weights.GetWeightMax(), 1.f, 0.001f))
		{
			m_Parameter = parameter;
		}
	}

	void UpdateVariationsCB()
	{
		crpmParameterizedMotionVariable* vpm = static_cast<crpmParameterizedMotionVariable*>(m_Pm);
		const crpmParameterizedMotionVariableData* vpmData = vpm->GetVariableData();
		
		const int numVariations = vpmData->GetNumVariations();
		for(int i=0; i<numVariations; ++i)
		{
			vpm->SetVariationOptionByIndex(i, m_Variations[i]);
		}
	}
#endif // __BANK

	void ShutdownClient()
	{
		if(m_Pm)
		{
			delete m_Pm;
			m_Pm = NULL;
		}

		if(m_Sampler)
		{
			delete m_Sampler;
			m_Sampler = NULL;
		}

		if(m_Expressions)
		{
			delete m_Expressions;
			m_Expressions = NULL;
		}

		crExpressions::ShutdownClass();
		crpmParameterSampler::ShutdownClass();
		crClip::ShutdownClass();

		craSampleManager::ShutdownClient();
	}

	void UpdateClient()
	{
		if(m_FromPad)
		{
			const float damping = 0.95f;
			static float x0 = 0.f;
			static float y0 = 0.f;

			float x = ioPad::GetPad(0).GetNormLeftX();
			float y = ioPad::GetPad(0).GetNormLeftY();

			const float dead = 0.15f;
			if(fabsf(x) < dead && fabsf(y) < dead)
			{
				x = 0.f;
				y = 0.f;
			}

			x*=-1.f;
			y*=-1.f;

			x0 = Lerp(damping, x, x0);
			y0 = Lerp(damping, y, y0);

			int numParamDims = m_Pm->GetNumParameterDimensions();
			crpmParameter minParam(numParamDims);
			crpmParameter maxParam(numParamDims);
			m_Pm->GetParameterBoundaries(minParam, maxParam);

			if(minParam.GetValue(0) < 0.f && maxParam.GetValue(0) > 0.f)
			{
				m_Parameter.SetValue(0, m_Pm->GetParameterization().GetDimensionInfo(0).Lerp(fabsf(x0), 0.f, (x0<0.f)?minParam.GetValue(0):maxParam.GetValue(0)));
			}
			else
			{
				m_Parameter.SetValue(0, m_Pm->GetParameterization().GetDimensionInfo(0).Lerp((x0+1.f)*0.5f, minParam.GetValue(0), maxParam.GetValue(0)));
			}
			if(numParamDims > 1)
			{
				if(minParam.GetValue(1) < 0.f && maxParam.GetValue(1) > 0.f)
				{
					m_Parameter.SetValue(1, m_Pm->GetParameterization().GetDimensionInfo(1).Lerp(fabsf(y0), 0.f, (y0<0.f)?minParam.GetValue(1):maxParam.GetValue(1)));
				}
				else
				{
					m_Parameter.SetValue(1, m_Pm->GetParameterization().GetDimensionInfo(1).Lerp((y0+1.f)*0.5f, minParam.GetValue(1), maxParam.GetValue(1)));
				}
			}
		}

		m_Pm->SetParameter(m_Parameter);

		float lastU = m_Pm->GetU();

//		m_Pm->SetLooped(false);
		m_Pm->Update(TIME.GetSeconds() * GetPlaybackRate());

//		m_Pm->SetU(m_U);

		if(m_ResetOnLoop && m_Pm->GetU() < lastU)
		{
			GetMoverMtx() = Mat34V(V_IDENTITY);
		}

		m_Pm->Composite(m_Frame);
		m_Frame.Pose(GetSkeleton());

		if(m_Expressions)
		{
			m_Expressions->Process(m_Frame, 0.f, 0.f, &GetCreature(), &GetSkeleton().GetSkeletonData());
		}

		if(GetSkeleton().GetParentMtx())
		{
			Mat34V mtx;
			if(m_Frame.GetMoverMatrix(0, mtx))
			{
				Transform(GetMoverMtx(), GetMoverMtx(), mtx);
			}
		}

		if(m_DumpBlendWeights)
		{
			Printf("\n");
			m_DumpString.Reset();

			crFrameCompositorDump dumpCompositor;
			m_Pm->Composite(dumpCompositor);

			dumpCompositor.Dump(m_DumpString, m_DumpCollapsed);

			const int len = m_DumpString.GetLength();
			for(int i=0; i<len; ++i)
			{
				const int bufSize=254;
				char buf[bufSize+1];
				int c;
				for(c=0; i<len && c<bufSize; ++i,++c)
				{
					if(m_DumpString[i] == '\n')
					{
						break;
					}
					buf[c] = m_DumpString[i];
				}
				buf[c] = '\0';
				Printf("%s\n", buf);
			}
		}

		craSampleManager::UpdateClient();
	}

	void DrawPoint(Vec3V_In v, const Color32& c, float s)
	{
		const Vec3V axisX(V_X_AXIS_WONE), axisY(V_Y_AXIS_WONE), axisZ(V_Z_AXIS_WONE);
		ScalarV scale = ScalarVFromF32(s);
		grcColor(c);
		grcBegin(drawLines, 6);
		grcVertex3f(v-axisX*scale);
		grcVertex3f(v+axisX*scale);
		grcVertex3f(v-axisY*scale);
		grcVertex3f(v+axisY*scale);
		grcVertex3f(v-axisZ*scale);
		grcVertex3f(v+axisZ*scale);
		grcEnd();
	}

	void DrawClient()
	{
		if(m_Sampler)
		{
			const crpmBlendDatabase& bd = *static_cast<crpmParameterizedMotionSimple*>(m_Pm)->GetSimpleData()->GetBlendDatabase();

			crpmParameter parameter(bd.GetNumParameterDimensions(), 0.f);
			crpmWeights weights(bd.GetNumWeightDimensions(), 0.f);
			crpmVariations variations;

			grcBindTexture(NULL);
			grcLightState::SetEnabled(false);
			grcViewport::SetCurrentWorldIdentity();

			const float s = 0.01f;

			const int numEntries = bd.GetNumEntries();
			for(int i=0; i<numEntries; ++i)
			{
				bd.GetEntry(i, parameter, weights, variations);

				Vec3V v;
				m_Sampler->DebugMapParameter(parameter, GetSkeleton(), v);

				Color32 c = IsClose(weights.GetWeightMax(), 1.f, 0.001f)?Color32(255,0,0,255):Color32(128,128,128,255);
				DrawPoint(v, c, s*((variations==0x2)?3.f:((variations==0x1)?2.f:1.f)));
			}

			Vec3V v;
			m_Sampler->DebugMapParameter(m_Parameter, GetSkeleton(), v);
			DrawPoint(v, Color32(0, 255, 0, 255), s);
		}

		if(m_DumpBlendWeights && m_DumpToViewport)
		{
			float x = 50.f;
			float y = 50.f;

			const int len = m_DumpString.GetLength();
			for(int i=0; i<len; ++i)
			{
				const int bufSize=254;
				char buf[bufSize+1];
				int c;
				for(c=0; i<len && c<bufSize; ++i,++c)
				{
					if(m_DumpString[i] == '\n')
					{
						break;
					}
					buf[c] = m_DumpString[i];
				}
				buf[c] = '\0';
				grcColor(Color32(255,255,255,255));
				grcDraw2dText(x, y, buf);

				y+=12.f;
			}
		}

		craSampleManager::DrawClient();
	}

	const char* GetDefaultFile()
	{
		const char* filename = NULL;
		if(PARAM_file.Get(filename))
		{
			return filename;
		}

		int demoIdx = 0;
		PARAM_demo.Get(demoIdx);

		return g_DemoFiles[demoIdx];
	}

private:
	crpmParameterizedMotion* m_Pm;
	crpmParameterSampler* m_Sampler;
	crpmParameter m_Parameter;
	crFrame m_Frame;
	crExpressions* m_Expressions;
	atArray<int> m_Variations;
	int m_SnapToEntry;
	bool m_ResetOnLoop;
	bool m_FromPad;
	bool m_DumpBlendWeights;
	bool m_DumpToViewport;
	bool m_DumpCollapsed;
	atString m_DumpString;

	float m_U;
};


// main application
int Main()
{
	crSampleParameterizedMotion samplePm;
	samplePm.Init("sample_pm");

	samplePm.UpdateLoop();

	samplePm.Shutdown();

	return 0;
}
