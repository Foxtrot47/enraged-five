// 
// /sample_posematcher.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

// TITLE: Pose matcher sample.
// PURPOSE: This sample demonstrates how to initialize and use
// the pose matcher class provided in crExtra.


#include "sample_motiontree/sample_motiontree.h"

#include "cranimation/animation.h"
#include "cranimation/weightset.h"
#include "crextra/posematcher.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeanimation.h"
#include "crmotiontree/observer.h"
#include "crmotiontree/requestanimation.h"
#include "crmotiontree/requestblend.h"
#include "crmotiontree/requestmirror.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

#define NUM_ANIMS (3)

#define DEFAULT_ANIM0 "$/animation/motion/male_wlk_m.anim"
#define DEFAULT_ANIM1 "$/animation/motion/male_wlk_lft_turn_m.anim"
#define DEFAULT_ANIM2 "$/animation/motion/male_wlk_rgt_turn_m.anim"

PARAM(anim0, "[sample_posematcher] Animation file 0 to load (default is \"" DEFAULT_ANIM0 "\")");
PARAM(anim1, "[sample_posematcher] Animation file 1 to load (default is \"" DEFAULT_ANIM1 "\")");
PARAM(anim2, "[sample_posematcher] Animation file 2 to load (default is \"" DEFAULT_ANIM2 "\")");

#define DEFAULT_WEIGHTSET "$/sample_posematcher/sparse.weightset"

PARAM(weightset, "[sample_posematcher] Weight set (default is \"" DEFAULT_WEIGHTSET "\")");

#define MAX_SKELETONS (3)
#define DEFAULT_WINDOWSIZE (1)

PARAM(windowsize, "[sample_posematcher] Window size (default is 1)");

#define DEFAULT_FILE "$/rage_male/rage_male.type"

XPARAM(file);



namespace ragesamples 
{

	class crSamplePoseMatcher : public ragesamples::crMotionTreeSampleManager
	{
	public:
		crSamplePoseMatcher() 
			: ragesamples::crMotionTreeSampleManager()
		{
			for(int i=0; i<NUM_ANIMS; ++i)
			{
				m_Animations[i] = NULL;
			}

			for(int i=0; i<MAX_SKELETONS; ++i)
			{
				m_Skeletons[i] = NULL;
			}
			m_SkeletonCount = 0;

			m_Observer.AddRef();
		}

	protected:

		void InitClient()
		{
			crMotionTreeSampleManager::InitClient();

			// STEP #1. Load the animations

			// -- This sample uses three animations, which need to be loaded
			// using either their default filenames, or ones provided via the 
			// command line.
			for(int i=0; i<NUM_ANIMS; ++i)
			{
				const char* animName = NULL;
				switch(i)
				{
				case 0: 
					{
						animName = DEFAULT_ANIM0;
						PARAM_anim0.Get(animName);
					}
					break;
				case 1:
					{
						animName = DEFAULT_ANIM1;
						PARAM_anim1.Get(animName);
					}
					break;
				case 2:
					{
						animName = DEFAULT_ANIM2;
						PARAM_anim2.Get(animName);
					}
					break;
				default:
					Assert(0);
				}

				m_Animations[i] = crAnimation::AllocateAndLoad(animName);

				// -- Check that the animations loaded successfully.
				if(!m_Animations[i])
				{
					Quitf("Failed to load animation '%s%s'", ASSET.GetPath(), animName);
				}
			}

			// -STOP


			// STEP #2. Initialize the pose matcher

			// -- Find the skeleton data
			const crSkeletonData& skelData = GetSkeleton().GetSkeletonData();

			// -- Define a window size

			// - Window sizes > 1 will allow the pose matcher to 
			// match features at higher orders than just position
			// ie 2 == velocity, 3 == acceleration etc...
			// - However, these require a set of poses (rather than just
			// the current pose) to be presented at search time
			m_WindowSize = DEFAULT_WINDOWSIZE;
			PARAM_windowsize.Get(m_WindowSize);
			if(!InRange(m_WindowSize, 1, MAX_SKELETONS+1))
			{
				Quitf("Window size must be between 1 and %d", MAX_SKELETONS);
			}

			// -- Load a weight set

			// - Weight sets specify different weights for different joints
			// in the body.  Using this we can control which joints
			// participate in the matching process, and to what extent.
			// - In order for this to work efficiently at runtime, and 
			// use minimal storage using a sparse set of only the key joints 
			// is recommended.
			const char* weightSetName = DEFAULT_WEIGHTSET;
			PARAM_weightset.Get(weightSetName);

			crWeightSet* weightSet = NULL;
			if(weightSetName)
			{
				weightSet = crWeightSet::AllocateAndLoad(weightSetName, skelData);
			}

			// -- Define a default sample rate

			// - This is important for window sizes > 1, it defines the typical
			// frame rate the game will run at.  It can vary from this, but that
			// will effect the quality of higher order matching.
			const float sampleRate = 1.f/60.f;

			// -- Initialize the pose matcher, these parameters can not vary
			// past initialization - they are integral to the building of the database.
			m_PoseMatcher.Init(skelData, weightSet, m_WindowSize, sampleRate);


			// STEP #3. Add samples to the database

			// - Add the three animations to the database

			m_PoseMatcher.AddSamples(*m_Animations[0], 0);
			m_PoseMatcher.AddSamples(*m_Animations[1], 1);
			m_PoseMatcher.AddSamples(*m_Animations[2], 2);


			// STEP #4. Set up motion tree to play animation on character
			crmtRequestAnimation reqAnim(m_Animations[0]);
			reqAnim.SetLooping(true, true);

			GetMotionTree().Request(reqAnim);
			m_Observer.Attach(reqAnim.GetObserver());


			// -STOP

			// clean up temporary weight set
			if(weightSet)
			{
				delete weightSet;
				weightSet = NULL;
			}

			// initialize the stack of skeletons (used in window sizes > 1)
			for(int i=0; i<MAX_SKELETONS; ++i)
			{
				m_Skeletons[i] = rage_new crSkeleton();
				m_Skeletons[i]->Init(skelData);
			}

#if __BANK
			// BANKS?
#endif // __BANK
		}

		void UpdateMotionTree()
		{
			crMotionTreeSampleManager::UpdateMotionTree();

			bool found = false;
			u32 key;
			float time;
			Mat34V transform;

			if(m_WindowSize == 1)
			{
				// STEP #5. Find best match
				// -- If window size == 1 then we only need to present the current pose
				// in order to retrieve a match from the database

				found = m_PoseMatcher.FindBestMatch(GetSkeleton(), key, time, transform);
			}
			else
			{
				// -- If window size > 1 we need to present a history of poses, culminating in the
				// current pose.  The history should contain the same number of poses as the window size.
				// There is no benefit in exceeding the window size, and the database will cope with 
				// fewer poses (though the quality of the match will be poorer).

				// - Allocate a temporary skeleton pointer array
				const int numSkeletons = Min(m_SkeletonCount+1, MAX_SKELETONS+1);
				const crSkeleton** skeletons = Alloca(const crSkeleton*, numSkeletons);

				// - Place the current pose at the end of the skeleton pointer array,
				// and reverse copy the other elements with progressively older
				// captured skeleton poses, so the array contains oldest to current poses.
				skeletons[numSkeletons-1] = &GetSkeleton();
				for(int i=numSkeletons-2,j=MAX_SKELETONS-1; i>=0; --i,--j)
				{
					skeletons[i] = m_Skeletons[j];
				}
				
				// - Find the best pose, with history
				found = m_PoseMatcher.FindBestMatchWithHistory(numSkeletons, skeletons, key, time, transform);

				// - STOP
			}

			if(found)
			{
				// - Display the best match, and the transform.
				const crAnimPlayer& player = static_cast<crmtNodeAnimation*>(m_Observer.GetNode())->GetAnimPlayer();
				Vec3V eulers = Scale(Mat33VToEulersXYZ(transform.GetMat33()), ScalarV(V_TO_RADIANS));
				Vec3V pos = transform.GetCol3();
				Printf("MATCH: %s %.3f == %s %.3f\n       (rot %.3f trans %.3f %.3f %.3f)\n", player.GetAnimation()->GetName(), player.GetTime(), m_Animations[key]->GetName(), time, eulers[1], pos[0], pos[1], pos[2]);
			}

			// - replace oldest captured pose with new pose
			for(int i=1; i<MAX_SKELETONS; ++i)
			{
				SwapEm(m_Skeletons[i], m_Skeletons[i-1]);
			}
			for(int i=0; i<GetSkeleton().GetBoneCount(); ++i)
			{
				m_Skeletons[MAX_SKELETONS-1]->GetLocalMtx(i) = GetSkeleton().GetLocalMtx(i);
				m_Skeletons[MAX_SKELETONS-1]->GetObjectMtx(i) = GetSkeleton().GetObjectMtx(i);
			}
			m_SkeletonCount++;
		}

		void ShutdownClient()
		{
			for(int i=0; i<NUM_ANIMS; ++i)
			{
				if(m_Animations[i])
				{
					m_Animations[i]->Release();
					m_Animations[i] = NULL;
				}
			}

			for(int i=0; i<MAX_SKELETONS; ++i)
			{
				if(m_Skeletons[i])
				{
					delete m_Skeletons[i];
				}
				m_Skeletons[i] = NULL;
			}

			m_Observer.Detach();

			crMotionTreeSampleManager::ShutdownClient();
		}

		void InitMover()
		{
			// suppress creation of a mover inst
		}

		void InitFragment()
		{
			// suppress creation of fragment
		}
	
		const char* GetDefaultFile()
		{
			const char* defaultFile = DEFAULT_FILE;
			PARAM_file.Get(defaultFile);
			return defaultFile;
		}

	private:
		crAnimation* m_Animations[NUM_ANIMS];
		crSkeleton* m_Skeletons[MAX_SKELETONS];
		crWeightSet m_WeightSet;
		crPoseMatcher m_PoseMatcher;
		crmtObserver m_Observer;
		int m_SkeletonCount;
		int m_WindowSize;
	};

} // namespace ragesamples


// main application
int Main()
{
	ragesamples::crSamplePoseMatcher samplePoseMatcher;
	samplePoseMatcher.Init();

	samplePoseMatcher.UpdateLoop();

	samplePoseMatcher.Shutdown();

	return 0;
}

