set TESTERS=sample_posematcher
set SAMPLE_LIBS=sample_rmcore sample_cranimation sample_physics sample_crfragment sample_motiontree %RAGE_SAMPLE_LIBS%
set LIBS=%SAMPLE_LIBS% %RAGE_CORE_LIBS% %RAGE_GFX_LIBS%
set LIBS=%LIBS% %RAGE_CR_LIBS% %RAGE_SUITE_CR_LIBS% %RAGE_PH_LIBS%
set LIBS=%LIBS% breakableglass phglass fragment cloth grrope event spatialdata
set XPROJ=%RAGE_DIR%/base/src %RAGE_DIR%/base/samples %RAGE_DIR%/suite/src %RAGE_DIR%/suite/samples
