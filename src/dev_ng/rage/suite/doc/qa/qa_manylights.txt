3$@@Overview$DTX_SECTION_NOT_FILLED_IN
3$@@Components$DTX_SECTION_NOT_FILLED_IN
3$@@Relationships$DTX_SECTION_NOT_FILLED_IN
3$@@Working Examples$DTX_SECTION_NOT_FILLED_IN
3$Working$POSSIBLE_STRAY_TOPIC
1$rage$FUNC_PARAM_NO_DESC
    2$rage::FastLighting::SetLightsFast4@atArray<Light>&$FUNC_PARAM_NO_DESC
    2$rage::FastLighting::UsesFragment@grmShader&$FUNC_PARAM_NO_DESC
    2$rage::FastLightList::GetHeightRange@const Vector4&@const Vector4&$FUNC_PARAM_NO_DESC
    2$rage::FastSpot4Lighting::SetLightsFast4@atArray<Light>&@float$FUNC_PARAM_NO_DESC
    2$rage::LightingComposite::UsesFragment@grmShader&$FUNC_PARAM_NO_DESC
    2$rage::LightList::CompactLights@LightList&@float@float$FUNC_PARAM_NO_DESC
    2$rage::ManyShadows::UsesFragment@grmShader&$FUNC_PARAM_NO_DESC
1$rage$FUNC_PARAM_NO_NAME
    2$rage::FastLighting::SetLightsFast4@atArray<Light>&$FUNC_PARAM_NO_NAME
    2$rage::FastLighting::UsesFragment@grmShader&$FUNC_PARAM_NO_NAME
    2$rage::FastLightList::GetHeightRange@const Vector4&@const Vector4&$FUNC_PARAM_NO_NAME
    2$rage::FastSpot4Lighting::SetLightsFast4@atArray<Light>&@float$FUNC_PARAM_NO_NAME
    2$rage::LightingComposite::UsesFragment@grmShader&$FUNC_PARAM_NO_NAME
    2$rage::LightList::CompactLights@LightList&@float@float$FUNC_PARAM_NO_NAME
    2$rage::ManyShadows::UsesFragment@grmShader&$FUNC_PARAM_NO_NAME
1$rage$FUNC_RETURN_VAL_NO_DESC
    2$rage::FastLighting::UsesFragment@grmShader&$FUNC_RETURN_VAL_NO_DESC
    2$rage::FastLightList::GetHeightRange@const Vector4&@const Vector4&$FUNC_RETURN_VAL_NO_DESC
    2$rage::Light::GetShadowRange$FUNC_RETURN_VAL_NO_DESC
    2$rage::LightingComposite::UsesFragment@grmShader&$FUNC_RETURN_VAL_NO_DESC
    2$rage::LightList::GetCount$FUNC_RETURN_VAL_NO_DESC
    2$rage::ManyShadows::UsesFragment@grmShader&$FUNC_RETURN_VAL_NO_DESC
1$rage$SYMBOL_NO_DESC
    2$rage::BufferNumber$SYMBOL_NO_DESC
    2$rage::BufferNumber::BufferNumber$SYMBOL_NO_DESC
    2$rage::BufferNumber::count$SYMBOL_NO_DESC
    2$rage::BufferNumber::Get@const$SYMBOL_NO_DESC
    2$rage::BufferNumber::GetNumBuffers@const$SYMBOL_NO_DESC
    2$rage::BufferNumber::GetWorkingSet@const$SYMBOL_NO_DESC
    2$rage::BufferNumber::inc$SYMBOL_NO_DESC
    2$rage::CalculateSpotLightBound@const Light&@float@Vector3&@Vector3&$SYMBOL_NO_DESC
    2$rage::CompareKeyPair$SYMBOL_NO_DESC
    2$rage::CompareKeyPair::CompareKeyPair$SYMBOL_NO_DESC
    2$rage::CompareKeyPair::CompareKeyPair@_T1@_T2$SYMBOL_NO_DESC
    2$rage::CompareKeyPair::first$SYMBOL_NO_DESC
    2$rage::CompareKeyPair::Key@const$SYMBOL_NO_DESC
    2$rage::CompareKeyPair::second$SYMBOL_NO_DESC
    2$rage::CompareKeyPair::value@const$SYMBOL_NO_DESC
    2$rage::CreateFLight@const Light&@FastLight&@int@const Vector3&$SYMBOL_NO_DESC
    2$rage::DrawAABB@const Vector3&@const Vector3&@Color32$SYMBOL_NO_DESC
    2$rage::DrawSphere@const Vector4&@Color32$SYMBOL_NO_DESC
    2$rage::FastLight::FastLight$SYMBOL_NO_DESC
    2$rage::FastLight::FastLight@const Vector3&@const Vector3&@const Vector3&@float@int$SYMBOL_NO_DESC
    2$rage::FastLight::GetLightIndex@const$SYMBOL_NO_DESC
    2$rage::FastLight::GetLightIndexV@const$SYMBOL_NO_DESC
    2$rage::FastLight::InBound@const Vector3&@const Vector3&@const$SYMBOL_NO_DESC
    2$rage::FastLight::InBoundBool@const Vector3&@const Vector3&@const$SYMBOL_NO_DESC
    2$rage::FastLight::index$SYMBOL_NO_DESC
    2$rage::FastLight::Intensity@const Vector3&@const Vector3&@const$SYMBOL_NO_DESC
    2$rage::FastLight::IntensityAtPoint@const Vector3&@const Vector3&@const Vector3&@const Vector3&@const$SYMBOL_NO_DESC
    2$rage::FastLight::invInten$SYMBOL_NO_DESC
    2$rage::FastLight::pos$SYMBOL_NO_DESC
    2$rage::FastLight::rangeMax$SYMBOL_NO_DESC
    2$rage::FastLight::rangeMin$SYMBOL_NO_DESC
    2$rage::FastLighting::CreateVariableCache$SYMBOL_NO_DESC
    2$rage::FastLighting::FastLighting$SYMBOL_NO_DESC
    2$rage::FastLighting::LightVals$SYMBOL_NO_DESC
    2$rage::FastLighting::LightVals::m_amtNormalLights$SYMBOL_NO_DESC
    2$rage::FastLighting::LightVals::m_attenuation$SYMBOL_NO_DESC
    2$rage::FastLighting::LightVals::m_color$SYMBOL_NO_DESC
    2$rage::FastLighting::LightVals::m_directions$SYMBOL_NO_DESC
    2$rage::FastLighting::LightVals::m_position$SYMBOL_NO_DESC
    2$rage::FastLighting::MaxFastLights$SYMBOL_NO_DESC
    2$rage::FastLighting::SetGlobals$SYMBOL_NO_DESC
    2$rage::FastLighting::SetHeightMap@const grcTexture*$SYMBOL_NO_DESC
    2$rage::FastLighting::SetLights@atArray<Light>&@bool$SYMBOL_NO_DESC
    2$rage::FastLighting::SetPositionalTexture@const grcTexture*@const grcTexture*@const grcTexture*@const grcTexture*@const grcTexture*$SYMBOL_NO_DESC
    2$rage::FastLighting::SetRayTracedReflections@const grcTexture*@const grcTexture*$SYMBOL_NO_DESC
    2$rage::FastLighting::SetSortTexture@const grcTexture*@const grcTexture*@const grcTexture*@const Vector3&@const Vector3&$SYMBOL_NO_DESC
    2$rage::FastLighting::SetupShader@grmShader&@VarCache*$SYMBOL_NO_DESC
    2$rage::FastLighting::SetupShaderPass$SYMBOL_NO_DESC
    2$rage::FastLightList$SYMBOL_NO_DESC
    2$rage::FastLightList::[]@int$SYMBOL_NO_DESC
    2$rage::FastLightList::Copy@const FastLightList<NumLights>&$SYMBOL_NO_DESC
    2$rage::FastLightList::count$SYMBOL_NO_DESC
    2$rage::FastLightList::CreateFastLights@const atArray<Light>&@const Vector3&$SYMBOL_NO_DESC
    2$rage::FastLightList::CullFastLights@const FastLightList&@Vector3&@Vector3&$SYMBOL_NO_DESC
    2$rage::FastLightList::fLights$SYMBOL_NO_DESC
    2$rage::FastLightList::GetCount@const$SYMBOL_NO_DESC
    2$rage::FastLightList::GetIndices@const Vector4&$SYMBOL_NO_DESC
    2$rage::FastLightList::GetIndices8@const Vector4&@Vector4&@Vector4&$SYMBOL_NO_DESC
    2$rage::FastLightList::Resize@int$SYMBOL_NO_DESC
    2$rage::FastSpot4Lighting$SYMBOL_NO_DESC
    2$rage::FastSpot4Lighting::CreateVariableCache$SYMBOL_NO_DESC
    2$rage::FastSpot4Lighting::FastSpot4Lighting$SYMBOL_NO_DESC
    2$rage::FastSpot4Lighting::MaxFastLights$SYMBOL_NO_DESC
    2$rage::FastSpot4Lighting::Reset$SYMBOL_NO_DESC
    2$rage::FastSpot4Lighting::SetShadows@SHADOWCACHE&@atArray<Light>&$SYMBOL_NO_DESC
    2$rage::FastSpot4Lighting::SetupShader@grmShader&@VarCache*$SYMBOL_NO_DESC
    2$rage::FastSpot4Lighting::SetupShaderPass$SYMBOL_NO_DESC
    2$rage::FastSpot4Lighting::UsesFragment@grmShader&$SYMBOL_NO_DESC
    2$rage::GetBoundBoxPosition@const atArray<Light>&@Vector3&@Vector3&$SYMBOL_NO_DESC
    2$rage::GetBoundSpherePosition@const atArray<Light>&$SYMBOL_NO_DESC
    2$rage::GetLightPositionBoundsScaleDown@const atArray<Light>&@Vector3&@Vector3&$SYMBOL_NO_DESC
    2$rage::GetLightPositionBoundsScaleUp@const atArray<Light>&@Vector3&@Vector3&$SYMBOL_NO_DESC
    2$rage::Grid::~Grid$SYMBOL_NO_DESC
    2$rage::Grid::CalculateLightGrid@int$SYMBOL_NO_DESC
    2$rage::Grid::CalculateThreadFunc@sysTaskParameters &$SYMBOL_NO_DESC
    2$rage::Grid::ClearHeights$SYMBOL_NO_DESC
    2$rage::Grid::CopyHeightsToTexture@grcTexture*@const$SYMBOL_NO_DESC
    2$rage::Grid::CopyResults2ToTexture@grcTexture*@const$SYMBOL_NO_DESC
    2$rage::Grid::CopyResultsToTexture@grcTexture*@const$SYMBOL_NO_DESC
    2$rage::Grid::GetCell2V@int@int@int$SYMBOL_NO_DESC
    2$rage::Grid::GetCellV@int@int@int$SYMBOL_NO_DESC
    2$rage::Grid::GetLevels$SYMBOL_NO_DESC
    2$rage::Grid::SetExtents@const Vector3&@const Vector3&$SYMBOL_NO_DESC
    2$rage::Grid::SetFastLights@const FastLightList<NumLights>&$SYMBOL_NO_DESC
    2$rage::Grid::SizeInBytes@const$SYMBOL_NO_DESC
    2$rage::Grid::UseTwoTextures@const$SYMBOL_NO_DESC
    2$rage::Grid::Width$SYMBOL_NO_DESC
    2$rage::KeyIndexPair$SYMBOL_NO_DESC
    2$rage::Light::~Light$SYMBOL_NO_DESC
    2$rage::Light::CalculateConeCosineAngle$SYMBOL_NO_DESC
    2$rage::Light::CalculateSpotScaleBias@float&@float&@const$SYMBOL_NO_DESC
    2$rage::Light::CalculateTanAngle@const$SYMBOL_NO_DESC
    2$rage::Light::CastLight$SYMBOL_NO_DESC
    2$rage::Light::CastShadow$SYMBOL_NO_DESC
    2$rage::Light::CastsLight@const$SYMBOL_NO_DESC
    2$rage::Light::CastsShadow@const$SYMBOL_NO_DESC
    2$rage::Light::CastsVolume@const$SYMBOL_NO_DESC
    2$rage::Light::Colour$SYMBOL_NO_DESC
    2$rage::Light::ConeAngle$SYMBOL_NO_DESC
    2$rage::Light::DirectionVector$SYMBOL_NO_DESC
    2$rage::Light::DropOff$SYMBOL_NO_DESC
    2$rage::Light::FarAttenEnd$SYMBOL_NO_DESC
    2$rage::Light::FarAttenStart$SYMBOL_NO_DESC
    2$rage::Light::GetAngleRange@const$SYMBOL_NO_DESC
    2$rage::Light::GetCullSphere$SYMBOL_NO_DESC
    2$rage::Light::GetDirection@const$SYMBOL_NO_DESC
    2$rage::Light::GetLightFogVolume$SYMBOL_NO_DESC
    2$rage::Light::GetLightIntensityOnPoint@const Vector3&@const$SYMBOL_NO_DESC
    2$rage::Light::GetMax$SYMBOL_NO_DESC
    2$rage::Light::GetMaxStyleAttenuation@const$SYMBOL_NO_DESC
    2$rage::Light::GetMin$SYMBOL_NO_DESC
    2$rage::Light::GetPosition@const$SYMBOL_NO_DESC
    2$rage::Light::GetRange@Vector3&@Vector3&@Vector3&@float&@const$SYMBOL_NO_DESC
    2$rage::Light::GetShadowBias@const$SYMBOL_NO_DESC
    2$rage::Light::GetShadowEplision@const$SYMBOL_NO_DESC
    2$rage::Light::GetShadowFilterSize@const$SYMBOL_NO_DESC
    2$rage::Light::GetShadowSize@const$SYMBOL_NO_DESC
    2$rage::Light::GetSphere$SYMBOL_NO_DESC
    2$rage::Light::InBoundXZ@const Vector3&@const Vector3&@const$SYMBOL_NO_DESC
    2$rage::Light::Intensity$SYMBOL_NO_DESC
    2$rage::Light::IsClose@const Light&@float@float@const$SYMBOL_NO_DESC
    2$rage::Light::IsDirty$SYMBOL_NO_DESC
    2$rage::Light::Light$SYMBOL_NO_DESC
    2$rage::Light::LightPoint@const Vector3&@const$SYMBOL_NO_DESC
    2$rage::Light::LightType$SYMBOL_NO_DESC
    2$rage::Light::LightType::DIRECTIONAL$SYMBOL_NO_DESC
    2$rage::Light::LightType::POINT$SYMBOL_NO_DESC
    2$rage::Light::LightType::SPOT$SYMBOL_NO_DESC
    2$rage::Light::LightVolume$SYMBOL_NO_DESC
    2$rage::Light::m_boundMax$SYMBOL_NO_DESC
    2$rage::Light::m_boundMin$SYMBOL_NO_DESC
    2$rage::Light::m_boundSphere$SYMBOL_NO_DESC
    2$rage::Light::m_invCosConeAngle$SYMBOL_NO_DESC
    2$rage::Light::m_isDirty$SYMBOL_NO_DESC
    2$rage::Light::MarkDirty$SYMBOL_NO_DESC
    2$rage::Light::Merge@const Light&@int$SYMBOL_NO_DESC
    2$rage::Light::name$SYMBOL_NO_DESC
    2$rage::Light::NearAttenEnd$SYMBOL_NO_DESC
    2$rage::Light::Penumbra$SYMBOL_NO_DESC
    2$rage::Light::Reset$SYMBOL_NO_DESC
    2$rage::Light::ResetDirty$SYMBOL_NO_DESC
    2$rage::Light::SetSphereOfInfluence@float$SYMBOL_NO_DESC
    2$rage::Light::ShadowBias$SYMBOL_NO_DESC
    2$rage::Light::ShadowEplision$SYMBOL_NO_DESC
    2$rage::Light::ShadowFilter$SYMBOL_NO_DESC
    2$rage::Light::ShadowSize$SYMBOL_NO_DESC
    2$rage::Light::Type$SYMBOL_NO_DESC
    2$rage::Light::Update@int$SYMBOL_NO_DESC
    2$rage::LIGHT_GRID_MAX_LIGHTS$SYMBOL_NO_DESC
    2$rage::LIGHT_GRID_NUM_LEVELS$SYMBOL_NO_DESC
    2$rage::LIGHT_GRID_USE_EIGHT$SYMBOL_NO_DESC
    2$rage::LIGHT_GRID_WIDTH$SYMBOL_NO_DESC
    2$rage::LightCalculateIncidentLighting@const atArray<Light>&@int*@int@const Vector3&$SYMBOL_NO_DESC
    2$rage::LightGetStrongestNLightsInBox@const atArray<Light>&@int*@const Vector3&@int@const Vector3&@const Vector3&$SYMBOL_NO_DESC
    2$rage::LightGridGlobalVarCache$SYMBOL_NO_DESC
    2$rage::LightGridGlobalVarCache::CacheEffectVars$SYMBOL_NO_DESC
    2$rage::LightGridGlobalVarCache::LightGridGlobalVarCache$SYMBOL_NO_DESC
    2$rage::LightGridGlobalVarCache::m_cellDivisionsTextureID$SYMBOL_NO_DESC
    2$rage::LightGridGlobalVarCache::m_set$SYMBOL_NO_DESC
    2$rage::LightGridGlobalVarCache::m_sortBiasID$SYMBOL_NO_DESC
    2$rage::LightGridGlobalVarCache::m_sortScaleID$SYMBOL_NO_DESC
    2$rage::LightGridGlobalVarCache::m_sortTextureID$SYMBOL_NO_DESC
    2$rage::LightGridGlobalVarCache::SetGlobals@const grcTexture*@const grcTexture*@const Vector3&@const Vector3&$SYMBOL_NO_DESC
    2$rage::LightGridMemory::()@int@int@int$SYMBOL_NO_DESC
    2$rage::LightGridMemory::CalculateLightGrid@int$SYMBOL_NO_DESC
    2$rage::LightGridMemory::Clear$SYMBOL_NO_DESC
    2$rage::LightGridMemory::ClearHeights$SYMBOL_NO_DESC
    2$rage::LightGridMemory::CopyHeightsToTexture@grcTexture*@const$SYMBOL_NO_DESC
    2$rage::LightGridMemory::CopyMem@u32*@const$SYMBOL_NO_DESC
    2$rage::LightGridMemory::CopyResults2ToTexture@grcTexture*@const$SYMBOL_NO_DESC
    2$rage::LightGridMemory::CopyResultsToTexture@grcTexture*@const$SYMBOL_NO_DESC
    2$rage::LightGridMemory::CopyToSubGrid@int@int@int@u32@u32$SYMBOL_NO_DESC
    2$rage::LightGridMemory::GetCell2V@int@int@int$SYMBOL_NO_DESC
    2$rage::LightGridMemory::GetCellV@int@int@int$SYMBOL_NO_DESC
    2$rage::LightGridMemory::GetLevels$SYMBOL_NO_DESC
    2$rage::LightGridMemory::GetMem$SYMBOL_NO_DESC
    2$rage::LightGridMemory::LightGridMemory$SYMBOL_NO_DESC
    2$rage::LightGridMemory::SetSortedLightGrids@int@int@int@FastLightList<Size>&@const Vector3&@Vector3&$SYMBOL_NO_DESC
    2$rage::LightGridMemory::SizeInBytes@const$SYMBOL_NO_DESC
    2$rage::LightGridMemory::UseTwoTextures@const$SYMBOL_NO_DESC
    2$rage::LightGridMemory::Width$SYMBOL_NO_DESC
    2$rage::LightingComposite$SYMBOL_NO_DESC
    2$rage::LightingComposite::AddWidgets@bkBank*$SYMBOL_NO_DESC
    2$rage::LightingComposite::CreateVariableCache$SYMBOL_NO_DESC
    2$rage::LightingComposite::LightingComposite$SYMBOL_NO_DESC
    2$rage::LightingComposite::SetGlobals$SYMBOL_NO_DESC
    2$rage::LightingComposite::SetupShader@grmShader&@VarCache*$SYMBOL_NO_DESC
    2$rage::LightingComposite::SetupShaderPass$SYMBOL_NO_DESC
    2$rage::LightList::[]@int$SYMBOL_NO_DESC
    2$rage::LightList::~LightList$SYMBOL_NO_DESC
    2$rage::LightList::AllLights$SYMBOL_NO_DESC
    2$rage::LightList::Clear$SYMBOL_NO_DESC
    2$rage::LightList::Cull@grcViewport*$SYMBOL_NO_DESC
    2$rage::LightList::Draw@bool@bool@bool$SYMBOL_NO_DESC
    2$rage::LightList::Empty$SYMBOL_NO_DESC
    2$rage::LightList::fillLights$SYMBOL_NO_DESC
    2$rage::LightList::GetCamera$SYMBOL_NO_DESC
    2$rage::LightList::GetDirectional@Vector3&@Vector3&$SYMBOL_NO_DESC
    2$rage::LightList::GetStrongestNLights@int*@const Vector3&@int@const$SYMBOL_NO_DESC
    2$rage::LightList::GetVersion@const$SYMBOL_NO_DESC
    2$rage::LightList::LightList$SYMBOL_NO_DESC
    2$rage::LightList::lights$SYMBOL_NO_DESC
    2$rage::LightList::Pad$SYMBOL_NO_DESC
    2$rage::LightList::Scale@float@float$SYMBOL_NO_DESC
    2$rage::LightList::Update$SYMBOL_NO_DESC
    2$rage::ManyShadows$SYMBOL_NO_DESC
    2$rage::ManyShadows::CreateVariableCache$SYMBOL_NO_DESC
    2$rage::ManyShadows::ManyShadows$SYMBOL_NO_DESC
    2$rage::ManyShadows::SetGlobals$SYMBOL_NO_DESC
    2$rage::ManyShadows::SetShadows@LIGHTSLIST&@SHADOWCACHE&@float@float$SYMBOL_NO_DESC
    2$rage::ManyShadows::SetupShader@grmShader&@VarCache*$SYMBOL_NO_DESC
    2$rage::ManyShadows::SetupShaderPass$SYMBOL_NO_DESC
    2$rage::MAX_LIGHTS$SYMBOL_NO_DESC
    2$rage::NearestMap::~NearestMap$SYMBOL_NO_DESC
    2$rage::NearestMap::Create@const Vector3&@const Vector3&$SYMBOL_NO_DESC
    2$rage::NearestMap::Create@CShadowCache& UNUSED_PARAMshadows@const atArray<Light>&@const Vector3&@const Vector3&@bool UNUSED_PARAM hasShadows$SYMBOL_NO_DESC
    2$rage::NearestMap::GetColors$SYMBOL_NO_DESC
    2$rage::NearestMap::GetColors2$SYMBOL_NO_DESC
    2$rage::NearestMap::GetGrid$SYMBOL_NO_DESC
    2$rage::NearestMap::GetGrid2$SYMBOL_NO_DESC
    2$rage::NearestMap::GetHeights@const$SYMBOL_NO_DESC
    2$rage::NearestMap::GetPosition$SYMBOL_NO_DESC
    2$rage::NearestMap::GetPosition2$SYMBOL_NO_DESC
    2$rage::NearestMap::Init$SYMBOL_NO_DESC
    2$rage::NearestMap::NearestMap@int@int@bool@bool$SYMBOL_NO_DESC
    2$rage::NearestMap::SetFastLights@const FastLightList<256>&$SYMBOL_NO_DESC
    2$rage::NearestMap::SetUpLights@const atArray<Light>&$SYMBOL_NO_DESC
    2$rage::PackedLight::colorSpotBias$SYMBOL_NO_DESC
    2$rage::PackedLight::directionSpotScale$SYMBOL_NO_DESC
    2$rage::PackedLight::PackedLight$SYMBOL_NO_DESC
    2$rage::PackedLight::PackedLight@const Vector3&@const Vector3&@const Vector3&@float@const Vector3&@const Vector3&@float@float$SYMBOL_NO_DESC
    2$rage::PackedLight::posIntensity$SYMBOL_NO_DESC
    2$rage::SpotLightFogRenderer::CullSortRender@T&@float@float$SYMBOL_NO_DESC
    2$rage::SpotLightFogRenderer::HasVolume@Light&@float&$SYMBOL_NO_DESC
    2$rage::SpotLightFogRenderer::render$SYMBOL_NO_DESC
    2$rage::SpotLightFogRenderer::Render@Light&@float@float$SYMBOL_NO_DESC
    2$rage::SpotLightFogRenderer::Set@Light&@float@float$SYMBOL_NO_DESC
    2$rage::SpotLightFogRenderer::SetClipIntensity@float$SYMBOL_NO_DESC
    2$rage::SpotLightFogRenderer::SetMaxClip@float$SYMBOL_NO_DESC
    2$rage::SpotLightFogRenderer::SetMinClip@float$SYMBOL_NO_DESC
    2$rage::SpotLightFogRenderer::SetTechnique@const char*$SYMBOL_NO_DESC
    2$rage::SpotLightFogRenderer::SortLightStruct$SYMBOL_NO_DESC
    2$rage::SpotLightFogRenderer::SpotLightFogRenderer$SYMBOL_NO_DESC
    2$rage::TextureLightStream$SYMBOL_NO_DESC
    2$rage::TextureLightStream::~TextureLightStream$SYMBOL_NO_DESC
    2$rage::TextureLightStream::Create@Grid*@bool@bool$SYMBOL_NO_DESC
    2$rage::TextureLightStream::GetColors@const$SYMBOL_NO_DESC
    2$rage::TextureLightStream::GetPosition@const$SYMBOL_NO_DESC
    2$rage::TextureLightStream::Init$SYMBOL_NO_DESC
    2$rage::TextureLightStream::SetGrid@int@int@grcImage*@grcImage*@const PackedLight&@const PackedLight&@const PackedLight&@const PackedLight&$SYMBOL_NO_DESC
    2$rage::TextureLightStream::SetGrid@int@int@grcImage*@grcImage*@const Vector3&@const Vector3&@const Light&@const Light&@const Light&@const Light&@u32$SYMBOL_NO_DESC
    2$rage::TextureLightStream::SetUpLights@const atArray<Light>&$SYMBOL_NO_DESC
    2$rage::TextureLightStream::TextureLightStream@int@int$SYMBOL_NO_DESC
    2$rage::UnitTestLightGridMemory$SYMBOL_NO_DESC
    2$rage::USE_COMPRESSED_DIRECTION$SYMBOL_NO_DESC
    2$rage::Volume::FogDensity$SYMBOL_NO_DESC
    2$rage::Volume::FogEnd$SYMBOL_NO_DESC
    2$rage::Volume::FogStart$SYMBOL_NO_DESC
    2$rage::Volume::IsOn@const$SYMBOL_NO_DESC
    2$rage::Volume::Transparency$SYMBOL_NO_DESC
    2$rage::Volume::Volume$SYMBOL_NO_DESC
3$TEST_QA_ITEM_BEGIN$SYMBOL_NO_DESC
3$TEST_QA_ITEM_END$SYMBOL_NO_DESC
3$USE_THREADING$SYMBOL_NO_DESC
