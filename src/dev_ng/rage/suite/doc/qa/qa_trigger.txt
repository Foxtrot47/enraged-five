3$@@Overview$DTX_SECTION_NOT_FILLED_IN
3$@@Components$DTX_SECTION_NOT_FILLED_IN
3$Command$POSSIBLE_STRAY_TOPIC
3$Working$POSSIBLE_STRAY_TOPIC
1$rage$FUNC_PARAM_NO_DESC
    2$rage::trgAction::Activated@trgActionState &$FUNC_PARAM_NO_DESC
    2$rage::trgAction::Activated1@aGuid@atDList<trgActionState> &$FUNC_PARAM_NO_DESC
    2$rage::trgAction::ActivatedWithGuid@aGuid*$FUNC_PARAM_NO_DESC
    2$rage::trgAction::Active@trgActionState &$FUNC_PARAM_NO_DESC
    2$rage::trgAction::CountResources@aActorResources &@const$FUNC_PARAM_NO_DESC
    2$rage::trgAction::Deactivated@trgActionState &$FUNC_PARAM_NO_DESC
    2$rage::trgAction::DeactivatedWithGuid@aGuid*$FUNC_PARAM_NO_DESC
    2$rage::trgActorSet::GetNumActors@void@const$FUNC_PARAM_NO_DESC
    2$rage::trgActorSet::IsInList@aGuid@int@const$FUNC_PARAM_NO_DESC
    2$rage::trgCondition::CountResources@aActorResources &@const$FUNC_PARAM_NO_DESC
    2$rage::trgFlags::trgFlags@int$FUNC_PARAM_NO_DESC
    2$rage::trgFlagsBase::Create@const char *$FUNC_PARAM_NO_DESC
    2$rage::trgFlagsBool::trgFlagsBool@int$FUNC_PARAM_NO_DESC
    2$rage::trgFlagsFloat::trgFlagsFloat@int$FUNC_PARAM_NO_DESC
    2$rage::trgFlagsInt::trgFlagsInt@int$FUNC_PARAM_NO_DESC
    2$rage::trgInstantSingleActorAction::trgInstantSingleActorAction@datResource &$FUNC_PARAM_NO_DESC
    2$rage::trgMessageAction::trgMessageAction@void$FUNC_PARAM_NO_DESC
    2$rage::trgTriggerGroup::GetLayoutMissionIndex@void@const$FUNC_PARAM_NO_DESC
    2$rage::trgTriggerGroup::GetLayoutName@void@const$FUNC_PARAM_NO_DESC
    2$rage::trgTriggerGroup::GetNamespace@void@const$FUNC_PARAM_NO_DESC
    2$rage::trgTriggerManager::SetMessage@const char *@float$FUNC_PARAM_NO_DESC
1$rage$FUNC_PARAM_NO_NAME
    2$rage::trgAction::Activated@trgActionState &$FUNC_PARAM_NO_NAME
    2$rage::trgAction::Activated1@aGuid@atDList<trgActionState> &$FUNC_PARAM_NO_NAME
    2$rage::trgAction::ActivatedWithGuid@aGuid*$FUNC_PARAM_NO_NAME
    2$rage::trgAction::Active@trgActionState &$FUNC_PARAM_NO_NAME
    2$rage::trgAction::CountResources@aActorResources &@const$FUNC_PARAM_NO_NAME
    2$rage::trgAction::Deactivated@trgActionState &$FUNC_PARAM_NO_NAME
    2$rage::trgAction::DeactivatedWithGuid@aGuid*$FUNC_PARAM_NO_NAME
    2$rage::trgActorSet::GetNumActors@void@const$FUNC_PARAM_NO_NAME
    2$rage::trgActorSet::IsInList@aGuid@int@const$FUNC_PARAM_NO_NAME
    2$rage::trgCondition::CountResources@aActorResources &@const$FUNC_PARAM_NO_NAME
    2$rage::trgFlags::trgFlags@int$FUNC_PARAM_NO_NAME
    2$rage::trgFlagsBase::Create@const char *$FUNC_PARAM_NO_NAME
    2$rage::trgFlagsBool::trgFlagsBool@int$FUNC_PARAM_NO_NAME
    2$rage::trgFlagsFloat::trgFlagsFloat@int$FUNC_PARAM_NO_NAME
    2$rage::trgFlagsInt::trgFlagsInt@int$FUNC_PARAM_NO_NAME
    2$rage::trgInstantSingleActorAction::trgInstantSingleActorAction@datResource &$FUNC_PARAM_NO_NAME
    2$rage::trgMessageAction::trgMessageAction@void$FUNC_PARAM_NO_NAME
    2$rage::trgTriggerGroup::GetLayoutMissionIndex@void@const$FUNC_PARAM_NO_NAME
    2$rage::trgTriggerGroup::GetLayoutName@void@const$FUNC_PARAM_NO_NAME
    2$rage::trgTriggerGroup::GetNamespace@void@const$FUNC_PARAM_NO_NAME
    2$rage::trgTriggerManager::SetMessage@const char *@float$FUNC_PARAM_NO_NAME
1$rage$FUNC_RETURN_VAL_NO_DESC
    2$rage::trgAction::Activated@trgActionState &$FUNC_RETURN_VAL_NO_DESC
    2$rage::trgAction::ActivatedWithGuid@aGuid*$FUNC_RETURN_VAL_NO_DESC
    2$rage::trgAction::Active@trgActionState &$FUNC_RETURN_VAL_NO_DESC
    2$rage::trgAction::CountResources@aActorResources &@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::trgAction::Deactivated@trgActionState &$FUNC_RETURN_VAL_NO_DESC
    2$rage::trgAction::DeactivatedWithGuid@aGuid*$FUNC_RETURN_VAL_NO_DESC
    2$rage::trgActorSet::GetNumActors@void@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::trgActorSet::IsInList@aGuid@int@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::trgCondition::CountResources@aActorResources &@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::trgFlagsBase::Create@const char *$FUNC_RETURN_VAL_NO_DESC
    2$rage::trgTriggerGroup::GetLayoutMissionIndex@void@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::trgTriggerGroup::GetLayoutName@void@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::trgTriggerGroup::GetNamespace@void@const$FUNC_RETURN_VAL_NO_DESC
1$rage$SYMBOL_NO_DESC
    2$rage::DECLARE_PLACE_TRIGGER_CONDITION$SYMBOL_NO_DESC
    2$rage::IMPLEMENT_PLACE_TRIGGER_CONDITION$SYMBOL_NO_DESC
    2$rage::trgAction::~trgAction$SYMBOL_NO_DESC
    2$rage::trgAction::ChangeActivation@aGuid@atDList<trgActionState> &@bool$SYMBOL_NO_DESC
    2$rage::trgAction::ClearPagingGroup@void$SYMBOL_NO_DESC
    2$rage::trgAction::Deactivated1@aGuid@atDList<trgActionState> &$SYMBOL_NO_DESC
    2$rage::trgAction::DebugDraw$SYMBOL_NO_DESC
    2$rage::trgAction::DECLARE_IDMGR_TYPEID@trgAction$SYMBOL_NO_DESC
    2$rage::trgAction::DECLARE_PLACE@trgAction$SYMBOL_NO_DESC
    2$rage::trgAction::GetLockedNamespace@void@const$SYMBOL_NO_DESC
    2$rage::trgAction::GetRemoteExecute@const$SYMBOL_NO_DESC
    2$rage::trgAction::Init@void$SYMBOL_NO_DESC
    2$rage::trgAction::InitClass@void$SYMBOL_NO_DESC
    2$rage::trgAction::PagedIn@void$SYMBOL_NO_DESC
    2$rage::trgAction::PagedOut@void$SYMBOL_NO_DESC
    2$rage::trgAction::Reset@void$SYMBOL_NO_DESC
    2$rage::trgAction::SetGroup@trgTriggerGroup &$SYMBOL_NO_DESC
    2$rage::trgAction::SetLockedNamespace@bool$SYMBOL_NO_DESC
    2$rage::trgAction::SetName@const char *$SYMBOL_NO_DESC
    2$rage::trgAction::SetPagingGroup@trgTriggerGroup &$SYMBOL_NO_DESC
    2$rage::trgAction::Shutdown@void$SYMBOL_NO_DESC
    2$rage::trgAction::trgAction@bool@int$SYMBOL_NO_DESC
    2$rage::trgAction::trgAction@datResource &$SYMBOL_NO_DESC
    2$rage::trgActionData::CreateFunc$SYMBOL_NO_DESC
    2$rage::trgActionData::ID$SYMBOL_NO_DESC
    2$rage::trgActionData::PlaceFunc$SYMBOL_NO_DESC
    2$rage::trgActionData::Template$SYMBOL_NO_DESC
    2$rage::trgActionData::trgActionData$SYMBOL_NO_DESC
    2$rage::trgActions::=@const trgActions &$SYMBOL_NO_DESC
    2$rage::trgActions::Add@aGuid@atDList<trgActionState> &@trgTriggerGroup &@const$SYMBOL_NO_DESC
    2$rage::trgActions::AddAction@trgTriggerIndex$SYMBOL_NO_DESC
    2$rage::trgActions::IsEmpty@void@const$SYMBOL_NO_DESC
    2$rage::trgActions::Remove@aGuid@atDList<trgActionState> &@trgTriggerGroup &@const$SYMBOL_NO_DESC
    2$rage::trgActions::trgActions@const trgActions &$SYMBOL_NO_DESC
    2$rage::trgActions::trgActions@datResource &$SYMBOL_NO_DESC
    2$rage::trgActions::trgActions@void$SYMBOL_NO_DESC
    2$rage::trgActionState::Action$SYMBOL_NO_DESC
    2$rage::trgActionState::Actor$SYMBOL_NO_DESC
    2$rage::trgActionState::union@1::UserData$SYMBOL_NO_DESC
    2$rage::trgActionState::union@1::UserPointer$SYMBOL_NO_DESC
    2$rage::trgActorData::==@const trgActorData &@const$SYMBOL_NO_DESC
    2$rage::trgActorData::Guid$SYMBOL_NO_DESC
    2$rage::trgActorSet::~trgActorSet$SYMBOL_NO_DESC
    2$rage::trgActorSet::=@const trgActorSet &$SYMBOL_NO_DESC
    2$rage::trgActorSet::Add@aGuid$SYMBOL_NO_DESC
    2$rage::trgActorSet::AddActions@const trgActions &@atDList<trgActionState> &@trgTriggerGroup &@const$SYMBOL_NO_DESC
    2$rage::trgActorSet::And@const trgActorSet &$SYMBOL_NO_DESC
    2$rage::trgActorSet::Clear@void$SYMBOL_NO_DESC
    2$rage::trgActorSet::Difference@const trgActorSet &@const trgActorSet &$SYMBOL_NO_DESC
    2$rage::trgActorSet::GetList@void@const$SYMBOL_NO_DESC
    2$rage::trgActorSet::IsEmpty@void@const$SYMBOL_NO_DESC
    2$rage::trgActorSet::Or@const trgActorSet &$SYMBOL_NO_DESC
    2$rage::trgActorSet::RemoveActions@const trgActions &@atDList<trgActionState> &@trgTriggerGroup &@const$SYMBOL_NO_DESC
    2$rage::trgActorSet::trgActorSet@const trgActorSet &$SYMBOL_NO_DESC
    2$rage::trgActorSet::trgActorSet@void$SYMBOL_NO_DESC
    2$rage::trgCaseInsensitiveString$SYMBOL_NO_DESC
    2$rage::trgCheckValueBaseCondition$SYMBOL_NO_DESC
    2$rage::trgCheckValueBaseCondition::DECLARE_IDMGR_TYPEID@trgCheckValueBaseCondition$SYMBOL_NO_DESC
    2$rage::trgCheckValueBaseCondition::DoCompare@float$SYMBOL_NO_DESC
    2$rage::trgCheckValueBaseCondition::InitClass@void$SYMBOL_NO_DESC
    2$rage::trgCheckValueBaseCondition::trgCheckValueBaseCondition$SYMBOL_NO_DESC
    2$rage::trgCheckValueBaseCondition::trgCheckValueBaseCondition@datResource &$SYMBOL_NO_DESC
    2$rage::trgCheckValueBaseCondition::Update$SYMBOL_NO_DESC
    2$rage::trgCompareTypes::enum@1::kEqual$SYMBOL_NO_DESC
    2$rage::trgCompareTypes::enum@1::kGreater$SYMBOL_NO_DESC
    2$rage::trgCompareTypes::enum@1::kGreaterOrEqual$SYMBOL_NO_DESC
    2$rage::trgCompareTypes::enum@1::kLess$SYMBOL_NO_DESC
    2$rage::trgCompareTypes::enum@1::kLessOrEqual$SYMBOL_NO_DESC
    2$rage::trgCondition::~trgCondition$SYMBOL_NO_DESC
    2$rage::trgCondition::AddWidgets@bkBank &@const aDataStruct *@bool$SYMBOL_NO_DESC
    2$rage::trgCondition::ClearPagingGroup@void$SYMBOL_NO_DESC
    2$rage::trgCondition::DebugDraw@void$SYMBOL_NO_DESC
    2$rage::trgCondition::DECLARE_IDMGR_TYPEID@trgCondition$SYMBOL_NO_DESC
    2$rage::trgCondition::DECLARE_PLACE@trgCondition$SYMBOL_NO_DESC
    2$rage::trgCondition::DrawBound@void$SYMBOL_NO_DESC
    2$rage::trgCondition::DrawCulls@void$SYMBOL_NO_DESC
    2$rage::trgCondition::GetEnabled@void@const$SYMBOL_NO_DESC
    2$rage::trgCondition::GetLockedNamespace@void@const$SYMBOL_NO_DESC
    2$rage::trgCondition::GetName@const$SYMBOL_NO_DESC
    2$rage::trgCondition::GetSignal@void@const$SYMBOL_NO_DESC
    2$rage::trgCondition::Init@void$SYMBOL_NO_DESC
    2$rage::trgCondition::InitClass@void$SYMBOL_NO_DESC
    2$rage::trgCondition::InitOrder@int &$SYMBOL_NO_DESC
    2$rage::trgCondition::ManagerUpdate@void$SYMBOL_NO_DESC
    2$rage::trgCondition::PagedIn@void$SYMBOL_NO_DESC
    2$rage::trgCondition::PagedOut@void$SYMBOL_NO_DESC
    2$rage::trgCondition::Reset@void$SYMBOL_NO_DESC
    2$rage::trgCondition::ResetOrder@void$SYMBOL_NO_DESC
    2$rage::trgCondition::SetEnabled@bool$SYMBOL_NO_DESC
    2$rage::trgCondition::SetGroup@trgTriggerGroup &$SYMBOL_NO_DESC
    2$rage::trgCondition::SetLockedNamespace@bool$SYMBOL_NO_DESC
    2$rage::trgCondition::SetName@const char *$SYMBOL_NO_DESC
    2$rage::trgCondition::SetPagingGroup@trgTriggerGroup &$SYMBOL_NO_DESC
    2$rage::trgCondition::Shutdown@void$SYMBOL_NO_DESC
    2$rage::trgCondition::Sleep@float$SYMBOL_NO_DESC
    2$rage::trgCondition::trgCondition@datResource &$SYMBOL_NO_DESC
    2$rage::trgCondition::trgCondition@void$SYMBOL_NO_DESC
    2$rage::trgCondition::Update@void$SYMBOL_NO_DESC
    2$rage::trgCondition::UpdateContinuous@void$SYMBOL_NO_DESC
    2$rage::trgCondition::WokeUp@void$SYMBOL_NO_DESC
    2$rage::trgConditionData::CreateFunc$SYMBOL_NO_DESC
    2$rage::trgConditionData::ID$SYMBOL_NO_DESC
    2$rage::trgConditionData::PlaceFunc$SYMBOL_NO_DESC
    2$rage::trgConditionData::Template$SYMBOL_NO_DESC
    2$rage::trgConditionData::trgConditionData$SYMBOL_NO_DESC
    2$rage::trgCounterActions::enum@1::kAdd$SYMBOL_NO_DESC
    2$rage::trgCounterActions::enum@1::kDiv$SYMBOL_NO_DESC
    2$rage::trgCounterActions::enum@1::kMul$SYMBOL_NO_DESC
    2$rage::trgCounterActions::enum@1::kSet$SYMBOL_NO_DESC
    2$rage::trgCounterActions::enum@1::kSetRandom$SYMBOL_NO_DESC
    2$rage::trgCounterActions::enum@1::kSub$SYMBOL_NO_DESC
    2$rage::trgCounterHandle$SYMBOL_NO_DESC
    2$rage::trgCounterHandle::GetCounter@bool &@const$SYMBOL_NO_DESC
    2$rage::trgCounterHandle::m_CounterIndex$SYMBOL_NO_DESC
    2$rage::trgCounterHandle::PagedIn@const char *@int@trgTriggerGroup &$SYMBOL_NO_DESC
    2$rage::trgCounterHandle::SetCounter@float@const$SYMBOL_NO_DESC
    2$rage::trgCounterHandle::trgCounterHandle$SYMBOL_NO_DESC
    2$rage::trgFlagContext$SYMBOL_NO_DESC
    2$rage::trgFlagContext::~trgFlagContext$SYMBOL_NO_DESC
    2$rage::trgFlagContext::AddWidgets@bkBank &$SYMBOL_NO_DESC
    2$rage::trgFlagContext::GetCounters$SYMBOL_NO_DESC
    2$rage::trgFlagContext::GetFlags$SYMBOL_NO_DESC
    2$rage::trgFlagContext::GetInts$SYMBOL_NO_DESC
    2$rage::trgFlagContext::Reset$SYMBOL_NO_DESC
    2$rage::trgFlagContext::trgFlagContext@int@int@int$SYMBOL_NO_DESC
    2$rage::trgFlagContextInfo$SYMBOL_NO_DESC
    2$rage::trgFlagContextInfo::PagedIn@const char *@int@trgTriggerGroup &$SYMBOL_NO_DESC
    2$rage::trgFlagContextInfo::trgFlagContextInfo$SYMBOL_NO_DESC
    2$rage::trgFlagContextManager$SYMBOL_NO_DESC
    2$rage::trgFlagContextManager::~trgFlagContextManager$SYMBOL_NO_DESC
    2$rage::trgFlagContextManager::GetCurrent$SYMBOL_NO_DESC
    2$rage::trgFlagContextManager::GetFlagContext@int$SYMBOL_NO_DESC
    2$rage::trgFlagContextManager::GetFlagContextGlobal$SYMBOL_NO_DESC
    2$rage::trgFlagContextManager::SetCurrent@trgFlagContextManager*$SYMBOL_NO_DESC
    2$rage::trgFlagContextManager::ShutdownClass$SYMBOL_NO_DESC
    2$rage::trgFlagFuncs$SYMBOL_NO_DESC
    2$rage::trgFlagFuncs::CompareCounters@float@float@int$SYMBOL_NO_DESC
    2$rage::trgFlagFuncs::CompareInts@int@int@int$SYMBOL_NO_DESC
    2$rage::trgFlagFuncs::ModifyValue@float@float@int$SYMBOL_NO_DESC
    2$rage::trgFlagFuncs::ModifyValue@int@int@int$SYMBOL_NO_DESC
    2$rage::trgFlagHandle$SYMBOL_NO_DESC
    2$rage::trgFlagHandle::GetFlag@bool &@const$SYMBOL_NO_DESC
    2$rage::trgFlagHandle::m_FlagIndex$SYMBOL_NO_DESC
    2$rage::trgFlagHandle::PagedIn@const char *@int@trgTriggerGroup &$SYMBOL_NO_DESC
    2$rage::trgFlagHandle::SetFlag@bool@const$SYMBOL_NO_DESC
    2$rage::trgFlagHandle::trgFlagHandle$SYMBOL_NO_DESC
    2$rage::trgFlags$SYMBOL_NO_DESC
    2$rage::trgFlags::~trgFlags$SYMBOL_NO_DESC
    2$rage::trgFlags::Get@int@const$SYMBOL_NO_DESC
    2$rage::trgFlags::Reset@const D&$SYMBOL_NO_DESC
    2$rage::trgFlags::Set@int@const D&$SYMBOL_NO_DESC
    2$rage::trgFlagsBase$SYMBOL_NO_DESC
    2$rage::trgFlagsBase::~trgFlagsBase$SYMBOL_NO_DESC
    2$rage::trgFlagsBase::AddWidget@bkBank &@const char *@int$SYMBOL_NO_DESC
    2$rage::trgFlagsBase::AddWidgets@bkBank &$SYMBOL_NO_DESC
    2$rage::trgFlagsBase::enum@1::kMaxNameLength$SYMBOL_NO_DESC
    2$rage::trgFlagsBase::GetIndex@const char *@const$SYMBOL_NO_DESC
    2$rage::trgFlagsBase::GetMaxFlags@const$SYMBOL_NO_DESC
    2$rage::trgFlagsBase::trgFlagsBase@int$SYMBOL_NO_DESC
    2$rage::trgFlagsBool$SYMBOL_NO_DESC
    2$rage::trgFlagsBool::AddWidget@bkBank &@const char *@int$SYMBOL_NO_DESC
    2$rage::trgFlagScope$SYMBOL_NO_DESC
    2$rage::trgFlagScope::enum@1::kFlagScopeGlobal$SYMBOL_NO_DESC
    2$rage::trgFlagScope::enum@1::kFlagScopeMission$SYMBOL_NO_DESC
    2$rage::trgFlagsFloat$SYMBOL_NO_DESC
    2$rage::trgFlagsFloat::AddWidget@bkBank &@const char *@int$SYMBOL_NO_DESC
    2$rage::trgFlagsInt$SYMBOL_NO_DESC
    2$rage::trgFlagsInt::AddWidget@bkBank &@const char *@int$SYMBOL_NO_DESC
    2$rage::trgInstantAction::ActivatedWithGuid@aGuid *$SYMBOL_NO_DESC
    2$rage::trgInstantAction::DeactivatedWithGuid@aGuid *$SYMBOL_NO_DESC
    2$rage::trgInstantAction::DECLARE_IDMGR_TYPEID@trgInstantAction$SYMBOL_NO_DESC
    2$rage::trgInstantAction::enum@1::kOnEnd$SYMBOL_NO_DESC
    2$rage::trgInstantAction::enum@1::kOnStart$SYMBOL_NO_DESC
    2$rage::trgInstantAction::enum@1::kOnStartAndEnd$SYMBOL_NO_DESC
    2$rage::trgInstantAction::InitClass@void$SYMBOL_NO_DESC
    2$rage::trgInstantAction::trgInstantAction@datResource &$SYMBOL_NO_DESC
    2$rage::trgInstantSingleActorAction::DECLARE_IDMGR_TYPEID@trgInstantSingleActorAction$SYMBOL_NO_DESC
    2$rage::trgInstantSingleActorAction::InitClass$SYMBOL_NO_DESC
    2$rage::trgInstantSubjectObjectAction::DECLARE_IDMGR_TYPEID@trgInstantSubjectObjectAction$SYMBOL_NO_DESC
    2$rage::trgInstantSubjectObjectAction::Init$SYMBOL_NO_DESC
    2$rage::trgInstantSubjectObjectAction::InitClass$SYMBOL_NO_DESC
    2$rage::trgInstantSubjectObjectAction::PagedIn$SYMBOL_NO_DESC
    2$rage::trgInstantSubjectObjectAction::trgInstantSingleActorAction$SYMBOL_NO_DESC
    2$rage::trgInstantSubjectObjectAction::trgInstantSubjectObjectAction@datResource &$SYMBOL_NO_DESC
    2$rage::trgIntHandle$SYMBOL_NO_DESC
    2$rage::trgIntHandle::GetInt@bool &@const$SYMBOL_NO_DESC
    2$rage::trgIntHandle::m_IntIndex$SYMBOL_NO_DESC
    2$rage::trgIntHandle::PagedIn@const char *@int@trgTriggerGroup &$SYMBOL_NO_DESC
    2$rage::trgIntHandle::SetInt@int@const$SYMBOL_NO_DESC
    2$rage::trgIntHandle::trgIntHandle$SYMBOL_NO_DESC
    2$rage::trgItemTypes$SYMBOL_NO_DESC
    2$rage::trgItemTypes::enum@1::kAccessory$SYMBOL_NO_DESC
    2$rage::trgItemTypes::enum@1::kGadget$SYMBOL_NO_DESC
    2$rage::trgItemTypes::enum@1::kItem$SYMBOL_NO_DESC
    2$rage::trgItemTypes::enum@1::kNumItems$SYMBOL_NO_DESC
    2$rage::trgItemTypes::enum@1::kWeapon$SYMBOL_NO_DESC
    2$rage::trgMessageAction::DECLARE_IDMGR_TYPEID@trgMessageAction$SYMBOL_NO_DESC
    2$rage::trgMessageAction::InitClass@void$SYMBOL_NO_DESC
    2$rage::trgMessageAction::trgMessageAction@datResource &$SYMBOL_NO_DESC
    2$rage::trgMsgQueryUseVehicleSpeed$SYMBOL_NO_DESC
    2$rage::trgMsgQueryUseVehicleSpeed::DECLARE_IDMGR_TYPEID@trgMsgQueryUseVehicleSpeed$SYMBOL_NO_DESC
    2$rage::trgMsgQueryUseVehicleSpeed::trgMsgQueryUseVehicleSpeed@void$SYMBOL_NO_DESC
    2$rage::trgMsgQueryUseVehicleSpeed::UseVehicleSpeed$SYMBOL_NO_DESC
    2$rage::trgMsgQueryUseVehicleSpeed::Valid$SYMBOL_NO_DESC
    2$rage::trgOrderLTCaseInsensitive@const char *@const char *$SYMBOL_NO_DESC
    2$rage::trgSignal::=@bool$SYMBOL_NO_DESC
    2$rage::trgSignal::AddActor@aGuid$SYMBOL_NO_DESC
    2$rage::trgSignal::And@const trgSignal &@bool$SYMBOL_NO_DESC
    2$rage::trgSignal::ClearActors@void$SYMBOL_NO_DESC
    2$rage::trgSignal::enum@1::kActorSet$SYMBOL_NO_DESC
    2$rage::trgSignal::enum@1::kFalse$SYMBOL_NO_DESC
    2$rage::trgSignal::enum@1::kTrue$SYMBOL_NO_DESC
    2$rage::trgSignal::GetActors@void@const$SYMBOL_NO_DESC
    2$rage::trgSignal::HasActors@void@const$SYMBOL_NO_DESC
    2$rage::trgSignal::IsFalse@void@const$SYMBOL_NO_DESC
    2$rage::trgSignal::IsTrue@void@const$SYMBOL_NO_DESC
    2$rage::trgSignal::Or@const trgSignal &@bool$SYMBOL_NO_DESC
    2$rage::trgSignal::Set@bool$SYMBOL_NO_DESC
    2$rage::trgSignal::trgSignal@bool$SYMBOL_NO_DESC
    2$rage::trgSignal::trgSignal@void$SYMBOL_NO_DESC
    2$rage::trgSleepID::~trgSleepID$SYMBOL_NO_DESC
    2$rage::trgSleepID::friend class trgSleepManager$SYMBOL_NO_DESC
    2$rage::trgSleepID::IsInUse@void@const$SYMBOL_NO_DESC
    2$rage::trgSleepID::trgSleepID@void$SYMBOL_NO_DESC
    2$rage::trgSleepManager::AddToTimeQueue@float@trgCondition &@trgSleepID &$SYMBOL_NO_DESC
    2$rage::trgSleepManager::RemoveFromTimeQueue@trgSleepID &$SYMBOL_NO_DESC
    2$rage::trgSleepManager::trgSleepManager@int$SYMBOL_NO_DESC
    2$rage::trgSleepManager::Update$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::~trgTriggerGroup$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::ActionResidentAdd@void$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::ActionResidentRemove@void$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::AddWidgets@bkBank &$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::ConditionResidentAdd@void$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::ConditionResidentRemove@void$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::DebugDraw@bool@bool@bool@bool$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::friend class trgTriggerManager$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::GetAction@trgTriggerIndex$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::GetActionIndex@const char *@const$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::GetActionsNum@void@const$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::GetActorTypeManager@void@const$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::GetCondition@trgTriggerIndex$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::GetConditionIndex@const char *@const$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::GetConditionsNum@void@const$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::GetInManagementList@void@const$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::GetInPagingList@void@const$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::GetNumConditions@void@const$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::GetUserData$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::IsEverythingResident@void@const$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::IsNamedConditionValid@const char *@const$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::IsPagedIn@void@const$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::IsStreaming@void@const$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::Load@const trgTriggerManager &@aGuidNamespace &@bool@aActorResources *$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::PagedIn$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::PagedOut$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::ParseActionList@const char *@trgActions &$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::ParseConditionList@const char *@trgTriggerIndex *&@int &$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::ReportNamespaceNotResident@void$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::ReportNamespaceResident@aGuidNamespace &$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::Reset@void$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::SetInManagementList@bool$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::SetInPagingList@bool$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::SetLayoutInfo@const char*@int$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::trgTriggerGroup@aActorTypeManager &$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::trgTriggerGroup@datResource &@aActorTypeManager &$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::UnPlace$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::Update@void$SYMBOL_NO_DESC
    2$rage::trgTriggerGroup::UpdatePaging@void$SYMBOL_NO_DESC
    2$rage::trgTriggerIndex$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::AddActionType@const char *@int@trgAction& *createFunc@void *placeFuncdatResource &rsc, trgAction &obj$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::AddConditionType@const char *@int@trgCondition& *createFunc@void *placeFuncdatResource &rsc, trgCondition &obj$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::AddWidgets$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::AddWidgetsCB@bkBank &$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::Bank$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::ClearMessage$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::CreateAction@const char *@const trgAction **@const$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::CreateActionState$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::CreateActorData$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::CreateAndLoadGroup@aActorTypeManager &@aGuidNamespace &@bool@aActorResources *@const char *$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::DebugDraw$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::DestroyActionState@atDNode<trgActionState> &$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::DestroyActorData@atDNode<trgActorData> &$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::Draw$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::DrawActions$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::DrawBounds$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::DrawConditions$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::DrawCulls$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::GetActionTemplateFolder$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::GetActionTemplateName$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::GetConditionTemplateFolder$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::GetConditionTemplateName$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::GetConditionType@const char *@const$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::GetInstance$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::GetNameOfLayoutBeingLoaded@const$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::InitClass@int@int$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::LoadActionTemplates$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::LoadTemplates$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::LoadTriggerTemplates$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::ParseList@const char *@const char **&@int &$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::PlaceAction@datResource &@trgAction &@const$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::PlaceCondition@datResource &@trgCondition &@const$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::RemoveWidgets$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::Reset$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::Sleep@float@trgCondition &@trgSleepID &$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::sm_StreamDebugLevel$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::StartManagement@trgTriggerGroup &$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::StartPaging@trgTriggerGroup &$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::StopManagement@trgTriggerGroup &$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::StopPaging@trgTriggerGroup &$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::trgConditionOrActionDataPtr$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::trgConditionOrActionDataPtr::m_ActionData$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::trgConditionOrActionDataPtr::m_ConditionData$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::trgConditionOrActionDataPtr::m_Void$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::Unsleep@trgSleepID &$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::Update$SYMBOL_NO_DESC
    2$rage::trgTriggerManager::UpdatePaging$SYMBOL_NO_DESC
    2$rage::trgTriggerStateAction::~trgTriggerStateAction$SYMBOL_NO_DESC
    2$rage::trgTriggerStateAction::DECLARE_IDMGR_TYPEID@trgTriggerStateAction$SYMBOL_NO_DESC
    2$rage::trgTriggerStateAction::enum@1::kDisableCondition$SYMBOL_NO_DESC
    2$rage::trgTriggerStateAction::enum@1::kEnableCondition$SYMBOL_NO_DESC
    2$rage::trgTriggerStateAction::Init@void$SYMBOL_NO_DESC
    2$rage::trgTriggerStateAction::InitClass@void$SYMBOL_NO_DESC
    2$rage::trgTriggerStateAction::trgTriggerStateAction@datResource &$SYMBOL_NO_DESC
    2$rage::trgTriggerStateAction::trgTriggerStateAction@void$SYMBOL_NO_DESC
    2$rage::trgTriggerStats::EXT_PF_GROUP@UpdateLoop$SYMBOL_NO_DESC
    2$rage::trgTriggerStats::EXT_PF_PAGE@Triggers$SYMBOL_NO_DESC
    2$rage::trgTriggerStats::EXT_PF_TIMER@Actions$SYMBOL_NO_DESC
    2$rage::trgTriggerStats::EXT_PF_TIMER@All$SYMBOL_NO_DESC
    2$rage::trgTriggerStats::EXT_PF_TIMER@Conditions$SYMBOL_NO_DESC
    2$rage::trgTriggerStats::EXT_PF_TIMER@Culling$SYMBOL_NO_DESC
    2$rage::trgTriggerStats::EXT_PF_TIMER@Volume$SYMBOL_NO_DESC
    2$rage::trgTriggerStats::PF_GROUP@UpdateLoop$SYMBOL_NO_DESC
    2$rage::trgTriggerStats::PF_LINK@Triggers@UpdateLoop$SYMBOL_NO_DESC
    2$rage::trgTriggerStats::PF_TIMER@Actions@UpdateLoop$SYMBOL_NO_DESC
    2$rage::trgTriggerStats::PF_TIMER@All@UpdateLoop$SYMBOL_NO_DESC
    2$rage::trgTriggerStats::PF_TIMER@Conditions@UpdateLoop$SYMBOL_NO_DESC
    2$rage::trgTriggerStats::PF_TIMER@Culling@UpdateLoop$SYMBOL_NO_DESC
    2$rage::trgTriggerStats::PF_TIMER@Volume@UpdateLoop$SYMBOL_NO_DESC
    2$rage::trgTriggerViz::EXT_PFD_DECLARE_GROUP@Triggers$SYMBOL_NO_DESC
    2$rage::trgTriggerViz::EXT_PFD_DECLARE_ITEM@Culling$SYMBOL_NO_DESC
    2$rage::trgTriggerViz::EXT_PFD_DECLARE_ITEM@Volume$SYMBOL_NO_DESC
    2$rage::trgTriggerViz::PFD_DECLARE_GROUP_ON@Triggers$SYMBOL_NO_DESC
    2$rage::trgTriggerViz::PFD_DECLARE_ITEM_ON@Culling@Color_orange@Triggers$SYMBOL_NO_DESC
    2$rage::trgTriggerViz::PFD_DECLARE_ITEM_ON@Volume@Color_green@Triggers$SYMBOL_NO_DESC
3$trgAction::sm_PagingGroup$SYMBOL_NO_DESC
3$trgCondition::sm_PagingGroup$SYMBOL_NO_DESC
3$trgFlagContextManager::sm_CurrentImplementation$SYMBOL_NO_DESC
3$trgTriggerGroup::sm_NameOfLayoutBeingLoaded$SYMBOL_NO_DESC
1$trgTriggerManager$SYMBOL_NO_DESC
    2$trgTriggerManager::sm_StreamDebugLevel$SYMBOL_NO_DESC
    2$trgTriggerManager::smInstance$SYMBOL_NO_DESC
3$RAGE_TRGACTIONS_NOACTOR$SYMBOL_NO_DESC
3$RAGE_TRGACTIONSABSTRACT$SYMBOL_NO_DESC
3$RAGE_TRGCONDITIONS_ACTOR$SYMBOL_NO_DESC
3$RAGE_TRGCONDITIONS_ACTOR_SPECIAL$SYMBOL_NO_DESC
3$RAGE_TRGCONDITIONS_BOOLEAN$SYMBOL_NO_DESC
3$RAGE_TRGCONDITIONS_NOACTOR$SYMBOL_NO_DESC
3$RAGE_TRGCONDITIONS_NOACTOR_SPECIAL$SYMBOL_NO_DESC
3$ReleaseAssert$SYMBOL_NO_DESC
