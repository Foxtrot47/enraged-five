3$@@Overview$DTX_SECTION_NOT_FILLED_IN
3$@@Components$DTX_SECTION_NOT_FILLED_IN
3$@@Relationships$DTX_SECTION_NOT_FILLED_IN
3$@@Working Examples$DTX_SECTION_NOT_FILLED_IN
3$Working$POSSIBLE_STRAY_TOPIC
1$rage$FUNC_PARAM_NO_DESC
    2$rage::audMusic::FindUserMusic@const int@const int@audUserPlaylistInfo&@int&$FUNC_PARAM_NO_DESC
    2$rage::audMusic::Play@audUserPlaylistInfo *$FUNC_PARAM_NO_DESC
    2$rage::audMusic::SetChangeTrackFunc@const Functor0 *$FUNC_PARAM_NO_DESC
    2$rage::audMusic::SetFindUserMusicFunc@const Functor0 *$FUNC_PARAM_NO_DESC
    2$rage::audMusic::SetVolume@const float$FUNC_PARAM_NO_DESC
1$rage$FUNC_PARAM_NO_NAME
    2$rage::audMusic::Play@audUserPlaylistInfo *$FUNC_PARAM_NO_NAME
    2$rage::audMusic::SetChangeTrackFunc@const Functor0 *$FUNC_PARAM_NO_NAME
    2$rage::audMusic::SetFindUserMusicFunc@const Functor0 *$FUNC_PARAM_NO_NAME
    2$rage::audMusic::SetVolume@const float$FUNC_PARAM_NO_NAME
1$rage$FUNC_RETURN_VAL_NO_DESC
    2$rage::audMusic::FindUserMusic@const int@const int@audUserPlaylistInfo&@int&$FUNC_RETURN_VAL_NO_DESC
    2$rage::audMusic::GetCurrentSongInfo@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::audMusic::Play@audUserPlaylistInfo *$FUNC_RETURN_VAL_NO_DESC
    2$rage::audMusic::SetChangeTrackFunc@const Functor0 *$FUNC_RETURN_VAL_NO_DESC
    2$rage::audMusic::SetFindUserMusicFunc@const Functor0 *$FUNC_RETURN_VAL_NO_DESC
    2$rage::audMusic::SetVolume@const float$FUNC_RETURN_VAL_NO_DESC
    2$rage::audMusic::Update$FUNC_RETURN_VAL_NO_DESC
1$rage$SYMBOL_NO_DESC
    2$rage::audMusic$SYMBOL_NO_DESC
    2$rage::audMusic::~audMusic$SYMBOL_NO_DESC
    2$rage::audMusic::audMusic$SYMBOL_NO_DESC
    2$rage::audMusic::Continue$SYMBOL_NO_DESC
    2$rage::audMusic::enum@1::kStateBusy$SYMBOL_NO_DESC
    2$rage::audMusic::enum@1::kStateFindUserMusic$SYMBOL_NO_DESC
    2$rage::audMusic::enum@1::kStateNone$SYMBOL_NO_DESC
    2$rage::audMusic::enum@2::kIdle$SYMBOL_NO_DESC
    2$rage::audMusic::enum@2::kPaused$SYMBOL_NO_DESC
    2$rage::audMusic::enum@2::kPlay$SYMBOL_NO_DESC
    2$rage::audMusic::GetPlayState@const$SYMBOL_NO_DESC
    2$rage::audMusic::GetState@const$SYMBOL_NO_DESC
    2$rage::audMusic::Next$SYMBOL_NO_DESC
    2$rage::audMusic::Pause$SYMBOL_NO_DESC
    2$rage::audMusic::Previous$SYMBOL_NO_DESC
    2$rage::audMusic::Stop$SYMBOL_NO_DESC
    2$rage::audMusicXenon$SYMBOL_NO_DESC
    2$rage::audMusicXenon::~audMusicXenon$SYMBOL_NO_DESC
    2$rage::audMusicXenon::audMusicXenon$SYMBOL_NO_DESC
    2$rage::audMusicXenon::Continue$SYMBOL_NO_DESC
    2$rage::audMusicXenon::FindUserMusic@const int@const int@audUserPlaylistInfo&@int&$SYMBOL_NO_DESC
    2$rage::audMusicXenon::GetCurrentSongInfo@const$SYMBOL_NO_DESC
    2$rage::audMusicXenon::GetPlayState@const$SYMBOL_NO_DESC
    2$rage::audMusicXenon::GetState@const$SYMBOL_NO_DESC
    2$rage::audMusicXenon::Next$SYMBOL_NO_DESC
    2$rage::audMusicXenon::Pause$SYMBOL_NO_DESC
    2$rage::audMusicXenon::Play@audUserPlaylistInfo *$SYMBOL_NO_DESC
    2$rage::audMusicXenon::Previous$SYMBOL_NO_DESC
    2$rage::audMusicXenon::SetVolume@const float$SYMBOL_NO_DESC
    2$rage::audMusicXenon::Stop$SYMBOL_NO_DESC
    2$rage::audMusicXenon::Update$SYMBOL_NO_DESC
    2$rage::audUserPlaylistInfo$SYMBOL_NO_DESC
    2$rage::audUserPlaylistInfo::UserPlaylist$SYMBOL_NO_DESC
    2$rage::audUserSongInfo$SYMBOL_NO_DESC
    2$rage::audUserSongInfo::SongInfo$SYMBOL_NO_DESC
3$AUDMUSIC_SONGINFO_H$SYMBOL_NO_DESC
3$NUM_SONGS_PER_QUERY$SYMBOL_NO_DESC
