3$@@Overview$DTX_SECTION_NOT_FILLED_IN
3$@@Components$DTX_SECTION_NOT_FILLED_IN
3$@@Working Examples$DTX_SECTION_NOT_FILLED_IN
3$Command$POSSIBLE_STRAY_TOPIC
3$Working$POSSIBLE_STRAY_TOPIC
1$rage$FUNC_PARAM_NO_DESC
    2$rage::vehAudio::AddWidgets@bkBank&$FUNC_PARAM_NO_DESC
    2$rage::vehAudio::SetVehicle@vehEntity*$FUNC_PARAM_NO_DESC
    2$rage::vehDamage::AddWidgets@bkBank&$FUNC_PARAM_NO_DESC
    2$rage::vehDamage::Impact@phImpact::Iterator$FUNC_PARAM_NO_DESC
    2$rage::vehDamage::SetVehicle@vehEntity*$FUNC_PARAM_NO_DESC
    2$rage::vehEntity::AddWidgets@bkBank&$FUNC_PARAM_NO_DESC
    2$rage::vehEntity::Impact@phImpact::Iterator$FUNC_PARAM_NO_DESC
    2$rage::vehEntity::SetAudio@vehAudio*$FUNC_PARAM_NO_DESC
    2$rage::vehEntity::SetBehavior@phInstBehavior*$FUNC_PARAM_NO_DESC
    2$rage::vehEntity::SetDamage@vehDamage*$FUNC_PARAM_NO_DESC
    2$rage::vehEntity::SetFeedback@vehFeedback*$FUNC_PARAM_NO_DESC
    2$rage::vehEntity::SetInput@vehInput*$FUNC_PARAM_NO_DESC
    2$rage::vehEntity::SetInstance@phInst*$FUNC_PARAM_NO_DESC
    2$rage::vehEntity::SetModel@vehModel*$FUNC_PARAM_NO_DESC
    2$rage::vehEntity::SetSim@vehSim*$FUNC_PARAM_NO_DESC
    2$rage::vehEntity::SetSkeleton@crSkeleton*$FUNC_PARAM_NO_DESC
    2$rage::vehFactory::Construct@vehType*$FUNC_PARAM_NO_DESC
    2$rage::vehFactory::SetType@vehType*$FUNC_PARAM_NO_DESC
    2$rage::vehFeedback::ClearAllSamples@bool$FUNC_PARAM_NO_DESC
    2$rage::vehFeedback::GetNextUnit@int$FUNC_PARAM_NO_DESC
    2$rage::vehFeedback::PlayFeedbackSample@int@int@float *@int *$FUNC_PARAM_NO_DESC
    2$rage::vehFeedback::SetActuatorValue@int@float$FUNC_PARAM_NO_DESC
    2$rage::vehFeedback::SetFeedback@bool$FUNC_PARAM_NO_DESC
    2$rage::vehFeedback::SetPad@ioPad *$FUNC_PARAM_NO_DESC
    2$rage::vehFeedback::SetPadID@int$FUNC_PARAM_NO_DESC
    2$rage::vehFeedback::SetTimingUnit@float$FUNC_PARAM_NO_DESC
    2$rage::vehFeedback::SetVehicle@vehEntity *$FUNC_PARAM_NO_DESC
    2$rage::vehInput::AddWidgets@bkBank&$FUNC_PARAM_NO_DESC
    2$rage::vehInput::Impact@phImpact::Iterator$FUNC_PARAM_NO_DESC
    2$rage::vehInput::SetAttitude@float$FUNC_PARAM_NO_DESC
    2$rage::vehInput::SetBrake@float$FUNC_PARAM_NO_DESC
    2$rage::vehInput::SetGear@short$FUNC_PARAM_NO_DESC
    2$rage::vehInput::SetHandbrake@float$FUNC_PARAM_NO_DESC
    2$rage::vehInput::SetIndex@s32$FUNC_PARAM_NO_DESC
    2$rage::vehInput::SetPitch@float$FUNC_PARAM_NO_DESC
    2$rage::vehInput::SetRoll@float$FUNC_PARAM_NO_DESC
    2$rage::vehInput::SetSpecial@int$FUNC_PARAM_NO_DESC
    2$rage::vehInput::SetSteer@float$FUNC_PARAM_NO_DESC
    2$rage::vehInput::SetStyle@s32$FUNC_PARAM_NO_DESC
    2$rage::vehInput::SetThrottle@float$FUNC_PARAM_NO_DESC
    2$rage::vehInput::SetType@const s32$FUNC_PARAM_NO_DESC
    2$rage::vehInput::SetVehicle@vehEntity *$FUNC_PARAM_NO_DESC
    2$rage::vehInput::vehInput@s32$FUNC_PARAM_NO_DESC
    2$rage::vehManager::AddEntry@vehEntity*$FUNC_PARAM_NO_DESC
    2$rage::vehManager::DelEntry@vehEntity*$FUNC_PARAM_NO_DESC
    2$rage::vehModel::AddWidgets@bkBank&$FUNC_PARAM_NO_DESC
    2$rage::vehModel::SetVehicle@vehEntity*$FUNC_PARAM_NO_DESC
    2$rage::vehSim::AddWidgets@bkBank&$FUNC_PARAM_NO_DESC
    2$rage::vehSim::ApplyDamage@const Vector3&@float$FUNC_PARAM_NO_DESC
    2$rage::vehSim::GetAxle@int@const$FUNC_PARAM_NO_DESC
    2$rage::vehSim::GetDrivetrain@int@const$FUNC_PARAM_NO_DESC
    2$rage::vehSim::GetSuspension@int@const$FUNC_PARAM_NO_DESC
    2$rage::vehSim::GetWheel@int@const$FUNC_PARAM_NO_DESC
    2$rage::vehSim::Impact@phImpact::Iterator UNUSED_PARAMimpacts$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetAero@vehAero *$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetAttitude@float$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetAxle@int@vehAxle *$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetBrake@float$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetCenterOfMass@const Vector3&$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetCollider@phCollider*$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetDrivetrain@int@vehDrivetrain *$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetEngine@vehEngine *$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetFluid@vehFluid *$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetHandbrake@float$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetModelOffset@const Vector3&$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetResetAngle@const Vector3&$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetResetAngle@float$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetResetAngVel@const Vector3&$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetResetPos@const Vector3&$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetResetVel@const Vector3&$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetSteer@float$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetSuspension@int@vehSuspension *$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetThrottle@float$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetTrans@vehTransmission *$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetVehicle@vehEntity *$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetWheel@int@vehWheel *$FUNC_PARAM_NO_DESC
    2$rage::vehSim::SetWorldMatrix@const Matrix34&$FUNC_PARAM_NO_DESC
    2$rage::vehSim::Teleport@const Matrix34&$FUNC_PARAM_NO_DESC
    2$rage::vehSimTune::Lerp@float@const vehSimTune*@const vehSimTune*$FUNC_PARAM_NO_DESC
    2$rage::vehSimTune::SetCenterOfMass@const Vector3&$FUNC_PARAM_NO_DESC
    2$rage::vehSimTune::SetModelOffset@const Vector3&$FUNC_PARAM_NO_DESC
    2$rage::vehSimTune::vehSimTune@class datResource&$FUNC_PARAM_NO_DESC
    2$rage::vehStats::Start@eSubject$FUNC_PARAM_NO_DESC
    2$rage::vehStats::Stop@eSubject$FUNC_PARAM_NO_DESC
    2$rage::vehTuneBase::AddWidgets@bkBank&$FUNC_PARAM_NO_DESC
    2$rage::vehTuneBase::AllocData@const char*@const char*$FUNC_PARAM_NO_DESC
    2$rage::vehTuneBase::DestroyWidgets@bkBank &$FUNC_PARAM_NO_DESC
    2$rage::vehTuneBase::Load@void$FUNC_PARAM_NO_DESC
    2$rage::vehTuneBase::LoadData@const char*@const char*$FUNC_PARAM_NO_DESC
    2$rage::vehTuneBase::SetAssetFolder@const char*$FUNC_PARAM_NO_DESC
    2$rage::vehTuneBase::SetDefaultFolder@const char*$FUNC_PARAM_NO_DESC
    2$rage::vehTuneBase::SetExt@const char *$FUNC_PARAM_NO_DESC
1$rage$FUNC_PARAM_NO_NAME
    2$rage::vehAudio::AddWidgets@bkBank&$FUNC_PARAM_NO_NAME
    2$rage::vehAudio::SetVehicle@vehEntity*$FUNC_PARAM_NO_NAME
    2$rage::vehDamage::AddWidgets@bkBank&$FUNC_PARAM_NO_NAME
    2$rage::vehDamage::Impact@phImpact::Iterator$FUNC_PARAM_NO_NAME
    2$rage::vehDamage::SetVehicle@vehEntity*$FUNC_PARAM_NO_NAME
    2$rage::vehEntity::AddWidgets@bkBank&$FUNC_PARAM_NO_NAME
    2$rage::vehEntity::Impact@phImpact::Iterator$FUNC_PARAM_NO_NAME
    2$rage::vehEntity::SetAudio@vehAudio*$FUNC_PARAM_NO_NAME
    2$rage::vehEntity::SetBehavior@phInstBehavior*$FUNC_PARAM_NO_NAME
    2$rage::vehEntity::SetDamage@vehDamage*$FUNC_PARAM_NO_NAME
    2$rage::vehEntity::SetFeedback@vehFeedback*$FUNC_PARAM_NO_NAME
    2$rage::vehEntity::SetInput@vehInput*$FUNC_PARAM_NO_NAME
    2$rage::vehEntity::SetInstance@phInst*$FUNC_PARAM_NO_NAME
    2$rage::vehEntity::SetModel@vehModel*$FUNC_PARAM_NO_NAME
    2$rage::vehEntity::SetSim@vehSim*$FUNC_PARAM_NO_NAME
    2$rage::vehEntity::SetSkeleton@crSkeleton*$FUNC_PARAM_NO_NAME
    2$rage::vehFactory::Construct@vehType*$FUNC_PARAM_NO_NAME
    2$rage::vehFactory::SetType@vehType*$FUNC_PARAM_NO_NAME
    2$rage::vehFeedback::ClearAllSamples@bool$FUNC_PARAM_NO_NAME
    2$rage::vehFeedback::GetNextUnit@int$FUNC_PARAM_NO_NAME
    2$rage::vehFeedback::PlayFeedbackSample@int@int@float *@int *$FUNC_PARAM_NO_NAME
    2$rage::vehFeedback::SetActuatorValue@int@float$FUNC_PARAM_NO_NAME
    2$rage::vehFeedback::SetFeedback@bool$FUNC_PARAM_NO_NAME
    2$rage::vehFeedback::SetPad@ioPad *$FUNC_PARAM_NO_NAME
    2$rage::vehFeedback::SetPadID@int$FUNC_PARAM_NO_NAME
    2$rage::vehFeedback::SetTimingUnit@float$FUNC_PARAM_NO_NAME
    2$rage::vehFeedback::SetVehicle@vehEntity *$FUNC_PARAM_NO_NAME
    2$rage::vehInput::AddWidgets@bkBank&$FUNC_PARAM_NO_NAME
    2$rage::vehInput::Impact@phImpact::Iterator$FUNC_PARAM_NO_NAME
    2$rage::vehInput::SetAttitude@float$FUNC_PARAM_NO_NAME
    2$rage::vehInput::SetBrake@float$FUNC_PARAM_NO_NAME
    2$rage::vehInput::SetGear@short$FUNC_PARAM_NO_NAME
    2$rage::vehInput::SetHandbrake@float$FUNC_PARAM_NO_NAME
    2$rage::vehInput::SetIndex@s32$FUNC_PARAM_NO_NAME
    2$rage::vehInput::SetPitch@float$FUNC_PARAM_NO_NAME
    2$rage::vehInput::SetRoll@float$FUNC_PARAM_NO_NAME
    2$rage::vehInput::SetSpecial@int$FUNC_PARAM_NO_NAME
    2$rage::vehInput::SetSteer@float$FUNC_PARAM_NO_NAME
    2$rage::vehInput::SetStyle@s32$FUNC_PARAM_NO_NAME
    2$rage::vehInput::SetThrottle@float$FUNC_PARAM_NO_NAME
    2$rage::vehInput::SetType@const s32$FUNC_PARAM_NO_NAME
    2$rage::vehInput::SetVehicle@vehEntity *$FUNC_PARAM_NO_NAME
    2$rage::vehInput::vehInput@s32$FUNC_PARAM_NO_NAME
    2$rage::vehManager::AddEntry@vehEntity*$FUNC_PARAM_NO_NAME
    2$rage::vehManager::DelEntry@vehEntity*$FUNC_PARAM_NO_NAME
    2$rage::vehModel::AddWidgets@bkBank&$FUNC_PARAM_NO_NAME
    2$rage::vehModel::SetVehicle@vehEntity*$FUNC_PARAM_NO_NAME
    2$rage::vehSim::AddWidgets@bkBank&$FUNC_PARAM_NO_NAME
    2$rage::vehSim::ApplyDamage@const Vector3&@float$FUNC_PARAM_NO_NAME
    2$rage::vehSim::GetAxle@int@const$FUNC_PARAM_NO_NAME
    2$rage::vehSim::GetDrivetrain@int@const$FUNC_PARAM_NO_NAME
    2$rage::vehSim::GetSuspension@int@const$FUNC_PARAM_NO_NAME
    2$rage::vehSim::GetWheel@int@const$FUNC_PARAM_NO_NAME
    2$rage::vehSim::Impact@phImpact::Iterator UNUSED_PARAMimpacts$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetAero@vehAero *$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetAttitude@float$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetAxle@int@vehAxle *$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetBrake@float$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetCenterOfMass@const Vector3&$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetCollider@phCollider*$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetDrivetrain@int@vehDrivetrain *$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetEngine@vehEngine *$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetFluid@vehFluid *$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetHandbrake@float$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetModelOffset@const Vector3&$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetResetAngle@const Vector3&$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetResetAngle@float$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetResetAngVel@const Vector3&$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetResetPos@const Vector3&$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetResetVel@const Vector3&$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetSteer@float$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetSuspension@int@vehSuspension *$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetThrottle@float$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetTrans@vehTransmission *$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetVehicle@vehEntity *$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetWheel@int@vehWheel *$FUNC_PARAM_NO_NAME
    2$rage::vehSim::SetWorldMatrix@const Matrix34&$FUNC_PARAM_NO_NAME
    2$rage::vehSim::Teleport@const Matrix34&$FUNC_PARAM_NO_NAME
    2$rage::vehSimTune::Lerp@float@const vehSimTune*@const vehSimTune*$FUNC_PARAM_NO_NAME
    2$rage::vehSimTune::SetCenterOfMass@const Vector3&$FUNC_PARAM_NO_NAME
    2$rage::vehSimTune::SetModelOffset@const Vector3&$FUNC_PARAM_NO_NAME
    2$rage::vehSimTune::vehSimTune@class datResource&$FUNC_PARAM_NO_NAME
    2$rage::vehStats::Start@eSubject$FUNC_PARAM_NO_NAME
    2$rage::vehStats::Stop@eSubject$FUNC_PARAM_NO_NAME
    2$rage::vehTuneBase::AddWidgets@bkBank&$FUNC_PARAM_NO_NAME
    2$rage::vehTuneBase::AllocData@const char*@const char*$FUNC_PARAM_NO_NAME
    2$rage::vehTuneBase::DestroyWidgets@bkBank &$FUNC_PARAM_NO_NAME
    2$rage::vehTuneBase::Load@void$FUNC_PARAM_NO_NAME
    2$rage::vehTuneBase::LoadData@const char*@const char*$FUNC_PARAM_NO_NAME
    2$rage::vehTuneBase::SetAssetFolder@const char*$FUNC_PARAM_NO_NAME
    2$rage::vehTuneBase::SetDefaultFolder@const char*$FUNC_PARAM_NO_NAME
    2$rage::vehType::vehType@const char *$FUNC_PARAM_NO_NAME
1$rage$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehAudio::~vehAudio$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehAudio::AddWidgets@bkBank&$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehAudio::GetVehicle@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehAudio::Reset$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehAudio::Update$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehDamage::~vehDamage$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehDamage::AddWidgets@bkBank&$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehDamage::GetVehicle@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehDamage::Impact@phImpact::Iterator$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehDamage::Reset$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehDamage::Update$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehEntity::~vehEntity$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehEntity::AddWidgets@bkBank&$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehEntity::GetAudio@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehEntity::GetBehavior@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehEntity::GetDamage@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehEntity::GetFeedback@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehEntity::GetInput@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehEntity::GetInstance@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehEntity::GetModel@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehEntity::GetSim@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehEntity::GetSkeleton@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehEntity::Impact@phImpact::Iterator$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehEntity::Reset$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehEntity::Update$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFactory::~vehFactory$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFactory::Construct@vehType*$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFactory::GetType@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFeedback::~vehFeedback$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFeedback::ClearAllSamples@bool$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFeedback::GetNextUnit@int$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFeedback::GetNumActuators$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFeedback::GetPad$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFeedback::GetPadID$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFeedback::GetVehicle@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFeedback::IsEnabled$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFeedback::PlayFeedbackSample@int@int@float *@int *$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFeedback::SetActuatorValue@int@float$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFeedback::SetFeedback@bool$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFeedback::SetTimingUnit@float$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehFeedback::Update$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::~vehInput$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::AddWidgets@bkBank&$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::GetAttitude@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::GetBrake@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::GetGear@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::GetHandbrake@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::GetIndex@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::GetPitch@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::GetRoll@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::GetSpecial@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::GetSteer@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::GetStyle@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::GetThrottle@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::GetType@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::GetVehicle@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::Impact@phImpact::Iterator$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::Reset$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::SetIndex@s32$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::SetStyle@s32$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehInput::Update$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehManager::~vehManager$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehManager::AddEntry@vehEntity*$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehManager::Construct$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehManager::DelEntry@vehEntity*$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehManager::Destruct$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehManager::Draw$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehManager::Instance$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehManager::Reset$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehManager::Update$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehModel::~vehModel$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehModel::AddWidgets@bkBank&$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehModel::GetVehicle@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehModel::Reset$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehModel::Update$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::~vehSim$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::AddWidgets@bkBank&$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::ApplyDamage@const Vector3&@float$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::ClearDamage$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetAero@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetAirTime@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetAngInertiaScale@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetAttitude@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetAxle@int@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetBrake@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetCenterOfMass@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetCollider$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetCollider@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetDrivetrain@int@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetEngine@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetFluid@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetHandbrake@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetModelOffset@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetNumAxles@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetNumDrivetrains@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetNumSuspensions@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetNumWheels@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetResetAngle@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetResetAngVel@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetResetPos@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetResetVel@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetSpeed@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetSteer@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetSuspension@int@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetThrottle@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetTrans@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetVehicle@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetVelocity@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetWheel@int@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetWorldMatrix@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::GetWorldPosition@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::Impact@phImpact::Iterator UNUSED_PARAMimpacts$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::IsDrivable@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::OnGround@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::Reset$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::Teleport@const Matrix34&$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::TestProbe$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::Update$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::UpdateBound$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::UpdateInput$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSim::UpdatePush$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSimTune::Allocate$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSimTune::GetCenterOfMass@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehSimTune::GetModelOffset@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehTuneBase::AddWidgets@bkBank&$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehTuneBase::Allocate$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehTuneBase::AllocData@const char*@const char*$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehTuneBase::ApplyChanges@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehTuneBase::GetAssetFolder$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehTuneBase::GetBank@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehTuneBase::GetDefaultFolder$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehTuneBase::GetExt@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehTuneBase::GetName@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehTuneBase::IsBankShown@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehTuneBase::Load@void$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehTuneBase::LoadData@const char*@const char*$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehTuneBase::SetDefaultFolder@const char*$FUNC_RETURN_VAL_NO_DESC
    2$rage::vehType::GetName@const$FUNC_RETURN_VAL_NO_DESC
1$rage$SYMBOL_NO_DESC
    2$rage::CollisionLargeVals$SYMBOL_NO_DESC
    2$rage::CollisionPriorities$SYMBOL_NO_DESC
    2$rage::CollisionSmallVals$SYMBOL_NO_DESC
    2$rage::VEH_STATS_START$SYMBOL_NO_DESC
    2$rage::VEH_STATS_STOP$SYMBOL_NO_DESC
    2$rage::VEH_UPDATE_BATCHED$SYMBOL_NO_DESC
    2$rage::vehFeedback::eActuator::kLarge$SYMBOL_NO_DESC
    2$rage::vehFeedback::eActuator::kSmall$SYMBOL_NO_DESC
    2$rage::vehFeedback::enum@1::kMaxSampleTracks$SYMBOL_NO_DESC
    2$rage::vehFeedback::eSampleStandard::kCollision$SYMBOL_NO_DESC
    2$rage::vehFeedback::PlayFeedbackSampleID@int$SYMBOL_NO_DESC
    2$rage::vehFeedback::Sample$SYMBOL_NO_DESC
    2$rage::vehFeedback::Sample::length$SYMBOL_NO_DESC
    2$rage::vehFeedback::Sample::nSamples$SYMBOL_NO_DESC
    2$rage::vehFeedback::Sample::position$SYMBOL_NO_DESC
    2$rage::vehFeedback::Sample::ppPriorities$SYMBOL_NO_DESC
    2$rage::vehFeedback::Sample::ppVals$SYMBOL_NO_DESC
    2$rage::vehFeedback::SampleRec::length$SYMBOL_NO_DESC
    2$rage::vehFeedback::SampleRec::pPriorities$SYMBOL_NO_DESC
    2$rage::vehFeedback::SampleRec::pVals$SYMBOL_NO_DESC
    2$rage::VEHMANAGER$SYMBOL_NO_DESC
    2$rage::vehSim::GetWorldCenterOfMass@const$SYMBOL_NO_DESC
    2$rage::vehSim::s_Back$SYMBOL_NO_DESC
    2$rage::vehSim::s_Back@0@0@1$SYMBOL_NO_DESC
    2$rage::vehSim::s_Right$SYMBOL_NO_DESC
    2$rage::vehSim::s_Right@1@0@0$SYMBOL_NO_DESC
    2$rage::vehSim::s_Up$SYMBOL_NO_DESC
    2$rage::vehSim::s_Up@0@1@0$SYMBOL_NO_DESC
    2$rage::vehSimTune$SYMBOL_NO_DESC
    2$rage::VEHSTATS$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kAudio$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kCarTotal$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kCarUpdate$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kDamage$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kDriver$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kFeedback$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kGyro$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kInput$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kModel$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kNumSubjects$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kParticles$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kSimAero$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kSimCollider$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kSimDrivetrain$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kSimEngine$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kSimOther$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kSimProbe$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kSimSuspension$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kSimTotal$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kSimTrans$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kSimUpdate$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kSimWheel$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kSimWheelProbe$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kSimWheelTorque$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kStuck$SYMBOL_NO_DESC
    2$rage::vehStats::eSubject::kTest$SYMBOL_NO_DESC
    2$rage::vehStats::vehStatsStruct::DCache$SYMBOL_NO_DESC
    2$rage::vehStats::vehStatsStruct::ICache$SYMBOL_NO_DESC
    2$rage::vehStats::vehStatsStruct::Time$SYMBOL_NO_DESC
    2$rage::vehType::vehType@datResource&$SYMBOL_NO_DESC
3$VEH_STATS_START$SYMBOL_NO_DESC
3$VEH_STATS_STOP$SYMBOL_NO_DESC
