set ARCHIVE=qa_ainavigation
set CODEONLY=qa_navhiersearch

set LIBS_CORE=%RAGE_CORE_LIBS% spatialdata curve
set LIBS_GRAPHICS=%RAGE_GFX_LIBS%
set LIBS_BASE_AI=
set LIBS_PHYSICS=%RAGE_PH_LIBS%
set LIBS_SAMPLE=sample_ai %RAGE_SAMPLE_GRCORE_LIBS%
set LIBS=%LIBS_CORE% %LIBS_GRAPHICS% %LIBS_BASE_AI% %LIBS_PHYSICS% %LIBS_SAMPLE%
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\qa %RAGE_DIR%\base\samples 
set XPROJ=%XPROJ% %RAGE_DIR%\suite\src %RAGE_DIR%\suite\qa %RAGE_DIR%\suite\samples
