/* qa_ainavigation/qa_navhiersearch.cpp */

/*
 *	PURPOSE
 *	Run a nav hierarchy tester as a QA item.
 */
//////////////////////////////////////////////////////////////////////////

#include "qa/qa.h"

#if __QA

#include "sample_ai/navtestbed.h"
#include "sample_ai/testnavhiersearch.h"
#include "system/param.h"

using namespace rage;
using namespace ragesamples;
using namespace ai;
using namespace navNodeDefs;

PARAM(path,		"The base asset path.");
PARAM(resource,	"The resource to load (or build, if it's not built already).");

//////////////////////////////////////////////////////////////////////////
//		qaSampleNavHierSearch
//////////////////////////////////////////////////////////////////////////


class qaSampleNavHierSearch : public qaItem
{
public:
	qaSampleNavHierSearch() 
		: m_Sample(NULL)
	{	}

	void Init(const char* srcFile, const char* logFile, int batchCount);
	void Shutdown();

	void Update(qaResult& result);

private:
	navNavigationSampleManager* m_Sample;
};


void qaSampleNavHierSearch::Init(const char* srcFile, const char* logFile, int batchCount)
{
	if (batchCount < 1)
	{
		return;
	}

	// TODO:  This is of course quite mad.  Or at the very least, it's
	// not much in keeping with the notion of unit testing, unless
	// you don't mind looking at the entire program as a unit.
	//
	// But actually, the main thing that's crazy is the danger that user 
	// input could interrupt the test, or that the sample will just stop 
	// for some reason and the test will never abort.
	//
	// Also problematic is how much longer it takes to run a pathfinding 
	// operation when you're actually "timeslicing" it and displaying it 
	// onscreen.  And the idea that we use the loosely-coupled PARAM stuff 
	// to interface with the tester.  Much craziness.
	// 
	// Long story short (too late!):  We don't really want to run an 
	// automated variant of a sample program; we want to run a completely 
	// inputless system.  However, until we carve the code for that system
	// out of the sample, this will get us up and running with AI QA.

	const char* pathName	= "t:/rage/assets/";
	const char* meshRscName	= "$/sample_ai/sample_navhiersearch/sw_mission";		// Currently this resource doesn't exist, unfortunately. /FF
	PARAM_path.Get(pathName);
	PARAM_resource.Get(meshRscName);

	Assert(!m_Sample);
	m_Sample = new navNavigationSampleManager(pathName, srcFile, meshRscName, logFile, batchCount);
	m_Sample->Init();
}

void qaSampleNavHierSearch::Shutdown()
{
	m_Sample->Shutdown();
	delete m_Sample;
	m_Sample = NULL;
}

void qaSampleNavHierSearch::Update(qaResult& result)
{
	if (!m_Sample)
	{
		QALog( "Bogus test - tried to do a batch of 0 or less." );
		TST_FAIL;
		return;
	}
	
	// Should only need to do this once; it runs the entire batch.
	m_Sample->UpdateLoop();

	// "Hey, Rocky, watch me pull a rabbit outta my hat!"
	// "Awww, that trick never works."
	// "This time for sure!"
	TST_PASS;
}


//////////////////////////////////////////////////////////////////////////
//		qaNavTestbedSearches
//////////////////////////////////////////////////////////////////////////


class qaNavTestbedSearches : public qaItem
{
public:
	qaNavTestbedSearches() 
		: m_Testbed(NULL)
	{	}

	void Init(const char* srcFile, const char* logFile, int batchCount);
	void Shutdown();

	void Update(qaResult& result);

private:
	navNavigationTestbed* m_Testbed;
};


void qaNavTestbedSearches::Init(const char* srcFile, const char* logFile, int batchCount)
{
	if (batchCount < 1)
	{
		return;
	}

	Assert(!m_Testbed);
	m_Testbed = new navNavigationTestbed;
	m_Testbed->Init("t:/rage/assets/", srcFile, NULL, logFile, batchCount);
}

void qaNavTestbedSearches::Shutdown()
{
	m_Testbed->Shutdown();
	delete m_Testbed;
	m_Testbed = NULL;
}

void qaNavTestbedSearches::Update(qaResult& result)
{
	if (!m_Testbed)
	{
		QALog( "Bogus test - tried to do a batch of 0 or less." );
		TST_FAIL;
		return;
	}

	// The batch init parameter to the testbed will make it start automatically.
	m_Testbed->Update(false);

	if (m_Testbed->GetWantsExit())
	{
		TST_PASS;
	}
}


QA_ITEM_FAMILY(qaNavTestbedSearches, (const char* srcFile, const char* logFile, int batchCount), (srcFile, logFile, batchCount));

QA_ITEM(qaNavTestbedSearches, ("$/sample_ai/sample_navhiersearch/playground_tests.nav", "playground_tests", 5), qaResult::FAIL_OR_TOTAL_TIME);

QA_ITEM(qaNavTestbedSearches, ("$/sample_ai/buildmesh_sw/sw_mission.nav", "mission_tests", 5), qaResult::FAIL_OR_TOTAL_TIME);

#else

int qa_ainavigation_dont_cause_linker_warning;


#endif // __QA

/* EOF qa_ainavigation/qa_navhiersearch.cpp */
