// 
// qa_aicover/qa_coverfinderinitshutdown.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "qa/qa.h"

#if __QA

#include "aicover/coverfindercontext.h"
#include "aicover/covermanager.h"
#include "aicover/covershooter.h"
#include "aicover/finder.h"
#include "file/asset.h"
#include "math/random.h"
#include "parser/manager.h"
#include "sample_aicover/world.h"
#include "system/rageroot.h"
#include "qa/item.h"

#include "system/timemgr.h"

using namespace rage;
using namespace rage::ai;
using namespace ragesamples;

class qa_aiCoverFinderInitShutdown : public qaItem
{
public:
	qa_aiCoverFinderInitShutdown();
	virtual ~qa_aiCoverFinderInitShutdown();

	// PURPOSE:	Initialize the test.
	// PARAMS:	numCoverFinders		- The number of "AI actors" finding cover.
	//			numEnemies			- The number of target/threat points to create.
	//			enemySwitchProb		- The probability (0..1) on any given update
	//								  for each cover finder to switch enemies.
	//			stateSwitchProb		- The probability (0..1) on any given update to
	//								  for a cover finder to switch
	//								  initialization/update state.
	//			iterMax				- The total number of updates before considering
	//								  the test successful.
	void Init(int numCoverFinders, int numEnemies, float testDist,
			float enemySwitchProb, float stateSwitchProb, int iterMax);

	virtual void Shutdown();

	virtual void Update(qaResult &result);

protected:
	void SetCoverFinderState(int index, int newState);
	void UpdateCoverFinder(int index);
	void UpdateCoverFinderActive(int index);

	// PURPOSE:	Enumerate the initialization/update states each bhCombatCoverfinder
	//			we're testing can be in.
	// NOTES:	The order is not arbitrary and is used for transitions, so be careful
	//			if you make changes.
	enum
	{
		// Not created at all, m_CoverFinder == NULL.
		kStateNotCreated,

		// bhCombatCoverFinder created, but not within a Start()/End() call pair.
		kStateNotInitialized,

		// bhCombatCoverFinder::Start() has been called, but right now UpdateNoCover()
		// is being called, i.e. there are no active targets/threats.
		kStateNotActive,

		// Fully active and looking for cover against enemies.
		kStateActive,

		// Leave last, this indicates the number of states.
		kNumStates
	};

	int								m_Iteration;
	int								m_IterationMax;

	struct CoverFinderData
	{
		// PURPOSE:	The cover finder object.
		bhCombatCoverFinder*		m_CoverFinder;

		// PURPOSE:	The index of the first enemy (target/threat) in m_Enemies
		//			that this cover finder is considering.
		int							m_FirstEnemy;

		// PURPOSE:	The current initialization/update state of the object.
		int							m_State;

		// PURPOSE:	The current position of the cover finder.
		Vector3						m_Position;
	};

	// PURPOSE:	Array of cover finder objects.
	atArray<CoverFinderData>		m_CoverFinders;

	// PURPOSE:	This is essentially the "camera" or "player" position.
	Vector3							m_Position;

	// PURPOSE:	Array of enemies.
	atArray<Vector3>				m_Enemies;

	float							m_EnemySwitchProbability;

	float							m_StateSwitchProbability;

	// PURPOSE:	The random number generator we use. We keep our own so we can
	//			seed it and get consistent tests.
	mthRandom						m_RandGen;

	// PURPOSE:	Pointer to an owned "world", incl. navigation, cover,
	//			and physics data.
	aiCoverSampleWorld*				m_World;
};


qa_aiCoverFinderInitShutdown::qa_aiCoverFinderInitShutdown()
		: m_Iteration(-1)
		, m_IterationMax(-1)
		, m_EnemySwitchProbability(0.0f)
		, m_StateSwitchProbability(0.0f)
		, m_World(NULL)
{
}


qa_aiCoverFinderInitShutdown::~qa_aiCoverFinderInitShutdown()
{
	Assert(!m_World);
}


void qa_aiCoverFinderInitShutdown::Init(int numCoverFinders, int numEnemies, float testDist,
		float enemySwitchProb, float stateSwitchProb, int iterMax)
{
	// Initialize the parser. This used to be done by aiCoverSampleWorld, I think. /FF
	INIT_PARSER;

	// Copy some input parameters.
	m_EnemySwitchProbability = enemySwitchProb;
	m_StateSwitchProbability = stateSwitchProb;
	m_IterationMax = iterMax;

	// At least when running this qaitems.sln by itself, it had problems
	// finding the assets if this wasn't done.
	// TODO: Check if this is valid - hope it doesn't screw up other tests
	// in any way... I think I did see some physics QA code that did the same though.
	ASSET.SetPath(RAGE_ASSET_ROOT);

	// Initialize the random number generator.
	// TODO: Make the seed a parameter?
	m_RandGen.Reset(99);

	// Load the world.
	ASSET.PushFolder("$/sample_aicover/sample_coverfinder");
	Assert(!m_World);
	m_World = rage_new aiCoverSampleWorld;
	m_World->Init();
	ASSET.PopFolder();

	// Compute a reasonable starting position.
	m_World->GetCameraStartPos(m_Position);

	// Distribute the cover-finding actors.
	Assert(!m_CoverFinders.GetCount());
	m_CoverFinders.Reset();
	m_CoverFinders.Reserve(numCoverFinders);
	int i;
	for(i = 0; i < numCoverFinders; i++)
	{
		CoverFinderData &d = m_CoverFinders.Append();
		d.m_CoverFinder = NULL;
		d.m_State = kStateNotCreated;
		d.m_FirstEnemy = i;

		Vector3 r;
		r.x = m_RandGen.GetFloat()*testDist - 0.5f*testDist;
		r.y = m_RandGen.GetFloat()*testDist - 0.5f*testDist;
		r.z = m_RandGen.GetFloat()*testDist - 0.5f*testDist;
		d.m_Position.Add(m_Position, r);
	}

	// Randomly distribute enemies.
	Assert(!m_Enemies.GetCount());
	m_Enemies.Reset();
	m_Enemies.Reserve(numEnemies);
	for(i = 0; i < numEnemies; i++)
	{
		Vector3 &enemyPos = m_Enemies.Append();
		Vector3 r;
		r.x = m_RandGen.GetFloat()*testDist - 0.5f*testDist;
		r.y = m_RandGen.GetFloat()*testDist - 0.5f*testDist;
		r.z = m_RandGen.GetFloat()*testDist - 0.5f*testDist;
		enemyPos.Add(m_Position, r);
	}

	m_Iteration = 0;
}


void qa_aiCoverFinderInitShutdown::Shutdown()
{
	int i;
	for(i = 0; i < m_CoverFinders.GetCount(); i++)
	{
		// This should delete the bhCombatCoverFinder object.
		SetCoverFinderState(i, kStateNotCreated);
#if __ASSERT
		CoverFinderData &d = m_CoverFinders[i];
		Assert(d.m_State == kStateNotCreated);
		Assert(!d.m_CoverFinder);
#endif
	}
	m_CoverFinders.Reset();

	Assert(m_World);
	m_World->Shutdown();
	delete m_World;
	m_World = NULL;

	// Shut down the parser. This used to be done by aiCoverSampleWorld, I think. /FF
	SHUTDOWN_PARSER;
}


void qa_aiCoverFinderInitShutdown::Update(qaResult &result)
{
	TIME.Update();

	Assert(m_World);
	m_World->Update(m_Position);

	int i;
	for(i = 0; i < m_CoverFinders.GetCount(); i++)
	{
		UpdateCoverFinder(i);
	}

	// TODO: Randomly move enemy positions.
	// TODO: Randomly move shooter positions.

	if(++m_Iteration >= m_IterationMax)
	{
		TST_PASS;
	}
}


void qa_aiCoverFinderInitShutdown::SetCoverFinderState(int index, int newState)
{
	CoverFinderData &d = m_CoverFinders[index];

	// Loop until we are in the state we want to be in.
	while(d.m_State != newState)
	{
		if(newState < d.m_State)
		{
			// We want to go towards a "less initialized" state.

			switch(d.m_State)
			{
				case kStateNotCreated:
					Assert(0);
					break;
				case kStateNotInitialized:
					Assert(d.m_CoverFinder);
					delete d.m_CoverFinder;
					d.m_CoverFinder = NULL;
					d.m_State = kStateNotCreated;
					break;
				case kStateNotActive:
					Assert(d.m_CoverFinder);
					d.m_CoverFinder->End();
					d.m_State = kStateNotInitialized;
					break;
				case kStateActive:
					d.m_State = kStateNotActive;
					break;
				default:
					Assert(0);
					break;
			}
		}
		else
		{
			// We want to go towards a "more initialized" state.

			switch(d.m_State)
			{
				case kStateNotCreated:
					Assert(!d.m_CoverFinder);
					d.m_CoverFinder = rage_new bhCombatCoverFinder;
					d.m_State = kStateNotInitialized;
					break;
				case kStateNotInitialized:
					Assert(d.m_CoverFinder);
					d.m_CoverFinder->Start();
					d.m_State = kStateNotActive;
					break;
				case kStateNotActive:
					d.m_State = kStateActive;
					break;
				case kStateActive:
					Assert(0);
					break;
				default:
					Assert(0);
					break;
			}
		}
	}
}


void qa_aiCoverFinderInitShutdown::UpdateCoverFinder(int index)
{
	CoverFinderData &d = m_CoverFinders[index];

	// Consider switching to a new initialization/update state.
	int newState = d.m_State;
	if(m_RandGen.GetFloat() < m_StateSwitchProbability)
	{
		// Create a new state randomly. This means that we will effectively
		// test both single step transitions per frame, as well as doing larger
		// transitions.
		newState = m_RandGen.GetRanged(0, kNumStates - 1);
	}

	// Consider switching enemies. If it's done, it just advances the
	// window of enemies by one.
	if(m_RandGen.GetFloat() < m_EnemySwitchProbability)
	{
		if(++d.m_FirstEnemy >= m_Enemies.GetCount())
			d.m_FirstEnemy = 0;
	}

	// Switch to the new state, potentially. The transitions can include
	// allocating the object, calling Start(), calling End(), or
	// destroying the object.
	SetCoverFinderState(index, newState);

	// Update based on the current state.
	switch(d.m_State)
	{
		case kStateNotCreated:
		case kStateNotInitialized:
			break;
		case kStateNotActive:
			Assert(d.m_CoverFinder);
			d.m_CoverFinder->UpdateNoCover();
			break;
		case kStateActive:
			Assert(d.m_CoverFinder);
			UpdateCoverFinderActive(index);
			break;
		default:
			Assert(0);
			break;
	}
}


void qa_aiCoverFinderInitShutdown::UpdateCoverFinderActive(int index)
{
	CoverFinderData &d = m_CoverFinders[index];
	Assert(d.m_CoverFinder);
	Assert(d.m_State == kStateActive);

	const float maxCoverRange = 30.0f; // MAGIC

	// Note: most of the code here was adapted frlom sample_coverfinder.cpp. /FF

	// -- Set up a bhCombatCoverShooter object. This contains various
	//    parameters describing how large we are and how we hold a weapon.
	//    This object can be constructed on the stack like this, or you could
	//    let it be a member variable. Also, you could share it between multiple
	//    identical actors if you want to.
	bhCombatCoverShooter shooter(0.9f, 1.4f, 0.36f, 1.75f);		// MAGIC!

	// -- We need to use a cache for visibility. Currently, bhCoverManager
	//    contains one we can use.
	bhCombatCoverVisibilityCache &visCache = bhCoverManager::GetInstance().GetVisibilityCache();

	// -- Construct the bhCombatCoverContext object. This holds input data that
	//    the cover finder will need. It can (at least currently) be safely
	//    constructed on the stack like this, without allocating memory.
	bhCombatCoverContext coverContext(d.m_Position, maxCoverRange, shooter, visCache, 0.0f);

	// -- Information about the weapon that we are holding is not a part of the constructor.
	//    This allows actors to find cover (for protection) even if they don't hold a weapon.
	//    If this AI actor has a weapon, we call SetWeaponParameters() so that the cover
	//    finder can take that into account later.
	coverContext.SetWeaponParameters(1.0f, 50.0f);	// TEMP

	// -- If we're already actively using a cover location (moving towards it,
	//    hiding at it, or shooting from it), we tell the cover finder about that.
//	if(m_CoverLocationCurrentlyUsing.GetGuid().IsValid())
//		coverContext.SetCoverCurrentlyUsing(m_CoverLocationCurrentlyUsing);

	// -- Set up a variable for basically keeping track of how many targets we've got.
	float weightSum = 0.0f;

	// -- Obtain a reference to the bhCombatCoverTargets inside coverContext,
	//    so we don't need to look it up for each target.
	bhCombatCoverTargets &targets = coverContext.GetTargets();

	// -- This is a weight we will use for this target.
	float weight = 1.0f;

	int i;

	// The first enemy for this cover finder is used as the target to attack.
	targets.AddShootAt(m_Enemies[d.m_FirstEnemy], 0, weight);
	weightSum += weight;

	// Set up threats, i.e. enemies that we want to be protected from but
	// don't care about attacking ourselves right now. These are taken
	// within a window in the enemy array.
	for(i = 0; i < m_Enemies.GetCount() && i < bhCombatCoverTargets::kMaxToProtectFromPoints; i++)
	{
		int enemy = (d.m_FirstEnemy + i) % m_Enemies.GetCount();
		targets.AddProtectFrom(m_Enemies[enemy], 0, weight);
		weightSum += weight;
	}

	// -- Compute a threshold that will be used for what we consider a good
	//    cover location. In this case, we make it proportional to the total
	//    weight of targets/threats that we have added, so that if we add
	//    more threats/targets we require a higher score.
	const float weightSumThresholdFraction = 0.75f;	// MAGIC
	coverContext.SetNodeThreshold(weightSum*weightSumThresholdFraction);

	// Update the cover finder.
	d.m_CoverFinder->Update(coverContext);
}


QA_ITEM_FAMILY(qa_aiCoverFinderInitShutdown, (int numCoverFinders, int numEnemies, float testDist, float enemySwitchProb, float stateSwitchProb, int iterMax), (numCoverFinders, numEnemies, testDist, enemySwitchProb, stateSwitchProb, iterMax));
QA_ITEM(qa_aiCoverFinderInitShutdown, (100, 100, 50.0f, 0.2f, 0.1f, 10000), qaResult::PASS_OR_FAIL);

#else

int qa_aicover_dont_cause_linker_warning;

#endif

/* End of file qa_aicover/qa_coverfinderinitshutdown.cpp */
