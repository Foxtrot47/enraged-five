QA_ITEM_FAMILY(qa_aiCoverFinderInitShutdown, (int numCoverFinders, int numEnemies, float testDist, float enemySwitchProb, float stateSwitchProb, int iterMax), (numCoverFinders, numEnemies, testDist, enemySwitchProb, stateSwitchProb, iterMax));
QA_ITEM(qa_aiCoverFinderInitShutdown, (100, 100, 50.0f, 0.2f, 0.1f, 10000), qaResult::PASS_OR_FAIL);
