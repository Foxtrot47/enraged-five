Project RageFUI

RootDirectory ..\..\..\..\..\

IncludePath ..\..
IncludePath ..\..\..\..\..\rage\suite\src
IncludePath ..\..\..\..\..\rage\script\src
IncludePath ..\..\..\..\..\rage\base\src

Directory {
	..\..\fuicore
	..\..\fuicomponents
	..\..\fuiflash
}
