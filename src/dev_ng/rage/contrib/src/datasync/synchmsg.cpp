// 
// datasync/synchmsg.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synchmsg.h"
#include "synchserialize.h"
#include "synchobject.h"
#include "bank/bank.h"
#include "system/timer.h"

namespace rage {

#if __BANK

//Stats tracking data
NetSyncDataMsg::Stats NetSyncDataMsg::m_Stats;
sysTimer						MsgTime;

void NetSyncDataMsg::Stats::StatsItem::AddWidgets(bkBank &bk, const char *pName)
{
	bk.PushGroup(pName);
		bk.AddText("Current",	&m_iCurrent);
		bk.AddText("Max",		&m_iMax);
		bk.AddText("bps",		&m_iBPS);
		bk.AddText("Max bps",	&m_iMaxBPS);
	bk.PopGroup();
}

void NetSyncDataMsg::Stats::StatsItem::FrameMarker()
{
	m_iMax = Max(m_iMax, m_iCurrent);
	m_Records[m_iFrame] = m_iCurrent;
	m_fFrameTime[m_iFrame] = MsgTime.GetTime();

	//Take current average
	float fTimeMin=FLT_MAX;
	float fTimeMax=0.0f;
	for (int i=0 ; i<kNumStatsFrames ; i++)
	{
		float fStatsFrameTime = m_fFrameTime[i];
		if (fStatsFrameTime > 0.0f)
		{
			fTimeMin = Min(fTimeMin, fStatsFrameTime);
			fTimeMax = Max(fTimeMax, fStatsFrameTime);
			m_iBPS += m_Records[i];
		}
	}

	float fElapsedTime = fTimeMax - fTimeMin;
	if (fElapsedTime > 0.0f)
	{
		float fInvTime = 1.0f / fElapsedTime;
		m_iBPS =	(int)(((float)m_iBPS) * fInvTime);
		m_iMaxBPS = Max(m_iBPS, m_iMaxBPS);
	}

	++m_iFrame;
	if (m_iFrame >= kNumStatsFrames)
	{
		m_iFrame = 0;
	}

	m_iCurrent = 0;
}

void NetSyncDataMsg::Stats::AddWidgets(bkBank &bk)
{
	bk.PushGroup("DataSyncMsg");
	m_SentReliable.AddWidgets(bk, "Outgoing reliable");
	m_SentUnReliable.AddWidgets(bk, "Outgoing UNReliable");
	m_Received.AddWidgets(bk, "Incoming");
	bk.PopGroup();
}

void NetSyncDataMsg::AddTypeWidgets(bkBank &bk)
{
	m_Stats.AddWidgets(bk);
}

void NetSyncDataMsg::StatsFrameMarker()
{
	m_Stats.FrameMarker();
}

#endif

void NetSyncDataMsg::ReadData(datImportBuffer &rBuff, bool bIsReliable) const
{
	NSOInstance *pReciever = OnGetReciever( m_iObjectId );
	if (pReciever)
	{
		NSEReadSerializer readBuffer(rBuff, (u8)(bIsReliable ? SendMode::eReliable : SendMode::eUnReliable));
		if (m_bHasTimeStamp)
		{
			readBuffer.SetDataTimeStamp(m_fNetTime);
		}
		else
		{
			readBuffer.SetDataTimeStamp( OnGetNetTime() );
		}

		pReciever->Import( readBuffer );

		BANK_ONLY(m_Stats.m_Received.m_iCurrent += rBuff.GetBitLength());
	}
}

void NetSyncDataMsg::SendMessage(NSOInstance &rSyncData)
{
	AssertMsg(!mp_ExpBuffer , "Use of SendMessage conflicts with user setting up the export buffers them selves");

	//Send reliable data if needed (and on a time stamp)
	if (rSyncData.SendReliable())
	{
		datExportBuffer reliable;
		const int kMaxReliableBits = 1024;
		char objBufferReliable[kMaxReliableBits];
		reliable.SetReadWriteBits(objBufferReliable, sizeof(objBufferReliable), 0);

		//Write a flag stating that we are a reliable buffer
		///reliable.WriteBool(true);

		//Export the data into each buffer in our single pass.
		NSEWriteSerializer writeBuffer(reliable, SendMode::eReliable);
		writeBuffer.SetDataTimeStamp(m_fNetTime);

		rSyncData.Export( writeBuffer );
		m_bHasTimeStamp |= writeBuffer.RequiresTimeStamp();

		//Read guard flag at base of buffer
		bool bBaseBit = false;
		reliable.PeekBool(bBaseBit, 0);
		if (bBaseBit)
		{
			//Need to construct a reliable sync message with this data
			mp_ExpBuffer = &reliable;
			BANK_ONLY(m_Stats.m_SentReliable.m_iCurrent += reliable.GetBitLength());
			SetReliableInfo(true, rSyncData.GetPendingReliableSeq());
			OnSendMsg();
		}
		ASSERT_ONLY(else {Assert(reliable.GetCursorPos() == 1);	})		//No data so the cursor should be set beyond a single guard bit

		//Let the object know we've tried to send reliable data
		rSyncData.OnSentReliable();
	}

	if (rSyncData.IsDirty())
	{
		datExportBuffer unreliable;
		const int kMaxUnreliableBits = 2048;
		char objBufferUNReliable[kMaxUnreliableBits];
		unreliable.SetReadWriteBits(objBufferUNReliable, sizeof(objBufferUNReliable), 0);

		//Write a flag stating that we are not a reliable buffer
		//unreliable.WriteBool(false);

		//Export the data into each buffer in our unreliable pass.
		NSEWriteSerializer writeBuffer(unreliable, SendMode::eUnReliable);
		writeBuffer.SetDataTimeStamp(m_fNetTime);

		rSyncData.Export( writeBuffer );
		m_bHasTimeStamp |= writeBuffer.RequiresTimeStamp();

		bool bBaseBit = false;
		unreliable.PeekBool(bBaseBit, 0);
		if (bBaseBit)
		{
			mp_ExpBuffer = &unreliable;
			BANK_ONLY(m_Stats.m_SentUnReliable.m_iCurrent += unreliable.GetBitLength());
			OnSendMsg();

			//Read back data test! //TMS: At time of writing I aint actually got dis workin' but its a nice idea :(
#if NSE_TRACE_ON
			NSE_TRACE_VERIFY("VERIFY");
			NSEVerifySerializer readBuffer(unreliable);
			rSyncData.Import( readBuffer );
#endif
		}
		ASSERT_ONLY(else {Assert(unreliable.GetCursorPos() == 1);	});	//No data so the cursor should be set beyond a single guard bit}
	}

	mp_ExpBuffer = 0;
}

} // namespace rage
