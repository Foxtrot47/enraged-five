// 
// datasync/synchutil.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synchserialize.h"

namespace rage {

void NSEBufferSerializer::GuardedSection::Place()
{
	Assert(!m_bPlaced);
	m_bPlaced = true;

	//Putting a marker bit for the whole section bit down so we can fill it out later..
	//Note that we need to put down markers for both reliable and unreliable channels as 
	//different elements will have been written for both
	bool bGuardFlagGuess = false;
	m_iStartCursor = mp_Buffer->GetCurrentOffset();
	m_iStartElems = mp_Buffer->GetWrittenElementCount();
	mp_Buffer->SerGuard(bGuardFlagGuess);
}

bool NSEBufferSerializer::GuardedSection::Finalize()
{
	bool bRet = false;
	Assert(m_bPlaced);
	m_bPlaced = false;

	int iEndPos = mp_Buffer->GetCurrentOffset();
	int iEndElem = mp_Buffer->GetWrittenElementCount();
	bool bGuardFlag = iEndElem > m_iStartElems;
	mp_Buffer->SetCurrentOffset(m_iStartCursor);
	mp_Buffer->SerGuard(bGuardFlag);

	//We can leave the cursor back at the groups beginning if all we wrote was a guard flag!
	if (bGuardFlag)
	{
		bRet = true;
		mp_Buffer->SetCurrentOffset(iEndPos);
	}
	return bRet;
}

bool NSEBufferSerializer::GuardedSection::StartImport()
{
	bool bGuardFlag = false;
	mp_Buffer->SerGuard(bGuardFlag);//Mode currently ignored...
	return bGuardFlag;

	//TMS: IF we read multiple data packets we'll need something here...
	//bool bGuardFlags[2]={false,false};
	//for (int i=0 ; i<2 ; i++)
	//{
	//	SendMode::Mode mode = (SendMode::Mode)i;
	//	if (!mp_Buffer->IsSupressed(mode))
	//	{
	//		mp_Buffer->SerGuard(bGuardFlags[i], mode);
	//	}
	//}

	////We'll maintain a stack of suppression so we can resume
	////normal behavior at the correct point.
	//mp_Buffer->PushSupressed(!bGuardFlags[0], !bGuardFlags[1]);

	////If either mode type is open then we need to parse more data
	//return bGuardFlags[0] && bGuardFlags[1];
}

void NSEBufferSerializer::GuardedSection::EndImport()
{
	//TMS: IF we read multiple data packets we'll need something here...
	//We'll maintain a stack of suppression so we can resume
	//normal behavior at the correct point.
	//mp_Buffer->PopSupressed();
}

void NSESerializer::QuantizeVector(const Vector3 &v, float fMin, float fMax, int xBits, int yBits, int zBits, NSESerializer& rBuffer)
{
	u32 iTmpVal;
	iTmpVal = Quantizers::Quantize<u32>(v.x, fMin, fMax, xBits);
	rBuffer.SerValue(iTmpVal, xBits);
	iTmpVal = Quantizers::Quantize<u32>(v.y, fMin, fMax, yBits);
	rBuffer.SerValue(iTmpVal, yBits);
	iTmpVal = Quantizers::Quantize<u32>(v.z, fMin, fMax, zBits);
	rBuffer.SerValue(iTmpVal, zBits);
}

void NSESerializer::DeQuantizeVector(Vector3 &v, float fMin, float fMax, int xBits, int yBits, int zBits,NSESerializer& rBuffer)
{
	u32 iTmpVal;
	rBuffer.SerValue(iTmpVal, xBits);
	v.x = Quantizers::DeQuantize(iTmpVal, fMin, fMax, xBits);
	rBuffer.SerValue(iTmpVal, yBits);
	v.y = Quantizers::DeQuantize(iTmpVal, fMin, fMax, yBits);
	rBuffer.SerValue(iTmpVal, zBits);
	v.z = Quantizers::DeQuantize(iTmpVal, fMin, fMax, zBits);
}

void NSESerializer::QuantizeQuaternion(const Quaternion &v, int xBits, int yBits, int zBits, int wBits, NSESerializer& rBuffer)
{
	u32 iTmpVal;
	iTmpVal = Quantizers::Quantize<u32>(v.w, -2.0f*PI, 2.0f*PI, wBits);
	rBuffer.SerValue(iTmpVal, wBits);
	Vector3 vAxis;
	v.GetDirection(vAxis);
	QuantizeVector(vAxis, -1.0f , 1.0f, xBits, yBits, zBits, rBuffer);
}

void NSESerializer::DeQuantizeQuaternion(Quaternion &v, int xBits, int yBits, int zBits, int wBits, NSESerializer& rBuffer)
{
	u32 iTmpVal;
	rBuffer.SerValue(iTmpVal, wBits);
	float fAng = Quantizers::DeQuantize(iTmpVal, -2.0f*PI, 2.0f*PI, wBits);
	Vector3 vAxis;
	DeQuantizeVector(vAxis, -1.0f , 1.0f, xBits, yBits, zBits, rBuffer);
	v.Set(vAxis.x, vAxis.y, vAxis.z, fAng);
}

} // namespace rage


