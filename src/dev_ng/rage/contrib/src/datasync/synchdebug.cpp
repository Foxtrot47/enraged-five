// 
// datasync/synchdebug.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synchdebug.h"
#include "string/string.h"

namespace rage {

#if NSE_TRACE_ON
bool NSETraceOut::sm_bTraceOn=false;
int NSETraceOut::sm_TraceDepth=0;
int NSETraceOut::sm_TraceCount=0;
bool NSETraceIn::sm_bTraceOn=false;
int NSETraceIn::sm_TraceDepth=0;
int NSETraceIn::sm_TraceCount=0;

NSETraceBase::NSETraceBase(const char *pPrefix, const char *pLabel, int iDepth, int iCount, bool bTraceOn)
{
	if (bTraceOn)
	{
		char prefix[128]={0};
		if (iDepth)
		{
			for (int i=0; i<iDepth*5 ; i++)
			{
				if (i%5)
				{
					prefix[i] = ' ';
				}
				else
				{
					prefix[i] = '|';
				}
			}
			prefix[iDepth*5]=0;
		}
		else
		{
			formatf(prefix,sizeof(prefix),"%s[%d]: ", pPrefix, iCount);
		}
		
		char buffer[512];
		formatf(buffer, sizeof(buffer), "%s%s", prefix, pLabel);
		Displayf(buffer);
	}
}

NSETraceOut::NSETraceOut(const char *pPrefix, const char *pLabel)
	:NSETraceBase(pPrefix, pLabel, sm_TraceDepth, sm_TraceCount, sm_bTraceOn)
{
	if (!sm_TraceDepth)
	{
		++sm_TraceCount;
	}
	++sm_TraceDepth;
}

NSETraceOut::~NSETraceOut()
{	--sm_TraceDepth;	}

NSETraceIn::NSETraceIn(const char *pPrefix, const char *pLabel)
	:NSETraceBase(pPrefix, pLabel, sm_TraceDepth, sm_TraceCount, sm_bTraceOn)
{	
	if (!sm_TraceDepth)
	{
		++sm_TraceCount;
	}
	++sm_TraceDepth;
}

NSETraceIn::~NSETraceIn()
{	--sm_TraceDepth;	}

#endif
} // namespace rage


