// 
// datasync/synchrules.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DATASYNC_SYNCHRULES_H 
#define DATASYNC_SYNCHRULES_H 

#include "synchobject.h"
#include "math/amath.h"
#include "math/simplemath.h"
#include "math/float16.h"
#include "vector/vector3.h"

namespace rage {

template<typename T>
inline bool ValidateValue(T )
{
	return true;
}

inline bool ValidateValue(float val)
{
	return FPIsFinite(val);
}

template<typename T>
inline void ValidateRange(T ASSERT_ONLY(value), T ASSERT_ONLY(min), T ASSERT_ONLY(max))
{
	Assertf(InRange(value, min, max), "Value %d out of range <%d, %d>", value, min, max);
}

template<>
inline void ValidateRange<float>(float ASSERT_ONLY(value), float ASSERT_ONLY(min), float ASSERT_ONLY(max))
{
	Assertf(InRange(value, min, max), "Value %f out of range <%f, %f>", value, min, max);
}

//-------------------------------------------------------------------------------------------------
//Common Serialization options
//-------------------------------------------------------------------------------------------------
//Default serialization with no quantization

template<typename T>
static inline void BasicPrint(T iVal, char *pBuffer, int iSizeBuffer)
{
	formatf(pBuffer, iSizeBuffer, "%d", iVal);
}

static inline void BasicPrint(float fVal, char *pBuffer, int iSizeBuffer)
{
	formatf(pBuffer, iSizeBuffer, "%f", fVal);
}

template<typename T>
struct SimpleSerialize
{
	void Print(const T &rValue, char *pBuffer, int iSizeBuffer)
	{
		BasicPrint(rValue, pBuffer, iSizeBuffer);
	}

	void Export(T &value, const T &/*objectvalue*/, NSESerializer& rBuffer)
	{
		Assert(ValidateValue(value));
		rBuffer.SerValue(value);
	}
	void Import(T &value, const T &/*objectvalue*/, NSESerializer& rBuffer)
	{
		rBuffer.SerValue(value);
		//Assert(ValidateValue(value));
	}
};

template<>
struct SimpleSerialize<Vector3>
{
	void Print(const Vector3 &rValue, char *pBuffer, int iSizeBuffer)
	{
		formatf(pBuffer, iSizeBuffer, "<%f,%f,%f>", rValue.x, rValue.y, rValue.z);
	}

	void Export(Vector3 &value, const Vector3 &/*objectvalue*/, NSESerializer& rBuffer)
	{
		Assert(ValidateValue(value));
		rBuffer.SerValue(value);
	}
	void Import(Vector3 &value, const Vector3 &/*objectvalue*/, NSESerializer& rBuffer)
	{
		rBuffer.SerValue(value);
		Assert(ValidateValue(value));
	}
};

//Used to quantize 16 bit float values
template<typename T>
struct SmallFloat16
{
	void Print(const T &rValue, char *pBuffer, int iSizeBuffer)
	{
		BasicPrint(rValue, pBuffer, iSizeBuffer);
	}

	void Export(T &valueIn, const T &/*objectvalue*/, NSESerializer& rBuffer)
	{ 
		Float16 fSmaller(valueIn);
		u16 bits = fSmaller.GetBinaryData();
		rBuffer.SerValue(bits);
	}

	void Import(T &value, const T &/*objectvalue*/, NSESerializer& rBuffer)
	{
		u16 bits = 0;
		rBuffer.SerValue(bits);
		Float16 fSmaller;
		fSmaller.SetBinaryData(bits);
		value = fSmaller.GetFloat32_FromFloat16();
	}
};

template<>
struct SmallFloat16<Vector3>
{
	SmallFloat16<float> m_SmallFloat;
	void Print(const Vector3 &rValue, char *pBuffer, int iSizeBuffer)
	{
		formatf(pBuffer, iSizeBuffer, "<%f,%f,%f>", rValue.x, rValue.y, rValue.z);
	}

	void Export(Vector3 &valueIn, const Vector3 &objectvalue, NSESerializer& rBuffer)
	{ 
		m_SmallFloat.Export(valueIn.x, objectvalue.x, rBuffer);
		m_SmallFloat.Export(valueIn.y, objectvalue.y, rBuffer);
		m_SmallFloat.Export(valueIn.z, objectvalue.z, rBuffer);
	}

	void Import(Vector3 &value, const Vector3 &objectvalue, NSESerializer& rBuffer)
	{
		m_SmallFloat.Import(value.x, objectvalue.x, rBuffer);
		m_SmallFloat.Import(value.y, objectvalue.y, rBuffer);
		m_SmallFloat.Import(value.z, objectvalue.z, rBuffer);
	}
};


//While we might want to quantize some values (ints, floats etc) to get better compression
template<typename T>
struct QuantizeInRange
{
	void Print(const T &rValue, char *pBuffer, int iSizeBuffer)
	{
		BasicPrint(rValue, pBuffer, iSizeBuffer);
	}

	QuantizeInRange(T tMin, T tMax, u16 iBitsOrSteps, bool bIsBits=true)
		:m_Min(tMin)
		,m_Max(tMax)
	{
		if(!bIsBits)
		{
			u8 bits;
			Assign(bits, iBitsOrSteps);
			Steps(m_Min, m_Max, bits);
		}
		else
		{
			Assign(m_iNumBits, iBitsOrSteps);
		}
	}

	QuantizeInRange()
	{}

	QuantizeInRange& Bits(T min, T max, u8 iNumBits)
	{
		m_Min = min;
		m_Max = max;
		m_iNumBits = iNumBits;
		m_Scale = 1 << iNumBits;
		return *this;
	}

	QuantizeInRange& Steps(T min, T max, u16 steps)
	{
		m_Min = min;
		m_Max = max;
		m_Scale = steps;
		m_iNumBits = 1;
		int iCount =1;
		while (iCount < steps)
		{
			iCount<<=1;
			++m_iNumBits;
		}
		return *this;
	}

	T m_Min;
	T m_Max;
	u16 m_Scale;
	u8 m_iNumBits;
	void Export(T &valueIn, const T &/*objectvalue*/, NSESerializer& rBuffer)
	{ 
		ValidateRange(valueIn, m_Min, m_Max);
		T value = Clamp(valueIn, m_Min, m_Max);
		value -= m_Min;
		value *= (T)((1<<m_iNumBits)-1);
		value /= (m_Max - m_Min);
		u32 iVal = (int)value;
		rBuffer.SerValue(iVal, m_iNumBits);
	}

	void Import(T &value, const T &/*objectvalue*/, NSESerializer& rBuffer)
	{
		u32 in;
		rBuffer.SerValue(in, m_iNumBits);
		value = (T)in;
		value *= (m_Max - m_Min);
		value /= (T)((1<<m_iNumBits)-1);
		value += m_Min;

		//Right now we do a fake import to auto calculate item sizes during type
		//data initialisation. This uses uninitialised data and trips the following assertions!
		//Assert(ValidateValue(value));
		//Assert(InRange(value, m_Min, m_Max));
	}
};

template<>
struct QuantizeInRange<Vector3>
{
	void Print(const Vector3 &rValue, char *pBuffer, int iSizeBuffer)
	{
		formatf(pBuffer, iSizeBuffer, "<%f,%f,%f>", rValue.x, rValue.y, rValue.z);
	}

	QuantizeInRange<float> m_FloatQuant;
	
	QuantizeInRange(float tMin, float tMax, u16 iBitsOrSteps, bool bIsBits=true)
		:m_FloatQuant(tMin, tMax, iBitsOrSteps, bIsBits)
	{
	}

	QuantizeInRange<Vector3>()
	{}

	QuantizeInRange<Vector3>& Bits(float min, float max, u8 iNumBits)
	{
		m_FloatQuant.Bits(min, max, iNumBits);
		return *this;
	}

	QuantizeInRange<Vector3>& Steps(float min, float max, u16 steps)
	{
		m_FloatQuant.Steps(min, max, steps);
		return *this;
	}

	void Export(Vector3 &value, const Vector3 &objectvalue, NSESerializer& rBuffer)
	{
		m_FloatQuant.Export(value.x, objectvalue.x, rBuffer);
		m_FloatQuant.Export(value.y, objectvalue.y, rBuffer);
		m_FloatQuant.Export(value.z, objectvalue.z, rBuffer);
	}

	void Import(Vector3 &value, const Vector3 &objectvalue, NSESerializer& rBuffer)
	{
		m_FloatQuant.Import(value.x, objectvalue.x, rBuffer);
		m_FloatQuant.Import(value.y, objectvalue.y, rBuffer);
		m_FloatQuant.Import(value.z, objectvalue.z, rBuffer);
	}
};


template<>
struct QuantizeInRange<Vector2>
{
	void Print(const Vector2 &rValue, char *pBuffer, int iSizeBuffer)
	{
		formatf(pBuffer, iSizeBuffer, "<%f,%f>", rValue.x, rValue.y);
	}

	QuantizeInRange<float> m_FloatQuant;
	
	QuantizeInRange(float tMin, float tMax, u16 iBitsOrSteps, bool bIsBits=true)
		:m_FloatQuant(tMin, tMax, iBitsOrSteps, bIsBits)
	{
	}

	QuantizeInRange<Vector2>()
	{}

	QuantizeInRange<Vector2>& Bits(float min, float max, u8 iNumBits)
	{
		m_FloatQuant.Bits(min, max, iNumBits);
		return *this;
	}

	QuantizeInRange<Vector2>& Steps(float min, float max, u16 steps)
	{
		m_FloatQuant.Steps(min, max, steps);
		return *this;
	}

	void Export(Vector2 &value, const Vector2 &objectvalue, NSESerializer& rBuffer)
	{
		m_FloatQuant.Export(value.x, objectvalue.x, rBuffer);
		m_FloatQuant.Export(value.y, objectvalue.y, rBuffer);
	}

	void Import(Vector2 &value, const Vector2 &objectvalue, NSESerializer& rBuffer)
	{
		m_FloatQuant.Import(value.x, objectvalue.x, rBuffer);
		m_FloatQuant.Import(value.y, objectvalue.y, rBuffer);
	}
};

//-------------------------------------------------------------------------------------------------



//-------------------------------------------------------------------------------------------------
//Errors rules
//-------------------------------------------------------------------------------------------------
struct ErrorRulesBase
{
	void AddWidgets(bkBank&){}
};

template<typename T>
struct NoErrorRule : public ErrorRulesBase
{
	struct InstanceData{InstanceData(NSEContext &){}};
	void OnSend(NSEInstanceBase &, InstanceData &, float){}
	bool Update(NSEInstanceBase &, InstanceData &, const T &, const T &, NSEContext &){return false;}
	NoErrorRule()
	{}
};

//Send anytime the data has changed
template<typename T>
struct OnAnyChange : public ErrorRulesBase
{
	struct InstanceData{InstanceData(NSEContext &){}};
	void OnSend(NSEInstanceBase &, InstanceData &, float){}
	bool Update(NSEInstanceBase &, InstanceData &, const T &shadow, const T &value, NSEContext &)
	{
		if (shadow != value)
		{
			return true;
		}
		return false;
	}

	//c'tor
	OnAnyChange()
	{}
};

//Send when the discrepency to the last value exceeds some tolerance
template<typename T>
struct OnDelta : public ErrorRulesBase
{
	float m_Delta;
	struct InstanceData{InstanceData(NSEContext &){}};
	
	float Delta(const Vector3& rA, const Vector3& rB)
	{
		return rA.Dist( rB );
	}
	float Delta(const float rA, const float rB)
	{
		return Abs( rA - rB );
	}
	float Delta(const int rA, const int rB)
	{
		return (float)Abs( rA - rB );
	}

	void OnSend(NSEInstanceBase &, InstanceData &, float){}
	bool Update(NSEInstanceBase &rData, InstanceData &, const T &shadow, const T &value, NSEContext &)
	{
		float fCurDelta = Delta(shadow, value);
		if (fCurDelta > m_Delta)
		{
			return true;
		}
		rData.SetNearlyDirty(fCurDelta > m_Delta*0.8f);
		return false;
	}

	//c'tor
	OnDelta(float delta)
		:m_Delta(delta)
	{}

	OnDelta()
	{}
};
//-------------------------------------------------------------------------------------------------



//-------------------------------------------------------------------------------------------------
//Send prioritised
//-------------------------------------------------------------------------------------------------
//template<typename T>
//struct PriorityWithDelta
//{
//	float m_fDeltaToPriorityScale;
//	struct InstanceData : public NSEPrioritized {
//		InstanceData(NSEContext &rCtxt):NSEPrioritized(rCtxt){}
//	};
//	void OnSend(InstanceData &){NSEPrioritized::OnSend()}
//
//	//The update function updates the queued priority - external code actually marks the data for sending
//	bool Update(NSEInstanceBase &rData, InstanceData &rInstData, const T &shadow, const T &value, NSEContext &)
//	{
//		//Update the prioritization queue data
//		rInstData.m_fPriority = fabs((float)(shadow - value)) * m_fDeltaToPriorityScale;
//
//		//We're not sending the value here - it needs to be balanced with other values
//		return false;
//	}
//
//	//C'tor
//	PriorityWithDelta(float fDeltaToPriority)
//		:m_fDeltaToPriorityScale(fDeltaToPriority)
//	{	}
//};
//
////Accumulate priority with delta
//template<typename T>
//struct AccumulatePriorityWithDelta
//{
//	float m_fDeltaToPriorityScale;
//	struct InstanceData : public NSEPrioritized
//	{
//		InstanceData(NSEContext &rCtxt):NSEPrioritized(rCtxt){}
//	};
//
//	void SetPriorityScale(float fScale)
//	{
//		m_fDeltaToPriorityScale = fScale;
//	}
//
//	//When sending data reset the accumulation
//	void OnSend(NSEInstanceBase &, InstanceData &/*rData*/)
//	{
//	}
//
//	//The update function updates the queued priority - external code actually marks the data for sending
//	bool Update(InstanceData &rInstanceData, const T &shadow, const T &value, NSEContext &)
//	{
//		//Accumulate current delta
//		rInstanceData.m_fPriority += fabs((float)(shadow - value)) * m_fDeltaToPriorityScale;
//		return false;
//	}
//
//	AccumulatePriorityWithDelta()
//		:m_fDeltaToPriorityScale(0.0f)
//	{	}
//
//	//C'tor
//	AccumulatePriorityWithDelta(float fDeltaToPriority)
//		:m_fDeltaToPriorityScale(fDeltaToPriority)
//	{	}
//};
//-------------------------------------------------------------------------------------------------



//-------------------------------------------------------------------------------------------------
//Simple scheduling
//-------------------------------------------------------------------------------------------------
struct ScheduleBase
{
	void AddWidgets(bkBank &){}
};

struct NoScheduledUpdate : public ScheduleBase
{
	struct InstanceData{InstanceData(NSEContext&){}};

	void OnSend(NSEInstanceBase &, InstanceData &, float){}

	bool CanUpdate(InstanceData&) const {return true;}

	bool NeedsUpdate(NSEInstanceBase &, InstanceData &/*rData*/, float /*fDataStampTime*/) const
	{
		return false;
	}

	bool AllowsUpdate(InstanceData&)
	{
		return true;
	}
};

struct UpdateOnHeartbeat : public ScheduleBase
{
	struct InstanceData
	{
		float m_fLastSentTime;
		InstanceData(NSEContext&):m_fLastSentTime(0.0f){}
	};

	float m_fHeartbeat;

	void SetHeartbeat(float fHeartBeat)
	{	m_fHeartbeat = fHeartBeat;	}
	void OnSend(NSEInstanceBase &, InstanceData &rData, float);
	bool CanUpdate(InstanceData&) const {return true;}
	bool NeedsUpdate(NSEInstanceBase &, InstanceData &rData, float fDataStampTime) const;
	BANK_ONLY(void AddWidgets(bkBank &));

	bool AllowsUpdate(InstanceData&)
	{
		return true;
	}

	UpdateOnHeartbeat()
		:m_fHeartbeat(0.0f)
	{	}
	UpdateOnHeartbeat(float fHeartbeat)
		:m_fHeartbeat(fHeartbeat)
	{	}
};
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
template<typename _DataType, typename ErrorRule, typename ErrorRuleInstance>
struct SimpleError
{
	typedef TNSESyncType <
				_DataType,
				ErrorRule, ErrorRuleInstance,
				NoScheduledUpdate, NoScheduledUpdate::InstanceData
			> TElemTypeBase;

	struct ElemType :	public TElemTypeBase
	{
		ElemType(float fPriScale)
			:TElemTypeBase(ErrorRule(fPriScale), NoScheduledUpdate())
		{	}
	protected:
		ElemType()
			:TElemTypeBase(ErrorRule(), NoScheduledUpdate())
		{	}
	};
};

//template<typename _DataType>
//struct AcccumulateError : public SimpleError<_DataType, AccumulatePriorityWithDelta<_DataType>, typename AccumulatePriorityWithDelta<_DataType>::InstanceData>
//{
//};
//
//template<typename _DataType>
//struct DeltaError : public SimpleError<_DataType, PriorityWithDelta<_DataType>, typename PriorityWithDelta<_DataType>::InstanceData>
//{
//};


template<typename _DataType>
struct OnDeltaElemBase
{
	typedef TNSESyncType <	_DataType,
		OnDelta<_DataType>, typename OnDelta<_DataType>::InstanceData,
		NoScheduledUpdate, NoScheduledUpdate::InstanceData > TElemTypeBase;

	struct ElemType :	public TElemTypeBase
	{
		ElemType(float fDelta)
			:TElemTypeBase(OnDelta<_DataType>(fDelta), NoScheduledUpdate())
		{	}
	protected:
		ElemType()
			:TElemTypeBase(OnDelta<_DataType>(), NoScheduledUpdate())
		{	}
	};
};

template<typename _DataType>
struct OnAnyChangeElemBase
{
	typedef TNSESyncType <	_DataType,
		OnAnyChange<_DataType>, typename OnAnyChange<_DataType>::InstanceData,
		NoScheduledUpdate, NoScheduledUpdate::InstanceData > TElemTypeBase;

	struct ElemType :	public TElemTypeBase
	{
		ElemType()
			:TElemTypeBase(OnAnyChange<_DataType>(), NoScheduledUpdate())
		{	}
	};
};

template<typename _DataType>
struct OnSneakElemBase
{
	typedef TNSESyncType <
			_DataType,
			NoErrorRule<_DataType>, typename NoErrorRule<_DataType>::InstanceData, 
			NoScheduledUpdate, NoScheduledUpdate::InstanceData > TElemTypeBase;
	
	struct ElemType : public TElemTypeBase
	{
		ElemType()
			:TElemTypeBase(NoErrorRule<_DataType>(), NoScheduledUpdate())
		{	}
	};
};


template<typename _DataType>
struct OnHeartbeatElemBase
{
	typedef TNSESyncType <
			_DataType,
			NoErrorRule<_DataType>, typename NoErrorRule<_DataType>::InstanceData, 
			UpdateOnHeartbeat, UpdateOnHeartbeat::InstanceData > TElemTypeBase;
	
	struct ElemType : public TElemTypeBase
	{
		ElemType(float fHeartbeat)
			:TElemTypeBase(NoErrorRule<_DataType>(), UpdateOnHeartbeat(fHeartbeat))
		{	}
	protected:
		ElemType()
			:TElemTypeBase(NoErrorRule<_DataType>(), UpdateOnHeartbeat())
		{	}
	};
};

template<typename _DataType>
struct OnHeartbeatAndChangeElemBase
{
	typedef TNSESyncType <
			_DataType,
			OnAnyChange<_DataType>, typename OnAnyChange<_DataType>::InstanceData, 
			UpdateOnHeartbeat, UpdateOnHeartbeat::InstanceData > TElemTypeBase;
	
	struct ElemType : public TElemTypeBase
	{
		ElemType(float fHeartbeat)
			:TElemTypeBase(OnAnyChange<_DataType>(), UpdateOnHeartbeat(fHeartbeat))
		{	}
	protected:
		ElemType()
			:TElemTypeBase(OnAnyChange<_DataType>(), UpdateOnHeartbeat())
		{	}
	};
};

template<typename _DataType>
struct OnHeartbeatAndDeltaElemBase
{
	typedef TNSESyncType <
			_DataType,
			OnDelta<_DataType>, typename OnDelta<_DataType>::InstanceData, 
			UpdateOnHeartbeat, UpdateOnHeartbeat::InstanceData > TElemTypeBase;
	
	struct ElemType : public TElemTypeBase
	{
		ElemType(float fDelta, float fHeartbeat)
			:TElemTypeBase(OnDelta<_DataType>(fDelta), UpdateOnHeartbeat(fHeartbeat))
		{	}
	protected:
		ElemType()
			:TElemTypeBase(OnDelta<_DataType>(), UpdateOnHeartbeat())
		{	}
	};
};


template<typename _DataType>
struct NoRulesElemBase
{
	typedef TNSESyncType <
		_DataType,
		NoErrorRule<_DataType>, typename NoErrorRule<_DataType>::InstanceData, 
		NoScheduledUpdate, NoScheduledUpdate::InstanceData > TElemTypeBase;

	struct ElemType : public TElemTypeBase
	{
		ElemType()
			:TElemTypeBase(NoErrorRule<_DataType>(), NoScheduledUpdate())
		{	}
	};
};

//-------------------------------------------------------------------------------------------------
//The Rules namespace contains prepackaged rules that clients can easily use without needing
//to worry about template complications.
//-------------------------------------------------------------------------------------------------
namespace Rules {
	namespace AnyChange
	{
		typedef OnAnyChangeElemBase<float>::ElemType	Float;
		typedef OnAnyChangeElemBase<int>::ElemType		Int;
	}

	namespace OnHeartBeat{
		typedef OnHeartbeatElemBase<float>::ElemType	Float;
		typedef OnHeartbeatElemBase<int>::ElemType		Int;
	}

	namespace OnDelta
	{
		typedef OnDeltaElemBase<Vector3>::ElemType	Vector3;
		typedef OnDeltaElemBase<float>::ElemType	Float;
		typedef OnDeltaElemBase<int>::ElemType		Int;
	}

	//This will never mark the data dirty - useful for testing and
	//perhaps some external management scheme I haven't thought of yet.
	namespace NoRule
	{
		typedef NoRulesElemBase<float>::ElemType	Float;
		typedef NoRulesElemBase<int>::ElemType	Int;
	}

	//namespace Prioritized
	//{
	//	namespace Accumulate
	//	{
	//		typedef AcccumulateError<float> Float;
	//		typedef AcccumulateError<int> Int;
	//	}

	//	namespace Delta
	//	{
	//		typedef DeltaError<float> Float;
	//		typedef DeltaError<int> Int;
	//	}
	//}
}

//-------------------------------------------------------------------------------------------------
//The Elems namespace contains predefined sync elements defining callback behavior and serialization
//-------------------------------------------------------------------------------------------------
namespace Elems
{
	namespace Float
	{
		namespace _Internal
		{
			struct Simple
			{
				typedef TSyncElemWrapper< float, SimpleSerialize<float> > Elem;
			};

			struct Quantized
			{
				typedef TSyncElemWrapper< float, QuantizeInRange<float> > Elem;
			};

			struct SmallFloat
			{
				typedef TSyncElemWrapper< float, SmallFloat16<float> > Elem;
			};
		}

		typedef _Internal::Simple::Elem Simple;
		typedef _Internal::Quantized::Elem Quantized;
		typedef _Internal::SmallFloat::Elem SmallFloat;
	}

	namespace Vector3
	{
		namespace _Internal
		{
			struct Simple
			{
				typedef TSyncElemWrapper< rage::Vector3, rage::SimpleSerialize<rage::Vector3> > Elem;
			};

			//struct Quantized
			//{
			//	typedef TSyncElemWrapper< Vector3, QuantizeInRange<Vector3> > Elem;
			//};

			//struct SmallFloat
			//{
			//	typedef TSyncElemWrapper< Vector3, SmallFloat16<Vector3> > Elem;
			//};
		}

		typedef _Internal::Simple::Elem Simple;
		//typedef _Internal::Quantized::Elem Quantized;
		//typedef _Internal::SmallFloat::Elem SmallFloat;
	}

	namespace Int
	{
		namespace _Internal
		{
			struct Simple
			{
				typedef TSyncElemWrapper< int, SimpleSerialize<float> > Elem;
			};

			struct Quantized
			{
				typedef TSyncElemWrapper< int, QuantizeInRange<float> > Elem;
			};
		}

		typedef _Internal::Simple::Elem		Simple;
		typedef _Internal::Quantized::Elem	Quantized;
	}
}

//-------------------------------------------------------------------------------------------------
//Quantizers contains some predefined quantizers that can be used easily elsewhere
//-------------------------------------------------------------------------------------------------
namespace Quantizers
{
	extern QuantizeInRange<float> FloatUnit8;
	extern QuantizeInRange<float> FloatUnit6;
	extern QuantizeInRange<float> FloatUnit4;
	extern QuantizeInRange<float> Float;
	extern QuantizeInRange<Vector3> V3;
	extern QuantizeInRange<Vector2> V2;
	extern QuantizeInRange<int> Int;
}
//-------------------------------------------------------------------------------------------------

} // namespace rage
#endif // DATASYNC_SYNCHRULES_H 
