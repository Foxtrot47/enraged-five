// 
// datasync/synchrules.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synchrules.h"
#include "bank/bank.h"

namespace rage {
	class Vector3;

namespace Quantizers
{
	QuantizeInRange<float> FloatUnit8(0.0f, 1.0f, 8);
	QuantizeInRange<float> FloatUnit6(0.0f, 1.0f, 6);
	QuantizeInRange<float> FloatUnit4(0.0f, 1.0f, 4);
	QuantizeInRange<float> Float(0.0f, 1.0f, 8);
	QuantizeInRange<Vector3> V3(0.0f, 1.0f, 8);
	QuantizeInRange<Vector2> V2(0.0f, 1.0f, 8);
	QuantizeInRange<int> Int(0,100,3);
}

void UpdateOnHeartbeat::OnSend(NSEInstanceBase &, InstanceData &rData, float fDataStampTime)
{
	rData.m_fLastSentTime = fDataStampTime;
}

bool UpdateOnHeartbeat::NeedsUpdate(NSEInstanceBase &rDataBase, InstanceData &rHBData, float fDataStampTime) const
{
	float fAge = fDataStampTime - rHBData.m_fLastSentTime;
	if (fAge < 0)
	{
		//Somehow we've gone backwards in time - can happen with network
		//time sync and session changes the trick is to now recover and
		//try and make sure we start counting again.
		rHBData.m_fLastSentTime = fDataStampTime;
		return false;
	}

	if (fAge > m_fHeartbeat)
	{
		return true;
	}

	if (fAge > m_fHeartbeat * 0.8f)
	{
		rDataBase.SetNearlyDirty( true );
	}
	return false;
}

#if __BANK
void UpdateOnHeartbeat::AddWidgets(bkBank &bk)
{
	bk.AddSlider("Heartbeat", &m_fHeartbeat, 0.0f, 60.0f, 1.0f / 60.0f);
}
#endif
} // namespace rage
