// 
// datasync/synchpresent.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synchpresent.h"
#include "synchelements.h"
#include "synchrules.h"

namespace rage {

#define __COMPILATION_TEST__ 1

bool NSEPresenter::StartObject(const char * pBlockName)
{
	if (StartBlock(pBlockName, false))
	{
		return true;
	}
	return false;
}

NSETypeBase &NSEPresenter::EndObject()
{
	Assert(!m_BlockStack[ m_iCurrentDepth].m_bIsGroup);
	return EndBlock();
}

bool NSEPresenter::StartGroupInternal(const char * pBlockName, const bool *pGroupGuard)
{
	//Root groups are now automatically treated as object blocks
	if (m_iCurrentDepth < 0)
	{
		return StartObject(pBlockName);
	}
	else
	{
		if (StartBlock(pBlockName, true, pGroupGuard))
		{
			return true;
		}
	}
	return false;
}


NSETypeBase &NSEPresenter::EndGroup()
{
	//TMS:	Removed assertion as root groups are becoming object roots
	//		by default!
	//Assert(m_BlockStack[ m_iCurrentDepth].m_bIsGroup);
	return EndBlock();
}


bool NSEPresenter::StartBlock(const char * pBlockName, bool bIsGroup, const bool *pGroupGuard)
{
	//We don't allocate the root element until the end of the block when we will know
	//more about what we are dealing with - for instance how many children we need to
	//allocate for in the case of groups.
	++m_iCurrentDepth;
	Assertf(m_iCurrentDepth< kMaxStackedObjects,"Nesting overflow in data block %s", pBlockName);
	if (m_iCurrentDepth < kMaxStackedObjects)
	{
		m_BlockStack[ m_iCurrentDepth].mp_BlockName = pBlockName;
		m_BlockStack[ m_iCurrentDepth].m_iFirstOffset = m_iCurrentTrackedOffset;	//Used to return the tracker if needed
		m_BlockStack[ m_iCurrentDepth].m_bIsGroup = bIsGroup;
		m_BlockStack[ m_iCurrentDepth].mp_BlockGuard = pGroupGuard;
		return true;
	}
	return false;
}

NSETypeBase &NSEPresenter::EndBlock()
{
	BlockInfo &rBI = m_BlockStack[ m_iCurrentDepth];
	const int iChildCount = m_iCurrentTrackedOffset - rBI.m_iFirstOffset;
	NSETypeBase *pChildrensParent = 0;
	NSETypeBase *pGroupGuard = 0;
	NSETypeBase *pNewBlock = 0;
	//Blocks have optional guard flags. These flags prevent further data being processed
	if (rBI.mp_BlockGuard)
	{	
		//We'll need to add the child objects to a group and assign that below the block
		NSETypeRulesBase *pRule = rage_new OnAnyChangeElemBase<bool>::ElemType;
		pGroupGuard = rage_new TSyncElemWrapper< bool, SimpleSerialize<bool> >( *pRule );
		pGroupGuard->SetSneakItIn();
		SetupElement(*rBI.mp_BlockGuard, *pGroupGuard);
		NOT_FINAL(pGroupGuard->SetName("GROUPGUARD"));
	}

	if (rBI.m_bIsGroup)
	{
		pChildrensParent = rage_new NSEGroup(iChildCount, pGroupGuard);
		pGroupGuard = 0;
		NOT_FINAL(pChildrensParent->SetName(rBI.mp_BlockName));
		pNewBlock = pChildrensParent;
	}
	else
	{
		pNewBlock = rage_new NSEObjectRoot;
		NOT_FINAL(pNewBlock->SetName(rBI.mp_BlockName));
		if (!m_iCurrentDepth)
		{
			mp_TypeRoot = pNewBlock;
		}

		//Create the relevant base object (we delay this
		//until we know if we're going to need a group or not - if there is
		//only a single sync'd object in this section then there is no need for a block
		AssertMsg(iChildCount >= 1 , "Empty blocks of sync data are pointless!");
		if ((iChildCount > 1) || pGroupGuard)	//Block guards not yet supported directly through NSEObjectRoot
		{
			pChildrensParent = rage_new NSEGroup(iChildCount, pGroupGuard);
			pGroupGuard = 0;

			pNewBlock->AddChildData(*pChildrensParent);
			NOT_FINAL(pChildrensParent->SetName("Children"));
		}
		else if (iChildCount == 1)
		{
			pChildrensParent = pNewBlock;
		}
	}
	AssertMsg(!pGroupGuard , "pGroupGuard was created but not assigned to something (or else not cleared to indicate assignment happened");
	
	//Add the child(ren) in
	Assert(m_iCurrentTrackedOffset<kMaxTrackedElements);
	for (int i=rBI.m_iFirstOffset ; i<m_iCurrentTrackedOffset ; i++)
	{
		Assert(mp_TrackedElements[i]);
		pChildrensParent->AddChildData(*mp_TrackedElements[i]);
	}

	--m_iCurrentDepth;

	//Are we returning to a previous layer?
	if (m_iCurrentDepth>=0)
	{
		//Reset the current offset...
		m_iCurrentTrackedOffset = rBI.m_iFirstOffset;

		//But remember to add this sub block onto any existing parent group!
		mp_TrackedElements[m_iCurrentTrackedOffset] = pNewBlock;
		++m_iCurrentTrackedOffset;
	}

	return *pNewBlock;
}

#if __COMPILATION_TEST__
	//Raw example - no macros... Make sure these examples compile
	class MyClass
	{
		float m_fFloatA;
		float m_fFloatB;
		float m_fFloatC;
		float m_fFloatD;
	public:
		static bool PresentClassData(NSEPresenter &rPresenter)
		{
			if (rPresenter.StartObject("MyClass"))
			{
				MyClass* pBaseOffset = 0;	//Fools GCC (can't deref cast to zero)
				rPresenter.Present("FloatA", pBaseOffset->m_fFloatA, *rage_new Elems::Float::Simple(*rage_new Rules::OnDelta::Float(0.1f)));
				if (rPresenter.StartGroup("MyGroup"))
				{
					Rules::OnHeartBeat::Float *pSharedRule = rage_new Rules::OnHeartBeat::Float(0.2f);
					rPresenter.Present("FloatB", pBaseOffset->m_fFloatB, *rage_new Elems::Float::Simple(*pSharedRule, SendMode::eReliable));
					rPresenter.Present("FloatC", pBaseOffset->m_fFloatC, *rage_new Elems::Float::Simple(*pSharedRule));
					rPresenter.Present("FloatD", pBaseOffset->m_fFloatD, *rage_new Elems::Float::Quantized(*pSharedRule, Quantizers::Float.Bits(0.0f, 1.0f, 8)));
					rPresenter.EndGroup();
				}
				rPresenter.EndObject().SetRequiresCreationCallback();
				return true;
			}
			return false;
		}
	};
#endif
} // namespace rage

