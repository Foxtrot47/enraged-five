// 
// DataSync/syncmovable.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "syncmovable.h"
#include "synchpresent.h"
#include "system/timemgr.h"
#include "system/param.h"
#include "math/random.h"		//For the serialize test

PARAM(nonetposquantizations, "Disable position quantizations");

#define CHECK_VAL 0

namespace rage {

const float kMovingThreshold = 0.001f;

float NetTransform::sm_PositionDeltaScale = 1.0f;
float NetTransform::sm_OrientationErrorScale = 3.0f;
float NetTransform::sm_LinearVelScale = 1.0f;
float NetTransform::sm_LinearVelDeltaScale = 4.0f;
float NetTransform::sm_LinearErrorWithTime = 0.05f;
float NetTransform::sm_InstantDeltaScale=0.01f;

float NetOrient::sm_OrientationDelta = 15.0f;
float NetOrient::sm_AngularVelScale = 5.0f;
float NetOrient::sm_AngularVelDeltaScale = 1.0f;
float NetOrient::sm_OrientErrorWithTime = 0.05f;
float NetOrient::sm_InstantDeltaScale=0.01f;

namespace PositionQuantize {

	Vector3 m_vWorldMin(Vector3::ZeroType);
	Vector3 m_vWorldDiag(Vector3::ZeroType);
	Vector3 m_vHalfCell(Vector3::ZeroType);
	float m_fGridCellSize=-1.0f;
	float m_fInvCellSize=-1.0f;
	u32 m_iGridXBits=0;
	u32 m_iGridXMax=0;
	u32 m_iGridYBits=0;
	u32 m_iGridYMax=0;
	u32 m_iGridZBits=0;
	u32 m_iGridZMax=0;
	bool m_bUseGrid = true;
	bool m_bUseNegligableY = true; 
	bool m_bUsePivot = true;
	bool m_bAllow8bitEncoding = true;
	bool m_bInited = false;
	
	const float kfMinYForNegligableY=0.1f;
	const int kHeaderBits = 3;

	enum ePosQuantType
	{
		ePQT_Pivot_Float16XZ_NeglibableY,
		ePQT_Pivot_Float16XYZ,
		ePQT_Grid_Float32XYZ,
		ePQT_Float32XYZ,
		ePQT_PivotFixed8XY_NegligableY,
		ePQT_PivotFixed8XYZ,
		ePQT_MethodCount
	};
	
	#if _POS_QUANT_STATS_
	int m_UsageCount[ePQT_MethodCount+1];//[ePQT_MethodCount] used to track disabled mode data
	#endif

#if 0 //__DEV
	void Test()
	{
		if (!m_bInited)
		{
			Vector3 vMin(-5000.0f, -5000.0f, -5000.0f);
			Vector3 vMax( 5000.0f,  5000.0f,  5000.0f);
			PositionQuantize::InitClass(vMin, vMax);
		}

		datBitBuffer testBuffer;
		char mem[256];
		testBuffer.SetReadWriteBits(mem, sizeof(mem), 0);
		
		for (int iTest=0; iTest<4 ; iTest++)
		{
			switch(iTest)
			{
			case 0:
				m_bUseGrid = true;
				m_bUsePivot = true;
				m_bAllow8bitEncoding = true;
				m_bInited = false;
				break;
			case 1:
				m_bUseGrid = true;
				m_bUsePivot = false;
				m_bAllow8bitEncoding = false;
				m_bInited = true;
				break;
			case 2:
				m_bUseGrid = true;
				m_bUsePivot = true;
				m_bAllow8bitEncoding = false;
				m_bInited = true;
				break;
			case 3:
				m_bUseGrid = true;
				m_bUsePivot = true;
				m_bAllow8bitEncoding = true;
				m_bInited = true;
				break;
			}

			int iTestBits=0;
			const int kNumIterations=5000;
			g_ReplayRand.SetFullSeed(2345);	//Always seed the same
			for (int i=0 ; i<kNumIterations ; i++)
			{
				float fTestAccuracy = g_ReplayRand.GetRanged(0.01f, 0.5f);
				const float kPiv = 300.0f;	//Range pivot can differ from position
				const float kPos = 5000.0f;
				const float kPivX = g_ReplayRand.GetRanged(-kPiv, kPiv);
				const float kPivY = g_ReplayRand.GetRanged(-kPiv, kPiv);
				const float kPivZ = g_ReplayRand.GetRanged(-kPiv, kPiv);
				const float kPosX = g_ReplayRand.GetRanged(-kPos, kPos);
				const float kPosY = g_ReplayRand.GetRanged(-kPos, kPos);
				const float kPosZ = g_ReplayRand.GetRanged(-kPos, kPos);
				Vector3 vPivot(kPivX, kPivY, kPivZ);
				Vector3 vTestPos(kPosX, kPosY, kPosZ);
				
				vPivot += vTestPos;

				testBuffer.SetCursorPos(0);
				testBuffer.SetNumBitsWritten(0);
				Export(testBuffer, vTestPos, fTestAccuracy, &vPivot, 1);
				int iWrittenBits = testBuffer.GetNumBitsWritten();
				iTestBits += iWrittenBits;

				testBuffer.SetCursorPos(0);
				Vector3 vPosIn;
				vPosIn.y = vTestPos.y;//For the "negligable Y values"
				testBuffer.SetNumBitsRead(0);
				Import(testBuffer, vPosIn, &vPivot, 1);
				int iReadBits = testBuffer.GetNumBitsRead();
				Assertf(iWrittenBits == iReadBits, "Bit count mismatch in PositionQuantize::Test [%d != %d]", iReadBits, iWrittenBits);
				Assertf(vPosIn.Dist( vTestPos ) < fTestAccuracy, "PositionQuantize test error: IN<%f,%f,%f>[acc:%f] OUT<%f,%f,%f>, Error: %f",
						vPosIn.x,
						vPosIn.y,
						vPosIn.z,
						fTestAccuracy,
						vTestPos.x,
						vTestPos.y,
						vTestPos.z,
						vPosIn.Dist( vTestPos )
					);
			}

			Displayf("%d bits (%f av)", iTestBits, ((float)iTestBits)/(float)kNumIterations);
		}
	}
#endif

	//World mins and maxes are used to setup the grid calculation
	void InitClass(Vector3::Param vWorldMin, Vector3::Param vWorldMax, float fGridCellSize, bool bUseGrid, bool bUsePivot, bool bAllowYDiscard, bool bUse8Bit)
	{
		if (PARAM_nonetposquantizations.Get())
		{
			return;
		}

		m_bUseGrid = bUseGrid;
		m_bUseNegligableY = bAllowYDiscard;	//TMS: Disabled until 
		m_bUsePivot = bUsePivot;
		m_bAllow8bitEncoding = bUse8Bit;

		m_vWorldMin = vWorldMin;
		m_vWorldDiag = Vector3(vWorldMax) - m_vWorldMin;
		float fHalfCell = fGridCellSize*0.5f;
		m_vHalfCell.Set(fHalfCell, fHalfCell, fHalfCell);
		Assert(m_vWorldDiag.x>=0.0f && m_vWorldDiag.y>=0.0f && m_vWorldDiag.z>=0.0f);

		Assert(fGridCellSize>0.0f);
		m_fGridCellSize = fGridCellSize;
		m_fInvCellSize = 1.0f / m_fGridCellSize;

		//NOTE: Gridcell bits could be zero in any dimention depending on world bounds
		int iCells;
		Vector3 vTmp(m_vWorldDiag);
		vTmp.Scale(m_fInvCellSize);
		iCells = (int)(float)(vTmp.x);
		while (iCells)
		{
			iCells>>=1;
			m_iGridXBits++;
		}
		if (m_iGridXBits)
		{
			m_iGridXMax = (1<<m_iGridXBits) - 1;
		}

		iCells = (int)(float)(vTmp.y);
		while (iCells)
		{
			iCells>>=1;
			m_iGridYBits++;
		}
		if (m_iGridYBits)
		{
			m_iGridYMax = (1<<m_iGridYBits) - 1;
		}

		iCells = (int)(float)(vTmp.z);
		while (iCells)
		{
			iCells>>=1;
			m_iGridZBits++;
		}
		if (m_iGridZBits)
		{
			m_iGridZMax = (1<<m_iGridZBits) - 1;
		}

		if (m_iGridXBits + m_iGridYBits + m_iGridZBits > 40)
		{
			Warningf("Not saving anything using the position quantization grid - disabling!");
			m_bUseGrid = false;
		}

		m_bInited  =true;
	}
	
	void GetGridCell(const Vector3 &rPos, Vector3 &rvGridRootOut, u32 &riGridXOut, u32 &riGridYOut, u32 &riGridZOut)
	{
		//TMS: Offset by half a cell to centre the get the cell origin/pivot nearer the request pos on average
		Vector3 vSearchCell = rPos + m_vHalfCell;
		Vector3 r = vSearchCell - m_vWorldMin;

		//Clamp into the grid (will just lose accuracy outside)
		r.x = Clamp(r.x, 0.0f, m_vWorldDiag.x);
		r.y = Clamp(r.y, 0.0f, m_vWorldDiag.y);
		r.z = Clamp(r.z, 0.0f, m_vWorldDiag.z);
		
		float fXFloored = floor(r.x  * m_fInvCellSize);
		float fYFloored = floor(r.y  * m_fInvCellSize);
		float fZFloored = floor(r.z  * m_fInvCellSize);

		//Store grid cell offsets (final clamp to make sure inside bit fields, again only losing accuracy outside
		riGridXOut = Min((u32)fXFloored, m_iGridXMax);
		riGridYOut = Min((u32)fYFloored, m_iGridYMax);
		riGridZOut = Min((u32)fZFloored, m_iGridZMax);

		//Store grid cell base
		rvGridRootOut.x = (((float)riGridXOut) * m_fGridCellSize) + m_vWorldMin.x;
		rvGridRootOut.y = (((float)riGridYOut) * m_fGridCellSize) + m_vWorldMin.y;
		rvGridRootOut.z = (((float)riGridZOut) * m_fGridCellSize) + m_vWorldMin.z;
	}

	void ReadCell(datBitBuffer& rBuffer, Vector3 &rCellPivotOut)
	{
		//Read back cell X Y Z
		u32 iCellX=0;
		u32 iCellY=0;
		u32 iCellZ=0;
		if (m_iGridXBits)
		{
			rBuffer.ReadUns(iCellX, m_iGridXBits);
		}
		if (m_iGridYBits)
		{
			rBuffer.ReadUns(iCellY, m_iGridYBits);
		}
		if (m_iGridZBits)
		{
			rBuffer.ReadUns(iCellZ, m_iGridZBits);
		}

		//Now calculate the pivot
		rCellPivotOut.x = ((float)iCellX) * m_fGridCellSize;
		rCellPivotOut.y = ((float)iCellY) * m_fGridCellSize;
		rCellPivotOut.z = ((float)iCellZ) * m_fGridCellSize;

		//Offset back to world coords
		rCellPivotOut += m_vWorldMin;
	}

	void WriteCell(datBitBuffer& rBuffer, u32 iGridX, u32 iGridY, u32 iGridZ)
	{
		if (m_iGridXBits)
		{
			rBuffer.WriteUns(iGridX, m_iGridXBits);
		}
		if (m_iGridYBits)
		{
			rBuffer.WriteUns(iGridY, m_iGridYBits);
		}
		if (m_iGridZBits)
		{
			rBuffer.WriteUns(iGridZ, m_iGridZBits);
		}
	}

#if CHECK_VAL
	const u16 kMyCheck=0x1234;
#endif

	//Import the vector. If pivot is specified then we will try to export quantized and local to the pivot
	//the assumption being that we have the same pivot point at the receiver.
	//fRequiredPrecision is used to select validate quantization based on precision available
	
	void Import(datBitBuffer &rBuffer, Vector3 &rPosInAndOut, const Vector3* pPivot, u16 iPivotID, int iPivotIdBits)
	{
		if (!m_bInited)
		{
			rBuffer.ReadFloat(rPosInAndOut.x);
			rBuffer.ReadFloat(rPosInAndOut.y);
			rBuffer.ReadFloat(rPosInAndOut.z);
			return;
		}

		u32 iQuantType;
		rBuffer.ReadUns(iQuantType, kHeaderBits);
		
		switch(iQuantType)
		{
		case ePQT_PivotFixed8XY_NegligableY:
			Assert(pPivot);
			if (pPivot)
			{
				u32 val;
				if (iPivotIdBits > 0)
				{
					rBuffer.ReadUns(val, iPivotIdBits);
					if (iPivotID != val)
					{
						Displayf("PivotFixed8XY_NegligableY: Discarding position due to ID mismatch [IN:%d != LOCAL:%d]", val, iPivotID);
						//Still need to keep the buffer inline.
						rBuffer.ReadUns(val, 8);
						rBuffer.ReadUns(val, 8);
						return;
					}
				}
				rBuffer.ReadUns(val, 8);
				float fX = Quantizers::DeQuantize(val, -2.0f, 2.0f, 8);
				rBuffer.ReadUns(val, 8);
				float fZ = Quantizers::DeQuantize(val, -2.0f, 2.0f, 8);

				Vector3 r(fX,0.0f,fZ);
				r += *pPivot;
				r.y = rPosInAndOut.y;
				if (Abs(r.y - pPivot->y) > kfMinYForNegligableY)
				{
					r.y = Clamp(r.y, pPivot->y-kfMinYForNegligableY, pPivot->y+kfMinYForNegligableY);
				}
				rPosInAndOut = r;

			}
			break;

		case ePQT_PivotFixed8XYZ:
			Assert(pPivot);
			if (pPivot)
			{
				u32 val;
				if (iPivotIdBits > 0)
				{
					rBuffer.ReadUns(val, iPivotIdBits);
					if (iPivotID != val)
					{
						Displayf("PivotFixed8XYZ: Discarding position due to ID mismatch [IN:%d != LOCAL:%d]", val, iPivotID);
						//Still need to keep the buffer inline.
						rBuffer.ReadUns(val, 8);
						rBuffer.ReadUns(val, 8);
						rBuffer.ReadUns(val, 8);
						return;
					}
				}
				rBuffer.ReadUns(val, 8);
				float fX = Quantizers::DeQuantize(val, -2.0f, 2.0f, 8);
				
				rBuffer.ReadUns(val, 8);
				float fY = Quantizers::DeQuantize(val, -0.5f, 0.5f, 8);
				
				rBuffer.ReadUns(val, 8);
				float fZ = Quantizers::DeQuantize(val, -2.0f, 2.0f, 8);

				Vector3 r(fX,fY,fZ);
				r += *pPivot;
				rPosInAndOut = r;
			}
			break;

		case ePQT_Pivot_Float16XZ_NeglibableY:
			Assert(pPivot);
			if (pPivot)
			{
				Float16 x16, z16;
				u16 val;
				if (iPivotIdBits > 0)
				{
					rBuffer.ReadUns(val, iPivotIdBits);
					if (iPivotID != val)
					{
						Displayf("Pivot_Float16XZ_NeglibableY: Discarding position due to ID mismatch [IN:%d != LOCAL:%d]", val, iPivotID);
						rBuffer.ReadUns(val, 16);
						rBuffer.ReadUns(val, 16);
						return;
					}
				}
				rBuffer.ReadUns(val, 16);
				x16.SetBinaryData(val);
				rBuffer.ReadUns(val, 16);
				z16.SetBinaryData(val);

				Vector3 r(x16.GetFloat32_FromFloat16(),0.0f,z16.GetFloat32_FromFloat16());
//				Displayf("IN:XZ_NegY<%f,%f> Pivot<%f,%f,%f>, Y<%f>", r.x, r.z, pPivot->x, pPivot->y, pPivot->z, rPosInAndOut.y);
				r += *pPivot;
				r.y = rPosInAndOut.y;

				if (Abs(r.y - pPivot->y) > kfMinYForNegligableY)
				{
					r.y = Clamp(r.y, pPivot->y-kfMinYForNegligableY, pPivot->y+kfMinYForNegligableY);
				}

				rPosInAndOut = r;
			}
			break;
		case ePQT_Pivot_Float16XYZ:
			Assert(pPivot);
			if (pPivot)
			{
				u16 val;
#if CHECK_VAL
				rBuffer.ReadUns(val,16);
				if (kMyCheck != val)
				{
					Quitf("Check Val!");
				}
#endif
				if (iPivotIdBits > 0)
				{
					rBuffer.ReadUns(val, iPivotIdBits);
					if (iPivotID != val)
					{
						Displayf("Pivot_Float16XYZ: Discarding position due to ID mismatch [IN:%d != LOCAL:%d]", val, iPivotID);
						rBuffer.ReadUns(val, 16);
						rBuffer.ReadUns(val, 16);
						rBuffer.ReadUns(val, 16);
#if CHECK_VAL
						rBuffer.ReadUns(val,16);
						if (kMyCheck != val)
						{
							Quitf("Check Val!");
						}
#endif
						return;
					}
				}

				Float16 x16, y16, z16;
				rBuffer.ReadUns(val, 16);
				x16.SetBinaryData(val);
				rBuffer.ReadUns(val, 16);
				y16.SetBinaryData(val);
				rBuffer.ReadUns(val, 16);
				z16.SetBinaryData(val);

				Vector3 r(x16.GetFloat32_FromFloat16(),y16.GetFloat32_FromFloat16(),z16.GetFloat32_FromFloat16());
//				Displayf("IN:XYZ<%f,%f,%f> Pivot<%f,%f,%f>", r.x, r.y, r.z, pPivot->x, pPivot->y, pPivot->z);
				rPosInAndOut = r + (*pPivot);

#if CHECK_VAL
				rBuffer.ReadUns(val,16);
				if (kMyCheck != val)
				{
					Quitf("Check Val!");
				}
#endif
			}
			break;
		case ePQT_Grid_Float32XYZ: {
			//Like ePQT_Pivot_Float16XYZ only we need to extrapolate the pivot point
			Vector3 vPivot;
			ReadCell(rBuffer, vPivot);

			Float16 x16, y16, z16;
			u16 val;
			rBuffer.ReadUns(val, 16);
			x16.SetBinaryData(val);
			rBuffer.ReadUns(val, 16);
			y16.SetBinaryData(val);
			rBuffer.ReadUns(val, 16);
			z16.SetBinaryData(val);

			Vector3 r(x16.GetFloat32_FromFloat16(),y16.GetFloat32_FromFloat16(),z16.GetFloat32_FromFloat16());
			rPosInAndOut = r + vPivot;
			break;
		}
		case ePQT_Float32XYZ:
			rBuffer.ReadFloat(rPosInAndOut.x);
			rBuffer.ReadFloat(rPosInAndOut.y);
			rBuffer.ReadFloat(rPosInAndOut.z);
			break;
		}
	}

	//Export the vector. If pivot is specified then we will try to export quantized and local to the pivot
	//the assumption being that we have the same pivot point at the reciever.
	void Export(datBitBuffer& rBuffer, const Vector3 &rPos,  float fRequiredPrecision, const Vector3* pPivot, u16 iPivotID, int iPivotIdBits)
	{
		if (!m_bInited)
		{
			POS_QUANT_STATS_ONLY( m_UsageCount[ePQT_MethodCount]++;)
			rBuffer.WriteFloat(rPos.x);
			rBuffer.WriteFloat(rPos.y);
			rBuffer.WriteFloat(rPos.z);
			return;
		}

		float fRequiredPrecision2 = fRequiredPrecision*fRequiredPrecision;
		if (m_bUsePivot && pPivot && iPivotID)
		{
			//Try and use the pivot. It may or may not work
			Vector3 r = rPos - (*pPivot);

			if (m_bAllow8bitEncoding)
			{
				//If close try 8bit encoding
				//TMS Majic -	theres a whole load that could be done at this end of the spectrum
				//				but this is an experiment
				if (	(r.Mag2() < 4.0f) 
					&&	(Abs(r.y)<0.5f)				//TMS: More majic - we use less tolerance and more accuracy on the Y
					&&	(fRequiredPrecision > 0.04f) 
					)	
				{
					if (m_bUseNegligableY && (Abs(r.y) < kfMinYForNegligableY))	//Good for small offsets or flat areas like towns
					{
						POS_QUANT_STATS_ONLY( m_UsageCount[ePQT_PivotFixed8XY_NegligableY]++;)
	
						rBuffer.WriteUns(ePQT_PivotFixed8XY_NegligableY,kHeaderBits);

						if (iPivotIdBits > 0)
						{
							rBuffer.WriteUns(iPivotID, iPivotIdBits);
						}

						u32 val;
						val = Quantizers::Quantize<u32>(r.x, -2.0f, 2.0f, 8);
						rBuffer.WriteUns(val, 8);
						val = Quantizers::Quantize<u32>(r.z, -2.0f, 2.0f, 8);
						rBuffer.WriteUns(val, 8);
						return;
					}
					else
					{
						POS_QUANT_STATS_ONLY( m_UsageCount[ePQT_PivotFixed8XYZ]++;)

						rBuffer.WriteUns(ePQT_PivotFixed8XYZ,kHeaderBits);

						if (iPivotIdBits > 0)
						{
							rBuffer.WriteUns(iPivotID, iPivotIdBits);
						}

						u32 val;
						val = Quantizers::Quantize<u32>(r.x, -2.0f, 2.0f, 8);
						rBuffer.WriteUns(val, 8);
						val = Quantizers::Quantize<u32>(r.y, -0.5f, 0.5f, 8);
						rBuffer.WriteUns(val, 8);
						val = Quantizers::Quantize<u32>(r.z, -2.0f, 2.0f, 8);
						rBuffer.WriteUns(val, 8);
						return;
					}
				}
			}

			Float16 x16(r.x);
			Float16 y16(r.y);
			Float16 z16(r.z);
			if (m_bUseNegligableY && (Abs(r.y) < kfMinYForNegligableY))	//Good for small offsets or flat areas like towns
			{
				Vector3 vTest(x16.GetFloat32_FromFloat16(),r.y,z16.GetFloat32_FromFloat16());
				if (vTest.Dist2(r) < fRequiredPrecision2)
				{
					//Its good enough!
					rBuffer.WriteUns(ePQT_Pivot_Float16XZ_NeglibableY, kHeaderBits);

					if (iPivotIdBits > 0)
					{
						rBuffer.WriteUns(iPivotID, iPivotIdBits);
					}
					rBuffer.WriteUns(x16.GetBinaryData(),16);
					rBuffer.WriteUns(z16.GetBinaryData(),16);

					POS_QUANT_STATS_ONLY( m_UsageCount[ePQT_Pivot_Float16XZ_NeglibableY]++;)
					return;
				}
			}
			else
			{
				Vector3 vTest(x16.GetBinaryData(),y16.GetBinaryData(),z16.GetBinaryData());
				if (vTest.Dist2(r) < fRequiredPrecision2)
				{
					//Its good enough!
					rBuffer.WriteUns(ePQT_Pivot_Float16XYZ,kHeaderBits);
#if CHECK_VAL
					rBuffer.WriteUns(kMyCheck,16);
#endif
	
					if (iPivotIdBits > 0)
					{
						rBuffer.WriteUns(iPivotID, iPivotIdBits);
					}

					rBuffer.WriteUns(x16.GetBinaryData(),16);
					rBuffer.WriteUns(y16.GetBinaryData(),16);
					rBuffer.WriteUns(z16.GetBinaryData(),16);

#if CHECK_VAL
					rBuffer.WriteUns(kMyCheck,16);
#endif
					POS_QUANT_STATS_ONLY( m_UsageCount[ePQT_Pivot_Float16XYZ]++;)
					return;
				}
			}
		}

		//Okay, lets see if we can use the grid within the accuracy specified
		if (m_bUseGrid)
		{
			Vector3 vGridRoot;
			u32 iGridX, iGridY, iGridZ;
			GetGridCell(rPos, vGridRoot, iGridX, iGridY, iGridZ);
			
			//Same idea as with the pivot only this time (if the result is decent) we'll need
			//to send over the grid coordinates too.
			Vector3 r = rPos - vGridRoot;
			Float16 x16(r.x);
			Float16 y16(r.y);
			Float16 z16(r.z);
			Vector3 vTest(x16.GetBinaryData(),y16.GetBinaryData(),z16.GetBinaryData());
			if (vTest.Dist2(r) < fRequiredPrecision2)
			{
				//Its good!
				rBuffer.WriteUns(ePQT_Grid_Float32XYZ,kHeaderBits);
				
				WriteCell(rBuffer, iGridX, iGridY, iGridZ);

				rBuffer.WriteUns(x16.GetBinaryData(),16);
				rBuffer.WriteUns(y16.GetBinaryData(),16);
				rBuffer.WriteUns(z16.GetBinaryData(),16);

				POS_QUANT_STATS_ONLY( m_UsageCount[ePQT_Grid_Float32XYZ]++;)
				return;
			}
		}

		//Oh well, no quantization found, revert to full  96bits (+two now for type)
		rBuffer.WriteUns(ePQT_Float32XYZ,kHeaderBits);
		rBuffer.WriteFloat(rPos.x);
		rBuffer.WriteFloat(rPos.y);
		rBuffer.WriteFloat(rPos.z);
		POS_QUANT_STATS_ONLY( m_UsageCount[ePQT_Float32XYZ]++;)
	}

#if __BANK
	void AddWidgets(bkBank &bk)
	{
		bk.PushGroup("Position quantization");
			bk.AddToggle("Use pivot", &m_bUsePivot);
			bk.AddToggle("Use grid", &m_bUseGrid);
			bk.AddToggle("Use 8bit", &m_bAllow8bitEncoding);
			bk.AddToggle("Allow Y discard", &m_bUseNegligableY);
			
			
#if _POS_QUANT_STATS_
			bk.AddText("P XZ 8", &m_UsageCount[ePQT_PivotFixed8XY_NegligableY]);
			bk.AddText("P XYZ 8", &m_UsageCount[ePQT_PivotFixed8XYZ]);
			bk.AddText("P XZ f16", &m_UsageCount[ePQT_Pivot_Float16XZ_NeglibableY]);
			bk.AddText("P XYZ f16", &m_UsageCount[ePQT_Pivot_Float16XYZ]);
			bk.AddText("G XYZ f16", &m_UsageCount[ePQT_Grid_Float32XYZ]);
			bk.AddText("XYZ f32", &m_UsageCount[ePQT_Float32XYZ]);
			bk.AddText("Disabled", &m_UsageCount[ePQT_MethodCount]);
#endif
		bk.PopGroup();
	}

#endif
}


bool NetOrient::Capture(const INetMovable &rObject)
{
	Matrix34 trans;
	rObject.GetTransform(trans);
	trans.ToQuaternion( m_vOrient );
	rObject.GetAngVel(m_vAngVel);

	m_fStoredTime = TIME.GetElapsedTime();
	return true;
}

//Project from vFrom to current time and store locally
void NetOrient::ProjectCurrent(const NetOrient &vFrom)
{
	float fElapsed = TIME.GetElapsedTime() - vFrom.m_fStoredTime;
	if(fElapsed > 0.f)
	{
		Quaternion q;
		q.FromRotation(vFrom.m_vAngVel);
		m_vOrient.Multiply(vFrom.m_vOrient, q);
	}
	else
	{
		m_vOrient = vFrom.m_vOrient;
	}
	m_vAngVel = vFrom.m_vAngVel;
}

//Gets the delta of the two transforms (assumes equivalents time frames)
float NetOrient::GetDelta(const NetOrient &rOther) const
{
	//Discrepancy in orientation
	float fDelta = m_vOrient.RelAngle( rOther.m_vOrient ) * sm_OrientationDelta;
	fDelta += rOther.m_vAngVel.Dist( m_vAngVel ) * sm_AngularVelDeltaScale;
	fDelta += m_vAngVel.Mag() * sm_AngularVelScale;
	return fDelta;
}

//Get the delta between the two transformations projected to current game time
float NetOrient::GetProjectedDelta(const NetOrient &rOther) const
{
	NetOrient vMeProjected;
	vMeProjected.ProjectCurrent(*this);

	NetOrient vOtherProjected;
	vOtherProjected.ProjectCurrent(rOther);

	return vMeProjected.GetDelta(vOtherProjected);
}

float NetOrient::GetDRDelta(const NetOrient &rOther) const
{
	float fError = GetDelta(rOther) * sm_InstantDeltaScale;
	fError +=GetProjectedDelta(rOther);
	fError += (GetAgeDiff(rOther) * sm_OrientErrorWithTime);
	return fError;
}

bool NetTransform::Capture(const INetMovable &rObject)
{
	NetOrient::Capture( rObject );

	Matrix34 trans;
	rObject.GetTransform(trans);
	m_vPos = trans.d;
	//trans.ToQuaternion(m_vOrient);

	Vector3 vTmpVel;
	//Blend velocity over a couple of frames.
	rObject.GetLinVel(m_vLinVel);

	//TMS: Transitions from moving to not moving are critical to send early
	bool bIsMoving = m_vLinVel.Mag2() > kMovingThreshold;
	if (m_bWasMoving != bIsMoving)
	{
		MakeDirty();
		m_bWasMoving = bIsMoving;
	}

	return true;
}

void NetTransform::ProjectCurrent(const NetTransform &vFrom)
{
	NetOrient::ProjectCurrent( vFrom );

	float fElapsed = TIME.GetElapsedTime() - vFrom.m_fStoredTime;
	if(fElapsed > 0.f)
	{
		m_vPos = vFrom.m_vPos + vFrom.m_vLinVel * fElapsed;
	}
	else
	{
		m_vPos = vFrom.m_vPos;
	}
	m_vLinVel = vFrom.m_vLinVel;
}

NSETypeBase& NetOrient::PresentOrientation(const char *pName, NSEPresenter &presenter, NetOrient &trans, float fPri, float fFalloff, float fConst, float fFrameMaxDelta)
{
	typedef TSyncElemWrapper< NetOrient, NetOrient::QuantizeOrientation > NetOrientWrapper;
	return presenter.Present(pName, trans, *rage_new NetOrientWrapper(
				*rage_new NetDistPriorityElemBase<NetOrient>::ElemType(fPri, fFalloff, fConst, fFrameMaxDelta),
				NetOrient::QuantizeOrientation()
			)
		);
}


NSETypeBase& NetTransform::PresentPosition(const char *pName, NSEPresenter &presenter, NetTransform &trans, float fPri, float fFalloff, float fConst, float fFrameMaxDelta)
{
	typedef TSyncElemWrapper< NetTransform, NetTransform::QuantizePosition > NetTransformWrapper;
	return presenter.Present(pName, trans, *rage_new NetTransformWrapper(
				*rage_new NetDistPriorityElemBase<NetTransform>::ElemType(fPri, fFalloff, fConst, fFrameMaxDelta),
				NetTransform::QuantizePosition()
			)
		);
}

#if __BANK
void NetOrient::AddTypeWidgets(bkBank &bk)
{
	bk.PushGroup("NetOrientation");
		bk.AddSlider("Orientation delta",	&sm_OrientationDelta,		0.0f, 100.0f, 0.1f);
		bk.AddSlider("AngVel scale",		&sm_AngularVelScale,		0.0f, 100.0f, 0.1f);
		bk.AddSlider("AngVel delta",		&sm_AngularVelDeltaScale,	0.0f, 100.0f, 0.1f);
		bk.AddSlider("Time error",			&sm_OrientErrorWithTime,	0.0f, 10.0f, 0.05f);
		bk.AddSlider("Instant error",		&sm_InstantDeltaScale,		0.0f, 10.0f, 0.05f);
	bk.PopGroup();
}
void NetTransform::AddTypeWidgets(bkBank &bk)
{
	bk.PushGroup("NetPosition");
		bk.AddSlider("Position delta",		&sm_PositionDeltaScale,		0.0f, 100.0f, 0.1f);
		bk.AddSlider("Orientation error",	&sm_OrientationErrorScale,	0.0f, 100.0f, 0.1f);
		bk.AddSlider("Vel scale",			&sm_LinearVelScale,			0.0f, 100.0f, 0.1f);
		bk.AddSlider("Vel delta",			&sm_LinearVelDeltaScale,	0.0f, 100.0f, 0.1f);
		bk.AddSlider("Time error",			&sm_LinearErrorWithTime,	0.0f, 10.0f, 0.05f);
		bk.AddSlider("Instant error",		&sm_InstantDeltaScale,		0.0f, 10.0f, 0.05f);
	bk.PopGroup();
}
#endif

float NetTransform::GetDelta(const NetTransform &rOther) const
{
	float fDelta = NetOrient::GetDelta( rOther ) * sm_OrientationErrorScale;

	//Discrepancy in position
	fDelta += rOther.m_vPos.Dist( m_vPos ) * sm_PositionDeltaScale;

	fDelta += rOther.m_vLinVel.Dist( m_vLinVel ) * sm_LinearVelDeltaScale;

	return fDelta;
}

float NetTransform::GetProjectedDelta(const NetTransform &rOther) const
{
	NetTransform vMeProjected;
	vMeProjected.ProjectCurrent(*this);

	NetTransform vOtherProjected;
	vOtherProjected.ProjectCurrent(rOther);

	return vMeProjected.GetDelta(vOtherProjected);
}

float NetTransform::GetDRDelta(const NetTransform &rOther) const
{
	float fError = GetDelta(rOther) * sm_InstantDeltaScale;
	fError +=GetProjectedDelta(rOther);
	fError += (GetAgeDiff(rOther) * sm_LinearErrorWithTime);
	return fError;
}

void NetTransform::QuantizePosition::Export(NetTransform &value, const NetTransform &objectvalue, NSESerializer& rBuffer)
{
	float fAccuracy = Min(0.05f + value.m_vLinVel.Mag() * 0.025f, 0.3f);	//10ms^-1 = 30cm allowance on accuracy
	PositionQuantize::Export( rBuffer.GetBuffer(), value.m_vPos, fAccuracy, &objectvalue.m_vPivot, objectvalue.m_iPivotID, NetTransform::kPivotIdBits);
	rBuffer.IncWrittenElementCount();
	rBuffer.SetRequiresTimeStamp();
}

void NetTransform::QuantizePosition::Import(NetTransform &value, const NetTransform &objectvalue, NSESerializer& rBuffer)
{
	if (rBuffer.HasBuffer())
	{
		//TMS:	Copying in the current position prevents glitches when failing to import
		//		and makes sure the correct y value is used with the "discard y value" modes
		value.m_vPos = objectvalue.m_vPos;

		PositionQuantize::Import( rBuffer.GetBuffer(), value.m_vPos, &objectvalue.m_vPivot, objectvalue.m_iPivotID, NetTransform::kPivotIdBits);
	}
	else
	{
		rBuffer.SerBytes(&value.m_vPos, 3*sizeof(float));	//TMS: This is for the bit counter and leads to worst case estimates
	}
	value.m_fStoredTime = rBuffer.GetDataTimeStamp();
}

void NetTransform::QuantizeOrientation::Export(NetOrient &value, const NetOrient &/*objectvalue*/, NSESerializer& rBuffer)
{
	//Many objects are upright (actors). For these guys we're going to serialize a simple heading
	bool bIsQuat = false;
	Matrix34 matY;
	matY.FromQuaternion( value.m_vOrient );
	matY.d.Zero();
	//If not upright we'll do the heading
	if (matY.b.y < 0.99f)
	{
		bIsQuat = true;
	}
	value.m_bWasUpright = !bIsQuat;
	rBuffer.SerValue(bIsQuat);
	if (bIsQuat)
	{
		u32 valBits = Quantizers::QuantizeQuaternionS3(value.m_vOrient);
		rBuffer.SerValue(valBits);
	}
	else
	{
		//Serialize heading
		float fHeading = atan2f(matY.c.x, matY.c.z);
		u32 iTmpVal = Quantizers::Quantize<u32>(fHeading, -PI, PI, kHeadingBits);
		rBuffer.SerValue(iTmpVal, kHeadingBits);
	}

	rBuffer.SetRequiresTimeStamp();
}

void NetTransform::QuantizeOrientation::Import(NetOrient &value, const NetOrient &/*objectvalue*/, NSESerializer& rBuffer)
{
	bool bIsQuat = false;
	rBuffer.SerValue(bIsQuat);
	if (bIsQuat)
	{
		value.m_bWasUpright = false;
		u32 valBits;
		rBuffer.SerValue(valBits);
		Quantizers::DeQuantizeQuaternionS3(value.m_vOrient, valBits);
	}
	else
	{
		value.m_bWasUpright = true;
		u32 iTmpVal=0;
		rBuffer.SerValue(iTmpVal, kHeadingBits);
		float fHeading = Quantizers::DeQuantize<u32>(iTmpVal, -PI, PI, kHeadingBits);
		Matrix34 matY;
		matY.Identity();
		Vector3 eulers(0.0f, fHeading, 0.0f);
		matY.FromEulersXYZ(eulers);
		value.m_vOrient.FromMatrix34( matY );
	}
	value.m_fStoredTime = rBuffer.GetDataTimeStamp();
}

} // namespace rage
