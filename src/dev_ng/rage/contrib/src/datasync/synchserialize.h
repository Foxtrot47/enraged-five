// 
// datasync/synchserialize.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DATASYNC_SYNCHSERIALIZE_H 
#define DATASYNC_SYNCHSERIALIZE_H 

#include "data/bitbuffer.h"
#include "vector/vector3.h"
#include "vector/quaternion.h"
#include "synchdebug.h"
#include "quantizers.h"

namespace rage {

class NSOObjectBase;

//-------------------------------------------------------------------------------------------------
//The send mode
//-------------------------------------------------------------------------------------------------
namespace SendMode{
	enum Mode{
		eNA,			//Not Applicable
		eUnReliable,	//UDP - fire and forget
		eReliable,		//UDP - latest state
	};
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//NSESerializer presents the same interface for reading and writing data to a bit buffer - makes
//it easier to write templated code
//-------------------------------------------------------------------------------------------------
struct NSESerializer
{
	float m_fTimeStamp;
	void SetDataTimeStamp(float fSendTime)
	{
		m_fTimeStamp = fSendTime;
	}

	float GetDataTimeStamp() const
	{
		return m_fTimeStamp;
	}

	void SerValue(double &rValue)
	{
		m_iWrittenElements++;
		OnSerValue(rValue);
	}

	void SerValue(float &rValue)
	{
		m_iWrittenElements++;
		OnSerValue(rValue);
	}

	void SerValue(Vector3 &rValue)
	{
		m_iWrittenElements++;
		OnSerValue(rValue.x);
		OnSerValue(rValue.y);
		OnSerValue(rValue.z);
	}

	void SerValue(u64 &rValue, int iNumBits=64)
	{
		m_iWrittenElements++;
		OnSerValue(rValue, iNumBits);
	}

	void SerValue(s64 &rValue, int iNumBits=64)
	{
		m_iWrittenElements++;
		OnSerValue(rValue, iNumBits);
	}

	void SerValue(u32 &rValue, int iNumBits=32)
	{
		m_iWrittenElements++;
		OnSerValue(rValue, iNumBits);
	}

	void SerValue(s32 &rValue, int iNumBits=32)
	{
		m_iWrittenElements++;
		OnSerValue(rValue, iNumBits);
	}

	void SerValue(u16 &rValue, int iNumBits=16)
	{
		m_iWrittenElements++;
		OnSerValue(rValue, iNumBits);
	}

	void SerValue(s16 &rValue, int iNumBits=16)
	{
		m_iWrittenElements++;
		OnSerValue(rValue, iNumBits);
	}

	void SerValue(u8 &rValue, int iNumBits=8)
	{
		m_iWrittenElements++;
		OnSerValue(rValue, iNumBits);
	}

	void SerValue(s8 &rValue, int iNumBits=8)
	{
		m_iWrittenElements++;
		OnSerValue(rValue, iNumBits);
	}

	void SerValue(bool &rValue)
	{
		m_iWrittenElements++;
		OnSerValue(rValue);
	}

	void SerBytes(void *pAddr, int iNumBytes)
	{
		m_iWrittenElements++;
		OnSerBytes(pAddr, iNumBytes);
	}

	virtual void SerGuard(bool &rValue)
	{
		OnSerValue(rValue);
	}

	int GetWrittenElementCount() const
	{
		return m_iWrittenElements;
	}

	virtual void SetRequiresTimeStamp()	{	AssertMsg(0,"Function does nothing at this level - perhaps re-eval some of the interface now its more settled");	}

	virtual int GetCurrentOffset() const=0;
	virtual void SetCurrentOffset(int iPos)=0;
	virtual bool HasBuffer() const {return false;}
	virtual NSOObjectBase *GetCurrentObjectRoot() { return 0; };
	virtual void PushRoot(NSOObjectBase * /*pRoot*/){}
	virtual void PopRoot(){}

	virtual u8 GetMode() const
	{
		return 0;
	}

	void IncWrittenElementCount()
	{
		m_iWrittenElements++;
	}

	//Exposing the bit buffer directly allows for custom export / import sections
	virtual datBitBuffer& GetBuffer() const = 0;

	NSESerializer()
	{
		m_iWrittenElements = 0;
	}
	virtual ~NSESerializer(){}

	static void DeQuantizeQuaternion(Quaternion &v, int xBits, int yBits, int zBits, int wBits, NSESerializer& rBuffer);
	static void QuantizeQuaternion(const Quaternion &v, int xBits, int yBits, int zBits, int wBits, NSESerializer& rBuffer);
	static void DeQuantizeVector(Vector3 &v, float fMin, float fMax, int xBits, int yBits, int zBits,NSESerializer& rBuffer);
	static void QuantizeVector(const Vector3 &v, float fMin, float fMax, int xBits, int yBits, int zBits, NSESerializer& rBuffer);

private:

	virtual void OnSerValue(double &rValue)=0;
	virtual void OnSerValue(float &rValue)=0;
	virtual void OnSerValue(u64 &rValue, int iNumBits=64)=0;
	virtual void OnSerValue(s64 &rValue, int iNumBits=64)=0;
	virtual void OnSerValue(u32 &rValue, int iNumBits=32)=0;
	virtual void OnSerValue(s32 &rValue, int iNumBits=32)=0;
	virtual void OnSerValue(u16 &rValue, int iNumBits=16)=0;
	virtual void OnSerValue(s16 &rValue, int iNumBits=16)=0;
	virtual void OnSerValue(u8 &rValue, int iNumBits=8)=0;
	virtual void OnSerValue(s8 &rValue, int iNumBits=8)=0;
	virtual void OnSerValue(bool &rVal)=0;
	virtual void OnSerBytes(void *pAddr, int iNumBytes)=0;

	int m_iWrittenElements;
};


//-------------------------------------------------------------------------------------------------
//Light weight serializer that simply counts the bits that a data would have used
//-------------------------------------------------------------------------------------------------
struct NSEBitCountSerializer : public NSESerializer
{
	NSEBitCountSerializer()
		:m_iBitCount(0)
	{
	}

	int GetBitsCounted() const
	{
		return m_iBitCount;
	}
private:
	template<typename T>
	int GenericCount(T &)
	{
		return sizeof(T) * 8;
	}

	virtual int GetCurrentOffset() const
	{
		AssertMsg(0,"Unsupported functionality. Assertion may be over heavy");
		return 0;
	}

	virtual u8 GetMode() const
	{	
		//Can't count unless we fake fill buffers
		return 0;
	}

	virtual void SetCurrentOffset(int /*iPos*/)	
	{
		AssertMsg(0,"Unsupported functionality. Assertion may be over heavy");
	}

#ifdef __SNC__
#pragma diag_suppress 285
#endif
	rage::datBitBuffer &GetBuffer() const
	{
		AssertMsg(0,"Unsupported functionality. Assertion is valid!!");
		return *(rage::datBitBuffer*)0;
	}

	virtual void OnSerValue(double &rValue)
	{
		m_iBitCount += GenericCount(rValue);
	}

	virtual void OnSerValue(float &rValue)
	{
		m_iBitCount += GenericCount(rValue);
	}

	virtual void OnSerValue(u64 &, int iNumBits)
	{
		m_iBitCount += iNumBits;
	}

	virtual void OnSerValue(s64 &, int iNumBits=64)
	{
		m_iBitCount += iNumBits;
	}

	virtual void OnSerValue(u32 &, int iNumBits=32)
	{
		m_iBitCount += iNumBits;
	}

	virtual void OnSerValue(s32 &, int iNumBits=32)
	{
		m_iBitCount += iNumBits;
	}

	virtual void OnSerValue(u16 &, int iNumBits=16)
	{
		m_iBitCount += iNumBits;
	}

	virtual void OnSerValue(s16 &, int iNumBits=16)
	{
		m_iBitCount += iNumBits;
	}

	virtual void OnSerValue(u8 &, int iNumBits=8)
	{
		m_iBitCount += iNumBits;
	}

	virtual void OnSerValue(s8 &, int iNumBits=8)
	{
		m_iBitCount += iNumBits;
	}

	virtual void OnSerValue(bool &)
	{
		m_iBitCount += 1;
	}

	virtual void OnSerBytes(void *, int iNumBytes)
	{
		m_iBitCount += iNumBytes * 8;
	}

	int m_iBitCount;
};
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//NSEBufferSerializer presents the same interface for reading and writing data to a bit buffer - makes
//it easier to write templated code
//-------------------------------------------------------------------------------------------------
struct NSEBufferSerializer : public NSESerializer
{
	virtual bool HasBuffer() const {return true;}

	//A guarded section is a section that is optional and preceded by a 
	//guard flag. If the flag isn't set true then the section doesn't get read.
	//The guarded section allows subsections to tentatively write their own sub guard flags
	//which will be rewound if the parent container is otherwise empty
	class GuardedSection
	{
		NSESerializer *mp_Buffer;
		int m_iStartCursor;
		int m_iStartElems;
		bool m_bPlaced;
		GuardedSection(){}
	public:
		GuardedSection(NSESerializer &rBuffer)
			:mp_Buffer(&rBuffer)
			,m_bPlaced(false)
		{
		}
		void Place();
		bool Finalize();
		bool StartImport();
		void EndImport();
	};

	NSOObjectBase *GetCurrentObjectRoot()
	{
		Assert(m_iCurrentRootDepth>-1);
		return mp_Root[m_iCurrentRootDepth];
	}
	void PushRoot(NSOObjectBase *pRoot)
	{
		Assert(m_iCurrentRootDepth<kMaxObjectRoots-1);
		AssertMsg((m_iCurrentRootDepth<0 || pRoot==mp_Root[m_iCurrentRootDepth]) , "Remove assertion when getting composite objects working");
		m_iCurrentRootDepth++;
		mp_Root[m_iCurrentRootDepth] = pRoot;
	}
	void PopRoot()
	{
		Assert(m_iCurrentRootDepth>-1);
		--m_iCurrentRootDepth;
	}

	NSEBufferSerializer()
		:m_iCurrentRootDepth(-1)
	{
	}
	virtual ~NSEBufferSerializer(){}
private:

	enum {kMaxObjectRoots=8};
	NSOObjectBase *mp_Root[kMaxObjectRoots];
	int m_iCurrentRootDepth;
};

struct NSEWriteSerializer : public NSEBufferSerializer
{
	//We read/write reliable and unreliable data at the same time
	datBitBuffer *mp_Dbb;
	u8			m_WriteMode;
	bool m_bRequiresTimeStamp;

	rage::datBitBuffer &GetBuffer() const
	{
		return *(rage::datBitBuffer*)mp_Dbb;
	}


	NSEWriteSerializer(datBitBuffer &rBitBuffer, SendMode::Mode mode) 
		: mp_Dbb(&rBitBuffer)
		, m_WriteMode((u8)mode)
		, m_bRequiresTimeStamp(false)
	{}
	NSEWriteSerializer(const NSEWriteSerializer &rOther)
		: mp_Dbb(rOther.mp_Dbb)
		, m_WriteMode(rOther.m_WriteMode)
		, m_bRequiresTimeStamp(false)
	{}

	virtual u8 GetMode() const
	{
		return m_WriteMode;
	}

	virtual void OnSerValue(double &rValue)
	{
		mp_Dbb->WriteDouble(rValue);
		NSE_TRACE_OUT("double");
	}
	virtual void OnSerValue(float &rValue)
	{
		mp_Dbb->WriteFloat(rValue);
		NSE_TRACE_OUT("float");
	}
	virtual void OnSerValue(u64 &rValue, int iNumBits=64)
	{
		mp_Dbb->WriteUns(rValue, iNumBits);
		NSE_TRACE_OUT("u64");
	}
	virtual void OnSerValue(s64 &rValue, int iNumBits=64)
	{
		mp_Dbb->WriteInt(rValue, iNumBits);
		NSE_TRACE_OUT("s64");
	}
	virtual void OnSerValue(u32 &rValue, int iNumBits=32)
	{
		mp_Dbb->WriteUns(rValue, iNumBits);
		NSE_TRACE_OUT("u32");
	}
	virtual void OnSerValue(s32 &rValue, int iNumBits=32)
	{
		mp_Dbb->WriteInt(rValue, iNumBits);
		NSE_TRACE_OUT("double");
	}
	virtual void OnSerValue(u16 &rValue, int iNumBits=16)
	{
		mp_Dbb->WriteUns(rValue, iNumBits);
		NSE_TRACE_OUT("u16");
	}
	virtual void OnSerValue(s16 &rValue, int iNumBits=16)
	{
		mp_Dbb->WriteInt(rValue, iNumBits);
		NSE_TRACE_OUT("s16");
	}
	virtual void OnSerValue(u8 &rValue, int iNumBits=8)
	{
		mp_Dbb->WriteUns(rValue, iNumBits);
		NSE_TRACE_OUT("u8");
	}
	virtual void OnSerValue(s8 &rValue, int iNumBits=8)
	{
		mp_Dbb->WriteInt(rValue, iNumBits);
		NSE_TRACE_OUT("s8");
	}
	virtual void OnSerValue(bool &rValue)
	{
		u8 rVal = rValue ? 1 : 0;
		mp_Dbb->WriteUns(rVal, 1);
		NSE_TRACE_OUT("bool");
	}
	void OnSerBytes(void *pAddr, int iNumBytes)
	{
		mp_Dbb->WriteBytes(pAddr, iNumBytes);
		NSE_TRACE_OUT("bytes");
	}

	virtual void SerGuard(bool &rValue)
	{
		u8 rVal = rValue ? 1 : 0;
		mp_Dbb->WriteUns(rVal, 1);
//#if NSE_TRACE_ON
//		if (rValue)
//		{
//			if (mode == SendMode::eReliable)
//			{
//				NSE_TRACE_OUT("RELguard[SET]");
//			}
//			else
//			{
//				NSE_TRACE_OUT("UNRELguard[SET]");
//			}
//		}
//		else
//		{
//			if (mode == SendMode::eReliable)
//			{
//				NSE_TRACE_OUT("RELguard[EMPTY]");
//			}
//			else
//			{
//				NSE_TRACE_OUT("UNRELguard[EMPTY]");
//			}
//		}
//#endif
	}

	virtual int GetCurrentOffset() const
	{
		return mp_Dbb->GetCursorPos();
	}
	virtual void SetCurrentOffset(int iPos)
	{
		mp_Dbb->SetCursorPos(iPos);
	}

	virtual void SetRequiresTimeStamp()
	{ 
		m_bRequiresTimeStamp = true;
	}

	bool RequiresTimeStamp() const
	{ 
		return m_bRequiresTimeStamp;
	}

private:
	NSEWriteSerializer(){}//Not religiously private, just would need protection on the pointer
};

struct NSEReadSerializer : public NSEBufferSerializer
{
	//TMS: Currently we import a single packed at a time. It
	//may prove more efficient to import all packets concerning
	//a single object at once.
	datBitBuffer *mp_BitBuffer;
	u8			 m_Mode;

	rage::datBitBuffer &GetBuffer() const
	{
		return *(rage::datBitBuffer*)mp_BitBuffer;
	}

	virtual u8 GetMode() const
	{
		return m_Mode;
	}

	NSEReadSerializer(datBitBuffer &rBB, u8 mode) 
		: mp_BitBuffer(&rBB)
		, m_Mode(mode)
	{}

	NSEReadSerializer(const NSEReadSerializer &rOther)
		: mp_BitBuffer(rOther.mp_BitBuffer)
		, m_Mode(rOther.m_Mode)
	{}

	virtual void OnSerValue(double &rValue)
	{
		mp_BitBuffer->ReadDouble(rValue);
		NSE_TRACE_IN("double");
	}
	virtual void OnSerValue(float &rValue)
	{
		mp_BitBuffer->ReadFloat(rValue);
		NSE_TRACE_IN("float");
	}
	virtual void OnSerValue(u64 &rValue, int iNumBits=64)
	{
		mp_BitBuffer->ReadUns(rValue, iNumBits);
		NSE_TRACE_IN("u64");
	}
	virtual void OnSerValue(s64 &rValue, int iNumBits=64)
	{
		mp_BitBuffer->ReadInt(rValue, iNumBits);
		NSE_TRACE_IN("s64");
	}
	virtual void OnSerValue(u32 &rValue, int iNumBits=32)
	{
		mp_BitBuffer->ReadUns(rValue, iNumBits);
		NSE_TRACE_IN("u32");
	}
	virtual void OnSerValue(s32 &rValue, int iNumBits=32)
	{
		mp_BitBuffer->ReadInt(rValue, iNumBits);
		NSE_TRACE_IN("s32");
	}
	virtual void OnSerValue(u16 &rValue, int iNumBits=16)
	{
		mp_BitBuffer->ReadUns(rValue, iNumBits);
		NSE_TRACE_IN("u16");
	}
	virtual void OnSerValue(s16 &rValue, int iNumBits=16)
	{
		mp_BitBuffer->ReadInt(rValue, iNumBits);
		NSE_TRACE_IN("s16");
	}
	virtual void OnSerValue(u8 &rValue, int iNumBits=8)
	{
		mp_BitBuffer->ReadUns(rValue, iNumBits);
		NSE_TRACE_IN("u8");
	}
	virtual void OnSerValue(s8 &rValue, int iNumBits=8)
	{
		mp_BitBuffer->ReadInt(rValue, iNumBits);
		NSE_TRACE_IN("s8");
	}
	void OnSerBytes(void *pAddr, int iNumBytes)
	{
		mp_BitBuffer->ReadBytes(pAddr, iNumBytes);
		NSE_TRACE_IN("bytes");
	}
	virtual void OnSerValue(bool &rValue)
	{
		u8 rVal;
		mp_BitBuffer->ReadUns(rVal, 1);
		rValue = rVal ? true : false;
		NSE_TRACE_IN("bool");
	}

	virtual void SerGuard(bool &rValue)
	{
		u8 rVal;
		mp_BitBuffer->ReadUns(rVal, 1);
		rValue = rVal ? true : false;
#if NSE_TRACE_ON
		if (rValue)
		{
			NSE_TRACE_IN("guard[SET]");
		}
		else
		{
			NSE_TRACE_IN("guard[EMPTY]");
		}
#endif
	}

	virtual int GetCurrentOffset() const
	{
		return mp_BitBuffer->GetCursorPos();
	}
	virtual void SetCurrentOffset(int iPos)
	{
		mp_BitBuffer->SetCursorPos(iPos);
	}

private:
	NSEReadSerializer(){}//Not religiously private, just would need protection on the pointer
};
//-------------------------------------------------------------------------------------------------

#if __ASSERT
//Like read only simply compares value with that expected....
struct NSEVerifySerializer : public NSEBufferSerializer
{
	datBitBuffer *mp_BitBuffer;
	NSEVerifySerializer(datBitBuffer &rBB) 
		: mp_BitBuffer(&rBB)
	{}
	NSEVerifySerializer(const NSEReadSerializer &rOther)
		: mp_BitBuffer(rOther.mp_BitBuffer)
	{}


	virtual void OnSerValue(double &rValue)
	{
		double tmp;
		mp_BitBuffer->ReadDouble(tmp);
		Assert(tmp == rValue);
		NSE_TRACE_VERIFY("double");
	}
	virtual void OnSerValue(float &rValue)
	{
		float tmp;
		mp_BitBuffer->ReadFloat(tmp);
		Assert(tmp == rValue);
		NSE_TRACE_VERIFY("float");
	}
	virtual void OnSerValue(u64 &rValue, int iNumBits=64)
	{
		u64 tmp;
		mp_BitBuffer->ReadUns(tmp, iNumBits);
		Assert(tmp == rValue);
		NSE_TRACE_VERIFY("u64");
	}
	virtual void OnSerValue(s64 &rValue, int iNumBits=64)
	{
		s64 tmp;
		mp_BitBuffer->ReadInt(tmp, iNumBits);
		Assert(tmp == rValue);
		NSE_TRACE_VERIFY("s64");
	}
	virtual void OnSerValue(u32 &rValue, int iNumBits=32)
	{
		u32 tmp;
		mp_BitBuffer->ReadUns(tmp, iNumBits);
		Assert(tmp == rValue);
		NSE_TRACE_VERIFY("u32");
	}
	virtual void OnSerValue(s32 &rValue, int iNumBits=32)
	{
		s32 tmp;
		mp_BitBuffer->ReadInt(tmp, iNumBits);
		Assert(tmp == rValue);
		NSE_TRACE_VERIFY("s32");
	}
	virtual void OnSerValue(u16 &rValue, int iNumBits=16)
	{
		u16 tmp;
		mp_BitBuffer->ReadUns(tmp, iNumBits);
		Assert(tmp == rValue);
		NSE_TRACE_VERIFY("u16");
	}
	virtual void OnSerValue(s16 &rValue, int iNumBits=16)
	{
		s16 tmp;
		mp_BitBuffer->ReadInt(tmp, iNumBits);
		Assert(tmp == rValue);
		NSE_TRACE_VERIFY("s16");
	}
	virtual void OnSerValue(u8 &rValue, int iNumBits=8)
	{
		u8 tmp;
		mp_BitBuffer->ReadUns(tmp, iNumBits);
		Assert(tmp == rValue);
		NSE_TRACE_VERIFY("u8");
	}
	virtual void OnSerValue(s8 &rValue, int iNumBits=8)
	{
		s8 tmp;
		mp_BitBuffer->ReadInt(tmp, iNumBits);
		Assert(tmp == rValue);
		NSE_TRACE_VERIFY("s8");
	}

	virtual void SerGuard(bool &rValue)
	{
		//Assumption:
		//Guard flags need to be written to continue parsing of the data.
		//They should be in temporaries so this *should* be fine.
		s8 tmp;
		mp_BitBuffer->ReadUns(tmp, 1);
		rValue = tmp ? 1 : 0;
		NSE_TRACE_VERIFY("guard");
	}

	void OnSerBytes(void *pAddr, int iNumBytes)
	{
		u8 *pObjectData = (u8 *)pAddr;
		while (iNumBytes)
		{
			u8 tmp;
			mp_BitBuffer->ReadBytes(&tmp, 1);
			Assert(tmp == *pObjectData);
			++pObjectData;
		}
		NSE_TRACE_VERIFY("bytes");
	}
	virtual void OnSerValue(bool &rValue)
	{
		u8 tmp;
		mp_BitBuffer->ReadUns(tmp, 1);
		u8 rVal = rValue ? true : false;
		Assert(rVal == tmp);
		NSE_TRACE_VERIFY("bool");
	}
	virtual int GetCurrentOffset() const
	{
		return mp_BitBuffer->GetCursorPos();
	}
	virtual void SetCurrentOffset(int iPos)
	{
		mp_BitBuffer->SetCursorPos(iPos);
	}
private:
	NSEVerifySerializer(){}//Not religiously private, just would need protection on the pointer
};
#endif
//-------------------------------------------------------------------------------------------------

} // namespace rage

#endif // DATASYNC_SYNCHSERIALIZE_H 
