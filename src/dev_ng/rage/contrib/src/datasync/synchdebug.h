// 
// datasync/synchdebug.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DATASYNC_SYNCHDEBUG_H 
#define DATASYNC_SYNCHDEBUG_H 

namespace rage {

#define NSE_TRACE_ON 0//__BANK

#define TRACE_ACK(x)

//Debugging help
#if NSE_TRACE_ON
	struct NSETraceBase
	{
		NSETraceBase(const char *pPrefix, const char *pLabel, int iDepth, int iCount, bool bTraceOn);
	};
	struct NSETraceOut : public NSETraceBase
	{
		static bool sm_bTraceOn;
		static int sm_TraceDepth;
		static int sm_TraceCount;
		NSETraceOut(const char *pPrefix, const char *pLabel);
		~NSETraceOut();
	};

	struct NSETraceIn : public NSETraceBase
	{
		static bool sm_bTraceOn;
		static int sm_TraceDepth;
		static int sm_TraceCount;
		NSETraceIn(const char *pPrefix, const char *pLabel);
		~NSETraceIn();
	};

	#define NSE_TRACE_OUT(x) NSETraceOut NSE_Tracingnesting_##__LINE__("OUT", x)
	#define NSE_TRACE_IN(x) NSETraceIn NSE_Tracingnesting_##__LINE__("IN", x)
	#define NSE_TRACE_VERIFY(x) NSETraceIn NSE_Tracingnesting_##__LINE__("VERIFY", x)

#else
	#define NSE_TRACE_IN(x)
	#define NSE_TRACE_OUT(x)
	#define NSE_TRACE_VERIFY(x)
#endif


} // namespace rage
#endif // DATASYNC_SYNCHDEBUG_H 
