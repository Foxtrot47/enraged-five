// 
// datasync/synchpriority.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DATASYNC_SYNCHPRIORITY_H 
#define DATASYNC_SYNCHPRIORITY_H 

#include "atl/array.h"
#include "synchelements.h"

namespace rage {
	class bkBank;

#define VALIDATE_QUEUE 0

#if VALIDATE_QUEUE
	#define VALIDATE_ONLY(x) ASSERT_ONLY(x)
#else
	#define VALIDATE_ONLY(x)
#endif

//-------------------------------------------------------------------------------------------------
//Prioritization queue - make sure the right clients are receiving the most important information
//											at any time.
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//Prioritized instance data derives from this class
//-------------------------------------------------------------------------------------------------
struct NSEPrioritized : public NSEInstanceBase
{
	float m_fPriority;									//Updated externally
	s16 m_iQueueIndex;									//Used for fast removals
	s8	m_iClientIndex;									//Client this element is to be sent for
	//u16 m_iNetObjectIndex??							//Throwback to parent object?
	NSEPrioritized(NSEContext &rCtxt);
	~NSEPrioritized();
	void OnSend()
	{
		m_fPriority = 0.0f;
	}
	int GetClient() const
	{
		return m_iClientIndex;
	}
	static int CompareByPtr(const void *_p0, const void *_p1)
	{
		const NSEPrioritized *p0 = *(NSEPrioritized **)_p0;
		const NSEPrioritized *p1 = *(NSEPrioritized **)_p1;
		float fp0=p0->m_fPriority;
		float fp1=p1->m_fPriority;
		if (fp0 > fp1)
			return -1;
		if (fp0 < fp1)
			return +1;
		return 0;
	}

};
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//The priority manager makes sure the most important prioritized data (deriving from NSEPrioritized)
//gets sent to each client.
//-------------------------------------------------------------------------------------------------
class NSEPriorityManager
{
	enum {kMaxClients = 31};
	enum {kMaxQueuedDefault = 4096};
	atArray<NSEPrioritized*> m_Queue;

	enum {kMaxPeers = 32};;
	struct PriorityPeer
	{
		u64 m_PeerId;
		int m_iAllowedBitsPerFrame;
		int m_iPeerIndex;
		PriorityPeer()
			:m_PeerId(0)
			,m_iAllowedBitsPerFrame(0)
		{
		}
	};
	PriorityPeer m_PeerList[kMaxPeers];

	int m_MaxBitsPerFrame;
	int m_MaxBitsPerFramePerClient;
	float m_fMinPriorityForSend;

	static NSEPriorityManager *smp_Instance;

	BANK_ONLY(int m_BytesQueued;)

	int FindClientIndex(u64 iPeerId) const;

	VALIDATE_ONLY(void ValidateQueue(bool bSpacesOkay=true));

public:
	static NSEPriorityManager &GetInstance()
	{
		AssertMsg(smp_Instance , "NSEPriorityManager needs to be instantiated to ");
		return *smp_Instance;
	}

	#if __BANK
	void AddWidgets(bkBank &bk);
	#endif

	//Add a managed data element (one piece of data represented for one client)
	bool AddNewElement(NSEPrioritized &rElem, NSEContext &rCtxt);

	//Remove a managed data element (one piece of data represented for one client)
	bool RemoveElement(NSEPrioritized &rElem);

	void SetMinPriForSend(float fMinPri)
	{	m_fMinPriorityForSend = fMinPri; }

	float GetMinPriForSend() const
	{	return m_fMinPriorityForSend; }

	void OnPeerJoined(u64 iPeerId, u32 iIndex);
	void OnPeerLeft(u64 iPeerId);
	void SetClientAllowance(u64 iPeerId, u32 iAllowedBitsPerFrame);
	//Update will:
	//	For each object update data priorities
	//	Sort the data by priority, at least approximately
	//	For each net object
	//		For each client
	//			Create dispatch data
	void Update();

	//Allow maximum upload to be tweaked
	void SetMaxBitsPerFrame(int iBitsPerSecond)
	{
		m_MaxBitsPerFrame = iBitsPerSecond;
	}

	//Allow maximum upload to be tweaked
	int GetMaxBitsPerFrame() const
	{
		return m_MaxBitsPerFrame;
	}

	NSEPriorityManager(int iMaxPrioritized=kMaxQueuedDefault)
		:m_MaxBitsPerFrame(3000)
		,m_MaxBitsPerFramePerClient(1000)
		,m_fMinPriorityForSend(0.001f)
	{
		AssertMsg(!smp_Instance , "Cannot support multiple priority managers until we sort out how to assign elements to the right one");
		smp_Instance = this;
		m_Queue.Reserve(iMaxPrioritized);
	}
	~NSEPriorityManager()
	{
		Assert(smp_Instance);
		smp_Instance = 0;
	}
};
//-------------------------------------------------------------------------------------------------



} // namespace rage
#endif // DATASYNC_SYNCHPRIORITY_H 
