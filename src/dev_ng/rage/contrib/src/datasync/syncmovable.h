// 
// DataSync/syncmovable.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DATASYNC_SYNCMOVABLE_H 
#define DATASYNC_SYNCMOVABLE_H 

#include "vector/quaternion.h"
#include "synchserialize.h"
#include "synchrules.h"
#include "synchpriority.h"
#include "bank/bank.h"

namespace rage {

#define _POS_QUANT_STATS_ 1
#if _POS_QUANT_STATS_
	#define POS_QUANT_STATS_ONLY(x) x
#else
	#define POS_QUANT_STATS_ONLY(x)
#endif

namespace PositionQuantize {

	//World mins and maxes are used to setup the grid calculation
	void InitClass(Vector3::Param vWorldMin, Vector3::Param vWorldMax, float fGridCellSize=256.0f, bool bUseGrid=true, bool bUsePivot=true, bool bAllowYDiscard=false, bool bUse8Bit=true);
	
	//Import the vector. If pivot is specified then we will try to export quantized and local to the pivot
	//the assumption being that we have the same pivot point at the reciever.
	//fRequiredPrecision is used to select quantization based on precision avialable
	void Import(datBitBuffer &rBuffer, Vector3 &rPosInAndOut, const Vector3* pPivot=0, u16 iPivotID=0, int iPivotIdBits=0);

	//Export the vector. If pivot is specified then we will try to export quantized and local to the pivot
	//the assumption being that we have the same pivot point at the reciever.
	void Export(datBitBuffer &rBuffer, const Vector3 &rPos, float fRequiredPrecision, const Vector3* pPivot=0, u16 iPivotID=0, int iPivotIdBits=0);

#if __BANK
	void AddWidgets(bkBank &bk);
#endif

	void Test();
}

class NetPresenter;
class INetMovable
{
public:
	virtual void GetTransform(Matrix34 &rTrans) const =0;
	virtual void SetTransform(const Matrix34 &rTrans)=0;
	virtual void GetLinVel(Vector3 &rVal) const { rVal.Zero(); }
	virtual void SetLinVel(const Vector3 &/*rVal*/) {}
	virtual void GetAngVel(Vector3 &rVal) const { rVal.Zero(); }
	virtual void SetAngVel(const Vector3 &/*rVal*/) {}
	virtual bool DisableMovable() const { return false; };
	virtual ~INetMovable(){};
};

//PURPOSE: Maintain orientation, project it for crude DR calculations and quantize it as efficiently as possible
struct NetOrient
{
	enum {kHeadingBits=9};

	static float sm_OrientationDelta;
	static float sm_AngularVelScale;
	static float sm_AngularVelDeltaScale;
	static float sm_OrientErrorWithTime;
	static float sm_InstantDeltaScale;

	Quaternion m_vOrient;	//Stored orientation
	Vector3 m_vAngVel;		//Stored velocity
	float	m_fStoredTime;	//Time information was stored (or sent)
	bool	m_bInitial;		//First time?
	bool	m_bWasUpright;	//Sent using upright rotation rather than quaternion?
	bool	m_bSendDisabled;	//Updates are currently disabled
	u8		m_iDirtyStamp;	//Used to mark the data as dirty
	NetOrient()
		:m_vOrient(Quaternion::sm_I)
		,m_vAngVel(Vector3::ZeroType)
		,m_fStoredTime(-1.0f)
		,m_iDirtyStamp(0)
		,m_bInitial(true)
		,m_bWasUpright(false)
		,m_bSendDisabled(false)
	{
	}

	void SetSendDisabled(bool bIsDisabled)
	{
		m_bSendDisabled = bIsDisabled;
	}

	bool IsSendDisabled() const
	{
		return m_bSendDisabled;
	}

	bool IsDirty(const NetOrient &rRelativeTo) const
	{
		return m_iDirtyStamp != rRelativeTo.m_iDirtyStamp;
	}

	//Increment the dirty stamp so it doesn't match any shadow data
	void MakeDirty()
	{
		m_iDirtyStamp++;
	}

	//Given a gohGUID setup its current NetTransform
	bool Capture(const INetMovable &rObject);

	//Project from vFrom to current time and store locally
	void ProjectCurrent(const NetOrient &vFrom);

	//Gets the delta of the two transforms (assumes equivalents time frames)
	float GetDelta(const NetOrient &rOther) const;

	//Get the delta between the two transformations projected to current game time
	float GetProjectedDelta(const NetOrient &rOther) const;

	//Get a delta value for Dead reckoning purposes
	float GetDRDelta(const NetOrient &rOther) const;

	//Get the age difference between two orientations
	float GetAgeDiff(const NetOrient &rOther) const
	{	return 	Abs(m_fStoredTime - rOther.m_fStoredTime);	}

	//Add widgets if needed
	BANK_ONLY(static void AddTypeWidgets(bkBank &bk));

	void operator=(const NetOrient &rOTher)
	{
		m_vOrient = rOTher.m_vOrient;
		m_vAngVel = rOTher.m_vAngVel;
		m_fStoredTime = rOTher.m_fStoredTime;
		m_iDirtyStamp = rOTher.m_iDirtyStamp;
		m_bWasUpright = rOTher.m_bWasUpright;
	}

	bool operator==(const NetOrient &rOTher)
	{
		return	m_vOrient.IsEqual(rOTher.m_vOrient)
			&&	m_vAngVel == rOTher.m_vAngVel
			&&	m_fStoredTime == rOTher.m_fStoredTime;
	}

	struct QuantizeOrientation
	{
		void Print(const NetOrient &rValue, char *pBuffer, int iSizeBuffer)
		{
			formatf(pBuffer, iSizeBuffer, "<%f, %f, %f, %f>", rValue.m_vOrient.x, rValue.m_vOrient.y, rValue.m_vOrient.z, rValue.m_vOrient.w);
		}

		void Export(NetOrient &value, const NetOrient &/*objectvalue*/, NSESerializer& rBuffer);
		void Import(NetOrient &value, const NetOrient &/*objectvalue*/, NSESerializer& rBuffer);
	};

	static NSETypeBase& PresentOrientation(const char *pName, NSEPresenter &presenter, NetOrient &trans, float fPri, float fFalloff, float fConst, float fDeltaMax);
};

//PURPOSE: Maintain transformation, project it for crude DR calculations and quantize it as efficiently as possible
struct NetTransform : public NetOrient
{
	static float sm_PositionDeltaScale;
	static float sm_OrientationErrorScale;
	static float sm_LinearVelScale;
	static float sm_LinearVelDeltaScale;
	static float sm_LinearErrorWithTime;
	static float sm_InstantDeltaScale;

	enum {kPivotIdBits = 4};//Number of bits to use in pivot ID

	Vector3 m_vPos;			//Stored position
	Vector3 m_vLinVel;		//Stored velocity
	Vector3 m_vPivot;		//Stored to optimize position update bandwidth
	u16	m_iPivotID;			//0 == no pivot, used to make sure we are using the same pivot!
	bool m_bWasMoving;		//Were we moving? (used to make sure we are considered dirty on transitions)

	NetTransform()
		:m_vPos(Vector3::ZeroType)
		,m_vLinVel(Vector3::ZeroType)
		,m_vPivot(Vector3::ZeroType)
		,m_iPivotID(0)
		,m_bWasMoving(false)
	{
	}

	void SetPivot(const Vector3& rPivot, u16 iPivotID)
	{
		m_vPivot = rPivot;
		m_iPivotID = iPivotID;
	}

	//Given a gohGUID setup its current NetTransform
	bool Capture(const INetMovable &rObject);

	//Project from vFrom to current time and store locally
	void ProjectCurrent(const NetTransform &vFrom);

	//Gets the delta of the two transforms (assumes equivalents time frames)
	float GetDelta(const NetTransform &rOther) const;

	//Get the delta between the two transformations projected to current game time
	float GetProjectedDelta(const NetTransform &rOther) const;

	//Get a delta value for Dead reckoning purposes
	float GetDRDelta(const NetTransform &rOther) const;

	//Add widgets if needed
	BANK_ONLY(static void AddTypeWidgets(bkBank &bk));


	void operator=(const NetTransform &rOTher)
	{
		NetOrient::operator=(rOTher);
		m_vPos = rOTher.m_vPos;
		m_vLinVel = rOTher.m_vLinVel;
	}

	bool operator==(const NetTransform &rOTher)
	{
		return		NetOrient::operator==(rOTher)
				&&	(m_vPos == rOTher.m_vPos)
				&&	(m_vLinVel == rOTher.m_vLinVel);
	}

	struct QuantizePosition
	{
		void Print(const NetTransform &rValue, char *pBuffer, int iSizeBuffer)
		{
			formatf(pBuffer, iSizeBuffer, "<%f, %f, %f>", rValue.m_vPos.x, rValue.m_vPos.y, rValue.m_vPos.z);
		}

		void Export(NetTransform &value, const NetTransform &objectvalue, NSESerializer& rBuffer);
		void Import(NetTransform &value, const NetTransform &objectvalue, NSESerializer& rBuffer);
	};

	static NSETypeBase& PresentPosition(const char *pName, NSEPresenter &presenter, NetTransform &trans, float fPri, float fFalloff, float fConst, float fDeltaMax);
};


namespace NetDistPriorityHelpers
{
	#if __BANK
		template<typename T>
		inline void AddTypeWidgets(bkBank&)
		{
		}

		template<>
		inline void AddTypeWidgets<NetTransform>(bkBank& bk)
		{
			NetTransform::AddTypeWidgets(bk);
		}

		template<>
		inline void AddTypeWidgets<NetOrient>(bkBank& bk)
		{
			NetOrient::AddTypeWidgets(bk);
		}
	#endif

	template<typename T>
	inline bool DataIsDirty(const T &/*val0*/, const T &/*val1*/)
	{
		//Default - will work for u16, s8 etc
		return false;
	}

	template<>
	inline bool DataIsDirty(const NetTransform &val0, const NetTransform &val1)
	{
		return val0.IsDirty(val1);
	}

	template<>
	inline bool DataIsDirty(const NetOrient &val0, const NetOrient &val1)
	{
		return val0.IsDirty(val1);
	}

	template<typename T>
	inline float FloatDelta(const T &val0, const T &val1)
	{
		//Default - will work for u16, s8 etc
		return (float)(val0 - val1);
	}

	template<>
	inline float FloatDelta(const NetTransform &val0, const NetTransform &val1)
	{
		return val0.GetDRDelta(val1);
	}

	template<>
	inline float FloatDelta(const NetOrient &val0, const NetOrient &val1)
	{
		return val0.GetDRDelta(val1);
	}

	template<>
	inline float FloatDelta(const Vector2 &val0, const Vector2 &val1)
	{
		return val0.Dist(val1);
	}

	template<>
	inline float FloatDelta(const Vector3 &val0, const Vector3 &val1)
	{
		return val0.Dist(val1);
	}

	template<>
	inline float FloatDelta(const float &val0, const float &val1)
	{
		return val0 - val1;
	}

	template<>
	inline float FloatDelta(const int &val0, const int &val1)
	{
		return (float)(val0 - val1);
	}

	template<typename T>
	inline bool IsSendDisabled(const T &)
	{
		return false;
	}

	template<>
	inline bool IsSendDisabled(const NetOrient &val0)
	{
		return val0.IsSendDisabled();
	}

	template<>
	inline bool IsSendDisabled(const NetTransform &val0)
	{
		return val0.IsSendDisabled();
	}
}

//Accumulate priority with delta
template<typename _DataType>
struct NetDistPriority
{
	float m_fMaxPriScale;
	float m_fHalfLife;
	float m_fAccumulation;
	float m_fFrameMax;

#if __BANK
	float m_fCurrentSeen;
	void AddWidgets(bkBank &bk)
	{
		bk.AddSlider("Priority scale", &m_fMaxPriScale, 0.0f, 50.0f, 0.1f);
		bk.AddSlider("Priority half life (dist)", &m_fHalfLife, 0.0f, 50.0f, 0.1f);
		bk.AddSlider("Const accum per second", &m_fAccumulation, 0.0f, 50.0f, 0.01f);
		bk.AddSlider("Max recorded per frame", &m_fFrameMax, 0.0f, 50.0f, 0.01f);
		bk.AddText("Smoothed raw value", &m_fCurrentSeen);

		NetDistPriorityHelpers::AddTypeWidgets<_DataType>(bk);
	}
#endif

	struct InstanceBase : public NSEPrioritized
	{
		InstanceBase(NSEContext &rCtxt)
			:NSEPrioritized(rCtxt)
		{
			NSEPriorityManager::GetInstance().AddNewElement( *this, rCtxt );
		}
		~InstanceBase()
		{
			NSEPriorityManager::GetInstance().RemoveElement( *this );
		}
	};

	struct InstanceData
	{
		float m_fLastUpdateTime;
		InstanceData(NSEContext &)
			:m_fLastUpdateTime(-1.0f)
		{}	
	};

	void OnSend(NSEInstanceBase &rData, InstanceData &, float)
	{
		NSEPrioritized &rPri = static_cast<NSEPrioritized &>(rData);
		rPri.OnSend();
	}

	NetDistPriority()
		:m_fMaxPriScale(1.0f)
		,m_fHalfLife(1.0f)
		,m_fAccumulation(0.001f)
		,m_fFrameMax(10.0f)
#if __BANK
		,m_fCurrentSeen(0.0f)
#endif
	{

	}

	NetDistPriority(float fMaxPriority, float fHalfLife, float fTolerance, float fFrameMax)
		:m_fMaxPriScale(fMaxPriority)
		,m_fHalfLife(fHalfLife)
		,m_fAccumulation(fTolerance)
		,m_fFrameMax(fFrameMax)
#if __BANK
		,m_fCurrentSeen(0.0f)
#endif
	{
	}


	//The update function updates the queued priority - external code actually marks the data for sending
	bool Update(NSEInstanceBase &rData, InstanceData & rInstanceData, const _DataType &shadow, const _DataType &value, NSEContext &rContext)
	{
		NSEPrioritized &rPriBase = rData.UpCast<NSEPrioritized>();
		Assert(FPIsFinite(rPriBase.m_fPriority));

		//Check if the data is marked dirty (returning true force the data to be send regardless of priority)
		if (NetDistPriorityHelpers::DataIsDirty(shadow, value ))
			return true;

		if (NetDistPriorityHelpers::IsSendDisabled(value))
		{
			rPriBase.m_fPriority = 0.0f;
			return false;
		}

		//Accumulate current delta
		float fInvDist = rContext.GetInvDistance();

		//Priority is combination of accumulation and delta, for now at least!
		float fDeltaRaw = fabs(NetDistPriorityHelpers::FloatDelta(shadow, value));
		BANK_ONLY(m_fCurrentSeen = m_fCurrentSeen*0.45f + fDeltaRaw*0.05f);
		float fDelta = Min(m_fFrameMax, fDeltaRaw);
		float fTimeStamp = rContext.GetDataTimeStamp();

		float fTimeDelta = Clamp(	rInstanceData.m_fLastUpdateTime >= 0.0f ? fTimeStamp - rInstanceData.m_fLastUpdateTime : 0.0f,
									0.0f,	//No backwards future time stuff
									10.0f	//And be wary of large time differences too while we're at it
								);
		float fCurrentAccum = (m_fHalfLife * m_fMaxPriScale) * Min(fInvDist, 1.0f);
		rPriBase.m_fPriority += (fDelta + m_fAccumulation) * fCurrentAccum * fTimeDelta;
		rInstanceData.m_fLastUpdateTime = fTimeStamp;
		return false;	//Returning true force the data to be send regardless of priority. false leaves it down to the priority manager to
						//schedule the update.
	}
};

template<typename _DataType>
struct NetDistPriorityElemBase
{
	typedef TNSESyncType <	_DataType,
		NetDistPriority<_DataType>, typename NetDistPriority<_DataType>::InstanceData,
		NoScheduledUpdate, NoScheduledUpdate::InstanceData,
		typename NetDistPriority<float>::InstanceBase
	> TElemTypeBase;

	struct ElemType :	public TElemTypeBase
	{
		ElemType(float fMaxPri, float fHalfLife, float fTolerance, float fFrameMaxDelta)
			:TElemTypeBase(NetDistPriority<_DataType>(fMaxPri, fHalfLife, fTolerance, fFrameMaxDelta), NoScheduledUpdate())
		{	}
	protected:
		ElemType()
			:TElemTypeBase(NetDistPriority<_DataType>(), NoScheduledUpdate())
		{	}
	};
};

} // namespace rage

#endif // DATASYNC_SYNCMOVABLE_H 
