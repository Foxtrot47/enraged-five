// 
// datasync/netpresent.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "netpresent.h"
//#include "netmovable.h"
#include "synchrules.h"
#include "syncmovable.h"

namespace rage {

NSETypeBase& NetPresenter::Heartbeat(const char *pElemName, const Vector3 &rElem, float fTime)
{
	return HearbeatCore<Vector3>(*this, pElemName, rElem, fTime);
}

NSETypeBase& NetPresenter::Heartbeat(const char *pElemName, const float &rElem, float fTime)
{
	return HearbeatCore(*this, pElemName, rElem, fTime);
}

NSETypeBase& NetPresenter::Heartbeat(const char *pElemName, const s16 &rElem, float fTime, int iNumBits)
{
	return HearbeatCore(*this, pElemName, rElem, fTime, iNumBits);
}

NSETypeBase& NetPresenter::Heartbeat(const char *pElemName, const u16 &rElem, float fTime, int iNumBits)
{
	return HearbeatCore(*this, pElemName, rElem, fTime, iNumBits);
}

NSETypeBase& NetPresenter::Heartbeat(const char *pElemName, const u32 &rElem, float fTime, int iNumBits)
{
	return HearbeatCore(*this, pElemName, rElem, fTime, iNumBits);
}

NSETypeBase& NetPresenter::Heartbeat(const char *pElemName, const s32 &rElem, float fTime, int iNumBits)
{
	return HearbeatCore(*this, pElemName, rElem, fTime, iNumBits);
}

NSETypeBase& NetPresenter::Heartbeat(const char *pElemName, const bool &rElem, float fTime)
{
	return HearbeatCore(*this, pElemName, rElem, fTime);
}

NSETypeBase& NetPresenter::Heartbeat(const char *pElemName, const u8 &rElem, float fTime, int iNumBits)
{
	return HearbeatCore(*this, pElemName, rElem, fTime, iNumBits);
}

NSETypeBase& NetPresenter::Heartbeat(const char *pElemName, const s8 &rElem, float fTime, int iNumBits)
{
	return HearbeatCore(*this, pElemName, rElem, fTime, iNumBits);
}

NSETypeBase& NetPresenter::HeartbeatAndChange(const char *pElemName, const Vector3 &rElem, float fTime)
{
	return HearbeatAndChangeCore<Vector3>(*this, pElemName, rElem, fTime);
}

NSETypeBase& NetPresenter::HeartbeatAndChange(const char *pElemName, const float &rElem, float fTime)
{
	return HearbeatAndChangeCore(*this, pElemName, rElem, fTime);
}

NSETypeBase& NetPresenter::HeartbeatAndChange(const char *pElemName, const s16 &rElem, float fTime, int iNumBits)
{
	return HearbeatAndChangeCore(*this, pElemName, rElem, fTime, iNumBits);
}

NSETypeBase& NetPresenter::HeartbeatAndChange(const char *pElemName, const u16 &rElem, float fTime, int iNumBits)
{
	return HearbeatAndChangeCore(*this, pElemName, rElem, fTime, iNumBits);
}

NSETypeBase& NetPresenter::HeartbeatAndChange(const char *pElemName, const u32 &rElem, float fTime, int iNumBits)
{
	return HearbeatAndChangeCore(*this, pElemName, rElem, fTime, iNumBits);
}

NSETypeBase& NetPresenter::HeartbeatAndChange(const char *pElemName, const s32 &rElem, float fTime, int iNumBits)
{
	return HearbeatAndChangeCore(*this, pElemName, rElem, fTime, iNumBits);
}

NSETypeBase& NetPresenter::HeartbeatAndChange(const char *pElemName, const bool &rElem, float fTime)
{
	return HearbeatAndChangeCore(*this, pElemName, rElem, fTime);
}

NSETypeBase& NetPresenter::HeartbeatAndChange(const char *pElemName, const u8 &rElem, float fTime, int iNumBits)
{
	return HearbeatAndChangeCore(*this, pElemName, rElem, fTime, iNumBits);
}

NSETypeBase& NetPresenter::HeartbeatAndChange(const char *pElemName, const s8 &rElem, float fTime, int iNumBits)
{
	return HearbeatAndChangeCore(*this, pElemName, rElem, fTime, iNumBits);
}

NSETypeBase& NetPresenter::HeartbeatAndChangeQuantized(const char *pElemName, const float &rElem, float fTime, float fMin, float fMax, u8 iNumBits)
{
	NSETypeRulesBase *pRule = rage_new OnHeartbeatAndChangeElemBase<float>::ElemType(fTime);
	NSETypeBase *pElem = rage_new Helpers::Quantized<float>::Elem( *pRule, Quantizers::Float.Bits(fMin, fMax, iNumBits) );
	Present(pElemName, rElem, *pElem);
	return *pElem;
}

NSETypeBase& NetPresenter::HeartbeatAndDeltaQuantized(const char *pElemName, const float &rElem, float fDelta, float fTime, float fMin, float fMax, u8 iNumBits)
{
	NSETypeRulesBase *pRule = rage_new OnHeartbeatAndDeltaElemBase<float>::ElemType(fDelta, fTime);
	NSETypeBase *pElem = rage_new Helpers::Quantized<float>::Elem( *pRule, Quantizers::Float.Bits(fMin, fMax, iNumBits) );
	Present(pElemName, rElem, *pElem);
	return *pElem;
}



NSETypeBase& NetPresenter::OnChange(const char *pElemName, const float &rElem)
{
	return OnChangeCore(*this, pElemName, rElem);
}

NSETypeBase& NetPresenter::OnChangeQuantized(const char *pElemName, const float &rElem, float fMin, float fMax, u8 iNumBits)
{
	NSETypeRulesBase *pRule = rage_new OnAnyChangeElemBase<float>::ElemType;
	NSETypeBase *pElem = rage_new Helpers::Quantized<float>::Elem( *pRule, Quantizers::Float.Bits(fMin, fMax, iNumBits) );
	Present(pElemName, rElem, *pElem);
	pElem->SetMode(SendMode::eReliable);
	return *pElem;
}

NSETypeBase& NetPresenter::OnChange(const char *pElemName, const u32 &rElem, int iNumBits)
{
	return OnChangeCore(*this, pElemName, rElem, iNumBits);
}

NSETypeBase& NetPresenter::OnChange(const char *pElemName, const s32 &rElem, int iNumBits)
{
	return OnChangeCore(*this, pElemName, rElem, iNumBits);
}


NSETypeBase& NetPresenter::OnChange(const char *pElemName, const Vector3 &rElem)
{
	return OnChangeCore(*this, pElemName, rElem);
}

NSETypeBase& NetPresenter::OnChange(const char *pElemName, const bool &rElem)
{
	return OnChangeCore(*this, pElemName, rElem);
}

NSETypeBase& NetPresenter::OnChange(const char *pElemName, const u16 &rElem, int iNumBits)
{
	return OnChangeCore(*this, pElemName, rElem, iNumBits);
}

NSETypeBase& NetPresenter::OnChange(const char *pElemName, const s16 &rElem, int iNumBits)
{
	return OnChangeCore(*this, pElemName, rElem, iNumBits);
}

NSETypeBase& NetPresenter::OnChange(const char *pElemName, const u8 &rElem, int iNumBits)
{
	return OnChangeCore(*this, pElemName, rElem, iNumBits);
}

NSETypeBase& NetPresenter::OnChange(const char *pElemName, const s8 &rElem, int iNumBits)
{
	return OnChangeCore(*this, pElemName, rElem, iNumBits);
}



NSETypeBase& NetPresenter::SneakIn(const char *pElemName, const float &rElem)
{
	return OnSneakCore(*this, pElemName, rElem);
}

NSETypeBase& NetPresenter::SneakIn(const char *pElemName, const u32 &rElem, int iNumBits)
{
	return OnSneakCore(*this, pElemName, rElem, iNumBits);
}

NSETypeBase& NetPresenter::SneakIn(const char *pElemName, const s32 &rElem, int iNumBits)
{
	return OnSneakCore(*this, pElemName, rElem, iNumBits);
}

NSETypeBase& NetPresenter::SneakIn(const char *pElemName, const bool &rElem)
{
	return OnSneakCore(*this, pElemName, rElem);
}

NSETypeBase& NetPresenter::SneakIn(const char *pElemName, const u16 &rElem, int iNumBits)
{
	return OnSneakCore(*this, pElemName, rElem, iNumBits);
}

NSETypeBase& NetPresenter::SneakIn(const char *pElemName, const s16 &rElem, int iNumBits)
{
	return OnSneakCore(*this, pElemName, rElem, iNumBits);
}

NSETypeBase& NetPresenter::SneakIn(const char *pElemName, const u8 &rElem, int iNumBits)
{
	return OnSneakCore(*this, pElemName, rElem, iNumBits);
}

NSETypeBase& NetPresenter::SneakIn(const char *pElemName, const s8 &rElem, int iNumBits)
{
	return OnSneakCore(*this, pElemName, rElem, iNumBits);
}

NSETypeBase& NetPresenter::Quantized(const char *pElemName, const float &rElem, float fMin, float fMax, u8 iNumBits)
{
	NSETypeRulesBase *pRule = rage_new Rules::AnyChange::Float();
	NSETypeBase *pElem = rage_new Elems::Float::Quantized(*pRule, Quantizers::Float.Bits(fMin, fMax, iNumBits));
	Present(pElemName, rElem, *pElem);
	return *pElem;
}

NSETypeBase& NetPresenter::Delta(const char *pElemName, const float &rElem, float fDelta)
{
	NSETypeBase *pElem = rage_new Elems::Float::Simple(*rage_new Rules::OnDelta::Float(fDelta));
	Present(pElemName, rElem, *pElem);
	return *pElem;
}

NSETypeBase& NetPresenter::Delta(const char *pElemName, const Vector3 &rElem, float fDelta)
{
	NSETypeBase *pElem = rage_new Elems::Vector3::Simple(*rage_new Rules::OnDelta::Vector3(fDelta));
	Present(pElemName, rElem, *pElem);
	return *pElem;
}

NSETypeBase& NetPresenter::DeltaQuantized(const char *pElemName, const float &rElem, float fDelta, float fMin, float fMax, u8 iNumBits)
{
	NSETypeRulesBase *pRule = rage_new Rules::OnDelta::Float(fDelta);
	NSETypeBase *pElem = rage_new Elems::Float::Quantized(*pRule, Quantizers::Float.Bits(fMin, fMax, iNumBits));
	Present(pElemName, rElem, *pElem);
	return*pElem;
}

NSETypeBase& NetPresenter::DistancePriority(const char *pElemName, const float &rElem, float fPriorityScale, float fHalfLifeDist, float fAccumulation, float fFrameMaxDelta)
{
	NSETypeBase *pElem = rage_new Elems::Float::Simple(
								*rage_new NetDistPriorityElemBase<float>::ElemType(fPriorityScale, fHalfLifeDist, fAccumulation, fFrameMaxDelta)
							);
	Present(pElemName, rElem, *pElem);
	return *pElem;
}

NSETypeBase& NetPresenter::DistancePriority(const char *pElemName, const Vector3 &rElem, float fPriorityScale, float fHalfLifeDist, float fAccumulation, float fFrameMaxDelta)
{
	NSETypeRulesBase *pRule = rage_new NetDistPriorityElemBase<Vector3>::ElemType(fPriorityScale, fHalfLifeDist, fAccumulation, fFrameMaxDelta);
	NSETypeBase *pElem = rage_new Helpers::Simple<Vector3>::Elem( *pRule );
	Present(pElemName, rElem, *pElem);
	return *pElem;
}

NSETypeBase& NetPresenter::DistancePrioritySmallFloat(const char *pElemName, const float &rElem, float fPriorityScale, float fHalfLifeDist, float fAccumulation, float fFrameMaxDelta)
{
	NSETypeRulesBase *pRule = rage_new NetDistPriorityElemBase<float>::ElemType(fPriorityScale, fHalfLifeDist, fAccumulation, fFrameMaxDelta);
	NSETypeBase *pElem = rage_new Elems::Float::SmallFloat( *pRule );
	Present(pElemName, rElem, *pElem);
	return *pElem;
}

NSETypeBase& NetPresenter::DistancePrioritySmallFloat(const char *pElemName, const Vector3 &rElem, float fPriorityScale, float fHalfLifeDist, float fAccumulation, float fFrameMaxDelta)
{
	NSETypeRulesBase *pRule = rage_new NetDistPriorityElemBase<Vector3>::ElemType(fPriorityScale, fHalfLifeDist, fAccumulation, fFrameMaxDelta);
	NSETypeBase *pElem = rage_new Helpers::SmallFloat<Vector3>::Elem( *pRule );
	Present(pElemName, rElem, *pElem);
	return *pElem;
}

NSETypeBase& NetPresenter::DistancePriorityQuantized(const char *pElemName, const float &rElem, float fPriorityScale, float fHalfLifeDist, float fAccumulation, float fFrameMaxDelta, float fMin, float fMax, u8 iNumBits)
{
	NSETypeRulesBase *pRule = rage_new NetDistPriorityElemBase<float>::ElemType(fPriorityScale, fHalfLifeDist, fAccumulation, fFrameMaxDelta);
	NSETypeBase *pElem = rage_new Elems::Float::Quantized(*pRule, Quantizers::Float.Bits(fMin, fMax, iNumBits));
	Present(pElemName, rElem, *pElem);
	return *pElem;
}

NSETypeBase& NetPresenter::DistancePriorityQuantized(const char *pElemName, const Vector2 &rElem, float fPriorityScale, float fHalfLifeDist, float fAccumulation, float fFrameMaxDelta, float fMin, float fMax, u8 iNumBits)
{
	NSETypeRulesBase *pRule = rage_new NetDistPriorityElemBase<Vector2>::ElemType(fPriorityScale, fHalfLifeDist, fAccumulation, fFrameMaxDelta);
	NSETypeBase *pElem = rage_new Helpers::Quantized<Vector2>::Elem(
							*pRule,
							Quantizers::V2.Bits(fMin, fMax, iNumBits)
						);
	Present(pElemName, rElem, *pElem);
	return *pElem;
}

NSETypeBase& NetPresenter::DistancePriorityQuantized(const char *pElemName, const Vector3 &rElem, float fPriorityScale, float fHalfLifeDist, float fAccumulation, float fFrameMaxDelta, float fMin, float fMax, u8 iNumBits)
{
	NSETypeRulesBase *pRule = rage_new NetDistPriorityElemBase<Vector3>::ElemType(fPriorityScale, fHalfLifeDist, fAccumulation, fFrameMaxDelta);
	NSETypeBase *pElem = rage_new Helpers::Quantized<Vector3>::Elem(
							*pRule,
							Quantizers::V3.Bits(fMin, fMax, iNumBits)
						);
	Present(pElemName, rElem, *pElem);
	return *pElem;
}


} // namespace rage

