// 
// datasync/synchmgr.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synchpresent.h"
#include "synchobject.h"
#include "bank/bank.h"
#include "system/timemgr.h"

namespace rage {


NSEInstanceBase* NSOType::CreateInstanceData(NSEContext &rContext) const
{
	RAGE_TRACK(NSOType__CreateInstanceData);
	return mp_RootItem ? mp_RootItem->CreateClientData(rContext) : 0;
}

void NSOType::DestroyInstanceData(NSEInstanceBase &rInst) const
{	
	if (mp_RootItem)
	{
		mp_RootItem->DestroyClientData(rInst);
	}
}

void NSOType::Update( NSEInstanceBase &rInst, NSEContext &rObj) const
{
	if (mp_RootItem)
	{
		mp_RootItem->Update( rInst, rObj, rObj.GetObject() );
	}
}

void NSOType::Visit( NSEVisitor &rVisitor ) const
{
	if (mp_RootItem)
	{
		mp_RootItem->Visit( rVisitor );
	}
}

void NSOType::Visit( NSEVisitor &rVisitor, NSEContext &context, NSEInstanceBase &rInst ) const
{
	if (mp_RootItem)
	{
		mp_RootItem->Visit(rVisitor, &rInst, &context, context.GetObject() );
	}
}


class SetDataTypes : public StackedVisitor
{
public:
	virtual void OnElement(NSETypeBase &rObject, NSEInstanceBase * /*pData*/, void * /*pObjectRoot*/)		
	{
		//Push the send mode up the stack
		for (int i=m_iStackDepth-1 ; i>=0 ; i--)
		{
			//If the mode is already known we can stop
			NSETypeBase *pType = m_GroupStack[i].mp_Type;
			if (pType)
			{
				if ((pType->GetMode() & rObject.GetMode()) == rObject.GetMode())
				{
					break;
				}

				//Otherwise push it on
				pType->AddMode( (SendMode::Mode)rObject.GetMode() );
			}
		}
	}
};


void NSOType::SetupFromPresenter(NSEPresenter &rPresenter)
{
	Assert(!mp_RootItem);
	mp_RootItem = rPresenter.GetRoot();

	//Now recurs through and set the flags for the groups
	//so we can post process the messages
	SetDataTypes setDT;
	Visit(setDT);
}

#if __BANK
class AddWidgetsVisitor : public NSEVisitor
{
	bkBank *mp_Bank;
public:
	AddWidgetsVisitor(bkBank &rBank)
		:mp_Bank(&rBank)
	{
	}
	virtual bool OnStartGroup(NSEGroup &rGroup, NSEInstanceBase * /*pData*/)
	{
		mp_Bank->PushGroup(rGroup.GetName(), false);
		rGroup.AddWidgets(*mp_Bank);
		return true;
	}

	virtual void OnEndGroup(NSEGroup &/*rGroup*/, NSEInstanceBase * /*pData*/)
	{
		mp_Bank->PopGroup();
	}

	virtual void OnElement(NSETypeBase &rObject, NSEInstanceBase * pData, void * /*pObjectRoot*/)
	{
		if (pData)
		{
			//Adding per instance widgets
		}
		else
		{
			mp_Bank->PushGroup(rObject.GetName(), false);
			rObject.AddWidgets(*mp_Bank);
			mp_Bank->PopGroup();
		}
	}
};

void NSOType::AddWidgets(bkBank& bk)
{
	AddWidgetsVisitor addTypeWidgets(bk);
	Visit(addTypeWidgets);
}
#endif


void NSOType::Import( NSEInstanceBase &rInst, NSEBufferSerializer &rBuffer) const
{
	if (mp_RootItem)
	{
		mp_RootItem->Import(rInst, rBuffer, false);
	}
}

void NSOType::Export( NSEInstanceBase &rInst, NSEBufferSerializer &rBuffer) const
{
	if (mp_RootItem)
	{
		mp_RootItem->Export(rInst, rBuffer, false, false);		//TMS: Not sure about the sneaky data flag here
	}
}

bool NSOInstance::IsDirty() const
{
	if (mp_RootElements)
	{
		return mp_RootElements->IsDirty();
	}
	return false;
}

int NSOInstance::Export(NSEBufferSerializer &rBuffer) const
{
	if (mp_TypeData && mp_RootElements)
	{
		mp_TypeData->Export(*mp_RootElements, rBuffer);
	}
	return 0;
}

int NSOInstance::Import(NSEBufferSerializer &rBuffer) const
{
	if (mp_TypeData && mp_RootElements)
	{
		mp_TypeData->Import(*mp_RootElements, rBuffer);
	}
	return 0;
}

bool NSOInstance::Create(const NSOType &rType, NSEContext &rContext)
{
	RAGE_TRACK(NSOInstance__Create);
	mp_RootElements = rType.CreateInstanceData(rContext);
	if (mp_RootElements)
	{
		mp_TypeData = &rType;
		return true;
	}
	return false;
}

void NSOInstance::Destroy()
{
	if (mp_TypeData && mp_RootElements)
	{
		mp_TypeData->DestroyInstanceData(*mp_RootElements);
		mp_RootElements = 0;
	}
}


void NSOInstance::Update(NSOObjectBase & rObj, float fDataTimeStamp, float fDistanceToObject, float fRoundTripTime)
{
	if (mp_TypeData && mp_RootElements)
	{
		NSEContext context(rObj, *this, fDataTimeStamp, fDistanceToObject);

		//We use the ack num to paint data with a "dirty date". That way we know when
		//to clear flags based on 
		context.SetAckSeqNum((u16)m_iPendingReliableSeqNum+1);

		mp_TypeData->Update( *mp_RootElements, context );

		//If someone accessed the ack we can bump it, though we only
		//really need to do this when we send data so right now we just
		//mark it dirty
		if (context.WasAckUsed())
		{
			m_bAckIsDirty = true;
		}
	}

	//Only up the ack seq when we have different data to send
	if (m_bAckIsDirty || (m_iPendingReliableSeqNum > m_iAckedReliableSeqNum))
	{
		//Wait for 0 seconds (initial packet), 1/6 second, 1/3 second, 1/2 second and finally 1 second between resends
		//Displayf("NSOInstance::Update: Update ack sequence");
		const int kNumAttempts=6;
		const float kfTimeDelays[kNumAttempts] = {0.0f, 1.0f, 1.5, 2.0f, 4.0f, 8.0f};	//Scaling factor on fSendDelayScaler, trying to avoid spamming dodgy connections
		float fDelay = kfTimeDelays[m_iUnAckedSequence] * (fRoundTripTime + 0.1f);
		float fCurrentTime = TIME.GetElapsedTime();
		float fTime = fCurrentTime - m_fTimeOfSend;
		if (fTime >= fDelay)
		{
			TRACE_ACK(Displayf("NSOInstance::Update: Time for message [seqnum: %d, acknum %d]", m_iUnAckedSequence, m_iPendingReliableSeqNum));
			if (m_bAckIsDirty)
			{
				++m_iPendingReliableSeqNum;
				m_bAckIsDirty = false;
			}

			Assert(m_iPendingReliableSeqNum);

			//Inc the sequence number
			if (m_iUnAckedSequence < kNumAttempts-1)
			{
				++m_iUnAckedSequence;
			}
			//else panic - we're waiting a really long time here!

			//We should send the accumulated reliable state
			m_bSendReliable = true;
			m_fTimeOfSend = fCurrentTime;
		}
	}
}

class OnAckVisitor : public NSEVisitor
{
	u16 m_iAckSeqNumber;
public:
	OnAckVisitor(u16 iAckNum)
		:m_iAckSeqNumber( iAckNum )
	{
	}
	void OnElement(NSETypeBase &rElement, NSEInstanceBase * pData, void * /*pObjectRoot*/)
	{
		if (pData && (rElement.GetMode() & SendMode::eReliable))
		{
			if (pData->GetSeqNum() && (pData->GetSeqNum() <= m_iAckSeqNumber))
			{
				TRACE_ACK(Displayf("     ClearAck: %s [%d >= %d]", rElement.GetName(), m_iAckSeqNumber, pData->GetSeqNum()));
				pData->ClearAck();
			}
		}
	}
};

void NSOInstance::OnAck(u16 ackSeqNumber, NSOObjectBase & rObj)
{
	//Store the number
	TRACE_ACK(Displayf("NSOInstance::OnAck [ack=%d]", ackSeqNumber));
	m_iAckedReliableSeqNum = ackSeqNumber;
	m_iUnAckedSequence = 0;		//Reset timers!

	//Clear any acks that are below this seq number
	if (mp_TypeData && mp_RootElements)
	{
		TRACE_ACK(Displayf("NSOInstance::OnAck: [%d]", ackSeqNumber));
		NSEContext context(rObj, *this);
		OnAckVisitor ackIt(ackSeqNumber);
		mp_TypeData->Visit( ackIt, context, *mp_RootElements );
	}
}

void NSOInstance::Visit(NSEVisitor &rVisitor, NSOObjectBase & rObj)
{
	if (mp_TypeData && mp_RootElements)
	{
		NSEContext context(rObj, *this);
		mp_TypeData->Visit( rVisitor, context, *mp_RootElements );
	}
}

void MarkUnackedDirty::OnElement(NSETypeBase &/*rTypeData*/, NSEInstanceBase * pData, void * /*pObjectRoot*/)		
{
	if (pData)
	{
		if (pData->GetSeqNum())
		{
			if (!pData->IsDirty())
			{
				pData->SetDirty(true);
				for (int i=m_iStackDepth-1 ; i>=0 ; i--)
				{
					NSEInstanceBase *pInsData = m_GroupStack[i].mp_Instance;
					if (pInsData)
					{
						if (!pInsData->IsDirty())
						{
							pInsData->SetDirty(true);
						}
					}
				}
			}
		}
	}
}

} // namespace rage

