
// 
// datasync/synchelements.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DATASYNC_SYNCHELEMENTS_H 
#define DATASYNC_SYNCHELEMENTS_H 

#include "atl/array.h"
#include "synchserialize.h"
#include "synchdebug.h"
#include "bank/bank.h"

namespace rage {

//-------------------------------------------------------------------------------------------------
//											NSE
//-------------------------------------------------------------------------------------------------
//NSE stands for Network Sync Element
//-------------------------------------------------------------------------------------------------

class NSOInstance;
class NSOObjectBase;
struct NSEContext;
class NSETypeBase;
class NSETypeRulesBase;
class NSEGroup;
class NSEObjectRoot;
class NSEInstanceBase;

namespace NSOCBs{
	bool ImportCB(NSOObjectBase &rObj, const NSETypeRulesBase& rRule, const NSEInstanceBase& rInstData, u16 iDataID, NSEBufferSerializer& rBuffer);
	bool ExportCB(NSOObjectBase &rObj, const NSETypeRulesBase& rRule, const NSEInstanceBase& rInstData, u16 iDataID, NSEBufferSerializer& rBuffer);
	bool CreateCB(NSOObjectBase &rObj, const NSETypeRulesBase& rRule, const NSEInstanceBase& rInstData, u16 iDataID);
	bool CreateSectionCB(NSOObjectBase &rObj, const NSETypeBase& rSection, const NSEInstanceBase& rInstData);
}

//Base class for parsing the data structure externally
class NSEVisitor
{
public:
	NSEVisitor(){}
	virtual ~NSEVisitor(){};
	virtual bool OnStartGroup(NSEGroup &/*rGroup*/, NSEInstanceBase * /*pData*/)		{	return true;	};
	virtual void OnEndGroup(NSEGroup &/*rGroup*/, NSEInstanceBase * /*pData*/)			{};
	virtual void OnStartObject(NSEObjectRoot &/*rObject*/, NSEInstanceBase * /*pData*/)	{};
	virtual void OnEndObject(NSEObjectRoot &/*rObject*/, NSEInstanceBase * /*pData*/)	{};
	virtual void OnElement(NSETypeBase &/*rObject*/, NSEInstanceBase * /*pData*/, void * /*pObjectRoot*/)		{};
	virtual void OnRule(NSETypeRulesBase &/*rRule*/, NSEInstanceBase * /*pData*/, NSEContext * /*rContext*/, void * /*pObjectData*/) {};
};

//-------------------------------------------------------------------------------------------------
//This is the base class for the instance data (per element). Declared this early to avoid a pointless forward decl.
//-------------------------------------------------------------------------------------------------
class NSEInstanceBase
{
#if !__FINAL
	float m_fTimeStampTime;
#endif
	u16 m_iSeqNum;;								//0 if not awaiting ack, otherwise indicating last seqnum of update request
	u16 m_iPackedBits : 14;						//Number of bits this item will take when serialized

	//u8 m_iFramesWaiting;						//Number of frames we've been marked as dirty
	u16 m_bDirty :1;							//Element needs data broadcast to given client
	u16 m_bNearlyDirty : 1;						//Element needs data broadcast to given client
public:
	NSEInstanceBase(NSEContext &)
		:m_iSeqNum(0)
		,m_iPackedBits(0)						//Until known better
		,m_bDirty(true)							//Data is always dirty until send once
		,m_bNearlyDirty(false)
#if !__FINAL
		,m_fTimeStampTime(0.0f)
#endif
	{}

	template<typename T>
	T& UpCast()
	{	return *(T*)this;		}				//TMS: Can build some type safety in in the future if needed

	void SetDirty(bool b)
	{	m_bDirty = b;			
		if (!b)
		{	m_bNearlyDirty = false;
			//m_iFramesWaiting=0;	
		}
	}
	void SetSeqNum(int iAck)
	{	Assign(m_iSeqNum, iAck);	}

	int GetSeqNum() const
	{	return m_iSeqNum;	}

	void ClearAck()
	{	m_iSeqNum = 0;	}

	void SetNearlyDirty(bool b)
	{	m_bNearlyDirty = b;	}

	bool IsDirty() const
	{	return m_bDirty!=false;		}

	bool IsNearlyDirty() const
	{	return m_bNearlyDirty!=false;		}

	//TMS: This doesn't account for variable sized data based on custom data rules
	int GetPackedDataSize() const 
	{	return m_iPackedBits;	}

	void SetPackedBits(int bits)
	{	m_iPackedBits = bits; 
		Assert((int)m_iPackedBits == bits);
	}

#if !__FINAL
	void StampIt(float fTimeStamp)
	{	m_fTimeStampTime = fTimeStamp;	}

	float GetStamp() const
	{	return m_fTimeStampTime;	}
#endif

};
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//NSEContext is passed around the export and update functions and allows rules an idea of what
//context the element in question lives within.
//-------------------------------------------------------------------------------------------------
struct NSEContext
{
	NSOObjectBase *mp_LocalObject;		//Object (normally a net object shadowing a real game object)
	NSOInstance *mp_Instance;			//Instance of representation that we are updating (client view)
	u64 m_iPeerId;						//Client this data is 
	Vector3 m_vPosition;				//Cached object position (if m_bHasPosition is set)
	float m_fClientObjDist;				//Cached distance from client we are processing rules for to object
	float m_fInvClientObjDist;			//Cached inverse distance from client we are processing rules for to object
	float m_fDataTimeStamp;				//Cached networked time stamp for rules to use when updating
	int m_iContextDepth;
	u16	m_iAckSeqNum;					//Current sequence number for acked data
	bool m_bAccessedAckSeqNum;			//Did someone access the acked sequence number?
	bool m_bHasPosition;				//Object position has been cached for better rules speed
	bool m_bHasDistance;				//Object distance has been cached for better rules speed
	enum {kMaxContexts = 16};
	NSEInstanceBase *mp_ContextStack[kMaxContexts];
public:
	bool HasPosition() const
	{
		return m_bHasPosition;
	}

	u16 GetAckSeqNum()
	{
		m_bAccessedAckSeqNum = true;
		return m_iAckSeqNum;
	}

	bool WasAckUsed() const
	{
		return m_bAccessedAckSeqNum;
	}

	void GetPosition(Vector3 &vOut) const
	{
		Assert(m_bHasPosition);
		vOut = m_vPosition;
	}

	void SetPosition(const Vector3 &vIn)
	{
		m_bHasPosition = true;
		m_vPosition = vIn;
	}

	bool HasDistance() const
	{
		return m_bHasDistance;
	}

	void SetDistance(float fDistObjToClient)
	{
		m_bHasDistance = true;
		m_fClientObjDist = fDistObjToClient;
		m_fInvClientObjDist = (fDistObjToClient>1.0f) ? 1.0f / fDistObjToClient : 1.0f;	//Special case clamping of distances to 1.0f
	}

	float GetInvDistance() const
	{
		Assert(m_bHasDistance);
		return m_fInvClientObjDist;
	}

	float GetDistance() const
	{
		Assert(m_bHasDistance);
		return m_fClientObjDist;
	}

	float GetDataTimeStamp() const
	{
		return m_fDataTimeStamp;
	}

	NSEContext(NSOObjectBase &rLocal, NSOInstance &rInstance, u64 iRemoteClientId)
		:mp_LocalObject(&rLocal)
		,mp_Instance(&rInstance)
		,m_iPeerId(iRemoteClientId)
		,m_iContextDepth(-1)
		,m_fDataTimeStamp(0.0f)
		,m_iAckSeqNum(0)
		,m_bAccessedAckSeqNum(false)
		,m_bHasPosition(false)
		,m_bHasDistance(false)
	{	}

	NSEContext(NSOObjectBase &rLocal, NSOInstance &rInstance, float fTimeStamp, float fDistanceToObject)
		:mp_LocalObject(&rLocal)
		,mp_Instance(&rInstance)
		,m_iPeerId(0)
		,m_iContextDepth(-1)
		,m_fDataTimeStamp(fTimeStamp)
		,m_iAckSeqNum(0)
		,m_bAccessedAckSeqNum(false)
		,m_bHasPosition(false)
		,m_bHasDistance(false)
	{
		if (fDistanceToObject >= 0.0f)
		{
			SetDistance(fDistanceToObject);
		}
	}

	NSEContext(NSOObjectBase &rLocal, NSOInstance &rInstance)
		:mp_LocalObject(&rLocal)
		,mp_Instance(&rInstance)
		,m_iPeerId(0)
		,m_iContextDepth(-1)
		,m_fDataTimeStamp(0.0f)
		,m_iAckSeqNum(0)
		,m_bAccessedAckSeqNum(false)
		,m_bHasPosition(false)
		,m_bHasDistance(false)
	{	}
	
	void PushContext(NSEInstanceBase &rData)
	{
		++m_iContextDepth;
		Assert(m_iContextDepth<kMaxContexts-1);
		mp_ContextStack[m_iContextDepth] = &rData;
	}
	
	void PopContext()
	{	
		--m_iContextDepth;	
	}

	u64 GetPeerId() const
	{
		return m_iPeerId;
	}

	NSOObjectBase* GetObject() const
	{
		return mp_LocalObject;
	}

	//Sets dirty flags progressively up the stack until
	//we find that it is already marked dirty
	void MarkDirtySection()
	{
		for (int i=m_iContextDepth ; i>=0 ; i--)
		{
			//if (mp_ContextStack[i]->IsDirty())
			//{
			//	break;
			//}

			mp_ContextStack[i]->SetDirty(true);
		}
	}

	void SetAckSeqNum(u16 iAckNum)
	{
		m_iAckSeqNum = iAckNum;
	}
};
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//Interface for element types. The type data is made up of a tree of these items
//-------------------------------------------------------------------------------------------------
class NSETypeBase
{
	NSETypeBase *mp_Parent;							//Parent in hierarchical organization

	u8				m_Mode;						//Are we serializing reliably


	bool m_bSneakItIn;
	bool m_bRequiresCreationCallback;
	virtual int GetPackedDataSize() const { return 1; }
	virtual void OnExport	(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer) = 0;
	virtual void OnImport	(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer) = 0;
	virtual void OnUpdate	(NSEInstanceBase &rData, NSEContext &rObj, void *pCurrentDataRoot) = 0;
	
	virtual void OnVisit	(NSEVisitor &rVisitor, NSEInstanceBase *pData, NSEContext *pContext, void *pCurrentDataRoot) = 0;
	BANK_ONLY(virtual void OnAddWidgets(bkBank &){});

#if !__FINAL
	ConstString m_Name;
public:
	int			m_Outbound;							//Count of outbound data
	int			m_Incoming;							//Count of incoming data
	int			m_OutboundBits;						//Amount of outbound data
	int			m_IncomingBits;						//Amount of incoming data
#endif

public:

	NSETypeBase& SetRequiresCreationCallback() { 
		m_bRequiresCreationCallback = true; 
		return *this;
	}
	bool RequiresCreationCallback() const 
	{ return m_bRequiresCreationCallback; 
	}
	bool SneakItIn() const { return m_bSneakItIn; }
	
	void ExportElement(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer, bool bSendSneakyData);
	void ImportElement(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer);

	virtual bool IsContainer()		const {	return false;	}
	BANK_ONLY(virtual NSETypeBase&  SetTrace(bool){return *this;});

#if !__FINAL
	virtual const char * GetDebugDesc	(NSEInstanceBase &/*rData*/, void * /*pObjectRoot*/, bool /*bReciever*/, char *pBuffer, int /*iSizeBuffer*/)
	{
		pBuffer[0] = 0; return pBuffer;
	}

	void ResetCounts()
	{
		m_Outbound = m_Incoming = 0;
		m_OutboundBits = m_IncomingBits = 0;
	}
	void SetName(const char *pName)
	{
		m_Name = pName;
	}
	const char* GetName() const
	{
		return m_Name.c_str();
	}
	BANK_ONLY(virtual void AddWidgets(bkBank &));
#endif


	virtual void AddChildData(NSETypeBase & /*rData*/) {AssertMsg(0,"NSETypeBase derived element does not support children");}
	virtual void SetDataOffset(int /*iOffset*/) {AssertMsg(0,"NSETypeBase does not support data offsets");}
	virtual int GetDataOffset() const {AssertMsg(0,"NSETypeBase does not support data offsets"); return 0;}
	virtual NSETypeBase& SetDataID(u16 /*id*/) {AssertMsg(0,"NSETypeBase does not support data ids"); return *this;}
	void Import	(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer, bool bSkipModeCheck)
	{
		if (bSkipModeCheck || (rBuffer.GetMode() & GetMode()))
		{
	#if !__FINAL
			m_Incoming++;
			m_IncomingBits+=GetPackedDataSize();
			rData.StampIt( rBuffer.GetDataTimeStamp() );
	#endif
			OnImport(rData, rBuffer);
		}
	}

	void Visit(NSEVisitor &rVisitor, NSEInstanceBase *pData=0, NSEContext *pContext=0, void *pCurrentDataRoot=0)
	{
		OnVisit	(rVisitor, pData, pContext, pCurrentDataRoot);
	}

	//If this is called then the data will be sent when any other data in the same group is sent (or on its own rules)
	NSETypeBase& SetSneakItIn()
	{	m_bSneakItIn = true;
		return *this;
	}

	void Export	(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer, bool bSendSneakyData, bool bSkipModeCheck)
	{
		if (rData.IsDirty() || ((SneakItIn() || rData.IsNearlyDirty()) && bSendSneakyData))
		{
			NSE_TRACE_OUT(GetName());
			if (bSkipModeCheck || (rBuffer.GetMode() & GetMode()))
			{
	#if !__FINAL
				int iStartPos = rBuffer.GetCurrentOffset();
	#endif			
				OnExport(rData, rBuffer);
	#if !__FINAL
				m_Outbound++;
				m_OutboundBits+=(rBuffer.GetCurrentOffset() - iStartPos);
				rData.StampIt( rBuffer.GetDataTimeStamp() );
	#endif
				rData.SetDirty(false);
			}
		}
	}

	void Update	(NSEInstanceBase &rData, NSEContext &rContext, void *pCurrentDataRoot)
	{
		if (IsContainer())
		{
			//We are a container so make sure the context stack contains us
			rContext.PushContext(rData);
		}

		OnUpdate(rData, rContext, pCurrentDataRoot);

		if (IsContainer())
		{
			rContext.PopContext();
		}
	}

	virtual NSEInstanceBase* CreateClientData(NSEContext &rContext) const = 0;
	virtual void DestroyClientData(NSEInstanceBase&) const = 0;
	void SetParent(NSETypeBase &rParent)
	{
		AssertMsg(!mp_Parent , "Setting NSETypeBase parent multiple times - this data is not currently considered dynamic or reusable");
		mp_Parent = &rParent;
	}

	u8 GetMode() const
	{
		return m_Mode;
	}

	NSETypeBase& SetMode(SendMode::Mode mode)
	{
		m_Mode = (u8)mode;
		return *this;
	}

	NSETypeBase& AddMode(SendMode::Mode mode)
	{
		m_Mode |= (u8)mode;
		return *this;
	}

	NSETypeBase *GetParent() const
	{	return mp_Parent;	}

	NSETypeBase(SendMode::Mode sendMode=SendMode::eUnReliable)
		:mp_Parent(0)
		,m_bSneakItIn(false)
		,m_bRequiresCreationCallback(false)
		,m_Mode((u8)sendMode)
#if !__FINAL
		,m_Outbound(0)
		,m_Incoming(0)
		,m_OutboundBits(0)
		,m_IncomingBits(0)
#endif
	{	}
	virtual ~NSETypeBase()		{	}
};
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//
//-------------------------------------------------------------------------------------------------
class NSETypeRulesBase
{
public:
	template<typename _DataType>
	bool GetInstData(const NSEInstanceBase &rData, _DataType &rOut) const
	{
		const void *pData = GetDataShadow(rData);
		if (pData)
		{
			Assertf(GetDataShadowSize() == sizeof(rOut), "%d != %d", GetDataShadowSize(), sizeof(rOut));
			if (GetDataShadowSize() == sizeof(rOut))
			{
				rOut = *(_DataType*)pData;
				return true;
			}
		}
		return false;
	}

	BANK_ONLY(virtual void AddWidgets(bkBank &){};);
	virtual void *GetDataShadow(NSEInstanceBase & /*rData*/) const {return 0;}
	virtual const void *GetDataShadow(const NSEInstanceBase & /*rData*/) const {return 0;}
	virtual int GetDataShadowSize() const {return -1;}
	virtual bool Export	(NSEInstanceBase &rData, void *pObjectData, NSEBufferSerializer &rBuffer, bool bSneak) = 0;
	virtual void Import	(NSEInstanceBase &rData, void *pObjectData) = 0;
	virtual void Update(NSEInstanceBase &rData, NSEContext &rObj, void *pObjectData, bool bIsReliable) = 0;
	virtual NSEInstanceBase* CreateClientData(NSEContext &rContext) const = 0;
	virtual void DestroyClientData(NSEInstanceBase&) const = 0;

	virtual ~NSETypeRulesBase()
	{	}
};
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//Object root defines a new start for sub data offsets. This allows us to have sections of objects
//that live in blocks of memory allocated separately.
//-------------------------------------------------------------------------------------------------
class NSEObjectRoot : public NSETypeBase
{
	NSETypeBase *mp_ChildData;			//This is the sync object data that falls within this section

	struct InstanceData : public NSEInstanceBase
	{
		NSOObjectBase *mp_ObjectSectionRoot;		//This is the network object data representation
		NSEInstanceBase *mp_ChildData;	//This is the sync object data that falls within this section
		InstanceData(NSEContext &rCtxt)
			:NSEInstanceBase(rCtxt)
			,mp_ObjectSectionRoot(0)
			,mp_ChildData(0)
		{
		}
		void Destroy()
		{
			delete mp_ChildData;
		}
	};

	virtual void AddChildData(NSETypeBase &rData) {
		AssertMsg(!mp_ChildData,"NSEObjectRoot only supports a single child");
		mp_ChildData = &rData;
		AssertMsg(!rData.GetParent() ,"NSETypeBase derived objects can only have one parent at a time!");
		rData.SetParent(*this);
	}
	virtual void OnExport	(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer);
	virtual void OnImport	(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer);
	virtual void OnUpdate	(NSEInstanceBase &rData, NSEContext &rObj, void *pCurrentDataRoot);
	virtual void OnVisit	(NSEVisitor &rVisitor, NSEInstanceBase *pData, NSEContext *pContext, void *pCurrentDataRoot);

	virtual bool IsContainer() const {return true;}

	virtual NSEInstanceBase* CreateClientData(NSEContext &rContext) const
	{	
		InstanceData *pRet = rage_new InstanceData(rContext);
		if (pRet && mp_ChildData)
		{
			pRet->mp_ObjectSectionRoot = rContext.mp_LocalObject;	//TEMP HACK: When the structure becomes flexible this will need to change!

			//TMS: CreateSectionCBs are used to "trim" needless childdata
			if (	!RequiresCreationCallback()
				||	NSOCBs::CreateSectionCB(*rContext.GetObject(), *this, *pRet)
				)
			{
				pRet->mp_ChildData = mp_ChildData->CreateClientData(rContext);
			}
			else
			{
				pRet->mp_ChildData = 0;
			}
		}
		return pRet;
	}



public:
	NSEObjectRoot()
		:mp_ChildData(0)
	{	}
	virtual ~NSEObjectRoot();
	
	virtual void DestroyClientData(NSEInstanceBase& rBase) const
	{	
		InstanceData *pX = &rBase.UpCast<InstanceData>();
		mp_ChildData->DestroyClientData(*pX->mp_ChildData);
		delete pX;
	}
};
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//TMS: 	I was originally thinking to allocate large blocks of data (i.e. one lump per client per object) but then reaslised
//		that this, while more efficient in terms of CPU, cache and memory utilization was going to be more engineering
//		and run higher risks of memory fragmentation than  a lot of small allocations. This is an implementation detail and
//		should be hidden from higher level code so can be changed if needed at a later date.
//TMS2:	There are some strong similarities to gringos here (they have a pooled memory system though while I'm thinking 
//		to use a memory allocator here). Might be exploitable at a later date once the system is better known.
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//All elements live in groups. Dirty flags are propogated up groups to indicate that deeper data needs parsing
//Groups can be nested - hence derivation from ElementTypeBase
//-------------------------------------------------------------------------------------------------
class NSEGroup : public NSETypeBase
{
	struct InstanceData : public NSEInstanceBase
	{
		atArray<NSEInstanceBase*> m_ClientElements;
		NSEInstanceBase* mp_GroupGuard;
		InstanceData(NSEContext &rCtxt)
			:NSEInstanceBase(rCtxt)
			,mp_GroupGuard(0)
		{}
	};
	atArray<NSETypeBase*> m_Elements;		//Polymorphic array of elements
	NSETypeBase* mp_GroupGuard;				//Guard flag element

	virtual void AddChildData(NSETypeBase &rData) {
		AssertMsg(m_Elements.GetCount() < m_Elements.GetCapacity() , "Overflowing capacity for NSEGroup - please SetSize");
		AssertMsg(!rData.GetParent() , "NSETypeBase derived objects can only have one parent at a time!");
		m_Elements.Append() = &rData;
		rData.SetParent(*this);
	}
	bool CheckGuardFlag(void *pCurrentDataRoot);
	virtual void OnVisit(NSEVisitor &rVisitor, NSEInstanceBase *pData, NSEContext *pContext, void *pCurrentDataRoot);
	virtual void OnUpdate(NSEInstanceBase &rData, NSEContext &rObj, void *pCurrentDataRoot);
	virtual void OnExport(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer);
	virtual void OnImport(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer);
	virtual bool IsContainer() const {return true;}

	virtual NSEInstanceBase* CreateClientData(NSEContext &rContext) const;
public:
	NSEGroup(int iSize, NSETypeBase *pGroupGuard)
		:mp_GroupGuard(pGroupGuard)
	{	SetSize( iSize ); }

	virtual ~NSEGroup();
	
	void SetSize(int iSize)	{
		m_Elements.Reserve(iSize);
	}

	virtual void DestroyClientData(NSEInstanceBase& rBase) const
	{	
		InstanceData *pX = &rBase.UpCast<InstanceData>();
		const int kCount = pX->m_ClientElements.GetCount();
		for (int i=0 ; i<kCount ; i++)
		{
			m_Elements[i]->DestroyClientData(*pX->m_ClientElements[i]);
		}
		if (mp_GroupGuard)
		{
			mp_GroupGuard->DestroyClientData(*pX->mp_GroupGuard);
		}
		delete pX;
	}
};
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//TNSESyncType
//-------------------------------------------------------------------------------------------------
//Templated sync element uses traits to govern the actual synchronization data. We will have a set
//of partially specialized varients for different common schemes
//-------------------------------------------------------------------------------------------------
template <	typename _DataType,
			typename ErrorRulesType, 	typename ErrorRulesInstance,
			typename ScheduleRulesType, typename ScheduleRulesInstance,
			typename InstanceDataBase = NSEInstanceBase
		>
class TNSESyncType : public NSETypeRulesBase
{
public:
	struct InstanceData : public InstanceDataBase
	{
		_DataType				m_Shadow;					//Our shadowed copy of the data
		ErrorRulesInstance		m_ErrorRulesData;		//Per client per data instance error rules data		(error accumulation)
		ScheduleRulesInstance	m_ScheduleRulesData;//Per client per data instance schedule rules data 	(last sent time...)

		InstanceData(NSEContext &rCtxt)
			:InstanceDataBase(rCtxt)
			,m_ErrorRulesData(rCtxt)
			,m_ScheduleRulesData(rCtxt)
		{

		}
	};

	InstanceData &UpCastInstanceData(NSEInstanceBase &rData) const
	{
		return *(InstanceData*)&rData;
	}

	const InstanceData &UpCastInstanceData(const NSEInstanceBase &rData) const
	{
		return *(InstanceData*)&rData;
	}

protected:
	ErrorRulesType &GetErrorRules() { return m_ErrorRulesType; }
	ScheduleRulesType &GetScheduleRules() { return m_ScheduleRules; }

private:
	ErrorRulesType 		m_ErrorRulesType;		//Prioritization management (if any)
	ScheduleRulesType 	m_ScheduleRules;		//min broadcast time, heartbeat (if any)

	//Unfortunately void* to untangle some dependencies and allow external serialization
	virtual void *GetDataShadow(NSEInstanceBase &rData) const
	{
		return &UpCastInstanceData(rData).m_Shadow;
	}

	virtual const void *GetDataShadow(const NSEInstanceBase &rData) const
	{
		return &UpCastInstanceData(rData).m_Shadow;
	}

	virtual int GetDataShadowSize() const
	{
		return sizeof(_DataType);
	}


	//Receive the data from the remote owner
	virtual void Import(NSEInstanceBase &rData, void *pObjectData)
	{
		//Update core value (the false value above allows the client app to handle the
		//updated value distribution itself, for instance when blending to the new value
		*(_DataType*)pObjectData = UpCastInstanceData(rData).m_Shadow;
	}

	//Export the data. 
	//NOTE: This function doesn't send any data, just wraps it up for sending
	virtual bool Export(NSEInstanceBase &rData, void * pObjectData, NSEBufferSerializer &rBuffer, bool bForceSend)
	{
		if (bForceSend || rData.IsDirty())
		{
			_DataType &rObjectData = *(_DataType*)pObjectData;

			InstanceData &rClientData = UpCastInstanceData(rData);
			//Copy the object value into the shadow
			rClientData.m_Shadow = rObjectData;

			//Notify the sync rules that the data has left the building
			float fDataStampTime = rBuffer.GetDataTimeStamp();
			m_ScheduleRules.OnSend( rData, rClientData.m_ScheduleRulesData, fDataStampTime );
			m_ErrorRulesType.OnSend( rData, rClientData.m_ErrorRulesData, fDataStampTime );

			return true;
		}
		return false;
	}

	//Update the filter logic
	virtual void Update(NSEInstanceBase &rData, NSEContext &rContext, void *pObjectData, bool bIsReliable)
	{
		//If masked we are not doing any further updates of rules, just making sure the the
		//existing dirty flags get propagated upwards (This allows the prioritized queues to clear).

		//TMS:	Not quite sure about the reliable data yet - this should make sure they get
		//		decent ack numbers as needed.
		if (!rData.IsDirty() || (bIsReliable && !rData.GetSeqNum()))
		{
			InstanceData &rClientData = UpCastInstanceData(rData);
			if (m_ScheduleRules.CanUpdate( rClientData.m_ScheduleRulesData ))
			{
				//If scheduling wants to send the data we can ignore the error rules, at least for now.
				//It is possible that we could end up backlogged (too much scheduled data) in which case
				//it may need to be prioritized but I suspect we don't need that.
				if (m_ScheduleRules.NeedsUpdate( rData, rClientData.m_ScheduleRulesData, rContext.GetDataTimeStamp() ))
				{
					rData.SetDirty(true);

					//Reliable data needs a sequence number
					if (bIsReliable)
					{
						rData.SetSeqNum( rContext.GetAckSeqNum() );
					}
				}
				else if (m_ScheduleRules.AllowsUpdate( rClientData.m_ScheduleRulesData ))
				{
					//Update any prioritization for distribution, should be no-op if using no error rules... Can tell system to post right 
					//away (or might instruct posting from external prioritization system).
					if (m_ErrorRulesType.Update(
						rData,
						rClientData.m_ErrorRulesData, 
						rClientData.m_Shadow, 
						*(const _DataType*)pObjectData, 
						rContext
						)
						)
					{
						rData.SetDirty(true);

						if (bIsReliable)
						{
							rData.SetSeqNum( rContext.GetAckSeqNum() );
						}
					}
				}
			}
		}

		//If this item became dirty (which could be because of external code!)
		//Then we need to mark the whole section dirty
		if (rData.IsDirty())
		{
			rContext.MarkDirtySection();
		}
	}

#if __BANK
	virtual void AddWidgets(bkBank &bk)
	{
		m_ErrorRulesType.AddWidgets(bk);
		m_ScheduleRules.AddWidgets(bk);
	}
#endif

public:
	TNSESyncType(ErrorRulesType rERT, ScheduleRulesType rSRT)
		:m_ErrorRulesType(rERT)
		,m_ScheduleRules(rSRT)
	{		
	}

	virtual NSEInstanceBase* CreateClientData(NSEContext &rCtxt) const
	{
		return rage_new InstanceData(rCtxt);
	}

	virtual void DestroyClientData(NSEInstanceBase& rBase) const
	{	
		InstanceData *pX = &rBase.UpCast<InstanceData>();
		delete pX;
	}
};
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//The element wrapper is the item contained in the type data structure. This allows sharing of sync
//rules between different elements which have different callbacks, data offsets and serialization rules
//-------------------------------------------------------------------------------------------------

template<typename _DataType, typename Serializer >
class TSyncElemWrapper : public NSETypeBase
{
private:
	NSETypeRulesBase *mp_SyncRuleType;
	Serializer			m_Serializer;			//Serialization of data
	u16 m_iDataOffset;							//Offset of data in object data section
	u16 m_iDataID;								//ID of data for 
	u16 m_iPackedBits;							//Amount of bit buffer the data will need

	bool			m_bSneakItIn;				//Should we pack in with any other data in our group?
	BANK_ONLY(bool	m_bTrace;)					//Output this data
	virtual int GetPackedDataSize() const 
	{
		return m_iPackedBits;
	}

	virtual void SetDataOffset(int iOffset)
	{
		Assign(m_iDataOffset, iOffset);
	}

	virtual int GetDataOffset() const
	{
		return m_iDataOffset;
	}

	virtual void *GetObjectData(void *pObjectRoot)
	{
		return ((unsigned char *)pObjectRoot) + m_iDataOffset;
	}

	virtual void *GetObjectData(NSESerializer &rBuffer)
	{
		return GetObjectData(rBuffer.GetCurrentObjectRoot());
	}

	_DataType& GetTypedObjectData(NSESerializer &rBuffer)
	{
		return *(_DataType*)GetObjectData(rBuffer);
	}

	_DataType& GetTypedObjectData(void *pObjectRoot)
	{
		return *(_DataType*)GetObjectData(pObjectRoot);
	}

	_DataType& GetShadowData(NSEInstanceBase &rData)
	{
		return *(_DataType*)mp_SyncRuleType->GetDataShadow(rData);
	}

	virtual void OnImport(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer)
	{
		//Unpack to shadow
		_DataType &rShadowData = GetShadowData(rData);
		_DataType &rObjectData = GetTypedObjectData(rBuffer);
		m_Serializer.Import(rShadowData, rObjectData, rBuffer);

		Assert(rBuffer.GetCurrentObjectRoot());
		if (	(m_iDataID==0xffff) 
			||	NSOCBs::ImportCB(*rBuffer.GetCurrentObjectRoot(), *mp_SyncRuleType, rData, m_iDataID, rBuffer)
			)
		{
			mp_SyncRuleType->Import(rData, &rObjectData);

#if __BANK
			if (m_bTrace)
			{
				char desc[256];
				GetDebugDesc(rData, rBuffer.GetCurrentObjectRoot(), true, desc, sizeof(desc));
				Displayf("NSETRACE: OnImport: %s: %s", GetName(), desc);
			}
#endif
		}
	}

	virtual const char* GetDebugDesc	(NSEInstanceBase &rData, void *pObjectRoot, bool bReciever, char *pBuffer, int iSizeBuffer)
	{
		_DataType &rObjectData = GetTypedObjectData(pObjectRoot);
		_DataType &rShadowData = GetShadowData(rData);
		if (bReciever || (rObjectData == rShadowData))
		{
			char bufferA[256];
			m_Serializer.Print(rObjectData, bufferA, sizeof(bufferA));
			formatf(pBuffer, iSizeBuffer, "%s", bufferA);
		}
		else
		{
			char bufferA[256];
			char bufferB[256];
			m_Serializer.Print(rObjectData, bufferA, sizeof(bufferA));
			m_Serializer.Print(rShadowData, bufferB, sizeof(bufferB));
			formatf(pBuffer, iSizeBuffer, "%s [sent: %s]", bufferA , bufferB);
		}
		return pBuffer;
	}

	virtual void OnExport(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer)
	{
		void *pObjectData = GetObjectData(rBuffer);
		if (mp_SyncRuleType->Export(rData, pObjectData, rBuffer, SneakItIn()))
		{
#if __BANK
			if (m_bTrace)
			{
				char desc[256];
				GetDebugDesc(rData, rBuffer.GetCurrentObjectRoot(), false, desc, sizeof(desc));
				Displayf("NSETRACE: OnExport: %s: %s", GetName(), desc);
			}
#endif
			_DataType &rShadowData = GetShadowData(rData);
			//Seralize current object data
			m_Serializer.Export(rShadowData, *(_DataType*)pObjectData, rBuffer);

			NSOCBs::ExportCB(*rBuffer.GetCurrentObjectRoot(), *mp_SyncRuleType, rData, m_iDataID, rBuffer);
		}
	}

	virtual void OnUpdate(NSEInstanceBase &rData, NSEContext &rContext, void *pCurrentObjectRoot)
	{
		mp_SyncRuleType->Update(rData, rContext, GetObjectData(pCurrentObjectRoot), (GetMode() & SendMode::eReliable) != 0 );
	}

	virtual void OnVisit	(NSEVisitor &rVisitor, NSEInstanceBase *pData, NSEContext *pContext, void *pCurrentObjectRoot)
	{
		rVisitor.OnElement(*this, pData, pCurrentObjectRoot);

		if (pCurrentObjectRoot)
		{
			rVisitor.OnRule(*mp_SyncRuleType, pData, pContext, GetObjectData(pCurrentObjectRoot));
		}
		else
		{
			rVisitor.OnRule(*mp_SyncRuleType, pData, pContext, 0);
		}
	}


#if __BANK
	virtual NSETypeBase&  SetTrace(bool bOn)
	{
		m_bTrace = bOn;
		return *this;
	}
	virtual void OnAddWidgets(bkBank &bk)
	{
		bk.AddToggle("Trace", &m_bTrace);
		mp_SyncRuleType->AddWidgets(bk);
	}
#endif

	void Init()
	{
		BANK_ONLY(m_bTrace=false;);
		NSEBitCountSerializer count;
		_DataType tmp;
		m_Serializer.Import(tmp, tmp, count);
		Assign(m_iPackedBits, count.GetBitsCounted());	//Will return bits recorded since StartBitCount
	}
public:
	TSyncElemWrapper(NSETypeRulesBase &rRules, Serializer rSer, SendMode::Mode sendMode=SendMode::eUnReliable)
		:NSETypeBase(sendMode)
		,mp_SyncRuleType(&rRules)
		,m_Serializer(rSer)
		,m_bSneakItIn(false)
		,m_iDataID(0xffff)
	{
		Init();
	}

	TSyncElemWrapper(NSETypeRulesBase &rRules, SendMode::Mode sendMode=SendMode::eUnReliable)
		:NSETypeBase(sendMode)
		,mp_SyncRuleType(&rRules)
		,m_Serializer(Serializer())
		,m_iDataID(0xffff)
	{
		Init();
	}

	virtual void Update(NSEInstanceBase &rData, NSEContext &rObj, void *pCurrentObjectRoot)
	{
		mp_SyncRuleType->Update(rData, rObj, pCurrentObjectRoot, GetMode()==SendMode::eReliable);
	}

	virtual NSEInstanceBase* CreateClientData(NSEContext &rContext) const
	{
		NSEInstanceBase* pRet = mp_SyncRuleType->CreateClientData(rContext);
		if (pRet)
		{
			pRet->SetPackedBits(m_iPackedBits);
			if (RequiresCreationCallback())
			{
				NSOCBs::CreateCB(*rContext.GetObject(), *mp_SyncRuleType, *pRet, m_iDataID);
			}
		}
		return pRet;
	}

	virtual void DestroyClientData(NSEInstanceBase &rData) const
	{
		mp_SyncRuleType->DestroyClientData(rData);
	}

	NSETypeBase& SetDataID(u16 id)
	{	m_iDataID = id;
		return *this;
	}
};

} // namespace rage

#endif // DATASYNC_SYNCHELEMENTS_H 
