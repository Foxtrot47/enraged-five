// 
// datasync/synchmgr.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DATASYNC_SYNCHOBJECT_H 
#define DATASYNC_SYNCHOBJECT_H 

#include "synchelements.h"
#include "synchserialize.h"
#include "atl/array.h"
#include "diag/tracker.h"

namespace rage {

class bkBank;

#define VALIDATE_QUEUE 0

#if VALIDATE_QUEUE
	#define VALIDATE_ONLY(x) ASSERT_ONLY(x)
#else
	#define VALIDATE_ONLY(x)
#endif

class NSEPresenter;
struct NSEBufferSerializer;

//-------------------------------------------------------------------------------------------------
//											NSO
//-------------------------------------------------------------------------------------------------
//NSO stands for Network Sync Object
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//NSOObjectBase: Objects that have synchronized data derive from this
//-------------------------------------------------------------------------------------------------
class NSOObjectBase
{
public:
	//Override for user defined data hookups (called when instance data is created, if SetRequiresCreationCallback has been called)
	virtual bool OnNewData(const NSETypeRulesBase& /*rRule*/, const NSEInstanceBase& /*rInstData*/, u16 /*iDataID*/){ return true; }

	//Override for user defined group hookup (called when instance data is created, if SetRequiresCreationCallback has been called)
	//Return FALSE to prevent child data being created
	virtual bool OnNewGroup(const NSETypeBase& /*rSection*/, const NSEInstanceBase& /*rInstData*/) { return true; }

	//Called when new data is imported, return FALSE to prevent auto copy of data. Called is data id is set on instance data
	//Can be used to serialize custom data
	virtual bool OnSyncDataImport(const NSETypeRulesBase& /*rRule*/, const NSEInstanceBase& /*rInstData*/, u16 /*iDataID*/, NSEBufferSerializer &/*rBuffer*/){ return true; }

	//Called when data is exported, return FALSE to prevent auto copy of data. Called is data id is set on instance data
	//Can be used to serialize custom data
	virtual bool OnSyncDataExport(const NSETypeRulesBase& /*rRule*/, const NSEInstanceBase& /*rInstData*/, u16 /*iDataID*/, NSEBufferSerializer &/*rBuffer*/){ return true; }

	//C/DTOR
	NSOObjectBase()
	{}
	virtual ~NSOObjectBase(){}
};
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//NSOType is the root class of a given type (actor, prop etc) that uses sync data. The data contained
//in this will be constant besides any stats tracking.
//-------------------------------------------------------------------------------------------------
class NSOType
{
	NSETypeBase *mp_RootItem;
public:

	NSEInstanceBase* CreateInstanceData(NSEContext &rContext) const;
	void DestroyInstanceData(NSEInstanceBase &rInst) const;
	void Update( NSEInstanceBase &rInst, NSEContext &rObj) const;
	void Import( NSEInstanceBase &rInst, NSEBufferSerializer &rBuffer) const;
	void Export( NSEInstanceBase &rInst, NSEBufferSerializer &rBuffer) const;
	void Visit( NSEVisitor &rVisitor ) const;
	void Visit( NSEVisitor &rVisitor, NSEContext &context, NSEInstanceBase &rInst ) const;
	void SetupFromPresenter(NSEPresenter &rPresenter);
	BANK_ONLY(void AddWidgets(bkBank& bk));

	NSOType()
		:mp_RootItem(0)
	{}
};
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//NSOInstance is the root class for instance sync data - every unique view of the object will need to
//contain one of these.
//-------------------------------------------------------------------------------------------------
class NSOInstance
{
	const NSOType *mp_TypeData;
	NSEInstanceBase *mp_RootElements;

	//Reliable message sequencing information
	u32 m_iPendingReliableSeqNum;	//Used for send only
	u32 m_iAckedReliableSeqNum;		//For receiving this maintains sanity
	float m_fTimeOfSend;			//Used for send only
	u8 m_iUnAckedSequence;			//Used for send only, increments timeout
	bool m_bSendReliable;			//Used for send only - set when we need to send reliable data
	bool m_bAckIsDirty;				//We'll need a new ack number because we're adding data
	NSOInstance(NSOType &rType, NSOObjectBase &rObj, u64 iRemoteTarget)
		:mp_TypeData(&rType)
		,mp_RootElements(0)
		,m_iPendingReliableSeqNum(0)
		,m_iAckedReliableSeqNum(0)
		,m_fTimeOfSend(0.0f)
		,m_iUnAckedSequence(0)
		,m_bSendReliable(false)
		,m_bAckIsDirty(false)
	{
		NSEContext context(rObj, *this, iRemoteTarget);
		Create(rType, context);
	}

	NSOInstance(NSOType &rType, NSOObjectBase &rObj)
		:mp_TypeData(&rType)
		,mp_RootElements(0)
		,m_iPendingReliableSeqNum(0)
		,m_iAckedReliableSeqNum(0)
		,m_fTimeOfSend(0.0f)
		,m_iUnAckedSequence(0)
		,m_bSendReliable(false)
		,m_bAckIsDirty(false)
	{
		NSEContext context(rObj, *this);
		Create(rType, context);
	}

public:
	bool SendReliable() const
	{ return m_bSendReliable; }

	void OnSentReliable()
	{	m_bSendReliable = false;	}

	//Callback on received reliable ack
	void OnAck(u16 ackSeqNumber, NSOObjectBase & rObj);

	u32 GetPendingReliableSeq() const
	{	return m_iPendingReliableSeqNum;	}

	u32 GetNewPendingReliableSeq() const
	{	return m_iPendingReliableSeqNum;	}

	void BumpReliableSeq()
	{	m_iPendingReliableSeqNum ++;	}


	bool IsDirty() const;
	bool Create(const NSOType &rType, NSEContext &rContext);
	void Destroy();
	void Update(NSOObjectBase &rObj, float fDataTimeStamp, float fDistanceToObject=1.0f, float fRoundTripTime=0.0f);
	int Export(NSEBufferSerializer &rBuffer) const;
	int Import(NSEBufferSerializer &rBuffer) const;
	void Visit(NSEVisitor &rVisitor, NSOObjectBase & rObj);

	static NSOInstance* CreateTransmitter(NSOType &rType, NSOObjectBase &rObj, u64 iRemoteTarget)
	{
		return rage_new NSOInstance(rType, rObj, iRemoteTarget);
	}

	static NSOInstance* CreateReceiver(NSOType &rType, NSOObjectBase &rObj)
	{
		return rage_new NSOInstance(rType, rObj);
	}

	NSOInstance()
		:mp_TypeData(0)
		,mp_RootElements(0)
	{
	}

	~NSOInstance()
	{
		Destroy();
	}
};
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//Helper class that maintains a stack of container objects (instance and type data as applicable
//-------------------------------------------------------------------------------------------------
class StackedVisitor : public NSEVisitor
{
protected:
	struct StackItem
	{
		NSETypeBase *mp_Type;
		NSEInstanceBase *mp_Instance;
	};
	enum {kMaxStackDepth=16};
	StackItem m_GroupStack[kMaxStackDepth];
	int m_iStackDepth;

public:
	StackedVisitor()
		:m_iStackDepth(0)
	{
	}
	virtual void OnStartObject(NSEObjectRoot &rObject, NSEInstanceBase * pData)	
	{
		m_GroupStack[m_iStackDepth].mp_Type = &rObject;
		m_GroupStack[m_iStackDepth].mp_Instance = pData;
		++m_iStackDepth;
	}
	virtual void OnEndObject(NSEObjectRoot &/*rObject*/, NSEInstanceBase * /*pData*/)
	{
		--m_iStackDepth;
	}

	virtual bool OnStartGroup(NSEGroup &rGroup, NSEInstanceBase * pData)		
	{	
		m_GroupStack[m_iStackDepth].mp_Type = &rGroup;
		m_GroupStack[m_iStackDepth].mp_Instance = pData;
		++m_iStackDepth;
		return true;	
	}
	virtual void OnEndGroup(NSEGroup &/*rGroup*/, NSEInstanceBase * /*pData*/)			
	{
		--m_iStackDepth;
	}
};
//-------------------------------------------------------------------------------------------------

class MarkUnackedDirty : public StackedVisitor
{
	virtual void OnElement(NSETypeBase &/*rTypeData*/, NSEInstanceBase * pData, void * /*pObjectRoot*/);
public:
	MarkUnackedDirty(){}
};

} // namespace rage

#endif // DATASYNC_SYNCHOBJECT_H 
