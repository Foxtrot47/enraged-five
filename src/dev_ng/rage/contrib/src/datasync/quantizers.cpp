// 
// DataSync/quantizers.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "quantizers.h"

namespace rage {
	namespace Quantizers {

	float GetQuantizationAccuracy(float fMin, float fMax, int iNumBits)
	{
		float fStepsF = (float)(1<<iNumBits);
		float fMinValue = (fMax - fMin) * 1.0f / fStepsF;
		return fMinValue;
	}

	u32 QuantizeNormalizedVector(const Vector3 &_v)
	{
		Vector3 v = _v;
		//Smallest 2 quantization (Vector3 assumed normalized)
		u32 iLargest = 0;
		float fLargest = v.x;
		float fLargestAbs = Abs(fLargest);

		float fTest = v.y;
		float fTestAbs = Abs(fTest);
		if (fTestAbs > fLargestAbs)
		{
			iLargest = 1;
			fLargest = fTest;
			fLargestAbs = fTestAbs;
		}
		fTest = v.z;
		fTestAbs = Abs(fTest);
		if (fTestAbs > fLargestAbs)
		{
			iLargest = 2;
			fLargest = fTest;
			fLargestAbs = fTestAbs;
		}


		//Serialize the index of the largest value and the flip
		u32 iRet = iLargest<<30;

		//Do we need to flip the vector?
		if ((fLargest > 0) ^ (fLargestAbs > 0))
		{
			iRet |= (1 << 29);
		}

		//29 bits left for the remaining two values
		float fRemaining[2];
		switch(iLargest)
		{
		case 0:
			fRemaining[0] = v.y;
			fRemaining[1] = v.z;
			break;
		case 1:
			fRemaining[0] = v.x;
			fRemaining[1] = v.z;
			break;
		default:
			fRemaining[0] = v.x;
			fRemaining[1] = v.y;
			break;
		}

		//Quantize each value from -0.707 to 0.707
		const float fOneOverSqrt2 = 1.0f / sqrtf(2.0f);
		Assert(Abs(fRemaining[0]) <= fOneOverSqrt2);
		Assert(Abs(fRemaining[1]) <= fOneOverSqrt2);
		u32 iTmpVal;
		iTmpVal = Quantizers::Quantize<u32>(fRemaining[0], -fOneOverSqrt2, fOneOverSqrt2, 15);
		iRet |= iTmpVal<<14;
		iTmpVal = Quantizers::Quantize<u32>(fRemaining[1], -fOneOverSqrt2, fOneOverSqrt2, 14);
		iRet |= iTmpVal;
		return iRet;
	}

	void DeQuantizeNormalizedVector(Vector3 &v, u32 iVal)
	{
		//Smallest 3 quantization (Quaternion assumed normalized)

		//Serialize the index of the largest value
		u32 iLargest = iVal>>30;
		bool bFlip = (iVal>>29) & 1 ? true : false;

		float fOthers[2];
		
		//Quantize each value from -0.707 to 0.707
		const float fOneOverSqrt2 = 1.0f / sqrtf(2.0f);
		float fTotal2=0.0f;

		u32 iTmpVal;
		float fValue;
		iTmpVal = (iVal>>14) & ((1<<15) - 1);
		fValue = Quantizers::DeQuantize<u32>(iTmpVal, -fOneOverSqrt2, fOneOverSqrt2, 15);
		fTotal2 += fValue * fValue;
		fOthers[0] = fValue;
		iTmpVal = (iVal) & ((1<<14) - 1);
		fValue = Quantizers::DeQuantize<u32>(iTmpVal, -fOneOverSqrt2, fOneOverSqrt2, 14);
		fTotal2 += fValue * fValue;
		fOthers[1] = fValue;

		//Recalculate the largest value (quantization defined it as being positive by flipping the quaternion if needed)
		float fLargest = sqrtf(1.0f - fTotal2);
		if (bFlip)
			fLargest = -fLargest;

		//Fill the quaternion
		switch(iLargest)
		{
		case 0:
			v.Set(fLargest, fOthers[0], fOthers[1]);
			break;
		case 1:
			v.Set(fOthers[0], fLargest, fOthers[1]);
			break;
		case 2:
			v.Set(fOthers[0], fOthers[1], fLargest);
			break;
		}
		v.Normalize();
	}

	u32 QuantizeQuaternionS3(const Quaternion &_v)
	{
		Quaternion v = _v;
		//Smallest 3 quantization (Quaternion assumed normalized)
		u32 iLargest = 0;
		float fLargest = v.x;
		float fLargestAbs = Abs(fLargest);


		float fTest = v.y;
		float fTestAbs = Abs(fTest);
		if (fTestAbs > fLargestAbs)
		{
			iLargest = 1;
			fLargest = fTest;
			fLargestAbs = fTestAbs;
		}
		fTest = v.z;
		fTestAbs = Abs(fTest);
		if (fTestAbs > fLargestAbs)
		{
			iLargest = 2;
			fLargest = fTest;
			fLargestAbs = fTestAbs;
		}
		fTest = v.w;
		fTestAbs = Abs(fTest);
		if (fTestAbs > fLargestAbs)
		{
			iLargest = 3;
			fLargest = fTest;
			fLargestAbs = fTestAbs;
		}

		//Do we need to flip the quaternion?
		if ((fLargest > 0) ^ (fLargestAbs > 0))
		{
			v.Negate();
		}

		//Serialize the index of the larget value
		u32 iRet = iLargest<<30;

		float fRemaining[3];
		switch(iLargest)
		{
		case 0:
			fRemaining[0] = v.y;
			fRemaining[1] = v.z;
			fRemaining[2] = v.w;
			break;
		case 1:
			fRemaining[0] = v.x;
			fRemaining[1] = v.z;
			fRemaining[2] = v.w;
			break;
		case 2:
			fRemaining[0] = v.x;
			fRemaining[1] = v.y;
			fRemaining[2] = v.w;
			break;
		default:
			fRemaining[0] = v.x;
			fRemaining[1] = v.y;
			fRemaining[2] = v.z;
			break;
		}

		//Quantize each value from -0.707 to 0.707
		const float fOneOverSqrt2 = 1.0f / sqrtf(2.0f);
		Assert(Abs(fRemaining[0]) <= fOneOverSqrt2);
		Assert(Abs(fRemaining[1]) <= fOneOverSqrt2);
		Assert(Abs(fRemaining[2]) <= fOneOverSqrt2);
		u32 iTmpVal;
		iTmpVal = Quantizers::Quantize<u32>(fRemaining[0], -fOneOverSqrt2, fOneOverSqrt2, 10);
		iRet |= iTmpVal<<20;
		iTmpVal = Quantizers::Quantize<u32>(fRemaining[1], -fOneOverSqrt2, fOneOverSqrt2, 10);
		iRet |= iTmpVal<<10;
		iTmpVal = Quantizers::Quantize<u32>(fRemaining[2], -fOneOverSqrt2, fOneOverSqrt2, 10);
		iRet |= iTmpVal;
		return iRet;
	}

	void DeQuantizeQuaternionS3(Quaternion &v, u32 iVal)
	{
		//Smallest 3 quantization (Quaternion assumed normalized)

		//Serialize the index of the larget value
		u32 iLargest = iVal>>30;
		float fOthers[3];
		
		//Quantize each value from -0.707 to 0.707
		const float fOneOverSqrt2 = 1.0f / sqrtf(2.0f);
		float fTotal2=0.0f;

		u32 iTmpVal;
		float fValue;
		const int iTenBits = ((1<<10) - 1);
		iTmpVal = (iVal>>20) & iTenBits;
		fValue = Quantizers::DeQuantize<u32>(iTmpVal, -fOneOverSqrt2, fOneOverSqrt2, 10);
		fTotal2 += fValue * fValue;
		fOthers[0] = fValue;
		iTmpVal = (iVal>>10) & iTenBits;
		fValue = Quantizers::DeQuantize<u32>(iTmpVal, -fOneOverSqrt2, fOneOverSqrt2, 10);
		fTotal2 += fValue * fValue;
		fOthers[1] = fValue;
		iTmpVal = iVal & iTenBits;
		fValue = Quantizers::DeQuantize<u32>(iTmpVal, -fOneOverSqrt2, fOneOverSqrt2, 10);
		fTotal2 += fValue * fValue;
		fOthers[2] = fValue;

		//Recalculate the largest value (quantization defined it as being positive by flipping the quaternion if needed)
		float fLargest = sqrtf(1.0f - fTotal2);

		//Fill the quaternion
		switch(iLargest)
		{
		case 0:
			v.Set(fLargest, fOthers[0], fOthers[1], fOthers[2]);
			break;
		case 1:
			v.Set(fOthers[0], fLargest, fOthers[1], fOthers[2]);
			break;
		case 2:
			v.Set(fOthers[0], fOthers[1], fLargest, fOthers[2]);
			break;
		case 3:
			v.Set(fOthers[0], fOthers[1], fOthers[2], fLargest);
			break;
		}
	}
	} // namespace Quantizers

} // namespace rage
