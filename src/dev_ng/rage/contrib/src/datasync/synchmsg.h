// 
// datasync/synchmsg.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DATASYNC_SYNCHMSG_H 
#define DATASYNC_SYNCHMSG_H 

#include "net/message.h"
#include "atl/delegate.h"
#include "data/bitbuffer.h"
#include "synchdebug.h"


namespace rage {

class NSOInstance;
class bkBank;
class NetSyncDataMsg
{
	datExportBuffer *mp_ExpBuffer;
	u32	m_iObjectId;
	u32 m_AckNum;
	float m_fNetTime;
	bool m_bHasTimeStamp;
	bool m_bReliable;

	//Purpose allow derived classes to handle sending data
	virtual void OnSendMsg() {};

	//Incoming messages need a way to find their NSOInstance that will
	//receive the data
	virtual NSOInstance* OnGetReciever(u32 /*net_id*/) const { return 0; };

	//Incoming messages need a way to know the current networked time
	virtual float OnGetNetTime() const { AssertMsg(0 , "Please implement this function in the derived class!"); return 0.0f; };

	void ReadData(datImportBuffer &rBuff, bool bIsReliable) const;
protected:
	void SetReliableInfo(bool bReliable, int iAckNum)
	{
		m_bReliable = bReliable;
		m_AckNum = iAckNum;
	}

	bool IsReliable() const 
	{
		return m_bReliable;
	}

	u32 GetObjectID() const
	{
		return m_iObjectId;
	}
	
	u32 GetAckNum() const
	{
		return m_AckNum;
	}

public:
#if __BANK
	struct Stats
	{
		struct StatsItem
		{
			enum {kNumStatsFrames = 32};
			int m_Records[kNumStatsFrames];
			float m_fFrameTime[kNumStatsFrames];
			int m_iCurrent;
			int m_iMax;
			int m_iBPS;
			int m_iMaxBPS;
			int m_iFrame;
			StatsItem()
				:m_iCurrent(0)
				,m_iMax(0)
				,m_iBPS(0)
				,m_iMaxBPS(0)
				,m_iFrame(0)
			{
				for (int i=0; i<kNumStatsFrames ; i++)
				{
					m_Records[i] = 0;
					m_fFrameTime[i] = 0.0f;
				}
			}
			void AddWidgets(bkBank &bk, const char *pName);
			void FrameMarker();
		};

		StatsItem m_SentReliable;
		StatsItem m_SentUnReliable;
		StatsItem m_Received;

		void FrameMarker()
		{
			m_SentReliable.FrameMarker();
			m_SentUnReliable.FrameMarker();
			m_Received.FrameMarker();
		}
		void AddWidgets(bkBank &bk);
	};
	static Stats m_Stats;
#endif	
	
#if __BANK
	static void AddTypeWidgets(bkBank &bk);

	static void StatsFrameMarker();
#endif
	//Used to send messages, relies on derived OnSendMsg to do actual
	//legwork sending data
	void SendMessage(NSOInstance &rSyncData);

	NET_MESSAGE_DECL( NetSyncDataMsg );
	NetSyncDataMsg()
		:mp_ExpBuffer(0)
		,m_iObjectId(0)
		,m_AckNum(0)
		,m_fNetTime(0.0f)
		,m_bReliable(false)
		,m_bHasTimeStamp(false)
	{}
	NetSyncDataMsg(const u32 iObjectId, float fNetTime)
		:mp_ExpBuffer(0)
		,m_iObjectId(iObjectId)
		,m_AckNum(0)
		,m_fNetTime(fNetTime)
		,m_bReliable(false)
		,m_bHasTimeStamp(false)
	{	
	}
	NetSyncDataMsg(const u32 iObjectId, float fNetTime, datExportBuffer &rExportedBuffer)
		:mp_ExpBuffer(&rExportedBuffer)
		,m_iObjectId(iObjectId)
		,m_AckNum(0)
		,m_fNetTime(fNetTime)
		,m_bReliable(false)
		,m_bHasTimeStamp(false)
	{	
	}
	virtual ~NetSyncDataMsg() {}
	NET_MESSAGE_SER( bb, msg )
	{
		bool bRet = bb.SerUns(msg.m_iObjectId, 32);
		if (bRet)
		{
			bb.SerBool(msg.m_bReliable);
			if (msg.m_bReliable)
			{
				bb.SerUns(msg.m_AckNum, 14);
			}

			//TMS:	This is a hack - or at least more complicated than it needs to be
			//		(requiring abuse of the write buffer to store the need for time).
			//		Instead we should bundle up sync messages together.
			bb.SerBool(msg.m_bHasTimeStamp);
			if (msg.m_bHasTimeStamp)
			{
				bb.SerFloat(msg.m_fNetTime);
			}
			if (!bb.IsReadOnly())
			{
				Assert(msg.mp_ExpBuffer);
				if (msg.mp_ExpBuffer)
				{
					(*(datExportBuffer*)&bb).WriteBits(msg.mp_ExpBuffer->GetReadWriteBits(), msg.mp_ExpBuffer->GetCursorPos(), 0);
				}
			}
			else
			{
				if (msg.m_bReliable)
				{
					TRACE_ACK(Displayf("Reliable recieved [%d]", msg.m_AckNum));
				}
				msg.ReadData(*(datImportBuffer*)&bb, msg.m_bReliable);
			}
		}
		return bRet;
	}
};

} // namespace rage

#endif // DATASYNC_SYNCHMSG_H 
