// 
// datasync/synchpresent.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DATASYNC_SYNCHPRESENT_H 
#define DATASYNC_SYNCHPRESENT_H 

#include "synchelements.h"
#if __FINAL
#define NOT_FINAL(x) 
#else
#define NOT_FINAL(x) x
#endif

namespace rage {

class NSETypeBase;

class NSEPresenter
{
	enum {kMaxStackedObjects=8};
	enum {kMaxTrackedElements=256};

	struct BlockInfo
	{
		const char *mp_BlockName;
		const bool *mp_BlockGuard;
		int m_iFirstOffset;
		bool m_bIsGroup;
		bool m_bSectionCB;
		BlockInfo()
			:mp_BlockName(0)
			,mp_BlockGuard(0)
			,m_iFirstOffset(0)
			,m_bIsGroup(false)
			,m_bSectionCB(false)
		{
		}
	};
	BlockInfo m_BlockStack[kMaxStackedObjects];
	NSETypeBase* mp_TrackedElements[kMaxTrackedElements];
	NSETypeBase* mp_TypeRoot;
	int m_iCurrentDepth;
	int m_iCurrentTrackedOffset;

	template<typename _DataType>
	static inline void SetupElement(const _DataType &rValue, NSETypeBase &rElementType)
	{
		//rValue is presumed sent in as offset from ZERO!
		//*((ClassName*0)->m_fMyFloatExample)
		int iOffset = (int)((unsigned char*)&rValue);
		rElementType.SetDataOffset(iOffset);
	}

	template<typename T>
	bool PresentCore(const char * NOT_FINAL(pElemName), const T &rValue, NSETypeBase & rElementType)
	{
		SetupElement(rValue, rElementType);
		mp_TrackedElements[m_iCurrentTrackedOffset] = &rElementType;
		NOT_FINAL(rElementType.SetName(pElemName));
		++m_iCurrentTrackedOffset;
		return true;
	}

	bool StartBlock(const char * /*pBlockName*/, bool bIsGroup, const bool *pGroupGuard=0);	//Block name is stats only
	NSETypeBase &EndBlock();

	bool StartGroupInternal(const char *pBlockName, const bool *pGroupGuard);
public:
	NSEPresenter()
		:mp_TypeRoot(0)
		,m_iCurrentDepth(-1)
		,m_iCurrentTrackedOffset(0)
	{
	}

	virtual ~NSEPresenter()
	{
		Assertf(m_iCurrentDepth==-1, "You are probably missing a matching Start/End group pair (%s most likely)", m_iCurrentDepth>-1 ? "End" : "Start");
	}

	bool StartGroup(const char *pBlockName)
	{
		return StartGroupInternal(pBlockName, 0);
	}
	bool StartGroup(const char *pBlockName, const bool &rGroupGuard)
	{
		return StartGroupInternal(pBlockName, &rGroupGuard);
	}
	NSETypeBase &EndGroup();

	bool StartObject(const char *pBlockName);
	NSETypeBase &EndObject();

	NSETypeBase* GetRoot() const
	{
		return mp_TypeRoot;
	}

	template<typename T>
	NSETypeBase &Present(const char *pElemName, const T &rValue, NSETypeBase & rElementType)
	{	
		PresentCore(pElemName, rValue, rElementType);
		return rElementType;
	}
};

} // namespace rage
#endif // DATASYNC_SYNCHPRESENT_H 
