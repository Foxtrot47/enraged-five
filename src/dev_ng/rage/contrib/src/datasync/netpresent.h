// 
// datasync/netpresent.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DATASYNC_NETPRESENT_H 
#define DATASYNC_NETPRESENT_H 

#include "synchrules.h"
#include "synchpresent.h"

namespace rage {

class Vector3;

class NetPresenter : public NSEPresenter
{
protected:

	template<typename T>
	struct SerializeBits
	{
		void Print(const T &rValue, char *pBuffer, int iSizeBuffer)
		{
			formatf(pBuffer, iSizeBuffer, "%d", (int)rValue);
		}

		int m_iNumBits;
		void Export(T &value, const T &/*objectvalue*/, NSESerializer& rBuffer)
		{
			rBuffer.SerValue(value, m_iNumBits);
		}
		void Import(T &value, const T &/*objectvalue*/, NSESerializer& rBuffer)
		{
			rBuffer.SerValue(value, m_iNumBits);
		}
		SerializeBits(int iNumBits)
			:m_iNumBits(iNumBits)
		{
		}
	};

	struct Helpers
	{
		template<typename TElem>
		struct Simple
		{
			typedef TSyncElemWrapper< TElem, SimpleSerialize<TElem> > Elem;
		};

		template<typename TElem>
		struct Quantized
		{
			typedef TSyncElemWrapper< TElem, QuantizeInRange<TElem> > Elem;
		};

		template<typename TElem>
		struct SomeBits
		{
			typedef TSyncElemWrapper< TElem, SerializeBits<TElem> > Elem;
		};

		template<typename TElem>
		struct SmallFloat
		{
			typedef TSyncElemWrapper< TElem, SmallFloat16<TElem> > Elem;
		};

	};

	//Perhaps useful for data we only ever "sneak in"
	template<typename _DataType>
	NSETypeBase& OnSneakCore(NSEPresenter &presenter, const char *pElemName, const _DataType &rElem)
	{
		NSETypeRulesBase *pRule = rage_new typename OnSneakElemBase<_DataType>::ElemType();
		NSETypeBase *pElem = rage_new typename Helpers::Simple<_DataType>::Elem( *pRule );
		presenter.Present(pElemName, rElem, *pElem);
		return *pElem;
	}

	template<typename _DataType>
	NSETypeBase& OnSneakCore(NSEPresenter &presenter, const char *pElemName, const _DataType &rElem, int iNumBits)
	{
		if (!iNumBits)
		{
			return OnSneakCore(presenter, pElemName, rElem);
		}
		SerializeBits<_DataType> serializer(iNumBits);
		NSETypeRulesBase *pRule = rage_new typename OnSneakElemBase<_DataType>::ElemType();
		NSETypeBase *pElem = rage_new typename Helpers::SomeBits<_DataType>::Elem( *pRule, serializer );
		presenter.Present(pElemName, rElem, *pElem);
		return *pElem;
	}

	template<typename _DataType>
	NSETypeBase& HearbeatCore(NSEPresenter &presenter, const char *pElemName, const _DataType &rElem, float fTime)
	{
		NSETypeRulesBase *pRule = rage_new typename OnHeartbeatElemBase<_DataType>::ElemType(fTime);
		NSETypeBase *pElem = rage_new typename Helpers::Simple<_DataType>::Elem( *pRule );
		presenter.Present(pElemName, rElem, *pElem);
		return *pElem;
	}

	template<typename _DataType>
	NSETypeBase& HearbeatCore(NSEPresenter &presenter, const char *pElemName, const _DataType &rElem, float fTime, int iNumBits)
	{
		if (!iNumBits)
		{
			return HearbeatCore(presenter, pElemName, rElem, fTime);
		}
		SerializeBits<_DataType> serializer(iNumBits);
		NSETypeRulesBase *pRule = rage_new typename OnHeartbeatElemBase<_DataType>::ElemType(fTime);
		NSETypeBase *pElem = rage_new typename Helpers::SomeBits<_DataType>::Elem( *pRule, serializer );
		presenter.Present(pElemName, rElem, *pElem);
		return *pElem;
	}

	template<typename _DataType>
	NSETypeBase& HearbeatAndChangeCore(NSEPresenter &presenter, const char *pElemName, const _DataType &rElem, float fTime)
	{
		NSETypeRulesBase *pRule = rage_new typename OnHeartbeatAndChangeElemBase<_DataType>::ElemType(fTime);
		NSETypeBase *pElem = rage_new typename Helpers::Simple<_DataType>::Elem( *pRule );
		presenter.Present(pElemName, rElem, *pElem);
		return *pElem;
	}

	template<typename _DataType>
	NSETypeBase& HearbeatAndChangeCore(NSEPresenter &presenter, const char *pElemName, const _DataType &rElem, float fTime, int iNumBits)
	{
		if (!iNumBits)
		{
			return HearbeatAndChangeCore(presenter, pElemName, rElem, fTime);
		}
		SerializeBits<_DataType> serializer(iNumBits);
		NSETypeRulesBase *pRule = rage_new typename OnHeartbeatAndChangeElemBase<_DataType>::ElemType(fTime);
		NSETypeBase *pElem = rage_new typename Helpers::SomeBits<_DataType>::Elem( *pRule, serializer );
		presenter.Present(pElemName, rElem, *pElem);
		return *pElem;
	}

	template<typename _DataType>
	NSETypeBase& OnChangeCore(NSEPresenter &presenter, const char *pElemName, const _DataType &rElem)
	{
		NSETypeRulesBase *pRule = rage_new typename OnAnyChangeElemBase<_DataType>::ElemType;
		NSETypeBase *pElem = rage_new typename Helpers::Simple<_DataType>::Elem( *pRule );
		presenter.Present(pElemName, rElem, *pElem);
		pElem->SetMode(SendMode::eReliable);
		return *pElem;
	}

	template<typename _DataType>
	NSETypeBase& OnChangeCore(NSEPresenter &presenter, const char *pElemName, const _DataType &rElem, int iNumBits)
	{
		if (!iNumBits)
		{
			return OnChangeCore(presenter, pElemName, rElem);
		}
		SerializeBits<_DataType> serializer(iNumBits);
		NSETypeRulesBase *pRule = rage_new typename OnAnyChangeElemBase<_DataType>::ElemType;
		NSETypeBase *pElem = rage_new typename Helpers::SomeBits<_DataType>::Elem( *pRule, serializer );
		presenter.Present(pElemName, rElem, *pElem);
		pElem->SetMode(SendMode::eReliable);
		return *pElem;
	}

public:
	//SneakIn data gets sent whenever any other data in the same layer is sent
	//but does have any rules (will not get sent unless something else does)
	NSETypeBase& SneakIn(const char *pElemName, const float &rElem);
	NSETypeBase& SneakIn(const char *pElemName, const u32 &rElem, int iNumBits = 0);
	NSETypeBase& SneakIn(const char *pElemName, const s32 &rElem, int iNumBits = 0);
	NSETypeBase& SneakIn(const char *pElemName, const u16 &rElem, int iNumBits = 0);
	NSETypeBase& SneakIn(const char *pElemName, const s16 &rElem, int iNumBits = 0);
	NSETypeBase& SneakIn(const char *pElemName, const u8 &rElem, int iNumBits = 0);
	NSETypeBase& SneakIn(const char *pElemName, const s8 &rElem, int iNumBits = 0);
	NSETypeBase& SneakIn(const char *pElemName, const bool &rElem);

	//On change rules are distributed whenever the data changes
	NSETypeBase& OnChange(const char *pElemName, const float &rElem);
	NSETypeBase& OnChangeQuantized(const char *pElemName, const float &rElem, float fMin, float fMax, u8 iBits);
	NSETypeBase& OnChange(const char *pElemName, const u32 &rElem, int iNumBits = 0);
	NSETypeBase& OnChange(const char *pElemName, const s32 &rElem, int iNumBits = 0);
	NSETypeBase& OnChange(const char *pElemName, const u16 &rElem, int iNumBits = 0);
	NSETypeBase& OnChange(const char *pElemName, const s16 &rElem, int iNumBits = 0);
	NSETypeBase& OnChange(const char *pElemName, const u8 &rElem, int iNumBits = 0);
	NSETypeBase& OnChange(const char *pElemName, const s8 &rElem, int iNumBits = 0);
	NSETypeBase& OnChange(const char *pElemName, const bool &rElem);
	NSETypeBase& OnChange(const char *pElemName, const Vector3 &rElem);

	//Heartbeats mean the data gets sent every X seconds, regardless of change
	NSETypeBase& Heartbeat(const char *pElemName, const Vector3 &rElem, float fTime);
	NSETypeBase& Heartbeat(const char *pElemName, const float &rElem, float fTime);
	NSETypeBase& Heartbeat(const char *pElemName, const u32 &rElem, float fTime, int iNumBits = 0);
	NSETypeBase& Heartbeat(const char *pElemName, const s32 &rElem, float fTime, int iNumBits = 0);
	NSETypeBase& Heartbeat(const char *pElemName, const s16 &rElem, float fTime, int iNumBits = 0);
	NSETypeBase& Heartbeat(const char *pElemName, const u16 &rElem, float fTime, int iNumBits = 0);
	NSETypeBase& Heartbeat(const char *pElemName, const u8 &rElem, float fTime, int iNumBits = 0);
	NSETypeBase& Heartbeat(const char *pElemName, const s8 &rElem, float fTime, int iNumBits = 0);
	NSETypeBase& Heartbeat(const char *pElemName, const bool &rElem, float fTime);

	//Heartbeat rule + OnChange
	NSETypeBase& HeartbeatAndChange(const char *pElemName, const Vector3 &rElem, float fTime);
	NSETypeBase& HeartbeatAndChange(const char *pElemName, const float &rElem, float fTime);
	NSETypeBase& HeartbeatAndChange(const char *pElemName, const u32 &rElem, float fTime, int iNumBits = 0);
	NSETypeBase& HeartbeatAndChange(const char *pElemName, const s32 &rElem, float fTime, int iNumBits = 0);
	NSETypeBase& HeartbeatAndChange(const char *pElemName, const s16 &rElem, float fTime, int iNumBits = 0);
	NSETypeBase& HeartbeatAndChange(const char *pElemName, const u16 &rElem, float fTime, int iNumBits = 0);
	NSETypeBase& HeartbeatAndChange(const char *pElemName, const u8 &rElem, float fTime, int iNumBits = 0);
	NSETypeBase& HeartbeatAndChange(const char *pElemName, const s8 &rElem, float fTime, int iNumBits = 0);
	NSETypeBase& HeartbeatAndChange(const char *pElemName, const bool &rElem, float fTime);

	//Quantized versions
	NSETypeBase& HeartbeatAndChangeQuantized(const char *pElemName, const float &rElem, float fTime, float fMin, float fMax, u8 iBits);
	NSETypeBase& HeartbeatAndDeltaQuantized(const char *pElemName, const float &rElem, float fDelta, float fTime, float fMin, float fMax, u8 iBits);

	NSETypeBase& Quantized(const char *pElemName, const float &rElem, float fMin, float fMax, u8 iNumBits);

	//Send data if different to last sent by more that X delta
	NSETypeBase& Delta(const char *pElemName, const float &rElem, float fDelta);
	NSETypeBase& Delta(const char *pElemName, const Vector3 &rElem, float fDelta);

	//Delta + Quantization
	NSETypeBase& DeltaQuantized(const char *pElemName, const float &rElem, float fDelta, float fMin, float fMax, u8 iNumBits);

	//PURPOSE:
	//	Reduce amount of data sent with distance
	//COMMON PARAMS:
	//	pElemName - name for debuggin, widgets, display
	//	some value, different types
	//	fPriorityScale - Used as a delta (current value vs last sent value) scalar. Its used to allow balance between data with different value ranges and importances.
	//	fHalfLifeDist - Drop off with distance. A priority of 1.0f max will be 0.5f at this distance and 0.25f at double this distance.
	//	fAccumulation - Priority accumulated per frame with no regard to value delta. This pretty much governs a heartbeat, but does this balanced against other data's higher priority needs
	//	fMaxFrameDelta - Stop certain data types from flooding the system when they go through large deltas that arn't as important as they might seem
	//	fMin, fMax and iNumBits - Define the quantization range. You will find APIs for raw floats [DistancePriority] and 16bit floats [DistancePrioritySmallFloat] too
	NSETypeBase& DistancePriority(const char *pElemName, const float &rElem, float fPriorityScale, float fHalfLifeDist, float fAccumulation, float fFrameMaxDelta);
	NSETypeBase& DistancePriority(const char *pElemName, const Vector3 &rElem, float fPriorityScale, float fHalfLifeDist, float fAccumulation, float fFrameMaxDelta);
	NSETypeBase& DistancePrioritySmallFloat(const char *pElemName, const float &rElem, float fPriorityScale, float fHalfLifeDist, float fAccumulation, float fFrameMaxDelta);
	NSETypeBase& DistancePrioritySmallFloat(const char *pElemName, const Vector3 &rElem, float fPriorityScale, float fHalfLifeDist, float fAccumulation, float fFrameMaxDelta);
	NSETypeBase& DistancePriorityQuantized(const char *pElemName, const float &rElem, float fPriorityScale, float fHalfLifeDist, float fAccumulation, float fFrameMaxDelta, float fMin, float fMax, u8 iNumBits);
	NSETypeBase& DistancePriorityQuantized(const char *pElemName, const Vector2 &rElem, float fPriorityScale, float fHalfLifeDist, float fAccumulation, float fFrameMaxDelta, float fMin, float fMax, u8 iNumBits);
	NSETypeBase& DistancePriorityQuantized(const char *pElemName, const Vector3 &rElem, float fPriorityScale, float fHalfLifeDist, float fAccumulation, float fFrameMaxDelta, float fMin, float fMax, u8 iNumBits);

	NetPresenter()
	{
	}
};

} // namespace rage

#endif // DATASYNC_NETPRESENT_H 
