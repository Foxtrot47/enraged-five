// 
// DataSync/quantizers.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DATASYNC_QUANTIZERS_H 
#define DATASYNC_QUANTIZERS_H 

#include "vector/vector3.h"
#include "vector/quaternion.h"

namespace rage {

	namespace Quantizers
	{
		template<typename T>
		static T Quantize(float fValue, float fMin, float fMax, int iBits)
		{
			fValue = Clamp(fValue, fMin, fMax);
			fValue -= fMin;
			fValue *= 1.0f / (fMax - fMin);
			float fRange = (float)((1<<iBits)-1);
			fValue += 0.5f / fRange;	//Reduce rounding errors
			fValue *= fRange;
			return (T)fValue;
		}

		template<typename T>
		static float DeQuantize(T iValue, float fMin, float fMax, int iBits)
		{
			float fValue = iValue / (float)((1<<iBits)-1);
			fValue *= (fMax-fMin);
			fValue += fMin;
			return fValue;
		}

		void DeQuantizeNormalizedVector(Vector3 &v, u32 iVal);
		u32 QuantizeNormalizedVector(const Vector3 &_v);
		void DeQuantizeQuaternionS3(Quaternion &v, u32 iVal);
		u32 QuantizeQuaternionS3(const Quaternion &_v);

		float GetQuantizationAccuracy(float fMin, float fMax, int iNumBits);
	}
} // namespace rage

#endif // DATASYNC_QUANTIZERS_H 
