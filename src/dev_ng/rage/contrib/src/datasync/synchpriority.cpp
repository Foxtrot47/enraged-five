// 
// datasync/synchpriority.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synchpriority.h"
#include "bank/bank.h"
#include "system/param.h"

namespace rage {

NSEPriorityManager *NSEPriorityManager::smp_Instance = 0;

#if __BANK
void NSEPriorityManager::AddWidgets(bkBank &bk)
{
	bk.PushGroup("NSEPriorityManager", false);
	bk.AddSlider("Max bits out per frame", &m_MaxBitsPerFrame, 0, 10000, 1);
	bk.AddSlider("Max bits out per frame per client", &m_MaxBitsPerFramePerClient, 0, 10000, 1);
	bk.AddSlider("Min priority for send", &m_fMinPriorityForSend, 0.0f, 10.0f, 0.01f);
	m_BytesQueued = 0;
	bk.AddText("Bytes queued", &m_BytesQueued);
	bk.PushGroup("Bandwidth allowances", false);
	for (int i=0 ; i<kMaxPeers ; i++)
	{
		char buffer[64];
		formatf(buffer, sizeof(buffer), "Peer: %d", i);
		bk.PushGroup(buffer, false);
			bk.AddText("Index", &m_PeerList[i].m_iPeerIndex);
			bk.AddText("MaxBitsPerFrame", &m_PeerList[i].m_iAllowedBitsPerFrame);
		bk.PopGroup();
	}
	bk.PopGroup();
	bk.PopGroup();
}
#endif

NSEPrioritized::NSEPrioritized(NSEContext &rCtxt)
	:NSEInstanceBase(rCtxt)
	,m_fPriority(0.0f)
	,m_iQueueIndex(-1)
	,m_iClientIndex(-1)
{
}

NSEPrioritized::~NSEPrioritized()
{
}

int NSEPriorityManager::FindClientIndex(u64 iPeerId) const
{
	for (int i=0 ; i<kMaxPeers ; i++)
	{	
		if (m_PeerList[i].m_PeerId==iPeerId)
			return i;
	}
	return -1;
}

void NSEPriorityManager::SetClientAllowance(u64 iPeerId, u32 iAllowedBitsPerFrame)
{
	int iPrior = FindClientIndex(iPeerId);
	if(iPrior < 0)
	{
		Assertf(iPrior>=0, "NSEPriorityManager::OnPeerLeft called for we didn't know about (peer = 0x%x)", iPeerId);
		return;
	}

	Assert(m_PeerList[iPrior].m_PeerId == iPeerId);
	m_PeerList[iPrior].m_iAllowedBitsPerFrame = iAllowedBitsPerFrame;
}

void NSEPriorityManager::OnPeerJoined(u64 iPeerId, u32 iPeerIndex)
{
	int iPrior = FindClientIndex(iPeerId);
	Assertf(iPrior<0, "NSEPriorityManager::OnPeerJoined called for peer we already had (index %d, peer = 0x%x)", iPrior, iPeerId);
	if (iPrior >= 0)
	{
		return;
	}
	
	for (int i=0 ; i<kMaxPeers ; i++)
	{
		if (!m_PeerList[i].m_PeerId)
		{
			m_PeerList[i].m_PeerId = iPeerId;
			m_PeerList[i].m_iPeerIndex = iPeerIndex;
			m_PeerList[i].m_iAllowedBitsPerFrame = m_MaxBitsPerFramePerClient;
			return;
		}
	}

	AssertMsg(0, "Failed to find open peer slot in NSEPriorityManager::OnPeerJoined");
}

void NSEPriorityManager::OnPeerLeft(u64 iPeerId)
{
	int iPrior = FindClientIndex(iPeerId);
	if(iPrior < 0)
	{
		Assertf(iPrior>=0, "NSEPriorityManager::OnPeerLeft called for we didn't know about (peer = 0x%x)", iPeerId);
		return;
	}

	m_PeerList[iPrior].m_PeerId = 0;
}

bool NSEPriorityManager::AddNewElement(NSEPrioritized &rElem, NSEContext &rCtxt)
{
	if (!rCtxt.GetPeerId())
	{
		//If no peer index then this data is currently not needed to send data.
		rElem.m_iClientIndex = -1;	//Indicates its not in the system
		VALIDATE_ONLY(ValidateQueue());
		return false;
	}

	//Find the client in our list - this will give the client index
	//for now but eventually we need a more defined lower level concept of peer index.
	Assign(rElem.m_iClientIndex, FindClientIndex( rCtxt.GetPeerId() ));
	Assert(rElem.m_iClientIndex >= 0);
	if (rElem.m_iClientIndex>=0)
	{
		Assert(m_Queue.GetCount()<m_Queue.GetCapacity());
		if (m_Queue.GetCount()<m_Queue.GetCapacity())
		{
			m_Queue.Push(&rElem);
			Assign(rElem.m_iQueueIndex, m_Queue.GetCount()-1);
			VALIDATE_ONLY(ValidateQueue());
			return true;
		}
	}
	VALIDATE_ONLY(ValidateQueue());
	return false;
}

#if VALIDATE_QUEUE
void NSEPriorityManager::ValidateQueue(bool bSpacesOkay)
{
	const int kCount = m_Queue.GetCount();
	for (int i=0; i<kCount ; i++)
	{
		Assert(m_Queue[ i ] || bSpacesOkay);
		if (m_Queue[ i ])
		{
			Assert(m_Queue[ i ]->m_iClientIndex >= 0);
			Assert(m_Queue[ i ]->m_iQueueIndex == i);
		}
	}
}
#endif

bool NSEPriorityManager::RemoveElement(NSEPrioritized &rElem)
{
	if (rElem.m_iClientIndex < 0)
	{
		Assert(rElem.m_iQueueIndex < 0);
		VALIDATE_ONLY(ValidateQueue());
		return false;
	}

	Assert(rElem.m_iQueueIndex > -1);
	if (rElem.m_iQueueIndex > -1)
	{
		//TMS: Safety check can probably be removed in the long term
		Assertf(m_Queue[rElem.m_iQueueIndex] == &rElem, "m_Queue[%d] != 0x%x", rElem.m_iQueueIndex, &rElem);
		if (m_Queue[rElem.m_iQueueIndex] == &rElem)
		{
			m_Queue[rElem.m_iQueueIndex] = 0;
			//m_Queue.DeleteFast(rElem.m_iQueueIndex);
			//All trailing queue members need adjusting now....
			VALIDATE_ONLY(ValidateQueue());
			return true;
		}
		else
		{
			//Fallback route to try and release the element anyway
			const int kCount = m_Queue.GetCount();
			for (int i=0; i<kCount ; i++)
			{
				if (m_Queue[ i ] == &rElem)
				{
					Assertf(0, "Found element 0x%x out of place at %d, was expecting at %d", &rElem, i, rElem.m_iQueueIndex);
					m_Queue[ i ] = 0;
					VALIDATE_ONLY(ValidateQueue());
					return true;
				}
			}
		}
	}

	//Will need to reset this if adding again
	rElem.m_iClientIndex = -1;
	VALIDATE_ONLY(ValidateQueue());
	return false;
}

void NSEPriorityManager::Update()
{
	VALIDATE_ONLY(ValidateQueue());
	
	//Remove the blanked out spaces as we go to keep the queue nice and compact.
	//NOTE: Until we reset the indices later in this function all indices are probably bolloxs
	for (int i=0 ; i<m_Queue.GetCount() ; i++)
	{
		//Delete empty elements!
		while ((i<m_Queue.GetCount()) && (!m_Queue[i]))
		{
			m_Queue.DeleteFast(i);
		}
	}

	const int kCount = m_Queue.GetCount();

	//Sort list by priority (will make this more efficient later - we don't need hard organization really
	qsort(m_Queue.GetElements(), kCount, sizeof(m_Queue[0]), NSEPrioritized::CompareByPtr);

	//Reset the indices 
	for (int i=0 ; i<kCount ; i++)
	{
		Assign(m_Queue[i]->m_iQueueIndex, i);
	}

	VALIDATE_ONLY(ValidateQueue(false));

	//Distribute data
	int iClientCaps[kMaxClients];
	int iClientTargeted[kMaxClients];

	//Setup defaults to fall back to
	for (int i=0 ; i<kMaxClients ; i++)
	{
		iClientCaps[i] = m_MaxBitsPerFramePerClient;	//TODO: Calculate how much data a client should get from me - 1000 * 30 == 30kbps for auto sync data
		iClientTargeted[i] = 0;
	}

	//Set our actual bandwidths (provided by remote client)
	for (int i=0 ; i<kMaxPeers ; i++)
	{
		if (m_PeerList[i].m_PeerId)
		{
			iClientCaps[m_PeerList[i].m_iPeerIndex] = m_PeerList[i].m_iAllowedBitsPerFrame;
		}
	}

	int iElement=0;
	int iSentData = 0;			//Amount of data sent
	int iMaxOut = m_MaxBitsPerFrame;			//Cap of data sent per frame - 1000 * 30 == 30kbps for auto sync data
	while ((iElement < kCount) && (iSentData < iMaxOut))
	{
		//Send element to client.
		if (m_Queue[iElement]->m_fPriority<=m_fMinPriorityForSend)
			break;

		int iClient = m_Queue[iElement]->GetClient();
		if (iClientTargeted[iClient] < iClientCaps[iClient])
		{
			m_Queue[iElement]->SetDirty( true );
			m_Queue[iElement]->m_fPriority = 0.0f;	//No priority now - its going to be sent and this makes it easy to accumulate priority

			//Count our data to be sent (perhaps just serialize here)
			//This is a count in bits - does not take into account headers that will be added - this is *assumed* to be 
			int iDataAmount = m_Queue[iElement]->GetPackedDataSize();
			iClientTargeted[ iClient ] += iDataAmount;
			iSentData += iDataAmount;
		}

		++iElement;
	}

	BANK_ONLY(m_BytesQueued = iSentData>>3;)

	//Mark data that nearly got in... (80% more allowed to reduced header overhead)
	iMaxOut*=5;
	iMaxOut>>=2;
	for (int i=0 ; i<kMaxClients ; i++)
	{
		iClientCaps[i] *= 5;
		iClientCaps[i] >>= 2;
	}
	while ((iElement < kCount) && (iSentData < iMaxOut))
	{
		//Send element to client.
		if (m_Queue[iElement]->m_fPriority<=m_fMinPriorityForSend)
			break;

		int iClient = m_Queue[iElement]->GetClient();
		if (iClientTargeted[iClient] < iClientCaps[iClient])
		{
			m_Queue[iElement]->SetDirty( true );
			//Count our data to be sent (perhaps just serialize here)
			//This is a count in bits - does not take into account headers that will be added - this is *assumed* to be 
			int iDataAmount = m_Queue[iElement]->GetPackedDataSize();
			iClientTargeted[ iClient ] += iDataAmount;
			iSentData += iDataAmount;
		}

		++iElement;
	}

	VALIDATE_ONLY(ValidateQueue(false));
}
} // namespace rage

