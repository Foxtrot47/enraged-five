// 
// datasync/synchelements.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synchelements.h"
#include "bank/bank.h"
#include "synchobject.h"

namespace rage {

#if !__FINAL
#if __BANK
void NSETypeBase::AddWidgets(bkBank &bk)
{
	bk.AddText("Outbound count", &m_Outbound);
	if (GetPackedDataSize() > 1)
	{
		bk.AddText("Outbound bits", &m_OutboundBits);
	}
	bk.AddText("Incoming count", &m_Incoming);
	if (GetPackedDataSize() > 1)
	{
		bk.AddText("Incoming bits", &m_IncomingBits);
	}
	OnAddWidgets(bk);
}
#endif
#endif


void NSETypeBase::ExportElement(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer, bool bSendSneakyData)
{
	if (SneakItIn())
	{
		//Sneaky data doesn't have a flag around it. Also - if its marked dirty then
		//bSendSneakyData MUST be set (assertion in other branch to check this)
		if (bSendSneakyData)
		{
			Export(rData, rBuffer, bSendSneakyData, false);
		}
	}
	else
	{
		Assert((!rData.IsDirty()) || (bSendSneakyData));

		//Each sub element has a guard flag around it.
		NSEBufferSerializer::GuardedSection objectGuard(rBuffer);

		objectGuard.Place();
		Export(rData, rBuffer, bSendSneakyData, false);
		objectGuard.Finalize();
	}
}

void NSETypeBase::ImportElement(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer)
{
	if (SneakItIn())
	{
		//Sneaky data doesn't have a flag around it - we assume that if we've got 
		//here then we're going to have the data.
		Import(rData, rBuffer, false);
	}
	else
	{
		//Each sub element has a guard flag around it.
		NSEBufferSerializer::GuardedSection objectGuard(rBuffer);
		if (objectGuard.StartImport())
		{
			Import(rData, rBuffer, false);
			objectGuard.EndImport();
		}
	}
}

void NSEObjectRoot::OnExport(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer)
{
	InstanceData &rInstData = rData.UpCast<InstanceData>();
	if (mp_ChildData && rInstData.mp_ChildData)
	{
		NSE_TRACE_OUT(GetName());
		rBuffer.PushRoot(rInstData.mp_ObjectSectionRoot);

		mp_ChildData->ExportElement(*rInstData.mp_ChildData, rBuffer, rInstData.mp_ChildData->IsDirty());

		rBuffer.PopRoot();
	}
}

void NSEObjectRoot::OnImport	(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer)
{
	InstanceData &rInstData = rData.UpCast<InstanceData>();
	if (mp_ChildData && rInstData.mp_ChildData)
	{
		NSE_TRACE_IN(GetName());
		//Object roots need to be push and popped to get correct pointer offsets
		//TMS: Not sure we should put this in the serializer - perhaps a context object is needed here too?
		rBuffer.PushRoot(rInstData.mp_ObjectSectionRoot);

		mp_ChildData->ImportElement(*rInstData.mp_ChildData, rBuffer);

		rBuffer.PopRoot();
	}
}

void NSEObjectRoot::OnVisit	(NSEVisitor &rVisitor, NSEInstanceBase *pData, NSEContext *pContext, void * /*pCurrentDataRoot*/)
{
	rVisitor.OnStartObject(*this, pData);
	if (mp_ChildData)
	{
		if (!pData)
		{
			mp_ChildData->Visit(rVisitor);
		}
		else
		{
			InstanceData &rInstData = pData->UpCast<InstanceData>();
			if (rInstData.mp_ChildData)
			{
				mp_ChildData->Visit(rVisitor, rInstData.mp_ChildData, pContext, rInstData.mp_ObjectSectionRoot);
			}
		}
	}
	rVisitor.OnEndObject(*this, pData);
}

void NSEObjectRoot::OnUpdate	(NSEInstanceBase &rData, NSEContext &rContext, void * /*pCurrentDataRoot*/)
{
	InstanceData &rInstData = rData.UpCast<InstanceData>();
	if (mp_ChildData && rInstData.mp_ChildData)
	{
		//Our child data needs to be updated with us as a root.
		mp_ChildData->Update(*rInstData.mp_ChildData, rContext, rInstData.mp_ObjectSectionRoot);
	}
}

NSEObjectRoot::~NSEObjectRoot()
{
	delete mp_ChildData;
}

bool NSEGroup::CheckGuardFlag(void *pCurrentDataRoot)
{
	//No guard flag == read by default
	if (!mp_GroupGuard)
		return true;

	return *(bool*)((char *)pCurrentDataRoot + mp_GroupGuard->GetDataOffset());
}

void NSEGroup::OnUpdate(NSEInstanceBase &rData, NSEContext &rContext, void *pCurrentDataRoot)
{
	//Need to check the guard flag state.
	InstanceData &rInstData = rData.UpCast<InstanceData>();
	const int kNumElem = rInstData.m_ClientElements.GetCount();

	//If we don't have any elements then we don't do anything at all
	if (kNumElem)
	{
		if (mp_GroupGuard)
		{
			Assert(rInstData.mp_GroupGuard);
			mp_GroupGuard->Update(*rInstData.mp_GroupGuard, rContext, pCurrentDataRoot);
		}

		if (CheckGuardFlag( pCurrentDataRoot ))
		{
			//Loop through and update all children
			for (int i=0 ; i<kNumElem ; i++)
			{
				m_Elements[i]->Update(*rInstData.m_ClientElements[i], rContext, pCurrentDataRoot);
			}
		}
	}
}


void NSEGroup::OnVisit	(NSEVisitor &rVisitor, NSEInstanceBase *pData, NSEContext *pContext, void *pCurrentDataRoot)
{
	if (rVisitor.OnStartGroup(*this, pData))
	{
		if (pData)
		{
			InstanceData &rInstData = pData->UpCast<InstanceData>();
			const int kNumElem = rInstData.m_ClientElements.GetCount();
			if (kNumElem)
			{
				if (mp_GroupGuard)
				{
					Assert(rInstData.mp_GroupGuard);
					mp_GroupGuard->Visit(rVisitor, rInstData.mp_GroupGuard, pContext, pCurrentDataRoot);
				}

				//Loop through and update all children
				for (int i=0 ; i<kNumElem ; i++)
				{
					m_Elements[i]->Visit(rVisitor, rInstData.m_ClientElements[i], pContext, pCurrentDataRoot);
				}
			}
		}
		else
		{
			if (mp_GroupGuard)
			{
				mp_GroupGuard->Visit(rVisitor);
			}

			const int kNumElem = m_Elements.GetCount();
			//Loop through and update all children
			for (int i=0 ; i<kNumElem ; i++)
			{
				m_Elements[i]->Visit(rVisitor);
			}
		}
		rVisitor.OnEndGroup(*this, pData);
	}
}

void NSEGroup::OnExport(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer)
{
	//Early out if flags for group (include child groups too) don't match the buffer
	//then we won't need to read anything, including the mode flags
	if (!(rBuffer.GetMode() & GetMode()))
	{
		return;
	}

	InstanceData &rInstData = rData.UpCast<InstanceData>();
	const int kNumElem = rInstData.m_ClientElements.GetCount();
	if (!kNumElem)
	{
		//TMS: Early out if empty layer (used to trim data based on actual object type)
		return;
	}

	//Each sub element has a guard flag around it. At some point I want
	//to specialize booleans to use the guard flag as a value but this
	//might be tricky (wrt empty sections)
	NSEBufferSerializer::GuardedSection elemGuard(rBuffer);
	bool bGuardFlagSet = CheckGuardFlag(rBuffer.GetCurrentObjectRoot());
	bool bHasDirtyData=mp_GroupGuard ? rInstData.mp_GroupGuard->IsDirty() : false;
	if (bGuardFlagSet && !bHasDirtyData)
	{
		for (int i=0 ; i<kNumElem ; i++)
		{
			//Only consider elements that match mode.
			if (m_Elements[i]->GetMode() & rBuffer.GetMode())
			{
				if (rInstData.m_ClientElements[i]->IsDirty())
				{
					bHasDirtyData = true;
					break;
				}
			}
		}
	}

	//Check our own guard flag
	if (mp_GroupGuard)
	{
		//TMS:	Note too happy about the double flag thing going on here. The trouble is that if the flag changes
		//		we need to send that change.
		Assert(rInstData.mp_GroupGuard);
		elemGuard.Place();
		mp_GroupGuard->Export(*rInstData.mp_GroupGuard, rBuffer, bHasDirtyData, true);
		elemGuard.Finalize();
	}

	//Guard the whole group with a flag. This helper class will rewind
	//the buffer if all we write are guard flags.
	NSEBufferSerializer::GuardedSection groupGuard(rBuffer);
	groupGuard.Place();

	if (bGuardFlagSet && bHasDirtyData)
	{		
		//Loop through and export all children, writing a guard bit as needed
		for (int i=0 ; i<kNumElem ; i++)
		{
			m_Elements[i]->ExportElement(*rInstData.m_ClientElements[i], rBuffer, bHasDirtyData);
		}
	}

	//Finalize the guard flag (will modify original flag written)
	if (groupGuard.Finalize())
	{
		NSE_TRACE_OUT("---> Section Exported <---");
	}
	else
	{	
		//TMS: Whats wrong with this?
		//Assert((!(bHasDirtyData || bGuardFlagSet)) && "Group exported without expected child data export");
	}
}

void NSEGroup::OnImport(NSEInstanceBase &rData, NSEBufferSerializer &rBuffer)
{
	//Early out if flags for group (include child groups too) don't match the buffer
	//then we won't need to read anything, including the mode flags
	if (!(rBuffer.GetMode() & GetMode()))
	{
		return;
	}

	InstanceData &rInstData = rData.UpCast<InstanceData>();
	const int kNumElem = rInstData.m_ClientElements.GetCount();
	if (!kNumElem)
	{
		//TMS: Early out if empty layer (used to trim data based on actual object type)
		return;
	}

	//Each sub element has a guard flag around it.
	NSEBufferSerializer::GuardedSection elemGuard(rBuffer);

	if (mp_GroupGuard && elemGuard.StartImport())
	{
		Assert(rInstData.mp_GroupGuard);
		mp_GroupGuard->Import(*rInstData.mp_GroupGuard, rBuffer, true);
		elemGuard.EndImport();
	}

	//Grab and check the guard bit
	NSEBufferSerializer::GuardedSection groupGuard(rBuffer);
	if (groupGuard.StartImport())
	{
		NSE_TRACE_IN(GetName());

		//TMS:	Cannot check currently - we would have to force it to be
		//		sent with every section update AND when changing
		//if (CheckGuardFlag(rBuffer.GetCurrentObjectRoot())
		{
			//Loop through and check each child's guard bit. If set then write 
			for (int i=0 ; i<kNumElem ; i++)
			{
				m_Elements[i]->ImportElement(*rInstData.m_ClientElements[i], rBuffer);
			}
		}
		groupGuard.EndImport();
	}
}

NSEInstanceBase* NSEGroup::CreateClientData(NSEContext &rContext) const
{
	const int kNumElem = m_Elements.GetCount();
	Assert(kNumElem);
	if (!kNumElem)
	{
		return 0;
	}

	InstanceData *pInstData = rage_new InstanceData(rContext);
	if (pInstData)
	{
		if (	!RequiresCreationCallback()
			||	NSOCBs::CreateSectionCB(*rContext.GetObject(), *this, *pInstData)
			)
		{
			pInstData->m_ClientElements.Resize(kNumElem);
			for (int i=0 ; i<kNumElem ; i++)
			{
				pInstData->m_ClientElements[i] = m_Elements[i]->CreateClientData(rContext);
			}

			if (mp_GroupGuard && kNumElem)
			{
				pInstData->mp_GroupGuard = mp_GroupGuard->CreateClientData(rContext);
			}
		}
	}
	return pInstData;
}

NSEGroup::~NSEGroup()
{
	for (int i=0 ; i<m_Elements.GetCount() ; i++)
	{
		delete m_Elements[i];
	}
	delete mp_GroupGuard;
}

	namespace NSOCBs{
		bool ImportCB(NSOObjectBase &rObj, const NSETypeRulesBase& rRule, const NSEInstanceBase& rInstData, u16 iDataID, NSEBufferSerializer& rBuffer)
		{
			return rObj.OnSyncDataImport(rRule, rInstData, iDataID, rBuffer);
		}

		bool ExportCB(NSOObjectBase &rObj, const NSETypeRulesBase& rRule, const NSEInstanceBase& rInstData, u16 iDataID, NSEBufferSerializer& rBuffer)
		{
			return rObj.OnSyncDataExport(rRule, rInstData, iDataID, rBuffer);
		}

		bool CreateCB(NSOObjectBase &rObj, const NSETypeRulesBase& rRule, const NSEInstanceBase& rInstData, u16 iDataID)
		{
			return rObj.OnNewData(rRule, rInstData, iDataID);
		}

		bool CreateSectionCB(NSOObjectBase &rObj, const NSETypeBase& rSection, const NSEInstanceBase& rInstData)
		{
			return rObj.OnNewGroup(rSection, rInstData);
		}
	}
} // namespace rage

