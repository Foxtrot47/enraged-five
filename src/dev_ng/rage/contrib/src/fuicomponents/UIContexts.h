// 
// UIComponents/UIContexts.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#ifndef UICONTEXTS_H
#define UICONTEXTS_H

#include "flash/misc.h"
#include "fuicomponents/UIObject.h"
#include "fuicomponents/Components.h"

//TEMPLATES indices
enum eMenuTemplate
{
	TEMPLATE_SIMPLE					= 1,
	TEMPLATE_DEFAULT				= 2,
	TEMPLATE_MENUWITHDESCRIPTION	= 3,
	TEMPLATE_PLAYERLIST				= 4,
	TEMPLATE_LOBBY_PLAYERS			= 5,
	TEMPLATE_PAUSEMENU				= 1,
	TEMPLATE_DIALOGBOX				= TEMPLATE_DEFAULT,
	TEMPLATE_POPUP					= 2
};

namespace rage{
class UILayer;
class UIScene;
class UIList;
class UIScrollBar;
class UIProgressBar;
class UISpinner;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIContext
//An interface to an external tool that artists use to represent/decorate these UI components.
//Each component should have a corresponding UIContext.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIContext : public datBase
{
public:
	UIContext(){}
	virtual ~UIContext(){}

	virtual void SetVisible(bool b) = 0;
	virtual void SetEnabled(bool b) = 0;
	virtual void SetFocused(bool b) = 0;
	virtual void SetActive(bool b) = 0;	
	virtual void OnShow() = 0;
	virtual void OnHide() = 0;
	virtual void SetTemplate(int) = 0;
	virtual void SetColor(u32 color) = 0;
	virtual void Reset() = 0;
	virtual void ProcessDataTag(const char* name,const char* value) = 0;
	virtual UIObject* GetParent(){return m_pOwner->GetParent();}	
	virtual void SetOwner(UIObject* pObject){m_pOwner = pObject;}
	virtual void PostStream(char){}
	virtual void PostLoad(){}
	virtual void OnActivate(){}
	virtual void OnDeactivate(){}
	virtual void Refresh(int){}
	virtual void ResetFlashObjectToDefaults(){}


protected:
	UIObject* m_pOwner;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UILayerContext
//A context interface to be used with UILayer 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UILayerContext : public virtual UIContext
{
public:
	UILayerContext() : UIContext(){}
	virtual ~UILayerContext(){}

	virtual void PostLoad() = 0;
	virtual void PostStream(char) = 0;
	virtual void Reset() = 0;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIPanelContext
//A context interface to be used with UIPanel
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIPanelContext : public virtual UIContext
{
public:
	UIPanelContext() : UIContext(){}
	virtual ~UIPanelContext(){}

	virtual void Reset() = 0;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UISceneContext
//A context interface to be used with UIScene 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UISceneContext : public virtual UIContext
{
public:
	UISceneContext() : UIContext(){}
	virtual ~UISceneContext(){}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIButtonContext
//A context interface to be used with UIButton 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIButtonContext : public virtual UIContext
{
public:
	UIButtonContext() : UIContext(){}
	virtual ~UIButtonContext(){}

	virtual void SetBlinking(bool b)=0;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UILabelContext
//A context interface to be used with UITabbedContainer 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UILabelContext : public virtual UIContext
{
public:
	UILabelContext() : UIContext(){}
	virtual ~UILabelContext(){}

	// PURPOSE: Defines the single line of text this component will display.
	virtual void SetText(const char* stringID) = 0;
	virtual void SetText(u32 stringIDHash) = 0;
	virtual void PostLoad() = 0;
	virtual void PostStream(char) = 0;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIIconContext
//A context interface to be used with UIIcon 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIIconContext : public virtual UIContext
{
public:
	UIIconContext() : UIContext(){}
	virtual ~UIIconContext(){}

	// PURPOSE: Defines the single line of text this component will display.
	virtual void SetIcon(UIIcon::IconType) = 0;
	virtual void SetIcon(UIPromptStrip::PromptIcons) = 0;
	virtual void PostLoad() = 0;
	virtual void PostStream(char) = 0;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIListContext
//A context interface to be used with UIList
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIListContext : public virtual UIContext
{
public:
	UIListContext() : UIContext() {}
	virtual ~UIListContext(){}

	//virtual void SetNumListItems(u32) = 0;
	virtual void SetNumVisibleListItems(u32) = 0;
	virtual void SetCurrentSelection(u32) = 0;
	virtual void PostLoad() = 0;
	virtual void PostStream(char) = 0;
	virtual void OnScroll() = 0;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIScrollBarContext
//A context interface to be used with UIScrollBar 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIScrollBarContext : public virtual UIContext
{
public:
	UIScrollBarContext() : UIContext(){}
	virtual ~UIScrollBarContext(){}

	// PURPOSE: Sets the progress bar's current value to n.
	virtual void SetValue(f32 n) = 0;

	virtual void PostLoad() = 0;
	virtual void PostStream(char) = 0;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UITabbedContainerContext
//A context interface to be used with UITabbedContainer 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UITabbedContainerContext : public virtual UIContext
{
public:
    UITabbedContainerContext() : UIContext(){}
    virtual ~UITabbedContainerContext(){}

	// PURPOSE: Sets the selected index for this UITabbedContainer.
	virtual void SetCurrentTab(u32) = 0;
	virtual void SetNumVisibleTabs(u32 n) = 0;
	virtual void PostLoad() = 0;
	virtual void PostStream(char) = 0;
	virtual void OnTabChanged() = 0;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UITabContext
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UITabContext : public virtual UIContext
{
public:
	UITabContext() : UIContext(){}
	virtual ~UITabContext(){}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UITickerContext
//A context interface to be used with UITicker
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UITickerContext : public virtual UIContext
{
public:
	UITickerContext() : UIContext(){}
	virtual ~UITickerContext(){}

	// PURPOSE: Sets the max number of visible items in the ticker
	virtual void SetMaxItems(f32 n) = 0;

	// PURPOSE: Sets the value of an item in the ticker
	virtual void SetItemValue(int index, const char *szValue) = 0;

	// PURPOSE: Sets the visible y position of an item in the list
	virtual void SetItemPosition(int index, int y) = 0;

	// PURPOSE: Sets the alpha on an item in the list
	virtual void SetItemAlpha(int index, int alpha) = 0;

	virtual void PostLoad() = 0;
	virtual void PostStream(char) = 0;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIProgressBarContext
//A context interface to be used with UIProgressBar 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIProgressBarContext : public virtual UIContext
{
public:
	UIProgressBarContext() : UIContext(){}
	virtual ~UIProgressBarContext(){}

	// PURPOSE: Sets the indeterminate property of the progress bar, which determines 
	//			whether the progress bar is in determinate or indeterminate mode.	
	virtual void SetIndeterminate(bool newValue) = 0;

	// PURPOSE: Sets the progress bar's maximum value to n.
	virtual void SetMaximum(f32 n) = 0;
		
	// PURPOSE: Sets the progress bar's minimum value to n.
	virtual void SetMinimum(f32 n) = 0;
	
	// PURPOSE: Sets the value of the progress string.
	virtual void SetString(const char* s) = 0;

	// PURPOSE: Sets the progress bar's current value to n.
	virtual void SetValue(f32 n) = 0;

	virtual void PostLoad() = 0;
	virtual void PostStream(char) = 0;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UISpinnerContext
//A context interface to be used with UISpinner 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UISpinnerContext : public virtual UIContext
{
public:
	UISpinnerContext() : UIContext(){}
	virtual ~UISpinnerContext(){}

	virtual void SetMinimum(f32 value) = 0;
	virtual void SetMaximum(f32 value) = 0;
	virtual void SetValue(f32 value) = 0;
	virtual void SetIncrement(f32 increment ) = 0;
	virtual void PostLoad() = 0;
	virtual void PostStream(char) = 0;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIPromptStripContext
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIPromptStripContext : public virtual UILayerContext
{
public:
	UIPromptStripContext() : UIContext(){}
	virtual ~UIPromptStripContext(){}

	virtual void PostLoad() = 0;
	virtual void PostStream(char) = 0;
	virtual void SetNumPrompts(int) = 0;
	virtual void SetStyle(int) = 0;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIMessageBoxContext
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIMessageBoxContext : public virtual UIListContext
{
public:
	UIMessageBoxContext() : UIContext(){}
	virtual ~UIMessageBoxContext(){}

	virtual void SetTitle(const char*) = 0;
	virtual void SetDescription( const char*) = 0;	
	virtual void PostLoad() = 0;
	virtual void PostStream(char) = 0;
	virtual void SetStyle(int) = 0;
};

}//end namespace rage
#endif // UICOMPONENTS_COMPONENTS_H
