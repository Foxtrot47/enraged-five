// 
// flashUIComponents/PauseMenu.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "system/param.h"

#include "vector/matrix34.h"
#include "flashuicomponents/Transitions.h"
#include "flashuicore/UIManager.h"
#include "flashuicore/UIStringTable.h"
#include "flashuicomponents/Components.h"
#include "snu/snugamer.h"

////////////////////////////////////
//includes needed for state instantiation
#include "flashuicore/VirtualHSM.h"
// #include "mcNet/NetLobby.h"
////////////////////////////////////

// #include "mcNet/NetManager.h"
// #include "mcNet/NetScript.h"
// #include "mcnet/NetStatScribe.h"

//transition includes
//#include "phcore/phmath.h"

using namespace rage;
#define MYGAMER (*(SNUSOCKET.GetLocalGamer(0)))

PARAM(devmenus,"Show debug menus."); //defined in mcData/ConfigGame.cpp
PARAM(nodevmenus,"Don't show development-related and debug menus.");
//XPARAM(demo);
//XPARAM(racename);
PARAM(redeliver,"Allow restart at end of delivery.");

const char * szTabNames[] = {"pause_tab_mainmenu","pause_tab_gamemodes","pause_tab_settings"};

#define MAIN_FORM (static_cast<fuiMainForm&>(*fuiLogic::GetInstance()->GetMainForm()))

fuiPromptItem pausePrompt[] = {
	fuiPromptItem(EPromptA, "prompt_accept"),
	fuiPromptItem(EPromptB, "prompt_back"),
	fuiPromptItem(EPromptBACK, "Navmap_Prompt_GPSMap")
};

namespace{
	bool IsDamagedOut() { return false; }
}

bool PlaySoundIfEnabled(UIObject *pState)
{
	if(!pState)
	{
		return false;
	}

	if( pState->IsEnabled() &&
		!pState->IsBlocked()
		)
	{
		fuiCallbacks::sm_PlayFlashSoundFunc(k_szMenuSelectSound); 
		return true;
	}
	else
	{
		return false;
	}
}

bool IsSelected(const char *rowName, bool playAudio = true)
{
	FlashNavigator* pNav	= UIMANAGER->GetUINavigator();
	Assert(pNav);
	UIObject *pState = pNav->GetStateByIdName(rowName);

	fuiPauseForm& rPauseForm = (fuiPauseForm&)MAIN_FORM.GetForm(ePauseForm);
	if(pState && pState==rPauseForm.m_pausePanel.GetSelectedRow())
	{
		if(!pState->IsBlocked())
		{
			if(playAudio)
			{
				fuiCallbacks::sm_PlayFlashSoundFunc(k_szMenuSelectSound); 
			}
			return true;
		}
		else
		{
			fuiCallbacks::sm_PlayFlashSoundFunc(k_szMenuErrorSound); 
		}
	}
	
	return false;
}

bool IsDelivery()
{
	bool bIsDelivery = false;
/*
	if(mcRace::GetCurrentRace())
	{
		Assert(mcRace::GetCurrentRace()->GetRaceDataPtr());
		if(mcRace::GetCurrentRace()->GetRaceDataPtr()->m_nUIType == mc::EUIRaceType_DELIVERY)
		{
			bIsDelivery = true;
		}
	}

*/
	return bIsDelivery;
}

bool IsPayback()
{
	bool bIsDelivery = false;
/*
	if(mcRace::GetCurrentRace())
	{
		Assert(mcRace::GetCurrentRace()->GetRaceDataPtr());
		if(mcRace::GetCurrentRace()->GetRaceDataPtr()->m_nUIType == mc::EUIRaceType_PAYBACK)
		{
			bIsDelivery = true;
		}
	}

*/
	return bIsDelivery;
}

bool IsArcade()
{
// 	if(mcRace::GetCurrentRace())
// 	{
// 		Assert(mcRace::GetCurrentRace()->GetRaceDataPtr());
// 		if(mcRace::GetCurrentRace()->GetRaceDataPtr()->m_bIsArcadeRace)
// 		{
// 			return true;
// 		}
// 	}

	return true;
}

bool fuiBaseMenu::IsRaceOver() const
{
	// oh.. it's so very over...
	return false; //mcRace::IsFinished( mcRace::GetCurrentRace() );
}

bool IsGarageUnlocked()
{
/*
	int beachesState, hollywoodState;
	fuiFlashManagerSingleton::GetInstance().GetRegistry().GetValueInt( "be_garage_state", beachesState );
	fuiFlashManagerSingleton::GetInstance().GetRegistry().GetValueInt( "hw_garage_state", hollywoodState );
	if( beachesState != eGarageState_ACTIVE && hollywoodState != eGarageState_ACTIVE && mcCareer::sanCheckUnlockableFlag( MCCAREER_UNLOCKABLE_ENTERED_GARAGE ) )
	{
		return false;
	}
*/

	return true;
}

void Go(const char *stateName)
{
	FlashNavigator *pUI = UIMANAGER->GetUINavigator();
	pUI->RequestTransition(pUI->GetStateByIdName(stateName));
}

void SafelyPopState(const char *stateToPop)
{
	FlashNavigator *pUI = UIMANAGER->GetUINavigator();
	UIObject *pPause = pUI->GetStateByIdName(stateToPop);
	pUI->GetDefaultHandler().stackPush(pUI,pPause);
	pUI->GetDefaultHandler().stackPop(pUI);
	pUI->RequestTransition(pUI->GetDefaultHandler().m_Stack[pUI->GetDefaultHandler().m_StackHeight-1]);
}

void SafelyCloseToNormal()
{
	MAIN_FORM.GetForm(ePauseForm).Close();
	
	//SafelyPopState("PauseMenu");
	FlashNavigator *pUI = UIMANAGER->GetUINavigator();
	if (pUI->GetStateByIdName("HudPlayerList"))
	{
		pUI->RequestTransition(pUI->GetStateByIdName("HudPlayerList"));
	}
//	mcAudioManager::ForcePauseState(false);
}

void SafelyCloseToReplay()
{
	FlashNavigator *pUI = UIMANAGER->GetUINavigator();
	pUI->RequestTransition(pUI->GetStateByIdName("Replay"));

	MAIN_FORM.GetForm(ePauseForm).Close();

	//	Shut speech up and make sure audio unpauses
//	mcAudioManager::ForcePauseState(false);
//	SPEECHSYSTEM.StopEverything();
//	mcAudioManager::StopCutsceneStreams();
}

void ToCruise()
{
/*
	fuiRegistry::GetInstance().SetValue("raceOverCommand","returnToCruise");
	fuiRegistry::GetInstance().SetValue("raceOverTrigger",1);

	UIObject* pState;

	pState = UIMANAGER->GetUINavigator()->GetStateByIdName("Cruising");
	Assert(pState);
	UIMANAGER->GetUINavigator()->RequestTransition(pState);
*/
}

bool IsComplete()
{
	fuiVariant& rVariant = fuiRegistry::GetInstance().GetValue("bComplete");

	if(&rVariant)
	{
		return rVariant.asBool();
	}
	else
	{
		return false;
	}
}

bool IsFailure()
{
	fuiVariant& rVariant = fuiRegistry::GetInstance().GetValue("bFailure");

	if(&rVariant)
	{
		return rVariant.asBool();
	}
	else
	{
		return false;
	}
}

bool IsPoliceChasingPlayer()
{
// 	int localPlayerIdx = mcConfig::GetLocalPlayerIndex();
// 	if(PLAYERMGR && PLAYERMGR->IsPlayerValid(localPlayerIdx))
// 	{
// 		mcPlayer* pPlayer = &PLAYERMGR->GetPlayer(localPlayerIdx);
// 		return mcPoliceManager::IsPullingRacerOver(pPlayer) || mcPoliceManager::IsRacerPursued(pPlayer);
// 	}

	return false;
}

void InitRaceFavorites()
{
/*
	((mcRaceListForm&)MAIN_FORM.GetForm(eArcadeForm)).UpdateNextFavoritesDataSet();

	const mcRaceScriptData* pRaceData = mcRace::GetCurrentRace()? mcRace::GetCurrentRace()->GetRaceDataPtr() : NULL;

	if(pRaceData && !pRaceData->m_bIsEditorRace)
	{
		switch(pRaceData->m_nUIType)
		{
		case mc::EUIRaceType_BEAT_ME_THERE:
		case mc::EUIRaceType_BEAT_ME_THERE_TUTORIAL:
		case mc::EUIRaceType_PAYBACK:
		case mc::EUIRaceType_DELIVERY:
		case mc::EUIRaceType_FREEWAY:
		case mc::EUIRaceType_DYNAMIC:
			break;
		default:
			// Determine whether or not the current race is already in the favorites list
			if ( fuiLogic::GetInstance()->findInRaceFavorites() != -1 )
			{
				// Race is in the favorites list, enable the remove state
				UIMANAGER->GetState("RO_AddFav")->Disable();
				UIMANAGER->GetState("RO_RemoveFav")->Enable();
				UIMANAGER->GetState("PM_AddFav")->Disable();
				UIMANAGER->GetState("PM_RemoveFav")->Enable();
				return;
			}
			else
			{
				// Race isn't in the favorites list, enable the add state
				UIMANAGER->GetState("RO_AddFav")->Enable();
				UIMANAGER->GetState("RO_RemoveFav")->Disable();
				UIMANAGER->GetState("PM_AddFav")->Enable();
				UIMANAGER->GetState("PM_RemoveFav")->Disable();
				return;
			}
		}
	}

	UIMANAGER->GetState("RO_AddFav")->Disable();
	UIMANAGER->GetState("RO_RemoveFav")->Disable();
	UIMANAGER->GetState("PM_AddFav")->Disable();
	UIMANAGER->GetState("PM_RemoveFav")->Disable();
*/	
}

bool fuiFixForm::IsControlCamera()
{
	return true;
	//if(GetParentForm())
	//	return GetParentForm()->IsControlCamera();
	//else
	//	return false;
}

fuiFixForm::fuiFixForm() : fuiSimpleListForm("","",3,1),
	m_quickFixItem("Common_Yes")//("quick_fix")
	//, m_fullFixItem("full_fix")
{
	//m_mainList.GetItems().Push(&m_quickFixItem);
	////m_mainList.GetItems().Push(&m_fullFixItem);
	//m_mainList.GetItems().Push(&m_actionItem);
}

//
// Show quick fix menu item only if there is damage.  Only show Full Fix item if the cost of
// a full fix is greater than zero.  Only show action if action is requested by caller of this function.
// If we are damaged out, we don't have the option to not fix.  We also want to set the default selection
// to the first ungrayed out option.
//
void fuiFixForm::Show(UIObject* parent, const char *text, const char *caption, const char *actionText, bool commitment, bool , rage::datCallback callback)
{
	m_promptStrip.SetItems(AcceptBackPrompt,2);

	Assert(parent);
	this->SetContext(parent->GetContext());
	this->SetText(caption);
	m_label.SetText(text);
	m_callback = callback;

	m_mainList.GetItems().clear();
	m_mainList.GetItems().Push(&m_quickFixItem);
	////m_mainList.GetItems().Push(&m_fullFixItem);
	if(actionText)
	{
		m_mainList.GetItems().Push(&m_actionItem);
	}
	
	m_quickFixItem.SetEnabled(false);
//	m_fullFixItem.SetEnabled(false);
	m_actionItem.SetEnabled(false);
	m_actionItem.SetText(actionText);

	int iFirstEnabledIndex = 1; //2;

//	mcGarage::GetInstance()->SetQuickFixPrice();

	if(actionText && actionText[0] /*&& !IsDamagedOut()*/)
	{
		m_actionItem.SetEnabled(true);
	}

// 	if(mcGarage::GetInstance()->CalculateQuickFixPrice() || IsDamagedOut())
// 	{
// //		m_fullFixItem.SetEnabled(true);
// //		iFirstEnabledIndex = 1;
// 		iFirstEnabledIndex = 0;
// 		m_quickFixItem.SetEnabled(true);
// 	}

	m_commitment = commitment;

	this->ShowDialog((fuiForm*)parent);

	m_mainList.SetSelectedIndex(iFirstEnabledIndex);

	//removed as per new design stemming from bug 19717
	//if( !mcCareer::sanCheckUnlockableFlag( MCCAREER_UNLOCKABLE_FLAG_FIX_TUTORIAL ) )
	//{
	//	mcCareer::sanSetUnlockableFlag( MCCAREER_UNLOCKABLE_FLAG_FIX_TUTORIAL );
	//	fuiLogic::ShowForm( &MAIN_FORM, &MAIN_FORM.GetForm(eTutorialQuickFixForm) );
	//}
}

// void NoFix(bool commitment)
// {
// 	fuiCallbacks::nothingCar(NULL);
// 	if(commitment)
// 	{
// //		mcGarage::GetInstance()->RestoreDamageToCar();
// 	}
// }

// void QuickFix(bool commitment)
// {
// 	fuiCallbacks::sm_PlayFlashSoundFunc("QUICK_FIX_MASTER"); 
// 
// 	fuiCallbacks::quickFixCar(NULL);
// 	if(commitment)
// 	{
// 		fuiLogic::SaveCarDamage();
// 	}
// }

void fuiFixForm::OnPaint()
{
	fuiSimpleListForm::OnPaint();

	if(this->IsControlCamera())
	{
		this->GetContext()->SetGlobal("transition_type",2);
	}
}

//
// You can't back out during damaged out
//
bool fuiFixForm::OnKeyPress(int key, const rage::Vector3& velocity)
{
	switch(key)
	{
	case MENU_ACCEPT:
		{
			fuiCtrl *pSelection = m_mainList.GetItems()[m_mainList.GetSelectedIndex()];
			if(pSelection->IsEnabled())
			{
				if(pSelection == &m_quickFixItem)
				{
//					QuickFix(m_commitment);
				}
				//else if(pSelection == &m_fullFixItem)
				//{
				//	fuiLogic::GetInstance()->FullFix(m_commitment);
				//}
				else if(pSelection == &m_actionItem)
				{
//					NoFix(m_commitment);
				}
				
				fuiCallbacks::sm_PlayFlashSoundFunc(k_szMenuSelectSound); 
				this->SetDialogResult(eDR_Yes);
				this->Close();
				//NETMGR.GetLobby().GetStatScribe().SetGoalAttack(false);
			}
			else
			{
				fuiCallbacks::sm_PlayFlashSoundFunc(k_szMenuErrorSound); 
			}
			return true;
		}
	case MENU_CANCEL:
		{
			fuiCallbacks::sm_PlayFlashSoundFunc(k_szMenuBackSound); 
			this->SetDialogResult(eDR_Cancel);
			this->Close();
			return true;
		}
	default:
		fuiSimpleListForm::OnKeyPress(key,velocity); // MIGRAX - was fuiSimpleListForm<const char*>
		return true; //don't let the key stroke propagate anywhere
	}
}

void fuiPause::OnPaint()
{
	UIObject::OnPaint();

	m_promptStrip.SetContext(UIMANAGER->FindMovie("PAUSEMOVIE")->GetContextPtr());
	m_promptStrip.OnPaint();
	m_promptStrip.NotifyTransition(0);
}

void Replay()
{
	fuiRegistry::GetInstance().SetValue("raceOverCommand","replay");
	fuiRegistry::GetInstance().SetValue("raceOverTrigger",1);

	SafelyCloseToReplay();
}

bool IsReplay()
{
// 	mcRace* pRace = mcRace::GetCurrentRace();
// 
// 	if(pRace)
// 	{
// 		return pRace->GetRaceDataPtr()->m_bReplay ? true : false;
// 	}
// 	else
	{
		return false;
	}
}

//
// Returns false because we still want the state machine to handle the key
//
bool fuiPause::OnKeyPress(int key, const rage::Vector3& velocity)
{
	int iTransitionOK;
	this->GetContext()->GetGlobal("transition_isplaying",iTransitionOK);

	if(iTransitionOK != -1)
	{
		return true;
	}

	if(UIObject::OnKeyPress( key, velocity))
	{
		return true;
	}

	return false;
}

void fuiBaseMenu::ConfirmClose()
{
	if(IsRaceOver() /*|| IsDamagedOut()*/)
	{
		if(IsPoliceChasingPlayer()) //added for bug 23832 - Player is prompted to repair vehicle when cops in pursuit
		{
//			ReturnToCruise();
		}
		else
		{
			fuiForm& rPauseForm = MAIN_FORM.GetForm(ePauseForm);
			if( this->IsFixingAllowed() )
			{
				m_fixMenu.Show(&rPauseForm,this->GetContinueDescription(),this->GetContinueTitle(),/*"Dialog_ReturnToCruise_Action"*/"Common_No",true,false,datCallback(MFA1(fuiBaseMenu::OnReturnToCruise), (datBase*)this,NULL,true));
			}
			else
			{
				m_msgBox.Show(&rPauseForm,this->GetContinueDescription(),this->GetContinueTitle(),eMBB_YesNo,datCallback(MFA1(fuiBaseMenu::OnReturnToCruise), (datBase*)this,NULL,true));
			}
		}
	}
	else
	{
		SafelyCloseToNormal();
	}
}

void fuiBaseMenu::OnNextRace(fuiForm* form)
{
	Assert(form);
	if(form->GetDialogResult() == eDR_Yes)
	{
		//PostRaceFix(form);

//		GoNextRace();
	}
}

void GoRestartRace()
{
//	CINESCRIPTMGR->SetRestartTimeOffset( 1.f );
	fuiRegistry::GetInstance().SetValue("raceOverCommand","restart");
	fuiRegistry::GetInstance().SetValue("raceOverTrigger",1);

	SafelyCloseToNormal();
}

void fuiBaseMenu::OnGoRestartRaceWithFullFix( fuiForm* form )
{
	Assert(form);
	if(form->GetDialogResult() == eDR_Yes)
	{
//		fuiLogic::GetInstance()->FullFix( false, false ); 
		GoRestartRace();
	}
}

//
// Restore damage or quick fix, or don't do anything at all
//
void fuiBaseMenu::OnRestartRace(fuiForm* form)
{
	Assert(form);
	if(form->GetDialogResult() == eDR_Yes)
	{
		//
		// TODO: figure out a way to get rid of this dynamic cast:
		//
		if(dynamic_cast<fuiFixForm *>(form))
		{
			//PostRaceFix(form);
		}

		GoRestartRace();
	}
}

// void mcPaybackMenu::OnRestartDontFix(fuiForm* form)
// {
// 	Assert(form);
// 	if(form->GetDialogResult() == eDR_Yes)
// 	{
// 		GoRestartRace();
// 	}
// }
// 
void GoQuitRace()
{
	fuiRegistry::GetInstance().SetValue("raceOverCommand","quit");
	fuiRegistry::GetInstance().SetValue("raceOverTrigger",1);
}

void fuiBaseMenu::OnQuitRace(fuiForm* form)
{
	Assert(form);
	if(form->GetDialogResult() == eDR_Yes)
	{
		GoQuitRace();
		//PostRaceFix(form);
// 		NETMGR.GetLobby().GetStatScribe().SetGoalAttack(false);
// 		NETMGR.GetLobby().GetStatScribe().SetOnlineTournament(false);
// 		mcConfig::SetPowerupsEnabled(false);

		FlashNavigator* pNav = UIMANAGER->GetUINavigator();
		if(pNav->GetStateByIdName("RacingOL_FromLobby")->IsListener())
		{
			pNav->GetStateByIdName("LobbyMeeting")->Goto(pNav);
		}
		else if(pNav->GetStateByIdName("LobbyMeeting")->IsListener())
		{
			pNav->GetStateByIdName("OnlineLobbyScene")->Goto(pNav);
		}
		else
		{
			Assert(!IsDelivery() );

			pNav->GetStateByIdName("Cruising")->Goto(pNav);

			SafelyCloseToNormal();
		}
	}
}

bool fuiBaseMenu::IsFixingAllowed() const
{
	return true; // mcPoliceMgrSingleton::IsInstantiated() ? !IsPoliceChasingPlayer()||POLICEMGR.HasLostTarget(true) : true;
}

void fuiBaseMenu::OnReturnToCruise(fuiForm* form)
{
	if(form->GetDialogResult() == eDR_Yes)
	{
//		ReturnToCruise();
	}
}

rage::swfCONTEXT* fuiPause::GetContext() const
{
	return &UIMANAGER->FindMovie("PAUSEMOVIE")->GetContext();
}

bool IsTelephoneChallenge()
{
// 	bool bIsTelephoneChallenge = false;
// 	if(mcRace::GetCurrentRace())
// 	{
// 		Assert(mcRace::GetCurrentRace()->GetRaceDataPtr());
// 		if(mcRace::GetCurrentRace()->GetRaceDataPtr()->m_nUIType == mc::EUIRaceType_TELEPHONE_CHALLENGE)
// 		{
// 			bIsTelephoneChallenge = true;
// 		}
// 	}
// 
// 	return bIsTelephoneChallenge;
	return false;
}

bool IsMissionRace()
{
// 	if(mcRace::GetCurrentRace())
// 	{
// 		Assert(mcRace::GetCurrentRace()->GetRaceDataPtr());
// 		return mcRace::GetCurrentRace()->GetRaceDataPtr()->m_bIsMissionRace != 0;
// 	}
// 
	return false;
}

bool fuiBaseMenu::IsResultsVisible() const
{
	if(IsReplay())
	{
		return true;
	}
//	else if(IsDamagedOut())
// 	{
// //		if(IsTelephoneChallenge()) //22658 - [SD] [UI] The 'Race Results' option is missing from end of race menus when the Player damages out
// //		{
// 			return true;
// //		}
// //		else
// //		{
// //			return false;
// //		}
// 	}
// //	else
// 	{
 		return true;
// 	}
}

void fuiBaseMenu::OnJumpToGarageConfirm(fuiForm* form)
{
	switch(form->GetDialogResult())
	{
	case eDR_Yes:
		{
			////
			//// Stop the OnDamagedOut event from firing
			////
			//if(IsDamagedOut())
			//{
			//	mcPlayer& rPlayer = PLAYERMGR->GetPlayer(0); //TODO: support splitscreen
			//	mcCar* pCar = rPlayer.GetCar();
			//	Assert(pCar && "Expected non-null car");
			//	pCar->GetSim()->SetDrivableState();
			//}
			//else
			//{
			//	if((dynamic_cast<fuiFixForm *>(form))->GetSelectedIndex())
			//	{
			//		fuiCallbacks::repairCar(NULL);
			//	}
			//	else
			//	{
			//		QuickFix(false);
			//	}
			//}
//			GoGarage();
		}
		break;
	default:
		break;
	}
}

void fuiBaseMenu::OnReturnToHangout(fuiForm *form)
{
	switch(form->GetDialogResult())
	{
	case eDR_Yes:
		{
			fuiRegistry::GetInstance().SetValue("raceOverTrigger",1);
			fuiRegistry::GetInstance().SetValue("raceOverCommand","");
			//mcGarage::GetInstance()->WarpToGarage();

			//PostRaceFix(&m_fixMenu);
	
			FlashNavigator* pNav = UIMANAGER->GetUINavigator();
			pNav->GetStateByIdName("Cruising")->Goto(pNav);
			SafelyCloseToNormal();
		}
		break;
	default:
		break;
	}
}

void fuiBaseMenu::OnStartArcade(fuiForm *form)
{
	switch(form->GetDialogResult())
	{
	case eDR_Yes:
		{
// 			NETMGR.GetLobby().GetStatScribe().SetGoalAttack(false); //make sure goal attack hud displays do not stay on
// 			NETMGR.GetLobby().GetStatScribe().SetOnlineTournament(false);
// 			ToCruise();
			Go("ArcadeMenuDefault");
		}
		break;
	default:
		break;
	}
}

void fuiBaseMenu::OnStartRaceEditor(fuiForm *form)
{
	switch(form->GetDialogResult())
	{
	case eDR_Yes:
		{
// 			NETMGR.GetLobby().GetStatScribe().SetGoalAttack(false); //make sure goal attack hud displays do not stay on
// 			NETMGR.GetLobby().GetStatScribe().SetOnlineTournament(false);
// 			ToCruise();
			UIMANAGER->GetState("Editor_Enter_Options")->TransitionTo();
			//Go("Editor_Enter_Options");
		}
		break;
	default:
		break;
	}
}

void fuiBaseMenu::OnEnterRaceEditorThroughPause(fuiForm *form)
{
	FlashNavigator* pNav = UIMANAGER->GetUINavigator();
	switch(form->GetDialogResult())
	{
	case eDR_Yes:
		{
// 			mcInGameEditor::GetInstance()->SetRELoadonEnter(NULL);
// 			mcInGameEditor::GetInstance()->SetEditorPaused(false);
			fuiRegistry::GetInstance().SetValue("Script_SuspendAllOpponents",1.0f);
			fuiCallbacks::sm_PlayFlashSoundFunc("UI_MENU_SELECT");
			pNav->GetStateByIdName("Editor_WaitEnter_Create")->Goto(pNav);
		}
		break;
	case eDR_No:
		{
// 			mcInGameEditor::GetInstance()->SetRELoadonEnter(NULL);
// 			mcInGameEditor::GetInstance()->SetEditorPaused(true);
			fuiRegistry::GetInstance().SetValue("Script_SuspendAllOpponents",1.0f);
			fuiCallbacks::sm_PlayFlashSoundFunc("UI_MENU_SELECT");
			pNav->GetStateByIdName("Editor_WaitEnter_Load")->Goto(pNav);
		}
		break;
	default:
		break;
	}
}

void fuiBaseMenu::OnStartGoalAttack(fuiForm *form)
{
	switch(form->GetDialogResult())
	{
	case eDR_Yes:
		{
// 			NETMGR.GetLobby().GetStatScribe().SetGoalAttack(true);
// 			NETMGR.GetLobby().GetStatScribe().SetOnlineTournament(false);
// 			ToCruise();
			Go("ArcadeMenuGoalAttack");
		}
		break;
	default:
		break;
	}
}

void fuiBaseMenu::OnStartOnline(fuiForm *form)
{
	switch(form->GetDialogResult())
	{
	case eDR_Yes:
		{
// 			NETMGR.GetLobby().GetStatScribe().SetGoalAttack(false); //make sure goal attack hud displays do not stay on
// 			NETMGR.GetLobby().GetStatScribe().SetOnlineTournament(false);
// 			ToCruise();
			Go("Online");
		}
		break;
	default:
		break;
	}
}

void fuiBaseMenu::OnStartLAN(fuiForm *form)
{
	switch(form->GetDialogResult())
	{
	case eDR_Yes:
		{
// 			NETMGR.GetLobby().GetStatScribe().SetGoalAttack(false); //make sure goal attack hud displays do not stay on
// 			NETMGR.GetLobby().GetStatScribe().SetOnlineTournament(false);
// 			ToCruise();
			Go("LAN");
		}
		break;
	default:
		break;
	}
}

void fuiBaseMenu::OnStartTournament(fuiForm *form)
{
	switch(form->GetDialogResult())
	{
	case eDR_Yes:
		{
// 			NETMGR.GetLobby().GetStatScribe().SetGoalAttack(false); //make sure goal attack hud displays do not stay on
// 			NETMGR.GetLobby().GetStatScribe().SetOnlineTournament(true);
// 			ToCruise();
			Go("TournamentLeaderboard");
		}
		break;
	default:
		break;
	}
}



void fuiBaseMenu::OnViewReplay(fuiForm* form)
{
	switch(form->GetDialogResult())
	{
	case eDR_Yes:
		{
			//NETMGR.GetLobby().GetStatScribe().SetGoalAttack(false); //make sure goal attack hud displays do not stay on
			//NETMGR.GetLobby().GetStatScribe().SetOnlineTournament(false);
			Replay();
		}
		break;
	default:
		break;
	}
}

void fuiBaseMenu::OnRemoveFav(fuiForm* form)
{
	if(form->GetDialogResult() == eDR_Yes)
	{
//		fuiLogic::GetInstance()->RemoveCurrentRaceFromFavorites(true);
		InitRaceFavorites();
		fuiPauseForm& rForm =
			(fuiPauseForm&)MAIN_FORM.GetForm(ePauseForm);
			
		rForm.RefreshMenuItems();
		rForm.m_pausePanel.GetTabControl().Refresh();
	}
}

// bool fuiRedLightMenu::IsGarageFlyable() const
// {
// 	if( IsDamagedOut() || 
// 		IsRaceOver() )
// 	{
// 		return IsGarageUnlocked();
// 	}
// 	else
// 	{
// 		return false;
// 	}
// }

namespace {
	bool IsNetworked() { return false; }
}
bool fuiCruiseMenu::IsResultsVisible() const
{
	//the last race results are always available online
	if (IsNetworked())
		return true;

	return false;
}

void fuiCruiseMenu::Setup(bool bIsRaceOver)
{
// 	mcLobby::netJoinStageID eNetJoinStage	= NETMGR.GetLobby().GetJoinStage();
// 	mc::eNetGameMode		eNetMode		= NETMGR.GetLobby().GetGameMode();

	fuiBaseMenu::Setup(bIsRaceOver);

	UIMANAGER->GetState("RO_Replay")->SetEnabled( false );

//	if (eNetMode==mc::eNetGameModeCruise && eNetJoinStage>=mcLobby::netGameStageJoinIntent)
		//UIMANAGER->GetState("PM_CreateRace")->Block( );
// 	else
// 		UIMANAGER->GetState("PM_CreateRace")->Unblock( );

	UIMANAGER->GetState("PM_ViewReplay")->SetEnabled( false );
	UIMANAGER->GetState("PM_Restart")->SetEnabled( false );
	UIMANAGER->GetState("PM_AbortRace")->SetEnabled( false );
//	UIMANAGER->GetState("PM_ChangeRide")->SetEnabled( true /*eNetMode==mc::eNetGameModeCruise && eNetJoinStage<mcLobby::netGameStageJoinIntent */);

	if(IsDamagedOut())
	{
		UIMANAGER->GetState("RO_ReturnToCruise")->SetEnabled( false );
		UIMANAGER->GetState("RO_Restart")->SetEnabled( false );
	}

// 	UIObject * pState = UIMANAGER->GetState("PM_Downloadable");
// 	int numNewContentDownloads = 0, numAvailableContentDownloads = 0;
// //	MYGAMER.GetContentCounts(numNewContentDownloads, numAvailableContentDownloads);
// #if !__XENON
// 	numAvailableContentDownloads = 1;
// #endif
// 	if (numNewContentDownloads>0 || numAvailableContentDownloads>0)
// 	{
// 		pState->SetEnabled( true );
// 		if (numNewContentDownloads>0)
// 			pState->SetText( "pause_downloadable_content_new" );
// 		else
// 			pState->SetText( "pause_downloadable_content" );
// 	}
// 	else
// 	{
// 		pState->SetEnabled( false );
// 	}
}

bool fuiCruiseMenu::IsNeedGarageConfirmation() const
{
	return false;
}

bool IsMidReplay()
{
//	mcReplayMgr& replay = mcReplayMgr::GetInstance();
//	return IsReplay() && !replay.GetHasDiverged() && replay.GetCurrentFrame() < replay.GetNumFrames() - 1 ;
	return false;
}

bool fuiBaseMenu::IsGameModesDisabled() const
{
	bool bLocked = false;
	fuiVariant* rVariant = fuiRegistry::GetInstance().Access("nInTutorial");
	if(rVariant)
	{
		bLocked = rVariant->asBool();
	}
	return bLocked;
}

void fuiBaseMenu::SetupOrderPause()
{
	UIObject *pState = UIMANAGER->GetState("PauseTab");

// #if !__FINAL
// 	if(PARAM_demo.Get())
// 	{
// 		pState->SetupChildRelationships(UIMANAGER->GetState("PM_Restart"));
// 	}
// 	else
// #endif
	{
		pState->SetupChildRelationships(UIMANAGER->GetState("PM_Continue"));
		pState->SetupChildRelationships(UIMANAGER->GetState("PM_NextRace"));
		pState->SetupChildRelationships(UIMANAGER->GetState("PM_AbortRace"));
		pState->SetupChildRelationships(UIMANAGER->GetState("PM_Restart"));
		pState->SetupChildRelationships(UIMANAGER->GetState("PM_ReturnToHangout"));
		pState->SetupChildRelationships(UIMANAGER->GetState("PM_AddFav"));
		pState->SetupChildRelationships(UIMANAGER->GetState("PM_RemoveFav"));
		pState->SetupChildRelationships(UIMANAGER->GetState("PM_ViewReplay"));
		pState->SetupChildRelationships(UIMANAGER->GetState("PM_RequestRace"));
#if __PPU
		if(UIMANAGER->GetState("PM_Friends")) 
			pState->SetupChildRelationships(UIMANAGER->GetState("PM_Friends"));
#endif
		pState->SetupChildRelationships(UIMANAGER->GetState("PM_ChangeRide"));
		pState->SetupChildRelationships(UIMANAGER->GetState("PM_Offline"));
		pState->SetupChildRelationships(UIMANAGER->GetState("PM_WarpGarage"));
		pState->SetupChildRelationships(UIMANAGER->GetState("PM_TakePhoto"));
	}
}

void fuiBaseMenu::SetupOrderRaceOver()
{
	UIObject *pState = UIMANAGER->GetState("RaceOverTab");
//#if !__FINAL
//	if(PARAM_demo.Get())
//	{
//		pState->SetupChildRelationships(UIMANAGER->GetState("RO_Restart"));
//	}
//	else
//#endif
	{
		pState->SetupChildRelationships(UIMANAGER->GetState("RO_FullFix"));
		pState->SetupChildRelationships(UIMANAGER->GetState("RO_NextRace"));
		pState->SetupChildRelationships(UIMANAGER->GetState("RO_ReturnToCruise"));
		pState->SetupChildRelationships(UIMANAGER->GetState("RO_ContinueToRaceStart"));
		pState->SetupChildRelationships(UIMANAGER->GetState("RO_AbortRace"));
		pState->SetupChildRelationships(UIMANAGER->GetState("RO_ReturnToHangout"));
		pState->SetupChildRelationships(UIMANAGER->GetState("RO_RaceBack"));
		pState->SetupChildRelationships(UIMANAGER->GetState("RO_TimeTrialComplete"));
		pState->SetupChildRelationships(UIMANAGER->GetState("RO_Restart"));
		if( this->IsResultsVisible() )
		{
			pState->SetupChildRelationships(UIMANAGER->GetState("RO_Results"));
		}
		pState->SetupChildRelationships(UIMANAGER->GetState("RO_AddFav"));
		pState->SetupChildRelationships(UIMANAGER->GetState("RO_RemoveFav"));
		pState->SetupChildRelationships(UIMANAGER->GetState("RO_Replay"));
		pState->SetupChildRelationships(UIMANAGER->GetState("RO_Garage"));
	}
}

void fuiBaseMenu::SetupMenuStrings()
{
	FlashNavigator* pNav = UIMANAGER->GetUINavigator();
	pNav->GetStateByIdName("PM_Continue")->SetText("pause_continue");
	pNav->GetStateByIdName("PM_NextRace")->SetText("pause_next_race");
	pNav->GetStateByIdName("PM_Restart")->SetText("pause_restart_race");
	pNav->GetStateByIdName("PM_AbortRace")->SetText("pause_quit_race");

	pNav->GetStateByIdName("RO_NextRace")->SetText("pause_next_race");
	pNav->GetStateByIdName("RO_Restart")->SetText("pause_restart_race");
	pNav->GetStateByIdName("RO_Garage")->SetText("pause_return_garage");
	pNav->GetStateByIdName("RO_Results")->SetText("pause_race_results");

	if(IsDamagedOut())
	{
		pNav->GetStateByIdName("RO_ReturnToCruise")->SetText("pause_return_cruise");
	}
	else
	{
		pNav->GetStateByIdName("RO_ReturnToCruise")->SetText("pause_continue_to_cruise");
	}
}

void fuiBaseMenu::HandleWANRequest()
{
	fuiForm& rPauseForm = MAIN_FORM.GetForm(ePauseForm);

	if(fuiBaseMenu::IsRacing())
	{
		if(this->IsFixingAllowed())
		{
			m_fixMenu.Show(&rPauseForm,this->GetQuitDescription(),this->GetQuitTitle(),this->GetQuitAction(),false,false,datCallback(MFA1(fuiBaseMenu::OnStartOnline), (datBase*)this,NULL,true));
		}
		else
		{
			m_msgBox.Show(&rPauseForm,this->GetQuitDescription(),this->GetQuitTitle(),eMBB_YesNo,datCallback(MFA1(fuiBaseMenu::OnStartOnline), (datBase*)this,NULL,true));
		}
	}
	else if(IsDamagedOut())
	{
		m_fixMenu.Show(&rPauseForm,"Dialog_Description_DamagedOut","Dialog_Title_DamagedOut",NULL,false,false,datCallback(MFA1(fuiBaseMenu::OnStartOnline), (datBase*)this,NULL,true));
	}
	else
	{
		Go("Online");
	}
}

void fuiBaseMenu::HandleArcadeRequest()
{
	fuiForm& rPauseForm = MAIN_FORM.GetForm(ePauseForm);

	if(fuiBaseMenu::IsRacing())
	{
		if(this->IsFixingAllowed())
		{
			m_fixMenu.Show(&rPauseForm,this->GetQuitDescription(),this->GetQuitTitle(),this->GetQuitAction(),false,false,datCallback(MFA1(fuiBaseMenu::OnStartArcade), (datBase*)this,NULL,true));
		}
		else
		{
			m_msgBox.Show(&rPauseForm,this->GetQuitDescription(),this->GetQuitTitle(),eMBB_YesNo,datCallback(MFA1(fuiBaseMenu::OnStartArcade), (datBase*)this,NULL,true));
		}
	}
	else if(IsDamagedOut())
	{
		m_fixMenu.Show(&rPauseForm,"Dialog_Description_DamagedOut","Dialog_Title_DamagedOut",NULL,false,false,datCallback(MFA1(fuiBaseMenu::OnStartArcade), (datBase*)this,NULL,true));
	}
	else
	{
		Go("ArcadeMenuDefault");
	}
}

void fuiBaseMenu::HandleGoalAttackRequest()
{
	fuiForm& rPauseForm = MAIN_FORM.GetForm(ePauseForm);

	if(fuiBaseMenu::IsRacing())
	{
		if(this->IsFixingAllowed())
		{
			m_fixMenu.Show(&rPauseForm,this->GetQuitDescription(),this->GetQuitTitle(),this->GetQuitAction(),false,false,datCallback(MFA1(fuiBaseMenu::OnStartGoalAttack), (datBase*)this,NULL,true));
		}
		else
		{
			m_msgBox.Show(&rPauseForm,this->GetQuitDescription(),this->GetQuitTitle(),eMBB_YesNo,datCallback(MFA1(fuiBaseMenu::OnStartGoalAttack), (datBase*)this,NULL,true));
		}
	}
	else if(IsDamagedOut())
	{
		m_fixMenu.Show(&rPauseForm,"Dialog_Description_DamagedOut","Dialog_Title_DamagedOut",NULL,false,false,datCallback(MFA1(fuiBaseMenu::OnStartGoalAttack), (datBase*)this,NULL,true));
	}
	else
	{
		Go("ArcadeMenuGoalAttack");
	}
}

void fuiBaseMenu::HandleLANRequest()
{
	fuiForm& rPauseForm = MAIN_FORM.GetForm(ePauseForm);

	if(fuiBaseMenu::IsRacing())
	{
		if(this->IsFixingAllowed())
		{
			m_fixMenu.Show(&rPauseForm,this->GetQuitDescription(),this->GetQuitTitle(),this->GetQuitAction(),false,false,datCallback(MFA1(fuiBaseMenu::OnStartLAN), (datBase*)this,NULL,true));
		}
		else
		{
			m_msgBox.Show(&rPauseForm,this->GetQuitDescription(),this->GetQuitTitle(),eMBB_YesNo,datCallback(MFA1(fuiBaseMenu::OnStartLAN), (datBase*)this,NULL,true));
		}
	}
	else if(IsDamagedOut())
	{
		m_fixMenu.Show(&rPauseForm,"Dialog_Description_DamagedOut","Dialog_Title_DamagedOut",NULL,false,false,datCallback(MFA1(fuiBaseMenu::OnStartLAN), (datBase*)this,NULL,true));
	}
	else
	{
		Go("LAN");
	}
}

void fuiBaseMenu::HandleTournamentRequest()
{
	fuiForm& rPauseForm = MAIN_FORM.GetForm(ePauseForm);

	if(fuiBaseMenu::IsRacing())
	{
		if(this->IsFixingAllowed())
		{
			m_fixMenu.Show(&rPauseForm,this->GetQuitDescription(),this->GetQuitTitle(),this->GetQuitAction(),false,false,datCallback(MFA1(fuiBaseMenu::OnStartTournament), (datBase*)this,NULL,true));
		}
		else
		{
			m_msgBox.Show(&rPauseForm,this->GetQuitDescription(),this->GetQuitTitle(),eMBB_YesNo,datCallback(MFA1(fuiBaseMenu::OnStartTournament), (datBase*)this,NULL,true));
		}
	}
	else if(IsDamagedOut())
	{
		m_fixMenu.Show(&rPauseForm,"Dialog_Description_DamagedOut","Dialog_Title_DamagedOut",NULL,false,false,datCallback(MFA1(fuiBaseMenu::OnStartTournament), (datBase*)this,NULL,true));
	}
	else
	{
		Go("TournamentLeaderboard");
	}
}



void fuiBaseMenu::OnClosed()
{
	//resuming should be handled by the state machine now (depending on whether we go HUD or replay HUD)
	//if(!this->IsRaceOver())
	//{
	//	fuiCallbacks::resume();
	//}
}

void fuiBaseMenu::OnPayFineConfirmArcade(fuiForm *form)
{
	if(form->GetDialogResult() == eDR_Yes)
	{
		this->HandleArcadeRequest();
	}
}

void fuiBaseMenu::OnPayFineConfirmGoalAttack(fuiForm *form)
{
	if(form->GetDialogResult() == eDR_Yes)
	{
		this->HandleGoalAttackRequest();
	}
}

void fuiBaseMenu::OnPayFineConfirmWAN(fuiForm *form)
{
	if(form->GetDialogResult() == eDR_Yes)
	{
		this->HandleWANRequest();
	}
}

void fuiBaseMenu::OnPayFineConfirmLAN(fuiForm *form)
{
	if(form->GetDialogResult() == eDR_Yes)
	{
		this->HandleLANRequest();
	}
}

void fuiBaseMenu::OnPayFineConfirmTournament(fuiForm *form)
{
	if(form->GetDialogResult() == eDR_Yes)
	{
		this->HandleTournamentRequest();
	}
}

void fuiBaseMenu::OnDamageOutFix() { } // delete this (among other things)

bool fuiBaseMenu::OnKeyPress(int key, const rage::Vector3&)
{
	fuiPauseForm& rPauseForm = (fuiPauseForm&)MAIN_FORM.GetForm(ePauseForm);

	switch(key)
	{
//	case ALT_PAUSE:
	case MENU_CANCEL:
		if(!IsRaceOver() && !IsDamagedOut())
		{
			fuiCallbacks::sm_PlayFlashSoundFunc(k_szMenuBackSound);
			this->ConfirmClose();
		}
		return true;
	case MENU_ACCEPT:
		//if(IsSelected("PM_Playlist"))
		//{
		//	fuiForm& rForm =
		//		MAIN_FORM.GetForm(eMusicGenreForm);

		//	rForm.ShowDialog(&rPauseForm);
		//	return true;
		//}
		//else
		if(IsSelected("PM_GameOptions"))
		{
//			MAIN_FORM.GetForm(eOptionsForm).ShowDialog(&rPauseForm);
			return true;
		}
		else if(IsSelected("PM_Playlist"))
		{
			rPauseForm.SetVisible(false);
//			MAIN_FORM.GetForm(eMusicGenreForm).ShowDialog(&rPauseForm);
			return true;
		}
		else if(IsSelected("PM_Statistics"))
		{
			MAIN_FORM.GetForm(eStatisticsForm).ShowDialog(&rPauseForm);
			return true;
		}
		else if(IsSelected("PM_CheatCodes"))
		{
//			MAIN_FORM.GetForm(eCheatForm).ShowDialog(&rPauseForm);
			return true;
		}
		else if(IsSelected("PM_Continue"))
		{
			SafelyCloseToNormal();
			return true;
		}
		else if(IsSelected("RO_Results") /*|| IsSelected("PM_Results")*/)
		{
//			mcRaceScriptData*		pRaceData	= mcRace::GetCurrentRace()? mcRace::GetCurrentRace()->GetRaceDataPtr() : NULL;
//			if(pRaceData) // added for the special case of damanging out at the end of the crusie game and the raceptr going NULL. Why is these even on the damage out menu anyways?????
// 			{
// 				MAIN_FORM.GetForm(eRaceResultsForm).ShowDialog(&rPauseForm);
// 			}
// 			else
			{
				fuiCallbacks::sm_PlayFlashSoundFunc(k_szMenuErrorSound);
			}
			return true;
		}
		else if(IsSelected("RO_FullFix"))
		{
			OnDamageOutFix();
			//No need to confirm since it is free now - BUG FIX #21744
			//m_msgBox.Show(&rPauseForm,"Dlg_QuickFix_Desc","Dlg_QuickFix_Title",eMBB_YesNo,datCallback(MFA1(fuiBaseMenu::OnDamageOutFix), (datBase*)this,0,true));
			return true;
		}
		else if(IsSelected("RO_ReturnToHangout") || IsSelected("PM_ReturnToHangout"))
		{
			m_fixMenu.Show(&rPauseForm,"Dialog_Description_ReturnHangout","Dialog_Title_ReturnHangout",/*"Dialog_ReturnHangout_Action"*/"Common_No",false,false,datCallback(MFA1(fuiBaseMenu::OnReturnToHangout), (datBase*)this,NULL,true));
			return true;
		}
		else if(IsSelected("RO_AddFav",false) || IsSelected("PM_AddFav",false))
		{
// 			fuiLogic::GetInstance()->AddCurrentRaceToFavorites();
			InitRaceFavorites();
			
			rPauseForm.RefreshMenuItems();
			rPauseForm.m_pausePanel.GetTabControl().Refresh();

			return true;
		}
		else if(IsSelected("RO_RemoveFav") || IsSelected("PM_RemoveFav"))
		{
			m_favBox.Show(&rPauseForm,"Dialog_Description_RemoveFav","Dialog_Title_RemoveFav",eMBB_YesNo,datCallback(MFA1(fuiBaseMenu::OnRemoveFav), (datBase*)this,0,true));

			return true;
		}
		else if(IsSelected("RO_Replay") || IsSelected("PM_ViewReplay"))
		{
			if(IsRaceOver())
			{
				Replay();
			}
			else
			{
				m_msgBox.Show(&rPauseForm,this->GetReplayDescription(),"Dialog_Title_ViewReplay",eMBB_YesNo,datCallback(MFA1(fuiBaseMenu::OnViewReplay), (datBase*)this,0,true));
			}
			return true;
		}
		else if(IsSelected("RO_ReturnToCruise"))
		{
			this->ConfirmClose();
			return true;
		}
		else if
		(
			IsSelected("PM_Restart") ||
			IsSelected("RO_Restart")
		)
		{

#if !__FINAL
// 			if(PARAM_demo.Get() && PARAM_racename.Get())	// restart is the ONLY end of race option in a 'race only' demo
// 			{
// //				fuiLogic::GetInstance()->FullFix(true);
// 
// 				GoRestartRace();
// 			}
// 			else
#endif
			if(!this->IsFixingAllowed())
			{
				m_msgBox.Show(&rPauseForm,this->GetRestartDescription(),this->GetRestartTitle(),eMBB_YesNo,datCallback(MFA1(fuiBaseMenu::OnRestartRace), (datBase*)this,0,true));
			}
			else
			{
				m_fixMenu.Show(&rPauseForm,this->GetRestartDescription(),this->GetRestartTitle(),this->GetRestartAction(),true,false,datCallback(MFA1(fuiBaseMenu::OnRestartRace), (datBase*)this,0,true));
			}

			return true;
		}
		else if
		(
			!IsNetworked() &&
			(IsSelected("RO_AbortRace") ||
			IsSelected("PM_AbortRace") ||
			IsSelected("RO_ContinueToRaceStart"))
		)
		{
			if(!this->IsFixingAllowed())
			{
				//No quick fix for delivery, cause it's not your car and you failed the mission.
				m_msgBox.Show(&rPauseForm,this->GetQuitDescription(),this->GetQuitTitle(),eMBB_YesNo,datCallback(MFA1(fuiBaseMenu::OnQuitRace), (datBase*)this,0,true));
			}
			else
			{
				m_fixMenu.Show(&rPauseForm,this->GetQuitDescription(),this->GetQuitTitle(),this->GetQuitAction(),false,false,datCallback(MFA1(fuiBaseMenu::OnQuitRace), (datBase*)this,0,true));
			}
			return true;
		}
		else if(IsSelected("PM_Arcade"))
		{
			bool bHandleRequest = true;
// 			if(mcPoliceMgrSingleton::IsInstantiated())
// 			{
// 				int iFine = POLICEMGR.GetFine();
// 				if(iFine>0 && IsPoliceChasingPlayer())
// 				{
// 					char szAmount[16];
// 					sprintf(szAmount,"%d",iFine);
// 					UIStringTable->SetString("PayOffAmount",szAmount);
// 					m_msgBox.Show(&rPauseForm,"Dlg_PayFine_Desc","Dlg_PayFine_Title",eMBB_OKCancel,datCallback(MFA1(fuiBaseMenu::OnPayFineConfirmArcade), (datBase*)this,NULL,true));
// 					bHandleRequest = false;
// 				}
// 			}
			if(bHandleRequest)
			{
				this->HandleArcadeRequest();
			}
			return true;
		}
		else if(IsSelected("PM_CreateRace"))
		{
			if(fuiBaseMenu::IsRacing())
			{
				if(this->IsFixingAllowed())
				{
					m_fixMenu.Show(&rPauseForm,this->GetQuitDescription(),this->GetQuitTitle(),this->GetQuitAction(),false,false,datCallback(MFA1(fuiBaseMenu::OnStartRaceEditor), (datBase*)this,NULL,true));
				}
				else
				{
					m_msgBox.Show(&rPauseForm,this->GetQuitDescription(),this->GetQuitTitle(),eMBB_YesNo,datCallback(MFA1(fuiBaseMenu::OnStartRaceEditor), (datBase*)this,NULL,true));
				}
			}
			else if(IsDamagedOut())
			{
				m_fixMenu.Show(&rPauseForm,"Dialog_Description_DamagedOut","Dialog_Title_DamagedOut",NULL,false,false,datCallback(MFA1(fuiBaseMenu::OnStartRaceEditor), (datBase*)this,NULL,true));
			}
			else
			{
				// if there are saved races, ask the player whether they want to load or create a new race
				//if( mcInGameEditor::GetInstance()->NumberOfValidEditorRaces(true) || mcInGameEditor::GetInstance()->NumberOfValidEditorRaces(false) )
				{
					m_msgBox.Show(&rPauseForm, "RE_WontWorkWithWheel","Race Editor", eMBB_YesNo, "RE_Create_New_Race", "RE_Load_Race", "RE_Exit_Race_Editor", datCallback(MFA1(fuiBaseMenu::OnEnterRaceEditorThroughPause), (datBase*)this,NULL,true));
				}
				// no saved races, so just enter the editor
				/*else
				{
					mcInGameEditor::GetInstance()->SetRELoadonEnter(NULL);
					mcInGameEditor::GetInstance()->SetEditorPaused(false);
					fuiRegistry::GetInstance().SetValue("Script_SuspendAllOpponents",1.0f);
					fuiCallbacks::sm_PlayFlashSoundFunc("UI_MENU_SELECT");

					UIMANAGER->GetUINavigator()->GetStateByIdName("Editor_WaitEnter_Create")->Goto(UIMANAGER->GetUINavigator());
				}*/
			}
			return true;
		}
		else if(IsSelected("PM_GoalAttack"))
		{
			bool bHandleRequest = true;
// 			if(mcPoliceMgrSingleton::IsInstantiated())
// 			{
// 				int iFine = POLICEMGR.GetFine();
// 				if(iFine>0 && IsPoliceChasingPlayer())
// 				{
// 					char szAmount[16];
// 					sprintf(szAmount,"%d",iFine);
// 					UIStringTable->SetString("PayOffAmount",szAmount);
// 					m_msgBox.Show(&rPauseForm,"Dlg_PayFine_Desc","Dlg_PayFine_Title",eMBB_OKCancel,datCallback(MFA1(fuiBaseMenu::OnPayFineConfirmGoalAttack), (datBase*)this,NULL,true));
// 					bHandleRequest = false;
// 				}
// 			}
			if(bHandleRequest)
			{
				this->HandleGoalAttackRequest();
			}
			return true;
		}
		else if(IsSelected("PM_Online"))
		{
			bool bHandleRequest = true;
// 			if(mcPoliceMgrSingleton::IsInstantiated())
// 			{
// 				int iFine = POLICEMGR.GetFine();
// 				if(iFine>0 && IsPoliceChasingPlayer())
// 				{
// 					char szAmount[16];
// 					sprintf(szAmount,"%d",iFine);
// 					UIStringTable->SetString("PayOffAmount",szAmount);
// 					m_msgBox.Show(&rPauseForm,"Dlg_PayFine_Desc","Dlg_PayFine_Title",eMBB_OKCancel,datCallback(MFA1(fuiBaseMenu::OnPayFineConfirmWAN), (datBase*)this,NULL,true));
// 					bHandleRequest = false;
// 				}
// 			}
			if(bHandleRequest)
			{
				this->HandleWANRequest();
			}
			return true;
		}
		else if(IsSelected("PM_Lan"))
		{
			bool bHandleRequest = true;
// 			if(mcPoliceMgrSingleton::IsInstantiated())
// 			{
// 				int iFine = POLICEMGR.GetFine();
// 				if(iFine>0 && IsPoliceChasingPlayer())
// 				{
// 					char szAmount[16];
// 					sprintf(szAmount,"%d",iFine);
// 					UIStringTable->SetString("PayOffAmount",szAmount);
// 					m_msgBox.Show(&rPauseForm,"Dlg_PayFine_Desc","Dlg_PayFine_Title",eMBB_OKCancel,datCallback(MFA1(fuiBaseMenu::OnPayFineConfirmLAN), (datBase*)this,NULL,true));
// 					bHandleRequest = false;
// 				}
// 			}
			if(bHandleRequest)
			{
				this->HandleLANRequest();
			}
			return true;
		}
		else if(IsSelected("ol_Tournament"))
		{
			bool bHandleRequest = true;
// 			if(mcPoliceMgrSingleton::IsInstantiated())
// 			{
// 				int iFine = POLICEMGR.GetFine();
// 				if(iFine>0 && IsPoliceChasingPlayer())
// 				{
// 					char szAmount[16];
// 					sprintf(szAmount,"%d",iFine);
// 					UIStringTable->SetString("PayOffAmount",szAmount);
// 					m_msgBox.Show(&rPauseForm,"Dlg_PayFine_Desc","Dlg_PayFine_Title",eMBB_OKCancel,datCallback(MFA1(fuiBaseMenu::OnPayFineConfirmTournament), (datBase*)this,NULL,true));
// 					bHandleRequest = false;
// 				}
// 			}
			if(bHandleRequest)
			{
				this->HandleTournamentRequest();
			}
			return true;
		}	
		else if(IsSelected("RO_NextRace") || IsSelected("PM_NextRace"))
		{
			m_fixMenu.Show(&rPauseForm,
				IsRaceOver() ? "Dialog_Description_NextRace" : "Dialog_Desc_NextRaceMidRace",
				"Dialog_Title_NextRace",
				"Common_No",//"Dialog_NextRace_Action",
				false,false,datCallback(MFA1(fuiBaseMenu::OnNextRace), (datBase*)this,0,true));

			return true;
		}
		else if(IsSelected("PM_WarpGarage") || IsSelected("RO_Garage"))
		{
			if(this->IsNeedGarageConfirmation())
			{
				m_msgBox.Show(&rPauseForm,this->GetGarageDescription(),"Dialog_Title_GarageConfirm",eMBB_YesNo,datCallback(MFA1(fuiBaseMenu::OnJumpToGarageConfirm), (datBase*)this,0,true));
				return true;
			}
			else
			{
				//no damage to the car so jump straight to the gerage
//				GoGarage();
				return true;
			}
		}
		else
		{
			return false;
		}
	default:
		return false;
	}
}

bool fuiBaseMenu::IsNeedGarageConfirmation() const
{
	if(IsRaceOver())
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool fuiBaseMenu::IsNextRace() const
{
	return IsRaceOver() && IsTelephoneChallenge();
}

bool fuiBaseMenu::IsRacing()
{
	return false; // NULL != mcRace::GetCurrentRace();
}

bool fuiBaseMenu::IsGarageFlyable() const
{
// 	if(mcRace::GetCurrentRace())
// 	{
// 		if(!IsRaceOver())
// 		{
// 			return false; //as per mc4_menus.xls design file, bug 20456.
// 		}
// 	}

	return IsGarageUnlocked();
}

void fuiBaseMenu::Setup(bool bIsRaceOverMode)
{
	//if(bIsRaceOverMode)
	//{
	//	FlashNavigator *pUI = UIMANAGER->GetUINavigator();
	//	UIObject& rReplayState = *pUI->GetStateByIdName("Replay");
	//	if(rReplayState.IsUIActive())
	//	{
	//		pUI->GetStateByIdName("HudPlayerList")->Select();
	//		Assert(pUI->GetStateByIdName("HudPlayerList")->IsUIActive());
	//		Assert(!pUI->GetStateByIdName("Replay")->IsUIActive());
	//	}
	//}

	//
	// Set up the menu-item order:
	//
	UIObject *pState;

#if !__FINAL
// 	if(PARAM_demo.Get() && PARAM_racename.Get())	// restrict pause menu in a 'race only' demo
// 	{
// 		pState = UIMANAGER->GetState("PauseTab");
// 		pState->DetachChildFromParent();
// 		this->SetupOrderPause();
// 	}
#endif

	pState = UIMANAGER->GetState("RaceOverTab");
	pState->DetachChildFromParent();
	this->SetupOrderRaceOver();

	if(bIsRaceOverMode)
	{
		UIMANAGER->GetState("PM_Playlist")->Block();
	}
	else
	{
		UIMANAGER->GetState("PM_Playlist")->Unblock();
	}

	//
	// Enable and disable items:
	//

	UIMANAGER->GetUILogic().refreshHUD(NULL);

	bool bIsPoliceChasingPlayer = IsPoliceChasingPlayer();

	bool bArcadeEnabled = true;

	bool bIsNextRace = this->IsNextRace() && !IsNetworked() && !IsTelephoneChallenge();

	UIMANAGER->GetState("RO_NextRace")->SetEnabled(bIsNextRace);
	UIMANAGER->GetState("PM_NextRace")->SetEnabled(bIsNextRace);

	if(bIsPoliceChasingPlayer)
	{
		UIMANAGER->GetState("RO_NextRace")->Block();
		UIMANAGER->GetState("PM_NextRace")->Block();
	}
	else
	{
		UIMANAGER->GetState("RO_NextRace")->Unblock();
		UIMANAGER->GetState("PM_NextRace")->Unblock();
	}

	//UIMANAGER->GetState("PM_Results")->SetEnabled(false);
	UIMANAGER->GetState("RO_Results")->SetEnabled(this->IsResultsVisible());
	if(bIsPoliceChasingPlayer)
	{
		//UIMANAGER->GetState("PM_Results")->Block();
		UIMANAGER->GetState("RO_Results")->Block();
	}
	else
	{
		//UIMANAGER->GetState("PM_Results")->Unblock();
		UIMANAGER->GetState("RO_Results")->Unblock();
	}

	UIMANAGER->GetState("RO_ContinueToRaceStart")->SetEnabled( false );
	UIMANAGER->GetState("RO_FullFix")->SetEnabled( false );
	UIMANAGER->GetState("RO_Restart")->SetEnabled( false );
#if !__FINAL
// 	if(PARAM_demo.Get() && PARAM_racename.Get())	// restrict pause menu (restart only) in a 'race only' demo
// 	{
// 		UIMANAGER->GetState("PM_Restart")->SetEnabled( true );
// 		UIMANAGER->GetState("RO_Restart")->SetEnabled( true );
// 	}
#endif
	UIMANAGER->GetState("RO_ReturnToCruise")->SetEnabled( true );
	UIMANAGER->GetState("RO_AbortRace")->SetEnabled( false );
	UIMANAGER->GetState("RO_RaceBack")->SetEnabled(false);

	UIMANAGER->GetState("PM_Continue")->SetEnabled(true);
	UIMANAGER->GetState("PM_Arcade")->Unblock();
	UIMANAGER->GetState("PM_GoalAttack")->Unblock();
	//UIMANAGER->GetState("PM_CreateRace")->Unblock();

#if __PPU
	if(UIMANAGER->GetState("PM_Friends")) 
	{
		snSession* pSession= IsNetworked()? MYGAMER.GetMySession(0) : NULL;
		UIMANAGER->GetState("PM_Friends")->SetEnabled(pSession && pSession->IsOnline() && !pSession->IsRanked());
	}
#endif

	//UIMANAGER->GetState("PM_CreateRace")->Block();

	if(IsDamagedOut() && this->IsFixingAllowed() )
	{
		if(!IsRacing())
		{
			UIMANAGER->GetState("RO_FullFix")->SetEnabled( true );
			UIMANAGER->GetState("RO_Replay")->SetEnabled( false );
			UIMANAGER->GetState("PM_ViewReplay")->SetEnabled( false );
		}
		else
		{
			UIMANAGER->GetState("RO_Replay")->SetEnabled( true );
			UIMANAGER->GetState("PM_ViewReplay")->SetEnabled( true );
		}
	}

	if(IsNetworked())
	{
		UIMANAGER->GetState("RO_Replay")->SetEnabled( false );
		UIMANAGER->GetState("PM_ViewReplay")->SetEnabled( false );
		UIMANAGER->GetState("PM_Online")->Block();
		if(FLASHNAVIGATOR->GetStateByIdName("PM_Lan")) FLASHNAVIGATOR->GetStateByIdName("PM_Lan")->Block();
		UIMANAGER->GetState("ol_Tournament")->Block();
		UIMANAGER->GetState("PM_CheatCodes")->Block();
	}
	else
	{
		UIMANAGER->GetState("RO_Replay")->SetEnabled( true );
		UIMANAGER->GetState("PM_ViewReplay")->SetEnabled( true );
		UIMANAGER->GetState("RO_Replay")->Unblock();
		UIMANAGER->GetState("PM_ViewReplay")->Unblock();
//		UIMANAGER->GetState("ol_Tournament")->Unblock();
		UIMANAGER->GetState("PM_Online")->Unblock();
		if(FLASHNAVIGATOR->GetStateByIdName("PM_Lan")) FLASHNAVIGATOR->GetStateByIdName("PM_Lan")->Unblock();
		UIMANAGER->GetState("PM_CheatCodes")->Unblock();

	}
	
	UIMANAGER->GetState("RO_ReturnToHangout")->SetEnabled( false );
	UIMANAGER->GetState("PM_ReturnToHangout")->SetEnabled( false );

	if(!IsNetworked()) //goal attack and arcade are allowed during chases now.  must pay fine though.
	{
		bArcadeEnabled = true;
		UIMANAGER->GetState("PM_GoalAttack")->Unblock();
	}
	else
	{
		bArcadeEnabled = false;
		UIMANAGER->GetState("PM_GoalAttack")->Block();
	}

#if !__FINAL
	UIMANAGER->GetState("PM_ReloadRace")->SetEnabled(fuiBaseMenu::IsRacing());
#endif

	if(this->IsGarageFlyable() && !IsNetworked())
	{
		UIMANAGER->GetState("RO_Garage")->SetEnabled( true );
		UIMANAGER->GetState("PM_WarpGarage")->SetEnabled( true );
	}
	else
	{
		UIMANAGER->GetState("RO_Garage")->SetEnabled( false );
		UIMANAGER->GetState("PM_WarpGarage")->SetEnabled( false );
	}

	if(bIsPoliceChasingPlayer)
	{
		UIMANAGER->GetState("RO_Garage")->Block();
		UIMANAGER->GetState("PM_WarpGarage")->Block();
	}
	else
	{
		UIMANAGER->GetState("RO_Garage")->Unblock();
		UIMANAGER->GetState("PM_WarpGarage")->Unblock();
	}
	
// 	if(UIMANAGER->GetState("EditingRace")->IsListener())
// 	{
// 		bArcadeEnabled = false;
// 	}

	// the CruiseScript will Unblock the race editor so we need to 
	// disable it also in this case where it is not valid in cruise
	//UIMANAGER->GetState("PM_CreateRace")->SetEnabled(!bIsPoliceChasingPlayer);

	//if(bArcadeEnabled)
	//{
	//	UIMANAGER->GetState("PM_Arcade")->Unblock();
	//}
	//else
	//{
	//	UIMANAGER->GetState("PM_Arcade")->Block();
	//}

	if(__FINAL || PARAM_nodevmenus.Get())
	{
		UIMANAGER->GetState("PM_ReloadRace")->SetEnabled(false);
		//UIMANAGER->GetState("PM_CheatCodesDev")->SetEnabled(false);
		UIMANAGER->GetState("PM_Separator2")->SetEnabled(false);
		UIMANAGER->GetState("PM_Save")->SetEnabled(false);
		UIMANAGER->GetState("PM_SpecialAbility")->SetEnabled(false);
		UIMANAGER->GetState("PM_TestMovie")->SetEnabled(false);
		UIMANAGER->GetState("PM_ChooseVehicle")->SetEnabled(false);
		UIMANAGER->GetState("PM_ChooseTOD")->SetEnabled(false);
		UIMANAGER->GetState("PM_Quit")->SetEnabled(false);
	}
	else
	{
		UIMANAGER->GetState("PM_ReloadRace")->SetEnabled(true);
		//UIMANAGER->GetState("PM_CheatCodesDev")->SetEnabled(true);
		UIMANAGER->GetState("PM_Separator2")->SetEnabled(true);
		UIMANAGER->GetState("PM_Save")->SetEnabled(true);
		UIMANAGER->GetState("PM_SpecialAbility")->SetEnabled(true);
		UIMANAGER->GetState("PM_TestMovie")->SetEnabled(true);
//		UIMANAGER->GetState("PM_ChooseVehicle")->SetEnabled(true);
		UIMANAGER->GetState("PM_Quit")->SetEnabled(true);
	}

	UIMANAGER->GetState("PM_AbortRace")->SetEnabled( true );
//	UIMANAGER->GetState("PM_ChangeRide")->SetEnabled(false);

	InitRaceFavorites();

	this->SetupMenuStrings();

	if(this->IsGameModesDisabled())
	{
		UIMANAGER->GetState("PM_Arcade")->Block();
		UIMANAGER->GetState("PM_GoalAttack")->Block();
		UIMANAGER->GetState("PM_Online")->Block();
		if(FLASHNAVIGATOR->GetStateByIdName("PM_Lan")) FLASHNAVIGATOR->GetStateByIdName("PM_Lan")->Block();
		//UIMANAGER->GetState("PM_CreateRace")->Block();
		UIMANAGER->GetState("ol_Tournament")->Block();
		//UIMANAGER->GetState("PM_CreateRace")->SetEnabled(false);	// the CruiseScript will Unblock the race editor so we need to disable it also in this case where it is not valid in cruise
	}

	if(IsRacing() || IsNetworked())
		UIMANAGER->GetState("PM_CheatCodes")->Block();
	else
		UIMANAGER->GetState("PM_CheatCodes")->Unblock();

	//HACK for RDR -DANC
	UIMANAGER->GetState("PM_Arcade")->SetEnabled( false ); //Block();
	UIMANAGER->GetState("PM_GoalAttack")->Block();
	//UIMANAGER->GetState("PM_CreateRace")->Block();

}

void fuiPauseMenu::OnExit()
{
	this->SetFocused(false);
	fuiPauseForm& rPauseForm = (fuiPauseForm&)MAIN_FORM.GetForm(ePauseForm);
	rPauseForm.SetVisible(false);
	rPauseForm.Refresh(); //refresh now to get the invalidate out of the way

	fuiPause::OnExit();

	//m_Matrix.Set(MCRENDERMGR.GetUpdateCamera());
	//SetMatrix(&m_Matrix);	
}

void fuiPauseMenu::Refresh()
{	
	this->OnPaint();
}

void fuiPauseMenu::OnPaint()
{
	fuiPauseForm& rPauseForm = (fuiPauseForm&)MAIN_FORM.GetForm(ePauseForm);
	rPauseForm.SetContext(GetContext());
	rPauseForm.Refresh();
}

bool fuiPauseMenu::HandleEvent(VirtualHSM *pUI,  const UIExtObject::NotifyCode priority, int* p_EventID,rage::s8 stateMachineIndex)
{
// 	if (UILOGIC.IsWarping() && !NAVMAP->IsFocused())
// 	{
// 		//don't process any events if we are warping still
// 		return false;
// 	}

	if(p_Obj==FLASHNAVIGATOR->GetInputHandler()) switch(*p_EventID)
	{
		//This is so it won't flicker and play beep sounds when you're in a blank custom playlist:
	case FlashNavigator::fnInputHandler::fnInputEventLeft:
	case FlashNavigator::fnInputHandler::fnInputEventRight:
	case FlashNavigator::fnInputHandler::fnInputEventLAnalogLeft:
	case FlashNavigator::fnInputHandler::fnInputEventLAnalogRight:
	case FlashNavigator::fnInputHandler::fnInputEventUp:
	case FlashNavigator::fnInputHandler::fnInputEventDown:
	case FlashNavigator::fnInputHandler::fnInputEventLAnalogUp:
	case FlashNavigator::fnInputHandler::fnInputEventLAnalogDown:
		return true;
	}

	return fuiPause::HandleEvent(pUI,p_Obj,priority,p_EventID, stateMachineIndex);
}

bool fuiPauseMenu::OnKeyPress(int key, const rage::Vector3& velocity)
{
// 	if (UILOGIC.IsWarping() && !NAVMAP->IsFocused())
// 	{
// 		//don't process any events if we are warping still
// 		return false;
// 	}

	if(fuiPause::OnKeyPress( key, velocity))
	{
		return true;
	}

	return false;
}

rage::u8 fuiPauseMenu::GetTemplate() // u8 or s32?
{
	fuiPauseForm& rPauseForm = (fuiPauseForm&)MAIN_FORM.GetForm(ePauseForm);
	return rPauseForm.GetTemplate();
}

void fuiPauseMenu::OnStackPush()
{
	fuiPause::OnStackPush();
	fuiPauseForm& rPauseForm = (fuiPauseForm&)MAIN_FORM.GetForm(ePauseForm);
	rPauseForm.ShowDialog(NULL);
}

void fuiPauseMenu::OnStackPop()
{
	fuiPause::OnStackPop();
/*	int localPlayerIdx = mcConfig::GetLocalPlayerIndex();
	if(PLAYERMGR->IsPlayerValid(localPlayerIdx))
	{
		mcPlayer& rPlayer = PLAYERMGR->GetPlayer(localPlayerIdx);
		rPlayer.ApplyVisibleInOwnViewport();

		if(rPlayer.GetCamera()->GetCinematicCam()->GetMode() != mcCinematicType::kIdle)
		{
			rPlayer.GetCamera()->SetCinematicCam(false,mcCinematicType::kNone,0.7f);
			rPlayer.GetInput()->ResetIdleTime();
		}
	}
*/

	Assert(0);
	MAIN_FORM.GetForm(ePauseForm).Close();
}

//
// Set various menu disabled/enabled and visible/invisible, and then always default to the first menu in each tab.
//
void fuiPauseMenu::OnEnter()
{
	fuiPause::OnEnter();

	//show:
	this->SetFocused(true);
	fuiPauseForm& rPauseForm = (fuiPauseForm&)MAIN_FORM.GetForm(ePauseForm);
	rPauseForm.RefreshMenuItems();
	rPauseForm.SetVisible(true);

	//rendering:
/*
	int localPlayerIdx = mcConfig::GetLocalPlayerIndex();
	mcPlayer* pPlayer = NULL;
	if(PLAYERMGR->IsPlayerValid(localPlayerIdx))
	{
		pPlayer = &PLAYERMGR->GetPlayer(localPlayerIdx);
		pPlayer->ApplyVisibleInOwnViewport();
	}
*/

	//refresh:
	Assert(MAIN_FORM.GetTopMost());
	if(MAIN_FORM.GetTopMost())
	{
		MAIN_FORM.GetTopMost()->Refresh();
	}

//	mcAudioManager::ForcePauseState(true);
}

fuiBaseMenu& fuiPausePanel::GetMenuType()
{
// 	if(IsReplay())
// 	{
// 		if(IsMidReplay())
// 		{
// 			return m_replayMenu;
// 		}
// 		if(MAIN_FORM.GetForm(eReplayForm).GetIsLoaded())
// 		{
// 			return m_replayMenu;
// 		}
// 	}

	bool bIsRacing = fuiBaseMenu::IsRacing(); // mcRace::GetCurrentRace()!=NULL;

// 	if (IsNetworked() && MYGAMER.GetDataUns(gamerData_enum_ParticipantStatus)!=gamerParticipantStatus_Active)
// 		bIsRacing=false;

	if(bIsRacing)
	{
#if !__FINAL
// 		if(PARAM_demo.Get() && PARAM_racename.Get())	// restrict pause menu in a 'race only' demo
// 		{
// 			return m_baseBehavior;
// 		}
#endif

//		mc::EUIRaceType iRaceType = UIMANAGER->GetUILogic().GetCurrentUIRaceType();
#if __DEV
		//Displayf("The current race type is: %s",mc::UIRaceTypeNames[(int)iRaceType]);
#endif
// 		switch(iRaceType)
// 		{
// 		case mc::EUIRaceType_DYNAMIC:
// 			return m_redLightMenu;
// 		case mc::EUIRaceType_SERIES:
// 			return m_seriesMenu;
// 		case mc::EUIRaceType_TOURNAMENT:
// 			return m_tournamentMenu;
// 		case mc::EUIRaceType_TIME_TRIAL:
// 			return m_timeTrialMenu;
// 		case mc::EUIRaceType_DELIVERY:
// 			return m_deliveryMenu;
// 		case mc::EUIRaceType_PAYBACK:
// 			return m_paybackMenu;
// 		case mc::EUIRaceType_WAGER:
// 			return m_wagerMenu;
// 		case mc::EUIRaceType_FREEWAY:
// 			return m_freewayMenu;
// 		case mc::EUIRaceType_NORMAL:
// 		case mc::EUIRaceType_TUTORIAL:
// 		case mc::EUIRaceType_MISSION:
// 		case mc::EUIRaceType_TELEPHONE_CHALLENGE:
// 		case mc::EUIRaceType_CTF_BASEWAR:
// 		case mc::EUIRaceType_CTF_SPLITBASE:
// 			return m_localRacerMenu;
// 		case mc::EUIRaceType_BEAT_ME_THERE:
// 		case mc::EUIRaceType_BEAT_ME_THERE_TUTORIAL:
// 			return m_beatMeThereMenu;
// 		case mc::EUIRaceType_PINKSLIP:
// 			return m_pinkslipMenu;
// 		default:
			return m_baseBehavior;
//		}
	}
	else
	{
		return m_cruiseMenu;
	}
}

//
// Pause panel and pause form:
//

fuiPausePanel::fuiPausePanel() :
fuiCtrl(2),
m_mainPage("pause_tab_mainmenu",1),
m_gamePage("pause_tab_gamemodes",1),
m_optionsPage("pause_tab_settings",1),
m_raceOverPage("pause_tab_mainmenu",1),
m_mainList(32,1),
m_gameList(32,1),
m_optionsList(32,1),
m_raceOverList(32,1)
{
	for(int i = 0; i < NELEM(m_mainDataSets); i++)
	{
		m_mainDataSets[i] = rage_new fUIObjectDataSource(32);
	}

	//
	// Have to call these here since tab control gets its context set during load.  probably don't need it for the individual pages.
	//
	fuiCtrls* pControls = &this->GetControls();
	pControls->Push(&m_prompts);
	pControls->Push(&m_tabControl);
	fuiTabPages* pTabPages = m_tabControl.GetTabPages();
	pTabPages->Push(&m_mainPage);
	pTabPages->Push(&m_gamePage);
	pTabPages->Push(&m_optionsPage);

	//Add the list control to each tab page:
	pControls = &m_mainPage.GetControls();
	pControls->Push(&m_mainList);
	pControls = &m_gamePage.GetControls();
	pControls->Push(&m_gameList);
	pControls = &m_optionsPage.GetControls();
	pControls->Push(&m_optionsList);
	pControls = &m_raceOverPage.GetControls();
	pControls->Push(&m_raceOverList);

	m_mainPage.SetIcon(1);
	m_raceOverPage.SetIcon(1);
	m_gamePage.SetIcon(2);
	m_optionsPage.SetIcon(3);

	m_tabControl.SetTabIndex(0);
}

fuiPausePanel::~fuiPausePanel()
{
	for(int i = 0; i < NELEM(m_mainDataSets); i++)
	{
		delete m_mainDataSets[i];
	}
}

void fuiPausePanel::SyncDataSource()
{
	UIObject *pState[NELEM(m_mainDataSets)];
	pState[0] = UIManagerSingleton::GetInstance().GetUINavigator()->GetStateByIdName("PauseTab");
	pState[1] = UIManagerSingleton::GetInstance().GetUINavigator()->GetStateByIdName("GameModesMenu");
	pState[2] = UIManagerSingleton::GetInstance().GetUINavigator()->GetStateByIdName("SettingsMenu");
	pState[3] = UIManagerSingleton::GetInstance().GetUINavigator()->GetStateByIdName("RaceOverTab");

	for(int i = 0; i < NELEM(m_mainDataSets); i++)
	{
		m_mainDataSets[i]->SetState(*pState[i]);
	}

	m_mainList.SetDataSource(*m_mainDataSets[0]);
	m_gameList.SetDataSource(*m_mainDataSets[1]);
	m_optionsList.SetDataSource(*m_mainDataSets[2]);
	m_raceOverList.SetDataSource(*m_mainDataSets[3]);

	for(int i = 0; i < NELEM(m_mainDataSets); i++)
	{
		m_mainDataSets[i]->SetFilter("","");
	}
}

int FindFirstEnabledItem(fuiIListSource<const char*>* ds)
{
	int iHilite = -1;
	if(ds)
	{
		for(int i = 0; i < ds->GetNumRows(); i++)
		{
			if(ds->IsElementEnabled(i,0))
			{
				iHilite = i;
				break;
			}
		}
	}

	return iHilite;
}

void SetToFirstEnabledItem(fuiListView& lv)
{
	lv.SetSelectedIndex(FindFirstEnabledItem(lv.GetDataSource()));
}

void fuiPausePanel::InitializeTabsAndSelections()
{
	//initialize:
	m_tabControl.SetTabIndex(0);

	//
	// Ensures that if there were any menu items removed, the selection won't overshoot:
	//
	m_mainList.SetSelectedIndex(m_mainList.GetSelectedIndex());
	m_gameList.SetSelectedIndex(m_gameList.GetSelectedIndex());
	m_optionsList.SetSelectedIndex(m_optionsList.GetSelectedIndex());
	m_raceOverList.SetSelectedIndex(m_raceOverList.GetSelectedIndex());

	//
	// Make sure no grayed out menu items are highlighted:
	//
	SetToFirstEnabledItem(m_mainList);
	SetToFirstEnabledItem(m_gameList);
	SetToFirstEnabledItem(m_optionsList);
	SetToFirstEnabledItem(m_raceOverList);
}

void fuiPausePanel::Setup(bool raceOverMode, bool damagedOut)
{
	//set full-fix dynamic string:
//	mcGarage::GetInstance()->SetQuickFixPrice();
	UIStringTable->GetString("full_fix"); // UTF8

	//set rewager-amount dynamic string:
	fuiVariant& rVariant = fuiRegistry::GetInstance().GetValue("nWagerAmount");
	int iAmount = 0;
	if(&rVariant)
	{
		iAmount = rVariant.asInt();
	}
	UIStringTable->SetMoney("rewageramount",iAmount, true);

	//Add the tab control to this form:
	fuiCtrls* pControls = &this->GetControls();
	pControls->clear();
	pControls->Push(&m_prompts);

#if !__FINAL
// 	if(PARAM_demo.Get() && (&m_baseBehavior == &this->GetMenuType()) && PARAM_racename.Get())	// restrict pause menu in a 'race only' demo
// 	{
// 		if(raceOverMode)
// 		{
// 			pControls->Push(&m_raceOverPage);
// 		}
// 		else
// 		{
// 			pControls->Push(&m_mainPage);
// 		}
// 	}
// 	else
#endif
// 	if(MAIN_FORM.GetForm(eReplayForm).GetIsLoaded())
// 	{
// 		Assert(IsReplay());
// 		pControls->Push(&m_mainList);
// 	}
// 	else
	{
		pControls->Push(&m_tabControl);

		//add the tab pages to the tab control:
		fuiTabPages* pTabPages = m_tabControl.GetTabPages();
		pTabPages->clear();
		if(raceOverMode)
		{
			pTabPages->Push(&m_raceOverPage);
			pTabPages->Push(&m_gamePage);
			pTabPages->Push(&m_optionsPage);
		}
		else
		{
			pTabPages->Push(&m_mainPage);
			pTabPages->Push(&m_gamePage);
			pTabPages->Push(&m_optionsPage);
		}
	}

	
	bool bSelectOK = UIMANAGER->GetState("Invisible")->BackButtonEnabled();

	if(raceOverMode)
	{
		m_prompts.SetItems(AcceptPrompt,1);
	}
	else if(!bSelectOK)
	{
		m_prompts.SetItems(AcceptBackPrompt,2);
	}
	else
	{
		m_prompts.SetItems(pausePrompt,NELEM(pausePrompt));
	}

	if(damagedOut)
	{
		m_mainPage.SetText("Dialog_Title_DamagedOut");
		m_raceOverPage.SetText("Dialog_Title_DamagedOut");
	}
	else
	{
		m_mainPage.SetText("pause_tab_mainmenu");
		m_raceOverPage.SetText("pause_tab_mainmenu");
	}
}

UIObject * fuiPausePanel::GetSelectedRow()
{
	fuiListView *pList;

	//get the active list:
// 	if(MAIN_FORM.GetForm(eReplayForm).GetIsLoaded())
// 	{
// 		pList = &m_mainList;
// 	}
// 	else
	{
		mcTabPage& rActiveTab = m_tabControl.GetCurrentPage();
		Assert(rActiveTab.GetControls().GetCount() == 1);
		pList = (fuiListView*)rActiveTab.GetControls()[0];
	}

	//get the state machine state that is inside this tab:
	fUIObjectDataSource *pStateSource = (fUIObjectDataSource*)pList->GetDataSource();
	UIObject& rActiveState = pStateSource->GetState();

	if(pList->GetSelectedIndex() == -1)
	{
		return NULL;
	}

	//figure out which is the selection:
	int iActualIndex = pStateSource->GetActualIndex(pList->GetSelectedIndex());

	return rActiveState.GetChild(iActualIndex);
}

bool fuiPausePanel::OnKeyPress		(int key, const rage::Vector3& velocity)
{
	if(this->GetMenuType().OnKeyPress(key,velocity))
	{
		return true;
	}

	switch(key)
	{
	case PLAY_ONLINE:
		{
// 			if(m_prompts.GetItems() == pausePrompt)
// 			{
// 				mcAudioManager::ForcePauseState(false);
// 				return HandleBackButton();
// 			}
			return true;
		}
	case MENU_ACCEPT:
		{
			int iEventID = (int)FlashNavigator::fnInputHandler::fnInputEventAction;

			UIObject *pState = this->GetSelectedRow();

			if(pState)
			{
				PlaySoundIfEnabled(pState);

				pState->HandleEvent(
					UIManagerSingleton::GetInstance().GetUINavigator(),
					UIManagerSingleton::GetInstance().GetUINavigator()->GetInputHandler(),
					UIExtObject::UIEventPriorityNormal,
					&iEventID,
					1
					);
			}
			return true;
		}
	default:
		return fuiCtrl::OnKeyPress(key,velocity);
	}
}

fuiPauseForm::fuiPauseForm() :
fuiForm("PauseMenu",1)
{
	//Add the tab control to this form:
	fuiCtrls* pControls = &this->GetControls();
	pControls->Push(&m_browser);

	pControls = &m_browser.GetControls();
	pControls->Push(&m_pausePanel);

	m_browser.Navigate(&m_pausePanel);
	//m_tabControl.RegisterSelectedIndexChanged(datCallback(MFA(fuiPauseForm::OnTabChanged), (datBase*)this));
}

bool fuiPauseForm::OnKeyPress(int key, const rage::Vector3& velocity)
{	
// 	switch(key)
// 	{
// // 	case PHOTO_CAM_MOVE_LT:
// // 	case PHOTO_CAM_MOVE_RT:
// // 		{
// // 			if(MAIN_FORM.GetTopMost() == this && !FLASHNAVIGATOR->IsTransitioning())
// // 			{
// // 				if (!SAVEGAMEMGR->IsSaving())
// // 				{
// // 					this->SetVisible(false);
// // 					this->Refresh();
// // 		
// // 					int localPlayerIdx = 0; //TODO: support split screen
// // 					const bool bIsPlayerValid = (PLAYERMGR && PLAYERMGR->IsPlayerValid(localPlayerIdx));	
// // 
// // 					if (bIsPlayerValid)
// // 					{
// // 						mcPlayer& rPlayer  = PLAYERMGR->GetPlayer(localPlayerIdx);
// // 						if (!rPlayer.GetIsFlyCamera() && !rPlayer.GetIsMarketingFlyCamera())
// // 						{
// // 							FLASHNAVIGATOR->RequestTransition(FLASHNAVIGATOR->GetStateByIdName("SnapShot"));
// // 							return true;
// // 						}
// // 					}
// // 				}
// // 			}
// // 			return true;
// // 		}
// 	default:
// 		break;
// 	}
	return fuiForm::OnKeyPress(key,velocity);
}

fuiForm::TransitionType fuiPauseForm::DetermineTransition(UIObject::TransitionAction transition)
{
	if(transition == UIObject::eTA_Close && !this->GetParentForm())
	{
		return fuiForm::FadeInOut;
	}
	else
	{
		Assert(m_iCameraIndexToUse >= 0 && "You are probably parenting this form wrong.  It doesn't have a parent.");
		return fuiPerspectiveForm::DetermineTransitionType(*GetContext(), false, m_iCameraIndexToUse);
	}
}

void fuiPauseForm::OnShowDialog()
{
	fuiForm::OnShowDialog();

	m_iCameraIndexToUse = 1;
	FindValidCameraLerp(m_iCameraIndexToUse);
	Assert(m_iCameraIndexToUse >= 0);
}

void fuiPauseForm::Refresh()
{
	this->RefreshMenuItems();

	fuiForm::Refresh();
}

void fuiPauseForm::OnPaint()
{
	fuiForm::OnPaint();

	if(this->IsVisible())
	{
		GetContext()->SetGlobal("transition_type",(int)m_transitionType);
		Assert(m_iCameraIndexToUse >= 0);
		GetContext()->SetGlobal("cur_camera",m_iCameraIndexToUse);
	}
}

void fuiPauseForm::RefreshMenuItems()
{
	//
	// Default these to enabled.  If there is a police chase, it will get overwritten later in this method.
	//
	UIMANAGER->GetState("RO_Restart")->Unblock();
	UIMANAGER->GetState("PM_Restart")->Unblock();

	UIMANAGER->GetState("PM_AbortRace")->Unblock();
	UIMANAGER->GetState("RO_AbortRace")->Unblock();

	UIMANAGER->GetState("PM_ViewReplay")->Unblock();
	UIMANAGER->GetState("RO_Replay")->Unblock();

	fuiBaseMenu& rMenu = m_pausePanel.GetMenuType();
	//initialize:
//	Assert(rMenu.IsRaceOver() ? mcRace::GetCurrentRace() != NULL : true);
	bool bIsRaceOverMode = rMenu.IsRaceOver() || IsDamagedOut();
	m_pausePanel.Setup(bIsRaceOverMode,IsDamagedOut());

	//set up menu behavior:
	rMenu.Setup(bIsRaceOverMode);

	if(!IsReplay())
	{
		//
		// Special considerations when cop chase in progress
		// 
		if(IsPoliceChasingPlayer())
		{
			UIMANAGER->GetState("RO_Restart")->Block();
			UIMANAGER->GetState("PM_Restart")->Block();

			UIMANAGER->GetState("PM_AbortRace")->Block();
			UIMANAGER->GetState("RO_AbortRace")->Block();

			UIMANAGER->GetState("PM_ViewReplay")->Block();
			UIMANAGER->GetState("RO_Replay")->Block();

			// allow player to fix car if cops have lost him
			/*if(POLICEMGR.HasLostTarget(true) && IsDamagedOut())
			{
				UIMANAGER->GetState("RO_ReturnToCruise")->SetEnabled(false);
				UIMANAGER->GetState("PM_Continue")->SetEnabled(false);
				UIMANAGER->GetState("RO_FullFix")->SetEnabled(true);
			}*/

			UIMANAGER->GetState("RO_ReturnToCruise")->SetText("pause_ReturntoChase");
			UIMANAGER->GetState("PM_Continue")->SetText("pause_ReturntoChase");
		}
	}

	m_pausePanel.SyncDataSource();
}

void fuiPauseForm::RefreshEntireMenu()
{
}

//
// First, set up all the mode-specific menu items in the UI.
// Then do any miscellaneous OnOpen for the mode.
// Then Sync data source to UI.
// Then init each tab's first menu selection.
//
void fuiPauseForm::OnOpen()
{
	fuiCallbacks::pause();

#if __ASSERT
// 	mcPlayer& player = PLAYERMGR->GetPlayer(0);
// 	Assert(player.GetCar());
#endif
	
	fuiBaseMenu& rMenu = m_pausePanel.GetMenuType();
	rMenu.OnOpen();

	this->RefreshMenuItems();
	
	m_pausePanel.InitializeTabsAndSelections();
}

void fuiPauseForm::OnClosed()
{
	fuiBaseMenu& rMenu = m_pausePanel.GetMenuType();
	rMenu.OnClosed();

// 	fuiPerspectiveForm& rEorForm = (fuiPerspectiveForm&)MAIN_FORM.GetForm(eEndOfRaceForm);
// 	rEorForm.Close();

	//	Moved to here from mcEndOfRaceForm::OnClosed() since it seemed to be closing prematurely
//	mcAudioManager::SetActiveEndRace(false);
}

void fuiPauseForm::Update(float dt)
{
	m_pausePanel.GetMenuType().Update(dt);
}
