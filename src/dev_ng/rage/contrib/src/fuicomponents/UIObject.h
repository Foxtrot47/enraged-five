// 
// flashUIComponents/UIObject.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef UI_OBJECT_H
#define UI_OBJECT_H

#include "data/base.h"
#include "data/callback.h"
#include "atl/map.h"
#include "fuicore/Common.h"
#include "xmldata/xmltoken.h"
#include "file/stream.h"
#include "script/hash.h"

#define MAX_XML_ATTRIBUTES 32
#define MAX_XML_ATTR_LEN 256

namespace rage 
{
	class UIContext;
	class UITransition;
	class UIEventHandler;
	struct UITransitionPath;
	class xmlAsciiTokenizerXml;
	class UICommand;
	struct UIReadMode;
	class fuiEvent;
	class atString;
	class Matrix34;
	class bkBank;
	class swfCONTEXT;
	class xmlTokenizerHack;
	struct UIInfo;
	class UIVariant;
	class UIObject;
	class UICallback;

typedef void (*uiCmd)(UIInfo&);
typedef atMap<const char*,UICallback*, atMapCaseInsensitiveHashFn, atMapCaseInsensitiveEquals> FunctionMap;
class UICallback
{
public:
	UICallback(uiCmd c, UIObject* i) : fn(c),thisPtr(i){}
	uiCmd fn;
	UIObject* thisPtr;
};

class xmlTokenizerHack : public xmlAsciiTokenizerXml
{
	int		pushcount;
	char	pushbuf[128];

protected:

	int	GetTokenCh2()
	{
		int result;

		if (pushcount)
			result = pushbuf[--pushcount];
		else
			result = S->FastGetCh();
		if (result == 10)
			++line;
		return result;	
	}

	inline int iswhitespace(int ch)
	{
		return ch == 32 || ch == 9 || ch == 10 || ch == 13 || ch == 0;
	}

public:
	xmlTokenizerHack() : pushcount(0) 
	{
	}

	int GetTokenToCharFixed(char *dest, int maxLen, char terminator, bool stripWhiteSpace=true, bool ignoreCommentChar=false)
	{
		bool start = true;
		int stored = 0, i;
		nextch = 0;
		while (stored < (maxLen-1))
		{
			nextch = GetTokenCh2();
			if ((nextch==CommentChar) && !ignoreCommentChar)
				SkipComment();
			if (nextch == -1 && stored == 0) stored = -1;
			if (nextch == -1 || nextch == terminator)	break;
			if (!stripWhiteSpace || !start || !iswhitespace(nextch))
			{
				start = false;
				dest[stored++] = (char) nextch;
			}
		}

		// Trim trailing whitespace
		for (i = stored - 1; i >= 0; i--)
			if (iswhitespace(dest[i]))
				stored--;
			else
				break;

		if (stored>=0)
			dest[stored] = 0;
		return stored;
	}
};

//common base class for all UI objects
class UIObject : public datBase
{
public:

	enum UIObjectState
	{
		UI_ENABLED			= 1,//can be focused
		UI_INTERRUPTIBLE	= 2,//can be unfocused
		UI_ACTIVE			= 4,//can receive events,updates
		UI_FOCUSED			= 8,//can receive inputs
		UI_VISIBLE			= 16,//can receive paints, draws
		UI_ALL				= 255
	};

	enum RefreshFrequency{ONFOCUSED=1,SECONDS=2,ONEVENT=4,ONTRANSITION=8};


	//construction/destruction
	UIObject();
	virtual ~UIObject();
	virtual void Init();
	void Shutdown();

	//Visibility dictates if a state is shown or hidden.  
	//Ideally the textures/meshes associated with the state should not be rendered at all.  
	//A visible state will have it's Draw/Render function called.  
	//A non-visible state will not have it's Draw/Render function called
	virtual bool IsVisible() const;
	virtual void SetVisible(bool b);
	virtual void ToggleVisibility();		
	virtual void Show();
	virtual void Hide();
	virtual void OnShow();
	virtual void OnHide();

	//Enabled / Disabled describes how input, events, and transitions are processed on a state.  
	//Input and events are processed if this flag is true, otherwise input and events are not processed.  
	//A disabled state should never be transitioned to. (At least in the normal sort of way).  
	//Visual representation of a state may change based off this state, but is not required.
	//An Enabled state will have it's handleEvents functions called.  
	//A disabled state will not have it's handleEvents functions called.
	virtual bool IsEnabled()const;
	virtual void SetEnabled(bool b);
	virtual void Disable();
	virtual void Enable();
	virtual void ToggleEnabled();
	virtual void OnEnabled(){}
	virtual void OnDisabled(){}

	//Focused describes if a component is selected or not.  
	//All states for a focused component that are on a path from that focused component to its root component should also be focused.  
	//Although all component for a non-focused component that are on a path from that non-focused component to its root state may 
	//or may not be non-focused.  The focused component should be the first component to receive and decide to consume input and events.  
	//If it does not consume input and events, then it should pass that responsibility to its parent and on up to its root component.  
	//If nobody consumes the event, then non-focused component may then choose to process and consume events and input.
	//A focused component will have it's Update function called.  An unfocused state will not have it's Update function called
	//The only way for a component to receive input is if it is focused
	virtual bool IsFocused() const;
	virtual void SetFocused(bool b);
	virtual void OnFocused();
	virtual void OnUnfocused();
	virtual void Focus();
	virtual void Unfocus();

	//Active describes if a component is running. All component for an active component that are on a path 
	//from that active component to its root parent component should also be active. 
	//Siblings and children of this active component may or may not be active. 
	//The active component should be the first component to receive and decide to consume non input events. 
	//If it does not consume events, then it should pass that responsibility to its parent and on up to 
	//its root component. If nobody consumes the event, then non-focused components may then choose to 
	//process and consume events. 
	virtual void ToggleActivate()					;
	virtual bool IsActive()const					;
	virtual void SetActive(bool)					;
	virtual void Activate()							;
	virtual void Deactivate()						;
	virtual void OnActivate()						;
	virtual void OnDeactivate()						;

	//TODO document these
	virtual void Exit()								;
	virtual void Enter()							;
	virtual void OnExit()							;
	virtual void OnEnter()							{ ProcessEventCommands("onEnter"); }

	//TODO document these
	virtual void OnReturn()							{ }
	virtual void OnStackPush()  					{ ProcessEventCommands("OnStackPush"); }
	virtual void OnStackPop()						{ ProcessEventCommands("OnStackPop"); }

	//relationship funcs
	virtual void AddChild(UIObject* pChild)			;
	virtual UIObject* GetPreviousSibling()			;
	virtual UIObject* GetFirstSibling()				;
	virtual const UIObject* GetFirstSibling() const	;
	virtual void SetupChildRelationships(UIObject*)	;
	virtual UIObject* GetParent()					{ return m_pParent;		}
	virtual void SetParent(UIObject* pParent)		{ m_pParent = pParent;	}
	virtual bool HasChildren()						{ return m_pFirstChild != NULL; }
	virtual void Reset()							;
	virtual UIObject* GetLastSibling()				;
	virtual UIObject* GetChild(int index)	const	;
	virtual UIObject* GetChild(const char* )const	;
	virtual UIObject* GetChildWithText(const char*)	;
	virtual u32 GetChildIndex(const char* ) const	;
	virtual UIObject* GetFocusedChild()				;
	virtual UIObject* GetNextSibling()				;
	virtual void RemoveChild(u32 index)				;
	virtual void RemoveChild(UIObject* pBadChild)	;
	virtual void RemoveAllChildren()				;
	virtual void SetNextSibling (UIObject* n)		{ m_pNextSibling = n; }
	virtual u32	GetNumberOfChildren()		const	;
	virtual u32	GetNumberOfVisibleChildren()const	;
	virtual void SetFirstChild(UIObject* pFirst)	{ m_pFirstChild = pFirst; }
	virtual UIObject* GetFirstChild()				{ return m_pFirstChild;}
	virtual void DetachFromParent()					;

	//life cycle funcs
	virtual void OnPaint()							{ ProcessEventCommands("OnPaint"); }
	virtual void Update()							;
	virtual void Update(f32 dt)						;
	virtual void Draw(float,u32)					;
	virtual void PostStream(char movieID)			;
	virtual bool ProcessDataTag(const char* ,const char* )		;
	virtual bool ProcessAttributeTags(xmlTokenizerHack& tokenizer, const char* tagName)				;
	virtual void PostLoad()							; //called after all states are read in from xml
	virtual bool OnSetCamera(Matrix34& camera, float& fovy, float& near, float& far);

	//Dictates if a state can be transitioned via external and independent events.  
	//If a state is not interruptible, then all events that it receives (that are not specified in its local event list) 
	//will be queued and processed after this state is transitioned out of.  
	//For example, we may not want a tutorial movie to be interrupted by a popup that appears because a controller is unplugged.  
	//If the tutorial states set their interruptible flag to false, then the popup will display after the movie ends.  
	//This flag doesn't come into play much with the xbox, but may be significant with the ps3.
	virtual bool IsInterruptible() const;
	virtual void SetInterruptible(bool b);

	//transitioning
	// PURPOSE:	Transitions are UITransitions that start when this component is Activated and Deactivated
	virtual bool IsTransitioningIn(){return false;}
	virtual bool IsTransitioningOut(){return false;}
	virtual bool IsChildTransitioningIn();
	virtual bool IsChildTransitioningOut();
	virtual bool IsChildTransitioning();	
	virtual bool GetPath(UIObject* pTarget, UITransitionPath& path);
	virtual void SetInTransition(UITransition* ){}
	virtual void SetOutTransition(UITransition* ){}
	virtual UITransition* GetInTransition(){return NULL;}
	virtual UITransition* GetOutTransition(){return NULL;}
	virtual void OnTransitionComplete();
	virtual void TransitionTo();
	virtual void TransitionFrom();
	virtual void RequestTransitionToThis();
	// PURPOSE:	Tells if this object is in "transition" mode.  An object can be in "transition" mode if
	//				it's m_pInTransition or m_pOutTransition is focused
	// RETURNS:	true if transitioning false otherwise
	virtual bool IsTransitioning(){return false;}
	virtual void Goto();
	virtual void Goto(const char* id);
	virtual void GotoCB(){Goto();}

	//events
	virtual void ProcessEventCommands(const char* );
	virtual void SendEvent(const char* ){}
	virtual bool SendEvent(fuiEvent*, bool isFromQueue = false);
	virtual bool SendEvent(const char* pEvent , void* pData = NULL, rage::u8 priority = 10, bool sendToCode = true,  bool sendToScripts = true, bool isFromQueue = false);
	virtual bool HandleEvents(fuiEvent*);
	virtual bool HandleInputs(fuiEvent*);
	virtual UIEventHandler* GetFirstEventHandler(){return m_pFirstEventHandler;}
	virtual void SetFirstEventHandler(UIEventHandler* p){m_pFirstEventHandler = p;}
	virtual void HandleCommand(const char* commandName, const UICommand *pCmd, UIObject *pState);
	UIEventHandler* GetEventHandler(fuiEvent* pEvent);
	void AddEventHandler(const char* id, void* callback);	
	void ExecuteCommand(const char* commandName, int numParameters, UIVariant *pParameters);
	FunctionMap* GetFunctionMap(){return &m_FunctionMap;}
	static void RegisterCommand(UIObject* pObject,const char *cmdName,uiCmd handler, UIObject* thisPtr);

	virtual void Refresh(){m_bRefresh = true;}
	virtual const rage::Matrix34* GetMatrix(){return NULL;}
	virtual void SetMatrix(const rage::Matrix34*){}
	virtual const char* GetName() const{ return m_szName; }
	virtual void SetName(const char* pName);
	virtual const char* GetText() const { return NULL; }
	virtual void Clear();
	//virtual void SetCallback(const char* command, datCallback callback);

	//Visual abstraction
	virtual UIContext* GetContext(){return NULL;}
	virtual void SetText(const char*) { AssertMsg(false , "This object does not have text to set"); };

	//static calls
	static void RegisterScriptCommands();
	static UIObject* sm_GetChild(UIObject *s, s32 nChildIndex)	;
	static void	sm_Enable(UIObject *s);
	static void	sm_Disable(UIObject *s);
	static void	sm_Activate(UIObject *s);
	static bool	sm_IsActive(UIObject *s);
	static void SetupXmlArrays();//1 time init for all uiobjects

#if __DEV
	virtual void DrawDebug();
#endif

#if __BANK
	virtual void AddWidgets( bkBank & bk );
	void AddMatrixWidgets( bkBank & bk , Matrix34* pMatrix);
#endif

	//EXPOSEFUNCTIONS //exposes functions to xml

protected:
	void AddProperties(int f){ m_Properties |= f;}
	void RemoveProperties(int f){ m_Properties &= ~f;}
	
	const char*		m_szName;
	UIEventHandler*	m_pFirstEventHandler;
	UIObject*		m_pFirstChild;
	UIObject*		m_pParent;
	UIObject*		m_pNextSibling;
	bool			m_bRefresh;
	int				m_Properties;
	FunctionMap		m_FunctionMap;

#if __BANK
	bool m_bIsActive;
	bool m_bIsFocused;
	bool m_bIsVisible;
	bool m_bIsInterruptible;
	bool m_bIsEnabled;
#endif
};

} // namespace rage

#endif //UI_OBJECT_H