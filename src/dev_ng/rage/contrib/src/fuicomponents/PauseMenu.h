// 
// flashUIComponents/PauseMenu.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef UIPAUSEMENU_H
#define UIPAUSEMENU_H

#include "vector/quaternion.h"

//#include "flashUIComponents/Components.h"
#include "flashUIComponents/Controls.h"
#include "flashUIComponents/CommonForms.h"

bool IsMidReplay();
bool IsPoliceChasingPlayer();
void SafelyPopState(const char *stateToPop);

namespace rage {

class swfCONTEXT;

class fuiFixForm : public fuiSimpleListForm
{
public:
	fuiFixForm();

	bool IsControlCamera();
	
	void Show(UIObject* parent, const char *text, const char *caption, const char *actionText, bool commitment, bool showQuickFix, datCallback callback = NullCallback);
protected:
	void Close()
	{
		fuiSimpleListForm::Close();
		m_callback.Call((CallbackData)this);
	}
	bool OnKeyPress(int key, const Vector3& velocity);
	void OnPaint();
private:
	fuiListViewItem	m_actionItem;
	fuiListViewItem	m_quickFixItem;
	//fuiListViewItem	m_fullFixItem;

	datCallback	m_callback;

	bool			m_commitment; //true during restart or next race, restoring damage, or saving new quickfix
};

class fuiPause : public UIMenu
{
public:
	fuiPause() : UIMenu(), m_promptStrip(AcceptPrompt,1){}
	virtual bool OnKeyPress(int key, const Vector3& velocity);
	virtual void OnPaint();
	virtual swfCONTEXT* GetContext() const;
protected:
	fuiMessageBox	m_msgBox;
	fuiFixForm		m_fixMenu;
private:
	fuiPromptStrip m_promptStrip;
};

// class fuiReplayPause : public fuiPause
// {
// public:
// 	void OnEnter();
// 	void OnExit();
// 	bool OnKeyPress(int key, const Vector3& velocity);
// 	void Update(float dt);   
// 
// protected:
// 	float m_IdleTime;
// 	float m_Timeout;
// };

class fuiBaseMenu : public datBase
{
public:
	//
	// Decide what menus should be shown:
	//
	virtual void Setup(bool bIsRaceOver);

	//
	// Decide the menu text in a separate pass:
	//
	virtual void SetupMenuStrings();

	//
	// Decide the order of the menus in an additional pass (eventually, it would be nice to merge with Setup)
	//
	virtual void SetupOrderPause();
	virtual void SetupOrderRaceOver();
	
	virtual bool OnKeyPress(int key, const Vector3& velocity);

	static bool IsRacing();

	virtual bool IsGarageFlyable() const;
	virtual bool IsNeedGarageConfirmation() const;
	virtual bool IsNextRace() const;
	virtual bool IsResultsVisible() const;
	virtual bool IsReturnToCruiseWillAbort() const { return false; }
	virtual bool IsGameModesDisabled() const;
	virtual bool IsRaceOver() const;
	virtual bool IsFixingAllowed() const;

	//Message box handlers:
	void OnDamageOutFix();
	void OnReturnToHangout(fuiForm *form);
	void OnRemoveFav(fuiForm* form);
	void OnRestartRace(fuiForm* form);
	void OnQuitRace(fuiForm* form);
	void OnNextRace(fuiForm* form);
	void OnJumpToGarageConfirm(fuiForm *form);
	void OnViewReplay(fuiForm *form);

	void OnPayFineConfirmArcade(fuiForm *form);
	void OnPayFineConfirmGoalAttack(fuiForm *form);
	void OnPayFineConfirmWAN(fuiForm *form);
	void OnPayFineConfirmLAN(fuiForm *form);
	void OnPayFineConfirmTournament(fuiForm *form);

	void HandleArcadeRequest();
	void HandleGoalAttackRequest();
	void HandleWANRequest();
	void HandleLANRequest();
	void HandleTournamentRequest();

	//
	// Use this in Delivery and Time Trial when you don't own the vehicle.
	//
	void OnGoRestartRaceWithFullFix( fuiForm* form );
	
	void OnReturnToCruise(fuiForm* form);

	virtual void Update(float UNUSED_PARAM(dt)){}

	virtual void OnOpen() {}
	virtual void OnClosed();
protected:
	virtual const char *GetContinueDescription() const { return "Dialog_Description_ReturnToCruise"; }
	virtual const char *GetContinueTitle() const { return "Dialog_Title_ReturnToCruise"; }

	virtual const char *GetGarageDescription() const { return "Dialog_Description_GarageConfirm"; }

	virtual const char *GetReplayDescription() const { return "Dialog_Description_ViewReplay"; }

	virtual const char *GetRestartDescription() const { return "Dlg_Restart_Desc"; }
	virtual const char *GetRestartTitle() const { return "Dlg_Restart_Title"; }
	virtual const char *GetRestartAction() const { return "Common_No";}//{ return "Dlg_Restart_Action"; }

	virtual const char *GetQuitDescription() const { return "Dialog_Description_QuitRace"; }
	virtual const char *GetQuitTitle() const { return "Dialog_Title_QuitRace"; }
	virtual const char *GetQuitAction() const { return "Common_No";}//{ return "Dialog_QuitRace_Action"; }

	void ConfirmClose();
	void OnStartArcade(fuiForm* form);
	void OnStartRaceEditor(fuiForm* form);
	void OnEnterRaceEditorThroughPause( fuiForm* form);
	void OnStartGoalAttack(fuiForm* form);
	void OnStartOnline(fuiForm *form);
	void OnStartLAN(fuiForm *form);
	void OnStartTournament(fuiForm *form);
protected:
	fuiMessageBox			m_msgBox;
	fuiFavoritesMessageBox	m_favBox;
	fuiFixForm				m_fixMenu;
};

// class fuiRedLightMenu : public fuiBaseMenu
// {
// public:
// 	void Setup(bool bIsRaceOver);
// 	bool OnKeyPress(int key, const Vector3& velocity);
// 	bool IsGarageFlyable();
// protected:
// 	void OnRaceBack(fuiForm *form);
// };

// class fuiSeriesMenu : public fuiBaseMenu
// {
// public:
// 	void Setup(bool bIsRaceOver);
// 	virtual void SetupMenuStrings();
// 
// 	virtual bool IsReturnToCruiseWillAbort();
// 	virtual bool IsNeedGarageConfirmation();
// 
// 	virtual bool IsGarageFlyable();
// 	bool IsNextRace();
// 	bool OnKeyPress(int key, const Vector3& velocity);
// protected:
// 	virtual const char *GetReplayDescription() const { return "Dialog_Desc_ViewReplaySeries"; }
// 
// 	virtual const char *GetRestartDescription() const { return "Dlg_Desc_Series_Restart"; }
// 	virtual const char *GetRestartTitle() const { return "Dlg_Title_Series_Restart"; }
// 
// 	virtual const char *GetQuitDescription() const { return "Dlg_Desc_Series_Quit"; }
// 	virtual const char *GetQuitTitle() const { return "Dlg_Title_Series_Quit"; }
// 	virtual const char *GetQuitAction() const { return "Dialog_QuitSeries_Action"; }
// };

//
// Tournament is very similar to Series, so we'll just derive off of series:
//
// class fuiTournamentMenu : public fuiSeriesMenu
// {
// public:
// 	void Setup(bool bIsRaceOver);
// 	void SetupMenuStrings();
// 	void SetupOrderPause();
// 	bool IsGarageFlyable();
// 	void SetupOrderRaceOver();
// 	bool OnKeyPress(int key, const Vector3& velocity);
// protected:
// 	const char *GetReplayDescription() const { return "Dialog_Desc_ViewReplayTourn"; }
// 
// 	const char *GetRestartDescription() const { return "Dlg_Desc_Tourn_Restart"; }
// 	const char *GetRestartTitle() const { return "Dlg_Title_Tourn_Restart"; }
// 
// 	const char *GetQuitDescription() const { return "Dialog_Description_QuitTournament"; }
// 	const char *GetQuitTitle() const { return "Dialog_Title_QuitTournament"; }
// 	const char *GetQuitAction() const { return "Dialog_QuitTournament_Action"; }
// };

// class fuiCopChaseMenu : public fuiBaseMenu
// {
// public:
// 	void Setup(bool bIsRaceOver);
// 	bool IsNextRace() { return false; }
// };
// 
// class fuiArrestedMenu : public fuiBaseMenu
// {
// public:
// 	void Setup(bool bIsRaceOver);
// 	bool IsNextRace() { return false; }
// };
// 
// class fuiTimeTrialMenu : public fuiBaseMenu
// {
// public:
// 	void Setup(bool bIsRaceOver);
// 	void SetupMenuStrings();
// 	bool OnKeyPress(int key, const Vector3& velocity);
// protected:
// 	bool IsFixingAllowed() const { return false; }
// 	bool IsResultsVisible() { return true; }
// 
// 	const char *GetRestartDescription() const { return "Dlg_Desc_TimeTrial_Restart"; }
// 	const char *GetRestartTitle() const { return "Dlg_Title_TimeTrial_Restart"; }
// 
// 	const char *GetQuitDescription() const { return "Dlg_Desc_TimeTrial_Quit"; }
// 	const char *GetQuitTitle() const { return "Dlg_Title_TimeTrial_Quit"; }
// };
// 
// class fuiDeliveryMenu : public fuiBaseMenu
// {
// public:
// 	void Setup(bool bIsRaceover);
// 	void SetupMenuStrings();
// 	bool IsGarageFlyable();
// 	bool IsNextRace();
// 	bool OnKeyPress(int key, const Vector3& velocity);
// protected:
// 	bool IsFixingAllowed() const { return false; }
// 	bool IsResultsVisible() { return false; }
// 
// 	const char *GetRestartDescription() const { return "Dialog_Description_DeliveryRestart"; }
// 	const char *GetRestartTitle() const { return "Dialog_Title_DeliveryRestart"; }
// 
// 	const char *GetQuitDescription() const { return "Dialog_Description_DeliveryQuit"; }
// 	const char *GetQuitTitle() const { return "Dialog_Title_DeliveryQuit"; }
// };

// class fuiPaybackMenu : public fuiBaseMenu
// {
// public:
// 	void Setup(bool bIsRaceOver);
// 	void SetupMenuStrings();
// 	bool OnKeyPress(int key, const Vector3& velocity);
// protected:
// 	bool IsFixingAllowed() const { return false; }
// 	void OnRestartDontFix(fuiForm* form);
// 
// 	const char *GetReplayDescription() const { return "Dialog_Desc_ViewReplayPayback"; }
// 
// 	const char *GetRestartDescription() const { return "Dialog_Description_PaybackRestart"; }
// 	const char *GetRestartTitle() const { return "Dialog_Title_PaybackRestart"; }
// 	const char *GetRestartAction() const { return "Dialog_PaybackRestart_Action"; }
// 
// 	const char *GetQuitDescription() const { return "Dialog_Description_PaybackQuit"; }
// 	const char *GetQuitTitle() const { return "Dialog_Title_PaybackQuit"; }
// 	const char *GetQuitAction() const { return "Dialog_PaybackQuit_Action"; }
// };

// class fuiWagerMenu : public fuiBaseMenu
// {
// public:
// 	void SetupMenuStrings();
// 	void Setup(bool bIsRaceOver);
// 	bool IsGarageFlyable();
// protected:
// 	const char *GetReplayDescription() const;
// 
// 	const char *GetRestartDescription() const { return "PM_restart_wager_desc"; }
// 	const char *GetRestartTitle() const { return "PM_restart_wager_title"; }
// 	const char *GetRestartAction() const { return "Dialog_WagerRestart_Action"; }
// 
// 	const char *GetQuitDescription() const { return "Dialog_Description_QuitWager"; }
// 	const char *GetQuitTitle() const { return "Dialog_Title_QuitWager"; }
// 	const char *GetQuitAction() const { return "Dialog_QuitWager_Action"; }
// };
// 
// class fuiPinkslipMenu : public fuiBaseMenu
// {
// public:
// 	void Setup(bool bIsRaceOver);
// 	void SetupOrderRaceOver();
// 
// protected:
// 	bool IsResultsVisible() { return false; }
// 
// 	const char *GetGarageDescription() const;
// 
// 	const char *GetReplayDescription() const;
// 
// 	const char *GetQuitDescription() const { return "Dlg_Desc_Pinkslip_Quit"; }
// 	const char *GetQuitTitle() const { return "Dlg_Title_Pinkslip_Quit"; }
// 	const char *GetQuitAction() const { return "Dlg_Action_Pinkslip_Quit"; }
// };
// 
// class fuiFreewayMenu : public fuiBaseMenu
// {
// public:
// 	void Setup(bool bIsRaceOver);
// };
// 
// class fuiLocalRacerMenu : public fuiBaseMenu
// {
// public:
// 	void SetupMenuStrings();
// 	void Setup(bool bIsRaceOver);
// 	bool IsGarageFlyable();
// 	bool IsNextRace();
// };
// 
// class fuiBeatMeThereMenu : public fuiBaseMenu
// {
// public:
// 	void SetupMenuStrings();
// 	void Setup(bool bIsRaceOver);
// 	bool IsGarageFlyable();
// 	
// 	void OnContinueToRaceStart(fuiForm* form);
// protected:
// 	bool OnKeyPress		(int key, const Vector3& velocity);
// };

class fuiCruiseMenu : public fuiBaseMenu
{
public:
	void Setup(bool bIsRaceOver);
	bool IsResultsVisible() const;
	bool IsNeedGarageConfirmation() const;
};

class mcReplayMenu : public fuiBaseMenu
{
public:
	void Setup(bool bIsRaceOver);
	void SetupOrderPause();
	void SetupMenuStrings();

	void OnOpen();
	void ResumeReplay();

	void Update(float dt);

	bool IsRaceOver() const;
	bool IsGarageFlyable() const { return false; }
	bool IsNextRace() const { return false; }
	bool IsResultsVisible() const { return false; }
	bool IsGameModesDisabled() const { return true; }
protected:
	bool OnKeyPress		(int key, const Vector3& velocity);
private:
	void ExitReplay();
protected:
	float m_IdleTime;
	float m_Timeout;
};

class fuiIgnoreState : public UIObject
{
public:
	void OnPaint() {}
	void Refresh() {}
};

class fuiPauseList : public fuiListView // MIGRAX <const char16*>
{
public:
	fuiPauseList(int rows, int columns) : fuiListView(rows,columns) {}

	virtual const char* GetTextAt(int row, int col) const { return GetDataSource()->GetElementAt(row,col); }
};

class fuiPauseForm;

class fuiPausePanel : public fuiCtrl
{
public:
	fuiPausePanel();
	virtual ~fuiPausePanel();

	void Setup(bool raceOverMode, bool damagedOut);

	UIObject* GetSelectedRow();
	bool OnKeyPress		(int key, const Vector3& velocity);

	fuiBaseMenu& GetMenuType();

	fuiTabControl& GetTabControl() { return m_tabControl; }
private:
	friend class fuiPauseForm;
	//
	// Sync data source with UI state.
	//
	void SyncDataSource();

	//
	// This should be called if you need to reset the selections to the first tab, and the first
	// menu items in each tab.  This is intended for when you open the pause menu for the first time,
	// not when you are returning to it from spin cam or other sub menus.
	//
	void InitializeTabsAndSelections();
private:
	fuiTabControl				m_tabControl;
	mcTabPage					m_mainPage, m_gamePage, m_optionsPage, m_raceOverPage;
	fuiPauseList					m_mainList, m_gameList, m_optionsList, m_raceOverList;
	fUIObjectDataSource			*m_mainDataSets[4];
	fuiPromptStrip				m_prompts;
private:
// 	fuiRedLightMenu		m_redLightMenu;
// 	fuiSeriesMenu		m_seriesMenu;
// 	fuiTournamentMenu	m_tournamentMenu;
// 	fuiTimeTrialMenu	m_timeTrialMenu;
// 	fuiDeliveryMenu		m_deliveryMenu;
// 	fuiPaybackMenu		m_paybackMenu;
// 	fuiWagerMenu		m_wagerMenu;
// 	fuiPinkslipMenu		m_pinkslipMenu;
// 	mcFreewayMenu		m_freewayMenu;
// 	mcLocalRacerMenu	m_localRacerMenu;
// 	mcBeatMeThereMenu	m_beatMeThereMenu;
	fuiCruiseMenu		m_cruiseMenu;
	fuiBaseMenu			m_baseBehavior;

	fuiMessageBox	m_msgBox;
	fuiFixForm		m_fixMenu;
};

class fuiPauseForm : public fuiForm
{
	friend class fuiPauseMenu;

public:
	fuiPauseForm();

	//
	// Call this when the entire menu, including tabs, changed.  E.g. when you go from
	// replay pause menu to race over menu.
	//
	void RefreshEntireMenu();

	//
	// Call this when race mode changed while the pause form was open.
	//
	void RefreshMenuItems();

	void Refresh();

	int GetCameraIndexToUse		() const		{ return m_iCameraIndexToUse; }

	fuiPausePanel& GetPanel() { return m_pausePanel; }

public: //temporarily public
	void OnPaint();

	bool OnKeyPress(int key, const Vector3& velocity);
protected:
	bool IsControlCamera() { return true; }

	fuiForm::TransitionType DetermineTransition(UIObject::TransitionAction transition);

	void OnOpen();
	void OnClosed();
	void OnShowDialog();

	void Update(float dt);
	
public: //temporary
	fuiBrowser					m_browser;
	fuiPausePanel				m_pausePanel;

	int							m_iCameraIndexToUse; //if main camera index is obstructed
};

class fuiPauseMenu : public fuiPause
{
public:
	void Refresh();

	bool HandleEvent(  const UIExtObject::NotifyCode , int* p_EventID,s8 stateMachineIndex);

protected:
	friend class fuiBaseMenu;
	friend class fuiLogic;

	void OnEnter();
	void OnExit();
	bool OnKeyPress(int key, const Vector3& velocity);
	void OnPaint();
	void OnStackPush();
	void OnStackPop();

	u8 GetTemplate();

protected:
	int m_CarDrawBits;
};

} // namespace rage


#endif
