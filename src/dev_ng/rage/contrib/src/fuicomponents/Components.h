// 
// flashUIComponents/Components.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
// Many of these components are modeled after Javas Swing.  Much of the documentation is provided
// by the Javax/swing api documentation

#ifndef FLASHUICOMPONENTS_COMPONENTS_H
#define FLASHUICOMPONENTS_COMPONENTS_H

#include "atl/dlist.h"
#include "atl/array.h"
#include "fuicore/UIStringTable.h" //can't pre-declare classes since we are using a template
#include "vector/matrix34.h"
#include "flash/instance.h"
#include "fuicomponents/UIObject.h"

namespace rage 
{

//components
/*
class UILabel;							
class UILayer;							
class UITabbedContainer;
class UIScrollBar;
class UITransition;
class UIObject;
*/

//contexts
class UIButtonContext;
class UILabelContext;
class UITabContext;
class UITabbedContainerContext;
class UIListContext;
class UIScrollBarContext;
class UIProgressBarContext;
class UISpinnerContext;
class UILayerContext;
class UISceneContext;
class UIIconContext;
class UIMessageBoxContext;
class UIPromptStripContext;
class UIContext;
class UIButton;

class audSound;
class grcImage;
class swfCONTEXT;
  
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIComponent
//The base class for all components . To use a component that inherits from UIComponent, 
//you must place the component in a containment hierarchy whose root is a top-level 
//container. Top-level containers -- such as UILayer and UIScene -- are specialized components 
//that provide a place for other components to paint themselves. For an explanation of 
//containment hierarchies, see Swing Components and the Containment Hierarchy, a section in 
//The Java Tutorial http://java.sun.com/docs/books/tutorial/uiswing/learn/index.html.
//
//The UIComponent class provides:
//
//	* The base class for both standard and custom components that use this UI architecture.
//	* A "pluggable look and feel" (L&F) that can be specified by artists.  FLASH is the default
//    tool that is used to visually represent the UI
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIComponent : public UIObject
{
public:

	UIComponent();
	virtual ~UIComponent(){}

public:
	//accessors
	virtual u32 GetRefreshFrequencyFlag(){return m_RefreshFrequencyFlags;}
	virtual UIContext* GetContext();
	virtual void PostStream(char movieID);
	virtual void PostLoad();
	virtual bool ProcessDataTag(const char *name,const char *value);//XML:
	virtual void Reset();
	virtual void AddChild(UIObject* pChild);
	virtual void RemoveChild(UIObject* pChild);
	virtual void RemoveChild(u32 pChild);
	virtual void SetAutoExitTime(f32 time){m_bAutoExit=true;m_TimeBeforeExit=time;}

protected:
	// PURPOSE:	Component common life cycle functions. These functions should never be 
	//			called by client code.  It may be overridden by a more custom component 
	//			which inherits this 
	virtual bool HandleEvents(fuiEvent* pEvent);
	virtual void OnFocused();	
	virtual void OnShow();
	virtual void OnHide();
	virtual void OnActivate();
	virtual void OnDeactivate();
	virtual void OnPaint();
	virtual void Update();

	u8 m_RefreshFrequencyFlags;//TODO move this up into the various component subclasses that will actually use it
	UIContext*	m_pContext;
	bool		m_bAutoExit;
	float		m_TimeBeforeExit;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UILayer 
//UILayer is a generic lightweight container.  It is a container that groups any number of components
//All children are managed by this component
//UILayers can contain other UILayers
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UILayer : public UIComponent
{
public:
	UILayer();

	// PURPOSE:	Transitions are UITransitions that start when this component is Activated and Deactivated
	virtual void SetInTransition(UITransition* pInTransition){m_pInTransition = pInTransition;}
	virtual void SetOutTransition(UITransition* pOutTransition){m_pOutTransition = pOutTransition;}
	virtual UITransition* GetInTransition(){return m_pInTransition;}
	virtual UITransition* GetOutTransition(){return m_pOutTransition;}

	// PURPOSE:	Tells if this object is in "transition" mode.  An object can be in "transition" mode if
	//				it's m_pInTransition or m_pOutTransition is focused
	// RETURNS:	true if transitioning false otherwise
	virtual bool IsTransitioning();
	virtual bool IsTransitioningIn();
	virtual bool IsTransitioningOut();

	virtual void Init();

protected:

	// PURPOSE:	Component common life cycle functions. These functions should never be 
	//			called by client code.  It may be overridden by a more custom layer 
	//			which inherits this 
	virtual bool ProcessDataTag(const char *name,const char *value);//XML:

	UITransition* m_pInTransition;
	UITransition* m_pOutTransition;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIPanel
//UIPanel is a generic lightweight container.  It is a container that groups any number of components
//All children are managed by this component
//In many types of look and feel, panels are opaque by default. Opaque panels work well as content panes and can help 
//with painting efficiently. A transparent panel draws no background, so that any components underneath show through. 
//UIPanels set their visible and active flags to all their children by default.  It only sets the first childs focused flag by default.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIPanel : public UILayer
{
public:
	UIPanel() : UILayer(){}
	virtual ~UIPanel(){}
	virtual void SetFocused(bool b);
	virtual void SetActive(bool b);
	virtual void SetVisible(bool b);
	virtual void Refresh();
	virtual void Update();
	//virtual void Init();
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIScene
//UIScene is a container that groups any number of components
//All UIScenes should be mutually exclusive from all other UIScenes.  This allows us to optimize accordingly.
//For example,  A garage scene may be streamed out when not in the garage.
//UIScene should NOT contain other UIScenes
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIScene : public UILayer
{
public:
	UIScene(){}
	virtual ~UIScene(){}

	//UIScene does a full transition when calling Goto
	virtual void Goto();
	
protected:
		
};

//inherit this interface if you want to make the component scrollable
//class UIScrollable
//{
//	//these are not pure virtual because we may just want to scroll in 1 dimension
//	virtual void ScrollHorizontally(double percent){}
//	virtual void ScrollVertically(double percent){}
//};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIScrollBar
//An implementation of a scrollbar. The user positions the knob in the scrollbar to determine the contents of the viewing area. 
//The program typically adjusts the display so that the end of the scrollbar represents the end of the displayable contents, 
//or 100% of the contents. The start of the scrollbar is the beginning of the displayable contents, or 0%. The position of the 
//knob within those bounds then translates to the corresponding percentage of the displayable contents.
//
//Typically, as the position of the knob in the scrollbar changes a corresponding change is made to the position of the UIList 
//on the underlying view, changing the contents of the UIList. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIScrollBar : public UIComponent
{
public:
	friend class UIList;

	UIScrollBar();
	~UIScrollBar(){}

	// PURPOSE:	Returns the percent complete for the progress bar
	virtual f32	GetPercentScrolled(){return m_Value;}

	// PURPOSE:	Returns the progress bar's current value from 0 to 1
	virtual f32	GetValue(){return m_Value;}

	// PURPOSE: Sets the progress bar's current value to n.
	virtual void SetValue(f32 n){m_Value = n;}

	// PURPOSE: initializes this component to its default values
	virtual void Reset();

	virtual void Init();

protected:
	// PURPOSE:	These functions should never be called by client code.  
	//			It may be overridden by a more custom progress bar which inherits this
	virtual void OnPaint();

	f32 m_Value;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UILabel
//A display area for a short text string
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UILabel : public UIComponent
{
public:

	friend class UIButton;

	UILabel();
	UILabel(const char* pText)	;
	virtual ~UILabel()			{}

	// PURPOSE:	Returns the text string that the label displays.
	const char* GetText() const {return m_pString;}

	// PURPOSE: Defines the string id of the single line of text this component will display.
	void SetText(const char* stringID);

	// PURPOSE: Defines the single line of raw text this component will display. 
	//			Raw strings usually originate from things like gamer tags and metrics.
	virtual void SetTextRaw(const char* rawString);

	void SetColor(s32 color)	{ m_Color = color;	}
	s32	GetColor()				{ return m_Color;	}

	// PURPOSE:	This function should never be called by client code.  
	//			It may be overridden by a more custom progress bar which inherits this
	bool ProcessDataTag(const char* name ,const char* value);

	// PURPOSE:	initializes this component to its default values
	virtual void Reset();

#if __BANK
	void AddWidgets( rage::bkBank & bk );
#endif

protected:

	// PURPOSE:	This function should never be called by client code.  
	//			It may be overridden by a more custom progress bar which inherits this
	virtual void OnPaint();

	// PURPOSE: Initializes local variables
	virtual void Init();

	virtual void PostLoad();

	u8  m_Alpha;
	const char* m_pString;		
	f32 m_Scale;
	s32 m_Color;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIPromptStrip
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIPromptStrip : public UIPanel
{
public:

	friend class UIMessageBox;

	enum PromptStyle
	{
		eMessageBox,
		eBottomScreen
	};

	enum PromptTypes
	{
		None,
		OK,
		OKCancel,
		YesNo,
		YesNoCancel,
		Cancel,
		Continue,
		Custom,
	};

	enum PromptIcons {
		eEMPTY0,
		eACCEPT,
		eCANCEL,
		eLEFT,
		eUP,
		eL_SHOULDER,
		eR_SHOULDER,
		eL_TRIGGER,
		eR_TRIGGER,
		eSTART,
		eSELECT,
		eANALOG_L,
		eANALOG_L_UP_DOWN,
		eANALOG_L_LEFT_RIGHT,
		eANALOG_L_UP_DOWN_LEFT_RIGHT,
		eANALOG_R,
		eANALOG_R_UP_DOWN,
		eANALOG_R_LEFT_RIGHT,
		eANALOG_R_UP_DOWN_LEFT_RIGHT,
		eDPAD,
		eDPAD_LEFT_RIGHT,
		eDPAD_UP_DOWN,
		eDPAD_UP_DOWN_LEFT_RIGHT,
		eLTRIGGER_RTRIGGER,
		eLSHOULDER_RSHOULDER,
		eEMPTY1,
		eEMPTY2,
		eEMPTY3,
		eR3,
		eL3,
		eANALOG_L_RIGHT,
		eANALOG_L_LEFT,
		eANALOG_L_UP,
		eANALOG_L_DOWN,
		eANALOG_R_RIGHT,
		eANALOG_R_LEFT,
		eANALOG_R_UP,
		eANALOG_R_DOWN,
		eDPAD_RIGHT,
		eDPAD_LEFT,
		eDPAD_UP,
		eDPAD_DOWN,
	};

	UIPromptStrip();

	int GetNumPrompts() const { return m_NumPrompts; }
	void AddPrompt(PromptTypes type);
	void AddPrompt(UIButton* );
	PromptIcons GetIconType(PromptTypes type);
	const char* GetPromptText(PromptTypes type);
	virtual void Init();
	virtual void OnPaint();
	virtual void SetStyle(PromptStyle style){m_Style = style;}
	virtual void RemoveAllChildren();
	virtual void RemoveChild(UIObject* pChild);
	virtual void Update();

protected:

	virtual void PostLoad();

	int m_NumPrompts;
	PromptStyle m_Style;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIIcon
//represents single images
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIIcon : public UIComponent
{
public:
	UIIcon();
	virtual ~UIIcon(){}

	friend class UIButton;

	enum IconType{				
		NONE=0,
		NOTREADY=1,
		READY=2,
		BLANK=3,
		VOICE_ENABLED=5,
		VOICE_MUTED=6,
		VOICE_TALKING=7,
		CONNECTION_STRENGTH_0=10,
		CONNECTION_STRENGTH_1=11,
		CONNECTION_STRENGTH_2=12,
		CONNECTION_STRENGTH_3=13,
		CONNECTION_STRENGTH_4=14,
		HASFLAG=17,
		PARTY_LEADER=20,
		PARTY_INVITE=21,
		PARTY_REQUESTED=22,
		PARTY_MEMBER=23,

		NUMICONTYPES
	};

	virtual void SetIconType(IconType);
	virtual void SetIconType(UIPromptStrip::PromptIcons);
	virtual IconType GetIconType(){return m_IconType;}
	virtual UIPromptStrip::PromptIcons GetPromptType(){return m_PromptType;}

	// PURPOSE: initializes this component to its default values
	virtual void Reset();

	virtual void Init();
	virtual bool ProcessDataTag(const char* name ,const char* value);

protected:
	virtual void OnPaint();
#if __BANK
	void AddWidgets( rage::bkBank & bk );
#endif
	//icon could be a generic icon or a button icon
	//union{
	IconType m_IconType;
	UIPromptStrip::PromptIcons m_PromptType;
	//};
};

typedef void(*SimpleEventHandler)(void);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIButton
//An implementation of a "push" button.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIButton : public UIComponent
{
public:
	UIButton();
	virtual ~UIButton();

	// PURPOSE: Returns the text string that the label displays.
	const char* GetText() const {return m_pLabel->GetText();}

	// PURPOSE: Defines the single line of text this component will display.
	void SetText(const char* stringID){m_pLabel->SetText(stringID);}

	// PURPOSE: Returns the text string that the label displays.
	UIIcon::IconType GetIconType() const {return m_pIcon->GetIconType();}

	// PURPOSE: Defines the single line of text this component will display.
	void SetIconType(UIIcon::IconType iconType){return m_pIcon->SetIconType(iconType);}
	void SetIconType(UIPromptStrip::PromptIcons iconType){return m_pIcon->SetIconType(iconType);}

	virtual void SetBlinking(bool b){m_bIsBlinking = b;}
	virtual bool IsBlinking(){return m_bIsBlinking;}

	// PURPOSE: initializes this component to its default values
	virtual void Reset();

	virtual void Init();

	virtual void Enter();
	virtual void Exit();
	virtual void Refresh();
	virtual void SetVisible(bool b);
	virtual void SetFocused(bool b);
	virtual void SetActive(bool b);
	virtual void Update();

	//facade funcs
	virtual UIPromptStrip::PromptIcons GetPromptType(){return m_pIcon->GetPromptType();}

	//only process events when focused
	virtual bool HandleEvents(fuiEvent* e) { return IsFocused()? UIComponent::HandleEvents(e) : false; }

protected:
#if __BANK
	void AddWidgets( rage::bkBank & bk );
#endif

	virtual void OnPaint();
	virtual bool ProcessDataTag(const char* name ,const char* value);

	bool m_bIsBlinking;
	UIIcon* m_pIcon;
	UILabel* m_pLabel;
	atMap<const char* ,SimpleEventHandler, atMapCaseInsensitiveHashFn, atMapCaseInsensitiveEquals> m_EventHandlers;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIList
//A component that allows the user to select one or more objects from a list.
//UIList supports scrolling directly.  By default the UIList selection model allows only 1 
//of the items to be selected at a time.  The UIList contents does not have to be homogeneous.  Items
//in the list may be different types of components.  However they must all inherit from UIObject.
//UIList supports wrapping.  Note that the size of this list does not necessarily equal the number
//children.  For example,  if there are 10 children, but 2 items in the list, then we may visually
//represent this as a list of 10 components where 2 or populated and 8 are empty.  Alternatively, 
//we may have 10 children and 20 items in the list.  This would possibly display 10 components with
//a scrollbar that allows the player to scroll through the 20 items.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIList : public UIComponent
{
public:
	enum Orientation{HORIZONTAL,VERTICAL};

	UIList();

	// PURPOSE:
	virtual void SetListItem(u32 row , UIObject* pData);

	// PURPOSE: 
	virtual void GetListItem(u32 row , u32, char*);

	// PURPOSE: Clears/initializes this list of all data.
	virtual void Reset();

	// PURPOSE: Sets the size of the list.  Note that the size does not necessarily equal the 
	//			number of children.
	virtual void SetLength(int length);

	// PURPOSE: 
	virtual s32 GetCurrentSelection() const;

	// PURPOSE: 
	virtual bool SetCurrentSelection(s32);

	// PURPOSE: Max number of items in the list
	virtual void SetNumListItems(int length){m_NumListItems=length;}

	// PURPOSE: Number of visible items in list
	void SetNumVisibleItems(int length);

	// PURPOSE: if this list is a list of strings, then it can be optimized
	virtual bool IsListOfStrings(){return m_bIsListOfStrings;}	
	virtual void SetIsListOfStrings(bool b){m_bIsListOfStrings = b;}


	// PURPOSE: Sets the previous index to the current tab the selected index for 
	//			this UIList.  If wrapping is allowed, then it will be
	//			accounted for.
	virtual void SelectPrevious();

	// PURPOSE: Sets the next index to the current tab the selected index for 
	//			this UIList.  If wrapping is allowed, then it will be
	//			accounted for.
	virtual void SelectNext();

	// PURPOSE: Removes the given child object/index from the list
	virtual void RemoveChild(UIObject* pChild);
	virtual void RemoveChild(u32 index);

	// PURPOSE: Adds the given child object from the list
	virtual void AddChild(UIObject* pChild);

	// PURPOSE: Adds the given child object from the list
	virtual s32 GetLastItemInList();

	virtual Orientation GetOrientation(){return m_Orientation;}
	virtual Orientation GetScrollOrientation(){return GetOrientation();}
	virtual void HideAllRows();

	virtual void Init();

	virtual void SetTemplate(int i);

protected:

	// PURPOSE:	scrolls this list.  This is not the same as setting the selected list item.  The whole list is actually scrolled.	
	virtual void ProcessScrolling();
	virtual void Scroll(s32 numChildren);

	virtual s32 GetGlobalSelection(){return m_CurrentSelection;}
	virtual s32 GetRelativeSelection(){return m_CurrentSelection - m_FirstItemInList;}
	virtual u32 GetSelectableOffset	(int offset) const	;

	// PURPOSE:	Component common life cycle functions. These functions should never be 
	//			called by client code.  It may be overridden by a more custom progress 
	//			bar which inherits this 
	virtual void OnShow();
	virtual void OnHide();	
	virtual void OnExit();
	virtual void OnActivate();
	virtual void OnDeactivate();
	virtual void OnScroll();
	virtual void OnSelectionChanged();
	virtual void OnFocused();
	virtual void OnUnfocused();
	virtual void OnTransitionComplete();
	virtual bool HandleInputs( fuiEvent* pEvent );
	virtual void OnPaint();
	virtual bool ProcessDataTag(const char* name ,const char* value);
	virtual void PostLoad();
	virtual void PostStream(char movieID);
	virtual void Update();
#if __BANK
	void AddWidgets( rage::bkBank & bk );
#endif


	UIScrollBar m_Scrollbar;
	s32 m_CurrentSelection;
	Orientation m_Orientation;
	s32 m_FirstItemInList;
	s32 m_NumListItems;
	s32 m_NumVisibleListItems;//this may be smaller than mNumListItems and we may want to enable scrolling
	bool m_bAllowWrap;
	bool m_bIsListOfStrings;//optimization flag
	bool m_bIsScrollingEnabled;//able to scroll the whole list if the current selections moves beyond the number of visible components
	bool m_bIsSelectionEnabled;//able to change the current selection in any way
	fuiEvent* m_pListSelectionChanged;
	bool m_bManageScrolling;
	bool m_bChildrenChanged;
	bool m_bAllowRepeat;
	f32 m_RepeatDelay;//seconds to continue repeating
	f32 m_RepeatStartDelay;//seconds to start repeating
	f32 m_TimeSinceLastRepeat;
	bool m_bProcessRepeatScrolling;
};

/*
class UISpinner : public UIMessageBox
{
public:

	UISpinner() : UIMessageBox() {}; 
	~UISpinner(){}

	virtual void OnActivate();
	virtual void OnDeactivate();
};
*/

class UISubList : public UIList
{
	virtual void SetVisible(bool b);
	virtual void Init();
};

class UIMenu : public UIList
{
public:
	UIMenu()
		: UIList()
		, m_Template(-1)
	{}
	~UIMenu() {}

	virtual void OnPaint();
	
	void SetTemplate(int i) { m_Template=i; }

protected:
	int m_Template;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIButton
//An implementation of a "push" button.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UITab : public UILayer
{
public:
	UITab();
	virtual ~UITab();

	// PURPOSE: Returns the text string that the label displays.
	const char* GetText() const {return m_pLabel->GetText();}

	// PURPOSE: Defines the single line of text this component will display.
	void SetText(const char* stringID){m_pLabel->SetText(stringID);}

	// PURPOSE: Returns the text string that the label displays.
	UIIcon::IconType GetIconType() const {return m_pIcon->GetIconType();}

	// PURPOSE: Defines the single line of text this component will display.
	void SetIconType(UIIcon::IconType iconType){return m_pIcon->SetIconType(iconType);}

	// PURPOSE: initializes this component to its default values
	virtual void Reset();

	virtual void Init();

protected:

	bool ProcessDataTag(const char* name ,const char* value);

	UIIcon* m_pIcon;
	UILabel* m_pLabel;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UITabbedContainer
//A component that lets the user switch between a group of components by clicking on a tab with a 
//given title and/or icon. 
//
//Tabs/components are added to a UITabbedContainer object by using the AddTab method. A tab is 
//represented by an index corresponding to the position it was added in, where the first tab 
//has an index equal to 0 and the last tab has an index equal to the tab count minus 1. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UITabbedContainer : public UIComponent
{
public:
   UITabbedContainer();
   ~UITabbedContainer(){}

	// PURPOSE: Returns the number of tabs for this UITabbedContainer 
	virtual u32 GetNumTabs(){return GetNumberOfChildren();}

	// PURPOSE: Sets the selected index for this UITabbedContainer.
	virtual void SetCurrentTab(u32);

	// PURPOSE: Sets the selected tab with the given name for this UITabbedContainer.
	virtual void SetCurrentTab(const char* pName){SetCurrentTab(GetChildIndex(pName));}

	// PURPOSE: Gets the selected index for this UITabbedContainer.
	virtual u32 GetCurrentTabIndex(){return m_CurrentTab;}

	// PURPOSE: Gets the tab child with the given name.
	virtual UIObject* GetCurrentTab(){return GetChild(m_CurrentTab);}

	// PURPOSE: Gets the tab child with the given name.
	virtual UIObject* GetTab(const char* pName){return GetChild(pName);}

	// PURPOSE: Sets the previous index to the current tab the selected index for 
	//			this UITabbedContainer.  If wrapping is allowed, then it will be
	//			accounted for.
	virtual void SelectPrevious();

	// PURPOSE: Sets the next index to the current tab the selected index for 
	//			this UITabbedContainer.  If wrapping is allowed, then it will be
	//			accounted for.
	virtual void SelectNext();

	// PURPOSE: Adds a component and tip  represented by a title and/or icon, either 
	//			of which can be null.
	virtual void AddTab(const char* tabNameId , UIIcon::IconType iconId);

	// PURPOSE: initializes this component to its default values
	virtual void Reset();

	virtual void Init();

protected:

	// PURPOSE:	This function should never be called by client code by client code.  
	//			It is called when the current tab has changed
	//			It may be overridden by a more custom progress bar which inherits this 
	virtual void OnTabChanged();

	// PURPOSE:	These functions should never be called by client code.  
	//			It may be overridden by a more custom progress bar which inherits this 
	virtual bool HandleInputs( fuiEvent* pEvent);
	virtual bool ProcessDataTag(const char* name ,const char* value);
	virtual void OnPaint();
	virtual void OnShow();
	virtual void OnHide();
	virtual void OnActivate();
	virtual void OnDeactivate();
	virtual void OnFocused();
	virtual void OnUnfocused();

	u32 m_CurrentTab;
	bool m_bAllowWrap;
};

//class UIPrompt
//{
//private:
//	int				m_PromptButtonID;
//	flashString		m_PromptLabel;
//	float			m_PromptPosition[2];
//	float			m_PromptButtonAlpha;
//	float			m_PromptLabelAlpha;
//	int				m_PromptListPosition;
//public:
//	UIPrompt()
//	{
//		m_PromptButtonID = -1;
//		m_PromptLabel = "";
//		m_PromptPosition[0] = 0.0f;
//		m_PromptPosition[1] = 0.0f;
//		m_PromptButtonAlpha = 100.0f;
//		m_PromptLabelAlpha = 100.0f;
//		m_PromptListPosition = 0;
//	}
//	~UIPrompt(){}
//	int				GetButtonID() { return m_PromptButtonID; }
//	const char*		GetLabel() { return m_PromptLabel; }
//	float			GetPositionX() { return m_PromptPosition[0]; }
//	float			GetPositionY() { return m_PromptPosition[1]; }
//	float			GetButtonAlpha() { return m_PromptButtonAlpha; }
//	float			GetLabelAlpha() { return m_PromptLabelAlpha; }
//	int				GetListPosition() { return m_PromptListPosition; }
//
//	void			SetButtonID(int ID) { m_PromptButtonID = ID; }
//	void			SetLabel(char* label) { m_PromptLabel = label; }
//	void			SetPositionX(float val) { m_PromptPosition[0] =  val; }
//	void			SetPositionY(float val) { m_PromptPosition[1] = val; }
//	void			SetButtonAlpha(float val) { m_PromptButtonAlpha = val; }
//	void			SetLabelAlpha(float val) { m_PromptLabelAlpha = val; }
//	void			SetListPosition(int val) { m_PromptListPosition = val; }
//	void			Update();
//	void			SetPrompt(int ID, char* lable, float x, float y, float buttonAlpha, float labelAlpha, int listPosition);
//};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UITicker
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UITicker : public UIComponent
{
public:

	enum
	{
		MAX_TEXT_LENGTH = 128,
		MAX_NUM_ITEMS = 6,
	};

	struct TickerItem
	{
		char	m_Text[MAX_TEXT_LENGTH];
		f32		m_fLife;

		void Reset() 
		{ 
			m_Text[0]=0;
			m_fLife=.0f;
		}
		bool IsUsed() const { return m_Text[0]!=0; }
	};

	UITicker();
	~UITicker(){}

	virtual void OnPaint();
	virtual void Init();
	virtual void Update();

	TickerItem& AddItem();

#if __BANK
	void AddWidgets( rage::bkBank & bk );
#endif

protected:

	int			m_iNumItems;
	TickerItem	m_Items[MAX_NUM_ITEMS];
	rage::atQueue< TickerItem*, MAX_NUM_ITEMS> m_SortedItems;
	
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIMessageBox
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIMessageBox : public UIList
{
public:

	enum UIMessageBox_Styles
	{
	eDefault,
	eSimple
	};

	UIMessageBox(); 
	~UIMessageBox(){}

	virtual void OnPaint();
	virtual void OnActivate();
	virtual void OnDeactivate();
	virtual void OnShow();
	virtual void OnHide();
	virtual bool ProcessDataTag(const char* name ,const char* value);
	virtual void Goto();
	virtual bool HandleInputs(  fuiEvent* pEvent);
	virtual void SetTitle(const char* t);
	virtual void SetDescription ( const char* );
	//virtual void SetupPrompts();
	virtual bool SetCurrentSelection(rage::s32 currentSelection);
	static void	 ShowDialog(const char* name);
	virtual void Init();
	virtual void SetStyle(UIMessageBox_Styles style);
	virtual void Refresh();
	virtual void Update();
	virtual void PostStream(char movieID);

protected:	

	virtual void PostLoad();

	UIPromptStrip*		m_pPrompts;
	const char* 		m_Description;
	const char* 		m_Title;
	bool 				m_bPauseOnShow;
	bool 				m_bWasPaused;
	u8					m_Priority;
	UIMessageBox_Styles m_Styles;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIProgressBar
//A component that, by default, displays an integer value within a bounded interval. A progress 
//bar typically communicates the progress of some work by displaying its percentage of completion 
//and possibly a textual display of this percentage.
//
//To indicate that a task of unknown length is executing, you can put a progress bar into 
//indeterminate mode. While the bar is in indeterminate mode, it animates constantly to show that 
//work is occurring. As soon as you can determine the task's length and amount of progress, you 
//should update the progress bar's value and switch it back to determinate mode.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIProgressBar : public UIComponent
{
public:
	UIProgressBar();
	~UIProgressBar(){}

	// PURPOSE:	Returns the progress bar's maximum value
	virtual f32	GetMaximum(){return m_Maximum;}

	// PURPOSE:	Returns the progress bar's minimum value
	virtual f32	GetMinimum(){return m_Minimum;}

	// PURPOSE:	Returns the percent complete for the progress bar
	virtual f32	GetPercentComplete(){return m_Value / (m_Maximum - m_Minimum);}

	// PURPOSE:	Returns the progress bar's current value
	virtual f32	GetValue(){return m_Value;}

	// PURPOSE:	Returns the value of the indeterminate property
	virtual bool IsIndeterminate(){return m_bIsIndeterminate;}

	// PURPOSE:	This function should never be called by client code.  
	//			It may be overridden by a more custom progress bar which inherits this
	virtual void OnPaint();

	// PURPOSE: Sets the indeterminate property of the progress bar, which determines 
	//			whether the progress bar is in determinate or indeterminate mode.	
	virtual void SetIndeterminate(bool newValue){m_bIsIndeterminate = newValue;}

	// PURPOSE: Sets the progress bar's maximum value to n.
	virtual void SetMaximum(f32 n){m_Maximum = n;}
		
	// PURPOSE: Sets the progress bar's minimum value to n.
	virtual void SetMinimum(f32 n){m_Minimum = n;}
	
	// PURPOSE: Sets the value of the progress string.
	virtual void SetTextRaw(const char* s);

	// PURPOSE: Defines the string id of the single line of text this component will display.
	void SetText(const char* stringID);

	// PURPOSE: Sets the progress bar's current value to n.
	virtual void SetValue(f32 n);

	virtual void Init();

	bool ProcessDataTag(const char* name ,const char* value);
protected:

	f32 m_Minimum;
	f32 m_Maximum;
	f32 m_Value;
	bool m_bIsIndeterminate;
	const char* m_pProgressString;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UISpinner
//A single line input field that lets the user select a number or an object value from an ordered 
//sequence. Spinners typically provide a pair of tiny arrow buttons for stepping through the 
//elements of the sequence. Although combo boxes provide similar functionality, spinners are 
//sometimes preferred because they don't require a drop down list that can obscure important data.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UISpinner : public UILayer
{
public:

	UISpinner(){}
	UISpinner(int min, int max, int value);

	//properties:
	int GetMinimum() const { return m_iMin; }
	int GetMaximum() const { return m_iMax; }
	int GetValue() const { return m_iValue; }
	int GetIncrement() const { return m_iIncrement; }

	void SetMinimum(int value) { m_iMin = value; }
	void SetMaximum(int value) { m_iMax = value; }
	void SetValue(int value) { int iOld = m_iValue; m_iValue = value; if(iOld != value) m_valueChanged.Call(); }
	void SetIncrement(int increment ) { m_iIncrement = increment; }

	void SetLeftAudio(const char *sound) { m_pLeftAudio = sound; }
	void SetRightAudio(const char *sound) { m_pRightAudio = sound; }

	void SetIsWrap(bool wrap) { m_bWrap = wrap; }
	bool GetIsWrap() const { return m_bWrap; }

	//events
	void RegisterValueChanged(datCallback callback) { m_valueChanged = callback; }

	virtual void Init();
protected:
	int	m_iMin;
	int	m_iMax;
	int	m_iValue;
	int	m_iIncrement;
	bool m_bWrap;
	datCallback	m_valueChanged;
	const char *m_pLeftAudio;
	const char *m_pRightAudio;
};


}
#endif // UICOMPONENTS_COMPONENTS_H
