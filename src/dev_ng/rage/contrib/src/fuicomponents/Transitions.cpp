// 
// flashUIObjects/transitions.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "system/param.h"
#include "system/timemgr.h"
#include "vector/matrix34.h"
#include "Transitions.h"
#include "fuicore/UIManager.h"
#include "fuicomponents/UIObject.h"
#include "fuicore\Common.h"
#include "fuicore/EventManager.h"
#include "fuiflash/Flash.h"
#include "flash/resourceversions.h"
#include "fuicore/UICallback.h"

namespace rage{

#define FRAME_TIME .033333333f
#define MAX_ANGLE PI/4
#define MAX_ROTATE_INC_PER_FRAME .033f
#define VERY_LOW_DT .0001f

#if __DEV
	static int s_NumTransitions = 0;
	static size_t iAfter;
	static size_t iDiff;
	static size_t iBefore;
	static size_t iBlock; 
	static size_t iUsed;
	static size_t iObject;
#endif

const char* UIObjectUIEventName[] =
{
   "ListSelectionChanged",
   "ListEmpty",
   NULL
};

const char* UIObjectUICmdName[] =
{
   "Enable",
   "Disable",
   "ToggleEnabled",
   NULL
};

UITransition::UITransition() : UIObject()
{
	m_MotionType = PHYSICS;
	m_TransitionTime = -1.0f;
	m_PercentDistance = 1.0f;
	m_pTarget = NULL;
	m_StartPos.Identity();
	m_FinalPos.Identity();
	m_DelayTime = 0.0f;
	m_bComplete = false;
	m_ElapsedTime = 0.0f;
	m_PercentComplete = 0.0f;
	m_TimeTakenThisFrame = 0.0f;
	m_Timestep = 0.0f;

#if __DEV
	m_TimeTakenThisFrameSUM = 0.0f;
	s_NumTransitions++;
#endif

	m_bEaseIn = false;
	m_bEaseOut = false;
	m_bEaseInOut = false;
	m_bIsDisabled = false;
	m_RealTotalTime = 0.0f;
	m_IsSimulating = false;
	m_bIsRoot = false;

}

#if __BANK
void UITransition::StartThis()
{
//	UILOGIC.GetWarper().SetUserRequestedTransition(this);
//	mcUILogic::GetInstance()->GetWarper().GetCameraTransition()->SetWarpType("TEST");
//	UILOGIC.GetWarper().Warp();
}

void UITransition::AddWidgets( rage::bkBank & bk ) 
{      
   bk.AddToggle( "Complete", &m_bComplete );
   bk.AddToggle( "Disabled", &m_bIsDisabled );
   bk.AddSlider( "Transition Time", &m_TransitionTime, -1.0f, 1000.0f, 0.1f );
   bk.AddSlider( "Delay Time", &m_DelayTime, -1.0f, 1000.0f, 0.1f );
   
   bk.AddButton("START",datCallback(MFA(UITransition::StartThis), this));

   bk.PushGroup( "Start Matrix", false );
   {
      AddMatrixWidgets(bk,&m_StartPos);
   }
   bk.PopGroup();
   bk.PushGroup( "Final Matrix", false );
   {
      AddMatrixWidgets(bk,&m_FinalPos);
   }
   bk.PopGroup();

   UIObject::AddWidgets(bk);
}
#endif

f32 UITransition::CalculateRealTotalTime()
{
	Matrix34 simTarget;
	Matrix34* pTarget;
	Matrix34* pFinal;
	Matrix34* pStart;

	Assert(IsActive()==false);
	m_IsSimulating = true;
	m_RealTotalTime = 0.0f;

	pTarget = GetTarget();
	pFinal = GetFinalMatrix();
	pStart = GetStartMatrix();

	SetTarget(&simTarget);
	SetComplete(false);
	Activate();
	SetActive(true);
	SetVisible(true);
	SetFocused(true);

	//calculate our total real time
	while (IsComplete()==false)
	{
		Update(FRAME_TIME);
		m_RealTotalTime += GetTimeTakenThisFrame();
	}		  		   

	SetFocused(false);
	SetVisible(false);
	SetActive(false);	
	Deactivate();	
	SetComplete(false);
	m_IsSimulating = false;

	SetTarget(pTarget);
	SetFinalMatrix(pFinal);
	SetStartMatrix(pStart);
	return m_RealTotalTime;
}

void UITransition::Reset()
{
	m_bComplete = false;
	m_ElapsedTime = 0.0f;   
	m_PercentComplete = 0.0f;
	m_PercentDistance = 1.0f;
	m_TimeTakenThisFrame = 0.0f;

#if __DEV
	m_TimeTakenThisFrameSUM = 0.0f;
#endif

}

void UITransition::OnDeactivate()
{
    UIObject::OnDeactivate();

	//fire transition complete event if we are the root transition
	if (!m_IsSimulating && m_bIsRoot)
	{
		UIMANAGER->SendEvent("TRANSITION_COMPLETE",this);
	}
	
	Reset();
}

void UITransition::OnActivate()
{
	UIObject::OnActivate();
	
	m_ElapsedTime = 0.0f;	
	m_TimeTakenThisFrame = 0;
	m_PercentComplete = 0;
	m_PercentDistance = 0;
	SetComplete(false);

#if __DEV
	m_TimeTakenThisFrameSUM = 0.0f;
#endif

}

void UITransition::OnFocused()
{
	UIObject::OnFocused();

	//init the "current" matrix
	m_Matrix.Zero();
}

void UITransition::Update(rage::f32 dt)
{
	//change dt to use the set timestamp
	if (m_Timestep)
	{
		dt = m_Timestep;
	}

	Assertf(m_FinalPos.IsOrthonormal(.1f),"final position is screwed up or was never set");
	Assertf(m_StartPos.IsOrthonormal(.1f),"start position is screwed up or was never set");

	if (m_bIsDisabled)
	{
		SetComplete(true);
		return;
	}

	UIObject::Update(dt);
}

void UITransition::SetStartMatrix(const rage::Matrix34* startPos)
{
	m_StartPos.Set(*startPos);
	m_StartPos.a.Normalize();
	m_StartPos.b.Normalize();
	m_StartPos.c.Normalize();
	Assertf(m_StartPos.IsOrthonormal(.1f),"start position is screwed up");
}

void UITransition::SetFinalMatrix(const rage::Matrix34* finalPos)
{
	m_FinalPos.Set(*finalPos);
	m_FinalPos.a.Normalize();
	m_FinalPos.b.Normalize();
	m_FinalPos.c.Normalize();

	Assertf(m_FinalPos.IsOrthonormal(.1f),"final position is screwed up");
}

void UITransition::GetUpdatedFinalMatrix(rage::Matrix34* m)
{
	if (m_bIsRoot)
	{
		m->Set(m_StartPos);
		UpdateMatrixToFinal(m);
		return;
	}
	else
	{
		verify_cast<UITransition*>(m_pParent)->GetUpdatedFinalMatrix(m);
		return;
	}
}

//find a target and it's matrix by walking up the parent path
Matrix34* UITransition::GetTarget()
{
	if (m_pTarget)
	{
		return m_pTarget;
	}
	
	UITransition* pTransition = verify_cast<UITransition*>(m_pParent);

	if (pTransition)
	{
		return pTransition->GetTarget();
	}

	return NULL;
}

void UITransition::SetComplete(bool complete)
{   
	if (!m_bComplete && complete)
	{
		UIDebugf1("%s COMPLETE.  Time taken = %f,(%f)",GetName(), GetElapsedTime() , GetRealElapsedTime());
		SetFocused(false);
	}

	//set all children as complete too
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		verify_cast<UITransition*>(pChild)->SetComplete(complete);
	}

	m_bComplete = complete;
}

void UITransition::SetMotionType(const char* value)
{
	if (value)
	{
		if(stricmp(value,"WAVE")==0)
		{
			m_MotionType = WAVE;
		}
		else if(stricmp(value,"ACCELERATE")==0)
		{
			m_MotionType = ACCELERATE;
		}
		else if(stricmp(value,"DECELERATE")==0)
		{
			m_MotionType = DECELERATE;
		}
		else if(stricmp(value,"NONE")==0)
		{
			m_MotionType = NONE;
			SetTransitionTime(1.0f);//time is not a factor when warping.  just set it to something that will be greater than dt
		}
	}
	else
	{
		m_MotionType = WAVE;
	}
}

bool UITransition::ProcessDataTag(const char* name , const char* value)
{
	if(stricmp(name,"total_time")==0)
	{
	  SetTransitionTime((f32)atof(value));      
	  if (m_RealTotalTime <= 0.1f)
	  {
		  m_RealTotalTime = GetTransitionTime();
	  }
	}
	else if(stricmp(name,"real_total_time")==0)
	{
		m_RealTotalTime = ((f32)atof(value));      
	}	
	else if(stricmp(name,"motion_type")==0)
	{
	  SetMotionType(value);
	}
	else if(stricmp(name,"ease_in")==0)
	{
		m_bEaseIn = true;
	}
	else if(stricmp(name,"ease_out")==0)
	{
		m_bEaseOut = true;
	}
	else if(stricmp(name,"ease_in_out")==0)
	{
		m_bEaseInOut = true;
	}
	else if(stricmp(name,"delay_start_time")==0)
	{
		SetDelayTime((f32)atof(value));
	}
	else if(stricmp(name,"na")==0)
	{		
	}
	else if(stricmp(name,"ax")==0)
	{
		SetFinal_AX((f32)atof(value));
	}
	else if(stricmp(name,"ay")==0)
	{
		SetFinal_AY((f32)atof(value));
	}
	else if(stricmp(name,"az")==0)
	{
		SetFinal_AZ((f32)atof(value));
	}
	else if(stricmp(name,"bx")==0)
	{
		SetFinal_BX((f32)atof(value));
	}
	else if(stricmp(name,"by")==0)
	{
		SetFinal_BY((f32)atof(value));
	}
	else if(stricmp(name,"bz")==0)
	{
		SetFinal_BZ((f32)atof(value));
	}
	else if(stricmp(name,"cx")==0)
	{
		SetFinal_CX((f32)atof(value));
	}
	else if(stricmp(name,"cy")==0)
	{
		SetFinal_CY((f32)atof(value));
	}
	else if(stricmp(name,"cz")==0)
	{
		SetFinal_CZ((f32)atof(value));
	}
	else if(stricmp(name,"dx")==0)
	{
		SetFinal_DX((f32)atof(value));
	}
	else if(stricmp(name,"dy")==0)
	{
		SetFinal_DY((f32)atof(value));
	}
	else if(stricmp(name,"dz")==0)
	{
		SetFinal_DZ((f32)atof(value));
	}
	else
	{
		return UIObject::ProcessDataTag(name,value);
	}

    return true;
}

f32 UITransition::MakeTimeDevisibleByDT(float time)
{
	//lets reprocess time so that we can be guaranteed to end on a frame boundary
	f32 numFrames = time * TIME.GetFPS();

	//numFrames is rounded and we can now get a time that cause the transition to end on a frame boundary
	int numFramesRounded = rage::round(numFrames);
	return numFramesRounded / TIME.GetFPS();
}

void UITransition::SetTransitionTime(float time)
{
	//lets reprocess time so that we can be guaranteed to end on a frame boundary
	m_TransitionTime = MakeTimeDevisibleByDT(time);
	UIDebugf1("Transition time converted from %f to %f for %s",time,m_TransitionTime,GetName());
}

void UITransition::SetDelayTime(float time)
{
	//lets reprocess time so that we can be guaranteed to end on a frame boundary
	m_DelayTime = MakeTimeDevisibleByDT(time);

	UIDebugf1("Delay time converted from %f to %f for %s",time,m_DelayTime,GetName());
}

void UITransition::Terminate()
{
   if (IsFocused())
   {
      if (IsComplete() == false)
      {
         SetComplete(true);         
      }
      UIMANAGER->GetUINavigator()->PopTransition(this);
   }

   //OnActivate all parallel children
   for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
   {
      verify_cast<UITransition*>(pChild)->Terminate();
   }
}

f32 UITransition_Parallel::GetTimeTakenThisFrame()
{
	f32 maxTime=0.0f;

	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		f32 time = (verify_cast<UITransition*>(pChild))->GetTimeTakenThisFrame();
		if (time > maxTime)
		{
			maxTime = time;
		}
	}   

	return maxTime;
}

void UITransition_Parallel::SetTimestep(rage::f32 f)
{
	UITransition::SetTimestep(f);

	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		(verify_cast<UITransition*>(pChild))->SetTimestep(f);
	}
}

void UITransition_Parallel::SetStartMatrix(const Matrix34* pStartPos)
{
	if (pStartPos)
	{
		m_StartPos.Set(*pStartPos);

		for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
		{
			(verify_cast<UITransition*>(pChild))->SetStartMatrix(pStartPos);
		}
	}
}

void UITransition_Parallel::SetFinalMatrix(const Matrix34* pFinalPos)
{   
	if (pFinalPos)
	{
		m_FinalPos.Set(*pFinalPos);
		for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
		{
			(verify_cast<UITransition*>(pChild))->SetFinalMatrix(&m_FinalPos);
		}
	}
}

void UITransition_Parallel::SetTarget(Matrix34* pTarget)
{  
	if (pTarget)
	{
		m_pTarget = pTarget;

		m_bIsRoot = true;

		SetMatrix(pTarget);

		for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
		{
			verify_cast<UITransition*>(pChild)->SetMatrix(pTarget);
		}
	}
}

void UITransition_Parallel::Reset()
{
	UITransition::Reset();

	m_RealElapsedTime = 0.0f;
}

void UITransition_Parallel::OnActivate()
{
   UITransition::OnActivate();

   m_RealElapsedTime = 0.0f;
   SetComplete(false);

   //OnActivate all parallel children
   for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
   {
	   pChild->Activate();
   }

   SetTransitionTime(GetTotalTime());
}

void UITransition_Parallel::OnDeactivate()
{
   //OnDeactivate all parallel children
   for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
   {
      pChild->Deactivate();
   }

   Reset();

   UITransition::OnDeactivate();
}

void UITransition_Parallel::SetFocused(bool b)
{
	//OnDeactivate all parallel children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		pChild->SetFocused(b);
	}

	UITransition::SetFocused(b);
}

void UITransition_Parallel::Activate()
{
	//OnDeactivate all parallel children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		pChild->Activate();
	}

	UITransition::Activate();
}

void UITransition_Parallel::Deactivate()
{
	//OnDeactivate all parallel children
	for (UIObject* pChild=verify_cast<UIObject*>(m_pFirstChild); pChild!=NULL; pChild = verify_cast<UIObject*>(pChild->GetNextSibling()))
	{
		pChild->Deactivate();
	}

	UITransition::Deactivate();
}

void UITransition_Parallel::OnUnFocused()
{
	//OnDeactivate all children
	for (UIObject* pChild=verify_cast<UIObject*>(m_pFirstChild); pChild!=NULL; pChild = verify_cast<UIObject*>(pChild->GetNextSibling()))	
	{
		pChild->SetFocused(false);
	}

	UITransition::OnUnfocused();
}

void UITransition_Parallel::SetVisible(bool b)
{
	//OnDeactivate all parallel children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		pChild->SetVisible(b);
	}

	UITransition::SetVisible(b);
}
//
//bool UITransition_Parallel::HandleEvents(  const UIExtObject::NotifyCode priority, int* p_EventID,rage::s8 stateMachineIndex)
//{
//	bool consumeEvent = false;
//
//	consumeEvent |= UITransition::HandleEvent(pUI , p_Obj,priority, p_EventID, stateMachineIndex);
//
//	if (!consumeEvent)
//	{
//		if(p_Obj == UIManagerSingleton::GetInstance().GetUINavigator()->GetInputHandler())
//		{
//			AssertMsgf(*p_EventID >= 0 && *p_EventID < FlashNavigator::fnInputHandler::fnInputEventCount,("Event index out of range"));
//
//			//this must be an input event type
//			switch(*p_EventID)
//			{
//			case FlashNavigator::fnInputHandler::fnInputEventAction:
//				{
//					//SetComplete(true);
//					break;
//				}
//			default :
//				{
//					break;
//				}
//			}
//		}
//	}
//
//	return consumeEvent;
//}

void UITransition_Parallel::OnPaint()
{
   UITransition::OnPaint();

   //OnPaint all parallel children
   for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
   {
      pChild->OnPaint();
   }
}

void UITransition_Parallel::SetLookAtTargetOffset(rage::f32 f)
{
	UIObject* pChild = m_pFirstChild;
	while(pChild)
	{
		(verify_cast<UITransition*>(pChild))->SetLookAtTargetOffset(f);
		pChild = pChild->GetNextSibling();
	}
}

//void UITransition_Parallel::GetVelocity(Vector3* v)
//{
//   //sync my velocity with childs velocities
//   UIObject* pChild = m_pFirstChild;
//   while(pChild)
//   {
//	   (verify_cast<UITransition*>(pChild))->GetVelocity(v);
//	   pChild = pChild->GetNextSibling();
//   }
//}

void UITransition_Parallel::SetVelocity(Vector3* v)
{
	UITransition* pTransition;

	UIObject* pChild = m_pFirstChild;
	while(pChild)
	{
		pTransition = verify_cast<UITransition*>(pChild);
		pTransition->SetVelocity(v);
		pChild = pChild->GetNextSibling();
	}
}

void UITransition_Parallel::SetTargetVelocity(Vector3* v)
{
	UITransition* pTransition;

	UIObject* pChild = m_pFirstChild;
	while(pChild)
	{
		pTransition = verify_cast<UITransition*>(pChild);
		pTransition->SetTargetVelocity(v);
		pChild = pChild->GetNextSibling();
	}
}

void UITransition_Parallel::SetPreviousVelocity(Vector3* pAcceleration)
{
	//SetVelocity all parallel children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		(verify_cast<UITransition*>(pChild))->SetPreviousVelocity(pAcceleration);
	}
}

void UITransition_Parallel::SetAcceleration(Vector3* pAcceleration)
{
	//SetVelocity all parallel children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		(verify_cast<UITransition*>(pChild))->SetAcceleration(pAcceleration);
	}
}

void UITransition_Parallel::SetPreviousAcceleration(Vector3* pAcceleration)
{
	//SetVelocity all parallel children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		(verify_cast<UITransition*>(pChild))->SetPreviousAcceleration(pAcceleration);
	}
}

//void UITransition_Parallel::GetRotationalVelocity(f32 rotVelocity)
//{	
//	//sync my velocity with childs velocities
//	UIObject* pChild = m_pFirstChild;
//	while(pChild)
//	{
//		(verify_cast<UITransition*>(pChild))->GetRotationalVelocity(rotVelocity);
//		pChild = pChild->GetNextSibling();
//	}
//}

void UITransition_Parallel::SetRotationalVelocity(f32 rv)
{
	UITransition* pTransition;

	UIObject* pChild = m_pFirstChild;
	while(pChild)
	{
		pTransition = verify_cast<UITransition*>(pChild);
		pTransition->SetRotationalVelocity(rv);
		pChild = pChild->GetNextSibling();
	}
}

void UITransition_Parallel::SetPreviousRotationalVelocity(f32 rotVelocity)
{
	//SetVelocity all parallel children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		(verify_cast<UITransition*>(pChild))->SetPreviousRotationalVelocity(rotVelocity);
	}
}

//void UITransition_Parallel::SetTargetPosition(Vector3* rotVelocity)
//{
//	//SetVelocity all parallel children
//	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
//	{
//		(verify_cast<UITransition*>(pChild))->SetTargetPosition(rotVelocity);
//	}
//}

rage::f32 UITransition_Parallel::GetTotalTime()
{
	f32 childTime = 0.0f;
	f32 maxTime= 0.0f;
	UITransition* pTransition;
	for (UIObject* pChild=GetChild(0); pChild; pChild = pChild->GetNextSibling())
	{
		pTransition = (verify_cast<UITransition*>(pChild)); 
		childTime = pTransition->GetTransitionTime() + pTransition->GetDelayTime();
		if (childTime > maxTime){maxTime = childTime;}
	}

	if (GetNumberOfChildren() == 0)
	{
		return m_TransitionTime;
	}
	else
	{
		return maxTime;
	}
}

float UITransition_Parallel::SlowOut(float t)
{
	if (t>0.0f)
	{
		if (t<1.0f)
		{
			return rage::Sinf(Sqrtf(t)*PI*0.5f);
		}
		return 1.0f;
	}
	return 0.0f;
}

void UITransition_Parallel::Update(float dt)
{  	
	//change dt to use the set timestamp
	if (m_Timestep)
	{
		dt = m_Timestep;
	}

	////lets use non-fixed frame time
	//if (m_bIsRoot)
	//{
	//	dt = TIME.GetWarpedRealtimeSeconds();
	//}

	if (m_bIsDisabled)
	{
		SetComplete(true);
		return;
	}

	if (IsComplete())
	{
		m_TimeTakenThisFrame = 0.0f;
		return;
	}

	f32 amplifiedDT = dt;
	f32 totalTime = (m_TransitionTime+m_DelayTime);
	m_RealElapsedTime += dt;

	m_PercentComplete =(m_RealElapsedTime-m_DelayTime)/m_TransitionTime;
	if(m_PercentComplete<0.0f){m_PercentComplete=0.0f;}

	if (m_bEaseIn)
	{
		//get the current amplified time slice
		amplifiedDT = (totalTime*SlowIn(m_PercentComplete)) - m_ElapsedTime;
	}
	else if (m_bEaseOut)
	{
		//get the current amplified time slice
		amplifiedDT = (totalTime*rage::SlowOut(m_PercentComplete)) - m_ElapsedTime;
	}
	else if (m_bEaseInOut)
	{
		//get the current amplified time slice
		amplifiedDT = (totalTime*SlowInOut(m_PercentComplete)) - m_ElapsedTime;
	}
	else
	{		
		amplifiedDT = dt;
	}

	m_ElapsedTime += amplifiedDT;

	if (m_ElapsedTime<m_DelayTime)
	{
		return;
	}

	UITransition::Update(amplifiedDT);

	bool allComplete=true;
	
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		UITransition* pTransition = verify_cast<UITransition*>(pChild);
		if (pTransition->IsComplete()==false)
		{
			allComplete = false;
		}			
	}  

	//sync the targets matrix with this matrix
	if (m_pTarget)
	{
		m_pTarget->Set(m_StartPos);
		UpdateMatrix(m_pTarget);
	}

	if (allComplete)
	{            
		//adjust elapsed time depending on if all of dt was used
		m_ElapsedTime -= amplifiedDT;
		m_ElapsedTime += GetTimeTakenThisFrame();

		SetComplete(allComplete);
	}

#if __DEV
	m_TimeTakenThisFrameSUM += m_TimeTakenThisFrame;
#endif

}

void UITransition_Parallel::PostLoad()
{
   //set the transition time to all children
   for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
   {
	   UITransition* pTransition = (verify_cast<UITransition*>(pChild));

	   if (m_TransitionTime != -1 && pTransition->GetTransitionTime() == -1.0f)//don't overwrite a child that is setting his own time
	   {         
		   pTransition->SetTransitionTime(m_TransitionTime);
	   }
   }

   //post load children after this state has done special stuff
   UITransition::PostLoad();
}

void UITransition_Parallel::SetDistanceOffset(const rage::Vector3* v)
{
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		(verify_cast<UITransition*>(pChild))->SetDistanceOffset(v);
	}
}

void UITransition_Parallel::UpdateMatrix(Matrix34* pMatrix)
{
	if (!m_bIsDisabled && m_ElapsedTime > m_DelayTime)
	{
		for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
		{
			(verify_cast<UITransition*>(pChild))->UpdateMatrix(pMatrix);
		}
	}
}

void UITransition_Parallel::UpdateMatrixToFinal(Matrix34* pMatrix)
{
	if (!m_bIsDisabled)
	{
		for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
		{
			(verify_cast<UITransition*>(pChild))->UpdateMatrixToFinal(pMatrix);
		}
	}
}

void UITransition_Serial::SetTimestep(rage::f32 f)
{
	UITransition::SetTimestep(f);

	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		(verify_cast<UITransition*>(pChild))->SetTimestep(f);
	}
}

f32 UITransition_Serial::GetTimeTakenThisFrame()
{
	UITransition* pState = verify_cast<UITransition*>(GetChild(m_FocusedChildIndex));

	return pState->GetTimeTakenThisFrame();
}

void UITransition_Serial::SetDistanceOffset(const rage::Vector3* v)
{
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		(verify_cast<UITransition*>(pChild))->SetDistanceOffset(v);
	}
}

void UITransition_Serial::UpdateMatrix(Matrix34* pMatrix)
{
	if (!m_bIsDisabled && m_ElapsedTime > m_DelayTime)
	{
		UIObject* pChild = m_pFirstChild;
		while(pChild)
		{
			//apply any modified matrices from the start state to the current one
			UITransition* pState = verify_cast<UITransition*>(pChild);
			if (pState->IsComplete() || pState->IsFocused())
			{
				pState->UpdateMatrix(pMatrix);
				pChild = pChild->GetNextSibling();
			}
			else
			{
				return;
			}
		}
	}	
	return;
}

void UITransition_Serial::UpdateMatrixToFinal(Matrix34* pMatrix)
{
	Matrix34 matrix;

	if (!m_bIsDisabled)
	{
		UIObject* pChild = m_pFirstChild;
		while(pChild)
		{
			//apply any modified matrices from the start state to the current one
			UITransition* pState = verify_cast<UITransition*>(pChild);

			pState->UpdateMatrixToFinal(pMatrix);
			pChild = pChild->GetNextSibling();

			UITransition* pNextState = verify_cast<UITransition*>(pChild);		
			if (pNextState)
			{
				//since we want to use distance values based off the target, then we need to calculate 
				//it due to previous transitions changing it
				pNextState->SetStartMatrix(pMatrix);//update the next transition so that it takes account any change in position               				
				pNextState->SetMatrix(pMatrix);//set the start pos to the prev states current pos
			}
		}
	}	
	return;
}

void UITransition_Serial::Reset()
{
	UITransition::Reset();

	m_FocusedChildIndex = 0;
}

void UITransition_Serial::OnActivate()
{
	UITransition::OnActivate();    

	SetComplete(false);

	//OnActivate first serial child      
	m_pFirstChild->Activate();
	m_RealElapsedTime = 0.0f;

	SetTransitionTime(GetTotalTime());
}

void UITransition_Serial::OnDeactivate()
{
	UITransition::OnDeactivate();    

	Reset();
}

void UITransition_Serial::OnFocused()
{
	UITransition::OnFocused();    

	//OnActivate first serial child      
	m_pFirstChild->SetFocused(true);
}

void UITransition_Serial::OnShow()
{
	UITransition::OnShow();    

	//OnActivate first serial child      
	m_pFirstChild->SetVisible(true);
}

void UITransition_Serial::Deactivate()
{
	//OnDeactivate all children
	for (UIObject* pChild=verify_cast<UIObject*>(m_pFirstChild); pChild!=NULL; pChild = verify_cast<UIObject*>(pChild->GetNextSibling()))	
	{
		pChild->Deactivate();
	}

	UITransition::Deactivate();
}

void UITransition_Serial::OnUnFocused()
{
	//OnDeactivate all children
	for (UIObject* pChild=verify_cast<UIObject*>(m_pFirstChild); pChild!=NULL; pChild = verify_cast<UIObject*>(pChild->GetNextSibling()))	
	{
		pChild->SetFocused(false);
	}	

	UITransition::OnUnfocused();
}

void UITransition_Serial::SetVisible(bool b)
{
	UITransition* pState = verify_cast<UITransition*>(GetChild(m_FocusedChildIndex));
	pState->SetVisible(b);
	UITransition::SetVisible(b);
}

void UITransition_Serial::PostLoad()
{      
	int numChildren = GetNumberOfChildren();
	//setup children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
		UITransition* pTransition = (verify_cast<UITransition*>(pChild));      

		//default the transition time to all children
		if (GetTransitionTime() != -1 && pTransition->GetTransitionTime() == -1.0f)//don't overwrite a child that is setting his own time
		{         
			pTransition->SetTransitionTime(m_TransitionTime/numChildren);
		}
	}
	UITransition::PostLoad();//post load children after parent
}

void UITransition_Serial::GetVelocity(Vector3* pVelocity)
{
   //sync my velocity with childs velocities
   UIObject* pChild = m_pFirstChild;
   while(pChild)
   {
      //just use the last childs velocity
      if (pChild->GetNextSibling() == NULL)
      {                 
         (verify_cast<UITransition*>(pChild))->GetVelocity(pVelocity);
      }
      pChild = pChild->GetNextSibling();
   }
}

void UITransition_Serial::GetRotationalVelocity(f32* pVelocity)
{
   //sync my velocity with childs velocities
   UIObject* pChild = m_pFirstChild;
   while(pChild)
   {
      //just use the last childs velocity
      if (pChild->GetNextSibling() == NULL)
      {                 
         (verify_cast<UITransition*>(pChild))->GetRotationalVelocity(pVelocity);
      }
      pChild = pChild->GetNextSibling();
   }
}

void UITransition_Serial::GetAcceleration(Vector3* pAcceleration)
{
	//sync my velocity with childs velocities
	UIObject* pChild = m_pFirstChild;
	while(pChild)
	{
		//just use the last childs velocity
		if (pChild->GetNextSibling() == NULL)
		{                 
			(verify_cast<UITransition*>(pChild))->GetAcceleration(pAcceleration);
		}
		pChild = pChild->GetNextSibling();
	}
}

void UITransition_Serial::GetPreviousVelocity(Vector3* pVelocity)
{
	//sync my velocity with childs velocities
	UIObject* pChild = m_pFirstChild;
	while(pChild)
	{
		//just use the last childs velocity
		if (pChild->GetNextSibling() == NULL)
		{                 
			(verify_cast<UITransition*>(pChild))->GetPreviousVelocity(pVelocity);
		}
		pChild = pChild->GetNextSibling();
	}
}

void UITransition_Serial::GetPreviousRotationalVelocity(f32* pVelocity)
{
	//sync my velocity with childs velocities
	UIObject* pChild = m_pFirstChild;
	while(pChild)
	{
		//just use the last childs velocity
		if (pChild->GetNextSibling() == NULL)
		{                 
			(verify_cast<UITransition*>(pChild))->GetPreviousRotationalVelocity(pVelocity);
		}
		pChild = pChild->GetNextSibling();
	}
}

void UITransition_Serial::GetPreviousAcceleration(Vector3* pAcceleration)
{
	//sync my velocity with childs velocities
	UIObject* pChild = m_pFirstChild;
	while(pChild)
	{
		//just use the last childs velocity
		if (pChild->GetNextSibling() == NULL)
		{                 
			(verify_cast<UITransition*>(pChild))->GetPreviousAcceleration(pAcceleration);
		}
		pChild = pChild->GetNextSibling();
	}
}

void UITransition_Serial::SetTargetVelocity(rage::Vector3* v)
{
	UIObject* pChild = m_pFirstChild;
	UIObject* pLastChild = pChild;
	while(pChild)
	{
		pLastChild = pChild;
		(verify_cast<UITransition*>(pChild))->SetTargetVelocityVector(v);
		pChild = pChild->GetNextSibling();
	}

	UITransition* pTransition = verify_cast<UITransition*>(pLastChild);	
	pTransition->SetTargetVelocity(v);
}

void UITransition_Serial::SetLookAtTargetOffset(rage::f32 f)
{
	UIObject* pChild = m_pFirstChild;
	while(pChild)
	{
		(verify_cast<UITransition*>(pChild))->SetLookAtTargetOffset(f);
		pChild = pChild->GetNextSibling();
	}
}

void UITransition_Serial::SetVelocity(rage::Vector3* v)
{
	UITransition* pTransition = verify_cast<UITransition*>(GetChild(m_FocusedChildIndex));	
	pTransition->SetVelocity(v);
}

void UITransition_Serial::SetPreviousVelocity(rage::Vector3* v)
{
	UITransition* pTransition = verify_cast<UITransition*>(GetChild(m_FocusedChildIndex));	
	pTransition->SetPreviousVelocity(v);
}

void UITransition_Serial::SetRotationalVelocity(f32 v)
{
	UITransition* pTransition = verify_cast<UITransition*>(GetChild(m_FocusedChildIndex));	
	pTransition->SetRotationalVelocity(v);
}

void UITransition_Serial::SetPreviousRotationalVelocity(f32 v)
{
	UITransition* pTransition = verify_cast<UITransition*>(GetChild(m_FocusedChildIndex));	
	pTransition->SetPreviousRotationalVelocity(v);
}

void UITransition_Serial::SetPreviousAcceleration(rage::Vector3* v)
{
	UITransition* pTransition = verify_cast<UITransition*>(GetChild(m_FocusedChildIndex));	
	pTransition->SetPreviousAcceleration(v);
}

void UITransition_Serial::SetAcceleration(rage::Vector3* v)
{
	UITransition* pTransition = verify_cast<UITransition*>(GetChild(m_FocusedChildIndex));	
	pTransition->SetAcceleration(v);
}

//void UITransition_Serial::SetTargetPosition(rage::Vector3* pPosition)
//{
//	UITransition* pTransition = verify_cast<UITransition*>(GetChild(m_FocusedChildIndex));	
//	pTransition->SetTargetPosition(pPosition);
//}

void UITransition_Serial::SetStartMatrix(const Matrix34* pStartPos)
{
	if (pStartPos)
	{
		m_StartPos.Set(*pStartPos);

		for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
		{
			(verify_cast<UITransition*>(pChild))->SetStartMatrix(pStartPos);
		}
	}
}

void UITransition_Serial::SetFinalMatrix(const Matrix34* pFinalPos)
{   
	if (pFinalPos)
	{
		m_FinalPos.Set(*pFinalPos);
		for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
		{
			(verify_cast<UITransition*>(pChild))->SetFinalMatrix(&m_FinalPos);
		}
	}
}

void UITransition_Serial::SetTarget(Matrix34* pTarget)
{  
	if (pTarget)
	{
		m_pTarget = pTarget;

		m_bIsRoot = true;

		SetMatrix(pTarget);

		for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
		{
			verify_cast<UITransition*>(pChild)->SetMatrix(pTarget);
		}
	}
}

rage::f32 UITransition_Serial::GetTotalTime()
{
	f32 maxTime= 0.0f;
	for (UIObject* pChild=GetChild(0); pChild; pChild = pChild->GetNextSibling())
	{
		maxTime += (verify_cast<UITransition*>(pChild))->GetTransitionTime();
	}

	return maxTime;
}

void UITransition_Serial::Update(float dt)
{  
	//change dt to use the set timestamp
	if (m_Timestep)
	{
		dt = m_Timestep;
	}

	if (m_bIsDisabled)
	{
		SetComplete(true);
		return;
	}

	if (IsComplete())
	{
		m_TimeTakenThisFrame = 0.0f;
		m_PercentComplete = 1.0f;
		return;
	}

   //UITransition::Update(dt); UPDATING BELOW
	Matrix34 matrix;
	Vector3 velocity;
	f32 rotVelocity;

	f32 amplifiedDT = dt;
	f32 totalTime = (m_TransitionTime+m_DelayTime);
	m_RealElapsedTime += dt;

	m_PercentComplete =(m_RealElapsedTime-m_DelayTime)/m_TransitionTime;
	if(m_PercentComplete<0.0f){m_PercentComplete=0.0f;}

	if (m_bEaseInOut)
	{
		//get the current amplified time slice
		amplifiedDT = (totalTime*SlowInOut(m_PercentComplete)) - m_ElapsedTime;
	}
	else
	{		
		amplifiedDT = dt;
	}

	m_ElapsedTime +=amplifiedDT;

	if (m_ElapsedTime<m_DelayTime)
	{
		return;
	}

	UITransition* pState = verify_cast<UITransition*>(GetChild(m_FocusedChildIndex));
	UITransition* pNextState = verify_cast<UITransition*>(pState->GetNextSibling());		

	if (!IsComplete())
	{
		if (pState)
		{
			UITransition::Update(amplifiedDT);

			if (pState->IsComplete())//has our active child state already completed
			{
				pState->SetFocused(false);
				pState->SetVisible(false);
				pState->Deactivate();

				if (pNextState)
				{            
					//since we want to use distance values based off the target, then we need to calculate 
					//it due to previous transitions changing it
					matrix.Set(m_StartPos);
					UpdateMatrix(&matrix);//should be the current camera matrix with respect to this transition
					pNextState->SetStartMatrix(&matrix);//update the next transition so that it takes account any change in position               				

					pNextState->SetMatrix(pState->GetMatrix());//set the start pos to the prev states current pos
					velocity.Zero();
					pState->GetVelocity(&velocity);
					pNextState->SetVelocity(&velocity);
					velocity.Zero();
					pState->GetPreviousVelocity(&velocity);
					pNextState->SetPreviousVelocity(&velocity);
					rotVelocity = 0.0f;
					pState->GetRotationalVelocity(&rotVelocity);
					pNextState->SetRotationalVelocity(rotVelocity);
					rotVelocity = 0.0f;
					pState->GetPreviousRotationalVelocity(&rotVelocity);
					pNextState->SetPreviousRotationalVelocity(rotVelocity);
					velocity.Zero();
					pState->GetAcceleration(&velocity);
					pNextState->SetAcceleration(&velocity);               				
					velocity.Zero();
					pState->GetPreviousAcceleration(&velocity);
					pNextState->SetPreviousAcceleration(&velocity);

					pNextState->Activate();
					//pNextState->Begin();
					pNextState->Activate();
					pNextState->SetVisible(true);
					pNextState->SetFocused(true);

					f32 timeLeft = pState->GetTimeTakenThisFrame();
					if (amplifiedDT - timeLeft > VERY_LOW_DT)
					{
						pNextState->Update(amplifiedDT - timeLeft);
					}

					m_FocusedChildIndex++;
					if (m_FocusedChildIndex == GetNumberOfChildren())
					{
						m_FocusedChildIndex = 0;
					}
				}
				else
				{
					//no more transitions to process					
					
					//adjust elapsed time depending on if all of dt was used
					m_ElapsedTime -= dt;
					m_ElapsedTime += GetTimeTakenThisFrame();

					SetComplete(true);
				}
			}
		}
	}
	else
	{
		//keep updating our last transition even though it is complete.  
		//this is so transitions like "look at target" can keep looking at the target
		pState->Update(amplifiedDT);
	}

	//sync the targets matrix with this matrix
	if (m_pTarget)
	{
		m_pTarget->Set(m_StartPos);
		UpdateMatrix(m_pTarget);
	}

#if __DEV
	m_TimeTakenThisFrameSUM += m_TimeTakenThisFrame;
#endif

}

UITransition_Translate_Axis::UITransition_Translate_Axis() : UITransition()
{
	m_PreviousVelocity = 0.0f;
	m_CurrentVelocity = 0.0f;
	m_TargetVelocity = 0.0f;

	m_PreviousAcceleration = 0.0f;
	m_CurrentAcceleration = 0.0f;

	m_PreviousPosition = 0.0f;
	m_CurrentPosition = 0.0f;
	m_TargetPosition = 0.0f;
	m_InitialTargetPosition = 0.0f;	
	
	m_Axis = X_AXIS;

	m_bAccelerate = false;
	m_bDecelerate = false;
	m_bInvert = false;
	m_bIsAdding = true;
	m_bIsWorld = false;
	m_bUseConstantAcceleration = true;

	m_DecelerationPoint = .5f;
	m_RealElapsedTime = 0.0f;
	m_bIncreaseSteps = false;
	m_MidPointPosition = 0.0f;

	m_TargetVelocityVector.Zero();
}

#if __BANK
void UITransition_Translate_Axis::AddWidgets( rage::bkBank & bk ) 
{	
	bk.AddSlider( "Target Velocity", &m_TargetVelocity, -10000.0f, 10000.0f, 10.0f );
	bk.AddSlider( "Initial Target Position", &m_InitialTargetPosition, -10000.0f, 10000.0f, 10.0f );
	UITransition::AddWidgets(bk);
}
#endif

void UITransition_Translate_Axis::Reset()
{
	UITransition::Reset();

	m_CurrentPosition = 0.0f;
	m_PreviousPosition = 0.0f;
	m_CurrentVelocity = 0.0f;
	m_PreviousVelocity = 0.0f;
	m_CurrentAcceleration = 0.0f;
	m_PreviousAcceleration = 0.0f;
	m_bUseConstantAcceleration = true;
	m_DecelerationPoint = .5f;
	m_RealElapsedTime = 0.0f;
	m_MidPointPosition = 0.0f;
}

bool UITransition_Translate_Axis::ProcessDataTag(const char* name , const char* value)
{
	if(stricmp(name,"target")==0)														  
	{
		m_InitialTargetPosition = (f32)atof(value);
		if (m_InitialTargetPosition <0 )
		{
			//m_InitialTargetPosition *= -1;
			m_bInvert = true;
		}
	}
	else if(stricmp(name,"world")==0)
	{
		m_bIsWorld = true;
	}
	else if(stricmp(name,"set")==0)
	{
		m_bIsAdding = false;
	}
	else if(stricmp(name,"axis")==0)
	{
		if(stricmp(value,"X")==0 || (stricmp(value,"x")==0))
		{
			m_Axis = X_AXIS;
		}
		else if(stricmp(value,"Y")==0 || (stricmp(value,"y")==0))
		{
			m_Axis = Y_AXIS;
		}
		else if(stricmp(value,"Z")==0 || (stricmp(value,"z")==0))
		{
			m_Axis = Z_AXIS;
		}
		else if(stricmp(value,"along_final_look")==0)
		{
			m_Axis = ALONG_FINAL_LOOK;
		}
		else if(stricmp(value,"final_look")==0)
		{
			m_Axis = PARALLEL_TO_FINAL_LOOK;
		}
		else if(stricmp(value,"final_side")==0)
		{
			m_Axis = FINAL_SIDE;
		}
		else if(stricmp(value,"start_look")==0)
		{
			m_Axis = START_LOOK;
		}
		else if(stricmp(value,"car_velocity")==0)
		{
			m_Axis = CAR_VELOCITY;
		}
		else if(stricmp(value,"target_velocity")==0)
		{
			m_Axis = TARGET_VELOCITY;
		}
		else if(stricmp(value,"start_to_final")==0)
		{
			m_Axis = START_TO_FINAL;
		}
	}
	else if(stricmp(name,"default")==0)
	{   
		m_bAccelerate = m_bDecelerate = false;
	}
	else if(stricmp(name,"decelerate")==0)
	{   
		m_bDecelerate = true;
	}
	else if(stricmp(name,"accelerate")==0)
	{   
		m_bAccelerate = true;
	}
	else if(stricmp(name,"target_velocity")==0)
	{      
		m_TargetVelocity = (f32)atof(value);
	}
	else if(stricmp(name,"total_time")==0)
	{
		if (m_bIncreaseSteps)
		{
			SetTransitionTime((f32)atof(value)*3.0f);
		}
		else
		{
			SetTransitionTime((f32)atof(value));
		}
		
		if (m_RealTotalTime <= 0.1f)
		{
			m_RealTotalTime = GetTransitionTime();
		}
	}
	else if(stricmp(name,"delay_start_time")==0)
	{		
		if (m_bIncreaseSteps)
		{
			SetDelayTime((f32)atof(value)*3.0f);
		}
		else
		{
			SetDelayTime((f32)atof(value));
		}
	}
	else
	{
		return UITransition::ProcessDataTag(name,value);
	}

	return true;
}

//convenience function to set a variable
void UITransition_Translate_Axis::SetValue(f32* variable , Vector3* pValue)
{
	if (pValue)
	{
		switch(m_Axis)
		{
		case X_AXIS:
			{
				*variable = pValue->x;
			}
			break;
		case Y_AXIS:
			{
				*variable = pValue->y;
			}
			break;
		case Z_AXIS:
			{
				*variable = pValue->z;
			}
			break;
		case START_LOOK:
			{
				Vector3 normalizedLook;
				Vector3 normalizedVelocity;
				normalizedLook.Set(m_StartPos.c);
				normalizedLook.Normalize();
				normalizedVelocity.Set(*pValue);
				*variable = normalizedLook.Dot(normalizedVelocity);
			}
			break;
		case START_TO_FINAL:
			{
				//lets create a neutral point
				Vector3 distanceVec;
				Matrix34 newFinalPos(m_FinalPos);
				distanceVec.Subtract(m_FinalPos.d,m_StartPos.d);
				distanceVec.y = 0.0f;
				distanceVec.Normalize();

				*variable = distanceVec.Dot(*pValue);
			}
			break;
		case PARALLEL_TO_FINAL_LOOK:
			{
				Vector3 splatLook;
				Vector3 normalizedLook;
				Vector3 normalizedVelocity;
				splatLook.Set(m_FinalPos.c);
				splatLook.y=0.0f;
				splatLook.Normalize();
				normalizedLook.Set(splatLook);
				normalizedVelocity.Set(*pValue);
				*variable = normalizedLook.Dot(normalizedVelocity);
			}
			break;
		case ALONG_FINAL_LOOK:
			{
				Matrix34 target;
				Vector3 normalizedLook;
				target.Set(m_FinalPos);

				//find the position along the look at vector that is m_InitialTargetPosition away
				TraverseAlongVector(&m_FinalPos.c,&target.d,m_InitialTargetPosition);

				//get the direction
				normalizedLook.Subtract(target.d, m_StartPos.d);
				normalizedLook.Normalize();

				//apply velocity along that direction
				*variable = normalizedLook.Dot(*pValue);
			}
			break;
		case TARGET_VELOCITY:
			{
				Vector3 normalizedLook;
				Vector3 normalizedVelocity;
				normalizedLook.Set(m_TargetVelocityVector);
				normalizedLook.Normalize();
				normalizedVelocity.Set(*pValue);
				*variable = normalizedLook.Dot(normalizedVelocity);
			}
			break;
		case FINAL_SIDE:
			{
				Vector3 normalizedSide;
				Vector3 normalizedVelocity;
				normalizedSide.Set(m_FinalPos.a);
				normalizedSide.Normalize();
				normalizedVelocity.Set(*pValue);
				*variable = normalizedSide.Dot(normalizedVelocity);
			}
			break;
// 		case CAR_VELOCITY:
// 			{
// // 				Vector3 normalizedCarVelocity;
// // 				Vector3 normalizedVelocity;
// // 				normalizedCarVelocity.Set(*UILOGIC.GetVelocitySample());
// // 				normalizedCarVelocity.Normalize();
// // 				normalizedVelocity.Set(*pValue);
// // 				*variable = normalizedCarVelocity.Dot(normalizedVelocity);
// 			}
//			break;
		default:break;
		}
	}
}


void UITransition_Translate_Axis::FindValue(Axis axis , f32 value , Vector3* pVariable)
{
	if (pVariable)
	{
		pVariable->Zero();//since we are getting not adding

		switch (axis)
		{
		case X_AXIS:
			{
				pVariable->x = value;
			}
			break;
		case Y_AXIS:
			{
				pVariable->y = value;
			}
			break;
		case Z_AXIS:
			{
				pVariable->z = value;
			}
			break;
		case START_LOOK:
			{
				TraverseAlongVector(&m_StartPos.c,pVariable,value);
			}
			break;
		case START_TO_FINAL:
			{
				//lets create a neutral point
				Vector3 distanceVec;
				distanceVec.Subtract(m_FinalPos.d,m_StartPos.d);
				distanceVec.y = 0.0f;
				f32 distance = distanceVec.Mag();
				distanceVec.Normalize();
				distanceVec.AddScaled(distanceVec,distance-m_InitialTargetPosition);//go towards the light but stop short
				TraverseAlongVector(&distanceVec,pVariable,value);

			}
			break;
		case PARALLEL_TO_FINAL_LOOK:
			{
				Vector3 splatLook;
				splatLook.Set(m_FinalPos.c);
				splatLook.y=0.0f;
				splatLook.Normalize();
				TraverseAlongVector(&splatLook,pVariable,value);
			}
			break;
		case ALONG_FINAL_LOOK:
			{
				Vector3 normalizedLook,splat,target;
				target.Set(m_FinalPos.d);
				splat.Set(m_FinalPos.c);
				splat.y=0.0f;
				target.y=0.0f;

				//find the position along the look at vector that is m_InitialTargetPosition away
				TraverseAlongVector(&splat,&target,m_InitialTargetPosition);

				//get the direction
				splat.Set(m_StartPos.d);
				splat.y=0.0f;
				normalizedLook.Subtract(target, splat);
				normalizedLook.Normalize();

				//find the position along the look at vector that is m_InitialTargetPosition away				
				TraverseAlongVector(&normalizedLook,pVariable,value);
			}
			break;
		case TARGET_VELOCITY:
			{
				TraverseAlongVector(&m_TargetVelocityVector,pVariable,value);
			}
			break;
		case FINAL_SIDE:
			{
				TraverseAlongVector(&m_FinalPos.a,pVariable,value);
			}
			break;
// 		case CAR_VELOCITY:
// 			{
//  				TraverseAlongVector(UILOGIC.GetVelocitySample(),pVariable,value);
// 			}
// 			break;
		default:break;
		}
	}
}

void UITransition_Translate_Axis::AddValue(Axis axis , f32 value , Vector3* pVariable)
{
	if (pVariable)
	{
		switch (axis)
		{
		case X_AXIS:
			{
				pVariable->x += value;
			}
			break;
		case Y_AXIS:
			{
				pVariable->y += value;
			}
			break;
		case Z_AXIS:
			{
				pVariable->z += value;
			}
			break;
		case PARALLEL_TO_FINAL_LOOK:
			{
				Vector3 splatLook;
				splatLook.Set(m_FinalPos.c);
				splatLook.y=0.0f;
				splatLook.Normalize();
				TraverseAlongVector(&splatLook,pVariable,value);
			}
			break;
		case ALONG_FINAL_LOOK:
			{
				Vector3 normalizedLook,splat,target;
				target.Set(m_FinalPos.d);
				splat.Set(m_FinalPos.c);
				splat.y=0.0f;
				target.y=0.0f;
				splat.Normalize();

				//find the position along the look at vector that is m_InitialTargetPosition away
				target.AddScaled(splat,m_InitialTargetPosition);

				//get the direction
				splat.Set(m_StartPos.d);
				splat.y=0.0f;
				normalizedLook.Subtract(target,splat);
				normalizedLook.Normalize();

				//find the position along the look at vector that is m_InitialTargetPosition away
				TraverseAlongVector(&normalizedLook,pVariable,value);
			}
			break;
		case TARGET_VELOCITY:
			{
				TraverseAlongVector(&m_TargetVelocityVector,pVariable,value);
			}
			break;
		case START_LOOK:
			{
				TraverseAlongVector(&m_StartPos.c,pVariable,value);
			}
			break;
		case START_TO_FINAL:
			{
				//lets create a neutral point
				Vector3 distanceVec;
				distanceVec.Subtract(m_FinalPos.d,m_StartPos.d);
				distanceVec.y = 0.0f;
				distanceVec.Normalize();
				TraverseAlongVector(&distanceVec,pVariable,value);

			}
			break;
		case FINAL_SIDE:
			{
				TraverseAlongVector(&m_FinalPos.a,pVariable,value);
			}
			break;
// 		case CAR_VELOCITY:
// 			{
// 				TraverseAlongVector(UILOGIC.GetVelocitySample(),pVariable,value);
// 			}
// 			break;
		default:break;
		}
	}
}

void UITransition_Translate_Axis::SetVelocity(Vector3* pVelocity)
{		
	SetValue(&m_CurrentVelocity , pVelocity);

	if (m_bIncreaseSteps)
	{
		m_CurrentVelocity *= .33f;
	}
}

void UITransition_Translate_Axis::SetVelocity(rage::f32 velocity)
{
	m_CurrentVelocity = velocity;

	if (m_bIncreaseSteps)
	{
		m_CurrentVelocity *= .33f;
	}
}

void UITransition_Translate_Axis::SetTargetVelocityVector(Vector3* pVelocity)
{	
	m_TargetVelocityVector.Set(*pVelocity);
}

void UITransition_Translate_Axis::SetTargetVelocity(Vector3* pVelocity)
{	
	m_TargetVelocityVector.Set(*pVelocity);

	SetValue(&m_TargetVelocity , pVelocity);
	
	if (m_bIncreaseSteps)
	{
		m_TargetVelocity *= .33f;
	}
}

void UITransition_Translate_Axis::GetVelocity(Vector3* pVelocity)
{	
	if (m_bIncreaseSteps)
	{
		f32 temp = m_CurrentVelocity*3;
		FindValue(m_Axis , temp , pVelocity);
	}
	else
	{
		FindValue(m_Axis , m_CurrentVelocity , pVelocity);
	}
}

void UITransition_Translate_Axis::SetPreviousVelocity(Vector3* pVelocity)
{	
	SetValue(&m_PreviousVelocity , pVelocity);
	if (m_bIncreaseSteps)
	{
		m_PreviousVelocity *= .33f;
	}
}

void UITransition_Translate_Axis::SetPreviousVelocity(rage::f32 velocity)
{	
	m_PreviousVelocity = velocity;

	if (m_bIncreaseSteps)
	{
		m_PreviousVelocity *= .33f;
	}
}

void UITransition_Translate_Axis::GetPreviousVelocity(Vector3* pVelocity)
{
	if (m_bIncreaseSteps)
	{
		f32 temp = m_PreviousVelocity*3;
		FindValue(m_Axis , temp , pVelocity);
	}
	else
	{
		FindValue(m_Axis , m_PreviousVelocity , pVelocity);
	}	
}

void UITransition_Translate_Axis::SetPreviousAcceleration(Vector3* pAcceleration)
{
	SetValue(&m_PreviousAcceleration , pAcceleration);
	if (m_bIncreaseSteps)
	{
		m_PreviousAcceleration *= .33f;
	}
}

void UITransition_Translate_Axis::SetPreviousAcceleration(rage::f32 acceleration)
{
	m_PreviousAcceleration = acceleration;

	if (m_bIncreaseSteps)
	{
		m_PreviousAcceleration *= .33f;
	}
}

void UITransition_Translate_Axis::GetPreviousAcceleration(Vector3* pAcceleration)
{
	if (m_bIncreaseSteps)
	{
		f32 temp = m_PreviousAcceleration*3;
		FindValue(m_Axis , temp , pAcceleration);
	}
	else
	{
		FindValue(m_Axis , m_PreviousAcceleration , pAcceleration);
	}	
}
void UITransition_Translate_Axis::SetAcceleration(Vector3* pAcceleration)
{
	SetValue(&m_CurrentAcceleration , pAcceleration);
	if (m_bIncreaseSteps)
	{
		m_CurrentAcceleration *= .33f;
	}
}

void UITransition_Translate_Axis::SetAcceleration(rage::f32 acceleration)
{
	m_CurrentAcceleration = acceleration;

	if (m_bIncreaseSteps)
	{
		m_CurrentAcceleration *= .33f;
	}
}

void UITransition_Translate_Axis::GetAcceleration(Vector3* pAcceleration)
{
	if (m_bIncreaseSteps)
	{
		f32 temp = m_CurrentAcceleration*3;
		FindValue(m_Axis , temp , pAcceleration);
	}
	else
	{
		FindValue(m_Axis , m_CurrentAcceleration , pAcceleration);
	}	
}

//travel along the source vector a distance of distance. store the result in dest
void UITransition_Translate_Axis::TraverseAlongVector(Vector3* source, Vector3* dest, f32 distance)
{
	if (source && dest)
	{
		Vector3 forwardVector;
		forwardVector.Set(*source);
		//forwardVector.y=0;//flatten out vector so that it does not effect the vertical coord
		forwardVector.Normalize();
		dest->AddScaled(forwardVector,distance);
	}
}

void UITransition_Translate_Axis::OnDeactivate()
{
	UITransition::OnDeactivate();

	m_PreviousVelocity = 0.0f;
	m_CurrentVelocity = 0.0f;

	m_PreviousAcceleration = 0.0f;
	m_CurrentAcceleration = 0.0f;

	m_PreviousPosition = 0.0f;
	m_CurrentPosition = 0.0f;

	m_ElapsedTime = 0.0f;	
	m_RealElapsedTime = 0.0f;	
	m_TimeTakenThisFrame = 0.0f;

	m_PercentComplete = 0.0f;
	m_PercentDistance = 0.0f;
}

void UITransition_Translate_Axis::OnActivate()
{
	SetComplete(false);

	//we need to calculate the target position
	if (m_bIsAdding)
	{
		switch (m_Axis)
		{
			case X_AXIS:
			{
				m_TargetPosition = m_FinalPos.d.x - m_StartPos.d.x;
				m_TargetPosition += m_InitialTargetPosition;
			}
			break;
			case Y_AXIS:
			{
				m_TargetPosition = m_FinalPos.d.y - m_StartPos.d.y;
				m_TargetPosition += m_InitialTargetPosition;
			}
			break;
			case Z_AXIS:
			{
				m_TargetPosition = m_FinalPos.d.z - m_StartPos.d.z;
				m_TargetPosition += m_InitialTargetPosition;
			}
			break;
			case PARALLEL_TO_FINAL_LOOK:
			{
				m_TargetPosition = m_InitialTargetPosition;
			}
			break;
			case START_TO_FINAL:
				{
					//lets create a neutral point
					Vector3 distanceVec;
					Matrix34 newFinalPos(m_FinalPos);
					distanceVec.Subtract(m_FinalPos.d,m_StartPos.d);
					distanceVec.y = 0.0f;
					m_TargetPosition = distanceVec.Mag();
					if (m_TargetPosition < 0)
					{
						m_TargetPosition -= m_InitialTargetPosition;
					}
					else
					{
						m_TargetPosition += m_InitialTargetPosition;
					}

				}
				break;
			case ALONG_FINAL_LOOK:
				{
					Vector3 splat,target,distance;
					target.Set(m_FinalPos.d);
					splat.Set(m_FinalPos.c);
					splat.y=0.0f;
					target.y=0.0f;
					splat.Normalize();

					//find the position along the look at vector that is m_InitialTargetPosition away
					TraverseAlongVector(&splat,&target,m_InitialTargetPosition);

					splat.Set(m_StartPos.d);
					splat.y=0.0f;

					distance.Subtract(target, splat);
					m_TargetPosition = distance.Mag();
				}
				break;
			case FINAL_SIDE:
			{
				m_TargetPosition = 0.0f;
			}
			break;
// 			case CAR_VELOCITY:
// 				{
// 					m_TargetPosition = 0.0f;
// 				}
// 			break;

			default:break;
		}		
	}
	else
	{
		m_TargetPosition = m_InitialTargetPosition;
	}

	if (m_bIsWorld)
	{
		m_TargetPosition =	m_InitialTargetPosition - m_StartPos.d.y;
	}

	m_MidPointPosition = 0.0f;
	m_InitialVelocity = m_CurrentVelocity;
	m_MidPointVelocity = (2.0f*m_TargetPosition / m_TransitionTime) - (m_CurrentVelocity*.5f) - (m_TargetVelocity*.5f);
	m_ConstantAcceleration = (2.0f*(m_MidPointVelocity - m_CurrentVelocity) ) / m_TransitionTime;
	m_ConstantDeceleration = (2.0f*(m_TargetVelocity - m_MidPointVelocity) ) / m_TransitionTime;

	UITransition::OnActivate();
}

f32 UITransition_Translate_Axis::TimeToTakeThisFrame(f32 dt)
{
	return TimeToTakeThisFrame(dt , (m_TransitionTime+m_DelayTime) - m_ElapsedTime , m_DelayTime , m_ElapsedTime , m_CurrentPosition , m_TargetPosition , m_CurrentVelocity);
}

f32 UITransition_Translate_Axis::TimeToTakeThisFrame(f32 dt, f32 timeleft , f32 , f32  , f32 currentPosition , f32 targetPosition , f32 currentVelocity)
{
	const f32 CONSIDERED_COMPLETE = .01f;

	//invert so distance is always positive
	if (targetPosition < 0)
	{
		targetPosition *= -1;	
		currentPosition *= -1;
		currentVelocity *= -1;
	}

	f32 distance = targetPosition - currentPosition;

	//test distance
	if (distance <= CONSIDERED_COMPLETE)
	{
		return 0.0f;
	}

	//test velocity when time is up
	if (timeleft < 0 && currentVelocity==0)
	{
		return dt;
	}

	//test if velocity may be zero, but could still be accelerating
	if (timeleft > 0 && currentVelocity==0)
	{
		return dt;
	}

	//if velocity is in the opposite direction of the distance
	if (distance > 0 && currentVelocity < 0)
	{
		return dt;
	}

	//return time in the range from 0 to dt
	f32 time =  distance / currentVelocity;
	if (time > dt )
	{
		return dt;
	}

	return time;
}

f32 UITransition_Translate_Axis::CalcAcceleration(f32 )
{
	f32 distance;
	f32 timeLeft;
	
	if (m_bUseConstantAcceleration)
	{
		return m_ConstantAcceleration;
	}
	else
	{
		if (m_bAccelerate)
		{
			//we are accelerating the whole way
			distance = m_TargetPosition - m_CurrentPosition;
			timeLeft = (m_TransitionTime+m_DelayTime) - m_ElapsedTime;
		}
		else
		{
			//only accelerate to half way if we are accelerating then decelerating
			distance = (m_TargetPosition*.5f) - m_CurrentPosition;
			timeLeft = ((m_TransitionTime*.5f)+m_DelayTime) - m_ElapsedTime;
		}

		return 2.0f*(distance-m_CurrentVelocity*timeLeft)/square(timeLeft);
	}
}

f32 UITransition_Translate_Axis::CalcVelocity_UsingRungeKutta( f32 dt , f32 time , f32 distance)
{	
	f32 squareTime = square(time);

	//use Runge Kutta method for accuracy
	f32 k1 = dt * ( 2.0f * ( distance - ( m_CurrentVelocity * time ) / squareTime ) );
	f32 k2 = dt * ( 2.0f * ( distance - (( m_CurrentVelocity + k1 * .5f ) * ( time + dt * .5f ) ) / squareTime ) );
	f32 k3 = dt * ( 2.0f * ( distance - (( m_CurrentVelocity + k2 * .5f ) * ( time + dt * .5f ) ) / squareTime ) );
	f32 k4 = dt * ( 2.0f * ( distance - (( m_CurrentVelocity + k3 ) * ( time + dt ) ) / squareTime ) );

	f32 velocityInc = CalcDeceleration(dt) * dt;
	velocityInc = (k1 + 2.0f*k2 + 2.0f*k3 + k4 ) / 6;
	m_CurrentVelocity = m_CurrentVelocity + velocityInc;

	return m_CurrentVelocity;
}

//f32 UITransition_Translate_Axis::CalcPosition( f32 dt , f32 time , f32 distance)
//{
//	return m_PreviousPosition + CalcVelocity(dt , time , distance) * dt;
//}

f32 UITransition_Translate_Axis::CalcDeceleration(f32 )
{
	//f32 distance = (m_TargetPosition-m_CurrentPosition);

	//if (fabs(distance) > 0.001f)
	//{
	//	return 0.5f*(square(m_TargetVelocity)-square(m_CurrentVelocity))/distance;
	//}
	//else
	//{
	//	return 0.0f;
	//}

	if (m_bUseConstantAcceleration)
	{
		return m_ConstantDeceleration;
	}
	else
	{
		f32 distance;
		f32 timeLeft;

		//we are accelerating the whole way
		distance = m_TargetPosition - m_CurrentPosition;
		timeLeft = (m_TransitionTime+m_DelayTime) - m_ElapsedTime;

		return 2.0f*(distance-m_CurrentVelocity*timeLeft)/square(timeLeft);
	}
}

bool UITransition_Translate_Axis::Integrate(f32 dt)
{	
	bool isComplete = false;
	// f32 distancePercent;//percent finished with respect to position
	//f32 idealInitialVelocity;
	
	//given our current velocity, are we going to complete in less than dt?
	m_TimeTakenThisFrame = TimeToTakeThisFrame(dt);
	
	if (m_TimeTakenThisFrame < dt || (m_ElapsedTime > VERY_LOW_DT && dt < VERY_LOW_DT))
	{
		isComplete = true;	
	}

	m_PreviousVelocity = m_CurrentVelocity;
	m_PreviousPosition = m_CurrentPosition;
	m_PreviousAcceleration = m_CurrentAcceleration;		

	//accelerate for the first half then decelerate
	// distancePercent = (m_TargetPosition - m_CurrentPosition) / m_TargetPosition;
	if  (m_bAccelerate || (((m_TransitionTime*.5f)+m_DelayTime) - m_ElapsedTime > 0.0f))
	{			
		//keep accelerating until we reach exactly m_TransitionTime/2
		f32 timeLeft =  TimeToTakeThisFrame(
			m_TimeTakenThisFrame , 
			((m_TransitionTime*.5f)+m_DelayTime) - m_ElapsedTime,
			m_DelayTime,
			m_ElapsedTime,
			m_CurrentPosition,
			m_TargetPosition*.5f,
			m_CurrentVelocity);

		m_CurrentAcceleration = CalcAcceleration(timeLeft);
		//m_CurrentVelocity = idealInitialVelocity + m_CurrentAcceleration*timeLeft;//use average acceleration			
		//idealInitialVelocity = m_CurrentPosition * m_ElapsedTime;
		//m_CurrentPosition += (idealInitialVelocity+m_CurrentVelocity)*.5f*timeLeft;//add distance traveled since last update

		m_CurrentPosition = m_InitialVelocity*((m_ElapsedTime+timeLeft)-m_DelayTime) + .5f*m_CurrentAcceleration*square((m_ElapsedTime+timeLeft)-m_DelayTime);

		//do we need to decelerate any?
		if (timeLeft < m_TimeTakenThisFrame)
		{
			m_PreviousVelocity = m_CurrentVelocity;
			m_PreviousPosition = m_CurrentPosition;
			m_PreviousAcceleration = m_CurrentAcceleration;		

			//decelerate with the rest of the time
			m_CurrentAcceleration = CalcDeceleration(m_TimeTakenThisFrame - timeLeft);
			m_CurrentVelocity += m_CurrentAcceleration*(m_TimeTakenThisFrame - timeLeft);//use average acceleration			
			m_CurrentPosition += ((m_PreviousVelocity+m_CurrentVelocity)*.5f)*(m_TimeTakenThisFrame - timeLeft);//add distance traveled since last update
		}
	}
	else
	{
		//decelerating
		m_CurrentAcceleration = CalcDeceleration(m_TimeTakenThisFrame);
		m_CurrentVelocity += m_CurrentAcceleration*m_TimeTakenThisFrame;//use average acceleration			
		m_CurrentPosition += m_CurrentVelocity*m_TimeTakenThisFrame;//add distance traveled since last update
	}				
	//}
	//else
	//{		
	//	if (m_TimeTakenThisFrame > dt || m_TimeTakenThisFrame < 0){m_TimeTakenThisFrame = 0;}//guard against very small floats

	//	if (!m_bAccelerate)
	//	{
	//		//only set to target velocity when decelerating
	//		//HAD TO COMMENT THIS OUT CAUSE THE ACCURACY OF GETTING TO THE FINAL VELOCITY WAS TOO LOW AND 
	//		//m_CurrentVelocity = m_TargetVelocity;
	//	}			

	//	m_CurrentPosition = m_TargetPosition;
	//	m_CurrentAcceleration = 0;
	//}

	return isComplete;
}

bool UITransition_Translate_Axis::IntegrateConstantAcceleration(f32 dt)
{	
	bool isComplete = false;
	f32 time;

	m_PreviousVelocity = m_CurrentVelocity;
	m_PreviousPosition = m_CurrentPosition;
	m_PreviousAcceleration = m_CurrentAcceleration;		

	//given our current velocity, are we going to complete in less than dt?
	m_TimeTakenThisFrame = dt;
		
	if (m_ElapsedTime > VERY_LOW_DT && dt < VERY_LOW_DT)
	{
		isComplete = true;	
	}

	//accelerate for the first half then decelerate
	f32 accelerationTimeLeft = ((m_TransitionTime*.5f)+m_DelayTime) - m_ElapsedTime;
	if  (m_bAccelerate || accelerationTimeLeft > 0.0f)
	{			
		if (accelerationTimeLeft < dt)
		{
			f32 decelerationTimeLeft = dt - accelerationTimeLeft;
			time = ((m_ElapsedTime+accelerationTimeLeft)-m_DelayTime);
			m_CurrentVelocity = m_InitialVelocity + m_ConstantAcceleration*time;
			m_CurrentPosition = m_InitialVelocity*time + .5f*m_ConstantAcceleration*square(time);

			m_PreviousVelocity = m_CurrentVelocity;
			m_PreviousPosition = m_CurrentPosition;
			m_PreviousAcceleration = m_CurrentAcceleration;		

			m_MidPointPosition = m_CurrentPosition;			

			//decelerate with the rest of the time			
			m_CurrentPosition = m_MidPointPosition + m_MidPointVelocity*(decelerationTimeLeft) + .5f*m_ConstantDeceleration*square(decelerationTimeLeft);
		}
		else
		{
			time = (m_ElapsedTime+dt)-m_DelayTime;
			m_CurrentVelocity = m_InitialVelocity + m_ConstantAcceleration*(m_ElapsedTime+dt);
			m_CurrentPosition = m_InitialVelocity*time + .5f*m_ConstantAcceleration*square(time);
		}
	}
	else
	{
		if (m_MidPointPosition == 0.0f){m_MidPointPosition = m_CurrentPosition;}//needed this cause we may end acceleration at the very end of a frame
		//decelerating
		//decelerationElapsedTime should be zero at the midpoint (V1/totalTime div 2)
		f32 decelerationTransitionTime = m_TransitionTime*.5f;
		f32 decelerationElapsedTime = m_ElapsedTime - (decelerationTransitionTime+m_DelayTime);
		
		time = decelerationElapsedTime+dt;

		//check to see if we are going to finish in the middle of this frame
		if ( time > decelerationTransitionTime)
		{
			time = decelerationTransitionTime;
			m_TimeTakenThisFrame = decelerationTransitionTime - decelerationElapsedTime;
		}
		m_CurrentVelocity = m_MidPointVelocity + m_ConstantDeceleration*time;
		m_CurrentPosition = m_MidPointPosition + m_MidPointVelocity*time + .5f*m_ConstantDeceleration*square(time);
	}				

	return isComplete;
}

void UITransition_Translate_Axis::Update(float dt)
{
	//change dt to use the set timestamp
	if (m_Timestep)
	{
		dt = m_Timestep;
	}

	Update3(dt);

	if(m_bIncreaseSteps)
	{
		Update3(dt);
		Update3(dt);
	}
}

void UITransition_Translate_Axis::Update3(float dt)
{
	if (m_bIsDisabled)
	{
		SetComplete(true);
		return;
	}

	if (IsComplete())
	{
		m_TimeTakenThisFrame = 0.0f;
		m_PercentComplete = 1.0f;
		return;
	}

	UITransition::Update(dt);		

	f32 amplifiedDT = dt;
	f32 totalTime = (m_TransitionTime+m_DelayTime);

	if (m_ElapsedTime>=m_DelayTime)
	{
		switch(m_MotionType)
		{
		case PHYSICS:
			{
				//check to see if we are complete			
				if (!m_bComplete) 
				{
					Process(amplifiedDT);
				}
			}
			break;
		case NONE:
			{
				m_ElapsedTime -= amplifiedDT;//time is not a factor when we are warping
				m_CurrentPosition = m_TargetPosition;
			}
			break;
		default:
			break;
		}
		
		if (m_pTarget)
		{
			Matrix34 matrix;
			Matrix34 targetMatrix;
			matrix.Set(m_StartPos);
			UpdateMatrix(&matrix);

			targetMatrix.Set(*m_pTarget);
			switch (m_Axis)
			{
				case X_AXIS:
				{
					targetMatrix.d.x = matrix.d.x;//sync the targets matrix with this matrix
				}
				break;
				case Y_AXIS:
				{
					targetMatrix.d.y = matrix.d.y;//sync the targets matrix with this matrix
				}
				break;
				case Z_AXIS:
				{
					targetMatrix.d.z = matrix.d.z;//sync the targets matrix with this matrix
				}
				break;
				case START_LOOK:
				case PARALLEL_TO_FINAL_LOOK:
				case ALONG_FINAL_LOOK:
				case FINAL_SIDE:
// 				case CAR_VELOCITY:
// 				{
// 					targetMatrix.d.Add(matrix.d);
// 				}
// 				break;
				default:break;
			}
			m_pTarget->Set(targetMatrix);
		}
	}

	m_RealElapsedTime += dt;
	m_PercentComplete =(m_RealElapsedTime-m_DelayTime)/m_TransitionTime;
	if(m_PercentComplete<0.0f){m_PercentComplete=0.0f;}

	if (m_bEaseInOut)
	{
		//get the current amplified time slice
		amplifiedDT = (totalTime*SlowInOut(m_PercentComplete)) - m_ElapsedTime;
	}
	else
	{		
		amplifiedDT = dt;
	}

	if (m_ElapsedTime + amplifiedDT + VERY_LOW_DT > totalTime || (m_ElapsedTime > VERY_LOW_DT && amplifiedDT < VERY_LOW_DT))
	{
		m_TimeTakenThisFrame = totalTime - m_ElapsedTime;
		m_ElapsedTime = totalTime;
		SetComplete(true);
	}
	else
	{
		m_ElapsedTime += amplifiedDT;
		m_TimeTakenThisFrame = amplifiedDT;
	}

#if __DEV
	m_TimeTakenThisFrameSUM += m_TimeTakenThisFrame;
#endif

}

void UITransition_Translate_Axis::Process(float dt)
{
	if (m_bUseConstantAcceleration)
	{
		IntegrateConstantAcceleration(dt);
	}
	else
	{
		Integrate(dt);
	}
}

void UITransition_Translate_Axis::UpdateMatrix(Matrix34* pMatrix)
{
	if (pMatrix && !m_bIsDisabled && m_ElapsedTime > m_DelayTime)
	{
		AddValue(m_Axis, m_CurrentPosition,&pMatrix->d);
	}
}

void UITransition_Translate_Axis::UpdateMatrixToFinal(Matrix34* pMatrix)
{
	//need to setup all my vars
	Activate();//PrepareToStart();

	if (pMatrix && !m_bIsDisabled)
	{
		//sync the targets matrix with this matrix
		AddValue(m_Axis, m_TargetPosition,&pMatrix->d);
	}
}

UITransition_Translate_Spline::UITransition_Translate_Spline() : UITransition_Translate_Axis()
{
	m_bXZOnly=true;
}

//bool UITransition_Translate_Spline::ProcessDataTag(char* name , char* value)
//{
//	if(stricmp(name,"vertex")==0)
//	{
//		char arg[32];
//
//		GetArgument(0,value,arg,32);
//		if(stricmp(name,"vertex")==0)
//		{
//
//		}
//	}
//	else
//	{
//		return UIObject::ProcessDataTag(hsm,cmd,name,value);
//	}
//
//	return true;
//}

void UITransition_Translate_Spline::PostLoad()
{
	UITransition_Translate_Axis::PostLoad();
}

float UITransition_Translate_Spline::GetStaticDistance()
{
	int previousSegment,currentSegment;
	f32 /*previousPosition,*/currentPosition,step,distance;
	rage::Vector3 previousPositionVector,currentPositionVector,temp;
	rage::Vector3 &v1 = m_CubicCurve.GetVertex(1);
	
	step = .033f;
	distance = 0.0f;
	previousSegment = currentSegment = 1;
	/*previousPosition = */ currentPosition = 0.0f;
	currentPositionVector.Set(v1);
	m_VertexTValues[0] = 0.0f;
	m_VertexTValues[1] = 0.0f;

	while (currentSegment >= previousSegment)
	{
		previousSegment = currentSegment;
		// previousPosition = currentPosition;
		previousPositionVector.Set(currentPositionVector);
		m_CubicCurve.Move(currentSegment,currentPosition,step);
		if (currentSegment > previousSegment)
		{
			//hit the next vertex
			m_VertexTValues[currentSegment] = distance;				
		}
		distance += step;
	}

	return distance;
}

void UITransition_Translate_Spline::OnActivate()
{
	UITransition_Translate_Axis::OnActivate();
}

void UITransition_Translate_Spline::Reset()
{
	UITransition_Translate_Axis::Reset();
	m_CurrentPosition = 0.0f;
	m_CurrentSegment = 0;
	m_PreviousT = m_CurrentT = 0.0f;	
	m_PreviousVelocity = m_CurrentVelocity = 0.0f;
	m_StartDecelerationVelocity = 0.0f;
	m_PreviousAcceleration = m_CurrentAcceleration = 1000.0f;
	m_CurrentPositionVector.Set(m_StartPos.d);
}

f32 UITransition_Translate_Spline::CalculateRealTotalTime()
{
	f32 transitionTime = 0.0f;
	f32 dt = .03333f;

	Reset();
	while(!IsComplete())	
	{
		Process(dt);
		transitionTime += dt;
	}
	Reset();
	return transitionTime;
}

void UITransition_Translate_Spline::Process(f32 dt)
{
	m_PreviousVelocity = m_CurrentVelocity;

	//m_PercentComplete = m_CurrentPosition/m_Length;

	//lets slow down on the last segment
	//if (m_CurrentSegment == m_NumVertices-2)
	//{
	//	if (m_PreviousSegment < m_CurrentSegment)
	//	{
	//		//we just got to the last segment
	//		m_StartDecelerationVelocity = m_CurrentVelocity;
	//	}
	//	//decelerate so that our velocity becomes 0 exactly when we reach our final position
	//	m_CurrentVelocity = m_StartDecelerationVelocity * (1 - m_CurrentT);

	//	if ( m_CurrentT > .99f )
	//	{
	//		SetComplete(true);
	//	}
	//}
	//else
	//{
	//	//accelerate to max velocity
	//	m_CurrentVelocity += m_CurrentAcceleration*dt;
	//	if (m_CurrentVelocity >= m_MaxVelocity)
	//	{
	//		m_CurrentVelocity = m_MaxVelocity;
	//	}
	//}

	//m_PreviousSegment = m_CurrentSegment;
	//m_PreviousT = m_CurrentT;

	//distanceThisFrame = dt*m_CurrentVelocity;
	////lets make sure we don't stop too soon
	//if (distanceThisFrame < .5f )
	//{
	//	distanceThisFrame = .5f;
	//}

	//m_CurrentPosition += distanceThisFrame;

	//m_CubicCurve.Move(m_CurrentSegment,m_CurrentT,distanceThisFrame);
	//m_CubicCurve.SolveSegment(m_CurrentPositionVector,m_CurrentSegment,m_CurrentT);

	m_CurrentVelocity = m_MaxVelocity;

	m_PreviousSegment = m_CurrentSegment;
	m_PreviousT = m_CurrentT;

	f32 distanceThisFrame = dt*m_CurrentVelocity;
	//lets make sure we don't stop too soon
	//if (distanceThisFrame < .5f )
	//{
	//	distanceThisFrame = .5f;
	//}

	m_CurrentPosition += distanceThisFrame;

	//m_CubicCurve.Move(m_CurrentSegment,m_CurrentT,distanceThisFrame);
	m_CurrentT +=dt;
	if (m_CurrentT > 1.0f)
	{
		m_CurrentT -= 1.0f;
		m_CurrentSegment++;
	}

	if ( m_CurrentSegment < m_PreviousSegment || (m_CurrentSegment == m_NumVertices-2 && m_CurrentT > .99f ))
	{
		SetComplete(true);
		m_CurrentPositionVector.Set(m_FinalPos.d);
	}
	else
	{
		m_CubicCurve.SolveSegment(m_CurrentPositionVector,m_CurrentSegment,m_CurrentT);	
	}
}

void UITransition_Translate_Spline::Update3(float dt)
{
	if (m_bIsDisabled)
	{
		SetComplete(true);
		return;
	}

	if (IsComplete())
	{
		m_TimeTakenThisFrame = 0.0f;
		m_PercentComplete = 1.0f;
		return;
	}

	UITransition::Update(dt);		

	f32 amplifiedDT = dt;
	//f32 totalTime = (m_TransitionTime+m_DelayTime);
	f32 percentComplete;
	m_RealElapsedTime += dt;

	m_PercentComplete =(m_RealElapsedTime-m_DelayTime)/m_TransitionTime;
	if(m_PercentComplete<0.0f){m_PercentComplete=0.0f;}

	if (m_bEaseIn)
	{
		percentComplete = SlowIn(m_PercentComplete);
	}
	else if (m_bEaseOut)
	{
		percentComplete = SlowOut(m_PercentComplete);
	}
	else if (m_bEaseInOut)
	{
		percentComplete = SlowInOut(m_PercentComplete);
	}
	else
	{		
		percentComplete = m_PercentComplete;
	}
	
	m_ElapsedTime += amplifiedDT;
	m_TimeTakenThisFrame = amplifiedDT;

	m_PreviousPosition = m_CurrentPosition;
	m_PreviousPositionVector.Set(m_CurrentPositionVector);
	m_PreviousT = m_CurrentT;

	//if (percentComplete < 1.0f)
	//{
	//	m_CurrentSegment = 0;
	//	m_CurrentT = 0;	

	//	while (percentComplete > m_VertexTValues[m_CurrentSegment+1])
	//	{
	//		m_CurrentSegment++;
	//	}

	//	f32 tLeftOver = percentComplete - m_VertexTValues[m_CurrentSegment];
	//	f32 currentSegPercent = m_VertexTValues[m_CurrentSegment+1] - m_VertexTValues[m_CurrentSegment];
	//	m_CurrentT = tLeftOver/currentSegPercent;
	//}

	//convert to seg and segs t value
	int numSegs = m_NumVertices-1;
	f32 segPercent = 1.0f/numSegs;
	m_CurrentSegment = (s32)(percentComplete / segPercent);
	m_CurrentT = (percentComplete - (segPercent*m_CurrentSegment))*numSegs;

	//m_CurrentT += amplifiedDT;
	//if (m_CurrentT > 1.0f)
	//{
	//	m_CurrentT -= 1.0f;
	//	m_CurrentSegment++;
	//}

	if (m_PercentComplete >= .9999f || m_CurrentSegment >= numSegs)
	{
		SetComplete(true);
		m_CurrentPositionVector.Set(m_FinalPos.d);
	}
	else
	{
		m_CubicCurve.SolveSegment(m_CurrentPositionVector,m_CurrentSegment,m_CurrentT);	
	}

	rage::Vector3 m_Distance;
	m_Distance.Subtract(m_CurrentPositionVector,m_PreviousPositionVector);
	m_CurrentPosition += m_Distance.Mag();

#if __DEV
	m_TimeTakenThisFrameSUM += m_TimeTakenThisFrame;
#endif

}

void UITransition_Translate_Spline::UpdateMatrix(Matrix34* pMatrix)
{
	Matrix34 previousMatrix;
	previousMatrix.Set(*pMatrix);

	if (pMatrix && !m_bIsDisabled && m_ElapsedTime >= m_DelayTime)
	{
		pMatrix->d.Set(m_CurrentPositionVector);
		if (m_bXZOnly)
		{
			pMatrix->d.y = previousMatrix.d.y;
		}
	}
}

void UITransition_Translate_Spline::UpdateMatrixToFinal(Matrix34* pMatrix)
{
	//need to setup all my vars
	Activate();//PrepareToStart();

	if (pMatrix && !m_bIsDisabled)
	{
		//sync the targets matrix with this matrix
		pMatrix->d.Set(m_FinalPos.d);
	}
}

UITransition_Rotate::UITransition_Rotate() : UITransition()
{
	m_PreviousVelocity = 0.0f;
	m_CurrentVelocity = 0.0f;
	m_TargetVelocity = 0.0f;

	m_PreviousAcceleration = 0.0f;
	m_CurrentAcceleration = 0.0f;

	m_PreviousPosition = 0.0f;
	m_CurrentPosition = 0.0f;
	m_TargetPosition = 0.0f;
	m_InitialTargetPosition = 0.0f;
	m_TargetOffset = 0.0f;
	m_Axis = X_AXIS;

	m_bAccelerate = false;
	m_bDecelerate = false;

	m_UpVectorOffset = 1.0f;
}

#if __BANK
const char *UITransition_Rotate::sm_ppRotationNames[] = 
{
   "NORTH",
   "SOUTH",
   "EAST",
   "WEST",
   "DOWN",
   "UP",
   "RADIANS"
};

void UITransition_Rotate::AddWidgets( rage::bkBank & bk ) 
{     
	bk.AddSlider( "Target Velocity", &m_TargetVelocity, -10000.0f, 10000.0f, 0.1f );
	bk.AddSlider( "Initial Target Position", &m_TargetOffset, -10000.0f, 10000.0f, 0.1f );
	UITransition::AddWidgets(bk);
}
#endif

void UITransition_Rotate::Reset()
{
	UITransition::Reset();

	m_CurrentPosition = 0.0f;
	m_PreviousPosition = 0.0f;
	m_CurrentVelocity = 0.0f;
	m_PreviousVelocity = 0.0f;
	m_CurrentAcceleration = 0.0f;
	m_PreviousAcceleration = 0.0f;


}

bool UITransition_Rotate::ProcessDataTag(const char* name , const char* value)
{
	if(stricmp(name,"axis")==0)
	{
		if(stricmp(value,"X")==0 || (stricmp(value,"x")==0))
		{
			m_Axis = X_AXIS;
		}
		else if(stricmp(value,"Y")==0 || (stricmp(value,"y")==0))
		{
			m_Axis = Y_AXIS;
		}
		else if(stricmp(value,"Z")==0 || (stricmp(value,"z")==0))
		{
			m_Axis = Z_AXIS;
		}
	}
	else if(stricmp(name,"type")==0)
	{
		if(stricmp(value,"NORTH")==0)
		{
			m_RotateType = NORTH;
		}
		else if(stricmp(value,"SOUTH")==0)
		{
			m_RotateType = SOUTH;
		}
		else if(stricmp(value,"EAST")==0)
		{
			m_RotateType = EAST;
		}
		else if(stricmp(value,"WEST")==0)
		{
			m_RotateType = WEST;
		}
		else if(stricmp(value,"final_look")==0)
		{
			m_RotateType = FINAL_LOOK;
		}
		else if(stricmp(value,"target")==0)
		{
			m_RotateType = TARGET;
		}
		else if(stricmp(value,"radians")==0)
		{
			m_RotateType = RADIANS;
		}
		else
		{
			m_RotateType = RADIANS;
			m_TargetOffset = (f32)atof(value);
		}
	}
	else
	{
		return UITransition::ProcessDataTag(name,value);
	}

	return true;
}

void UITransition_Rotate::RotateAroundLocalXAxis()
{
	switch (m_RotateType)
	{
		case RADIANS:
		{
			//rotates the current matrix so that it's final look vector is parallel to the vector that is m_bIsRotateVerticalValue above the x,z plane   
			m_StartPos.Normalize();
			f32 distanceFromXZPlane = -asinf(m_StartPos.c.y);
			m_TargetPosition = (m_TargetOffset - distanceFromXZPlane);
			//m_FinalPos.Set(m_StartPos);
			//m_FinalPos.RotateLocalX(m_TargetPosition);

			//Vector3 startPosAngles,destPosAngles,angles;
			//m_StartPos.ToEulersZXY(startPosAngles);
			//m_FinalPos.ToEulersZXY(destPosAngles);
			//angles = destPosAngles - startPosAngles;
			//m_TargetPosition = angles.x;
		}
		break;
		case FINAL_LOOK:
		{
			Vector3 startPosAngles,destPosAngles,angles;
			m_StartPos.ToEulersZXY(startPosAngles);
			m_FinalPos.ToEulersZXY(destPosAngles);
			angles = destPosAngles - startPosAngles;
			m_TargetPosition = angles.x;
		}
		break;
		case TARGET:
			{	
				//create a matrix that is looking at the target from our current position
				Matrix34 finalMatrix;
				finalMatrix.LookAt(m_StartPos.d,m_FinalPos.d,Vector3(0,1,0));

				Vector3 startPosAngles,destPosAngles,angles;
				m_StartPos.ToEulersZXY(startPosAngles);
				finalMatrix.ToEulersZXY(destPosAngles);
				angles = destPosAngles - startPosAngles;
				m_TargetPosition = angles.x;
			}
			break;

		default:
		break;
	}
}

void UITransition_Rotate::RotateAroundLocalYAxis()
{
	switch (m_RotateType)
	{
	case NORTH:
		{
			Vector3 startPosAngles,destPosAngles,angles;
			m_StartPos.ToEulersZXY(startPosAngles);
			m_FinalPos.ToEulersZXY(destPosAngles);
			angles = destPosAngles - startPosAngles;
			m_TargetPosition = angles.y;
			//m_TargetPosition = m_StartPos.c.AngleY(Vector3(0.0f,0.0f,1.0f));			
		}
		break;
	case SOUTH:
		{
			m_TargetPosition = m_StartPos.c.AngleY(Vector3(0.0f,0.0f,-1.0f));
		}
		break;
	case EAST:
		{
			m_TargetPosition = m_StartPos.c.AngleY(Vector3(-1.0f,0.0f,0.0f));
		}
		break;
	case WEST:
		{
			m_TargetPosition = m_StartPos.c.AngleY(Vector3(1.0f,0.0f,0.0f));
		}
		break;
	case FINAL_LOOK:
		{
			//m_TargetPosition = m_StartPos.c.AngleY(m_FinalPos.c);	
			Vector3 startPosAngles,destPosAngles,angles;
			m_StartPos.ToEulersZXY(startPosAngles);
			m_FinalPos.ToEulersZXY(destPosAngles);
			angles = destPosAngles - startPosAngles;
			m_TargetPosition = angles.y;
		}
		break;
		case TARGET:
			{
				Vector3 diff;
				diff.Subtract(m_StartPos.d,m_FinalPos.d);
				m_TargetPosition = m_StartPos.c.AngleY(diff);	
			}
			break;
	default:
		break;
	}

	if (m_TargetPosition < -PI)
	{
		m_TargetPosition += 2*PI;
	}
	else if (m_TargetPosition > PI)
	{
		m_TargetPosition -= 2*PI;
	}
}

void UITransition_Rotate::RotateAroundLocalZAxis()
{
	Vector3 startPosAngles,destPosAngles,angles;
	m_StartPos.ToEulersZXY(startPosAngles);
	m_FinalPos.ToEulersZXY(destPosAngles);
	angles = destPosAngles - startPosAngles;
	m_TargetPosition = angles.z;
}

void UITransition_Rotate::OnDeactivate()
{
	UITransition::OnDeactivate();

	m_PreviousVelocity = 0.0f;
	m_CurrentVelocity = 0.0f;

	m_PreviousAcceleration = 0.0f;
	m_CurrentAcceleration = 0.0f;

	m_PreviousPosition = 0.0f;
	m_CurrentPosition = 0.0f;

	m_ElapsedTime = 0.0f;	
	m_RealElapsedTime = 0.0f;
	m_TimeTakenThisFrame = 0.0f;

	m_PercentComplete = 0.0f;
	m_PercentDistance = 0.0f;
}

void UITransition_Rotate::OnActivate()
{
	m_PreviousMatrix.Set(m_StartPos);

	SetComplete(false);

	//we need to calculate the target position
	switch (m_Axis)
	{
		case X_AXIS:
			{
				RotateAroundLocalXAxis();
			}
			break;
		case Y_AXIS:
			{
				RotateAroundLocalYAxis();
			}
			break;
		case Z_AXIS:
			{
				RotateAroundLocalZAxis();
			}
			break;
		default:
			break;
	}

	m_MidPointPosition = 0.0f;
	m_InitialVelocity = m_CurrentVelocity;
	m_MidPointVelocity = (2.0f*m_TargetPosition / m_TransitionTime) - (m_CurrentVelocity*.5f) - (m_TargetVelocity*.5f);
	m_ConstantAcceleration = (2.0f*(m_MidPointVelocity - m_CurrentVelocity) ) / m_TransitionTime;
	m_ConstantDeceleration = (2.0f*(m_TargetVelocity - m_MidPointVelocity) ) / m_TransitionTime;

    UITransition::OnActivate();
}

void UITransition_Rotate::UpdateMatrix(Matrix34* pMatrix)
{
	if (pMatrix && !m_bIsDisabled && m_ElapsedTime > m_DelayTime)
	{
		//sync the targets matrix with this matrix
		switch (m_Axis)
		{
			case X_AXIS:
				{					
					switch(m_RotateType)
					{
					case TARGET:
						{
							//create a matrix that is looking at the target from our current position
							Matrix34 finalMatrix;
							finalMatrix.LookAt(m_Matrix.d,m_FinalPos.d,Vector3(0,1,0));

							Vector3 startPosAngles,destPosAngles,angles;
							m_StartPos.ToEulersZXY(startPosAngles);
							finalMatrix.ToEulersZXY(destPosAngles);
							angles = destPosAngles - startPosAngles;

							pMatrix->RotateLocalX(m_CurrentPosition);
						}
						break;
					default:
						{
							pMatrix->RotateLocalX(m_CurrentPosition);
						}
						break;
					}
				}
				break;
			case Y_AXIS:
				{
					m_PreviousMatrix.Set(*pMatrix);

					//perform the rotation
					pMatrix->RotateY(m_CurrentPosition);
				}
				break;
			case Z_AXIS:
				{
					pMatrix->RotateLocalZ(m_CurrentPosition);
				}
				break;
			default:
				break;
		}
	}
}

void UITransition_Rotate::UpdateMatrixToFinal(Matrix34* pMatrix)
{
	//need to setup all my vars
	Activate();

	if (pMatrix && !m_bIsDisabled)
	{
		//sync the targets matrix with this matrix
		switch (m_Axis)
		{
		case X_AXIS:
			{
				pMatrix->RotateLocalX(m_TargetPosition);
			}
			break;
		case Y_AXIS:
			{
				pMatrix->RotateY(m_TargetPosition);
			}
			break;
		case Z_AXIS:
			{
				pMatrix->RotateLocalZ(m_TargetPosition);
			}
			break;
		default:
			break;
		}
	}
}


f32 UITransition_Rotate::TimeToTakeThisFrame(f32 dt)
{
	return TimeToTakeThisFrame(dt , (m_TransitionTime+m_DelayTime) - m_ElapsedTime , m_DelayTime , m_ElapsedTime , m_CurrentPosition , m_TargetPosition , m_CurrentVelocity);
}

f32 UITransition_Rotate::TimeToTakeThisFrame(f32 dt, f32 timeleft , f32 , f32  , f32 currentPosition , f32 targetPosition , f32 currentVelocity)
{
	const f32 CONSIDERED_COMPLETE = .01f;

	//invert so distance is always positive
	if (targetPosition < 0)
	{
		targetPosition *= -1;	
		currentPosition *= -1;
		currentVelocity *= -1;
	}

	f32 distance = targetPosition - currentPosition;

	//test distance
	if (distance <= CONSIDERED_COMPLETE)
	{
		return 0.0f;
	}

	//test velocity when time is up
	if (timeleft < 0 && currentVelocity==0)
	{
		return dt;
	}

	//test if velocity may be zero, but could still be accelerating
	if (timeleft > 0 && currentVelocity==0)
	{
		return dt;
	}

	//if velocity is in the opposite direction of the distance
	if (distance > 0 && currentVelocity < 0)
	{
		return dt;
	}

	//return time in the range from 0 to dt
	f32 time =  distance / currentVelocity;
	if (time > dt )
	{
		return dt;
	}

	return time;
}

f32 UITransition_Rotate::CalcAcceleration(f32 )
{
	f32 distance;
	f32 timeLeft;

	if (m_bUseConstantAcceleration)
	{
		return m_ConstantAcceleration;
	}
	else
	{
		if (m_bAccelerate)
		{
			//we are accelerating the whole way
			distance = m_TargetPosition - m_CurrentPosition;
			timeLeft = (m_TransitionTime+m_DelayTime) - m_ElapsedTime;
		}
		else
		{
			//only accelerate to half way if we are accelerating then decelerating
			distance = (m_TargetPosition*.5f) - m_CurrentPosition;
			timeLeft = ((m_TransitionTime*.5f)+m_DelayTime) - m_ElapsedTime;
		}

		return 2.0f*(distance-m_CurrentVelocity*timeLeft)/square(timeLeft);
	}
}

f32 UITransition_Rotate::CalcDeceleration(f32 )
{
	//f32 distance = (m_TargetPosition-m_CurrentPosition);

	//if (fabs(distance) > 0.001f)
	//{
	//	return 0.5f*(square(m_TargetVelocity)-square(m_CurrentVelocity))/distance;
	//}
	//else
	//{
	//	return 0.0f;
	//}

	if (m_bUseConstantAcceleration)
	{
		return m_ConstantDeceleration;
	}
	else
	{
		f32 distance;
		f32 timeLeft;

		//we are accelerating the whole way
		distance = m_TargetPosition - m_CurrentPosition;
		timeLeft = (m_TransitionTime+m_DelayTime) - m_ElapsedTime;

		return 2.0f*(distance-m_CurrentVelocity*timeLeft)/square(timeLeft);
	}
}

bool UITransition_Rotate::Integrate(f32 dt)
{	
	bool isComplete = false;
	// f32 distancePercent;//percent finished with respect to position
	//f32 idealInitialVelocity;

	//given our current velocity, are we going to complete in less than dt?
	m_TimeTakenThisFrame = TimeToTakeThisFrame(dt);	
	if (m_TimeTakenThisFrame < dt || (m_ElapsedTime > VERY_LOW_DT && dt < VERY_LOW_DT))
	{
		isComplete = true;	
	}

	m_PreviousVelocity = m_CurrentVelocity;
	m_PreviousPosition = m_CurrentPosition;
	m_PreviousAcceleration = m_CurrentAcceleration;		

	//accelerate for the first half then decelerate
	// distancePercent = (m_TargetPosition - m_CurrentPosition) / m_TargetPosition;
	if  (m_bAccelerate || (((m_TransitionTime*.5f)+m_DelayTime) - m_ElapsedTime > 0.0f))
	{			
		//keep accelerating until we reach exactly m_TransitionTime/2
		f32 timeLeft =  TimeToTakeThisFrame(
			m_TimeTakenThisFrame , 
			((m_TransitionTime*.5f)+m_DelayTime) - m_ElapsedTime,
			m_DelayTime,
			m_ElapsedTime,
			m_CurrentPosition,
			m_TargetPosition*.5f,
			m_CurrentVelocity);

		m_CurrentAcceleration = CalcAcceleration(timeLeft);
		//m_CurrentVelocity = idealInitialVelocity + m_CurrentAcceleration*timeLeft;//use average acceleration			
		//idealInitialVelocity = m_CurrentPosition * m_ElapsedTime;
		//m_CurrentPosition += (idealInitialVelocity+m_CurrentVelocity)*.5f*timeLeft;//add distance traveled since last update

		m_CurrentPosition = m_InitialVelocity*((m_ElapsedTime+timeLeft)-m_DelayTime) + .5f*m_CurrentAcceleration*square((m_ElapsedTime+timeLeft)-m_DelayTime);

		//do we need to decelerate any?
		if (timeLeft < m_TimeTakenThisFrame)
		{
			m_PreviousVelocity = m_CurrentVelocity;
			m_PreviousPosition = m_CurrentPosition;
			m_PreviousAcceleration = m_CurrentAcceleration;		

			//decelerate with the rest of the time
			m_CurrentAcceleration = CalcDeceleration(m_TimeTakenThisFrame - timeLeft);
			m_CurrentVelocity += m_CurrentAcceleration*(m_TimeTakenThisFrame - timeLeft);//use average acceleration			
			m_CurrentPosition += ((m_PreviousVelocity+m_CurrentVelocity)*.5f)*(m_TimeTakenThisFrame - timeLeft);//add distance traveled since last update
		}
	}
	else
	{
		//decelerating
		m_CurrentAcceleration = CalcDeceleration(m_TimeTakenThisFrame);
		m_CurrentVelocity += m_CurrentAcceleration*m_TimeTakenThisFrame;//use average acceleration			
		m_CurrentPosition += m_CurrentVelocity*m_TimeTakenThisFrame;//add distance traveled since last update
	}				
	//}
	//else
	//{		
	//	if (m_TimeTakenThisFrame > dt || m_TimeTakenThisFrame < 0){m_TimeTakenThisFrame = 0;}//guard against very small floats

	//	if (!m_bAccelerate)
	//	{
	//		//only set to target velocity when decelerating
	//		//HAD TO COMMENT THIS OUT CAUSE THE ACCURACY OF GETTING TO THE FINAL VELOCITY WAS TOO LOW AND 
	//		//m_CurrentVelocity = m_TargetVelocity;
	//	}			

	//	m_CurrentPosition = m_TargetPosition;
	//	m_CurrentAcceleration = 0;
	//}

	return isComplete;
}

bool UITransition_Rotate::IntegrateConstantAcceleration(f32 dt)
{	
	bool isComplete = false;
	f32 time;

	m_PreviousVelocity = m_CurrentVelocity;
	m_PreviousPosition = m_CurrentPosition;
	m_PreviousAcceleration = m_CurrentAcceleration;		

	//given our current velocity, are we going to complete in less than dt?
	m_TimeTakenThisFrame = dt;
	
	if (m_ElapsedTime > VERY_LOW_DT && dt < VERY_LOW_DT)
	{
		isComplete = true;	
	}

	//accelerate for the first half then decelerate
	f32 accelerationTimeLeft = ((m_TransitionTime*.5f)+m_DelayTime) - m_ElapsedTime;
	if  (m_bAccelerate || accelerationTimeLeft > 0.0f)
	{			
		if (accelerationTimeLeft < dt)
		{
			f32 decelerationTimeLeft = dt - accelerationTimeLeft;
			time = ((m_ElapsedTime+accelerationTimeLeft)-m_DelayTime);
			m_CurrentVelocity = m_InitialVelocity + m_ConstantAcceleration*time;
			m_CurrentPosition = m_InitialVelocity*time + .5f*m_ConstantAcceleration*square(time);

			m_PreviousVelocity = m_CurrentVelocity;
			m_PreviousPosition = m_CurrentPosition;
			m_PreviousAcceleration = m_CurrentAcceleration;		

			m_MidPointPosition = m_CurrentPosition;			

			//decelerate with the rest of the time			
			m_CurrentPosition = m_MidPointPosition + m_MidPointVelocity*(decelerationTimeLeft) + .5f*m_ConstantDeceleration*square(decelerationTimeLeft);
		}
		else
		{
			time = (m_ElapsedTime+dt)-m_DelayTime;
			m_CurrentVelocity = m_InitialVelocity + m_ConstantAcceleration*(m_ElapsedTime+dt);
			m_CurrentPosition = m_InitialVelocity*time + .5f*m_ConstantAcceleration*square(time);
		}
	}
	else
	{
		if (m_MidPointPosition == 0.0f){m_MidPointPosition = m_CurrentPosition;}//needed this cause we may end acceleration at the very end of a frame
		//decelerating
		//decelerationElapsedTime should be zero at the midpoint (V1/totalTime div 2)
		f32 decelerationTransitionTime = m_TransitionTime*.5f;
		f32 decelerationElapsedTime = m_ElapsedTime - (decelerationTransitionTime+m_DelayTime);

		time = decelerationElapsedTime+dt;
		if (time > decelerationTransitionTime)
		{
			time = decelerationTransitionTime;
			m_TimeTakenThisFrame = decelerationTransitionTime - decelerationElapsedTime;
		}
		m_CurrentVelocity = m_MidPointVelocity + m_ConstantDeceleration*time;
		m_CurrentPosition = m_MidPointPosition + m_MidPointVelocity*time + .5f*m_ConstantDeceleration*square(time);
	}				

	return isComplete;
}

void UITransition_Rotate::Update(float dt)
{
	//change dt to use the set timestamp
	if (m_Timestep)
	{
		dt = m_Timestep;
	}

	if (m_bIsDisabled)
	{
		SetComplete(true);
		return;
	}

	if (IsComplete())
	{
		m_TimeTakenThisFrame = 0.0f;
		m_PercentComplete = 1.0f;
		return;
	}

	UITransition::Update(dt);		

	f32 amplifiedDT = dt;
	f32 totalTime = (m_TransitionTime+m_DelayTime);

	if (m_ElapsedTime>=m_DelayTime)
	{
		switch(m_MotionType)
		{
		case PHYSICS:
			{
				//check to see if we are complete			
				if (!m_bComplete) 
				{
					if (m_bUseConstantAcceleration)
					{
						IntegrateConstantAcceleration(amplifiedDT);
					}
					else
					{
						Integrate(amplifiedDT);
					}
				}
			}
			break;
		case NONE:
			{
				m_ElapsedTime -= amplifiedDT;//time is not a factor when we are warping
				m_CurrentPosition = m_TargetPosition;
			}
			break;
		default:
			break;
		}
	}

	m_RealElapsedTime += dt;

	m_PercentComplete =(m_RealElapsedTime-m_DelayTime)/m_TransitionTime;
	if(m_PercentComplete<0.0f){m_PercentComplete=0.0f;}

	if (m_bEaseInOut)
	{
		//get the current amplified time slice
		amplifiedDT = (totalTime*SlowInOut(m_PercentComplete)) - m_ElapsedTime;
	}
	else
	{		
		amplifiedDT = dt;
	}

	if (m_ElapsedTime + amplifiedDT + VERY_LOW_DT> totalTime || (m_ElapsedTime > VERY_LOW_DT && amplifiedDT < VERY_LOW_DT))
	{
		m_TimeTakenThisFrame = totalTime - m_ElapsedTime;
		m_ElapsedTime = totalTime;
		SetComplete(true);
	}
	else
	{
		m_ElapsedTime += amplifiedDT;
		m_TimeTakenThisFrame = amplifiedDT;
	}

#if __DEV
	m_TimeTakenThisFrameSUM += m_TimeTakenThisFrame;
#endif

}

#if __BANK
void UITransition_Lerp::AddWidgets( rage::bkBank & bk ) 
{      
   bk.AddToggle( "Look At Start Location", &m_bLookAtStart );
   bk.AddToggle( "Set X", &m_bSetX );
   bk.AddToggle( "Set Y", &m_bSetY );
   bk.AddToggle( "Set Z", &m_bSetZ );
   bk.AddSlider( "Add or Set To X Axis Value", &m_AddX, -10000.0f, 10000.0f, 10.0f );
   bk.AddSlider( "Add or Set To Y Axis Value", &m_AddY, -10000.0f, 10000.0f, 10.0f );
   bk.AddSlider( "Add or Set To Z Axis Value", &m_AddZ, -10000.0f, 10000.0f, 10.0f );
   UITransition::AddWidgets(bk);
}
#endif

UITransition_Lerp::UITransition_Lerp() : UITransition()
{
	m_AddX = 0.0f;
	m_AddY = 0.0f;
	m_AddZ = 0.0f;
	m_bLookAtStart = false;
	m_bSetX = false;
	m_bSetY = false;
	m_bSetZ = false;
	m_bOrientationOnly = false;
}

bool UITransition_Lerp::ProcessDataTag(const char* name , const char* value)
{
   if(stricmp(name,"add_x")==0)
   {      
      m_AddX += (f32)atof(value);
   }
   else if(stricmp(name,"add_y")==0)
   {      
      m_AddY += (f32)atof(value);
   }
   else if(stricmp(name,"add_z")==0)
   {      
      m_AddZ += (f32)atof(value);
   }
   else if(stricmp(name,"set_x")==0)
   {      
      m_bSetX = true;
      m_AddX = (f32)atof(value);
   }
   else if(stricmp(name,"set_y")==0)
   {  
      m_bSetY = true;
      m_AddY = (f32)atof(value);
   }
   else if(stricmp(name,"set_z")==0)
   {  
      m_bSetZ = true;
      m_AddZ = (f32)atof(value);
   }
   else if(stricmp(name,"add_south")==0)
   {      
      m_AddZ += (f32)atof(value);
   }
   else if(stricmp(name,"look_at")==0)
   {      
      if(stricmp(value,"start")==0)
      {
         m_bLookAtStart = true;
      }
   }
	else if(stricmp(name,"orientation_only")==0)
	{      
		if(stricmp(value,"true")==0)
		{
			m_bOrientationOnly = true;
		}
	}	
	else
	{
		return UITransition::ProcessDataTag(name,value);
	}

   return true;
}

void UITransition_Lerp::Reset()
{
	UITransition::Reset();
}

void UITransition_Lerp::OnActivate()
{
   UITransition::OnActivate();

   SetComplete(false);

   Vector3 pos;
   pos.Set(m_FinalPos.d);

   if (m_bSetX)
   {
      pos.x = m_AddX;
   }
   else
   {
      pos.x += m_AddX;
   }

   if (m_bSetY)
   {
      pos.y= m_AddY;
   }
   else
   {
      pos.y += m_AddY;
   }

   if (m_bSetZ)
   {
      pos.z = m_AddZ;
   }
   else
   {
      pos.z += m_AddZ;
   }

   if (m_bLookAtStart)
   {
      m_FinalPos.LookAt(pos,m_StartPos.d,Vector3(0.0f,1.0f,0.0f));
   }

   qStart.Identity();
   qFinal.Identity();

   qStart.FromMatrix34(m_StartPos);
   qFinal.FromMatrix34(m_FinalPos);
   qStart.PrepareSlerp(qFinal);

   m_RealElapsedTime = 0.0f;
}

void UITransition_Lerp::UpdateMatrix(Matrix34* m)
{
	if (m && !m_bIsDisabled && m_ElapsedTime > m_DelayTime)
	{
		//we want this orientation to override all others
		if (m_bOrientationOnly)
		{
			m->Set3x3(m_Matrix);
		}
		else
		{
			m->Set(m_Matrix);
		}
	}
}

void UITransition_Lerp::UpdateMatrixToFinal(Matrix34* m)
{
	//need to setup all my vars
	Activate();

	if (m && !m_bIsDisabled)
	{
		//we want this orientation to override all others
		if (m_bOrientationOnly)
		{
			m->Set3x3(m_FinalPos);
		}
		else
		{
			m->Set(m_FinalPos);
		}
	}
}

void UITransition_Lerp::UpdateUsingTrig()
{
	m_PercentComplete = m_PercentComplete > 1.0f ? 1.0f : m_PercentComplete;
	m_PercentComplete = m_PercentComplete < 0.0f ? 0.0f : m_PercentComplete;     

	if (!m_bOrientationOnly)
	{			
		m_Matrix.d.Lerp(m_PercentComplete,m_StartPos.d,m_FinalPos.d);
	}

	Quaternion qTemp;

	qTemp.Set(qStart);
	qTemp.Slerp(m_PercentComplete,qFinal);
	qTemp.Normalize();

	m_Matrix.FromQuaternion(qTemp);
}

void UITransition_Lerp::Update(float dt)
{
	//change dt to use the set timestamp
	if (m_Timestep)
	{
		dt = m_Timestep;
	}

	if (m_bIsDisabled)
	{
		SetComplete(true);
		return;
	}

	if (IsComplete())
	{
		m_TimeTakenThisFrame = 0.0f;
		m_PercentComplete = 1.0f;
		return;
	}

	UITransition::Update(dt);

	f32 amplifiedDT = dt;
	f32 totalTime = (m_TransitionTime+m_DelayTime);
	m_RealElapsedTime += dt;

	m_PercentComplete =(m_RealElapsedTime-m_DelayTime)/m_TransitionTime;
	if(m_PercentComplete<0.0f){m_PercentComplete=0.0f;}

	if (m_bEaseIn)
	{
		//get the current amplified time slice
		amplifiedDT = (totalTime*rage::SlowIn(m_PercentComplete)) - m_ElapsedTime;
	}
	else if (m_bEaseOut)
	{
		//get the current amplified time slice
		amplifiedDT = (totalTime*rage::SlowOut(m_PercentComplete)) - m_ElapsedTime;
	}
	else if (m_bEaseInOut)
	{
		//get the current amplified time slice
		amplifiedDT = (totalTime*rage::SlowInOut(m_PercentComplete)) - m_ElapsedTime;
	}
	else
	{		
		amplifiedDT = dt;
	}		

	//must set m_PercentComplete before so we can lerp to 100% on the last frame
	m_PercentComplete = (m_ElapsedTime+amplifiedDT)/(m_TransitionTime+m_DelayTime);

	if (m_ElapsedTime >= m_DelayTime)
	{
		if (m_MotionType == WAVE)
		{
			UpdateUsingTrig();
		}
		else if (m_MotionType == PHYSICS)
		{
			UpdateUsingTrig();
		}
	}

	//sync the targets matrix with this matrix
	if (m_pTarget)
	{
		m_pTarget->Set(m_StartPos);
		UpdateMatrix(m_pTarget);
	}

	if (m_ElapsedTime + amplifiedDT + VERY_LOW_DT> totalTime || (m_ElapsedTime > VERY_LOW_DT && amplifiedDT < VERY_LOW_DT))
	{
		m_TimeTakenThisFrame = totalTime - m_ElapsedTime;
		m_ElapsedTime = totalTime;
		SetComplete(true);

		Assert(fabs(m_FinalPos.a.Mag2() - 1.0f) <= 0.0001f);
		Assert(fabs(m_FinalPos.b.Mag2() - 1.0f) <= 0.0001f);
		Assert(fabs(m_FinalPos.c.Mag2() - 1.0f) <= 0.0001f);
	}
	else
	{
		m_ElapsedTime += amplifiedDT;
		m_TimeTakenThisFrame = amplifiedDT;
	}

#if __DEV
	m_TimeTakenThisFrameSUM += m_TimeTakenThisFrame;
#endif

}

UITransition_Value::UITransition_Value() : UITransition()
{
	m_StartValue = 0.0f;
	m_FinalValue = 1.0f;
	m_pCurrentValue = NULL;
	m_CurrentValue = 0.0f;
}

void UITransition_Value::Reset()
{
	UITransition::Reset();

	if (m_pCurrentValue)
	{
		*m_pCurrentValue = 0.0f;
	}
	m_CurrentValue = 0.0f;
}

void UITransition_Value::OnActivate()
{
	UITransition::OnActivate();
	if (m_pCurrentValue)
	{
		*m_pCurrentValue = m_StartValue;
	}
	m_CurrentValue = m_StartValue;
}

void UITransition_Value::OnDeactivate()
{
	UITransition::OnDeactivate();
	if (m_pCurrentValue)
	{
		*m_pCurrentValue = m_FinalValue;
	}
	m_CurrentValue = m_FinalValue;
}

bool UITransition_Value::ProcessDataTag(const char* name , const char* value)
{
	if(stricmp(name,"start_Value")==0)
	{
		m_StartValue = (f32)atof(value);
	}   
	else if(stricmp(name,"final_Value")==0)
	{
		m_FinalValue = (f32)atof(value);
	}
	else
	{
		return UITransition::ProcessDataTag(name,value);
	}

	return true;
}

void UITransition_Value::Update(f32 dt)
{
	//change dt to use the set timestamp
	if (m_Timestep)
	{
		dt = m_Timestep;
	}

	if (m_bIsDisabled)
	{
		SetComplete(true);
		return;
	}

	if (IsComplete())
	{
		m_TimeTakenThisFrame = 0.0f;
		m_PercentComplete = 1.0f;
		return;
	}

	UITransition::Update(dt);

	if (m_ElapsedTime>=m_DelayTime)
	{		
		if (m_ElapsedTime < (m_TransitionTime+m_DelayTime))
		{
			m_PercentComplete =(m_ElapsedTime-m_DelayTime)/m_TransitionTime;
			if(m_PercentComplete<0.0f){m_PercentComplete=0.0f;}
			
			if (m_bEaseIn)
			{
				m_PercentComplete = rage::SlowIn(m_PercentComplete);
			}
			else if (m_bEaseOut)
			{
				m_PercentComplete = rage::SlowOut(m_PercentComplete);
			}
			else if (m_bEaseInOut)
			{
				m_PercentComplete = rage::SlowInOut(m_PercentComplete);
			}

			
			if (m_pCurrentValue)
			{
				*m_pCurrentValue = rage::Lerp(m_PercentComplete, m_StartValue, m_FinalValue);
			}
			else
			{
				m_CurrentValue = rage::Lerp(m_PercentComplete, m_StartValue, m_FinalValue);
			}
			
		}
		else
		{
			SetComplete(true);
		}   
	}

	m_ElapsedTime += dt;

#if __DEV
	m_TimeTakenThisFrameSUM += m_TimeTakenThisFrame;
#endif

}

UITransition_LookAtTarget::UITransition_LookAtTarget() : UITransition()
{
	m_TimeToLook = 0;
	m_Axis = FINAL_LOOK;
	m_Up = Y_AXIS;
	m_InitialTargetPosition = 0.0f;
	m_LookAtTargetOffset = 0.0f;
	m_bClockwiseX = true;
	m_bClockwiseY = true;
	m_bClockwiseZ = true;
	m_bDelay_up_rotate = false;

	Reset();
}

#if __BANK
void UITransition_LookAtTarget::AddWidgets( rage::bkBank & bk ) 
{	
	bk.AddSlider( "Time To Look", &m_TimeToLook, -1000.0f, 1000.0f, 0.1f );
	UITransition::AddWidgets(bk);
}
#endif

bool UITransition_LookAtTarget::ProcessDataTag(const char* name , const char* value)
{
	if(stricmp(name,"time_to_look")==0)
	{  
		m_TimeToLook = (f32)atof(value);
	}
	else if(stricmp(name,"target")==0)
	{
		m_InitialTargetPosition = (f32)atof(value);
	}
	else if(stricmp(name,"axis")==0)
	{
		if(stricmp(value,"X")==0 || (stricmp(value,"x")==0))
		{
			m_Axis = X_AXIS;
		}
		else if(stricmp(value,"Y")==0 || (stricmp(value,"y")==0))
		{
			m_Axis = Y_AXIS;
		}
		else if(stricmp(value,"Z")==0 || (stricmp(value,"z")==0))
		{
			m_Axis = Z_AXIS;
		}
		else if(stricmp(value,"final_look")==0)
		{
			m_Axis = FINAL_LOOK;
		}
		else if(stricmp(value,"city_center_y")==0)
		{
			m_Axis = CITY_CENTER_Y;
		}
		else if(stricmp(value,"city_center_z")==0)
		{
			m_Axis = CITY_CENTER_Z;
		}
	}
	else if(stricmp(name,"lean_factor")==0)
	{
	}
	else if(stricmp(name,"delay_up_rotate")==0)
	{
		m_bDelay_up_rotate = true;
	}	
	else if(stricmp(name,"up")==0)
	{
		if(stricmp(value,"Y")==0 || (stricmp(value,"y")==0))
		{
			m_Up = Y_AXIS;
		}
		if(stricmp(value,"final_up")==0)
		{
			m_Up = FINAL_UP;
		}
	}
	else
	{
		return UITransition::ProcessDataTag(name,value);
	}

	return true;
}

void UITransition_LookAtTarget::Reset()
{
	UITransition::Reset();

	m_bClockwiseX = true;
	m_bClockwiseY = true;
	m_bClockwiseZ = true;
	m_bCompleteX = false;
	m_bCompleteY = false;
	m_bCompleteZ = false;
	m_DynamicTargetOffset.Zero();
}

void UITransition_LookAtTarget::PrepareToStart()
{
	m_DynamicTargetOffset.Zero();
	m_TargetPosition = m_FinalPos.d;
	m_bCompleteX = false;
	m_bCompleteY = false;
	m_bCompleteZ = false;

	//set which up vector we want to use
	if (m_Up == Y_AXIS)
	{
		m_FinalUpVector.Set(0.0f,1.0f,0.0f);
		m_UpVector.Set(0.0f,1.0f,0.0f);
	}
	else
	{
		m_FinalUpVector.Set(m_FinalPos.b);
		m_UpVector.Set(m_FinalPos.b);
	}

	if (m_Axis == FINAL_LOOK)
	{
		TraverseAlongVector(&m_FinalPos.c,&m_TargetPosition,m_InitialTargetPosition+m_LookAtTargetOffset);
	}
	else if (m_Axis == CITY_CENTER_Y)
	{
// 		m_TargetPosition.Set(*NAVMAP->GetCityCenterPosition());
// 		m_TargetPosition.y += (m_InitialTargetPosition+m_LookAtTargetOffset);		
	}
	else if (m_Axis == CITY_CENTER_Z)
	{
// 		m_TargetPosition.Set(*NAVMAP->GetCityCenterPosition());
// 		m_TargetPosition.z += (m_InitialTargetPosition+m_LookAtTargetOffset);		
	}

	//set our matrix to the game matrix and update below
	Matrix34 final;
	Vector3 lookFrom,lookTo;
	float angle;
	lookFrom.Zero();
	lookTo.Zero();

	m_StartPos.Set(*UIMANAGER->GetUICamera()->GetPreUICameraMatrix());
	m_Matrix.Set(*UIMANAGER->GetUICamera()->GetPreUICameraMatrix());

	//check which way we should rotate around the Y axis
	final.LookAt(m_Matrix.d,m_TargetPosition,m_UpVector);

	lookTo.Set(final.c.x,0.0f,final.c.z);	
	lookTo.Normalize();
	lookFrom.Set(m_StartPos.c.x,0.0f,m_StartPos.c.z);
	lookFrom.Normalize();
	angle = lookFrom.Angle(lookTo);

	if (angle<0.0f)//if the angle is large, then we must need to rotate in the other direction
	{
		m_bClockwiseY = m_bPreviousDirectionY = true;
	}
	else
	{
		m_bClockwiseY = m_bPreviousDirectionY = false;			
	}

	m_PreviousMatrix.Set(m_StartPos);
	m_RealElapsedTime = 0.0f;
}

void UITransition_LookAtTarget::OnActivate()
{
	Vector3 currentLook,finalLook;

	UITransition::OnActivate();

	PrepareToStart();

	GetUpdatedFinalMatrix(&m_UpdatedFinalPos);
}

void UITransition_LookAtTarget::TraverseAlongVector(Vector3* source, Vector3* dest, f32 distance)
{
	if (source && dest)
	{
		Vector3 forwardVector;
		forwardVector.Set(*source);

		forwardVector.Normalize();
		dest->AddScaled(forwardVector,distance);
	}
}

void UITransition_LookAtTarget::UpdateMatrix(Matrix34* m)
{
	Matrix34 finalMatrix;
	Vector3 targetPosition;
	if (m_bIsDisabled)
	{
		SetComplete(true);
		return;
	}

	if (m && m_ElapsedTime>=m_DelayTime)
	{
		//set our matrix to the game matrix and update below
		m_Matrix.Set(*m);
		m_Matrix.Set3x3(m_PreviousMatrix);
		m_Matrix.Set(*UIMANAGER->GetUICamera()->GetPreUICameraMatrix());

		targetPosition.Set(m_TargetPosition);
		targetPosition.Add(m_DynamicTargetOffset);

		if (m_PercentComplete < 1.0f && !IsComplete())
		{
			m_PercentComplete = (m_ElapsedTime-m_DelayTime) / m_TimeToLook;
			if(m_PercentComplete<0.0f){m_PercentComplete=0.0f;}
			m_PercentComplete = rage::SlowInOut(m_PercentComplete);
			
			//create a matrix that is looking at the target
			finalMatrix.LookAt(m_Matrix.d,targetPosition,m_FinalUpVector);

			Assertf(m_Matrix.d.x != targetPosition.x || m_Matrix.d.z != targetPosition.z, "Looking at the target position from the current position creates a vector that is pointing straight down!");

			//lerp towards the matrix that is looking at the target
			//use m_StartPos instead of m_Matrix, otherwise we will be looking at the target much sooner than m_TransitionTime
			RotateTo(m_StartPos, finalMatrix, m_PercentComplete);
		}
		else
		{
			//keep looking at our target even though we may have been "complete"
			finalMatrix.LookAt(m_Matrix.d,targetPosition,m_FinalUpVector);

			//lerp towards the matrix that is looking at the target
			RotateTo(m_StartPos, finalMatrix, 1.0f);

			SetComplete(true);
		}

		m_PreviousMatrix.Set(m_Matrix);
		m->Set3x3(m_Matrix);
	}
}

void UITransition_LookAtTarget::UpdateMatrixToFinal(Matrix34* m)
{
	if (m && !m_bIsDisabled)
	{
		PrepareToStart();

		//we want this orientation to override all others
		m->LookAt(m->d,m_TargetPosition,m_FinalUpVector);
	}
}

bool UITransition_LookAtTarget::IsOppositeDirection(rage::f32 a , rage::f32 b)
{
	if ((a > 0 && b < 0) || (a < 0 && b > 00))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void UITransition_LookAtTarget::RotateTo(Matrix34&  matrixFrom, Matrix34& matrixTo , float t) 
{
	Vector3 axis,lookFrom,lookTo;
	float angle;
	Matrix34 matrixY,matrixX,matrixZ,final;
	
	m_bPreviousDirectionY = m_bCurrentDirectionY;

	matrixY.Set(matrixFrom);//so we use the same initial matrix for all rotations below
	lookFrom.Zero();
	lookTo.Zero();
	final.Set(matrixFrom);

	//rotate about world y
	lookTo.Set(matrixTo.c.x,0.0f,matrixTo.c.z);	
	lookTo.Normalize();
//	f32 lookToAngle = atan2f(lookTo.x, lookTo.z);
	lookFrom.Set(matrixFrom.c.x,0.0f,matrixFrom.c.z);
	lookFrom.Normalize();
// 	f32 lookFromAngle = atan2f(lookFrom.x, lookFrom.z);
// 	#pragma warning( disable : 4189)
	angle = lookFrom.Angle(lookTo);
// 	f32 angle1 = lookFrom.AngleY(lookTo);
// 	f32 angle2 = lookFromAngle + lookToAngle;
// 	if (fabs(angle) > fabs(angle1)){angle = angle1;}

	//remove "mirroring". Prevent angle from going to other side of the lookvector by a large amount
	//if (angle < -MAX_ROTATE_INC_PER_FRAME && !m_bClockwiseY )
	//{
	//	angle = angle + (2*PI);
	//}
	//if (angle > MAX_ROTATE_INC_PER_FRAME && m_bClockwiseY )
	//{
	//	angle = angle - (2*PI);
	//}

	//determine direction we should rotate
	lookFrom.RotateY(angle);
	lookFrom.Normalize();
	if (lookFrom.Angle(lookTo) <= (angle*0.1f))//if the angle is large, then we must need to rotate in the other direction
	{
		m_bCurrentDirectionY = 1;
	}
	else
	{
		angle = -angle;
		m_bCurrentDirectionY = 0;
	}

	//if (m_bPreviousDirectionY == m_bCurrentDirectionY)
	//{
		final.RotateY(angle*t);
	//}
	//else
	//{
	//	//we swept over our target, so lets just complete so we can continue looking at it
	//	final.RotateY(angle);
	//	SetComplete(true);
	//}

	//rotate about local x
	matrixX.Set(matrixFrom);
	matrixX.RotateY(angle);//rotate all the way
	matrixX.Normalize();

	matrixZ.Set(matrixX);//set before we rotate matrixX
	ComputeRotation(matrixX.c,matrixTo.c,axis,angle);
	final.RotateUnitAxis(axis,angle*t);	

	//rotate up
	if (m_bDelay_up_rotate)
	{
		//wait for a bit before we rotate just in case the "up" vector is pointing sideways
		f32 upT = (t-.5f) * 2.0f;
		if (upT>.0f)
		{
			matrixZ.Normalize();
			matrixZ.RotateUnitAxis(axis,angle);//rotate all the way
			ComputeRotation(matrixZ.b,matrixTo.b,axis,angle);
			final.RotateUnitAxis(axis,angle*SlowIn(upT));
		}
	}
	else
	{
		matrixZ.Normalize();
		matrixZ.RotateUnitAxis(axis,angle);//rotate all the way
		ComputeRotation(matrixZ.b,matrixTo.b,axis,angle);
		final.RotateUnitAxis(axis,angle*t);
	}

	m_Matrix.Set(final);
}

void UITransition_LookAtTarget::ComputeRotation(const Vector3& unitFrom, const Vector3& unitTo, Vector3& unitAxis, float& angle)
{
	const float underOne = square(0.999f);
	ASSERT_ONLY(const float overOne = square(1.001f);)
	Assertf(unitFrom.Mag2() >= underOne && unitFrom.Mag2() <= overOne, "Vector3 <%f, %f, %f> does not have length 1",unitFrom.x,unitFrom.y,unitFrom.z);
	Assertf(unitTo.Mag2() >= underOne && unitTo.Mag2() <= overOne, "Vector3 <%f, %f, %f> does not have length 1",unitTo.x,unitTo.y,unitTo.z);
	float dot = unitFrom.Dot(unitTo);
	//if (dot<underOne)
	{
		// The vectors defining the rotation are not nearly parallel.
		unitAxis.Set(unitFrom);
		angle = PI;
		if (dot>-underOne)
		{
			// The vectors defining the rotation are not nearly anti-parallel.
			unitAxis.Cross(unitTo);
			float sine = unitAxis.Mag();
			if (sine!=0.0f)
			{
				unitAxis.InvScale(sine);
				angle = dot>0.0f ? AsinfSafe(sine) : PI-AsinfSafe(sine);
			}
			else
			{
				unitAxis.Set(XAXIS);
				angle = 0.0f;
			}
		}
		else
		{
			// The vectors defining the rotation are in nearly opposite directions, so choose an axis for a 180 degree rotation.
			Vector3 perpendicular = (fabsf(unitFrom.x)<SQRT3INVERSE ? XAXIS : (fabsf(unitFrom.y)<SQRT3INVERSE ? YAXIS : ZAXIS));
			unitAxis.Cross(perpendicular);
			unitAxis.Normalize();
		}

		// Return true to indicate that the given vectors are not parallel, so a rotation should be done.
		//return true;
	}

	// Return false to indicate that the given vectors are parallel, so no rotation is needed.
	//return false;
}

void UITransition_LookAtTarget::Update(float dt)
{
	//change dt to use the set timestamp
	if (m_Timestep)
	{
		dt = m_Timestep;
	}

	if (m_bIsDisabled)
	{
		SetComplete(true);
		return;
	}

	if (IsComplete())
	{
		m_TimeTakenThisFrame = 0.0f;
		m_PercentComplete = 1.0f;
		return;
	}

	UITransition::Update(dt);		

	f32 amplifiedDT = dt;
	f32 totalTime = (m_TransitionTime+m_DelayTime);

	if (m_ElapsedTime>=m_DelayTime)
	{
		//sync the targets matrix with this matrix
		if (m_pTarget)
		{
			m_pTarget->Set(m_StartPos);
			UpdateMatrix(m_pTarget);
		}
	}

	m_RealElapsedTime += dt;

	if (m_bEaseInOut)
	{
		//get the current amplified time slice
		amplifiedDT = (totalTime*SlowInOut(m_PercentComplete)) - m_ElapsedTime;
	}
	else
	{		
		amplifiedDT = dt;
	}

	if (m_ElapsedTime + amplifiedDT + VERY_LOW_DT> totalTime || (m_ElapsedTime > VERY_LOW_DT && amplifiedDT < VERY_LOW_DT))
	{
		m_TimeTakenThisFrame = totalTime - m_ElapsedTime;
		m_ElapsedTime = totalTime;
		SetComplete(true);
	}
	else
	{
		m_ElapsedTime += amplifiedDT;
		m_TimeTakenThisFrame = amplifiedDT;
	}
#if __DEV
	m_TimeTakenThisFrameSUM += m_TimeTakenThisFrame;
#endif
}

UITransition_Lean::UITransition_Lean() : UITransition_Parallel()
{
	m_UpVectorOffset = 1.0f;
	m_MaxLeanAngle = MAX_ANGLE;
	m_bDelay_up_rotate = false;
	m_NumPreviousAngles = NUM_PREVIOUS_ANGLES;
}

bool UITransition_Lean::ProcessDataTag(const char* name , const char* value)
{
	if(stricmp(name,"lean_factor")==0)
	{
		m_UpVectorOffset = (f32)atof(value);
	}
	else if(stricmp(name,"delay_up_rotate")==0)
	{
		m_bDelay_up_rotate = true;
	}	
	else
	{
		return UITransition_Parallel::ProcessDataTag(name,value);
	}

	return true;
}

#if __BANK
void UITransition_Lean::AddWidgets( rage::bkBank & bk ) 
{	
	bk.AddSlider( "Max lean angle", &m_MaxLeanAngle, -1000.0f, 1000.0f, 0.1f );
	bk.AddSlider( "m_UpVectorOffset", &m_UpVectorOffset, -1000.0f, 1000.0f, 0.1f );
	UITransition::AddWidgets(bk);
}
#endif

void UITransition_Lean::OnActivate()
{
	UITransition_Parallel::OnActivate();

	for(int i=0;i<m_NumPreviousAngles;i++)
	{
		m_PreviousAngles[i] =0.0f;
	}

    m_PreviousMatrix.Set(m_StartPos);
	m_DistanceLeanOut = 0.0f;	
}

void UITransition_Lean::UpdateMatrix(Matrix34* pMatrix)
{
	Matrix34 startMatrix,finalMatrix;
	Vector3 up;
	Vector3 axis,startLook,finalLook;
	float angleRotated,angleToLean;

	if (pMatrix && !m_bIsDisabled && m_ElapsedTime > m_DelayTime)
	{

		startMatrix.Set(m_PreviousMatrix);

		UITransition_Parallel::UpdateMatrix(pMatrix);

		finalMatrix.Set(*pMatrix);
		m_PreviousMatrix.Set(*pMatrix);

		//Force us to rotate out of lean no matter what the rotation over the last part of the transition
		if (m_ElapsedTime >= (m_TransitionTime+m_DelayTime)-.5f/*m_EaseOutTime*/)//make sure we rotate back up
		{
			f32 percentEasedOut = ((m_TransitionTime+m_DelayTime) - (m_ElapsedTime+.0333f));//+.0333 so we can remove lean a frames before we finish
			if (percentEasedOut<0){percentEasedOut=0;}
			angleToLean = m_DistanceLeanOut * (percentEasedOut*2);//percentEasedOut is actually inverted here
		}
		else
		{
			//get look vectors on land
			finalLook.Set(finalMatrix.c.x,0.0f,finalMatrix.c.z);	
			finalLook.Normalize();
			startLook.Set(startMatrix.c.x,0.0f,startMatrix.c.z);
			startLook.Normalize();

			//lean the up vector if we are rotating about the Y
			angleRotated = startLook.AngleY(finalLook);	
			
			angleToLean = angleRotated * m_UpVectorOffset;//manually adjust angle magnitude

			//average leaning so we can achieve an "ease in/out" over some frames.  		
			for(int i=0;i<m_NumPreviousAngles;i++)
			{
				angleToLean += m_PreviousAngles[i];
			}
			angleToLean = angleToLean / (m_NumPreviousAngles + 1);//includes rage_new angleToLean
			
			for(int i=0;i<m_NumPreviousAngles-1;i++)
			{
				m_PreviousAngles[i] = m_PreviousAngles[i+1];
			}

			//clamp
			if (angleToLean > m_MaxLeanAngle){angleToLean = m_MaxLeanAngle;}
			if (angleToLean < -m_MaxLeanAngle){angleToLean = -m_MaxLeanAngle;}

			m_DistanceLeanOut = m_PreviousAngles[m_NumPreviousAngles-1] = angleToLean;
		}

		//clamp
		if (angleToLean > m_MaxLeanAngle){angleToLean = m_MaxLeanAngle;}
		if (angleToLean < -m_MaxLeanAngle){angleToLean = -m_MaxLeanAngle;}

		//if the camera is pointing down, then reduce the lean
		f32 facingDownLimit = (1-pMatrix->c.y)*2;
		if (facingDownLimit < 0.0f ){facingDownLimit = 1.0f;}
		if (facingDownLimit > 1.0f ){facingDownLimit = 1.0f;}
		angleToLean *= facingDownLimit;
		pMatrix->RotateLocalZ(angleToLean);
	}
}

void UITransition_Bounded::UpdateMatrix(Matrix34* pMatrix)
{
	//the transition should be moving towards the target in UITransition_Parallel::UpdateMatrix(pMatrix);
	UITransition_Parallel::UpdateMatrix(pMatrix);

	Vector3 maxHeight;
	f32 y = pMatrix->d.y;//prevent clamping of the y value
	maxHeight.Set(pMatrix->d);
	//maxHeight.y = NAVMAP->GetMaxHeight();

	//clamp position so that we don't see outside the city
//	NAVMAP->ClampPositionToBoundsCreatedFromPosition(&maxHeight);

	pMatrix->d.Set(maxHeight.x,y,maxHeight.z);
}

void UITransition_Zoom::OnActivate()
{
	UITransition_Parallel::OnActivate();
}

void UITransition_Zoom::UpdateMatrix(Matrix34* pMatrix)
{	
	UITransition_Parallel::UpdateMatrix(pMatrix);	

	f32 distancePercent = 1 - (pMatrix->d.y/m_StartPos.d.y);

	//lerp position towards cursor based off height
	f32 lerpDistanceX = (m_FinalPos.d.x - pMatrix->d.x) * distancePercent;//X distance we should move towards target based off land distance changed
	f32 lerpDistanceZ = (m_FinalPos.d.z - pMatrix->d.z) * distancePercent;//Z distance we should move towards target based off land distance changed

	pMatrix->d.x += lerpDistanceX;
	pMatrix->d.z += lerpDistanceZ;
}

bool UITransition_FarAwayTarget::IsOnScreen(Matrix34* /*pMatrix*/)
{
	//Vector3 screenPosition;

	//RENDERER.GetMainCameraViewport()->Transform(pMatrix->d,screenPosition.x,screenPosition.y);
	//if (screenPosition.x >0 && screenPosition.x<RENDERER.GetMainCameraViewport()->GetWidth() &&
	//	screenPosition.y >0 && screenPosition.y<RENDERER.GetMainCameraViewport()->GetHeight()
	//	)
	//{
	//	return true;
	//}
	//else
	{
		return false;
	}
}

void UITransition_FarAwayTarget::UpdateMatrix(Matrix34* pMatrix)
{
	UITransition_Parallel::UpdateMatrix(pMatrix);

	//translate until target is on screen
	if (IsOnScreen(&m_FinalPos))
	{
		SetComplete(true);
	}
}

void UITransition_Translate_Spline_ExitSharpAngle::PostLoad()
{
	UITransition_Translate_Spline::PostLoad();

	m_NumVertices = 3;
	m_CubicCurve.AllocateVertices(m_NumVertices);
	m_CubicCurve.SetNumVertices(m_NumVertices);	
	m_CubicCurve.SetLooping(false);

	m_FinalPos.Zero();

	distanceSide = 100.0f;
	distanceLook = -150.0f;
}

void UITransition_Translate_Spline_ExitSharpAngle::OnActivate()
{
	UITransition_Translate_Spline::OnActivate();

	rage::f32 angleFromTargetLook = m_FinalPos.c.AngleY(m_StartPos.c);
	rage::f32 whichSide = 1.0f;	
	if (angleFromTargetLook < 0.0f) {whichSide = -1.0f;}

	rage::Vector3 &v0 = m_CubicCurve.GetVertex(0);
	v0.Set(m_StartPos.d);

	//set v2 before v1 since v1 needs to be between v0 and v2
	rage::Vector3 &v2 = m_CubicCurve.GetVertex(2);
	v2.Set(m_FinalPos.d);

	//create a cv that is 2/3 between v0 and v2
	rage::Vector3 &v1 = m_CubicCurve.GetVertex(1);
	v1.Set(v2);
	AddValue(FINAL_SIDE,distanceSide*whichSide,&v1);
	AddValue(ALONG_FINAL_LOOK,distanceLook,&v1);

	m_CubicCurve.PostInit();
	m_CubicCurve.SetLooping(false);

	Reset();	

	rage::Vector3 temp;
	f32 seg1Distance;
	m_TargetPosition = GetStaticDistance();
	temp.Subtract( v1 , v0 );
	seg1Distance = temp.Mag();
	m_TargetPosition += seg1Distance;

	//convert distances to t values
	for(int i=1;i<m_NumVertices-1;i++)
	{
		m_VertexTValues[i] += seg1Distance;
		m_VertexTValues[i] /= m_TargetPosition;
	}

	m_VertexTValues[m_NumVertices-1] = 1.0f;
}

//
//void UITransition_Parallel_Rotations::SetFinalMatrix(const Matrix34*)
//{
//	//hack the final matrix in here for now (this will only be used for maxheight transition
//	Vector3 cityCenter(*NAVMAP->GetCityBounds()->GetCityCenter());
//	Vector3 cityCenterLookAt(cityCenter);
//	cityCenterLookAt.z -= 10.0f;
//
//	m_FinalPos.LookAt(cityCenter,cityCenterLookAt,Vector3(0,1,0));
//
//	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
//	{
//		(verify_cast<UITransition*>(pChild))->SetFinalMatrix(&m_FinalPos);
//	}
//}
//

UITransition_StreamFlashMovie::UITransition_StreamFlashMovie()
{
	m_pMovie = NULL;
}

void UITransition_StreamFlashMovie::PostLoad()
{
	UITransition::PostLoad();

	//UI_REGISTER(UITransition_StreamFlashMovie, SetMovie);
}

void UITransition_StreamFlashMovie::PlaceMovie(void *pStreamable, rage::datResourceMap &Map, void * pThis)
{
	fuiMovie::PlaceMovie(pStreamable, Map, (reinterpret_cast<UITransition_StreamFlashMovie*>(pThis))->m_pMovie);
	(reinterpret_cast<UITransition_StreamFlashMovie*>(pThis))->SetComplete(true);

#if __DEV
	iObject = sysMemAllocator::GetCurrent().GetMemoryAvailable();
	iAfter = sysMemAllocator::GetCurrent().GetMemoryAvailable();
	iDiff = iBefore - iAfter;
	UIDebugf1("Before loading : %d, After: %d, Diff: %d, Obj: %d",iBefore,iAfter,iDiff,iObject - iAfter);
	UIDebugf1("After loading movie Available mem:\n   Game heap : %d (%d biggest block)",
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetMemoryAvailable(),
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetLargestAvailableBlock() );
#endif

}

void UITransition_StreamFlashMovie::Stream()
{
	GRAB_FLASH_UPDATE_SEMA();

#if __DEV
	iBefore = sysMemAllocator::GetCurrent().GetMemoryAvailable();
	iBlock = sysMemAllocator::GetCurrent().GetLargestAvailableBlock();
	iUsed = sysMemAllocator::GetCurrent().GetMemoryUsed(-1);

	UIDebugf1("Loading movie %s, available mem: %d (block %d) (%d used)",m_pMovie->GetName(),iBefore, iBlock, iUsed);
	UIDebugf1("Loading movie %s Available mem:\n   Game heap : %d (%d biggest block)",m_pMovie->GetName(),
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetMemoryAvailable(),
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetLargestAvailableBlock() );
#endif

	if (m_pMovie->IsLoaded() == false)
	{
		m_pMovie->GetStreamable()->Schedule(0.0f, PlaceMovie, (void *)this);
	}
	else
	{
		SetComplete(true);
	}
}

void UITransition_StreamFlashMovie::OnFocused()
{
	UITransition::OnFocused();
	if (m_pMovie)
	{
		Stream();
	}
	else
	{
		SetComplete(true);
	}
}

bool UITransition_StreamFlashMovie::ProcessDataTag(const char* name , const char* value)
{
	if(stricmp(name,"movie")==0)
	{
		fuiMovie* pMovie = FLASHMANAGER->GetMovie(value);
		if (pMovie)
		{
			SetMovie(pMovie);
		}
	}
	else
	{
		return UITransition::ProcessDataTag(name,value);
	}

	return true;
}

void UITransition_StreamFlashMovie::SetMovie(fuiMovie* pMovie)
{
	m_pMovie = pMovie;
}

}//namespace rage

