// 
// flashUIComponents/Components.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
// Many of these components are modeled after Javas Swing.  Much of the documentation is provided
// by the Javax/swing api documentation

#include "diag/output.h"

#include "system\nelem.h"
#include "system/timemgr.h"
#include "fuicore/EventManager.h"
#include "fuicore/UIManager.h"
#include "fuicore/Common.h"
#include "fuicomponents/Components.h"
#include "fuicomponents/Transitions.h"
#include "UIContexts.h"

#if USEFLASH
#include "flash/file.h"
#include "fuiflash/Flash.h"
#include "fuiflash/FlashUIContexts.h"
#endif

#if __PPU
#include <sysutil/sysutil_common.h>
#include <sysutil/sysutil_sysparam.h>
#endif

#if __DEV
static int s_NumComponents=0;
static int s_NumMessageBoxes=0;
#endif


using namespace rage;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIComponent
//The base class for all components . To use a component that inherits from UIComponent, 
//you must place the component in a containment hierarchy whose root is a top-level 
//container. Top-level containers -- such as UILayer and UIScene -- are specialized components 
//that provide a place for other components to paint themselves. For an explanation of 
//containment hierarchies, see Swing Components and the Containment Hierarchy, a section in 
//The Java Tutorial http://java.sun.com/docs/books/tutorial/uiswing/learn/index.html.
//
//The UIComponent class provides:
//
//	* The base class for both standard and custom components that use this UI architecture.
//	* A "pluggable look and feel" (L&F) that can be specified by artists.  FLASH is the default
//    tool that is used to visually represent the UI
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UIComponent::UIComponent() 
: UIObject()
, m_RefreshFrequencyFlags	(ONFOCUSED|ONTRANSITION)
, m_pContext			(NULL)
, m_TimeBeforeExit		(0.0f)
, m_bAutoExit			(false)
{
	//start hidden by default.  I'm not calling SetVisible cause i don't want all the "stuff" that comes along with that
	RemoveProperties(UI_VISIBLE);

#if __DEV
	s_NumComponents++;
#endif
}

bool UIComponent::ProcessDataTag(const char* name ,const char* value)
{
	if(stricmp(name,"refresh")==0)
	{
		if(stricmp(value,"onfocused")==0)
		{
			m_RefreshFrequencyFlags = ONFOCUSED;
		}
		else if(stricmp(value,"onevent")==0)
		{
			m_RefreshFrequencyFlags = ONEVENT;
		}
		else if(stricmp(value,"ontransition")==0)
		{
			m_RefreshFrequencyFlags = ONTRANSITION;
		}
		else if(stricmp(value,"all")==0)
		{
			m_RefreshFrequencyFlags = 0xFF;
		}
	}
	else if(stricmp(name,"InTransition")==0)
	{
		//SetInTransition( verify_cast<UITransition*>(UIMANAGER->GetUINavigator()->GetStateByIdName(value)) );
	}
	else if(stricmp(name,"OutTransition")==0)
	{
		//SetOutTransition(verify_cast<UITransition*>(UIMANAGER->GetUINavigator()->GetStateByIdName(value)));
	}
	else
	{
		if (GetContext()){m_pContext->ProcessDataTag(name,value);}
		return UIObject::ProcessDataTag(name,value);
	}

	return true;
}

void UIComponent::PostStream(char movieID)
{
	if (GetContext()){m_pContext->PostStream(movieID);}
	UIObject::PostStream(movieID);
}

void UIComponent::PostLoad()
{
	if (GetContext()){m_pContext->PostLoad();}
	UIObject::PostLoad();

	UI_REGISTER(UIComponent,SetAutoExitTime);
}

void UIComponent::OnShow()
{
	if (GetContext()){m_pContext->OnShow();}
	UIObject::OnShow();
}

void UIComponent::OnHide()
{
	if (GetContext()){m_pContext->OnHide();}
	UIObject::OnHide();
}

void UIComponent::OnActivate()
{
	if (GetContext()){m_pContext->OnActivate();}
	UIObject::OnActivate();
}

void UIComponent::OnDeactivate()
{
	m_bAutoExit = false;
	if (GetContext()){m_pContext->OnDeactivate();}
	UIObject::OnDeactivate();
}

void UIComponent::OnPaint()
{
	UIObject::OnPaint();
}

void UIComponent::OnFocused()
{
	UIObject::OnFocused();

	if(IsVisible() && m_RefreshFrequencyFlags & ONFOCUSED)
	{
		Refresh();
	}
}

bool UIComponent::HandleEvents(fuiEvent* pEvent)
{
	bool eventConsumed = false;

	eventConsumed |= UIObject::HandleEvents(pEvent);

	if (m_RefreshFrequencyFlags & ONEVENT)
	{
		Refresh();
	}
	return eventConsumed;
}

UIContext* UIComponent::GetContext()
{
	Assertf(m_pContext || !UIMANAGER->IsUILoaded() ,"No context created for this component.  Please verify that you have instantiated this component in parents constructore and added it as a child in its parents constructor.  OR call Init() on the component right after instantiation.");
	return m_pContext;
}

void UIComponent::Reset()
{
	UIObject::Reset();
	if (m_pContext) m_pContext->ResetFlashObjectToDefaults();
}

void UIComponent::AddChild(UIObject* pChild)
{
	UIObject::AddChild(pChild);
	if (m_pContext) m_pContext->Reset();//need to now refresh the contexts
	if (m_pContext) m_pContext->Refresh(1);//need to now refresh the contexts
	Refresh();
}

void UIComponent::RemoveChild(UIObject* pChild)
{
	UIObject::RemoveChild(pChild);
	if (m_pContext) m_pContext->Reset();//need to now refresh the contexts
	if (m_pContext) m_pContext->Refresh(1);//need to now refresh the contexts
	Refresh();
}

void UIComponent::RemoveChild(u32 pChild)
{
	UIObject::RemoveChild(pChild);
	if (m_pContext) m_pContext->Reset();//need to now refresh the contexts
	if (m_pContext) m_pContext->Refresh(1);//need to now refresh the contexts
	Refresh();
}

void UIComponent::Update()
{
	//check to see if we want to auto exit this object
	if (m_bAutoExit)
	{
		m_TimeBeforeExit -= TIME.GetUnwarpedRealtimeSeconds();

		if(m_TimeBeforeExit <= 0.0f)
		{
			Exit();
		}
	}

	UIObject::Update();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UILayer 
//UILayer is a generic lightweight container.  It is a container that groups any number of components
//All children are managed by this component
//UILayers can contain other UILayers
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UILayer::UILayer() : 
UIComponent(),
m_pInTransition(NULL),
m_pOutTransition(NULL)
{
}

void UILayer::Init()
{
#ifdef USEFLASH
	Assertf(!m_pContext,"somethign not right. somebody already instantiated the context");
	m_pContext = rage_new fuiLayerContext(this);
#endif

	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
	UIObject::Init();
}

bool UILayer::IsTransitioning()
{
	return (IsTransitioningIn() || IsTransitioningOut());
}

bool UILayer::IsTransitioningIn()
{
	return (m_pInTransition && m_pInTransition->IsActive());
}

bool UILayer::IsTransitioningOut()
{
	return (m_pOutTransition && m_pOutTransition->IsActive());
}

bool UILayer::ProcessDataTag(const char* name ,const char* value)
{
	if(stricmp(name,"InTransition")==0)
	{
		SetInTransition( verify_cast<UITransition*>(UIFACTORY->GetUIObject(value) ));
	}
	else if(stricmp(name,"OutTransition")==0)
	{
		SetOutTransition(verify_cast<UITransition*>(UIFACTORY->GetUIObject(value)));
	}
	else
	{
		UIComponent::ProcessDataTag(name,value);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIPanel
//UIPanel is a generic lightweight container.  It is a container that groups any number of components
//All children are managed by this component
//In many types of look and feel, panels are opaque by default. Opaque panels work well as content panes and can help 
//with painting efficiently. A transparent panel draws no background, so that any components underneath show through. 
//UIPanels set their visible and active flags to all their children by default.  It only sets the first childs focused flag by default.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//void UIPanel::Init()
//{
//#ifdef USEFLASH
//	Assertf(!m_pContext,"somethign not right. somebody already instantiated the context");
//	m_pContext = rage_new fuiPanelContext(this);
//#endif
//
//	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
//	UIObject::Init();
//}

void UIPanel::SetActive(bool b)
{
	UILayer::SetActive(b);

	UIObject* pChild;

	//activate all kids
	for(rage::u32 i=0;i<GetNumberOfChildren();i++)
	{
		pChild = GetChild(i);
		if (pChild)
			pChild->SetActive(b);
	}
}

void UIPanel::SetFocused(bool b)
{
	UILayer::SetFocused(b);

	UIObject* pChild;

	//show all children by default
	for(rage::u32 i=0;i<GetNumberOfChildren();i++)
	{
		pChild = GetChild(i);
		if (pChild)
			pChild->SetFocused(b);
	}
}

void UIPanel::SetVisible(bool b)
{
	UILayer::SetVisible(b);

	UIObject* pChild;

	//show all children by default
	for(rage::u32 i=0;i<GetNumberOfChildren();i++)
	{
		pChild = GetChild(i);
		if (pChild)
			pChild->SetVisible(b);
	}
}

void UIPanel::Refresh()
{
	if (GetContext())
	{
		UILayer::Refresh();
		UIObject* pChild;
		//Paint all children by default
		for(rage::u32 i=0;i<GetNumberOfChildren();i++)
		{
			pChild = GetChild(i);
			if (pChild)
				pChild->Refresh();
		}
	}
}

void UIPanel::Update()
{
	//check to see if we want to auto exit this object
	if (m_bAutoExit)
	{
		m_TimeBeforeExit -= TIME.GetUnwarpedRealtimeSeconds();

		if(m_TimeBeforeExit <= 0.0f)
		{
			Exit();
		}
	}

	//paint this object before its children
	if (m_bRefresh && IsVisible())
	{
		m_bRefresh = 0;
		OnPaint();
	}

	UIObject* pChild;

	//TODO: make sure we set up the visible system correctly.  (We don't want to update everything)

	//update all children
	for (pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->GetNextSibling())
	{
			pChild->Update();
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIScene
//UIScene is a container that groups any number of components
//All UIScenes should be mutually exclusive from all other UIScenes.  This allows us to optimize accordingly.
//For example,  A garage scene may be streamed out when not in the garage.
//UIScene should NOT contain other UIScenes
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void UIScene::Goto()
{
	UIMANAGER->RequestTransition(this);
}

//UIContext* UIScene::GetContext()
//{
//	return m_pContext;
//}

//UITransition* UILayer::GetCurrentTransition()
//{
//	//return first transition from me to all my children that is active
//	if (m_pInTransition && m_pInTransition->IsActive())
//	{
//		return m_pInTransition;
//	}
//
//	if (m_pOutTransition && m_pOutTransition->IsActive())
//	{
//		return m_pOutTransition;
//	}
//
//	for (UIObject* pChild=GetChild(0); pChild; pChild=pChild->GetNextSibling() )
//	{
//		UITransition* pTransition = verify_cast<UITransition*>(pChild)->GetCurrentTransition();
//		if (pTransition)
//		{
//			return pTransition;
//		}
//	}
//
//	return NULL;
//}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIList
//A component that allows the user to select one or more objects from a list.
//UIList supports scrolling directly.  By default the UIList selection model allows only 1 
//of the items to be selected at a time.  The UIList contents does not have to be homogeneous.  Items
//in the list may be different types of components.  However they must all inherit from UIObject.
//UIList supports wrapping.  Note that the size of this list does not necessarily equal the number
//children.  For example,  if there are 10 children, but 2 items in the list, then we may visually
//represent this as a list of 10 components where 2 or populated and 8 are empty.  Alternatively, 
//we may have 10 children and 20 items in the list.  This would possibly display 10 components with
//a scrollbar that allows the player to scroll through the 20 items.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UIList::UIList() 
: UIComponent()
, m_CurrentSelection	(0)
, m_Orientation			(VERTICAL)
, m_NumListItems		(0)
, m_NumVisibleListItems	(15)
, m_bAllowWrap			(true)
, m_bAllowRepeat		(false)
, m_bProcessRepeatScrolling(false)
, m_TimeSinceLastRepeat (0.0f)
{

	SetIsListOfStrings(true);
	m_bIsScrollingEnabled = false;
	m_bIsSelectionEnabled = true;   
	m_FirstItemInList = 0;
	m_RepeatDelay = .5f;//seconds to continue repeating
	m_RepeatStartDelay = 1.0f;//seconds to start repeating
}

#if __BANK
void UIList::AddWidgets( rage::bkBank & bk )
{
	bk.AddText( "number of children", &m_NumListItems , false); 
	bk.AddText( "number of visible children", &m_NumListItems , false); 
	bk.AddText( "current Selection", &m_CurrentSelection , false); 
	bk.AddSlider("Delay to start repeating",&m_RepeatStartDelay,0.0f,2.0f,.1f);
	bk.AddSlider("Delay to continue repeating",&m_RepeatDelay,0.0f,2.0f,.1f);

	UIObject::AddWidgets(bk);
}
#endif

void UIList::Init()
{
#ifdef USEFLASH
	Assertf(!m_pContext,"somethign not right. somebody already instantiated the context");
	m_pContext = rage_new fuiListContext(this);
#endif

	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
	UIObject::Init();
	m_Scrollbar.Init();
}

void UIList::OnFocused()
{
	UIComponent::OnFocused();

	UIObject* pChild;

	//focus a list item
	if (m_CurrentSelection >= 0 && m_CurrentSelection < (rage::s32)GetNumberOfChildren())
	{
		pChild = GetChild(m_CurrentSelection);
		pChild->SetFocused(true);
	}
	Refresh();

	if (m_pFirstEventHandler)
	{
		UIMANAGER->ComponentFocused();
	}
}

void UIList::OnExit()
{
	UIComponent::OnExit();

	//reset some vars.  I reset OnExit because we may want to setup a UIList before it is "entered".  This is why i don't do it in OnEnter()
	m_CurrentSelection	= 0;
	m_FirstItemInList = 0;

	m_Scrollbar.Exit();
}

void UIList::OnUnfocused()
{
	UIComponent::OnUnfocused();

	UIObject* pChild;

	//hide all list items
	for(rage::u32 i=0;i<GetNumberOfChildren();i++)
	{
		pChild = GetChild(i);
		if (pChild)
		{
			pChild->SetFocused(false);
		}
	}

	if (m_pFirstEventHandler)
	{
		UIMANAGER->ComponentUnfocused();
	}
}

void UIList::OnActivate()
{
	m_Scrollbar.Activate();

	UIComponent::OnActivate();

	UIObject* pChild;

	if(GetNumberOfVisibleChildren())
	{
		SetLength( GetNumberOfVisibleChildren() );
	}

	//activate all kids
	for(rage::u32 i=0;i<GetNumberOfChildren();i++)
	{
		pChild = GetChild(i);
		if (pChild)
			pChild->Activate();
	}
}

void UIList::OnDeactivate()
{
	m_Scrollbar.Deactivate();

	UIComponent::OnDeactivate();

	UIObject* pChild;

	//Deactivate kids
	for(rage::u32 i=0;i<GetNumberOfChildren();i++)
	{
		pChild = GetChild(i);
		if (pChild)
			pChild->Deactivate();
	}
}

bool UIList::ProcessDataTag(const char* name , const char* value)
{
	if (stricmp(name,"MaxHeight")==0)
	{
		m_NumVisibleListItems = atoi(value);
	}
	else if (stricmp(name,"scrollable")==0)
	{
		m_bIsScrollingEnabled = true;		
	}
	else if (stricmp(name,"vertical")==0)
	{
		m_Orientation = VERTICAL;
	}
	else if (stricmp(name,"horizontal")==0)
	{
		m_Orientation = HORIZONTAL;
	}
	else if (stricmp(name,"startRepeatDelay")==0)
	{
		m_RepeatStartDelay = (f32)atof(value);
	}
	else if (stricmp(name,"repeatDelay")==0)
	{
		m_RepeatDelay = (f32)atof(value);
	}
	else
	{
		m_Scrollbar.ProcessDataTag(name,value);
		return UIComponent::ProcessDataTag(name,value);
	}

	return true;
}

rage::s32 UIList::GetCurrentSelection() const
{
	/*
	if (m_CurrentSelection < 0 && m_NumListItems > 0)
	{
		//set the initial selection
		SetCurrentSelection(0);
	}
	*/

	return m_CurrentSelection;
}

void UIList::OnTransitionComplete()
{
	UIComponent::OnTransitionComplete();

	//focus current selection
	if( m_CurrentSelection >= 0 && m_CurrentSelection < (rage::s32)GetNumberOfChildren() )
		UIMANAGER->RequestTransition(GetChild(m_CurrentSelection),UIObject::UI_FOCUSED);
}

void UIList::OnShow()
{
	UIComponent::OnShow();

	//show children up to the max visible allowed
	UIObject*	pListItem = GetChild(0);
	int i=0;
	while(pListItem && i!=m_NumVisibleListItems)
	{
		pListItem->SetVisible(true);
		pListItem = pListItem->GetNextSibling();
		i++;
	}
}

void UIList::OnHide()
{
	m_Scrollbar.Hide();

	UIComponent::OnHide();

	UIObject*	pListItem = GetChild(0);
	while(pListItem)
	{
		pListItem->SetVisible(false);
		pListItem = pListItem->GetNextSibling();
	}
}

void UIList::HideAllRows()
{
	UIObject*	pListItem = GetChild(0);
	while(pListItem)
	{
		pListItem->SetVisible(false);
		pListItem = pListItem->GetNextSibling();
	}
}

void UIList::Update()
{
	UIComponent::Update();

	if (m_bProcessRepeatScrolling)
	{
		m_TimeSinceLastRepeat+=TIME.GetUnwarpedRealtimeSeconds();
		if(m_RepeatDelay<=m_TimeSinceLastRepeat)
		{
			m_TimeSinceLastRepeat = 0.0f;
			m_bAllowRepeat = true;
		}
	}
}

void UIList::OnPaint()
{
	UIListContext* pListContext = dynamic_cast<UIListContext*>(m_pContext);

	if(pListContext)
	{
		if (m_bChildrenChanged)
		{
			pListContext->Reset();
			pListContext->Refresh(2);
		}
		m_bChildrenChanged=false;

		UIObject*	pListItem = GetChild(m_FirstItemInList);
		while(pListItem)
		{
			pListItem->Refresh();
			pListItem = pListItem->GetNextSibling();
		}

		if (m_bIsScrollingEnabled)
		{
			//scrollbar setup
			if (m_NumListItems > m_NumVisibleListItems)
			{
				m_Scrollbar.SetVisible(true);
				m_Scrollbar.SetValue((rage::f32)GetCurrentSelection() / (rage::f32)(m_NumListItems - 1));
			}
			else
			{
				m_Scrollbar.SetVisible(false);
			}
		}
		else
		{
			//scrollbar setup
			m_Scrollbar.SetVisible(false);

			//resize the list to be the size of the number of visible children
			m_NumVisibleListItems = GetNumberOfVisibleChildren();
		}

		pListContext->SetNumVisibleListItems(m_NumVisibleListItems);
		//pListContext->SetNumListItems(m_NumListItems);
		pListContext->SetCurrentSelection(GetRelativeSelection());

		//Tell Visualization which menu is focused and visible
		if (dynamic_cast<UIList*>(GetParent())==NULL)
		{
			pListContext->SetFocused(IsFocused());
			pListContext->SetVisible(IsVisible());
		}
	}

	m_Scrollbar.OnPaint();
	UIComponent::OnPaint();
}

void UIList::SetListItem(rage::u32 row , UIObject* /*pData*/)
{
	if(m_NumListItems < (rage::s32)row+1)
	{
		m_NumListItems = row+1;
	}
}

void UIList::AddChild(UIObject* pChild) 
{
	UIComponent::AddChild(pChild);

	m_bChildrenChanged=true;
	m_NumListItems++;
	ProcessScrolling();
}

void UIList::RemoveChild(UIObject* pChildToRemove) 
{
	UIObject* pChild;
	rage::u32 numChildren = GetNumberOfChildren();
	for (rage::u32 index=0;index<numChildren;index++)
	{
		pChild=GetChild(index);
		if (pChild == pChildToRemove)
		{
			RemoveChild(index);
			break;
		}
	}

	--m_NumListItems;
	m_bChildrenChanged=true;
	Refresh();
}

void UIList::RemoveChild(rage::u32 index) 
{
	UIComponent::RemoveChild(index);

	--m_NumListItems;
	ProcessScrolling();
	Refresh();
}

//void UIList::AppendToList(rage::atArray<const char*> /*&pData*/)
//{
//	//if(GetContext() && GetMenu())
//	//{
//	//	NotifyTransition( 1 );
//	//	rage::swfVARIANT  varTargetProperty;
//	//	GetMenu()->GetProperty("elements",varTargetProperty);
//
//	//	rage::swfVARIANT&  varRow = (*(rage::swfSCRIPTARRAY*)varTargetProperty.asObject())[m_NumListItems];
//	//	for(int i =0; i < pData.GetCount(); ++i)
//	//	{
//	//		fuiMovie::SetProperty(varRow, getFlashColumnPropertyString(i), UIStringTable->GetStringUTF8(pData[i]), m_pContext);
//	//	}
//		
//		++m_NumListItems;
////	}
//}

void UIList::GetListItem(rage::u32 /*row*/ , rage::u32 /*column*/, char* /*buffer*/)
{
	//if (GetMenu())
	//{
	//   rage::swfVARIANT  varTargetProperty;
	//   GetMenu()->GetProperty("elements",varTargetProperty);

	//   rage::swfVARIANT&  varRow = (*(rage::swfSCRIPTARRAY*)varTargetProperty.asObject())[row];
	//   fuiMovie::GetProperty(varRow, getFlashColumnPropertyString(column), buffer);
	//}
}

void UIList::Reset()
{  
	UIComponent::Reset();
	m_NumListItems = 0;
	SetLength(0);
}

void UIList::SetLength(int length)
{
	m_NumListItems = length;
	//CopyLength(GetMenu(),length);
}

void UIList::OnSelectionChanged()
{
	Refresh();

	UIMANAGER->SendEvent("UI_ListSelectionChanged");
}

void UIList::OnScroll()
{
	//refresh component vars		
	int i=0;
	for (UIObject* pChild=GetChild(0); pChild; pChild=pChild->GetNextSibling() )
	{
		//only show items that are visible
		if (i>=m_FirstItemInList && i<=GetLastItemInList())
		{
			pChild->SetVisible(true);
		}
		else
		{
			pChild->SetVisible(false);
		}
		i++;
	}

	dynamic_cast<UIListContext*>(m_pContext)->OnScroll();
	Refresh();
}

rage::s32 UIList::GetLastItemInList()
{
	if (m_FirstItemInList + m_NumVisibleListItems > m_NumListItems)
	{
		return m_NumListItems-1;
	}
	else
	{
		return m_FirstItemInList + m_NumVisibleListItems - 1;
	}
}

void UIList::ProcessScrolling()
{
	if (m_bIsScrollingEnabled)
	{
		//did we select an item that is past the end of the list?
		if (m_CurrentSelection > GetLastItemInList())
		{
			Scroll(m_CurrentSelection - GetLastItemInList());
		}
		else if (m_CurrentSelection < m_FirstItemInList)
		{
			Scroll(m_CurrentSelection - m_FirstItemInList);
		}
	}
}

//used if flash is managing horizontal or vertical scrolling
//scroll forward or backward by the given numChildren
void UIList::Scroll(rage::s32 numChildren)
{
	//update flash with the new list of data
	m_FirstItemInList += numChildren;

	if (!m_bAllowWrap)
	{
		//test to see if we need to wrap the list items
		if (m_NumListItems - m_CurrentSelection > m_NumVisibleListItems)
		{
			//we hit the end of the list
			m_FirstItemInList = m_NumListItems - m_NumVisibleListItems;
		}
		else if (m_FirstItemInList < 0)
		{
			m_FirstItemInList = 0;
		}
	}

	OnScroll();
}

void UIList::SetNumVisibleItems(int length)
{	
	m_NumVisibleListItems = length;
}

void UIList::SelectPrevious()
{
	if (m_bIsSelectionEnabled && m_NumListItems)
	{
		if (SetCurrentSelection(GetSelectableOffset(-1)))
		{
			UIMANAGER->SendEvent("UI_ListScrolledPrevious");
		}
		else
		{
			UIMANAGER->SendEvent("UI_ListScrolledError");
		}
	}
}

void UIList::SelectNext()
{ 
	if (m_bIsSelectionEnabled && m_NumListItems)
	{
		if (SetCurrentSelection(GetSelectableOffset(+1)))
		{
			UIMANAGER->SendEvent("UI_ListScrolledNext");
		}
		else
		{
			UIMANAGER->SendEvent("UI_ListScrolledError");
		}
	}
}

bool UIList::SetCurrentSelection(rage::s32 iNewSelection)
{
	//select
	if (iNewSelection != m_CurrentSelection)
	{
		if(m_CurrentSelection>=0)
			GetChild(m_CurrentSelection)->Unfocus();

		if(iNewSelection>=0 && iNewSelection < (rage::s32)GetNumberOfChildren())
			GetChild(iNewSelection)->Focus();

		UIDebugf1("List selection changed from %d to %d in %s",m_CurrentSelection,iNewSelection,GetName());

		m_CurrentSelection = iNewSelection;

		ProcessScrolling();
		OnSelectionChanged();

		return true;
	}
	return false;
}


rage::u32 UIList::GetSelectableOffset(int offset) const
{
	int index = m_CurrentSelection;

	if (m_NumListItems > 0)
	{
		if(offset<0)
		{
			if(index==0 && m_bAllowWrap)
			{
				index = m_NumListItems-1;
			}
			else if (index==0 && !m_bAllowWrap)
			{
				//do nothing
			}
			else
			{
				index--;
			}	
		}
		else if(offset>0)
		{
			if(index==m_NumListItems-1 && m_bAllowWrap)
			{
				index = 0;
			}
			else if (index==m_NumListItems-1 && !m_bAllowWrap)
			{
				//do nothing
			}
			else
			{
				index++;
			}	
		}
	}

	return index;
}

void UIList::SetTemplate(int i)
{
	if (m_pContext){GetContext()->SetTemplate(i);}
}

bool UIList::HandleInputs(fuiEvent* pEvent)
{
	bool eventConsumed = false;
	m_bProcessRepeatScrolling = false;

	//let children handle the event first
	eventConsumed |= UIComponent::HandleInputs(pEvent);

	//lets see if we want to handle the event
	if(!eventConsumed)
	{
		//this must be an input event type
		if(	pEvent == FUIEVENTMGR->GetEvent("lAnalogUp") || pEvent == FUIEVENTMGR->GetEvent("Up"))
		{
			if(GetScrollOrientation() == VERTICAL)
			{			
				//start the repeat delay timer just in case player is holding down the button
				m_TimeSinceLastRepeat -= (m_RepeatStartDelay - m_RepeatDelay);
				SelectPrevious();
				eventConsumed = true;
			}
		}
		else if(pEvent == FUIEVENTMGR->GetEvent("lAnalogDown") || pEvent == FUIEVENTMGR->GetEvent("Down"))
		{
			if(GetScrollOrientation() == VERTICAL)
			{
				//start the repeat delay timer just in case player is holding down the button
				m_TimeSinceLastRepeat -= (m_RepeatStartDelay - m_RepeatDelay);
				SelectNext();
				eventConsumed = true;
			}
		}
		else if(pEvent == FUIEVENTMGR->GetEvent("lAnalogLeft") || pEvent == FUIEVENTMGR->GetEvent("Left"))
		{
			if(GetScrollOrientation() == HORIZONTAL)
			{
				//start the repeat delay timer just in case player is holding down the button
				m_TimeSinceLastRepeat -= (m_RepeatStartDelay - m_RepeatDelay);
				SelectPrevious();
				eventConsumed = true;
			}
		}
		else if(pEvent == FUIEVENTMGR->GetEvent("lAnalogLeft") || pEvent == FUIEVENTMGR->GetEvent("Left"))
		{
			if(GetScrollOrientation() == HORIZONTAL)
			{
				//start the repeat delay timer just in case player is holding down the button
				m_TimeSinceLastRepeat -= (m_RepeatStartDelay - m_RepeatDelay);
				SelectNext();
				eventConsumed = true;
			}
		}
		else if(pEvent == FUIEVENTMGR->GetEvent("lAnalogUp_Repeat") || pEvent == FUIEVENTMGR->GetEvent("Up_Repeat"))
		{
			m_bProcessRepeatScrolling = true;
			if(GetScrollOrientation() == VERTICAL)
			{	
				if (m_bAllowRepeat)
				{
					m_TimeSinceLastRepeat = 0.0f;
					m_bAllowRepeat = false;
					SelectPrevious();
				}
				eventConsumed = true;
			}
		}
		else if(pEvent == FUIEVENTMGR->GetEvent("lAnalogDown_Repeat") || pEvent == FUIEVENTMGR->GetEvent("Down_Repeat"))
		{
			m_bProcessRepeatScrolling = true;
			if(GetScrollOrientation() == VERTICAL)
			{
				if (m_bAllowRepeat)
				{
					m_TimeSinceLastRepeat = 0.0f;
					m_bAllowRepeat = false;
					SelectNext();
				}
				eventConsumed = true;
			}
		}
		else if(pEvent == FUIEVENTMGR->GetEvent("lAnalogLeft_Repeat") || pEvent == FUIEVENTMGR->GetEvent("Left_Repeat"))
		{
			m_bProcessRepeatScrolling = true;
			if(GetScrollOrientation() == HORIZONTAL)
			{
				if (m_bAllowRepeat)
				{
					m_TimeSinceLastRepeat = 0.0f;
					m_bAllowRepeat = false;
					SelectPrevious();
				}
				eventConsumed = true;
			}
		}
		else if(pEvent == FUIEVENTMGR->GetEvent("lAnalogRight_Repeat") || pEvent == FUIEVENTMGR->GetEvent("Right_Repeat"))
		{
			m_bProcessRepeatScrolling = true;
			if(GetScrollOrientation() == HORIZONTAL)
			{
				if (m_bAllowRepeat)
				{
					m_TimeSinceLastRepeat = 0.0f;
					m_bAllowRepeat = false;
					SelectNext();
				}
				eventConsumed = true;
			}
		}
	}

	return eventConsumed;
}

void UIList::PostStream(char movieID)
{
	m_Scrollbar.PostStream(movieID);
	UIComponent::PostStream(movieID);

}

void UIList::PostLoad()
{	
	m_Scrollbar.PostLoad();
	UIComponent::PostLoad();

	if (!m_bIsScrollingEnabled)
	{
		SetNumListItems(GetNumberOfChildren());
	}

	UI_REGISTER(UIList, SetTemplate);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UISubList

void UISubList::Init()
{
#ifdef USEFLASH
	Assertf(!m_pContext,"somethign not right. somebody already instantiated the context");
	m_pContext = rage_new fuiSubListContext(this);
#endif

	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
	UIObject::Init();
	m_Scrollbar.Init();
}

void UISubList::SetVisible(bool b)
{
	static int s_count=0;

	//change in visibility? Tell Graphics
	if (IsVisible()!=b)
	{
		if (b)
			s_count++;
		else
			s_count--;

		GetContext()->SetVisible(s_count>0);
	}

	UIList::SetVisible(b);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIMenu

void UIMenu::OnPaint()
{
	UIList::OnPaint();

	if(m_Template>=0)
		GetContext()->SetTemplate(m_Template);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIScrollBar
//An implementation of a scrollbar. The user positions the knob in the scrollbar to determine the contents of the viewing area. 
//The program typically adjusts the display so that the end of the scrollbar represents the end of the displayable contents, 
//or 100% of the contents. The start of the scrollbar is the beginning of the displayable contents, or 0%. The position of the 
//knob within those bounds then translates to the corresponding percentage of the displayable contents.
//
//Typically, as the position of the knob in the scrollbar changes a corresponding change is made to the position of the UIList 
//on the underlying view, changing the contents of the UIList. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UIScrollBar::UIScrollBar() 
: UIComponent()
, m_Value(0.0f)
{
}

void UIScrollBar::Init()
{
#ifdef USEFLASH
	Assertf(!m_pContext,"somethign not right. somebody already instantiated the context");
	m_pContext = rage_new fuiScrollBarContext(this);
#endif

	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
	UIObject::Init();
}

void UIScrollBar::OnPaint()
{
	UIComponent::OnPaint();

	if(GetContext())
	{
		dynamic_cast<UIScrollBarContext*>(m_pContext)->SetValue(m_Value);
	}	
}

void UIScrollBar::Reset()
{
	UIComponent::Reset();
	SetValue(0);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIMessageBox
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

UIMessageBox::UIMessageBox()  
: UIList()
, m_Description		(NULL)
, m_Title			(NULL)
, m_bPauseOnShow	(true)
, m_bWasPaused		(false)
, m_Priority		(0)
, m_Styles (eDefault)
{
#if __DEV
	s_NumMessageBoxes++;
#endif

	m_pPrompts = verify_cast<UIPromptStrip*>(UIFACTORY->NewUIObject("UIPromptStrip"));
	m_pPrompts->SetParent(this);//i want to be his parent, but i don't want him to be my child
	m_pPrompts->SetStyle(UIPromptStrip::eMessageBox);
}

void UIMessageBox::Init()
{
#ifdef USEFLASH
	Assertf(!m_pContext,"something not right. somebody already instantiated the context");
	m_pContext = rage_new fuiMessageBoxContext(this);
#endif

	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
	UIObject::Init();
	m_pPrompts->Init();//ick
	m_Scrollbar.Init();//ick
}

bool UIMessageBox::ProcessDataTag( const char* name , const char* value)
{
	if(stricmp(name,"description")==0)
	{
		SetDescription(value);		
	}
	else if(stricmp(name,"title")==0)
	{
		SetTitle(value);
	}
	else if(stricmp(name,"priority")==0)
	{
		if(stricmp(value,"1")==0)
		{
			m_Priority = 1;//high
		}
		else
		{
			m_Priority = 0;//low
		}
	}
	else if(stricmp(name,"pause")==0)
	{
		m_bPauseOnShow = true;
	}
	else if(stricmp(name,"Error")==0)
	{
		m_pPrompts->AddPrompt(UIPromptStrip::OK);
		SetTitle("Common_Error");
	}
	else if(stricmp(name,"Warning")==0)
	{
		m_pPrompts->AddPrompt(UIPromptStrip::OK);
		SetTitle("Common_Warning");
	}
	else if(stricmp(name,"Continue")==0)
	{
		m_pPrompts->AddPrompt(UIPromptStrip::Continue);
		SetTitle("Common_Message");
	}
	else if(stricmp(name,"Message")==0)
	{
		m_pPrompts->AddPrompt(UIPromptStrip::OK);
		SetTitle("Common_Message");
	}
	else if(stricmp(name,"Process")==0)
	{
		SetTitle("Common_PleaseWait");
	}
	else if(stricmp(name,"Alert")==0)
	{
		m_pPrompts->AddPrompt(UIPromptStrip::OK);
		SetTitle("Common_Alert");
	}	
	else if(stricmp(name,"Ok_Cancel")==0)
	{
		
		m_pPrompts->AddPrompt(UIPromptStrip::OK);
		m_pPrompts->AddPrompt(UIPromptStrip::Cancel);
		SetTitle("Common_Message");
	}
	else
	{
		m_pPrompts->ProcessDataTag(name,value);
		UIList::ProcessDataTag(name,value);
	}
	return true;
}

void UIMessageBox::ShowDialog(const char* name)
{
	UIObject* pObject = verify_cast<UIObject*>(UIMANAGER->GetUIObject(name));
	if (pObject)
	{
		pObject->Enter();
	}
}

void UIMessageBox::Goto()
{
	if (m_Priority > 0 )
	{
		SetVisible(true);
		SetFocused(true);	
	}

	Activate();
}

void UIMessageBox::SetTitle ( const char* title )
{
	m_Title = GET_STRING(title);
}

void UIMessageBox::SetDescription ( const char* desc )
{
	m_Description = GET_STRING(desc);
}

void UIMessageBox::OnActivate()
{
	UIList::OnActivate();
	if (m_Priority == 1)
	{
		UIMANAGER->GetUINavigator()->PushPopup(this,1);
	}
	else
	{
		UIMANAGER->GetUINavigator()->PushPopup(this,0);
	}

	m_pPrompts->Activate();
}

void UIMessageBox::OnDeactivate()
{
	UIList::OnDeactivate();
	UIMANAGER->GetUINavigator()->PopPopup(this);
	m_pPrompts->Deactivate();
}

void UIMessageBox::OnShow()
{
	UIList::OnShow();
	m_pPrompts->Show();
}

void UIMessageBox::OnHide()
{
	UIList::OnHide();
	m_pPrompts->Hide();
}

void UIMessageBox::Refresh()
{
	UIList::Refresh();
	m_pPrompts->Refresh();
}

void UIMessageBox::Update()
{
	UIList::Update();
	m_pPrompts->Update();
}

void UIMessageBox::OnPaint()
{
	if (IsVisible() && GetContext())
	{		
		UIList::OnPaint();

		dynamic_cast<UIMessageBoxContext*>(m_pContext)->SetStyle(int(m_Styles));

		dynamic_cast<UIMessageBoxContext*>(m_pContext)->SetDescription(m_Description);

		if (m_Title)
		{
			dynamic_cast<UIMessageBoxContext*>(m_pContext)->SetTitle(m_Title);
		}
		else
		{
			dynamic_cast<UIMessageBoxContext*>(m_pContext)->SetTitle("I NEED A TITLE");
		}

		dynamic_cast<UIMessageBoxContext*>(m_pContext)->SetVisible(IsVisible());
	}
}

bool UIMessageBox::HandleInputs(fuiEvent* pEvent)
{
	//UIObject*	pSelected			= GetChild(GetCurrentSelection());
	bool		eventConsumed		= false;

	//let children handle the event first
	eventConsumed |= UIList::HandleInputs(pEvent);

	if(!eventConsumed)
	{
		eventConsumed |= m_pPrompts->HandleInputs(pEvent);
	}

	//lets see if we want to handle the event
	if(!eventConsumed)
	{
		if (pEvent == FUIEVENTMGR->GetEvent("action"))
		{
			//an option was chosen so lets close the window
			Exit();
		}
		else if(pEvent == FUIEVENTMGR->GetEvent("cancel"))
		{
			//message boxes should not allow the cancel button
		}

		eventConsumed = true;//if this message box is on screen, then lets not let anybody else handle inputs
	}

	//disable repeating for popups
	m_bProcessRepeatScrolling = false;

	return eventConsumed;
}

void UIMessageBox::PostLoad()
{	
	m_pPrompts->PostLoad();
	UIList::PostLoad();
}

void UIMessageBox::PostStream(char movieID)
{	
	m_pPrompts->PostStream(movieID);
	UIList::PostStream(movieID);
}

void UIMessageBox::SetStyle(UIMessageBox_Styles style)
{
	m_Styles = style;
}

bool UIMessageBox::SetCurrentSelection(rage::s32 currentSelection)
{
	//select
	if (currentSelection != m_CurrentSelection)
	{
		if(currentSelection >= 0 && currentSelection < (rage::s32)GetNumberOfChildren())
		{      
			GetChild(m_CurrentSelection)->Unfocus();
			GetChild(currentSelection)->Focus();
		}

		UIDebugf1("List selection changed from %d to %d in %s",m_CurrentSelection,currentSelection,GetName());

		m_CurrentSelection = currentSelection;

		ProcessScrolling();

		OnSelectionChanged();

		return true;
	}
	return false;
}

/*
void UISpinner::OnActivate()
{
	UIList::OnActivate();//not calling UIMessageBox here on purpose

	UIMANAGER->GetUINavigator()->PushPopup(this,true);
}

void UISpinner::OnDeactivate()
{
	UIMessageBox::OnDeactivate();
}
*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UILabel
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UILabel::UILabel() 
: UIComponent()
, m_Color(-1)
{
	SetText("");
}

void UILabel::Init()
{
#ifdef USEFLASH
	Assertf(!m_pContext,"somethign not right. somebody already instantiated the context");
	m_pContext = rage_new fuiLabelContext(this);
#endif

	m_Scale = 100.0f;
	m_Alpha = 1;
	m_Color = -1;

	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
	UIObject::Init();
}

UILabel::UILabel(const char* pText) : UIComponent(), m_Color(0)
{
	SetText(pText);
}

#if __BANK
void UILabel::AddWidgets( rage::bkBank & bk )
{   
	UIComponent::AddWidgets(bk);

	bk.AddTitle(m_pString); 
}
#endif

void UILabel::PostLoad()
{
	UIComponent::PostLoad();

	//UI_REGISTER(SetText);	
	//m_FunctionMap.Insert("SetText" , datCallback(MFA1(UILabel::SetText), this));
}

bool UILabel::ProcessDataTag( const char* name , const char* value)
{

	if(stricmp(name,"text")==0)
	{
		SetText(value);
	}
	else if(stricmp(name,"color")==0)
	{
		//TMS: Use hex? Comma separation?
		m_Color = atoi(value);		
	}
	else
	{
		UIComponent::ProcessDataTag(name,value);
	}

	return true;
}

void UILabel::SetTextRaw(const char* pText)
{
	Assert(pText);

	m_pString = pText;

	Refresh();
}


void UILabel::SetText(const char* pText)
{
	Assert(pText);
	
	SetTextRaw(GET_STRING(pText));
}

void UILabel::OnPaint()
{
	UIComponent::OnPaint();

	if (GetContext())
	{
		dynamic_cast<UILabelContext*>(m_pContext)->SetText(m_pString);
		dynamic_cast<UILabelContext*>(m_pContext)->SetColor(m_Color);
	}
}

void UILabel::Reset()
{
	UIComponent::Reset();
	SetText("");
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIIcon
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UIIcon::UIIcon() 
: UIComponent()
, m_IconType(NONE)
{
}

void UIIcon::Init()
{
#ifdef USEFLASH
	Assertf(!m_pContext,"somethign not right. somebody already instantiated the context");
	m_pContext = rage_new fuiIconContext(this);
#endif

	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
	UIObject::Init();
}

bool UIIcon::ProcessDataTag(const char* name ,const char* value)
{
	if(stricmp(name,"icon")==0)
	{
		if(stricmp(value,"back")==0 || stricmp(value,"cancel")==0)
		{
			SetIconType(UIPromptStrip::eCANCEL);
		}
	}
	else
	{
		return UIComponent::ProcessDataTag(name,value);
	}

	return true;
}

void UIIcon::SetIconType(IconType type)
{
	m_IconType = type;
}
void UIIcon::SetIconType(UIPromptStrip::PromptIcons type)
{
	m_PromptType = type;
}

#if __BANK
void UIIcon::AddWidgets( rage::bkBank & bk )
{   
	UIComponent::AddWidgets(bk);

	bk.AddSlider( "icon type", (int*)&m_IconType ,0,32,1); 
}
#endif

void UIIcon::OnPaint()
{
	UIComponent::OnPaint();
	UIIconContext* pContext = dynamic_cast<UIIconContext*>(GetContext());

	if (pContext)
	{
		if (m_IconType == NONE)
			pContext->SetIcon(m_PromptType);
		else
			pContext->SetIcon(m_IconType);
	}
}

void UIIcon::Reset()
{
	UIComponent::Reset();
	SetIconType(BLANK);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIButton
//An implementation of a "push" button.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UIButton::UIButton() : UIComponent() , m_bIsBlinking(false)
{
	m_pIcon = verify_cast<UIIcon*>(UIFACTORY->NewUIObject("UIIcon"));
	m_pLabel = verify_cast<UILabel*>(UIFACTORY->NewUIObject("UILabel"));

	m_pIcon->SetName("icon");
	m_pLabel->SetName("label");

	AddChild(m_pIcon);
	AddChild(m_pLabel);
}

void UIButton::Init()
{
#ifdef USEFLASH
	Assertf(!m_pContext,"somethign not right. somebody already instantiated the context");
	m_pContext = rage_new fuiButtonContext(this);
#endif

	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
	UIObject::Init();
}

UIButton::~UIButton()
{
	//no need to delete here. the whole UI tree is deleted
	//delete(m_pIcon);
	//delete(m_pLabel);
}

bool UIButton::ProcessDataTag( const char* name , const char* value)
{
	//allow UILabel and UIIcon data tags to be parsed under this parent button
	m_pLabel->ProcessDataTag(name,value);
	m_pIcon->ProcessDataTag(name,value);
	return UIComponent::ProcessDataTag(name,value);
}

#if __BANK
void UIButton::AddWidgets( rage::bkBank & bk )
{   
	UIComponent::AddWidgets(bk);

	bk.AddToggle("icon type", &m_bIsBlinking); 
}
#endif

//
////simple string events with no data or args. (good for input handling)
//void UIButton::AddEventHandler(const char* pEvent, void (*handler)(void))
//{
//	m_EventHandlers.Insert(pEvent, handler);
//}
//
//bool UIButton::HandleInputs(fuiEvent* pEvent)
//{
//	bool eventConsumed = false;
//
//	//let children handle the event first
//	eventConsumed |= UIObject::HandleInputs(pEvent);
//
//	//lets see if we want to handle the event
//	if(!eventConsumed)
//	{
//		const SimpleEventHandler* handler = m_EventHandlers.Access(pEvent->GetName());
//
//		if (handler)
//		{
//			*handler();
//			eventConsumed = true;
//		}
//	}
//
//	return eventConsumed;
//}

void UIButton::OnPaint()
{
	if (GetContext())
	{		
		UIComponent::OnPaint();

		dynamic_cast<UIButtonContext*>(m_pContext)->SetEnabled(IsEnabled());
		if (IsEnabled()) {dynamic_cast<UIButtonContext*>(m_pContext)->SetBlinking(IsBlinking());}
	}

}

void UIButton::Refresh()
{
	UIComponent::Refresh();

	if (m_pIcon)
	{
		m_pIcon->Refresh();
	}
	if (m_pLabel)
	{
		m_pLabel->Refresh();
	}
}

void UIButton::Enter()
{
	UIComponent::Enter();

	if (m_pIcon)
	{
		m_pIcon->Enter();
	}
	if (m_pLabel)
	{
		m_pLabel->Enter();
	}
}

void UIButton::Exit()
{
	UIComponent::Exit();

	if (m_pIcon)
	{
		m_pIcon->Exit();
	}
	if (m_pLabel)
	{
		m_pLabel->Exit();
	}
}

void UIButton::Update()
{
	UIComponent::Update();

	if (m_pIcon)
	{
		m_pIcon->Update();
	}
	if (m_pLabel)
	{
		m_pLabel->Update();
	}
}

//facade functions
void UIButton::Reset(){UIComponent::Reset();if (m_pIcon){m_pIcon->Reset();}if (m_pLabel){m_pLabel->Reset();}}
void UIButton::SetVisible(bool b){UIComponent::SetVisible(b);if (m_pIcon){m_pIcon->SetVisible(b);}if (m_pLabel){m_pLabel->SetVisible(b);}}
void UIButton::SetFocused(bool b){UIComponent::SetFocused(b);if (m_pIcon){m_pIcon->SetFocused(b);}if (m_pLabel){m_pLabel->SetFocused(b);}}
void UIButton::SetActive(bool b){UIComponent::SetActive(b);if (m_pIcon){m_pIcon->SetActive(b);}if (m_pLabel){m_pLabel->SetActive(b);}}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UITabbedContainer
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

UITabbedContainer::UITabbedContainer() 
: UIComponent()
{
	m_bAllowWrap = false;
	m_CurrentTab = 0;
}

void UITabbedContainer::Init()
{
#ifdef USEFLASH
	Assertf(!m_pContext,"somethign not right. somebody already instantiated the context");
	m_pContext = rage_new fuiTabbedContainerContext(this);
#endif

	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
	UIObject::Init();
}

void UITabbedContainer::OnShow()
{
	UIComponent::OnShow();

	UIObject* pChild;

	//show a tab
	if (GetNumberOfChildren())
	{
		pChild = GetChild(m_CurrentTab);
		pChild->SetVisible(true);
	}
}

void UITabbedContainer::OnHide()
{
	UIComponent::OnHide();
	UIObject* pChild;

	//hide all tabs
	for(rage::u32 i=0;i<GetNumberOfChildren();i++)
	{
		pChild = GetChild(i);
		if (pChild)
		{
			pChild->SetVisible(false);
		}
	}
}

void UITabbedContainer::OnFocused()
{
	UIComponent::OnFocused();	

	UIObject* pChild;

	//show the first tab
	if (GetNumberOfChildren())
	{
		pChild = GetChild(m_CurrentTab);
		pChild->SetFocused(true);
	}

	if (m_pFirstEventHandler)
	{
		UIMANAGER->ComponentFocused();
	}
}

void UITabbedContainer::OnUnfocused()
{
	UIComponent::OnUnfocused();

	UIObject* pChild;

	//hide all tabs
	for(rage::u32 i=0;i<GetNumberOfChildren();i++)
	{
		pChild = GetChild(i);
		if (pChild)
		{
			pChild->SetFocused(false);
		}
	}

	if (m_pFirstEventHandler)
	{
		UIMANAGER->ComponentUnfocused();
	}
}

void UITabbedContainer::OnActivate()
{
	UIComponent::OnActivate();	

	UIObject* pChild;

	//activate the first tab
	if (GetNumberOfChildren())
	{
		pChild = GetChild(m_CurrentTab);
		pChild->Activate();		
	}

	//just in case this is our first time here.
	SetCurrentTab(m_CurrentTab);
}

void UITabbedContainer::OnDeactivate()
{
	UIComponent::OnDeactivate();

	UIObject* pChild;

	//Deactivate all tabs
	for(rage::u32 i=0;i<GetNumberOfChildren();i++)
	{
		pChild = GetChild(i);
		if (pChild)
		{
			pChild->Deactivate();
		}
	}
}

bool UITabbedContainer::ProcessDataTag(const char* name , const char* value)
{
	if (stricmp(name,"AllowWrap")==0)
	{
		m_bAllowWrap = true;
	}
	else if (stricmp(name,"cur_tab")==0)
	{
		m_CurrentTab = atoi(value);
	}
	else
	{
		return UIComponent::ProcessDataTag(name,value);
	}

	return true;
}

void UITabbedContainer::AddTab(const char* tabNameId , UIIcon::IconType iconId)
{
	UITab* pTab = verify_cast<UITab*>(UIFACTORY->NewUIObject("UITab"));
	pTab->Init();
	pTab->SetText(tabNameId);
	pTab->SetIconType(iconId);
	AddChild(pTab);
}

void UITabbedContainer::OnPaint()
{
	UIComponent::OnPaint();

	if (GetContext())dynamic_cast<UITabbedContainerContext*>(m_pContext)->SetCurrentTab(m_CurrentTab);
}

void UITabbedContainer::OnTabChanged()
{
	Refresh();

	UIMANAGER->SendEvent("UI_TabSelectionChanged");

	dynamic_cast<UITabbedContainerContext*>(m_pContext)->OnTabChanged();
}

void UITabbedContainer::SetCurrentTab(rage::u32 currentSelection)
{
	rage::u32 prevSelection = m_CurrentTab;
	m_CurrentTab = currentSelection;

	//select
	if (prevSelection != m_CurrentTab)
	{
		UIMANAGER->RequestTransition(GetChild(m_CurrentTab));
		OnTabChanged();
	}

	Refresh();
}

void UITabbedContainer::SelectPrevious()
{  
	rage::u32 prevSelection = m_CurrentTab;

	if(m_CurrentTab==0 && m_bAllowWrap)
	{
		m_CurrentTab = GetNumberOfChildren()-1;		
	}
	else if (m_CurrentTab==0 && !m_bAllowWrap)
	{
		//do nothing
	}
	else
	{
		m_CurrentTab--;		
	}

	if (prevSelection != m_CurrentTab)
	{
		UIMANAGER->RequestTransition(GetChild(m_CurrentTab));
		//fuiCallbacks::sm_PlayFlashSoundFunc("UI_MENU_NAV_LEFT");
		OnTabChanged();
	}
	else
	{
		//fuiCallbacks::sm_PlayFlashSoundFunc("UI_MENU_ERROR");
	}

	Refresh();
}

void UITabbedContainer::SelectNext()
{
	rage::u32 prevSelection = m_CurrentTab;

	if(m_CurrentTab==GetNumberOfChildren()-1 && m_bAllowWrap)
	{
		m_CurrentTab = 0;
	}
	else if (m_CurrentTab==GetNumberOfChildren()-1 && !m_bAllowWrap)
	{
		//do nothing
	}
	else
	{
		m_CurrentTab++;
	}

	if (prevSelection != m_CurrentTab)
	{
		UIMANAGER->RequestTransition(GetChild(m_CurrentTab));

		//fuiCallbacks::sm_PlayFlashSoundFunc("UI_MENU_NAV_RIGHT");
		OnTabChanged();
	}
	else
	{
		//fuiCallbacks::sm_PlayFlashSoundFunc("UI_MENU_ERROR");
	}

	Refresh();
}

bool UITabbedContainer::HandleInputs( fuiEvent* pEvent)
{
	bool eventConsumed = false;

	//let children handle the event first
	eventConsumed |= UIComponent::HandleInputs(pEvent);

	//lets see if we want to handle the event
	if(!eventConsumed)
	{
		//this must be an input event type
		if (	
				pEvent == FUIEVENTMGR->GetEvent("l1")
			||  pEvent == FUIEVENTMGR->GetEvent("left"))
		{
			SelectPrevious();
			return true;
		}
		else if(pEvent == FUIEVENTMGR->GetEvent("r1")
			||  pEvent == FUIEVENTMGR->GetEvent("right"))
		{	
			SelectNext();
			return true;
		}
	}

	return eventConsumed;
}

void UITabbedContainer::Reset()
{
	UIComponent::Reset();

	SetCurrentTab((u32)0);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UITab
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UITab::UITab() : UILayer()
{
	m_pIcon = verify_cast<UIIcon*>(UIFACTORY->NewUIObject("UIIcon"));
	m_pLabel = verify_cast<UILabel*>(UIFACTORY->NewUIObject("UILabel"));

	AddChild(m_pIcon);
	AddChild(m_pLabel);
}

void UITab::Init()
{
#ifdef USEFLASH
	Assertf(!m_pContext,"somethign not right. somebody already instantiated the context");
	m_pContext = rage_new fuiTabContext(this);
#endif

	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
	UIObject::Init();
}

UITab::~UITab()
{
	//no need to delete here. the whole UI tree is deleted
	//delete(m_pIcon);
	//delete(m_pLabel);
}

bool UITab::ProcessDataTag( const char* name , const char* value)
{
	//allow UILabel and UIIcon data tags to be parsed under this parent button
	m_pLabel->ProcessDataTag(name,value);
	m_pIcon->ProcessDataTag(name,value);
	UILayer::ProcessDataTag(name,value);

	return true;
}

void UITab::Reset()
{
	UIComponent::Reset();

	if (m_pIcon)
	{
		m_pIcon->Reset();
	}
	if (m_pLabel)
	{
		m_pLabel->Reset();
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UITicker
const float		fLifeTime	= 4.0f;
const float		fFadeInTime = 0.25f;
const float		fFadeOutTime= 0.50f;
const float		fScrollTime	= 0.25f;
const unsigned	uTextSpacing= 21;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if __BANK
void AddTestItemToTicker(UITicker* pTicker)
{
	static int time=0; 
	UITicker::TickerItem& rNew = pTicker->AddItem();
	formatf(rNew.m_Text, "TEST STRING <0xffff0000>%d</0x>", time);
	time++;
}
void UITicker::AddWidgets( rage::bkBank & bk )
{
	bk.AddButton("AddItem", datCallback(CFA1(AddTestItemToTicker),this));

	UIComponent::AddWidgets(bk);
}
#endif
UITicker::UITicker() 
: UIComponent()
, m_iNumItems(0)
{
	for (int i=0; i!=MAX_NUM_ITEMS; i++)
		m_Items[i].Reset();

	m_SortedItems.Reset();
}
void UITicker::Update()
{
	if (m_SortedItems.GetCount()>0)
	{
		for(int i=0;i!=m_SortedItems.GetCount(); i++)
		{
			Assert(m_SortedItems[i]);
			m_SortedItems[i]->m_fLife -= TIME.GetUnwarpedRealtimeSeconds();
		}

		//top is old? pop it off
		TickerItem* pTop = m_SortedItems.Top();
		if (pTop->m_fLife<.0f)
			m_SortedItems.Pop()->Reset();

		Refresh();
	}	

	UIComponent::Update();
}

UITicker::TickerItem& UITicker::AddItem()
{
	TickerItem* pNew = NULL;

	if (m_SortedItems.IsFull())
	{
		pNew = m_SortedItems.Pop();
	}
	else
	{
		for (int i=0; i!=MAX_NUM_ITEMS; i++)
		{
			if (!m_Items[i].IsUsed())
			{
				pNew = m_Items+i;
				break;
			}
		}
	}

	AssertMsg(pNew , "something is seriously wrong with the logic in this function if pNew is NULL right now");

	pNew->m_Text[0]=0;
	pNew->m_fLife=fLifeTime;
	m_SortedItems.Append() = pNew;
	Refresh();

	return *pNew;
}

void UITicker::OnPaint()
{
	UITickerContext*	pContext= dynamic_cast<UITickerContext*>(GetContext());
	int					nCount	= m_SortedItems.GetCount();
	const TickerItem*	pNewItem= nCount? m_SortedItems.End() : NULL;

	UIComponent::OnPaint();

	if (!pContext)
		return;	

	int	nOffset	= uTextSpacing;	
	if (nCount && pNewItem && fLifeTime-pNewItem->m_fLife<fScrollTime)
		nOffset = int(nOffset*(fLifeTime-pNewItem->m_fLife)/fScrollTime);

	int i= 0;	
	for (;i<nCount;i++)
	{
		const TickerItem* pItem = m_SortedItems[nCount-1-i];

		int alpha	= 100;
		int pos		= -nOffset-i*uTextSpacing;	

		//close to death? fade out and float away
		if (pItem->m_fLife<fFadeOutTime)
		{
			alpha = int((100/fFadeOutTime)*(pItem->m_fLife));
			pos	-= int(uTextSpacing*(fFadeOutTime-pItem->m_fLife)/fFadeOutTime);
		}

		//young? fade in
		if (pItem->m_fLife>(fLifeTime-fFadeInTime))
		{
			alpha = int((100/fFadeInTime)*(fLifeTime-pItem->m_fLife));
		}

		pContext->SetItemValue(i,pItem->m_Text);
		pContext->SetItemAlpha(i,alpha);
		pContext->SetItemPosition(i,pos);

	}
	for (;i<MAX_NUM_ITEMS;i++)
	{
		pContext->SetItemValue(i, "");
	}
}
void UITicker::Init()
{
#ifdef USEFLASH
	Assertf(!m_pContext,"something not right. somebody already instantiated the context");
	m_pContext = rage_new fuiTickerContext(this);
#endif

	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
	UIObject::Init();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UISpinner
//A single line input field that lets the user select a number or an object value from an ordered 
//sequence. Spinners typically provide a pair of tiny arrow buttons for stepping through the 
//elements of the sequence. Although combo boxes provide similar functionality, spinners are 
//sometimes preferred because they don't require a drop down list that can obscure important data.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UISpinner::UISpinner(int min, int max, int value) 
: UILayer()
, m_iMin(min)
, m_iMax(max)
, m_iValue(value)
, m_iIncrement(1)
, m_bWrap(true)
{
}

void UISpinner::Init()
{
#ifdef USEFLASH
	Assertf(!m_pContext,"somethign not right. somebody already instantiated the context");
	m_pContext = rage_new fuiSpinnerContext(this);
#endif

	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
	UIObject::Init();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIProgressBar
//A component that, by default, displays an integer value within a bounded interval. A progress 
//bar typically communicates the progress of some work by displaying its percentage of completion 
//and possibly a textual display of this percentage.
//
//To indicate that a task of unknown length is executing, you can put a progress bar into 
//indeterminate mode. While the bar is in indeterminate mode, it animates constantly to show that 
//work is occurring. As soon as you can determine the task's length and amount of progress, you 
//should update the progress bar's value and switch it back to determinate mode.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UIProgressBar::UIProgressBar() 
: UIComponent()
,m_Value(0)
,m_Maximum(100)
,m_Minimum(0)
,m_bIsIndeterminate(true)
,m_pProgressString(NULL)
{
}

void UIProgressBar::Init()
{
#ifdef USEFLASH
	Assertf(!m_pContext,"somethign not right. somebody already instantiated the context");
	m_pContext = rage_new fuiProgressBarContext(this);
#endif

	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
	UIObject::Init();
}

bool UIProgressBar::ProcessDataTag( const char* name , const char* value)
{

	if(stricmp(name,"text")==0)
	{
		SetText(value);
	}
	else
	{
		UIComponent::ProcessDataTag(name,value);
	}

	return true;
}

void UIProgressBar::SetText(const char* pText)
{
	Assert(pText);

	SetTextRaw(GET_STRING(pText));
}

void UIProgressBar::SetTextRaw(const char* pText)
{
	m_pProgressString = pText;

	Refresh();
}

void UIProgressBar::OnPaint()
{
	UIComponent::OnPaint();

	if (GetContext())
	{
		UIProgressBarContext* pContext = dynamic_cast<UIProgressBarContext*>(m_pContext);
		pContext->SetValue(m_Value);
		pContext->SetIndeterminate(m_bIsIndeterminate);
		pContext->SetMaximum(m_Maximum);
		pContext->SetMinimum(m_Minimum);
		if (m_pProgressString)
		{
			pContext->SetString(m_pProgressString);
		}
		pContext->SetVisible(IsVisible());
	}
}

void UIProgressBar::SetValue(float value)
{
	if (value != m_Value)
	{
		m_Value= value;
		Refresh(); //TODO: should really be Invalidate of progress control
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIPromptStrip
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UIPromptStrip::UIPromptStrip() 
: UIPanel()
, m_NumPrompts(0)
, m_Style(eBottomScreen)
{
}


void UIPromptStrip::Init()
{
#ifdef USEFLASH
	Assertf(!m_pContext,"something not right. somebody already instantiated the context");
	m_pContext = rage_new fuiPromptStripContext(this);
#endif

	//call uiobject Init() only.  We only want to init this subclass and uiobject.  uiobject propogates the init down to children
	UIObject::Init();
}

void UIPromptStrip::PostLoad()
{
	m_NumPrompts = GetNumberOfChildren();

	if (GetContext())
	{
		//needed for refreshing the contexts and variants in postLoad
		dynamic_cast<UIPromptStripContext*>(m_pContext)->SetStyle(int(m_Style));
	}

	UIPanel::PostLoad();
}

void UIPromptStrip::OnPaint()
{
	if (GetContext())
	{
		UIPromptStripContext* pContext = dynamic_cast<UIPromptStripContext*>(m_pContext);
		pContext->SetStyle(int(m_Style));
		pContext->SetNumPrompts(m_NumPrompts);
		pContext->SetVisible(IsVisible());
	}
	UIPanel::OnPaint();
}

void UIPromptStrip::Update()
{
	//TEMP HACK UNTIL I CAN TALK TO TODD ABOUT THIS
	UIPanel::Update();
	if (m_pContext){m_pContext->Refresh(1);}
}


void UIPromptStrip::AddPrompt(PromptTypes type)
{
	UIButton* b = dynamic_cast<UIButton*>(UIFACTORY->NewUIObject("UIButton"));
	b->Init();
	b->SetText(GetPromptText(type));
	b->SetIconType(GetIconType(type));
	AddChild(b);
	++m_NumPrompts;
}

void UIPromptStrip::AddPrompt(UIButton* b)
{
	AddChild(b);
	++m_NumPrompts;
}

void UIPromptStrip::RemoveChild(UIObject* pChild)
{
	UIComponent::RemoveChild(pChild);
	m_NumPrompts--;
}

void UIPromptStrip::RemoveAllChildren()
{
	UIComponent::RemoveAllChildren();
	m_NumPrompts = 0;

}
const char* UIPromptStrip::GetPromptText(PromptTypes type)
{
	switch(type)
	{
		case OK:
			{
				return "OK";//GET_STRING(value);
			}
		case Cancel:
			{
				return "Cancel";//GET_STRING(value);
			}
		case Continue:
			{
				return "Continue";//GET_STRING(value);
			}
		default:
			return "type not added";
	}
}

UIPromptStrip::PromptIcons UIPromptStrip::GetIconType(PromptTypes type)
{
	switch(type)
	{
	case OK:
		{
			return eACCEPT;
		}
	case Cancel:
		{
			return eCANCEL;//TODO: PS3 we may need to swap with triangle
		}
	case Continue:
		{
			return eACCEPT;
		}
	default:
		return eACCEPT;
	}
}

