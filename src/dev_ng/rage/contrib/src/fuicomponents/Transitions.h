// 
// flashUIComponents/Transitions.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef UITRANSITIONS_H
#define UITRANSITIONS_H

#include "vector/quaternion.h"
#include "curve/curve.h"
#include "curve/curvecatrom.h"
#include "fuiflash/Flash.h"
#include "fuicomponents/Components.h"
#include "paging/streamable.h"

namespace rage 
{

class fuiMovie;

// PURPOSE: Contains logic associated with a transition from one state to another
class UITransition : public UIObject
{
public:
	enum MotionType {PHYSICS,LERP,WAVE,ACCELERATE,DECELERATE,LINEAR,NONE};

	UITransition();
	virtual ~UITransition(){}

	//component funcs
	virtual void OnActivate();
	virtual void OnDeactivate();
	virtual void OnPaint() {}
	virtual void OnShow() {}
	virtual void OnFocused();
	virtual bool ProcessDataTag(const char* name , const char* value);
	virtual void Update(f32 dt);

	//misc funcs
	virtual void SetMotionType(const char* value);
	virtual bool IsComplete() const {return m_bComplete;}
	virtual void SetComplete(bool complete);
	virtual f32 GetPercentComplete(){return m_PercentComplete;}
	virtual void Terminate();
	virtual void Reset();
	virtual void GetUpdatedFinalMatrix(Matrix34* m);

	//time funcs
	virtual void SetTransitionTime(float time);
	virtual void SetDelayTime(f32 f);
	virtual void SetElapsedTime(f32 elapsedTime){m_ElapsedTime = elapsedTime;}
	virtual f32 GetElapsedTime(){return m_ElapsedTime;}
	virtual f32 GetTransitionTime(){return m_TransitionTime;}
	virtual f32 GetTimeTakenThisFrame(){return m_TimeTakenThisFrame;}
	virtual f32 GetRealTotalTime(){return m_RealTotalTime;}
	virtual f32 CalculateRealTotalTime();
	virtual f32 GetRealElapsedTime(){return m_RealElapsedTime;}
	virtual f32 GetDelayTime(){return m_DelayTime;}
	virtual void SetTimestep(f32 f){m_Timestep = f;}
	f32 MakeTimeDevisibleByDT(f32 time);
	virtual void SetSlowIn(){m_bEaseIn = true;}
	virtual void SetSlowOut(){m_bEaseOut = true;}
	virtual void SetSlowInOut(){m_bEaseInOut = true;}

	//matrix funcs
	virtual void SetStartMatrix(const Matrix34* startPos);
	virtual void SetFinalMatrix(const Matrix34* finalPos);
	virtual Matrix34* GetStartMatrix(){return &m_StartPos;}
	virtual Matrix34* GetFinalMatrix(){return &m_FinalPos;}
	virtual void SetTarget(Matrix34* pTarget){m_pTarget = pTarget;}
	virtual void UpdateMatrix(Matrix34* ){}
	virtual Matrix34* GetTargetMatrix(){return GetTarget();}
	virtual Matrix34* GetTarget();
	virtual void UpdateMatrixToFinal(Matrix34*){}
	virtual void SetDistanceOffset(const Vector3* ){}

	//these funcs are only here cause the state machine can't parse matrices in data
	virtual void SetFinal_AX(f32 ax){m_FinalPos.a.x = ax;}
	virtual void SetFinal_AY(f32 ay){m_FinalPos.a.y = ay;}	
	virtual void SetFinal_AZ(f32 az){m_FinalPos.a.z = az;}
	virtual void SetFinal_BX(f32 bx){m_FinalPos.b.x = bx;}
	virtual void SetFinal_BY(f32 by){m_FinalPos.b.y = by;}
	virtual void SetFinal_BZ(f32 bz){m_FinalPos.b.z = bz;}
	virtual void SetFinal_CX(f32 cx){m_FinalPos.c.x = cx;}
	virtual void SetFinal_CY(f32 cy){m_FinalPos.c.y = cy;}	
	virtual void SetFinal_CZ(f32 cz){m_FinalPos.c.z = cz;}
	virtual void SetFinal_DX(f32 dx){m_FinalPos.d.x = dx;}
	virtual void SetFinal_DY(f32 dy){m_FinalPos.d.y = dy;}
	virtual void SetFinal_DZ(f32 dz){m_FinalPos.d.z = dz;}

	//kinematics (empty)
	virtual void SetTargetVelocity(Vector3*){}
	virtual void SetTargetVelocityVector(Vector3*){}
	virtual void GetVelocity(f32*){}
	virtual void SetVelocity(f32){}
	virtual void SetVelocity(Vector3*){}
	virtual void GetVelocity(Vector3*){}
	virtual void SetPreviousVelocity(f32 ){}
	virtual void SetPreviousVelocity(Vector3* ){}
	virtual void GetPreviousVelocity(Vector3*){}
	virtual void SetRotationalVelocity(f32){}
	virtual void GetRotationalVelocity(f32*){}
	virtual void SetPreviousRotationalVelocity(f32 ){}
	virtual void GetPreviousRotationalVelocity(f32*){}
	virtual void SetPreviousAcceleration(f32){}
	virtual void SetPreviousAcceleration(Vector3*){}
	virtual void GetPreviousAcceleration(Vector3*){}
	virtual void SetAcceleration(Vector3*){}
	virtual void SetAcceleration(f32){}
	virtual void GetAcceleration(Vector3*){}

	//temp until we can name are freaking states and access lookat transition directly
	virtual void SetLookAtTargetOffset(f32){}
#if __BANK
   virtual void AddWidgets( bkBank & bk );
   virtual void StartThis();
#endif

protected:
	bool m_bComplete;
	bool m_bIsDisabled;
	bool m_IsSimulating;
	bool m_bEaseIn;
	bool m_bEaseOut;
	bool m_bEaseInOut;
	bool m_bIsRoot;

	f32 m_TransitionTime;
	f32 m_ElapsedTime;   
	f32 m_TimeTakenThisFrame;
	f32 m_DelayTime;
	f32 m_RealTotalTime;
	f32 m_RealElapsedTime;
	f32 m_Timestep;

	f32 m_PercentComplete;
	f32 m_PercentDistance;

	Matrix34 m_StartPos;
	Matrix34 m_FinalPos;
	Matrix34 m_UpdatedFinalPos;

	Matrix34 m_Matrix;//current position
	Matrix34* m_pTarget;//target component that this transition will animate

	MotionType m_MotionType;

#if __DEV
	f32 m_TimeTakenThisFrameSUM;
#endif
};

class UITransition_Parallel : public UITransition
{
public:
	UITransition_Parallel() : UITransition(){}
	virtual ~UITransition_Parallel(){};
	virtual void OnDeactivate();
	virtual void OnActivate();	
	//virtual bool HandleEvents(  fuiEvent* pEvent);
	virtual void OnPaint();
	virtual void OnUnFocused();
	virtual void Update(float dt);
	virtual void PostLoad(); 
	virtual void Activate();
	virtual void Deactivate();
	virtual void SetFocused(bool b);
	virtual void SetVisible(bool b);
	virtual void Reset();
	virtual f32 GetTotalTime();
	virtual f32 GetTransitionTime(){if (m_TransitionTime<0.0f){SetTransitionTime(GetTotalTime());}return UITransition::GetTransitionTime();}
	float SlowOut(float t);
	virtual void SetTimestep(f32 f);

	//matrix funcs
	virtual void SetStartMatrix(const Matrix34* startPos);
	virtual void SetFinalMatrix(const Matrix34* finalPos);
	virtual void SetTarget(Matrix34*);
	virtual void UpdateMatrix(Matrix34* m);
	virtual void UpdateMatrixToFinal(Matrix34*);
	virtual f32 GetTimeTakenThisFrame();
	virtual void SetDistanceOffset(const Vector3* );

	//kinematics
	virtual void SetTargetVelocity(Vector3*);
	virtual void SetVelocity(Vector3*);
	virtual void SetPreviousVelocity(Vector3* pVelocity);
	virtual void SetRotationalVelocity(f32 v);
	virtual void SetPreviousRotationalVelocity(f32 rotVelocity);
	virtual void SetPreviousAcceleration(Vector3* v);
	virtual void SetAcceleration(Vector3*);
	
	//temp until we can name are freaking states and access lookat transition directly
	virtual void SetLookAtTargetOffset(f32);
};

class UITransition_Serial : public UITransition
{
public:
	UITransition_Serial() : UITransition(){m_FocusedChildIndex=0;}
	virtual ~UITransition_Serial(){};
	virtual void OnActivate();
	virtual void OnUnFocused();
	virtual void OnDeactivate();
	virtual void Update(float dt);
	virtual void PostLoad();
	virtual void OnShow();
	virtual void OnFocused();
	virtual void Deactivate();
	virtual void SetVisible(bool b);
	virtual void Reset();
	virtual f32 GetTotalTime();
	virtual f32 GetTransitionTime(){if (m_TransitionTime<0.0f){SetTransitionTime(GetTotalTime());}return UITransition::GetTransitionTime();}
	virtual void SetTimestep(f32 f);

	//matrix funcs
	virtual void SetStartMatrix(const Matrix34* startPos);
	virtual void SetFinalMatrix(const Matrix34* finalPos);
	virtual void SetTarget(Matrix34*);
	virtual void UpdateMatrix(Matrix34* m);
	virtual f32 GetTimeTakenThisFrame();
	virtual void SetDistanceOffset(const Vector3* );

	//kinematics
	virtual void SetTargetVelocity(Vector3*);
	virtual void SetVelocity(Vector3*);
	virtual void GetVelocity(Vector3* pVelocity);
	virtual void SetPreviousVelocity(Vector3* pVelocity);
	virtual void GetPreviousVelocity(Vector3* pVelocity);
	virtual void SetRotationalVelocity(f32 v);
	virtual void GetRotationalVelocity(f32* rotVelocity);
	virtual void SetPreviousRotationalVelocity(f32 rotVelocity);
	virtual void GetPreviousRotationalVelocity(f32* rotVelocity);
	virtual void SetPreviousAcceleration(Vector3* v);
	virtual void GetPreviousAcceleration(Vector3*);
	virtual void SetAcceleration(Vector3*);
	virtual void GetAcceleration(Vector3*);
	virtual void UpdateMatrixToFinal(Matrix34*);

	//temp until we can name are freaking states and access lookat transition directly
	virtual void SetLookAtTargetOffset(f32);

protected:
	u32 m_FocusedChildIndex;
};

// PURPOSE: Contains logic associated with a transition from one state to another
class UITransition_Translate_Axis : public UITransition
{
public:
	enum Axis {X_AXIS,Y_AXIS,Z_AXIS,START_LOOK,PARALLEL_TO_FINAL_LOOK,ALONG_FINAL_LOOK,TARGET_VELOCITY,FINAL_SIDE,CAR_VELOCITY,START_TO_FINAL,NUM_AXIS_TYPES};

	UITransition_Translate_Axis();
	virtual ~UITransition_Translate_Axis(){}
	virtual void Update(float dt);
	virtual void Update3(float dt);
	virtual void Process(float dt);     
	virtual bool Integrate(float dt);     
	virtual bool IntegrateConstantAcceleration(f32 dt);
	virtual void OnActivate();
	virtual void OnDeactivate();
	virtual bool ProcessDataTag(const char* name , const char* value);
	virtual void TraverseAlongVector(Vector3* source, Vector3* dest, f32 distance);
	virtual f32 TimeToTakeThisFrame(f32 dt);
	virtual f32 TimeToTakeThisFrame(f32 dt, f32 totalTime , rage::f32 delayTime , rage::f32 elapsedTime , rage::f32 currentPosition , rage::f32 targetPosition , rage::f32 currentVelocity);
	virtual void UpdateMatrix(rage::Matrix34*);
	virtual void UpdateMatrixToFinal(rage::Matrix34*);
	virtual void Reset();

	//kinematics
	virtual void SetTargetVelocity(rage::Vector3*);
	virtual void SetTargetVelocityVector(rage::Vector3*);
	virtual void SetVelocity(rage::f32 v);
	virtual void SetVelocity(rage::Vector3*);
	virtual void GetVelocity(rage::Vector3* pVelocity);
	virtual void SetPreviousVelocity(rage::f32 pVelocity);
	virtual void SetPreviousVelocity(rage::Vector3* pVelocity);
	virtual void GetPreviousVelocity(rage::Vector3* pVelocity);
	virtual void SetPreviousAcceleration(rage::f32 v);
	virtual void SetPreviousAcceleration(rage::Vector3* v);
	virtual void GetPreviousAcceleration(rage::Vector3*);
	virtual void SetAcceleration(rage::Vector3*);
	virtual void SetAcceleration(rage::f32);
	virtual void GetAcceleration(rage::Vector3*);
	virtual void SetAccelerateOnly(){m_bAccelerate=true;m_bDecelerate=false;}
	virtual void SetDecelerateOnly(){m_bAccelerate=false;m_bDecelerate=true;}
	virtual void SetAccelerateThenDecelerate(){m_bAccelerate=false;m_bDecelerate=false;}
	virtual rage::f32 CalcAcceleration(rage::f32);
	virtual rage::f32 CalcDeceleration(rage::f32);
	virtual rage::f32 CalcVelocity_UsingRungeKutta( rage::f32 dt , rage::f32 time , rage::f32 distance);
	virtual rage::f32 GetRealElapsedTime(){if (m_bIncreaseSteps){return m_ElapsedTime/3;}else{return m_ElapsedTime;}}
	virtual rage::f32 GetTransitionTime(){if (m_bIncreaseSteps){return m_TransitionTime/3;}else{return m_TransitionTime;}}
	//virtual rage::f32 CalcPosition(  rage::f32 dt , rage::f32 time , rage::f32 distance);

#if __BANK
	virtual void AddWidgets( rage::bkBank & bk );
#endif

protected:

	virtual void AddValue(Axis axis , rage::f32 value , rage::Vector3* pVariable);
	virtual void FindValue(Axis axis , rage::f32 value , rage::Vector3* pVariable);//couldn't use GetValue for function name cause base class is using it for something different
	virtual void SetValue(rage::f32* variable , rage::Vector3* pValue);

	rage::f32 m_TargetPosition;
	rage::f32 m_CurrentPosition;
	rage::f32 m_PreviousPosition;
	rage::f32 m_InitialTargetPosition;
	rage::f32 m_DecelerationPoint;

	rage::f32 m_TargetVelocity;
	rage::f32 m_CurrentVelocity;
	rage::f32 m_PreviousVelocity;	
	rage::f32 m_InitialVelocity;

	rage::f32 m_CurrentAcceleration;
	rage::f32 m_PreviousAcceleration;
	rage::f32 m_ConstantAcceleration;
	rage::f32 m_ConstantDeceleration;
	rage::f32 m_MidPointVelocity;
	rage::f32 m_MidPointPosition;


	rage::Vector3 m_TargetVelocityVector;

	Axis m_Axis;
	bool m_bDecelerate;
	bool m_bAccelerate;
	bool m_bIsComplete;
	bool m_bIncreaseSteps;
	bool m_bInvert;//is negative when we are going in the opposite direction.  This allows us to always translate from 0 to n
	bool m_bIsAdding;//true goes from 0 to targetValue - (finalPos - startPos).  false is targetValue + (finalPos - startPos)
	bool m_bIsWorld;//true goes to 0 to targetValue - startPos.
	bool m_bUseConstantAcceleration;
};

//class SplineVertex
//{
//public:
//	enum Position {X_AXIS,Y_AXIS,Z_AXIS,START_LOOK,PARALLEL_TO_FINAL_LOOK,ALONG_FINAL_LOOK,TARGET_VELOCITY,FINAL_SIDE,CAR_VELOCITY,NUM_AXIS_TYPES};
//	virtual void AddValue(Position axis , rage::f32 value , rage::Vector3* pVariable);
//	rage::Vector3 m_Position;
//};

class UITransition_Translate_Spline : public UITransition_Translate_Axis
{
public:
	UITransition_Translate_Spline();
	virtual ~UITransition_Translate_Spline(){}
	enum eMaxVertices {MAX_VERTICES=100};
	virtual void OnActivate();
	virtual void PostLoad();
	virtual void Update3(float dt);     
	virtual void Process(float dt);
	virtual void UpdateMatrix(rage::Matrix34*);
	virtual void UpdateMatrixToFinal(rage::Matrix34*);
	virtual rage::f32 GetStaticDistance();
	virtual rage::f32 CalculateRealTotalTime();
	virtual void Reset();
	//virtual bool ProcessDataTag(char* name , char* value);

protected:
	rage::Vector3 m_CurrentPositionVector;
	rage::Vector3 m_PreviousPositionVector;
	rage::s32 m_CurrentSegment;
	rage::s32 m_PreviousSegment;
	rage::s32 m_NumVertices;
	rage::f32 m_MaxVelocity;
	rage::f32 m_StaticDistance;
	rage::f32 m_CurrentT;
	rage::f32 m_PreviousT;
	rage::f32 m_StartDecelerationVelocity;
	rage::cvCurveCatRom m_CubicCurve;
	rage::f32 m_VertexTValues[MAX_VERTICES];
	//atArray<SplineVertex*> m_Vertices;

	bool m_bXZOnly;
};

// PURPOSE: Contains logic to translate the camera along the cameras look vector
class UITransition_Rotate : public UITransition
{
public:
	enum RotateTo {NORTH,SOUTH,EAST,WEST,TARGET,DOWN,UP,FINAL_LOOK,RADIANS,NUM_ROTATION_TYPES};
	enum Axis {X_AXIS,Y_AXIS,Z_AXIS,NUM_AXIS_TYPES};
	UITransition_Rotate();
	virtual ~UITransition_Rotate(){}
	virtual void Update(rage::f32 dt);
	virtual void OnActivate();   
	virtual bool ProcessDataTag(const char* name , const char* value);
	virtual void UpdateMatrix(rage::Matrix34*);
	virtual void Reset();
	virtual bool Integrate(float dt);
	virtual bool IntegrateConstantAcceleration(rage::f32 dt);
	virtual void OnDeactivate();

	//kinematics
	virtual rage::f32 TimeToTakeThisFrame(rage::f32 dt);
	virtual rage::f32 TimeToTakeThisFrame(rage::f32 dt, rage::f32 totalTime , rage::f32 delayTime , rage::f32 elapsedTime , rage::f32 currentPosition , rage::f32 targetPosition , rage::f32 currentVelocity);
	virtual rage::f32 CalcAcceleration(rage::f32);
	virtual rage::f32 CalcDeceleration(rage::f32);
	virtual void SetVelocity(rage::f32 v){m_CurrentVelocity = v;}
	virtual void GetVelocity(rage::f32* rotVelocity){*rotVelocity = m_CurrentVelocity;}
	virtual void GetPreviouslVelocity(rage::f32* rotVelocity){*rotVelocity = m_CurrentVelocity;}
	virtual void UpdateMatrixToFinal(rage::Matrix34*);
	virtual	void RotateAroundLocalXAxis();
	virtual	void RotateAroundLocalYAxis();
	virtual	void RotateAroundLocalZAxis();


#if __BANK
   static const char *sm_ppRotationNames[NUM_ROTATION_TYPES];
   virtual void AddWidgets( rage::bkBank & bk );
#endif

protected:
	bool m_bAccelerate;
	bool m_bDecelerate;
	bool m_bUseConstantAcceleration;

	rage::f32 m_CurrentWorldYRotationAngle;
	rage::f32 m_CurrentLocalXRotationAngle;

	rage::f32 m_PreviousAcceleration;
	rage::f32 m_CurrentAcceleration;
	rage::f32 m_ConstantAcceleration;
	rage::f32 m_ConstantDeceleration;
	rage::f32 m_MidPointVelocity;
	rage::f32 m_MidPointPosition;

	rage::f32 m_InitialTargetPosition;
	rage::f32 m_TargetOffset;
	rage::f32 m_PreviousPosition;
	rage::f32 m_CurrentPosition;//current rotation angle
	rage::f32 m_TargetPosition;//target rotation angle

	rage::f32 m_PreviousVelocity;
	rage::f32 m_CurrentVelocity;
	rage::f32 m_TargetVelocity;
	rage::f32 m_InitialVelocity;

	rage::f32 m_UpVectorOffset; //used when rotating around the Y axis.  we can lean into the rotation
	rage::f32 m_PreviousAngle;
	rage::Matrix34 m_PreviousMatrix;

	Axis m_Axis;
	RotateTo m_RotateType;
};

class UITransition_Lerp : public UITransition
{
public:
	UITransition_Lerp();
	virtual ~UITransition_Lerp(){}
	virtual void Update(float dt);
	void SlowInOut(float fBlendWeight);
	void SlowOut(float fBlendWeight);
	void SlowIn(float fBlendWeight);
	void OnActivate();
	void UpdateUsingTrig();
	void UpdateUsingPhysics(rage::f32 dt);
	bool ProcessDataTag(const char* name , const char* value);
	virtual void UpdateMatrix(rage::Matrix34*);
	virtual void UpdateMatrixToFinal(rage::Matrix34*);
	virtual void Reset();

#if __BANK
   virtual void AddWidgets( rage::bkBank & bk );
#endif

   rage::Quaternion qStart,qFinal; 

protected:
	rage::f32 m_AddX;
	rage::f32 m_AddY;
	rage::f32 m_AddZ;	
	bool m_bSetX;
	bool m_bSetY;
	bool m_bSetZ;
	bool m_bLookAtStart;
	bool m_bOrientationOnly;
};

//transitions a float from start to finish
class UITransition_Value : public UITransition
{
public:
	UITransition_Value();
	virtual ~UITransition_Value(){}
	virtual void Update(float dt);
	virtual void SetCurrentValue(rage::f32* currentValue){m_pCurrentValue = currentValue;}
	virtual void SetStartValue(rage::f32 startValue){m_StartValue = startValue;}
	virtual rage::f32  GetStartValue(){return m_StartValue;}
	virtual void SetFinalValue(rage::f32 finalValue){m_FinalValue = finalValue;}
	virtual rage::f32  GetFinalValue(){return m_FinalValue;}
	virtual bool ProcessDataTag(const char* name , const char* value);
	virtual void OnActivate();
	virtual void OnDeactivate();
	virtual void Reset();

protected:
	rage::f32 m_StartValue;
	rage::f32 m_FinalValue;
	rage::f32* m_pCurrentValue;//could be some other objects pointer
	rage::f32 m_CurrentValue;
	bool m_TargetVisibility;
};

class UITransition_LookAtTarget : public UITransition
{
public:
	enum Axis {X_AXIS,Y_AXIS,Z_AXIS,FINAL_LOOK,FINAL_UP,CITY_CENTER_Y,CITY_CENTER_Z,NUM_AXIS_TYPES};

	UITransition_LookAtTarget();
	virtual void Update(float dt); 
	virtual bool ProcessDataTag(const char* name , const char* value);
	virtual void UpdateMatrix(rage::Matrix34*);
	virtual void OnActivate();
	virtual void TraverseAlongVector(rage::Vector3* source, rage::Vector3* dest, rage::f32 distance);
	virtual void UpdateMatrixToFinal(rage::Matrix34*);
	virtual void Reset();
	virtual bool IsOppositeDirection(rage::f32 a , rage::f32 b);
	virtual void RotateTo(rage::Matrix34& matrixFrom , rage::Matrix34& matrixTo , float t);
	virtual void ComputeRotation(const rage::Vector3& unitFrom, const rage::Vector3& unitTo, rage::Vector3& unitAxis, float& angle);
	void PrepareToStart();
	virtual void SetLookAtTargetOffset(rage::f32 f){m_LookAtTargetOffset = f;}
	virtual void SetDistanceOffset(const rage::Vector3* v ){m_DynamicTargetOffset.Set(*v);}

#if __BANK
	virtual void AddWidgets( rage::bkBank & bk );
#endif

protected:
	rage::f32 m_TimeToLook;//the time it should take to point camera at the target (different than total time)
	rage::Vector3 m_TargetPosition;
	rage::Vector3 m_UpVector;
	rage::Vector3 m_FinalUpVector;	
	rage::f32 m_InitialTargetPosition;
	rage::f32 m_LookAtTargetOffset;
	rage::Matrix34 m_PreviousMatrix;
	rage::Vector3 m_DynamicTargetOffset;//represents the distance that the target has moved since this transition started
	
	Axis m_Axis;
	Axis m_Up;
	bool m_bClockwiseX;
	bool m_bClockwiseY;
	bool m_bClockwiseZ;
	bool m_bCompleteX;
	bool m_bCompleteY;
	bool m_bCompleteZ;
	bool m_bDelay_up_rotate;
	bool m_bPreviousDirectionY;
	bool m_bCurrentDirectionY;
};

class UITransition_Lean : public UITransition_Parallel
{
public:
	enum eNumPreviousAngles {NUM_PREVIOUS_ANGLES=4};

	UITransition_Lean();
	bool ProcessDataTag(const char* name , const char* value);
	virtual void UpdateMatrix(rage::Matrix34*);
	void OnActivate();

#if __BANK
	virtual void AddWidgets( rage::bkBank & bk );
#endif

protected:
	rage::f32 m_UpVectorOffset; //used when rotating around the Y axis.  we can lean into the rotation
	rage::f32 m_PreviousAngles[NUM_PREVIOUS_ANGLES];
	rage::f32 m_MaxLeanAngle;
	rage::f32 m_AngleToUseForLeaning;
	rage::f32 m_DistanceLeanOut;
	rage::s32 m_NumPreviousAngles;

	rage::Matrix34 m_PreviousMatrix;
	bool m_bDelay_up_rotate;
};

//ensures that we never look outside of the city bounds.  (CAMERA MATRIX MUST BE ORIENTED NORTH)
class UITransition_Bounded : public UITransition_Parallel
{
	virtual void UpdateMatrix(rage::Matrix34*);
};

//decreases y value
//translates x,z in relation to y in such a way that that the target always remains at the same spot on the screen
class UITransition_Zoom : public UITransition_Parallel
{
	virtual void UpdateMatrix(rage::Matrix34*);
	virtual void OnActivate();
};

class UITransition_FarAwayTarget : public UITransition_Parallel
{
	virtual void UpdateMatrix(rage::Matrix34*);
	bool IsOnScreen(rage::Matrix34* pMatrix);
};

class UITransition_Translate_Spline_ExitSharpAngle : public UITransition_Translate_Spline
{
	virtual void PostLoad();
	virtual void OnActivate();

	rage::f32 distanceSide;
	rage::f32 distanceLook;
};

//camera goes from some ground level height to a high level height
//weather,shadows,ect are managed during the up->wait->down transition

//this class is used for rotations that depend on other child rotations
//class UITransition_Parallel_Rotations : public UITransition_Parallel
//{
//public:
//	virtual void OnActivate();
//};

//Streams a flash movie swfFILE
class UITransition_StreamFlashMovie : public UITransition
{
public:
	UITransition_StreamFlashMovie();
	virtual ~UITransition_StreamFlashMovie(){}
	virtual void OnFocused();
	virtual void Stream();
	static void PlaceMovie(void *pStreamable, rage::datResourceMap &Map, void *idx);
	void SetMovie(fuiMovie* pMovie);
	virtual bool ProcessDataTag(const char* name , const char* value);
	virtual void PostLoad();

protected:
	fuiMovie* m_pMovie;
};

}//namespace rage

#endif

