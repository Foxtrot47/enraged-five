// 
// flashUIObjects/UIObject.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "fuicomponents/UIObject.h"
#include "fuicomponents/UIContexts.h"
#include "fuicore/Common.h"
#include "fuicore/UIManager.h"
#include "fuicore/UIStringtable.h"
#include "fuicore/EventManager.h"
#include "fuicore/Event.h"
#include "system/timemgr.h"

#include "fuicore/UIManager.h"
#include "atl/singleton.h"
#include "data/marker.h"
#include "file/asset.h"
#include "bank/bkmgr.h"
#include "diag/tracker.h"
#include "fuicomponents/UIObject.h"
#include "fuicomponents/Components.h"
#include "fuicore\Common.h"
#include "profile/profiler.h"
#include "system/param.h"
#include "grprofile/pix.h"
#include "system/timemgr.h"
#include "fuicore/UIStringTable.h"
#include "snu/snusocket.h"
#include "xmldata/xmltoken.h"
#include "fuicomponents/Transitions.h"
#include "fuicore/TextureStream.h"
#include "script/wrapper.h"
#include "script/hash.h"
#include "fuicore/UICallback.h"

namespace rage{

const char * k_szMenuSelectSound = "UI_MENU_SELECT";
const char * k_szMenuBackSound = "UI_MENU_BACK";
const char * k_szMenuUpSound = "UI_MENU_NAV_UP";
const char * k_szMenuDownSound = "UI_MENU_NAV_DOWN";
const char * k_szMenuLeftSound = "UI_MENU_NAV_LEFT";
const char * k_szMenuRightSound = "UI_MENU_NAV_RIGHT";
const char * k_szMenuErrorSound = "UI_MENU_ERROR";

//used for parsing xml
char a_szComponentAttrName_[MAX_XML_ATTRIBUTES][MAX_XML_ATTR_LEN];
char a_szComponentAttrValue_[MAX_XML_ATTRIBUTES][MAX_XML_ATTR_LEN];
char* a_szComponentAttrValue[MAX_XML_ATTRIBUTES];
char* a_szComponentAttrName[MAX_XML_ATTRIBUTES];
void ClearComponentAttrValues()
{
	for (int i=0;i<MAX_XML_ATTRIBUTES;i++)
	{
		a_szComponentAttrName[i][0] = '\0';
		a_szComponentAttrValue[i][0] = '\0';
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

UIObject::UIObject()
: m_pParent				(NULL)
, m_pFirstChild			(NULL)
, m_pNextSibling		(NULL)
, m_pFirstEventHandler	(NULL)
, m_Properties			(UI_ENABLED | UI_INTERRUPTIBLE)
, m_szName				(NULL)
, m_bRefresh			(false)
#if __BANK
, m_bIsActive (false)
, m_bIsFocused (false)
, m_bIsVisible (false)
, m_bIsInterruptible (false)
, m_bIsEnabled (false)
#endif

{
	SetInterruptible(true);
	SetEnabled(true);
}

UIObject::~UIObject()
{		
	m_FunctionMap.Kill();
}

void UIObject::SetupXmlArrays()
{
	//to satisfy rage level functions
	for(int i=0;i<MAX_XML_ATTRIBUTES;++i)
	{
		a_szComponentAttrValue[i] = a_szComponentAttrValue_[i];
		a_szComponentAttrName[i] = a_szComponentAttrName_[i];
	}
}

void UIObject::PostStream(char movieID)
{
	//call postLoad on all children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->m_pNextSibling)
	{
		pChild->PostStream(movieID);
	}
	
	Refresh();
}

void UIObject::PostLoad()
{
	//UI_REGISTER(UIObject, Activate);
	//UI_REGISTER(UIObject, Focus);
	//UI_REGISTER(UIObject, Show);
	//UI_REGISTER(UIObject, Deactivate);
	//UI_REGISTER(UIObject, Unfocus);
	//UI_REGISTER(UIObject, Hide);

	//call postLoad on all children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->m_pNextSibling)
	{
		pChild->PostLoad();
	}
}

//component style "turning on" of a component
void UIObject::TransitionTo()
{
	Enter();
}

//component style "turning off" of a component
void UIObject::TransitionFrom()
{
	Exit();
}

//component style "turning on" of a component
void UIObject::Enter()
{
	SetActive(true);
	SetFocused(true);
	SetVisible(true);
}

//component style "turning off" of a component
void UIObject::Exit()
{
	SetActive(false);
	SetFocused(false);
	SetVisible(false);
}

void UIObject::Goto(const char* id)
{
	UIFACTORY->GetUIObject(id)->Goto();
}

void UIObject::Goto()
{
	UIMANAGER->RequestTransition(this);
}

u32 UIObject::GetNumberOfChildren() const
{
	int i=0;
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->m_pNextSibling)
	{
		i++;
	}
	return i;
}

u32 UIObject::GetNumberOfVisibleChildren() const
{
	int i=0;
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->m_pNextSibling)
	{
		if (pChild->IsVisible())
		{
			i++;
		}
	}
	return i;
}

void UIObject::AddChild(UIObject* pNewChild)
{
	Assert(pNewChild != NULL);

	SetupChildRelationships(pNewChild);
}

UIObject* UIObject::GetChild(int index) const
{
	if ((u32)index < GetNumberOfChildren())	
	{
		UIObject* pNext = m_pFirstChild;

		for (int i=0; i!=index && pNext; i++)
		{
			pNext = pNext->m_pNextSibling;
		}

		return pNext;
	}

	return NULL;
}

UIObject* UIObject::GetChild(const char* pName) const
{
	UIObject* pNext = m_pFirstChild;

	for (u32 i=0; i<GetNumberOfChildren();i++)
	{
		if(stricmp(pName,pNext->GetName())==0)
		{
			return pNext;
		}
		pNext = pNext->m_pNextSibling;
	}

	Assertf(0,"Could not find %s",pName);
	return NULL;
}

UIObject* UIObject::GetChildWithText(const char* pText)
{
	UIObject* pNext = m_pFirstChild;

	for (u32 i=0; i<GetNumberOfChildren();i++)
	{
		if(strstr(pText,pNext->GetText())==0)
		{
			return pNext;
		}
		pNext = pNext->m_pNextSibling;
	}

	Assertf(0,"Could not find child with text %s",pText);
	return NULL;
}

u32 UIObject::GetChildIndex(const char* pName) const
{
	UIObject* pNext = m_pFirstChild;

	for (u32 i=0; i<GetNumberOfChildren();i++)
	{
		if(stricmp(pName,pNext->GetName())==0)
		{
			return i;
		}
		pNext = pNext->m_pNextSibling;
	}

	Assertf(0,"Could not find %s",pName);
	return 0;
}

UIObject* UIObject::GetFocusedChild()
{
	for (UIObject* pChild=GetChild(0); pChild; pChild=pChild->GetNextSibling() )
	{
		if (pChild->IsFocused())
		{
			return pChild;
		}
	}

	return NULL;
}

UIObject* UIObject::GetNextSibling()
{
	if (m_pNextSibling)	
		return smart_cast<UIObject*>(m_pNextSibling);

	return NULL;
}

void UIObject::SetupChildRelationships(UIObject* pNewChild)
{
	UIObject* pLastChild	= m_pFirstChild;

	//we can allow resetting of parents now...
	//if (pNewChild->m_pParent)
	//{
	//	Assertf(false,"Duplicate State ID");
	//	return;
	//}

	if(pNewChild)
	{
		if (pLastChild)
		{
			while (pLastChild->m_pNextSibling)
			{
				pLastChild = pLastChild->m_pNextSibling;
			}

			pLastChild->m_pNextSibling = pNewChild;			
		} 
		else
		{
			m_pFirstChild = pNewChild;
		}

		pNewChild->m_pNextSibling	= NULL;
		pNewChild->m_pParent		= this;
	}
}

#if __DEV
void UIObject::DrawDebug()
{
	for (UIObject* pChild=GetChild(0); pChild; pChild=pChild->GetNextSibling() )
	{
		//if (pChild->IsVisible())
		{
			pChild->DrawDebug();
		}
	}
}
#endif

bool UIObject::OnSetCamera(rage::Matrix34& camera, float& fovy, float& near, float& far)
{
	SetMatrix(&camera);

	for (UIObject* pChild=GetChild(0); pChild; pChild=pChild->GetNextSibling() )
	{
		if (pChild->IsVisible())
		{
			pChild->OnSetCamera(camera,fovy,near,far);
		}
	}

	return true;
}

void UIObject::Update()
{
	Update(TIME.GetUnwarpedSeconds());
}

void UIObject::Update(rage::f32)
{
	// UIDebugf2("Updating %s",GetName());

	//paint this object before its children
	if (m_bRefresh && IsVisible())
	{
		m_bRefresh = 0;
		OnPaint();
	}

	UIObject* pChild;

	//TODO: make sure we set up the visible system correctly.  (We don't want to update everything)

	//update all visible children
	for (pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->m_pNextSibling)
	{
		if(pChild->IsActive())
			pChild->Update();
	}
}

void UIObject::Draw(float dt, u32 pass)
{
	UIObject* pChild = m_pFirstChild;

	//all child states should have the same context
	while(pChild)
	{
		if(pChild->IsVisible())
			pChild->Draw(dt,pass);

		pChild = pChild->GetNextSibling();
	}
}

#if __BANK
void UIObject::AddMatrixWidgets( rage::bkBank & bk , rage::Matrix34* pMatrix) 
{
	bk.AddSlider( "a", &pMatrix->a, -1.0f, 1.0f, 0.01f );
	bk.AddSlider( "b", &pMatrix->b, -1.0f, 1.0f, 0.01f );
	bk.AddSlider( "c", &pMatrix->c, -1.0f, 1.0f, 0.01f );
	bk.AddSlider( "d", &pMatrix->d, -10000.0f, 10000.0f, 10.0f );
}

void UIObject::AddWidgets( rage::bkBank & bk )
{   
	UIObject* pChild = m_pFirstChild;

	bk.AddButton("TransitionTo",datCallback(MFA(UIObject::GotoCB), this));
	bk.AddButton("Enter",datCallback(MFA(UIObject::Enter), this));
	bk.AddButton("Exit",datCallback(MFA(UIObject::Exit), this));
	bk.AddButton("Refresh",datCallback(MFA(UIObject::Refresh), this));

	bk.PushGroup( "Advanced", false );
	{
		bk.AddButton("Activate",datCallback(MFA(UIObject::Activate), this));
		bk.AddButton("Focus",datCallback(MFA(UIObject::Focus), this));
		bk.AddButton("Show",datCallback(MFA(UIObject::Show), this));
		bk.AddButton("De-Activate",datCallback(MFA(UIObject::Deactivate), this));
		bk.AddButton("Un-Focus",datCallback(MFA(UIObject::Unfocus), this));
		bk.AddButton("Hide",datCallback(MFA(UIObject::Hide), this));
	}
	bk.PopGroup();

	while(pChild)
	{
		bk.PushGroup( pChild->GetName(), false );
		{
			pChild->AddWidgets(bk);
		}
		bk.PopGroup();
		pChild = pChild->GetNextSibling();
	}
 	bk.AddText( "Visible", &m_bIsVisible,true );
 	bk.AddText( "Active", &m_bIsActive,true  );
 	bk.AddText( "Enabled", &m_bIsEnabled,true  );
 	bk.AddText( "Focused", &m_bIsFocused,true  );
	bk.AddText( "Interruptible", &m_bIsInterruptible,true  );
}
#endif

void UIObject::ProcessEventCommands(const char* szEvent)
{
	for
	(
		UIEventHandler* pHandler	=	m_pFirstEventHandler; 
		pHandler					!=	NULL; 
		pHandler					=	pHandler->m_pNext
	)
	{
		if(pHandler->m_Trigger == FUIEVENTMGR->GetEvent(szEvent) || pHandler->m_Trigger == FUIEVENTMGR->GetEvent("ALL"))		
			pHandler->m_pTargetCommand->ProcessCommand(this);
	}
}

void UIObject::OnShow()
{
	ProcessEventCommands("OnShow");
	Refresh();
}

void UIObject::OnHide()
{
	ProcessEventCommands("OnHide");

	//hide all children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->m_pNextSibling)
	{
		pChild->SetVisible(false);
	}
}

//Visibility dictates if a state is shown or hidden.  
//Ideally the draw/render/ect funcs will not be called if not visible. Nothing should be rendered if not visible   
bool UIObject::IsVisible() const
{
	return (m_Properties&UI_VISIBLE) > 0;
}

void UIObject::SetVisible(bool b)
{	
	bool prevVisiblity = IsVisible();

	if (b && b != prevVisiblity)
	{
#if __BANK
		m_bIsVisible = true;
#endif

		AddProperties(UI_VISIBLE);
		OnShow();
		Refresh();
	}
	else if (!b && b != prevVisiblity)
	{
#if __BANK
		m_bIsVisible = false;
#endif

		RemoveProperties(UI_VISIBLE);
		OnHide();
		Refresh();
	}
}

void UIObject::Hide()
{
	SetVisible(false);
}

void UIObject::Show()
{
	SetVisible(true);
}

void UIObject::ToggleVisibility()
{
	if (IsVisible())
	{
		SetVisible(false);
	}
	else
	{
		SetVisible(true);
	}
}

//Enabled / Disabled describes how input, events, and transitions are processed on a state.  
//Input and events are processed if this flag is true, otherwise input and events are not processed.  
//A disabled state should never be transitioned to. 
//Visual representation of a state may change based off this state, but is not required.
//in our UI/state terms, a state should be entered on enable and exited on disable
bool UIObject::IsEnabled() const
{
	return (m_Properties&UI_ENABLED) > 0;
}

void UIObject::SetEnabled(bool b)
{
	if (GetContext()){GetContext()->SetEnabled(b);}

	if (b)
	{
#if __BANK
		m_bIsEnabled = true;
#endif

		AddProperties(UI_ENABLED);
		OnEnabled();
		Refresh();
	}
	else 
	{
#if __BANK
		m_bIsEnabled = false;
#endif
		RemoveProperties(UI_ENABLED);
		OnDisabled();
		Refresh();
	}
}

void UIObject::Disable()
{
	SetEnabled(false);
}

void UIObject::Enable()
{
	SetEnabled(true);
}

void UIObject::ToggleEnabled()
{
	if (IsEnabled())
	{
		Disable();
	}
	else
	{
		Enable();
	}
}

//Focused describes if a component is selected and can receive input.  
//All states for a focused state that are on a path from that focused state to its root state should also be focused.  
//Although all states for a non-focused state that are on a path from that non-focused state to its root state may 
//or may not be non-focused.  The focused state should be the first state to receive and decide to consume input.  
//If it does not consume input, then it should pass that responsibility to its parent and on up to its root state.  
//If nobody consumes the input event, then non-focused states may then choose to process and consume input.
bool UIObject::IsFocused() const
{
	return (m_Properties&UI_FOCUSED) > 0;
}

void UIObject::SetFocused(bool b)
{		
	if(b && !IsFocused())
	{
#if __BANK
		m_bIsFocused = true;
#endif
		AddProperties(UI_FOCUSED);
		OnFocused();
		Refresh();
		UIDebugf1("FOCUSED %s",m_szName);
	}	
	else if(!b && IsFocused())
	{
#if __BANK
		m_bIsFocused = false;
#endif
		RemoveProperties(UI_FOCUSED);
		OnUnfocused();
		Refresh();
		UIDebugf1("UNFOCUSED %s",m_szName);
	}
}

void UIObject::Focus()
{
	SetFocused(true);
}

void UIObject::Unfocus()
{
	SetFocused(false);
}

//Active describes if a component is running.  
//All component for an active component that are on a path from that active component to its root parent component should also be active.  
//Siblings and children of this active component may or may not be active.  The active component should be the first component to receive and decide to consume events.  
//If it does not consume events, then it should pass that responsibility to its parent and on up to its root component.  
//If nobody consumes the event, then non-focused components may then choose to process and consume events.
bool UIObject::IsActive() const
{
	return (m_Properties&UI_ACTIVE) > 0;
}

void UIObject::Activate()
{
	SetActive(true);
}

void UIObject::Deactivate()
{
	SetActive(false);
}

void UIObject::SetActive(bool b)
{	
	if (GetContext()){GetContext()->SetActive(b);}

	if(b && !IsActive())
	{
#if __BANK
		m_bIsActive = true;
#endif
		AddProperties(UI_ACTIVE);
		OnActivate();
		Refresh();
		UIDebugf1("ACTIVATED %s",m_szName);

		if(m_pParent)
			m_pParent->Activate();
	}	
	else if(!b && IsActive())
	{
#if __BANK
		m_bIsActive = false;
#endif
		RemoveProperties(UI_ACTIVE);
		OnDeactivate();
		Refresh();
		UIDebugf1("DEACTIVATED %s",m_szName);
	}
}

void UIObject::ToggleActivate()
{
	if (IsActive())
	{
		Deactivate();
	}
	else
	{
		Activate();
	}
}

//Dictates if a component can be transitioned via external and independent events.  
//If a state is not interruptible, then all events that it receives (that are not specified in it�s local event list) 
//will be queued and processed after this state is transitioned out of.  
//For example, we may not want a tutorial movie to be interrupted by a popup that appears because a controller is unplugged.  
//If the tutorial states set their interruptible flag to false, then the popup will display after the movie ends.  
bool UIObject::IsInterruptible() const
{
	 return (m_Properties&UI_INTERRUPTIBLE) > 0;
}

void UIObject::SetInterruptible(bool b)
{
	 if (b) 
	 {
#if __BANK
		 m_bIsInterruptible = true;
#endif

		 AddProperties(UI_INTERRUPTIBLE);
	 }  
	 else 
	 {
#if __BANK
		 m_bIsInterruptible = false;
#endif

		 RemoveProperties(UI_INTERRUPTIBLE);
	 }
}

void UIObject::OnFocused()
{
	for
	(
		UIEventHandler* pHandler	=	m_pFirstEventHandler; 
		pHandler					!=	NULL; 
		pHandler					=	pHandler->m_pNext
	)
	{
		if(pHandler->m_Trigger == FUIEVENTMGR->GetEvent("OnFocused"))
			pHandler->m_pTargetCommand->ProcessCommand(this);
	}

	if (m_pFirstEventHandler)
	{
		UIMANAGER->ComponentFocused();
	}
}

void UIObject::OnUnfocused()
{
	for
	(
		UIEventHandler* pHandler	=	m_pFirstEventHandler; 
		pHandler					!=	NULL; 
		pHandler					=	pHandler->m_pNext
	)
	{
		if(pHandler->m_Trigger == FUIEVENTMGR->GetEvent("OnUnfocused"))
			pHandler->m_pTargetCommand->ProcessCommand(this);
	}

	//Components do not "goto" so we need to process this queue when we lose focus
	UIMANAGER->ProcessInterruptingQueue();
	UIMANAGER->m_ProcessQueuedEvents = true;

	//unfocused all children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->m_pNextSibling)
	{
		pChild->SetFocused(false);
	}

	if (m_pFirstEventHandler)
	{
		UIMANAGER->ComponentUnfocused();
	}
}

void UIObject::OnDeactivate()
{
	//UIMANAGER->SendEvent("UI_TransitionOut",this);

	ProcessEventCommands("OnDeactivate");

	//deactivate all children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->m_pNextSibling)
	{
		pChild->SetActive(false);
	}
}

void UIObject::OnActivate()
{ 
	//UIMANAGER->SendEvent("UI_TransitionIn",this); 
	
	ProcessEventCommands("OnActivate"); 
}

void UIObject::OnExit()
{
	ProcessEventCommands("onExit");

	//exit all children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->m_pNextSibling)
	{
		pChild->Exit();
	}
}

void UIObject::OnTransitionComplete()
{
	ProcessEventCommands("onTransitionComplete");

	//only need to be called for focused children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->m_pNextSibling)
	{
		if (pChild->IsFocused())
			pChild->OnTransitionComplete();
	}
}

bool UIObject::IsChildTransitioningIn()
{
	for (UIObject* pChild=GetChild(0); pChild; pChild=pChild->GetNextSibling() )
	{
		if (pChild->IsTransitioningIn() || pChild->IsChildTransitioningIn())
			return true;
	}

	return false;
}

bool UIObject::IsChildTransitioningOut()
{
	for (UIObject* pChild=GetChild(0); pChild; pChild=pChild->GetNextSibling() )
	{
		if (pChild->IsTransitioningOut() || pChild->IsChildTransitioningOut())
			return true;
	}

	return false;
}

bool UIObject::IsChildTransitioning()
{
	for (UIObject* pChild=GetChild(0); pChild; pChild=pChild->GetNextSibling() )
	{
		if (pChild->IsTransitioning())
			return true;
	}

	return false;
}

void UIObject::Init()
{
	//hide all children
	for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->m_pNextSibling)
	{
		pChild->Init();
	}
}

bool UIObject::SendEvent(fuiEvent* pEvent , bool)
{
	return HandleEvents(pEvent);
}

bool UIObject::SendEvent(const char* pEventName , void* pData, rage::u8 priority , bool sendToCode,  bool sendToScripts, bool)
{
	fuiEvent* pEvent = FUIEVENTMGR->GetEvent(pEventName);
	pEvent->SetData(pData);
	pEvent->SetPriority(priority);
	pEvent->SendToCode(sendToCode);
	pEvent->SendToScript(sendToScripts);
	return HandleEvents(pEvent);
}

void UIObject::RegisterCommand(UIObject* pObject, const char *cmdName,uiCmd handler, UIObject* thisPtr) 
{
	//u32 hashCode = scrComputeHash(cmdName);
	UICallback* c = rage_new UICallback(handler,thisPtr);
	pObject->GetFunctionMap()->Insert(cmdName,c);
}

void UIObject::ExecuteCommand(const char* commandName, int numParameters, UIVariant *pParameters) 
{
	UIVariant unusedResult;
	UICallback** cb = m_FunctionMap.Access( commandName );
	if (cb) {
		UIInfo curInfo((*cb)->thisPtr,&unusedResult, numParameters, pParameters);
		(*((*cb)->fn))(curInfo);		
	}
}

void UIObject::HandleCommand(const char* commandName, const UICommand *pCmd, UIObject* /*pState*/)
{
	//convert from linked list to array
	UIVariant pArgs[12];
	Assertf(pCmd->m_NumArgs < 12,"wow a func with 12+ params?!  is that really necessary?");
	for(int i=0;i<pCmd->m_NumArgs;++i)
	{
		pArgs[i].Set(*pCmd->GetArg(i));
	}

	ExecuteCommand(commandName,pCmd->m_NumArgs,pArgs);
}

//returns an event handler if one exists. otherwise null
UIEventHandler* UIObject::GetEventHandler(fuiEvent* pEvent)
{
	UIEventHandler*	pHandler	= NULL;

	pHandler = m_pFirstEventHandler;
	while (pHandler)
	{
		if(pHandler->m_Trigger == pEvent)
			return pHandler;

		pHandler = pHandler->m_pNext;
	}

	return NULL;
}

bool UIObject::HandleEvents(fuiEvent* pEvent)
{
	bool eventConsumed = false;

	UIObject* pChild = m_pFirstChild;

	while(pChild && !eventConsumed)
	{
		if(pChild->IsActive())
			eventConsumed |= pChild->HandleEvents(pEvent);			

		pChild = pChild->GetNextSibling();
	}

	if (!eventConsumed)
	{
		//find a handler defined in xml
		UIEventHandler*	pHandler= GetEventHandler(pEvent);
		UICommand*		pCmd	= NULL;

		if (!pHandler)
			return false;

		UIDebugf1("Event handler found for %s in %s",pEvent->GetName(),GetName());

		pCmd = pHandler->m_pTargetCommand;
		if (pCmd)
		{
			while(pCmd)
			{
				pCmd->ProcessCommand(this);
				pCmd = pCmd->m_pNext;
			}
		}
		else
		{
			UIDebugf1("Handler has no target or actions,  it will just consume event and do nothing");
		}

		eventConsumed |= pHandler->m_bConsumeEvent;
	}

	if (!eventConsumed && IsInterruptible() == false) //queue up event.  this state does not want to be interrupted by parent handlers
	{
		if (pEvent->GetPriority() >= FUIEVENTLISTENER_DEFAULT_PRIORITY)
		{
			UIMANAGER->PushEventToInterruptingQueue(pEvent);

			UIDebugf1("State %s does not want to be interrupted by parents handling %s; event is now queued",GetName(),pEvent->GetName());            
		}

		//don't process this event further
		eventConsumed = true;
	}

	return eventConsumed;
}

bool UIObject::HandleInputs(fuiEvent* pEvent)
{
	bool eventConsumed = false;

	UIObject* pChild = m_pFirstChild;

	while(pChild && !eventConsumed)
	{
		if(pChild->IsActive() && pChild->IsFocused())
			eventConsumed |= pChild->HandleInputs(pEvent);			

		pChild = pChild->GetNextSibling();
	}

	if (!eventConsumed)
	{
		//find a handler defined in xml
		UIEventHandler*	pHandler= GetEventHandler(pEvent);
		UICommand*		pCmd	= NULL;

		if (!pHandler)
			return false;

		UIDebugf1("Event handler found for %s in %s",pEvent->GetName(),GetName());

		pCmd = pHandler->m_pTargetCommand;
		if (pCmd)
		{
			while(pCmd)
			{
				pCmd->ProcessCommand(this);
				pCmd = pCmd->m_pNext;
			}
		}
		else
		{
			UIDebugf1("Handler has no target or actions,  it will just consume event and do nothing");
		}

		eventConsumed |= pHandler->m_bConsumeEvent;
	}

	return eventConsumed;
}

void UIObject::SetName(const char* pName)
{
	m_szName = UIFACTORY->CreateString(pName);
}

void UIObject::RequestTransitionToThis()
{
	UIMANAGER->RequestTransition(this);
}

bool UIObject::ProcessDataTag(const char* ,const char* )
{
	////call ProcessDataTag on all children
	//for (UIObject* pChild=m_pFirstChild; pChild!=NULL; pChild = pChild->m_pNextSibling)
	//{
	//	pChild->ProcessDataTag(name,value);
	//}
	return false;
}

bool UIObject::ProcessAttributeTags(xmlTokenizerHack& tokenizer, const char* tagName)
{
	ClearComponentAttrValues();
	tokenizer.GetTagAttributes(tagName, a_szComponentAttrName, MAX_XML_ATTR_LEN, a_szComponentAttrValue,MAX_XML_ATTRIBUTES,false);
	for (int i=0;i<MAX_XML_ATTRIBUTES;i++)
	{
		if (a_szComponentAttrValue[i][0] && a_szComponentAttrName[i][0])
		{
			ProcessDataTag(a_szComponentAttrName[i] , a_szComponentAttrValue[i]);
		}
	}

	return false;
}

bool UIObject::GetPath(UIObject* pTarget, UITransitionPath& path)
{
	//find common relative
	UIObject*	pStartRelative;
	UIObject*	pTargetRelative;

	for( pStartRelative = this; pStartRelative;	pStartRelative = pStartRelative->m_pParent )
	{
		path.m_Exit.Push(pStartRelative);

		for (pTargetRelative = pTarget; pTargetRelative; pTargetRelative = pTargetRelative->m_pParent)
		{
			if (pStartRelative == pTargetRelative) //common relative found
				return true;

			//emulate stack style push
			if (!path.m_Enter.IsEmpty())
				path.m_Enter.Insert(0, pTargetRelative);
			else
				path.m_Enter.Push(pTargetRelative);
		}

		//no common relative found so clear enter path
		path.m_Enter.Reset();
	}

	return false;
}

UIObject* UIObject::GetFirstSibling()
{
	if (m_pParent)
		return m_pParent->m_pFirstChild;
	else
		return NULL;
}

const UIObject* UIObject::GetFirstSibling() const
{
	if (m_pParent)
		return m_pParent->m_pFirstChild;
	else
		return NULL;
}

UIObject* UIObject::GetPreviousSibling()
{
	UIObject* pFirstSibling = GetFirstSibling();
	Assert(pFirstSibling);
	if (!pFirstSibling || pFirstSibling == this)
		return NULL;

	while (pFirstSibling && pFirstSibling->m_pNextSibling != this)
	{
		pFirstSibling = pFirstSibling->m_pNextSibling;
		Assert(pFirstSibling);
	}

	return pFirstSibling;
}

UIObject* UIObject::GetLastSibling()
{
	UIObject* pLast = GetFirstSibling();

	if(pLast)
	{
		while (pLast->m_pNextSibling)
		{
			pLast = pLast->m_pNextSibling;
		}
	}

	return pLast;
}

void UIObject::Reset()
{
	UIObject* pChild = m_pFirstChild;
	UIObject* pNextChild = NULL;

	RemoveProperties(UI_VISIBLE | UI_ACTIVE | UI_ENABLED | UI_FOCUSED);

	while(pChild)
	{
		pNextChild = pChild->GetNextSibling();
		pChild->Reset();
		pChild = pNextChild;
	}
}

void UIObject::RemoveChild(u32 index)
{	
	UIObject* pPrevChild = NULL;
	UIObject* pChild = GetChild(index);
	UIObject* pNextChild = NULL;

	if (pChild)
	{
		if (index > 0)
		{
			pPrevChild = GetChild(index-1);
		}

		if (index < GetNumberOfChildren()-1)
		{
			pNextChild = GetChild(index+1);
		}

		if(pPrevChild)
		{
			pPrevChild->SetNextSibling(pNextChild);
		}
		else
		{
			//must be the first child in the list
			SetFirstChild(pNextChild);
		}
		pChild->Exit();
		pChild->Clear();
	}
	else
	{
		UIDebugf3("Child does not exist at index %d",index);
	}
}

void UIObject::RemoveChild(UIObject* pBadChild)
{	
	UIObject* pChild = m_pFirstChild;
	UIObject* pPrevChild = NULL;

	while(pChild != pBadChild)
	{
		pPrevChild = pChild;
		pChild = pChild->GetNextSibling();
	}

	if (pPrevChild)
	{
		pPrevChild->SetNextSibling(pChild->m_pNextSibling);
	}
	else
	{
		SetFirstChild(pChild->m_pNextSibling);
	}

	pBadChild->Exit();
	pBadChild->Clear();
}

void UIObject::RemoveAllChildren()
{	
	UIObject* pChild = GetFirstChild();
	UIObject* pNextChild = GetFirstChild();

	while (pChild && pChild->m_pNextSibling != this)
	{	
		pNextChild = pChild;
		pChild->Exit();
		pChild->DetachFromParent();
		pChild = pNextChild->m_pNextSibling;
	}

	m_pFirstChild = NULL;
}

void UIObject::DetachFromParent()
{
	m_pParent= NULL;
	m_pNextSibling= NULL;
	m_pFirstChild= NULL;
}

void UIObject::Clear()
{
	DetachFromParent();

	m_szName = NULL;
	m_pFirstEventHandler= NULL;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}//end namespace rage

