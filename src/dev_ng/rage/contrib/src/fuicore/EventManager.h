// 
// flashuimisc/EventManager.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FLASHUIMISC_EVENTMANAGER_H
#define FLASHUIMISC_EVENTMANAGER_H

// RAGE include
#include "atl/singleton.h"

// Event includes
#include "fuicore/Event.h"

// Global fuiEventManager defines
#define FUIEVENTMGR_EVENT_ELEMENT_POOL_COUNT		512

namespace rage {
// fuiEventManager declaration //
class fuiEventManager
{
	friend class fuiEventHashElement;

protected:
	// Do not invoke these... The creation and destruction of the Event Manager happens through atSingleton
	fuiEventManager();
	~fuiEventManager();

public:
	// Registration of events
	fuiEvent* RegisterEvent( fuiEventListener* pListener, const char* pStrEventIdentifier, s32 nPriority = FUIEVENTLISTENER_DEFAULT_PRIORITY );
	void UnregisterEvent( fuiEventListener* pListener, fuiEvent* pEvent );

	// Post Event
	void SendEvent( const char* pStrEventIdentifier, void* pData = NULL );
	void SendEvent( fuiEvent* pEventToSend );

	// Returns a hash element with a designated event string identifier
	const fuiEventHashElement* GetEventHashElement( const char* pStrEventIdentifier ) const;
	fuiEvent* GetEvent( const char* pStrEventIdentifier , bool createIfDoesntExist = true );
	
	// Binding Script function
	static void RegisterScriptCommands( void );		

	// Script native wrapper functions
	static void PostEventStr( const char* pStrEventIndentifier, const char* pStrData );
	static void PostEventInt( const char* pStrEventIndentifier, s32 nIntData );
	static void PostEventFloat( const char* pStrEventIndentifier, f32 fFloatData );

	bool IsEventInfoEnabled( void ) const { return m_bEventDebugInfo; }

protected:
	fuiEventDataPool*			m_pEventDataPool;
	fuiEventHashTable*			m_pEventHashTable;	// Hash table of all the registered Events
	bool						m_bEventDebugInfo;	// Whether or not we spew the event debug information
};

//////////////////////////////////////////////////////////////////////////
// fuiEventManager aliases
typedef atSingleton< fuiEventManager > fuiEventManagerSingleton;

// fuiEventManager singleton reference
#define FUIEVENTMGR fuiEventManagerSingleton::InstancePtr()

// Registration macros makes life so much easier
//#define REGISTER_EVENT( pEvent, pStrEventIdentifier )	pEvent = FUIEVENTMGR->RegisterEvent( this, pStrEventIdentifier ); 
#define REGISTER_EVENT( pEvent, pStrEventIdentifier, nPriority )	pEvent = FUIEVENTMGR->RegisterEvent( this, pStrEventIdentifier, nPriority ); 
#define UNREGISTER_EVENT( pEvent ) FUIEVENTMGR->UnregisterEvent( this, pEvent );
#define SEND_EVENT( pEvent ) FUIEVENTMGR->SendEvent( pEvent );
//////////////////////////////////////////////////////////////////////////

} // namespace rage
#endif // FLASHUIMISC_EVENTMANAGER_H
