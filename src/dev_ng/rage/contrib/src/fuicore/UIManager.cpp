// 
// fuicore/UIManager.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "fuicore/UIManager.h"
#include "atl/singleton.h"
#include "data/marker.h"
#include "file/asset.h"
#include "bank/bkmgr.h"
#include "diag/tracker.h"
#include "fuicomponents/UIObject.h"
#include "fuicomponents/Components.h"
#include "fuicore\Common.h"
#include "profile/profiler.h"
#include "system/param.h"
#include "grprofile/pix.h"
#include "system/timemgr.h"
#include "fuicore/EventManager.h"
#include "fuicore/Event.h"
#include "fuicore/EventListener.h"
#include "fuiflash/Flash.h"
#include "fuicore/UIStringTable.h"
#include "snu/snusocket.h"
#include "xmldata/xmltoken.h"
#include "fuicomponents/Transitions.h"
#include "fuicore/TextureStream.h"
#include "script/wrapper.h"
#include "script/hash.h"
#include "fuicore/UICallback.h"

#if __XENON
#include	<xdk.h>
#include	"system/xtl.h"
#include	<xbox.h>
#endif // __XENON
using namespace rage;

#define UIXMLLOCATION "$/content/ui/"

int UIFactory::m_UIObjectCount=0;
int UIFactory::m_UIObjectBytes=0;

#if __DEV
	rage::u32 s_HandlerBytes=0;
	rage::u32 s_CommandBytes=0;
	rage::u32 s_StringBytes=0;
	rage::u32 s_DataBytes=0;
	rage::u32 s_NumUIObject=0;
	rage::u32 s_NumDataTags=0;
	rage::u32 s_NumStates=0;
	rage::u32 s_NumCommands=0;
	rage::u32 s_NumStrings=0;
	rage::u32 s_NumHandlers=0;
#endif

PARAM(nofui, "Doesn't load and show the fui user interface.");

static RebootThread gRebootThread;

struct EventHandlerDesc
{
	char* m_szTag;
	char* m_szName;
};

const EventHandlerDesc a_EventDescriptors[] = 
{
	{"onenter",				"onEnter"				},
	{"onpush",				"onPush"				},
	{"ontransitioncomplete",	"OnTransitionComplete"	},
	{"onpop",					"onPop"					},
	{"onexit",				"onExit"				},
	{"onactivate",			"onActivate"			},
	{"ondeactivate",			"onDeactivate"			},
	{"onopen",				"onOpen"				},
	{"onclose",				"onClose"				},
	{"onshow",				"onShow"				},
	{"onhide",				"onHide"				},
	{"onfocused",				"onFocused"				},
	{"onunfocused",			"onUnfocused"			},
	{"onpaint",				"onPaint"				},
};


const char*	a_szUIObjectAttrName	[]	= { "id",		"desc",		"type",		"class",	"target",	"consume",	"" };
const char*	a_szTransitionAttrName	[]	= { "event",	"target",	"consume",							"" };
const char*	a_szDataAttrName		[]	= { "name",		"expr",											"" };
const char*	a_szOnTransAttrName		[]	= { "expr",														"" };
const char*	a_szActionAttrName		[]	= { "expr",														"" };
const char*	a_szIncludeAttrName		[]	= { "src",		"arg",		"count",							"" }; 
char 		a_szAttrValue_[7][MAX_XML_ATTR_LEN];
char*		a_szAttrValue[] = {a_szAttrValue_[0], a_szAttrValue_[1], a_szAttrValue_[2], a_szAttrValue_[3], a_szAttrValue_[4], a_szAttrValue_[5]};
void clear_attr_values()
{
	a_szAttrValue_[0][0] = '\0';
	a_szAttrValue_[1][0] = '\0';
	a_szAttrValue_[2][0] = '\0';
	a_szAttrValue_[3][0] = '\0';
	a_szAttrValue_[4][0] = '\0';
	a_szAttrValue_[5][0] = '\0';
	a_szAttrValue_[6][0] = '\0';
}

namespace UIStats
{
	PF_PAGE(UIPage,"UI");
	PF_GROUP(UIGroup);

	PF_TIMER(AllTotal,UIGroup);
	PF_TIMER(AllUpdate,UIGroup);
	PF_TIMER(AllDraw,UIGroup);
	PF_TIMER(MenuUpdate,UIGroup);

	PF_LINK(UIPage,UIGroup);
};

using namespace UIStats;


void fuiDelayedEvent::Disable()
{
	// Mark this delay event object as unused
	m_fCurrentTime = 0.0f;
	m_pEvent = NULL;
}

void fuiDelayedEvent::Update( rage::f32 fDeltaTime )
{
	AssertMsg( m_pEvent , "fuiDelayEventObj::Update - Attempt to update a disabled delay event object." );

	m_fCurrentTime -= fDeltaTime;
	if ( m_fCurrentTime <= 0.0f )
	{
		fuiEvent* pEvent = m_pEvent;

		Disable();
		UIMANAGER->SendEvent(pEvent);
	}
}

void fuiDelayedEvent::Register( fuiEvent* pEvent, rage::f32 fDelayTime, bool bPauseSensitive )
{
	m_pEvent			= pEvent;
	m_fCurrentTime		= fDelayTime;
	m_bPauseSensitive	= bPauseSensitive;
}

UIManager::UIManager() 
:   m_bShowUI(true)
,	m_pUserInterface(NULL)
,   m_pTextureStreamer(NULL)
,   m_pUINavigator(NULL)
,   m_pUIInput(NULL)
,   m_pUIFactory(NULL)
,   m_pUICamera(NULL)
,   m_pUIPlayer(NULL)
,	m_pUIGame(NULL)
,	m_bIsUILoaded(false)
,   m_bIsHandlingEvent(false)
,   m_ProcessQueuedEvents(false)
,	m_Disabled(false)
,	m_TrackingBucket(0)
{
	m_FuiWindowMutex = sysIpcCreateMutex();

	UIObjectFactorySingleton::Instantiate();
	fuiEventManagerSingleton::Instantiate();
	fuiFlashManagerSingleton::Instantiate();

	m_pUIInput = rage_new UIInput();
	m_pUINavigator = rage_new UINavigator();
	m_pUIPlayer = rage_new UIPlayer();
	//m_pUIGame = rage_new RDRGame();
	m_pUICamera = rage_new UICamera();
	m_pUIFactory = rage_new UIFactory();

	// create the texture streamer
	m_pTextureStreamer = rage_new UITextureStream();

	m_aDelayedEvents.Resize( 4 );
	//m_CommandHash.Init(100);

	Displayf("UIManager::UIManager()  Available memory: %3.3f M (Game %3.3fV + %3.3fP Rsc %3.3fV + %3.3fP)",sysMemAllocator::GetCurrent().GetMemoryAvailable() / (1024.f * 1024.f),
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetMemoryAvailable() / (1024.0f * 1024.f),
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_PHYSICAL)->GetMemoryAvailable() / (1024.0f * 1024.f),
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->GetMemoryAvailable() / (1024.0f * 1024.f),
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL)->GetMemoryAvailable() / (1024.0f * 1024.f));

}

UIManager::~UIManager()
{
	fuiFlashManagerSingleton::Destroy();
	UIStringTableSingleton::Destroy();
	fuiEventManagerSingleton::Destroy();
	UIObjectFactorySingleton::Destroy();

	delete m_pTextureStreamer;	

	sysIpcDeleteMutex(m_FuiWindowMutex);
}

void UIManager::Load()
{
	RAGE_FUNC();

	if (PARAM_nofui.Get())
	{
		UIDisplayf("The fui system is disabled via -nofui on your cmd line");
		SetDisabled(true);
		return;
	}

	if (m_UIFactory.IsBound() && m_pUIFactory)
	{
		m_UIFactory();
	}

	UIObject::SetupXmlArrays();

	RAGE_TRACK(FUI_XML);
	const char* szFile = "UserInterface.sc";
	ASSET.PushFolder("$/content/ui");
	m_pUIFactory->LoadFromXml(szFile);
	ASSET.PopFolder();

	Goto("Setup");

	//allows for post load/setup/init stuff
	RAGE_TRACK(FUI_PostLoad);
	PostLoad();

	m_bIsUILoaded = true;
}

#if __BANK
void UIManager::GotoInvisibleUIObject()
{
	UIMANAGER->RequestTransition("HudScene");
}
#endif

void UIManager::AssertAndDisableFUI(const char* ASSERT_ONLY(error))
{
	Assertf(0,"%s - Disabling the FUI system.  You will not be able to use the pause menu or networking UI.",error);
	SetDisabled(true);
}

void UIManager::AddDelayedEvent(const char* szEvent)
{
	fuiEvent* pEvent = FUIEVENTMGR->GetEvent(szEvent);
	

	// Try to find an unused delay object
	for (s32 nDelayedEvenIdx = 0; nDelayedEvenIdx < m_aDelayedEvents.GetCount(); nDelayedEvenIdx++ )
	{
		if( !m_aDelayedEvents[ nDelayedEvenIdx ].IsEnabled() )
		{
			m_aDelayedEvents[ nDelayedEvenIdx ].Register( pEvent, 2.0f, false );
			return;
		}
	}

	// Otherwise we can grow the array... for now assert
	AssertMsg( 0 , "VirtualHSM::RegisterDelayEvent - All delayed events are being used. Increase the total number of objects allocated. ");
}

//
// UI should be instantiated and loaded at this time
//
void UIManager::PostLoad()
{
	if (IsDisabled())
	{
		return;
	}

	m_pUserInterface = UIFACTORY->GetUIObject("UserInterface");
	if (!m_pUserInterface)
	{
		AssertAndDisableFUI("The content/ui xmls could not be found");
		return;
	}

	SetUserInterface(m_pUserInterface);


	// Initialize the texture streamer
	m_pTextureStreamer->Init(0/*base content*/, m_TrackingBucket);

	m_pUIFactory->PostLoad();
	m_pUINavigator->PostLoad();

#if __BANK
	bkBank& rUIBank = BANKMGR.CreateBank("UI Manager");
	AddWidgets(rUIBank);
#endif

	UI_REGISTER(UIManager, Pause);
	UI_REGISTER(UIManager, Resume);
	UI_REGISTER(UIManager, AddDelayedEvent);
	UI_REGISTER_STATIC(Reboot, UIManager::Reboot);
	UI_REGISTER_ADV(UIManager, SendEvent, SendEventCB , this);
	UI_REGISTER_ADV(UINavigator, Goto, GotoUIObject , m_pUINavigator);
	UI_REGISTER_ADV(UINavigator, Enter, EnterUIObject , m_pUINavigator);
	UI_REGISTER_ADV(UINavigator, Exit, ExitUIObject , m_pUINavigator);
	UI_REGISTER_ADV(UINavigator, Show, ShowUIObject , m_pUINavigator);
	UI_REGISTER_ADV(UINavigator, Hide, HideUIObject , m_pUINavigator);
	UI_REGISTER_ADV(UINavigator, Refresh, RefreshUIObject , m_pUINavigator);
	UI_REGISTER_ADV(UINavigator, Activate, ActivateUIObject , m_pUINavigator);
	UI_REGISTER_ADV(UINavigator, Deactivate, DeactivateUIObject , m_pUINavigator);
	UI_REGISTER_ADV(UINavigator, Focus, FocusUIObject , m_pUINavigator);
	UI_REGISTER_ADV(UINavigator, Unfocus, UnfocusUIObject , m_pUINavigator);
	UI_REGISTER_ADV(UINavigator, StackPop, StackPop , m_pUINavigator);
	UI_REGISTER_ADV(UINavigator, StackPush, StackPush , m_pUINavigator);
	UI_REGISTER_STATIC(SetFlashInt, fuiMovie::SetInt);
	UI_REGISTER_STATIC(SetFlashFloat, fuiMovie::SetFloat);
	UI_REGISTER_STATIC(SetFlashString, fuiMovie::SetString);

	//fire event for data to handle post loading from data
	SendEvent("postLoad");
}

bool RebootThread::Fire(const char *szType)
{
	if (IsRunning())
	{
		Displayf("RebootThread: Can not reboot twice");
		return false;
	}

	if(gRebootThread.Start("reboot",sysIpcMinThreadStackSize,0))
	{
		if(strcmp(szType,"DONGLE")==0)
		{
			m_Instance.m_eType		= RebootInstance::DONGLE;
		}
		//else if (strcmp(szType,"JOINWISH")==0)
		//{
		//	m_Instance.m_eType		= RebootInstance::JOINWISH;
		//	m_Instance.m_JoinWish	= NETMGR.GetJoinWish();
		//	m_Instance.m_iRebootPad	= NETMGR.GetJoinWish().m_pGamer->GetLocalGamerIndex();
		//}
		else if (strcmp(szType,"MOUNTDLC")==0)
		{
			//if (NETMGR.GetJoinWish().IsValid())
			//{
			//	// do a cross title invite join wish, this will automatically mount the content on this reboot
			//	m_Instance.m_eType		= RebootInstance::JOINWISH;
			//	//m_Instance.m_JoinWish	= NETMGR.GetJoinWish();
			//	//m_Instance.m_iRebootPad	= mcConfig::GetController(0);
			//}
			//else
			//{
			//	// do a mount of the DLC - there was no pending title invite
			//	m_Instance.m_eType		= RebootInstance::MOUNT_DOWNLOADABLECONTENT;
			//	//m_Instance.m_iRebootPad	= mcConfig::GetController(0);
			//}
		}
		else if (strcmp(szType,"PROFILE_CHANGE")==0)
		{
			m_Instance.m_eType		= RebootInstance::PROFILE_CHANGE;
		}

		gRebootThread.Wakeup();

		return true;
	}

	return false;	
}

void RebootThread::Perform()
{
	// perform soft reboot of MC without restarting the console.		
	Displayf("Rebooting game.... (reason=%d)", m_Instance.m_eType);
	Printf("Reboot gamer = %p pad=%d\n", m_Instance.m_JoinWish.m_pGamer, m_Instance.m_iRebootPad);
#if __XENON
	DWORD errorCode = XSetLaunchData( &m_Instance, sizeof(RebootInstance) );
	Displayf("Rebooting game.... (reason=%d error=%x", m_Instance.m_eType, errorCode);
	const char* progName = sysParam::GetProgramName();
	Displayf("Rebooting executable: %s", progName);
	XLaunchNewImage(progName,0);
#else
	//mcGame::SetWantReboot(true);
#endif
}

void UIManager::Reboot(const char* pType)
{
	gRebootThread.Fire(pType);	
}
void UIManager::Pause()
{
	m_pUIGame->Pause();
}

void UIManager::Resume()
{
	m_pUIGame->Resume();
}

void UIManager::SetUserInterface(UIObject* ui)
{
	m_pUINavigator->SetUserInterface(ui);
	m_pUIInput->SetUserInterface(ui);
	m_pUIFactory->SetUserInterface(ui);
}

//
// Update miscellaneous movies that are not set to manual update and are enabled.
//
void UIManager::Update(float dt)
{
	RAGE_TRACK(UIManager);
	LockMutex();

	if(IsDisabled())
	{
		return;
	}

	if (m_pUIInput)
	{
		m_pUIInput->UpdateMapper();

		m_pUIInput->Update();
	}

	if (m_pUINavigator)
	{
		m_pUINavigator->Update(dt);
	}

	if (m_pTextureStreamer)
	{
		m_pTextureStreamer->Update();
	}

	if (m_pUserInterface)
	{
		m_pUserInterface->Update(dt);
	}

	//if (FLASHMANAGER)
	//{
	//	FLASHMANAGER->Update(dt);
	//}

	//if (SAVEGAMEMANAGER)
	//{
	//	SAVEGAMEMANAGER.Update(dt);
	//}

	// Iterate through registered delay events and determine
	for( s32 nDelayedEventsIdx = 0; nDelayedEventsIdx < m_aDelayedEvents.GetCount(); nDelayedEventsIdx++ )
	{
		// Determine if the delayed object is enabled and if so update it
		if( m_aDelayedEvents[ nDelayedEventsIdx ].IsEnabled() )
		{
			m_aDelayedEvents[ nDelayedEventsIdx ].Update( dt );
		}
	}

	//throw queued events
	if (m_ProcessQueuedEvents && !IsTransitioning() && !m_bIsHandlingEvent)
	{
		m_ProcessQueuedEvents = false;

		//if we are doing nothing, then fire any queued events
		ThrowNextQueuedEvent();
	}

//	UIVariant unusedResult;
//	uiCmd cmd = s_CommandHash.Lookup(scrComputeHash( "UI_SHOW" ));
//	Assert(cmd);
//	if (cmd) {
//		UIVariant uiObject;
//		uiObject.Set(GetUIObject("UserInterface"));
//		UIInfo curInfo(&unusedResult, 1, &uiObject);
//		(*cmd)(curInfo);
//	}

	//ExecuteCommand("UI_SHOW",1,GetUIObject("UserInterface"));
	UnlockMutex();
}

void UIManager::DrawInViewport(const grcViewport* /*pGameVP*/,f32 dt,u32 pass)
{
	if(IsDisabled())
	{
		return;
	}

	if (m_pUserInterface)
	{
		m_pUserInterface->Draw(dt,pass);
	}

	//if (FLASHMANAGER)
	//{
	//	FLASHMANAGER->Draw(pGameVP);
	//}
}

void UIManager::Goto(const char* id)
{
	if(IsDisabled())
	{
		return;
	}

	UIObject* o = GetUIObject(id);
	o->Goto();
}

bool UIManager::RequestTransition(const char* c , UIObjectState transitionType)
{
	if(UIMANAGER->IsDisabled())
	{
		return false;
	}

	UIObject* o = UIMANAGER->GetUIObject(c);
	Assert(o);
	return UIMANAGER->GetUINavigator()->RequestTransition(o, transitionType);
}
bool UIManager::RequestTransition(UIObject* o , UIObjectState transitionType)
{
	if(UIMANAGER->IsDisabled())
	{
		return false;
	}

	return UIMANAGER->GetUINavigator()->RequestTransition(o,transitionType);
}

//void UIManager::TransitionFrom(const char* id)
//{
//	UIObject* o = UIMANAGER->GetUIObject(id);
//	o->TransitionFrom();
//}

UIObject* UIManager::GetUIObject(const char *id)
{
	if(IsDisabled())
	{
		return NULL;
	}

	if(m_pUIFactory)
	{
		UIObject* pObject = m_pUIFactory->GetUIObject(id);

		Assertf(pObject,"Couldn't find UIObject \"%s\"",id);

		return pObject;
	}
	else
	{
		return NULL;
	}
}

void UIManager::ProcessInterruptingQueue()
{
	if(IsDisabled())
	{
		return;
	}

	int count = m_InterruptingQueue.GetCount();

	if (count)
	{
		UIDebugf1("Interrupting event queue contains %d events",count);
	}

	for (int i=0; i!=count; i++)
	{
		fuiEvent* e = m_InterruptingQueue.Pop();
		UIDebugf1("Queuing up event %s from interrupting event queue", m_InterruptingQueue[i]->GetName());
		m_EventQueue.Push(e);
	}

	if (count)
		UIDebugf1("queueing up %d events from the interrupting event queue",count);

	m_ProcessQueuedEvents = true;
}

void UIManager::ThrowNextQueuedEvent()
{	
	if(IsDisabled())
	{
		return;
	}

	if( !m_EventQueue.IsEmpty() && !IsTransitioning() && !m_bIsHandlingEvent/* && !isInRenderThread() */)
	{

#if __DEV
		UIDebugf1("UI Event queue contains");
		for (int i=0; i!=m_EventQueue.GetCount(); i++)
		{
			UIDebugf1("%s", m_EventQueue[i]->GetName());
		}
#endif

		fuiEvent* event = m_EventQueue.Pop();

		//use this instead of SEND_EVENT otherwise it will just get queued again
		m_pUINavigator->SendEventTo(event->GetTargetObject(),event, event->IsInput());

		if( m_InterruptingQueue.GetCount() == 0)
		{
			//we are not in an un-interruptible UIObject so lets continue to process queued events
			m_ProcessQueuedEvents = true;
		}
	}
}

void UIManager::SendInput(const char* e)
{
	if(IsDisabled())
	{
		return;
	}

	fuiEvent* pEvent = FUIEVENTMGR->GetEvent(e);
	SendInput(pEvent);
}

void UIManager::SendInput(fuiEvent* pEvent)
{
	if(IsDisabled())
	{
		return;
	}

	SendEventTo(GetUserInterface() , pEvent , false, true);
}

bool UIManager::HandleEvents(fuiEvent* pEvent)
{
	if(IsDisabled())
	{
		return false;
	}

	SendEvent(pEvent, false);
	return true;
}

bool UIManager::SendEvent(fuiEvent* pEvent, bool isFromQueue)
{
	if(IsDisabled())
	{
		return false;
	}

	UIMANAGER->SendEventTo(UIMANAGER->GetUserInterface(), pEvent , isFromQueue);
	return false;
}

bool UIManager::SendEvent(const char *pEvent , void* pData , u8 priority, bool sendToCode,  bool sendToScripts, bool isFromQueue)
{
	if(IsDisabled())
	{
		return false;
	}

	UIMANAGER->SendEventTo(UIMANAGER->GetUserInterface(), pEvent , pData , priority, sendToCode, sendToScripts, isFromQueue);
	return false;
}

void UIManager::SendEventTo(const char* pUIObject, const char *pEvent , void* pData , u8 priority , bool sendToCode,  bool sendToScripts, bool isFromQueue)
{
	if(IsDisabled())
	{
		return;
	}

	UIMANAGER->SendEventTo(UIMANAGER->GetUIObject(pUIObject), pEvent , pData , priority, sendToCode, sendToScripts, isFromQueue);
}

void UIManager::SendEventTo(UIObject* pUIObject, const char *event , void* pData , u8 priority , bool sendToCode,  bool sendToScripts, bool isFromQueue)
{
	if(IsDisabled())
	{
		return;
	}

	fuiEvent* pEvent = FUIEVENTMGR->GetEvent(event);
	pEvent->SetData(pData);
	pEvent->SetPriority(priority);
	pEvent->SendToCode(sendToCode);
	pEvent->SendToScript(sendToScripts);

	UIMANAGER->SendEventTo(pUIObject , pEvent, isFromQueue);
}

void UIManager::SendEventTo(UIObject* pUIObject, fuiEvent* pEvent, bool isFromQueue, bool isInput)
{
	if(IsDisabled())
	{
		return;
	}

	UIDebugf1("Received event %s", pEvent->GetName());

	//save off the target object
	pEvent->SetTargetObject(pUIObject);
	pEvent->SetIsInput(isInput);

	//queue if we are in the render thread or transitioning
	if (IsTransitioning() || m_bIsHandlingEvent )
	{
		if (pEvent->GetPriority() >= FUIEVENTLISTENER_DEFAULT_PRIORITY)
		{
			UIDebugf1("Busy: Queuing up event %s", pEvent->GetName());
			UIDebugf1("IsTransitioning() = %d, m_bIsHandlingEvent = %d", IsTransitioning(),m_bIsHandlingEvent);

			QueueEvent(pEvent);			
		}
		else
		{
			UIDebugf1("Busy: Ignoring event [%s]", pEvent->GetName());
		}
	}
	else if (!isFromQueue && m_EventQueue.GetCount())//do not requeue queued events
	{
		if (pEvent->GetPriority() >= FUIEVENTLISTENER_DEFAULT_PRIORITY)
		{
			//new events will get put at the end of the line if we have queued events
			UIDebugf1("There are %d events in queue. Queueing [%s] so that we can process the previously fired and queued events first.",m_EventQueue.GetCount(),pEvent->GetName());			

			QueueEvent(pEvent);
		}
		else
		{
			UIDebugf1("Busy handling higher priority events: Ignoring event [%s]",pEvent->GetName());
		}
	}
	else
	{
		m_pUINavigator->SendEventTo(pUIObject,pEvent, isInput);

		//process the next queued event
		m_ProcessQueuedEvents = true;
	}
}

void UIManager::QueueEvent(fuiEvent* pEvent)
{
	if(IsDisabled())
	{
		return;
	}

	m_EventQueue.Push(pEvent);
}

void UIManager::PushEventToInterruptingQueue(fuiEvent* pEvent)
{
	if(IsDisabled())
	{
		return;
	}

	m_InterruptingQueue.Push(pEvent);

	//process the next queued event
	m_ProcessQueuedEvents = true;	
}

bool UIManager::IsTransitioning( void )
{
	if(IsDisabled())
	{
		return false;
	}

	if( UIMANAGER->GetUINavigator() )
	{
		return UIMANAGER->GetUINavigator()->IsTransitioning();
	}

	return false;
}

void UIManager::PushTransition(const char* pTransitionName)
{
	if(IsDisabled())
	{
		return;
	}

	m_pUINavigator->PushTransition(verify_cast<UITransition*>(UIMANAGER->GetUIObject(pTransitionName)));
}

void UIManager::SetPadIndex(u32 index)
{
	if (m_pUIInput)
		m_pUIInput->SetPadIndex(index);
}

#if __BANK

void UIManager::CaptureScreenShotJpeg()
{
	GRCDEVICE.CaptureScreenShotToJpegFile("C:\\screen_capture.JPG");
}

void UIManager::AddWidgets(bkBank& b)
{
	b.PushGroup("Components",true);
	{
		if  (m_pUserInterface)
		{
			m_pUserInterface->AddWidgets(b);
		}
	}
	b.PopGroup();

	FLASHMANAGER->AddWidgets(b);
	m_pUINavigator->AddWidgets(b);

	b.AddButton("Capture screenshot to JPEG", datCallback(MFA(UIManager::CaptureScreenShotJpeg), this));
	//b.AddButton("Output PROMPTMOVIE variables",	datCallback(MFA(UIManager::OutputPromptMovieVariables), this));
	//b.AddButton("Output pool sizes",	datCallback(MFA(UIManager::OutputPoolSizes), this));
 //   b.AddSlider("UI Spew Level",&UISpewLevel,0,2,1);
	//b.AddToggle("Disable UI",&m_bDisableUI);
}

#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIFactory
///////////////////////////////////////////////////////////////////////////////////////////////////////////
const UIObject* UIFactory::GetUIObjectByIdName(const char* szID) const
{
	return *m_UIObjects.Access(szID);
}

UIObject* UIFactory::GetUIObjectByIdName(const char* szID)
{
	UIObject** pData = m_UIObjects.Access(szID);

	if (pData)
	{
		return *pData;
	}
	else
	{
		//Assertf(0,"Could not find ui object.  You may need to get latest on content/ui");
		return NULL;
	}
}

bool UIFactory::LoadFromXml(const char* className, UIObject* pObject,xmlAsciiTokenizerXml& _xmlTokenizer, const UICommand& arg, UIReadMode readMode)
{
	bool ok = true;
	const char* pName = pObject->GetName();

	//set name
	if (pName)
	{
		Assert(strcmp(pName, a_szAttrValue[0]) == 0); // if name already set make sure it matches the attrib value
	}
	else
	{
		pName = CreateString(a_szAttrValue[0]);      
	}

	//set properties
	if (a_szAttrValue[2][0])
	{
		if(strstr(a_szAttrValue[2], "Uninterruptible"))
		{
			pObject->SetInterruptible(false);
		}
	}

	//set elements
	UIDebugf2("%s",pName);
	UIDebugf2("{");
	int childReads = 0;
	ok = ReadElementsFromXml(className, pObject,_xmlTokenizer, arg, readMode, childReads);
	UIDebugf2("}");

	return ok;
}

void UIFactory::RegisterUIObject(const char *name, UIObjectFactoryFunc func)
{	
	Assert(m_FactoryMap.Access(name)==NULL);
	m_FactoryMap.Insert(name, func);
}

UIObject* UIFactory::CreateUIObject(const char *classType)
{
	if (classType /*&& strlen(classType)*/)
	{
		UIObjectFactoryFunc *func = m_FactoryMap.Access(classType);
		if(func)
		{
			UIObject* pObject = (*func)();
			return pObject;
		}

		if (stricmp("UIObject",classType)!=0)
		{
			Assertf(0,"UI Class %s is not registered! This usually means you have a code-asset dependency. Attempting to default to a basic UIObject...",classType);
			return CreateUIObject("UIObject");
		}
		else
		{
			Assertf(0,"UI Class %s is not registered! This really sucks.",classType);
			return NULL;
		}
	}
	else
	{
		Assert(0); // do we ever get here at all?
		return NULL;
	}		
}

const char* UIFactory::CreateString(const char *szString)
{	
	char lowercase[256];
	safecpy(lowercase,szString,256);

	u32 hash = atStringHash(lowercase);
	const char** pString = m_StringIds.Access(hash);
	if (pString)
	{
		return *pString;
	}
	else
	{
		char* pNewString = rage_new char[strlen(szString)+1];
		safecpy(pNewString,szString,strlen(szString)+1);

#if __DEV
		s_NumStrings ++;
		s_StringBytes += (sizeof(char)*strlen(szString)+1);	
#endif
		m_StringIds.Insert(hash,pNewString);
		UIDebugf2("Allocating %d bytes for string \"%s\"",sizeof(char)*strlen(szString)+1, pNewString);
		return pNewString;
	}
}

fuiEvent* UIFactory::CreateNewEvent(const char* id)
{
	return FUIEVENTMGR->RegisterEvent(UIMANAGER->GetEventManager(),id);
}

//fuiEvent* UIFactory::CreateEvent(const char* id, )
//{
//	return FUIEVENTMGR->RegisterEvent(UIMANAGER->GetEventManager(),id);
//}

UIEventHandler*	UIFactory::NewEventHandler(UIObject* pObject)
{
	UIEventHandler* pLastHandler	= pObject->GetFirstEventHandler();
	UIEventHandler* pNewHandler	= rage_new UIEventHandler();

#if __DEV
	s_NumHandlers++;
	s_HandlerBytes += sizeof(UIEventHandler);
#endif

	UIDebugf2("Allocating %d bytes for handler ",sizeof(UIEventHandler));

	if (AssertVerify(pNewHandler))
	{
		if(pLastHandler)
		{
			while (pLastHandler->m_pNext)
			{
				pLastHandler = pLastHandler->m_pNext;
			}

			pLastHandler->m_pNext	= pNewHandler;
		}
		else
		{
			pObject->SetFirstEventHandler(pNewHandler);
		}

		pNewHandler->m_pNext = NULL;
	}

	return pNewHandler;
}

bool UIFactory::LoadEventHandler(const char* tag, const char* name, const UICommand& arg,UIObject* pObject,xmlTokenizerHack* pTokenizer)
{
	UIDebugf2(" %s element",tag);

	UIEventHandler* pEventHandler = NewEventHandler(pObject);

	if(!AssertVerify(pEventHandler))
		return false;

	clear_attr_values();
	pTokenizer->GetTag(tag, a_szOnTransAttrName, MAX_XML_ATTR_LEN, a_szAttrValue);

	pEventHandler->m_Trigger = CreateNewEvent(name);
	pEventHandler->m_Trigger->LoadFromString(name);
	pEventHandler->NewCommand()->LoadFromString	( a_szAttrValue[0], arg);

	if(!AssertVerify(pTokenizer->CheckEndTag(tag,true)))
		return false;

	return true;
}

UIObject* UIFactory::NewChild(UIObject* pUIObject, const char *szName, const char *stateClass)
{	
	UIObject* pNewChild = GetUIObjectByIdName(szName);

	if (!pNewChild)
	{
		pNewChild = CreateNewChild(szName,stateClass);

		Assert(pNewChild != NULL);
	}
	
	if (!pNewChild)
	{
		return 0;
	}

	if (pNewChild->GetParent())
	{
		Displayf("duplicate Child name: %s",szName);
		Assertf(false,"Duplicate State ID: %s", szName);
		return NULL;
	}

	pUIObject->SetupChildRelationships(pNewChild);

	return pNewChild;
}

bool UIFactory::ReadElementsFromXml(const char* szClassName, UIObject* pObject,xmlAsciiTokenizerXml& _xmlTokenizer, const UICommand& arg, UIFactory::UIReadMode readMode, int& childrenRead)
{
	xmlTokenizerHack&	tokenizer		= *((xmlTokenizerHack*)&_xmlTokenizer);
	char				szFilename		[MAX_XML_ATTR_LEN];	szFilename[0]='\0';
	char				szTokenName		[MAX_XML_ATTR_LEN];	szTokenName[0]='\0';
	xmlTokenizerHack	xmlTokenizer	;
	fiStream*			pXmlStream		= NULL;	

	while
		( 
		(readMode==UIReadMode_SkipAll||!tokenizer.CheckEndTag(szClassName,true))	&& 
		!tokenizer.CheckEndTag("scxml",false)									&& 
		!tokenizer.CheckEndTag("ifdef",false)
		)
	{
		int haveTag = tokenizer.GetTagName(szTokenName, MAX_XML_ATTR_LEN);

		bool tagProcessed = false;

		if (haveTag && readMode==UIReadMode_All)
		{
			tagProcessed = true;

			if (szTokenName[0]=='o' && szTokenName[1]=='n')
			{
				int iEventDesc = 0;

				for (iEventDesc=0; iEventDesc!=NELEM(a_EventDescriptors); iEventDesc++)
				{
					if(stricmp(szTokenName+2, a_EventDescriptors[iEventDesc].m_szTag+2) == 0)
						break;
				}
				if (iEventDesc!=NELEM(a_EventDescriptors))
				{
					const EventHandlerDesc& rDesc = a_EventDescriptors[iEventDesc];
					if(!LoadEventHandler(rDesc.m_szTag,rDesc.m_szName,arg,pObject,&tokenizer)) 
						return false;
				}
				else
				{
					UIErrorf("Unregistered event tag: %s", szTokenName);
					if(!AssertVerify(tokenizer.CheckEndTag(szTokenName,true)))
						return false;
				}
			}			
			else if (stricmp(szTokenName, "transition") == 0)
			{
				UIDebugf2("transition element");

				NewEventHandler(pObject)->LoadFromXml(_xmlTokenizer, arg);

				if(!AssertVerify(tokenizer.CheckEndTag("transition",true)))
					return false;
			}
			else if(stricmp(szTokenName, "data") == 0)
			{
				xmlTokenizerHack& tokenizer = *((xmlTokenizerHack*)&_xmlTokenizer);				

				clear_attr_values();
				tokenizer.GetTag("data", a_szDataAttrName, MAX_XML_ATTR_LEN, a_szAttrValue);

				//convert arg to its actual value before passing it on
				int argIndex=0;
				if (sscanf(a_szAttrValue[0], "arg%d", &argIndex) == 1)							//a style sheet argument?
				{
					arg.GetArg(argIndex)->PrintValue(a_szAttrValue[0]);
				}
				if (sscanf(a_szAttrValue[1], "arg%d", &argIndex) == 1)							//a style sheet argument?
				{
					arg.GetArg(argIndex)->PrintValue(a_szAttrValue[1]);
				}

				UIDebugf2("data element (%s,%s)",a_szAttrValue[0],a_szAttrValue[1]);

				if (pObject->ProcessDataTag( a_szAttrValue[0],a_szAttrValue[1]) == false)
					UIDebugf1("data element does not exist for component and is not used");

				if(!AssertVerify(tokenizer.CheckEndTag("data",true)))
					return false;
			}
			else
			{
				tagProcessed = false;
			}
		}

		if (haveTag && !tagProcessed && readMode!=UIReadMode_SkipAll)
		{
			tagProcessed = true;

			clear_attr_values();

			UIObjectFactoryFunc* pClass = m_FactoryMap.Access(szTokenName);
			if (pClass && stricmp(szTokenName, "uiobject") != 0)
			{
				//store the class name in a_szAttrValue[3] so it can be processed below
				safecpy(a_szAttrValue[3],szTokenName,MAX_XML_ATTR_LEN);
			}

			if (pClass || stricmp(szTokenName, "uiobject") == 0)
			{
				tokenizer.GetTag(szTokenName, a_szUIObjectAttrName, MAX_XML_ATTR_LEN, a_szAttrValue,false,false);

				//determine name
				if(!a_szAttrValue[0][0])
					formatf(a_szAttrValue[0], MAX_XML_ATTR_LEN, "%s.s%d", pObject->GetName(), childrenRead);

				UIObject* pUIObject = NULL;

				if (readMode==UIReadMode_ClassOnly)
				{
					pUIObject = NewChild(pObject, a_szAttrValue[0], a_szAttrValue[3]);
					Assert(pUIObject);				
				}
				else if (readMode==UIReadMode_All)
				{
					pUIObject = GetUIObjectByIdName(a_szAttrValue[0]);	
					Assert(pUIObject);
					if (pUIObject)
					{
						pUIObject->ProcessAttributeTags(tokenizer,szTokenName);
					}
				}
				
				if(!pUIObject)
					return false;

				//consume the tag
				tokenizer.GetTag(szTokenName, a_szUIObjectAttrName, MAX_XML_ATTR_LEN, a_szAttrValue,false);

				if (a_szAttrValue[1][0])
					pUIObject->SetText(a_szAttrValue[1]);

				if (a_szAttrValue[4][0] && readMode==UIReadMode_All)
				{
					UIEventHandler* pAction = NewEventHandler(pUIObject);

					pAction->m_Trigger		= FUIEVENTMGR->GetEvent("action",true);
					if (a_szAttrValue[5][0] && a_szAttrValue[5][0] == 'f')
					{
						pAction->m_bConsumeEvent= false;
					}
					else
					{
						pAction->m_bConsumeEvent= true;
					}

					pAction->NewCommand()	->LoadFromString(a_szAttrValue[4],arg);									
				}

				if(!AssertVerify(LoadFromXml(szTokenName,pUIObject,_xmlTokenizer, arg, readMode)))
					return false;

				childrenRead++;
			}
			else if(stricmp(szTokenName, "ifdef") == 0)
			{
				UIDebugf2("ifdef element");

				clear_attr_values();
				tokenizer.GetTag("ifdef", a_szOnTransAttrName, MAX_XML_ATTR_LEN, a_szAttrValue);

				bool	bDefined	= false;
				int		argIdx		= 0;

				if (sscanf(a_szAttrValue[0], "arg%d", &argIdx)==1 && arg.GetArg(argIdx))
					bDefined = true;
#if __XENON
				else if (0==stricmp(a_szAttrValue[0], "XENON"))
					bDefined = true;
#elif __PPU
				else if (0==stricmp(a_szAttrValue[0], "PSN"))
					bDefined = true;
#endif

				ReadElementsFromXml(szClassName, pObject, _xmlTokenizer, arg, bDefined? readMode : UIReadMode_SkipAll, childrenRead);

				if(!AssertVerify(tokenizer.CheckEndTag("ifdef",true)))
					return false;
			}
			else if(stricmp(szTokenName, "include") == 0)
			{
				clear_attr_values();

				char				szFileArg		[MAX_XML_ATTR_LEN];
				int					count			= 1;
				bool				ok				= true;
				UICommand			includeArg		;

				tokenizer.GetTag("include", a_szIncludeAttrName, MAX_XML_ATTR_LEN, a_szAttrValue);

				UIDebugf2("include file %s", a_szAttrValue[0]);

				if (a_szAttrValue[2][0])
					AssertVerify(sscanf(a_szAttrValue[2], "%d", &count)==1);

				//include arguments
				safecpy(szFileArg, a_szAttrValue[1], MAX_XML_ATTR_LEN );
				if (readMode==UIReadMode_All)
				{
					if(!AssertVerify(includeArg.LoadArgsFromString( szFileArg, '\0', arg)))
						return false;
				}

				//load file stream
				if (szFilename[0] && 0==stricmp(szFilename, a_szAttrValue[0]) && AssertVerify(pXmlStream))
				{
					//don't need to load file steam
				}
				else
				{
					if (pXmlStream)
						AssertVerify(pXmlStream->Close()==0); //close old stream if it exists

					safecpy(szFilename,	a_szAttrValue[0], MAX_XML_ATTR_LEN );

					pXmlStream	= ASSET.Open(szFilename, "xml");

					// Preload the file here instead of in the XML parser because we want to be able to re-parse the file
					// and preloading inside the XML parser may invalidate our stream pointer.
					if (pXmlStream)
						pXmlStream = fiStream::PreLoad(pXmlStream);
				}

				Assertf(pXmlStream, "Could not load file \"%s\"", szFilename);

				//read elements
				if(pXmlStream)
				{
					int pos;
					char szDirectory[128];
					strcpy(szDirectory, szFilename);		

					for (pos = strlen(szFilename); pos; pos--)
					{
						if (szDirectory[pos]=='/')
						{
							szDirectory[pos] = '\0';
							UIDebugf2("Pushing Asset path %s", szDirectory);
							ASSET.PushFolder(szDirectory);
							break;
						}
					}		

					for (int i=0; i!=count; i++)
					{
						pXmlStream->Seek(0);
						// Do not preload inside the XML parser because we want to be able to re-parse the file
						// and preloading inside the XML parser may invalidate our stream pointer.
						xmlTokenizer.Init("UITest", pXmlStream, false);
						if (!xmlTokenizer.CheckTag("scxml"))
						{
							char namespaceXsi[MAX_XML_ATTR_LEN];
							char namespaceSchema[MAX_XML_ATTR_LEN];
							const char * scxmlAttrNames[] = { "xmlns:xsi", "xsi:noNamespaceSchemaLocation", ""};
							char *scxmlAttrVals[] = {namespaceXsi, namespaceSchema};
							xmlTokenizer.GetTag("scxml", scxmlAttrNames, MAX_XML_ATTR_LEN, scxmlAttrVals, false); //empty
						}
						ok &= ReadElementsFromXml(szClassName,pObject,xmlTokenizer,includeArg,readMode,childrenRead);	if(!AssertVerify(ok)) return false;
						ok &= xmlTokenizer.CheckEndTag("scxml");										if(!AssertVerify(ok)) return false;
					}

					if (pos)
					{
						UIDebugf2("Popping Asset path %s", szDirectory);
						ASSET.PopFolder();
					}
				}

				if(!AssertVerify(tokenizer.CheckEndTag("include",true)))
					return false;
			}
			else
			{
				tagProcessed = false;
			}
		}

		if(!tagProcessed)
		{
			char temp[32];
			tokenizer.GetToken(temp,32);

			if(readMode==UIReadMode_All)
			{
				Assertf(false,  "unrecognized tag, \"%s\" in state scope, might not be supported yet", temp);
				return false;
			}
		}
	}

	if (pXmlStream)
		AssertVerify(pXmlStream->Close()==0); //close old stream if it exists

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIInput
///////////////////////////////////////////////////////////////////////////////////////////////////////////

const char* fnInputEventName[] =
{
	//digital pressed
	"up",
	"down",
	"left",
	"right",
	"action",
	"cancel",
	"select",
	"start",
	"x",
	"y",
	"l1",
	"r1",
	"l2",
	"r2",
	"l3",
	"r3",
	"lAnalogUp", //IOMS_PAD_ANALOGBUTTON
	"lAnalogDown", //IOMS_PAD_ANALOGBUTTON
	"lAnalogLeft", //IOMS_PAD_ANALOGBUTTON
	"lAnalogRight", //IOMS_PAD_ANALOGBUTTON
	"rAnalogUp", //IOMS_PAD_ANALOGBUTTON
	"rAnalogDown", //IOMS_PAD_ANALOGBUTTON
	"rAnalogLeft", //IOMS_PAD_ANALOGBUTTON
	"rAnalogRight", //IOMS_PAD_ANALOGBUTTON

	//digital released
	"up_released",
	"down_released",
	"left_released",
	"right_released",
	"action_released",
	"cancel_released",
	"select_released",
	"start_released",
	"x_released",
	"y_released",
	"l1_released",
	"r1_released",
	"l2_released",
	"r2_released",
	"l3_released",
	"r3_released",
	"lAnalogUp_released", //IOMS_PAD_ANALOGBUTTON
	"lAnalogDown_released", //IOMS_PAD_ANALOGBUTTON
	"lAnalogLeft_released", //IOMS_PAD_ANALOGBUTTON
	"lAnalogRight_released", //IOMS_PAD_ANALOGBUTTON
	"rAnalogUp_released", //IOMS_PAD_ANALOGBUTTON
	"rAnalogDown_released", //IOMS_PAD_ANALOGBUTTON
	"rAnalogLeft_released", //IOMS_PAD_ANALOGBUTTON
	"rAnalogRight_released", //IOMS_PAD_ANALOGBUTTON

	//digital repeat
	"up_repeat",
	"down_repeat",
	"left_repeat",
	"right_repeat",
	"action_repeat",
	"cancel_repeat",
	"select_repeat",
	"start_repeat",
	"x_repeat",
	"y_repeat",
	"l1_repeat",
	"r1_repeat",
	"l2_repeat",
	"r2_repeat",
	"l3_repeat",
	"r3_repeat",
	"lAnalogUp_repeat", //IOMS_PAD_ANALOGBUTTON
	"lAnalogDown_repeat", //IOMS_PAD_ANALOGBUTTON
	"lAnalogLeft_repeat", //IOMS_PAD_ANALOGBUTTON
	"lAnalogRight_repeat", //IOMS_PAD_ANALOGBUTTON
	"rAnalogUp_repeat", //IOMS_PAD_ANALOGBUTTON
	"rAnalogDown_repeat", //IOMS_PAD_ANALOGBUTTON
	"rAnalogLeft_repeat", //IOMS_PAD_ANALOGBUTTON
	"rAnalogRight_repeat", //IOMS_PAD_ANALOGBUTTON

	"lAnalog",
	"rAnalog",
	"lAnalog_released",
	"rAnalog_released",
	"lAnalog_repeat",
	"lAnalog_repeat",

	NULL
};

const char* fnInputCmdName[] =
{
	"flush",
	NULL
};


int UIInput::fnInputEventMap[] = 
{
	ioPad::LUP, 	
	ioPad::LDOWN, 
	ioPad::LLEFT, 
	ioPad::LRIGHT,
	ioPad::RDOWN, 
	ioPad::RRIGHT,
	ioPad::SELECT,
	ioPad::START,
	ioPad::RLEFT,
	ioPad::RUP,
	ioPad::L1,
	ioPad::R1,
	ioPad::L2,
	ioPad::R2,
	ioPad::L3,
	ioPad::R3,
	IOM_AXIS_LUP,
	IOM_AXIS_LDOWN,
	IOM_AXIS_LLEFT,
	IOM_AXIS_LRIGHT,
	IOM_AXIS_RUP,
	IOM_AXIS_RDOWN,
	IOM_AXIS_RLEFT,
	IOM_AXIS_RRIGHT
};

rage::ioMapperSource mapperSources[] =
{
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_DIGITALBUTTON,
	IOMS_PAD_AXIS,
	IOMS_PAD_AXIS,
	IOMS_PAD_AXIS,
	IOMS_PAD_AXIS,
	IOMS_PAD_AXIS,
	IOMS_PAD_AXIS,
	IOMS_PAD_AXIS,
	IOMS_PAD_AXIS
};

ioValue	a_InputButton		[NELEM(UIInput::fnInputEventMap)];

UIInput::UIInput()
: m_IsPressed		(false)
, m_bConnected		(true)
, m_bIsDisabled (false)
, m_ActiveResource (0)
{
	int i;

	for (i=0; i!=NELEM(fnInputEventMap); i++)
		m_Mapper.Map(mapperSources[i], fnInputEventMap[i], a_InputButton[i]);

	m_AnalogDeadZone			= 0.5f;

	// set the PAD index (called player down in the mapper - but this is not correct on 360)
//TODO, THE 0 MAY BE WRONG FOR RDR2, NEED TO SEE WHAT WE SHOULD BE PASSING IN THERE
	m_Mapper.SetPlayer( 0 );

#if __PPU
	m_swapCircleButton = false;
	//int value;
	//Assert(!cellSysutilGetSystemParamInt(CELL_SYSUTIL_SYSTEMPARAM_ID_ENTER_BUTTON_ASSIGN, &value));
	//if(cellSysutilGetSystemParamInt(CELL_SYSUTIL_SYSTEMPARAM_ID_ENTER_BUTTON_ASSIGN, &value) == 0)
	//{
	//	if(value == CELL_SYSUTIL_ENTER_BUTTON_ASSIGN_CIRCLE)
	//	{
	//		m_swapCircleButton = true;
	//	}
	//}
#endif
}

void UIInput::UpdateMapper()
{
	m_Mapper.Update();
}

bool UIInput::IsDown(fnInputEventID index )
{
	if (a_InputButton[index].IsDown())
	{
		return true;
	}
	else
	{
		return false;
	}
}

void UIInput::SetPadIndex(u32 index)
{
	m_Mapper.SetPlayer(index);
}

void UIInput::Update()
{
	char keyNameBuffer[256];

	//lets just disable all input when we are performing a camera transition
	if ( m_bIsDisabled || m_ActiveResource==0)
		return;

	m_IsPressed = false;

	if (!SNUSOCKET.GetSysUi().IsUiShowing())
	{
		//digital buttons		
		for (int i=0; i!=NELEM(fnInputEventMap); i++)
		{
			int	iEvent	= i;
#if __PPU
			if(m_swapCircleButton)
			{
				if(i == fnInputEventAction)
				{
					iEvent	= fnInputEventCancel;
				}
				else if(i == fnInputEventCancel)
				{
					iEvent	= fnInputEventAction;
				}
			}
#endif //__PSN

			if(a_InputButton[i].IsPressed())
			{
				UIDebugf2("Button %d pressed",i);
				m_IsPressed = true;

				safecpy(keyNameBuffer,fnInputEventName[iEvent],sizeof(keyNameBuffer) - strlen(keyNameBuffer));
				UIMANAGER->SendInput(keyNameBuffer);

				//send non-directional analog events
				if (iEvent == IOM_AXIS_LUP 
					|| IOM_AXIS_LDOWN
					|| IOM_AXIS_LLEFT
					|| IOM_AXIS_LRIGHT)
				{
					UIMANAGER->SendInput("lAnalog");
				}
				else if (iEvent == IOM_AXIS_RUP 
					|| IOM_AXIS_RDOWN
					|| IOM_AXIS_RLEFT
					|| IOM_AXIS_RRIGHT)
				{
					UIMANAGER->SendInput("rAnalog");
				}
			}
			else if(a_InputButton[i].IsReleased())
			{
				m_IsPressed = false;

				safecpy(keyNameBuffer,fnInputEventName[iEvent+NELEM(fnInputEventMap)],sizeof(keyNameBuffer) - strlen(keyNameBuffer));
				UIMANAGER->SendInput(keyNameBuffer);

				//send non-directional analog events
				if (iEvent == IOM_AXIS_LUP 
					|| IOM_AXIS_LDOWN
					|| IOM_AXIS_LLEFT
					|| IOM_AXIS_LRIGHT)
				{
					UIMANAGER->SendInput("lAnalog_released");
				}
				else if (iEvent == IOM_AXIS_RUP 
					|| IOM_AXIS_RDOWN
					|| IOM_AXIS_RLEFT
					|| IOM_AXIS_RRIGHT)
				{
					UIMANAGER->SendInput("rAnalog_released");
				}
			}
			else if( a_InputButton[i].IsDown())
			{
				safecpy(keyNameBuffer,fnInputEventName[iEvent+2*NELEM(fnInputEventMap)],sizeof(keyNameBuffer) - strlen(keyNameBuffer));
				UIMANAGER->SendInput(keyNameBuffer);

				//send non-directional analog events
				if (iEvent == IOM_AXIS_LUP 
					|| iEvent == IOM_AXIS_LDOWN
					|| iEvent == IOM_AXIS_LLEFT
					|| iEvent == IOM_AXIS_LRIGHT)
				{
					UIMANAGER->SendInput("lAnalog_repeat");
				}
				else if (iEvent == IOM_AXIS_RUP 
					|| iEvent == IOM_AXIS_RDOWN
					|| iEvent == IOM_AXIS_RLEFT
					|| iEvent == IOM_AXIS_RRIGHT)
				{
					UIMANAGER->SendInput("rAnalog_repeat");
				}
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////
//UINavigator
///////////////////////////////////////////////////////////////////////////////////////////////////////////

const char* fnUIEventName[] =
{
	"refresh",
	"start",
	"postLoad",
	"flashTransitionComplete",
	"transitionComplete",
	NULL
};

enum UI_ID
{
	UI_PushTransition,
	UI_SetTransitionTarget,
	UI_SetTransitionTargetStart, 
	UI_SetTransitionTargetFinal,

	UI_Count
};

const char* UI_Name[] =
{
	"pushTransition",
	"setTransitionTarget",
	"setStartTarget",
	"setFinalTarget",

	NULL
};

CompileTimeAssert(NELEM(UI_Name)==UI_Count+1);

UINavigator::UINavigator()
: m_bIsTransitioning		(false)
, m_bIsHandlingEvent		(false)
, m_pTargetState			(NULL)
, m_pCurrentState			(NULL)
, m_pUserInterface			(NULL)
, m_UIObjectStackHeight		(0)
{	
}

UINavigator::~UINavigator() 
{
	//end and delete any transitions
	for(int i=0;i<mCurrentTransitions.size();i++)
	{
		//if (mCurrentTransitions[i]->IsComplete())
		{
			mCurrentTransitions[i]->Terminate();
			mCurrentTransitions[i]->SetFocused(false);
			mCurrentTransitions[i]->SetVisible(false);
			mCurrentTransitions[i]->Deactivate();
			mCurrentTransitions[i]->Exit();
			mCurrentTransitions.Delete(i);
		}
	}
	mCurrentTransitions.empty();
}

#if __BANK

void UINavigator::AddWidgets(bkBank& b)
{
	b.AddButton("Output state stack", datCallback(MFA(UINavigator::OutputStateStack), this));
}

void UINavigator::OutputStateStack()
{
	for (int i=0;i<m_UIObjectStackHeight;i++)
	{
		UIDebugf1("%s",m_UIObjectStack[i]->GetName());
	}
}

#endif

//component style "turning on" of a component
void UINavigator::TransitionToUIObject(const char *UIObjectName)
{
	UIObject* pUIObject = verify_cast<UIObject*>(UIMANAGER->GetUIObject(UIObjectName));
	pUIObject->Enter();
	pUIObject->Activate();
	pUIObject->SetVisible(true);
	pUIObject->SetFocused(true);
}

void UINavigator::SendEventTo(UIObject* pUIObject, fuiEvent* pEvent, bool isInput)
{
	m_bIsHandlingEvent = true;
	bool bConsumeEvent = false;
	UIObject* pPopup = NULL;

	if (pEvent->ShouldSendToCode())
	{
		//top popup gets to handle all events
		if ( m_pPopupQueue.GetCount())
		{
			pPopup = m_pPopupQueue.Top();
			if (isInput)
			{
				bConsumeEvent |= pPopup->HandleInputs( pEvent );
			}
			else
			{
				bConsumeEvent |= pPopup->SendEvent( pEvent );
			}

			//walk down the popup stack and send non input events to the popups
			if(bConsumeEvent==false && m_pPopupQueue.GetCount())
			{
				//cycle through all the children starting with the top
				if (m_pPopupQueue.Top() == pPopup)
				{
					pPopup = m_pPopupQueue.Pop();
					m_pPopupQueue.Push(pPopup);
				}

				for(int i=m_pPopupQueue.GetCount()-2;i>=0;--i)
				{
					pPopup = m_pPopupQueue.Top();
					if (!bConsumeEvent)
					{
						bConsumeEvent |= pPopup->SendEvent( pEvent );
					}
					pPopup = m_pPopupQueue.Pop();
					m_pPopupQueue.Push(pPopup);
				}
			}
		}

		//only let the UI handle event if it is high priority
		if (bConsumeEvent==false)
		{
			if (pEvent->GetPriority() >= FUIEVENTLISTENER_DEFAULT_PRIORITY)
				UIDebugf1("Sending event to user interface");

			//handle the event
			if (UIMANAGER->IsInterruptible())
			{
				if (isInput)
				{
					bConsumeEvent |= pUIObject->HandleInputs( pEvent );
				}
				else
				{
					for (int i=0;i<m_UIExternalEventListeners.GetCount();i++)
					{
						//left out consume logic cause i think ppl may forget to return the correct value.
						m_UIExternalEventListeners[i]->SendEvent( pEvent );
					}
					bConsumeEvent |= pUIObject->SendEvent( pEvent );				
				}
			}
		}
		m_bIsHandlingEvent = false;
	}
}

bool UINavigator::RequestTransition(UIObject* pTargetScene , UIObject::UIObjectState transitionType)
{
	if (!m_pTargetState)
	{
		m_pTargetState = UIMANAGER->GetUIObject("UserInterface");
	}

	UIDebugf1("Transition requested from current %s to %s", m_pTargetState->GetName(), pTargetScene->GetName());

//TODO: lets try allowing transitioning mid-transition
	if (IsTransitioning())
	{
		UIDebugf1("Transition requested for a UIObject already in transition!");
		m_TransitionQueue.Push(pTargetScene);
		m_TransitionTypeQueue.Push(transitionType);
		return false;
	}

	m_pCurrentState = m_pTargetState;
	m_pTargetState = pTargetScene;

	if(m_pCurrentState == NULL)
	{
		//set to the user interface root UIObject
		m_pCurrentState = m_pUserInterface;
	}

	m_bIsTransitioning = true;

	m_TransitionType = transitionType;
	m_TransitionState = eTransitionOutStart;	
	TransitionOutStart();		
	return true;
}

bool UINavigator::IsShowingDialog() const
{
	return m_pPopupQueue.GetCount()>0;
}

void UINavigator::PushPopup(UIObject* pPopup, bool force)
{
	//interrupt the current popup with this one.  WE SHOULD RARELY DO THIS!!
	if (m_pPopupQueue.GetCount() && force)
	{
		UIObject* pTop = m_pPopupQueue.Top();
		if (pTop)
		{
			pTop = verify_cast<UIComponent*>(pTop);
			if (pTop)
			{
				pTop->SetVisible(false);
				pTop->SetFocused(false);

				m_pPopupQueue.PushTop(pPopup);
			}
		}
	}
	else
	{
		m_pPopupQueue.Push(pPopup);
	}
}

void UINavigator::PopPopup(UIObject* pPopup)
{
	if (!m_pPopupQueue.GetCount())
	{
		Assertf(0,"You are trying to pop %s off the popup queue, but there are no popups to pop!",pPopup->GetName());
		return;
	}

	int index;
	UIComponent* pObject;
	UIObject* pTop = m_pPopupQueue.Top();

	if (m_pPopupQueue.Find(pPopup,&index))
	{
		if (index>=0 && index<m_pPopupQueue.GetCount())
		{
			m_pPopupQueue.Delete(index);

			if (m_pPopupQueue.GetCount())
			{
				if(pTop == pPopup)//only activate and focus top popup if it was not already the top popup before pop
				{
					pTop = m_pPopupQueue.Top();
					if (pTop)
					{
						pObject = verify_cast<UIComponent*>(pTop);
						if (pObject)
						{
							Assert(!pObject->IsVisible() && !pObject->IsFocused() && !pObject->IsActive());

							pObject->SetVisible(true);
							pObject->Activate();
							pObject->SetFocused(true);
							pObject->Refresh();
						}
					}
				}
			}
			else
			{
				//popups are done so refresh the scene
				if (m_pCurrentState)
				{
					m_pCurrentState->Refresh();
				}
			}
		}
	}
}


void UINavigator::Update(f32 dt)
{
	UpdateTransitions(dt);

	//HACK: update the current popup
	//This has the possability of updating a popup twice.  I am doing this because some older xml does not guarantee that a popups parent will be active and therefore not updateable
	if (m_pPopupQueue.GetCount())
	{
		m_pPopupQueue.Top()->Update(dt);
	}
}

void UINavigator::TransitionOutStart()
{
	UIObject* pCurrentState = NULL;

	//start the actual transition from start state to top state
	if (m_TransitionType&UIObject::UI_FOCUSED)
	{
		UnfocusOutTransitionPath();
	}

	//walk up the queue and find the first state with an exit transition
	int n = m_TempTransitionPath.m_Exit.GetCount();
	for(int i = 0;i< n ;++i)
	{
		pCurrentState = verify_cast<UIObject*>(m_TempTransitionPath.m_Exit.Top());
		if(pCurrentState && pCurrentState->GetOutTransition())
		{
			break;
		}
		else
		{
			m_TempTransitionPath.m_Exit.Pop();
		}
	}

	if (pCurrentState && pCurrentState->GetOutTransition())
	{
		//start the exit transition
		UITransition* out = pCurrentState->GetOutTransition();
		out->Enter();
		m_TransitionState = eTransitionOut;
		TransitionOut();
	}
	else
	{
		m_TransitionState = eTransitionInStart;
		TransitionInStart();
	}
}

void UINavigator::TransitionOut()
{
	//poll for exit transition completion
	UITransition* out =  verify_cast<UIObject*>(m_TempTransitionPath.m_Exit.Top())->GetOutTransition();
	if (out && out->IsComplete())
	{
		out->Exit();

		m_TempTransitionPath.m_Exit.Pop();

		if (m_TempTransitionPath.m_Exit.GetCount()>0 && verify_cast<UIObject*>(m_TempTransitionPath.m_Exit.Top())->GetOutTransition())
		{
			//start the next exit transition
			UITransition* out =  verify_cast<UIObject*>(m_TempTransitionPath.m_Exit.Top())->GetOutTransition();
			out->Enter();
		}
		else
		{
			//we have traversed all exit states to the root and run all exit transitions
			m_TransitionState = eTransitionInStart;
			TransitionInStart();
		}
	}							
}
void UINavigator::TransitionInStart()
{
	UIObject* pCurrentState = NULL;

	//open the target state (if it is not a root state)
	if (m_pTargetState)						
	{					
		//walk up the queue and find the first state with an enter transition
		int n = m_TempTransitionPath.m_Enter.GetCount();
		for(int i = 0;i<n;++i)
		{
			pCurrentState = verify_cast<UIObject*>(m_TempTransitionPath.m_Enter.Top());
			if(pCurrentState && pCurrentState->GetInTransition())
			{
				break;
			}
			else
			{
				m_TempTransitionPath.m_Enter.Pop();
			}
		}

		if (pCurrentState && pCurrentState->GetInTransition())
		{
			UITransition* in =  pCurrentState->GetInTransition();

			//start the enter transition
			in->Enter();
			m_TransitionState = eTransitionIn;
			TransitionIn();
		}
		else
		{
			if (m_TransitionType&UIObject::UI_ACTIVE)
			{
				DeactivateOutTransitionPath();
				ActivateInTransitionPath();
			}

			//target state does not have an enter transition so just transition to it now			
			if (m_TransitionType&UIObject::UI_FOCUSED)
			{
				FocusInTransitionPath();
			}

			m_TransitionState = eTransitionComplete;
			TransitionComplete();
		}
	}
	else
	{		
		if (m_TransitionType&UIObject::UI_ACTIVE)
		{
			DeactivateOutTransitionPath();
			ActivateInTransitionPath();
		}

		if (m_TransitionType&UIObject::UI_FOCUSED)
		{
			FocusInTransitionPath();
		}

		m_TransitionState = eTransitionComplete;
		TransitionComplete();
	}
}

void UINavigator::TransitionIn()
{
	//poll for enter transition completion
	UITransition* in =  verify_cast<UIObject*>(m_TempTransitionPath.m_Enter.Top())->GetInTransition();
	if (in && in->IsComplete())
	{
		in->Exit();

		m_TempTransitionPath.m_Enter.Pop();

		if (m_TempTransitionPath.m_Enter.GetCount()>0 && verify_cast<UIObject*>(m_TempTransitionPath.m_Enter.Top())->GetInTransition())
		{
			UITransition* in =  verify_cast<UIObject*>(m_TempTransitionPath.m_Enter.Top())->GetInTransition();

			//start the next enter transition
			in->Enter();
		}
		else
		{
			m_TransitionState = eTransitionComplete;
		}
	}
}

bool UINavigator::UnfocusOutTransitionPath()
{		
	UIObject*	pCurrent	= m_pCurrentState;
	UIObject*	nextState	= pCurrent;

	m_CurrentFocusTransitionPath.Reset();
	pCurrent->GetPath(m_pTargetState, m_CurrentFocusTransitionPath);
	m_TempTransitionPath.Reset();
	pCurrent->GetPath(m_pTargetState, m_TempTransitionPath);

	if(m_CurrentFocusTransitionPath.m_Exit.GetCount() == 0 || !AssertVerify( pCurrent==m_CurrentFocusTransitionPath.m_Exit.Pop()) )
		return false;

	while (m_CurrentFocusTransitionPath.m_Exit.GetCount())
	{
		nextState = m_CurrentFocusTransitionPath.m_Exit.Pop();
		pCurrent->SetFocused(false);
		pCurrent = nextState;
	}
	return true;
}

bool UINavigator::FocusInTransitionPath()
{
	UIObject* nextState	= m_pTargetState; // just want to set it to something

	while (1)
	{
		// try to follow the preplanned path
		if (m_CurrentFocusTransitionPath.m_Enter.GetCount())
			nextState = m_CurrentFocusTransitionPath.m_Enter.Pop();
		else
			break;

		nextState->SetFocused(true);
	}

	return true;
}

bool UINavigator::DeactivateOutTransitionPath()
{		
	UIObject*	pCurrent	= m_pCurrentState;
	UIObject*	nextState	= m_pTargetState; // just want to set it to something

	m_CurrentActivateTransitionPath.Reset();
	pCurrent->GetPath(m_pTargetState, m_CurrentActivateTransitionPath);

	if(!AssertVerify( pCurrent==m_CurrentActivateTransitionPath.m_Exit.Pop()) )
		return false;

	while (m_CurrentActivateTransitionPath.m_Exit.GetCount())
	{
		nextState = m_CurrentActivateTransitionPath.m_Exit.Pop();

		pCurrent->Deactivate();		

		pCurrent = nextState;
	}
	return true;
}

bool UINavigator::ActivateInTransitionPath()
{
	UIObject*	nextState = m_pTargetState; // just want to set it to something

	while (1)
	{
		// try to follow the preplanned path
		if (m_CurrentActivateTransitionPath.m_Enter.GetCount())
			nextState = m_CurrentActivateTransitionPath.m_Enter.Pop();
		else
			break;

		nextState->Activate();		
	}

	return true;
}

void UINavigator::OnTransitionComplete()
{		
	//for now lets just call for the target state (so he can setup children accordingly)
	m_pTargetState->OnTransitionComplete();		
	return;
}

bool UINavigator::HideOutTransitionPath()
{		
	UIObject*	pCurrent	= m_pCurrentState;
	UIObject*	nextState	= m_pTargetState; // just want to set it to something

	m_CurrentVisibleTransitionPath.Reset();
	pCurrent->GetPath(m_pTargetState, m_CurrentVisibleTransitionPath);

	if(!AssertVerify( pCurrent==m_CurrentVisibleTransitionPath.m_Exit.Pop()) )
		return false;

	while (m_CurrentVisibleTransitionPath.m_Exit.GetCount())
	{
		nextState = m_CurrentVisibleTransitionPath.m_Exit.Pop();

		pCurrent->Hide();		

		pCurrent = nextState;
	}
	return true;
}

bool UINavigator::ShowInTransitionPath()
{
	UIObject*	nextState = m_pTargetState; // just want to set it to something

	while (1)
	{
		// try to follow the preplanned path
		if (m_CurrentVisibleTransitionPath.m_Enter.GetCount())
			nextState = m_CurrentVisibleTransitionPath.m_Enter.Pop();
		else
			break;

		nextState->Show();		
	}

	return true;
}
void UINavigator::UpdateTransitions(f32)
{
	switch(m_TransitionState)
	{
	case eTransitionNone:									break;
	case eTransitionOutStart:	TransitionOutStart();		break;
	case eTransitionOut:		TransitionOut();			break;
	case eTransitionInStart:	TransitionInStart();		break;
	case eTransitionIn:			TransitionIn();				break;
	case eTransitionComplete:	TransitionComplete();		break;
	}

	//UITransitions
	for(int i=0;i<mCurrentTransitions.size();i++)
	{
		if (mCurrentTransitions[i]->IsComplete())
		{
			mCurrentTransitions[i]->Exit();

			//remove the completed transition from the list
			mCurrentTransitions.Delete(i);
		}
	}
}

void UINavigator::TerminateTransition(UITransition* pTransition)
{
	u32 size = mCurrentTransitions.GetCount();

	for(u32 i=0;i<size;i++)
	{
		if ( pTransition == mCurrentTransitions[i])
		{
			mCurrentTransitions[i]->Exit();
			return;
		}
	}
}

void UINavigator::TerminateAllTransitions()
{
	u32 size = mCurrentTransitions.GetCount();

	for(u32 i=0;i<size;i++)
	{
		mCurrentTransitions[i]->Exit();
	}

	mCurrentTransitions.clear();
}

void UINavigator::GetTransition(const char* pName, Matrix34* pStartPosition, Matrix34* pFinalPosition, Matrix34* pTarget)
{
	UITransition* pTransition = verify_cast<UITransition*>(UIFACTORY->GetUIObject(pName));
	pTransition->SetStartMatrix(pStartPosition);
	pTransition->SetFinalMatrix(pFinalPosition);
	pTransition->SetTarget(pTarget);
}

void UINavigator::PushTransition(UITransition* pTransition)
{
	if (mCurrentTransitions.Find(pTransition) == -1)
	{
		mCurrentTransitions.PushAndGrow(pTransition);      
	}   

	pTransition->Enter();
}

void UINavigator::PopTransition(UITransition* pTransition)
{
	int i=  mCurrentTransitions.Find(pTransition);
	if (i != -1)
	{
		mCurrentTransitions[i]->Exit();

		//remove the completed transition from the list
		mCurrentTransitions.Delete(i);		
	}   
	else
	{
		UIDebugf1("Tried to pop transition which is not on stack");
	}
}

void UINavigator::TransitionComplete()
{
	if (m_TransitionType&UIObject::UI_ACTIVE)
	{
		DeactivateOutTransitionPath();
		ActivateInTransitionPath();
	}

	if (m_TransitionType&UIObject::UI_FOCUSED)
	{
		FocusInTransitionPath();
	}

	//finally hide/show components
	if (m_TransitionType&UIObject::UI_VISIBLE)
	{
		HideOutTransitionPath();
		ShowInTransitionPath();
	}	

	OnTransitionComplete();

	m_bIsTransitioning = false;
	m_TransitionState = eTransitionNone;

	UIDebugf1("UI Transition complete");

	//process next transition
	if (m_TransitionQueue.GetCount())
	{
		RequestTransition(m_TransitionQueue.Pop(),(rage::UIObject::UIObjectState)m_TransitionTypeQueue.Pop());
	}

	//re-fire queued events that were queued due to this UIObject being non-interruptible
	UIMANAGER->ProcessInterruptingQueue();		
}

bool UINavigator::IsTransitioning()
{
	return m_bIsTransitioning;
}

void UINavigator::PostLoad()
{
	//m_pTargetState = UIFACTORY->GetUIObject("UserInterface");
}

void UINavigator::GotoUIObject( UIObject* v )
{
	Assert(v);
	if (v)
	{
		UIDebugf1("Going to %s", v->GetName());
		v->Goto();
	}
}

void UINavigator::EnterUIObject( UIObject* v )
{
	Assert(v);
	if (v)
	{
		v->Enter();
	}
}

void UINavigator::ExitUIObject( UIObject* v )
{
	Assert(v);
	if (v)
	{
		v->Exit();
	}
}

void UINavigator::ShowUIObject( UIObject* v )
{
	Assert(v);
	if (v)
	{
		v->Show();
	}
}

void UINavigator::RefreshUIObject( UIObject* v )
{
	Assert(v);
	if (v)
	{
		v->Refresh();
	}
}

void UINavigator::HideUIObject( UIObject* v )
{
	Assert(v);
	if (v)
	{
		v->Hide();
	}
}

void UINavigator::ActivateUIObject( UIObject* v )
{
	Assert(v);
	if (v)
	{
		v->Activate();
	}
}

void UINavigator::DeactivateUIObject( UIObject* v )
{
	Assert(v);
	if (v)
	{
		v->Deactivate();
	}
}

void UINavigator::FocusUIObject( UIObject* v )
{
	Assert(v);
	if (v)
	{
		v->Focus();
	}
}

void UINavigator::UnfocusUIObject( UIObject* v )
{
	Assert(v);
	if (v)
	{
		v->Unfocus();
	}
}

UIObject* UINavigator::StackPush( UIObject* v )
{
	//avoid pushing the same UIObject twice
	for (int i=0; i!=m_UIObjectStackHeight; i++)
	{
		if (m_UIObjectStack[i]==v){	//a match in the stack?
			m_UIObjectStackHeight = i+1;
			return m_UIObjectStack[i];} //just pop down to it
	}

	//push
	UIDebugf1("Pushing onto stack %s", v->GetName());
	m_UIObjectStack[m_UIObjectStackHeight++] = v;
	v->OnStackPush();

	return v;
}

UIObject* UINavigator::StackPop(/*int howMany*/)
{
	//if (!howMany) howMany=1;//hack cause initializations don't take with datcallback i guess

	if (m_UIObjectStackHeight>1)
	{
		UIObject* pUIObject = m_UIObjectStack[m_UIObjectStackHeight-1];

		UIDebugf1("Popping %s off stack", pUIObject->GetName());

		pUIObject->OnStackPop();

		m_UIObjectStackHeight--;
	}
	else
	{
		Assertf(0,"You are tryint to pop the last thing off the stack!");
		return m_UIObjectStack[0];
	}

	RequestTransition(m_UIObjectStack[m_UIObjectStackHeight-1]);

	return m_UIObjectStack[m_UIObjectStackHeight-1];
}

void UINavigator::ClearStack()
{
	UIDebugf1("Clearing the UI stack");

	for (int i=m_UIObjectStackHeight-1; i>=0 ; i--)
	{
		UIObject* pUIObject = m_UIObjectStack[m_UIObjectStackHeight-1];

		UIDebugf1("Popping %s off stack", pUIObject->GetName());

		pUIObject->OnStackPop();

		m_UIObjectStackHeight--;
	}
}

void UINavigator::RegisterEventListener(UIObject* pObject)
{
	m_UIExternalEventListeners.Push(pObject);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIFactory
///////////////////////////////////////////////////////////////////////////////////////////////////////////

UIFactory::UIFactory() 
: m_pUserInterface(NULL)
{
}

UIFactory::~UIFactory()
{
	//delete all UIObjects
	atMap< atString, UIObject*, atMapCaseInsensitiveHashFn, atMapCaseInsensitiveEquals >::Iterator iter = m_UIObjects.CreateIterator();
	for (iter.Start(); !iter.AtEnd(); iter.Next())
	{
		delete iter.GetData();
	}

	m_UIObjects.Kill();
	m_StringIds.Kill();
}

void UIFactory::RegisterAllUIScriptCommands()
{
	//fuiMovie::RegisterScriptCommands();
	UIMANAGER->RegisterScriptCommands();
}

#define REGISTER_UIOBJECT(T)					UIFACTORY->RegisterUIObject(#T, CreateObject<T>)

void UIFactory::RegisterObjects() 
{
	// set up classes and systems specific to the flashuicomponents libraries and used by the fuicore systems
	UIFACTORY->RegisterUIObject("", CreateObject<UIObject>);

	//generic components/controls
	REGISTER_UIOBJECT(UIObject);
	REGISTER_UIOBJECT(UIComponent);
	REGISTER_UIOBJECT(UIList); 
	REGISTER_UIOBJECT(UISubList); 
	REGISTER_UIOBJECT(UITabbedContainer);
	REGISTER_UIOBJECT(UITab);
	REGISTER_UIOBJECT(UIScene);
	REGISTER_UIOBJECT(UIMessageBox);
	REGISTER_UIOBJECT(UILabel);
	REGISTER_UIOBJECT(UIButton);
	REGISTER_UIOBJECT(UILayer);
	REGISTER_UIOBJECT(UIIcon);
	REGISTER_UIOBJECT(UISpinner);		
	REGISTER_UIOBJECT(UIPromptStrip);
	REGISTER_UIOBJECT(UITicker);
	REGISTER_UIOBJECT(UIProgressBar);		
	REGISTER_UIOBJECT(UIPanel);

	//animating transitions
	REGISTER_UIOBJECT(UITransition_Translate_Axis);
	REGISTER_UIOBJECT(UITransition_Translate_Spline);
	REGISTER_UIOBJECT(UITransition_Parallel);
	REGISTER_UIOBJECT(UITransition_Serial);
	REGISTER_UIOBJECT(UITransition_Rotate);
	REGISTER_UIOBJECT(UITransition_Lerp);
	REGISTER_UIOBJECT(UITransition_LookAtTarget);	
	REGISTER_UIOBJECT(UITransition_Value);
	REGISTER_UIOBJECT(UITransition_Bounded);		
	REGISTER_UIOBJECT(UITransition_FarAwayTarget);		
	REGISTER_UIOBJECT(UITransition_Zoom);				
	REGISTER_UIOBJECT(UITransition_Lean);
	REGISTER_UIOBJECT(UITransition);
	REGISTER_UIOBJECT(UITransition_Translate_Spline_ExitSharpAngle);
	REGISTER_UIOBJECT(UITransition_StreamFlashMovie);

	UIFACTORY->RegisterAllUIScriptCommands();
}
UIObject* UIFactory::CreateNewChild(const char *szName , const char *szClass)
{
	UIDebugf2("Allocating UIObject %s of type %s", szName,szClass);
	UIObject* pUIObject;

	if (!szClass || !szClass[0])
	{
		//get the UIObject type from the lookup table
		atString *pClass = m_UIObjectTypes.Access(szName);
		if (pClass)
		{
			szClass = pClass->c_str();
		}
	}

	if (!szClass || !szClass[0])
	{
		pUIObject = NewUIObject("");
		Assertf(pUIObject, "Failed to create new classless UIObject: %s", szName);
	}
	else
	{
		pUIObject = NewUIObject(szClass);
		Assertf(pUIObject, "Failed to create new UIObject: %s [class %s]", szName, szClass);
	}

	if (pUIObject)
	{
		pUIObject->SetName(szName);

		Assertf( false==m_UIObjects.Access(atString(szName)), "Hash overlap in UIObject Names [%s tried]", szName);

		m_UIObjects.Insert(atString(szName),pUIObject);
	}

	return pUIObject;
}

UIObject* UIFactory::NewUIObject(const char* UIObjectType)
{
	UIObject* pObject = CreateUIObject(UIObjectType);

	if (!pObject)
		return 0;

	m_UIObjectCount++;

	return pObject;
}

void UICommand::ProcessCommand(UIObject* pUIObject) const
{
#if __BANK

	char	szArgBuf[256]	= "";
	char*	pos				= szArgBuf;

	for 
		(
		UIVariantLL* pArg=m_pFirstArg; 
	pArg!=NULL; 
	pArg = pArg->m_pNext
		)
	{
		//add comma
		if (pos>szArgBuf)
		{
			pos[0]=',';
			pos++;
		}
		pos+= pArg->PrintValue(pos);

	}
	pos[0]='\0';

	UIDebugf1("\t%s: Processing command, %s(%s)",pUIObject->GetName(),GetName(),szArgBuf);

#endif

	m_pUIObject->HandleCommand( m_pName, this , pUIObject);
}

//returns number of chars printed or -1 on error
int UIVariant::PrintValue(char *szBuf) const
{
	switch(m_Type)
	{
	case UIVarTypeInt:		return sprintf(szBuf, "%d", 	m_Int);
	case UIVarTypeFloat:	return sprintf(szBuf, "%f", 	m_Float);	
	case UIVarTypeStringKey:
	case UIVarTypeStringValue:return sprintf(szBuf, "%s", 	m_String);		
	case UIVarTypeEvent:	return sprintf(szBuf, "todo with fuiEvent");
	case UIVarTypeBool:		return sprintf(szBuf, "%s", 	m_Bool ? "true" : "false" );
	case UIVarTypeUIObjectPtr:return sprintf(szBuf, "%s", 	m_UIObjectPointer->GetName() );		
	case UIVarTypeVoid:		AssertMsg(0,"void UIVariant"); return 0;
	}

	UIErrorf("Bad UIVariant type");
	return -1;
}

bool UIVariant::LoadFromString(char *szText, const UICommand& arg)
{
	if (szText[0]=='\'')													//a string?
	{
		char *pTemp	= strchr(szText+1, '\'');
		pTemp[0]	= 0;

		if (szText[1] == '%')
			SetToValue( UIFACTORY->CreateString(szText+1+1) );
		else
			Set( UIFACTORY->CreateString(szText+1) );

		if (!AssertVerify(m_String))
			return false;
	}
	else if (sscanf(szText, "arg%d", &m_Int) == 1)							//a style sheet argument?
	{
		const UIVariant* pVar = arg.GetArg(m_Int);
		if (Verifyf(pVar , "this style sheet needs an arg[m_Int]"))
			Set(*pVar);
		else
			return false;

	}
	else if (strchr(szText, '.') && sscanf(szText, "%f", &m_Float) == 1)	//a float?
	{
		m_Type		= UIVariant::UIVarTypeFloat;
	}
	//else if (strchr(szText, '.') && m_Event.LoadFromString(szText))	//an event?
	//{
	//	m_Type		= UIVariant::UIVarTypeEvent;
	//}
	else if (sscanf(szText, "%d", &m_Int) == 1)								//an integer?
	{
		m_Type		= UIVariant::UIVarTypeInt;
	}
	else if ( !stricmp(szText, "true" ) )
	{
		Set(true);
	}
	else if ( !stricmp(szText, "false" ) )
	{
		Set(false);
	}
	else																	//a UIObject id
	{
		m_UIObjectPointer = UIFACTORY->GetUIObject(szText);

		if (!m_UIObjectPointer)
		{
			Assertf(0, "I dont know what \"%s\" is", szText);
			return false;
		}

		m_Type	= UIVariant::UIVarTypeUIObjectPtr;
	}

	return true;
}

UICommand* UIEventHandler::NewCommand()
{
	UICommand* pLastCommand	= m_pTargetCommand;
	UICommand* pNewCommand	= rage_new UICommand(); 

#if __DEV
	s_NumCommands++;
	s_CommandBytes +=sizeof(UICommand);
#endif

	UIDebugf2("Allocating %d bytes for command",sizeof(UICommand));

	if (AssertVerify(pNewCommand))
	{
		if(pLastCommand)
		{
			while (pLastCommand->m_pNext)
			{
				pLastCommand = pLastCommand->m_pNext;
			}

			pLastCommand->m_pNext = pNewCommand;
		}
		else
		{
			m_pTargetCommand = pNewCommand;
		}

		pNewCommand->m_pNext = NULL;
	}

	return pNewCommand;
}

bool UIEventHandler::LoadFromXml(xmlAsciiTokenizerXml& _xmlTokenizer, const UICommand& arg)
{
	xmlTokenizerHack& tokenizer = *((xmlTokenizerHack*)&_xmlTokenizer);

	if (!AssertVerify(tokenizer.CheckTagName("transition")))
		return false;

	clear_attr_values();
	tokenizer.GetTag("transition", a_szTransitionAttrName, MAX_XML_ATTR_LEN, a_szAttrValue);
	
	m_Trigger = FUIEVENTMGR->GetEvent(a_szAttrValue[0],true);

	//do we want to consume the event?
	if (a_szAttrValue[2][0] == 'f')
	{
		m_bConsumeEvent = false;
	}
	else
	{
		m_bConsumeEvent = true;
	}

	//you cant have action tags if the transition has a target attribute
	if(a_szAttrValue[1][0])
	{
		NewCommand()->LoadFromString(a_szAttrValue[1], arg);
	}
	else
	{
		while(!tokenizer.CheckEndTag("transition",false))
		{
			if(tokenizer.CheckTagName("action"))
			{
				clear_attr_values();
				tokenizer.GetTag("action", a_szActionAttrName, MAX_XML_ATTR_LEN, a_szAttrValue);

				NewCommand()->LoadFromString(a_szAttrValue[0], arg);

				if(!AssertVerify(tokenizer.CheckEndTag("action",true)))
					return false;
			}
			else
			{
				AssertMsg(false , "unrecognised tag in transition scope, might not be supported yet");
			}		
		}
	}

	return true;
}

UICommand::~UICommand()
{
	if(m_pFirstArg)
	{
		delete m_pFirstArg;
		m_pFirstArg = NULL;
	}

	if(m_pNext)
	{
		delete m_pNext;
		m_pNext = NULL;
	}
}

bool UICommand::LoadFromString(char* szCmd, const UICommand& arg)
{
	char* 			cmd 	= NULL;
	char* 			pos		= NULL;
	char			szText	[256];

	strcpy(szText, szCmd);
	pos  = strchr(szText, '(');

	if(!pos)
	{
		//default to goto
		sprintf(szText,"Goto(%s)",szCmd);
		pos = &szText[4];
	}

	pos[0]=0;
	pos++;//pos now points to start of args

	//find the object
	cmd = strchr(szText, '.');
	if(cmd)
	{
		cmd[0]=0;
		cmd++; //cmd now points to start of function
		m_pUIObject = UIFACTORY->GetUIObject(szText);
	}
	else
	{
		//default to using UIMANAGER
		cmd = szText;
		m_pUIObject = UIMANAGER;
	}

	Assert(cmd);

	SetName(UIFACTORY->CreateString(cmd));

	if (!m_pUIObject)
	{
		//UIError("Trying to create a command callback, but UIObject, \"%s\" not found! ", szText);
		//instantiate the new object.  it will get setup on the 2nd pass
		m_pUIObject = UIFACTORY->CreateNewChild(szText,"");
		return false;
	}

	if(!AssertVerify(LoadArgsFromString(pos, ')', arg)) )
		return false;

	return true;
}

const UIVariantLL*	UICommand::GetArg(int i) const 
{
	UIVariantLL* pArg;

	Assert(i>=0);

	for (pArg = m_pFirstArg; pArg!=NULL && i!=0; pArg = pArg->m_pNext)
	{
		i--;
	}

	return pArg;
}

UIVariantLL*	UICommand::NewArg()
{
	UIVariantLL* pLastArg	= m_pFirstArg;
	UIVariantLL* pNewArg	= rage_new UIVariantLL(); 

#if __DEV
	s_NumDataTags++;
	s_DataBytes += sizeof(UIVariantLL);
#endif

	UIDebugf2("Allocating %d bytes for command argument",sizeof(UIVariantLL));

	if (AssertVerify(pNewArg))
	{
		//attach to linked list
		if(pLastArg)
		{
			while (pLastArg->m_pNext)
			{
				pLastArg = pLastArg->m_pNext;
			}

			pLastArg->m_pNext	= pNewArg;
		}
		else
		{
			m_pFirstArg	= pNewArg;
		}

		pNewArg->m_pNext = NULL;
	}

	m_NumArgs++;
	return pNewArg;
}

bool UICommand::LoadArgsFromString(char *szArgs, char cTerminator, const UICommand& args)
{
	char*			arg		= NULL;
	char*			pos		= szArgs;
	bool			done	= false;

	while (pos[0] && !done)
	{
		arg = pos;

		//find delimiter
		pos = strchr(arg, ',');
		if (pos==NULL)
		{
			pos = strchr( arg, cTerminator );
			done = pos!=NULL;
		}
		if(!Verifyf(pos , "neither delimiter nor terminator found"))
			return false;			

		//enclose variant
		pos[0] = 0;

		//read!
		if(arg[0])
		{
			UIVariantLL* pArg = NewArg();

			if (AssertVerify(pArg))
			{
				pArg->LoadFromString(arg, args);
			}			
		}

		//advance position
		pos++;
	}

	return true;
}

#if __DEV
void UIFactory::PrintMemoryUsage()
{
	UIDebugf2("%d UIObjects allocated with combined data size of %d bytes",m_UIObjectCount,m_UIObjectBytes);
	UIDebugf2("%d event handlers allocated with combined data size of %d bytes",s_NumHandlers,s_HandlerBytes);
	UIDebugf2("%d commands allocated with combined data size of %d bytes",s_NumCommands,s_CommandBytes);
	UIDebugf2("%d strings allocated with combined data size of %d bytes",s_NumStrings,s_StringBytes);
	UIDebugf2("%d data args allocated with combined data size of %d bytes",s_NumDataTags,s_DataBytes);
	UIDebugf2("Total FUI data mem size = %d",m_UIObjectBytes + s_HandlerBytes + s_CommandBytes + s_StringBytes + s_DataBytes);
}
#endif

void UIFactory::PostLoad()
{
//	BEGIN_UI_ALLOCATOR(this);

	Assert(m_pUserInterface);

	//call post load on all UIObjects (starting from root UIObject and propogate through all child UIObjects
	m_pUserInterface->PostLoad();		

	//ok lets begin
	m_pUserInterface->Activate();
	m_pUserInterface->Show();
	m_pUserInterface->SetFocused(true);

//	END_UI_ALLOCATOR();

#if __DEV
	PrintMemoryUsage();
#endif
}

bool UIFactory::LoadFromXml(const char *sz_Name)
{
	Assert(m_UIObjectCount==0);

	bool ok = false;

	fiStream* pXmlStream = ASSET.Open(sz_Name, "xml");

	UICommand arg;

	//first pass
	if(pXmlStream)
	{
		ok = true;
		xmlAsciiTokenizerXml xmlTokenizer;
		xmlTokenizer.Init("UITest", pXmlStream);

		if (!xmlTokenizer.CheckTag("scxml"))
		{
			// read (and ignore) schema if it is there
			char namespaceXsi[MAX_XML_ATTR_LEN];
			char namespaceSchema[MAX_XML_ATTR_LEN];
			const char * scxmlAttrNames[] = { "xmlns:xsi", "xsi:noNamespaceSchemaLocation", ""};
			char *scxmlAttrVals[] = {namespaceXsi, namespaceSchema};
			xmlTokenizer.GetTag("scxml", scxmlAttrNames, MAX_XML_ATTR_LEN, scxmlAttrVals, false); //empty
		}

		while (!xmlTokenizer.CheckEndTag("scxml",false))
		{
			UIObject* pNewUIObject = NULL;

			clear_attr_values();
			xmlTokenizer.GetTag("uiobject", a_szUIObjectAttrName, MAX_XML_ATTR_LEN, a_szAttrValue);

			pNewUIObject = CreateNewChild(a_szAttrValue[0], a_szAttrValue[3]);

			ok &= pNewUIObject!=NULL;

			if (pNewUIObject)
			{
				ok &= LoadFromXml("uiobject",pNewUIObject,xmlTokenizer, arg, UIReadMode_ClassOnly);
			}
			else
			{
				ok = false;				
			}

			if (!ok)
			{
				UIMANAGER->AssertAndDisableFUI("UI could not be loaded.  Your content/ui/*.xml files may be out of date");
				return false;
			}

		}

		AssertVerify(xmlTokenizer.CheckEndTag("scxml"));

		pXmlStream->Close();
	}

	//post ctor init pass
	m_pUserInterface = UIFACTORY->GetUIObject("UserInterface");
	if (!m_pUserInterface)
	{
		UIMANAGER->AssertAndDisableFUI("The content/ui xmls could not be found");
		return false;
	}
	m_pUserInterface->Init();

	//2nd pass
	pXmlStream = ASSET.Open(sz_Name, "xml");

	if(pXmlStream)
	{
		ok = true;
		xmlAsciiTokenizerXml xmlTokenizer;
		xmlTokenizer.Init("UITest", pXmlStream);

		if (!xmlTokenizer.CheckTag("scxml"))
		{
			// read (and ignore) schema if it is there
			char namespaceXsi[MAX_XML_ATTR_LEN];
			char namespaceSchema[MAX_XML_ATTR_LEN];
			const char * scxmlAttrNames[] = { "xmlns:xsi", "xsi:noNamespaceSchemaLocation", ""};
			char *scxmlAttrVals[] = {namespaceXsi, namespaceSchema};
			xmlTokenizer.GetTag("scxml", scxmlAttrNames, MAX_XML_ATTR_LEN, scxmlAttrVals, false); //empty
		}

		while (!xmlTokenizer.CheckEndTag("scxml",false))
		{
			clear_attr_values();
			xmlTokenizer.GetTag("uiobject", a_szUIObjectAttrName, MAX_XML_ATTR_LEN, a_szAttrValue);

			UIObject* pObject = GetUIObjectByIdName(a_szAttrValue[0]);
			ok &= LoadFromXml("uiobject",pObject,xmlTokenizer, arg, UIReadMode_All);
		}

		AssertVerify(xmlTokenizer.CheckEndTag("scxml"));

		pXmlStream->Close();
	}

	UIDebugf1("Done parsing xml UIObject files. Created %d UIObjects",m_UIObjectCount);

	return ok;
}

void UIPlayer::GetPlayerMatrix( s32 /*nPlayerIdx*/, Matrix34* /*pMatrix*/)
{
}

UICamera::UICamera() :
m_PreUIFOV(0.0f),
m_PreUINear(0.0f),
m_PreUIFar(0.0f),
m_PreUIOrtho(0.0f),
m_PostUIFOV(0.0f),
m_PostUINear(0.0f),
m_PostUIFar(0.0f),
m_PostUIOrtho(0.0f),
m_bIsCameraPaused(false),
m_bIsRequestingCameraPause(false),
m_FramesToPauseCamera(0)
{

	m_PreUICameraMatrix.Identity();
	m_PreUICameraVelocity.Zero();
	m_PostUICameraMatrix.Identity();
	m_PostUICameraVelocity.Zero();
}

//
// UI is trying to control the camera (e.g. pause menu, Nav System, garage)
// Use blend factor provided by Flash movie to lerp between
// game camera and Flash movie's camera.  Make sure lerp factor is [0..1].
// If the lerp factor is 1, then just use the dest matrix and save CPU by not blending.
// Likewise, if the lerp factor is 0, then just use the source matrix.
// If we are in lookback camera, then don't blend at all since it causes some weird
// artifacts (I think due to the PrepareSlerp not working well with jittery source matrix).
// Also, don't do any camera blending during fly cam.
//
// FeedCoordinates can happen last because the coordinate frames are only used during the Draw pass.
//
void UICamera::OnSetCamera(Matrix34& camMtx, float& fFov, float& fNear, float& fFar, float& fOrthoHeight)
{
	fOrthoHeight = -1.0f;

	SetPreUICameraMatrix(&camMtx);
	SetPreUIFOV(fFov);
	SetPreUINearPlane(fNear);
	SetPreUIFarPlane(fFar);
	SetPreUIOrthoHeight(fOrthoHeight);

	if (m_bIsCameraPaused)
	{
		camMtx = m_PausedCameraMatrix;
		fFov = m_PausedFOV;
		fNear = m_PausedNear;
		fFar = m_PausedFar;
		fOrthoHeight = m_PausedOrtho;
		if (m_FramesToPauseCamera == 0)
		{
			m_bIsCameraPaused = false;
		}
		else if (m_FramesToPauseCamera > 0)
		{
			--m_FramesToPauseCamera;
		}
	}

	SetPostUICameraMatrix(&camMtx);
	SetPostUIFOV(fFov);
	SetPostUINearPlane(fNear);
	SetPostUIFarPlane(fFar);
	SetPostUIOrthoHeight(fOrthoHeight);

	//see if we want to pause here
	if (m_bIsRequestingCameraPause)
	{
		PauseCamera(-1);
	}
}

void UICamera::UnPauseCamera()
{
	m_bIsCameraPaused = false;
}
void UICamera::PauseCamera(s32 numFrames)
{
	m_FramesToPauseCamera = numFrames;	
	m_bIsCameraPaused = true;
	m_bIsRequestingCameraPause = false;
	m_PausedCameraMatrix.Set(*GetPreUICameraMatrix());
	m_PausedFOV = GetPreUIFOV();
	m_PausedNear = GetPreUINearPlane();
	m_PausedFar = GetPreUIFarPlane();
	m_PausedOrtho = GetPreUIOrthoHeight();
}

void UICamera::PauseCamera(const rage::Matrix34* m,rage::f32 fov)
{
	m_FramesToPauseCamera = -1;	
	m_bIsCameraPaused = true;
	m_bIsRequestingCameraPause = false;
	m_PausedCameraMatrix.Set(*m);
	m_PausedFOV = fov;
	m_PausedNear = GetPreUINearPlane();
	m_PausedFar = GetPreUIFarPlane();
	m_PausedOrtho = GetPreUIOrthoHeight();
}

void Enter(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	if (pObject)
	{
		pObject->Enter();	
	}
}

void Exit(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	if (pObject)
	{
		pObject->Exit();	
	}
}

void Activate(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	if (pObject)
	{
		pObject->Activate();	
	}
}

void Deactivate(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	if (pObject)
	{
		pObject->Deactivate();	
	}
}

void Focus(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	if (pObject)
	{
		pObject->Focus();	
	}
}

void Unfocus(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	if (pObject)
	{
		pObject->Unfocus();	
	}
}

void Enable(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	if (pObject)
	{
		pObject->Enable();	
	}
}

void Disable(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	if (pObject)
	{
		pObject->Disable();	
	}
}

void Show(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	if (pObject)
	{
		pObject->Show();	
	}
}

void Hide(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	if (pObject)
	{
		pObject->Hide();	
	}
}

bool IsActive(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	if (pObject)
	{
		return pObject->IsActive();	
	}
	return false;
}

bool IsFocused(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	if (pObject)
	{
		return pObject->IsFocused();	
	}
	return false;
}

void SendEvent(const char* eventName)
{
	UIMANAGER->SendEvent(eventName);
}

void Pause()
{
	UIMANAGER->Pause();
}

void Resume()
{
	UIMANAGER->Resume();
}

void Reboot(const char* type)
{
	UIMANAGER->Reboot(type);
}

void Refresh(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	if (pObject)
	{
		pObject->Refresh();	
	}
}

void StackPush(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	UIMANAGER->StackPush(pObject);
}

void StackPop()
{
	UIMANAGER->StackPop();
}

void Goto(const char* objectName)
{
	UIObject* pObject = UIFACTORY->GetUIObject(objectName);
	if (pObject)
	{
		pObject->Goto();	
	}
}

void UISetPrompt(int /*iPromptSet*/)
{
	//mcUILogic::GetInstance()->SetPrompt(iPromptSet);
}

bool UIIsShowingDialog()
{
	/*if (FLASHNAVIGATOR)
	return (FLASHNAVIGATOR->IsShowingDialog() && !FLASHNAVIGATOR->IsShowingBubble());
	else*/
	return false;
}

void UITransitionTo(const char* id)
{
	UIObject* o = verify_cast<UIObject*>(UIMANAGER->GetUIObject(id));
	if (o)
	{
		o->Goto();
	}
}

int UIGetRebootReason( int& /*rebootPad*/ /* controller number or -1 */ )
{
	/*if( UIMANAGER )
	{
	rebootPad = (int)UIMANAGER->m_RebootData.m_iRebootPad;
	return (int)UIMANAGER->m_RebootData.m_eType;
	}*/
	return 0;
}

void UIFireButtonPressedEvent(int /*key*/)
{
	/*if (FLASHNAVIGATOR)
	FLASHNAVIGATOR->GetInputObject().NotifyEvent(key, vhsmExtObject::vhsmEventPriorityLow);*/
}

//void SetString(const char* /*stringID*/)
//{
//	//UIObject* o = verify_cast<UIObject*>(UIMANAGER->GetUIObject(id));
//	//o->Goto();
//}

//void AddEventHandler(const char* id, void(*callback)(rdr2GOH*))
//void AddEventHandler(const char* id, void* callback)
//{
	//UIObject* o = verify_cast<UIObject*>(UIMANAGER->GetUIObject(id));
	//UIEventHandler* newHandler = UIFACTORY->NewEventHandler(o);

	//newHandler->m_Trigger = UIFACTORY->CreateEvent("ALL");
	//newHandler->m_Trigger->m_pEventObject = o;
	//newHandler->NewCommand()->LoadFromString	( a_szAttrValue[0], arg);

	//o->AddEventHandler(callback);
	//UIInfo* pInfo = new UIInfo(NULL,1,)
	//UIMANAGER->RegisterCommand(id,callback);
	//UI_REGISTER(id,callback);
//}

void AddLifecyclefunction()
{

}

void MessageBoxSetTitle(const char* component, const char* title)
{
	UIMessageBox* pBox = verify_cast<UIMessageBox*>(UIMANAGER->GetUIObject(component));
	pBox->SetTitle(title);
}

void MessageBoxSetDescription(const char* component, const char* description)
{
	UIMessageBox* pBox = verify_cast<UIMessageBox*>(UIMANAGER->GetUIObject(component));
	pBox->SetDescription(description);
}

void LabelSetText(const char* component, const char* text)
{
	UILabel* pLabel = verify_cast<UILabel*>(UIMANAGER->GetUIObject(component));
	pLabel->SetText(text);
}

void ButtonSetText(const char* component, const char* text)
{
	UIButton* pLabel = verify_cast<UIButton*>(UIMANAGER->GetUIObject(component));
	pLabel->SetText(text);
}

const char* GetStringCB(const char *id)
{
	return UISTRINGTABLE->GetString(id);
}

void SetupScriptCommands()
{
	//UIObject machine stuff:
	SCR_REGISTER_SECURE(UI_STACKPOP, 0x97a50e69,StackPop);
	SCR_REGISTER_SECURE(UI_STACKPUSH, 0xb1a2028a,StackPush);
	SCR_REGISTER_SECURE(UI_GOTO, 0xa0159c77,Goto);
	//SCR_REGISTER(UI_ENTER,Enter);
	SCR_REGISTER_SECURE(UI_EXIT, 0x2df89c2e,Exit);
	SCR_REGISTER_SECURE(UI_SHOW, 0xd1d1d467,Enter);//just make it easy for scripters show/hide are common verbs
	SCR_REGISTER_SECURE(UI_REFRESH, 0xfac3df71,Refresh);
	SCR_REGISTER_SECURE(UI_HIDE, 0x7508e7f3,Exit);//just make it easy for scripters show/hide are common verbs
	SCR_REGISTER_SECURE(UI_ENABLE, 0xe576dcad,Enable);
	SCR_REGISTER_SECURE(UI_DISABLE, 0xc4532f84,Disable);
	SCR_REGISTER_SECURE(UI_ISACTIVE, 0xb1fdb70d,IsActive);
	SCR_REGISTER_SECURE(UI_ACTIVATE, 0xd11bd55a,Activate);
	SCR_REGISTER_SECURE(UI_DEACTIVATE, 0xca35bb5e,Deactivate);
	SCR_REGISTER_SECURE(UI_ISFOCUSED, 0x6f2509e8,IsFocused);
	SCR_REGISTER_SECURE(UI_FOCUS, 0x699cc85e,Focus);
	SCR_REGISTER_SECURE(UI_UNFOCUS, 0xacea059,Unfocus);
	SCR_REGISTER_SECURE(UI_SENDEVENT, 0xf10a56c5,SendEvent);
	SCR_REGISTER_SECURE(UI_REBOOT, 0x592df51e,Reboot);
	SCR_REGISTER_SECURE(UI_PAUSE, 0xb129b00c,Pause);
	SCR_REGISTER_SECURE(UI_UNPAUSE, 0xf09ff990,Resume);
	//SCR_REGISTER(UI_SET_PROMPT, UISetPrompt);
	//SCR_REGISTER(UI_IS_SHOWING_DIALOG, UIIsShowingDialog);
	//SCR_REGISTER(UI_TRANSITION_TO, UITransitionTo);
	//SCR_REGISTER(UI_GET_REBOOT_REASON, UIGetRebootReason);
	//SCR_REGISTER(UI_FIRE_BUTTON_PRESSED_EVENT, UIFireButtonPressedEvent);
	SCR_REGISTER_SECURE(UI_SET_STRING, 0xe457546c, UIStringTable::SetStringCallback);
	SCR_REGISTER_SECURE(UI_GET_STRING, 0xcccff80b, GetStringCB);
	//SCR_REGISTER(UI_ADD_EVENT_HANDLER, AddEventHandler);
	//SCR_REGISTER(UI_ADD_LIFECYCLEFUNCTION, AddLifecyclefunction);

	//component specific callbacks
	SCR_REGISTER_SECURE(UI_MESSAGEBOX_SET_TITLE, 0xb77b39bb,MessageBoxSetTitle);
	SCR_REGISTER_SECURE(UI_MESSAGEBOX_SET_DESCRIPTION, 0x591339b9,MessageBoxSetDescription);
	SCR_REGISTER_SECURE(UI_LABEL_SET_TEXT, 0xb3fc8cb7,LabelSetText);
	SCR_REGISTER_SECURE(UI_BUTTON_SET_TEXT, 0x3db16a3d,ButtonSetText);

	//UI_REGISTER(UI_SHOW,Enter);
}

void UIManager::RegisterScriptCommands()
{
	//Stringtable stuff
	UIStringTable::RegisterScriptCommands();
	UIStringTableStub::RegisterScriptCommands();

	SetupScriptCommands();
}
