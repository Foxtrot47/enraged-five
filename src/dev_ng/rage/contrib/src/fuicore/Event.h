// 
// fuimisc/Event.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FLASHUIMISC_EVENT_H
#define FLASHUIMISC_EVENT_H

// User includes
#include "bank/bank.h"
#include "data/base.h"
#include "atl/hashsimple.h"
#include "atl/slist.h"
#include "atl/array.h"
#include "fuicore/UIManager.h"
#include "script/thread.h"
#include "fuicore/UICallback.h"

// Event include 
#include "fuicore/EventListener.h"
namespace rage {

class UIObject;
class UICommand;
class UIVariant;
class xmlAsciiTokenizerXml;
struct UIInfo;


// fuiEvent declaration //
class fuiEvent
{
	// Friend class declarations
	friend class fuiEventHashElement;
public:
	fuiEvent();
	fuiEvent( const char* pStrEventIdentifier );
	~fuiEvent();

	//fuiEvent& operator=( fuiEvent& rEvent );

	void SetName( const char* pStrEventName );
	const char* GetName( void ) const { return m_strEventName; }
	void SetData( void* pData){m_pData = pData;}
	void SetPriority( u8 priority ){m_priority = priority;}
	void* GetData(){return m_pData;}
	u8 GetPriority(){return m_priority;}
	u32 GetHash( void )  const { return m_uHashValue; } 
	void SetTargetObject(UIObject* o){m_pTargetObject = o;}
	UIObject* GetTargetObject(){return m_pTargetObject;}
	void SetIsInput(bool b){m_bIsInput = b;}
	bool IsInput(){return m_bIsInput;}
	void SendToCode(bool b){m_bSendToCode = b;}
	bool ShouldSendToCode(){return m_bSendToCode;}
	void SendToScript(bool b){m_bSendToScripts = b;}
	bool ShouldSendToScripts(){return m_bSendToScripts;}
	
	bool LoadFromString(const char *szText);	

protected:
	const char* m_strEventName;
	u32	m_uHashValue;
	u8 m_priority;
	bool m_bIsInput;
	bool m_bSendToScripts;
	bool m_bSendToCode;
	void* m_pData;
	UIObject* m_pEventObject;
	UIObject* m_pTargetObject;
};

// fuiEvent aliases
typedef atSList< fuiEvent* >		fuiEventList;
typedef atSNode< fuiEvent* >		fuiEventListNode;
typedef atArray< fuiEvent* >		fuiEventPtrArray;


// Global event hash element defines
#define DATA_ARRAY_INIT_ELEMENTS	3
#define DATA_ARRAY_GROW_SIZE		3

// fuiEventHashElement declaration //
class fuiEventHashElement
{
	// Friend class declarations
	friend class fuiEventManager;

public:
	fuiEventHashElement()
		:m_nValidDataElements( 0 )
	{
		// Initialize the event data array
		m_aEventData.Resize( DATA_ARRAY_INIT_ELEMENTS );
	}

	fuiEventHashElement( fuiEvent* pEvent );
	fuiEventHashElement( const char* pStrEventIdentifier );

	fuiEvent* GetEvent( void ) { return &m_Event; }
	fuiEventData* GetEventData( s32 nDataIdx ) { Assert( nDataIdx > -1 && nDataIdx < m_aEventData.GetCount() ); return &m_aEventData[ nDataIdx ]; }
	s32 GetNumValidDataElements( void ) const { return m_nValidDataElements; }

	// Generates a hash value to identify this data class
	u32 GenerateHash( void ) const { return m_Event.GetHash(); }

	fuiEventHashElement& operator=( const fuiEventHashElement& rSrcEventElement );
	// Comparison method to satisfy search criteria
	bool IsEqual( fuiEventHashElement* pSrcHashElement ) const;

protected:
	void AddEventData( fuiEventListener* pListener, s32 nPriority );
	bool RemoveEventData( fuiEventListener* pListener );

	void PostEvent();

private:
	fuiEvent				m_Event;
	fuiEventDataArray	m_aEventData;
	s32			m_nValidDataElements;
};

// fuiEventData aliases
typedef SimpleHash< fuiEventHashElement >	fuiEventHashTable;



//==============================================================================================================================
class UIEventHandler
	//==============================================================================================================================
{
public:

	enum { maxCommandsPerEvent	= 4 };

	fuiEvent* m_Trigger;
	UICommand* m_pTargetCommand;
	UIEventHandler*	m_pNext;
	bool m_bConsumeEvent;

	UIEventHandler() :
	m_pNext(NULL),
		m_pTargetCommand(NULL),
		m_bConsumeEvent(true)//always consume event by default
	{
	}

	~UIEventHandler()
	{
		if(m_pNext) 
			delete m_pNext;

		if(m_pTargetCommand) 
			delete m_pTargetCommand;
	}

	UICommand*	NewCommand();
	bool LoadFromXml(xmlAsciiTokenizerXml&, const UICommand& arg);
};


} // namespace rage
#endif // #ifndef FLASHUIMISC_EVENT_H
