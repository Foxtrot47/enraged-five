// 
// fuicore/Common.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FLASHUICORE_COMMON_H
#define FLASHUICORE_COMMON_H

#include "diag/output.h"
#include "diag/channel.h"
#include "system/param.h"
#include "system/ipc.h"

namespace rage {

#define PIX_HI 2
#define PIX_LO 1

#define USEFLASH 1

RAGE_DECLARE_CHANNEL(UI);

#define UIAssertf(cond,fmt,...)			RAGE_ASSERTF(UI,cond,fmt,##__VA_ARGS__)
#define UIVerifyf(cond,fmt,...)			RAGE_VERIFYF(UI,cond,fmt,##__VA_ARGS__)
#define UIFatalf(fmt,...)				RAGE_FATALF(UI,fmt,##__VA_ARGS__)
#define UIErrorf(fmt,...)				RAGE_ERRORF(UI,fmt,##__VA_ARGS__)
#define UIWarningf(fmt,...)				RAGE_WARNINGF(UI,fmt,##__VA_ARGS__)
#define UIDisplayf(fmt,...)				RAGE_DISPLAYF(UI,fmt,##__VA_ARGS__)
#define UIDebugf1(fmt,...)				RAGE_DEBUGF1(UI,fmt,##__VA_ARGS__)
#define UIDebugf2(fmt,...)				RAGE_DEBUGF2(UI,fmt,##__VA_ARGS__)
#define UIDebugf3(fmt,...)				RAGE_DEBUGF3(UI,fmt,##__VA_ARGS__)
#define UILogf(severity,fmt,...)		RAGE_LOGF(UI,fmt,##__VA_ARGS__)
#define UICondLogf(cond,severity,fmt,...)	RAGE_CONDLOGF(cond,UI,severity,fmt,##__VA_ARGS__)

//macros for state registration and instantiation
#define REGISTER_UIBOJECT(T) UIFACTORY->RegisterUIObject(#T, UIFactory::CreateObject<T>)

enum fuiRenderEngine
{
	UI_ENGINE_FLASH,
	UI_ENGINE_RAGE
};

//convenience mutex.  use macros
//UI_LOCK(m_Mutex);
//UI_VERIFY_UNLOCKED(m_Mutex);
struct UIMutex {
	UIMutex() : m_InUse(false),m_LockedByVerify(false),m_bAssertOnFail(false) {m_FuiMutex = sysIpcCreateMutex(); }
	~UIMutex(){sysIpcDeleteMutex(m_FuiMutex);}
	bool m_InUse;
	bool m_LockedByVerify;
	sysIpcMutex m_FuiMutex;
	bool m_bAssertOnFail;//asserts if we attempt to lock a locked mutex

	void SetAssertOnFail(bool b){m_bAssertOnFail = b;}

	__forceinline void VerifyUnlocked()
	{
		if(m_InUse)
		{
			m_LockedByVerify = true;
			sysIpcLockMutex(m_FuiMutex);
//			m_InUse = false;
//#if !__DEV && !__FINAL //Forces a crash in release builds
//			if (m_bAssertOnFail)
//			{
//				int *i = NULL;
//				*i = 0;
//			}
//#endif
//			Assertf(!m_bAssertOnFail,"doing this elsewhere, same time");
//			m_InUse = true;
		}
	}

	__forceinline void UnlockIfLocked()
	{
		if(m_LockedByVerify)
		{
			sysIpcUnlockMutex(m_FuiMutex);
			//			m_InUse = false;
			//#if !__DEV && !__FINAL //Forces a crash in release builds
			//			if (m_bAssertOnFail)
			//			{
			//				int *i = NULL;
			//				*i = 0;
			//			}
			//#endif
			//			Assertf(!m_bAssertOnFail,"doing this elsewhere, same time");
			//			m_InUse = true;
		}
	}

	__forceinline void Lock()
	{
		if(m_InUse)
		{
			m_InUse = false;
#if !__DEV && !__FINAL //Forces a crash in release builds
			if (m_bAssertOnFail)
			{
				int *i = NULL;
				*i = 0;
			}
#endif
			Assertf(!m_bAssertOnFail,"doing this elsewhere, same time");
		}
		m_InUse = true;
		sysIpcLockMutex(m_FuiMutex);
	}

	__forceinline void Unlock()
	{
		if(!m_InUse)
		{
			m_InUse = true;
#if !__DEV && !__FINAL //Forces a crash in release builds
			if (m_bAssertOnFail)
			{
				int *i = NULL;
				*i = 0;
			}
#endif
			Assertf(!m_bAssertOnFail,"doing this elsewhere, same time");
		}
		m_InUse = false;
		sysIpcUnlockMutex(m_FuiMutex);
	}
};

//can just call the macro at beginning of a func.  locks/unlocks in ctor/dtor.  when the func falls out of scope, this mutex will unlock in its destructor
class UIStackLockMutex
{
public:
	UIStackLockMutex(UIMutex* pMutex) : m_Mutex(pMutex){m_Mutex->Lock();}
	~UIStackLockMutex(){m_Mutex->Unlock();}

	UIMutex* m_Mutex;
};

class UIStackVerifyLockMutex
{
public:
	UIStackVerifyLockMutex(UIMutex* pMutex) : m_Mutex(pMutex){m_Mutex->VerifyUnlocked();}
	~UIStackVerifyLockMutex(){m_Mutex->UnlockIfLocked();}

	UIMutex* m_Mutex;
};

#define UI_LOCK(M) UIStackLockMutex _mutex(&M)
#define UI_VERIFY_UNLOCKED(M) UIStackVerifyLockMutex _verifymutex(&M)

} // namespace rage
#endif
