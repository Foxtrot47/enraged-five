// 
// mcUI/UISaveGameData.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SAVE_GAME_DATA_H
#define SAVE_GAME_DATA_H

#include "fuicomponents/Components.h"
#include "fuicore/Event.h"
#include "atl/singleton.h"
#include "file/savegame.h"
#include "string/unicode.h"			// For _TCHAR
#include "rline/rlpresence.h"

class snuGamerLocal;

namespace rage {
class SaveGameData;

#define DATA_COUNT 128 //32 seperate save objects
#define MAX_SAVE_GAMES 32 //needs to be the max games available accross all devices
#define MAIN_DISPLAY_NAME "Red Dead Redemption" //temporary display name.  we may dynamically create one with more info about the save game
#define MAIN_FILE_NAME "rdr2save0.sav"

#define MAX_DISPLAY_NAME_SIZE 128
#define MAX_FILE_NAME_SIZE 42
#define MAX_KEY_SIZE 32

//SaveGameData
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//an element to save a data packet
class SaveGameData : public rage::datBase
{  
public:
	void* mpData;
	rage::u32 mDataSize;//length of the data in bytes. Used for serializing/packing
	char mpKey[MAX_KEY_SIZE];
	rage::u32 mKeySize;//length of the key in bytes. Used for serializing/packing
	rage::u8 mVersion; //can't have more than 256 versions

	SaveGameData();
	virtual ~SaveGameData();

	// PURPOSE:	Serialize data to the given data buffer.
	// PARAMS:	buffer - serialize data to this buffer
	//          size - size of the buffer, used for to assert that we don't have a buffer overrun
	virtual rage::u32 Serialize( rage::u8* buffer, rage::u32 bufferSize);

	// PURPOSE:	Serialize version and data size from the given buffer.
	// PARAMS:	buffer - serialize data to this buffer
	//          size - size of the buffer, used for to assert that we don't have a buffer overrun
	virtual rage::u32 SerializeHeader(unsigned char* pBuffer , unsigned int bufferSize);

	// PURPOSE:	Serialize data to the given data buffer.
	// PARAMS:	buffer - serialize data to this buffer
	//          size - size of the buffer, used for to assert that we don't have a buffer overrun
	virtual rage::u32 SerializeKey(rage::u8* buffer, rage::u32 bufferSize);

	// PURPOSE:	Deserialize data to the given buffer.
	// PARAMS:	buffer - serialize data to this buffer
	//          size - size of the buffer, used for to assert that we don't have a buffer overrun
	virtual rage::u32 Deserialize( rage::u8* buffer, rage::u32 bufferSize);

	// PURPOSE:	Deserialize version and data size from the given buffer.
	// PARAMS:	buffer - serialize data to this buffer
	//          size - size of the buffer, used for to assert that we don't have a buffer overrun
	virtual bool DeserializeHeader( rage::u8* buffer, rage::u32 bufferSize, rage::u32* mainBufferIterator);

	virtual void OnLoadFail(){};
	virtual rage::u32 GetTotalSize();

	virtual void SetKey(char* key);
	virtual void SetDataSize(rage::u32 dataSize);

private:
	bool mOwnsData;//if true, then this object will delete mpData when destructed

};

//SaveGameFile
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//class used to link data to a specific data file.  for example.  Each screen shot can go to it's own file.  Saved replays
//can be saved to individual replay files.  Or, stats can be saved to a combined / cross player stat file.
class SaveGameFile
{  
public:
	char mFilename[MAX_DISPLAY_NAME_SIZE];
	char mDisplayName[MAX_DISPLAY_NAME_SIZE];

	//use an array during development(slow when iterating), but for release we should 
	//have O(1) iteration by using #defines or a structure map
	SaveGameData* mpData[DATA_COUNT];//save data

	rage::u32 m_NumSaveObjects;
	rage::u64 m_Timestamp; //timestamp of the file on disk

	rage::u8* mpBuffer;//buffer that is sent to save profile
	rage::u32 m_BufferSize;
	rage::u32 m_Version;
	atArray<SaveGameData*> mDeletableData;//used to delete objects that we have created

	//flags used in order to synchronize multiple save/loads simultaneously
	bool m_bSaveRequested;
	bool mLoadRequested;

	bool m_bLoaded;//true if this file was loaded before

	SaveGameFile();
	virtual ~SaveGameFile();

	virtual SaveGameData* GetData(const char* key);

	// PURPOSE:	Register data so that when save/loading it will Serialize/Deserialize data to the internal data buffer.  
	// PARAMS:	version - a user defined version #.  This will be tested against the version that is loaded from the save file
	//          key,  a user defined key that will be linked to this data.
	//          pData - data to save. This data must persist while doing any loads/saves including future loads/saves
	//          size - size in bytes of pData
	virtual void RegisterData(rage::u8 version, const char* key , void* pData, rage::u32 size);

	// PURPOSE:	Register data so that when save/loading it will Serialize/Deserialize data to the internal data buffer.  
	// PARAMS:	pData - data to save. This data must persist while doing any loads/saves including future loads/saves
	virtual void RegisterData(SaveGameData* pData);

	// PURPOSE:	Serialize data and save to profile. This function is asynchronous and the save will not be complete on return.
	virtual void SaveData();

	// PURPOSE: Loads data from the save game file. This function is asynchronous and the load will not be complete on return.
	//          Device must be selected and enumerated before this call!
	virtual void LoadData();   

	// PURPOSE:	Deserialize data from the save game file into the internal data buffer.
	virtual bool DeserializeData();

	virtual rage::u32 GetTotalSize();

	void SetupMemory();

	rage::u8* GetBuffer(){return mpBuffer;}

protected:
	void Init();
	virtual void Shutdown();
};

//SaveGameDataManager
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//This class manages the organization of data that needs to be serialized and saved 
//to a profile.
//
//Class priorities
//1:CONVENIENCE
//     We want to provide functionality so that it will be easy and convenient for developers to save/load data.
//     This will include allowing developers to use data (that they own) as part of the buffer that will be written 
//     to disk.  This data must persist throughout the life of the game.  While in development, keys will be used 
//     to save/retrieve data. Also an optional version number for each data object can be specified.
//     This will minimize the corruption of previously saved data as various save data items
//     are modified or added.
//
//     Two ways to save data:
//     way 1: subclass the SaveGameData class and override the SaveData function
//     way 2: call the provided registerData functions which will internally wrap the given data with a savedata class
//
//2:SAVE/LOAD AT PRE-DEFINED TIMES.  
//     I.e. Save after race completion, high score, career goal met, load at startup, ect. Allows manual save/load
//     by player pressing ui button.  You can save or load the set of currently registered data object by using the 
//     SaveData/LoadData functions.
//
//3:MINIMIZE STORAGE/MEMORY USAGE.  
//     The XBOX 360 is very fast when saving/loading so we shouldn't need to worry about speed too much.  
//     While in development, this class utilizes keys which link saved data to a specific area.  The keys, 
//     data sizes, and data will be saved to disk.  The keys and data sizes may be removed from the serialized
//     data after all save data items are known and finalized.  At this time a structure map or #defines may be
//     used in order to reduce save file size.
//
//The save/load cycle is as follows
//Save:
//  1:data is registered for saving/loading
//  2:SaveData() function is called
//  3:data is serialized from the internal list to a buffer
//  4:buffer is saved to a game file through the player profile
//
//Load:
//  1:data is registered for saving/loading
//  2:LoadData() function is called
//  3:buffer is read from a game file through the player profile
//  4:data is de-serialized from the buffer to the internal list
//
//all save data pieces should be wrapped in a SaveGameData object.  This is needed so that we can know the 
//specifics of each. We do not want to duplicate data.  So we are storing pointers to data that exists for the game.  
//This data must persist for the life of the game.  

class SaveGameDataManager : public rage::UIScene
{   

public: 
	enum STATE {IDLE,SAVING,LOADING,GET_FILE_TIME,STARTUP_CHECKS,VERIFY_SAVE_GAME,CREATE_GAME,CHANGING_DEVICE,NUM_STATES};

	SaveGameDataManager();
	virtual ~SaveGameDataManager();

	// PURPOSE:	Register data so that when save/loading it will Serialize/Deserialize data to the internal data buffer.  
	// PARAMS:	version - a user defined version #.  This will be tested against the version that is loaded from the save file
	//          key,  a user defined key that will be linked to this data.
	//          pData - data to save. This data must persist while doing any loads/saves including future loads/saves
	//          size - size in bytes of pData
	//          pFile - optional filename to use
	//          pDisplayName - optional display name
	void RegisterData(rage::u8 version, const char* key , void* pData, rage::u32 size , 
		const char* pFile = MAIN_FILE_NAME , const char* pDisplayName = MAIN_DISPLAY_NAME);

	// PURPOSE:	Register data so that when save/loading it will Serialize/Deserialize data to the internal data buffer.  
	// PARAMS:	pData - data to save. This data must persist while doing any loads/saves including future loads/saves
	//          pFile - optional filename to use
	//          pDisplayName - optional display name
	void RegisterData(SaveGameData* pData,
		const char* pFile = MAIN_FILE_NAME , const char* pDisplayName = MAIN_DISPLAY_NAME);

	// PURPOSE:	Serialize data to the internal data buffer.  
	// PARAMS:	pfile - file to save
	//          size - size in bits of pData
	void RegisterData(SaveGameFile* pFile, const char* pFilename , const char* pDisplayName);

	// PURPOSE:	queues the file for saving. Serialize data and save to profile.
	// PARAMS:	pFile - optional filename to use
	//          verifyFile - performs a verification process on the file to make sure it is valid.  The verification includes
	//          the following :
	//          1:makes sure the correct player is signed in
	//          2:makes sure the device is valid and has room to save
	//          3:makes sure the file is the same file that has been used throughout the players game
	virtual void SaveData(const char* pFile = MAIN_FILE_NAME , bool verifyFile = true);

	// PURPOSE: queues the file for loading. Loads data from the save game file
	// PARAMS:	pFile - optional filename to use
	virtual void LoadData(const char* pFile = MAIN_FILE_NAME);

	// PURPOSE: retrieves the timestamp for the specified file on disk
	// PARAMS:	pFile - optional filename to use
	virtual void GetFileTime(const char* pFile = MAIN_FILE_NAME);

	// PURPOSE: Loads data from the save game file
	void LoadDataCB(){LoadData(MAIN_FILE_NAME);}

	// PURPOSE:	Serialize data and save to profile.
	void SaveDataCB(){SaveData(MAIN_FILE_NAME, true);}

	// PURPOSE: Does initial checks and setup for the save game system
	// 1:Selects device
	// 2:Checks for existence of a save game. Populates the mSavedGameInfo with all existing save files
	// 3:Checks to make sure there is enough free space on device
	virtual void StartupChecks();

	virtual void VerifyFile(const char* pFile= MAIN_FILE_NAME);

	virtual void Update();

	// PURPOSE: Disables saving and loading
	void SetDisabled(bool disabled);
	void SetAutoSaveDisabled(bool disabled);

	bool IsDisabled(){return m_bDisabled;}
	bool IsAutoSaveDisabled(){return m_bAutoSaveDisabled;}
	bool IsSaveSuccessful();
	void SetSaveSuccessful(bool b){m_bIsSaveSuccessful=b;}

	SaveGameFile* GetFile(const char* pFilename);

	void AddHandlers();

	// PURPOSE: checks to see if there is enough space available to save the given file
	// PARAMS:  file - The file to query.  Defaults to the main save game file
	// RETURNS: true if space is available, else false
	bool HasRoomToSave(const char* pFile = MAIN_FILE_NAME);

	bool IsCurrentDeviceValid();

	bool IsIdle(){return m_CurrentState==IDLE;}
	void SetIdle(){m_CurrentState=IDLE;}
	void SetState(STATE state){ m_CurrentState = state;}
	bool IsSaving(){return m_bIsSaving;}
	void SetIsSaving(bool b){m_bIsSaving = b;}

	void RequestRebootAfterNextSave()				{m_RebootAfterSave=true;}	// for DLC
	bool GetWillRebootAfterNextSave( ) const		{ return m_RebootAfterSave; }

	// PURPOSE: displays the device selection hud.  handles events associated with changing the device
	void ChangeDevice();
	void SetSelectedDeviceToAny();

	virtual void SetupMemory();

	void ShowDeviceSelectWindow();
	void ShowSignInWindow();

	bool IsSignedIn();

	bool IsStartupComplete(){return m_bStartupChecksComplete;}

	rage::u32 GetPadIndex() const { return m_PadIndex; }
	void SetPadIndex(rage::u32 padIndex) { m_PadIndex = padIndex; }

#ifdef __BANK   
	//void Test();

	//PURPOSE: writes a file to the device with a size that will completely fill the device up
	void FillDevice();
	void SetError();
	void ThrowTestError();

	enum
	{
		SAVEOP_NONE,
		SAVEOP_SAVE,
		SAVEOP_LOAD,
		SAVEOP_GETFILETIME,
		SAVEOP_ENUMERATE,
		SAVEOP_DEVICESELECTION,
		SAVEOP_VERIFYCONTENT,
		NUM_SAVEOPS
	};

	enum
	{
		ERROR_NONE,
		NOT_ENOUGH_FREE_SPACE_ON_DEVICE,
		USER_CANCELLED_DEVICE_SELECTION,
		FILE_CORRUPT,
		DEVICE_CORRUPT,
		PATH_NOT_FOUND,
		COULD_NOT_MAP_DEVICE,
		THE_DEVICE_IS_NOT_READY,
		UNKNOWN_ERROR,
		NUM_ERRORS
	};
#endif

protected:
	// PURPOSE: Calculate the total space needed to save the given file
	// PARAMS:  file - The file to query.  Defaults to the main save game file
	// RETURNS: The space needed in bytes
	rage::u32 GetTotalSpaceNeededToSave(const char* pFile = MAIN_FILE_NAME);

	// PURPOSE: Calculate the total space needed to save the given file
	// PARAMS:  file - The file to query.  Defaults to the main save game file
	// RETURNS: The space needed in bytes
	rage::u32 GetTotalSpaceNeededToSave(SaveGameFile*);

	// PURPOSE: Calculate the total space needed to save all files (save game, stats, photos, ect)   
	// RETURNS: The space needed in bytes
	rage::u32 GetTotalSpaceNeededToSaveAllFiles();

	// PURPOSE: Calculate the total space available for the selected device.
	// RETURNS: The space available in bytes
	rage::u32 GetTotalAvailableSpace();

	// PURPOSE: checks to see if there is enough space available to save the given file
	// PARAMS:  file - The file to query.  Defaults to the main save game file
	// RETURNS: true if space is available, else false
	bool HasRoomToSave(SaveGameFile*);

	// PURPOSE: checks to see if there is enough space available to save all available files
	// PARAMS:  file - The file to query.  Defaults to the main save game file
	// RETURNS: true if space is available, else false
	virtual bool HasRoomToSaveAllFiles();

	// PURPOSE: looks at all enumerated files and adds the index of each file that is a save game to mDevicesContainingProfiles
	//          There should only be 1 save game for each profile.
	virtual void EnumerateSaveGames();

	virtual bool DoesProfileExist();
	virtual bool DoMultipleProfilesExist();

	virtual void UpdateLoad(rage::f32);
	virtual void UpdateSave(rage::f32);
	virtual void UpdateGetFileTime(rage::f32);
	virtual void UpdateStartupChecks(rage::f32);
	virtual void UpdateVerifySaveGame(rage::f32);
	virtual void UpdateChangingDevice(rage::f32);

	virtual void Init();
	virtual void Shutdown();

	//does the actual saving and loading
	virtual void LoadData(SaveGameFile*);
	virtual void SaveData(SaveGameFile* , bool verifyFile);
	virtual void GetFileTime(SaveGameFile*);
	virtual SaveGameFile* CreateSaveFile(const char* pFilename, const char* pDisplayName);

	virtual void OnFileStartupEnumerationComplete();
	virtual void OnFileEnumerationComplete();
	virtual void OnLoadComplete();
	virtual void OnGetFileTimeComplete();
	virtual void OnCreateNewSaveGame(const char* filename = NULL);

	virtual bool AreAllSaveFilesCreated() const;
	virtual void CreateNextSaveFile();

	virtual bool HandleEvents( fuiEvent* pEvent);
	virtual bool HandleLoadEvents( fuiEvent* pEvent);
	virtual bool HandleSaveEvents( fuiEvent* pEvent);
	virtual bool HandleGetFileTimeEvents( fuiEvent* pEvent);
	virtual bool HandleStartupChecksEvents( fuiEvent* pEvent);
	virtual bool HandleVerifySaveGameEvents( fuiEvent* pEvent);
	virtual bool HandleCreateSaveEvents( fuiEvent* pEvent);
	virtual bool HandleChangingDeviceEvents( fuiEvent* pEvent);

	bool IsStorageDeviceRemoved();
	bool IsStorageDeviceChanged();
	void SetDoingFreeSpaceCheck(bool b){m_bIsOnlyCheckingSpace = b;}
	void SetIsPassedStartScreen(bool b){m_bIsPassedStartScreen = b;}

	void HandleError();

	void OnPresenceEvent (rage::rlPresence* presenceMgr, const rage::rlPresenceEvent* evt);

	void SetStateToIdle();

	virtual void ClearSaveDataArray();

	virtual void ConstructDisplayName(char16* displayName, int displayNameSize);

	// Functions for the dongle (memory card based)
	void CreateDongle();
	bool CheckDongle();
	bool IsDongleEnabled(){return m_bIsDongleEnabled;}

	void StartTimer(float f);
	void StopTimer();

protected:

	rage::s32 m_NumProfileFiles;//num save games files that were found in profile
	rage::fiSaveGame::Content mSavedGameInfo[MAX_SAVE_GAMES];//to be filled with info for all saved games
	rage::atArray<rage::u32> mDevicesContainingProfiles;
	bool m_bMultipleGamesFound;	

	STATE m_CurrentState;
	bool m_bDisabled;
	bool m_bAutoSaveDisabled;
	bool m_bDeviceSelected;
	bool m_bIsTryingToShowDeviceSelectWindow;
	bool m_bIsTryingToShowSignInWindow;
	rage::u32 m_SignInId;
	rage::u32 m_PadIndex;
	SaveGameFile* m_pSaveGameFiles[MAX_SAVE_GAMES];
	rage::u32 m_NumSaveGameFiles;
	rage::rlPresence::Delegate mPresenceDlgt;
	snuGamerLocal* m_pActiveGamer;
	rage::f32 m_DeltaTime;
	rage::f32 m_SaveTime;
	bool m_bFileVerified;
	bool m_bImmediateMode;//if true, then interruptible errors are handled immediately, otherwise they are handled at the time of save	
	bool m_bSavingNewGame;
	bool m_bIsRegistrationEnabled;
	bool m_bIsDongleEnabled;
	bool m_bSaveGameCreated;
	bool m_bDisableLoad;
	bool m_bStartupChecksComplete;
	bool m_bUserWantsToCreateANewGame;
	int m_PreviousStorageDevice;
	int m_CurrentStorageDevice;
	bool m_bControllerIsUnplugged;
	bool m_bIsOnlyCheckingSpace;
	bool m_bIsPassedStartScreen;
	bool m_bJustCreatedMainSave;

	//queue for loading multiple data loads
	SaveGameFile* m_pCurrentFile;
	char m_CurrentFilename[MAX_FILE_NAME_SIZE];

	//next 3 vars are only here instead of stack cause i'm trying to fix a weird bug where they are not being populated
	char16 m_Subtitle[MAX_DISPLAY_NAME_SIZE];
	char m_SubtitleChar[MAX_DISPLAY_NAME_SIZE];
	char m_TitleChar[MAX_DISPLAY_NAME_SIZE];

	bool m_bIsSaving;
	bool m_bIsSaveSuccessful;

	bool m_RebootAfterSave;

	//garbage registered for deletion on destruction
	rage::atArray<SaveGameFile*> m_pGarbageSaveGameFiles;

#if __BANK

	int m_SelectedOperation;
	int m_SelectedErrorCode;
	bool m_bErrorSet;
#endif
};

//PSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSN
#if __PS3
class PS3SaveGameDataManager : public SaveGameDataManager
{   
public:

	enum RageState {
		RAGEIDLE, HAD_ERROR,
		SELECTING_DEVICE, SELECTED_DEVICE,
		ENUMERATING_CONTENT, ENUMERATED_CONTENT,
		SAVING_CONTENT, SAVED_CONTENT,
		LOADING_CONTENT, LOADED_CONTENT,
		SAVING_ICON, SAVED_ICON,
		VERIFYING_CONTENT, VERIFIED_CONTENT,
		DELETING_CONTENT, DELETED_CONTENT,
		CHECKING_FREE_SPACE_NO_EXISTING_FILE, CHECKING_FREE_SPACE, HAVE_CHECKED_FREE_SPACE,
		READING_FILE_TIME, HAVE_READ_FILE_TIME,
		GETTING_CREATOR, GOT_CREATOR
	};

	RageState m_RageState;
	char* m_pIcon;

public:   
	PS3SaveGameDataManager();
	virtual ~PS3SaveGameDataManager();

	// PURPOSE: Does initial checks and setup for the save game system
	// 1:Selects device
	// 2:Checks for existence of a save game. Populates the mSavedGameInfo with all existing save files
	// 3:Checks to make sure there is enough free space on device
	virtual void StartupChecks();

	void VerifyFile(const char* pFile= MAIN_FILE_NAME);

	// PURPOSE: Disables saving and loading
	//void SetDisabled(bool disabled);

	void SaveData(const char* pFile = MAIN_FILE_NAME , bool verifyFile = true){SaveGameDataManager::SaveData(pFile,verifyFile);}
	void LoadData(const char* pFile = MAIN_FILE_NAME){SaveGameDataManager::LoadData(pFile);}
	void GetFileTime(const char* pFile = MAIN_FILE_NAME){SaveGameDataManager::GetFileTime(pFile);}
protected:
	void UpdateLoad(rage::f32);
	void UpdateSave(rage::f32);
	void UpdateGetFileTime(rage::f32);
	void UpdateStartupChecks(rage::f32);
	void UpdateVerifySaveGame(rage::f32);

	void Init();

	//does the actual saving and loading
	void LoadData(SaveGameFile*);
	void SaveData(SaveGameFile* , bool verifyFile);
	void GetFileTime(SaveGameFile*);

	virtual void OnFileStartupEnumerationComplete();

	bool HandleEvents( fuiEvent* pEvent);
	bool HandleLoadEvents( fuiEvent* pEvent);
	bool HandleSaveEvents( fuiEvent* pEvent);
	bool HandleGetFileTimeEvents( fuiEvent* pEvent);
	bool HandleStartupChecksEvents( fuiEvent* pEvent);
	bool HandleVerifySaveGameEvents( fuiEvent* pEvent);
	bool HandleCreateSaveEvents( fuiEvent* pEvent);

	bool DoMultipleProfilesExist();
};

#endif
//PSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSN

}//end namespace rage
#endif


