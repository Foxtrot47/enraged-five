// 
// mcUI/UISaveGameData.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "UISaveGameData.h"
#include "system/memory.h"
#include "Data/marker.h"
#include "diag/output.h"
#include "input/dongle.h"
#include "grcore/device.h"
#include "fuicore/UIManager.h"
#include "system/param.h"
#include "file/stream.h"
#include "file/asset.h"
#include "system/timemgr.h"
#include "snu/snusocket.h"
#include "bank/bkmgr.h"
#include "fuicore/EventManager.h"
#include "snu/snugamer.h"

#if __PS3
#include "rline/rlachievement.h"
#endif

#if __BANK
#include "bank/bank.h"
#include "data/callback.h"
#endif

#if __XENON
#include <system/xtl.h>
#include <xdk.h>
#endif // __XENON

//################################################################################################################################
//increment this if you want to invalidate everyones local save game file.  A new save game will be created on load.
// Next time this changes, just make it 11.
#define SAVEGAME_VERSION 1
//################################################################################################################################
namespace rage {
USES_CONVERSION;

// set FORCE_DONGLE to make the game require a dongle to load
#define FORCE_DONGLE (0)
#define ICONSIZE 78131 

//TODO: use something like fiSaveGame::GetContentType(SAVED_GAME); instead of hard coding the content type
#define CONTENT_TYPE 1 /*XCONTENTTYPE_SAVEDGAME*/

#define DEVICE_POLL_FREQUENCY 1 //poll for mem card insertion/removal every second

#define REQUIRED_TROPHY_SPACE 50 *1024 * 1024

PARAM(nosave,"Disable saving");
PARAM(noload,"Disable saving");
PARAM(disablesave,"Disable saving");
PARAM(deletesave,"virtually deletes your save game every time you restart");
PARAM(dongle,"Test Dongle functionality");

#if __BANK
const char* operations[SaveGameDataManager::NUM_SAVEOPS] = { "None", "Save", "Load", "Get File Time", "Enumerate", "Select Device", "Verify Content" };
const char* errorcodes[SaveGameDataManager::NUM_ERRORS] = { "None",
	"Not enough free space",
	"Cancelled Device Selection",
	"File Corrupt",
	"Device Corrupt",
	"Path Not Found",
	"Could Not Map Device",
	"Device Not Ready",
	"Unexpected Error" };
#endif

double				sm_StateTimerStop = 0;
f32           sm_StateTimeElapsed = 0;
bool				sm_TimingAState = false;
bool			bDisableLoad = false;
bool			bIsRegistrationEnabled = true;

SaveGameData::SaveGameData()
{
	mOwnsData=false;
	mVersion = 0;
	mDataSize = 0;
	strcpy(mpKey,"");
	mKeySize = 0;
	mpData = NULL;  
}

SaveGameData::~SaveGameData()
{
	if ( mOwnsData )
	{
		if(mpData)
		{
			delete (char*) mpData;
			mpData = NULL;  
		}
	}
}

void SaveGameData::SetKey(char* pKey)
{
	mKeySize = strlen(pKey);

	Assertf(mKeySize < MAX_KEY_SIZE-1,"Key size too big. reduce key size or increase MAX_KEY_SIZE");

	strncpy(mpKey,pKey,MAX_KEY_SIZE);
	mpKey[MAX_KEY_SIZE-1] = 0;
}

void SaveGameData::SetDataSize(u32 dataSize)
{
	mDataSize = dataSize;
}

u32 SaveGameData::SerializeKey( u8* pBuffer , u32 bufferSize)
{
	u32 bufferIterator=0;

	if (sizeof(u32)>bufferSize-bufferIterator)
	{      
		UIDebugf1("SAVE GAME : No more room on buffer to serialize");
		return bufferIterator;
	}

	memcpy(&pBuffer[bufferIterator],&mKeySize,sizeof( u32 ));
	bufferIterator += sizeof( u32);

	if (mKeySize>bufferSize-bufferIterator)
	{      
		UIDebugf1("SAVE GAME : No more room on buffer to serialize");
		return bufferIterator;
	}

	memcpy(&pBuffer[bufferIterator],mpKey,mKeySize);
	bufferIterator += mKeySize;

	UIDebugf1("SAVE GAME : %-10s key serialized",mpKey);

	return bufferIterator;
}

u32 SaveGameData::SerializeHeader(unsigned char* pBuffer , unsigned int bufferSize)
{
	u32 bufferIterator=0;

	if (sizeof(u8)>bufferSize-bufferIterator)
	{      
		UIDebugf1("SAVE GAME : No more room on buffer to serialize");
		return bufferIterator;
	}

	pBuffer[bufferIterator] = mVersion;

	bufferIterator += sizeof(u8);

	if (sizeof(u32)>bufferSize-bufferIterator)
	{      
		UIDebugf1("SAVE GAME : No more room on buffer to serialize");
		return bufferIterator;
	}

	memcpy(&pBuffer[bufferIterator],&mDataSize,sizeof(u32));
	bufferIterator += sizeof(u32);

	return bufferIterator;
}

u32 SaveGameData::Serialize(unsigned char* pBuffer , unsigned int bufferSize)
{
	u32 bufferIterator=0;

	if (mDataSize>bufferSize-bufferIterator)
	{      
		UIDebugf1("SAVE GAME : No more room on buffer to serialize");

		//serialize as much as we can
		memcpy(&pBuffer[bufferIterator],mpData,bufferSize-bufferIterator);
		bufferIterator += bufferSize-bufferIterator;

		return bufferIterator;
	}

	memcpy(&pBuffer[bufferIterator],mpData,mDataSize);
	bufferIterator += mDataSize;

	UIDebugf1("SAVE GAME : %-10s data serialized",mpKey);
	return bufferIterator;
}

bool SaveGameData::DeserializeHeader( u8* pBuffer , u32 bufferSize, u32* mainBufferIterator)
{
	u32 bufferIterator=0;
	u32 dataSize;
	u8 version;
	u32 versionSize = sizeof(u8);
	bool saveFileValidationFailed=false;

	if(versionSize > bufferSize-bufferIterator )
	{
		UIDebugf1("SAVE GAME : No more room on buffer to deserialize");
		*mainBufferIterator += bufferIterator;
		return false;
	}   

	//OPTIMIZE:pack to the bit instead of every 32 bits or Byte   
	version = pBuffer[bufferIterator];
	bufferIterator += versionSize;

	if(sizeof(u32) > bufferSize-bufferIterator )
	{
		UIDebugf1("SAVE GAME : No more room on buffer to deserialize");
		*mainBufferIterator += bufferIterator;
		return false;
	}   

	memcpy(&dataSize,&pBuffer[bufferIterator],sizeof(u32));
	bufferIterator += sizeof(u32);

	//version check
	if (mVersion != version)
	{
		UIDebugf1("Save game uses an older version of data for %s. Save game version = %d while build version = %d",mpKey,version,mVersion);
		saveFileValidationFailed = true;
	}

	//data size check
	if(mDataSize != dataSize)
	{
		UIDebugf1("SAVE GAME : data size in memory differs from data size on disk for %s data. Your save file may be outdated or corrupt.",mpKey);      
		saveFileValidationFailed = true;
	}

	//mDataSize = dataSize;
	*mainBufferIterator += bufferIterator;

	if (saveFileValidationFailed)
	{
		return false;
	}
	else
	{
		return true;
	}
}

u32 SaveGameData::Deserialize( u8* pBuffer , u32 bufferSize)
{
	u32 bufferIterator=0;
	u32 availableSpace = bufferSize-bufferIterator;

	if( mDataSize > availableSpace )
	{
		UIDebugf1("SAVE GAME : No more room on buffer to deserialize");

		//only copy up to the available space on the buffer
		memcpy(mpData,&pBuffer[bufferIterator],availableSpace);
		bufferIterator += availableSpace;
	}     
	else
	{
		//room to copy all the data
		memcpy(mpData,&pBuffer[bufferIterator],mDataSize);
		bufferIterator += mDataSize;
	}   

	return bufferIterator;
}

u32 SaveGameData::GetTotalSize()
{
	u32 size=0;
	size += mKeySize;
	size += mDataSize;
	size += sizeof(u32);//mKeySize size
	size += sizeof(u32);//mDataSize size
	size += sizeof(u8);//version number

	return size;
}

SaveGameFile::~SaveGameFile()
{
}

SaveGameFile::SaveGameFile()
{
	Init();
}

void SaveGameFile::Shutdown()
{
	//delete save objects that we have created
	for(s32 i=0;i<mDeletableData.size();i++)
	{  
		if(mDeletableData[i])
		{
			delete mDeletableData[i];
			mDeletableData[i] = NULL;  
		}
	}   

	delete[] mpBuffer;
	mpBuffer = NULL;
}

void SaveGameFile::Init()
{
	for(u32 i=0;i<DATA_COUNT;i++)
	{  
		mpData[i] = NULL;
	}

	m_NumSaveObjects=0;
	m_BufferSize = 0;
	mpBuffer = NULL;
	m_bSaveRequested = false;
	mLoadRequested = false;
	m_bLoaded = false;
	m_Version = SAVEGAME_VERSION;
	m_Timestamp = 0;
}

SaveGameData* SaveGameFile::GetData(const char* pKey)
{
	for(u32 i=0;i<m_NumSaveObjects;i++)
	{  
		if ( stricmp( mpData[i]->mpKey , pKey ) == 0 )
		{
			return mpData[i];
		}
	}

	return NULL;
}

u32 SaveGameFile::GetTotalSize()
{
	u32 size=0;
	for(u32 i=0;i<m_NumSaveObjects;i++)
	{
		size += mpData[i]->GetTotalSize();
	}

	return size;
}

void SaveGameFile::SetupMemory()
{
	RAGE_FUNC();

	u32 bufferSize=0;
	//find out how large the buffer needs to be
	for(u32 i=0;i<m_NumSaveObjects;++i)
	{
		UIDebugf1("SAVE GAME : %-16s data (estimated) size %d",mpData[i]->mpKey,mpData[i]->GetTotalSize());
		bufferSize += mpData[i]->GetTotalSize();
	}
	bufferSize += sizeof(m_Version);
	bufferSize += sizeof(m_NumSaveObjects);

	//resize the buffer if it is too small
	if (m_BufferSize < bufferSize)
	{
		//saved data has changed, lets make a new buffer with new size
		if (mpBuffer)
		{
			delete[] mpBuffer;
		}
		UIDebugf1("SAVE GAME : Allocating buffer of %d bytes",bufferSize);
		mpBuffer = rage_new u8[bufferSize];  

		m_BufferSize = bufferSize;
	}
}

// PURPOSE:	Serialize data to the internal data buffer.  
// PARAMS:	pData - data to save
//          size - size in bits of pData
void SaveGameFile::RegisterData(u8 version, const char* pKey , void* pData, u32 size)
{	
	//check to see if this data has been saved before
	SaveGameData* data = GetData( pKey );

	Assert(pData);

	if(data==NULL)
	{
		Assert(m_NumSaveObjects<DATA_COUNT);

		Assertf(bIsRegistrationEnabled,"This data must be registered while registration is enabled");

		mpData[m_NumSaveObjects] = rage_new SaveGameData();

		mpData[m_NumSaveObjects]->mVersion = version;
		strncpy(mpData[m_NumSaveObjects]->mpKey , pKey,MAX_KEY_SIZE-1);
		mpData[m_NumSaveObjects]->mKeySize = strlen(pKey);
		mpData[m_NumSaveObjects]->mDataSize = size;
		mpData[m_NumSaveObjects]->mpData = pData;

		//register for deletion on shutdown
		mDeletableData.PushAndGrow( mpData[m_NumSaveObjects] );

		++m_NumSaveObjects;

		UIDebugf1("SAVE GAME : Registering new data for save, key=%s",pKey);
		UIDebugf1("SAVE GAME : Number of objects registered to save = %d",m_NumSaveObjects);
	}
	else
	{
		//overwrite data that was already registered
		data->mVersion = version;
		strncpy(data->mpKey , pKey,MAX_KEY_SIZE-1);
		data->mKeySize = strlen(pKey);
		data->mDataSize = size;
		data->mpData = pData;

		UIDebugf1("SAVE GAME : reregistering data for save! key=%s",pKey);
	}
}

// PURPOSE:	Serialize data to the internal data buffer.  
// PARAMS:	pData - data to save
void SaveGameFile::RegisterData(SaveGameData* pData)
{   
	//check to see if this data has been saved before
	SaveGameData* data = GetData( pData->mpKey );

	Assert(pData);

	if(data==NULL)
	{
		//this is new data
		mpData[m_NumSaveObjects] = pData;

		++m_NumSaveObjects;

		UIDebugf1("SAVE GAME : Registering new data for save, key=%s",pData->mpKey);
		UIDebugf1("SAVE GAME : Number of objects registered to save = %d",m_NumSaveObjects);
	}
	else
	{
		//overwrite data that was already registered
		for(u32 i=0;i<m_NumSaveObjects;++i)
		{
			if (strcmp(mpData[i]->mpKey,pData->mpKey)==0)
			{
				mpData[i] = pData;
				break;
			}
		}
		UIDebugf1("SAVE GAME : reregistering data for save! key=%s",pData->mpKey);
	}
}

// PURPOSE:	Serialize data and save to profile.
void SaveGameFile::SaveData()
{
	u32 i=0;   
	u32 bufferIterator=0;
	u32 bufferSize=0;	

	//find out how large the buffer needs to be
	for(i=0;i<m_NumSaveObjects;++i)
	{
		UIDebugf1("SAVE GAME : %-16s data (estimated) size %d",mpData[i]->mpKey,mpData[i]->GetTotalSize());
		bufferSize += mpData[i]->GetTotalSize();
	}
	bufferSize += sizeof(m_NumSaveObjects);
	bufferSize += sizeof(m_Version);

	//resize the buffer if it is too small
	if (m_BufferSize < bufferSize)
	{
		Assertf(0,"Save game buffer size too small!!");
		//saved data has changed, lets make a new buffer with new size
		if (mpBuffer)
		{
			delete[] mpBuffer;
		}
		UIDebugf1("SAVE GAME : Allocating buffer of %d bytes",bufferSize);
		mpBuffer = rage_new u8[bufferSize];  //mpBuffer = (u8*)UIMANAGER->GetFlashNavigator()->GetAllocator()->Allocate(bufferSize*sizeof(u8), 0);

		m_BufferSize = bufferSize;
	}

	//save out the file version
	memcpy(&mpBuffer[bufferIterator] , &m_Version , sizeof(m_Version));
	bufferIterator += sizeof(m_Version);

	//save out the number of objects
	memcpy(&mpBuffer[bufferIterator] , &m_NumSaveObjects , sizeof(m_NumSaveObjects));
	bufferIterator += sizeof(m_NumSaveObjects);

	//serialize all objects to the buffer
	for(i=0;i<m_NumSaveObjects;++i)
	{
		u32 bytesSent = 0;
		bytesSent += mpData[i]->SerializeKey   (&mpBuffer[bufferIterator],bufferSize-bufferIterator);
		bytesSent += mpData[i]->SerializeHeader(&mpBuffer[bufferIterator+bytesSent],bufferSize-bufferIterator-bytesSent);
		bytesSent += mpData[i]->Serialize      (&mpBuffer[bufferIterator+bytesSent],bufferSize-bufferIterator-bytesSent);
		if (bytesSent!=mpData[i]->GetTotalSize())
		{
			Errorf("SAVE GAME : Buffer size mismatch in %s (sent %d, estimated %d)\n", mpData[i]->mpKey, bytesSent, mpData[i]->GetTotalSize());
		}
		bufferIterator += bytesSent;
	}
}

// PURPOSE:	Deserialize data from the save game file into the internal data buffer.
void SaveGameFile::LoadData()
{
	u32 i=0;      
	u32 bufferSize=0;

	//find out how large the buffer needs to be
	UIDebugf1("total objects = %d",m_NumSaveObjects);
	for(i=0;i<m_NumSaveObjects;++i)
	{
		UIDebugf1("SAVE GAME : %-16s data (estimated) size %d",mpData[i]->mpKey,mpData[i]->GetTotalSize());
		bufferSize += mpData[i]->GetTotalSize();
	}
	bufferSize += sizeof(m_Version);
	bufferSize += sizeof(m_NumSaveObjects);

	//resize the buffer if it is too small
	if (m_BufferSize < bufferSize)
	{
		Assertf(0,"Save game buffer size too small!!");
		//saved data has changed, lets make a new buffer with new size
		if (mpBuffer)
		{
			delete[] mpBuffer;
		}
		UIDebugf1("SAVE GAME : Allocating buffer of %d bytes",bufferSize);
		mpBuffer = rage_new u8[bufferSize];
		m_BufferSize = bufferSize;
	}
}

// PURPOSE:	Deserialize data from the save game file into the internal data buffer.
bool SaveGameFile::DeserializeData()
{
	u32 bufferIterator=0;
	u32 keySize;
	char key[MAX_KEY_SIZE];
	SaveGameData* pData;
	u32 i;
	u32 numLoadObjects;
	bool validEntry;
	u32 version;

	if(bDisableLoad || PARAM_noload.Get()  || PARAM_deletesave.Get())
	{
		UIDebugf1("SAVE GAME : -noload is found on your cammand line. All save data is reset to defaults");
		return false;
	}

	//load the number of objects
	memcpy(&version ,&mpBuffer[bufferIterator] , sizeof(version));
	bufferIterator += sizeof(version); 

	if (version != m_Version)
	{
		UIDebugf1("SAVE GAME : your save game is now invalid due to a recent change.  All save data is reset to defaults");
		return false;
	}

	memcpy(&numLoadObjects ,&mpBuffer[bufferIterator] , sizeof(numLoadObjects));
	bufferIterator += sizeof(numLoadObjects); 

	//deserialize all objects to the buffer
	for(i=0;i<numLoadObjects && bufferIterator<m_BufferSize;++i)
	{
		//get the key
		memcpy(&keySize,&mpBuffer[bufferIterator],sizeof(u32));      
		bufferIterator += sizeof(u32);//for key size
		UIDebugf1("key size %d\n", keySize);
		memcpy(key, &mpBuffer[bufferIterator],keySize);
		key[keySize] = '\0';
		bufferIterator += keySize;//for key

		pData = GetData(key);

		if (pData)
		{
			validEntry = pData->DeserializeHeader(&mpBuffer[bufferIterator],m_BufferSize-bufferIterator,&bufferIterator);
			if (validEntry)
			{
				//this entry looks valid.  Lets deserialize it
				bufferIterator += pData->Deserialize(&mpBuffer[bufferIterator],m_BufferSize-bufferIterator);
				UIDebugf1("SAVE GAME : %-16s data loaded",key);
			}
			else
			{
				//invalid data.  lets skip it
				bufferIterator += pData->mDataSize;
				pData->OnLoadFail();
				UIDebugf1("Could not load %s data",key);
				return false;//lets not try to load anymore data (mDataSize value may be corrupt)
			}
		}
		else
		{
			UIDebugf1("Can't load saved object %s.  It must be registered before loading",key);
		}
	}

	return true;
}

SaveGameDataManager::SaveGameDataManager()
{
	AddHandlers();
	mPresenceDlgt.Bind(this, &SaveGameDataManager::OnPresenceEvent);

#if ENABLE_DONGLE
	m_bIsDongleEnabled = true;
#else
	m_bIsDongleEnabled = false;
#endif 

	m_SignInId = 0;//just default to controller 1 for now
	m_PadIndex = 0;
	m_bDisabled = false;
	m_bAutoSaveDisabled = false;
	SetState(IDLE);
	m_bDeviceSelected = false;
	m_bIsTryingToShowDeviceSelectWindow = false;
	m_bIsTryingToShowSignInWindow = false;
	m_DeltaTime = 0.0;
	m_bFileVerified = false;
	m_bImmediateMode = false;
	m_bMultipleGamesFound = false;
	m_bSavingNewGame = false;
	m_pCurrentFile = NULL;
	m_bSaveGameCreated = false;
	m_bDisableLoad = false;
	m_bStartupChecksComplete = false;
	m_bIsSaving = false;
	m_bIsSaveSuccessful = true;
	m_RebootAfterSave = false;
	m_bUserWantsToCreateANewGame = false;
	m_PreviousStorageDevice = 0;
	m_bControllerIsUnplugged = false;
	m_bIsOnlyCheckingSpace = false;
	m_bIsPassedStartScreen = false;
	m_bJustCreatedMainSave = false;
#if __BANK
	m_SelectedOperation = 0;
	m_SelectedErrorCode = 0;
#endif

	u32 i;

	for(i=0;i<MAX_SAVE_GAMES;i++)
	{
		m_pSaveGameFiles[i] = NULL;
	}
	m_NumSaveGameFiles = 0;
	m_NumProfileFiles = 0;
}

SaveGameDataManager::~SaveGameDataManager()
{
	Shutdown();
}

void SaveGameDataManager::Shutdown()
{
	SNUSOCKET.GetPresenceManager()->RemoveDelegate(&mPresenceDlgt);

	for(int i=0;i<m_pGarbageSaveGameFiles.GetCount();i++)
	{
		delete m_pGarbageSaveGameFiles[i];
	}
}

void SaveGameDataManager::Init()
{
	UIScene::Init();

#if __BANK
	bkBank& b = BANKMGR.CreateBank("SaveGame");

	//b.AddButton("Save",datCallback(MFA(SaveGameDataManager::SaveDataCB), this));
	//b.AddButton("Load",datCallback(MFA(SaveGameDataManager::LoadDataCB), this));
	b.AddButton("Fill Device",datCallback(MFA(SaveGameDataManager::FillDevice), this));
	b.PushGroup("Error Testing");
	b.AddCombo("Operation", &m_SelectedOperation, NUM_SAVEOPS, operations, 0);
	b.AddCombo("Error", &m_SelectedErrorCode, NUM_ERRORS, errorcodes, 0);
	b.AddButton("Set Error",datCallback(MFA(SaveGameDataManager::SetError), this));
	b.PopGroup();
#endif
}

void SaveGameDataManager::SetupMemory()
{
	for(u32 i=0;i<m_NumSaveGameFiles;i++)
	{  
		m_pSaveGameFiles[i]->SetupMemory();
	}
}

bool SaveGameDataManager::IsSaveSuccessful()
{
	if (PARAM_disablesave.Get() || IsDisabled())
	{
		return true;
	}
	else
	{
		return m_bIsSaveSuccessful;
	}
}

bool SaveGameDataManager::IsSignedIn()
{
	if (m_pActiveGamer)
	{
		return m_pActiveGamer->IsSignedIn();
	}
	else
	{
		return false;
	}
}

void SaveGameDataManager::StartTimer(float f)
{
	sm_TimingAState = true;
	sm_StateTimeElapsed = 0;
	sm_StateTimerStop = f;
}

void SaveGameDataManager::StopTimer()
{
	sm_TimingAState = false;
	sm_StateTimeElapsed = 0;
	sm_StateTimerStop = 0;
}

void SaveGameDataManager::ClearSaveDataArray()
{
	for(u32 i=0;i<MAX_SAVE_GAMES;i++)
	{  
		mSavedGameInfo[i].DeviceId = 0;
		mSavedGameInfo[i].ContentType = 0;
		mSavedGameInfo[i].DisplayName[0] = 0;
		mSavedGameInfo[i].Filename[0] = 0;
	}
}

void SaveGameDataManager::ConstructDisplayName(char16* displayName, int displayNameSize)
{
	AsciiToWide(displayName, m_pCurrentFile->mDisplayName, displayNameSize);
}

SaveGameFile* SaveGameDataManager::GetFile(const char* pFilename)
{
	for(u32 i=0;i<m_NumSaveGameFiles;i++)
	{  
		if ( strcmp( m_pSaveGameFiles[i]->mFilename , pFilename ) == 0 )
		{
			return m_pSaveGameFiles[i];
		}
	}

	return NULL;
}

SaveGameFile* SaveGameDataManager::CreateSaveFile(const char* pFilename, const char* pDisplayName)
{
	Assertf(m_NumSaveGameFiles<MAX_SAVE_GAMES,"You must increase the size of MAX_SAVE_GAMES");

	//this is a new file
	m_pSaveGameFiles[m_NumSaveGameFiles] = rage_new SaveGameFile();
	m_pGarbageSaveGameFiles.PushAndGrow(m_pSaveGameFiles[m_NumSaveGameFiles]);//register for deletion
	strncpy(m_pSaveGameFiles[m_NumSaveGameFiles]->mFilename , pFilename,MAX_DISPLAY_NAME_SIZE-1);
	strncpy(m_pSaveGameFiles[m_NumSaveGameFiles]->mDisplayName , pDisplayName,MAX_DISPLAY_NAME_SIZE-1);

	++m_NumSaveGameFiles;

	UIDebugf1("SAVE GAME : Registering new file for save, file=%s",pDisplayName);
	UIDebugf1("SAVE GAME : Number of files registered to save = %d",m_NumSaveGameFiles);

	return m_pSaveGameFiles[m_NumSaveGameFiles-1];
}

// PURPOSE:	Serialize data to the internal data buffer.  
// PARAMS:	pfile - file to save
//          size - size in bits of pData
void SaveGameDataManager::RegisterData(SaveGameFile* pFile, const char* pFilename , const char* pDisplayName)
{
	//this is a new file
	m_pSaveGameFiles[m_NumSaveGameFiles] = pFile;
	strncpy(m_pSaveGameFiles[m_NumSaveGameFiles]->mFilename , pFilename,MAX_FILE_NAME_SIZE-1);
	strncpy(m_pSaveGameFiles[m_NumSaveGameFiles]->mDisplayName , pDisplayName,MAX_DISPLAY_NAME_SIZE-1);

	++m_NumSaveGameFiles;

	Assertf(m_NumSaveGameFiles<MAX_SAVE_GAMES,"You must increase the size of MAX_SAVE_GAMES");

	UIDebugf1("SAVE GAME : Registering new file for save, file=%s",pDisplayName);
	UIDebugf1("SAVE GAME : Number of files registered to save = %d",m_NumSaveGameFiles);      
}

// PURPOSE:	Serialize data to the internal data buffer.  
// PARAMS:	pData - data to save
//          size - size in bits of pData
void SaveGameDataManager::RegisterData(u8 version, const char* pKey , void* pData, u32 size,
	const char* pFile/*= MAIN_FILE_NAME*/ , const char* pDisplayName/* = MAIN_DISPLAY_NAME*/)
{
	//check to see if this file has been registered already
	SaveGameFile* file = GetFile( pFile );  

	if(file==NULL)
	{
		file = CreateSaveFile(pFile,pDisplayName);
	}

	file->RegisterData(version,pKey,pData,size);
}

// PURPOSE:	Serialize data to the internal data buffer.  
// PARAMS:	pData - data to save
void SaveGameDataManager::RegisterData(SaveGameData* pData,
	const char* pFile/*= MAIN_FILE_NAME*/ , const char* pDisplayName/* = MAIN_DISPLAY_NAME*/)
{   
	//check to see if this file has been registered already
	SaveGameFile* file = GetFile( pFile );  

	if(file==NULL)
	{
		file = CreateSaveFile(pFile,pDisplayName);
	}

	file->RegisterData(pData);
}

// PURPOSE:	Serialize data and save to profile.
void SaveGameDataManager::SaveData(const char* pFile/*= MAIN_FILE_NAME*/, bool verifyFile )
{
	SaveGameFile* file = GetFile(pFile);
	SaveData(file,verifyFile);
}

void SaveGameDataManager::SaveData(SaveGameFile* pFile, bool verifyFile)
{
	m_SaveTime = 0.0f;

	if(m_bDisabled)
	{
		UIDebugf1("SAVE GAME : Saving and Loading disabled");
	}
	else if(PARAM_nosave.Get())
	{
		UIDebugf1("SAVE GAME : -nosave found, Saving disabled");
		UIMANAGER->SendEvent("fileSaveSuccess");
	}
	else if(!pFile)
	{
		UIDebugf1("SAVE GAME : No file registered for loading!");
	}
	else if(m_CurrentState!=IDLE)
	{
		UIDebugf1("SAVE GAME : DROPPING VERIFY SAVE REQUEST, You are currently doing IO");			
	}
	else if(m_pActiveGamer && m_pActiveGamer->IsSignedIn() == false)
	{
		UIDebugf1("SAVE GAME : Player has signed out!");
		UIMANAGER->SendEvent("filePlayerHasSignedOut");
	}	
	else
	{
		m_bIsSaving = true;
		m_bIsSaveSuccessful = false;

		if (!m_bSaveGameCreated && !m_bSavingNewGame && m_bStartupChecksComplete)
		{
			verifyFile=false;//don't verify if startup checks is complete cause this means that the file is corrupt
		}

		if (m_bSaveGameCreated || m_bSavingNewGame || m_bStartupChecksComplete)
		{		
			UIDebugf1("SAVE GAME : Save Starting");			

			pFile->m_bSaveRequested = true;
			m_pCurrentFile = pFile;
			safecpy(m_CurrentFilename,pFile->mFilename,MAX_FILE_NAME_SIZE);			

			UIDebugf1("SAVE GAME : Save Starting %s",m_CurrentFilename);

			if (verifyFile)
			{
				UIMANAGER->SendEvent("fileVerifyAndSaveStart");
			}
			else
			{
				m_CurrentState = SAVING;

				UIMANAGER->SendEvent("fileSaveStart");

				if (SAVEGAME.IsCurrentDeviceValid(m_SignInId)==false)
				{
					UIDebugf1("SAVE GAME : Storage device has changed!");
					m_CurrentState = IDLE;//we get a hang cause we are still in SAVING state when trying to changeDevice()
					UIMANAGER->SendEvent("fileStorageDeviceChanged");         
				}
				else if (HasRoomToSave(pFile->mFilename) == false)
				{
					UIMANAGER->SendEvent("fileNoFreeSpace");
					UIDebugf1("SAVE GAME : No room on current storage device!");
				}
				else 
				{   
					m_pCurrentFile->SaveData();

					//start saving
					char16 displayName[MAX_DISPLAY_NAME_SIZE];
					ConstructDisplayName(displayName, MAX_DISPLAY_NAME_SIZE);
					WideToUtf8(m_pCurrentFile->mDisplayName, displayName, MAX_DISPLAY_NAME_SIZE);

					UIDebugf1("SAVE GAME : Saving file %s",m_pCurrentFile->mDisplayName );

					m_SaveTime = 0.0f;

					SAVEGAME.BeginSave(m_SignInId,(unsigned)CONTENT_TYPE,displayName,m_pCurrentFile->mFilename,
						m_pCurrentFile->mpBuffer,m_pCurrentFile->m_BufferSize,true);
				}
			}
		}
		else
		{
			//we need to create a new game and do all the checks before we can save
			UIMANAGER->SendEvent("fileStartPlayerChecks");
		}
	}
}

// PURPOSE:	Deserialize data from the save game file into the internal data buffer.
void SaveGameDataManager::VerifyFile(const char* pFile/*= MAIN_FILE_NAME*/)
{
	SaveGameFile* file = GetFile(pFile);

	if(m_bDisabled)
	{
		UIDebugf1("SAVE GAME : Saving and Loading disabled");
	}
	else if(m_CurrentState!=IDLE)
	{
		UIDebugf1("SAVE GAME : DROPPING VERIFY SAVE REQUEST, You are currently doing IO");			
	}
	else if(m_pActiveGamer->IsSignedIn() == false)
	{
		UIDebugf1("SAVE GAME : Player has signed out!");
		UIMANAGER->SendEvent("filePlayerHasSignedOut");
	}
	else if(!file)
	{
		UIDebugf1("SAVE GAME : No file registered for loading!");
	}
	else
	{
		file->mLoadRequested = true;		
		m_pCurrentFile = file;
		safecpy(m_CurrentFilename,m_pCurrentFile->mFilename,MAX_FILE_NAME_SIZE);
		ClearSaveDataArray();
		SAVEGAME.BeginEnumeration(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo,MAX_SAVE_GAMES);

		m_CurrentState = VERIFY_SAVE_GAME;
	}
}

// PURPOSE:	Deserialize data from the save game file into the internal data buffer.
void SaveGameDataManager::LoadData(const char* pFile/*= MAIN_FILE_NAME*/)
{	
	SaveGameFile* file = GetFile(pFile);
	LoadData(file);
}

void SaveGameDataManager::LoadData(SaveGameFile* pFile)
{
	if(m_bDisabled)
	{
		UIDebugf1("SAVE GAME : Saving and Loading disabled");
	}
	else if(PARAM_noload.Get())
	{
		UIDebugf1("SAVE GAME : -noload found, Loading disabled");
		UIMANAGER->SendEvent("fileLoadSuccess");
	}
	else if(!pFile)
	{
		UIDebugf1("SAVE GAME : No file registered for loading!");
	}
	else if(m_CurrentState!=IDLE)
	{
		UIDebugf1("SAVE GAME : DROPPING LOAD GAME REQUEST, You are currently doing IO");			
	}
	else if(m_pActiveGamer->IsSignedIn() == false)
	{
		UIDebugf1("SAVE GAME : Player has signed out!");
		UIMANAGER->SendEvent("filePlayerHasSignedOut");
	}
	else
	{
		u32 index = 0;

		//start the load process
		m_CurrentState = LOADING;

		UIMANAGER->SendEvent("fileLoadStart");

		pFile->LoadData();

		//find the index for this file
		while(strcmp(mSavedGameInfo[index].Filename , pFile->mFilename)!=0 && index < MAX_SAVE_GAMES)
		{
			++index;
		}

		if (index >= MAX_SAVE_GAMES)
		{
			//file not found. Display error to player?
			UIDebugf1("SAVE GAME : File not found.");

			UIMANAGER->SendEvent("fileNoFilesExist");              
		}
		else
		{
			pFile->mLoadRequested = true;
			m_pCurrentFile = pFile;
			safecpy(m_CurrentFilename,m_pCurrentFile->mFilename,MAX_FILE_NAME_SIZE);

			SAVEGAME.BeginLoad(m_SignInId,(unsigned)CONTENT_TYPE,
				mSavedGameInfo[index].DisplayName,mSavedGameInfo[index].DeviceId, mSavedGameInfo[index].Filename,pFile->mpBuffer,pFile->m_BufferSize);
		}
	}
}

void SaveGameDataManager::GetFileTime(const char* pFile/*= MAIN_FILE_NAME*/)
{	
	SaveGameFile* file = GetFile(pFile);
	GetFileTime(file);
}

void SaveGameDataManager::GetFileTime(SaveGameFile* pFile)
{
	if(m_bDisabled)
	{
		UIDebugf1("SAVE GAME : Saving and Loading disabled");
	}
	else if(!pFile)
	{
		UIDebugf1("SAVE GAME : No file registered for finding timestamp!");
	}
	else if(m_CurrentState!=IDLE)
	{
		UIDebugf1("SAVE GAME : IGNORING GET FILE TIME REQUEST, You are currently doing IO");			
	}
	else if(m_pActiveGamer->IsSignedIn() == false)
	{
		UIDebugf1("SAVE GAME : Player has signed out!");
		UIMANAGER->SendEvent("filePlayerHasSignedOut");
	}
	else
	{
		u32 index = 0;

		//start the load process
		m_CurrentState = GET_FILE_TIME;

		UIMANAGER->SendEvent("fileGetFileTimeStart");

		//find the index for this file
		while(strcmp(mSavedGameInfo[index].Filename , pFile->mFilename)!=0 && index < MAX_SAVE_GAMES)
		{
			++index;
		}

		if (index >= MAX_SAVE_GAMES)
		{
			//file not found. Display error to player?
			UIDebugf1("SAVE GAME : File not found.");

			UIMANAGER->SendEvent("fileNoFilesExist");
		}
		else
		{
			UIDebugf1("SAVE GAME : Get file time for file %s.", mSavedGameInfo[index].Filename);

			m_pCurrentFile = pFile;
			safecpy(m_CurrentFilename,m_pCurrentFile->mFilename,MAX_FILE_NAME_SIZE);

			SAVEGAME.BeginGetFileTimeAndDisplayName(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo[index].DeviceId,
				mSavedGameInfo[index].Filename,mSavedGameInfo[index].DisplayName,MAX_DISPLAY_NAME_SIZE);
		}
	}
}


void SaveGameDataManager::EnumerateSaveGames()
{
	if(m_bDisabled)
	{
		UIDebugf1("SAVE GAME : Saving and Loading disabled");
	}
	else if(m_pActiveGamer->IsSignedIn() == false)
	{
		UIDebugf1("SAVE GAME : Player has signed out!");
		UIMANAGER->SendEvent("filePlayerHasSignedOut");
	}
	else
	{
		mDevicesContainingProfiles.clear();		

		char* filename;

		//default to main save game if no file was given
		if (m_pCurrentFile)
		{
			filename = m_pCurrentFile->mFilename;
		}
		else
		{
			filename = MAIN_FILE_NAME;
		}

		UIDebugf1("SAVE GAME : Enumarating all %s files across all devices",filename);
		for(s32 i=0;i<m_NumProfileFiles;i++)
		{			
			UIDebugf1("SAVE GAME : file = %s , device = %d",mSavedGameInfo[i].Filename,mSavedGameInfo[i].DeviceId);

			if (strcmp(mSavedGameInfo[i].Filename , filename)==0)
			{
				mDevicesContainingProfiles.PushAndGrow(i);
			}
		}
	}
}

void SaveGameDataManager::AddHandlers()
{
	SNUSOCKET.GetPresenceManager()->AddDelegate(&mPresenceDlgt);
}

void SaveGameDataManager::StartupChecks()
{  
	//#if 0 //ENABLE_DONGLE
	//	if (IsDongleEnabled() && !CheckDongle())
	//	{
	//		UIMANAGER->SendEvent("fileDongleError");
	//		FiniteAction action("","DONGLE");
	//		mcUICallbacks::reboot(&action);
	//		return;
	//	}
	//#endif // ENABLE_DONGLE
	if(PARAM_disablesave.Get())
	{
		SetDisabled(true);
		UIDebugf1("SAVE GAME : Saving and Loading disabled");
	}
	else
	{
		m_CurrentState = STARTUP_CHECKS;
		safecpy(m_CurrentFilename,MAIN_FILE_NAME,MAX_FILE_NAME_SIZE);
		UIMANAGER->SendEvent("fileStartupChecksStart");

		//check for a signed in player. On Live platforms the controllers are linked to sign-ins, on other platforms assume only one user signed in (all controllers work)
		//TODO: USE CORRECT INDEX FOR MAPPING OF CONTROLLER
#if __LIVE
		m_pActiveGamer = SNUSOCKET.GetLocalGamer( m_PadIndex );
#else
		m_pActiveGamer = SNUSOCKET.GetLocalGamer( m_PadIndex );
#endif
		if( m_pActiveGamer->IsSignedIn() )
		{
			//m_pActiveGamer->AddNotifyHandler( &mGamerNotifyHandler );
			m_SignInId = m_pActiveGamer->GetLocalGamerIndex();

			// Load/mount downloadable content
#if !__WIN32PC
			//TODO - CHECK FOR DOWNLOADABLE CONTENT
			//const bool cbRebootRequiredImmediately = mc::NotifyStorageUpdated();
			//if (cbRebootRequiredImmediately)
			//{
			//	//TODO: PERFORM SOFT REBOOT
			//}
			//else
#endif
			{
				// Need to set device to XCONTENTDEVICE_ANY, in order to enumerate content
				// accross ALL connected storage devices
				SAVEGAME.SetSelectedDeviceToAny(m_SignInId);
				m_bDeviceSelected=false;
				m_bUserWantsToCreateANewGame = false;
				UIDebugf1("Beginning Enumeration");
				UIMANAGER->SendEvent("fileEnumerateStart");
				ClearSaveDataArray();
				SAVEGAME.BeginEnumeration(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo,MAX_SAVE_GAMES);
			}
		}
		else
		{         
			//SetDisabled(true);
			UIDebugf1("SAVE GAME : No signed in player. Gui needed to tell player that he can't save until he signes in.");
			UIMANAGER->SendEvent("fileNoSignedInPlayer");            
		}
	}
}

void SaveGameDataManager::SetSelectedDeviceToAny()
{
	SAVEGAME.SetSelectedDeviceToAny(m_SignInId);
}

void SaveGameDataManager::ShowDeviceSelectWindow()
{   
	if (SAVEGAME.BeginSelectDevice(m_SignInId,CONTENT_TYPE,0,true,false)) 
	{
		UIMANAGER->SendEvent("fileDeviceSelectionStart");
		m_bIsTryingToShowDeviceSelectWindow = false;

		//hack cause we are in the SAVING state after startup checks but this is a case where we can't save cause device is full and they want to change device
		if (!IsStartupComplete())
		{
			m_CurrentState = STARTUP_CHECKS;
		}
	}
	else
	{
		Errorf("Could not show device select window. Retrying in 1 second.");
		m_bIsTryingToShowDeviceSelectWindow = true;
	}
}

void SaveGameDataManager::ChangeDevice()
{   
	if(m_bDisabled)
	{
		UIDebugf1("SAVE GAME : Saving and Loading disabled");
	}
	else if(m_CurrentState!=IDLE)
	{
		UIDebugf1("SAVE GAME : DROPPING VERIFY SAVE REQUEST, You are currently doing IO");			
	}
	else if(m_pActiveGamer && m_pActiveGamer->IsSignedIn() == false)
	{
		UIDebugf1("SAVE GAME : Player has signed out!");
		UIMANAGER->SendEvent("filePlayerHasSignedOut");
	}
	else 
	{
		if (SAVEGAME.BeginSelectDevice(m_SignInId,CONTENT_TYPE,0,true)) 
		{
			UIMANAGER->SendEvent("fileDeviceChangeStart");
			m_bIsTryingToShowDeviceSelectWindow = false;
			m_bDeviceSelected = false;
			m_CurrentState = CHANGING_DEVICE;
		}
		else
		{
			Errorf("Could not show device select window. Retrying in 1 second.");
			m_bIsTryingToShowDeviceSelectWindow = true;
		}
	}
}

void SaveGameDataManager::ShowSignInWindow()
{   
	m_pActiveGamer->ShowSignInUI(false);
}

void SaveGameDataManager::Update()
{  
	UIScene::Update();

	//#if __XENON && ENABLE_DONGLE
	//	ioDongle::CheckWriteCodeFile();
	//#endif

	f32 dt = TIME.GetWarpedRealtimeSeconds();
	m_SaveTime += dt;

	if (!m_bDisabled)
	{
		//update common items  
		m_DeltaTime += dt;
		if (m_DeltaTime >= DEVICE_POLL_FREQUENCY)
		{
			//keep trying to show the device selector window if it failed.(probably failed cause another window was open)
			if (m_bIsTryingToShowSignInWindow)
			{
				ShowSignInWindow();
			}

			//keep trying to show the device selector window if it failed.(probably failed cause another window was open)
			if (m_bIsTryingToShowDeviceSelectWindow)
			{
				ShowDeviceSelectWindow();
			}

			//check for storage device removal
			if (m_bImmediateMode && SAVEGAME.IsStorageDeviceChanged(m_SignInId))
			{
				UIMANAGER->SendEvent("fileStorageDeviceChanged");
			}

#if __PS3
			//TODO:ENABLE THIS WHEN THE CONTROLLER MAPPINGS ARE FIXED
			//if (m_bIsPassedStartScreen && !ioPad::GetPad(mcConfig::GetPlayer(0).GetController()).IsConnected())
			//{
			//	if(!m_bControllerIsUnplugged)
			//	{
			//		m_bControllerIsUnplugged = true;
			//		UIMANAGER->SendEvent("fileControllerUnplugged");
			//	}
			//}
			//else
			//{
			//	m_bControllerIsUnplugged = false;
			//}
#endif
			m_DeltaTime = 0.0;
		}

		if(sm_TimingAState)
		{
			sm_StateTimeElapsed += dt;
			if (sm_StateTimeElapsed >= sm_StateTimerStop && !IsSaving())
			{
				sm_StateTimeElapsed = 0;
				sm_TimingAState = false;

				//fire the event
				UIMANAGER->SendEvent("fileTimerExpired");
			}
		}

		//update current action items
		switch(m_CurrentState)
		{
		case IDLE:
			return;
		case SAVING:
			UpdateSave(dt);
			break;
		case LOADING:
			UpdateLoad(dt);
			break;
		case GET_FILE_TIME:
			UpdateGetFileTime(dt);
			break;
		case STARTUP_CHECKS:
			UpdateStartupChecks(dt);
			break;
		case VERIFY_SAVE_GAME:
			UpdateVerifySaveGame(dt);
			break;
		case CHANGING_DEVICE:
			UpdateChangingDevice(dt);
			break;			
		default:
			break;
		}
	}
}

void SaveGameDataManager::UpdateGetFileTime(float)
{
	fiSaveGameState::State currentState = SAVEGAME.GetState(m_SignInId);

	switch (currentState)
	{
	case fiSaveGameState::IDLE:
		break;
	case fiSaveGameState::HAD_ERROR:
		HandleError();
		//UIMANAGER->SendEvent("fileGetFileTimeError");       
		break;
	case fiSaveGameState::READING_FILE_TIME:
		SAVEGAME.CheckGetFileTimeAndDisplayName(m_SignInId, m_pCurrentFile->m_Timestamp);
		break;
	case fiSaveGameState::HAVE_READ_FILE_TIME:
		SAVEGAME.EndGetFileTimeAndDisplayName(m_SignInId);
#if __BANK
		if (m_bErrorSet && m_SelectedOperation == SAVEOP_GETFILETIME)
		{
			ThrowTestError();
			return;
		}
#endif
		UIMANAGER->SendEvent("fileGetFileTimeComplete");           
		break;
	default:
		Assertf(0,"Should not ever get here");
		break;
	}
}

void SaveGameDataManager::UpdateSave(float)
{
	bool outIsValid,fileExists;   

	fiSaveGameState::State currentState = SAVEGAME.GetState(m_SignInId);

	switch (currentState)
	{
	case fiSaveGameState::IDLE:
		break;
	case fiSaveGameState::HAD_ERROR:      
		//UIMANAGER->SendEvent("fileSaveError");
		HandleError();
		break;
	case fiSaveGameState::SAVING_CONTENT:
		SAVEGAME.CheckSave(m_SignInId,outIsValid,fileExists);
		break;
	case fiSaveGameState::SAVED_CONTENT:
		SAVEGAME.EndSave(m_SignInId);
#if __BANK
		if (m_bErrorSet && m_SelectedOperation == SAVEOP_SAVE)
		{
			ThrowTestError();
			return;
		}
#endif
		UIDebugf1("SAVE GAME : %s saved successfully.  Took %f ms",m_pCurrentFile->mDisplayName , m_SaveTime);
		UIMANAGER->SendEvent("fileSaveComplete");     
		break;
	case fiSaveGameState::ENUMERATING_CONTENT:
		m_NumProfileFiles = 0;
		SAVEGAME.CheckEnumeration(m_SignInId,m_NumProfileFiles);
		break;
	case fiSaveGameState::ENUMERATED_CONTENT:
		SAVEGAME.EndEnumeration(m_SignInId);
#if __BANK
		if (m_bErrorSet && m_SelectedOperation == SAVEOP_ENUMERATE)
		{
			ThrowTestError();
			return;
		}
#endif
		UIMANAGER->SendEvent("fileEnumerateComplete");           
		break;
	case fiSaveGameState::SAVING_ICON:break;
	case fiSaveGameState::SAVED_ICON:break;
	default:
		Assertf(0,"Should not ever get here");
		break;									
	}
}

void SaveGameDataManager::UpdateLoad(float)
{
	u32 loadSize;
	bool outIsValid;

	fiSaveGameState::State currentState = SAVEGAME.GetState(m_SignInId);

	switch (currentState)
	{
	case fiSaveGameState::IDLE:
		break;
	case fiSaveGameState::HAD_ERROR:
		HandleError();
		//UIMANAGER->SendEvent("fileLoadError");       
		break;
	case fiSaveGameState::LOADING_CONTENT:
		SAVEGAME.CheckLoad(m_SignInId,outIsValid,loadSize);
		break;
	case fiSaveGameState::LOADED_CONTENT:
		SAVEGAME.EndLoad(m_SignInId);
#if __BANK
		if (m_bErrorSet && m_SelectedOperation == SAVEOP_LOAD)
		{
			ThrowTestError();
			return;
		}
#endif
		UIMANAGER->SendEvent("fileLoadComplete");           
		break;
	default:
		Assertf(0,"Should not ever get here");
		break;
	}
}

void SaveGameDataManager::UpdateStartupChecks(float)
{
	fiSaveGameState::State currentState = SAVEGAME.GetState(m_SignInId);

	switch (currentState)
	{
	case fiSaveGameState::IDLE:
		break;
	case fiSaveGameState::ENUMERATING_CONTENT:
		m_NumProfileFiles = 0;
		SAVEGAME.CheckEnumeration(m_SignInId,m_NumProfileFiles);
		break;
	case fiSaveGameState::ENUMERATED_CONTENT:
		SAVEGAME.EndEnumeration(m_SignInId);
#if __BANK
		if (m_bErrorSet && m_SelectedOperation == SAVEOP_ENUMERATE)
		{
			ThrowTestError();
			return;
		}
#endif
		UIMANAGER->SendEvent("fileEnumerateComplete");           
		Assertf(m_NumProfileFiles <= MAX_SAVE_GAMES,"too many files are saved.  increase MAX_SAVE_GAMES");
		break;
	case fiSaveGameState::SELECTING_DEVICE:
		SAVEGAME.CheckSelectDevice(m_SignInId);      
		break;
	case fiSaveGameState::SELECTED_DEVICE:
		SAVEGAME.EndSelectDevice(m_SignInId);
#if __BANK
		if (m_bErrorSet && m_SelectedOperation == SAVEOP_DEVICESELECTION)
		{
			ThrowTestError();
			return;
		}
#endif
		UIMANAGER->SendEvent("fileDeviceSelectionComplete");           
		UIDebugf1("SAVE GAME : Device Selected ");      
		break;
	case fiSaveGameState::HAD_ERROR:
		HandleError();
		break;

	default:
		Assertf(0,"Should not ever get here");
		break;

	}
}

void SaveGameDataManager::UpdateVerifySaveGame(float)
{   
	fiSaveGameState::State currentState = SAVEGAME.GetState(m_SignInId);

	switch (currentState)
	{
	case fiSaveGameState::IDLE:
		break;
	case fiSaveGameState::ENUMERATING_CONTENT:
		SAVEGAME.CheckEnumeration(m_SignInId,m_NumProfileFiles);
		break;
	case fiSaveGameState::ENUMERATED_CONTENT:
		SAVEGAME.EndEnumeration(m_SignInId);
#if __BANK
		if (m_bErrorSet && m_SelectedOperation == SAVEOP_ENUMERATE)
		{
			ThrowTestError();
			return;
		}
#endif
		UIMANAGER->SendEvent("fileEnumerateComplete");           
		Assertf(m_NumProfileFiles <= MAX_SAVE_GAMES,"too many files are saved.  increase MAX_SAVE_GAMES");     
		break;
	case fiSaveGameState::VERIFYING_CONTENT:
		SAVEGAME.CheckContentVerify(m_SignInId,m_bFileVerified);
		break;
	case fiSaveGameState::VERIFIED_CONTENT:
		SAVEGAME.EndContentVerify(m_SignInId);
#if __BANK
		if (m_bErrorSet && m_SelectedOperation == SAVEOP_VERIFYCONTENT)
		{
			ThrowTestError();
			return;
		}
#endif

		if (m_bFileVerified)
		{         
			UIMANAGER->SendEvent("fileSaveGameVerified");			
		}
		else
		{
			UIDebugf1("Verification failed for file %s on device %d",m_pCurrentFile->mFilename,mSavedGameInfo[mDevicesContainingProfiles[0]].DeviceId);
			UIDebugf1("size of mDevicesContainingProfiles is %d",mDevicesContainingProfiles.GetCount());
			//try another device

			//remove the first element in the queue
			//mDevicesContainingProfiles.Delete(0);

			//if (mDevicesContainingProfiles.size() > 0)
			//{
			//	char* filename;

			//	//default to main save game if no file was given
			//	if (m_pCurrentFile)
			//	{
			//		filename = m_pCurrentFile->mFilename;
			//	}
			//	else
			//	{
			//		filename = MAIN_FILE_NAME;
			//	}

			//	UIDebugf1("Trying to verify file %s on device %d",filename,mSavedGameInfo[mDevicesContainingProfiles[0]].DeviceId);

			//	//check the next device that has a save game
			//	SAVEGAME.BeginContentVerify(m_SignInId,CONTENT_TYPE,mSavedGameInfo[mDevicesContainingProfiles[0]].DeviceId,filename);
			//}
			//else
			{
				if (m_pCurrentFile != NULL && SAVEGAME.HasFileBeenAccessed(m_SignInId, m_pCurrentFile->mFilename))
				{
					if (m_pCurrentFile==NULL){UIDebugf1("m_pCurrentFile == NULL");}
					UIMANAGER->SendEvent("fileActiveSaveGameNotFound");  
				}
				else
				{
					UIMANAGER->SendEvent("fileNoFilesExist");
				}
			}
		}
		break;
	case fiSaveGameState::HAD_ERROR:
		HandleError();
		break;
	default:
		Assertf(0,"Should not ever get here");
		break;
	}
}

void SaveGameDataManager::UpdateChangingDevice(float)
{
	fiSaveGameState::State currentState = SAVEGAME.GetState(m_SignInId);

	switch (currentState)
	{
	case fiSaveGameState::IDLE:
		break;
	case fiSaveGameState::SELECTING_DEVICE:
		SAVEGAME.CheckSelectDevice(m_SignInId);      
		break;
	case fiSaveGameState::SELECTED_DEVICE:
		SAVEGAME.EndSelectDevice(m_SignInId);
#if __BANK
		if (m_bErrorSet && m_SelectedOperation == SAVEOP_DEVICESELECTION)
		{
			ThrowTestError();
			return;
		}
#endif
		UIMANAGER->SendEvent("fileDeviceSelectionComplete");           
		UIDebugf1("SAVE GAME : Device Selected ");      
		break;
	case fiSaveGameState::ENUMERATING_CONTENT:
		m_NumProfileFiles = 0;
		SAVEGAME.CheckEnumeration(m_SignInId,m_NumProfileFiles);
		break;
	case fiSaveGameState::ENUMERATED_CONTENT:
		SAVEGAME.EndEnumeration(m_SignInId);
#if __BANK
		if (m_bErrorSet && m_SelectedOperation == SAVEOP_ENUMERATE)
		{
			ThrowTestError();
			return;
		}
#endif
		UIMANAGER->SendEvent("fileEnumerateComplete");           
		Assertf(m_NumProfileFiles <= MAX_SAVE_GAMES,"too many files are saved.  increase MAX_SAVE_GAMES");
		break;
	case fiSaveGameState::HAD_ERROR:
		HandleError();
		break;
	default:
		Assertf(0,"Should not ever get here");
		break;

	}
}

void SaveGameDataManager::HandleError()
{
	SAVEGAME.SetStateToIdle(m_SignInId);

	switch (SAVEGAME.GetError(m_SignInId))
	{
	case fiSaveGame::NOT_ENOUGH_FREE_SPACE_ON_DEVICE:
		UIMANAGER->SendEvent("fileNoFreeSpace");
		break;
	case fiSaveGame::USER_CANCELLED_DEVICE_SELECTION:
		UIMANAGER->SendEvent("fileUserCanceledDeviceSelection");
		break;
	case fiSaveGame::FILE_CORRUPT:
		Displayf("The save game is corrupt!!");
		UIMANAGER->SendEvent("fileFileCorrupt");         
		break;
	case fiSaveGame::PATH_NOT_FOUND:
		Displayf("Path not found!!");
		UIMANAGER->SendEvent("fileUnexpectedError");         
		break;
	case fiSaveGame::DEVICE_CORRUPT:
		Displayf("fiSaveGame::DEVICE_CORRUPT!!");
		UIMANAGER->SendEvent("fileUnexpectedError");         
		break;
	case fiSaveGame::COULD_NOT_MAP_DEVICE:
		UIMANAGER->SendEvent("fileStorageDeviceInvalid");         		
		break;
	case fiSaveGame::THE_DEVICE_IS_NOT_READY:
		UIMANAGER->SendEvent("fileStorageDeviceInvalid");         
		break;
	default:
		Displayf("UNEXPECTED ERROR!!");
		UIMANAGER->SendEvent("fileUnexpectedError");
		break;
	}

	UIMANAGER->SendEvent("fileErrorOccured");
}

void SaveGameDataManager::OnPresenceEvent(rlPresence* /*presenceMgr*/, const rlPresenceEvent* evt)
{  
	//	if (!m_bDisabled)
	{
		switch(evt->GetId())
		{
		case PRESENCE_EVENT_SIGNIN_STATUS_CHANGED:
			if(evt->m_SigninStatusChanged->SignedOut())
			{
				if (m_pActiveGamer->GetGamerInfo() == evt->m_GamerInfo)
				{
					UIMANAGER->SendEvent("filePlayerHasSignedOut");
					m_bDeviceSelected = false;
				}
			}
			else if(evt->m_SigninStatusChanged->SignedIn())
			{
				if (m_pActiveGamer->GetGamerInfo() == evt->m_GamerInfo)
				{
					//active gamer has signed in.  Gui needed to ask if he wants to initialize everything with his profile
					UIDebugf1("SAVE GAME : you have signed in.  Gui needed to ask if he wants to initialize everything with his profile");
					UIMANAGER->SendEvent("filePlayerHasSignedIn");
				}
				else
				{
					UIDebugf1("SAVE GAME : secondary (non-active) player signed in");
				}
			}
			break;
		default:
			break;
		}
	}
}

void SaveGameDataManager::OnFileStartupEnumerationComplete()
{
	//load the save game. note that this function needs to be after state is set to idle
	if (!m_bSavingNewGame)
	{
		SetState(IDLE);
		LoadData();
	}
	else
	{
		m_bSavingNewGame = false;
		UIMANAGER->SendEvent("fileSaveSuccess");
		UIMANAGER->SendEvent("fileNewSaveCreated", NULL );

		SetState(IDLE);
	}
}

void SaveGameDataManager::OnLoadComplete()
{
	m_pCurrentFile->DeserializeData();
}

void SaveGameDataManager::OnFileEnumerationComplete()
{

}

void SaveGameDataManager::OnGetFileTimeComplete()
{

}

void SaveGameDataManager::OnCreateNewSaveGame(const char* filename)
{
	SaveData(filename ? filename : MAIN_FILE_NAME,false);//create the file. note that this function needs to be after state is set to idle
}

bool SaveGameDataManager::AreAllSaveFilesCreated() const
{
	u32 numFilesOnDevice = 0;
	for (u32 i = 0; i < m_NumSaveGameFiles; ++i)
	{
		for (int j = 0; j < m_NumProfileFiles; ++j)
		{
			if (!stricmp(mSavedGameInfo[j].Filename, m_pSaveGameFiles[i]->mFilename))
			{
				++numFilesOnDevice;
				break;
			}
		}
	}

	return numFilesOnDevice == m_NumSaveGameFiles;
}

void SaveGameDataManager::CreateNextSaveFile()
{
	int missingFile = -1;
	for (u32 i = 0; i < m_NumSaveGameFiles; ++i)
	{
		int j = 0;
		for (j = 0; j < m_NumProfileFiles; ++j)
		{
			if (!stricmp(mSavedGameInfo[j].Filename, m_pSaveGameFiles[i]->mFilename))
				break;
		}

		if (j == m_NumProfileFiles)
		{
			missingFile = i;
			break;
		}
	}

	Assert(missingFile != -1);
	SetState(IDLE);
	m_bSavingNewGame = true;
	OnCreateNewSaveGame(m_pSaveGameFiles[missingFile]->mFilename);
}

bool SaveGameDataManager::HandleEvents( fuiEvent* pEvent)
{
	bool		eventConsumed		= false;

	//let children handle the event first
	eventConsumed |= UIObject::HandleEvents(pEvent);

	//lets see if we want to handle the event
	if(eventConsumed)
		return true;

	if (pEvent == FUIEVENTMGR->GetEvent("fileDisableAutosave"))
	{
		SetStateToIdle();//since we may be in a state other than idle
		SetAutoSaveDisabled(true);
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileNoDeviceSelected"))
	{
		ClearSaveDataArray();
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileLoad"))
	{
		LoadData();
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileLoadCurrent"))
	{
		LoadData(m_pCurrentFile);
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileSave"))
	{
		SaveData(MAIN_FILE_NAME,true);
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileSaveCurrent"))
	{
		SaveData(m_pCurrentFile,true);
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileEnableAutoSave"))
	{
		SetAutoSaveDisabled(false);
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileDisableLoad"))
	{
		bDisableLoad = true;
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileSelectDevice"))
	{
		ShowDeviceSelectWindow();
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileChangeDevice"))
	{
		if (IsStartupComplete())
		{
			ChangeDevice();
		}
		else
		{
			//do the simple device selection
			ShowDeviceSelectWindow();
		}
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileSetSelectedDeviceToAny"))
	{
		SetSelectedDeviceToAny();		
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileSaveInSelectedSlot"))
	{
		SaveData(m_pCurrentFile, true);
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileUserWantsToLoadSelectedGame"))
	{
		LoadData(m_pCurrentFile);
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileAllSavesCreated"))
	{
		if (!m_bStartupChecksComplete)
			OnFileStartupEnumerationComplete();
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileCheckSignedIn"))
	{
		if (!IsSignedIn())
		{
			UIMANAGER->SendEvent("fileNoSignedInPlayer");	
		}
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileCheckSaveDisabled"))
	{
		if (IsDisabled())
		{
			UIMANAGER->SendEvent("fileAutoSaveDisabled");	
		}
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileSetPassedStartScreen"))
	{
		{
			SetIsPassedStartScreen(true);
		}
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileStopTimer"))
	{
		{
			StopTimer();
		}
	}

	//this if is for dev purposes only
	if (pEvent == FUIEVENTMGR->GetEvent("filePlayerLoaded"))
	{
		if(m_bDisabled)
		{
			UIMANAGER->SendEvent( "fileNoSaveEvent" );
			UIMANAGER->SendEvent("fileDisableSaving");
		}
	}
	if (!m_bDisabled)
	{
		//handle common events
		if (pEvent==FUIEVENTMGR->GetEvent("fileStorageDeviceChanged"))
		{
			if (m_bImmediateMode)
			{
				UIDebugf1("SAVE GAME : Storage device has changed. GUI needed to tell user of the consequences");

				//verify that the save game is still there
				UIMANAGER->SendEvent("fileVerifySaveGame");
			}
		}
		else if(pEvent==FUIEVENTMGR->GetEvent("fileUserWantsToCreateNewSaveGame"))
		{
			UIDebugf1("SAVE GAME : Player wants to create a new save game");
			UIMANAGER->SendEvent("fileCreateNewSaveGame"); 

			m_bUserWantsToCreateANewGame = true;

			if (m_bDeviceSelected)
			{
				//if user decides to create a new profile, bring up device selection to save profile to
				UIDebugf1("SAVE GAME : User has chosen to create a new save game on this device1");     

				//user has already chosen a device to save to
				SetState(IDLE);
				m_bSavingNewGame = true;
				OnCreateNewSaveGame();
			}
			else
			{
				m_CurrentState = STARTUP_CHECKS;
				ShowDeviceSelectWindow();
			}
		}
		else if (pEvent==FUIEVENTMGR->GetEvent("fileUnexpectedError"))
		{
			UIDebugf1("SAVE GAME : Unexpected Error");
		}
		else if (pEvent==FUIEVENTMGR->GetEvent("fileStartPlayerChecks"))
		{
			m_bStartupChecksComplete = false;
			SetDoingFreeSpaceCheck(false);

			//kick off the startup checks
			StartupChecks();
		}
		else if (pEvent==FUIEVENTMGR->GetEvent("fileDoFreeSpaceCheck"))
		{
			SetDoingFreeSpaceCheck(true);

			//kick off the startup checks
			StartupChecks();
		}
		else if(m_CurrentState != STARTUP_CHECKS && pEvent==FUIEVENTMGR->GetEvent("fileStartupChecksComplete"))
		{   
			UIDebugf1("SAVE GAME : Startup Checks Complete");

			m_bStartupChecksComplete = true;
			m_bIsSaveSuccessful = true;
		}
		else if (pEvent==FUIEVENTMGR->GetEvent("fileUserWantsToSignIn"))
		{
			ShowSignInWindow();         
		}
		else if (pEvent==FUIEVENTMGR->GetEvent("fileVerifySaveGame"))
		{
			UIDebugf1("SAVE GAME : Starting to verify file");
			if(m_bDisabled)
			{
				UIDebugf1("SAVE GAME : Saving and Loading disabled");
			}
			else if(PARAM_nosave.Get())
			{
				UIDebugf1("SAVE GAME : -nosave found, Saving disabled");
				UIMANAGER->SendEvent("fileSaveSuccess");
			}
			else if(m_CurrentState!=IDLE)
			{
				UIDebugf1("SAVE GAME : DROPPING VERIFY SAVE REQUEST, You are currently doing IO");			
			}
			else if(m_pActiveGamer && m_pActiveGamer->IsSignedIn())
			{
				//set the device to any, just in case the memory card was removed and re-inserted
				//SAVEGAME.SetSelectedDeviceToAny(m_SignInId);
				ClearSaveDataArray();
				SAVEGAME.BeginEnumeration(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo,MAX_SAVE_GAMES);

				m_CurrentState = VERIFY_SAVE_GAME;
			}
			//else
			//{
			//	UIMANAGER->SendEvent("filePlayerHasSignedOut");
			//}
		}
		else if (pEvent == FUIEVENTMGR->GetEvent("fileSaveGame"))
		{         
			UIMANAGER->SendEvent("fileSaveStart");

			//if no file specified, then default to the main save file
			if (m_pCurrentFile==NULL)
			{
				m_pCurrentFile = GetFile(MAIN_FILE_NAME);
			}
			SetState(IDLE);
			m_pCurrentFile->SaveData();

			//start saving
			char16 displayName[MAX_DISPLAY_NAME_SIZE];
			ConstructDisplayName(displayName, sizeof(displayName));
			WideToUtf8(m_pCurrentFile->mDisplayName, displayName, MAX_DISPLAY_NAME_SIZE);

			UIDebugf1("SAVE GAME : Saving file %s",m_pCurrentFile->mDisplayName );

			m_CurrentState = SAVING;

			SAVEGAME.BeginSave(m_SignInId,(unsigned)CONTENT_TYPE,displayName,m_pCurrentFile->mFilename,
				m_pCurrentFile->mpBuffer,m_pCurrentFile->m_BufferSize,true);
		}
		else if (pEvent == FUIEVENTMGR->GetEvent("loadComplete"))
		{
		}
		else if(pEvent==FUIEVENTMGR->GetEvent("fileNoFreeSpace"))
		{
			UIMANAGER->SendEvent("fileNoRoomToSave"); 
		}


		//handle current action events
		switch(m_CurrentState)
		{
		case IDLE:
			return false;
		case SAVING:
			HandleSaveEvents(pEvent);
			break;
		case LOADING:
			HandleLoadEvents(pEvent);
			break;
		case GET_FILE_TIME:
			HandleGetFileTimeEvents(pEvent);
			break;
		case STARTUP_CHECKS:
			HandleStartupChecksEvents(pEvent);
			break;
		case VERIFY_SAVE_GAME:
			HandleVerifySaveGameEvents(pEvent);
			break;
		case CHANGING_DEVICE:
			HandleChangingDeviceEvents(pEvent);
			break;
		default:
			break;         
		}
	}
	return false;
}

bool SaveGameDataManager::HandleGetFileTimeEvents( fuiEvent* pEvent)
{
	if (pEvent==FUIEVENTMGR->GetEvent("fileGetFileTimeStart"))
	{
		UIDebugf1("SAVE GAME : Get File Time Starting");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileStorageDeviceInvalid"))
	{
		UIDebugf1("fileStorageDeviceInvalid");
		UIMANAGER->SendEvent("fileSaveGameRemovedOnLoad");         
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileFileCorrupt"))
	{
		SetState(IDLE);
		SAVEGAME.SetStateToIdle(m_SignInId);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileGetFileTimeComplete"))
	{
		UIMANAGER->SendEvent("fileGetFileTimeSuccess");

		SetState(IDLE);

		OnGetFileTimeComplete();
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileErrorOccured"))
	{
		UIDebugf1("SAVE GAME : Error getting file time");

		if (IsStorageDeviceRemoved() || IsStorageDeviceChanged())
		{
			UIMANAGER->SendEvent("fileSaveGameRemovedOnLoad");
		}
		else
		{
			UIMANAGER->SendEvent("fileGetFileTimeError");
		}

		SetState(IDLE);
		SAVEGAME.SetStateToIdle(m_SignInId);
	}

	return false;
}

bool SaveGameDataManager::HandleLoadEvents( fuiEvent* pEvent)
{
	if (pEvent==FUIEVENTMGR->GetEvent("fileLoadStart"))
	{
#if 0//ENABLE_DONGLE
		if (!CheckDongle())
		{
			UIMANAGER->SendEvent("fileDongleError");
			return false;
		}
#endif // ENABLE_DONGLE
		UIDebugf1("SAVE GAME : Load Starting");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileStorageDeviceInvalid"))
	{
		UIDebugf1("fileStorageDeviceInvalid");
		UIMANAGER->SendEvent("fileSaveGameRemovedOnLoad");         
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileFileCorrupt"))
	{
		m_pCurrentFile->mLoadRequested = false;

		//error loading a file from a specific device. so lets work with this device and either save over the file or create a new one
		SAVEGAME.SetSelectedDevice(m_SignInId,mSavedGameInfo[mDevicesContainingProfiles[0]].DeviceId);         
		m_PreviousStorageDevice = m_CurrentStorageDevice = mSavedGameInfo[mDevicesContainingProfiles[0]].DeviceId;
		m_bDeviceSelected = true;
		m_bStartupChecksComplete = true;

		SetState(IDLE);
		SAVEGAME.SetStateToIdle(m_SignInId);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileLoadComplete"))
	{
		OnLoadComplete();

		m_pCurrentFile->mLoadRequested = false;

		UIMANAGER->SendEvent("fileLoadSuccess");            

		m_bSaveGameCreated = true;
		UIDebugf1("SAVE GAME : %s loaded successfully.",m_pCurrentFile->mDisplayName);

		m_pCurrentFile->m_bLoaded = true;
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileErrorOccured"))
	{
		UIDebugf1("SAVE GAME : Error Loading");

		if (IsStorageDeviceRemoved() || IsStorageDeviceChanged())
		{
			UIMANAGER->SendEvent("fileSaveGameRemovedOnLoad");
		}
		else
		{
			UIMANAGER->SendEvent("fileLoadError");
		}

		m_pCurrentFile->mLoadRequested = false;

		//error loading a file from a specific device. so lets work with this device and either save over the file or create a new one
		SAVEGAME.SetSelectedDevice(m_SignInId,mSavedGameInfo[mDevicesContainingProfiles[0]].DeviceId);         
		m_PreviousStorageDevice = m_CurrentStorageDevice = mSavedGameInfo[mDevicesContainingProfiles[0]].DeviceId;
		m_bDeviceSelected = true;
		m_bStartupChecksComplete = true;

		SetState(IDLE);
		SAVEGAME.SetStateToIdle(m_SignInId);

	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateStart"))
	{
		UIDebugf1("SAVE GAME : Enumerate Starting");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateComplete"))
	{
		OnFileEnumerationComplete();

		UIMANAGER->SendEvent("fileEnumerateSuccess");            

		UIDebugf1("SAVE GAME : Done checking for data. %d files found",m_NumProfileFiles);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateError"))
	{
		UIDebugf1("SAVE GAME : Enumerate Error");
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileNoFilesExist"))
	{
		UIDebugf1("SAVE GAME : There are no files to load");
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileDongleError"))
	{
		UIDebugf1("SAVE GAME : Dongle Error");
	}

	return false;
}

bool SaveGameDataManager::HandleSaveEvents( fuiEvent* pEvent)
{
	if(pEvent==FUIEVENTMGR->GetEvent("fileSaveComplete"))
	{
		m_pCurrentFile->m_bSaveRequested = false;
		m_bSaveGameCreated = true;
		m_bIsSaving = false;
		m_bIsSaveSuccessful = true;		

		//enumerate all files so we are in sync with what is now on file
		UIMANAGER->SendEvent("fileEnumerateStart");		
		ClearSaveDataArray();
		SAVEGAME.BeginEnumeration(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo,MAX_SAVE_GAMES);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileStorageDeviceInvalid"))
	{
		UIMANAGER->SendEvent("fileStorageDeviceChanged");         
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileFileCorrupt"))
	{
		//UIMANAGER->SendEvent("fileUnexpectedSaveError");
		Displayf("SAVE GAME : Error saving %s",m_pCurrentFile->mDisplayName);
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileErrorOccured"))
	{
		if (IsStorageDeviceRemoved() || IsStorageDeviceChanged())
		{
			if (IsStorageDeviceRemoved()){UIDebugf1("IsStorageDeviceRemoved()==true");}
			else if(IsStorageDeviceChanged() ){UIDebugf1("IsStorageDeviceChanged()== true");}

			UIMANAGER->SendEvent("fileSaveGameRemoved");
		}
		else if (m_bSavingNewGame)
		{
			UIMANAGER->SendEvent("fileCreateNewSaveGameError");
		}
		else
		{
			UIMANAGER->SendEvent("fileSaveError");
		}

		UIDebugf1("SAVE GAME : Error saving %s",m_pCurrentFile->mDisplayName);

		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileNoFilesExist"))
	{
		UIDebugf1("SAVE GAME : There are no files on device");
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateStart"))
	{
		UIDebugf1("SAVE GAME : Enumerate Starting");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateComplete"))
	{
		OnFileEnumerationComplete();

		UIDebugf1("SAVE GAME : Enumerate Complete");
		SetState(IDLE);

		UIMANAGER->SendEvent("fileSaveSuccess");

		if (m_bSavingNewGame)
		{
			UIMANAGER->SendEvent("fileNewSaveCreated", NULL );
			m_bSavingNewGame = false;

			if (!strcmp(m_pCurrentFile->mFilename, MAIN_FILE_NAME))
				m_bJustCreatedMainSave = true;

			if (!AreAllSaveFilesCreated())
				CreateNextSaveFile();
			else
				UIMANAGER->SendEvent("fileAllSavesCreated", NULL );
		}
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateError"))
	{
		UIDebugf1("SAVE GAME : Enumerate Error");
		SetState(IDLE);
	}
	return false;
}

bool SaveGameDataManager::HandleCreateSaveEvents( fuiEvent* pEvent )
{
	if(pEvent==FUIEVENTMGR->GetEvent("fileSaveComplete"))
	{
		UIMANAGER->SendEvent("fileSaveSuccess");

		m_pCurrentFile->m_bSaveRequested = false;

		m_bSaveGameCreated = true;
		UIDebugf1("SAVE GAME : %s created successfully.",m_pCurrentFile->mDisplayName);

		//enumerate all files so we are in sync with what is now on file
		UIMANAGER->SendEvent("fileEnumerateStart");
		ClearSaveDataArray();
		SAVEGAME.BeginEnumeration(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo,MAX_SAVE_GAMES);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileStorageDeviceInvalid"))
	{
		UIMANAGER->SendEvent("fileStorageDeviceChanged");         
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileFileCorrupt"))
	{
		UIMANAGER->SendEvent("fileSaveError");
		UIDebugf1("SAVE GAME : Error creating %s",m_pCurrentFile->mDisplayName);
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileSaveError"))
	{
		//UIMANAGER->SendEvent("fileSaveError");

		UIDebugf1("SAVE GAME : Error creating %s",m_pCurrentFile->mDisplayName);
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateStart"))
	{
		UIDebugf1("SAVE GAME : Enumerate Starting");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateComplete"))
	{
		OnFileEnumerationComplete();

		UIDebugf1("SAVE GAME : Enumerate Complete");
		SetState(IDLE);

		UIMANAGER->SendEvent("fileNewSaveCreated", NULL );
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateError"))
	{
		UIDebugf1("SAVE GAME : Enumerate Error");

		SetState(IDLE);
	}
	return false;
}

bool SaveGameDataManager::HandleStartupChecksEvents( fuiEvent* pEvent)
{
	if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateStart"))
	{
		//#if ENABLE_DONGLE
		//		if (!CheckDongle())
		//		{
		//			UIMANAGER->SendEvent("fileDongleError");
		//			return false;
		//		}
		//#endif // ENABLE_DONGLE
		UIDebugf1("SAVE GAME : Enumerate Starting");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateComplete"))
	{
		UIMANAGER->SendEvent("fileEnumerateSuccess");            

		UIDebugf1("SAVE GAME : Done checking for data. %d files found",m_NumProfileFiles);

		//gather all save games across all device for the signed in player
		EnumerateSaveGames();

		if(m_NumProfileFiles==0)
		{
			if (m_bUserWantsToCreateANewGame)
			{
				//if user decides to create a new profile, bring up device selection to save profile to
				UIDebugf1("SAVE GAME : User has chosen to create a new save game on this device2");     

				//user has already chosen a device to save to
				if (HasRoomToSaveAllFiles() == false)//check for room here so we can stay in startup checks state if failed
				{
					UIMANAGER->SendEvent("fileNoFreeSpace");
					UIDebugf1("SAVE GAME : No room on current storage device!");
				}
				else
				{
					SetState(IDLE);
					m_bSavingNewGame = true;
					OnCreateNewSaveGame();
				}
			}
			else
			{
				//lets not worry about this until the first save
				UIMANAGER->SendEvent("fileNoFilesExist");
			}
		}
		else if(DoesProfileExist()==false)
		{
			if (m_bUserWantsToCreateANewGame)
			{
				//if user decides to create a new profile, bring up device selection to save profile to
				UIDebugf1("SAVE GAME : User has chosen to create a new save game on this device3");     

				//user has already chosen a device to save to
				if (HasRoomToSaveAllFiles() == false)//check for room here so we can stay in startup checks state if failed
				{
					UIMANAGER->SendEvent("fileNoFreeSpace");
					UIDebugf1("SAVE GAME : No room on current storage device!");
				}
				else
				{
					SetState(IDLE);
					m_bSavingNewGame = true;
					OnCreateNewSaveGame();
				}
			}
			else
			{
				//lets not worry about this until the first save
				UIMANAGER->SendEvent("fileNoFilesExist");
			}
		}
		else if(DoMultipleProfilesExist()==true)
		{
			UIMANAGER->SendEvent("fileMultipleProfilesExist");
		}
		else
		{
			if (m_bUserWantsToCreateANewGame)
			{
				UIMANAGER->SendEvent("fileOverwriteConfirmation");
			}
			else
			{
				//mDevicesContainingProfiles should only contain 1 save game if we get into this block
				if (!SAVEGAME.IsCurrentDeviceValid(m_SignInId))
				{
					SAVEGAME.SetSelectedDevice(m_SignInId,mSavedGameInfo[mDevicesContainingProfiles[0]].DeviceId);         
					m_PreviousStorageDevice = m_CurrentStorageDevice = mSavedGameInfo[mDevicesContainingProfiles[0]].DeviceId;
					SAVEGAME.BeginEnumeration(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo,MAX_SAVE_GAMES);
				}
				else
				{
#if !__FINAL
					char displayName[MAX_DISPLAY_NAME_SIZE*6];//probably never encounter a 6 byte char but playing it safe
					WideToUtf8(displayName,mSavedGameInfo[mDevicesContainingProfiles[0]].DisplayName,MAX_DISPLAY_NAME_SIZE);
					UIDebugf1("SAVE GAME : Found profile %s on device %d",displayName, mSavedGameInfo[mDevicesContainingProfiles[0]].DeviceId); 
#endif // !__FINAL

					if (HasRoomToSaveAllFiles() == false)//check for room here so we can stay in startup checks state if failed
					{
						UIMANAGER->SendEvent("fileNoFreeSpace");
						UIDebugf1("SAVE GAME : No room on current storage device!");
					}
					else
					{
						if (AreAllSaveFilesCreated())
							OnFileStartupEnumerationComplete();
						else
							CreateNextSaveFile();
					}
				}
			}
		}
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateError"))
	{		
		UIDebugf1("SAVE GAME : There was an error with the device.  Gui needed here to tell user that something is thrashed");
		UIMANAGER->SendEvent("fileStartupChecksError");
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileNoFilesExist"))
	{
		if (m_bDeviceSelected && m_bMultipleGamesFound == false)
		{
			UIMANAGER->SendEvent("fileUserWantsToCreateNewSaveGame");
		}
		else 
		{
			//if user decides to create a new profile, bring up device selection to save profile to
			UIDebugf1("SAVE GAME : There are no save games found.");     
			UIMANAGER->SendEvent("fileNoGamesFound");            
		}
		//dont set to idle since we need to handle the evetns sent in this block
		//SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileMultipleProfilesExist"))
	{
		m_bMultipleGamesFound = true;
		UIDebugf1("SAVE GAME : There are multiple devices that contain a save game. please choose which device to use.Gui needed.");
		UIMANAGER->SendEvent("fileMultipleGamesFound");            
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileSelectSaveGame"))
	{
		//start selecting a device
		ShowDeviceSelectWindow();   
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileDeviceSelectionComplete"))
	{
		if (SAVEGAME.IsCurrentDeviceValid(m_SignInId)==false)
		{
			SetState(IDLE);
			UIMANAGER->SendEvent("fileUserCanceledDeviceSelection");
		}
		else
		{
			m_bDeviceSelected = true;

			UIMANAGER->SendEvent("fileDeviceSelectionSuccess");

			//re-enumerate just on this device
			ClearSaveDataArray();
			SAVEGAME.BeginEnumeration(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo,MAX_SAVE_GAMES);
		}
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileDeviceSelectionError"))
	{
		HandleError();
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileStartupChecksStart"))
	{
		//UIMANAGER->SendEvent("fileStartupChecksStart");
		UIDebugf1("SAVE GAME : Beginning startup Checks");

		UIObject* pSaveGame_DoNotInterruptMe = UIMANAGER->GetUIObject("SaveGame_DoNotInterruptMe");
		pSaveGame_DoNotInterruptMe->Activate();
		pSaveGame_DoNotInterruptMe->SetVisible(true);
		pSaveGame_DoNotInterruptMe->SetFocused(true);

		UIObject* pSaveGameIO = UIMANAGER->GetUIObject("SaveGameIO");
		pSaveGameIO->Activate();
		pSaveGameIO->SetVisible(true);
		pSaveGameIO->SetFocused(true);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileStartupChecksComplete"))
	{   
		UIDebugf1("SAVE GAME : Startup Checks Complete");
		SAVEGAME.SetStateToIdle(m_SignInId);
		SetState(IDLE);

		m_bStartupChecksComplete = true;
		m_bIsSaveSuccessful = true;
		m_bUserWantsToCreateANewGame = false;
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileStartupChecksError"))
	{
		//UIMANAGER->SendEvent("fileStartupChecksError");
		UIDebugf1("SAVE GAME : Error during startup checks");
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileUnexpectedError"))
	{
		UIMANAGER->SendEvent("fileStartupChecksError");
		UIDebugf1("SAVE GAME : Error during startup checks");
		SetState(IDLE);
	}
	return false;
}

bool SaveGameDataManager::HandleVerifySaveGameEvents( fuiEvent* pEvent)
{
	if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateStart"))
	{
		UIDebugf1("SAVE GAME : Enumerate Starting");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateComplete"))
	{
		UIMANAGER->SendEvent("fileEnumerateSuccess");            

		UIDebugf1("SAVE GAME : Done checking for data for device change. %d files found",m_NumProfileFiles);

		//gather all save games across all device for the signed in player
		EnumerateSaveGames();

		if (mDevicesContainingProfiles.GetCount() > 0)
		{
			UIDebugf1("Verifying file %s on device %d",m_pCurrentFile->mFilename,mSavedGameInfo[mDevicesContainingProfiles[0]].DeviceId);
			UIDebugf1("size of mDevicesContainingProfiles is %d",mDevicesContainingProfiles.GetCount());
			//check the next device that has a save game
			SAVEGAME.BeginContentVerify(m_SignInId,CONTENT_TYPE,mSavedGameInfo[mDevicesContainingProfiles[0]].DeviceId,m_pCurrentFile->mFilename);
		}
		else
		{			
			if (m_pCurrentFile != NULL && SAVEGAME.HasFileBeenAccessed(m_SignInId, m_pCurrentFile->mFilename))
			{
				if (m_pCurrentFile==NULL)
				{
					UIDebugf1("m_pCurrentFile==NULL");
				}
				else
				{
					UIDebugf1("file %s has not been accessed?",m_pCurrentFile->mFilename);
				}
				UIMANAGER->SendEvent("fileActiveSaveGameNotFound");  
			}
			else
			{
				UIDebugf1("WTF where is the file?!");
				UIMANAGER->SendEvent("fileNoFilesExist");
			}
			//dont set to idle since we need to handle the events sent in this block
			//SetState(IDLE);
		}
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileUnexpectedError"))
	{
		Displayf("SAVE GAME : There was an error with the device.");
		//UIMANAGER->SendEvent("fileUnexpectedSaveError");
		SetState(IDLE);
		SAVEGAME.SetStateToIdle(m_SignInId);

	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileStorageDeviceInvalid"))
	{
		UIMANAGER->SendEvent("fileStorageDeviceChanged");         
		SetState(IDLE);
		SAVEGAME.SetStateToIdle(m_SignInId);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileFileCorrupt"))
	{
		UIMANAGER->SendEvent("fileSaveError");
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileActiveSaveGameNotFound"))
	{
		UIDebugf1("SAVE GAME : Your save game has been removed.  please replace it1");
		UIMANAGER->SendEvent("fileVerifyAndSaveFail");
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileSaveGameVerified"))
	{
		UIDebugf1("SAVE GAME : Your save game has been verified");      

#if !__FINAL
		char displayName[MAX_DISPLAY_NAME_SIZE*6];//probably never encounter a 6 byte char but playing it safe
		WideToUtf8(displayName,mSavedGameInfo[mDevicesContainingProfiles[0]].DisplayName,MAX_DISPLAY_NAME_SIZE);
		UIDebugf1("SAVE GAME : Found profile %s on device %d",displayName, mSavedGameInfo[mDevicesContainingProfiles[0]].DeviceId); 
#endif // !__FINAL

		//use this device since the previously set device may be invalid from removal and reinserting of mem card
		SAVEGAME.SetSelectedDevice(m_SignInId,mSavedGameInfo[mDevicesContainingProfiles[0]].DeviceId);

		SetState(IDLE);
		UIMANAGER->SendEvent("fileSaveGameVerifySuccess");            
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileNoFilesExist"))
	{
		if (m_pCurrentFile->m_bLoaded)
		{
			UIDebugf1("m_pCurrentFile->m_bLoaded==true");
			UIMANAGER->SendEvent("fileSaveGameRemoved");                  
		}
		else
		{
			UIMANAGER->SendEvent("fileNoGamesFound");                  
		}
		//dont set to idle since we need to handle the events sent in this block
		//SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileUserWantsToCreateNewSaveGame"))
	{
		UIDebugf1("SAVE GAME : Player wants to crate a new save game");
		UIMANAGER->SendEvent("fileCreateNewSaveGame"); 

		if (m_bDeviceSelected)
		{
			//if user decides to create a new profile, bring up device selection to save profile to
			UIDebugf1("SAVE GAME : User has chosen to create a new save game on this device4");     

			//user has already chosen a device to save to
			SetState(IDLE);
			m_bSavingNewGame = true;
			SaveData(MAIN_FILE_NAME,false);//create the file. note that this function needs to be after state is set to idle        			
		}
		else
		{
			ShowDeviceSelectWindow();
		}
	}
	return false;
}

bool SaveGameDataManager::HandleChangingDeviceEvents( fuiEvent* pEvent )
{
	if(pEvent==FUIEVENTMGR->GetEvent("fileDeviceSelectionComplete"))
	{
		if (SAVEGAME.IsCurrentDeviceValid(m_SignInId)==false)
		{
			UIMANAGER->SendEvent("fileUserCanceledDeviceSelection"); 
		}
		else
		{
			m_bDeviceSelected = true;			
			if (m_pCurrentFile){m_pCurrentFile->m_bLoaded = false;}//this is in case the file on the new/same device is corrupt and we need to resave
			m_PreviousStorageDevice = m_CurrentStorageDevice;
			m_CurrentStorageDevice = SAVEGAME.GetSelectedDevice(m_SignInId);

			if (m_PreviousStorageDevice != m_CurrentStorageDevice)
			{				
				//re-enummerate the files
				m_CurrentState = CHANGING_DEVICE;//lets stay in the CHANGING_DEVICE state
				ClearSaveDataArray();
				SAVEGAME.BeginEnumeration(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo,MAX_SAVE_GAMES);
			}
			else
			{
				SetState(IDLE);
				UIMANAGER->SendEvent("fileDeviceSelectionSuccess");
			}
		}

	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileUserCanceledDeviceSelection"))
	{
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateComplete"))
	{
		SetState(IDLE);
		OnFileEnumerationComplete();

		if (HasRoomToSaveAllFiles() == false)//do we care about the photo album space requirements?
		{
			//lets revert to the previously selected device for now
			m_CurrentStorageDevice = m_PreviousStorageDevice;
			SAVEGAME.SetSelectedDevice(m_SignInId,m_CurrentStorageDevice);

			UIMANAGER->SendEvent("fileFullDeviceSelected"); 
			UIDebugf1("SAVE GAME : No room on current storage device!");
		}
		else
		{
			UIMANAGER->SendEvent("fileDeviceSelectionSuccess");
		}		
	}

	return false;
}

bool SaveGameDataManager::IsStorageDeviceRemoved()
{
	return SAVEGAME.IsStorageDeviceRemoved(m_SignInId);
}

bool SaveGameDataManager::IsStorageDeviceChanged()
{
	return SAVEGAME.IsStorageDeviceChanged(m_SignInId);
}

bool SaveGameDataManager::DoMultipleProfilesExist()
{
	return (mDevicesContainingProfiles.size()>1);
}

bool SaveGameDataManager::DoesProfileExist()
{
	for (int i=0;i<m_NumProfileFiles;i++)
	{
		if (strcmp(m_CurrentFilename,mSavedGameInfo[i].Filename)==0)
		{
			return true;
		}
	}

	return false;
}

// PURPOSE: Calculate the total space needed to save the given file
// PARAMS:  file - The file to query.  Defaults to the main save game file
// RETURNS: The space needed in bytes
u32 SaveGameDataManager::GetTotalSpaceNeededToSave(const char* pFilename)
{
	SaveGameFile* pFile = GetFile(pFilename);

	return GetTotalSpaceNeededToSave(pFile);
}

// PURPOSE: Calculate the total space needed to save the given file
// PARAMS:  file - The file to query.  Defaults to the main save game file
// RETURNS: The space needed in bytes
u32 SaveGameDataManager::GetTotalSpaceNeededToSave(SaveGameFile* pFile)
{   
	if (pFile)
	{
		u32 size = pFile->GetTotalSize();

		//add any overhead that the system may contain to the size
		return (u32)SAVEGAME.CalculateDataSizeOnDisk(m_SignInId,size);
	}
	else
	{
		return 0;
	}
}

// PURPOSE: Calculate the total space needed to save all files (save game, stats, photos, ect)   
// RETURNS: The space needed in bytes
u32 SaveGameDataManager::GetTotalSpaceNeededToSaveAllFiles()
{
	u32 size=0;

	for(u32 i=0;i<m_NumSaveGameFiles;++i)
	{
		size += m_pSaveGameFiles[i]->GetTotalSize();
	}

	//add any overhead that the system may contain to the size
	return (u32)SAVEGAME.CalculateDataSizeOnDisk(m_SignInId,size);
}

// PURPOSE: Calculate the total space available for the selected device.
// RETURNS: The space available in bytes
u32 SaveGameDataManager::GetTotalAvailableSpace()
{
	return (u32)SAVEGAME.GetTotalAvailableSpace(m_SignInId);
}

// PURPOSE: checks to see if there is enough space available to save the given file
// PARAMS:  file - The file to query.  Defaults to the main save game file
// RETURNS: true if space is available, else false
bool SaveGameDataManager::HasRoomToSave(SaveGameFile* pFile)
{
	u32 spaceNeeded = GetTotalSpaceNeededToSave(pFile);
	u32 availableSpace = GetTotalAvailableSpace();

	//if we already have a save game on this device then we can overwrite
	if (m_NumProfileFiles)
	{
		spaceNeeded = 0;
	}

	if (spaceNeeded  < availableSpace)
	{
		return true;
	}
	else
	{		
		return false;
	}
}

// PURPOSE: checks to see if there is enough space available to save the given file
// PARAMS:  file - The file to query.  Defaults to the main save game file
// RETURNS: true if space is available, else false
bool SaveGameDataManager::HasRoomToSave(const char* pFile)
{
	u32 spaceNeeded = GetTotalSpaceNeededToSave(pFile);
	u32 availableSpace = GetTotalAvailableSpace();

	//if we already have a save game on this device then we can overwrite
	if (DoesProfileExist())
	{
		spaceNeeded = 0;
	}

	if (spaceNeeded < availableSpace)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// PURPOSE: checks to see if there is enough space available to save all available files
// PARAMS:  file - The file to query.  Defaults to the main save game file
// RETURNS: true if space is available, else false
bool SaveGameDataManager::HasRoomToSaveAllFiles()
{
	u32 spaceNeeded = GetTotalSpaceNeededToSaveAllFiles();
	u32 availableSpace = GetTotalAvailableSpace();

	return (spaceNeeded < availableSpace);
}

bool SaveGameDataManager::IsCurrentDeviceValid()
{
	return SAVEGAME.IsCurrentDeviceValid(m_SignInId);
}

void SaveGameDataManager::SetStateToIdle()
{
	SetState(IDLE);
}

void SaveGameDataManager::SetDisabled(bool disabled)
{	
	if (disabled && !m_bDisabled)
	{
		m_bDisabled = disabled;
		UIMANAGER->SendEvent("fileNoSaveEvent" );
		UIMANAGER->SendEvent("fileDisableSaving");
		m_bIsSaving=false;
		m_bIsSaveSuccessful=true;
		m_bStartupChecksComplete = true;
	}
	else if(!disabled && m_bDisabled)
	{
		m_bDisabled = disabled;
		UIMANAGER->SendEvent( "fileSaveEnabledEvent" );
	}
	else
	{
		m_bDisabled = disabled;
	}
}

void SaveGameDataManager::SetAutoSaveDisabled(bool disabled)
{	
	if (disabled && !m_bAutoSaveDisabled)
	{
		m_bAutoSaveDisabled = disabled;
		UIMANAGER->SendEvent("fileNoSaveEvent");
		UIMANAGER->SendEvent("fileDisableSaving");
		m_bIsSaving=false;
		m_bIsSaveSuccessful=true;
		m_bStartupChecksComplete = true;
	}
	else if(!disabled && m_bDisabled)
	{
		m_bAutoSaveDisabled = disabled;
		UIMANAGER->SendEvent("fileAutoSaveEnabled");
	}
	else
	{
		m_bDisabled = disabled;
	}
}
//#ifdef __DEV
//void SaveGameDataManager::Test()
//{
//	UIDebugf1("SAVE GAME : Available memory before Test: %3.3f M",sysMemAllocator::GetCurrent().GetMemoryAvailable() / (1024.f * 1024.f));
//
//	//static mcTestSaveGameData testObject;
//	static char careerData[100];
//	static char statData[100];
//	static int stuff = 777;
//	static float otherStuff = 0.333f;
//
//	strcpy(careerData,"This is my career data");
//	strcpy(statData,"This is my stat data");
//
//	RegisterData(2,"career",careerData,100*sizeof(char));
//	RegisterData(1,"stats",statData,100*sizeof(char));
//	RegisterData(1,"stuff",&stuff,sizeof(int));
//	RegisterData(1,"otherStuff",&otherStuff,100*sizeof(int));
//	//testObject.mVersion = 1;
//	//testObject.SetKey("pwnd");
//	//testObject.setDataSize(100);
//	//RegisterData(testObject);
//
//	//RegisterData("career",careerData,100*sizeof(char));
//	SaveData();
//	//LoadData();
//
//	UIDebugf1("SAVE GAME : career data is [%s]",careerData);
//	UIDebugf1("SAVE GAME : stat data is [%s]",statData);
//
//	UIDebugf1("SAVE GAME : Available memory after Test: %3.3f M",sysMemAllocator::GetCurrent().GetMemoryAvailable() / (1024.f * 1024.f));
//}
//
//#endif

#if __BANK
void SaveGameDataManager::FillDevice()
{
	SAVEGAME.FillDevice(m_SignInId);
}

void SaveGameDataManager::SetError()
{
	if (m_SelectedOperation != SAVEOP_NONE && m_SelectedErrorCode > 0)
		m_bErrorSet = true;
	else
		m_bErrorSet = false;
}

void SaveGameDataManager::ThrowTestError()
{
	Assert(m_bErrorSet);
	UIDebugf1("Throwing test error %s for operation %s", errorcodes[m_SelectedErrorCode], operations[m_SelectedOperation]);
	switch (m_SelectedErrorCode)
	{
	case NOT_ENOUGH_FREE_SPACE_ON_DEVICE:
		UIMANAGER->SendEvent("fileNoFreeSpace");
		break;
	case USER_CANCELLED_DEVICE_SELECTION:
		UIMANAGER->SendEvent("fileUserCanceledDeviceSelection");
		break;
	case FILE_CORRUPT:
		Displayf("The save game is corrupt!!");
		UIMANAGER->SendEvent("fileFileCorrupt");         
		break;
	case PATH_NOT_FOUND:
		Displayf("Path not found!!");
		UIMANAGER->SendEvent("fileUnexpectedError");         
		break;
	case DEVICE_CORRUPT:
		Displayf("fiSaveGame::DEVICE_CORRUPT!!");
		UIMANAGER->SendEvent("fileUnexpectedError");         
		break;
	case COULD_NOT_MAP_DEVICE:
		UIMANAGER->SendEvent("fileStorageDeviceInvalid");         		
		break;
	case THE_DEVICE_IS_NOT_READY:
		UIMANAGER->SendEvent("fileStorageDeviceInvalid");         
		break;
	default:
		Displayf("UNEXPECTED ERROR!!");
		UIMANAGER->SendEvent("fileUnexpectedError");
		break;
	}

	UIMANAGER->SendEvent("fileErrorOccured");

	m_bErrorSet = false;
	m_SelectedErrorCode = ERROR_NONE;
	m_SelectedOperation = SAVEOP_NONE;
}

#endif

#if ENABLE_DONGLE

#if !__FINAL
void SaveGameDataManager::CreateDongle()
{
	// Retrieve the xbox360 network address
	char macAddrString [18]; 
#if __XENON
	XNADDR lXnaddr; 
	DWORD lGetXnAddrStatus;
	lGetXnAddrStatus = XNetGetTitleXnAddr (&lXnaddr); // Verify errors on the status
	// Get the Hexa String of the Mac Address
	int ret;
	ret = sprintf(macAddrString, "%02X:%02X:%02X:%02X:%02X:%02X", lXnaddr.abEnet[0], lXnaddr.abEnet[1], 
		lXnaddr.abEnet[2], lXnaddr.abEnet[3], 
		lXnaddr.abEnet[4], lXnaddr.abEnet[5]);
#else
	formatf(macAddrString, sizeof(macAddrString), "00:00:00:00:00:00");
#endif // __XENON
	macAddrString[17] = '\0';

	fiDevice::SystemTime expiryTime;
	expiryTime.wYear = 9;			// Two digit year
	expiryTime.wMonth = 1;			// 1=Jan, 2=Feb, etc
	expiryTime.wDayOfWeek = 0;		// 0=Sun, 1=Mon, etc
	expiryTime.wDay = 0;			// Day of month (1, 2, 3..)
	expiryTime.wHour = 0;
	expiryTime.wMinute = 0;
	expiryTime.wSecond = 0;
	expiryTime.wMilliseconds = 0;

	ioDongle::WriteCodeFile("piEHLexlarlE_iUpLUwRiAt#IuwrluFoAWRiEHou9iuKLUvLuZoAsPiapLeSPou" /*encodeString*/, macAddrString, expiryTime,false, "root41.sys");
}
#endif // !__FINAL

bool SaveGameDataManager::CheckDongle()
{
	if (FORCE_DONGLE || (__FINAL && ENABLE_DONGLE) || PARAM_dongle.Get())
	{
		char outputString[128];
		const bool result = ioDongle::ReadCodeFile("piEHLexlarlE_iUpLUwRiAt#IuwrluFoAWRiEHou9iuKLUvLuZoAsPiapLeSPou", "root41.sys", outputString);
#if __BANK
		if (!result)
		{
			// kill the bank manager to stop people from hacking into the game that way!!!
			//bkManager::DeleteBankManager();
		}
#endif // __BANK
		return result;
	}
	else
		return true;
}
#endif // ENABLE_DONGLE


//PSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSN
#if __PS3

PS3SaveGameDataManager::PS3SaveGameDataManager()
	:SaveGameDataManager( )
{
	ASSET.PushFolder("$\\textures");
	m_pIcon = rage_new char[ICONSIZE];
	fiStream* pIconFile = ASSET.Open( "ICON0", "png" );
	if (pIconFile)
	{
		pIconFile->Read(m_pIcon,ICONSIZE);
		SAVEGAME.SetIcon(m_pIcon,ICONSIZE);
		pIconFile->Close();
	}	
	ASSET.PopFolder();

	m_RageState = RAGEIDLE;
}

PS3SaveGameDataManager::~PS3SaveGameDataManager()
{
	delete m_pIcon;
}

void PS3SaveGameDataManager::Init()
{	
	SaveGameDataManager::Init();
	m_pActiveGamer = NULL;	
}

void PS3SaveGameDataManager::SaveData(SaveGameFile* pFile, bool verifyFile)
{
	m_SaveTime = 0.0f;

	if(m_bDisabled)
	{
		UIDebugf1("SAVE GAME : Saving and Loading disabled");
	}
	else if(PARAM_nosave.Get())
	{
		UIDebugf1("SAVE GAME : -nosave found, Saving disabled");
		UIMANAGER->SendEvent("fileSaveSuccess");
	}
	else if(!pFile)
	{
		UIDebugf1("SAVE GAME : No file registered for loading!");
	}
	else if(m_CurrentState!=IDLE)
	{
		UIDebugf1("SAVE GAME : DROPPING VERIFY SAVE REQUEST, You are currently doing IO");			
	}
	else if(m_pActiveGamer && m_pActiveGamer->IsSignedIn() == false)
	{
		UIDebugf1("SAVE GAME : Player has signed out!");
		UIMANAGER->SendEvent("filePlayerHasSignedOut");
	}
	else
	{
		safecpy(m_CurrentFilename,pFile->mFilename,MAX_FILE_NAME_SIZE);

		m_bIsSaving = true;
		m_bIsSaveSuccessful = false;

		if (!m_bSaveGameCreated && !m_bSavingNewGame && m_bStartupChecksComplete)
		{
			verifyFile=false;//don't verify if startup checks is complete cause this means that the file is corrupt
		}

		if (m_bSaveGameCreated || m_bSavingNewGame || m_bStartupChecksComplete)
		{
			UIDebugf1("SAVE GAME : Save Starting");

			pFile->m_bSaveRequested = true;
			m_pCurrentFile = pFile;
			safecpy(m_CurrentFilename,pFile->mFilename,MAX_FILE_NAME_SIZE);

			if (verifyFile)
			{
				UIMANAGER->SendEvent("fileVerifyAndSaveStart");
			}
			else
			{
				m_CurrentState = SAVING;
				UIMANAGER->SendEvent("fileSaveStart");	

				m_pCurrentFile->SaveData();

				//start saving
				//UIDebugf1("SAVE GAME : Saving MC4 save game via SaveData()");

				//TODO:  UN-HACK
				//if(UIMANAGER->GetUIObject("PhotoAlbumScene")->IsActive())
				//{
				//	formatf(m_SubtitleChar,MAX_DISPLAY_NAME_SIZE,"%s: %d",MCSTRINGTABLE->GetStringUTF8("PhotoAlbum_File_Subtitle"),s32(PHOTOALBUM->GetNumPhotos()));
				//	A2WHelper(m_Subtitle,m_SubtitleChar,MAX_DISPLAY_NAME_SIZE);
				//	strncpy(m_TitleChar,MCSTRINGTABLE->GetStringUTF8("PhotoAlbum_File_Title"),MAX_DISPLAY_NAME_SIZE);
				//	SAVEGAME.SetSaveGameTitle(m_TitleChar);
				//}
				//else
				//{
				//	//format a string with career % complete for the sub title
				//	formatf(m_SubtitleChar,MAX_DISPLAY_NAME_SIZE,"%s: %d",MCSTRINGTABLE->GetStringUTF8("Save_File_Subtitle"),s32(MISSIONMGR->GetPercentComplete() * 100.0f));
				//	A2WHelper(m_Subtitle,m_SubtitleChar,MAX_DISPLAY_NAME_SIZE);
				//	strncpy(m_TitleChar,MCSTRINGTABLE->GetStringUTF8("Save_File_Title"),MAX_DISPLAY_NAME_SIZE);
				//	SAVEGAME.SetSaveGameTitle(m_TitleChar);
				//}

				char16 displayName[MAX_DISPLAY_NAME_SIZE];
				ConstructDisplayName(displayName, MAX_DISPLAY_NAME_SIZE);
				WideToUtf8(m_pCurrentFile->mDisplayName, displayName, MAX_DISPLAY_NAME_SIZE);

				UIDebugf1("SAVE GAME : Saving file %s",m_pCurrentFile->mDisplayName );

				bool success = SAVEGAME.BeginSave(m_SignInId,(unsigned)CONTENT_TYPE,m_Subtitle,m_pCurrentFile->mFilename,
					m_pCurrentFile->mpBuffer,m_pCurrentFile->m_BufferSize,true);
				if (success)
				{
					UIDebugf1("Saving game");
					m_RageState = SAVING_CONTENT;
				}
				else
				{
					UIMANAGER->SendEvent("fileSaveError");
					UIDebugf1("Could not save. BeginSave returned false!");
				}
			}
		}
		else
		{
			//we need to create a new game and do all the checks before we can save
			UIMANAGER->SendEvent("fileStartPlayerChecks");
		}
	}
}

// PURPOSE:	Deserialize data from the save game file into the internal data buffer.
void PS3SaveGameDataManager::VerifyFile(const char* pFile/*= MAIN_FILE_NAME*/)
{
	SaveGameFile* file = GetFile(pFile);

	if(m_bDisabled)
	{
		UIDebugf1("SAVE GAME : Saving and Loading disabled");
	}
	else if(m_CurrentState!=IDLE)
	{
		UIDebugf1("SAVE GAME : DROPPING VERIFY SAVE REQUEST, You are currently doing IO");			
	}
	else if(m_pActiveGamer->IsSignedIn() == false)
	{
		UIDebugf1("SAVE GAME : Player has signed out!");
		UIMANAGER->SendEvent("filePlayerHasSignedOut");
	}
	else if(!file)
	{
		UIDebugf1("SAVE GAME : No file registered for loading!");
	}
	else
	{
		file->mLoadRequested = true;		
		m_pCurrentFile = file;
		safecpy(m_CurrentFilename,m_pCurrentFile->mFilename,MAX_FILE_NAME_SIZE);
		m_CurrentState = VERIFY_SAVE_GAME;
		m_RageState = ENUMERATING_CONTENT;

		ClearSaveDataArray();
		bool success = SAVEGAME.BeginEnumeration(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo,MAX_SAVE_GAMES);

		if (!success)
		{
			m_RageState = RAGEIDLE;
			SetState(IDLE);
			UIDebugf1("Could not begin enumeration. BeginEnumeration returned false!");
		}
	}
}

void PS3SaveGameDataManager::LoadData(SaveGameFile* pFile)
{
	if(m_bDisabled)
	{
		UIDebugf1("SAVE GAME : Saving and Loading disabled");
	}
	else if(PARAM_noload.Get())
	{
		UIDebugf1("SAVE GAME : -nosave found, Loading disabled");
		UIMANAGER->SendEvent("fileLoadSuccess");
	}
	else if(!pFile)
	{
		UIDebugf1("SAVE GAME : No file registered for loading!");
	}
	else if(m_CurrentState!=IDLE)
	{
		UIDebugf1("SAVE GAME : DROPPING LOAD GAME REQUEST, You are currently doing IO");			
	}
	else if(m_pActiveGamer->IsSignedIn() == false)
	{
		UIDebugf1("SAVE GAME : Player has signed out!");
		UIMANAGER->SendEvent("filePlayerHasSignedOut");
	}
	else
	{
		u32 index = 0;

		//start the load process
		m_CurrentState = LOADING;

		UIMANAGER->SendEvent("fileLoadStart");

		pFile->LoadData();

		//pFile->mLoadRequested = true;
		//m_pCurrentFile = pFile;
		//safecpy(m_CurrentFilename,m_pCurrentFile->mFilename,MAX_FILE_NAME_SIZE);

		//find the index for this file
		while(strcmp(mSavedGameInfo[index].Filename , pFile->mFilename)!=0 && index < MAX_SAVE_GAMES)
		{
			++index;
		}

		if (index >= MAX_SAVE_GAMES)
		{
			//file not found. Display error to player?
			UIDebugf1("SAVE GAME : File not found.");

			UIMANAGER->SendEvent("fileNoFilesExist");              
		}
		else
		{
			//TODO: UN-HACK
			//if(UIMANAGER->GetUIObject("PhotoAlbumScene")->IsActive())
			//{
			//	formatf(m_SubtitleChar,MAX_DISPLAY_NAME_SIZE,"%s: %d",MCSTRINGTABLE->GetStringUTF8("PhotoAlbum_File_Subtitle"),s32(PHOTOALBUM->GetNumPhotos()));
			//	A2WHelper(m_Subtitle,m_SubtitleChar,MAX_DISPLAY_NAME_SIZE);
			//	strncpy(m_TitleChar,MCSTRINGTABLE->GetStringUTF8("PhotoAlbum_File_Title"),MAX_DISPLAY_NAME_SIZE);
			//	SAVEGAME.SetSaveGameTitle(m_TitleChar);
			//}
			//else
			//{
			//	//format a string with career % complete for the sub title
			//	formatf(m_SubtitleChar,MAX_DISPLAY_NAME_SIZE,"%s: %d",MCSTRINGTABLE->GetStringUTF8("Save_File_Subtitle"),s32(MISSIONMGR->GetPercentComplete() * 100.0f));
			//	A2WHelper(m_Subtitle,m_SubtitleChar,MAX_DISPLAY_NAME_SIZE);
			//	strncpy(m_TitleChar,MCSTRINGTABLE->GetStringUTF8("Save_File_Title"),MAX_DISPLAY_NAME_SIZE);
			//	SAVEGAME.SetSaveGameTitle(m_TitleChar);
			//}

			pFile->mLoadRequested = true;
			m_pCurrentFile = pFile;
			safecpy(m_CurrentFilename,m_pCurrentFile->mFilename,MAX_FILE_NAME_SIZE);

			bool success = SAVEGAME.BeginLoad(m_SignInId,(unsigned)CONTENT_TYPE,
				m_Subtitle,mSavedGameInfo[index].DeviceId, mSavedGameInfo[index].Filename,pFile->mpBuffer,pFile->m_BufferSize);
			if (success)
			{
				UIDebugf1("SAVEGAME: Load In Progress");
				m_RageState = LOADING_CONTENT;
			}
			else
			{			
				UIMANAGER->SendEvent("fileLoadError");
				UIDebugf1("Could not Load BeginLoad!");
			}
		}
	}
}

void PS3SaveGameDataManager::GetFileTime(SaveGameFile* pFile)
{
	if(m_bDisabled)
	{
		UIDebugf1("SAVE GAME : Saving and Loading disabled");
	}
	else if(!pFile)
	{
		UIDebugf1("SAVE GAME : No file registered for get file time!");
	}
	else if(m_CurrentState!=IDLE)
	{
		UIDebugf1("SAVE GAME : DROPPING GET FILE TIME REQUEST, You are currently doing IO");			
	}
	else if(m_pActiveGamer->IsSignedIn() == false)
	{
		UIDebugf1("SAVE GAME : Player has signed out!");
		UIMANAGER->SendEvent("filePlayerHasSignedOut");
	}
	else
	{
		u32 index = 0;

		//start the load process
		m_CurrentState = GET_FILE_TIME;

		UIMANAGER->SendEvent("fileGetFileTimeStart");

		//m_pCurrentFile = pFile;
		//safecpy(m_CurrentFilename,m_pCurrentFile->mFilename,MAX_FILE_NAME_SIZE);

		//find the index for this file
		while(strcmp(mSavedGameInfo[index].Filename , pFile->mFilename)!=0 && index < MAX_SAVE_GAMES)
		{
			++index;
		}

		if (index >= MAX_SAVE_GAMES)
		{
			//file not found. Display error to player?
			UIDebugf1("SAVE GAME : File not found.");

			UIMANAGER->SendEvent("fileNoFilesExist");              
		}
		else
		{
			UIDebugf1("SAVE GAME : Get file time for file %s.", mSavedGameInfo[index].Filename);

			m_pCurrentFile = pFile;
			safecpy(m_CurrentFilename,m_pCurrentFile->mFilename,MAX_FILE_NAME_SIZE);

			bool success = SAVEGAME.BeginGetFileTimeAndDisplayName(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo[index].DeviceId,
				mSavedGameInfo[index].Filename,mSavedGameInfo[index].DisplayName,MAX_DISPLAY_NAME_SIZE);

			if (success)
			{
				UIDebugf1("SAVEGAME: Get File Time In Progress");
				m_RageState = READING_FILE_TIME;
			}
			else
			{			
				UIMANAGER->SendEvent("fileGetFileTimeError");
				UIDebugf1("Could not Load BeginLoad!");
			}
		}
	}
}

void PS3SaveGameDataManager::StartupChecks()
{  
//#if ENABLE_DONGLE
//	if (IsDongleEnabled() && !CheckDongle())
//	{
//		UIMANAGER->SendEvent("fileDongleError");
//		return;
//	}
//#endif // ENABLE_DONGLE
	if(PARAM_disablesave.Get())
	{
		SetDisabled(true);
		UIDebugf1("SAVE GAME : Saving and Loading disabled");
	}
	else
	{
		m_CurrentState = STARTUP_CHECKS;
		safecpy(m_CurrentFilename,MAIN_FILE_NAME,MAX_FILE_NAME_SIZE);

		UIMANAGER->SendEvent("fileStartupChecksStart");

		//check for a signed in player. just take first one for now
		m_pActiveGamer = SNUSOCKET.GetLocalGamer( m_PadIndex );
		if( m_pActiveGamer->IsSignedIn() )
		{
			//m_pActiveGamer->AddNotifyHandler( &mGamerNotifyHandler );
			m_SignInId = m_pActiveGamer->GetLocalGamerIndex();

			// Need to set device to XCONTENTDEVICE_ANY, in order to enumerate content
			// accross ALL connected storage devices
			SAVEGAME.SetSelectedDeviceToAny(m_SignInId);
			m_bDeviceSelected=false;

			//lets get the amount of free space on the HDD it will be saved off internally. 
			//this will also fire no space event for no room for trophies
			m_RageState = CHECKING_FREE_SPACE;
			SAVEGAME.BeginFreeSpaceCheck(m_SignInId,MAIN_FILE_NAME,REQUIRED_TROPHY_SPACE);
		}
		else
		{         
			//SetDisabled(true);
			UIDebugf1("SAVE GAME : No signed in player");
			UIMANAGER->SendEvent("fileNoSignedInPlayer");            
		}
	}
}

void PS3SaveGameDataManager::UpdateSave(float)
{
	bool outIsValid,fileExists,success;   

	switch (m_RageState)
	{
	case RAGEIDLE:
		break;
	case HAD_ERROR:      
		//UIMANAGER->SendEvent("fileSaveError");
		m_RageState = RAGEIDLE;
		HandleError();
		break;
	case SAVING_CONTENT:
		success = SAVEGAME.CheckSave(m_SignInId,outIsValid,fileExists);
		if (success)
		{
			UIDebugf2("Checking Save");
			m_RageState = SAVED_CONTENT;
		}
		break;
	case SAVED_CONTENT:
		UIDebugf1("Save Success");
		SAVEGAME.EndSave(m_SignInId);
		m_RageState = RAGEIDLE;
		UIMANAGER->SendEvent("fileSaveComplete");     
		break;
	case ENUMERATING_CONTENT:
		m_NumProfileFiles = 0;
		success = SAVEGAME.CheckEnumeration(m_SignInId,m_NumProfileFiles);
		if (success)
		{
			m_RageState = ENUMERATED_CONTENT;
		}
		break;
	case ENUMERATED_CONTENT:
		SAVEGAME.EndEnumeration(m_SignInId);
		m_RageState = RAGEIDLE;
		UIMANAGER->SendEvent("fileEnumerateComplete");           
		break;
	case SAVING_ICON:break;
	case SAVED_ICON:break;
	case CHECKING_FREE_SPACE:
		{
			int extraSpaceRequired=0;
			success = SAVEGAME.CheckFreeSpaceCheck(m_SignInId,extraSpaceRequired);
			UIDebugf2("Checking For Room To Save");
			if (success)
			{
				SAVEGAME.EndFreeSpaceCheck(m_SignInId);
				m_RageState = RAGEIDLE;

#ifdef __DEV
				UIDebugf1("extraSpaceRequired = %d",extraSpaceRequired);
#endif
				if (extraSpaceRequired>=0)
				{
					UIMANAGER->SendEvent("HAS_ROOM_TO_CREATE_SAVE");
				}
				else
				{
					UIMANAGER->SendEvent("fileNoRoomToSave");
				}
			}
			break;		 
		}
	default:
		Assertf(0,"Should not ever get here");
		break;									
	}
}

void PS3SaveGameDataManager::UpdateLoad(float)
{
	u32 loadSize;
	bool outIsValid;

	switch (m_RageState)
	{
	case RAGEIDLE:
		break;
	case HAD_ERROR:
		HandleError();
		SAVEGAME.EndLoad(m_SignInId);
		m_RageState = RAGEIDLE;
		//UIMANAGER->SendEvent("fileLoadError");       
		break;
	case LOADING_CONTENT:
		UIDebugf2("Checking Loaded");
		if (SAVEGAME.CheckLoad(m_SignInId,outIsValid,loadSize))
		{
			if (outIsValid)
			{
				UIDebugf1("Check Loaded Success");
				m_RageState = LOADED_CONTENT;
			}
			else
			{
				m_RageState = HAD_ERROR;
			}
		}
		break;
	case LOADED_CONTENT:
		UIDebugf1("Save Game Loaded");
		SAVEGAME.EndLoad(m_SignInId);
		m_RageState = RAGEIDLE;  
		UIMANAGER->SendEvent("fileLoadComplete");           
		break;
	default:
		Assertf(0,"Should not ever get here");
		break;
	}
}

void PS3SaveGameDataManager::UpdateGetFileTime(float)
{
	switch (m_RageState)
	{
	case RAGEIDLE:
		break;
	case HAD_ERROR:
		HandleError();
		SAVEGAME.EndGetFileTimeAndDisplayName(m_SignInId);
		m_RageState = RAGEIDLE;
		//UIMANAGER->SendEvent("fileGetFileTimeError");       
		break;
	case READING_FILE_TIME:
		UIDebugf2("Checking get file time");
		SAVEGAME.CheckGetFileTimeAndDisplayName(m_SignInId, m_pCurrentFile->m_Timestamp);
		break;
	case HAVE_READ_FILE_TIME:
		UIDebugf1("Got File Time");
		SAVEGAME.EndGetFileTimeAndDisplayName(m_SignInId);
		m_RageState = RAGEIDLE;    
		UIMANAGER->SendEvent("fileGetFileTimeComplete");           
		break;
	default:
		Assertf(0,"Should not ever get here");
		break;
	}
}

void PS3SaveGameDataManager::UpdateStartupChecks(float)
{
	bool success = false;

	switch (m_RageState)
	{
	case RAGEIDLE:
		break;
	case ENUMERATING_CONTENT:
		m_NumProfileFiles = 0;
		success = SAVEGAME.CheckEnumeration(m_SignInId,m_NumProfileFiles);
		if (success)
		{
			m_RageState = ENUMERATED_CONTENT;
			//#ifdef __DEV
			for(int i=0;i<m_NumProfileFiles;i++)
			{
				UIDebugf1("file[%d] = %s",i,mSavedGameInfo[i].Filename);
			}
			//#endif
		}
		break;
	case ENUMERATED_CONTENT:
		SAVEGAME.EndEnumeration(m_SignInId);
		m_RageState = RAGEIDLE;
		UIMANAGER->SendEvent("fileEnumerateComplete");           
		Assertf(m_NumProfileFiles <= MAX_SAVE_GAMES,"too many files are saved.  increase MAX_SAVE_GAMES");
		break;
	case CHECKING_FREE_SPACE:
		{
			int extraSpaceRequired=0;
			success = SAVEGAME.CheckFreeSpaceCheck(m_SignInId,extraSpaceRequired);
			UIDebugf2("Checking For Room To Save");
			if (success)
			{
				SAVEGAME.EndFreeSpaceCheck(m_SignInId);
				m_RageState = RAGEIDLE;
				if (extraSpaceRequired>=0)
				{
					UIDebugf1("Beginning Enumeration");
					UIMANAGER->SendEvent("fileEnumerateStart");
					ClearSaveDataArray();
					bool success = SAVEGAME.BeginEnumeration(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo,MAX_SAVE_GAMES);
					if (success)
					{
						m_RageState = ENUMERATING_CONTENT;
					}
					else
					{				
						UIMANAGER->SendEvent("fileEnumerateError");
						UIDebugf1("Could not begin enumeration. It returned false!");
						SAVEGAME.EndEnumeration(m_SignInId);
					}
				}
				else
				{
					UIMANAGER->SendEvent("fileNoRoomToSave");
				}
			}
			break;		 
		}
	case HAD_ERROR:
		m_RageState = RAGEIDLE;
		HandleError();
		break;

	default:
		Assertf(0,"Should not ever get here");
		break;

	}
}

void PS3SaveGameDataManager::UpdateVerifySaveGame(float)
{   
	bool success;

	switch (m_RageState)
	{
	case RAGEIDLE:
		break;
	case ENUMERATING_CONTENT:
		m_NumProfileFiles = 0;
		success = SAVEGAME.CheckEnumeration(m_SignInId,m_NumProfileFiles);
		if (success)
		{
			m_RageState = ENUMERATED_CONTENT;
			//#ifdef __DEV
			for(int i=0;i<m_NumProfileFiles;i++)
			{
				UIDebugf1("file[%d] = %s",i,mSavedGameInfo[i].Filename);
			}
			//#endif
		}
		break;
	case ENUMERATED_CONTENT:
		SAVEGAME.EndEnumeration(m_SignInId);
		m_RageState = RAGEIDLE;
		UIMANAGER->SendEvent("fileEnumerateComplete");           
		Assertf(m_NumProfileFiles <= MAX_SAVE_GAMES,"too many files are saved.  increase MAX_SAVE_GAMES");     
		break;
	case VERIFYING_CONTENT:
		success = SAVEGAME.CheckContentVerify(m_SignInId,m_bFileVerified);
		if (success)
		{
			m_RageState = VERIFIED_CONTENT;
		}
		break;
	case VERIFIED_CONTENT:
		SAVEGAME.EndContentVerify(m_SignInId);
		m_RageState = RAGEIDLE;
		if (m_bFileVerified)
		{         
			UIMANAGER->SendEvent("fileSaveGameVerified");			
		}
		else
		{
			UIMANAGER->SendEvent("fileActiveSaveGameNotFound");  
		}
		break;
	case HAD_ERROR:
		//UIMANAGER->SendEvent("fileUnexpectedError");
		m_RageState = RAGEIDLE;
		HandleError();
		break;
	default:
		Assertf(0,"Should not ever get here");
		break;
	}
}

bool PS3SaveGameDataManager::HandleEvents( fuiEvent* pEvent)
{
	bool		eventConsumed		= false;

	//let children handle the event first
	eventConsumed |= UIObject::HandleEvents(pEvent);

	//lets see if we want to handle the event
	if(eventConsumed)
		return true;

	if (pEvent == FUIEVENTMGR->GetEvent("fileDisableAutosave"))
	{
		SetStateToIdle();//since we may be in a state other than idle
		SetAutoSaveDisabled(true);
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileLoad"))
	{
		LoadData();
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileLoadCurrent"))
	{
		LoadData(m_pCurrentFile);
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileSave"))
	{
		SaveData(MAIN_FILE_NAME,true);
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileSaveCurrent"))
	{
		SaveData(m_pCurrentFile,true);
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileEnableAutoSave"))
	{
		SetAutoSaveDisabled(false);
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileSaveInSelectedSlot"))
	{
		SaveData(m_pCurrentFile, true);
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileUserWantsToLoadSelectedGame"))
	{
		LoadData(m_pCurrentFile);
	}
	if (pEvent == FUIEVENTMGR->GetEvent("fileAllSavesCreated"))
	{
		if (!m_bStartupChecksComplete)
			OnFileStartupEnumerationComplete();
	}
	//this if is for dev purposes only
	if (pEvent == FUIEVENTMGR->GetEvent("filePlayerLoaded"))
	{
		if(m_bDisabled)
		{
			UIMANAGER->SendEvent( "fileNoSaveEvent" );
			UIMANAGER->SendEvent("fileDisableSaving");
		}
	}
	else if (pEvent==FUIEVENTMGR->GetEvent("fileStartupChecksComplete"))
	{
		if(m_bSaveGameCreated==false)
		{
			//had to create a new game since we didn't load one
			UIMANAGER->SendEvent("fileNewSaveCreated");			
		}
	}
	else if (pEvent==FUIEVENTMGR->GetEvent("fileNewSaveCreated"))
	{

	}

	if (!m_bDisabled)
	{
		if(pEvent==FUIEVENTMGR->GetEvent("fileUserWantsToCreateNewSaveGame"))
		{
			UIMANAGER->SendEvent("fileCreateNewSaveGame"); 

			//if user decides to create a new profile, bring up device selection to save profile to
			UIDebugf1("SAVE GAME : User has chosen to create a new save game on this device5");     

			//user has already chosen a device to save to
			m_CurrentState = SAVING;
			m_RageState = CHECKING_FREE_SPACE;
			SAVEGAME.BeginFreeSpaceCheck(m_SignInId,MAIN_FILE_NAME,GetTotalSpaceNeededToSaveAllFiles() + REQUIRED_TROPHY_SPACE);
		}
		else if(pEvent==FUIEVENTMGR->GetEvent("fileHasRoomToCreateSave"))
		{
			SAVEGAME.EndFreeSpaceCheck(m_SignInId);
			SetState(IDLE);
			m_RageState = RAGEIDLE;
			m_bSavingNewGame = true;
			OnCreateNewSaveGame();
			//SaveData(MAIN_FILE_NAME,false);//create the file. note that this function needs to be after state is set to idle        		
		}
		else if (pEvent==FUIEVENTMGR->GetEvent("fileDoFreeSpaceCheck"))
		{
			UIDebugf1("SAVE GAME : Starting PS3 free space check");     

			if (PARAM_disablesave.Get() || IsDisabled())
			{
				UIMANAGER->SendEvent("fileEndFreeSpaceCheck");
			}
			else
			{
				SetDoingFreeSpaceCheck(true);
				//kick off the startup checks
				StartupChecks();
			}
		}
		else if(pEvent==FUIEVENTMGR->GetEvent("fileNoFreeSpace"))
		{
			SetState(IDLE);
			m_RageState = RAGEIDLE;
			UIMANAGER->SendEvent("fileNoRoomToSave"); 
		}
		else if (pEvent==FUIEVENTMGR->GetEvent("fileUnexpectedError"))
		{
			UIDebugf1("SAVE GAME : Unexpected Error");
		}
		else if (pEvent==FUIEVENTMGR->GetEvent("fileStartPlayerChecks"))
		{
			m_bStartupChecksComplete = false;
			SetDoingFreeSpaceCheck(false);

			//kick off the startup checks
			StartupChecks();
		}
		else if(m_CurrentState != STARTUP_CHECKS && pEvent==FUIEVENTMGR->GetEvent("fileStartupChecksComplete"))
		{   
			UIDebugf1("SAVE GAME : Startup Checks Complete");

			m_bStartupChecksComplete = true;
			m_bIsSaveSuccessful = true;

		}
		else if (pEvent==FUIEVENTMGR->GetEvent("fileUserWantsToSignIn"))
		{
			ShowSignInWindow();         
		}
		else if (pEvent==FUIEVENTMGR->GetEvent("fileVerifySaveGame"))
		{
			UIDebugf1("SAVE GAME : Starting to verify file");
			if(m_bDisabled)
			{
				UIDebugf1("SAVE GAME : Saving and Loading disabled");
			}
			else if(PARAM_nosave.Get())
			{
				UIDebugf1("SAVE GAME : -nosave found, Saving disabled");
				UIMANAGER->SendEvent("fileSaveSuccess");
			}
			else if(m_CurrentState!=IDLE)
			{
				UIDebugf1("SAVE GAME : DROPPING VERIFY SAVE REQUEST, You are currently doing IO");			
			}
			else if(m_pActiveGamer && m_pActiveGamer->IsSignedIn())			
			{
				ClearSaveDataArray();
				bool success = SAVEGAME.BeginEnumeration(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo,MAX_SAVE_GAMES);
				if (success)
				{
					m_RageState = ENUMERATING_CONTENT;
				}
				else
				{
					UIMANAGER->SendEvent("fileEnumerateError");
					UIDebugf1("Could not begin enumeration. It returned false!");
				}

				m_CurrentState = VERIFY_SAVE_GAME;
			}
			//else
			//{
			//	UIMANAGER->SendEvent("filePlayerHasSignedOut");
			//}
		}
		else if (pEvent == FUIEVENTMGR->GetEvent("fileSaveGame"))
		{   
			UIMANAGER->SendEvent("fileSaveStart");

			//if no file specified, then default to the main save file
			if (m_pCurrentFile==NULL)
			{
				m_pCurrentFile = GetFile(MAIN_FILE_NAME);
			}
			SetState(IDLE);
			m_pCurrentFile->SaveData();

			//start saving
			char16 displayName[MAX_DISPLAY_NAME_SIZE];
			ConstructDisplayName(displayName, sizeof(displayName));
			WideToUtf8(m_pCurrentFile->mDisplayName, displayName, MAX_DISPLAY_NAME_SIZE);

			UIDebugf1("SAVE GAME : Saving MC4 save game via SAVE_GAME event");

			m_CurrentState = SAVING;

			//TODO: UN-HACK
			//if(UIMANAGER->GetUIObject("PhotoAlbumScene")->IsActive())
			//{
			//	formatf(m_SubtitleChar,MAX_DISPLAY_NAME_SIZE,"%s: %d",MCSTRINGTABLE->GetStringUTF8("PhotoAlbum_File_Subtitle"),s32(PHOTOALBUM->GetNumPhotos()));
			//	A2WHelper(m_Subtitle,m_SubtitleChar,MAX_DISPLAY_NAME_SIZE);
			//	strncpy(m_TitleChar,MCSTRINGTABLE->GetStringUTF8("PhotoAlbum_File_Title"),MAX_DISPLAY_NAME_SIZE);
			//	SAVEGAME.SetSaveGameTitle(m_TitleChar);
			//}
			//else
			//{
			//	//format a string with career % complete for the sub title
			//	formatf(m_SubtitleChar,MAX_DISPLAY_NAME_SIZE,"%s: %d",MCSTRINGTABLE->GetStringUTF8("Save_File_Subtitle"),s32(MISSIONMGR->GetPercentComplete() * 100.0f));
			//	A2WHelper(m_Subtitle,m_SubtitleChar,MAX_DISPLAY_NAME_SIZE);
			//	strncpy(m_TitleChar,MCSTRINGTABLE->GetStringUTF8("Save_File_Title"),MAX_DISPLAY_NAME_SIZE);
			//	SAVEGAME.SetSaveGameTitle(m_TitleChar);
			//}

			bool success = SAVEGAME.BeginSave(m_SignInId,(unsigned)CONTENT_TYPE,m_Subtitle,m_pCurrentFile->mFilename,
				m_pCurrentFile->mpBuffer,m_pCurrentFile->m_BufferSize,true);
			if (success)
			{
				UIDebugf1("Saving game");
				m_RageState = SAVING_CONTENT;
			}
			else
			{
				UIMANAGER->SendEvent("fileSaveError");
				UIDebugf1("Could not save. BeginSave returned false!");
			}
		}
		else
		{
			//handle current action events
			switch(m_CurrentState)
			{
			case IDLE:
				return false;
			case SAVING:
				HandleSaveEvents(pEvent);
				break;
			case LOADING:
				HandleLoadEvents(pEvent);
				break;
			case GET_FILE_TIME:
				HandleGetFileTimeEvents(pEvent);
				break;
			case STARTUP_CHECKS:
				HandleStartupChecksEvents(pEvent);
				break;
			case VERIFY_SAVE_GAME:
				HandleVerifySaveGameEvents(pEvent);
				break;
			default:
				break;         
			}
		}
	}
	return false;
}

bool PS3SaveGameDataManager::HandleGetFileTimeEvents( fuiEvent* pEvent )
{
	if (pEvent==FUIEVENTMGR->GetEvent("fileGetFileTimeStart"))
	{
		UIDebugf1("SAVE GAME : Get File Time Starting");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileGetFileTimeComplete"))
	{
		UIMANAGER->SendEvent("fileGetFileTimeSuccess");            
		UIDebugf1("SAVE GAME : %s got file time successfully.",m_pCurrentFile->mDisplayName);
		SetState(IDLE);
		OnGetFileTimeComplete();
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileErrorOccured"))
	{
		UIMANAGER->SendEvent("fileGetFileTimeError");            

		UIDebugf1("SAVE GAME : Error getting file time");

		//m_bStartupChecksComplete = true;
		SetState(IDLE);
		SAVEGAME.SetStateToIdle(m_SignInId);
	}

	return false;
}

bool PS3SaveGameDataManager::HandleLoadEvents( fuiEvent* pEvent )
{
	if (pEvent==FUIEVENTMGR->GetEvent("fileLoadStart"))
	{
		//#if ENABLE_DONGLE
		//		if (!CheckDongle())
		//		{
		//			UIMANAGER->SendEvent("fileDongleError");
		//			return false;
		//		}
		//#endif // ENABLE_DONGLE
		//UIMANAGER->SendEvent("fileLoadStart");
		UIDebugf1("SAVE GAME : Load Starting");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileLoadComplete"))
	{
		OnLoadComplete();

		m_pCurrentFile->mLoadRequested = false;

		UIMANAGER->SendEvent("fileLoadSuccess");            
		m_bSaveGameCreated = true;

		UIDebugf1("SAVE GAME : %s loaded successfully.",m_pCurrentFile->mDisplayName);

		m_pCurrentFile->m_bLoaded = true;
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileErrorOccured"))
	{
		UIMANAGER->SendEvent("fileLoadError");            

		UIDebugf1("SAVE GAME : Error Loading");

		m_pCurrentFile->mLoadRequested = false;

		m_bStartupChecksComplete = true;
		SetState(IDLE);
		SAVEGAME.SetStateToIdle(m_SignInId);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateStart"))
	{
		UIDebugf1("SAVE GAME : Enumerate Starting");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateComplete"))
	{
		OnFileEnumerationComplete();

		UIMANAGER->SendEvent("fileEnumerateSuccess");            

		UIDebugf1("SAVE GAME : Done checking for data. %d files found",m_NumProfileFiles);

		//check for available space
		// u32 totalAvailableSpace;
		// totalAvailableSpace = GetTotalAvailableSpace();
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateError"))
	{
		UIDebugf1("SAVE GAME : Enumerate Error");
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileNoFilesExist"))
	{
		UIDebugf1("SAVE GAME : There are no files to load");
		//UIMANAGER->SendEvent("fileNoGamesFound");            		
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileDongleError"))
	{
		UIDebugf1("SAVE GAME : Dongle Error");
	}

	return false;
}

bool PS3SaveGameDataManager::HandleSaveEvents( fuiEvent* pEvent)
{
	if(pEvent==FUIEVENTMGR->GetEvent("fileSaveComplete"))
	{
		//UIMANAGER->SendEvent("fileSaveSuccess");

		m_pCurrentFile->m_bSaveRequested = false;
		m_bSaveGameCreated = true;
		m_bIsSaving = false;
		m_bIsSaveSuccessful = true;
		UIDebugf1("SAVE GAME : %s saved successfully.",m_pCurrentFile->mDisplayName);

		//enumerate all files so we are in sync with what is now on file
		UIMANAGER->SendEvent("fileEnumerateStart");
		ClearSaveDataArray();
		bool success = SAVEGAME.BeginEnumeration(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo,MAX_SAVE_GAMES);
		if (success)
		{
			m_RageState = ENUMERATING_CONTENT;
		}
		else
		{
			UIMANAGER->SendEvent("fileErrorOccured");
			UIDebugf1("Could not begin enumeration. It returned false!");
		}
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileErrorOccured"))
	{
		if (m_bSavingNewGame)
		{
			UIMANAGER->SendEvent("fileCreateNewSaveGameError");
		}
		else
		{
			UIMANAGER->SendEvent("fileSaveError");
		}

		m_pCurrentFile->m_bSaveRequested = false;

		UIDebugf1("SAVE GAME : Error saving %s",m_pCurrentFile->mDisplayName);

		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileNoFilesExist"))
	{
		UIDebugf1("SAVE GAME : There are no files on device");
		SetState(IDLE);
	}
	if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateStart"))
	{
		UIDebugf1("SAVE GAME : Enumerate Starting");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateComplete"))
	{
		OnFileEnumerationComplete();

		UIDebugf1("SAVE GAME : Enumerate Complete");
		SetState(IDLE);

		if (m_bSavingNewGame)
		{
			UIMANAGER->SendEvent("fileNewSaveCreated", NULL );
			m_bSavingNewGame = false;

			if (!strcmp(m_pCurrentFile->mFilename, MAIN_FILE_NAME))
				m_bJustCreatedMainSave = true;

			if (!AreAllSaveFilesCreated())
				CreateNextSaveFile();
			else
				UIMANAGER->SendEvent("fileAllSavesCreated", NULL );
		}
	}
	if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateError"))
	{
		UIDebugf1("SAVE GAME : Enumerate Error");

		SetState(IDLE);
	}
	return false;
}

bool PS3SaveGameDataManager::HandleCreateSaveEvents( fuiEvent* pEvent)
{
	if(pEvent==FUIEVENTMGR->GetEvent("fileSaveComplete"))
	{
		UIMANAGER->SendEvent("fileSaveSuccess");

		m_pCurrentFile->m_bSaveRequested = false;
		m_bSaveGameCreated = true;
		//m_bIsSaving = false;
		//m_bIsSaveSuccessful = true;
		UIDebugf1("SAVE GAME : %s created successfully.",m_pCurrentFile->mDisplayName);

		//enumerate all files so we are in sync with what is now on file
		UIMANAGER->SendEvent("fileEnumerateStart");
		ClearSaveDataArray();
		bool success = SAVEGAME.BeginEnumeration(m_SignInId,(unsigned)CONTENT_TYPE,mSavedGameInfo,MAX_SAVE_GAMES);
		if (success)
		{
			m_RageState = ENUMERATING_CONTENT;
		}
		else
		{
			UIMANAGER->SendEvent("fileSaveError");
			UIDebugf1("Could not begin enumeration. It returned false!");
		}
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileSaveError"))
	{
		UIDebugf1("SAVE GAME : Error creating %s",m_pCurrentFile->mDisplayName);
	}
	if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateStart"))
	{
		UIDebugf1("SAVE GAME : Enumerate Starting");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateComplete"))
	{
		OnFileEnumerationComplete();

		UIDebugf1("SAVE GAME : Enumerate Complete");
		SetState(IDLE);

		UIMANAGER->SendEvent("fileNewSaveCreated", NULL );
	}
	if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateError"))
	{
		UIDebugf1("SAVE GAME : Enumerate Error");

		SetState(IDLE);
	}
	return false;
}

bool PS3SaveGameDataManager::HandleStartupChecksEvents( fuiEvent* pEvent)
{
	if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateStart"))
	{
		//#if ENABLE_DONGLE
		//		if (!CheckDongle())
		//		{
		//			UIMANAGER->SendEvent("fileDongleError");
		//			return false;
		//		}
		//#endif // ENABLE_DONGLE
		UIDebugf1("SAVE GAME : Enumerate Starting");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateComplete"))
	{
		//OnFileEnumerationComplete();

		UIMANAGER->SendEvent("fileEnumerateSuccess");            

		UIDebugf1("SAVE GAME : Done checking for data. %d files found",m_NumProfileFiles);

		int dummy=0;
#ifdef __DEV
		UIDebugf1("total avail space = %d,  space needed = %d",(int)SAVEGAME.GetTotalAvailableSpace(dummy)*1024,(int)GetTotalSpaceNeededToSaveAllFiles() + REQUIRED_TROPHY_SPACE);
#endif
		//gather all save games across all device for the signed in player
		EnumerateSaveGames();

		if(m_NumProfileFiles==0)
		{
			if (SAVEGAME.GetTotalAvailableSpace(dummy)*1024 >= GetTotalSpaceNeededToSaveAllFiles() + REQUIRED_TROPHY_SPACE)
			{
				if(!m_bIsOnlyCheckingSpace)//hack to add a free space check before startscreen
				{
					//lets not worry about this until the first save
					UIMANAGER->SendEvent("fileNoFilesExist");			
				}
				else
				{
					SetState(IDLE); //Idle? Probably not a good idea
					UIMANAGER->SendEvent("fileEndFreeSpaceCheck");
				}
			}
			else
			{
				UIMANAGER->SendEvent("fileNoFreeSpace");			
			}
		}
		else if(DoesProfileExist()==false)
		{
			if (SAVEGAME.GetTotalAvailableSpace(dummy)*1024 >= GetTotalSpaceNeededToSaveAllFiles() + REQUIRED_TROPHY_SPACE)
			{
				if(!m_bIsOnlyCheckingSpace)//hack to add a free space check before startscreen
				{
					//lets not worry about this until the first save
					UIMANAGER->SendEvent("fileNoFilesExist");			
				}
				else
				{
					SetState(IDLE);
					UIMANAGER->SendEvent("fileEndFreeSpaceCheck");
				}
			}
			else
			{
				UIMANAGER->SendEvent("fileNoFreeSpace");			
			}
		}
		else if(DoMultipleProfilesExist()==true)
		{
			if(m_bIsOnlyCheckingSpace)//hack to add a free space check before startscreen
			{
				//TODO: need to fix so that it checks for all files that do not exist!
				if (!SAVEGAME.GetTotalAvailableSpace(dummy)*1024 < (int)GetTotalSpaceNeededToSaveAllFiles() + REQUIRED_TROPHY_SPACE)
				{
					UIMANAGER->SendEvent("fileNoFreeSpace");			
				}
				else
				{
					SetState(IDLE);
					UIMANAGER->SendEvent("fileEndFreeSpaceCheck");
				}
			}
			else
			{
				UIMANAGER->SendEvent("fileMultipleProfilesExist");
			}
		}
		else
		{
			UIDebugf1("SAVE GAME : Found profile"); 

			//load the save game. note that this function needs to be after state is set to idle
			if (!m_bSavingNewGame)
			{
#ifdef __DEV
				UIDebugf1("total avail space = %d,  space needed = %d",(int)SAVEGAME.GetTotalAvailableSpace(dummy)*1024,(int)GetTotalSpaceNeededToSave(GetFile("PHOTO_ALBUM")) + REQUIRED_TROPHY_SPACE);
#endif
				//TODO: need to fix so that it checks for all files that do not exist!
				if (SAVEGAME.GetTotalAvailableSpace(dummy)*1024 < GetTotalSpaceNeededToSaveAllFiles() + REQUIRED_TROPHY_SPACE)
				{
					UIMANAGER->SendEvent("fileNoFreeSpace");			
				}
				else
				{
					if (AreAllSaveFilesCreated())
						OnFileStartupEnumerationComplete();
					else
						CreateNextSaveFile();
					//SetState(IDLE);
					//OnFileStartupEnumerationComplete();
				}
			}
			else
			{
				UIDebugf1("startup checks not creating new save");
				m_bSavingNewGame = false;
				UIMANAGER->SendEvent("fileSaveSuccess");
				UIMANAGER->SendEvent("fileNewSaveCreated", NULL );

				SetState(IDLE);
			}
		}
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateError"))
	{
		UIDebugf1("SAVE GAME : There was an error with the device.  Gui needed here to tell user that something is thrashed");
		UIMANAGER->SendEvent("fileStartupChecksError");
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileUserCanceledDeviceSelection"))
	{
		UIDebugf1("SAVE GAME : User cancelled device selection.  Gui needed here to confirm that user does not want to save");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileNoFilesExist"))
	{
		//if user decides to create a new profile, bring up device selection to save profile to
		UIDebugf1("SAVE GAME : There are no save games found.");
		UIMANAGER->SendEvent("fileNoGamesFound");            
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileMultipleProfilesExist"))
	{
		m_bMultipleGamesFound = true;
		UIDebugf1("SAVE GAME : There are multiple devices that contain a save game. please choose which device to use.Gui needed.");
		UIMANAGER->SendEvent("fileMultipleGamesFound");
	}
	/*else if(pEvent==FUIEVENTMGR->GetEvent("fileUserWantsToCreateNewSaveGame"))
	{
		UIMANAGER->SendEvent("fileCreateNewSaveGame"); 

		//if user decides to create a new profile, bring up device selection to save profile to
		UIDebugf1("SAVE GAME : User has chosen to create a new save game on this device6");     

		//user has already chosen a device to save to
		m_bSavingNewGame = true;
		SetState(IDLE);
		SaveData(MAIN_FILE_NAME,false);//create the file. note that this function needs to be after state is set to idle        		
	}*/
	else if(pEvent==FUIEVENTMGR->GetEvent("fileStartupChecksStart"))
	{
		//UIMANAGER->SendEvent("fileStartupChecksStart");
		UIDebugf1("SAVE GAME : Beginning startup Checks");

		UIObject* pSaveGame_DoNotInterruptMe = UIMANAGER->GetUIObject("SaveGame_DoNotInterruptMe");
		pSaveGame_DoNotInterruptMe->Activate();
		pSaveGame_DoNotInterruptMe->SetVisible(true);
		pSaveGame_DoNotInterruptMe->SetFocused(true);

		if (!m_bIsOnlyCheckingSpace)
		{
			UIObject* pSaveGameDialogs = UIMANAGER->GetUIObject("SaveGameDialogs");
			pSaveGameDialogs->Activate();
			pSaveGameDialogs->SetVisible(true);
			pSaveGameDialogs->SetFocused(true);
		}

		UIObject* pSaveGameIO = UIMANAGER->GetUIObject("SaveGameIO");
		pSaveGameIO->Activate();
		pSaveGameIO->SetVisible(true);
		pSaveGameIO->SetFocused(true);		
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileStartupChecksComplete"))
	{   
		UIDebugf1("SAVE GAME : Startup Checks Complete");
		SAVEGAME.SetStateToIdle(m_SignInId);
		SetState(IDLE);

		m_bStartupChecksComplete = true;
		m_bIsSaveSuccessful = true;
		m_bUserWantsToCreateANewGame = false;
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileStartupChecksError"))
	{
		UIDebugf1("SAVE GAME : Error during startup checks");
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileUnexpectedError"))
	{
		UIMANAGER->SendEvent("fileStartupChecksError");
		UIDebugf1("SAVE GAME : Error during startup checks");
		SetState(IDLE);
	}

	return false;
}

bool PS3SaveGameDataManager::HandleVerifySaveGameEvents( fuiEvent* pEvent)
{
	if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateStart"))
	{
		UIDebugf1("SAVE GAME : Enumerate Starting");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileEnumerateComplete"))
	{
		//OnFileEnumerationComplete();

		UIMANAGER->SendEvent("fileEnumerateSuccess");

		//gather all save games across all device for the signed in player
		EnumerateSaveGames();

		if(m_NumProfileFiles==0)
		{
			//lets not worry about this until the first save
			UIMANAGER->SendEvent("fileNoFilesExist");			
		}
		else if(DoesProfileExist()==false)
		{
			//lets not worry about this until the first save
			UIMANAGER->SendEvent("fileNoFilesExist");
		}
		else
		{
			UIDebugf1("SAVE GAME : Found save game");

			//check the next device that has a save game
			//TODO: NEEDS TO BE IMPLEMENTED FOR PS3! bool success = SAVEGAME.BeginContentVerify(m_SignInId,CONTENT_TYPE,0/*not used on PS3*/,m_pCurrentFile->mFilename);
			if (1)
			{
				m_RageState = VERIFYING_CONTENT;
			}
			else
			{				
				UIMANAGER->SendEvent("fileSaveError");
				UIDebugf1("Could not begin verification. BeginContentVerify returned false!");
			}
		}
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileUnexpectedError"))
	{
		UIDebugf1("SAVE GAME : There was an error with the device.");
		SetState(IDLE);
		SAVEGAME.SetStateToIdle(m_SignInId);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileActiveSaveGameNotFound"))
	{
		UIDebugf1("SAVE GAME : Your save game has been removed.  please replace it2");
		UIMANAGER->SendEvent("fileVerifyAndSaveFail");
		SetState(IDLE);
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileSaveGameVerified"))
	{
		UIDebugf1("SAVE GAME : Your save game has been verified");      

		SetState(IDLE);

		UIMANAGER->SendEvent("fileSaveGameVerifySuccess");
	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileNoFilesExist"))
	{
		UIDebugf1("SAVE GAME : There are no files to load");
		SetState(IDLE);

		UIMANAGER->SendEvent("fileNoGamesFound");                  

	}
	else if(pEvent==FUIEVENTMGR->GetEvent("fileUserWantsToCreateNewSaveGame"))
	{
		UIMANAGER->SendEvent("fileCreateNewSaveGame"); 

		//if user decides to create a new profile, bring up device selection to save profile to
		UIDebugf1("SAVE GAME : User has chosen to create a new save game on this device7");     

		//user has already chosen a device to save to
		m_bSavingNewGame = true;
		SetState(IDLE);
		SaveData(MAIN_FILE_NAME,false);//create the file. note that this function needs to be after state is set to idle        		
	}

	return false;
}

void PS3SaveGameDataManager::OnFileStartupEnumerationComplete()
{
	if(!m_bIsOnlyCheckingSpace)//hack to add a free space check before startscreen
	{
		LoadData();
	}
	else
	{
		UIMANAGER->SendEvent("fileEndFreeSpaceCheck");
	}
}

bool PS3SaveGameDataManager::DoMultipleProfilesExist()
{
	return false;
}

//void PS3SaveGameDataManager::SetDisabled(bool disabled)
//{
//	m_bDisabled = disabled;
//	if (disabled)
//	{
//		UIMANAGER->SendEvent("fileDisableSaving");
//	}
//}

#endif

}//end namespace rage
//PSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSNPSN


