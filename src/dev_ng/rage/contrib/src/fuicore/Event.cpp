// 
// fuiEvent/Event.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// Event include
#include "fuicore/EventManager.h"
#include "string/stringhash.h"
#include "fuicore/UIManager.h"

namespace rage {

// fuiEvent Implementation //

fuiEvent::fuiEvent( void )
	:m_strEventName( NULL ),
	 m_pEventObject(NULL),
	 m_pTargetObject(NULL),
	 m_uHashValue( 0 ),
	 m_bSendToScripts(true),
	 m_bSendToCode(true),
	 m_bIsInput(false)
{
	// Nothing for now
}

fuiEvent::fuiEvent( const char* pStrEventIdentifier )
{
	SetName( pStrEventIdentifier );
	m_uHashValue = atStringHash( pStrEventIdentifier );
}

fuiEvent::~fuiEvent( void )
{
	//if( m_strEventName )
	//{
	//	delete [] m_strEventName;
	//}
}

//fuiEvent& fuiEvent::operator=( fuiEvent& rEvent )
//{
//	SetName( rEvent.GetName() );
//	m_uHashValue = rEvent.GetHash();
//	return *this;
//}

void fuiEvent::SetName( const char* pStrEventIdentifier )
{
	// Allocate a string long enough to accommodate event name
	//Assert(!m_strEventName);
	m_strEventName = UIFACTORY->CreateString(pStrEventIdentifier);

	// Hash the newly created event
	m_uHashValue = atStringHash( pStrEventIdentifier );
}

bool fuiEvent::LoadFromString(const char* szEvent)
{
	char	szTrigger	[128];
	char*	pos 		;

	strcpy(szTrigger, szEvent);

	//find the object
	pos = strchr(szTrigger, '.');
	if(pos)
	{
		pos[0]=0;
		pos++;

		m_pEventObject = UIFACTORY->GetUIObject(szTrigger);

		if (!m_pEventObject)
		{		
			return false;
		}
	}
	else
	{
		m_pEventObject = UIMANAGER;
		pos				= szTrigger;
	}

	m_strEventName	= UIFACTORY->CreateString(pos);

	return true;
}

// fuiEventHashElement Implementation //

fuiEventHashElement::fuiEventHashElement( fuiEvent* pEvent )
	:m_nValidDataElements( 0 )
{
	AssertMsg( pEvent , "fuiEventHashElement ctor - Attempt to initialize a event element with an invalid event." );
	m_Event = *pEvent;

	// Initialize the event data array
	m_aEventData.Resize( DATA_ARRAY_INIT_ELEMENTS );
}

fuiEventHashElement::fuiEventHashElement( const char* pStrEventIdentifier )
	:m_nValidDataElements( 0 )
{
	m_Event.SetName( pStrEventIdentifier );

	// Initialize the event data array
	m_aEventData.Resize( DATA_ARRAY_INIT_ELEMENTS );
}

fuiEventHashElement& fuiEventHashElement::operator=( const fuiEventHashElement& rSrcEventElement )
{
	m_Event = *const_cast<fuiEventHashElement&>(rSrcEventElement).GetEvent();
	m_aEventData = rSrcEventElement.m_aEventData;
	return *this;
}

void fuiEventHashElement::AddEventData( fuiEventListener* pListener, rage::s32 nPriority )
{
	AssertMsg( pListener , "fuiEventHashElement::AddEventData - Attempt to add an event with an invalid listener." );

	// First search the current array for an unused data element
	EEventDataState eDataState = EEventDataState_INVALID;
	for ( s32 nDataArrayItr = 0; nDataArrayItr < m_aEventData.GetCount(); nDataArrayItr++ )
	{	
		eDataState = m_aEventData[ nDataArrayItr ].GetState();
		if( eDataState == EEventDataState_INVALID ||
			eDataState == EEventDataState_DIRTY )
		{
			m_aEventData[ nDataArrayItr ].SetListener( pListener );
			m_aEventData[ nDataArrayItr ].SetPriority( nPriority );
			m_aEventData[ nDataArrayItr ].SetState( EEventDataState_UNMARKED );
			m_nValidDataElements++;
			return;
		}
	}

	// We couldn't find an open slot therefore we must grow the array
	fuiEventData oNewEventData( pListener, nPriority );
	oNewEventData.SetState( EEventDataState_UNMARKED );

	// Grow the array and add the new data element
	m_aEventData.Grow( DATA_ARRAY_GROW_SIZE ) = oNewEventData;
	m_nValidDataElements++;
}

bool fuiEventHashElement::RemoveEventData( fuiEventListener* pListener )
{
	for ( s32 nDataArrayItr = 0; nDataArrayItr < m_aEventData.GetCount(); nDataArrayItr++ )
	{
		if( m_aEventData[ nDataArrayItr ].GetListener() == pListener )
		{
			m_aEventData[ nDataArrayItr ].Reset();
			m_nValidDataElements--;
			break;
		}
	}

	return m_nValidDataElements == 0;
}

//quicksort compare function, where type of elements in the array is fuiEventData
//sorts in ascending order
static rage::s32 sCompareEventData( const fuiEventData* pEventA, const fuiEventData* pEventB )
{
	rage::s32 nPriorityA = pEventA->GetPriority();
	rage::s32 nPriorityB = pEventB->GetPriority();

	if( nPriorityA == nPriorityB )
	{
		return 0;	// a == b
	}
	else if( nPriorityA < nPriorityB )
	{
		return -1;	// a < b
	}
	else
	{
		return 1;   // a > b
	}
}

void fuiEventHashElement::PostEvent()
{
	// Sort the events based on the priority and state
	m_aEventData.QSort( 0, m_aEventData.GetCount(), (rage::s32 (/*__cdecl*/ *)(const fuiEventData*, const fuiEventData*)) sCompareEventData );

	// Before moving forward, mark any unmarked events ( events that have been added but not yet acknowledged )
	rage::s32 nDataArrayItr = 0;
	for ( nDataArrayItr = 0; nDataArrayItr < m_aEventData.GetCount(); nDataArrayItr++ )
	{
		if( m_aEventData[ nDataArrayItr ].GetState() == EEventDataState_UNMARKED )
		{
			m_aEventData[ nDataArrayItr ].SetState( EEventDataState_MARKED );
		}
	}

	// Finally walk through and post the event to all valid listeners
	for ( nDataArrayItr = 0; nDataArrayItr < m_aEventData.GetCount(); nDataArrayItr++ )
	{
		// Only post events to marked events
		if( m_aEventData[ nDataArrayItr ].GetState() == EEventDataState_MARKED )
		{
			if( FUIEVENTMGR->IsEventInfoEnabled() ) 
				Displayf( "fuiEventManager::HandleEvents - EVENT '%s' RECEIVED by %s", m_Event.GetName(), m_aEventData[ nDataArrayItr ].GetListener()->GetName() );	

			// Post the event and determine if we should consume the event
			if( m_aEventData[ nDataArrayItr ].GetListener()->HandleEvents( &m_Event ) )
			{
				if( FUIEVENTMGR->IsEventInfoEnabled() ) 
					Displayf( "fuiEventManager::HandleEvents - EVENT '%s' CONSUMED", m_Event.GetName() );	

				break;
			}
		}
	}
}

// Comparison method to satisfy search criteria
bool fuiEventHashElement::IsEqual( fuiEventHashElement* pSrcHashElement ) const
{
	if( pSrcHashElement->GetEvent()->GetHash() == m_Event.GetHash() )
			return true;

	return false;
}

} // namespace rage
