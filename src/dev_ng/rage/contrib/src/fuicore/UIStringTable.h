// 
// fuicore/mcUIStringTables.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef UISTRINGTABLES_H
#define UISTRINGTABLES_H

#include "atl/array.h"
#include "atl/map.h"
#include "atl/singleton.h"
#include "text/language.h"
#include "text/stringtable.h"
#include "data/base.h"
#include "diag/output.h"
#include "diag/channel.h"
#include "string/unicode.h"
#include "flash/file.h"
#include "fuicore/Common.h"
#include "system/noncopyable.h"

namespace rage {

#define MAX_STRING_VALUE_SIZE	1023 + 1// 1022  + 1 for string term (NOTE THAT UTF8 CAN BE UP TO 3 BYTES)
#define MAX_STRING_ID_SIZE		1023 + 1 // 15 + 1 for string term
#define GET_STRING(x)			UISTRINGTABLE->GetStringUTF8(x) // MIGRAX - not UTF8?

RAGE_DECLARE_CHANNEL(StringTable);

#define StringTableDebug1(fmt,...)	RAGE_DEBUGF1(StringTable,fmt,##__VA_ARGS__)

extern int StringSpewLevel;


class UIStringTable :  public datBase
{
public:
	enum {MAX_STREAMED_STRING_TABLES = 4};
	enum FILTERTYPE{ST_FRONTEND=1, ST_GAMEPLAY=2,ST_CAREER=4, ST_GARAGE=8, ST_LOADSCREEN=16,ST_ONLINE=32,ST_ALL=0xFF};

	UIStringTable();
	virtual ~UIStringTable();
	void Init(txtLanguage::eLanguage language);
	void Shutdown();

	// PURPOSE:	Loads a string table with current flags
	void Load();   

	// PURPOSE:	Appends a string table with current flags onto the end of the existing loaded data
	void Append(const int cnFileIndex/*index of file to add*/);
	void Append(const char * pStringtableFilename);
	
	// PURPOSE: Append/Remove streamed string tables
	void Append(txtStringTable& tbl, const char * pStringtableFilename);
	void Remove(txtStringTable& tbl);

	// PURPOSE:	UnLoads the current string table
	void UnLoad();

	// PURPOSE:	Sets a game area flag.  This will enable all strings that are specified 
	//          in this area to be loaded when the Load function is called.
	// PARAMS:	gameAreas: game area flag to use when loading
	// PARAMS:	enable: if true, then this game are will be loaded on next load.  Otherwise it will not be loaded   
	void SetFilter(short gameAreas, bool enable);

	// PURPOSE:	Retrieves a string value encoded in UTF8 format out of the current string table
	// PARAMS:	stringId: string id to use when retrieving this string.
	// NOTES:	All dynamic strings associated with this string will be handled
	const char* GetStringUTF8(const char* stringId);
	const char* GetStringUTF8(u32 stringIdHash);

	// PURPOSE:	Sets the value of a dynamic string
	// PARAMS:	stringId: string id to use when retrieving this string.
	// PARAMS:	value: string value that is set to the string
	// NOTES:   Although no error will occur when setting non pure-dynamic strings, those string will get overwritten 
	// on retrieval.  Only pure-dynamic strings will actually keep this value that is set.  pure-dynamic
	// strings are specified with <D>####</D> in our string table
	void SetString(const char* id , const char16* value);
	void SetString(const char* id , const char* value);

	// NOTES: Similar to sprintf().  The dynamic string in the string table is the buffer for this string
	const char* FormatString(const char* id , const char* format, ...);

	// PURPOSE:	Formats and sets the value of a dynamic int which displays as money
	// PARAMS:	stringId: string id to use when retrieving this string.
	// PARAMS:	value: string value that is set to the string
	// NOTES:	The money will be localized accordingly
	void SetMoney(const char* id , s32 money, bool bAddDollarSign = false);

	// PURPOSE:	Formats and sets the value of a dynamic int which displays as money
	// PARAMS:	id: Buffer that will get filled up.
	// PARAMS:	nBufferSize: Length of the 'id' string buffer
	// PARAMS:	value: string value that is set to the string
	// NOTE:		This does not going through the string table
	const char * SetMoneyRaw(char* id, s32 nBufferSize, s32 money, bool bAddDollarSign = false);

	// PURPOSE:	Formats and returns an amount of money given the current language
	// PARAMS:	id: String buffer it will use to fill the formated string
	// PARAMS:	nBufferSize: Length of the 'id' string buffer
	// PARAMS:	money: amount
	// PARAMS:	bAddDollarSign: Whether or not to use the '$'
	const char* LocalizeMoney( char* id, s32 nBufferSize, s32 money, bool bAddDollarSign );

	// PURPOSE:	Sets the current language of the string table
	// NOTES:	This will cause a reload of the string table
	void SetAndLoadLanguage(txtLanguage::eLanguage language);   

	// PURPOSE:	Sets a string in flash with the correct string out of the string table
	// PARAMS:	pContext: context to set the string in
	// PARAMS:	variantName: optional variant ("menu")
	// PARAMS:	propertyName: property to set the string value to
	// PARAMS:	stringId: string id to use when retrieving this string.
	// NOTES:   This is a convenience function for setting a string in a flash movie.  
	//          This is so you don't have to concern yourself with getting string out
	//          of the string table and converting them to UTF8 ect.  Just use this 
	//          to set a string and your done!
	void SetFlashString(swfCONTEXT* pContext, const char* variantName, const char* propertyName,const char* stringID);

	// PURPOSE:	Retrieves the hash value of the string id.  This will allow us to store only the hash value instead of the whole string id
	u32 GetHash(const char* stringId);	

	// PURPOSE:	Outputs debug messages if this string does not exist in the string tables
	// PARAMS:	stringId: string id to check
	void CheckString(const char* stringId);

	const char* CreateDebugString(u32 hash , const char* stringId = NULL);

#if __BANK
	//outputs all strings in the string table
	void PrintStrings();
#endif
	const char* GetString(const char* id);
	const char* GetString(const char16* id);
	const char* GetString(u32 hash , const char* id=NULL);
	const char16* GetString16(u32 hash , const char* id=NULL);

	bool m_NoStringBANG;
	bool GetUsingStringBANG(){return m_NoStringBANG;}
	void SetUsingStringBANG(bool b){m_NoStringBANG = b;}

protected:
	// PURPOSE:	Retrieves a string value encoded in UTF8 format out of the current string table
	// PARAMS:	stringId: string id to use when retrieving this string.
	// NOTES:	All dynamic strings associated with this string will be handled
	char* GetStringUTF8(const char* stringId, char *out, int maxlen);

	// PURPOSE:	Retrieves a string value encoded in UTF8 format out of the current string table
	// PARAMS:	stringIdHash: hash of the string id to use when retrieving this string.
	// NOTES:	All dynamic strings associated with this string will be handled
	char* GetStringUTF8(u32 stringIdHash, char *out, int maxlen);

	//converts from a char16 to a char with UTF-8 encoding.
	//IMPORTANT! the out buffer can use up to 3 bytes per character/glyph.  length should be 3 times the length of the in parameter
	void WideToUtf8(const char16* in, char* out,u32 length);
	void Utf8ToWide(const char* in, char16* out,u32 length);

	// PURPOSE:	Retrieves the maximum length that this string could be
	// NOTES:	the maximum length may be different than the actuall length of the string value
	int GetMaxStringLength(u32 hash);

	//	const char* GetString(u32 hash , const char* id=NULL);
	//	const char* GetString(const char* id);
	char* GetDynamicString(u32 hash);
	bool IsDynamicString(u32 hash);
	void AddToDynamicStringTable(u32 hash);
	void RefreshDynamicString(u32 hash , char* dynamicString, int maxDynamicStringLength);
	void AddSpaceCharacter(char* buffer, char spaceChar, s32 money );

	txtStringData* Get(u32 hash);
	txtStringData* Get(const txtCharString string);	

protected:
	//used for outputting string id's instead of hash values (DEBUG ONLY)
	UIMutex m_HashToIDMapMutex;
	atMap<u32,char*> mp_HashToIDMap;//contains processed values for each string table value
	txtStringTable mpStringTable;
	atFixedArray<txtStringTable*, MAX_STREAMED_STRING_TABLES> mpStreamedStrings;
	
	UIMutex m_StringTableValuesMutex;
	atMap<u32,char*> mpStringTableValues;//contains processed values for each string table value
	txtLanguage::eLanguage mLanguage;
	short mGameAreas;
	u32 mMemoryUsed;
	bool m_DisplayStringId;	

	//
	// Scripting interface:
	//
public:
	static void RegisterScriptCommands();
	static void SetStringCallback(const char *id, const char *value);
	static void SetInt(const char *id, int value);
	static const char* sanGetString( const char *id );
	static void sanAppendTable( const char *strtblFilename );
};

//
// For string-table-streaming requests from script.
//
class UIStringTableStub : sysNonCopyable
{
public:
	UIStringTableStub(const char* filename);
	~UIStringTableStub();

	static void Request(const char* filename);
	static void Unrequest(const char* filename);
	static bool IsLoaded(const char* filename);

	void AddRef() {++m_RefCount; Assert(m_RefCount > 0);}
	void Release() {Assert(m_RefCount > 0); if (!--m_RefCount && m_Loaded) delete this;}
	bool IsLoaded() const {return m_Loaded;}

	static void RegisterScriptCommands();

private:
	static void StringTableLoadedCB(void *data, void *userData, size_t dataSize);
	void LoadedCB(void* pData, size_t size);

	static atMap<u32, UIStringTableStub*> sm_StringTables;

	char			m_Filename[64];
	int				m_RefCount;
	bool			m_Loaded;
	txtStringTable	m_StringTable;
};

typedef atSingleton< UIStringTable > UIStringTableSingleton;
#define UISTRINGTABLE UIStringTableSingleton::InstancePtr()

} // namespace rage

#endif
