// 
// flashuimisc/EventListener.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FLASHUIMISC_EVENTLISTENER_H
#define FLASHUIMISC_EVENTLISTENER_H

// User includes
#include "data/base.h"
#include "bank/bank.h"
#include "atl/pool.h"
#include "atl/array.h"

namespace rage {
// Forward declarations
class fuiEvent;

// Global Event defines
#define FUIEVENTLISTENER_DEFAULT_PRIORITY	10
#define FUIEVENTLISTENER_IGNORE_PRIORITY		-1
#define FUIEVENTLISTENER_MAX_NAME_LENGTH		32

// fuiEventListner declaration //
class fuiEventListener
{
public:
	fuiEventListener(){}
	fuiEventListener( const char* pszListenerName )
	{
		// Copy over the name
		strncpy( m_szName, pszListenerName, FUIEVENTLISTENER_MAX_NAME_LENGTH );
	}

	virtual ~fuiEventListener() {}

	// return value will define whether or not you want to consume the event
	// if true - the event will be consumed and ignore all other event listeners for that event
	// if false - the event will continue to propagate to all other event listeners for that event
	virtual bool HandleEvents( fuiEvent* pEvent ) = 0;

	const char* GetName( void ) const { return m_szName; }
	void SetName(const char* pName){safecpy( m_szName, pName, FUIEVENTLISTENER_MAX_NAME_LENGTH );}

protected:
	char		m_szName[ FUIEVENTLISTENER_MAX_NAME_LENGTH ];	// Identifier used in debug spew
};

enum EEventDataState
{
	EEventDataState_INVALID = -1,

	EEventDataState_DIRTY,
	EEventDataState_UNMARKED,
	EEventDataState_MARKED,

	EEventDataState_COUNT
};

class fuiEventData
{
public:
	fuiEventData()
		:m_pListener( NULL ),
		 m_nPriority( FUIEVENTLISTENER_DEFAULT_PRIORITY ),
		 m_eState ( EEventDataState_INVALID )
	{
		// Nothing for now
	}

	fuiEventData( fuiEventListener* pEventListener, s32 nPriority = FUIEVENTLISTENER_DEFAULT_PRIORITY )
		:m_pListener( pEventListener ),
		 m_nPriority( nPriority ),
		 m_eState ( EEventDataState_INVALID )
	{
		// Nothing for now
	}


	fuiEventData& operator=(const fuiEventData& src)
	{
		m_pListener = src.GetListener();
		m_nPriority = src.GetPriority();
		m_eState = src.GetState();
		return *this;
	}

	void Reset( void )
	{
		m_pListener = NULL;
		m_nPriority = FUIEVENTLISTENER_DEFAULT_PRIORITY;
		m_eState = EEventDataState_INVALID;
	}

	s32 GetPriority( void ) const { return m_nPriority; }
	fuiEventListener* GetListener( void ) const { return m_pListener; }
	EEventDataState GetState( void ) const { return m_eState; }

	void SetPriority( s32 nPriority ) { m_nPriority = nPriority; }
	void SetListener( fuiEventListener* pListener ) { m_pListener = pListener; }
	void SetState( EEventDataState eState ) { Assert( eState > EEventDataState_INVALID && eState < EEventDataState_COUNT ); m_eState = eState; }

protected:
	fuiEventListener*	m_pListener;	// Pointer to the object that will handle the event that is posted
	s32			m_nPriority;	// Used to organize the sequence of event data when registered
	EEventDataState		m_eState;
};

typedef atPool< fuiEventData  >	fuiEventDataPool;
typedef atArray< fuiEventData >	fuiEventDataArray;		
} // namespace rage

#endif // FLASHUIMISC_EVENTLISTENER_H
