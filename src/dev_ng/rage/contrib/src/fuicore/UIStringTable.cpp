// 
// fuicore/UIStringTables.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "fuicore/UIStringTable.h"

#include "atl/array.h"
#include "atl/map.h"
#include "string/unicode.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "data/marker.h"
#include "file/asset.h"
#include "flash/file.h"
#include "system/param.h"
#include "script/wrapper.h"
#include "system/timer.h"
#include "text/hashtable.h"
#include "fuicore/UIStringTable.h"
#include "paging/queuescheduler.h"

PARAM(stringid,"Display String IDs");
PARAM(nostringBANG,"surround string with explamations");

#if __PS3
#define STRING_TABLE_FILENAME "global"
#else
#define STRING_TABLE_FILENAME "global"
#endif

#ifdef __SNC__
#pragma diag_suppress 178
#endif
SCR_DEFINE_NEW_POINTER_PARAM_AND_RET(UIStringTableStub);

namespace rage {

RAGE_DEFINE_CHANNEL(StringTable)

const char m_EmptyString[5] = "";

//static const char* stringNotFound = "String ID Not Found";
//static const char* stringNotSet = "String Not Set, please use SetString() to set the value";

UIStringTable::UIStringTable()
{
	mLanguage = txtLanguage::kEn;
	mGameAreas = 0xff;
	mMemoryUsed=0;
	m_NoStringBANG = false;	
	m_HashToIDMapMutex.SetAssertOnFail(__DEV);
	m_StringTableValuesMutex.SetAssertOnFail(__DEV);
}

UIStringTable::~UIStringTable()
{
   Shutdown();
}

void UIStringTable::Init(rage::txtLanguage::eLanguage language)
{
	RAGE_FUNC();

	mpStringTable.Init();

	mGameAreas = 0xff;

	mMemoryUsed=0;

#if __BANK
	rage::bkBank& b = BANKMGR.CreateBank("String Table");

	b.AddToggle("Display String Id instead of value",&m_DisplayStringId);   
	b.AddButton("Print Current Strings",rage::datCallback(MFA(UIStringTable::PrintStrings), this));   
#endif

	m_DisplayStringId = false;

#if __DEV
	if(PARAM_stringid.Get())
	{
		m_DisplayStringId = true;
	}

	if(PARAM_nostringBANG.Get())
	{
		m_NoStringBANG = true;
	}
#endif

	txtStringTable::SetMissingModeNoCreate();

	// Load the string table
	SetFilter(UIStringTable::ST_ALL , true);
	SetAndLoadLanguage(language);
}

void UIStringTable::RegisterScriptCommands()
{
	SCR_REGISTER_SECURE(StringTable_SetString, 0x521fa0d7, UIStringTable::SetStringCallback);
	SCR_REGISTER_SECURE(StringTable_SetInt, 0x873c5b6c, UIStringTable::SetInt);
	SCR_REGISTER_SECURE(StringTable_GetString, 0x8841214b, UIStringTable::sanGetString);
	SCR_REGISTER_SECURE(STRINGTABLE_APPEND_TABLE, 0xb3e44649, UIStringTable::sanAppendTable);
}

void UIStringTable::SetStringCallback(const char *id, const char *value)
{
	UISTRINGTABLE->SetString(id,value);
}

void UIStringTable::SetInt(const char *id, int value)
{
	char szTemp[10];
	sprintf(szTemp,"%d",value);

	UISTRINGTABLE->SetString(id,szTemp);
}

const char* UIStringTable::sanGetString( const char * /*id */)
{
	//return UIStringTable->GetStringUTF8( id );
	Assert(0);
	return NULL;
}

void UIStringTable::sanAppendTable( const char *strtblFilename )
{
	Assert(UISTRINGTABLE);

	UISTRINGTABLE->Append(strtblFilename);
}

void UIStringTable::Shutdown()
{
	UnLoad();

	UI_LOCK(m_HashToIDMapMutex);
	atMap<u32,char*>::Iterator entry = mp_HashToIDMap.CreateIterator();
	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		if (entry)
		{
			//u32 key = entry.GetKey();
			char* data = entry.GetData();

			//delete key;//user owns the key
			delete data;
		}
	}

	mp_HashToIDMap.Kill();
	mpStringTable.Kill();
}

void UIStringTable::UnLoad()
{
	UI_LOCK(m_StringTableValuesMutex);
	atMap<u32,char*>::Iterator entry = mpStringTableValues.CreateIterator();
	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		if (entry)
		{
			//u32 key = entry.GetKey();
			char* data = entry.GetData();

			//delete key;//user owns the key
			delete data;
		}
	}

	mpStringTableValues.Kill();
}

#if __BANK
//outputs all strings in the string table
void UIStringTable::PrintStrings()
{
	UI_VERIFY_UNLOCKED(m_StringTableValuesMutex);
	int i;
	rage::Displayf("--Main String Table--");
	for(i=0;i<mpStringTable.GetNumIdentifiers();i++)
	{
		rage::Displayf("[%d] %s = %s",i,mpStringTable.GetIdentifier(i),Get(mpStringTable.GetIdentifier(i))->GetStringUTF8());
	}

	rage::Displayf("--Dynamic Table--");
	atMap<u32,char*>::Iterator entry = mpStringTableValues.CreateIterator();
	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		if (entry)
		{
			rage::Displayf("[%d] %d = %s",i,entry.GetKey(),entry.GetData());
		}

	}

	rage::Displayf("Dynamic Table Data size = %d",mMemoryUsed);
}
#endif

void UIStringTable::SetFlashString(swfCONTEXT* pContext, const char* variantNameString, const char* propertyNameString ,const char* stringID)
{
	AssertMsg(stringID , "You are trying to look up a NULL pointer in LookupString for the string table.");

	swfVARIANT propertyName(propertyNameString);
	swfBASE* pMenu = pContext->GetVariant(variantNameString)->asObject();
	swfVARIANT property;
	const char* pString="";

	if (pMenu)
	{
		pMenu->GetProperty( propertyName, property,pContext );

		if (strlen(stringID))
		{
			pString = GetStringUTF8(stringID);
		}

		property.Set( pString );
		pMenu->SetProperty( propertyName, property,pContext );
	}
	else
	{
		pContext->SetGlobal(propertyNameString,GetStringUTF8(stringID));
	}
}

void UIStringTable::WideToUtf8(const char16* in, char* out,rage::u32 length)
{
	char* outPtr = out;
	for(rage::u32 i = 0; i < length-4 && in[i] != /*_T*/('\0'); i++)
	{
		int bytes = rage::WideToUtf8Char(in[i], outPtr);
		outPtr+=bytes;
	}

	*outPtr = '\0';
}

void UIStringTable::Utf8ToWide(const char* in, char16* out,rage::u32 length)
{   
	rage::u32 i;

	for(i = 0; i < length && in[0] != '\0'; i++)
	{
		int bytes = rage::Utf8ToWideChar(in, out[i]);
		in+=bytes;
	}

	if (i >= length){i = length-1;}

	out[i] = /*_T*/('\0');
}

void UIStringTable::SetFilter(short gameAreas, bool enable)
{
	short mask = 0xFF;

	if (enable)
	{
		mGameAreas |= gameAreas;
	}
	else
	{
		mask = (short)~gameAreas;
		mGameAreas &= mask;
	}
}

int UIStringTable::GetMaxStringLength(u32 hash)
{
    //get string
    rage::txtStringData* pData = Get(hash);
	const char* s;
	int dStart=0;
	char dynamicId[MAX_STRING_ID_SIZE];   
	int processedStringLength=0;
	int length;

	if (pData)
	{
		s = pData->GetStringUTF8();
		length = strlen(s);

		for(int i=0;i<length;i++)
		{
			if (strncmp(&(s[i]),"<D>",3)==0)
			{
				//string is dynamic
				i+=3;
				dStart=i;
			}
			else if (strncmp(&(s[i]),"</D>",3)==0)
			{
				//insert string referenced in the <D></D> tags
				safecpy(dynamicId,&(s[dStart]),i-dStart+1);

				Assertf((i-dStart)+1<MAX_STRING_ID_SIZE,"one of the Id's in our string table .xls spreadsheet is > %",MAX_STRING_ID_SIZE);
				dynamicId[i-dStart] = '\0';

				//check to see if this is a pure dynamic entry with format <D>#</D>
				if (atoi(dynamicId))
				{
					return atoi(dynamicId);
				}
				else
				{            
					processedStringLength += GetMaxStringLength(GetHash(dynamicId));
				}
			}
			else
			{
				//copy this 1 char
				processedStringLength++;
			}
		}
	}
	else
	{
		//string not found in static string table, lets check to see if it is in the dynamic string table as a debug string
		s = GetDynamicString(hash);
		if (!s)
		{
			s = CreateDebugString(hash,NULL);
			
		}
		return strlen(s);
	}
   return processedStringLength;
}

char* UIStringTable::GetDynamicString(u32 hash)
{
	UI_VERIFY_UNLOCKED(m_StringTableValuesMutex);

	//grab it out of the values table and return that string
	char** stringValue = mpStringTableValues.Access(hash);
	if (stringValue)
	{
		return *stringValue;
	}
	else
	{
		return NULL;
	}
}

void UIStringTable::RefreshDynamicString(u32 hash , char* dynamicString, int maxDynamicStringLength)
{
	const char* s;
	int length;
	int dStart=0;
	char dynamicId[MAX_STRING_ID_SIZE];   
	int processedStringLength=0;
	bool parsingDynamicString=false;

	//retrieve the source string from the static string table
	rage::txtStringData* pData = Get(hash);
	if (pData)
	{
		s = pData->GetStringUTF8();
		length = strlen(s);
	}
	else
	{
		StringTableDebug1("SOMETHING IS WRONG WITH THE STRINGS!!, WE SHOULD NOT BE HERE!");
		return;
	}

	for(int i=0;i<length;i++)
	{
		if (strncmp(&(s[i]),"<D>",3)==0)
		{
			//string is dynamic
			i+=3;//consume <D>
			dStart=i;
			parsingDynamicString = true;
		}
		else if (strncmp(&(s[i]),"</D>",3)==0)
		{
			//insert string referenced in the <D></D> tags
			strncpy(dynamicId,&(s[dStart]),i-dStart);
			Assertf((i-dStart)+1<MAX_STRING_ID_SIZE,"one of the Id's in our string table .xls spreadsheet is > %",MAX_STRING_ID_SIZE);
			dynamicId[i-dStart] = '\0';

			//check to see if this is a pure dynamic entry with format <D>#</D>
			if (atoi(dynamicId))
			{
				return;//do nothing
			}
			else
			{
				//this is an embedded string i.e. "some text <D>stringID</D> some other text"
				u32 dynamicHash = GetHash(dynamicId);

				const char* embeddedString = GetString(dynamicHash , dynamicId);

				//appended it to the current dynamic string
				strncpy(&(dynamicString[processedStringLength]),embeddedString,Min(maxDynamicStringLength-processedStringLength,GetMaxStringLength(dynamicHash)));				
				processedStringLength += Min((int)strlen(embeddedString),GetMaxStringLength(dynamicHash));
				Assert(processedStringLength < maxDynamicStringLength);
			}
			parsingDynamicString = false;
			i+=3;//consume </D>
		}
		else
		{
			if (parsingDynamicString==false)
			{
				//copy this 1 char
				dynamicString[processedStringLength] = s[i];
				processedStringLength++;
				Assert(processedStringLength < maxDynamicStringLength);
			}
		}
	}

	//set the string term
	dynamicString[processedStringLength] = '\0';
}

const char* UIStringTable::GetString(const char* id)
{
	return GetString(GetHash(id),id);
}

const char16* UIStringTable::GetString16(u32 hash , const char* /*id*/)
{
	char16* dynamicString = NULL;
	rage::txtStringData* pData = Get(hash);

	dynamicString = NULL; // GetDynamicString(hash);
	if(!dynamicString)
	{		
		//string id does not exist in dynamic string table so try the static string table
		if (pData)
		{
			return pData->GetString();
		}
		else
		{
			//string not found in any tables, return a debug string
			return (const char16*)L"NOTFOUND"; // CreateDebugString(hash , id);
		}
	}

	if (pData)//note that if this is null, it means that we created a debug string and added it to the dynamic table
	{
		//this is a valid dynamic string so lets refresh the value of it
		// RefreshDynamicString(hash,dynamicString,GetMaxStringLength(hash));
		Assert(0);
	}

	return dynamicString;
}

//get string from static or dynamic string table
const char* UIStringTable::GetString(u32 hash , const char* id)
{
	char* dynamicString = NULL;
	rage::txtStringData* pData = Get(hash);

	dynamicString = GetDynamicString(hash);
	if(!dynamicString)
	{		
		//string id does not exist in dynamic string table so try the static string table
		if (pData)
		{
			return pData->GetStringUTF8();
		}
		else
		{
			//string not found in any tables, return a debug string
			return CreateDebugString(hash , id);
		}
	}

	if (pData)//note that if this is null, it means that we created a debug string and added it to the dynamic table
	{
		//this is a valid dynamic string so lets refresh the value of it
		RefreshDynamicString(hash,dynamicString,GetMaxStringLength(hash));
	}

	return dynamicString;
}

u32 UIStringTable::GetHash(const char* stringId)
{	
	u32 hash = mpStringTable.Hash(stringId);

	//create a map from id to hash value for dev only
#if __DEV
	//map hash to string if it has not been done yet
	UI_VERIFY_UNLOCKED(m_HashToIDMapMutex);
	char** pIDBuffer = mp_HashToIDMap.Access(hash);
	if (!pIDBuffer)
	{
		u32 length = strlen(stringId);
		UI_LOCK(m_HashToIDMapMutex);
		char* newString = rage_new char[length+1];
		safecpy(newString,stringId,length+1);
		mp_HashToIDMap.Insert(hash,newString);
	}
#endif

	return hash;
}

const char* UIStringTable::CreateDebugString(u32 hash , const char* stringId)
{
	UI_VERIFY_UNLOCKED(m_HashToIDMapMutex);
	char** pIDBuffer = mp_HashToIDMap.Access(hash);			
	char debugString[MAX_STRING_ID_SIZE];

	if (pIDBuffer)
	{
		StringTableDebug1("[STRING TABLE] [%s] Please add to the string table", *pIDBuffer);
		safecpy(debugString , *pIDBuffer , MAX_STRING_ID_SIZE);
	}
	else  if (stringId)
	{
		StringTableDebug1("[STRING TABLE] [%s] Please add to the string table", stringId);
		safecpy(debugString , stringId , MAX_STRING_ID_SIZE);
	}
	else
	{
		StringTableDebug1("[STRING TABLE] UNKNOWN STRING. Please add to the string table");
		safecpy(debugString , "Please add to the string table",MAX_STRING_ID_SIZE);
	}

	//see if we need to create a new entry in the dynamic string table for this
	char* pValue = GetDynamicString(hash);
	if (!pValue)
	{
		//allocate mem for string value
		UI_LOCK(m_StringTableValuesMutex);
		pValue = rage_new char[MAX_STRING_ID_SIZE+4];//+4 FOR exclamations !!ID!!
		mMemoryUsed += MAX_STRING_ID_SIZE+4;

		//insert
		mpStringTableValues.Insert(hash,pValue);

		StringTableDebug1("Allocated %d bytes for debug string: %s",MAX_STRING_ID_SIZE+4,debugString);

#if __DEV
		if(!m_NoStringBANG)
		{
			formatf(pValue,MAX_STRING_ID_SIZE+4,"!!%s!!",debugString);
		}
		else
#endif
		{
			formatf(pValue,MAX_STRING_ID_SIZE+4,"%s",debugString);
		}

		return pValue;
	}
	
	safecpy(pValue , debugString , MAX_STRING_ID_SIZE);
	pValue[MAX_STRING_ID_SIZE-1] = '\0';
	return pValue;
}

//get a string out of the string table.  
//The string will be processed for any dynamic entries
//The returned string is encoded in UTF-8
char* UIStringTable::GetStringUTF8(const char* stringId, char *out, int maxlen)
{
	AssertMsg(stringId , "You are trying to look up a NULL pointer in LookupString for the string table.");

	if (!stringId || strlen(stringId) == 0)
	{	
		strncpy(out,m_EmptyString,maxlen);
	}

	if (m_DisplayStringId)
	{
		strncpy(out,stringId,maxlen);
	}
	else
	{
		u32 hash = GetHash(stringId);
		const char16* pValue = GetString16(hash , stringId);
		::rage::WideToAscii(out, pValue, maxlen);
		Warningf("UTF8 for: '%s'",stringId);
	}
	return out;
}

const char* UIStringTable::GetStringUTF8(const char* stringId)
{
	AssertMsg(stringId , "You are trying to look up a NULL pointer in LookupString for the string table.");

	if (!stringId || strlen(stringId) == 0)
	{
		return m_EmptyString;
	}

	u32 hash = GetHash(stringId);

	const char* pValue = GetString(hash , stringId);

	if (m_DisplayStringId)
	{
		return stringId;
	}
	else
	{
		return pValue;
	}
}

//get a string out of the string table.  
//The string will be processed for any dynamic entries
//The returned string is encoded in UTF-8
char* UIStringTable::GetStringUTF8(u32 hash, char *out, int maxlen)
{
	const char16* uistring = GetString16(hash);
	::rage::WideToAscii(out, uistring, maxlen);
	return out;
}
const char* UIStringTable::GetStringUTF8(u32 hash)
{
	return GetString(hash);
}

//just forces a getstring which spews stuff for this string if it is not in the string table
void UIStringTable::CheckString(const char* stringId)
{
	GetString(stringId);
}

void UIStringTable::SetString(const char* stringId , const char16* value)
{
	AssertMsg(stringId , "You are trying to look up a NULL pointer in LookupString for the string table.");

	//get string
	u32 hash = GetHash(stringId);
	UI_VERIFY_UNLOCKED(m_StringTableValuesMutex);
	char** pValueBuffer = mpStringTableValues.Access(hash);

	if (pValueBuffer)
	{
		WideToUtf8(value,*pValueBuffer,GetMaxStringLength(GetHash(stringId)));
	}
	else
	{
		StringTableDebug1("could not find string %s",stringId);
	}
}

void UIStringTable::SetString(const char* stringId , const char* value)
{
	AssertMsg(stringId , "You are trying to look up a NULL pointer in LookupString for the string table.");

	//get string
	u32 hash = GetHash(stringId);

	UI_LOCK(m_StringTableValuesMutex);
	char** pValueBuffer = mpStringTableValues.Access(hash);

	if (pValueBuffer)
	{
		safecpy(*pValueBuffer, value, GetMaxStringLength(GetHash(stringId)));
	}
	else
	{
		StringTableDebug1("could not find string %s",stringId);
	}
}

const char* UIStringTable::FormatString(const char* stringId , const char* fmt, ...)
{
	AssertMsg(stringId , "You are trying to look up a NULL pointer in LookupString for the string table.");

	u32		hash = GetHash(stringId);
	UI_VERIFY_UNLOCKED(m_StringTableValuesMutex);
	char**	pValueBuffer = mpStringTableValues.Access(hash);
	va_list args;

	if (pValueBuffer)
	{
		va_start( args, fmt );		
		return vformatf( *pValueBuffer, GetMaxStringLength(GetHash(stringId)), fmt, args );
	}
	else
	{
		StringTableDebug1("could not find string %s",stringId);
		return stringId;
	}
}

void UIStringTable::SetAndLoadLanguage(rage::txtLanguage::eLanguage language)
{
#if __DEV
	sysTimer T;
	Printf("Loading Stringtable...");
#endif

	mLanguage = language;
	Load();

#if __DEV
	Printf("Done. (%6.4f seconds)\n",T.GetTime());
#endif
}

bool UIStringTable::IsDynamicString(u32 hash)
{
	rage::txtStringData* pData = Get(hash);
	const char* s = pData->GetStringUTF8();

	if (strstr(s,"<D>"))
	{
		return true;
	}

	return false;
}

void UIStringTable::AddToDynamicStringTable(u32 hash)
{	
	UI_LOCK(m_StringTableValuesMutex);
	u32 maxLength = GetMaxStringLength(hash) + 1;

	//allocate mem for string value
	char* pValue = rage_new char[maxLength];
	mMemoryUsed += maxLength;
	pValue[0] = '\0';

	//insert
	mpStringTableValues.Insert(hash,pValue);

#if __DEV
	UI_VERIFY_UNLOCKED(m_HashToIDMapMutex);
	char** pIDBuffer = mp_HashToIDMap.Access(hash);			
	if (pIDBuffer)
	{
		StringTableDebug1("Allocated %d bytes for %s",maxLength,*pIDBuffer);
	}
#endif
}

#define DOWNLOADABLE_STRINGTABLE 0

void UIStringTable::Load()
{
	//unload current table
	mpStringTable.Kill();

	// Load the main game stringtable
#if __DEV
	mpStringTable.Load(STRING_TABLE_FILENAME,mLanguage,mGameAreas,true,false,false,false,UTF8);
#else
	mpStringTable.Load(STRING_TABLE_FILENAME,mLanguage,mGameAreas,true,false,false,false,UTF8);
#endif

	// Append (potential) downloadable content stringtables - note these files may be missing (and probably will be on 360 where content is mounted after 'press start')
	// should this be an optional feature, if anything?
	for(int i=1;i<=8;i++)
	{
		Append(i);
	}

	StringTableDebug1("allocating dynamic string table...");

	txtHashPosition stringEntry;
	txtStringData* stringData;
	u32 hash;

	//allocate mem for the dynamic string table
	if (mpStringTable.GetFirstEntry(stringEntry))
	{
		stringData = stringEntry.GetData();
		hash = stringData->GetHash();

		if (IsDynamicString(hash))
		{
			AddToDynamicStringTable(hash);
		}

		while (mpStringTable.GetNextEntry(stringEntry))
		{
			stringData = stringEntry.GetData();
			hash = stringData->GetHash();

			if (IsDynamicString(hash))
			{
				AddToDynamicStringTable(hash);
			}
		}
	}

#if __DEV	
	StringTableDebug1("Allocated a total of %d bytes for the dynamic string table",mMemoryUsed);
	StringTableDebug1("Allocated a total of %d bytes for the static string table",mpStringTable.GetMemoryAllocated());
#endif
}


void UIStringTable::Append(const char * pStringtableFilename)
{
//	rage::ASSET.PushFolder("$/ui");	//no need to force a hard-coded path on projects
#if __DEV
	const bool bLoadIdentifiers = true;
#else
	const bool bLoadIdentifiers = false;
#endif

	mpStringTable.Load(pStringtableFilename,mLanguage,mGameAreas,bLoadIdentifiers,true,false,false,UTF8);

	//	rage::ASSET.PopFolder();
}

void UIStringTable::Append(txtStringTable& tbl, const char * pStringtableFilename)
{
#if __DEV
	const bool bLoadIdentifiers = true;
#else
	const bool bLoadIdentifiers = false;
#endif

	tbl.Load(pStringtableFilename,mLanguage,mGameAreas,bLoadIdentifiers,true,false,false,UTF8);
	mpStreamedStrings.Push(&tbl);
}

void UIStringTable::Remove(txtStringTable& tbl)
{
	mpStreamedStrings.Delete(mpStreamedStrings.Find(&tbl));
}

txtStringData* UIStringTable::Get(u32 hash)
{
	txtStringData* str = mpStringTable.Get(hash);
	for(int i=0; !str && i<mpStreamedStrings.GetCount(); ++i)
		str = mpStreamedStrings[i]->Get(hash);
	return str;
}

txtStringData* UIStringTable::Get(const txtCharString string)
{
	return Get(mpStringTable.Hash(string));
}


//PURPOSE: Append extra stringtables (from downloadable content usually)
void UIStringTable::Append(const int cnFileIndex/*index of file to add*/)
{
	rage::ASSET.PushFolder("ui");

	// Append (potential) downloadable content stringtables - note these files may be missing
	char filename[64];
	formatf(filename, sizeof(filename)-1, "%s_%04d", STRING_TABLE_FILENAME, cnFileIndex);
	Append(filename);

	rage::ASSET.PopFolder();

}



void UIStringTable::AddSpaceCharacter(char* buffer, char spaceChar, rage::s32 money)
{
	int moneyLength = 0;
	int numSpaceCharacters = 0;
	int i=0;

	sprintf(buffer, "%d",money);
	i = moneyLength = strlen(buffer);

	if (money < 0)
	{
		//compensate for the negative sign
		--moneyLength;
	}

	//just gonna do if stmts instead of log
	if (moneyLength < 4){numSpaceCharacters=0;}
	else if(moneyLength < 7){numSpaceCharacters=1;}
	else if(moneyLength < 10){numSpaceCharacters=2;}
	else if(moneyLength < 13){numSpaceCharacters=3;}
	else {numSpaceCharacters=4;}

	buffer[i+numSpaceCharacters] = 0;//string term

	while(numSpaceCharacters > 0)
	{
		//shift right numCommas digits and add a comma
		buffer[i+numSpaceCharacters-1] = buffer[i-1];
		buffer[i+numSpaceCharacters-2] = buffer[i-2];
		buffer[i+numSpaceCharacters-3] = buffer[i-3];		
		buffer[i+numSpaceCharacters-4] = spaceChar;
		i -= 3;
		numSpaceCharacters--;		
	}
}
void UIStringTable::SetMoney(const char* id, rage::s32 money, bool bAddDollarSign)
{
	char buffer[64];
	SetString( id, LocalizeMoney( buffer, 64, money, bAddDollarSign ));//note that this function copies buffer into the string tables internal value buffer. So it's safe to be on the stack
}

const char* UIStringTable::SetMoneyRaw(char* id, rage::s32 nBufferSize, rage::s32 money, bool bAddDollarSign)
{
	return LocalizeMoney( id, nBufferSize, money, bAddDollarSign );	
}

const char* UIStringTable::LocalizeMoney( char* id, rage::s32 nBufferSize, rage::s32 money, bool bAddDollarSign )
{
	char buffer[64];
	switch(mLanguage)
	{
		case txtLanguage::kEn:
		{
			AddSpaceCharacter( buffer, ',', money );
			if( bAddDollarSign )
				formatf( id, nBufferSize, "%s%s", "$", buffer );
			else
				safecpy( id, buffer, nBufferSize );

			break;
		}
		case txtLanguage::kDe:
		{
			AddSpaceCharacter( buffer, '.', money );
			if( bAddDollarSign )
				formatf( id, nBufferSize, "%s%s", "$", buffer );
			else
				safecpy( id, buffer, nBufferSize );

			break;
		}
		case txtLanguage::kEs:
		case txtLanguage::kIt:
		{
			AddSpaceCharacter( buffer, '.', money );
			if( bAddDollarSign )
				formatf( id, nBufferSize, "%s%s", buffer, "$" );
			else
				safecpy( id, buffer, nBufferSize );

			break;
		}
		case txtLanguage::kFr:
		{
			AddSpaceCharacter( buffer, '|', money );
			if( bAddDollarSign )
				formatf( id, nBufferSize, "%s%s", buffer, "$" );
			else
				safecpy( id, buffer, nBufferSize );

			break;
		}	
		default:
		{
			AddSpaceCharacter( buffer, ',', money );
			if( bAddDollarSign )
				formatf( id, nBufferSize, "%s%s", "$", buffer );
			else
				safecpy( id, buffer, nBufferSize );

			break;
		}
	}

	return id;
}

void UIStringTableStub::RegisterScriptCommands()
{
	SCR_REGISTER_SECURE(REQUEST_STRING_TABLE, 0x82b4dcce, UIStringTableStub::Request);
	SCR_REGISTER_SECURE(HAS_STRING_TABLE_LOADED, 0x12d77eec, UIStringTableStub::IsLoaded);
	SCR_REGISTER_SECURE(REMOVE_STRING_TABLE, 0x6857e514, UIStringTableStub::Unrequest);
}

atMap<u32, UIStringTableStub*> UIStringTableStub::sm_StringTables;

UIStringTableStub::UIStringTableStub(const char* filename)
:	m_RefCount(0)
,	m_Loaded(false)
{
	safecpy(m_Filename, filename);
	m_StringTable.Init();
}

void UIStringTableStub::Request(const char* /*filename*/)
{
#if 0// HACK!
	u32 key = atStringHash(filename);
	UIStringTableStub*& pStringTable = sm_StringTables[key];
	if (!pStringTable)
	{
		pStringTable = rage_new UIStringTableStub(filename);
		float fRequestPriority = -200.0f;
		pgQueueScheduler& rScheduler = static_cast<pgQueueScheduler&>(pgQueueScheduler::GetScheduler());
		char fullpath[256];
		formatf(fullpath, "$/content/stringtable/%s.strtbl", filename);
		rScheduler.Schedule(fullpath, fRequestPriority, UIStringTableStub::StringTableLoadedCB, pStringTable);
	}
	pStringTable->AddRef();
#endif
}

void UIStringTableStub::Unrequest(const char* filename)
{
	u32 key = atStringHash(filename);
	UIStringTableStub*& pStringTable = sm_StringTables[key];
	Assertf(pStringTable, "String table unrequested more times than it was requested?");
	pStringTable->Release();
}

UIStringTableStub::~UIStringTableStub()
{
	UISTRINGTABLE->Remove(m_StringTable);
	m_StringTable.Kill();
	u32 key = atStringHash(m_Filename);
	sm_StringTables.Delete(key);
}

void UIStringTableStub::StringTableLoadedCB(void* data, void* userData, size_t dataSize)
{
	static_cast<UIStringTableStub*>(userData)->LoadedCB(data, dataSize);
}

void UIStringTableStub::LoadedCB(void* pData, size_t len)
{	
	if (!m_RefCount)
	{
		// string table was subsequently unrequested
		delete [](char*)pData;
		delete this;
		return;
	}
	char filename[256];
	fiDevice::MakeMemoryFileName(filename, sizeof(filename), pData, len, true, m_Filename);
	UISTRINGTABLE->Append(m_StringTable, filename);
	m_Loaded = true;
}

bool UIStringTableStub::IsLoaded(const char* filename)
{
	u32 key = atStringHash(filename);
	UIStringTableStub** pStringTable = sm_StringTables.Access(key);
	return pStringTable && (*pStringTable)->IsLoaded();
}

} // namespace rage
