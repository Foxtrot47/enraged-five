// 
// uicore/UIManager.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#ifndef UICORE_UIMANAGER_H
#define UICORE_UIMANAGER_H

//core ui includes
#include "fuicore/Event.h"
#include "fuicore/EventListener.h"
#include "fuicomponents/UIObject.h"
#include "fuicore/TextureStream.h"

//core rage includes
#include "bank/bank.h"
#include "atl/queue.h"
#include "atl/map.h"
#include "atl/string.h"
#include "atl/delegate.h"
#include "vector/matrix34.h"
#include "atl/singleton.h"
#include "input/mapper.h"
#include "grcore/viewport.h"
#include "rline/rlworker.h"
#include "snu/snusocket.h"
//#include "scriptobjects/eventobjects.h"

namespace rage {

class UIManager;
class UINavigator;
class UIFactory;
class UICamera;
class UIPlayer;
class UIGame;
class UICommand;
class UIInput;
class UIVariant;
class UIEvent;
class UIEventHandler;
class UIObject;	
class xmlAsciiTokenizerXml;
class vhsmExtObject;
class UITransition;
class UIObjectState;
class UIEventManager;
struct UIInfo;
struct UIMutex;
//class UITextureStream;
 
//let the subclass singleton us #define UIMANAGER UIManagerSingleton::InstancePtr()
//#define SAFE_FRAME_WIDTH (MCRENDERMGR.GetUpdateViewport()->GetWidth()*.10f)
//#define SAFE_FRAME_HEIGHT (MCRENDERMGR.GetUpdateViewport()->GetHeight()*.10f)

#define FUI_SCR_REGISTER( class , function) \
	SCR_REGISTER( class ## _ ##function , fui##class::function )

//==============================================================================================================================
struct fuiDelayedEvent
//==============================================================================================================================
{
	fuiDelayedEvent( bool bPauseSensitive=true )
	: m_fCurrentTime	( 0.0f )
	, m_bPauseSensitive	(bPauseSensitive)
	, m_pEvent			(NULL)
	{
		// Nothing for now
	}

	bool IsEnabled	( void ) { return m_pEvent!=NULL; }
	void Disable	( void );
	void Update		( float fDeltaTime );
	void Register	( fuiEvent*, f32 fDelayTime, bool bPauseSensitive);

	bool		m_bPauseSensitive;
	f32			m_fCurrentTime;
	fuiEvent*	m_pEvent;
};

typedef atArray<fuiDelayedEvent> fuiDelayedEventArray;
//==============================================================================================================================

class ConfigRebootData
{
public:
	// We can 'subclass' this at will (to support diffent types/data), but this is the basic reboot data
	// The JOINWISH type is dependant on additional data not in the base class
	// This basic data is read at the very start of the game

	enum RebootType {NONE,DONGLE,PROFILE_CHANGE,JOINWISH,MOUNT_DOWNLOADABLECONTENT};

	RebootType			m_eType;
	int					m_iRebootPad;

	ConfigRebootData()
	{
		m_eType	= ConfigRebootData::NONE;
		m_iRebootPad = -1;
	}
};

class RebootInstance : public ConfigRebootData
{
public:
	void Set(const ConfigRebootData & data)						// write over the base class with the data from mcConfig (allows us to modify the mcConfig version of the data - something of a hack)
	{
		memcpy( this, &data, sizeof(class RebootInstance) ); 
	}


public:
	snuGamerJoinWish	m_JoinWish;

	RebootInstance() : ConfigRebootData()
	{
		m_JoinWish.Reset();
	}
};

class RebootThread : private rlWorker
{
	RebootInstance m_Instance;

public:

	bool Fire(const char * szType);
	void Perform();	
};

/*
	The UIFactory class manages creation UI elements.  UI elements may be UIObjects or 
	any other UI related construct.  This factory supports the creation of UIObjects
	through xml.
*/
class UIFactory : public datBase {
public:
	friend class	UIVariant;
	friend class	UICommand;
	friend class	UIEvent;
	friend class	UIEventHandler;
	friend class	UIObject;	

	typedef UIObject*(*UIObjectFactoryFunc)(void);
		
	//PURPOSE
	//Construction/Destruction *
	UIFactory();
	virtual ~UIFactory();
		
	//PURPOSE
	//Accessor wich retrieves a UI object.  The objects are stored in a map, but this function is
	//only sporatically called throughout games so speed should not be an issue.  NEVER call this in
	//update/draw paths 
	UIObject* GetUIObject(const char* szName){return GetUIObjectByIdName(szName);}

	//PURPOSE
	//Use this function to create any string that is not intended to be in the string table.
	//Strings such as UIObject names, UIEvent names, UIEventHandler names, ect, should use this. 
	//This may be optimized for RELEASE builds to use hash values instead of strings */
	const char* CreateString(const char *szString);

	//PURPOSE
	//Accessor to set the root UIObject in the UI heirarchy 
	void SetUserInterface(UIObject* pObject){m_pUserInterface = pObject;}

	//PURPOSE
	//loads a user interface heirarchy from xml 
	bool LoadFromXml(const char *szFileName);

	//PURPOSE
	//called strategically after um...loading 
	void PostLoad();	

	//PURPOSE
	//Registers a class type for use when instantiating UIObjects.  You can then call NewUIObject() to instantiate them
	void RegisterUIObject(const char *name, UIObjectFactoryFunc func);

	//PURPOSE
	//Instantiates a UIObject with the type given by the string param.  The string must be registered via RegisterUIObject()
	UIObject* NewUIObject(const char* UIObjectType);	

	//PURPOSE
	//Do not call these directly.  Use the REGISTER_UIBOJECT macro
	template <typename T> static UIObject* CreateObject()
	{ 
#if __DEV
		m_UIObjectBytes += sizeof(T);
#endif
		return rage_new T; 
	}

	//PURPOSE
	//Do not call these directly.  Use the REGISTER_SINGLETON_UIBOJECT macro
	template <typename T> static UIObject* CreateSingletonObject()
	{ 
		if (!T::IsInstantiated())
		{
#if __DEV
		m_UIObjectBytes += sizeof(T);
#endif
		T::Instantiate(); 
		}

		return T::InstancePtr();
	}
	static void RegisterObjects();	

#if __DEV
	void PrintMemoryUsage();
	//typedef UIObject*(*UIObjectSizeOfFunc)(void);
	//template <typename T> static int SizeOfObject() { return sizeof(T); }
	//atMap<const char*,UIObjectSizeOfFunc> m_ObjectSizeMap;
#endif

	UIEventHandler*	NewEventHandler(UIObject* pObject);

protected:
	//xml parsing
	enum UIReadMode
	{
		UIReadMode_All,
		UIReadMode_ClassOnly,
		UIReadMode_SkipAll,
	};
	
	//accessors
	UIObject* GetUIObjectByIdName(const char* szID);
	const UIObject*	GetUIObjectByIdName(const char* szID)const;

	//UI element loading and allocation functions
	virtual UIObject* CreateUIObject(const char *UIObjectType);
	UIObject* CreateNewChild(const char *szName , const char *szClass);	
	bool LoadFromXml(const char* classname, UIObject* pObject,xmlAsciiTokenizerXml& _xmlTokenizer, const UICommand& arg, UIFactory::UIReadMode readMode);
	bool LoadEventHandler(const char* tag, const char* name, const UICommand& arg,UIObject* pObject,xmlTokenizerHack* pTokenizer);	
	bool ReadElementsFromXml(const char* classname,UIObject* pObject, xmlAsciiTokenizerXml&, const UICommand& rArg, UIFactory::UIReadMode, int& childReads);
	
	UIObject* NewChild(UIObject* pUIObject,const char *szName, const char *stateClass);
	fuiEvent* CreateNewEvent(const char* id);	
	void RegisterAllUIScriptCommands();	
	const char* GetClassName(int index){return m_FactoryMap.GetEntry(index)->key;}

	//all the UIObjects that are ever allocated should be included in this map
	atMap<atString,UIObject*, atMapCaseInsensitiveHashFn, atMapCaseInsensitiveEquals> m_UIObjects;
	static int m_UIObjectCount;
	static int m_UIObjectBytes;

	//used for first pass of xml parsing only
	rage::atMap<rage::atString,rage::atString> m_UIObjectTypes;

	rage::atMap<int,const char*> m_StringIds;

	//root UIObject
	UIObject* m_pUserInterface;

	atMap<const char*,UIObjectFactoryFunc> m_FactoryMap;
};

class UICamera {
public:
	UICamera();
	virtual ~UICamera(){}

	void UnPauseCamera();
	void PauseCamera(rage::s32);
	void PauseCamera(const rage::Matrix34* m,rage::f32);
	void OnSetCamera(Matrix34& camMtx, float& fFov, float& fNear, float& fFar, float& fOrthoHeight);

	// accessors
	bool					IsCameraControl() const { return m_bIsCameraControl; }
	const rage::Matrix34*	GetPreUICameraMatrix(){return &m_PreUICameraMatrix;}
	rage::f32				GetPreUIFOV(){return m_PreUIFOV;}
	rage::f32				GetPreUINearPlane(){return m_PreUINear;}
	rage::f32				GetPreUIFarPlane(){return m_PreUIFar;}
	rage::f32				GetPreUIOrthoHeight(){return m_PreUIOrtho;}
	rage::Vector3*			GetPreUIVelocity(){return &m_PreUICameraVelocity;}
	void					SetPreUICameraMatrix(rage::Matrix34* m){m_PreUICameraMatrix.Set(*m);}
	void 					SetPreUIFOV(rage::f32 f){m_PreUIFOV = f;}
	void 					SetPreUINearPlane(rage::f32 f){m_PreUINear = f;}
	void 					SetPreUIFarPlane(rage::f32 f){m_PreUIFar = f;}
	void 					SetPreUIOrthoHeight(rage::f32 f){m_PreUIOrtho = f;}	
	const rage::Matrix34*	GetPostUICameraMatrix(){return &m_PostUICameraMatrix;}
	rage::f32 				GetPostUIFOV(){return m_PostUIFOV;}
	rage::f32 				GetPostUINearPlane(){return m_PostUINear;}
	rage::f32 				GetPostUIFarPlane(){return m_PostUIFar;}
	rage::f32 				GetPostUIOrthoHeight(){return m_PostUIOrtho;}
	rage::Vector3*			GetPostUIVelocity(){return &m_PostUICameraVelocity;}
	void 					SetPostUICameraMatrix(rage::Matrix34* m){m_PostUICameraMatrix.Set(*m);}
	void 					SetPostUIFOV(rage::f32 f){m_PostUIFOV = f;}
	void 					SetPostUINearPlane(rage::f32 f){m_PostUINear = f;}
	void 					SetPostUIFarPlane(rage::f32 f){m_PostUIFar = f;}
	void 					SetPostUIOrthoHeight(rage::f32 f){m_PostUIOrtho = f;}	
	const rage::Matrix34*	GetPauseUICameraMatrix(){return &m_PausedCameraMatrix;}
	rage::f32 				GetPauseUIFOV(){return m_PausedFOV;}
	rage::f32 				GetPauseUINearPlane(){return m_PausedNear;}
	rage::f32 				GetPauseUIFarPlane(){return m_PausedFar;}
	rage::f32 				GetPauseUIOrthoHeight(){return m_PausedOrtho;}
	rage::Vector3*			GetPauseUIVelocity(){return &m_PausedCameraVelocity;}
	void 					SetPauseUICameraMatrix(rage::Matrix34* m){m_PausedCameraMatrix.Set(*m);}
	void 					SetPauseUIFOV(rage::f32 f){m_PausedFOV = f;}
	void 					SetPauseUINearPlane(rage::f32 f){m_PausedNear = f;}
	void 					SetPauseUIFarPlane(rage::f32 f){m_PausedFar = f;}
	void 					SetPauseUIOrthoHeight(rage::f32 f){m_PausedOrtho = f;}	

protected:
	// UI Rendering Variables
	rage::Matrix34	m_PreUICameraMatrix;
	rage::Vector3	m_PreUICameraVelocity;
	rage::f32		m_PreUIFOV;
	rage::f32		m_PreUINear;
	rage::f32		m_PreUIFar;
	rage::f32		m_PreUIOrtho;
	rage::Matrix34	m_PostUICameraMatrix;
	rage::Vector3	m_PostUICameraVelocity;
	rage::f32		m_PostUIFOV;
	rage::f32		m_PostUINear;
	rage::f32		m_PostUIFar;
	rage::f32		m_PostUIOrtho;
	rage::Matrix34	m_PausedCameraMatrix;
	rage::Vector3	m_PausedCameraVelocity;
	rage::f32		m_PausedFOV;
	rage::f32		m_PausedNear;
	rage::f32		m_PausedFar;
	rage::f32		m_PausedOrtho;
	bool			m_bIsCameraControl;
	bool			m_bIsCameraPaused;
	bool			m_bIsRequestingCameraPause;
	rage::s32		m_FramesToPauseCamera;
};

class UIPlayer {
public:
	UIPlayer(){}
	~UIPlayer(){}

	void GetPlayerMatrix( s32 nPlayerIdx, Matrix34* pMatrix);
};

//
// This class maps inputs to the ones that the ui uses.  
// This can go away if we want, but since inputs are usually project specific, 
// then we will need to map them...
class UIInput : public rage::datBase
{
public:
	enum fnInputCmdID
	{
		fnInputCmdFlush = 0,

		fnInputCmdCount
	};

	enum fnInputEventID
	{
		fnInputEventUp = 0,
		fnInputEventDown,
		fnInputEventLeft,
		fnInputEventRight,
		fnInputEventAction,
		fnInputEventCancel,
		fnInputEventSelect,
		fnInputEventStart,
		fnInputEventX,
		fnInputEventY,
		fnInputEventL1,//10
		fnInputEventR1,
		fnInputEventL2,
		fnInputEventR2,
		fnInputEventL3,
		fnInputEventR3,
		fnInputEventLAnalogUp,
		fnInputEventLAnalogDown,
		fnInputEventLAnalogLeft,
		fnInputEventLAnalogRight,
		fnInputEventRAnalogUp,//20
		fnInputEventRAnalogDown,
		fnInputEventRAnalogLeft,
		fnInputEventRAnalogRight,

		fnInputEventUp_Released,
		fnInputEventDown_Released,
		fnInputEventLeft_Released,
		fnInputEventRight_Released,
		fnInputEventAction_Released,
		fnInputEventCancel_Released,
		fnInputEventSelect_Released,//30
		fnInputEventStart_Released,
		fnInputEventX_Released,
		fnInputEventY_Released,
		fnInputEventL1_Released,
		fnInputEventR1_Released,
		fnInputEventL2_Released,
		fnInputEventR2_Released,
		fnInputEventL3_Released,
		fnInputEventR3_Released,
		fnInputEventLAnalogUp_Released,
		fnInputEventLAnalogDown_Released,
		fnInputEventLAnalogLeft_Released,
		fnInputEventLAnalogRight_Released,
		fnInputEventRAnalogUp_Released,
		fnInputEventRAnalogDown_Released,
		fnInputEventRAnalogLeft_Released,
		fnInputEventRAnalogRight_Released,

		fnInputEventUp_repeat,
		fnInputEventDown_repeat,
		fnInputEventLeft_repeat,//50
		fnInputEventRight_repeat,
		fnInputEventAction_repeat,
		fnInputEventCancel_repeat,
		fnInputEventSelect_repeat,
		fnInputEventStart_repeat,
		fnInputEventX_repeat,
		fnInputEventY_repeat,
		fnInputEventL1_repeat,
		fnInputEventR1_repeat,
		fnInputEventL2_repeat,
		fnInputEventR2_repeat,
		fnInputEventL3_repeat,
		fnInputEventR3_repeat,
		fnInputEventLAnalogUp_repeat,
		fnInputEventLAnalogDown_repeat,
		fnInputEventLAnalogLeft_repeat,
		fnInputEventLAnalogRight_repeat,
		fnInputEventRAnalogUp_repeat,
		fnInputEventRAnalogDown_repeat,
		fnInputEventRAnalogLeft_repeat,
		fnInputEventRAnalogRight_repeat,

		fnInputEventRAnalog,
		fnInputEventLAnalog,

		fnInputEventCount
	};

	UIInput();

protected:
	UIObject*		m_pUserInterface;
	bool			m_IsPressed;
	ioMapper		m_Mapper;
	bool			m_bConnected;
	bool			m_bIsDisabled;
	float			m_AnalogDeadZone;
	int				m_ActiveResource;//0 if there is no focused component that responds to input
	PPU_ONLY(bool	m_swapCircleButton;)


public:

	void			SetPadIndex(u32 index)							;
	void			UpdateMapper()									;
	void			Update		()									;
	void			Flush		()									;			
	bool			IsPressed	()							const	{ return m_IsPressed;			}
	int				GetKeyId	(fnInputEventID eventId)	const	;
	static bool		IsDown		(fnInputEventID eventId)			;
	void			SetUserInterface(UIObject* pObject){m_pUserInterface = pObject;}
	void			ComponentFocused(){m_ActiveResource++;}
	void			ComponentUnfocused(){m_ActiveResource--;}
	PPU_ONLY(bool	GetIsSwapCircleButton()					const	{ return m_swapCircleButton; })

	static int fnInputEventMap[];
};

//==============================================================================================================================
struct UITransitionPath
	//==============================================================================================================================
{
	rage::atQueue<UIObject*, 8> m_Exit;
	rage::atQueue<UIObject*, 8> m_Enter;

	void		Reset()			{ m_Exit.Reset(); m_Enter.Reset();	}
	UIObject*	GetTop() const	{ return  m_Exit.Top();				}

	UITransitionPath()
	{
		Reset();
	}
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	UINavigator
//	These class merely manages navigation/transitions between various UI components.  It includes popup management. 
//	It also includes state stack management.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UINavigator : public UIObject
{

public:
	//PURPOSE
	//Construction/Destruction
	UINavigator();
	virtual ~UINavigator();

	//PURPOSE:
	// Request a transition from one UIObject to another.  
	//PARAMS:
	//	UIObject - the target object
	//	state - what type of transition you want to perform.  could be	UI_ENABLED,	UI_INTERRUPTIBLE,UI_ACTIVE,UI_FOCUSED,UI_VISIBLE, or UI_ALL 
	//NOTES:
	//	Stage 1: Request a Transition
	//		Transition is requested.  A transition will be denied if we are already transitioning
	//	Stage 2: UnFocus
	//		SetFocused(false) is called for the UIScene,  (note that it is up to each custom scene to UnFocus its children accordingly)
	//	Stage 3: Out Transition
	//		If the current component has an out/exit transition then that transition will be started(entered,focused,activated,shown,ect).  
	//	Stage 4: Activation
	//		Deactivate (OnDeactivate) is called for the UIScene,  (note that it is up to each custom scene to Deactivate its children accordingly)
	//		Activate (OnActivate) is called for the UIScene,  (note that it is up to each custom scene to activate its children accordingly)
	//	Stage 5: In Transition
	//		If the current component has an in/enter transition then that transition will be started(entered,focused,activated,shown).  
	//	Stage 6: Focus
	//		SetFocused(true) is called for the UIScene,  (note that it is up to each custom scene to activate its children accordingly)
	bool			RequestTransition(UIObject* UIObject , UIObject::UIObjectState state = UIObject::UI_ALL);
	bool 			IsTransitioning();
	void 			PushTransition(UITransition* pTransition);
	void			TerminateAllTransitions();
	void			TerminateTransition(UITransition*);
	void			GetTransition(const char* pName, rage::Matrix34* pStartPosition, rage::Matrix34* pFinalPosition, rage::Matrix34* pTarget);
	void			SetUserInterface(UIObject* pObject){m_pUserInterface = pObject;}
	UIObject&		GetTopPopup(){ return *m_pPopupQueue.Top();}
	const UIObject& GetTopPopup()const{ return *m_pPopupQueue.Top();}
	void 			PushPopup(UIObject*,bool force=false);
	void 			PopPopup(UIObject*);
	bool			IsShowingDialog()const;
	void			PostLoad();
	void			PopTransition(UITransition* pTransition);
	UIObject*		GetTargetUIObject(){return m_pTargetState;}
	UIObject*		StackPush( UIObject* v );
	UIObject*		StackPop(/*int howMany=1*/);
	void			GotoUIObject( UIObject* v );
	void			ShowUIObject( UIObject* v );
	void			HideUIObject( UIObject* v );
	void			RefreshUIObject( UIObject* v );
	void			ActivateUIObject( UIObject* v );
	void			DeactivateUIObject( UIObject* v );
	void			FocusUIObject( UIObject* v );
	void			UnfocusUIObject( UIObject* v );
	void			EnterUIObject( UIObject* v );
	void			ExitUIObject( UIObject* v );
	void			Update(rage::f32 dt);
	void			SendEventTo(UIObject* pUIObject,fuiEvent* pEvent, bool isInput);
	void			RegisterAllUIScriptCommands();
	void			ClearStack();
	void			RegisterEventListener(UIObject* pObject);

#if __BANK
	void AddWidgets(bkBank& b);
	void OutputStateStack();
#endif

protected:
	bool FocusInTransitionPath	();//process all UIObjects that we are "focusing"
	bool UnfocusOutTransitionPath();//process all UIObjects that we are "unfocusing"
	bool ActivateInTransitionPath();//process all UIObjects that we are "activating"
	bool DeactivateOutTransitionPath();//process all UIObjects that we are "deactivating"	
	bool ShowInTransitionPath();//process all UIObjects that we are "activating"
	bool HideOutTransitionPath();//process all UIObjects that we are "deactivating"	
	void TransitionComplete();
	void BroadcastEvent(fuiEvent* pEvent, bool isFromQueue=false);
	void UpdateTransitions(rage::f32 dt);
	bool OnTransitionStart(UIObject* pTop);
	void OnTransitionEnd();
	void TransitionOutStart();
	void TransitionInStart();
	void TransitionOut();
	void TransitionIn();
	void TransitionToUIObject(const char *UIObjectName);
	void UpdateInput();
	void UpdateOther();
	void UpdateMapper();	
	void OnTransitionComplete();

	enum TransitionStates
	{
		eTransitionNone = 0,
		eTransitionOutStart,
		eTransitionOut,
		eTransitionInStart,
		eTransitionIn,
		eTransitionComplete,
	};

	int							m_TransitionType;
	TransitionStates			m_TransitionState;
	UIObject* 					m_pCurrentState;
	UIObject* 					m_pTargetState;
	UIObject*					m_pUserInterface;
	rage::atQueue<UIObject*,3>	m_pPopupQueue;
	bool						m_bIsTransitioning;
	bool						m_bIsHandlingEvent;
	rage::atArray<UITransition*>mCurrentTransitions;
	UITransitionPath			m_CurrentFocusTransitionPath;	//holds a transitions path from start state to dest state
	UITransitionPath			m_CurrentActivateTransitionPath;//holds a transitions path from start state to dest state
	UITransitionPath			m_CurrentVisibleTransitionPath;
	UITransitionPath			m_TempTransitionPath;
	UITransitionPath			m_CurrentTransitionPath;
	rage::atQueue<UIObject*,5>	m_TransitionQueue;
	rage::atQueue<int,5>		m_TransitionTypeQueue;
	atFixedArray<UIObject*,16>  m_UIExternalEventListeners;


	enum { stackSize = 32 };
	UIObject* m_UIObjectStack[stackSize];
	int m_UIObjectStackHeight;	

};

//
// UIManager
//
// PLEASE DO NOT PUT ANY CUSTOM TYPE LOGIC IN HERE.  THAT MEANS NO FLASH AND NO PROJECT SPECIFIC OBJECTS!!!!
//
// This class uses a "Facade" design for various subsystem that manages a UI system
class UIManager : public UIObject
{
public:
	friend class atSingleton<UIManager>;
	friend class	UIVariant;
	friend class	UICommand;
	friend class	UIEvent;
	friend class	UIEventHandler;
	friend class	UINavigator;
	friend class	UIObject;	


	UIManager();
	virtual ~UIManager();

	// Life cycle functions
	void Load();
	void UnLoad();
	void DrawInViewport(const grcViewport* pGameVP,f32,u32);
	bool IsUILoaded(){return m_bIsUILoaded;}
	void PostLoad();
	void RegisterScriptCommands();
	void Goto(const char* id);
	void Update(float dt);

	//accessors
	UIObject* GetUserInterface() const { return m_pUserInterface; }
	void SetUserInterface();
	//virtual bool IsInterruptible(){return m_bIsInterruptible;}
	//virtual void SetInterruptible(bool b){m_bIsInterruptible=b;}
	void ProcessInterruptingQueue();
	void ThrowNextQueuedEvent();
	UIObject* GetUIObject(const char *id);
	UINavigator* GetUINavigator(){return m_pUINavigator;}
	UICamera* GetUICamera(){return m_pUICamera;}
	void SetUserInterface(UIObject* ui);
	UIFactory* GetUIFactory(){return m_pUIFactory;}
	UITextureStream* GetTextureStreamer () const { return m_pTextureStreamer; }
	UIEventManager* GetEventManager(){return m_pEventManager;}
	void SetPadIndex(u32 index);
	void SetTrackingBucket(int trackingBucket)	{ m_TrackingBucket = trackingBucket; }
	void SetGame(UIGame* pGame){m_pUIGame = pGame;}

	//events and input
	virtual bool HandleEvents(fuiEvent*);//override of fuiEventListener::HandleEvents() so we can receive fuiEvents;
	void QueueEvent(fuiEvent* pEvent);
	void SendInput(fuiEvent*);
	void SendInput(const char* pEvent);
	bool SendEvent(fuiEvent*, bool isFromQueue = false);
	void SendEventCB(const char* pEvent){SendEvent(pEvent,NULL);}
	bool SendEvent(const char* pEvent , void* pData = NULL, rage::u8 priority = FUIEVENTLISTENER_DEFAULT_PRIORITY, bool sendToCode = true,  bool sendToScripts = true, bool isFromQueue = false);
	void SendEventTo(const char* UIObject, const char *event , void* pData = NULL, rage::u8 priority = FUIEVENTLISTENER_DEFAULT_PRIORITY, bool sendToCode = true,  bool sendToScripts = true, bool isFromQueue = false);
	void SendEventTo(UIObject* pUIObject, const char *event , void* pData = NULL , u8 priority = FUIEVENTLISTENER_DEFAULT_PRIORITY, bool sendToCode = true,  bool sendToScripts = true, bool isFromQueue = false);
	void SendEventTo(UIObject* UIObject, fuiEvent* pEvent , bool isFromQueue = false, bool isInput=false);
	void SetDisabled(bool b){ m_Disabled = b;}
	bool IsDisabled(){ return m_Disabled;}	

	//misc
	void LockMutex(){ sysIpcLockMutex(m_FuiWindowMutex); }
	void UnlockMutex(){ sysIpcUnlockMutex(m_FuiWindowMutex); }
	void Pause();
	void Resume();
	void AddDelayedEvent	(const char *sEvent);
	void AssertAndDisableFUI(const char* error)	;
	static void Reboot( const char *pType);

	//facade functions. convenience calls to subsystem functions
	static bool	RequestTransition(const char* c , UIObjectState state = UI_ALL);
	static bool RequestTransition(UIObject* o , UIObjectState state = UI_ALL);
	bool IsTransitioning( void );
	bool IsShowingDialog() const { return m_pUINavigator->IsShowingDialog(); }
	void PushEventToInterruptingQueue(fuiEvent* e);
	void StackPush(UIObject* o){m_pUINavigator->StackPush(o);}
	void StackPop(){m_pUINavigator->StackPop();}
	void TerminateAllTransitions(){m_pUINavigator->TerminateAllTransitions();}
	void PushTransition(const char* pTransitionName);
	void RegisterEventListener(UIObject* pObject){m_pUINavigator->RegisterEventListener(pObject);}
	void ComponentFocused(){m_pUIInput->ComponentFocused();}
	void ComponentUnfocused(){m_pUIInput->ComponentUnfocused();}

	atDelegate<void ()> m_UIFactory;

private:

#if __BANK
	void CaptureScreenShotJpeg();
	void GotoInvisibleUIObject();
	void AddWidgets(bkBank& b);
#endif

	UIObject* m_pUserInterface;//root UIObject that holds all UI components // was UIObject
	UINavigator* m_pUINavigator;
	UIInput* m_pUIInput;
	UIFactory* m_pUIFactory;
	UICamera* m_pUICamera;
	UIPlayer* m_pUIPlayer;
	UIGame* m_pUIGame;
	UIEventManager* m_pEventManager;	
	UITextureStream* m_pTextureStreamer;
	int m_TrackingBucket;			// Tracking bucket for resources
	atFixedArray<UIObject*,16> m_UILayerStack;//used for navigating via push/pop	

	//event system
	bool m_bIsHandlingEvent;
	bool m_ProcessQueuedEvents;
	rage::atQueue<fuiEvent*, 32>m_EventQueue;
	rage::atQueue<fuiEvent*, 32>m_InterruptingQueue;//secondary queue that we store events that we don't want to process immediately
	fuiDelayedEventArray		m_aDelayedEvents;

	bool m_bShowUI;
	bool m_bIsUILoaded;
	bool m_Disabled;
	
	sysIpcMutex	m_FuiWindowMutex;

	static UIObject	m_UIObject;
	UIObject* GetEventAndCommandObject(){return &m_UIObject;}

	//loading
	bool LoadFromXml(const char *szFileName);
};

//singletons
typedef atSingleton<UIManager> UIManagerSingleton;
#define UIMANAGER UIManagerSingleton::InstancePtr()
typedef atSingleton<UIFactory> UIObjectFactorySingleton;
#define UIFACTORY UIMANAGER->GetUIFactory()

//this class is only created cause the database system freaks out with multiple inheritance
class UIEventManager : public fuiEventListener
{
	UIEventManager() : fuiEventListener("UIManager"){}

	virtual bool HandleEvents(fuiEvent* e){return UIMANAGER->HandleEvents(e);}//override of fuiEventListener::HandleEvents() so we can receive fuiEvents;
};

class UIGame : public UIObject
{
public:
	friend class atSingleton<UIManager>;
	friend class	UIVariant;
	friend class	UICommand;
	friend class	UIEvent;
	friend class	UIEventHandler;
	friend class	UINavigator;
	friend class	UIObject;

	UIGame(){}
	virtual ~UIGame(){}

	//project specific funcs
	virtual void Pause()=0;
	virtual void Resume()=0;
	virtual void SetGameLoaded(bool )=0;
	virtual bool IsGameLoaded()=0;

protected:
};

} // namespace rage
#endif
