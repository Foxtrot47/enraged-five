// 
// flashuimisc/EventManager.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// Event includes
#include "fuicore/EventManager.h"

// Script includes
#include "script/wrapper.h"

// Command line param include
#include "system/param.h"
#include "data/marker.h"

#include "fuicore/UIManager.h"

namespace rage {
// Command line parameters
PARAM(eventdebuginfo, "Event Debug Info");

// fuiEventManager implementation //

// fuiEventManager ctor
fuiEventManager::fuiEventManager( void )
{
	RAGE_FUNC();

	// Create the mission title hash table
	m_pEventHashTable = rage_new SimpleHash< fuiEventHashElement >;
	Assert( m_pEventHashTable );
	m_pEventHashTable->Init( FUIEVENTMGR_EVENT_ELEMENT_POOL_COUNT );

	m_pEventDataPool = rage_new fuiEventDataPool( FUIEVENTMGR_EVENT_ELEMENT_POOL_COUNT );
	Assert( m_pEventDataPool );

	m_bEventDebugInfo = PARAM_eventdebuginfo.Get();
}

// fuiEventManager dtor
fuiEventManager::~fuiEventManager( void )
{
	// Delete the previously allocated event Hash Table
	if( m_pEventHashTable )
	{
		delete m_pEventHashTable;
	}

	if( m_pEventDataPool )
	{
		delete m_pEventDataPool;
	}
}

// Method:	RegisterEvent( fuiEventListner* pListener, const char* pStrEventIdentifier, s32 nPriority )
// Purpose: Method that will register an event with a listener. This function is wrapped around the macro REGISTER_EVENT
fuiEvent* fuiEventManager::RegisterEvent( fuiEventListener* pListener, const char* pStrEventIdentifier, s32 nPriority )
{
	// Make sure we have a designated event identifier
	if( pStrEventIdentifier )
	{
		// Determine if this event has already been registered
		fuiEventHashElement searchElement( pStrEventIdentifier );
		fuiEventHashElement* pResultElement = m_pEventHashTable->Search( &searchElement );
		if( pResultElement )
		{
			if( IsEventInfoEnabled() ) 
				Displayf( "fuiEventManager::RegisterEvent - PREEXISTING EVENT %s", pStrEventIdentifier );

			// If so, add the newly registered listener to the event element
			pResultElement->AddEventData( pListener, nPriority );
			return pResultElement->GetEvent();
		}
		// Otherwise we need to add a new one
		else
		{
			// Insert the newly registered event
			fuiEventHashElement* pNewlyInsertedElement = m_pEventHashTable->Insert( searchElement );

			if( IsEventInfoEnabled() ) 
				Displayf( "fuiEventManager::RegisterEvent - NEW EVENT %s", pStrEventIdentifier );

			// Add a the designated listener to the event element
			pNewlyInsertedElement->AddEventData( pListener, nPriority );
			pNewlyInsertedElement->GetEvent()->SetName(pStrEventIdentifier);
			return pNewlyInsertedElement->GetEvent();
		}
	}

	// Default case.... always return NULL
	return NULL;
}

// Method:	UnregisterEvent( fuiEventListner* pListener, fuiEvent* pEvent )
// Purpose: Method that will unregister an event given a listener. This function is wrapped around the macro UNREGISTER_EVENT
void fuiEventManager::UnregisterEvent( fuiEventListener* pListener, fuiEvent* pEvent )
{
	if( pEvent )
	{
		fuiEventHashElement searchElement( pEvent );
		fuiEventHashElement* pResultElement = m_pEventHashTable->Search( &searchElement );
		if( pResultElement )
		{
			if( IsEventInfoEnabled() ) 
				Displayf( "fuiEventManager::UnregisterEvent - EVENT REMOVED %s", pEvent->GetName() );

			// Determine if we have removed all listeners for this event
			pResultElement->RemoveEventData( pListener );
			/*{
				if( IsEventInfoEnabled() ) 
					Displayf( "fuiEventManager::UnregisterEvent - EVENT DELETED %s", pEvent->GetName() );

				m_pEventHashTable->Delete( pResultElement );
			}*/
		}
	}
}

// Method:	SendEvent( const char* pStrEventIdentifier, void* pData )
// Purpose: Method that will propagate an event defined by a string. Wrapped around SEND_EVENT macro
void fuiEventManager::SendEvent( const char* pStrEventIdentifier , void* pData)
{
	if( IsEventInfoEnabled() ) 
		Displayf( "fuiEventManager::SendEvent - EVENT SENT %s", pStrEventIdentifier);	

	// Determine if anybody has registered for this event 
	fuiEventHashElement searchElement( pStrEventIdentifier );
	fuiEventHashElement* pResultElement = m_pEventHashTable->Search( &searchElement );
	if( pResultElement )
	{
		// Okay, lets post the event to all listeners and send off the data
		if (pData) pResultElement->GetEvent()->SetData(pData);
		pResultElement->PostEvent();
	}
}

// Method:	SendEvent( fuiEvent* pEventToSend, void* pData )
// Purpose: Method that will propagate an event defined by an event. Wrapped around SEND_EVENT macro
void fuiEventManager::SendEvent( fuiEvent* pEventToSend)
{
	if( pEventToSend && IsEventInfoEnabled() ) 
		Displayf( "fuiEventManager::SendEvent - EVENT SENT %s", pEventToSend->GetName() );

	// Determine if anybody has registered for this event 
	fuiEventHashElement searchElement( pEventToSend );
	fuiEventHashElement* pResultElement = m_pEventHashTable->Search( &searchElement );
	if( pResultElement )
	{
		// Okay, lets post the event to all listeners and send off the data
		pResultElement->PostEvent();	
	}
}

// Method:	GetEventHashElement( const char* pStrEventIdentifier )
// Purpose: Accessor method that will return a hash element (if available) with the designated event string identifier
const fuiEventHashElement* fuiEventManager::GetEventHashElement( const char* pStrEventIdentifier ) const
{
	// Return the event element, if anybody has registered for it 
	fuiEventHashElement searchElement( pStrEventIdentifier );
	return m_pEventHashTable->Search( &searchElement );
}

// Method:	GetEvent( const char* pStrEventIdentifier )
// Purpose: Accessor method that will return a event (if available) with the designated event string identifier
fuiEvent* fuiEventManager::GetEvent( const char* pStrEventIdentifier, bool createIfDoesntExist)
{
	// Return the event element, if anybody has registered for it 
	fuiEventHashElement searchElement( pStrEventIdentifier );
	fuiEventHashElement* p =  m_pEventHashTable->Search( &searchElement );
	if (!p)
	{
		if (createIfDoesntExist)
		{
			return RegisterEvent(UIMANAGER->GetEventManager(),pStrEventIdentifier);
		}
		else
		{
			return NULL;
		}
	}
	return p->GetEvent();
}

// Method:	RegisterScriptCommands( void )
// Purpose: Binds event related methods with the scripting system
void fuiEventManager::RegisterScriptCommands( void )
{
	// Post Event native functions
	SCR_REGISTER_SECURE(mcEvent_PostEventStr, 0xf0fb95ea, fuiEventManager::PostEventStr );
	SCR_REGISTER_SECURE(mcEvent_PostEventInt, 0xadefae9e, fuiEventManager::PostEventInt );
	SCR_REGISTER_SECURE(mcEvent_PostEventFloat, 0x9f6356ba, fuiEventManager::PostEventFloat );
//	SCR_REGISTER( mcEvent_PostEventRaceResults, fuiEventManager::PostEventRaceResults );
//	SCR_REGISTER( mcEvent_PostEventRaceAbort, fuiEventManager::PostEventRaceAbort );
	
}

// Method:	PostEventStr( const char* pStrEventIndentifier, const char* pStrData )
// Purpose: Native sanScript wrapper function to post a string data event
void fuiEventManager::PostEventStr( const char* pStrEventIndentifier, const char* pStrData )
{
	FUIEVENTMGR->SendEvent( pStrEventIndentifier, (void*)pStrData );
}

// Method:	PostEventInt( const char* pStrEventIndentifier, s32 nIntData )
// Purpose: Native sanScript wrapper function to post an integer data event
void fuiEventManager::PostEventInt( const char* pStrEventIndentifier, s32 nIntData )
{
	FUIEVENTMGR->SendEvent( pStrEventIndentifier, (void*)&nIntData );
}

// Method:	PostEventFloat( const char* pStrEventIndentifier, f32 fFloatData )
// Purpose: Native sanScript wrapper function to post a float data event
void fuiEventManager::PostEventFloat( const char* pStrEventIndentifier, f32 fFloatData )
{
	FUIEVENTMGR->SendEvent( pStrEventIndentifier, (void*)&fFloatData );
}
} // namespace rage

