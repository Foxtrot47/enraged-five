// 
// mcUI/TextureStream.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#ifndef __MCUI_TEXTURESTREAM_H__
#define __MCUI_TEXTURESTREAM_H__

// Key to the design is that the 'old' texture hangs around until the new one is streamed in and placed.
// Obviously does not stream out too early etc

#include "atl\array.h"
#include "atl\map.h"
#include "paging\streamable.h"
#include "paging\dictionary.h"
#include "grcore/texture.h"

namespace rage {
	class grcTexture;
	class grcTextureReference;
};

class UITextureStream
{
public:
	UITextureStream();
	~UITextureStream();

	void Init(const int cnListIndex /*0 for base content, 1+ for dlc*/, int trackingBucket = 0);					// Setup all the streamable objects

	rage::grcTextureReference * CreateTextureReference( const int cnSlot, const char * pReferenceName, const bool bHaveTransparentFallback = true );		// register a texture reference so the game can do a texture lookup
	void StartStreaming( const int cnSlot, const char * pTextureName );					// start streaming the named texture into the given slot index
	void StopStreaming( const int cnSlot );									// unload the contents of the slot (and anything that was requested to load there) - unless another slot is using it

	void DeleteTextureReference( const int cnSlot );							// deregister the texture reference (and calls StopStreaming)

	void Update();

private:

	static const int kMaxNumSlots = 44;
	static const int kMaxDeletingItems = 128;	// is this enough, too many?  Hammering the streaming may cause us to overflow this

	typedef rage::pgStreamable< rage::pgDictionary<rage::grcTexture> >			tStreamableTextureDictionary;

	class mcStreamableTextureDictionaryMapEntry {
	public:
		mcStreamableTextureDictionaryMapEntry(const int cnContentPackIndex) : m_StreamableDictionary(), m_nContentPackIndex(cnContentPackIndex) {}
		tStreamableTextureDictionary	m_StreamableDictionary;
		int								m_nContentPackIndex;
	};


	// map of all the streamable objects we could bring in
	// TODO: Consider using atStringMap
	rage::atMap< rage::atString, mcStreamableTextureDictionaryMapEntry *, rage::atMapCaseInsensitiveHashFn,rage::atMapCaseInsensitiveEquals >				m_StreamableTextures;

	// current set of texture references (texture names the game is using)
	rage::atRangeArray< rage::grcTextureReference*, kMaxNumSlots>				m_TextureReferences;
	rage::atRangeArray< bool, kMaxNumSlots >									m_TextureReferencesUseFallback;


	// streamables either being requested for this slot or resident for this slot
	rage::atRangeArray< tStreamableTextureDictionary *, kMaxNumSlots>			m_SlotResident;					// array of the streamables that are resident (usable) in each of the slots
	rage::atRangeArray< tStreamableTextureDictionary *, kMaxNumSlots>			m_SlotLoading;					// array of the streamables that are loading (not yet usable) in each of the slots

	rage::grcTexture * m_pBlankTexture;											// blank texture used when nothing is referenced


	// array of the items we are waiting to delete
	struct sDeleteSlot {
		sDeleteSlot() : StreamingIn(false), Timer(5), Data(NULL) { }
		bool StreamingIn;		// set if the data being deleted is being streamed in (in process of streaming in but orphaned)
		int  Timer;				// countdown to deletion (dont delete if StreamingIn is set)
		tStreamableTextureDictionary * Data;
	};
	rage::atArray< struct sDeleteSlot >		m_DeleteSlots;

private:
	void AddToDeleteList( tStreamableTextureDictionary * pStreamable, const bool cbSlotStillStreamingIn/*used if the texture has been sceduled to load and then we want to delete before load has finished*/);

	static void Place(void *streamable, rage::datResourceMap& , void *userData);
	void Place( tStreamableTextureDictionary * pStreamable );
	void MakeStreamableResident(const int i, tStreamableTextureDictionary * pResidentStreamable);

	void StopStreaming_Loading( const int cnLoadingSlot );
	void StopStreaming_Resident( const int cnResidentSlot );
};

#endif // __MCUI_TEXTURESTREAM_H__



