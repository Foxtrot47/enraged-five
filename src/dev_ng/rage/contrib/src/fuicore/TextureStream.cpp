// 
// mcUI/TextureStream.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "fuicore\TextureStream.h"
#include "fuicore\Common.h"
#include "grcore\texturereference.h"
#include "atl\string.h"
#include "file\asset.h"
#include "file\token.h"
#include "grcore/im.h"

using namespace rage;


UITextureStream::UITextureStream() : 	m_TextureReferences(NULL),
											m_SlotResident(NULL),
											m_SlotLoading(NULL),
											m_pBlankTexture(NULL)

{
}

UITextureStream::~UITextureStream()
{
	if (m_pBlankTexture)
		m_pBlankTexture->Release();
}

void UITextureStream::Init(const int cnListIndex /*0 for base content (and any dlc already monted), 1+ for dlc*/, int trackingBucket)
{
	int numStreamables = 0;

	//const int start = cnListIndex;
	//const int end = (start==0) ? 8 : cnListIndex;

	//for(int i=start;i<=end;i++)
	{
		char filename[128];
		//if (cnListIndex==0)
			safecpy(filename, "$/mapRes/UI_bink", 128);
		//else
		//	formatf(filename, 127, "$/mapRes/UI_Bink_%04d", i);

		rage::fiStream * file = rage::ASSET.Open( filename, "xlist" );

		Assertf(file, "Your xlist file is missing, path = (%s). Bink is now hosed.", filename);
		if( file )
		{
			rage::fiTokenizer token;
			token.Init( "StreamableTextures", file );

			if( token.CheckIToken( "Resources" ) )
			{
				token.MatchToken("{");
				while( !token.CheckToken("}") )
				{
					char resfilename[96];
					token.GetToken(resfilename, sizeof(resfilename));
					char resfullfilename[96];
					formatf(resfullfilename, sizeof(resfullfilename), "$/mapRes/%s", resfilename);
					char * pfilenameStart = strrchr( resfilename, '\\');
					pfilenameStart = (pfilenameStart!=NULL) ? pfilenameStart+1 : resfilename;

					if (m_StreamableTextures.Access(pfilenameStart)==NULL)	// check we didn't add this texture already
					{
						mcStreamableTextureDictionaryMapEntry * pStreamable = rage_new mcStreamableTextureDictionaryMapEntry(cnListIndex);
						if ( pStreamable->m_StreamableDictionary.Init( resfullfilename, "#td", grcTexture::RORC_VERSION, trackingBucket ) )
						{
							m_StreamableTextures.Insert( atString(pfilenameStart), pStreamable );
							numStreamables++;
						}
						else
						{
							// Error initializing the streamable
						}
					}

				}
			}
			file->Close();
		}
	}

	if (cnListIndex == 0)
		m_DeleteSlots.Reserve( numStreamables );

	// Load a blank texture for use when there is nothing to display (black fully transparent)
	if (!m_pBlankTexture)
		m_pBlankTexture = grcCreateTexture("$/textures/blank");
}


rage::grcTextureReference * UITextureStream::CreateTextureReference( const int cnSlot, const char * pReferenceName, const bool bHaveTransparentFallback )
{
	Assert(m_TextureReferences[cnSlot]==NULL);

	m_TextureReferences[cnSlot] = rage_new grcTextureReference(pReferenceName,NULL);
	grcTextureFactory::GetInstance().RegisterTextureReference(pReferenceName, m_TextureReferences[cnSlot]);
	
	if (bHaveTransparentFallback)
		m_TextureReferences[cnSlot]->SetReference( m_pBlankTexture );
	m_TextureReferencesUseFallback[cnSlot] = bHaveTransparentFallback;

	return m_TextureReferences[cnSlot];
}


void UITextureStream::DeleteTextureReference( const int cnSlot )
{
	StopStreaming(cnSlot);

	if (m_TextureReferences[cnSlot]) 
	{
		grcTextureFactory::GetInstance().DeleteTextureReference( m_TextureReferences[cnSlot]->GetName() );
		m_TextureReferences[cnSlot]->Release();
	}
	m_TextureReferences[cnSlot] = NULL;
}


void UITextureStream::StartStreaming( const int cnSlot, const char * pTextureName )
{
	mcStreamableTextureDictionaryMapEntry ** ppStreamable = m_StreamableTextures.Access( pTextureName );
	if (ppStreamable)
	{
		mcStreamableTextureDictionaryMapEntry * pStreamableEntry = *ppStreamable;
		Assert(pStreamableEntry);
		if (pStreamableEntry->m_nContentPackIndex < 0)
			return;
		tStreamableTextureDictionary * pStreamable = &pStreamableEntry->m_StreamableDictionary;
		
		UIDebugf1("UITextureStream::StartStreaming slot %d streamable %p\n", cnSlot, pStreamable);

		if (m_SlotLoading[cnSlot] == pStreamable)
			// already loading into this slot!
			return;

		if (m_SlotResident[cnSlot] == pStreamable)
			// already resident in this slot!
			return;

		UIDebugf1("UITextureStream::StartStreaming did not early out\n");

		if (m_SlotLoading[cnSlot])
		{
			// stop the thing that was streaming in from streaming in any longer (no longer required by this slot - may be required by another)
			StopStreaming_Loading(cnSlot);
		}


		// now see if the slot we are trying to load is being unloaded (if so remove it from the unload list and 'save' the item)
		bool bWasBeingDeleted = false;
		bool bSavedItemWasLoading = false;
		for(int i=0;i<m_DeleteSlots.GetCount();)
		{
			if (m_DeleteSlots[i].Data == pStreamable)
			{
				bWasBeingDeleted = true;
				bSavedItemWasLoading = m_DeleteSlots[i].StreamingIn;
				m_DeleteSlots.Delete(i);
				break;
			}
			else
				i++;
		}
		if (bWasBeingDeleted)
		{
			if (bSavedItemWasLoading)
			{
				// item in delete queue was waiting to be deleted after it's load finished
				UIDebugf1("Moving item from loading but waiting to be deleted (orphaned during load) to loading %d %p\n", cnSlot, pStreamable);
				Assert(m_SlotLoading[cnSlot]==NULL);			// this hits if something is loading into the slot that we are now assigning the streamable that was in the delete list - hopefully all this causes is that the streamables are out of order and the one that was loading will load after the more recent request (that was found in the delete list)
				m_SlotLoading[cnSlot] = pStreamable;
			}
			else
			{
				// item in delete queue was waiting to be deleted and was resident
				UIDebugf1("Saved from deletion %d %p\n", cnSlot, pStreamable);
				MakeStreamableResident(cnSlot, pStreamable);
			}
			return;
		}

		// now see if the streamable is already loaded into a slot
		for(int i=0;i<m_SlotResident.GetMaxCount();i++)
			if (m_SlotResident[i]==pStreamable)
			{
				MakeStreamableResident(cnSlot, pStreamable);
				return;		// early exit
			}

		// now see if the streamable is already loading
		for(int i=0;i<m_SlotLoading.GetMaxCount();i++)
			if (m_SlotLoading[i]==pStreamable)
			{
				m_SlotLoading[cnSlot] = pStreamable;
				return;		// early exit
			}


		// not in any of the lists (deleting or resident)
		// actually schedule a load
		if (pStreamable->GetState() == pgStreamableBase::NONRESIDENT)
		{
			pStreamable->Schedule(0.5f, Place, this );
			m_SlotLoading[cnSlot] = pStreamable;
		}
	}
}


void UITextureStream::AddToDeleteList( tStreamableTextureDictionary * pSlotContents, const bool cbSlotStillStreamingIn )
{
	UIDebugf1("AddToDeleteList %p %d\n", pSlotContents, m_DeleteSlots.GetCount());
	Assert(pSlotContents);
	
	for(int i=0;i<m_DeleteSlots.GetCount();i++)
	{
		Assert(m_DeleteSlots[i].Data!=pSlotContents);		// should not have been trying to delete this in the first place
	}

	struct sDeleteSlot d;
	d.Data = pSlotContents;
	d.StreamingIn = cbSlotStillStreamingIn;
	m_DeleteSlots.Append() = d;
}



void UITextureStream::Update()
{
	// scan the delete list

	for(int i=0; i<m_SlotLoading.GetMaxCount(); i++)
	{
		if(m_SlotLoading[i] && m_SlotLoading[i]->GetState() == rage::pgStreamableBase::RESIDENT)
		{
			Place((tStreamableTextureDictionary *) m_SlotLoading[i]);
		}
	}

	for(int i=0;i<m_DeleteSlots.GetCount();)
	{
		if (m_DeleteSlots[i].StreamingIn==false && --m_DeleteSlots[i].Timer <= 0)
		{
#if __DEV
			// check there are not two slots that are trying to delete the same thing
			for(int j=0;j<m_DeleteSlots.GetCount();j++)
			{
				Assert(i==j || m_DeleteSlots[i].Data!=m_DeleteSlots[j].Data);
			}
			// check the slot is not trying to load at the same time
			for(int j=0;j<m_SlotLoading.GetMaxCount();j++)
			{
				Assert(m_DeleteSlots[i].Data!=m_SlotLoading[j]);
			}
			// make sure nothing still referencing this data
			if (m_DeleteSlots[i].Data->IsResident())
			{
				for(int j=0;j<m_TextureReferences.GetMaxCount();j++)
					if (m_TextureReferences[j])
					{
						if (m_TextureReferences[j]->GetReference() == m_DeleteSlots[i].Data->GetObject()->GetEntry(0))
							Printf("Delete slot %d is being referenced by texture %d\n", i, j);
						Assert(m_TextureReferences[j]->GetReference() != m_DeleteSlots[i].Data->GetObject()->GetEntry(0));
					}
			}
#endif
			// Do the actual delete
			m_DeleteSlots[i].Data->Delete();
			m_DeleteSlots.Delete(i);
			// don't increment i (we just deleted slot i - and moved i+1 into the i slot)
		}
		else
		{
			i++;
		}
	}


}



void UITextureStream::Place(void *streamable,datResourceMap& rMap, void *)
{
	Assert(streamable);
	if (!streamable)
		return;

	// Place the data (do resource construction)
	tStreamableTextureDictionary * pStreamableDictionary = (tStreamableTextureDictionary *) streamable;
	pStreamableDictionary->Place(rMap);
}


void UITextureStream::Place(tStreamableTextureDictionary * pStreamable)
{
	bool bFoundTargetSlot = false;

	for(int i=0;i<m_SlotLoading.GetMaxCount();i++)
	{
		if (m_SlotLoading[i] == pStreamable)
		{
			UIDebugf1("UITextureStream::Place into slot %d - %p\n", i, pStreamable);
			m_SlotLoading[i] = NULL;		// loaded
			MakeStreamableResident(i, pStreamable);

			bFoundTargetSlot = true;
		}
	}

	// scan the delete list (can contain items that are still loading)
	for(int i=0;i<m_DeleteSlots.GetCount();i++)
	{
		if (m_DeleteSlots[i].Data == pStreamable)
		{
			Assert(m_DeleteSlots[i].StreamingIn);	// if we hit this then we have something in delete list that is coming in, but we thought was not resident
			Assert(bFoundTargetSlot==false);		// bad bad bad (deleting something that we just brought in on the loading list - item was on loading and deleting lists, which is bad)
			if (m_DeleteSlots[i].StreamingIn)
			{
				// now we can delete the data on the next update
				m_DeleteSlots[i].StreamingIn = false;
				m_DeleteSlots[i].Timer = 0;
				bFoundTargetSlot = true;
			}
		}
	}


	if (bFoundTargetSlot == false)
	{
		// texture was orphaned (selected to load and then something else decided to load to that slot)
		// just add ourselves to the delete list and move on
		AddToDeleteList( pStreamable, false );
	}
}


// set the given streamable (which must be resident in memory) into the given slot
// also schedules any old contents for deletion and fixes up the texture references
// Called as part of the place (post streaming load) process
void UITextureStream::MakeStreamableResident(const int cnSlot, tStreamableTextureDictionary * pStreamable)
{
	UIDebugf1("UITextureStream::MakeStreamableResident %d - %p\n", cnSlot, pStreamable);
	if (m_SlotResident[cnSlot])
	{
		StopStreaming_Resident(cnSlot);
	}

#if __DEV
	for(int i=0;i<m_DeleteSlots.GetCount();i++)
	{
		// trying to place a texture we are also deleting
		Assert(m_DeleteSlots[i].Data != pStreamable);
	}
#endif

	// Hook up this texture for this slot
	m_SlotResident[cnSlot] = pStreamable;
	if (m_TextureReferences[cnSlot])
	{
		Assert(pStreamable->GetObject());
		Assert(pStreamable->GetObject()->GetEntry(0));
		if (pStreamable->GetObject()->GetEntry(0))
			m_TextureReferences[cnSlot]->SetReference( pStreamable->GetObject()->GetEntry(0) /*dictionary only has one texture*/ );
	}

}


// user callable way to remove a streamable from memory
// does not remove the texture reference itself (though does stop the reference from pointing to that thing)
void UITextureStream::StopStreaming( const int cnSlot )
{
	if (m_SlotLoading[cnSlot])
	{
		StopStreaming_Loading(cnSlot);
	}
	if (m_SlotResident[cnSlot])
	{
		StopStreaming_Resident(cnSlot);
	}
}


void UITextureStream::StopStreaming_Loading( const int cnSlot )
{
#if __DEV
	// check if the old 'loading' slot is already loaded (bad)
	for(int i=0;i<m_SlotResident.GetMaxCount();i++)
	{
		Assert (m_SlotResident[i] != m_SlotLoading[cnSlot]);
	}
#endif					
	// check if the old 'loading' slot is loading into another slot (good)
	bool bLoadingElsewhere = false;
	for(int i=0;i<m_SlotLoading.GetMaxCount();i++)
	{
		if (m_SlotLoading[i] == m_SlotLoading[cnSlot] && i!=cnSlot)
		{
			bLoadingElsewhere = true;
			UIDebugf1("UITextureStream::StartStreaming old texture loading elsewhere %d\n", i);
		}
	}
	if (!bLoadingElsewhere)
	{
		// add to delete list
		UIDebugf1("UITextureStream::StartStreaming old texture NOT loading elsewhere %p\n", m_SlotLoading[cnSlot]);
		AddToDeleteList(m_SlotLoading[cnSlot], true);
	}
	m_SlotLoading[cnSlot] = NULL;
}



void UITextureStream::StopStreaming_Resident( const int cnSlot )
{
	bool bInUse = false;
	tStreamableTextureDictionary * pOldTexture = m_SlotResident[cnSlot];
	m_SlotResident[cnSlot] = NULL;

	// remove the reference to the streamed texture (point back to the blank transparent texture)
	if (m_TextureReferencesUseFallback[cnSlot])
		m_TextureReferences[cnSlot]->SetReference( m_pBlankTexture );
	else
		m_TextureReferences[cnSlot]->SetReference( NULL );

	for(int i=0;i<m_SlotResident.GetMaxCount();i++)
	{
		if (m_SlotResident[i] == pOldTexture)
			bInUse = true;
	}
#if __DEV
	for(int i=0;i<m_SlotLoading.GetMaxCount();i++)
	{
		// hmm we shouldn't hit this - means we were trying to load this texture in two load slots
		Assert(m_SlotLoading[i] != pOldTexture);
	}
#endif
	if (!bInUse)
	{
		AddToDeleteList(pOldTexture, false);
	}
}


