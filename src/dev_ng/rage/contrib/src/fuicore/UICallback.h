// 
// fuicore/UICallback.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FUICORE_UICALLBACK_H
#define FUICORE_UICALLBACK_H

namespace rage {

class UIObject;
class UICommand;
class UIVariant;
class fuiEvent;
struct UIInfo;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	UIVariant
//	
//	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UIVariant
{
public:
	enum UIVarType
	{
		UIVarTypeVoid,
		UIVarTypeInt,
		UIVarTypeFloat,
		UIVarTypeUIObjectPtr,
		UIVarTypeEvent,
		UIVarTypeStringKey,
		UIVarTypeStringValue,
		UIVarTypeBool
		//UIVarTypeGOHEvent
	};

protected:

	UIVarType	m_Type;

	union
	{
		int				m_Int;
		float			m_Float;
		bool			m_Bool;
		const char*		m_String;		
		void*			m_Pointer;
		fuiEvent*		m_Event;
		UIObject*		m_UIObjectPointer;
		//gohEventObject* m_GohEvent;
	};

public:

	UIVarType			GetType()const{ return m_Type; }
	int					PrintValue	(char* buf)	const 	;
	bool				LoadFromString	(char *szText, const UICommand& arg);

	UIVariant() : 
	m_Int	(-1),
		m_Type	(UIVarTypeVoid)
	{
	}

	void*			AsPointer   ()const { return m_Pointer;}
	float			AsFloat		()const 	{ Assert(m_Type==UIVarTypeFloat);return m_Float;}
	int				AsInt		()const 	{ Assert(m_Type==UIVarTypeInt);return m_Int;}
	UIObject*		AsUIObject	()const 	{ Assert(m_Type==UIVarTypeUIObjectPtr);return m_UIObjectPointer;}
	const fuiEvent* AsEvent		()const 	{ Assert(m_Type==UIVarTypeEvent);return m_Event;}
	//const gohEventObject*  AsGohEvent()const { Assert(m_Type==UIVarTypeGOHEvent);return m_GohEvent;}
	const char*		AsString	()const 	{ Assert(m_Type==UIVarTypeStringKey || m_Type==UIVarTypeStringValue);
	return m_String;	
	}
	bool			AsBool		()const	{ Assert(m_Type==UIVarTypeBool);return m_Bool;}
	bool			IsTrue		()	const	;

	void 			Set			(int i)					{ m_Int		= i;	m_Type = UIVarTypeInt;					}	
	void 			Set			(float f)				{ m_Float	= f;	m_Type = UIVarTypeFloat;					}
	void 			Set			(bool b) 				{ m_Bool	= b;	m_Type = UIVarTypeBool;					}
	void 			Set			(const char* s)			{ m_String	= s;	m_Type = UIVarTypeStringKey;				}
	void 			Set			(UIObject* o)			{ m_UIObjectPointer	= o;	m_Type = UIVarTypeUIObjectPtr;				}
	//void 			Set			(gohEventObject* o)		{ m_GohEvent	= o;m_Type = UIVarTypeGOHEvent;				}
	void 			SetToValue	(const char* s)			{ m_String	= s;	m_Type = UIVarTypeStringValue;			}
	void			Set			(const UIVariant& v)	{ *this = v;													}
};
class UIVariantLL : public UIVariant
{
public:
	UIVariantLL* m_pNext;

	UIVariantLL	() : m_pNext(NULL)	{ ; }
	~UIVariantLL	()					{ if(m_pNext) delete m_pNext; }
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	UICommand
//	
//	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class UICommand
{
public:
	UIVariantLL* m_pFirstArg;
	UICommand*	m_pNext;
	u8 m_NumArgs;
	UIObject* m_pUIObject;//replaces the external object from vhsm
	const char* m_pName;
	u32 ID;

	UICommand() :
	m_pFirstArg		(NULL),
		m_pNext			(NULL),
		m_NumArgs		(0)
	{
	}
	~UICommand();

	bool					LoadFromString				(char*	szText, const UICommand& arg);
	bool					LoadArgsFromString			(char*	szText, char cTerminator, const UICommand& arg);
	void					ProcessCommand				(UIObject*) const;
	UIVariantLL*			NewArg						();
	const UIVariantLL*	GetArg (int i) const;
	void SetName( const char* pName ){m_pName = pName;}
	const char* GetName( void ) const { return m_pName; }

};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	UIInfo
//	
//	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct UIInfo {
	UIInfo(UIVariant *resultPtr, int parameterCount, const UIVariant *params) :
ResultPtr(resultPtr), ParamCount(parameterCount), Params(params)/*,BufferCount(0)*/ { }

	UIInfo(UIObject* pObject, UIVariant *resultPtr, int parameterCount, const UIVariant *params) :
ResultPtr(resultPtr), ParamCount(parameterCount), Params(params)/*,BufferCount(0)*/, m_Object(pObject) { }

UIVariant *ResultPtr;// Return result, if applicable
int ParamCount;// Parameter count
const UIVariant *Params;// Pointer to parameter values
UIObject* m_Object;//this pointer for member functions
};

//////////////////////////////////
// Return value templates.
// UIInfo.Result should be set to the value of the second argument

template<typename T>
inline static void AssignReturnValue(UIInfo& UIInfo, T t);

inline static void AssignReturnValue(UIInfo& UIInfo, float f) {
	UIInfo.ResultPtr->Set(f);
}

inline static void AssignReturnValue(UIInfo& UIInfo, int i) {
	UIInfo.ResultPtr->Set(i);
}

//template<>
//inline static void AssignReturnValue(UIInfo& UIInfo, const Vector3& v) {
//	*reinterpret_cast<scrVector*>(&UIInfo.ResultPtr->Reference->Float) = v;
//}

inline static void AssignReturnValue(UIInfo& UIInfo, bool b) {
	UIInfo.ResultPtr->Set(b);
}

inline static void AssignReturnValue(UIInfo& UIInfo, const char* s) {
	UIInfo.ResultPtr->Set(s);
}

inline static void AssignReturnValue(UIInfo& UIInfo, UIObject* o) {
	UIInfo.ResultPtr->Set(o);
}

//////////////////////////////////
// Argument type templates.

template<typename T>
struct Arg {
	inline static T Value(UIInfo& s, int& N);
};

template<>
struct Arg<float> {
	inline static float Value(UIInfo& UIInfo, int& N) {return UIInfo.Params[N++].AsFloat();}
};

template<>
struct Arg<int> {
	inline static int Value(UIInfo& UIInfo, int& N) {return UIInfo.Params[N++].AsInt();}
};

template<>
struct Arg<bool> {
	inline static bool Value(UIInfo& UIInfo, int& N) {return UIInfo.Params[N++].AsBool();}
};

template<>
struct Arg<const char*> {
	inline static const char* Value(UIInfo& UIInfo, int& N) {return UIInfo.Params[N++].AsString();}
};

template<>
struct Arg<UIObject*> {
	inline static UIObject* Value(UIInfo& UIInfo, int& N) {return UIInfo.Params[N++].AsUIObject();}
};

template<>
struct Arg<const UIObject*> {
	inline static UIObject* Value(UIInfo& UIInfo, int& N) {return UIInfo.Params[N++].AsUIObject();}
};

//template<>
//struct Arg<const Vector3&> {
//	inline static Vector3 Value(UIInfo& UIInfo, int& N) {
//		const float* f = &UIInfo.Params[N++].Reference->Float;
//		return Vector3(f[0], f[1], f[2]);
//	}
//};

template<>
struct Arg<scrVector> {
	inline static scrVector Value(UIInfo& UIInfo, int& N) {
		const float x = UIInfo.Params[N].AsFloat();N++;
		const float y = UIInfo.Params[N].AsFloat();N++;
		const float z = UIInfo.Params[N].AsFloat();N++;
		return scrVector(x,y,z);
	}
};

//template<>
//struct Arg<const scrVector&> {
//	inline static scrVector Value(UIInfo& UIInfo, int& N) {
//		const float* f = &UIInfo.Params[N++].Reference->m_Float;
//		return scrVector(f[0], f[1], f[2]);
//	}
//};

//template<>
//struct Arg<float&> {
//	inline static float& Value(UIInfo& UIInfo, int& N) {return UIInfo.Params[N++].Reference->AsFloat();}
//};
//
//template<>
//struct Arg<int&> {
//	inline static int& Value(UIInfo& UIInfo, int& N) {return UIInfo.Params[N++].Reference->AsInt();}
//};
//
//template<>
//struct Arg<bool&> {
//	inline static bool& Value(UIInfo& UIInfo, int& N) {return UIInfo.Params[N++].Reference->AsBool() ;}
//};

// template<>
// struct Arg<const char*&> {
//	inline static const char*& Value(UIInfo& UIInfo, int& N) {return UIInfo.Params[N++].Reference->String;}
//};

//template<>
//struct Arg<Vector3&> {
//	inline static Vector3& Value(UIInfo& UIInfo, int& N) {
//		return UIInfo.GetVector3(N);
//	}
//};

///////////////////////////////////
// Type-based access to TLS values
//

template<typename T, int N>
inline static T* GetTlsGlobal() {
	scrThread *pThread = scrThread::GetCurrentThread();
	return reinterpret_cast<T*>(pThread->TLS(N).Reference.GetObject());
}

// Use this to store a particular pointer type in a threads local storage. For example if you stored an aActor pointer
// in slot 0, you would use SCR_DEFINE_TLS_TYPE_VALUE(aActor, 0). Then any functions which took an aActor* argument
// would get the aActor* in slot 0.
#define UI_DEFINE_TLS_TYPE_VALUE(tlsType, value) \
	namespace rage {\
	namespace scrWrapper {\
	template<>\
struct Arg<tlsType*> {\
	inline static tlsType* Value(UIInfo& , int&) {return ::rage::scrWrapper::GetTlsGlobal<tlsType, value>();}\
};\
}\
}\
	//END

// Should be able to do more template voodoo here. Given a conversion function, automatically create this template?
// The conversionFunc should be T(*)(scrValue&), and Arg::Value should return T.
#define UI_DEFINE_NEW_PARAM_TYPE(newType, conversionFunc) \
	namespace rage{\
	namespace scrWrapper {\
	template<>\
struct Arg<newType> {\
	inline static newType Value(UIInfo& info, int& N) {return conversionFunc(info.Params[N++]);}\
};\
}\
}\
//END

// PURPOSE: Define newType as a new wrappable type for both function parameters and return values.
#define UI_DEFINE_NEW_POINTER_PARAM_AND_RET(newType) \
	template<>\
struct Arg<newType*> {\
	inline static newType* Value(UIInfo& info, int& N) {return reinterpret_cast<newType*>(info.Params[N++].AsPointer());}\
};\
	template<>\
struct Arg<const newType*> {\
	inline static const newType* Value(UIInfo& info, int& N) {return reinterpret_cast<const newType*>(info.Params[N++].AsPointer());}\
};\
	/*
	template<>\
	inline static void AssignReturnValue(UIInfo& info, newType* p) {info.ResultPtr = reinterpret_cast<scrValue*>(const_cast<newType*>(p));}\
	template<>\
	inline static void AssignReturnValue(UIInfo& info, const newType* p) {info.ResultPtr->AsPointer() = reinterpret_cast<scrValue*>(const_cast<newType*>(p));}\
	*/
//END

UI_DEFINE_NEW_POINTER_PARAM_AND_RET(UIVariant);

///////////////////////////////////
// Member function call templates.
// Each of these comes in two forms. One for a void return value, one for non-void.

// Non-void return types
template <typename c, typename RT>
inline void CallFunction(RT(c::*M)(), UIInfo& s)
{
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)());
}

template <typename c, typename RT, typename T0>
inline void CallFunction(RT(c::*M)(T0), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0));
}

template <typename c, typename RT, typename T0, typename T1>
inline void CallFunction(RT(c::*M)(T0, T1), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1));
}

template <typename c, typename RT, typename T0, typename T1, typename T2>
inline void CallFunction(RT(c::*M)(T0, T1, T2), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2));
}

template <typename c, typename RT, typename T0, typename T1, typename T2, typename T3>
inline void CallFunction(RT(c::*M)(T0, T1, T2, T3), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2, t3));
}

template <typename c, typename RT, typename T0, typename T1, typename T2, typename T3, typename T4>
inline void CallFunction(RT(c::*M)(T0, T1, T2, T3, T4), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4));
}

template <typename c, typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5>
inline void CallFunction(RT(c::*M)(T0, T1, T2, T3, T4, T5), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5));
}

template <typename c, typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
inline void CallFunction(RT(c::*M)(T0, T1, T2, T3, T4, T5, T6), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6));
}

template <typename c, typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
inline void CallFunction(RT(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7));
}

template <typename c, typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
inline void CallFunction(RT(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8));
}

template <typename c, typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
inline void CallFunction(RT(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9));
}

template <typename c, typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
inline void CallFunction(RT(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10));
}

template <typename c, typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
inline void CallFunction(RT(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11));
}

template <typename c, typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
inline void CallFunction(RT(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12));
}

template <typename c, typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13>
inline void CallFunction(RT(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13));
}

template <typename c, typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
inline void CallFunction(RT(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14));
}

template <typename c, typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
inline void CallFunction(RT(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	T15 t15 = Arg<T15>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15));
}

template <typename c, typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16>
inline void CallFunction(RT(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	T15 t15 = Arg<T15>::Value(s, N);
	T16 t16 = Arg<T16>::Value(s, N);
	AssignReturnValue(s, (((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16));
}

// Void return types

template <typename c>
inline void CallFunction(void(c::*M)(), UIInfo& s)
{
	(((c*)(s.m_Object))->*M)(); 
}

template <typename c,typename T0>
inline void CallFunction(void(c::*M)(T0), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0);
}

template <typename c, typename T0, typename T1>
inline void CallFunction(void(c::*M)(T0, T1), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1);
}

template <typename c, typename T0, typename T1, typename T2>
inline void CallFunction(void(c::*M)(T0, T1, T2), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2);
}

template <typename c, typename T0, typename T1, typename T2, typename T3>
inline void CallFunction(void(c::*M)(T0, T1, T2, T3), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2, t3);
}

template <typename c, typename T0, typename T1, typename T2, typename T3, typename T4>
inline void CallFunction(void(c::*M)(T0, T1, T2, T3, T4), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4);
}

template <typename c, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5>
inline void CallFunction(void(c::*M)(T0, T1, T2, T3, T4, T5), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5);
}

template <typename c, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
inline void CallFunction(void(c::*M)(T0, T1, T2, T3, T4, T5, T6), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6);
}

template <typename c, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
inline void CallFunction(void(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7);
}

template <typename c, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
inline void CallFunction(void(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8);
}

template <typename c, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
inline void CallFunction(void(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9);
}

template <typename c, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
inline void CallFunction(void(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10);
}

template <typename c, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
inline void CallFunction(void(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11);
}

template <typename c, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
inline void CallFunction(void(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12);
}

template <typename c, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13>
inline void CallFunction(void(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13);
}

template <typename c, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
inline void CallFunction(void(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14);
}

template <typename c, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
inline void CallFunction(void(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	T15 t15 = Arg<T15>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15);
}

template <typename c, typename C, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16>
inline void CallFunction(void(c::*M)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	T15 t15 = Arg<T15>::Value(s, N);
	T16 t16 = Arg<T16>::Value(s, N);
	(((c*)(s.m_Object))->*M)(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16);
}

///////////////////////////////////
// Static function call templates.
// Each of these comes in two forms. One for a void return value, one for non-void.

// Non-void return types
template <typename RT>
inline void CallFunction(RT(*fn)(), UIInfo& s)
{
	AssignReturnValue(s, fn());
}

template <typename RT, typename T0>
inline void CallFunction(RT(*fn)(T0), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	AssignReturnValue(s, fn(t0));
}

template <typename RT, typename T0, typename T1>
inline void CallFunction(RT(*fn)(T0, T1), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1));
}

template <typename RT, typename T0, typename T1, typename T2>
inline void CallFunction(RT(*fn)(T0, T1, T2), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3>
inline void CallFunction(RT(*fn)(T0, T1, T2, T3), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2, t3));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4>
inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5>
inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13>
inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	T15 t15 = Arg<T15>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16>
inline void CallFunction(RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	T15 t15 = Arg<T15>::Value(s, N);
	T16 t16 = Arg<T16>::Value(s, N);
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16));
}

// Void return types

template <>
inline void CallFunction(void(*fn)(), UIInfo& )
{
	fn();
}

template <typename T0>
inline void CallFunction(void(*fn)(T0), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	fn(t0);
}

template <typename T0, typename T1>
inline void CallFunction(void(*fn)(T0, T1), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	fn(t0, t1);
}

template <typename T0, typename T1, typename T2>
inline void CallFunction(void(*fn)(T0, T1, T2), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	fn(t0, t1, t2);
}

template <typename T0, typename T1, typename T2, typename T3>
inline void CallFunction(void(*fn)(T0, T1, T2, T3), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	fn(t0, t1, t2, t3);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4>
inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	fn(t0, t1, t2, t3, t4);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5>
inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	fn(t0, t1, t2, t3, t4, t5);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	fn(t0, t1, t2, t3, t4, t5, t6);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	fn(t0, t1, t2, t3, t4, t5, t6, t7);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13>
inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	T15 t15 = Arg<T15>::Value(s, N);
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16>
inline void CallFunction(void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16), UIInfo& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	T15 t15 = Arg<T15>::Value(s, N);
	T16 t16 = Arg<T16>::Value(s, N);
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16);
}

// Given a function pointer, return a function pointer that calls CallFunction with the f.p. and the scrThread::UIInfo
#define UI_REGISTER(c, fn) \
struct uiWrapped_ ## fn {\
	static void Call(UIInfo& s) {\
	::CallFunction<c>(&c::fn, s);\
}\
};\
	::UIObject::RegisterCommand(this, #fn, uiWrapped_ ## fn::Call,this)\
	//END

#define UI_REGISTER_ADV(c, name, fn, i) \
struct uiWrapped_ ## name {\
	static void Call(UIInfo& s) {\
	::CallFunction<c>(&c::fn, s);\
}\
};\
	::UIObject::RegisterCommand(this, #name, uiWrapped_ ## name::Call,i)\
	//END

#define UI_REGISTER_STATIC(name,fn) \
struct uiWrapped_ ## name {\
	static void Call(UIInfo& s) {\
	::CallFunction(fn, s);\
}\
};\
	::UIObject::RegisterCommand(this, #name, uiWrapped_ ## name::Call,this)\
	//END
} // namespace rage
#endif // #ifndef FLASHUIMISC_EVENT_H
