// 
// flashUIComponents/FlashUIContexts.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 

#include "fuiflash/FlashUIContexts.h"
#include "fuiflash/Flash.h"
#include "fuicore/UIManager.h"

#define verify_if(x) if(AssertVerify( (x) ))

namespace rage
{

// this will cause the game to do less dynamic string hashing during the frame
fuiContext::HashedString _vCONTEXT_LIST = {CONTEXT_LIST, atStringHash(CONTEXT_LIST)};
fuiContext::HashedString _vCONTEXT_LIST_CHILDREN = {CONTEXT_LIST_CHILDREN  , atStringHash(CONTEXT_LIST_CHILDREN )};
fuiContext::HashedString _vCONTEXT_LIST_NUMVISIBLECHILDREN ={CONTEXT_LIST_NUMVISIBLECHILDREN , atStringHash(CONTEXT_LIST_NUMVISIBLECHILDREN )};
fuiContext::HashedString _vCONTEXT_LIST_CURRENTSELECTION ={CONTEXT_LIST_CURRENTSELECTION , atStringHash(CONTEXT_LIST_CURRENTSELECTION)};
fuiContext::HashedString _vCONTEXT_TABS = {CONTEXT_TABS, atStringHash(CONTEXT_TABS)};
fuiContext::HashedString _vCONTEXT_TABS_CHILDREN = {CONTEXT_TABS_CHILDREN, atStringHash(CONTEXT_TABS_CHILDREN)};
fuiContext::HashedString _vCONTEXT_MESSAGEBOX_OPTIONSLIST = {CONTEXT_MESSAGEBOX_OPTIONSLIST, atStringHash(CONTEXT_MESSAGEBOX_OPTIONSLIST)};
fuiContext::HashedString _vCONTEXT_MESSAGEBOX_ISVISIBLE = {CONTEXT_MESSAGEBOX_ISVISIBLE, atStringHash(CONTEXT_MESSAGEBOX_ISVISIBLE)};
fuiContext::HashedString _vCONTEXT_MESSAGEBOX_CHILDREN = {CONTEXT_MESSAGEBOX_CHILDREN, atStringHash(CONTEXT_MESSAGEBOX_CHILDREN)};
fuiContext::HashedString _vCONTEXT_PROMPTSTRIP = {CONTEXT_PROMPTSTRIP, atStringHash(CONTEXT_PROMPTSTRIP)};
fuiContext::HashedString _vCONTEXT_PROMPTSTRIP_CHILDREN = {CONTEXT_PROMPTSTRIP_CHILDREN, atStringHash(CONTEXT_PROMPTSTRIP_CHILDREN)};
fuiContext::HashedString _vCONTEXT_PROMPTSTRIP_NUMPROMPTS = {CONTEXT_PROMPTSTRIP_NUMPROMPTS, atStringHash(CONTEXT_PROMPTSTRIP_NUMPROMPTS)};
fuiContext::HashedString _vCONTEXT_PROMPTSTRIP_ISVISIBLE = {CONTEXT_PROMPTSTRIP_ISVISIBLE, atStringHash(CONTEXT_PROMPTSTRIP_ISVISIBLE)};
fuiContext::HashedString _vCONTEXT_ICON_VALUE = {CONTEXT_ICON_VALUE, atStringHash(CONTEXT_ICON_VALUE)};
fuiContext::HashedString _vCONTEXT_LABEL_VALUE = {CONTEXT_LABEL_VALUE, atStringHash(CONTEXT_LABEL_VALUE)};
fuiContext::HashedString _vCONTEXT_VISIBLE = {CONTEXT_VISIBLE, atStringHash(CONTEXT_VISIBLE)};
fuiContext::HashedString _vCONTEXT_FOCUSED = {CONTEXT_FOCUSED, atStringHash(CONTEXT_FOCUSED)};
fuiContext::HashedString _vCONTEXT_ENABLED = {CONTEXT_ENABLED, atStringHash(CONTEXT_ENABLED)};
fuiContext::HashedString _vCONTEXT_ACTIVE = {CONTEXT_ACTIVE, atStringHash(CONTEXT_ACTIVE)};
fuiContext::HashedString _vCONTEXT_COLOR = {CONTEXT_COLOR, atStringHash(CONTEXT_COLOR)};
fuiContext::HashedString _vCONTEXT_SUBLIST= {CONTEXT_SUBLIST, atStringHash(CONTEXT_SUBLIST)};
fuiContext::HashedString _vCONTEXT_PROGRESSBAR_INDETERMINATE = {CONTEXT_PROGRESSBAR_INDETERMINATE, atStringHash(CONTEXT_PROGRESSBAR_INDETERMINATE)};
fuiContext::HashedString _vCONTEXT_PROGRESSBAR_ISVISIBLE = {CONTEXT_PROGRESSBAR_ISVISIBLE, atStringHash(CONTEXT_PROGRESSBAR_ISVISIBLE)};
fuiContext::HashedString _vCONTEXT_TABS_NUMVISIBLECHILDREN = {CONTEXT_TABS_NUMVISIBLECHILDREN, atStringHash(CONTEXT_TABS_NUMVISIBLECHILDREN)};
fuiContext::HashedString _vCONTEXT_TABS_CURRENTSELECTION = {CONTEXT_TABS_CURRENTSELECTION, atStringHash(CONTEXT_TABS_CURRENTSELECTION)};
fuiContext::HashedString _vCONTEXT_MESSAGEBOX_TITLE = {CONTEXT_MESSAGEBOX_TITLE, atStringHash(CONTEXT_MESSAGEBOX_TITLE)};
fuiContext::HashedString _vCONTEXT_MESSAGEBOX_TITLE_DEFAULTVALUE= {CONTEXT_MESSAGEBOX_TITLE_DEFAULTVALUE, atStringHash(CONTEXT_MESSAGEBOX_TITLE_DEFAULTVALUE)};
fuiContext::HashedString _vCONTEXT_MESSAGEBOX_DESCRIPTION = {CONTEXT_MESSAGEBOX_DESCRIPTION, atStringHash(CONTEXT_MESSAGEBOX_DESCRIPTION)};
fuiContext::HashedString _vCONTEXT_MESSAGEBOX_DESCRIPTION_DEFAULTVALUE= {CONTEXT_MESSAGEBOX_DESCRIPTION_DEFAULTVALUE, atStringHash(CONTEXT_MESSAGEBOX_DESCRIPTION_DEFAULTVALUE)};

//commenting out temporarily until i can sort out an easy way to do this without forcing all new contexts to do the same
//#undef CONTEXT_LIST
//#define CONTEXT_LIST swfVARIANT(_vCONTEXT_LIST.String, _vCONTEXT_LIST.Hash)
//#undef CONTEXT_LIST_CHILDREN 
//#define CONTEXT_LIST_CHILDREN swfVARIANT(_vCONTEXT_LIST_CHILDREN.String, _vCONTEXT_LIST_CHILDREN.Hash)
//#undef CONTEXT_LIST_NUMVISIBLECHILDREN 
//#define CONTEXT_LIST_NUMVISIBLECHILDREN swfVARIANT(_vCONTEXT_LIST_NUMVISIBLECHILDREN.String, _vCONTEXT_LIST_NUMVISIBLECHILDREN.Hash)
//#undef CONTEXT_LIST_CURRENTSELECTION 
//#define CONTEXT_LIST_CURRENTSELECTION swfVARIANT(_vCONTEXT_LIST_CURRENTSELECTION.String, _vCONTEXT_LIST_CURRENTSELECTION.Hash)
//#undef CONTEXT_TABS 
//#define CONTEXT_TABS swfVARIANT(_vCONTEXT_TABS.String, _vCONTEXT_TABS.Hash)
//#undef CONTEXT_TABS_CHILDREN 
//#define CONTEXT_TABS_CHILDREN swfVARIANT(_vCONTEXT_TABS_CHILDREN.String, _vCONTEXT_TABS_CHILDREN.Hash)
//#undef CONTEXT_TABS_NUMVISIBLECHILDREN 
//#define CONTEXT_TABS_NUMVISIBLECHILDREN swfVARIANT(_vCONTEXT_TABS_NUMVISIBLECHILDREN.String, _vCONTEXT_TABS_NUMVISIBLECHILDREN.Hash)
//#undef CONTEXT_TABS_CURRENTSELECTION 
//#define CONTEXT_TABS_CURRENTSELECTION swfVARIANT(_vCONTEXT_TABS_CURRENTSELECTION.String, _vCONTEXT_TABS_CURRENTSELECTION.Hash)
//#undef CONTEXT_MESSAGEBOX_TITLE 
//#define CONTEXT_MESSAGEBOX_TITLE swfVARIANT(_vCONTEXT_MESSAGEBOX_TITLE.String, _vCONTEXT_MESSAGEBOX_TITLE.Hash)
//#undef CONTEXT_MESSAGEBOX_TITLE_DEFAULTVALU 
//#define CONTEXT_MESSAGEBOX_TITLE_DEFAULTVALU swfVARIANT(_vCONTEXT_MESSAGEBOX_TITLE_DEFAULTVALU.String, _vCONTEXT_MESSAGEBOX_TITLE_DEFAULTVALU.Hash)
//#undef CONTEXT_MESSAGEBOX_DESCRIPTION 
//#define CONTEXT_MESSAGEBOX_DESCRIPTION swfVARIANT(_vCONTEXT_MESSAGEBOX_DESCRIPTION.String, _vCONTEXT_MESSAGEBOX_DESCRIPTION.Hash)
//#undef CONTEXT_MESSAGEBOX_DESCRIPTION_DEFAULTVALU 
//#define CONTEXT_MESSAGEBOX_DESCRIPTION_DEFAULTVALU swfVARIANT(_vCONTEXT_MESSAGEBOX_DESCRIPTION_DEFAULTVALU.String, _vCONTEXT_MESSAGEBOX_DESCRIPTION_DEFAULTVALU.Hash)
//#undef CONTEXT_MESSAGEBOX_OPTIONSLIST 
//#define CONTEXT_MESSAGEBOX_OPTIONSLIST swfVARIANT(_vCONTEXT_MESSAGEBOX_OPTIONSLIST.String, _vCONTEXT_MESSAGEBOX_OPTIONSLIST.Hash)
//#undef CONTEXT_MESSAGEBOX_ISVISIBLE 
//#define CONTEXT_MESSAGEBOX_ISVISIBLE swfVARIANT(_vCONTEXT_MESSAGEBOX_ISVISIBLE.String, _vCONTEXT_MESSAGEBOX_ISVISIBLE.Hash)
//#undef CONTEXT_MESSAGEBOX_CHILDREN 
//#define CONTEXT_MESSAGEBOX_CHILDREN swfVARIANT(_vCONTEXT_MESSAGEBOX_CHILDREN.String, _vCONTEXT_MESSAGEBOX_CHILDREN.Hash)
//#undef CONTEXT_PROMPTSTRIP 
//#define CONTEXT_PROMPTSTRIP swfVARIANT(_vCONTEXT_PROMPTSTRIP.String, _vCONTEXT_PROMPTSTRIP.Hash)
//#undef CONTEXT_PROMPTSTRIP_CHILDREN 
//#define CONTEXT_PROMPTSTRIP_CHILDREN swfVARIANT(_vCONTEXT_PROMPTSTRIP_CHILDREN.String, _vCONTEXT_PROMPTSTRIP_CHILDREN.Hash)
//#undef CONTEXT_PROMPTSTRIP_NUMPROMPTS 
//#define CONTEXT_PROMPTSTRIP_NUMPROMPTS swfVARIANT(_vCONTEXT_PROMPTSTRIP_NUMPROMPTS.String, _vCONTEXT_PROMPTSTRIP_NUMPROMPTS.Hash)
//#undef CONTEXT_PROMPTSTRIP_ISVISIBLE 
//#define CONTEXT_PROMPTSTRIP_ISVISIBLE swfVARIANT(_vCONTEXT_PROMPTSTRIP_ISVISIBLE.String, _vCONTEXT_PROMPTSTRIP_ISVISIBLE.Hash)
//#undef CONTEXT_ICON_VALUE 
//#define CONTEXT_ICON_VALUE swfVARIANT(_vCONTEXT_ICON_VALUE.String, _vCONTEXT_ICON_VALUE.Hash)
//#undef CONTEXT_LABEL_VALUE 
//#define CONTEXT_LABEL_VALUE swfVARIANT(_vCONTEXT_LABEL_VALUE.String, _vCONTEXT_LABEL_VALUE.Hash)
//#undef CONTEXT_VISIBLE 
//#define CONTEXT_VISIBLE swfVARIANT(_vCONTEXT_VISIBLE.String, _vCONTEXT_VISIBLE.Hash)
//#undef CONTEXT_FOCUSED 
//#define CONTEXT_FOCUSED swfVARIANT(_vCONTEXT_FOCUSED.String, _vCONTEXT_FOCUSED.Hash)
//#undef CONTEXT_ENABLED 
//#define CONTEXT_ENABLED swfVARIANT(_vCONTEXT_ENABLED.String, _vCONTEXT_ENABLED.Hash)
//#undef CONTEXT_ACTIVE 
//#define CONTEXT_ACTIVE swfVARIANT(_vCONTEXT_ACTIVE.String, _vCONTEXT_ACTIVE.Hash)
//#undef CONTEXT_COLOR 
//#define CONTEXT_COLOR swfVARIANT(_vCONTEXT_COLOR.String, _vCONTEXT_COLOR.Hash)
//#undef CONTEXT_SUBLIST 
//#define CONTEXT_SUBLIST swfVARIANT(_vCONTEXT_SUBLIST.String, _vCONTEXT_SUBLIST.Hash)
//#undef CONTEXT_PROGRESSBAR_INDETERMINATE 
//#define CONTEXT_PROGRESSBAR_INDETERMINATE swfVARIANT(_vCONTEXT_PROGRESSBAR_INDETERMINATE.String, _vCONTEXT_PROGRESSBAR_INDETERMINATE.Hash)
//#undef CONTEXT_PROGRESSBAR_ISVISIBLE 
//#define CONTEXT_PROGRESSBAR_ISVISIBLE swfVARIANT(_vCONTEXT_PROGRESSBAR_ISVISIBLE.String, _vCONTEXT_PROGRESSBAR_ISVISIBLE.Hash)

swfCONTEXT* fuiContext::GetContext()
{	
	if(m_MovieID >= 0)
	{	
		if (FLASHMANAGER->GetMovie(m_MovieID)->IsLoaded())
		{
			return FLASHMANAGER->GetMovie(m_MovieID)->GetContext();
		}
		else
		{
			return NULL;
		}
	}
	else
	{
		UIObject* pParent = GetParent();

		if (pParent)
		{
			UIContext* pContext = verify_cast<UIComponent*>(pParent)->GetContext();
			if (pContext)
			{
				fuiContext* pf = dynamic_cast<fuiContext*>(pContext);
				return pf->GetContext();
			}
			else
			{
				//can a parent not be a component here?
				return NULL;
			}
		}
		else
		{
			fuiMovie* pMovie = GetMovie();
			if (pMovie)
			{
				return pMovie->GetContext();
			}
			else
			{
				return NULL;
			}
		}
	}
}

void fuiContext::OnShow()
{
	if (m_MovieID >= 0)
	{
		FLASHMANAGER->GetMovie(m_MovieID)->OnShow();
	}
}

void fuiContext::OnHide()
{
	if (m_MovieID >= 0)
	{
		FLASHMANAGER->GetMovie(m_MovieID)->OnHide();
	}
}

fuiMovie* fuiContext::GetMovie()
{
	if (m_MovieID != -1)
		return FLASHMANAGER->GetMovie(m_MovieID);


	UIObject* pParent = GetParent();

	if (pParent && pParent->GetContext())
	{
		fuiContext* pContext = dynamic_cast<fuiContext*>(verify_cast<UIComponent*>(pParent)->GetContext());
		if(pContext)
			return pContext->GetMovie();
	}

	return NULL;
}

swfVARIANT* fuiContext::GetVariantSimple(const char* value,swfSCRIPTOBJECT* prop)
{
	const swfVARIANT variant(value);

	if (prop)
	{
		return prop->AccessProperty(variant);
	}
	else if (m_pFlashObject)
	{
		return m_pFlashObject->AccessProperty(variant.GetStringHash(),variant.asConstString());
	}
	else
	{
		return GetContext()->GetVariant(variant.GetStringHash(), variant.asConstString());
	}
}

//parses this string and find the correct variants
swfVARIANT* fuiContext::GetVariant(const char* value)
{
	//movie must be loaded or streamed in
	if (!GetContext())
	{
		UIDebugf3("Could not get variant %s, movie not loaded or streamed",value);
		return NULL;
	}

	const char* dot = strstr(value,".");		
	const char* afterDot = NULL;
	char currentValue[256];
	int index=0;
	rage::swfVARIANT* pObject = NULL;

	GRAB_FLASH_UPDATE_SEMA();

	while(dot)
	{
		//take off the dot and rest of stuff after the dot and try to get just the first object		
		//get the first object
		afterDot = dot+1;
		index = (int)(dot - value);
		strncpy(currentValue,value,256);
		currentValue[index] = '\0';
		
		//get the next variant
		pObject = GetVariant(currentValue,pObject);
		
		dot = strstr(afterDot,".");
	}

	if (afterDot)
	{
		return GetVariant(afterDot,pObject);
	}
	else
	{
		return GetVariant(value,pObject);
	}
}

// getting a const string variant passed in means that the string is already hashed
swfVARIANT* fuiContext::GetVariant(const char* value ,swfVARIANT* variant)
{
	//movie must be loaded or streamed in
	if (!GetContext())
	{
		UIDebugf3("Could not get variant %s, movie not loaded or streamed",value);
		return NULL;
	}

	swfSCRIPTOBJECT* property = NULL;
	const char* array = strstr(value,"[");		
	char currentValue[256];
	int index=0;

	GRAB_FLASH_UPDATE_SEMA();

	if (variant)
	{
		property = verify_cast<swfSCRIPTOBJECT*>(variant->asObject());
	}

	if (array)
	{
		const char* leftBracket = strstr(value,"[");
		strncpy(currentValue,value,256);
		index = (int)(leftBracket - &value[index]);
		currentValue[index] = '\0';

		rage::swfVARIANT* pArrayVariant = GetVariantSimple(currentValue, property);

		//get the index				
		strncpy(currentValue,leftBracket+1,256);
		const char* rightBracket = strstr(currentValue,"]");
		Assertf(rightBracket, "Found [ without matching ]");
		index = (int)(rightBracket - currentValue);
		currentValue[index] = '\0';
		index = atoi(currentValue);
		Assertf(index >= 0 && index < ((rage::swfSCRIPTARRAY*)pArrayVariant->asObject())->GetCount(),"Your flash variant index is out of range");

		return &(*(rage::swfSCRIPTARRAY*)pArrayVariant->asObject())[index];
	}
	else
	{
		return GetVariantSimple(value, property);
	}

#if __PS3
	// Should never get here
	/*NOTREACHED*/
	return NULL;
#endif // __PS3
}

// getting a const string variant passed in means that the string is already hashed
swfVARIANT* fuiContext::GetArrayVariant(const char *ArrayName, int iItemIndex)
{
	swfCONTEXT*	pContext= GetContext();

	if (pContext)
	{
		swfSCRIPTARRAY& rArray	= fuiMovie::GetArray(*pContext, ArrayName);

		verify_if( iItemIndex < rArray.GetCount() )
			return &rArray[iItemIndex];
	}


	return NULL;
}

void fuiContext::ProcessDataTag(const char* name , const char* value)
{
	if(stricmp(name,"movie")==0)
	{
		SetMovieID(FLASHMANAGER->GetMovieID(value));
	}
	else if(stricmp(name,"context")==0)
	{
		SetMovieID(FLASHMANAGER->GetMovieID(value));
	}
	else if(stricmp(name,"flashObject")==0)
	{
		m_pFlashObjectName = UIFACTORY->CreateString(value);

		//swfVARIANT* pVariant = GetVariant(value);
		//if (pVariant && pVariant->Type == AS_OBJECT)
		//{
		//	SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(pVariant->asObject()));
		//}
	}
	//else if(stricmp(name,"visibleVariant")==0)
	//{
	//	m_pIsVisible = UIFACTORY->CreateString(value);
	//}
	//else if(stricmp(name,"focusedVariant")==0)
	//{
	//	m_pIsFocused = UIFACTORY->CreateString(value);
	//}
	//else if(stricmp(name,"activeVariant")==0)
	//{
	//	m_pIsActive = UIFACTORY->CreateString(value);
	//}
	//else if(stricmp(name,"enabledVariant")==0)
	//{
	//	m_pIsEnabled = UIFACTORY->CreateString(value);
	//}
}

void fuiContext::SetFlashObject(swfSCRIPTOBJECT* pParent)
{
	m_pFlashObject = pParent;
}

void fuiContext::NotifyTransition()
{
	NotifyTransition(0);
}

void fuiContext::NotifyTransition(int level)
{
	if (GetContext())
	{
		//Ensure lower levels don't get forgotten:
		int iOldLevel;
		GetContext()->GetGlobal("TransitionLevel", iOldLevel );
		if(iOldLevel < 0 || level > iOldLevel)
		{
			GetContext()->SetGlobal("TransitionLevel", level );
		}		
	}
}

void fuiContext::SetTemplate(int t)
{
	if (GetContext())
	{
		GetContext()->SetGlobal("template", t );
	}
}

void fuiContext::Reset()
{
	//we want flash to be defaults anytime we Reset.  then fui will paint and set things up
	ResetFlashObjectToDefaults();

	//put this here cause we need to set this when things are already shown but movie has not streamed in yet
	fuiMovie* pMovie = GetMovie();
	if (m_pOwner && m_pOwner->IsVisible() && pMovie && pMovie->IsLoaded())
	{
		fuiMovie::SetGlobalInt(pMovie,"cur_visibility",1);
	}
}

void fuiContext::OnActivate()
{
	//ResetFlashObjectToDefaults();
	//Reset();
}

void fuiContext::OnDeactivate()
{
	ResetFlashObjectToDefaults();
} 

void fuiContext::Refresh(int level)
{
	if(m_pFlashObject && GetContext())
	{
		swfVARIANT* v = m_pFlashObject->AccessProperty("refresh");
		
		if (v && v->asInt() < level)
		{
			v->Set(level);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiLayerContext
//A Flash context interface to be used with UILayer 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void fuiLayerContext::PostStream(char movieID)
{
	//if we are using a movie that was just streamed in, then lets fixup all our variants ect
	if ( m_MovieID != -1 && movieID != m_MovieID)
	{
		return;
	}

	Reset();
}

void fuiLayerContext::OnShow()
{
	fuiContext::OnShow();
	NotifyTransition(1);
	Refresh(1);
}

void fuiLayerContext::OnHide()
{
	fuiContext::OnHide();
	NotifyTransition(1);	
}

void fuiLayerContext::Reset()
{
	fuiContext::Reset();

	if (GetContext())
	{
		//reset all children
		for(u32 i=0;i<m_pOwner->GetNumberOfChildren();i++)
		{
			UIComponent* pChild = verify_cast<UIComponent*>(m_pOwner->GetChild(i));
			UIContext* pUIContext = pChild->GetContext();
			fuiContext* pContext = dynamic_cast<fuiContext*>(pUIContext);
			Assert(pContext);
			pContext->Reset();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiPanelContext
//A Flash context interface to be used with UIPanel 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//void fuiPanelContext::Reset()
//{
//	fuiContext::Reset();
//
//	//reset all children
//	for(u32 i=0;i<m_pOwner->GetNumberOfChildren();i++)
//	{
//		UIComponent* pChild = verify_cast<UIComponent*>(m_pOwner->GetChild(i));
//		UIContext* pUIContext = pChild->GetContext();
//		fuiContext* pContext = dynamic_cast<fuiContext*>(pUIContext);
//		Assert(pContext);
//		pContext->Reset();
//	}
//}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiLabelContext
//A Flash context interface to be used with UITabbedContainer 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//void fuiLabelContext::ProcessDataTag(char* name , char* value)
//{
//	if(stricmp(name,"textVariant")==0)
//	{
//		m_pText = UIFACTORY->CreateString(value);
//	}
//	else
//	{
//		fuiContext::ProcessDataTag(name,value);
//	}
//}

void fuiLabelContext::Reset()
{
	fuiContext::Reset();
	if (m_pFlashObjectName && GetContext())
	{
		swfVARIANT* v = GetVariant(m_pFlashObjectName);
		if (v && v->Type == AS_OBJECT)
		{
			SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));
		}
	}
}

void fuiLabelContext::SetText(const char* stringID)
{
	swfVARIANT* v = GetVariant(CONTEXT_LABEL_VALUE);

	if (!v)
		return;

	if (!stringID) 
		v->Set("");
	else
		v->Set(stringID);

	NotifyTransition(0);
	Refresh(1);
}


//setup all variants based off the given parent
void fuiLabelContext::SetFlashObject(swfSCRIPTOBJECT* pParent)
{
	m_pFlashObject = pParent;
	//m_pText = pParent->AccessProperty(atStringHash(CONTEXT_LABEL_VALUE),CONTEXT_LABEL_VALUE);
	//m_pColor = pParent->AccessProperty(atStringHash(CONTEXT_LABEL_COLOR),CONTEXT_LABEL_COLOR);
}

void fuiLabelContext::PostStream(char movieID)
{
	//if (!m_pFlashObject)
	//{
	//	m_pFlashObject = m_pContext->GetVariant(CONTEXT_SCROLLBAR_VALUE);
	//}

	//if we are using a movie that was just streamed in, then lets fixup all our variants ect
	if ( m_MovieID != -1 && movieID != m_MovieID)
	{
		return;
	}

	Reset();
}

void fuiLabelContext::ResetFlashObjectToDefaults()
{	
	rage::swfVARIANT varTargetProperty;

	//lets clear the contexts values in preparation for population
	if (m_pFlashObject)
	{
		//unfortunately we must diverge from the cool OOD we have and just set all known possible objects that can go in this cell
		swfVARIANT* prop;
		prop = m_pFlashObject->AccessProperty(CONTEXT_LABEL_VALUE);
		if (prop) prop->Set(CONTEXT_LABEL_VALUE_DEFAULTVALUE);
		prop = m_pFlashObject->AccessProperty(CONTEXT_ICON_VALUE);
		if (prop) prop->Set(CONTEXT_ICON_VALUE_DEFAULTVALUE);
		prop = m_pFlashObject->AccessProperty(CONTEXT_COLOR);
		if (prop) prop->Set(CONTEXT_LABEL_COLOR_DEFAULTVALUE);
	}
	else
	{
		UIDisplayf("This movie is not streamed/loaded or this label (%s) has no map to flash",m_pOwner->GetName());
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiIconContext
//A Flash context interface to be used with UIIcon 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//void fuiIconContext::ProcessDataTag(char* name , char* value)
//{
//	if(stricmp(name,"iconVariant")==0)
//	{
//		m_pIconType = UIFACTORY->CreateString(value);
//	}
//	else
//	{
//		fuiContext::ProcessDataTag( name,value);
//	}
//}

void fuiIconContext::Reset()
{
	fuiContext::Reset();
	if (m_pFlashObjectName && GetContext())
	{
		swfVARIANT* v = GetVariant(m_pFlashObjectName);
		if (v && v->Type == AS_OBJECT)
		{
			SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));
		}
	}
}

//setup all variants based off the given parent
void fuiIconContext::SetFlashObject(swfSCRIPTOBJECT* pParent)
{
	m_pFlashObject = pParent;
	//m_pIconType = pParent->AccessProperty(atStringHash(CONTEXT_ICON_VALUE),CONTEXT_ICON_VALUE);
}

void fuiIconContext::SetIcon(UIIcon::IconType type)
{
	swfVARIANT* v = GetVariant(CONTEXT_ICON_VALUE);
	
	//notify flash to animate or whatever if the icon has changed
	if (v && (v->asInt() != (int)type))
	{
		NotifyTransition(0);
		Refresh(1);
		v->Set((int)type);
	}
}

void fuiIconContext::SetIcon(UIPromptStrip::PromptIcons type)
{
	swfVARIANT* v = GetVariant(CONTEXT_ICON_VALUE);

	//notify flash to animate or whatever if the icon has changed
	if (v && v->asInt() != (int)type)
	{
		NotifyTransition(0);
		Refresh(1);
		v->Set((int)type);
	}
}

void fuiIconContext::ResetFlashObjectToDefaults()
{
	rage::swfVARIANT varTargetProperty;

	//lets clear the contexts values in preparation for population
	if (m_pFlashObject)
	{
		//unfortunately we must diverge from the cool OOD we have and just set all known possible objects that can go in this cell
		swfVARIANT* prop;
		prop = m_pFlashObject->AccessProperty(CONTEXT_LABEL_VALUE);
		if (prop) prop->Set(CONTEXT_LABEL_VALUE_DEFAULTVALUE);
		prop = m_pFlashObject->AccessProperty(CONTEXT_ICON_VALUE);
		if (prop) prop->Set(CONTEXT_ICON_VALUE_DEFAULTVALUE);
		prop = m_pFlashObject->AccessProperty(CONTEXT_COLOR);
		if (prop) prop->Set(CONTEXT_LABEL_COLOR_DEFAULTVALUE);
	}
	else
	{
		UIDisplayf("This movie is not streamed/loaded or this icon (%s) has no map to flash",m_pOwner->GetName());
	}
}

void fuiIconContext::PostStream(char movieID)
{
	//if we are using a movie that was just streamed in, then lets fixup all our variants ect
	if ( m_MovieID != -1 && movieID != m_MovieID)
	{
		return;
	}

	Reset();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiButtonContext
//A Flash context interface to be used with UIButton
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void fuiButtonContext::Reset()
{
	fuiContext::Reset();
	if (m_pFlashObjectName && GetContext())
	{
		swfVARIANT* v = GetVariant(m_pFlashObjectName);
		if (v && v->Type == AS_OBJECT)
		{
			SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));
		}
	}
}

//setup all variants based off the given parent
void fuiButtonContext::SetFlashObject(swfSCRIPTOBJECT* pParent)
{
	m_pFlashObject = pParent;
	UIButton* pButton = verify_cast<UIButton*>(m_pOwner);
	for(u32 i=0;i<pButton->GetNumberOfChildren();i++)
	{
			UIComponent* pChild = verify_cast<UIComponent*>(pButton->GetChild(i));
			UIContext* pUIContext = pChild->GetContext();
			fuiContext* pContext = dynamic_cast<fuiContext*>(pUIContext);

		Assert(pContext);

		//use the same flash object for this button and the child icon and child label
		pContext->SetFlashObject(pParent);
	}
}

void fuiButtonContext::SetEnabled(bool b)				
{
	swfVARIANT* v=GetVariant(CONTEXT_BUTTON_STATUS);	

	if (v) 
		v->Set(b?CONTEXT_BUTTON_STATUS_ENABLED:CONTEXT_BUTTON_STATUS_DISABLED);	
}

void fuiButtonContext::SetBlinking(bool b)				
{
	swfVARIANT* v=GetVariant(CONTEXT_BUTTON_STATUS);	

	if (v && b) 
		v->Set(CONTEXT_BUTTON_STATUS_BLINKING);	
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiListContext
//A Flash context interface to be used with UIList
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//void fuiListContext::ProcessDataTag(char* name , char* value)
//{
//	if(stricmp(name,"numListItemsVariant")==0)
//	{
//		m_pFlashNumListItems = UIFACTORY->CreateString(value);
//	}
//	else if(stricmp(name,"numVisibleListItemsVariant")==0)
//	{
//		m_pFlashNumVisibleListItems = UIFACTORY->CreateString(value);
//	}
//	else if(stricmp(name,"currentSelectionVariant")==0)
//	{
//		m_pFlashCurrentSelection = UIFACTORY->CreateString(value);
//	}
//	else
//	{
//		fuiContext::ProcessDataTag(name,value);
//	}
//}

void fuiListContext::PostStream(char movieID)
{
	//if we are using a movie that was just streamed in, then lets fixup all our variants ect
	if ( m_MovieID != -1 && movieID != m_MovieID)
	{
		return;
	}

	Reset();
}

void fuiListContext::PostLoad()
{
	Reset();
}

void fuiListContext::Reset()
{
	fuiContext::Reset();
	
	if (GetContext())
	{
		if (m_pFlashObjectName)
		{
			swfVARIANT* v = GetVariant(m_pFlashObjectName);
			if (v && v->Type == AS_OBJECT)
			{
				SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));
			}
		}
		else
		{
			//default
			swfVARIANT* v = GetVariant(CONTEXT_LIST);
			if (v) SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));
		}
	}

	if (m_pFlashObject)
	{
		//setup all the list items variants 
		rage::swfVARIANT* rows = m_pFlashObject->AccessProperty(CONTEXT_LIST_CHILDREN);

		UIList* pList = verify_cast<UIList*>(m_pOwner);
		for(u32 i=0;i<pList->GetNumberOfChildren();i++)
		{
			UIComponent* pChild = verify_cast<UIComponent*>(pList->GetChild(i));
			UIContext* pUIContext = pChild->GetContext();
			fuiContext* pContext = dynamic_cast<fuiContext*>(pUIContext);
			Assert(pContext);

			swfVARIANT* row = &(*(rage::swfSCRIPTARRAY*)rows->asObject())[i];

			if (pList->GetOrientation() != UIList::HORIZONTAL)
			{
				//set cell 1 of each row as the flash object for now
				//setup all the list items variants 
				rage::swfVARIANT* cells = verify_cast<swfSCRIPTOBJECT*>(row->asObject())->AccessProperty(CONTEXT_LIST_CHILDREN);
				swfVARIANT* cell = &(*(rage::swfSCRIPTARRAY*)cells->asObject())[0];

				//hack for now.  this defaults the numVisibleItems for the first column to 1 (it each columns cell is a list then it will overwrite this)
				swfSCRIPTOBJECT* pCell = dynamic_cast<swfSCRIPTOBJECT*>(cell->asObject());
				rage::swfVARIANT* numVisibleChildren = pCell->AccessProperty(CONTEXT_LIST_NUMVISIBLECHILDREN);
				
				if (numVisibleChildren)
					numVisibleChildren->Set(1);

				pContext->Reset();
				pContext->SetFlashObject(pCell);
			}
			else
			{
				pContext->Reset();
				pContext->SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(row->asObject()));
			}
		}
	}
}

//setup all variants based off the given parent
void fuiListContext::SetFlashObject(swfSCRIPTOBJECT* pParent)
{
	m_pFlashObject = pParent;
	//m_pFlashNumListItems = m_pFlashObject->AccessProperty(atStringHash(CONTEXT_LIST_NUMCHILDREN),CONTEXT_LIST_CURRENTSELECTION);
	//m_pFlashNumVisibleListItems = m_pFlashObject->AccessProperty(atStringHash(CONTEXT_LIST_NUMVISIBLECHILDREN),CONTEXT_LIST_NUMVISIBLECHILDREN);
	//m_pFlashCurrentSelection = m_pFlashObject->AccessProperty(atStringHash(CONTEXT_LIST_CURRENTSELECTION),CONTEXT_LIST_CURRENTSELECTION);
}

void fuiListContext::ResetFlashObjectToDefaults()
{
	//lets clear the contexts values in preparation for population
	if (m_pFlashObject)
	{
		//unfortunately we must diverge from the cool OOD we have and just set all known possible objects that can go in this cell
		swfVARIANT* prop;
		prop = m_pFlashObject->AccessProperty(CONTEXT_LABEL_VALUE);
		if (prop) prop->Set(CONTEXT_LABEL_VALUE_DEFAULTVALUE);
		prop = m_pFlashObject->AccessProperty(CONTEXT_ICON_VALUE);
		if (prop) prop->Set(CONTEXT_ICON_VALUE_DEFAULTVALUE);
		prop = m_pFlashObject->AccessProperty(CONTEXT_COLOR);
		if (prop) prop->Set(CONTEXT_LABEL_COLOR_DEFAULTVALUE);
		prop = m_pFlashObject->AccessProperty(CONTEXT_LIST_NUMVISIBLECHILDREN);
		if (prop) prop->Set(CONTEXT_LIST_NUMVISIBLECHILDREN_DEFAULTVALUE);
		prop = m_pFlashObject->AccessProperty(CONTEXT_LIST_CURRENTSELECTION);
		if (prop) prop->Set(CONTEXT_LIST_CURRENTSELECTION_DEFAULTVALUE);
		prop = m_pFlashObject->AccessProperty(CONTEXT_LIST_CURRENTSELECTION);
		if (prop) prop->Set(CONTEXT_LIST_CURRENTSELECTION_DEFAULTVALUE);
	}
	else
	{
		UIDisplayf("This movie is not streamed/loaded or this button (%s) has no map to flash",m_pOwner->GetName());
	}
}

void fuiListContext::SetNumVisibleListItems(u32 n)	
{
	swfVARIANT* v = GetVariant(CONTEXT_LIST_NUMVISIBLECHILDREN);

	//notify flash to animate or whatever if the icon has changed
	if (v && v->asInt() != (int)n)
	{
		NotifyTransition(1);
		Refresh(1);
		v->Set((int)n);
	}
}

void fuiListContext::SetCurrentSelection(u32 n)		
{
	swfVARIANT* v = GetVariant(CONTEXT_LIST_CURRENTSELECTION);

	//notify flash to animate or whatever if the icon has changed
	if (v && v->asInt() != (int)n)
	{
		NotifyTransition(0);
		Refresh(1);
		v->Set((int)n);
	}
}

void fuiListContext::SetFocused(bool b)				
{
	swfVARIANT* v=GetVariant(CONTEXT_FOCUSED);	

	if (v) 
		v->Set(b?1:0);	
}

void fuiListContext::SetVisible(bool b)				
{
	swfVARIANT* v=GetVariant(CONTEXT_VISIBLE);	

	//notify flash to animate or whatever if the icon has changed
	NotifyTransition(1);
	Refresh(2);

	if (v) 
		v->Set(b?1:0);	
}

void fuiListContext::SetEnabled(bool b)				
{
	swfVARIANT* v=GetVariant(CONTEXT_LIST_ROWCOLOR);	

	if (v) 
		v->Set(b?CONTEXT_LIST_ENABLEDROWCOLOR:CONTEXT_LIST_DISABLEDROWCOLOR);	
}

void fuiListContext::OnScroll()
{
	NotifyTransition(0);
	Refresh(1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiSubListContext
//A Flash context interface to be used with UISubList
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void fuiSubListContext::Reset()
{
	fuiContext::Reset();	

	if (GetContext())
	{
		if (m_pFlashObjectName)
		{
			swfVARIANT* v = GetVariant(m_pFlashObjectName);
			if (v && v->Type == AS_OBJECT)
			{
				SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));
			}
		}
		else
		{
			//default
			swfVARIANT* v = GetVariant(CONTEXT_SUBLIST);
			if (v){	SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));}
		}
	}

	if (m_pFlashObject)
	{
		//setup all the list items variants 
		rage::swfVARIANT* rows = m_pFlashObject->AccessProperty(CONTEXT_LIST_CHILDREN);

		UIList* pList = verify_cast<UIList*>(m_pOwner);
		for(u32 i=0;i<pList->GetNumberOfChildren();i++)
		{
			UIComponent* pChild = verify_cast<UIComponent*>(pList->GetChild(i));
			UIContext* pUIContext = pChild->GetContext();
			fuiContext* pContext = dynamic_cast<fuiContext*>(pUIContext);
			Assert(pContext);

			swfVARIANT* row = &(*(rage::swfSCRIPTARRAY*)rows->asObject())[i];

			if (row)
			{
				pContext->Reset();
				pContext->SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(row->asObject()));
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//UIScrollBarContext
//A context interface to be used with UIScrollBar 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//void fuiScrollBarContext::ProcessDataTag(char* name , char* value)
//{
//	if(stricmp(name,"valueVariant")==0)
//	{
//		m_Value = UIFACTORY->CreateString(value);
//	}
//	else
//	{
//		fuiContext::ProcessDataTag(name,value);
//	}
//}

//setup all variants based off the given parent
void fuiScrollBarContext::SetFlashObject(swfSCRIPTOBJECT* pParent)
{
	m_pFlashObject = pParent;
	//m_Value = pParent->AccessProperty(atStringHash(CONTEXT_SCROLLBAR_VALUE),CONTEXT_SCROLLBAR_VALUE);
	//m_pIsVisible = pParent->AccessProperty(atStringHash(CONTEXT_SCROLLBAR_VISIBLE),CONTEXT_SCROLLBAR_VISIBLE);

}

void fuiScrollBarContext::PostLoad()
{
	Reset();
}

void fuiScrollBarContext::PostStream(char movieID)
{
	//if we are using a movie that was just streamed in, then lets fixup all our variants ect
	if ( m_MovieID != -1 && movieID != m_MovieID)
	{
		return;
	}

	Reset();
}

void fuiScrollBarContext::Reset()
{
	fuiContext::Reset();

	//these are global vars	
	if (GetContext())
	{
		if (m_pFlashObjectName)
		{
			swfVARIANT* v = GetVariant(m_pFlashObjectName);
			if (v && v->Type == AS_OBJECT)
			{
				SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));
			}
		}
		else
		{
			//default
			SetFlashObject(&GetContext()->Get_global());
		}
	}
	//m_pFlashObject = SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(GetVariant(CONTEXT_LIST)->asObject()));
}

void fuiScrollBarContext::ResetFlashObjectToDefaults()
{
	//lets clear the contexts values in preparation for population
	if (m_pFlashObject)
	{
		//unfortunately we must diverge from the cool OOD we have and just set all known possible objects that can go in this cell
		swfVARIANT* prop;
		prop = m_pFlashObject->AccessProperty(CONTEXT_SCROLLBAR_VALUE);
		if (prop) prop->Set(CONTEXT_SCROLLBAR_VALUE_DEFAULTVALUE);
		prop = m_pFlashObject->AccessProperty(atStringHash(CONTEXT_SCROLLBAR_VISIBLE),CONTEXT_SCROLLBAR_VISIBLE);
		if (prop) prop->Set(CONTEXT_SCROLLBAR_VISIBLE_DEFAULTVALUE);
	}
	else
	{
		UIDisplayf("This movie is not streamed/loaded!!");
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiTabbedContainerContext
//A flash context interface to be used with UITabbedContainer 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//void fuiTabbedContainerContext::ProcessDataTag(char* name , char* value)
//{
//	if(stricmp(name,"currentTabVariant")==0)
//	{
//		m_CurrentTab = UIFACTORY->CreateString(value);
//	}
//	else
//	{
//		fuiContext::ProcessDataTag(name,value);
//	}
//}

//setup all variants based off the given parent
void fuiTabbedContainerContext::SetFlashObject(swfSCRIPTOBJECT* pParent)
{
	m_pFlashObject = pParent;
	//m_pNumVisibleListItems = m_pFlashObject->AccessProperty(atStringHash(CONTEXT_TABS_NUMVISIBLECHILDREN),CONTEXT_TABS_NUMVISIBLECHILDREN);
	//m_CurrentTab = m_pFlashObject->AccessProperty(atStringHash(CONTEXT_TABS_CURRENTSELECTION),CONTEXT_TABS_CURRENTSELECTION);
}

void fuiTabbedContainerContext::PostLoad()
{
	Reset();
}

void fuiTabbedContainerContext::PostStream(char movieID)
{
	//if we are using a movie that was just streamed in, then lets fixup all our variants ect
	if ( m_MovieID != -1 && movieID != m_MovieID)
	{
		return;
	}

	Reset();
}

void fuiTabbedContainerContext::Reset()
{
	fuiContext::Reset();
	
	if (GetContext())
	{
		if (m_pFlashObjectName)
		{
			swfVARIANT* v = GetVariant(m_pFlashObjectName);
			if (v && v->Type == AS_OBJECT)
			{
				SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));
			}
		}
		else
		{
			//default
			swfVARIANT* v = GetVariant(CONTEXT_TABS);
			if (v){	SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));}
		}
	}

	//setup all the list items variants 
	if (m_pFlashObject)
	{
		rage::swfVARIANT* varTargetProperty = m_pFlashObject->AccessProperty(CONTEXT_TABS_CHILDREN);

		UIList* pList = verify_cast<UIList*>(m_pOwner);
		for(u32 i=0;i<pList->GetNumberOfChildren();i++)
		{
			UIComponent* pChild = verify_cast<UIComponent*>(pList->GetChild(i));
			UIContext* pUIContext = pChild->GetContext();
			fuiContext* pContext = dynamic_cast<fuiContext*>(pUIContext);
			Assert(pContext);

			swfVARIANT* v = &(*(rage::swfSCRIPTARRAY*)varTargetProperty->asObject())[i];
			pContext->Reset();
			pContext->SetFlashObject(dynamic_cast<swfSCRIPTOBJECT*>(v->asObject()));
		}
	}
}

void fuiTabbedContainerContext::OnTabChanged()
{
	NotifyTransition(0);
	Refresh(1);
}

void fuiTabbedContainerContext::ResetFlashObjectToDefaults()
{
	rage::swfVARIANT varTargetProperty;

	//lets clear the contexts values in preparation for population
	if (m_pFlashObject)
	{
		//unfortunately we must diverge from the cool OOD we have and just set all known possible objects that can go in this cell
		swfVARIANT* prop;
		prop = m_pFlashObject->AccessProperty(CONTEXT_TABS_NUMVISIBLECHILDREN);
		if (prop) prop->Set(CONTEXT_TABS_NUMVISIBLECHILDREN_DEFAULTVALUE);
		prop = m_pFlashObject->AccessProperty(CONTEXT_TABS_CURRENTSELECTION);
		if (prop) prop->Set(CONTEXT_TABS_CURRENTSELECTION_DEFAULTVALUE);
	}
	else
	{
		UIDisplayf("This movie is not streamed/loaded!!");
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiTabContext
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//setup all variants based off the given parent
void fuiTabContext::SetFlashObject(swfSCRIPTOBJECT* pParent)
{
	m_pFlashObject = pParent;
	UIButton* pButton = verify_cast<UIButton*>(m_pOwner);
	for(u32 i=0;i<pButton->GetNumberOfChildren();i++)
	{
		fuiContext* pContext = dynamic_cast<fuiContext*>(dynamic_cast<UIComponent*>(pButton->GetChild(i))->GetContext());
		Assert(pContext);

		//use the same flash object for this button and the child icon and child label
		pContext->SetFlashObject(pParent);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiTickerContext
//A flash context interface to be used with UITicker 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void fuiTickerContext::SetFlashObject(swfSCRIPTOBJECT* pParent)
{
	m_pFlashObject = pParent;
}

void fuiTickerContext::PostStream(char movieID)
{
	//if we are using a movie that was just streamed in, then lets fixup all our variants ect
	if ( m_MovieID != -1 && movieID != m_MovieID)
	{
		return;
	}

	Reset();
}

void fuiTickerContext::Reset()
{
	if (m_pFlashObjectName && GetContext())
	{
		swfVARIANT* v = GetVariant(m_pFlashObjectName);
		if (v && v->Type == AS_OBJECT)
		{
			SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));
		}
	}

	m_MovieID = FLASHMANAGER->GetMovieID("mp_HUD");

	fuiContext::Reset();
}

void fuiTickerContext::OnHide()
{
	fuiContext::OnHide();

	SetVisible(false);
}

void fuiTickerContext::OnShow()
{
	fuiContext::OnShow();

	SetVisible(true);	
}

void fuiTickerContext::SetVisible(bool value)
{ 
	swfVARIANT* v = GetVariant("cur_visibility");

	//notify flash to animate or whatever if the icon has changed
	if (v && v->asBool() != value)
	{
		NotifyTransition(1);
		Refresh(2);
		v->Set(value);
	}
}

swfVARIANT* fuiTickerContext::GetItemProperty(int index, const char *propName)
{
	swfVARIANT*	pVar = GetArrayVariant(CONTEXT_TICKER_ITEM, index);

	if (pVar)
	{
		swfSCRIPTOBJECT* pObj = (swfSCRIPTOBJECT*)pVar->asObject();
		return pObj->AccessProperty(propName);
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiProgressBarContext
//A flash context interface to be used with UIProgressBar 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//void fuiProgressBarContext::ProcessDataTag(char* name , char* value)
//{
//	if(stricmp(name,"minimumVariant")==0)
//	{
//		m_Minimum = UIFACTORY->CreateString(value);
//	}
//	else if(stricmp(name,"maximumVariant")==0)
//	{
//		m_Maximum = UIFACTORY->CreateString(value);
//	}
//	else if(stricmp(name,"valueVariant")==0)
//	{
//		m_Value = UIFACTORY->CreateString(value);
//	}
//	else if(stricmp(name,"indeterminateVariant")==0)
//	{
//		m_bIsIndeterminate = UIFACTORY->CreateString(value);
//	}
//	else if(stricmp(name,"textVariant")==0)
//	{
//		m_bProgressString = UIFACTORY->CreateString(value);
//	}
//	else
//	{
//		fuiContext::ProcessDataTag(name,value);
//	}
//}

//setup all variants based off the given parent
void fuiProgressBarContext::SetFlashObject(swfSCRIPTOBJECT* pParent)
{
	m_pFlashObject = pParent;
	//m_Value = pParent->AccessProperty(atStringHash(CONTEXT_PROGRESSBAR_VALUE),CONTEXT_PROGRESSBAR_VALUE);
	//m_Maximum = pParent->AccessProperty(atStringHash(CONTEXT_PROGRESSBAR_MAX),CONTEXT_PROGRESSBAR_MAX);
	//m_Minimum = pParent->AccessProperty(atStringHash(CONTEXT_PROGRESSBAR_MIN),CONTEXT_PROGRESSBAR_MIN);
	//m_bProgressString = pParent->AccessProperty(atStringHash(CONTEXT_PROGRESSBAR_TEXT),CONTEXT_PROGRESSBAR_TEXT);
	//m_bIsIndeterminate = pParent->AccessProperty(atStringHash(CONTEXT_PROGRESSBAR_INDETERMINATE),CONTEXT_PROGRESSBAR_INDETERMINATE);
}

void fuiProgressBarContext::PostStream(char movieID)
{
	//if we are using a movie that was just streamed in, then lets fixup all our variants ect
	if ( m_MovieID != -1 && movieID != m_MovieID)
	{
		return;
	}

	Reset();
}

void fuiProgressBarContext::Reset()
{
	if (m_pFlashObjectName && GetContext())
	{
		swfVARIANT* v = GetVariant(m_pFlashObjectName);
		if (v && v->Type == AS_OBJECT)
		{
			SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));
		}
	}

	m_MovieID = FLASHMANAGER->GetMovieID("popup");

	fuiContext::Reset();
}

void fuiProgressBarContext::OnHide()
{
	fuiContext::OnHide();

	SetVisible(false);//next popup in the stack should show itself
}

void fuiProgressBarContext::OnShow()
{
	fuiContext::OnShow();

	SetVisible(true);	
}

void fuiProgressBarContext::SetVisible(bool value)
{ 
	swfVARIANT* v = GetVariant(CONTEXT_PROGRESSBAR_INDETERMINATE); 

	//notify flash to animate or whatever if the icon has changed
	if (v && v->asBool() != value)
	{
		NotifyTransition(1);
		Refresh(1);
		v->Set(value);
	}
}

//void fuiProgressBarContext::ResetFlashObjectToDefaults()
//{
//	fuiListContext::ResetFlashObjectToDefaults();
//
//	//lets clear the contexts values in preparation for population
//	if (m_pFlashObject)
//	{
//		//unfortunately we must diverge from the cool OOD we have and just set all known possible objects that can go in this cell
//	}
//	else
//	{
//		UIDisplayf("This movie is not streamed/loaded!!");
//	}
//}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiSpinnerContext
//A flash context interface to be used with UISpinner 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//void fuiSpinnerContext::ProcessDataTag(char* name , char* value)
//{
//	if(stricmp(name,"minimumVariant")==0)
//	{
//		m_Minimum = UIFACTORY->CreateString(value);
//	}
//	else if(stricmp(name,"maximumVariant")==0)
//	{
//		m_Maximum = UIFACTORY->CreateString(value);
//	}
//	else if(stricmp(name,"valueVariant")==0)
//	{
//		m_Value = UIFACTORY->CreateString(value);
//	}
//	else
//	{
//		fuiContext::ProcessDataTag(name,value);
//	}
//}

//setup all variants based off the given parent
void fuiSpinnerContext::SetFlashObject(swfSCRIPTOBJECT* pParent)
{
	m_pFlashObject = pParent;
	//m_Value = pParent->AccessProperty(atStringHash(CONTEXT_SPINNER_VALUE),CONTEXT_SPINNER_VALUE);
	//m_Maximum = pParent->AccessProperty(atStringHash(CONTEXT_SPINNER_MAX),CONTEXT_SPINNER_MAX);
	//m_Minimum = pParent->AccessProperty(atStringHash(CONTEXT_SPINNER_MIN),CONTEXT_SPINNER_MIN);
}

void fuiSpinnerContext::PostStream(char movieID)
{
	//if (!m_pFlashObject)
	//{
	//	m_pFlashObject = m_pContext->GetVariant(CONTEXT_SCROLLBAR_VALUE);
	//}
	
	//if we are using a movie that was just streamed in, then lets fixup all our variants ect
	if ( m_MovieID != -1 && movieID != m_MovieID)
	{
		return;
	}

	Reset();
}

void fuiMessageBoxContext::PostLoad()
{
	Reset();
}

void fuiMessageBoxContext::PostStream(char movieID)
{	
	//if we are using a movie that was just streamed in, then lets fixup all our variants ect
	if ( m_MovieID != -1 && movieID != m_MovieID)
	{
		return;
	}

	Reset();
}

void fuiMessageBoxContext::Reset()
{
	m_MovieID = FLASHMANAGER->GetMovieID("popup");

	fuiContext::Reset();	

	if (GetContext())
	{
		if (m_pFlashObjectName)
		{
			swfVARIANT* v = GetVariant(m_pFlashObjectName);
			if (v && v->Type == AS_OBJECT)
			{
				SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));
			}
		}
		else
		{
			//default
			swfVARIANT* v = GetVariant(CONTEXT_MESSAGEBOX_OPTIONSLIST);
			if (v){	SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));}
		}
	}

	if (m_pFlashObject)
	{		
		//setup all the list items variants 
		rage::swfVARIANT* rows = m_pFlashObject->AccessProperty(CONTEXT_MESSAGEBOX_CHILDREN);

		UIMessageBox* pList = verify_cast<UIMessageBox*>(m_pOwner);
		for(u32 i=0;i<pList->GetNumberOfChildren();i++)
		{
			UIComponent* pChild = verify_cast<UIComponent*>(pList->GetChild(i));
			UIContext* pUIContext = pChild->GetContext();
			fuiContext* pContext = dynamic_cast<fuiContext*>(pUIContext);
			Assert(pContext);

			swfVARIANT* row = &(*(rage::swfSCRIPTARRAY*)rows->asObject())[i];

			//if (pList->GetOrientation() == UIList::HORIZONTAL)
			//{
			//	pContext->SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(row->asObject()));
			//}
			//else
			{
				//set cell 1 of each row as the flash object for now
				//setup all the list items variants 
				rage::swfVARIANT* cells = verify_cast<swfSCRIPTOBJECT*>(row->asObject())->AccessProperty(CONTEXT_MESSAGEBOX_CHILDREN);
				swfVARIANT* cell = &(*(rage::swfSCRIPTARRAY*)cells->asObject())[0];
				pContext->Reset();
				pContext->SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(cell->asObject()));
			}
		}
	}

	ResetFlashObjectToDefaults();
}

void fuiMessageBoxContext::SetFlashObject(swfSCRIPTOBJECT* pParent)
{
	m_pFlashObject = pParent;
}

void fuiMessageBoxContext::ResetFlashObjectToDefaults()
{
	rage::swfVARIANT varTargetProperty;
	
	//lets clear the contexts values in preparation for population
	if (m_pFlashObject)
	{
		//unfortunately we must diverge from the cool OOD we have and just set all known possible objects that can go in this cell
		swfVARIANT* prop;
		prop = m_pFlashObject->AccessProperty(CONTEXT_MESSAGEBOX_TITLE);
		if (prop) prop->Set(CONTEXT_MESSAGEBOX_TITLE_DEFAULTVALUE);
		prop = m_pFlashObject->AccessProperty(CONTEXT_MESSAGEBOX_DESCRIPTION);
		if (prop) prop->Set(CONTEXT_MESSAGEBOX_DESCRIPTION_DEFAULTVALUE);
		prop = m_pFlashObject->AccessProperty(CONTEXT_MESSAGEBOX_ISVISIBLE);
		if (prop) prop->Set(0);
	}
	else
	{
		UIDisplayf("This movie is not streamed/loaded!!");
	}
}

void fuiMessageBoxContext::OnHide()
{
	fuiListContext::OnHide();

	SetVisible(false);//next popup in the stack should show itself
	NotifyTransition(1);
}

void fuiMessageBoxContext::OnShow()
{
	fuiListContext::OnShow();
	SetVisible(true);
	Refresh(2);
	NotifyTransition(1);
}

void fuiMessageBoxContext::SetVisible(bool value)
{ 
	swfVARIANT* v = GetVariant(CONTEXT_MESSAGEBOX_ISVISIBLE); 

	//notify flash to animate or whatever if the icon has changed
	if (v && v->asBool() != value)
	{
		v->Set(value);
	}
}

//pleace do not change the case # or UIComponent enums. If we use a new system, then only change the defines
void fuiMessageBoxContext::SetStyle(int style)
{
	switch (style)
	{
	case 0:SetTemplate(CONTEXT_MESSAGEBOX_TEMPLATE_DEFAULT);
		break;
	case 1:SetTemplate(CONTEXT_MESSAGEBOX_TEMPLATE_SIMPLE);
		break;
	default:SetTemplate(CONTEXT_MESSAGEBOX_TEMPLATE_DEFAULT);
		break;
	}
}

void fuiPromptStripContext::PostLoad()
{
	fuiLayerContext::PostLoad();
	Reset();
}

void fuiPromptStripContext::OnHide()
{
	fuiLayerContext::OnHide();

	SetVisible(false);//next popup in the stack should show itself
}

void fuiPromptStripContext::OnShow()
{
	fuiLayerContext::OnShow();

	SetVisible(true);
}

void fuiPromptStripContext::PostStream(char movieID)
{
	//if we are using a movie that was just streamed in, then lets fixup all our variants ect
	if ( m_MovieID != -1 && movieID != m_MovieID)
	{
		return;
	}

	fuiLayerContext::PostStream(movieID);

	Reset();
}

void fuiPromptStripContext::Reset()
{
	fuiLayerContext::Reset();

	m_MovieID = FLASHMANAGER->GetMovieID("popup");

	//if (m_Style == CONTEXT_PROMPTSTRIP_TEMPLATE_MESSAGEBOX)
	{
		if (GetContext())
		{
			if (m_pFlashObjectName)
			{
				swfVARIANT* v = GetVariant(m_pFlashObjectName);
				if (v && v->Type == AS_OBJECT)
				{
					SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));
				}
			}
			else
			{
				//default
				swfVARIANT* v = GetVariant(CONTEXT_PROMPTSTRIP);
				if (v){	SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(v->asObject()));}
			}

			if (m_pFlashObject)
			{
				//setup all the list items variants 
				rage::swfVARIANT* promptStrip = m_pFlashObject->AccessProperty(CONTEXT_PROMPTSTRIP_CHILDREN);

				UIPromptStrip* pList = verify_cast<UIPromptStrip*>(m_pOwner);
				for(u32 i=0;i<pList->GetNumberOfChildren();++i)//icon and text are in their own cell
				{
					UIComponent* pChild = verify_cast<UIComponent*>(pList->GetChild(i));
					UIContext* pUIContext = pChild->GetContext();
					fuiContext* pContext = dynamic_cast<fuiContext*>(pUIContext);
					Assert(pContext);

					swfVARIANT* prompt = &(*(rage::swfSCRIPTARRAY*)promptStrip->asObject())[i];
					pContext->SetFlashObject(verify_cast<swfSCRIPTOBJECT*>(prompt->asObject()));
				}
			}
		}
	}

	ResetFlashObjectToDefaults();
}

//setup all variants based off the given parent
void fuiPromptStripContext::SetFlashObject(swfSCRIPTOBJECT* pParent)
{
	fuiLayerContext::SetFlashObject(pParent);
	m_pFlashObject = pParent;
}


void fuiPromptStripContext::ResetFlashObjectToDefaults()
{
	fuiLayerContext::ResetFlashObjectToDefaults();

	rage::swfVARIANT varTargetProperty;

	//lets clear the contexts values in preparation for population
	if (m_pFlashObject)
	{
		//unfortunately we must diverge from the cool OOD we have and just set all known possible objects that can go in this cell
		swfVARIANT* prop;
		prop = m_pFlashObject->AccessProperty(CONTEXT_PROMPTSTRIP_NUMPROMPTS);
		if (prop) prop->Set(CONTEXT_PROMPTSTRIP_NUMPROMPTS_DEFAULTVALUE);
		prop = m_pFlashObject->AccessProperty(CONTEXT_PROMPTSTRIP_ISVISIBLE);
		if (prop) prop->Set(0);

	}
	else
	{
		UIDisplayf("This movie is not streamed/loaded!!");
	}
}

void fuiPromptStripContext::SetVisible(bool value)
{ 
	swfVARIANT* v = GetVariant(CONTEXT_PROMPTSTRIP_ISVISIBLE); 

	//notify flash to animate or whatever if the icon has changed
	if (v && v->asBool() != value)
	{
		NotifyTransition(1);
		Refresh(1);
		v->Set(value);
	}
}


}//end namespace rage

