// 
// flashUIComponents/FlashUIContexts.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#ifndef FLASHUICONTEXTS_H
#define FLASHUICONTEXTS_H

#include "flash/misc.h"
#include "fuicomponents/UIObject.h"
#include "fuicomponents/Components.h"
#include "fuicomponents/UIContexts.h"

namespace rage{
class swfBase;
class swfContext;
class swfSCRIPTOBJECT;
struct swfVARIANT;
class fuiMovie;

#if !__PPU
#pragma warning(push)
#pragma warning(disable:4250)//disable the informative warning 'class1' : inherits 'class2::member' via dominance
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiContext
//An interface to an external tool that artists use to represent/decorate these UI components.
//Each component should have a corresponding UIContext.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CONTEXT_VISIBLE "visible"
#define CONTEXT_FOCUSED "focus"
#define CONTEXT_ENABLED "enabled"
#define CONTEXT_ACTIVE "active"
#define CONTEXT_COLOR "color"

class fuiContext : public virtual UIContext
{
public:
	fuiContext() : UIContext(),m_MovieID(-1),m_pFlashObject(NULL),m_pFlashObjectName(NULL){}
	virtual ~fuiContext(){}

	// PURPOSE:	Sets/Gets the context that is used to interface an external tool.
	//			The external tool is used to "decorate" the UI.  By default FLASH
	//			is used as the external tool.
	virtual swfCONTEXT* GetContext();
	virtual fuiMovie* GetMovie();
	virtual void SetMovieID(char index){m_MovieID = index;}	

	virtual void SetVisible(bool /*b*/){/*GetVariant(CONTEXT_VISIBLE)->Set(b);*/}
	virtual void SetEnabled(bool /*b*/){/*GetVariant(CONTEXT_ENABLED)->Set(b);*/}
	virtual void SetFocused(bool /*b*/){/*GetVariant(CONTEXT_FOCUSED)->Set(b);*/}
	virtual void SetActive(bool /*b*/){/*GetVariant(CONTEXT_ACTIVE)->Set(b);*/}
	virtual void SetColor(u32 color){swfVARIANT* v = GetVariant(CONTEXT_COLOR); if (v ){v->Set((int)color);}}
	virtual void NotifyTransition();
	virtual void NotifyTransition(int level);
	virtual void OnActivate();
	virtual void OnDeactivate();
	virtual void OnShow();
	virtual void OnHide();
	virtual void Refresh(int level);
	virtual void SetTemplate(int);
	virtual void PostLoad(){Reset();}
	virtual void SetFlashObject(swfSCRIPTOBJECT*);			
	virtual void ResetFlashObjectToDefaults(){}
	virtual void SetStyle(int ){}
	virtual void Reset();	
	swfVARIANT* GetVariant(const char* value);
	swfVARIANT* GetVariant(const char* value ,swfVARIANT* property);
	swfVARIANT* GetVariantSimple(const char* value,swfSCRIPTOBJECT* property);
	swfVARIANT* GetArrayVariant(const char *name, int index);
	swfVARIANT* GetArrayItemProperty(const char *array, int index, const char* property);

	struct HashedString
	{
		const char* String;
		u32 Hash;
	};

protected:	
	virtual void ProcessDataTag(const char* name , const char* value);

	char m_MovieID;
	swfSCRIPTOBJECT* m_pFlashObject;//helps in mapping from OOD to the union style struct in flash
	const char* m_pFlashObjectName;//useful for mapping directly to custom flash variables.  but adds more bloat to fui
	//const char* m_pIsVisible;
	//const char* m_pIsFocused;
	//const char* m_pIsActive;
	//const char* m_pIsEnabled;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiLayerContext
//A Flash context interface to be used with UILayer 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class fuiLayerContext : public fuiContext,public UILayerContext
{
public:
	fuiLayerContext() : fuiContext(),UILayerContext(){}
	fuiLayerContext(UILayer* pLayer) : fuiContext(),UILayerContext()
	{
		SetOwner(pLayer);
	}
	virtual ~fuiLayerContext(){}

	//virtual void ProcessDataTag(char*  name , char* value){fuiContext::ProcessDataTag(name,value);}
	virtual void SetFlashObject(swfSCRIPTOBJECT*){}
	virtual void PostStream(char movieID);
	virtual void PostLoad(){Reset();}
	virtual void OnShow();
	virtual void OnHide();
	virtual void Reset();
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiLayerContext
//A Flash context interface to be used with UILayer 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//class fuiPanelContext : public fuiContext,public UIPanelContext
//{
//	virtual void Reset();
//};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiSceneContext
//A Flash context interface to be used with UIScrollBar 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class fuiSceneContext : public fuiLayerContext,public UISceneContext
{
public:
	fuiSceneContext(UIScene* pScene) : fuiLayerContext(),UISceneContext()
	{
		SetOwner(pScene);
	}
	virtual ~fuiSceneContext(){}

	//virtual void ProcessDataTag(char* name , char* value){fuiLayerContext::ProcessDataTag(name,value);}
	virtual void SetFlashObject(swfSCRIPTOBJECT*){}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiListContext
//A Flash context interface to be used with UIList
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CONTEXT_LIST "menu"
#define CONTEXT_LIST_CHILDREN "elements"
#define CONTEXT_LIST_NUMVISIBLECHILDREN "numVisibleListItems"
#define CONTEXT_LIST_NUMVISIBLECHILDREN_DEFAULTVALUE 1
#define CONTEXT_LIST_CURRENTSELECTION "currentSelection"
#define CONTEXT_LIST_DISABLEDROWCOLOR 0
#define CONTEXT_LIST_ENABLEDROWCOLOR -1
#define CONTEXT_LIST_ROWCOLOR "color"
#define CONTEXT_LIST_CURRENTSELECTION_DEFAULTVALUE 0

class fuiListContext : public virtual fuiContext, public virtual UIListContext
{
public:
	fuiListContext(UIList* pList) : fuiContext(),UIListContext()//,m_pFlashNumListItems(NULL),m_pFlashNumVisibleListItems(NULL),m_pFlashCurrentSelection(NULL)
	{
		SetOwner(pList);
	}
	virtual ~fuiListContext(){}

	//virtual void SetNumListItems(u32 n)	{GetVariant(CONTEXT_LIST_NUMCHILDREN)->Set((int)n);}
	virtual void SetNumVisibleListItems(u32 n);
	virtual void SetCurrentSelection(u32 n);
	virtual void SetFocused(bool b);
	virtual void SetVisible(bool b);
	virtual void SetEnabled(bool b);
	virtual void ResetFlashObjectToDefaults();
	virtual void PostStream(char movieID);
	virtual void PostLoad();
	virtual void SetFlashObject(swfSCRIPTOBJECT*);
	virtual void Reset();
	virtual void OnScroll();

protected:
	//const char* m_pFlashNumListItems;
	//const char* m_pFlashNumVisibleListItems;
	//const char* m_pFlashCurrentSelection;
};

class fuiVerticalListContext : public fuiListContext
{
public:
	fuiVerticalListContext(UIList* pList) : fuiListContext(pList)
	{
		SetOwner(pList);
	}
	virtual ~fuiVerticalListContext(){}

	virtual void PostStream(char movieID);

};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiSubListContext
//A Flash context interface to be used with UIList
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CONTEXT_SUBLIST CONTEXT_LIST".submenu"

class fuiSubListContext : public virtual fuiListContext
{
public:
	fuiSubListContext(UIList* pList) : fuiListContext(pList)
	{}
	virtual ~fuiSubListContext(){}

	//virtual void SetNumListItems(u32 n)	{GetVariant(CONTEXT_LIST_NUMCHILDREN)->Set((int)n);}
	virtual void Reset();

protected:
	//const char* m_pFlashNumListItems;
	//const char* m_pFlashNumVisibleListItems;
	//const char* m_pFlashCurrentSelection;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiScrollBarContext
//A Flash context interface to be used with UIScrollBar 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CONTEXT_SCROLLBAR_VALUE "ScrollbarSliderPosition"
#define CONTEXT_SCROLLBAR_VALUE_DEFAULTVALUE 0
#define CONTEXT_SCROLLBAR_VISIBLE "ScrollbarVisible"
#define CONTEXT_SCROLLBAR_VISIBLE_DEFAULTVALUE 0
class fuiScrollBarContext : public fuiContext,public UIScrollBarContext
{
public:
	fuiScrollBarContext(UIScrollBar* pScrollBar) : fuiContext(),UIScrollBarContext()//,m_Value(NULL)
	{
		SetOwner(pScrollBar);
	}
	virtual ~fuiScrollBarContext(){}

	// PURPOSE: Sets the progress bar's current value to n.
	virtual void SetValue(f32 n){swfVARIANT* v = GetVariant(CONTEXT_SCROLLBAR_VALUE); if (v ){v->Set(n);}}
	virtual void SetVisible(bool b){swfVARIANT* v = GetVariant(CONTEXT_SCROLLBAR_VISIBLE); if (v ){v->Set(b);}}

	virtual void ResetFlashObjectToDefaults();
	virtual void SetFlashObject(swfSCRIPTOBJECT*);
	virtual void PostStream(char movieID);
	virtual void PostLoad();
	virtual void Reset();

protected:
	//const char* m_Value;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiTabbedContainerContext
//A flash context interface to be used with UITabbedContainer 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CONTEXT_TABS "tabs"
#define CONTEXT_TABS_CHILDREN "elements"
#define CONTEXT_TABS_NUMVISIBLECHILDREN "count"
#define CONTEXT_TABS_NUMVISIBLECHILDREN_DEFAULTVALUE 0
#define CONTEXT_TABS_CURRENTSELECTION "currentTab"
#define CONTEXT_TABS_CURRENTSELECTION_DEFAULTVALUE 0

class fuiTabbedContainerContext : public fuiContext,public UITabbedContainerContext
{
public:
	fuiTabbedContainerContext(UITabbedContainer* p) : fuiContext(),UITabbedContainerContext()//,m_CurrentTab(NULL)
	{
		SetOwner(p);
	}
	~fuiTabbedContainerContext(){}

	// PURPOSE: Sets the selected index for this UITabbedContainer.
	virtual void SetCurrentTab(u32 n){swfVARIANT* v = GetVariant(CONTEXT_TABS_CURRENTSELECTION); if (v ){v->Set((int)n);}}
	virtual void SetNumVisibleTabs(u32 n){swfVARIANT* v = GetVariant(CONTEXT_TABS_NUMVISIBLECHILDREN); if (v ){v->Set((int)n);}}
	virtual void ResetFlashObjectToDefaults();
	virtual void PostStream(char movieID);
	virtual void PostLoad();
	virtual void SetFlashObject(swfSCRIPTOBJECT*);
	virtual void Reset();
	virtual void OnTabChanged();

protected:
	//const char* m_CurrentTab;
	//const char* m_pNumVisibleListItems;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiTabContext
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class fuiTabContext : public fuiContext,public UITabContext
{
public:
	fuiTabContext(UITab* pTab) : fuiContext(),UITabContext()
	{
		SetOwner(pTab);
	}

	virtual ~fuiTabContext(){}

	virtual void SetFlashObject(swfSCRIPTOBJECT*);
	virtual void PostLoad(){Reset();}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiLabelContext
//A Flash context interface to be used with UITabbedContainer 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CONTEXT_LABEL_VALUE "text"
#define CONTEXT_LABEL_VALUE_DEFAULTVALUE ""
#define CONTEXT_LABEL_COLOR_DEFAULTVALUE -1
class fuiLabelContext : public fuiContext,public UILabelContext
{
public:
	fuiLabelContext(UILabel* pLabel) : fuiContext(),UILabelContext()//,m_pText(NULL),m_pColor(NULL)
	{
		SetOwner(pLabel);
	}

	virtual ~fuiLabelContext(){}

	// PURPOSE: Defines the single line of text this component will display.
	void SetText(const char* stringID);
	void SetText(u32 stringIDHash){GetVariant(CONTEXT_LABEL_VALUE)->Set(UISTRINGTABLE->GetStringUTF8(stringIDHash));}

	virtual void ResetFlashObjectToDefaults();
	virtual void SetFlashObject(swfSCRIPTOBJECT*);
	virtual void PostStream(char movieID);
	virtual void PostLoad(){Reset();}
	virtual void Reset();
protected:
	//const char* m_pText;
	//const char* m_pColor;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiIconContext
//A Flash context interface to be used with UIIcon 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CONTEXT_ICON_VALUE "iconType"
#define CONTEXT_ICON_VALUE_DEFAULTVALUE 0
class fuiIconContext : public fuiContext,public UIIconContext
{
public:
	fuiIconContext(UIIcon* pIcon) : fuiContext(),UIIconContext()//,m_pIconType(NULL)
	{
		SetOwner(pIcon);
	}

	virtual ~fuiIconContext(){}

	// PURPOSE: Defines the single line of text this component will display.
	virtual void SetIcon(UIIcon::IconType type);
	virtual void SetIcon(UIPromptStrip::PromptIcons type);

	virtual void ResetFlashObjectToDefaults();
	virtual void SetFlashObject(swfSCRIPTOBJECT*);
	virtual void PostStream(char );
	virtual void PostLoad(){Reset();}
	virtual void Reset();

protected:
	//virtual void ProcessDataTag(char* name , char* value);
	//const char* m_pIconType;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiButtonContext
//A Flash context interface to be used with UIButton
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CONTEXT_BUTTON_STATUS "type"
#define CONTEXT_BUTTON_STATUS_ENABLED 0
#define CONTEXT_BUTTON_STATUS_DISABLED -1
#define CONTEXT_BUTTON_STATUS_BLINKING 1
class fuiButtonContext : public fuiContext,public UIButtonContext
{
public:
	fuiButtonContext(UIButton* pIcon) : fuiContext(),UIButtonContext()
	{
		SetOwner(pIcon);
	}

	virtual ~fuiButtonContext(){}

	virtual void SetFlashObject(swfSCRIPTOBJECT*);
	virtual void PostLoad(){Reset();}
	virtual void SetEnabled(bool b);
	virtual void SetBlinking(bool b);
	virtual void Reset();
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiTickerContext
//A flash context interface to be used with UITicker 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CONTEXT_TICKER_ITEM			"ticker"
#define CONTEXT_TICKER_ITEM_VALUE	"text"
#define CONTEXT_TICKER_ITEM_POS		"y"
#define CONTEXT_TICKER_ITEM_ALPHA	"alpha"
#define CONTEXT_TICKER_NUMITEMS		"ticker_count"
class fuiTickerContext : public fuiContext,public UITickerContext
{
public:
	fuiTickerContext(UITicker* p) : fuiContext(),UITickerContext()
	{
		SetOwner(p);
	}
	virtual ~fuiTickerContext(){}

	virtual void SetVisible(bool value);
	virtual void OnHide();
	virtual void OnShow();
	virtual void SetFlashObject(swfSCRIPTOBJECT*);
	virtual void PostStream(char );
	virtual void PostLoad(){Reset();}
	virtual void Reset();

	// PURPOSE: Gets the Item
	swfVARIANT* GetItemProperty(int i, const char* propName);

	// PURPOSE: Sets the number of items to display
	virtual void SetMaxItems(f32 n)					{swfVARIANT* v = GetVariant(CONTEXT_TICKER_NUMITEMS);			if(v){v->Set(n);} }

	// PURPOSE: Sets the value of an item in the ticker
	virtual void SetItemValue(int i, const char *n)	{swfVARIANT* v = GetItemProperty(i,CONTEXT_TICKER_ITEM_VALUE);	if(v){v->Set(n);} }

	// PURPOSE: Sets the visible y position of an item in the list
	virtual void SetItemPosition(int i, int y)		{swfVARIANT* v = GetItemProperty(i,CONTEXT_TICKER_ITEM_POS);	if(v){v->Set(y);} }
																											
	// PURPOSE: Sets the alpha on an item in the list														
	virtual void SetItemAlpha(int i, int n)			{swfVARIANT* v = GetItemProperty(i,CONTEXT_TICKER_ITEM_ALPHA);	if(v){v->Set(n);} }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiProgressBarContext
//A flash context interface to be used with UIProgressBar 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CONTEXT_PROGRESSBAR_VALUE ""
#define CONTEXT_PROGRESSBAR_MAX ""
#define CONTEXT_PROGRESSBAR_MIN ""
#define CONTEXT_PROGRESSBAR_TEXT "loadbar_text"
#define CONTEXT_PROGRESSBAR_INDETERMINATE "showLoadBar"
#define CONTEXT_PROGRESSBAR_ISVISIBLE "visibility"
class fuiProgressBarContext : public fuiContext,public UIProgressBarContext
{
public:
	fuiProgressBarContext(UIProgressBar* p) : fuiContext(),UIProgressBarContext()//,m_Minimum(NULL),m_Maximum(NULL),m_Value(NULL),m_bIsIndeterminate(NULL),m_bProgressString(NULL)
	{
		SetOwner(p);
	}
	virtual ~fuiProgressBarContext(){}

	// PURPOSE: Sets the indeterminate property of the progress bar, which determines 
	//			whether the progress bar is in determinate or indeterminate mode.	
	virtual void SetIndeterminate(bool newValue){swfVARIANT* v = GetVariant(CONTEXT_PROGRESSBAR_INDETERMINATE); if (v ){v->Set((int)newValue);}}

	// PURPOSE: Sets the progress bar's maximum value to n.
	virtual void SetMaximum(f32 n){swfVARIANT* v = GetVariant(CONTEXT_PROGRESSBAR_MAX); if (v ){v->Set(n);}}

	// PURPOSE: Sets the progress bar's minimum value to n.
	virtual void SetMinimum(f32 n){swfVARIANT* v = GetVariant(CONTEXT_PROGRESSBAR_MIN); if (v ){v->Set(n);}}

	// PURPOSE: Sets the value of the progress string.
	virtual void SetString(const char* s){swfVARIANT* v = GetVariant(CONTEXT_PROGRESSBAR_TEXT); if (v ){v->Set(s);}}

	// PURPOSE: Sets the progress bar's current value to n.
	virtual void SetValue(f32 n){swfVARIANT* v = GetVariant(CONTEXT_PROGRESSBAR_VALUE); if (v ){v->Set(n);}}

	virtual void SetVisible(bool value);
	virtual void OnHide();
	virtual void OnShow();
	virtual void SetFlashObject(swfSCRIPTOBJECT*);
	virtual void PostStream(char );
	virtual void PostLoad(){Reset();}
	virtual void Reset();

protected:
	//const char* m_Minimum;
	//const char* m_Maximum;
	//const char* m_Value;
	//const char* m_bIsIndeterminate;
	//const char* m_bProgressString;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiSpinnerContext
//A flash context interface to be used with UISpinner 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CONTEXT_SPINNER_VALUE ""
#define CONTEXT_SPINNER_MAX ""
#define CONTEXT_SPINNER_MIN ""
#define CONTEXT_SPINNER_TEXT ""
class fuiSpinnerContext : public fuiContext,public UISpinnerContext
{
public:
	fuiSpinnerContext(UISpinner* p) : fuiContext(),UISpinnerContext()//,m_Minimum(NULL),m_Maximum(NULL),m_Value(NULL),m_Increment(NULL)
	{
		SetOwner(p);
	}
	virtual ~fuiSpinnerContext(){}

	virtual void SetMinimum(f32 value) { swfVARIANT* v = GetVariant(CONTEXT_SPINNER_MIN); if (v ){v->Set(value);} }
	virtual void SetMaximum(f32 value) { swfVARIANT* v = GetVariant(CONTEXT_SPINNER_MAX); if (v ){v->Set(value);} }
	virtual void SetValue(f32 value) { swfVARIANT* v = GetVariant(CONTEXT_SPINNER_VALUE); if (v ){v->Set(value);} }
	virtual void SetIncrement(f32 increment ) { GetVariant("TODO")->Set(increment); }

	//virtual void ProcessDataTag(char* name , char* value);

	virtual void SetFlashObject(swfSCRIPTOBJECT*);

	virtual void PostStream(char movieID);
	virtual void PostLoad(){Reset();}
protected:
	//const char*	m_Minimum;
	//const char*	m_Maximum;
	//const char*	m_Value;
	//const char*	m_Increment;
	//const char* m_bWrap;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiPromptStripContext
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CONTEXT_PROMPTSTRIP "prompt"
#define CONTEXT_PROMPTSTRIP_CHILDREN "elements"
#define CONTEXT_PROMPTSTRIP_NUMPROMPTS "numVisibleListItems"
#define CONTEXT_PROMPTSTRIP_ISVISIBLE "visibility"
#define CONTEXT_PROMPTSTRIP_NUMPROMPTS_DEFAULTVALUE 1
#define CONTEXT_PROMPTSTRIP_TEMPLATE_MESSAGEBOX 0
#define CONTEXT_PROMPTSTRIP_TEMPLATE_DEFAULT 1

class fuiPromptStripContext : public fuiLayerContext,public UIPromptStripContext
{
public:
	fuiPromptStripContext(UIPromptStrip* pPromptStrip) : fuiLayerContext(pPromptStrip),UIPromptStripContext()
	{
		SetOwner(pPromptStrip);
	}

	virtual ~fuiPromptStripContext(){}

	virtual void SetNumPrompts(int value) { swfVARIANT* v = GetVariant(CONTEXT_PROMPTSTRIP_NUMPROMPTS); if (v ){v->Set(value);} }
	virtual void SetVisible(bool value);
	virtual void ResetFlashObjectToDefaults();
	virtual void PostStream(char movieID);
	virtual void SetFlashObject(swfSCRIPTOBJECT*);
	virtual void PostLoad();
	virtual void Reset();
	virtual void OnHide();
	virtual void OnShow();
	virtual void SetStyle(int style){m_Style=style;}

protected:
	int m_Style;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiMessageBoxContext
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CONTEXT_MESSAGEBOX_TITLE "title"
#define CONTEXT_MESSAGEBOX_TITLE_DEFAULTVALUE "i need a title"
#define CONTEXT_MESSAGEBOX_DESCRIPTION "description"
#define CONTEXT_MESSAGEBOX_DESCRIPTION_DEFAULTVALUE "i need a description"
#define CONTEXT_MESSAGEBOX_OPTIONSLIST "menu"
#define CONTEXT_MESSAGEBOX_ISVISIBLE "visibility"
#define CONTEXT_MESSAGEBOX_CHILDREN "elements"
#define CONTEXT_MESSAGEBOX_TEMPLATE_SIMPLE 1
#define CONTEXT_MESSAGEBOX_TEMPLATE_DEFAULT 2

class fuiMessageBoxContext : public fuiListContext,public UIMessageBoxContext
{
public:
	fuiMessageBoxContext(UIMessageBox* pTab) : fuiListContext(pTab),UIMessageBoxContext()
	{
		SetOwner(pTab);
	}

	virtual ~fuiMessageBoxContext(){}

	virtual void SetTitle(const char* pTitle) { swfVARIANT* v = GetVariant(CONTEXT_MESSAGEBOX_TITLE); if (v ){v->Set(pTitle);} }
	virtual void SetDescription( const char* pDescription) { swfVARIANT* v = GetVariant(CONTEXT_MESSAGEBOX_DESCRIPTION); if (v ){v->Set(pDescription);} }
	virtual void SetVisible(bool value);

	virtual void ResetFlashObjectToDefaults();
	virtual void SetFlashObject(swfSCRIPTOBJECT*);
	virtual void PostStream(char );
	virtual void PostLoad();
	virtual void SetStyle(int style);
	virtual void Reset();
	virtual void OnShow();
	virtual void OnHide();
};

#if !__PPU
#pragma warning(pop)
#endif

}//end namespace rage
#endif // FLASHUICONTEXT_H
