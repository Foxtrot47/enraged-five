// 
// fuicore/Flash.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#define USE_OBJECT_EXPLORER 1

#include "file/asset.h"
#include "flash/resourceversions.h"
#include "grcore/state.h"
#include "paging/rscbuilder.h"
#include "script/wrapper.h"
#include "system/param.h"
#include "system/cache.h"
#include "system/timemgr.h"
#include "grprofile/pix.h"
#include "atl/pool.h"
#include "atl/array.h"
#include "atl/map.h"
#include "data/marker.h"
#include "grcore/im.h"
#include "data/base.h"
#include "fuiflash/Flash.h"
#include "fuicore/Common.h"
#include "fuicore/UIManager.h"
#include "fuicomponents/Transitions.h"
#include "flash/misc.h"
#include "paging/queuescheduler.h"

#define UI_STREAMING_PRIORITY -1000.0f

#if USE_OBJECT_EXPLORER
#include "flash/objectexplorer.h"
#endif

using namespace rage;

bool fuiFlashManager::m_bVisibleOverride = false;

#if __STATS
bool g_ProfileFlash = false;
#endif

atMap<atString, datCallback>		fuiFlashManager::s_callbacks;	// TODO: Consider using atStringMap

const char *fuiFlashManager::sm_szFlashResourceLocation = "$/mapres/";

#if __BANK
bkBank* smp_Bank = 0;
swfObjectExplorer *fuiFlashManager::smp_GlobalsExplorer = 0;
bkGroup *fuiFlashManager::smp_Globals = 0;
#endif

fuiMovieData::fuiMovieData() : 
m_InstanceCount(50),
m_SymbolCount(50),
m_ScriptObjectCount(50),
m_StringHeapSize(50),
m_ClampTextures(true)
{
	m_FileName[0] = 0;
}

void fuiMovieData::SetFileName(const char* name)
{
	strncpy(m_FileName, name, kMaxNameLength);
	strncpy(m_Name, name, kMaxNameLength);
	formatf(m_PathAndFilename,"/%s/%s",name,name);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiMovie
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
fuiMovie::fuiMovie()
:	m_UnloadCounter(0)
,	m_LoadRequested(0)
,	m_bIsLoaded(false)
,	m_bIsStreamed(false)
,	m_IsVisibleResource(0)
,	m_pStreamingMovie(NULL)
,	m_pContext(NULL)
,	m_bVisibleOverride(false)
,	m_TransitionState(-1)
#if __BANK
,	mp_Group(0)
,	mp_VarExplorer(0)
#endif
{	
	RAGE_TRACK(FUI_Flash);

#if STREAM_FLASH_CONTEXTS
	m_pStreamingMovie = rage_new rage::pgStreamable<FlashFileAndContext>;
#else
	m_pStreamingMovie = rage_new rage::pgStreamable<swfFILE>;
#endif
}

void fuiMovie::Init(int memoryBucket)
{
	RAGE_TRACK(FUI_Flash);

	//ready the pgStreamable for streaming this movie
	ASSET.PushFolder(fuiFlashManager::sm_szFlashResourceLocation);
	m_pStreamingMovie->Init(GetPath(), "#sf", swfResourceVersion,memoryBucket);
	ASSET.PopFolder();
}

/*
* PURPOSE: Destructor
*/
fuiMovie::~fuiMovie()
{
	RAGE_TRACK(FUI_Flash);

}

//#if __BANK
//void fuiMovie::AddWidgets(bkBank& b)
//{
//	smp_Bank = &b;
//	RemoveWidgets();	
//	mp_Group = b.PushGroup(GetName());
//	b.AddButton("Output variables",	datCallback(MFA(fuiMovie::OutputFlashVariables), this));
//#if USE_OBJECT_EXPLORER
//	mp_VarExplorer = rage_new swfObjectExplorer(GetContext(), GetName(), &b, mp_Group);
//#endif
//	b.PopGroup();
//}
//
//void fuiMovie::RefreshWidgets()
//{
//	AddWidgets(*smp_Bank);
//}
//
//void fuiMovie::RemoveWidgets()
//{
//	delete mp_VarExplorer;
//	mp_VarExplorer = 0;
//	if (mp_Group)
//	{
//		smp_Bank->Remove(*mp_Group);
//		mp_Group = 0;
//	}
//}
//
//#endif

#if __BANK
void fuiMovie::AddWidgets(bkBank& b)
{
	if (!mp_Group)
	{
		if (GetContext())
		{
			RemoveWidgets();
			mp_Group = b.PushGroup(GetName());
			delete mp_VarExplorer;
			mp_VarExplorer = rage_new swfObjectExplorer(GetContext(), GetName(), &b, mp_Group);
			b.AddButton("Output variables",	datCallback(MFA(fuiMovie::OutputFlashVariables), this));
			b.AddButton("Output Memory Stats",	datCallback(MFA(fuiMovie::OutputMemoryUsage), this));
			b.AddToggle( "Disable Drawing (Force Hide)", &m_bVisibleOverride );
			b.PopGroup();
		}
	}
}

void fuiMovie::RemoveWidgets()
{
	if (mp_Group && smp_Bank)
	{
		delete mp_VarExplorer;
		mp_VarExplorer = 0;
		smp_Bank->Remove(*mp_Group);
		mp_Group = 0;
	}
}
#endif

void fuiMovie::Load(int memoryBucket)
{
	GRAB_FLASH_UPDATE_SEMA();
	RAGE_TRACK(FUI_Flash);

#if __DEV
	size_t iBefore = sysMemAllocator::GetCurrent().GetMemoryAvailable();
	size_t iBlock = sysMemAllocator::GetCurrent().GetLargestAvailableBlock();
	size_t iUsed = sysMemAllocator::GetCurrent().GetMemoryUsed(-1);

	UIDebugf1("Loading movie %s, available mem: %d (block %d) (%d used)",GetName(),iBefore, iBlock, iUsed);
	UIDebugf1("Loading movie %s Available mem:\n   Game heap : %d (%d biggest block)",GetName(),
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetMemoryAvailable(),
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetLargestAvailableBlock() );
#endif

	FLASHMANAGER->PushSharedTextures();
	ASSET.PushFolder(fuiFlashManager::sm_szFlashResourceLocation);
	pgRscBuilder::Load(m_pStreamingFile, GetName(), "#sf", swfResourceVersion, memoryBucket);
	ASSET.PopFolder();
	FLASHMANAGER->PopSharedTextures();

	RAGE_TRACK(FUI_Components);
	if (m_pStreamingFile)
	{
		OnStreamComplete();
	}
	else
	{
		SetLoaded(false);
		SetStreamed(false);
	}


#if __DEV
	size_t iObject = sysMemAllocator::GetCurrent().GetMemoryAvailable();
	size_t iAfter = sysMemAllocator::GetCurrent().GetMemoryAvailable();
	size_t iDiff = iBefore - iAfter;
	UIDebugf1("Before loading : %d, After: %d, Diff: %d, Obj: %d",iBefore,iAfter,iDiff,iObject - iAfter);
	UIDebugf1("After loading movie Available mem:\n   Game heap : %d (%d biggest block)",
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetMemoryAvailable(),
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetLargestAvailableBlock() );
#endif
}

void fuiMovie::PlaceMovie(void *pStreamable, rage::datResourceMap &Map, void * pThis)
{
	GRAB_FLASH_UPDATE_SEMA();
	RAGE_TRACK(FUI_Flash);

#if STREAM_FLASH_CONTEXTS
	rage::pgStreamable<FlashFileAndContext> *movie = (rage::pgStreamable<FlashFileAndContext> *)pStreamable;
#else
	rage::pgStreamable<rage::swfFILE> *movie = (rage::pgStreamable<rage::swfFILE> *)pStreamable;
#endif

	FLASHMANAGER->PushSharedTextures();
	movie->Place(Map);
	FLASHMANAGER->PopSharedTextures();

	(reinterpret_cast<fuiMovie*>(pThis))->m_pStreamingFile = movie->GetObject();
	(reinterpret_cast<fuiMovie*>(pThis))->SetStreamed(true);
}

void fuiMovie::Stream()
{
	RAGE_TRACK(FUI_Flash);

	if(!IsStreamed())
	{
		if (m_pStreamingMovie->IsResident())
		{
			//already resident so no need to place
			SetStreamed(true);
			SetLoaded(true);
		}
		GetStreamable()->Schedule(UI_STREAMING_PRIORITY, PlaceMovie, (void *)this);
	}
}

void fuiMovie::OnStreamComplete()
{
	GRAB_FLASH_UPDATE_SEMA();
	RAGE_TRACK(FUI_Flash);

#if STREAM_FLASH_CONTEXTS
	m_pStreamingFile->m_Context->PostResourceFixup(*m_pStreamingFile->m_File);
	m_pContext = m_pStreamingFile->m_Context;
#else

	//HACK: m_pContext->SetFile(*m_pStreamingFile); doesn't seem to link up all objects. 
	if (m_pContext)
	{
		delete m_pContext;
		m_pContext = NULL;
	}

	if (!m_pContext)
	{
		//create the context if this is the first time this movie has been loaded
		m_pContext = rage_new rage::swfCONTEXT(*m_pStreamingFile,
			(u16)m_FlashData.GetInstanceCount(),
			100,
			(u16)m_FlashData.GetSymbolCount(),
			(u16)m_FlashData.GetScriptObjectCount(),
			m_FlashData.GetStringHeapSize());
	}
	else
	{
		m_pContext->SetFile(*m_pStreamingFile);
	}
#endif

	//this will allocate all the movies global vars
	Init(TIME.GetUnwarpedSeconds());

	SetLoaded(true);

	//notify the user interface hierarchy that we have streamed in a movie
	UIObject* pUI = UIMANAGER->GetUserInterface();

	if (pUI)
	{
		pUI->PostStream(FLASHMANAGER->GetMovieID(GetName()));	
	}
}

void fuiMovie::Unstream()
{	
	Unload();
}

void fuiMovie::Unload()
{	
	GRAB_FLASH_UPDATE_SEMA();
	RAGE_TRACK(FUI_Flash);
	if (IsStreamed())
	{
		SetStreamed(false);
		m_pStreamingMovie->Unschedule();
	}

	if(IsLoaded())
	{
		SetLoaded(false);		
	}
}

void fuiMovie::Update()
{
	GRAB_FLASH_UPDATE_SEMA();
	RAGE_TRACK(FUI_Flash);
	PIXBegin(0, GetName());

	//had to comment out cause got a compile error 'ctxt' undeclared
	//FLASH_METHOD("fuiMovie::Update", 0, 0);
	if (IsLoaded())
	{
		//had to comment out cause got a compile error 'ctxt' undeclared
		//FLASH_METHOD(GetName(), 0, 0);

//#if __BANK
//		if(mp_VarExplorer)
//		{
//			mp_VarExplorer->Bank2Movie();
//		}
//#endif

		GetContext()->GarbageCollect();
		GetContext()->Update(TIME.GetUnwarpedSeconds());

		GetContext()->GetGlobal("transition_isplaying", m_TransitionState);

#if __BANK
		if (smp_Bank && !mp_Group)
		{
			AddWidgets(*smp_Bank);
		}

		if(mp_VarExplorer)
		{
			mp_VarExplorer->Movie2Bank();
		}
#endif
	}
	PIXEnd();
}

/*
* PURPOSE: Reset the state of the flash and audio
*/
void fuiMovie::Reset()
{
	if(!IsLoaded())
	{
		return;
	}

	if (GetContext())
		GetContext()->Reset();
}

#define SETUP_RENDERER 1
void fuiMovie::Draw(const grcViewport* )
{
	GRAB_FLASH_UPDATE_SEMA();

	PIXBegin(0, GetName());

	if (!m_bIsLoaded || !GetContext() || m_bVisibleOverride)
		return;

		//Render 2D Flash Movie:
#if SETUP_RENDERER
		grcViewport *old = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
#endif
		{
#if SETUP_RENDERER
			grcWorldIdentity();

			// temporary!!!!  Shouldn't need to setup lights - and if we do we should do this once (not in draw code)
			grcLightGroup lighting;
			lighting.SetAmbient(1.0f, 1.0f, 1.0f, 1.0f);
			lighting.SetActiveCount(1/*crashes if 0 - rage bug?*/);
			lighting.SetColor(0, 0.0f, 0.0f, 0.0f);

			grcState::Default();
			

			rage::grcState::SetLightingGroup(lighting);		// setting lights seems like a waste of time!!!  Not sure why the 2d shader does lighting calculations

			grcState::SetDepthTest(true);
			grcState::SetDepthWrite(true);
			grcState::SetAlphaBlend(true);
			grcState::SetAlphaTest(true);
			grcLightState::SetEnabled(false);
			grcState::SetCullMode(grccmNone);
			grcState::SetBlendSet(grcbsNormal);

			
#endif
			//draw HUD flash:
			GetContext()->Draw();
		}
#if SETUP_RENDERER
		grcViewport::SetCurrent(old);
#endif		
		PIXEnd();
}

void fuiMovie::Init(float time)
{
	GRAB_FLASH_UPDATE_SEMA();
	if(!GetContext())
	{
		return;
	}

	m_pStreamingFile->SetUsesInput(false);

	//TODO: might be losing dozens of milliseconds during load by passing false for updateonce.
	GetContext()->Update(time,true,false); //hack to get around pre-fscommand legacy techniques
	GetContext()->Update(time,true,false); //hack to get around pre-fscommand legacy techniques

	UIDebugf2("%d funcs",this->GetContext()->m_numFunctions);
}

#if __BANK
void fuiMovie::OutputFlashVariables()
{
	GRAB_FLASH_UPDATE_SEMA();
	if (IsLoaded())
	{
		GetContext()->OutputVariables();
	}
}

void fuiMovie::OutputMemoryUsage()
{
	GRAB_FLASH_UPDATE_SEMA();
	if (IsLoaded())
	{
		GetContext()->OutputMemoryUsage();
	}
}
#endif

void fuiMovie::FadeOut()
{
	GRAB_FLASH_UPDATE_SEMA();
	if (IsLoaded())
	{
		//TODO: IMPLEMENT :)
		GetContext()->SetGlobal("cur_visibility",0);
	}
}

swfVARIANT* fuiMovie::GetVariantSimple(const char* value,swfSCRIPTOBJECT* prop)
{
	const swfVARIANT variant(value);

	if (prop)
	{
		return prop->AccessProperty(variant);
	}
	else
	{
		return GetContext()->GetVariant(variant.GetStringHash(), variant.asConstString());
	}
}

//parses this string and find the correct variants
swfVARIANT* fuiMovie::GetVariant(const char* value)
{
	//movie must be loaded or streamed in
	if (!GetContext())
	{
		UIDebugf3("Could not get variant %s, movie not loaded or streamed",value);
		return NULL;
	}

	const char* dot = strstr(value,".");		
	const char* afterDot = NULL;
	char currentValue[256];
	int index=0;
	rage::swfVARIANT* pObject = NULL;

	GRAB_FLASH_UPDATE_SEMA();

	while(dot)
	{
		//take off the dot and rest of stuff after the dot and try to get just the first object		
		//get the first object
		afterDot = dot+1;
		index = (int)(dot - value);
		strncpy(currentValue,value,256);
		currentValue[index] = '\0';

		//get the next variant
		pObject = GetVariant(currentValue,pObject);

		dot = strstr(afterDot,".");
	}

	if (afterDot)
	{
		return GetVariant(afterDot,pObject);
	}
	else
	{
		return GetVariant(value,pObject);
	}
}

// getting a const string variant passed in means that the string is already hashed
swfVARIANT* fuiMovie::GetVariant(const char* value ,swfVARIANT* variant)
{
	//movie must be loaded or streamed in
	if (!GetContext())
	{
		UIDebugf3("Could not get variant %s, movie not loaded or streamed",value);
		return NULL;
	}

	swfSCRIPTOBJECT* property = NULL;
	const char* array = strstr(value,"[");		
	char currentValue[256];
	int index=0;

	GRAB_FLASH_UPDATE_SEMA();

	if (variant)
	{
		property = verify_cast<swfSCRIPTOBJECT*>(variant->asObject());
	}

	if (array)
	{
		const char* leftBracket = strstr(value,"[");
		strncpy(currentValue,value,256);
		index = (int)(leftBracket - &value[index]);
		currentValue[index] = '\0';

		rage::swfVARIANT* pArrayVariant = GetVariantSimple(currentValue, property);

		//get the index				
		strncpy(currentValue,leftBracket+1,256);
		const char* rightBracket = strstr(currentValue,"]");
		Assertf(rightBracket, "Found [ without matching ]");
		index = (int)(rightBracket - currentValue);
		currentValue[index] = '\0';
		index = atoi(currentValue);
		Assertf(index >= 0 && index < ((rage::swfSCRIPTARRAY*)pArrayVariant->asObject())->GetCount(),"Your flash variant index is out of range");

		return &(*(rage::swfSCRIPTARRAY*)pArrayVariant->asObject())[index];
	}
	else
	{
		return GetVariantSimple(value, property);
	}

}
//bool fuiMovie::GetGlobalString(fuiMovie *pFlash, const char *id, const rage::scrTextLabel63 output64	/* must be 64 chars or more*/)
//{
//	if(!pFlash->IsLoaded())
//	{
//		return false;
//	}
//
//	return pFlash->GetContext()->GetGlobal(id,(char*)&output64[0],63);
//}

bool fuiMovie::GetGlobalInt(fuiMovie *pFlash, const char *id, int & output)
{
	if(!pFlash->IsLoaded())
	{
		return false;
	}

	return pFlash->GetContext()->GetGlobal(id,output);
}

void fuiMovie::SetGlobalString(fuiMovie *pFlash, const char *id, const char *value	/* must be 64 chars or more*/)
{
	GRAB_FLASH_UPDATE_SEMA();
	if(!pFlash->IsLoaded())
	{
		return;
	}

	//This is a courtesy check for frequently used empty strings.  Since scripts, if they get unloaded, may leave bad
	//const char * pointers behind in the flash movies.
	if(value[0])
	{
		pFlash->GetContext()->SetGlobal(id,(const char*)&value[0]);
	}
	else
	{
		pFlash->GetContext()->SetGlobal(id,"");
	}
}

void fuiMovie::SetGlobalStringArray(fuiMovie *pFlash, const char *id, int index, const char *value	/* must be 64 chars or more*/)
{
	GRAB_FLASH_UPDATE_SEMA();
	if(!pFlash->IsLoaded())
	{
		return;
	}

	pFlash->GetContext()->SetGlobalArray(id,index,(const char*)&value[0]);
}

void fuiMovie::SetGlobalInt(fuiMovie *pFlash, const char *id, int value)
{
	if(!pFlash->IsLoaded())
	{
		return;
	}

	pFlash->GetContext()->SetGlobal(id,value);
}

void fuiMovie::SetGlobalIntArray(fuiMovie *pFlash, const char *id, int index, int value)
{
	GRAB_FLASH_UPDATE_SEMA();
	if(!pFlash->IsLoaded())
	{
		return;
	}

	pFlash->GetContext()->SetGlobalArray(id,index,value);
}

void fuiMovie::SetGlobalFloat(fuiMovie * pFlash, const char * id, rage::f32 value)
{
	GRAB_FLASH_UPDATE_SEMA();
	if(!pFlash->IsLoaded())
	{
		return;
	}

	pFlash->GetContext()->SetGlobal(id,value);
}

void fuiMovie::SetGlobalFloatArray(fuiMovie *pFlash, const char *id, int index, rage::f32 value)
{
	GRAB_FLASH_UPDATE_SEMA();
	if(!pFlash->IsLoaded())
	{
		return;
	}

	pFlash->GetContext()->SetGlobalArray(id,index,value);
}

void fuiMovie::OnShow()
{
	GRAB_FLASH_UPDATE_SEMA();
	m_IsVisibleResource++;

	if (m_IsVisibleResource == 1 && IsLoaded())
	{
		SetGlobalInt(this,"cur_visibility",1);
		m_TransitionState = 1;
	}

	//if we havent been activated, then just do it here for convenience
	//SetActive(true);
}

void fuiMovie::OnHide()
{
	GRAB_FLASH_UPDATE_SEMA();
	m_IsVisibleResource--;
	if (m_IsVisibleResource == 0 && IsLoaded())
	{
		SetGlobalInt(this,"cur_visibility",0);
		m_TransitionState = 1;
	}
}

bool fuiMovie::GetProperty(const swfVARIANT& variant, const char *propertyName, swfVARIANT &property)
{
	swfVARIANT propertyNameVariant(propertyName);
	if(variant.asObject()->GetProperty(propertyNameVariant,property,NULL))
	{
		return true;
	}
	return false;
}

bool fuiMovie::GetProperty(const swfVARIANT& variant, const char *propertyName, float& out)
{
	swfVARIANT propertyNameVariant(propertyName);
	swfVARIANT property;
	if(variant.asObject()->GetProperty(propertyNameVariant,property,NULL))
	{
		out = property.asFloat();
		return true;
	}
	else
	{
		//Quitf("Property not found");
		return false;
	}
}

bool fuiMovie::GetProperty(const swfVARIANT& variant, const char *propertyName, int& out)
{
	swfVARIANT propertyNameVariant(propertyName);
	swfVARIANT property;
	if(variant.asObject()->GetProperty(propertyNameVariant,property,NULL))
	{
		out = property.asInt();
		return true;
	}
	else
	{
		//Quitf("Property not found");
		return false;
	}
}

bool fuiMovie::GetProperty(const swfVARIANT& variant, const char *propertyName, Vector3& out)
{
	swfVARIANT propertyNameVariant(propertyName);
	swfVARIANT property;
	if(variant.asObject()->GetProperty(propertyNameVariant,property,NULL))
	{
		out.x = (*(swfSCRIPTARRAY*)property.asObject())[0].asFloat();
		out.y = (*(swfSCRIPTARRAY*)property.asObject())[1].asFloat();
		out.z = (*(swfSCRIPTARRAY*)property.asObject())[2].asFloat();
		return true;
	}
	else
	{
		//Quitf("Property not found");
		return false;
	}
}

bool fuiMovie::GetProperty(const swfVARIANT& variant, const char *propertyName, Vector4& out)
{
	swfVARIANT propertyNameVariant(propertyName);
	swfVARIANT property;
	if(variant.asObject()->GetProperty(propertyNameVariant,property,NULL))
	{
		out.x = (*(swfSCRIPTARRAY*)property.asObject())[0].asFloat();
		out.y = (*(swfSCRIPTARRAY*)property.asObject())[1].asFloat();
		out.z = (*(swfSCRIPTARRAY*)property.asObject())[2].asFloat();
		out.w = (*(swfSCRIPTARRAY*)property.asObject())[3].asFloat();
		return true;
	}
	else
	{
		//Quitf("Property not found");
		return false;
	}
}

void fuiMovie::SetProperty(swfVARIANT& variant, const char *propertyName, int value)
{
	GRAB_FLASH_UPDATE_SEMA();
	swfVARIANT propertyNameVariant(propertyName);
	swfVARIANT property;
	property.Set(value);
	if(!variant.asObject()->SetProperty(propertyNameVariant,property,NULL))
	{
		Quitf("Property not found");
	}
}

void fuiMovie::SetProperty(swfVARIANT& variant, const char *propertyName, float value)
{
	GRAB_FLASH_UPDATE_SEMA();
	swfVARIANT propertyNameVariant(propertyName);
	swfVARIANT property;
	property.Set(value);
	if(!variant.asObject()->SetProperty(propertyNameVariant,property,NULL))
	{
		Quitf("Property not found");
	}
}

void fuiMovie::SetProperty(swfVARIANT& variant, const char *propertyName, const Vector3& value)
{
	GRAB_FLASH_UPDATE_SEMA();

	swfVARIANT propertyNameVariant(propertyName);
	swfVARIANT property;
	if(variant.asObject()->GetProperty(propertyNameVariant,property,NULL))
	{
		(*(swfSCRIPTARRAY*)property.asObject())[0].Set(value.x);
		(*(swfSCRIPTARRAY*)property.asObject())[1].Set(value.y);
		(*(swfSCRIPTARRAY*)property.asObject())[2].Set(value.z);
	}
	else
	{
		Quitf("Couldn't find expected property for Flash 3D mesh");
	}
}

fuiMovie* fuiMovie::GetMovie(const char* m)
{
	return FLASHMANAGER->GetMovie(m);
}

int fuiMovie::GetArraySize(swfCONTEXT& context, const char *arrayName)
{
	return GetArray(context,arrayName).GetCount();
}

swfSCRIPTARRAY& fuiMovie::GetArray(swfCONTEXT& context, const char *arrayName)
{
	swfVARIANT& rVariant = *context.GetVariant(arrayName);

	return *((swfSCRIPTARRAY*)rVariant.asObject());
}

//This is to get around the global nature of the fscommand callback handler in swfCONTEXT:
void fuiFlashManager::FSHandler(const char* command, const swfVARIANT& param)
{
	GRAB_FLASH_UPDATE_SEMA();
	datCallback* callback = s_callbacks.Access(command);
	Assertf(callback,"Couldn't find callback for fscommand \"%s\"",command);
	if(callback)
	{
		callback->Call((CallbackData)&param);
	}
}

void fuiFlashManager::GetVarCallback(const swfVARIANT* )
{

}
void fuiFlashManager::SetVarCallback(const swfVARIANT* )
{

}
void fuiFlashManager::DisableMovie(const swfVARIANT* )
{

}
void fuiFlashManager::ReadyforTransition(const swfVARIANT* )
{

}

void fuiFlashManager::FSAssert(const swfVARIANT* ASSERT_ONLY(param))
{
#if __ASSERT
	const char* pAssertStr = param->asConstString();
	Assertf(0, "FlashAssert: %s", pAssertStr);
#endif
}

void fuiFlashManager::TerminateFlashTransition()
{
	UITransition* pTransition = verify_cast<UITransition*>(UIMANAGER->GetUIObject("UITransition_WaitForFlashTransition"));
	pTransition->Terminate();
}

fuiFlashManager::fuiFlashManager()
{
	m_NumMovies = 0;
	m_pSharedTextures = NULL;
	m_bVisibleOverride = false;
#if __BANK
	m_bReloadRequested = false;
#endif

	swfCONTEXT::SetFSCommandCallback("FSCommand:assert",datCallback(MFA1(fuiFlashManager::FSAssert), this, 0, true));
	swfCONTEXT::SetFSCommandCallback("FSCommand:disable",datCallback(MFA2(fuiFlashManager::DisableMovie), this, 0));
	swfCONTEXT::SetFSCommandCallback("FSCommand:ReadyforTransition",datCallback(MFA2(fuiFlashManager::ReadyforTransition), this, 0));
	swfCONTEXT::SetFSCommandCallback("FSCommand:setvar",datCallback(MFA2(fuiFlashManager::SetVarCallback), this, 0));
	swfCONTEXT::SetFSCommandCallback("FSCommand:getvar",datCallback(MFA2(fuiFlashManager::GetVarCallback), this, 0));
	swfCONTEXT::SetFSCommandCallback("FSCommand:MeasureString",datCallback(MFA2(fuiFlashManager::MeasureStringCallback), this, 0));
	swfCONTEXT::SetFSCommandCallback("FSCommand:MeasureSlotHeight",datCallback(MFA2(fuiFlashManager::CountLinesCallback), this, 0));
	swfCONTEXT::SetFSCommandCallback("FSCommand:Getstring",datCallback(MFA2(fuiFlashManager::GetString), this, 0));
	swfCONTEXT::SetFSCommandCallback("FSCommand:TransitionComplete",datCallback(MFA(fuiFlashManager::TerminateFlashTransition), this));
	swfCONTEXT::SetFSCommandCallback("FSCommand:IsWide",datCallback(MFA(fuiFlashManager::IsWide), this));
	swfCONTEXT::SetFSCommandCallback("FSCommand:IsHighDef",datCallback(MFA(fuiFlashManager::IsHighDef), this));

	//streamable flash movies
	for(int i = 0; i < MAX_MOVIES; i++)
	{
		m_MoviesList[i] = NULL;
	}
}
fuiFlashManager::~fuiFlashManager()
{
	swfCONTEXT::SetFSCommandHandler(NULL);

	if(m_pSharedTextures)
	{
		pgRscBuilder::Delete(m_pSharedTextures);
	}

	UnloadAllMovies();
}

bool fuiFlashManager::IsWide()
{
	return GRCDEVICE.GetWideScreen();
}

bool fuiFlashManager::IsHighDef()
{
	return GRCDEVICE.GetHiDef();
}

void fuiFlashManager::RegisterMovie(fuiMovie* movie)
{
	m_MoviesList[m_NumMovies] = movie;
	m_MovieIDs.Insert(movie->GetName(),(char)m_NumMovies);
	m_NumMovies++;
}

void fuiFlashManager::MeasureStringCallback(swfCONTEXT* ctxt, const swfVARIANT *)
{
	swfFILE *file = ctxt->GetFile();
	swfCONTEXT *context = ctxt;

	Assert(file && context);

	char szFontName[64];
	context->GetGlobal("fs_measure_font",szFontName,sizeof(szFontName));

	AssertMsg(strlen(szFontName) < 512 , "Hit limit on string to measure");

	char szText[512];

	float fPointSize = 0;
	context->GetGlobal("fs_measure_size", fPointSize);

	context->GetGlobal("fs_measure_string",szText,sizeof(szText));

	// need to pick one here
	float result = file->MeasureString(szFontName,fPointSize,szText);
	context->SetGlobal("fs_measure_result", result);	// for MC
	context->SetGlobal("fs_measure_width", result);		// for RDR
}

void fuiFlashManager::GetString(const swfVARIANT *)
{

}

void fuiFlashManager::CountLinesCallback(swfCONTEXT* ctxt, const swfVARIANT *)
{
	// int iLines = 0;
	float fWidth;

	swfFILE *file = ctxt->GetFile(); 
	swfCONTEXT *context = ctxt;

	Assert(file && context);

	char szFontName[64];
	context->GetGlobal("fs_measure_font",szFontName,sizeof(szFontName));

	AssertMsg(strlen(szFontName) < 512 , "Hit limit on string to measure");

	char szText[512];

	float fPointSize = 0;
	context->GetGlobal("fs_measure_size", fPointSize);

	context->GetGlobal("fs_measure_string",szText,sizeof(szText));

	context->GetGlobal("fs_measure_slotWidth",fWidth);

	context->SetGlobal("fs_measure_height", /*iLines = */file->CountLines(szFontName,fPointSize,szText,fWidth));
}

#if __BANK
void fuiFlashManager::AddGlobals()
{
	if ((!smp_Globals) && smp_Bank)
	{
		smp_Globals = smp_Bank->PushGroup("GLOBALS");
		smp_GlobalsExplorer = rage_new swfObjectExplorer(0, "GENERAL GLOBALS", smp_Bank, smp_Globals);
		smp_Bank->PopGroup();
	}
}

void fuiFlashManager::AddAllWidgets()
{
	if(smp_Bank)
		RemoveAllWidgets();

	Assert(!smp_Bank);
	smp_Bank = &BANKMGR.CreateBank("fuiFlashManager");
	smp_Bank->AddButton("Rebuild Widgets", datCallback(fuiFlashManager::AddAllWidgets));
	smp_Bank->AddToggle( "Disable UI Drawing (Force Hide)", &m_bVisibleOverride );

	smp_Globals = 0;

	//AddGlobals();

	for (char i=0;i<MAX_MOVIES;i++)
	{
		if (FLASHMANAGER->GetMovie(i))
		{
			FLASHMANAGER->GetMovie(i)->AddWidgets(*smp_Bank);
		}
	}
}

void fuiFlashManager::AddWidgets(bkBank& )
{
	AddAllWidgets();
}

void fuiFlashManager::RemoveAllWidgets()
{
	for (char i=0;i<MAX_MOVIES;i++)
	{
		if (FLASHMANAGER->GetMovie(i))
		{
			FLASHMANAGER->GetMovie(i)->RemoveWidgets();
		}
	}

	delete smp_GlobalsExplorer;
	smp_GlobalsExplorer = 0;

	Assert(smp_Bank);
	BANKMGR.DestroyBank(*smp_Bank);

	//Reset pointers
	smp_Globals = 0;
	smp_Bank = 0;
}

#endif

void fuiFlashManager::SetCallback(const char* command, datCallback callback)
{
#if __DEV
	if(s_callbacks.Access(command))
	{
		Warningf("Fscommand \"%s\" has already been registered",command);
	}
#endif

	s_callbacks.Insert(atString(command),callback);
}

fuiMovie* fuiFlashManager::FindMovie(const char *identifier)
{
	return m_MoviesList[(int)GetMovieID(identifier)];
}

#if __BANK
void fuiFlashManager::OutputPoolSizes()
{
	for (int i=0;i<MAX_MOVIES;i++)
	{
		if (m_MoviesList[i])
		{
			Printf("Begin pool sizes for %s\n", m_MoviesList[i]->GetName());
			m_MoviesList[i]->GetContext()->OutputPoolSizes();
			Printf("End pool sizes for %s\n", m_MoviesList[i]->GetName());
		}
	}
}
#endif

void fuiFlashManager::UnloadAllMovies()
{
	GRAB_FLASH_UPDATE_SEMA();

	for (int i=0;i<MAX_MOVIES;i++)
	{
		if (m_MoviesList[i])
		{
			delete m_MoviesList[i];
		}
	}
}

void fuiFlashManager::Update(float /*dt*/)
{
	GRAB_FLASH_UPDATE_SEMA();

#if __STATS
	if (g_ProfileFlash)
	{
		swfPROFILER::StartProfiling();
	}
#endif

	//Reload movies if it has been requested to do so:
#if __BANK
	if(m_bReloadRequested)
	{
		//ReloadMovies(dt);

		m_bReloadRequested = false;
	}
#endif

	for (int i=0;i<MAX_MOVIES;i++)
	{
		if(m_MoviesList[i] && m_MoviesList[i]->IsLoaded() && m_MoviesList[i]->IsVisible())
		{
				m_MoviesList[i]->Update();
		}
	}

#if __STATS
	g_ProfileFlash = false;
	swfPROFILER::sm_Profiling = false;
#endif
}

void fuiFlashManager::PushSharedTextures()
{
	if (m_pSharedTextures)
	{
		m_pSharedTextures->Push();
	}
}

void fuiFlashManager::PopSharedTextures()
{

	if (m_pSharedTextures)
	{
		m_pSharedTextures->Pop();
	}
}

//
// For streamed movies, schedule the swfFILE for unstreaming.  And then schedule the swfCONTEXT for deletion.
//
void fuiFlashManager::UnloadMovie(const char *identifier)
{
	GRAB_FLASH_UPDATE_SEMA();

	char movieIndex = GetMovieID(identifier);

	//Find movie, delete, and remove from storage (list, and index):
	if(m_MoviesList[(int)movieIndex])
	{
		m_MoviesList[(int)movieIndex]->SetLoaded(false);
		m_MoviesList[(int)movieIndex]->SetStreamed(false);
		m_MoviesList[(int)movieIndex]->SetVisible(false);
		m_MoviesList[(int)movieIndex]->MarkForUnload();
	}
}

char fuiFlashManager::GetMovieID(const char* name)
{
	char* id = m_MovieIDs.Access(name);
	if (id)
	{
		return *id;
	}
	else
	{
		UIDebugf1("movie %s was not loaded or registered",name);
		return -1;
	}
}

void fuiFlashManager::Load(int memoryTrackingBucket)
{
	GRAB_FLASH_UPDATE_SEMA();

	ASSET.PushFolder(sm_szFlashResourceLocation);

	pgRscBuilder::Load(m_pSharedTextures, "shared", "#td", grcTexture::RORC_VERSION,memoryTrackingBucket);

	ASSET.PopFolder();
}

void fuiFlashManager::Draw(const grcViewport* pGameVP)
{
	//PF_START(FUIFlashDraw);

	if (m_bVisibleOverride)
	{
		return;
	}

	for (int i=0; i<MAX_MOVIES; i++)
	{
		if (m_MoviesList[i] && m_MoviesList[i]->IsVisible())
		{
			m_MoviesList[i]->Draw(pGameVP);
		}
	}

	//PF_STOP(FUIFlashDraw);
}

namespace {
	void _justAssert()
	{
		//Assert(0);
	}
}

void (*fuiFlashManager::sm_GrabFlashSemaFunc)() = _justAssert;
void (*fuiFlashManager::sm_ReleaseFlashSemaFunc)() = _justAssert;

flashSemaGrabber::flashSemaGrabber()
{
	fuiFlashManager::sm_GrabFlashSemaFunc();
}

flashSemaGrabber::~flashSemaGrabber()
{
	fuiFlashManager::sm_ReleaseFlashSemaFunc();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DEPRECATED BELOW

fuiRegistry *fuiRegistry::s_pRegistryInstance = NULL;

void fuiRegistry::RegisterScriptCommands()
{
	SCR_REGISTER_SECURE(Registry_SetValueFloat, 0x3518b4ea, fuiRegistry::SetValueFloat);
	SCR_REGISTER_SECURE(Registry_SetValueInt, 0x1623e188, fuiRegistry::SetValueInt);
	SCR_REGISTER_SECURE(Registry_SetValueString, 0x3f064e6c, fuiRegistry::SetValueString);
	SCR_REGISTER_SECURE(Registry_SetValueBool, 0x2ce7da55, fuiRegistry::SetValueBool);

	SCR_REGISTER_SECURE(Registry_GetValueFloat, 0x257cf32f, fuiRegistry::GetValueFloat);
	SCR_REGISTER_SECURE(Registry_GetValueInt, 0x2ca24db0, fuiRegistry::GetValueInt);
	SCR_REGISTER_SECURE(Registry_GetValueConstString, 0x7019657f, fuiRegistry::GetValueConstString);
	SCR_REGISTER_SECURE(Registry_GetValueBool, 0xce7b5628, fuiRegistry::GetValueBool);
}


fuiVariant::fuiVariant() : Dirty(false)
{
	this->Type = AS_UNDEFINED;
	this->szStr[0] = '\0';
}
fuiVariant::fuiVariant(const char * value) : Dirty(true)
{
	this->Set(value);
}
fuiVariant::fuiVariant(int value) : Dirty(true)
{
	this->Set(value);
}
fuiVariant::fuiVariant(float value) : Dirty(true)
{
	this->Set(value);
}
fuiVariant::fuiVariant(bool value) : Dirty(true)
{
	this->Set(value);
}


#if __BANK
void FlashHelper::AddWidgets(bkBank& b)
{
	bkGroup* pGroup = b.PushGroup(m_szName);
	b.AddButton("Output variables",	datCallback(MFA(FlashHelper::OutputFlashVariables), this));
#if USE_OBJECT_EXPLORER
	mp_VarExplorer = rage_new swfObjectExplorer(GetContextPtr(), m_szName, &b, pGroup);
#endif
	b.PopGroup();
}

void FlashHelper::RemoveWidgets(bkBank&)
{
	delete mp_VarExplorer;
	mp_VarExplorer = 0;
}

void FlashHelper::OutputFlashVariables()
{
	GetContext().OutputVariables();
}
#endif

//===============================================================================================================
//											FlashHelper
//===============================================================================================================

const char* fhCmdName[] =
{
	"setInt",
	"setString",
	"setArray",
	NULL
};


//FlashHelper::fhVhsmObject::fhVhsmObject(FlashHelper *pMovie, const char *szName) : 
//	m_pMovie		(pMovie),
//	vhsmExtObject	(szName, fhCmdName, NULL)
//{
//}

//vhsmState* FlashHelper::fhVhsmObject::HandleCommand(VirtualHSM*, const vhsmCommand *pCmd, vhsmState*)
//{
//	switch(pCmd->m_CommandIdx)
//	{
//	case fhVhsmSetInt:
//		{
//			int Int;
//			m_pMovie->GetContext().GetGlobal(pCmd->GetArg(0)->AsString(), Int);
//			Displayf("[Flash] %s is %d", pCmd->GetArg(0)->AsString(), Int);
//		}
//		m_pMovie->GetContext().SetGlobal(pCmd->GetArg(0)->AsString(), pCmd->GetArg(1)->AsInt());
//		{
//			int Int;
//			if(m_pMovie->GetContext().GetGlobal(pCmd->GetArg(0)->AsString(), Int))
//				Displayf("[Flash] %s set to %d", pCmd->GetArg(0)->AsString(), Int);
//		}
//		break;
//
//	case fhVhsmSetString:
//		m_pMovie->GetContext().SetGlobal(pCmd->GetArg(0)->AsString(), pCmd->GetArg(1)->AsString());
//		break;
//
//	case fhVhsmSetArray:
//		Assert(false && "setArray not implemented yet");
//		break;
//	}
//
//	return NULL;
//}

RAGE_OBJECT_DECLARE(swfFILE);


bool FlashHelper::BeginStream(const char *szMoviePath, pgStreamerInfo &outInfo,datResourceMap &map , int memoryBucket)
{
	char pckname[256];
	strncpy(pckname,szMoviePath,sizeof(pckname)-1);
	pckname[sizeof(pckname)-1]=0;
	char * pDot = strrchr(pckname,'.');
	if (pDot)
		*pDot = 0;
	//strncat(pckname,".pck",sizeof(pckname)-1);


	RAGE_OBJECT_INIT(szMoviePath);
	RAGE_OBJECT_LABEL(szMoviePath, "%s", szMoviePath);

	bool result;

	{
		RAGE_SCOPE_OBJECT(szMoviePath);

#if 1 //__PAGING
		// Load resource file
		// This object must stay in scope and be unmodified until EndStream
		// completes.
		ASSET.PushFolder(fuiFlashManager::sm_szFlashResourceLocation);
		result = (pgRscBuilder::BeginStream(pckname, "#sf", swfResourceVersion, outInfo, map) != NULL);

		//Shouldn't be needed:
		//WritebackDC((const void *)info.ReadHandlePhysical->m_Dest, info.ReadHandlePhysical->m_Size);
		ASSET.PopFolder();
#endif // __PAGING
	}

	// BEGIN MODS RDR2 HACK HACK: Track memory. We shouldn't be calling
	// BeginStream directly in the first place. /MAK
	if (result && outInfo.ReadHandle)
	{
		pgRscMemTracker::SafeAddResource(outInfo.Info, memoryBucket);
	}
	// END MODS RDR2 HACK HACK: Track memory. We shouldn't be calling
	// BeginStream directly in the first place. /MAK


	RAGE_OBJECT_KILL(szMoviePath);

	return result;
}


swfFILE* FlashHelper::EndStream(const char *szMoviePath, pgStreamerInfo & info,datResourceMap & map)
{
	swfFILE *pSwfFile = NULL;

	char pckname[256];
	strncpy(pckname,szMoviePath,sizeof(pckname)-1);
	pckname[sizeof(pckname)-1]=0;
	char * pDot = strrchr(pckname,'.');
	if (pDot)
		*pDot = 0;
	//strncat(pckname,".pck",sizeof(pckname)-1);

#ifndef __SNC__
	RAGE_OBJECT_INIT(pSwfFile);
	RAGE_OBJECT_LABEL(pSwfFile, "%s", szMoviePath);
#endif

	{
		RAGE_SCOPE_OBJECT(pSwfFile);

#if 1 //__PAGING
		// Load resource file
		// This object must stay in scope and be unmodified until EndStream
		// completes.
		ASSET.PushFolder(fuiFlashManager::sm_szFlashResourceLocation);
		FLASHMANAGER->PushSharedTextures();
		pgRscBuilder::EndStream(pSwfFile,info,map,true /*block*/,true /*close*/,pckname);
		FLASHMANAGER->PopSharedTextures();
		//Shouldn't be needed:
		//WritebackDC((const void *)info.ReadHandlePhysical->m_Dest, info.ReadHandlePhysical->m_Size);
		ASSET.PopFolder();
#else // __PAGING

		ASSET.PushFolder("$/ui/");
		pSwfFile = rage_new swfFILE;
		pSwfFile->Load(szMoviePath);
		ASSET.PopFolder();

#endif // __PAGING
	}

	RAGE_OBJECT_KILL(pSwfFile);

	RAGE_OBJECT_INIT(*pSwfFile);
	RAGE_OBJECT_LABEL(*pSwfFile,"%s",szMoviePath);

	return pSwfFile;
}


RAGE_OBJECT_DECLARE(FlashFileAndContext);

bool FlashHelper::BeginFFCStream(const char *szMoviePath, pgStreamerInfo & /*outInfo*/,datResourceMap & /*map*/)
{
	Assert(STREAM_FLASH_CONTEXTS);
	char pckname[256];
	strncpy(pckname,szMoviePath,sizeof(pckname)-1);
	pckname[sizeof(pckname)-1]=0;
	char * pDot = strrchr(pckname,'.');
	if (pDot)
		*pDot = 0;
	//strncat(pckname,".pck",sizeof(pckname)-1);


	RAGE_OBJECT_INIT(szMoviePath);
	RAGE_OBJECT_LABEL(szMoviePath, "%s", szMoviePath);

	bool result = false;

	{
		RAGE_SCOPE_OBJECT(szMoviePath);

#if STREAM_FLASH_CONTEXTS
		// Load resource file
		// This object must stay in scope and be unmodified until EndStream
		// completes.
		ASSET.PushFolder(fuiFlashManager::sm_szFlashResourceLocation);
		result = (pgRscBuilder::BeginStream(pckname, "#sfc", swfcResourceVersion, outInfo, map) != NULL);

		//Shouldn't be needed:
		//WritebackDC((const void *)info.ReadHandlePhysical->m_Dest, info.ReadHandlePhysical->m_Size);
		ASSET.PopFolder();
#endif // STREAM_FLASH_CONTEXTS
	}

	RAGE_OBJECT_KILL(szMoviePath);

	return result;
}


FlashFileAndContext* FlashHelper::EndFFCStream(const char *szMoviePath, pgStreamerInfo & /*info*/,datResourceMap & /*map*/)
{
	Assert(STREAM_FLASH_CONTEXTS);
	FlashFileAndContext *pFFCFile = NULL;

	char pckname[256];
	strncpy(pckname,szMoviePath,sizeof(pckname)-1);
	pckname[sizeof(pckname)-1]=0;
	char * pDot = strrchr(pckname,'.');
	if (pDot)
		*pDot = 0;
	//strncat(pckname,".pck",sizeof(pckname)-1);


#ifndef __SNC__
	RAGE_OBJECT_INIT(pFFCFile);
	RAGE_OBJECT_LABEL(pFFCFile, "%s", szMoviePath);
#endif

#if STREAM_FLASH_CONTEXTS
	{
		RAGE_SCOPE_OBJECT(pFFCFile);

		// Load resource file
		// This object must stay in scope and be unmodified until EndStream
		// completes.
		ASSET.PushFolder(fuiFlashManager::sm_szFlashResourceLocation);
		pgRscBuilder::EndStream(pFFCFile,info,map,true /*block*/,true /*close*/,pckname);
		pFFCFile->m_Context->PostResourceFixup(*pFFCFile->m_File);
		//Shouldn't be needed:
		//WritebackDC((const void *)info.ReadHandlePhysical->m_Dest, info.ReadHandlePhysical->m_Size);
		ASSET.PopFolder();
	}
#endif // STREAM_FLASH_CONTEXTS
	RAGE_OBJECT_KILL(pFFCFile);

	RAGE_OBJECT_INIT(*pFFCFile);
	RAGE_OBJECT_LABEL(*pFFCFile,"%s",szMoviePath);

	return pFFCFile;
}


void FlashHelper::ForceLoad()
{
	// THIS IS NOT SAFE - SCHEDULER UPDATE MUST BE CALLED IN DMZ!!!!
	//Assert(m_pStreamingMovie && m_pStreamingMovie->IsScheduled());
	//if(!m_pStreamingMovie || !m_pStreamingMovie->IsScheduled())
	//	return;

	while (m_pStreamingMovie->GetState() != pgStreamableBase::RESIDENT)
	{
		m_pStreamingMovie->ScheduleAndPlace(0.5f);
		pgQueueScheduler::GetScheduler().Update();
		pgStreamer::Drain();
	}
}

swfFILE* FlashHelper::Stream(const char *szMoviePath)
{
	pgStreamerInfo info;
	datResourceMap map;

	if (BeginStream(szMoviePath, info, map,0))
		return EndStream(szMoviePath, info, map);
	else
		return NULL;
}

FlashFileAndContext* FlashHelper::FFCStream(const char *szMoviePath)
{
	pgStreamerInfo info;
	datResourceMap map;

	if (BeginFFCStream(szMoviePath, info, map))
		return EndFFCStream(szMoviePath, info, map);
	else
		return NULL;
}

FlashHelper::FlashHelper(int memoryBucket, const char* pName, const char* pFilename, int instanceCount, int symbolCount, int scriptObjectCount, int stringHeapSize) : 
	m_swfContext(NULL),
	m_pSwfFile(NULL),
	m_pStreamingMovie(NULL),
	m_pStreamingFile(NULL),
	m_pFileAndContext(NULL),
	m_pGlobalDictionary(NULL),
	m_bEnabled(true),
	m_bManualUpdate(false),
	m_bCameraEnabled(true),
	m_bGarbageCollect(true),
	m_bOwnsFile(true),
	m_IsVisibleResource(0),
	m_bIsActive(false),
	m_bIsStreamed(false),
	m_bOwnsContext(true)
#if __BANK
	, mp_VarExplorer(NULL)
#endif
{
	RAGE_TRACK(FUI_Flash);

#if STREAM_FLASH_CONTEXTS
	m_pStreamingMovie = rage_new rage::pgStreamable<FlashFileAndContext>;
#else
	m_pStreamingMovie = rage_new rage::pgStreamable<swfFILE>;
#endif

	m_FlashData.SetName(pName);
	m_FlashData.SetFileName(pFilename);
	m_FlashData.SetInstanceCount(instanceCount);
	m_FlashData.SetSymbolCount(symbolCount);
	m_FlashData.SetScriptObjectCount(scriptObjectCount);
	m_FlashData.SetStringHeapSize(stringHeapSize);

	//ready the pgStreamable for streaming this movie
	ASSET.PushFolder(fuiFlashManager::sm_szFlashResourceLocation);
	m_pStreamingMovie->Init(GetPath(), "#sf", swfResourceVersion,memoryBucket);
	ASSET.PopFolder();
}

FlashHelper::FlashHelper(const char *szMoviePath, fuiRegistry* globalDictionary, 
						 u16 instanceCount, u16 swfScriptObjectCount, 
						 int stringHeapSize
						 ) : 
	m_swfContext(NULL),
	m_pSwfFile(NULL),
	m_pStreamingFile(NULL),
	m_pFileAndContext(NULL),
	m_pGlobalDictionary(globalDictionary),
	m_bEnabled(true),
	m_bManualUpdate(false),
	m_bCameraEnabled(true),
	m_bGarbageCollect(false),
	m_bOwnsFile(true),
	m_IsVisibleResource(0),
	m_bIsActive(false),
	m_bIsStreamed(false),
	m_bOwnsContext(true)
{
	RAGE_FUNC();

#if !__RESOURCECOMPILER
//	MCTHREADMGR->FlashSemaSignal(false);
#endif

	pgStreamerInfo info;
	datResourceMap map;

#if STREAM_FLASH_CONTEXTS
	const char* extn = ASSET.FindExtensionInPath(szMoviePath);
	if (extn && (stricmp(extn+2, "sfc") == 0))
	{
		bool result = FlashHelper::BeginFFCStream(szMoviePath, info, map);
		
#if !__RESOURCECOMPILER
//		MCTHREADMGR->FlashSemaWait(false);
#endif

		this->m_pFileAndContext = result ? FlashHelper::EndFFCStream(szMoviePath, info, map) : NULL;

		if(!this->m_pFileAndContext)
		{
			Quitf("Cannot stream '%s'.  Try evicting something from the streamining heap before it gets streamed in.",szMoviePath);
		}

		this->m_pSwfFile = this->m_pFileAndContext->m_File;
		this->m_swfContext = this->m_pFileAndContext->m_Context;
		
		// make sure the counts match what we resourced with:
		u32 actualInsts, actualClipEvents, actualSymbols, actualStringHeap, actualScriptObj;
		this->m_swfContext->GetPoolSizes(actualInsts, actualClipEvents, actualSymbols, actualScriptObj, actualStringHeap);
		Assert(actualInsts == instanceCount);
		Assert(actualClipEvents == 100);
		Assert(actualSymbols == 0); // symbol pool currently unused
		Assert(actualStringHeap == (u32)stringHeapSize/12); // div by 12 because string heap stores in 12byte chunks
	}
	else
#endif
	{

		bool result = FlashHelper::BeginStream(szMoviePath, info, map, 0);

#if !__RESOURCECOMPILER
//		MCTHREADMGR->FlashSemaWait(false);
#endif

		this->m_pSwfFile = result ? FlashHelper::EndStream(szMoviePath, info, map) : NULL;

		if(!this->m_pSwfFile)
		{
			Quitf("Cannot stream '%s'.  Try evicting something from the streamining heap before it gets streamed in.",szMoviePath);
		}
		
	//	Printf("Streaming %s: inst=%d, stringHeap=%d\n", szMoviePath, instanceCount, stringHeapSize);

	// try and load from a resource
		// script obj count is unused - pass 0.																		
		// u16 swfScriptObjectCount = 0;
		this->m_swfContext = rage_new rage::swfCONTEXT(*this->m_pSwfFile,instanceCount,100,1500,swfScriptObjectCount,stringHeapSize);
	}

	//setup fs_command hooks:
	//this->m_swfContext->SetTag((void*)this);

#if __BANK
	this->SetMoviePath(szMoviePath);
#endif

	m_pSwfFile->SetUsesInput(false);
}


//
// Set file pointer to NULL if we are not the owner, so that it doesn't get deleted more than once.
//
FlashHelper::FlashHelper(FlashFileAndContext& fileAndContext, fuiRegistry* globalDictionary, bool owner,int memoryBucket) 
:m_swfContext(NULL),
m_pSwfFile(fileAndContext.m_File),
m_pFileAndContext(&fileAndContext), // keep this NULL so we don't try to delete it.
m_pGlobalDictionary(globalDictionary),
m_bEnabled(true),
m_bManualUpdate(false),
m_bCameraEnabled(true),
m_bGarbageCollect(false),
m_bOwnsFile(owner),
m_bOwnsContext(owner),
m_IsVisibleResource(0),
m_bIsActive(false),
m_bIsStreamed(false),
m_bEnableRequested(false)
{
	RAGE_FUNC();
	RAGE_SCOPE_OBJECT(*fileAndContext.m_File);

	Assert(&fileAndContext);

	m_swfContext = fileAndContext.m_Context;

	//setup fs_command hooks:
	//this->m_swfContext->SetTag((void*)this);

	m_pSwfFile->SetUsesInput(false);

	RAGE_TRACK(FUI_Flash);

#if STREAM_FLASH_CONTEXTS
	m_pStreamingMovie = rage_new rage::pgStreamable<FlashFileAndContext>;
#else
	m_pStreamingMovie = rage_new rage::pgStreamable<swfFILE>;
#endif

	//ready the pgStreamable for streaming this movie
	ASSET.PushFolder(fuiFlashManager::sm_szFlashResourceLocation);
	m_pStreamingMovie->Init(GetPath(), "#sf", swfResourceVersion,memoryBucket);
	ASSET.PopFolder();
}

//
// Set file pointer to NULL if we are not the owner, so that it doesn't get deleted more than once.
//
FlashHelper::FlashHelper(swfFILE& file, fuiRegistry* globalDictionary, u16 instanceCount, u16 swfScriptObjectCount, int stringHeapSize, bool owner) :
	m_swfContext(NULL),
	m_pSwfFile(&file),
	m_pFileAndContext(NULL),
	m_pGlobalDictionary(globalDictionary),
	m_bEnabled(true),
	m_bManualUpdate(false),
	m_bCameraEnabled(true),
	m_bGarbageCollect(false),
	m_bOwnsFile(owner),
	m_bOwnsContext(true),
	m_IsVisibleResource(0),
	m_bIsActive(false),
	m_bIsStreamed(false),
	m_bEnableRequested(false)
{
	RAGE_FUNC();
	RAGE_SCOPE_OBJECT(*this->m_pSwfFile);

	Assert(&file);
	
	this->m_swfContext = rage_new rage::swfCONTEXT(*this->m_pSwfFile,instanceCount,100,1500,swfScriptObjectCount,stringHeapSize);

	//setup fs_command hooks:
//	this->m_swfContext->SetTag((void*)this);

	m_pSwfFile->SetUsesInput(false);
}

void FlashHelper::Init(float time, bool /*loadtime , mcUIMeshDictionary* meshes*/)
{
	//TODO: might be losting dozens of milliseconds during load by passing false for updateonce.
	this->m_swfContext->Update(time,true,false); //hack to get around pre-fscommand legacy techniques
	this->m_swfContext->Update(time,true,false); //hack to get around pre-fscommand legacy techniques

//	this->m_swfContext->Init(loadtime,meshes);

#if __BANK && !__RESOURCECOMPILER
	Displayf("%d funcs",this->m_swfContext->m_numFunctions);
#endif

}

void FlashHelper::Unstream(/*datResourceInfo hdr*/)
{
	//if(m_swfContext && m_bOwnsContext)
	//{
	//	delete m_swfContext;
	//	m_swfContext = NULL;
	//	m_bOwnsContext = false;
	//}

	if(m_pStreamingFile && m_pStreamingMovie && IsStreamed())
	{
		 UIDebugf1("Streaming out %s",GetName());
		 SetStreamed(false);
		 m_pStreamingMovie->Unschedule();//pgQueueScheduler::GetInstance().Unschedule();

		////BEGIN MODS RDR2 HACK HACK: Track memory.
		//#define SAG_TRACKING_BUCKET_HUD_FLASH (MEM_USER_TRACKING_BUCKET + 11)
		//pgRscMemTracker::SafeRemoveResource(hdr, SAG_TRACKING_BUCKET_HUD_FLASH);
		////ND MODS RDR2 HACK HACK: Track memory.
		//pgRscBuilder::Delete(m_pSwfFile);
		//RAGE_OBJECT_KILL(*m_pSwfFile);
		//m_pSwfFile = NULL;
	}
	
//#if STREAM_FLASH_CONTEXTS	
//	if (m_pFileAndContext && m_bOwnsContext)
//	{
//		// we just handled deleting the sub-objects.
//		m_pFileAndContext->m_File = NULL;
//		m_pFileAndContext->m_Context = NULL;
//		delete m_pFileAndContext;
//		m_pFileAndContext = NULL;
//	}
//#endif
}

FlashHelper::~FlashHelper()
{
	if(m_swfContext && m_bOwnsContext)
	{
		delete m_swfContext;
	}

	Assertf(!m_pSwfFile || !m_bOwnsFile,"deleting FlashHelper without unstreaming the movie, memory will not be tracked");
	if(m_pStreamingFile /*&& m_bOwnsFile*/)
	{
		UIDebugf1("Streaming out %s",GetName());
		//pgQueueScheduler::GetInstance().Unschedule(m_pStreamingMovie);
	}

	if (m_pFileAndContext && m_bOwnsContext)
	{
		// we just handled deleting the sub-objects.
		m_pFileAndContext->m_File = NULL;
		m_pFileAndContext->m_Context = NULL;
		delete m_pFileAndContext;
	}
}

#if !__RESOURCECOMPILER

void FlashHelper::Update(float time, bool oneFrameOnly, bool updateInput)
{
	GRAB_FLASH_UPDATE_SEMA();

	if(this->m_bEnabled || IsActive())
	{
#if __BANK && USE_OBJECT_EXPLORER
		if(mp_VarExplorer)
		{
			mp_VarExplorer->Bank2Movie();
		}
#endif

		m_swfContext->Update(time,oneFrameOnly,updateInput);

#if __BANK && USE_OBJECT_EXPLORER
		if(mp_VarExplorer)
		{
			mp_VarExplorer->Movie2Bank();
		}
#endif

//		m_swfContext->RefreshRenderTargetViewports(); //we need to call this because the cameras and vp are set up in the update pass.
		if(m_bGarbageCollect)
		{
			this->GarbageCollect();
		}

		/*if (strcmp(m_szName, "TRANSITIONMOVIE") == 0)
		{
			int transIn, transOut, isReady;
			m_swfContext->GetGlobal("StartOfTransitionin", transIn);
			m_swfContext->GetGlobal("StartOfTransitionout", transOut);
			m_swfContext->GetGlobal("TransitionOutisReady", isReady);
			Displayf("transIn: %d, transOut: %d, isReady: %d", transIn, transOut, isReady);
		}*/
	}
}

#define SETUP_RENDERER 1
void FlashHelper::Draw()
{
	if(!m_bEnabled && !IsActive())
	{
		return;
	}

	if(this->m_bEnabled || IsActive())
	{
		//Render 2D Flash Movie:
#if SETUP_RENDERER
		grcViewport *old = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
#endif
		{
#if SETUP_RENDERER
			grcWorldIdentity();

			// temporary!!!!  Shouldn't need to setup lights - and if we do we should do this once (not in draw code)
			grcLightGroup lighting;
			lighting.SetAmbient(1.0f, 1.0f, 1.0f, 1.0f);
			lighting.SetActiveCount(1/*crashes if 0 - rage bug?*/);
			lighting.SetColor(0, 0.0f, 0.0f, 0.0f);

			grcState::Default();
			

			rage::grcState::SetLightingGroup(lighting);		// setting lights seems like a waste of time!!!  Not sure why the 2d shader does lighting calculations

			grcState::SetDepthTest(true);
			grcState::SetDepthWrite(true);
			grcState::SetAlphaBlend(true);
			grcState::SetAlphaTest(true);
			grcLightState::SetEnabled(false);
			grcState::SetCullMode(grccmNone);
			grcState::SetBlendSet(grcbsNormal);

			
#endif
			//draw HUD flash:
			this->m_swfContext->Draw();
		}
#if SETUP_RENDERER
		grcViewport::SetCurrent(old);
#endif		
	}
}

void FlashHelper::GarbageCollect()
{
	if(this->m_bEnabled)
	{
		this->m_swfContext->GarbageCollect();
	}
}

#endif //!__RESOURCECOMPILER

void FlashHelper::EnableApproved()
{
	m_bEnableRequested = false;

	this->SetCameraEnabled(true);

	m_bEnabled = true;

	m_swfContext->SetGlobal("cur_visibility",1);
}

//
// If we are going to hide, then go ahead and request hide.
// If we are going to show, and we require the render target, then just put in a request.
// If we are showing, and we don't use render target, then go ahead and show.
//
void FlashHelper::SetEnabled(bool value)
{
	if(!value)
	{
		m_bEnableRequested = false;
		m_bEnabled = value;
	}
/*	else if(m_swfContext->GetSelfRenderTarget())
	{
		rage::grcRenderTarget *pTarget = GetContext().GetSelfRenderTarget();

		FlashHelper* pHelper;
		
		pHelper = fuiManagerSingleton::GetInstance().FindMovie("PAUSEMOVIE");
		if(!m_bEnabled && pTarget && pHelper && pHelper->GetContext().GetSelfRenderTarget() == pTarget && pHelper->GetEnabled())
		{
			m_bEnableRequested = true;
			return;
		}

		pHelper = fuiManagerSingleton::GetInstance().FindMovie("NAVSYSMOVIE");
		if(!m_bEnabled && pTarget && pHelper && pHelper->GetContext().GetSelfRenderTarget() == pTarget && pHelper->GetEnabled())
		{
			m_bEnableRequested = true;
			return;
		}

		pHelper = fuiManagerSingleton::GetInstance().FindMovie("RE_MOVIE");
		if(!m_bEnabled && pTarget && pHelper && pHelper->GetContext().GetSelfRenderTarget() == pTarget && pHelper->GetEnabled())
		{
			m_bEnableRequested = true;
			return;
		}

		pHelper = fuiManagerSingleton::GetInstance().FindMovie("GARAGEMOVIE");
		if(!m_bEnabled && pTarget && pHelper && pHelper->GetContext().GetSelfRenderTarget() == pTarget && pHelper->GetEnabled())
		{
			m_bEnableRequested = true;
			return;
		}

		EnableApproved();
	} */
	else
	{
		EnableApproved();
	}
}


bool FlashHelper::IsTransitioning() const
{		
	return false;
}

void FlashHelper::FadeOut()
{
	m_bEnableRequested = false;
	m_swfContext->SetGlobal("cur_visibility",0);
}

void FlashHelper::SetCameraEnabled	(bool enabled)
{
	m_bCameraEnabled = enabled;
}

void FlashHelper::PlaceMovie(void *pStreamable, rage::datResourceMap &Map, void * pThis)
{
	GRAB_FLASH_UPDATE_SEMA();
	RAGE_TRACK(FUI_Flash);

#if STREAM_FLASH_CONTEXTS
	rage::pgStreamable<FlashFileAndContext> *movie = (rage::pgStreamable<FlashFileAndContext> *)pStreamable;
#else
	rage::pgStreamable<rage::swfFILE> *movie = (rage::pgStreamable<rage::swfFILE> *)pStreamable;
#endif

	FLASHMANAGER->PushSharedTextures();
	movie->Place(Map);
	FLASHMANAGER->PopSharedTextures();

	(reinterpret_cast<FlashHelper*>(pThis))->m_pStreamingFile = movie->GetObject();
	(reinterpret_cast<FlashHelper*>(pThis))->OnStreamComplete();
}

void FlashHelper::Stream()
{
	RAGE_TRACK(FUI_Flash);

	if (!IsStreamed())
	{
		if (m_pStreamingMovie->IsResident())
		{
			//it's already resident so no need to place
			SetStreamed(true);
		}
		m_pStreamingMovie->Schedule(UI_STREAMING_PRIORITY, PlaceMovie, (void *)this);
	}
}

void FlashHelper::OnStreamComplete()
{
	GRAB_FLASH_UPDATE_SEMA();

	RAGE_TRACK(FUI_Flash);

#if STREAM_FLASH_CONTEXTS
	m_pStreamingFile->m_Context->PostResourceFixup(*m_pStreamingFile->m_File);
	m_swfContext = m_pStreamingFile->m_Context;
#else

	if (GetContextPtr())
	{
		delete m_swfContext;
		m_swfContext = NULL;
	}
	if (!GetContextPtr())
	{
		//create the context if this is the first time this movie has been loaded
		m_swfContext = rage_new rage::swfCONTEXT(*m_pStreamingFile,
			(u16)m_FlashData.GetInstanceCount(),
			100,
			(u16)m_FlashData.GetSymbolCount(),
			(u16)m_FlashData.GetScriptObjectCount(),
			m_FlashData.GetStringHeapSize());
	}
	else
	{
		m_swfContext->SetFile(*m_pStreamingFile);
	}
#endif

	//this will allocate all the movies global vars
	Init(TIME.GetUnwarpedSeconds(),false);

	SetStreamed(true);
}

void FlashHelper::Load(int memoryBucket)
{
	GRAB_FLASH_UPDATE_SEMA();
	RAGE_TRACK(FUI_Flash);

#if __DEV
	size_t iBefore = sysMemAllocator::GetCurrent().GetMemoryAvailable();
	size_t iBlock = sysMemAllocator::GetCurrent().GetLargestAvailableBlock();
	size_t iUsed = sysMemAllocator::GetCurrent().GetMemoryUsed(-1);

	UIDebugf1("Loading movie %s, available mem: %d (block %d) (%d used)",GetName(),iBefore, iBlock, iUsed);
	UIDebugf1("Loading movie %s Available mem:\n   Game heap : %d (%d biggest block)",GetName(),
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetMemoryAvailable(),
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetLargestAvailableBlock() );
#endif

	FLASHMANAGER->PushSharedTextures();
	ASSET.PushFolder(fuiFlashManager::sm_szFlashResourceLocation);
	pgRscBuilder::Load(m_pStreamingFile, m_FlashData.GetName(), "#sf", swfResourceVersion, memoryBucket);
	ASSET.PopFolder();
	FLASHMANAGER->PopSharedTextures();

	RAGE_TRACK(FUI_Components);
	if (m_pStreamingFile)
	{
		OnStreamComplete();
	}

#if __DEV
	size_t iObject = sysMemAllocator::GetCurrent().GetMemoryAvailable();
	size_t iAfter = sysMemAllocator::GetCurrent().GetMemoryAvailable();
	size_t iDiff = iBefore - iAfter;
	UIDebugf1("Before loading : %d, After: %d, Diff: %d, Obj: %d",iBefore,iAfter,iDiff,iObject - iAfter);
	UIDebugf1("After loading movie Available mem:\n   Game heap : %d (%d biggest block)",
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetMemoryAvailable(),
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetLargestAvailableBlock() );
#endif
}


