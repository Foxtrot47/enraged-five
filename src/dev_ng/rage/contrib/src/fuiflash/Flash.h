// 
// fuicore/Flash.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FLASHUICORE_FLASH_H
#define FLASHUICORE_FLASH_H

#include "system/param.h"
#include "flash/input.h"
#include "flash/misc.h"
#include "flash/file.h"
#include "atl/map.h"
#include "atl/inmap.h"
#include "atl/singleton.h"
#include "atl/string.h"
#include "atl/dlist.h"
#include "paging/rscbuilder.h"
#include "string/unicode.h"
#include "paging/dictionary.h"
#include "paging/streamable.h"
#include "grcore/viewport.h"
#include "fuicore/Common.h"

#if __SPU
#define SIZEOF_PGSTREAMERINFO  16

namespace rage
{
	struct pgStreamerInfo
	{
		u8	pad[SIZEOF_PGSTREAMERINFO];
	};
}
#endif

namespace rage {

#define STREAM_FLASH_CONTEXTS 0
#define MAX_MOVIES 32

class fuiFlashManager;
class FlashHelper;
class fuiRegistry;
class fuiVariant;
class swfObjectExplorer;

typedef rage::atDList<FlashHelper*>	FlashHelperList;
typedef rage::atDNode<FlashHelper*>	FlashHelperNode;

struct MovieStreamRequest
{
	MovieStreamRequest(){}

#if STREAM_FLASH_CONTEXTS
	MovieStreamRequest(const char *id, const char *pPath,bool prepend, bool garbageCollect) :
	m_bPrepend(prepend), m_bGarbageCollect(garbageCollect)
	{ strcpy(this->identifier,id); strcpy(this->path,pPath); }
#else
	MovieStreamRequest(const char *id, const char *pPath,u16 instanceCount, u16 swfScriptObjectCount, int stringHeapSize, bool prepend, bool garbageCollect) :
	m_iInstanceCount(instanceCount),
		m_swfScriptObjectCount(swfScriptObjectCount), m_iStringHeapSize(stringHeapSize),
		m_bPrepend(prepend), m_bGarbageCollect(garbageCollect)
	{ strcpy(this->identifier,id); strcpy(this->path,pPath); }
#endif

	MovieStreamRequest(const MovieStreamRequest& source)
	{
		strcpy(this->identifier,source.identifier);
		strcpy(this->path,source.path);
#if STREAM_FLASH_CONTEXTS
		m_fileAndContext = source.m_fileAndContext;
#else
		m_iInstanceCount = source.m_iInstanceCount;
		m_swfScriptObjectCount = source.m_swfScriptObjectCount;
		m_iStringHeapSize = source.m_iStringHeapSize;
		m_file = source.m_file;
#endif
		m_bPrepend = source.m_bPrepend;
		m_bGarbageCollect = source.m_bGarbageCollect;
	}

	char			identifier[16];
	char			path[64];
#if STREAM_FLASH_CONTEXTS
	FlashFileAndContext* m_fileAndContext;
#else
	u16		m_iInstanceCount, m_swfScriptObjectCount;
	int				m_iStringHeapSize;
	swfFILE*	m_file;
#endif
	bool			m_bPrepend;
	bool			m_bGarbageCollect;
};
class bkGroup;

// brought over from Flash3d.h (which doesn't exist in RAGE)
struct FlashFileAndContext : public pgBase
{
	datOwner<swfFILE>			m_File;
	datOwner<swfCONTEXT>		m_Context;

	FlashFileAndContext() : m_File(NULL), m_Context(NULL) {}
	FlashFileAndContext(datResource& rsc) : m_File(rsc), m_Context(NULL) {} // can't resource contexts in this cut of rage
	~FlashFileAndContext();
	DECLARE_PLACE(FlashFileAndContext);
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif
	int Release() { delete this; return 0; } //adding for paging/streamable

};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiMovieData
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class fuiMovieData
{
public:
	fuiMovieData();

	// PAR_PARSABLE adds a virtual function, so we need a virtual destructor
	virtual ~fuiMovieData() {}

	const char*	GetFileName() const							{ return m_FileName; }
	const char*	GetName() const								{ return m_Name; }
	int			GetInstanceCount() const					{ return m_InstanceCount; }
	int			GetSymbolCount() const						{ return m_SymbolCount; }
	int			GetScriptObjectCount() const				{ return m_ScriptObjectCount; }
	int			GetStringHeapSize() const					{ return m_StringHeapSize; }
	bool		ClampTextures() const						{ return m_ClampTextures; }
	void		SetFileName(const char* name);
	void		SetName(const char* name){safecpy(m_Name,name,kMaxNameLength);}
	void		SetInstanceCount(int instanceCount)			{ m_InstanceCount = instanceCount; } 
	void		SetSymbolCount(int symbolCount)				{ m_SymbolCount = symbolCount; } 
	void		SetScriptObjectCount(int scriptObjectCount)	{ m_ScriptObjectCount = scriptObjectCount; } 
	void		SetStringHeapSize(int stringHeapSize)		{ m_StringHeapSize = stringHeapSize; } 
	void		SetClampTextures(bool clampTextures)		{ m_ClampTextures = clampTextures; } 
protected:
	enum { kMaxNameLength = 32 };
	char	m_FileName[kMaxNameLength];
	char	m_Name[kMaxNameLength];
	char	m_PathAndFilename[kMaxNameLength];
	int		m_InstanceCount;
	int		m_SymbolCount;
	int		m_ScriptObjectCount;
	int		m_StringHeapSize;
	bool	m_ClampTextures;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DEPRECATED BELOW

	template <int L>
	class FixedString
	{
		char szStr[L];
	public:
		FixedString()
		{
			this->szStr[0] = '\0';
		}
		FixedString(const char *value)
		{
			strncpy(this->szStr,value,L);
		}
		virtual ~FixedString(){}

		FixedString& operator=(const FixedString& s) {
			strncpy(this->szStr,s.szStr,L);

			return *this;
		}
		FixedString& operator=(const char *s) {
			strncpy(this->szStr,s,L);

			return *this;
		}
		operator const char * () const {
			return this->szStr;
		}
		bool operator<=(const char * s) const { return strcmp(*this,s) <= 0; }
		bool operator<(const char * s) const { return strcmp(*this,s) < 0; }
		bool operator>(const char * s) const { return strcmp(*this,s) > 0; }
		bool operator>=(const char * s) const { return strcmp(*this,s) >= 0; }
		bool operator==(const char * s) const { return strcmp(*this,s) == 0; }
	};

	class fuiVariant
	{
	public:
		inmap_node< FixedString<64>, fuiVariant > m_MapA;
	private:
		union
		{
			float f;
			int i;
		};
		char szStr[256]; //TODO: consider switching to atString
	public:		
		u16 Type; // valid types are AS_STRING, AS_INT, AS_FLOAT (and sometimes AS_BOOL)
		bool Dirty;

		fuiVariant();
		fuiVariant(const char * value);
		fuiVariant(int value);
		fuiVariant(float value);
		fuiVariant(bool value);
		virtual ~fuiVariant(){}

		virtual fuiVariant& operator=(const fuiVariant& src)
		{
			this->Set(src);

			return *this;
		}
		virtual const char * operator=(const char * src)
		{
			this->Set(src);

			return (const char*)this->szStr;
		}
		bool operator <(const fuiVariant& src)
		{
			Assertf(src.Type == this->Type,"I am type %d, and other is type %d",this->Type,src.Type);

			switch(this->Type)
			{
			case AS_STRING:
				{
					return stricmp(this->asString(),src.asString()) < 0;
				}
			case AS_FLOAT:
				{
					return this->asFloat() < src.asFloat();
				}
			case AS_INT:
				{
					return this->asInt() < src.asInt();
				}
			default:
				{
					AssertMsg(0,"unsupported type");
					return false;
				}
			}
		}
		bool operator <=(const fuiVariant& src)
		{
			Assertf(src.Type == this->Type,"I am type %d, and other is type %d",this->Type,src.Type);

			switch(this->Type)
			{
			case AS_STRING:
				{
					return stricmp(this->asString(),src.asString()) <= 0;
				}
			case AS_FLOAT:
				{
					return this->asFloat() <= src.asFloat();
				}
			case AS_INT:
				{
					return this->asInt() <= src.asInt();
				}
			default:
				{
					AssertMsg(0,"unsupported type");
					return false;
				}
			}
		}
		bool operator > (const fuiVariant& src)
		{
			Assertf(src.Type == this->Type,"I am type %d, and other is type %d",this->Type,src.Type);

			switch(this->Type)
			{
			case AS_STRING:
				{
					return stricmp(this->asString(),src.asString()) > 0;
				}
			case AS_FLOAT:
				{
					return this->asFloat() > src.asFloat();
				}
			case AS_INT:
				{
					return this->asInt() > src.asInt();
				}
			default:
				{
					AssertMsg(0,"unsupported type");
					return false;
				}
			}
		}
		bool operator >= (const fuiVariant& src)
		{
			Assertf(src.Type == this->Type,"I am type %d, and other is type %d",this->Type,src.Type);

			switch(this->Type)
			{
			case AS_STRING:
				{
					return stricmp(this->asString(),src.asString()) >= 0;
				}
			case AS_FLOAT:
				{
					return this->asFloat() >= src.asFloat();
				}
			case AS_INT:
				{
					return this->asInt() >= src.asInt();
				}
			default:
				{
					AssertMsg(0,"unsupported type");
					return false;
				}
			}
		}
		bool operator == (const fuiVariant& src)
		{
			Assertf(src.Type == this->Type,"I am type %d, and other is type %d",this->Type,src.Type);

			switch(this->Type)
			{
			case AS_STRING:
				{
					return stricmp(this->asString(),src.asString()) == 0;
				}
			case AS_FLOAT:
				{
					return this->asFloat() == src.asFloat();
				}
			case AS_INT:
				{
					return this->asInt() == src.asInt();
				}
			default:
				{
					AssertMsg(0,"unsupported type");
					return false;
				}
			}
		}
		void Set(const fuiVariant& src)
		{
			Dirty = true;
			strncpy(this->szStr,src.szStr,sizeof(this->szStr));
			this->Type = src.Type;
			this->i = src.i;
		}
		void Set(const swfVARIANT& value)
		{
			this->Type = (u16)value.Type;
			switch(value.Type)
			{
			case AS_CONSTSTRING:
				strncpy(this->szStr,value.asConstString(),sizeof(this->szStr));
				break;
			case AS_INT:
				this->i = value.asInt();
				break;
			case AS_FLOAT:
				this->f = value.asFloat();
				break;
			case AS_BOOL:
				this->i = value.asBool();
				break;
			default:
				{
					Assertf(0,"Unsupported swfVARIANT type %d",value.Type);
				}
			}
		}
		void Set(const char * value)
		{
			Dirty = true;
			if (strlen(value) > sizeof(this->szStr))UIWarningf("String \"%s\" is longer than %d characters",value, (int)sizeof(this->szStr));
			safecpy(this->szStr,value);
			Type = AS_STRING;
		}
		void Set(int value)
		{
			Dirty = true;
			i = value;
			sprintf(this->szStr,"%d",i);
			Type = AS_INT;
		}
		void Set(float value)
		{
			Dirty = true;
			sprintf(this->szStr,"%f",value);
			f = value;
			Type = AS_FLOAT;
		}
		void Set(bool value)
		{
			Dirty = true;
			i = (int)value;
			if(value)
			{
				strcpy(this->szStr,"1");
			}
			else
			{
				strcpy(this->szStr,"0");
			}
			Type = AS_BOOL;
		}
		void Get(swfVARIANT &var)
		{
			switch(this->Type)
			{
			case AS_STRING:
				{
					var.Set(asString());
					break;
				}
			case AS_FLOAT:
				{
					var.Set(this->f);
					break;
				}
			case AS_INT:
				{
					var.Set(this->i);
					break;
				}
			case AS_BOOL:
				{
					var.Set(i!=0);
					break;
				}
			default:
				{
					AssertMsg(0,"unsupported type");
				}
			}
		}
		void Get(bool &value)
		{
			value = asBool();
		}
		void Get(int &value)
		{
			value = asInt();
		}
		void Get(float &value)
		{
			value = asFloat();
		}
		void Get(const char* &value)
		{
			value = asString();
		}
		const char * asString() const { return this->szStr; }  //TODO: if fuiVariant gets reallocated while flash is referencing it as const str, things will blow up.  So don't reallocate fuiVariants!
		int asInt() const {
			switch(this->Type)
			{
			case AS_FLOAT:
				return (int)f;
			case AS_STRING:
				return atoi(szStr);
			case AS_INT:
			case AS_BOOL:
				return i;
			default:
				Quitf("Unknown fuiVariant type in asInt");
				return -1;
			}
		}
		float asFloat() const
		{
			switch(this->Type)
			{
			case AS_FLOAT:
				return f;
			case AS_STRING:
				return (float)atof(szStr);
			case AS_INT:
			case AS_BOOL:
				return (float)i;
			default:
				Quitf("Unknown fuiVariant type in asFloat");
				return -1.0;
			}
		}
		bool asBool() const { return i != 0; }
	};

	typedef inmap< FixedString<64>, fuiVariant, &fuiVariant::m_MapA > registryMap;

	class fuiRegistry
	{
	public:
		fuiRegistry(int maxVariants = 64) : m_MaxVariants(maxVariants), m_pool(rage_new fuiVariant[maxVariants]), 
		  m_iPoolSize(0)
		  {
			  if(s_pRegistryInstance==NULL) // first one gets it
				s_pRegistryInstance = this;

			  this->m_pMap = rage_new registryMap();
		  }
		  virtual ~fuiRegistry() { Reset(); delete this->m_pMap; delete[] m_pool; }

		  registryMap* GetMap() { return m_pMap; }

		  int GetCount() const { return this->m_iPoolSize; }

		  void Reset()
		  {
			  m_iPoolSize = 0;
			  m_pMap->clear();
		  }

		  fuiVariant* Access(const char *key)
		  {
			  registryMap::iterator it = this->m_pMap->find( key );

			  if(this->m_pMap->end() == it)
			  {
				  return NULL;
			  }
			  else
			  {
				  return it->second;
			  }
		  }

		  void Insert(const char *key, fuiVariant *value)
		  {
			  this->m_pMap->insert( registryMap::value_type( key,value ) );
		  }

		  //Copies the string.
		  const char* SetValue(const char *key, const char *value)
		  {
			  fuiVariant *variant = this->AddIfMissing(key,value);
			  if(!variant)
				  return NULL;

			  variant->Set((char*)value);

			  return variant->asString();
		  }
		  fuiVariant& SetValue(const char *key, float value)
		  {
			  fuiVariant *variant = this->AddIfMissing(key,value);
			  if(variant) // yuck! we're still going to return this!
			  variant->Set(value);

			  return *variant;
		  }
		  fuiVariant& SetValue(const char *key, int value)
		  {
			  fuiVariant *variant = this->AddIfMissing(key,value);
			  if(variant) // yuck! we're still going to return this!
			  variant->Set(value);

			  return *variant;
		  }
		  fuiVariant& SetValue(const char *key, const swfVARIANT &value)
		  {
			  fuiVariant *variant = this->AddIfMissing(key,0);
			  if(variant) // yuck! we're still going to return this!
				  variant->Set(value);

			  return *variant;
		  }
		  fuiVariant& SetValue(const char *key, bool value)
		  {
			  fuiVariant *variant = this->AddIfMissing(key,value);
			  if(variant) // yuck! we're still going to return this!
			  variant->Set(value);

			  return *variant;
		  }
		  fuiVariant& GetValue(const char *key)
		  {
			  fuiVariant *pVariant = this->Access(key);
			  //Assertf(pVariant,"Couldn't find value \"%s\" in registry",key);
			  return *pVariant;
		  }

		  static fuiRegistry& GetInstance() { return *s_pRegistryInstance; }

		  //scripting interface:
		  static void RegisterScriptCommands();

		  static void SetValueFloat(const char * key, float value)
		  {
			  AssertMsg(s_pRegistryInstance , "fuiRegistry not created.");

			  s_pRegistryInstance->SetValue(key, value);
		  }

		  static void SetValueInt(const char * key, int value)
		  {
			  AssertMsg(s_pRegistryInstance , "fuiRegistry not created.");

			  s_pRegistryInstance->SetValue(key, value);
		  }

		  static const char * SetValueString(const char * key,const char *value)
		  {
			  AssertMsg(s_pRegistryInstance , "fuiRegistry not created.");

			  return s_pRegistryInstance->SetValue(key,value);
		  }

		  static void SetValueBool(const char * key,bool value)
		  {
			  AssertMsg(s_pRegistryInstance , "fuiRegistry not created.");
			  s_pRegistryInstance->SetValue(key,value);
		  }

		  //static void GetValueString(const char * key, const scrTextLabel63 output64	/* must be 64 chars or more*/)
		  //{
			 // Assert(s_pRegistryInstance && "fuiRegistry not created.");

			 // fuiVariant& rVar = s_pRegistryInstance->GetValue(key);
			 // const char *pVal = rVar.asString();

			 // strncpy((char*)&output64[0],pVal,63); //TODO: is 63 the right limit?
		  //}

		  static const char * GetValueConstString(const char *key)
		  {
			  AssertMsg(s_pRegistryInstance , "fuiRegistry not created.");

			  fuiVariant& rVar = s_pRegistryInstance->GetValue(key);
			  return rVar.asString();
		  }

		  static void GetValueInt(const char * key, int& output)
		  {
			  AssertMsg(s_pRegistryInstance , "fuiRegistry not created.");

			  fuiVariant& rVar = s_pRegistryInstance->GetValue(key);
			  if(NULL == &rVar)
			  {
				  output = 0;
			  }
			  else
			  {
				  output = rVar.asInt();
			  }
		  }

		  static void GetValueFloat(const char * key, float& output)
		  {
			  AssertMsg(s_pRegistryInstance , "fuiRegistry not created.");

			  fuiVariant& rVar = s_pRegistryInstance->GetValue(key);
			  if(NULL == &rVar)
			  {
				  output = 0.0f;
			  }
			  else
			  {
				  output = rVar.asFloat();
			  }
		  }

		  static bool GetValueBool(const char * key )
		  {
			  AssertMsg(s_pRegistryInstance , "fuiRegistry not created.");

			  fuiVariant& rVar = s_pRegistryInstance->GetValue(key);
			  if( &rVar )
			  {
				  return rVar.asBool();
			  }

			  return false;
		  }

	protected:
		fuiVariant* AddIfMissing(const char *key, const char *value)
		{
			fuiVariant* pVariant = this->Access(key);
			if(pVariant)
			{
				return pVariant;
			}
			else
			{
				Assert(this->m_iPoolSize < m_MaxVariants);
				if(this->m_iPoolSize >= m_MaxVariants)
				{
					UIWarningf("[fuiVariant] discarding %s",key);
					return NULL;
				}
				fuiVariant& rVariant = this->m_pool[this->m_iPoolSize];
				this->m_iPoolSize++;
				rVariant.Set((char*)value);

				this->Insert(key,&rVariant);
#if __DEV
				pVariant = this->Access(key);
				AssertMsg(pVariant , "insertion was expected to be successful at all times.");
#endif

				return &rVariant;
			}
		}
		fuiVariant* AddIfMissing(const char *key, float value)
		{
			fuiVariant* pVariant = this->Access(key);
			if(pVariant)
			{
				return pVariant;
			}
			else
			{
				Assert(this->m_iPoolSize < m_MaxVariants);
				if(this->m_iPoolSize >= m_MaxVariants)
				{
					UIWarningf("[fuiVariant] discarding %s",key);
					return NULL;
				}
				fuiVariant& rVariant = this->m_pool[this->m_iPoolSize];
				this->m_iPoolSize++;
				rVariant.Set(value);

				this->Insert(key,&rVariant);
#if __DEV
				pVariant = this->Access(key);
				AssertMsg(pVariant , "insertion was expected to be successful at all times.");
#endif

				return &rVariant;
			}
		}
		fuiVariant* AddIfMissing(const char *key, int value)
		{
			fuiVariant* pVariant = this->Access(key);
			if(pVariant)
			{
				return pVariant;
			}
			else
			{
				Assert(this->m_iPoolSize < m_MaxVariants);
				if(this->m_iPoolSize >= m_MaxVariants)
				{
					UIWarningf("[fuiVariant] discarding %s",key);
					return NULL;
				}
				fuiVariant& rVariant = this->m_pool[this->m_iPoolSize];
				this->m_iPoolSize++;
				rVariant.Set(value);

				this->Insert(key,&rVariant);
#if __DEV
				pVariant = this->Access(key);
				AssertMsg(pVariant , "insertion was expected to be successful at all times.");
#endif

				return &rVariant;
			}
		}
		fuiVariant* AddIfMissing(const char *key, bool value)
		{
			fuiVariant* pVariant = this->Access(key);
			if(pVariant)
			{
				return pVariant;
			}
			else
			{
				Assert(this->m_iPoolSize < m_MaxVariants);
				if(this->m_iPoolSize >= m_MaxVariants)
				{
					UIWarningf("[fuiVariant] discarding %s",key);
					return NULL;
				}
				fuiVariant& rVariant = this->m_pool[this->m_iPoolSize];
				this->m_iPoolSize++;
				rVariant.Set(value);

				this->Insert(key,&rVariant);
#if __DEV
				pVariant = this->Access(key);
				AssertMsg(pVariant , "insertion was expected to be successful at all times.");
#endif

				return &rVariant;
			}
		}
	private:
		static fuiRegistry	*s_pRegistryInstance; //TODO: make improved interface to scripting interface that doesn't use this hacky static instance
		int					m_MaxVariants;
		fuiVariant			*m_pool;
		int					m_iPoolSize;

		registryMap			*m_pMap;
	};

	class FlashHelper : public datBase
	{
	public:

	protected:
		//save movie paths so that we can reload them from the bank:
	private:
		char								m_szName[32]; //TODO: unhardcode
#if __BANK
		char								m_szMoviePath[32]; //TODO: unhardcode
#endif
	public:
		void								SetName(const char *value) { Assert(strlen(value) < sizeof(m_szName)); strncpy(m_szName,value,sizeof(m_szName) - 1); }
		const char*							GetName() const { return m_FlashData.GetName(); }
#if __BANK
		void								SetMoviePath(const char *value) { Assert(strlen(value) < sizeof(m_szMoviePath)); strncpy(m_szMoviePath,value,sizeof(m_szMoviePath)); }
		const char*							GetMoviePath() const { return m_szMoviePath; }	
		swfObjectExplorer					*mp_VarExplorer;
		void OutputFlashVariables();
		void AddWidgets(bkBank& );
		void RemoveWidgets(bkBank& b);
#endif

#if !__RESOURCECOMPILER
	public:
		void Draw3D(int bucket);
		void DrawSelfPass();
		void Draw();
		void DrawOverlay(grcRenderTarget *rt);
#else
		void Draw(){}
#endif

	public:
		FlashHelper(const char *szMoviePath, fuiRegistry *globalDictionary, u16 instanceCount, u16 swfScriptObjectCount, int stringHeapSize);
		FlashHelper(int memoryBucket, const char* pName, const char* pFilename, int instanceCount, int symbolCount, int scriptObjectCount, int stringHeapSize);

		//
		// Use this if you stream in movie separately.  Pass false for owner if someone else is in charge of deleting the swfFILE object.
		//
		FlashHelper(swfFILE& file, fuiRegistry *globalDictionary, u16 instanceCount, u16 swfScriptObjectCount, int stringHeapSize, bool owner = true);
		FlashHelper(FlashFileAndContext& file, fuiRegistry *globalDictionary, bool owner = true, int memoryBucket=0);
		virtual FlashHelper& operator = (const FlashHelper&) { Quitf("If you get this error, the FlashHelper = operator needs to be implemented."); return *this; }
		virtual ~FlashHelper();

		void Init(float dt, bool loadtime); //sorry, this is necessary because of the bug in the flash player.  Call after the constructor.
		static void PlaceMovie(void *pStreamable, rage::datResourceMap &Map, void * pThis);
		void ForceLoad();
		void Stream();
		void OnStreamComplete();

		//only does anything if the object is enabled:
#if !__RESOURCECOMPILER
		void Update(float time, bool oneFrameOnly=false, bool updateInput=true);
		void GarbageCollect();
#else
		void Update(float, bool, bool){}
#endif

		static swfFILE* Stream(const char *moviePath); //returns NULL if failed
		void Unstream(/*datResourceInfo hdr*/);

		// Instead of the single Stream function above, you can break it into two parts,
		// BeginStream which can be called outside of the flash semaphore and
		// EndStream which needs to be called from inside the flash semaphore
#if !__SPU
		static bool BeginStream(const char *moviePath, pgStreamerInfo &outInfo, datResourceMap &map, int ); //returns false if failed
		static swfFILE* EndStream(const char *szMoviePath, pgStreamerInfo &outInfo, datResourceMap &map); //returns NULL if failed
#endif

		// Same as above but load up FlashFileAndContext objects instead of swfFILE objects
		static FlashFileAndContext* FFCStream(const char *moviePath); //returns NULL if failed
#if !__SPU
		static bool BeginFFCStream(const char *moviePath, pgStreamerInfo &outInfo, datResourceMap &map); //returns false if failed
		static FlashFileAndContext* EndFFCStream(const char *szMoviePath, pgStreamerInfo &outInfo, datResourceMap &map); //returns NULL if failed
#endif

		//accessors and mutators:
		swfCONTEXT&			GetContext			()			const	{ return *this->m_swfContext;							}
		swfCONTEXT*			GetContextPtr		()			const	{ return this->m_swfContext;							}
		void				SetContext			(swfCONTEXT*context, bool own=true)	{ Assert(!m_swfContext); m_swfContext = context; m_bOwnsContext = own; }
		void				SetFile				(swfFILE*file, bool own=true)		{ Assert(!m_pSwfFile); m_pSwfFile = file; m_bOwnsFile = own; }
		inline fuiRegistry&	GetRegistry			()					{ Assert(m_pGlobalDictionary); return *this->m_pGlobalDictionary; 					}
		void 				SetEnabled			(bool value);
		inline bool 		GetEnabled			()			const	{ return this->m_bEnabled;								}
		void				NotifyTransition	(int level)			{ GetContextPtr()->SetGlobal("TransitionLevel", level );}
		bool				IsTransitioning		()			const	;
		swfFILE*			GetFile				()					{ return m_pStreamingFile; }
		void				OnClosed			()					;
		void Load(int memoryBucket);

		//
		// Initiates fade out.  call this instead of setting cur_visibility manually.
		// Compliant movies will disable themselves once they are completely transparent.
		//
		void					FadeOut				();

		//
		// If you don't want fuiFlashManager/fuiManager to update this movie in the update pass automatically,
		// but you want to update this at your own defined time, set manual update to true.
		//
		inline bool				IsManualUpdate		()			const	{ return m_bManualUpdate;			}
		void					SetManualUpdate		(bool manualUpdate)	{ m_bManualUpdate = manualUpdate;	}

		inline bool				IsCameraEnabled		()			const	{ return m_bCameraEnabled; }
		void					SetCameraEnabled	(bool enabled);

		inline bool				IsGarbageCollect	()			const	{ return m_bGarbageCollect; }
		void					SetGarbageCollect	(bool gc)			{ m_bGarbageCollect = gc; }
		bool IsVisible(){return (m_IsVisibleResource != 0);}
		void OnShow();
		void OnHide();
		virtual void Activate(){m_bIsActive = true;}
		virtual void Deactivate(){m_bIsActive = false;}
		virtual bool IsActive(){return m_bIsActive;}
		void SetStreamed(bool b){m_bIsStreamed = b;}
		bool IsStreamed(){return m_bIsStreamed;}

		// script interface:
		static void RegisterScriptCommands();

		static void SetMovieEnabled(FlashHelper *pFlash, bool value) { pFlash->SetEnabled(value); }

//		static bool GetGlobalString(FlashHelper * pFlash, const char * id, const scrTextLabel63 output64	/* must be 64 chars or more*/);
		static bool GetGlobalInt(FlashHelper * pFlash, const char * id, int & output);
		static void SetGlobalString(FlashHelper * pFlash, const char * id, const char * value	/* must be 64 chars or more*/);
		static void SetGlobalInt(FlashHelper * pFlash, const char * id, int value);
		static void SetGlobalStringArray(FlashHelper *pFlash, const char *id, int index, const char *value	/* must be 64 chars or more*/);
		static void SetGlobalIntArray(FlashHelper *pFlash, const char *id, int index, int value);
		static void SetGlobalFloat(FlashHelper * pFlash, const char * id, f32 value);
		static void SetGlobalFloatArray(FlashHelper *pFlash, const char *id, int index, f32 value);

		//Helper functions only for your convenience:
		static bool GetProperty(const swfVARIANT& variant, const char *propertyName, swfVARIANT &property);
		static bool GetProperty(const swfVARIANT& variant, const char *propertyName, char *out);
		static bool GetProperty(const swfVARIANT& variant, const char *propertyName, int& out);
		static bool GetProperty(const swfVARIANT& variant, const char *propertyName, float& out);
		static bool GetProperty(const swfVARIANT& variant, const char *propertyName, Vector3& out);
		static bool GetProperty(const swfVARIANT& variant, const char *propertyName, Vector4& out);
		static void SetProperty(swfVARIANT& variant, const char *propertyName, const char *value, swfCONTEXT* context);
		static void SetProperty(swfVARIANT& variant, const char *propertyName, const char16 *value, swfCONTEXT* context);
		static void SetProperty(swfVARIANT& variant, const char *propertyName, char16 *value, swfCONTEXT* context);
		static void SetProperty(swfVARIANT& variant, const char *propertyName, int value);
		static void SetProperty(swfVARIANT& variant, const char *propertyName, float value);
		static void SetProperty(swfVARIANT& variant, const char *propertyName, const Vector3& value);
		static int GetArraySize(swfCONTEXT& context, const char *arrayName);
		static swfSCRIPTARRAY& GetArray(swfCONTEXT& context, const char *arrayName);
	protected:
		friend class fuiFlashManager;

		void EnableApproved();
		bool GetIsEnableRequested() const { return m_bEnableRequested; }
		const fuiMovieData* GetFlashData() const { return &m_FlashData; }
		//const char* GetName() const { return m_FlashData.GetName(); }
		const char* GetPath() const { return m_FlashData.GetFileName(); }		

	protected:
	#define SIZEOF_PGSTREAMERINFO  20
#if !__SPU
		pgStreamerInfo info;
		CompileTimeAssert( sizeof(pgStreamerInfo) == SIZEOF_PGSTREAMERINFO);
#else
		u8		info[SIZEOF_PGSTREAMERINFO];
#endif

		swfFILE						*m_pSwfFile;
		swfCONTEXT						*m_swfContext;

		// For resourced loads
		FlashFileAndContext					*m_pFileAndContext;

		fuiRegistry*							m_pGlobalDictionary;
		bool								m_bEnabled;
		bool								m_bManualUpdate;
		bool								m_bCameraEnabled;

		bool								m_bGarbageCollect;

		bool								m_bOwnsFile;
		bool								m_bOwnsContext;

		//for movies that share the self-render target, requests to enable the movie are "queued"
		bool								m_bEnableRequested;
		static atPool<swfSTRING>		sm_StringPool;
		u8 m_IsVisibleResource;//number of components that are using this movie currently.  when this is zero, then the movie should not be drawn/updated
		bool m_bIsActive;
		fuiMovieData	m_FlashData;
		bool m_bIsStreamed;

#if STREAM_FLASH_CONTEXTS
		pgStreamable<FlashFileAndContext>* GetStreamable(){return m_pStreamingMovie;}
#else
		pgStreamable<swfFILE>* GetStreamable(){return m_pStreamingMovie;}
#endif

		//streamable objects
#if STREAM_FLASH_CONTEXTS
		pgStreamable<FlashFileAndContext>* m_pStreamingMovie;
public:
	FlashFileAndContext* m_pStreamingFile;
#else
		pgStreamable<swfFILE>* m_pStreamingMovie;
public:
	swfFILE* m_pStreamingFile;
#endif
	};
//deprecated above
////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiMovie
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class fuiMovie : public datBase
{
public:
	// Constructor / Destructor
	fuiMovie();
	virtual ~fuiMovie();
	void Init(float dt); //sorry, this is necessary because of the bug in the flash player.

	// Lifecycle funcs
	virtual void Update();
	virtual void Reset();
	virtual void Draw(const grcViewport* pGameVP);
	virtual void OnShow();
	virtual void OnHide();
	
	virtual bool IsTransitioning()	const	{ return m_TransitionState>=0;		}
	virtual void SetVisible		(bool b)	{ if(b) OnShow(); else OnHide();	}
	virtual bool IsVisible		()	const	{ return m_IsVisibleResource>0 || IsTransitioning(); }

	virtual void Init(int memoryBucket);

	//loading/streaming
	virtual void Load(int memoryBucket=0);
	virtual void Unload();
	virtual void Unstream();
	void MarkForUnload() { if(m_LoadRequested) m_LoadRequested = false; else m_UnloadCounter = 30; }
	bool ReadyForUnload() { return m_UnloadCounter?(!--m_UnloadCounter):false; }
	void RequestLoad() { if(m_UnloadCounter) m_UnloadCounter=0; else { m_LoadRequested = true; } }
	bool LoadRequested() { return m_LoadRequested!=0; }
	void OnLoadComplete(){}
	void OnStreamComplete();
	virtual bool IsLoaded(){return m_bIsLoaded;}
	virtual void SetLoaded(bool b){m_bIsLoaded = b;if (b){OnLoadComplete();}}
	virtual bool IsStreamed(){return m_bIsStreamed;}
	virtual void SetStreamed(bool b){m_bIsStreamed = b; if (b) {OnStreamComplete();}}

	static void PlaceMovie(void *pStreamable, rage::datResourceMap &Map, void * pThis);
	virtual void Stream();

	//misc
	void FadeOut();

	struct someString
	{
		char someStringMem[256];
	};

	//accessors and mutators:
	const fuiMovieData &GetFlashData() const { return m_FlashData; }
	const char* GetName() const { return m_FlashData.GetName(); }
	const char* GetPath() const { return m_FlashData.GetFileName(); }
	swfCONTEXT* GetContext() const{return m_pContext;}
	swfVARIANT* GetVariant(const char* propertyPathAndName);
	swfVARIANT* GetVariant(const char* value ,swfVARIANT* property);
	swfVARIANT* GetVariantSimple(const char* value,swfSCRIPTOBJECT* property);
	static fuiMovie* GetMovie(const char*);
	
#if STREAM_FLASH_CONTEXTS
	pgStreamable<FlashFileAndContext>* GetStreamable(){return m_pStreamingMovie;}
#else
	pgStreamable<swfFILE>* GetStreamable(){return m_pStreamingMovie;}
#endif
	//virtual swfFILE* GetFile(){return GetContext()?GetContext()->GetFile():NULL}

	//access functions for your convenience
//	static bool GetGlobalString(fuiMovie * pFlash, const char * id, const scrTextLabel63 output64	/* must be 64 chars or more*/);
	static bool GetGlobalInt(fuiMovie * pFlash, const char * id, int & output);
	static void SetGlobalString(fuiMovie * pFlash, const char * id, const char * value	/* must be 64 chars or more*/);
	static void SetGlobalInt(fuiMovie * pFlash, const char * id, int value);
	static void SetGlobalStringArray(fuiMovie *pFlash, const char *id, int index, const char *value	/* must be 64 chars or more*/);
	static void SetGlobalIntArray(fuiMovie *pFlash, const char *id, int index, int value);
	static void SetGlobalFloat(fuiMovie * pFlash, const char * id, f32 value);
	static void SetGlobalFloatArray(fuiMovie *pFlash, const char *id, int index, f32 value);

	//Helper functions only for your convenience:
	static bool GetProperty(const swfVARIANT& variant, const char *propertyName, swfVARIANT &property);
	static bool GetProperty(const swfVARIANT& variant, const char *propertyName, char *out);
	static bool GetProperty(const swfVARIANT& variant, const char *propertyName, int& out);
	static bool GetProperty(const swfVARIANT& variant, const char *propertyName, float& out);
	static bool GetProperty(const swfVARIANT& variant, const char *propertyName, Vector3& out);
	static bool GetProperty(const swfVARIANT& variant, const char *propertyName, Vector4& out);
	static void SetProperty(swfVARIANT& variant, const char *propertyName, const char *value, swfCONTEXT* context);
	static void SetProperty(swfVARIANT& variant, const char *propertyName, const char16 *value, swfCONTEXT* context);
	static void SetProperty(swfVARIANT& variant, const char *propertyName, char16 *value, swfCONTEXT* context);
	static void SetProperty(swfVARIANT& variant, const char *propertyName, int value);
	static void SetProperty(swfVARIANT& variant, const char *propertyName, float value);
	static void SetProperty(swfVARIANT& variant, const char *propertyName, const Vector3& value);
	static int GetArraySize(swfCONTEXT& context, const char *arrayName);
	static swfSCRIPTARRAY& GetArray(swfCONTEXT& context, const char *arrayName);

	//for the following SetX functions, you can pass a path as the variant id.  i.e. menu.elements[3].text. The logic 
	//will parse and find that variant
	template< typename T>
	void SetVariable(const char *propertyPathAndName, T value )
	{
		swfVARIANT* pVariant = GetVariant(propertyPathAndName);
		if (pVariant)
		{
			pVariant->Set(value);
		}
	}

	template< typename T>
	static void SetVariable(const char* movie, const char *propertyPathAndName, T value )
	{
		fuiMovie* pMovie = fuiMovie::GetMovie(movie);
		pMovie->SetVariable(propertyPathAndName, value);
	}
	static void SetInt(const char* pFlash, const char * id, s32 value){fuiMovie::SetVariable(pFlash,id,value);}
	static void SetString(const char* pFlash, const char * id, const char* value){fuiMovie::SetVariable(pFlash,id,value);}
	static void SetFloat(const char* pFlash, const char * id, f32 value){fuiMovie::SetVariable(pFlash,id,value);}
	//end parsable id funcs/////////////////////////////////////////////////////////////////////////////////////////////

	template< typename T>
	void SetGlobalArrayProperty(const char *arrayName, int index, const char *propertyName, T value )
	{
		swfVARIANT* pVariant = GetContext()->GetVariant(arrayName);
		if (pVariant)
		{
			swfVARIANT* v1 = &(*(rage::swfSCRIPTARRAY*)pVariant->asObject())[index];
			if (v1)
			{
				swfSCRIPTOBJECT* prop = (swfSCRIPTOBJECT*)v1->asObject();

				swfVARIANT* v = prop->AccessProperty(propertyName);if (v) v->Set(value);
			}
		}
	}
	
	swfVARIANT* GetGlobalArrayVariant(const char *arrayName, int index, const char *propertyName)
	{
		swfVARIANT* pVariant = GetContext()->GetVariant(arrayName);
		if (pVariant)
		{
			swfVARIANT* v1 = &(*(rage::swfSCRIPTARRAY*)pVariant->asObject())[index];
			if (v1)
			{
				swfSCRIPTOBJECT* prop = (swfSCRIPTOBJECT*)v1->asObject();

				return prop->AccessProperty(propertyName);
			}
		}

		return NULL;
	}

	template< typename T>
	static bool SetGlobalVariable(swfCONTEXT &rCtxt, u32 hash, const char *pVariableName, T value)
	{
		rCtxt.SetGlobal(hash, pVariableName, value);
		return true;
	}

	template< typename T >
	static bool SetGlobalVariable(swfCONTEXT &rCtxt, const char *pVariableName, T value)
	{
		rCtxt.SetGlobal(pVariableName, value);
		return true;
	}

	template< typename T >
	bool SetGlobalVariable(const char *pVariableName, T value)
	{
		if (GetContext())
		{
			return SetGlobalVariable(*GetContext(), pVariableName, value);
		}
		return false;
	}

	template< typename T >
	bool SetGlobalVariable(u32 hash, const char *pVariableName, T value)
	{
		if (GetContext())
		{
			return SetGlobalVariable(*GetContext(), hash, pVariableName, value);
		}
		return false;
	}

	template< typename T >
	static bool GetGlobalVariable(swfCONTEXT &rCtxt, u32 hash, const char *pVariableName, T &rRet)
	{
		if (!rCtxt.GetVariant(hash, pVariableName))
		{
			return false;
		}
		rCtxt.GetGlobal(hash,pVariableName,rRet);
		return true;
	}

	template< typename T>
	bool GetGlobalVariable(u32 hash, const char *pVariableName, T &ret)
	{
		if( GetContext())
			return GetGlobalVariable(*GetContext(), hash, pVariableName, ret);
		return false;
	}

	template< typename T >
	static bool GetGlobalVariable(swfCONTEXT &rCtxt, const char *pVariableName, T &rRet)
	{
		if (!rCtxt.GetVariant(pVariableName))
		{
			return false;
		}
		rCtxt.GetGlobal(pVariableName,rRet);
		return true;
	}

	template< typename T >
	bool GetGlobalVariable(const char *pVariableName, T &rRet)
	{
		if (GetContext())
		{
			return GetGlobalVariable(*GetContext(), pVariableName, rRet);
		}
		return false;
	}

	bool GetGlobalVariable(const char *pVariableName, someString &rRet)
	{
		if (GetContext())
		{
			if (!GetContext()->GetVariant(pVariableName))
			{
				return false;
			}
			GetContext()->GetGlobal(pVariableName,rRet.someStringMem, sizeof(rRet.someStringMem));
			return true;
		}
		return false;
	}

	template< typename T >
	static bool SetGlobalArrayElement(swfCONTEXT &rCtxt, const char *pVariableName, T value, int iIndex)
	{
		swfVARIANT *var = rCtxt.GetVariant(pVariableName);
		if (!var)
		{
			return false;
		}
		swfVARIANT tempVar(value);
		rCtxt.SetArrayVariant(*var, iIndex, tempVar);
		return true;
	}

	template< typename T >
	bool SetGlobalArrayElement(const char *pVariableName, T value, int iIndex)
	{
		if (GetContext())
		{
			return SetGlobalArrayElement<T>(*GetContext(), pVariableName, value, iIndex);
		}
		return false;
	}

	template< typename T >
	bool SetGlobalArrayElement(u32 hash, const char *pVariableName, T value, int iIndex)
	{
		if (GetContext())
		{
			swfVARIANT *var = GetContext()->GetVariant(hash, pVariableName);
			if (!var)
			{
				return false;
			}
			swfVARIANT tempVar(value);
			GetContext()->SetArrayVariant(*var, iIndex, tempVar);
			return true;
		}
		return false;
	}

	template< typename T >
	inline void ConvertVarientBack(swfVARIANT /*tempVar*/, T &/*var*/)
	{	Assert(!"If here then you are working with a type that we can't convert back from the swfVARIENT format");}

	template< typename T >
	bool GetGlobalArrayElement(swfCONTEXT &rCtxt, const char *pVariableName, T &value, int iIndex)
	{
		if (!rCtxt.GetVariant(pVariableName))
		{
			return false;
		}
		swfVARIANT tempVar(value);
		rCtxt.GetArrayVariant(pVariableName, iIndex, tempVar);
		ConvertVarientBack<T>(tempVar, value);
		return true;
	}

	template< typename T >
	bool GetGlobalArrayElement(swfCONTEXT &rCtxt, u32 hash, const char *pVariableName, T &value, int iIndex)
	{
		if (!rCtxt.GetVariant(hash, pVariableName))
		{
			return false;
		}
		swfVARIANT tempVar(value);
		rCtxt.GetArrayVariant(hash, pVariableName, iIndex, tempVar);
		ConvertVarientBack<T>(tempVar, value);
		return true;
	}

	template< typename T >
	bool GetGlobalArrayElement(u32 hash, const char *pVariableName, T &value, int iIndex)
	{
		if(!GetContext())
		{
			return false;
		}
		return GetGlobalArrayElement(*GetContext(), hash, pVariableName, value, iIndex);
	}

	template< typename T >
	bool GetGlobalArrayElement(const char *pVariableName, T &value, int iIndex)
	{
		if (GetContext())
		{
			return GetGlobalArrayElement(*GetContext(), pVariableName, value, iIndex);
		}
		return false;
	}

	//Static convenience functions
	//template< typename T >
	//static bool SetGlobalVariable(const char *pFileName, const char *pVariableName, T value)
	//{
	//	fuiMovie *pBase = FLASHMANAGER->GetMovie( pFileName );
	//	if (pBase && pBase->GetContext())
	//	{
	//		if (!pBase->GetContext()->GetVariant(pVariableName))
	//		{
	//			return false;
	//		}

	//		return pBase->SetGlobalVariable<T>(pVariableName, value);
	//	}
	//	return false;
	//}

	//template< typename T >
	//static bool GetGlobalVariable(const char *pFileName, const char *pVariableName, T &rRet)
	//{
	//	fuiMovie *pBase = FLASHMANAGER->GetMovie( pFileName );
	//	if (pBase && pBase->GetContext())
	//	{
	//		if (!pBase->GetContext()->GetVariant(pVariableName))
	//		{
	//			return false;
	//		}

	//		pBase->GetGlobalVariable(pVariableName,rRet);
	//		return true;
	//	}
	//	return false;
	//}

	template< size_t T>
	bool GetGlobalArrayString(const char *pVariableName, char (&rRet)[T], int iIndex)
	{
		if (GetContext())
		{
			if (!GetContext()->GetVariant(pVariableName))
			{
				return false;
			}
			GetContext()->GetArrayString(pVariableName,iIndex,rRet, T);
			//GetGlobalArrayElement(pVariableName, value, iIndex);
			return true;
		}
		return false;
	}

	//template< size_t T>
	//static bool GetGlobalArrayString(const char *pFileName, const char *pVariableName, char (&rRet)[T], int iIndex)
	//{
	//	fuiMovie *pBase = FLASHMANAGER->GetMovie( pFileName );
	//	if (pBase && pBase->GetContext())
	//	{
	//		if (!pBase->GetContext()->GetVariant(pVariableName))
	//		{
	//			return false;
	//		}
	//		pBase->GetContext()->GetArrayString(pVariableName,iIndex,rRet, sizeof(rRet));
	//		//pBase->GetGlobalArrayElement(pVariableName, value, iIndex);
	//		return true;
	//	}
	//	return false;
	//}


	//template< typename T >
	//static bool SetGlobalArrayElement(const char *pFileName, const char *pVariableName, T value, int iIndex)
	//{
	//	fuiMovie *pBase = FLASHMANAGER->GetMovie( pFileName );
	//	if (pBase && pBase->GetContext())
	//	{
	//		if (!pBase->GetContext()->GetVariant(pVariableName))
	//		{
	//			return false;
	//		}

	//		pBase->SetGlobalArrayElement(pVariableName, value, iIndex);
	//		return true;
	//	}
	//	return false;
	//}

	//template< typename T >
	//static bool GetGlobalArrayElement(const char *pFileName, const char *pVariableName, T &value, int iIndex)
	//{
	//	fuiMovie *pBase = FLASHMANAGER->GetMovie( pFileName );
	//	if (pBase && pBase->GetContext())
	//	{
	//		if (!pBase->GetContext()->GetVariant(pVariableName))
	//		{
	//			return false;
	//		}

	//		pBase->GetGlobalArrayElement(pVariableName, value, iIndex);
	//		return true;
	//	}
	//	return false;
	//}

	template< typename T >
	static void CopyGlobalVariable(fuiMovie *destination, fuiMovie *source, u32 hash, const char* pVariableName)
	{
		T out = 0;
		source->GetGlobalVariable(hash, pVariableName, out);
		destination->SetGlobalVariable(hash, pVariableName, out);
	}

	template< typename T >
	static void CopyGlobalVariable(fuiMovie *destination, fuiMovie *source, const char* pVariableName)
	{
		u32 hash = atStringHash(pVariableName);
		CopyGlobalVariable<T>(destination, source, hash, pVariableName);
	}

	bool IsVariableValid(const char *pVariableName)
	{
		if (!GetContext() || !GetContext()->GetVariant(pVariableName))
		{
			return false;
		}
		return true;
	}

#if __BANK
	//void AddWidgets(bkBank& b);
	//void RemoveWidgets();
	//void RefreshWidgets();
	void SetMoviePath(const char *value) { Assert(strlen(value) < sizeof(m_szMoviePath)); strncpy(m_szMoviePath,value,sizeof(m_szMoviePath)); }
	const char* GetMoviePath() const { return m_szMoviePath; }	
	void OutputFlashVariables();
	void OutputMemoryUsage();
#endif

#if __BANK
	void AddWidgets(bkBank& b);
	void RemoveWidgets();
#endif

protected:
	void Load(const fuiMovieData &flashData)
	{
		m_FlashData = flashData;
		Load();
	}

	void FinishLoad();

	static void LockFlashMutex();
	static void UnlockFlashMutex();

	// Member variables
	bool			m_bIsLoaded;
	bool			m_bIsStreamed;
	u8				m_UnloadCounter;
	bool			m_LoadRequested;
	fuiMovieData	m_FlashData;
	pgStreamerInfo	info;
	u8				m_IsVisibleResource;//number of components that are using this movie currently.  when this is zero, then the movie should not be drawn/updated
	bool			m_bVisibleOverride;//can hide this flash movie regardless of the value of m_IsVisibleResource
	swfCONTEXT*		m_pContext;
	int				m_TransitionState;

	//streamable objects
#if STREAM_FLASH_CONTEXTS
	pgStreamable<FlashFileAndContext>* m_pStreamingMovie;
public:
	FlashFileAndContext* m_pStreamingFile;
#else
	pgStreamable<swfFILE>* m_pStreamingMovie;
public:
	swfFILE* m_pStreamingFile;
#endif

#if __BANK
	char m_szMoviePath[128];		
	swfObjectExplorer *mp_VarExplorer;
	bkGroup *mp_Group;
#endif
};

template<>
inline void fuiMovie::ConvertVarientBack(swfVARIANT tempVar, bool &var)
{	var = tempVar.asBool(); }

template<>
inline void fuiMovie::ConvertVarientBack(swfVARIANT tempVar, int &var)
{	var = tempVar.asInt(); }

template<>
inline void fuiMovie::ConvertVarientBack(swfVARIANT tempVar, float &var)
{	var = tempVar.asFloat(); }

template<>
inline void fuiMovie::ConvertVarientBack(swfVARIANT tempVar, const char* &var)
{	var = tempVar.ConstString.String; }

#define DECLARE_HASHED_STRING(A) static u32 A##_HASH = atStringHash(#A)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fuiFlashManager
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class fuiFlashManager : public datBase
{
public:
	fuiFlashManager();
	virtual ~fuiFlashManager();

	//REMEMBER TO INCLUDE THE PREFIX "FSCommand:" (e.g. "FSCommand:getvar")
	static void FSHandler(const char* command, const swfVARIANT& param);

	//some fs callbacks:
	void MeasureStringCallback(swfCONTEXT* ctxt, const swfVARIANT *param);
	void CountLinesCallback(swfCONTEXT* ctxt, const swfVARIANT *param);
	void GetString(const swfVARIANT *param);
	void FSAssert(const swfVARIANT* );

	void UnloadAllMovies();
	void Update(float dt);
	void Load(int memoryTrackingBucket);
	void UpdateStream();
	void PushSharedTextures();
	void PopSharedTextures();
	void UnloadMovieThreadSafe();
	void UnloadMovie(const char *identifier);
	void Draw(const grcViewport* pGameVP);
	swfFILE* LoadFile(const char *pPath);
	void SetCallback(const char* command, datCallback callback);
	fuiMovie* FindMovie(const char *identifier); 
	fuiMovie* GetMovie(const char *identifier){return GetMovie(GetMovieID(identifier));}
	fuiMovie* GetMovie(char index){if (index>=0 && index <m_NumMovies){return m_MoviesList[(int)index];}else{UIErrorf("Movie index %d is out of range",index);return NULL;}}
	void UnstreamMovies();
	void AddWidgets(bkBank& b);
	char GetMovieID(const char* name);
	void RegisterMovie(fuiMovie*);
	void GetVarCallback(const swfVARIANT* );
	void SetVarCallback(const swfVARIANT* );
	void DisableMovie(const swfVARIANT* );
	void ReadyforTransition(const swfVARIANT* );
	void DisableDrawing(bool b){m_bVisibleOverride = b;}//dev func to hide/show UI on the fly
	void TerminateFlashTransition();
	bool IsWide();
	bool IsHighDef();
	//void SetFlashVariable(const char* name, );

#if __BANK
	void OutputPoolSizes();
	static void AddGlobals();
	static void AddAllWidgets();
	static void RemoveAllWidgets();
#endif

	static const char *sm_szFlashResourceLocation;

#if __BANK
	bool m_bReloadRequested;
	static swfObjectExplorer *smp_GlobalsExplorer;
	static bkGroup *smp_Globals;
#endif

	FlashHelper* 		LoadMovie				(bool loadtime, const char *id, const char *path,	u16 instanceCount, u16 swfScriptObjectCount, int stringHeapSize, bool prepend, bool secondaryThread);
	FlashHelper* 		LoadMovie				(bool loadtime, const char *id, swfFILE& file, u16 instanceCount, u16 swfScriptObjectCount, int stringHeapSize, bool prepend, bool secondaryThread, bool owner = true);
	FlashHelper* 		LoadMovie				(bool loadtime, const char *id, FlashFileAndContext& file, bool prepend, bool secondaryThread, bool owner = true);
	fuiRegistry& GetRegistry() { return m_flashGlobalDictionary; }
		
protected:
	friend class atSingleton<fuiFlashManager>;	
	static atMap<atString, datCallback> s_callbacks; //TODO: having this static is kludgey!
	pgDictionary<grcTexture>* m_pSharedTextures;
	fuiMovie* m_MoviesList[MAX_MOVIES]; // main list (map is just an index for fast lookup)
	atMap<const char* , char> m_MovieIDs; //TODO: prealloc memory to prevent fragmentation
	int m_NumMovies;
	static bool m_bVisibleOverride;

	//////////////
	fuiRegistry m_flashGlobalDictionary;
	atMap<atString,FlashHelperNode*> m_movies; //TODO: prealloc memory to prevent fragmentation
	atArray<FlashHelper*>	m_moviesDrawList;	// draw thread safe list of movies to draw
	atArray<FlashHelperNode*>	m_moviesDeleteList; // kill these when it's safe for draw and update threads
	FlashHelperList	m_moviesList; // main list (map is just an index for fast lookup)
//////////////////

public:
	static void (*sm_GrabFlashSemaFunc)();
	static void (*sm_ReleaseFlashSemaFunc)();
};
typedef atSingleton<fuiFlashManager> fuiFlashManagerSingleton;
#define FLASHMANAGER fuiFlashManagerSingleton::InstancePtr()

class flashSemaGrabber
{
public:
	flashSemaGrabber();
	~flashSemaGrabber();
};

#define GRAB_FLASH_UPDATE_SEMA() flashSemaGrabber _grabby;


} // namespace rage

#endif
