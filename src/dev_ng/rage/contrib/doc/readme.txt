!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!! ROCKSTAR OPEN SOURCE !!!!!

Directory:
//rage//<flavor>/contrib/...

Purpose:
This directory is for code that can be shared studio wide, but not supported by RAGE.  The thought is that code that is placed here is done so on the initiative of one of the game teams, with the thought of making the code usable/accessable by/to another game team, or the rage team itself.

Modules here may be supported to some degree by the game team, but are largely considered to be made available "as is."  When adopting a module from this library, it is recommended that you contact the owner (according to perforce if documentation for that library is lacking) and notify them that you will be using the code.  This will ensure that you have a chance to be notified when changes to the system are made, as well as being included in the thought going into moving the library forward.
