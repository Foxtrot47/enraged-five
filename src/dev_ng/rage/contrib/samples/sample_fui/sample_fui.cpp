// 
// sample_fui/sample_fui.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
// TITLE: Sample_Fui
// PURPOSE:
//		This sample is for verifying that contrib/fui still compiles with no game dependencies

#include "sample_grcore/sample_grcore.h"

#include "grcore/im.h"
#include "grcore/im.h"
#include "grcore/state.h"
#include "input/pad.h"
#include "input/keyboard.h"
#include "system/main.h"
#include "script/thread.h"
#include "system/timemgr.h"

#include "fuicore/UIManager.h"
#include "fuicomponents/Components.h"

namespace rage
{
	class rdrPauseMenuScene : public UIScene
	{
	public:
		rdrPauseMenuScene() : UIScene() {;}
	};

	class fuiSample : public ragesamples::grcSampleManager
	{
	public:
		void Init(const char* path = NULL) 
		{
			grcSampleManager::Init(path);

			// hide the stuff in the default tester
			m_DrawBackground = false;
			m_DrawGrid = false;
			m_DrawThreeGrids = false;
			m_DrawHelp = false;
			m_DrawWorldAxes = false;

			// init the systems that fui expects to be initialized
			//scrThread::InitClass(127); // should be prime and > twice the number of expected commands. See http://www.utm.edu/research/primes/lists/small/1000.txt for some prime numbers.
			pgStreamer::InitClass();
			//snuSocketSingleton::Instantiate();

			// init fui
			UIManagerSingleton::Instantiate();
			UIMANAGER->Load();
		}

		void DrawClient()
		{
			// draw fui
			UIMANAGER->Draw(TIME.GetUnwarpedSeconds(),0);
		}

		void Update()
		{
			grcSampleManager::Update();

			// update fui
			UIMANAGER->Update(TIME.GetUnwarpedSeconds());
		}

	protected:
		static ioMapper sm_Mapper;

	};
	ioMapper fuiSample::sm_Mapper;
}

using namespace rage;

int Main()
{
	fuiSample sample;

	sample.Init();

	sample.UpdateLoop();

	sample.Shutdown();

	return 0;
}