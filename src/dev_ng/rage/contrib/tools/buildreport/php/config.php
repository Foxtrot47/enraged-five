<?php /*
(c) 11/03/09 - David Huebner - Rockstar Leeds
R* Test Plan web tool to help with testing builds with many tester.

Uses phpMyDataGrid 2007 by Gur� Sistemas and/or Gustavo Adolfo Arcila Trujillo (www.gurusistemas.com)

Tested to work on PHP5.x and MySql 5.x, Javascript enabled. Best on Firefox.
Internet Explorer has speed issues when building up the huge table.

Note that testers should always hit the refresh button before they assign a mission to themself, to make sure that nobody else has taken the mission in the meanwhile.
*/

//this is the configuration file for the build report


//change the following variables to work with your mysql server

$mysql_address = "127.0.0.1";

$mysql_username = "admin";

$mysql_password = "houseplant";

$mysql_config_database = "br_projects";


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

//do not change any of the following variables!!
$mysql_database = "";
$project_name = "";
$param_db_suffix = "";
$displayTestPlanLink = true;
$col_show = array();
$col_widths = array();
$col_locks = array();
$repeat_header = 0;

//redirect to project list page (index.php) if project name is given but not in the list - or to project admin page if project db does not exist or is empty
if (!$noRedirect)
	include ("proj_redirect.php");

?>