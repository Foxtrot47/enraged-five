<?php
/*
(c) 11/03/09 - David Huebner - Rockstar Leeds
R* Test Plan web tool to help with testing builds with many tester.

Uses phpMyDataGrid 2007 by Gur� Sistemas and/or Gustavo Adolfo Arcila Trujillo (www.gurusistemas.com)

Tested to work on PHP5.x and MySql 5.x, Javascript enabled. Best on Firefox.
Internet Explorer has speed issues when building up the huge table.

Note that testers should always hit the refresh button before they assign a mission to themself, to make sure that nobody else has taken the mission in the meanwhile.
*/

//this is the project selection page

	$pagetitle = "R*Test Plan - Project Selection";
	$noRedirect = true;
	include ("config.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>
<?php echo $pagetitle; ?>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta http-equiv="imagetoolbar" content="no" />
<style type="text/css">
<!--
table {border-color:#aaaaaa;}
a { text-decoration:underline; color:105eaa; }
a:link { text-decoration:underline; color:#105eaa; }
a:visited { text-decoration:underline; color:#105eaa; }
a:hover { text-decoration:underline; color:black; }
a:active { text-decoration:underline; color:#105eaa; }
a:focus { text-decoration:underline; color:#105eaa; }

ul#Navigation {
    margin: 0; padding: 0;
  }
  ul#Navigation li {
    list-style: none;
    margin: 0; padding: 0;
  }
  a.planlink {
	font-size: 1.7em;
    display: block;
	width: 97%;
    padding: 0.6em;
    font-weight: bold;
    border: 1px dashed silver;
  }
  a.planlink:link { background-color: #EEE; }
  a.planlink:visited { background-color: #EEE; }
  a.planlink:hover { background-color: #CCC; }
  a.planlink:active { background-color: #CCC; }


-->
</style>
</head>

<body style="font-family:Verdana, Arial; font-size:0.8em">
<br />

<?php
	//check if project database exists or if it is empty
	$showWelcome = true;
	$projectListEmpty = true;
	$projectList = "";
	$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
	if ($connection) //found connection to database
	{
		$db_selected = mysql_select_db($mysql_config_database, $connection);
		if ($db_selected)
		{
			$showWelcome = false;
			$query = "SELECT `name`,`db_suffix` FROM `projects` WHERE `enabled` != 0 ORDER BY `id`";
			$result=mysql_query($query);
			if (mysql_num_rows($result) != 0)
			{
				$projectListEmpty = false;
				while($row = mysql_fetch_object($result))
				{
					$projectList .= '<li><a class="planlink" href="testplan.php?project='.$row->db_suffix.'">'.$row->name.'</a><br /></li>';
				}
			}
		}
	}

	
	if ($showWelcome) //if there aren't any projects to display, show welcome message and link to projects admin page
	{
?>
		<span style='color:#105eaa; font-weight:bold; font-size:1.7em'>Welcome to <i>R*Test Plan</i></span><br />
		Please make sure you have read the Test_Plan_Notes document.<br /><br />
		You can now start adding projects <a href="admin/project_admin.php">here</a>.
<?php
	}
	else //otherwise list the projects
	{
?>
		<table width="99%" border="0"><tr>
			<td align="left" style='color:#105eaa; font-weight:bold; font-size:1.7em'>R*<i>Test Plan</i> - Project Selection</td>
			<td align="right">Admin use only: <a href="admin/project_admin.php">Project Management</a></td>
		</tr></table>
		<br /><br />
<?php
		if ($projectListEmpty)
		{
			echo 'No projects accessible';
		}
		else
		{
			echo '<ul id="Navigation">' . $projectList . '</ul>';
		}
	}

	mysql_close($connection);
?>
<br /><br /><br />
</body>
</html>
