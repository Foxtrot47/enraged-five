<?php /*
(c) 11/03/09 - David Huebner - Rockstar Leeds
R* Test Plan web tool to help with testing builds with many tester.

Uses phpMyDataGrid 2007 by Gurú Sistemas and/or Gustavo Adolfo Arcila Trujillo (www.gurusistemas.com)

Tested to work on PHP5.x and MySql 5.x, Javascript enabled. Best on Firefox.
Internet Explorer has speed issues when building up the huge table.

Note that testers should always hit the refresh button before they assign a mission to themself, to make sure that nobody else has taken the mission in the meanwhile.
*/

//this is the file that will make sure the database exists

if (mysql_select_db($mysql_database) == FALSE)
{
	$sqlCommands = array();
	$sqlCommands[] = 'SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";';
	$sqlCommands[] = 'CREATE DATABASE `'.$mysql_database.'` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;';
	$sqlCommands[] = 'USE `'.$mysql_database.'`;';
	$sqlCommands[] = 'CREATE TABLE IF NOT EXISTS `buildtypes` (
						  `id` int(11) NOT NULL AUTO_INCREMENT,
						  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
						  PRIMARY KEY (`id`)
						) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;';
	$sqlCommands[] = "INSERT INTO `buildtypes` (`id`, `name`) VALUES
						(4, 'PS3US'),
						(3, 'PS3EU'),
						(5, '360EU'),
						(6, '360US');";
	$sqlCommands[] = 'CREATE TABLE IF NOT EXISTS `missiontypes` (
						  `id` int(11) NOT NULL AUTO_INCREMENT,
						  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
						  `checked` tinyint(1) NOT NULL,
						  PRIMARY KEY (`id`)
						) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;';
	$sqlCommands[] = "INSERT INTO `missiontypes` (`id`, `name`) VALUES
						(1, 'Singleplayer'),
						(3, 'Multiplayer');";
	$sqlCommands[] = 'CREATE TABLE IF NOT EXISTS `report` (
						  `id` int(11) NOT NULL AUTO_INCREMENT,
						  `order_id` int(11) NOT NULL,
						  `mission_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
						  `missiontype` smallint(6) NOT NULL,
						  `mission_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
						  `scripter` mediumint(9) NOT NULL,
						  `tester` mediumint(9) NOT NULL,
						  `owner` mediumint(9) NOT NULL,
						  `fix_status` smallint(6) NOT NULL,
						  `notes` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
						  `bug_no` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
						  `PS3EU` smallint(6) DEFAULT NULL,
						  `PS3US` smallint(6) DEFAULT NULL,
						  `360EU` smallint(6) DEFAULT NULL,
						  `360US` smallint(6) DEFAULT NULL,
						  PRIMARY KEY (`id`)
						) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;';
	$sqlCommands[] = 'CREATE TABLE IF NOT EXISTS `testers` (
						  `hidden_id` int(11) NOT NULL AUTO_INCREMENT,
						  `id` int(11) NOT NULL,
						  `name` varchar(50) COLLATE utf8_bin NOT NULL,
						  `checked` tinyint(1) NOT NULL,
						  PRIMARY KEY (`hidden_id`)
						) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;';

	foreach($sqlCommands as $command)
	{
		$result = mysql_query($command);
		if (mysql_errno()!=0 || !$result)
			$actionText .= "ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "<br />";
	}
}

?>