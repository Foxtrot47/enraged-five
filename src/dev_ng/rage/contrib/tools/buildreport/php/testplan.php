<?php /*
(c) 11/03/09 - David Huebner - Rockstar Leeds
R* Test Plan web tool to help with testing builds with many tester.

Uses phpMyDataGrid 2007 by Gur� Sistemas and/or Gustavo Adolfo Arcila Trujillo (www.gurusistemas.com)

Tested to work on PHP5.x and MySql 5.x, Javascript enabled. Best on Firefox.
Internet Explorer has speed issues when building up the huge table.

Note that testers should always hit the refresh button before they assign a mission to themself, to make sure that nobody else has taken the mission in the meanwhile.
*/ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<style type="text/css">
<!--
a { text-decoration:underline; color:105eaa; }
a:link { text-decoration:underline; color:#105eaa; }
a:visited { text-decoration:underline; color:#105eaa; }
a:hover { text-decoration:underline; color:black; }
a:active { text-decoration:underline; color:#105eaa; }
a:focus { text-decoration:underline; color:#105eaa; }
-->
</style>

<?php

	$noDisabledProjects = true;
	if ($adminChangesAllowed)
	{
		$noDisabledProjects = false;
	}


	include ("phpmydatagrid.class.php");

	include ("config.php");

	if ($adminChangesAllowed)
		echo "<title>R* Test Plan - " . $project_name . " - [ADMIN MODE]</title>";
	else
		echo "<title>R* Test Plan - " . $project_name . "</title>";
	
	$objGrid = new datagrid;

	$adminLink = "";
	$formLink = "";
	$projectSelectionLink = "";
	if ($adminChangesAllowed)
	{
		$adminLink  = "assign.php". $param_db_suffix;
		$objGrid -> setHeader("", "../js/dgscripts.js", "../css/dgstyle.css");
		$formLink = "admintestplan.php";
		$projectSelectionLink = "../index.php";
	}
	else
	{
		$adminLink  = "admin/assign.php". $param_db_suffix;
		$objGrid -> setHeader();
		$formLink = "testplan.php";
		$projectSelectionLink = "index.php";
	}
	
	$objGrid -> friendlyHTML();

	if ($adminChangesAllowed)
		$objGrid -> pathtoimages("../images/");
	else
		$objGrid -> pathtoimages("./images/");

	$objGrid -> closeTags(true);  
	
	$objGrid -> form('testplan', true);

	$objGrid -> methodForm("post"); 
	
	$objGrid -> linkparam("sess=".$_REQUEST["sess"]."&username=".$_REQUEST["username"]);	 
	
	$objGrid -> decimalDigits(2);
	
	$objGrid -> decimalPoint(",");
	
	$objGrid -> conectadb($mysql_address, $mysql_username, $mysql_password, $mysql_database);
	
	//GET TABLE NAME AND CHOOSE IT
	$table = "report";
	if (isset($_POST['selectDB']))
	{
		if (isset($_POST['chosen_table']) && $_POST['chosen_table'] != "report" && $_POST['chosen_table'] != "no_go")
		{
			$table = $_POST['chosen_table'];
		}
	}
	$objGrid -> tabla ("`".$table."`");

	$objGrid -> buttons(false,false,false,false);
	
	$objGrid -> keyfield("id");

	$objGrid -> salt("Some Code4Stronger(Protection)");

	if ($adminChangesAllowed)
		$objGrid -> TituloGrid("R*<i>Test Plan</i> - " . $project_name . " - [<i>ADMIN MODE</i>]");
	else
		$objGrid -> TituloGrid("R*<i>Test Plan</i> - " . $project_name);

	$objGrid -> FooterGrid("<div style='float:left'><a href='".$adminLink."'>Administration</a></div>");

	$objGrid -> datarows(10000);
	
	$objGrid -> paginationmode('links');

	$objGrid -> orderby("order_id", "ASC");

	$objGrid -> repeatHeaderAfterRows($repeat_header);

	//$objGrid -> noorderarrows();

	
	$buildlist = array();
	$buildstr = "";
	$sql = "SELECT `name` FROM `buildtypes` ORDER BY `id`";
	$result = mysql_query($sql);
	if (mysql_errno()!=0 || !$result)
		echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
	else
	{
		while($row = mysql_fetch_object($result))
		{
			$buildstr .= $row->name.",";
			$buildlist[] = $row->name;
		}
		//$buildstr = substr($buildstr, 0, -1);
		$buildstr .= "fix_status";
	}

	$objGrid -> total($buildstr);

	$isDisplayingBackup = false;
	if ($table != "report")
		$isDisplayingBackup = true;

	if ($adminChangesAllowed && !$isDisplayingBackup) { foreach ($col_locks as &$value) $value = 0; }
	else if ($isDisplayingBackup) { foreach ($col_locks as &$value) $value = 1; }

	$testerlist = "select:0_&mdash;";
	$sql = "SELECT * FROM `testers` ORDER BY `id`";
	$result = mysql_query($sql);
	if (mysql_errno()!=0 || !$result)
		echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
	while($row = mysql_fetch_object($result))
	{
		$testerlist .= ":" . $row->id . "_" . $row->name;
	}
	$testerlist .= ":8388607_&mdash;";

	$missiontypelist = "select:0_&mdash;";
	$sql = "SELECT `name`,`id` FROM `missiontypes` ORDER BY `id`";
	$result = mysql_query($sql);
	if (mysql_errno()!=0 || !$result)
		echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
	$i = 1;
	while($row = mysql_fetch_object($result))
	{
		$nm = $row->name;
		$missiontypelist .= ":" . $row->id . "_<span class='c" . ($i-1) . "'>" . $nm . "</span>";
		$i++;
	}
	$missiontypelist .= ":32767_&mdash;";

	if ($col_show['id']) $objGrid -> FormatColumn("order_id", "ID", 5, 5, 1, $col_widths['id'], "center");
	if ($col_show['mission_id']) $objGrid -> FormatColumn("mission_id", "missionID", 50, 50, $col_locks['mission_id'], $col_widths['mission_id'], "left");
	if ($col_show['mission_type']) $objGrid -> FormatColumn("missiontype", "Missiontype", 50, 50, $col_locks['mission_type'], $col_widths['mission_type'], "left", $missiontypelist);
	if ($col_show['mission_title']) $objGrid -> FormatColumn("mission_title", "Title", 100, 100, $col_locks['mission_title'], $col_widths['mission_title'], "left");
	if ($col_show['scripter']) $objGrid -> FormatColumn("scripter", "Scripter", 5, 5, $col_locks['scripter'], $col_widths['scripter'], "left", $testerlist);
	if ($col_show['tester']) $objGrid -> FormatColumn("tester", "Tester", 5, 5, $col_locks['tester'], $col_widths['tester'], "left", $testerlist);
	if ($col_show['build_types'])
	{
		foreach ($buildlist as $type)
		{
			$objGrid -> FormatColumn($type, $type, 5, 5, $col_locks['build_types'], $col_widths['build_types'], "center", "select:0_&mdash;:1_<span class='cred'>NO</span>:2_<span class='cgreen'>YES</span>:3_&mdash;");
		}
	}
	if ($col_show['bug_no']) $objGrid -> FormatColumn("bug_no", "Bug#", 10, 10, $col_locks['bug_no'], $col_widths['bug_no'], "left",'text','',0);
	if ($col_show['owner']) $objGrid -> FormatColumn("owner", "Owner", 5, 5, $col_locks['owner'], $col_widths['owner'], "left", $testerlist);
	if ($col_show['fix_status']) $objGrid -> FormatColumn("fix_status" , "FixStatus", 5, 5, $col_locks['fix_status'], $col_widths['fix_status'], "center", "select:0_&mdash;:1_<span>not fixed</span>:2_<span class='cgreen'>resolved</span>:3_&mdash;");
	if ($col_show['notes']) $objGrid -> FormatColumn("notes", "Notes", 1000, 1000, $col_locks['notes'], $col_widths['notes'], "left",'text','',0);
?>
</head>

<?php 
	echo '<body onload="DG_setProjectName('."'".str_replace('?', '&amp;', $param_db_suffix)."')".'">';
	$sql = "SHOW TABLES FROM $mysql_database";
	$result = mysql_query($sql);
	if (mysql_errno()!=0 || !$result)
		echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";

	if (!isset($_REQUEST["DG_ajaxid"])){
?>
	<table width="99%" border="0"><tr><td align="left">
	<form action="<?php echo $formLink.$param_db_suffix; ?>" method="post">
	List of Test Plans:
	<select name='chosen_table' size='1'>
		<option value='report'>current plan</option>
		<option value='no_go'>==============</option>
<?php

	while ($row = mysql_fetch_row($result)) {
		$name = $row[0];
		$value = $row[0];
		if ($name != "report" && strncmp($name, "rep", 3) == 0)
		{
			$selected = "";
			if ($table == $value)
				$selected = " selected='selected'";
			echo "<option".$selected." value='".$value."'>".$name."</option>";
		}
	}
?>
	</select>
	<input type="submit" name="selectDB" value="Go" />
	</form></td>
	<td align="left"><a href="<?php echo $projectSelectionLink; ?>"><b>Project Selection</b></a></td>
<?php
	if ($table == "report")
	{
		echo '<td align="left"><a href="javascript:DG_refreshPage()"><b>Refresh</b></a></td>';
	}
?>
	<td align="right">Admin use only: <a href='<?php echo $adminLink; ?>'><b>Administration</b></a></td>
	</tr></table>

<?php

	} //end of if (!isset($_REQUEST["DG_ajaxid"])){

	mysql_free_result ($result);

	$objGrid -> ajax("silent");

	$objGrid -> grid();
	
	$objGrid -> desconectar();
?>
</body>
</html>
