<?php
/*
(c) 11/03/09 - David Huebner - Rockstar Leeds
R* Test Plan web tool to help with testing builds with many tester.

Uses phpMyDataGrid 2007 by Gur� Sistemas and/or Gustavo Adolfo Arcila Trujillo (www.gurusistemas.com)

Tested to work on PHP5.x and MySql 5.x, Javascript enabled. Best on Firefox.
Internet Explorer has speed issues when building up the huge table.

Note that testers should always hit the refresh button before they assign a mission to themself, to make sure that nobody else has taken the mission in the meanwhile.
*/

//this is file redirects the browser...
// ... to project list page (index.php) if project name is given but not in the list
// ... or to project admin page if project db does not exist or is empty

$redirectAndExit = false;
$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
if (!$connection) //no connection to database
{
	$redirectAndExit = true;
	$refreshMeta = '';
	$refreshLink = 'No connection to database. Please ask the administrator to fix the mysql server or to adjust config.php.';
}
else
{
	$db_selected = mysql_select_db($mysql_config_database, $connection);
	if (!$db_selected || 0 == mysql_num_rows( mysql_query("SHOW TABLES LIKE 'projects'")))//db or table does not exist
	{
		$redirectAndExit = true;
		$refreshMeta = '<meta HTTP-EQUIV="REFRESH" content="3; url=index.php">';
		$refreshLink = 'The configuration database has not been set up yet. You will be redirected.<br />If you do not get redirected automatically, please follow this <a href="index.php">link</a>.';
	}
	else if ( !( isset($_GET['project']) || $_GET['project']!="" || isset($_POST['project']) || $_POST['project']!="" ) )
	{
		$redirectAndExit = true;
		$refreshMeta = '<meta HTTP-EQUIV="REFRESH" content="3; url=index.php">';
		$refreshLink = 'You have not specified a project. You will be redirected to the project selection page.<br />If you do not get redirected automatically, please follow this <a href="index.php">link</a>.';
	}
	else
	{
		$project_param = "";
		if (isset($_GET['project']) && $_GET['project']!="")
			$project_param = $_GET['project'];
		else
			$project_param = $_POST['project'];

		$query = "SELECT * FROM `projects` WHERE `db_suffix` = '". $project_param . "'";
		$result=mysql_query($query);
		if (mysql_num_rows($result) == 0)
		{
			$redirectAndExit = true;
			$refreshMeta = '<meta HTTP-EQUIV="REFRESH" content="3; url=index.php">';
			$refreshLink = 'The specified project does not exist. You will be redirected to the project selection page.<br />If you do not get redirected automatically, please follow this <a href="index.php">link</a>.';
		}
		else
		{
			$row = mysql_fetch_object($result);
			if ($row->enabled == 0 && $noDisabledProjects)
			{
				$redirectAndExit = true;
				$refreshMeta = '<meta HTTP-EQUIV="REFRESH" content="3; url=index.php">';
				$refreshLink = 'The specified project is disabled. You will be redirected to the project selection page.<br />If you do not get redirected automatically, please follow this <a href="index.php">link</a>.';
			}
			else
			{
				$param_db_suffix = "?project=".$row->db_suffix;
				$mysql_database = "br_".$row->db_suffix;
				$project_name = $row->name;

				if ($row->enabled == 0)
					$displayTestPlanLink = false;
				
				$col_show['id'] = $row->show_id;
				$col_show['mission_id'] = $row->show_mission_id;
				$col_show['mission_type'] = $row->show_mission_type;
				$col_show['mission_title'] = $row->show_mission_title;
				$col_show['scripter'] = $row->show_scripter;
				$col_show['tester'] = $row->show_tester;
				$col_show['build_types'] = $row->show_build_types;
				$col_show['bug_no'] = $row->show_bug_no;
				$col_show['owner'] = $row->show_owner;
				$col_show['fix_status'] = $row->show_fix_status;
				$col_show['notes'] = $row->show_notes;
				
				$col_widths['id'] = $row->width_id;
				$col_widths['mission_id'] = $row->width_mission_id;
				$col_widths['mission_type'] = $row->width_mission_type;
				$col_widths['mission_title'] = $row->width_mission_title;
				$col_widths['scripter'] = $row->width_scripter;
				$col_widths['tester'] = $row->width_tester;
				$col_widths['build_types'] = $row->width_build_types;
				$col_widths['bug_no'] = $row->width_bug_no;
				$col_widths['owner'] = $row->width_owner;
				$col_widths['fix_status'] = $row->width_fix_status;
				$col_widths['notes'] = $row->width_notes;

				$col_locks['id'] = $row->locked_id;
				$col_locks['mission_id'] = $row->locked_mission_id;
				$col_locks['mission_type'] = $row->locked_mission_type;
				$col_locks['mission_title'] = $row->locked_mission_title;
				$col_locks['scripter'] = $row->locked_scripter;
				$col_locks['tester'] = $row->locked_tester;
				$col_locks['build_types'] = $row->locked_build_types;
				$col_locks['bug_no'] = $row->locked_bug_no;
				$col_locks['owner'] = $row->locked_owner;
				$col_locks['fix_status'] = $row->locked_fix_status;
				$col_locks['notes'] = $row->locked_notes;

				$repeat_header =  $row->repeat_header;
				
				$actionText = "";
				include ("create_database.php"); //create database in case it got lost
				if ($actionText != "")
					echo "<span style='color:red'>".$actionText."</span><br />";
			}
		}
	}
}
mysql_close($connection);

if ($redirectAndExit)
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>
Build report</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta http-equiv="imagetoolbar" content="no" />
<?php echo $refreshMeta; ?>
</head>

<body style="font-family:Verdana, Arial; font-size:0.8em"">

<br />
<?php echo $refreshLink; ?>
</body>
</html>
<?php
	exit();
}
?>