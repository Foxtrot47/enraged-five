<?php
/*
(c) 11/03/09 - David Huebner - Rockstar Leeds
R* Test Plan web tool to help with testing builds with many tester.

Uses phpMyDataGrid 2007 by Gur� Sistemas and/or Gustavo Adolfo Arcila Trujillo (www.gurusistemas.com)

Tested to work on PHP5.x and MySql 5.x, Javascript enabled. Best on Firefox.
Internet Explorer has speed issues when building up the huge table.

Note that testers should always hit the refresh button before they assign a mission to themself, to make sure that nobody else has taken the mission in the meanwhile.
*/

//this is the administration page for the missiontypes

	include ("../config.php");
	$pagetitle = "Mission Types";
	$curent_page = "missiontypes";
	include ("adminmenu.php");


	if (isset($_POST['SubmitDelete']))
	{
		$missiontypes = $_POST["missiontypes"];
		$nummissiontypes = count($missiontypes);
		if ($nummissiontypes == 0)
		{
			echo"<span style='color:red'>You need to select the mission types you want to delete!</span><br />";
		}
		else
		{
			$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
			mysql_select_db($mysql_database);
			foreach ($missiontypes as $id)
			{
				$sql = "DELETE FROM `missiontypes` WHERE `id`=$id";
				//echo "query1:".$sql."<br />";
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";

				$query = "UPDATE `report` SET `missiontype`=0 WHERE `missiontype`=$id";
				$result = mysql_query($query);
				if (mysql_errno()!=0)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				else
					echo"<span style='color:blue'>Mission type deleted!</span><br />";
			}
			mysql_close($connection);
		}
	}
	if (isset($_POST['SubmitNew']))
	{
		if (!$_POST["missiontypename"] || $_POST["missiontypename"] == "")
		{
			echo"<span style='color:red'>No name for the new mission type specified</span><br />";
		}
		else
		{
			$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
			mysql_select_db($mysql_database);
			//$missiontypename = ereg_replace("[)( .-]","",$_POST["missiontypename"]);
			$missiontypename = mysql_real_escape_string($_POST["missiontypename"]);
			$query = "INSERT INTO missiontypes (name) VALUES ('$missiontypename')";
			$result = mysql_query($query);
			if (mysql_errno()!=0)
				echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
			else
				echo"<span style='color:blue'>Missiontype $missiontypename created!</span><br />";
			mysql_close($connection);
		}
	}

	if (isset($_POST['SubmitSave']))
	{
		$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
		mysql_select_db($mysql_database);
		$new_name = $_POST['name'];
		$new_name = mysql_real_escape_string($new_name);
		if ($new_name == "")
		{
			echo "<span style='color:red'>Name can not be left empty.</span><br />";
		}
		else
		{
			$query = "UPDATE `missiontypes` SET name='".$new_name."' WHERE `id`=".$_POST['id'];
			$result = mysql_query($query);
			if (mysql_errno()!=0)
				echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
			else
				echo"<span style='color:blue'>Missiontype renamed!</span><br />";
		}
		mysql_close($connection);
	}
?>

<?php
	$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
	mysql_select_db($mysql_database);

	foreach($_POST as $k => $v)
	{
		if (substr($k, 0, 12) == "SubmitEditId")
		{
			preg_match('{(\d+)}', $k, $m);
			$editId = $m[1];

			if ($editId == "" || !is_numeric($editId))
			{
				echo "<span style='color:red'>Wrong parameters to edit missiontype.</span><br />";
			}
			else
			{
				$query = "SELECT * FROM `missiontypes` WHERE `id` = ".$editId;
				$result=mysql_query($query);
				if (mysql_errno()!=0)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				if (mysql_num_rows($result) == 0)
				{
					echo "<span style='color:red'>Missiontype id does not exist.</span><br />";
				}
				else
				{
					$row = mysql_fetch_object($result);
	?>
					<b>Edit a missiontype:</b><br />
					<form name="EditForm" action="missiontypes.php<?php echo $param_db_suffix; ?>" method="post">
					<table border="1" style="border-collapse:collapse">
					<tr>
	<?php
					echo '<td><input type="hidden" name="id" value="'.$row->id.'" /><input id="onloadFocusFirst" type="text" name="name" size="30" maxlength="40" value="'.$row->name.'" /></td><td>&nbsp;&nbsp;&nbsp;<input type="submit" name="SubmitSave" value="Save" /></td></tr></table></form>';
				}
			}
			break;
		}
	}
?>

<br />
<form action="missiontypes.php<?php echo $param_db_suffix; ?>" method="post">
<b>Add mission types:</b><br />
<table border="1" style="border-collapse:collapse"><tr>
<td><input id='onloadFocus' name="missiontypename" type="text" size="30" maxlength="40" /></td>
<td>&nbsp;&nbsp;&nbsp;<input type="submit" name="SubmitNew" value="Add" /></td>
</tr></table>
</form>
<br /><br />
<form action="missiontypes.php<?php echo $param_db_suffix; ?>" method="post">
<b>Delete mission types:</b><br />
<table border="1" style="border-collapse:collapse">
<tr>
<td><input type="submit" name="SubmitDelete" value="Delete" /></td>
</tr>
<tr>
<td><input type='checkbox' name='checkall' onclick="toggleAllCheckboxes(this, 'missiontypes[]');" /> All</td>
</tr>
<tr><td>
<table border="0">
<?
	$sql = "SELECT `id`,`name` FROM `missiontypes` ORDER BY `id`";
	$result = mysql_query($sql);
	if (mysql_errno()!=0 || !$result)
		echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
	while($row = mysql_fetch_object($result))
	{
		echo "<tr><td><input type='checkbox' name='missiontypes[]' value='".$row->id."' /></td><td>".$row->name."</td>
		<td><input type='image' src='../images/edit.gif' name='SubmitEditId*".$row->id."*' alt='Edit missiontype' /></td></tr>";
	}
?>
</table>
</td></tr>
<tr>
<td><input type='checkbox' name='checkall' onclick="toggleAllCheckboxes(this, 'missiontypes[]');" /> All</td>
</tr>
<tr>
<td><input type="submit" name="SubmitDelete" value="Delete" /></td>
</tr>
</table>
</form>
<br /><br /><br />

<?
	mysql_close($connection);
?>
</body>
</html>
