<?php /*
(c) 11/03/09 - David Huebner - Rockstar Leeds
R* Test Plan web tool to help with testing builds with many tester.

Uses phpMyDataGrid 2007 by Gurú Sistemas and/or Gustavo Adolfo Arcila Trujillo (www.gurusistemas.com)

Tested to work on PHP5.x and MySql 5.x, Javascript enabled. Best on Firefox.
Internet Explorer has speed issues when building up the huge table.

Note that testers should always hit the refresh button before they assign a mission to themself, to make sure that nobody else has taken the mission in the meanwhile.
*/

//this is the file that will make sure the database exists

//$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
if (mysql_select_db($mysql_config_database) == FALSE)
{
	$sqlCommands = array();
	$sqlCommands[] = 'SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";';
	$sqlCommands[] = 'CREATE DATABASE `'.$mysql_config_database.'` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;';
	$sqlCommands[] = 'USE `'.$mysql_config_database.'`;';
	$sqlCommands[] = 'CREATE TABLE IF NOT EXISTS `projects` ( `id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL, `db_suffix` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL, `enabled` tinyint(1) NOT NULL, `repeat_header` mediumint(9) NOT NULL, `show_id` tinyint(1) NOT NULL, `show_mission_id` tinyint(1) NOT NULL, `show_mission_type` tinyint(1) NOT NULL, `show_mission_title` tinyint(1) NOT NULL, `show_scripter` tinyint(1) NOT NULL, `show_tester` tinyint(1) NOT NULL, `show_build_types` tinyint(1) NOT NULL, `show_bug_no` tinyint(1) NOT NULL, `show_owner` tinyint(1) NOT NULL, `show_fix_status` tinyint(1) NOT NULL, `show_notes` tinyint(1) NOT NULL, `width_id` mediumint(9) NOT NULL, `width_mission_id` mediumint(9) NOT NULL, `width_mission_type` mediumint(9) NOT NULL, `width_mission_title` mediumint(9) NOT NULL, `width_scripter` mediumint(9) NOT NULL, `width_tester` mediumint(9) NOT NULL, `width_build_types` mediumint(9) NOT NULL, `width_bug_no` mediumint(9) NOT NULL, `width_owner` mediumint(9) NOT NULL, `width_fix_status` mediumint(9) NOT NULL, `width_notes` mediumint(9) NOT NULL, `locked_id` tinyint(1) NOT NULL, `locked_mission_id` tinyint(1) NOT NULL, `locked_mission_type` tinyint(1) NOT NULL, `locked_mission_title` tinyint(1) NOT NULL, `locked_scripter` tinyint(1) NOT NULL, `locked_tester` tinyint(1) NOT NULL, `locked_build_types` tinyint(1) NOT NULL, `locked_bug_no` tinyint(1) NOT NULL, `locked_owner` tinyint(1) NOT NULL, `locked_fix_status` tinyint(1) NOT NULL, `locked_notes` tinyint(1) NOT NULL, PRIMARY KEY (`id`) ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;';

	foreach($sqlCommands as $command)
	{
		$result = mysql_query($command);
		if (mysql_errno()!=0 || !$result)
			$actionText .= "Error creating the database. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
	}
}

//mysql_close($connection);

?>