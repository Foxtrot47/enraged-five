<?php
/*
(c) 11/03/09 - David Huebner - Rockstar Leeds
R* Test Plan web tool to help with testing builds with many tester.

Uses phpMyDataGrid 2007 by Gur� Sistemas and/or Gustavo Adolfo Arcila Trujillo (www.gurusistemas.com)

Tested to work on PHP5.x and MySql 5.x, Javascript enabled. Best on Firefox.
Internet Explorer has speed issues when building up the huge table.

Note that testers should always hit the refresh button before they assign a mission to themself, to make sure that nobody else has taken the mission in the meanwhile.
*/

//this is the administration page for the report

	include ("../config.php");
	$pagetitle = "Assign Testers";
	$curent_page = "assign";
	include ("adminmenu.php");


	if (isset($_POST['SubmitDelete']))
	{
		$queryCols = "";

		if (isset($_POST['tester']) && $_POST['tester']=="on")
		{
			if ($queryCols!="")
				$queryCols.=",";
			$queryCols.=" `tester` = 0";
		}

		if (isset($_POST['owner']) && $_POST['owner']=="on")
		{
			if ($queryCols!="")
				$queryCols.=",";
			$queryCols.=" `owner` = 0";
		}
		
		if (isset($_POST["buildtypes"]))
		{
			$buildtypes = $_POST["buildtypes"];
			echo "number of buildtypes: ".count($buildtypes)."<br />";
			if (count($buildtypes) > 0)
			{
				foreach ($buildtypes as $build)
				{
					if ($queryCols!="")
						$queryCols.=",";
					$queryCols.=" ".$build." = 0";
				}
			}
		}
		if (isset($_POST['notes']) && $_POST['notes']=="on")
		{
			if ($queryCols!="")
				$queryCols.=",";
			$queryCols.=" notes = ''";
		}
		if (isset($_POST['bugno']) && $_POST['bugno']=="on")
		{
			if ($queryCols!="")
				$queryCols.=",";
			$queryCols.=" bug_no = ''";
		}
		if (isset($_POST['fix_status']) && $_POST['fix_status']=="on")
		{
			if ($queryCols!="")
				$queryCols.=",";
			$queryCols.=" `fix_status` = 0";
		}
		
		if ($queryCols=="")
		{
			echo"<span style='color:red'>No checkboxes selected!</span><br />";
		}
		else
		{
			$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
			mysql_select_db($mysql_database);
			$query = "UPDATE `report` SET ".$queryCols;
			$result = mysql_query($query);
			if (mysql_errno()!=0)
				echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
			else
				echo "<span style='color:blue'>Rows cleared!</span><br />";
			mysql_close($connection);
		}
	}
	if (isset($_POST['SubmitRandom']) || isset($_POST['SubmitSaveSelection']))
	{
		$missiontypes = $_POST["missiontypes"];
		$testers = $_POST["testers"];
		echo "number of missiontypes: ".count($missiontypes)."<br />";
		echo "number of testers: ".count($testers)."<br />";
		$nummissiontypes = count($missiontypes);
		$numTesters = count($testers);
		if (!isset($_POST['SubmitSaveSelection']) && ($nummissiontypes == 0 || $numTesters == 0))
		{
			echo"<span style='color:red'>You need to select Mission Types and Testers!</span><br />";
		}
		else
		{
			$emplString = "";
			if ($nummissiontypes > 0)
			{
				foreach ($missiontypes as $empl)
				{
					$emplString .= "'" . $empl . "',";
				}
				$emplString = substr($emplString, 0, -1);
			}
			$testerString = "";
			if ($numTesters > 0)
			{
				foreach ($testers as $tester)
				{
					$testerString .= "'" . $tester . "',";
				}
				$testerString = substr($testerString, 0, -1);
			}
			$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
			mysql_select_db($mysql_database);

			///////////
			//STORE STATE OF CHECKBOXES
			//set all to unchecked
			$sql = "UPDATE `missiontypes` SET `checked`=0";
			$result = mysql_query($sql);
			if (mysql_errno()!=0 || !$result)
				echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
			//set only the checked ones to checked in the db
			if ($emplString != "")
			{
				$sql = "UPDATE `missiontypes` SET `checked`=1 WHERE `id` IN (".$emplString.")";
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
			}
			//set all to unchecked
			$sql = "UPDATE `testers` SET `checked`=0";
			$result = mysql_query($sql);
			if (mysql_errno()!=0 || !$result)
				echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
			//set only the checked ones to checked in the db
			if ($testerString != "")
			{
				$sql = "UPDATE `testers` SET `checked`=1 WHERE `id` IN (".$testerString.")";
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
			}

			if (isset($_POST['SubmitRandom']))
			{
				$sql = "SELECT `id`,`missiontype` FROM `report` WHERE `missiontype` IN (".$emplString.")";
				//echo "query1:".$sql."<br />";
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				$currentMax = 1;
				$testersInced = 0;

				$missionsPerTester[$numTesters];
				for ($i = 0; $i < $numTesters; $i++)
				{
					$missionsPerTester[$i] = 0;
				}

				while($row = mysql_fetch_object($result))
				{
					$found = -1;
					while ($found == -1)
					{
						$thisTester = rand(0,$numTesters-1);
						if ($missionsPerTester[$thisTester] < $currentMax)
						{
							$found = $thisTester;
							$missionsPerTester[$thisTester]++;
						}
					}
					$testersInced++;
					if ($testersInced == $numTesters)
					{
						$currentMax++;
						$testersInced = 0;
					}
					$query = "UPDATE `report` Set `tester` = '".$testers[$found]."' WHERE `id` = '".$row->id."'";
					//echo "query1:".$query."<br />";
					$result2 = mysql_query($query);
				}
				if (mysql_errno()!=0)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result2 . "</span><br />";
				else
					echo"<span style='color:blue'>Random testers assigned to chosen mission types!</span><br />";
			}
			else
			{
				echo"<span style='color:blue'>Selection saved!</span><br />";
			}
			mysql_close($connection);
		}
	}
?>

<?php
	$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
	mysql_select_db($mysql_database);
?>

<?php
	$buildlist = array();
	$sql = "SELECT `name` FROM `buildtypes` ORDER BY `id`";
	$result = mysql_query($sql);
	if (mysql_errno()!=0 || !$result)
		echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
	else
		while($row = mysql_fetch_object($result))
			$buildlist[] = $row->name;
?>

<br />
<b>Note, that you might want to create a <a href="backups.php<?php echo $param_db_suffix; ?>">backup</a> of the build report,<br />before clearing or reassigning the current one.</b><br />
<br /><br />
<form action="assign.php<?php echo $param_db_suffix; ?>" method="post">
<b>Clear Report Colums:</b><br />
<table border="1" style="border-collapse:collapse">
<tr>
<td><input type="checkbox" name="checkallClearColumns" checked="checked" onclick="toggleAllCheckboxes(this, '');" /> All<br /></td>
<td><input type="checkbox" name="tester" checked="checked" /> Tester<br /></td>
<?php
	$numBuilds = 0;
	foreach ($buildlist as $type)
	{
		$numBuilds++;
		echo '<td><input type="checkbox" name="buildtypes[]" checked="checked" value="'.$type.'" /> '.$type.'<br /></td>';
	}
?>
<td><input type="checkbox" name="bugno" checked="checked" /> Bug#<br /></td>
<td><input type="checkbox" name="owner" checked="checked" /> Owner<br /></td>
<td><input type="checkbox" name="fix_status" checked="checked" /> FixStatus<br /></td>
<td><input type="checkbox" name="notes" checked="checked" /> Notes<br /></td>
</tr>
<tr>
<?php
echo '<td colspan="'. ($numBuilds+6) .'"><input type="submit" name="SubmitDelete" value="Clear" /></td>';
?>
</tr>
</table>
</form>
<br /><br /><br />

<form action="assign.php<?php echo $param_db_suffix; ?>" method="post">
<b>Assign selected testers randomly to missions<br />matching the selected mission types:</b><br />
<table border="1" style="border-collapse:collapse">
<tr><td align="center" colspan="2"><input type="submit" name="SubmitRandom" value="Assign" />&nbsp;&nbsp;<input type="submit" name="SubmitSaveSelection" value="Save Selection" /></td></tr>
<tr><td align="center"><b>Mission Type</b></td><td align="center"><b>Testers</b></td></tr>
<tr>
	<td align="left"><input type='checkbox' name='checkallMissions' onclick="toggleAllCheckboxes(this, 'missiontypes[]');" /> All</td>
	<td align="left"><input type='checkbox' name='checkallTesters' onclick="toggleAllCheckboxes(this, 'testers[]');" /> All</td>
</tr>
<tr><td align="left" valign="top" style="font-size:80%">
<?
	$sql = "SELECT `id`,`name`,`checked` FROM `missiontypes`";
	$result = mysql_query($sql);
	if (mysql_errno()!=0 || !$result)
		echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
	while($row = mysql_fetch_object($result))
	{
		$checked = ($row->checked == 1)?"checked='checked'":"";
		echo "<input type='checkbox' name='missiontypes[]' value='".$row->id."' ".$checked." /> ".$row->name."<br />";
	}
?>
</td><td align="left" valign="top" style="font-size:80%">
<?
	$sql = "SELECT `id`,`name`,`checked` FROM `testers` ORDER BY CONVERT (name USING latin2)";
	$result = mysql_query($sql);
	if (mysql_errno()!=0 || !$result)
		echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
	while($row = mysql_fetch_object($result))
	{
		$checked = ($row->checked == 1)?"checked='checked'":"";
		echo "<input type='checkbox' name='testers[]' value='".$row->id."' ".$checked." /> ".$row->name."<br />";
	}
?>
</td></tr>
<tr>
	<td align="left"><input type='checkbox' name='checkallMissions' onclick="toggleAllCheckboxes(this, 'missiontypes[]');" /> All</td>
	<td align="left"><input type='checkbox' name='checkallTesters' onclick="toggleAllCheckboxes(this, 'testers[]');" /> All</td>
</tr>
<tr><td align="center" colspan="2"><input type="submit" name="SubmitRandom" value="Assign" />&nbsp;&nbsp;<input type="submit" name="SubmitSaveSelection" value="Save Selection" /></td></tr>
</table>
</form>
<?
	mysql_close($connection);
?>
</body>
</html>
