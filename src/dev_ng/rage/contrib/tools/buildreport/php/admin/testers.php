<?php
/*
(c) 11/03/09 - David Huebner - Rockstar Leeds
R* Test Plan web tool to help with testing builds with many tester.

Uses phpMyDataGrid 2007 by Gur� Sistemas and/or Gustavo Adolfo Arcila Trujillo (www.gurusistemas.com)

Tested to work on PHP5.x and MySql 5.x, Javascript enabled. Best on Firefox.
Internet Explorer has speed issues when building up the huge table.

Note that testers should always hit the refresh button before they assign a mission to themself, to make sure that nobody else has taken the mission in the meanwhile.
*/

//this is the administration page for the testers

	include ("../config.php");
	$pagetitle = "Testers";
	$curent_page = "testers";
	include ("adminmenu.php");


	if (isset($_POST['SubmitDelete']))
	{
		$testers = $_POST["testers"];
		$numtesters = count($testers);
		if ($numtesters == 0)
		{
			echo"<span style='color:red'>You need to select the testers you want to delete!</span><br />";
		}
		else
		{
			$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
			mysql_select_db($mysql_database);
			rsort($testers);
			foreach ($testers as $id)
			{
				$sql = "DELETE FROM `testers` WHERE `id`=$id";
				//echo "query1:".$sql."<br />";
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";

				$query = "UPDATE `report` SET `tester`=0 WHERE `tester`=$id";
				$result = mysql_query($query);
				if (mysql_errno()!=0)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				$query = "UPDATE `report` SET `scripter`=0 WHERE `scripter`=$id";
				$result = mysql_query($query);
				if (mysql_errno()!=0)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				$query = "UPDATE `report` SET `owner`=0 WHERE `owner`=$id";
				$result = mysql_query($query);
				if (mysql_errno()!=0)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				else
					echo"<span style='color:blue'>Tester ". $id ." deleted!</span><br />";

				//decrement all from this number on
				$sql = "UPDATE `report` SET scripter = scripter - 1 WHERE scripter > ".$id;
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				$sql = "UPDATE `report` SET tester = tester - 1 WHERE tester  > ".$id;
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				$sql = "UPDATE `report` SET owner = owner - 1 WHERE owner  > ".$id;
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				$sql = "UPDATE `testers` SET id = id - 1 WHERE id > ".$id;
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";

			}
			mysql_close($connection);
		}
	}
	if (isset($_POST['SubmitNew']))
	{
		if (!$_POST["testername"] || $_POST["testername"] == "")
		{
			echo"<span style='color:red'>No name for the new tester specified</span><br />";
		}
		else
		{
			$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
			mysql_select_db($mysql_database);
			//$testername = ereg_replace("[)( .-]","",$_POST["testername"]);
			$testername = mysql_real_escape_string($_POST["testername"]);

			//insert the new tester at the alphabetically correct place
			$sql = "SELECT `id`,`name` FROM `testers` ORDER BY `id`";
			$result = mysql_query($sql);
			if (mysql_errno()!=0 || !$result)
				echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
			$foundBiggerName = false;
			$currId = 0;
			while($row = mysql_fetch_object($result))
			{
				$currId = $row->id;
				if (strcasecmp($testername, $row->name) <= 0)
				{
					$foundBiggerName = true;
					break;
				}
			}

			if ($foundBiggerName == true)
			{
				//echo "found bigger name:" . $currId ."<br />";
				//increment all from this number on
				$sql = "UPDATE `report` SET scripter = scripter + 1 WHERE scripter >= ".$currId;
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				$sql = "UPDATE `report` SET tester = tester + 1 WHERE tester >= ".$currId;
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				$sql = "UPDATE `report` SET owner = owner + 1 WHERE owner >= ".$currId;
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				$sql = "UPDATE `testers` SET id = id + 1 WHERE id >= ".$currId;
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
			}
			else
			{
				//adding the new name to the end
				$currId++;
			}

			echo "adding new one<br />";
			$query = "INSERT INTO testers (id, name) VALUES ($currId, '$testername')";
			$result = mysql_query($query);
			if (mysql_errno()!=0)
				echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
			else
				echo"<span style='color:blue'>Tester $testername created!</span><br />";
			mysql_close($connection);
		}
	}

	if (isset($_POST['SubmitSave']))
	{
		$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
		mysql_select_db($mysql_database);
		$new_name = $_POST['name'];
		$new_name = mysql_real_escape_string($new_name);
		if ($new_name == "")
		{
			echo "<span style='color:red'>Name can not be left empty.</span><br />";
		}
		else
		{
			$query = "UPDATE `testers` SET name='".$new_name."' WHERE `id`=".$_POST['id'];
			$result = mysql_query($query);
			if (mysql_errno()!=0)
				echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
			else
				echo"<span style='color:blue'>Tester renamed!</span><br />";
		}
		mysql_close($connection);
	}

	$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
	mysql_select_db($mysql_database);

	foreach($_POST as $k => $v)
	{
		if (substr($k, 0, 12) == "SubmitEditId")
		{
			preg_match('{(\d+)}', $k, $m);
			$editId = $m[1];

			if ($editId == "" || !is_numeric($editId))
			{
				echo "<span style='color:red'>Wrong parameters to edit tester.</span><br />";
			}
			else
			{
				$query = "SELECT * FROM `testers` WHERE `id` = ".$editId;
				$result=mysql_query($query);
				if (mysql_errno()!=0)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				if (mysql_num_rows($result) == 0)
				{
					echo "<span style='color:red'>Tester id does not exist.</span><br />";
				}
				else
				{
					$row = mysql_fetch_object($result);
	?>
					<b>Edit a tester:</b><br />
					<form name="EditForm" action="testers.php<?php echo $param_db_suffix; ?>" method="post">
					<table border="1" style="border-collapse:collapse">
					<tr>
	<?php
					echo '<td><input type="hidden" name="id" value="'.$row->id.'" /><input id="onloadFocusFirst" type="text" name="name" size="30" maxlength="40" value="'.$row->name.'" /></td><td>&nbsp;&nbsp;&nbsp;<input type="submit" name="SubmitSave" value="Save" /></td></tr></table></form>';
				}
			}
			break;
		}
	}
?>

<br />
<form action="testers.php<?php echo $param_db_suffix; ?>" method="post">
<b>Add testers (/scripters):</b><br />
<table border="1" style="border-collapse:collapse"><tr>
<td><input id='onloadFocus' name="testername" type="text" size="30" maxlength="40" /></td>
<td>&nbsp;&nbsp;&nbsp;<input type="submit" name="SubmitNew" value="Add" /></td>
</tr></table>
</form>
<br /><br />
<form action="testers.php<?php echo $param_db_suffix; ?>" method="post">
<b>Delete testers (/scripters):</b><br />
<table border="1" style="border-collapse:collapse">
<tr>
<td><input type="submit" name="SubmitDelete" value="Delete" /></td>
</tr>
<tr>
<td><input type='checkbox' name='checkall' onclick="toggleAllCheckboxes(this, 'testers[]');" /> All</td>
</tr>
<tr><td>
<table border="0">
<?
	$sql = "SELECT `id`,`name` FROM `testers` ORDER BY `id`";
	$result = mysql_query($sql);
	if (mysql_errno()!=0 || !$result)
		echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
	while($row = mysql_fetch_object($result))
	{
		echo "<tr><td><input type='checkbox' name='testers[]' value='".$row->id."' /></td><td>".$row->name."</td>
		<td><input type='image' src='../images/edit.gif' name='SubmitEditId*".$row->id."*' alt='Edit Tester' /></td></tr>";
	}
?>
</table>
</td></tr>
<tr>
<td><input type='checkbox' name='checkall' onclick="toggleAllCheckboxes(this, 'testers[]');" /> All</td>
</tr>
<tr>
<td><input type="submit" name="SubmitDelete" value="Delete" /></td>
</tr>
</table>
</form>
<br /><br /><br />

<?
	mysql_close($connection);
?>
</body>
</html>
