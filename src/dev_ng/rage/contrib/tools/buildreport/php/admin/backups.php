<?php
/*
(c) 11/03/09 - David Huebner - Rockstar Leeds
R* Test Plan web tool to help with testing builds with many tester.

Uses phpMyDataGrid 2007 by Gur� Sistemas and/or Gustavo Adolfo Arcila Trujillo (www.gurusistemas.com)

Tested to work on PHP5.x and MySql 5.x, Javascript enabled. Best on Firefox.
Internet Explorer has speed issues when building up the huge table.

Note that testers should always hit the refresh button before they assign a mission to themself, to make sure that nobody else has taken the mission in the meanwhile.
*/

//this is the administration page for the backups

	include ("../config.php");
	$pagetitle = "Backups";
	$curent_page = "backups";
	include ("adminmenu.php");

	if (isset($_POST['SubmitDelete']))
	{
		$backups = $_POST["backups"];
		$numbackups = count($backups);
		if ($numbackups == 0)
		{
			echo"<span style='color:red'>You need to select the backups you want to delete!</span><br />";
		}
		else
		{
			$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
			mysql_select_db($mysql_database);
			foreach ($backups as $table)
			{
				if ($table != "report" && strncmp($table, "rep", 3) == 0)
				{
					$sql = "DROP TABLE `".$table."`";
					$result = mysql_query($sql);
					if (mysql_errno()!=0 || !$result)
						echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
					else
						echo"<span style='color:blue'>Backup " . $table . " deleted!</span><br />";
				}
			}
			mysql_close($connection);
		}
	}
	if (isset($_POST['SubmitBackup']))
	{
		$timestamp = time();
		$timedate = "rep" . date("y-m-d_H-i-s",$timestamp);

		$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
		mysql_select_db($mysql_database);

		$buildstrSelect = "";
		$buildstrCreate = "";
		$buildstrInsert = "";
		$buildlist = array();
		$query = "SELECT `name` FROM `buildtypes` ORDER BY `id`";
		$result = mysql_query($query);
		if (mysql_errno()!=0 || !$result)
			echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
		else if (mysql_num_rows($result) != 0)
		{
			while($row = mysql_fetch_object($result))
			{
				$buildstrSelect .= ",r.".$row->name;
				$buildstrCreate .= "`".$row->name."` smallint(6) DEFAULT NULL,";
				$buildstrInsert .= ",".$row->name;
				$buildlist[] = $row->name;
			}
		}

		//CREATE BACKUP TABLE
		$query = 'CREATE TABLE IF NOT EXISTS `'.$timedate.'` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `order_id` int(11) NOT NULL,
			  `mission_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
			  `missiontype` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
			  `mission_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `scripter` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
			  `tester` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
			  `owner` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
			  `fix_status` smallint(6) NOT NULL,
			  `notes` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
			  `bug_no` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
			  '.$buildstrCreate.'
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;';
		$result = mysql_query($query);
		if (mysql_errno()!=0)
			echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";

		//GET ALL THE DATA
		$query = "SELECT r.id,r.order_id,r.mission_id,m.name AS missiontype,r.mission_title,s.name AS scripter,t.name AS tester,o.name AS owner,r.fix_status,r.notes,r.bug_no".$buildstrSelect."
				FROM report AS r
				LEFT JOIN missiontypes AS m ON r.missiontype = m.id
				LEFT JOIN testers AS s ON r.scripter = s.id
				LEFT JOIN testers AS t ON r.tester = t.id
				LEFT JOIN testers AS o ON r.owner = o.id
				";
		$result = mysql_query($query);
		if (mysql_errno()!=0)
			echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
		else
		{
			while($row = mysql_fetch_assoc($result))
			{
				$buildValues = "";
				foreach ($buildlist as $build)
				{
					if (!$row[$build])
						$buildValues .= ",0";
					else
						$buildValues .= ",".$row[$build];
				}
				$query = "INSERT INTO `".$timedate."` (id, order_id, mission_id, missiontype, mission_title, scripter, tester, owner, fix_status, notes, bug_no ".$buildstrInsert.") 
						VALUES (".$row['id'].", ".$row['order_id'].", '".mysql_real_escape_string($row['mission_id'])."', '".mysql_real_escape_string($row['missiontype'])."',
						'".mysql_real_escape_string($row['mission_title'])."', '".mysql_real_escape_string($row['scripter'])."', '".mysql_real_escape_string($row['tester'])."',
						'".mysql_real_escape_string($row['owner'])."', ".$row['fix_status'].", '".mysql_real_escape_string($row['notes'])."', '".mysql_real_escape_string($row['bug_no'])."'".$buildValues.")";
				$result2 = mysql_query($query);
				if (mysql_errno()!=0)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result2 . "</span><br />";
			}

			echo"<span style='color:blue'>Backup " . $timedate . " created!</span><br />";
		}

		mysql_close($connection);
	}
?>

<?php
	$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
	mysql_select_db($mysql_database);
?>

<?php
	$buildlist = array();
	$sql = "SELECT `name` FROM `buildtypes` ORDER BY `id`";
	$result = mysql_query($sql);
	if (mysql_errno()!=0 || !$result)
		echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
	else
		while($row = mysql_fetch_object($result))
			$buildlist[] = $row->name;
?>

<br />
<form action="backups.php<?php echo $param_db_suffix; ?>" method="post">
<b>Create Backup</b><br />
<input type="submit" name="SubmitBackup" value="Create Backup" />
</form>
<br /><br />
<form action="backups.php<?php echo $param_db_suffix; ?>" method="post">
<b>Delete backups:</b><br />
<table border="1" style="border-collapse:collapse">
<tr>
<td><input type="submit" name="SubmitDelete" value="Delete" /></td>
</tr>
<tr>
<td><input type='checkbox' name='checkall' onclick="toggleAllCheckboxes(this, 'backups[]');" /> All</td>
</tr>
<tr><td>
<?
	$sql = "SHOW TABLES FROM `".$mysql_database."`";
	$result = mysql_query($sql);
	if (mysql_errno()!=0 || !$result)
		echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";

	while ($row = mysql_fetch_row($result))
	{
		$name = $row[0];
		if ($name != "report" && strncmp($name, "rep", 3) == 0)
		{
			echo "<input type='checkbox' name='backups[]' value='".$name."' /> ".$name."<br />";
		}
	}
?>
</td></tr>
<tr>
<td><input type='checkbox' name='checkall' onclick="toggleAllCheckboxes(this, 'backups[]');" /> All</td>
</tr>
<tr>
<td><input type="submit" name="SubmitDelete" value="Delete" /></td>
</tr>
</table>
</form>
<br /><br /><br />
<?
	mysql_close($connection);
?>
</body>
</html>
