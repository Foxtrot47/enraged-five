<?php
/*
(c) 11/03/09 - David Huebner - Rockstar Leeds
R* Test Plan web tool to help with testing builds with many tester.

Uses phpMyDataGrid 2007 by Gur� Sistemas and/or Gustavo Adolfo Arcila Trujillo (www.gurusistemas.com)

Tested to work on PHP5.x and MySql 5.x, Javascript enabled. Best on Firefox.
Internet Explorer has speed issues when building up the huge table.

Note that testers should always hit the refresh button before they assign a mission to themself, to make sure that nobody else has taken the mission in the meanwhile.
*/

//this is the project administration page

	$pagetitle = "R*Test Plan - Project Management";
	$noRedirect = true;
	include ("../config.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>
<?php echo $pagetitle; ?>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta http-equiv="imagetoolbar" content="no" />
<style type="text/css">
<!--
table {border-color:#aaaaaa}
td {text-align:center}
th {text-align:center}
.grey {background-color:#dddddd}
a { text-decoration:underline; color:105eaa; }
a:link { text-decoration:underline; color:#105eaa; }
a:visited { text-decoration:underline; color:#105eaa; }
a:hover { text-decoration:underline; color:black; }
a:active { text-decoration:underline; color:#105eaa; }
a:focus { text-decoration:underline; color:#105eaa; }
-->
</style>
<script type="text/javascript">
<!--
function chkFormInputIsNumeric (formElement)
{
  var chkZ = 1;
  for (i = 0; i < formElement.value.length; ++i)
  if (formElement.value.charAt(i) < "0" ||
      formElement.value.charAt(i) > "9")
    chkZ = -1;
  if (chkZ == -1) {
    alert("Some fields only allow numbers!");
    formElement.focus();
    return false;
  }
  return true;
}

function chkFormInputIsValid (formElement)
{
  var chkZ = 1;
  for (i = 0; i < formElement.value.length; ++i)
  if ( !( (formElement.value.charAt(i) >= "0" && formElement.value.charAt(i) <= "9")
	  || (formElement.value.charAt(i) >= "A" && formElement.value.charAt(i) <= "Z")
	  || (formElement.value.charAt(i) >= "a" && formElement.value.charAt(i) <= "z")
  	  || (formElement.value.charAt(i) == "_") ) )
    chkZ = -1;
  if (chkZ == -1) {
    alert("Some fields only allow numbers!");
    formElement.focus();
    return false;
  }
  return true;
}

function chkFormInputIsFilledIn (formElement)
{
  if (formElement.value == "") {
    alert("All input fields must be filled out");
    formElement.focus();
    return false;
  }
  return true;
}

function chkEditForm ()
{
  if (!chkFormInputIsFilledIn(document.EditForm.name)) return false;
  if (!chkFormInputIsNumeric(document.EditForm.repeat_header)) return false;
  if (!chkFormInputIsNumeric(document.EditForm.width_id)) return false;
  if (!chkFormInputIsNumeric(document.EditForm.width_mission_id)) return false;
  if (!chkFormInputIsNumeric(document.EditForm.width_mission_type)) return false;
  if (!chkFormInputIsNumeric(document.EditForm.width_mission_title)) return false;
  if (!chkFormInputIsNumeric(document.EditForm.width_scripter)) return false;
  if (!chkFormInputIsNumeric(document.EditForm.width_tester)) return false;
  if (!chkFormInputIsNumeric(document.EditForm.width_build_types)) return false;
  if (!chkFormInputIsNumeric(document.EditForm.width_bug_no)) return false;
  if (!chkFormInputIsNumeric(document.EditForm.width_owner)) return false;
  if (!chkFormInputIsNumeric(document.EditForm.width_fix_status)) return false;
  if (!chkFormInputIsNumeric(document.EditForm.width_notes)) return false;
}

function chkAddForm ()
{
  if (!chkFormInputIsFilledIn(document.AddForm.name)) return false;
  if (!chkFormInputIsFilledIn(document.AddForm.db_suffix)) return false;
  if (!chkFormInputIsValid(document.AddForm.db_suffix)) return false;
  if (!chkFormInputIsNumeric(document.AddForm.copy_id)) return false;
}

function chkDeletePoject (projectName)
{
	return confirm("Are you sure you want to delete the project " + projectName + "? This will delete all the project's data from the database.");
}
//-->
</script>

</head>

<body style="font-family:Verdana, Arial; font-size:0.8em">
<br />

<?php

	function Ena($enabled)
	{
		if ($enabled)
			return '<img src="../images/enabled.gif" width="12" height="12" alt="enabled" />';
		return '<img src="../images/disabled.gif" width="12" height="12" alt="disabled" />';
	}

	function EnaIfZero($enabled)
	{
		if ($enabled == 0)
			return Ena($enabled);
		return $enabled;
	}

	function ChkBox($chkBoxState)
	{
		if ($chkBoxState == "on")
			return 1;
		return 0;
	}

	$editMode = false;
	$editProjectForm = "";
	$actionText = "";

	//check if project database exists or if it is empty
	$projectListEmpty = true;
	$projectList = "";
	$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
	if (!$connection) //no connection to database
	{
		$actionText .= "Can't connect to mysql database. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
	}
	else
	{
		$db_selected = mysql_select_db($mysql_config_database, $connection);
		if (!$db_selected)
		{
			$actionText .= "Created project database.<br />";
			include ("create_projects_db.php"); //create database
		}
		else
		{
			//check the get and post variables
			if (isset($_POST['SubmitEdit']) && isset($_POST['id']))
			{
				$editId = $_POST['id'];
				if ($editId == "" || !is_numeric($editId))
				{
					$actionText .= "Wrong parameters to edit project.<br />";
				}
				else
				{
					$query = "SELECT * FROM `projects` WHERE `id` = ".$editId;
					$result=mysql_query($query);
					if (mysql_errno()!=0 || !$result)
					{
						$actionText .= "Error querying the database. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
					}
					if (mysql_num_rows($result) == 0)
					{
						$actionText .= "Project id does not exist.<br />";
					}
					else
					{
						$editMode = true;
						$row = mysql_fetch_object($result);
						$editProjectForm = '
							<tr><td><input type="submit" name="SubmitSave" value="Save" /></td>
							<td><input type="hidden" name="id" value="'.$row->id.'" />'.$row->id.'</td>
							<td><input type="text" name="name" size="20" maxlength="100" value="'.$row->name.'" /></td>
							<td>'.$row->db_suffix.'</td>
							<td><input type="checkbox" name="enabled" '.(($row->enabled==0)?'':'checked="checked"').' /></td>
							<td><input type="text" name="repeat_header" size="1" maxlength="4" value="'.$row->repeat_header.'" /></td>
							<td><input type="checkbox" name="show_id" '.(($row->show_id==0)?'':'checked="checked"').' /></td><td><input type="text" name="width_id" size="1" maxlength="4" value="'.$row->width_id.'" /></td><td>'.Ena(1).'</td>
							<td><input type="checkbox" name="show_mission_id" '.(($row->show_mission_id==0)?'':'checked="checked"').' /></td><td><input type="text" name="width_mission_id" size="1" maxlength="4" value="'.$row->width_mission_id.'" /></td><td><input type="checkbox" name="locked_mission_id" '.(($row->locked_mission_id==0)?'':'checked="checked"').' /></td>
							<td><input type="checkbox" name="show_mission_type" '.(($row->show_mission_type==0)?'':'checked="checked"').' /></td><td><input type="text" name="width_mission_type" size="1" maxlength="4" value="'.$row->width_mission_type.'" /></td><td><input type="checkbox" name="locked_mission_type" '.(($row->locked_mission_type==0)?'':'checked="checked"').' /></td>
							<td><input type="checkbox" name="show_mission_title" '.(($row->show_mission_title==0)?'':'checked="checked"').' /></td><td><input type="text" name="width_mission_title" size="1" maxlength="4" value="'.$row->width_mission_title.'" /></td><td><input type="checkbox" name="locked_mission_title" '.(($row->locked_mission_title==0)?'':'checked="checked"').' /></td>
							<td><input type="checkbox" name="show_scripter" '.(($row->show_scripter==0)?'':'checked="checked"').' /></td><td><input type="text" name="width_scripter" size="1" maxlength="4" value="'.$row->width_scripter.'" /></td><td><input type="checkbox" name="locked_scripter" '.(($row->locked_scripter==0)?'':'checked="checked"').' /></td>
							<td><input type="checkbox" name="show_tester" '.(($row->show_tester==0)?'':'checked="checked"').' /></td><td><input type="text" name="width_tester" size="1" maxlength="4" value="'.$row->width_tester.'" /></td><td><input type="checkbox" name="locked_tester" '.(($row->locked_tester==0)?'':'checked="checked"').' /></td>
							<td><input type="checkbox" name="show_build_types" '.(($row->show_build_types==0)?'':'checked="checked"').' /></td><td><input type="text" name="width_build_types" size="1" maxlength="4" value="'.$row->width_build_types.'" /></td><td><input type="checkbox" name="locked_build_types" '.(($row->locked_build_types==0)?'':'checked="checked"').' /></td>
							<td><input type="checkbox" name="show_bug_no" '.(($row->show_bug_no==0)?'':'checked="checked"').' /></td><td><input type="text" name="width_bug_no" size="1" maxlength="4" value="'.$row->width_bug_no.'" /></td><td><input type="checkbox" name="locked_bug_no" '.(($row->locked_bug_no==0)?'':'checked="checked"').' /></td>
							<td><input type="checkbox" name="show_owner" '.(($row->show_owner==0)?'':'checked="checked"').' /></td><td><input type="text" name="width_owner" size="1" maxlength="4" value="'.$row->width_owner.'" /></td><td><input type="checkbox" name="locked_owner" '.(($row->locked_owner==0)?'':'checked="checked"').' /></td>
							<td><input type="checkbox" name="show_fix_status" '.(($row->show_fix_status==0)?'':'checked="checked"').' /></td><td><input type="text" name="width_fix_status" size="1" maxlength="4" value="'.$row->width_fix_status.'" /></td><td><input type="checkbox" name="locked_fix_status" '.(($row->locked_fix_status==0)?'':'checked="checked"').' /></td>
							<td><input type="checkbox" name="show_notes" '.(($row->show_notes==0)?'':'checked="checked"').' /></td><td><input type="text" name="width_notes" size="1" maxlength="4" value="'.$row->width_notes.'" /></td><td><input type="checkbox" name="locked_notes" '.(($row->locked_notes==0)?'':'checked="checked"').' /></td></tr>';
					}
				}
			}

			if (isset($_POST['SubmitSave']))
			{
				$new_name = mysql_real_escape_string($_POST['name']);
				$header_repetition = mysql_real_escape_string($_POST['repeat_header']);
				if (!is_numeric($header_repetition))
					$header_repetition = 0;
				$query = "UPDATE `projects` SET 
					name='".$new_name."',
					enabled=".ChkBox($_POST['enabled']).",
					repeat_header=".$header_repetition.",
					show_id=".ChkBox($_POST['show_id']).",width_id=".$_POST['width_id'].",
					show_mission_id=".ChkBox($_POST['show_mission_id']).",width_mission_id=".$_POST['width_mission_id'].",locked_mission_id=".ChkBox($_POST['locked_mission_id']).",
					show_mission_type=".ChkBox($_POST['show_mission_type']).",width_mission_type=".$_POST['width_mission_type'].",locked_mission_type=".ChkBox($_POST['locked_mission_type']).",
					show_mission_title=".ChkBox($_POST['show_mission_title']).",width_mission_title=".$_POST['width_mission_title'].",locked_mission_title=".ChkBox($_POST['locked_mission_title']).",
					show_scripter=".ChkBox($_POST['show_scripter']).",width_scripter=".$_POST['width_scripter'].",locked_scripter=".ChkBox($_POST['locked_scripter']).",
					show_tester=".ChkBox($_POST['show_tester']).",width_tester=".$_POST['width_tester'].",locked_tester=".ChkBox($_POST['locked_tester']).",
					show_build_types=".ChkBox($_POST['show_build_types']).",width_build_types=".$_POST['width_build_types'].",locked_build_types=".ChkBox($_POST['locked_build_types']).",
					show_bug_no=".ChkBox($_POST['show_bug_no']).",width_bug_no=".$_POST['width_bug_no'].",locked_bug_no=".ChkBox($_POST['locked_bug_no']).",
					show_owner=".ChkBox($_POST['show_owner']).",width_owner=".$_POST['width_owner'].",locked_owner=".ChkBox($_POST['locked_owner']).",
					show_fix_status=".ChkBox($_POST['show_fix_status']).",width_fix_status=".$_POST['width_fix_status'].",locked_fix_status=".ChkBox($_POST['locked_fix_status']).",
					show_notes=".ChkBox($_POST['show_notes']).",width_notes=".$_POST['width_notes'].", locked_notes=".ChkBox($_POST['locked_notes'])." 
					WHERE `id`=".$_POST['id'];
				$result = mysql_query($query);
				if (mysql_errno()!=0)
					$actionText .= "Error updating the database. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
				else
					$actionText .= "Project &quot;".$_POST['name']."&quot; updated.<br />";
			}

			if (isset($_POST['SubmitDelete']) && isset($_POST['id']))
			{
				$deleteId = $_POST['id'];
				if ($deleteId == "" || !is_numeric($deleteId))
				{
					$actionText .= "Wrong parameters to edit project.<br />";
				}
				else
				{
					$query = "SELECT name,db_suffix FROM `projects` WHERE `id` = ".$deleteId;
					$result=mysql_query($query);
					if (mysql_errno()!=0 || !$result)
					{
						$actionText .= "Error querying the database. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
					}
					if (mysql_num_rows($result) == 0)
					{
						$actionText .= "Project id does not exist.<br />";
					}
					else
					{
						$row = mysql_fetch_object($result);
						$delete_database = "br_".$row->db_suffix;
						$query = "DROP DATABASE ".$delete_database;
						$result=mysql_query($query);
						if (mysql_errno()!=0 || !$result)
						{
							$actionText .= "Error deleting the database. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
						}
						else
						{
							$actionText .= "Database ".$delete_database." deleted.<br />";
						}
						$query = "DELETE FROM `projects` WHERE `id`='".$deleteId."';";
						$result=mysql_query($query);
						if (mysql_errno()!=0 || !$result)
						{
							$actionText .= "Error removing the project entry. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
						}
						else
						{
							$actionText .= "Project ".$row->name." removed.<br />";
						}
					}
				}
			}

			if (isset($_POST['SubmitNew']))
			{
				$canAdd = true;
				$new_name = $_POST['name'];
				$new_name = mysql_real_escape_string($new_name);
				$new_db_suffix = $_POST['db_suffix'];
				$new_db_suffix = ereg_replace("[)( .-]","",$new_db_suffix);
				$new_db_suffix = mysql_real_escape_string($new_db_suffix);
				$copyId = $_POST['copy_id'];
				$draftDatabase = "";
				$copyRow;

				$query = "SELECT * FROM `projects`";
				$result=mysql_query($query);
				if (mysql_errno()!=0 || !$result)
				{
					$actionText .= "Error querying the database. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
				}
				if ($new_db_suffix == "projects")
				{
					$actionText .= "db suffix can not be 'projects'!<br />";
					$canAdd = false;
				}
				else if (mysql_num_rows($result) == 0)
				{
					if ($copyId != "")
					{
						$actionText .= "Project id for draft project does not exist.<br />";
						$canAdd = false;
					}
				}
				else
				{
					while($row = mysql_fetch_object($result))
					{
						if ($row->name == $new_name)
						{
							$actionText .= "Project name already exists.<br />";
							$canAdd = false;
						}
						if ($row->db_suffix == $new_db_suffix)
						{
							$actionText .= "Project database suffix already exists.<br />";
							$canAdd = false;
						}
						if ($copyId != "" && $row->id == $copyId)
						{
							$draftDatabase = "br_".$row->db_suffix;
							$copyRow = $row;
						}
					}
					if ($copyId != "" && $draftDatabase == "")
					{
						$actionText .= "Project id for draft project does not exist.<br />";
						$canAdd = false;
					}
				}

				if ($canAdd)
				{
					$query = "";
					if ($copyId != "" && $_POST['copy_settings'] == "on")
					{
						$query = "INSERT INTO `br_projects`.`projects` (`name` ,`db_suffix` ,`enabled` ,`repeat_header` ,`show_id` ,`show_mission_id` ,`show_mission_type` ,`show_mission_title` ,`show_scripter` ,`show_tester` ,`show_build_types` ,`show_bug_no` ,`show_owner` ,`show_fix_status` ,`show_notes` ,`width_id` ,`width_mission_id` ,`width_mission_type` ,`width_mission_title` ,`width_scripter` ,`width_tester` ,`width_build_types` ,`width_bug_no` ,`width_owner` ,`width_fix_status` ,`width_notes`,`locked_id` ,`locked_mission_id` ,`locked_mission_type` ,`locked_mission_title` ,`locked_scripter` ,`locked_tester` ,`locked_build_types` ,`locked_bug_no` ,`locked_owner` ,`locked_fix_status` ,`locked_notes`)
								VALUES ( '".$new_name."', '".$new_db_suffix."', '0', '".$copyRow->repeat_header."', '".$copyRow->show_id."', '".$copyRow->show_mission_id."', '".$copyRow->show_mission_type."', '".$copyRow->show_mission_title."', '".$copyRow->show_scripter."', '".$copyRow->show_tester."', '".$copyRow->show_build_types."', '".$copyRow->show_bug_no."', '".$copyRow->show_owner."', '".$copyRow->show_fix_status."', '".$copyRow->show_notes."', '".$copyRow->width_id."', '".$copyRow->width_mission_id."', '".$copyRow->width_mission_type."', '".$copyRow->width_mission_title."', '".$copyRow->width_scripter."', '".$copyRow->width_tester."', '".$copyRow->width_build_types."', '".$copyRow->width_bug_no."', '".$copyRow->width_owner."', '".$copyRow->width_fix_status."', '".$copyRow->width_notes."', '1', '".$copyRow->locked_mission_id."', '".$copyRow->locked_mission_type."', '".$copyRow->locked_mission_title."', '".$copyRow->locked_scripter."', '".$copyRow->locked_tester."', '".$copyRow->locked_build_types."', '".$copyRow->locked_bug_no."', '".$copyRow->locked_owner."', '".$copyRow->locked_fix_status."', '".$copyRow->locked_notes."');";
					}
					else
					{
						$query = "INSERT INTO `br_projects`.`projects` (`name` ,`db_suffix` ,`enabled` ,`repeat_header` ,`show_id` ,`show_mission_id` ,`show_mission_type` ,`show_mission_title` ,`show_scripter` ,`show_tester` ,`show_build_types` ,`show_bug_no` ,`show_owner` ,`show_fix_status` ,`show_notes` ,`width_id` ,`width_mission_id` ,`width_mission_type` ,`width_mission_title` ,`width_scripter` ,`width_tester` ,`width_build_types` ,`width_bug_no` ,`width_owner` ,`width_fix_status` ,`width_notes` ,`locked_id` ,`locked_mission_id` ,`locked_mission_type` ,`locked_mission_title` ,`locked_scripter` ,`locked_tester` ,`locked_build_types` ,`locked_bug_no` ,`locked_owner` ,`locked_fix_status` ,`locked_notes`)
								VALUES ( '".$new_name."', '".$new_db_suffix."', '0', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1',
								'40', '150', '50', '215', '80', '80', '40', '40', '80', '70', '495',
								'1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');";
					}
					$result=mysql_query($query);
					if (mysql_errno()!=0)
					{
						$actionText .= "Error adding new project. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
					}
					else
					{
						$actionText .= "Project ".$new_name." added.<br />";
						//add database
						$mysql_database = "br_".$new_db_suffix;
						include ("../create_database.php");
						if ($_POST['copy_mission_types'] == "on")
						{
							//drop old table, recreate it and populate it with the data from the old table
							$query = "DROP TABLE IF EXISTS `missiontypes`;";
							$result=mysql_query($query);
							if (mysql_errno()!=0) $actionText .= "Error copying database table. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
							$query = "CREATE TABLE `missiontypes` LIKE `".$draftDatabase."`.`missiontypes`;";
							$result=mysql_query($query);
							if (mysql_errno()!=0) $actionText .= "Error copying database table. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
							$query = "INSERT INTO `missiontypes` SELECT * FROM `".$draftDatabase."`.`missiontypes`;";
							$result=mysql_query($query);
							if (mysql_errno()!=0) $actionText .= "Error copying database table. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
						}
						if ($_POST['copy_testers'] == "on")
						{
							//drop old table, recreate it and populate it with the data from the old table
							$query = "DROP TABLE IF EXISTS `testers`;";
							$result=mysql_query($query);
							if (mysql_errno()!=0) $actionText .= "Error copying database table. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
							$query = "CREATE TABLE `testers` LIKE `".$draftDatabase."`.`testers`;";
							$result=mysql_query($query);
							if (mysql_errno()!=0) $actionText .= "Error copying database table. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
							$query = "INSERT INTO `testers` SELECT * FROM `".$draftDatabase."`.`testers`;";
							$result=mysql_query($query);
							if (mysql_errno()!=0) $actionText .= "Error copying database table. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
						}
						if ($_POST['copy_test_plan'] == "on")
						{
							//drop old table, recreate it and populate it with the data from the old table
							$query = "DROP TABLE IF EXISTS `report`;";
							$result=mysql_query($query);
							if (mysql_errno()!=0) $actionText .= "Error copying database table. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
							$query = "CREATE TABLE `report` LIKE `".$draftDatabase."`.`report`;";
							$result=mysql_query($query);
							if (mysql_errno()!=0) $actionText .= "Error copying database table. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
							$query = "INSERT INTO `report` SELECT * FROM `".$draftDatabase."`.`report`;";
							$result=mysql_query($query);
							if (mysql_errno()!=0) $actionText .= "Error copying database table. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";

							//drop old table, recreate it and populate it with the data from the old table
							$query = "DROP TABLE IF EXISTS `buildtypes`;";
							$result=mysql_query($query);
							if (mysql_errno()!=0) $actionText .= "Error copying database table. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
							$query = "CREATE TABLE `buildtypes` LIKE `".$draftDatabase."`.`buildtypes`;";
							$result=mysql_query($query);
							if (mysql_errno()!=0) $actionText .= "Error copying database table. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
							$query = "INSERT INTO `buildtypes` SELECT * FROM `".$draftDatabase."`.`buildtypes`;";
							$result=mysql_query($query);
							if (mysql_errno()!=0) $actionText .= "Error copying database table. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
						}

						//back to projects table
						$db_selected = mysql_select_db($mysql_config_database, $connection);
					}
				}
			}

			if (!$editMode)
			{
				$query = "SELECT * FROM `projects` ORDER BY `id`";
				$result=mysql_query($query);
				if (mysql_errno()!=0 || !$result)
					$actionText .= "Error querying the database. (<i>". mysql_errno() . ": " . mysql_error() ."</i>)<br />";
				if (mysql_num_rows($result) != 0)
				{
					$projectListEmpty = false;
					while($row = mysql_fetch_object($result))
					{
						$projectList .= '
							<tr>
								<td><form action="project_admin.php" method="post"><input type="hidden" name="id" value="'.$row->id.'" />
									<input type="hidden" name="SubmitEdit" value="" /><input type="image" src="../images/edit.gif" alt="Edit project '.$row->name.'" /></form></td>
								<td><form action="project_admin.php" method="post" onsubmit="return chkDeletePoject('."'".$row->name."'".')"><input type="hidden" name="id" value="'.$row->id.'" />
									<input type="hidden" name="SubmitDelete" value="" /><input type="image" src="../images/delete.gif" alt="Delete project '.$row->name.'" /></form></td>
								<td>'.$row->id.'</td><td><a href="admintestplan.php?project='.$row->db_suffix.'">'.$row->name.'</a></td>
								<td>'.$row->db_suffix.'</td>
								<td>'.Ena($row->enabled).'</td>
								<td>'.EnaIfZero($row->repeat_header).'</td>
								<td bgcolor="#EEEEEE">'.Ena($row->show_id).'</td><td bgcolor="#EEEEEE">'.$row->width_id.'</td><td bgcolor="#EEEEEE">'.Ena(1).'</td>
								<td>'.Ena($row->show_mission_id).'</td><td>'.$row->width_mission_id.'</td><td>'.Ena($row->locked_mission_id).'</td>
								<td bgcolor="#EEEEEE">'.Ena($row->show_mission_type).'</td><td bgcolor="#EEEEEE">'.$row->width_mission_type.'</td><td bgcolor="#EEEEEE">'.Ena($row->locked_mission_type).'</td>
								<td>'.Ena($row->show_mission_title).'</td><td>'.$row->width_mission_title.'</td><td>'.Ena($row->locked_mission_title).'</td>
								<td bgcolor="#EEEEEE">'.Ena($row->show_scripter).'</td><td bgcolor="#EEEEEE">'.$row->width_scripter.'</td><td bgcolor="#EEEEEE">'.Ena($row->locked_scripter).'</td>
								<td>'.Ena($row->show_tester).'</td><td>'.$row->width_tester.'</td><td>'.Ena($row->locked_tester).'</td>
								<td bgcolor="#EEEEEE">'.Ena($row->show_build_types).'</td><td bgcolor="#EEEEEE">'.$row->width_build_types.'</td><td bgcolor="#EEEEEE">'.Ena($row->locked_build_types).'</td>
								<td>'.Ena($row->show_bug_no).'</td><td>'.$row->width_bug_no.'</td><td>'.Ena($row->locked_bug_no).'</td>
								<td bgcolor="#EEEEEE">'.Ena($row->show_owner).'</td><td bgcolor="#EEEEEE">'.$row->width_owner.'</td><td bgcolor="#EEEEEE">'.Ena($row->locked_owner).'</td>
								<td>'.Ena($row->show_fix_status).'</td><td>'.$row->width_fix_status.'</td><td>'.Ena(!$row->locked_fix_status).'</td>
								<td bgcolor="#EEEEEE">'.Ena($row->show_notes).'</td><td bgcolor="#EEEEEE">'.$row->width_notes.'</td><td bgcolor="#EEEEEE">'.Ena($row->locked_notes).'</td>
							</tr>';
					}
				}
			}
		}
	}


?>
	<table width="99%" border="0"><tr>
		<td align="left" style="text-align:left; color:#105eaa; font-weight:bold; font-size:1.7em">R*<i>Test Plan</i> - Project Management</td>
		<td align="right" style="text-align:right">Back to <a href="../index.php">Project Selection</a></td>
	</tr></table>
	<br />
<?php
	if ($actionText != "") echo "<span style='color:red'>" . $actionText . "</span><br />";
	
	if ($editMode)
	{
?>
		Edit a single project:<br />
		<form name="EditForm" action="project_admin.php" method="post" onsubmit="return chkEditForm()">
			<table border="1" style="border-collapse:collapse; font-size:0.75em">
				<tr><th rowspan="2" valign="top">&nbsp;</th><th rowspan="2" valign="top">Id</th><th rowspan="2" valign="top">Name</th><th rowspan="2" valign="top">DB Suffix</th><th rowspan="2" valign="top">Enabled</th><th rowspan="2" valign="top">Repeat<br />Header</th><th colspan="3" bgcolor="#EEEEEE">Order Id</th><th colspan="3">Mission ID</th><th colspan="3" bgcolor="#EEEEEE">Mission Type</th><th colspan="3">Mission Title</th><th colspan="3" bgcolor="#EEEEEE">Scripter</th><th colspan="3">Tester</th><th colspan="3" bgcolor="#EEEEEE">Build Types</th><th colspan="3">Bug #</th><th colspan="3" bgcolor="#EEEEEE">Owner</th><th colspan="3">Fix Status</th><th colspan="3" bgcolor="#EEEEEE">Notes</th></tr>
				<tr><?php
					for ($line=0; $line<11; $line++)
					{
						$colourTag =  ' bgcolor="#EEEEEE"';
						if ($line % 2 == 1) $colourTag = '';
						echo '<td'.$colourTag.'><img src="../images/show.gif" width="12" height="12" alt="show" /></td><td'.$colourTag.'><img src="../images/width.gif" width="12" height="12" alt="width" /></td><td'.$colourTag.'><img src="../images/locked.gif" width="12" height="12" alt="locked" /></td>';
					}
				?></tr>
<?php
			echo $editProjectForm;
?>
			</table>
		</form>
		<span style="font-size:0.75em"><img src="../images/show.gif" width="12" height="12" alt="show" />&nbsp;=&nbsp;column visible&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/width.gif" width="12" height="12" alt="width" />&nbsp;=&nbsp;width of column&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/locked.gif" width="12" height="12" alt="locked" />&nbsp;=&nbsp;column locked for testers
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;set &quot;Repeat Header&quot; to &quot;0&quot; for no header repetition</span>
		<br /><br /><a href="project_admin.php">Cancel Editing</a>
<?php
	}
	else
	{
		if ($projectListEmpty)
		{
			echo 'No projects available';
		}
		else
		{
?>
			Click the pencil icon next to a project to edit its settings or the bin to delete it:<br />
			<table border="1" style="border-collapse:collapse; font-size:0.75em">
				<tr><th rowspan="2" valign="top">Edit</th><th rowspan="2" valign="top">Delete</th><th rowspan="2" valign="top">Id</th><th rowspan="2" valign="top">Name</th><th rowspan="2" valign="top">DB Suffix</th><th rowspan="2" valign="top">Enabled</th><th rowspan="2" valign="top">Repeat<br />Header</th><th colspan="3" bgcolor="#EEEEEE">Order Id</th><th colspan="3">Mission ID</th><th colspan="3" bgcolor="#EEEEEE">Mission Type</th><th colspan="3">Mission Title</th><th colspan="3" bgcolor="#EEEEEE">Scripter</th><th colspan="3">Tester</th><th colspan="3" bgcolor="#EEEEEE">Build Types</th><th colspan="3">Bug #</th><th colspan="3" bgcolor="#EEEEEE">Owner</th><th colspan="3">Fix Status</th><th colspan="3" bgcolor="#EEEEEE">Notes</th></tr>
				<tr><?php
					for ($line=0; $line<11; $line++)
					{
						$colourTag =  ' bgcolor="#EEEEEE"';
						if ($line % 2 == 1) $colourTag = '';
						echo '<td'.$colourTag.'><img src="../images/show.gif" width="12" height="12" alt="show" /></td><td'.$colourTag.'><img src="../images/width.gif" width="12" height="12" alt="width" /></td><td'.$colourTag.'><img src="../images/locked.gif" width="12" height="12" alt="locked" /></td>';
					}
				?></tr>
<?php
			echo $projectList;
?>
			</table>
			<span style="font-size:0.75em"><img src="../images/show.gif" width="12" height="12" alt="show" />&nbsp;=&nbsp;column visible&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/width.gif" width="12" height="12" alt="width" />&nbsp;=&nbsp;width of column&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/locked.gif" width="12" height="12" alt="locked" />&nbsp;=&nbsp;column locked for testers</span>
<?php
		}
?>
			<br /><br />
			<form name="AddForm" action="project_admin.php" method="post" onsubmit="return chkAddForm()">
				Add a new project:<br />
				<table border="1" style="border-collapse:collapse; font-size:0.75em">
					<tr><th rowspan="2">&nbsp;</th><th rowspan="2" valign="top">Name</th><th rowspan="2" valign="top">DB Suffix</th><th class="grey" colspan="5" valign="top"><span style='color:red'>(optional)</span> Copy parts from another project identified by its Id</th></tr>
					<tr><td class="grey">Draft Project Id</td><td class="grey">Copy Project Settings</td><td class="grey">Copy Mission Types</td><td class="grey">Copy Testers</td><td class="grey">Copy Test Plan &amp; Build Types</td></tr>
					<tr>
						<td><input type="submit" name="SubmitNew" value="Add" /></td>
						<td><input type="text" name="name" size="20" maxlength="100" value="" /></td>
						<td><input type="text" name="db_suffix" size="10" maxlength="20" value="" /></td>
						<td class="grey"><input type="text" name="copy_id" size="1" maxlength="10" value="" /></td>
						<td class="grey"><input type="checkbox" name="copy_settings" /></td>
						<td class="grey"><input type="checkbox" name="copy_mission_types" /></td>
						<td class="grey"><input type="checkbox" name="copy_testers" /></td>
						<td class="grey"><input type="checkbox" name="copy_test_plan" /></td>
					</tr>
				</table>
			</form>

<?php
	}

	mysql_close($connection);
?>
<br /><br /><br />
</body>
</html>
