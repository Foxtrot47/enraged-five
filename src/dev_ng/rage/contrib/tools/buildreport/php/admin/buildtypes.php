<?php
/*
(c) 11/03/09 - David Huebner - Rockstar Leeds
R* Test Plan web tool to help with testing builds with many tester.

Uses phpMyDataGrid 2007 by Gur� Sistemas and/or Gustavo Adolfo Arcila Trujillo (www.gurusistemas.com)

Tested to work on PHP5.x and MySql 5.x, Javascript enabled. Best on Firefox.
Internet Explorer has speed issues when building up the huge table.

Note that testers should always hit the refresh button before they assign a mission to themself, to make sure that nobody else has taken the mission in the meanwhile.
*/

//this is the administration page for the buildtypes

	include ("../config.php");
	$pagetitle = "Build Types";
	$curent_page = "buildtypes";
	include ("adminmenu.php");

	if (isset($_POST['SubmitDelete']))
	{
		$buildtypes = $_POST["buildtypes"];
		$numbuildtypes = count($buildtypes);
		if ($numbuildtypes == 0)
		{
			echo"<span style='color:red'>You need to select the build types you want to delete!</span><br />";
		}
		else
		{
			foreach ($buildtypes as $id)
			{
				$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
				mysql_select_db($mysql_database);

				$sql = "SELECT `name` FROM `buildtypes` WHERE `id`=$id";
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";

				while($row = mysql_fetch_object($result))
				{
					$sql = "DELETE FROM `buildtypes` WHERE `id`=$id";
					$result2 = mysql_query($sql);
					if (mysql_errno()!=0 || !$result2)
						echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result2 . "</span><br />";

					$query = "ALTER TABLE report DROP COLUMN `$row->name`";
					$result3 = mysql_query($query);
					if (mysql_errno()!=0)
						echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result3 . "</span><br />";
					else
						echo"<span style='color:blue'>Build type $row->name deleted!</span><br />";
				}
			}

				mysql_close($connection);
		}
	}
	if (isset($_POST['SubmitNew']))
	{
		if (!$_POST["buildtypename"] || $_POST["buildtypename"] == "")
		{
			echo"<span style='color:red'>No name for the new build type specified</span><br />";
		}
		else
		{
			$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
			mysql_select_db($mysql_database);

			//$buildtypename = ereg_replace("[)( .-]","",$_POST["buildtypename"]);
			$buildtypename = mysql_real_escape_string($_POST["buildtypename"]);
			if ($buildtypename == "" || preg_match("/^[0-9]/",substr($buildtypename,0,1)))
				$buildtypename = '_'.$buildtypename;
			$query = "INSERT INTO buildtypes (name) VALUES ('$buildtypename')";
			$result = mysql_query($query);
			if (mysql_errno()!=0)
				echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";

			$query = "ALTER TABLE report ADD $buildtypename SMALLINT(6)";
			$result = mysql_query($query);
			if (mysql_errno()!=0)
				echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
			else
				echo"<span style='color:blue'>Buildtype $buildtypename created!</span><br />";

			mysql_close($connection);
		}
	}

	if (isset($_POST['SubmitSave']))
	{
		$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
		mysql_select_db($mysql_database);
		$new_name = $_POST['name'];
		$new_name = mysql_real_escape_string($new_name);
		if (preg_match("/^[0-9]/",substr($new_name,0,1)))
			$new_name = '_'.$new_name;
		if ($new_name == "")
		{
			echo "<span style='color:red'>Name can not be left empty.</span><br />";
		}
		else
		{
			$sql = "SELECT `name` FROM `buildtypes` WHERE `id`=".$_POST['id'];
			$result = mysql_query($sql);
			if (mysql_errno()!=0 || !$result)
				echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
			else
			{
				$row = mysql_fetch_object($result);
				$oldname = $row->name;
				$query = "UPDATE `buildtypes` SET name='".$new_name."' WHERE `id`=".$_POST['id'];
				$result = mysql_query($query);
				if (mysql_errno()!=0)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				else
				{
					$query = "ALTER TABLE `report` CHANGE $oldname $new_name smallint(6) DEFAULT NULL";
					$result = mysql_query($query);
					if (mysql_errno()!=0)
						echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
					else
						echo"<span style='color:blue'>Buildtype renamed!</span><br />";
				}
			}
		}
		mysql_close($connection);
	}
?>

<?php
	$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
	mysql_select_db($mysql_database);

	foreach($_POST as $k => $v)
	{
		if (substr($k, 0, 12) == "SubmitEditId")
		{
			preg_match('{(\d+)}', $k, $m);
			$editId = $m[1];
			if ($editId == "" || !is_numeric($editId))
			{
				echo "<span style='color:red'>Wrong parameters to edit buildtype.</span><br />";
			}
			else
			{
				$query = "SELECT * FROM `buildtypes` WHERE `id` = ".$editId;
				$result=mysql_query($query);
				if (mysql_errno()!=0)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				if (mysql_num_rows($result) == 0)
				{
					echo "<span style='color:red'>Buildtype id does not exist.</span><br />";
				}
				else
				{
					$row = mysql_fetch_object($result);
	?>
					<b>Edit a buildtype:</b><br />
					<form name="EditForm" action="buildtypes.php<?php echo $param_db_suffix; ?>" method="post">
					<table border="1" style="border-collapse:collapse">
					<tr>
	<?php
					$displayName = $row->name;
					if ($displayName{0} == "_")
						$displayName = substr($displayName, 1);
					echo '<td><input type="hidden" name="id" value="'.$row->id.'" /><input id="onloadFocusFirst" type="text" name="name" size="30" maxlength="40" value="'.$displayName.'" /></td><td>&nbsp;&nbsp;&nbsp;<input type="submit" name="SubmitSave" value="Save" /></td></tr></table></form>';
				}
			}
			break;
		}
	}
?>

<br />
<form action="buildtypes.php<?php echo $param_db_suffix; ?>" method="post">
<b>Add build types:</b><br />
<table border="1" style="border-collapse:collapse"><tr>
<td><input id='onloadFocus' name="buildtypename" type="text" size="30" maxlength="40" /></td>
<td>&nbsp;&nbsp;&nbsp;<input type="submit" name="SubmitNew" value="Add" /></td>
</tr></table>
</form>
<br /><br />
<form action="buildtypes.php<?php echo $param_db_suffix; ?>" method="post">
<b>Delete build types:</b><br />
<table border="1" style="border-collapse:collapse">
<tr>
<td><input type="submit" name="SubmitDelete" value="Delete" /></td>
</tr>
<tr>
<td><input type='checkbox' name='checkall' onclick="toggleAllCheckboxes(this, 'buildtypes[]');" /> All</td>
</tr>
<tr><td>
<table border="0">
<?
	$sql = "SELECT `id`,`name` FROM `buildtypes` ORDER BY `id`";
	$result = mysql_query($sql);
	if (mysql_errno()!=0 || !$result)
		echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
	while($row = mysql_fetch_object($result))
	{
		$displayName = $row->name;
		if ($displayName{0} == "_")
			$displayName = substr($displayName, 1);
		echo "<tr><td><input type='checkbox' name='buildtypes[]' value='".$row->id."' /></td><td>".$displayName."</td>
		<td><input type='image' src='../images/edit.gif' name='SubmitEditId*".$row->id."*' alt='Edit buildtypes' /></td></tr>";
	}
?>
</table>
</td></tr>
<tr>
<td><input type='checkbox' name='checkall' onclick="toggleAllCheckboxes(this, 'buildtypes[]');" /> All</td>
</tr>
<tr>
<td><input type="submit" name="SubmitDelete" value="Delete" /></td>
</tr>
</table>
</form>
<br /><br /><br />

<?
	mysql_close($connection);
?>
</body>
</html>
