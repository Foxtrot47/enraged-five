<?php /*
(c) 11/03/09 - David Huebner - Rockstar Leeds
R* Test Plan web tool to help with testing builds with many tester.

Uses phpMyDataGrid 2007 by Gur� Sistemas and/or Gustavo Adolfo Arcila Trujillo (www.gurusistemas.com)

Tested to work on PHP5.x and MySql 5.x, Javascript enabled. Best on Firefox.
Internet Explorer has speed issues when building up the huge table.

Note that testers should always hit the refresh button before they assign a mission to themself, to make sure that nobody else has taken the mission in the meanwhile.
*/ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>
<?php echo "R*Test Plan Administration: " . $pagetitle . " - " . $project_name; ?>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta http-equiv="imagetoolbar" content="no" />

<style type="text/css">
<!--
table {border-color:#aaaaaa}
a { text-decoration:underline; color:105eaa; }
a:link { text-decoration:underline; color:#105eaa; }
a:visited { text-decoration:underline; color:#105eaa; }
a:hover { text-decoration:underline; color:black; }
a:active { text-decoration:underline; color:#105eaa; }
a:focus { text-decoration:underline; color:#105eaa; }
-->
</style>

<script type="text/javascript">
<!--
function setFocus()
{
	if (document.getElementById("onloadFocusFirst"))
	{
	     document.getElementById("onloadFocusFirst").focus();
	}
	else if (document.getElementById("onloadFocus"))
	{
	     document.getElementById("onloadFocus").focus();
	}
}
function toggleAllCheckboxes(toggleBox, boxName)
{
	var ignoreNames = false;
	if (boxName == '') ignoreNames = true;
	var parentForm = toggleBox.form;
	var i = 0;
	var makeChecked = toggleBox.checked;
	for(i=0; i<parentForm.length; i++)
	{
		if(parentForm[i].type == 'checkbox' && (ignoreNames == true || parentForm[i].name == boxName || parentForm[i].name == toggleBox.name) )
		{
			parentForm[i].checked = makeChecked;
		}
	}
}
//-->
</script>

</head>

<body style="font-family:Verdana, Arial; font-size:0.8em" onload="setFocus()">
<br />
<span style='color:#105eaa; font-weight:bold; font-size:1.7em'>R*<i>Test Plan</i> - Administration - <?php echo $project_name; ?></span>
<table border="1" cellpadding="5" style="border-collapse:collapse"><tr>
<td><a href="project_admin.php">Project Management</a></td>
<?php if ($displayTestPlanLink) echo '<td><a href="../testplan.php'.$param_db_suffix.'">Test Plan</a></td>' ?>
<td><a href="admintestplan.php<?php echo $param_db_suffix; ?>">Test Plan (admin. mode)</a></td>
<?php
if ($curent_page == "assign") echo '<td bgcolor="#CCCCCC">Assign testers</td>'; else echo '<td><a href="assign.php'.$param_db_suffix.'">Assign testers</a></td>';
if ($curent_page == "backups") echo '<td bgcolor="#CCCCCC">Backups</td>'; else echo '<td><a href="backups.php'.$param_db_suffix.'">Backups</a></td>';
if ($curent_page == "missions") echo '<td bgcolor="#CCCCCC">Missions</td>'; else echo '<td><a href="missions.php'.$param_db_suffix.'">Missions</a></td>';
if ($curent_page == "testers") echo '<td bgcolor="#CCCCCC">Testers</td>'; else echo '<td><a href="testers.php'.$param_db_suffix.'">Testers</a></td>';
if ($curent_page == "missiontypes") echo '<td bgcolor="#CCCCCC">Missiontypes</td>'; else echo '<td><a href="Missiontypes.php'.$param_db_suffix.'">Missiontypes</a></td>';
if ($curent_page == "buildtypes") echo '<td bgcolor="#CCCCCC">Buildtypes</td>'; else echo '<td><a href="buildtypes.php'.$param_db_suffix.'">Buildtypes</a></td>';
?>
</tr></table>
<br /><br />