<?php
/*
(c) 11/03/09 - David Huebner - Rockstar Leeds
R* Test Plan web tool to help with testing builds with many tester.

Uses phpMyDataGrid 2007 by Gur� Sistemas and/or Gustavo Adolfo Arcila Trujillo (www.gurusistemas.com)

Tested to work on PHP5.x and MySql 5.x, Javascript enabled. Best on Firefox.
Internet Explorer has speed issues when building up the huge table.

Note that testers should always hit the refresh button before they assign a mission to themself, to make sure that nobody else has taken the mission in the meanwhile.
*/

//this is the administration page for the missions

	include ("../config.php");
	$pagetitle = "Missions";
	$curent_page = "missions";
	include ("adminmenu.php");


	if (isset($_POST['SubmitDelete']))
	{
		$missions = $_POST["missions"];
		$nummissions = count($missions);
		if ($nummissions == 0)
		{
			echo"<span style='color:red'>You need to select the missions you want to delete!</span><br />";
		}
		else
		{
			$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
			mysql_select_db($mysql_database);
			rsort($missions);
			foreach ($missions as $id)
			{
				$sql = "SELECT order_id FROM `report` WHERE `id`=$id";
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				else if (mysql_num_rows($result) != 0)
				{
					$row = mysql_fetch_object($result);
					$orderid = $row->order_id;
					$sql = "DELETE FROM `report` WHERE `id`=$id";
					//echo "query1:".$sql."<br />";
					$result = mysql_query($sql);
					if (mysql_errno()!=0 || !$result)
						echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
					else
					{
						echo"<span style='color:blue'>Mission deleted!</span><br />";
						$sql = "UPDATE `report` SET order_id = order_id - 1 WHERE order_id > ".$orderid;
						$result = mysql_query($sql);
						if (mysql_errno()!=0 || !$result)
							echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
					}
				}
			}
			mysql_close($connection);
		}
	}
	if (isset($_POST['SubmitNew']))
	{
		if (!$_POST["missionname"] || $_POST["missionname"] == "")
		{
			echo"<span style='color:red'>No name for the new mission specified</span><br />";
		}
		else
		{
			$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
			mysql_select_db($mysql_database);
			$addNewHere = 0;
			$addIt = true;
			if (!$_POST["mission_orderid"] || $_POST["mission_orderid"]=="")
			{
				//add new mission to the end of the list
				$sql = "SELECT `order_id` FROM `report` ORDER BY `order_id` DESC";
				$result = mysql_query($sql);
				if (mysql_errno()!=0 || !$result)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				else
				{
					$row = mysql_fetch_object($result);
					$addNewHere = intval($row->order_id) + 1;
				}
			}
			else
			{
				if (!is_numeric($_POST["mission_orderid"]))
				{
					echo"<span style='color:red'>The position id is not a number!</span><br />";
					$addIt = false;
				}
				else
				{
					$addNewHere = intval($_POST["mission_orderid"]);
					$sql = "SELECT order_id FROM `report` WHERE `order_id`=$addNewHere";
					$result = mysql_query($sql);
					if (mysql_errno()!=0 || !$result)
						echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
					else
					{
						if (mysql_fetch_object($result))
						{
							//inc all from this number on and add it to the wanted number
							$sql = "UPDATE `report` SET order_id = order_id + 1 WHERE order_id >= ".$addNewHere;
							$result = mysql_query($sql);
							if (mysql_errno()!=0 || !$result)
								echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
						}
					}
				}
			}
			if ($addIt)
			{
				//$missionname = ereg_replace("[)( .-]","",$_POST["missionname"]);
				$missionname = mysql_real_escape_string($_POST["missionname"]);
				$missionid = "";
				$missiontype = 0;
				if ($_POST["mission_type"] && is_numeric($_POST["mission_type"]))
				{
					$missiontype = intval($_POST["mission_type"]);
				}
				if ($_POST["missionid"])
				{
					//$missionid = ereg_replace("[)( .-]","",$_POST["missionid"]);
					$missionid = mysql_real_escape_string($_POST["missionid"]);
				}
				$query = "INSERT INTO report (order_id, mission_id, mission_title, missiontype) VALUES ('$addNewHere', '$missionid', '$missionname', $missiontype)";
				$result = mysql_query($query);
				if (mysql_errno()!=0)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				else
					echo"<span style='color:blue'>Mission $missionname created!</span><br />";
			}
			mysql_close($connection);
		}
	}

	if (isset($_POST['SubmitSave']))
	{
		$editId = $_POST['id'];
		if ($editId == "" || !is_numeric($editId))
		{
			echo "<span style='color:red'>Wrong parameters to edit the mission.</span><br />";
		}
		else
		{
			$order_id = 0;
			$old_order_id = $_POST["old_order_id"];
			if (!$_POST["mission_orderid"] || $_POST["mission_orderid"]=="")
			{
				$order_id = $old_order_id;
			}
			else
			{
				if (!is_numeric($_POST["mission_orderid"]))
				{
					echo"<span style='color:red'>The position id is not a number!</span><br />";
				}
				else
				{
					$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
					mysql_select_db($mysql_database);

					$order_id = $_POST["mission_orderid"];
					$new_name = $_POST['missionname'];
					$new_name = mysql_real_escape_string($new_name);
					$missionid = "";
					$missiontype = 0;
					if ($_POST["mission_type"] && is_numeric($_POST["mission_type"]))
					{
						$missiontype = intval($_POST["mission_type"]);
					}
					if ($_POST["missionid"])
					{
						//$missionid = ereg_replace("[)( .-]","",$_POST["missionid"]);
						$missionid = mysql_real_escape_string($_POST["missionid"]);
					}
					if ($new_name == "")
					{
						echo "<span style='color:red'>MissionName can not be left empty.</span><br />";
					}
					else
					{
						//REORDER THE MISSIONS
						if ($order_id != $old_order_id)
						{
							$sql = "SELECT order_id FROM `report` WHERE `order_id`=$order_id";
							$result = mysql_query($sql);
							if (mysql_errno()!=0 || !$result)
								echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
							else
							{
								if (mysql_fetch_object($result))
								{
									$sql = "";
									if ($order_id > $old_order_id)
									{
										$sql = "UPDATE `report` SET order_id = order_id - 1 WHERE order_id > ".$old_order_id." AND order_id <= ".$order_id;
									}
									else
									{
										$sql = "UPDATE `report` SET order_id = order_id + 1 WHERE order_id >= ".$order_id." AND order_id < ".$old_order_id;
									}
									$result = mysql_query($sql);
									if (mysql_errno()!=0 || !$result)
										echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
								}
							}
						}

						//UPDATE THE EDITED ONE
						$query = "UPDATE `report` SET mission_title='".$new_name."', mission_id='".$missionid."', missiontype='".$missiontype."', order_id='".$order_id."' WHERE `id`=".$editId;
						$result = mysql_query($query);
						echo $query;
						if (mysql_errno()!=0)
							echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
						else
							echo"<span style='color:blue'>Mission saved!</span><br />";
					}
					mysql_close($connection);
				}
			}
		}
	}
?>

<?php
	$connection = mysql_connect($mysql_address, $mysql_username, $mysql_password);
	mysql_select_db($mysql_database);

	foreach($_POST as $k => $v)
	{
		if (substr($k, 0, 12) == "SubmitEditId")
		{
			preg_match('{(\d+)}', $k, $m);
			$editId = $m[1];
			if ($editId == "" || !is_numeric($editId))
			{
				echo "<span style='color:red'>Wrong parameters to edit the mission.</span><br />";
			}
			else
			{
				$query = "SELECT * FROM `report` WHERE `id` = ".$editId;
				$result=mysql_query($query);
				if (mysql_errno()!=0)
					echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
				if (mysql_num_rows($result) == 0)
				{
					echo "<span style='color:red'>Mission id does not exist.</span><br />";
				}
				else
				{
					$row = mysql_fetch_object($result);
?>
					<b>Edit a mission:</b><br />
					<form action="missions.php<?php echo $param_db_suffix; ?>" method="post">
					<table border="1" style="border-collapse:collapse">
						<tr>
						<td>MissionId (e.g. KEN_A01)<br />[optional]</td>
						<td>MissionName (e.g. Yu Jian)<br />[required]</td>
						<td>MissionType<br />[optional]</td>
						<td>Move to this position<br />[optional]</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td><input id='onloadFocusFirst' name="missionid" type="text" size="30" maxlength="40" value="<?php echo $row->mission_id; ?>" /></td>
						<td><input name="missionname" type="text" size="30" maxlength="40" value="<?php echo $row->mission_title; ?>" /></td>
						<td>
							<select name="mission_type" size="1">
								<option value="0">---</option>
								<?php
									$sql = "SELECT `id`,`name` FROM `missiontypes`";
									$result = mysql_query($sql);
									if (mysql_errno()!=0 || !$result)
										echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
									while($entry = mysql_fetch_object($result))
									{
										if ($row->missiontype == $entry->id)
											echo "<option value='".$entry->id."' selected='selected'>".$entry->name."</option>";
										else
											echo "<option value='".$entry->id."'>".$entry->name."</option>";
									}
								?>
							</select>
						</td>
						<td><input name="mission_orderid" type="text" size="8" maxlength="5" value="<?php echo $row->order_id; ?>" /><input type='hidden' name='old_order_id' value="<?php echo $row->order_id; ?>" /></td>
						<td>&nbsp;&nbsp;&nbsp;<input type="hidden" name="id" value="<?php echo $editId; ?>" /><input type="submit" name="SubmitSave" value="Save" /></td>
						</tr>
					</table>
					</form>
					<br />
<?php
				}
			}
			break;
		}
	}
?>

<br />
<form action="missions.php<?php echo $param_db_suffix; ?>" method="post">
<b>Add mission:</b><br />
<table border="1" style="border-collapse:collapse">
<tr>
<td>MissionId (e.g. KEN_A01)<br />[optional]</td>
<td>MissionName (e.g. Yu Jian)<br />[required]</td>
<td>MissionType<br />[optional]</td>
<td>Add at this position<br />[optional]*</td>
<td>&nbsp;</td>
</tr>
<tr>
<td><input id='onloadFocus' name="missionid" type="text" size="30" maxlength="40" /></td>
<td><input name="missionname" type="text" size="30" maxlength="40" /></td>
<td>
	<select name="mission_type" size="1">
		<option value="0">---</option>
		<?
			$sql = "SELECT `id`,`name` FROM `missiontypes`";
			$result = mysql_query($sql);
			if (mysql_errno()!=0 || !$result)
				echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
			while($row = mysql_fetch_object($result))
			{
				echo "<option value='".$row->id."'>".$row->name."</option>";
			}
		?>
    </select>
</td>
<td><input name="mission_orderid" type="text" size="8" maxlength="5" /></td>
<td>&nbsp;&nbsp;&nbsp;<input type="submit" name="SubmitNew" value="Add" /></td>
</tr>
</table>
* if not specified, then the new mission will be added to the end of the list
</form>
<br /><br />
<form action="missions.php<?php echo $param_db_suffix; ?>" method="post">
<b>Delete missions:</b><br />
<table border="1" style="border-collapse:collapse" rules="rows" frame="void" >
<tr>
<td colspan="4"><input type="submit" name="SubmitDelete" value="Delete" /></td>
</tr>
<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;&nbsp;<b>Mission ID</b></td><td>&nbsp;&nbsp;<b>Mission Title</b></td><td>&nbsp;&nbsp;<b>Mission Type</b></td></tr>
<tr>
<td colspan="4"><input type='checkbox' name='checkall' onclick="toggleAllCheckboxes(this, 'missions[]');" /> All</td>
</tr>
<?
	$sql = "SELECT `report`.`id`,`report`.`order_id`,`report`.`mission_id`,`report`.`mission_title`,`missiontypes`.`name` FROM `report` LEFT JOIN `missiontypes` ON `report`.`missiontype` = `missiontypes`.`id` ORDER BY `order_id`";
	$result = mysql_query($sql);
	if (mysql_errno()!=0 || !$result)
		echo "<span style='color:red'>ERROR: " . mysql_errno() . ":" . mysql_error(). " result:" . $result . "</span><br />";
	while($row = mysql_fetch_object($result))
	{
		echo "<tr>
				<td><input type='checkbox' name='missions[]' value='".$row->id."' />&nbsp;&nbsp;".$row->order_id."</td>
				<td><input type='image' src='../images/edit.gif' name='SubmitEditId*".$row->id."*' alt='Edit mission' /></td>
				<td>&nbsp;&nbsp;".$row->mission_id."</td>
				<td>&nbsp;&nbsp;".$row->mission_title."</td>
				<td>&nbsp;&nbsp;".$row->name."</td>
			</tr>";
	}
?>
<tr>
<td colspan="4"><input type='checkbox' name='checkall' onclick="toggleAllCheckboxes(this, 'missions[]');" /> All</td>
</tr>
<tr>
<td colspan="4"><input type="submit" name="SubmitDelete" value="Delete" /></td>
</tr>
</table>
</form>
<br /><br /><br />

<?
	mysql_close($connection);
?>
</body>
</html>
