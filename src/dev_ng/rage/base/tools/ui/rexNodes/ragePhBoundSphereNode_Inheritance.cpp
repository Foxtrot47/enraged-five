#include "ragePhBoundSphereNode.h"

using namespace rage;

MObject ragePhBoundSphereNode::shapeType;
MObject ragePhBoundSphereNode::radius1;
MObject ragePhBoundSphereNode::radius2;
MObject ragePhBoundSphereNode::height;
MObject ragePhBoundSphereNode::startAngle;
MObject ragePhBoundSphereNode::sweepAngle;
MObject ragePhBoundSphereNode::slices;
MObject ragePhBoundSphereNode::loops;
MObject ragePhBoundSphereNode::stacks;
MTypeId ragePhBoundSphereNode::id( 0x80112 );


ragePhBoundSphereNode::ragePhBoundSphereNode(void)
{
//	Displayf("ragePhBoundSphereNode::ragePhBoundSphereNode");
}

ragePhBoundSphereNode::~ragePhBoundSphereNode(void)
{
//	Displayf("ragePhBoundSphereNode::~ragePhBoundSphereNode");
}

void* ragePhBoundSphereNode::creator()
{
//	Displayf("ragePhBoundSphereNode::creator()");
	return new ragePhBoundSphereNode();
}


MStatus ragePhBoundSphereNode::initialize()
{ 
	//	Displayf("ragePhBoundSphereNode::initialize()");
	MStatus				stat;
	MFnNumericAttribute	numericAttr;
	MFnEnumAttribute	enumAttr;

	// QUADRIC type enumerated attribute
	//
	shapeType = enumAttr.create( "shapeType", "st", 0, &stat );
	MCHECKERROR( stat, "create shapeType attribute" );
	enumAttr.addField( "cylinder", 0 );
	enumAttr.addField( "disk", 1 );
	enumAttr.addField( "partialDisk", 2 );
	enumAttr.addField( "sphere", 3 );
	enumAttr.addField( "closed cylinder", 4 );
	enumAttr.setHidden( false );
	enumAttr.setKeyable( true );
	stat = addAttribute( shapeType );
	MCHECKERROR( stat, "Error adding shapeType attribute." );

	// QUADRIC ATTRIBUTES
	//
	MAKE_NUMERIC_ATTR( radius1, "r1", MFnNumericData::kDouble, 1.0, true );
	MAKE_NUMERIC_ATTR( radius2, "r2", MFnNumericData::kDouble, 1.0, true );
	MAKE_NUMERIC_ATTR( height, "ht", MFnNumericData::kDouble, 2.0, true );
	MAKE_NUMERIC_ATTR( startAngle, "sta", MFnNumericData::kDouble, 0.0, true );
	MAKE_NUMERIC_ATTR( sweepAngle, "swa", MFnNumericData::kDouble, 90.0, true );
	MAKE_NUMERIC_ATTR( slices, "sl", MFnNumericData::kShort, 8, true );
	MAKE_NUMERIC_ATTR( loops, "lp", MFnNumericData::kShort, 6, true );
	MAKE_NUMERIC_ATTR( stacks, "sk", MFnNumericData::kShort, 4, true );

	return stat;}


/* override */
void ragePhBoundSphereNode::postConstructor()
//
// Description
// 
//    When instances of this node are created internally, the MObject associated
//    with the instance is not created until after the constructor of this class
//    is called. This means that no member functions of MPxSurfaceShape can
//    be called in the constructor.
//    The postConstructor solves this problem. Maya will call this function
//    after the internal object has been created.
//    As a general rule do all of your initialization in the postConstructor.
//
{ 
//	Displayf("ragePhBoundSphereNode::postConstructor()");
	// Call the parent's post constructor
	REXNodeShape::postConstructor();

	// Setup the attributes to force it into a unit sphere
	MPlug obShapeTypePlug(thisMObject(), shapeType);
	obShapeTypePlug.setValue(3);
	obShapeTypePlug.setLocked(true);

	MPlug obRadius1Plug(thisMObject(), radius1);
	obRadius1Plug.setValue(1.0f);
	obRadius1Plug.setLocked(true);

	MPlug obRadius2Plug(thisMObject(), radius2);
	obRadius2Plug.setValue(1.0f);
	obRadius2Plug.setLocked(true);

	MPlug obHeightPlug(thisMObject(), height);
	obHeightPlug.setValue(1.0f);
	obHeightPlug.setLocked(true);

	MPlug obStartAnglePlug(thisMObject(), startAngle);
	obStartAnglePlug.setValue(0.0f);
	obStartAnglePlug.setLocked(true);

	MPlug obSweepAnglePlug(thisMObject(), sweepAngle);
	obSweepAnglePlug.setValue(90.0f);
	obSweepAnglePlug.setLocked(true);

	MPlug obSlicesPlug(thisMObject(), slices);
	obSlicesPlug.setValue(12.0f);
	obSlicesPlug.setLocked(false);

	MPlug obLoopsPlug(thisMObject(), loops);
	obLoopsPlug.setValue(10.0f);
	obLoopsPlug.setLocked(false);

	MPlug obStacksPlug(thisMObject(), stacks);
	obStacksPlug.setValue(6.0f);
	obStacksPlug.setLocked(false);
}

