#ifndef REXNodeShapeUI_h
#define REXNodeShapeUI_h

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)
#include <maya/MIOStream.h>
#include <maya/MPxSurfaceShape.h>
#include <maya/MPxSurfaceShapeUI.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MPoint.h>
#include <maya/MPlug.h>
#include <maya/MDrawData.h>
#include <maya/MDrawRequest.h>
#include <maya/MSelectionMask.h>
#include <maya/MSelectionList.h>
#include <maya/MDagPath.h>
#include <maya/MMaterial.h>
#include <GL/glu.h>
#pragma warning(pop)

/////////////////////////////////////////////////////////////////////

#define MCHECKERROR(STAT,MSG)       \
	if ( MS::kSuccess != STAT ) {   \
	cerr << MSG << endl;        \
	return MS::kFailure;        \
	}

#define MAKE_NUMERIC_ATTR( NAME, SHORTNAME, TYPE, DEFAULT, KEYABLE ) \
	MStatus NAME##_stat;                                             \
	MFnNumericAttribute NAME##_fn;                                   \
	NAME = NAME##_fn.create( #NAME, SHORTNAME, TYPE, DEFAULT );      \
	MCHECKERROR(NAME##_stat, "numeric attr create error");		     \
	NAME##_fn.setHidden( !KEYABLE );								 \
	NAME##_fn.setKeyable( KEYABLE );								 \
	NAME##_fn.setInternal( true );									 \
	NAME##_stat = addAttribute( NAME );                              \
	MCHECKERROR(NAME##_stat, "addAttribute error");

#define LEAD_COLOR				18	// green
#define ACTIVE_COLOR			15	// white
#define ACTIVE_AFFECTED_COLOR	8	// purple
#define DORMANT_COLOR			4	// blue
#define HILITE_COLOR			17	// pale blue

/////////////////////////////////////////////////////////////////////
//
// UI class	- defines the UI part of a shape node
//
class REXNodeShapeUI : public MPxSurfaceShapeUI
{
public:
	REXNodeShapeUI();
	virtual ~REXNodeShapeUI(); 

	virtual void	getDrawRequests( const MDrawInfo & info,		bool objectAndActiveOnly,		MDrawRequestQueue & requests );
	virtual void	draw( const MDrawRequest & request,		M3dView & view ) const;	
	virtual bool	select( MSelectInfo &selectInfo,		MSelectionList &selectionList,		MPointArray &worldSpaceSelectPts ) const;

	void			getDrawRequestsWireframe( MDrawRequest&,		const MDrawInfo& );
	void			getDrawRequestsShaded(	  MDrawRequest&,		const MDrawInfo&,		MDrawRequestQueue&,		MDrawData& data );

	static  void *  creator();

private:
	enum {
		kDrawCylinder,
		kDrawDisk,
		kDrawPartialDisk,
		kDrawSphere,
		kDrawClosedCylinder
	};

	// Draw Tokens
	//
	enum {
		kDrawWireframe,
		kDrawWireframeOnShaded,
		kDrawSmoothShaded,
		kDrawFlatShaded,
		kLastToken
	};
};

#endif

