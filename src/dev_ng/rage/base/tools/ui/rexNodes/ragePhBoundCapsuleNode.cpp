//-
// ==========================================================================
// Copyright (C) 1995 - 2005 Alias Systems Corp. and/or its licensors.  All 
// rights reserved. 
// 
// The coded instructions, statements, computer programs, and/or related 
// material (collectively the "Data") in these files are provided by Alias 
// Systems Corp. ("Alias") and/or its licensors for the exclusive use of the 
// Customer (as defined in the Alias Software License Agreement that 
// accompanies this Alias software). Such Customer has the right to use, 
// modify, and incorporate the Data into other products and to distribute such 
// products for use by end-users.
//  
// THE DATA IS PROVIDED "AS IS".  ALIAS HEREBY DISCLAIMS ALL WARRANTIES 
// RELATING TO THE DATA, INCLUDING, WITHOUT LIMITATION, ANY AND ALL EXPRESS OR 
// IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND/OR FITNESS FOR A 
// PARTICULAR PURPOSE. IN NO EVENT SHALL ALIAS BE LIABLE FOR ANY DAMAGES 
// WHATSOEVER, WHETHER DIRECT, INDIRECT, SPECIAL, OR PUNITIVE, WHETHER IN AN 
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, OR IN EQUITY, 
// ARISING OUT OF ACCESS TO, USE OF, OR RELIANCE UPON THE DATA.
// ==========================================================================
//+

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)
#include <maya/MMatrix.h>
#include <maya/MFnDagNode.h>
#include <maya/MTransformationMatrix.h>
#pragma warning(pop)

#include "ragePhBoundCapsuleNode.h"

///////////////////////////////////////////////////////////////////////////////
//
// ragePhBoundCapsuleNode.cpp
//
// Description:
//    Registers a new type of shape with maya called "ragePhBoundCapsuleNode".
//    This shape will display spheres, Capsules, disks, and partial disks
//    using the OpenGL gluQuadric functions.
//
//    There are no output attributes for this shape.
//    The following input attributes define the type of shape to draw.
//
//       shapeType  : 0=Capsule, 1=disk, 2=partialDisk, 3=sphere
//       radius1	: Capsule base radius, disk inner radius, sphere radius
//       radius2	: Capsule top radius, disk outer radius
//       height		: Capsule height
//       startAngle	: partial disk start angle
//       sweepAngle	: partial disk sweep angle
//       slices		: Capsule, disk, sphere slices
//       loops		: disk loops
//       stacks		: Capsule, sphere stacks
//
////////////////////////////////////////////////////////////////////////////////

using namespace rage;

/////////////////////////////////////////////////////////////////////
// SHAPE NODE IMPLEMENTATION
/////////////////////////////////////////////////////////////////////

MObject ragePhBoundCapsuleNode::slices;
MObject ragePhBoundCapsuleNode::loops;
MObject ragePhBoundCapsuleNode::stacks;
MTypeId ragePhBoundCapsuleNode::id( 0x7011 );

ragePhBoundCapsuleNode::ragePhBoundCapsuleNode()
{
//	Displayf("ragePhBoundCapsuleNode::ragePhBoundCapsuleNode");
	fGeometry = new quadricGeom;
	fGeometry->radius1		= 1.0;
	fGeometry->radius2		= 1.0;
	fGeometry->height		= 1.0;
	fGeometry->startAngle	= 0.0;
	fGeometry->sweepAngle	= 90.0;
	fGeometry->slices		= 12;
	fGeometry->loops		= 8;
	fGeometry->stacks		= 2;
	fGeometry->shapeType	= quadricGeom::kDrawCapsule;
}

ragePhBoundCapsuleNode::~ragePhBoundCapsuleNode()
{
//	Displayf("ragePhBoundCapsuleNode::~ragePhBoundCapsuleNode");
	delete fGeometry;
}

/* override */
void ragePhBoundCapsuleNode::postConstructor()
//
// Description
// 
//    When instances of this node are created internally, the MObject associated
//    with the instance is not created until after the constructor of this class
//    is called. This means that no member functions of MPxSurfaceShape can
//    be called in the constructor.
//    The postConstructor solves this problem. Maya will call this function
//    after the internal object has been created.
//    As a general rule do all of your initialization in the postConstructor.
//
{ 
//	Displayf("ragePhBoundCapsuleNode::postConstructor");
	// This call allows the shape to have shading groups assigned
	//
	setRenderable( true );
}

/* override */
MStatus ragePhBoundCapsuleNode::compute( const MPlug& /*plug*/, MDataBlock& /*datablock*/ )
//
// Since there are no output attributes this is not necessary but
// if we wanted to compute an output mesh for rendering it would
// be done here base on the inputs.
//
{ 
//	Displayf("ragePhBoundCapsuleNode::compute");
	return MS::kUnknownParameter;
}

/* override */
bool ragePhBoundCapsuleNode::getInternalValue( const MPlug& plug,
									MDataHandle& datahandle )
									//
									// Handle internal attributes.
									// In order to impose limits on our attribute values we
									// mark them internal and use the values in fGeometry intead.
									//
{
//	const char* pcPlugName = plug.name().asChar();
//	Displayf("ragePhBoundCapsuleNode::getInternalValue(%s)", pcPlugName);
	bool isOk = true;

	if ( plug == slices ) {
		datahandle.set( fGeometry->slices );
		isOk = true;
	}
	else if ( plug == loops ) {
		datahandle.set( fGeometry->loops );
		isOk = true;
	}
	else if ( plug == stacks ) {
		datahandle.set( fGeometry->stacks );
		isOk = true;
	}
	else {
		isOk = MPxSurfaceShape::getInternalValue( plug, datahandle );
	}

	return isOk;
}
/* override */
bool ragePhBoundCapsuleNode::setInternalValue( const MPlug& plug,
									const MDataHandle& datahandle )
									//
									// Handle internal attributes.
									// In order to impose limits on our attribute values we
									// mark them internal and use the values in fGeometry intead.
									//
{
//	const char* pcPlugName = plug.name().asChar();
//	Displayf("ragePhBoundCapsuleNode::setInternalValue(%s)", pcPlugName);
	bool isOk = true;

	// In the case of a disk or partial disk the inner radius must
	// never exceed the outer radius and the minimum radius is 0
	//
	if ( plug == slices ) {
		short val = datahandle.asShort();
		if ( val < 3 ) {
			val = 3;
		}
		fGeometry->slices = val;
	}
	else if ( plug == loops ) {
		short val = datahandle.asShort();
		if ( val < 3 ) {
			val = 3;
		}
		fGeometry->loops = val;
	}
	else if ( plug == stacks ) {
		short val = datahandle.asShort();
		if ( val < 2 ) {
			val = 2;
		}
		fGeometry->stacks = val;
	}
	else {
		isOk = MPxSurfaceShape::setInternalValue( plug, datahandle );
	}

	return isOk;
}

/* override */
bool ragePhBoundCapsuleNode::isBounded() const { return true; }

/* override */
MBoundingBox ragePhBoundCapsuleNode::boundingBox() const
//
// Returns the bounding box for the shape.
// In this case just use the radius and height attributes
// to determine the bounding box.
//
{
	MDagPath obMeAsADagPath;
	MFnDagNode(thisMObject()).getPath(obMeAsADagPath);
	// MFnDagNode obMeAsAFnDagNode(obMeAsADagPath);

	// Get my matrix
	MTransformationMatrix obTransformationMatrix = obMeAsADagPath.inclusiveMatrix();

	// Get my inverse matrix
	MTransformationMatrix obInverseTransformationMatrix = obMeAsADagPath.inclusiveMatrixInverse();

	// Ok, what I want to do is remove the scale from my matrix, but my matrix has 
	// already been applied by Maya before I reach my draw code.
	// So, I first need to apply my Inverse Matrix to remove the transformations already 
	// applied by Maya, then I need to apply my matrix, but with the scale removed.

	// First remove scale from my matrix
	double adIdentityScale[3] = {1.0, 1.0, 1.0};
	obTransformationMatrix.setScale(adIdentityScale, MSpace::kWorld);

	// Then apply my inverse matrix
	obTransformationMatrix = obTransformationMatrix.asMatrix() * obInverseTransformationMatrix.asMatrix();

	// Get my actual scale for drawing
	double adScale[3];
	obTransformationMatrix.getScale(adScale, MSpace::kWorld);
	//float fRadius = (float)adScale[0];

	MBoundingBox result;	
	// ragePhBoundSphereNode* nonConstThis = const_cast <ragePhBoundSphereNode*> (this);
	// quadricGeom* geom = nonConstThis->geometry();

	//double r = 0.5f * geom->radius1;
	//result.expand( MPoint(r,r,r) );	result.expand( MPoint(-r,-r,-r) );
	//r = 0.5f * geom->radius2;
	//result.expand( MPoint(r,r,r) );	result.expand( MPoint(-r,-r,-r) );
	//r = 0.5f * geom->height;
	//result.expand( MPoint(r,r,r) );	result.expand( MPoint(-r,-r,-r) );
	float fOneOverXScale = (1.0f / (float)adScale[0]);
	float fOneOverYScale = (1.0f / (float)adScale[1]);
	//float fHalfHeightOfCylinderPart = (0.5f * fOneOverYScale * (float)adScale[1]);
	//result.expand( MPoint(1.0f, (fOneOverXScale * (float)adScale[1]) + fHalfHeightOfCylinderPart,fOneOverXScale * (float)adScale[2]) );	
	//result.expand( MPoint(-1.0f, -(fOneOverXScale * (float)adScale[1]) - fHalfHeightOfCylinderPart,-fOneOverXScale * (float)adScale[2]) );

	float fHalfHeight = (float)adScale[1] * (fOneOverXScale + 0.5f * fOneOverYScale);
	result.expand( MPoint(1.0f, fHalfHeight,fOneOverXScale * (float)adScale[2]) );	
	result.expand( MPoint(-1.0f, -fHalfHeight,-fOneOverXScale * (float)adScale[2]) );
	return result;
}

void* ragePhBoundCapsuleNode::creator()
{
//	Displayf("ragePhBoundCapsuleNode::creator()");
	return new ragePhBoundCapsuleNode();
}

MStatus ragePhBoundCapsuleNode::initialize()
{ 
//	Displayf("ragePhBoundCapsuleNode::initialize()");
	MStatus				stat;
	MFnNumericAttribute	numericAttr;
	MFnEnumAttribute	enumAttr;

	// QUADRIC ATTRIBUTES
	MAKE_NUMERIC_ATTR( slices, "sl", MFnNumericData::kShort, 8, true );
	MAKE_NUMERIC_ATTR( loops, "lp", MFnNumericData::kShort, 6, true );
	MAKE_NUMERIC_ATTR( stacks, "sk", MFnNumericData::kShort, 4, true );

	return stat;
}

quadricGeom* ragePhBoundCapsuleNode::geometry()
//
// This function gets the values of all the attributes and
// assigns them to the fGeometry. Calling MPlug::getValue
// will ensure that the values are up-to-date.
//
{
//	Displayf("ragePhBoundCapsuleNode::geometry()");
	MObject this_object = thisMObject();
	MPlug plug( this_object, slices );
	plug.setAttribute( slices );		plug.getValue( fGeometry->slices );
	plug.setAttribute( loops );			plug.getValue( fGeometry->loops );
	plug.setAttribute( stacks );		plug.getValue( fGeometry->stacks );

	return fGeometry;
}


