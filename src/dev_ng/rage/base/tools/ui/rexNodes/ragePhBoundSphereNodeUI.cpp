//-
// ==========================================================================
// Copyright (C) 1995 - 2005 Alias Systems Corp. and/or its licensors.  All 
// rights reserved. 
// 
// The coded instructions, statements, computer programs, and/or related 
// material (collectively the "Data") in these files are provided by Alias 
// Systems Corp. ("Alias") and/or its licensors for the exclusive use of the 
// Customer (as defined in the Alias Software License Agreement that 
// accompanies this Alias software). Such Customer has the right to use, 
// modify, and incorporate the Data into other products and to distribute such 
// products for use by end-users.
//  
// THE DATA IS PROVIDED "AS IS".  ALIAS HEREBY DISCLAIMS ALL WARRANTIES 
// RELATING TO THE DATA, INCLUDING, WITHOUT LIMITATION, ANY AND ALL EXPRESS OR 
// IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND/OR FITNESS FOR A 
// PARTICULAR PURPOSE. IN NO EVENT SHALL ALIAS BE LIABLE FOR ANY DAMAGES 
// WHATSOEVER, WHETHER DIRECT, INDIRECT, SPECIAL, OR PUNITIVE, WHETHER IN AN 
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, OR IN EQUITY, 
// ARISING OUT OF ACCESS TO, USE OF, OR RELIANCE UPON THE DATA.
// ==========================================================================
//+

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)
#include <maya/MFnDagNode.h>
#include <maya/MMatrix.h>
#include <maya/MTransformationMatrix.h>
#include <GL/glu.h>
#pragma warning(pop)

#include "ragePhBoundSphereNode.h"
#include "ragePhBoundSphereNodeUI.h"

///////////////////////////////////////////////////////////////////////////////
//
// ragePhBoundSphereNode.cpp
//
// Description:
//    Registers a new type of shape with maya called "ragePhBoundSphereNode".
//    This shape will display spheres, Spheres, disks, and partial disks
//    using the OpenGL gluQuadric functions.
//
//    There are no output attributes for this shape.
//    The following input attributes define the type of shape to draw.
//
//       shapeType  : 0=Sphere, 1=disk, 2=partialDisk, 3=sphere
//       radius1	: Sphere base radius, disk inner radius, sphere radius
//       radius2	: Sphere top radius, disk outer radius
//       height		: Sphere height
//       startAngle	: partial disk start angle
//       sweepAngle	: partial disk sweep angle
//       slices		: Sphere, disk, sphere slices
//       loops		: disk loops
//       stacks		: Sphere, sphere stacks
//
////////////////////////////////////////////////////////////////////////////////

using namespace rage;

/////////////////////////////////////////////////////////////////////
// UI IMPLEMENTATION
/////////////////////////////////////////////////////////////////////

ragePhBoundSphereNodeUI::ragePhBoundSphereNodeUI() {}
ragePhBoundSphereNodeUI::~ragePhBoundSphereNodeUI() {}

void* ragePhBoundSphereNodeUI::creator()
{
//	Displayf("ragePhBoundSphereNodeUI::creator()");
	return new ragePhBoundSphereNodeUI();
}

/* override */
void ragePhBoundSphereNodeUI::getDrawRequests( const MDrawInfo & info,
									 bool /*objectAndActiveOnly*/,
									 MDrawRequestQueue & queue )
{
//	Displayf("ragePhBoundSphereNodeUI::getDrawRequests");
	// The draw data is used to pass geometry through the 
	// draw queue. The data should hold all the information
	// needed to draw the shape.
	//
	MDrawData data;
	MDrawRequest request = info.getPrototype( *this );
	ragePhBoundSphereNode* shapeNode = (ragePhBoundSphereNode*)surfaceShape();
	quadricGeom* geom = shapeNode->geometry();
	getDrawData( geom, data );
	request.setDrawData( data );

	// Are we displaying meshes?
	if ( ! info.objectDisplayStatus( M3dView::kDisplayMeshes ) )
		return;

	// Use display status to determine what color to draw the object
	//
	switch ( info.displayStyle() )
	{		 
	case M3dView::kWireFrame :
		getDrawRequestsWireframe( request, info );
		queue.add( request );
		break;

	case M3dView::kGouraudShaded :
		request.setToken( kDrawSmoothShaded );
		getDrawRequestsShaded( request, info, queue, data );
		queue.add( request );
		break;

	case M3dView::kFlatShaded :
		request.setToken( kDrawFlatShaded );
		getDrawRequestsShaded( request, info, queue, data );
		queue.add( request );
		break;
	default:	
		break;
	}
}

/* override */
void ragePhBoundSphereNodeUI::draw( const MDrawRequest & request, M3dView & view ) const
//
// From the given draw request, get the draw data and determine
// which quadric to draw and with what values.
//
{ 	
	view.beginGL(); 
	//	Displayf("ragePhBoundBoxNodeUI::draw");
	// Covert the scale into something I can deal with
	// Get me as a dag path
	ragePhBoundSphereNode* shapeNode = (ragePhBoundSphereNode*)surfaceShape();
	MDagPath obMeAsADagPath;
	MFnDagNode(shapeNode->thisMObject()).getPath(obMeAsADagPath);
//	MFnDagNode obMeAsAFnDagNode(obMeAsADagPath);
	// Displayf("%s", obMeAsADagPath.fullPathName().asChar());

	// Get my matrix
	MTransformationMatrix obTransformationMatrix = obMeAsADagPath.inclusiveMatrix();
	//Displayf("MTransformationMatrix:");
	//Displayf("%.3f %.3f %.3f %.3f", obTransformationMatrix.asMatrix()[0][0], obTransformationMatrix.asMatrix()[0][1], obTransformationMatrix.asMatrix()[0][2], obTransformationMatrix.asMatrix()[0][3]);
	//Displayf("%.3f %.3f %.3f %.3f", obTransformationMatrix.asMatrix()[1][0], obTransformationMatrix.asMatrix()[1][1], obTransformationMatrix.asMatrix()[1][2], obTransformationMatrix.asMatrix()[1][3]);
	//Displayf("%.3f %.3f %.3f %.3f", obTransformationMatrix.asMatrix()[2][0], obTransformationMatrix.asMatrix()[2][1], obTransformationMatrix.asMatrix()[2][2], obTransformationMatrix.asMatrix()[2][3]);
	//Displayf("%.3f %.3f %.3f %.3f", obTransformationMatrix.asMatrix()[3][0], obTransformationMatrix.asMatrix()[3][1], obTransformationMatrix.asMatrix()[3][2], obTransformationMatrix.asMatrix()[3][3]);

	//// Get the GL_MODELVIEW_MATRIX going in
	//double adGLModelMatrixGoingIn[16];
	//glGetDoublev(GL_MODELVIEW_MATRIX, adGLModelMatrixGoingIn);
	//Displayf("GL_MODELVIEW_MATRIX going in:");
	//Displayf("%.3f %.3f %.3f %.3f", adGLModelMatrixGoingIn[0], adGLModelMatrixGoingIn[1], adGLModelMatrixGoingIn[2], adGLModelMatrixGoingIn[3]);
	//Displayf("%.3f %.3f %.3f %.3f", adGLModelMatrixGoingIn[4], adGLModelMatrixGoingIn[5], adGLModelMatrixGoingIn[6], adGLModelMatrixGoingIn[7]);
	//Displayf("%.3f %.3f %.3f %.3f", adGLModelMatrixGoingIn[8], adGLModelMatrixGoingIn[9], adGLModelMatrixGoingIn[10], adGLModelMatrixGoingIn[11]);
	//Displayf("%.3f %.3f %.3f %.3f", adGLModelMatrixGoingIn[12], adGLModelMatrixGoingIn[13], adGLModelMatrixGoingIn[14], adGLModelMatrixGoingIn[15]);

	//glGetDoublev(GL_PROJECTION_MATRIX, adGLModelMatrixGoingIn);
	//Displayf("GL_PROJECTION_MATRIX going in:");
	//Displayf("%.3f %.3f %.3f %.3f", adGLModelMatrixGoingIn[0], adGLModelMatrixGoingIn[1], adGLModelMatrixGoingIn[2], adGLModelMatrixGoingIn[3]);
	//Displayf("%.3f %.3f %.3f %.3f", adGLModelMatrixGoingIn[4], adGLModelMatrixGoingIn[5], adGLModelMatrixGoingIn[6], adGLModelMatrixGoingIn[7]);
	//Displayf("%.3f %.3f %.3f %.3f", adGLModelMatrixGoingIn[8], adGLModelMatrixGoingIn[9], adGLModelMatrixGoingIn[10], adGLModelMatrixGoingIn[11]);
	//Displayf("%.3f %.3f %.3f %.3f", adGLModelMatrixGoingIn[12], adGLModelMatrixGoingIn[13], adGLModelMatrixGoingIn[14], adGLModelMatrixGoingIn[15]);

	// Get my inverse matrix
	MTransformationMatrix obInverseTransformationMatrix = obMeAsADagPath.inclusiveMatrixInverse();

	// Get my actual scale for drawing
	double adScale[3];
	obTransformationMatrix.getScale(adScale, MSpace::kWorld);
	float fRadius = (float)adScale[0];

	// Ok, what I want to do is remove the scale from my matrix, but my matrix has 
	// already been applied by Maya before I reach my draw code.
	// So, I first need to apply my Inverse Matrix to remove the transformations already 
	// applied by Maya, then I need to apply my matrix, but with the scale removed.

	// First remove scale from my matrix
	double adIdentityScale[3] = {1.0, 1.0, 1.0};
	obTransformationMatrix.setScale(adIdentityScale, MSpace::kWorld);

	// Then apply my inverse matrix
	obTransformationMatrix = obTransformationMatrix.asMatrix() * obInverseTransformationMatrix.asMatrix();

	// Now convert that matrix into something openGl understands
	double aadMatrix[4][4];
	double aadGLMatrix[16];
	obTransformationMatrix.asMatrix().get(aadMatrix);
	for(int i=0; i<4; i++)
	{
		for(int j=0; j<4; j++)
		{
			aadGLMatrix[(i*4) + j] = aadMatrix[i][j];
		}
	}

	//Displayf("GL_MODELVIEW_MATRIX setting to:");
	//Displayf("%.3f %.3f %.3f %.3f", aadGLMatrix[0], aadGLMatrix[1], aadGLMatrix[2], aadGLMatrix[3]);
	//Displayf("%.3f %.3f %.3f %.3f", aadGLMatrix[4], aadGLMatrix[5], aadGLMatrix[6], aadGLMatrix[7]);
	//Displayf("%.3f %.3f %.3f %.3f", aadGLMatrix[8], aadGLMatrix[9], aadGLMatrix[10], aadGLMatrix[11]);
	//Displayf("%.3f %.3f %.3f %.3f", aadGLMatrix[12], aadGLMatrix[13], aadGLMatrix[14], aadGLMatrix[15]);


	// Draw it
	glPushMatrix();
	glMultMatrixd(aadGLMatrix);
	MDrawData data = request.drawData();
	quadricGeom * geom = (quadricGeom*)data.geometry();
	geom->radius1 = fRadius;
	geom->radius2 = fRadius;
	geom->draw(request, view);
	glPopMatrix();

	// Draw bounding box
	glPushMatrix();
//	MPlug obDrawBoundingBoxPlug( thisMObject(), m_obDrawBoundingBoxAttribute );
	bool bDrawBoundingBox = true;
//	obDrawBoundingBoxPlug.getValue(bDrawBoundingBox);
	if(bDrawBoundingBox)// || (DisplayStatus == M3dView::kActive) || (DisplayStatus == M3dView::kLead))
	{
		glColor3f(1.0f, 0.0f, 0.0f);
		MBoundingBox    obBoundingBox = shapeNode->boundingBox();
		float fMinX = (float)obBoundingBox.min().x;
		float fMinY = (float)obBoundingBox.min().y;
		float fMinZ = (float)obBoundingBox.min().z;
		float fMaxX = (float)obBoundingBox.max().x;
		float fMaxY = (float)obBoundingBox.max().y;
		float fMaxZ = (float)obBoundingBox.max().z;

		// Enable line stippling
		glEnable(GL_LINE_STIPPLE);
		// Set the stippling pattern
		glLineStipple(3, 0xAAAA);

		// Draw BB
		glBegin(GL_LINE_LOOP);
		glVertex3f(fMinX, fMinY, fMinZ);
		glVertex3f(fMinX, fMaxY, fMinZ);
		glVertex3f(fMinX, fMaxY, fMaxZ);
		glVertex3f(fMinX, fMinY, fMaxZ);
		glEnd();

		glBegin(GL_LINE_LOOP);
		glVertex3f(fMaxX, fMinY, fMinZ);
		glVertex3f(fMaxX, fMaxY, fMinZ);
		glVertex3f(fMaxX, fMaxY, fMaxZ);
		glVertex3f(fMaxX, fMinY, fMaxZ);
		glEnd();

		glBegin(GL_LINES);
		glVertex3f(fMinX, fMinY, fMinZ);
		glVertex3f(fMaxX, fMinY, fMinZ);

		glVertex3f(fMinX, fMaxY, fMinZ);
		glVertex3f(fMaxX, fMaxY, fMinZ);

		glVertex3f(fMinX, fMinY, fMaxZ);
		glVertex3f(fMaxX, fMinY, fMaxZ);

		glVertex3f(fMinX, fMaxY, fMaxZ);
		glVertex3f(fMaxX, fMaxY, fMaxZ);
		glEnd();

		// Disable line stippling
		glDisable(GL_LINE_STIPPLE);
	}

	glPopMatrix();
	view.endGL(); 
}


/* override */
bool ragePhBoundSphereNodeUI::select( MSelectInfo &selectInfo,
							MSelectionList &selectionList,
							MPointArray &worldSpaceSelectPts ) const
							//
							// Select function. Gets called when the bbox for the object is selected.
							// This function just selects the object without doing any intersection tests.
							//
{
//	Displayf("ragePhBoundSphereNodeUI::select");
	MSelectionMask priorityMask( MSelectionMask::kSelectObjectsMask );
	MSelectionList item;
	item.add( selectInfo.selectPath() );
	MPoint xformedPt;
	selectInfo.addSelection( item, xformedPt, selectionList,
		worldSpaceSelectPts, priorityMask, false );
	return true;
}

void ragePhBoundSphereNodeUI::getDrawRequestsWireframe( MDrawRequest& request,
											  const MDrawInfo& info )
{
//	Displayf("ragePhBoundSphereNodeUI::getDrawRequestsWireframe");
	request.setToken( kDrawWireframe );

	M3dView::DisplayStatus displayStatus = info.displayStatus();
	M3dView::ColorTable activeColorTable = M3dView::kActiveColors;
	M3dView::ColorTable dormantColorTable = M3dView::kDormantColors;
	switch ( displayStatus )
	{
	case M3dView::kLead :
		request.setColor( LEAD_COLOR, activeColorTable );
		break;
	case M3dView::kActive :
		request.setColor( ACTIVE_COLOR, activeColorTable );
		break;
	case M3dView::kActiveAffected :
		request.setColor( ACTIVE_AFFECTED_COLOR, activeColorTable );
		break;
	case M3dView::kDormant :
		request.setColor( DORMANT_COLOR, dormantColorTable );
		break;
	case M3dView::kHilite :
		request.setColor( HILITE_COLOR, activeColorTable );
		break;
	default:	
		break;
	}
}

void ragePhBoundSphereNodeUI::getDrawRequestsShaded( MDrawRequest& request,
										   const MDrawInfo& info,
										   MDrawRequestQueue& queue,
										   MDrawData& data )
{
//	Displayf("ragePhBoundSphereNodeUI::getDrawRequestsShaded");
	// Need to get the material info
	//
	MDagPath path = info.multiPath();	// path to your dag object 
	M3dView view = info.view();; 		// view to draw to
	MMaterial material = MPxSurfaceShapeUI::material( path );
	M3dView::DisplayStatus displayStatus = info.displayStatus();

	// Evaluate the material and if necessary, the texture.
	//
	if ( ! material.evaluateMaterial( view, path ) ) {
		cerr << "Couldnt evaluate\n";
	}

	bool drawTexture = true;
	if ( drawTexture && material.materialIsTextured() ) {
		material.evaluateTexture( data );
	}

	request.setMaterial( material );

	bool materialTransparent = false;
	material.getHasTransparency( materialTransparent );
	if ( materialTransparent ) {
		request.setIsTransparent( true );
	}

	// create a draw request for wireframe on shaded if
	// necessary.
	//
	if ( (displayStatus == M3dView::kActive) ||
		(displayStatus == M3dView::kLead) ||
		(displayStatus == M3dView::kHilite) )
	{
		MDrawRequest wireRequest = info.getPrototype( *this );
		wireRequest.setDrawData( data );
		getDrawRequestsWireframe( wireRequest, info );
		wireRequest.setToken( kDrawWireframeOnShaded );
		wireRequest.setDisplayStyle( M3dView::kWireFrame );
		queue.add( wireRequest );
	}
}

