#ifndef ragePhBoundBoxNode_h
#define ragePhBoundBoxNode_h

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)
#include <maya/MIOStream.h>
#include <maya/MPxSurfaceShape.h>
#include <maya/MPxSurfaceShapeUI.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MPoint.h>
#include <maya/MPlug.h>
#include <maya/MDrawData.h>
#include <maya/MDrawRequest.h>
#include <maya/MSelectionMask.h>
#include <maya/MSelectionList.h>
#include <maya/MDagPath.h>
#include <maya/MMaterial.h>
#include <GL/glu.h>
#pragma warning(pop)

#include "quadricGeom.h"

/////////////////////////////////////////////////////////////////////

#define MCHECKERROR(STAT,MSG)       \
	if ( MS::kSuccess != STAT ) {   \
	cerr << MSG << endl;        \
	return MS::kFailure;        \
	}

#define MAKE_NUMERIC_ATTR( NAME, SHORTNAME, TYPE, DEFAULT, KEYABLE ) \
	MStatus NAME##_stat;                                             \
	MFnNumericAttribute NAME##_fn;                                   \
	NAME = NAME##_fn.create( #NAME, SHORTNAME, TYPE, DEFAULT );      \
	MCHECKERROR(NAME##_stat, "numeric attr create error");		     \
	NAME##_fn.setHidden( !KEYABLE );								 \
	NAME##_fn.setKeyable( KEYABLE );								 \
	NAME##_fn.setInternal( true );									 \
	NAME##_stat = addAttribute( NAME );                              \
	MCHECKERROR(NAME##_stat, "addAttribute error");

#define LEAD_COLOR				18	// green
#define ACTIVE_COLOR			15	// white
#define ACTIVE_AFFECTED_COLOR	8	// purple
#define DORMANT_COLOR			4	// blue
#define HILITE_COLOR			17	// pale blue


/////////////////////////////////////////////////////////////////////
//
// Shape class - defines the non-UI part of a shape node
//
class ragePhBoundBoxNode : public MPxSurfaceShape
{
public:
	ragePhBoundBoxNode();
	virtual ~ragePhBoundBoxNode(); 

	virtual void			postConstructor();
	virtual MStatus			compute( const MPlug&, MDataBlock& );

	virtual bool            isBounded() const;
	virtual MBoundingBox    boundingBox() const; 

	static  void *		creator();
	static  MStatus		initialize();
	quadricGeom*		geometry();

public:
	// Shape type id
	//
	static	MTypeId		id;

private:
	quadricGeom*		fGeometry;
};

#endif

