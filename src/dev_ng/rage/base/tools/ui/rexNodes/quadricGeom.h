#ifndef quadricGeom_h
#define quadricGeom_h

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)
#include <maya/MIOStream.h>
#include <maya/MPxSurfaceShape.h>
#include <maya/MPxSurfaceShapeUI.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MPoint.h>
#include <maya/MPlug.h>
#include <maya/MDrawData.h>
#include <maya/MDrawRequest.h>
#include <maya/MSelectionMask.h>
#include <maya/MSelectionList.h>
#include <maya/MDagPath.h>
#include <maya/MMaterial.h>
#include <GL/glu.h>
#pragma warning(pop)


/////////////////////////////////////////////////////////////////////
//
// Geometry class
//
class quadricGeom
{
public:
	double radius1;
	double radius2;
	double height;
	double startAngle;
	double sweepAngle;
	short slices;
	short loops;
	short stacks;
	short shapeType;

	void draw( const MDrawRequest & request,		M3dView & view ) const;

	enum {
		kDrawCylinder,
		kDrawDisk,
		kDrawPartialDisk,
		kDrawSphere,
		kDrawClosedCylinder,
		kDrawCapsule
	};

	// Draw Tokens
	//
	enum {
		kDrawWireframe,
		kDrawWireframeOnShaded,
		kDrawSmoothShaded,
		kDrawFlatShaded,
		kLastToken
	};
};

#endif


