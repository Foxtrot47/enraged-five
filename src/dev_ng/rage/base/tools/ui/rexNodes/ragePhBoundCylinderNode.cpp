//-
// ==========================================================================
// Copyright (C) 1995 - 2005 Alias Systems Corp. and/or its licensors.  All 
// rights reserved. 
// 
// The coded instructions, statements, computer programs, and/or related 
// material (collectively the "Data") in these files are provided by Alias 
// Systems Corp. ("Alias") and/or its licensors for the exclusive use of the 
// Customer (as defined in the Alias Software License Agreement that 
// accompanies this Alias software). Such Customer has the right to use, 
// modify, and incorporate the Data into other products and to distribute such 
// products for use by end-users.
//  
// THE DATA IS PROVIDED "AS IS".  ALIAS HEREBY DISCLAIMS ALL WARRANTIES 
// RELATING TO THE DATA, INCLUDING, WITHOUT LIMITATION, ANY AND ALL EXPRESS OR 
// IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND/OR FITNESS FOR A 
// PARTICULAR PURPOSE. IN NO EVENT SHALL ALIAS BE LIABLE FOR ANY DAMAGES 
// WHATSOEVER, WHETHER DIRECT, INDIRECT, SPECIAL, OR PUNITIVE, WHETHER IN AN 
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, OR IN EQUITY, 
// ARISING OUT OF ACCESS TO, USE OF, OR RELIANCE UPON THE DATA.
// ==========================================================================
//+

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)
#include <maya/MMatrix.h>
#include <maya/MFnDagNode.h>
#include <maya/MTransformationMatrix.h>
#include <maya/MFnNumericAttribute.h>
#pragma warning(pop)

#include "ragePhBoundCylinderNode.h"

///////////////////////////////////////////////////////////////////////////////
//
// ragePhBoundCylinderNode.cpp
//
// Description:
//    Registers a new type of shape with maya called "ragePhBoundCylinderNode".
//    This shape will display spheres, cylinders, disks, and partial disks
//    using the OpenGL gluQuadric functions.
//
//    There are no output attributes for this shape.
//    The following input attributes define the type of shape to draw.
//
//       shapeType  : 0=cylinder, 1=disk, 2=partialDisk, 3=sphere
//       radius1	: cylinder base radius, disk inner radius, sphere radius
//       radius2	: cylinder top radius, disk outer radius
//       height		: cylinder height
//       startAngle	: partial disk start angle
//       sweepAngle	: partial disk sweep angle
//       slices		: cylinder, disk, sphere slices
//       loops		: disk loops
//       stacks		: cylinder, sphere stacks
//
////////////////////////////////////////////////////////////////////////////////

using namespace rage;

/////////////////////////////////////////////////////////////////////
// SHAPE NODE IMPLEMENTATION
/////////////////////////////////////////////////////////////////////

MObject ragePhBoundCylinderNode::shapeType;
MObject ragePhBoundCylinderNode::radius1;
MObject ragePhBoundCylinderNode::radius2;
MObject ragePhBoundCylinderNode::height;
MObject ragePhBoundCylinderNode::startAngle;
MObject ragePhBoundCylinderNode::sweepAngle;
MObject ragePhBoundCylinderNode::slices;
MObject ragePhBoundCylinderNode::loops;
MObject ragePhBoundCylinderNode::stacks;
MObject ragePhBoundCylinderNode::sideCurve;
MObject ragePhBoundCylinderNode::capCurve;

MTypeId ragePhBoundCylinderNode::id( 0x7023 );

ragePhBoundCylinderNode::ragePhBoundCylinderNode()
{
//	Displayf("ragePhBoundCylinderNode::ragePhBoundCylinderNode");
	fGeometry = new quadricGeom;
	fGeometry->radius1		= 1.0;
	fGeometry->radius2		= 1.0;
	fGeometry->height		= 2.0;
	fGeometry->startAngle	= 0.0;
	fGeometry->sweepAngle	= 90.0;
	fGeometry->slices		= 16;
	fGeometry->loops		= 2;
	fGeometry->stacks		= 2;
	fGeometry->shapeType	= 0;
}

ragePhBoundCylinderNode::~ragePhBoundCylinderNode()
{
//	Displayf("ragePhBoundCylinderNode::~ragePhBoundCylinderNode");
	delete fGeometry;
}

/* override */
void ragePhBoundCylinderNode::postConstructor()
//
// Description
// 
//    When instances of this node are created internally, the MObject associated
//    with the instance is not created until after the constructor of this class
//    is called. This means that no member functions of MPxSurfaceShape can
//    be called in the constructor.
//    The postConstructor solves this problem. Maya will call this function
//    after the internal object has been created.
//    As a general rule do all of your initialization in the postConstructor.
//
{ 
//	Displayf("ragePhBoundCylinderNode::postConstructor");
	// This call allows the shape to have shading groups assigned
	//
	setRenderable( true );
}

/* override */
MStatus ragePhBoundCylinderNode::compute( const MPlug& /*plug*/, MDataBlock& /*datablock*/ )
//
// Since there are no output attributes this is not necessary but
// if we wanted to compute an output mesh for rendering it would
// be done here base on the inputs.
//
{ 
//	Displayf("ragePhBoundCylinderNode::compute");
	return MS::kUnknownParameter;
}

/* override */
bool ragePhBoundCylinderNode::getInternalValue( const MPlug& plug,
									MDataHandle& datahandle )
									//
									// Handle internal attributes.
									// In order to impose limits on our attribute values we
									// mark them internal and use the values in fGeometry intead.
									//
{
//	const char* pcPlugName = plug.name().asChar();
//	Displayf("ragePhBoundCylinderNode::getInternalValue(%s)", pcPlugName);
	bool isOk = true;

	if ( plug == radius1 ) {
		datahandle.set( fGeometry->radius1 );
		isOk = true;
	}
	else if ( plug == radius2 ) {
		datahandle.set( fGeometry->radius2 );
		isOk = true;
	}
	else if ( plug == height ) {
		datahandle.set( fGeometry->height );
		isOk = true;
	}
	else if ( plug == startAngle ) {
		datahandle.set( fGeometry->startAngle );
		isOk = true;
	}
	else if ( plug == sweepAngle ) {
		datahandle.set( fGeometry->sweepAngle );
		isOk = true;
	}
	else if ( plug == slices ) {
		datahandle.set( fGeometry->slices );
		isOk = true;
	}
	else if ( plug == loops ) {
		datahandle.set( fGeometry->loops );
		isOk = true;
	}
	else if ( plug == stacks ) {
		datahandle.set( fGeometry->stacks );
		isOk = true;
	}
	else 
	{
		isOk = MPxSurfaceShape::getInternalValue( plug, datahandle );
	}

	return isOk;
}
/* override */
bool ragePhBoundCylinderNode::setInternalValue( const MPlug& plug,
									const MDataHandle& datahandle )
									//
									// Handle internal attributes.
									// In order to impose limits on our attribute values we
									// mark them internal and use the values in fGeometry intead.
									//
{
//	const char* pcPlugName = plug.name().asChar();
//	Displayf("ragePhBoundCylinderNode::setInternalValue(%s)", pcPlugName);
	bool isOk = true;

	// In the case of a disk or partial disk the inner radius must
	// never exceed the outer radius and the minimum radius is 0
	//
	if ( plug == radius1 ) {
		double innerRadius = datahandle.asDouble();
		double outerRadius = fGeometry->radius2;

		if ( innerRadius > outerRadius ) {
			outerRadius = innerRadius;
		}

		if ( innerRadius < 0 ) {
			innerRadius = 0;
		}

		fGeometry->radius1 = innerRadius;
		fGeometry->radius2 = outerRadius;
		isOk = true;
	}
	else if ( plug == radius2 ) {
		double outerRadius = datahandle.asDouble();
		double innerRadius = fGeometry->radius1;

		if ( outerRadius <= 0 ) {
			outerRadius = 0.1;
		}

		if ( innerRadius > outerRadius ) {
			innerRadius = outerRadius;
		}

		if ( innerRadius < 0 ) {
			innerRadius = 0;
		}

		fGeometry->radius1 = innerRadius;
		fGeometry->radius2 = outerRadius;
		isOk = true;
	}
	else if ( plug == height ) {
		double val = datahandle.asDouble();
		if ( val <= 0 ) {
			val = 0.1;
		}
		fGeometry->height = val;
	}
	else if ( plug == startAngle ) {
		double val = datahandle.asDouble();
		fGeometry->startAngle = val;
	}
	else if ( plug == sweepAngle ) {
		double val = datahandle.asDouble();
		fGeometry->sweepAngle = val;
	}
	else if ( plug == slices ) {
		short val = datahandle.asShort();
		if ( val < 3 ) {
			val = 3;
		}
		fGeometry->slices = val;
	}
	else if ( plug == loops ) {
		short val = datahandle.asShort();
		if ( val < 3 ) {
			val = 3;
		}
		fGeometry->loops = val;
	}
	else if ( plug == stacks ) {
		short val = datahandle.asShort();
		if ( val < 2 ) {
			val = 2;
		}
		fGeometry->stacks = val;
	}
	else 
	{
		isOk = MPxSurfaceShape::setInternalValue( plug, datahandle );
	}

	return isOk;
}

/* override */
bool ragePhBoundCylinderNode::isBounded() const { return true; }

/* override */
MBoundingBox ragePhBoundCylinderNode::boundingBox() const
//
// Returns the bounding box for the shape.
// In this case just use the radius and height attributes
// to determine the bounding box.
//
{
	// Get the shape's geometry
	ragePhBoundCylinderNode* nonConstThis = const_cast <ragePhBoundCylinderNode*> (this);
	quadricGeom* geom = nonConstThis->geometry();

	// Get the shape's transform
	MDagPath thisDagPath;
	MFnDagNode(thisMObject()).getPath(thisDagPath);
	MTransformationMatrix transformationMatrix = thisDagPath.inclusiveMatrix();

	// Get the scale of this shape
	double scaleArray[3];
	transformationMatrix.getScale(scaleArray, MSpace::kObject);

	// Calculate the farthest corner point
	MPoint cornerPoint(geom->radius2, 0.5f * geom->height, geom->radius2);
	cornerPoint.z *= (1.0f / (float)scaleArray[2]);		// Remove any Z scale value
	cornerPoint.z *= (float)scaleArray[0];				// Apply the X scale value instead
	
	// Calculate the bounding box
	MBoundingBox result;
	result.expand( cornerPoint );
	result.expand( cornerPoint * -1.0f );

	return result;
}

void* ragePhBoundCylinderNode::creator()
{
//	Displayf("ragePhBoundCylinderNode::creator()");
	return new ragePhBoundCylinderNode();
}

MStatus ragePhBoundCylinderNode::initialize()
{ 
//	Displayf("ragePhBoundCylinderNode::initialize()");
	MStatus				stat;
	MFnNumericAttribute	numericAttr;
	MFnEnumAttribute	enumAttr;

	// QUADRIC type enumerated attribute
	//
	shapeType = enumAttr.create( "shapeType", "st", 0, &stat );
	MCHECKERROR( stat, "create shapeType attribute" );
	enumAttr.addField( "cylinder", 0 );
	
	/*
	enumAttr.addField( "disk", 1 );
	enumAttr.addField( "partialDisk", 2 );
	enumAttr.addField( "sphere", 3 );
	enumAttr.addField( "closed cylinder", 4 );
	*/

	enumAttr.setHidden( false );
	enumAttr.setKeyable( true );
	stat = addAttribute( shapeType );
	MCHECKERROR( stat, "Error adding shapeType attribute." );

	// QUADRIC ATTRIBUTES
	//
	MAKE_NUMERIC_ATTR( radius1, "r1", MFnNumericData::kDouble, 1.0, true );
	MAKE_NUMERIC_ATTR( radius2, "r2", MFnNumericData::kDouble, 1.0, true );
	MAKE_NUMERIC_ATTR( height, "ht", MFnNumericData::kDouble, 2.0, true );
	MAKE_NUMERIC_ATTR( startAngle, "sta", MFnNumericData::kDouble, 0.0, false );
	MAKE_NUMERIC_ATTR( sweepAngle, "swa", MFnNumericData::kDouble, 90.0, false );
	MAKE_NUMERIC_ATTR( slices, "sl", MFnNumericData::kShort, 8, false );
	MAKE_NUMERIC_ATTR( loops, "lp", MFnNumericData::kShort, 6, false );
	MAKE_NUMERIC_ATTR( stacks, "sk", MFnNumericData::kShort, 4, false );

	// Curvature attributes
	MFnNumericAttribute fnAttrSideCurve;
	sideCurve = fnAttrSideCurve.create( "sideCurve", "sc", MFnNumericData::kDouble, 0.0);
	fnAttrSideCurve.setHidden(false);
	fnAttrSideCurve.setKeyable(true);
	fnAttrSideCurve.setInternal(false);
	fnAttrSideCurve.setMin(0.0);
	fnAttrSideCurve.setMax(1.0);
	addAttribute(sideCurve);

	MFnNumericAttribute fnAttrCapCurve;
	capCurve = fnAttrCapCurve.create( "capCurve", "cc", MFnNumericData::kDouble, 0.0);
	fnAttrCapCurve.setHidden(false);
	fnAttrCapCurve.setKeyable(true);
	fnAttrCapCurve.setInternal(false);
	fnAttrCapCurve.setMin(0.0);
	fnAttrCapCurve.setMax(1.0);
	addAttribute(capCurve);

	return stat;
}

quadricGeom* ragePhBoundCylinderNode::geometry()
//
// This function gets the values of all the attributes and
// assigns them to the fGeometry. Calling MPlug::getValue
// will ensure that the values are up-to-date.
//
{
//	Displayf("ragePhBoundCylinderNode::geometry()");
	MObject this_object = thisMObject();
	MPlug plug( this_object, radius1 );	plug.getValue( fGeometry->radius1 );
	plug.setAttribute( radius2 );		plug.getValue( fGeometry->radius2 );
	plug.setAttribute( height );		plug.getValue( fGeometry->height );
	plug.setAttribute( startAngle );	plug.getValue( fGeometry->startAngle );
	plug.setAttribute( sweepAngle );	plug.getValue( fGeometry->sweepAngle );
	plug.setAttribute( slices );		plug.getValue( fGeometry->slices );
	plug.setAttribute( loops );			plug.getValue( fGeometry->loops );
	plug.setAttribute( stacks );		plug.getValue( fGeometry->stacks );
	plug.setAttribute( shapeType );		plug.getValue( fGeometry->shapeType );

	return fGeometry;
}

