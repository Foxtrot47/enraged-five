#include "quadricGeom.h"

void quadricGeom::draw( const MDrawRequest & request, M3dView & view ) const
//
// From the given draw request, get the draw data and determine
// which quadric to draw and with what values.
//
{ 	
	//	Displayf("ragePhBoundBoxNodeUI::draw");
	MDrawData data = request.drawData();
	int token = request.token();
	bool drawTexture = false;

	if ( (token == kDrawSmoothShaded) || (token == kDrawFlatShaded) )
	{
#if		defined(SGI) || defined(MESA)
		glEnable( GL_POLYGON_OFFSET_EXT );
#else
		glEnable( GL_POLYGON_OFFSET_FILL );
#endif
		// Set up the material
		//
		MMaterial material = request.material();
		material.setMaterial( request.multiPath(), request.isTransparent() );

		// Enable texturing
		//
		drawTexture = material.materialIsTextured();
		if ( drawTexture ) glEnable(GL_TEXTURE_2D);

		// Apply the texture to the current view
		//
		if ( drawTexture ) {
			material.applyTexture( view, data );
		}
	}

	GLUquadricObj* qobj = gluNewQuadric();

	switch( token )
	{
	case kDrawWireframe :
	case kDrawWireframeOnShaded :
		gluQuadricDrawStyle( qobj, GLU_LINE );
		break;

	case kDrawSmoothShaded :
		gluQuadricNormals( qobj, GLU_SMOOTH );
		gluQuadricTexture( qobj, true );
		gluQuadricDrawStyle( qobj, GLU_FILL );
		break;

	case kDrawFlatShaded :
		gluQuadricNormals( qobj, GLU_FLAT );
		gluQuadricTexture( qobj, true );
		gluQuadricDrawStyle( qobj, GLU_FILL );
		break;
	}

	switch ( shapeType )
	{
	case kDrawCylinder :
		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -0.5f * (float)height);
		gluCylinder( qobj, radius1, radius2, height,	slices, stacks );
		glPopMatrix();
		break;
	case kDrawDisk :
		gluDisk( qobj, radius1, radius2, slices, loops );
		break;
	case kDrawPartialDisk :
		gluPartialDisk( qobj, radius1, radius2, slices,
			loops, startAngle, sweepAngle );
		break;
	case kDrawSphere : 
		gluSphere( qobj, radius1, slices, stacks );
		break;
	case kDrawClosedCylinder : 
		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -0.5f * (float)height);
		gluCylinder( qobj, radius1, radius2, height,	slices, stacks );
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.0f, 0.0f, 0.5f * (float)height);
		gluDisk( qobj, 0, radius2, slices,	loops );
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -0.5f * (float)height);
		gluDisk( qobj, 0, radius1, slices,	loops );
		glPopMatrix();
		break;
	case kDrawCapsule : 
		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -0.5f * (float)height);
		gluCylinder( qobj, radius1, radius2, height,	slices, stacks );
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.0f, 0.0f, 0.5f * (float)height);
		gluSphere( qobj, radius2, slices,	loops );
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -0.5f * (float)height);
		gluSphere( qobj, radius1, slices,	loops );
		glPopMatrix();
		break;
	default :
		gluSphere( qobj, radius1, slices, stacks );
		break;
	}

	// Turn off texture mode
	//
	if ( drawTexture ) glDisable(GL_TEXTURE_2D);
}

