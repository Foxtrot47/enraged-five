//-
// ==========================================================================
// Copyright (C) 1995 - 2005 Alias Systems Corp. and/or its licensors.  All 
// rights reserved. 
// 
// The coded instructions, statements, computer programs, and/or related 
// material (collectively the "Data") in these files are provided by Alias 
// Systems Corp. ("Alias") and/or its licensors for the exclusive use of the 
// Customer (as defined in the Alias Software License Agreement that 
// accompanies this Alias software). Such Customer has the right to use, 
// modify, and incorporate the Data into other products and to distribute such 
// products for use by end-users.
//  
// THE DATA IS PROVIDED "AS IS".  ALIAS HEREBY DISCLAIMS ALL WARRANTIES 
// RELATING TO THE DATA, INCLUDING, WITHOUT LIMITATION, ANY AND ALL EXPRESS OR 
// IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND/OR FITNESS FOR A 
// PARTICULAR PURPOSE. IN NO EVENT SHALL ALIAS BE LIABLE FOR ANY DAMAGES 
// WHATSOEVER, WHETHER DIRECT, INDIRECT, SPECIAL, OR PUNITIVE, WHETHER IN AN 
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, OR IN EQUITY, 
// ARISING OUT OF ACCESS TO, USE OF, OR RELIANCE UPON THE DATA.
// ==========================================================================
//+

#include "REXNodeShape.h"
#include "REXNodeShapeUI.h"

///////////////////////////////////////////////////////////////////////////////
//
// REXNodeShape.cpp
//
// Description:
//    Registers a new type of shape with maya called "REXNodeShape".
//    This shape will display spheres, cylinders, disks, and partial disks
//    using the OpenGL gluQuadric functions.
//
//    There are no output attributes for this shape.
//    The following input attributes define the type of shape to draw.
//
//       shapeType  : 0=cylinder, 1=disk, 2=partialDisk, 3=sphere
//       radius1	: cylinder base radius, disk inner radius, sphere radius
//       radius2	: cylinder top radius, disk outer radius
//       height		: cylinder height
//       startAngle	: partial disk start angle
//       sweepAngle	: partial disk sweep angle
//       slices		: cylinder, disk, sphere slices
//       loops		: disk loops
//       stacks		: cylinder, sphere stacks
//
////////////////////////////////////////////////////////////////////////////////

using namespace rage;

/////////////////////////////////////////////////////////////////////
// UI IMPLEMENTATION
/////////////////////////////////////////////////////////////////////

REXNodeShapeUI::REXNodeShapeUI() {}
REXNodeShapeUI::~REXNodeShapeUI() {}

void* REXNodeShapeUI::creator()
{
//	Displayf("REXNodeShapeUI::creator()");
	return new REXNodeShapeUI();
}

/* override */
void REXNodeShapeUI::getDrawRequests( const MDrawInfo & info,
									 bool /*objectAndActiveOnly*/,
									 MDrawRequestQueue & queue )
{
//	Displayf("REXNodeShapeUI::getDrawRequests");
	// The draw data is used to pass geometry through the 
	// draw queue. The data should hold all the information
	// needed to draw the shape.
	//
	MDrawData data;
	MDrawRequest request = info.getPrototype( *this );
	REXNodeShape* shapeNode = (REXNodeShape*)surfaceShape();
	quadricGeom* geom = shapeNode->geometry();
	getDrawData( geom, data );
	request.setDrawData( data );

	// Are we displaying meshes?
	if ( ! info.objectDisplayStatus( M3dView::kDisplayMeshes ) )
		return;

	// Use display status to determine what color to draw the object
	//
	switch ( info.displayStyle() )
	{		 
	case M3dView::kWireFrame :
		getDrawRequestsWireframe( request, info );
		queue.add( request );
		break;

	case M3dView::kGouraudShaded :
		request.setToken( kDrawSmoothShaded );
		getDrawRequestsShaded( request, info, queue, data );
		queue.add( request );
		break;

	case M3dView::kFlatShaded :
		request.setToken( kDrawFlatShaded );
		getDrawRequestsShaded( request, info, queue, data );
		queue.add( request );
		break;
	default:	
		break;
	}
}

/* override */
void REXNodeShapeUI::draw( const MDrawRequest & request, M3dView & view ) const
//
// From the given draw request, get the draw data and determine
// which quadric to draw and with what values.
//
{ 	
//	Displayf("REXNodeShapeUI::draw");
	MDrawData data = request.drawData();
	quadricGeom * geom = (quadricGeom*)data.geometry();
	int token = request.token();
	bool drawTexture = false;

	view.beginGL(); 

	if ( (token == kDrawSmoothShaded) || (token == kDrawFlatShaded) )
	{
#if		defined(SGI) || defined(MESA)
		glEnable( GL_POLYGON_OFFSET_EXT );
#else
		glEnable( GL_POLYGON_OFFSET_FILL );
#endif
		// Set up the material
		//
		MMaterial material = request.material();
		material.setMaterial( request.multiPath(), request.isTransparent() );

		// Enable texturing
		//
		drawTexture = material.materialIsTextured();
		if ( drawTexture ) glEnable(GL_TEXTURE_2D);

		// Apply the texture to the current view
		//
		if ( drawTexture ) {
			material.applyTexture( view, data );
		}
	}

	GLUquadricObj* qobj = gluNewQuadric();

	switch( token )
	{
	case kDrawWireframe :
	case kDrawWireframeOnShaded :
		gluQuadricDrawStyle( qobj, GLU_LINE );
		break;

	case kDrawSmoothShaded :
		gluQuadricNormals( qobj, GLU_SMOOTH );
		gluQuadricTexture( qobj, true );
		gluQuadricDrawStyle( qobj, GLU_FILL );
		break;

	case kDrawFlatShaded :
		gluQuadricNormals( qobj, GLU_FLAT );
		gluQuadricTexture( qobj, true );
		gluQuadricDrawStyle( qobj, GLU_FILL );
		break;
	}

	switch ( geom->shapeType )
	{
	case kDrawCylinder :
		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -0.5f * (float)geom->height);
		gluCylinder( qobj, geom->radius1, geom->radius2, geom->height,	geom->slices, geom->stacks );
		glPopMatrix();
		break;
	case kDrawDisk :
		gluDisk( qobj, geom->radius1, geom->radius2, geom->slices, geom->loops );
		break;
	case kDrawPartialDisk :
		gluPartialDisk( qobj, geom->radius1, geom->radius2, geom->slices,
			geom->loops, geom->startAngle, geom->sweepAngle );
		break;
	case kDrawSphere : 
		gluSphere( qobj, geom->radius1, geom->slices, geom->stacks );
		break;
	case kDrawClosedCylinder : 
		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -0.5f * (float)geom->height);
		gluCylinder( qobj, geom->radius1, geom->radius2, geom->height,	geom->slices, geom->stacks );
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.0f, 0.0f, 0.5f * (float)geom->height);
		gluDisk( qobj, 0, geom->radius2, geom->slices,	geom->loops );
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -0.5f * (float)geom->height);
		gluDisk( qobj, 0, geom->radius1, geom->slices,	geom->loops );
		glPopMatrix();
		break;
	default :
		gluSphere( qobj, geom->radius1, geom->slices, geom->stacks );
		break;
	}

	// Turn off texture mode
	//
	if ( drawTexture ) glDisable(GL_TEXTURE_2D);

	view.endGL(); 
}

/* override */
bool REXNodeShapeUI::select( MSelectInfo &selectInfo,
							MSelectionList &selectionList,
							MPointArray &worldSpaceSelectPts ) const
							//
							// Select function. Gets called when the bbox for the object is selected.
							// This function just selects the object without doing any intersection tests.
							//
{
//	Displayf("REXNodeShapeUI::select");
	MSelectionMask priorityMask( MSelectionMask::kSelectObjectsMask );
	MSelectionList item;
	item.add( selectInfo.selectPath() );
	MPoint xformedPt;
	selectInfo.addSelection( item, xformedPt, selectionList,
		worldSpaceSelectPts, priorityMask, false );
	return true;
}

void REXNodeShapeUI::getDrawRequestsWireframe( MDrawRequest& request,
											  const MDrawInfo& info )
{
//	Displayf("REXNodeShapeUI::getDrawRequestsWireframe");
	request.setToken( kDrawWireframe );

	M3dView::DisplayStatus displayStatus = info.displayStatus();
	M3dView::ColorTable activeColorTable = M3dView::kActiveColors;
	M3dView::ColorTable dormantColorTable = M3dView::kDormantColors;
	switch ( displayStatus )
	{
	case M3dView::kLead :
		request.setColor( LEAD_COLOR, activeColorTable );
		break;
	case M3dView::kActive :
		request.setColor( ACTIVE_COLOR, activeColorTable );
		break;
	case M3dView::kActiveAffected :
		request.setColor( ACTIVE_AFFECTED_COLOR, activeColorTable );
		break;
	case M3dView::kDormant :
		request.setColor( DORMANT_COLOR, dormantColorTable );
		break;
	case M3dView::kHilite :
		request.setColor( HILITE_COLOR, activeColorTable );
		break;
	default:	
		break;
	}
}

void REXNodeShapeUI::getDrawRequestsShaded( MDrawRequest& request,
										   const MDrawInfo& info,
										   MDrawRequestQueue& queue,
										   MDrawData& data )
{
//	Displayf("REXNodeShapeUI::getDrawRequestsShaded");
	// Need to get the material info
	//
	MDagPath path = info.multiPath();	// path to your dag object 
	M3dView view = info.view();; 		// view to draw to
	MMaterial material = MPxSurfaceShapeUI::material( path );
	M3dView::DisplayStatus displayStatus = info.displayStatus();

	// Evaluate the material and if necessary, the texture.
	//
	if ( ! material.evaluateMaterial( view, path ) ) {
		cerr << "Couldnt evaluate\n";
	}

	bool drawTexture = true;
	if ( drawTexture && material.materialIsTextured() ) {
		material.evaluateTexture( data );
	}

	request.setMaterial( material );

	bool materialTransparent = false;
	material.getHasTransparency( materialTransparent );
	if ( materialTransparent ) {
		request.setIsTransparent( true );
	}

	// create a draw request for wireframe on shaded if
	// necessary.
	//
	if ( (displayStatus == M3dView::kActive) ||
		(displayStatus == M3dView::kLead) ||
		(displayStatus == M3dView::kHilite) )
	{
		MDrawRequest wireRequest = info.getPrototype( *this );
		wireRequest.setDrawData( data );
		getDrawRequestsWireframe( wireRequest, info );
		wireRequest.setToken( kDrawWireframeOnShaded );
		wireRequest.setDisplayStyle( M3dView::kWireFrame );
		queue.add( wireRequest );
	}
}

