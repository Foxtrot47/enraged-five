//
// Copyright (C) 
// 
// File: pluginMain.cpp
//
// Author: Maya Plug-in Wizard 2.0
//

// specify additional libraries to link to
#pragma comment(lib, "Foundation.lib")
#pragma comment(lib, "OpenMaya.lib")
#pragma comment(lib, "OpenMayaUI.lib")
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "Glu32.lib")

#include "REXNodeShape.h"
#include "REXNodeShapeUI.h"
#include "ragePhBoundSphereNode.h"
#include "ragePhBoundSphereNodeUI.h"
#include "ragePhBoundCylinderNode.h"
#include "ragePhBoundCylinderNodeUI.h"
#include "ragePhBoundBoxNode.h"
#include "ragePhBoundBoxNodeUI.h"
#include "ragePhBoundCapsuleNode.h"
#include "ragePhBoundCapsuleNodeUI.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)
#include <maya/MPxTransformationMatrix.h>
#include <maya/MFnPlugin.h>
#pragma warning(pop)

using namespace rage;

MStatus initializePlugin( MObject obj )
{ 
	MFnPlugin plugin( obj, "Alias", "3.0", "Any");

	MStatus obStatus = MS::kSuccess;
	obStatus = plugin.registerShape( "REXNodeShape", REXNodeShape::id,		&REXNodeShape::creator,		&REXNodeShape::initialize,		&REXNodeShapeUI::creator  );
	if(obStatus != MS::kSuccess)
	{
		Displayf("An error occured registering REXNodeShape shape \"%s\"", obStatus.errorString().asChar());
		return obStatus;
	}
	obStatus = plugin.registerShape( "ragePhBoundSphereNode", ragePhBoundSphereNode::id,		&ragePhBoundSphereNode::creator,		&ragePhBoundSphereNode::initialize,		&ragePhBoundSphereNodeUI::creator  );
	if(obStatus != MS::kSuccess)
	{
		Displayf("An error occured registering ragePhBoundSphereNode shape \"%s\"", obStatus.errorString().asChar());
		return obStatus;
	}
	obStatus = plugin.registerShape( "ragePhBoundCylinderNode", ragePhBoundCylinderNode::id,		&ragePhBoundCylinderNode::creator,		&ragePhBoundCylinderNode::initialize,		&ragePhBoundCylinderNodeUI::creator  );
	if(obStatus != MS::kSuccess)
	{
		Displayf("An error occured registering ragePhBoundCylinderNode shape \"%s\"", obStatus.errorString().asChar());
		return obStatus;
	}
	obStatus = plugin.registerShape( "ragePhBoundBoxNode", ragePhBoundBoxNode::id,		&ragePhBoundBoxNode::creator,		&ragePhBoundBoxNode::initialize,		&ragePhBoundBoxNodeUI::creator  );
	if(obStatus != MS::kSuccess)
	{
		Displayf("An error occured registering ragePhBoundBoxNode shape \"%s\"", obStatus.errorString().asChar());
		return obStatus;
	}
	obStatus = plugin.registerShape( "ragePhBoundCapsuleNode", ragePhBoundCapsuleNode::id,		&ragePhBoundCapsuleNode::creator,		&ragePhBoundCapsuleNode::initialize,		&ragePhBoundCapsuleNodeUI::creator  );
	if(obStatus != MS::kSuccess)
	{
		Displayf("An error occured registering ragePhBoundCapsuleNode shape \"%s\"", obStatus.errorString().asChar());
		return obStatus;
	}
	return obStatus;
}

MStatus uninitializePlugin( MObject obj)
{
	MFnPlugin plugin( obj );
	MStatus obStatus;
	obStatus = plugin.deregisterNode( REXNodeShape::id );
	obStatus = plugin.deregisterNode( ragePhBoundSphereNode::id );
	obStatus = plugin.deregisterNode( ragePhBoundCylinderNode::id );
	obStatus = plugin.deregisterNode( ragePhBoundBoxNode::id );
	obStatus = plugin.deregisterNode( ragePhBoundCapsuleNode::id );
	return obStatus;
}

