//-
// ==========================================================================
// Copyright (C) 1995 - 2005 Alias Systems Corp. and/or its licensors.  All 
// rights reserved. 
// 
// The coded instructions, statements, computer programs, and/or related 
// material (collectively the "Data") in these files are provided by Alias 
// Systems Corp. ("Alias") and/or its licensors for the exclusive use of the 
// Customer (as defined in the Alias Software License Agreement that 
// accompanies this Alias software). Such Customer has the right to use, 
// modify, and incorporate the Data into other products and to distribute such 
// products for use by end-users.
//  
// THE DATA IS PROVIDED "AS IS".  ALIAS HEREBY DISCLAIMS ALL WARRANTIES 
// RELATING TO THE DATA, INCLUDING, WITHOUT LIMITATION, ANY AND ALL EXPRESS OR 
// IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND/OR FITNESS FOR A 
// PARTICULAR PURPOSE. IN NO EVENT SHALL ALIAS BE LIABLE FOR ANY DAMAGES 
// WHATSOEVER, WHETHER DIRECT, INDIRECT, SPECIAL, OR PUNITIVE, WHETHER IN AN 
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, OR IN EQUITY, 
// ARISING OUT OF ACCESS TO, USE OF, OR RELIANCE UPON THE DATA.
// ==========================================================================
//+

#include "ragePhBoundBoxNode.h"

///////////////////////////////////////////////////////////////////////////////
//
// ragePhBoundBoxNode.cpp
//
// Description:
//    Registers a new type of shape with maya called "ragePhBoundBoxNode".
//    This shape will display spheres, Boxs, disks, and partial disks
//    using the OpenGL gluQuadric functions.
//
//    There are no output attributes for this shape.
//    The following input attributes define the type of shape to draw.
//
//       shapeType  : 0=Box, 1=disk, 2=partialDisk, 3=sphere
//       radius1	: Box base radius, disk inner radius, sphere radius
//       radius2	: Box top radius, disk outer radius
//       height		: Box height
//       startAngle	: partial disk start angle
//       sweepAngle	: partial disk sweep angle
//       slices		: Box, disk, sphere slices
//       loops		: disk loops
//       stacks		: Box, sphere stacks
//
////////////////////////////////////////////////////////////////////////////////

using namespace rage;

/////////////////////////////////////////////////////////////////////
// SHAPE NODE IMPLEMENTATION
/////////////////////////////////////////////////////////////////////

MTypeId ragePhBoundBoxNode::id( 0x7010 );

ragePhBoundBoxNode::ragePhBoundBoxNode()
{
//	Displayf("ragePhBoundBoxNode::ragePhBoundBoxNode");
	fGeometry = new quadricGeom;
	fGeometry->radius1		= 0.707;
	fGeometry->radius2		= 0.707;
	fGeometry->height		= 1.0;
	fGeometry->startAngle	= 0.0;
	fGeometry->sweepAngle	= 90.0;
	fGeometry->slices		= 4;
	fGeometry->loops		= 1;
	fGeometry->stacks		= 1;
	fGeometry->shapeType	= quadricGeom::kDrawClosedCylinder;
}

ragePhBoundBoxNode::~ragePhBoundBoxNode()
{
//	Displayf("ragePhBoundBoxNode::~ragePhBoundBoxNode");
	delete fGeometry;
}

/* override */
void ragePhBoundBoxNode::postConstructor()
//
// Description
// 
//    When instances of this node are created internally, the MObject associated
//    with the instance is not created until after the constructor of this class
//    is called. This means that no member functions of MPxSurfaceShape can
//    be called in the constructor.
//    The postConstructor solves this problem. Maya will call this function
//    after the internal object has been created.
//    As a general rule do all of your initialization in the postConstructor.
//
{ 
//	Displayf("ragePhBoundBoxNode::postConstructor");
	// This call allows the shape to have shading groups assigned
	//
	setRenderable( true );
}

/* override */
MStatus ragePhBoundBoxNode::compute( const MPlug& /*plug*/, MDataBlock& /*datablock*/ )
//
// Since there are no output attributes this is not necessary but
// if we wanted to compute an output mesh for rendering it would
// be done here base on the inputs.
//
{ 
//	Displayf("ragePhBoundBoxNode::compute");
	return MS::kUnknownParameter;
}

/* override */
bool ragePhBoundBoxNode::isBounded() const { return true; }

/* override */
MBoundingBox ragePhBoundBoxNode::boundingBox() const
//
// Returns the bounding box for the shape.
// In this case just use the radius and height attributes
// to determine the bounding box.
//
{
	MBoundingBox result;	
	ragePhBoundBoxNode* nonConstThis = const_cast <ragePhBoundBoxNode*> (this);
	quadricGeom* geom = nonConstThis->geometry();

	double r = 0.5f * geom->radius1;
	result.expand( MPoint(r,r,r) );	result.expand( MPoint(-r,-r,-r) );
	r = 0.5f * geom->radius2;
	result.expand( MPoint(r,r,r) );	result.expand( MPoint(-r,-r,-r) );
	r = 0.5f * geom->height;
	result.expand( MPoint(r,r,r) );	result.expand( MPoint(-r,-r,-r) );

	return result;
}

void* ragePhBoundBoxNode::creator()
{
//	Displayf("ragePhBoundBoxNode::creator()");
	return new ragePhBoundBoxNode();
}

MStatus ragePhBoundBoxNode::initialize()
{ 
//	Displayf("ragePhBoundBoxNode::initialize()");
	return MS::kSuccess;
}

quadricGeom* ragePhBoundBoxNode::geometry()
//
// This function gets the values of all the attributes and
// assigns them to the fGeometry. Calling MPlug::getValue
// will ensure that the values are up-to-date.
//
{
	return fGeometry;
}


