#pragma once
#include "REXNodeShape.h"

class ragePhBoundSphereNode :	public REXNodeShape
{
public:
	ragePhBoundSphereNode(void);
	virtual ~ragePhBoundSphereNode(void);
	virtual void			postConstructor();

	static  void *		creator();
	static  MStatus		initialize();

public:
	// Attributes
	//
	static  MObject     shapeType;
	static	MObject		radius1;
	static	MObject		radius2;
	static	MObject		height;
	static	MObject		startAngle;
	static	MObject		sweepAngle;
	static	MObject		slices;
	static	MObject		loops;
	static	MObject		stacks;

public:
	// Shape type id
	//
	static	MTypeId		id;
};

