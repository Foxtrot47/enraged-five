//-
// ==========================================================================
// Copyright (C) 1995 - 2005 Alias Systems Corp. and/or its licensors.  All 
// rights reserved. 
// 
// The coded instructions, statements, computer programs, and/or related 
// material (collectively the "Data") in these files are provided by Alias 
// Systems Corp. ("Alias") and/or its licensors for the exclusive use of the 
// Customer (as defined in the Alias Software License Agreement that 
// accompanies this Alias software). Such Customer has the right to use, 
// modify, and incorporate the Data into other products and to distribute such 
// products for use by end-users.
//  
// THE DATA IS PROVIDED "AS IS".  ALIAS HEREBY DISCLAIMS ALL WARRANTIES 
// RELATING TO THE DATA, INCLUDING, WITHOUT LIMITATION, ANY AND ALL EXPRESS OR 
// IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND/OR FITNESS FOR A 
// PARTICULAR PURPOSE. IN NO EVENT SHALL ALIAS BE LIABLE FOR ANY DAMAGES 
// WHATSOEVER, WHETHER DIRECT, INDIRECT, SPECIAL, OR PUNITIVE, WHETHER IN AN 
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, OR IN EQUITY, 
// ARISING OUT OF ACCESS TO, USE OF, OR RELIANCE UPON THE DATA.
// ==========================================================================
//+

#include "ragePhBoundBoxNode.h"
#include "ragePhBoundBoxNodeUI.h"

///////////////////////////////////////////////////////////////////////////////
//
// ragePhBoundBoxNode.cpp
//
// Description:
//    Registers a new type of shape with maya called "ragePhBoundBoxNode".
//    This shape will display spheres, Boxs, disks, and partial disks
//    using the OpenGL gluQuadric functions.
//
//    There are no output attributes for this shape.
//    The following input attributes define the type of shape to draw.
//
//       shapeType  : 0=Box, 1=disk, 2=partialDisk, 3=sphere
//       radius1	: Box base radius, disk inner radius, sphere radius
//       radius2	: Box top radius, disk outer radius
//       height		: Box height
//       startAngle	: partial disk start angle
//       sweepAngle	: partial disk sweep angle
//       slices		: Box, disk, sphere slices
//       loops		: disk loops
//       stacks		: Box, sphere stacks
//
////////////////////////////////////////////////////////////////////////////////

using namespace rage;

/////////////////////////////////////////////////////////////////////
// UI IMPLEMENTATION
/////////////////////////////////////////////////////////////////////

ragePhBoundBoxNodeUI::ragePhBoundBoxNodeUI() {}
ragePhBoundBoxNodeUI::~ragePhBoundBoxNodeUI() {}

void* ragePhBoundBoxNodeUI::creator()
{
//	Displayf("ragePhBoundBoxNodeUI::creator()");
	return new ragePhBoundBoxNodeUI();
}

/* override */
void ragePhBoundBoxNodeUI::getDrawRequests( const MDrawInfo & info,
									 bool /*objectAndActiveOnly*/,
									 MDrawRequestQueue & queue )
{
//	Displayf("ragePhBoundBoxNodeUI::getDrawRequests");
	// The draw data is used to pass geometry through the 
	// draw queue. The data should hold all the information
	// needed to draw the shape.
	//
	MDrawData data;
	MDrawRequest request = info.getPrototype( *this );
	ragePhBoundBoxNode* shapeNode = (ragePhBoundBoxNode*)surfaceShape();
	quadricGeom* geom = shapeNode->geometry();
	getDrawData( geom, data );
	request.setDrawData( data );

	// Are we displaying meshes?
	if ( ! info.objectDisplayStatus( M3dView::kDisplayMeshes ) )
		return;

	// Use display status to determine what color to draw the object
	//
	switch ( info.displayStyle() )
	{		 
	case M3dView::kWireFrame :
		getDrawRequestsWireframe( request, info );
		queue.add( request );
		break;

	case M3dView::kGouraudShaded :
		request.setToken( quadricGeom::kDrawSmoothShaded );
		getDrawRequestsShaded( request, info, queue, data );
		queue.add( request );
		break;

	case M3dView::kFlatShaded :
		request.setToken( quadricGeom::kDrawFlatShaded );
		getDrawRequestsShaded( request, info, queue, data );
		queue.add( request );
		break;
	default:	
		break;
	}
}

/* override */
void ragePhBoundBoxNodeUI::draw( const MDrawRequest & request, M3dView & view ) const
//
// From the given draw request, get the draw data and determine
// which quadric to draw and with what values.
//
{ 	
//	Displayf("ragePhBoundBoxNodeUI::draw");
	view.beginGL(); 

	glPushMatrix();
	glRotatef(45.0f, 0.0f, 0.0f, 1.0f);
	MDrawData data = request.drawData();
	quadricGeom * geom = (quadricGeom*)data.geometry();
	geom->draw(request, view);
	glPopMatrix();


	// Draw bounding box
	glPushMatrix();
	//	MPlug obDrawBoundingBoxPlug( thisMObject(), m_obDrawBoundingBoxAttribute );
	bool bDrawBoundingBox = true;
	//	obDrawBoundingBoxPlug.getValue(bDrawBoundingBox);
	if(bDrawBoundingBox)// || (DisplayStatus == M3dView::kActive) || (DisplayStatus == M3dView::kLead))
	{
		glColor3f(1.0f, 0.0f, 0.0f);
		ragePhBoundBoxNode* shapeNode = (ragePhBoundBoxNode*)surfaceShape();
		MBoundingBox    obBoundingBox = shapeNode->boundingBox();
		float fMinX = (float)obBoundingBox.min().x;
		float fMinY = (float)obBoundingBox.min().y;
		float fMinZ = (float)obBoundingBox.min().z;
		float fMaxX = (float)obBoundingBox.max().x;
		float fMaxY = (float)obBoundingBox.max().y;
		float fMaxZ = (float)obBoundingBox.max().z;

		// Enable line stippling
		glEnable(GL_LINE_STIPPLE);
		// Set the stippling pattern
		glLineStipple(3, 0xAAAA);

		// Draw BB
		glBegin(GL_LINE_LOOP);
		glVertex3f(fMinX, fMinY, fMinZ);
		glVertex3f(fMinX, fMaxY, fMinZ);
		glVertex3f(fMinX, fMaxY, fMaxZ);
		glVertex3f(fMinX, fMinY, fMaxZ);
		glEnd();

		glBegin(GL_LINE_LOOP);
		glVertex3f(fMaxX, fMinY, fMinZ);
		glVertex3f(fMaxX, fMaxY, fMinZ);
		glVertex3f(fMaxX, fMaxY, fMaxZ);
		glVertex3f(fMaxX, fMinY, fMaxZ);
		glEnd();

		glBegin(GL_LINES);
		glVertex3f(fMinX, fMinY, fMinZ);
		glVertex3f(fMaxX, fMinY, fMinZ);

		glVertex3f(fMinX, fMaxY, fMinZ);
		glVertex3f(fMaxX, fMaxY, fMinZ);

		glVertex3f(fMinX, fMinY, fMaxZ);
		glVertex3f(fMaxX, fMinY, fMaxZ);

		glVertex3f(fMinX, fMaxY, fMaxZ);
		glVertex3f(fMaxX, fMaxY, fMaxZ);
		glEnd();

		// Disable line stippling
		glDisable(GL_LINE_STIPPLE);
	}

	glPopMatrix();
	view.endGL(); 
}

/* override */
bool ragePhBoundBoxNodeUI::select( MSelectInfo &selectInfo,
							MSelectionList &selectionList,
							MPointArray &worldSpaceSelectPts ) const
							//
							// Select function. Gets called when the bbox for the object is selected.
							// This function just selects the object without doing any intersection tests.
							//
{
//	Displayf("ragePhBoundBoxNodeUI::select");
	MSelectionMask priorityMask( MSelectionMask::kSelectObjectsMask );
	MSelectionList item;
	item.add( selectInfo.selectPath() );
	MPoint xformedPt;
	selectInfo.addSelection( item, xformedPt, selectionList,
		worldSpaceSelectPts, priorityMask, false );
	return true;
}

void ragePhBoundBoxNodeUI::getDrawRequestsWireframe( MDrawRequest& request,
											  const MDrawInfo& info )
{
//	Displayf("ragePhBoundBoxNodeUI::getDrawRequestsWireframe");
	request.setToken( quadricGeom::kDrawWireframe );

	M3dView::DisplayStatus displayStatus = info.displayStatus();
	M3dView::ColorTable activeColorTable = M3dView::kActiveColors;
	M3dView::ColorTable dormantColorTable = M3dView::kDormantColors;
	switch ( displayStatus )
	{
	case M3dView::kLead :
		request.setColor( LEAD_COLOR, activeColorTable );
		break;
	case M3dView::kActive :
		request.setColor( ACTIVE_COLOR, activeColorTable );
		break;
	case M3dView::kActiveAffected :
		request.setColor( ACTIVE_AFFECTED_COLOR, activeColorTable );
		break;
	case M3dView::kDormant :
		request.setColor( DORMANT_COLOR, dormantColorTable );
		break;
	case M3dView::kHilite :
		request.setColor( HILITE_COLOR, activeColorTable );
		break;
	default:	
		break;
	}
}

void ragePhBoundBoxNodeUI::getDrawRequestsShaded( MDrawRequest& request,
										   const MDrawInfo& info,
										   MDrawRequestQueue& queue,
										   MDrawData& data )
{
//	Displayf("ragePhBoundBoxNodeUI::getDrawRequestsShaded");
	// Need to get the material info
	//
	MDagPath path = info.multiPath();	// path to your dag object 
	M3dView view = info.view();; 		// view to draw to
	MMaterial material = MPxSurfaceShapeUI::material( path );
	M3dView::DisplayStatus displayStatus = info.displayStatus();

	// Evaluate the material and if necessary, the texture.
	//
	if ( ! material.evaluateMaterial( view, path ) ) {
		cerr << "Couldnt evaluate\n";
	}

	bool drawTexture = true;
	if ( drawTexture && material.materialIsTextured() ) {
		material.evaluateTexture( data );
	}

	request.setMaterial( material );

	bool materialTransparent = false;
	material.getHasTransparency( materialTransparent );
	if ( materialTransparent ) {
		request.setIsTransparent( true );
	}

	// create a draw request for wireframe on shaded if
	// necessary.
	//
	if ( (displayStatus == M3dView::kActive) ||
		(displayStatus == M3dView::kLead) ||
		(displayStatus == M3dView::kHilite) )
	{
		MDrawRequest wireRequest = info.getPrototype( *this );
		wireRequest.setDrawData( data );
		getDrawRequestsWireframe( wireRequest, info );
		wireRequest.setToken( quadricGeom::kDrawWireframeOnShaded );
		wireRequest.setDisplayStyle( M3dView::kWireFrame );
		queue.add( wireRequest );
	}
}

