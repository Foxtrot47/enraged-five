namespace HelpGeneratorUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( Form1 ) );
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileNewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileOpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.fileSaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileSaveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.filePrintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filePrintPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.fileRecentFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.fileExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsReloadLastFileAtStartupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsClearRecentFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpContentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.helpAboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputFormatsGroupBox = new System.Windows.Forms.GroupBox();
            this.windowsHelpFormatCheckBox = new System.Windows.Forms.CheckBox();
            this.pdfFormatCheckBox = new System.Windows.Forms.CheckBox();
            this.xmlFormatCheckBox = new System.Windows.Forms.CheckBox();
            this.help2FormatCheckBox = new System.Windows.Forms.CheckBox();
            this.htmlHelpFormatCheckBox = new System.Windows.Forms.CheckBox();
            this.htmlFormatCheckBox = new System.Windows.Forms.CheckBox();
            this.templateFilesGroupBox = new System.Windows.Forms.GroupBox();
            this.browseMasterDoxFileButton = new System.Windows.Forms.Button();
            this.masterDoxFileTextBox = new System.Windows.Forms.TextBox();
            this.masterDoxFileLabel = new System.Windows.Forms.Label();
            this.browseOverviewDoxFileButton = new System.Windows.Forms.Button();
            this.overviewDoxFileTextBox = new System.Windows.Forms.TextBox();
            this.overviewDoxLabel = new System.Windows.Forms.Label();
            this.browseDoxFileButton = new System.Windows.Forms.Button();
            this.doxFileTextBox = new System.Windows.Forms.TextBox();
            this.doxFileLabel = new System.Windows.Forms.Label();
            this.browseDtxFileButton = new System.Windows.Forms.Button();
            this.dtxFileTextBox = new System.Windows.Forms.TextBox();
            this.dtxFileLabel = new System.Windows.Forms.Label();
            this.sourceControlGroupBox = new System.Windows.Forms.GroupBox();
            this.sourceControlEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this.outputGroupBox = new System.Windows.Forms.GroupBox();
            this.unifiedHelpFileNameTextBox = new System.Windows.Forms.TextBox();
            this.qaWarningsExecutableLabel = new System.Windows.Forms.Label();
            this.unifyHelpFilesCheckBox = new System.Windows.Forms.CheckBox();
            this.browseOutputDirectoryButton = new System.Windows.Forms.Button();
            this.outputDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.overrideOutputDirectoryCheckBox = new System.Windows.Forms.CheckBox();
            this.outputOverviewHelpFilesCheckBox = new System.Windows.Forms.CheckBox();
            this.browseHelp2QaExecutableButton = new System.Windows.Forms.Button();
            this.help2QaExecutableTextBox = new System.Windows.Forms.TextBox();
            this.outputSourceDirectoryTreesTextBox = new System.Windows.Forms.TextBox();
            this.outputSourceDirectoryTreesLabel = new System.Windows.Forms.Label();
            this.rebuildCheckBox = new System.Windows.Forms.CheckBox();
            this.verboseLevelLabel = new System.Windows.Forms.Label();
            this.verboseLevelNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.uninstallHelpFilesButton = new System.Windows.Forms.Button();
            this.installHelpFilesButton = new System.Windows.Forms.Button();
            this.haltOnErrorCheckBox = new System.Windows.Forms.CheckBox();
            this.generateHelpFilesButton = new System.Windows.Forms.Button();
            this.outputKeepIntermediateFilesCheckBox = new System.Windows.Forms.CheckBox();
            this.browseOutputSourceDirectoriesButton = new System.Windows.Forms.Button();
            this.outputSourceDirectoriesTextBox = new System.Windows.Forms.TextBox();
            this.outputSourceDirectoriesLabel = new System.Windows.Forms.Label();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.printPreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument = new System.Drawing.Printing.PrintDocument();
            this.printDialog = new System.Windows.Forms.PrintDialog();
            this.inputGroupBox = new System.Windows.Forms.GroupBox();
            this.buildHelpGroupBox = new System.Windows.Forms.GroupBox();
            this.showWhenFinishedCheckBox = new System.Windows.Forms.CheckBox();
            this.extractQAInfoButton = new System.Windows.Forms.Button();
            this.treeNamespaceLabel = new System.Windows.Forms.Label();
            this.treeNamespaceTextBox = new System.Windows.Forms.TextBox();
            this.hxaLinkGroupLabel = new System.Windows.Forms.Label();
            this.hxaLinkGroupTextBox = new System.Windows.Forms.TextBox();
            this.help2RegExecutableLabel = new System.Windows.Forms.Label();
            this.help2RegExecutableTextBox = new System.Windows.Forms.TextBox();
            this.browseHelp2RegExecutableButton = new System.Windows.Forms.Button();
            this.companyFilterGroupBox = new System.Windows.Forms.GroupBox();
            this.companyFilterAttributeValueTextBox = new System.Windows.Forms.TextBox();
            this.companyFilterAttributeValueLabel = new System.Windows.Forms.Label();
            this.companyFilterAttributeNameTextBox = new System.Windows.Forms.TextBox();
            this.companyFilterAttributeNameLabel = new System.Windows.Forms.Label();
            this.companyFilterDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.companyFilterDescriptionLabel = new System.Windows.Forms.Label();
            this.installationInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.projectFilterGroupBox = new System.Windows.Forms.GroupBox();
            this.projectFilterAttributeValueTextBox = new System.Windows.Forms.TextBox();
            this.projectFilterAttributeValueLabel = new System.Windows.Forms.Label();
            this.projectFilterAttributeNameTextBox = new System.Windows.Forms.TextBox();
            this.projectFilterAttributeNameLabel = new System.Windows.Forms.Label();
            this.projectFilterDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.projectFilterDescriptionLabel = new System.Windows.Forms.Label();
            this.sourceControlProviderControl = new RSG.Base.Forms.SourceControlProviderControl();
            this.menuStrip1.SuspendLayout();
            this.outputFormatsGroupBox.SuspendLayout();
            this.templateFilesGroupBox.SuspendLayout();
            this.sourceControlGroupBox.SuspendLayout();
            this.outputGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.verboseLevelNumericUpDown)).BeginInit();
            this.inputGroupBox.SuspendLayout();
            this.buildHelpGroupBox.SuspendLayout();
            this.companyFilterGroupBox.SuspendLayout();
            this.installationInfoGroupBox.SuspendLayout();
            this.projectFilterGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem} );
            this.menuStrip1.Location = new System.Drawing.Point( 0, 0 );
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size( 874, 24 );
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.fileNewToolStripMenuItem,
            this.fileOpenToolStripMenuItem,
            this.toolStripSeparator,
            this.fileSaveToolStripMenuItem,
            this.fileSaveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.filePrintToolStripMenuItem,
            this.filePrintPreviewToolStripMenuItem,
            this.toolStripSeparator3,
            this.fileRecentFilesToolStripMenuItem,
            this.toolStripSeparator2,
            this.fileExitToolStripMenuItem} );
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size( 35, 20 );
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // fileNewToolStripMenuItem
            // 
            this.fileNewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject( "fileNewToolStripMenuItem.Image" )));
            this.fileNewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileNewToolStripMenuItem.Name = "fileNewToolStripMenuItem";
            this.fileNewToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.fileNewToolStripMenuItem.Size = new System.Drawing.Size( 151, 22 );
            this.fileNewToolStripMenuItem.Text = "&New";
            this.fileNewToolStripMenuItem.Click += new System.EventHandler( this.fileNewToolStripMenuItem_Click );
            // 
            // fileOpenToolStripMenuItem
            // 
            this.fileOpenToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject( "fileOpenToolStripMenuItem.Image" )));
            this.fileOpenToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileOpenToolStripMenuItem.Name = "fileOpenToolStripMenuItem";
            this.fileOpenToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.fileOpenToolStripMenuItem.Size = new System.Drawing.Size( 151, 22 );
            this.fileOpenToolStripMenuItem.Text = "&Open";
            this.fileOpenToolStripMenuItem.Click += new System.EventHandler( this.fileOpenToolStripMenuItem_Click );
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size( 148, 6 );
            // 
            // fileSaveToolStripMenuItem
            // 
            this.fileSaveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject( "fileSaveToolStripMenuItem.Image" )));
            this.fileSaveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileSaveToolStripMenuItem.Name = "fileSaveToolStripMenuItem";
            this.fileSaveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.fileSaveToolStripMenuItem.Size = new System.Drawing.Size( 151, 22 );
            this.fileSaveToolStripMenuItem.Text = "&Save";
            this.fileSaveToolStripMenuItem.Click += new System.EventHandler( this.fileSaveToolStripMenuItem_Click );
            // 
            // fileSaveAsToolStripMenuItem
            // 
            this.fileSaveAsToolStripMenuItem.Name = "fileSaveAsToolStripMenuItem";
            this.fileSaveAsToolStripMenuItem.Size = new System.Drawing.Size( 151, 22 );
            this.fileSaveAsToolStripMenuItem.Text = "Save &As";
            this.fileSaveAsToolStripMenuItem.Click += new System.EventHandler( this.fileSaveAsToolStripMenuItem_Click );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 148, 6 );
            // 
            // filePrintToolStripMenuItem
            // 
            this.filePrintToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject( "filePrintToolStripMenuItem.Image" )));
            this.filePrintToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.filePrintToolStripMenuItem.Name = "filePrintToolStripMenuItem";
            this.filePrintToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.filePrintToolStripMenuItem.Size = new System.Drawing.Size( 151, 22 );
            this.filePrintToolStripMenuItem.Text = "&Print";
            this.filePrintToolStripMenuItem.Click += new System.EventHandler( this.filePrintToolStripMenuItem_Click );
            // 
            // filePrintPreviewToolStripMenuItem
            // 
            this.filePrintPreviewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject( "filePrintPreviewToolStripMenuItem.Image" )));
            this.filePrintPreviewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.filePrintPreviewToolStripMenuItem.Name = "filePrintPreviewToolStripMenuItem";
            this.filePrintPreviewToolStripMenuItem.Size = new System.Drawing.Size( 151, 22 );
            this.filePrintPreviewToolStripMenuItem.Text = "Print Pre&view";
            this.filePrintPreviewToolStripMenuItem.Click += new System.EventHandler( this.filePrintPreviewToolStripMenuItem_Click );
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size( 148, 6 );
            // 
            // fileRecentFilesToolStripMenuItem
            // 
            this.fileRecentFilesToolStripMenuItem.Name = "fileRecentFilesToolStripMenuItem";
            this.fileRecentFilesToolStripMenuItem.Size = new System.Drawing.Size( 151, 22 );
            this.fileRecentFilesToolStripMenuItem.Text = "&Recent Files";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 148, 6 );
            // 
            // fileExitToolStripMenuItem
            // 
            this.fileExitToolStripMenuItem.Name = "fileExitToolStripMenuItem";
            this.fileExitToolStripMenuItem.Size = new System.Drawing.Size( 151, 22 );
            this.fileExitToolStripMenuItem.Text = "E&xit";
            this.fileExitToolStripMenuItem.Click += new System.EventHandler( this.fileExitToolStripMenuItem_Click );
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.toolsReloadLastFileAtStartupToolStripMenuItem,
            this.toolsClearRecentFilesToolStripMenuItem} );
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size( 44, 20 );
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // toolsReloadLastFileAtStartupToolStripMenuItem
            // 
            this.toolsReloadLastFileAtStartupToolStripMenuItem.Checked = true;
            this.toolsReloadLastFileAtStartupToolStripMenuItem.CheckOnClick = true;
            this.toolsReloadLastFileAtStartupToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolsReloadLastFileAtStartupToolStripMenuItem.Name = "toolsReloadLastFileAtStartupToolStripMenuItem";
            this.toolsReloadLastFileAtStartupToolStripMenuItem.Size = new System.Drawing.Size( 212, 22 );
            this.toolsReloadLastFileAtStartupToolStripMenuItem.Text = "&Reload Last File at Startup";
            // 
            // toolsClearRecentFilesToolStripMenuItem
            // 
            this.toolsClearRecentFilesToolStripMenuItem.Name = "toolsClearRecentFilesToolStripMenuItem";
            this.toolsClearRecentFilesToolStripMenuItem.Size = new System.Drawing.Size( 212, 22 );
            this.toolsClearRecentFilesToolStripMenuItem.Text = "Clear Recent Files";
            this.toolsClearRecentFilesToolStripMenuItem.Click += new System.EventHandler( this.toolsClearRecentFilesToolStripMenuItem_Click );
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.helpContentsToolStripMenuItem,
            this.toolStripSeparator5,
            this.helpAboutToolStripMenuItem} );
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size( 40, 20 );
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // helpContentsToolStripMenuItem
            // 
            this.helpContentsToolStripMenuItem.Name = "helpContentsToolStripMenuItem";
            this.helpContentsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.helpContentsToolStripMenuItem.Size = new System.Drawing.Size( 148, 22 );
            this.helpContentsToolStripMenuItem.Text = "&Contents";
            this.helpContentsToolStripMenuItem.Click += new System.EventHandler( this.helpContentsToolStripMenuItem_Click );
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size( 145, 6 );
            // 
            // helpAboutToolStripMenuItem
            // 
            this.helpAboutToolStripMenuItem.Name = "helpAboutToolStripMenuItem";
            this.helpAboutToolStripMenuItem.Size = new System.Drawing.Size( 148, 22 );
            this.helpAboutToolStripMenuItem.Text = "&About...";
            this.helpAboutToolStripMenuItem.Click += new System.EventHandler( this.helpAboutToolStripMenuItem_Click );
            // 
            // outputFormatsGroupBox
            // 
            this.outputFormatsGroupBox.Controls.Add( this.windowsHelpFormatCheckBox );
            this.outputFormatsGroupBox.Controls.Add( this.pdfFormatCheckBox );
            this.outputFormatsGroupBox.Controls.Add( this.xmlFormatCheckBox );
            this.outputFormatsGroupBox.Controls.Add( this.help2FormatCheckBox );
            this.outputFormatsGroupBox.Controls.Add( this.htmlHelpFormatCheckBox );
            this.outputFormatsGroupBox.Controls.Add( this.htmlFormatCheckBox );
            this.outputFormatsGroupBox.Location = new System.Drawing.Point( 6, 19 );
            this.outputFormatsGroupBox.Name = "outputFormatsGroupBox";
            this.outputFormatsGroupBox.Size = new System.Drawing.Size( 410, 66 );
            this.outputFormatsGroupBox.TabIndex = 0;
            this.outputFormatsGroupBox.TabStop = false;
            this.outputFormatsGroupBox.Text = "Formats";
            // 
            // windowsHelpFormatCheckBox
            // 
            this.windowsHelpFormatCheckBox.AutoSize = true;
            this.windowsHelpFormatCheckBox.Location = new System.Drawing.Point( 113, 42 );
            this.windowsHelpFormatCheckBox.Name = "windowsHelpFormatCheckBox";
            this.windowsHelpFormatCheckBox.Size = new System.Drawing.Size( 113, 17 );
            this.windowsHelpFormatCheckBox.TabIndex = 3;
            this.windowsHelpFormatCheckBox.Text = "Windows Help (rtf)";
            this.windowsHelpFormatCheckBox.UseVisualStyleBackColor = true;
            this.windowsHelpFormatCheckBox.CheckedChanged += new System.EventHandler( this.EnableGenerateHelpFilesButton_EventHandler );
            // 
            // pdfFormatCheckBox
            // 
            this.pdfFormatCheckBox.AutoSize = true;
            this.pdfFormatCheckBox.Location = new System.Drawing.Point( 232, 19 );
            this.pdfFormatCheckBox.Name = "pdfFormatCheckBox";
            this.pdfFormatCheckBox.Size = new System.Drawing.Size( 50, 17 );
            this.pdfFormatCheckBox.TabIndex = 4;
            this.pdfFormatCheckBox.Text = "PDF ";
            this.pdfFormatCheckBox.UseVisualStyleBackColor = true;
            this.pdfFormatCheckBox.CheckedChanged += new System.EventHandler( this.EnableGenerateHelpFilesButton_EventHandler );
            // 
            // xmlFormatCheckBox
            // 
            this.xmlFormatCheckBox.AutoSize = true;
            this.xmlFormatCheckBox.Location = new System.Drawing.Point( 232, 42 );
            this.xmlFormatCheckBox.Name = "xmlFormatCheckBox";
            this.xmlFormatCheckBox.Size = new System.Drawing.Size( 43, 17 );
            this.xmlFormatCheckBox.TabIndex = 5;
            this.xmlFormatCheckBox.Text = "Xml";
            this.xmlFormatCheckBox.UseVisualStyleBackColor = true;
            this.xmlFormatCheckBox.CheckedChanged += new System.EventHandler( this.EnableGenerateHelpFilesButton_EventHandler );
            // 
            // help2FormatCheckBox
            // 
            this.help2FormatCheckBox.AutoSize = true;
            this.help2FormatCheckBox.Location = new System.Drawing.Point( 6, 42 );
            this.help2FormatCheckBox.Name = "help2FormatCheckBox";
            this.help2FormatCheckBox.Size = new System.Drawing.Size( 57, 17 );
            this.help2FormatCheckBox.TabIndex = 1;
            this.help2FormatCheckBox.Text = "Help 2";
            this.help2FormatCheckBox.UseVisualStyleBackColor = true;
            this.help2FormatCheckBox.CheckedChanged += new System.EventHandler( this.help2FormatCheckBox_CheckedChanged );
            // 
            // htmlHelpFormatCheckBox
            // 
            this.htmlHelpFormatCheckBox.AutoSize = true;
            this.htmlHelpFormatCheckBox.Location = new System.Drawing.Point( 6, 19 );
            this.htmlHelpFormatCheckBox.Name = "htmlHelpFormatCheckBox";
            this.htmlHelpFormatCheckBox.Size = new System.Drawing.Size( 101, 17 );
            this.htmlHelpFormatCheckBox.TabIndex = 0;
            this.htmlHelpFormatCheckBox.Text = "Html Help (chm)";
            this.htmlHelpFormatCheckBox.UseVisualStyleBackColor = true;
            this.htmlHelpFormatCheckBox.CheckedChanged += new System.EventHandler( this.EnableGenerateHelpFilesButton_EventHandler );
            // 
            // htmlFormatCheckBox
            // 
            this.htmlFormatCheckBox.AutoSize = true;
            this.htmlFormatCheckBox.Location = new System.Drawing.Point( 113, 19 );
            this.htmlFormatCheckBox.Name = "htmlFormatCheckBox";
            this.htmlFormatCheckBox.Size = new System.Drawing.Size( 47, 17 );
            this.htmlFormatCheckBox.TabIndex = 2;
            this.htmlFormatCheckBox.Text = "Html";
            this.htmlFormatCheckBox.UseVisualStyleBackColor = true;
            this.htmlFormatCheckBox.CheckedChanged += new System.EventHandler( this.EnableGenerateHelpFilesButton_EventHandler );
            // 
            // templateFilesGroupBox
            // 
            this.templateFilesGroupBox.Controls.Add( this.browseMasterDoxFileButton );
            this.templateFilesGroupBox.Controls.Add( this.masterDoxFileTextBox );
            this.templateFilesGroupBox.Controls.Add( this.masterDoxFileLabel );
            this.templateFilesGroupBox.Controls.Add( this.browseOverviewDoxFileButton );
            this.templateFilesGroupBox.Controls.Add( this.overviewDoxFileTextBox );
            this.templateFilesGroupBox.Controls.Add( this.overviewDoxLabel );
            this.templateFilesGroupBox.Controls.Add( this.browseDoxFileButton );
            this.templateFilesGroupBox.Controls.Add( this.doxFileTextBox );
            this.templateFilesGroupBox.Controls.Add( this.doxFileLabel );
            this.templateFilesGroupBox.Controls.Add( this.browseDtxFileButton );
            this.templateFilesGroupBox.Controls.Add( this.dtxFileTextBox );
            this.templateFilesGroupBox.Controls.Add( this.dtxFileLabel );
            this.templateFilesGroupBox.Location = new System.Drawing.Point( 12, 27 );
            this.templateFilesGroupBox.Name = "templateFilesGroupBox";
            this.templateFilesGroupBox.Size = new System.Drawing.Size( 422, 126 );
            this.templateFilesGroupBox.TabIndex = 3;
            this.templateFilesGroupBox.TabStop = false;
            this.templateFilesGroupBox.Text = "Template Files";
            // 
            // browseMasterDoxFileButton
            // 
            this.browseMasterDoxFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseMasterDoxFileButton.Enabled = false;
            this.browseMasterDoxFileButton.Location = new System.Drawing.Point( 386, 95 );
            this.browseMasterDoxFileButton.Name = "browseMasterDoxFileButton";
            this.browseMasterDoxFileButton.Size = new System.Drawing.Size( 30, 23 );
            this.browseMasterDoxFileButton.TabIndex = 11;
            this.browseMasterDoxFileButton.Text = "...";
            this.browseMasterDoxFileButton.UseVisualStyleBackColor = true;
            this.browseMasterDoxFileButton.Click += new System.EventHandler( this.browseMasterDoxFileButton_Click );
            // 
            // masterDoxFileTextBox
            // 
            this.masterDoxFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.masterDoxFileTextBox.Enabled = false;
            this.masterDoxFileTextBox.Location = new System.Drawing.Point( 108, 97 );
            this.masterDoxFileTextBox.Name = "masterDoxFileTextBox";
            this.masterDoxFileTextBox.Size = new System.Drawing.Size( 272, 20 );
            this.masterDoxFileTextBox.TabIndex = 10;
            this.masterDoxFileTextBox.TextChanged += new System.EventHandler( this.EnableGenerateHelpFilesButton_EventHandler );
            // 
            // masterDoxFileLabel
            // 
            this.masterDoxFileLabel.AutoSize = true;
            this.masterDoxFileLabel.Location = new System.Drawing.Point( 19, 100 );
            this.masterDoxFileLabel.Name = "masterDoxFileLabel";
            this.masterDoxFileLabel.Size = new System.Drawing.Size( 84, 13 );
            this.masterDoxFileLabel.TabIndex = 9;
            this.masterDoxFileLabel.Text = "Unified Dox File:";
            // 
            // browseOverviewDoxFileButton
            // 
            this.browseOverviewDoxFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseOverviewDoxFileButton.Enabled = false;
            this.browseOverviewDoxFileButton.Location = new System.Drawing.Point( 386, 69 );
            this.browseOverviewDoxFileButton.Name = "browseOverviewDoxFileButton";
            this.browseOverviewDoxFileButton.Size = new System.Drawing.Size( 30, 23 );
            this.browseOverviewDoxFileButton.TabIndex = 8;
            this.browseOverviewDoxFileButton.Text = "...";
            this.browseOverviewDoxFileButton.UseVisualStyleBackColor = true;
            this.browseOverviewDoxFileButton.Click += new System.EventHandler( this.browseOverviewDoxFileButton_Click );
            // 
            // overviewDoxFileTextBox
            // 
            this.overviewDoxFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.overviewDoxFileTextBox.Enabled = false;
            this.overviewDoxFileTextBox.Location = new System.Drawing.Point( 108, 71 );
            this.overviewDoxFileTextBox.Name = "overviewDoxFileTextBox";
            this.overviewDoxFileTextBox.Size = new System.Drawing.Size( 272, 20 );
            this.overviewDoxFileTextBox.TabIndex = 7;
            this.overviewDoxFileTextBox.TextChanged += new System.EventHandler( this.EnableGenerateHelpFilesButton_EventHandler );
            // 
            // overviewDoxLabel
            // 
            this.overviewDoxLabel.AutoSize = true;
            this.overviewDoxLabel.Location = new System.Drawing.Point( 6, 74 );
            this.overviewDoxLabel.Name = "overviewDoxLabel";
            this.overviewDoxLabel.Size = new System.Drawing.Size( 96, 13 );
            this.overviewDoxLabel.TabIndex = 6;
            this.overviewDoxLabel.Text = "Overview Dox File:";
            // 
            // browseDoxFileButton
            // 
            this.browseDoxFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseDoxFileButton.Location = new System.Drawing.Point( 386, 43 );
            this.browseDoxFileButton.Name = "browseDoxFileButton";
            this.browseDoxFileButton.Size = new System.Drawing.Size( 30, 23 );
            this.browseDoxFileButton.TabIndex = 5;
            this.browseDoxFileButton.Text = "...";
            this.browseDoxFileButton.UseVisualStyleBackColor = true;
            this.browseDoxFileButton.Click += new System.EventHandler( this.browseDoxFileButton_Click );
            // 
            // doxFileTextBox
            // 
            this.doxFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.doxFileTextBox.Location = new System.Drawing.Point( 108, 45 );
            this.doxFileTextBox.Name = "doxFileTextBox";
            this.doxFileTextBox.Size = new System.Drawing.Size( 272, 20 );
            this.doxFileTextBox.TabIndex = 4;
            this.doxFileTextBox.TextChanged += new System.EventHandler( this.EnableGenerateHelpFilesButton_EventHandler );
            // 
            // doxFileLabel
            // 
            this.doxFileLabel.AutoSize = true;
            this.doxFileLabel.Location = new System.Drawing.Point( 54, 48 );
            this.doxFileLabel.Name = "doxFileLabel";
            this.doxFileLabel.Size = new System.Drawing.Size( 48, 13 );
            this.doxFileLabel.TabIndex = 3;
            this.doxFileLabel.Text = "Dox File:";
            // 
            // browseDtxFileButton
            // 
            this.browseDtxFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseDtxFileButton.Location = new System.Drawing.Point( 386, 17 );
            this.browseDtxFileButton.Name = "browseDtxFileButton";
            this.browseDtxFileButton.Size = new System.Drawing.Size( 30, 23 );
            this.browseDtxFileButton.TabIndex = 2;
            this.browseDtxFileButton.Text = "...";
            this.browseDtxFileButton.UseVisualStyleBackColor = true;
            this.browseDtxFileButton.Click += new System.EventHandler( this.browseDtxFileButton_Click );
            // 
            // dtxFileTextBox
            // 
            this.dtxFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dtxFileTextBox.Location = new System.Drawing.Point( 108, 19 );
            this.dtxFileTextBox.Name = "dtxFileTextBox";
            this.dtxFileTextBox.Size = new System.Drawing.Size( 272, 20 );
            this.dtxFileTextBox.TabIndex = 1;
            this.dtxFileTextBox.TextChanged += new System.EventHandler( this.EnableGenerateHelpFilesButton_EventHandler );
            // 
            // dtxFileLabel
            // 
            this.dtxFileLabel.AutoSize = true;
            this.dtxFileLabel.Location = new System.Drawing.Point( 57, 22 );
            this.dtxFileLabel.Name = "dtxFileLabel";
            this.dtxFileLabel.Size = new System.Drawing.Size( 45, 13 );
            this.dtxFileLabel.TabIndex = 0;
            this.dtxFileLabel.Text = "Dtx File:";
            // 
            // sourceControlGroupBox
            // 
            this.sourceControlGroupBox.Controls.Add( this.sourceControlProviderControl );
            this.sourceControlGroupBox.Controls.Add( this.sourceControlEnabledCheckBox );
            this.sourceControlGroupBox.Location = new System.Drawing.Point( 440, 27 );
            this.sourceControlGroupBox.Name = "sourceControlGroupBox";
            this.sourceControlGroupBox.Size = new System.Drawing.Size( 422, 182 );
            this.sourceControlGroupBox.TabIndex = 5;
            this.sourceControlGroupBox.TabStop = false;
            this.sourceControlGroupBox.Text = "Source Control";
            // 
            // sourceControlEnabledCheckBox
            // 
            this.sourceControlEnabledCheckBox.AutoSize = true;
            this.sourceControlEnabledCheckBox.Location = new System.Drawing.Point( 9, 19 );
            this.sourceControlEnabledCheckBox.Name = "sourceControlEnabledCheckBox";
            this.sourceControlEnabledCheckBox.Size = new System.Drawing.Size( 132, 17 );
            this.sourceControlEnabledCheckBox.TabIndex = 0;
            this.sourceControlEnabledCheckBox.Text = "Enable Source Control";
            this.sourceControlEnabledCheckBox.UseVisualStyleBackColor = true;
            this.sourceControlEnabledCheckBox.CheckedChanged += new System.EventHandler( this.sourceControlEnabledCheckBox_CheckedChanged );
            // 
            // outputGroupBox
            // 
            this.outputGroupBox.Controls.Add( this.unifiedHelpFileNameTextBox );
            this.outputGroupBox.Controls.Add( this.qaWarningsExecutableLabel );
            this.outputGroupBox.Controls.Add( this.unifyHelpFilesCheckBox );
            this.outputGroupBox.Controls.Add( this.browseOutputDirectoryButton );
            this.outputGroupBox.Controls.Add( this.outputDirectoryTextBox );
            this.outputGroupBox.Controls.Add( this.overrideOutputDirectoryCheckBox );
            this.outputGroupBox.Controls.Add( this.outputFormatsGroupBox );
            this.outputGroupBox.Controls.Add( this.outputOverviewHelpFilesCheckBox );
            this.outputGroupBox.Controls.Add( this.browseHelp2QaExecutableButton );
            this.outputGroupBox.Controls.Add( this.help2QaExecutableTextBox );
            this.outputGroupBox.Location = new System.Drawing.Point( 440, 291 );
            this.outputGroupBox.Name = "outputGroupBox";
            this.outputGroupBox.Size = new System.Drawing.Size( 422, 198 );
            this.outputGroupBox.TabIndex = 7;
            this.outputGroupBox.TabStop = false;
            this.outputGroupBox.Text = "Output";
            // 
            // unifiedHelpFileNameTextBox
            // 
            this.unifiedHelpFileNameTextBox.Enabled = false;
            this.unifiedHelpFileNameTextBox.Location = new System.Drawing.Point( 158, 123 );
            this.unifiedHelpFileNameTextBox.Name = "unifiedHelpFileNameTextBox";
            this.unifiedHelpFileNameTextBox.Size = new System.Drawing.Size( 258, 20 );
            this.unifiedHelpFileNameTextBox.TabIndex = 5;
            this.unifiedHelpFileNameTextBox.TextChanged += new System.EventHandler( this.unifiedHelpFileNameTextBox_TextChanged );
            // 
            // qaWarningsExecutableLabel
            // 
            this.qaWarningsExecutableLabel.AutoSize = true;
            this.qaWarningsExecutableLabel.Location = new System.Drawing.Point( 23, 172 );
            this.qaWarningsExecutableLabel.Name = "qaWarningsExecutableLabel";
            this.qaWarningsExecutableLabel.Size = new System.Drawing.Size( 129, 13 );
            this.qaWarningsExecutableLabel.TabIndex = 7;
            this.qaWarningsExecutableLabel.Text = "QA Warnings Executable:";
            // 
            // unifyHelpFilesCheckBox
            // 
            this.unifyHelpFilesCheckBox.AutoSize = true;
            this.unifyHelpFilesCheckBox.Location = new System.Drawing.Point( 6, 123 );
            this.unifyHelpFilesCheckBox.Name = "unifyHelpFilesCheckBox";
            this.unifyHelpFilesCheckBox.Size = new System.Drawing.Size( 99, 17 );
            this.unifyHelpFilesCheckBox.TabIndex = 4;
            this.unifyHelpFilesCheckBox.Text = "Unify Help Files";
            this.unifyHelpFilesCheckBox.UseVisualStyleBackColor = true;
            this.unifyHelpFilesCheckBox.CheckedChanged += new System.EventHandler( this.unifyHelpFilesCheckBox_CheckedChanged );
            // 
            // browseOutputDirectoryButton
            // 
            this.browseOutputDirectoryButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseOutputDirectoryButton.Location = new System.Drawing.Point( 386, 94 );
            this.browseOutputDirectoryButton.Name = "browseOutputDirectoryButton";
            this.browseOutputDirectoryButton.Size = new System.Drawing.Size( 30, 23 );
            this.browseOutputDirectoryButton.TabIndex = 3;
            this.browseOutputDirectoryButton.Text = "...";
            this.browseOutputDirectoryButton.UseVisualStyleBackColor = true;
            this.browseOutputDirectoryButton.Click += new System.EventHandler( this.browseOutputDirectoryButton_Click );
            // 
            // outputDirectoryTextBox
            // 
            this.outputDirectoryTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.outputDirectoryTextBox.Location = new System.Drawing.Point( 158, 96 );
            this.outputDirectoryTextBox.Name = "outputDirectoryTextBox";
            this.outputDirectoryTextBox.Size = new System.Drawing.Size( 222, 20 );
            this.outputDirectoryTextBox.TabIndex = 2;
            this.outputDirectoryTextBox.TextChanged += new System.EventHandler( this.outputDirectoryTextBox_TextChanged );
            // 
            // overrideOutputDirectoryCheckBox
            // 
            this.overrideOutputDirectoryCheckBox.AutoSize = true;
            this.overrideOutputDirectoryCheckBox.Location = new System.Drawing.Point( 6, 99 );
            this.overrideOutputDirectoryCheckBox.Name = "overrideOutputDirectoryCheckBox";
            this.overrideOutputDirectoryCheckBox.Size = new System.Drawing.Size( 146, 17 );
            this.overrideOutputDirectoryCheckBox.TabIndex = 1;
            this.overrideOutputDirectoryCheckBox.Text = "Override Output Directory";
            this.overrideOutputDirectoryCheckBox.UseVisualStyleBackColor = true;
            this.overrideOutputDirectoryCheckBox.CheckedChanged += new System.EventHandler( this.overrideOutputDirectoryCheckBox_CheckedChanged );
            // 
            // outputOverviewHelpFilesCheckBox
            // 
            this.outputOverviewHelpFilesCheckBox.AutoSize = true;
            this.outputOverviewHelpFilesCheckBox.Location = new System.Drawing.Point( 6, 146 );
            this.outputOverviewHelpFilesCheckBox.Name = "outputOverviewHelpFilesCheckBox";
            this.outputOverviewHelpFilesCheckBox.Size = new System.Drawing.Size( 155, 17 );
            this.outputOverviewHelpFilesCheckBox.TabIndex = 6;
            this.outputOverviewHelpFilesCheckBox.Text = "Output Overview Help Files";
            this.outputOverviewHelpFilesCheckBox.UseVisualStyleBackColor = true;
            this.outputOverviewHelpFilesCheckBox.CheckedChanged += new System.EventHandler( this.outputOverviewHelpFilesCheckBox_CheckedChanged );
            // 
            // browseHelp2QaExecutableButton
            // 
            this.browseHelp2QaExecutableButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseHelp2QaExecutableButton.Location = new System.Drawing.Point( 386, 167 );
            this.browseHelp2QaExecutableButton.Name = "browseHelp2QaExecutableButton";
            this.browseHelp2QaExecutableButton.Size = new System.Drawing.Size( 30, 23 );
            this.browseHelp2QaExecutableButton.TabIndex = 9;
            this.browseHelp2QaExecutableButton.Text = "...";
            this.browseHelp2QaExecutableButton.UseVisualStyleBackColor = true;
            this.browseHelp2QaExecutableButton.Click += new System.EventHandler( this.browseHelp2QaExecutableButton_Click );
            // 
            // help2QaExecutableTextBox
            // 
            this.help2QaExecutableTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.help2QaExecutableTextBox.Location = new System.Drawing.Point( 158, 169 );
            this.help2QaExecutableTextBox.Name = "help2QaExecutableTextBox";
            this.help2QaExecutableTextBox.Size = new System.Drawing.Size( 222, 20 );
            this.help2QaExecutableTextBox.TabIndex = 8;
            this.help2QaExecutableTextBox.TextChanged += new System.EventHandler( this.EnableGenerateHelpFilesButton_EventHandler );
            // 
            // outputSourceDirectoryTreesTextBox
            // 
            this.outputSourceDirectoryTreesTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.outputSourceDirectoryTreesTextBox.Location = new System.Drawing.Point( 158, 39 );
            this.outputSourceDirectoryTreesTextBox.Name = "outputSourceDirectoryTreesTextBox";
            this.outputSourceDirectoryTreesTextBox.Size = new System.Drawing.Size( 257, 20 );
            this.outputSourceDirectoryTreesTextBox.TabIndex = 4;
            this.outputSourceDirectoryTreesTextBox.TextChanged += new System.EventHandler( this.EnableProjectSave_EventHandler );
            // 
            // outputSourceDirectoryTreesLabel
            // 
            this.outputSourceDirectoryTreesLabel.AutoSize = true;
            this.outputSourceDirectoryTreesLabel.Location = new System.Drawing.Point( 33, 42 );
            this.outputSourceDirectoryTreesLabel.Name = "outputSourceDirectoryTreesLabel";
            this.outputSourceDirectoryTreesLabel.Size = new System.Drawing.Size( 119, 13 );
            this.outputSourceDirectoryTreesLabel.TabIndex = 3;
            this.outputSourceDirectoryTreesLabel.Text = "Source Directory Trees:";
            // 
            // rebuildCheckBox
            // 
            this.rebuildCheckBox.AutoSize = true;
            this.rebuildCheckBox.Location = new System.Drawing.Point( 180, 20 );
            this.rebuildCheckBox.Name = "rebuildCheckBox";
            this.rebuildCheckBox.Size = new System.Drawing.Size( 62, 17 );
            this.rebuildCheckBox.TabIndex = 3;
            this.rebuildCheckBox.Text = "Rebuild";
            this.rebuildCheckBox.UseVisualStyleBackColor = true;
            this.rebuildCheckBox.CheckedChanged += new System.EventHandler( this.EnableProjectSave_EventHandler );
            // 
            // verboseLevelLabel
            // 
            this.verboseLevelLabel.AutoSize = true;
            this.verboseLevelLabel.Location = new System.Drawing.Point( 3, 21 );
            this.verboseLevelLabel.Name = "verboseLevelLabel";
            this.verboseLevelLabel.Size = new System.Drawing.Size( 78, 13 );
            this.verboseLevelLabel.TabIndex = 0;
            this.verboseLevelLabel.Text = "Verbose Level:";
            // 
            // verboseLevelNumericUpDown
            // 
            this.verboseLevelNumericUpDown.Location = new System.Drawing.Point( 87, 19 );
            this.verboseLevelNumericUpDown.Maximum = new decimal( new int[] {
            3,
            0,
            0,
            0} );
            this.verboseLevelNumericUpDown.Minimum = new decimal( new int[] {
            1,
            0,
            0,
            0} );
            this.verboseLevelNumericUpDown.Name = "verboseLevelNumericUpDown";
            this.verboseLevelNumericUpDown.Size = new System.Drawing.Size( 70, 20 );
            this.verboseLevelNumericUpDown.TabIndex = 1;
            this.verboseLevelNumericUpDown.Value = new decimal( new int[] {
            2,
            0,
            0,
            0} );
            this.verboseLevelNumericUpDown.ValueChanged += new System.EventHandler( this.verboseLevelNumericUpDown_ValueChanged );
            // 
            // uninstallHelpFilesButton
            // 
            this.uninstallHelpFilesButton.Enabled = false;
            this.uninstallHelpFilesButton.Location = new System.Drawing.Point( 619, 49 );
            this.uninstallHelpFilesButton.Name = "uninstallHelpFilesButton";
            this.uninstallHelpFilesButton.Size = new System.Drawing.Size( 225, 23 );
            this.uninstallHelpFilesButton.TabIndex = 8;
            this.uninstallHelpFilesButton.Text = "Uninstall Help 2 Files";
            this.uninstallHelpFilesButton.UseVisualStyleBackColor = true;
            this.uninstallHelpFilesButton.Click += new System.EventHandler( this.uninstallHelpFilesButton_Click );
            // 
            // installHelpFilesButton
            // 
            this.installHelpFilesButton.Enabled = false;
            this.installHelpFilesButton.Location = new System.Drawing.Point( 619, 19 );
            this.installHelpFilesButton.Name = "installHelpFilesButton";
            this.installHelpFilesButton.Size = new System.Drawing.Size( 225, 23 );
            this.installHelpFilesButton.TabIndex = 7;
            this.installHelpFilesButton.Text = "Install Help 2 Files";
            this.installHelpFilesButton.UseVisualStyleBackColor = true;
            this.installHelpFilesButton.Click += new System.EventHandler( this.installHelpFilesButton_Click );
            // 
            // haltOnErrorCheckBox
            // 
            this.haltOnErrorCheckBox.AutoSize = true;
            this.haltOnErrorCheckBox.Location = new System.Drawing.Point( 6, 53 );
            this.haltOnErrorCheckBox.Name = "haltOnErrorCheckBox";
            this.haltOnErrorCheckBox.Size = new System.Drawing.Size( 87, 17 );
            this.haltOnErrorCheckBox.TabIndex = 2;
            this.haltOnErrorCheckBox.Text = "Halt On Error";
            this.haltOnErrorCheckBox.UseVisualStyleBackColor = true;
            this.haltOnErrorCheckBox.CheckedChanged += new System.EventHandler( this.EnableProjectSave_EventHandler );
            // 
            // generateHelpFilesButton
            // 
            this.generateHelpFilesButton.Enabled = false;
            this.generateHelpFilesButton.Location = new System.Drawing.Point( 388, 19 );
            this.generateHelpFilesButton.Name = "generateHelpFilesButton";
            this.generateHelpFilesButton.Size = new System.Drawing.Size( 225, 23 );
            this.generateHelpFilesButton.TabIndex = 5;
            this.generateHelpFilesButton.Text = "Generate Help Files";
            this.generateHelpFilesButton.UseVisualStyleBackColor = true;
            this.generateHelpFilesButton.Click += new System.EventHandler( this.generateHelpFilesButton_Click );
            // 
            // outputKeepIntermediateFilesCheckBox
            // 
            this.outputKeepIntermediateFilesCheckBox.AutoSize = true;
            this.outputKeepIntermediateFilesCheckBox.Location = new System.Drawing.Point( 180, 53 );
            this.outputKeepIntermediateFilesCheckBox.Name = "outputKeepIntermediateFilesCheckBox";
            this.outputKeepIntermediateFilesCheckBox.Size = new System.Drawing.Size( 136, 17 );
            this.outputKeepIntermediateFilesCheckBox.TabIndex = 4;
            this.outputKeepIntermediateFilesCheckBox.Text = "Keep Intermediate Files";
            this.outputKeepIntermediateFilesCheckBox.UseVisualStyleBackColor = true;
            this.outputKeepIntermediateFilesCheckBox.CheckedChanged += new System.EventHandler( this.EnableProjectSave_EventHandler );
            // 
            // browseOutputSourceDirectoriesButton
            // 
            this.browseOutputSourceDirectoriesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseOutputSourceDirectoriesButton.Location = new System.Drawing.Point( 386, 11 );
            this.browseOutputSourceDirectoriesButton.Name = "browseOutputSourceDirectoriesButton";
            this.browseOutputSourceDirectoriesButton.Size = new System.Drawing.Size( 30, 23 );
            this.browseOutputSourceDirectoriesButton.TabIndex = 2;
            this.browseOutputSourceDirectoriesButton.Text = "...";
            this.browseOutputSourceDirectoriesButton.UseVisualStyleBackColor = true;
            this.browseOutputSourceDirectoriesButton.Click += new System.EventHandler( this.browseOutputSourceDirectoriesButton_Click );
            // 
            // outputSourceDirectoriesTextBox
            // 
            this.outputSourceDirectoriesTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.outputSourceDirectoriesTextBox.Location = new System.Drawing.Point( 158, 13 );
            this.outputSourceDirectoriesTextBox.Name = "outputSourceDirectoriesTextBox";
            this.outputSourceDirectoriesTextBox.Size = new System.Drawing.Size( 222, 20 );
            this.outputSourceDirectoriesTextBox.TabIndex = 1;
            this.outputSourceDirectoriesTextBox.TextChanged += new System.EventHandler( this.outputSourceDirectoriesTextBox_TextChanged );
            // 
            // outputSourceDirectoriesLabel
            // 
            this.outputSourceDirectoriesLabel.AutoSize = true;
            this.outputSourceDirectoriesLabel.Location = new System.Drawing.Point( 55, 16 );
            this.outputSourceDirectoriesLabel.Name = "outputSourceDirectoriesLabel";
            this.outputSourceDirectoriesLabel.Size = new System.Drawing.Size( 97, 13 );
            this.outputSourceDirectoriesLabel.TabIndex = 0;
            this.outputSourceDirectoriesLabel.Text = "Source Directories:";
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.Description = "Add Directory to Process";
            this.folderBrowserDialog.ShowNewFolderButton = false;
            // 
            // openFileDialog
            // 
            this.openFileDialog.AddExtension = false;
            this.openFileDialog.Title = "Select File";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "xml";
            this.saveFileDialog.Filter = "Xml Files (*.xml)|*.xml";
            this.saveFileDialog.Title = "Save HelpGeneratorUI File";
            // 
            // printPreviewDialog
            // 
            this.printPreviewDialog.AutoScrollMargin = new System.Drawing.Size( 0, 0 );
            this.printPreviewDialog.AutoScrollMinSize = new System.Drawing.Size( 0, 0 );
            this.printPreviewDialog.ClientSize = new System.Drawing.Size( 400, 300 );
            this.printPreviewDialog.Document = this.printDocument;
            this.printPreviewDialog.Enabled = true;
            this.printPreviewDialog.Icon = ((System.Drawing.Icon)(resources.GetObject( "printPreviewDialog.Icon" )));
            this.printPreviewDialog.Name = "printPreviewDialog";
            this.printPreviewDialog.ShowIcon = false;
            this.printPreviewDialog.Visible = false;
            // 
            // printDocument
            // 
            this.printDocument.OriginAtMargins = true;
            this.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler( this.printDocument_PrintPage );
            this.printDocument.EndPrint += new System.Drawing.Printing.PrintEventHandler( this.printDocument_EndPrint );
            this.printDocument.BeginPrint += new System.Drawing.Printing.PrintEventHandler( this.printDocument_BeginPrint );
            // 
            // printDialog
            // 
            this.printDialog.AllowCurrentPage = true;
            this.printDialog.Document = this.printDocument;
            this.printDialog.UseEXDialog = true;
            // 
            // inputGroupBox
            // 
            this.inputGroupBox.Controls.Add( this.outputSourceDirectoriesLabel );
            this.inputGroupBox.Controls.Add( this.outputSourceDirectoriesTextBox );
            this.inputGroupBox.Controls.Add( this.browseOutputSourceDirectoriesButton );
            this.inputGroupBox.Controls.Add( this.outputSourceDirectoryTreesTextBox );
            this.inputGroupBox.Controls.Add( this.outputSourceDirectoryTreesLabel );
            this.inputGroupBox.Location = new System.Drawing.Point( 440, 215 );
            this.inputGroupBox.Name = "inputGroupBox";
            this.inputGroupBox.Size = new System.Drawing.Size( 422, 70 );
            this.inputGroupBox.TabIndex = 8;
            this.inputGroupBox.TabStop = false;
            this.inputGroupBox.Text = "Input";
            // 
            // buildHelpGroupBox
            // 
            this.buildHelpGroupBox.Controls.Add( this.showWhenFinishedCheckBox );
            this.buildHelpGroupBox.Controls.Add( this.extractQAInfoButton );
            this.buildHelpGroupBox.Controls.Add( this.generateHelpFilesButton );
            this.buildHelpGroupBox.Controls.Add( this.outputKeepIntermediateFilesCheckBox );
            this.buildHelpGroupBox.Controls.Add( this.haltOnErrorCheckBox );
            this.buildHelpGroupBox.Controls.Add( this.rebuildCheckBox );
            this.buildHelpGroupBox.Controls.Add( this.installHelpFilesButton );
            this.buildHelpGroupBox.Controls.Add( this.verboseLevelLabel );
            this.buildHelpGroupBox.Controls.Add( this.uninstallHelpFilesButton );
            this.buildHelpGroupBox.Controls.Add( this.verboseLevelNumericUpDown );
            this.buildHelpGroupBox.Location = new System.Drawing.Point( 12, 495 );
            this.buildHelpGroupBox.Name = "buildHelpGroupBox";
            this.buildHelpGroupBox.Size = new System.Drawing.Size( 850, 84 );
            this.buildHelpGroupBox.TabIndex = 9;
            this.buildHelpGroupBox.TabStop = false;
            this.buildHelpGroupBox.Text = "Build";
            // 
            // showWhenFinishedCheckBox
            // 
            this.showWhenFinishedCheckBox.AutoSize = true;
            this.showWhenFinishedCheckBox.Location = new System.Drawing.Point( 255, 20 );
            this.showWhenFinishedCheckBox.Name = "showWhenFinishedCheckBox";
            this.showWhenFinishedCheckBox.Size = new System.Drawing.Size( 127, 17 );
            this.showWhenFinishedCheckBox.TabIndex = 9;
            this.showWhenFinishedCheckBox.Text = "Show When Finished";
            this.showWhenFinishedCheckBox.UseVisualStyleBackColor = true;
            this.showWhenFinishedCheckBox.CheckedChanged += new System.EventHandler( this.EnableProjectSave_EventHandler );
            // 
            // extractQAInfoButton
            // 
            this.extractQAInfoButton.Enabled = false;
            this.extractQAInfoButton.Location = new System.Drawing.Point( 388, 49 );
            this.extractQAInfoButton.Name = "extractQAInfoButton";
            this.extractQAInfoButton.Size = new System.Drawing.Size( 225, 23 );
            this.extractQAInfoButton.TabIndex = 6;
            this.extractQAInfoButton.Text = "Extract QA Warnings";
            this.extractQAInfoButton.UseVisualStyleBackColor = true;
            this.extractQAInfoButton.Click += new System.EventHandler( this.extractQAInfoButton_Click );
            // 
            // treeNamespaceLabel
            // 
            this.treeNamespaceLabel.AutoSize = true;
            this.treeNamespaceLabel.Location = new System.Drawing.Point( 42, 49 );
            this.treeNamespaceLabel.Name = "treeNamespaceLabel";
            this.treeNamespaceLabel.Size = new System.Drawing.Size( 92, 13 );
            this.treeNamespaceLabel.TabIndex = 6;
            this.treeNamespaceLabel.Text = "Tree Namespace:";
            // 
            // treeNamespaceTextBox
            // 
            this.treeNamespaceTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeNamespaceTextBox.Location = new System.Drawing.Point( 140, 46 );
            this.treeNamespaceTextBox.Name = "treeNamespaceTextBox";
            this.treeNamespaceTextBox.Size = new System.Drawing.Size( 276, 20 );
            this.treeNamespaceTextBox.TabIndex = 7;
            this.treeNamespaceTextBox.TextChanged += new System.EventHandler( this.EnableInstallHelpFilesButton_EventHandler );
            // 
            // hxaLinkGroupLabel
            // 
            this.hxaLinkGroupLabel.AutoSize = true;
            this.hxaLinkGroupLabel.Location = new System.Drawing.Point( 50, 76 );
            this.hxaLinkGroupLabel.Name = "hxaLinkGroupLabel";
            this.hxaLinkGroupLabel.Size = new System.Drawing.Size( 84, 13 );
            this.hxaLinkGroupLabel.TabIndex = 8;
            this.hxaLinkGroupLabel.Text = "Hxa Link Group:";
            // 
            // hxaLinkGroupTextBox
            // 
            this.hxaLinkGroupTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.hxaLinkGroupTextBox.Location = new System.Drawing.Point( 140, 73 );
            this.hxaLinkGroupTextBox.Name = "hxaLinkGroupTextBox";
            this.hxaLinkGroupTextBox.Size = new System.Drawing.Size( 276, 20 );
            this.hxaLinkGroupTextBox.TabIndex = 9;
            this.hxaLinkGroupTextBox.TextChanged += new System.EventHandler( this.EnableProjectSave_EventHandler );
            // 
            // help2RegExecutableLabel
            // 
            this.help2RegExecutableLabel.AutoSize = true;
            this.help2RegExecutableLabel.Location = new System.Drawing.Point( 12, 22 );
            this.help2RegExecutableLabel.Name = "help2RegExecutableLabel";
            this.help2RegExecutableLabel.Size = new System.Drawing.Size( 122, 13 );
            this.help2RegExecutableLabel.TabIndex = 0;
            this.help2RegExecutableLabel.Text = "Registration Executable:";
            // 
            // help2RegExecutableTextBox
            // 
            this.help2RegExecutableTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.help2RegExecutableTextBox.Location = new System.Drawing.Point( 140, 19 );
            this.help2RegExecutableTextBox.Name = "help2RegExecutableTextBox";
            this.help2RegExecutableTextBox.Size = new System.Drawing.Size( 240, 20 );
            this.help2RegExecutableTextBox.TabIndex = 1;
            this.help2RegExecutableTextBox.TextChanged += new System.EventHandler( this.EnableInstallHelpFilesButton_EventHandler );
            // 
            // browseHelp2RegExecutableButton
            // 
            this.browseHelp2RegExecutableButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseHelp2RegExecutableButton.Location = new System.Drawing.Point( 386, 17 );
            this.browseHelp2RegExecutableButton.Name = "browseHelp2RegExecutableButton";
            this.browseHelp2RegExecutableButton.Size = new System.Drawing.Size( 30, 23 );
            this.browseHelp2RegExecutableButton.TabIndex = 2;
            this.browseHelp2RegExecutableButton.Text = "...";
            this.browseHelp2RegExecutableButton.UseVisualStyleBackColor = true;
            this.browseHelp2RegExecutableButton.Click += new System.EventHandler( this.browseHelp2RegExecutableButton_Click );
            // 
            // companyFilterGroupBox
            // 
            this.companyFilterGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.companyFilterGroupBox.Controls.Add( this.companyFilterAttributeValueTextBox );
            this.companyFilterGroupBox.Controls.Add( this.companyFilterAttributeValueLabel );
            this.companyFilterGroupBox.Controls.Add( this.companyFilterAttributeNameTextBox );
            this.companyFilterGroupBox.Controls.Add( this.companyFilterAttributeNameLabel );
            this.companyFilterGroupBox.Controls.Add( this.companyFilterDescriptionTextBox );
            this.companyFilterGroupBox.Controls.Add( this.companyFilterDescriptionLabel );
            this.companyFilterGroupBox.Location = new System.Drawing.Point( 6, 99 );
            this.companyFilterGroupBox.Name = "companyFilterGroupBox";
            this.companyFilterGroupBox.Size = new System.Drawing.Size( 410, 94 );
            this.companyFilterGroupBox.TabIndex = 10;
            this.companyFilterGroupBox.TabStop = false;
            this.companyFilterGroupBox.Text = "Company Filter";
            // 
            // companyFilterAttributeValueTextBox
            // 
            this.companyFilterAttributeValueTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.companyFilterAttributeValueTextBox.Location = new System.Drawing.Point( 91, 66 );
            this.companyFilterAttributeValueTextBox.Name = "companyFilterAttributeValueTextBox";
            this.companyFilterAttributeValueTextBox.Size = new System.Drawing.Size( 313, 20 );
            this.companyFilterAttributeValueTextBox.TabIndex = 5;
            this.companyFilterAttributeValueTextBox.TextChanged += new System.EventHandler( this.EnableInstallHelpFilesButton_EventHandler );
            // 
            // companyFilterAttributeValueLabel
            // 
            this.companyFilterAttributeValueLabel.AutoSize = true;
            this.companyFilterAttributeValueLabel.Location = new System.Drawing.Point( 6, 69 );
            this.companyFilterAttributeValueLabel.Name = "companyFilterAttributeValueLabel";
            this.companyFilterAttributeValueLabel.Size = new System.Drawing.Size( 79, 13 );
            this.companyFilterAttributeValueLabel.TabIndex = 4;
            this.companyFilterAttributeValueLabel.Text = "Attribute Value:";
            // 
            // companyFilterAttributeNameTextBox
            // 
            this.companyFilterAttributeNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.companyFilterAttributeNameTextBox.Location = new System.Drawing.Point( 91, 40 );
            this.companyFilterAttributeNameTextBox.Name = "companyFilterAttributeNameTextBox";
            this.companyFilterAttributeNameTextBox.Size = new System.Drawing.Size( 313, 20 );
            this.companyFilterAttributeNameTextBox.TabIndex = 3;
            this.companyFilterAttributeNameTextBox.TextChanged += new System.EventHandler( this.EnableInstallHelpFilesButton_EventHandler );
            // 
            // companyFilterAttributeNameLabel
            // 
            this.companyFilterAttributeNameLabel.AutoSize = true;
            this.companyFilterAttributeNameLabel.Location = new System.Drawing.Point( 6, 43 );
            this.companyFilterAttributeNameLabel.Name = "companyFilterAttributeNameLabel";
            this.companyFilterAttributeNameLabel.Size = new System.Drawing.Size( 80, 13 );
            this.companyFilterAttributeNameLabel.TabIndex = 2;
            this.companyFilterAttributeNameLabel.Text = "Attribute Name:";
            // 
            // companyFilterDescriptionTextBox
            // 
            this.companyFilterDescriptionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.companyFilterDescriptionTextBox.Location = new System.Drawing.Point( 91, 14 );
            this.companyFilterDescriptionTextBox.Name = "companyFilterDescriptionTextBox";
            this.companyFilterDescriptionTextBox.Size = new System.Drawing.Size( 313, 20 );
            this.companyFilterDescriptionTextBox.TabIndex = 1;
            this.companyFilterDescriptionTextBox.TextChanged += new System.EventHandler( this.EnableInstallHelpFilesButton_EventHandler );
            // 
            // companyFilterDescriptionLabel
            // 
            this.companyFilterDescriptionLabel.AutoSize = true;
            this.companyFilterDescriptionLabel.Location = new System.Drawing.Point( 22, 17 );
            this.companyFilterDescriptionLabel.Name = "companyFilterDescriptionLabel";
            this.companyFilterDescriptionLabel.Size = new System.Drawing.Size( 63, 13 );
            this.companyFilterDescriptionLabel.TabIndex = 0;
            this.companyFilterDescriptionLabel.Text = "Description:";
            // 
            // installationInfoGroupBox
            // 
            this.installationInfoGroupBox.Controls.Add( this.projectFilterGroupBox );
            this.installationInfoGroupBox.Controls.Add( this.companyFilterGroupBox );
            this.installationInfoGroupBox.Controls.Add( this.browseHelp2RegExecutableButton );
            this.installationInfoGroupBox.Controls.Add( this.help2RegExecutableTextBox );
            this.installationInfoGroupBox.Controls.Add( this.help2RegExecutableLabel );
            this.installationInfoGroupBox.Controls.Add( this.hxaLinkGroupTextBox );
            this.installationInfoGroupBox.Controls.Add( this.hxaLinkGroupLabel );
            this.installationInfoGroupBox.Controls.Add( this.treeNamespaceTextBox );
            this.installationInfoGroupBox.Controls.Add( this.treeNamespaceLabel );
            this.installationInfoGroupBox.Enabled = false;
            this.installationInfoGroupBox.Location = new System.Drawing.Point( 12, 159 );
            this.installationInfoGroupBox.Name = "installationInfoGroupBox";
            this.installationInfoGroupBox.Size = new System.Drawing.Size( 422, 330 );
            this.installationInfoGroupBox.TabIndex = 4;
            this.installationInfoGroupBox.TabStop = false;
            this.installationInfoGroupBox.Text = "Help 2 Installation Info";
            // 
            // projectFilterGroupBox
            // 
            this.projectFilterGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.projectFilterGroupBox.Controls.Add( this.projectFilterAttributeValueTextBox );
            this.projectFilterGroupBox.Controls.Add( this.projectFilterAttributeValueLabel );
            this.projectFilterGroupBox.Controls.Add( this.projectFilterAttributeNameTextBox );
            this.projectFilterGroupBox.Controls.Add( this.projectFilterAttributeNameLabel );
            this.projectFilterGroupBox.Controls.Add( this.projectFilterDescriptionTextBox );
            this.projectFilterGroupBox.Controls.Add( this.projectFilterDescriptionLabel );
            this.projectFilterGroupBox.Location = new System.Drawing.Point( 6, 201 );
            this.projectFilterGroupBox.Name = "projectFilterGroupBox";
            this.projectFilterGroupBox.Size = new System.Drawing.Size( 410, 94 );
            this.projectFilterGroupBox.TabIndex = 11;
            this.projectFilterGroupBox.TabStop = false;
            this.projectFilterGroupBox.Text = "Project Filter";
            // 
            // projectFilterAttributeValueTextBox
            // 
            this.projectFilterAttributeValueTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.projectFilterAttributeValueTextBox.Location = new System.Drawing.Point( 91, 66 );
            this.projectFilterAttributeValueTextBox.Name = "projectFilterAttributeValueTextBox";
            this.projectFilterAttributeValueTextBox.Size = new System.Drawing.Size( 313, 20 );
            this.projectFilterAttributeValueTextBox.TabIndex = 5;
            this.projectFilterAttributeValueTextBox.TextChanged += new System.EventHandler( this.EnableInstallHelpFilesButton_EventHandler );
            // 
            // projectFilterAttributeValueLabel
            // 
            this.projectFilterAttributeValueLabel.AutoSize = true;
            this.projectFilterAttributeValueLabel.Location = new System.Drawing.Point( 6, 69 );
            this.projectFilterAttributeValueLabel.Name = "projectFilterAttributeValueLabel";
            this.projectFilterAttributeValueLabel.Size = new System.Drawing.Size( 79, 13 );
            this.projectFilterAttributeValueLabel.TabIndex = 4;
            this.projectFilterAttributeValueLabel.Text = "Attribute Value:";
            // 
            // projectFilterAttributeNameTextBox
            // 
            this.projectFilterAttributeNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.projectFilterAttributeNameTextBox.Location = new System.Drawing.Point( 91, 40 );
            this.projectFilterAttributeNameTextBox.Name = "projectFilterAttributeNameTextBox";
            this.projectFilterAttributeNameTextBox.Size = new System.Drawing.Size( 313, 20 );
            this.projectFilterAttributeNameTextBox.TabIndex = 3;
            this.projectFilterAttributeNameTextBox.TextChanged += new System.EventHandler( this.EnableInstallHelpFilesButton_EventHandler );
            // 
            // projectFilterAttributeNameLabel
            // 
            this.projectFilterAttributeNameLabel.AutoSize = true;
            this.projectFilterAttributeNameLabel.Location = new System.Drawing.Point( 6, 43 );
            this.projectFilterAttributeNameLabel.Name = "projectFilterAttributeNameLabel";
            this.projectFilterAttributeNameLabel.Size = new System.Drawing.Size( 80, 13 );
            this.projectFilterAttributeNameLabel.TabIndex = 2;
            this.projectFilterAttributeNameLabel.Text = "Attribute Name:";
            // 
            // projectFilterDescriptionTextBox
            // 
            this.projectFilterDescriptionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.projectFilterDescriptionTextBox.Location = new System.Drawing.Point( 91, 14 );
            this.projectFilterDescriptionTextBox.Name = "projectFilterDescriptionTextBox";
            this.projectFilterDescriptionTextBox.Size = new System.Drawing.Size( 313, 20 );
            this.projectFilterDescriptionTextBox.TabIndex = 1;
            this.projectFilterDescriptionTextBox.TextChanged += new System.EventHandler( this.EnableInstallHelpFilesButton_EventHandler );
            // 
            // projectFilterDescriptionLabel
            // 
            this.projectFilterDescriptionLabel.AutoSize = true;
            this.projectFilterDescriptionLabel.Location = new System.Drawing.Point( 22, 17 );
            this.projectFilterDescriptionLabel.Name = "projectFilterDescriptionLabel";
            this.projectFilterDescriptionLabel.Size = new System.Drawing.Size( 63, 13 );
            this.projectFilterDescriptionLabel.TabIndex = 0;
            this.projectFilterDescriptionLabel.Text = "Description:";
            // 
            // sourceControlProviderControl
            // 
            this.sourceControlProviderControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.sourceControlProviderControl.Enabled = false;
            this.sourceControlProviderControl.Location = new System.Drawing.Point( 6, 42 );
            this.sourceControlProviderControl.Name = "sourceControlProviderControl";
            this.sourceControlProviderControl.Size = new System.Drawing.Size( 409, 107 );
            this.sourceControlProviderControl.TabIndex = 1;
            this.sourceControlProviderControl.SettingChanged += new System.EventHandler( this.sourceControlProviderControl_SettingChanged );
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 874, 585 );
            this.Controls.Add( this.buildHelpGroupBox );
            this.Controls.Add( this.inputGroupBox );
            this.Controls.Add( this.outputGroupBox );
            this.Controls.Add( this.sourceControlGroupBox );
            this.Controls.Add( this.installationInfoGroupBox );
            this.Controls.Add( this.templateFilesGroupBox );
            this.Controls.Add( this.menuStrip1 );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject( "$this.Icon" )));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Rage Help Generator";
            this.Load += new System.EventHandler( this.Form1_Load );
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.Form1_FormClosing );
            this.menuStrip1.ResumeLayout( false );
            this.menuStrip1.PerformLayout();
            this.outputFormatsGroupBox.ResumeLayout( false );
            this.outputFormatsGroupBox.PerformLayout();
            this.templateFilesGroupBox.ResumeLayout( false );
            this.templateFilesGroupBox.PerformLayout();
            this.sourceControlGroupBox.ResumeLayout( false );
            this.sourceControlGroupBox.PerformLayout();
            this.outputGroupBox.ResumeLayout( false );
            this.outputGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.verboseLevelNumericUpDown)).EndInit();
            this.inputGroupBox.ResumeLayout( false );
            this.inputGroupBox.PerformLayout();
            this.buildHelpGroupBox.ResumeLayout( false );
            this.buildHelpGroupBox.PerformLayout();
            this.companyFilterGroupBox.ResumeLayout( false );
            this.companyFilterGroupBox.PerformLayout();
            this.installationInfoGroupBox.ResumeLayout( false );
            this.installationInfoGroupBox.PerformLayout();
            this.projectFilterGroupBox.ResumeLayout( false );
            this.projectFilterGroupBox.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.GroupBox outputFormatsGroupBox;
        private System.Windows.Forms.GroupBox templateFilesGroupBox;
        private System.Windows.Forms.GroupBox sourceControlGroupBox;
        private System.Windows.Forms.CheckBox xmlFormatCheckBox;
        private System.Windows.Forms.CheckBox help2FormatCheckBox;
        private System.Windows.Forms.CheckBox htmlHelpFormatCheckBox;
        private System.Windows.Forms.CheckBox htmlFormatCheckBox;
        private System.Windows.Forms.CheckBox windowsHelpFormatCheckBox;
        private System.Windows.Forms.CheckBox pdfFormatCheckBox;
        private System.Windows.Forms.Button browseOverviewDoxFileButton;
        private System.Windows.Forms.TextBox overviewDoxFileTextBox;
        private System.Windows.Forms.Label overviewDoxLabel;
        private System.Windows.Forms.Button browseDoxFileButton;
        private System.Windows.Forms.TextBox doxFileTextBox;
        private System.Windows.Forms.Label doxFileLabel;
        private System.Windows.Forms.Button browseDtxFileButton;
        private System.Windows.Forms.TextBox dtxFileTextBox;
        private System.Windows.Forms.Label dtxFileLabel;
        private System.Windows.Forms.CheckBox sourceControlEnabledCheckBox;
        private System.Windows.Forms.GroupBox outputGroupBox;
        private System.Windows.Forms.Button browseOutputSourceDirectoriesButton;
        private System.Windows.Forms.TextBox outputSourceDirectoriesTextBox;
        private System.Windows.Forms.Label outputSourceDirectoriesLabel;
        private System.Windows.Forms.CheckBox outputKeepIntermediateFilesCheckBox;
        private System.Windows.Forms.Button generateHelpFilesButton;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button browseHelp2QaExecutableButton;
        private System.Windows.Forms.TextBox help2QaExecutableTextBox;
        private System.Windows.Forms.CheckBox haltOnErrorCheckBox;
        private System.Windows.Forms.Button uninstallHelpFilesButton;
        private System.Windows.Forms.Button installHelpFilesButton;
        private System.Windows.Forms.CheckBox outputOverviewHelpFilesCheckBox;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog;
        private System.Windows.Forms.PrintDialog printDialog;
        private System.Drawing.Printing.PrintDocument printDocument;
        private System.Windows.Forms.Label verboseLevelLabel;
        private System.Windows.Forms.NumericUpDown verboseLevelNumericUpDown;
        private System.Windows.Forms.CheckBox rebuildCheckBox;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileNewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileOpenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem fileSaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileSaveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem filePrintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filePrintPreviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem fileExitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsReloadLastFileAtStartupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsClearRecentFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpContentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem helpAboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem fileRecentFilesToolStripMenuItem;
        private System.Windows.Forms.TextBox outputSourceDirectoryTreesTextBox;
        private System.Windows.Forms.Label outputSourceDirectoryTreesLabel;
        private System.Windows.Forms.Button browseOutputDirectoryButton;
        private System.Windows.Forms.TextBox outputDirectoryTextBox;
        private System.Windows.Forms.CheckBox overrideOutputDirectoryCheckBox;
        private System.Windows.Forms.Button browseMasterDoxFileButton;
        private System.Windows.Forms.TextBox masterDoxFileTextBox;
        private System.Windows.Forms.Label masterDoxFileLabel;
        private System.Windows.Forms.GroupBox inputGroupBox;
        private System.Windows.Forms.GroupBox buildHelpGroupBox;
        private System.Windows.Forms.CheckBox unifyHelpFilesCheckBox;
        private System.Windows.Forms.Button extractQAInfoButton;
        private System.Windows.Forms.Label qaWarningsExecutableLabel;
        private System.Windows.Forms.TextBox unifiedHelpFileNameTextBox;
        private System.Windows.Forms.Label treeNamespaceLabel;
        private System.Windows.Forms.TextBox treeNamespaceTextBox;
        private System.Windows.Forms.Label hxaLinkGroupLabel;
        private System.Windows.Forms.TextBox hxaLinkGroupTextBox;
        private System.Windows.Forms.Label help2RegExecutableLabel;
        private System.Windows.Forms.TextBox help2RegExecutableTextBox;
        private System.Windows.Forms.Button browseHelp2RegExecutableButton;
        private System.Windows.Forms.GroupBox companyFilterGroupBox;
        private System.Windows.Forms.TextBox companyFilterAttributeValueTextBox;
        private System.Windows.Forms.Label companyFilterAttributeValueLabel;
        private System.Windows.Forms.TextBox companyFilterAttributeNameTextBox;
        private System.Windows.Forms.Label companyFilterAttributeNameLabel;
        private System.Windows.Forms.TextBox companyFilterDescriptionTextBox;
        private System.Windows.Forms.Label companyFilterDescriptionLabel;
        private System.Windows.Forms.GroupBox installationInfoGroupBox;
        private System.Windows.Forms.GroupBox projectFilterGroupBox;
        private System.Windows.Forms.TextBox projectFilterAttributeValueTextBox;
        private System.Windows.Forms.Label projectFilterAttributeValueLabel;
        private System.Windows.Forms.TextBox projectFilterAttributeNameTextBox;
        private System.Windows.Forms.Label projectFilterAttributeNameLabel;
        private System.Windows.Forms.TextBox projectFilterDescriptionTextBox;
        private System.Windows.Forms.Label projectFilterDescriptionLabel;
        private System.Windows.Forms.CheckBox showWhenFinishedCheckBox;
        private RSG.Base.Forms.SourceControlProviderControl sourceControlProviderControl;
    }
}

