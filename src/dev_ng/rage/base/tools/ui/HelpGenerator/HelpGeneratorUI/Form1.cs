using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using HelpGeneratorUI.Properties;
using HelpGeneratorCore;
using RSG.Base.Command;
using RSG.Base.Drawing;
using RSG.Base.SCM;

namespace HelpGeneratorUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            InitializeAdditionalComponents();
        }

        #region Initialization
        private void InitializeAdditionalComponents()
        {
            this.printPreviewDialog.ClientSize = new Size( 800, 800 );

            SetUIFromProject( m_projectSettings );

            // start off unmodified
            this.ProjectIsModified = false;
        }
        #endregion

        #region Variables
        private ProjectSettings m_projectSettings = new ProjectSettings();
        private bool m_projectIsModified = false;

        private string m_printTempFilename;
        private TextReader m_printTextReader;
        #endregion

        #region Properties
        private bool ProjectIsModified
        {
            get
            {
                return m_projectIsModified;
            }
            set
            {
                m_projectIsModified = value;

                this.fileSaveToolStripMenuItem.Enabled = value;
            }
        }
        #endregion

        #region Event Handlers

        private void Form1_Load( object sender, EventArgs e )
        {
            LoadLayout();
        }

        private void Form1_FormClosing( object sender, FormClosingEventArgs e )
        {
            // save for the kluge
            string projectFilename = m_projectSettings.ProjectFilename;

            if ( !CloseProjectFile( false ) )
            {
                e.Cancel = true;
                return;
            }

            // kluge
            m_projectSettings.ProjectFilename = projectFilename;

            SaveLayout();
        }

        #region PrintDocument
        private void printDocument_BeginPrint( object sender, System.Drawing.Printing.PrintEventArgs e )
        {
            m_printTempFilename = Path.GetTempFileName();

            SetProjectFromUI( m_projectSettings );

            if ( !m_projectSettings.SaveFile( m_printTempFilename ) )
            {
                e.Cancel = true;
            }

            m_printTextReader = new StreamReader( m_printTempFilename );
        }

        private void printDocument_EndPrint( object sender, System.Drawing.Printing.PrintEventArgs e )
        {
            m_printTextReader.Close();
            
            File.Delete( m_printTempFilename );

            m_printTempFilename = null;
            m_printTextReader = null;
        }

        private void printDocument_PrintPage( object sender, System.Drawing.Printing.PrintPageEventArgs e )
        {
            RectangleF layout_rect;
            int ymin = e.MarginBounds.Top;
            int xmin = e.MarginBounds.Left;
            
            Font the_font = new Font( "Arial", 10, FontStyle.Regular, GraphicsUnit.Point );
            
            StringFormat string_format = new StringFormat( StringFormatFlags.LineLimit );
            string_format.Alignment = StringAlignment.Near;
            string_format.Trimming = StringTrimming.Word;

            int characters_fitted;
            int lines_filled;

            string line = m_printTextReader.ReadLine();
            while ( line != null )
            {
                // Get the area available for this text.
                layout_rect = new RectangleF( xmin, ymin, e.MarginBounds.Right - xmin, the_font.Height );

                // If the layout rectangle's height < 1, make it 1.
                if ( layout_rect.Height < 1 )
                {
                    layout_rect.Height = 1;
                }

                int startIndex = 0;
                while ( startIndex < line.Length )
                {
                    // See how big the text will be and how many characters will fit.
                    string subLine = line.Substring( startIndex );
                    SizeF text_size = e.Graphics.MeasureString( subLine, the_font, 
                        new SizeF( layout_rect.Width, layout_rect.Height + 1 ), string_format, out characters_fitted, out lines_filled );

                    // print the characters
                    if ( characters_fitted > 0 )
                    {
                        e.Graphics.DrawString( line.Substring( startIndex, characters_fitted ), the_font, Brushes.Black,
                            new PointF( xmin, ymin ), string_format );

                        ymin += (int)(layout_rect.Height + 1);
                        startIndex += characters_fitted;
                    }
                    else
                    {
                        break;
                    }
                }

                if ( ymin + the_font.Height > e.MarginBounds.Bottom )
                {
                    break;
                }

                line = m_printTextReader.ReadLine();
            }

            e.HasMorePages = line != null;
        }
        #endregion

        #region Menus

        #region File Menu
        private void fileNewToolStripMenuItem_Click( object sender, EventArgs e )
        {
            CloseProjectFile( false );

            this.ProjectIsModified = true;
        }

        private void fileOpenToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Title = "Open HelpGeneratorUI File";
            this.openFileDialog.Filter = "Xml Files (*.xml)|*.xml";
            if ( this.openFileDialog.ShowDialog( this ) == DialogResult.OK )
            {
                LoadProjectFile( this.openFileDialog.FileName );
            }
        }

        private void fileSaveToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( !m_projectSettings.IsLoaded )
            {
                SaveProjectFileAs();
            }
            else
            {
                SaveProjectFile();
            }
        }

        private void fileSaveAsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            SaveProjectFileAs();
        }

        private void filePrintToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.printDialog.ShowDialog( this );
        }

        private void filePrintPreviewToolStripMenuItem_Click( object sender, EventArgs e )
        {            
            CenterLocation( this, this.printPreviewDialog );
            this.printPreviewDialog.ShowDialog( this );
        }

        private void fileExitToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.Close();
        }
        #endregion

        #region Tools Menu
        private void toolsClearRecentFilesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.fileRecentFilesToolStripMenuItem.DropDownItems.Clear();
        }
        #endregion

        #region Help Menu
        private void helpContentsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            string docDir = ProjectSettings.FindDocDirectory( Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ) );
            if ( docDir != null )
            {
                string filename = Path.Combine( docDir, "chm\\overview_helpgenerator.chm" );
                if ( File.Exists( filename ) )
                {
                    Process.Start( filename );
                }
            }
        }

        private void helpAboutToolStripMenuItem_Click( object sender, EventArgs e )
        {
            string versionStr = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            MessageBox.Show( this, "Rage Help Generator written by Adam Dickinson, Rockstar San Diego 2007\nVersion " + versionStr, "About..." );
        }
        #endregion

        #endregion

        #region Shared
        private void fileRecentFilesItemToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            LoadProjectFile( item.Text );
        }

        private void EnableGenerateHelpFilesButton_EventHandler( object sender, EventArgs e )
        {
            // use a temporary so that SetProjectFromUI doesn't blow away our file as we're loading it
            ProjectSettings settings = new ProjectSettings();
            settings.ProjectFilename = m_projectSettings.ProjectFilename;
            
            SetProjectFromUI( settings );

            this.generateHelpFilesButton.Enabled = settings.CanGenerateHelp;
            this.extractQAInfoButton.Enabled = settings.CanExtractQAWarnings;

            this.ProjectIsModified = true;
        }

        private void EnableInstallHelpFilesButton_EventHandler( object sender, EventArgs e )
        {
            // use a temporary so that SetProjectFromUI doesn't blow away our file as we're loading it
            ProjectSettings settings = new ProjectSettings();
            settings.ProjectFilename = m_projectSettings.ProjectFilename;
            
            SetProjectFromUI( settings );

            this.installHelpFilesButton.Enabled = this.uninstallHelpFilesButton.Enabled = settings.CanInstallHelp;
            this.extractQAInfoButton.Enabled = settings.CanExtractQAWarnings;

            this.ProjectIsModified = true;
        }

        private void EnableProjectSave_EventHandler( object sender, EventArgs e )
        {
            this.ProjectIsModified = true;
        }
        #endregion

        #region Templates
        private void browseDtxFileButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Title = "Select Documentation Template File";
            this.openFileDialog.Filter = "Documentation File (*.dtx)|*.dtx";
            if ( this.openFileDialog.ShowDialog( this ) == DialogResult.OK )
            {
                this.dtxFileTextBox.Text = this.openFileDialog.FileName;
            }
        }

        private void browseDoxFileButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Title = "Select Doc-O-Matic Project Template File";
            this.openFileDialog.Filter = "Doc-O-Matic Projects (*.dox)|*.dox";
            if ( this.openFileDialog.ShowDialog( this ) == DialogResult.OK )
            {
                this.doxFileTextBox.Text = this.openFileDialog.FileName;
            }
        }

        private void browseOverviewDoxFileButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Title = "Select Doc-O-Matic Project Template File for Overviews";
            this.openFileDialog.Filter = "Doc-O-Matic Projects (*.dox)|*.dox";
            if ( this.openFileDialog.ShowDialog( this ) == DialogResult.OK )
            {
                this.overviewDoxFileTextBox.Text = this.openFileDialog.FileName;
            }
        }

        private void browseMasterDoxFileButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Title = "Select Master Doc-O-Matic Project Template File for Unified Help";
            this.openFileDialog.Filter = "Doc-O-Matic Projects (*.dox)|*.dox";
            if ( this.openFileDialog.ShowDialog( this ) == DialogResult.OK )
            {
                this.masterDoxFileTextBox.Text = this.openFileDialog.FileName;
            }
        }
        #endregion

        #region Installation Info
        private void browseHelp2RegExecutableButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Title = "Select Registration Executable File";
            this.openFileDialog.Filter = "Executable Files (*.exe)|*.exe";
            if ( this.openFileDialog.ShowDialog( this ) == DialogResult.OK )
            {
                this.help2RegExecutableTextBox.Text = this.openFileDialog.FileName;
            }
        }

        private void hxaAttributesDataGridView_CellValueChanged( object sender, DataGridViewCellEventArgs e )
        {
            EnableProjectSave_EventHandler( sender, new EventArgs() );

            EnableInstallHelpFilesButton_EventHandler( sender, new EventArgs() );
        }
        #endregion

        #region Source Control
        private void sourceControlEnabledCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.sourceControlProviderControl.Enabled = this.sourceControlEnabledCheckBox.Checked;

            this.ProjectIsModified = true;
        }

        private void sourceControlProviderControl_SettingChanged( object sender, EventArgs e )
        {
            this.ProjectIsModified = true;
        }
        #endregion

        #region Input
        private void outputSourceDirectoriesTextBox_TextChanged( object sender, EventArgs e )
        {
            EnableGenerateHelpFilesButton_EventHandler( sender, e );

            EnableInstallHelpFilesButton_EventHandler( sender, e );
        }

        private void browseOutputSourceDirectoriesButton_Click( object sender, EventArgs e )
        {
            this.folderBrowserDialog.Description = "Add Directory to Process";
            this.folderBrowserDialog.ShowNewFolderButton = false;
            if ( this.folderBrowserDialog.ShowDialog( this ) == DialogResult.OK )
            {
                string text = this.outputSourceDirectoriesTextBox.Text;
                if ( (text.Length > 0) && (text[text.Length - 1] != Path.PathSeparator) )
                {
                    text += Path.PathSeparator;
                }

                text += this.folderBrowserDialog.SelectedPath;

                this.outputSourceDirectoriesTextBox.Text = text;
            }
        }
        #endregion

        #region Output
        private void help2FormatCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.installationInfoGroupBox.Enabled = this.help2FormatCheckBox.Checked;

            EnableGenerateHelpFilesButton_EventHandler( sender, e );
            EnableInstallHelpFilesButton_EventHandler( sender, e );
        }

        private void overrideOutputDirectoryCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.outputDirectoryTextBox.Enabled = this.browseOutputDirectoryButton.Enabled = this.overrideOutputDirectoryCheckBox.Checked;

            EnableGenerateHelpFilesButton_EventHandler( sender, e );
        }

        private void outputDirectoryTextBox_TextChanged( object sender, EventArgs e )
        {
            EnableGenerateHelpFilesButton_EventHandler( sender, e );

            EnableInstallHelpFilesButton_EventHandler( sender, e );
        }

        private void browseOutputDirectoryButton_Click( object sender, EventArgs e )
        {
            this.folderBrowserDialog.Description = "Select Directory for Output Files";
            this.folderBrowserDialog.ShowNewFolderButton = true;
            if ( this.folderBrowserDialog.ShowDialog( this ) == DialogResult.OK )
            {
                this.outputDirectoryTextBox.Text = this.folderBrowserDialog.SelectedPath;
            }
        }

        private void unifyHelpFilesCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.masterDoxFileTextBox.Enabled = this.browseMasterDoxFileButton.Enabled 
                = this.unifiedHelpFileNameTextBox.Enabled = this.unifyHelpFilesCheckBox.Checked && this.unifyHelpFilesCheckBox.Enabled;

            if ( this.unifyHelpFilesCheckBox.Enabled )
            {
                this.doxFileTextBox.Enabled = this.browseDoxFileButton.Enabled
                    = this.overviewDoxFileTextBox.Enabled = this.browseOverviewDoxFileButton.Enabled
                    = !this.unifyHelpFilesCheckBox.Checked;
            }

            EnableGenerateHelpFilesButton_EventHandler( sender, e );

            EnableInstallHelpFilesButton_EventHandler( sender, e );
        }

        private void unifiedHelpFileNameTextBox_TextChanged( object sender, EventArgs e )
        {
            EnableGenerateHelpFilesButton_EventHandler( sender, e );

            EnableInstallHelpFilesButton_EventHandler( sender, e );
        }

        private void browseHelp2QaExecutableButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Title = "Select QA Executable File";
            this.openFileDialog.Filter = "Executables (*.exe)|*.exe";
            if ( this.openFileDialog.ShowDialog( this ) == DialogResult.OK )
            {
                this.help2QaExecutableTextBox.Text = this.openFileDialog.FileName;
            }
        }
        
        private void outputOverviewHelpFilesCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.overviewDoxFileTextBox.Enabled
                = this.browseOverviewDoxFileButton.Enabled
                = this.outputOverviewHelpFilesCheckBox.Checked;

            EnableGenerateHelpFilesButton_EventHandler( sender, e );
        }
        #endregion

        #region Generate
        private void verboseLevelNumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            this.ProjectIsModified = true;
        }

        private void generateHelpFilesButton_Click( object sender, EventArgs e )
        {
            SetProjectFromUI( m_projectSettings );

            rageStatus status;
            m_projectSettings.SourceControlProviderSettings.Validate( out status );
            if ( !status.Success() )
            {
                MessageBox.Show( this, status.ErrorString, "Invalid Source Control Settings", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
                return;
            }

            Generator generator = new Generator( m_projectSettings );

            OutputForm form = new OutputForm( generator, OutputForm.Action.Generate );

            form.ShowDialog( this );
        }

        private void installHelpFilesButton_Click( object sender, EventArgs e )
        {
            SetProjectFromUI( m_projectSettings );

            rageStatus status;
            m_projectSettings.SourceControlProviderSettings.Validate( out status );
            if ( !status.Success() )
            {
                MessageBox.Show( this, status.ErrorString, "Invalid Source Control Settings", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
                return;
            }

            Generator generator = new Generator( m_projectSettings );

            OutputForm form = new OutputForm( generator, OutputForm.Action.Install );

            form.ShowDialog( this );
        }

        private void uninstallHelpFilesButton_Click( object sender, EventArgs e )
        {
            SetProjectFromUI( m_projectSettings );

            rageStatus status;
            m_projectSettings.SourceControlProviderSettings.Validate( out status );
            if ( !status.Success() )
            {
                MessageBox.Show( this, status.ErrorString, "Invalid Source Control Settings", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
                return;
            }

            Generator generator = new Generator( m_projectSettings );

            OutputForm form = new OutputForm( generator, OutputForm.Action.Uninstall );

            form.ShowDialog( this );
        }

        private void extractQAInfoButton_Click( object sender, EventArgs e )
        {
            SetProjectFromUI( m_projectSettings );

            rageStatus status;
            m_projectSettings.SourceControlProviderSettings.Validate( out status );
            if ( !status.Success() )
            {
                MessageBox.Show( this, status.ErrorString, "Invalid Source Control Settings", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
                return;
            }

            Generator generator = new Generator( m_projectSettings );

            OutputForm form = new OutputForm( generator, OutputForm.Action.ExtractQAWarnings );

            form.ShowDialog( this );
        }
        #endregion

        #endregion

        #region Public Functions
        public bool LoadProjectFile( string filename )
        {
            if ( m_projectSettings.IsLoaded )
            {
                CloseProjectFile( false );
            }

            if ( !File.Exists( filename ) )
            {
                MessageBox.Show( this, "Unable to load '" + filename + "'.", "File Not Found", 
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
                RemoveRecentFile( filename );
                return false;
            }

            if ( !m_projectSettings.LoadFile( filename ) )
            {
                MessageBox.Show( this, "Unable to load '" + filename + "'.", "File Not Loaded",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
                RemoveRecentFile( filename );
                return false;
            }

            this.Text = filename + " - Rage Help Generator";

            SetUIFromProject( m_projectSettings );

            this.ProjectIsModified = false;

            return true;
        }

        public void ReloadProjectFile()
        {
            string filename = m_projectSettings.ProjectFilename;

            CloseProjectFile( true );

            LoadProjectFile( filename );
        }

        public bool CloseProjectFile( bool discardChanges )
        {
            if ( !discardChanges && this.ProjectIsModified )
            {
                DialogResult result = DialogResult.No;
                if ( m_projectSettings.IsLoaded )
                {
                    // save modified project
                    result = MessageBox.Show( this, m_projectSettings.ProjectFilename + "\n\n\nThis file has been modified. Do you wish to save it before closing?",
                       "File Modified", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning );
                }
                else
                {
                    result = MessageBox.Show( this, "You have unsaved changes.  Would you like to save them to a file?", "Unsaved File",
                        MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning );
                }

                if ( result == DialogResult.Yes )
                {
                    if ( m_projectSettings.IsLoaded )
                    {
                        SaveProjectFile();
                    }
                    else
                    {
                        SaveProjectFileAs();
                    }
                }

                if ( result == DialogResult.Cancel )
                {
                    return false;
                }
            }

            if ( m_projectSettings.IsLoaded )
            {
                AddRecentFile( m_projectSettings.ProjectFilename );
            }

            m_projectSettings.Clear();

            SetUIFromProject( m_projectSettings );

            this.Text = "Rage Help Generator";

            this.ProjectIsModified = false;

            return true;
        }

        public bool SaveProjectFile()
        {
            if ( !this.ProjectIsModified )
            {
                return true;
            }

            SetProjectFromUI( m_projectSettings );

            rageStatus status;
            m_projectSettings.SourceControlProviderSettings.Validate( out status );
            if ( !status.Success() )
            {
                MessageBox.Show( this, "Unable to save '" + m_projectSettings.ProjectFilename + "'.\n\n" + status.ErrorString, "Save Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
                return false;
            }

            bool success = true;
            if ( File.Exists( m_projectSettings.ProjectFilename ) )
            {
                m_projectSettings.SourceControlProviderSettings.CheckOut( m_projectSettings.ProjectFilename, out status );
            }

            if ( status.Success() )
            {
                if ( m_projectSettings.SaveFile() )
                {
                    this.ProjectIsModified = false;
                }
                else
                {
                    MessageBox.Show( this, "Unable to save '" + m_projectSettings.ProjectFilename + "'.", "Save Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
                    success = false;
                }
            }
            else
            {
                MessageBox.Show( this, status.ErrorString, "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );

                success = false;
            }

            return success;
        }

        public bool SaveProjectFileAs()
        {
            if ( this.saveFileDialog.ShowDialog( this ) != DialogResult.OK )
            {
                return false;
            }

            string oldFilename = m_projectSettings.ProjectFilename;

            // change the name for path-relative file info
            m_projectSettings.ProjectFilename = this.saveFileDialog.FileName;

            // make sure we're marked to be saved
            this.ProjectIsModified = true;
            
            if ( !SaveProjectFile() )
            {
                // restore the old filename on failure
                m_projectSettings.ProjectFilename = oldFilename;
                return false;
            }

            this.Text = m_projectSettings.ProjectFilename + " - Rage Help Generator";

            return true;
        }
        #endregion

        #region Private Functions
        private void SaveLayout()
        {
            // set this so we know when we can call Upgrade()
            Settings.Default.UpgradeNeeded = "no";

            // Screen resolution
            Settings.Default.ScreenSize = Screen.GetWorkingArea( this ).Size;

            // Form
            Settings.Default.FormLocation = this.Location;
            Settings.Default.FormWindowState = this.WindowState;

            // Last File
            Settings.Default.LoadLastFile = this.toolsReloadLastFileAtStartupToolStripMenuItem.Checked;
            if ( Settings.Default.LoadLastFile )
            {
                if ( m_projectSettings.IsLoaded )
                {
                    Settings.Default.LastFileLoaded = m_projectSettings.ProjectFilename;
                }
                else
                {
                    Settings.Default.LastFileLoaded = string.Empty;
                }
            }

            // Recent Files (collect in reverse order because of how AddRecentFile works)
            System.Collections.Specialized.StringCollection recentFiles = new System.Collections.Specialized.StringCollection();
            for ( int i = this.fileRecentFilesToolStripMenuItem.DropDownItems.Count - 1; i >= 0; --i )
            {
                recentFiles.Add( this.fileRecentFilesToolStripMenuItem.DropDownItems[i].Text );
            }
            Settings.Default.RecentFiles = recentFiles;

            // now save it
            int retryCount = 10;
            do
            {
                try
                {
                    Settings.Default.Save();
                    break;
                }
                catch
                {
                    --retryCount;
                    System.Threading.Thread.Sleep( 100 );
                }
            }
            while ( retryCount > 0 );
        }

        private void LoadLayout()
        {
            // ensure the window handle has been created before setting the window position
            int handle = (int)this.Handle;

            // decide when to upgrade
            if ( Settings.Default.UpgradeNeeded == string.Empty )
            {
                Settings.Default.Upgrade();
            }

            try
            {
                // Form
                Point loc = Settings.Default.FormLocation;
                Size size = this.Size;
                rageImage.AdjustForResolution( Settings.Default.ScreenSize, Screen.GetWorkingArea( this ), ref loc, ref size );

                this.Location = loc;
                this.WindowState = Settings.Default.FormWindowState;

                // Last File (if we didn't load one off the command line)
                if ( Settings.Default.LoadLastFile && !m_projectSettings.IsLoaded )
                {
                    string filename = Settings.Default.LastFileLoaded;
                    if ( filename != string.Empty )
                    {
                        LoadProjectFile( filename );
                    }
                }

                // Recent Files
                System.Collections.Specialized.StringCollection recentFiles = Settings.Default.RecentFiles;
                if ( recentFiles != null )
                {
                    foreach ( string recentFile in recentFiles )
                    {
                        AddRecentFile( recentFile );
                    }
                }
            }
            catch ( Exception e )
            {
                Console.WriteLine( e.Message );
            }
        }

        private void SetUIFromProject( ProjectSettings settings )
        {
            this.outputFormatsGroupBox.SuspendLayout();
            this.templateFilesGroupBox.SuspendLayout();
            this.installationInfoGroupBox.SuspendLayout();
            this.sourceControlGroupBox.SuspendLayout();
            this.outputGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.verboseLevelNumericUpDown)).BeginInit();
            this.inputGroupBox.SuspendLayout();
            this.buildHelpGroupBox.SuspendLayout();

            this.companyFilterDescriptionTextBox.Text = settings.CompanyDescription;
            this.doxFileTextBox.Text = settings.DoxTemplateFile;
            this.dtxFileTextBox.Text = settings.DtxTemplateFile;
            this.haltOnErrorCheckBox.Checked = settings.HaltOnError;
            this.help2RegExecutableTextBox.Text = settings.Help2RegExecutableFile;
            this.companyFilterAttributeNameTextBox.Text = settings.HxaAttributeCompanyName;
            this.companyFilterAttributeValueTextBox.Text = settings.HxaAttributeCompanyValue;
            this.projectFilterAttributeNameTextBox.Text = settings.HxaAttributeProjectName;
            this.projectFilterAttributeValueTextBox.Text = settings.HxaAttributeProjectValue;
            this.hxaLinkGroupTextBox.Text = settings.HxaLinkGroup;
            this.outputKeepIntermediateFilesCheckBox.Checked = settings.KeepIntermediateFiles;
            this.projectFilterDescriptionTextBox.Text = settings.ProjectDescription;
            this.help2FormatCheckBox.Checked = settings.OutputHelp2;
            this.htmlFormatCheckBox.Checked = settings.OutputHtml;
            this.htmlHelpFormatCheckBox.Checked = settings.OutputHtmlHelp;
            this.outputOverviewHelpFilesCheckBox.Checked = settings.OutputOverviewHelpFiles;
            this.pdfFormatCheckBox.Checked = settings.OutputPdf;
            this.windowsHelpFormatCheckBox.Checked = settings.OutputWindowsHelp;
            this.xmlFormatCheckBox.Checked = settings.OutputXml;
            this.overrideOutputDirectoryCheckBox.Checked = settings.OverrideDefaultOutputDirectoryBehavior;
            this.outputDirectoryTextBox.Text = settings.OutputDirectory;
            this.overviewDoxFileTextBox.Text = settings.OverviewDoxTemplateFile;
            this.help2QaExecutableTextBox.Text = settings.QaExecutable;
            this.rebuildCheckBox.Checked = settings.Rebuild;
            this.showWhenFinishedCheckBox.Checked = settings.ShowWhenFinishedBuilding;
            this.sourceControlEnabledCheckBox.Checked = settings.SourceControlProviderSettings.Enabled;
            
            List<rageSourceControlProvider> providers = new List<rageSourceControlProvider>();
            foreach ( rageSourceControlProvider provider in settings.SourceControlProviderSettings.Providers )
            {
                providers.Add( provider.Clone() );
            }

            this.sourceControlProviderControl.Providers = providers;
            this.sourceControlProviderControl.SelectedIndex = settings.SourceControlProviderSettings.SelectedIndex;

            this.outputSourceDirectoriesTextBox.Text = settings.SourceDirectories;
            this.outputSourceDirectoryTreesTextBox.Text = settings.SourceDirectoryTrees;
            this.treeNamespaceTextBox.Text = settings.TreeNamespace;
            this.verboseLevelNumericUpDown.Value = settings.VerboseLevel;
            this.masterDoxFileTextBox.Text = settings.MasterDoxTemplateFile;
            this.unifyHelpFilesCheckBox.Checked = settings.UnifyHelpFiles;
            this.unifiedHelpFileNameTextBox.Text = settings.UnifiedHelpFileName;

            // HACK
            overrideOutputDirectoryCheckBox_CheckedChanged( this, EventArgs.Empty );

            this.outputFormatsGroupBox.ResumeLayout( false );
            this.outputFormatsGroupBox.PerformLayout();
            this.templateFilesGroupBox.ResumeLayout( false );
            this.templateFilesGroupBox.PerformLayout();
            this.installationInfoGroupBox.ResumeLayout( false );
            this.installationInfoGroupBox.PerformLayout();
            this.sourceControlGroupBox.ResumeLayout( false );
            this.sourceControlGroupBox.PerformLayout();
            this.outputGroupBox.ResumeLayout( false );
            this.outputGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.verboseLevelNumericUpDown)).EndInit();
            this.inputGroupBox.ResumeLayout( false );
            this.inputGroupBox.PerformLayout();
            this.buildHelpGroupBox.ResumeLayout( false );
            this.buildHelpGroupBox.PerformLayout();
        }

        private void SetProjectFromUI( ProjectSettings settings )
        {
            settings.CompanyDescription = this.companyFilterDescriptionTextBox.Text.Trim();
            settings.DoxTemplateFile = this.doxFileTextBox.Text.Trim();
            settings.DtxTemplateFile = this.dtxFileTextBox.Text.Trim();
            settings.HaltOnError = this.haltOnErrorCheckBox.Checked;
            settings.Help2RegExecutableFile = this.help2RegExecutableTextBox.Text.Trim();
            settings.HxaAttributeCompanyName = this.companyFilterAttributeNameTextBox.Text.Trim();
            settings.HxaAttributeCompanyValue = this.companyFilterAttributeValueTextBox.Text.Trim();
            settings.HxaAttributeProjectName = this.projectFilterAttributeNameTextBox.Text.Trim();
            settings.HxaAttributeProjectValue = this.projectFilterAttributeValueTextBox.Text.Trim();
            settings.HxaLinkGroup = this.hxaLinkGroupTextBox.Text.Trim();
            settings.KeepIntermediateFiles = this.outputKeepIntermediateFilesCheckBox.Checked;
            settings.ProjectDescription = this.projectFilterDescriptionTextBox.Text.Trim();
            settings.OutputHelp2 = this.help2FormatCheckBox.Checked;
            settings.OutputHtml = this.htmlFormatCheckBox.Checked;
            settings.OutputHtmlHelp = this.htmlHelpFormatCheckBox.Checked;
            settings.OutputOverviewHelpFiles = this.outputOverviewHelpFilesCheckBox.Checked;
            settings.OutputPdf = this.pdfFormatCheckBox.Checked;
            settings.OutputWindowsHelp = this.windowsHelpFormatCheckBox.Checked;
            settings.OutputXml = this.xmlFormatCheckBox.Checked;
            settings.OverrideDefaultOutputDirectoryBehavior = this.overrideOutputDirectoryCheckBox.Checked;
            settings.OutputDirectory = this.outputDirectoryTextBox.Text.Trim();
            settings.OverviewDoxTemplateFile = this.overviewDoxFileTextBox.Text.Trim();
            settings.QaExecutable = this.help2QaExecutableTextBox.Text.Trim();
            settings.Rebuild = this.rebuildCheckBox.Checked;
            settings.ShowWhenFinishedBuilding = this.showWhenFinishedCheckBox.Checked;
            settings.SourceControlProviderSettings.Enabled = this.sourceControlEnabledCheckBox.Checked;

            if ( this.sourceControlProviderControl.Providers.Count == settings.SourceControlProviderSettings.Providers.Count )
            {
                for ( int i = 0; i < settings.SourceControlProviderSettings.Providers.Count; ++i )
                {
                    settings.SourceControlProviderSettings.Providers[i].CopyFrom( this.sourceControlProviderControl.Providers[i] );
                }

                settings.SourceControlProviderSettings.SelectedIndex = this.sourceControlProviderControl.SelectedIndex;
            }
            
            settings.SourceDirectories = this.outputSourceDirectoriesTextBox.Text.Trim();
            settings.SourceDirectoryTrees = this.outputSourceDirectoryTreesTextBox.Text.Trim();
            settings.TreeNamespace = this.treeNamespaceTextBox.Text.Trim();
            settings.VerboseLevel = (int)this.verboseLevelNumericUpDown.Value;
            settings.MasterDoxTemplateFile = this.masterDoxFileTextBox.Text;
            settings.UnifyHelpFiles = this.unifyHelpFilesCheckBox.Checked;
            settings.UnifiedHelpFileName = this.unifiedHelpFileNameTextBox.Text;
        }

        private void AddRecentFile( string filename )
        {
            if ( this.fileRecentFilesToolStripMenuItem.DropDownItems.Count == 10 )
            {
                this.fileRecentFilesToolStripMenuItem.DropDownItems.RemoveAt( 9 );
            }

            foreach ( ToolStripMenuItem item in this.fileRecentFilesToolStripMenuItem.DropDownItems )
            {
                if ( item.Text == filename )
                {
                    return;
                }
            }

            ToolStripMenuItem newItem = new ToolStripMenuItem( filename, null, new EventHandler( fileRecentFilesItemToolStripMenuItem_Click ) );
            this.fileRecentFilesToolStripMenuItem.DropDownItems.Insert( 0, newItem );
        }

        private void RemoveRecentFile( string filename )
        {
            for ( int i = 0; i < this.fileRecentFilesToolStripMenuItem.DropDownItems.Count; ++i )
            {
                if ( this.fileRecentFilesToolStripMenuItem.DropDownItems[i].Text == filename )
                {
                    this.fileRecentFilesToolStripMenuItem.DropDownItems.RemoveAt( i );
                    break;
                }
            }
        }

        private void CenterLocation( Control parent, Form form )
        {
            Rectangle rect = parent.Bounds;
            form.StartPosition = FormStartPosition.Manual;

            int x = (rect.Width / 2) + rect.Left - (form.Width / 2);
            int y = (rect.Height / 2) + rect.Top - (form.Height / 2);

            form.Location = new Point( x, y );
        }
        #endregion
    }
}