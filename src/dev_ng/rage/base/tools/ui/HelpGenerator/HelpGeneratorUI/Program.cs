using Microsoft.VisualBasic.ApplicationServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

using HelpGeneratorCore;
using RSG.Base;

namespace HelpGeneratorUI
{
    class MultipleInstanceProgram : WindowsFormsApplicationBase
    {
        public MultipleInstanceProgram()
        {
            this.MainForm = new Form1();

            this.Startup += new StartupEventHandler( MultipleInstanceProgram_Startup );
        }

        #region Event Handlers
        private void MultipleInstanceProgram_Startup( object sender, StartupEventArgs e )
        {
            if ( e.CommandLine.Count > 0 )
            {
                rageCommandLineParser parser = new rageCommandLineParser();

                rageCommandLineItem settingsFileItem = parser.AddItem( new rageCommandLineItem( string.Empty, 1,
                    true, "filename", "The settings file that provides all of the information necessary to build, install and/or uninstall the help files." ) );

                string[] args = new string[e.CommandLine.Count];
                e.CommandLine.CopyTo( args, 0 );
                if ( !parser.Parse( args ) )
                {
                    DialogResult result = MessageBox.Show( parser.Error + "\n\nWould you like to continue?", "Command Line Error",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Error );
                    if ( result == DialogResult.No )
                    {
                        e.Cancel = true;
                    }

                    return;
                }

                ((Form1)this.MainForm).LoadProjectFile( settingsFileItem.Value as string );
            }
        }
        #endregion
    }

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main( string[] args )
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler( Application_ThreadException );

            sm_program = new MultipleInstanceProgram();
            sm_program.Run( args );
        }

        static MultipleInstanceProgram sm_program;

        static void Application_ThreadException( object sender, System.Threading.ThreadExceptionEventArgs e )
        {
            if ( sm_program.OpenForms.Count > 0 )
            {
                MessageBox.Show( sm_program.OpenForms[0], e.Exception.ToString(), "Unhandled Exception!", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
            else
            {
                MessageBox.Show( e.Exception.ToString(), "Unhandled Exception!", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
        }
    }
}