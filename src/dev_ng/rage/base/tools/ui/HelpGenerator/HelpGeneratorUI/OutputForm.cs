using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using HelpGeneratorCore;

namespace HelpGeneratorUI
{
    public partial class OutputForm : Form
    {
        protected OutputForm()
        {
            InitializeComponent();
        }

        public OutputForm( Generator generator, Action action )
            : this()
        {
            m_generator = generator;
            m_action = action;

            m_generator.OutputMessage += new OuputMessageEventHandler( Generator_OuputMessage );

            m_backgroundWorker.WorkerReportsProgress = false;
            m_backgroundWorker.WorkerSupportsCancellation = true;
            m_backgroundWorker.DoWork += new DoWorkEventHandler( backgroundWorker_DoWork );
            m_backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler( backgroundWorker_RunWorkerCompleted );
        }

        #region Enums
        public enum Action
        {
            Generate,
            Install,
            Uninstall,
            ExtractQAWarnings
        }
        #endregion

        #region Variables
        private Generator m_generator;
        private Action m_action;
        private BackgroundWorker m_backgroundWorker = new BackgroundWorker();
        #endregion

        #region Event Handlers
        private void OutputForm_Load( object sender, EventArgs e )
        {
            m_backgroundWorker.RunWorkerAsync();
        }

        private void saveLogButton_Click( object sender, EventArgs e )
        {
            if ( this.saveFileDialog.ShowDialog( this ) == DialogResult.OK )
            {
                try
                {
                    TextWriter writer = new StreamWriter( this.saveFileDialog.FileName, false );

                    foreach ( string line in this.outputRichTextBox.Lines )
                    {
                        writer.WriteLine( line );
                    }

                    writer.Close();
                }
                catch ( Exception ex )
                {
                    MessageBox.Show( "There was an error writing to '" + this.saveFileDialog.FileName + "':\n\n" + ex.ToString(), "Save File Error!",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
                }
            }
        }

        private void okCancelButton_Click( object sender, EventArgs e )
        {
            if ( m_backgroundWorker.IsBusy )
            {
                m_generator.Cancel = true;
                m_backgroundWorker.CancelAsync();
            }
            else
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        #region BackgroundWorker
        private void backgroundWorker_DoWork( object sender, DoWorkEventArgs e )
        {
            switch ( m_action )
            {
                case Action.Generate:
                    m_generator.GenerateHelpFiles( this );
                    e.Result = true;
                    break;
                case Action.Install:
                    m_generator.InstallHelpFiles( this );
                    e.Result = true;
                    break;
                case Action.Uninstall:
                    m_generator.UninstallHelpFiles( this );
                    e.Result = true;
                    break;
                case Action.ExtractQAWarnings:
                    m_generator.ExtractQAWarningsFromHelpFiles( this );
                    e.Result = true;
                    break;
            }

            if ( m_backgroundWorker.CancellationPending )
            {
                e.Cancel = true;
            }
        }
        
        private void backgroundWorker_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            if ( e.Cancelled )
            {
                m_generator.Cancel = false;
                this.DialogResult = DialogResult.Cancel;
            }

            this.okCancelButton.Text = "OK";
            this.saveLogButton.Enabled = true;
        }
        #endregion

        #region Generator
        private const int WM_VSCROLL = 277; // Vertical scroll
        private const int SB_BOTTOM = 7; // Scroll to bottom 

        [DllImport( "user32.dll", CharSet = CharSet.Auto )]
        private static extern int SendMessage( IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam );

        private void Generator_OuputMessage( object sender, OutputMessageEventArgs e )
        {
            this.outputRichTextBox.SuspendLayout();

            this.outputRichTextBox.AppendText( e.Message );
            SendMessage( this.outputRichTextBox.Handle, WM_VSCROLL, (IntPtr)SB_BOTTOM, IntPtr.Zero );

#if MULTICOLORED_OUTPUT
            string trimmedMessage = e.Message.Trim();
            Color foreColor = Color.Empty;
            if ( trimmedMessage.StartsWith( "Error:" ) || trimmedMessage.StartsWith( "Failure:" ) )
            {
                foreColor = Color.Red;
            }
            else if ( trimmedMessage.StartsWith( "Warning:" ) )
            {
                foreColor = Color.Goldenrod;
            }
            else if ( trimmedMessage.StartsWith( "Fatal Error:" ) )
            {
                foreColor = Color.Maroon;
            }

            if ( foreColor != Color.Empty )
            {
                int offset = 0;
                while ( (offset < e.Message.Length) && Char.IsWhiteSpace( e.Message[offset] ) )
                {
                    ++offset;
                }

                int startIndex = this.outputRichTextBox.Text.Length - e.Message.Length + offset;
                int length = e.Message.Length - offset;
                this.outputRichTextBox.Select( (startIndex < 0) ? 0 : startIndex, (length > this.outputRichTextBox.Text.Length) ? this.outputRichTextBox.Text.Length : length );
                this.outputRichTextBox.SelectionColor = foreColor;
                this.outputRichTextBox.SelectionBackColor = this.outputRichTextBox.BackColor;
                this.outputRichTextBox.DeselectAll();
            }
#endif

            this.outputRichTextBox.ResumeLayout( true );
        }
        #endregion

        private void OutputForm_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( m_backgroundWorker.IsBusy )
            {
                e.Cancel = true;
                m_generator.Cancel = true;
                m_backgroundWorker.CancelAsync();

                if ( Debugger.IsAttached )
                {
                    e.Cancel = false;
                    this.DialogResult = DialogResult.Cancel;
                }
            }
            else
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        #endregion
    }
}