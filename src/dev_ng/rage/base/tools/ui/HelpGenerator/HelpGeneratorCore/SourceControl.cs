using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

using rageUsefulCSharpToolClasses;

namespace HelpGeneratorCore
{
    public class SourceControl
    {
        public SourceControl( ProjectSettings settings )
        {
            m_projectSettings = settings;
        }
        
        public SourceControl( Form mainForm, ProjectSettings settings )
            : this( settings )
        {
            m_mainForm = mainForm;            
        }

        #region Variables
        private Form m_mainForm;
        private ProjectSettings m_projectSettings;
        private bool m_loggedOnToSourceControl = false;
        #endregion

        #region Public Functions
        public bool CheckOutFile( string filename, ref string errorMessage )
        {
            // see if it exists
            if ( !File.Exists( filename ) )
            {
                return true;
            }

            // see if it's writable
            FileAttributes attributes = File.GetAttributes( filename );
            if ( (attributes & FileAttributes.ReadOnly) != FileAttributes.ReadOnly )
            {
                return true;
            }

            // we're using source control
            if ( m_projectSettings.SourceControlEnabled )
            {
                string srcCtrlFileName = m_projectSettings.GetSourceControlFileName( filename, false );
                if ( srcCtrlFileName != string.Empty )
                {
                    if ( !LogonSourceControl() )
                    {
                        errorMessage = "Could not log onto source control";
                        return false;
                    }

                    rageStatus status;
                    switch ( m_projectSettings.SourceControlProvider )
                    {
                        case ProjectSettings.Provider.Alienbrain:
                            {
                                if ( rageNXN.FileExists( srcCtrlFileName, out status ) )
                                {
                                    rageNXN.CheckOutFile( srcCtrlFileName, filename, false, out status );
                                    
                                    if ( !status.Success() )
                                    {
                                        errorMessage = status.GetErrorString();
                                        return false;
                                    }
                                }
                                else
                                {
                                    errorMessage = String.Format( "{0} is not in source control.", filename );
                                    return false;
                                }
                            }
                            break;
                        case ProjectSettings.Provider.CVS:
                            {
                                status = new rageStatus();
                                // nothing to do
                            }
                            break;
                        case ProjectSettings.Provider.Perforce:
                            {
                                if ( ragePerforce.FileExists( srcCtrlFileName, out status ) )
                                {
                                    ragePerforce.CheckOutFile( srcCtrlFileName, out status );

                                    if ( !status.Success() )
                                    {
                                        errorMessage = status.GetErrorString();
                                        return false;
                                    }
                                }
                                else
                                {
                                    if ( !status.Success() && (status.ErrorString.Contains( " not in client view." ) ) )
                                    {
                                        errorMessage = String.Format( "{0} is not in source control.", filename );
                                        return false;
                                    }
                                }
                            }
                            break;
                    }
                }
            }

            // silently make it writable
            return MakeFileWritable( filename, true );
        }

        public bool UndoCheckOutFile( string filename, bool force, ref string errorMessage )
        {
            // see if it exists
            if ( !File.Exists( filename ) )
            {
                return true;
            }

            // we're using source control
            if ( m_projectSettings.SourceControlEnabled )
            {
                string srcCtrlFileName = m_projectSettings.GetSourceControlFileName( filename, false );
                if ( srcCtrlFileName != string.Empty )
                {
                    if ( !LogonSourceControl() )
                    {
                        errorMessage = "Could not log onto source control";
                        return false;
                    }

                    rageStatus status = new rageStatus();
                    switch ( m_projectSettings.SourceControlProvider )
                    {
                        case ProjectSettings.Provider.Alienbrain:
                            {
                                if ( rageNXN.FileExists( srcCtrlFileName, out status ) )
                                {
                                    rageNXN.UndoCheckOutFile( srcCtrlFileName, !force, out status );

                                    if ( !status.Success() )
                                    {
                                        errorMessage = status.GetErrorString();
                                        return false;
                                    }
                                }
                                else
                                {
                                    errorMessage = String.Format( "{0} is not in source control.", filename );
                                    return false;
                                }
                            }
                            break;
                        case ProjectSettings.Provider.CVS:
                            {
                                if ( force )
                                {                                    
                                    rageExecuteCommand command = new rageExecuteCommand();
                                    command.Command = "cvs";
                                    command.Arguments = " update \"" + srcCtrlFileName + "\"";
                                    command.EchoToConsole = false;
                                    command.LogToHtmlFile = false;
                                    command.UpdateLogFileInRealTime = false;
                                    command.UseBusySpinner = false;
                                    command.WorkingDirectory = string.Empty;

                                    command.Execute( out status );

                                    if ( !status.Success() )
                                    {
                                        errorMessage = status.GetErrorString();
                                        return false;
                                    }
                                }
                            }
                            break;
                        case ProjectSettings.Provider.Perforce:
                            {
                                if ( ragePerforce.FileExists( srcCtrlFileName, out status ) )
                                {
                                    ragePerforce.RevertFile( srcCtrlFileName, !force, out status );

                                    if ( !status.Success() )
                                    {
                                        errorMessage = status.GetErrorString();
                                        return false;
                                    }
                                }
                                else
                                {
                                    if ( !status.Success() && (status.ErrorString.Contains( " not in client view." ) ) )
                                    {
                                        errorMessage = String.Format( "{0} is not in source control.", filename );
                                        return false;
                                    }
                                }
                            }
                            break;
                    }
                }
            }

            return true;
        }

        public bool AddFileToSourceControl( string filename, ref string errorMessage )
        {
            // see if it exists
            if ( !File.Exists( filename ) )
            {
                return true;
            }

            if ( m_projectSettings.SourceControlEnabled )
            {
                string srcCtrlFileName = m_projectSettings.GetSourceControlFileName( filename, false );
                if ( srcCtrlFileName != string.Empty )
                {
                    if ( !LogonSourceControl() )
                    {
                        errorMessage = "Could not log onto source control";
                        return false;
                    }

                    rageStatus status = new rageStatus();
                    switch ( m_projectSettings.SourceControlProvider )
                    {
                        case ProjectSettings.Provider.Alienbrain:
                            {
                                if ( !rageNXN.FileExists( srcCtrlFileName, out status ) )
                                {
                                    rageNXN.Import( srcCtrlFileName, filename, string.Empty, out status );
                                }
                            }
                            break;
                        case ProjectSettings.Provider.CVS:
                            {
                                rageExecuteCommand command = new rageExecuteCommand();
                                command.Command = "cvs";

                                command.Arguments = " add ";
                                string ext = Path.GetExtension( srcCtrlFileName );
                                if ( (ext != "txt") && (ext != "dtx") && (ext != "dox") )
                                {
                                    command.Arguments += "-kb ";
                                }
                                command.Arguments += "\"" + srcCtrlFileName + "\"";

                                command.EchoToConsole = false;
                                command.LogToHtmlFile = false;
                                command.UpdateLogFileInRealTime = false;
                                command.UseBusySpinner = false;
                                command.WorkingDirectory = string.Empty;

                                command.Execute( out status );
                            }
                            break;
                        case ProjectSettings.Provider.Perforce:
                            {
                                if ( !ragePerforce.FileExists( srcCtrlFileName, out status ) )
                                {
                                    ragePerforce.AddFile( srcCtrlFileName, false, out status );
                                }
                            }
                            break;
                    }

                    if ( !status.Success() )
                    {
                        errorMessage = status.GetErrorString();
                        return false;
                    }
               }
            }

            return true;
        }

        public bool AddDirectoryToSourceControl( string directory, ref string errorMessage )
        {
            // see if it exists
            if ( !Directory.Exists( directory ) )
            {
                return true;
            }

            if ( m_projectSettings.SourceControlEnabled )
            {
                string srcCtrlDirectory = m_projectSettings.GetSourceControlFileName( directory, false );
                if ( srcCtrlDirectory != string.Empty )
                {
                    if ( !LogonSourceControl() )
                    {
                        return false;
                    }

                    rageStatus status = null;
                    switch ( m_projectSettings.SourceControlProvider )
                    {
                        case ProjectSettings.Provider.Alienbrain:
                        case ProjectSettings.Provider.CVS:
                            {
                                bool success = true;
                                string lastErrorMessage = null;
                                 
                                string[] filenames = Directory.GetFiles( directory );
                                foreach ( string filename in filenames )
                                {
                                    string errMsg = null;
                                    if ( !AddFileToSourceControl( filename, ref errMsg ) )
                                    {
                                        success = false;
                                        lastErrorMessage = errMsg;
                                    }
                                }

                                if ( success )
                                {
                                    status = new rageStatus();
                                }
                                else
                                {
                                    status = new rageStatus( lastErrorMessage );
                                }
                            }
                            break;
                        case ProjectSettings.Provider.Perforce:
                            ragePerforce.AddAllFilesInFolder( srcCtrlDirectory, false, out status );
                            break;
                    }
                    
                    if ( !status.Success() )
                    {
                        errorMessage = status.GetErrorString();
                        return false;
                    }

                    return true;
                }
            }

            return true;
        }

        public bool LogonSourceControl()
        {
            if ( m_projectSettings.SourceControlEnabled && !m_loggedOnToSourceControl
                && (m_projectSettings.SourceControlProvider == ProjectSettings.Provider.Alienbrain) )
            {
                rageStatus status;
                rageNXN.LogonToProject( m_projectSettings.SourceControlServer, m_projectSettings.SourceControlProject,
                    m_projectSettings.SourceControlLoginName, out status );

                if ( !status.Success() )
                {
                    return false;
                }

                m_loggedOnToSourceControl = true;
            }

            return true;
        }

        public bool LogoffSourceControl()
        {
            if ( m_projectSettings.SourceControlEnabled && m_loggedOnToSourceControl
                && (m_projectSettings.SourceControlProvider == ProjectSettings.Provider.Alienbrain) )
            {
                rageStatus status;
                rageNXN.LogoffFromProject( m_projectSettings.SourceControlProject, out status );

                if ( !status.Success() )
                {
                    return false;
                }

                m_loggedOnToSourceControl = false;
            }

            return true;
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Helper function to make a file writable after we already know it exists and that it is read-only
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="silent"></param>
        /// <returns>true if it is ok to save</returns>
        private bool MakeFileWritable( string filename, bool silent )
        {
            DialogResult result = DialogResult.Yes;

            if ( !silent )
            {
                if ( m_mainForm != null )
                {
                    result = MessageBox.Show( m_mainForm, "'" + filename + "' is read-only.  Shall I attempt to remove the write protection and save the file?",
                        "File Read Only", MessageBoxButtons.YesNo, MessageBoxIcon.Question );
                }
                else
                {
                    Console.WriteLine( "'" + filename + "' is read-only.  Shall I attempt to remove the write protection and save the file?" );
                    Console.WriteLine( "y/n?" );
                    int character = Console.Read();
                    char c = Convert.ToChar( character );
                    if (( c == 'y') || (c == 'Y') )
                    {
                        result = DialogResult.Yes;
                    }
                    else
                    {
                        result = DialogResult.No;
                    }
                }
            }

            try
            {
                if ( result == DialogResult.Yes )
                {
                    File.SetAttributes( filename, File.GetAttributes( filename ) & (~FileAttributes.ReadOnly) );
                    return true;
                }
            }
            catch ( Exception e )
            {
                if ( m_mainForm != null )
                {
                    MessageBox.Show( m_mainForm, "Unable to make '" + filename + "' writable.\n\n" + e.Message, "File Make Writable Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error );
                }
                else
                {
                    Console.WriteLine( "Unable to make '" + filename + "' writable.\n\n" + e.Message );
                }
            }

            return false;
        }
        #endregion
    }
}
