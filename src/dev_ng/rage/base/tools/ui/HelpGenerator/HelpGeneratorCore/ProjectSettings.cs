using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;

using RSG.Base.SCM;

namespace HelpGeneratorCore
{
    [Serializable]
    public class ProjectSettings
    {
        public ProjectSettings()
        {
            Clear();
        }

        #region Enums
        public enum Provider
        {
            Perforce,
            CVS
        }
        #endregion

        #region Variables
        private string m_projectFilename;
        private string m_originalProjectFilename;

        private string m_dtxTemplateFile;
        private string m_doxTemplateFile;
        private string m_overviewDoxTemplateFile;
        private string m_masterDoxTemplateFile;

        private string m_help2RegExecutableFile;
        private string m_treeNamespace;
        private string m_namespaceDescription;
        private string m_companyDescription;
        private string m_hxaLinkGroup;
        private string m_hxaAttributeCompanyName;
        private string m_hxaAttributeCompanyValue;
        private string m_hxaAttributeProjectName;
        private string m_hxaAttributeProjectValue;

        private bool m_outputHtmlHelp;
        private bool m_outputHelp2;
        private bool m_outputHtml;
        private bool m_outputWindowsHelp;
        private bool m_outputPdf;
        private bool m_outputXml;
        private bool m_overrideDefaultOutputDirectoryBehavior;
        private string m_outputDirectory;

        private rageSourceControlProviderSettings m_sourceControlProviderSettings = new rageSourceControlProviderSettings();

        private string m_qaExecutable;
        private List<string> m_sourceDirectories = new List<string>();
        private List<string> m_sourceDirectoryTrees = new List<string>();
        private bool m_keepIntermediateFiles = false;
        private bool m_haltOnError = true;
        private bool m_unifyHelpFiles = false;
        private string m_unifiedHelpFileName;
        private bool m_outputOverviewHelpFiles = false;
        private int m_verboseLevel = 1;
        private bool m_rebuild = false;
        private bool m_showWhenFinishedBuilding = true;        
        #endregion

        #region Properties
        [XmlIgnoreAttribute]
        public string ProjectFilename
        {
            get
            {
                return m_projectFilename;
            }
            set
            {
                // use a bit of "reflection" to re-store these with new project-relative path information
                if ( this.IsLoaded )
                {
                    // retrieve with full path (if the project was already loaded)
                    string dtxTemplateFile = this.DtxTemplateFile;
                    string doxTemplateFile = this.DoxTemplateFile;
                    string overviewDoxTemplateFile = this.OverviewDoxTemplateFile;
                    string masterDoxTemplateFile = this.MasterDoxTemplateFile;
                    string sourceDirectories = this.SourceDirectories;
                    string qaExecutable = this.QaExecutable;

                    if ( value != null )
                    {
                        m_projectFilename = Path.GetFullPath( value );
                    }
                    else
                    {
                        m_projectFilename = null;
                    }

                    if ( m_originalProjectFilename == null )
                    {
                        m_originalProjectFilename = m_projectFilename;
                    }

                    if ( !String.IsNullOrEmpty( dtxTemplateFile ) )
                    {
                        this.DtxTemplateFile = dtxTemplateFile;
                    }

                    if ( !String.IsNullOrEmpty( doxTemplateFile ) )
                    {
                        this.DoxTemplateFile = doxTemplateFile;
                    }

                    if ( !String.IsNullOrEmpty( overviewDoxTemplateFile ) )
                    {
                        this.OverviewDoxTemplateFile = overviewDoxTemplateFile;
                    }

                    if ( !String.IsNullOrEmpty( masterDoxTemplateFile ) )
                    {
                        this.MasterDoxTemplateFile = masterDoxTemplateFile;
                    }

                    if ( !String.IsNullOrEmpty( sourceDirectories ) )
                    {
                        this.SourceDirectories = sourceDirectories;
                    }

                    if ( !String.IsNullOrEmpty( qaExecutable ) )
                    {
                        this.QaExecutable = qaExecutable;
                    }
                }
                else
                {
                    if ( value != null )
                    {
                        m_projectFilename = Path.GetFullPath( value );
                    }
                    else
                    {
                        m_projectFilename = null;
                    }
                }
            }
        }

        public string OriginalProjectFilename
        {
            get
            {
                return m_originalProjectFilename;
            }
            set
            {
                m_originalProjectFilename = value;
            }
        }

        [XmlIgnoreAttribute]
        public bool IsLoaded
        {
            get
            {
                return !String.IsNullOrEmpty( m_projectFilename );
            }
        }

        [XmlIgnoreAttribute]
        public string DtxTemplateFile
        {
            get
            {
                if ( this.IsLoaded && !String.IsNullOrEmpty( m_dtxTemplateFile ) )
                {
                    return GetAbsolutePath( Path.GetDirectoryName( m_originalProjectFilename ),
                        Path.GetDirectoryName( m_projectFilename ), m_dtxTemplateFile );
                }
                else
                {
                    return m_dtxTemplateFile;
                }
            }
            set
            {
                if ( this.IsLoaded )
                {
                    m_dtxTemplateFile = GetRelativePath( Path.GetDirectoryName( m_projectFilename ), value );
                }
                else
                {
                    m_dtxTemplateFile = value;
                }
            }
        }

        public string DtxTemplateFileRaw
        {
            get
            {
                return m_dtxTemplateFile;
            }
            set
            {
                m_dtxTemplateFile = value;
            }
        }

        [XmlIgnoreAttribute]
        public string DoxTemplateFile
        {
            get
            {
                if ( this.IsLoaded && !String.IsNullOrEmpty( m_doxTemplateFile ) )
                {
                    return GetAbsolutePath( Path.GetDirectoryName( m_originalProjectFilename ), 
                        Path.GetDirectoryName( m_projectFilename ), m_doxTemplateFile );
                }
                else
                {
                    return m_doxTemplateFile;
                }
            }
            set
            {
                if ( this.IsLoaded )
                {
                    m_doxTemplateFile = GetRelativePath( Path.GetDirectoryName( m_projectFilename ), value );
                }
                else
                {
                    m_doxTemplateFile = value;
                }
            }
        }

        public string DoxTemplateFileRaw
        {
            get
            {
                return m_doxTemplateFile;
            }
            set
            {
                m_doxTemplateFile = value;
            }
        }

        [XmlIgnoreAttribute]
        public string OverviewDoxTemplateFile
        {
            get
            {
                if ( this.IsLoaded && !String.IsNullOrEmpty( m_overviewDoxTemplateFile ) )
                {
                    return GetAbsolutePath( Path.GetDirectoryName( m_originalProjectFilename ), 
                        Path.GetDirectoryName( m_projectFilename ), m_overviewDoxTemplateFile );
                }
                else
                {
                    return m_overviewDoxTemplateFile;
                }
            }
            set
            {
                if ( this.IsLoaded )
                {
                    m_overviewDoxTemplateFile = GetRelativePath( Path.GetDirectoryName( m_projectFilename ), value );
                }
                else
                {
                    m_overviewDoxTemplateFile = value;
                }
            }
        }

        public string OverviewDoxTemplateFileRaw
        {
            get
            {
                return m_overviewDoxTemplateFile;
            }
            set
            {
                m_overviewDoxTemplateFile = value;
            }
        }

        [XmlIgnoreAttribute]
        public string MasterDoxTemplateFile
        {
            get
            {
                if ( this.IsLoaded && !String.IsNullOrEmpty( m_masterDoxTemplateFile ) )
                {
                    return GetAbsolutePath( Path.GetDirectoryName( m_originalProjectFilename ), 
                        Path.GetDirectoryName( m_projectFilename ), m_masterDoxTemplateFile );
                }
                else
                {
                    return m_masterDoxTemplateFile;
                }
            }
            set
            {
                if ( this.IsLoaded )
                {
                    m_masterDoxTemplateFile = GetRelativePath( Path.GetDirectoryName( m_projectFilename ), value );
                }
                else
                {
                    m_masterDoxTemplateFile = value;
                }
            }
        }

        public string MasterDoxTemplateFileRaw
        {
            get
            {
                return m_masterDoxTemplateFile;
            }
            set
            {
                m_masterDoxTemplateFile = value;
            }
        }

        [XmlIgnoreAttribute]
        public string Help2RegExecutableFile
        {
            get
            {
                if ( this.IsLoaded && !String.IsNullOrEmpty( m_help2RegExecutableFile ) )
                {
                    return GetAbsolutePath( Path.GetDirectoryName( m_originalProjectFilename ), 
                        Path.GetDirectoryName( m_projectFilename ), m_help2RegExecutableFile );
                }
                else
                {
                    return m_help2RegExecutableFile;
                }
            }
            set
            {
                if ( this.IsLoaded )
                {
                    m_help2RegExecutableFile = GetRelativePath( Path.GetDirectoryName( m_projectFilename ), value );
                }
                else
                {
                    m_help2RegExecutableFile = value;
                }
            }
        }

        public string Help2RegExecutableFileRaw
        {
            get
            {
                return m_help2RegExecutableFile;
            }
            set
            {
                m_help2RegExecutableFile = value;
            }
        }
        
        public string TreeNamespace
        {
            get
            {
                return m_treeNamespace;
            }
            set
            {
                m_treeNamespace = value;
            }
        }

        public string ProjectDescription
        {
            get
            {
                return m_namespaceDescription;
            }
            set
            {
                m_namespaceDescription = value;
            }
        }

        public string CompanyDescription
        {
            get
            {
                return m_companyDescription;
            }
            set
            {
                m_companyDescription = value;
            }
        }

        [XmlIgnoreAttribute]
        public string CompanyFilterQuery
        {
            get
            {
                StringBuilder filter = new StringBuilder();

                if ( !String.IsNullOrEmpty( m_hxaAttributeCompanyName ) && !String.IsNullOrEmpty( m_hxaAttributeCompanyValue ) )
                {
                    filter.Append( "(\"" );
                    filter.Append( m_hxaAttributeCompanyName );
                    filter.Append( "\"=\"" );
                    filter.Append( m_hxaAttributeCompanyValue );
                    filter.Append( "\")" );
                }

                return filter.ToString();
            }
        }

        [XmlIgnoreAttribute]
        public string ProjectFilterQuery
        {
            get
            {
                StringBuilder filter = new StringBuilder();

                if ( !String.IsNullOrEmpty( m_hxaAttributeProjectName ) && !String.IsNullOrEmpty( m_hxaAttributeProjectValue ) )
                {
                    filter.Append( "(\"" );
                    filter.Append( m_hxaAttributeProjectName );
                    filter.Append( "\"=\"" );
                    filter.Append( m_hxaAttributeProjectValue );
                    filter.Append( "\")" );
                }

                return filter.ToString();
            }
        }

        public string HxaLinkGroup
        {
            get
            {
                return m_hxaLinkGroup;
            }
            set
            {
                m_hxaLinkGroup = value;
            }
        }

        public string HxaAttributeCompanyName
        {
            get
            {
                return m_hxaAttributeCompanyName;
            }
            set
            {
                m_hxaAttributeCompanyName = value;
            }
        }

        public string HxaAttributeCompanyValue
        {
            get
            {
                return m_hxaAttributeCompanyValue;
            }
            set
            {
                m_hxaAttributeCompanyValue = value;
            }
        }

        public string HxaAttributeProjectName
        {
            get
            {
                return m_hxaAttributeProjectName;
            }
            set
            {
                m_hxaAttributeProjectName = value;
            }
        }

        public string HxaAttributeProjectValue
        {
            get
            {
                return m_hxaAttributeProjectValue;
            }
            set
            {
                m_hxaAttributeProjectValue = value;
            }
        }

        public bool OutputHtmlHelp
        {
            get
            {
                return m_outputHtmlHelp;
            }
            set
            {
                m_outputHtmlHelp = value;
            }
        }

        public bool OutputHelp2
        {
            get
            {
                return m_outputHelp2;
            }
            set
            {
                m_outputHelp2 = value;
            }
        }

        public bool OutputHtml
        {
            get
            {
                return m_outputHtml;
            }
            set
            {
                m_outputHtml = value;
            }
        }

        public bool OutputWindowsHelp
        {
            get
            {
                return m_outputWindowsHelp;
            }
            set
            {
                m_outputWindowsHelp = value;
            }
        }

        public bool OutputPdf
        {
            get
            {
                return m_outputPdf;
            }
            set
            {
                m_outputPdf = value;
            }
        }

        public bool OutputXml
        {
            get
            {
                return m_outputXml;
            }
            set
            {
                m_outputXml = value;
            }
        }

        public bool OverrideDefaultOutputDirectoryBehavior
        {
            get
            {
                return m_overrideDefaultOutputDirectoryBehavior;
            }
            set
            {
                m_overrideDefaultOutputDirectoryBehavior = value;
            }
        }

        [XmlIgnoreAttribute]
        public string OutputDirectory
        {
            get
            {
                if ( this.IsLoaded && !String.IsNullOrEmpty( m_outputDirectory ) )
                {
                    return GetAbsolutePath( Path.GetDirectoryName( m_originalProjectFilename ), 
                        Path.GetDirectoryName( m_projectFilename ), m_outputDirectory );
                }
                else
                {
                    return m_outputDirectory;
                }
            }
            set
            {
                if ( this.IsLoaded )
                {
                    m_outputDirectory = GetRelativePath( Path.GetDirectoryName( m_projectFilename ), value );
                }
                else
                {
                    m_outputDirectory = value;
                }
            }
        }

        public string OutputDirectoryRaw
        {
            get
            {
                return m_outputDirectory;
            }
            set
            {
                m_outputDirectory = value;
            }
        }

        /// <summary>
        /// Deprecated
        /// </summary>
        public bool SourceControlEnabled
        {
            get
            {
                return this.SourceControlProviderSettings.Enabled;
            }
            set
            {
                this.SourceControlProviderSettings.Enabled = value;
            }
        }

        /// <summary>
        /// Deprecated
        /// </summary>
        public Provider SourceControlProvider
        {
            get
            {
                if ( this.SourceControlProviderSettings.SelectedProvider != null )
                {
                    if ( this.SourceControlProviderSettings.SelectedProvider is ragePerforceSourceControlProvider )
                    {
                        return Provider.Perforce;
                    }
                    else
                    {
                        return Provider.CVS;    // not really supported
                    }
                }

                return Provider.Perforce;
            }
            set
            {
                switch ( value )
                {
                    case Provider.Perforce:
                        {
                            for ( int i = 0; i < this.SourceControlProviderSettings.Providers.Count; ++i )
                            {
                                if ( this.SourceControlProviderSettings.Providers[i] is ragePerforceSourceControlProvider )
                                {
                                    this.SourceControlProviderSettings.SelectedIndex = i;
                                    break;
                                }
                            }
                        }
                        break;
                    case Provider.CVS:
                        {
                            this.SourceControlProviderSettings.SelectedIndex = -1;  // not supported
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Deprecated
        /// </summary>
        public string SourceControlServer
        {
            get
            {
                if ( this.SourceControlProviderSettings.SelectedProvider != null )
                {
                    return this.SourceControlProviderSettings.SelectedProvider.ServerOrPort;
                }

                return string.Empty;
            }
            set
            {
                if ( this.SourceControlProviderSettings.SelectedProvider != null )
                {
                    this.SourceControlProviderSettings.SelectedProvider.ServerOrPort = value;
                }
            }
        }

        /// <summary>
        /// Deprecated
        /// </summary>
        public string SourceControlProject
        {
            get
            {
                if ( this.SourceControlProviderSettings.SelectedProvider != null )
                {
                    return this.SourceControlProviderSettings.SelectedProvider.ProjectOrWorkspace;
                }

                return string.Empty;
            }
            set
            {
                if ( this.SourceControlProviderSettings.SelectedProvider != null )
                {
                    this.SourceControlProviderSettings.SelectedProvider.ProjectOrWorkspace = value;
                }
            }
        }

        /// <summary>
        /// Deprecated
        /// </summary>
        public string SourceControlLoginName
        {
            get
            {
                if ( this.SourceControlProviderSettings.SelectedProvider != null )
                {
                    return this.SourceControlProviderSettings.SelectedProvider.LoginOrUsername;
                }

                return string.Empty;
            }
            set
            {
                if ( this.SourceControlProviderSettings.SelectedProvider != null )
                {
                    this.SourceControlProviderSettings.SelectedProvider.LoginOrUsername = value;
                }
            }
        }

        public rageSourceControlProviderSettings SourceControlProviderSettings
        {
            get
            {
                return m_sourceControlProviderSettings;
            }
            set
            {
                if ( value != null )
                {
                    m_sourceControlProviderSettings = value;
                }
            }
        }

        [XmlIgnoreAttribute]
        public string QaExecutable
        {
            get
            {
                if ( this.IsLoaded && !String.IsNullOrEmpty( m_qaExecutable ) )
                {
                    return GetAbsolutePath( Path.GetDirectoryName( m_originalProjectFilename ), 
                        Path.GetDirectoryName( m_projectFilename ), m_qaExecutable );
                }
                else
                {
                    return m_qaExecutable;
                }
            }
            set
            {
                if ( this.IsLoaded )
                {
                    m_qaExecutable = GetRelativePath( Path.GetDirectoryName( m_projectFilename ), value );
                }
                else
                {
                    m_qaExecutable = value;
                }
            }
        }

        public string QaExecutableRaw
        {
            get
            {
                return m_qaExecutable;
            }
            set
            {
                m_qaExecutable = value;
            }
        }

        [XmlIgnoreAttribute]
        public string[] SourceDirectoriesList
        {
            get
            {
                if ( this.IsLoaded )
                {
                    string originalProjectDirectory = Path.GetDirectoryName( m_originalProjectFilename );
                    string projectDirectory = Path.GetDirectoryName( m_projectFilename );

                    string[] directories = new string[m_sourceDirectories.Count];
                    for ( int i = 0; i < m_sourceDirectories.Count; ++i )
                    {
                        directories[i] = GetAbsolutePath( originalProjectDirectory, projectDirectory, m_sourceDirectories[i] );
                    }

                    return directories;
                }
                else
                {
                    return m_sourceDirectories.ToArray();
                }
            }
            set
            {
                m_sourceDirectories.Clear();

                if ( value != null )
                {
                    string projectDirectory = null;
                    if ( this.IsLoaded )
                    {
                        projectDirectory = Path.GetDirectoryName( m_projectFilename );
                    }

                    foreach ( string path in value )
                    {
                        m_sourceDirectories.Add( (projectDirectory == null) ? path : GetRelativePath( projectDirectory, path ) );
                    }
                }
            }
        }

        public string[] SourceDirectoriesListRaw
        {
            get
            {
                return m_sourceDirectories.ToArray();
            }
            set
            {
                m_sourceDirectories.Clear();

                if ( value != null )
                {
                    foreach ( string dir in value )
                    {
                        m_sourceDirectories.Add( dir );
                    }
                }
            }
        }

        [XmlIgnoreAttribute]
        public string SourceDirectories
        {
            get
            {
                StringBuilder directories = new StringBuilder();

                string originalProjectDirectory = null;
                string projectDirectory = null;
                if ( this.IsLoaded )
                {
                    originalProjectDirectory = Path.GetDirectoryName( m_originalProjectFilename );
                    projectDirectory = Path.GetDirectoryName( m_projectFilename );
                }
                
                for ( int i = 0; i < m_sourceDirectories.Count; ++i )
                {
                    if ( i > 0 )
                    {
                        directories.Append( Path.PathSeparator );
                    }

                    directories.Append( (projectDirectory == null)
                        ? m_sourceDirectories[i] : GetAbsolutePath( originalProjectDirectory, projectDirectory, m_sourceDirectories[i] ) );
                }

                return directories.ToString();
            }
            set
            {
                m_sourceDirectories.Clear();

                string projectDirectory = null;
                if ( this.IsLoaded )
                {
                    projectDirectory = Path.GetDirectoryName( m_projectFilename );
                }

                string[] pathSplit = value.Split( new char[] { Path.PathSeparator }, StringSplitOptions.RemoveEmptyEntries );
                foreach ( string path in pathSplit )
                {
                    string fullPath = path;
                    if ( path.StartsWith( ".." ) )
                    {
                        fullPath = Path.GetFullPath( Path.Combine( Directory.GetCurrentDirectory(), path ) );
                    }

                    string srcDir = (projectDirectory == null) ? fullPath : GetRelativePath( projectDirectory, fullPath );
                    m_sourceDirectories.Add( srcDir.TrimEnd( new char[]{ '\\' } ) );
                }
            }
        }

        public string[] SourceDirectoryTreesList
        {
            get
            {
                return m_sourceDirectoryTrees.ToArray();
            }
            set
            {
                m_sourceDirectoryTrees.Clear();

                if ( value != null )
                {
                    m_sourceDirectoryTrees.AddRange( value );
                }
            }
        }

        [XmlIgnoreAttribute]
        public string SourceDirectoryTrees
        {
            get
            {
                StringBuilder trees = new StringBuilder();

                for ( int i = 0; i < m_sourceDirectoryTrees.Count; ++i )
                {
                    if ( i > 0 )
                    {
                        trees.Append( Path.PathSeparator );
                    }

                    trees.Append( m_sourceDirectoryTrees[i] );
                }

                return trees.ToString();
            }
            set
            {
                m_sourceDirectoryTrees.Clear();

                if ( value != null )
                {
                    string[] pathSplit = value.Split( new char[] { Path.PathSeparator }, StringSplitOptions.RemoveEmptyEntries );
                    foreach ( string path in pathSplit )
                    {
                        m_sourceDirectoryTrees.Add( path );
                    }
                }
            }
        }
        
        public bool KeepIntermediateFiles
        {
            get
            {
                return m_keepIntermediateFiles;
            }
            set
            {
                m_keepIntermediateFiles = value;
            }
        }

        public bool HaltOnError
        {
            get
            {
                return m_haltOnError;
            }
            set
            {
                m_haltOnError = value;
            }
        }

        public bool UnifyHelpFiles
        {
            get
            {
                return m_unifyHelpFiles;
            }
            set
            {
                m_unifyHelpFiles = value;
            }
        }

        public string UnifiedHelpFileName
        {
            get
            {
                return m_unifiedHelpFileName;
            }
            set
            {
                m_unifiedHelpFileName = value;
            }
        }

        public bool OutputOverviewHelpFiles
        {
            get
            {
                return m_outputOverviewHelpFiles;
            }
            set
            {
                m_outputOverviewHelpFiles = value;
            }
        }

        public int VerboseLevel
        {
            get
            {
                return m_verboseLevel;
            }
            set
            {                
                m_verboseLevel = value;

                if ( m_verboseLevel < 1 )
                {
                    m_verboseLevel = 1;
                }
                else if ( m_verboseLevel > 3 )
                {
                    m_verboseLevel = 3;
                }
            }
        }

        public bool Rebuild
        {
            get
            {
                return m_rebuild;
            }
            set
            {
                m_rebuild = value;
            }
        }

        public bool ShowWhenFinishedBuilding
        {
            get
            {
                return m_showWhenFinishedBuilding;
            }
            set
            {
                m_showWhenFinishedBuilding = value;
            }
        }

        [XmlIgnoreAttribute]
        public bool CanGenerateHelp
        {
            get
            {
                return !String.IsNullOrEmpty( m_dtxTemplateFile )
                    && !String.IsNullOrEmpty( m_doxTemplateFile )
                    && (!m_outputOverviewHelpFiles || !String.IsNullOrEmpty( m_overviewDoxTemplateFile ))
                    && (m_outputHtml || m_outputHelp2 || m_outputHtmlHelp || m_outputWindowsHelp || m_outputPdf || m_outputXml)
                    && (m_sourceDirectories.Count > 0)
                    && (!m_overrideDefaultOutputDirectoryBehavior || !String.IsNullOrEmpty( m_outputDirectory ) )
                    && (!m_unifyHelpFiles || (!String.IsNullOrEmpty( m_unifiedHelpFileName) && !String.IsNullOrEmpty( m_masterDoxTemplateFile )));
            }
        }

        [XmlIgnoreAttribute]
        public bool CanInstallHelp
        {
            get
            {
                return m_outputHelp2
                    && !String.IsNullOrEmpty( m_help2RegExecutableFile )
                    && !String.IsNullOrEmpty( m_treeNamespace )
                    && !String.IsNullOrEmpty( m_namespaceDescription ) && !String.IsNullOrEmpty( m_hxaAttributeProjectName ) && !String.IsNullOrEmpty( m_hxaAttributeProjectValue )
                    && !String.IsNullOrEmpty( m_companyDescription ) && !String.IsNullOrEmpty( m_hxaAttributeCompanyName ) && !String.IsNullOrEmpty( m_hxaAttributeCompanyValue )                                        
                    && (m_sourceDirectories.Count > 0)
                    && (!m_overrideDefaultOutputDirectoryBehavior || !String.IsNullOrEmpty( m_outputDirectory ))
                    && (!m_unifyHelpFiles || (!String.IsNullOrEmpty( m_unifiedHelpFileName ) && !String.IsNullOrEmpty( m_masterDoxTemplateFile )));
            }
        }

        [XmlIgnoreAttribute]
        public bool CanExtractQAWarnings
        {
            get
            {
                return this.CanGenerateHelp
                    && !String.IsNullOrEmpty( m_doxTemplateFile )
                    && !String.IsNullOrEmpty( m_qaExecutable );
            }
        }
        #endregion

        #region Public Functions
        public string GetSourceControlFileName( string filename, bool fullname )
        {
            if ( (this.SourceControlServer == string.Empty) || (this.SourceControlProject == string.Empty) )
            {
                return string.Empty;
            }
			
			return filename;
        }

        public ProjectSettings Clone()
        {
            ProjectSettings settings = new ProjectSettings();
            
            this.Clone( settings );

            return settings;
        }

        public void Clone( ProjectSettings settings )
        {
            settings.CompanyDescription = this.CompanyDescription;
            settings.DoxTemplateFileRaw = this.DoxTemplateFileRaw;
            settings.DtxTemplateFileRaw = this.DtxTemplateFileRaw;
            settings.HaltOnError = this.HaltOnError;
            settings.Help2RegExecutableFileRaw = this.Help2RegExecutableFileRaw;
            settings.HxaAttributeCompanyName = this.HxaAttributeCompanyName;
            settings.HxaAttributeProjectName = this.HxaAttributeProjectName;
            settings.HxaAttributeCompanyValue = this.HxaAttributeCompanyValue;
            settings.HxaAttributeProjectValue = this.HxaAttributeProjectValue;
            settings.HxaLinkGroup = this.HxaLinkGroup;
            settings.KeepIntermediateFiles = this.KeepIntermediateFiles;
            settings.MasterDoxTemplateFile = this.MasterDoxTemplateFileRaw;
            settings.ProjectDescription = this.ProjectDescription;
            settings.OriginalProjectFilename = this.OriginalProjectFilename;
            settings.OutputHelp2 = this.OutputHelp2;
            settings.OutputHtml = this.OutputHtml;
            settings.OutputHtmlHelp = this.OutputHtmlHelp;
            settings.OutputOverviewHelpFiles = this.OutputOverviewHelpFiles;
            settings.OutputPdf = this.OutputPdf;
            settings.OutputWindowsHelp = this.OutputWindowsHelp;
            settings.OutputXml = this.OutputXml;
            settings.OverrideDefaultOutputDirectoryBehavior = this.OverrideDefaultOutputDirectoryBehavior;
            settings.OutputDirectoryRaw = this.OutputDirectoryRaw;
            settings.OverviewDoxTemplateFileRaw = this.OverviewDoxTemplateFileRaw;
            settings.QaExecutable = this.QaExecutable;
            settings.Rebuild = this.Rebuild;
            settings.ShowWhenFinishedBuilding = this.ShowWhenFinishedBuilding;
            settings.SourceControlEnabled = this.SourceControlEnabled;
            settings.SourceControlLoginName = this.SourceControlLoginName;
            settings.SourceControlProject = this.SourceControlProject;
            settings.SourceControlProvider = this.SourceControlProvider;
            settings.SourceControlServer = this.SourceControlServer;
            settings.SourceDirectoriesListRaw = this.SourceDirectoriesListRaw;
            settings.SourceDirectoryTreesList = this.SourceDirectoryTreesList;
            settings.TreeNamespace = this.TreeNamespace;
            settings.UnifyHelpFiles = this.UnifyHelpFiles;
            settings.UnifiedHelpFileName = this.UnifiedHelpFileName;
            settings.VerboseLevel = this.VerboseLevel;
        }

        public void Clear()
        {
            this.CompanyDescription = null;
            this.DoxTemplateFileRaw = null;
            this.DtxTemplateFileRaw = null;
            this.HaltOnError = false;
            this.Help2RegExecutableFileRaw = null;
            this.HxaAttributeCompanyName = null;
            this.HxaAttributeProjectName = null;
            this.HxaAttributeCompanyValue = null;
            this.HxaAttributeProjectValue = null;
            this.HxaLinkGroup = null;
            this.KeepIntermediateFiles = false;
            this.MasterDoxTemplateFile = null;
            this.ProjectDescription = null;
            this.OriginalProjectFilename = null;
            this.OutputHelp2 = false;
            this.OutputHtml = false;
            this.OutputHtmlHelp = false;
            this.OutputOverviewHelpFiles = false;
            this.OutputPdf = false;
            this.OutputWindowsHelp = false;
            this.OutputXml = false;
            this.OverrideDefaultOutputDirectoryBehavior = false;
            this.OutputDirectory = null;
            this.OverviewDoxTemplateFileRaw = null;
            this.ProjectFilename = null;
            this.QaExecutable = null;
            this.Rebuild = false;
            this.ShowWhenFinishedBuilding = false;
            this.SourceControlEnabled = false;
            this.SourceControlLoginName = null;
            this.SourceControlProject = null;
            this.SourceControlProvider = Provider.Perforce;
            this.SourceControlServer = null;
            this.SourceDirectoriesListRaw = null;
            this.SourceDirectoryTreesList = null;
            this.TreeNamespace = null;
            this.UnifyHelpFiles = false;
            this.UnifiedHelpFileName = null;
            this.VerboseLevel = 1;

            // try to auto-fill some values.
            string directory = Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location );
            if ( directory != null )
            {
                string filename = Path.Combine( directory, "templates" + Path.DirectorySeparatorChar + "template.dtx" );
                if ( File.Exists( filename ) )
                {
                    this.DtxTemplateFile = filename;
                }

                filename = Path.Combine( directory, "templates" + Path.DirectorySeparatorChar + "template_help2.dox" );
                if ( File.Exists( filename ) )
                {
                    this.DoxTemplateFile = filename;
                }

                filename = Path.Combine( directory, "templates" + Path.DirectorySeparatorChar + "template_section_help2.dox" );
                if ( File.Exists( filename ) )
                {
                    this.OverviewDoxTemplateFile = filename;
                }

                filename = Path.Combine( directory, "templates" + Path.DirectorySeparatorChar + "template_master.dox" );
                if ( File.Exists( filename ) )
                {
                    this.MasterDoxTemplateFile = filename;
                }

                filename = Path.Combine( directory, "h2reg.exe" );
                if ( File.Exists( filename ) )
                {
                    this.Help2RegExecutableFile = filename;
                }

                filename = Path.Combine( directory, "docqa.exe" );
                if ( File.Exists( filename ) )
                {
                    this.QaExecutable = filename;
                }
            }

            m_sourceDirectoryTrees.Add( "src" );
        }

        public bool SaveFile()
        {
            return SaveFile( this.ProjectFilename );
        }

        public bool SaveFile( string filename )
        {
            TextWriter writer = null;

            this.OriginalProjectFilename = filename;

            try
            {
                writer = new StreamWriter( filename );
                XmlSerializer serializer = new XmlSerializer( this.GetType() );
                serializer.Serialize( writer, this );
            }
            catch ( Exception e )
            {
                Console.WriteLine( "Error serializing '{0}': {1}", filename, e.Message );
                return false;
            }
            finally
            {
                if ( writer != null )
                {
                    writer.Close();
                }
            }

            return true;
        }

        public bool LoadFile( string filename )
        {
            FileStream stream = null;

            try
            {
                stream = new FileStream( filename, FileMode.Open, FileAccess.Read );
                
                XmlSerializer serializer = new XmlSerializer( this.GetType() );
                ProjectSettings settings = serializer.Deserialize( stream ) as ProjectSettings;
                if ( settings == null )
                {
                    Console.WriteLine( "Unable to deserialize '{0}'", filename );
                    return false;
                }

                this.ProjectFilename = filename;
                settings.Clone( this );
            }
            catch ( Exception e )
            {
                Console.WriteLine( "Error deserializing '{0}': {1}", filename, e.Message );
                return false;
            }
            finally
            {
                if ( stream != null )
                {
                    stream.Close();
                }
            }

            return true;
        }
        #endregion

        #region Public Static Functions
        public static string FindDocDirectory( string directory )
        {
            string[] files = Directory.GetFiles( directory, "*.vcproj" );
            if ( files.Length == 0 )
            {
                string[] dirs = Directory.GetDirectories( directory );
                foreach ( string dir in dirs )
                {
                    if ( dir.ToLower().EndsWith( Path.DirectorySeparatorChar + "doc" ) )
                    {
                        return dir;
                    }
                }
            }

            int indexOf = directory.LastIndexOf( Path.DirectorySeparatorChar );
            while ( indexOf > -1 )
            {
                string parentDirectory = directory.Substring( 0, indexOf );
                if ( parentDirectory.ToLower().EndsWith( Path.DirectorySeparatorChar + "doc" ) )
                {
                    return parentDirectory;
                }

                string[] dirs = Directory.GetDirectories( parentDirectory );
                foreach ( string dir in dirs )
                {
                    if ( dir.ToLower().EndsWith( Path.DirectorySeparatorChar + "doc" ) )
                    {
                        return dir;
                    }
                }

                indexOf = parentDirectory.LastIndexOf( Path.DirectorySeparatorChar );
            }

            return null;
        }

        public static string GetRelativePath( string path, string file )
        {
            if ( String.IsNullOrEmpty( file ) )
            {
                return string.Empty;
            }

            if ( Path.GetPathRoot( path ).ToLower() != Path.GetPathRoot( file ).ToLower() )
            {
                return file;
            }

            // break each down into their folders
            string[] pathSplit = path.Split( new char[] { '\\' } );
            string[] fileSplit = file.Split( new char[] { '\\' } );

            // eliminate the common root
            int i = 0;
            int j = 0;
            while ( (i < pathSplit.Length) && (j < fileSplit.Length - 1) && (pathSplit[i].ToLower() == fileSplit[j].ToLower()) )
            {
                ++i;
                ++j;
            }

            StringBuilder relativeFile = new StringBuilder();

            // for each remaining level in the path, add a ..
            for ( ; i < pathSplit.Length; ++i )
            {
                relativeFile.Append( ".." );
                relativeFile.Append( '\\' );
            }

            // for each level in the file path, add the path
            for ( ; j < fileSplit.Length - 1; ++j )
            {
                relativeFile.Append( fileSplit[j] );
                relativeFile.Append( '\\' );
            }

            // add the file name
            relativeFile.Append( fileSplit[j] );

            return relativeFile.ToString();
        }

        public static string GetAbsolutePath( string originalPath, string path, string file )
        {
            if ( originalPath != null )
            {
                string[] originalPathSplit = originalPath.Split( new char[] { Path.DirectorySeparatorChar } );
                string[] fileSplit = file.Split( new char[] { Path.DirectorySeparatorChar } );

                int relativeCount = 0;
                for ( int i = 0; i < fileSplit.Length; ++i )
                {
                    if ( fileSplit[i] == ".." )
                    {
                        ++relativeCount;
                    }
                    else
                    {
                        break;
                    }
                }

                if ( relativeCount == originalPathSplit.Length - 1 )
                {
                    return Path.GetFullPath( Path.Combine( originalPath, file ) );
                }
            }

            return Path.GetFullPath( Path.Combine( path, file ) );
        }
        #endregion
    }
}
