//#define ADD_MODULE
//#define ADD_TOPIC_FOR_MODULE
//#define ADD_CLASS_HIERARCHY
//#define DISABLE_FILES_TOPIC
//#define CREATE_HXQ_AND_HXR_FILES
#define DISABLE_VCPROJ_FILE_TOPICS
#define INCLUDE_VCPROJ_IN_SOURCE_FILES

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;
using Microsoft.Win32;

using RSG.Base.Command;
using RSG.Base.IO;
using RSG.Base.SCM;

namespace HelpGeneratorCore
{
    public class Generator
    {
        public Generator( ProjectSettings settings )
        {
            m_projectSettings = settings;
        }

        #region Enums
        private enum HelpType
        {
            html,
            chm,
            help2,
            pdf,
            rtf,
            xml
        }

        private enum SampleState
        {
            FindTitle,
            FindPurpose,
            FindStep,
            InStep,
            InCode
        }
        #endregion

        #region Variables
        private ProjectSettings m_projectSettings;
        private Form m_parentForm;

        private bool m_cancel = false;

        private List<string> m_itemsToDelete = new List<string>();
        private List<string> m_itemsToForceRevert = new List<string>();
        private List<string> m_itemsToRevert = new List<string>();
        private Dictionary<string, string> m_itemsToOverwrite = new Dictionary<string, string>();

        private Regex m_regexSampleCommentStart = new Regex( @"\A\s*//", RegexOptions.Compiled );
        private Regex m_regexSampleTitle = new Regex( @"\A\s*//\s*TITLE:", RegexOptions.Compiled );
        private Regex m_regexSamplePurpose = new Regex( @"\A\s*//\s*PURPOSE:", RegexOptions.Compiled );
        private Regex m_regexSampleStep = new Regex( @"\A\s*//\s*STEP\s*#\s*(?<Num>[0-9]+)\.", RegexOptions.Compiled ); // "// STEP #<num.":
        private Regex m_regexSampleDashes = new Regex( @"\A\s*//\s*--\s*", RegexOptions.Compiled ); // "// -- ":
        private Regex m_regexSampleStop = new Regex( @"\A\s*//\s*-STOP\s*", RegexOptions.Compiled );
        private Regex m_regexSampleBackslashEscape = new Regex( @"\\(?!/)", RegexOptions.Compiled );    // Replaces \ with \\, unless the \ is being used to escape a / in an XML close tag "<\/something>"

        private BuildResults m_buildResults;

        const HelpType c_QaHelpKind = HelpType.chm;
        #endregion

        #region Properties
        public OuputMessageEventHandler OutputMessage;

        public bool Cancel
        {
            set
            {
                m_cancel = value;
            }
        }
        #endregion

        #region Public Functions
        public BuildResults GenerateHelpFiles( Form parentForm )
        {
            m_parentForm = parentForm;

            List<HelpType> helpTypes = new List<HelpType>();
            if ( m_projectSettings.OutputHelp2 )
            {
                helpTypes.Add( HelpType.help2 );
            }

            if ( m_projectSettings.OutputHtml )
            {
                helpTypes.Add( HelpType.html );
            }

            if ( m_projectSettings.OutputHtmlHelp )
            {
                helpTypes.Add( HelpType.chm );
            }

            if ( m_projectSettings.OutputPdf )
            {
                helpTypes.Add( HelpType.pdf );
            }

            if ( m_projectSettings.OutputWindowsHelp )
            {
                helpTypes.Add( HelpType.rtf );
            }

            if ( m_projectSettings.OutputXml )
            {
                helpTypes.Add( HelpType.xml );
            }

            m_buildResults = new BuildResults( helpTypes.Count );

            // get directories to process
            SortedDictionary<string, SortedDictionary<string, string>> docDirDict = GetModulesAndDocDirectories();
            if ( docDirDict == null )
            {
                return m_buildResults;
            }

            if ( m_projectSettings.UnifyHelpFiles )
            {
                if ( m_projectSettings.OverrideDefaultOutputDirectoryBehavior )
                {
                    GenerateSingleUnifiedHelpFile( helpTypes, docDirDict );
                }
                else
                {
                    GenerateUnifiedHelpFiles( helpTypes, docDirDict, true );
                }
            }
            else
            {
                GenerateSeparateHelpFiles( helpTypes, docDirDict );
            }

            // clean up intermediate files
            if ( !m_projectSettings.KeepIntermediateFiles )
            {
                CleanupIntermediateFiles( 0 );
            }

            if ( m_cancel )
            {
                OnOutputMessage( "Cancelled.\r\n", 0 );
            }
            else
            {
                OnOutputMessage( "Complete (" + m_buildResults.BuildsFailed + " failed, " + m_buildResults.BuildsSucceeded + " built, " + m_buildResults.BuildsSkipped + " skipped).\r\n", 0 );
            }

            return m_buildResults;
        }

        public BuildResults InstallHelpFiles( Form parentForm )
        {
            m_buildResults = new BuildResults( 1 );
            m_parentForm = parentForm;

            SortedDictionary<string, SortedDictionary<string, string>> docDirDict = GetModulesAndDocDirectories();
            if ( docDirDict == null )
            {
                return m_buildResults;
            }

            if ( m_projectSettings.UnifyHelpFiles )
            {
                if ( m_projectSettings.OverrideDefaultOutputDirectoryBehavior )
                {
                    InstallSingleUnifiedHelp2File( m_projectSettings.OutputDirectory, docDirDict );
                }
                else
                {
                    InstallUnifiedHelp2File( docDirDict );
                }
            }
            else
            {
                List<string> docDirectories = new List<string>();
                foreach ( string docDirectory in docDirDict.Keys )
                {
                    docDirectories.Add( docDirectory );
                }

                InstallHelp2File( docDirectories );
            }

            // clean up intermediate files
            if ( !m_projectSettings.KeepIntermediateFiles )
            {
                CleanupIntermediateFiles( 0 );
            }

            if ( m_cancel )
            {
                OnOutputMessage( "Cancelled.\r\n", 0 );
            }
            else
            {
                OnOutputMessage( "Complete (" + m_buildResults.BuildsFailed + " failed, " + m_buildResults.BuildsSucceeded + " installed, " + m_buildResults.BuildsSkipped + " skipped).\r\n", 0 );
            }

            return m_buildResults;
        }

        public BuildResults UninstallHelpFiles( Form parentForm )
        {
            m_buildResults = new BuildResults( 1 );
            m_parentForm = parentForm;

            SortedDictionary<string, SortedDictionary<string, string>> docDirDict = GetModulesAndDocDirectories();
            if ( docDirDict == null )
            {
                return m_buildResults;
            }

            List<string> docDirectories = new List<string>();
            foreach ( string docDirectory in docDirDict.Keys )
            {
                docDirectories.Add( docDirectory );
            }

            if ( m_projectSettings.UnifyHelpFiles )
            {
                if ( m_projectSettings.OverrideDefaultOutputDirectoryBehavior )
                {
                    UnInstallSingleUnifiedHelp2File( m_projectSettings.OutputDirectory );
                }
                else
                {
                    UnInstallUnifiedHelp2File( docDirectories );
                }
            }
            else
            {
                UnInstallHelp2File( docDirectories );
            }

            // clean up intermediate files
            if ( !m_projectSettings.KeepIntermediateFiles )
            {
                CleanupIntermediateFiles( 0 );
            }

            if ( m_cancel )
            {
                OnOutputMessage( "Cancelled.\r\n", 0 );
            }
            else
            {
                OnOutputMessage( "Complete (" + m_buildResults.BuildsFailed + " failed, " + m_buildResults.BuildsSucceeded + " uninstalled, " + m_buildResults.BuildsSkipped + " skipped).\r\n", 0 );
            }

            return m_buildResults;
        }

        public BuildResults ExtractQAWarningsFromHelpFiles( Form parentForm )
        {
            m_parentForm = parentForm;

            m_buildResults = new BuildResults( 1 );

            // get the directories to process along with their doc directories
            SortedDictionary<string, SortedDictionary<string, string>> docDirDict = new SortedDictionary<string, SortedDictionary<string, string>>();
            foreach ( string directory in m_projectSettings.SourceDirectoriesList )
            {
                if ( m_cancel )
                {
                    break;
                }

                if ( !GetDirectoriesToProcess( directory, docDirDict, true, true, 0 ) )
                {
                    if ( m_projectSettings.HaltOnError )
                    {
                        return m_buildResults;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_projectSettings.OutputOverviewHelpFiles )
                {
                    string docDirectory = ProjectSettings.FindDocDirectory( directory );
                    if ( (docDirectory != null) && Directory.Exists( docDirectory ) )
                    {
                        string[] dirs = Directory.GetDirectories( docDirectory, "overview_*" );
                        foreach ( string dir in dirs )
                        {
                            if ( m_cancel )
                            {
                                break;
                            }

                            SortedDictionary<string, string> dirDict = null;
                            if ( !docDirDict.TryGetValue( docDirectory.ToLower(), out dirDict ) )
                            {
                                dirDict = new SortedDictionary<string, string>();
                                docDirDict.Add( docDirectory.ToLower(), dirDict );
                            }

                            dirDict[dir.ToLower()] = dir;
                        }
                    }
                }
            }

            ExtractQAWarningsInDirectories( docDirDict );

            // clean up intermediate files
            if ( !m_projectSettings.KeepIntermediateFiles )
            {
                CleanupIntermediateFiles( 0 );
            }

            if ( m_cancel )
            {
                OnOutputMessage( "Cancelled.\r\n", 0 );
            }
            else
            {
                OnOutputMessage( "Complete (" + m_buildResults.BuildsFailed + " failed, " + m_buildResults.BuildsSucceeded + " succeeded, " + m_buildResults.BuildsSkipped + " skipped).\r\n", 0 );
            }

            return m_buildResults;
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Retrieves a sorted list of module directories, sorted by their associated doc directory
        /// </summary>
        /// <returns>A Sorted Dictionary of Sorted Dictionaries, <c>null</c> on error.</returns>
        public SortedDictionary<string, SortedDictionary<string, string>> GetModulesAndDocDirectories()
        {
            // get the directories to process along with their doc directories
            SortedDictionary<string, SortedDictionary<string, string>> docDirDict = new SortedDictionary<string, SortedDictionary<string, string>>();
            foreach ( string directory in m_projectSettings.SourceDirectoriesList )
            {
                if ( m_cancel )
                {
                    break;
                }

                if ( !GetDirectoriesToProcess( directory, docDirDict, true, true, 0 ) )
                {
                    if ( m_projectSettings.HaltOnError )
                    {
                        return null;
                    }
                }

                if ( m_projectSettings.OutputOverviewHelpFiles )
                {
                    string docDirectory = ProjectSettings.FindDocDirectory( directory );
                    if ( (docDirectory != null) && Directory.Exists( docDirectory ) )
                    {
                        string[] dirs = Directory.GetDirectories( docDirectory, "overview_*" );
                        foreach ( string dir in dirs )
                        {
                            if ( m_cancel )
                            {
                                break;
                            }

                            SortedDictionary<string, string> dirDict = null;
                            if ( !docDirDict.TryGetValue( docDirectory.ToLower(), out dirDict ) )
                            {
                                dirDict = new SortedDictionary<string, string>();
                                docDirDict.Add( docDirectory.ToLower(), dirDict );
                            }

                            dirDict[dir.ToLower()] = dir;
                        }
                    }
                }
            }

            return docDirDict;
        }

        /// <summary>
        /// Searches the directory for a vcproj file that we can use to generate help, and/or uses 
        /// <see cref="ProjectSettings"/>.<see cref="SourceDirectoryTreesList"/> to recursively
        /// locate vcproj files below directory.
        /// </summary>
        /// <param name="directory">The directory to search</param>
        /// <param name="docDirDict">A <see cref="SortedDictionary"/> to store the directories to be processed, sorted by docDirectory.</param>
        /// <param name="useTrees">When <c>true</c>, use <see cref="ProjectSettings"/>.<see cref="SourceDirectoryTreesList"/> to search any folders below directory.</param>
        /// <param name="recurse">When <c>true</c>, recusively search directory.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success.</returns>
        private bool GetDirectoriesToProcess( string directory, SortedDictionary<string, SortedDictionary<string, string>> docDirDict,
            bool useTrees, bool recurse, int depth )
        {
            if ( !Directory.Exists( directory ) )
            {
                OnOutputMessage( "Error: Source Directory '" + directory + "' does not exist.\r\n", depth );

                ++m_buildResults.BuildsFailed;
                return !m_projectSettings.HaltOnError;
            }

            // see if we're in a project directory or an overview
            bool isOverview = false;
            string name = GetModuleName( directory, out isOverview );
            string vcprojFile = GetVcprojFilename( name, directory );

            if ( (vcprojFile != null) || isOverview )
            {
                string docDirectory = ProjectSettings.FindDocDirectory( directory );
                if ( docDirectory != null )
                {
                    SortedDictionary<string, string> dirDict = null;
                    if ( !docDirDict.TryGetValue( docDirectory.ToLower(), out dirDict ) )
                    {
                        dirDict = new SortedDictionary<string, string>();
                        docDirDict.Add( docDirectory.ToLower(), dirDict );
                    }

                    dirDict[directory.ToLower()] = directory;
                    return true;
                }
                else
                {
                    OnOutputMessage( "Error: Could not locate doc directory for '" + directory + "'.\r\n", depth );

                    ++m_buildResults.BuildsFailed;
                    return !m_projectSettings.HaltOnError;
                }
            }
            else
            {
                bool success = true;

                if ( useTrees )
                {
                    string[] trees = m_projectSettings.SourceDirectoryTreesList;
                    foreach ( string tree in trees )
                    {
                        if ( m_cancel )
                        {
                            return false;
                        }

                        string subDirectory = Path.Combine( directory, tree );
                        if ( Directory.Exists( subDirectory ) )
                        {
                            // process all directories under the tree
                            success = GetDirectoriesToProcess( subDirectory, docDirDict, false, true, depth );
                            if ( !success && m_projectSettings.HaltOnError )
                            {
                                return false;
                            }
                        }
                    }
                }

                if ( recurse )
                {
                    string[] subDirectories = Directory.GetDirectories( directory );
                    foreach ( string subDirectory in subDirectories )
                    {
                        if ( m_cancel )
                        {
                            return false;
                        }

                        success = GetDirectoriesToProcess( subDirectory, docDirDict, false, false, depth );
                        if ( !success && m_projectSettings.HaltOnError )
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Parses the name from the directory.  This is used to build the dtx and dox file names, as well as output file names.
        /// </summary>
        /// <param name="directory">The directory name to parse.</param>
        /// <param name="isOverview">Returns <c>true</c> if the name starts with "overview_", otherwise, <c>false</c>.</param>
        /// <returns>The <c>string</c> name.</returns>
        private string GetModuleName( string directory, out bool isOverview )
        {
            // find base directory name
            string[] directorySplit = directory.Split( new char[] { Path.DirectorySeparatorChar } );
            string name = directorySplit[directorySplit.Length - 1];

            // determine if this is an overview directory
            isOverview = name.StartsWith( "overview_" );

            return name;
        }

        /// <summary>
        /// Tries to determine which branch of code the directory resides in by first locating the "doc" directory, then
        /// returning the name of the directory that contains it.
        /// </summary>
        /// <param name="directory">The directory name to parse.</param>
        /// <returns>The <c>string</c> name, String.Empty if nothing could be determined.</returns>
        private string GetBranchName( string directory )
        {
            string docDirectory = ProjectSettings.FindDocDirectory( directory );
            string[] split = directory.ToLower().Split( new char[] { Path.DirectorySeparatorChar } );
            if ( split.Length > 2 )
            {
                return split[split.Length - 2];
            }

            return string.Empty;
        }

        /// <summary>
        /// Looks for a vcproj file in the given directory matching the given name
        /// </summary>
        /// <param name="name">The name of the vcproj file.</param>
        /// <param name="directory">The directory to look in.</param>
        /// <returns>The vcproj file, null if not found.</returns>
        private string GetVcprojFilename( string name, string directory )
        {
            string[] extensions = { "_2010.vcproj", "_2008.vcproj", "_2005.vcproj", ".vcproj" };

            foreach (string extn in extensions)
            {
                string[] vcprojFiles = Directory.GetFiles(directory, name + extn);
                if (vcprojFiles.Length > 0)
                {
                    return vcprojFiles[0];
                }
            }
            return null;
        }

        /// <summary>
        /// For each <see cref="HelpType"/> and for each directory, builds the help.
        /// </summary>
        /// <param name="helpTypes">The list of <see cref="HelpType"/>s to build.</param>
        /// <param name="docDirDict">The dictionary of docDirectories and directories to build.</param>
        /// <remarks>If <see cref="HelpType"/>.<see cref="Html"/> is included in the list, an index file is generated in the html destination folder.</remarks>
        private void GenerateSeparateHelpFiles( List<HelpType> helpTypes, SortedDictionary<string, SortedDictionary<string, string>> docDirDict )
        {
            // go through all of the docDirectories
            foreach ( KeyValuePair<string, SortedDictionary<string, string>> pair in docDirDict )
            {
                // determine the destination directory
                string destinationDirectory = string.Empty;
                if ( m_projectSettings.OverrideDefaultOutputDirectoryBehavior )
                {
                    destinationDirectory = m_projectSettings.OutputDirectory;
                }
                else
                {
                    destinationDirectory = pair.Key;
                }

                if ( !Directory.Exists( destinationDirectory ) )
                {
                    OnOutputMessage( "Error: The destination directory '" + destinationDirectory + "' doesn't exist.\r\n", 0 );

                    m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                bool success = true;

                // go through all of the directories
                foreach ( string directory in pair.Value.Values )
                {
                    if ( m_cancel )
                    {
                        break;
                    }
                    
                    success = ProcessSeparateDirectory( directory, destinationDirectory, helpTypes, 0 );
                    if ( !success )
                    {
                        OnOutputMessage( "Failure: " + directory + "\r\n", 1 );

                        if ( m_projectSettings.HaltOnError )
                        {
                            break;
                        }
                    }
                }
                
                if ( (!success && m_projectSettings.HaltOnError) || m_cancel )
                {
                    break;
                }

                // build an index file for html help
                if ( helpTypes.Contains( HelpType.html ) )
                {
                    if ( !CreateIndexHtmlFile( Path.Combine( destinationDirectory, HelpType.html.ToString() ), 0 ) 
                        && m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Extracts the QA Warnings for each directory in the dictionaries
        /// </summary>
        /// <param name="docDirDict">The dictionary of docDirectories and directories to extra warnings from.</param>
        private void ExtractQAWarningsInDirectories( SortedDictionary<string, SortedDictionary<string, string>> docDirDict )
        {
            // go through all of the docDirectories
            foreach ( KeyValuePair<string, SortedDictionary<string, string>> pair in docDirDict )
            {
                if ( !Directory.Exists( pair.Key ) )
                {
                    OnOutputMessage( "Error: The doc directory '" + pair.Key + "' doesn't exist.\r\n", 0 );

                    m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                bool success = true;

                // go through all of the directories
                foreach ( string directory in pair.Value.Values )
                {
                    if ( m_cancel )
                    {
                        break;
                    }

                    success = ProcessDirectoryForQAWarnings( directory, pair.Key );
                    if ( !success )
                    {
                        OnOutputMessage( "Failure: " + directory + "\r\n", 1 );

                        if ( m_projectSettings.HaltOnError )
                        {
                            break;
                        }
                    }
                }

                if ( (!success && m_projectSettings.HaltOnError) || m_cancel )
                {
                    break;
                }
            }
        }

        /// <summary>
        /// For each <see cref="HelpType"/> and for each directory, builds one unified set of help files for all doc directories
        /// </summary>
        /// <param name="helpTypes">The list of <see cref="HelpType"/>s to build.</param>
        /// <param name="docDirDict">The dictionary of docDirectories and directories to build.</param>
        private void GenerateSingleUnifiedHelpFile( List<HelpType> helpTypes, SortedDictionary<string, SortedDictionary<string, string>> docDirDict )
        {
            // The Chm and Help2 Help Compilers can handle only so much, so we may have to do multiple Unified Help Files, and copy
            // then to the output directory.
            List<HelpType> mergedHelpTypes = new List<HelpType>();
            if ( helpTypes.Contains( HelpType.chm ) )
            {
                mergedHelpTypes.Add( HelpType.chm );
                helpTypes.Remove( HelpType.chm );
            }

            if ( helpTypes.Contains( HelpType.help2 ) )
            {
                mergedHelpTypes.Add( HelpType.help2 );
                helpTypes.Remove( HelpType.help2 );
            }

            if ( mergedHelpTypes.Count > 0 )
            {
                GenerateSingleUnifiedMergedHelpFile( mergedHelpTypes, docDirDict );

                if ( helpTypes.Count == 0 )
                {
                    return;
                }
            }

            string doxFilename = Path.GetFullPath( Path.Combine( m_projectSettings.OutputDirectory, m_projectSettings.UnifiedHelpFileName + ".dox" ) );
            bool didNotExist;
            if ( !OpenFileForEdit( doxFilename, m_projectSettings.MasterDoxTemplateFile, true, true, 0, out didNotExist ) )
            {
                return;
            }

            if ( m_cancel )
            {
                return;
            }

            if ( didNotExist && !FixupRelativePathsInDox( m_projectSettings.MasterDoxTemplateFile, doxFilename, 0 ) )
            {
                return;
            }

            if ( m_cancel )
            {
                return;
            }

            DoxInfo dInfo = ReadDoxFile( doxFilename, 0 );
            if ( dInfo == null )
            {
                return;
            }

            if ( m_cancel )
            {
                return;
            }

            OnOutputMessage( "Getting unified layout hierarchy:\r\n", 0 );

            // go through all of the docDirectories 
            HelpLayout masterLayout = new HelpLayout();
            foreach ( KeyValuePair<string, SortedDictionary<string, string>> pair in docDirDict )
            {
                if ( m_cancel )
                {
                    break;
                }

                // make sure the doc directory exists
                if ( !Directory.Exists( pair.Key ) )
                {
                    OnOutputMessage( "Error: The doc directory '" + pair.Key + "' doesn't exist.\r\n", 1 );

                    m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                // get the HelpLayout for this docDirectory
                Dictionary<string, string> moduleNames = new Dictionary<string, string>();
                foreach ( string directory in pair.Value.Values )
                {
                    bool isOverview;
                    string moduleName = GetModuleName( directory, out isOverview );
                    moduleNames.Add( moduleName, moduleName );
                }

                HelpLayout layout = GetHelpLayout( pair.Key, moduleNames, docDirDict.Count > 1, 1 );
                if ( layout == null )
                {
                    m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                // merge with our master layout
                foreach ( HelpLayoutItem item in layout.Items )
                {
                    masterLayout.Items.Add( item );
                }
            }

            if ( m_cancel )
            {
                return;
            }

            // create the unified dtx file which will define our layout hierarchy
            Dictionary<string, string> topicIDDict = new Dictionary<string, string>();
            Dictionary<string, string> moduleToTopicIDDict = new Dictionary<string, string>();

            string unifiedDtxFilename = Path.Combine( m_projectSettings.OutputDirectory, m_projectSettings.UnifiedHelpFileName + ".dtx" );
            if ( !CreateUnifiedDtx( unifiedDtxFilename, masterLayout, topicIDDict, moduleToTopicIDDict, 1 ) )
            {
                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            OnOutputMessage( "Complete.\r\n", 1 );

            if ( m_cancel )
            {
                return;
            }

            List<string> sourceFiles = new List<string>();
            sourceFiles.Add( unifiedDtxFilename );
           
            // process each directory
            bool success = true;
            int moduleNumber = 0;
            List<string> disabledTopics = new List<string>();
            foreach ( KeyValuePair<string, SortedDictionary<string, string>> pair in docDirDict )
            {
                // go through all of the directories
                foreach ( string directory in pair.Value.Values )
                {
                    if ( m_cancel )
                    {
                        break;
                    }

                    OnOutputMessage( "Processing '" + directory + "...\r\n", 0 );

                    bool isOverview;
                    string moduleName = GetModuleName( directory, out isOverview );

                    string groupTopicID = null;
                    if ( !moduleToTopicIDDict.TryGetValue( moduleName, out groupTopicID ) )
                    {
                        OnOutputMessage( "Warning: Missing Group TopicID for '" + moduleName + "'.\r\n", 1 );
                    }

                    success = ProcessUnifiedDirectory( directory, dInfo, moduleNumber, sourceFiles, disabledTopics,
                        topicIDDict, groupTopicID, 1 );
                    if ( !success )
                    {
                        OnOutputMessage( "Failure: " + directory + "\r\n", 1 );

                        if ( m_projectSettings.HaltOnError )
                        {
                            break;
                        }
                    }

                    ++moduleNumber;

                    OnOutputMessage( "Complete.\r\n", 1 );
                }

                if ( (!success && m_projectSettings.HaltOnError) || m_cancel )
                {
                    break;
                }
            }

            if ( (!success && m_projectSettings.HaltOnError) || m_cancel )
            {
                return;
            }

            // modify the dox file
            if ( !EditUnifiedDoxFile( dInfo, moduleNumber, sourceFiles, 0 ) )
            {
                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            // save the file.  This will be our template file for the Doc-o-matic build process
            if ( !OverwriteDoxFile( dInfo, doxFilename, 0 ) )
            {
                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            // build the help files for each HelpType
            if ( !BuildUnifiedHelpFiles( m_projectSettings.UnifiedHelpFileName, helpTypes, m_projectSettings.OutputDirectory, 
                disabledTopics, doxFilename, true, 0 ) )
            {
                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }
        }

        /// <summary>
        /// Creates a single unified chm and/or help2 help file.
        /// </summary>
        /// <param name="docDirDict">The dictionary of docDirectories and directories to build.</param>
        private void GenerateSingleUnifiedMergedHelpFile( List<HelpType> helpTypes, 
            SortedDictionary<string, SortedDictionary<string, string>> docDirDict )
        {
            GenerateHelpFilesForSingleUnifiedMerging( helpTypes, docDirDict );

            if ( helpTypes.Contains( HelpType.chm ) )
            {
                // build the merged chm file
                HelpLayout masterLayout = new HelpLayout();
                foreach ( KeyValuePair<string, SortedDictionary<string, string>> pair in docDirDict )
                {
                    bool isOverview;

                    // get the HelpLayout for this docDirectory
                    Dictionary<string, string> moduleNames = new Dictionary<string, string>();
                    foreach ( string directory in pair.Value.Values )
                    {
                        string moduleName = GetModuleName( directory, out isOverview );
                        moduleNames.Add( moduleName, moduleName );
                    }

                    HelpLayout layout = GetHelpLayout( pair.Key, moduleNames, docDirDict.Count > 1, 0 );
                    if ( layout == null )
                    {
                        m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                        if ( m_projectSettings.HaltOnError )
                        {
                            break;
                        }
                        else
                        {
                            continue;
                        }
                    }

                    // merge with our master layout
                    foreach ( HelpLayoutItem item in layout.Items )
                    {
                        masterLayout.Items.Add( item );
                    }
                }

                string destinationDirectory = Path.Combine( m_projectSettings.OutputDirectory, "chm" );

                string hhpFilename = Path.Combine( destinationDirectory, "master.hhp" );
                string compiledFilename = Path.Combine( destinationDirectory, m_projectSettings.UnifiedHelpFileName + ".chm" );

                // create the files required to build the master chm
                if ( !CreateMasterChmFiles( hhpFilename, compiledFilename, masterLayout, 0 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return;
                    }
                }

                // open the master help file for edit
                bool didNotExist = false;
                if ( !OpenFileForEdit( compiledFilename, null, false, false, 0, out didNotExist ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return;
                    }
                }

                // compile the master help file
                if ( !BuildMasterChmFile( hhpFilename, compiledFilename, m_projectSettings.ShowWhenFinishedBuilding, 0 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return;
                    }
                }

                // add the master help file to source control
                if ( didNotExist )
                {
                    if ( !AddToSourceControl( compiledFilename, 0 ) )
                    {
                        if ( m_projectSettings.HaltOnError )
                        {
                            return;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// For each <see cref="HelpType"/> and for each directory, builds a set of unified help files for each doc directory.
        /// </summary>
        /// <param name="helpTypes">The list of <see cref="HelpType"/>s to build.</param>
        /// <param name="docDirDict">The dictionary of docDirectories and directories to build.</param>
        /// <param name="addOutputFilesToSourceControl"><c>true</c> or <c>false</c>.</param>
        private void GenerateUnifiedHelpFiles( List<HelpType> helpTypes, SortedDictionary<string, SortedDictionary<string, string>> docDirDict,
            bool addOutputFilesToSourceControl )
        {
            // The Chm and Help2 Help Compilers can handle only so much, so we may have to do multiple Unified Help Files, and copy
            // then to the output directory.
            List<HelpType> mergedHelpTypes = new List<HelpType>();
            if ( helpTypes.Contains( HelpType.chm ) )
            {
                mergedHelpTypes.Add( HelpType.chm );
                helpTypes.Remove( HelpType.chm );
            }

            if ( helpTypes.Contains( HelpType.help2 ) )
            {
                mergedHelpTypes.Add( HelpType.help2 );
                helpTypes.Remove( HelpType.help2 );
            }

            if ( mergedHelpTypes.Count > 0 )
            {
                GenerateUnifiedMergedHelpFile( mergedHelpTypes, docDirDict, addOutputFilesToSourceControl );

                if ( helpTypes.Count == 0 )
                {
                    return;
                }
            }

            // go through all of the docDirectories
            foreach ( KeyValuePair<string, SortedDictionary<string, string>> pair in docDirDict )
            {
                OnOutputMessage( "Processing doc directory '" + pair.Key + "'...\r\n", 0 );

                if ( !Directory.Exists( pair.Key ) )
                {
                    OnOutputMessage( "Error: The doc directory '" + pair.Key + "' doesn't exist.\r\n", 1 );

                    m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                // try to extract a suitable name
                string name = GetBranchName( pair.Key );
                if ( name != string.Empty )
                {
                    name = name + " " + m_projectSettings.UnifiedHelpFileName;
                }
                else
                {
                    name = m_projectSettings.UnifiedHelpFileName;
                }

                // open the master dox for edit
                string doxFilename = Path.Combine( pair.Key, name + ".dox" );
                bool didNotExist;
                if ( !OpenFileForEdit( doxFilename, m_projectSettings.MasterDoxTemplateFile, true, true, 1, out didNotExist ) )
                {
                    m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( didNotExist && !FixupRelativePathsInDox( m_projectSettings.MasterDoxTemplateFile, doxFilename, 1 ) )
                {
                    m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                // read in the master dox
                DoxInfo dInfo = ReadDoxFile( doxFilename, 1 );
                if ( dInfo == null )
                {
                    m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                OnOutputMessage( "Getting unified layout hierarchy:\r\n", 1 );

                // get the HelpLayout for this docDirectory
                Dictionary<string, string> moduleNames = new Dictionary<string, string>();
                foreach ( string directory in pair.Value.Values )
                {
                    bool isOverview;
                    string moduleName = GetModuleName( directory, out isOverview );
                    moduleNames.Add( moduleName, moduleName );
                }

                HelpLayout layout = GetHelpLayout( pair.Key, moduleNames, false, 2 );
                if ( layout == null )
                {
                    m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                // create the unified dtx file which will define our layout hierarchy
                Dictionary<string, string> topicIDDict = new Dictionary<string, string>();
                Dictionary<string, string> moduleToTopicIDDict = new Dictionary<string, string>();

                string unifiedDtxFilename = Path.Combine( pair.Key, name + " " + m_projectSettings.UnifiedHelpFileName + ".dtx" );
                if ( !CreateUnifiedDtx( unifiedDtxFilename, layout, topicIDDict, moduleToTopicIDDict, 2 ) )
                {
                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                OnOutputMessage( "Complete.\r\n", 2 );

                List<string> sourceFiles = new List<string>();
                sourceFiles.Add( unifiedDtxFilename );

                // go through all of the directories
                bool success = true;
                int moduleNumber = 0;
                List<string> disabledTopics = new List<string>();
                foreach ( string directory in pair.Value.Values )
                {
                    if ( m_cancel )
                    {
                        break;
                    }

                    bool isOverview;
                    string moduleName = GetModuleName( directory, out isOverview );

                    string groupTopicID = null;
                    if ( !moduleToTopicIDDict.TryGetValue( moduleName, out groupTopicID ) )
                    {
                        OnOutputMessage( "Warning: Missing Group TopicID for '" + moduleName + "'.\r\n", 1 );
                    }

                    success = ProcessUnifiedDirectory( directory, dInfo, moduleNumber, sourceFiles, disabledTopics,
                        topicIDDict, groupTopicID, 1 );
                    if ( !success )
                    {
                        OnOutputMessage( "Failure: " + directory + "\r\n", 2 );

                        if ( m_projectSettings.HaltOnError )
                        {
                            break;
                        }
                    }

                    ++moduleNumber;
                }

                if ( (!success && m_projectSettings.HaltOnError) || m_cancel )
                {
                    break;
                }

                // modify the dox file
                if ( !EditUnifiedDoxFile( dInfo, moduleNumber, sourceFiles, 1 ) )
                {
                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                // save the file.  This will be our template file for the Doc-o-matic build process
                if ( !OverwriteDoxFile( dInfo, doxFilename, 1 ) )
                {
                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                // build the help files for each HelpType
                if ( !BuildUnifiedHelpFiles( name, helpTypes, pair.Key, disabledTopics, doxFilename, addOutputFilesToSourceControl, 1 ) )
                {
                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
        }

        /// <summary>
        /// Creates a unified chm and/or help2 help file for each doc directory.
        /// </summary>
        /// <param name="helpTypes"></param>
        /// <param name="docDirDict"></param>
        /// <param name="addOutputFilesToSourceControl"></param>
        private void GenerateUnifiedMergedHelpFile( List<HelpType> helpTypes, SortedDictionary<string, SortedDictionary<string, string>> docDirDict, 
            bool addOutputFilesToSourceControl )
        {
            GenerateHelpFilesForUnifiedMerging( helpTypes, docDirDict );

            if ( helpTypes.Contains( HelpType.chm ) )
            {
                // build the merged chm file for each doc directory
                foreach ( KeyValuePair<string, SortedDictionary<string, string>> pair in docDirDict )
                {
                    bool isOverview;

                    // get the HelpLayout for this docDirectory
                    Dictionary<string, string> moduleNames = new Dictionary<string, string>();
                    foreach ( string directory in pair.Value.Values )
                    {
                        string moduleName = GetModuleName( directory, out isOverview );
                        moduleNames.Add( moduleName, moduleName );
                    }

                    HelpLayout layout = GetHelpLayout( pair.Key, moduleNames, docDirDict.Count > 1, 0 );
                    if ( layout == null )
                    {
                        m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                        if ( m_projectSettings.HaltOnError )
                        {
                            break;
                        }
                        else
                        {
                            continue;
                        }
                    }

                    // determine the destination directory
                    string destinationDirectory = string.Empty;
                    if ( m_projectSettings.OverrideDefaultOutputDirectoryBehavior )
                    {
                        destinationDirectory = m_projectSettings.OutputDirectory;
                    }
                    else
                    {
                        destinationDirectory = pair.Key;
                    }

                    destinationDirectory = Path.Combine( destinationDirectory, "chm" );

                    string name = GetBranchName( pair.Key );

                    string hhpFilename = Path.Combine( destinationDirectory, "master_" + name + ".hhp" );
                    string compiledFilename = Path.Combine( destinationDirectory, name + " " + m_projectSettings.UnifiedHelpFileName + ".chm" );

                    // create the files required to build the master chm
                    if ( !CreateMasterChmFiles( hhpFilename, compiledFilename, layout, 0 ) )
                    {
                        ++m_buildResults.BuildsFailed;

                        if ( m_projectSettings.HaltOnError )
                        {
                            return;
                        }
                    }

                    // open the master help file for edit
                    bool didNotExist = false;
                    if ( !OpenFileForEdit( compiledFilename, null, false, false, 0, out didNotExist ) )
                    {
                        ++m_buildResults.BuildsFailed;

                        if ( m_projectSettings.HaltOnError )
                        {
                            return;
                        }
                    }

                    // compile the master help file
                    if ( !BuildMasterChmFile( hhpFilename, compiledFilename, m_projectSettings.ShowWhenFinishedBuilding, 0 ) )
                    {
                        ++m_buildResults.BuildsFailed;

                        if ( m_projectSettings.HaltOnError )
                        {
                            return;
                        }
                    }

                    // add the master help file to source control
                    if ( didNotExist )
                    {
                        if ( !AddToSourceControl( compiledFilename, 0 ) )
                        {
                            if ( m_projectSettings.HaltOnError )
                            {
                                return;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Build the chm and/or help2 help files for single unified help.
        /// </summary>
        /// <param name="helpTypes"></param>
        /// <param name="docDirDict"></param>
        private void GenerateHelpFilesForSingleUnifiedMerging( List<HelpType> helpTypes, 
            SortedDictionary<string, SortedDictionary<string, string>> docDirDict )
        {
            Dictionary<string, string> topicIDDict = new Dictionary<string, string>();

            // go through all of the docDirectories
            foreach ( KeyValuePair<string, SortedDictionary<string, string>> pair in docDirDict )
            {
                // determine the destination directory
                string destinationDirectory = string.Empty;
                if ( m_projectSettings.OverrideDefaultOutputDirectoryBehavior )
                {
                    destinationDirectory = m_projectSettings.OutputDirectory;
                }
                else
                {
                    destinationDirectory = pair.Key;
                }

                if ( !Directory.Exists( destinationDirectory ) )
                {
                    OnOutputMessage( "Error: The destination directory '" + destinationDirectory + "' doesn't exist.\r\n", 0 );

                    m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( !GenerateHelpFilesForMerging( helpTypes, destinationDirectory, pair.Value, topicIDDict, true ) )
                {
                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Build the chm and/or help2 help files for unified help.
        /// </summary>
        /// <param name="helpTypes"></param>
        /// <param name="docDirDict"></param>
        private void GenerateHelpFilesForUnifiedMerging( List<HelpType> helpTypes, SortedDictionary<string, 
            SortedDictionary<string, string>> docDirDict )
        {
            // go through all of the docDirectories
            foreach ( KeyValuePair<string, SortedDictionary<string, string>> pair in docDirDict )
            {
                // determine the destination directory
                string destinationDirectory = string.Empty;
                if ( m_projectSettings.OverrideDefaultOutputDirectoryBehavior )
                {
                    destinationDirectory = m_projectSettings.OutputDirectory;
                }
                else
                {
                    destinationDirectory = pair.Key;
                }

                if ( !Directory.Exists( destinationDirectory ) )
                {
                    OnOutputMessage( "Error: The destination directory '" + destinationDirectory + "' doesn't exist.\r\n", 0 );

                    m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( !GenerateHelpFilesForMerging( helpTypes, destinationDirectory, pair.Value, new Dictionary<string, string>(), true ) )
                {
                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Build the chm and/or help2 files for single unified help or unified help.
        /// </summary>
        /// <param name="helpTypes"></param>
        /// <param name="destinationDirectory"></param>
        /// <param name="dirDict"></param>
        /// <param name="topicIDDict"></param>
        /// <param name="addOutputFilesToSourceControl"></param>
        /// <returns></returns>
        private bool GenerateHelpFilesForMerging( List<HelpType> helpTypes, string destinationDirectory, SortedDictionary<string, string> dirDict, 
            Dictionary<string, string> topicIDDict, bool addOutputFilesToSourceControl )
        {
            bool success = true;

            // go through all of the directories                
            foreach ( string directory in dirDict.Values )
            {
                if ( m_cancel )
                {
                    return success;
                }

                success = ProcessDirectoryForMergedHelp( directory, destinationDirectory, helpTypes, topicIDDict, 0 );
                if ( !success )
                {
                    OnOutputMessage( "Failure: " + directory + "\r\n", 1 );

                    if ( m_projectSettings.HaltOnError )
                    {
                        return success;
                    }
                }
            }

            return success;
        }

        /// <summary>
        /// Generates help using the help source files in the given directory.
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="destinationDirectory"></param>
        /// <param name="helpTypes"></param>
        /// <param name="topicIDDict"></param>
        /// <returns></returns>
        private bool ProcessSeparateDirectory( string directory, string destinationDirectory, List<HelpType> helpTypes, int depth )
        {
            return ProcessDirectory( directory, destinationDirectory, helpTypes, new Dictionary<string, string>(), null,
                m_projectSettings.ShowWhenFinishedBuilding, depth );
        }

        /// <summary>
        /// Generates help using the help source files in the given directory.
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="destinationDirectory"></param>
        /// <param name="helpTypes"></param>
        /// <param name="topicIDDict"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        private bool ProcessDirectoryForMergedHelp( string directory, string destinationDirectory, List<HelpType> helpTypes,
            Dictionary<string, string> topicIDDict, int depth )
        {
            return ProcessDirectory( directory, destinationDirectory, helpTypes, topicIDDict, null, false, depth );
        }

        private List<string> FindDtxesInProject(string vcprojFile)
        {
            List<string> dtxes = new List<string>();

            XmlDocument doc = new XmlDocument();
            doc.Load(vcprojFile);

            string projDir = Path.GetDirectoryName(vcprojFile);

            foreach (XmlNode item in doc.SelectNodes("//File/@RelativePath"))
            {
                if (item.Value.EndsWith(".dtx"))
                {
                    dtxes.Add(Path.GetFullPath(Path.Combine(projDir, item.Value)));
                }   
            }
            return dtxes;
        }

        /// <summary>
        /// Generates help using the help source files in the given directory.
        /// </summary>
        /// <param name="directory">The directory containing the help source files.</param>
        /// <param name="destinationDirectory">The directory where the generated help files will be saved.</param>
        /// <param name="helpTypes">A list of <see cref="HelpType"/>s to output help files for.</param>
        /// <param name="topicIDDict"></param>
        /// <param name="groupTopicID"></param>
        /// <returns><c>true</c> if the help files were built correctly, otherwise <c>false</c>.</returns>
        private bool ProcessDirectory( string directory, string destinationDirectory, List<HelpType> helpTypes,
            Dictionary<string, string> topicIDDict, string groupTopicID, bool showWhenFinished, int depth )
        {            
            bool isOverview = false;
            string name = GetModuleName( directory, out isOverview );

            string vcprojFile = null;

            if ( !isOverview )
            {
                // skip directory that doesn't have a vcproj file with the name we're looking for
                vcprojFile = GetVcprojFilename( name, directory );
                if ( vcprojFile == null )
                {
                    m_buildResults.BuildsSkipped += m_buildResults.BuildsPerDirectory;
                    return true;
                }
            }

            OnOutputMessage( "Processing " + directory + "...\r\n", depth );

            string dtxFile = Path.Combine( directory, name + ".dtx" );
            string doxFile = Path.Combine( directory, name + ".dox" );

            // see which HelpTypes we can skip
            List<HelpType> skipHelpTypes = new List<HelpType>();
            foreach ( HelpType helpKind in helpTypes )
            {
                string destDir = Path.Combine( destinationDirectory, helpKind.ToString() );

                if ( !NeedsToBeRebuilt( name, directory, destDir, helpKind, 0, true, dtxFile, doxFile ) )
                {
                    skipHelpTypes.Add( helpKind );
                }
            }

            bool didNotExist = false;
            List<string> disabledTopics = null;

            List<string> sourceFilesToAdd = new List<string>();

            // If we're going to generate anything, prepare the dtx and dox files
            if ( skipHelpTypes.Count < helpTypes.Count )
            {
                // Create the auto-generated sections
                if ( !OpenAndEditDtxFile( name, isOverview, dtxFile, topicIDDict, null, depth + 1 ) )
                {
                    return false;
                }

                if ( m_cancel )
                {
                    return true;
                }

                // look for additional dtx files in the vcproj file
                string[] dtxFilenames;
                if (vcprojFile != null)
                {
                    OnOutputMessage("Searching '" + vcprojFile + "' for DTXes:\r\n", depth + 1);
                    dtxFilenames = FindDtxesInProject(vcprojFile).ToArray();
                }
                else
                {
                    OnOutputMessage("Searching '" + directory + "' for DTXes:\r\n", depth + 1);
                    dtxFilenames = Directory.GetFiles(directory, "*.dtx");
                }

                foreach (string dtxFilename in dtxFilenames)
                {
                    if (dtxFilename.ToLower() != dtxFile.ToLower())
                    {
                        sourceFilesToAdd.Add(dtxFilename);
                    }
                }

                // open or create the dox, which will become our new template                    
                string templateFile = isOverview ? m_projectSettings.OverviewDoxTemplateFile : m_projectSettings.DoxTemplateFile;
                if ( !OpenFileForEdit( doxFile, templateFile, true, true, depth + 1, out didNotExist ) )
                {
                    m_buildResults.BuildsFailed += m_buildResults.BuildsPerDirectory;
                    return false;
                }

                if ( didNotExist )
                {
                    if ( !FixupRelativePathsInDox( templateFile, doxFile, depth + 1 ) )
                    {
                        if ( m_projectSettings.HaltOnError )
                        {
                            m_buildResults.BuildsFailed += m_buildResults.BuildsPerDirectory;
                            return false;
                        }
                    }
                }

                // extract MACROs to disable
                disabledTopics = GetDisabledTopics( name, directory, depth + 1 );
            }

            foreach ( HelpType helpKind in helpTypes )
            {
                if ( m_cancel )
                {
                    return true;
                }

                if ( skipHelpTypes.Contains( helpKind ) )
                {
                    ++m_buildResults.BuildsSkipped;
                    OnOutputMessage( helpKind.ToString() + " help files are up to date.  Skipping.\r\n", 1 );
                    continue;
                }

                OnOutputMessage( "Processing '" + doxFile + "':\r\n", depth + 1 );

                // read in dox file
                DoxInfo dInfo = ReadDoxFile( doxFile, depth + 2 );
                if ( dInfo == null )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_cancel )
                {
                    return true;
                }

                if ( sourceFilesToAdd.Count > 0 )
                {
                    if ( !AddSourceFilesToDox( dInfo, sourceFilesToAdd, depth + 2 ) )
                    {
                        ++m_buildResults.BuildsFailed;

                        if ( m_projectSettings.HaltOnError )
                        {
                            return false;
                        }
                        else
                        {
                            continue;
                        }
                    }
                }

                // these types need a config defined in the dox file, we can't "generate" one
                if ( (helpKind == HelpType.pdf) || (helpKind == HelpType.rtf) || (helpKind == HelpType.xml) )
                {
                    if ( !dInfo.ConfigurationsByHelpType.ContainsKey( helpKind ) )
                    {
                        OnOutputMessage( "Warning: '" + doxFile + "' needs a " + helpKind.ToString() + " configuration defined in order to build the help files.\r\n", depth + 2 );

                        ++m_buildResults.BuildsSkipped;
                        continue;
                    }
                }

                if ( !EditDoxFileConfiguration( name, isOverview, directory, destinationDirectory, dInfo, helpKind, disabledTopics, depth + 2 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_cancel )
                {
                    return true;
                }

                // write the new dox file over the old one
                if ( !OverwriteDoxFile( dInfo, doxFile, depth + 2 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_cancel )
                {
                    return true;
                }

                DoxConfigurationInfo dcInfo;
                if ( !dInfo.ConfigurationsByHelpType.TryGetValue( helpKind, out dcInfo ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                // open the output files for editing
                List<string> newFiles = null;
                if ( !OpenOutputFilesForEdit( name, helpKind, dcInfo.Help2CompileResult, dcInfo.CreatePrintableDocumentation, 
                    destinationDirectory, out newFiles, depth + 2 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_cancel )
                {
                    return true;
                }

                // create output directories
                if ( !CreateOutputDirectories( name, helpKind, directory, destinationDirectory, depth + 2 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_cancel )
                {
                    return true;
                }

                // call dox to build the project
                string doxdbFile = Path.Combine( directory, name + ".doxdb" );
                string outputFile = Path.Combine( BuildOutputDir( name, helpKind, directory, destinationDirectory ), name + ".txt" );
                if ( !BuildHelpFiles( doxFile, doxdbFile, outputFile, false, showWhenFinished, depth + 2 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_cancel )
                {
                    return true;
                }

                if ( !AddOutputFilesToSourceControl( newFiles, depth + 2 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                ++m_buildResults.BuildsSucceeded;
                OnOutputMessage( "Complete.\r\n", depth + 2 );
            }

            // clean up intermediate files
            if ( !m_projectSettings.KeepIntermediateFiles )
            {
                CleanupIntermediateFiles( depth + 1 );
            }

            if ( m_cancel )
            {
                return true;
            }

            OnOutputMessage( "Complete.\r\n", depth + 1 );

            return true;
        }

        /// <summary>
        /// Runs the Doc-O-Matic compilier to extract QA warnings.
        /// </summary>
        /// <param name="directory">The directory containing the help source files.</param>
        /// <param name="destinationDirectory">The directory where the generated help files will be saved.</param>
        /// <returns><c>true</c> if the help files were built correctly, otherwise <c>false</c>.</returns>
        private bool ProcessDirectoryForQAWarnings( string directory, string destinationDirectory )
        {
            bool isOverview = false;
            string name = GetModuleName( directory, out isOverview );

            if ( !isOverview )
            {
                // skip directory that doesn't have a vcproj file with the name we're looking for
                string vcprojFile = GetVcprojFilename( name, directory );
                if ( vcprojFile == null )
                {
                    ++m_buildResults.BuildsFailed;
                    return true;
                }
            }

            OnOutputMessage( "Processing " + directory + "...\r\n", 0 );

            string dtxFile = Path.Combine( directory, name + ".dtx" );
            string doxFile = Path.Combine( directory, name + ".dox" );

            string destDir = Path.Combine( destinationDirectory, c_QaHelpKind.ToString() );

            bool didNotExist = false;
            List<string> disabledTopics = null;

            // Create the auto-generated sections
            if ( !OpenAndEditDtxFile( name, isOverview, dtxFile, new Dictionary<string, string>(), null, 1 ) )
            {
                return false;
            }

            if ( m_cancel )
            {
                return true;
            }

            List<string> sourceFilesToAdd = new List<string>();

            // look for additional dtx files in the directory
            string[] dtxFilenames = Directory.GetFiles( directory, "*.dtx" );
            foreach ( string dtxFilename in dtxFilenames )
            {
                if ( Path.GetFileNameWithoutExtension( dtxFilename ).ToLower() != name.ToLower() )
                {
                    sourceFilesToAdd.Add( Path.GetFileName( dtxFilename ) );
                }
            }

            // open or create the help2 dox, which will become our new template                    
            string templateFile = isOverview ? m_projectSettings.OverviewDoxTemplateFile : m_projectSettings.DoxTemplateFile;
            if ( !OpenFileForEdit( doxFile, templateFile, true, false, 1, out didNotExist ) )
            {
                ++m_buildResults.BuildsFailed;
                return false;
            }

            if ( m_cancel )
            {
                return true;
            }

            if ( didNotExist )
            {
                if ( !FixupRelativePathsInDox( templateFile, doxFile, 1 ) )
                {
                    if ( m_projectSettings.HaltOnError )
                    {
                        ++m_buildResults.BuildsFailed;
                        return false;
                    }
                }
            }

            if ( m_cancel )
            {
                return true;
            }

            // extract MACROs to disable
            disabledTopics = GetDisabledTopics( name, directory, 1 );

            if ( m_cancel )
            {
                return true;
            }

            OnOutputMessage( "Processing '" + doxFile + "':\r\n", 1 );

            // read in dox file
            DoxInfo dInfo = ReadDoxFile( doxFile, 2 );
            if ( dInfo == null )
            {
                ++m_buildResults.BuildsFailed;

                return !m_projectSettings.HaltOnError;
            }

            if ( m_cancel )
            {
                return true;
            }

            if ( !EditDoxFileConfiguration( name, isOverview, directory, destinationDirectory, dInfo, c_QaHelpKind, disabledTopics, 2 ) )
            {
                ++m_buildResults.BuildsFailed;

                return !m_projectSettings.HaltOnError;
            }

            if ( m_cancel )
            {
                return true;
            }

            if ( sourceFilesToAdd.Count > 0 )
            {
                if ( !AddSourceFilesToDox( dInfo, sourceFilesToAdd, 2 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    return m_projectSettings.HaltOnError;
                }
            }

            if ( m_cancel )
            {
                return true;
            }

            // write the new dox file over the old one
            if ( !OverwriteDoxFile( dInfo, doxFile, 2 ) )
            {
                ++m_buildResults.BuildsFailed;

                return !m_projectSettings.HaltOnError;
            }

            if ( m_cancel )
            {
                return true;
            }

            /* Shouldn't have to do this since we're only going to do a check
            // open the  output files for editing
            if ( !OpenOutputFilesForEdit( name, HelpType.help2, destinationDirectory, 2 ) )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return false;
                }
                else
                {
                    continue;
                }
            }

            if ( m_cancel )
            {
                return true;
            }
            */


            // create output directories
            if ( !CreateOutputDirectories( name, c_QaHelpKind, directory, destinationDirectory, 2 ) )
            {
                ++m_buildResults.BuildsFailed;

                return !m_projectSettings.HaltOnError;
            }

            if ( m_cancel )
            {
                return true;
            }

            // call dox to build the project
            string doxdbFile = Path.Combine( directory, name + ".doxdb" );
            string outputFile = Path.Combine( BuildOutputDir( name, c_QaHelpKind, directory, destinationDirectory ), name + ".txt" );
            if ( !BuildHelpFiles( doxFile, doxdbFile, outputFile, true, m_projectSettings.ShowWhenFinishedBuilding, 2 ) )
            {
                ++m_buildResults.BuildsFailed;

                return !m_projectSettings.HaltOnError;
            }

            if ( m_cancel )
            {
                return true;
            }

            // extract qa warning messages
            if ( !ExtractQAWarnings( name, c_QaHelpKind, directory, destinationDirectory, 2 ) )
            {
                ++m_buildResults.BuildsFailed;

                return !m_projectSettings.HaltOnError;
            }
            else
            {
                ++m_buildResults.BuildsSucceeded;
            }

            // clean up intermediate files
            if ( !m_projectSettings.KeepIntermediateFiles )
            {
                CleanupIntermediateFiles( 1 );
            }

            if ( m_cancel )
            {
                return true;
            }

            OnOutputMessage( "Complete.\r\n", 1 );

            return true;
        }

        /// <summary>
        /// Processes the directory by editing its dtx file for placement into the unified dox file and modifies
        /// the <see cref="DoxInfo"/> by adding the directory's module and class hierarchy information
        /// </summary>
        /// <param name="directory">The directory containing the help source files.</param>
        /// <param name="dInfo">The <see cref="DoxInfo"/> representing the unified dox file.</param>
        /// <param name="moduleNumber">The index of this module that is used for the module and class hierarchy info.</param>
        /// <param name="sourceFiles">A list of source files to be added to.</param>
        /// <param name="disabledTopics">A list of disabled topics to be added to.</param>
        /// <param name="topicIDdict">A Dictionary of TopicIDs so we can enforce unqiue TopicIDs.</param>
        /// <param name="groupTopicID">The TopicID of the group under which this module's topics should reside.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool ProcessUnifiedDirectory( string directory, DoxInfo dInfo, int moduleNumber,
            List<string> sourceFiles, List<string> disabledTopics, Dictionary<string, string> topicIDdict, 
            string groupTopicID, int depth )
        {
            bool isOverview = false;
            string name = GetModuleName( directory, out isOverview );

            string vcprojFile = null;
            if ( !isOverview )
            {
                // skip directory that doesn't have a vcproj file with the name we're looking for
                vcprojFile = GetVcprojFilename( name, directory );
                if ( vcprojFile == null )
                {
                    m_buildResults.BuildsSkipped += m_buildResults.BuildsPerDirectory;
                    return true;
                }
            }

            OnOutputMessage( "Processing " + directory + "...\r\n", depth );

            string dtxFile = Path.Combine( directory, name + ".dtx" );

            // Create the auto-generated sections
            if ( !OpenAndEditDtxFile( name, isOverview, dtxFile, topicIDdict, groupTopicID, depth + 1 ) )
            {
                return false;
            }

            sourceFiles.Add( dtxFile );

            // look for additional dtx files in the directory
            string[] dtxFilenames;
            if (vcprojFile != null)
            {
                OnOutputMessage("Searching '" + vcprojFile + "' for DTXes:\r\n", depth + 1);
                dtxFilenames = FindDtxesInProject(vcprojFile).ToArray();
            }
            else
            {
                OnOutputMessage("Searching '" + directory + "' for DTXes:\r\n", depth + 1);
                dtxFilenames = Directory.GetFiles(directory, "*.dtx");
            }

            foreach (string dtxFilename in dtxFilenames)
            {
                if (Path.GetFileNameWithoutExtension(dtxFilename).ToLower() != name.ToLower())
                {
                    // Create the auto-generated sections (treated like overview files so we skip the command line and samples sections)
                    if (!OpenAndEditDtxFile(name, true, dtxFilename, topicIDdict, groupTopicID, depth + 1))
                    {
                        return false;
                    }

                    if (m_cancel)
                    {
                        return true;
                    }

                    sourceFiles.Add(dtxFilename);
                }
            }

            if (vcprojFile != null) 
            {

#if INCLUDE_VCPROJ_IN_SOURCE_FILES
                sourceFiles.Add( vcprojFile );
#elif !ADD_MODULE
                List<string> files = new List<string>();
                if ( ReadFilesFromVcprojFile( vcprojFile, files ) )
                {
                    sourceFiles.AddRange( files );
                }
#endif

                if ( m_cancel )
                {
                    return true;
                }

                // extract MACROs to disable
                List<string> topics = GetDisabledTopics( name, directory, depth + 1 );
                if ( topics != null )
                {
                    disabledTopics.AddRange( topics );
                }

                if ( m_cancel )
                {
                    return true;
                }

#if ADD_CLASS_HIERARCHY
                // Add Class Hierarchy section
                if ( !AddClassHierarchyToDox( dInfo, name, moduleNumber, depth + 1 ) )
                {
                    return false;
                }

                if ( m_cancel )
                {
                    return true;
                }
#endif

#if ADD_MODULE
#if ADD_TOPIC_FOR_MODULE
                string topicID = groupTopicID + "_module";
#else
                string topicID = string.Empty;
#endif
                // Add Module section
                if ( !AddModuleToDox( dInfo, name, topicID, vcprojFile, moduleNumber, depth + 1 ) )
                {
                    return false;
                }

                if ( topicID != string.Empty )
                {
                    topicIDdict.Add( topicID, topicID );
                }
#endif
            }

            OnOutputMessage( "Complete.\r\n", depth + 1 );

            return true;
        }

        /// <summary>
        /// Formats the message with the appropriate number of tabs, and invokes the OutputMessage event.
        /// </summary>
        /// <param name="msg">The message to send.</param>
        /// <param name="indent">The number of tabs to prepend the message with.</param>
        private void OnOutputMessage( string msg, int indent )
        {
            if ( this.OutputMessage != null )
            {
                StringBuilder message = new StringBuilder();
                for ( int i = 0; i < indent; ++i )
                {
                    message.Append( '\t' );
                }
                message.Append( msg );

                if ( m_parentForm != null )
                {
                    object[] args = new object[] { this, new OutputMessageEventArgs( message.ToString() ) };
                    m_parentForm.Invoke( this.OutputMessage, args );
                }
                else
                {
                    this.OutputMessage( this, new OutputMessageEventArgs( message.ToString() ) );
                }
            }
        }

        /// <summary>
        /// Attempts to add a file or an entire directory to source control
        /// </summary>
        /// <param name="item">The file to directory to be added.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success.</returns>
        private bool AddToSourceControl( string item, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Adding '" + item + "' to source control:", depth );
            }

            int d = m_projectSettings.VerboseLevel >= 2 ? depth + 1 : depth;

            if ( File.Exists( item ) )
            {
                rageStatus status;
                m_projectSettings.SourceControlProviderSettings.Add( item, out status );
                if ( !status.Success() )
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( "\r\n", 0 );
                    }

                    OnOutputMessage( "Warning: Unable to add '" + item + "' to source control: " + status.ErrorString + ".\r\n", d );
                    return false;
                }
            }
            else if ( Directory.Exists( item ) )
            {
                rageStatus status;
                m_projectSettings.SourceControlProviderSettings.AddDirectory( item, out status );
                if ( !status.Success() )
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( "\r\n", 0 );
                    }

                    OnOutputMessage( "Warning: Unable to add everything in '" + item + "' to source control: " + status.ErrorString + ".\r\n", d );
                    return false;
                }
            }
            else
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Warning: Trying to add something that doesn't exist.\r\n", d );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            m_itemsToDelete.Remove( item.ToLower() );
            m_itemsToForceRevert.Remove( item.ToLower() );
            m_itemsToRevert.Remove( item.ToLower() );

            return true;
        }

        /// <summary>
        /// Determines if a module's help files need to be rebuilt.
        /// </summary>
        /// <param name="name">The module name</param>
        /// <param name="directory">The directory of the module.</param>
        /// <param name="destDir">The directory where the help files will be saved.  Typically the doc directory's folder for the <see cref="HelpType"/>.</param>
        /// <param name="helpKind">The <see cref="HelpType"/> that we want to build.</param>
        /// <param name="help2CompileResult">The value of the Help2CompileResult.</param>
        /// <param name="rtfCreatePrintableDocumentation">Build Windows Help or RTF.</param>
        /// <param name="dtxFile">The filename of the dtx file.</param>
        /// <param name="doxFile">The filename of the dox file.  Should be the base file, not the one specific to the <see cref="HelpType"/>.</param>
        /// <returns><c>true</c> if they should be build, otherwise <c>false</c>.</returns>
        private bool NeedsToBeRebuilt( string name, string directory, string destDir, HelpType helpKind, 
            int help2CompileResult, bool rtfCreatePrintableDocumentation,
            string dtxFile, string doxFile )
        {
            if ( m_projectSettings.Rebuild )
            {
                return true;
            }

            if ( !File.Exists( dtxFile ) )
            {
                return true;
            }

            List<string> destDirFilenames;
            if ( !BuildOutputFilenames( name, destDir, helpKind, help2CompileResult, rtfCreatePrintableDocumentation, out destDirFilenames ) )
            {
                return false;
            }

            DateTime lastOutputTime = DateTime.MinValue;
            foreach ( string destDirFilename in destDirFilenames )
            {
                if ( !File.Exists( destDirFilename ) )
                {
                    return true;
                }

                DateTime dt = File.GetLastWriteTime( destDirFilename );
                if ( dt > lastOutputTime )
                {
                    lastOutputTime = dt;
                }
            }

            List<string> dependencies = GetDependencies( name, directory );
            DateTime lastDependencyTime = DateTime.MinValue;
            foreach ( string dependency in dependencies )
            {
                if ( !File.Exists( dependency ) )
                {
                    continue;
                }

                DateTime dt = File.GetLastWriteTime( dependency );
                if ( dt > lastDependencyTime )
                {
                    lastDependencyTime = dt;
                }
            }

            string[] dtxFilenames = Directory.GetFiles( Path.GetDirectoryName( dtxFile ), "*.dtx" );
            foreach ( string dtxFilename in dtxFilenames )
            {
                DateTime dtxDT = File.GetLastWriteTime( dtxFilename );
                if ( dtxDT > lastDependencyTime )
                {
                    lastDependencyTime = dtxDT;
                }
            }

            if ( File.Exists( doxFile ) )
            {
                DateTime doxDT = File.GetLastWriteTime( doxFile );
                if ( doxDT > lastDependencyTime )
                {
                    lastDependencyTime = doxDT;
                }
            }

            return lastDependencyTime > lastOutputTime;
        }

        /// <summary>
        /// Retrieves a list of files that the module depends on.
        /// </summary>
        /// <param name="name">The name of the module.</param>
        /// <param name="directory">The directory of the module.</param>
        /// <returns>A <see cref="List"/> of filenames.</returns>
        private List<string> GetDependencies( string name, string directory )
        {
            List<string> dependencies = new List<string>();

            string vcprojFile = GetVcprojFilename( name, directory );
            if ( vcprojFile == null )
            {
                return dependencies;
            }

            if ( !ReadFilesFromVcprojFile( vcprojFile, dependencies ) )
            {
                return dependencies;
            }            

            string samplesDirectory = GetSamplesDirectory( name, directory );
            if ( samplesDirectory != null )
            {
                string[] sampleVcprojs = Directory.GetFiles( samplesDirectory, "sample_*.vcproj" );
                foreach ( string sampleVcproj in sampleVcprojs )
                {
                    dependencies.AddRange( GetDependencies( Path.GetFileNameWithoutExtension( sampleVcproj ), samplesDirectory ) );
                }
            }

            return dependencies;
        }

        /// <summary>
        /// Retrieves a given module's samples directory, if it exists.
        /// </summary>
        /// <param name="name">The module name.</param>
        /// <param name="directory">The directory of the module.</param>
        /// <returns>The full path name of the sample directory if it exists, otherwise <c>null</c>.</returns>
        private string GetSamplesDirectory( string name, string directory )
        {
            string samplesDirectory = Path.Combine( directory, "..\\..\\samples\\sample_" + name );
            samplesDirectory = Path.GetFullPath( samplesDirectory );

            if ( Directory.Exists( samplesDirectory ) )
            {
                return samplesDirectory;
            }

            return null;
        }

        /// <summary>
        /// For the given module and <see cref="HelpType"/>, build a list of all the output files.
        /// </summary>
        /// <param name="name">The module name.</param>
        /// <param name="destDir">The directory where we want to save the output files.</param>
        /// <param name="helpKind">The <see cref="HelpType"/> we're building.</param>
        /// <param name="help2CompileResult">The value of the Help2CompileResult attribute.</param>
        /// <param name="rtfCreatePrintableDocumentation">Whether the RTF <see cref="HelpType"/> configuration indicates to output Windows Help or RTF.</param>
        /// <param name="destDirFilenames">The <see cref="List"/>of destination filenames.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool BuildOutputFilenames( string name, string destDir, HelpType helpKind, 
            int help2CompileResult, bool rtfCreatePrintableDocumentation, out List<string> destDirFilenames )
        {
            destDirFilenames = new List<string>(); 

            switch ( helpKind )
            {
                case HelpType.help2:
                    {
                        destDirFilenames.Add( Path.Combine( destDir, name + ".HxS" ) );
                        if ( help2CompileResult == 1 )
                        {
                            destDirFilenames.Add( Path.Combine( destDir, name + ".HxI" ) );
                        }

                        /*
                        destDirFilenames.Add( Path.Combine( destDir, name + ".HxF" ) );
                        destDirFilenames.Add( Path.Combine( destDir, name + "_A.HxK" ) );
                        destDirFilenames.Add( Path.Combine( destDir, name + "_F.HxK" ) );
                        destDirFilenames.Add( Path.Combine( destDir, name + "_K.HxK" ) );
                        destDirFilenames.Add( Path.Combine( destDir, name + "_NamedURL.HxK" ) );
                        */
                    }
                    break;
                case HelpType.html:
                    {
                        /* FIXME
                        // the html files for this module live in a subdir of destDir
                        destDir = Path.Combine( destDir, name );
                        if ( Directory.Exists( destDir ) )
                        {
                            destDirFilenames.AddRange( Directory.GetFiles( destDir ) );
                        }
                        */
                    }
                    break;
                case HelpType.rtf:
                    {                        
                        if ( rtfCreatePrintableDocumentation )
                        {
                            destDirFilenames.Add( Path.Combine( destDir, name + "." + helpKind.ToString() ) );
                        }
                        else
                        {
                            destDirFilenames.Add( Path.Combine( destDir, name + ".hlp" ) );                            
                        }
                    }
                    break;
                default:
                    {
                        destDirFilenames.Add( Path.Combine( destDir, name + "." + helpKind.ToString() ) );
                    }
                    break;
            }

            return true;
        }

        /// <summary>
        /// Opens the file for edit.  If Source Control is enabled, it will be checked out.  Otherwise, it will be
        /// make writable.  If the file does not exist, and templateFilename is not <c>null</c>, a copy of it
        /// will be saved to filename.
        /// </summary>
        /// <param name="filename">The file to open for edit.</param>
        /// <param name="templateFilename">The template file on which filename is based.</param>
        /// <param name="markForDelete"><c>true</c> if we need to delete this file when we call <see cref="CleanupIntermediateFiles"/>.</param>
        /// <param name="markForForcedRevert"><c>true</c> if we need to revert this file when we call <see cref="CleanupIntermediateFiles"/>, otherwise <c>false</c> and we will revert only if unchanged.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <param name="didNotExist">An out argument indicating whether the file existed previously or not. When not, we will probably end up adding it to source control.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool OpenFileForEdit( string filename, string templateFilename, bool markForDelete, bool markForForcedRevert, int depth, out bool didNotExist )
        {
            didNotExist = false;

            if ( !File.Exists( filename ) )
            {
                didNotExist = true;

                if ( templateFilename != null )
                {
                    rageStatus status = new rageStatus();
                    rageFileUtilities.CopyFile( templateFilename, filename, out status );
                    if ( status.Success() )
                    {
                        if ( markForDelete )
                        {
                            m_itemsToDelete.Add( filename.ToLower() );
                        }

                        // our copied file might have retained it's ReadOnly status
                        File.SetAttributes( filename, File.GetAttributes( filename ) & (~FileAttributes.ReadOnly) );

                        if ( m_projectSettings.VerboseLevel >= 2 )
                        {
                            OnOutputMessage( "Created '" + filename + "' from template.\r\n", depth );
                        }
                    }
                    else
                    {
                        OnOutputMessage( "Warning: Unable to create '" + filename + "' from template '" + templateFilename + ":\r\n" + status.GetErrorString() + "\r\n", depth );
                        return false;
                    }
                }
                else
                {
                    if ( markForDelete )
                    {
                        m_itemsToDelete.Add( filename.ToLower() );
                    }
                }
            }
            else
            {
                rageStatus status;
                m_projectSettings.SourceControlProviderSettings.CheckOut( filename, out status );
                if ( !status.Success() )
                {
                    OnOutputMessage( "Warning: Unable to check out '" + filename + "' from source control: " + status.ErrorString + ".\r\n", depth );
                    return false;
                }
                else
                {
                    // just to make sure
                    File.SetAttributes( filename, File.GetAttributes( filename ) & (~FileAttributes.ReadOnly) );

                    /*
                    rageStatus status = null;
                    if ( ((m_projectSettings.SourceControlProvider == ProjectSettings.Provider.Alienbrain)
                        && rageNXN.FileExists( filename, out status ))
                        || ((m_projectSettings.SourceControlProvider == ProjectSettings.Provider.Perforce)
                        && ragePerforce.FileExists( filename, out status ))
                        || (m_projectSettings.SourceControlProvider == ProjectSettings.Provider.CVS) )
                    {
                    */
                        if ( markForForcedRevert )
                        {
                            m_itemsToForceRevert.Add( filename.ToLower() );
                        }
                        else
                        {
                            m_itemsToRevert.Add( filename.ToLower() );
                        }

                        if ( m_projectSettings.VerboseLevel >= 2 )
                        {
                            OnOutputMessage( "Opened '" + filename + "' for edit.\r\n", depth );
                        }
                    //}

                    if ( markForDelete )
                    {
                        m_itemsToDelete.Add( filename.ToLower() );
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Opens the dtx file for edit, opens the dtx file, creates the auto-generated sections, and saves it out again.
        /// </summary>
        /// <param name="name">The module name.</param>
        /// <param name="isOverview">Indicates if the module is an overview (containing no source code).</param>
        /// <param name="dtxFile">The dtx file to edit.</param>
        /// <param name="topicIDDict">A Dictionary of TopicIDs so we can enforce unique names when unifying the help files.</param>
        /// <param name="groupTopicID">The TopicID of the group under which this module's topics should reside.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool OpenAndEditDtxFile( string name, bool isOverview, string dtxFile, 
            Dictionary<string, string> topicIDDict, string groupTopicID, int depth )
        {
            bool didNotExist = false;

            if ( !isOverview )
            {
                // open or create the dtx
                if ( !OpenFileForEdit( dtxFile, m_projectSettings.DtxTemplateFile, false, false, depth, out didNotExist ) )
                {
                    m_buildResults.BuildsFailed += m_buildResults.BuildsPerDirectory;
                    return false;
                }

                if ( didNotExist )
                {
                    if ( !AddToSourceControl( dtxFile, depth ) )
                    {
                        if ( m_projectSettings.HaltOnError )
                        {
                            m_buildResults.BuildsFailed += m_buildResults.BuildsPerDirectory;
                            return false;
                        }
                    }
                }
            }
            else
            {
                // open the dtx
                if ( !OpenFileForEdit( dtxFile, null, false, false, depth, out didNotExist ) )
                {
                    m_buildResults.BuildsFailed += m_buildResults.BuildsPerDirectory;
                    return false;
                }
            }

            try
            {
                string dtxTempFilename = Path.GetTempFileName();

                // make a copy so that the one we ultimately check into source control is the unedited version
                File.Copy( dtxFile, dtxTempFilename, true );

                m_itemsToOverwrite.Add( dtxFile, dtxTempFilename );
            }
            catch ( Exception e )
            {
                Console.WriteLine( "File Copy error: {0}.", e.Message );
                m_itemsToOverwrite.Remove( dtxFile );
            }

            // edit the auto-generated sections
            if ( !EditDtxAutoGeneratedSections( name, isOverview, dtxFile, topicIDDict, groupTopicID, depth ) )
            {
                if ( m_projectSettings.HaltOnError )
                {
                    m_buildResults.BuildsFailed += m_buildResults.BuildsPerDirectory;
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Opens the dtx file, creates the auto-generated sections, and saves it out again.
        /// </summary>
        /// <param name="name">The module name.</param>
        /// <param name="isOverview">Indicates if the module is an overview (containing no source code).</param>
        /// <param name="dtxFile">The dtx file to edit.</param>
        /// <param name="topicIDDict">A Dictionary of TopicIDs so we can enforce unique names when unifying the help files.</param>
        /// <param name="groupTopicID">The TopicID of the group under which this module's topics should reside.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool EditDtxAutoGeneratedSections( string name, bool isOverview, string dtxFile, 
            Dictionary<string, string> topicIDDict, string groupTopicID, int depth )
        {
            OnOutputMessage( "Auto-generating '" + dtxFile + "' sections:", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            // read in the dtx file
            DtxInfo xInfo = new DtxInfo();
            try
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "Reading '" + dtxFile + "':", depth + 1 );
                }

                TextReader reader = new StreamReader( dtxFile );
                string line = reader.ReadLine();
                while ( line != null )
                {
                    xInfo.Lines.Add( line );
                    line = reader.ReadLine();
                }

                reader.Close();
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: could not open '" + dtxFile + "' for reading: " + e.Message + ".\r\n", depth + 2 );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            List<string> parametersSection = null;
            if ( !isOverview )
            {
                // look for samples directory relative to dtxFile
                string samplesDirectory = GetSamplesDirectory( name, Path.GetDirectoryName( dtxFile ) );
                if ( samplesDirectory != null )
                {
                    List<string> samplesSection = GetSamplesSectionText( name, samplesDirectory, depth + 1 );
                    if ( samplesSection.Count > 0 )
                    {
                        ReplaceSectionInDtx( xInfo, "SAMPLES-SECTION", samplesSection, depth + 1 );
                    }
                }
                else
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( "No sample directory exists.  Skipping.\r\n", depth + 1 );
                    }
                }

                parametersSection = GetParametersSectionText( name, Path.GetDirectoryName( dtxFile ), depth + 1 );
                if ( (parametersSection != null) && (parametersSection.Count > 0) )
                {
                    ReplaceSectionInDtx( xInfo, "COMMAND-LINE-SECTION", parametersSection, depth + 1 );
                }
            }

            // make sure we have unique TopicIDs and are grouped under groupTopicID
            if ( m_projectSettings.UnifyHelpFiles )
            {
                EditDtxTopicIDsForUnifiedHelp( xInfo, name, groupTopicID, topicIDDict, depth + 1 );
            }

            try
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "Saving '" + dtxFile + "':", depth + 1 );
                }

                TextWriter writer = new StreamWriter( dtxFile, false );
                foreach ( string line in xInfo.Lines )
                {
                    writer.WriteLine( line );
                }

                writer.Close();
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: could not save '" + dtxFile + "': " + e.Message + ".\r\n", depth + 2 );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", parametersSection == null ? depth + 1 : 0 );
            }

            return true;
        }

        /// <summary>
        /// Generates the Samples Section text for a dtx file.
        /// </summary>
        /// <param name="name">The module name.</param>
        /// <param name="samplesDirectory">The module's corresponding samples directory.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns>A <see cref="List"/> of strings.</returns>
        private List<string> GetSamplesSectionText( string name, string samplesDirectory, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Generating Samples section:", depth );
            }

            List<string> sampleLines = new List<string>();
            int numberOfSamples = 0;
            bool addedLinebreak = false;

            // loop through each
            string[] filenames = Directory.GetFiles( samplesDirectory, "sample_*.cpp" );
            foreach ( string filename in filenames )
            {
                SampleState state = SampleState.FindTitle;
                string title = string.Empty;
                List<string> purpose = new List<string>();
                List<SampleStep> steps = new List<SampleStep>();
                int stepNumber = 0;

                if ( m_projectSettings.VerboseLevel >= 3 )
                {
                    if ( !addedLinebreak )
                    {
                        OnOutputMessage( "\r\n", 0 );
                        addedLinebreak = true;
                    }

                    OnOutputMessage( "Parsing '" + filename + "' for sample info:", depth + 1 );
                    addedLinebreak = false;
                }

                TextReader reader = null;
                try
                {

                    reader = new StreamReader( filename );
                }
                catch ( Exception e )
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        if ( !addedLinebreak )
                        {
                            OnOutputMessage( "\r\n", 0 );
                            addedLinebreak = true;
                        }
                    }

                    int d = (m_projectSettings.VerboseLevel >= 3) ? depth + 2 : ((m_projectSettings.VerboseLevel >= 2) ? depth + 1 : depth);
                    OnOutputMessage( "Warning: Could not open '" + filename + "' for parsing: " + e.Message + ".\r\n", d );
                    continue;
                }

                string line = null;
                SampleStep currentStep = null;
                bool useLineContents = false;
                bool waitingToCloseCodeTag = false;

                while ( (useLineContents && line != null) || ((line = reader.ReadLine()) != null) )
                {
                    useLineContents = false;

                    line = line.Trim();

                    if ( state == SampleState.FindTitle )
                    {
                        if ( m_regexSampleTitle.Match( line ).Success == true )
                        {
                            title = m_regexSampleTitle.Replace( line, string.Empty, 1 ).Trim();
                            state = SampleState.FindPurpose;
                        }
                    }
                    else if ( state == SampleState.FindPurpose )
                    {
                        if ( m_regexSamplePurpose.Match( line ).Success == true )
                        {
                            purpose.Add( m_regexSamplePurpose.Replace( line, string.Empty, 1 ).Trim() );

                            // read rest of purpose:
                            while ( (line = reader.ReadLine()) != null )
                            {
                                if ( m_regexSampleCommentStart.Match( line ).Success == true )
                                {
                                    // add rest of purpose comment:
                                    purpose.Add( m_regexSampleCommentStart.Replace( line, string.Empty, 1 ).Trim() );
                                }
                                else
                                {
                                    break;
                                }
                            }

                            // switch to next step:
                            state = SampleState.FindStep;
                        }
                    }
                    else
                    {
                        // find the step #:
                        if ( m_regexSampleStep.Match( line ).Success )
                        {
                            currentStep = new SampleStep( ++stepNumber, m_regexSampleStep.Replace( line, string.Empty, 1 ).Trim() );

                            steps.Add( currentStep );

                            state = SampleState.InStep;
                        }
                        else if ( state == SampleState.InStep )
                        {
                            while ( (line != null) && m_regexSampleDashes.Match( line ).Success )
                            {
                                currentStep.StepBody.Add( m_regexSampleDashes.Replace( line, string.Empty, 1 ).Trim() );

                                // read rest of subsection comment:
                                useLineContents = false;
                                bool foundEndDelim = false;
                                while ( (useLineContents && (line != null)) || ((line = reader.ReadLine()) != null) )
                                {
                                    line = line.Trim();

                                    // see if we've found a new step:
                                    if ( m_regexSampleStep.Match( line ).Success )
                                    {
                                        if ( state == SampleState.InCode )
                                        {
                                            if ( waitingToCloseCodeTag )
                                            {
                                                currentStep.StepBody.Add( "</CODE>\r\n" );
                                            }

                                            waitingToCloseCodeTag = false;
                                        }

                                        state = SampleState.InStep;
                                        useLineContents = true;
                                        break;
                                    }
                                    else if ( state == SampleState.InStep )
                                    {
                                        if ( m_regexSampleCommentStart.Match( line ).Success )
                                        {
                                            // add rest of purpose comment:
                                            currentStep.StepBody.Add( m_regexSampleCommentStart.Replace( line, string.Empty, 1 ).Trim() );
                                        }
                                        else
                                        {
                                            state = SampleState.InCode;
                                            if ( m_regexSampleStop.Match( line ).Success )
                                            {
                                                foundEndDelim = true;
                                            }
                                            else if ( !foundEndDelim )
                                            {
                                                currentStep.StepBody.Add( "<CODE>\r\n" );
                                                waitingToCloseCodeTag = true;

                                                line = m_regexSampleBackslashEscape.Replace( line, @"\\" );

                                                currentStep.StepBody.Add( line );
                                            }
                                        }
                                    }
                                    else if ( state == SampleState.InCode )
                                    {
                                        // found a new step or found next substep:
                                        if ( m_regexSampleStep.Match( line ).Success || m_regexSampleDashes.Match( line ).Success )
                                        {
                                            state = SampleState.InStep;
                                            if ( waitingToCloseCodeTag )
                                            {
                                                currentStep.StepBody.Add( "</CODE>" );
                                            }

                                            waitingToCloseCodeTag = false;
                                            useLineContents = true;
                                            break;
                                        }
                                        else if ( m_regexSampleStop.Match( line ).Success )
                                        {
                                            // we found the stop token:
                                            if ( waitingToCloseCodeTag )
                                            {
                                                currentStep.StepBody.Add( "</CODE>" );
                                            }

                                            waitingToCloseCodeTag = false;
                                            foundEndDelim = true;
                                            useLineContents = false;
                                        }
                                        else if ( !foundEndDelim )
                                        {
                                            line = m_regexSampleBackslashEscape.Replace( line, @"\\" );
                                            currentStep.StepBody.Add( line );
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if ( waitingToCloseCodeTag )
                {
                    currentStep.StepBody.Add( "</CODE>" );
                }

                reader.Close();

                // if we found a title, build the dtx section
                if ( title != string.Empty )
                {
                    if ( m_projectSettings.VerboseLevel >= 3 )
                    {
                        if ( !addedLinebreak )
                        {
                            OnOutputMessage( "\r\n", 0 );
                            addedLinebreak = true;
                        }

                        OnOutputMessage( "Creating dtx section:", depth + 2 );
                    }

                    sampleLines.Add( "@@" + title + " Sample" );
                    sampleLines.Add( "<GROUP Working Examples>" );

                    foreach ( string s in purpose )
                    {
                        sampleLines.Add( s );
                    }

                    if ( steps.Count > 0 )
                    {
                        sampleLines.Add( "The sample is broken into several steps:" );

                        foreach ( SampleStep step in steps )
                        {
                            sampleLines.Add( step.StepNumber + ". " + step.Description );
                        }

                        foreach ( SampleStep step in steps )
                        {
                            sampleLines.Add( string.Empty );
                            sampleLines.Add( "* " + step.Description + " *" );

                            foreach ( string s in step.StepBody )
                            {
                                sampleLines.Add( s );
                            }
                        }
                    }

                    sampleLines.Add( string.Empty );
                    sampleLines.Add( string.Empty );

                    ++numberOfSamples;

                    if ( m_projectSettings.VerboseLevel >= 3 )
                    {
                        OnOutputMessage( " Complete (found " + steps.Count + " steps).\r\n", 0 );
                        OnOutputMessage( "Complete.\r\n", depth + 1 );
                    }
                }
                else if ( m_projectSettings.VerboseLevel >= 3 )
                {
                    OnOutputMessage( " Complete (no sample info found).\r\n", 0 );
                }
            }

            if ( filenames.Length > 0 )
            {
                if ( (m_projectSettings.VerboseLevel >= 3) || addedLinebreak )
                {
                    OnOutputMessage( "Complete (" + numberOfSamples + " files contained sample info).\r\n", depth + 1 );
                }
                else if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( " Complete (" + numberOfSamples + " files contained sample info).\r\n", 0 );
                }
            }
            else
            {
                if ( (m_projectSettings.VerboseLevel >= 3) || addedLinebreak )
                {
                    OnOutputMessage( "Complete (no files to process).\r\n", depth + 1 );
                }
                else if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( " Complete (no files to process).\r\n", 0 );
                }                
            }

            return sampleLines;
        }

        /// <summary>
        /// Generates the Command Line Section text for a dtx file.
        /// </summary>
        /// <param name="name">The module name.</param>
        /// <param name="directory">The directory of the module.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns>A <see cref="List"/> of strings, null on error.</returns>
        private List<string> GetParametersSectionText( string name, string directory, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Generating Command Line section:\r\n", depth );
            }


            // see if there are any files to grep
            string[] filenames = Directory.GetFiles( directory, "*.cpp" );
            if ( filenames.Length == 0 )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( " No files to search.\r\n", 0 );
                }

                return new List<string>();
            }

            Regex paramRe = new Regex(@"\b          # word boundary 
                                        (REQ)?      # optional 'REQ'
                                        PARAM       # literal PARAM
                                        \s* \( \s*  # '(' surrounded by whitespace
                                        (?<pname>\w+) \s* , \s* # match the param name word
                                        "" (?<desc>.*) ""       # the description string  
                                        \s* \) \s* ;            # and the final );
                                        ", RegexOptions.IgnorePatternWhitespace);

            Regex fparamRe = new Regex(@"\b          # word boundary 
                                        FPARAM       # literal PARAM
                                        \s* \( \s*  # '(' surrounded by whitespace
                                        (?<pnum>\d+) \s* , \s*    # match the param number
                                        (?<pname>\w+) \s* , \s*   # match the param name word
                                        "" (?<desc>.*) ""     # the description string  
                                        \s* \) \s* ;          # and the final );
                                        ", RegexOptions.IgnorePatternWhitespace);


            List<ParamInfo> paramInfos = new List<ParamInfo>();

            foreach (FindUtil.GrepResult result in FindUtil.GrepInFiles(filenames, new Regex[] { paramRe, fparamRe }))
            {
                Group paramGroup = result.Match.Groups["pname"];
                Group numGroup = result.Match.Groups["pnum"];
                Group descGroup = result.Match.Groups["desc"];

                if (m_projectSettings.VerboseLevel >= 2)
                {
                    OnOutputMessage(String.Format("Found param {0} ({1} line {2})\r\n", paramGroup.Value, result.Filename, result.LineNumber), depth+1);
                }

                ParamInfo newInfo = new ParamInfo(
                    Path.GetDirectoryName(result.Filename),
                    result.Filename,
                    paramGroup.Value,
                    descGroup.Value,
                    numGroup.Value == String.Empty ? -1 : Int32.Parse(numGroup.Value)
                    );

                paramInfos.Add(newInfo);
            }


            // sort the list
            paramInfos.Sort();

            // find the longest item in each column
            int longestName = 0;
            int longestFilename = 0;
            int longestDescription = 0;

            foreach ( ParamInfo pInfo in paramInfos )
            {
                if ( (pInfo.ParamName != null) && (pInfo.ParamName.Length > longestName) )
                {
                    longestName = pInfo.ParamName.Length;
                }

                if ( (pInfo.FileName != null) && (pInfo.FileName.Length > longestFilename) )
                {
                    longestFilename = pInfo.FileName.Length;
                }

                if ( (pInfo.Description != null) && (pInfo.Description.Length > longestDescription) )
                {
                    longestDescription = pInfo.Description.Length;
                }
            }

            // build the table line by line
            List<string> parameterLines = new List<string>();
            parameterLines.Add( "@@Command Line Options" );
            parameterLines.Add( "<TABLE>" );

            string filenameHeader = "In File";
            if ( filenameHeader.Length > longestFilename )
            {
                longestFilename = filenameHeader.Length;
            }

            string paramNameHeader = "Parameter Name";
            if ( paramNameHeader.Length > longestName )
            {
                longestName = paramNameHeader.Length;
            }

            string descriptionHeader = "Description";
            if ( descriptionHeader.Length > longestDescription )
            {
                longestDescription = descriptionHeader.Length;
            }

            // headers
            ParamInfo headerInfo = new ParamInfo( null, filenameHeader, paramNameHeader, descriptionHeader, -1 );
            parameterLines.Add( headerInfo.GetTableRowString( longestName, longestFilename, longestDescription ) );
            parameterLines.Add( headerInfo.GetSeparatorRowString( longestName, longestFilename, longestDescription ) );

            foreach ( ParamInfo pInfo in paramInfos )
            {
                parameterLines.Add( pInfo.GetTableRowString( longestName, longestFilename, longestDescription ) );
            }

            parameterLines.Add( "</TABLE>" );

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
                OnOutputMessage( "Complete (" + paramInfos.Count + " results).\r\n", depth + 1 );
            }
            else if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete (" + paramInfos.Count + " results).\r\n", 0 );
            }

            return parameterLines;
        }

        /// <summary>
        /// Replaces a specially-marked section of the dtx file (given by lines) with the replaceText.
        /// </summary>
        /// <param name="xInfo">The <see cref="DtxInfo"/> to edit.</param>
        /// <param name="section">The name of the section to replace.</param>
        /// <param name="replaceText">The lines of text that go in the section.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool ReplaceSectionInDtx( DtxInfo xInfo, string section, List<string> replaceText, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Replacing '" + section + "':", depth );
            }

            if ( !xInfo.ReplaceSection( section, replaceText ) )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Warning: ReplaceSection failed.\r\n", m_projectSettings.VerboseLevel >= 2 ? depth + 1 : depth );
                return true;
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Ensures that a top-level TopicID topicID exists, and that all TopicIDs that aren't otherwise grouped, are
        /// grouped underneath it.
        /// </summary>
        /// <param name="xInfo">The <see cref="DtxInfo"/> to edit.</param>
        /// <param name="name">The name of the module.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <param name="topicIDDict">A dictionary of TopicIDs so we can enforce uniqueness.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool EditDtxTopicIDsForUnifiedHelp( DtxInfo xInfo, string name, string groupTopicID, 
            Dictionary<string, string> topicIDDict, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Editing Dtx TopicIDs:", depth );
            }

            if ( !String.IsNullOrEmpty( groupTopicID ) )
            {
                if ( !xInfo.GroupUnderTopicID( groupTopicID ) )
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( "\r\n", 0 );
                    }

                    OnOutputMessage( "Warning: GroupUnderTopicID '" + groupTopicID + "' failed.\r\n", m_projectSettings.VerboseLevel >= 2 ? depth + 1 : depth );
                    return true;
                }
            }

            if ( !xInfo.EnforceUniqueTopicIDs( topicIDDict ) )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Warning: EnforceUniqueTopicIDs failed.\r\n", m_projectSettings.VerboseLevel >= 2 ? depth + 1 : depth );
                return true;
            }

#if ADD_MODULE && ADD_TOPIC_FOR_MODULE
            if ( !xInfo.AddTopic( groupTopicID + "_module", "Symbol Reference", groupTopicID ) )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Warning: AddTopic '" + groupTopicID + "_module' failed.\r\n", m_projectSettings.VerboseLevel >= 2 ? depth + 1 : depth );
                return true;
            }
#endif

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        private void DisableSymbol(string newSymbol, List<string> symbols, int depth)
        {
            symbols.Add(newSymbol);
            if (m_projectSettings.VerboseLevel >= 2)
            {
                OnOutputMessage(String.Format("Disabling symbol {0}\r\n", newSymbol), depth);
            }
        }

        /// <summary>
        /// Searches the directory for header macros so that we may disable them in the dox file.
        /// </summary>
        /// <param name="name">The name of the module.</param>
        /// <param name="directory">The directory containing the header files.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns>A <see cref="List"/> containing the macros to disable.</returns>
        private List<string> GetDisabledTopics( string name, string directory, int depth )
        {
            OnOutputMessage( "Searching for topics to disable:", depth );

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "\r\n", 0 );
                OnOutputMessage( "grep #ifndef *.h:\r\n", depth + 1 );
            }

            // see if there are any files to grep
            string[] filenames = Directory.GetFiles( directory, "*.h" );
            if ( filenames.Length == 0 )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( " No files to search.\r\n", 0 );
                }

                return new List<string>();
            }

            Regex includeGuardRE = new Regex(@"#ifndef\s+(\w+_H)"); // match #ifndef, whitespace, symbol ending in _H

            List<string> results = new List<string>();

            foreach(FindUtil.GrepResult res in FindUtil.GrepInFiles(filenames, new Regex[]{includeGuardRE}))
            {
                DisableSymbol(res.Match.Groups[1].Value, results, depth + 1);
            }

#if DISABLE_VCPROJ_FILE_TOPICS
            string vcprojFile = GetVcprojFilename( name, directory );
            if ( vcprojFile != null )
            {
                results.Add( Path.GetFileName( vcprojFile ) );
            }
#endif

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "Complete (" + results.Count + " results).\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete (" + results.Count + " results).\r\n", 0 );
            }

            return results;
        }

        /// <summary>
        /// Loads and parses the dox file for items that we will later edit.
        /// </summary>
        /// <param name="doxFile">The dox file to load.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns>Returns a <see cref="DoxInfo"/> object containing the lines of text and parsed information.</returns>
        private DoxInfo ReadDoxFile( string doxFile, int depth )
        {
            OnOutputMessage( "Parsing '" + doxFile + ":\r\n", depth );

            StreamReader reader = new StreamReader( doxFile, Encoding.Default );

            DoxInfo dInfo = new DoxInfo();

            string currentConfigurationName = null;
            Dictionary<string, int> configIndexes = new Dictionary<string, int>();

            string line = reader.ReadLine();
            while ( line != null )
            {
                dInfo.Lines.Add( line );

                if ( line == "[Configurations]" )
                {
                    line = reader.ReadLine();
                    dInfo.Lines.Add( line );

                    // get the number of configurations
                    int count = 0;
                    if ( line.StartsWith( "Count" ) )
                    {
                        string countString = line.Substring( line.IndexOf( '=' ) + 1 );
                        count = int.Parse( countString );
                    }

                    line = reader.ReadLine();
                    dInfo.Lines.Add( line );

                    // get the index of the current configuration
                    int currentIndex = -1;
                    if ( line.StartsWith( "Current" ) )
                    {
                        string currentString = line.Substring( line.IndexOf( '=' ) + 1 );
                        currentIndex = int.Parse( currentString );

                        dInfo.CurrentConfigurationIndex = dInfo.Lines.Count - 1;
                    }

                    // look for the name of the current configuration
                    for ( int i = 0; i < count; ++i )
                    {
                        line = reader.ReadLine();
                        dInfo.Lines.Add( line );

                        string configName = line.Substring( line.IndexOf( '=' ) + 1 );
                        if ( i == currentIndex )
                        {
                            currentConfigurationName = configName;
                        }

                        configIndexes.Add( configName, i );
                    }
                }
                else if ( line.StartsWith( "HelpKind" ) )
                {
                    // get the HelpType
                    string helpTypeString = line.Substring( line.IndexOf( '=' ) + 1 );
                    HelpType helpKind = (HelpType)int.Parse( helpTypeString );

                    string configurationName = FindConfigurationName( dInfo, dInfo.Lines.Count - 2 );
                    if ( configurationName != null )
                    {
                        int configIndex = -1;
                        configIndexes.TryGetValue( configurationName, out configIndex );

                        DoxConfigurationInfo dcInfo = new DoxConfigurationInfo( configurationName, configIndex, helpKind, dInfo.Lines.Count - 1 );
                        dInfo.AddConfiguration( dcInfo );

                        if ( configurationName == currentConfigurationName )
                        {
                            dInfo.CurrentConfigurationHelpKind = helpKind;
                        }

                        if ( m_projectSettings.VerboseLevel >= 3 )
                        {
                            OnOutputMessage( "Found '" + dcInfo.Name + "', HelpKind=" + dcInfo.HelpKind.ToString() + "\r\n", depth + 1 );
                        }
                    }
                }
                else if ( line.Contains( "Disabled Topics" ) )
                {
                    string configurationName = FindConfigurationName( dInfo, dInfo.Lines.Count - 2 );
                    if ( configurationName != null )
                    {
                        DoxConfigurationInfo dcInfo;
                        if ( dInfo.ConfigurationsByName.TryGetValue( configurationName, out dcInfo ) )
                        {
                            dcInfo.DisabledTopicIndex = dInfo.Lines.Count - 1;
                        }
                        else
                        {
                            OnOutputMessage( "Warning: Found the Disabled Topics section for '"
                                + configurationName + "' before the HelpKind.\r\n", depth + 1 );
                        }
                    }
                }
                else if ( line.StartsWith( "OutputDir" ) )
                {
                    string configurationName = FindConfigurationName( dInfo, dInfo.Lines.Count - 2 );
                    if ( configurationName != null )
                    {
                        DoxConfigurationInfo dcInfo;
                        if ( dInfo.ConfigurationsByName.TryGetValue( configurationName, out dcInfo ) )
                        {
                            dcInfo.OutputDirIndex = dInfo.Lines.Count - 1;
                        }
                        else
                        {
                            OnOutputMessage( "Warning: Found the OutputDir attribute for '"
                                + configurationName + "' before the HelpKind.\r\n", depth + 1 );
                        }
                    }
                }
                else if ( line.StartsWith( "ClassHierarchyLayoutOutputDir" ) )
                {
                    string configurationName = FindConfigurationName( dInfo, dInfo.Lines.Count - 2 );
                    if ( configurationName != null )
                    {
                        DoxConfigurationInfo dcInfo;
                        if ( dInfo.ConfigurationsByName.TryGetValue( configurationName, out dcInfo ) )
                        {
                            dcInfo.ClassHierarchyLayoutOutputDirIndex = dInfo.Lines.Count - 1;
                        }
                        else
                        {
                            OnOutputMessage( "Warning: Found the ClassHierarchyLayoutOutputDir attribute for '"
                                + configurationName + "' before the HelpKind.\r\n", depth + 1 );
                        }
                    }
                }
                else if ( line.StartsWith( "HTMLHelpFilename" ) )
                {
                    string configurationName = FindConfigurationName( dInfo, dInfo.Lines.Count - 1 );
                    if ( configurationName != null )
                    {
                        DoxConfigurationInfo dcInfo;
                        if ( dInfo.ConfigurationsByName.TryGetValue( configurationName, out dcInfo ) )
                        {
                            dcInfo.HtmlHelpFilenameIndex = dInfo.Lines.Count - 1;
                        }
                        else
                        {
                            OnOutputMessage( "Warning: Found the HTMLHelpFilename attribute for '"
                                + configurationName + "' before the HelpKind.\r\n", depth + 1 );
                        }
                    }
                }
                else if ( line.StartsWith( "HTMLHelp2Filename" ) )
                {
                    string configurationName = FindConfigurationName( dInfo, dInfo.Lines.Count - 1 );
                    if ( configurationName != null )
                    {
                        DoxConfigurationInfo dcInfo;
                        if ( dInfo.ConfigurationsByName.TryGetValue( configurationName, out dcInfo ) )
                        {
                            dcInfo.HtmlHelp2FilenameIndex = dInfo.Lines.Count - 1;
                        }
                        else
                        {
                            OnOutputMessage( "Warning: Found the HTMLHelp2Filename attribute for '"
                                + configurationName + "' before the HelpKind.\r\n", depth + 1 );
                        }
                    }
                }
                else if ( line.StartsWith( "OutputFile" ) )
                {
                    string configurationName = FindConfigurationName( dInfo, dInfo.Lines.Count - 1 );
                    if ( configurationName != null )
                    {
                        DoxConfigurationInfo dcInfo;
                        if ( dInfo.ConfigurationsByName.TryGetValue( configurationName, out dcInfo ) )
                        {
                            dcInfo.OutputFileIndex = dInfo.Lines.Count - 1;
                        }
                        else
                        {
                            OnOutputMessage( "Warning: Found the OutputFile attribute for '"
                                + configurationName + "' before the HelpKind.\r\n", depth + 1 );
                        }
                    }
                }
                else if ( line.StartsWith( "HLPFilename" ) )
                {
                    string configurationName = FindConfigurationName( dInfo, dInfo.Lines.Count - 1 );
                    if ( configurationName != null )
                    {
                        DoxConfigurationInfo dcInfo;
                        if ( dInfo.ConfigurationsByName.TryGetValue( configurationName, out dcInfo ) )
                        {
                            dcInfo.HlpFilenameIndex = dInfo.Lines.Count - 1;
                        }
                        else
                        {
                            OnOutputMessage( "Warning: Found the OutputFile attribute for '"
                                + configurationName + "' before the HelpKind.\r\n", depth + 1 );
                        }
                    }
                }
                else if ( line.StartsWith( "Help2CompileResult" ) )
                {
                    string val = line.Substring( line.IndexOf( '=' ) + 1 );

                    string configurationName = FindConfigurationName( dInfo, dInfo.Lines.Count - 1 );
                    if ( configurationName != null )
                    {
                        DoxConfigurationInfo dcInfo;
                        if ( dInfo.ConfigurationsByName.TryGetValue( configurationName, out dcInfo ) )
                        {
                            dcInfo.Help2CompileResult = int.Parse( val );
                        }
                        else
                        {
                            OnOutputMessage( "Warning: Found the Help2CompileResult attribute for '"
                                + configurationName + "' before the HelpKind.\r\n", depth + 1 );
                        }
                    }
                }
                else if ( line.StartsWith( "CreatePrintableDocumentation" ) )
                {
                    // this is how we identify the RTF configuration
                    string configurationName = FindConfigurationName( dInfo, dInfo.Lines.Count - 1 );
                    if ( configurationName != null )
                    {
                        int configIndex = -1;
                        configIndexes.TryGetValue( configurationName, out configIndex );

                        DoxConfigurationInfo dcInfo = new DoxConfigurationInfo( configurationName, configIndex, HelpType.rtf, -1 );
                        dInfo.AddConfiguration( dcInfo );

                        if ( configurationName == currentConfigurationName )
                        {
                            dInfo.CurrentConfigurationHelpKind = HelpType.rtf;
                        }

                        if ( m_projectSettings.VerboseLevel >= 3 )
                        {
                            OnOutputMessage( "Found '" + dcInfo.Name + "', HelpKind=" + dcInfo.HelpKind.ToString() + "\r\n", depth + 1 );
                        }

                        string val = line.Substring( line.IndexOf( '=' ) + 1 ).Trim();
                        if ( val != string.Empty )
                        {
                            dcInfo.CreatePrintableDocumentation = val == "1";
                        }
                    }
                }
                else if ( line.StartsWith( "AddLastDelim" ) )
                {
                    // this is how we identify the PDF configuration
                    string configurationName = FindConfigurationName( dInfo, dInfo.Lines.Count - 1 );
                    if ( configurationName != null )
                    {
                        int configIndex = -1;
                        configIndexes.TryGetValue( configurationName, out configIndex );

                        DoxConfigurationInfo dcInfo = new DoxConfigurationInfo( configurationName, configIndex, HelpType.pdf, -1 );
                        dInfo.AddConfiguration( dcInfo );

                        if ( configurationName == currentConfigurationName )
                        {
                            dInfo.CurrentConfigurationHelpKind = HelpType.pdf;
                        }

                        if ( m_projectSettings.VerboseLevel >= 3 )
                        {
                            OnOutputMessage( "Found '" + dcInfo.Name + "', HelpKind=" + dcInfo.HelpKind.ToString() + "\r\n", depth + 1 );
                        }
                    }
                }
                else if ( line.StartsWith( "Encoding" ) )
                {
                    // this is how we identify the XML configuration
                    string configurationName = FindConfigurationName( dInfo, dInfo.Lines.Count - 1 );
                    if ( configurationName != null )
                    {
                        int configIndex = -1;
                        configIndexes.TryGetValue( configurationName, out configIndex );

                        DoxConfigurationInfo dcInfo = new DoxConfigurationInfo( configurationName, configIndex, HelpType.xml, -1 );
                        dInfo.AddConfiguration( dcInfo );

                        if ( configurationName == currentConfigurationName )
                        {
                            dInfo.CurrentConfigurationHelpKind = HelpType.xml;
                        }

                        if ( m_projectSettings.VerboseLevel >= 3 )
                        {
                            OnOutputMessage( "Found '" + dcInfo.Name + "', HelpKind=" + dcInfo.HelpKind.ToString() + "\r\n", depth + 1 );
                        }
                    }
                }
                else if ( line == "[Source Files]" )
                {
                    dInfo.SourceFilesIndex = dInfo.Lines.Count - 1;
                }
                else if ( line == "[Class Hierarchy]" )
                {
                    dInfo.ClassHierarchyIndex = dInfo.Lines.Count - 1;
                }
                else if ( line == "[Modules]" )
                {
                    dInfo.ModulesIndex = dInfo.Lines.Count - 1;
                }
                else if ( line == @"[Class Hierarchy\0]" )
                {
                    dInfo.ClassHierarchyStartIndex = dInfo.Lines.Count - 1;
                }
                else if ( line == @"[Class Hierarchy\0\Options\Shadows]" )
                {
                    // read til the section break
                    line = reader.ReadLine();
                    while ( line != null )
                    {
                        dInfo.Lines.Add( line );

                        if ( line.Trim() == String.Empty )
                        {
                            dInfo.ClassHierarchyEndIndex = dInfo.Lines.Count - 1;
                            break;
                        }

                        line = reader.ReadLine();
                    }
                }
                else
                {
                    // see if we have a File or a Path
                    if ( (line.StartsWith( "File" ) || line.StartsWith( "Path" )) && line.Contains( @"..\" ) )
                    {
                        dInfo.RelativePathIndexes.Add( dInfo.Lines.Count - 1 );
                    }

                    // see if we have a line with 1 or more macros
                    int indexOf = line.IndexOf( "$$" );
                    if ( indexOf > -1 )
                    {
                        indexOf = line.IndexOf( "$$", indexOf + 1 );
                        if ( indexOf > -1 )
                        {
                            dInfo.MacroIndexes.Add( dInfo.Lines.Count - 1 );
                        }
                    }
                }

                line = reader.ReadLine();
            }

            reader.Close();

            OnOutputMessage( "Complete.\r\n", depth + 1 );

            return dInfo;
        }

        /// <summary>
        /// The dox file is layed out in blocks of settings by Configuration name.  Using the given lineIndex
        /// of a setting, locates the Configuration name of the block it is in.
        /// </summary>
        /// <param name="dInfo">The <see cref="DoxInfo"/> to search.</param>
        /// <param name="lineIndex">The line index in <see cref="DoxInfo.Lines"/> of the setting.</param>
        /// <returns>The name of the Configuration.</returns>
        private string FindConfigurationName( DoxInfo dInfo, int lineIndex )
        {
            // search backwards for the configuration name
            while ( (lineIndex >= 0) && !dInfo.Lines[lineIndex].StartsWith( "[Configurations" ) )
            {
                --lineIndex;
            }

            // extract the configuration name
            if ( lineIndex >= 0 )
            {
                string[] lineSplit = dInfo.Lines[lineIndex].Split( new char[] { '\\' } );
                if ( lineSplit.Length >= 2 )
                {
                    return lineSplit[1];
                }
            }

            return null;
        }

        /// <summary>
        /// Changes all relative paths in the copiedFile from being relative to templateFile to being relative to copiedFile
        /// </summary>
        /// <param name="templateFile">The source file the relative paths are relative to.</param>
        /// <param name="copiedFile">The destination file that we want the relative paths to be relative to.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool FixupRelativePathsInDox( string templateFile, string copiedFile, int depth )
        {
            OnOutputMessage( "Fixing up Relative Paths in Dox file '" + copiedFile + "':\r\n", depth );

            // load the dox
            DoxInfo dInfo = ReadDoxFile( copiedFile, depth + 1 );
            if ( dInfo == null )
            {
                return true;
            }
            
            // fixup the paths
            if ( !dInfo.FixupRelativePaths( Path.GetDirectoryName( templateFile ), Path.GetDirectoryName( copiedFile ) ) )
            {
                OnOutputMessage( "Warning: Could not fixup relative paths in Dox file '" + copiedFile + "':", depth + 1 );
                return true;
            }

            // save the dox file
            if ( !OverwriteDoxFile( dInfo, copiedFile, depth + 1 ) )
            {
                return true;
            }

            OnOutputMessage( "Complete.\r\n", depth + 1 );

            return true;
        }
        
        /// <summary>
        /// Edits <see cref="DoxInfo.Lines"/> to make it ready to build the help files of type <see cref="HelpType"/>.
        /// </summary>
        /// <param name="name">The name of the module.</param>
        /// <param name="isOverview">Whether the module is an overview or not.</param>
        /// <param name="directory">The directory of the module.</param>
        /// <param name="destinationDirectory">The directory where the help files should be placed.</param>
        /// <param name="dInfo">The <see cref="DoxInfo"/> to edit.</param>
        /// <param name="helpKind">The <see cref="HelpType"/> we're building.</param>
        /// <param name="disabledTopics">The <see cref="List"/> of disabled topics.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool EditDoxFileConfiguration( string name, bool isOverview, string directory, string destinationDirectory, 
            DoxInfo dInfo, HelpType helpKind, List<string> disabledTopics, int depth )
        {
            OnOutputMessage( "Modifying dox file in memory:", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            // see if the configuration we want is in the dox file already
            DoxConfigurationInfo dcInfo;
            if ( !dInfo.ConfigurationsByHelpType.TryGetValue( helpKind, out dcInfo ) )
            {
                // these 3 are pretty much interchangeable
                if ( (helpKind == HelpType.chm) || (helpKind == HelpType.help2) || (helpKind == HelpType.html) )
                {
                    if ( !dInfo.ConfigurationsByHelpType.TryGetValue( HelpType.chm, out dcInfo )
                        && !dInfo.ConfigurationsByHelpType.TryGetValue( HelpType.help2, out dcInfo )
                        && !dInfo.ConfigurationsByHelpType.TryGetValue( HelpType.html, out dcInfo ) )
                    {
                        if ( m_projectSettings.VerboseLevel < 2 )
                        {
                            OnOutputMessage( "\r\n", 0 );
                        }

                        OnOutputMessage( "Error: Unable to generate " + helpKind.ToString() + " help because no compatible configurations exist.\r\n", depth + 1 );
                        return false;
                    }
                }
                // everything else, we must have the config in the dox file
                else
                {
                    if ( m_projectSettings.VerboseLevel < 2 )
                    {
                        OnOutputMessage( "\r\n", 0 );
                    }

                    OnOutputMessage( "Error: Unable to generate " + helpKind.ToString() + " help because that configuration does not exist.\r\n", depth + 1 );
                    return false;
                }
            }

            // set the current configuration
            if ( !SetCurrentConfigurationInDox( dInfo, dcInfo, helpKind, depth + 1 ) )
            {
                return false;
            }

            if ( !isOverview )
            {
                // Add the disabled topics
                if ( !AddDisabledTopicsToDox( dInfo, dcInfo, disabledTopics, depth + 1 ) )
                {
                    return false;
                }
            }

            // replace the macros
            if ( !ReplaceMacrosInDox( dInfo, name, depth + 1 ) )
            {
                return false;
            }

            // customize the name and output dirs
            if ( !SetOutputDirectoriesAndFilenamesInDox( name, directory, destinationDirectory, dInfo, dcInfo, helpKind, depth + 1 ) )
            {
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Modifies the <see cref="DoxInfo"/> for a unified help build.
        /// </summary>
        /// <param name="dInfo">The <see cref="DoxInfo"/> to edit.</param>
        /// <param name="moduleCount">The number of modules that it will contain</param>
        /// <param name="sourceFiles">The source files.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool EditUnifiedDoxFile( DoxInfo dInfo, int moduleCount, List<string> sourceFiles, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Modifying unified dox file in memory:", depth );
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

#if ADD_CLASS_HIERARCHY
            // set the class hierarchy count
            if ( !SetClassHierarchyCountInDox( dInfo, moduleCount, depth + 1 ) )
            {
                return false;
            }
#endif

#if ADD_MODULE
            // set the module count
            if ( !SetModuleCountInDox( dInfo, moduleCount, depth + 1 ) )
            {
                return false;
            }
#endif

            // add the source files
            if ( !AddSourceFilesToDox( dInfo, sourceFiles, depth + 1 ) )
            {
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Sets the current Configuration in the <see cref="DoxInfo.Lines"/> to corresponding to <see cref="HelpType"/>, the help
        /// we're building.
        /// </summary>
        /// <param name="dInfo">The <see cref="DoxInfo"/> to edit.</param>
        /// <param name="helpKindIndex">The index in <see cref="DoxInfo.Lines"/> containing the current Configuration setting.</param>
        /// <param name="helpKind">The <see cref="HelpType"/> we're building.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool SetCurrentConfigurationInDox( DoxInfo dInfo, DoxConfigurationInfo dcInfo, HelpType helpKind, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Setting current configuration to " + (int)helpKind + ":", depth );
            }

            // if the config matches, set the current config to be it
            if ( dcInfo.HelpKind == helpKind )
            {
                if ( (dInfo.CurrentConfigurationIndex > -1) && (dcInfo.Index > -1) )
                {
                    dInfo.Lines[dInfo.CurrentConfigurationIndex] = "Current=" + dcInfo.Index;

                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( " Complete.\r\n", 0 );
                    }
                }
                else
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( "\r\n", 0 );
                    }

                    OnOutputMessage( "Error: Couldn't set the Current configuration in the dox file.\r\n", depth + 1 );
                    return false;
                }
            }
            // if we can, alter the current config's HelpKind
            else if ( (helpKind != HelpType.pdf) && (helpKind != HelpType.rtf) && (helpKind != HelpType.xml) )
            {
                if ( dcInfo.HelpKindIndex > -1 )
                {
                    dInfo.Lines[dcInfo.HelpKindIndex] = "HelpKind=" + (int)helpKind;

                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( " Complete.\r\n", 0 );
                    }
                }
                else
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( "\r\n", 0 );
                    }

                    OnOutputMessage( "Error: Couldn't locate Current configuration's HelpKind line in the dox file.\r\n", depth + 1 );
                    return false;
                }
            }
            // there's nothing we can do
            else
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: Couldn't set the Current configuration.\r\n", depth + 1 );
                return false;
            }

            return true;
        }

        /// <summary>
        /// Merges the disabled topics into the Disabled Topics section of <see cref="DoxInfo.Lines"/>.
        /// </summary>
        /// <param name="dInfo">The <see cref="DoxInfo"/> to edit.</param>
        /// <param name="dcInfo">The <see cref="DoxConfigurationInfo"/> containing the indexes of our parsed items.</param>
        /// <param name="disabledTopics">The <see cref="List"/> of topics to disable.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool AddDisabledTopicsToDox( DoxInfo dInfo, DoxConfigurationInfo dcInfo, List<string> disabledTopics, int depth )
        {
            if ( dcInfo.DisabledTopicIndex == -1 )
            {
                OnOutputMessage( "Warning: No 'Disabled Topics' section.\r\n", depth );
                return true;
            }
            else
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "Modifying 'Disabled Topics' section:", depth );
                }

                if ( (disabledTopics == null) || (disabledTopics.Count == 0) )
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( " No topics to disable.\r\n", 0 );
                    }

                    return true;
                }
            }

            if ( !dInfo.AddDisabledTopics( dcInfo, disabledTopics ) )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Warning: Could not add Disabled Topics.\r\n", m_projectSettings.VerboseLevel >= 2 ? depth + 1 : depth );
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Replaces all occurrences of the allowed macros in the <see cref="DoxInfo"/>.
        /// </summary>
        /// <param name="dInfo">The <see cref="DoxInfo"/> to edit.</param>
        /// <param name="name">The name of the module.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool ReplaceMacrosInDox( DoxInfo dInfo, string name, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Replacing macros in Dox file:", depth );
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            // replace "$$ARCHIVE$$"
            if ( !ReplaceMacroInDox( dInfo, "ARCHIVE", name, depth + 1 ) )
            {
                return false;
            }

            // replace "$$TREE_NAMESPACE$$"
            if ( !ReplaceMacroInDox( dInfo, "TREE_NAMESPACE", m_projectSettings.TreeNamespace, depth + 1 ) )
            {
                return false;
            }

            // replace "$$TOPIC_ID$$"
            string topicID = m_projectSettings.TreeNamespace + "." + name;
            if ( !ReplaceMacroInDox( dInfo, "TOPIC_ID", topicID.Replace( ' ', '_' ), depth + 1 ) )
            {
                return false;
            }

            // replace "$$COMPANY_DESC$$"
            if ( !ReplaceMacroInDox( dInfo, "COMPANY_DESC", m_projectSettings.CompanyDescription, depth + 1 ) )
            {
                return false;
            }

            // replace "$$HXA_ATTRIBUTE_PROJECT_NAME$$"
            if ( !ReplaceMacroInDox( dInfo, "HXA_ATTRIBUTE_COMPANY_NAME", m_projectSettings.HxaAttributeCompanyName, depth + 1 ) )
            {
                return false;
            }

            // replace "$$HXA_ATTRIBUTE_COMPANY_VALUE$$"
            if ( !ReplaceMacroInDox( dInfo, "HXA_ATTRIBUTE_COMPANY_VALUE", m_projectSettings.HxaAttributeCompanyValue, depth + 1 ) )
            {
                return false;
            }

            // replace "$$PROJECT_DESC$$"
            if ( !ReplaceMacroInDox( dInfo, "PROJECT_DESC", m_projectSettings.ProjectDescription, depth + 1 ) )
            {
                return false;
            }

            // replace "$$HXA_ATTRIBUTE_PROJECT_NAME$$"
            if ( !ReplaceMacroInDox( dInfo, "HXA_ATTRIBUTE_PROJECT_NAME", m_projectSettings.HxaAttributeProjectName, depth + 1 ) )
            {
                return false;
            }

            // replace "$$HXA_ATTRIBUTE_PROJECT_VALUE$$"
            if ( !ReplaceMacroInDox( dInfo, "HXA_ATTRIBUTE_PROJECT_VALUE", m_projectSettings.HxaAttributeProjectValue, depth + 1 ) )
            {
                return false;
            }

            // replace "$$HXA_LINK_GROUP$$"
            if ( !ReplaceMacroInDox( dInfo, "HXA_LINK_GROUP", m_projectSettings.HxaLinkGroup, depth + 1 ) )
            {
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Replaces every instance of the given macro in the <see cref="DoxInfo"/> with the given value.
        /// </summary>
        /// <param name="dInfo">The <see cref="DoxInfo"/> to edit.</param>
        /// <param name="macro">The macro to replace.  Do not include delimeters such as "$$".</param>
        /// <param name="value">The value to place.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool ReplaceMacroInDox( DoxInfo dInfo, string macro, string value, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "Replacing '" + macro + "' with value '" + value + "':", depth );
            }

            int replacementsMade = 0;

            // replace $$macro$$ with value
            macro = "$$" + macro + "$$";
            foreach ( int index in dInfo.MacroIndexes )
            {
                string newText = dInfo.Lines[index].Replace( macro, value );

                if ( newText != dInfo.Lines[index] )
                {
                    dInfo.Lines[index] = newText;
                    ++replacementsMade;
                }
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( " Complete (" + replacementsMade + " replacement(s) made).\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Changes the output directory lines in <see cref="DoxInfo.Lines"/> to output in a directory
        /// whose name is based on the <see cref="HelpType"/>.
        /// </summary>
        /// <param name="name">The name of the module.</param>
        /// <param name="directory">The directory of the module</param>
        /// <param name="destinationDirectory">The destination directory for the help files.</param>
        /// <param name="dInfo">The <see cref="DoxInfo"/> to edit.</param>
        /// <param name="dcInfo">The <see cref="DoxConfigurationInfo"/> containing the indexes of our parsed items.</param>
        /// <param name="helpKind">The <see cref="HelpType"/> that we're building.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool SetOutputDirectoriesAndFilenamesInDox( string name, string directory, string destinationDirectory, 
            DoxInfo dInfo, DoxConfigurationInfo dcInfo, HelpType helpKind, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Setting output directories:", depth );
            }

            bool warning = false;

            // replace OutputDir
            if ( dcInfo.OutputDirIndex == -1 )
            {
                // xml, pdf, and rtf don't have OutputDir
                if ( (helpKind == HelpType.chm) || (helpKind == HelpType.help2) || (helpKind == HelpType.html) )
                {
                    if ( m_projectSettings.VerboseLevel < 2 )
                    {
                        OnOutputMessage( "\r\n", 0 );
                    }

                    OnOutputMessage( "Warning: No OutputDir attribute found.\r\n", depth + 1 );
                    warning = true;
                }
            }
            else
            {
                string outputDir = BuildOutputDir( name, helpKind, directory, destinationDirectory );
                outputDir = ProjectSettings.GetRelativePath( directory, outputDir );

                int indexOf = dInfo.Lines[dcInfo.OutputDirIndex].IndexOf( '=' );
                if ( indexOf > -1 )
                {
                    dInfo.Lines[dcInfo.OutputDirIndex] = dInfo.Lines[dcInfo.OutputDirIndex].Substring( 0, indexOf + 1 );
                    dInfo.Lines[dcInfo.OutputDirIndex] += outputDir;
                }
                else
                {
                    dInfo.Lines[dcInfo.OutputDirIndex] = "OutputDir=" + outputDir;
                }
            }

            // replace ClassHierarchyLayoutOutputDir
            if ( dcInfo.ClassHierarchyLayoutOutputDirIndex == -1 )
            {
                if ( (m_projectSettings.VerboseLevel < 2) && !warning )
                {
                    OnOutputMessage( "\r\n", 0 );
                }
                
                OnOutputMessage( "Warning: No ClassHierarchyLayoutOutputDir attribute found.\r\n", depth + 1 );
                warning = true;
            }
            else
            {
                string classHierarchyDir = BuildClassHierarchyLayoutOutputDir( name, helpKind, directory, destinationDirectory );
                classHierarchyDir = ProjectSettings.GetRelativePath( directory, classHierarchyDir );

                int indexOf = dInfo.Lines[dcInfo.ClassHierarchyLayoutOutputDirIndex].IndexOf( '=' );
                if ( indexOf > -1 )
                {
                    dInfo.Lines[dcInfo.ClassHierarchyLayoutOutputDirIndex] = dInfo.Lines[dcInfo.ClassHierarchyLayoutOutputDirIndex].Substring( 0, indexOf + 1 );
                    dInfo.Lines[dcInfo.ClassHierarchyLayoutOutputDirIndex] += classHierarchyDir;
                }
                else
                {
                    dInfo.Lines[dcInfo.ClassHierarchyLayoutOutputDirIndex] = "ClassHierarchyLayoutOutputDir=" + classHierarchyDir;
                }
            }

            // replace the output filename, whatever it is
            int outputFilenameIndex = dcInfo.GetHelpFilenameIndex( helpKind );
            if ( outputFilenameIndex == -1 )
            {
                // html doesn't have an output filename of any kind
                if ( helpKind != HelpType.html )
                {
                    if ( (m_projectSettings.VerboseLevel < 2) && !warning )
                    {
                        OnOutputMessage( "\r\n", 0 );
                    }

                    OnOutputMessage( "Warning: No " + dcInfo.GetHelpFilenameAttributeName( helpKind ) + " attribute found.\r\n", depth + 1 );
                    warning = true;
                }
            }
            else 
            {
                string outputFilename = Path.Combine( destinationDirectory, helpKind.ToString() );
                outputFilename = Path.Combine( outputFilename, name + dcInfo.GetHelpFilenameExtension( helpKind ) );
                outputFilename = ProjectSettings.GetRelativePath( directory, outputFilename );

                int indexOf = dInfo.Lines[outputFilenameIndex].IndexOf( '=' );
                if ( indexOf > -1 )
                {
                    dInfo.Lines[outputFilenameIndex] = dInfo.Lines[outputFilenameIndex].Substring( 0, indexOf + 1 );
                    dInfo.Lines[outputFilenameIndex] += outputFilename;
                }
                else
                {
                    dInfo.Lines[outputFilenameIndex] = dcInfo.GetHelpFilenameAttributeName( helpKind ) + "=" + outputFilename;
                }
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                if ( warning )
                {
                    OnOutputMessage( "Complete.\r\n", depth + 1 );
                }
                else
                {
                    OnOutputMessage( " Complete.\r\n", 0 );
                }
            }

            return true;
        }

        /// <summary>
        /// Sets the Count and Current fields of the Class Hierarchy settings in the <see cref="DoxInfo"/>.
        /// </summary>
        /// <param name="dInfo">The <see cref="DoxInfo"/> to edit</param>
        /// <param name="count">The number of modules we are going to add, each getting their own class hierarchy.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool SetClassHierarchyCountInDox( DoxInfo dInfo, int count, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "Setting Class Hierarchy count to " + count + ":", depth );
            }

            if ( dInfo.ClassHierarchyIndex != -1 )
            {
                dInfo.Lines[dInfo.ClassHierarchyIndex + 1] = "Count=" + count;
                dInfo.Lines[dInfo.ClassHierarchyIndex + 2] = "Current=1";
            }
            else
            {
                if ( m_projectSettings.VerboseLevel >= 3 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: ClassHierarchyIndex not set.\r\n", m_projectSettings.VerboseLevel >= 3 ? depth + 1 : depth );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Adds a new set of Class Hierarchy options with the given name and module number.
        /// </summary>
        /// <param name="dInfo">The <see cref="DoxInfo"/> to edit.</param>
        /// <param name="name">The name of the module.</param>
        /// <param name="moduleNumber">The module number for this class hierarchy.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool AddClassHierarchyToDox( DoxInfo dInfo, string name, int moduleNumber, int depth )
        {
            OnOutputMessage( "Adding Class Hierarchy for '" + name + "':", depth );

            if ( !dInfo.AddClassHierarchy( name, name + " Symbol Reference", moduleNumber ) )
            {
                OnOutputMessage( "\r\n", 0 );
                OnOutputMessage( "Error: ClassHierarchy Start/End not set.\r\n", depth + 1 );
                return false;
            }

            OnOutputMessage( " Complete.\r\n", 0 );

            return true;
        }

        /// <summary>
        /// Sets the Count field of the Modules settings in the <see cref="DoxInfo"/>.
        /// </summary>
        /// <param name="dInfo">The <see cref="DoxInfo"/> to edit</param>
        /// <param name="count">The number of modules we are going to add.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool SetModuleCountInDox( DoxInfo dInfo, int count, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "Setting Module count to " + count + ":", depth );
            }

            if ( dInfo.ModulesIndex != -1 )
            {
                dInfo.Lines[dInfo.ModulesIndex + 1] = "Count=" + count;
            }
            else
            {
                if ( m_projectSettings.VerboseLevel >= 3 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: ModulesIndex not set.\r\n", m_projectSettings.VerboseLevel >= 3 ? depth + 1 : depth );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Adds a new set of Module options with the given name.  When adding more than one, this should be called with
        /// names in reverse sorted order.
        /// </summary>
        /// <param name="dInfo">The <see cref="DoxInfo"/> to edit.</param>
        /// <param name="name">The name of the module.</param>
        /// <param name="topicID">The TopicID to assign to the Module.</param>
        /// <param name="vcprojFile">The full name and path of the .vcproj file defining the module.</param>
        /// <param name="moduleNumber">The module number for this class hierarchy.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool AddModuleToDox( DoxInfo dInfo, string name, string topicID, string vcprojFile, int moduleNumber, int depth )
        {
            OnOutputMessage( "Adding Module '" + name + "':", depth );

            // get the module's filenames
            List<string> filenames = new List<string>();
            if ( !ReadFilesFromVcprojFile( vcprojFile, filenames ) )
            {
                return false;
            }

            filenames.Sort();

            if ( !dInfo.AddModule( name, topicID, moduleNumber, filenames ) )
            {
                OnOutputMessage( "\r\n", 0 );
                OnOutputMessage( "Error: ModulesIndex not set.\r\n", depth + 1 );
                return false;
            }

            OnOutputMessage( " Complete.\r\n", 0 );

            return true;
        }

        /// <summary>
        /// Opens the vcproj file and adds all files included in the project the list.
        /// </summary>
        /// <param name="vcprojFile">The .vcproj file to open.</param>
        /// <param name="filenames">The list of filenames to add to.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool ReadFilesFromVcprojFile( string vcprojFile, List<string> filenames )
        {
            XmlTextReader reader = null;
            try
            {
                reader = new XmlTextReader( vcprojFile );
            }
            catch ( Exception )
            {
                return false;
            }

            while ( reader.Read() )
            {
                if ( !reader.IsStartElement() || reader.IsEmptyElement )
                {
                    continue;
                }
                
                if ( reader.Name == "VisualStudioProject" )
                {
                    while ( reader.Read() )
                    {
                        if ( !reader.IsStartElement() || reader.IsEmptyElement )
                        {
                            if ( reader.Name == "VisualStudioProject" )
                            {
                                break; // we're done
                            }
                        }
                        else if ( reader.Name == "Files" )
                        {
                            if ( !ReadFilesFromVcprojFile( reader, Path.GetDirectoryName( vcprojFile ), filenames ) )
                            {
                                return false;
                            }
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Recursively finds filenames in the XmlTextReader of a .vcproj file.
        /// </summary>
        /// <param name="reader">The <see cref="XmlTextReader"/> that is the .vcproj file.</param>
        /// <param name="vcprojDirectory">The directory of the .vcproj file so we can determine absolute path information.</param>
        /// <param name="filenames">The list of filenames to addd to.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool ReadFilesFromVcprojFile( XmlTextReader reader, string vcprojDirectory, List<string> filenames )
        {
            string parentName = reader.Name;

            while ( reader.Read() )
            {
                if ( !reader.IsStartElement() || reader.IsEmptyElement )
                {
                    if ( reader.Name == parentName )
                    {
                        break; // we're done
                    }
                }
                else if ( reader.Name == "File" )
                {
                    string filename = reader["RelativePath"];
                    if ( filename != null )
                    {
                        string ext = Path.GetExtension( filename ).ToLower();
                        if ( (ext == ".cpp") || (ext == ".h") || (ext == ".c") )
                        {
                            filenames.Add( Path.GetFullPath( Path.Combine( vcprojDirectory, filename ) ) );
                        }
                    }
                }
                else if ( reader.Name == "Filter" )
                {
                    if ( !ReadFilesFromVcprojFile( reader, vcprojDirectory, filenames ) )
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Adds the list of files to the Source Files section of the Dox file
        /// </summary>
        /// <param name="dInfo">The <see cref="DoxInfo"/> to edit.</param>
        /// <param name="files">A list of files to add.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool AddSourceFilesToDox( DoxInfo dInfo, List<string> files, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "Adding " + files.Count + " Source Files:", depth );
            }
            
            if ( !dInfo.AddSourceFiles( files ) )
            {
                if ( m_projectSettings.VerboseLevel >= 3 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: SourceFilesIndex not set.\r\n", m_projectSettings.VerboseLevel >= 3 ? depth + 1 : depth );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Creates the name of the Doc-O-Matic output directory for the given <see cref="HelpType"/>.
        /// </summary>
        /// <param name="name">The name of the module.  Used to when helpKind is html.</param>
        /// <param name="helpKind">The <see cref="HelpType"/> that is used to create the directory name when not html.</param>
        /// <param name="directory">The module directory.  Used to when helpKind is not html.</param>
        /// <param name="destinationDirectory">The help file destination directory.  Used to when helpKind is html.</param>
        /// <returns>The name of the output directory.</returns>
        private string BuildOutputDir( string name, HelpType helpKind, string directory, string destinationDirectory )
        {
            string dir = string.Empty;
            if ( helpKind == HelpType.html )
            {
                dir = Path.Combine( helpKind.ToString(), name );

                if ( destinationDirectory != null )
                {
                    dir = Path.Combine( destinationDirectory, dir );
                }
            }
            else
            {
                dir = "created_" + helpKind.ToString();

                if ( directory != null )
                {
                    dir = Path.Combine( directory, dir );
                }
            }

            return dir;
        }

        /// <summary>
        /// Creates the name of the Doc-O-Matic graphics directory for the given <see cref="HelpType"/>.
        /// </summary>
        /// <param name="name">The name of the module.  Used to when helpKind is html.</param>
        /// <param name="helpKind">The <see cref="HelpType"/> that is used to create the directory name when not html.</param>
        /// <param name="directory">The module directory.  Used to when helpKind is not html.</param>
        /// <param name="destinationDirectory">The help file destination directory.  Used to when helpKind is html.</param>
        /// <returns>The name of the output directory.</returns>
        private string BuildClassHierarchyLayoutOutputDir( string name, HelpType helpKind, string directory, string destinationDirectory )
        {
            string dir = BuildOutputDir( name, helpKind, directory, destinationDirectory );
            dir = Path.Combine( dir, "createdgraphics" );

            if ( !Path.IsPathRooted( dir ) )
            {
                return ".\\" + dir;
            }
            else
            {
                return dir;
            }
        }

        /// <summary>
        /// Writes over the dox file with the lines of text in <see cref="DoxInfo.Lines"/>.
        /// </summary>
        /// <param name="dInfo">The <see cref="DoxInfo"/> that contains the lines of text.</param>
        /// <param name="doxFile">The name of the file to save.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool OverwriteDoxFile( DoxInfo dInfo, string doxFile, int depth )
        {
            OnOutputMessage( "Overwriting '" + doxFile + "':", depth );

            bool success = true;
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter( doxFile, false, Encoding.Default );
                foreach ( string line in dInfo.Lines )
                {
                    writer.WriteLine( line );
                }
            }
            catch ( Exception e )
            {
                OnOutputMessage( "\r\n", 0 );
                OnOutputMessage( "Error: " + e.Message + ".\r\n", depth + 1 );
                success = false;
            }
            finally
            {
                if ( writer != null )
                {
                    writer.Close();
                }
            }

            if ( success )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return success;
        }

        /// <summary>
        /// Creates the Doc-O-Matic output and graphics directories that we specified in the edited dox file.
        /// </summary>
        /// <param name="name">The name of the module to create the directories for.</param>
        /// <param name="helpKind">The <see cref="HelpType"/> that we're building.</param>
        /// <param name="directory">The directory of the module.</param>
        /// <param name="destinationDirectory">The destination directory for the help files.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateOutputDirectories( string name, HelpType helpKind, string directory, string destinationDirectory, int depth )
        {
            OnOutputMessage( "Creating output directories:", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            // create the "doc\helpKind" directory
            string destDir = Path.Combine( destinationDirectory, helpKind.ToString() );
            if ( !CreateDirectory( destDir, false, depth + 1 ) )
            {
                return false;
            }

            // create "createdgraphics" directory
            string classDir = BuildClassHierarchyLayoutOutputDir( name, helpKind, directory, destinationDirectory );
            if ( !CreateDirectory( classDir, helpKind != HelpType.html, depth + 1 ) )
            {
                return false;
            }

            // create "created" directory
            string outputDir = BuildOutputDir( name, helpKind, directory, destinationDirectory );
            if ( !CreateDirectory( outputDir, helpKind != HelpType.html, depth + 1 ) )
            {
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Creates a directory
        /// </summary>
        /// <param name="directory">The directory to create.</param>
        /// <param name="deleteContents"><c>true</c> deletes the contents of the directory.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateDirectory( string directory, bool deleteContents, int depth )
        {
            if ( !Directory.Exists( directory ) )
            {
                try
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( "Creating directory '" + directory + "':", depth );
                    }

                    Directory.CreateDirectory( directory );

                    if ( deleteContents )
                    {
                        m_itemsToDelete.Add( directory.ToLower() );
                    }

                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( " Complete.\r\n", 0 );
                    }
                }
                catch ( Exception e )
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( "\r\n", 0 );
                    }

                    OnOutputMessage( "Error: " + e.Message + ".\r\n", depth + 1 );
                    return false;
                }
            }
            else if ( deleteContents )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "Deleting contents of directory '" + directory + "':", depth );
                }

                if ( m_projectSettings.VerboseLevel >= 3 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                if ( DeleteFiles( directory, depth + 1 ) )
                {
                    if ( m_projectSettings.VerboseLevel >= 3 )
                    {
                        OnOutputMessage( "Complete.\r\n", depth + 1 );
                    }
                    else if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( " Complete.\r\n", 0 );
                    }

                    m_itemsToDelete.Add( directory.ToLower() );
                }
                else
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( "Incomplete.\r\n", depth + 1 );
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Enables editing of the files that Doc-o-matic is about to generate.
        /// </summary>
        /// <param name="name">The name of the module.</param>
        /// <param name="helpKind">The <see cref="HelpType"/> we're building.</param>
        /// <param name="help2CompileResult">The value of the Help2CompileResult attribute.</param>
        /// <param name="rtfCreatePrintableDocumentation"><c>true</c> when we're building RTF type as opposed to HLP.</param>
        /// <param name="destinationDirectory">The directory where the files will be.</param>
        /// <param name="newFiles">Return a list of files that should be added to source control after the build.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool OpenOutputFilesForEdit( string name, HelpType helpKind, int help2CompileResult, bool rtfCreatePrintableDocumentation,
            string destinationDirectory, out List<string> newFiles, int depth )
        {
            newFiles = new List<string>();

            OnOutputMessage( "Opening output files for edit:\r\n", depth );

            string destDir = Path.Combine( destinationDirectory, helpKind.ToString() );
            List<string> destDirFilenames;
            if ( !BuildOutputFilenames( name, destDir, helpKind, help2CompileResult, rtfCreatePrintableDocumentation, out destDirFilenames ) )
            {                
                return false;
            }

            foreach ( string destDirFilename in destDirFilenames )
            {
                bool didNotExist;
                if ( OpenFileForEdit( destDirFilename, null, false, false, depth + 1, out didNotExist ) )
                {
                    if ( didNotExist )
                    {
                        newFiles.Add( destDirFilename );
                    }
                }
                else
                {
                    OnOutputMessage( "Warning: Unable to open '" + destDirFilename + "' for edit.\r\n", depth + 1 );
                }
            }

            OnOutputMessage( "Complete.\r\n", depth + 1 );
            return true;
        }

        /// <summary>
        /// Copies the Help2 files from the output directory to the destination.  Used for Unified Help files so that we may register them later.
        /// </summary>
        /// <param name="sourceDirectory">The directory where the files are.</param>
        /// <param name="destinationDirectory">The directory where the files should go.</param>
        /// <param name="help2CompileResult">The value of the Help2CompileResult attribute.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CopyHelp2FilesToDestination( string name, string sourceDirectory, string destinationDirectory, int help2CompileResult, int depth )
        {
            OnOutputMessage( "Copying output files to '" + destinationDirectory + "':", depth );

            bool needLineBreak = true;
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
                needLineBreak = false;
            }

            string destDir = Path.Combine( destinationDirectory, HelpType.help2.ToString() );
            List<string> destDirFilenames;
            if ( !BuildOutputFilenames( name, destDir, HelpType.help2, help2CompileResult, false, out destDirFilenames ) )
            {                
                return false;
            }

            foreach ( string destDirFilename in destDirFilenames )
            {
                string srcDirFilename = Path.Combine( sourceDirectory, Path.GetFileName( destDirFilename ) );
                if ( File.Exists( srcDirFilename ) )
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        if ( needLineBreak )
                        {
                            OnOutputMessage( "\r\n", 0 );
                            needLineBreak = false;
                        }

                        OnOutputMessage( "Copying '" + srcDirFilename + "' to '" + destDirFilename + "':", depth + 1 );
                    }

                    try
                    {
                        File.Copy( srcDirFilename, destDirFilename, true );
                    }
                    catch ( Exception e )
                    {
                        if ( needLineBreak )
                        {
                            OnOutputMessage( "\r\n", 0 );
                            needLineBreak = false;
                        }

                        OnOutputMessage( "Warning: " + e.Message + ".\r\n", depth + 1 );
                        continue;
                    }

                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        if ( needLineBreak )
                        {
                            OnOutputMessage( "\r\n", 0 );
                            needLineBreak = false;
                        }

                        OnOutputMessage( " Complete.\r\n", 0 );
                    }
                }
            }

            OnOutputMessage( "Complete.\r\n", depth + 1 );
            return true;
        }

        /// <summary>
        /// Adds the list of files to source control.
        /// </summary>
        /// <param name="filenames">The list of files to add.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool AddOutputFilesToSourceControl( List<string> filenames, int depth )
        {
            OnOutputMessage( "Adding output files to source control:\r\n", depth );

            bool error = false;
            foreach ( string filename in filenames )
            {
                if ( !AddToSourceControl( filename, depth + 1 ) )
                {
                    error = true;
                }
            }

            if ( error )
            {
                OnOutputMessage( "Incomplete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }

            // it's ok if some fail
            return true;
        }

        /// <summary>
        /// Edits the dox file in order to build the unified help files, builds them, and finally copies them to the destination
        /// </summary>
        /// <param name="name">The name to give the help files.</param>
        /// <param name="helpTypes">A list of <see cref="HelpType"/>s to build.</param>
        /// <param name="destinationDirectory">The destination folder for the help files.</param>
        /// <param name="disabledTopics">A list of topics to disable.</param>
        /// <param name="templateDoxFile">The template dox file to base edits off of.</param>
        /// <param name="addOutputFilesToSourceControl"><c>true</c> or <c>false</c>.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool BuildUnifiedHelpFiles( string name, List<HelpType> helpTypes, string destinationDirectory, List<string> disabledTopics,
            string templateDoxFile, bool addOutputFilesToSourceControl, int depth )
        {
            foreach ( HelpType helpKind in helpTypes )
            {
                if ( m_cancel )
                {
                    return true;
                }

                string doxFile = Path.Combine( destinationDirectory, name + ".dox" );

                OnOutputMessage( "Processing '" + doxFile + "':\r\n", depth );

                // open or create the dox from our template
                bool didNotExist;
                if ( !OpenFileForEdit( doxFile, templateDoxFile, true, true, depth + 1, out didNotExist ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_cancel )
                {
                    return true;
                }

                // read in dox file
                DoxInfo dInfo = ReadDoxFile( doxFile, depth + 1 );
                if ( dInfo == null )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_cancel )
                {
                    return true;
                }

                // these types need a config defined in the dox file, we can't "generate" one
                if ( (helpKind == HelpType.pdf) || (helpKind == HelpType.rtf) || (helpKind == HelpType.xml) )
                {
                    if ( !dInfo.ConfigurationsByHelpType.ContainsKey( helpKind ) )
                    {
                        OnOutputMessage( "Warning: '" + doxFile + "' needs a " + helpKind.ToString() + " configuration defined in order to build the help files.\r\n", depth + 1 );

                        ++m_buildResults.BuildsSkipped;
                        continue;
                    }
                }

                if ( !EditDoxFileConfiguration( name, false, destinationDirectory, destinationDirectory, dInfo, helpKind, disabledTopics, depth + 1 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_cancel )
                {
                    return true;
                }

                // write the new dox file over the old one
                if ( !OverwriteDoxFile( dInfo, doxFile, depth + 1 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_cancel )
                {
                    return true;
                }

                DoxConfigurationInfo dcInfo;
                if ( !dInfo.ConfigurationsByHelpType.TryGetValue( helpKind, out dcInfo ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                // open the  output files for editing
                List<string> newFiles = null;
                if ( !OpenOutputFilesForEdit( name, helpKind, dcInfo.Help2CompileResult, dcInfo.CreatePrintableDocumentation, 
                    destinationDirectory, out newFiles, 2 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_cancel )
                {
                    return true;
                }

                // create output directories
                if ( !CreateOutputDirectories( name, helpKind, destinationDirectory, destinationDirectory, depth + 1 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_cancel )
                {
                    return true;
                }

                // call dox to build the project
                string doxdbFile = Path.Combine( destinationDirectory, name + ".doxdb" );
                string outputFile = Path.Combine( BuildOutputDir( name, helpKind, destinationDirectory, destinationDirectory ), name + ".txt" );
                if ( !BuildHelpFiles( doxFile, doxdbFile, outputFile, false, m_projectSettings.ShowWhenFinishedBuilding, depth + 1 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_cancel )
                {
                    return true;
                }

                // for a single unified help2 file, we need to copy the remainder of the files manually
                if ( (helpKind == HelpType.help2)
                    && !CopyHelp2FilesToDestination( name, Path.GetDirectoryName( outputFile ), destinationDirectory, dcInfo.Help2CompileResult, 2 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if ( m_cancel )
                {
                    return true;
                }

                if ( addOutputFilesToSourceControl && !AddOutputFilesToSourceControl( newFiles, depth + 1 ) )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                ++m_buildResults.BuildsSucceeded;
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }

            // clean up intermediate files
            if ( !m_projectSettings.KeepIntermediateFiles )
            {
                CleanupIntermediateFiles( depth );
            }

            if ( m_cancel )
            {
                return true;
            }

            OnOutputMessage( "Complete.\r\n", depth );

            return true;
        }

        /// <summary>
        /// Calls Doc-O-Matic to build the help files.
        /// </summary>
        /// <param name="doxFile">The dox file to build.</param>
        /// <param name="doxdbFile">The doxdb file that should be marked for deletion.</param>
        /// <param name="outputFile">The name of the file where we will save the results from the Doc-O-Matic build.</param>
        /// <param name="qaWarnings"><c>true</c> when building in order to extract QA Warnings.</param>
        /// <param name="showWhenFinished"><c>true</c> or <c>false</c>.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool BuildHelpFiles( string doxFile, string doxdbFile, string outputFile,
            bool qaWarnings, bool showWhenFinished, int depth )
        {
            OnOutputMessage( "Exporting help files (Doc-O-Matic):\r\n", depth );

            // setup the command to execute
            rageExecuteCommand command = new rageExecuteCommand();
            command.Command = rageFileUtilities.GetProgramFilesx86() + "\\Doc-O-Matic 4 Professional\\dmcc.exe";

            if ( c_QaHelpKind == HelpType.chm )
            {
                // look for and shut down any instances of the help that is running
                Process[] processes = Process.GetProcessesByName( "hh" );
                string name = Path.GetFileNameWithoutExtension( doxFile );
                foreach ( Process process in processes )
                {
                    if ( process.MainWindowTitle.StartsWith( name ) )
                    {
                        process.Kill();
                        break;
                    }
                }
            }

            if ( qaWarnings )
            {
                command.Arguments = "-w2 " + (showWhenFinished ? "" : "-noshow ") + "\"" + doxFile + "\"";
                command.TimeOutWithoutOutputInSeconds = 60.0f;  // 1 minute
                command.TimeOutInSeconds = 60.0f * 5.0f;    // 5 minutes
            }
            else
            {
                string warnings = string.Empty;
                switch ( m_projectSettings.VerboseLevel )
                {
                    case 0:
                    case 1:
                        warnings = "-w3 ";
                        break;
                    case 2:
                        warnings = "-w2 ";
                        break;
                    case 3:
                    default:
                        warnings = "-w1 ";
                        break;
                }
                
                command.Arguments = warnings + (showWhenFinished ? "" : "-noshow ") + "\"" + doxFile + "\"";
                command.TimeOutInSeconds = m_projectSettings.UnifyHelpFiles ? (60.0f * 60.0f * 4.0f) : (60.0f * 20.0f); // 4 hours or 20 minutes
            }

            command.EchoToConsole = false;            
            command.LogToHtmlFile = false;            
            command.UseBusySpinner = false;
            command.WorkingDirectory = Path.GetDirectoryName( doxFile );

            // execute the command
            rageStatus status = new rageStatus();
            command.Execute( out status );

            // print the log
            List<string> log = command.GetLogAsArray();
            foreach ( string line in log )
            {
                string l = line.Trim();
                if ( l.StartsWith( "[" ) )
                {
                    l = l.Substring( l.IndexOf( ']' ) + 1 );
                }

                OnOutputMessage( l + "\r\n", depth + 1 );
            }

            if ( !status.Success() )
            {
                string reason = string.Empty;
                if ( status.GetErrorString().Contains( "process exited with error code" ) )
                {
                    switch ( command.GetExitCode() )
                    {
                        case 0:
                            // No error.
                            break;
                        case 20:
                            // Warnings (such as broken links)
                            break;
                        case 30:
                            // QA checks set up in the project failed.
                            break;
                        case 40:
                            reason = "Errors during the output phase.";
                            break;
                        case 41:
                            reason = "Fatal errors during output (usually indicates bugs).";
                            break;
                        case 50:
                            reason = "Output file cannot be created (locked by another application).";
                            break;
                        case 100:
                            reason = "Invalid command line (eg. wrong command line switch).";
                            break;
                        case 110:
                            reason = "The given project cannot be found or loaded.";
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    reason = status.GetErrorString();
                }

                if ( !String.IsNullOrEmpty( reason ) )
                {
                    OnOutputMessage( "Error: " + reason + "\r\n", depth + 1 );
                }
                else
                {
                    // this is actually a success
                    status = new rageStatus();
                }
            }

            try
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "Saving output file '" + outputFile + "':", depth + 1 );
                }

                StreamWriter writer = new StreamWriter( outputFile, false );

                foreach ( string line in log )
                {
                    writer.WriteLine( line );
                }

                writer.Close();

                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( " Complete.\r\n", 0 );
                }
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Warning: " + e.Message + ".\r\n", m_projectSettings.VerboseLevel >= 2 ? depth + 2 : depth + 1 );
            }

            if ( File.Exists( doxdbFile ) )
            {
                m_itemsToDelete.Add( doxdbFile.ToLower() );
            }

            if ( status.Success() )
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( "Fatal Error.\r\n", depth + 1 );
            }

            return status.Success();
        }

        /// <summary>
        /// Calls the QA Executable to extract the warnings that Doc-O-Matic generated.
        /// </summary>
        /// <param name="name">The module name.</param>
        /// <param name="helpKind">The <see cref="HelpType"/> we build.</param>
        /// <param name="srcDir">The directory of the module.</param>
        /// <param name="docDir">The doc directory containing the "qa" folder where the results of this operation will be stored.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool ExtractQAWarnings( string name, HelpType helpKind, string srcDir, string docDir, int depth )
        {
            OnOutputMessage( "Extracting QA warning information:\r\n", depth );

            string qa = m_projectSettings.QaExecutable;
            if ( !File.Exists( qa ) )
            {
                OnOutputMessage( "QA Executable '" + qa + "' does not exist.\r\n", depth + 1 );
                return false;
            }

            string outdir = Path.Combine( docDir, "qa" );

            string outputFile = Path.Combine( outdir, "qa_" + name + ".txt" );
            bool didNotExist = false;
            if ( !OpenFileForEdit( outputFile, null, false, false, depth + 1, out didNotExist ) )
            {
                return false;
            }

            string outputDir = BuildOutputDir( name, helpKind, srcDir, docDir );
            outputDir = ProjectSettings.GetRelativePath( srcDir, outputDir );

            StringBuilder args = new StringBuilder();
            args.Append( "/created_dir:" );
            args.Append( outputDir );
            args.Append( " /out_file:" );
            args.Append( outputFile );
            args.Append( " /template:" );
            args.Append( m_projectSettings.DtxTemplateFile );
            args.Append( " /module:" );
            args.Append( name );

            rageExecuteCommand command = new rageExecuteCommand();
            command.Command = m_projectSettings.QaExecutable;
            command.Arguments = args.ToString();
            command.EchoToConsole = false;
            command.LogToHtmlFile = true;
            command.UseBusySpinner = false;
            command.WorkingDirectory = srcDir;

            rageStatus status = new rageStatus();
            command.Execute( out status );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                List<string> lines = command.GetLogAsArray();
                foreach ( string line in lines )
                {
                    OnOutputMessage( line.Trim() + "\r\n", depth + 1 );
                }
            }

            if ( !status.Success() )
            {
                OnOutputMessage( "Error: " + status.GetErrorString() + "\r\n", depth + 1 );
                return false;
            }

            if ( didNotExist )
            {
                AddToSourceControl( outputFile, depth + 1 );
            }

            OnOutputMessage( "Complete.\r\n", depth + 1 );

            return true;
        }

        /// <summary>
        /// Creates a master index.html file in the given directory that links to all index.html files found in sub-directories
        /// </summary>
        /// <param name="docDirectory">The doc directory containing sub-directories of help files.  Example: c:\soft\rage\base\doc\html.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateIndexHtmlFile( string docDirectory, int depth )
        {
            string filename = Path.Combine( docDirectory, "index.html" );

            TextWriter writer = null;
            try
            {
                OnOutputMessage( "Creating '" + filename + "':", depth );

                writer = new StreamWriter( filename, false );
            }
            catch ( Exception e )
            {
                OnOutputMessage( "\r\n", 0 );
                OnOutputMessage( "Error: Unable to open or create '" + filename + "': " + e.Message + ".\r\n", depth + 1 );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
                OnOutputMessage( "Writing to '" + filename + "':", depth + 1 );
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            string[] directories = Directory.GetDirectories( docDirectory );
            
            // find a directory that we can pull the default.css and other items out of
            string assetDirName = string.Empty;
            if ( directories.Length > 0 )
            {
                string[] split = directories[0].Split( Path.DirectorySeparatorChar );
                if ( split.Length > 0 )
                {
                    assetDirName = split[split.Length - 1] + Path.DirectorySeparatorChar;
                }
            }

            writer.WriteLine( "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">" );
            writer.WriteLine( "<html>" );
            writer.WriteLine( "    <head>" );
            writer.WriteLine( "        <title>Index</title>" );
            writer.WriteLine( "        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\" />" );
            writer.WriteLine( "        <meta name=\"generator\" content=\"Doc-O-Matic\" />" );
            writer.WriteLine( "        <meta http-equiv=\"Content-Style-Type\" content=\"text/css\" />" );
            writer.WriteLine( "        <link rel=\"STYLESHEET\" href=\"" + assetDirName + "default.css\" type=\"text/css\" />" );
            writer.WriteLine( "        <script type=\"text/javascript\" src=\"" + assetDirName + "scripts.js\"></script>" );
            writer.WriteLine( "        <xml>" );
            writer.WriteLine( "            <MSHelp:Attr Name=\"Rockstar\" Value=\"Yes\"/>" );
            writer.WriteLine( "            <MSHelp:Attr Name=\"RockstarProject\" Value=\"rage\"/>" );
            writer.WriteLine( "        </xml>" );
            writer.WriteLine( "    </head>" );

            writer.WriteLine( "    <body class=\"Element700\" onload=\"onBodyLoad();\" onmousedown=\"onBodyMouseDown();\">" );
            
            writer.WriteLine( "        <!-- Begin Popups -->" );
            writer.WriteLine( "        <!-- End Popups -->" );

            writer.WriteLine( "        <!-- Begin Page Header -->" );
            writer.WriteLine( "        <div class=\"Element710\" id=\"areafixed\">" );
            writer.WriteLine( "            <div class=\"Element92\">" );
            writer.WriteLine( "                <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">" );
            writer.WriteLine( "                    <tr><td width=\"33%\">" );
            writer.WriteLine( "                    <div class=\"Element1\">" );
            writer.WriteLine( "                        <img src=\"" + assetDirName + "treelogo.jpg\" border=\"0\" alt=\"\"> Rage Documentation</div>" );
            writer.WriteLine( "                    </td><td width=\"34%\">" );
            writer.WriteLine( "                </td></tr></table>" );
            writer.WriteLine( "            </div>" );
            writer.WriteLine( "        </div>" );
            writer.WriteLine( "        <!-- End Page Header -->" );

            writer.WriteLine( "        <!-- Begin Client Area -->" );
            writer.WriteLine( "        <div class=\"Element720\" id=\"areascroll\">" );
            writer.WriteLine( "            <div class=\"Element721\">" );

            writer.WriteLine( "            <!-- Begin Page Content -->" );
            writer.WriteLine( "            <div class=\"Element58\">" );
            writer.WriteLine( "                <div class=\"Element25\">" );
            foreach ( string directory in directories )
            {
                string dirName = directory;
                string indexFile = Path.Combine( directory, "index.html" );
                
                string[] split = directory.Split( Path.DirectorySeparatorChar );
                if ( split.Length > 0 )
                {
                    dirName = split[split.Length - 1];
                    indexFile = split[split.Length - 1] + Path.DirectorySeparatorChar + "index.html";
                }

                writer.WriteLine( "                <div class=\"Element49\">" );
                writer.WriteLine( "                    <a href=\"" + indexFile + "\">" + dirName + "</a></div>" );

                if ( m_projectSettings.VerboseLevel >= 3 )
                {
                    OnOutputMessage( "Linked to '" + indexFile + "'.\r\n", depth + 2 );
                }
            }
            writer.WriteLine( "            <!-- End Page Content -->" );

            writer.WriteLine( "            <!-- Begin Page Footer -->" );
            writer.WriteLine( "            <div class=\"Element93\">" );
            writer.WriteLine( "                <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">" );
            writer.WriteLine( "                    <tr><td width=\"100%\">" );
            writer.WriteLine( "                    <div class=\"Element3\">" );
            writer.WriteLine( "                        <img src=\"" + assetDirName + "rslogo.jpg\" border=\"0\" alt=\"\"> Copyright (c) 2003-2007 Rockstar San Diego. All rights reserved.</div>" );
            writer.WriteLine( "                    </td></tr><tr><td width=\"100%\">" );
            writer.WriteLine( "            </td></tr></table></div>" );
            writer.WriteLine( "            <!-- End Page Footer -->" );

            writer.WriteLine( "            </div>" );
            writer.WriteLine( "        </div>" );
            writer.WriteLine( "        <!-- End Client Area -->" );
            
            writer.WriteLine( "    </body>" );
            writer.WriteLine( "</html>" );

            writer.Close();

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 2 );
            }
            else if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Deletes, force reverts, and/or reverts intermediate files that we've marked as such.
        /// </summary>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CleanupIntermediateFiles( int depth )
        {
            if ( (m_itemsToDelete.Count == 0) && (m_itemsToForceRevert.Count == 0) 
                && (m_itemsToRevert.Count == 0) && (m_itemsToOverwrite.Count == 0) )
            {
                return true;
            }

            OnOutputMessage( "Cleaning up intermediate files:", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            foreach ( KeyValuePair<string, string> pair in m_itemsToOverwrite )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "Copying over '" + pair.Key + "' for Source Control submission:", depth + 1 );
                }

                try
                {
                    File.Copy( pair.Value, pair.Key, true );
                }
                catch ( Exception e )
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( "\r\n", 0 );
                    }

                    OnOutputMessage( "Error: Unable to copy over '" + pair.Key + "': " + e.Message + ".\r\n",
                        m_projectSettings.VerboseLevel >= 2 ? depth + 2 : depth + 1 );
                    continue;
                }

                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( " Complete.\r\n", 0 );
                }
            }

            foreach ( string file in m_itemsToForceRevert )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "Reverting '" + file + "':", depth + 1 );
                }

                rageStatus status;
                m_projectSettings.SourceControlProviderSettings.Revert( file, out status );
                if ( !status.Success() )
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( "\r\n", 0 );
                    }

                    OnOutputMessage( "Warning: Unable to revert '" + file + "': " + status.ErrorString + ".\r\n", depth + 1 );
                }
                else
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( " Complete.\r\n", 0 );
                    }
                }
            }

            foreach ( string file in m_itemsToRevert )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "Reverting '" + file + "' if unchanged:", depth + 1 );
                }

                DateTime dt = File.GetLastWriteTime( file );

                rageStatus status;
                m_projectSettings.SourceControlProviderSettings.RevertIfUnchanged( file, out status );
                if ( !status.Success() )
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        OnOutputMessage( "\r\n", 0 );
                    }

                    OnOutputMessage( "Warning: Unable to revert '" + file + "' if it was unchanged: " + status.ErrorString + ".\r\n", depth + 1 );
                }
                else
                {
                    if ( m_projectSettings.VerboseLevel >= 2 )
                    {
                        if ( !File.Exists( file ) )
                        {
                            OnOutputMessage( " Deleted.\r\n", 0 );
                        }
                        else if ( File.GetLastWriteTime( file ) > dt )
                        {
                            OnOutputMessage( " Reverted.\r\n", 0 );
                        }
                        else
                        {
                            OnOutputMessage( " Complete.\r\n", 0 );
                        }
                    }
                }
            }

            foreach ( string item in m_itemsToDelete )
            {
                if ( File.Exists( item ) )
                {
                    DeleteFile( item, m_projectSettings.VerboseLevel >= 2, depth + 1 );
                }
                else if ( Directory.Exists( item ) )
                {
                    try
                    {
                        if ( m_projectSettings.VerboseLevel >= 2 )
                        {
                            OnOutputMessage( "Deleting directory '" + item + "':", depth + 1 );
                        }

                        if ( m_projectSettings.VerboseLevel >= 3 )
                        {
                            OnOutputMessage( "\r\n", 0 );
                        }

                        if ( DeleteFiles( item, depth + 2 ) )
                        {
                            Directory.Delete( item );

                            if ( m_projectSettings.VerboseLevel >= 3 )
                            {
                                OnOutputMessage( "Complete.\r\n", depth + 2 );
                            }
                            else if ( m_projectSettings.VerboseLevel >= 2 )
                            {
                                OnOutputMessage( " Complete.\r\n", 0 );
                            }
                        }
                        else
                        {
                            if ( m_projectSettings.VerboseLevel >= 2 )
                            {
                                OnOutputMessage( "Incomplete.\r\n", depth + 2 );
                            }
                        }
                    }
                    catch ( Exception e )
                    {
                        if ( m_projectSettings.VerboseLevel == 2 )
                        {
                            OnOutputMessage( "\r\n", 0 );
                        }

                        OnOutputMessage( "Warning: Unable to delete directory '" + item + "': " + e.Message + ".\r\n", depth + 2 );
                    }
                }
            }

            m_itemsToDelete.Clear();
            m_itemsToForceRevert.Clear();
            m_itemsToRevert.Clear();
            m_itemsToOverwrite.Clear();

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Deletes all files from the given directory.
        /// </summary>
        /// <param name="directory">The directory to delete files from.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool DeleteFiles( string directory, int depth )
        {
            bool success = true;

            string[] filenames = Directory.GetFiles( directory );
            foreach ( string filename in filenames )
            {
                if ( !DeleteFile( filename, m_projectSettings.VerboseLevel >= 3, depth ) )
                {
                    success = false;
                }
            }

            return success;
        }

        /// <summary>
        /// Deletes the given file.
        /// </summary>
        /// <param name="filename">The file to delete.</param>
        /// <param name="displayMessages"><c>true</c> if we wish to display message about the process, otherwise <c>false</c>.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool DeleteFile( string filename, bool displayMessages, int depth )
        {
            try
            {
                if ( displayMessages )
                {
                    OnOutputMessage( "Deleting '" + filename + "':", depth );
                }

                File.SetAttributes( filename, File.GetAttributes( filename ) & (~FileAttributes.ReadOnly) );
                File.Delete( filename );

                if ( displayMessages )
                {
                    OnOutputMessage( " Complete.\r\n", 0 );
                }
            }
            catch ( Exception e )
            {
                if ( displayMessages )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Warning: Unable to delete '" + filename + "': " + e.Message + ".\r\n", depth + 1 );
                return false;
            }

            return true;
        }

        #region Help 2 Registration

        /// <summary>
        /// Installs the Single Unified Help 2 file located in the given directory.
        /// </summary>
        /// <param name="outputDirectory">The directory that contains the help file to register.</param>
        private void InstallSingleUnifiedHelp2File( string outputDirectory, SortedDictionary<string, SortedDictionary<string, string>> docDirDict )
        {

            // the actual destinationDirectory
            string destinationDirectory = Path.Combine( outputDirectory, HelpType.help2.ToString() );
            if ( !Directory.Exists( destinationDirectory ) )
            {
                OnOutputMessage( "Error: the expected output help2 directory '" + destinationDirectory + "' doesn't exist.\r\n", 0 );

                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            OnOutputMessage( "Processing '" + destinationDirectory + "':\r\n", 0 );

            // build the merged help2 layout
            SortedDictionary<string, string> hxsHxiFilenames = new SortedDictionary<string, string>();

            HelpLayout masterLayout = new HelpLayout();            
            foreach ( KeyValuePair<string, SortedDictionary<string, string>> pair in docDirDict )
            {
                bool isOverview;

                // get the HxS and HxI files in this directory, as well as the module names
                Dictionary<string, string> moduleNames = new Dictionary<string, string>();

                foreach ( string directory in pair.Value.Values )
                {
                    string moduleName = GetModuleName( directory, out isOverview );
                    moduleNames.Add( moduleName, moduleName );

                    // get the HxS and HxI files in this directory          
                    string hxsFilename = Path.Combine( destinationDirectory, moduleName + ".HxS" );
                    if ( !File.Exists( hxsFilename ) )
                    {
                        OnOutputMessage( "Error: the expected unified help2 file '" + hxsFilename + "' doesn't exist.\r\n", 0 );

                        ++m_buildResults.BuildsFailed;

                        if ( m_projectSettings.HaltOnError )
                        {
                            return;
                        }
                    }

                    string hxiFilename = Path.Combine( destinationDirectory, moduleName + ".HxI" );
                    if ( File.Exists( hxiFilename ) )
                    {
                        hxsHxiFilenames.Add( hxsFilename, hxiFilename );
                    }
                    else
                    {
                        hxsHxiFilenames.Add( hxsFilename, hxsFilename );
                    }

                    if ( m_cancel )
                    {
                        return;
                    }
                }

                HelpLayout layout = GetHelpLayout( pair.Key, moduleNames, docDirDict.Count > 1, 0 );
                if ( layout == null )
                {
                    m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                // merge with our master layout
                foreach ( HelpLayoutItem item in layout.Items )
                {
                    masterLayout.Items.Add( item );
                }
            }

            // store the install files in the output directory
            string installDirectory = Path.Combine( outputDirectory, "help2_install" );
            if ( !Directory.Exists( installDirectory ) )
            {
                Directory.CreateDirectory( installDirectory );
            }

            if ( !CreateMasterHelp2Files( m_projectSettings.UnifiedHelpFileName, installDirectory, masterLayout, hxsHxiFilenames, 0 ) )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            bool success = RegisterHelpFiles( m_projectSettings.UnifiedHelpFileName, installDirectory, 0 );
            if ( !success )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;                       
            }

            success = RegisterSearchFilters( 0 );
            if ( !success )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            if ( m_projectSettings.ShowWhenFinishedBuilding )
            {
                OpenHelp2Help();
                
                if ( m_cancel )
                {
                    return;
                }
            }

            // clean up intermediate files
            if ( !m_projectSettings.KeepIntermediateFiles )
            {
                CleanupIntermediateFiles( 0 );
            }

            if ( success )
            {
                ++m_buildResults.BuildsSucceeded;
            }

            OnOutputMessage( "Complete.\r\n", 1 );
        }

        /// <summary>
        /// Installs the Unified Help 2 files located in the given directories.
        /// </summary>
        /// <param name="docDirectories">A list of doc Directories.</param>
        private void InstallUnifiedHelp2File( SortedDictionary<string, SortedDictionary<string, string>> docDirDict )
        {
            SortedDictionary<string, string> hxsHxiFilenames = new SortedDictionary<string, string>();

            HelpLayout masterLayout = new HelpLayout();
            foreach ( KeyValuePair<string, SortedDictionary<string, string>> pair in docDirDict  )
            {
                if ( m_cancel )
                {
                    break;
                }

                // the actual destinationDirectory
                string destinationDirectory = Path.Combine( pair.Key, HelpType.help2.ToString() );
                if ( !Directory.Exists( destinationDirectory ) )
                {
                    OnOutputMessage( "Error: the expected doc help2 directory '" + destinationDirectory + "' doesn't exist.\r\n", 0 );

                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                OnOutputMessage( "Processing '" + destinationDirectory + "':\r\n", 0 );

                bool isOverview;

                // get the HxS and HxI files in this directory, as well as the module names
                Dictionary<string, string> moduleNames = new Dictionary<string, string>();

                foreach ( string directory in pair.Value.Values )
                {
                    string moduleName = GetModuleName( directory, out isOverview );
                    moduleNames.Add( moduleName, moduleName );

                    string hxsFilename = Path.Combine( destinationDirectory, moduleName + ".HxS" );
                    if ( !File.Exists( hxsFilename ) )
                    {
                        OnOutputMessage( "Error: the expected unified help2 file '" + hxsFilename + "' doesn't exist.\r\n", 0 );

                        ++m_buildResults.BuildsFailed;

                        if ( m_projectSettings.HaltOnError )
                        {
                            return;
                        }
                    }

                    string hxiFilename = Path.Combine( destinationDirectory, moduleName + ".HxI" );
                    if ( File.Exists( hxiFilename ) )
                    {
                        hxsHxiFilenames.Add( hxsFilename, hxiFilename );
                    }
                    else
                    {
                        hxsHxiFilenames.Add( hxsFilename, hxsFilename );
                    }

                    if ( m_cancel )
                    {
                        return;
                    }
                }

                // Get the help layout for this docDirectory
                HelpLayout layout = GetHelpLayout( pair.Key, moduleNames, docDirDict.Count > 1, 0 );
                if ( layout == null )
                {
                    m_buildResults.BuildsFailed += (pair.Value.Count * m_buildResults.BuildsPerDirectory);

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                // merge with our master layout
                foreach ( HelpLayoutItem item in layout.Items )
                {
                    masterLayout.Items.Add( item );
                }
            }

            // let's choose the first doc directory to store the files in, and create a subdirectory under it
            string firstKey = string.Empty;
            foreach ( string key in docDirDict.Keys )
            {
                firstKey = key;
                break;
            }

            string installDirectory = Path.Combine( firstKey, "help2_install" );
            if ( !Directory.Exists( installDirectory ) )
            {
                Directory.CreateDirectory( installDirectory );
            }

            if ( !CreateMasterHelp2Files( "unified_master", installDirectory, masterLayout, hxsHxiFilenames, 0 ) )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            bool success = RegisterHelpFiles( "unified_master", installDirectory, 0 );
            if ( !success )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            success = RegisterSearchFilters( 0 );
            if ( !success )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            if ( m_projectSettings.ShowWhenFinishedBuilding )
            {
                OpenHelp2Help();

                if ( m_cancel )
                {
                    return;
                }
            }

            // clean up intermediate files
            if ( !m_projectSettings.KeepIntermediateFiles )
            {
                CleanupIntermediateFiles( 0 );
            }

            if ( success )
            {
                ++m_buildResults.BuildsSucceeded;
            }

            OnOutputMessage( "Complete.\r\n", 1 );
        }

        /// <summary>
        /// Installs the Help 2 files located in the given directories.
        /// </summary>
        /// <param name="docDirectories">A list of doc Directories.</param>
        private void InstallHelp2File( List<string> docDirectories )
        {
            HelpLayout masterLayout = new HelpLayout();
            SortedDictionary<string, string> hxsHxiFilenames = new SortedDictionary<string,string>();

            foreach ( string docDirectory in docDirectories )
            {
                if ( m_cancel )
                {
                    break;
                }

                // the actual destinationDirectory
                string destinationDirectory = Path.Combine( docDirectory, HelpType.help2.ToString() );
                if ( !Directory.Exists( destinationDirectory ) )
                {
                    OnOutputMessage( "Error: the expected doc help2 directory '" + destinationDirectory + "' doesn't exist.\r\n", 0 );

                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                OnOutputMessage( "Processing '" + destinationDirectory + "':\r\n", 0 );

                // get the HxS and HxI files in this directory, as well as the module names
                Dictionary<string, string> moduleNames = new Dictionary<string, string>();

                string[] hxsFilenames = Directory.GetFiles( destinationDirectory, "*.HxS" );
                foreach ( string hxsFilename in hxsFilenames )
                {
                    string moduleName = Path.GetFileNameWithoutExtension( hxsFilename );
                    moduleNames.Add( moduleName, moduleName );

                    string hxiFilename = Path.Combine( destinationDirectory, moduleName + ".HxI" );
                    if ( File.Exists( hxiFilename ) )
                    {
                        hxsHxiFilenames.Add( hxsFilename, hxiFilename );
                    }
                    else
                    {
                        hxsHxiFilenames.Add( hxsFilename, hxsFilename );
                    }
                }

                if ( m_cancel )
                {
                    break;
                }

                // get the HelpLayout for this tree
                HelpLayout layout = GetHelpLayout( docDirectory, moduleNames, docDirectories.Count > 1, 1 );
                if ( layout == null )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                // merge with our master layout
                foreach ( HelpLayoutItem item in layout.Items )
                {
                    masterLayout.Items.Add( item );
                }

                if ( m_cancel )
                {
                    break;
                }
            }

            // let's choose the first doc directory to store the files in, and create a subdirectory under it
            string installDirectory = Path.Combine( docDirectories[0], "help2_install" );
            if ( !Directory.Exists( installDirectory ) )
            {
                Directory.CreateDirectory( installDirectory );
            }

            if ( !CreateMasterHelp2Files( "master", installDirectory, masterLayout, hxsHxiFilenames, 0 ) )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            bool success = RegisterHelpFiles( "master", installDirectory, 0 );
            if ( !success )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            success = RegisterSearchFilters( 0 );
            if ( !success )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            if ( m_projectSettings.ShowWhenFinishedBuilding )
            {
                OpenHelp2Help();

                if ( m_cancel )
                {
                    return;
                }
            }

            // clean up intermediate files
            if ( !m_projectSettings.KeepIntermediateFiles )
            {
                CleanupIntermediateFiles( 0 );
            }

            if ( success )
            {
                ++m_buildResults.BuildsSucceeded;
            }

            OnOutputMessage( "Complete.\r\n", 1 );
        }

        private void OpenHelp2Help()
        {
            ProcessStartInfo psInfo = new ProcessStartInfo(rageFileUtilities.GetProgramFilesx86() + @"\Common Files\Microsoft Shared\Help 8\dexplore.exe", 
                String.Format( "/usehelpsettings VisualStudio.8.0 /helpcol ms-help://{0}", m_projectSettings.TreeNamespace ) );
            Process.Start( psInfo );
        }

        /// <summary>
        /// Uninstalls the Single Unified Help 2 file located in the given directory.
        /// </summary>
        /// <param name="outputDirectory">The directory that contains the help file to register.</param>
        private void UnInstallSingleUnifiedHelp2File( string outputDirectory )
        {
            HelpLayout masterLayout = new HelpLayout();
            SortedDictionary<string, string> hxsHxiFilenames = new SortedDictionary<string, string>();

            // the actual destinationDirectory
            string destinationDirectory = Path.Combine( outputDirectory, HelpType.help2.ToString() );
            if ( !Directory.Exists( destinationDirectory ) )
            {
                OnOutputMessage( "Error: the expected output help2 directory '" + destinationDirectory + "' doesn't exist.\r\n", 0 );

                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            OnOutputMessage( "Processing '" + destinationDirectory + "':\r\n", 0 );

            // get the HxS and HxI files in this directory, as well as the module names
            Dictionary<string, string> moduleNames = new Dictionary<string, string>();
            moduleNames.Add( m_projectSettings.UnifiedHelpFileName, m_projectSettings.UnifiedHelpFileName );

            string hxsFilename = Path.Combine( destinationDirectory, m_projectSettings.UnifiedHelpFileName + ".HxS" );
            if ( !File.Exists( hxsFilename ) )
            {
                OnOutputMessage( "Error: the expected unified help2 file '" + hxsFilename + "' doesn't exist.\r\n", 0 );

                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            string hxiFilename = Path.Combine( destinationDirectory, m_projectSettings.UnifiedHelpFileName + ".HxI" );
            if ( File.Exists( hxiFilename ) )
            {
                hxsHxiFilenames.Add( hxsFilename, hxiFilename );
            }
            else
            {
                hxsHxiFilenames.Add( hxsFilename, hxsFilename );
            }

            if ( m_cancel )
            {
                return;
            }

            // the layout is simple, just the one module            
            masterLayout.Items.Add( new HelpLayoutModule( m_projectSettings.UnifiedHelpFileName ) );

            // let's choose the output directory to store the files in, and create a subdirectory under it
            string installDirectory = Path.Combine( outputDirectory, "help2_install" );
            if ( !Directory.Exists( installDirectory ) )
            {
                Directory.CreateDirectory( installDirectory );
            }

            if ( !CreateMasterHelp2Files( m_projectSettings.UnifiedHelpFileName, installDirectory, masterLayout, hxsHxiFilenames, 0 ) )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            bool success = UnregisterHelpFiles( m_projectSettings.UnifiedHelpFileName, installDirectory, 0 );
            if ( !success )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            success = UnregisterSearchFilters( 0 );
            if ( !success )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            // clean up intermediate files
            if ( !m_projectSettings.KeepIntermediateFiles )
            {
                CleanupIntermediateFiles( 0 );
            }

            if ( success )
            {
                ++m_buildResults.BuildsSucceeded;
            }

            OnOutputMessage( "Complete.\r\n", 1 );
        }

        /// <summary>
        /// Uninstalls the Unified Help 2 files located in the given directories.
        /// </summary>
        /// <param name="docDirectories">A list of doc Directories.</param>
        private void UnInstallUnifiedHelp2File( List<string> docDirectories )
        {
            HelpLayout masterLayout = new HelpLayout();
            SortedDictionary<string, string> hxsHxiFilenames = new SortedDictionary<string, string>();

            foreach ( string docDirectory in docDirectories )
            {
                if ( m_cancel )
                {
                    break;
                }

                // the actual destinationDirectory
                string destinationDirectory = Path.Combine( docDirectory, HelpType.help2.ToString() );
                if ( !Directory.Exists( destinationDirectory ) )
                {
                    OnOutputMessage( "Error: the expected doc help2 directory '" + destinationDirectory + "' doesn't exist.\r\n", 0 );

                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                OnOutputMessage( "Processing '" + destinationDirectory + "':\r\n", 0 );

                // try to extract a suitable name
                string name = GetBranchName( docDirectory );
                if ( name != string.Empty )
                {
                    name = name + " " + m_projectSettings.UnifiedHelpFileName;
                }
                else
                {
                    name = m_projectSettings.UnifiedHelpFileName;
                }

                // get the HxS and HxI file in this directory
                string hxsFilename = Path.Combine( destinationDirectory, name + ".HxS" );
                if ( !File.Exists( hxsFilename ) )
                {
                    OnOutputMessage( "Error: the expected unified help2 file '" + hxsFilename + "' doesn't exist.\r\n", 0 );

                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        return;
                    }
                }

                string hxiFilename = Path.Combine( destinationDirectory, name + ".HxI" );
                if ( File.Exists( hxiFilename ) )
                {
                    hxsHxiFilenames.Add( hxsFilename, hxiFilename );
                }
                else
                {
                    hxsHxiFilenames.Add( hxsFilename, hxsFilename );
                }

                if ( m_cancel )
                {
                    return;
                }

                // the layout is simple, just add the one module
                masterLayout.Items.Add( new HelpLayoutModule( name ) );

                if ( m_cancel )
                {
                    break;
                }
            }

            // let's choose the first doc directory to store the files in, and create a subdirectory under it
            string installDirectory = Path.Combine( docDirectories[0], "help2_install" );
            if ( !Directory.Exists( installDirectory ) )
            {
                Directory.CreateDirectory( installDirectory );
            }

            if ( !CreateMasterHelp2Files( "unified_master", installDirectory, masterLayout, hxsHxiFilenames, 0 ) )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            if ( !UnregisterSearchFilters( 0 ) )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            bool success = UnregisterHelpFiles( "unified_master", installDirectory, 0 );
            if ( !success )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            // clean up intermediate files
            if ( !m_projectSettings.KeepIntermediateFiles )
            {
                CleanupIntermediateFiles( 0 );
            }

            if ( success )
            {
                ++m_buildResults.BuildsSucceeded;
            }

            OnOutputMessage( "Complete.\r\n", 1 );
        }

        /// <summary>
        /// Installs the Help 2 files located in the given directories.
        /// </summary>
        /// <param name="docDirectories">A list of doc Directories.</param>
        private void UnInstallHelp2File( List<string> docDirectories )
        {
            HelpLayout masterLayout = new HelpLayout();
            SortedDictionary<string, string> hxsHxiFilenames = new SortedDictionary<string, string>();

            foreach ( string docDirectory in docDirectories )
            {
                if ( m_cancel )
                {
                    break;
                }

                // the actual destinationDirectory
                string destinationDirectory = Path.Combine( docDirectory, HelpType.help2.ToString() );
                if ( !Directory.Exists( destinationDirectory ) )
                {
                    OnOutputMessage( "Error: the expected doc help2 directory '" + destinationDirectory + "' doesn't exist.\r\n", 0 );

                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                OnOutputMessage( "Processing '" + destinationDirectory + "':\r\n", 0 );

                // get the HxS and HxI files in this directory, as well as the module names
                Dictionary<string, string> moduleNames = new Dictionary<string, string>();

                string[] hxsFilenames = Directory.GetFiles( destinationDirectory, "*.HxS" );
                foreach ( string hxsFilename in hxsFilenames )
                {
                    string moduleName = Path.GetFileNameWithoutExtension( hxsFilename );
                    moduleNames.Add( moduleName, moduleName );

                    string hxiFilename = Path.Combine( destinationDirectory, moduleName + ".HxI" );
                    if ( File.Exists( hxiFilename ) )
                    {
                        hxsHxiFilenames.Add( hxsFilename, hxiFilename );
                    }
                    else
                    {
                        hxsHxiFilenames.Add( hxsFilename, hxsFilename );
                    }
                }

                if ( m_cancel )
                {
                    break;
                }

                // get the HelpLayout for this tree
                HelpLayout layout = GetHelpLayout( docDirectory, moduleNames, docDirectories.Count > 1, 1 );
                if ( layout == null )
                {
                    ++m_buildResults.BuildsFailed;

                    if ( m_projectSettings.HaltOnError )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                // merge with our master layout
                foreach ( HelpLayoutItem item in layout.Items )
                {
                    masterLayout.Items.Add( item );
                }

                if ( m_cancel )
                {
                    break;
                }
            }

            // let's choose the first doc directory to store the files in, and create a subdirectory under it
            string installDirectory = Path.Combine( docDirectories[0], "help2_install" );
            if ( !Directory.Exists( installDirectory ) )
            {
                Directory.CreateDirectory( installDirectory );
            }

            if ( !CreateMasterHelp2Files( "master", installDirectory, masterLayout, hxsHxiFilenames, 0 ) )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            bool success = UnregisterHelpFiles( "master", installDirectory, 0 );
            if ( !success )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            success = UnregisterSearchFilters( 0 );
            if ( !success )
            {
                ++m_buildResults.BuildsFailed;

                if ( m_projectSettings.HaltOnError )
                {
                    return;
                }
            }

            if ( m_cancel )
            {
                return;
            }

            // clean up intermediate files
            if ( !m_projectSettings.KeepIntermediateFiles )
            {
                CleanupIntermediateFiles( 0 );
            }

            if ( success )
            {
                ++m_buildResults.BuildsSucceeded;
            }

            OnOutputMessage( "Complete.\r\n", 1 );
        }

        /// <summary>
        /// Creates the master Help 2 files that will be used to register the help with Visual Studio.
        /// </summary>
        /// <param name="name">The name of the collection-level help file.</param>
        /// <param name="directory">The directory that contains all of the Help 2 files.</param>
        /// <param name="layout">The <see cref="HelpLayout"/> structure defining the hierarchy of nodes and TOCs.</param>        
        /// <param name="hxsHxiFilenames">A <see cref="SortedDictionary"/> of the HxS and HxI files to be registered.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateMasterHelp2Files( string name, string directory, 
            HelpLayout layout, SortedDictionary<string, string> hxsHxiFilenames, int depth )
        {
            OnOutputMessage( "Creating master help files:\r\n", depth );

            if ( !CreateHxT( Path.Combine( directory, name + ".HxT" ), layout, depth + 1 ) )
            {
                return false;
            }

            if ( m_cancel )
            {
                return true;
            }

            if ( !CreateHxC( Path.Combine( directory, name + ".HxC" ), depth + 1 ) )
            {
                return false;
            }

            if ( m_cancel )
            {
                return true;
            }

            if ( !CreateHxA( Path.Combine( directory, name + ".HxA" ), depth + 1 ) )
            {
                return false;
            }

            if ( m_cancel )
            {
                return true;
            }

#if CREATE_HXQ_AND_HXR_FILES
            string hxfFilename = Path.Combine( directory, name + ".HxF" );
            if ( !CreateHxF( hxfFilename, hxsHxiFilenames, depth + 1 ) )
            {
                return false;
            }

            if ( m_cancel )
            {
                return true;
            }

            string hxmQFilename = Path.Combine( directory, name + "Q.HxM" );
            if ( !CreateHxM( hxmQFilename, Path.GetFileName( hxfFilename ), name + ".HxQ", "FTS", depth + 1 ) )
            {
                return false;
            }

            if ( m_cancel )
            {
                return true;
            }

            if ( !CreateQueryIndexFile( hxmQFilename, depth + 1 ) )
            {
                return false;
            }

            if ( m_cancel )
            {
                return true;
            }

            string hxmRFilename = Path.Combine( directory, name + "R.HxM" );
            if ( !CreateHxM( hxmRFilename, Path.GetFileName( hxfFilename ), name + ".HxR", "Attr", depth + 1 ) )
            {
                return false;
            }

            if ( m_cancel )
            {
                return true;
            }

            if ( !CreateQueryIndexFile( hxmRFilename, depth + 1 ) )
            {
                return false;
            }

            if ( m_cancel )
            {
                return true;
            }
#endif

            if ( !CreateHxK( Path.Combine( directory, name + "_A.HxK" ), "A", false, depth + 1 ) )
            {
                return false;
            }

            if ( m_cancel )
            {
                return true;
            }

            if ( !CreateHxK( Path.Combine( directory, name + "_F.HxK" ), "F", false, depth + 1 ) )
            {
                return false;
            }

            if ( m_cancel )
            {
                return true;
            }

            if ( !CreateHxK( Path.Combine( directory, name + "_K.HxK" ), "K", true, depth + 1 ) )
            {
                return false;
            }

            if ( m_cancel )
            {
                return true;
            }

            if ( !CreateHxK( Path.Combine( directory, name + "_NamedURL.HxK" ), "NamedURLIndex", false, depth + 1 ) )
            {
                return false;
            }

            if ( m_cancel )
            {
                return true;
            }

            if ( !CreateH2RegIni( Path.Combine( directory, "H2Reg.ini" ), name, hxsHxiFilenames, depth + 1 ) )
            {
                return false;
            }

            OnOutputMessage( "Complete.\r\n", depth + 1 );

            return true;
        }

        /// <summary>
        /// Creates a master HxT file for Help 2 registration.
        /// </summary>
        /// <param name="filename">The name of the HxT file to create.</param>
        /// <param name="layout">The <see cref="HelpLayout"/> that describes the hierarchy of node and TOCs.</param>
        /// <param name="hxsHxiFilenames">A <see cref="SortedDictionary"/> of the HxS and HxI files that form the basis of the help.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateHxT( string filename, HelpLayout layout, int depth )
        {
            OnOutputMessage( "Creating '" + filename + "':", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            bool didNotExist;
            if ( !OpenFileForEdit( filename, null, false, false, depth + 1, out didNotExist ) )
            {
                return false;
            }

            // create one from scratch
            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel < 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: Could not open/create '" + filename + "': " + e.Message + ".\r\n", depth + 1 );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "Writing to '" + filename + "':", depth + 1 );
            }

            // header
            writer.WriteLine( "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" );
            writer.WriteLine( "<!DOCTYPE HelpTOC>" );

            // body
            writer.WriteLine( "<HelpTOC DTDVersion=\"1.0\" PluginStyle=\"Hierarchical\" PluginTitle=\"" + m_projectSettings.ProjectDescription + "\">" );
            foreach ( HelpLayoutItem item in layout.Items )
            {
                WriteHxT( writer, item, 1 );
            }
            writer.WriteLine( "</HelpTOC>" );

            writer.Close();

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Creates a master HxC file for Help 2 registration.
        /// </summary>
        /// <param name="filename">The name of the HxC file to create.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateHxC( string filename, int depth )
        {
            OnOutputMessage( "Creating '" + filename + "':", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            bool didNotExist;
            if ( !OpenFileForEdit( filename, null, false, false, depth + 1, out didNotExist ) )
            {
                return false;
            }

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel < 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: Could not open/create '" + filename + "': " + e.Message + ".\r\n", depth + 1 );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Writing to '" + filename + "':", depth + 1 );
            }

            writer.WriteLine( "<?xml version=\"1.0\"?>" );
            writer.WriteLine( "<!DOCTYPE HelpCollection>" );
            writer.WriteLine( "<HelpCollection DTDVersion=\"1.0\" LangId=\"1033\" Title=\"" + m_projectSettings.ProjectDescription + "\" Copyright=\"Copyright (c) 2003-2007 Rockstar San Diego. All rights reserved.\" FileVersion=\"7.0.9293.0725\">" );

            string baseFilename = Path.GetFileNameWithoutExtension( filename );
            writer.WriteLine( "\t<AttributeDef File=\"" + baseFilename + ".HxA\"/>" );
            writer.WriteLine( "\t<TOCDef File=\"" + baseFilename + ".HxT\"/>" );
            writer.WriteLine( "\t<KeywordIndexDef File=\"" + baseFilename + "_A.HxK\"/>" );
            writer.WriteLine( "\t<KeywordIndexDef File=\"" + baseFilename + "_F.HxK\"/>" );
            writer.WriteLine( "\t<KeywordIndexDef File=\"" + baseFilename + "_K.HxK\"/>" );
            writer.WriteLine( "\t<KeywordIndexDef File=\"" + baseFilename + "_NamedURL.HxK\"/>" );
            writer.WriteLine( "\t<ItemMoniker Name=\"!DefaultToc\" ProgId=\"HxDs.HxHierarchy\" InitData=\"\"/>" );
            writer.WriteLine( "\t<ItemMoniker Name=\"!DefaultFullTextSearch\" ProgId=\"HxDs.HxFullTextSearch\" InitData=\"\"/>" );
            writer.WriteLine( "\t<ItemMoniker Name=\"!DefaultKeywordIndex\" ProgId=\"HxDs.HxIndex\" InitData=\"K\"/>" );
            writer.WriteLine( "\t<ItemMoniker Name=\"!DefaultAssociativeIndex\" ProgId=\"HxDs.HxIndex\" InitData=\"A\"/>" );
            writer.WriteLine( "\t<ItemMoniker Name=\"!DefaultContextWindowIndex\" ProgId=\"HxDs.HxIndex\" InitData=\"F\"/>" );

            writer.WriteLine( "</HelpCollection>" );

            writer.Close();

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Creates a master HxA file for Help 2 registration.
        /// </summary>
        /// <param name="filename">The name of the HxA file to create.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateHxA( string filename, int depth )
        {
            OnOutputMessage( "Creating '" + filename + "':", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            bool didNotExist;
            if ( !OpenFileForEdit( filename, null, false, false, depth + 1, out didNotExist ) )
            {
                return false;
            }

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel < 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: Could not open/create '" + filename + "': " + e.Message + ".\r\n", depth + 1 );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Writing to '" + filename + "':", depth + 1 );
            }

	        writer.WriteLine( "<?xml version=\"1.0\"?>" );
	        writer.WriteLine( "<!DOCTYPE HelpAttributes>" );
	        writer.WriteLine( "<HelpAttributes DTDVersion=\"1.0\">" );
        	
	        // company attribute:
            writer.WriteLine( "\t<AttName Id=\"1\" Name=\"" + m_projectSettings.HxaAttributeCompanyName + "\" Display=\"Yes\" UIString=\"" + m_projectSettings.CompanyDescription + "\" AttType=\"Enum\">" );
            writer.WriteLine( "\t\t<AttVal Id=\"1_1\" Name=\"" + m_projectSettings.HxaAttributeCompanyValue + "\" Display=\"Yes\" UIString=\"" + m_projectSettings.HxaAttributeCompanyValue + "\" />" );
	        writer.WriteLine( "\t</AttName>" );
        	
	        // project attribute:
            writer.WriteLine( "\t<AttName Id=\"2\" Name=\"" + m_projectSettings.HxaAttributeProjectName + "\" Display=\"Yes\" UIString=\"" + m_projectSettings.ProjectDescription + "\" AttType=\"Enum\">" );
            writer.WriteLine( "\t\t<AttVal Id=\"2_1\" Name=\"" + m_projectSettings.HxaAttributeProjectValue + "\" Display=\"Yes\" UIString=\"" + m_projectSettings.HxaAttributeProjectValue + "\" />" );
	        writer.WriteLine( "\t</AttName>" );
        	
	        // linkgroup attribute:
	        writer.WriteLine( "\t<AttName Id=\"8\" Name=\"LinkGroup\" Display=\"No\" UIString=\"LinkGroup\" AttType=\"Enum\">" );
            writer.WriteLine( "\t\t<AttVal Id=\"8_1\" Name=\"" + m_projectSettings.HxaLinkGroup + "\" Display=\"No\" UIString=\"" + m_projectSettings.HxaLinkGroup + "\" />" );
	        writer.WriteLine( "\t</AttName>" );
	        writer.WriteLine( "</HelpAttributes>" );

	        writer.Close();

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Creates a master HxF file for Help 2 registration.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="hxsFilenames"></param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateHxF( string filename, SortedDictionary<string, string> hxsHxiFilenames, int depth )
        {
            OnOutputMessage( "Creating '" + filename + "':", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            bool didNotExist;
            if ( !OpenFileForEdit( filename, null, false, false, depth + 1, out didNotExist ) )
            {
                return false;
            }

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel < 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: Could not open/create '" + filename + "': " + e.Message + ".\r\n", depth + 1 );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Writing to '" + filename + "':", depth + 1 );
            }

            foreach ( string hxsFilename in hxsHxiFilenames.Keys )
            {
                writer.WriteLine( hxsFilename );
            }

            writer.Close();

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Creates a master HxM file for Help 2 registration.
        /// </summary>
        /// <param name="filename">The name of the file to create.</param>
        /// <param name="includeFilename">The include file (.HxF).</param>
        /// <param name="outputFilename">The output file (.HxQ or .HxR).</param>
        /// <param name="buildResult">The type of combined query file: FTS or Attr.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateHxM( string filename, string includeFilename, string outputFilename, string buildResult, int depth )
        {
            OnOutputMessage( "Creating '" + filename + "':", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            bool didNotExist;
            if ( !OpenFileForEdit( filename, null, false, false, depth + 1, out didNotExist ) )
            {
                return false;
            }

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel < 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: Could not open/create '" + filename + "': " + e.Message + ".\r\n", depth + 1 );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Writing to '" + filename + "':", depth + 1 );
            }

            writer.WriteLine( "<?xml version=\"1.0\"?>" );
            writer.WriteLine( "<!DOCTYPE HelpAttributes>" );
            writer.WriteLine( "<CombinedHelpIndex" );
            writer.WriteLine( "  Id = \"\"" );
            writer.WriteLine( "  DTDVersion = \"1.0\"" );
            writer.WriteLine( "  FileVersion = \"1.0\"" );
            writer.WriteLine( "  LangID = \"1033\"" );
            writer.WriteLine( "  StopWordFile = \"\"" );
            writer.WriteLine( "  OutputFile = \"" + outputFilename + "\"" );
            writer.WriteLine( "  BuildResult = \"" + buildResult + "\">" );
            writer.WriteLine( "<IncludeFile File=\"" + includeFilename + "\"/>" );
            writer.WriteLine( "</CombinedHelpIndex>" );

            writer.Close();

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Create a master HxK file for Help 2 registration.
        /// </summary>
        /// <param name="filename">The name of the file to create</param>
        /// <param name="name">The name of the index.</param>
        /// <param name="visible"><c>true</c> if the index is visible, otherwise <c>false</c>.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateHxK( string filename, string name, bool visible, int depth )
        {
            OnOutputMessage( "Creating '" + filename + "':", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            bool didNotExist;
            if ( !OpenFileForEdit( filename, null, false, false, depth + 1, out didNotExist ) )
            {
                return false;
            }

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel < 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: Could not open/create '" + filename + "': " + e.Message + ".\r\n", depth + 1 );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Writing to '" + filename + "':", depth + 1 );
            }

            writer.WriteLine( "<?xml version=\"1.0\"?>" );
            writer.WriteLine( "<!DOCTYPE HelpIndex>" );
            writer.WriteLine( "<HelpIndex" );
            writer.WriteLine( "\tName = \"" + name + "\"" );
            writer.WriteLine( "\tDTDVersion = \"1.0\"" );

            if ( visible )
            {
                writer.WriteLine( "\tVisible=\"Yes\"" );
            }

            writer.WriteLine( "\tLangId = \"1033\"" );
            writer.WriteLine( ">" );
            writer.WriteLine( "</HelpIndex>" );

            writer.Close();

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Creates an ini file that H2Reg.exe will use for Help 2 registration.
        /// </summary>
        /// <param name="filename">The name of the ini file to create.</param>
        /// <param name="name">The name of the help file(s) to be registered.</param>
        /// <param name="hxsHxiFilenames">A <see cref="SortedDictionary"/> of the modules to add.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateH2RegIni( string filename, string name, SortedDictionary<string, string> hxsHxiFilenames, int depth )
        {
            OnOutputMessage( "Creating '" + filename + "':", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }
           
            bool didNotExist;
            if ( !OpenFileForEdit( filename, null, false, false, depth + 1, out didNotExist ) )
            {
                return false;
            }

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );
            }
            catch ( Exception e )
            {
                OnOutputMessage( "Error: Could not open/create '" + filename + "': " + e.Message + ".\r\n", depth + 1 );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Writing to '" + filename + "':", depth + 1 );
            }

            writer.WriteLine( "[MAIN]" );
            writer.WriteLine( "DebugMode=0                   ;set to 1 to log everything - Check your resources (ship with =0)" );
            writer.WriteLine( "DumpNSToLog_before=0          ;Very verbose but good for debugging - dumps all NS info to log file before Registration" );
            writer.WriteLine( "DumpNSToLog_after=0           ;Very verbose but good for debugging - dumps all NS info to log file after Registration" );
            writer.WriteLine( "OKtoReport_FinalRegError=0    ;Report general error at end if a registration error was logged" );
            writer.WriteLine( "OKtoReport_FinalUnRegError=0  ;Report general error at end if a unregistration error was logged" );
            writer.WriteLine( "" );
            writer.WriteLine( ";Advanced feature - These can be set then used as <userdir1> etc specifiers. They can" );
            writer.WriteLine( ";also be set via the H2Reg.exe command line. Command line has preference over INI file." );
            writer.WriteLine( "UserDir1=''" );
            writer.WriteLine( "UserDir2=''" );
            writer.WriteLine( "UserDir3=''" );
            writer.WriteLine( "UserDir4=''" );
            writer.WriteLine( "" );
            writer.WriteLine( "; Resource Strings" );
            writer.WriteLine( "" );
            writer.WriteLine( "[en] ; English" );
            writer.WriteLine( "ErrSt_SysFileNotFound = 'Installation Error. Error reading system file or file not found.|%s'" );
            writer.WriteLine( "ErrSt_MSHelp2RTNotFound = 'MS Help 2.x runtime files are not installed on this PC.'" );
            writer.WriteLine( "ErrSt_NotAdminMode = 'You must be logged on as an Administrator.'" );
            writer.WriteLine( "ErrSt_Extra = 'Installation/registration of Help files cannot proceed.'" );
            writer.WriteLine( "" );
            writer.WriteLine( "Msg_Registering = 'Registering Online Documentation Files:'" );
            writer.WriteLine( "Msg_UnRegistering = 'Unregistering Online Documentation Files:'" );
            writer.WriteLine( "Msg_LoggingNSInfo = 'Logging Namespace Info'" );
            writer.WriteLine( "Msg_Registering_Namespaces =  'Registering Namespaces'" );
            writer.WriteLine( "Msg_Registering_Titles =  'Registering Titles'" );
            writer.WriteLine( "Msg_Registering_Plugins =  'Registering Plug-ins'" );
            writer.WriteLine( "Msg_Registering_Filters =  'Registering Filters'" );
            writer.WriteLine( "Msg_UnRegistering_Namespaces =  'Unregistering Namespaces'" );
            writer.WriteLine( "Msg_UnRegistering_Titles =  'Unregistering Titles'" );
            writer.WriteLine( "Msg_UnRegistering_Plugins =  'Unregistering Plug-ins'" );
            writer.WriteLine( "Msg_UnRegistering_Filters =  'Unregistering Filters'" );
            writer.WriteLine( "" );
            writer.WriteLine( "Msg_Merging_Namespaces = 'Merging Help Indexes. This may take several minutes'" );
            writer.WriteLine( "" );
            writer.WriteLine( "PopupSt_FinalRegError='There were errors reported while Registering help files.||View Log file?'" );
            writer.WriteLine( "PopupSt_FinalUnRegError='There were errors reported while Unregistering help files.||View Log file?'" );
            writer.WriteLine( "" );
            writer.WriteLine( "" );
            writer.WriteLine( "" );
            writer.WriteLine( "; International Strings - Defaults to [en]" );
            writer.WriteLine( "[de] ; German" );
            writer.WriteLine( "[ja] ; Japanese" );
            writer.WriteLine( "[fr] ; French" );
            writer.WriteLine( "[es] ; Spanish" );
            writer.WriteLine( "[it] ; Italian" );
            writer.WriteLine( "[ko] ; Korean" );
            writer.WriteLine( "[cn] ; Chinese (Simplified)" );
            writer.WriteLine( "[tw] ; Chinese (Traditional)" );
            writer.WriteLine( "[sv] ; Swedish" );
            writer.WriteLine( "[nl] ; Dutch" );
            writer.WriteLine( "[ru] ; Russian" );
            writer.WriteLine( "[ar] ; Arabic" );
            writer.WriteLine( "[he] ; Hebrew" );
            writer.WriteLine( "[da] ; Danish" );
            writer.WriteLine( "[no] ; Norwegian" );
            writer.WriteLine( "[fi] ; Finnish" );
            writer.WriteLine( "[pt] ; Portuguese" );
            writer.WriteLine( "[br] ; Brazilian" );
            writer.WriteLine( "[cs] ; Czech" );
            writer.WriteLine( "[pl] ; Polish" );
            writer.WriteLine( "[hu] ; Hungarian" );
            writer.WriteLine( "[el] ; Greek" );
            writer.WriteLine( "[tr] ; Turkish" );
            writer.WriteLine( "[sl] ; Slovenian" );
            writer.WriteLine( "[sk] ; Slovakian" );
            writer.WriteLine( "[eu] ; Basque" );
            writer.WriteLine( "[ca] ; Catalan" );
            writer.WriteLine( "" );
            writer.WriteLine( "" );
            writer.WriteLine( ";--- Optionally you can place your Registration Commands in this file" );
            writer.WriteLine( "" );
            writer.WriteLine( ";------- Register -r switch" );
            writer.WriteLine( "" );
            writer.WriteLine( "[Reg_Namespace]" );
            writer.WriteLine( ";<nsName>|<nsColfile>|<nsDesc>" );
            
            writer.WriteLine( m_projectSettings.TreeNamespace + "|" + name + ".HxC|" + m_projectSettings.ProjectDescription );
            writer.WriteLine( "" );
            writer.WriteLine( "[Reg_Title]" );
            writer.WriteLine( ";<nsName>|<TitleID>|<LangId>|<HxS_HelpFile>|<HxI_IndexFile>|<HxQ_QueryFile>|<HxR_AttrQueryFile>|<HxsMediaLoc>|<HxqMediaLoc>|<HxrMediaLoc>|<SampleMediaLoc>" );

            foreach ( KeyValuePair<string, string> pair in hxsHxiFilenames )
            {
#if CREATE_HXQ_AND_HXR_FILES
                string partialFilename = Path.Combine( directory, name );
                WriteH2RegIniTitle( writer, pair.Key, pair.Value, partialFilename + ".HxQ", partialFilename + ".HxR", true );
#else
                WriteH2RegIniTitle( writer, pair.Key, pair.Value, string.Empty, string.Empty, true );
#endif
            }

            writer.WriteLine( "" );
            writer.WriteLine( "[Reg_Plugin]" );
            writer.WriteLine( ";<nsName_Parent>|<HxT_Parent>|<nsName_Child>|<HxT_Child>|<HxA_Child>" );
            writer.WriteLine( "MS.VSIPCC+|_DEFAULT|" + m_projectSettings.TreeNamespace + "|" + name + ".HxT|" + name + ".HxA" );
            writer.WriteLine( "" );
            writer.WriteLine( "[Reg_Filter]" );
            writer.WriteLine( ";<nsName>|<FilterName>|<FilterQueryStr>" );
            writer.WriteLine( m_projectSettings.TreeNamespace + "|" + m_projectSettings.CompanyDescription + "|" + m_projectSettings.CompanyFilterQuery );
            writer.WriteLine( m_projectSettings.TreeNamespace + "|" + m_projectSettings.ProjectDescription + "|" + m_projectSettings.ProjectFilterQuery );
            writer.WriteLine( "" );
            writer.WriteLine( ";------- UnRegister -u switch" );
            writer.WriteLine( "" );
            writer.WriteLine( "[UnReg_Namespace]" );
            writer.WriteLine( ";<nsName>" );
            writer.WriteLine( m_projectSettings.TreeNamespace );
            writer.WriteLine( "" );
            writer.WriteLine( "[UnReg_Title]" );
            writer.WriteLine( ";<nsName>|<TitleID>|<LangId>" );

            foreach ( KeyValuePair<string, string> pair in hxsHxiFilenames )
            {
#if CREATE_HXQ_AND_HXR_FILES
                string partialFilename = Path.Combine( directory, name );
                WriteH2RegIniTitle( writer, pair.Key, pair.Value, partialFilename + ".HxQ", partialFilename + ".HxR", false );
#else
                WriteH2RegIniTitle( writer, pair.Key, pair.Value, string.Empty, string.Empty, false );
#endif
            }

            writer.WriteLine( "" );
            writer.WriteLine( "[UnReg_Plugin]" );
            writer.WriteLine( ";<nsName_Parent>|<HxT_Parent>|<nsName_Child>|<HxT_Child>|<HxA_Child>" );
            writer.WriteLine( "MS.VSIPCC+|_DEFAULT|" + m_projectSettings.TreeNamespace + "|" + name + ".HxT|" + name + ".HxA" );
            writer.WriteLine( "" );
            writer.WriteLine( "[UnReg_Filter]" );
            writer.WriteLine( ";<nsName>|<FilterName>" );
            writer.WriteLine( m_projectSettings.TreeNamespace + "|" + m_projectSettings.CompanyDescription );
            writer.WriteLine( m_projectSettings.TreeNamespace + "|" + m_projectSettings.ProjectDescription );
            writer.WriteLine( "" );
            writer.WriteLine( ";------- Merge -m switch" );
            writer.WriteLine( "" );
            writer.WriteLine( "[Merge_Namespace]" );
            writer.WriteLine( ";<nsName>|[AUTO]" );
            writer.WriteLine( "MS.VSIPCC+|AUTO" );
            writer.WriteLine( "" );

            writer.Close();

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Writes the HelpTOCNodes to the <see cref="TextWriter"/> according to the <see cref="HelpLayoutItem"/>.
        /// </summary>
        /// <param name="writer">The <see cref="TextWriter"/> to write to.</param>
        /// <param name="item">The <see cref="HelpLayoutItem"/> to write.</param>
        /// <param name="indent">The indentation level of the xml file.</param>
        private void WriteHxT( TextWriter writer, HelpLayoutItem item, int indent )
        {
            if ( item is HelpLayoutTitle )
            {
                WriteHxtTitle( writer, item.Name, indent );

                HelpLayoutTitle title = item as HelpLayoutTitle;
                foreach ( HelpLayoutItem subItem in title.SubItems )
                {
                    WriteHxT( writer, subItem, indent + 1 );
                }

                WriteHxtTitleEnd( writer, indent );
            }
            else if ( item is HelpLayoutModule )
            {
                WriteHxtNode( writer, item.Name, m_projectSettings.TreeNamespace + "." + item.Name, indent );
            }
        }

        /// <summary>
        /// Writes a HelpTOCNode with topic title and link to the HxT file.
        /// </summary>
        /// <param name="writer">The <see cref="TextWriter"/> to write to.</param>
        /// <param name="title">The title.</param>
        /// <param name="url">The url.</param>
        /// <param name="depth">The indentation level.</param>
        private void WriteHxtNode( TextWriter writer, string title, string url, int depth )
        {
            WriteHxtTitle( writer, title, depth );
            WriteHxtToc( writer, url, depth + 1 );
            WriteHxtTitleEnd( writer, depth );
        }

        /// <summary>
        /// Writes a regular HelpTOCNode with a title to the HxT file.
        /// </summary>
        /// <param name="writer">The <see cref="TextWriter"/> to write to.</param>
        /// <param name="title">The title.</param>
        /// <param name="depth">The indentation level.</param>
        private void WriteHxtTitle( TextWriter writer, string title, int depth )
        {
            StringBuilder text = new StringBuilder();
            for ( int i = 0; i < depth; ++i )
            {
                text.Append( '\t' );
            }

            text.Append( "<HelpTOCNode NodeType=\"Regular\" Title=\"" );
            text.Append( title );
            text.Append( "\">" );

            writer.WriteLine( text.ToString() );
        }

        /// <summary>
        /// Writes the closing HelpTOCNode to the Hxt file.
        /// </summary>
        /// <param name="writer">The <see cref="TextWriter"/> to write to.</param>
        /// <param name="depth">The indentation level.</param>
        private void WriteHxtTitleEnd( TextWriter writer, int depth )
        {
            StringBuilder text = new StringBuilder();
            for ( int i = 0; i < depth; ++i )
            {
                text.Append( '\t' );
            }

            text.Append( "</HelpTOCNode>" );

            writer.WriteLine( text.ToString() );
        }

        /// <summary>
        /// Writes a TOC HelpTOCNode with a url to the HxT file.
        /// </summary>
        /// <param name="writer">The <see cref="TextWriter"/> to write to.</param>
        /// <param name="url">The url.</param>
        /// <param name="depth">The indentation level.</param>
        private void WriteHxtToc( TextWriter writer, string url, int depth )
        {
            StringBuilder text = new StringBuilder();
            for ( int i = 0; i < depth; ++i )
            {
                text.Append( '\t' );
            }

            text.Append( "<HelpTOCNode NodeType=\"TOC\" Url=\"" );
            text.Append( url.Replace( ' ', '_' ) );
            text.Append( "\" />" );

            writer.WriteLine( text.ToString() );
        }

        /// <summary>
        /// Builds the line to register a single HxS in an H2Reg ini file.
        /// </summary>
        /// <param name="writer">The <see cref="TextWriter"/> to write to.</param>
        /// <param name="hxsFilename">The HxS file to register.</param>
        /// <param name="hxiFilename">The HxI file to register.</param>
        /// <param name="hxqFilename">The HxQ file.</param>
        /// <param name="hxrFilename">The HxR file.</param>
        /// <param name="register"><c>true</c> if the list is for installation, <c>false</c> if it is for uninstallation.</param>
        /// <returns>The formatted string.</returns>
        private void WriteH2RegIniTitle( TextWriter writer, string hxsFilename, string hxiFilename, 
            string hxqFilename, string hxrFilename, bool register )
        {
            StringBuilder text = new StringBuilder();

            string directory = Path.GetDirectoryName( hxsFilename );

            text.Append( m_projectSettings.TreeNamespace + "|" );
            text.Append( m_projectSettings.TreeNamespace + "." + Path.GetFileNameWithoutExtension( hxsFilename ).Replace( ' ', '_' ) + "|" );
            text.Append( "1033" );

            if ( register )
            {
                text.Append( "|" + hxsFilename );
                text.Append( "|" + hxiFilename );
                text.Append( "|" + hxqFilename );
                text.Append( "|" + hxrFilename );
                text.Append( "||||" );
            }

            writer.WriteLine( text.ToString() );
        }

        /// <summary>
        /// Creates a Query Index file for Help 2 Registration using HxMerge.exe
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="hsxFilenames"></param>
        /// <param name="outputFilename"></param>
        /// <param name="buildResult"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        private bool CreateQueryIndexFile( string filename, int depth )
        {
            OnOutputMessage( "Using '" + filename + "' to generate Query Index with HxMerge:\r\n", depth );

            rageExecuteCommand command = new rageExecuteCommand();

            command.Command = rageFileUtilities.GetProgramFilesx86() + @"\Visual Studio 2005 SDK\2006.09\VisualStudioIntegration\Archive\HelpIntegration\HxMerge.exe";
            command.Arguments = "-p \"" + Path.GetFileName( filename ) + "\"";
            command.EchoToConsole = false;
            command.LogToHtmlFile = false;
            command.TimeOutInSeconds = 10.0f * 60.0f;  // 10 minutes
            command.TimeOutWithoutOutputInSeconds = 5.0f * 60.0f; // 5 minutes
            command.UpdateLogFileInRealTime = false;
            command.UseBusySpinner = false;
            command.WorkingDirectory = Path.GetDirectoryName( filename );

            rageStatus status = new rageStatus();
            command.Execute( out status );

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                List<string> results = command.GetLogAsArray();
                foreach ( string result in results )
                {
                    OnOutputMessage( result.Trim() + "\r\n", depth + 1 );
                }
            }

            if ( !status.Success() )
            {
                OnOutputMessage( "Error: " + status.GetErrorString() + "\r\n", depth + 1 );
                return false;
            }

            OnOutputMessage( "Complete.\r\n", depth + 1 );
            return true;
        }

        /// <summary>
        /// Creates the files and registry key(s) necessary for the Help2 Full-Text search system.
        /// </summary>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool RegisterSearchFilters( int depth )
        {
            OnOutputMessage( "Registering Search Filters:\r\n", depth );

            // create company attribute filter file
            string companyFilterName = m_projectSettings.CompanyDescription.Replace( ' ', '_' ) + "TechFilter";
            string companyFilterFilename = Path.Combine(rageFileUtilities.GetProgramFilesx86() + @"\Common Files\Microsoft Shared\VS Help Data\8.0\Filters\1033",
                companyFilterName + ".xml" );
            CreateSearchFilterFile( companyFilterFilename, "Technology", "Technology",
                m_projectSettings.TreeNamespace + "." + m_projectSettings.CompanyDescription.Replace( ' ', '_' ), 
                m_projectSettings.CompanyDescription, m_projectSettings.CompanyFilterQuery, depth + 1 );

            // create project attribute filter file
            string projectFilterName = m_projectSettings.ProjectDescription.Replace( ' ', '_' ) + "TechFilter";
            string projectFilterFilename = Path.Combine(rageFileUtilities.GetProgramFilesx86() + @"\Common Files\Microsoft Shared\VS Help Data\8.0\Filters\1033",
                projectFilterName + ".xml" );
            CreateSearchFilterFile( projectFilterFilename, "Technology", "Technology",
                m_projectSettings.TreeNamespace + "." + m_projectSettings.ProjectDescription.Replace( ' ', '_' ),
                m_projectSettings.ProjectDescription, m_projectSettings.ProjectFilterQuery, depth + 1 );

            // Add the registry keys.
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Adding '" + companyFilterName + @"' to registry key 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\VisualStudio\8.0\Help\VisibleFilters':", depth + 1 );
            }

            bool success = true;
            try
            {
                Registry.SetValue( @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\VisualStudio\8.0\Help\VisibleFilters",
                    companyFilterName, "1", RegistryValueKind.DWord );
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( @"Warning: Could not set registry key 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\VisualStudio\8.0\Help\VisibleFilters' with '"
                    + companyFilterName + "': " + e.Message + ".\r\n", depth + 1 );
                success = false;
            }

            if ( success && (m_projectSettings.VerboseLevel >= 2) )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Adding '" + projectFilterName + @"' to registry key 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\VisualStudio\8.0\Help\VisibleFilters':", depth + 1 );
            }

            success = true;
            try
            {
                Registry.SetValue( @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\VisualStudio\8.0\Help\VisibleFilters",
                    projectFilterName, "1", RegistryValueKind.DWord );
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( @"Warning: Could not set registry key 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\VisualStudio\8.0\Help\VisibleFilters' with '"
                    + projectFilterName + "': " + e.Message + ".\r\n", depth + 1 );
                success = false;
            }

            if ( success && (m_projectSettings.VerboseLevel >= 2) )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            OnOutputMessage( "Complete.\r\n", depth + 1 );
            return true;
        }

        /// <summary>
        /// Creates the files and registry key(s) used by the Help2 Full-Text search system.
        /// </summary>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool UnregisterSearchFilters( int depth )
        {
            OnOutputMessage( "Unregistering Search Filters:", depth );

            bool needLineBreak = true;
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
                needLineBreak = false;
            }

            // mark the company attribute filter file for deletion
            string companyFilterName = m_projectSettings.CompanyDescription.Replace( ' ', '_' ) + "TechFilter";
            string companyFilterFilename = Path.Combine(rageFileUtilities.GetProgramFilesx86() + @"\Common Files\Microsoft Shared\VS Help Data\8.0\Filters\1033",
                companyFilterName + ".xml" );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Deleting '" + companyFilterFilename + "':", depth + 1 );
            }

            bool success = true;
            try
            {
                if ( File.Exists( companyFilterFilename ) )
                {
                    File.Delete( companyFilterName );
                }
            }
            catch ( Exception e )
            {
                if ( needLineBreak )
                {
                    OnOutputMessage( "\r\n", 0 );
                    needLineBreak = false;
                }

                OnOutputMessage( "Warning: Unable to delete '" + companyFilterFilename + "': " + e.Message + ".\r\n", depth + 1 );
                success = false;
            }

            if ( success && (m_projectSettings.VerboseLevel >= 2) )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            // mark the project attribute filter file for deletion
            string projectFilterName = m_projectSettings.ProjectDescription.Replace( ' ', '_' ) + "TechFilter";
            string projectFilterFilename = Path.Combine(rageFileUtilities.GetProgramFilesx86() + @"\Common Files\Microsoft Shared\VS Help Data\8.0\Filters\1033",
                projectFilterName + ".xml" );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Deleting '" + projectFilterFilename + "':", depth + 1 );
            }

            success = true;
            try
            {
                if ( File.Exists( projectFilterFilename ) )
                {
                    File.Delete( projectFilterFilename );
                }
            }
            catch ( Exception e )
            {
                if ( needLineBreak )
                {
                    OnOutputMessage( "\r\n", 0 );
                    needLineBreak = false;
                }

                OnOutputMessage( "Warning: Unable to delete '" + projectFilterFilename + "': " + e.Message + ".\r\n", depth + 1 );
                success = false;
            }

            if ( success && (m_projectSettings.VerboseLevel >= 2) )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            // Remove the registry keys.
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Deleting '" + companyFilterName + @"' from registry key 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\VisualStudio\8.0\Help\VisibleFilters':", depth + 1 );
            }

            success = true;
            try
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey( @"SOFTWARE\Microsoft\VisualStudio\8.0\Help\VisibleFilters", true );
                key.DeleteValue( companyFilterName );
            }
            catch ( Exception e )
            {
                if ( needLineBreak )
                {
                    OnOutputMessage( "\r\n", 0 );
                    needLineBreak = false;
                }

                OnOutputMessage( @"Warning: Could not delete registry key 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\VisualStudio\8.0\Help\VisibleFilters' value '"
                    + companyFilterName + "': " + e.Message + ".\r\n", depth + 1 );
                success = false;
            }

            if ( success && (m_projectSettings.VerboseLevel >= 2) )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Deleting '" + projectFilterName + @"' from registry key 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\VisualStudio\8.0\Help\VisibleFilters':", depth + 1 );
            }

            success = true;
            try
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey( @"SOFTWARE\Microsoft\VisualStudio\8.0\Help\VisibleFilters", true );
                key.DeleteValue( projectFilterName );
            }
            catch ( Exception e )
            {
                if ( needLineBreak )
                {
                    OnOutputMessage( "\r\n", 0 );
                    needLineBreak = false;
                }

                OnOutputMessage( @"Warning: Could not delete registry key 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\VisualStudio\8.0\Help\VisibleFilters' with '"
                    + projectFilterName + "': " + e.Message + ".\r\n", depth + 1 );
                success = false;
            }

            if ( success && (m_projectSettings.VerboseLevel >= 2) )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            if ( needLineBreak )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }
            else
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            return true;
        }

        /// <summary>
        /// Creates an xml file to enable full-text search in Help2.
        /// </summary>
        /// <param name="filename">The file to create.</param>
        /// <param name="filterAttributeId">The ID of the FilterAttribute.</param>
        /// <param name="filterAttributeName">The Name of the FilterAttribute.</param>
        /// <param name="filterValueId">The ID of the FilterValue.</param>
        /// <param name="filterAttributeName">The Name of the FilterAttribute.</param>
        /// <param name="localFilterString">The LocalFilterString.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateSearchFilterFile( string filename, string filterAttributeId, string filterAttributeName, 
            string filterValueId, string filterValueName, string localFilterString, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Creating '" + filename + "':", depth );
            }

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }
                
                OnOutputMessage( "Error: Unable to create '" + filename + "': " + e.Message + ".\r\n", 
                    m_projectSettings.VerboseLevel >= 2 ? depth + 1 : depth );
                return false;
            }

            writer.WriteLine( "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" );
            writer.WriteLine( "<SearchFilter xmlns=\"http://schemas.microsoft.com/VisualStudio/2004/08/Help/SearchFilter\" Version=\"0.1.0.0\">" );
            writer.WriteLine( "\t<FilterAttribute>" );
            writer.WriteLine( "\t\t<Id>" + filterAttributeId + "</Id>" ); 
            writer.WriteLine( "\t\t<Name _locID=\"name.1\">" + filterAttributeName + "</Name> " );
            writer.WriteLine( "\t\t<FilterValue>" );
            writer.WriteLine( "\t\t\t<Id>" + filterValueId + "</Id> " );
            writer.WriteLine( "\t\t\t<Name _locID=\"name.2\">" + filterValueName + "</Name> " );
            writer.WriteLine( "\t\t\t<Meaning>" );
            writer.WriteLine( "\t\t\t\t<LocalFilterString>" + localFilterString + "</LocalFilterString> " );
            writer.WriteLine( "\t\t\t\t<TocInclude /> " );
            writer.WriteLine( "\t\t\t</Meaning>" );
            writer.WriteLine( "\t\t</FilterValue>" );
            writer.WriteLine( "\t</FilterAttribute>" );
            writer.WriteLine( "</SearchFilter>" );

            writer.Close();

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Calls H2Reg.exe in the given directory to install the Help 2 files.
        /// </summary>
        /// <param name="name">The name of the collection-level help.</param>
        /// <param name="directory">The directory to with the registration files.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool RegisterHelpFiles( string name, string directory, int depth )
        {
            OnOutputMessage( "Registering help files:\r\n", depth );

            string h2reg = Path.Combine( directory, Path.GetFileName( m_projectSettings.Help2RegExecutableFile ) );
            
            bool didNotExist;
            if ( !OpenFileForEdit( h2reg, m_projectSettings.Help2RegExecutableFile, true, true, depth + 1, out didNotExist ) )
            {
                return false;
            }

            rageExecuteCommand command = new rageExecuteCommand();
            command.Command = h2reg;
            command.Arguments = "-r -m";
            command.EchoToConsole = false;
            command.LogToHtmlFile = true;
            command.UseBusySpinner = false;
            command.WorkingDirectory = directory;

            rageStatus status = new rageStatus();
            command.Execute( out status );

            if ( !status.Success() )
            {
                OnOutputMessage( "Error: " + status.GetErrorString() + "\r\n", depth + 1 );
                return false;
            }

            List<string> lines = command.GetLogAsArray();            
            if ( lines.Count > 0 )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    foreach ( string line in lines )
                    {
                        OnOutputMessage( line.Trim() + "\r\n", depth + 1 );
                    }
                }
            }
            else
            {
                string logFile = Path.Combine( directory, "H2Reg_Log.txt" );
                if ( File.Exists( logFile ) )
                {
                    TextReader reader = new StreamReader( logFile );

                    int errors = 0;
                    string line = reader.ReadLine();
                    while ( line != null )
                    {
                        if ( m_projectSettings.VerboseLevel >= 2 )
                        {
                            OnOutputMessage( line.Trim() + "\r\n", depth + 1 );
                        }

                        if ( line.Contains( "** Reg Error **" ) )
                        {
                            ++errors;
                        }

                        line = reader.ReadLine();
                    }

                    reader.Close();

                    if ( errors > 0 )
                    {
                        OnOutputMessage( "Error: Register failed.  " + errors + " error(s) found.\r\n", depth + 1 );
                        return false;
                    }
                }
            }

            OnOutputMessage( "Complete.\r\n", depth + 1 );

            return true;
        }

        /// <summary>
        /// Calls H2Reg.exe in the given directory to uninstall the Help 2 files.
        /// </summary>
        /// <param name="name">The name of the collection-level help.</param>
        /// <param name="directory">The directory to unregister.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool UnregisterHelpFiles( string name, string directory, int depth )
        {
            OnOutputMessage( "Unregistering help files:\r\n", depth );

            string h2reg = Path.Combine( directory, Path.GetFileName( m_projectSettings.Help2RegExecutableFile ) );
            
            bool didNotExist;
            if ( !OpenFileForEdit( h2reg, m_projectSettings.Help2RegExecutableFile, true, true, depth + 1, out didNotExist ) )
            {
                return false;
            }

            rageExecuteCommand command = new rageExecuteCommand();
            command.Command = h2reg;
            command.Arguments = "-u -m";
            command.EchoToConsole = false;
            command.LogToHtmlFile = true;
            command.UseBusySpinner = false;
            command.WorkingDirectory = directory;

            rageStatus status = new rageStatus();
            command.Execute( out status );

            if ( !status.Success() )
            {
                OnOutputMessage( "Error: " + status.GetErrorString() + "\r\n", depth + 1 );
                return false;
            }

            List<string> lines = command.GetLogAsArray();
            if ( lines.Count > 0 )
            {
                if ( m_projectSettings.VerboseLevel >= 2 )
                {
                    foreach ( string line in lines )
                    {
                        OnOutputMessage( line.Trim() + "\r\n", depth + 1 );
                    }
                }
            }
            else
            {
                string logFile = Path.Combine( directory, "H2Reg_Log.txt" );
                if ( File.Exists( logFile ) )
                {
                    TextReader reader = new StreamReader( logFile );

                    int errors = 0;
                    string line = reader.ReadLine();
                    while ( line != null )
                    {
                        if ( m_projectSettings.VerboseLevel >= 2 )
                        {
                            OnOutputMessage( line.Trim() + "\r\n", depth + 1 );
                        }

                        if ( line.Contains( "** Reg Error **" ) )
                        {
                            ++errors;
                        }

                        line = reader.ReadLine();
                    }

                    reader.Close();

                    if ( errors > 0 )
                    {
                        OnOutputMessage( "Error: Unregister failed.  " + errors + " error(s) found.\r\n", depth + 1 );
                        return false;
                    }
                }
            }

            OnOutputMessage( "Complete.\r\n", depth + 1 );

            return true;
        }

        #endregion

        #region HelpLayout
        /// <summary>
        /// Reads the Help Layout text file into a <see cref="HelpLayout"/> structure.
        /// </summary>
        /// <param name="filename">The file to read.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns>The <see cref="HelpLayout"/>, <c>null</c> on error.</returns>
        private HelpLayout ReadHelpLayoutFile( string filename, int depth )
        {
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Loading Help Layout File '" + filename + "':", depth );
            }

            bool needLineBreak = true;
            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "\r\n", 0 );
                needLineBreak = false;
            }

            int msgDepth = (m_projectSettings.VerboseLevel >= 2) ? depth + 1 : depth;

            TextReader reader = null;
            try
            {
                reader = new StreamReader( filename );
            }
            catch ( Exception e )
            {
                if ( needLineBreak )
                {
                    OnOutputMessage( "\r\n", 0 );
                    needLineBreak = false;
                }

                OnOutputMessage( "Error: Unable to load Unified Help Layout File '" + filename + "': " + e.Message + ".\r\n", msgDepth );
                return null;
            }

            HelpLayout layout = new HelpLayout();

            Stack<HelpLayoutTitle> titles = new Stack<HelpLayoutTitle>();

            string line = reader.ReadLine();
            while ( line != null )
            {
                line = line.Trim();
                if ( line.StartsWith( "TITLE" ) )
                {
                    if ( line.Length > 5 )
                    {
                        string name = line.Substring( 5 ).Trim();
                        HelpLayoutTitle title = new HelpLayoutTitle( name );

                        if ( titles.Count > 0 )
                        {
                            titles.Peek().SubItems.Add( title );
                        }
                        else
                        {
                            layout.Items.Add( title );
                        }

                        titles.Push( title );

                        if ( m_projectSettings.VerboseLevel >= 3 )
                        {
                            OnOutputMessage( "TITLE " + name + "\r\n", msgDepth );
                            ++msgDepth;
                        }
                    }
                    else
                    {
                        if ( needLineBreak )
                        {
                            OnOutputMessage( "\r\n", 0 );
                            needLineBreak = false;
                        }

                        OnOutputMessage( "Warning: missing name for TITLE.\r\n", msgDepth );
                    }
                }
                else if ( line.StartsWith( "ENDTITLE" ) )
                {
                    titles.Pop();

                    if ( m_projectSettings.VerboseLevel >= 3 )
                    {
                        --msgDepth;
                        OnOutputMessage( "ENDTITLE\r\n", msgDepth );
                    }
                }
                else if ( line.StartsWith( "MODULE" ) )
                {
                    if ( line.Length > 6 )
                    {
                        string name = line.Substring( 6 ).Trim();
                        HelpLayoutModule module = new HelpLayoutModule( name );

                        if ( titles.Count > 0 )
                        {
                            titles.Peek().SubItems.Add( module );
                        }
                        else
                        {
                            layout.Items.Add( module );
                        }

                        if ( m_projectSettings.VerboseLevel >= 3 )
                        {
                            OnOutputMessage( "MODULE " + name + "\r\n", msgDepth );
                        }
                    }
                    else
                    {
                        if ( needLineBreak )
                        {
                            OnOutputMessage( "\r\n", 0 );
                            needLineBreak = false;
                        }

                        OnOutputMessage( "Warning: missing name for MODULE.\r\n", msgDepth );
                    }
                }

                line = reader.ReadLine();
            }

            if ( titles.Count > 0 )
            {
                if ( needLineBreak )
                {
                    OnOutputMessage( "\r\n", 0 );
                    needLineBreak = false;
                }

                OnOutputMessage( "Warning: unbalanced TITLE/ENDTITLE.\r\n", msgDepth );
            }

            reader.Close();

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                if ( needLineBreak )
                {
                    OnOutputMessage( " Complete.\r\n", 0 );
                }
                else
                {
                    OnOutputMessage( "Complete.\r\n", msgDepth );
                }
            }

            return layout;
        }

        /// <summary>
        /// Gets or generates a HelpLayout for the given docDirectory and collection of modules.
        /// </summary>
        /// <param name="docDirectory">The directory that may or may not contain a col_hierarchy.txt file.</param>
        /// <param name="moduleNames">The module names that are part of the help collection.</param>
        /// <param name="insertBranch">When <c>true</c>, a top-level TITLE is inserted with the name of the branch, and all subItems are added to it.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private HelpLayout GetHelpLayout( string docDirectory, Dictionary<string, string> moduleNames, bool insertBranch, int depth )
        {
            HelpLayout layout = null;
            
            // try to load the help layout file
            if ( docDirectory != null )
            {
                string helpLayoutFilename = Path.Combine( docDirectory, "col_hierarchy.txt" );
                if ( File.Exists( helpLayoutFilename ) )
                {
                    layout = ReadHelpLayoutFile( helpLayoutFilename, depth );
                }
            }

            bool loadedLayout = false;
            if ( layout == null )
            {
                layout = new HelpLayout();
            }
            else
            {
                loadedLayout = true;
            }

            HelpLayoutTitle branchTitle = null;
            if ( insertBranch )
            {
                // create the branch TITLE
                string branchName = GetBranchName( docDirectory );
                if ( branchName != string.Empty )
                {
                    branchTitle = new HelpLayoutTitle( branchName );
                    
                    // move all items to the branch TITLE
                    foreach( HelpLayoutItem item in layout.Items )
                    {
                        branchTitle.SubItems.Add( item );
                    }

                    layout.Items.Clear();
                    layout.Items.Add( branchTitle );
                }
            }

            // we'll add missing modules here
            HelpLayoutTitle miscTitle = null;

            // we'll add overview modules here
            HelpLayoutTitle overviewTitle = null;

            // add missing modules
            foreach ( string moduleName in moduleNames.Values )
            {
                if ( !layout.ModuleExists( moduleName ) )
                {
                    HelpLayoutModule module = new HelpLayoutModule( moduleName );

                    if ( branchTitle != null )
                    {
                        // add new sections under the branch TITLE
                        if ( moduleName.StartsWith( "overview_" ) )
                        {
                            // make sure we have an Overview TITLE, if not add one under the branch TITLE
                            if ( overviewTitle == null )
                            {
                                overviewTitle = layout.FindTitle( "Overview" );
                                if ( overviewTitle == null )
                                {
                                    overviewTitle = new HelpLayoutTitle( "Overview" );

                                    branchTitle.SubItems.Insert( 0, overviewTitle );
                                }
                            }

                            // add to the Overview TITLE 
                            overviewTitle.SubItems.Add( module );
                        }
                        else
                        {
                            if ( loadedLayout )
                            {
                                // if we loaded a layout, make sure we have a Miscellaneous TITLE, if not add one under the branch TITLE
                                if ( miscTitle == null )
                                {
                                    miscTitle = layout.FindTitle( "Miscellaneous" );
                                    if ( miscTitle == null )
                                    {
                                        miscTitle = new HelpLayoutTitle( "Miscellaneous" );

                                        branchTitle.SubItems.Add( miscTitle );
                                    }
                                }

                                // add to the Miscellaneous TITLE
                                miscTitle.SubItems.Add( module );
                            }
                            else
                            {
                                // we didn't load a layout, so just add it to the HelpLayout
                                layout.Items.Add( module );
                            }
                        }
                    }
                    else
                    {
                        // add new sections under the layout
                        if ( moduleName.StartsWith( "overview_" ) )
                        {
                            // make sure we have an Overview TITLE, if not add one to the layout
                            if ( overviewTitle == null )
                            {
                                overviewTitle = layout.FindTitle( "Overview" );
                                if ( overviewTitle == null )
                                {
                                    overviewTitle = new HelpLayoutTitle( "Overview" );

                                    layout.Items.Insert( 0, overviewTitle );
                                }
                            }

                            // add to the Overview TITLE 
                            overviewTitle.SubItems.Add( module );
                        }
                        else
                        {
                            if ( loadedLayout )
                            {
                                // if we loaded a layout, make sure we have a Miscellaneous TITLE, if not add one to the layout
                                if ( miscTitle == null )
                                {
                                    miscTitle = layout.FindTitle( "Miscellaneous" );
                                    if ( miscTitle == null )
                                    {
                                        miscTitle = new HelpLayoutTitle( "Miscellaneous" );

                                        layout.Items.Add( miscTitle );
                                    }
                                }

                                // add to the Miscellaneous TITLE
                                miscTitle.SubItems.Add( module );
                            }
                            else
                            {
                                // we didn't load a layout, so just add it to the HelpLayout
                                layout.Items.Add( module );
                            }
                        }
                    }
                }
            }

            // remove modules we don't have
            layout.RemoveModulesNotInDictionary( moduleNames );

            return layout;
        }

        /// <summary>
        /// Writes the given <see cref="HelpLayoutItem"/> to the Dtx file.
        /// </summary>
        /// <param name="writer">The file to write to.</param>
        /// <param name="item">The <see cref="HelpLayoutItem"/> to write.</param>
        /// <param name="groupTopicID">The parent TITLE of the item.  Can be <c>null</c> or empty if there is no parent.</param>
        /// <param name="topicIDDict">A Dictionary where we will store the TopicIDs to enforce uniqueness.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        private void WriteHelpLayoutItem( TextWriter writer, HelpLayoutItem item, string groupTopicID, 
            Dictionary<string, string> topicIDDict, Dictionary<string, string> moduleToTopicIDDict, int depth )
        {
            // make sure we have a unique TopicID
            string topicID = item.Name;
            if ( topicIDDict.ContainsKey( topicID ) )
            {
                Random r = new Random();
                topicID = item.Name + r.Next();
                while ( topicIDDict.ContainsKey( topicID ) )
                {
                    topicID = item.Name + r.Next();
                }
            }

            // add it to our dictionary
            topicIDDict[topicID] = topicID;

            if ( item is HelpLayoutModule )
            {
                // we'll use the items in this dictionary to find the groupTopicIDs when we process the module dtx files.
                moduleToTopicIDDict[item.Name] = topicID;
            }

            // write the TopicID
            string topic = "@@" + topicID;
            writer.WriteLine( topic );

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( topic + "\r\n", depth );
            }

            // write the TITLE, if needed
            if ( topicID != item.Name )
            {
                string title = "<TITLE " + item.Name + ">";
                writer.WriteLine( title );

                if ( m_projectSettings.VerboseLevel >= 3 )
                {
                    OnOutputMessage( title + "\r\n", depth );
                }
            }

            // write the GROUP, if needed
            if ( !String.IsNullOrEmpty( groupTopicID ) )
            {
                string group = "<GROUP " + groupTopicID + ">";
                writer.WriteLine( group );

                if ( m_projectSettings.VerboseLevel >= 3 )
                {
                    OnOutputMessage( group + "\r\n", depth );
                }
            }

            // give us a space
            writer.WriteLine( String.Empty );

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "\r\n", depth );
            }

            // process the Title's SubItems
            if ( item is HelpLayoutTitle )
            {
                HelpLayoutTitle title = item as HelpLayoutTitle;
                foreach ( HelpLayoutItem subItem in title.SubItems )
                {
                    WriteHelpLayoutItem( writer, subItem, topicID, topicIDDict, moduleToTopicIDDict, depth );
                }
            }
        }

        /// <summary>
        /// Creates a Dtx file from the <see cref="HelpLayout"/>.
        /// </summary>
        /// <param name="filename">The file to create.</param>
        /// <param name="layout">The <see cref="HelpLayout"/> that defines the hierarchy.</param>
        /// <param name="topicIDDict">A Dictionary where we will store the TopicIDs to enforce uniqueness.</param>
        /// <param name="moduleToTopicIDDict">A Dictionary where we will store the module names and their assigned TopicIDs.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateUnifiedDtx( string filename, HelpLayout layout, Dictionary<string, string> topicIDDict,
            Dictionary<string, string> moduleToTopicIDDict, int depth )
        {
            OnOutputMessage( "Creating unified dtx file '" + filename + "':", depth );

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );
            }
            catch ( Exception e )
            {
                OnOutputMessage( "\r\n", 0 );
                OnOutputMessage( "Error: Could not open '" + filename + "' for edit: " + e.Message + ".\r\n", depth + 1 );
                return false;
            }

            // write to the file
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
                OnOutputMessage( "Writing to file:", depth + 1 );
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            foreach ( HelpLayoutItem item in layout.Items )
            {
                WriteHelpLayoutItem( writer, item, null, topicIDDict, moduleToTopicIDDict, depth + 2 );
            }

            writer.Close();

            m_itemsToDelete.Add( filename );

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 2 );
            }
            else if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        #endregion

        #region Single Unified CHM

        /// <summary>
        /// Creates a hhc file.
        /// </summary>
        /// <param name="filename">The name of the file to create.</param>
        /// <param name="indexHtmFilename">The name of the index htm file.</param>
        /// <param name="layout"></param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateHHC( string filename, string indexHtmFilename, HelpLayout layout, int depth )
        {
            OnOutputMessage( "Creating '" + filename + "':", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            bool didNotExist;
            if ( !OpenFileForEdit( filename, null, true, false, depth + 1, out didNotExist ) )
            {
                return false;
            }

            // create one from scratch
            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel < 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: Could not open/create '" + filename + "': " + e.Message + ".\r\n", depth + 1 );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "Writing to '" + filename + "':", depth + 1 );
            }

            writer.WriteLine( "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML//EN\">" );
            writer.WriteLine( "<HTML>" );
            writer.WriteLine( "\t<HEAD>" );
            writer.WriteLine( "\t\t<meta name=\"GENERATOR\" content=\"Microsoft&reg; HTML Help Workshop 4.1\">" );
            writer.WriteLine( "\true\t<!-- Sitemap 1.0 -->" );
            writer.WriteLine( "\t</HEAD>" );
            writer.WriteLine( "\t<BODY>" );
            writer.WriteLine( "\t\t<OBJECT type=\"text/site properties\">" );
            writer.WriteLine( "\t\t\t<param name=\"Window Styles\" value=\"0x800025\">" );
            writer.WriteLine( "\t\t\t<param name=\"FrameName\" value=\"right\">" );
            writer.WriteLine( "\t\t\t<param name=\"comment\" value=\"title:Online Help\">" );
            writer.WriteLine( "\t\t\t<param name=\"comment\" value=\"base:" + Path.GetFileName( indexHtmFilename ) + "\">" );
            writer.WriteLine( "\t\t</OBJECT>" );
            writer.WriteLine( "\t\t<UL>" );
            
            layout.WriteToHHC( writer );

            writer.WriteLine( "\t\t</UL>" );
            writer.WriteLine( "\t</BODY>" );
            writer.WriteLine( "</HTML>" );

            writer.Close();

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Creates a hhk file.
        /// </summary>
        /// <param name="filename">The name of the file to create.</param>
        /// <param name="indexHtmFilename">The name of the index htm file.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateHHK( string filename, string indexHtmFilename, int depth )
        {
            OnOutputMessage( "Creating '" + filename + "':", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            bool didNotExist;
            if ( !OpenFileForEdit( filename, null, true, false, depth + 1, out didNotExist ) )
            {
                return false;
            }

            // create one from scratch
            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel < 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: Could not open/create '" + filename + "': " + e.Message + ".\r\n", depth + 1 );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "Writing to '" + filename + "':", depth + 1 );
            }

            writer.WriteLine( "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML//EN\">" );
            writer.WriteLine( "<HTML>" );
            writer.WriteLine( "\t<HEAD>" );
            writer.WriteLine( "\t\t<meta name=\"GENERATOR\" content=\"Microsoft&reg; HTML Help Workshop 4.1\">" );
            writer.WriteLine( "\t\t<!-- Sitemap 1.0 -->" );
            writer.WriteLine( "\t</HEAD>" );
            writer.WriteLine( "\t<BODY>" );
            writer.WriteLine( "\t\t<OBJECT type=\"text/site properties\">" );
            writer.WriteLine( "\t\t\t<param name=\"FrameName\" value=\"right\">" );
            writer.WriteLine( "\t\t</OBJECT>" );
            writer.WriteLine( "\t\t<UL>" );
            writer.WriteLine( "\t\t\t<LI> <OBJECT type=\"text/sitemap\">" );
            writer.WriteLine( "\t\t\t\t<param name=\"Name\" value=\"Home Page\">" );
            writer.WriteLine( "\t\t\t\t<param name=\"Name\" value=\"Home Page\">" );
            writer.WriteLine( "\t\t\t\t<param name=\"Local\" value=\"" + Path.GetFileName( indexHtmFilename ) + "\">" );
            writer.WriteLine( "\t\t\t</OBJECT>" );
            writer.WriteLine( "\t\t</UL>" );
            writer.WriteLine( "\t</BODY>" );
            writer.WriteLine( "</HTML>" );

            writer.Close();

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Creats a hhp file
        /// </summary>
        /// <param name="filename">The name of the file to create.</param>
        /// <param name="hhcFilename">The hhc file.</param>
        /// <param name="hhkFilename">The hhk file.</param>
        /// <param name="indexHtmFilename">The htm file.</param>
        /// <param name="compiledFilename">The output file.</param>
        /// <param name="layout">A list of chm files to merge.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateHHP( string filename, string hhcFilename, string hhkFilename, string indexHtmFilename, string compiledFilename, 
            HelpLayout layout, int depth )
        {
            OnOutputMessage( "Creating '" + filename + "':", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            bool didNotExist;
            if ( !OpenFileForEdit( filename, null, true, false, depth + 1, out didNotExist ) )
            {
                return false;
            }

            // create one from scratch
            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel < 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: Could not open/create '" + filename + "': " + e.Message + ".\r\n", depth + 1 );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "Writing to '" + filename + "':", depth + 1 );
            }

            writer.WriteLine( "[MERGE FILES]" );

            layout.WriteToHHP( writer );

            writer.WriteLine( "" );
            writer.WriteLine( "[FILES]" );
            writer.WriteLine( Path.GetFileName( indexHtmFilename ) );
            writer.WriteLine( "" );
            writer.WriteLine( "[OPTIONS]" );
            writer.WriteLine( "Auto Index=Yes" );
            writer.WriteLine( "Compatibility=1.1 or later" );
            writer.WriteLine( "Compiled File=" + compiledFilename );
            writer.WriteLine( "Contents File=" + hhcFilename );
            writer.WriteLine( "Index File=" + hhkFilename );
            writer.WriteLine( "Default Window=$global_main" );
            writer.WriteLine( "Default Topic=" + Path.GetFileName( indexHtmFilename ) );
            writer.WriteLine( "Display compile progress=Yes" );
            writer.WriteLine( "Error log file=_errorlog.txt" );
            writer.WriteLine( "Full-text search=Yes" );
            writer.WriteLine( "Language=0x409 English (United States)" );
            writer.WriteLine( "Title=" + Path.GetFileNameWithoutExtension( compiledFilename ) );
            writer.WriteLine( "Binary TOC=No" );
            writer.WriteLine( "Binary Index=Yes" );
            writer.WriteLine( "Default Font=" );
            writer.WriteLine( "Create CHI file=No" );
            writer.WriteLine( "" );
            writer.WriteLine( "[WINDOWS]" );
            writer.WriteLine( "$global_main=,\"" + hhcFilename + "\",\"" + hhkFilename + "\",\"" + Path.GetFileName( indexHtmFilename ) + "\",\"" + Path.GetFileName( indexHtmFilename ) + "\",,,,,0x63520,222,0x1046,[5,5,888,531],0xB0000,,,,,,0" );

            writer.Close();

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Creates an htm file.
        /// </summary>
        /// <param name="filename">The name of the file to create.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateIndexHtm( string filename, int depth )
        {
            OnOutputMessage( "Creating '" + filename + "':", depth );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
            }

            bool didNotExist;
            if ( !OpenFileForEdit( filename, null, true, false, depth + 1, out didNotExist ) )
            {
                return false;
            }

            // create one from scratch
            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );
            }
            catch ( Exception e )
            {
                if ( m_projectSettings.VerboseLevel < 2 )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: Could not open/create '" + filename + "': " + e.Message + ".\r\n", depth + 1 );
                return false;
            }

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( "Writing to '" + filename + "':", depth + 1 );
            }

            writer.WriteLine( "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Frameset//EN\">" );
            writer.WriteLine( "<!-- see the JavaScript template file for available variables -->" );
            writer.WriteLine( "<head>" );
            writer.WriteLine( "\t<title>" + m_projectSettings.UnifiedHelpFileName + "</title>" );
            writer.WriteLine( "</head>" );
            writer.WriteLine( "<html>" );
            writer.WriteLine( "\t<body bgcolor=white lang=EN-US style='tab-interval:.5in'>" );
            writer.WriteLine( "" );
            writer.WriteLine( "\t\t<div class=Section1>" );
            writer.WriteLine( "" );
            writer.WriteLine( "\t\t\t<h3 align=center style='text-align:center'><o:p>&nbsp;</o:p></h3>" );
            writer.WriteLine( "" );
            writer.WriteLine( "\t\t\t<h3 align=center style='text-align:center'><o:p>&nbsp;</o:p></h3>" );
            writer.WriteLine( "" );
            writer.WriteLine( "\t\t\t<h3 align=center style='text-align:center'><o:p>&nbsp;</o:p></h3>" );
            writer.WriteLine( "" );
            writer.WriteLine( "\t\t\t<h3 align=center style='text-align:center'><o:p>&nbsp;</o:p></h3>" );
            writer.WriteLine( "" );
            writer.WriteLine( "\t\t\t<p class=MsoNormal align=center style='text-align:center'><b style='mso-bidi-font-weight:normal'><span style='font-size:36.0pt'>" + m_projectSettings.UnifiedHelpFileName + "<o:p></o:p></span></b></p>" );
            writer.WriteLine( "" );
            writer.WriteLine( "\t\t</div>" );
            writer.WriteLine( "" );
            writer.WriteLine( "\t</body>" );
            writer.WriteLine( "</html>" );

            writer.Close();

            if ( m_projectSettings.VerboseLevel >= 3 )
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "Complete.\r\n", depth + 1 );
            }
            else
            {
                OnOutputMessage( " Complete.\r\n", 0 );
            }

            return true;
        }

        /// <summary>
        /// Creates the necessary files so that we can build a master chm file with HHC.exe
        /// </summary>
        /// <param name="hhpFilename">The name to give the hhp file.</param>
        /// <param name="compiledFilename">The name of the master help file to build.</param>
        /// <param name="unifiedFilenames">A list of filenames to be merged with the master.</param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool CreateMasterChmFiles( string hhpFilename, string compiledFilename, HelpLayout layout, int depth )
        {
            OnOutputMessage( "Creating master help files:\r\n", depth );

            string basename = Path.GetFileNameWithoutExtension( hhpFilename );
            string directory = Path.GetDirectoryName( hhpFilename );

            string indexHtmFilename = Path.Combine( directory, "index.htm" );
            if ( !CreateIndexHtm( indexHtmFilename, depth + 1 ) )
            {
                return false;
            }

            string hhkFilename = Path.Combine( directory, basename + ".hhk" );
            if ( !CreateHHK( hhkFilename, indexHtmFilename, depth + 1 ) )
            {
                return false;
            }

            string hhcFilename = Path.Combine( directory, basename + ".hhc" );
            if ( !CreateHHC( hhcFilename, indexHtmFilename, layout, depth + 1 ) )
            {
                return false;
            }

            if ( !CreateHHP( hhpFilename, basename + ".hhc", basename + ".hhk", indexHtmFilename, compiledFilename, layout, depth + 1 ) )
            {
                return false;
            }

            OnOutputMessage( "Complete.\r\n", depth + 1 );
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hhpFilename">The name of the project file</param>
        /// <param name="compiledFilename">The name of the .chm file that is being produced.</param>
        /// <param name="showWhenFinished"></param>
        /// <param name="depth">Indentation depth for <see cref="OnOutputMessage"/>.</param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        private bool BuildMasterChmFile( string hhpFilename, string compiledFilename, bool showWhenFinished, int depth )
        {
            OnOutputMessage( "Compiling Html Help file (HHC.exe):", depth );

            bool needLineBreak = true;
            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                OnOutputMessage( "\r\n", 0 );
                needLineBreak = false;
            }

            // look for and shut down any instances of the help that is running
            Process[] processes = Process.GetProcessesByName( "hh" );
            string name = Path.GetFileNameWithoutExtension( compiledFilename );
            foreach ( Process process in processes )
            {
                if ( process.MainWindowTitle.StartsWith( name ) )
                {
                    process.Kill();
                    break;
                }
            }

            rageExecuteCommand command = new rageExecuteCommand();
            command.Command = rageFileUtilities.GetProgramFilesx86() + @"\HTML Help Workshop\hhc.exe";
            command.Arguments = Path.GetFileName( hhpFilename );
            command.EchoToConsole = false;
            command.LogToHtmlFile = false;
            command.TimeOutInSeconds = 60.0f * 10.0f; // 10 minutes
            command.TimeOutWithoutOutputInSeconds = 60.0f * 5.0f; // 5 minutes
            command.UseBusySpinner = false;
            command.WorkingDirectory = Path.GetDirectoryName( hhpFilename );

            rageStatus status = new rageStatus();
            command.Execute( out status );

            if ( m_projectSettings.VerboseLevel >= 2 )
            {
                List<string> log = command.GetLogAsArray();
                foreach ( string line in log )
                {
                    OnOutputMessage( line.Trim() + "\r\n", depth + 1 );
                }
            }

            if ( command.GetExitCode() != 1 )
            {
                if ( needLineBreak )
                {
                    OnOutputMessage( "\r\n", 0 );
                }

                OnOutputMessage( "Error: " + status.GetErrorString() + "\r\n", depth + 1 );
                return false;
            }
            else
            {
                if ( needLineBreak )
                {
                    OnOutputMessage( " Complete.\r\n", 0 );
                }
                else
                {
                    OnOutputMessage( "Complete.\r\n", depth + 1 );
                }
            }

            if ( showWhenFinished )
            {
                Process.Start( compiledFilename );
            }

            return true;
        }
        #endregion

        #endregion

        #region Helper Classes

        #region Dox
        class DoxInfo
        {
            public DoxInfo()
            {

            }

            #region Variables
            private List<string> m_lines = new List<string>();
            private Dictionary<HelpType, DoxConfigurationInfo> m_configurationsByHelpType = new Dictionary<HelpType, DoxConfigurationInfo>();
            private Dictionary<string, DoxConfigurationInfo> m_configurationsByName = new Dictionary<string, DoxConfigurationInfo>();
            private int m_currentConfigurationIndex = -1;
            private HelpType m_currentConfigurationHelpKind = HelpType.help2;
            private List<int> m_macroIndexes = new List<int>();
            private List<int> m_relativePathIndexes = new List<int>();

            private int m_sourceFilesIndex = -1;
            
            private int m_classHierarchyIndex = -1;
            private int m_classHierarchyStartIndex = -1;
            private int m_classHiearchyEndIndex = -1;
            
            private int m_modulesIndex = -1;

            private int m_insertClassHierarchyAt = -1;
            private int m_insertModuleAt = -1;
            #endregion

            #region Properties
            public List<string> Lines
            {
                get
                {
                    return m_lines;
                }
            }

            public Dictionary<HelpType, DoxConfigurationInfo> ConfigurationsByHelpType
            {
                get
                {
                    return m_configurationsByHelpType;
                }
            }

            public Dictionary<string, DoxConfigurationInfo> ConfigurationsByName
            {
                get
                {
                    return m_configurationsByName;
                }
            }

            public int CurrentConfigurationIndex
            {
                get
                {
                    return m_currentConfigurationIndex;
                }
                set
                {
                    m_currentConfigurationIndex = value;
                }
            }

            public HelpType CurrentConfigurationHelpKind
            {
                get
                {
                    return m_currentConfigurationHelpKind;
                }
                set
                {
                    m_currentConfigurationHelpKind = value;
                }
            }

            public List<int> MacroIndexes
            {
                get
                {
                    return m_macroIndexes;
                }
            }

            public List<int> RelativePathIndexes
            {
                get
                {
                    return m_relativePathIndexes;
                }
            }

            public int SourceFilesIndex
            {
                get
                {
                    return m_sourceFilesIndex;
                }
                set
                {
                    m_sourceFilesIndex = value;
                }
            }

            public int ClassHierarchyIndex
            {
                get
                {
                    return m_classHierarchyIndex;
                }
                set
                {
                    m_classHierarchyIndex = value;
                }
            }

            public int ClassHierarchyStartIndex
            {
                get
                {
                    return m_classHierarchyStartIndex;
                }
                set
                {
                    m_classHierarchyStartIndex = value;
                }
            }

            public int ClassHierarchyEndIndex
            {
                get
                {
                    return m_classHiearchyEndIndex;
                }
                set
                {
                    m_classHiearchyEndIndex = value;

                    m_insertClassHierarchyAt = m_classHiearchyEndIndex + 1;
                }
            }

            public int ModulesIndex
            {
                get
                {
                    return m_modulesIndex;
                }
                set
                {
                    m_modulesIndex = value;

                    m_insertModuleAt = m_modulesIndex + 3;
                }
            }
            #endregion

            #region Public Functions
            public void AddConfiguration( string name, int index, HelpType helpKind, int helpKindIndex )
            {
                DoxConfigurationInfo dcInfo = new DoxConfigurationInfo( name, index, helpKind, helpKindIndex );

                m_configurationsByName[name] = dcInfo;
                m_configurationsByHelpType[helpKind] = dcInfo;
            }

            public void AddConfiguration( DoxConfigurationInfo dcInfo )
            {
                m_configurationsByName[dcInfo.Name] = dcInfo;
                m_configurationsByHelpType[dcInfo.HelpKind] = dcInfo;
            }

            public bool AddClassHierarchy( string module, string name, int hierarchyNumber )
            {
                if ( (this.ClassHierarchyEndIndex == -1) || (this.ClassHierarchyStartIndex == -1) )
                {
                    return false;
                }

                // our hierarchy will just be a copy of the Global Class Hierarchy
                int count = this.ClassHierarchyEndIndex - this.ClassHierarchyStartIndex + 1;
                string[] hierarchyLines = new string[count];
                this.Lines.CopyTo( this.ClassHierarchyStartIndex, hierarchyLines, 0, count );

                for ( int i = 0; i < count; ++i )
                {
                    string line = string.Empty;
                    if ( i == 1 )
                    {
                        line = "Module=" + module;
                    }
                    else if ( i == 2 )
                    {
                        line = "Name=" + name;
                    }
                    else
                    {
                        // add one to the number because because 0 is the Global Class Hierarchy
                        line = hierarchyLines[i].Replace( @"[Class Hierarchy\0", @"[Class Hierarchy\" + (hierarchyNumber + 1) );
                    }

                    this.Lines.Insert( m_insertClassHierarchyAt + i, line );
                }

                // fixup indexes
                ApplyOffset( count, m_insertClassHierarchyAt, null );

                // setup for next add, if any
                m_insertClassHierarchyAt += count;
                
                return true;
            }

            public bool AddModule( string name, string topicID, int moduleNumber, List<string> files )
            {
                if ( this.ModulesIndex == -1 )
                {
                    return false;
                }

                // create the module text
                // CAREFUL:  Since this isn't in the template, the format may change if the version of Doc-o-matic changes
                List<string> moduleLines = new List<string>();

                moduleLines.Add( @"[Modules\" + moduleNumber + "]" );
                moduleLines.Add( "Name=" + name );
                moduleLines.Add( "TopicID=" + topicID );
                moduleLines.Add( string.Empty );

                moduleLines.Add( @"[Modules\" + moduleNumber + @"\Files]" );
                moduleLines.Add( "Count=" + files.Count );
                for ( int i = 0; i < files.Count; ++i )
                {
                    moduleLines.Add( "File" + i + "=" + files[i] );
                }
                moduleLines.Add( string.Empty );

                // insert the text
                for ( int i = 0; i < moduleLines.Count; ++i )
                {
                    this.Lines.Insert( m_insertModuleAt + i, moduleLines[i] );
                }

                // fixup indexes
                ApplyOffset( moduleLines.Count, m_insertModuleAt, null );

                // setup for next add, if any
                m_insertModuleAt += moduleLines.Count;

                return true;
            }

            public bool AddDisabledTopics( HelpType helpKind, List<string> disabledTopics )
            {
                DoxConfigurationInfo dcInfo;
                if ( m_configurationsByHelpType.TryGetValue( helpKind, out dcInfo ) )
                {
                    return AddDisabledTopics( dcInfo, disabledTopics );
                }

                return false;
            }

            public bool AddDisabledTopics( string configName, List<string> disabledTopics )
            {
                DoxConfigurationInfo dcInfo;
                if ( m_configurationsByName.TryGetValue( configName, out dcInfo ) )
                {
                    return AddDisabledTopics( dcInfo, disabledTopics );
                }

                return false;
            }

            public bool AddDisabledTopics( DoxConfigurationInfo dcInfo, List<string> disabledTopics )
            {
                SortedDictionary<string, string> topicDictionary = new SortedDictionary<string, string>();

                // get count
                int countIndex = dcInfo.DisabledTopicIndex + 1;
                int indexOf = this.Lines[countIndex].IndexOf( '=' );
                int count = 0;
                if ( indexOf > -1 )
                {
                    string countString = this.Lines[countIndex].Substring( indexOf + 1 ).Trim();
                    count = int.Parse( countString );
                }

                // get the name of each disabled topic
                for ( int j = 0; j < count; ++j )
                {
                    int nameIndex = countIndex + j + 1;
                    indexOf = this.Lines[nameIndex].IndexOf( '=' );
                    if ( indexOf > -1 )
                    {
                        string nameString = this.Lines[nameIndex].Substring( indexOf + 1 ).Trim();

                        if ( !topicDictionary.ContainsKey( nameString.ToLower() ) )
                        {
                            topicDictionary.Add( nameString.ToLower(), nameString );
                        }
                    }
                }

#if DISABLE_FILES_TOPIC
                // make sure !!FILES is in there
                if ( !topicDictionary.ContainsKey( "!!FILES" ) )
                {
                    topicDictionary.Add( "!!FILES", "!!FILES" );
                }
#else
                // make sure !!FILES is in there
                if ( topicDictionary.ContainsKey( "!!FILES" ) )
                {
                    topicDictionary.Remove( "!!FILES" );
                }
#endif

                // add disabledTopics to the dictionary
                foreach ( string disabledTopic in disabledTopics )
                {
                    if ( !topicDictionary.ContainsKey( disabledTopic.ToLower() ) )
                    {
                        topicDictionary.Add( disabledTopic.ToLower(), disabledTopic );
                    }
                }

                // replace/add the disabled topics
                string[] sortedTopics = new string[topicDictionary.Count];
                topicDictionary.Values.CopyTo( sortedTopics, 0 );
                for ( int j = 0; j < sortedTopics.Length; ++j )
                {
                    int nameIndex = countIndex + j + 1;
                    if ( j < count )
                    {
                        this.Lines[nameIndex] = "ID" + j + "=" + sortedTopics[j];
                    }
                    else
                    {
                        this.Lines.Insert( nameIndex, "ID" + j + "=" + sortedTopics[j] );
                    }
                }

                // fixup indexes
                ApplyOffset( sortedTopics.Length - count, dcInfo.DisabledTopicIndex, dcInfo );

                return true;
            }

            public bool AddSourceFiles( List<string> files )
            {
                if ( this.SourceFilesIndex == -1 )
                {
                    return false;
                }

                if ( files.Count == 0 )
                {
                    return true;
                }

                int currentCount = int.Parse( this.Lines[this.SourceFilesIndex + 1].Substring( this.Lines[this.SourceFilesIndex + 1].IndexOf( '=' ) + 1 ) );

                this.Lines[this.SourceFilesIndex + 1] = "Count=" + (files.Count + currentCount);

                for ( int i = currentCount; i < files.Count + currentCount; ++i )
                {
                    this.Lines.Insert( this.SourceFilesIndex + i + 2, "File" + i + "=" + files[i - currentCount] );
                }

                // fixup indexes
                ApplyOffset( files.Count, this.SourceFilesIndex + currentCount + 2, null );

                return true;
            }

            public bool FixupRelativePaths( string srcPath, string destPath )
            {
                foreach ( int index in this.RelativePathIndexes )
                {
                    int indexOf = this.Lines[index].IndexOf( '=' ) + 1;
                    string relativePath = this.Lines[index].Substring( indexOf );
                    string absolutePath = Path.GetFullPath( Path.Combine( srcPath, relativePath ) );
                    
                    relativePath = ProjectSettings.GetRelativePath( destPath, absolutePath );

                    this.Lines[index] = this.Lines[index].Substring( 0, indexOf ) + relativePath;
                }

                return true;
            }
            #endregion

            #region Private Functions
            private void ApplyOffset( int offset, int relativeIndex, DoxConfigurationInfo relativeDcInfo )
            {
                if ( this.SourceFilesIndex > relativeIndex )
                {
                    this.SourceFilesIndex += offset;
                }

                if ( this.ClassHierarchyIndex > relativeIndex )
                {
                    this.ClassHierarchyIndex += offset;
                }

                if ( this.ClassHierarchyStartIndex > relativeIndex )
                {
                    this.ClassHierarchyStartIndex += offset;
                }

                if ( this.ClassHierarchyEndIndex > relativeIndex )
                {
                    this.ClassHierarchyEndIndex += offset;
                }

                if ( this.ModulesIndex > relativeIndex )
                {
                    this.ModulesIndex += offset;
                }

                if ( relativeDcInfo != null )
                {
                    relativeDcInfo.ApplyOffset( offset, relativeIndex );
                }

                foreach ( DoxConfigurationInfo info in this.ConfigurationsByHelpType.Values )
                {
                    if ( info == relativeDcInfo )
                    {
                        continue;
                    }

                    info.ApplyOffset( offset, relativeIndex );
                }

                for ( int i = 0; i < this.MacroIndexes.Count; ++i )
                {
                    if ( this.MacroIndexes[i] > relativeIndex )
                    {
                        this.MacroIndexes[i] += offset;
                    }
                }

                for ( int i = 0; i < this.RelativePathIndexes.Count; ++i )
                {
                    if ( this.RelativePathIndexes[i] > relativeIndex )
                    {
                        this.RelativePathIndexes[i] += offset;
                    }
                }
            }
            #endregion
        };

        class DoxConfigurationInfo
        {
            public DoxConfigurationInfo( string name, int index, HelpType helpKind, int helpKindIndex )
            {
                m_name = name;
                m_index = index;
                m_helpType = helpKind;
                m_helpKindIndex = helpKindIndex;
            }

            #region Variables
            private string m_name;
            private int m_index;
            private HelpType m_helpType;
            private int m_helpKindIndex = -1;
            private int m_disabledTopicIndex = -1;

            private int m_outputDirIndex = -1;
            private int m_classHierarchyLayoutOutputDirIndex = -1;

            private int m_htmlHelpFilenameIndex = -1;
            private int m_htmlHelp2FilenameIndex = -1;
            private int m_hlpFilenameIndex = -1;
            private int m_outputFileIndex = -1;
            
            private bool m_createPrintableDocumentation = false;
            private int m_help2CompileResult = 0;
            #endregion

            #region Properties
            public string Name
            {
                get
                {
                    return m_name;
                }
            }

            public int Index
            {
                get
                {
                    return m_index;
                }
                set
                {
                    m_index = value;
                }
            }

            public HelpType HelpKind
            {
                get
                {
                    return m_helpType;
                }
            }

            public int HelpKindIndex
            {
                get
                {
                    return m_helpKindIndex;
                }
                set
                {
                    m_helpKindIndex = value;
                }
            }

            public int DisabledTopicIndex
            {
                get
                {
                    return m_disabledTopicIndex;
                }
                set
                {
                    m_disabledTopicIndex = value;
                }
            }

            /// <summary>
            /// The directory where intermediate files will be placed.  In the case of Html, the destination directory.
            /// </summary>
            public int OutputDirIndex
            {
                get
                {
                    return m_outputDirIndex;
                }
                set
                {
                    m_outputDirIndex = value;
                }
            }

            /// <summary>
            /// The directory where the class hierarchy graphics files will be placed.  In the case of Html, the destination directory (or subdirectory).
            /// </summary>
            public int ClassHierarchyLayoutOutputDirIndex
            {
                get
                {
                    return m_classHierarchyLayoutOutputDirIndex;
                }
                set
                {
                    m_classHierarchyLayoutOutputDirIndex = value;
                }
            }

            /// <summary>
            /// The output filename for Html Help.
            /// </summary>
            public int HtmlHelpFilenameIndex
            {
                get
                {
                    return m_htmlHelpFilenameIndex;
                }
                set
                {
                    m_htmlHelpFilenameIndex = value;
                }
            }

            /// <summary>
            /// The output filename for Help 2.
            /// </summary>
            public int HtmlHelp2FilenameIndex
            {
                get
                {
                    return m_htmlHelp2FilenameIndex;
                }
                set
                {
                    m_htmlHelp2FilenameIndex = value;
                }
            }

            /// <summary>
            /// The ouput file for Windows Help.
            /// </summary>
            public int HlpFilenameIndex
            {
                get
                {
                    return m_hlpFilenameIndex;
                }
                set
                {
                    m_hlpFilenameIndex = value;
                }
            }

            /// <summary>
            /// The output file for RTF, XML, and PDF.
            /// </summary>
            public int OutputFileIndex
            {
                get
                {
                    return m_outputFileIndex;
                }
                set
                {
                    m_outputFileIndex = value;
                }
            }

            /// <summary>
            /// Used by RTF configuration to determine when a .hlp file is generated
            /// </summary>
            public bool CreatePrintableDocumentation
            {
                get
                {
                    return m_createPrintableDocumentation;
                }
                set
                {
                    m_createPrintableDocumentation = value;
                }
            }

            public int Help2CompileResult
            {
                get
                {
                    return m_help2CompileResult;
                }
                set
                {
                    m_help2CompileResult = value;
                }
            }
            #endregion

            #region Public Functions
            public int GetHelpFilenameIndex( HelpType helpKind )
            {
                switch ( helpKind )
                {
                    case HelpType.chm:
                        return this.HtmlHelpFilenameIndex;
                    case HelpType.help2:
                        return this.HtmlHelp2FilenameIndex;
                    case HelpType.html:
                        return -1;
                    case HelpType.pdf:
                        return this.OutputFileIndex;
                    case HelpType.rtf:
                        if ( this.CreatePrintableDocumentation )
                        {
                            return this.OutputFileIndex;
                        }
                        else
                        {
                            return this.HlpFilenameIndex;
                        }
                    case HelpType.xml:
                        return this.OutputFileIndex;

                    default:
                        return -1;
                }
            }

            public string GetHelpFilenameAttributeName( HelpType helpKind )
            {
                switch ( helpKind )
                {
                    case HelpType.chm:
                        return "HtmlHelpFilename";
                    case HelpType.help2:
                        return "HtmlHelp2Filename";
                    case HelpType.html:
                        return string.Empty;
                    case HelpType.pdf:
                        return "OutputFile";
                    case HelpType.rtf:
                        if ( this.CreatePrintableDocumentation )
                        {
                            return "OutputFile";
                        }
                        else
                        {
                            return "HlpFilename";
                        }
                    case HelpType.xml:
                        return "OutputFile";

                    default:
                        return null;
                }
            }

            public string GetHelpFilenameExtension( HelpType helpKind )
            {
                switch ( helpKind )
                {
                    case HelpType.chm:
                        return ".chm";
                    case HelpType.help2:
                        return ".HxS";
                    case HelpType.html:
                        return string.Empty;
                    case HelpType.pdf:
                        return ".pdf";
                    case HelpType.rtf:
                        if ( this.CreatePrintableDocumentation )
                        {
                            return ".rtf";
                        }
                        else
                        {
                            return ".hlp";
                        }
                    case HelpType.xml:
                        return ".xml";

                    default:
                        return null;
                }
            }

            public void ApplyOffset( int offset, int relativeIndex )
            {
                if ( this.HelpKindIndex > relativeIndex )
                {
                    this.HelpKindIndex += offset;
                }

                if ( this.DisabledTopicIndex > relativeIndex )
                {
                    this.DisabledTopicIndex += offset;
                }

                if ( this.OutputDirIndex > relativeIndex )
                {
                    this.OutputDirIndex += offset;
                }

                if ( this.ClassHierarchyLayoutOutputDirIndex > relativeIndex )
                {
                    this.ClassHierarchyLayoutOutputDirIndex += offset;
                }

                if ( this.HtmlHelpFilenameIndex > relativeIndex )
                {
                    this.HtmlHelpFilenameIndex += offset;
                }

                if ( this.HtmlHelp2FilenameIndex > relativeIndex )
                {
                    this.HtmlHelp2FilenameIndex += offset;
                }

                if ( this.HlpFilenameIndex > relativeIndex )
                {
                    this.HlpFilenameIndex += offset;
                }

                if ( this.OutputFileIndex > relativeIndex )
                {
                    this.OutputFileIndex += offset;
                }
            }
            #endregion
        };
        #endregion

        #region Dtx
        class DtxTopicInfo
        {
            public DtxTopicInfo()
            {

            }

            public int TopicIDLineIndex = -1;
            public string TopicID = null;

            public int GroupTagLineIndex = -1;
            public string GroupTopicID = null;
            public int GroupTagIndex = -1;
            public int GroupTagLength = -1;

            public int TitleTagLineIndex = -1;

            public void ApplyOffset( int offset, int relativeIndex )
            {
                if ( this.TopicIDLineIndex >= relativeIndex )
                {
                    this.TopicIDLineIndex += offset;
                }

                if ( this.GroupTagLineIndex >= relativeIndex )
                {
                    this.GroupTagLineIndex += offset;
                }

                if ( this.TitleTagLineIndex >= relativeIndex )
                {
                    this.TitleTagLineIndex += offset;
                }
            }
        };

        class DtxSectionInfo
        {
            public DtxSectionInfo()
            {

            }

            public string SectionName = null;
            public int StartLineIndex = -1;
            public int EndLineIndex = -1;

            public void ApplyOffset( int offset, int relativeIndex )
            {
                if ( this.StartLineIndex > +relativeIndex )
                {
                    this.StartLineIndex += offset;
                }

                if ( this.EndLineIndex >= relativeIndex )
                {
                    this.EndLineIndex += offset;
                }
            }
        };

        class DtxInfo
        {
            public DtxInfo()
            {

            }

            #region Variables
            private List<string> m_lines = new List<string>();
            private List<DtxTopicInfo> m_topicInfos = new List<DtxTopicInfo>();
            private List<DtxSectionInfo> m_sectionInfos = new List<DtxSectionInfo>();

            private bool m_initialized = false;
            private bool m_hasBeenGrouped = false;
            #endregion

            #region Properties
            public List<string> Lines
            {
                get
                {
                    return m_lines;
                }
            }

            public bool TopicInfosInitialized
            {
                get
                {
                    return m_initialized;
                }
            }
            #endregion

            #region Public Functions
            public bool ReplaceSection( string sectionName, List<string> replacementLines )
            {
                if ( !m_initialized )
                {
                    if ( !Initialize() )
                    {
                        return false;
                    }
                }

                for ( int i = 0; i < m_sectionInfos.Count; ++i )
                {
                    DtxSectionInfo sInfo = m_sectionInfos[i];
                    if ( sInfo.SectionName == sectionName )
                    {
                        // remove lines
                        int offset = (sInfo.EndLineIndex - 1) - (sInfo.StartLineIndex + 1) + 1;

                        if ( offset > 0 )
                        {
                            this.Lines.RemoveRange( sInfo.StartLineIndex + 1, offset );
                        }

                        // add lines
                        this.Lines.InsertRange( sInfo.StartLineIndex + 1, replacementLines );

                        // fixup indexes
                        offset = replacementLines.Count - offset;
                        for ( int j = i + 1; j < m_sectionInfos.Count; ++j )
                        {
                            m_sectionInfos[j].ApplyOffset( offset, sInfo.StartLineIndex );
                        }

                        foreach ( DtxTopicInfo tInfo in m_topicInfos )
                        {
                            tInfo.ApplyOffset( offset, sInfo.StartLineIndex );
                        }

                        sInfo.EndLineIndex += offset;

                        // re-initialize to pick up any new topics or sections 
                        Initialize();

                        return true;
                    }
                }

                return false;
            }

            public bool GroupUnderTopicID( string groupTopicID )
            {
                if ( m_hasBeenGrouped )
                {
                    return true;
                }

                if ( !m_initialized )
                {
                    if ( !Initialize() )
                    {
                        return false;
                    }
                }

                // make sure everything has a group tag, if not, group it under groupTopicID
                string groupTag = "<GROUP " + groupTopicID + ">";
                for ( int i = 0; i < m_topicInfos.Count; ++i )
                {
                    DtxTopicInfo tInfo = m_topicInfos[i];
                    if ( tInfo.GroupTagLineIndex == -1 )
                    {
                        tInfo.GroupTagLineIndex = tInfo.TopicIDLineIndex + 1;
                        tInfo.GroupTopicID = groupTopicID;
                        tInfo.GroupTagIndex = 0;
                        tInfo.GroupTagLength = groupTag.Length;

                        this.Lines.Insert( tInfo.GroupTagLineIndex, groupTag );

                        // fixup indexes
                        if ( tInfo.TitleTagLineIndex != -1 )
                        {
                            tInfo.TitleTagLineIndex += 1;
                        }

                        for ( int j = i + 1; j < m_topicInfos.Count; ++j )
                        {
                            m_topicInfos[j].ApplyOffset( 1, tInfo.TopicIDLineIndex );
                        }

                        foreach ( DtxSectionInfo sInfo in m_sectionInfos )
                        {
                            sInfo.ApplyOffset( 1, tInfo.TopicIDLineIndex );
                        }
                    }
                }

                m_hasBeenGrouped = true;
                return true;
            }

            public bool EnforceUniqueTopicIDs( Dictionary<string, string> topicIDDict )
            {
                if ( !m_initialized )
                {
                    if ( !Initialize() )
                    {
                        return false;
                    }
                }

                Dictionary<string, string> topicIDReplacementDict = new Dictionary<string, string>();

                // make sure all TopicIDs are unique
                for ( int i = 0; i < m_topicInfos.Count; ++i )
                {
                    DtxTopicInfo tInfo = m_topicInfos[i];
                    if ( topicIDDict.ContainsKey( tInfo.TopicID ) )
                    {
                        // create new topicID
                        Random r = new Random();
                        string newTopicID = tInfo.TopicID + r.Next();
                        while ( topicIDDict.ContainsKey( newTopicID ) )
                        {
                            newTopicID = tInfo.TopicID + r.Next();
                        }

                        // change the topic name
                        this.Lines[tInfo.TopicIDLineIndex] = "@@" + newTopicID;

                        // add it to our dictionary
                        topicIDDict[newTopicID] = newTopicID;

                        // we need to replace all occurrences of this topic if we find it in a GROUP tag
                        topicIDReplacementDict[tInfo.TopicID] = newTopicID;

                        // add a title so we preserve the old name in the TOC
                        if ( tInfo.TitleTagLineIndex == -1 )
                        {
                            tInfo.TitleTagLineIndex = tInfo.TopicIDLineIndex + 1;
                            this.Lines.Insert( tInfo.TitleTagLineIndex, "<TITLE " + tInfo.TopicID + ">" );

                            // fixup indexes
                            if ( tInfo.GroupTagLineIndex != -1 )
                            {
                                tInfo.GroupTagLineIndex += 1;
                            }

                            for ( int j = i + 1; j < m_topicInfos.Count; ++j )
                            {
                                m_topicInfos[j].ApplyOffset( 1, tInfo.TopicIDLineIndex );
                            }

                            foreach ( DtxSectionInfo sInfo in m_sectionInfos )
                            {
                                sInfo.ApplyOffset( 1, tInfo.TopicIDLineIndex );
                            }
                        }

                        // make the change permanent
                        tInfo.TopicID = newTopicID;
                    }
                    else
                    {
                        topicIDDict.Add( tInfo.TopicID, tInfo.TopicID );
                    }
                }

                // look for GROUP tags and make replacements where necessary
                for ( int i = 0; i < m_topicInfos.Count; ++i )
                {
                    DtxTopicInfo tInfo = m_topicInfos[i];
                    if ( tInfo.GroupTagLineIndex != -1 )
                    {
                        string newGroupTopicID;
                        if ( topicIDReplacementDict.TryGetValue( tInfo.GroupTopicID, out newGroupTopicID ) )
                        {
                            // separate the line into the group tag and what comes before and after it
                            string preText = string.Empty;
                            if ( tInfo.GroupTagIndex > 0 )
                            {
                                preText = this.Lines[tInfo.GroupTagLineIndex].Substring( 0, tInfo.GroupTagIndex );
                            }

                            string groupTag = this.Lines[tInfo.GroupTagLineIndex].Substring( tInfo.GroupTagIndex, tInfo.GroupTagLength );

                            string postText = string.Empty;
                            if ( this.Lines[tInfo.GroupTagLineIndex].Length > tInfo.GroupTagIndex + tInfo.GroupTagLength )
                            {
                                postText = this.Lines[tInfo.GroupTagLineIndex].Substring( tInfo.GroupTagIndex + tInfo.GroupTagLength );
                            }

                            // separate the tag into "<GROUP" and ">..."
                            int indexOf = groupTag.ToLower().IndexOf( "<group " );
                            string preGroupTag = groupTag.Substring( 0, 7 );

                            indexOf = groupTag.IndexOf( ">" );
                            string postGroupTag = groupTag.Substring( indexOf );

                            // form the new tag
                            string newGroupTag = preGroupTag + newGroupTopicID + postGroupTag;

                            // replace the old line with the new
                            this.Lines[tInfo.GroupTagLineIndex] = preText + newGroupTag + postText;

                            // make the change permanent
                            tInfo.GroupTopicID = newGroupTopicID;
                        }
                    }
                }

                return true;
            }

            public bool AddTopic( string topicID, string topicTitle, string groupTopicID )
            {
                DtxTopicInfo tInfo = new DtxTopicInfo();
                m_topicInfos.Add( tInfo );

                tInfo.TopicID = topicID;

                this.Lines.Add( string.Empty );

                tInfo.TopicIDLineIndex = this.Lines.Count;
                this.Lines.Add( "@@" + topicID );

                if ( !String.IsNullOrEmpty( topicTitle ) )
                {
                    tInfo.TitleTagLineIndex = this.Lines.Count;
                    this.Lines.Add( "<TITLE " + topicTitle + ">" );
                }

                if ( !String.IsNullOrEmpty( groupTopicID ) )
                {
                    string groupTag = "<GROUP " + groupTopicID + ">";
                    tInfo.GroupTagLineIndex = this.Lines.Count;
                    tInfo.GroupTagIndex = 0;
                    tInfo.GroupTagLength = groupTag.Length;

                    this.Lines.Add( groupTag );
                }

                this.Lines.Add( string.Empty );

                return true;
            }
            #endregion

            #region Private Functions
            private bool Initialize()
            {
                m_topicInfos.Clear();
                m_sectionInfos.Clear();

                Regex regExTopicID = new Regex( @"\A\x40\x40\s*(?<topicID>.+)\s*", RegexOptions.Compiled );
                Regex regExGroupTag = new Regex( @"\x3cGROUP\s+(?<topicID>.+)\s*\x3e", RegexOptions.IgnoreCase | RegexOptions.Compiled );
                Regex regExTitleTag = new Regex( @"\x3cTITLE\s+(?<title>.+)\s*\x3e", RegexOptions.IgnoreCase | RegexOptions.Compiled );
                Regex regExSectionBegin = new Regex( @"\A\s*\##BEGIN\s+(?<sectionName>\S+)", RegexOptions.Compiled );
                Regex regExSectionEnd = new Regex( @"\A\s*\##END\s+(?<sectionName>\S+)", RegexOptions.Compiled );

                DtxTopicInfo tInfo = null;
                DtxSectionInfo sInfo = null;

                for ( int i = 0; i < this.Lines.Count; ++i )
                {
                    Match match;
                    if ( (match = regExTopicID.Match( this.Lines[i] )).Success )
                    {
                        tInfo = new DtxTopicInfo();
                        tInfo.TopicIDLineIndex = i;
                        tInfo.TopicID = match.Result( "${topicID}" );

                        m_topicInfos.Add( tInfo );
                    }
                    else if ( (match = regExSectionBegin.Match( this.Lines[i] )).Success )
                    {
                        if ( sInfo != null )
                        {
                            // this is an error
                            return false;
                        }

                        sInfo = new DtxSectionInfo();
                        sInfo.StartLineIndex = i;
                        sInfo.SectionName = match.Result( "${sectionName}" );

                    }
                    else if ( (match = regExSectionEnd.Match( this.Lines[i] )).Success )
                    {
                        if ( (sInfo == null) || (match.Result( "${sectionName}" ) != sInfo.SectionName) )
                        {
                            // this is an error
                            return false;
                        }

                        sInfo.EndLineIndex = i;

                        m_sectionInfos.Add( sInfo );

                        sInfo = null;
                    }
                    else
                    {
                        match = regExGroupTag.Match( this.Lines[i] );
                        if ( match.Success )
                        {
                            if ( tInfo != null )
                            {
                                tInfo.GroupTagLineIndex = i;
                                tInfo.GroupTopicID = match.Result( "${topicID}" );
                                tInfo.GroupTagIndex = match.Index;
                                tInfo.GroupTagLength = match.Length;
                            }
                        }

                        match = regExTitleTag.Match( this.Lines[i] );
                        if ( match.Success )
                        {
                            if ( tInfo != null )
                            {
                                tInfo.TitleTagLineIndex = i;
                            }
                        }
                    }
                }

                m_initialized = true;
                return true;
            }
            #endregion
        };
        #endregion

        #region Samples
        class SampleStep : System.Object
        {
            public SampleStep( int stepNum, string description )
            {
                m_stepNumber = stepNum;
                m_description = description;
            }

            #region Variables
            private int m_stepNumber = -1;
            private string m_description = string.Empty;
            private List<string> m_stepBody = new List<string>();
            #endregion

            #region Properties
            public int StepNumber
            {
                get
                {
                    return m_stepNumber;
                }
            }

            public string Description
            {
                get
                {
                    return m_description;
                }
            }

            public List<string> StepBody
            {
                get
                {
                    return m_stepBody;
                }
            }
            #endregion

            #region Overrides
            public override String ToString()
            {
                return Convert.ToString( this.StepNumber );
            }
            #endregion
        };
        #endregion

        #region Command Line Parameters
        class ParamInfo : IComparable
        {
            public ParamInfo( string moduleName, string filename, string paramName, string description, int numArgs )
            {
                m_moduleName = moduleName;
                m_filename = filename;
                m_paramName = paramName;
                m_description = description;
                m_numArguments = numArgs;
            }

            #region Variables
            private string m_moduleName;
            private string m_filename;
            private string m_paramName;
            private string m_description;
            private int m_numArguments;
            #endregion

            #region Properties
            public string ModuleName
            {
                get
                {
                    return m_moduleName;
                }
            }

            public string FileName
            {
                get
                {
                    return m_filename;
                }
            }

            public string ParamName
            {
                get
                {
                    return m_paramName;
                }
            }

            public string Description
            {
                get
                {
                    return m_description;
                }
            }

            public int NumArguments
            {
                get
                {
                    return m_numArguments;
                }
            }
            #endregion

            /// <summary>
            /// IComparable.CompareTo implementation.
            /// </summary>
            public int CompareTo( object obj )
            {
                if ( obj is ParamInfo )
                {
                    ParamInfo param = (ParamInfo)obj;

                    return ParamName.CompareTo( param.ParamName );
                }

                throw new ArgumentException( "object is not a ParamInfo" );
            }

            #region Public Functions
            public string GetTableRowString( int paramNameLength, int filenameLength, int descriptionLength )
            {
                StringBuilder text = new StringBuilder();

                text.Append( BuildTableString( this.ParamName, paramNameLength ) );
                text.Append( BuildTableString( this.FileName, filenameLength ) );
                text.Append( BuildTableString( this.Description, descriptionLength ) );

                return text.ToString();
            }

            public string GetSeparatorRowString( int paramNameLength, int filenameLength, int descriptionLength )
            {
                StringBuilder text = new StringBuilder();

                text.Append( BuildSeparatorString( paramNameLength ) );
                text.Append( BuildSeparatorString( filenameLength ) );
                text.Append( BuildSeparatorString( descriptionLength ) );

                return text.ToString();
            }
            #endregion

            #region Private Functions
            private string BuildTableString( string header, int longestLength )
            {
                StringBuilder text = new StringBuilder();

                int headerLength = 0;
                if ( header != null )
                {
                    text.Append( header );
                    headerLength = header.Length;
                }

                for ( int i = 0; i < longestLength - headerLength; ++i )
                {
                    text.Append( ' ' );
                }

                text.Append( "  " );

                return text.ToString();
            }

            private string BuildSeparatorString( int length )
            {
                StringBuilder text = new StringBuilder();

                for ( int i = 0; i < length; i++ )
                {
                    text.Append( '-' );
                }

                text.Append( "  " );

                return text.ToString();
            }
            #endregion
        };
        #endregion

        #region Help Layout
        abstract class HelpLayoutItem : System.Object
        {
            public HelpLayoutItem( string name )
            {
                m_name = name;
            }

            #region Variables
            protected string m_name = string.Empty;
            #endregion

            #region Properties
            public string Name
            {
                get
                {
                    return m_name;
                }
            }
            #endregion

            #region Overrides
            public override string ToString()
            {
                return this.Name;
            }
            #endregion

            #region Abstract Functions
            public abstract void WriteToHHC( TextWriter writer );

            public abstract void WriteToHHP( TextWriter writer );
            #endregion
        };

        class HelpLayoutModule : HelpLayoutItem
        {
            public HelpLayoutModule( string name )
                : base( name )
            {

            }

            #region Public Functions
            public override void WriteToHHC( TextWriter writer )
            {
                writer.WriteLine( "\t\t\t<LI><OBJECT type=\"text/sitemap\">" );
                writer.WriteLine( "\t\t\t\t<param name=\"Name\" value=\"" + this.ToString() + "\">" );
                writer.WriteLine( "\t\t\t</OBJECT>" );
                writer.WriteLine( "\t\t\t<OBJECT type=\"text/sitemap\">" );
                writer.WriteLine( "\t\t\t\t<param name=\"Merge\" value=\"" + this.ToString() + ".chm::/" + this.ToString() + ".hhc\">" );
                writer.WriteLine( "\t\t\t</OBJECT>" );
            }

            public override void WriteToHHP( TextWriter writer )
            {
                writer.WriteLine( this.ToString() + ".chm" );
            }
            #endregion
        };

        class HelpLayoutTitle : HelpLayoutItem
        {
            public HelpLayoutTitle( string title )
                : base( title )
            {
            }

            #region Variables
            private List<HelpLayoutItem> m_subItems = new List<HelpLayoutItem>();
            #endregion

            #region Properties
            public List<HelpLayoutItem> SubItems
            {
                get
                {
                    return m_subItems;
                }
            }
            #endregion

            #region Public Functions
            public override void WriteToHHC( TextWriter writer )
            {
                writer.WriteLine( "<LI> <OBJECT type=\"text/sitemap\">" );
                writer.WriteLine( "\t<param name=\"Name\" value=\"" + this.ToString() + "\">" );
                writer.WriteLine( "</OBJECT>" );
                writer.WriteLine( "<UL>" );

                foreach ( HelpLayoutItem item in this.SubItems )
                {
                    item.WriteToHHC( writer );
                }

                writer.WriteLine( "</UL>" );
            }

            public override void WriteToHHP( TextWriter writer )
            {
                foreach ( HelpLayoutItem item in this.SubItems )
                {
                    item.WriteToHHP( writer );
                }
            }
            #endregion
        };
        
        class HelpLayout
        {
            public HelpLayout()
            {

            }

            #region Variables
            private List<HelpLayoutItem> m_items = new List<HelpLayoutItem>();
            #endregion

            #region Properties
            public List<HelpLayoutItem> Items
            {
                get
                {
                    return m_items;
                }
            }
            #endregion

            #region Public Functions
            public HelpLayoutTitle FindTitle( string name )
            {
                foreach ( HelpLayoutItem item in m_items )
                {
                    HelpLayoutTitle title = FindTitle( name, item );
                    if ( title != null )
                    {
                        return title;
                    }
                }

                return null;
            }

            public bool TitleExists( string name )
            {
                return FindTitle( name ) != null;
            }

            public HelpLayoutModule FindModule( string name )
            {
                foreach ( HelpLayoutItem item in m_items )
                {
                    HelpLayoutModule module = FindModule( name, item );
                    if ( module != null )
                    {
                        return module;
                    }
                }

                return null;
            }

            public bool ModuleExists( string name )
            {
                return FindModule( name ) != null;
            }

            public void RemoveModule( string name )
            {
                for ( int i = 0; i < m_items.Count; ++i )
                {
                    if ( RemoveModule( name, m_items[i] ) )
                    {
                        m_items.RemoveAt( i );
                        break;
                    }
                }
            }

            public void RemoveModulesNotInDictionary( Dictionary<string, string> modules )
            {
                for ( int i = 0; i < m_items.Count; ++i )
                {
                    if ( RemoveModulesNotInDictionary( modules, m_items[i] ) )
                    {
                        m_items.RemoveAt( i );
                        break;
                    }
                }
            }

            public void WriteToHHC( TextWriter writer )
            {
                foreach ( HelpLayoutItem item in this.Items )
                {
                    item.WriteToHHC( writer );
                }
            }

            public void WriteToHHP( TextWriter writer )
            {
                foreach ( HelpLayoutItem item in this.Items )
                {
                    item.WriteToHHP( writer );
                }
            }
            #endregion

            #region private Functions
            private HelpLayoutTitle FindTitle( string name, HelpLayoutItem item )
            {
                if ( item is HelpLayoutTitle )
                {
                    if ( item.Name == name )
                    {
                        return item as HelpLayoutTitle;
                    }

                    HelpLayoutTitle title = item as HelpLayoutTitle;
                    foreach ( HelpLayoutItem subItem in title.SubItems )
                    {
                        HelpLayoutTitle subTitle = FindTitle( name, subItem );
                        if ( subTitle != null )
                        {
                            return subTitle;
                        }
                    }
                }

                return null;
            }

            private HelpLayoutModule FindModule( string name, HelpLayoutItem item )
            {
                if ( item is HelpLayoutModule )
                {
                    if ( item.Name == name )
                    {
                        return item as HelpLayoutModule;
                    }
                }
                else if ( item is HelpLayoutTitle )
                {
                    HelpLayoutTitle title = item as HelpLayoutTitle;
                    foreach ( HelpLayoutItem subItem in title.SubItems )
                    {
                        HelpLayoutModule module = FindModule( name, subItem );
                        if ( module != null )
                        {
                            return module;
                        }
                    }
                }

                return null;
            }

            private bool RemoveModule( string name, HelpLayoutItem item )
            {
                if ( item is HelpLayoutModule )
                {
                    if ( item.Name == name )
                    {
                        return true;
                    }
                }
                else if ( item is HelpLayoutTitle )
                {
                    HelpLayoutTitle title = item as HelpLayoutTitle;
                    for ( int i = 0; i < title.SubItems.Count; ++i )
                    {
                        if ( RemoveModule( name, title.SubItems[i] ) )
                        {
                            title.SubItems.RemoveAt( i );

                            return title.SubItems.Count == 0;
                        }
                    }
                }

                return false;
            }

            private bool RemoveModulesNotInDictionary( Dictionary<string, string> modules, HelpLayoutItem item )
            {
                if ( item is HelpLayoutModule )
                {
                    if ( !modules.ContainsKey( item.Name ) )
                    {
                        return true;
                    }
                }
                else if ( item is HelpLayoutTitle )
                {
                    HelpLayoutTitle title = item as HelpLayoutTitle;
                    for ( int i = 0; i < title.SubItems.Count; ++i )
                    {
                        if ( RemoveModulesNotInDictionary( modules, title.SubItems[i] ) )
                        {
                            title.SubItems.RemoveAt( i );

                            return title.SubItems.Count == 0;
                        }
                    }
                }

                return false;
            }
            #endregion
        };
        #endregion

        #endregion
    }

    public delegate void OuputMessageEventHandler( object sender, OutputMessageEventArgs e );

    public class OutputMessageEventArgs : EventArgs
    {
        public OutputMessageEventArgs( string msg )
        {
            m_message = msg;
        }

        #region Variables
        private string m_message;
        #endregion

        #region Properties
        public string Message
        {
            get
            {
                return m_message;
            }
        }
        #endregion
    }

    public class BuildResults
    {
        public BuildResults( int numBuildsPerDirectory )
        {
            m_numBuildsPerDirectory = numBuildsPerDirectory;
        }

        #region Variables
        private int m_numBuildsPerDirectory = 0;
        private int m_numBuildsSucceeded = 0;
        private int m_numBuildsSkipped = 0;
        private int m_numBuildsFailed = 0;
        #endregion

        #region Properties
        public int BuildsPerDirectory
        {
            get
            {
                return m_numBuildsPerDirectory;
            }
        }

        public int BuildsAttempted
        {
            get
            {
                return m_numBuildsSucceeded + m_numBuildsSkipped + m_numBuildsFailed;
            }
        }

        public int BuildsSucceeded
        {
            get
            {
                return m_numBuildsSucceeded;
            }
            set
            {
                m_numBuildsSucceeded = value;
            }
        }

        public int BuildsSkipped
        {
            get
            {
                return m_numBuildsSkipped;
            }
            set
            {
                m_numBuildsSkipped = value;
            }
        }

        public int BuildsFailed
        {
            get
            {
                return m_numBuildsFailed;
            }
            set
            {
                m_numBuildsFailed = value;
            }
        }
        #endregion
    };
}
