
call :installFile HelpGenerator\bin\x86\Debug\HelpGenerator.exe ..\..\..\..\top\doc\bin\HelpGenerator.exe
call :installFile HelpGenerator\bin\Debug\HelpGeneratorCore.dll ..\..\..\..\top\doc\bin\HelpGeneratorCore.dll
call :installFile HelpGenerator\bin\x86\Debug\HelpGeneratorUI.exe ..\..\..\..\top\doc\bin\HelpGeneratorUI.exe
call :installFile ..\..\libs\RSG.Base\bin\x86\Debug\RSG.Base.dll ..\..\..\..\top\doc\bin\RSG.Base.dll
call :installFile ..\..\ui\libs\RSG.Base.Forms\bin\x86\Debug\RSG.Base.Forms.dll ..\..\..\..\top\doc\bin\RSG.Base.Forms.dll

pause
goto :eof


:installFile

	p4 edit %2
	xcopy %1 %2 /R /Y /D
	p4 revert -a %2

	GOTO eof

:eof