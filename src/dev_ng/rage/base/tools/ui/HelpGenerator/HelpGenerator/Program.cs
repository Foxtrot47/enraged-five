using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using HelpGeneratorCore;
using RSG.Base;

namespace HelpGenerator
{
    class Program
    {
        static TextWriter sm_writer;

        static int Main( string[] args )
        {
            BuildResults results = null;

            rageCommandLineParser parser = new rageCommandLineParser();

            rageCommandLineItem settingsFileItem = parser.AddItem( new rageCommandLineItem( string.Empty, 1,
                true, "filename", "The settings file that provides all of the information necessary to build, install and/or uninstall the help files." ) );
            rageCommandLineItem sourceDirectoriesItem = parser.AddItem( new rageCommandLineItem( "sourceDir", 1,
                false, "path", "Semi-colon-separated directories that override the ones found in the settings file." ) );
            rageCommandLineItem outputOverviewItem = parser.AddItem( new rageCommandLineItem( "overview", 1,
                false, "true|false", "Overrides the Output Overview Help Files option in the settings file." ) );
            rageCommandLineItem buildItem = parser.AddItem( new rageCommandLineItem( "build", rageCommandLineItem.NumValues.None,
                false, string.Empty, "Builds the help contained in the settings file." ) );
            rageCommandLineItem rebuildItem = parser.AddItem( new rageCommandLineItem( "rebuild", rageCommandLineItem.NumValues.None,
                false, string.Empty, "Rebuilds the help contained in the settings file." ) );
            rageCommandLineItem installItem = parser.AddItem( new rageCommandLineItem( "install", rageCommandLineItem.NumValues.None,
                false, string.Empty, "Installs Help 2 files." ) );
            rageCommandLineItem uninstallItem = parser.AddItem( new rageCommandLineItem( "uninstall", rageCommandLineItem.NumValues.None,
                false, string.Empty, "Uninstalls Help 2 files." ) );
            rageCommandLineItem qaItem = parser.AddItem( new rageCommandLineItem( "qa", rageCommandLineItem.NumValues.None,
                false, string.Empty, "Builds the help to a temporary location and extract the QA Warnings information." ) );
            rageCommandLineItem logFileItem = parser.AddItem( new rageCommandLineItem( "logfile", rageCommandLineItem.NumValues.NoneOrOne,
                false, "[log filename]", "The name of the log file to save.  Default is named helplog.txt in the same directory as the settings file." ) );
            rageCommandLineItem sourceControlItem = parser.AddItem( new rageCommandLineItem( "sourceControl", 1,
                false, "true|false", "Enables or disabled source control." ) );

            try
            {
                StringBuilder cmdline = new StringBuilder();
                cmdline.Append( "Command Line:\r\n\t" );
                foreach ( string arg in args )
                {
                    cmdline.Append( arg );
                    cmdline.Append( ' ' );
                }
                cmdline.Append( "\r\n" );
                generator_OutputMessage( null, new OutputMessageEventArgs( cmdline.ToString() ) );
                
                if ( !parser.Parse( args ) )
                {
                    Console.WriteLine( "Command line parse error: " + parser.Error );
                    return -1;
                }

                string filename = settingsFileItem.Value as string;
                if ( !Path.IsPathRooted( filename ) )
                {
                    filename = Path.GetFullPath( Path.Combine( Directory.GetCurrentDirectory(), filename ) );
                }

                if ( logFileItem.WasSet )
                {
                    string logfile = logFileItem.Value as string;
                    if ( String.IsNullOrEmpty( logfile ) )
                    {
                        logfile = Path.Combine( Path.GetDirectoryName( filename ), "helplog.txt" );
                    }

                    sm_writer = new StreamWriter( logfile, false );
                }

                ProjectSettings settings = new ProjectSettings();

                if ( !File.Exists( filename ) )
                {
                    generator_OutputMessage( null, new OutputMessageEventArgs( "Error: File Not Found: Unable to load '" + filename + "'.\r\n" ) );
                    return -1;
                }

                if ( !settings.LoadFile( filename ) )
                {
                    generator_OutputMessage( null, new OutputMessageEventArgs( "Error: File Not Loaded: Unable to load '" + filename + "'.\r\n" ) );
                    return -1;
                }

                if ( sourceDirectoriesItem.WasSet )
                {
                    settings.SourceDirectories = sourceDirectoriesItem.Value as string;
                }

                if ( outputOverviewItem.WasSet )
                {
                    settings.OutputOverviewHelpFiles = bool.Parse( outputOverviewItem.Value as string );
                }

                if ( sourceControlItem.WasSet )
                {
                    settings.SourceControlEnabled = bool.Parse( sourceControlItem.Value as string );
                }

                Generator generator = new Generator( settings );
                generator.OutputMessage += new OuputMessageEventHandler( generator_OutputMessage );

                if ( buildItem.WasSet )
                {
                    settings.Rebuild = false;

                    if ( settings.CanGenerateHelp )
                    {
                        results = generator.GenerateHelpFiles( null );
                    }
                    else
                    {
                        generator_OutputMessage( null, new OutputMessageEventArgs( "Error: '" + filename + "' does not meet the minimum requirements to build help files.\r\n" ) );
                    }
                }
                else if ( rebuildItem.WasSet )
                {
                    settings.Rebuild = true;

                    if ( settings.CanGenerateHelp )
                    {
                        results = generator.GenerateHelpFiles( null );
                    }
                    else
                    {
                        generator_OutputMessage( null, new OutputMessageEventArgs( "Error: '" + filename + "' does not meet the minimum requirements to rebuild help files.\r\n" ) );
                    }
                }
                else if ( installItem.WasSet )
                {
                    if ( settings.CanInstallHelp )
                    {
                        results = generator.InstallHelpFiles( null );
                    }
                    else
                    {
                        generator_OutputMessage( null, new OutputMessageEventArgs( "Error: '" + filename + "' does not meet the minimum requirements to install help files.\r\n" ) );
                    }
                }
                else if ( uninstallItem.WasSet )
                {
                    if ( settings.CanInstallHelp )
                    {
                        results = generator.UninstallHelpFiles( null );
                    }
                    else
                    {
                        generator_OutputMessage( null, new OutputMessageEventArgs( "Error: '" + filename + "' does not meet the minimum requirements to uninstall help files.\r\n" ) );
                    }
                }                              
                else if ( qaItem.WasSet )
                {
                    if ( settings.CanExtractQAWarnings )
                    {
                        results = generator.ExtractQAWarningsFromHelpFiles( null );
                    }
                    else
                    {
                        generator_OutputMessage( null, new OutputMessageEventArgs( "Error: '" + filename + "' does not meet the minimum requirements to extract QA Warnings help files.\r\n" ) );
                    }
                }
            }
            catch ( Exception e )
            {
                generator_OutputMessage( null, new OutputMessageEventArgs( "Unhandled Exception: " + e.ToString() + "\r\n" ) );
            }
            finally
            {
                if ( sm_writer != null )
                {
                    sm_writer.Close();
                }
            }

            if ( results == null )
            {
                return -1;
            }
            else
            {
                return results.BuildsFailed;
            }
        }

        private static void generator_OutputMessage( object sender, OutputMessageEventArgs e )
        {          
            if ( sm_writer != null )
            {
                sm_writer.Write( e.Message );
            }
            else
            {
                Console.Write( e.Message );
            }
        }
    }
}
