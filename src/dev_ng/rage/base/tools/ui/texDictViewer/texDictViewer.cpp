// 
// /texDictViewer.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "file/asset.h"
#include "file/packfile.h"
#include "grcore/effect.h"
#include "grcore/effect_values.h"
#include "grcore/im.h"
#include "grcore/quads.h"
#include "grcore/setup.h"
#include "grcore/stateblock.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"
#include "input/input.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/pad.h"
#include "paging/dictionary.h"
#include "system/param.h"
#include "system/main.h"

#if __XENON
#include "system/xtl.h"
#elif __PS3
#include "grcore/wrapper_gcm.h"
#endif

#if __WIN32PC
#include <direct.h> //RAGE_QA: INCLUDE_OK
#endif

using namespace rage;

// These should be officially exposed!
namespace rage {
extern grcEffect *g_DefaultEffect;
extern grcEffectVar g_DefaultSampler;
}


bool ListTexture(grcTexture &texture, u32 hash, void * /*data*/)
{
	Displayf("   %s %xx%x %d mip level%s (hash: 0x%x) (%dk)", texture.GetName(), texture.GetWidth(), texture.GetHeight(), 
			texture.GetMipMapCount(), (texture.GetMipMapCount() == 1) ? "" : "s", hash, texture.GetPhysicalSize()>>10);

	return true;
}

const char *pretty(const char *s)
{
	const char *colon = strrchr(s,':');
	return colon? colon + 1 : s;
}

const char *fmtname(int bpp) {
	switch (bpp) {
		case 4: return "DXT1";
		case 8: return "DXT5";
		case 16: return "5551";
		case 32: return "8888";
		default: return "????";
	}
}


// RDR2-specific resources.
#include "rmcore/drawable.h"

class rdrVisualDictionary : public pgBase
{
public:
	enum
	{
		__WATERDECALBSP = 0
	};

	rdrVisualDictionary(datResource &rsc);

	int m_Garbage[8];

	// The texture dictionary for this object, could be NULL.
	datOwner<pgDictionary<grcTexture> > m_Textures;

	DECLARE_PLACE(rdrVisualDictionary);

	static const int RORC_VERSION;
};

const int rdrVisualDictionary::RORC_VERSION = rmcDrawable::RORC_VERSION + grcTexture::RORC_VERSION + 3 + __WATERDECALBSP;

IMPLEMENT_PLACE(rdrVisualDictionary);

rdrVisualDictionary::rdrVisualDictionary(datResource &rsc)
	: m_Textures(rsc)
{
}


class sagFragType : public pgBase
{
public:
	enum
	{
		RORC_VERSION = 129
	};

	sagFragType(datResource &rsc);

	DECLARE_PLACE(sagFragType);

	int m_Trash[122];
	datOwner<pgDictionary<grcTexture> >	m_Textures;

};

IMPLEMENT_PLACE(sagFragType);

sagFragType::sagFragType(datResource &rsc)
: m_Textures(rsc)
{
}


// END RDR2-specific resources.



#if __XENON
void RouteAlphaToColor(grcTexture *tex,int flag) {
	if (!tex)
		return;
	grcTextureObject *t = tex->GetCachedTexturePtr();
	t->Format.SwizzleX = !flag? GPUSWIZZLE_X : GPUSWIZZLE_W;
	t->Format.SwizzleY = !flag? GPUSWIZZLE_Y : GPUSWIZZLE_W;
	t->Format.SwizzleZ = !flag? GPUSWIZZLE_Z : GPUSWIZZLE_W;
}
#elif __PS3
void RouteAlphaToColor(grcTexture *tex,int flag) {
	if (!tex)
		return;
	grcTextureObject *t = tex->GetCachedTexturePtr();
	t->remap = flag
		? CELL_GCM_TEXTURE_REMAP_REMAP << 14 |
		CELL_GCM_TEXTURE_REMAP_REMAP << 12 |
		CELL_GCM_TEXTURE_REMAP_REMAP << 10 |
		CELL_GCM_TEXTURE_REMAP_REMAP << 8 |
		CELL_GCM_TEXTURE_REMAP_FROM_A << 6 |
		CELL_GCM_TEXTURE_REMAP_FROM_A << 4 |
		CELL_GCM_TEXTURE_REMAP_FROM_A << 2 |
		CELL_GCM_TEXTURE_REMAP_FROM_A
		: CELL_GCM_TEXTURE_REMAP_REMAP << 14 |
		CELL_GCM_TEXTURE_REMAP_REMAP << 12 |
		CELL_GCM_TEXTURE_REMAP_REMAP << 10 |
		CELL_GCM_TEXTURE_REMAP_REMAP << 8 |
		CELL_GCM_TEXTURE_REMAP_FROM_B << 6 |
		CELL_GCM_TEXTURE_REMAP_FROM_G << 4 |
		CELL_GCM_TEXTURE_REMAP_FROM_R << 2 |
		CELL_GCM_TEXTURE_REMAP_FROM_A;
}
#else
#define RouteAlphaToColor(tex,flag)
#endif

int Main()
{
	if (sysParam::GetArgCount() < 2)
		Quitf("Requires texture dictionary name as first arg");

	RAGE_TRACK(TexDictViewer);

	const char *dictName = sysParam::GetArg(1);

	char cwd[RAGE_MAX_PATH];
#if __WIN32PC
	_getcwd(cwd, RAGE_MAX_PATH);
#else
	safecpy(cwd, RAGE_ASSET_ROOT);
#endif

	grcSetup SETUP;
	SETUP.Init(cwd, dictName);
	SETUP.BeginGfx(true);
	SETUP.CreateDefaultFactories();

	ioInput::Begin(true);
	ioPad::BeginAll();

	int dictCount = 0;
	const char *ext = strrchr(dictName,'.');
	const int maxDicts = 64;
	const char *dictNames[maxDicts];
	pgDictionary<grcTexture> *dicts[maxDicts];
	fiPackfile *pf = NULL;

	if (ext && !stricmp(ext,".rpf")) {
		fiPackfile *pf = rage_new fiPackfile;
		if (!pf->Init(dictName,true,fiPackfile::CACHE_NONE))
			Quitf("Unable to mount archive '%s'",dictName);
		char mountName[256];
		formatf(mountName,"%s:/",ASSET.FileName(dictName));
		pf->MountAs(mountName);
		fiFindData data;
		fiHandle h = pf->FindFileBegin(mountName,data);
		if (fiIsValidHandle(h)) {
			do {
				const char *ext = strrchr(data.m_Name,'.');
				char temp[256];
				if (ext && ext[2] == 't' && ext[3] == 'd' && dictCount < maxDicts)
					dictNames[dictCount++] = StringDuplicate(formatf(temp,"%s%s",mountName,data.m_Name));
			} while (pf->FindFileNext(h,data));
			pf->FindFileEnd(h);
		}
	}
	else {
		for (int i=0; i<sysParam::GetArgCount()-1 && i<maxDicts; i++)
			dictNames[dictCount++] = sysParam::GetArg(i+1);
	}

	for (int i=0; i<dictCount; i++)
	{
		pgRscBuilder::Load(dicts[i], dictNames[i], "#td",grcTexture::RORC_VERSION);
		Displayf("Contents of %s: ", dictNames[i]);
		if (dicts[i])
		{
			dicts[i]->ForAll(ListTexture, NULL);
		}
		else
		{
			Quitf("Could not load dictionary %s", dictNames[i]);
		}
	}

	if (pf) {
		fiDevice::Unmount(*pf);
		delete pf;
	}

	int current = 0;
	int which = 0;
	int mip = 0;
	int filtered = 1;	// this isn't even hooked up to anything any longer?
	int zoom = 0;
	int allmips = 1;
	int alltexs = 1;
	int texzoom = 0;
	int down = 0, up = 0;	// lame
	int alpha = 0;
	float tiling = 1.0f;
	float a = 0.5f;

	grcSamplerStateHandle samplerStates[12][2];	// mip / filtered
	grcSamplerStateDesc desc;
	for (int i=0; i<12; i++) {
		for (int j=0; j<2; j++) {
			desc.MinLod = desc.MaxLod = float(i);
			desc.Filter = j? grcSSV::FILTER_MIN_MAG_LINEAR_MIP_POINT : grcSSV::FILTER_MIN_MAG_MIP_POINT;
			samplerStates[i][j] = grcStateBlock::CreateSamplerState(desc);
		}
	}

	do {
		SETUP.BeginUpdate();
			ioPad::UpdateAll();
			ioKeyboard::Update();
			int buttons = ioPad::GetPad(0).GetPressedButtons();
			if ((buttons & (ioPad::R1 | ioPad::LRIGHT)) || (KEYBOARD.KeyPressed(KEY_RIGHT))) {
				++current;
				if (current >= dicts[which]->GetCount())
					current = 0;
			}
			if ((buttons & (ioPad::LLEFT)) || (KEYBOARD.KeyPressed(KEY_LEFT))) {
				if (current == 0)
					current = dicts[which]->GetCount();
				--current;
			}
			if ((buttons & ioPad::LUP) || (KEYBOARD.KeyPressed(KEY_UP))) {
				if (alltexs)
					up = 1;
				else if (mip)
					--mip;
			}
			if ((buttons & ioPad::LDOWN) || (KEYBOARD.KeyPressed(KEY_DOWN))) {
				if (alltexs)
					down = 1;
				else if (mip < 12)
					++mip;
			}
			if ((buttons & ioPad::RLEFT) || (KEYBOARD.KeyPressed(KEY_X))) {
				alltexs = !alltexs;
			}
			if ((buttons & ioPad::RUP) || (KEYBOARD.KeyPressed(KEY_Y))) {
				if (alltexs)
					texzoom = (texzoom + 1) & 3;
				else
					zoom = !zoom;
			}
			if ((buttons & ioPad::RRIGHT) || (KEYBOARD.KeyPressed(KEY_B))) {
				tiling += 1.0f;
				if (tiling > 4.0f)
					tiling = 1.0f;
			}
			if (((buttons & ioPad::R1) || (KEYBOARD.KeyPressed(KEY_A))) && dictCount > 1)
				which = (which + 1) % dictCount;
			if (((buttons & ioPad::L1) || (KEYBOARD.KeyPressed(KEY_Z))) && dictCount > 1)
				which = (which + dictCount - 1) % dictCount;
			if ((buttons & ioPad::SELECT) || (KEYBOARD.KeyPressed(KEY_BACK)))
				allmips = !allmips;
			alpha = ((ioPad::GetPad(0).GetButtons() & ioPad::L2)) != 0;
		SETUP.EndUpdate();

		if (current >= dicts[which]->GetCount())
			current = 0;

		SETUP.SetClearColor(Color32(40,40,40,255));
		SETUP.BeginDraw();
			a += 0.025f;
			if (a > 1)
				a = 0.5f;
			PUSH_DEFAULT_SCREEN();

			if (alltexs) {
				int toShow = dicts[which]->GetCount();
				int size = 256;
				int maxrows = 2;
				float vmargin = 44.0f;
				int basex = 64, basey = 150;
				int textOffset = 34;
				bool brief = false;
				int perRow = 4;
				if (toShow > 8 && texzoom < 3) {
					size = 128;
					maxrows = 3;
					perRow = 8;
					if (toShow > 24 && texzoom < 2) {
						size = 64;
						maxrows = 5;
						perRow = 16;
						if (toShow > 80 && texzoom < 1) {
							size = 32;
							maxrows = 13;
							vmargin = 14.0f;
							textOffset = 10;
							brief = true;
							perRow = 30;
							basex = 44;
							basey = 110;
						}
					}
				}
				if (down) {
					if (current + perRow < toShow)
						current += perRow;
					down = 0;
				}
				else if (up) {
					if (current >= perRow)
						current -= perRow;
					up = 0;
				}
				int first = 0;
				while (current >= first + perRow * maxrows)
					first += perRow;
				for (int i=first; i < toShow; i++) {
					grcTexture *ct = dicts[which]->GetEntry(i);
					int col = (i - first) % perRow;
					int row = (i - first) / perRow;
					if (row >= maxrows)
						continue;		// can't see it, no point
					float x1 = basex + col * (size + 8.0f);
					float y1 = basey + row * (size + vmargin);
					if (ct) {
						int width = ct->GetWidth();
						int height = ct->GetHeight();
						while (width > size || height > size) {
							width >>= 1;
							height >>= 1;
						}
						RouteAlphaToColor(ct,alpha);
						g_DefaultEffect->SetSamplerState(g_DefaultSampler, samplerStates[0][filtered]);
						grcBindTexture(ct);
						grcBeginQuads(1);
						grcDrawQuadf(x1,y1,x1+float(width),y1+float(height), 0, 
							0,0,tiling,tiling,Color32(255,255,255,255));
						grcEndQuads();
						grcBindTexture(NULL);
						if (i == current) {
							grcColor3f(a,a,a);
							grcBegin(drawLineStrip,5);
								grcVertex2f(x1-1,y1-1);
								grcVertex2f(x1+width,y1-1);
								grcVertex2f(x1+width,y1+height);
								grcVertex2f(x1-1,y1+height);
								grcVertex2f(x1-1,y1-1);
							grcEnd();
						}
						g_DefaultEffect->SetSamplerState(g_DefaultSampler, grcStateBlock::SS_Default);
						char buffer[256];
						if (brief)
							formatf(buffer,sizeof(buffer),"%dk",ct->GetPhysicalSize() >> 10);
						else
							formatf(buffer,sizeof(buffer),"%-*.*s\n%dx%d\n%dk\n%s",
								size/8,size/8,
								pretty(ct->GetName()),
								ct->GetWidth(),
								ct->GetHeight(),
								ct->GetPhysicalSize() >> 10,
								fmtname(ct->GetBitsPerPixel()));
						if (ct->GetPhysicalSize() >= 1024*1024)
							grcColor3f(0.9f,0.1f,0.1f);
						else if (ct->GetPhysicalSize() >= 512*1024)
							grcColor3f(0.9f,0.9f,0.1f);
						else
							grcColor3f(0.8f,0.8f,0.8f);
						grcDraw2dText(x1,y1 - textOffset,buffer);
						grcColor3f(1,1,1);
					}
				}
				const char *help = 
#if __PS3
					"Dpad    : Select texture\n"
					"L1/R1   : Cycle dictionaries\n"
					"Square  : View one texture\n"
					"Circle  : Cycle tiling factor\n"
					"Triangle: Overview zoom factor\n"
					"L2      : Alpha preview (hold)";
#else
					"Dpad : Select texture\n"
					"LB/RB: Cycle dictionaries\n"
					"X    : View one texture\n"
					"B    : Cycle tiling factor\n"
					"Y    : Overview zoom factor\n"
					"LT   : Alpha preview (hold)";
#endif
				grcDraw2dText(50,50,help);
			} else {
				grcTexture *ct = dicts[which]->GetEntry(current);
				if (ct) {
					int mipct = ct->GetMipMapCount();
					int width = ct->GetWidth(), height = ct->GetHeight();
					int awidth = width;
					int dwidth = GRCDEVICE.GetWidth(), dheight = GRCDEVICE.GetHeight();

					for (int i=1; i<mipct; i++)
						awidth += 8 + (width>>i);

					// Pick a reasonable display size
					if (zoom) {
						while (awidth < dwidth || height < dheight) {
							awidth<<=1;
							width<<=1;
							height<<=1;
						}
						while (awidth > dwidth || height > dheight) {
							awidth>>=1;
							width>>=1;
							height>>=1;
						}
					}

					float x1 = (float)((dwidth - awidth)>>1), y1 = (float)((dheight-height)>>1);
					if (allmips) {
						for (int i=0; i<mipct; i++) {
							g_DefaultEffect->SetSamplerState(g_DefaultSampler, samplerStates[i][filtered]);
							RouteAlphaToColor(ct,alpha);
							grcBindTexture(ct);
							grcBeginQuads(1);
							grcDrawQuadf(x1,y1,x1+float(width),y1+float(height), 0, 
								0,0,tiling,tiling,Color32(255,255,255,255));
							grcEndQuads();
							x1 += float(width) + 8.0f;
							width >>= 1;
							height >>= 1;
						}
					}
					else {
						g_DefaultEffect->SetSamplerState(g_DefaultSampler, samplerStates[mip][filtered]);
						RouteAlphaToColor(ct,alpha);
						grcBindTexture(ct);
						grcBeginQuads(1);
						grcDrawQuadf(x1,y1,x1+float(width),y1+float(height), 0, 
							0,0,tiling,tiling,Color32(255,255,255,255));
						grcEndQuads();
					}
					g_DefaultEffect->SetSamplerState(g_DefaultSampler, grcStateBlock::SS_Default);
				}
				const char *help =
#if __PS3
					"Dpad U/D: Change mip\n"
					"L1/R1   : Cycle dictionaries\n"
					"Square  : Switch to overview mode\n"
					"Circle  : Cycle tiling factor\n"
					"Triangle: Zoom / Actual size\n"
					"Select  : Toggle single / all mips\n"
					"L2      : Alpha Preview (hold)";
#else
					"Dpad U/D: Change mip\n"
					"LB/RB   : Cycle dictionaries\n"
					"X       : Switch to overview mode\n"
					"B       : Cycle tiling factor\n"
					"Y       : Zoom / Actual size\n"
					"Back    : Toggle single / all mips\n"
					"LT      : Alpha Preview (hold)";
#endif
				grcDraw2dText(50,50,help);
			}
			// g_DefaultEffect->SetSamplerState(g_DefaultSampler, oldSamplerState);
			char buffer[256];
			if (dicts[which]->GetCount() == 0) {
				formatf(buffer,sizeof(buffer),"%s\nempty texture dictionary!",dictNames[which]);
			}
			else {
				grcTexture *ct = dicts[which]->GetEntry(current);
				formatf(buffer,sizeof(buffer),"%s\n%3d/%3d : mip %d : %4dx%4d '%s' (%d) (%dk %s)",
						dictNames[which],
						current+1,dicts[which]->GetCount(),
						mip,
						ct->GetWidth(),
						ct->GetHeight(),
						pretty(ct->GetName()),
						ct->GetRefCount(),
						ct->GetPhysicalSize() >> 10,
						fmtname(ct->GetBitsPerPixel())
						);
			}
			grcDraw2dText(400,50,buffer);
			POP_DEFAULT_SCREEN();
		grcBindTexture(NULL);
		SETUP.EndDraw();
	} while (!SETUP.WantExit());

	ioPad::EndAll();
	ioInput::End();
	SETUP.DestroyFactories();
	SETUP.EndGfx();

	return 0;
}

