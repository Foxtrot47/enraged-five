#ifndef MEM_GAME_CONNECTION_H
#define MEM_GAME_CONNECTION_H

#include <vector>
#include <hash_map>
#include "atl/queue.h"
#include "diag/tracker_remote2.h"
#include "file/handle.h"
#include "memstack.h"
#include "memsymfile.h"

class memHeap;

class memGameConnection
{
	friend class CMainFrame;

public:
	memGameConnection(HWND mainFrame, fiHandle handle);
	~memGameConnection();

	void MarkAllViewsDirty();
	
	int SaveAllToTxt(const char* directory, bool quiet = false);

	void SaveAllToMvz(const char * file, bool quiet = false);

	void SaveAllToCSV();
	int SaveAllToCSV(const char* directory, bool quiet = false);

	void ShowNewAllocationsOnly();

	void ShowAllAllocations();

	void AddressSearch();

	void ReadFromMvz(const char *filename);

	void Filter();

	void CreateLogFile();

	void CreateLogFile(const char *filename);


private:
	FILE *OpenWritable(const char *name);

	static DWORD WINAPI StartConnectionThread(LPVOID);

	void		ConnectionThread();

	void		BroadcastLostConnection();
	bool		Read(void* buf, int size);
	const char* TagName(const char* name);

	// Given a PS4 game IP address, get the PFS path. On failure, returns false and sets errorOrPath
	// to an error message. On success, returns true and sets errorOrPath to the path, e.g.
	// "O:\\10.11.25.227\\".
	static bool GetPFSPathFromGameIP(const char* gameIp, std::string& errorOrPath);

	HANDLE							m_connectionThread;
	HANDLE							m_processingThread;

	fiHandle						m_Handle;
	HWND							m_hWndClient;
	std::vector<memHeap*>			m_Heaps;
	stdext::hash_map<u32, const char*> m_Tags;
	int								m_Frame;
	memSymFile						m_Symbols;
	diagTrackerRemote2::ClientInfo	m_ClientInfo;
	diagTrackerRemote2::VersionInfo m_VersionInfo;
};

#endif // MEM_GAME_CONNECTION_H
