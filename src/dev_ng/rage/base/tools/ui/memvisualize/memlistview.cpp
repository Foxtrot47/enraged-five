#include "stdafx.h"
#include "memlistview.h"

#if ENABLE_ALLOCATION_LIST_VIEW

#include "MemTags.h"

#define COL_Id (0)
#define COL_Frame (1)
#define COL_Bytes (2)
#define COL_Address (3)
#define COL_Bucket (4)
#define COL_Callstack (5)

int memListView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	SetMsgHandled(FALSE);
	InsertColumn(COL_Id, "Id", LVCFMT_RIGHT, 50);
	InsertColumn(COL_Frame, "Frame", LVCFMT_RIGHT, 50);
	InsertColumn(COL_Bytes, "Bytes", LVCFMT_RIGHT, 70);
	InsertColumn(COL_Address, "Address", LVCFMT_RIGHT, 100);
	InsertColumn(COL_Bucket, "Bucket", LVCFMT_LEFT, 100);
	InsertColumn(COL_Callstack, "Callstack", LVCFMT_LEFT, 500);
	SetColumnWidth(COL_Callstack, LVSCW_AUTOSIZE);
	SetExtendedListViewStyle(LVS_EX_FULLROWSELECT|LVS_EX_DOUBLEBUFFER|LVS_EX_GRIDLINES);
	return 0;
}

void memListView::OnAlloc(memBlock*)
{
	m_Dirty = true;
}

void memListView::OnFree(memBlock*)
{
}

void memListView::OnUpdate()
{
	if (m_Dirty && m_hWnd)
	{
		m_Dirty = false;
		SetRedraw(FALSE);
		int count = GetItemCount();
		int scrollpos = GetTopIndex() + GetCountPerPage();
		SetItemCountEx(m_Heap->m_Allocations.size(), LVSICF_NOSCROLL);
		HWND focus = GetFocus();
		if (focus != m_hWnd && scrollpos >= count)
			EnsureVisible(m_Heap->m_Allocations.size()-1, FALSE);
		SetRedraw(TRUE);
	}
}

BOOL memListView::OnEraseBkgnd(CDCHandle dc)
{
	return TRUE;
}

// LRESULT memListView::OnPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCustomDraw)
// {
// 	return CDRF_NOTIFYITEMDRAW;
// }
// 
// LRESULT memListView::OnItemPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCustomDraw)
// {	
// 	LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)lpNMCustomDraw;
// 	int id = lplvcd->nmcd.dwItemSpec;
// 	m_Heap->m_Cs.Lock();
// 	if (id < m_Heap->m_Allocations.size())
// 	{
// 		memStackEntry* stack = m_Heap->m_Allocations[id].m_Stack;
// 		if (stack->m_Colour != RGB(128,128,128))
// 			lplvcd->clrTextBk = stack->m_Colour;
// 	}
// 	m_Heap->m_Cs.Unlock();
// 	return CDRF_NEWFONT;
// }

LRESULT memListView::GetDispInfo(int /*idCtrl*/, LPNMHDR pnmh, BOOL& bHandled)
{
	LV_DISPINFO* dispinfo = (LV_DISPINFO*)pnmh;
	int id = dispinfo->item.iItem;
	int col = dispinfo->item.iSubItem;
	static char text[2048];
	dispinfo->item.pszText = text;
	dispinfo->item.cchTextMax = 2048;
	text[0] = 0;
	
	switch(col)
	{
		case COL_Id:
		{
			sprintf_s(text, "%i", id);
		}
		break;

	case COL_Frame:
		{
			m_Heap->m_Cs.Lock();
			int frame = m_Heap->m_Allocations[id].m_Id;
			m_Heap->m_Cs.Unlock();
			sprintf_s(text, "%i", frame);
		}
		break;

	case COL_Bytes:
		{
			m_Heap->m_Cs.Lock();
			int size = m_Heap->m_Allocations[id].m_End - m_Heap->m_Allocations[id].m_Start;
			m_Heap->m_Cs.Unlock();
			sprintf_s(text, "%i", size);
		}
		break;

	case COL_Bucket:
		{
			m_Heap->m_Cs.Lock();
			int bucketId = m_Heap->m_Allocations[id].m_Bucket;
			m_Heap->m_Cs.Unlock();
			
			const char *bucketName = memTags::GetbucketName(bucketId);
			
			if( bucketName )
				sprintf_s(text, "%s", bucketName);
			else
				sprintf_s(text, "%i", bucketId);
		}
		break;

	case COL_Address:
		{
			m_Heap->m_Cs.Lock();
			int addr = m_Heap->m_HeapStart + m_Heap->m_Allocations[id].m_Start;
			m_Heap->m_Cs.Unlock();

			sprintf_s(text, "0x%8x", (unsigned int)addr);
		}
		break;
	
	case COL_Callstack:
	default:
		{
			m_Heap->m_Cs.Lock();
			memStackEntry* stack = m_Heap->m_Allocations[id].m_Stack;
			m_Heap->m_Cs.Unlock();
			int length = 0;
			while (stack)
			{
				length += strlen(stack->m_Name) + 3;
				if (length >= 2048)
					break;

				strcat_s(text, stack->m_Name);
				stack = stack->m_Parent;
				if (stack)
					strcat_s(text, " | ");
			}
		}
		break;
	}
	
	return TRUE;
}

LRESULT memListView::OnItemChanged(int /*idCtrl*/, LPNMHDR pnmh, BOOL& bHandled)
{
	LPNMLISTVIEW pnmv = (LPNMLISTVIEW)pnmh; 
	if ((pnmv->uChanged & LVIF_STATE) && (pnmv->uNewState & LVIS_SELECTED))
	{
		memBlock* pSelect = 0;
		{
			SYS_CS_SYNC(m_Heap->m_Cs);		
			memBlock& block = m_Heap->m_Allocations[pnmv->iItem];
			std::set<memBlock>::iterator i = m_Heap->m_ActiveBlocks.find(block);
			if (i != m_Heap->m_ActiveBlocks.end() && i->m_Id == pnmv->iItem)
			{
				pSelect = &*i;
			}	
		}
		if (pSelect)
			m_Heap->Select(pSelect);
	}
	return TRUE;	
}

#endif // ENABLE_ALLOCATION_LIST_VIEW