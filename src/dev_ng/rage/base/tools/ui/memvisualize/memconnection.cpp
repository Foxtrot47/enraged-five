#include "stdafx.h"
#include "memconnection.h"
#include "AddressSearch.h"
#include "FilterDlg.h"
#include "memsymfile.h"
#include "diag/tracker_remote2.h"
#include "file/asset.h"
#include "file/tcpip.h"
#include "math/amath.h"
#include "system/timer.h"
#include "memheap.h"
#include "memwnd.h"
#include "mainfrm.h"
#include "string/string.h"
#include "string/stringhash.h"
#include "system/magicnumber.h"
#include "system/platform.h"
#include <atlsafe.h>
#import "progid:ortmapi.ORTMAPI"

extern memGameConnection* g_memGameConnection;
const u32 MVZ_MAGIC = MAKE_MAGIC_NUMBER('M', 'V', 'Z', 'L');
const u32 MVZ_VERSION = 7;

DWORD WINAPI memGameConnection::StartConnectionThread(LPVOID lpThreadParameter)
{
	CoInitialize(NULL);
	static_cast<memGameConnection*>(lpThreadParameter)->ConnectionThread();
	static_cast<memGameConnection*>(lpThreadParameter)->BroadcastLostConnection();
	return 0;
}

memGameConnection::memGameConnection(HWND mainFrame, fiHandle handle)
:	m_Handle(handle)
,	m_hWndClient(mainFrame)
,	m_Symbols(mainFrame)
{
	if (handle != fiHandleInvalid)
	{
		m_connectionThread = CreateThread(0, 0, StartConnectionThread, this, 0, 0);
		::SetThreadPriority(m_connectionThread, THREAD_PRIORITY_NORMAL);
	}
}

memGameConnection::~memGameConnection()
{
	TerminateThread( m_connectionThread, 0 );
	TerminateThread( m_processingThread, 0 );
	m_Symbols.Unload();
	for (std::vector< memHeap* >::iterator it = m_Heaps.begin(); it < m_Heaps.end(); ++it)
		delete *it;
}

bool memGameConnection::Read(void* buf, int size)
{
	while (size)
	{
		int read = fiDeviceTcpIp::GetInstance().Read(m_Handle, buf, size);
		if (read == -1)
		{
			Errorf("Connection terminated.");
			return false;
		}

		if (!read)
			Sleep(10);
		size -= read;
		buf = (u8*)buf + read;
	}

	return true;
}

enum memPacketType
{
	MV_INIT = 1,
	MV_NEWHEAP,
	MV_ALLOC,
	MV_FREE,
	MV_PUSH,
	MV_POP,
	MV_FRAME,
	MV_REQUESTDUMP,
	MV_ADD_FLAGS,
	MV_CLEAR_FLAGS,
	MV_FULL_REPORT,
	MV_VERSION_INFO,
};

struct memThreadInfo
{
	u32 threadId;
	tagStackEntry* stack;
	tagStackEntry tagStack[64];
};

const char* memGameConnection::TagName(const char* name)
{
	u32 hash = atStringHash(name);
	const char*& tag = m_Tags[hash];
	if (!tag)
		tag = StringDuplicate(name);
	return tag;
}

void memGameConnection::MarkAllViewsDirty()
{
	for(u32 i=0; i<m_Heaps.size(); ++i)
		m_Heaps[i]->MarkAllViewsDirty();
}

FILE *memGameConnection::OpenWritable(const char *name)
{
	while (true)
	{
		FILE *result = fopen(name, "w");

		if (result)
		{
			return result;
		}

		char errorMsg[256];
		snprintf(errorMsg, sizeof(errorMsg), "%s could not be opened - it's either read-only, or in use, or the path doesn't exist. "
			"Click YES to make it writable, NO to try again, or "
			"CANCEL to abort.",
			name);

		int choice = MessageBox(m_hWndClient, errorMsg, "Can't write file", MB_ICONERROR|MB_YESNOCANCEL);

		if (choice == IDCANCEL)
		{
			return NULL;
		}

		if (choice == IDYES)
		{
			// And for good measure, just make it writable.
			SetFileAttributes(name, GetFileAttributes(name) & ~FILE_ATTRIBUTE_READONLY);
		}
	}
}

void memGameConnection::ReadFromMvz(const char *filename)
{
	FILE *fp = fopen(filename, "rb");

	if (!fp)
	{
		MessageBox(m_hWndClient, CString("Cannot open file ") + filename, "Error opening file", MB_ICONHAND | MB_OK);
		return;
	}

	u32 inValue;

	fread(&inValue, sizeof(inValue), 1, fp);
	if (inValue != MVZ_MAGIC)
	{
		MessageBox(m_hWndClient, "This does not seem to be a MemVisualize file.", "Bad file", MB_ICONHAND | MB_OK);
		fclose(fp);
		return;
	}

	int version;
	fread(&version, sizeof(version), 1, fp);
	if (version != MVZ_VERSION)
	{
		MessageBox(m_hWndClient, "Unknown version - you may need to update your MemVisualize client.", "Unknown version", MB_ICONHAND | MB_OK);
		fclose(fp);
		return;
	}

	u8 pointerSizeNative, pointerSizeClient;
	fread(&pointerSizeNative, sizeof(pointerSizeNative), 1, fp);
	fread(&pointerSizeClient, sizeof(pointerSizeClient), 1, fp);
	if (pointerSizeClient != sizeof(void *))
	{
		MessageBox(m_hWndClient, "This file was created with an incompatible version - MemVisualize should always use 64bit, but this file doesn't match this MemVisualize client.", "Incompatible version", MB_ICONHAND | MB_OK);
		fclose(fp);
		return;
	}

	ReadString(m_ClientInfo.m_Platform, sizeof(m_ClientInfo.m_Platform), fp);
	ReadString(m_ClientInfo.m_Configuration, sizeof(m_ClientInfo.m_Configuration), fp);

	// Version 3: Version info.
	if (version > 2)
	{
		ReadString(m_VersionInfo.m_BuildVersion, sizeof(m_VersionInfo.m_BuildVersion), fp);
		ReadString(m_VersionInfo.m_PackageType, sizeof(m_VersionInfo.m_PackageType), fp);
	}

	u8 heapCount;
	fread(&heapCount, sizeof(heapCount), 1, fp);

	for (u8 x=0; x<heapCount; x++)
	{
		atArray<memStackEntry *, 0, u32> stackReferenceArray;
		char heapName[256];
		ReadString(heapName, sizeof(heapName), fp);
		memHeap *heap = rage_new memHeap(heapName, NULL);
		heap->m_ClientInfo = &m_ClientInfo;
		heap->m_VersionInfo = &m_VersionInfo;
		bool result = heap->ReadFromMvz(fp, version, stackReferenceArray);

		if (!result)
		{
			MessageBox(m_hWndClient, "Error reading file - data is corrupted.", "File corrupted", MB_ICONHAND | MB_OK);
			break;
		}
		memWnd *wnd = g_MainFrame->CreateHeapWindow(heap);

		wnd->ReadFromMvz(fp, version, stackReferenceArray);

		m_Heaps.push_back(heap);
	}

	fclose(fp);
}

void memGameConnection::SaveAllToMvz(const char *file, bool quiet)
{
	FILE *out = fopen(file, "wb");

	if (!out)
	{
		if (!quiet)
		{
			MessageBox(m_hWndClient, (const char *) (CString("Unable to open MVZ file for writing: ") + file), "Error writing file", MB_ICONHAND | MB_OK);
		}
	}
	else
	{
		fwrite(&MVZ_MAGIC, sizeof(MVZ_MAGIC), 1, out);
		fwrite(&MVZ_VERSION, sizeof(MVZ_VERSION), 1, out);

		u8 pointerSizeNative = (u8) m_ClientInfo.m_PointerSize;
		u8 pointerSizeClient = (u8) sizeof(void *);
		fwrite(&pointerSizeNative, sizeof(pointerSizeNative), 1, out);
		fwrite(&pointerSizeClient, sizeof(pointerSizeClient), 1, out);

		WriteString(m_ClientInfo.m_Platform, out);
		WriteString(m_ClientInfo.m_Configuration, out);

		// Version 3: Version info.
		WriteString(m_VersionInfo.m_BuildVersion, out);
		WriteString(m_VersionInfo.m_PackageType, out);

		//m_Symbols->SaveToMvz(out);

		u8 heapCount = (u8) m_Heaps.size();
		fwrite(&heapCount, sizeof(heapCount), 1, out);

		for(u32 i=0; i<m_Heaps.size(); ++i)
		{
			WriteString(m_Heaps[i]->m_Root.m_Name, out);
			stdext::hash_map<memStackEntry *, int> stackReferenceMap;
			m_Heaps[i]->SaveAsMvz(out, stackReferenceMap);
			m_Heaps[i]->m_Wnd->SaveAsMvz(out, stackReferenceMap);
		}

		fclose(out);
	}
}

int memGameConnection::SaveAllToTxt(const char* directory, bool quiet)
{
	for(u32 i=0; i<m_Heaps.size(); ++i)
	{
		memStackEntry* root = &m_Heaps[i]->m_Root;
		char filename[256];
		sprintf(filename,"%s\\%s.txt",directory,root->m_Name);

		FILE* out = OpenWritable(filename);

		if (out)
		{
			m_Heaps[i]->SaveAsTxt(out);
			fclose(out);
		}
		else
		{
			char errorMsg[256];
			sprintf_s(errorMsg, sizeof(errorMsg), "Unable to write file %s", filename);
			Errorf(errorMsg);

			if (!quiet)
			{
				MessageBox(m_hWndClient, errorMsg, "Error writing file", MB_ICONHAND | MB_OK);
			}
			return 0;
		}
	}

	return (int) m_Heaps.size();
}

void memGameConnection::SaveAllToCSV()
{
	static int saveCount = 0;
	
	for(u32 i=0; i<m_Heaps.size(); ++i)
	{
		memStackEntry* root = &m_Heaps[i]->m_Root;
		char filename[256];
		sprintf(filename,"%s.%d.csv",root->m_Name,saveCount);
		FILE* out = OpenWritable(filename);
		if (out)
		{
			m_Heaps[i]->SaveAsCsv(out);
		}
		fclose(out);
	}
	
	saveCount++;
}

int memGameConnection::SaveAllToCSV(const char* directory, bool quiet)
{
	for(u32 i=0; i<m_Heaps.size(); ++i)
	{
		memStackEntry* root = &m_Heaps[i]->m_Root;
		char filename[256];
		sprintf(filename,"%s\\%s.csv",directory,root->m_Name);

		FILE* out = OpenWritable(filename);

		if (out)
		{
			m_Heaps[i]->SaveAsCsv(out);
			fclose(out);
		}
		else
		{
			char errorMsg[256];
			sprintf_s(errorMsg, sizeof(errorMsg), "Unable to write file %s", filename);
			Errorf(errorMsg);

			if (!quiet)
			{
				MessageBox(m_hWndClient, errorMsg, "Error writing file", MB_ICONHAND | MB_OK);
			}
			return 0;
		}
	}

	return (int) m_Heaps.size();
}

void memGameConnection::ShowNewAllocationsOnly()
{
	for(u32 i=0; i<m_Heaps.size(); ++i)
		m_Heaps[i]->ShowNewAllocationsOnly();
}

void memGameConnection::ShowAllAllocations()
{
	for(u32 i=0; i<m_Heaps.size(); ++i)
		m_Heaps[i]->ShowAllAllocations();
}

void memGameConnection::AddressSearch()
{
	CAddressSearch addressSearch;

	if (addressSearch.DoModal() == IDOK)
	{
		size_type address = addressSearch.m_Address;

		for(u32 i=0; i<m_Heaps.size(); ++i)
			m_Heaps[i]->AddressSearch(address);
	}
}

void memGameConnection::Filter()
{
	CFilterDlg filterDlg(&m_Heaps);

	if (filterDlg.DoModal() == IDOK)
	{
/*		size_type address = addressSearch.m_Address;

		for(u32 i=0; i<m_Heaps.size(); ++i)
			m_Heaps[i]->AddressSearch(address);*/
	}
}

void memGameConnection::CreateLogFile()
{
	CFileDialog fd(false, "log", NULL, OFN_HIDEREADONLY, "*.log (Log file)\0*.log\0*.* (All Files)\0*.*\0");
	if (fd.DoModal() == IDOK)
	{
		CreateLogFile(fd.m_szFileName);
	}
}

void memGameConnection::CreateLogFile(const char *filename)
{
	FILE *out = fopen(filename, "wt");

	if (out)
	{



		fclose(out);
	}
}

void memGameConnection::ConnectionThread()
{
	m_Frame = 0;
	sysTimer t;
	u32 curthread = ~0u;
	u32 curthreadid = 0;
	tagStackEntry* stack = 0;
	tagStackEntry* stackBase = 0;
	std::vector<memThreadInfo*> threads;
	u32 lastCommand = 0;	

	while (1)
	{
		if (t.GetMsTime() > 1000 || !fiDeviceTcpIp::GetReadCount(m_Handle))
		{
			for(u32 i=0; i<m_Heaps.size(); ++i)
				m_Heaps[i]->Update();
			::Sleep(10);
			t.Reset();
		}

		u32 threadAndType;
		if (!Read(&threadAndType, sizeof(u32)))
		{
			Errorf("Connection lost");
			// Terminate thread - we lost connection to the client.
			fiDeviceTcpIp::GetInstance().Close(m_Handle);
			return;
		}

		u32 thread = threadAndType>>8;
		u32 type = threadAndType&255;
		if (thread != curthread)
		{
			if (curthreadid < threads.size())
				threads[curthreadid]->stack = stack;
			curthread = thread;
			for(curthreadid=0; curthreadid<threads.size(); ++curthreadid)
				if (threads[curthreadid]->threadId == thread)
					break;
			if (curthreadid == threads.size())
			{
				memThreadInfo* info = new memThreadInfo;
				info->threadId = thread;
				info->stack = &info->tagStack[0];
				threads.push_back(info);
			}
			stack = threads[curthreadid]->stack;
			stackBase = threads[curthreadid]->tagStack;
		}
		switch (type)
		{
		case MV_INIT:
		{
			// Clear History
			for(u32 i = 0; i < m_Heaps.size(); ++i)
			{
				if (m_Heaps[i])
					m_Heaps[i]->ClearHistory();
			}

			bool readSuccessful = Read(&m_ClientInfo, sizeof(m_ClientInfo));

			if (m_ClientInfo.m_ProtocolVersion != diagTrackerRemote2::PROTOCOL_VERSION && m_ClientInfo.m_ProtocolVersion != 7)	// Backwards compatible with v7, because we rule.
			{
				char errorMsg[1024];
				sprintf_s(errorMsg, sizeof(errorMsg), "Protocol version mismatch! Your memvisualize.exe is too old or too new for the executable - "
					"it's expecting version %d, but the game has returned version %d.", diagTrackerRemote2::PROTOCOL_VERSION, m_ClientInfo.m_ProtocolVersion);

				Errorf(errorMsg);
				MessageBox(m_hWndClient, errorMsg, "Protocol version mismatch", MB_ICONHAND | MB_OK);
				fiDeviceTcpIp::GetInstance().Close(m_Handle);
				// Kill this thread.
				return;
			}

			if (!readSuccessful)
			{
				Errorf("Error reading client info packet");
				MessageBox(m_hWndClient, "There was a communication error during the handshake. The protocol version seems to be correct, but something still went wrong.", "Communication error", MB_ICONHAND | MB_OK);
				fiDeviceTcpIp::GetInstance().Close(m_Handle);
				// Kill this thread.
				return;
			}

			if (m_ClientInfo.m_PointerSize != 4 && m_ClientInfo.m_PointerSize != 8)
			{
				char errorMsg[1024];
				sprintf_s(errorMsg, sizeof(errorMsg), "The client has an unsupported pointer size (%d).", m_ClientInfo.m_PointerSize);

				Errorf(errorMsg);
				MessageBox(m_hWndClient, errorMsg, "Unsupported pointer size", MB_ICONHAND | MB_OK);
				fiDeviceTcpIp::GetInstance().Close(m_Handle);
				// Kill this thread.
				return;
			}

			Displayf("Beginning new session, pointer size %d, %s %s", m_ClientInfo.m_PointerSize, m_ClientInfo.m_Platform, m_ClientInfo.m_Configuration);

			char* name = m_ClientInfo.m_SymbolFile;
			if (strncmp(name, "/app_home/", 10) == 0)
			{
				name += 10;
			}
			else
			{
				if (m_ClientInfo.m_PlatformIdentifier == platform::XENON)
				{
					char tmp[1024];
					char cmd[1024];
					sprintf_s(tmp, "%%XEDK%%\\bin\\win32\\xbcp /Y x%s \"%%TEMP%%\\memvis.cmp\"", m_ClientInfo.m_SymbolFile);
					ExpandEnvironmentStrings(tmp, cmd, 1024);
					STARTUPINFO si = {sizeof(STARTUPINFO)};
					PROCESS_INFORMATION pi = {0};
					if (CreateProcess(0, cmd, 0, 0, FALSE, CREATE_NO_WINDOW, 0, 0, &si, &pi))
					{
						WaitForSingleObject(pi.hProcess, INFINITE);
						CloseHandle(pi.hThread);
						CloseHandle(pi.hProcess);
						ExpandEnvironmentStrings("%TEMP%\\memvis.cmp", m_ClientInfo.m_SymbolFile, 256);
					}
				}
				else if (m_ClientInfo.m_PlatformIdentifier == platform::DURANGO)
				{
					char tmp[1024] = {0};
					strcpy(tmp, m_ClientInfo.m_SymbolFile);
					ExpandEnvironmentStrings(tmp, m_ClientInfo.m_SymbolFile, sizeof(m_ClientInfo.m_SymbolFile));
				}
				else if (m_ClientInfo.m_PlatformIdentifier == platform::ORBIS && strncmp(name, "/app0/", 6) == 0)
				{
					// Get remote IP - NB: This will be the "Game LAN" IP, not the "Host IP", which we need
					char remoteAddr[64];
					fiDeviceTcpIp::GetInstance().GetRemoteName(m_Handle, remoteAddr, sizeof(remoteAddr));

					// Look up the PFS directory for this game IP
					std::string hostPath;
					if(!GetPFSPathFromGameIP(remoteAddr, hostPath))
					{
						Errorf("%s", hostPath.c_str());
						MessageBox(m_hWndClient, hostPath.c_str(), "Error accessing symbol file", MB_ICONHAND | MB_OK);
						fiDeviceTcpIp::GetInstance().Close(m_Handle);
						return;
					}

					// Patch symbol filename to PFS directory
					static char symBuff[128];
					formatf(symBuff, "%sdata\\app\\%s", hostPath.c_str(), name+6);
					name = symBuff;
				}
			}
			m_Symbols.Load(name, m_ClientInfo.m_PointerSize == 8, m_ClientInfo.m_PlatformIdentifier, m_ClientInfo.m_MainSize);
			break;
		}
		case MV_NEWHEAP:
		{
			size_type addr = 0, size = 0;
			char name[33];

			Read(&addr, m_ClientInfo.m_PointerSize);
			Read(&size, m_ClientInfo.m_PointerSize);
			Read(name, 32);
			name[32] = 0;			
			memHeap* heap = new memHeap(TagName(name), &m_Symbols);
			heap->m_HeapStart = addr;
			heap->m_HeapSize = size;
			heap->m_ClientInfo = &m_ClientInfo;
			heap->m_VersionInfo = &m_VersionInfo;
			g_MainFrame->CreateHeapWindow(heap);
			m_Heaps.push_back(heap);
			break;
		}
		case MV_ALLOC:
		{
			size_type addr = 0, size = 0;
			u32 bucketAndFlags;
			size_type callstackTemp[128];
			u32 dynamicSize;
			u32 pad;
			Read(&dynamicSize, sizeof(u32)); // DYNAMIC
			Read(&addr, m_ClientInfo.m_PointerSize);			
			Read(&size, m_ClientInfo.m_PointerSize);
			Read(&bucketAndFlags, sizeof(u32));
			Read(&pad, sizeof(u32));
			dynamicSize -= 2 + (2 * m_ClientInfo.m_PointerSize / 4);
			u32 stackEntries = dynamicSize * 4 / m_ClientInfo.m_PointerSize;
			Read(&callstackTemp, stackEntries * m_ClientInfo.m_PointerSize);

			Assert(stackEntries <= 128);

			// If this is a 32-bit system, we need to convert the callstack.
			size_type callstackBacking[128];
			size_type *callstack = callstackTemp;

			if (m_ClientInfo.m_PointerSize != sizeof(size_type))
			{
				u32 *callstack32 = (u32 *) callstackTemp;

				for (u32 x=0; x<stackEntries; x++)
				{
					callstackBacking[x] = callstack32[x];
				}

				callstack = callstackBacking;
			}

			bool success = false;
			for(u32 i=0; i<m_Heaps.size(); ++i)
				success |= m_Heaps[i]->Alloc(addr, size, bucketAndFlags, callstack, stackEntries, stackBase, (u32) (stack - stackBase));

			if (success == false)
			{
				//				Displayf("Cannot identify addr %x, size %x (%x)", addr, size, addr + size);
			}
			break;
		}
		case MV_FREE:
		{			
			size_type addr = 0;
			u32 dynamicSize;
			size_type callstackTemp[128];

			if (m_ClientInfo.m_ProtocolVersion > 7)
			{
				Read(&dynamicSize, sizeof(u32)); // DYNAMIC
			}
			Read(&addr, m_ClientInfo.m_PointerSize);

			// If this is a 32-bit system, we need to convert the callstack.
			size_type callstackBacking[128];
			size_type *callstack = callstackTemp;
			u32 stackEntries = 0;

			if (m_ClientInfo.m_ProtocolVersion > 7)
			{
				dynamicSize -= m_ClientInfo.m_PointerSize / 4;
				stackEntries = dynamicSize * 4 / m_ClientInfo.m_PointerSize;
				Read(&callstackTemp, stackEntries * m_ClientInfo.m_PointerSize);

				Assert(stackEntries <= 128);

				if (m_ClientInfo.m_PointerSize != sizeof(size_type))
				{
					u32 *callstack32 = (u32 *) callstackTemp;

					for (u32 x=0; x<stackEntries; x++)
					{
						callstackBacking[x] = callstack32[x];
					}

					callstack = callstackBacking;
				}
			}

			for(u32 i=0; i<m_Heaps.size(); ++i)
				m_Heaps[i]->Free(addr, callstack, stackEntries, stackBase, (u32) (stack - stackBase));
			break;

		}
		case MV_ADD_FLAGS:
		{
			size_type addr = 0, flags = 0;
			Read(&addr, m_ClientInfo.m_PointerSize);
			Read(&flags, m_ClientInfo.m_PointerSize);

			for(u32 i=0; i<m_Heaps.size(); ++i)
				m_Heaps[i]->SetFlags(addr, (u32) flags);
			break;
		}
		case MV_CLEAR_FLAGS:
		{
			size_type addr = 0, flags = 0;
			Read(&addr, m_ClientInfo.m_PointerSize);
			Read(&flags, m_ClientInfo.m_PointerSize);

			for(u32 i=0; i<m_Heaps.size(); ++i)
				m_Heaps[i]->ClearFlags(addr, (u32) flags);
			break;
		}
		case MV_FULL_REPORT:
		{
			char targetFile[RAGE_MAX_PATH];
			Read(targetFile, sizeof(targetFile));

			char targetPath[RAGE_MAX_PATH];
			ASSET.CreateLeadingPath(targetFile);
			ASSET.RemoveNameFromPath(targetPath, sizeof(targetPath), targetFile);

			SaveAllToTxt(targetPath, true);
			SaveAllToMvz((const char *) (CString(targetPath) + "\\MemVisualizeLog.mvz"), true);
			SaveAllToCSV(targetPath, true);
			break;
		}
		case MV_VERSION_INFO:
		{
			Read(&m_VersionInfo, sizeof(m_VersionInfo));

			for(u32 i=0; i<m_Heaps.size(); ++i)
			{
				m_Heaps[i]->UpdateWindowTitle();
			}

			break;
		}
		case MV_PUSH:
		{
			char name[64];
			name[63] = 0;
			Read(name, 63);
			stack->name = TagName(name);
			Read(&stack->stackLevel, 1);
			++stack;
			break;
		}
		case MV_POP:
		{
			--stack;
			break;
		}
		case MV_FRAME:
		{
			++m_Frame;
			for(u32 i=0; i<m_Heaps.size(); ++i)
				m_Heaps[i]->NextFrame();
			break;
		}
		case MV_REQUESTDUMP:
		{
			SaveAllToCSV();
			break;
		}	
		default:
		{
			char errorMsg[512];

			sprintf_s(errorMsg, sizeof(errorMsg), "Received invalid command %d. The previous command was of "
				"type %d, so that one probably sent too much or too little data.", type, lastCommand);

			Errorf(errorMsg);
			MessageBox(m_hWndClient, errorMsg, "Invalid command received", MB_ICONHAND | MB_OK);
			fiDeviceTcpIp::GetInstance().Close(m_Handle);
			return;
		}
		};

		lastCommand = type;
	}	
}

void memGameConnection::BroadcastLostConnection()
{
	for (std::vector< memHeap* >::iterator it = m_Heaps.begin(); it < m_Heaps.end(); ++it)
	{
		g_MainFrame->MarkConnectionLost(*it);
	}
}

bool memGameConnection::GetPFSPathFromGameIP(const char* gameIp, std::string& errorOrPath)
{
	// Get SCE_ROOT_DIR env var
	char tmp[1024];
	char SCE_ROOT_DIR[1024];
	if(!GetEnvironmentVariable("SCE_ROOT_DIR", SCE_ROOT_DIR, 1024))
	{
		errorOrPath = "Could not look up SCE_ROOT_DIR environment variable";
		return false;
	}

	// Initialise Orbis Target Manager API COM interface (*shudder*)
	try
	{
		ORTMAPILib::IORTMAPIPtr pTmapi("ortmapi.ORTMAPI");

		// Check API is compatible
		HRESULT hr = pTmapi->CheckCompatibility(ORTMAPILib::BuildVersion);
		if(FAILED(hr))
		{
			if(hr == ORTMAPILib::TMAPI_INCOMPATIBLE_CLIENT_TOO_OLD)
				errorOrPath = "TM API is not compatible, client too old";
			else if(hr == ORTMAPILib::TMAPI_INCOMPATIBLE_CLIENT_TOO_NEW)
				errorOrPath = "TM API is not compatible, client too new";
			else
			{
				formatf(tmp, "TM API is not compatible, error: 0x%08x", hr);
				errorOrPath = tmp;
			}
			return false;
		}

		// Get PlayStation File System drive and options
		unsigned int options = 0;
		char chDriveLetter = pTmapi->GetPFSDrive(&options);
		if((options & ORTMAPILib::PFS_FLAG_TARGETFOLDER_USE_DOTTEDIP) == 0)
		{
			// NB: This is fixable, but requires more code and there's quite enough here just now
			errorOrPath = "Expected PFS to use dotted IP notation!";
			return false;
		}

		// Get all registered targets
		_variant_t vtTargets = pTmapi->GetTargets();
		if(vtTargets.vt != (VT_ARRAY | VT_VARIANT))
		{
			errorOrPath = "Could to get target list from TM API (1)";
			return false;
		}
		CComSafeArray<VARIANT> targets(vtTargets.parray);
		int numTargets = targets.GetCount(); // Cache it - relatively expensive to call
		for(int i=0; i<numTargets; ++i)
		{
			// Get this target...
			if(targets[i].vt != VT_DISPATCH)
			{
				errorOrPath = "Could to get target list from TM API (2)";
				return false;
			}
			ORTMAPILib::ITargetPtr pTarget(targets[i].punkVal);

			// Get the target info and go through all the name value pairs
			_variant_t vtTargetInfo = pTarget->TargetInfo;
			if(vtTargetInfo.vt != (VT_ARRAY | VT_VARIANT))
			{
				errorOrPath = "Could to get target list from TM API (3)";
				return false;
			}
			CComSafeArray<VARIANT> keyValues(vtTargetInfo.parray);
			int numKeys = keyValues.GetCount(); // Cache it - relatively expensive to call
			for(int j=0; j<numKeys; ++j)
			{
				// Get this key
				if(keyValues[j].vt != VT_DISPATCH)
				{
					errorOrPath = "Could to get target list from TM API (4)";
					return false;
				}
				// Is this the "GameLanIpAddress"?
				ORTMAPILib::INameValuePairPtr pNameValuePair(keyValues[j].punkVal);
				_bstr_t bstrKey = pNameValuePair->Name; // NB: Must hold this var while "key" is in scope!
				const char* key = bstrKey;
				if(strcmp(key, "GameLanIpAddress") == 0)
				{
					// Is this the GameLanIpAddress we're interested in?
					_bstr_t bstrValue = pNameValuePair->ValueString;  // NB: Must hold this var while "value" is in scope!
					const char* value = bstrValue;
					if(strcmp(value, gameIp) == 0)
					{
						// Found it!
						_bstr_t bstrHostName = pTarget->HostName;
						errorOrPath = chDriveLetter;
						errorOrPath += ":\\";
						errorOrPath += bstrHostName;
						errorOrPath += "\\";
						return true;
					}
				}
			}
		}
	}
	catch(const _com_error& comerr)
	{
		formatf(tmp, "Could not initialize IORTMAPI interface - make sure the Orbis SDK is installed\nError: %s",
			comerr.ErrorMessage());
		errorOrPath = tmp;
		return false;
	}

	errorOrPath = "Could not find host IP address for kit with game IP address ";
	errorOrPath += gameIp;
	errorOrPath += ". Make sure the devkit is added in PS4 Neighbourhood";
	return false;
}
