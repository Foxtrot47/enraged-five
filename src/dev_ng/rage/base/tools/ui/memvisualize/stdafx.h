// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#pragma once

// Change these values to use different versions
#define WINVER		0x0500
#define _WIN32_WINNT	0x0501
#define _WIN32_IE	0x0501
#define _RICHEDIT_VER	0x0200

#pragma warning(disable:4265)
#pragma warning(disable:4244)	// To make MFC shut up under x64.
#pragma warning(disable:4302)	// To make MFC shut up under x64.

// NOTE: If you get missing includes here, you're missing the RAGE_3RDPARTY environment variable.

#include <atlbase.h>

// NOTE: If the line below produces a compile error, you need to install the following updates:
// Security Update for Microsoft Visual Studio 2008 Service Pack 1 ATL for Smart Devices (KB973675)
// Security Update for Microsoft Visual Studio 2008 Service Pack 1 (KB971092)
typedef ClassesAllowedInStream __READ_COMMENT_ABOVE;

#include <atlapp.h>	// If you get a compile error here, make sure you have RAGE_3RDPARTY defined, pointing to x:\3rdparty, and
					// that it's in your P4 clientspec and up-to-date.

extern CAppModule _Module;

#include <atlwin.h>

#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>
#include <atlscrl.h>
#include <atlmisc.h>
#include <atlcrack.h>
#include <atlsplit.h>
//#include <atlctrlx.h>

#if defined _M_IX86
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

inline void WriteString(const char *string, FILE *fp)
{
	unsigned short strLen = (unsigned short) strlen(string) + 1;
	fwrite(&strLen, sizeof(unsigned short), 1, fp);
	fwrite(string, strLen, 1, fp);
}

inline void ReadString(char *buffer, size_t bufferSize, FILE *fp)
{
	unsigned short strLen;
	fread(&strLen, sizeof(unsigned short), 1, fp);

	size_t toRead = ((size_t) strLen <  bufferSize) ? (size_t) strLen : bufferSize;
	fread(buffer, toRead, 1, fp);

	buffer[bufferSize-1] = 0;

	if (strLen > bufferSize)
	{
		fseek(fp, (int) strLen - (int) bufferSize, SEEK_CUR);
	}
}

#define STORE_DATA(member, fp)			fwrite(&member, sizeof(member), 1, fp)
#define READ_DATA(member, fp)			fread(&member, sizeof(member), 1, fp)


#pragma warning(default:4302)
#pragma warning(default:4244)
