// MainFrm.cpp : implmentation of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"

#include "aboutdlg.h"
#include "memwnd.h"
#include "mainfrm.h"
#include "file/asset.h"
#include "file/tcpip.h"
#include "memconnection.h"
#include "memtags.h"
#include "memheap.h"

using namespace rage;

memGameConnection* g_memGameConnection = NULL;
static volatile int s_CustomPort;
static volatile bool s_Listening;

static void UpdateStatusBarText()
{
	char customPort[128];
	if (!s_Listening)
	{
		formatf(customPort, "Still trying to set up the network listener - do not connect yet!");
	}
	else
	{
		if (!s_CustomPort)
		{
			formatf(customPort, "Running and ready to connect at the normal port.");
		}
		else
		{
			formatf(customPort, "There's another MemVisualize running already! Use @memvisualize:%d to connect to this one.", s_CustomPort);
		}
	}
	::SendMessage(g_MainFrame->m_hWndStatusBar, SB_SETTEXTA, 0, (LPARAM)customPort);
}

BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{
	if(CMDIFrameWindowImpl<CMainFrame>::PreTranslateMessage(pMsg))
		return TRUE;

	HWND hWnd = MDIGetActive();
	if(hWnd != NULL)
		return (BOOL)::SendMessage(hWnd, WM_FORWARDMSG, 0, (LPARAM)pMsg);

	return FALSE;
}

BOOL CMainFrame::OnIdle()
{
	UIUpdateToolBar();
	return FALSE;
}

DWORD WINAPI StartListenerThread(LPVOID lpThreadParameter)
{
	static_cast<CMainFrame*>(lpThreadParameter)->ListenerThread();
	return 0;
}

CMainFrame* g_MainFrame = 0;

LRESULT CMainFrame::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	g_MainFrame = this;

	CreateSimpleToolBar();

	CreateSimpleStatusBar();
	m_StatusBar = m_hWndStatusBar; //.SubclassWindow(m_hWndStatusBar);
	UpdateStatusBarText();

	CreateMDIClient();

	UIAddToolBar(m_hWndToolBar);
	UISetCheck(ID_VIEW_TOOLBAR, 1);
	UISetCheck(ID_VIEW_STATUS_BAR, 1);
	UISetCheck(ID_VIEW_USEBUCKETS, 1);
	UISetCheck(ID_VIEW_USEHISTORY, memTags::UseHistory());
	UISetCheck(ID_VIEW_SHOWLOCKED, 1);
	UISetCheck(ID_VIEW_SHOWEXTERNAL, 1);
	UISetCheck(ID_VIEW_SHOWDEFRAG, 1);
	UISetCheck(ID_VIEW_SHOWMOVED, 1);

	// register object for message filtering and idle updates
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->AddMessageFilter(this);
	pLoop->AddIdleHandler(this);

	memTags::ReadTags("memvisualize.dat");

	m_Thread = CreateThread(NULL, 0, StartListenerThread, this, 0, 0);

	const char *args = GetCommandLine();

	char nameBuffer[RAGE_MAX_PATH];
	int argc = 0;

	while (*args)
	{
		char *argPtr = nameBuffer;
		int bufferSize = sizeof(nameBuffer) - 1;

		while (*args == ' ')
			args++;

		if (*args == '\"')
		{
			args++;

			while (bufferSize-- && *args != 0 && *args != '\"')
			{
				*(argPtr++) = *args;
				args++;
			}

			if (*args == '\"')
			{
				args++;
			}
		}
		else
		{
			while (bufferSize-- && *args && *args != ' ')
			{
				*(argPtr++) = *(args++);
			}
		}

		*argPtr = 0;

		// Skip the first argument, it's the exe name
		if (argc++)
		{
			// Skip empty strings
			if (argPtr != nameBuffer)
			{
				CreateHeapsFromMvz(nameBuffer);
			}
		}
	}
	
	return 0;
}

LRESULT CMainFrame::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	// unregister message filtering and idle updates
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->RemoveMessageFilter(this);
	pLoop->RemoveIdleHandler(this);

	bHandled = FALSE;
	return 1;
}

LRESULT CMainFrame::OnFileExit(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	PostMessage(WM_CLOSE);
	return 0;
}

LRESULT CMainFrame::OnViewToolBar(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL bVisible = !::IsWindowVisible(m_hWndToolBar);
	::ShowWindow(m_hWndToolBar, bVisible ? SW_SHOWNOACTIVATE : SW_HIDE);
	UISetCheck(ID_VIEW_TOOLBAR, bVisible);
	UpdateLayout();
	return 0;
}

LRESULT CMainFrame::OnViewStatusBar(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL bVisible = !::IsWindowVisible(m_hWndStatusBar);
	::ShowWindow(m_hWndStatusBar, bVisible ? SW_SHOWNOACTIVATE : SW_HIDE);
	UISetCheck(ID_VIEW_STATUS_BAR, bVisible);
	UpdateLayout();
	return 0;
}

LRESULT CMainFrame::OnUseBuckets(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL bUseBuckets = !memTags::UseBuckets();

	UISetCheck(ID_VIEW_USEBUCKETS, bUseBuckets);


	memTags::SetUseBuckets(bUseBuckets == TRUE);

	if (g_memGameConnection)
		g_memGameConnection->MarkAllViewsDirty();

	UpdateLayout();

	return 0;
}

LRESULT CMainFrame::OnUseHistory(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL bUseHistory = !memTags::UseHistory();

	UISetCheck(ID_VIEW_USEHISTORY, bUseHistory);

	memTags::SetUseHistory(bUseHistory == TRUE);

	// TODO: This might be useful one day
	/*if (!bUseHistory && g_memGameConnection)
	{		
		for(u32 i = 0; i < g_memGameConnection->m_Heaps.size(); ++i)
			g_memGameConnection->m_Heaps[i]->ClearHistory();
	}*/

	if (g_memGameConnection)
		g_memGameConnection->MarkAllViewsDirty();

	UpdateLayout();

	return 0;
}

LRESULT CMainFrame::OnShowLocked(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL bShowLocked = memTags::GetShowLocked();

	UISetCheck(ID_VIEW_SHOWLOCKED, !bShowLocked);


	memTags::SetShowLocked(!bShowLocked);

	if (g_memGameConnection)
		g_memGameConnection->MarkAllViewsDirty();

	UpdateLayout();

	return 0;
}

LRESULT CMainFrame::OnShowExternal(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL bShowExternal = memTags::GetShowExternal();

	UISetCheck(ID_VIEW_SHOWEXTERNAL, !bShowExternal);


	memTags::SetShowExternal(!bShowExternal);

	if (g_memGameConnection)
		g_memGameConnection->MarkAllViewsDirty();

	UpdateLayout();

	return 0;
}


LRESULT CMainFrame::OnShowDefrag(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL bShowDefrag = memTags::GetShowDefrag();

	UISetCheck(ID_VIEW_SHOWDEFRAG, !bShowDefrag);


	memTags::SetShowDefrag(!bShowDefrag);

	if (g_memGameConnection)
		g_memGameConnection->MarkAllViewsDirty();

	UpdateLayout();

	return 0;
}

LRESULT CMainFrame::OnShowMoved(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL bShowMoved = memTags::GetShowMoved();

	UISetCheck(ID_VIEW_SHOWMOVED, !bShowMoved);


	memTags::SetShowMoved(!bShowMoved);

	if (g_memGameConnection)
		g_memGameConnection->MarkAllViewsDirty();

	UpdateLayout();

	return 0;
}

LRESULT CMainFrame::OnAppAbout(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CAboutDlg dlg;
	dlg.DoModal();
	return 0;
}

LRESULT CMainFrame::OnWindowCascade(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	MDICascade();
	return 0;
}

LRESULT CMainFrame::OnWindowTile(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	MDITile();
	return 0;
}

LRESULT CMainFrame::OnWindowArrangeIcons(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	MDIIconArrange();	
	return 0;
}

#if USE_DEJA_STYLE_BROADCAST
#include "ps3tmapi.h"
#pragma comment(lib, "ps3tmapi.lib")

struct sChecker
{
	u32 addr;
	bool isValid;
};

int __stdcall CheckForPs3(HTARGET target, void* param)
{
	sChecker& checker = *(sChecker*)param;
	ECONNECTSTATUS status;
	char* usage = 0;
	SNPS3GetConnectStatus(target, &status, &usage);
	if (status == CS_CONNECTED)
	{
		SNPS3GamePortIPAddressData addr;
		SNPS3GetGamePortIPAddrData(target, 0, &addr);		
 		if (addr.uIPAddress == checker.addr)
 		{
 			checker.isValid = true;
 			return 1;
 		}
	}
	return 0;
}

void CMainFrame::ListenerThread()
{
	// initialise ps3/360 libs for determining our console IP address
	SNPS3InitTargetComms();
	char libname[256];
	ExpandEnvironmentStrings("%XEDK%\\bin\\win32\\xbdm.dll", libname, 256);
	HMODULE h = LoadLibrary(libname);
	typedef HRESULT (__stdcall*tDmGetAltAddress)(LPDWORD);
	tDmGetAltAddress DmGetAltAddress = (tDmGetAltAddress)GetProcAddress(h, "DmGetAltAddress");

	fiDeviceTcpIp::InitClass(0, NULL);	
	struct sockaddr_in si_me, si_other;
	int slen = sizeof(si_other);
	char buf[256];

	// get local host addr & bind to listening UDP socket
	char myname[64];
	struct hostent *hp;
	memset(&si_me,0,sizeof(si_me));
	gethostname(myname,sizeof(myname)-1);
	if ((si_me.sin_addr.s_addr = inet_addr(myname)) != -1)
		si_me.sin_family = AF_INET;
	else if ((hp = gethostbyname(myname)) != 0) {
		si_me.sin_family = hp->h_addrtype;
		memcpy(&si_me.sin_addr,hp->h_addr, hp->h_length);
	}
	si_me.sin_port = htons(8798);
	SOCKET s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	bind(s, (sockaddr*)&si_me, sizeof(si_me));
	while (1)
	{
		// wait for the broadcast packet
		recvfrom(s, buf, sizeof(buf), 0, (sockaddr*)&si_other, &slen);
		// check if it's from our connected PS3 or 360
		u32 addr = ntohl(si_other.sin_addr.s_addr);
		sChecker checker;	
		checker.isValid = false;
		checker.addr = addr;
		SNPS3EnumerateTargetsEx(CheckForPs3, &checker);
		DWORD xboxaddr = 0;
		if (DmGetAltAddress)
			DmGetAltAddress(&xboxaddr);
		if (xboxaddr == addr || checker.isValid)
		{
			char addrstr[256];
			formatf(addrstr, "%i.%i.%i.%i", addr>>24, (addr>>16)&255, (addr>>8)&255, addr&255);
			Displayf("Incoming connection from %s", addrstr);
			fiHandle handle = fiDeviceTcpIp::Connect(addrstr, 8798);
			if (handle != fiHandleInvalid)
			{
				g_MainFrame->DestroyHeapWindows();

				if ( g_memGameConnection )
				{
					delete g_memGameConnection;
					g_memGameConnection = NULL;
				}

				g_memGameConnection = new memGameConnection(m_hWndClient, handle);
			}
		}
	}
}
#else
void CMainFrame::ListenerThread()
{
	fiDeviceTcpIp::InitClass(0, NULL);	
	int socketPort = 8798;
	fiHandle listener = fiDeviceTcpIp::Listen(socketPort, 1); //, fiDeviceTcpIp::GetLocalHost());
	while (!fiIsValidHandle(listener)) 
	{
		Errorf("sysNamedPipe - cannot create listen socket on port %d - trying the next one", socketPort);
		socketPort++;
		s_CustomPort = socketPort;
		listener = fiDeviceTcpIp::Listen(socketPort, 1);		
	}
	s_Listening = true;
	UpdateStatusBarText();

	while (1)
	{
		fiHandle handle = fiDeviceTcpIp::Pickup(listener);
		if (fiIsValidHandle(handle))
		{
			g_MainFrame->DestroyHeapWindows();

			if ( g_memGameConnection )
			{
				delete g_memGameConnection;
				g_memGameConnection = NULL;
			}

			g_memGameConnection = new memGameConnection(m_hWndClient, handle);
		}
	}
}
#endif

LRESULT CMainFrame::OnCreateHeapWindow(UINT, WPARAM, LPARAM param, BOOL& handled)
{
	memHeap* heap = (memHeap*)param;
	memWnd* wnd = new memWnd(heap);
	heap->m_Wnd = wnd;
	char name[256];
	heap->CreateWindowTitle(name, sizeof(name));
	HWND hwnd = wnd->CreateEx(m_hWndClient, NULL, name);
	
	m_heapWindows.push_back( hwnd );
	
	handled = TRUE;
	return (LRESULT) wnd;
}

LRESULT CMainFrame::OnDestroyHeapWindows(UINT, WPARAM, LPARAM, BOOL& handled)
{
	for ( std::vector< HWND >::iterator it = m_heapWindows.begin(); it != m_heapWindows.end(); ++it)
		::DestroyWindow( *it );
	m_heapWindows.clear();

	handled = TRUE;
	return 0;
}

void CMainFrame::MarkConnectionLost(memHeap *heap)
{
	CompileTimeAssert(sizeof(memHeap *) <= sizeof(LPARAM));

	// Pass the message along to all children
	for ( std::vector< HWND >::iterator it = m_heapWindows.begin(); it != m_heapWindows.end(); ++it)
		::SendMessage( *it, WM_MARK_CONNECTION_LOST, 0, (LPARAM) heap);
}

memWnd *CMainFrame::CreateHeapWindow(memHeap* heap)
{
	return (memWnd *) SendMessage(m_hWnd, WM_CREATE_HEAP_WINDOW, 0, (LPARAM)heap);
}

void CMainFrame::DestroyHeapWindows()
{
	SendMessage(m_hWnd, WM_DESTROY_HEAP_WINDOWS, 0, 0);
}

LRESULT CMainFrame::OnFileSave(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	HWND hWnd = MDIGetActive();
	if(hWnd != NULL)
		return (BOOL)::SendMessage(hWnd, WM_COMMAND, ID_FILE_SAVE, 0);
	return 0;
}

LRESULT CMainFrame::OnFileSaveAll(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if( g_memGameConnection )
	{
		BROWSEINFO			browseInfo;
		PIDLIST_ABSOLUTE	pidl;
		char				path[ MAX_PATH ];

		memset( &browseInfo, 0, sizeof(BROWSEINFO) );
		browseInfo.hwndOwner = this->m_hWnd;
		browseInfo.ulFlags = BIF_USENEWUI;
		pidl = SHBrowseForFolder( &browseInfo );

		if (pidl != NULL)
		{
			SHGetPathFromIDList( pidl, path );
			int filesSaved = g_memGameConnection->SaveAllToTxt( path );
			filesSaved += g_memGameConnection->SaveAllToCSV( path );

			if (filesSaved)
			{
				g_memGameConnection->SaveAllToMvz( (const char *) (CString(path) + "\\MemVisualizeLog.mvz") );				

				char infoMsg[256];
				sprintf_s(infoMsg, sizeof(infoMsg), "Successfully wrote %d files", filesSaved + 1);
				Displayf(infoMsg);

				MessageBox(infoMsg, "Writing successful", MB_ICONINFORMATION | MB_OK);
			}
		}		
	}

	return 0;
}

LRESULT CMainFrame::OnFileOpen(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
/*
	// Query for the path
	BROWSEINFO			browseInfo;
	PIDLIST_ABSOLUTE	pidl;
	char				path[ MAX_PATH ];

	memset( &browseInfo, 0, sizeof(BROWSEINFO) );
	browseInfo.hwndOwner = this->m_hWnd;
	browseInfo.ulFlags = BIF_USENEWUI;
	pidl = SHBrowseForFolder( &browseInfo );
	if (SHGetPathFromIDList( pidl, path ))
	{
		CreateHeapsFromLogs(path);
	}
*/
	CFileDialog fd(true, "mvz", NULL, OFN_HIDEREADONLY, "*.mvz (MemVisualize)\0*.mvz\0*.* (All Files)\0*.*\0");
	if (fd.DoModal() == IDOK)
	{
		CreateHeapsFromMvz(fd.m_szFileName);
	}

	return 0;
}
/*
bool CMainFrame::CreateHeapFromSingleLog(const char *path, diagTrackerRemote2::ClientInfo *clientInfo, diagTrackerRemote2::ClientInfo *versionInfo)
{
	// Let's parse this file.
	FILE *file = fopen(path, "rt");

	if (!file)
	{
		MessageBox(CString("Error opening ") + path, "File error", MB_ICONHAND | MB_OK);
		return false;
	}

	// Let's parse this file line by line.
	memSymFile *syms = new memSymFile();

	memHeap *heap = new memHeap(ASSET.FileName(path), syms);
	heap->m_ClientInfo = clientInfo;
	heap->m_VersionInfo = versionInfo;

	while (!feof(file))
	{
		char line[1024];
		
		fgets(line, sizeof(line), file);

		// Chomp it.
		size_t len = strlen(line);

		if (len > 0)
		{
			if (line[len-1] == '\n')
			{
				line[len-1] = 0;
			}
		}

		if (line[0] == '#')
		{
			char *valueStr = strchr(line, ':');

			if (valueStr)
			{
				valueStr++;
				while (*valueStr == ' ')
				{
					valueStr++;
				}

				// This is a comment. Maybe it's got something interesting for us.
				if (strstr(line, "DumpVersion"))
				{
					int dumpVersion = atoi(valueStr);

					if (dumpVersion < 1 || dumpVersion > 1)
					{
						MessageBox(CString("Error in file ") + path + ": Unknown file version.", "Unknown file version", MB_ICONHAND | MB_OK);
						return false;
					}
				}

				if (strstr(line, "Configuration"))
				{
					safecpy(clientInfo->m_Configuration, valueStr);
				}

				if (strstr(line, "Platform"))
				{
					safecpy(clientInfo->m_Platform, valueStr);
				}
			}
		}
	}

	fclose(file);
	CreateHeapWindow(heap);

	return true;
}
*/
/*
void CMainFrame::CreateHeapsFromLogs(const char *path)
{
	// Let's scan the path for logs.
	CFileFind finder;
	CString filter = path;
	filter += "\\*.txt";

	diagTrackerRemote2::ClientInfo *clientInfo = new diagTrackerRemote2::ClientInfo();

	if (finder.FindFile((const char *) filter))
	{
		do 
		{
			if (!finder.IsDots() && !finder.IsDirectory())
			{
				const char *filePath = (const char *) finder.GetFilePath();

				if (filePath[strlen(filePath)-1] != '\\')
				{
					// Here's a heap.	
					if (!(CreateHeapFromSingleLog(path, clientInfo)))
					{
						// Abort - possibly a version error, and let's not spam the user with 12 dialog boxes.
						break;
					}
				}
			}
		} while (finder.FindNextFile());
	}
}
*/
void CMainFrame::CreateHeapsFromMvz(const char *path)
{
	memGameConnection *connection = rage_new memGameConnection((HWND) *this, fiHandleInvalid);
	connection->ReadFromMvz(path);
	Displayf("File read");

	g_memGameConnection = connection;
}

LRESULT CMainFrame::OnNewAllocationsOnly(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if( g_memGameConnection )
	{
		g_memGameConnection->ShowNewAllocationsOnly();
	}


	return 0;
}

LRESULT CMainFrame::OnAddressSearch(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if( g_memGameConnection )
	{
		g_memGameConnection->AddressSearch();
	}

	return 0;
}

LRESULT CMainFrame::OnFilter(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if( g_memGameConnection )
	{
		g_memGameConnection->Filter();
	}

	return 0;
}

LRESULT CMainFrame::OnAllAllocations(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if( g_memGameConnection )
	{
		g_memGameConnection->ShowAllAllocations();
	}

	return 0;
}
