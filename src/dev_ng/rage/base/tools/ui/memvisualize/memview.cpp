// memvisualizeView.cpp : implementation of the CMemvisualizeView class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "memview.h"
#include "diag/tracker.h"
#include "file/tcpip.h"
#include "mainfrm.h"
#include <cmath>
#include "memheap.h"
#include "memstack.h"
#include "memtags.h"
#include "HistoryDlg.h"

memView::memView(memHeap* heap)
:	memHeapView(heap)
{	
}

void memView::OnAlloc(memBlock* block)
{
	m_NeedUpdate = true;
}

void memView::OnFree(memBlock* /*block*/)
{
	m_NeedUpdate = true;
}

void memView::OnUpdate()
{
	if (m_NeedUpdate && m_hWnd)
	{
		Invalidate();
		m_NeedUpdate = false;
	}
}

void memView::NeedsRedraw()
{
	m_NeedUpdate = true;
	memHeapView::NeedsRedraw();
}

void memView::BringToTop()
{
	HWND thisWindow = m_hWnd;
	bool isChild = true;
	WINDOWINFO winfo;

	winfo.cbSize = sizeof(WINDOWINFO);

	do
	{
		::BringWindowToTop(thisWindow);
		SetForegroundWindow(thisWindow);
		::GetWindowInfo(thisWindow, &winfo);
		isChild = (winfo.dwStyle & MDIS_ALLCHILDSTYLES) == 0;
		thisWindow = ::GetParent(thisWindow);
	} while (isChild && thisWindow != 0);
}


void memView::OnSelect(memStackEntry* stack)
{
	m_NeedUpdate = true;
	OnUpdate();
}

BOOL memView::PreTranslateMessage(MSG* pMsg)
{
	pMsg;
	return FALSE;
}

void memView::OnInit()
{
    glClearColor(0,0,0,0);

	m_ScrollY = 0;
	m_RowHeight = 4;
	m_BytesPerRow = (size_type) 1 << int(ceilf(logf(m_Heap->m_HeapSize / 200.0f)/logf(2)));
	//m_BytesPerRow = 5504<<int(ceilf(logf(m_Heap->m_HeapSize / 5504 / 200.0f)/logf(2)));

	m_Tip.Create(m_hWnd, 0, 0, TTS_ALWAYSTIP|TTS_NOPREFIX|TTS_NOFADE|TTS_NOANIMATE);
	CToolInfo toolInfo(TTF_IDISHWND | TTF_SUBCLASS, m_hWnd, 0, 0, "");
	m_Tip.AddTool(&toolInfo);
	m_Tip.SetMaxTipWidth(0);
	m_Tip.Activate(TRUE);
	m_Tip.SetDelayTime(TTDT_INITIAL, 100); //2000);
	m_Tip.SetDelayTime(TTDT_AUTOPOP, 32767); //5000);
	m_Tip.SetDelayTime(TTDT_RESHOW, 2000);
}

void memView::OnResize(int cx, int cy)
{
	UpdateScrollRange();
    CRect rect;
    GetClientRect(&rect);
	cx = rect.Width();

	glViewport(0, 0, cx, cy);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, cx, cy, 0, 1, -1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void memView::UpdateScrollRange()
{
    CRect rect;
    GetClientRect(&rect);
	int cy = rect.Height();

	int heapHeightInPixels = int((m_Heap->m_HeapSize / m_BytesPerRow) * m_RowHeight);
	int maxScrollY = max(0, heapHeightInPixels - cy);
	m_ScrollY = max(0, min(m_ScrollY, maxScrollY));
	SCROLLINFO scrollInfo = {sizeof(SCROLLINFO)};
	scrollInfo.fMask = SIF_RANGE | SIF_PAGE | SIF_POS;
	scrollInfo.nMin = 0;
	scrollInfo.nMax = heapHeightInPixels;
	scrollInfo.nPage = cy;
	scrollInfo.nPos = m_ScrollY;
	SetScrollInfo(SB_VERT, &scrollInfo);	

    GetClientRect(&rect);
	int cx = rect.Width();
	m_PixelsPerByte = float(cx) / m_BytesPerRow;
}

void SetCol(COLORREF c)
{
   glColor3ub((GLubyte)c&255,(GLubyte)((c>>8)&255),(GLubyte)((c>>16)&255));
}

void memView::OnRender()
{
    glClear(GL_COLOR_BUFFER_BIT);
	glDepthMask(false);
	glDisable(GL_DEPTH_TEST);

    CRect rect;
    GetClientRect(&rect);
	size_type cx = rect.Width();
	size_type cy = rect.Height();
	size_type row = m_ScrollY / m_RowHeight;

	u32 minFrame = m_MinTimestamp;
	size_type startaddr = row * m_BytesPerRow;
	size_type lastrow = (m_ScrollY + cy + m_RowHeight - 1) / m_RowHeight;
	size_type lastVisible = lastrow * m_BytesPerRow;

	m_Heap->m_Cs.Lock();

	// Render filters
	u32 mustHaveFlags = 0;
	u32 mustNotHaveFlags = 0;

	if (!memTags::GetShowLocked())
		mustNotHaveFlags |= diagTracker::MEMBUCKETFLAG_LOCKED;

	if (!memTags::GetShowExternal())
		mustNotHaveFlags |= diagTracker::MEMBUCKETFLAG_EXTERNAL;

	if (!memTags::GetShowMoved())
		mustNotHaveFlags |= diagTracker::MEMBUCKETFLAG_MOVED;

	if (!memTags::GetShowDefrag())
		mustNotHaveFlags |= diagTracker::MEMBUCKETFLAG_CAN_DEFRAG;

	for (int pass=0; pass<2; pass++)
	{
		bool hasDefraggableBlocks = false;

		if (pass == 0)
		{
			glLineWidth((GLfloat)(m_RowHeight - 1));
		}
		else
		{
			glLineWidth(1);
			glLineStipple(2, 0x5555);
			glEnable(GL_LINE_STIPPLE);
		}

		glBegin(GL_LINES);
		std::set<memBlock>::iterator it = m_Heap->m_ActiveBlocks.lower_bound(memBlock(startaddr));
		if (it != m_Heap->m_ActiveBlocks.begin())
			--it;

		// The second pass will draw purple/white lines only.
		if (pass == 1)
		{
			glColor3f(1,0,1);
		}

		for(; it != m_Heap->m_ActiveBlocks.end(); ++it)
		{
			size_type start = it->m_Start;
 			if (start > lastVisible)
 				break;

			if (it->m_FrameAllocated < minFrame)
				continue;

			u32 flags = it->m_BucketAndFlags;

			// Apply the filter.
			if ((mustHaveFlags & flags) != mustHaveFlags)
				continue;

			if (mustNotHaveFlags & flags)
				continue;

			// We only mark stuff that can be defragmented.
			bool canDefrag = (flags & diagTracker::MEMBUCKETFLAG_CAN_DEFRAG) != 0;
			if (pass == 1 && !canDefrag)
				continue;

			hasDefraggableBlocks |= canDefrag;

			size_type end = it->m_End;

			if (pass == 0)
			{

				if (&*it == m_ActiveBlock || it->m_Stack->IsHighlighted())
				{
					glColor3f(1,1,1);
				}
				else
				{
					int b = it->m_Stack->m_Colour;
					if  (memTags::UseBuckets())
						SetCol(memTags::GetColorFromIndex((it->m_BucketAndFlags & diagTracker::MEMBUCKET_BUCKETMASK) + 1));
					else
						SetCol(b);
				}
			}

			int row = int(start / m_BytesPerRow);
			size_type rowStart = row * m_BytesPerRow;
			while (start < end)
			{
				int x1 = int((start - rowStart) * m_PixelsPerByte);
				int x2 = int((end - rowStart) * m_PixelsPerByte);
				if (x2 > (int) cx)
					x2 = (int) cx;
				// force block start & end to even boundaries and
				// then subtract 1 from the end so there is always
				// a gap after a block
				x1 &= ~1;
				x2 = (x2&~1)-1;
				if (x2 > x1)
				{
					int y = row * m_RowHeight - m_ScrollY + 2 + pass;
					glVertex2i(x1, y);
					glVertex2i(x2, y);

					if (pass == 1)
					{
						glColor3f(1,1,1);
						glVertex2i(x1 + 1, y);
						glVertex2i(x2 - 1, y);
						glColor3f(1,0,1);
					}
				}
				rowStart += m_BytesPerRow; ++row;
				start = rowStart;
			}		
		}

		if (pass == 0)
		{
			size_type end = m_Heap->m_HeapSize;
			row = end / m_BytesPerRow;
			size_type rowStart = row * m_BytesPerRow;
			int x1 = int((end - rowStart) * m_PixelsPerByte);
			int y = int(row * m_RowHeight - m_ScrollY + 2);
			glColor3f(.2f,.2f,.2f);
			glVertex2i(x1, y);
			glVertex2i((GLint) cx, y);
		}

		glEnd();
		glLineStipple(1, 0xffff);
		glDisable(GL_LINE_STIPPLE);

		// Early-out if we don't have any defraggable blocks to render.
		if (!hasDefraggableBlocks)
			break;
	}

	m_Heap->m_Cs.Unlock();

	size_type end = m_Heap->m_HeapSize;
	row = end / m_BytesPerRow;
	int y = int(row * m_RowHeight - m_ScrollY + 2);
	y += m_RowHeight - 3;
	glBegin(GL_QUADS);
	glColor3f(.2f,.2f,.2f);
	glVertex2i(0, y);
	glVertex2i((GLint) cx, y);
	glVertex2i((GLint) cx, (GLint) cy);
	glVertex2i(0, (GLint) cy);
	glEnd();
}

LRESULT memView::OnMouse(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	bHandled = FALSE;
	if (m_Tip.IsWindow())
		m_Tip.RelayEvent((LPMSG)GetCurrentMessage());
	return FALSE;
}

LRESULT memView::OnDestroyHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	memHeapView::OnDestroy();
	return COpenGL<memView>::OnDestroy(uMsg, wParam, lParam, bHandled);
}

BOOL memView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	if (zDelta) //nFlags & MK_CONTROL)
	{
		ScreenToClient(&pt);
		int row = pt.y + m_ScrollY;
		if (zDelta > 0 && m_BytesPerRow > 256)
		{
			m_BytesPerRow /= 2;
			row *= 2;
		}
		else if (zDelta < 0 && m_BytesPerRow < 1024 * 1024)
		{
			m_BytesPerRow *= 2;
			row /= 2;
		}
		m_ScrollY = row - pt.y;
		UpdateScrollRange();
		Invalidate();
	}
    return TRUE;
}

void memView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar pScrollBar)
{
	SCROLLINFO si = {sizeof(SCROLLINFO), SIF_ALL};
	GetScrollInfo(SB_VERT, &si);
    switch (nSBCode)
    {
    case SB_LINEUP:		m_ScrollY -= m_RowHeight*4; break;
	case SB_LINEDOWN:   m_ScrollY += m_RowHeight*4; break;
	case SB_PAGEUP:     m_ScrollY -= si.nPage; break;
	case SB_PAGEDOWN:	m_ScrollY += si.nPage; break;
    case SB_THUMBPOSITION: break;
    case SB_THUMBTRACK:	m_ScrollY = si.nTrackPos; break;
    case SB_TOP:        m_ScrollY = si.nMin; break;
    case SB_BOTTOM:		m_ScrollY = si.nMax - si.nPage; break;
    case SB_ENDSCROLL:  break;
    }
	if (m_ScrollY > int(si.nMax - si.nPage))
		m_ScrollY = si.nMax - si.nPage;
	if (m_ScrollY < 0)
		m_ScrollY = 0;
	SetScrollPos(SB_VERT, m_ScrollY);
	Invalidate();
}

memBlock* memView::GetBlock(CPoint point, size_type* pAddr, memBlock** nextblock)
{
    CRect rect;
    GetClientRect(&rect);
	int row = (point.y + m_ScrollY) / m_RowHeight;
	size_type addr = row * m_BytesPerRow + int(point.x / m_PixelsPerByte);
	if (pAddr)
		*pAddr = addr;
	memBlock block(addr);
	memBlock* pBlock = 0;
	m_Heap->m_Cs.Lock();
	std::set<memBlock>::iterator i = m_Heap->m_ActiveBlocks.lower_bound(block);	
	while (i != m_Heap->m_ActiveBlocks.begin() && (i == m_Heap->m_ActiveBlocks.end() || i->m_Start > addr))
		--i;
	if (i != m_Heap->m_ActiveBlocks.end())
	{
		pBlock = (memBlock *) &*i;
		if (++i != m_Heap->m_ActiveBlocks.end() && nextblock)
			*nextblock = (memBlock *) &*i;
	}
	m_Heap->m_Cs.Unlock();
	return pBlock;
}

void memView::OnMouseMove(UINT nFlags, CPoint point)
{
	if (point == m_OldPoint)
		return;
	m_OldPoint = point;

	char text[16384];
	text[0] = 0;

	memBlock* nextblock = 0;
	size_type addr = 0;
	memBlock* block = GetBlock(point, &addr, &nextblock);

	if (m_ActiveBlock != block)
	{
		m_ActiveBlock = block;
		Invalidate();
		if (block)
		{
			if (block->m_End < addr || block->m_Start > addr)
			{
				sprintf_s(text, "0x%08llx : %i bytes - FREE BLOCK", m_Heap->m_HeapStart + block->m_End, (nextblock ? nextblock->m_Start : m_Heap->m_HeapSize) - block->m_End);
				m_ActiveBlock = 0;
			}
			else
			{
				const char *bucketName = memTags::GetbucketName(block->m_BucketAndFlags & diagTracker::MEMBUCKET_BUCKETMASK);
				
				const char *extra = "";
				if (block->m_BucketAndFlags & diagTracker::MEMBUCKETFLAG_EXTERNAL)
					extra = "EXTERNAL ";
				else if (block->m_BucketAndFlags & diagTracker::MEMBUCKETFLAG_LOCKED)
					extra = "LOCKED ";
				else if (block->m_BucketAndFlags & diagTracker::MEMBUCKETFLAG_MOVED)
					extra = "MOVED, DEFRAGMENTABLE";
				else if (block->m_BucketAndFlags & diagTracker::MEMBUCKETFLAG_CAN_DEFRAG)
					extra = "DEFRAGMENTABLE ";
				
				if( bucketName )
					sprintf_s(text, "[%i|%s] 0x%08llx : %i bytes %s\n", block->m_Id, bucketName , m_Heap->m_HeapStart + block->m_Start, block->m_End - block->m_Start, extra);
				else
					sprintf_s(text, "[%i|%i] 0x%08llx : %i bytes %s\n", block->m_Id, block->m_BucketAndFlags & diagTracker::MEMBUCKET_BUCKETMASK, m_Heap->m_HeapStart + block->m_Start, block->m_End - block->m_Start, extra);
				
				memStackEntry* stack = block->m_Stack;
				
				while (stack)
				{
					if (stack->m_Name[0] &&
						!strstr(stack->m_Name, "sysIpcThreadWrapper") &&
						!strstr(stack->m_Name, "CommonMain") &&
						!strstr(stack->m_Name, "rage::ExceptMain"))
					{
						strcat_s(text, "\n");
						strcat_s(text, stack->m_Name);
					}
					stack = stack->m_Parent;
				}
			}
			m_Tip.UpdateTipText(text, m_hWnd);
		}
	}
}

void memView::OnLButtonDown(UINT nFlags, CPoint point)
{
	memBlock* pBlock = GetBlock(point);

	if (pBlock)
	{
		if (nFlags & MK_CONTROL)
		{
			// Set the current bucket to whatever this block's bucket is.
			memTags::ms_FocusBucket = pBlock->m_BucketAndFlags & diagTracker::MEMBUCKET_BUCKETMASK;
			OnSelect(NULL);
		}
		else
		{
			memTags::ms_FocusBucket = -1;
			m_Heap->Select(pBlock);
		}
	}
}

void memView::OnLButtonUp(UINT nFlags, CPoint point)
{
	SetFocus();
}

void memView::OnRButtonDown(UINT nFlags, CPoint point)
{
	size_type addr = 0;
	memBlock* pNextBlock = 0;	
	memBlock* pBlock = GetBlock(point, &addr, &pNextBlock);
	
	if (pBlock)
	{
		const bool used = (addr < pBlock->m_End && addr >= pBlock->m_Start);
		if (used)
		{
			CHistoryDlg dlg(m_Heap, *pBlock, used);
			dlg.DoModal();
		}
		else
		{
			const size_t bytes = (pNextBlock ? pNextBlock->m_Start : m_Heap->m_HeapSize) - pBlock->m_End;
			memBlock block(pBlock->m_End, pBlock->m_End + bytes);
			CHistoryDlg dlg(m_Heap, block, used);
			dlg.DoModal();
		}
	}
	else
	{
		memBlock block(0, m_Heap->m_HeapSize);
		CHistoryDlg dlg(m_Heap, block, false);
		dlg.DoModal();
	}
}

void memView::OnRButtonUp(UINT nFlags, CPoint point)
{
	SetFocus();
}