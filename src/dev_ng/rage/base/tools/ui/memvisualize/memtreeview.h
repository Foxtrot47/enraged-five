#ifndef MEM_TREE_VIEW_H
#define MEM_TREE_VIEW_H

#include "memheap.h"
#include <hash_map>

#define WM_TREEITEM		(WM_USER + 200)

class memTreeView : public CWindowImpl<memTreeView, CTreeViewCtrl>, public memHeapView
{
public:
	DECLARE_WND_SUPERCLASS(NULL, CTreeViewCtrl::GetWndClassName())

	memTreeView(memHeap* heap) : memHeapView(heap) {}

	BEGIN_MSG_MAP(memTreeView)
		MESSAGE_HANDLER(WM_TREEITEM, OnTreeItemHandler)
 		MSG_WM_CREATE(OnCreate)
		MSG_WM_DESTROY(OnDestroy)
		MSG_WM_KILLFOCUS(OnKillFocus)
		MSG_WM_ERASEBKGND(OnEraseBkgnd)
		MSG_WM_PAINT(OnPaint)
		REFLECTED_NOTIFY_CODE_HANDLER(TVN_SELCHANGED, OnSelChanged)
		REFLECTED_NOTIFY_CODE_HANDLER(TVN_GETDISPINFO, GetDispInfo)
	END_MSG_MAP()

	bool ReadFromMvz(FILE *fp, int version, const atArray<memStackEntry *, 0, u32> &stackReferenceArray);
	void SaveAsMvz(FILE* out, stdext::hash_map<memStackEntry *, int> &stackReferenceMap);

	void OnPaint(CDCHandle dc);
	BOOL OnEraseBkgnd(CDCHandle dc);
	int OnCreate(LPCREATESTRUCT lpCreateStruct);
	void OnKillFocus(CWindow wndFocus);
	LRESULT GetDispInfo(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/);
	LRESULT OnSelChanged(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/);

	LRESULT OnTreeItemHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	virtual void OnAlloc(memBlock* block);
	virtual void OnFree(memBlock* block);
	virtual void OnSelect(memBlock* block);
	virtual void OnUpdate();

	HTREEITEM TreeItem(memStackEntry* stack);
private:
	void RecurseSaveTree(FILE* out, stdext::hash_map<memStackEntry *, int> &stackReferenceMap, HTREEITEM item);
	bool RecurseLoadTree(FILE *fp, int version, const atArray<memStackEntry *, 0, u32> &stackReferenceArray, HTREEITEM parent, HTREEITEM prev);



public:


	stdext::hash_map<memStackEntry*, HTREEITEM> m_items;
};

#endif // MEM_TREE_VIEW_H
