#pragma once

#include "memview.h"
#include "memtreeview.h"
#include "memlistview.h"
#include "memstacklistview.h"
#include "resource.h"

#include <hash_map>

#define WM_MARK_CONNECTION_LOST	(WM_USER+801)


class memWnd : public CMDIChildWindowImpl<memWnd>
{
public:
	DECLARE_FRAME_WND_CLASS(NULL, IDR_MDICHILD)

	memWnd(memHeap* heap)
		: m_view(heap)
		, m_stackview(heap)
		, m_tree(heap)
#if ENABLE_ALLOCATION_LIST_VIEW
		, m_list(heap)
#endif
		, m_heap(heap)
	{}

	bool ReadFromMvz(FILE *fp, int version, const atArray<memStackEntry *, 0, u32> &stackReferenceArray);
	void SaveAsMvz(FILE* out, stdext::hash_map<memStackEntry *, int> &stackReferenceMap);

	CHorSplitterWindow m_horsplitter;
	CSplitterWindow m_splitter;
	CSplitterWindow m_splitter2;
	memView m_view;
	memTreeView m_tree;
	memStackListView m_stackview;
#if ENABLE_ALLOCATION_LIST_VIEW
	memListView m_list;
#endif
	memHeap *m_heap;

	virtual void OnFinalMessage(HWND /*hWnd*/);

	BEGIN_MSG_MAP(memWnd)
		COMMAND_ID_HANDLER(ID_FILE_SAVE, OnFileSave)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_FORWARDMSG, OnForwardMsg)
		MESSAGE_HANDLER(WM_MARK_CONNECTION_LOST, OnMarkConnectionLost)
		REFLECT_NOTIFICATIONS()
		CHAIN_MSG_MAP(CMDIChildWindowImpl<memWnd>)
	END_MSG_MAP()

// Handler prototypes (uncomment arguments if needed):
//	LRESULT MessageHandler(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
//	LRESULT CommandHandler(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled);
	LRESULT OnForwardMsg(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/);
	LRESULT OnFileSave(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnMarkConnectionLost(UINT, WPARAM, LPARAM lParam, BOOL& handled);

};
