// FilterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FilterDlg.h"
#include "memheap.h"


// CFilterDlg dialog

CFilterDlg::CFilterDlg(std::vector<memHeap*> *heaps)
{
	m_StartFrame = 0;
	m_EndFrame = 10000;

	m_Heaps = heaps;
}

CFilterDlg::~CFilterDlg()
{
}

void CFilterDlg::OnFrameChange(int wParam, short wParam2, HWND wnd)
{
	CWindow startFrameCtrl = GetDlgItem(IDC_STARTFRAME);
	CWindow endFrameCtrl = GetDlgItem(IDC_ENDFRAME);
	CTrackBarCtrl trackbar;
	int *targetValue;

	if (wnd == startFrameCtrl.m_hWnd)
	{
		trackbar = (CTrackBarCtrl) startFrameCtrl;
		targetValue = &m_StartFrame;
	}
	else if (wnd == endFrameCtrl.m_hWnd)
	{
		trackbar = (CTrackBarCtrl) endFrameCtrl;
		targetValue = &m_EndFrame;
	}
	else
	{
		return;
	}

	int position = trackbar.GetPos();

	*targetValue = position;
	
	u32 minTimestamp = m_StartFrame;

	for (int x=0; x<m_Heaps->size(); x++)
	{
		(*m_Heaps)[x]->SetMinTimestamp(minTimestamp);
	}
}
