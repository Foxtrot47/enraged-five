#include "stdafx.h"
#include "memsymfile.h"
#include "file/stream.h"
#include "file/device.h"
#include "system/platform.h"

memSymFile::memSymFile(HWND hWnd /* = NULL*/) : m_hWnd(hWnd), m_SymFile(NULL), m_MainSize(0), m_UnrelocatedMainAddress(0)
{
}


void memSymFile::Load(const char* filename, bool is64Bit, char platformId, size_type mainSize /*= 0*/)
{
	fiStream* f = fiStream::Open(filename, fiDeviceLocal::GetInstance());
	if (!f)
	{
		if (platformId == platform::WIN64PC)
		{
			// EJ: Try one last hack before we're out of luck...
			char path[128] = {0};
			
			// 1) 
			formatf(path, "X:\\gta5\\dev_ng\\%s", filename);
			f = fiStream::Open(path, fiDeviceLocal::GetInstance());

			if (!f)
			{
				// 2)
				formatf(path, "C:\\Program Files\\Rockstar Games\\Grand Theft Auto V\\%s", filename);
				f = fiStream::Open(path, fiDeviceLocal::GetInstance());
				if (!f)
				{
					MessageBox(m_hWnd, CString("Cannot open file ") + filename + ". This means MemVis won't have callstacks!", "Error opening file", MB_ICONHAND | MB_OK);
					return;
				}
			}
		}
		else
		{
			return;
		}
	}

	m_MainSize = mainSize;
	int size = f->Size();
	m_SymFile = rage_new char[size];
	f->Read(m_SymFile, size);
	f->Close();
	
	if (is64Bit)
	{
		m_NumSyms = (u32) *(u64*)m_SymFile;
	}
	else
	{
		m_NumSyms = *(u32*)m_SymFile;
	}
	
	// right after NumSyms there's another int that represents the preferred load address of the executable
	// (only meaningful on PC - it should be 0 on consoles). For now we'll just ignore it - it should be
	// used when computing addresses for PC callstacks. - AP
	
	if (is64Bit)
	{
		if (m_MainSize)
		{
			// Durango and orbis have an additional 64-bit value (unrelocated address of main())
			if (platformId == 'd' || platformId == 'o')
			{
				m_UnrelocatedMainAddress = (size_type) (*(void**) &m_SymFile[16]);
				m_Syms64 = (Symbol64*)&m_SymFile[24];
			}
			else
			{
				m_UnrelocatedMainAddress = (size_type) (*(void**) &m_SymFile[8]);
				m_Syms64 = (Symbol64*)&m_SymFile[16];	
			}
		}
		else
		{
			m_Syms64 = (Symbol64*)&m_SymFile[16];
		}
	}
	else
	{
		m_Syms = (Symbol*)&m_SymFile[8];
	}
	m_Is64Bit = is64Bit;
}

void memSymFile::Unload()
{
	delete[] m_SymFile;
	m_SymFile = NULL;
}

const char* memSymFile::GetName(size_type addr)
{
	int low = 0, high = m_NumSyms - 2;

	// HACK: This "should" only happen on Durango
	if (m_MainSize)
		addr = addr - m_MainSize + m_UnrelocatedMainAddress - 0x10;

	if (m_Is64Bit)
	{
		while (low<high) 
		{
			int mid = (low + high) >> 1;
			if (addr >= m_Syms64[mid].Addr && addr < m_Syms64[mid+1].Addr) 
				return m_SymFile + m_Syms64[mid].Name;
			else if (addr > m_Syms64[mid].Addr)
				low = mid+1;
			else
				high = mid;
		}
	}
	else
	{
		while (low<high) 
		{
			int mid = (low + high) >> 1;
			if (addr >= m_Syms[mid].Addr && addr < m_Syms[mid+1].Addr) 
				return m_SymFile + m_Syms[mid].Name;
			else if (addr > m_Syms[mid].Addr)
				low = mid+1;
			else
				high = mid;
		}
	}
	return "";
}
