#pragma once

#include <afxmsg_.h>

#include "resource.h"

#include "MemStack.h"
#include "MemHeap.h"

class CHistoryDlg : public CDialogImpl<CHistoryDlg>
{
public:
	// Dialog Data
	enum { IDD = IDD_HISTORY };

	BEGIN_MSG_MAP(CHistoryDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_ID_HANDLER(IDOK, OnOKCancel)
		COMMAND_ID_HANDLER(IDCANCEL, OnOKCancel)
		COMMAND_ID_HANDLER(IDC_HISTORY_UPDATE, OnHistoryUpdate)
	END_MSG_MAP()

private:
	memHeap* m_pHeap;
	memBlock m_block;
	bool m_used;

public:
	CHistoryDlg(memHeap* pHeap, const memBlock& block, bool used = true);
	virtual ~CHistoryDlg(void);

protected:
	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnOKCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnHistoryUpdate(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);

private:
	void UpdateCallstacks();
};

