#ifndef MEM_SYM_FILE_H
#define MEM_SYM_FILE_H

#include "MemStack.h"

using namespace rage;

struct Symbol {
	u32 Addr;
	u32 Name;
};
struct Symbol64 {
	u64 Addr;
	u64 Name;
};

class memSymFile
{
private:
	HWND m_hWnd;

public:
	memSymFile(HWND hWnd = NULL);

	void Load(const char* filename, bool is64bit, char platformId, size_type mainOffset = 0);
	void Unload();
	const char* GetName(size_type addr);

	char* m_SymFile;
	u32 m_NumSyms;
	size_type m_MainSize;
	size_type m_UnrelocatedMainAddress;

	union {
		Symbol* m_Syms;
		Symbol64* m_Syms64;
	};

	bool m_Is64Bit;
};

#endif // MEM_SYM_FILE_H
