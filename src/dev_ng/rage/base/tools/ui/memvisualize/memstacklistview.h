#ifndef MEM_STACK_LIST_VIEW_H
#define MEM_STACK_LIST_VIEW_H

#include "memheap.h"
#include <hash_map>

// simple class to draw a caption above a window
class CCaptionedWnd : public CWindowImpl<CCaptionedWnd>
{
public:
	DECLARE_WND_CLASS(NULL)

	BEGIN_MSG_MAP(memStackListView)
		MSG_WM_CREATE(OnCreate)
		MSG_WM_SIZE(OnSize)
		MSG_WM_ERASEBKGND(OnEraseBkgnd)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		FORWARD_NOTIFICATIONS()
	END_MSG_MAP()

	int OnCreate(LPCREATESTRUCT lpCreateStruct);
	void SetCaption(const char* caption);
	void SetChild(HWND child);
	BOOL OnEraseBkgnd(CDCHandle dc);
	LRESULT OnPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	void OnSize(UINT nType, CSize size);

	const char* m_caption;
	CWindow m_hWndChild;
	CFont m_hFont;
	int m_height;
};

class memStackFlatEntry;

class memStackListView : public CWindowImpl<memStackListView>, public CCustomDraw<memStackListView>, public memHeapView
{
public:
	memStackListView(memHeap* heap) : memHeapView(heap) {}
	~memStackListView();

	BEGIN_MSG_MAP_EX(memStackListView)
		MESSAGE_HANDLER(WM_UPDATE, OnUpdateHandler)
 		MSG_WM_CREATE(OnCreate)
		MSG_WM_DESTROY(OnDestroy)
		MSG_WM_SIZE(OnSize)
		MSG_WM_ERASEBKGND(OnEraseBkgnd)
		NOTIFY_CODE_HANDLER(NM_DBLCLK, OnLButtonDblClk)
		NOTIFY_CODE_HANDLER(LVN_COLUMNCLICK, OnColumnClick)
		NOTIFY_CODE_HANDLER(LVN_GETDISPINFO, OnGetDispInfo)
		NOTIFY_CODE_HANDLER(LVN_ITEMCHANGED, OnItemChanged)
		CHAIN_MSG_MAP(CCustomDraw<memStackListView>)
	ALT_MSG_MAP(1)
		MSG_WM_ERASEBKGND(OnEraseBkgnd)
	END_MSG_MAP()

	bool ReadFromMvz(FILE *fp, int version, const atArray<memStackEntry *, 0, u32> &stackReferenceArray);
	void SaveAsMvz(FILE* out, const stdext::hash_map<memStackEntry *, int> &stackReferenceMap);

	int OnCreate(LPCREATESTRUCT lpCreateStruct);
	void OnSize(UINT nType, CSize size);
	LRESULT OnColumnClick(int id, LPNMHDR nmh, BOOL& handled);
	LRESULT OnItemChanged(int id, LPNMHDR nmh, BOOL& handled);
	LRESULT OnGetDispInfo(int id, LPNMHDR nmh, BOOL& handled);
	LRESULT OnLButtonDblClk(int id, LPNMHDR nmh, BOOL& handled);
	LRESULT OnUpdateHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	void OnSelect(memBlock* block);
	void OnSelect(memStackEntry* stack);
	BOOL OnEraseBkgnd(CDCHandle) {return TRUE;}
 	LRESULT OnPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCustomDraw);
	LRESULT OnItemPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCustomDraw);
 	LRESULT OnSubItemPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCustomDraw);

	virtual void OnAlloc(memBlock* block);
	virtual void OnFree(memBlock* block);
	virtual void OnNextFrame();
	virtual void OnUpdate();

private:
	void Select(memStackFlatEntry* entry);
	memStackFlatEntry* Entry(memStackEntry* stack);
	void SortList(int i);

	CHorSplitterWindow	m_splitter1;
	CHorSplitterWindow	m_splitter2;
	CCaptionedWnd		m_childCaption;
	CCaptionedWnd		m_parentCaption;
	CContainedWindowT<CListViewCtrl> m_list[3];
	int					m_sortkey[3];
	sysCriticalSectionToken m_Cs;
	memStackFlatEntry* m_Selected;
	std::vector<memStackFlatEntry*> m_flatlist;
	stdext::hash_map<const char*, memStackFlatEntry*> m_flatmap;
};

#endif // MEM_STACK_LIST_VIEW_H
