#pragma once

#include <afxmsg_.h>

#include "resource.h"

#include "MemStack.h"

// CFilterDlg dialog

class memHeap;

class CFilterDlg : public CDialogImpl<CFilterDlg>
{
	//	DECLARE_DYNAMIC(CFilterDlg)

public:
	CFilterDlg(std::vector<memHeap*> *heaps);

	//	CFilterDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFilterDlg();

	// Dialog Data
	enum { IDD = IDD_FILTER };

	size_type m_Address;

	BEGIN_MSG_MAP(CFilterDlg)
		MSG_WM_HSCROLL(OnFrameChange)
		COMMAND_ID_HANDLER(IDOK, OnOKCancel)
		COMMAND_ID_HANDLER(IDCANCEL, OnOKCancel)
	END_MSG_MAP()

protected:
	LRESULT OnOKCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

public:
	void OnFrameChange(int wParam, short wParam2, HWND wnd);


private:
	std::vector<memHeap*> *m_Heaps;
	int m_StartFrame;
	int m_EndFrame;

};
