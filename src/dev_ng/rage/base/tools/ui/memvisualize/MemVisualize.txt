Project MemVisualize
Template wtl_configurations.xml

ConfigurationType exe

IncludePath WTL
IncludePath $(RAGE_DIR)\base\src

Files {
	AboutDlg.cpp
	AboutDlg.h
	AtlOpengl.h
	Main.cpp
	MainFrm.cpp
	MainFrm.h
	MemConnection.cpp
	MemConnection.h
	MemHeap.cpp
	MemHeap.h
	MemListview.cpp
	MemListView.h
	MemStack.cpp
	MemStack.h
	MemStackListView.cpp
	MemStackListView.h
	MemSymFile.cpp
	MemSymFile.h
	MemTags.cpp
	MemTags.h
	MemTreeView.cpp
	MemTreeView.h
	MemView.cpp
	MemView.h
	MemWnd.cpp
	MemWnd.h
	Resource.h
	Stdafx.cpp
	Stdafx.h
	TreeListCtrl.cpp
	TreeListCtrl.h
	TreeListWnd.cpp
	TreeListWnd.h

Folder "Resource Files" {

	res/memvisualize.ico
	res/memvisualizeDoc.ico
	res/toolbar.bmp
}
}
