#include "stdafx.h"
#include "resource.h"
#include "mainfrm.h"
#include "system/param.h"
#include "memTags.h"

CAppModule _Module;

int Run(LPTSTR /*lpstrCmdLine*/ = NULL, int nCmdShow = SW_SHOWDEFAULT)
{
	CMessageLoop theLoop;
	_Module.AddMessageLoop(&theLoop);

	CMainFrame wndMain;
	HWND hWnd = wndMain.CreateEx();

	if (hWnd == NULL)
	{
		ATLTRACE(_T("Main window creation failed!\n"));
		return 0;
	}

	wndMain.ShowWindow(nCmdShow);

	int nRet = theLoop.Run();

	_Module.RemoveMessageLoop();
	return nRet;
}

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/, LPTSTR lpstrCmdLine, int nCmdShow)
{
	HRESULT hRes = ::CoInitialize(NULL);
// If you are running on NT 4.0 or higher you can use the following call instead to 
// make the EXE free threaded. This means that calls come in on a random RPC thread.
//	HRESULT hRes = ::CoInitializeEx(NULL, COINIT_MULTITHREADED);
	ATLASSERT(SUCCEEDED(hRes));

	const char* psz = strstr(lpstrCmdLine, "--history");
	if (psz)
	{
		memTags::SetUseHistory(true);
		*lpstrCmdLine = NULL;
	}
	else
	{
		psz = strstr(lpstrCmdLine, "--nohistory");
		if (psz)
		{
			memTags::SetUseHistory(false);
			*lpstrCmdLine = NULL;
		}
	}

	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
	::DefWindowProc(NULL, 0, 0, 0L);

	AtlInitCommonControls(ICC_BAR_CLASSES);	// add flags to support other controls

	hRes = _Module.Init(NULL, hInstance);
	ATLASSERT(SUCCEEDED(hRes));

	int nRet = Run(lpstrCmdLine, nCmdShow);

	_Module.Term();
	::CoUninitialize();

	return nRet;
}

// SM: No idea why this is needed, but I get link errors in debug without it...
#ifdef _DEBUG
void* operator new(size_t size, const char*, int)
{
	return new unsigned char[size];
}

void* operator new[](size_t size, const char*, int )
{
	return new unsigned char[size];
}

void* operator new[](size_t size, size_t align, const char*, int)
{
	return _aligned_malloc(size, align);
}
#endif // _DEBUG
