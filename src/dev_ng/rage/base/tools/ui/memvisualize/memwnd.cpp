#include "stdafx.h"
#include "resource.h"
#include "memview.h"
#include "memwnd.h"
#include "MemTags.h"

void memWnd::OnFinalMessage(HWND /*hWnd*/)
{
	delete this;
}

LRESULT memWnd::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
#if ENABLE_ALLOCATION_LIST_VIEW
	m_hWndClient = m_horsplitter.Create(m_hWnd);

	m_splitter.Create(m_horsplitter);
	m_list.Create(m_horsplitter, rcDefault, NULL, LVS_SINGLESEL | LVS_OWNERDATA | LVS_REPORT | WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, WS_EX_CLIENTEDGE);
	m_horsplitter.SetSplitterPanes(m_splitter, m_list);
	m_horsplitter.SetSplitterPosPct(80);	
#else
	m_hWndClient = m_splitter.Create(m_hWnd);
#endif

	m_view.Create(m_splitter, rcDefault, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_VSCROLL, WS_EX_CLIENTEDGE);
	m_splitter2.Create(m_splitter);
	m_tree.Create(m_splitter2, rcDefault, NULL, TVS_LINESATROOT | TVS_SHOWSELALWAYS | TVS_HASBUTTONS | TVS_HASLINES | TVS_FULLROWSELECT | 
		WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, WS_EX_CLIENTEDGE);
	m_stackview.Create(m_splitter2); 
	m_splitter.SetSplitterPanes(m_view, m_splitter2);
	m_splitter2.SetSplitterPanes(m_tree, m_stackview);
	m_splitter.SetSplitterPosPct(25);
	m_splitter2.SetSplitterPosPct(33);

	bHandled = FALSE;
	return 1;
}

LRESULT memWnd::OnForwardMsg(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	LPMSG pMsg = (LPMSG)lParam;

	if(CMDIChildWindowImpl<memWnd>::PreTranslateMessage(pMsg))
		return TRUE;

	return m_view.PreTranslateMessage(pMsg);
}

LRESULT memWnd::OnFileSave(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	memStackEntry* root = &m_view.m_Heap->m_Root;
	CFileDialog filesave(FALSE, ".txt", root->m_Name,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"Stacked alloc (*.txt)\0*.txt\0CSV (*.csv)\0*.csv\0All Files (*.*)\0*.*\0");
	if (filesave.DoModal() == IDOK)
	{
		const char *ext = strrchr(filesave.m_szFileName,'.') + 1;
		const bool isCSV = ( ext[0] == 'c' && ext[1] == 's' && ext[2] == 'v' );
		FILE* out = fopen(filesave.m_szFileName, "w");
		if (out)
		{
			isCSV ? m_view.m_Heap->SaveAsCsv(out) : m_view.m_Heap->SaveAsTxt(out);
		}
		fclose(out);
	}
	return 0;
}

LRESULT memWnd::OnMarkConnectionLost(UINT, WPARAM, LPARAM lParam, BOOL& handled)
{
	// Is this one meant for us?
	if (lParam == (LPARAM) m_heap)
	{
		if (::IsWindow(m_hWnd))
		{
			// It is. Update ourselves.
			char title[256];
			::GetWindowText(m_hWnd, title, sizeof(title));

			if (strstr(title, "(DISC") == NULL)
			{
				safecat(title, " (DISCONNECTED)");
				::SetWindowText(m_hWnd, title);
			}
		}
	}

	handled = TRUE;
	return 0;
}

bool memWnd::ReadFromMvz(FILE *fp, int version, const atArray<memStackEntry *, 0, u32> &stackReferenceArray)
{
	// Older file formats don't even support saving memWnd data.
	if (version < 2)
	{
		return true;
	}

	if (!(m_tree.ReadFromMvz(fp, version, stackReferenceArray)))
	{
		return false;
	}

	return m_stackview.ReadFromMvz(fp, version, stackReferenceArray);
}

void memWnd::SaveAsMvz(FILE* out, stdext::hash_map<memStackEntry *, int> &stackReferenceMap)
{
	m_tree.SaveAsMvz(out, stackReferenceMap);
	m_stackview.SaveAsMvz(out, stackReferenceMap);
}
