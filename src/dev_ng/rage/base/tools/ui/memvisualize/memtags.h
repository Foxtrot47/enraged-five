/////////////////////////////////////////////////////////////////////////////

#pragma once

class memTags
{
public:
	static void ReadTags(const char * filename);
	static COLORREF GetColor(const char* tag, COLORREF default);
	static COLORREF GetColorFromIndex(int c);

	static bool UseHistory() { return ms_UseHistory;}
	static void SetUseHistory(bool useHistory) { ms_UseHistory = useHistory;}

	static bool UseBuckets() { return ms_UseBuckets;}
	static void SetUseBuckets(bool useBuckets) { ms_UseBuckets = useBuckets;}
	static const char *GetbucketName(int c);

	static bool GetShowLocked() { return ms_ShowLocked;}
	static void SetShowLocked(bool showLocked) { ms_ShowLocked = showLocked;}

	static bool GetShowExternal() { return ms_ShowExternal;}
	static void SetShowExternal(bool showExternal) { ms_ShowExternal = showExternal;}

	static bool GetShowDefrag() { return ms_ShowDefrag;}
	static void SetShowDefrag(bool showDefrag) { ms_ShowDefrag = showDefrag;}

	static bool GetShowMoved() { return ms_ShowMoved;}
	static void SetShowMoved(bool showMoved) { ms_ShowMoved = showMoved;}

	static bool ms_UseHistory;	
	static bool ms_UseBuckets;	
	static bool ms_ShowLocked;
	static bool ms_ShowExternal;
	static bool ms_ShowDefrag;
	static bool ms_ShowMoved;
	static int ms_FocusBucket;
};