#include <stdafx.h>
#include <algorithm>
#include "memstacklistview.h"

class memStackFlatEntry
{
public:
	memStackFlatEntry() {}

	memStackFlatEntry(const char* name, COLORREF colour)
	:	m_Name(name)
	,	m_Colour(colour)
	,	m_Size(0)
	,	m_Allocs(0)
	,	m_TotalAllocs(0)
	,	m_AllocsPerFrame(0)
	,	m_FrameLastAllocated(0)
	,	m_TotalAllocsAtLastFrame(0)
	{}

	void SaveAsMvz(FILE *out, stdext::hash_map<memStackFlatEntry *, int> &flatEntryReference);

	bool ReadFromMvz(FILE *fp, int version, memStackFlatEntry **base);

	void Alloc(memBlock* block);
	void Free(memBlock* block);
	const char*		m_Name;
	COLORREF		m_Colour;
	size_type		m_Size;
	u32				m_Allocs;
	u32				m_TotalAllocs;
	float			m_AllocsPerFrame;
	u32				m_FrameLastAllocated;
	u32				m_TotalAllocsAtLastFrame;

	std::vector<memStackFlatEntry*>	m_Parents;
	std::vector<memStackFlatEntry*>	m_Children;
};

void memStackFlatEntry::SaveAsMvz(FILE *out, stdext::hash_map<memStackFlatEntry *, int> &flatEntryReference)
{
	WriteString(m_Name, out);
	STORE_DATA(m_Colour, out);
	STORE_DATA(m_Size, out);
	STORE_DATA(m_Allocs, out);
	STORE_DATA(m_TotalAllocs, out);
	STORE_DATA(m_AllocsPerFrame, out);
	STORE_DATA(m_FrameLastAllocated, out);
	STORE_DATA(m_TotalAllocsAtLastFrame, out);

	short parentCount = (short) m_Parents.size();
	STORE_DATA(parentCount, out);

	for (short x=0; x<parentCount; x++)
	{
		int parentIndex = flatEntryReference[m_Parents[x]];
		STORE_DATA(parentIndex, out);
	}

	short childCount = (short) m_Children.size();
	STORE_DATA(childCount, out);

	for (short x=0; x<childCount; x++)
	{
		int childIndex = flatEntryReference[m_Children[x]];
		STORE_DATA(childIndex, out);
	}
}

bool memStackFlatEntry::ReadFromMvz(FILE *fp, int version, memStackFlatEntry **base)
{
	char name[256];
	ReadString(name, sizeof(name), fp);

	m_Name = rage_new char[strlen(name) + 1];		// TODO - we're leaking here.
	strcpy((char *) m_Name, name);

	READ_DATA(m_Colour, fp);
	READ_DATA(m_Size, fp);
	READ_DATA(m_Allocs, fp);
	READ_DATA(m_TotalAllocs, fp);
	READ_DATA(m_AllocsPerFrame, fp);
	READ_DATA(m_FrameLastAllocated, fp);
	READ_DATA(m_TotalAllocsAtLastFrame, fp);

	short parentCount;
	READ_DATA(parentCount, fp);

	m_Parents.reserve(parentCount);

	for (int x=0; x<parentCount; x++)
	{
		int parentIndex;
		READ_DATA(parentIndex, fp);
		m_Parents.push_back(base[parentIndex]);
	}

	short childCount;
	READ_DATA(childCount, fp);

	m_Children.reserve(childCount);

	for (int x=0; x<childCount; x++)
	{
		int childIndex;
		READ_DATA(childIndex, fp);
		m_Children.push_back(base[childIndex]);
	}

	return true;
}


memStackListView::~memStackListView()
{
	if (m_Heap) {
		m_Heap->m_Cs.Lock();
		m_Heap->LockViewListForReading();
	}

	for (std::vector< memStackFlatEntry* >::iterator it = m_flatlist.begin(); it != m_flatlist.end(); ++it)
		delete *it;

	m_flatmap.clear();

	if (m_Heap) {
		m_Heap->UnlockViewListForReading();
		m_Heap->m_Cs.Unlock();
	}
}

int memStackListView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	m_Selected = 0;

	m_splitter1.Create(m_hWnd);
	m_splitter2.Create(m_splitter1);

	int style = /*LVS_OWNERDRAWFIXED |*/ LVS_SINGLESEL | LVS_SHOWSELALWAYS | LVS_OWNERDATA | LVS_REPORT | WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
	m_list[0].Create(this, 1, m_splitter1, rcDefault, NULL, style, /*LVS_EX_GRIDLINES |*/ WS_EX_CLIENTEDGE, 1);

	m_childCaption.Create(m_splitter2);
	m_childCaption.SetCaption("Children");
	m_list[1].Create(this, 1, m_childCaption, rcDefault, NULL, style, /*LVS_EX_GRIDLINES |*/ WS_EX_CLIENTEDGE, 2);
	m_childCaption.SetChild(m_list[1]);

	m_parentCaption.Create(m_splitter2);	
	m_parentCaption.SetCaption("Parents");
	m_list[2].Create(this, 1, m_parentCaption, rcDefault, NULL, style, /*LVS_EX_GRIDLINES |*/ WS_EX_CLIENTEDGE, 3);
	m_parentCaption.SetChild(m_list[2]);

	m_splitter1.SetSplitterPanes(m_list[0], m_splitter2);
	m_splitter1.SetSplitterPosPct(40);
	m_splitter2.SetSplitterPanes(m_childCaption, m_parentCaption);
	m_splitter2.SetSplitterPosPct(50);

	for(int i=0; i<3; ++i)
	{
		m_sortkey[i] = 1;
		m_list[i].InsertColumn(0, "Name", LVCFMT_LEFT, 400);
		m_list[i].InsertColumn(1, "Bytes", LVCFMT_RIGHT, 80);
		m_list[i].InsertColumn(2, "Allocs", LVCFMT_RIGHT, 80);
		m_list[i].InsertColumn(3, "Total allocs", LVCFMT_RIGHT, 80);
		m_list[i].InsertColumn(4, "Allocs/s", LVCFMT_RIGHT, 80);
		m_list[i].SetExtendedListViewStyle(LVS_EX_FULLROWSELECT|LVS_EX_DOUBLEBUFFER|LVS_EX_GRIDLINES);
	}

	return 0;
}

void memStackListView::OnSize(UINT nType, CSize size)
{
	CRect rect;
	GetClientRect(&rect);
	m_splitter1.MoveWindow(rect);
}

LRESULT memStackListView::OnGetDispInfo(int ctrlId, LPNMHDR pnmh, BOOL& handled)
{
	LV_DISPINFO* dispinfo = (LV_DISPINFO*)pnmh;
	int id = dispinfo->item.iItem;
	int col = dispinfo->item.iSubItem;
	static char text[2048];
	dispinfo->item.pszText = text;
	dispinfo->item.cchTextMax = 2048;
	text[0] = 0;

	m_Cs.Lock();
	memStackFlatEntry* entry = 0;
	if (ctrlId == 1)
		entry = m_flatlist[id];
	else if (ctrlId == 2 && m_Selected && size_t(id) < m_Selected->m_Children.size())
		entry = m_Selected->m_Children[id];
	else if (ctrlId == 3 && m_Selected && size_t(id) < m_Selected->m_Parents.size())
		entry = m_Selected->m_Parents[id];
	m_Cs.Unlock();

	if (!entry)
		return 0;

	if (col == 0)
	{
		dispinfo->item.pszText = (LPSTR)entry->m_Name;
	}
	else
	{
		size_type i = 0;
		if (col == 1)
			i = entry->m_Size;
		else if (col == 2)
			i = entry->m_Allocs;
		else if (col == 3)
			i = entry->m_TotalAllocs;
		else if (col == 4)
			i = int(entry->m_AllocsPerFrame*30.0f+.5f);
		static char buf[256];
		char* dest = buf + 255;
		*dest = 0;
		int j = -1;
		do
		{
			if (++j == 3)
			{
				j = 0;
				*--dest = ',';
			}
			*--dest = '0'+(i%10);
			i /= 10;
		}
		while (i);
		dispinfo->item.pszText = dest;
	}
	return 0;
}

memStackFlatEntry* memStackListView::Entry(memStackEntry* stack)
{
	if (!stack)
		return 0;
	memStackFlatEntry*& flat = m_flatmap[stack->m_Name];
	if (!flat)
	{
		COLORREF colour = RGB(128,128,128);
		if (stack->m_Parent && stack->m_Colour != stack->m_Parent->m_Colour)
			colour = stack->m_Colour;
		flat = new memStackFlatEntry(stack->m_Name, colour);
		m_Cs.Lock();
		m_flatlist.push_back(flat);
		m_Cs.Unlock();
	}
	return flat;
}

void memStackListView::OnAlloc(memBlock* block)
{
	m_Dirty = true;
	memStackEntry *stack = block->m_Stack;
	m_Cs.Lock();
	memStackFlatEntry* entry = Entry(stack);
	while (1)
	{
		entry->m_FrameLastAllocated = m_Heap->m_Frame;
 		entry->Alloc(block);
		do {
			stack = stack->m_Parent;
		} while (stack && (!stack->m_Name[0] ||
			strstr(stack->m_Name, "sysIpcThreadWrapper") ||
			strstr(stack->m_Name, "CommonMain") ||
			strstr(stack->m_Name, "rage::ExceptMain") ||
			strstr(stack->m_Name, "mainCRTStartup") ||
			strstr(stack->m_Name, "XapiThreadStartup")));
		if (!stack)
			break;
		memStackFlatEntry* parententry = Entry(stack);
		if (entry != parententry && std::find(entry->m_Parents.begin(), entry->m_Parents.end(), parententry) == entry->m_Parents.end())
		{
			entry->m_Parents.push_back(parententry);
			parententry->m_Children.push_back(entry);
		}
		entry = parententry;
	}
	m_Cs.Unlock();
}

struct sortlist
{
	sortlist(int key) : m_key(key) {}
	bool operator()(const memStackFlatEntry* lhs, const memStackFlatEntry* rhs) const
	{
		switch (m_key)
		{
		case 0:	return _stricmp(lhs->m_Name, rhs->m_Name) < 0;
		case 1: return lhs->m_Size > rhs->m_Size;
		case 2: return lhs->m_Allocs > rhs->m_Allocs;
		case 3: return lhs->m_TotalAllocs > rhs->m_TotalAllocs;
		case 4: return lhs->m_AllocsPerFrame > rhs->m_AllocsPerFrame;
		default: return 0;
		}
	}
	int m_key;
};

void memStackListView::OnSelect(memBlock* block)
{
	if (block)
		OnSelect(block->m_Stack);
}

void memStackListView::OnSelect(memStackEntry* stack)
{
	if (stack)
		Select(Entry(stack));
}

LRESULT memStackListView::OnItemChanged(int id, LPNMHDR nmh, BOOL& handled)
{
	LPNMLISTVIEW pnmv = (LPNMLISTVIEW)nmh; 
	if ((pnmv->uChanged & LVIF_STATE) && (pnmv->uNewState & LVIS_SELECTED))
	{
		if (id == 1)
		{
			m_Cs.Lock();
			memStackFlatEntry* entry = m_flatlist[pnmv->iItem];
			m_Cs.Unlock();
			Select(entry);
		}
	}
	return 0;
}

void memStackListView::Select(memStackFlatEntry* entry)
{	
	m_Selected = entry;
	int oldSelIdx = m_list[0].GetSelectedIndex();
	int childCount = 0;
	int parentCount = 0;
	int idx = -1;
	if (entry)
	{
		m_Cs.Lock();
		idx = (int) (find(m_flatlist.begin(), m_flatlist.end(), entry) - m_flatlist.begin());
		std::stable_sort(entry->m_Children.begin(), entry->m_Children.end(), sortlist(m_sortkey[1]));
		std::stable_sort(entry->m_Parents.begin(), entry->m_Parents.end(), sortlist(m_sortkey[2]));
		childCount = (int) entry->m_Children.size();
		parentCount = (int) entry->m_Parents.size();
		m_Cs.Unlock();	
	}
	m_list[1].SetItemCountEx(childCount, LVSICF_NOSCROLL);
	m_list[2].SetItemCountEx(parentCount, LVSICF_NOSCROLL);
	if (oldSelIdx != idx)
		m_list[0].SelectItem(idx);
}

LRESULT memStackListView::OnLButtonDblClk(int id, LPNMHDR nmh, BOOL& handled)
{
	if (!m_Selected || id < 2)
		return 0;
	LPNMITEMACTIVATE lpnmitem = (LPNMITEMACTIVATE)nmh;
	int item = lpnmitem->iItem;
	m_Cs.Lock();
	memStackFlatEntry* entry = 0;
	if (id == 2 && unsigned(item) < m_Selected->m_Children.size())
		entry = m_Selected->m_Children[item];
	else if (id == 3 && unsigned(item) < m_Selected->m_Parents.size())
		entry = m_Selected->m_Parents[item];
	m_Cs.Unlock();
	if (entry)
		Select(entry);
	return 0;
}

LRESULT memStackListView::OnColumnClick(int id, LPNMHDR nmh, BOOL& handled)
{
	LPNMLISTVIEW pnmv = (LPNMLISTVIEW)nmh;
	m_Cs.Lock();
	m_sortkey[id-1] = pnmv->iSubItem;
	m_Cs.Unlock();
	SortList(id-1);
	return 0;
}

void memStackListView::SortList(int i)
{
	std::vector<memStackFlatEntry*>* lst = 0;
	if (i == 0)
		lst = &m_flatlist;
	else if (i == 1 && m_Selected)
		lst = &m_Selected->m_Children;
	else if (i == 2 && m_Selected)
		lst = &m_Selected->m_Parents;
	else
		return;
	
	memStackFlatEntry* selitem = 0;
	int selected = m_list[i].GetSelectedIndex();
	BOOL wasSelVisible = FALSE;
	if (unsigned(selected) < lst->size())
	{
		selitem = (*lst)[selected];
		CRect itemrect, clientrect;
		GetClientRect(&clientrect);
		m_list[i].GetItemRect(selected, &itemrect, LVIR_BOUNDS);
		wasSelVisible = (itemrect.bottom >= 0 && itemrect.top < clientrect.Height());
	}

	m_Cs.Lock();
	std::stable_sort(lst->begin(), lst->end(), sortlist(m_sortkey[i]));
	if (selitem)
		selected = (int) (std::find(lst->begin(), lst->end(), selitem) - lst->begin());
	m_Cs.Unlock();

	m_list[i].SetRedraw(FALSE);
	if (m_list[i].GetItemCount() != lst->size())
		m_list[i].SetItemCountEx((int) lst->size(), LVSICF_NOSCROLL);
	if (selitem)
	{
		m_list[i].SetItemState(selected, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		if (wasSelVisible)
			m_list[i].EnsureVisible(selected, FALSE);
	}
	m_list[i].SetRedraw(TRUE);
}

void memStackListView::OnNextFrame()
{
	m_Cs.Lock();
	for(std::vector<memStackFlatEntry*>::iterator it = m_flatlist.begin();
		it != m_flatlist.end(); ++it)
	{
		memStackFlatEntry* pEntry = *it;
		u32 allocsThisFrame = pEntry->m_TotalAllocs - pEntry->m_TotalAllocsAtLastFrame;
		pEntry->m_TotalAllocsAtLastFrame = pEntry->m_TotalAllocs;
		static const float a = .99667f; //e^-1/300 (10 seconds at 30hz)
		pEntry->m_AllocsPerFrame = pEntry->m_AllocsPerFrame * a + allocsThisFrame * (1.0f - a);
	}
	m_Cs.Unlock();
}

void memStackListView::OnUpdate()
{
	if (m_Dirty && !m_DirtyPosted)
	{
		m_DirtyPosted = true;
		PostMessage(WM_UPDATE);
	}
}

LRESULT memStackListView::OnUpdateHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	m_DirtyPosted = false;

	CRect clientrect;
	GetClientRect(&clientrect);
		for(int i=0; i<3; ++i)
			SortList(i);

	m_Dirty = false;
	return 0;
}

void memStackListView::OnFree(memBlock* block)
{
	m_Dirty = true;
	memStackEntry *stack = block->m_Stack;
	m_Cs.Lock();
	while (stack)
	{
		stdext::hash_map<const char*, memStackFlatEntry*>::iterator it = m_flatmap.find(stack->m_Name);
		if (it != m_flatmap.end())
		{
	 		memStackFlatEntry* entry = it->second;
			entry->Free(block);
		}
		stack = stack->m_Parent;
	}
	m_Cs.Unlock();
}

void memStackFlatEntry::Alloc(memBlock* block)
{
	m_Size += block->m_End - block->m_Start;
	m_TotalAllocs += 1;
	m_Allocs += 1;
}
void memStackFlatEntry::Free(memBlock* block)
{
	m_Size -= block->m_End - block->m_Start;
	m_Allocs -= 1;
}

LRESULT memStackListView::OnPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCustomDraw)
{
	return CDRF_NOTIFYITEMDRAW;
}
LRESULT memStackListView::OnItemPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCustomDraw)
{
	return CDRF_NOTIFYSUBITEMDRAW;
}
LRESULT memStackListView::OnSubItemPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCustomDraw)
{	
	LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)lpNMCustomDraw;

	if (lplvcd->iSubItem == 4)
	{
		std::vector<memStackFlatEntry*>* lst = 0;
		DWORD_PTR id = lplvcd->nmcd.dwItemSpec;
		if (idCtrl == 1)
			lst = &m_flatlist;
		else if (idCtrl == 2 && m_Selected)
			lst = &m_Selected->m_Children;
		else if (idCtrl == 3 && m_Selected)
			lst = &m_Selected->m_Parents;
		sysCriticalSection cs(m_Cs);
		if (lst && unsigned(id) < lst->size())
		{
			memStackFlatEntry* stack = (*lst)[id];
			int lastAlloc = m_Heap->m_Frame - stack->m_FrameLastAllocated;
			if (lastAlloc < 128)
			{
				int r = 255-lastAlloc*2;
				if (r < 0)
					r = 0;
				lplvcd->clrText = r;
				return CDRF_NEWFONT;
			}
		}
	}

	if (lplvcd->iSubItem == 1)
	{
		lplvcd->clrTextBk = GetSysColor(COLOR_WINDOW);
		return CDRF_NEWFONT;
	}
	else if (lplvcd->iSubItem != 0)
	{
		return CDRF_DODEFAULT;
	}

	int result = CDRF_DODEFAULT;
	std::vector<memStackFlatEntry*>* lst = 0;
	DWORD_PTR id = lplvcd->nmcd.dwItemSpec;
	m_Cs.Lock();
	if (idCtrl == 1)
		lst = &m_flatlist;
	else if (idCtrl == 2 && m_Selected)
		lst = &m_Selected->m_Children;
	else if (idCtrl == 3 && m_Selected)
		lst = &m_Selected->m_Parents;
	if (lst && unsigned(id) < lst->size())
	{
		memStackFlatEntry* stack = (*lst)[id];
		if (stack->m_Colour != RGB(128,128,128))
		{
			lplvcd->clrTextBk = stack->m_Colour;
			result = CDRF_NEWFONT;
		}
	}
	m_Cs.Unlock();
	return result;
}

void memStackListView::SaveAsMvz(FILE* out, const stdext::hash_map<memStackEntry *, int> & /*stackReferenceMap*/)
{
	if (!(::IsWindow(m_hWnd)))
	{
		int entryCount = 0;
		STORE_DATA(entryCount, out);
		STORE_DATA(entryCount, out);

		int selected = -1;
		STORE_DATA(selected, out);

		return;
	}

	int entryCount = (int) m_flatlist.size();
	STORE_DATA(entryCount, out);

	stdext::hash_map<memStackFlatEntry *, int> flatEntryReference;

	for (int x=0; x<entryCount; x++)
	{
		flatEntryReference[m_flatlist[x]] = x;
	}


	for (int x=0; x<entryCount; x++)
	{
		memStackFlatEntry &entry = *m_flatlist[x];
		entry.SaveAsMvz(out, flatEntryReference);
	}

	int mapSize = (int) m_flatmap.size();
	STORE_DATA(mapSize, out);

	stdext::hash_map<const char*, memStackFlatEntry*>::iterator it = m_flatmap.begin();

	while (it != m_flatmap.end())
	{
		WriteString(it->first, out);

		int valueIndex = flatEntryReference[it->second];
		STORE_DATA(valueIndex, out);

		++it;
	}

	int selIndex = -1;

	if (m_Selected)
	{
		selIndex = flatEntryReference[m_Selected];
	}

	STORE_DATA(selIndex, out);
}

bool memStackListView::ReadFromMvz(FILE *fp, int version, const atArray<memStackEntry *, 0, u32> &stackReferenceArray)
{
	int entryCount;
	READ_DATA(entryCount, fp);

	m_flatlist.reserve(entryCount);

	for (int x=0; x<entryCount; x++)
	{
		m_flatlist.push_back(rage_new memStackFlatEntry());
	}

	for (int x=0; x<entryCount; x++)
	{
		memStackFlatEntry *entry = m_flatlist[x];

		if (!(entry->ReadFromMvz(fp, version, &m_flatlist[0])))
		{
			return false;
		}
	}

	int mapSize;
	READ_DATA(mapSize, fp);

	for (int x=0; x<mapSize; x++)
	{
		char key[256];
		ReadString(key, sizeof(key), fp);

		int value;
		READ_DATA(value, fp);

		m_flatmap[key] = m_flatlist[value];
	}

	int selIndex;
	READ_DATA(selIndex, fp);

	m_Selected = (selIndex == -1) ? NULL : m_flatlist[selIndex];

	// After we're done, we need to call SortList() on all lists so they repopulate everything.
	BOOL handled;
	OnUpdateHandler(0, 0, 0, handled);

	return true;
}


//////////////////////////////////////////////////////////////////////////

int CCaptionedWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	SetMsgHandled(FALSE);
	LOGFONT	font;
	SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(font), &font, 0);
	m_hFont.CreateFontIndirect(&font);
	m_height = 2 - font.lfHeight;
	return 0;
}
void CCaptionedWnd::SetCaption(const char* caption)
{
	m_caption = caption;
}
void CCaptionedWnd::SetChild(HWND child)
{
	m_hWndChild = child;
}
BOOL CCaptionedWnd::OnEraseBkgnd(CDCHandle dc)
{
	return TRUE;
}
LRESULT CCaptionedWnd::OnPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	CPaintDC dc(m_hWnd);
	CRect rect;
	GetClientRect(rect);
	rect.bottom = m_height;
	dc.SetBkColor(GetSysColor(COLOR_3DFACE));
	dc.SetTextColor(GetSysColor(COLOR_BTNTEXT));
	HFONT f = dc.SelectFont(m_hFont);
	dc.ExtTextOut(0, 0, ETO_OPAQUE, rect, m_caption, (UINT) strlen(m_caption), 0);
	dc.SelectFont(f);
	return 0;
}
void CCaptionedWnd::OnSize(UINT nType, CSize size)
{
	if (m_hWndChild)
	{
		CRect rect;
		GetClientRect(rect);
		rect.top += m_height;
		m_hWndChild.MoveWindow(rect);
	}
}
