#include "stdafx.h"
#include "memstack.h"
#include "memsymfile.h"
#include "memtags.h"



memStackEntry::memStackEntry(const char* name, COLORREF colour, memStackEntry* parent)
{
	m_Name = name;
	m_Size = 0;
	m_Allocs = 0;
	m_TotalAllocs = 0;
	m_AllocsPerFrame = 0;
	m_TotalAllocsAtLastFrame = 0;
	m_Buckets = 0;

	m_Child = m_Next = 0;
	m_Parent = parent;

	m_Colour = memTags::GetColor(name, colour);
}

memStackEntry* memStackEntry::Child(const char* name)
{
	if (strstr(name, "operator new") ||
		strstr(name, "rage::sysMem") ||
		strstr(name, "rage::pgRscBuilder::") ||
		strstr(name, "physical_new") ||
		strstr(name, "operator_new"))
		return 0;
	memStackEntry* child = m_Child;
	while (child)
	{
		if (child->m_Name == name)
			return child;
		child = child->m_Next;
	}	
	child = new memStackEntry(name, m_Colour, this);
	child->m_Next = m_Child;	
	m_Child = child;
	return child;
}

void memStackEntry::NextFrame()
{
	u32 allocsThisFrame = m_TotalAllocs - m_TotalAllocsAtLastFrame;
	m_TotalAllocsAtLastFrame = m_TotalAllocs;
	static const float a = .99667f; //e^-1/300 (10 seconds at 30hz)
	m_AllocsPerFrame = m_AllocsPerFrame * a + allocsThisFrame * (1.0f - a);

	for(memStackEntry* child = m_Child; child; child = child->m_Next)
		child->NextFrame();
}



void memStackEntry::Highlight(bool val)
{
	if (val)
		m_Colour |= 1u<<31;
	else
		m_Colour &= ~(1u<<31);
	for(memStackEntry* child = m_Child; child; child = child->m_Next)
		child->Highlight(val);	
}

const int HAS_NEXT = ( 1 << 0);
const int HAS_CHILD = ( 1 << 1);

void memStackEntry::BuildTree(s32 &referenceIndex, stdext::hash_map<memStackEntry *, s32> &referenceMap)
{
	referenceMap[this] = referenceIndex++;

	u8 familyFlags = 0;

	if (m_Next)
		familyFlags |= HAS_NEXT;

	if (m_Child)
		familyFlags |= HAS_CHILD;

	if (m_Next)
	{
		m_Next->BuildTree(referenceIndex, referenceMap);
	}

	if (m_Child)
	{
		m_Child->BuildTree(referenceIndex, referenceMap);
	}
}

void memStackEntry::SaveAsMvz(FILE *out, s32 &referenceIndex, stdext::hash_map<memStackEntry *, s32> &referenceMap)
{
	WriteString(m_Name, out);
	STORE_DATA(m_Colour, out);
	STORE_DATA(m_Size, out);
	STORE_DATA(m_Allocs, out);
	STORE_DATA(m_TotalAllocs, out);
	STORE_DATA(m_AllocsPerFrame, out);
	STORE_DATA(m_TotalAllocsAtLastFrame, out);
	STORE_DATA(m_Buckets, out);

	referenceMap[this] = referenceIndex++;

	u8 familyFlags = 0;
	
	if (m_Next)
		familyFlags |= HAS_NEXT;

	if (m_Child)
		familyFlags |= HAS_CHILD;

	fwrite(&familyFlags, sizeof(familyFlags), 1, out);

	if (m_Next)
	{
		m_Next->SaveAsMvz(out, referenceIndex, referenceMap);
	}

	if (m_Child)
	{
		m_Child->SaveAsMvz(out, referenceIndex, referenceMap);
	}
}

void memStackEntry::ReadFromMvz(FILE *fp, int version, atArray<memStackEntry *, 0, u32> &referenceArray, memStackEntry* parent /*= NULL*/)
{
	atArray<memStackEntry *> siblings;
	atArray<bool> hasChildren;

	memStackEntry *currentSibling = this;

	do 
	{
		siblings.Grow(1024) = currentSibling;

		char tempName[128];
		ReadString(tempName, sizeof(tempName), fp);

		char *finalName = rage_new char[strlen(tempName)+1];
		strcpy(finalName, tempName);
		currentSibling->m_Name = finalName;
		READ_DATA(currentSibling->m_Colour, fp);
		READ_DATA(currentSibling->m_Size, fp);
		READ_DATA(currentSibling->m_Allocs, fp);
		READ_DATA(currentSibling->m_TotalAllocs, fp);
		READ_DATA(currentSibling->m_AllocsPerFrame, fp);
		READ_DATA(currentSibling->m_TotalAllocsAtLastFrame, fp);
		READ_DATA(currentSibling->m_Buckets, fp);

		referenceArray.Grow(512) = currentSibling;

		currentSibling->m_Parent = parent;

		u8 familyFlags;
		fread(&familyFlags, sizeof(familyFlags), 1, fp);

		hasChildren.Grow(1024) = (familyFlags & HAS_CHILD) != 0;
		memStackEntry *next = NULL;

		if (familyFlags & HAS_NEXT)
		{
			next = rage_new memStackEntry();
			next->m_Parent = parent;
		}
		currentSibling->m_Next = next;
		currentSibling = next;
	} while (currentSibling);
	
	int children = hasChildren.GetCount();

	for (int x=(children - 1); x >= 0; --x)
	{
		parent = siblings[x];

		if (hasChildren[x])
		{
			memStackEntry *child = rage_new memStackEntry();
			child->ReadFromMvz(fp, version, referenceArray, parent);
			parent->m_Child = child;
			child->m_Parent = parent;
		}
		else
		{
			parent->m_Child = NULL;
		}
	}
}
