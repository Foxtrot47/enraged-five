//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by memvisualize.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_MDICHILD                    129
#define IDD_ADDRESS_SEARCH              201
#define IDD_FILTER                      202
#define IDD_HISTORY                     203
#define IDC_ADDRESS                     1000
#define IDC_START                       1001
#define IDC_STARTFRAME                  1001
#define IDC_SLIDER2                     1002
#define IDC_ENDFRAME                    1002
#define IDC_STATUS                      1003
#define IDC_HISTORY_TEXT                1004
#define IDC_EDIT2                       1005
#define IDC_HISTORY_ADDRESS             1008
#define IDC_HISTORY_UPDATE              1010
#define IDC_RICHEDIT22                  1013
#define ID_VIEW_USEBUCKETS              32775
#define ID_NEW_ALLOCATIONS_ONLY         32779
#define ID_ALL_ALLOCATIONS              32780
#define ID_VIEW_SHOWDEFRAG              32782
#define ID_ADDRESS_SEARCH               32783
#define ID_BUTTON32784                  32784
#define ID_FILTER                       32784
#define ID_VIEW_SHOWLOCKED              32785
#define ID_VIEW_SHOWEXTERNAL            32786
#define ID_VIEW_SHOWMOVED               32788
#define ID_FILE_SAVE_ALL                32789
#define ID_ADDRESS                      32790
#define ID_VIEW_USEHISTORY              32791
#define ID_VIEW_USEHISTORY32792         32792

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        204
#define _APS_NEXT_COMMAND_VALUE         32793
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
