#include "stdafx.h"
#include "memtags.h"

#include "atl/string.h"
#include "file/stream.h"
#include "file/device.h"

using namespace rage;

static atStringArray s_tags;
static atStringArray s_buckets;
bool memTags::ms_UseBuckets = true;
bool memTags::ms_UseHistory = true;
bool memTags::ms_ShowLocked = true;
bool memTags::ms_ShowExternal = true;
bool memTags::ms_ShowDefrag = true;
bool memTags::ms_ShowMoved = true;
int memTags::ms_FocusBucket = -1;

COLORREF  memTags::GetColorFromIndex(int c)
{
	if (!c--)
	{
		return RGB(128,128,128);
	}	

	float l = .5f;
	float s = (c >= 12) ? .5f : 1.f;
	int h = (c * 30) % 360;

	if (ms_FocusBucket >= 0 && c != ms_FocusBucket)
	{
		l *= 0.33f;
	}

	float r, g, b;
	if (h < 120) {
		r = (120 - h) / 60.0f;
		g = h / 60.0f;
		b = 0;
	} else if (h < 240) {
		r = 0;
		g = (240 - h) / 60.0f;
		b = (h - 120) / 60.0f;
	} else {
		r = (h - 240) / 60.0f;
		g = 0;
		b = (360 - h) / 60.0f;
	}
	r = min(r,1.0f);
	g = min(g,1.0f);
	b = min(b,1.0f);

	r = 2 * s * r + (1 - s);
	g = 2 * s * g + (1 - s);
	b = 2 * s * b + (1 - s);

	if (l < 0.5) {
		r = l * r;
		g = l * g;
		b = l * b;
	} else {
		r = (1 - l) * r + 2 * l - 1;
		g = (1 - l) * g + 2 * l - 1;
		b = (1 - l) * b + 2 * l - 1;
	}

	return RGB(r*255,g*255,b*255);
}

const char *memTags::GetbucketName(int c)
{
	if( c < s_buckets.GetCount() )
	{
		return s_buckets[c].c_str();
	}
	
	return NULL;
}

void PopulateBuffer(fiSafeStream& stream)
{
	char buffer[512];
	while (fgetline(buffer, 512, stream))
	{
		char *bucketName = strrchr(buffer,'-');
		if( bucketName )
		{
			s_buckets.Grow() = atString(bucketName+1);
		}
		else
		{
			s_tags.Grow() = atString(buffer);
		}
	}
}

void memTags::ReadTags(const char * filename)
{
	fiSafeStream s(fiStream::Open(filename, *fiDevice::GetDevice(filename)));
	
	if (s)
	{
		PopulateBuffer(s);
	}
	else
	{
		// Try the hard-coded path
		const char* backup = "X:\\gta5\\tools\\bin\\memvisualize\\memvisualize.dat";
		fiSafeStream s2(fiStream::Open(backup, *fiDevice::GetDevice(backup)));

		if (s2)
		{
			PopulateBuffer(s2);			
		}
	}

	if (s_buckets.empty())
	{
		char sz[64] = {0};
		sprintf(sz, "Unable to read %s.  MemVisualize will shutdown now.", filename);
		::MessageBox(NULL, sz, "ERROR: Invalid I/O", MB_OK);
		::ExitProcess(0);
	}
}

COLORREF memTags::GetColor(const char* tag, COLORREF default)
{
	int i;
	COLORREF c = default;
	for(i=0; i<s_tags.GetCount(); ++i)
	{
		if (strcmp(tag, s_tags[i]) == 0)
		{
			c = GetColorFromIndex(i+1);
			break;
		}
	}

	return c;
}

