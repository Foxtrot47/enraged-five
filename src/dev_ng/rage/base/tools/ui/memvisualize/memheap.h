#ifndef MEM_VISUALIZE_HEAP_H
#define MEM_VISUALIZE_HEAP_H

#include "diag/tracker_remote2.h"
#include "system/criticalsection.h"
#include "system/ipc.h"
#include "system/spinlock.h"
#include "file/handle.h"
#include "memstack.h"
#include <deque>
#include <hash_map>
#include <map>
#include <queue>
#include <set>
#include <vector>

using namespace rage;

#define WM_UPDATE		(WM_USER + 100)

class memStackEntry;
class memSymFile;
class memWnd;

template<typename T, typename Container=std::deque<T> >
class iterable_queue : public std::queue<T,Container>
{
public:
	typedef typename Container::iterator iterator;
	typedef typename Container::const_iterator const_iterator;
	typedef typename Container::reverse_iterator reverse_iterator;
	typedef typename Container::const_reverse_iterator const_reverse_iterator;

	iterator begin() { return this->c.begin(); }
	iterator end() { return this->c.end(); }
	const_iterator begin() const { return this->c.begin(); }
	const_iterator end() const { return this->c.end(); }

	reverse_iterator rbegin() { return this->c.rbegin(); }
	reverse_iterator rend() { return this->c.rend(); }
	const_reverse_iterator rbegin() const { return this->c.rbegin(); }
	const_reverse_iterator rend() const { return this->c.rend(); }
};

#define CALLSTACK_HISTORY (20)
class memBlock;
typedef iterable_queue<memBlock> MemBlockQueue;
typedef std::map<size_type, MemBlockQueue*> InActiveBlockMap;

class memBlock
{
public:
	memBlock() : m_Start(0), m_End(0), m_BucketAndFlags(0), m_Stack(0), m_Id(0), m_FrameAllocated(0), m_FreeStack(NULL) {}
	memBlock(size_type start, size_type end=0, u32 bucketAndFlags=0, memStackEntry* stack=0, s64 id=0, u32 frameAllocated=0) : m_Start(start), m_End(end), m_BucketAndFlags(bucketAndFlags), m_Stack(stack), m_Id(id), m_FrameAllocated(frameAllocated), m_FreeStack(NULL) {}
	s64				m_Id;
	size_type		m_Start;
	size_type		m_End;
	u32				m_BucketAndFlags;
	u32				m_FrameAllocated;
	u32				m_FrameFreed;
	memStackEntry*	m_Stack;
	memStackEntry*	m_FreeStack;

	void			SaveAsMvz(FILE *out, stdext::hash_map<memStackEntry *, s32> &referenceMap);
	bool			ReadFromMvz(FILE *out, int version, atArray<memStackEntry *, 0, u32> &referenceArray);

	void Clear() {m_Start = 0; m_End = 0; m_BucketAndFlags = 0; m_Stack = NULL; m_Id = 0; m_FrameAllocated = 0;}

	void operator=(const size_type size) {Clear(); m_Start = size;}
	bool operator == (const memBlock& rhs) const {return m_Start == rhs.m_Start;}
	bool operator < (const memBlock& rhs) const {return m_Start < rhs.m_Start;}
};

class memHeap;

class memHeapView
{
public:
	memHeapView(memHeap* heap);
	virtual ~memHeapView();
	virtual void OnAlloc(memBlock* block) = 0;
	virtual void OnFree(memBlock* block) = 0;
	virtual void OnSelect(memBlock* block) {}
	virtual void OnSelect(memStackEntry* stack) {}
	virtual void NeedsRedraw() { m_Dirty = true; }
	virtual void OnNextFrame() {}
	virtual void OnUpdate() {}
	virtual void BringToTop() {}

	void OnDestroy();

	memHeap* m_Heap;
	u32 m_MinTimestamp;
	bool m_Dirty;
	bool m_DirtyPosted;
};

//struct memBlockComp
//{
//	bool operator() (const memBlock& lhs, const memBlock& rhs) const {return lhs.m_Start == rhs.m_Start;}
//};

class memHeap
{
public:
	memHeap(const char* name, memSymFile* syms);
	~memHeap();
	
	bool Alloc(size_type addr, size_type size, u32 bucketAndFlags, const size_type* callStack, u32 callStackCount, const tagStackEntry* tagStack, u32 tagCount);
	void Free(size_type addr, const size_type* callStack, u32 callStackCount, const tagStackEntry* tagStack, u32 tagCount);	

	void ClearHistory()
	{
		m_Cs.Lock();
		m_InActiveBlocks.clear();
		m_Cs.Unlock();
	}

	void SetFlags(size_type addr, u32 flags);
	void ClearFlags(size_type addr, u32 flags);
	void Update();
	void MarkAllViewsDirty();
	void ShowNewAllocationsOnly();
	void SetMinTimestamp(u32 timestamp);
	void ShowAllAllocations();
	void NextFrame();
	void Select(memBlock* pBlock);
	void Select(memStackEntry* pStack);
	void AddressSearch(size_type addr);

	void CreateWindowTitle(char *buffer, size_t bufferSize);
	void UpdateWindowTitle();

	void LockViewListForReading();
	void UnlockViewListForReading();
	void LockViewListForWriting();
	void UnlockViewListForWriting();

	void AddView(memHeapView* view);
	void RemoveView(memHeapView* view);

	size_t ComputeBucketSize(int bucket, memStackEntry *stack = NULL);

	void SaveBucketSizes(FILE* out);
	void SaveAsTxt(FILE* out);
	void SaveAsCsv(FILE* out);
	void SaveAsMvz(FILE* out, stdext::hash_map<memStackEntry *, int> &stackReferenceMap);

	bool ReadFromMvz(FILE *fp, int version, atArray<memStackEntry *, 0, u32> &stackReferenceArray);

	sysCriticalSectionToken	m_Cs;
	memSymFile*				m_Syms;
	memStackEntry			m_Root;
	memStackEntry*			m_Highlighted;
	memWnd*					m_Wnd;
	u32						m_Frame;
	size_type				m_HeapStart;
	size_type				m_HeapSize;
	int						m_AllocId;
	std::set<memBlock>		m_ActiveBlocks;
	InActiveBlockMap		m_InActiveBlocks;

#if ENABLE_ALLOCATION_LIST_VIEW
	std::vector<memBlock>	m_Allocations;
#endif
	std::vector<memHeapView*> m_Views;
	diagTrackerRemote2::ClientInfo *m_ClientInfo;
	diagTrackerRemote2::VersionInfo *m_VersionInfo;

	volatile u32			m_ViewListReadLockCount;
	volatile u32			m_ViewListWriteLockCount;
	sysSpinLockToken		m_ViewListReadLock;
	sysIpcMutex				m_ViewListWriteMutex;
};

#endif // MEM_VISUALIZE_HEAP_H
