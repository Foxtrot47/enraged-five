#pragma once

#include <afxmsg_.h>

#include "resource.h"

#include "MemStack.h"

// CAddressSearch dialog

class CAddressSearch : public CDialogImpl<CAddressSearch>
{
//	DECLARE_DYNAMIC(CAddressSearch)

public:
//	CAddressSearch(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAddressSearch();

// Dialog Data
	enum { IDD = IDD_ADDRESS_SEARCH };

	size_type m_Address;

	BEGIN_MSG_MAP(CAddressSearch)
		COMMAND_HANDLER_EX(IDC_ADDRESS, EN_CHANGE, OnEnChangeAddress)
		COMMAND_ID_HANDLER(IDOK, OnOKCancel)
		COMMAND_ID_HANDLER(IDCANCEL, OnOKCancel)
	END_MSG_MAP()

protected:
	LRESULT OnOKCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

public:
	void OnEnChangeAddress(UINT uCode, int nCtrlID, HWND hwndCtrl);
};
