#pragma once

#include "atlopengl.h"
#include "memheap.h"

using namespace rage;

class memView : public CWindowImpl<memView>, public COpenGL<memView>, public memHeapView
{
public:
	DECLARE_WND_CLASS(NULL)

	memView(memHeap* heap);

	BOOL PreTranslateMessage(MSG* pMsg);

	void DoPaint(CDCHandle dc);

	BEGIN_MSG_MAP(memView)
		MESSAGE_RANGE_HANDLER(WM_MOUSEFIRST,WM_MOUSELAST, OnMouse)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroyHandler)
		MSG_WM_LBUTTONDOWN(OnLButtonDown)
		MSG_WM_LBUTTONUP(OnLButtonUp)
		MSG_WM_RBUTTONDOWN(OnRButtonDown)
		MSG_WM_RBUTTONUP(OnRButtonUp)
        MSG_WM_MOUSEMOVE(OnMouseMove)
        MSG_WM_MOUSEWHEEL(OnMouseWheel)        
		MSG_WM_VSCROLL(OnVScroll)
		CHAIN_MSG_MAP(COpenGL<memView>)
	END_MSG_MAP()

	bool ReadFromMvz(FILE *fp, int version);
	void SaveAsMvz(FILE* out);

	void OnInit();
	void OnRender();
    void OnResize(int cx, int cy);

	void OnLButtonDown(UINT nFlags, CPoint point);
	void OnLButtonUp(UINT nFlags, CPoint point);
	void OnRButtonDown(UINT nFlags, CPoint point);
	void OnRButtonUp(UINT nFlags, CPoint point);
    BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
    void OnMouseMove(UINT nFlags, CPoint point);
	void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar pScrollBar);
	LRESULT OnMouse(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled);
	LRESULT OnDestroyHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	virtual void OnAlloc(memBlock* block);
	virtual void OnFree(memBlock* block);
	virtual void OnUpdate();
	virtual void OnSelect(memStackEntry* stack);
	virtual void BringToTop();
	virtual void NeedsRedraw();

	void UpdateScrollRange();

	memBlock* GetBlock(CPoint point, size_type* pAddr = 0, memBlock** nextblock = 0);


	memBlock*				m_ActiveBlock;
	int						m_ScrollY;
	int						m_RowHeight;
	size_type				m_BytesPerRow;
	bool					m_NeedUpdate;
	float					m_PixelsPerByte;

	CPoint					m_OldPoint;
	CToolTipCtrl			m_Tip;
};
