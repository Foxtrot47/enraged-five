#include "stdafx.h"
#include "memheap.h"
#include "memstack.h"
#include "memsymfile.h"
#include "MemTags.h"
#include "memwnd.h"

#include "diag/tracker.h"
#include "math/amath.h"
#include <algorithm>

static void DeallocateChildren(memStackEntry* stackEntry)
{
	memStackEntry*	child = stackEntry->m_Child;

	while ( child )
	{
		memStackEntry*	next = child->m_Next;
		DeallocateChildren( child );
		delete child;
		child = next;
	}
}

const u32 CHECKPOINT_MAGIC = 0xd1ed1cc0;
static u32 s_Checkpoint;


#define STORE_CHECKPOINT(fp)			fwrite(&CHECKPOINT_MAGIC, sizeof(CHECKPOINT_MAGIC), 1, fp)
#define READ_CHECKPOINT(fp)				do { fread(&s_Checkpoint, sizeof(s_Checkpoint), 1, fp); Assert(s_Checkpoint == CHECKPOINT_MAGIC); } while (false)



void memBlock::SaveAsMvz(FILE *out, stdext::hash_map<memStackEntry *, s32> &referenceMap)
{
	STORE_DATA(m_Id, out);
	STORE_DATA(m_Start, out);
	STORE_DATA(m_End, out);
	STORE_DATA(m_BucketAndFlags, out);
	STORE_DATA(m_FrameAllocated, out);
	STORE_DATA(m_FrameFreed, out);			// Version 5

	s32 refIndex = -1;

	if (m_Stack)
	{
		refIndex = referenceMap[m_Stack];
	}

	STORE_DATA(refIndex, out);

	refIndex = -1;

	if (m_FreeStack)
	{
		refIndex = referenceMap[m_FreeStack];
	}

	STORE_DATA(refIndex, out);				// Version 5
}

bool memBlock::ReadFromMvz(FILE *fp, int version, atArray<memStackEntry *, 0, u32> &referenceArray)
{
	READ_DATA(m_Id, fp);
	READ_DATA(m_Start, fp);
	READ_DATA(m_End, fp);
	READ_DATA(m_BucketAndFlags, fp);
	READ_DATA(m_FrameAllocated, fp);

	if (version > 4)
	{
		READ_DATA(m_FrameFreed, fp);
	}
	else
	{
		m_FrameFreed = 0;
	}

	if (m_Start > m_End)
	{
		return false;
	}

	s32 refIndex;
	READ_DATA(refIndex, fp);

	if (refIndex == -1)
	{
		m_Stack = NULL;
	}
	else
	{
		if (refIndex >= referenceArray.GetCount())
		{
			return false;
		}

		m_Stack = referenceArray[refIndex];
	}

	if (version > 4)
	{
		READ_DATA(refIndex, fp);

		if (refIndex == -1)
		{
			m_FreeStack = NULL;
		}
		else
		{
			if (refIndex >= referenceArray.GetCount())
			{
				return false;
			}

			m_FreeStack = referenceArray[refIndex];
		}
	}

	return true;
}



memHeap::memHeap(const char* name, memSymFile* syms)
:	m_AllocId(0)
,	m_Root(name)
,	m_Syms(syms)
,	m_Frame(0)
,	m_Highlighted(0)
,	m_ViewListReadLockCount(0)
,	m_ViewListWriteLockCount(0)
{
	m_ViewListReadLock = SYS_SPINLOCK_UNLOCKED;
	m_ViewListWriteMutex = sysIpcCreateMutex();
}

memHeap::~memHeap() {
	DeallocateChildren( &m_Root );
	sysIpcDeleteMutex(m_ViewListWriteMutex);
}

bool memHeap::Alloc(size_type addr, size_type size, u32 bucketAndFlags, const size_type* callStack, u32 callStackCount, const tagStackEntry* tagStack, u32 tagCount)
{
	if (addr < m_HeapStart || addr + size > m_HeapStart + m_HeapSize)
		return false;
	addr -= m_HeapStart;

	memStackEntry* stack = &m_Root;
	for(u32 i=0; i < callStackCount && callStack[i]; ++i)
	{
		while (tagCount && tagStack->stackLevel == i)
		{
			stack = stack->Child(tagStack->name);
			++tagStack; --tagCount;
		}
		memStackEntry* child = stack->Child(m_Syms->GetName(callStack[i]));
		if (!child)
			break;
		stack = child;
	}
	while (tagCount)
	{
		stack = stack->Child(tagStack->name);
		++tagStack; --tagCount;
	}
	memBlock newBlock(addr, addr + size, bucketAndFlags, stack, m_AllocId++, m_Frame);

	// Child
	if (stack)
	{
		stack->m_Size += size;		
		stack->m_TotalAllocs += 1;
		stack->m_Allocs += 1;
		stack->m_Buckets |= (u64)(1 << (bucketAndFlags & diagTracker::MEMBUCKET_BUCKETMASK));
		stack = stack->m_Parent;
	}

	// Parent
	while (stack)
	{
		stack->m_Size += size;		
		stack->m_TotalAllocs += 1;
		stack->m_Allocs += 1;
		stack = stack->m_Parent;
	}
	m_Cs.Lock();
	std::pair<std::set<memBlock>::iterator, bool> r = m_ActiveBlocks.insert(newBlock);
	newBlock.m_Id = m_Frame;
#if ENABLE_ALLOCATION_LIST_VIEW
	m_Allocations.push_back(newBlock);
#endif
	m_Cs.Unlock();

	LockViewListForReading();
	for(size_t i=0; i<m_Views.size(); ++i)
		m_Views[i]->OnAlloc((memBlock *) &*r.first);
		
	UnlockViewListForReading();

	return true;
}

void memHeap::Free(size_type addr, const size_type* callStack, u32 callStackCount, const tagStackEntry* tagStack, u32 tagCount)
{
	if (addr < m_HeapStart || addr >= m_HeapStart + m_HeapSize)
		return;
	addr -= m_HeapStart;
	memBlock block(addr);
	m_Cs.Lock();

	std::set<memBlock>::iterator it = m_ActiveBlocks.find(block);
	if (it != m_ActiveBlocks.end())
	{
		size_type size = it->m_End - it->m_Start;
		memStackEntry* stack = it->m_Stack;
		while (stack)
		{
			stack->m_Size -= size;
			stack->m_Allocs -= 1;
			stack = stack->m_Parent;
		}

		m_Cs.Unlock();
		
		LockViewListForReading();
		for(size_t j=0; j<m_Views.size(); ++j)
			m_Views[j]->OnFree((memBlock *) &*it);
		UnlockViewListForReading();

		m_Cs.Lock();
		it = m_ActiveBlocks.find(block);
		if (it != m_ActiveBlocks.end())
		{
			// NOTE: Endless history...
			if (memTags::UseHistory())
			{
				// Note when it was freed up.
				memStackEntry* stack = NULL;
				
				if (callStackCount > 0)
				{
					stack = &m_Root;
					for(u32 i=0; i < callStackCount && callStack[i]; ++i)
					{
						while (tagCount && tagStack->stackLevel == i)
						{
							stack = stack->Child(tagStack->name);
							++tagStack; --tagCount;
						}
						memStackEntry* child = stack->Child(m_Syms->GetName(callStack[i]));
						if (!child)
							break;
						stack = child;
					}
					while (tagCount)
					{
						stack = stack->Child(tagStack->name);
						++tagStack; --tagCount;
					}
				}

				// CALLSTACK_HISTORY
				memBlock& block = (memBlock &) *it;
				MemBlockQueue* pQueue = m_InActiveBlocks[block.m_Start];
				if (!pQueue)
					pQueue = m_InActiveBlocks[block.m_Start] = new MemBlockQueue();
				
				if (block.m_Stack)
				{
					block.m_FreeStack = stack;
					block.m_FrameFreed = m_Frame;
					pQueue->push(block);

					if (pQueue->size() > CALLSTACK_HISTORY)
						pQueue->pop();
				}
			}

			m_ActiveBlocks.erase(it);
		}
	}
	m_Cs.Unlock();
}

void memHeap::CreateWindowTitle(char *buffer, size_t bufferSize)
{
	formatf(buffer, bufferSize, "%s - %s %s - %s %s", m_Root.m_Name, m_ClientInfo->m_Platform, m_ClientInfo->m_Configuration,
		m_VersionInfo->m_BuildVersion, m_VersionInfo->m_PackageType);
}

void memHeap::UpdateWindowTitle()
{
	if (m_Wnd && m_Wnd->m_hWnd)
	{
		char title[256];
		CreateWindowTitle(title, sizeof(title));

		::SetWindowText(m_Wnd->m_hWnd, title);
	}
}

void memHeap::AddressSearch(size_type addr)
{
	if (addr < m_HeapStart || addr >= m_HeapStart + m_HeapSize)
		return;
	addr -= m_HeapStart;
	m_Cs.Lock();

	// It's in this heap. Let's find the allocation.
	std::set<memBlock>::iterator i = m_ActiveBlocks.begin();

	while(i != m_ActiveBlocks.end())
	{
		if (i->m_Start <= addr && i->m_End > addr)
		{
			// We found it.
			Select((memBlock *) &*i);

			for(size_t j=0; j<m_Views.size(); ++j)
			{
				m_Views[j]->BringToTop();
			}

			break;
		}

		++i;
	}

	m_Cs.Unlock();
}

void memHeap::SetFlags(size_type addr, u32 flags)
{
	if (addr < m_HeapStart || addr >= m_HeapStart + m_HeapSize)
		return;

	addr -= m_HeapStart;
	memBlock block(addr);

	m_Cs.Lock();
	std::set<memBlock>::iterator i = m_ActiveBlocks.find(block);
	if (i != m_ActiveBlocks.end())
	{
		i = m_ActiveBlocks.find(block);
		if (i != m_ActiveBlocks.end())
		{
			// EJ: Don't ever reference the raw iterator pointer directly!!!!
			memBlock& existing = (memBlock&) *i;
			existing.m_BucketAndFlags |= flags;
		}			
	}
	m_Cs.Unlock();
}

void memHeap::ClearFlags(size_type addr, u32 flags)
{
	if (addr < m_HeapStart || addr >= m_HeapStart + m_HeapSize)
		return;

	addr -= m_HeapStart;
	memBlock block(addr);

	m_Cs.Lock();
	std::set<memBlock>::iterator i = m_ActiveBlocks.find(block);
	if (i != m_ActiveBlocks.end())
	{
		i = m_ActiveBlocks.find(block);
		if (i != m_ActiveBlocks.end())
		{
			// EJ: Don't ever reference the raw iterator pointer directly!!!!
			memBlock& existing = (memBlock&) *i;
			existing.m_BucketAndFlags &= ~flags;
		}
	}
	m_Cs.Unlock();
}


void memHeap::NextFrame()
{
	++m_Frame;
	m_Root.NextFrame();
	LockViewListForReading();

	for(size_t i=0; i<m_Views.size(); ++i)
	{
		m_Views[i]->OnNextFrame();
		m_Views[i]->OnUpdate();
	}
	UnlockViewListForReading();
}

void memHeap::LockViewListForReading()
{
	// Prevent any subsequent write locks.
	u32 newVal = sysInterlockedIncrement(&m_ViewListReadLockCount);

	// Tell the writer that we're still busy here.
	if (newVal == 1)
	{
		sysSpinLockLock(m_ViewListReadLock);
	}

	// If this value is currently >0, there's an active writer.
	if (m_ViewListWriteLockCount > 0)
	{
		// Wait for it to finish.
		sysIpcLockMutex(m_ViewListWriteMutex);
		sysIpcUnlockMutex(m_ViewListWriteMutex);
	}
}

void memHeap::UnlockViewListForReading()
{
	u32 newVal = sysInterlockedDecrement(&m_ViewListReadLockCount);

	// Last one turns the lights off.
	if (newVal == 0)
	{
		sysSpinLockUnlock(m_ViewListReadLock);
	}
}

void memHeap::LockViewListForWriting()
{
	// Wait until the readers are done.
	sysSpinLockLock(m_ViewListReadLock);

	// Tell the readers that we're about to write.
	sysInterlockedIncrement(&m_ViewListWriteLockCount);

	// Now lock it while we're busy.
	sysIpcLockMutex(m_ViewListWriteMutex);
}

void memHeap::UnlockViewListForWriting()
{
	// We're done here.
	sysInterlockedDecrement(&m_ViewListWriteLockCount);

	// Unlock the mutex.

	sysIpcUnlockMutex(m_ViewListWriteMutex);
	sysSpinLockUnlock(m_ViewListReadLock);
}

void memHeap::AddView(memHeapView* view)
{
	SYS_CS_SYNC(m_Cs);
	LockViewListForWriting();
	m_Views.push_back(view);
	UnlockViewListForWriting();
}

void memHeap::RemoveView(memHeapView* view)
{
	LockViewListForWriting();
	m_Views.erase(std::remove(m_Views.begin(), m_Views.end(), view), m_Views.end());
	UnlockViewListForWriting();
}

void memHeap::Update()
{
	LockViewListForReading();
	for(size_t i=0; i<m_Views.size(); ++i)
		m_Views[i]->OnUpdate();
	UnlockViewListForReading();
}

void memHeap::MarkAllViewsDirty()
{
	LockViewListForReading();
	for(size_t i=0; i<m_Views.size(); ++i)
		m_Views[i]->m_Dirty = true;
	UnlockViewListForReading();
}

void memHeap::ShowNewAllocationsOnly()
{
	SetMinTimestamp(++m_Frame);
}

void memHeap::SetMinTimestamp(u32 timestamp)
{
	LockViewListForReading();

	for(size_t i=0; i<m_Views.size(); ++i)
	{
		m_Views[i]->m_MinTimestamp = timestamp;
		m_Views[i]->NeedsRedraw();
		m_Views[i]->OnUpdate();
	}

	UnlockViewListForReading();
}

void memHeap::ShowAllAllocations()
{
	LockViewListForReading();

	for(size_t i=0; i<m_Views.size(); ++i)
	{
		m_Views[i]->m_MinTimestamp = 0;
		m_Views[i]->m_Dirty = true;
	}

	UnlockViewListForReading();
}

void memHeap::Select(memBlock* pBlock)
{
	if (pBlock)
	{
		LockViewListForReading();

		for(size_t i=0; i<m_Views.size(); ++i)
		m_Views[i]->OnSelect(pBlock);

		UnlockViewListForReading();
	}
}
void memHeap::Select(memStackEntry* pStack)
{
	if (m_Highlighted)
		m_Highlighted->Highlight(false);
	m_Highlighted = pStack;
	if (m_Highlighted)
		m_Highlighted->Highlight(true);

	LockViewListForReading();
	for(size_t i=0; i<m_Views.size(); ++i)
		m_Views[i]->OnSelect(pStack);
	UnlockViewListForReading();
}

static void GetBucketNames(const u64 buckets, char* dst)
{
	bool	first = true;
	for (int i = 0; i < 64; ++i)
	{
		if ( buckets & (u64)(1<<i) )
		{
			const char*		bucketName = memTags::GetbucketName(i);
			if ( bucketName )
			{
				if ( !first )
					strcat( dst, ", " );
				first = false;
		
				strcat( dst, bucketName );
			}
		}
	}
}

static void SaveStackAsTxt(FILE* out, memStackEntry* stack, int indent)
{
	// Write some header metadata

	static const int numSpaces = 8;
	static const char spaces[numSpaces+1] = "        ";
	if (stack->m_Size)
	{
		if (stack->m_Name[0])
		{
			int towrite = indent;
			while ( towrite > 0 )
				towrite -= (int) fwrite(spaces, 1, rage::Min( towrite, numSpaces ), out);

			char	bucketNames[1024] = {0};
			GetBucketNames( stack->m_Buckets, bucketNames );

			if (bucketNames[0] == '\0')
				fprintf(out, "%s | %i (in %i allocations)\n", stack->m_Name, stack->m_Size, stack->m_Allocs );
			else
				fprintf(out, "%s | %i (in %i allocations) | %s\n", stack->m_Name, stack->m_Size, stack->m_Allocs, bucketNames );

			++indent;
		}				
		for(memStackEntry* child = stack->m_Child; child; child = child->m_Next)
			SaveStackAsTxt(out, child, indent);
	}
}

size_t memHeap::ComputeBucketSize(int bucket, memStackEntry *stack)
{
	size_t result = 0;

	if (!stack)
	{
		stack = &m_Root;
	}

	// Is there anything?
	if (stack->m_Size)
	{
		// Does it use the bucket?
		if (stack->m_Buckets & (u64)(1 << bucket))
		{
			// Then add the size. And since it includes all the children, we're done here.
			result = stack->m_Size;
		}
		else
		{
			// This one isn't using the bucket, but maybe the children are.
			for(memStackEntry* child = stack->m_Child; child; child = child->m_Next)
			{
				result += ComputeBucketSize(bucket, child);
			}
		}
	}

	return result;
}

void memHeap::SaveBucketSizes(FILE* out)
{
	for (int i = 0; i < 64; ++i)
	{
		const char*		bucketName = memTags::GetbucketName(i);

		if ( bucketName )
		{
			size_t bucketSize = ComputeBucketSize(i);
			fprintf(out, "# BUCKET_%s: %d\n", bucketName, bucketSize);
		}
	}
}

void memHeap::SaveAsTxt(FILE* out)
{
	fprintf(out, "# DumpVersion: 4\n");
	fprintf(out, "# Platform: %s\n", m_ClientInfo->m_Platform);
	fprintf(out, "# Configuration: %s\n", m_ClientInfo->m_Configuration);
	fprintf(out, "# ProtocolVersion: %d\n", diagTrackerRemote2::PROTOCOL_VERSION);
	fprintf(out, "# BuildVersion: %s\n", m_VersionInfo->m_BuildVersion);
	fprintf(out, "# PackageType: %s\n", m_VersionInfo->m_PackageType);

	SaveBucketSizes( out );

	SYS_CS_SYNC( m_Cs );
	SaveStackAsTxt( out, &m_Root, 0 );
}

void memHeap::SaveAsMvz(FILE* out, stdext::hash_map<memStackEntry *, int> &stackReferenceMap)
{
	m_Cs.Lock();

	// Basic heap information
	fwrite(&m_HeapStart, sizeof(m_HeapStart), 1, out);
	fwrite(&m_HeapSize, sizeof(m_HeapSize), 1, out);

	// The stack
	int stackReferenceIndex = 0;
	m_Root.SaveAsMvz(out, stackReferenceIndex, stackReferenceMap);

	// Blocks
	u32 blockCount = (u32) m_ActiveBlocks.size();
	fwrite(&blockCount, sizeof(blockCount), 1, out);

	std::set<memBlock>::iterator i = m_ActiveBlocks.begin();

	u32 validation = 0;

	while(i != m_ActiveBlocks.end())
	{
		// EJ: Don't ever reference the raw iterator pointer directly!!!!
		memBlock& block = (memBlock&) *i;
		block.SaveAsMvz(out, stackReferenceMap);

		++i;
		validation++;
	}

	Assert(validation == blockCount);

	// Inactive blocks
	blockCount = 0;
	InActiveBlockMap::const_iterator itMap = m_InActiveBlocks.begin();
	while (itMap != m_InActiveBlocks.end())
	{
		MemBlockQueue* pQueue  = itMap->second;
		if (pQueue)
			blockCount += (u32) pQueue->size();
		++itMap;
	}

	fwrite(&blockCount, sizeof(blockCount), 1, out);
	itMap = m_InActiveBlocks.begin();
	validation = 0;

	while (itMap != m_InActiveBlocks.end())
	{
		MemBlockQueue* pQueue  = itMap->second;
		MemBlockQueue::iterator itQueue = pQueue->begin();
		while (itQueue != pQueue->end())
		{
			memBlock& block = *itQueue;
			block.SaveAsMvz(out, stackReferenceMap);
			validation++;
			++itQueue;
		}

		++itMap;
	}

	Assert(validation == blockCount);

	m_Cs.Unlock();
}

bool memHeap::ReadFromMvz(FILE *fp, int version, atArray<memStackEntry *, 0, u32> &stackReferenceArray)
{
	// Basic heap information
	fread(&m_HeapStart, sizeof(m_HeapStart), 1, fp);
	fread(&m_HeapSize, sizeof(m_HeapSize), 1, fp);

	// The stack
	m_Root.ReadFromMvz(fp, version, stackReferenceArray);

	// Blocks
	u32 blockCount;
	fread(&blockCount, sizeof(blockCount), 1, fp);

	if (blockCount > 0x1000000)
	{
		return false;
	}

	Displayf("Reading heap %s, %d blocks", m_Root.m_Name, blockCount);

	for (u32 x=0; x<blockCount; x++)
	{
		memBlock newBlock;
		if (!(newBlock.ReadFromMvz(fp, version, stackReferenceArray)))
		{
			return false;
		}

		m_ActiveBlocks.insert(newBlock);
	}

	fread(&blockCount, sizeof(blockCount), 1, fp);

	if (blockCount > 0x10000000)
	{
		return false;
	}

	if (version >= 4)
	{
		Displayf("Reading %d inactive blocks", blockCount);

		for (u32 x=0; x<blockCount; x++)
		{
			memBlock newBlock;
			if (!(newBlock.ReadFromMvz(fp, version, stackReferenceArray)))
			{
				return false;
			}

			//m_InActiveBlocks.insert(newBlock);
			MemBlockQueue* pQueue = m_InActiveBlocks[newBlock.m_Start];
			if (!pQueue)
				pQueue = m_InActiveBlocks[newBlock.m_Start] = new MemBlockQueue();

			pQueue->push(newBlock);
		}
	}

	return true;
}

void memHeap::SaveAsCsv(FILE* out)
{
	fprintf(out,"Id,BucketId,BucketName,FrameAllocated,Address,Size,Stack\n");

	// Dump filters
	u32 mustHaveFlags = 0;
	u32 mustNotHaveFlags = 0;

	if (!memTags::GetShowLocked())
		mustNotHaveFlags |= diagTracker::MEMBUCKETFLAG_LOCKED;
	
	if (!memTags::GetShowExternal())
		mustNotHaveFlags |= diagTracker::MEMBUCKETFLAG_EXTERNAL;

	if (!memTags::GetShowDefrag())
		mustNotHaveFlags |= diagTracker::MEMBUCKETFLAG_CAN_DEFRAG;

	if (!memTags::GetShowMoved())
		mustNotHaveFlags |= diagTracker::MEMBUCKETFLAG_MOVED;
	
	m_Cs.Lock();
	std::set<memBlock>::iterator i = m_ActiveBlocks.begin();
	while(i != m_ActiveBlocks.end())
	{
		u32 flags = i->m_BucketAndFlags;

		if ((mustHaveFlags & flags) == mustHaveFlags)
		{
			if ((mustNotHaveFlags & flags) == 0)
			{
				unsigned int bits = 0;
				if ((i->m_BucketAndFlags & diagTracker::MEMBUCKETFLAG_CAN_DEFRAG) == diagTracker::MEMBUCKETFLAG_CAN_DEFRAG)
					bits |= 1;
				if ((i->m_BucketAndFlags & diagTracker::MEMBUCKETFLAG_LOCKED) == diagTracker::MEMBUCKETFLAG_LOCKED)
					bits |= 2;
				if ((i->m_BucketAndFlags & diagTracker::MEMBUCKETFLAG_EXTERNAL) == diagTracker::MEMBUCKETFLAG_EXTERNAL)
					bits |= 4;
				if ((i->m_BucketAndFlags & diagTracker::MEMBUCKETFLAG_MOVED) == diagTracker::MEMBUCKETFLAG_MOVED)
					bits |= 8;

				if (!bits)
					bits |= 16;

				fprintf(out,"%i,%i,%s,%i,%i,%i,\"",
						i->m_Id,
						bits,
						memTags::GetbucketName(i->m_BucketAndFlags & diagTracker::MEMBUCKET_BUCKETMASK),
						i->m_FrameAllocated,
						i->m_Start,
						i->m_End-i->m_Start);

				memStackEntry* stack = i->m_Stack;
				int length = 0;
				while (stack)
				{
					length += (int) strlen(stack->m_Name) + 3;
					if (length >= 2048)
						break;

					fprintf(out,stack->m_Name);
					stack = stack->m_Parent;
					if (stack)
						fprintf(out, "|");
				}
				fprintf(out,"\"\n");
			}
		}
			
		i++;
	}

	m_Cs.Unlock();
}
memHeapView::memHeapView(memHeap* heap) 
:	m_Heap(heap)
,	m_MinTimestamp(0)
,	m_DirtyPosted(false)
{
	m_Heap->AddView(this);
}
memHeapView::~memHeapView()
{
	// Shouldn't be necessary, we're already doing it in OnDestroy - but this is just an extra safeguard.
	m_Heap->RemoveView(this);
}

void memHeapView::OnDestroy()
{
	m_Heap->RemoveView(this);
}
