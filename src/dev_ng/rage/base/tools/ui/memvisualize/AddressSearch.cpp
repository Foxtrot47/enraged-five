// AddressSearch.cpp : implementation file
//

#include "stdafx.h"
#include "AddressSearch.h"


// CAddressSearch dialog

CAddressSearch::~CAddressSearch()
{
}


// CAddressSearch message handlers

void CAddressSearch::OnEnChangeAddress(UINT uCode, int nCtrlID, HWND hwndCtrl )
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	char text[256];
	::GetWindowText(hwndCtrl, text, sizeof(text));

	// Chomp it.
	const char *start = text;

	while (*start == ' ' || *start == '\t')
	{
		start++;
	}

	// Strip "0x" if present - we're always assuming hex.
	if (*start == '0' && (start[1] == 'x' || start[1] == 'X'))
	{
		start += 2;
	}

	size_t value;
	if (sscanf(start, "%llx", &value))
	{
		m_Address = value;
	}
	else
	{
		m_Address = 0;
	}
}
