#include "stdafx.h"
#include "memtreeview.h"
#include "memstack.h"

int memTreeView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	DefWindowProc(WM_CREATE, 0, (LPARAM)lpCreateStruct);
	SetBkColor(GetSysColor(COLOR_WINDOW)); // dunno why this is necessary but i get the wrong bk col otherwise
 	return 0;
}

void memTreeView::OnPaint(CDCHandle)
{
	CPaintDC dc(m_hWnd);
	CMemoryDC dcMem(dc.m_hDC, dc.m_ps.rcPaint);
	PrintClient(dcMem.m_hDC, PRF_CLIENT);
}

BOOL memTreeView::OnEraseBkgnd(CDCHandle dc)
{
	return TRUE;
}

void memTreeView::OnAlloc(memBlock* block)
{
	m_Dirty = true;

	PostMessage(WM_TREEITEM, 0, (LPARAM) block->m_Stack);
}

void memTreeView::OnFree(memBlock *block)
{
	m_Dirty = true;
}

void memTreeView::OnSelect(memBlock* block)
{
	HTREEITEM item = TreeItem(block->m_Stack);
	EnsureVisible(item);
	SelectItem(item); 
}

void memTreeView::OnUpdate()
{
	if (m_Dirty)
	{
		m_Dirty = false;
		Invalidate();
	}
}

LRESULT memTreeView::OnTreeItemHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	TreeItem((memStackEntry *) lParam);
	return 0;
}

HTREEITEM memTreeView::TreeItem(memStackEntry* stack)
{
	HTREEITEM& item = m_items[stack];
	if (item)
		return item;
	HTREEITEM parent = stack->m_Parent ? TreeItem(stack->m_Parent) : TVI_ROOT;
	if (!stack->m_Name[0] ||
		strstr(stack->m_Name, "sysIpcThreadWrapper") ||
		strstr(stack->m_Name, "CommonMain") ||
		strstr(stack->m_Name, "rage::ExceptMain") ||
		strstr(stack->m_Name, "mainCRTStartup") ||
		strstr(stack->m_Name, "XapiThreadStartup"))
	{
		item = parent;		
	}
	else
	{
		TVINSERTSTRUCT insert = {0};
		insert.hParent = parent;
		insert.hInsertAfter = TVI_LAST;
		insert.item.mask = TVIF_PARAM | TVIF_TEXT;
		insert.item.pszText = LPSTR_TEXTCALLBACK;
		insert.item.lParam = (LPARAM)stack;
		item = InsertItem(&insert);
	}
	return item;
}

LRESULT memTreeView::OnSelChanged(int /*idCtrl*/, LPNMHDR pnmh, BOOL& /*bHandled*/)
{
	LPNMTREEVIEW selchg = (LPNMTREEVIEW)pnmh;
	memStackEntry* stack = (memStackEntry*)GetItemData(selchg->itemNew.hItem);
	m_Heap->Select(stack);
	return 0;
}

void memTreeView::OnKillFocus(CWindow wndFocus)
{
	memStackEntry* stack = 0;
	m_Heap->Select(stack);
	SetMsgHandled(FALSE);
}

char* FormatNumber(size_type i)
{
	static char buf[256];
	char* dest = buf + 255;
	*dest = 0;
	int j = -1;
	do
	{
		if (++j == 3)
		{
			j = 0;
			*--dest = ',';
		}
		*--dest = '0'+(i%10);
		i /= 10;
	}
	while (i);
	return dest;
}

LRESULT memTreeView::GetDispInfo(int /*idCtrl*/, LPNMHDR pnmh, BOOL& bHandled)
{
	LPNMTVDISPINFO dispinfo = (LPNMTVDISPINFO)pnmh;
	memStackEntry* stack = (memStackEntry*)dispinfo->item.lParam;
	if (stack)
	{
		static char buf[2048];
		dispinfo->item.cchTextMax = 2048;
		dispinfo->item.pszText = buf;
		sprintf_s(buf, "%s | %s (%i allocations)", stack->m_Name, FormatNumber(stack->m_Size), stack->m_Allocs);
	}
	return TRUE;
}

const int HAS_SIBLING = 1;
const int HAS_CHILD = 2;

void memTreeView::RecurseSaveTree(FILE* out, stdext::hash_map<memStackEntry *, int> &stackReferenceMap, HTREEITEM item)
{
	// Apparently the compiler doesn't have tail end optimizations, so we can't use recursions for siblings.
	do 
	{
		char name[256];

		Assert(item);
		Assert(stackReferenceMap.find((memStackEntry *) GetItemData(item)) != stackReferenceMap.end());

		int refValue = stackReferenceMap[(memStackEntry *) GetItemData(item)];
		STORE_DATA(refValue, out);

		GetItemText(item, name, sizeof(name));
		WriteString(name, out);

		int flags = 0;

		HTREEITEM childItem = GetChildItem(item);
		HTREEITEM siblingItem = GetNextSiblingItem(item);

		if (childItem)
			flags |= HAS_CHILD;

		if (siblingItem)
			flags |= HAS_SIBLING;

		STORE_DATA(flags, out);

		if (childItem)
		{
			RecurseSaveTree(out, stackReferenceMap, childItem);
		}

		item = siblingItem;
	}
	while (item);
}

bool memTreeView::RecurseLoadTree(FILE *fp, int version, const atArray<memStackEntry *, 0, u32> &stackReferenceArray, HTREEITEM parent, HTREEITEM prev)
{
	// Apparently the compiler doesn't have tail end optimizations, so we can't use recursions for siblings.
	int flags;

	do 
	{
		char name[256];

		int refValue;
		READ_DATA(refValue, fp);

		if (refValue >= stackReferenceArray.GetCount())
		{
			Errorf("Bad stack reference - %d", refValue);
			return false;
		}

		ReadString(name, sizeof(name), fp);
		HTREEITEM item = InsertItem(name, parent, prev);

		m_items[stackReferenceArray[refValue]] = item;

		READ_DATA(flags, fp);

		if (flags & HAS_CHILD)
		{
			RecurseLoadTree(fp, version, stackReferenceArray, item, TVI_LAST);
		}
	}
	while (flags & HAS_SIBLING);

	return true;
}


void memTreeView::SaveAsMvz(FILE* out, stdext::hash_map<memStackEntry *, int> &stackReferenceMap)
{
	// The user may have closed this window already.
	if (::IsWindow(m_hWnd))
	{
		int itemCount = GetCount();
		STORE_DATA(itemCount, out);

		if (GetRootItem())
		{
			RecurseSaveTree(out, stackReferenceMap, GetRootItem());
		}
	}
	else
	{
		int itemCount = 0;
		STORE_DATA(itemCount, out);
	}

	// Terminator.
	int terminator = -1;
	STORE_DATA(terminator, out);
}

bool memTreeView::ReadFromMvz(FILE *fp, int version, const atArray<memStackEntry *, 0, u32> &stackReferenceArray)
{
	int itemCount;
	READ_DATA(itemCount, fp);

	if (itemCount)
	{
		bool result = RecurseLoadTree(fp, version, stackReferenceArray, TVI_ROOT, TVI_LAST);

		if (!result)
		{
			return false;
		}
	}

	int terminator;
	READ_DATA(terminator, fp);

	return terminator == -1;
}