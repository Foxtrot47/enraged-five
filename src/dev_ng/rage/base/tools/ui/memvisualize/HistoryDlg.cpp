
#include "stdafx.h"
#include "HistoryDlg.h"
#include "MemTags.h"
#include "atl/string.h"

#include <algorithm>
#include <list>
using namespace std;

CHistoryDlg::CHistoryDlg(memHeap* pHeap, const memBlock& block, bool used /*= true*/) : m_pHeap(pHeap), m_block(block), m_used(used)
{	
}

CHistoryDlg::~CHistoryDlg(void)
{
}

LRESULT CHistoryDlg::OnOKCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	EndDialog(wID);
	return 0;
}

LRESULT CHistoryDlg::OnHistoryUpdate(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	UpdateCallstacks();
	return 0;
}	

LRESULT CHistoryDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{		
	HWND hDlg = ::FindWindow(NULL, "Callstack History");

	char szAddress[32] = {0};
	sprintf(szAddress, "0x%p", m_pHeap->m_HeapStart + m_block.m_Start);
	::SetDlgItemText(hDlg, IDC_HISTORY_ADDRESS, szAddress);

	UpdateCallstacks();

	return TRUE;
}

void CHistoryDlg::UpdateCallstacks()
{
	HWND hDlg = ::FindWindow(NULL, "Callstack History");
	::SetDlgItemText(hDlg, IDC_HISTORY_TEXT, "");

	char szAddress[32] = {0};
	::GetDlgItemText(hDlg, IDC_HISTORY_ADDRESS, szAddress, 32);
	if (!strlen(szAddress))
		return;

	m_pHeap->m_Cs.Lock();
	size_type addr = _strtoui64(szAddress, NULL, 0) - m_pHeap->m_HeapStart;
	m_block = addr;

	std::set<memBlock>::iterator it = m_pHeap->m_ActiveBlocks.lower_bound(m_block);	
	while (it != m_pHeap->m_ActiveBlocks.begin() && (it == m_pHeap->m_ActiveBlocks.end() || it->m_Start > addr))
		--it;

	memBlock* pNextBlock = NULL;
	if (it != m_pHeap->m_ActiveBlocks.end())
	{
		m_block = *it;
		if (++it != m_pHeap->m_ActiveBlocks.end())
			pNextBlock = (memBlock *) &*it;
	}

	const bool used = (addr < m_block.m_End && addr >= m_block.m_Start);
	if (!used)
	{		
		const size_t bytes = (pNextBlock ? pNextBlock->m_Start : m_pHeap->m_HeapSize) - m_block.m_End;
		size_type start = m_block.m_End;
		size_type end = m_block.m_End + bytes;

		m_block.Clear();
		m_block.m_Start = addr;
		m_block.m_End = end;
	}
	else
	{
		sprintf(szAddress, "0x%p", m_pHeap->m_HeapStart + m_block.m_Start);
		::SetDlgItemText(hDlg, IDC_HISTORY_ADDRESS, szAddress);
	}

	char sz[512];
	list<atString> lines;
	atString endl("\r\n");
	atString dots("...");
	size_t callstacks = 0;

	// Active
	if (m_block.m_Stack)
	{
		InActiveBlockMap::const_iterator itMap = m_pHeap->m_InActiveBlocks.find(m_block.m_Start);
		const size_t count = (itMap != m_pHeap->m_InActiveBlocks.end()) ? (itMap->second->size() + 1) : 1;

		lines.push_back(atString("\r\n\r\nCURRENT ALLOCATION\r\n------------------------------\r\n"));
		formatf(sz, "Allocation %d\r\n0x%p - 0x%p\r\n%llu bytes\r\nFrame %d\r\n", count, (m_pHeap->m_HeapStart + m_block.m_Start), (m_pHeap->m_HeapStart + m_block.m_End), m_block.m_End - m_block.m_Start, m_block.m_FrameAllocated);
		lines.push_back(atString(sz));
		lines.push_back(dots);

		atString str;
		memStackEntry* pStack = m_block.m_Stack;
		callstacks++;

		while (pStack)
		{
			str = pStack->m_Name;
			lines.push_back(str);
			pStack = pStack->m_Parent;
		}
	}

	// Inactive
	if (memTags::UseHistory())
	{
		InActiveBlockMap::const_iterator itMap = m_pHeap->m_InActiveBlocks.find(m_block.m_Start);
		MemBlockQueue* pQueue = (itMap != m_pHeap->m_InActiveBlocks.end()) ? itMap->second : NULL;
		if (pQueue)
		{
			lines.push_back(atString("\r\n\r\nALLOCATION HISTORY\r\n------------------------------\r\n"));
			
			int i = 0;
			callstacks += pQueue->size();
			MemBlockQueue::const_reverse_iterator itQueue = pQueue->rbegin();
			while (itQueue != pQueue->rend())
			{
				const memBlock& block = *itQueue;
				memStackEntry* pStack = block.m_Stack;
				formatf(sz, "Allocation %d\r\n0x%p - 0x%p\r\n%llu bytes\r\nFrame %d\r\n", pQueue->size() - i, (m_pHeap->m_HeapStart + block.m_Start), (m_pHeap->m_HeapStart + block.m_End), block.m_End - block.m_Start, block.m_FrameAllocated);
				lines.push_back(atString(sz));
				lines.push_back(dots);

				atString str;
				while (pStack)
				{
					str = pStack->m_Name;
					lines.push_back(str);
					pStack = pStack->m_Parent;
				}

				// Any recorded FREE?
				if (pStack = block.m_FreeStack)
				{
					formatf(sz, "\r\nFree %d\r\n0x%p - 0x%p\r\n%llu bytes\r\nFrame %d\r\n", pQueue->size() - i, (m_pHeap->m_HeapStart + block.m_Start), (m_pHeap->m_HeapStart + block.m_End), block.m_End - block.m_Start, block.m_FrameFreed);
					lines.push_back(atString(sz));
					lines.push_back(dots);

					while (pStack)
					{
						str = pStack->m_Name;
						lines.push_back(str);
						pStack = pStack->m_Parent;
					}
				}

				lines.push_back(endl);
				++itQueue;
				++i;
			}
		}
	}	

	if (m_block.m_Stack)
		sprintf(sz, "SUMMARY\r\n------------------------------\r\nUSED\r\n0x%p\r\n%d callstack(s)\r\n\n", (m_pHeap->m_HeapStart + m_block.m_Start), callstacks);
	else
		sprintf(sz, "SUMMARY\r\n------------------------------\r\nFREE\r\n0x%p\r\n%llu bytes\r\n%d callstack(s)\r\n\n", (m_pHeap->m_HeapStart + m_block.m_Start), (m_block.m_End - m_block.m_Start), callstacks);

	atString text(sz);

	int i = 1;	
	for (list<atString>::iterator it = lines.begin(); it != lines.end(); it++)
	{
		atString& str = *it;
		text += str;
		if (!text.EndsWith("\r\n"))
			text += endl;

		if (text.length() > 60000)
		{
			text += atString("\r\n\r\nTRUNCATED ----");
			break;
		}
	}

	::SetDlgItemText(hDlg, IDC_HISTORY_TEXT, text.c_str());
	m_pHeap->m_Cs.Unlock();
}
