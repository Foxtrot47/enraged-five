#ifndef MEM_LIST_VIEW_H
#define MEM_LIST_VIEW_H

#define ENABLE_ALLOCATION_LIST_VIEW		0

#include "memheap.h"

#if ENABLE_ALLOCATION_LIST_VIEW
class memListView : public CWindowImpl<memListView, CListViewCtrl>, /*public CCustomDraw<memListView>,*/ public memHeapView
{
public:
	memListView(memHeap* heap) : memHeapView(heap) {}

	BEGIN_MSG_MAP(memListView)		
		MSG_WM_CREATE(OnCreate)
		MSG_WM_DESTROY(OnDestroy)
		MSG_WM_ERASEBKGND(OnEraseBkgnd)
		REFLECTED_NOTIFY_CODE_HANDLER(LVN_ITEMCHANGED, OnItemChanged)
		REFLECTED_NOTIFY_CODE_HANDLER(LVN_GETDISPINFO, GetDispInfo)
		//CHAIN_MSG_MAP(CCustomDraw<memListView>)
	END_MSG_MAP()

	int OnCreate(LPCREATESTRUCT lpCreateStruct);
	BOOL OnEraseBkgnd(CDCHandle dc);
	LRESULT GetDispInfo(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/);
	LRESULT OnItemChanged(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/);

// 	LRESULT OnPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCustomDraw);
// 	LRESULT OnItemPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCustomDraw);

	virtual void OnAlloc(memBlock* block);
	virtual void OnFree(memBlock* block);
	virtual void OnUpdate();
};
#endif

#endif // MEM_LIST_VIEW_H
