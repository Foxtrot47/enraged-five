#ifndef MEM_STACK_H
#define MEM_STACK_H

#include "atl/array.h"

#include <hash_map>

using namespace rage;

typedef u64 size_type;

struct tagStackEntry
{
	const char* name;
	u8 stackLevel;
};

class memStackEntry
{
public:
	memStackEntry() : m_Name(0) {}
	memStackEntry(const char* name, COLORREF colour = RGB(128,128,128), memStackEntry* parent = 0);

	memStackEntry* Child(const char* name);

	void NextFrame();
	void Highlight(bool val);
	bool IsHighlighted() const {return (m_Colour>>31)!=0;}

	void BuildTree(s32 &referenceIndex, stdext::hash_map<memStackEntry *, s32> &referenceMap);
	void SaveAsMvz(FILE *out, s32 &referenceIndex, stdext::hash_map<memStackEntry *, s32> &referenceMap);
	void ReadFromMvz(FILE *out, int version, atArray<memStackEntry *, 0, u32> &referenceArray, memStackEntry* parent = NULL);
	
	const char*		m_Name;
	COLORREF		m_Colour;

	size_type		m_Size;
	u32				m_Allocs;
	u32				m_TotalAllocs;
	float			m_AllocsPerFrame;
	u32				m_TotalAllocsAtLastFrame;
	u64				m_Buckets;
	memStackEntry*	m_Child;
	memStackEntry*	m_Next;
	memStackEntry*	m_Parent;
};


#endif // MEM_STACK_H
