﻿namespace RageErrorCodeManager
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabLanguages = new System.Windows.Forms.TabControl();
            this.tabAmerican = new System.Windows.Forms.TabPage();
            this.dataGridAmerican = new System.Windows.Forms.DataGridView();
            this.tabChinese = new System.Windows.Forms.TabPage();
            this.dataGridChinese = new System.Windows.Forms.DataGridView();
            this.tabFrench = new System.Windows.Forms.TabPage();
            this.dataGridFrench = new System.Windows.Forms.DataGridView();
            this.tabGerman = new System.Windows.Forms.TabPage();
            this.dataGridGerman = new System.Windows.Forms.DataGridView();
            this.tabItalian = new System.Windows.Forms.TabPage();
            this.dataGridItalian = new System.Windows.Forms.DataGridView();
            this.tabJapanese = new System.Windows.Forms.TabPage();
            this.dataGridJapanese = new System.Windows.Forms.DataGridView();
            this.tabKorean = new System.Windows.Forms.TabPage();
            this.dataGridKorean = new System.Windows.Forms.DataGridView();
            this.tabMexican = new System.Windows.Forms.TabPage();
            this.dataGridMexican = new System.Windows.Forms.DataGridView();
            this.tabPolish = new System.Windows.Forms.TabPage();
            this.dataGridPolish = new System.Windows.Forms.DataGridView();
            this.tabPortuguese = new System.Windows.Forms.TabPage();
            this.dataGridPortuguese = new System.Windows.Forms.DataGridView();
            this.tabRussian = new System.Windows.Forms.TabPage();
            this.dataGridRussian = new System.Windows.Forms.DataGridView();
            this.tabSpanish = new System.Windows.Forms.TabPage();
            this.dataGridSpanish = new System.Windows.Forms.DataGridView();
            this.btnAddErrorCode = new System.Windows.Forms.Button();
            this.btnLoadTextFiles = new System.Windows.Forms.Button();
            this.btnSaveTextFiles = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.txtHeaderFile = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabLanguages.SuspendLayout();
            this.tabAmerican.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAmerican)).BeginInit();
            this.tabChinese.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridChinese)).BeginInit();
            this.tabFrench.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridFrench)).BeginInit();
            this.tabGerman.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridGerman)).BeginInit();
            this.tabItalian.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridItalian)).BeginInit();
            this.tabJapanese.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridJapanese)).BeginInit();
            this.tabKorean.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridKorean)).BeginInit();
            this.tabMexican.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMexican)).BeginInit();
            this.tabPolish.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPolish)).BeginInit();
            this.tabPortuguese.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPortuguese)).BeginInit();
            this.tabRussian.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRussian)).BeginInit();
            this.tabSpanish.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridSpanish)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabLanguages
            // 
            this.tabLanguages.Controls.Add(this.tabAmerican);
            this.tabLanguages.Controls.Add(this.tabChinese);
            this.tabLanguages.Controls.Add(this.tabFrench);
            this.tabLanguages.Controls.Add(this.tabGerman);
            this.tabLanguages.Controls.Add(this.tabItalian);
            this.tabLanguages.Controls.Add(this.tabJapanese);
            this.tabLanguages.Controls.Add(this.tabKorean);
            this.tabLanguages.Controls.Add(this.tabMexican);
            this.tabLanguages.Controls.Add(this.tabPolish);
            this.tabLanguages.Controls.Add(this.tabPortuguese);
            this.tabLanguages.Controls.Add(this.tabRussian);
            this.tabLanguages.Controls.Add(this.tabSpanish);
            this.tabLanguages.Location = new System.Drawing.Point(12, 41);
            this.tabLanguages.Name = "tabLanguages";
            this.tabLanguages.SelectedIndex = 0;
            this.tabLanguages.Size = new System.Drawing.Size(1060, 517);
            this.tabLanguages.TabIndex = 0;
            // 
            // tabAmerican
            // 
            this.tabAmerican.Controls.Add(this.dataGridAmerican);
            this.tabAmerican.Location = new System.Drawing.Point(4, 22);
            this.tabAmerican.Name = "tabAmerican";
            this.tabAmerican.Padding = new System.Windows.Forms.Padding(3);
            this.tabAmerican.Size = new System.Drawing.Size(1052, 491);
            this.tabAmerican.TabIndex = 0;
            this.tabAmerican.Text = "American";
            this.tabAmerican.UseVisualStyleBackColor = true;
            // 
            // dataGridAmerican
            // 
            this.dataGridAmerican.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAmerican.Location = new System.Drawing.Point(6, 6);
            this.dataGridAmerican.Name = "dataGridAmerican";
            this.dataGridAmerican.Size = new System.Drawing.Size(1040, 479);
            this.dataGridAmerican.TabIndex = 0;
            // 
            // tabChinese
            // 
            this.tabChinese.Controls.Add(this.dataGridChinese);
            this.tabChinese.Location = new System.Drawing.Point(4, 22);
            this.tabChinese.Name = "tabChinese";
            this.tabChinese.Padding = new System.Windows.Forms.Padding(3);
            this.tabChinese.Size = new System.Drawing.Size(1052, 491);
            this.tabChinese.TabIndex = 0;
            this.tabChinese.Text = "Chinese";
            this.tabChinese.UseVisualStyleBackColor = true;
            // 
            // dataGridChinese
            // 
            this.dataGridChinese.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridChinese.Location = new System.Drawing.Point(6, 6);
            this.dataGridChinese.Name = "dataGridChinese";
            this.dataGridChinese.Size = new System.Drawing.Size(1040, 479);
            this.dataGridChinese.TabIndex = 0;
            // 
            // tabFrench
            // 
            this.tabFrench.Controls.Add(this.dataGridFrench);
            this.tabFrench.Location = new System.Drawing.Point(4, 22);
            this.tabFrench.Name = "tabFrench";
            this.tabFrench.Padding = new System.Windows.Forms.Padding(3);
            this.tabFrench.Size = new System.Drawing.Size(1052, 491);
            this.tabFrench.TabIndex = 1;
            this.tabFrench.Text = "French";
            this.tabFrench.UseVisualStyleBackColor = true;
            // 
            // dataGridFrench
            // 
            this.dataGridFrench.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridFrench.Location = new System.Drawing.Point(6, 6);
            this.dataGridFrench.Name = "dataGridFrench";
            this.dataGridFrench.Size = new System.Drawing.Size(1040, 479);
            this.dataGridFrench.TabIndex = 1;
            // 
            // tabGerman
            // 
            this.tabGerman.Controls.Add(this.dataGridGerman);
            this.tabGerman.Location = new System.Drawing.Point(4, 22);
            this.tabGerman.Name = "tabGerman";
            this.tabGerman.Padding = new System.Windows.Forms.Padding(3);
            this.tabGerman.Size = new System.Drawing.Size(1052, 491);
            this.tabGerman.TabIndex = 3;
            this.tabGerman.Text = "German";
            this.tabGerman.UseVisualStyleBackColor = true;
            // 
            // dataGridGerman
            // 
            this.dataGridGerman.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridGerman.Location = new System.Drawing.Point(6, 6);
            this.dataGridGerman.Name = "dataGridGerman";
            this.dataGridGerman.Size = new System.Drawing.Size(1040, 479);
            this.dataGridGerman.TabIndex = 1;
            // 
            // tabItalian
            // 
            this.tabItalian.Controls.Add(this.dataGridItalian);
            this.tabItalian.Location = new System.Drawing.Point(4, 22);
            this.tabItalian.Name = "tabItalian";
            this.tabItalian.Padding = new System.Windows.Forms.Padding(3);
            this.tabItalian.Size = new System.Drawing.Size(1052, 491);
            this.tabItalian.TabIndex = 2;
            this.tabItalian.Text = "Italian";
            this.tabItalian.UseVisualStyleBackColor = true;
            // 
            // dataGridItalian
            // 
            this.dataGridItalian.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridItalian.Location = new System.Drawing.Point(6, 6);
            this.dataGridItalian.Name = "dataGridItalian";
            this.dataGridItalian.Size = new System.Drawing.Size(1040, 479);
            this.dataGridItalian.TabIndex = 1;
            // 
            // tabJapanese
            // 
            this.tabJapanese.Controls.Add(this.dataGridJapanese);
            this.tabJapanese.Location = new System.Drawing.Point(4, 22);
            this.tabJapanese.Name = "tabJapanese";
            this.tabJapanese.Padding = new System.Windows.Forms.Padding(3);
            this.tabJapanese.Size = new System.Drawing.Size(1052, 491);
            this.tabJapanese.TabIndex = 6;
            this.tabJapanese.Text = "Japanese";
            this.tabJapanese.UseVisualStyleBackColor = true;
            // 
            // dataGridJapanese
            // 
            this.dataGridJapanese.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridJapanese.Location = new System.Drawing.Point(6, 6);
            this.dataGridJapanese.Name = "dataGridJapanese";
            this.dataGridJapanese.Size = new System.Drawing.Size(1040, 479);
            this.dataGridJapanese.TabIndex = 1;
            // 
            // tabKorean
            // 
            this.tabKorean.Controls.Add(this.dataGridKorean);
            this.tabKorean.Location = new System.Drawing.Point(4, 22);
            this.tabKorean.Name = "tabKorean";
            this.tabKorean.Padding = new System.Windows.Forms.Padding(3);
            this.tabKorean.Size = new System.Drawing.Size(1052, 491);
            this.tabKorean.TabIndex = 6;
            this.tabKorean.Text = "Korean";
            this.tabKorean.UseVisualStyleBackColor = true;
            // 
            // dataGridKorean
            // 
            this.dataGridKorean.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridKorean.Location = new System.Drawing.Point(6, 6);
            this.dataGridKorean.Name = "dataGridKorean";
            this.dataGridKorean.Size = new System.Drawing.Size(1040, 479);
            this.dataGridKorean.TabIndex = 1;
            // 
            // tabMexican
            // 
            this.tabMexican.Controls.Add(this.dataGridMexican);
            this.tabMexican.Location = new System.Drawing.Point(4, 22);
            this.tabMexican.Name = "tabMexican";
            this.tabMexican.Padding = new System.Windows.Forms.Padding(3);
            this.tabMexican.Size = new System.Drawing.Size(1052, 491);
            this.tabMexican.TabIndex = 6;
            this.tabMexican.Text = "Mexican";
            this.tabMexican.UseVisualStyleBackColor = true;
            // 
            // dataGridMexican
            // 
            this.dataGridMexican.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridMexican.Location = new System.Drawing.Point(6, 6);
            this.dataGridMexican.Name = "dataGridMexican";
            this.dataGridMexican.Size = new System.Drawing.Size(1040, 479);
            this.dataGridMexican.TabIndex = 1;
            // 
            // tabPolish
            // 
            this.tabPolish.Controls.Add(this.dataGridPolish);
            this.tabPolish.Location = new System.Drawing.Point(4, 22);
            this.tabPolish.Name = "tabPolish";
            this.tabPolish.Padding = new System.Windows.Forms.Padding(3);
            this.tabPolish.Size = new System.Drawing.Size(1052, 491);
            this.tabPolish.TabIndex = 7;
            this.tabPolish.Text = "Polish";
            this.tabPolish.UseVisualStyleBackColor = true;
            // 
            // dataGridPolish
            // 
            this.dataGridPolish.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridPolish.Location = new System.Drawing.Point(6, 6);
            this.dataGridPolish.Name = "dataGridPolish";
            this.dataGridPolish.Size = new System.Drawing.Size(1040, 479);
            this.dataGridPolish.TabIndex = 2;
            // 
            // tabPortuguese
            // 
            this.tabPortuguese.Controls.Add(this.dataGridPortuguese);
            this.tabPortuguese.Location = new System.Drawing.Point(4, 22);
            this.tabPortuguese.Name = "tabPortuguese";
            this.tabPortuguese.Padding = new System.Windows.Forms.Padding(3);
            this.tabPortuguese.Size = new System.Drawing.Size(1052, 491);
            this.tabPortuguese.TabIndex = 8;
            this.tabPortuguese.Text = "Portuguese";
            this.tabPortuguese.UseVisualStyleBackColor = true;
            // 
            // dataGridPortuguese
            // 
            this.dataGridPortuguese.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridPortuguese.Location = new System.Drawing.Point(6, 6);
            this.dataGridPortuguese.Name = "dataGridPortuguese";
            this.dataGridPortuguese.Size = new System.Drawing.Size(1040, 479);
            this.dataGridPortuguese.TabIndex = 2;
            // 
            // tabRussian
            // 
            this.tabRussian.Controls.Add(this.dataGridRussian);
            this.tabRussian.Location = new System.Drawing.Point(4, 22);
            this.tabRussian.Name = "tabRussian";
            this.tabRussian.Padding = new System.Windows.Forms.Padding(3);
            this.tabRussian.Size = new System.Drawing.Size(1052, 491);
            this.tabRussian.TabIndex = 5;
            this.tabRussian.Text = "Russian";
            this.tabRussian.UseVisualStyleBackColor = true;
            // 
            // dataGridRussian
            // 
            this.dataGridRussian.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRussian.Location = new System.Drawing.Point(6, 6);
            this.dataGridRussian.Name = "dataGridRussian";
            this.dataGridRussian.Size = new System.Drawing.Size(1040, 479);
            this.dataGridRussian.TabIndex = 1;
            // 
            // tabSpanish
            // 
            this.tabSpanish.Controls.Add(this.dataGridSpanish);
            this.tabSpanish.Location = new System.Drawing.Point(4, 22);
            this.tabSpanish.Name = "tabSpanish";
            this.tabSpanish.Padding = new System.Windows.Forms.Padding(3);
            this.tabSpanish.Size = new System.Drawing.Size(1052, 491);
            this.tabSpanish.TabIndex = 4;
            this.tabSpanish.Text = "Spanish";
            this.tabSpanish.UseVisualStyleBackColor = true;
            // 
            // dataGridSpanish
            // 
            this.dataGridSpanish.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridSpanish.Location = new System.Drawing.Point(6, 6);
            this.dataGridSpanish.Name = "dataGridSpanish";
            this.dataGridSpanish.Size = new System.Drawing.Size(1040, 479);
            this.dataGridSpanish.TabIndex = 1;
            // 
            // btnAddErrorCode
            // 
            this.btnAddErrorCode.Location = new System.Drawing.Point(12, 12);
            this.btnAddErrorCode.Name = "btnAddErrorCode";
            this.btnAddErrorCode.Size = new System.Drawing.Size(127, 23);
            this.btnAddErrorCode.TabIndex = 1;
            this.btnAddErrorCode.Text = "Add New Error Code";
            this.btnAddErrorCode.UseVisualStyleBackColor = true;
            this.btnAddErrorCode.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnLoadTextFiles
            // 
            this.btnLoadTextFiles.Location = new System.Drawing.Point(935, 12);
            this.btnLoadTextFiles.Name = "btnLoadTextFiles";
            this.btnLoadTextFiles.Size = new System.Drawing.Size(127, 23);
            this.btnLoadTextFiles.TabIndex = 2;
            this.btnLoadTextFiles.Text = "Load Text Files";
            this.btnLoadTextFiles.UseVisualStyleBackColor = true;
            this.btnLoadTextFiles.Click += new System.EventHandler(this.btnLoadTextFiles_Click);
            // 
            // btnSaveTextFiles
            // 
            this.btnSaveTextFiles.Location = new System.Drawing.Point(802, 12);
            this.btnSaveTextFiles.Name = "btnSaveTextFiles";
            this.btnSaveTextFiles.Size = new System.Drawing.Size(127, 23);
            this.btnSaveTextFiles.TabIndex = 3;
            this.btnSaveTextFiles.Text = "Save Text Files";
            this.btnSaveTextFiles.UseVisualStyleBackColor = true;
            this.btnSaveTextFiles.Click += new System.EventHandler(this.btnSaveTextFiles_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtLog);
            this.groupBox1.Location = new System.Drawing.Point(12, 564);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1060, 168);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Log";
            // 
            // txtLog
            // 
            this.txtLog.Location = new System.Drawing.Point(10, 19);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(1040, 143);
            this.txtLog.TabIndex = 0;
            // 
            // txtHeaderFile
            // 
            this.txtHeaderFile.Location = new System.Drawing.Point(233, 14);
            this.txtHeaderFile.Name = "txtHeaderFile";
            this.txtHeaderFile.Size = new System.Drawing.Size(563, 20);
            this.txtHeaderFile.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(163, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Header File:";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 745);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtHeaderFile);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSaveTextFiles);
            this.Controls.Add(this.btnLoadTextFiles);
            this.Controls.Add(this.btnAddErrorCode);
            this.Controls.Add(this.tabLanguages);
            this.Name = "frmMain";
            this.Text = "Rage Error Code Manager";
            this.tabLanguages.ResumeLayout(false);
            this.tabAmerican.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAmerican)).EndInit();
            this.tabChinese.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridChinese)).EndInit();
            this.tabFrench.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridFrench)).EndInit();
            this.tabGerman.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridGerman)).EndInit();
            this.tabItalian.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridItalian)).EndInit();
            this.tabJapanese.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridJapanese)).EndInit();
            this.tabKorean.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridKorean)).EndInit();
            this.tabMexican.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMexican)).EndInit();
            this.tabPolish.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPolish)).EndInit();
            this.tabPortuguese.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPortuguese)).EndInit();
            this.tabRussian.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRussian)).EndInit();
            this.tabSpanish.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridSpanish)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabLanguages;
        private System.Windows.Forms.TabPage tabAmerican;
        private System.Windows.Forms.TabPage tabChinese;
        private System.Windows.Forms.TabPage tabFrench;
        private System.Windows.Forms.TabPage tabGerman;
        private System.Windows.Forms.TabPage tabItalian;
        private System.Windows.Forms.TabPage tabJapanese;
        private System.Windows.Forms.TabPage tabKorean;
        private System.Windows.Forms.TabPage tabMexican;
        private System.Windows.Forms.TabPage tabPolish;
        private System.Windows.Forms.TabPage tabPortuguese;
        private System.Windows.Forms.TabPage tabRussian;
        private System.Windows.Forms.TabPage tabSpanish;
        private System.Windows.Forms.DataGridView dataGridAmerican;
        private System.Windows.Forms.DataGridView dataGridChinese;
        private System.Windows.Forms.DataGridView dataGridFrench;
        private System.Windows.Forms.DataGridView dataGridGerman;
        private System.Windows.Forms.DataGridView dataGridItalian;
        private System.Windows.Forms.DataGridView dataGridJapanese;
        private System.Windows.Forms.DataGridView dataGridKorean;
        private System.Windows.Forms.DataGridView dataGridMexican;
        private System.Windows.Forms.DataGridView dataGridPolish;
        private System.Windows.Forms.DataGridView dataGridPortuguese;
        private System.Windows.Forms.DataGridView dataGridSpanish;
        private System.Windows.Forms.DataGridView dataGridRussian;
        private System.Windows.Forms.Button btnAddErrorCode;
        private System.Windows.Forms.Button btnLoadTextFiles;
        private System.Windows.Forms.Button btnSaveTextFiles;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.TextBox txtHeaderFile;
        private System.Windows.Forms.Label label1;

    }
}

