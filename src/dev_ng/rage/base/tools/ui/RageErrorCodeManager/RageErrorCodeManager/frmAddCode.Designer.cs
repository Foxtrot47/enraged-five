﻿namespace RageErrorCodeManager
{
    partial class frmAddCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.txtNewErrorCode = new System.Windows.Forms.TextBox();
            this.lblNoOneCares = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(190, 51);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(109, 51);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // txtNewErrorCode
            // 
            this.txtNewErrorCode.Location = new System.Drawing.Point(12, 25);
            this.txtNewErrorCode.Name = "txtNewErrorCode";
            this.txtNewErrorCode.Size = new System.Drawing.Size(253, 20);
            this.txtNewErrorCode.TabIndex = 2;
            // 
            // lblNoOneCares
            // 
            this.lblNoOneCares.AutoSize = true;
            this.lblNoOneCares.Location = new System.Drawing.Point(12, 9);
            this.lblNoOneCares.Name = "lblNoOneCares";
            this.lblNoOneCares.Size = new System.Drawing.Size(158, 13);
            this.lblNoOneCares.TabIndex = 3;
            this.lblNoOneCares.Text = "Please enter a new error code...";
            // 
            // frmAddCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(277, 87);
            this.Controls.Add(this.lblNoOneCares);
            this.Controls.Add(this.txtNewErrorCode);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmAddCode";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Add New Error Code...";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TextBox txtNewErrorCode;
        private System.Windows.Forms.Label lblNoOneCares;
    }
}