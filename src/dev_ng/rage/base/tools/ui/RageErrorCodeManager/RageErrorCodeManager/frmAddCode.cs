﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RageErrorCodeManager
{
    public partial class frmAddCode : Form
    {
        public string ErrorCodeName = "";

        public frmAddCode()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            ErrorCodeName = txtNewErrorCode.Text;
        }
    }
}
