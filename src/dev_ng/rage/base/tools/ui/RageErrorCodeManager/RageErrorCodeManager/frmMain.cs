﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

//////////////////////////////////////////////////////////////////////////
// TODO:
// To anyone looking at this code. This wasn't meant to be a full fledged
// tool but seems to have turned into one. This was written in a very rigid
// and fast way to validate and generate new error codes. This should be
// re-factored at some point and cleaned up to use a lot of the newer RSG
// C# assemblies.
//////////////////////////////////////////////////////////////////////////

namespace RageErrorCodeManager
{
    public partial class frmMain : Form
    {
        public enum LanguageEnum
        {
            AMERICAN,
            CHINESE,
            FRENCH,
            GERMAN,
            ITALIAN,
            JAPANESE,
            KOREAN,
            MEXICAN,
            POLISH,
            PORTUGUESE,
            RUSSIAN,
            SPANISH
        };

        public class SortableBindingList<T> : BindingList<T> where T : class
        {
            private bool _isSorted;
            private ListSortDirection _sortDirection = ListSortDirection.Ascending;
            private PropertyDescriptor _sortProperty;

            /// <summary>
            /// Initializes a new instance of the <see cref="SortableBindingList{T}"/> class.
            /// </summary>
            public SortableBindingList()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="SortableBindingList{T}"/> class.
            /// </summary>
            /// <param name="list">An <see cref="T:System.Collections.Generic.IList`1" /> of items to be contained in the <see cref="T:System.ComponentModel.BindingList`1" />.</param>
            public SortableBindingList(IList<T> list)
                : base(list)
            {
            }

            /// <summary>
            /// Gets a value indicating whether the list supports sorting.
            /// </summary>
            protected override bool SupportsSortingCore
            {
                get { return true; }
            }

            /// <summary>
            /// Gets a value indicating whether the list is sorted.
            /// </summary>
            protected override bool IsSortedCore
            {
                get { return _isSorted; }
            }

            /// <summary>
            /// Gets the direction the list is sorted.
            /// </summary>
            protected override ListSortDirection SortDirectionCore
            {
                get { return _sortDirection; }
            }

            /// <summary>
            /// Gets the property descriptor that is used for sorting the list if sorting is implemented in a derived class; otherwise, returns null
            /// </summary>
            protected override PropertyDescriptor SortPropertyCore
            {
                get { return _sortProperty; }
            }

            /// <summary>
            /// Removes any sort applied with ApplySortCore if sorting is implemented
            /// </summary>
            protected override void RemoveSortCore()
            {
                _sortDirection = ListSortDirection.Ascending;
                _sortProperty = null;
            }

            /// <summary>
            /// Sorts the items if overridden in a derived class
            /// </summary>
            /// <param name="prop"></param>
            /// <param name="direction"></param>
            protected override void ApplySortCore(PropertyDescriptor prop, ListSortDirection direction)
            {
                _sortProperty = prop;
                _sortDirection = direction;

                List<T> list = Items as List<T>;
                if (list == null) return;

                list.Sort(Compare);

                _isSorted = true;
                //fire an event that the list has been changed.
                OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
            }


            private int Compare(T lhs, T rhs)
            {
                var result = OnComparison(lhs, rhs);
                //invert if descending
                if (_sortDirection == ListSortDirection.Descending)
                    result = -result;
                return result;
            }

            private int OnComparison(T lhs, T rhs)
            {
                object lhsValue = lhs == null ? null : _sortProperty.GetValue(lhs);
                object rhsValue = rhs == null ? null : _sortProperty.GetValue(rhs);
                if (lhsValue == null)
                {
                    return (rhsValue == null) ? 0 : -1; //nulls are equal
                }
                if (rhsValue == null)
                {
                    return 1; //first has value, second doesn't
                }
                if (lhsValue is IComparable)
                {
                    return ((IComparable)lhsValue).CompareTo(rhsValue);
                }
                if (lhsValue.Equals(rhsValue))
                {
                    return 0; //both are the same
                }
                //not comparable, compare ToString
                return lhsValue.ToString().CompareTo(rhsValue.ToString());
            }
        }

        public class ErrorEntry : INotifyPropertyChanged
        {
            public string _code;
            public string _errorString;

            public event PropertyChangedEventHandler PropertyChanged;

            public string ErrorCode
            {
                get { return _code; }
                set
                {
                    _code = value;
                    this.NotifyPropertyChanged("ErrorCode");
                }
            }

            public string ErrorString
            {
                get { return _errorString; }
                set
                {
                    _errorString = value;
                    this.NotifyPropertyChanged("ErrorString");
                }
            }

            public ErrorEntry(string code, string error)
            {
                _code = code;
                _errorString = error;
            }

            private void NotifyPropertyChanged(string name)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

            public override string ToString()
            {
                return ErrorCode;
            }
        }

        private BindingSource AmericanBindingSource = new BindingSource();
        public SortableBindingList<ErrorEntry> AmericanEntries = new SortableBindingList<ErrorEntry>();

        private BindingSource ChineseBindingSource = new BindingSource();
        public SortableBindingList<ErrorEntry> ChineseEntries = new SortableBindingList<ErrorEntry>();

        private BindingSource FrenchBindingSource = new BindingSource();
        public SortableBindingList<ErrorEntry> FrenchEntries = new SortableBindingList<ErrorEntry>();

        private BindingSource GermanBindingSource = new BindingSource();
        public SortableBindingList<ErrorEntry> GermanEntries = new SortableBindingList<ErrorEntry>();

        private BindingSource ItalianBindingSource = new BindingSource();
        public SortableBindingList<ErrorEntry> ItalianEntries = new SortableBindingList<ErrorEntry>();

        private BindingSource JapaneseBindingSource = new BindingSource();
        public SortableBindingList<ErrorEntry> JapaneseEntries = new SortableBindingList<ErrorEntry>();

        private BindingSource KoreanBindingSource = new BindingSource();
        public SortableBindingList<ErrorEntry> KoreanEntries = new SortableBindingList<ErrorEntry>();

        private BindingSource MexicanBindingSource = new BindingSource();
        public SortableBindingList<ErrorEntry> MexicanEntries = new SortableBindingList<ErrorEntry>();

        private BindingSource PolishBindingSource = new BindingSource();
        public SortableBindingList<ErrorEntry> PolishEntries = new SortableBindingList<ErrorEntry>();

        private BindingSource PortugueseBindingSource = new BindingSource();
        public SortableBindingList<ErrorEntry> PortugueseEntries = new SortableBindingList<ErrorEntry>();

        private BindingSource RussianBindingSource = new BindingSource();
        public SortableBindingList<ErrorEntry> RussianEntries = new SortableBindingList<ErrorEntry>();

        private BindingSource SpanishBindingSource = new BindingSource();
        public SortableBindingList<ErrorEntry> SpanishEntries = new SortableBindingList<ErrorEntry>();

        public Dictionary<string, SortableBindingList<ErrorEntry>> AllLangEntires = new Dictionary<string, SortableBindingList<ErrorEntry>>();
        public List<DataGridView> AllLangDataGrids = new List<DataGridView>();

        public frmMain()
        {
            InitializeComponent();

            // American
            AmericanBindingSource.DataSource = AmericanEntries;
            dataGridAmerican.DataSource = AmericanBindingSource;
            dataGridAmerican.Tag = (object)"American";

            // Chinese
            ChineseBindingSource.DataSource = ChineseEntries;
            dataGridChinese.DataSource = ChineseBindingSource;
            dataGridChinese.Tag = (object)"Chinese";

            // French
            FrenchBindingSource.DataSource = FrenchEntries;
            dataGridFrench.DataSource = FrenchBindingSource;
            dataGridFrench.Tag = (object)"French";

            // German
            GermanBindingSource.DataSource = GermanEntries;
            dataGridGerman.DataSource = GermanBindingSource;
            dataGridGerman.Tag = (object)"German";

            // Italian
            ItalianBindingSource.DataSource = ItalianEntries;
            dataGridItalian.DataSource = ItalianBindingSource;
            dataGridItalian.Tag = (object)"Italian";

            // Japanese
            JapaneseBindingSource.DataSource = JapaneseEntries;
            dataGridJapanese.DataSource = JapaneseBindingSource;
            dataGridJapanese.Tag = (object)"Japanese";

            // Korean
            KoreanBindingSource.DataSource = KoreanEntries;
            dataGridKorean.DataSource = KoreanBindingSource;
            dataGridKorean.Tag = (object)"Korean";

            // Mexican
            MexicanBindingSource.DataSource = MexicanEntries;
            dataGridMexican.DataSource = MexicanBindingSource;
            dataGridMexican.Tag = (object)"Mexican";

            // Polish
            PolishBindingSource.DataSource = PolishEntries;
            dataGridPolish.DataSource = PolishBindingSource;
            dataGridPolish.Tag = (object)"Polish";

            // Portuguese
            PortugueseBindingSource.DataSource = PortugueseEntries;
            dataGridPortuguese.DataSource = PortugueseBindingSource;
            dataGridPortuguese.Tag = (object)"Portuguese";

            // Russian
            RussianBindingSource.DataSource = RussianEntries;
            dataGridRussian.DataSource = RussianBindingSource;
            dataGridRussian.Tag = (object)"Russian";

            // Spanish
            SpanishBindingSource.DataSource = SpanishEntries;
            dataGridSpanish.DataSource = SpanishBindingSource;
            dataGridSpanish.Tag = (object)"Spanish";

            // Add all languages for easier code entries
            AllLangEntires.Add("American", AmericanEntries);
            AllLangEntires.Add("Chinese", ChineseEntries);
            AllLangEntires.Add("French", FrenchEntries);
            AllLangEntires.Add("German", GermanEntries);
            AllLangEntires.Add("Italian", ItalianEntries);
            AllLangEntires.Add("Japanese", JapaneseEntries);
            AllLangEntires.Add("Korean", KoreanEntries);
            AllLangEntires.Add("Mexican", MexicanEntries);
            AllLangEntires.Add("Polish", PolishEntries);
            AllLangEntires.Add("Portuguese", PortugueseEntries);
            AllLangEntires.Add("Russian", RussianEntries);
            AllLangEntires.Add("Spanish", SpanishEntries);

            AllLangDataGrids.Add(dataGridAmerican);
            AllLangDataGrids.Add(dataGridChinese);
            AllLangDataGrids.Add(dataGridFrench);
            AllLangDataGrids.Add(dataGridGerman);
            AllLangDataGrids.Add(dataGridItalian);
            AllLangDataGrids.Add(dataGridJapanese);
            AllLangDataGrids.Add(dataGridKorean);
            AllLangDataGrids.Add(dataGridMexican);
            AllLangDataGrids.Add(dataGridPolish);
            AllLangDataGrids.Add(dataGridPortuguese);
            AllLangDataGrids.Add(dataGridRussian);
            AllLangDataGrids.Add(dataGridSpanish);

            foreach (DataGridView dgv in AllLangDataGrids)
            {
                dgv.EditMode = DataGridViewEditMode.EditOnEnter;
                dgv.ReadOnly = false;
                dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                dgv.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dgv.Columns[0].ReadOnly = true;
                dgv.UserDeletingRow += new DataGridViewRowCancelEventHandler(dataGrid_UserDeletingRow);
                dgv.Columns[0].SortMode = DataGridViewColumnSortMode.Automatic;
            }

            txtHeaderFile.Text = "X:/gta5/src/dev_ng/rage/base/src/diag/errorcodes.h";
        }

        // taken from rage atl/map.cpp
        uint ComputeHash(string str)
        {
            if (str == null || str.Equals(""))
            {
                return 0;
            }

            //Convert .Net Wave name to a system string and then to a Rage hash code.
            byte[] utf8 = System.Text.Encoding.UTF8.GetBytes(str);


            // This is the one-at-a-time hash from this page:
            // http://burtleburtle.net/bob/hash/doobs.html
            // (borrowed from /soft/swat/src/swcore/string2key.cpp)
            uint key = 0;

            for (int i = 0; i < str.Length; i++)
            {
                byte character = utf8[i];
                if (character >= 'A' && character <= 'Z')
                    character += 'a' - 'A';
                else if (character == '\\')
                    character = (byte)'/';

                key += character;
                key += (key << 10);	//lint !e701
                key ^= (key >> 6);	//lint !e702
            }

            // This is "Finalise Hash" in the HashComputer which is set by default so keep it on.
            if (true)
            {
                key += (key << 3);	//lint !e701
                key ^= (key >> 11);	//lint !e702
                key += (key << 15);	//lint !e701
            }

            // The original swat code did several tests at this point to make
            // sure that the resulting value was representable as a valid
            // floating-point number (not a negative zero, or a NaN or Inf)
            // and also reserved a nonzero value to indicate a bad key.
            // We don't do this here for generality but the caller could
            // obviously add such checks again in higher-level code.
            return key;
        }

        private void SortLists(bool forceAscending)
        {
            foreach (DataGridView dgv in AllLangDataGrids)
            {
                ListSortDirection sortDirection = (dgv.SortOrder == SortOrder.Ascending || forceAscending) ? ListSortDirection.Ascending : ListSortDirection.Descending;
                dgv.Sort(dgv.Columns[0], sortDirection);
            }
        }

        void dataGrid_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete?\nThis will remove the error code for each language.", "Remove Code", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                e.Cancel = true;
            }
            else
            {
                ErrorEntry entry = ((ErrorEntry)e.Row.DataBoundItem);
                DataGridView senderDGV = (DataGridView)sender;
                string removedFromLanguage = (string)senderDGV.Tag;

                foreach (KeyValuePair<string, SortableBindingList<ErrorEntry>> kvp in AllLangEntires)
                {
                    if (kvp.Key != removedFromLanguage)
                    {
                        ErrorEntry removeEntry = null;

                        foreach (ErrorEntry langEntry in kvp.Value)
                        {
                            if (entry.ErrorCode == langEntry.ErrorCode)
                            {
                                removeEntry = langEntry;
                                break;
                            }
                        }

                        if (removeEntry != null)
                        {
                            kvp.Value.Remove(removeEntry);
                        }
                    }
                }
            }

            SortLists(false);
        }

        void dataGrid_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            DataGridView senderDGV = (DataGridView)sender;
            string removedFromLanguage = (string)senderDGV.Tag;
            int index = e.RowIndex;
            string errorCode = AllLangEntires[removedFromLanguage][index].ErrorCode;

            foreach (KeyValuePair<string, SortableBindingList<ErrorEntry>> kvp in AllLangEntires)
            {
                if (kvp.Key != removedFromLanguage)
                {
                    ErrorEntry removeEntry = null;

                    foreach (ErrorEntry entry in kvp.Value)
                    {
                        if (entry.ErrorCode == errorCode)
                        {
                            removeEntry = entry;
                            break;
                        }
                    }

                    if (removeEntry != null)
                    {
                        kvp.Value.Remove(removeEntry);
                    }
                }
            }

            SortLists(false);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmAddCode addNewCodeForm = new frmAddCode();
            if (addNewCodeForm.ShowDialog() == DialogResult.OK)
            {
                if (addNewCodeForm.ErrorCodeName != "")
                {
                    foreach (KeyValuePair<string, SortableBindingList<ErrorEntry>> entry in AllLangEntires)
                    {
                        string errorCode = addNewCodeForm.ErrorCodeName.ToUpper();
                        ErrorEntry newEntry = new ErrorEntry(errorCode, "<Enter an error code>");
                        entry.Value.Add(newEntry);
                    }
                }

                SortLists(false);
            }
        }

        public bool ValidateLanguageData()
        {
            int errorCount = 0;
            List<string> errorCodes = new List<string>();

            foreach (ErrorEntry entry in AmericanEntries)
                errorCodes.Add(entry.ErrorCode);

            Dictionary<String, List<ErrorEntry>> missingEntries = new Dictionary<String, List<ErrorEntry>>();

            // Read the american as the base language.
            foreach (KeyValuePair<string, SortableBindingList<ErrorEntry>> errorList in AllLangEntires)
            {
                if (errorList.Key != "American")
                {
                    List<string> currentLangErrorCodes = new List<string>();

                    foreach (ErrorEntry errorEntry in errorList.Value)
                    {
                        currentLangErrorCodes.Add(errorEntry.ErrorCode);

                        if (!errorCodes.Contains(errorEntry.ErrorCode))
                        {
                            errorCount++;
                            txtLog.Text += string.Format("{0} Language : American list does not contain definition for {1}.\r\n", errorList.Key, errorEntry.ErrorCode);
                        }
                    }

                    foreach (string errorCode in errorCodes)
                    {
                        if (!currentLangErrorCodes.Contains(errorCode))
                        {
                            errorCount++;
                            txtLog.Text += string.Format("{0} Language : {0} Language does not contain definition for {1} in the American list.\r\n", errorList.Key, errorCode);

                            if (!missingEntries.ContainsKey(errorList.Key))
                            {
                                missingEntries.Add(errorList.Key, new List<ErrorEntry>());
                            }

                            ErrorEntry missingEntry = new ErrorEntry(errorCode, "<Enter an error code>");
                            missingEntries[errorList.Key].Add(missingEntry);
                        }
                    }
                }
            }

            foreach (KeyValuePair<String, List<ErrorEntry>> kvp in missingEntries)
            {
                String region = kvp.Key;
                List<ErrorEntry> errors = kvp.Value;

                foreach (ErrorEntry entry in errors)
                {
                    AllLangEntires[region].Add(entry);
                }
            }

            if (errorCount > 0)
            {
                txtLog.Text += string.Format("Total Errors: {0}", errorCount);
                return true;
            }
            else
            {
                txtLog.Text = "No Errors In Language Entries...";
            }
            
            return true;
        }

        public void LoadTextFile(string filename, LanguageEnum lang)
        {
            if (File.Exists(filename))
            {
                SortableBindingList<ErrorEntry> list = new SortableBindingList<ErrorEntry>();
                StreamReader reader = new StreamReader(filename);

                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (line.StartsWith("{"))
                        continue;

                    // Found an error tag
                    if (line.StartsWith("["))
                    {
                        string[] splitLine = line.Split(new char[] { '[', ']' }, StringSplitOptions.RemoveEmptyEntries);

                        if (splitLine.Length != 1)
                        {
                            txtLog.Text = string.Format("Error reading error code line: {0} - {1}\r\n", line, filename);
                            continue;
                        }

                        string code = splitLine[0];
                        string message = reader.ReadLine();

                        ErrorEntry errorEntry = new ErrorEntry(code, message);
                        list.Add(errorEntry);
                    }
                }

                switch(lang)
                {
                    case LanguageEnum.AMERICAN:
                        {
                            AmericanEntries.Clear();
                            foreach (ErrorEntry entry in list)
                                AmericanEntries.Add(entry);
                        }
                        break;
                    case LanguageEnum.CHINESE:
                        {
                            ChineseEntries.Clear();
                            foreach (ErrorEntry entry in list)
                                ChineseEntries.Add(entry);
                        }
                        break;
                    case LanguageEnum.FRENCH:
                        {
                            FrenchEntries.Clear();
                            foreach (ErrorEntry entry in list)
                                FrenchEntries.Add(entry);
                        }
                        break;
                    case LanguageEnum.GERMAN:
                        {
                            GermanEntries.Clear();
                            foreach (ErrorEntry entry in list)
                                GermanEntries.Add(entry);
                        }
                        break;
                    case LanguageEnum.ITALIAN:
                        {
                            ItalianEntries.Clear();
                            foreach (ErrorEntry entry in list)
                                ItalianEntries.Add(entry);
                        }
                        break;
                    case LanguageEnum.JAPANESE:
                        {
                            JapaneseEntries.Clear();
                            foreach (ErrorEntry entry in list)
                                JapaneseEntries.Add(entry);
                        }
                        break;
                    case LanguageEnum.KOREAN:
                        {
                            KoreanEntries.Clear();
                            foreach (ErrorEntry entry in list)
                                KoreanEntries.Add(entry);
                        }
                        break;
                    case LanguageEnum.MEXICAN:
                        {
                            MexicanEntries.Clear();
                            foreach (ErrorEntry entry in list)
                                MexicanEntries.Add(entry);
                        }
                        break;
                    case LanguageEnum.POLISH:
                        {
                            PolishEntries.Clear();
                            foreach (ErrorEntry entry in list)
                                PolishEntries.Add(entry);
                        }
                        break;
                    case LanguageEnum.PORTUGUESE:
                        {
                            PortugueseEntries.Clear();
                            foreach (ErrorEntry entry in list)
                                PortugueseEntries.Add(entry);
                        }
                        break;
                    case LanguageEnum.RUSSIAN:
                        {
                            RussianEntries.Clear();
                            foreach (ErrorEntry entry in list)
                                RussianEntries.Add(entry);
                        }
                        break;
                    case LanguageEnum.SPANISH:
                        {
                            SpanishEntries.Clear();
                            foreach (ErrorEntry entry in list)
                                SpanishEntries.Add(entry);
                        }
                        break;
                }

                reader.Close();
            }

            SortLists(true);
        }

        public bool SaveHeaderFile()
        {
            string headerFileLocation = txtHeaderFile.Text;

            if (File.Exists(headerFileLocation))
            {
                FileInfo fInfo = new FileInfo(headerFileLocation);
                if (fInfo.IsReadOnly)
                {
                    MessageBox.Show("Header file is read-only.\nCheck out " + headerFileLocation + " before saving.", "Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            StreamWriter headerFile = new StreamWriter(headerFileLocation);

            string header = @"#ifndef _ERRORCODES_H_
#define _ERRORCODES_H_

namespace rage
{

// Fatal Error Codes to be passed into Quitf() calls.
//	- Enum values correspond to the hash of the error key strings.
//	- When adding/modifying/deleting error codes, please also update:
//		1) Localized text files in //depot/gta5/build/dev_ng/x64/data/errorcodes/
//			Use the RageErrorCodeManager tool in //depot/gta5/tools_ng/bin/RageErrorCodeManager/
//		2) Tracking spreadsheet in //depot/gta5/docs/Code/pc_errorcodes.xlsx
enum FatalErrorCode
{
	ERR_DEFAULT							    = 0,
";

            string footer = @"};

} // namespace rage

#endif // _ERRORCODES_H_
";

            headerFile.Write(header);

            foreach (ErrorEntry entry in AmericanEntries)
            {
                // Ignore the err_default code, it's already being set as 0 so we don't need a hash value.
                if (entry.ErrorCode.ToLower() == "err_default")
                    continue;

                uint uHash = ComputeHash(entry.ErrorCode);

                string tabSpaces = "";
                int indentLength = 40;

                int leftOver = indentLength - entry.ErrorCode.Length;
                int tabSpace = (int)Math.Ceiling((float)leftOver / 4);

                for (int i = 0; i < tabSpace; i++)
                    tabSpaces += "\t";

                headerFile.WriteLine(String.Format("\t{0}{1}= 0x{2:X},", entry.ErrorCode, tabSpaces, uHash));
            }

            headerFile.Write(footer);

            headerFile.Close();

            return true;
        }

        public void SaveTextFile(string filename, LanguageEnum lang)
        {
            StreamWriter writer = new StreamWriter(filename);

            writer.WriteLine("{Please don't hand-edit these files.}\r\n{This file was generated with RageErrorCodeManager.}\r\n");

            switch (lang)
            {
                case LanguageEnum.AMERICAN:
                    WriteErrorCodes(writer, AmericanEntries);
                    break;
                case LanguageEnum.CHINESE:
                    WriteErrorCodes(writer, ChineseEntries);
                    break;
                case LanguageEnum.FRENCH:
                    WriteErrorCodes(writer, FrenchEntries);
                    break;
                case LanguageEnum.GERMAN:
                    WriteErrorCodes(writer, GermanEntries);
                    break;
                case LanguageEnum.ITALIAN:
                    WriteErrorCodes(writer, ItalianEntries);
                    break;
                case LanguageEnum.JAPANESE:
                    WriteErrorCodes(writer, JapaneseEntries);
                    break;
                case LanguageEnum.KOREAN:
                    WriteErrorCodes(writer, KoreanEntries);
                    break;
                case LanguageEnum.MEXICAN:
                    WriteErrorCodes(writer, MexicanEntries);
                    break;
                case LanguageEnum.POLISH:
                    WriteErrorCodes(writer, PolishEntries);
                    break;
                case LanguageEnum.PORTUGUESE:
                    WriteErrorCodes(writer, PortugueseEntries);
                    break;
                case LanguageEnum.RUSSIAN:
                    WriteErrorCodes(writer, RussianEntries);
                    break;
                case LanguageEnum.SPANISH:
                    WriteErrorCodes(writer, SpanishEntries);
                    break;
            }

            writer.Close();
        }

        public void WriteErrorCodes(StreamWriter writer, BindingList<ErrorEntry> entries)
        {
            foreach (ErrorEntry entry in entries)
            {
                writer.WriteLine("[{0}]", entry.ErrorCode);
                writer.WriteLine(entry.ErrorString + "\r\n");
            }
        }

        private void btnLoadTextFiles_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.RootFolder = Environment.SpecialFolder.MyComputer;

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                string dir = fbd.SelectedPath;

                LoadTextFile(Path.Combine(dir, "american.txt"), LanguageEnum.AMERICAN);
                LoadTextFile(Path.Combine(dir, "chinese.txt"), LanguageEnum.CHINESE);
                LoadTextFile(Path.Combine(dir, "french.txt"), LanguageEnum.FRENCH);
                LoadTextFile(Path.Combine(dir, "german.txt"), LanguageEnum.GERMAN);
                LoadTextFile(Path.Combine(dir, "italian.txt"), LanguageEnum.ITALIAN);
                LoadTextFile(Path.Combine(dir, "japanese.txt"), LanguageEnum.JAPANESE);
                LoadTextFile(Path.Combine(dir, "korean.txt"), LanguageEnum.KOREAN);
                LoadTextFile(Path.Combine(dir, "mexican.txt"), LanguageEnum.MEXICAN);
                LoadTextFile(Path.Combine(dir, "polish.txt"), LanguageEnum.POLISH);
                LoadTextFile(Path.Combine(dir, "portuguese.txt"), LanguageEnum.PORTUGUESE);
                LoadTextFile(Path.Combine(dir, "russian.txt"), LanguageEnum.RUSSIAN);
                LoadTextFile(Path.Combine(dir, "spanish.txt"), LanguageEnum.SPANISH);

                ValidateLanguageData();
            }
        }

        private void btnSaveTextFiles_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.RootFolder = Environment.SpecialFolder.MyComputer;

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                string dir = fbd.SelectedPath;

                if (ValidateLanguageData())
                {
                    SortLists(true);

                    if (!SaveHeaderFile())
                        return;

                    SaveTextFile(Path.Combine(dir, "american.txt"), LanguageEnum.AMERICAN);
                    SaveTextFile(Path.Combine(dir, "chinese.txt"), LanguageEnum.CHINESE);
                    SaveTextFile(Path.Combine(dir, "french.txt"), LanguageEnum.FRENCH);
                    SaveTextFile(Path.Combine(dir, "german.txt"), LanguageEnum.GERMAN);
                    SaveTextFile(Path.Combine(dir, "italian.txt"), LanguageEnum.ITALIAN);
                    SaveTextFile(Path.Combine(dir, "japanese.txt"), LanguageEnum.JAPANESE);
                    SaveTextFile(Path.Combine(dir, "korean.txt"), LanguageEnum.KOREAN);
                    SaveTextFile(Path.Combine(dir, "mexican.txt"), LanguageEnum.MEXICAN);
                    SaveTextFile(Path.Combine(dir, "polish.txt"), LanguageEnum.POLISH);
                    SaveTextFile(Path.Combine(dir, "portuguese.txt"), LanguageEnum.PORTUGUESE);
                    SaveTextFile(Path.Combine(dir, "russian.txt"), LanguageEnum.RUSSIAN);
                    SaveTextFile(Path.Combine(dir, "spanish.txt"), LanguageEnum.SPANISH);
                }
                else
                {
                    MessageBox.Show("Please fix errors shown below before saving.", "Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
