﻿// 
// MissingPlatform
// Copyright (C) 2013 Rockstar Games.  All Rights Reserved. 
//
// A tool for parsing through all of the .h and .cpp files of a given folder,
// and examine the code for platform-specific #defines.
//
//  USAGE: MissingPlatform.exe {path} {outpath} {platform} {explicit} {singles} {excludes}
//   Path:
//     If no path is specified, the current directory is used
//   Outpath:
//     If no outpath is specified, the current directory is used
//   Platform:
//    -nextgen : Look for missing PC, Durango & Orbis (DEFAULT)
//    -pc : Look for missing PC
//    -orbis : Look for missing Orbis only
//    -durango : Look for missing durango only
//   Explicit:
//    -explicit : Treat #else cases as unhandled (DEFAULT)
//    -implicit : Treat #else cases as handled
//   Singles:
//    -singles : Include #if with only a single platform defined
//    -nosingles : Include #if with only a single platform defined
//    -lastgensingles : Include #if with only a single platform defined if it matches the specified -platform (DEFAULT)
//   Excludes:
//    Path to a file that lists paths and functions to exclude (Defaults to "exclude.txt")
//
// EXAMPLE USAGE:
//   MissingPlatform.exe X:\gta5\src\dev\rage\base\src\net\ results.txt -nextgen -implicit -lastgensingles exclude.txt

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MissingPlatform
{  
    // Result set defines the parameters of the result output, and contains a list of all the 
    // platform define blocks that are determined to be not well-formed.
    class ResultSet
    {
        static ResultSet sm_ResultSet = null;

        public static ResultSet Instance()
        {
            if (sm_ResultSet == null)
            {
                sm_ResultSet = new ResultSet();
                sm_ResultSet.Results = new List<PlatformDefineBlock>();
            }

            return sm_ResultSet;
        }

        public List<PlatformDefineBlock> Results;
        public int WellFormedBlocks = 0;
        public int NonWellFormedBlocks = 0;
        public OutType OutputTypes;
        public bool bExplicit;
        public SingleRules Singles;


    };

    public enum SingleRules
    {
        Singles,
        NoSingles,
        LastGen,
    };

    public enum OutType
    {
        NextGen,
        PC,
        Durango,
        Orbis
    };

    public enum WellFormedStatus
    {
        Singular_Define,
        Singular_ElseCaseIncluded,
        LastGen_Only,
        LastGen_Else,
        LastGen_PC_Only,
        LastGen_PC_Else,
        MissingPC,
        MissingDurango,
        MissingOrbis,
        FullyFormed,
    };

    class PlatformDefineBlock
    {
        public List<String> Buffer;
        public int m_StartLine;
        public string m_FileName;

        bool bXenon;
        bool bPS3;
        bool bPC;
        bool bDurango;
        bool bOrbis;
        bool bElseCase;
        WellFormedStatus m_Status;

        string[] sXenonStrings = { "__CONSOLE", "__XENON", "__LIVE", "__WIN32", "RSG_XENON" };
        string[] sPS3Strings = { "__CONSOLE", "__PS3", "__PPU", "__SPU", "RSG_NP", "RSG_PS3", "RSG_PPU", "RSG_SPU" };
        string[] sPCStrings = { "__WIN32", "__WIN32PC", "RSG_PC", "RLROS_SC_PLATFORM" };
        string[] sDurangoStrings = { "__CONSOLE", "__WIN32", "RSG_DURANGO" };
        string[] sOrbisStrings = { "__CONSOLE", "RSG_ORBIS", "RSG_NP" };

        static Int64 identifier = 0;
        public Int64 m_Identifier;

        public PlatformDefineBlock()
        {
            bXenon = false;
            bPS3 = false;
            bPC = false;
            bDurango = false;
            bOrbis = false;
            bElseCase = false;

            Buffer = new List<String>();
            m_Identifier = identifier;
            identifier++;
        }

        public int Parse(string line, StreamReader sr, string fileName, int counter)
        {
            m_StartLine = counter;
            m_FileName = fileName;

            int myCounter = 0;

            ParseLine(line);
            Buffer.Add(line);

            // Read until the end of file
            while ((line = sr.ReadLine()) != null)
            {
                // Increment line counter, add to buffer
                myCounter++;
                Buffer.Add(line);

                // Look for a nested #if block
                if (line.Contains("#if"))
                {
                    PlatformDefineBlock block = new PlatformDefineBlock();
                    myCounter += block.Parse(line, sr, m_FileName, m_StartLine + myCounter);

                    // If the set isn't well formed according to our rules, add it to the list
                    if (!block.IsWellFormed())
                    {
                        ResultSet.Instance().Results.Add(block);
                        ResultSet.Instance().NonWellFormedBlocks++;
                    }
                    else
                    {
                        ResultSet.Instance().WellFormedBlocks++;
                    }

                    // after parsing nested if block, add the text to our buffer 
                    int count = 0;
                    foreach (string str in block.Buffer)
                    {
                        // ignore the shared line
                        if (count != 0)
                            Buffer.Add(str);
                        count++;
                    }
                }
                // Parse #elif blocks, looking for more platofrms
                else if (line.Contains("#elif"))
                {
                    ParseLine(line);
                }
                // closing block
                else if (line.Contains("#endif"))
                {
                    return myCounter;
                }
                // make a note of the Else case
                else if (line.Contains("#else"))
                {
                    bElseCase = true;
                }
            }

            return myCounter;
        }

        // Check if an #if block is well formed according to our standards
        public bool IsWellFormed()
        {
            // Evaluate the status of the current block
            GetWellFormedStatus();

            // Apply our rules to it
            if (ResultSet.Instance().bExplicit)
            {
                if (ResultSet.Instance().OutputTypes == OutType.PC ||
                    ResultSet.Instance().OutputTypes == OutType.NextGen && !bPC)
                {
                    if (m_Status == WellFormedStatus.Singular_Define ||
                        m_Status == WellFormedStatus.Singular_ElseCaseIncluded)
                    {
                        if (ResultSet.Instance().Singles == SingleRules.Singles)
                            return false;
                        else
                            return true;
                    }
                    else if (m_Status == WellFormedStatus.LastGen_Only ||
                        m_Status == WellFormedStatus.LastGen_Else ||
                        m_Status == WellFormedStatus.MissingPC)
                    {
                        return false;
                    }
                }

                if (ResultSet.Instance().OutputTypes == OutType.Orbis ||
                    ResultSet.Instance().OutputTypes == OutType.NextGen && !bOrbis)
                {
                    if (m_Status == WellFormedStatus.Singular_Define ||
                        m_Status == WellFormedStatus.Singular_ElseCaseIncluded)
                    {
                        if (ResultSet.Instance().Singles == SingleRules.Singles)
                            return false;
                        else if (ResultSet.Instance().Singles == SingleRules.NoSingles)
                            return true;
                        else if (ResultSet.Instance().Singles == SingleRules.LastGen && bPS3)
                            return false;
                    }
                    else if (m_Status == WellFormedStatus.LastGen_Only ||
                        m_Status == WellFormedStatus.LastGen_Else ||
                        m_Status == WellFormedStatus.LastGen_PC_Else ||
                        m_Status == WellFormedStatus.LastGen_PC_Only ||
                        m_Status == WellFormedStatus.MissingOrbis)
                    {
                        return false;
                    }
                }

                if (ResultSet.Instance().OutputTypes == OutType.Durango ||
                    ResultSet.Instance().OutputTypes == OutType.NextGen && !bDurango)
                {
                    if (m_Status == WellFormedStatus.Singular_Define ||
                        m_Status == WellFormedStatus.Singular_ElseCaseIncluded)
                    {
                        if (ResultSet.Instance().Singles == SingleRules.Singles)
                            return false;
                        else if (ResultSet.Instance().Singles == SingleRules.NoSingles)
                            return true;
                        else if (ResultSet.Instance().Singles == SingleRules.LastGen && bXenon)
                            return false;
                    }
                    else if (m_Status == WellFormedStatus.LastGen_Only ||
                        m_Status == WellFormedStatus.LastGen_Else ||
                        m_Status == WellFormedStatus.LastGen_PC_Else ||
                        m_Status == WellFormedStatus.LastGen_PC_Only ||
                        m_Status == WellFormedStatus.MissingDurango)
                    {
                        return false;
                    }
                }
            }
            else
            {
                if (ResultSet.Instance().OutputTypes == OutType.PC ||
                    ResultSet.Instance().OutputTypes == OutType.NextGen && !bPC)
                {
                    if (m_Status == WellFormedStatus.Singular_Define)
                    {
                        if (ResultSet.Instance().Singles == SingleRules.Singles)
                            return false;
                        else
                            return true;
                    }
                    else if (!bElseCase && (m_Status == WellFormedStatus.LastGen_Only ||
                                            m_Status == WellFormedStatus.MissingPC))
                    {
                        return false;
                    }
                }

                if (ResultSet.Instance().OutputTypes == OutType.Orbis ||
                    ResultSet.Instance().OutputTypes == OutType.NextGen && !bOrbis)
                {
                    if (m_Status == WellFormedStatus.Singular_Define)
                    {
                        if (ResultSet.Instance().Singles == SingleRules.Singles)
                            return false;
                        else if (ResultSet.Instance().Singles == SingleRules.NoSingles)
                            return true;
                        else if (ResultSet.Instance().Singles == SingleRules.LastGen && bPS3)
                            return false;
                    }
                    else if (!bElseCase && (m_Status == WellFormedStatus.LastGen_Only ||
                                            m_Status == WellFormedStatus.LastGen_PC_Only ||
                                             m_Status == WellFormedStatus.MissingOrbis))
                    {
                        return false;
                    }
                }

                if (ResultSet.Instance().OutputTypes == OutType.Durango ||
                    ResultSet.Instance().OutputTypes == OutType.NextGen && !bDurango)
                {
                    if (m_Status == WellFormedStatus.Singular_Define)
                    {
                        if (ResultSet.Instance().Singles == SingleRules.Singles)
                            return false;
                        else if (ResultSet.Instance().Singles == SingleRules.NoSingles)
                            return true;
                        else if (ResultSet.Instance().Singles == SingleRules.LastGen && bXenon)
                            return false;
                    }
                    else if (!bElseCase && (m_Status == WellFormedStatus.LastGen_Only ||
                                             m_Status == WellFormedStatus.LastGen_PC_Only ||
                                             m_Status == WellFormedStatus.MissingDurango))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        // Output string based on found #if platforms for logging
        string GetDefinedStrings()
        {
            string outStr = "";

            if (bXenon)
                outStr += " Xbox,";
            if (bPS3)
                outStr += " PS3,";
            if (bPC)
                outStr += " PC,";
            if (bDurango)
                outStr += " Durango,";
            if (bOrbis)
                outStr += " Orbis,";
            if (bElseCase)
                outStr += " else";

            if (outStr[outStr.Length - 1] == ',')
                outStr = outStr.Substring(0, outStr.Length - 1);

            return outStr;
        }

        public string GetStatusString()
        {
            string outStr = "";

            switch (m_Status)
            {
                case WellFormedStatus.Singular_Define: 
                {
                    outStr = "Singular Define: Only" + GetDefinedStrings() + " is defined";
                    break; 
                }
                case WellFormedStatus.Singular_ElseCaseIncluded:
                {
                    outStr = "Singular define: Only" + GetDefinedStrings() + " are defined";
                    break; 
                }
                case WellFormedStatus.LastGen_Only:
                {
                    outStr = "Last gen defined: Only" + GetDefinedStrings() + " are defined";
                    break; 
                }
                case WellFormedStatus.LastGen_Else:
                {
                    outStr = "Last gen defined: Only" + GetDefinedStrings() + " are defined";
                    break; 
                }
                case WellFormedStatus.LastGen_PC_Only:
                {
                    outStr = "Last gen and PC: Only" + GetDefinedStrings() + " are defined";
                    break; 
                }
                case WellFormedStatus.LastGen_PC_Else:
                {
                    outStr = "Last gen and PC: Only" + GetDefinedStrings() + " are defined";
                    break;
                }
                case WellFormedStatus.MissingPC:
                {
                    outStr = "Missing PC: Only" + GetDefinedStrings() + "are defined";
                    break;
                }
                case WellFormedStatus.MissingDurango:
                {
                    outStr = "Missing Durango: Only" + GetDefinedStrings() + " are defined";
                    break; 
                }
                case WellFormedStatus.MissingOrbis:
                {
                    outStr = "Missing Orbis: Only" + GetDefinedStrings() + " are defined";
                    break; 
                }
                case WellFormedStatus.FullyFormed:
                {
                    outStr = "Fully formed.";
                    break; 
                }
                default:
                break;
            }

            return outStr;
        }

        void GetWellFormedStatus()
        {
            m_Status = WellFormedStatus.FullyFormed;

            int count = CountPlatformDefines();
            if (count == 1)
            {
                if (bElseCase)
                {
                    m_Status = WellFormedStatus.Singular_ElseCaseIncluded;
                }
                else
                {
                    m_Status = WellFormedStatus.Singular_Define;
                }
            }

            // Last Gen
            if (bXenon && bPS3)
            {
                if (!bDurango && !bOrbis)
                {
                    if (!bPC)
                    {
                        if (bElseCase)
                        {
                            m_Status = WellFormedStatus.LastGen_Else;
                        }
                        else
                        {
                            m_Status = WellFormedStatus.LastGen_Only;
                        }
                    }
                    else
                    {
                        if (bElseCase)
                        {
                            m_Status = WellFormedStatus.LastGen_PC_Else;
                        }
                        else
                        {
                            m_Status = WellFormedStatus.LastGen_PC_Only;
                        }
                    }
                }
                else if (!bDurango)
                {
                    m_Status = WellFormedStatus.MissingDurango;
                }
                else if (!bOrbis)
                {
                    m_Status = WellFormedStatus.MissingOrbis;
                }
                else if (!bPC)
                {
                    m_Status = WellFormedStatus.MissingPC;
                }
            }
        }

        int CountPlatformDefines()
        {
            int count = 0;

            if (bXenon) { count++; }
            if (bPS3) { count++; }
            if (bPC) { count++; }
            if (bDurango) { count++; }
            if (bOrbis) { count++; }

            return count;
        }

        void ParseLine(string line)
        {
            foreach (string str in sXenonStrings)
            {
                if (line.Contains(str))
                    bXenon = true;
            }

            foreach (string str in sPS3Strings)
            {
                if (line.Contains(str))
                    bPS3 = true;
            }

            foreach (string str in sPCStrings)
            {
                if (line.Contains(str))
                    bPC = true;
            }

            foreach (string str in sDurangoStrings)
            {
                if (line.Contains(str))
                    bDurango = true;
            }

            foreach (string str in sOrbisStrings)
            {
                if (line.Contains(str))
                    bOrbis = true;
            }
        }
    }

    class Program
    {
        static string rootDir = "";
        static string outPath = "";
        static string excludePath = "";
        static bool bHtml;

        static string[] headerFiles;
        static string[] sourceFiles;
        static List<String> excludeFiles = new List<String>();

        static bool ReadKeyAndExit(string message = "")
        {
            if (!string.IsNullOrEmpty(message))
                Console.WriteLine(message);
            Console.ReadKey();
            return false;
        }

        static void PrintUsage()
        {
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine("Usage: MissingPlatform.exe");
            Console.WriteLine("       {path} {outpath} {platform} {explicit} {singles} {excludes} {html}");
            Console.WriteLine(Environment.NewLine + "  Path:");
            Console.WriteLine("     If no path is specified, the current directory is used");
            Console.WriteLine(Environment.NewLine + "  Outpath:");
            Console.WriteLine("     If no outpath is specified, the current directory is used");
            Console.WriteLine(Environment.NewLine + "  Platform:");
            Console.WriteLine("    -nextgen : Look for missing PC, Durango & Orbis (DEFAULT)");
            Console.WriteLine("    -pc : Look for missing PC");
            Console.WriteLine("    -orbis : Look for missing Orbis only");
            Console.WriteLine("    -durango : Look for missing durango only");
            Console.WriteLine(Environment.NewLine + "  Explicit:");
            Console.WriteLine("     -explicit : Treat #else cases as unhandled (DEFAULT)");
            Console.WriteLine("     -implicit : Treat #else cases as handled");
            Console.WriteLine(Environment.NewLine + "  Singles:");
            Console.WriteLine("     -singles : Include #if with only a single platform defined");
            Console.WriteLine("     -nosingles : Include #if with only a single platform defined");
            Console.WriteLine("     -lastgensingles : Include #if with only a single platform defined if ");
            Console.WriteLine("                        it matches the specified -platform (DEFAULT)");
            Console.WriteLine(Environment.NewLine + "  Excludes:");
            Console.WriteLine("     Path to a file that lists paths and functions to exclude");
            Console.WriteLine("     Defaults to \"exclude.txt\"");
            Console.WriteLine(Environment.NewLine + "  Html:");
            Console.WriteLine("     -html : use HTML format instead of raw text");
            Console.WriteLine(Environment.NewLine);
        }

        static bool IntroAndSetup(string[] args)
        {
            rootDir = Directory.GetCurrentDirectory();

            ResultSet.Instance().bExplicit = true;
            ResultSet.Instance().Singles = SingleRules.LastGen;

            bHtml = false;

            if (args.Length >= 1)
            {
                if (args[0] == "-help" || args[0] == "-usage")
                {
                    PrintUsage();
                    return ReadKeyAndExit();
                }

                rootDir = args[0];

                try
                {
                    Path.GetFullPath(rootDir);
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine("Not a valid path: " + rootDir);
                    PrintUsage();
                    return ReadKeyAndExit("Error: " + ex.Message);
                }
            }

            if (args.Length >= 2)
            {
                outPath = args[1];

                if (!outPath.Contains("."))
                {
                    if (outPath.EndsWith("\\"))
                        outPath = outPath + "results.txt";
                    else
                        outPath = outPath + "\\results.txt";
                }

                try
                {
                    Path.GetFullPath(outPath);
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine("Not a valid path: " + outPath);
                    PrintUsage();
                    return ReadKeyAndExit(Environment.NewLine + "Error: " + ex.Message);
                }
            }
            else
            {
                outPath = Directory.GetCurrentDirectory() + "results.txt";
            }

            if (args.Length >= 3)
            {
                if (args[2] == "-nextgen")
                    ResultSet.Instance().OutputTypes = MissingPlatform.OutType.NextGen;
                else if (args[2] == "-pc")
                    ResultSet.Instance().OutputTypes = MissingPlatform.OutType.PC;
                else if (args[2] == "-orbis")
                    ResultSet.Instance().OutputTypes = MissingPlatform.OutType.Orbis;
                else if (args[2] == "-durango")
                    ResultSet.Instance().OutputTypes = MissingPlatform.OutType.Durango;
                else
                {
                    PrintUsage();
                    return ReadKeyAndExit(Environment.NewLine + "argv[2] " + args[2] + " is not a valid argument");
                }
            }

            if (args.Length >= 4)
            {
                if (args[3] == "-explicit")
                    ResultSet.Instance().bExplicit = true;
                else if (args[3] == "-implicit")
                    ResultSet.Instance().bExplicit = false;
                else
                {
                    PrintUsage();
                    return ReadKeyAndExit(Environment.NewLine + "argv[3] " + args[3] + " is not a valid argument");
                }
            }

            if (args.Length >= 5)
            {
                if (args[4] == "-singles")
                    ResultSet.Instance().Singles = SingleRules.Singles;
                else if (args[4] == "-nosingles")
                    ResultSet.Instance().Singles = SingleRules.NoSingles;
                else if (args[4] == "-lastgensingles")
                    ResultSet.Instance().Singles = SingleRules.LastGen;
                else
                {
                    PrintUsage();
                    return ReadKeyAndExit(Environment.NewLine + "argv[4] " + args[4] + " is not a valid argument");
                }
            }

            if (args.Length >= 6)
            {
                excludePath = args[5];

                try
                {
                    Path.GetFullPath(excludePath);
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine("Not a valid path: " + excludePath);
                    PrintUsage();
                    return ReadKeyAndExit("Error: " + ex.Message);
                }
            }
            else
            {
                excludePath = "exclude.txt";
            }

            if (args.Length >= 7)
            {
                if (args[6] == "-html")
                {
                    bHtml = true;
                }
                else
                {
                    PrintUsage();
                    return ReadKeyAndExit("");
                }
            }

            Console.WriteLine("MissingPlatform: Searching for missing platform #defines in directory:");
            Console.WriteLine("   " + rootDir + Environment.NewLine);

            return true;
        }

        static void GetAllFiles()
        {
            headerFiles = Directory.GetFiles(rootDir, "*.h", SearchOption.AllDirectories);
            sourceFiles = Directory.GetFiles(rootDir, "*.cpp", SearchOption.AllDirectories);
        }


        static void LoadExcludeFile()
        {
            try
            {
                StreamReader file = new StreamReader(excludePath);
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    excludeFiles.Add(line);
                }
            }
            catch (System.Exception)
            {

            }
        }

        static void ParseFile(string fileName)
        {
            // Filter exclude list
            foreach (String str in excludeFiles)
            {
                if (fileName.Contains(str))
                    return;
            }
            
            // filter platform specific files
            if (fileName.Contains("_psp.") || fileName.Contains("_orbis.") || fileName.Contains("_360.") || fileName.Contains("_win32."))
            {
                return;
            }

            StreamReader file = new StreamReader(fileName);
            string line;
            int counter = 0;
            while ((line = file.ReadLine()) != null)
            {
                counter++;
                if (line.Contains("#if"))
                {
                    PlatformDefineBlock block = new PlatformDefineBlock();
                    block.Parse(line, file,  fileName, counter);

                    if (!block.IsWellFormed())
                    {
                        ResultSet.Instance().Results.Add(block);
                        ResultSet.Instance().NonWellFormedBlocks++;
                    }
                    else
                    {
                        ResultSet.Instance().WellFormedBlocks++;
                    }
                }
            }
        }

        static void ParseFiles(string[] files)
        {
            foreach (string file in files)
            {
                ParseFile(file);
            }
        }

        static void PrintResults()
        {
            if (bHtml)
                outPath = outPath.Replace(".txt", ".html");

            System.IO.StreamWriter file = new System.IO.StreamWriter(outPath, false);

            if (bHtml)
            {
                file.WriteLine("<html>");
                file.WriteLine("<body>");
            }

            if (bHtml) file.Write("<p>"); 
                file.WriteLine("MissingPlatform results from " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " for: ");
            if (bHtml) file.Write("<BR>&nbsp;&nbsp;");
                file.WriteLine(" Search Directory: " + rootDir);
            if (bHtml) file.Write("<BR>");
                file.WriteLine(" Output File: " + outPath);
            if (bHtml) file.Write("<BR>");
                file.WriteLine(" Platform: " + ResultSet.Instance().OutputTypes.ToString());
            if (bHtml) file.Write("<BR>");
                file.WriteLine(ResultSet.Instance().bExplicit ? " Explicit Search" : " Implicit Search");
            if (bHtml) file.Write("<BR>");

            switch (ResultSet.Instance().Singles)
            {
                case SingleRules.LastGen:
                    file.WriteLine(" Last Gen Singles Only");
                    break;
                case SingleRules.NoSingles:
                    file.WriteLine(" No Singles");
                    break;
                case SingleRules.Singles:
                    file.WriteLine(" Singles");
                    break;
            }

            if (bHtml) file.Write("</p>");
            file.WriteLine("");

            // Print to Console
            Console.WriteLine("Found " + headerFiles.Length + " header .h files");
            Console.WriteLine("Found " + sourceFiles.Length + " source .cpp files");
            Console.WriteLine("Well formed #define blocks: " + ResultSet.Instance().WellFormedBlocks);
            Console.WriteLine("Potentially non-well formed #define blocks: " + ResultSet.Instance().NonWellFormedBlocks + Environment.NewLine);

            // Print to File
            if (bHtml) file.Write("<p>"); 
            file.WriteLine("Found " + headerFiles.Length + " header .h files");
            if (bHtml) file.Write("<BR>");
            file.WriteLine("Found " + sourceFiles.Length + " source .cpp files");
            if (bHtml) file.Write("<BR>");
            file.WriteLine("Well formed #define blocks: " + ResultSet.Instance().WellFormedBlocks);
            if (bHtml) file.Write("<BR>");
            file.WriteLine("Potentially non-well formed #define blocks: " + ResultSet.Instance().NonWellFormedBlocks + Environment.NewLine);
            if (bHtml) file.Write("</p>");


            if (bHtml)
                file.WriteLine("<h1 id=\"summary\">Summary</h1>");
            else
            {
                file.WriteLine("*************************************************");
                file.WriteLine("                   SUMMARY                       ");
                file.WriteLine("*************************************************");
            }

            // Summary
            if (bHtml) file.Write("<b>");
            file.WriteLine("Non-well formed #define blocks potentially found in: ");
            if (bHtml) file.Write("</b><BR>");

            foreach (PlatformDefineBlock block in ResultSet.Instance().Results)
            {
                if (bHtml)
                {
                    file.WriteLine(block.m_FileName + ", line: <a href=#" + block.m_Identifier + ">" + block.m_StartLine + "</a><BR>");
                }
                else
                {
                    file.WriteLine(block.m_FileName + ", line: " + block.m_StartLine);
                }                
            }

            if (bHtml) file.Write("<p>");
            file.WriteLine(Environment.NewLine);
            if (bHtml) file.Write("</p>");

            // Details
            foreach (PlatformDefineBlock block in ResultSet.Instance().Results)
            {
                if (bHtml) file.Write("<p id=\"" + block.m_Identifier + "\">");
                file.WriteLine("****************************************************************************");
                if (bHtml) file.Write("<BR>");

                if (bHtml)
                {
                    file.Write("<a href=\"" + block.m_FileName + "\">" + block.m_FileName + "</a>, line" + block.m_StartLine);
                    file.Write("&nbsp;&nbsp;<a href=\"#summary\">TOP</a>");
                }
                else
                {
                    file.WriteLine(block.m_FileName + ", line: " + block.m_StartLine);
                }

                if (bHtml) file.Write("<BR>");
                file.WriteLine("  Reason: " + block.GetStatusString());
                if (bHtml) file.Write("<BR>");
                file.WriteLine("***************************************************************************");
                if (bHtml) file.Write("<BR>");

                if (bHtml) file.Write("</p>");

                if (bHtml) file.Write("<p style=\"font-family:courier;\">");
                if (bHtml) file.Write("<pre style=\"tab-size:2\">");
                foreach (string str in block.Buffer)
                {
                    file.WriteLine(str);
                }
                if (bHtml) file.Write("</pre>");
                if (bHtml) file.Write("</p>");

                file.WriteLine(Environment.NewLine + Environment.NewLine);
            }

            if (bHtml)
            {
                file.WriteLine("</body>");
                file.WriteLine("</html>");
            }

            file.Close();
        }

        static void Main(string[] args)
        {
            if (!IntroAndSetup(args))
                return;

            LoadExcludeFile();
            GetAllFiles();

            ParseFiles(headerFiles);
            ParseFiles(sourceFiles);

            PrintResults();
        }
    }
}
