//
// viewcollision/viewcollision.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

/*
PURPOSE	This is a tool for programmers to debug collisions. Two bound files should be specified in command
		line arguments. With keyboard input, the objects can be moved in three dimensions. A collision
		test and response calculation are done on every frame. When a problem collision is found, it will
		then be repeated indefinitely for debugging.
NOTES
	Input control:
		arrow keys for horizontal motion
		'a' and 'z' for vertical motion
		shift with any motion key for a rotation
		ctrl for faster motion or rotation
		1,2,3 and 4 to switch the object under input control (current and previous position for object 1,
			and current and previous position for object 2)

	If command line arguments are missing, the bounds default to DEFAULT_BOUND1 and DEFAULT_BOUND2 (defined below).
	Command line argument examples:
		-bound1 "box" -bound2 "sphere"
		-bound1 "box_long"
		-bound1 "detroit" -bound2 "vp_z28_81"
		-bound1="plane_small_bvh" -testprobes=4
		-bound1="plane_small_bvh" -bound2="triangle" -testobject */

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "curve/curvemgr.h"
#include "devcam/polarcam.h"
#include "file/asset.h"
#include "gizmo/manager.h"
#include "gizmo/rotation.h"
#include "gizmo/translation.h"
#include "grcore/im.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/mouse.h"
#include "math/random.h"
#include "math/simplemath.h"
#include "phbound/boundcapsule.h"
#include "phbound/boundsphere.h"
#include "phcore/materialmgrimpl.h"
#include "phcore/phmath.h"
#include "phcore/pool.h"
#include "phcore/segment.h"
#include "physics/colliderdispatch.h"
#include "physics/intersection.h"
#include "physics/levelnew.h"
#include "physics/overlappingpairarray.h"
#include "physics/shapetest.h"
#include "physics/simulator.h"
#include "sample_grcore/sample_grcore.h"
#include "string/string.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/colors.h"
#include "vector/quaternion.h"
#include "vectormath/classes.h"


#define TRACE_SHAPE_TEST	0 && __XENON && !__FINAL

#if TRACE_SHAPE_TEST
#include "input/keyboard.h"
#include "input/keys.h"
#include "system/timemgr.h"
#include "system/xtl.h"
#include <tracerecording.h>
#pragma comment (lib, "tracerecording.lib")
#endif


namespace rage {

PFD_DECLARE_GROUP_ON(Polygons);
PFD_DECLARE_ITEM_ON(All,Color_white,Polygons);
PFD_DECLARE_ITEM(Thin,Color_red,Polygons);
PFD_DECLARE_ITEM(BadNormal,Color_red,Polygons);

EXT_PFD_DECLARE_GROUP(Physics);
EXT_PFD_DECLARE_GROUP(Bounds);
EXT_PFD_DECLARE_ITEM(DrawBoundMaterials);
EXT_PFD_DECLARE_ITEM(SupportPoints);
EXT_PFD_DECLARE_ITEM_SLIDER(BoundDrawDistance);
EXT_PFD_DECLARE_GROUP(Impetuses);

EXT_PFD_DECLARE_ITEM(Solid);
EXT_PFD_DECLARE_ITEM_SLIDER(NormalLength);
EXT_PFD_DECLARE_ITEM(Face);
EXT_PFD_DECLARE_ITEM(Edge);
EXT_PFD_DECLARE_ITEM_SLIDER_INT(HighlightFlags);

EXT_PFD_DECLARE_GROUP(ShapeTests);
EXT_PFD_DECLARE_ITEM(PhysicsLevelCullShape);

EXT_PFD_DECLARE_ITEM(ProbeSegments);
EXT_PFD_DECLARE_ITEM(ProbeIsects);
EXT_PFD_DECLARE_ITEM(ProbeNormals);
EXT_PFD_DECLARE_ITEM(EdgeSegments);
EXT_PFD_DECLARE_ITEM(EdgeIsects);
EXT_PFD_DECLARE_ITEM(EdgeNormals);
EXT_PFD_DECLARE_ITEM(SphereSegments);
EXT_PFD_DECLARE_ITEM(SphereIsects);
EXT_PFD_DECLARE_ITEM(SphereNormals);
EXT_PFD_DECLARE_ITEM(CapsuleSegments);
EXT_PFD_DECLARE_ITEM(CapsuleIsects);
EXT_PFD_DECLARE_ITEM(CapsuleNormals);
EXT_PFD_DECLARE_ITEM(SweptSphereSegments);
EXT_PFD_DECLARE_ITEM(SweptSphereIsects);
EXT_PFD_DECLARE_ITEM(SweptSphereNormals);
EXT_PFD_DECLARE_ITEM(TaperedSweptSphereSegments);
EXT_PFD_DECLARE_ITEM(TaperedSweptSphereIsects);
EXT_PFD_DECLARE_ITEM(TaperedSweptSphereNormals);
EXT_PFD_DECLARE_ITEM(ScalingSweptQuadSegments);
EXT_PFD_DECLARE_ITEM(ScalingSweptQuadIsects);
EXT_PFD_DECLARE_ITEM(ScalingSweptQuadNormals);
EXT_PFD_DECLARE_ITEM(BoxSegments);
EXT_PFD_DECLARE_ITEM(BoxIsects);
EXT_PFD_DECLARE_ITEM(BoxNormals);
EXT_PFD_DECLARE_ITEM(ObjectSegments);
EXT_PFD_DECLARE_ITEM(ObjectIsects);
EXT_PFD_DECLARE_ITEM(ObjectNormals);
EXT_PFD_DECLARE_ITEM(ObjectManifolds);

} // namespace rage 

using namespace rage;

namespace ragesamples {

#define SPEED_MIN	1.0f
#define SPEED_MAX	24.0f
#define	SPIN_MIN	0.1f
#define SPIN_MAX	4.0f
#define KEY_SPEED_SLOW	0.1f
#define KEY_SPEED_NORM	2.0f
#define KEY_SPEED_FAST	16.0f
#define INITIAL_SEP_MUL	1.1f	// initial separation multiplier of the radius sum
#define MAX_TIME		20.0f	// maximum allowed time for a single collision
#define	DEFAULT_BOUND1	"crate"
#define DEFAULT_BOUND2	"big_plane"
#define VIEWC_MAX_NUM_ISECTS	4
#define VIEWC_MAX_NUM_SHAPES	16

enum { AUTO_MANUAL, AUTO_TIME, AUTO_ACCURACY };
enum { SHAPE_TEST_NONE, SHAPE_TEST_POINT, SHAPE_TEST_PROBE, SHAPE_TEST_EDGE, SHAPE_TEST_SPHERE, SHAPE_TEST_CAPSULE, SHAPE_TEST_BOX,
		SHAPE_TEST_SWEPT_SPHERE, SHAPE_TEST_TAPERED_SWEPT_SPHERE, SHAPE_TEST_SCALING_SWEPT_QUAD, SHAPE_TEST_BATCH, SHAPE_TEST_OBJECT, NUM_SHAPE_TESTS };
enum { HELP_NONE, HELP_ARROWS, HELP_MOTION, HELP_INPUT, NUM_HELP_MODES };
enum { DRAW_NONE, DRAW_NORMALS, DRAW_PUSHES, DRAW_IMPULSES, DRAW_FORCES, DRAW_ALL, NUM_DRAW_MODES };

enum { INPUT_NO_ACTION, INPUT_RESET, INPUT_ADVANCE_FRAME };
enum { INPUT_MOVE_STEPS, INPUT_MOVE_SLIDE };


PARAM(bound1,"the name of bound 1");
PARAM(bound2,"the name of bound 2");
PARAM(bound3,"the name of bound 3");
PARAM(testpoint,"do a point test");
PARAM(testsphere,"do a sphere test");
PARAM(testprobe,"do a probe test");
PARAM(testedge,"do an edge test");
PARAM(testsweptsphere,"do a swept sphere test");
PARAM(testtaperedsweptsphere,"do a tapered swept sphere test");
PARAM(testscalingsweptquad,"do a scaling swept quad test");
PARAM(testcapsule,"do a capsule test");
PARAM(testobject,"do an object test");
PARAM(testbox,"do a box test");
PARAM(testpoints,"do point tests");
PARAM(testspheres,"do sphere tests");
PARAM(testprobes,"do probe tests");
PARAM(testedges,"do edge tests");
PARAM(testsweptspheres,"do swept sphere tests");
PARAM(testtaperedsweptspheres,"do tapered swept sphere tests");
PARAM(testscalingsweptquads,"do scaling swept quad tests");
PARAM(testcapsules,"do capsule tests");
PARAM(testboxes,"do box tests");
PARAM(testsphereandsweptsphere,"do a sphere and a swept sphere test");
PARAM(worldoffsetx,"x position offset for all objects");
PARAM(worldoffsety,"y position offset for all objects");
PARAM(worldoffsetz,"z position offset for all objects");
PARAM(pos1x,"x position of bound 1");
PARAM(pos1y,"y position of bound 1");
PARAM(pos1z,"z position of bound 1");
PARAM(pos2x,"x position of bound 2");
PARAM(pos2y,"y position of bound 2");
PARAM(pos2z,"z position of bound 2");
PARAM(pos3x,"x position of bound 3");
PARAM(pos3y,"y position of bound 3");
PARAM(pos3z,"z position of bound 3");
PARAM(random,"run random tests");


class viewCollisionManager : public grcSampleManager
{
public:
	viewCollisionManager()
		: m_ContinuousCollisionDetection(false)
		, m_Collider1(NULL)
		, m_Collider2(NULL)
		, m_Collider3(NULL)
		, m_Instance1(NULL)
		, m_Instance2(NULL)
		, m_Instance3(NULL)
		, m_CameraTarget(V_ZERO)
		, m_DrawMode(DRAW_NORMALS)
		, m_ShapeTest(SHAPE_TEST_NONE)
		, m_NumIsects(1)
		, m_NumHits(0)
		, m_RandomTests(false)
		, m_SaveCull(false)
		, m_ReuseCull(false)
		, m_EnablePrimitiveCullStorage(false)
		, m_RetestIntersections(false)
		, m_InitialSphereIntersection(false)
		, m_SpheresTestForBackFace(false)
	{
		m_CullResults.AllocateInstanceArray(MAX_SAVED_CULLED_INSTANCES);
		m_CullResults.AllocatePrimitiveIndexArray(MAX_SPU_NUM_CULLED_POLYS);
	}

	virtual void InitCamera()
	{
		Vec3V cameraPos = Add(Vec3V(3.0f,3.0f,3.0f),m_CameraTarget);
		grcSampleManager::InitSampleCamera(RCC_VECTOR3(cameraPos),RCC_VECTOR3(m_CameraTarget));
	}

	virtual const char* GetSampleName() const
	{
		return "Collision Viewer";
	}
	
	void ComputeRandomBox (const phInst* instance)
	{
		Vec3V centroid = instance->GetWorldCentroid();
		ScalarV radius = instance->GetArchetype()->GetBound()->GetRadiusAroundCentroidV();
		m_RandomBoxMin = Min(Subtract(centroid,Vec3V(radius)),m_RandomBoxMin);
		m_RandomBoxMax = Max(Add(centroid,Vec3V(radius)),m_RandomBoxMax);
	}

	virtual void InitClient()
	{
		ASSERT_ONLY(phConfig::sm_ValidatePhysics = true;)

		// Set the asset path.
		SetFullAssetPath(RAGE_ASSET_ROOT);

		// Set the current folder.
		ASSET.PushFolder("physics");

		// Make old octree and quadtree bounds load as bvh bounds.
		phBound::SetOctreeAsBVH(true);

		// Enable bound drawing.
		PFD_GROUP_ENABLE(Physics,true);
		PFD_GROUP_ENABLE(Bounds,true);
		PFD_GROUP_ENABLE(Impetuses,true);
		PFD_ITEM_ENABLE(SupportPoints,true);

		PFD_ITEM_ENABLE(ProbeSegments,true);
		PFD_ITEM_ENABLE(ProbeNormals,true);
		PFD_ITEM_ENABLE(ProbeIsects,true);
		PFD_ITEM_ENABLE(EdgeSegments,true);
		PFD_ITEM_ENABLE(EdgeNormals,true);
		PFD_ITEM_ENABLE(EdgeIsects,true);
		PFD_ITEM_ENABLE(SphereSegments,true);
		PFD_ITEM_ENABLE(SphereNormals,true);
		PFD_ITEM_ENABLE(SphereIsects,true);
		PFD_ITEM_ENABLE(CapsuleSegments,true);
		PFD_ITEM_ENABLE(CapsuleNormals,true);
		PFD_ITEM_ENABLE(CapsuleIsects,true);
		PFD_ITEM_ENABLE(SweptSphereSegments,true);
		PFD_ITEM_ENABLE(SweptSphereNormals,true);
		PFD_ITEM_ENABLE(SweptSphereIsects,true);
		PFD_ITEM_ENABLE(TaperedSweptSphereSegments,true);
		PFD_ITEM_ENABLE(TaperedSweptSphereNormals,true);
		PFD_ITEM_ENABLE(TaperedSweptSphereIsects,true);
		PFD_ITEM_ENABLE(ScalingSweptQuadSegments,true);
		PFD_ITEM_ENABLE(ScalingSweptQuadNormals,true);
		PFD_ITEM_ENABLE(ScalingSweptQuadIsects,true);
		PFD_ITEM_ENABLE(ObjectSegments,true);
		PFD_ITEM_ENABLE(ObjectNormals,true);
		PFD_ITEM_ENABLE(ObjectIsects,true);
		PFD_ITEM_ENABLE(BoxNormals,true);
		PFD_ITEM_ENABLE(BoxSegments,true);
		PFD_ITEM_ENABLE(BoxIsects,true);

#if ENABLE_UNUSED_CURVE_CODE
		// Create a curve manager, just in case it's used by one of the objects.
		cvCurveMgr::CreateInstance();
#endif // ENABLE_UNUSED_CURVE_CODE

		// Make the material manager with a large maximum number of materials to handle grid bounds.
		phMaterialMgrImpl<phMaterial>::Create();
		MATERIALMGR.Load();

		// Create, initialize and activate the physics level.
		phLevelNew* physLevel = rage_new phLevelNew;
		physLevel->SetNumOctreeNodes(100);
		const int maxNumObjects = 3;
		physLevel->SetMaxActive(maxNumObjects);
		physLevel->SetMaxObjects(maxNumObjects);
		Vec3V levelMin(-10000.0f, -10000.0f, -10000.0f);
		Vec3V levelMax(+10000.0f, +10000.0f, +10000.0f);
		physLevel->SetExtents(levelMin,levelMax);
		physLevel->Init();
		physLevel->SetActiveInstance();

		// Call the static initialization of the simulator.
		phSimulator::InitClass();

		// Create, initialize and activate the physics simulator.
		phSimulator* physSim = rage_new phSimulator;
		phSimulator::InitParams params;
		params.maxManagedColliders = maxNumObjects;
		physSim->Init(physLevel,params);
		physSim->SetActiveInstance();

		// Set the frame rate.
		const float frameRate = 30.0f;
		TIME.SetFixedFrameMode(frameRate);
		TIME.ShowFrameOutput(true);

		m_RandomTests = PARAM_random.Get();

		m_PointTest.SetCullResults(&m_CullResults);
		m_SphereTest.SetCullResults(&m_CullResults);
		m_ProbeTest.SetCullResults(&m_CullResults);
		m_EdgeTest.SetCullResults(&m_CullResults);
		m_ProbeTest.SetCullResults(&m_CullResults);
		m_CapsuleTest.SetCullResults(&m_CullResults);
		m_ObjectTest.SetCullResults(&m_CullResults);
		m_SweptSphereTest.SetCullResults(&m_CullResults);
		m_TaperedSweptSphereTest.SetCullResults(&m_CullResults);
		m_ScalingSweptQuadTest.SetCullResults(&m_CullResults);
		m_BatchTest.SetCullResults(&m_CullResults);

		// Get the shape test name.
		m_BatchNumPoints = 0;
		m_BatchNumSpheres = 0;
		m_BatchNumProbes = 0;
		m_BatchNumEdges = 0;
		m_BatchNumSweptSpheres = 0;
		m_BatchNumCapsules = 0;
		m_BatchNumBoxes = 0;
		m_BatchNumShapes = 0;
		m_ShapeTest = SHAPE_TEST_NONE;
		if (PARAM_testpoints.Get(m_BatchNumPoints) || PARAM_testpoint.Get())
		{
			m_ShapeTest = ((m_ShapeTest==SHAPE_TEST_BATCH || m_BatchNumPoints>0) ? SHAPE_TEST_BATCH : SHAPE_TEST_POINT);
		}

		if (PARAM_testspheres.Get(m_BatchNumSpheres) || PARAM_testsphere.Get())
		{
			m_ShapeTest = ((m_ShapeTest==SHAPE_TEST_BATCH || m_BatchNumSpheres>0) ? SHAPE_TEST_BATCH : SHAPE_TEST_SPHERE);
		}

		if (PARAM_testprobes.Get(m_BatchNumProbes) || PARAM_testprobe.Get())
		{
			m_ShapeTest = ((m_ShapeTest==SHAPE_TEST_BATCH || m_BatchNumProbes>0) ? SHAPE_TEST_BATCH : SHAPE_TEST_PROBE);
		}

		if (PARAM_testedge.Get(m_BatchNumEdges) || PARAM_testedge.Get())
		{
			m_ShapeTest = ((m_ShapeTest==SHAPE_TEST_BATCH || m_BatchNumEdges>0) ? SHAPE_TEST_BATCH : SHAPE_TEST_EDGE);
		}

		if (PARAM_testsweptspheres.Get(m_BatchNumSweptSpheres) || PARAM_testsweptsphere.Get())
		{
			m_ShapeTest = ((m_ShapeTest==SHAPE_TEST_BATCH || m_BatchNumSweptSpheres>0) ? SHAPE_TEST_BATCH : SHAPE_TEST_SWEPT_SPHERE);
		}

		if (PARAM_testtaperedsweptspheres.Get(m_BatchNumTaperedSweptSpheres) || PARAM_testtaperedsweptsphere.Get())
		{
			m_ShapeTest = ((m_ShapeTest==SHAPE_TEST_BATCH || m_BatchNumTaperedSweptSpheres>0) ? SHAPE_TEST_BATCH : SHAPE_TEST_TAPERED_SWEPT_SPHERE);
		}

		if (PARAM_testscalingsweptquads.Get(m_BatchNumScalingSweptQuads) || PARAM_testscalingsweptquads.Get())
		{
			m_ShapeTest = ((m_ShapeTest==SHAPE_TEST_BATCH || m_BatchNumScalingSweptQuads>0) ? SHAPE_TEST_BATCH : SHAPE_TEST_SCALING_SWEPT_QUAD);
		}

		if (PARAM_testcapsules.Get(m_BatchNumCapsules) || PARAM_testcapsule.Get())
		{
			m_ShapeTest = ((m_ShapeTest==SHAPE_TEST_BATCH || m_BatchNumCapsules>0) ? SHAPE_TEST_BATCH : SHAPE_TEST_CAPSULE);
		}

		if (PARAM_testobject.Get())
		{
			m_ShapeTest = SHAPE_TEST_OBJECT;
		}

		if (PARAM_testboxes.Get(m_BatchNumBoxes) || PARAM_testbox.Get())
		{
			m_ShapeTest = ((m_ShapeTest==SHAPE_TEST_BATCH || m_BatchNumBoxes>0) ? SHAPE_TEST_BATCH : SHAPE_TEST_BOX);
		}

		if (PARAM_testsphereandsweptsphere.Get())
		{
			m_ShapeTest = SHAPE_TEST_BATCH;
			m_BatchNumSpheres = 1;
			m_BatchNumSweptSpheres = 1;
		}

		if (m_ShapeTest==SHAPE_TEST_BATCH)
		{
			m_BatchNumShapes = m_BatchNumPoints+m_BatchNumSpheres+m_BatchNumProbes+m_BatchNumEdges+m_BatchNumSweptSpheres+m_BatchNumCapsules;
			int shapeIndex = 0;
			for (int pointIndex=0; pointIndex<m_BatchNumPoints; pointIndex++)
			{
				m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_POINT;
			}

			for (int sphereIndex=0; sphereIndex<m_BatchNumSpheres; sphereIndex++)
			{
				m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_SPHERE;
			}

			for (int probeIndex=0; probeIndex<m_BatchNumProbes; probeIndex++)
			{
				m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_PROBE;
			}

			for (int edgeIndex=0; edgeIndex<m_BatchNumEdges; edgeIndex++)
			{
				m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_EDGE;
			}

			for (int sweptSphereIndex=0; sweptSphereIndex<m_BatchNumSweptSpheres; sweptSphereIndex++)
			{
				m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_SWEPT_SPHERE;
			}

			for (int taperedSweptSphereIndex=0; taperedSweptSphereIndex<m_BatchNumTaperedSweptSpheres; taperedSweptSphereIndex++)
			{
				m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_TAPERED_SWEPT_SPHERE;
			}

			for (int scalingSweptQuadIndex=0; scalingSweptQuadIndex<m_BatchNumScalingSweptQuads; scalingSweptQuadIndex++)
			{
				m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_SCALING_SWEPT_QUAD;
			}

            for (int capsuleIndex=0; capsuleIndex<m_BatchNumCapsules; capsuleIndex++)
            {
                m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_CAPSULE;
            }

			for (int boxIndex=0; boxIndex<m_BatchNumBoxes; boxIndex++)
			{
				m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_BOX;
			}
		}

		if (m_ShapeTest==SHAPE_TEST_EDGE)
		{
			m_NumIsects = 2;
		}

		// Get the bound file name and load the first bound. Use the default bound if none was specified.
		const char* boundName1 = DEFAULT_BOUND1;
		PARAM_bound1.Get(boundName1);
		phBound* bound1 = m_OriginalBound1 = phBound::Load(boundName1);
		Assert(bound1);

		// Get the bound file name and load the second bound. Default to no second bound if a shape test was specified, otherwise use a default bound.
		const char* boundName2 = (m_ShapeTest==SHAPE_TEST_NONE ? DEFAULT_BOUND2 : NULL);
		PARAM_bound2.Get(boundName2);
		phBound* bound2 = m_OriginalBound2 = boundName2 ? phBound::Load(boundName2) : NULL;

		// Get the bound file name and load the third bound, if one was specified.
		const char* boundName3 = NULL;
		PARAM_bound3.Get(boundName3);
		phBound* bound3 = m_OriginalBound3 = boundName3 ? phBound::Load(boundName3) : NULL;

		m_Scale1 = m_Scale2 = m_Scale3 = 1.0f;

		// Try to get an optional position offset for all objects.
		Vec3V worldOffset;
		float position;
		worldOffset.SetXf(PARAM_worldoffsetx.Get(position) ? position : 0.0f);
		worldOffset.SetYf(PARAM_worldoffsety.Get(position) ? position : 0.0f);
		worldOffset.SetZf(PARAM_worldoffsetz.Get(position) ? position : 0.0f);
		m_CameraTarget = Add(m_CameraTarget,worldOffset);

		// Get an optional position for the first object.
		const Vec3V centroidOffset1 = bound1->GetCentroidOffset();
		m_ResetPosition1.SetXf(PARAM_pos1x.Get(position) ? position : -centroidOffset1.GetXf());
		m_ResetPosition1.SetYf(PARAM_pos1y.Get(position) ? position : -centroidOffset1.GetYf());
		m_ResetPosition1.SetZf(PARAM_pos1z.Get(position) ? position : -centroidOffset1.GetZf());
		m_ResetPosition1 = Add(m_ResetPosition1,worldOffset);
		m_Current1 = Mat34V(Mat33V(V_IDENTITY),m_ResetPosition1);
		m_Last1 = Mat34V(Mat33V(V_IDENTITY),m_ResetPosition1);

		// Create the first object and add it to the world.
		m_Fixed1 = (bound1->GetType()==phBound::BVH);
		CreateObject(bound1,m_ResetPosition1,m_Instance1,m_Collider1,m_Fixed1);

		// Create translation and rotation gizmos for the first object's matrix.
		m_CurrentTranslationGizmo1 = rage_new gzTranslation(RC_MATRIX34(m_Current1));
		m_CurrentTranslationGizmo1->SetSize(1.4f);
		m_CurrentRotationGizmo1 = rage_new gzRotation(RC_MATRIX34(m_Current1));
		m_CurrentRotationGizmo1->SetSize(1.2f);
		m_CurrentRotationGizmo1->Deactivate();
		m_LastTranslationGizmo1 = rage_new gzTranslation(RC_MATRIX34(m_Last1));
		m_LastTranslationGizmo1->SetSize(0.7f);
		m_LastTranslationGizmo1->Deactivate();
		m_LastRotationGizmo1 = rage_new gzRotation(RC_MATRIX34(m_Last1));
		m_LastRotationGizmo1->SetSize(0.6f);
		m_LastRotationGizmo1->Deactivate();

		// Set a current and last matrix for the first object.
		// This is useful for pasting known matrices from other applications.
	//	m_Current1.a.Set(1.0f,0.0f,-0.0f);
	//	m_Current1.b.Set(0.0f,0.99754536f,0.070023209f);
	//	m_Current1.c.Set(0.0f,-0.070023209f,0.99754536f);
	//	m_Current1.d.Set(0.0f,-1.6169742f,0.0f);
	//	m_Current1.d.Add(worldOffset);
	//	m_Last1.a.Set(1.0f,0.0f,-0.0f);
	//	m_Last1.b.Set(0.0f,0.99754536f,0.070023209f);
	//	m_Last1.c.Set(0.0f,-0.070023209f,0.99754536f);
	//	m_Last1.d.Set(0.0f,1.6408852f,0.0f);
	//	m_Last1.d.Add(worldOffset);

		if (bound2)
		{
			// Get an optional position for the second object.
			const Vec3V centroidOffset2 = bound2->GetCentroidOffset();
			m_ResetPosition2.SetXf(PARAM_pos2x.Get(position) ? position : -centroidOffset2.GetXf());
			m_ResetPosition2.SetYf(PARAM_pos2y.Get(position) ? position : -centroidOffset2.GetYf());
			m_ResetPosition2.SetZf(PARAM_pos2z.Get(position) ? position : -centroidOffset2.GetZf());
			m_ResetPosition2 = Add(m_ResetPosition2,worldOffset);
			m_Current2 = Mat34V(Mat33V(V_IDENTITY),m_ResetPosition2);
			m_Last2 = Mat34V(Mat33V(V_IDENTITY),m_ResetPosition2);

			// Create the second object and add it to the world.
			m_Fixed2 = (bound2->GetType()==phBound::BVH);
			const bool putInLevel = (m_ShapeTest!=SHAPE_TEST_OBJECT);
			CreateObject(bound2,m_ResetPosition2,m_Instance2,m_Collider2,m_Fixed2,putInLevel);

			// Create translation and rotation gizmos for the first object's matrix.
			m_CurrentTranslationGizmo2 = rage_new gzTranslation(RC_MATRIX34(m_Current2));
			m_CurrentTranslationGizmo2->SetSize(1.4f);
			m_CurrentRotationGizmo2 = rage_new gzRotation(RC_MATRIX34(m_Current2));
			m_CurrentRotationGizmo2->SetSize(1.2f);
			m_CurrentRotationGizmo2->Deactivate();
			m_LastTranslationGizmo2 = rage_new gzTranslation(RC_MATRIX34(m_Last2));
			m_LastTranslationGizmo2->SetSize(0.7f);
			m_LastTranslationGizmo2->Deactivate();
			m_LastRotationGizmo2 = rage_new gzRotation(RC_MATRIX34(m_Last2));
			m_LastRotationGizmo2->SetSize(0.6f);
			m_LastRotationGizmo2->Deactivate();

			// Set a current and last matrix for the second object.
			// This is useful for pasting known matrices from other applications.
		//	m_Current2.a.Set(0.064318508f,-0.91579241f,0.39647001f);
		//	m_Current2.b.Set(0.23507780f,0.40001756f,0.88584703f);
		//	m_Current2.c.Set(-0.96984631f,0.036224749f,0.24101104f);
		//	m_Current2.d.Set(-8.7518206f,6.8703914f,-13.940110f);
		//	m_Current2.d.Add(worldOffset);
		//	m_Last2.a.Set(0.15341081f,-0.91601652f,0.37064794f);
		//	m_Last2.b.Set(0.18844000f,0.39532506f,0.89900446f);
		//	m_Last2.c.Set(-0.97002870f,-0.068072222f,0.23326135f);
		//	m_Last2.d.Set(-8.8302889f,6.9231710f,-13.915118f);
		//	m_Last2.d.Add(worldOffset);
		}

		if (bound3)
		{
			// Get an optional position for the third object.
			const Vec3V centroidOffset3 = bound3->GetCentroidOffset();
			m_ResetPosition3.SetXf(PARAM_pos3x.Get(position) ? position : -centroidOffset3.GetXf());
			m_ResetPosition3.SetYf(PARAM_pos3y.Get(position) ? position : -centroidOffset3.GetYf());
			m_ResetPosition3.SetZf(PARAM_pos3z.Get(position) ? position : -centroidOffset3.GetZf());
			m_ResetPosition3 = Add(m_ResetPosition3,worldOffset);
			m_Current3 = Mat34V(Mat33V(V_IDENTITY),m_ResetPosition3);
			m_Last3 = Mat34V(Mat33V(V_IDENTITY),m_ResetPosition3);

			// Create the second object and add it to the world.
			m_Fixed3 = (bound3->GetType()==phBound::BVH);;
			CreateObject(bound3,m_ResetPosition3,m_Instance3,m_Collider3,m_Fixed3);

			// Create translation and rotation gizmos for the first object's matrix.
			m_CurrentTranslationGizmo3 = new gzTranslation(RC_MATRIX34(m_Current3));
			m_CurrentTranslationGizmo3->SetSize(1.4f);
			m_CurrentRotationGizmo3 = new gzRotation(RC_MATRIX34(m_Current3));
			m_CurrentRotationGizmo3->SetSize(1.2f);
			m_CurrentRotationGizmo3->Deactivate();
			m_LastTranslationGizmo3 = new gzTranslation(RC_MATRIX34(m_Last3));
			m_LastTranslationGizmo3->SetSize(0.7f);
			m_LastTranslationGizmo3->Deactivate();
			m_LastRotationGizmo3 = new gzRotation(RC_MATRIX34(m_Last3));
			m_LastRotationGizmo3->SetSize(0.6f);
			m_LastRotationGizmo3->Deactivate();
		}

		// Compute the minimum bounding box to surround the bounds, for random tests.
		m_RandomBoxMin = Vec3V(V_FLT_MAX);
		m_RandomBoxMax = Vec3V(V_NEG_FLT_MAX);
		ComputeRandomBox(m_Instance1);
		if (bound2)
		{
			ComputeRandomBox(m_Instance2);
			if (bound3)
			{
				ComputeRandomBox(m_Instance3);
			}
		}

		// Set the shape test matrices, if there are any.
		int shapeIndex;
		if (m_ShapeTest!=SHAPE_TEST_NONE)
		{
			int numShapes = (m_ShapeTest==SHAPE_TEST_BATCH ? m_BatchNumShapes : 1);
			Vec3V sweepStartOffset = Vec3V(1.0f,0.0f,0.0f);
			for (shapeIndex=0; shapeIndex<numShapes; shapeIndex++)
			{
				m_ShapeTestCurrent[shapeIndex] = Mat34V(Mat33V(V_IDENTITY),Add(Vec3V(1.0f,1.0f,1.0f),worldOffset));
				m_ShapeTestLast[shapeIndex].Set3x3(m_ShapeTestCurrent[shapeIndex].GetMat33ConstRef());
				m_ShapeTestLast[shapeIndex].SetCol3(Add(m_ShapeTestCurrent[shapeIndex].GetCol3(),sweepStartOffset));
				m_ShapeSize[shapeIndex] = Vec3V(V_ONE);
				m_ShapeSize2[shapeIndex] = Vec3V(V_ONE);
				m_ShapeNormal[shapeIndex] = Vec3V(V_X_AXIS_WZERO);
				m_SweepStartIntersectionFound[shapeIndex] = false;
			}

			m_ResetShapeTestCurrent = m_ShapeTestCurrent[0].GetCol3();
			m_ResetShapeTestLast = m_ShapeTestLast[0].GetCol3();
		}

		// Map the keyboard input.
		m_Mapper.Reset();
		m_Mapper.Map(IOMS_KEYBOARD,KEY_F2,m_DrawModeio);
		m_Mapper.Map(IOMS_KEYBOARD,KEY_F4,m_Resetio);
		m_Mapper.Map(IOMS_KEYBOARD,KEY_F10,m_AdvanceFrameio);
		m_Mapper.Map(IOMS_KEYBOARD,KEY_RBRACKET,m_NumIsectsUpio);
		m_Mapper.Map(IOMS_KEYBOARD,KEY_LBRACKET,m_NumIsectsDownio);

	#if __PFDRAW
		// Initialize profile drawing.
		GetRageProfileDraw().Init(200000);
		GetRageProfileDraw().SetEnabled(true);
	#endif

		m_BatchTest.GetShape().AllocateProbes(16);
		m_BatchTest.GetShape().AllocateSpheres(16);
		m_BatchTest.GetShape().AllocateCapsules(16);
		m_BatchTest.GetShape().AllocateSweptSpheres(16);

		ASSET.PopFolder();
	}

	virtual void ShutdownClient()
	{
		delete m_CurrentRotationGizmo1;
		delete m_LastRotationGizmo1;
		delete m_CurrentTranslationGizmo1;
		delete m_LastTranslationGizmo1;
		if (m_Instance2)
		{
			delete m_CurrentRotationGizmo2;
			delete m_LastRotationGizmo2;
			delete m_CurrentTranslationGizmo2;
			delete m_LastTranslationGizmo2;
		}

		if (m_Instance3)
		{
			delete m_CurrentRotationGizmo3;
			delete m_LastRotationGizmo3;
			delete m_CurrentTranslationGizmo3;
			delete m_LastTranslationGizmo3;
		}

	#if __PFDRAW
		GetRageProfileDraw().Shutdown();
	#endif

	}

	virtual void Update ()
	{
		// Update keyboard input.
		m_Mapper.Update();

		grcSampleManager::Update();

		// Turn on NaN errors. This can be useful for finding errors, but it frequently fails intentionally.
		//EnableNanSignal(true);

		// Get user input to determine whether to reset or advance the frame.
		// The default action is to not advance the frame, and to instead repeat the same collision or shape test every frame.
		switch (GetUserInput())
		{
			case INPUT_RESET:
			{
				ResetObjects();
				break;
			}
			case INPUT_ADVANCE_FRAME:
			{
				if (m_Instance1)
				{
					m_Current1 = m_Last1 = m_Instance1->GetMatrix();
					if (m_Collider1)
					{
						m_Last1 = m_Collider1->GetLastInstanceMatrix();
					}
				}
				if (m_Instance2)
				{
					m_Current2 = m_Last2 = m_Instance2->GetMatrix();
					if (m_Collider2)
					{
						m_Last2 = m_Collider2->GetLastInstanceMatrix();
					}
				}
				if (m_Instance3)
				{
					m_Current3 = m_Last3 = m_Instance3->GetMatrix();
					if (m_Collider3)
					{
						m_Last3 = m_Collider3->GetLastInstanceMatrix();
					}
				}
				break;
			}
		}

		// Set the object motions.
		RestoreObjects();

		if (m_ShapeTest==SHAPE_TEST_NONE)
		{
			float timeStep = TIME.GetSeconds();
			ScalarV invTimeStepV = Invert(ScalarVFromF32(timeStep));

#define USE_VELOCITY 0

			if (m_Instance1)
			{
				if (!m_LastTranslationGizmo1->IsActive())
				{
					m_Last1.SetCol3(m_Current1.GetCol3());
				}
				if (!m_LastRotationGizmo1->IsActive())
				{
					m_Last1.Set3x3(m_Current1);
				}
#if USE_VELOCITY
				PHSIM->TeleportObject(*m_Instance1, m_Last1, &m_Last1);
				if (phCollider* collider = PHSIM->GetCollider(m_Instance1))
				{
					collider->SetVelocity(Scale(Subtract(m_Current1.GetCol3(), m_Last1.GetCol3()),invTimeStepV));
				}
#else
				PHSIM->TeleportObject(*m_Instance1, RCC_MATRIX34(m_Current1), &RCC_MATRIX34(m_Last1));
#endif
			}

			if (m_Instance2)
			{
				if (!m_LastTranslationGizmo2->IsActive())
				{
					m_Last2.SetCol3(m_Current2.GetCol3());
				}
				if (!m_LastRotationGizmo2->IsActive())
				{
					m_Last2.Set3x3(m_Current2);
				}

#if USE_VELOCITY
				PHSIM->TeleportObject(*m_Instance2, m_Last2, &m_Last2);
				if (phCollider* collider = PHSIM->GetCollider(m_Instance2))
				{
					collider->SetVelocity(Scale(Subtract(m_Current2.GetCol3(), m_Last2.GetCol3()),invTimeStepV));
				}
#else
				PHSIM->TeleportObject(*m_Instance2, RCC_MATRIX34(m_Current2), &RCC_MATRIX34(m_Last2));
#endif
			}

			if (m_Instance3)
			{
				if (!m_LastTranslationGizmo3->IsActive())
				{
					m_Last3.SetCol3(m_Current3.GetCol3());
				}
				if (!m_LastRotationGizmo3->IsActive())
				{
					m_Last3.Set3x3(m_Current3);
				}

#if USE_VELOCITY
				PHSIM->TeleportObject(*m_Instance3, m_Last3, &m_Last3);
				if (phCollider* collider = PHSIM->GetCollider(m_Instance3))
				{
					collider->SetVelocity(Scale(Subtract(m_Current3.GetCol3(), m_Last3.GetCol3()),invTimeStepV));
				}
#else
				PHSIM->TeleportObject(*m_Instance3, RCC_MATRIX34(m_Current3), &RCC_MATRIX34(m_Last3));
#endif
			}

			// Clear all the manifolds so we get the same results every time
			//hs PHSIM->GetManifoldMgr()->ResetManifolds();
			//PHMANIFOLD->Reset();

			// Update the simulator.
			PHSIM->Update(timeStep);
		}
		else
		{
			// Move the objects.
			if (m_Instance1)
			{
				m_Instance1->SetMatrix(m_Current1);
				Assert(m_Instance1->IsInLevel());
				PHLEVEL->UpdateObjectLocation(m_Instance1->GetLevelIndex(),&RCC_MATRIX34(m_Last1));
			}
			if (m_Instance2)
			{
				m_Instance2->SetMatrix(m_Current2);
				if (m_Instance2->IsInLevel())
				{
					PHLEVEL->UpdateObjectLocation(m_Instance2->GetLevelIndex(),&RCC_MATRIX34(m_Last2));
				}
			}
			if (m_Instance3)
			{
				m_Instance3->SetMatrix(m_Current3);
				if (m_Instance3->IsInLevel())
				{
					PHLEVEL->UpdateObjectLocation(m_Instance3->GetLevelIndex(),&RCC_MATRIX34(m_Last3));
				}
			}
			PHLEVEL->CommitDeferredOctreeUpdates();

			// Test the shape tests in the level.
			m_NumHits = 0;
			for (int index=0; index<m_NumIsects; index++)
			{
				m_Isect[0][index].Reset();
				m_OldIsect[0][index].Reset();
			}

			if (m_RandomTests)
			{
				m_ShapeSize[0] = m_Random.GetRangedV(Vec3V(ScalarVFromF32(0.001f)),Vec3V(ScalarVFromF32(1.0f)));
				m_ShapeTestCurrent[0].SetCol3(m_Random.GetRangedV(m_RandomBoxMin,m_RandomBoxMax));
				m_ShapeTestLast[0].SetCol3(m_Random.GetRangedV(m_RandomBoxMin,m_RandomBoxMax));
			}

		#if TRACE_SHAPE_TEST
			bool recordTrace = false;
			const char* rttiName = GetRttiName(&m_ShapeType);
			if (!strcmp(rttiName,"class rage::phShapeObject") && ioKeyboard::KeyPressed(KEY_F12))
			{
				recordTrace = true;
				XTraceStartRecording("devkit:\\TestInLevel.pix2");
			}
		#endif

			m_CullResults.SetResultLevel(m_EnablePrimitiveCullStorage ? phShapeTestCullResults::RESULTLEVEL_PRIMITIVE : phShapeTestCullResults::RESULTLEVEL_INSTANCE);

			phShapeTestCull::State cullState = phShapeTestCull::DEFAULT;
			if(m_ReuseCull)
			{
				cullState = phShapeTestCull::READ;
			}
			else if(m_SaveCull)
			{
				cullState = phShapeTestCull::WRITE;
			}

			phIntersection* isectPtr = (m_NumIsects>0 ? &m_Isect[0][0] : NULL);
			switch (m_ShapeTest)
			{
				case SHAPE_TEST_POINT:
				{
					m_PointTest.SetCullState(cullState);
					m_PointTest.InitPoint(m_ShapeTestCurrent[0].GetCol3(),isectPtr,m_NumIsects);
					m_NumHits = m_PointTest.TestInLevel();
					break;
				}

				case SHAPE_TEST_SPHERE:
				{
					m_SphereTest.SetCullState(cullState);
					m_SphereTest.InitSphere(m_ShapeTestCurrent[0].GetCol3(),m_ShapeSize[0].GetX(),isectPtr,m_NumIsects);
					m_SphereTest.GetShape().SetTestBackFacingPolygons(m_SpheresTestForBackFace);
					m_NumHits = m_SphereTest.TestInLevel();
					break;
				}

				case SHAPE_TEST_PROBE:
				{
					m_ProbeTest.SetCullState(cullState);
					phSegmentV segment(m_ShapeTestLast[0].GetCol3(),m_ShapeTestCurrent[0].GetCol3());
					m_ProbeTest.InitProbe(segment,isectPtr,m_NumIsects);
					m_NumHits = m_ProbeTest.TestInLevel();
					break;
				}

				case SHAPE_TEST_EDGE:
				{
					m_EdgeTest.SetCullState(cullState);
					phSegmentV segment(m_ShapeTestLast[0].GetCol3(),m_ShapeTestCurrent[0].GetCol3());
					m_EdgeTest.InitEdge(segment,isectPtr,m_NumIsects);
					m_NumHits = m_EdgeTest.TestInLevel();
					break;
				} 

				case SHAPE_TEST_CAPSULE:
				{
					m_CapsuleTest.SetCullState(cullState);
					phSegmentV segment(m_ShapeTestLast[0].GetCol3(),m_ShapeTestCurrent[0].GetCol3());
					m_CapsuleTest.InitCapsule(segment,m_ShapeSize[0].GetX(),isectPtr,m_NumIsects);
					m_NumHits = m_CapsuleTest.TestInLevel();
					break;
				}

				case SHAPE_TEST_OBJECT:
				{
					m_ObjectTest.SetCullState(cullState);
					Assert(m_Instance2 && m_Instance2->GetArchetype() && m_Instance2->GetArchetype()->GetBound());
 					m_ObjectTest.InitObject(*m_Instance2->GetArchetype()->GetBound(),m_Current2,m_Last2,isectPtr,m_NumIsects);
					m_ObjectTest.GetShape().SetCullType(phCullShape::PHCULLTYPE_CAPSULE);
					m_NumHits = m_ObjectTest.TestInLevel();
					break;
                }

				case SHAPE_TEST_SWEPT_SPHERE:
				{
					m_SweptSphereTest.SetCullState(cullState);
					phSegmentV segment(m_ShapeTestLast[0].GetCol3(),m_ShapeTestCurrent[0].GetCol3());
					m_SweptSphereTest.InitSweptSphere(segment,m_ShapeSize[0].GetX(),isectPtr,m_NumIsects);
					m_SweptSphereTest.GetShape().SetTestInitialSphere(m_InitialSphereIntersection);
					m_NumHits = m_SweptSphereTest.TestInLevel();
					break;
				}

				case SHAPE_TEST_TAPERED_SWEPT_SPHERE:
				{
					m_TaperedSweptSphereTest.SetCullState(cullState);
					phSegmentV segment(m_ShapeTestLast[0].GetCol3(),m_ShapeTestCurrent[0].GetCol3());
					m_TaperedSweptSphereTest.InitTaperedSweptSphere(segment,m_ShapeSize[0].GetX(),m_ShapeSize[0].GetY(),isectPtr,m_NumIsects);
					m_NumHits = m_TaperedSweptSphereTest.TestInLevel();
					break;
				}

				case SHAPE_TEST_SCALING_SWEPT_QUAD:
				{
					m_ScalingSweptQuadTest.SetCullState(cullState);
					phSegmentV segment(m_ShapeTestLast[0].GetCol3(),m_ShapeTestCurrent[0].GetCol3());

					Mat33V rotation;
					rotation.SetCol0(NormalizeSafe(m_ShapeNormal[0], Vec3V(V_X_AXIS_WZERO)));
					rotation.SetCol2(NormalizeSafe(Cross(rotation.GetCol0(),Vec3V(V_Y_AXIS_WZERO)),Vec3V(V_Z_AXIS_WZERO)));
					rotation.SetCol1(Cross(rotation.GetCol2(),rotation.GetCol0()));

					Vec2V initialHalfExtents = m_ShapeSize[0].GetXY();
					Vec2V finalHalfExtents = m_ShapeSize2[0].GetXY();

					m_ScalingSweptQuadTest.InitScalingSweptQuad(rotation,segment,initialHalfExtents,finalHalfExtents,isectPtr,m_NumIsects);
					m_NumHits = m_ScalingSweptQuadTest.TestInLevel();

					m_SweepStartIntersectionFound[0] = m_ScalingSweptQuadTest.GetShape().SweepStartIntersectionFound();
					break;
				}

				case SHAPE_TEST_BOX:
				{
	//				m_BoxTest.KeepExistingCullData(m_UseOldCullData);
	//				Matrix34 boxAxes(m_ShapeTestCurrent[0]);
	//				m_BoxTest.InitBox(boxAxes,m_ShapeSize[0],isectPtr,m_NumIsects);
	//				m_NumHits = m_BoxTest.TestInLevel();
					break;
				}

				case SHAPE_TEST_BATCH:
				{
					m_BatchTest.SetCullState(cullState);
					int shapeIndex;
					for (shapeIndex=0; shapeIndex<m_BatchNumShapes; shapeIndex++)
					{
						m_Isect[shapeIndex][0].Reset();
						for (int index=1; index<m_NumIsects; index++)
						{
							m_Isect[shapeIndex][index].Reset();
						}
					}

					// Create a shape tester with batched tests. The spaces inside the phShapeTest declaration are necessary for PS3 builds (maybe it's confused by >>).
					m_BatchTest.GetShape().GetProbeGroup().ResetNumShapes();
					m_BatchTest.GetShape().GetSphereGroup().ResetNumShapes();
					m_BatchTest.GetShape().GetCapsuleGroup().ResetNumShapes();
					shapeIndex = 0;
					bool sphereAndSweptSphere = (m_BatchNumSpheres==1 && m_BatchNumSweptSpheres==1 && m_BatchNumPoints==0 && m_BatchNumProbes==0 && m_BatchNumEdges==0 && m_BatchNumCapsules==0 && m_BatchNumBoxes==0);
					if (sphereAndSweptSphere)
					{
						phSegmentV segment(m_ShapeTestLast[1].GetCol3(),m_ShapeTestCurrent[1].GetCol3());
						phIntersection* secondIsectPtr = (m_NumIsects>0 ? &m_Isect[1][0] : NULL);
						m_BatchTest.InitSphereAndSweptSphere(segment,m_ShapeSize[1].GetX(),isectPtr,m_NumIsects,secondIsectPtr,m_NumIsects);
						m_BatchShapeTypes[0] = SHAPE_TEST_SPHERE;
						m_BatchShapeTypes[1] = SHAPE_TEST_SWEPT_SPHERE;

						// Force the sphere to be at the swept sphere's starting position.
						m_ShapeTestCurrent[0] = m_ShapeTestLast[1];
					}
					else
					{
						for (int pointIndex=0; pointIndex<m_BatchNumPoints; pointIndex++)
						{
							isectPtr = (m_NumIsects>0 ? &m_Isect[shapeIndex][0] : NULL);
							m_BatchTest.InitPoint(m_ShapeTestCurrent[shapeIndex].GetCol3(),isectPtr,m_NumIsects);
							m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_POINT;
						}

						for (int sphereIndex=0; sphereIndex<m_BatchNumSpheres; sphereIndex++)
						{
							isectPtr = (m_NumIsects>0 ? &m_Isect[shapeIndex][0] : NULL);
							m_BatchTest.InitSphere(m_ShapeTestCurrent[shapeIndex].GetCol3(),m_ShapeSize[shapeIndex].GetX(),isectPtr,m_NumIsects);
							m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_SPHERE;
						}

						for (int probeIndex=0; probeIndex<m_BatchNumProbes; probeIndex++)
						{
							isectPtr = (m_NumIsects>0 ? &m_Isect[shapeIndex][0] : NULL);
							phSegmentV segment(m_ShapeTestLast[shapeIndex].GetCol3(),m_ShapeTestCurrent[shapeIndex].GetCol3());
							m_BatchTest.InitProbe(segment,isectPtr,m_NumIsects);
							m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_PROBE;
						}

						for (int edgeIndex=0; edgeIndex<m_BatchNumEdges; edgeIndex++)
						{
							isectPtr = (m_NumIsects>0 ? &m_Isect[shapeIndex][0] : NULL);
							phSegmentV segment(m_ShapeTestLast[shapeIndex].GetCol3(),m_ShapeTestCurrent[shapeIndex].GetCol3());
							m_BatchTest.InitEdge(segment,isectPtr,m_NumIsects);
							m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_EDGE;
						}

						for (int taperedSweptSphereIndex=0; taperedSweptSphereIndex<m_BatchNumTaperedSweptSpheres; taperedSweptSphereIndex++)
						{
							isectPtr = (m_NumIsects>0 ? &m_Isect[shapeIndex][0] : NULL);
							phSegmentV segment(m_ShapeTestLast[shapeIndex].GetCol3(),m_ShapeTestCurrent[shapeIndex].GetCol3());
							m_BatchTest.InitTaperedSweptSphere(segment,m_ShapeSize[shapeIndex].GetX(),m_ShapeSize[shapeIndex].GetY(),isectPtr,m_NumIsects);
							m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_TAPERED_SWEPT_SPHERE;
						}

						for (int sweptSphereIndex=0; sweptSphereIndex<m_BatchNumSweptSpheres; sweptSphereIndex++)
						{
							isectPtr = (m_NumIsects>0 ? &m_Isect[shapeIndex][0] : NULL);
							phSegmentV segment(m_ShapeTestLast[shapeIndex].GetCol3(),m_ShapeTestCurrent[shapeIndex].GetCol3());
							m_BatchTest.InitSweptSphere(segment,m_ShapeSize[shapeIndex].GetX(),isectPtr,m_NumIsects);
							m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_SWEPT_SPHERE;
						}

	                    for (int capsuleIndex=0; capsuleIndex<m_BatchNumCapsules; capsuleIndex++)
	                    {
							isectPtr = (m_NumIsects>0 ? &m_Isect[shapeIndex][0] : NULL);
	                        phSegmentV segment(m_ShapeTestLast[shapeIndex].GetCol3(),m_ShapeTestCurrent[shapeIndex].GetCol3());
	                        m_BatchTest.InitCapsule(segment,m_ShapeSize[shapeIndex].GetX(),isectPtr,m_NumIsects);
	                        m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_CAPSULE;
	                    }

						for (int boxIndex=0; boxIndex<m_BatchNumBoxes; boxIndex++)
						{
		//					isectPtr = (m_NumIsects>0 ? &m_Isect[shapeIndex][0] : NULL);
		//					Matrix34 boxAxes(m_ShapeTestCurrent[shapeIndex]);
		//					m_BatchTest.InitBox(boxAxes,m_ShapeSize[shapeIndex],&m_Isect[shapeIndex][0],m_NumIsects);
		//					m_BatchShapeTypes[shapeIndex++] = SHAPE_TEST_BOX;
						}
					}

					if (!sphereAndSweptSphere)
					{
					//	m_BatchTest.GetShape().ComputeCullSphere();
						m_BatchTest.GetShape().ComputeAxisAlignedCullBox();
					}

					m_NumHits = m_BatchTest.TestInLevel();
					break;
				}

			}

		#if TRACE_SHAPE_TEST
			if (recordTrace)
			{
				XTraceStopRecording();
			}
		#endif

		}

		//EnableNanSignal(false);
	}


#if __PFDRAW
	void DrawAxesAndNormals (const phBound& bound, const Mat34V& current, const Mat34V& last)
	{
		// Draw the bound's centroid axes at the current position.
		bound.DrawCentroid(current);

		// Draw the bound's centroid axes at the previous position.
		bound.DrawCentroid(last);

		// See if face normal drawing is turned on in widgets.
		if (PFD_Face.Begin())
		{
			// Draw the bound's face normals.
			bound.DrawNormals(current,phBound::FACE_NORMALS,phBound::ALL_POLYS,PFD_NormalLength.GetValue());
			PFD_Face.End();
		}

		// See if edge normal drawing is turned on in widgets.
		if (PFD_Edge.Begin())
		{
			// Draw the bound's edge normals.
			bound.DrawNormals(current,phBound::EDGE_NORMALS,phBound::ALL_POLYS,PFD_NormalLength.GetValue());
			PFD_Edge.End();
		}
	}
#endif

	virtual void DrawClient()
	{

#if __PFDRAW

		grcColor(Color_white);
		grcBindTexture(NULL);
		grcLightState::SetEnabled(false);
		grcViewport::SetCurrentWorldIdentity();
		if (m_Instance1)
		{
			grcColor(Color_white);
			const phBound& bound1 = *m_Instance1->GetArchetype()->GetBound();
			DrawAxesAndNormals(bound1,m_Current1,m_Last1);

			if (PFD_SupportPoints.WillDraw() && bound1.IsConvex() && !phBound::IsTypeComposite(bound1.GetType()))
			{
				bound1.DrawSupport(m_Current1);
			}
			else
			{
				bound1.Draw(m_Current1);
			}
		}

		if (m_Instance2)
		{
			grcColor(Color_white);
			const phBound& bound2 = *m_Instance2->GetArchetype()->GetBound();
			DrawAxesAndNormals(bound2,m_Current2,m_Last2);

			if (PFD_SupportPoints.WillDraw() && bound2.IsConvex() && !phBound::IsTypeComposite(bound2.GetType()))
			{
				bound2.DrawSupport(m_Current2);
			}
			else
			{
				bound2.Draw(m_Current2);
			}
		}

		if (m_Instance3)
		{
			grcColor(Color_white);
			const phBound& bound3 = *m_Instance3->GetArchetype()->GetBound();
			DrawAxesAndNormals(bound3,m_Current3,m_Last3);

			if (PFD_SupportPoints.WillDraw() && bound3.IsConvex() &&  !phBound::IsTypeComposite(bound3.GetType()))
			{
				bound3.DrawSupport(m_Current3);
			}
			else
			{
				bound3.Draw(m_Current3);
			}
		}

		if (m_ShapeTest==SHAPE_TEST_BATCH)
		{
			switch (m_BatchTest.GetShape().GetCullType())
			{
				case phCullShape::PHCULLTYPE_SPHERE:
				{
					Vector3 worldCenter = m_BatchTest.GetShape().GetCullSphereWorldCenter();
					float radius = m_BatchTest.GetShape().GetCullSphereRadius();
					grcDrawSphere(radius,worldCenter,8,true);
					break;
				}

				case phCullShape::PHCULLTYPE_CAPSULE:
				{
					float length = m_BatchTest.GetShape().GetCullCapsuleLength();
					float radius = m_BatchTest.GetShape().GetCullSphereRadius();
					Matrix34 capsulePose;
					capsulePose.b.Normalize(m_BatchTest.GetShape().GetCullCapsuleAxis());
					capsulePose.b.MakeOrthonormals(capsulePose.c,capsulePose.a);
					capsulePose.d.Set(m_BatchTest.GetShape().GetCullSphereWorldCenter());
					grcDrawCapsule(length,radius,capsulePose,8,true);
					break;
				}

				case phCullShape::PHCULLTYPE_BOX:
				{
					Matrix34 boxMatrix = m_BatchTest.GetShape().GetCullBoxAxes();
					Vector3 boxSize = m_BatchTest.GetShape().GetCullBoxHalfSize();
					boxSize.Scale(2.0f);
					grcDrawBox(boxSize,boxMatrix,Color_white);
					break;
				}

				default:
				{
					break;
				}
			}
		}

		GetRageProfileDraw().Render();
		PHSIM->ProfileDraw();
	#endif

	}

private:
	void CreateObject (phBound* bound, Vec3V_In resetPosition, phInst*& instance, phCollider*& collider, bool fixed=false, bool putInLevel=true)
	{
		instance = rage_new phInst;
		Assert(bound);
		phArchetype* archetype = rage_new phArchetypeDamp;
		instance->SetArchetype(archetype);
		archetype->SetBound(bound);
		Vec3V rotation(V_ZERO);
		instance->SetMatrix(MATRIX34_TO_MAT34V(CreateRotatedMatrix(RCC_VECTOR3(resetPosition),RCC_VECTOR3(rotation))));
		collider = NULL;
		if (putInLevel)
		{
			if (!fixed)
			{
				const bool alwaysActive = true;
				PHSIM->AddActiveObject(instance,alwaysActive);
				collider = PHSIM->GetCollider(instance);
			}
			else
			{
				PHSIM->AddFixedObject(instance);
				collider = NULL;
			}
		}
	}

	int GetUserInput (void)
	{
		if (m_DrawModeio.IsPressed())
		{
			// Increment the draw mode.
			m_DrawMode = (m_DrawMode+1)%NUM_DRAW_MODES;
		}
		else if (m_Resetio.IsPressed())
		{
			// Reset the objects, not the camera.
			return INPUT_RESET;
		}
		else if (m_AdvanceFrameio.IsPressed())
		{
			// Advance the collision to the next frame.
			return INPUT_ADVANCE_FRAME;
		}
		else if (m_NumIsectsUpio.IsPressed() && m_NumIsects<VIEWC_MAX_NUM_ISECTS)
		{
			m_NumIsects++;
		}
		else if (m_NumIsectsDownio.IsPressed() && m_NumIsects>1)
		{
			m_NumIsects--;
		}

		return INPUT_NO_ACTION;
	}

	void RestoreObjects ()
	{
		if (m_Collider1)
		{
			m_Collider1->Reset();
		}

		if (m_Collider2)
		{
			m_Collider2->Reset();
		}

		if (m_Collider3)
		{
			m_Collider3->Reset();
		}
	}

	void ResetObjects ()
	{
		m_Last1 = m_Current1 = Mat34V(Mat33V(V_IDENTITY),m_ResetPosition1);
		m_Last2 = m_Current2 = Mat34V(Mat33V(V_IDENTITY),m_ResetPosition2);
		m_Last3 = m_Current3 = Mat34V(Mat33V(V_IDENTITY),m_ResetPosition3);

		if (m_ShapeTest!=SHAPE_TEST_NONE)
		{
			if (m_ShapeTest==SHAPE_TEST_BATCH)
			{
				for (int shapeIndex=0; shapeIndex<m_BatchNumShapes; shapeIndex++)
				{
					m_ShapeTestCurrent[shapeIndex].SetCol3(m_ResetShapeTestCurrent);
					m_ShapeTestLast[shapeIndex].SetCol3(m_ResetShapeTestLast);
				}
			}
			else
			{
				m_ShapeTestCurrent[0].SetCol3(m_ResetShapeTestCurrent);
				m_ShapeTestLast[0].SetCol3(m_ResetShapeTestLast);
			}
		}

		RestoreObjects();
	}

	void SetMotion (phCollider* collider, float inverseTime)
	{
		// Set the collider's matrix from the instance.
		collider->SetColliderMatrixFromInstance();

		ScalarV inverseTimeV = ScalarVFromF32(inverseTime);

		// Compute and set the collider's velocity.
		collider->SetVelocity(Scale(Subtract(collider->GetInstance()->GetPosition(),collider->GetLastInstanceMatrix().GetCol3()),inverseTimeV).GetIntrin128());

		// Compute and set the collider's angular velocity.
		Quaternion currentQ,lastQ;
		currentQ.FromMatrix34(RCC_MATRIX34(collider->GetInstance()->GetMatrix()));
		lastQ.FromMatrix34(RCC_MATRIX34(collider->GetLastInstanceMatrix()));
		currentQ.MultiplyInverse(lastQ);
		Vec3V angVelocity = ScalarVFromF32(currentQ.GetAngle())*inverseTimeV*NormalizeSafe(Vec3V(currentQ.x,currentQ.y,currentQ.z),Vec3V(V_ZERO));
		collider->SetAngVelocity(angVelocity.GetIntrin128());
	}

#if __BANK
	void AddShapeWidgets (bkBank& bank, int shapeType, int shapeIndex)
	{
		const bool bankOpen = false;
		bank.AddSlider("Max Num Hits",&m_NumIsects,0,MAX_SHAPE_TEST_ISECTS,1);
		bank.AddText("Num Hits",&m_NumHits);
		float minCurr = MinElement(m_ShapeTestCurrent[shapeIndex].GetCol3()).Getf() - 100.0f;
		float maxCurr = MaxElement(m_ShapeTestCurrent[shapeIndex].GetCol3()).Getf() + 100.0f;
		float minLast = MinElement(m_ShapeTestLast[shapeIndex].GetCol3()).Getf() - 100.0f;
		float maxLast = MaxElement(m_ShapeTestLast[shapeIndex].GetCol3()).Getf() + 100.0f;
		switch (shapeType)
		{
			case SHAPE_TEST_POINT:
			{
				bank.PushGroup("Test Point",bankOpen);
				bank.AddVector("Position",&m_ShapeTestCurrent[shapeIndex].GetCol3Ref(),minCurr,maxCurr,0.1f);
				bank.PopGroup();
				break;
			}
			case SHAPE_TEST_SPHERE:
			{
				bank.PushGroup("Test Sphere",bankOpen);
				bank.AddSlider("Radius",&m_ShapeSize[shapeIndex][0],0.0f,100.0f,0.1f);
				bank.AddVector("Position",&m_ShapeTestCurrent[shapeIndex].GetCol3Ref(),minCurr,maxCurr,0.1f);
				bank.AddToggle("Test Polygon Back Faces",&m_SpheresTestForBackFace);
				bank.PopGroup();
				break;
			}
			case SHAPE_TEST_PROBE:
			{
				bank.PushGroup("Test Probe",bankOpen);
				bank.AddVector("Start Position",&m_ShapeTestLast[shapeIndex].GetCol3Ref(),minLast,maxLast,0.1f);
				bank.AddVector("End Position",&m_ShapeTestCurrent[shapeIndex].GetCol3Ref(),minCurr,maxCurr,0.1f);
				bank.PopGroup();
				break;
			}
			case SHAPE_TEST_EDGE:
			{
				bank.PushGroup("Test Edge",bankOpen);
				bank.AddVector("Start Position",&m_ShapeTestLast[shapeIndex].GetCol3Ref(),minLast,maxLast,0.1f);
				bank.AddVector("End Position",&m_ShapeTestCurrent[shapeIndex].GetCol3Ref(),minCurr,maxCurr,0.1f);
				bank.PopGroup();
				break;
			}
			case SHAPE_TEST_SWEPT_SPHERE:
			{
				bank.PushGroup("Test Swept Sphere",bankOpen);
				bank.AddToggle("Initial Sphere Test",&m_InitialSphereIntersection);
				bank.AddSlider("Radius",&m_ShapeSize[shapeIndex][0],0.0f,100.0f,0.1f);
				bank.AddVector("Start Position",&m_ShapeTestLast[shapeIndex].GetCol3Ref(),minLast,maxLast,0.1f);
				bank.AddVector("End Position",&m_ShapeTestCurrent[shapeIndex].GetCol3Ref(),minCurr,maxCurr,0.1f);
				bank.PopGroup();
				break;
			}
			case SHAPE_TEST_TAPERED_SWEPT_SPHERE:
			{
				bank.PushGroup("Test Tapered Swept Sphere",bankOpen);
				bank.AddSlider("Radius 1",&m_ShapeSize[shapeIndex][0],0.0f,100.0f,0.1f);
				bank.AddSlider("Radius 2",&m_ShapeSize[shapeIndex][1],0.0f,100.0f,0.1f);
				bank.AddVector("Start Position",&m_ShapeTestLast[shapeIndex].GetCol3Ref(),minLast,maxLast,0.1f);
				bank.AddVector("End Position",&m_ShapeTestCurrent[shapeIndex].GetCol3Ref(),minCurr,maxCurr,0.1f);
				bank.PopGroup();
				break;
			}
			case SHAPE_TEST_SCALING_SWEPT_QUAD:
			{
				bank.PushGroup("Test Scaling Swept Quad",bankOpen);
				bank.AddVector("Half Extents Initial",&m_ShapeSize[shapeIndex],0.0f,100.0f,0.1f);
				bank.AddVector("Half Extents Final",&m_ShapeSize2[shapeIndex],0.0f,100.0f,0.1f);
				bank.AddVector("Start Position",&m_ShapeTestLast[shapeIndex].GetCol3Ref(),minLast,maxLast,0.1f);
				bank.AddVector("End Position",&m_ShapeTestCurrent[shapeIndex].GetCol3Ref(),minCurr,maxCurr,0.1f);
				bank.AddVector("Normal",&m_ShapeNormal[shapeIndex],-10.0f,10.0f,0.1f);
				bank.AddText("Sweep Start Intersection", &m_SweepStartIntersectionFound[shapeIndex]);
				bank.PopGroup();
				break;
			}
			case SHAPE_TEST_CAPSULE:
			{
				bank.PushGroup("Test Capsule",bankOpen);
				bank.AddSlider("Radius",&m_ShapeSize[shapeIndex][0],0.0f,100.0f,0.1f);
				bank.AddVector("Start Position",&m_ShapeTestLast[shapeIndex].GetCol3Ref(),minLast,maxLast,0.1f);
				bank.AddVector("End Position",&m_ShapeTestCurrent[shapeIndex].GetCol3Ref(),minCurr,maxCurr,0.1f);
				bank.PopGroup();
				break;
			}
			case SHAPE_TEST_BOX:
			{
				bank.PushGroup("Test Box",bankOpen);
				bank.AddVector("Position",&m_ShapeTestCurrent[shapeIndex].GetCol3Ref(),minCurr,maxCurr,0.1f);
				bank.AddVector("Half Size",&m_ShapeSize[shapeIndex],0.0f,100.0f,0.1f);
				bank.PopGroup();
				break;
			}
		}
	}

	virtual void AddWidgetsClient ()
	{
		bkBank& bank = BANKMGR.CreateBank("viewcollision");
		if (m_ShapeTest==SHAPE_TEST_NONE)
		{
			bank.AddToggle("Continuous Collisions", &m_ContinuousCollisionDetection);
		}
		else
		{
			bank.AddToggle("Reuse Cull",&m_ReuseCull);
			bank.AddToggle("Save Cull",&m_SaveCull);
			bank.AddToggle("Enable Primitive Cull Storage",&m_EnablePrimitiveCullStorage);
			bank.AddToggle("Retest Intersections",&m_RetestIntersections);
			if (m_ShapeTest!=SHAPE_TEST_BATCH)
			{
				// Add widgets for the single shape test.
				const int shapeIndex = 0;
				AddShapeWidgets(bank,m_ShapeTest,shapeIndex);
			}
			else
			{
				for (int shapeIndex=0; shapeIndex<m_BatchNumShapes; shapeIndex++)
				{
					AddShapeWidgets(bank,m_BatchShapeTypes[shapeIndex],shapeIndex);
				}
			}
		}

		if (m_Instance1)
		{
			bank.PushGroup("Instance 1", true);
			bank.AddToggle("Current Pos 1", &m_CurrentTranslationGizmo1->GetActiveFlag());
			bank.AddToggle("Current Rot 1", &m_CurrentRotationGizmo1->GetActiveFlag());
			bank.AddToggle("Last Pos 1", &m_LastTranslationGizmo1->GetActiveFlag());
			bank.AddToggle("Last Rot 1", &m_LastRotationGizmo1->GetActiveFlag());
			bank.AddSlider("Scale 1", &m_Scale1, -1.0f, 1000.0f, 1.0f, datCallback(MFA(viewCollisionManager::UpdateScale), this));

			bank.AddToggle("Fixed 1", &m_Fixed1, datCallback(MFA(viewCollisionManager::UpdateFixedStatus), this));
			bank.PopGroup();
		}

		if (m_Instance2)
		{
			bank.PushGroup("Instance 2", true);
			bank.AddToggle("Current Pos 2", &m_CurrentTranslationGizmo2->GetActiveFlag());
			bank.AddToggle("Current Rot 2", &m_CurrentRotationGizmo2->GetActiveFlag());
			bank.AddToggle("Last Pos 2", &m_LastTranslationGizmo2->GetActiveFlag());
			bank.AddToggle("Last Rot 2", &m_LastRotationGizmo2->GetActiveFlag());
			bank.AddSlider("Scale 1", &m_Scale1, -1.0f, 1000.0f, 1.0f);
			bank.AddToggle("Fixed 2", &m_Fixed2, datCallback(MFA(viewCollisionManager::UpdateFixedStatus), this));
			bank.PopGroup();
		}

		if (m_Instance3)
		{
			bank.PushGroup("Instance 3", true);
			bank.AddToggle("Current Pos 3", &m_CurrentTranslationGizmo3->GetActiveFlag());
			bank.AddToggle("Current Rot 3", &m_CurrentRotationGizmo3->GetActiveFlag());
			bank.AddToggle("Last Pos 3", &m_LastTranslationGizmo3->GetActiveFlag());
			bank.AddToggle("Last Rot 3", &m_LastRotationGizmo3->GetActiveFlag());
			bank.AddToggle("Fixed 3", &m_Fixed3, datCallback(MFA(viewCollisionManager::UpdateFixedStatus), this));
			bank.PopGroup();
		}

		phSimulator::AddWidgets(BANKMGR.CreateBank("rage - Physics"));
	}

	void UpdateScale()
	{
		if(m_Instance1 && m_Instance1->GetArchetype() && m_Instance1->GetArchetype()->GetBound())
		{
			m_Instance1->GetArchetype()->SetBound(m_OriginalBound1->CloneAndScale(ScalarVFromF32(m_Scale1)));
		}

		UpdateFixedStatus();
	}

	void UpdateFixedStatus()
	{
		if (m_Instance1)
		{
			bool presentlyFixed1 = PHLEVEL->IsFixed(m_Instance1->GetLevelIndex());
			if (m_Fixed1 && !presentlyFixed1)
			{
				PHSIM->DeleteObject(m_Instance1->GetLevelIndex());
				PHSIM->AddFixedObject(m_Instance1);
				m_Collider1 = NULL;
			}
			if (!m_Fixed1 && presentlyFixed1)
			{
				PHSIM->DeleteObject(m_Instance1->GetLevelIndex());
				const bool alwaysActive = true;
				PHSIM->AddActiveObject(m_Instance1,alwaysActive);
				m_Collider1 = PHSIM->GetCollider(m_Instance1);
			}
		}

		if (m_Instance2)
		{
			bool presentlyFixed2 = PHLEVEL->IsFixed(m_Instance2->GetLevelIndex());
			if (m_Fixed2 && !presentlyFixed2)
			{
				PHSIM->DeleteObject(m_Instance2->GetLevelIndex());
				PHSIM->AddFixedObject(m_Instance2);
				m_Collider2 = NULL;
			}
			if (!m_Fixed2 && presentlyFixed2)
			{
				PHSIM->DeleteObject(m_Instance2->GetLevelIndex());
				const bool alwaysActive = true;
				PHSIM->AddActiveObject(m_Instance2,alwaysActive);
				m_Collider2 = PHSIM->GetCollider(m_Instance2);
			}
		}

		if (m_Instance3)
		{
			bool presentlyFixed3 = PHLEVEL->IsFixed(m_Instance3->GetLevelIndex());
			if (m_Fixed3 && !presentlyFixed3)
			{
				PHSIM->DeleteObject(m_Instance3->GetLevelIndex());
				PHSIM->AddFixedObject(m_Instance3);
				m_Collider3 = NULL;
			}
			if (!m_Fixed3 && presentlyFixed3)
			{
				PHSIM->DeleteObject(m_Instance3->GetLevelIndex());
				const bool alwaysActive = true;
				PHSIM->AddActiveObject(m_Instance3,alwaysActive);
				m_Collider3 = PHSIM->GetCollider(m_Instance3);
			}
		}
	}
#endif

	ioMapper m_Mapper;
	ioValue m_DrawModeio,m_Resetio,m_AdvanceFrameio,m_NumIsectsUpio,m_NumIsectsDownio;
	bool m_ContinuousCollisionDetection;
	
	phShapeTest<phShapeSphere> m_PointTest;
	phShapeTest<phShapeSphere> m_SphereTest;
	phShapeTest<phShapeProbe> m_ProbeTest;
	phShapeTest<phShapeEdge> m_EdgeTest;
	phShapeTest<phShapeCapsule> m_CapsuleTest;
	phShapeTest<phShapeObject> m_ObjectTest;
	phShapeTest<phShapeSweptSphere> m_SweptSphereTest;
	phShapeTest<phShapeTaperedSweptSphere> m_TaperedSweptSphereTest;
	phShapeTest<phShapeScalingSweptQuad> m_ScalingSweptQuadTest;
	//phShapeTest<phShapeBox> m_BoxTest;
	phShapeTest<phShapeBatch> m_BatchTest;

	gzRotation* m_CurrentRotationGizmo1;
	gzRotation* m_LastRotationGizmo1;
	gzRotation* m_CurrentRotationGizmo2;
	gzRotation* m_LastRotationGizmo2;
	gzRotation* m_CurrentRotationGizmo3;
	gzRotation* m_LastRotationGizmo3;
	gzTranslation* m_CurrentTranslationGizmo1;
	gzTranslation* m_LastTranslationGizmo1;
	gzTranslation* m_CurrentTranslationGizmo2;
	gzTranslation* m_LastTranslationGizmo2;
	gzTranslation* m_CurrentTranslationGizmo3;
	gzTranslation* m_LastTranslationGizmo3;

	float m_Scale1;
	float m_Scale2;
	float m_Scale3;

	phBound* m_OriginalBound1;
	phBound* m_OriginalBound2;
	phBound* m_OriginalBound3;

	bool m_Fixed1;
	bool m_Fixed2;
	bool m_Fixed3;

	mthRandom m_Random;
	Vec3V m_RandomBoxMin,m_RandomBoxMax;
	bool m_RandomTests;

	bool m_SaveCull;
	bool m_ReuseCull;
	bool m_EnablePrimitiveCullStorage;
	phShapeTestCullResults m_CullResults;

	bool m_RetestIntersections;
	bool m_InitialSphereIntersection;
	bool m_SpheresTestForBackFace;

	phCollider* m_Collider1;
	phCollider* m_Collider2;
	phCollider* m_Collider3;
	phInst* m_Instance1;
	phInst* m_Instance2;
	phInst* m_Instance3;
	phSegmentV m_Segment;
	phIntersection m_Isect[VIEWC_MAX_NUM_SHAPES][VIEWC_MAX_NUM_ISECTS];
	phIntersection m_RetestIsect[VIEWC_MAX_NUM_SHAPES][VIEWC_MAX_NUM_ISECTS];
	phIntersection m_OldIsect[VIEWC_MAX_NUM_SHAPES][VIEWC_MAX_NUM_ISECTS];
	Mat34V m_Current1;
	Mat34V m_Current2;
	Mat34V m_Current3;
	Mat34V m_Last1,m_Last2,m_Last3;
	Mat34V m_ShapeTestCurrent[VIEWC_MAX_NUM_SHAPES],m_ShapeTestLast[VIEWC_MAX_NUM_SHAPES];
	Vec3V m_ResetPosition1,m_ResetPosition2,m_ResetPosition3,m_ResetShapeTestCurrent,m_ResetShapeTestLast,m_CameraTarget;
	Vec3V m_ShapeSize[VIEWC_MAX_NUM_SHAPES];
	Vec3V m_ShapeSize2[VIEWC_MAX_NUM_SHAPES];
	Vec3V m_ShapeNormal[VIEWC_MAX_NUM_SHAPES];
	int m_BatchShapeTypes[VIEWC_MAX_NUM_SHAPES];
	bool m_SweepStartIntersectionFound[VIEWC_MAX_NUM_SHAPES];
	int m_DrawMode,m_NumIsects,m_NumHits;
	int m_ShapeTest,m_BatchNumPoints,m_BatchNumSpheres,m_BatchNumProbes,m_BatchNumEdges,m_BatchNumSweptSpheres,m_BatchNumTaperedSweptSpheres,m_BatchNumScalingSweptQuads,m_BatchNumCapsules,m_BatchNumBoxes,m_BatchNumShapes;
};


} // namespace ragesamples

using namespace ragesamples;

int Main()
{
	ragesamples::viewCollisionManager viewCollision;
	viewCollision.Init();

	viewCollision.UpdateLoop();

	viewCollision.Shutdown();

	return 0;
}

