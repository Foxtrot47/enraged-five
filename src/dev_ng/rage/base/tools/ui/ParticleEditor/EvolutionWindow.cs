using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
    public partial class EvolutionWindow : ParticleEditor.ptxForm
    {
        String m_CurEffectRule = "";
        public EvolutionWindow()
        {
            m_Name = "Evolution Preview";
            m_WindowID = "EVOLUTIONWINDOW";

            InitializeComponent();
        }

        public void DisplayEvolution(DataMod.dtaEffectRuleStd doe)
        {
            ClearInterface();
            m_CurEffectRule = doe.m_Name;
            if (doe.m_EvolutionList.m_Evolutions.Count == 0)
            {
                m_LabelEffectName.Text = "Effect (" + doe.m_Name + ")";
                Label labelNoEvo = new Label();
                labelNoEvo.Text = "No Evolution Data";
                m_PanelSliders.Controls.Add(labelNoEvo);
                labelNoEvo.Left = (m_PanelSliders.Width / 2) - (labelNoEvo.Width / 2);
                labelNoEvo.Top  = (m_PanelSliders.Height / 2) - (labelNoEvo.Height / 2);
                return;
            }

            int top = 3;
            int left = 0;
            int cntID = 0;

            m_PanelSliders.SuspendLayout();
            m_LabelEffectName.Text = "Effect (" + doe.m_Name+")";

            //Add The evolution Sliders
            foreach(DataMod.dtaEvolution evo in doe.m_EvolutionList.m_Evolutions)
            {
                
                EvolutionSlider slider = new EvolutionSlider();
                slider.Index = cntID+1;
                slider.SliderName = evo.m_Name;
                switch(cntID)
                {
                    case 0: slider.Color = Color.Red; break;
                    case 1: slider.Color = Color.Blue; break;
                    case 2: slider.Color = Color.Green; break;
                    case 3: slider.Color = Color.Purple; break;
                    case 4: slider.Color = Color.SaddleBrown; break;
                    default: slider.Color = Color.Black; break;
                }

                m_PanelSliders.Controls.Add(slider);
                slider.Left = left;
                slider.Top = top;
                slider.m_DataObject = evo;
                slider.Value = evo.m_TestEvolution;
                slider.Checked = evo.m_ProcOverride;
                slider.OnValueChanged += new EvolutionSlider.evoSliderEventDelegate(slider_OnValueChanged);
                cntID++;
                top += slider.Height;
            }
            m_PanelSliders.ResumeLayout();
        }

        public void ClearInterface()
        {
            m_CurEffectRule = "";
            m_LabelEffectName.Text = "Select Effect";
            m_PanelSliders.SuspendLayout();
            m_PanelSliders.Controls.Clear();
            m_PanelSliders.ResumeLayout();
        }
        private void slider_OnValueChanged(EvolutionSlider slider)
        {
            DataMod.dtaEvolution evo = slider.m_DataObject as DataMod.dtaEvolution;
            evo.m_ProcOverride = slider.Checked;
            evo.m_TestEvolution = slider.Value;
            DataCon.m_SDataCon.SendEffectRule(m_CurEffectRule);
        }
    }
}

