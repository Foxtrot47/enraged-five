﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
    public partial class TrackViewerFormBase : ptxForm
    {
        public TrackViewerFormBase()
        {

            InitializeComponent();
		}

		public string GetKeyframeId()
		{
			//return m_RuleName + "|" + m_RuleType.ToString() + "|" + m_PropertyId.ToString() + "|" + m_EventIdx.ToString() + "|" + m_EvoIdx.ToString();
			return GetKeyframeId(m_RuleName, m_RuleType, m_PropertyId, m_EventIdx, m_EvoIdx);
		}


		public static string GetKeyframeId(string ruleName, int ruleType, uint propertyId, int eventIdx, int evoIdx)
		{
			return ruleName + "|" + ruleType.ToString() + "|" + propertyId.ToString() + "|" + eventIdx.ToString() + "|" + evoIdx.ToString();
		}

        public static void ShowForm(string description, string defnName, string ruleName, int ruleType, uint propertyId, int eventIdx, int evoIdx)
        {
            // first check to see if there's already a form by this name open.
            TrackViewerFormBase f = null;
			if (sm_OpenKeyframeMap.TryGetValue(GetKeyframeId(ruleName, ruleType, propertyId, eventIdx, evoIdx), out f))
            {
                f.Show();
                f.Activate();
                return;
            }

            FormCreator c = new FormCreator();
            c.Description = description;
			c.DefnName = defnName;
			c.RuleName = ruleName;
			c.RuleType = ruleType;
			c.PropertyId = propertyId;
			c.EventIdx = eventIdx;
			c.EvoIdx = evoIdx;

            // send a request for the keyframe data
            // (don't actually show the window until the request is fulfilled)

			DataCon.m_SDataCon.RequestKeyframeData(ruleName, ruleType, propertyId, eventIdx, evoIdx, c.FinishShowingForm);
        }

        public static void UpdateKeyframeUI(tcpByteBuffer msg)
        {
            msg.SetReadPos(8);

			string ruleName = msg.Read_const_char();
			int ruleType = msg.Read_s32();
			uint propertyId = msg.Read_u32();
			int eventIdx = msg.Read_s32();
			int evoIdx = msg.Read_s32();

            TrackViewerFormBase f = null;
            //If the form is open then update the ui
			if (!sm_OpenKeyframeMap.TryGetValue(GetKeyframeId(ruleName, ruleType, propertyId, eventIdx, evoIdx), out f))
                return;

            //Else we need to update the form
            DataMod.KeyframeUiData uidata = new DataMod.KeyframeUiData();
            DataMod.LoadXmlStruct(msg, uidata);
            DataMod.KeyframeData data = new DataMod.KeyframeData();
            DataMod.LoadXmlStruct(msg, data);

            f.ConfigureEditor(uidata);
        }

        public static void CloseForm()
        {
        }

        public static void FindAndCloseUnusedForms()
        {
            // suspend all the forms while doing the query
			//foreach (TrackViewerFormBase form in sm_OpenEditorMap.Values)
			foreach (TrackViewerFormBase form in sm_OpenKeyframeMap.Values)
            {
                form.Enabled = false;
				DataCon.m_SDataCon.RequestKeyframeExists(form.m_RuleName, form.m_RuleType, form.m_PropertyId, form.m_EventIdx, form.m_EvoIdx, form.CloseIfUnused);
            }
        }

        public static void DisableAllForms()
        {
            //foreach (TrackViewerFormBase form in sm_OpenEditorMap.Values)
			foreach (TrackViewerFormBase form in sm_OpenKeyframeMap.Values)
            {
                form.Enabled = false;
            }
        }

		public string m_RuleName = "invalid";
		public int m_RuleType = -1;
		public uint m_PropertyId = 0;
		public int m_EventIdx = -1;
		public int m_EvoIdx = -1;

        public virtual void ConfigureEditor(DataMod.KeyframeUiData uidata) { }

        public virtual void SetInitialData(DataMod.KeyframeData data) { }

        class FormCreator
        {
			public string DefnName;
			public string RuleName = "invalid";
			public int RuleType = -1;
			public uint PropertyId = 0;
			public int EventIdx = -1;
			public int EvoIdx = -1;
            public string Description;

            public void FinishShowingForm(tcpByteBuffer response)
            {
                // Response packet should be the keyframe UI data followed by the initial
                // keyframe data
				if (sm_OpenKeyframeMap.ContainsKey(TrackViewerFormBase.GetKeyframeId(RuleName, RuleType, PropertyId, EventIdx, EvoIdx)))
                {
                    // this happens if 2 open requests are sent out before either of them 
                    // is fulfilled. The first response creates an editor window already,
                    // so don't open a 2nd one
                    return;
                }

                DataMod.KeyframeUiData uidata = new DataMod.KeyframeUiData();

                DataMod.LoadXmlStruct(response, uidata);

                DataMod.KeyframeData data = new DataMod.KeyframeData();
                DataMod.LoadXmlStruct(response, data);

				/*
				TrackViewerFormBase form = null;

                
				if (uidata.ShowAsColorGradient && ((Control.ModifierKeys & Keys.Alt) == 0))
				{
					form = new ColorKeyframeEditorForm();
				}
				else
				{
					form = new KeyframeEditorForm();
				}

				form.Text = Description + " " + DefnName;
				form.ConfigureEditor(uidata);
				form.SetInitialData(data);

				form.m_Name = "TV: Keyframe " + DefnName;
				form.m_WindowID = "TV: " + form.GetType().ToString() + " " + DefnName;
				form.Persistant = false;

				main.m_MainForm.AddOwnedForm(form);

				form.Show();
				*/

				TrackViewerForm tvh = new TrackViewerForm();
				tvh.Text = Description + " " + DefnName;
                tvh.ConfigureEditor(uidata);
                tvh.SetInitialData(data);

				tvh.m_Name = "Keyframe " + DefnName;
				tvh.m_WindowID = tvh.GetType().ToString() + " " + DefnName;
                tvh.Persistant = false;

				tvh.m_RuleName = RuleName;
				tvh.m_RuleType = RuleType;
				tvh.m_PropertyId = PropertyId;
				tvh.m_EventIdx = EventIdx;
				tvh.m_EvoIdx = EvoIdx;

                main.m_MainForm.AddOwnedForm(tvh);

				sm_OpenKeyframeMap.Add(TrackViewerFormBase.GetKeyframeId(RuleName, RuleType, PropertyId, EventIdx, EvoIdx), tvh);

                tvh.Show();
                
            }

        };

        public void CloseIfUnused(tcpByteBuffer response)
        {
            bool isUsed = response.Read_bool();
            if (isUsed)
            {
                Enabled = true;
            }
            else
            {
                Close();
            }
        }

        public static void ShowKeyframe(DataMod.KeyframeSpec key, String description)
        {
            if (key == null)
                return;
			TrackViewerFormBase.ShowForm(description, key.DefnName, key.RuleName, key.RuleType, key.PropertyId, key.EventIdx, key.EvoIdx);
        }


        public static void ShowKeyframeFromListBox(ListBox box)
        {
            if (box == null)
            {
                return;
            }

            DataMod.KeyframeSpec key = box.SelectedItem as DataMod.KeyframeSpec;
            if (key == null)
            {
                return;
            }

			TrackViewerFormBase.ShowForm("", key.DefnName, key.RuleName, key.RuleType, key.PropertyId, key.EventIdx, key.EvoIdx);
        }

		//static Dictionary<uint, TrackViewerFormBase> sm_OpenEditorMap = new Dictionary<uint, TrackViewerFormBase>();
		static Dictionary<string, TrackViewerFormBase> sm_OpenKeyframeMap = new Dictionary<string, TrackViewerFormBase>();

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

			sm_OpenKeyframeMap.Remove(GetKeyframeId());
        }

    }
}