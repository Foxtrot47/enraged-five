using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
    public partial class CustomBehaviorWindow : ParticleEditor.ptxForm
    {
        public DataMod.dtaPtxBehavior m_Behavior = null;
        public event BehaviorWindowBase.BehaviorWindowEvent OnBehaviorChanged = null;
        public event BehaviorWindowBase.BehaviorWindowEvent OnClosedWindow = null;

        public CustomBehaviorWindow()
        {
            InitializeComponent();
        }

        public virtual void PrepWindow(DataMod.dtaPtxBehavior prop)
        {
            m_Behavior = prop;
            Text = "Behavior: " + "[" + prop.m_PtxRule.m_Name + "] " + m_Behavior.m_BehaviorName;
            m_Name = Text;
            m_WindowID = GetType().ToString() + " " + m_Behavior.m_BehaviorName;
            Persistant = false;
        }

        private void CustomBehaviorWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (OnClosedWindow != null)
                OnClosedWindow(sender, new BehaviorWindowBase.BehaviorChangedEventArgs(m_Behavior));
        }
        public void BehaviorChanged(object sender)
        {
            if (OnBehaviorChanged != null)
                OnBehaviorChanged(sender, new BehaviorWindowBase.BehaviorChangedEventArgs(m_Behavior));
		}
    }
}

