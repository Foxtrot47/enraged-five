namespace ParticleEditor
{
    partial class BehaviorWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_DataGridPtxtKeyframeList = new System.Windows.Forms.DataGridView();
            this.m_ContextMenuKeyProp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.m_PanelOptions = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.m_ColumnTextRuleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvolutionLOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvoBlend = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Invis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridPtxtKeyframeList)).BeginInit();
            this.SuspendLayout();
            // 
            // m_DataGridPtxtKeyframeList
            // 
            this.m_DataGridPtxtKeyframeList.AllowUserToAddRows = false;
            this.m_DataGridPtxtKeyframeList.AllowUserToDeleteRows = false;
            this.m_DataGridPtxtKeyframeList.AllowUserToResizeColumns = false;
            this.m_DataGridPtxtKeyframeList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AliceBlue;
            this.m_DataGridPtxtKeyframeList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.m_DataGridPtxtKeyframeList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_DataGridPtxtKeyframeList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.m_DataGridPtxtKeyframeList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.m_DataGridPtxtKeyframeList.BackgroundColor = System.Drawing.SystemColors.Window;
            this.m_DataGridPtxtKeyframeList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.m_DataGridPtxtKeyframeList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.m_DataGridPtxtKeyframeList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridPtxtKeyframeList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.m_DataGridPtxtKeyframeList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.m_ColumnTextRuleName,
            this.Evolution1,
            this.Evolution2,
            this.Evolution3,
            this.Evolution4,
            this.Evolution5,
            this.EvolutionLOD,
            this.EvoBlend,
            this.Invis});
            this.m_DataGridPtxtKeyframeList.ContextMenuStrip = this.m_ContextMenuKeyProp;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridPtxtKeyframeList.DefaultCellStyle = dataGridViewCellStyle7;
            this.m_DataGridPtxtKeyframeList.GridColor = System.Drawing.SystemColors.Window;
            this.m_DataGridPtxtKeyframeList.Location = new System.Drawing.Point(2, 29);
            this.m_DataGridPtxtKeyframeList.MultiSelect = false;
            this.m_DataGridPtxtKeyframeList.Name = "m_DataGridPtxtKeyframeList";
            this.m_DataGridPtxtKeyframeList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridPtxtKeyframeList.RowHeadersVisible = false;
            this.m_DataGridPtxtKeyframeList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.m_DataGridPtxtKeyframeList.RowTemplate.Height = 15;
            this.m_DataGridPtxtKeyframeList.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridPtxtKeyframeList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_DataGridPtxtKeyframeList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.m_DataGridPtxtKeyframeList.ShowCellToolTips = false;
            this.m_DataGridPtxtKeyframeList.Size = new System.Drawing.Size(362, 194);
            this.m_DataGridPtxtKeyframeList.TabIndex = 24;
            this.m_DataGridPtxtKeyframeList.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_DataGridPtxtKeyframeList_CellValueChanged);
            this.m_DataGridPtxtKeyframeList.DoubleClick += new System.EventHandler(this.ShowKeyframe);
            this.m_DataGridPtxtKeyframeList.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SelectOnRightMouseButton_CellMouseDown);
            this.m_DataGridPtxtKeyframeList.CurrentCellDirtyStateChanged += new System.EventHandler(this.m_DataGridPtxtKeyframeList_CurrentCellDirtyStateChanged);
            this.m_DataGridPtxtKeyframeList.SelectionChanged += new System.EventHandler(this.DataGridAutoSelectValidCell);
            // 
            // m_ContextMenuKeyProp
            // 
            this.m_ContextMenuKeyProp.Name = "m_ContextMenuKeyProp";
            this.m_ContextMenuKeyProp.Size = new System.Drawing.Size(61, 4);
            this.m_ContextMenuKeyProp.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContextMenuKeyProps_Opening);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-1, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Keyframes:";
            // 
            // m_PanelOptions
            // 
            this.m_PanelOptions.AutoScroll = true;
            this.m_PanelOptions.AutoSize = true;
            this.m_PanelOptions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_PanelOptions.Location = new System.Drawing.Point(2, 250);
            this.m_PanelOptions.Name = "m_PanelOptions";
            this.m_PanelOptions.Size = new System.Drawing.Size(362, 108);
            this.m_PanelOptions.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-1, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Options:";
            // 
            // m_ColumnTextRuleName
            // 
            this.m_ColumnTextRuleName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.m_ColumnTextRuleName.HeaderText = "Property";
            this.m_ColumnTextRuleName.Name = "m_ColumnTextRuleName";
            this.m_ColumnTextRuleName.ReadOnly = true;
            this.m_ColumnTextRuleName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_ColumnTextRuleName.Width = 69;
            // 
            // Evolution1
            // 
            this.Evolution1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Red;
            this.Evolution1.DefaultCellStyle = dataGridViewCellStyle2;
            this.Evolution1.HeaderText = "Evolution1";
            this.Evolution1.Name = "Evolution1";
            this.Evolution1.ReadOnly = true;
            this.Evolution1.Width = 5;
            // 
            // Evolution2
            // 
            this.Evolution2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Blue;
            this.Evolution2.DefaultCellStyle = dataGridViewCellStyle3;
            this.Evolution2.HeaderText = "Evolution2";
            this.Evolution2.Name = "Evolution2";
            this.Evolution2.ReadOnly = true;
            this.Evolution2.Width = 5;
            // 
            // Evolution3
            // 
            this.Evolution3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Green;
            this.Evolution3.DefaultCellStyle = dataGridViewCellStyle4;
            this.Evolution3.HeaderText = "Evolution3";
            this.Evolution3.Name = "Evolution3";
            this.Evolution3.ReadOnly = true;
            this.Evolution3.Width = 5;
            // 
            // Evolution4
            // 
            this.Evolution4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Purple;
            this.Evolution4.DefaultCellStyle = dataGridViewCellStyle5;
            this.Evolution4.HeaderText = "Evolution4";
            this.Evolution4.Name = "Evolution4";
            this.Evolution4.ReadOnly = true;
            this.Evolution4.Width = 5;
            // 
            // Evolution5
            // 
            this.Evolution5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.SaddleBrown;
            this.Evolution5.DefaultCellStyle = dataGridViewCellStyle6;
            this.Evolution5.HeaderText = "Evolution5";
            this.Evolution5.MinimumWidth = 2;
            this.Evolution5.Name = "Evolution5";
            this.Evolution5.ReadOnly = true;
            this.Evolution5.Width = 2;
            // 
            // EvolutionLOD
            // 
            this.EvolutionLOD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.EvolutionLOD.HeaderText = "EvolutionLOD";
            this.EvolutionLOD.Name = "EvolutionLOD";
            this.EvolutionLOD.ReadOnly = true;
            this.EvolutionLOD.Width = 5;
            // 
            // EvoBlend
            // 
            this.EvoBlend.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EvoBlend.HeaderText = "EvoBlend";
            this.EvoBlend.Items.AddRange(new object[] {
            "Active Avg",
            "Add",
            "Max",
            "Full Avg"});
            this.EvoBlend.Name = "EvoBlend";
            this.EvoBlend.Width = 80;
            // 
            // Invis
            // 
            this.Invis.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Invis.HeaderText = "Invis";
            this.Invis.Name = "Invis";
            this.Invis.ReadOnly = true;
            // 
            // BehaviorWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(367, 454);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_PanelOptions);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_DataGridPtxtKeyframeList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "BehaviorWindow";
            this.Text = "BehaviorWindow";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BehaviorWindow_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridPtxtKeyframeList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView m_DataGridPtxtKeyframeList;
        private System.Windows.Forms.ContextMenuStrip m_ContextMenuKeyProp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel m_PanelOptions;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_ColumnTextRuleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution5;
        private System.Windows.Forms.DataGridViewTextBoxColumn EvolutionLOD;
        private System.Windows.Forms.DataGridViewComboBoxColumn EvoBlend;
        private System.Windows.Forms.DataGridViewTextBoxColumn Invis;
    }
}