namespace ParticleEditor
{
    partial class EvolutionSlider
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_LabelID = new System.Windows.Forms.Label();
            this.m_SliderValue = new ParticleEditor.ptxSlider();
            this.m_CheckOverride = new System.Windows.Forms.CheckBox();
            this.m_LabelName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // m_LabelID
            // 
            this.m_LabelID.AutoSize = true;
            this.m_LabelID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelID.Location = new System.Drawing.Point(3, 4);
            this.m_LabelID.Name = "m_LabelID";
            this.m_LabelID.Size = new System.Drawing.Size(22, 13);
            this.m_LabelID.TabIndex = 0;
            this.m_LabelID.Text = "[1]";
            // 
            // m_SliderValue
            // 
            this.m_SliderValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_SliderValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SliderValue.Decimals = 3;
            this.m_SliderValue.Increment = 0.01F;
            this.m_SliderValue.Location = new System.Drawing.Point(166, 2);
            this.m_SliderValue.MaxValue = 1F;
            this.m_SliderValue.MinValue = 0F;
            this.m_SliderValue.Multiline = true;
            this.m_SliderValue.Name = "m_SliderValue";
            this.m_SliderValue.SetValue = 0F;
            this.m_SliderValue.Size = new System.Drawing.Size(63, 20);
            this.m_SliderValue.TabIndex = 2;
            this.m_SliderValue.Text = "0.000";
            this.m_SliderValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_SliderValue.Value = 0F;
            // 
            // m_CheckOverride
            // 
            this.m_CheckOverride.AutoSize = true;
            this.m_CheckOverride.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_CheckOverride.Location = new System.Drawing.Point(28, 5);
            this.m_CheckOverride.Name = "m_CheckOverride";
            this.m_CheckOverride.Size = new System.Drawing.Size(15, 14);
            this.m_CheckOverride.TabIndex = 3;
            this.m_CheckOverride.UseVisualStyleBackColor = true;
            this.m_CheckOverride.MouseClick += new System.Windows.Forms.MouseEventHandler(this.m_CheckOverride_MouseClick);
            // 
            // m_LabelName
            // 
            this.m_LabelName.AutoSize = true;
            this.m_LabelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelName.Location = new System.Drawing.Point(42, 4);
            this.m_LabelName.Name = "m_LabelName";
            this.m_LabelName.Size = new System.Drawing.Size(39, 13);
            this.m_LabelName.TabIndex = 4;
            this.m_LabelName.Text = "Name";
            this.m_LabelName.Click += new System.EventHandler(this.m_LabelName_Click);
            // 
            // EvolutionSlider
            // 
            this.Controls.Add(this.m_CheckOverride);
            this.Controls.Add(this.m_LabelName);
            this.Controls.Add(this.m_SliderValue);
            this.Controls.Add(this.m_LabelID);
            this.Name = "EvolutionSlider";
            this.Size = new System.Drawing.Size(239, 24);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_LabelID;
        private ptxSlider m_SliderValue;
        private System.Windows.Forms.CheckBox m_CheckOverride;
        private System.Windows.Forms.Label m_LabelName;
    }
}
