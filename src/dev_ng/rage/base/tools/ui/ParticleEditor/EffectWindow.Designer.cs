namespace ParticleEditor
{
    partial class EffectWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_LabelHeader = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.m_PanelHeader = new System.Windows.Forms.Panel();
            this.m_PanelGroup = new System.Windows.Forms.Panel();
            this.m_PanelSection = new System.Windows.Forms.Panel();
            this.ptxSubSection3 = new ParticleEditor.ptxSubSection();
            this.m_CheckShowPointQuads = new System.Windows.Forms.CheckBox();
            this.m_CheckShowDataSpheres = new System.Windows.Forms.CheckBox();
            this.m_ShowPhysics = new System.Windows.Forms.CheckBox();
            this.m_CheckShowCullSp = new System.Windows.Forms.CheckBox();
            this.m_CheckShowInst = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_SubSectionGameSpecific = new ParticleEditor.ptxSubSection();
            this.m_PanelGameData = new System.Windows.Forms.Panel();
            this.m_SubSectionEffectOverride = new ParticleEditor.ptxSubSection();
            this.m_EffectOverrides = new ParticleEditor.EffectOverrides();
            this.ptxSubSection5 = new ParticleEditor.ptxSubSection();
            this.m_Scale = new ParticleEditor.ptxSlider();
            this.m_ButtonFreezeScale = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.ptxSubSection4 = new ParticleEditor.ptxSubSection();
            this.m_DataGridEffectKeyframeList = new System.Windows.Forms.DataGridView();
            this.m_ColumnTextRuleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvolutionLOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvoBlend = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Invis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_ContextMenuKeyProp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_MenuSelectedProp = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuEditKeyframe = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.m_MenuEvolutions = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuEvoAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuEvoRemoveAll = new System.Windows.Forms.ToolStripMenuItem();
            this.m_CheckUseRandomColorTint = new System.Windows.Forms.CheckBox();
            this.m_ComboDataVolumeType = new System.Windows.Forms.ComboBox();
            this.m_CheckUseDataSphere = new System.Windows.Forms.CheckBox();
            this.label35 = new System.Windows.Forms.Label();
            this.m_SubSectionTimeline = new ParticleEditor.ptxSubSection();
            this.label18 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.m_EvtEndRatio = new ParticleEditor.ptxSlider();
            this.m_ColorMaxButton = new RageUserControls.ButtonEX();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.m_EvtStartRatio = new ParticleEditor.ptxSlider();
            this.m_ColorMinButton = new RageUserControls.ButtonEX();
            this.m_EvtZoomMin = new ParticleEditor.ptxSlider();
            this.m_EvtZoomMax = new ParticleEditor.ptxSlider();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.m_ButtonEXFilterModels = new RageUserControls.ButtonEX();
            this.label22 = new System.Windows.Forms.Label();
            this.m_ComboPtxRule = new System.Windows.Forms.ComboBox();
            this.m_ButtonEXFilterSprites = new RageUserControls.ButtonEX();
            this.label1 = new System.Windows.Forms.Label();
            this.m_ComboEmitRule = new System.Windows.Forms.ComboBox();
            this.m_EvtPlaybackMin = new ParticleEditor.ptxSlider();
            this.m_EvtPlaybackMax = new ParticleEditor.ptxSlider();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.ptxSubSection2 = new ParticleEditor.ptxSubSection();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.m_CullViewportEmit = new System.Windows.Forms.CheckBox();
            this.m_CullViewportUpdate = new System.Windows.Forms.CheckBox();
            this.label36 = new System.Windows.Forms.Label();
            this.m_CullViewportOffsetZ = new ParticleEditor.ptxSlider();
            this.m_CullViewportRender = new System.Windows.Forms.CheckBox();
            this.m_CullViewportModeCombo = new System.Windows.Forms.ComboBox();
            this.m_CullViewportRadius = new ParticleEditor.ptxSlider();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.m_CullViewportOffsetY = new ParticleEditor.ptxSlider();
            this.m_CullViewportOffsetX = new ParticleEditor.ptxSlider();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.m_CullDistanceEmit = new System.Windows.Forms.CheckBox();
            this.m_CullDistanceCullDist = new ParticleEditor.ptxSlider();
            this.m_CullDistanceUpdate = new System.Windows.Forms.CheckBox();
            this.m_CullDistanceModeCombo = new System.Windows.Forms.ComboBox();
            this.m_CullDistanceRender = new System.Windows.Forms.CheckBox();
            this.label37 = new System.Windows.Forms.Label();
            this.m_CullDistanceFadeDist = new ParticleEditor.ptxSlider();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.m_ColnOnlyBVH = new System.Windows.Forms.CheckBox();
            this.m_ColnUseEntity = new System.Windows.Forms.CheckBox();
            this.label48 = new System.Windows.Forms.Label();
            this.m_ColnProbeDist = new ParticleEditor.ptxSlider();
            this.m_ColnRange = new ParticleEditor.ptxSlider();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.m_ColnTypeCombo = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.m_LODCreate = new System.Windows.Forms.Button();
            this.m_LODFarDistance = new ParticleEditor.ptxSlider();
            this.m_LODNearDistance = new ParticleEditor.ptxSlider();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.ptxSubSection1 = new ParticleEditor.ptxSubSection();
            this.m_RndOffsetPosZ = new ParticleEditor.ptxSlider();
            this.m_RndOffsetPosY = new ParticleEditor.ptxSlider();
            this.m_RndOffsetPosX = new ParticleEditor.ptxSlider();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.m_CheckSortEvents = new System.Windows.Forms.CheckBox();
            this.m_DurationMin = new ParticleEditor.ptxSlider();
            this.m_ComboDrawList = new System.Windows.Forms.ComboBox();
            this.m_DurationMax = new ParticleEditor.ptxSlider();
            this.label26 = new System.Windows.Forms.Label();
            this.m_PreUpdate = new ParticleEditor.ptxSlider();
            this.m_PreUpdateInterval = new ParticleEditor.ptxSlider();
            this.m_NumLoops = new ParticleEditor.ptxSlider();
            this.m_PlayBackMax = new ParticleEditor.ptxSlider();
            this.m_PlayBackMin = new ParticleEditor.ptxSlider();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.m_SubEvolutions = new ParticleEditor.ptxSubSection();
            this.m_DataGridEvoList = new System.Windows.Forms.DataGridView();
            this.Index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_ContextMenuEvoList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuItemNewEvo = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuItemLoadEvo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.m_MenuItemSelectedEvo = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuItemDelEvo = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuItemRenameEvo = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuItemCloneEvo = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuItemSaveEvo = new System.Windows.Forms.ToolStripMenuItem();
            this.ptxSlider1 = new ParticleEditor.ptxSlider();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.m_PanelHeader.SuspendLayout();
            this.m_PanelGroup.SuspendLayout();
            this.m_PanelSection.SuspendLayout();
            this.ptxSubSection3.SuspendLayout();
            this.m_SubSectionGameSpecific.SuspendLayout();
            this.m_SubSectionEffectOverride.SuspendLayout();
            this.ptxSubSection5.SuspendLayout();
            this.ptxSubSection4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridEffectKeyframeList)).BeginInit();
            this.m_ContextMenuKeyProp.SuspendLayout();
            this.m_SubSectionTimeline.SuspendLayout();
            this.ptxSubSection2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.ptxSubSection1.SuspendLayout();
            this.m_SubEvolutions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridEvoList)).BeginInit();
            this.m_ContextMenuEvoList.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_LabelHeader
            // 
            this.m_LabelHeader.AutoSize = true;
            this.m_LabelHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelHeader.Location = new System.Drawing.Point(-4, 4);
            this.m_LabelHeader.Name = "m_LabelHeader";
            this.m_LabelHeader.Size = new System.Drawing.Size(61, 22);
            this.m_LabelHeader.TabIndex = 2;
            this.m_LabelHeader.Text = "Effect:";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(0, 0);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 0;
            // 
            // m_PanelHeader
            // 
            this.m_PanelHeader.Controls.Add(this.m_LabelHeader);
            this.m_PanelHeader.Location = new System.Drawing.Point(13, -1);
            this.m_PanelHeader.Name = "m_PanelHeader";
            this.m_PanelHeader.Size = new System.Drawing.Size(436, 26);
            this.m_PanelHeader.TabIndex = 4;
            // 
            // m_PanelGroup
            // 
            this.m_PanelGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.m_PanelGroup.AutoScroll = true;
            this.m_PanelGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_PanelGroup.Controls.Add(this.m_PanelSection);
            this.m_PanelGroup.Location = new System.Drawing.Point(11, 26);
            this.m_PanelGroup.Margin = new System.Windows.Forms.Padding(0);
            this.m_PanelGroup.Name = "m_PanelGroup";
            this.m_PanelGroup.Size = new System.Drawing.Size(453, 712);
            this.m_PanelGroup.TabIndex = 5;
            // 
            // m_PanelSection
            // 
            this.m_PanelSection.AutoSize = true;
            this.m_PanelSection.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_PanelSection.Controls.Add(this.ptxSubSection3);
            this.m_PanelSection.Controls.Add(this.m_SubSectionGameSpecific);
            this.m_PanelSection.Controls.Add(this.m_SubSectionEffectOverride);
            this.m_PanelSection.Controls.Add(this.ptxSubSection5);
            this.m_PanelSection.Controls.Add(this.ptxSubSection4);
            this.m_PanelSection.Controls.Add(this.m_SubSectionTimeline);
            this.m_PanelSection.Controls.Add(this.ptxSubSection2);
            this.m_PanelSection.Controls.Add(this.ptxSubSection1);
            this.m_PanelSection.Controls.Add(this.m_SubEvolutions);
            this.m_PanelSection.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_PanelSection.Location = new System.Drawing.Point(0, 0);
            this.m_PanelSection.MaximumSize = new System.Drawing.Size(433, 0);
            this.m_PanelSection.MinimumSize = new System.Drawing.Size(433, 713);
            this.m_PanelSection.Name = "m_PanelSection";
            this.m_PanelSection.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.m_PanelSection.Size = new System.Drawing.Size(433, 1607);
            this.m_PanelSection.TabIndex = 2;
            // 
            // ptxSubSection3
            // 
            this.ptxSubSection3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptxSubSection3.Controls.Add(this.m_CheckShowPointQuads);
            this.ptxSubSection3.Controls.Add(this.m_CheckShowDataSpheres);
            this.ptxSubSection3.Controls.Add(this.m_ShowPhysics);
            this.ptxSubSection3.Controls.Add(this.m_CheckShowCullSp);
            this.ptxSubSection3.Controls.Add(this.m_CheckShowInst);
            this.ptxSubSection3.Controls.Add(this.panel1);
            this.ptxSubSection3.Dock = System.Windows.Forms.DockStyle.Top;
            this.ptxSubSection3.Expanded = true;
            this.ptxSubSection3.HeaderText = "";
            this.ptxSubSection3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ptxSubSection3.Location = new System.Drawing.Point(0, 1466);
            this.ptxSubSection3.Name = "ptxSubSection3";
            this.ptxSubSection3.SectionName = "Debug";
            this.ptxSubSection3.Size = new System.Drawing.Size(433, 121);
            this.ptxSubSection3.TabIndex = 26;
            // 
            // m_CheckShowPointQuads
            // 
            this.m_CheckShowPointQuads.AutoSize = true;
            this.m_CheckShowPointQuads.ForeColor = System.Drawing.Color.Red;
            this.m_CheckShowPointQuads.Location = new System.Drawing.Point(275, 28);
            this.m_CheckShowPointQuads.Name = "m_CheckShowPointQuads";
            this.m_CheckShowPointQuads.Size = new System.Drawing.Size(114, 17);
            this.m_CheckShowPointQuads.TabIndex = 45;
            this.m_CheckShowPointQuads.Text = "Show Point Quads";
            this.m_CheckShowPointQuads.UseVisualStyleBackColor = true;
            this.m_CheckShowPointQuads.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_CheckShowDataSpheres
            // 
            this.m_CheckShowDataSpheres.AutoSize = true;
            this.m_CheckShowDataSpheres.ForeColor = System.Drawing.Color.Red;
            this.m_CheckShowDataSpheres.Location = new System.Drawing.Point(145, 51);
            this.m_CheckShowDataSpheres.Name = "m_CheckShowDataSpheres";
            this.m_CheckShowDataSpheres.Size = new System.Drawing.Size(120, 17);
            this.m_CheckShowDataSpheres.TabIndex = 43;
            this.m_CheckShowDataSpheres.Text = "Show Data Capsule";
            this.m_CheckShowDataSpheres.UseVisualStyleBackColor = true;
            this.m_CheckShowDataSpheres.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_ShowPhysics
            // 
            this.m_ShowPhysics.AutoSize = true;
            this.m_ShowPhysics.ForeColor = System.Drawing.Color.Red;
            this.m_ShowPhysics.Location = new System.Drawing.Point(145, 28);
            this.m_ShowPhysics.Name = "m_ShowPhysics";
            this.m_ShowPhysics.Size = new System.Drawing.Size(92, 17);
            this.m_ShowPhysics.TabIndex = 44;
            this.m_ShowPhysics.Text = "Show Physics";
            this.m_ShowPhysics.UseVisualStyleBackColor = true;
            this.m_ShowPhysics.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_CheckShowCullSp
            // 
            this.m_CheckShowCullSp.AutoSize = true;
            this.m_CheckShowCullSp.ForeColor = System.Drawing.Color.Red;
            this.m_CheckShowCullSp.Location = new System.Drawing.Point(8, 51);
            this.m_CheckShowCullSp.Name = "m_CheckShowCullSp";
            this.m_CheckShowCullSp.Size = new System.Drawing.Size(110, 17);
            this.m_CheckShowCullSp.TabIndex = 41;
            this.m_CheckShowCullSp.Text = "Show Cullspheres";
            this.m_CheckShowCullSp.UseVisualStyleBackColor = true;
            this.m_CheckShowCullSp.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_CheckShowInst
            // 
            this.m_CheckShowInst.AutoSize = true;
            this.m_CheckShowInst.ForeColor = System.Drawing.Color.Red;
            this.m_CheckShowInst.Location = new System.Drawing.Point(8, 28);
            this.m_CheckShowInst.Name = "m_CheckShowInst";
            this.m_CheckShowInst.Size = new System.Drawing.Size(102, 17);
            this.m_CheckShowInst.TabIndex = 42;
            this.m_CheckShowInst.Text = "Show Instances";
            this.m_CheckShowInst.UseVisualStyleBackColor = true;
            this.m_CheckShowInst.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Location = new System.Drawing.Point(6, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(0, 0);
            this.panel1.TabIndex = 2;
            // 
            // m_SubSectionGameSpecific
            // 
            this.m_SubSectionGameSpecific.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SubSectionGameSpecific.Controls.Add(this.m_PanelGameData);
            this.m_SubSectionGameSpecific.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_SubSectionGameSpecific.Expanded = true;
            this.m_SubSectionGameSpecific.HeaderText = "";
            this.m_SubSectionGameSpecific.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.m_SubSectionGameSpecific.Location = new System.Drawing.Point(0, 1360);
            this.m_SubSectionGameSpecific.Name = "m_SubSectionGameSpecific";
            this.m_SubSectionGameSpecific.SectionName = "Game Specific Data";
            this.m_SubSectionGameSpecific.Size = new System.Drawing.Size(433, 106);
            this.m_SubSectionGameSpecific.TabIndex = 25;
            // 
            // m_PanelGameData
            // 
            this.m_PanelGameData.AutoSize = true;
            this.m_PanelGameData.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_PanelGameData.Location = new System.Drawing.Point(6, 28);
            this.m_PanelGameData.Name = "m_PanelGameData";
            this.m_PanelGameData.Size = new System.Drawing.Size(0, 0);
            this.m_PanelGameData.TabIndex = 2;
            // 
            // m_SubSectionEffectOverride
            // 
            this.m_SubSectionEffectOverride.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_SubSectionEffectOverride.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SubSectionEffectOverride.Controls.Add(this.m_EffectOverrides);
            this.m_SubSectionEffectOverride.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_SubSectionEffectOverride.Expanded = true;
            this.m_SubSectionEffectOverride.HeaderText = "";
            this.m_SubSectionEffectOverride.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.m_SubSectionEffectOverride.Location = new System.Drawing.Point(0, 1223);
            this.m_SubSectionEffectOverride.Name = "m_SubSectionEffectOverride";
            this.m_SubSectionEffectOverride.SectionName = "Effect Event Scalars";
            this.m_SubSectionEffectOverride.Size = new System.Drawing.Size(433, 137);
            this.m_SubSectionEffectOverride.TabIndex = 24;
            this.m_SubSectionEffectOverride.Visible = false;
            // 
            // m_EffectOverrides
            // 
            this.m_EffectOverrides.Location = new System.Drawing.Point(4, 25);
            this.m_EffectOverrides.Name = "m_EffectOverrides";
            this.m_EffectOverrides.Overridable = null;
            this.m_EffectOverrides.Size = new System.Drawing.Size(416, 113);
            this.m_EffectOverrides.TabIndex = 2;
            this.m_EffectOverrides.UseInheritDirection = false;
            // 
            // ptxSubSection5
            // 
            this.ptxSubSection5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptxSubSection5.Controls.Add(this.m_Scale);
            this.ptxSubSection5.Controls.Add(this.m_ButtonFreezeScale);
            this.ptxSubSection5.Controls.Add(this.label21);
            this.ptxSubSection5.Dock = System.Windows.Forms.DockStyle.Top;
            this.ptxSubSection5.Expanded = true;
            this.ptxSubSection5.HeaderText = "";
            this.ptxSubSection5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ptxSubSection5.Location = new System.Drawing.Point(0, 1163);
            this.ptxSubSection5.Name = "ptxSubSection5";
            this.ptxSubSection5.SectionName = "Re-Scale Effect";
            this.ptxSubSection5.Size = new System.Drawing.Size(433, 60);
            this.ptxSubSection5.TabIndex = 5;
            // 
            // m_Scale
            // 
            this.m_Scale.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_Scale.Decimals = 3;
            this.m_Scale.Increment = 0.1F;
            this.m_Scale.Location = new System.Drawing.Point(83, 26);
            this.m_Scale.MaxValue = 1000000F;
            this.m_Scale.MinValue = -1000000F;
            this.m_Scale.Multiline = true;
            this.m_Scale.Name = "m_Scale";
            this.m_Scale.SetValue = 100F;
            this.m_Scale.Size = new System.Drawing.Size(59, 20);
            this.m_Scale.TabIndex = 24;
            this.m_Scale.Text = "100.000";
            this.m_Scale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_Scale.Value = 100F;
            this.m_Scale.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_ButtonFreezeScale
            // 
            this.m_ButtonFreezeScale.AutoSize = true;
            this.m_ButtonFreezeScale.Location = new System.Drawing.Point(228, 23);
            this.m_ButtonFreezeScale.Name = "m_ButtonFreezeScale";
            this.m_ButtonFreezeScale.Size = new System.Drawing.Size(105, 23);
            this.m_ButtonFreezeScale.TabIndex = 21;
            this.m_ButtonFreezeScale.Text = "Normalise Zoom";
            this.m_ButtonFreezeScale.UseVisualStyleBackColor = true;
            this.m_ButtonFreezeScale.Click += new System.EventHandler(this.m_ButtonFreezeScale_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 28);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(63, 13);
            this.label21.TabIndex = 19;
            this.label21.Text = "Zoom Level";
            // 
            // ptxSubSection4
            // 
            this.ptxSubSection4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptxSubSection4.Controls.Add(this.m_DataGridEffectKeyframeList);
            this.ptxSubSection4.Controls.Add(this.m_CheckUseRandomColorTint);
            this.ptxSubSection4.Controls.Add(this.m_ComboDataVolumeType);
            this.ptxSubSection4.Controls.Add(this.m_CheckUseDataSphere);
            this.ptxSubSection4.Controls.Add(this.label35);
            this.ptxSubSection4.Dock = System.Windows.Forms.DockStyle.Top;
            this.ptxSubSection4.Expanded = true;
            this.ptxSubSection4.HeaderText = "";
            this.ptxSubSection4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ptxSubSection4.Location = new System.Drawing.Point(0, 971);
            this.ptxSubSection4.Name = "ptxSubSection4";
            this.ptxSubSection4.SectionName = "Global Effect Keyframing";
            this.ptxSubSection4.Size = new System.Drawing.Size(433, 192);
            this.ptxSubSection4.TabIndex = 4;
            // 
            // m_DataGridEffectKeyframeList
            // 
            this.m_DataGridEffectKeyframeList.AllowUserToAddRows = false;
            this.m_DataGridEffectKeyframeList.AllowUserToDeleteRows = false;
            this.m_DataGridEffectKeyframeList.AllowUserToResizeColumns = false;
            this.m_DataGridEffectKeyframeList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AliceBlue;
            this.m_DataGridEffectKeyframeList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.m_DataGridEffectKeyframeList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.m_DataGridEffectKeyframeList.BackgroundColor = System.Drawing.SystemColors.Window;
            this.m_DataGridEffectKeyframeList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.m_DataGridEffectKeyframeList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.m_DataGridEffectKeyframeList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridEffectKeyframeList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.m_DataGridEffectKeyframeList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.m_ColumnTextRuleName,
            this.Evolution1,
            this.Evolution2,
            this.Evolution3,
            this.Evolution4,
            this.Evolution5,
            this.EvolutionLOD,
            this.EvoBlend,
            this.Invis});
            this.m_DataGridEffectKeyframeList.ContextMenuStrip = this.m_ContextMenuKeyProp;
            this.m_DataGridEffectKeyframeList.GridColor = System.Drawing.SystemColors.Window;
            this.m_DataGridEffectKeyframeList.Location = new System.Drawing.Point(6, 25);
            this.m_DataGridEffectKeyframeList.MultiSelect = false;
            this.m_DataGridEffectKeyframeList.Name = "m_DataGridEffectKeyframeList";
            this.m_DataGridEffectKeyframeList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridEffectKeyframeList.RowHeadersVisible = false;
            this.m_DataGridEffectKeyframeList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.m_DataGridEffectKeyframeList.RowTemplate.Height = 16;
            this.m_DataGridEffectKeyframeList.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridEffectKeyframeList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_DataGridEffectKeyframeList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.m_DataGridEffectKeyframeList.ShowCellToolTips = false;
            this.m_DataGridEffectKeyframeList.Size = new System.Drawing.Size(422, 105);
            this.m_DataGridEffectKeyframeList.TabIndex = 22;
            this.m_DataGridEffectKeyframeList.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SelectOnRightMouseButton_CellMouseDown);
            this.m_DataGridEffectKeyframeList.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_DataGridEffectKeyframeList_CellValueChanged);
            this.m_DataGridEffectKeyframeList.CurrentCellDirtyStateChanged += new System.EventHandler(this.m_DataGridEffectKeyframeList_CurrentCellDirtyStateChanged);
            this.m_DataGridEffectKeyframeList.SelectionChanged += new System.EventHandler(this.DataGridAutoSelectValidCell);
            this.m_DataGridEffectKeyframeList.DoubleClick += new System.EventHandler(this.ShowKeyframe);
            // 
            // m_ColumnTextRuleName
            // 
            this.m_ColumnTextRuleName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.m_ColumnTextRuleName.HeaderText = "Property";
            this.m_ColumnTextRuleName.Name = "m_ColumnTextRuleName";
            this.m_ColumnTextRuleName.ReadOnly = true;
            this.m_ColumnTextRuleName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_ColumnTextRuleName.Width = 71;
            // 
            // Evolution1
            // 
            this.Evolution1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Red;
            this.Evolution1.DefaultCellStyle = dataGridViewCellStyle23;
            this.Evolution1.HeaderText = "Evolution1";
            this.Evolution1.MinimumWidth = 2;
            this.Evolution1.Name = "Evolution1";
            this.Evolution1.ReadOnly = true;
            this.Evolution1.Width = 2;
            // 
            // Evolution2
            // 
            this.Evolution2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Blue;
            this.Evolution2.DefaultCellStyle = dataGridViewCellStyle24;
            this.Evolution2.HeaderText = "Evolution2";
            this.Evolution2.MinimumWidth = 2;
            this.Evolution2.Name = "Evolution2";
            this.Evolution2.ReadOnly = true;
            this.Evolution2.Width = 2;
            // 
            // Evolution3
            // 
            this.Evolution3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Green;
            this.Evolution3.DefaultCellStyle = dataGridViewCellStyle25;
            this.Evolution3.HeaderText = "Evolution3";
            this.Evolution3.MinimumWidth = 2;
            this.Evolution3.Name = "Evolution3";
            this.Evolution3.ReadOnly = true;
            this.Evolution3.Width = 2;
            // 
            // Evolution4
            // 
            this.Evolution4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.Purple;
            this.Evolution4.DefaultCellStyle = dataGridViewCellStyle26;
            this.Evolution4.HeaderText = "Evolution4";
            this.Evolution4.MinimumWidth = 2;
            this.Evolution4.Name = "Evolution4";
            this.Evolution4.ReadOnly = true;
            this.Evolution4.Width = 2;
            // 
            // Evolution5
            // 
            this.Evolution5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.SaddleBrown;
            this.Evolution5.DefaultCellStyle = dataGridViewCellStyle27;
            this.Evolution5.HeaderText = "Evolution5";
            this.Evolution5.MinimumWidth = 2;
            this.Evolution5.Name = "Evolution5";
            this.Evolution5.ReadOnly = true;
            this.Evolution5.Width = 2;
            // 
            // EvolutionLOD
            // 
            this.EvolutionLOD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.EvolutionLOD.HeaderText = "EvolutionLOD";
            this.EvolutionLOD.MinimumWidth = 2;
            this.EvolutionLOD.Name = "EvolutionLOD";
            this.EvolutionLOD.ReadOnly = true;
            this.EvolutionLOD.Width = 2;
            // 
            // EvoBlend
            // 
            this.EvoBlend.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EvoBlend.FillWeight = 152.381F;
            this.EvoBlend.HeaderText = "EvoBlend";
            this.EvoBlend.Items.AddRange(new object[] {
            "Active Avg",
            "Add",
            "Max",
            "Full Avg"});
            this.EvoBlend.Name = "EvoBlend";
            this.EvoBlend.Width = 80;
            // 
            // Invis
            // 
            this.Invis.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Invis.FillWeight = 47.61905F;
            this.Invis.HeaderText = "Invis";
            this.Invis.Name = "Invis";
            this.Invis.ReadOnly = true;
            // 
            // m_ContextMenuKeyProp
            // 
            this.m_ContextMenuKeyProp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_MenuSelectedProp,
            this.m_MenuEditKeyframe,
            this.toolStripSeparator2,
            this.m_MenuEvolutions,
            this.m_MenuEvoAdd,
            this.m_MenuEvoRemoveAll});
            this.m_ContextMenuKeyProp.Name = "m_ContextMenuKeyProp";
            this.m_ContextMenuKeyProp.Size = new System.Drawing.Size(153, 120);
            this.m_ContextMenuKeyProp.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContextMenuKeyProp_Opening);
            // 
            // m_MenuSelectedProp
            // 
            this.m_MenuSelectedProp.Enabled = false;
            this.m_MenuSelectedProp.Name = "m_MenuSelectedProp";
            this.m_MenuSelectedProp.Size = new System.Drawing.Size(152, 22);
            this.m_MenuSelectedProp.Text = "Selected";
            // 
            // m_MenuEditKeyframe
            // 
            this.m_MenuEditKeyframe.Name = "m_MenuEditKeyframe";
            this.m_MenuEditKeyframe.Size = new System.Drawing.Size(152, 22);
            this.m_MenuEditKeyframe.Text = "Edit Keyframes";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(149, 6);
            // 
            // m_MenuEvolutions
            // 
            this.m_MenuEvolutions.Enabled = false;
            this.m_MenuEvolutions.Name = "m_MenuEvolutions";
            this.m_MenuEvolutions.Size = new System.Drawing.Size(152, 22);
            this.m_MenuEvolutions.Text = "Evolutions";
            // 
            // m_MenuEvoAdd
            // 
            this.m_MenuEvoAdd.Name = "m_MenuEvoAdd";
            this.m_MenuEvoAdd.Size = new System.Drawing.Size(152, 22);
            this.m_MenuEvoAdd.Text = "Add";
            // 
            // m_MenuEvoRemoveAll
            // 
            this.m_MenuEvoRemoveAll.Name = "m_MenuEvoRemoveAll";
            this.m_MenuEvoRemoveAll.Size = new System.Drawing.Size(152, 22);
            this.m_MenuEvoRemoveAll.Text = "Remove All";
            // 
            // m_CheckUseRandomColorTint
            // 
            this.m_CheckUseRandomColorTint.AutoSize = true;
            this.m_CheckUseRandomColorTint.Location = new System.Drawing.Point(6, 136);
            this.m_CheckUseRandomColorTint.Name = "m_CheckUseRandomColorTint";
            this.m_CheckUseRandomColorTint.Size = new System.Drawing.Size(136, 17);
            this.m_CheckUseRandomColorTint.TabIndex = 30;
            this.m_CheckUseRandomColorTint.Text = "Enable Colour Tint Max";
            this.m_CheckUseRandomColorTint.UseVisualStyleBackColor = true;
            this.m_CheckUseRandomColorTint.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_ComboDataVolumeType
            // 
            this.m_ComboDataVolumeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboDataVolumeType.FormattingEnabled = true;
            this.m_ComboDataVolumeType.Items.AddRange(new object[] {
            "Default"});
            this.m_ComboDataVolumeType.Location = new System.Drawing.Point(278, 158);
            this.m_ComboDataVolumeType.Name = "m_ComboDataVolumeType";
            this.m_ComboDataVolumeType.Size = new System.Drawing.Size(141, 21);
            this.m_ComboDataVolumeType.TabIndex = 89;
            this.m_ComboDataVolumeType.SelectionChangeCommitted += new System.EventHandler(this.m_ComboDataVolumeType_SelectedIndexChanged);
            // 
            // m_CheckUseDataSphere
            // 
            this.m_CheckUseDataSphere.AutoSize = true;
            this.m_CheckUseDataSphere.Location = new System.Drawing.Point(6, 160);
            this.m_CheckUseDataSphere.Name = "m_CheckUseDataSphere";
            this.m_CheckUseDataSphere.Size = new System.Drawing.Size(109, 17);
            this.m_CheckUseDataSphere.TabIndex = 31;
            this.m_CheckUseDataSphere.Text = "Use Data Volume";
            this.m_CheckUseDataSphere.UseVisualStyleBackColor = true;
            this.m_CheckUseDataSphere.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(177, 161);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(95, 13);
            this.label35.TabIndex = 88;
            this.label35.Text = "Data Volume Type";
            // 
            // m_SubSectionTimeline
            // 
            this.m_SubSectionTimeline.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SubSectionTimeline.Controls.Add(this.label18);
            this.m_SubSectionTimeline.Controls.Add(this.label5);
            this.m_SubSectionTimeline.Controls.Add(this.m_EvtEndRatio);
            this.m_SubSectionTimeline.Controls.Add(this.m_ColorMaxButton);
            this.m_SubSectionTimeline.Controls.Add(this.label30);
            this.m_SubSectionTimeline.Controls.Add(this.label29);
            this.m_SubSectionTimeline.Controls.Add(this.m_EvtStartRatio);
            this.m_SubSectionTimeline.Controls.Add(this.m_ColorMinButton);
            this.m_SubSectionTimeline.Controls.Add(this.m_EvtZoomMin);
            this.m_SubSectionTimeline.Controls.Add(this.m_EvtZoomMax);
            this.m_SubSectionTimeline.Controls.Add(this.label27);
            this.m_SubSectionTimeline.Controls.Add(this.label28);
            this.m_SubSectionTimeline.Controls.Add(this.m_ButtonEXFilterModels);
            this.m_SubSectionTimeline.Controls.Add(this.label22);
            this.m_SubSectionTimeline.Controls.Add(this.m_ComboPtxRule);
            this.m_SubSectionTimeline.Controls.Add(this.m_ButtonEXFilterSprites);
            this.m_SubSectionTimeline.Controls.Add(this.label1);
            this.m_SubSectionTimeline.Controls.Add(this.m_ComboEmitRule);
            this.m_SubSectionTimeline.Controls.Add(this.m_EvtPlaybackMin);
            this.m_SubSectionTimeline.Controls.Add(this.m_EvtPlaybackMax);
            this.m_SubSectionTimeline.Controls.Add(this.label20);
            this.m_SubSectionTimeline.Controls.Add(this.label17);
            this.m_SubSectionTimeline.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_SubSectionTimeline.Expanded = true;
            this.m_SubSectionTimeline.HeaderText = "";
            this.m_SubSectionTimeline.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.m_SubSectionTimeline.Location = new System.Drawing.Point(0, 791);
            this.m_SubSectionTimeline.Name = "m_SubSectionTimeline";
            this.m_SubSectionTimeline.SectionName = "Timeline Event";
            this.m_SubSectionTimeline.Size = new System.Drawing.Size(433, 180);
            this.m_SubSectionTimeline.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(6, 30);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 13);
            this.label18.TabIndex = 56;
            this.label18.Text = "Start Ratio";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(218, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 88;
            this.label5.Text = "End Ratio";
            // 
            // m_EvtEndRatio
            // 
            this.m_EvtEndRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_EvtEndRatio.Decimals = 3;
            this.m_EvtEndRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_EvtEndRatio.Increment = 0.001F;
            this.m_EvtEndRatio.Location = new System.Drawing.Point(353, 28);
            this.m_EvtEndRatio.MaxValue = 10000F;
            this.m_EvtEndRatio.MinValue = 0F;
            this.m_EvtEndRatio.Multiline = true;
            this.m_EvtEndRatio.Name = "m_EvtEndRatio";
            this.m_EvtEndRatio.SetValue = 0F;
            this.m_EvtEndRatio.Size = new System.Drawing.Size(59, 20);
            this.m_EvtEndRatio.TabIndex = 89;
            this.m_EvtEndRatio.Text = "0.000";
            this.m_EvtEndRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_EvtEndRatio.Value = 0F;
            this.m_EvtEndRatio.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_ColorMaxButton
            // 
            this.m_ColorMaxButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_ColorMaxButton.ButtonState = true;
            this.m_ColorMaxButton.ButtonText = "";
            this.m_ColorMaxButton.ColorDown = System.Drawing.Color.LightBlue;
            this.m_ColorMaxButton.ColorUp = System.Drawing.Color.White;
            this.m_ColorMaxButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ColorMaxButton.Location = new System.Drawing.Point(330, 105);
            this.m_ColorMaxButton.Name = "m_ColorMaxButton";
            this.m_ColorMaxButton.Size = new System.Drawing.Size(82, 17);
            this.m_ColorMaxButton.TabIndex = 87;
            this.m_ColorMaxButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.m_ColorMaxButton.OnButtonStateChangedByUser += new RageUserControls.ButtonEX.ButtonEXEventHandler(this.m_ColorMaxButton_OnButtonStateChangedByUser);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(218, 107);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(75, 13);
            this.label30.TabIndex = 85;
            this.label30.Text = "Color Tint Max";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(6, 107);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(72, 13);
            this.label29.TabIndex = 84;
            this.label29.Text = "Color Tint Min";
            // 
            // m_EvtStartRatio
            // 
            this.m_EvtStartRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_EvtStartRatio.Decimals = 3;
            this.m_EvtStartRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_EvtStartRatio.Increment = 0.001F;
            this.m_EvtStartRatio.Location = new System.Drawing.Point(119, 28);
            this.m_EvtStartRatio.MaxValue = 10000F;
            this.m_EvtStartRatio.MinValue = 0F;
            this.m_EvtStartRatio.Multiline = true;
            this.m_EvtStartRatio.Name = "m_EvtStartRatio";
            this.m_EvtStartRatio.SetValue = 0F;
            this.m_EvtStartRatio.Size = new System.Drawing.Size(59, 20);
            this.m_EvtStartRatio.TabIndex = 61;
            this.m_EvtStartRatio.Text = "0.000";
            this.m_EvtStartRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_EvtStartRatio.Value = 0F;
            this.m_EvtStartRatio.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_ColorMinButton
            // 
            this.m_ColorMinButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_ColorMinButton.ButtonState = true;
            this.m_ColorMinButton.ButtonText = "";
            this.m_ColorMinButton.ColorDown = System.Drawing.Color.LightBlue;
            this.m_ColorMinButton.ColorUp = System.Drawing.Color.White;
            this.m_ColorMinButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ColorMinButton.Location = new System.Drawing.Point(96, 105);
            this.m_ColorMinButton.Name = "m_ColorMinButton";
            this.m_ColorMinButton.Size = new System.Drawing.Size(82, 17);
            this.m_ColorMinButton.TabIndex = 86;
            this.m_ColorMinButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.m_ColorMinButton.OnButtonStateChangedByUser += new RageUserControls.ButtonEX.ButtonEXEventHandler(this.m_ColorMinButton_OnButtonStateChangedByUser);
            // 
            // m_EvtZoomMin
            // 
            this.m_EvtZoomMin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_EvtZoomMin.Decimals = 3;
            this.m_EvtZoomMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_EvtZoomMin.Increment = 0.001F;
            this.m_EvtZoomMin.Location = new System.Drawing.Point(119, 79);
            this.m_EvtZoomMin.MaxValue = 10000F;
            this.m_EvtZoomMin.MinValue = 0F;
            this.m_EvtZoomMin.Multiline = true;
            this.m_EvtZoomMin.Name = "m_EvtZoomMin";
            this.m_EvtZoomMin.SetValue = 0F;
            this.m_EvtZoomMin.Size = new System.Drawing.Size(59, 20);
            this.m_EvtZoomMin.TabIndex = 81;
            this.m_EvtZoomMin.Text = "0.000";
            this.m_EvtZoomMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_EvtZoomMin.Value = 0F;
            this.m_EvtZoomMin.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_EvtZoomMax
            // 
            this.m_EvtZoomMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_EvtZoomMax.Decimals = 3;
            this.m_EvtZoomMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_EvtZoomMax.Increment = 0.001F;
            this.m_EvtZoomMax.Location = new System.Drawing.Point(353, 79);
            this.m_EvtZoomMax.MaxValue = 10000F;
            this.m_EvtZoomMax.MinValue = 0F;
            this.m_EvtZoomMax.Multiline = true;
            this.m_EvtZoomMax.Name = "m_EvtZoomMax";
            this.m_EvtZoomMax.SetValue = 0F;
            this.m_EvtZoomMax.Size = new System.Drawing.Size(59, 20);
            this.m_EvtZoomMax.TabIndex = 80;
            this.m_EvtZoomMax.Text = "0.000";
            this.m_EvtZoomMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_EvtZoomMax.Value = 0F;
            this.m_EvtZoomMax.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(217, 81);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(68, 13);
            this.label27.TabIndex = 79;
            this.label27.Text = "Zoom Max %";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(6, 81);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(65, 13);
            this.label28.TabIndex = 78;
            this.label28.Text = "Zoom Min %";
            // 
            // m_ButtonEXFilterModels
            // 
            this.m_ButtonEXFilterModels.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_ButtonEXFilterModels.ButtonState = true;
            this.m_ButtonEXFilterModels.ButtonText = "M";
            this.m_ButtonEXFilterModels.ColorDown = System.Drawing.Color.LightBlue;
            this.m_ButtonEXFilterModels.ColorUp = System.Drawing.SystemColors.Control;
            this.m_ButtonEXFilterModels.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ButtonEXFilterModels.Location = new System.Drawing.Point(409, 131);
            this.m_ButtonEXFilterModels.Name = "m_ButtonEXFilterModels";
            this.m_ButtonEXFilterModels.Size = new System.Drawing.Size(19, 17);
            this.m_ButtonEXFilterModels.TabIndex = 77;
            this.m_ButtonEXFilterModels.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.m_ButtonEXFilterModels.OnButtonStateChangedByUser += new RageUserControls.ButtonEX.ButtonEXEventHandler(this.m_ButtonEXFilterModels_OnButtonStateChangedByUser);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(222, 133);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 13);
            this.label22.TabIndex = 73;
            this.label22.Text = "PtxRule:";
            // 
            // m_ComboPtxRule
            // 
            this.m_ComboPtxRule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboPtxRule.FormattingEnabled = true;
            this.m_ComboPtxRule.Location = new System.Drawing.Point(219, 149);
            this.m_ComboPtxRule.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.m_ComboPtxRule.Name = "m_ComboPtxRule";
            this.m_ComboPtxRule.Size = new System.Drawing.Size(210, 21);
            this.m_ComboPtxRule.Sorted = true;
            this.m_ComboPtxRule.TabIndex = 72;
            this.m_ComboPtxRule.DropDown += new System.EventHandler(this.m_ComboPtxRule_DropDown);
            this.m_ComboPtxRule.SelectionChangeCommitted += new System.EventHandler(this.m_ComboPtxRule_SelectionChangeCommitted);
            // 
            // m_ButtonEXFilterSprites
            // 
            this.m_ButtonEXFilterSprites.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_ButtonEXFilterSprites.ButtonState = true;
            this.m_ButtonEXFilterSprites.ButtonText = "S";
            this.m_ButtonEXFilterSprites.ColorDown = System.Drawing.Color.LightBlue;
            this.m_ButtonEXFilterSprites.ColorUp = System.Drawing.SystemColors.Control;
            this.m_ButtonEXFilterSprites.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ButtonEXFilterSprites.Location = new System.Drawing.Point(391, 131);
            this.m_ButtonEXFilterSprites.Name = "m_ButtonEXFilterSprites";
            this.m_ButtonEXFilterSprites.Size = new System.Drawing.Size(19, 17);
            this.m_ButtonEXFilterSprites.TabIndex = 76;
            this.m_ButtonEXFilterSprites.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.m_ButtonEXFilterSprites.OnButtonStateChangedByUser += new RageUserControls.ButtonEX.ButtonEXEventHandler(this.m_ButtonEXFilterSprites_OnButtonStateChangedByUser);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 133);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 69;
            this.label1.Text = "EmitRule:";
            // 
            // m_ComboEmitRule
            // 
            this.m_ComboEmitRule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboEmitRule.FormattingEnabled = true;
            this.m_ComboEmitRule.Location = new System.Drawing.Point(3, 149);
            this.m_ComboEmitRule.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.m_ComboEmitRule.Name = "m_ComboEmitRule";
            this.m_ComboEmitRule.Size = new System.Drawing.Size(210, 21);
            this.m_ComboEmitRule.Sorted = true;
            this.m_ComboEmitRule.TabIndex = 68;
            this.m_ComboEmitRule.DropDown += new System.EventHandler(this.m_ComboEmitRule_DropDown);
            this.m_ComboEmitRule.SelectionChangeCommitted += new System.EventHandler(this.m_ComboPtxRule_SelectionChangeCommitted);
            // 
            // m_EvtPlaybackMin
            // 
            this.m_EvtPlaybackMin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_EvtPlaybackMin.Decimals = 3;
            this.m_EvtPlaybackMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_EvtPlaybackMin.Increment = 0.001F;
            this.m_EvtPlaybackMin.Location = new System.Drawing.Point(119, 54);
            this.m_EvtPlaybackMin.MaxValue = 10000F;
            this.m_EvtPlaybackMin.MinValue = 0F;
            this.m_EvtPlaybackMin.Multiline = true;
            this.m_EvtPlaybackMin.Name = "m_EvtPlaybackMin";
            this.m_EvtPlaybackMin.SetValue = 0F;
            this.m_EvtPlaybackMin.Size = new System.Drawing.Size(59, 20);
            this.m_EvtPlaybackMin.TabIndex = 63;
            this.m_EvtPlaybackMin.Text = "0.000";
            this.m_EvtPlaybackMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_EvtPlaybackMin.Value = 0F;
            this.m_EvtPlaybackMin.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_EvtPlaybackMax
            // 
            this.m_EvtPlaybackMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_EvtPlaybackMax.Decimals = 3;
            this.m_EvtPlaybackMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_EvtPlaybackMax.Increment = 0.001F;
            this.m_EvtPlaybackMax.Location = new System.Drawing.Point(353, 54);
            this.m_EvtPlaybackMax.MaxValue = 10000F;
            this.m_EvtPlaybackMax.MinValue = 0F;
            this.m_EvtPlaybackMax.Multiline = true;
            this.m_EvtPlaybackMax.Name = "m_EvtPlaybackMax";
            this.m_EvtPlaybackMax.SetValue = 0F;
            this.m_EvtPlaybackMax.Size = new System.Drawing.Size(59, 20);
            this.m_EvtPlaybackMax.TabIndex = 62;
            this.m_EvtPlaybackMax.Text = "0.000";
            this.m_EvtPlaybackMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_EvtPlaybackMax.Value = 0F;
            this.m_EvtPlaybackMax.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(217, 56);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(100, 13);
            this.label20.TabIndex = 58;
            this.label20.Text = "Playback Rate Max";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(97, 13);
            this.label17.TabIndex = 55;
            this.label17.Text = "Playback Rate Min";
            // 
            // ptxSubSection2
            // 
            this.ptxSubSection2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptxSubSection2.Controls.Add(this.groupBox3);
            this.ptxSubSection2.Controls.Add(this.groupBox4);
            this.ptxSubSection2.Controls.Add(this.groupBox6);
            this.ptxSubSection2.Controls.Add(this.groupBox5);
            this.ptxSubSection2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ptxSubSection2.Expanded = true;
            this.ptxSubSection2.HeaderText = "";
            this.ptxSubSection2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ptxSubSection2.Location = new System.Drawing.Point(0, 340);
            this.ptxSubSection2.Name = "ptxSubSection2";
            this.ptxSubSection2.SectionName = "CullIng / LOD / Collision";
            this.ptxSubSection2.Size = new System.Drawing.Size(433, 451);
            this.ptxSubSection2.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.m_CullViewportEmit);
            this.groupBox3.Controls.Add(this.m_CullViewportUpdate);
            this.groupBox3.Controls.Add(this.label36);
            this.groupBox3.Controls.Add(this.m_CullViewportOffsetZ);
            this.groupBox3.Controls.Add(this.m_CullViewportRender);
            this.groupBox3.Controls.Add(this.m_CullViewportModeCombo);
            this.groupBox3.Controls.Add(this.m_CullViewportRadius);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this.label39);
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.m_CullViewportOffsetY);
            this.groupBox3.Controls.Add(this.m_CullViewportOffsetX);
            this.groupBox3.Location = new System.Drawing.Point(3, 23);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(425, 118);
            this.groupBox3.TabIndex = 54;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Viewport Culling";
            // 
            // m_CullViewportEmit
            // 
            this.m_CullViewportEmit.AutoSize = true;
            this.m_CullViewportEmit.Location = new System.Drawing.Point(94, 95);
            this.m_CullViewportEmit.Name = "m_CullViewportEmit";
            this.m_CullViewportEmit.Size = new System.Drawing.Size(46, 17);
            this.m_CullViewportEmit.TabIndex = 50;
            this.m_CullViewportEmit.Text = "Emit";
            this.m_CullViewportEmit.UseVisualStyleBackColor = true;
            this.m_CullViewportEmit.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_CullViewportUpdate
            // 
            this.m_CullViewportUpdate.AutoSize = true;
            this.m_CullViewportUpdate.Location = new System.Drawing.Point(94, 72);
            this.m_CullViewportUpdate.Name = "m_CullViewportUpdate";
            this.m_CullViewportUpdate.Size = new System.Drawing.Size(61, 17);
            this.m_CullViewportUpdate.TabIndex = 49;
            this.m_CullViewportUpdate.Text = "Update";
            this.m_CullViewportUpdate.UseVisualStyleBackColor = true;
            this.m_CullViewportUpdate.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(15, 19);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(34, 13);
            this.label36.TabIndex = 46;
            this.label36.Text = "Mode";
            // 
            // m_CullViewportOffsetZ
            // 
            this.m_CullViewportOffsetZ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_CullViewportOffsetZ.Decimals = 3;
            this.m_CullViewportOffsetZ.Increment = 0.1F;
            this.m_CullViewportOffsetZ.Location = new System.Drawing.Point(292, 96);
            this.m_CullViewportOffsetZ.MaxValue = 100F;
            this.m_CullViewportOffsetZ.MinValue = -100F;
            this.m_CullViewportOffsetZ.Multiline = true;
            this.m_CullViewportOffsetZ.Name = "m_CullViewportOffsetZ";
            this.m_CullViewportOffsetZ.SetValue = 0F;
            this.m_CullViewportOffsetZ.Size = new System.Drawing.Size(100, 20);
            this.m_CullViewportOffsetZ.TabIndex = 62;
            this.m_CullViewportOffsetZ.Text = "0.000";
            this.m_CullViewportOffsetZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_CullViewportOffsetZ.Value = 0F;
            this.m_CullViewportOffsetZ.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_CullViewportRender
            // 
            this.m_CullViewportRender.AutoSize = true;
            this.m_CullViewportRender.Location = new System.Drawing.Point(94, 49);
            this.m_CullViewportRender.Name = "m_CullViewportRender";
            this.m_CullViewportRender.Size = new System.Drawing.Size(61, 17);
            this.m_CullViewportRender.TabIndex = 48;
            this.m_CullViewportRender.Text = "Render";
            this.m_CullViewportRender.UseVisualStyleBackColor = true;
            this.m_CullViewportRender.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_CullViewportModeCombo
            // 
            this.m_CullViewportModeCombo.DisplayMember = "(none)";
            this.m_CullViewportModeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_CullViewportModeCombo.FormattingEnabled = true;
            this.m_CullViewportModeCombo.Location = new System.Drawing.Point(67, 19);
            this.m_CullViewportModeCombo.Name = "m_CullViewportModeCombo";
            this.m_CullViewportModeCombo.Size = new System.Drawing.Size(121, 21);
            this.m_CullViewportModeCombo.TabIndex = 44;
            this.m_CullViewportModeCombo.SelectionChangeCommitted += new System.EventHandler(this.m_ComboCullMode_SelectionChangeCommitted);
            // 
            // m_CullViewportRadius
            // 
            this.m_CullViewportRadius.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_CullViewportRadius.Decimals = 3;
            this.m_CullViewportRadius.Increment = 0.1F;
            this.m_CullViewportRadius.Location = new System.Drawing.Point(292, 19);
            this.m_CullViewportRadius.MaxValue = 100F;
            this.m_CullViewportRadius.MinValue = 0F;
            this.m_CullViewportRadius.Multiline = true;
            this.m_CullViewportRadius.Name = "m_CullViewportRadius";
            this.m_CullViewportRadius.SetValue = 0F;
            this.m_CullViewportRadius.Size = new System.Drawing.Size(100, 20);
            this.m_CullViewportRadius.TabIndex = 59;
            this.m_CullViewportRadius.Text = "0.000";
            this.m_CullViewportRadius.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_CullViewportRadius.Value = 0F;
            this.m_CullViewportRadius.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(223, 21);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(40, 13);
            this.label38.TabIndex = 52;
            this.label38.Text = "Radius";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(223, 47);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(45, 13);
            this.label39.TabIndex = 54;
            this.label39.Text = "X Offset";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(223, 73);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(45, 13);
            this.label40.TabIndex = 56;
            this.label40.Text = "Y Offset";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(223, 99);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(45, 13);
            this.label41.TabIndex = 58;
            this.label41.Text = "Z Offset";
            // 
            // m_CullViewportOffsetY
            // 
            this.m_CullViewportOffsetY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_CullViewportOffsetY.Decimals = 3;
            this.m_CullViewportOffsetY.Increment = 0.1F;
            this.m_CullViewportOffsetY.Location = new System.Drawing.Point(292, 70);
            this.m_CullViewportOffsetY.MaxValue = 100F;
            this.m_CullViewportOffsetY.MinValue = -100F;
            this.m_CullViewportOffsetY.Multiline = true;
            this.m_CullViewportOffsetY.Name = "m_CullViewportOffsetY";
            this.m_CullViewportOffsetY.SetValue = 0F;
            this.m_CullViewportOffsetY.Size = new System.Drawing.Size(100, 20);
            this.m_CullViewportOffsetY.TabIndex = 61;
            this.m_CullViewportOffsetY.Text = "0.000";
            this.m_CullViewportOffsetY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_CullViewportOffsetY.Value = 0F;
            this.m_CullViewportOffsetY.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_CullViewportOffsetX
            // 
            this.m_CullViewportOffsetX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_CullViewportOffsetX.Decimals = 3;
            this.m_CullViewportOffsetX.Increment = 0.1F;
            this.m_CullViewportOffsetX.Location = new System.Drawing.Point(292, 44);
            this.m_CullViewportOffsetX.MaxValue = 100F;
            this.m_CullViewportOffsetX.MinValue = -100F;
            this.m_CullViewportOffsetX.Multiline = true;
            this.m_CullViewportOffsetX.Name = "m_CullViewportOffsetX";
            this.m_CullViewportOffsetX.SetValue = 0F;
            this.m_CullViewportOffsetX.Size = new System.Drawing.Size(100, 20);
            this.m_CullViewportOffsetX.TabIndex = 60;
            this.m_CullViewportOffsetX.Text = "0.000";
            this.m_CullViewportOffsetX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_CullViewportOffsetX.Value = 0F;
            this.m_CullViewportOffsetX.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.m_CullDistanceEmit);
            this.groupBox4.Controls.Add(this.m_CullDistanceCullDist);
            this.groupBox4.Controls.Add(this.m_CullDistanceUpdate);
            this.groupBox4.Controls.Add(this.m_CullDistanceModeCombo);
            this.groupBox4.Controls.Add(this.m_CullDistanceRender);
            this.groupBox4.Controls.Add(this.label37);
            this.groupBox4.Controls.Add(this.m_CullDistanceFadeDist);
            this.groupBox4.Controls.Add(this.label43);
            this.groupBox4.Controls.Add(this.label42);
            this.groupBox4.Location = new System.Drawing.Point(3, 147);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(425, 120);
            this.groupBox4.TabIndex = 55;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Distance Culling";
            // 
            // m_CullDistanceEmit
            // 
            this.m_CullDistanceEmit.AutoSize = true;
            this.m_CullDistanceEmit.Location = new System.Drawing.Point(94, 95);
            this.m_CullDistanceEmit.Name = "m_CullDistanceEmit";
            this.m_CullDistanceEmit.Size = new System.Drawing.Size(46, 17);
            this.m_CullDistanceEmit.TabIndex = 53;
            this.m_CullDistanceEmit.Text = "Emit";
            this.m_CullDistanceEmit.UseVisualStyleBackColor = true;
            this.m_CullDistanceEmit.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_CullDistanceCullDist
            // 
            this.m_CullDistanceCullDist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_CullDistanceCullDist.Decimals = 3;
            this.m_CullDistanceCullDist.Increment = 0.1F;
            this.m_CullDistanceCullDist.Location = new System.Drawing.Point(292, 45);
            this.m_CullDistanceCullDist.MaxValue = 10000F;
            this.m_CullDistanceCullDist.MinValue = 0F;
            this.m_CullDistanceCullDist.Multiline = true;
            this.m_CullDistanceCullDist.Name = "m_CullDistanceCullDist";
            this.m_CullDistanceCullDist.SetValue = 0F;
            this.m_CullDistanceCullDist.Size = new System.Drawing.Size(100, 20);
            this.m_CullDistanceCullDist.TabIndex = 64;
            this.m_CullDistanceCullDist.Text = "0.000";
            this.m_CullDistanceCullDist.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_CullDistanceCullDist.Value = 0F;
            this.m_CullDistanceCullDist.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_CullDistanceUpdate
            // 
            this.m_CullDistanceUpdate.AutoSize = true;
            this.m_CullDistanceUpdate.Location = new System.Drawing.Point(94, 72);
            this.m_CullDistanceUpdate.Name = "m_CullDistanceUpdate";
            this.m_CullDistanceUpdate.Size = new System.Drawing.Size(61, 17);
            this.m_CullDistanceUpdate.TabIndex = 52;
            this.m_CullDistanceUpdate.Text = "Update";
            this.m_CullDistanceUpdate.UseVisualStyleBackColor = true;
            this.m_CullDistanceUpdate.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_CullDistanceModeCombo
            // 
            this.m_CullDistanceModeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_CullDistanceModeCombo.FormattingEnabled = true;
            this.m_CullDistanceModeCombo.Location = new System.Drawing.Point(67, 19);
            this.m_CullDistanceModeCombo.Name = "m_CullDistanceModeCombo";
            this.m_CullDistanceModeCombo.Size = new System.Drawing.Size(121, 21);
            this.m_CullDistanceModeCombo.TabIndex = 45;
            this.m_CullDistanceModeCombo.SelectionChangeCommitted += new System.EventHandler(this.m_ComboCullMode_SelectionChangeCommitted);
            // 
            // m_CullDistanceRender
            // 
            this.m_CullDistanceRender.AutoSize = true;
            this.m_CullDistanceRender.Location = new System.Drawing.Point(94, 49);
            this.m_CullDistanceRender.Name = "m_CullDistanceRender";
            this.m_CullDistanceRender.Size = new System.Drawing.Size(61, 17);
            this.m_CullDistanceRender.TabIndex = 51;
            this.m_CullDistanceRender.Text = "Render";
            this.m_CullDistanceRender.UseVisualStyleBackColor = true;
            this.m_CullDistanceRender.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(14, 27);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(34, 13);
            this.label37.TabIndex = 47;
            this.label37.Text = "Mode";
            // 
            // m_CullDistanceFadeDist
            // 
            this.m_CullDistanceFadeDist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_CullDistanceFadeDist.Decimals = 3;
            this.m_CullDistanceFadeDist.Increment = 0.1F;
            this.m_CullDistanceFadeDist.Location = new System.Drawing.Point(292, 19);
            this.m_CullDistanceFadeDist.MaxValue = 10000F;
            this.m_CullDistanceFadeDist.MinValue = 0F;
            this.m_CullDistanceFadeDist.Multiline = true;
            this.m_CullDistanceFadeDist.Name = "m_CullDistanceFadeDist";
            this.m_CullDistanceFadeDist.SetValue = 0F;
            this.m_CullDistanceFadeDist.Size = new System.Drawing.Size(100, 20);
            this.m_CullDistanceFadeDist.TabIndex = 63;
            this.m_CullDistanceFadeDist.Text = "0.000";
            this.m_CullDistanceFadeDist.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_CullDistanceFadeDist.Value = 0F;
            this.m_CullDistanceFadeDist.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(223, 23);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(52, 13);
            this.label43.TabIndex = 60;
            this.label43.Text = "Fade Dist";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(223, 49);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(45, 13);
            this.label42.TabIndex = 62;
            this.label42.Text = "Cull Dist";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.m_ColnOnlyBVH);
            this.groupBox6.Controls.Add(this.m_ColnUseEntity);
            this.groupBox6.Controls.Add(this.label48);
            this.groupBox6.Controls.Add(this.m_ColnProbeDist);
            this.groupBox6.Controls.Add(this.m_ColnRange);
            this.groupBox6.Controls.Add(this.label47);
            this.groupBox6.Controls.Add(this.label46);
            this.groupBox6.Controls.Add(this.m_ColnTypeCombo);
            this.groupBox6.Location = new System.Drawing.Point(3, 357);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(425, 86);
            this.groupBox6.TabIndex = 59;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Collision";
            // 
            // m_ColnOnlyBVH
            // 
            this.m_ColnOnlyBVH.AutoSize = true;
            this.m_ColnOnlyBVH.Checked = true;
            this.m_ColnOnlyBVH.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_ColnOnlyBVH.Location = new System.Drawing.Point(114, 53);
            this.m_ColnOnlyBVH.Name = "m_ColnOnlyBVH";
            this.m_ColnOnlyBVH.Size = new System.Drawing.Size(72, 17);
            this.m_ColnOnlyBVH.TabIndex = 73;
            this.m_ColnOnlyBVH.Text = "BVH Only";
            this.m_ColnOnlyBVH.UseVisualStyleBackColor = true;
            this.m_ColnOnlyBVH.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_ColnUseEntity
            // 
            this.m_ColnUseEntity.AutoSize = true;
            this.m_ColnUseEntity.Location = new System.Drawing.Point(18, 54);
            this.m_ColnUseEntity.Name = "m_ColnUseEntity";
            this.m_ColnUseEntity.Size = new System.Drawing.Size(83, 17);
            this.m_ColnUseEntity.TabIndex = 72;
            this.m_ColnUseEntity.Text = "Share Entity";
            this.m_ColnUseEntity.UseVisualStyleBackColor = true;
            this.m_ColnUseEntity.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(223, 54);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(56, 13);
            this.label48.TabIndex = 71;
            this.label48.Text = "Probe Dist";
            // 
            // m_ColnProbeDist
            // 
            this.m_ColnProbeDist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_ColnProbeDist.Decimals = 3;
            this.m_ColnProbeDist.Increment = 0.1F;
            this.m_ColnProbeDist.Location = new System.Drawing.Point(292, 47);
            this.m_ColnProbeDist.MaxValue = 25F;
            this.m_ColnProbeDist.MinValue = 0F;
            this.m_ColnProbeDist.Multiline = true;
            this.m_ColnProbeDist.Name = "m_ColnProbeDist";
            this.m_ColnProbeDist.SetValue = 0F;
            this.m_ColnProbeDist.Size = new System.Drawing.Size(100, 20);
            this.m_ColnProbeDist.TabIndex = 70;
            this.m_ColnProbeDist.Text = "0.000";
            this.m_ColnProbeDist.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_ColnProbeDist.Value = 0F;
            this.m_ColnProbeDist.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_ColnRange
            // 
            this.m_ColnRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_ColnRange.Decimals = 3;
            this.m_ColnRange.Increment = 0.1F;
            this.m_ColnRange.Location = new System.Drawing.Point(292, 21);
            this.m_ColnRange.MaxValue = 50F;
            this.m_ColnRange.MinValue = 0F;
            this.m_ColnRange.Multiline = true;
            this.m_ColnRange.Name = "m_ColnRange";
            this.m_ColnRange.SetValue = 0F;
            this.m_ColnRange.Size = new System.Drawing.Size(100, 20);
            this.m_ColnRange.TabIndex = 69;
            this.m_ColnRange.Text = "0.000";
            this.m_ColnRange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_ColnRange.Value = 0F;
            this.m_ColnRange.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(223, 28);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(39, 13);
            this.label47.TabIndex = 60;
            this.label47.Text = "Range";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(15, 28);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(31, 13);
            this.label46.TabIndex = 59;
            this.label46.Text = "Type";
            // 
            // m_ColnTypeCombo
            // 
            this.m_ColnTypeCombo.FormattingEnabled = true;
            this.m_ColnTypeCombo.Location = new System.Drawing.Point(67, 20);
            this.m_ColnTypeCombo.Name = "m_ColnTypeCombo";
            this.m_ColnTypeCombo.Size = new System.Drawing.Size(121, 21);
            this.m_ColnTypeCombo.TabIndex = 58;
            this.m_ColnTypeCombo.SelectionChangeCommitted += new System.EventHandler(this.m_ComboDrawList_SelectedIndexChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.m_LODCreate);
            this.groupBox5.Controls.Add(this.m_LODFarDistance);
            this.groupBox5.Controls.Add(this.m_LODNearDistance);
            this.groupBox5.Controls.Add(this.label45);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Location = new System.Drawing.Point(3, 273);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(425, 78);
            this.groupBox5.TabIndex = 56;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "LOD";
            // 
            // m_LODCreate
            // 
            this.m_LODCreate.Location = new System.Drawing.Point(80, 19);
            this.m_LODCreate.Name = "m_LODCreate";
            this.m_LODCreate.Size = new System.Drawing.Size(75, 23);
            this.m_LODCreate.TabIndex = 69;
            this.m_LODCreate.Text = "Create LOD";
            this.m_LODCreate.UseVisualStyleBackColor = true;
            this.m_LODCreate.Click += new System.EventHandler(this.m_CreateLodsBtn_Click);
            // 
            // m_LODFarDistance
            // 
            this.m_LODFarDistance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_LODFarDistance.Decimals = 3;
            this.m_LODFarDistance.Increment = 0.1F;
            this.m_LODFarDistance.Location = new System.Drawing.Point(292, 45);
            this.m_LODFarDistance.MaxValue = 10000F;
            this.m_LODFarDistance.MinValue = 0F;
            this.m_LODFarDistance.Multiline = true;
            this.m_LODFarDistance.Name = "m_LODFarDistance";
            this.m_LODFarDistance.SetValue = 0F;
            this.m_LODFarDistance.Size = new System.Drawing.Size(100, 20);
            this.m_LODFarDistance.TabIndex = 68;
            this.m_LODFarDistance.Text = "0.000";
            this.m_LODFarDistance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_LODFarDistance.Value = 0F;
            this.m_LODFarDistance.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_LODNearDistance
            // 
            this.m_LODNearDistance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_LODNearDistance.Decimals = 3;
            this.m_LODNearDistance.Increment = 0.1F;
            this.m_LODNearDistance.Location = new System.Drawing.Point(292, 19);
            this.m_LODNearDistance.MaxValue = 10000F;
            this.m_LODNearDistance.MinValue = 0F;
            this.m_LODNearDistance.Multiline = true;
            this.m_LODNearDistance.Name = "m_LODNearDistance";
            this.m_LODNearDistance.SetValue = 0F;
            this.m_LODNearDistance.Size = new System.Drawing.Size(100, 20);
            this.m_LODNearDistance.TabIndex = 67;
            this.m_LODNearDistance.Text = "0.000";
            this.m_LODNearDistance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_LODNearDistance.Value = 0F;
            this.m_LODNearDistance.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(223, 23);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(67, 13);
            this.label45.TabIndex = 65;
            this.label45.Text = "Evo Dist Min";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(223, 49);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(70, 13);
            this.label44.TabIndex = 66;
            this.label44.Text = "Evo Dist Max";
            // 
            // ptxSubSection1
            // 
            this.ptxSubSection1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptxSubSection1.Controls.Add(this.m_RndOffsetPosZ);
            this.ptxSubSection1.Controls.Add(this.m_RndOffsetPosY);
            this.ptxSubSection1.Controls.Add(this.m_RndOffsetPosX);
            this.ptxSubSection1.Controls.Add(this.label31);
            this.ptxSubSection1.Controls.Add(this.label32);
            this.ptxSubSection1.Controls.Add(this.label33);
            this.ptxSubSection1.Controls.Add(this.label34);
            this.ptxSubSection1.Controls.Add(this.m_CheckSortEvents);
            this.ptxSubSection1.Controls.Add(this.m_DurationMin);
            this.ptxSubSection1.Controls.Add(this.m_ComboDrawList);
            this.ptxSubSection1.Controls.Add(this.m_DurationMax);
            this.ptxSubSection1.Controls.Add(this.label26);
            this.ptxSubSection1.Controls.Add(this.m_PreUpdate);
            this.ptxSubSection1.Controls.Add(this.m_PreUpdateInterval);
            this.ptxSubSection1.Controls.Add(this.m_NumLoops);
            this.ptxSubSection1.Controls.Add(this.m_PlayBackMax);
            this.ptxSubSection1.Controls.Add(this.m_PlayBackMin);
            this.ptxSubSection1.Controls.Add(this.label2);
            this.ptxSubSection1.Controls.Add(this.label9);
            this.ptxSubSection1.Controls.Add(this.label7);
            this.ptxSubSection1.Controls.Add(this.label10);
            this.ptxSubSection1.Controls.Add(this.label8);
            this.ptxSubSection1.Controls.Add(this.label3);
            this.ptxSubSection1.Controls.Add(this.label4);
            this.ptxSubSection1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ptxSubSection1.Expanded = true;
            this.ptxSubSection1.HeaderText = "";
            this.ptxSubSection1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ptxSubSection1.Location = new System.Drawing.Point(0, 139);
            this.ptxSubSection1.Name = "ptxSubSection1";
            this.ptxSubSection1.SectionName = "Effect Setup";
            this.ptxSubSection1.Size = new System.Drawing.Size(433, 201);
            this.ptxSubSection1.TabIndex = 1;
            this.ptxSubSection1.Paint += new System.Windows.Forms.PaintEventHandler(this.ptxSubSection1_Paint);
            // 
            // m_RndOffsetPosZ
            // 
            this.m_RndOffsetPosZ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_RndOffsetPosZ.Decimals = 3;
            this.m_RndOffsetPosZ.Increment = 0.1F;
            this.m_RndOffsetPosZ.Location = new System.Drawing.Point(365, 165);
            this.m_RndOffsetPosZ.MaxValue = 10000F;
            this.m_RndOffsetPosZ.MinValue = -10000F;
            this.m_RndOffsetPosZ.Multiline = true;
            this.m_RndOffsetPosZ.Name = "m_RndOffsetPosZ";
            this.m_RndOffsetPosZ.SetValue = 0F;
            this.m_RndOffsetPosZ.Size = new System.Drawing.Size(59, 20);
            this.m_RndOffsetPosZ.TabIndex = 50;
            this.m_RndOffsetPosZ.Text = "0.000";
            this.m_RndOffsetPosZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_RndOffsetPosZ.Value = 0F;
            this.m_RndOffsetPosZ.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_RndOffsetPosY
            // 
            this.m_RndOffsetPosY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_RndOffsetPosY.Decimals = 3;
            this.m_RndOffsetPosY.Increment = 0.1F;
            this.m_RndOffsetPosY.Location = new System.Drawing.Point(253, 165);
            this.m_RndOffsetPosY.MaxValue = 10000F;
            this.m_RndOffsetPosY.MinValue = -10000F;
            this.m_RndOffsetPosY.Multiline = true;
            this.m_RndOffsetPosY.Name = "m_RndOffsetPosY";
            this.m_RndOffsetPosY.SetValue = 0F;
            this.m_RndOffsetPosY.Size = new System.Drawing.Size(59, 20);
            this.m_RndOffsetPosY.TabIndex = 49;
            this.m_RndOffsetPosY.Text = "0.000";
            this.m_RndOffsetPosY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_RndOffsetPosY.Value = 0F;
            this.m_RndOffsetPosY.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_RndOffsetPosX
            // 
            this.m_RndOffsetPosX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_RndOffsetPosX.Decimals = 3;
            this.m_RndOffsetPosX.Increment = 0.1F;
            this.m_RndOffsetPosX.Location = new System.Drawing.Point(145, 165);
            this.m_RndOffsetPosX.MaxValue = 10000F;
            this.m_RndOffsetPosX.MinValue = -10000F;
            this.m_RndOffsetPosX.Multiline = true;
            this.m_RndOffsetPosX.Name = "m_RndOffsetPosX";
            this.m_RndOffsetPosX.SetValue = 0F;
            this.m_RndOffsetPosX.Size = new System.Drawing.Size(59, 20);
            this.m_RndOffsetPosX.TabIndex = 48;
            this.m_RndOffsetPosX.Text = "0.000";
            this.m_RndOffsetPosX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_RndOffsetPosX.Value = 0F;
            this.m_RndOffsetPosX.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(345, 167);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(17, 13);
            this.label31.TabIndex = 47;
            this.label31.Text = "Z:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(233, 167);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(17, 13);
            this.label32.TabIndex = 46;
            this.label32.Text = "Y:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(125, 167);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(17, 13);
            this.label33.TabIndex = 45;
            this.label33.Text = "X:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 167);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(118, 13);
            this.label34.TabIndex = 44;
            this.label34.Text = "Random Offset Position";
            // 
            // m_CheckSortEvents
            // 
            this.m_CheckSortEvents.AutoSize = true;
            this.m_CheckSortEvents.ForeColor = System.Drawing.Color.Black;
            this.m_CheckSortEvents.Location = new System.Drawing.Point(3, 128);
            this.m_CheckSortEvents.Name = "m_CheckSortEvents";
            this.m_CheckSortEvents.Size = new System.Drawing.Size(141, 17);
            this.m_CheckSortEvents.TabIndex = 40;
            this.m_CheckSortEvents.Text = "Sort Events By Distance";
            this.m_CheckSortEvents.UseVisualStyleBackColor = true;
            this.m_CheckSortEvents.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_DurationMin
            // 
            this.m_DurationMin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_DurationMin.Decimals = 3;
            this.m_DurationMin.Increment = 0.001F;
            this.m_DurationMin.Location = new System.Drawing.Point(145, 25);
            this.m_DurationMin.MaxValue = 10000F;
            this.m_DurationMin.MinValue = 0F;
            this.m_DurationMin.Multiline = true;
            this.m_DurationMin.Name = "m_DurationMin";
            this.m_DurationMin.SetValue = 0F;
            this.m_DurationMin.Size = new System.Drawing.Size(59, 20);
            this.m_DurationMin.TabIndex = 18;
            this.m_DurationMin.Text = "0.000";
            this.m_DurationMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_DurationMin.Value = 0F;
            this.m_DurationMin.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_ComboDrawList
            // 
            this.m_ComboDrawList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboDrawList.FormattingEnabled = true;
            this.m_ComboDrawList.Items.AddRange(new object[] {
            "Default"});
            this.m_ComboDrawList.Location = new System.Drawing.Point(283, 124);
            this.m_ComboDrawList.Name = "m_ComboDrawList";
            this.m_ComboDrawList.Size = new System.Drawing.Size(141, 21);
            this.m_ComboDrawList.TabIndex = 41;
            this.m_ComboDrawList.SelectionChangeCommitted += new System.EventHandler(this.m_ComboDrawList_SelectedIndexChanged);
            // 
            // m_DurationMax
            // 
            this.m_DurationMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_DurationMax.Decimals = 3;
            this.m_DurationMax.Increment = 0.001F;
            this.m_DurationMax.Location = new System.Drawing.Point(365, 25);
            this.m_DurationMax.MaxValue = 10000F;
            this.m_DurationMax.MinValue = 0F;
            this.m_DurationMax.Multiline = true;
            this.m_DurationMax.Name = "m_DurationMax";
            this.m_DurationMax.SetValue = 0F;
            this.m_DurationMax.Size = new System.Drawing.Size(59, 20);
            this.m_DurationMax.TabIndex = 19;
            this.m_DurationMax.Text = "0.000";
            this.m_DurationMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_DurationMax.Value = 0F;
            this.m_DurationMax.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(225, 127);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(51, 13);
            this.label26.TabIndex = 42;
            this.label26.Text = "Draw List";
            // 
            // m_PreUpdate
            // 
            this.m_PreUpdate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_PreUpdate.Decimals = 3;
            this.m_PreUpdate.Increment = 0.001F;
            this.m_PreUpdate.Location = new System.Drawing.Point(365, 73);
            this.m_PreUpdate.MaxValue = 10000F;
            this.m_PreUpdate.MinValue = 0F;
            this.m_PreUpdate.Multiline = true;
            this.m_PreUpdate.Name = "m_PreUpdate";
            this.m_PreUpdate.SetValue = 0F;
            this.m_PreUpdate.Size = new System.Drawing.Size(59, 20);
            this.m_PreUpdate.TabIndex = 23;
            this.m_PreUpdate.Text = "0.000";
            this.m_PreUpdate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_PreUpdate.Value = 0F;
            this.m_PreUpdate.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_PreUpdateInterval
            // 
            this.m_PreUpdateInterval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_PreUpdateInterval.Decimals = 3;
            this.m_PreUpdateInterval.Increment = 0.001F;
            this.m_PreUpdateInterval.Location = new System.Drawing.Point(365, 98);
            this.m_PreUpdateInterval.MaxValue = 10000F;
            this.m_PreUpdateInterval.MinValue = 0F;
            this.m_PreUpdateInterval.Multiline = true;
            this.m_PreUpdateInterval.Name = "m_PreUpdateInterval";
            this.m_PreUpdateInterval.SetValue = 0F;
            this.m_PreUpdateInterval.Size = new System.Drawing.Size(59, 20);
            this.m_PreUpdateInterval.TabIndex = 24;
            this.m_PreUpdateInterval.Text = "0.000";
            this.m_PreUpdateInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_PreUpdateInterval.Value = 0F;
            this.m_PreUpdateInterval.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_NumLoops
            // 
            this.m_NumLoops.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_NumLoops.Decimals = 0;
            this.m_NumLoops.Increment = 1F;
            this.m_NumLoops.Location = new System.Drawing.Point(145, 73);
            this.m_NumLoops.MaxValue = 1000F;
            this.m_NumLoops.MinValue = -1F;
            this.m_NumLoops.Multiline = true;
            this.m_NumLoops.Name = "m_NumLoops";
            this.m_NumLoops.SetValue = 0F;
            this.m_NumLoops.Size = new System.Drawing.Size(59, 20);
            this.m_NumLoops.TabIndex = 22;
            this.m_NumLoops.Text = "0";
            this.m_NumLoops.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_NumLoops.Value = 0F;
            this.m_NumLoops.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_NumLoops_OnValueChanged);
            // 
            // m_PlayBackMax
            // 
            this.m_PlayBackMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_PlayBackMax.Decimals = 3;
            this.m_PlayBackMax.Increment = 0.001F;
            this.m_PlayBackMax.Location = new System.Drawing.Point(365, 49);
            this.m_PlayBackMax.MaxValue = 10000F;
            this.m_PlayBackMax.MinValue = 0F;
            this.m_PlayBackMax.Multiline = true;
            this.m_PlayBackMax.Name = "m_PlayBackMax";
            this.m_PlayBackMax.SetValue = 0F;
            this.m_PlayBackMax.Size = new System.Drawing.Size(59, 20);
            this.m_PlayBackMax.TabIndex = 21;
            this.m_PlayBackMax.Text = "0.000";
            this.m_PlayBackMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_PlayBackMax.Value = 0F;
            this.m_PlayBackMax.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_PlayBackMin
            // 
            this.m_PlayBackMin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_PlayBackMin.Decimals = 3;
            this.m_PlayBackMin.Increment = 0.001F;
            this.m_PlayBackMin.Location = new System.Drawing.Point(145, 49);
            this.m_PlayBackMin.MaxValue = 10000F;
            this.m_PlayBackMin.MinValue = 0F;
            this.m_PlayBackMin.Multiline = true;
            this.m_PlayBackMin.Name = "m_PlayBackMin";
            this.m_PlayBackMin.SetValue = 0F;
            this.m_PlayBackMin.Size = new System.Drawing.Size(59, 20);
            this.m_PlayBackMin.TabIndex = 20;
            this.m_PlayBackMin.Text = "0.000";
            this.m_PlayBackMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_PlayBackMin.Value = 0F;
            this.m_PlayBackMin.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Duration Min";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(225, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Duration Max";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(225, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Pre Update Time";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(225, 100);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(125, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Pre Update Time Interval";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(225, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(133, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Playback Rate Scalar Max";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Playback Rate Scalar Min";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Number of Loops";
            // 
            // m_SubEvolutions
            // 
            this.m_SubEvolutions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SubEvolutions.Controls.Add(this.m_DataGridEvoList);
            this.m_SubEvolutions.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_SubEvolutions.Expanded = true;
            this.m_SubEvolutions.HeaderText = "";
            this.m_SubEvolutions.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.m_SubEvolutions.Location = new System.Drawing.Point(0, 0);
            this.m_SubEvolutions.Name = "m_SubEvolutions";
            this.m_SubEvolutions.SectionName = "Evolutions";
            this.m_SubEvolutions.Size = new System.Drawing.Size(433, 139);
            this.m_SubEvolutions.TabIndex = 0;
            // 
            // m_DataGridEvoList
            // 
            this.m_DataGridEvoList.AllowUserToAddRows = false;
            this.m_DataGridEvoList.AllowUserToDeleteRows = false;
            this.m_DataGridEvoList.AllowUserToResizeColumns = false;
            this.m_DataGridEvoList.AllowUserToResizeRows = false;
            this.m_DataGridEvoList.BackgroundColor = System.Drawing.SystemColors.Window;
            this.m_DataGridEvoList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.m_DataGridEvoList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.m_DataGridEvoList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridEvoList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.m_DataGridEvoList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Index,
            this.dataGridViewTextBoxColumn1});
            this.m_DataGridEvoList.ContextMenuStrip = this.m_ContextMenuEvoList;
            this.m_DataGridEvoList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.m_DataGridEvoList.GridColor = System.Drawing.SystemColors.Window;
            this.m_DataGridEvoList.Location = new System.Drawing.Point(6, 24);
            this.m_DataGridEvoList.MultiSelect = false;
            this.m_DataGridEvoList.Name = "m_DataGridEvoList";
            this.m_DataGridEvoList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridEvoList.RowHeadersVisible = false;
            this.m_DataGridEvoList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.m_DataGridEvoList.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_DataGridEvoList.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Red;
            this.m_DataGridEvoList.RowTemplate.Height = 16;
            this.m_DataGridEvoList.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridEvoList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_DataGridEvoList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.m_DataGridEvoList.ShowCellToolTips = false;
            this.m_DataGridEvoList.Size = new System.Drawing.Size(422, 106);
            this.m_DataGridEvoList.TabIndex = 23;
            this.m_DataGridEvoList.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_DataGridEvoList_CellEndEdit);
            this.m_DataGridEvoList.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SelectOnRightMouseButton_CellMouseDown);
            // 
            // Index
            // 
            this.Index.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Index.HeaderText = "Index";
            this.Index.Name = "Index";
            this.Index.ReadOnly = true;
            this.Index.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Index.Width = 58;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Property";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // m_ContextMenuEvoList
            // 
            this.m_ContextMenuEvoList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.m_MenuItemNewEvo,
            this.m_MenuItemLoadEvo,
            this.toolStripSeparator1,
            this.m_MenuItemSelectedEvo,
            this.m_MenuItemDelEvo,
            this.m_MenuItemRenameEvo,
            this.m_MenuItemCloneEvo,
            this.m_MenuItemSaveEvo});
            this.m_ContextMenuEvoList.Name = "m_ContextMenuEvoList";
            this.m_ContextMenuEvoList.ShowImageMargin = false;
            this.m_ContextMenuEvoList.Size = new System.Drawing.Size(100, 186);
            this.m_ContextMenuEvoList.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContextMenuEvoList_Opening);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Enabled = false;
            this.toolStripMenuItem1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(99, 22);
            this.toolStripMenuItem1.Text = "Evolutions";
            this.toolStripMenuItem1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripMenuItem1.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // m_MenuItemNewEvo
            // 
            this.m_MenuItemNewEvo.Name = "m_MenuItemNewEvo";
            this.m_MenuItemNewEvo.Size = new System.Drawing.Size(99, 22);
            this.m_MenuItemNewEvo.Text = "New";
            this.m_MenuItemNewEvo.Click += new System.EventHandler(this.m_MenuItemNewEvo_Click);
            // 
            // m_MenuItemLoadEvo
            // 
            this.m_MenuItemLoadEvo.Name = "m_MenuItemLoadEvo";
            this.m_MenuItemLoadEvo.Size = new System.Drawing.Size(99, 22);
            this.m_MenuItemLoadEvo.Text = "Load...";
            this.m_MenuItemLoadEvo.Click += new System.EventHandler(this.m_MenuItemLoadEvo_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(96, 6);
            // 
            // m_MenuItemSelectedEvo
            // 
            this.m_MenuItemSelectedEvo.Enabled = false;
            this.m_MenuItemSelectedEvo.Name = "m_MenuItemSelectedEvo";
            this.m_MenuItemSelectedEvo.Size = new System.Drawing.Size(99, 22);
            this.m_MenuItemSelectedEvo.Text = "Selected: ";
            // 
            // m_MenuItemDelEvo
            // 
            this.m_MenuItemDelEvo.Name = "m_MenuItemDelEvo";
            this.m_MenuItemDelEvo.Size = new System.Drawing.Size(99, 22);
            this.m_MenuItemDelEvo.Text = "Remove";
            this.m_MenuItemDelEvo.Click += new System.EventHandler(this.m_MenuItemDelEvo_Click);
            // 
            // m_MenuItemRenameEvo
            // 
            this.m_MenuItemRenameEvo.Name = "m_MenuItemRenameEvo";
            this.m_MenuItemRenameEvo.Size = new System.Drawing.Size(99, 22);
            this.m_MenuItemRenameEvo.Text = "Rename";
            // 
            // m_MenuItemCloneEvo
            // 
            this.m_MenuItemCloneEvo.Enabled = false;
            this.m_MenuItemCloneEvo.Name = "m_MenuItemCloneEvo";
            this.m_MenuItemCloneEvo.Size = new System.Drawing.Size(99, 22);
            this.m_MenuItemCloneEvo.Text = "Clone";
            // 
            // m_MenuItemSaveEvo
            // 
            this.m_MenuItemSaveEvo.Name = "m_MenuItemSaveEvo";
            this.m_MenuItemSaveEvo.Size = new System.Drawing.Size(99, 22);
            this.m_MenuItemSaveEvo.Text = "Save...";
            this.m_MenuItemSaveEvo.Click += new System.EventHandler(this.m_MenuItemSaveEvo_Click);
            // 
            // ptxSlider1
            // 
            this.ptxSlider1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptxSlider1.Decimals = 1;
            this.ptxSlider1.Increment = 0.1F;
            this.ptxSlider1.Location = new System.Drawing.Point(0, 0);
            this.ptxSlider1.MaxValue = 100F;
            this.ptxSlider1.MinValue = 0F;
            this.ptxSlider1.Multiline = true;
            this.ptxSlider1.Name = "ptxSlider1";
            this.ptxSlider1.SetValue = 0F;
            this.ptxSlider1.Size = new System.Drawing.Size(113, 20);
            this.ptxSlider1.TabIndex = 0;
            this.ptxSlider1.Text = "0.0";
            this.ptxSlider1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ptxSlider1.Value = 0F;
            // 
            // EffectWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.m_PanelGroup);
            this.Controls.Add(this.m_PanelHeader);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "EffectWindow";
            this.Size = new System.Drawing.Size(463, 738);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.m_PanelHeader.ResumeLayout(false);
            this.m_PanelHeader.PerformLayout();
            this.m_PanelGroup.ResumeLayout(false);
            this.m_PanelGroup.PerformLayout();
            this.m_PanelSection.ResumeLayout(false);
            this.ptxSubSection3.ResumeLayout(false);
            this.ptxSubSection3.PerformLayout();
            this.m_SubSectionGameSpecific.ResumeLayout(false);
            this.m_SubSectionGameSpecific.PerformLayout();
            this.m_SubSectionEffectOverride.ResumeLayout(false);
            this.ptxSubSection5.ResumeLayout(false);
            this.ptxSubSection5.PerformLayout();
            this.ptxSubSection4.ResumeLayout(false);
            this.ptxSubSection4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridEffectKeyframeList)).EndInit();
            this.m_ContextMenuKeyProp.ResumeLayout(false);
            this.m_SubSectionTimeline.ResumeLayout(false);
            this.m_SubSectionTimeline.PerformLayout();
            this.ptxSubSection2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ptxSubSection1.ResumeLayout(false);
            this.ptxSubSection1.PerformLayout();
            this.m_SubEvolutions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridEvoList)).EndInit();
            this.m_ContextMenuEvoList.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label m_LabelHeader;
        private System.Windows.Forms.Panel m_PanelHeader;
        private ptxSlider ptxSlider1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Panel m_PanelGroup;
        private System.Windows.Forms.Panel m_PanelSection;
        private ptxSubSection ptxSubSection5;
        private System.Windows.Forms.Button m_ButtonFreezeScale;
        private ptxSlider m_Scale;
        private System.Windows.Forms.Label label21;
        private ptxSubSection ptxSubSection4;
        private ptxSubSection m_SubSectionTimeline;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox m_ComboEmitRule;
        private ptxSlider m_EvtPlaybackMin;
        private ptxSlider m_EvtPlaybackMax;
		private ptxSlider m_EvtStartRatio;
		private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label17;
		private ptxSubSection ptxSubSection2;
        private ptxSubSection ptxSubSection1;
        private ptxSlider m_DurationMin;
        private ptxSlider m_PreUpdate;
        private ptxSlider m_PreUpdateInterval;
        private ptxSlider m_NumLoops;
        private ptxSlider m_PlayBackMax;
        private ptxSlider m_PlayBackMin;
		private ptxSlider m_DurationMax;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox m_ComboPtxRule;
		private System.Windows.Forms.DataGridView m_DataGridEffectKeyframeList;
        private System.Windows.Forms.ContextMenuStrip m_ContextMenuEvoList;
        private System.Windows.Forms.ToolStripMenuItem m_MenuItemNewEvo;
        private System.Windows.Forms.ToolStripMenuItem m_MenuItemDelEvo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem m_MenuItemRenameEvo;
        private System.Windows.Forms.ToolStripMenuItem m_MenuItemCloneEvo;
		private System.Windows.Forms.ToolStripMenuItem m_MenuItemSelectedEvo;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip m_ContextMenuKeyProp;
        private System.Windows.Forms.ToolStripMenuItem m_MenuEvoAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem m_MenuEvolutions;
        private System.Windows.Forms.ToolStripMenuItem m_MenuEvoRemoveAll;
        private System.Windows.Forms.ToolStripMenuItem m_MenuSelectedProp;
        private System.Windows.Forms.ToolStripMenuItem m_MenuEditKeyframe;
        private RageUserControls.ButtonEX m_ButtonEXFilterSprites;
        private RageUserControls.ButtonEX m_ButtonEXFilterModels;
        private ptxSubSection m_SubSectionEffectOverride;
        private System.Windows.Forms.CheckBox m_CheckSortEvents;
		private EffectOverrides m_EffectOverrides;
        private System.Windows.Forms.ToolStripMenuItem m_MenuItemLoadEvo;
		private System.Windows.Forms.ToolStripMenuItem m_MenuItemSaveEvo;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox m_ComboDrawList;
        private System.Windows.Forms.CheckBox m_CheckUseRandomColorTint;
        private ptxSlider m_EvtZoomMin;
        private ptxSlider m_EvtZoomMax;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private RageUserControls.ButtonEX m_ColorMinButton;
        private RageUserControls.ButtonEX m_ColorMaxButton;
        private System.Windows.Forms.CheckBox m_CheckUseDataSphere;
        private ptxSlider m_RndOffsetPosZ;
        private ptxSlider m_RndOffsetPosY;
        private ptxSlider m_RndOffsetPosX;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Label label35;
        private ptxSubSection m_SubSectionGameSpecific;
        private System.Windows.Forms.Panel m_PanelGameData;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_ColumnTextRuleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution5;
        private System.Windows.Forms.DataGridViewTextBoxColumn EvolutionLOD;
        private System.Windows.Forms.DataGridViewComboBoxColumn EvoBlend;
		private System.Windows.Forms.DataGridViewTextBoxColumn Invis;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox m_CullDistanceModeCombo;
        private System.Windows.Forms.ComboBox m_CullViewportModeCombo;
        private System.Windows.Forms.CheckBox m_CullViewportRender;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox m_CullViewportEmit;
        private System.Windows.Forms.CheckBox m_CullViewportUpdate;
        private System.Windows.Forms.CheckBox m_CullDistanceEmit;
        private System.Windows.Forms.CheckBox m_CullDistanceUpdate;
        private System.Windows.Forms.CheckBox m_CullDistanceRender;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private ptxSlider m_CullViewportOffsetZ;
        private ptxSlider m_CullViewportOffsetY;
        private ptxSlider m_CullViewportOffsetX;
        private ptxSlider m_CullViewportRadius;
        private ptxSlider m_CullDistanceCullDist;
        private ptxSlider m_CullDistanceFadeDist;
        private System.Windows.Forms.GroupBox groupBox5;
        private ptxSlider m_LODFarDistance;
        private ptxSlider m_LODNearDistance;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button m_LODCreate;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox m_ColnTypeCombo;
        private ptxSlider m_ColnRange;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label48;
        private ptxSlider m_ColnProbeDist;
        private System.Windows.Forms.CheckBox m_ColnUseEntity;
		private System.Windows.Forms.CheckBox m_ColnOnlyBVH;
		private ptxSubSection ptxSubSection3;
		private System.Windows.Forms.CheckBox m_CheckShowDataSpheres;
		private System.Windows.Forms.CheckBox m_ShowPhysics;
		private System.Windows.Forms.CheckBox m_CheckShowCullSp;
		private System.Windows.Forms.CheckBox m_CheckShowInst;
		private System.Windows.Forms.Panel panel1;
		private ptxSubSection m_SubEvolutions;
		private System.Windows.Forms.DataGridView m_DataGridEvoList;
		private System.Windows.Forms.DataGridViewTextBoxColumn Index;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
		private System.Windows.Forms.Label label5;
		private ptxSlider m_EvtEndRatio;
		private System.Windows.Forms.ComboBox m_ComboDataVolumeType;
        private System.Windows.Forms.CheckBox m_CheckShowPointQuads;


    }
}
