namespace ParticleEditor
{
    partial class PtxWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_PanelGroup = new System.Windows.Forms.Panel();
            this.m_PanelSection = new System.Windows.Forms.Panel();
            this.m_SubSectionSpawnBOverrides = new ParticleEditor.ptxSubSection();
            this.m_SpawnBOverrides = new ParticleEditor.EffectOverrides();
            this.m_SubSectionSpawnAOverrides = new ParticleEditor.ptxSubSection();
            this.m_SpawnAOverrides = new ParticleEditor.EffectOverrides();
            this.m_SubSectionSprite = new ParticleEditor.ptxSubSection();
            this.label11 = new System.Windows.Forms.Label();
            this.m_comboProjMode = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.m_checkBoxIsNormalSpec = new System.Windows.Forms.CheckBox();
            this.m_comboDiffuseMode = new System.Windows.Forms.ComboBox();
            this.m_ComboShader = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_checkBoxIsRefract = new System.Windows.Forms.CheckBox();
            this.m_checkBoxIsScreenSpace = new System.Windows.Forms.CheckBox();
            this.m_checkBoxIsSoft = new System.Windows.Forms.CheckBox();
            this.m_checkBoxIsLit = new System.Windows.Forms.CheckBox();
            this.m_SubSectionModel = new ParticleEditor.ptxSubSection();
            this.m_ListModels = new System.Windows.Forms.ListBox();
            this.m_ContextModelList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_AddModelMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_DelModelMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ptxSubSectionBiasLinks = new ParticleEditor.ptxSubSection();
            this.m_DataGridLinkSet = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_ContexBiasSet = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_ContexAddSet = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ContexRemoveSet = new System.Windows.Forms.ToolStripMenuItem();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.m_DataGridBiasProps = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.m_ContextBiasProp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_ContextAddBiasProp = new System.Windows.Forms.ToolStripMenuItem();
            this.blankToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ContextRemoveBiasProp = new System.Windows.Forms.ToolStripMenuItem();
            this.label15 = new System.Windows.Forms.Label();
            this.m_SubSectionShader = new ParticleEditor.ptxSubSection();
            this.m_ShaderPanel = new System.Windows.Forms.Panel();
            this.m_SubSectionPhysical = new ParticleEditor.ptxSubSection();
            this.label24 = new System.Windows.Forms.Label();
            this.m_SlidePerKill = new ParticleEditor.ptxSlider();
            this.label21 = new System.Windows.Forms.Label();
            this.m_SlidePerPhysical = new ParticleEditor.ptxSlider();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.m_CheckShowPhysics = new System.Windows.Forms.CheckBox();
            this.m_StopVel = new ParticleEditor.ptxSlider();
            this.m_PhysicalRange = new ParticleEditor.ptxSlider();
            this.m_CheckOnCollideKill = new System.Windows.Forms.CheckBox();
            this.m_SubSectionKeyframes = new ParticleEditor.ptxSubSection();
            this.m_DataGridPtxtKeyframeList = new System.Windows.Forms.DataGridView();
            this.m_ColumnTextRuleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvolutionLOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvoBlend = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Invis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Profile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_ContextMenuKeyProp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_SubSectionSetup = new ParticleEditor.ptxSubSection();
            this.label9 = new System.Windows.Forms.Label();
            this.m_TrackNegDirACheck = new System.Windows.Forms.CheckBox();
            this.m_TrackNegDirBCheck = new System.Windows.Forms.CheckBox();
            this.m_TrackPosBCheck = new System.Windows.Forms.CheckBox();
            this.m_TrackDirBCheck = new System.Windows.Forms.CheckBox();
            this.m_TrackDirACheck = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.m_TrackPosACheck = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.m_TexFrameIdMin = new ParticleEditor.ptxSlider();
            this.label5 = new System.Windows.Forms.Label();
            this.m_LabelDrawType = new System.Windows.Forms.Label();
            this.m_TexFrameIdMax = new ParticleEditor.ptxSlider();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.m_AllowColorOverride = new System.Windows.Forms.CheckBox();
            this.m_AllowPosOverride = new System.Windows.Forms.CheckBox();
            this.m_ComboCullMode = new System.Windows.Forms.ComboBox();
            this.m_ComboSortType = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.m_InheritLifeBCheck = new System.Windows.Forms.CheckBox();
            this.m_SpawnEffectA = new System.Windows.Forms.ComboBox();
            this.m_SpawnEffectBTime = new ParticleEditor.ptxSlider();
            this.m_ComboDrawType = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.m_SpawnEffectB = new System.Windows.Forms.ComboBox();
            this.m_SpawnEffectATime = new ParticleEditor.ptxSlider();
            this.m_CheckDepthTest = new System.Windows.Forms.CheckBox();
            this.m_SpawnEffectBCheck = new System.Windows.Forms.CheckBox();
            this.m_ComboBlendMode = new System.Windows.Forms.ComboBox();
            this.m_InheritLifeACheck = new System.Windows.Forms.CheckBox();
            this.m_SpawnEffectACheck = new System.Windows.Forms.CheckBox();
            this.m_CheckDepthWrite = new System.Windows.Forms.CheckBox();
            this.m_PanelHeader = new System.Windows.Forms.Panel();
            this.m_LabelHeader = new System.Windows.Forms.Label();
            this.m_ContextBehavior = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_MenuClearBehavior = new System.Windows.Forms.ToolStripMenuItem();
            this.m_TimerProfile = new System.Windows.Forms.Timer(this.components);
            this.m_PanelGroup.SuspendLayout();
            this.m_PanelSection.SuspendLayout();
            this.m_SubSectionSpawnBOverrides.SuspendLayout();
            this.m_SubSectionSpawnAOverrides.SuspendLayout();
            this.m_SubSectionSprite.SuspendLayout();
            this.m_SubSectionModel.SuspendLayout();
            this.m_ContextModelList.SuspendLayout();
            this.ptxSubSectionBiasLinks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridLinkSet)).BeginInit();
            this.m_ContexBiasSet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridBiasProps)).BeginInit();
            this.m_ContextBiasProp.SuspendLayout();
            this.m_SubSectionShader.SuspendLayout();
            this.m_SubSectionPhysical.SuspendLayout();
            this.m_SubSectionKeyframes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridPtxtKeyframeList)).BeginInit();
            this.m_SubSectionSetup.SuspendLayout();
            this.m_PanelHeader.SuspendLayout();
            this.m_ContextBehavior.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_PanelGroup
            // 
            this.m_PanelGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_PanelGroup.AutoScroll = true;
            this.m_PanelGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_PanelGroup.Controls.Add(this.m_PanelSection);
            this.m_PanelGroup.Location = new System.Drawing.Point(11, 28);
            this.m_PanelGroup.Margin = new System.Windows.Forms.Padding(0);
            this.m_PanelGroup.Name = "m_PanelGroup";
            this.m_PanelGroup.Size = new System.Drawing.Size(462, 433);
            this.m_PanelGroup.TabIndex = 4;
            // 
            // m_PanelSection
            // 
            this.m_PanelSection.AutoSize = true;
            this.m_PanelSection.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_PanelSection.Controls.Add(this.m_SubSectionSpawnBOverrides);
            this.m_PanelSection.Controls.Add(this.m_SubSectionSpawnAOverrides);
            this.m_PanelSection.Controls.Add(this.m_SubSectionSprite);
            this.m_PanelSection.Controls.Add(this.m_SubSectionModel);
            this.m_PanelSection.Controls.Add(this.ptxSubSectionBiasLinks);
            this.m_PanelSection.Controls.Add(this.m_SubSectionShader);
            this.m_PanelSection.Controls.Add(this.m_SubSectionPhysical);
            this.m_PanelSection.Controls.Add(this.m_SubSectionKeyframes);
            this.m_PanelSection.Controls.Add(this.m_SubSectionSetup);
            this.m_PanelSection.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_PanelSection.Location = new System.Drawing.Point(0, 0);
            this.m_PanelSection.MaximumSize = new System.Drawing.Size(442, 0);
            this.m_PanelSection.MinimumSize = new System.Drawing.Size(442, 713);
            this.m_PanelSection.Name = "m_PanelSection";
            this.m_PanelSection.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.m_PanelSection.Size = new System.Drawing.Size(442, 1393);
            this.m_PanelSection.TabIndex = 1;
            // 
            // m_SubSectionSpawnBOverrides
            // 
            this.m_SubSectionSpawnBOverrides.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_SubSectionSpawnBOverrides.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SubSectionSpawnBOverrides.Controls.Add(this.m_SpawnBOverrides);
            this.m_SubSectionSpawnBOverrides.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_SubSectionSpawnBOverrides.Expanded = true;
            this.m_SubSectionSpawnBOverrides.HeaderText = "";
            this.m_SubSectionSpawnBOverrides.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.m_SubSectionSpawnBOverrides.Location = new System.Drawing.Point(0, 1247);
            this.m_SubSectionSpawnBOverrides.Name = "m_SubSectionSpawnBOverrides";
            this.m_SubSectionSpawnBOverrides.SectionName = "Effect Spawner B Scalars";
            this.m_SubSectionSpawnBOverrides.Size = new System.Drawing.Size(442, 126);
            this.m_SubSectionSpawnBOverrides.TabIndex = 86;
            // 
            // m_SpawnBOverrides
            // 
            this.m_SpawnBOverrides.Location = new System.Drawing.Point(4, 20);
            this.m_SpawnBOverrides.Name = "m_SpawnBOverrides";
            this.m_SpawnBOverrides.Overridable = null;
            this.m_SpawnBOverrides.Size = new System.Drawing.Size(416, 108);
            this.m_SpawnBOverrides.TabIndex = 2;
            this.m_SpawnBOverrides.UseInheritDirection = false;
            // 
            // m_SubSectionSpawnAOverrides
            // 
            this.m_SubSectionSpawnAOverrides.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_SubSectionSpawnAOverrides.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SubSectionSpawnAOverrides.Controls.Add(this.m_SpawnAOverrides);
            this.m_SubSectionSpawnAOverrides.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_SubSectionSpawnAOverrides.Expanded = true;
            this.m_SubSectionSpawnAOverrides.HeaderText = "";
            this.m_SubSectionSpawnAOverrides.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.m_SubSectionSpawnAOverrides.Location = new System.Drawing.Point(0, 1116);
            this.m_SubSectionSpawnAOverrides.Name = "m_SubSectionSpawnAOverrides";
            this.m_SubSectionSpawnAOverrides.SectionName = "Effect Spawner A Scalars";
            this.m_SubSectionSpawnAOverrides.Size = new System.Drawing.Size(442, 131);
            this.m_SubSectionSpawnAOverrides.TabIndex = 84;
            // 
            // m_SpawnAOverrides
            // 
            this.m_SpawnAOverrides.Location = new System.Drawing.Point(4, 23);
            this.m_SpawnAOverrides.Name = "m_SpawnAOverrides";
            this.m_SpawnAOverrides.Overridable = null;
            this.m_SpawnAOverrides.Size = new System.Drawing.Size(416, 107);
            this.m_SpawnAOverrides.TabIndex = 2;
            this.m_SpawnAOverrides.UseInheritDirection = false;
            // 
            // m_SubSectionSprite
            // 
            this.m_SubSectionSprite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SubSectionSprite.Controls.Add(this.label11);
            this.m_SubSectionSprite.Controls.Add(this.m_comboProjMode);
            this.m_SubSectionSprite.Controls.Add(this.label12);
            this.m_SubSectionSprite.Controls.Add(this.m_checkBoxIsNormalSpec);
            this.m_SubSectionSprite.Controls.Add(this.m_comboDiffuseMode);
            this.m_SubSectionSprite.Controls.Add(this.m_ComboShader);
            this.m_SubSectionSprite.Controls.Add(this.label2);
            this.m_SubSectionSprite.Controls.Add(this.m_checkBoxIsRefract);
            this.m_SubSectionSprite.Controls.Add(this.m_checkBoxIsScreenSpace);
            this.m_SubSectionSprite.Controls.Add(this.m_checkBoxIsSoft);
            this.m_SubSectionSprite.Controls.Add(this.m_checkBoxIsLit);
            this.m_SubSectionSprite.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_SubSectionSprite.Expanded = true;
            this.m_SubSectionSprite.HeaderText = "";
            this.m_SubSectionSprite.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.m_SubSectionSprite.Location = new System.Drawing.Point(0, 1010);
            this.m_SubSectionSprite.Name = "m_SubSectionSprite";
            this.m_SubSectionSprite.SectionName = "Shader / Technique";
            this.m_SubSectionSprite.Size = new System.Drawing.Size(442, 106);
            this.m_SubSectionSprite.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 81);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 13);
            this.label11.TabIndex = 97;
            this.label11.Text = "Projection Mode";
            // 
            // m_comboProjMode
            // 
            this.m_comboProjMode.DropDownWidth = 300;
            this.m_comboProjMode.FormattingEnabled = true;
            this.m_comboProjMode.Items.AddRange(new object[] {
            "PROJ_NONE",
            "PROJ_WATER",
            "PROJ_NON_WATER",
            "PROJ_ALL"});
            this.m_comboProjMode.Location = new System.Drawing.Point(101, 73);
            this.m_comboProjMode.Name = "m_comboProjMode";
            this.m_comboProjMode.Size = new System.Drawing.Size(176, 21);
            this.m_comboProjMode.TabIndex = 96;
            this.m_comboProjMode.SelectionChangeCommitted += new System.EventHandler(this.Technique_OnValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 13);
            this.label12.TabIndex = 92;
            this.label12.Text = "Diffuse Mode";
            // 
            // m_checkBoxIsNormalSpec
            // 
            this.m_checkBoxIsNormalSpec.AutoSize = true;
            this.m_checkBoxIsNormalSpec.Location = new System.Drawing.Point(362, 41);
            this.m_checkBoxIsNormalSpec.Name = "m_checkBoxIsNormalSpec";
            this.m_checkBoxIsNormalSpec.Size = new System.Drawing.Size(84, 17);
            this.m_checkBoxIsNormalSpec.TabIndex = 95;
            this.m_checkBoxIsNormalSpec.Text = "NormalSpec";
            this.m_checkBoxIsNormalSpec.UseVisualStyleBackColor = true;
            this.m_checkBoxIsNormalSpec.CheckedChanged += new System.EventHandler(this.m_checkBoxIsNormalSpec_CheckedChanged);
            this.m_checkBoxIsNormalSpec.Click += new System.EventHandler(this.Technique_OnValueChanged);
            // 
            // m_comboDiffuseMode
            // 
            this.m_comboDiffuseMode.DropDownWidth = 300;
            this.m_comboDiffuseMode.FormattingEnabled = true;
            this.m_comboDiffuseMode.Items.AddRange(new object[] {
            "TEX1_RGBA",
            "TEX1_RRRR",
            "TEX1_GGGG",
            "TEX1_BBBB",
            "TEX1_RGB",
            "TEX1_RG_BLEND"});
            this.m_comboDiffuseMode.Location = new System.Drawing.Point(101, 46);
            this.m_comboDiffuseMode.Name = "m_comboDiffuseMode";
            this.m_comboDiffuseMode.Size = new System.Drawing.Size(176, 21);
            this.m_comboDiffuseMode.TabIndex = 90;
            this.m_comboDiffuseMode.SelectionChangeCommitted += new System.EventHandler(this.Technique_OnValueChanged);
            // 
            // m_ComboShader
            // 
            this.m_ComboShader.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboShader.FormattingEnabled = true;
            this.m_ComboShader.Location = new System.Drawing.Point(103, 18);
            this.m_ComboShader.Name = "m_ComboShader";
            this.m_ComboShader.Size = new System.Drawing.Size(174, 21);
            this.m_ComboShader.Sorted = true;
            this.m_ComboShader.TabIndex = 55;
            this.m_ComboShader.SelectionChangeCommitted += new System.EventHandler(this.m_ComboShader_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 56;
            this.label2.Text = "Shader:";
            // 
            // m_checkBoxIsRefract
            // 
            this.m_checkBoxIsRefract.AutoSize = true;
            this.m_checkBoxIsRefract.Location = new System.Drawing.Point(283, 41);
            this.m_checkBoxIsRefract.Name = "m_checkBoxIsRefract";
            this.m_checkBoxIsRefract.Size = new System.Drawing.Size(61, 17);
            this.m_checkBoxIsRefract.TabIndex = 94;
            this.m_checkBoxIsRefract.Text = "Refract";
            this.m_checkBoxIsRefract.UseVisualStyleBackColor = true;
            this.m_checkBoxIsRefract.CheckedChanged += new System.EventHandler(this.m_checkBoxIsRefract_CheckedChanged);
            this.m_checkBoxIsRefract.Click += new System.EventHandler(this.Technique_OnValueChanged);
            // 
            // m_checkBoxIsScreenSpace
            // 
            this.m_checkBoxIsScreenSpace.AutoSize = true;
            this.m_checkBoxIsScreenSpace.Location = new System.Drawing.Point(283, 64);
            this.m_checkBoxIsScreenSpace.Name = "m_checkBoxIsScreenSpace";
            this.m_checkBoxIsScreenSpace.Size = new System.Drawing.Size(94, 17);
            this.m_checkBoxIsScreenSpace.TabIndex = 61;
            this.m_checkBoxIsScreenSpace.Text = "Screen Space";
            this.m_checkBoxIsScreenSpace.UseVisualStyleBackColor = true;
            this.m_checkBoxIsScreenSpace.Click += new System.EventHandler(this.Technique_OnValueChanged);
            // 
            // m_checkBoxIsSoft
            // 
            this.m_checkBoxIsSoft.AutoSize = true;
            this.m_checkBoxIsSoft.Checked = true;
            this.m_checkBoxIsSoft.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_checkBoxIsSoft.Location = new System.Drawing.Point(363, 18);
            this.m_checkBoxIsSoft.Name = "m_checkBoxIsSoft";
            this.m_checkBoxIsSoft.Size = new System.Drawing.Size(45, 17);
            this.m_checkBoxIsSoft.TabIndex = 60;
            this.m_checkBoxIsSoft.Text = "Soft";
            this.m_checkBoxIsSoft.UseVisualStyleBackColor = true;
            this.m_checkBoxIsSoft.Click += new System.EventHandler(this.Technique_OnValueChanged);
            // 
            // m_checkBoxIsLit
            // 
            this.m_checkBoxIsLit.AutoSize = true;
            this.m_checkBoxIsLit.Checked = true;
            this.m_checkBoxIsLit.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_checkBoxIsLit.Location = new System.Drawing.Point(283, 18);
            this.m_checkBoxIsLit.Name = "m_checkBoxIsLit";
            this.m_checkBoxIsLit.Size = new System.Drawing.Size(37, 17);
            this.m_checkBoxIsLit.TabIndex = 59;
            this.m_checkBoxIsLit.Text = "Lit";
            this.m_checkBoxIsLit.UseVisualStyleBackColor = true;
            this.m_checkBoxIsLit.Click += new System.EventHandler(this.Technique_OnValueChanged);
            // 
            // m_SubSectionModel
            // 
            this.m_SubSectionModel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SubSectionModel.Controls.Add(this.m_ListModels);
            this.m_SubSectionModel.Controls.Add(this.label7);
            this.m_SubSectionModel.Controls.Add(this.label6);
            this.m_SubSectionModel.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_SubSectionModel.Expanded = true;
            this.m_SubSectionModel.HeaderText = "";
            this.m_SubSectionModel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.m_SubSectionModel.Location = new System.Drawing.Point(0, 902);
            this.m_SubSectionModel.Name = "m_SubSectionModel";
            this.m_SubSectionModel.SectionName = "3D Model";
            this.m_SubSectionModel.Size = new System.Drawing.Size(442, 108);
            this.m_SubSectionModel.TabIndex = 7;
            // 
            // m_ListModels
            // 
            this.m_ListModels.ContextMenuStrip = this.m_ContextModelList;
            this.m_ListModels.FormattingEnabled = true;
            this.m_ListModels.Location = new System.Drawing.Point(8, 39);
            this.m_ListModels.Name = "m_ListModels";
            this.m_ListModels.Size = new System.Drawing.Size(287, 56);
            this.m_ListModels.TabIndex = 2;
            // 
            // m_ContextModelList
            // 
            this.m_ContextModelList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_AddModelMenuItem,
            this.m_DelModelMenuItem});
            this.m_ContextModelList.Name = "m_ContextModelList";
            this.m_ContextModelList.ShowImageMargin = false;
            this.m_ContextModelList.Size = new System.Drawing.Size(118, 48);
            this.m_ContextModelList.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContextModelList_Opening);
            // 
            // m_AddModelMenuItem
            // 
            this.m_AddModelMenuItem.Name = "m_AddModelMenuItem";
            this.m_AddModelMenuItem.Size = new System.Drawing.Size(117, 22);
            this.m_AddModelMenuItem.Text = "Add Model...";
            this.m_AddModelMenuItem.Click += new System.EventHandler(this.m_AddModelMenuItem_Click);
            // 
            // m_DelModelMenuItem
            // 
            this.m_DelModelMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.m_DelModelMenuItem.Name = "m_DelModelMenuItem";
            this.m_DelModelMenuItem.Size = new System.Drawing.Size(117, 22);
            this.m_DelModelMenuItem.Text = "Delete";
            this.m_DelModelMenuItem.Click += new System.EventHandler(this.m_DelModelMenuItem_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(5, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 95;
            this.label7.Text = "Assigned Models";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(301, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 13);
            this.label6.TabIndex = 94;
            this.label6.Text = "Right click to Add/Remove";
            // 
            // ptxSubSectionBiasLinks
            // 
            this.ptxSubSectionBiasLinks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptxSubSectionBiasLinks.Controls.Add(this.m_DataGridLinkSet);
            this.ptxSubSectionBiasLinks.Controls.Add(this.label14);
            this.ptxSubSectionBiasLinks.Controls.Add(this.label13);
            this.ptxSubSectionBiasLinks.Controls.Add(this.m_DataGridBiasProps);
            this.ptxSubSectionBiasLinks.Controls.Add(this.label15);
            this.ptxSubSectionBiasLinks.Dock = System.Windows.Forms.DockStyle.Top;
            this.ptxSubSectionBiasLinks.Expanded = true;
            this.ptxSubSectionBiasLinks.HeaderText = "";
            this.ptxSubSectionBiasLinks.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ptxSubSectionBiasLinks.Location = new System.Drawing.Point(0, 752);
            this.ptxSubSectionBiasLinks.Name = "ptxSubSectionBiasLinks";
            this.ptxSubSectionBiasLinks.SectionName = "Property Bias Linking";
            this.ptxSubSectionBiasLinks.Size = new System.Drawing.Size(442, 150);
            this.ptxSubSectionBiasLinks.TabIndex = 7;
            // 
            // m_DataGridLinkSet
            // 
            this.m_DataGridLinkSet.AllowUserToAddRows = false;
            this.m_DataGridLinkSet.AllowUserToDeleteRows = false;
            this.m_DataGridLinkSet.AllowUserToResizeColumns = false;
            this.m_DataGridLinkSet.AllowUserToResizeRows = false;
            this.m_DataGridLinkSet.BackgroundColor = System.Drawing.Color.White;
            this.m_DataGridLinkSet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_DataGridLinkSet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            this.m_DataGridLinkSet.ContextMenuStrip = this.m_ContexBiasSet;
            this.m_DataGridLinkSet.Location = new System.Drawing.Point(8, 42);
            this.m_DataGridLinkSet.MultiSelect = false;
            this.m_DataGridLinkSet.Name = "m_DataGridLinkSet";
            this.m_DataGridLinkSet.RowHeadersVisible = false;
            this.m_DataGridLinkSet.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_DataGridLinkSet.ShowCellToolTips = false;
            this.m_DataGridLinkSet.Size = new System.Drawing.Size(105, 95);
            this.m_DataGridLinkSet.TabIndex = 28;
            this.m_DataGridLinkSet.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.m_DataGridLinkSet_CellBeginEdit);
            this.m_DataGridLinkSet.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_DataGridLinkSet_CellEndEdit);
            this.m_DataGridLinkSet.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SelectOnRightMouseButton_CellMouseDown);
            this.m_DataGridLinkSet.SelectionChanged += new System.EventHandler(this.m_DataGridLinkSet_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Property";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // m_ContexBiasSet
            // 
            this.m_ContexBiasSet.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_ContexAddSet,
            this.m_ContexRemoveSet});
            this.m_ContexBiasSet.Name = "m_ContexBiasSet";
            this.m_ContexBiasSet.Size = new System.Drawing.Size(137, 48);
            this.m_ContexBiasSet.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContexBiasSet_Opening);
            // 
            // m_ContexAddSet
            // 
            this.m_ContexAddSet.Name = "m_ContexAddSet";
            this.m_ContexAddSet.Size = new System.Drawing.Size(136, 22);
            this.m_ContexAddSet.Text = "Add Set";
            this.m_ContexAddSet.Click += new System.EventHandler(this.m_ContexAddSet_Click);
            // 
            // m_ContexRemoveSet
            // 
            this.m_ContexRemoveSet.Name = "m_ContexRemoveSet";
            this.m_ContexRemoveSet.Size = new System.Drawing.Size(136, 22);
            this.m_ContexRemoveSet.Text = "Remove Set";
            this.m_ContexRemoveSet.Click += new System.EventHandler(this.m_ContexRemoveSet_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(122, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "Property";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "Link Set";
            // 
            // m_DataGridBiasProps
            // 
            this.m_DataGridBiasProps.AllowUserToAddRows = false;
            this.m_DataGridBiasProps.AllowUserToDeleteRows = false;
            this.m_DataGridBiasProps.AllowUserToResizeColumns = false;
            this.m_DataGridBiasProps.AllowUserToResizeRows = false;
            this.m_DataGridBiasProps.BackgroundColor = System.Drawing.Color.White;
            this.m_DataGridBiasProps.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_DataGridBiasProps.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.m_DataGridBiasProps.ContextMenuStrip = this.m_ContextBiasProp;
            this.m_DataGridBiasProps.Location = new System.Drawing.Point(123, 42);
            this.m_DataGridBiasProps.MultiSelect = false;
            this.m_DataGridBiasProps.Name = "m_DataGridBiasProps";
            this.m_DataGridBiasProps.RowHeadersVisible = false;
            this.m_DataGridBiasProps.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_DataGridBiasProps.ShowCellToolTips = false;
            this.m_DataGridBiasProps.Size = new System.Drawing.Size(307, 95);
            this.m_DataGridBiasProps.TabIndex = 25;
            this.m_DataGridBiasProps.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SelectOnRightMouseButton_CellMouseDown);
            this.m_DataGridBiasProps.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_DataGridBiasProps_CellValueChanged);
            this.m_DataGridBiasProps.CurrentCellDirtyStateChanged += new System.EventHandler(this.m_DataGridBiasProps_CurrentCellDirtyStateChanged);
            this.m_DataGridBiasProps.SelectionChanged += new System.EventHandler(this.DataGridAutoSelectValidCell);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Property";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 250;
            // 
            // Column2
            // 
            this.Column2.FalseValue = "false";
            this.Column2.HeaderText = "InvertBias";
            this.Column2.Name = "Column2";
            this.Column2.TrueValue = "true";
            this.Column2.Width = 53;
            // 
            // m_ContextBiasProp
            // 
            this.m_ContextBiasProp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_ContextAddBiasProp,
            this.m_ContextRemoveBiasProp});
            this.m_ContextBiasProp.Name = "m_ContextBiasProp";
            this.m_ContextBiasProp.Size = new System.Drawing.Size(166, 48);
            this.m_ContextBiasProp.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContextBiasProp_Opening);
            // 
            // m_ContextAddBiasProp
            // 
            this.m_ContextAddBiasProp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.blankToolStripMenuItem});
            this.m_ContextAddBiasProp.Name = "m_ContextAddBiasProp";
            this.m_ContextAddBiasProp.Size = new System.Drawing.Size(165, 22);
            this.m_ContextAddBiasProp.Text = "Add Property";
            this.m_ContextAddBiasProp.DropDownOpening += new System.EventHandler(this.m_ContextAddBiasProp_DropDownOpening);
            // 
            // blankToolStripMenuItem
            // 
            this.blankToolStripMenuItem.Name = "blankToolStripMenuItem";
            this.blankToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.blankToolStripMenuItem.Text = "blank";
            // 
            // m_ContextRemoveBiasProp
            // 
            this.m_ContextRemoveBiasProp.Name = "m_ContextRemoveBiasProp";
            this.m_ContextRemoveBiasProp.Size = new System.Drawing.Size(165, 22);
            this.m_ContextRemoveBiasProp.Text = "Remove Property";
            this.m_ContextRemoveBiasProp.Click += new System.EventHandler(this.m_ContextRemoveBiasProp_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(372, 25);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 13);
            this.label15.TabIndex = 27;
            this.label15.Text = "Invert Bias";
            // 
            // m_SubSectionShader
            // 
            this.m_SubSectionShader.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_SubSectionShader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SubSectionShader.Controls.Add(this.m_ShaderPanel);
            this.m_SubSectionShader.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_SubSectionShader.Expanded = true;
            this.m_SubSectionShader.HeaderText = "";
            this.m_SubSectionShader.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.m_SubSectionShader.Location = new System.Drawing.Point(0, 663);
            this.m_SubSectionShader.Name = "m_SubSectionShader";
            this.m_SubSectionShader.SectionName = "Shader Variables";
            this.m_SubSectionShader.Size = new System.Drawing.Size(442, 89);
            this.m_SubSectionShader.TabIndex = 22;
            // 
            // m_ShaderPanel
            // 
            this.m_ShaderPanel.Location = new System.Drawing.Point(0, 24);
            this.m_ShaderPanel.Name = "m_ShaderPanel";
            this.m_ShaderPanel.Size = new System.Drawing.Size(446, 196);
            this.m_ShaderPanel.TabIndex = 2;
            // 
            // m_SubSectionPhysical
            // 
            this.m_SubSectionPhysical.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SubSectionPhysical.Controls.Add(this.label24);
            this.m_SubSectionPhysical.Controls.Add(this.m_SlidePerKill);
            this.m_SubSectionPhysical.Controls.Add(this.label21);
            this.m_SubSectionPhysical.Controls.Add(this.m_SlidePerPhysical);
            this.m_SubSectionPhysical.Controls.Add(this.label19);
            this.m_SubSectionPhysical.Controls.Add(this.label20);
            this.m_SubSectionPhysical.Controls.Add(this.label18);
            this.m_SubSectionPhysical.Controls.Add(this.m_CheckShowPhysics);
            this.m_SubSectionPhysical.Controls.Add(this.m_StopVel);
            this.m_SubSectionPhysical.Controls.Add(this.m_PhysicalRange);
            this.m_SubSectionPhysical.Controls.Add(this.m_CheckOnCollideKill);
            this.m_SubSectionPhysical.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_SubSectionPhysical.Expanded = true;
            this.m_SubSectionPhysical.HeaderText = "";
            this.m_SubSectionPhysical.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.m_SubSectionPhysical.Location = new System.Drawing.Point(0, 545);
            this.m_SubSectionPhysical.Name = "m_SubSectionPhysical";
            this.m_SubSectionPhysical.SectionName = "Physical Properties";
            this.m_SubSectionPhysical.Size = new System.Drawing.Size(442, 118);
            this.m_SubSectionPhysical.TabIndex = 29;
            this.m_SubSectionPhysical.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(208, 86);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(60, 13);
            this.label24.TabIndex = 97;
            this.label24.Text = "Percent Kill";
            // 
            // m_SlidePerKill
            // 
            this.m_SlidePerKill.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SlidePerKill.Decimals = 0;
            this.m_SlidePerKill.Increment = 1F;
            this.m_SlidePerKill.Location = new System.Drawing.Point(294, 84);
            this.m_SlidePerKill.MaxValue = 100F;
            this.m_SlidePerKill.MinValue = 0F;
            this.m_SlidePerKill.Multiline = true;
            this.m_SlidePerKill.Name = "m_SlidePerKill";
            this.m_SlidePerKill.SetValue = 0F;
            this.m_SlidePerKill.Size = new System.Drawing.Size(37, 20);
            this.m_SlidePerKill.TabIndex = 96;
            this.m_SlidePerKill.Text = "0";
            this.m_SlidePerKill.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_SlidePerKill.Value = 0F;
            this.m_SlidePerKill.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(13, 76);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(86, 13);
            this.label21.TabIndex = 93;
            this.label21.Text = "Percent Physical";
            // 
            // m_SlidePerPhysical
            // 
            this.m_SlidePerPhysical.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SlidePerPhysical.Decimals = 0;
            this.m_SlidePerPhysical.Increment = 1F;
            this.m_SlidePerPhysical.Location = new System.Drawing.Point(125, 74);
            this.m_SlidePerPhysical.MaxValue = 100F;
            this.m_SlidePerPhysical.MinValue = 0F;
            this.m_SlidePerPhysical.Multiline = true;
            this.m_SlidePerPhysical.Name = "m_SlidePerPhysical";
            this.m_SlidePerPhysical.SetValue = 0F;
            this.m_SlidePerPhysical.Size = new System.Drawing.Size(37, 20);
            this.m_SlidePerPhysical.TabIndex = 92;
            this.m_SlidePerPhysical.Text = "0";
            this.m_SlidePerPhysical.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_SlidePerPhysical.Value = 0F;
            this.m_SlidePerPhysical.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(12, 50);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(91, 13);
            this.label19.TabIndex = 90;
            this.label19.Text = "Stop At Velocity <";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(181, 50);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 13);
            this.label20.TabIndex = 91;
            this.label20.Text = "OnCollide:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(12, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(81, 13);
            this.label18.TabIndex = 86;
            this.label18.Text = "Physical Range";
            // 
            // m_CheckShowPhysics
            // 
            this.m_CheckShowPhysics.AutoSize = true;
            this.m_CheckShowPhysics.Checked = true;
            this.m_CheckShowPhysics.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckShowPhysics.ForeColor = System.Drawing.Color.Red;
            this.m_CheckShowPhysics.Location = new System.Drawing.Point(184, 23);
            this.m_CheckShowPhysics.Name = "m_CheckShowPhysics";
            this.m_CheckShowPhysics.Size = new System.Drawing.Size(137, 17);
            this.m_CheckShowPhysics.TabIndex = 88;
            this.m_CheckShowPhysics.Text = "Display Physical Range";
            this.m_CheckShowPhysics.UseVisualStyleBackColor = true;
            this.m_CheckShowPhysics.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_StopVel
            // 
            this.m_StopVel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_StopVel.Decimals = 3;
            this.m_StopVel.Increment = 0.001F;
            this.m_StopVel.Location = new System.Drawing.Point(106, 48);
            this.m_StopVel.MaxValue = 10000F;
            this.m_StopVel.MinValue = -1F;
            this.m_StopVel.Multiline = true;
            this.m_StopVel.Name = "m_StopVel";
            this.m_StopVel.SetValue = 0F;
            this.m_StopVel.Size = new System.Drawing.Size(59, 20);
            this.m_StopVel.TabIndex = 89;
            this.m_StopVel.Text = "0.000";
            this.m_StopVel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_StopVel.Value = 0F;
            this.m_StopVel.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_PhysicalRange
            // 
            this.m_PhysicalRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_PhysicalRange.Decimals = 3;
            this.m_PhysicalRange.Increment = 0.001F;
            this.m_PhysicalRange.Location = new System.Drawing.Point(106, 22);
            this.m_PhysicalRange.MaxValue = 10000F;
            this.m_PhysicalRange.MinValue = -1F;
            this.m_PhysicalRange.Multiline = true;
            this.m_PhysicalRange.Name = "m_PhysicalRange";
            this.m_PhysicalRange.SetValue = 0F;
            this.m_PhysicalRange.Size = new System.Drawing.Size(59, 20);
            this.m_PhysicalRange.TabIndex = 85;
            this.m_PhysicalRange.Text = "0.000";
            this.m_PhysicalRange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_PhysicalRange.Value = 0F;
            this.m_PhysicalRange.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_CheckOnCollideKill
            // 
            this.m_CheckOnCollideKill.AutoSize = true;
            this.m_CheckOnCollideKill.Checked = true;
            this.m_CheckOnCollideKill.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckOnCollideKill.Location = new System.Drawing.Point(211, 66);
            this.m_CheckOnCollideKill.Name = "m_CheckOnCollideKill";
            this.m_CheckOnCollideKill.Size = new System.Drawing.Size(77, 17);
            this.m_CheckOnCollideKill.TabIndex = 87;
            this.m_CheckOnCollideKill.Text = "Kill Particle";
            this.m_CheckOnCollideKill.UseVisualStyleBackColor = true;
            this.m_CheckOnCollideKill.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_SubSectionKeyframes
            // 
            this.m_SubSectionKeyframes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SubSectionKeyframes.Controls.Add(this.m_DataGridPtxtKeyframeList);
            this.m_SubSectionKeyframes.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_SubSectionKeyframes.Expanded = true;
            this.m_SubSectionKeyframes.HeaderText = "";
            this.m_SubSectionKeyframes.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.m_SubSectionKeyframes.Location = new System.Drawing.Point(0, 353);
            this.m_SubSectionKeyframes.Name = "m_SubSectionKeyframes";
            this.m_SubSectionKeyframes.SectionName = "Behaviors";
            this.m_SubSectionKeyframes.Size = new System.Drawing.Size(442, 192);
            this.m_SubSectionKeyframes.TabIndex = 6;
            // 
            // m_DataGridPtxtKeyframeList
            // 
            this.m_DataGridPtxtKeyframeList.AllowUserToAddRows = false;
            this.m_DataGridPtxtKeyframeList.AllowUserToDeleteRows = false;
            this.m_DataGridPtxtKeyframeList.AllowUserToResizeColumns = false;
            this.m_DataGridPtxtKeyframeList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AliceBlue;
            this.m_DataGridPtxtKeyframeList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.m_DataGridPtxtKeyframeList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.m_DataGridPtxtKeyframeList.BackgroundColor = System.Drawing.SystemColors.Window;
            this.m_DataGridPtxtKeyframeList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.m_DataGridPtxtKeyframeList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.m_DataGridPtxtKeyframeList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridPtxtKeyframeList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.m_DataGridPtxtKeyframeList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.m_ColumnTextRuleName,
            this.Evolution1,
            this.Evolution2,
            this.Evolution3,
            this.Evolution4,
            this.Evolution5,
            this.EvolutionLOD,
            this.EvoBlend,
            this.Invis,
            this.Profile});
            this.m_DataGridPtxtKeyframeList.ContextMenuStrip = this.m_ContextMenuKeyProp;
            this.m_DataGridPtxtKeyframeList.GridColor = System.Drawing.SystemColors.Window;
            this.m_DataGridPtxtKeyframeList.Location = new System.Drawing.Point(8, 25);
            this.m_DataGridPtxtKeyframeList.MultiSelect = false;
            this.m_DataGridPtxtKeyframeList.Name = "m_DataGridPtxtKeyframeList";
            this.m_DataGridPtxtKeyframeList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridPtxtKeyframeList.RowHeadersVisible = false;
            this.m_DataGridPtxtKeyframeList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.m_DataGridPtxtKeyframeList.RowTemplate.Height = 15;
            this.m_DataGridPtxtKeyframeList.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridPtxtKeyframeList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_DataGridPtxtKeyframeList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.m_DataGridPtxtKeyframeList.ShowCellToolTips = false;
            this.m_DataGridPtxtKeyframeList.Size = new System.Drawing.Size(422, 159);
            this.m_DataGridPtxtKeyframeList.TabIndex = 23;
            this.m_DataGridPtxtKeyframeList.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SelectOnRightMouseButton_CellMouseDown);
            this.m_DataGridPtxtKeyframeList.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_DataGridPtxtKeyframeList_CellValueChanged);
            this.m_DataGridPtxtKeyframeList.CurrentCellDirtyStateChanged += new System.EventHandler(this.m_DataGridPtxtKeyframeList_CurrentCellDirtyStateChanged);
            this.m_DataGridPtxtKeyframeList.SelectionChanged += new System.EventHandler(this.DataGridAutoSelectValidCell);
            this.m_DataGridPtxtKeyframeList.DoubleClick += new System.EventHandler(this.ShowKeyframe);
            // 
            // m_ColumnTextRuleName
            // 
            this.m_ColumnTextRuleName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.m_ColumnTextRuleName.HeaderText = "Property";
            this.m_ColumnTextRuleName.Name = "m_ColumnTextRuleName";
            this.m_ColumnTextRuleName.ReadOnly = true;
            this.m_ColumnTextRuleName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_ColumnTextRuleName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.m_ColumnTextRuleName.Width = 52;
            // 
            // Evolution1
            // 
            this.Evolution1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Red;
            this.Evolution1.DefaultCellStyle = dataGridViewCellStyle2;
            this.Evolution1.HeaderText = "Evolution1";
            this.Evolution1.Name = "Evolution1";
            this.Evolution1.ReadOnly = true;
            this.Evolution1.Width = 5;
            // 
            // Evolution2
            // 
            this.Evolution2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Blue;
            this.Evolution2.DefaultCellStyle = dataGridViewCellStyle3;
            this.Evolution2.HeaderText = "Evolution2";
            this.Evolution2.Name = "Evolution2";
            this.Evolution2.ReadOnly = true;
            this.Evolution2.Width = 5;
            // 
            // Evolution3
            // 
            this.Evolution3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Green;
            this.Evolution3.DefaultCellStyle = dataGridViewCellStyle4;
            this.Evolution3.HeaderText = "Evolution3";
            this.Evolution3.Name = "Evolution3";
            this.Evolution3.ReadOnly = true;
            this.Evolution3.Width = 5;
            // 
            // Evolution4
            // 
            this.Evolution4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Purple;
            this.Evolution4.DefaultCellStyle = dataGridViewCellStyle5;
            this.Evolution4.HeaderText = "Evolution4";
            this.Evolution4.Name = "Evolution4";
            this.Evolution4.ReadOnly = true;
            this.Evolution4.Width = 5;
            // 
            // Evolution5
            // 
            this.Evolution5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.SaddleBrown;
            this.Evolution5.DefaultCellStyle = dataGridViewCellStyle6;
            this.Evolution5.HeaderText = "Evolution5";
            this.Evolution5.MinimumWidth = 2;
            this.Evolution5.Name = "Evolution5";
            this.Evolution5.ReadOnly = true;
            this.Evolution5.Width = 2;
            // 
            // EvolutionLOD
            // 
            this.EvolutionLOD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.EvolutionLOD.HeaderText = "EvolutionLOD";
            this.EvolutionLOD.Name = "EvolutionLOD";
            this.EvolutionLOD.ReadOnly = true;
            this.EvolutionLOD.Width = 5;
            // 
            // EvoBlend
            // 
            this.EvoBlend.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EvoBlend.HeaderText = "EvoBlend";
            this.EvoBlend.Items.AddRange(new object[] {
            "Active Avg",
            "Add",
            "Max",
            "Full Avg"});
            this.EvoBlend.Name = "EvoBlend";
            this.EvoBlend.Visible = false;
            this.EvoBlend.Width = 80;
            // 
            // Invis
            // 
            this.Invis.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Invis.HeaderText = "Invis";
            this.Invis.Name = "Invis";
            this.Invis.ReadOnly = true;
            // 
            // Profile
            // 
            this.Profile.HeaderText = "Profile";
            this.Profile.Name = "Profile";
            this.Profile.ReadOnly = true;
            this.Profile.Width = 61;
            // 
            // m_ContextMenuKeyProp
            // 
            this.m_ContextMenuKeyProp.Name = "m_ContextMenuKeyProp";
            this.m_ContextMenuKeyProp.Size = new System.Drawing.Size(61, 4);
            this.m_ContextMenuKeyProp.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContextMenuKeyProps_Opening);
            // 
            // m_SubSectionSetup
            // 
            this.m_SubSectionSetup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SubSectionSetup.Controls.Add(this.label9);
            this.m_SubSectionSetup.Controls.Add(this.m_TrackNegDirACheck);
            this.m_SubSectionSetup.Controls.Add(this.m_TrackNegDirBCheck);
            this.m_SubSectionSetup.Controls.Add(this.m_TrackPosBCheck);
            this.m_SubSectionSetup.Controls.Add(this.m_TrackDirBCheck);
            this.m_SubSectionSetup.Controls.Add(this.m_TrackDirACheck);
            this.m_SubSectionSetup.Controls.Add(this.label3);
            this.m_SubSectionSetup.Controls.Add(this.m_TrackPosACheck);
            this.m_SubSectionSetup.Controls.Add(this.label1);
            this.m_SubSectionSetup.Controls.Add(this.m_TexFrameIdMin);
            this.m_SubSectionSetup.Controls.Add(this.label5);
            this.m_SubSectionSetup.Controls.Add(this.m_LabelDrawType);
            this.m_SubSectionSetup.Controls.Add(this.m_TexFrameIdMax);
            this.m_SubSectionSetup.Controls.Add(this.label4);
            this.m_SubSectionSetup.Controls.Add(this.label10);
            this.m_SubSectionSetup.Controls.Add(this.m_AllowColorOverride);
            this.m_SubSectionSetup.Controls.Add(this.m_AllowPosOverride);
            this.m_SubSectionSetup.Controls.Add(this.m_ComboCullMode);
            this.m_SubSectionSetup.Controls.Add(this.m_ComboSortType);
            this.m_SubSectionSetup.Controls.Add(this.label8);
            this.m_SubSectionSetup.Controls.Add(this.label17);
            this.m_SubSectionSetup.Controls.Add(this.m_InheritLifeBCheck);
            this.m_SubSectionSetup.Controls.Add(this.m_SpawnEffectA);
            this.m_SubSectionSetup.Controls.Add(this.m_SpawnEffectBTime);
            this.m_SubSectionSetup.Controls.Add(this.m_ComboDrawType);
            this.m_SubSectionSetup.Controls.Add(this.label16);
            this.m_SubSectionSetup.Controls.Add(this.m_SpawnEffectB);
            this.m_SubSectionSetup.Controls.Add(this.m_SpawnEffectATime);
            this.m_SubSectionSetup.Controls.Add(this.m_CheckDepthTest);
            this.m_SubSectionSetup.Controls.Add(this.m_SpawnEffectBCheck);
            this.m_SubSectionSetup.Controls.Add(this.m_ComboBlendMode);
            this.m_SubSectionSetup.Controls.Add(this.m_InheritLifeACheck);
            this.m_SubSectionSetup.Controls.Add(this.m_SpawnEffectACheck);
            this.m_SubSectionSetup.Controls.Add(this.m_CheckDepthWrite);
            this.m_SubSectionSetup.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_SubSectionSetup.Expanded = true;
            this.m_SubSectionSetup.HeaderText = "";
            this.m_SubSectionSetup.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.m_SubSectionSetup.Location = new System.Drawing.Point(0, 0);
            this.m_SubSectionSetup.Name = "m_SubSectionSetup";
            this.m_SubSectionSetup.SectionName = "Particle Setup";
            this.m_SubSectionSetup.Size = new System.Drawing.Size(442, 353);
            this.m_SubSectionSetup.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(183, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 104;
            this.label9.Text = "Sort Mode";
            // 
            // m_TrackNegDirACheck
            // 
            this.m_TrackNegDirACheck.AutoSize = true;
            this.m_TrackNegDirACheck.Location = new System.Drawing.Point(263, 245);
            this.m_TrackNegDirACheck.Name = "m_TrackNegDirACheck";
            this.m_TrackNegDirACheck.Size = new System.Drawing.Size(172, 17);
            this.m_TrackNegDirACheck.TabIndex = 100;
            this.m_TrackNegDirACheck.Text = "Track Point Negative Direction";
            this.m_TrackNegDirACheck.UseVisualStyleBackColor = true;
            this.m_TrackNegDirACheck.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_TrackNegDirBCheck
            // 
            this.m_TrackNegDirBCheck.AutoSize = true;
            this.m_TrackNegDirBCheck.Location = new System.Drawing.Point(263, 326);
            this.m_TrackNegDirBCheck.Name = "m_TrackNegDirBCheck";
            this.m_TrackNegDirBCheck.Size = new System.Drawing.Size(172, 17);
            this.m_TrackNegDirBCheck.TabIndex = 103;
            this.m_TrackNegDirBCheck.Text = "Track Point Negative Direction";
            this.m_TrackNegDirBCheck.UseVisualStyleBackColor = true;
            this.m_TrackNegDirBCheck.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_TrackPosBCheck
            // 
            this.m_TrackPosBCheck.AutoSize = true;
            this.m_TrackPosBCheck.Location = new System.Drawing.Point(263, 303);
            this.m_TrackPosBCheck.Name = "m_TrackPosBCheck";
            this.m_TrackPosBCheck.Size = new System.Drawing.Size(121, 17);
            this.m_TrackPosBCheck.TabIndex = 101;
            this.m_TrackPosBCheck.Text = "Track Point Position";
            this.m_TrackPosBCheck.UseVisualStyleBackColor = true;
            this.m_TrackPosBCheck.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_TrackDirBCheck
            // 
            this.m_TrackDirBCheck.AutoSize = true;
            this.m_TrackDirBCheck.Location = new System.Drawing.Point(133, 326);
            this.m_TrackDirBCheck.Name = "m_TrackDirBCheck";
            this.m_TrackDirBCheck.Size = new System.Drawing.Size(126, 17);
            this.m_TrackDirBCheck.TabIndex = 102;
            this.m_TrackDirBCheck.Text = "Track Point Direction";
            this.m_TrackDirBCheck.UseVisualStyleBackColor = true;
            this.m_TrackDirBCheck.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_TrackDirACheck
            // 
            this.m_TrackDirACheck.AutoSize = true;
            this.m_TrackDirACheck.Location = new System.Drawing.Point(133, 245);
            this.m_TrackDirACheck.Name = "m_TrackDirACheck";
            this.m_TrackDirACheck.Size = new System.Drawing.Size(126, 17);
            this.m_TrackDirACheck.TabIndex = 99;
            this.m_TrackDirACheck.Text = "Track Point Direction";
            this.m_TrackDirACheck.UseVisualStyleBackColor = true;
            this.m_TrackDirACheck.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 97;
            this.label3.Text = "Game Overrides";
            // 
            // m_TrackPosACheck
            // 
            this.m_TrackPosACheck.AutoSize = true;
            this.m_TrackPosACheck.Location = new System.Drawing.Point(263, 222);
            this.m_TrackPosACheck.Name = "m_TrackPosACheck";
            this.m_TrackPosACheck.Size = new System.Drawing.Size(121, 17);
            this.m_TrackPosACheck.TabIndex = 98;
            this.m_TrackPosACheck.Text = "Track Point Position";
            this.m_TrackPosACheck.UseVisualStyleBackColor = true;
            this.m_TrackPosACheck.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 178);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 96;
            this.label1.Text = "Spawners";
            // 
            // m_TexFrameIdMin
            // 
            this.m_TexFrameIdMin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_TexFrameIdMin.Decimals = 0;
            this.m_TexFrameIdMin.Increment = 1F;
            this.m_TexFrameIdMin.Location = new System.Drawing.Point(106, 69);
            this.m_TexFrameIdMin.MaxValue = 99F;
            this.m_TexFrameIdMin.MinValue = 0F;
            this.m_TexFrameIdMin.Multiline = true;
            this.m_TexFrameIdMin.Name = "m_TexFrameIdMin";
            this.m_TexFrameIdMin.SetValue = 0F;
            this.m_TexFrameIdMin.Size = new System.Drawing.Size(56, 20);
            this.m_TexFrameIdMin.TabIndex = 94;
            this.m_TexFrameIdMin.Text = "0";
            this.m_TexFrameIdMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_TexFrameIdMin.Value = 0F;
            this.m_TexFrameIdMin.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_TexFrameIdMin_OnValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 13);
            this.label5.TabIndex = 93;
            this.label5.Text = "Tex Frame id Max";
            // 
            // m_LabelDrawType
            // 
            this.m_LabelDrawType.AutoSize = true;
            this.m_LabelDrawType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelDrawType.Location = new System.Drawing.Point(183, 100);
            this.m_LabelDrawType.Name = "m_LabelDrawType";
            this.m_LabelDrawType.Size = new System.Drawing.Size(59, 13);
            this.m_LabelDrawType.TabIndex = 91;
            this.m_LabelDrawType.Text = "Draw Type";
            // 
            // m_TexFrameIdMax
            // 
            this.m_TexFrameIdMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_TexFrameIdMax.Decimals = 0;
            this.m_TexFrameIdMax.Increment = 1F;
            this.m_TexFrameIdMax.Location = new System.Drawing.Point(106, 99);
            this.m_TexFrameIdMax.MaxValue = 99F;
            this.m_TexFrameIdMax.MinValue = 0F;
            this.m_TexFrameIdMax.Multiline = true;
            this.m_TexFrameIdMax.Name = "m_TexFrameIdMax";
            this.m_TexFrameIdMax.SetValue = 0F;
            this.m_TexFrameIdMax.Size = new System.Drawing.Size(56, 20);
            this.m_TexFrameIdMax.TabIndex = 95;
            this.m_TexFrameIdMax.Text = "0";
            this.m_TexFrameIdMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_TexFrameIdMax.Value = 0F;
            this.m_TexFrameIdMax.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_TexFrameIdMax_OnValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 92;
            this.label4.Text = "Tex Frame id Min";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(183, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 83;
            this.label10.Text = "Cull Mode";
            // 
            // m_AllowColorOverride
            // 
            this.m_AllowColorOverride.AutoSize = true;
            this.m_AllowColorOverride.Checked = true;
            this.m_AllowColorOverride.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_AllowColorOverride.Location = new System.Drawing.Point(186, 152);
            this.m_AllowColorOverride.Name = "m_AllowColorOverride";
            this.m_AllowColorOverride.Size = new System.Drawing.Size(159, 17);
            this.m_AllowColorOverride.TabIndex = 90;
            this.m_AllowColorOverride.Text = "Allow game to override color";
            this.m_AllowColorOverride.UseVisualStyleBackColor = true;
            this.m_AllowColorOverride.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_AllowPosOverride
            // 
            this.m_AllowPosOverride.AutoSize = true;
            this.m_AllowPosOverride.Checked = true;
            this.m_AllowPosOverride.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_AllowPosOverride.Location = new System.Drawing.Point(6, 152);
            this.m_AllowPosOverride.Name = "m_AllowPosOverride";
            this.m_AllowPosOverride.Size = new System.Drawing.Size(172, 17);
            this.m_AllowPosOverride.TabIndex = 89;
            this.m_AllowPosOverride.Text = "Allow game to override position";
            this.m_AllowPosOverride.UseVisualStyleBackColor = true;
            this.m_AllowPosOverride.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_ComboCullMode
            // 
            this.m_ComboCullMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboCullMode.FormattingEnabled = true;
            this.m_ComboCullMode.Items.AddRange(new object[] {
            "None",
            "Front",
            "Back"});
            this.m_ComboCullMode.Location = new System.Drawing.Point(253, 71);
            this.m_ComboCullMode.Name = "m_ComboCullMode";
            this.m_ComboCullMode.Size = new System.Drawing.Size(176, 21);
            this.m_ComboCullMode.TabIndex = 82;
            this.m_ComboCullMode.SelectionChangeCommitted += new System.EventHandler(this.m_ComboBlendMode_SelectionChangeCommitted);
            // 
            // m_ComboSortType
            // 
            this.m_ComboSortType.Enabled = false;
            this.m_ComboSortType.FormattingEnabled = true;
            this.m_ComboSortType.Items.AddRange(new object[] {
            "Closest First",
            "Farthest First",
            "Newest First",
            "Oldest First",
            "Random"});
            this.m_ComboSortType.Location = new System.Drawing.Point(253, 20);
            this.m_ComboSortType.Name = "m_ComboSortType";
            this.m_ComboSortType.Size = new System.Drawing.Size(176, 21);
            this.m_ComboSortType.TabIndex = 65;
            this.m_ComboSortType.Text = "Newest First";
            this.m_ComboSortType.SelectionChangeCommitted += new System.EventHandler(this.m_ComboSortType_SelectionChangeCommitted);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(183, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 54;
            this.label8.Text = "Blend Mode";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(306, 279);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 13);
            this.label17.TabIndex = 86;
            this.label17.Text = "Vel Thresh";
            // 
            // m_InheritLifeBCheck
            // 
            this.m_InheritLifeBCheck.AutoSize = true;
            this.m_InheritLifeBCheck.Location = new System.Drawing.Point(133, 303);
            this.m_InheritLifeBCheck.Name = "m_InheritLifeBCheck";
            this.m_InheritLifeBCheck.Size = new System.Drawing.Size(102, 17);
            this.m_InheritLifeBCheck.TabIndex = 88;
            this.m_InheritLifeBCheck.Text = "Inherit Point Life";
            this.m_InheritLifeBCheck.UseVisualStyleBackColor = true;
            this.m_InheritLifeBCheck.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_SpawnEffectA
            // 
            this.m_SpawnEffectA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_SpawnEffectA.Enabled = false;
            this.m_SpawnEffectA.FormattingEnabled = true;
            this.m_SpawnEffectA.Location = new System.Drawing.Point(135, 195);
            this.m_SpawnEffectA.Name = "m_SpawnEffectA";
            this.m_SpawnEffectA.Size = new System.Drawing.Size(165, 21);
            this.m_SpawnEffectA.Sorted = true;
            this.m_SpawnEffectA.TabIndex = 77;
            this.m_SpawnEffectA.DropDown += new System.EventHandler(this.BuildSpawnEffect_DropDown);
            this.m_SpawnEffectA.SelectionChangeCommitted += new System.EventHandler(this.SpawnEffect_SelectionChangeCommitted);
            // 
            // m_SpawnEffectBTime
            // 
            this.m_SpawnEffectBTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SpawnEffectBTime.Decimals = 3;
            this.m_SpawnEffectBTime.Increment = 0.01F;
            this.m_SpawnEffectBTime.Location = new System.Drawing.Point(380, 275);
            this.m_SpawnEffectBTime.MaxValue = 10F;
            this.m_SpawnEffectBTime.MinValue = 0F;
            this.m_SpawnEffectBTime.Multiline = true;
            this.m_SpawnEffectBTime.Name = "m_SpawnEffectBTime";
            this.m_SpawnEffectBTime.SetValue = 1F;
            this.m_SpawnEffectBTime.Size = new System.Drawing.Size(47, 20);
            this.m_SpawnEffectBTime.TabIndex = 87;
            this.m_SpawnEffectBTime.Text = "1.000";
            this.m_SpawnEffectBTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_SpawnEffectBTime.Value = 1F;
            this.m_SpawnEffectBTime.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_ComboDrawType
            // 
            this.m_ComboDrawType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboDrawType.FormattingEnabled = true;
            this.m_ComboDrawType.Items.AddRange(new object[] {
            "None"});
            this.m_ComboDrawType.Location = new System.Drawing.Point(253, 97);
            this.m_ComboDrawType.Name = "m_ComboDrawType";
            this.m_ComboDrawType.Size = new System.Drawing.Size(176, 21);
            this.m_ComboDrawType.TabIndex = 67;
            this.m_ComboDrawType.SelectionChangeCommitted += new System.EventHandler(this.m_ComboDrawType_SelectionChangeCommitted);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(306, 198);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 13);
            this.label16.TabIndex = 84;
            this.label16.Text = "Trigger Ratio";
            // 
            // m_SpawnEffectB
            // 
            this.m_SpawnEffectB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_SpawnEffectB.Enabled = false;
            this.m_SpawnEffectB.FormattingEnabled = true;
            this.m_SpawnEffectB.Location = new System.Drawing.Point(135, 276);
            this.m_SpawnEffectB.Name = "m_SpawnEffectB";
            this.m_SpawnEffectB.Size = new System.Drawing.Size(165, 21);
            this.m_SpawnEffectB.Sorted = true;
            this.m_SpawnEffectB.TabIndex = 80;
            this.m_SpawnEffectB.DropDown += new System.EventHandler(this.BuildSpawnEffect_DropDown);
            this.m_SpawnEffectB.SelectionChangeCommitted += new System.EventHandler(this.SpawnEffect_SelectionChangeCommitted);
            // 
            // m_SpawnEffectATime
            // 
            this.m_SpawnEffectATime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SpawnEffectATime.Decimals = 3;
            this.m_SpawnEffectATime.Increment = 0.01F;
            this.m_SpawnEffectATime.Location = new System.Drawing.Point(380, 194);
            this.m_SpawnEffectATime.MaxValue = 1F;
            this.m_SpawnEffectATime.MinValue = 0F;
            this.m_SpawnEffectATime.Multiline = true;
            this.m_SpawnEffectATime.Name = "m_SpawnEffectATime";
            this.m_SpawnEffectATime.SetValue = 0F;
            this.m_SpawnEffectATime.Size = new System.Drawing.Size(47, 20);
            this.m_SpawnEffectATime.TabIndex = 85;
            this.m_SpawnEffectATime.Text = "0.000";
            this.m_SpawnEffectATime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_SpawnEffectATime.Value = 0F;
            this.m_SpawnEffectATime.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_Slider_OnValueChanged);
            // 
            // m_CheckDepthTest
            // 
            this.m_CheckDepthTest.AutoSize = true;
            this.m_CheckDepthTest.Checked = true;
            this.m_CheckDepthTest.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckDepthTest.Location = new System.Drawing.Point(6, 46);
            this.m_CheckDepthTest.Name = "m_CheckDepthTest";
            this.m_CheckDepthTest.Size = new System.Drawing.Size(79, 17);
            this.m_CheckDepthTest.TabIndex = 22;
            this.m_CheckDepthTest.Text = "Depth Test";
            this.m_CheckDepthTest.UseVisualStyleBackColor = true;
            this.m_CheckDepthTest.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_SpawnEffectBCheck
            // 
            this.m_SpawnEffectBCheck.AutoSize = true;
            this.m_SpawnEffectBCheck.Location = new System.Drawing.Point(4, 278);
            this.m_SpawnEffectBCheck.Name = "m_SpawnEffectBCheck";
            this.m_SpawnEffectBCheck.Size = new System.Drawing.Size(129, 17);
            this.m_SpawnEffectBCheck.TabIndex = 79;
            this.m_SpawnEffectBCheck.Text = "Effect Spawner (Coln)";
            this.m_SpawnEffectBCheck.UseVisualStyleBackColor = true;
            this.m_SpawnEffectBCheck.CheckedChanged += new System.EventHandler(this.m_SpawnEffectB_CheckedChanged);
            this.m_SpawnEffectBCheck.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_ComboBlendMode
            // 
            this.m_ComboBlendMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboBlendMode.FormattingEnabled = true;
            this.m_ComboBlendMode.Items.AddRange(new object[] {
            "Sphere",
            "Box",
            "Cylinder"});
            this.m_ComboBlendMode.Location = new System.Drawing.Point(253, 46);
            this.m_ComboBlendMode.Name = "m_ComboBlendMode";
            this.m_ComboBlendMode.Size = new System.Drawing.Size(176, 21);
            this.m_ComboBlendMode.TabIndex = 53;
            this.m_ComboBlendMode.SelectionChangeCommitted += new System.EventHandler(this.m_ComboBlendMode_SelectionChangeCommitted);
            // 
            // m_InheritLifeACheck
            // 
            this.m_InheritLifeACheck.AutoSize = true;
            this.m_InheritLifeACheck.Location = new System.Drawing.Point(133, 222);
            this.m_InheritLifeACheck.Name = "m_InheritLifeACheck";
            this.m_InheritLifeACheck.Size = new System.Drawing.Size(102, 17);
            this.m_InheritLifeACheck.TabIndex = 81;
            this.m_InheritLifeACheck.Text = "Inherit Point Life";
            this.m_InheritLifeACheck.UseVisualStyleBackColor = true;
            this.m_InheritLifeACheck.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_SpawnEffectACheck
            // 
            this.m_SpawnEffectACheck.AutoSize = true;
            this.m_SpawnEffectACheck.Location = new System.Drawing.Point(4, 197);
            this.m_SpawnEffectACheck.Name = "m_SpawnEffectACheck";
            this.m_SpawnEffectACheck.Size = new System.Drawing.Size(125, 17);
            this.m_SpawnEffectACheck.TabIndex = 78;
            this.m_SpawnEffectACheck.Text = "Effect Spawner (Life)";
            this.m_SpawnEffectACheck.UseVisualStyleBackColor = true;
            this.m_SpawnEffectACheck.CheckedChanged += new System.EventHandler(this.m_SpawnEffectA_CheckedChanged);
            this.m_SpawnEffectACheck.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_CheckDepthWrite
            // 
            this.m_CheckDepthWrite.AutoSize = true;
            this.m_CheckDepthWrite.Checked = true;
            this.m_CheckDepthWrite.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckDepthWrite.Location = new System.Drawing.Point(91, 46);
            this.m_CheckDepthWrite.Name = "m_CheckDepthWrite";
            this.m_CheckDepthWrite.Size = new System.Drawing.Size(83, 17);
            this.m_CheckDepthWrite.TabIndex = 21;
            this.m_CheckDepthWrite.Text = "Depth Write";
            this.m_CheckDepthWrite.UseVisualStyleBackColor = true;
            this.m_CheckDepthWrite.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_PanelHeader
            // 
            this.m_PanelHeader.Controls.Add(this.m_LabelHeader);
            this.m_PanelHeader.Location = new System.Drawing.Point(11, -1);
            this.m_PanelHeader.Name = "m_PanelHeader";
            this.m_PanelHeader.Size = new System.Drawing.Size(458, 26);
            this.m_PanelHeader.TabIndex = 5;
            // 
            // m_LabelHeader
            // 
            this.m_LabelHeader.AutoSize = true;
            this.m_LabelHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelHeader.Location = new System.Drawing.Point(-4, 4);
            this.m_LabelHeader.Name = "m_LabelHeader";
            this.m_LabelHeader.Size = new System.Drawing.Size(51, 22);
            this.m_LabelHeader.TabIndex = 2;
            this.m_LabelHeader.Text = "PTX:";
            // 
            // m_ContextBehavior
            // 
            this.m_ContextBehavior.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_MenuClearBehavior});
            this.m_ContextBehavior.Name = "m_ContextBehavior";
            this.m_ContextBehavior.Size = new System.Drawing.Size(151, 26);
            this.m_ContextBehavior.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContextBehavior_Opening);
            // 
            // m_MenuClearBehavior
            // 
            this.m_MenuClearBehavior.Name = "m_MenuClearBehavior";
            this.m_MenuClearBehavior.Size = new System.Drawing.Size(150, 22);
            this.m_MenuClearBehavior.Text = "Clear Behavior";
            this.m_MenuClearBehavior.Click += new System.EventHandler(this.m_MenuClearBehavior_Click);
            // 
            // m_TimerProfile
            // 
            this.m_TimerProfile.Interval = 250;
            this.m_TimerProfile.Tick += new System.EventHandler(this.m_TimerProfile_Tick);
            // 
            // PtxWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.m_PanelHeader);
            this.Controls.Add(this.m_PanelGroup);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "PtxWindow";
            this.Size = new System.Drawing.Size(472, 464);
            this.m_PanelGroup.ResumeLayout(false);
            this.m_PanelGroup.PerformLayout();
            this.m_PanelSection.ResumeLayout(false);
            this.m_SubSectionSpawnBOverrides.ResumeLayout(false);
            this.m_SubSectionSpawnAOverrides.ResumeLayout(false);
            this.m_SubSectionSprite.ResumeLayout(false);
            this.m_SubSectionSprite.PerformLayout();
            this.m_SubSectionModel.ResumeLayout(false);
            this.m_SubSectionModel.PerformLayout();
            this.m_ContextModelList.ResumeLayout(false);
            this.ptxSubSectionBiasLinks.ResumeLayout(false);
            this.ptxSubSectionBiasLinks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridLinkSet)).EndInit();
            this.m_ContexBiasSet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridBiasProps)).EndInit();
            this.m_ContextBiasProp.ResumeLayout(false);
            this.m_SubSectionShader.ResumeLayout(false);
            this.m_SubSectionPhysical.ResumeLayout(false);
            this.m_SubSectionPhysical.PerformLayout();
            this.m_SubSectionKeyframes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridPtxtKeyframeList)).EndInit();
            this.m_SubSectionSetup.ResumeLayout(false);
            this.m_SubSectionSetup.PerformLayout();
            this.m_PanelHeader.ResumeLayout(false);
            this.m_PanelHeader.PerformLayout();
            this.m_ContextBehavior.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel m_PanelGroup;
        private System.Windows.Forms.Panel m_PanelSection;
        private ptxSubSection m_SubSectionSetup;
        private System.Windows.Forms.Panel m_PanelHeader;
        private System.Windows.Forms.Label m_LabelHeader;
		private System.Windows.Forms.CheckBox m_CheckDepthWrite;
        private System.Windows.Forms.CheckBox m_CheckDepthTest;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox m_ComboBlendMode;
        private System.Windows.Forms.ComboBox m_ComboSortType;
		private System.Windows.Forms.ComboBox m_ComboDrawType;
        private ptxSubSection m_SubSectionModel;
        private ptxSubSection m_SubSectionKeyframes;
        private ptxSubSection ptxSubSectionBiasLinks;
		private System.Windows.Forms.ListBox m_ListModels;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ContextMenuStrip m_ContextModelList;
        private System.Windows.Forms.ToolStripMenuItem m_AddModelMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_DelModelMenuItem;
		private System.Windows.Forms.Label label7;
        private ptxSubSection m_SubSectionShader;
        private System.Windows.Forms.Panel m_ShaderPanel;
        private System.Windows.Forms.DataGridView m_DataGridPtxtKeyframeList;
        private System.Windows.Forms.ContextMenuStrip m_ContextMenuKeyProp;
        private System.Windows.Forms.ComboBox m_SpawnEffectB;
        private System.Windows.Forms.CheckBox m_SpawnEffectBCheck;
        private System.Windows.Forms.ComboBox m_SpawnEffectA;
        private System.Windows.Forms.CheckBox m_SpawnEffectACheck;
        private System.Windows.Forms.CheckBox m_InheritLifeACheck;
        private System.Windows.Forms.Label label10;
		private System.Windows.Forms.ComboBox m_ComboCullMode;
        private ptxSubSection m_SubSectionSpawnAOverrides;
		private EffectOverrides m_SpawnAOverrides;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView m_DataGridBiasProps;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ContextMenuStrip m_ContexBiasSet;
        private System.Windows.Forms.ToolStripMenuItem m_ContexAddSet;
        private System.Windows.Forms.ToolStripMenuItem m_ContexRemoveSet;
        private System.Windows.Forms.DataGridView m_DataGridLinkSet;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.ContextMenuStrip m_ContextBiasProp;
        private System.Windows.Forms.ToolStripMenuItem m_ContextAddBiasProp;
        private System.Windows.Forms.ToolStripMenuItem m_ContextRemoveBiasProp;
        private System.Windows.Forms.ToolStripMenuItem blankToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
		private System.Windows.Forms.DataGridViewCheckBoxColumn Column2;
        private System.Windows.Forms.CheckBox m_InheritLifeBCheck;
        private System.Windows.Forms.Label label17;
        private ptxSlider m_SpawnEffectBTime;
        private System.Windows.Forms.Label label16;
        private ptxSlider m_SpawnEffectATime;
        private ptxSubSection m_SubSectionSpawnBOverrides;
        private EffectOverrides m_SpawnBOverrides;
        private ptxSubSection m_SubSectionPhysical;
        private System.Windows.Forms.Label label18;
        private ptxSlider m_PhysicalRange;
        private System.Windows.Forms.CheckBox m_CheckOnCollideKill;
        private System.Windows.Forms.CheckBox m_CheckShowPhysics;
        private System.Windows.Forms.Label label19;
        private ptxSlider m_StopVel;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox m_AllowPosOverride;
        private System.Windows.Forms.CheckBox m_AllowColorOverride;
        private System.Windows.Forms.Label label21;
        private ptxSlider m_SlidePerPhysical;
        private System.Windows.Forms.Label label24;
		private ptxSlider m_SlidePerKill;
        private System.Windows.Forms.Label m_LabelDrawType;
        private System.Windows.Forms.ContextMenuStrip m_ContextBehavior;
        private System.Windows.Forms.ToolStripMenuItem m_MenuClearBehavior;
        private System.Windows.Forms.Timer m_TimerProfile;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_ColumnTextRuleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution5;
        private System.Windows.Forms.DataGridViewTextBoxColumn EvolutionLOD;
        private System.Windows.Forms.DataGridViewComboBoxColumn EvoBlend;
        private System.Windows.Forms.DataGridViewTextBoxColumn Invis;
        private System.Windows.Forms.DataGridViewTextBoxColumn Profile;
		private ptxSlider m_TexFrameIdMax;
		private ptxSlider m_TexFrameIdMin;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private ptxSubSection m_SubSectionSprite;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.ComboBox m_comboDiffuseMode;
		private System.Windows.Forms.CheckBox m_checkBoxIsScreenSpace;
		private System.Windows.Forms.CheckBox m_checkBoxIsSoft;
		private System.Windows.Forms.CheckBox m_checkBoxIsLit;
		private System.Windows.Forms.ComboBox m_ComboShader;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.CheckBox m_checkBoxIsRefract;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox m_TrackNegDirBCheck;
		private System.Windows.Forms.CheckBox m_TrackDirBCheck;
		private System.Windows.Forms.CheckBox m_TrackPosBCheck;
		private System.Windows.Forms.CheckBox m_TrackNegDirACheck;
		private System.Windows.Forms.CheckBox m_TrackDirACheck;
		private System.Windows.Forms.CheckBox m_TrackPosACheck;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.CheckBox m_checkBoxIsNormalSpec;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.ComboBox m_comboProjMode;

    }
}
