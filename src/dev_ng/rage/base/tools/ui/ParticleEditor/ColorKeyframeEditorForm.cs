using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
    public partial class ColorKeyframeEditorForm : KeyframeEditorFormBase
    {
        public ColorKeyframeEditorForm()
        {
            InitializeComponent();
           
            colorGradientEditor1.colorGradientControl1.OnPegColorChanged += OnValueChanged;
            colorGradientEditor1.colorGradientControl1.OnPegLocationChanged += OnValueChanged;
        }

        void OnValueChanged(object sender, ragKeyframeEditorControl.ColorGradientControl.ColorGradientEventArgs args)
        {
            DataMod.KeyframeData data = new DataMod.KeyframeData();
            List<ragKeyframeEditorControl.ColorGradientControl.Keyframe> keys = colorGradientEditor1.GetKeyframeData();
            foreach(ragKeyframeEditorControl.ColorGradientControl.Keyframe key in keys)
            {
                DataMod.KeyframeData.Key k = new DataMod.KeyframeData.Key();
                k.Time = key.m_Time;
                k.Data = new float[4];
                k.Data[0] = key.m_ColorRGB.x;
                k.Data[1] = key.m_ColorRGB.y;
                k.Data[2] = key.m_ColorRGB.z;
                k.Data[3] = key.m_ColorRGB.w;

                data.m_KeyData.Add(k);
            }

			DataCon.m_SDataCon.SendKeyframeData(m_RuleName, m_RuleType, m_PropertyId, m_EventIdx, m_EvoIdx, data);
        }

        public override void ConfigureEditor(DataMod.KeyframeUiData uidata)
        {
            colorGradientEditor1.MinRange = uidata.XMin;
            colorGradientEditor1.MaxRange = uidata.XMax;
            colorGradientEditor1.AllowEditAlpha = (uidata.Labels[3] != String.Empty);
        }

        public override void SetInitialData(DataMod.KeyframeData data)
        {
            List<ragKeyframeEditorControl.ColorGradientControl.Keyframe> keys = new List<ragKeyframeEditorControl.ColorGradientControl.Keyframe>();

            for (int i = 0; i < data.m_KeyData.Count; i++)
            {
                ragKeyframeEditorControl.ColorGradientControl.Vector4 rgb = new ragKeyframeEditorControl.ColorGradientControl.Vector4();
                rgb.Set(
                    data.m_KeyData[i].Data[0], 
                    data.m_KeyData[i].Data[1], 
                    data.m_KeyData[i].Data[2], 
                    data.m_KeyData[i].Data[3]);
                ragKeyframeEditorControl.ColorGradientControl.Keyframe key = new ragKeyframeEditorControl.ColorGradientControl.Keyframe();
                key.Set(data.m_KeyData[i].Time, rgb, ragKeyframeEditorControl.ColorGradientControl.RGBToHSB(rgb));
                keys.Add(key);
            }

            colorGradientEditor1.BuildFromData(keys);
        }


    }
}

