using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace ParticleEditor
{
    public partial class tcpComClient :UserControl
    {

#region Variables
        String m_AddressString = "1.1.1.1";
        IPAddress m_Address = IPAddress.Any;
        Int32 m_PortNumber = 16000;
        TcpClient m_Client = null;
        static bool m_Connected = false;

        int m_NumBytesToRead = 0;
        tcpByteBuffer m_CurPacket = null;
        tcpByteBuffer m_Ping = new tcpByteBuffer(8);
        
#endregion
        
#region Properties
        [Description("Time in ms between updates"), Category("TCP")]
        public int UpdateInterval
        {
            get
            {
                return m_UpdateTimer.Interval;
            }
            set
            {
                m_UpdateTimer.Interval = value;
            }
        }

        [Description("IP Address"), Category("TCP")]
        public String Address
        {
            get
            {
                return m_Address.ToString();
            }
            set
            {
                IPAddress addy = new IPAddress(m_Address.GetAddressBytes());
                if (IPAddress.TryParse(value, out addy))
                {
                    m_AddressString = addy.ToString();
                    m_Address = addy;
                    return;
                }
            }
        }

        [Description("Port number"), Category("TCP")]
        public Int32 Port
        {
            get
            {
                return m_PortNumber;
            }
            set
            {
                m_PortNumber = value;
            }
        }

        public delegate void tcpComClientEventDelegate(tcpComClient obj);
        public delegate void tcpComClientBufferEventDelegate(tcpByteBuffer buff);
        
        [Description("Event triggered when client connects"), Category("TCP")]
        public event tcpComClientEventDelegate OnConnect = null;

        [Description("Event triggered when client disconnects"), Category("TCP")]
        public event tcpComClientEventDelegate OnDisconnect = null;

        [Description("Event triggered when complete byte buffer is received"), Category("TCP")]
        public event tcpComClientBufferEventDelegate OnCompleteBuffer = null;



#endregion

        public tcpComClient()
        {
            InitializeComponent();
            Visible = false;
            //Set up Ping packet
            m_Ping.Write_u32(4);
            m_Ping.Write_u32(0);
        }
        public void Stop()
        {
            Disconnect();
        }

        public void Start()
        {
            if (m_Client != null)
                Disconnect();

            m_Client = new TcpClient();
            try
            {
                //m_Client.BeginConnect(m_Address,m_PortNumber,new AsyncCallback(OnClientConnect), m_Client);
                m_Client = new TcpClient();
                m_Client.Connect(new IPEndPoint(m_Address,m_PortNumber));
                if (m_Client.Connected)
                    Connected();
                else
                    Disconnect();
            }
            catch (SocketException sockEx)
            {
                System.Diagnostics.Debug.WriteLine(sockEx.ToString());
                Disconnect();
            }
        }

        public bool IsConnected()
        {
            return m_Connected;
        }

        public bool SendByteBuffer(tcpByteBuffer buff)
        {
            if(!m_Connected)
                return false;
            Debug.Assert(buff.IsReady(),"TCP byte buffer size does not match actual size");
            if (WriteData(buff.m_Storage, buff.GetLength()) > 0)
                return true;
            return false;
        }
        
        protected void Connected()
        {
            m_Connected = true;
            m_UpdateTimer.Start();
            m_PingTimer.Start();
            if (OnConnect != null)
                OnConnect(this);
        }

        protected void Disconnect()
        {
            m_Connected = false;
            m_UpdateTimer.Stop();
            m_PingTimer.Stop();
            if (m_Client.Client != null)
                m_Client.Client.Close();
            m_Client = null;
            m_CurPacket = null;
            if (OnDisconnect != null)
                OnDisconnect(this);
        }

        protected void Update(object sender, EventArgs e)
        {
            if (!m_Connected)
                return;
            if ((m_Client.GetStream() != null) && (m_Client.GetStream().DataAvailable))
            {
                if (m_CurPacket == null)
                {
                    BeginNewPacket();
                    ContinuePacket();
                }
                else
                    ContinuePacket();
            }
            try
            {
                if ((m_CurPacket != null) && (m_CurPacket.IsReady()))
                {
                    ProcessData();
                }
            }
            catch(Exception ex)
            {

            }

        }
        
        protected virtual void ProcessData()
        {
            if (OnCompleteBuffer != null)
                OnCompleteBuffer(m_CurPacket);
            m_CurPacket = null;
        }
        
        protected void BeginNewPacket()
        {
            if (!m_Connected) return;
            
            //Get size and create the cur packet
            m_CurPacket = new tcpByteBuffer(4);
            ReadData(m_CurPacket.m_Storage, 4, true);
            int size = (int)m_CurPacket.Read_u32();
            m_CurPacket = new tcpByteBuffer(size + 4);
            m_CurPacket.Write_u32((uint)size);
            m_NumBytesToRead = size;
            m_CurPacket.SetLength(4);
        }
       
        protected void ContinuePacket()
        {
            byte[] buff = new byte[m_NumBytesToRead];
            if (ReadData(buff, m_NumBytesToRead, true) <= 0)
                return;
            //Copy buffer into the packet
            foreach(byte b in buff)
                m_CurPacket.Write_u8(b);
        }

        public int ReadData(byte[] buffer, int numBytesToRead, bool readExact)
        {
            int numBytesRead = 0;
            try
            {
                do
                {
                    int thisRead = m_Client.Client.Receive(buffer, numBytesRead, numBytesToRead, SocketFlags.None);
                    numBytesRead += thisRead;
                    numBytesToRead -= thisRead;
                } while (readExact && numBytesToRead > 0);
                m_PingTimer.Start();
            }
            catch
            {
                Disconnect();
            }

            return numBytesRead;
        }

        public int WriteData(byte[] buffer, int numBytesToWrite)
        {
            try
            {
                m_Client.Client.Send(buffer, numBytesToWrite, SocketFlags.None);
                m_PingTimer.Start();
            }
            catch
            {
                Disconnect();
            }

            return numBytesToWrite;
        }

        private void m_PingTimer_Tick(object sender, EventArgs e)
        {
            //Check connection status via ping
            SendByteBuffer(m_Ping);
        }
    }
}
