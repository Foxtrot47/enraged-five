namespace ParticleEditor
{
    partial class ptxSubSection
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_Title = new System.Windows.Forms.Label();
            this.m_HeaderPanel = new System.Windows.Forms.Panel();
            this.m_ButtonExpand = new System.Windows.Forms.Button();
            this.m_HeaderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_Title
            // 
            this.m_Title.AutoSize = true;
            this.m_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_Title.ForeColor = System.Drawing.SystemColors.ControlText;
            this.m_Title.Location = new System.Drawing.Point(3, 3);
            this.m_Title.Name = "m_Title";
            this.m_Title.Size = new System.Drawing.Size(112, 13);
            this.m_Title.TabIndex = 0;
            this.m_Title.Text = "Sub Section Name";
            // 
            // m_HeaderPanel
            // 
            this.m_HeaderPanel.BackColor = System.Drawing.SystemColors.MenuBar;
            this.m_HeaderPanel.Controls.Add(this.m_ButtonExpand);
            this.m_HeaderPanel.Controls.Add(this.m_Title);
            this.m_HeaderPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_HeaderPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.m_HeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.m_HeaderPanel.Name = "m_HeaderPanel";
            this.m_HeaderPanel.Size = new System.Drawing.Size(291, 20);
            this.m_HeaderPanel.TabIndex = 1;
            this.m_HeaderPanel.DoubleClick += new System.EventHandler(this.m_HeaderPanel_DoubleClick);
            // 
            // m_ButtonExpand
            // 
            this.m_ButtonExpand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_ButtonExpand.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.m_ButtonExpand.Font = new System.Drawing.Font("Marlett", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ButtonExpand.Location = new System.Drawing.Point(273, 3);
            this.m_ButtonExpand.Name = "m_ButtonExpand";
            this.m_ButtonExpand.Size = new System.Drawing.Size(15, 15);
            this.m_ButtonExpand.TabIndex = 1;
            this.m_ButtonExpand.Text = "6";
            this.m_ButtonExpand.UseVisualStyleBackColor = true;
            this.m_ButtonExpand.MouseClick += new System.Windows.Forms.MouseEventHandler(this.m_Expand_MouseClick);
            // 
            // ptxSubSection
            // 
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.m_HeaderPanel);
            this.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Name = "ptxSubSection";
            this.Size = new System.Drawing.Size(291, 300);
            this.Layout += new System.Windows.Forms.LayoutEventHandler(this.ptxSubSection_Layout);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ptxSubSection_Paint);
            this.SizeChanged += new System.EventHandler(this.ptxSubSection_SizeChanged);
            this.ClientSizeChanged += new System.EventHandler(this.ptxSubSection_ClientSizeChanged);
            this.m_HeaderPanel.ResumeLayout(false);
            this.m_HeaderPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label m_Title;
        private System.Windows.Forms.Panel m_HeaderPanel;
        private System.Windows.Forms.Button m_ButtonExpand;
    }
}
