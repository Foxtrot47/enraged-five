namespace ParticleEditor
{
    partial class ProfileWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.panel1 = new System.Windows.Forms.Panel();
			this.m_ShowEffectNames = new System.Windows.Forms.CheckBox();
			this.m_LabelTotalGPUDrawTime = new System.Windows.Forms.Label();
			this.m_LabelTotalCPUDrawTime = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.m_LabelTotalCPUUpdateTime = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.m_LabelTotalCulledInstances = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.m_LabelTotalActiveInstances = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.m_LabelTotalPoints = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.m_ProfileTimer = new System.Windows.Forms.Timer(this.components);
			this.panel2 = new System.Windows.Forms.Panel();
			this.m_LabelEffectCPUDrawTime = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.m_LabelEffectCUPUpdateTime = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.m_LabelEffectCulledInstances = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.m_LabelEffectActiveInstances = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.m_LabelEffectTotalPoints = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.m_LabelEffect = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.label8 = new System.Windows.Forms.Label();
			this.m_LabelEmitterActiveInstances = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.m_LabelEmitterTotalPoints = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.m_LabelEmitterCPUDrawTime = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.m_LabelEmitterCPUUpdateTime = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.m_LabelEmitter = new System.Windows.Forms.Label();
			this.m_LabelPtxRule = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.m_LabelTotalbehaviorUpdateTime = new System.Windows.Forms.Label();
			this.panel4 = new System.Windows.Forms.Panel();
			this.m_LabelEmitterCPUSortTime = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel4.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.m_ShowEffectNames);
			this.panel1.Controls.Add(this.m_LabelTotalGPUDrawTime);
			this.panel1.Controls.Add(this.m_LabelTotalCPUDrawTime);
			this.panel1.Controls.Add(this.label15);
			this.panel1.Controls.Add(this.label9);
			this.panel1.Controls.Add(this.m_LabelTotalCPUUpdateTime);
			this.panel1.Controls.Add(this.label7);
			this.panel1.Controls.Add(this.m_LabelTotalCulledInstances);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.m_LabelTotalActiveInstances);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.m_LabelTotalPoints);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Margin = new System.Windows.Forms.Padding(2);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(203, 152);
			this.panel1.TabIndex = 11;
			// 
			// m_ShowEffectNames
			// 
			this.m_ShowEffectNames.AutoSize = true;
			this.m_ShowEffectNames.Location = new System.Drawing.Point(5, 124);
			this.m_ShowEffectNames.Name = "m_ShowEffectNames";
			this.m_ShowEffectNames.Size = new System.Drawing.Size(120, 17);
			this.m_ShowEffectNames.TabIndex = 22;
			this.m_ShowEffectNames.Text = "Show Effect Names";
			this.m_ShowEffectNames.UseVisualStyleBackColor = true;
			this.m_ShowEffectNames.CheckedChanged += new System.EventHandler(this.m_ShowEffectNames_CheckedChanged);
			// 
			// m_LabelTotalGPUDrawTime
			// 
			this.m_LabelTotalGPUDrawTime.Location = new System.Drawing.Point(133, 106);
			this.m_LabelTotalGPUDrawTime.Name = "m_LabelTotalGPUDrawTime";
			this.m_LabelTotalGPUDrawTime.Size = new System.Drawing.Size(58, 13);
			this.m_LabelTotalGPUDrawTime.TabIndex = 21;
			this.m_LabelTotalGPUDrawTime.Text = "0.000";
			this.m_LabelTotalGPUDrawTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// m_LabelTotalCPUDrawTime
			// 
			this.m_LabelTotalCPUDrawTime.Location = new System.Drawing.Point(133, 90);
			this.m_LabelTotalCPUDrawTime.Name = "m_LabelTotalCPUDrawTime";
			this.m_LabelTotalCPUDrawTime.Size = new System.Drawing.Size(58, 13);
			this.m_LabelTotalCPUDrawTime.TabIndex = 21;
			this.m_LabelTotalCPUDrawTime.Text = "0.000";
			this.m_LabelTotalCPUDrawTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(2, 106);
			this.label15.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(88, 13);
			this.label15.TabIndex = 20;
			this.label15.Text = "Draw Time (gpu):";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(2, 90);
			this.label9.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(88, 13);
			this.label9.TabIndex = 20;
			this.label9.Text = "Draw Time (cpu):";
			// 
			// m_LabelTotalCPUUpdateTime
			// 
			this.m_LabelTotalCPUUpdateTime.Location = new System.Drawing.Point(133, 74);
			this.m_LabelTotalCPUUpdateTime.Name = "m_LabelTotalCPUUpdateTime";
			this.m_LabelTotalCPUUpdateTime.Size = new System.Drawing.Size(58, 13);
			this.m_LabelTotalCPUUpdateTime.TabIndex = 19;
			this.m_LabelTotalCPUUpdateTime.Text = "0.000";
			this.m_LabelTotalCPUUpdateTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(2, 74);
			this.label7.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(98, 13);
			this.label7.TabIndex = 18;
			this.label7.Text = "Update Time (cpu):";
			// 
			// m_LabelTotalCulledInstances
			// 
			this.m_LabelTotalCulledInstances.Location = new System.Drawing.Point(136, 56);
			this.m_LabelTotalCulledInstances.Name = "m_LabelTotalCulledInstances";
			this.m_LabelTotalCulledInstances.Size = new System.Drawing.Size(55, 13);
			this.m_LabelTotalCulledInstances.TabIndex = 17;
			this.m_LabelTotalCulledInstances.Text = "0";
			this.m_LabelTotalCulledInstances.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(2, 56);
			this.label5.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(88, 13);
			this.label5.TabIndex = 16;
			this.label5.Text = "Culled Instances:";
			// 
			// m_LabelTotalActiveInstances
			// 
			this.m_LabelTotalActiveInstances.Location = new System.Drawing.Point(133, 40);
			this.m_LabelTotalActiveInstances.Name = "m_LabelTotalActiveInstances";
			this.m_LabelTotalActiveInstances.Size = new System.Drawing.Size(58, 13);
			this.m_LabelTotalActiveInstances.TabIndex = 15;
			this.m_LabelTotalActiveInstances.Text = "0";
			this.m_LabelTotalActiveInstances.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(2, 40);
			this.label3.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(89, 13);
			this.label3.TabIndex = 14;
			this.label3.Text = "Active Instances:";
			// 
			// m_LabelTotalPoints
			// 
			this.m_LabelTotalPoints.Location = new System.Drawing.Point(130, 24);
			this.m_LabelTotalPoints.Name = "m_LabelTotalPoints";
			this.m_LabelTotalPoints.Size = new System.Drawing.Size(61, 13);
			this.m_LabelTotalPoints.TabIndex = 13;
			this.m_LabelTotalPoints.Text = "0";
			this.m_LabelTotalPoints.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(2, 24);
			this.label2.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(66, 13);
			this.label2.TabIndex = 12;
			this.label2.Text = "Total Points:";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(2, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(93, 13);
			this.label1.TabIndex = 11;
			this.label1.Text = "Global Profiling";
			// 
			// m_ProfileTimer
			// 
			this.m_ProfileTimer.Interval = 250;
			this.m_ProfileTimer.Tick += new System.EventHandler(this.m_ProfileTimer_Tick);
			// 
			// panel2
			// 
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel2.Controls.Add(this.m_LabelEffectCPUDrawTime);
			this.panel2.Controls.Add(this.label6);
			this.panel2.Controls.Add(this.m_LabelEffectCUPUpdateTime);
			this.panel2.Controls.Add(this.label10);
			this.panel2.Controls.Add(this.m_LabelEffectCulledInstances);
			this.panel2.Controls.Add(this.label12);
			this.panel2.Controls.Add(this.m_LabelEffectActiveInstances);
			this.panel2.Controls.Add(this.label14);
			this.panel2.Controls.Add(this.m_LabelEffectTotalPoints);
			this.panel2.Controls.Add(this.label16);
			this.panel2.Controls.Add(this.m_LabelEffect);
			this.panel2.Location = new System.Drawing.Point(0, 156);
			this.panel2.Margin = new System.Windows.Forms.Padding(2);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(203, 115);
			this.panel2.TabIndex = 12;
			// 
			// m_LabelEffectCPUDrawTime
			// 
			this.m_LabelEffectCPUDrawTime.Location = new System.Drawing.Point(133, 90);
			this.m_LabelEffectCPUDrawTime.Name = "m_LabelEffectCPUDrawTime";
			this.m_LabelEffectCPUDrawTime.Size = new System.Drawing.Size(58, 13);
			this.m_LabelEffectCPUDrawTime.TabIndex = 21;
			this.m_LabelEffectCPUDrawTime.Text = "0.000";
			this.m_LabelEffectCPUDrawTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(2, 90);
			this.label6.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(88, 13);
			this.label6.TabIndex = 20;
			this.label6.Text = "Draw Time (cpu):";
			// 
			// m_LabelEffectCUPUpdateTime
			// 
			this.m_LabelEffectCUPUpdateTime.Location = new System.Drawing.Point(133, 74);
			this.m_LabelEffectCUPUpdateTime.Name = "m_LabelEffectCUPUpdateTime";
			this.m_LabelEffectCUPUpdateTime.Size = new System.Drawing.Size(58, 13);
			this.m_LabelEffectCUPUpdateTime.TabIndex = 19;
			this.m_LabelEffectCUPUpdateTime.Text = "0.000";
			this.m_LabelEffectCUPUpdateTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(2, 74);
			this.label10.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(98, 13);
			this.label10.TabIndex = 18;
			this.label10.Text = "Update Time (cpu):";
			// 
			// m_LabelEffectCulledInstances
			// 
			this.m_LabelEffectCulledInstances.Location = new System.Drawing.Point(133, 56);
			this.m_LabelEffectCulledInstances.Name = "m_LabelEffectCulledInstances";
			this.m_LabelEffectCulledInstances.Size = new System.Drawing.Size(58, 13);
			this.m_LabelEffectCulledInstances.TabIndex = 17;
			this.m_LabelEffectCulledInstances.Text = "0";
			this.m_LabelEffectCulledInstances.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(2, 56);
			this.label12.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(88, 13);
			this.label12.TabIndex = 16;
			this.label12.Text = "Culled Instances:";
			// 
			// m_LabelEffectActiveInstances
			// 
			this.m_LabelEffectActiveInstances.Location = new System.Drawing.Point(133, 40);
			this.m_LabelEffectActiveInstances.Name = "m_LabelEffectActiveInstances";
			this.m_LabelEffectActiveInstances.Size = new System.Drawing.Size(58, 13);
			this.m_LabelEffectActiveInstances.TabIndex = 15;
			this.m_LabelEffectActiveInstances.Text = "0";
			this.m_LabelEffectActiveInstances.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(2, 40);
			this.label14.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(89, 13);
			this.label14.TabIndex = 14;
			this.label14.Text = "Active Instances:";
			// 
			// m_LabelEffectTotalPoints
			// 
			this.m_LabelEffectTotalPoints.Location = new System.Drawing.Point(133, 24);
			this.m_LabelEffectTotalPoints.Name = "m_LabelEffectTotalPoints";
			this.m_LabelEffectTotalPoints.Size = new System.Drawing.Size(58, 13);
			this.m_LabelEffectTotalPoints.TabIndex = 13;
			this.m_LabelEffectTotalPoints.Text = "0";
			this.m_LabelEffectTotalPoints.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(2, 24);
			this.label16.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(66, 13);
			this.label16.TabIndex = 12;
			this.label16.Text = "Total Points:";
			// 
			// m_LabelEffect
			// 
			this.m_LabelEffect.AutoSize = true;
			this.m_LabelEffect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_LabelEffect.Location = new System.Drawing.Point(2, 8);
			this.m_LabelEffect.Name = "m_LabelEffect";
			this.m_LabelEffect.Size = new System.Drawing.Size(91, 13);
			this.m_LabelEffect.TabIndex = 11;
			this.m_LabelEffect.Text = "Effect Profiling";
			// 
			// panel3
			// 
			this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel3.Controls.Add(this.m_LabelEmitterCPUSortTime);
			this.panel3.Controls.Add(this.label8);
			this.panel3.Controls.Add(this.m_LabelEmitterActiveInstances);
			this.panel3.Controls.Add(this.label4);
			this.panel3.Controls.Add(this.m_LabelEmitterTotalPoints);
			this.panel3.Controls.Add(this.label18);
			this.panel3.Controls.Add(this.m_LabelEmitterCPUDrawTime);
			this.panel3.Controls.Add(this.label11);
			this.panel3.Controls.Add(this.m_LabelEmitterCPUUpdateTime);
			this.panel3.Controls.Add(this.label13);
			this.panel3.Controls.Add(this.m_LabelEmitter);
			this.panel3.Location = new System.Drawing.Point(0, 275);
			this.panel3.Margin = new System.Windows.Forms.Padding(2);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(203, 114);
			this.panel3.TabIndex = 22;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(3, 88);
			this.label8.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(82, 13);
			this.label8.TabIndex = 26;
			this.label8.Text = "Sort Time (cpu):";
			// 
			// m_LabelEmitterActiveInstances
			// 
			this.m_LabelEmitterActiveInstances.Location = new System.Drawing.Point(133, 40);
			this.m_LabelEmitterActiveInstances.Name = "m_LabelEmitterActiveInstances";
			this.m_LabelEmitterActiveInstances.Size = new System.Drawing.Size(58, 13);
			this.m_LabelEmitterActiveInstances.TabIndex = 25;
			this.m_LabelEmitterActiveInstances.Text = "0";
			this.m_LabelEmitterActiveInstances.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(2, 40);
			this.label4.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(89, 13);
			this.label4.TabIndex = 24;
			this.label4.Text = "Active Instances:";
			// 
			// m_LabelEmitterTotalPoints
			// 
			this.m_LabelEmitterTotalPoints.Location = new System.Drawing.Point(133, 24);
			this.m_LabelEmitterTotalPoints.Name = "m_LabelEmitterTotalPoints";
			this.m_LabelEmitterTotalPoints.Size = new System.Drawing.Size(58, 13);
			this.m_LabelEmitterTotalPoints.TabIndex = 23;
			this.m_LabelEmitterTotalPoints.Text = "0";
			this.m_LabelEmitterTotalPoints.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(2, 24);
			this.label18.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(66, 13);
			this.label18.TabIndex = 22;
			this.label18.Text = "Total Points:";
			// 
			// m_LabelEmitterCPUDrawTime
			// 
			this.m_LabelEmitterCPUDrawTime.Location = new System.Drawing.Point(134, 72);
			this.m_LabelEmitterCPUDrawTime.Name = "m_LabelEmitterCPUDrawTime";
			this.m_LabelEmitterCPUDrawTime.Size = new System.Drawing.Size(58, 13);
			this.m_LabelEmitterCPUDrawTime.TabIndex = 21;
			this.m_LabelEmitterCPUDrawTime.Text = "0.000";
			this.m_LabelEmitterCPUDrawTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(3, 72);
			this.label11.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(88, 13);
			this.label11.TabIndex = 20;
			this.label11.Text = "Draw Time (cpu):";
			// 
			// m_LabelEmitterCPUUpdateTime
			// 
			this.m_LabelEmitterCPUUpdateTime.Location = new System.Drawing.Point(134, 56);
			this.m_LabelEmitterCPUUpdateTime.Name = "m_LabelEmitterCPUUpdateTime";
			this.m_LabelEmitterCPUUpdateTime.Size = new System.Drawing.Size(58, 13);
			this.m_LabelEmitterCPUUpdateTime.TabIndex = 19;
			this.m_LabelEmitterCPUUpdateTime.Text = "0.000";
			this.m_LabelEmitterCPUUpdateTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(3, 56);
			this.label13.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(98, 13);
			this.label13.TabIndex = 18;
			this.label13.Text = "Update Time (cpu):";
			// 
			// m_LabelEmitter
			// 
			this.m_LabelEmitter.AutoSize = true;
			this.m_LabelEmitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_LabelEmitter.Location = new System.Drawing.Point(2, 8);
			this.m_LabelEmitter.Name = "m_LabelEmitter";
			this.m_LabelEmitter.Size = new System.Drawing.Size(96, 13);
			this.m_LabelEmitter.TabIndex = 11;
			this.m_LabelEmitter.Text = "Emitter Profiling";
			// 
			// m_LabelPtxRule
			// 
			this.m_LabelPtxRule.AutoSize = true;
			this.m_LabelPtxRule.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_LabelPtxRule.Location = new System.Drawing.Point(2, 8);
			this.m_LabelPtxRule.Name = "m_LabelPtxRule";
			this.m_LabelPtxRule.Size = new System.Drawing.Size(101, 13);
			this.m_LabelPtxRule.TabIndex = 11;
			this.m_LabelPtxRule.Text = "PtxRule Profiling";
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(3, 24);
			this.label20.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(105, 13);
			this.label20.TabIndex = 22;
			this.label20.Text = "Total Behavior Time:";
			// 
			// m_LabelTotalbehaviorUpdateTime
			// 
			this.m_LabelTotalbehaviorUpdateTime.Location = new System.Drawing.Point(134, 24);
			this.m_LabelTotalbehaviorUpdateTime.Name = "m_LabelTotalbehaviorUpdateTime";
			this.m_LabelTotalbehaviorUpdateTime.Size = new System.Drawing.Size(58, 13);
			this.m_LabelTotalbehaviorUpdateTime.TabIndex = 23;
			this.m_LabelTotalbehaviorUpdateTime.Text = "0.000";
			this.m_LabelTotalbehaviorUpdateTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// panel4
			// 
			this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel4.Controls.Add(this.m_LabelTotalbehaviorUpdateTime);
			this.panel4.Controls.Add(this.label20);
			this.panel4.Controls.Add(this.m_LabelPtxRule);
			this.panel4.Location = new System.Drawing.Point(0, 393);
			this.panel4.Margin = new System.Windows.Forms.Padding(2);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(203, 48);
			this.panel4.TabIndex = 23;
			// 
			// m_LabelEmitterCPUSortTime
			// 
			this.m_LabelEmitterCPUSortTime.Location = new System.Drawing.Point(133, 88);
			this.m_LabelEmitterCPUSortTime.Name = "m_LabelEmitterCPUSortTime";
			this.m_LabelEmitterCPUSortTime.Size = new System.Drawing.Size(58, 13);
			this.m_LabelEmitterCPUSortTime.TabIndex = 27;
			this.m_LabelEmitterCPUSortTime.Text = "0.000";
			this.m_LabelEmitterCPUSortTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// ProfileWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(204, 443);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ProfileWindow";
			this.Text = "Profiling";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProfileWindow_FormClosing);
			this.VisibleChanged += new System.EventHandler(this.ProfileWindow_VisibleChanged);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.panel4.ResumeLayout(false);
			this.panel4.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label m_LabelTotalCPUDrawTime;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label m_LabelTotalCPUUpdateTime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label m_LabelTotalCulledInstances;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label m_LabelTotalActiveInstances;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label m_LabelTotalPoints;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer m_ProfileTimer;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label m_LabelEffectCPUDrawTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label m_LabelEffectCUPUpdateTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label m_LabelEffectCulledInstances;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label m_LabelEffectActiveInstances;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label m_LabelEffectTotalPoints;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label m_LabelEffect;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label m_LabelEmitterCPUUpdateTime;
        private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label m_LabelEmitter;
        private System.Windows.Forms.Label m_LabelEmitterTotalPoints;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label m_LabelEmitterCPUDrawTime;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox m_ShowEffectNames;
        private System.Windows.Forms.Label m_LabelTotalGPUDrawTime;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label m_LabelEmitterActiveInstances;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label m_LabelPtxRule;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label m_LabelTotalbehaviorUpdateTime;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label m_LabelEmitterCPUSortTime;

    }
}
