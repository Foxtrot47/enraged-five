using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
    public partial class BehaviorWindow : ParticleEditor.ptxForm
    {
        public DataMod.dtaPtxBehavior m_Behavior = null;
        public event BehaviorWindowBase.BehaviorWindowEvent OnBehaviorChanged = null;
        public event BehaviorWindowBase.BehaviorWindowEvent OnClosedWindow = null;

        public BehaviorWindow()
        {
            InitializeComponent();
            m_DataGridPtxtKeyframeList.ColumnHeadersVisible = false;
        }

        public void PrepWindow(DataMod.dtaPtxBehavior prop)
        {
            bool bNoTuningData = true;

            m_Behavior = prop;
            Text = "Behavior: " + "[" + prop.m_PtxRule.m_Name + "] " + m_Behavior.m_BehaviorName;
            m_Name = Text;
            m_WindowID = GetType().ToString() + " " + m_Behavior.m_BehaviorName;
            Persistant = false;

            main.m_MainForm.BuildKeyframeGrid(m_DataGridPtxtKeyframeList, m_Behavior.m_Keyframes);

            //Evolution
            DisplayEvolutionData(main.m_MainForm.m_EffectWindow.m_CurrentEvent);

            //Build the dynamic data
            m_PanelOptions.SuspendLayout();
            m_PanelOptions.Controls.Clear();
            if(prop.m_Vars!=null)
            {
                Point position = new Point(3, 3);
                int nextColumn = 0;

                foreach (DataMod.dtaBehaviorVar var in prop.m_Vars)
                {
                    bNoTuningData = false;
                    if (var.m_Type == "bool")
                    {
                        // Setup checkbox.
                        CheckBox option = new CheckBox();
                        option.AutoSize = true;
                        option.Text = var.m_Name;

                        // Expand the X location of the next row, if need be.
                        int rightExtent = position.X + option.Width;
                        if (rightExtent > nextColumn)
                            nextColumn = rightExtent;
                        option.Checked = var.m_Bool;
                        option.Location = position;
                        option.Tag = var;

                        // Set the checkbox.
                        option.Click += new EventHandler(OptionChanged_Click);
                        m_PanelOptions.Controls.Add(option);
                        position.Y += option.Height;
                    }
                    else if (var.m_Type == "float")
                    {
                        // Setup label.
                        Label label = new Label();
                        label.AutoSize = true;
                        label.Text = var.m_Name;
                        label.Location = position;

                        // Setup databox.
                        ptxSlider option = new ptxSlider();
                        option.Decimals = 3;
                        option.Increment = 0.001f;
                        option.MinValue = var.m_MinRange;
                        option.MaxValue = var.m_MaxRange;
                        option.SetValue = var.m_Float;
                        option.Location = new Point(label.Location.X + label.Width + 60 /* +60?? */, label.Location.Y);
                        option.Tag = var;

                        // Expand the X location of the next row, if need be.
                        int rightExtent = label.Location.X + label.Width + option.Width;
                        if (rightExtent > nextColumn)
                            nextColumn = rightExtent;

                        // Set the label.
                        m_PanelOptions.Controls.Add(label);

                        // Set the databox.
                        option.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(OptionChanged_Float);
                        m_PanelOptions.Controls.Add(option);
                        position.Y += option.Height;
                    }
                    else if (var.m_Type == "int")
                    {
                        // Setup label.
                        Label label = new Label();
                        label.AutoSize = true;
                        label.Text = var.m_Name;
                        label.Location = position;

                        // Setup databox.
                        ptxSlider option = new ptxSlider();
                        option.Decimals = 0;
                        option.Increment = 1;
                        option.MinValue = var.m_MinRange;
                        option.MaxValue = var.m_MaxRange;
                        option.SetValue = var.m_Int;
                        option.Location = new Point(label.Location.X + label.Width + 60 /* +60?? */, label.Location.Y);
                        option.Tag = var;

                        // Expand the X location of the next row, if need be.
                        int rightExtent = label.Location.X + label.Width + option.Width;
                        if (rightExtent > nextColumn)
                            nextColumn = rightExtent;

                        // Set the label.
                        m_PanelOptions.Controls.Add(label);

                        // Set the databox.
                        option.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(OptionChanged_Int);
                        m_PanelOptions.Controls.Add(option);
                        position.Y += option.Height;
                    }
					else if (var.m_Type == "combo")
					{
						// Setup label.
						Label label = new Label();
						label.AutoSize = true;
						label.Text = var.m_Name;
						label.Location = position;

						// Setup databox.
						ptxCombo option = new ptxCombo();
						//option.Decimals = 0;
						//option.Increment = 1;
						//option.MinValue = var.m_MinRange;
						//option.MaxValue = var.m_MaxRange;
						//option.SetValue = var.m_Int;

						option.Items.Clear();
						for (int i = 0; i < var.m_ComboNumItems; i++ )
						{
							option.Items.Add(var.m_ComboItemNames[i]);
						}

						option.SelectedIndex = var.m_Int;

						option.Location = new Point(label.Location.X + label.Width + 60 /* +60?? */, label.Location.Y);
						option.Tag = var;

						// Expand the X location of the next row, if need be.
						int rightExtent = label.Location.X + label.Width + option.Width;
						if (rightExtent > nextColumn)
							nextColumn = rightExtent;

						// Set the label.
						m_PanelOptions.Controls.Add(label);

						// Set the databox.
						option.OnSelectedIndexChanged_Custom += new ptxCombo.ptxComboEventDelegate(OptionChanged_Combo);
						m_PanelOptions.Controls.Add(option);
						position.Y += option.Height;
					}
                    
                    // Update global positioner.
                    if (position.Y > m_PanelOptions.Height)
                    {
                        position.X = nextColumn + 115; 
                        position.Y = 3;
                    }
                }
            }

            // If there's no tuning data, say so.
            if( bNoTuningData )
            {
                Label label = new Label();
                label.Text = "No tuning data.";
                m_PanelOptions.Controls.Add(label);
            }
            m_PanelOptions.ResumeLayout();
        }
        
        private void DisplayEvolutionData(DataMod.dtaEvent doev)
        {
            if (doev == null) return;
            if (doev is DataMod.dtaEventEmitter)
                if ((doev as DataMod.dtaEventEmitter).m_PtxRuleName == m_Behavior.m_PtxRule.m_Name)
                    main.m_MainForm.DisplayEvolutionData(m_DataGridPtxtKeyframeList, doev.m_EvoList); 
        }

        private DataMod.KeyframeSpec GetSelectedKeyframeFromDataGrid(DataGridView grid)
        {
            if (grid.CurrentRow == null) return null;
            DataMod.KeyframeSpec key = grid.CurrentRow.Cells[0].Tag as DataMod.KeyframeSpec;
            return key;

        }
        private void SelectPropertyFromDataGrid(DataGridView grid, String propName)
        {
            if (propName == "")
            {
                grid.CurrentCell = null;
                return;
            }

            foreach (DataGridViewRow row in grid.Rows)
            {
                if (row.Cells[0].Value.ToString() == propName)
                {
                    grid.CurrentCell = row.Cells[0];
                    return;
                }
            }
            grid.CurrentCell = null;
        }
        
        private void ShowKeyframe(object sender, EventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid.CurrentCell == null) return;
            int cdx = grid.CurrentCell.ColumnIndex;


            String desc = "PtxRule: " + m_Behavior.m_PtxRule.m_Name;
            if ((cdx > 0) && (cdx <= main.m_MainForm.m_EffectWindow.m_CurrentEvent.m_EvoList.m_Evolutions.Count))
            {
                String evoname = (main.m_MainForm.m_EffectWindow.m_CurrentEvent.m_EvoList.m_Evolutions[cdx - 1] as DataMod.dtaEvolution).m_Name;
                desc += ": Evolution (" + evoname + ")";
            }
            KeyframeEditorFormBase.ShowKeyframe(GetSelectedKeyframeCellFromDataGrid(sender as DataGridView), desc + ":");
        }
        private DataMod.KeyframeSpec GetSelectedKeyframeCellFromDataGrid(DataGridView grid)
        {
            if (grid.CurrentCell == null) return null;
            DataMod.KeyframeSpec key = grid.CurrentCell.Tag as DataMod.KeyframeSpec;
            return key;

        }

        private void m_DataGridPtxtKeyframeList_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataMod.guiPropEventData guievt = new DataMod.guiPropEventData();
            if (main.m_MainForm != null)
                main.m_MainForm.PropertyDataGrid_CellValueChanged(sender, guievt, e);
            if (guievt.InfoChanged)
            {
                if (main.m_MainForm != null)
                    main.m_MainForm.m_EffectWindow.ChangedEffect(main.m_MainForm.m_EffectWindow.m_CurrentEffectName);
            }
        }
        private void DataGridAutoSelectValidCell(object sender, EventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid.CurrentCell == null) return;
            if (grid.CurrentCell.Value == null)
                grid.CurrentCell = grid.Rows[grid.CurrentCell.RowIndex].Cells[0];
        }
        private void m_DataGridPtxtKeyframeList_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            main.m_MainForm.PropertyDataGrid_CurrentCellDirtyStateChanged(sender, e);
        }
        private void SelectOnRightMouseButton_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                DataGridView grid = sender as DataGridView;
                grid.CurrentCell = grid.Rows[e.RowIndex].Cells[e.ColumnIndex];
            }

        }
        private void m_ContextMenuKeyProps_Opening(object sender, CancelEventArgs e)
        {
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;
            if (doe == null) { e.Cancel = true; return; }

            DataMod.dtaPtxKeyframeProp keyProp = main.m_MainForm.GetSelectedKeyframePropFromDataGrid(m_DataGridPtxtKeyframeList);
            if (keyProp == null) { e.Cancel = true; return; }

            if (doe is DataMod.dtaEventEmitter)
            {
                if ((doe as DataMod.dtaEventEmitter).m_PtxRuleName == m_Behavior.m_PtxRule.m_Name)
                    main.m_MainForm.GenerateKeyPropEvoMenu("PTX", m_ContextMenuKeyProp, doe.m_EvoList, keyProp, new EventHandler(KeyProp_Menu_AddEvo_Click), new EventHandler(KeyProp_Menu_DelEvo_Click));
                else
                    main.m_MainForm.GenerateKeyPropMenu("PTX", m_ContextMenuKeyProp, keyProp);
            }

            e.Cancel = false;
        }

        private void KeyProp_Menu_AddEvo_Click(object sender, EventArgs e)
        {
            String effectName = main.m_MainForm.m_EffectWindow.m_CurrentEffectName;
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;

            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            String evoname = item.Tag as String;
            DataMod.dtaPtxKeyframeProp keyprop = m_ContextMenuKeyProp.Tag as DataMod.dtaPtxKeyframeProp;
            if (keyprop == null) return;
            DataCon.m_SDataCon.SendAddPropEvolutionEvent(effectName, evoname,doe.m_Index, keyprop);
        }
        private void KeyProp_Menu_DelEvo_Click(object sender, EventArgs e)
        {
            String effectName = main.m_MainForm.m_EffectWindow.m_CurrentEffectName;
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;

            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            String evoname = item.Tag as String;
            DataMod.dtaPtxKeyframeProp keyprop = m_ContextMenuKeyProp.Tag as DataMod.dtaPtxKeyframeProp;
            if (keyprop == null) return;
            DataCon.m_SDataCon.SendDelPropEvolutionEvent(effectName, evoname,doe.m_Index, keyprop);
        }

        private void OptionChanged_Click(object sender, EventArgs e)
        {
            if((sender != null) && (sender is CheckBox))
            {
                CheckBox box = sender as CheckBox;
                DataMod.dtaBehaviorVar var = box.Tag as DataMod.dtaBehaviorVar;
                var.m_Bool = box.Checked;
            }
            if (OnBehaviorChanged != null)
                OnBehaviorChanged(sender, new BehaviorWindowBase.BehaviorChangedEventArgs(m_Behavior));
        }

        private void OptionChanged_Float(ptxSlider slider)
        {
            if (slider != null)
            {
                DataMod.dtaBehaviorVar var = slider.Tag as DataMod.dtaBehaviorVar;
                var.m_Float = (float)Convert.ToDouble(slider.Text);
            }
            if (OnBehaviorChanged != null)
                OnBehaviorChanged(slider, new BehaviorWindowBase.BehaviorChangedEventArgs(m_Behavior));
        }

        private void OptionChanged_Int(ptxSlider slider)
        {
            if (slider != null)
            {
                DataMod.dtaBehaviorVar var = slider.Tag as DataMod.dtaBehaviorVar;
                var.m_Int = Convert.ToInt32(slider.Text);
            }
            if (OnBehaviorChanged != null)
                OnBehaviorChanged(slider, new BehaviorWindowBase.BehaviorChangedEventArgs(m_Behavior));
        }

		private void OptionChanged_Combo(ptxCombo combo)
		{
			if (combo != null)
			{
				DataMod.dtaBehaviorVar var = combo.Tag as DataMod.dtaBehaviorVar;
				var.m_Int = Convert.ToInt32(combo.SelectedIndex);
			}
			if (OnBehaviorChanged != null)
				OnBehaviorChanged(combo, new BehaviorWindowBase.BehaviorChangedEventArgs(m_Behavior));
		}

        private void BehaviorWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (OnClosedWindow != null)
                OnClosedWindow(sender, new BehaviorWindowBase.BehaviorChangedEventArgs(m_Behavior));
        }
    }
}