namespace ParticleEditor
{
    partial class EffectOverrides
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.m_DurationCheck = new System.Windows.Forms.CheckBox();
			this.m_PlaybackSpeedCheck = new System.Windows.Forms.CheckBox();
			this.m_ColorTintCheck = new System.Windows.Forms.CheckBox();
			this.m_ZoomCheck = new System.Windows.Forms.CheckBox();
			this.m_SizeScaleCheck = new System.Windows.Forms.CheckBox();
			this.m_ZoomMinValue = new ParticleEditor.ptxSlider();
			this.m_ZoomMaxValue = new ParticleEditor.ptxSlider();
			this.m_PlaybackSpeedMinValue = new ParticleEditor.ptxSlider();
			this.m_PlaybackSpeedMaxValue = new ParticleEditor.ptxSlider();
			this.m_DurationMinValue = new ParticleEditor.ptxSlider();
			this.m_DurationMaxValue = new ParticleEditor.ptxSlider();
			this.m_SizeYMinValue = new ParticleEditor.ptxSlider();
			this.m_SizeXMinValue = new ParticleEditor.ptxSlider();
			this.m_SizeZMinValue = new ParticleEditor.ptxSlider();
			this.m_SizeZMaxValue = new ParticleEditor.ptxSlider();
			this.m_SizeXMaxValue = new ParticleEditor.ptxSlider();
			this.m_SizeYMaxValue = new ParticleEditor.ptxSlider();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.m_ColorMinButton = new System.Windows.Forms.Button();
			this.m_ColorMaxButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// m_DurationCheck
			// 
			this.m_DurationCheck.AutoSize = true;
			this.m_DurationCheck.Location = new System.Drawing.Point(4, 4);
			this.m_DurationCheck.Name = "m_DurationCheck";
			this.m_DurationCheck.Size = new System.Drawing.Size(66, 17);
			this.m_DurationCheck.TabIndex = 0;
			this.m_DurationCheck.Text = "Duration";
			this.m_DurationCheck.UseVisualStyleBackColor = true;
			this.m_DurationCheck.CheckedChanged += new System.EventHandler(this.m_DurationCheck_CheckedChanged);
			// 
			// m_PlaybackSpeedCheck
			// 
			this.m_PlaybackSpeedCheck.AutoSize = true;
			this.m_PlaybackSpeedCheck.Location = new System.Drawing.Point(4, 28);
			this.m_PlaybackSpeedCheck.Name = "m_PlaybackSpeedCheck";
			this.m_PlaybackSpeedCheck.Size = new System.Drawing.Size(104, 17);
			this.m_PlaybackSpeedCheck.TabIndex = 3;
			this.m_PlaybackSpeedCheck.Text = "Playback Speed";
			this.m_PlaybackSpeedCheck.UseVisualStyleBackColor = true;
			this.m_PlaybackSpeedCheck.CheckedChanged += new System.EventHandler(this.m_PlaybackSpeedCheck_CheckedChanged);
			// 
			// m_ColorTintCheck
			// 
			this.m_ColorTintCheck.AutoSize = true;
			this.m_ColorTintCheck.Location = new System.Drawing.Point(4, 52);
			this.m_ColorTintCheck.Name = "m_ColorTintCheck";
			this.m_ColorTintCheck.Size = new System.Drawing.Size(68, 17);
			this.m_ColorTintCheck.TabIndex = 6;
			this.m_ColorTintCheck.Text = "ColorTint";
			this.m_ColorTintCheck.UseVisualStyleBackColor = true;
			this.m_ColorTintCheck.CheckedChanged += new System.EventHandler(this.m_ColorTintCheck_CheckedChanged);
			// 
			// m_ZoomCheck
			// 
			this.m_ZoomCheck.AutoSize = true;
			this.m_ZoomCheck.Location = new System.Drawing.Point(4, 76);
			this.m_ZoomCheck.Name = "m_ZoomCheck";
			this.m_ZoomCheck.Size = new System.Drawing.Size(53, 17);
			this.m_ZoomCheck.TabIndex = 7;
			this.m_ZoomCheck.Text = "Zoom";
			this.m_ZoomCheck.UseVisualStyleBackColor = true;
			this.m_ZoomCheck.CheckedChanged += new System.EventHandler(this.m_ZoomCheck_CheckedChanged);
			// 
			// m_SizeScaleCheck
			// 
			this.m_SizeScaleCheck.AutoSize = true;
			this.m_SizeScaleCheck.Location = new System.Drawing.Point(4, 149);
			this.m_SizeScaleCheck.Name = "m_SizeScaleCheck";
			this.m_SizeScaleCheck.Size = new System.Drawing.Size(76, 17);
			this.m_SizeScaleCheck.TabIndex = 10;
			this.m_SizeScaleCheck.Text = "Size Scale";
			this.m_SizeScaleCheck.UseVisualStyleBackColor = true;
			this.m_SizeScaleCheck.Visible = false;
			// 
			// m_ZoomMinValue
			// 
			this.m_ZoomMinValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_ZoomMinValue.Decimals = 3;
			this.m_ZoomMinValue.Enabled = false;
			this.m_ZoomMinValue.Increment = 0.1F;
			this.m_ZoomMinValue.Location = new System.Drawing.Point(154, 75);
			this.m_ZoomMinValue.MaxValue = 1000F;
			this.m_ZoomMinValue.MinValue = 0F;
			this.m_ZoomMinValue.Multiline = true;
			this.m_ZoomMinValue.Name = "m_ZoomMinValue";
			this.m_ZoomMinValue.SetValue = 0F;
			this.m_ZoomMinValue.Size = new System.Drawing.Size(59, 20);
			this.m_ZoomMinValue.TabIndex = 8;
			this.m_ZoomMinValue.Text = "0.000";
			this.m_ZoomMinValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.m_ZoomMinValue.Value = 0F;
			this.m_ZoomMinValue.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.OnSliderValueChanged);
			// 
			// m_ZoomMaxValue
			// 
			this.m_ZoomMaxValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_ZoomMaxValue.Decimals = 3;
			this.m_ZoomMaxValue.Enabled = false;
			this.m_ZoomMaxValue.Increment = 0.1F;
			this.m_ZoomMaxValue.Location = new System.Drawing.Point(265, 75);
			this.m_ZoomMaxValue.MaxValue = 1000F;
			this.m_ZoomMaxValue.MinValue = 0F;
			this.m_ZoomMaxValue.Multiline = true;
			this.m_ZoomMaxValue.Name = "m_ZoomMaxValue";
			this.m_ZoomMaxValue.SetValue = 0F;
			this.m_ZoomMaxValue.Size = new System.Drawing.Size(59, 20);
			this.m_ZoomMaxValue.TabIndex = 9;
			this.m_ZoomMaxValue.Text = "0.000";
			this.m_ZoomMaxValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.m_ZoomMaxValue.Value = 0F;
			this.m_ZoomMaxValue.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.OnSliderValueChanged);
			// 
			// m_PlaybackSpeedMinValue
			// 
			this.m_PlaybackSpeedMinValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_PlaybackSpeedMinValue.Decimals = 3;
			this.m_PlaybackSpeedMinValue.Enabled = false;
			this.m_PlaybackSpeedMinValue.Increment = 0.1F;
			this.m_PlaybackSpeedMinValue.Location = new System.Drawing.Point(154, 27);
			this.m_PlaybackSpeedMinValue.MaxValue = 1000F;
			this.m_PlaybackSpeedMinValue.MinValue = 0F;
			this.m_PlaybackSpeedMinValue.Multiline = true;
			this.m_PlaybackSpeedMinValue.Name = "m_PlaybackSpeedMinValue";
			this.m_PlaybackSpeedMinValue.SetValue = 0F;
			this.m_PlaybackSpeedMinValue.Size = new System.Drawing.Size(59, 20);
			this.m_PlaybackSpeedMinValue.TabIndex = 4;
			this.m_PlaybackSpeedMinValue.Text = "0.000";
			this.m_PlaybackSpeedMinValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.m_PlaybackSpeedMinValue.Value = 0F;
			this.m_PlaybackSpeedMinValue.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.OnSliderValueChanged);
			// 
			// m_PlaybackSpeedMaxValue
			// 
			this.m_PlaybackSpeedMaxValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_PlaybackSpeedMaxValue.Decimals = 3;
			this.m_PlaybackSpeedMaxValue.Enabled = false;
			this.m_PlaybackSpeedMaxValue.Increment = 0.1F;
			this.m_PlaybackSpeedMaxValue.Location = new System.Drawing.Point(265, 27);
			this.m_PlaybackSpeedMaxValue.MaxValue = 1000F;
			this.m_PlaybackSpeedMaxValue.MinValue = 0F;
			this.m_PlaybackSpeedMaxValue.Multiline = true;
			this.m_PlaybackSpeedMaxValue.Name = "m_PlaybackSpeedMaxValue";
			this.m_PlaybackSpeedMaxValue.SetValue = 0F;
			this.m_PlaybackSpeedMaxValue.Size = new System.Drawing.Size(59, 20);
			this.m_PlaybackSpeedMaxValue.TabIndex = 5;
			this.m_PlaybackSpeedMaxValue.Text = "0.000";
			this.m_PlaybackSpeedMaxValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.m_PlaybackSpeedMaxValue.Value = 0F;
			this.m_PlaybackSpeedMaxValue.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.OnSliderValueChanged);
			// 
			// m_DurationMinValue
			// 
			this.m_DurationMinValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_DurationMinValue.Decimals = 3;
			this.m_DurationMinValue.Enabled = false;
			this.m_DurationMinValue.Increment = 0.1F;
			this.m_DurationMinValue.Location = new System.Drawing.Point(154, 3);
			this.m_DurationMinValue.MaxValue = 1000F;
			this.m_DurationMinValue.MinValue = 0F;
			this.m_DurationMinValue.Multiline = true;
			this.m_DurationMinValue.Name = "m_DurationMinValue";
			this.m_DurationMinValue.SetValue = 0F;
			this.m_DurationMinValue.Size = new System.Drawing.Size(59, 20);
			this.m_DurationMinValue.TabIndex = 1;
			this.m_DurationMinValue.Text = "0.000";
			this.m_DurationMinValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.m_DurationMinValue.Value = 0F;
			this.m_DurationMinValue.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.OnSliderValueChanged);
			// 
			// m_DurationMaxValue
			// 
			this.m_DurationMaxValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_DurationMaxValue.Decimals = 3;
			this.m_DurationMaxValue.Enabled = false;
			this.m_DurationMaxValue.Increment = 0.1F;
			this.m_DurationMaxValue.Location = new System.Drawing.Point(265, 3);
			this.m_DurationMaxValue.MaxValue = 1000F;
			this.m_DurationMaxValue.MinValue = 0F;
			this.m_DurationMaxValue.Multiline = true;
			this.m_DurationMaxValue.Name = "m_DurationMaxValue";
			this.m_DurationMaxValue.SetValue = 0F;
			this.m_DurationMaxValue.Size = new System.Drawing.Size(59, 20);
			this.m_DurationMaxValue.TabIndex = 2;
			this.m_DurationMaxValue.Text = "0.000";
			this.m_DurationMaxValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.m_DurationMaxValue.Value = 0F;
			this.m_DurationMaxValue.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.OnSliderValueChanged);
			// 
			// m_SizeYMinValue
			// 
			this.m_SizeYMinValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_SizeYMinValue.Decimals = 3;
			this.m_SizeYMinValue.Enabled = false;
			this.m_SizeYMinValue.Increment = 0.1F;
			this.m_SizeYMinValue.Location = new System.Drawing.Point(219, 135);
			this.m_SizeYMinValue.MaxValue = 1000F;
			this.m_SizeYMinValue.MinValue = 0F;
			this.m_SizeYMinValue.Multiline = true;
			this.m_SizeYMinValue.Name = "m_SizeYMinValue";
			this.m_SizeYMinValue.SetValue = 0F;
			this.m_SizeYMinValue.Size = new System.Drawing.Size(59, 20);
			this.m_SizeYMinValue.TabIndex = 12;
			this.m_SizeYMinValue.Text = "0.000";
			this.m_SizeYMinValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.m_SizeYMinValue.Value = 0F;
			this.m_SizeYMinValue.Visible = false;
			this.m_SizeYMinValue.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.OnSliderValueChanged);
			// 
			// m_SizeXMinValue
			// 
			this.m_SizeXMinValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_SizeXMinValue.Decimals = 3;
			this.m_SizeXMinValue.Enabled = false;
			this.m_SizeXMinValue.Increment = 0.1F;
			this.m_SizeXMinValue.Location = new System.Drawing.Point(154, 135);
			this.m_SizeXMinValue.MaxValue = 1000F;
			this.m_SizeXMinValue.MinValue = 0F;
			this.m_SizeXMinValue.Multiline = true;
			this.m_SizeXMinValue.Name = "m_SizeXMinValue";
			this.m_SizeXMinValue.SetValue = 0F;
			this.m_SizeXMinValue.Size = new System.Drawing.Size(59, 20);
			this.m_SizeXMinValue.TabIndex = 11;
			this.m_SizeXMinValue.Text = "0.000";
			this.m_SizeXMinValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.m_SizeXMinValue.Value = 0F;
			this.m_SizeXMinValue.Visible = false;
			this.m_SizeXMinValue.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.OnSliderValueChanged);
			// 
			// m_SizeZMinValue
			// 
			this.m_SizeZMinValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_SizeZMinValue.Decimals = 3;
			this.m_SizeZMinValue.Enabled = false;
			this.m_SizeZMinValue.Increment = 0.1F;
			this.m_SizeZMinValue.Location = new System.Drawing.Point(284, 135);
			this.m_SizeZMinValue.MaxValue = 1000F;
			this.m_SizeZMinValue.MinValue = 0F;
			this.m_SizeZMinValue.Multiline = true;
			this.m_SizeZMinValue.Name = "m_SizeZMinValue";
			this.m_SizeZMinValue.SetValue = 0F;
			this.m_SizeZMinValue.Size = new System.Drawing.Size(59, 20);
			this.m_SizeZMinValue.TabIndex = 13;
			this.m_SizeZMinValue.Text = "0.000";
			this.m_SizeZMinValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.m_SizeZMinValue.Value = 0F;
			this.m_SizeZMinValue.Visible = false;
			this.m_SizeZMinValue.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.OnSliderValueChanged);
			// 
			// m_SizeZMaxValue
			// 
			this.m_SizeZMaxValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_SizeZMaxValue.Decimals = 3;
			this.m_SizeZMaxValue.Enabled = false;
			this.m_SizeZMaxValue.Increment = 0.1F;
			this.m_SizeZMaxValue.Location = new System.Drawing.Point(284, 161);
			this.m_SizeZMaxValue.MaxValue = 1000F;
			this.m_SizeZMaxValue.MinValue = 0F;
			this.m_SizeZMaxValue.Multiline = true;
			this.m_SizeZMaxValue.Name = "m_SizeZMaxValue";
			this.m_SizeZMaxValue.SetValue = 0F;
			this.m_SizeZMaxValue.Size = new System.Drawing.Size(59, 20);
			this.m_SizeZMaxValue.TabIndex = 16;
			this.m_SizeZMaxValue.Text = "0.000";
			this.m_SizeZMaxValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.m_SizeZMaxValue.Value = 0F;
			this.m_SizeZMaxValue.Visible = false;
			this.m_SizeZMaxValue.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.OnSliderValueChanged);
			// 
			// m_SizeXMaxValue
			// 
			this.m_SizeXMaxValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_SizeXMaxValue.Decimals = 3;
			this.m_SizeXMaxValue.Enabled = false;
			this.m_SizeXMaxValue.Increment = 0.1F;
			this.m_SizeXMaxValue.Location = new System.Drawing.Point(154, 161);
			this.m_SizeXMaxValue.MaxValue = 1000F;
			this.m_SizeXMaxValue.MinValue = 0F;
			this.m_SizeXMaxValue.Multiline = true;
			this.m_SizeXMaxValue.Name = "m_SizeXMaxValue";
			this.m_SizeXMaxValue.SetValue = 0F;
			this.m_SizeXMaxValue.Size = new System.Drawing.Size(59, 20);
			this.m_SizeXMaxValue.TabIndex = 14;
			this.m_SizeXMaxValue.Text = "0.000";
			this.m_SizeXMaxValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.m_SizeXMaxValue.Value = 0F;
			this.m_SizeXMaxValue.Visible = false;
			this.m_SizeXMaxValue.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.OnSliderValueChanged);
			// 
			// m_SizeYMaxValue
			// 
			this.m_SizeYMaxValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_SizeYMaxValue.Decimals = 3;
			this.m_SizeYMaxValue.Enabled = false;
			this.m_SizeYMaxValue.Increment = 0.1F;
			this.m_SizeYMaxValue.Location = new System.Drawing.Point(219, 161);
			this.m_SizeYMaxValue.MaxValue = 1000F;
			this.m_SizeYMaxValue.MinValue = 0F;
			this.m_SizeYMaxValue.Multiline = true;
			this.m_SizeYMaxValue.Name = "m_SizeYMaxValue";
			this.m_SizeYMaxValue.SetValue = 0F;
			this.m_SizeYMaxValue.Size = new System.Drawing.Size(59, 20);
			this.m_SizeYMaxValue.TabIndex = 15;
			this.m_SizeYMaxValue.Text = "0.000";
			this.m_SizeYMaxValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.m_SizeYMaxValue.Value = 0F;
			this.m_SizeYMaxValue.Visible = false;
			this.m_SizeYMaxValue.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.OnSliderValueChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(121, 5);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(27, 13);
			this.label1.TabIndex = 48;
			this.label1.Text = "Min:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(121, 29);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(27, 13);
			this.label2.TabIndex = 49;
			this.label2.Text = "Min:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(121, 77);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(27, 13);
			this.label3.TabIndex = 50;
			this.label3.Text = "Min:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(121, 137);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(27, 13);
			this.label4.TabIndex = 51;
			this.label4.Text = "Min:";
			this.label4.Visible = false;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(229, 5);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(30, 13);
			this.label5.TabIndex = 52;
			this.label5.Text = "Max:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(121, 53);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(27, 13);
			this.label6.TabIndex = 53;
			this.label6.Text = "Min:";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(229, 29);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(30, 13);
			this.label7.TabIndex = 54;
			this.label7.Text = "Max:";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(229, 53);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(30, 13);
			this.label8.TabIndex = 55;
			this.label8.Text = "Max:";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(229, 77);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(30, 13);
			this.label9.TabIndex = 56;
			this.label9.Text = "Max:";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(121, 163);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(30, 13);
			this.label10.TabIndex = 57;
			this.label10.Text = "Max:";
			this.label10.Visible = false;
			// 
			// m_ColorMinButton
			// 
			this.m_ColorMinButton.Enabled = false;
			this.m_ColorMinButton.Location = new System.Drawing.Point(154, 52);
			this.m_ColorMinButton.Name = "m_ColorMinButton";
			this.m_ColorMinButton.Size = new System.Drawing.Size(59, 17);
			this.m_ColorMinButton.TabIndex = 58;
			this.m_ColorMinButton.UseVisualStyleBackColor = true;
			this.m_ColorMinButton.Click += new System.EventHandler(this.m_ColorMinButton_Click);
			// 
			// m_ColorMaxButton
			// 
			this.m_ColorMaxButton.Enabled = false;
			this.m_ColorMaxButton.Location = new System.Drawing.Point(265, 52);
			this.m_ColorMaxButton.Name = "m_ColorMaxButton";
			this.m_ColorMaxButton.Size = new System.Drawing.Size(59, 17);
			this.m_ColorMaxButton.TabIndex = 59;
			this.m_ColorMaxButton.UseVisualStyleBackColor = true;
			this.m_ColorMaxButton.Click += new System.EventHandler(this.m_ColorMaxButton_Click);
			// 
			// EffectOverrides
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.m_ColorMaxButton);
			this.Controls.Add(this.m_ColorMinButton);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.m_SizeZMaxValue);
			this.Controls.Add(this.m_SizeXMaxValue);
			this.Controls.Add(this.m_SizeYMaxValue);
			this.Controls.Add(this.m_SizeZMinValue);
			this.Controls.Add(this.m_SizeXMinValue);
			this.Controls.Add(this.m_SizeYMinValue);
			this.Controls.Add(this.m_DurationMaxValue);
			this.Controls.Add(this.m_DurationMinValue);
			this.Controls.Add(this.m_PlaybackSpeedMaxValue);
			this.Controls.Add(this.m_PlaybackSpeedMinValue);
			this.Controls.Add(this.m_ZoomMaxValue);
			this.Controls.Add(this.m_ZoomMinValue);
			this.Controls.Add(this.m_SizeScaleCheck);
			this.Controls.Add(this.m_ZoomCheck);
			this.Controls.Add(this.m_ColorTintCheck);
			this.Controls.Add(this.m_PlaybackSpeedCheck);
			this.Controls.Add(this.m_DurationCheck);
			this.Name = "EffectOverrides";
			this.Size = new System.Drawing.Size(416, 103);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox m_DurationCheck;
        private System.Windows.Forms.CheckBox m_PlaybackSpeedCheck;
        private System.Windows.Forms.CheckBox m_ColorTintCheck;
        private System.Windows.Forms.CheckBox m_ZoomCheck;
        private System.Windows.Forms.CheckBox m_SizeScaleCheck;
        private ptxSlider m_ZoomMinValue;
        private ptxSlider m_ZoomMaxValue;
        private ptxSlider m_PlaybackSpeedMinValue;
        private ptxSlider m_PlaybackSpeedMaxValue;
        private ptxSlider m_DurationMinValue;
        private ptxSlider m_DurationMaxValue;
        private ptxSlider m_SizeYMinValue;
        private ptxSlider m_SizeXMinValue;
        private ptxSlider m_SizeZMinValue;
        private ptxSlider m_SizeZMaxValue;
        private ptxSlider m_SizeXMaxValue;
        private ptxSlider m_SizeYMaxValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button m_ColorMinButton;
		private System.Windows.Forms.Button m_ColorMaxButton;
    }
}
