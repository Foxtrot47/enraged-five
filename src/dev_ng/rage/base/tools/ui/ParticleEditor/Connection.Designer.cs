namespace ParticleEditor
{
    partial class Connection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.m_IP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_PanelConnectionStatus = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.ragConnectionButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 83);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(176, 32);
            this.button1.TabIndex = 0;
            this.button1.Text = "Connect to Hostname/IP";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // m_IP
            // 
            this.m_IP.Location = new System.Drawing.Point(15, 48);
            this.m_IP.Name = "m_IP";
            this.m_IP.Size = new System.Drawing.Size(176, 20);
            this.m_IP.TabIndex = 1;
            this.m_IP.Text = "0.0.0.0";
            this.m_IP.Enter += new System.EventHandler(this.m_IP_Enter);
            this.m_IP.Leave += new System.EventHandler(this.m_IP_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Hostname/IP:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Connection Status:";
            // 
            // m_PanelConnectionStatus
            // 
            this.m_PanelConnectionStatus.BackColor = System.Drawing.Color.Gray;
            this.m_PanelConnectionStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_PanelConnectionStatus.Location = new System.Drawing.Point(133, 9);
            this.m_PanelConnectionStatus.Name = "m_PanelConnectionStatus";
            this.m_PanelConnectionStatus.Size = new System.Drawing.Size(13, 13);
            this.m_PanelConnectionStatus.TabIndex = 10;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(15, 122);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(176, 32);
            this.button2.TabIndex = 12;
            this.button2.Text = "Connect to PC";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "RAG Connection:";
            // 
            // ragConnectionButton
            // 
            this.ragConnectionButton.Location = new System.Drawing.Point(15, 185);
            this.ragConnectionButton.Name = "ragConnectionButton";
            this.ragConnectionButton.Size = new System.Drawing.Size(176, 32);
            this.ragConnectionButton.TabIndex = 15;
            this.ragConnectionButton.Text = "Connect";
            this.ragConnectionButton.UseVisualStyleBackColor = true;
            this.ragConnectionButton.Click += new System.EventHandler(this.ragConnectionButton_Click);
            // 
            // Connection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(212, 233);
            this.Controls.Add(this.ragConnectionButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_PanelConnectionStatus);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_IP);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Connection";
            this.Text = "Connect To Game";
            this.Load += new System.EventHandler(this.Connection_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox m_IP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Panel m_PanelConnectionStatus;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ragConnectionButton;
    }
}