using System;
using System.Collections.Generic;
using System.Text;

namespace ParticleEditor
{
    public class tcpComObj
    {
        public int m_MessageID = -1;
        public tcpByteBuffer m_Data;
        public delegate void ComObjEventDelegate(tcpComObj obj);
        
        public event ComObjEventDelegate OnSendData = null;
        public event ComObjEventDelegate OnRecieveData = null;

        public tcpComObj(uint messageID)
        {
            m_MessageID = (int)messageID;
        }
        
        virtual public void RecieveData(tcpByteBuffer data)
        {
            m_Data = data;
            if(OnRecieveData != null)
                OnRecieveData(this);
        }
        virtual public void SendData()
        {
            if(OnSendData != null)
                OnSendData(this);
        }
    }
}
