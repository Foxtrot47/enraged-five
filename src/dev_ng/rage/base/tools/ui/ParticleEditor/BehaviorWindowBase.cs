using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
    public partial class BehaviorWindowBase : ParticleEditor.ptxForm
    {
        public class BehaviorChangedEventArgs
        {
            public DataMod.dtaPtxBehavior Behavior;
            internal BehaviorChangedEventArgs(DataMod.dtaPtxBehavior beh)
            {
                Behavior= beh;
            }
        }

        public delegate void BehaviorWindowEvent(object sender, BehaviorChangedEventArgs e);

        public BehaviorWindowBase()
        {
            InitializeComponent();
        }
        
        public static void UpdateBehavior(DataMod.dtaPtxBehavior behavior)
        {
            for (int i = 0; i < main.m_MainForm.OwnedForms.Length; i++)
            {
                Form form = main.m_MainForm.OwnedForms[i];
                if(form is BehaviorWindow)
                {
                    BehaviorWindow b = form as BehaviorWindow;
                    if (b.m_Behavior.m_BehaviorName == behavior.m_BehaviorName)
                        if (b.m_Behavior.m_PtxRule.m_Name == behavior.m_PtxRule.m_Name)
                            b.PrepWindow(behavior);
                }
                
                if (form is CustomBehaviorWindow)
                {
                    CustomBehaviorWindow cb = form as CustomBehaviorWindow;
                    if (cb.m_Behavior.m_BehaviorName == behavior.m_BehaviorName)
                        if (cb.m_Behavior.m_PtxRule.m_Name == behavior.m_PtxRule.m_Name)
                            cb.PrepWindow(behavior);
                }

            }
        }

        public static bool BehaviorShowing(DataMod.dtaPtxBehavior behavior)
        {
            for (int i = 0; i < main.m_MainForm.OwnedForms.Length; i++)
            {
                Form form = main.m_MainForm.OwnedForms[i];
                if(form is BehaviorWindow)
                {
                    BehaviorWindow b = form as BehaviorWindow;
                    if (b.m_Behavior.m_BehaviorName == behavior.m_BehaviorName)
                        if (b.m_Behavior.m_PtxRule.m_Name == behavior.m_PtxRule.m_Name)
                        return true;
                }
                if(form is CustomBehaviorWindow)
                {
                    CustomBehaviorWindow cb = form as CustomBehaviorWindow;
                    if (cb.m_Behavior.m_BehaviorName == behavior.m_BehaviorName)
                        if (cb.m_Behavior.m_PtxRule.m_Name == behavior.m_PtxRule.m_Name)
                            return true;
                }
            }
            return false;
        }

        public static void ShowBehavior(DataMod.dtaPtxBehavior behavior, BehaviorWindowEvent onChange, BehaviorWindowEvent onClosedWindow)
        {
            //First check to see if this behavior is already showing
            if (BehaviorShowing(behavior))
                return;

			// MN - removed the custom draw sprite window - just using the generic one           
			//if(behavior.HasCustomInterface())
			//{
			//    CustomBehaviorWindow custom = null;
                
			//    //Manually determine the proper form to use
			//    if(behavior.GetCustomInterfaceName()=="CustomBehaviorWindowDrawSprite")
			//        custom = new CustomBehaviorWindowDrawSprite();
                
			//    //If none found then do nothing
			//    if (custom == null)
			//        return;

			//    custom.OnBehaviorChanged += onChange;
			//    custom.OnClosedWindow += onClosedWindow;

			//    custom.PrepWindow(behavior);
			//    main.m_MainForm.AddOwnedForm(custom);
			//    custom.Show();
			//    return;
			//}

            BehaviorWindow form = new BehaviorWindow();
                   
            form.OnBehaviorChanged += onChange;
            form.OnClosedWindow += onClosedWindow;
        
            form.PrepWindow(behavior);
            main.m_MainForm.AddOwnedForm(form);
            form.Show();
        }

        public static void CloseBehaviorWindow(DataMod.dtaPtxBehavior behavior)
        {
            for (int i = 0; i < main.m_MainForm.OwnedForms.Length; i++)
            {
                Form form = main.m_MainForm.OwnedForms[i];
                if (form is BehaviorWindow)
                {
                    BehaviorWindow behForm = form as BehaviorWindow;
                    if( behForm.m_Behavior.m_BehaviorName.Equals(behavior.m_BehaviorName) )
                    {
                        behForm.Close();
                    }                    
                }
                if (form is CustomBehaviorWindow)
                {
                    CustomBehaviorWindow cbehForm = form as CustomBehaviorWindow;
                    if (cbehForm.m_Behavior.m_BehaviorName.Equals(behavior.m_BehaviorName))
                    {
                        cbehForm.Close();
                    }
                }
            }

        }
    }
}

