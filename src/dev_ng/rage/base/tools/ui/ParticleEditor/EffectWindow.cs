using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
    public partial class EffectWindow : UserControl
    {
        public String   m_CurrentEffectName="";
        public DataMod.dtaEvent m_CurrentEvent = null;
        List<ToolStripMenuItem> m_MenuListEvoDel = new List<ToolStripMenuItem>();
        EventEffect m_evtEffectDisplay = new EventEffect();
     
        public EffectWindow()
        {
            InitializeComponent();
            m_SubSectionTimeline.SuspendLayout();
            m_evtEffectDisplay.Location = new Point(0, 20);
            m_evtEffectDisplay.Visible = false;
            m_SubSectionTimeline.Controls.Add(m_evtEffectDisplay);
            m_evtEffectDisplay.Width = m_SubSectionTimeline.Width;
            m_SubSectionTimeline.ResumeLayout();
            m_PanelSection.Width = m_PanelGroup.Width - 1;
			m_EvtStartRatio.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(m_Timeline_OnValueChanged);
			m_EvtEndRatio.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(m_Timeline_OnValueChanged);
            m_evtEffectDisplay.m_EvtStartTime.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(m_Timeline_OnValueChanged);
//            m_EvtDurationScalarMin.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(m_Timeline_OnValueChanged);
//            m_EvtDurationScalarMax.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(m_Timeline_OnValueChanged);
            m_EvtPlaybackMin.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(m_Timeline_OnValueChanged);
            m_EvtPlaybackMax.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(m_Timeline_OnValueChanged);

            m_DurationMin.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(m_Timeline_OnValueChanged);
            m_DurationMax.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(m_Timeline_OnValueChanged);
            m_PlayBackMin.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(m_Timeline_OnValueChanged);
            m_PlayBackMax.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(m_Timeline_OnValueChanged);

            m_DataGridEffectKeyframeList.ColumnHeadersVisible = false;
            m_DataGridEvoList.ColumnHeadersVisible = false;
            //TEST
            BuildEvoBlendModeDropDown();

            InitCullingNewInterface();
            InitCollisionInterface();
        }

        public void UpdateDrawListNames()
        {
            m_ComboDrawList.SuspendLayout();
            m_ComboDrawList.Items.Clear();
            for (int i = 0; i < DataMod.m_SDataMod.m_DrawListNames.Count; i++)
                m_ComboDrawList.Items.Add(DataMod.m_SDataMod.m_DrawListNames[i].ToString());
            m_ComboDrawList.ResumeLayout();
        }

		public void UpdateDataVolumeTypeNames()
		{
			m_ComboDataVolumeType.SuspendLayout();
			m_ComboDataVolumeType.Items.Clear();
			for (int i = 0; i < DataMod.m_SDataMod.m_DataVolumeTypeNames.Count; i++)
				m_ComboDataVolumeType.Items.Add(DataMod.m_SDataMod.m_DataVolumeTypeNames[i].ToString());
			m_ComboDataVolumeType.ResumeLayout();
		}

        public void DisplayEffect(String name, bool asemitrule,bool asptxrule)
        {
            KeyframeEditorFormBase.FindAndCloseUnusedForms();
            DataMod.dtaEffectRuleStd doe = DataMod.GetEffectRule(name) as DataMod.dtaEffectRuleStd;
            if (doe == null)
            {
                main.m_MainForm.m_PlayControlForm.SetEffectRule("");
                EnableSubSections(false);
                return;
            }
            EnableSubSections(true);

            m_CurrentEffectName = doe.m_Name;
            main.m_MainForm.m_PlayControlForm.SetEffectRule(m_CurrentEffectName);

            m_LabelHeader.Text = "Effect: " + doe.m_Name;
            m_DurationMin.SetValue = doe.m_DurationMin;
            m_DurationMax.SetValue = doe.m_DurationMax;
            m_PlayBackMin.SetValue = doe.m_TimeScalarMin;
            m_PlayBackMax.SetValue = doe.m_TimeScalarMax;
            m_RndOffsetPosX.SetValue = doe.m_RndOffsetPos.x;
            m_RndOffsetPosY.SetValue = doe.m_RndOffsetPos.y;
            m_RndOffsetPosZ.SetValue = doe.m_RndOffsetPos.z;
            m_CheckUseRandomColorTint.Checked = doe.m_UseRandomColorTint;
            m_CheckUseDataSphere.Checked = doe.m_HasDataSphere;
            
            //Culling
//            m_CheckCullEnable.Checked = doe.m_UseCullSphere;
//            m_CullRadius.SetValue = doe.m_CullRadius;
//            m_CullDist.SetValue = doe.m_CullDistance;
//            m_FadeDist.SetValue = doe.m_FadeDistance;
//            m_LodNearDist.SetValue = doe.m_LodNearDistance;
//            m_LodFarDist.SetValue = doe.m_LodFarDistance;
//            m_CheckZoomCullDist.Checked = doe.m_ZoomCullDist;
//            m_CullSphereOffsetX.SetValue = doe.m_CullSphere.x;
//            m_CullSphereOffsetY.SetValue = doe.m_CullSphere.y;
//            m_CullSphereOffsetZ.SetValue = doe.m_CullSphere.z;
//            m_CheckCullSpNoEmit.Checked = doe.m_CullSpNoEmit;
//            m_CheckCullSpNoUpdate.Checked = doe.m_CullSpNoUpdate;
//            m_CheckCullSpNoDraw.Checked = doe.m_CullSpNoDraw;
            m_CullViewportModeCombo.SelectedIndex = doe.m_CullViewportMode;
            m_CullViewportRender.Checked = doe.m_CullViewportRender;
            m_CullViewportUpdate.Checked = doe.m_CullViewportUpdate;
            m_CullViewportEmit.Checked = doe.m_CullViewportEmit;
            m_CullViewportRadius.SetValue = doe.m_CullViewportRadius;
            m_CullViewportOffsetX.SetValue = doe.m_CullViewportOffsetX;
            m_CullViewportOffsetY.SetValue = doe.m_CullViewportOffsetY;
            m_CullViewportOffsetZ.SetValue = doe.m_CullViewportOffsetZ;
            m_CullDistanceModeCombo.SelectedIndex = doe.m_CullDistanceMode;
            m_CullDistanceRender.Checked = doe.m_CullDistanceRender;
            m_CullDistanceUpdate.Checked = doe.m_CullDistanceUpdate;
            m_CullDistanceEmit.Checked = doe.m_CullDistanceEmit;
            m_CullDistanceFadeDist.SetValue = doe.m_CullDistanceFadeDist;
            m_CullDistanceCullDist.SetValue = doe.m_CullDistanceCullDist;
            m_LODNearDistance.SetValue = doe.m_LODNearDist;
            m_LODFarDistance.SetValue = doe.m_LODFarDist;
            m_ColnTypeCombo.SelectedIndex = doe.m_ColnType;
            m_ColnRange.SetValue = doe.m_ColnRange;
            m_ColnProbeDist.SetValue = doe.m_ColnProbeDist;
            m_ColnUseEntity.Checked = doe.m_ColnUseEntity;
			m_ColnOnlyBVH.Checked = doe.m_ColnOnlyBVH;

            UpdateCullingInterface();

            //Timeline
            m_PreUpdate.SetValue = doe.m_PreUpdate;
            m_PreUpdateInterval.SetValue = doe.m_PreUpdateInterval;
            m_NumLoops.SetValue = doe.m_NumLoops;
            m_CheckSortEvents.Checked = doe.m_SortEvents;

            UpdateCullingNewInterface();    // must be called after m_NumLoops is set

            //fix the drawlist but not reset it
            int index = doe.m_DataObjectType;
            if (index >= DataMod.m_SDataMod.m_DrawListNames.Count)
                index = 0;
            m_ComboDrawList.Text = DataMod.m_SDataMod.m_DrawListNames[index].ToString();

			index = doe.m_DataObjectType;
			if (index >= DataMod.m_SDataMod.m_DataVolumeTypeNames.Count)
				index = 0;
			m_ComboDataVolumeType.Text = DataMod.m_SDataMod.m_DataVolumeTypeNames[index].ToString();

            UpdateTimeline();
            int selid = main.m_MainForm.m_TimeLine.GetSelectedChannelIndex();
            if (doe.m_TimeLine.m_Events.Count >= 1)
                DisplayTimelineEvent(doe.m_TimeLine.m_Events[Math.Max(0,selid)] as DataMod.dtaEvent,asemitrule,asptxrule);
            else
                DisplayTimelineEvent(null,true,true);

            //Debug 
            m_CheckShowInst.Checked   = doe.m_ShowInstances;
            m_CheckShowCullSp.Checked = doe.m_ShowCullSpheres;
            m_CheckShowDataSpheres.Checked = doe.m_ShowDataSpheres;
            m_Scale.SetValue = doe.m_Scale;
            m_ShowPhysics.Checked = doe.m_ShowPhysics;
            m_CheckShowPointQuads.Checked = doe.m_ShowPointQuads;

            //Keyframes
            main.m_MainForm.BuildKeyframeGrid(m_DataGridEffectKeyframeList, doe.m_KeyframeList);

			m_ComboDrawList.SelectedIndex = doe.m_DrawList;
			m_ComboDataVolumeType.SelectedIndex = doe.m_DataObjectType;

            DisplayEvolutionData(doe);
            BuildGameSpecificData(doe);
            main.m_MainForm.DisplayHeirarchy(doe);
        }
        public void DisplayTimelineEvent(DataMod.dtaEvent evt,bool asemitrule, bool asptxrule)
        {
            m_CurrentEvent = evt;
            if (evt == null)
            {
                //Clear the subsection;
                m_SubSectionTimeline.HeaderText = "";
                m_SubSectionTimeline.Enabled = false;
                main.m_MainForm.DisplayEmitter("");
                main.m_MainForm.DisplayPtxRule("");
                return;
            }
            m_SubSectionTimeline.Enabled = true;
            main.m_MainForm.m_TimeLine.SelectChannel(evt.m_Data as ragPtxTimeLine.ptxTimeLine.ptxChannel);
            
            //Emitter Event
            if (evt.m_Type == 0)
                DisplayEventEmitter(evt as DataMod.dtaEventEmitter,asemitrule,asptxrule);
            //Effect Event
            if (evt.m_Type == 1)
                DisplayEventEffect(evt as DataMod.dtaEventEffect);
        }

        private void SetEffectEventControls(bool state)
        {
            m_SubSectionTimeline.SuspendLayout();
            m_evtEffectDisplay.Visible = state;
            m_SubSectionTimeline.ResumeLayout();
        }
        private void SetEmitterEventControls(bool state)
        {
            m_SubSectionTimeline.SuspendLayout();
            //m_ComboEventEvoBlendMode.Visible = state;
            m_EvtStartRatio.Visible = state;
//            m_EvtDurationScalarMin.Visible = state;
//            m_EvtDurationScalarMax.Visible = state;
            m_EvtPlaybackMin.Visible = state;
            m_EvtPlaybackMax.Visible = state;
            m_ComboEmitRule.Visible = state;
            m_ComboPtxRule.Visible = state;
            m_ButtonEXFilterModels.Visible = state;
            m_ButtonEXFilterSprites.Visible = state;
            m_ColorMinButton.Visible = state;
            m_ColorMaxButton.Visible = state;
            m_EvtZoomMin.Visible = state;
            m_EvtZoomMax.Visible = state;

//            label16.Visible = state;
            label17.Visible = state;
            label18.Visible = state;
//            label19.Visible = state;
            label20.Visible = state;
            label22.Visible = state;
            //label23.Visible = state;
            label1.Visible = state;
            label27.Visible = state;
            label28.Visible = state;
            label29.Visible = state;
            label30.Visible = state;

            m_SubSectionTimeline.ResumeLayout();
        }
        private void DisplayEventEmitter(DataMod.dtaEventEmitter e,bool asemitrule, bool asptxrule)
        {
            if (e == null) return;
            SetEffectEventControls(false);
            SetEmitterEventControls(true);
            m_SubSectionTimeline.HeaderText = "(Emitter: " + e.ToString() + ")";
            m_SubSectionTimeline.SetHeight(202);

            //Set up auto filter mode
            m_ButtonEXFilterModels.ButtonState = true;
            m_ButtonEXFilterSprites.ButtonState = true;

            //if (e.m_EvoGroup.ContainsPtxRuleEvoData())
            //{
            //    DataMod.dtaObject dob = DataMod.m_SDataMod.GetObject(DataMod.m_SDataMod.m_PtxRuleList, e.m_PtxRuleName);
            //    if (dob != null)
            //    {
            //        if (dob.m_Type == 0)
            //            m_ButtonEXFilterSprites.ButtonState = false;
            //        else
            //            m_ButtonEXFilterModels.ButtonState = false;
            //    }
            //}

            if(asptxrule)
                BuildPtxRuleDropDown();
            m_ComboPtxRule.Text = e.m_PtxRuleName;

            //Data source doesn't seem to always work?
            m_ComboEmitRule.SuspendLayout();
            m_ComboEmitRule.Items.Clear();
            for (int i = 0; i < DataMod.m_SDataMod.m_EmitRuleList.Count; i++)
                m_ComboEmitRule.Items.Add(DataMod.m_SDataMod.m_EmitRuleList[i]);
            m_ComboEmitRule.ResumeLayout();
            m_ComboEmitRule.Text = e.m_EmitRuleName;

 //           m_EvtStartTime.SetValue = e.m_TriggerTime;
			m_EvtStartRatio.SetValue = e.m_StartRatio;
			m_EvtEndRatio.SetValue = e.m_EndRatio;
//			m_EvtDurationScalarMin.SetValue = 0.0f;// e.m_DurationScalarMin;
//			m_EvtDurationScalarMax.SetValue = 0.0f;// e.m_DurationScalarMax;
            m_EvtPlaybackMin.SetValue = e.m_TimeScalarMin;
            m_EvtPlaybackMax.SetValue = e.m_TimeScalarMax;
            m_EvtZoomMin.SetValue = e.m_ZoomMin;
            m_EvtZoomMax.SetValue = e.m_ZoomMax;
            m_ColorMinButton.SetColor(e.m_ColorTintMin);
            m_ColorMaxButton.SetColor(e.m_ColorTintMax);

            m_SubSectionEffectOverride.Visible = false;
            m_EffectOverrides.Overridable = null;

            //TODO: ALL TO GO
           // m_ComboEventEvoBlendMode.SelectedIndex = 0;// e.m_EvoGroup.m_EvoMode;

            if (asptxrule)
                main.m_MainForm.SelectAndRequestPtxRule(e.m_PtxRuleName);
            if (asemitrule)
                main.m_MainForm.SelectAndRequestEmitter(e.m_EmitRuleName);
        }
        private void DisplayEventEffect(DataMod.dtaEventEffect e)
        {
            if (e == null) return;
            SetEffectEventControls(true);
            SetEmitterEventControls(false);
            m_SubSectionTimeline.HeaderText = "(Effect: " + e.ToString() + ")";
            m_evtEffectDisplay.DisplayEvent(e);
            m_SubSectionEffectOverride.Visible = true;
            m_EffectOverrides.ValueChanged -= OnOverrideChanged; // remove any preexisting callback
            m_EffectOverrides.Overridable = e.m_Overridables;
            m_EffectOverrides.ValueChanged += OnOverrideChanged; // add a new one
            m_SubSectionTimeline.SetHeight(m_evtEffectDisplay.Height + m_evtEffectDisplay.Top);

        }

        private void OnOverrideChanged(object sender, EventArgs e)
        {
                ChangedEffect(m_CurrentEffectName);
        }

        private void DisplayEvolutionData(DataMod.dtaEffectRuleStd doe)
        {
            //Evolution list
            try
            {
                m_DataGridEvoList.SuspendLayout();
                m_DataGridEvoList.Rows.Clear();
                bool hasLod = false;
                foreach (DataMod.dtaEvolution evo in doe.m_EvolutionList.m_Evolutions)
                {
                    int idx = m_DataGridEvoList.Rows.Add();
                    int cnt = idx + 1;
                    m_DataGridEvoList.Rows[idx].Cells[0].Value = "[" + cnt.ToString() + "]";
                    m_DataGridEvoList.Rows[idx].Cells[1].Value = evo.m_Name;
                    m_DataGridEvoList.Rows[idx].Cells[1].Tag = evo;
                    switch (cnt)
                    {
                        case 1:
                            m_DataGridEvoList.Rows[idx].Cells[0].Style.ForeColor = Color.Red;
                            m_DataGridEvoList.Rows[idx].Cells[1].Style.ForeColor = Color.Red; break;
                        case 2:
                            m_DataGridEvoList.Rows[idx].Cells[0].Style.ForeColor = Color.Blue;
                            m_DataGridEvoList.Rows[idx].Cells[1].Style.ForeColor = Color.Blue; break;
                        case 3:
                            m_DataGridEvoList.Rows[idx].Cells[0].Style.ForeColor = Color.Green;
                            m_DataGridEvoList.Rows[idx].Cells[1].Style.ForeColor = Color.Green; break;
                        case 4:
                            m_DataGridEvoList.Rows[idx].Cells[0].Style.ForeColor = Color.Purple;
                            m_DataGridEvoList.Rows[idx].Cells[1].Style.ForeColor = Color.Purple; break;
                        case 5:
                            m_DataGridEvoList.Rows[idx].Cells[0].Style.ForeColor = Color.SaddleBrown;
                            m_DataGridEvoList.Rows[idx].Cells[1].Style.ForeColor = Color.SaddleBrown; break;
                        default:
                            m_DataGridEvoList.Rows[idx].Cells[0].Style.ForeColor = Color.Black;
                            m_DataGridEvoList.Rows[idx].Cells[1].Style.ForeColor = Color.Black; break;
                    }

                    if (String.Compare(evo.m_Name, "LOD", true) == 0)
                    {
                        hasLod = true;
                    }
                }
                m_DataGridEvoList.Sort(m_DataGridEvoList.Columns[0], ListSortDirection.Ascending);
                m_DataGridEvoList.ResumeLayout();
                main.m_MainForm.SelectPropertyFromDataGrid(m_DataGridEvoList, "");

//                m_LodNearDist.Enabled = hasLod;
//                m_LodFarDist.Enabled = hasLod;
//                m_CreateLodBtn.Enabled = !hasLod;

                m_LODNearDistance.Enabled = hasLod;
                m_LODFarDistance.Enabled = hasLod;
                m_LODCreate.Enabled = !hasLod;

                if (doe == null) return;
                if (doe.m_EvolutionList.m_Evolutions.Count == 6)
                {
//                    m_CreateLodBtn.Enabled = false;
                    m_LODCreate.Enabled = false;
                }

                main.m_MainForm.DisplayEvolutionData(m_DataGridEffectKeyframeList, doe.m_EvolutionList);
            } 
            catch (System.Exception /*e*/)
            {
                //System.Windows.Forms.MessageBox.Show("Catching Exception :" + e.Message);
            }
        
          }
        public void UpdateTimeline()
        {
            DataMod.dtaEffectRuleStd doe = DataMod.GetEffectRule(m_CurrentEffectName) as DataMod.dtaEffectRuleStd;
            if (doe == null) return;
            main.m_MainForm.m_TimeLine.Duration = doe.m_DurationMax;
            if (doe.m_DurationMin == doe.m_DurationMax)
                main.m_MainForm.m_TimeLine.MarkerPos = -1.0f;
            else
                main.m_MainForm.m_TimeLine.MarkerPos = doe.m_DurationMin;

            foreach (DataMod.dtaEvent evt in doe.m_TimeLine.m_Events)
            {
                ragPtxTimeLine.ptxTimeLine.ptxChannel c = evt.m_Data as ragPtxTimeLine.ptxTimeLine.ptxChannel;
                c.OnChannelInfoChanged -= new ragPtxTimeLine.ptxTimeLine.ChannelEventDelegate(main.m_MainForm.m_TimeLine_OnChannelInfoChanged);
                if (c == null)
                    continue;
				c.StartRatio = evt.GetStartRatio();
				c.EndRatio = evt.GetEndRatio();
//                Float2 dur = evt.GetDuration();
//                float pbrmin = Math.Max(0.0001f, doe.m_TimeScalarMin);
//                float pbrmax = Math.Max(0.0001f, doe.m_TimeScalarMax);
                //Playbackrate does not change the duration -just the speed
 //               c.DurationB = dur.x;// / pbrmax;
 //               c.Duration = dur.y;// / pbrmin;
                c.OnChannelInfoChanged += new ragPtxTimeLine.ptxTimeLine.ChannelEventDelegate(main.m_MainForm.m_TimeLine_OnChannelInfoChanged);
            }
        }
        
        protected void UpdateCullingInterface()
        {
            //if culling is disabled:
//            bool cullingEnabled = m_CheckCullEnable.Checked;
//            m_CullRadius.Enabled = cullingEnabled;
//            m_CullSphereOffsetX.Enabled = cullingEnabled;
//            m_CullSphereOffsetY.Enabled = cullingEnabled;
//            m_CullSphereOffsetZ.Enabled = cullingEnabled;
//            groupBox1.Enabled = cullingEnabled;
//            m_FadeDist.Enabled = cullingEnabled;
//            m_CullDist.Enabled = cullingEnabled;
//            m_CheckZoomCullDist.Enabled = cullingEnabled;

//            if(cullingEnabled)
//            {
//                //Update culling controls emit culling:
//                m_CheckCullSpNoEmit.Enabled = !m_CheckCullSpNoUpdate.Checked;
//                if (m_CheckCullSpNoUpdate.Checked)
//                    m_CheckCullSpNoEmit.Checked = true;
//            }
        }

        protected void InitCullingNewInterface()
        {
            m_CullViewportModeCombo.Items.Clear();
            m_CullViewportModeCombo.Items.Add("NONE");
            m_CullViewportModeCombo.Items.Add("KILL");
            m_CullViewportModeCombo.Items.Add("PLAYOUT");
            m_CullViewportModeCombo.Items.Add("FREEZE");
            m_CullViewportModeCombo.Items.Add("RECYCLE");
            m_CullViewportModeCombo.Items.Add("USER");

            m_CullDistanceModeCombo.Items.Clear();
            m_CullDistanceModeCombo.Items.Add("NONE");
            m_CullDistanceModeCombo.Items.Add("KILL");
            m_CullDistanceModeCombo.Items.Add("PLAYOUT");
            m_CullDistanceModeCombo.Items.Add("FREEZE");
            m_CullDistanceModeCombo.Items.Add("RECYCLE");
            m_CullDistanceModeCombo.Items.Add("USER");
        }

        protected void InitCollisionInterface()
        {
            m_ColnTypeCombo.Items.Clear();
            m_ColnTypeCombo.Items.Add("NONE");
            m_ColnTypeCombo.Items.Add("GROUND PLANE");
            m_ColnTypeCombo.Items.Add("GROUND BOUND");
            m_ColnTypeCombo.Items.Add("NEARBY BOUNDS");
            m_ColnTypeCombo.Items.Add("VEHICLE ABOVE");
            m_ColnTypeCombo.Items.Add("WATER PLANE");
            m_ColnTypeCombo.Items.Add("GROUND BOUNDS");
        }

        protected void UpdateCullingNewInterface()
        {
            // check that any selected culling mode corresponds to the type of effect
            if (m_NumLoops.Value == -1)
            {
                // infinitely looped effect (registered)
                // check that a triggered cull mode isn't selected
                if (m_CullViewportModeCombo.SelectedIndex==1 || m_CullViewportModeCombo.SelectedIndex==2)
                {
                    m_CullViewportModeCombo.SelectedIndex = 0;
                }
                if (m_CullDistanceModeCombo.SelectedIndex == 1 || m_CullDistanceModeCombo.SelectedIndex == 2)
                {
                    m_CullDistanceModeCombo.SelectedIndex = 0;
                }
            }
            else
            {
                // finite time effect (triggered)
                // check that a registered cull mode isn't selected
                if (m_CullViewportModeCombo.SelectedIndex == 3 || m_CullViewportModeCombo.SelectedIndex == 4)
                {
                    m_CullViewportModeCombo.SelectedIndex = 0;
                }
                if (m_CullDistanceModeCombo.SelectedIndex == 3 || m_CullDistanceModeCombo.SelectedIndex == 4)
                {
                    m_CullDistanceModeCombo.SelectedIndex = 0;
                }
            }

            // viewport culling
            //if (m_CullViewportModeCombo.SelectedIndex >= 0 && m_CullViewportModeCombo.SelectedIndex < 6)
            if (m_CullViewportModeCombo.SelectedItem.Equals("")==false)
            {
                // enable or disable the main settings
                if (m_CullViewportModeCombo.SelectedItem.Equals("NONE"))
                {
                    // disable the viewport sphere settings if no mode is selected
                    m_CullViewportRadius.Enabled = false;
                    m_CullViewportOffsetX.Enabled = false;
                    m_CullViewportOffsetY.Enabled = false;
                    m_CullViewportOffsetZ.Enabled = false;
                }
                else
                {
                    // otherwise enable the viewport sphere settings
                    m_CullViewportRadius.Enabled = true;
                    m_CullViewportOffsetX.Enabled = true;
                    m_CullViewportOffsetY.Enabled = true;
                    m_CullViewportOffsetZ.Enabled = true;
                }

                // enable or disable the render, update and emit settings
                if (m_CullViewportModeCombo.SelectedItem.Equals("USER"))
                {
                    // enable the tick box if user settings selected
                    m_CullViewportRender.Enabled = true;
                    m_CullViewportUpdate.Enabled = true;
                    m_CullViewportEmit.Enabled = true;
                }
                else
                {
                    // otherwise disable the tick boxes
                    m_CullViewportRender.Enabled = false;
                    m_CullViewportUpdate.Enabled = false;
                    m_CullViewportEmit.Enabled = false;

                    if (m_CullViewportModeCombo.SelectedItem.Equals("NONE") ||
                        m_CullViewportModeCombo.SelectedItem.Equals("KILL"))
                    {
                        // check the render tick box if none or kill mode are selected
                        m_CullViewportRender.Checked = true;
                    }
                    else
                    {
                        // otherwise uncheck the render tick box
                        m_CullViewportRender.Checked = false;
                    }

                    if (m_CullViewportModeCombo.SelectedItem.Equals("FREEZE"))
                    {
                        // uncheck the update tick box if freeze mode is selected
                        m_CullViewportUpdate.Checked = false;
                    }
                    else
                    {
                        // otherwise check the update tick box
                        m_CullViewportUpdate.Checked = true;
                    }

                    if (m_CullViewportModeCombo.SelectedItem.Equals("FREEZE") ||
                        m_CullViewportModeCombo.SelectedItem.Equals("RECYCLE"))
                    {
                        // uncheck the emit tick box if freeze or recycle mode is selected
                        m_CullViewportEmit.Checked = false;
                    }
                    else
                    {
                        // otherwise check the emit tick box
                        m_CullViewportEmit.Checked = true;
                    }
                }
            }

            // distance culling
            if (m_CullDistanceModeCombo.SelectedItem.Equals("") == false)
            {
                // enable or disable the main settings
                if (m_CullDistanceModeCombo.SelectedItem.Equals("NONE"))
                {
                    // disable the distance cull settings if no mode is selected
                    m_CullDistanceFadeDist.Enabled = false;
                    m_CullDistanceCullDist.Enabled = false;
                }
                else
                {
                    // otherwise enable the distance cull settings
                    m_CullDistanceFadeDist.Enabled = true;
                    m_CullDistanceCullDist.Enabled = true;
                }

                // enable or disable the render, update and emit settings
                if (m_CullDistanceModeCombo.SelectedItem.Equals("USER"))
                {
                    // enable the tick box if user settings selected
                    m_CullDistanceRender.Enabled = true;
                    m_CullDistanceUpdate.Enabled = true;
                    m_CullDistanceEmit.Enabled = true;

                }
                else
                {
                    // otherwise disable the tick boxes
                    m_CullDistanceRender.Enabled = false;
                    m_CullDistanceUpdate.Enabled = false;
                    m_CullDistanceEmit.Enabled = false;

                    if (m_CullDistanceModeCombo.SelectedItem.Equals("NONE") ||
                        m_CullDistanceModeCombo.SelectedItem.Equals("KILL"))
                    {
                        // check the render tick box if none or kill mode are selected
                        m_CullDistanceRender.Checked = true;
                    }
                    else
                    {
                        // otherwise uncheck the render tick box
                        m_CullDistanceRender.Checked = false;
                    }

                    if (m_CullDistanceModeCombo.SelectedItem.Equals("FREEZE"))
                    {
                        // uncheck the update tick box if freeze mode is selected
                        m_CullDistanceUpdate.Checked = false;
                    }
                    else
                    {
                        // otherwise check the update tick box
                        m_CullDistanceUpdate.Checked = true;
                    }

                    if (m_CullDistanceModeCombo.SelectedItem.Equals("FREEZE") ||
                        m_CullDistanceModeCombo.SelectedItem.Equals("RECYCLE"))
                    {
                        // uncheck the emit tick box if freeze or recycle mode is selected
                        m_CullDistanceEmit.Checked = false;
                    }
                    else
                    {
                        // otherwise check the emit tick box
                        m_CullDistanceEmit.Checked = true;
                    }
                }
            }
        }

        public void SetDataFromGui()
        {
            DataMod.dtaObject dob = DataMod.m_SDataMod.GetObject(DataMod.m_SDataMod.m_EffectRuleList, m_CurrentEffectName);
            if (dob == null) return;
            DataMod.dtaEffectRuleStd doe = dob.m_Object as DataMod.dtaEffectRuleStd;
            if (doe == null) return;

            doe.m_DurationMin   = m_DurationMin.Value;
            doe.m_DurationMax   = m_DurationMax.Value;
            doe.m_TimeScalarMin = m_PlayBackMin.Value;
            doe.m_TimeScalarMax = m_PlayBackMax.Value;
            doe.m_RndOffsetPos.x = m_RndOffsetPosX.Value;
            doe.m_RndOffsetPos.y = m_RndOffsetPosY.Value;
            doe.m_RndOffsetPos.z = m_RndOffsetPosZ.Value;
            doe.m_UseRandomColorTint = m_CheckUseRandomColorTint.Checked;
            doe.m_HasDataSphere = m_CheckUseDataSphere.Checked;

            //Culling
//            doe.m_UseCullSphere = m_CheckCullEnable.Checked;
//            doe.m_CullRadius    = m_CullRadius.Value;
//            doe.m_CullDistance = m_CullDist.Value;
//            doe.m_FadeDistance = m_FadeDist.Value;
//            doe.m_LodNearDistance = m_LodNearDist.Value;
//            doe.m_LodFarDistance = m_LodFarDist.Value;
//            doe.m_ZoomCullDist = m_CheckZoomCullDist.Checked;
//            doe.m_CullSphere.x = m_CullSphereOffsetX.Value;
//            doe.m_CullSphere.y = m_CullSphereOffsetY.Value;
//            doe.m_CullSphere.z = m_CullSphereOffsetZ.Value;
//            doe.m_CullSpNoEmit = m_CheckCullSpNoEmit.Checked;
//            doe.m_CullSpNoUpdate = m_CheckCullSpNoUpdate.Checked;
//            doe.m_CullSpNoDraw = m_CheckCullSpNoDraw.Checked;
            if (m_CullViewportModeCombo.SelectedIndex < 0 || m_CullViewportModeCombo.SelectedIndex > 5)
            {
                doe.m_CullViewportMode = 0;
            }
            else
            {
                doe.m_CullViewportMode = (byte)m_CullViewportModeCombo.SelectedIndex;
            }
            doe.m_CullViewportRender = m_CullViewportRender.Checked;
            doe.m_CullViewportUpdate = m_CullViewportUpdate.Checked;
            doe.m_CullViewportEmit = m_CullViewportEmit.Checked;
            doe.m_CullViewportRadius = m_CullViewportRadius.Value;
            doe.m_CullViewportOffsetX = m_CullViewportOffsetX.Value;
            doe.m_CullViewportOffsetY = m_CullViewportOffsetY.Value;
            doe.m_CullViewportOffsetZ = m_CullViewportOffsetZ.Value;
            if (m_CullDistanceModeCombo.SelectedIndex < 0 || m_CullDistanceModeCombo.SelectedIndex > 5)
            {
                doe.m_CullDistanceMode = 0;
            }
            else
            {
                doe.m_CullDistanceMode = (byte)m_CullDistanceModeCombo.SelectedIndex;
            }
            doe.m_CullDistanceRender = m_CullDistanceRender.Checked;
            doe.m_CullDistanceUpdate = m_CullDistanceUpdate.Checked;
            doe.m_CullDistanceEmit = m_CullDistanceEmit.Checked;
            doe.m_CullDistanceFadeDist = m_CullDistanceFadeDist.Value;
            doe.m_CullDistanceCullDist = m_CullDistanceCullDist.Value;
            doe.m_LODNearDist = m_LODNearDistance.Value;
            doe.m_LODFarDist = m_LODFarDistance.Value;
            doe.m_ColnType = (byte)m_ColnTypeCombo.SelectedIndex;
            doe.m_ColnRange = m_ColnRange.Value;
            doe.m_ColnProbeDist = m_ColnProbeDist.Value;
            doe.m_ColnUseEntity = m_ColnUseEntity.Checked;
			doe.m_ColnOnlyBVH = m_ColnOnlyBVH.Checked;

            //Timeline
            doe.m_SortEvents = m_CheckSortEvents.Checked;
            doe.m_NumLoops = (int) m_NumLoops.Value;
            doe.m_PreUpdate = m_PreUpdate.Value;
            doe.m_PreUpdateInterval = m_PreUpdateInterval.Value;

            SetEventDataFromGui();

            //Debug 
            doe.m_ShowInstances = m_CheckShowInst.Checked;
            doe.m_ShowCullSpheres = m_CheckShowCullSp.Checked;
            doe.m_ShowDataSpheres = m_CheckShowDataSpheres.Checked;
            doe.m_Scale = m_Scale.Value;
            doe.m_ShowPhysics = m_ShowPhysics.Checked;
            doe.m_ShowPointQuads = m_CheckShowPointQuads.Checked;

            doe.m_DrawList = (byte)m_ComboDrawList.SelectedIndex;
			doe.m_DataObjectType = (byte)m_ComboDataVolumeType.SelectedIndex;

        }
        public void SetEventDataFromGui()
        {
            if (m_CurrentEvent == null)
                return;
            if (m_ComboEmitRule.SelectedItem == null)
                return;

            //Emitter Event
            if (m_CurrentEvent.m_Type == 0)
            {
                DataMod.dtaEventEmitter e = m_CurrentEvent as DataMod.dtaEventEmitter;
                if (e == null) return;

                //Detect ptx type switch and potential loss of evolution data (Warn User)
                //if (e.m_EvoGroup.ContainsPtxRuleEvoData())
                //{
                //    if(e.m_PtxRuleName != m_ComboPtxRule.SelectedItem.ToString())
                //    {
                //        DataMod.dtaObject dro = DataMod.m_SDataMod.GetObject(DataMod.m_SDataMod.m_PtxRuleList,e.m_PtxRuleName);
                //        DataMod.dtaObject drn = DataMod.m_SDataMod.GetObject(DataMod.m_SDataMod.m_PtxRuleList, m_ComboPtxRule.SelectedItem.ToString());
                //        if (dro.m_Type != drn.m_Type)
                //        {
                //            String oldtype = "";
                //            String newtype = "";
                //            if (dro.m_Type == 0) oldtype = "Sprite"; else oldtype = "Model";
                //            if (drn.m_Type == 0) newtype  = "Sprite"; else newtype = "Model";
                //            String msg = "\nThis Event has " + oldtype + " Rule Evolution Data associated with it which is incompatible with the " + newtype + " Rule you've just selected.";
                //            msg += "\nDo you want to erase this data and continue with the " + newtype + " Rule?";
                //            DialogResult diagRes = MessageBox.Show(msg, "RMPTFX Warning Evolution Conflict", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3);
                //            if (diagRes != DialogResult.Yes)
                //                m_ComboPtxRule.Text = e.m_PtxRuleName;
                //        }
                //    }
                //}
                e.m_PtxRuleName = m_ComboPtxRule.Text;

                e.m_EmitRuleName = m_ComboEmitRule.SelectedItem.ToString();
//                e.m_TriggerTime = m_EvtStartTime.Value;

				// don't allow the end ratio to get too close to the start ratio
				if (m_EvtEndRatio.Value < m_EvtStartRatio.Value+0.01f)
				{
					m_EvtEndRatio.Value = m_EvtStartRatio.Value+0.01f;
				}

				e.m_StartRatio = m_EvtStartRatio.Value;
				e.m_EndRatio = m_EvtEndRatio.Value;
//                e.m_DurationScalarMin = m_EvtDurationScalarMin.Value;
//                e.m_DurationScalarMax = m_EvtDurationScalarMax.Value;
                e.m_TimeScalarMin = m_EvtPlaybackMin.Value;
                e.m_TimeScalarMax = m_EvtPlaybackMax.Value;
                e.m_ZoomMin= m_EvtZoomMin.Value;
                e.m_ZoomMax = m_EvtZoomMax.Value;
                e.m_ColorTintMin = m_ColorMinButton.ColorUp;
                e.m_ColorTintMax = m_ColorMaxButton.ColorUp;

                //TODO ALL TO GO:
                //if (m_ComboEventEvoBlendMode.SelectedIndex >= 0)
                //{
                    //e.m_EvoGroup.m_EvoMode = 0;// m_ComboEventEvoBlendMode.SelectedIndex;
                //}
                
                return;
            }

            //Effect Event
            if (m_CurrentEvent.m_Type == 1)
            {
                m_evtEffectDisplay.SetDataFromGui(m_CurrentEvent as DataMod.dtaEventEffect);
                return;
            }
        }
        public void BuildDomainTypeList()
        {
            m_evtEffectDisplay.BuildDomainTypeList();
        }
        public void BuildGameSpecificData(DataMod.dtaEffectRuleStd doe)
        {
            //right now we just have a bunch of tick boxes
            if (doe == null) return;
            
            //Build the interface
            m_PanelGameData.SuspendLayout();
            m_PanelGameData.Controls.Clear();
            int top = 0;
            for (int i = 0; i < doe.m_GameSpecificFlags.Count;i++ )
            {
                DataMod.dtaGameSpecificDataBool dta = doe.m_GameSpecificFlags[i] as DataMod.dtaGameSpecificDataBool;

                System.Windows.Forms.CheckBox box = new System.Windows.Forms.CheckBox();
                box.AutoSize = true;
                box.Text = dta.m_Name;
                box.Checked = dta.m_State;
                box.Left = 0;
                box.Top = top;
                box.Tag = dta;
                box.Click +=new EventHandler(m_GameSpecificData_Changed);
                m_PanelGameData.Controls.Add(box);
                top += box.Height;
            }
            m_PanelGameData.ResumeLayout();
            if (m_PanelGameData.Controls.Count > 0)
                m_SubSectionGameSpecific.Visible = true;
            else
                m_SubSectionGameSpecific.Visible = false;
            m_SubSectionGameSpecific.SetHeight(m_PanelGameData.Height+m_PanelGameData.Top);
            
        }
        public void ChangedEffect(String name)
        {
            if (name == "") return;
            DataCon.m_SDataCon.SendEffectRule(name);
            //DataMod.dtaObject dob = DataMod.m_SDataMod.GetObject(DataMod.m_SDataMod.m_EffectRuleList, name);
            //if (dob != null)
            //{
            //    if (!dob.m_NeedsSave)
            //    {
            //        dob.m_NeedsSave = true;
            //        main.m_MainForm.BuildEffectList();
            //    }
            //}

        }
        public void m_Timeline_OnValueChanged(ptxSlider slider)
        {
            UpdateTimeline();
        }
        public void EnableSubSections(bool b)
        {
            foreach (ptxSubSection sub in this.m_PanelSection.Controls)
                sub.Enabled = b;
        }

        private void BuildPtxRuleDropDown()
        {
            String curSel = m_ComboPtxRule.Text;
            m_ComboPtxRule.SuspendLayout();
            m_ComboPtxRule.Items.Clear();
            if (m_CurrentEvent is DataMod.dtaEventEmitter)
            {
                m_ComboPtxRule.Items.Add((m_CurrentEvent as DataMod.dtaEventEmitter).m_PtxRuleName);
                curSel = (m_CurrentEvent as DataMod.dtaEventEmitter).m_PtxRuleName;
            }
            foreach (DataMod.dtaObject dob in DataMod.m_SDataMod.m_PtxRuleList)
            {
                bool showsprites = true;
                bool showmodels = true;

                if ((!m_ButtonEXFilterModels.ButtonState) || (!m_ButtonEXFilterSprites.ButtonState))
                {
                    showmodels = !m_ButtonEXFilterModels.ButtonState;
                    showsprites = !m_ButtonEXFilterSprites.ButtonState;
                }

                //if ((showsprites) && (dob.m_Type == 0))
                //    if(dob.ToString()!= curSel)
                //        m_ComboPtxRule.Items.Add(dob.ToString());
                //if ((showmodels) && (dob.m_Type == 1))
                //    if(dob.ToString()!=curSel)
                //        m_ComboPtxRule.Items.Add(dob.ToString());
                if ((showmodels && showsprites))
                    if (dob.ToString() != curSel)
                        m_ComboPtxRule.Items.Add(dob.ToString());
            }
            m_ComboPtxRule.ResumeLayout();
            m_ComboPtxRule.Text = curSel;
        }
        private void BuildEvoBlendModeDropDown()
        {
        }

        private DataMod.KeyframeSpec GetSelectedKeyframeCellFromDataGrid(DataGridView grid)
        {
            if (grid.CurrentCell == null) return null;
            DataMod.KeyframeSpec key = grid.CurrentCell.Tag as DataMod.KeyframeSpec;
            return key;

        }
        private DataMod.dtaEvolution GetSelectedEvolutionFromDataGrid(DataGridView grid)
        {
            if (grid.CurrentRow == null) return null;
            DataMod.dtaEvolution evo = grid.CurrentRow.Cells[1].Tag as DataMod.dtaEvolution;
            return evo;

        }

        private void SelectOnRightMouseButton_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                DataGridView grid = sender as DataGridView;
                grid.CurrentCell = grid.Rows[e.RowIndex].Cells[e.ColumnIndex];
            }

        }
        private void DataGridAutoSelectValidCell(object sender, EventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid.CurrentCell == null) return;
            if (grid.CurrentCell.Value == null)
                grid.CurrentCell = grid.Rows[grid.CurrentCell.RowIndex].Cells[0];
        }


        public  void m_CheckBox_Click(object sender, EventArgs e)
        {
            UpdateCullingInterface();
            UpdateCullingNewInterface();
            SetDataFromGui();
            ChangedEffect(m_CurrentEffectName);
        }
        
        public void m_GameSpecificData_Changed(object sender, EventArgs e)
        {
            if(sender is CheckBox)
            {
                CheckBox box = sender as CheckBox;
                DataMod.dtaGameSpecificDataBool dta = box.Tag as DataMod.dtaGameSpecificDataBool;
                dta.m_State = box.Checked;
            }
            SetDataFromGui();
            ChangedEffect(m_CurrentEffectName);
        }
        public  void m_Slider_OnValueChanged(ptxSlider slider)
        {
			// make sure all effects last at least 0.1 seconds
			if (m_DurationMin.Value < 0.1f)
			{
				m_DurationMin.Value = 0.1f;
			}

			if (m_DurationMax.Value < 0.1f)
			{
				m_DurationMax.Value = 0.1f;
			}

            //do some convenience clamps (ensure min is not > max but changing the max can lower the min)
            if (m_DurationMin.Value >    m_DurationMax.Value)
                m_DurationMin.SetValue = m_DurationMax.Value;
            if (m_DurationMax.Value <    m_DurationMin.Value)
                m_DurationMax.SetValue = m_DurationMin.Value;
            
            if (m_PlayBackMin.Value >    m_PlayBackMax.Value)
                m_PlayBackMin.SetValue = m_PlayBackMax.Value;
            if (m_PlayBackMax.Value <    m_PlayBackMin.Value)
                m_PlayBackMax.SetValue = m_PlayBackMin.Value;

//            if (m_EvtDurationScalarMin.Value >    m_EvtDurationScalarMax.Value)
//                m_EvtDurationScalarMin.SetValue = m_EvtDurationScalarMax.Value;
//            if (m_EvtDurationScalarMax.Value <    m_EvtDurationScalarMin.Value)
//                m_EvtDurationScalarMax.SetValue = m_EvtDurationScalarMin.Value;

            if (m_EvtPlaybackMin.Value >    m_EvtPlaybackMax.Value)
                m_EvtPlaybackMin.SetValue = m_EvtPlaybackMax.Value;
            if (m_EvtPlaybackMax.Value <    m_EvtPlaybackMin.Value)
                m_EvtPlaybackMax.SetValue = m_EvtPlaybackMin.Value;

            SetDataFromGui();
            //SetEventDataFromGui();
            ChangedEffect(m_CurrentEffectName);
        }
        public void m_NumLoops_OnValueChanged(ptxSlider slider)
        {
            UpdateCullingNewInterface();
            SetDataFromGui();
            ChangedEffect(m_CurrentEffectName);
        }
        private void m_ButtonFreezeScale_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.SendFreezeScaleEvent(m_CurrentEffectName);
            m_Scale.SetValue = 100.0f;
        }

        private void ShowKeyframe(object sender, EventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid.CurrentCell == null) return;
            int cdx = grid.CurrentCell.ColumnIndex;
            String desc = "Effect: "+m_CurrentEffectName;
            if ( (cdx > 0) && (cdx<= m_DataGridEvoList.Rows.Count))
                desc += ": Evolution ("+m_DataGridEvoList.Rows[cdx-1].Cells[1].Value.ToString()+")";
            
            KeyframeEditorFormBase.ShowKeyframe(GetSelectedKeyframeCellFromDataGrid(sender as DataGridView),desc+":");
        }

        private void m_ComboPtxRule_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetEventDataFromGui();
            ChangedEffect(m_CurrentEffectName);

            if (m_ComboPtxRule.SelectedItem != null)
                main.m_MainForm.SelectAndRequestPtxRule(m_ComboPtxRule.SelectedItem.ToString());
            if (m_ComboEmitRule.SelectedItem != null)
                main.m_MainForm.SelectAndRequestEmitter(m_ComboEmitRule.SelectedItem.ToString());
            main.m_MainForm.RequestEffect(m_CurrentEffectName);
            main.m_MainForm.DisplayHeirarchy(DataMod.GetEffectRule(m_CurrentEffectName) as DataMod.dtaEffectRuleStd);
        }

        private void m_ComboPtxRule_DropDown(object sender, EventArgs e)
        {
            BuildPtxRuleDropDown();
        }
        private void m_ComboEmitRule_DropDown(object sender, EventArgs e)
        {
            String curSel = m_ComboEmitRule.Text;
            m_ComboEmitRule.SuspendLayout();
            m_ComboEmitRule.Items.Clear();
            for (int i = 0; i < DataMod.m_SDataMod.m_EmitRuleList.Count; i++)
                m_ComboEmitRule.Items.Add(DataMod.m_SDataMod.m_EmitRuleList[i]);
            m_ComboEmitRule.ResumeLayout();
            m_ComboEmitRule.Text = curSel;
        }

        private void m_ContextMenuEvoList_Opening(object sender, CancelEventArgs e)
        {
            DataMod.dtaEvolution evo = GetSelectedEvolutionFromDataGrid(m_DataGridEvoList);
            
            m_MenuItemNewEvo.Visible = false;
            m_MenuItemSelectedEvo.Visible = false;
            m_MenuItemRenameEvo.Visible = false;
            m_MenuItemDelEvo.Visible = false;
            m_MenuItemCloneEvo.Visible = false;
            m_MenuItemSaveEvo.Visible = false;
            if(evo != null)
            {
                int idx = m_DataGridEvoList.CurrentRow.Index+1;
                m_MenuItemSelectedEvo.Text = "Selected: " + "("+idx.ToString()+") \""+evo.m_Name+"\"";
                m_MenuItemSelectedEvo.Visible = true;
                m_MenuItemRenameEvo.Visible = true;
                m_MenuItemDelEvo.Visible = true;
                m_MenuItemSelectedEvo.Tag = evo.m_Name;
                m_MenuItemSaveEvo.Visible = true;

            }
            if (m_DataGridEvoList.Rows.Count < 6)
                m_MenuItemNewEvo.Visible = true;


        }
        private void m_ContextMenuKeyProp_Opening(object sender, CancelEventArgs e)
        {
            DataMod.dtaEffectRuleStd doe = DataMod.GetEffectRule(m_CurrentEffectName) as DataMod.dtaEffectRuleStd;
            if (doe == null){ e.Cancel = true; return;}

            DataMod.dtaPtxKeyframeProp keyProp = main.m_MainForm.GetSelectedKeyframePropFromDataGrid(m_DataGridEffectKeyframeList);
            if (keyProp == null) { e.Cancel = true; return; }
          
            main.m_MainForm.GenerateKeyPropEvoMenu("Effect", m_ContextMenuKeyProp, doe.m_EvolutionList, keyProp, new EventHandler(KeyProp_Menu_AddEvo_Click), new EventHandler(KeyProp_Menu_DelEvo_Click));
        }

     
        private void KeyProp_Menu_AddEvo_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            String evoname = item.Tag as String;

            DataMod.dtaPtxKeyframeProp keyprop = m_ContextMenuKeyProp.Tag as DataMod.dtaPtxKeyframeProp;
            if (keyprop == null) return;
            DataCon.m_SDataCon.SendAddPropEvolutionEvent(m_CurrentEffectName, evoname,-1, keyprop);  
        }
        private void KeyProp_Menu_DelEvo_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            String evoname = item.Tag as String;
            DataMod.dtaPtxKeyframeProp keyprop = m_ContextMenuKeyProp.Tag as DataMod.dtaPtxKeyframeProp;
            if (keyprop == null) return;
            DataCon.m_SDataCon.SendDelPropEvolutionEvent(m_CurrentEffectName, evoname,-1, keyprop);  
        }

        private void m_MenuItemNewEvo_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.SendCreateEvolutionEvent(m_CurrentEffectName, "");
        }
        private void m_MenuItemDelEvo_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.SendDeleteEvolutionEvent(m_CurrentEffectName, m_MenuItemSelectedEvo.Tag as String);
        }

        private void m_DataGridEvoList_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataMod.dtaEffectRuleStd doe = DataMod.GetEffectRule(m_CurrentEffectName) as DataMod.dtaEffectRuleStd;
            if (doe == null) return;
            DataGridViewTextBoxCell cell = m_DataGridEvoList.Rows[e.RowIndex].Cells[e.ColumnIndex] as DataGridViewTextBoxCell;
            if(cell == null) return;
            DataMod.dtaEvolution evo = cell.Tag as DataMod.dtaEvolution;
            if(evo == null) return;
           
            //TODO: -=Check for duplicates
            doe.RenameEvolution(evo.m_Name, cell.Value as String);
            DataCon.m_SDataCon.SendEffectRule(m_CurrentEffectName);
            DisplayEvolutionData(doe);
            DisplayTimelineEvent(m_CurrentEvent,false,false);
            //main.m_MainForm.SelectAndRequestEffect(m_CurrentEffectName);
            main.m_MainForm.m_EvolutionForm.DisplayEvolution(doe);
            main.m_MainForm.m_EmitterWindow.DisplayEmitter(main.m_MainForm.m_EmitterWindow.m_CurrentEmitterName);
            main.m_MainForm.m_PtxRuleWindow.DisplayRule(main.m_MainForm.m_PtxRuleWindow.m_CurrentPtxName);
        }

        private void m_ButtonEXFilterSprites_OnButtonStateChangedByUser(object sender, RageUserControls.ButtonEX.ButtonEXEventArgs e)
        {
            if(!e.ButtonState)
                m_ButtonEXFilterModels.ButtonState = true;
            BuildPtxRuleDropDown();
        }

        private void m_ButtonEXFilterModels_OnButtonStateChangedByUser(object sender, RageUserControls.ButtonEX.ButtonEXEventArgs e)
        {
            if (!e.ButtonState)
                m_ButtonEXFilterSprites.ButtonState = true;
            BuildPtxRuleDropDown();

        }

        public void Deselect()
        {
            if (m_CurrentEffectName == null || m_CurrentEffectName == String.Empty)
            {
                return;
            }

            DataMod.dtaEffectRuleStd doe = DataMod.GetEffectRule(m_CurrentEffectName) as DataMod.dtaEffectRuleStd;
            if (doe == null)
            {
                return;
            }
            
            foreach(DataMod.dtaEvent evt in doe.m_TimeLine.m_Events)
            {
                evt.m_ActiveToggle = true;
            }
            ChangedEffect(m_CurrentEffectName);
        }

        private void m_DataGridEffectKeyframeList_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            main.m_MainForm.PropertyDataGrid_CurrentCellDirtyStateChanged(sender, e);

        }

        private void m_DataGridEffectKeyframeList_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataMod.guiPropEventData guievt = new DataMod.guiPropEventData(); 
            if(main.m_MainForm != null)
                main.m_MainForm.PropertyDataGrid_CellValueChanged(sender,guievt,e);
            if(guievt.InfoChanged)
            {
                SetDataFromGui();
                ChangedEffect(m_CurrentEffectName);
            }

        }

        private void m_CreateLodsBtn_Click(object sender, EventArgs e)
        {
            // see if it already has an LOD evo
            DataMod.dtaEffectRuleStd doe = DataMod.GetEffectRule(m_CurrentEffectName) as DataMod.dtaEffectRuleStd;

            bool hasLod = false;
            foreach(DataMod.dtaEvolution evo in doe.m_EvolutionList.m_Evolutions)
            {
                if (String.Compare(evo.m_Name, "LOD", true) == 0)
                {
                    hasLod = true;
                    break;
                }
            }

            if (hasLod)
            {
                if (System.Windows.Forms.MessageBox.Show(
                    String.Format("The effect '{0}' already has an LOD evolution defined.\nPress OK to delete the exiting LOD and create a new one.\nPress Cancel to keep the exiting LOD", m_CurrentEffectName),
                    "Warning: LOD evolution already exists!",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Warning) != DialogResult.OK ) 
                {
                    return;
                }
            }

            DataCon.m_SDataCon.SendCreateEvolutionEvent(m_CurrentEffectName, "LOD");
        }

        private void m_MenuItemLoadEvo_Click(object sender, EventArgs e)
        {
            OpenFileDialog diag = new OpenFileDialog();
            diag.InitialDirectory = main.m_MainForm.m_LastEvoDir;
            diag.Filter = "Effect Evolution (*.effevo)|*.effevo";
            diag.CheckFileExists = true;
            if (diag.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            String path = System.IO.Path.GetDirectoryName(diag.FileName);
            main.m_MainForm.m_LastEvoDir = path;

            DataCon.m_SDataCon.RequestLoadEvolution(m_CurrentEffectName, -1, diag.FileName);
        }

        private void m_MenuItemSaveEvo_Click(object sender, EventArgs e)
        {
            SaveFileDialog diag = new SaveFileDialog();
            diag.InitialDirectory = main.m_MainForm.m_LastEvoDir;
            diag.Filter = "Effect Evolution (*.effevo)|*.effevo";
            if (diag.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            String path = System.IO.Path.GetDirectoryName(diag.FileName);
            main.m_MainForm.m_LastEvoDir = path;

            DataCon.m_SDataCon.RequestSaveEvolution(m_CurrentEffectName, -1, m_MenuItemSelectedEvo.Tag as String, diag.FileName);
        }

        private void m_ComboDrawList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDataFromGui();
            ChangedEffect(m_CurrentEffectName);
        }

        private void m_ComboCullMode_SelectionChangeCommitted(object sender, EventArgs e)
        {
            UpdateCullingNewInterface();
            SetDataFromGui();
            ChangedEffect(m_CurrentEffectName);
        }  

        private void m_ColorMinButton_Click(object sender, EventArgs e)
        {
            Sano.PersonalProjects.ColorPicker.Controls.ColorPickerDialog dlg = new Sano.PersonalProjects.ColorPicker.Controls.ColorPickerDialog();
            dlg.CurrentColor = m_ColorMinButton.ColorDown;
            dlg.ColorChanged += delegate
            {
                m_ColorMinButton.SetColor(dlg.CurrentColor);
            };

            dlg.ShowDialog();

            m_ColorMinButton.SetColor(dlg.CurrentColor);

            SetDataFromGui();
            ChangedEffect(m_CurrentEffectName);
        }

        private void m_ColorMinButton_OnButtonStateChangedByUser(object sender, RageUserControls.ButtonEX.ButtonEXEventArgs e)
        {
            Sano.PersonalProjects.ColorPicker.Controls.ColorPickerDialog dlg = new Sano.PersonalProjects.ColorPicker.Controls.ColorPickerDialog();
            dlg.CurrentColor = m_ColorMinButton.ColorDown;
            dlg.ColorChanged += delegate
            {
                m_ColorMinButton.SetColor(dlg.CurrentColor);
                SetDataFromGui();
                ChangedEffect(m_CurrentEffectName);
            };

            dlg.ShowDialog();

            m_ColorMinButton.SetColor(dlg.CurrentColor);
            m_ColorMinButton.ButtonState = true;

            SetDataFromGui();
            ChangedEffect(m_CurrentEffectName);
        }

        private void m_ColorMaxButton_OnButtonStateChangedByUser(object sender, RageUserControls.ButtonEX.ButtonEXEventArgs e)
        {
            Sano.PersonalProjects.ColorPicker.Controls.ColorPickerDialog dlg = new Sano.PersonalProjects.ColorPicker.Controls.ColorPickerDialog();
            dlg.CurrentColor = m_ColorMaxButton.ColorDown;
            dlg.ColorChanged += delegate
            {
                m_ColorMaxButton.SetColor(dlg.CurrentColor);
                SetDataFromGui();
                ChangedEffect(m_CurrentEffectName);

            };

            dlg.ShowDialog();

            m_ColorMaxButton.SetColor(dlg.CurrentColor);
            m_ColorMaxButton.ButtonState = true;

            SetDataFromGui();
            ChangedEffect(m_CurrentEffectName);
        }

		private void ptxSubSection1_Paint(object sender, PaintEventArgs e)
		{

		}

		private void m_ComboDataVolumeType_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetDataFromGui();
			ChangedEffect(m_CurrentEffectName);
		}
    }
}
