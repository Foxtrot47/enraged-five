﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using RSG.TrackViewer;
using RSG.TrackViewer.ViewModel;
using RSG.TrackViewer.Data;
using System.Windows.Media;
using System.Windows.Input;
using System.Diagnostics;
using rage;


namespace ParticleEditor
{
    /// <summary>
    /// Windows forms wrapper for WPF trackview control
    /// </summary>
    public partial class TrackViewerForm : TrackViewerFormBase
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public TrackViewerForm()
        {
            InitializeComponent();
            // nest our track viewer WPF control
            ElementHost elhost = new ElementHost();
            elhost.Size = new Size(this.Width - 100, this.Height - 100);
            elhost.Location = new Point(45, 35);

            m_Tv = new TrackViewer();
            m_Tgc = new TrackGroupCollection();
            m_Tg = new TrackGroup("Effect");
            
            m_Tv.TM.TrackGroupCollectionViewModel.SetTrackGroupCollection(m_Tgc);
            m_Tgc.Add(m_Tg);

            
            elhost.Child = m_Tv;

            this.Controls.Add(elhost);

            m_Tv.TM.CommonData.PropertyChanged += new PropertyChangedEventHandler(SendNewKeys);
        }

        #region Constants
        
        const int MAX_COLORS = 10;
        static System.Windows.Media.Brush[] sColors = new System.Windows.Media.Brush[MAX_COLORS] { System.Windows.Media.Brushes.Red, System.Windows.Media.Brushes.Green, System.Windows.Media.Brushes.Blue, System.Windows.Media.Brushes.Pink, System.Windows.Media.Brushes.AntiqueWhite,
                                                                    System.Windows.Media.Brushes.Black, System.Windows.Media.Brushes.Brown, System.Windows.Media.Brushes.Cornsilk, System.Windows.Media.Brushes.Orange, System.Windows.Media.Brushes.Olive};
        #endregion

        #region Private Functions

        void SendNewKeys(object sender, PropertyChangedEventArgs e)
        {
            if (m_Tv.TM.TrackGroupCollectionViewModel.TrackGroupViewModels.Count > 0)
            {
                List<Float5> f5s = ConvertTrackGroupToFloat5s(m_Tg);
                DataMod.KeyframeData data = new DataMod.KeyframeData();

                foreach (Float5 f5 in f5s)
                {
                    DataMod.KeyframeData.Key k = new DataMod.KeyframeData.Key();
                    k.Time = f5.t;
                    k.Data = new float[4];
                    k.Data[0] = f5.x;
                    k.Data[1] = f5.y;
                    k.Data[2] = f5.z;
                    k.Data[3] = f5.w;
                    data.m_KeyData.Add(k);
                }

                Debug.Assert(data.m_KeyData.Count > 0);

				DataCon.m_SDataCon.SendKeyframeData(m_RuleName, m_RuleType, m_PropertyId, m_EventIdx, m_EvoIdx, data);
            }
            
        }

        #endregion // Private Functions

        #region Static Functions
        /// <summary>
        /// Convert data to track format required for serialisation.   This is taken almost wholesale
        /// from the old system which used the KeyframeEditor
        /// </summary>
        private void ConvertFloat5sToTrackGroup(List<Float5> data)
        {

            int numCurves = m_Tg.Tracks.Count;
            int keyCount = 0;
            foreach (Float5 key in data)
            {
                if (numCurves > 0)
                {
                    Track track = m_Tg.Tracks[0];
                    double val = rage.math.RampValueUnclamped(key.x, m_Tv.TM.VertTimeline.DisplayMin, m_Tv.TM.VertTimeline.DisplayMax, 0.0, 1.0);
                    track.AddPoint(new FloatTrackPoint(key.t, val));
                    if(keyCount == 0)
                        track.GetPoint(keyCount).IsLockedX = true;
                }
                if (numCurves > 1)
                {
                    Track track = m_Tg.Tracks[1];
                    double val = rage.math.RampValueUnclamped(key.y, m_Tv.TM.VertTimeline.DisplayMin, m_Tv.TM.VertTimeline.DisplayMax, 0.0, 1.0);
                    track.AddPoint(new FloatTrackPoint(key.t, val));
                    if (keyCount == 0)
                        track.GetPoint(keyCount).IsLockedX = true;
                }
                if (numCurves > 2)
                {
                    Track track = m_Tg.Tracks[2];
                    double val = rage.math.RampValueUnclamped(key.z, m_Tv.TM.VertTimeline.DisplayMin, m_Tv.TM.VertTimeline.DisplayMax, 0.0, 1.0);
                    track.AddPoint(new FloatTrackPoint(key.t, val));
                    if (keyCount == 0)
                        track.GetPoint(keyCount).IsLockedX = true;
                }
                if (numCurves > 3)
                {
                    Track track = m_Tg.Tracks[3];
                    double val = rage.math.RampValueUnclamped(key.w, m_Tv.TM.VertTimeline.DisplayMin, m_Tv.TM.VertTimeline.DisplayMax, 0.0, 1.0);
                    track.AddPoint(new FloatTrackPoint(key.t, val));
                    if (keyCount == 0)
                        track.GetPoint(keyCount).IsLockedX = true;
                }
                ++keyCount;
            }


            // now for each keyframe beyond the first, delete it if it's just a lerp
            // between the neighbors
            foreach (Track track in m_Tg.Tracks)
            {
                for (int i = 1; i < track.NumPoints(); i++)
                {
                    TrackPoint curr = track.GetPoint(i);
                    TrackPoint prev = track.GetPoint(i - 1);
                    if (i + 1 == track.NumPoints())
                    {
                        if (prev.Y == curr.Y)
                        {
                            track.RemovePointAt(i);
                            i--;
                            continue;
                        }
                    }
                    else
                    {
                        TrackPoint next = track.GetPoint(i + 1);
                        // get lerp value
                        float dx = (float)(next.X - prev.X);
                        float dy = (float)(next.Y - prev.Y);

                        float t = (float)((curr.X - prev.X) / dx);
                        float lerpVal = (float)(t * dy + prev.Y);

                        if (System.Math.Abs(lerpVal - curr.Y) < 0.001f)
                        {
                            track.RemovePointAt(i);
                            i--;
                            continue;
                        }
                    }
                }
            }
        }
        

        /// <summary>
        /// Convert tracks to data format required for serialisation.   This is taken almost wholesale
        /// from the old system which used the KeyframeEditor
        /// </summary>
        private List<Float5> ConvertTrackGroupToFloat5s(TrackGroup tg)
        {
            List<Float5> keyframes = new List<Float5>();

            int numCurves = System.Math.Min(4, tg.Tracks.Count);

            int[] currKey = new int[numCurves];

            // Basic algorithm:
            // Find the smallest time T out of all the currKeys
            // create a new keyframe at this time
            // If currKey[i] is close enough to T, snap it to T
            // for each curve, get value at T.
            // increment currKey for any currKeys near T.

            List<Track> tempTracks = new List<Track>();
            foreach(Track t in tg.Tracks)
            {
                Track newTrack = new Track(t.Name);
                foreach(TrackPoint tp in t.Points)
                    newTrack.AddPoint(new FloatTrackPoint(tp));
                
                tempTracks.Add(newTrack);
            }

            bool done = false;
            while (!done)
            {

                float minT = System.Single.MaxValue;
                for (int i = 0; i < numCurves; i++)
                {
                    Track track = tempTracks[i];
                    if (currKey[i] >= track.NumPoints())
                    {
                        continue;
                    }
                    TrackPoint currVal = track.GetPoint(currKey[i]);
                    if (currVal.X < minT)
                    {
                        minT = (float)currVal.X;
                    }
                }
                if (minT == System.Single.MaxValue)
                {
                    done = true;
                    break;
                }

                // Now snap nearby keys onto the minT value
                float epsilon = 0.01f;
                for (int i = 0; i < numCurves; i++)
                {
                    Track track = tempTracks[i];
                    if (currKey[i] >= track.NumPoints())
                    {
                        continue;
                    }
                    TrackPoint currVal = track.GetPoint(currKey[i]);
                    if (currVal.X < minT + epsilon)
                    {
                        currVal.X = minT;
                    }
                }

                Float5 newKey = new Float5();
                newKey.t = minT;

                float[] keyVals = new float[4];

                for (int i = 0; i < numCurves; i++)
                {
                    if (numCurves > i)
                    {
                        Track track = tempTracks[i];
                        keyVals[i] = track.GetYValAt(currKey[i], minT);
                        if (currKey[i] < track.NumPoints() && minT == track.GetPoint(currKey[i]).X)
                        {
                            currKey[i]++; // move to the next key
                        }
                    }
                }
                newKey.x = keyVals[0] * (float)(m_Tv.TM.CommonData.YMax - m_Tv.TM.CommonData.YMin);
				newKey.y = keyVals[1] * (float)(m_Tv.TM.CommonData.YMax - m_Tv.TM.CommonData.YMin);
				newKey.z = keyVals[2] * (float)(m_Tv.TM.CommonData.YMax - m_Tv.TM.CommonData.YMin);
				newKey.w = keyVals[3] * (float)(m_Tv.TM.CommonData.YMax - m_Tv.TM.CommonData.YMin);

                keyframes.Add(newKey);
            }

            return keyframes;

        }
        #endregion

        #region Public Functions


        /// <summary>
        /// Populate our tracks with point data that has been load via the DataMod system
        /// </summary>
        public override void SetInitialData(DataMod.KeyframeData data)
        {
            List<Float5> float5s = new List<Float5>();
            foreach (DataMod.KeyframeData.Key k in data.m_KeyData)
            {
                Float5 f5 = new Float5();
                f5.t = k.Time;
                f5.x = k.Data[0];
                f5.y = k.Data[1];
                f5.z = k.Data[2];
                f5.w = k.Data[3];
                float5s.Add(f5);
            }

           ConvertFloat5sToTrackGroup(float5s);
           m_Tv.FramePoints(false);
        }

        /// <summary>
        ///Construct a single track group (the vfx editor only ever seems to require one) and build the relevant number
        /// of tracks
        /// </summary>
        public override void ConfigureEditor(DataMod.KeyframeUiData uidata)
        {
            float min = 10000.0f;
            float max = -10000.0f;
            int i=0;
            foreach (string lbl in uidata.Labels)
            {
                if (lbl != "")
                {
                    Track t = new Track(lbl);
                    m_Tg.Add(t);

                    if (uidata.YMin < min)
                        min = uidata.YMin;
                    if (uidata.XMax > max)
                        max = uidata.YMax;
                }
                i++;
            }
            m_Tv.TM.CommonData.SetYRange(min, max);
            m_Tv.TM.VertTimeline.DisplayMin = min;
            m_Tv.TM.VertTimeline.DisplayMax = max;
            
        }

        #endregion // Public Functions
                
        #region Properties

        TrackViewer m_Tv;
        TrackGroupCollection m_Tgc;
        TrackGroup m_Tg;

        #endregion // Properties

        public class Float5
        {
            public float t, x, y, z, w;

            public Float5()
            {
                t = x = y = z = w = 0.0f;
            }
            public void Zero()
            {
                t = x = y = z = w = 0.0f;
            }
        }
    }
}
