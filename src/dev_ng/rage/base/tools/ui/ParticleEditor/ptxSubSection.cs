using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Ini;

namespace ParticleEditor
{
    public partial class ptxSubSection : System.Windows.Forms.Panel
    {

#region Variables
        public bool m_Expanded = true;
        private String m_HeaderText="";
        private String m_SectionName="";
        private System.Drawing.Size m_OriginalSize = new System.Drawing.Size(-1,-1);
#endregion
        
        
#region Properties
        public String HeaderText
        {
            get { return m_HeaderText; }
            set { m_HeaderText = value; SetTitle(); }
        }
        
        public String SectionName
        {
            get { return m_SectionName; }
            set { m_SectionName = value; SetTitle(); }
        }

        public bool Expanded
        {
            get { return m_Expanded; }
            set
            {
                if (m_Expanded != value)
                {
                    m_Expanded = value;
                    CalculateSize();
                }
            }
        }
        public delegate void ptxSubSectionEventDelegate(ptxSubSection channel);
        [Description("Occurs when user toggles expanded state"), Category("Action")]
        public event ptxSubSectionEventDelegate OnExpandStateChange = null;


#endregion

        public ptxSubSection()
        {
            InitializeComponent();
        }

        public void SetHeight(int height)
        {
            m_OriginalSize.Height = height;
            CalculateSize();
        }

        public int GetHeight()
        {
            if (m_Expanded)
                return m_OriginalSize.Height;
            return m_HeaderPanel.Height;
        }

        private void SetTitle()
        {
            //Done this way to avoid flicker
            if(m_HeaderText.Length>0)
                 m_Title.Text = m_SectionName + "  " + m_HeaderText;
            else
                 m_Title.Text = m_SectionName;
        }
        public virtual void SaveConfigData(IniFile file)
        {
            file.IniWriteValue(m_SectionName, "EXPANDED", m_Expanded.ToString());
        }
        public virtual void LoadConfigData(IniFile file)
        {
            Expanded = bool.Parse(file.IniReadValueDefault(m_SectionName, "EXPANDED",m_Expanded.ToString()));
        }

        private void CalculateSize()
        {
            if (m_Expanded)
                Size = new System.Drawing.Size(Width, m_OriginalSize.Height);  
            else
                Size = new System.Drawing.Size(Width, m_HeaderPanel.Height);
            Invalidate();
        }
        
        private void ptxSubSection_Paint(object sender, PaintEventArgs e)
        {
            if (m_Expanded)
                m_ButtonExpand.Text = "6";
            else
                m_ButtonExpand.Text = "3";
        }

        private void m_Expand_MouseClick(object sender, MouseEventArgs e)
        { 
            Expanded = !m_Expanded;
            if (OnExpandStateChange != null)
                OnExpandStateChange(this);
        }

        private void ptxSubSection_ControlAdded(object sender, ControlEventArgs e)
        {

        }

        private void ptxSubSection_Layout(object sender, LayoutEventArgs e)
        {

        }

        private void ptxSubSection_ClientSizeChanged(object sender, EventArgs e)
        {
            if(sender == Parent)
                m_OriginalSize = new System.Drawing.Size(Width, Height);
        }

        private void ptxSubSection_SizeChanged(object sender, EventArgs e)
        {
            if (m_OriginalSize.Width == -1)
                m_OriginalSize = new System.Drawing.Size(Width, Height);
        }

        private void m_HeaderPanel_DoubleClick(object sender, EventArgs e)
        {
            Expanded = !Expanded;
        }



    }
}
