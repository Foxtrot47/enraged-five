using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
    public partial class Saving : Form
    {
        public Saving()
        {
            InitializeComponent();
            
        }

 
        private void timer1_Tick(object sender, EventArgs e)
        {
            if ( resetFocus)
            {
                this.Focus();
                this.TopMost = true;
                resetFocus = false; 
            }
            if (!DataCon.m_SDataCon.IsConnected())
            {
                this.button1.Text = "Connection Lost";
                //Stop(); 
            }
        }
        public void Start()
        {
            if (DataCon.m_SDataCon.IsConnected())
            {
                this.progressBar1.Maximum = 0;
                this.progressBar2.Maximum = 100;
                this.progressBar3.Maximum = 100;
                this.progressBar1.Value = 0;
                this.progressBar2.Value = 0;
                this.progressBar3.Value = 0;
                this.textBox1.Text = "Effect Rules ";
                this.textBox2.Text = "Emit Rules ";
                this.textBox3.Text = "Ptx Rules ";
                this.button1.Text = "Cancel";
                this.Show();
                this.Focus();
                this.timer1.Start();
                resetFocus = false;
            }
        }

        public void LoadFromByteBuffer(tcpByteBuffer buff)
        {
            int bar = buff.Read_s32(); 
            if (bar == 0)
            {
                this.progressBar1.Maximum = buff.Read_s32();
                this.progressBar1.Value = buff.Read_s32();
                this.textBox1.Text = "Effect Rules " + this.progressBar1.Value + " of " + this.progressBar1.Maximum; 
            }
            else if ( bar == 1 )
            {
                this.progressBar2.Maximum = buff.Read_s32();
                this.progressBar2.Value = buff.Read_s32();
                this.textBox2.Text = "Emit Rules " + this.progressBar2.Value + " of " + this.progressBar2.Maximum;
            }
            else if ( bar == 2 )
            {
                this.progressBar3.Maximum = buff.Read_s32();
                this.progressBar3.Value = buff.Read_s32();
                this.textBox3.Text = "Ptx Rules " + this.progressBar3.Value + " of " + this.progressBar3.Maximum;
            }
        }
        public void Finish()
        {
            this.button1.Text = "Save Complete";
        }
        public void Stop()
        {
            this.Hide();
            this.timer1.Stop();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Stop();
            //if ( this.button1.Text == "Cancel")
            {
                DataCon.m_SDataCon.CancelSaveAll();
            }
        }

        private void Saving_Load(object sender, EventArgs e)
        {

        }

        private void Saving_Leave(object sender, EventArgs e)
        {
            
        }

        private void ReturnFocus(object sender, EventArgs e)
        {
            resetFocus = true; 
        }


        bool resetFocus;

        private void progressBar1_MouseHover(object sender, EventArgs e)
        {

        }

        private void progressBar2_MouseHover(object sender, EventArgs e)
        {

        }

        private void progressBar3_MouseHover(object sender, EventArgs e)
        {

        } 
    }
}