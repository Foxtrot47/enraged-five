using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using RSG.Base.Network;

namespace ParticleEditor
{
    public partial class main : ptxForm
    {
        DataTable m_effectsData = null;
        DataTable m_emitterListData = null;
        DataTable m_ptxData = null;

        DataCon m_DataControl;

        public Playcontrol m_PlayControlForm = new Playcontrol();
        public Connection m_ConnectionForm = new Connection();
        public ProfileWindow m_ProfileForm = new ProfileWindow();
        public EvolutionWindow m_EvolutionForm = new EvolutionWindow();
        public Saving m_SavingPopup = new Saving(); 
        public static main m_MainForm = null;

        public String m_LastEffectRuleDir = "";
        public String m_LastEffectListDir = "";
        public String m_LastEmitRuleDir = "";
        public String m_LastPtxRuleDir = "";
        public String m_LastModelDir = "";
        public String m_LastEvoDir = "";

        private string m_effectRuleName;
        private string m_ptxRulName;

        Color m_ColorSelected = Color.DarkBlue;
        Color m_ColorIncluded = Color.Blue;
        ptxSlider m_forceEnable;

        public bool m_Connected = false;

        private List<string> m_emitterRuleNames = new List<string>();
        private List<string> m_ptxRuleNames = new List<string>();

        private List<string> m_emitterRuleNamesBold = new List<string>();
        private List<string> m_ptxRuleNamesBold = new List<string>();

        private DataMod.dtaObject m_currentEffectObject;

        public readonly cRemoteConsole m_remoteConsole = new cRemoteConsole();
        
        public main()
        {
            m_forceEnable = new ptxSlider();
            Controls.Add(m_forceEnable);
            m_forceEnable.BringToFront();

            m_Name = "Main";
            m_MainForm = this;
            m_WindowID = "MAINWINDOW";
            InitializeComponent();

            AddOwnedForm(m_PlayControlForm);
            AddOwnedForm(m_ConnectionForm);
            AddOwnedForm(m_ProfileForm);
            AddOwnedForm(m_EvolutionForm);

            m_PlayControlForm.ShowInTaskbar = false;
            m_ConnectionForm.ShowInTaskbar = false;
            m_ProfileForm.ShowInTaskbar = false;
            m_EvolutionForm.ShowInTaskbar = false;
            
            m_DataControl = new DataCon(this,tcpComClient1);
//            m_EmitterWindow.m_Duration.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(main.m_MainForm.m_EffectWindow.m_Timeline_OnValueChanged);
            
            m_DataGridEffects.ColumnHeadersVisible = false;
            m_DataGridEmitters.ColumnHeadersVisible = false;
            m_DataGridPtxList.ColumnHeadersVisible = false;
            InitTimeLine();

            m_EffectWindow.EnableSubSections(false);
            m_EmitterWindow.EnableSubSections(false);
            m_PtxRuleWindow.EnableSubSections(false);

            Text += "  (v"+DataMod.m_SDataMod.m_InterfaceVersion.ToString("0.00")+")";
       
            //this.m_UpdateTimer.Start();
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            m_forceEnable.SendToBack();
        }

        protected override void LoadConfigData()
        {
            m_LastEffectRuleDir = m_ConfigFile.IniReadValueDefault(m_WindowID, "LASTEFFECTRULEDIR", "");
            m_LastEmitRuleDir = m_ConfigFile.IniReadValueDefault(m_WindowID, "LASTEMITRULEDIR", "");
            m_LastPtxRuleDir = m_ConfigFile.IniReadValueDefault(m_WindowID, "LASTPTXRULEDIR", "");
            m_LastModelDir = m_ConfigFile.IniReadValueDefault(m_WindowID, "LASTMODELDIR", "");
            m_LastEffectListDir = m_ConfigFile.IniReadValueDefault(m_WindowID, "LASTEFFECTLISTDIR", "");
            m_LastEvoDir = m_ConfigFile.IniReadValueDefault(m_WindowID, "LASTEVODIR", "");

            foreach(ptxForm frm in OwnedForms)
            {
                frm.Visible = bool.Parse(m_ConfigFile.IniReadValueDefault(m_WindowID, "SHOW" + frm.m_WindowID, "true"));
                frm.ShowOnLoad = frm.Visible;
            }

            base.LoadConfigData();
        }

        protected override void SaveConfigData()
        {
            m_ConfigFile.IniWriteValue(m_WindowID, "LASTEFFECTRULEDIR", m_LastEffectRuleDir);
            m_ConfigFile.IniWriteValue(m_WindowID, "LASTEMITRULEDIR", m_LastEmitRuleDir);
            m_ConfigFile.IniWriteValue(m_WindowID, "LASTPTXRULEDIR", m_LastPtxRuleDir);
            m_ConfigFile.IniWriteValue(m_WindowID, "LASTMODELDIR", m_LastModelDir);
            m_ConfigFile.IniWriteValue(m_WindowID, "LASTEFFECTLISTDIR", m_LastEffectListDir);
            m_ConfigFile.IniWriteValue(m_WindowID, "LASTEVODIR", m_LastEvoDir);

            foreach (ptxForm frm in OwnedForms)
            {
                if(frm.Persistant)
                    m_ConfigFile.IniWriteValue(m_WindowID, "SHOW" + frm.m_WindowID, frm.ShowOnLoad.ToString());
            }

            base.SaveConfigData();
        }

        public void ClearInterface()
        {
            Clear(m_DataGridEffects); 
            m_effectsData = null;
            
            Clear(m_DataGridEmitters); 
            m_emitterListData = null;
            
            Clear(m_DataGridPtxList); 
            m_ptxData = null;
        }

        private void Clear(DataGridView dgv)
        {
            var source = dgv.DataSource as IDisposable;
            if (source != null)
            {
                source.Dispose();
            }

            dgv.DataSource = null;
        }

        public void UpdateProfile()
        {
            m_ProfileForm.UpdateProfiling();
        }

        public void UpdateDrawListNames()
		{
			m_EffectWindow.UpdateDrawListNames();
        }

		public void UpdateDataVolumeTypeNames()
		{
			m_EffectWindow.UpdateDataVolumeTypeNames();
		}

        public void UpdateProfilePtx()
        {
            m_PtxRuleWindow.UpdateProfiling();
        }

        public void DisplayEffect(String name)
        {
            DisplayEffect(name,true, true);
        }

        public void DisplayEffect(String name, bool asemitrule,bool asptxrule)
        {
            m_EffectWindow.Deselect();

            //Set up the timeline
            DataMod.dtaEffectRuleStd doe = DataMod.GetEffectRule(name) as DataMod.dtaEffectRuleStd;
            if (doe == null)
            {
                DataCon.m_SDataCon.RequestEffectInfo(name);
                return;
            }

            SelectEffect(name);
            int selid =0;
            if(name == m_EffectWindow.m_CurrentEffectName)
                selid = Math.Max(0,m_TimeLine.GetSelectedChannelIndex());

            m_TimeLine.Suspend();
            m_TimeLine.Clear();
            m_TimeLine.Duration = doe.m_DurationMax;
            foreach( DataMod.dtaEvent evt in doe.m_TimeLine.m_Events)
            {
//				Float2 duration = evt.GetDuration();
                ragPtxTimeLine.ptxTimeLine.ptxChannel channel;
                channel = new ragPtxTimeLine.ptxTimeLine.ptxChannel(evt.ToString(),evt.GetStartRatio(),evt.GetEndRatio(),evt.GetColor());
                m_TimeLine.AddChannel(channel);
                channel.m_Data = evt;
                evt.m_Data = channel;
            }
            m_TimeLine.SelectChannel(selid);
            m_TimeLine.Resume();

            m_EffectWindow.DisplayEffect(name,asemitrule,asptxrule);
            m_ProfileForm.SetEffect(name);
            m_EvolutionForm.DisplayEvolution(doe);
        }
        public void DisplayEmitter(String name)
        {
            SelectEmitter(name);
            m_EmitterWindow.DisplayEmitter(name);
            m_ProfileForm.SetEmitter(name);
        }
        public void DisplayPtxRule(String name)
        {
            SelectPtxRule(name);
            m_PtxRuleWindow.DisplayRule(name);
            m_ProfileForm.SetPtxRule(name);
        }
        
        public void SelectEffect(String name)
        {
            m_DataGridEffects.SelectionChanged -= m_DataGridEffects_SelectionChanged;
            m_DataGridEffects.Tag = "true";
            SelectRuleNameFromDataGrid(m_DataGridEffects, name);
            m_DataGridEffects.Tag = "";
            m_DataGridEffects.SelectionChanged += new EventHandler(m_DataGridEffects_SelectionChanged);
        }
        public void SelectEmitter(String name)
        {
            m_DataGridEmitters.SelectionChanged -= m_DataGridEmitters_SelectionChanged;
            SelectRuleNameFromDataGrid(m_DataGridEmitters, name);
            m_DataGridEmitters.SelectionChanged += new EventHandler(m_DataGridEmitters_SelectionChanged);
        }
        public void SelectPtxRule(String name)
        {
            m_DataGridPtxList.SelectionChanged -= m_DataGridPtxList_SelectionChanged;
            SelectRuleNameFromDataGrid(m_DataGridPtxList, name);
            m_DataGridPtxList.SelectionChanged += new EventHandler(m_DataGridPtxList_SelectionChanged);
        }

        public void RequestEffect(String name)
        {
            DataCon.m_SDataCon.RequestEffectInfo(name);
        }
        public void RequestEmitter(String name)
        {
            DataCon.m_SDataCon.RequestEmitterInfo(name);
        }
        public void RequestPtxRule(String name)
        {
            DataCon.m_SDataCon.RequestPtxRuleInfo(name);
        }

        public void SelectAndRequestEffect(String name)
        {
            SelectEffect(name);
            RequestEffect(name);
        }
        public void SelectAndRequestEmitter(String name)
        {
            SelectEmitter(name);
            RequestEmitter(name);
        }
        public void SelectAndRequestPtxRule(String name)
        {
            SelectPtxRule(name);
            RequestPtxRule(name);
        }

        public void BuildKeyframeGrid(DataGridView grid,DataMod.dtaPtxKeyframePropList proplist)
        {
            grid.SuspendLayout();
            grid.Rows.Clear();
            grid.Columns[7].Visible = false;
            foreach (DataMod.dtaPtxKeyframeProp prop in proplist.m_KeyframeProps)
            {
                int idx = grid.Rows.Add();
                grid.Rows[idx].Tag = prop;
                grid.Rows[idx].Cells[0].Value = prop.m_KeyframeSpec.DefnName;
                grid.Rows[idx].Cells[0].Tag = prop.m_KeyframeSpec;
            }
            grid.Sort(grid.Columns[0], ListSortDirection.Ascending);
            grid.ResumeLayout();
            SelectPropertyFromDataGrid(grid, "");

        }
        public void SelectPropertyFromDataGrid(DataGridView grid, String propName)
        {
            if (propName == "")
            {
                grid.CurrentCell = null;
                return;
            }

            System.Diagnostics.Debug.Assert(!String.IsNullOrEmpty(propName), "SelectPropertyFromDataGrid() does get called with a value. Who knew!?");

            foreach (DataGridViewRow row in grid.Rows)
            {
                if (row.Cells[1].Value.ToString() == propName)
                {
                    grid.CurrentCell = row.Cells[0];
                    return;
                }
            }
            grid.CurrentCell = null;
        }
        public void DisplayHeirarchy(DataMod.dtaEffectRuleStd doe)
		{
			if (doe == null) return;
            m_DataGridEffects.SelectionChanged -= m_DataGridEffects_SelectionChanged;
            m_DataGridEmitters.SelectionChanged -= m_DataGridEmitters_SelectionChanged;
            m_DataGridPtxList.SelectionChanged -= m_DataGridPtxList_SelectionChanged;
            //ClearCheckState(m_DataGridEmitters);
            //ClearCheckState(m_DataGridPtxList);
//            if (doe == null) return; // MN - moved this to the top to stop problems when right clicking on the timeline background
										// events would get deregistered but never set up again

            //if (doe.m_TimeLine.m_Events.Count == 0)
            //{
            //    DataGridUnBoldAll(m_DataGridEmitters);
            //    DataGridUnBoldAll(m_DataGridPtxList);
            //}

            m_emitterRuleNames.Clear();
            m_ptxRuleNames.Clear();
            m_emitterRuleNamesBold.Clear();
            m_ptxRuleNamesBold.Clear();

            foreach (DataMod.dtaEvent evt in doe.m_TimeLine.m_Events)
            {
                if (evt is DataMod.dtaEventEmitter)
                {
                    m_emitterRuleNames.Add((evt as DataMod.dtaEventEmitter).m_EmitRuleName);
                    m_ptxRuleNames.Add((evt as DataMod.dtaEventEmitter).m_PtxRuleName);

                    if (evt == m_EffectWindow.m_CurrentEvent)
                    {
                        m_emitterRuleNamesBold.Add((evt as DataMod.dtaEventEmitter).m_EmitRuleName);
                        m_ptxRuleNamesBold.Add((evt as DataMod.dtaEventEmitter).m_PtxRuleName);
                    }
                }
            }
            m_DataGridEffects.SelectionChanged += new EventHandler(m_DataGridEffects_SelectionChanged);
            m_DataGridEmitters.SelectionChanged += new EventHandler(m_DataGridEmitters_SelectionChanged);
            m_DataGridPtxList.SelectionChanged += new EventHandler(m_DataGridPtxList_SelectionChanged);

            m_DataGridEmitters.Invalidate();
            m_DataGridPtxList.Invalidate();
        }
        public void DisplayHeirarchy(String name)
        {
            DataMod.dtaEffectRuleStd dos = DataMod.GetEffectRule(name) as DataMod.dtaEffectRuleStd;
            if (dos != null)
                DisplayHeirarchy(dos);
        }

        public void RebuildInterface()
        {
            ClearInterface();
            BuildEffectList();
            BuildEmitterList();
            BuildPtxList();
        }

        /// <summary>
        /// Create a sortable table from an array list.
        /// </summary>
        /// <param name="arrayList">Array list.</param>
        /// <returns>Data table.</returns>
        private DataTable CreateTable(ArrayList arrayList)
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add("Value", typeof(DataMod.dtaObject));
            dataTable.Columns.Add("Name", typeof(string));

            foreach (DataMod.dtaObject obj in arrayList)
            {
                dataTable.Rows.Add(obj, obj.ToDisplayString());
            }

            dataTable.DefaultView.Sort = "Value";
            dataTable.AcceptChanges();

            return dataTable;
        }

        public void BuildEffectList()
        {
            m_DataGridEffects.AutoGenerateColumns = false;
            m_DataGridEffects.SelectionChanged -= m_DataGridEffects_SelectionChanged;
            m_DataGridEffects.SuspendLayout();
            String curRule = GetSelectedRuleNameFromDataGrid(m_DataGridEffects);

            string existingRowFilter = String.Empty;

            if (m_effectsData != null)
            {
                existingRowFilter = m_effectsData.DefaultView.RowFilter;
                m_effectsData.Dispose();
                m_effectsData = null;
            }

            m_effectsData = CreateTable(m_DataControl.m_DataModel.m_EffectRuleList);

            m_DataGridEffects.DataSource = null;
            m_DataGridEffects.DataSource = m_effectsData;
            m_effectsData.DefaultView.RowFilter = existingRowFilter;

            m_DataGridEffects.ResumeLayout();
            m_DataGridEffects.Refresh();

            m_DataGridEffects.SelectionChanged += new EventHandler(m_DataGridEffects_SelectionChanged);

			if (curRule == "")
			{
                curRule = GetSelectedRuleNameFromDataGrid(m_DataGridEffects);
			}

            SelectEffect(curRule);
            DisplayHeirarchy(curRule);

			DisplayEffect(curRule);
        }
        
        public void BuildEmitterList()
        {
            m_DataGridEmitters.AutoGenerateColumns = false;
            m_DataGridEmitters.SelectionChanged -= m_DataGridEmitters_SelectionChanged;
            m_DataGridEmitters.SuspendLayout();
            String curRule = GetSelectedRuleNameFromDataGrid(m_DataGridEmitters);

            bool addFormatterEvent = false;
            string existingRowFilter = String.Empty;

            if (m_emitterListData == null)
            {
                addFormatterEvent = true;
            }
            else
            {
                existingRowFilter = m_emitterListData.DefaultView.RowFilter;
                m_emitterListData.Dispose();
                m_emitterListData = null;                
            }

            m_emitterListData = CreateTable(m_DataControl.m_DataModel.m_EmitRuleList);

            m_DataGridEmitters.DataSource = null;
            m_DataGridEmitters.DataSource = m_emitterListData;
            m_emitterListData.DefaultView.RowFilter = existingRowFilter;

            m_DataGridEmitters.ResumeLayout();
            m_DataGridEmitters.SelectionChanged += new EventHandler(m_DataGridEmitters_SelectionChanged);
            SelectEmitter(curRule);

            if (addFormatterEvent)
            {
                m_DataGridEmitters.CellFormatting += new DataGridViewCellFormattingEventHandler(Emitters_CellFormatting);
            }
            DisplayHeirarchy(m_EffectWindow.m_CurrentEffectName);
        }
        
        public void BuildPtxList()
        {
            m_DataGridPtxList.AutoGenerateColumns = false;
            m_DataGridPtxList.SelectionChanged -= m_DataGridPtxList_SelectionChanged;
            m_DataGridPtxList.SuspendLayout();
            String curRule = GetSelectedRuleNameFromDataGrid(m_DataGridPtxList);

            bool addFormatterEvent = false;
            string existingRowFilter = String.Empty;

            if (m_ptxData == null)
            {
                addFormatterEvent = true;
            }
            else
            {
                existingRowFilter = m_ptxData.DefaultView.RowFilter;
                m_ptxData.Dispose();
                m_ptxData = null;
            }

            m_ptxData = CreateTable(m_DataControl.m_DataModel.m_PtxRuleList);

            m_DataGridPtxList.DataSource = null;
            m_DataGridPtxList.DataSource = m_ptxData;
            m_ptxData.DefaultView.RowFilter = existingRowFilter;

            m_DataGridPtxList.ResumeLayout();
            m_DataGridPtxList.SelectionChanged += new EventHandler(m_DataGridPtxList_SelectionChanged);
            SelectPtxRule(curRule);

            if (addFormatterEvent)
            {
                m_DataGridPtxList.CellFormatting += new DataGridViewCellFormattingEventHandler(Emitters_CellFormatting);
            }
            DisplayHeirarchy(m_EffectWindow.m_CurrentEffectName);
        }


        void Emitters_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var dg = sender as DataGridView;

            if (e.Value is ParticleEditor.DataMod.dtaObject)
            {
                var dta = e.Value as ParticleEditor.DataMod.dtaObject;

                var list = dg.Name == m_DataGridPtxList.Name ? m_ptxRuleNames : m_emitterRuleNames;
                var boldList = dg.Name == m_DataGridPtxList.Name ? m_ptxRuleNamesBold : m_emitterRuleNamesBold;

                Color colour = e.CellStyle.ForeColor;
                FontStyle fontStyle = FontStyle.Regular;

                if (list.Contains(dta.ToDisplayString()))
                {
                    colour = m_ColorIncluded;
                }

                if (boldList.Contains(dta.ToDisplayString()))
                {
                    fontStyle = FontStyle.Bold;
                }

                e.CellStyle.Font = new Font(e.CellStyle.Font, fontStyle);
                e.CellStyle.ForeColor = colour;
            }
        }

        public void BuildDomainTypeList()
        {
            m_EmitterWindow.BuildDomainTypeList();
            m_EffectWindow.BuildDomainTypeList();
        }
        public void BuildDrawTypeList()
        {
            m_PtxRuleWindow.BuildDrawTypeList();
        }

        public void BuildBlendSetList()
        {
            m_PtxRuleWindow.BuildBlendSetList();
        }
        public void BuildShaderList()
        {
            m_PtxRuleWindow.BuildShaderList();
        }

        public void EstablishConnection()
        {
            m_ConnectionForm.m_PanelConnectionStatus.BackColor = Color.Gold;
            //Force the led to draw
            m_ConnectionForm.m_PanelConnectionStatus.Invalidate();
            m_ConnectionForm.m_PanelConnectionStatus.Update();
            Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            tcpComClient1.Start();
        }
        public void Disconnect()
        {
            tcpComClient1.Stop();
        }

        private MenuItem m_NewEventItem;

        public void InitTimeLine()
        {
            m_NewEventItem = new MenuItem("New...");

            MenuItem EmitterEvent = new MenuItem("Emitter Event");
            EmitterEvent.Click += new System.EventHandler(TimeLine_Menu_NewEmitterEvent_Click);
            m_NewEventItem.MenuItems.Add(EmitterEvent);

//            MenuItem EffectEvent = new MenuItem("Effect Event");
//            EffectEvent.Click += new System.EventHandler(TimeLine_Menu_NewEffectEvent_Click);
//            m_NewEventItem.MenuItems.Add(EffectEvent);

            BuildTimelineContextMenu();

            m_TimeLine.Menu_Context.Popup += delegate
            {
                BuildTimelineContextMenu();
            };
        }

        public void BuildTimelineContextMenu()
        {
            m_TimeLine.Menu_Context.MenuItems.Clear();
            m_TimeLine.Menu_Context.MenuItems.Add(m_NewEventItem);

            if (m_EffectWindow != null && m_EffectWindow.m_CurrentEvent != null)
            {
                m_TimeLine.Menu_Context.MenuItems.Add(m_TimeLine.Menu_DeleteChannel);
            }

            if (m_EffectWindow != null && m_EffectWindow.m_CurrentEvent != null && m_EffectWindow.m_CurrentEvent is DataMod.dtaEventEmitter)
            {
                DataMod.dtaEventEmitter currEvt = m_EffectWindow.m_CurrentEvent as DataMod.dtaEventEmitter;

                m_TimeLine.Menu_Context.MenuItems.Add(new MenuItem("Load Evolution...", delegate
                {
                    LoadEventEvolution(m_EffectWindow.m_CurrentEffectName, currEvt);
                }));

                if (currEvt.m_EvoList.m_Evolutions.Count > 0)
                {
                    MenuItem saveEvo = new MenuItem("Save Evolution");

                    foreach (DataMod.dtaEvolution evo in currEvt.m_EvoList.m_Evolutions)
                    {
                        saveEvo.MenuItems.Add(
                            String.Format("{0}...", evo.m_Name),
                            delegate
                            {
                                SaveEventEvolution(m_EffectWindow.m_CurrentEffectName, currEvt, evo);
                            }
                            );
                    }

                    m_TimeLine.Menu_Context.MenuItems.Add(saveEvo);
                }

            }
        }

        public void LoadEventEvolution(string effect, DataMod.dtaEventEmitter evt)
        {
            OpenFileDialog diag = new OpenFileDialog();
            diag.InitialDirectory = m_LastEvoDir;
            diag.Filter = "Event Evolutions (*.evo)|*.evo";
            diag.CheckFileExists = true;
            if (diag.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            String path = System.IO.Path.GetDirectoryName(diag.FileName);
            m_LastEvoDir = path;

            DataCon.m_SDataCon.RequestLoadEvolution(effect, evt.m_Index, diag.FileName);
        }

        public void SaveEventEvolution(string effect, DataMod.dtaEventEmitter evt, DataMod.dtaEvolution evo)
        {
            SaveFileDialog diag = new SaveFileDialog();
            diag.InitialDirectory = m_LastEvoDir;
            diag.Filter = "Event Evolutions (*.evo)|*.evo";
            if (diag.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            String path = System.IO.Path.GetDirectoryName(diag.FileName);
            m_LastEvoDir = path;

            DataCon.m_SDataCon.RequestSaveEvolution(effect, evt.m_Index, evo.m_Name, diag.FileName);
        }

        /// <summary>
        /// Fetch the dtaObject at the specified row.
        /// </summary>
        /// <param name="dgv">Data grid view.</param>
        /// <param name="rowId">Row index.</param>
        /// <returns>The dtaObject at the specified row. If the row doesn't exist (outside bounds) 
        /// or the object isn't a dtaObject this method return null.</returns>
        public DataMod.dtaObject GetDtaObject(DataGridView dgv, int rowId)
        {
            var boundView = dgv.Rows.Count > 0 ? dgv.Rows[rowId].DataBoundItem as DataRowView : null;

            if (boundView != null && rowId >= 0 && rowId < dgv.Rows.Count)
            {
                var obj = boundView.Row[0] as DataMod.dtaObject;
                return obj;
            }

            return null;
        }

        public String GetSelectedRuleNameFromDataGrid(DataGridView dgv)
        {
            if (dgv.SelectedCells.Count <= 0) return "";

            var boundView = dgv.SelectedRows.Count > 0 ? dgv.SelectedRows[0].DataBoundItem as DataRowView : null;
            if (boundView != null)
            {
                var obj = boundView.Row[0] as DataMod.dtaObject;
                return obj.ToDisplayString();
            }

            return "";
        }

        public DataMod.dtaPtxKeyframeProp GetSelectedKeyframePropFromDataGrid(DataGridView grid)
        {
            if (grid.CurrentRow == null) return null;
            DataMod.dtaPtxKeyframeProp keyProp = grid.CurrentRow.Tag as DataMod.dtaPtxKeyframeProp;
            return keyProp;
        }

        public void SelectRuleNameFromDataGrid(DataGridView grid, String rulename)
        {
            if (rulename == "")
            {
                //grid.CurrentCell = null;
                return;
            }

            foreach (DataGridViewRow row in grid.Rows)
            {
                DataMod.dtaObject dto = row.Cells[0].Tag as DataMod.dtaObject;
                if (row.Cells[0].EditedFormattedValue.ToString() == rulename)
                {
                    grid.CurrentCell = row.Cells[0];
                    return;
                }
            }
            grid.CurrentCell = null;
        }

        public void DisplayEvolutionForBehaviors(DataGridView grid,DataMod.dtaEvolutionList evoList)
        {
            DataMod.guiDataGridTagInfo ginfo = new DataMod.guiDataGridTagInfo();
            ginfo.m_EvoList = evoList;
            grid.Tag = ginfo;

            if (evoList== null) return;
            if (evoList.m_Props.Count == 0) return;
            for (int e = 0; e < evoList.m_Evolutions.Count; e++)
            {
                for (int r = 0; r < grid.Rows.Count; r++)
                {
                    DataMod.dtaPtxBehavior behavior = grid.Rows[r].Cells[0].Tag as DataMod.dtaPtxBehavior;
                    if (behavior != null)
                    {
                        if (behavior.ContainsEvolution(e,evoList))
                        {
                            DataMod.dtaEvolution evo = evoList.m_Evolutions[e];
                            String name = evo.m_Name;
                            if (evo.m_Name.Length > 2)
                                name = evo.m_Name[0].ToString() + evo.m_Name[1].ToString();

                            grid.Rows[r].Cells[e + 1].Value = name;
                            grid.Rows[r].Cells[e + 1].Tag = behavior;
                            grid.Rows[r].Cells[0].Style.Font = new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Bold);
                            grid.Rows[r].Cells[e + 1].Style.Font = new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Bold);
                            grid.Columns[e + 1].Visible = true;
                        }
                    }
                }
            }
        }

        public void ClearEvolutionData(DataGridView grid)
        {
            for (int r = 0; r < grid.Rows.Count; r++)
            {
                grid.Rows[r].Cells[0].Style.Font = new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Regular);
                for (int c = 1; c < 8; c++)
                {
                    grid.Rows[r].Cells[c].Value = null;
                    grid.Rows[r].Cells[c].Tag = null;
                    grid.Columns[c].Visible = false;
                }
            }
        }
        public void DisplayEvolutionData(DataGridView grid,DataMod.dtaEvolutionList evoList)
        {
            DataMod.guiDataGridTagInfo ginfo = new DataMod.guiDataGridTagInfo();
            ginfo.m_EvoList = evoList;
            grid.Tag = ginfo;

            if( (evoList == null) || (evoList.m_Props.Count==0))
            {
                ClearEvolutionData(grid);
                return;
            }

            //Find the tag within the keyframe list
            for (int r = 0; r < grid.Rows.Count; r++)
            {
                DataMod.dtaPtxKeyframeProp keyProp = grid.Rows[r].Tag as DataMod.dtaPtxKeyframeProp;
                DataMod.ptxEvoPropBase propBase = evoList.GetPropBase(keyProp);

                if ( (keyProp != null) && (propBase != null))
                {
                    foreach (DataMod.ptxEvoPropChain evoProp in propBase.m_PropChain)
                    {
                        String eName = evoList.m_Evolutions[evoProp.m_EvoIndex].m_Name;
                        String name = eName;
                        if (eName.Length > 2)
                            name = eName[0].ToString() + eName[1].ToString();
                        int cellIndex = evoProp.m_EvoIndex + 1;
                        grid.Rows[r].Cells[0].Style.Font = new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Bold);
                        grid.Rows[r].Cells[cellIndex].Value = name;
                        grid.Rows[r].Cells[cellIndex].Tag = evoProp.m_Keyframe;
                        grid.Rows[r].Cells[cellIndex].Style.Font = new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Bold);
                        grid.Columns[cellIndex].Visible = true;
                        
                        //Show the blend mode
                        DataGridViewComboBoxCell cbcell = grid.Rows[r].Cells[7] as DataGridViewComboBoxCell;
                        cbcell.Value = cbcell.Items[(int)propBase.m_Blendmode];
                        grid.Columns[7].Visible = true;
                    }
                }
            }
         
        }
        
        public void SetEvoBlendDataFromGui(DataGridView grid,DataMod.dtaEvolutionList evoList)
        {
            for(int r=0;r<grid.Rows.Count;r++)
            {
                DataMod.dtaPtxKeyframeProp prop = grid.Rows[r].Tag as DataMod.dtaPtxKeyframeProp;
                if(prop != null)
                {
                    DataMod.ptxEvoPropBase propBase = evoList.GetPropBase(prop);
                    if(propBase != null)
                    {
                        DataGridViewComboBoxCell cbcell = grid.Rows[r].Cells[7] as DataGridViewComboBoxCell;
                        if(cbcell.Value != null)
                            propBase.m_Blendmode = (uint) cbcell.Items.IndexOf(cbcell.Value);
                    }
                }
            }
        }

        public void GenerateKeyPropMenu(String name, ContextMenuStrip menu, DataMod.dtaPtxKeyframeProp keyProp)
        {
            menu.Items.Clear();
            //Selected item
            ToolStripMenuItem selected = new ToolStripMenuItem(name + ": " + keyProp.m_KeyframeSpec.DefnName);
            selected.Enabled = false;
            //Edit
            ToolStripMenuItem editprop = new ToolStripMenuItem("Edit Keyframes");
            menu.Items.Add(selected);
            menu.Items.Add(editprop);
        }
        public void GenerateKeyPropEvoMenu(String name,ContextMenuStrip menu,DataMod.dtaEvolutionList evoList,DataMod.dtaPtxKeyframeProp keyProp,EventHandler AddEvo,EventHandler DelEvo)
        {
            menu.Items.Clear();
            
            //Selected item
			ToolStripMenuItem selected = new ToolStripMenuItem(name + ": " + keyProp.m_KeyframeSpec.DefnName);
            selected.Enabled = false;
            //Edit
            ToolStripMenuItem editprop = new ToolStripMenuItem("Edit Keyframes");
            //Evolutions
            ToolStripMenuItem mevo = new ToolStripMenuItem("Evolutions:");
            mevo.Enabled = false;
            mevo.Visible = false;
            ToolStripMenuItem evoadd = new ToolStripMenuItem("Add");
            evoadd.Visible = false;
            ToolStripMenuItem evoremoveall = new ToolStripMenuItem("Remove All");
            evoremoveall.Visible = false;
            
            menu.Items.Add(selected);
            menu.Items.Add(editprop);
            menu.Items.Add(new ToolStripSeparator());
            menu.Items.Add(mevo);
            menu.Items.Add(evoadd);
            menu.Items.Add(evoremoveall);

            int cnt = 1;
            int removecnt = 0;
            int addcnt = 0;

            //Now we need to get a DataMod.dtaEvolutionPropBase from the keyprop
            DataMod.ptxEvoPropBase propBase = evoList.GetPropBase(keyProp);

            for (int i = 0; i < evoList.m_Evolutions.Count;i++)
            {
                //Which can be removed
                if((propBase != null) && (propBase.HasEvolution(i)))
                {
                    ToolStripMenuItem delitem = new ToolStripMenuItem("Remove (" + cnt.ToString() + ") \"" + evoList.m_Evolutions[i].m_Name + "\"");
                    delitem.Tag = evoList.m_Evolutions[i].m_Name;
                    delitem.Click += DelEvo;
                    menu.Items.Add(delitem);
                    removecnt++;
                }
                else //Which can be added
                {
                    ToolStripMenuItem additem = new ToolStripMenuItem("(" + cnt.ToString() + ") \"" + evoList.m_Evolutions[i].m_Name + "\"");
                    additem.Click += AddEvo;
                    additem.Tag = evoList.m_Evolutions[i].m_Name;
                    evoadd.DropDownItems.Add(additem);
                    addcnt++;
                }
                cnt++;
            }

            menu.Tag = keyProp;

            if (removecnt > 0)
            {
//                evoremoveall.Visible = true;
                mevo.Visible = true;
            }
            if (addcnt > 0)
            {
                evoadd.Visible = true;
                mevo.Visible = true;
            }
        }

        private void tcpComClient1_OnConnect(tcpComClient obj)
        {
            m_Connected = true;
            m_ConnectionForm.m_PanelConnectionStatus.BackColor = Color.Green;
            Cursor.Current = System.Windows.Forms.Cursors.Default;
            ClearInterface();
            m_DataControl.RequestLists();
            if(m_ProfileForm.Visible)
                m_ProfileForm.StartProfiling();
        }

        private void tcpComClient1_OnDisconnect(tcpComClient obj)
        {
            m_Connected = false;
            m_ConnectionForm.m_PanelConnectionStatus.BackColor = Color.Red;
            Cursor.Current = System.Windows.Forms.Cursors.Default;
            m_ProfileForm.StopProfiling();
        }

        private void main_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;
        }

        private void m_WindowMenu_CheckedClicked(object sender, EventArgs e)
        {
            ToolStripMenuItem itm = sender as ToolStripMenuItem;
            Form frm = itm.Tag as Form;
            frm.Visible = !itm.Checked;
            
            if (frm is ptxForm)
                (frm as ptxForm).ShowOnLoad = frm.Visible;

            if (m_Connected && m_ProfileForm.Visible)
                m_ProfileForm.StartProfiling();
        }

        private void m_WindowMenu_DropDownOpening(object sender, EventArgs e)
        {
            m_WindowMenu.DropDownItems.Clear();

            //Scan all the ptxforms and add a view checkbox
            foreach (Form frm in OwnedForms)
            {
                ToolStripMenuItem itm = new ToolStripMenuItem();
                if (frm is ptxForm)
                {
                    itm.Text = (frm as ptxForm).m_Name;
                }
                else
                {
                    itm.Text = frm.Text;
                }
                itm.Checked = frm.Visible;
                itm.Tag = frm;
                m_WindowMenu.DropDownItems.Add(itm);
                itm.Click += new System.EventHandler(this.m_WindowMenu_CheckedClicked);
            }
        }

        private void loadEffectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog diag = new OpenFileDialog();
            diag.InitialDirectory = m_LastEffectRuleDir;
            diag.Filter = "EffectRules (*.effectrule)|*.effectrule";
            diag.Multiselect = true;
            diag.CheckFileExists = true;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                String path = System.IO.Path.GetDirectoryName(diag.FileNames[0]);
                m_LastEffectRuleDir = path;

                if (path.EndsWith("\\effectrules"))
                    path = path.Replace("\\effectrules", "");
                
                //TODO: -=Too much network traffic 
                List<String> names = new List<String>();
                for (int i = 0; i < diag.FileNames.Length; i++)
                {
                    String name = System.IO.Path.GetFileNameWithoutExtension(diag.FileNames[i]);
                    names.Add(name);
                }
                DataCon.m_SDataCon.RequestBatchLoadEffectRule(path, names);
            }
        }

        private void LoadEmitRules_Click(object sender, EventArgs e)
        {
            OpenFileDialog diag = new OpenFileDialog();
            diag.InitialDirectory = m_LastEmitRuleDir;
            diag.Filter = "EmitRules (*.emitrule)|*.emitrule";
            diag.Multiselect = true;
            diag.CheckFileExists = true;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                String path = System.IO.Path.GetDirectoryName(diag.FileNames[0]);
                m_LastEmitRuleDir = path;

                if (path.EndsWith("\\emitrules"))
                    path = path.Replace("\\emitrules", "");

                for (int i = 0; i < diag.FileNames.Length; i++)
                {
                    String name = System.IO.Path.GetFileNameWithoutExtension(diag.FileNames[i]);
                    DataCon.m_SDataCon.RequestLoadEmitRule(path, name);
                }
            }
        }

        private void LoadPtxRules_Click(object sender, EventArgs e)
        {
            OpenFileDialog diag = new OpenFileDialog();
            diag.InitialDirectory = m_LastPtxRuleDir;
            diag.Filter = "PTX Rules (*.ptxrule)|*.ptxrule";
            diag.Multiselect = true;
            diag.CheckFileExists = true;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                String path = System.IO.Path.GetDirectoryName(diag.FileNames[0]);
                m_LastPtxRuleDir = path;

                if (path.EndsWith("\\ptxrules"))
                    path = path.Replace("\\ptxrules", "");

                for (int i = 0; i < diag.FileNames.Length; i++)
                {
                    String name = System.IO.Path.GetFileNameWithoutExtension(diag.FileNames[i]);
                    DataCon.m_SDataCon.RequestLoadPtxRule(path, name);
                }
            }
        }

        private void m_TimeLine_OnSelectChannel(ragPtxTimeLine.ptxTimeLine.ptxChannel channel)
        {
            if (channel == null)
            {
                m_EffectWindow.DisplayTimelineEvent(null, false, false);
                DataMod.dtaEffectRuleStd bogus = null;
                DisplayHeirarchy(bogus);
                return;
            }
            if (channel.m_Data != null)
            {
                DataMod.dtaEvent evt = channel.m_Data as DataMod.dtaEvent;
                m_EffectWindow.DisplayTimelineEvent(channel.m_Data as DataMod.dtaEvent,true,true);
                DisplayHeirarchy(DataMod.GetEffectRule(m_EffectWindow.m_CurrentEffectName) as DataMod.dtaEffectRuleStd);
            }
        }

        public void m_TimeLine_OnChannelInfoChanged(ragPtxTimeLine.ptxTimeLine.ptxChannel channel)
        {
            if (channel.m_Data == null)
                return;
            DataMod.dtaEvent evt = channel.m_Data as DataMod.dtaEvent;
//            evt.m_TriggerTime = channel.m_StartTime;
            
            //Ensure the correct event is displayed 
            m_EffectWindow.DisplayTimelineEvent(evt,false,false);
            //Update the data model
            m_EffectWindow.SetEventDataFromGui();
            //Send the changes
            m_EffectWindow.ChangedEffect(m_EffectWindow.m_CurrentEffectName);
            
        }
        
        private void TimeLine_Menu_NewEmitter_Click(object sender, EventArgs e)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        private void m_TimeLine_OnDeleteChannel(ragPtxTimeLine.ptxTimeLine.ptxChannel channel)
        {
            if (channel == null)
                return;
            DataCon.m_SDataCon.SendDeleteTimeLineEvent(m_EffectWindow.m_CurrentEffectName, channel.m_Index);
        }

        private void m_TimeLine_OnReOrderChannels(object sender, EventArgs e)
        {
            DataMod.dtaEffectRuleStd doe = DataMod.GetEffectRule(m_EffectWindow.m_CurrentEffectName) as DataMod.dtaEffectRuleStd;
            if(doe == null) return;

            foreach (DataMod.dtaEvent evt in doe.m_TimeLine.m_Events)
            {
                ragPtxTimeLine.ptxTimeLine.ptxChannel c = evt.m_Data as ragPtxTimeLine.ptxTimeLine.ptxChannel;
                if(c == null) return;
                evt.m_Index = c.m_Index;
            }
            m_EffectWindow.ChangedEffect(m_EffectWindow.m_CurrentEffectName);
            DataCon.m_SDataCon.SendReorderTimeLineEvents(m_EffectWindow.m_CurrentEffectName);
        }

        private void removeEffect0ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string effectName = GetSelectedRuleNameFromDataGrid(m_DataGridEffects);
            DataCon.RequestResponse onReplyCallback = delegate(tcpByteBuffer resp) {
                bool success = resp.Read_bool();
                KeyframeEditorFormBase.FindAndCloseUnusedForms();
                if (success)
                {
                    ClearInterface();
                    DataCon.m_SDataCon.RequestLists();
                }
                else
                {
                    MessageBox.Show("Couldn't remove effect rule " + effectName + " \n You need to set the param rmptfxRemove in the commandline of the game", 
                        "Error Removing Effect",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
            };

            KeyframeEditorFormBase.DisableAllForms();
            DataCon.m_SDataCon.RequestRemoveEffect(effectName, onReplyCallback);
        }

        private void m_EffectContextMenu_Opening(object sender, CancelEventArgs e)
        {
            if (m_DataGridEffects.SelectedRows.Count > 0)
            {
                string selectedRule = GetSelectedRuleNameFromDataGrid(m_DataGridEffects);
				removeEffect0ToolStripMenuItem.Visible = false;// true;
                removeEffect0ToolStripMenuItem.Text = String.Format("Remove Effect {0}...", selectedRule );

                saveEffectAndChildrenToolStripMenuItem.Visible = true;
                saveEffectAndChildrenToolStripMenuItem.Text = String.Format("Save Effect {0} and All Dependencies", selectedRule);

                saveEffectToolStripMenuItem.Visible = true;
                saveEffectToolStripMenuItem.Text = String.Format("Save Effect {0} only", selectedRule);
            }
            else
          {
              removeEffect0ToolStripMenuItem.Visible = false;
              saveEffectToolStripMenuItem.Visible = false;
              saveEffectAndChildrenToolStripMenuItem.Visible = false;
          }
      }

      private void m_EmitterContextMenu_Opening(object sender, CancelEventArgs e)
      {
            if (m_DataGridEmitters.SelectedRows.Count > 0)
            {
                string selectedRule = GetSelectedRuleNameFromDataGrid(m_DataGridEmitters);
				removeEmitter0ToolStripMenuItem.Visible = false;//true;
                removeEmitter0ToolStripMenuItem.Text = String.Format("Remove Emitter {0}...", selectedRule);

               saveEmitter0ToolStripMenuItem.Visible = true;
                saveEmitter0ToolStripMenuItem.Text = String.Format("Save Emitter {0}", selectedRule);
            }
            else
          {
              removeEmitter0ToolStripMenuItem.Visible = false;
              saveEmitter0ToolStripMenuItem.Visible = false;
          }
      }

      private void m_PtxContextMenu_Opening(object sender, CancelEventArgs e)
      {
            if (m_DataGridPtxList.SelectedRows.Count > 0)
            {
                string selectedRule = GetSelectedRuleNameFromDataGrid(m_DataGridPtxList);
				removePTXRule0ToolStripMenuItem.Visible = false;//true;
                removePTXRule0ToolStripMenuItem.Text = String.Format("Remove PTX Rule {0}...", selectedRule);

                savePTXRule0ToolStripMenuItem.Visible = true;
                savePTXRule0ToolStripMenuItem.Text = String.Format("Save PTX Rule {0}", selectedRule);

            }
            else
            {
                removePTXRule0ToolStripMenuItem.Visible = false;
                savePTXRule0ToolStripMenuItem.Visible = false;
            }
        }

        private void removeEmitter0ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string emitterName = GetSelectedRuleNameFromDataGrid(m_DataGridEmitters);
            DataCon.RequestResponse onReplyCallback = delegate(tcpByteBuffer resp)
            {
                bool success = resp.Read_bool();
                KeyframeEditorFormBase.FindAndCloseUnusedForms();
                if (success)
                {
                    ClearInterface();
                    DataCon.m_SDataCon.RequestLists();
                }
                else
                {
                    MessageBox.Show("Couldn't remove emit rule " + emitterName + ". It may be in use by another effect \nor you need to set the param rmptfxRemove in the commandline of the game",
                        "Error Removing Emit Rule",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
            };

            KeyframeEditorFormBase.DisableAllForms();
            DataCon.m_SDataCon.RequestRemoveEmitter(emitterName, onReplyCallback);
        }

        private void removePTXRule0ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string ptxRuleName = GetSelectedRuleNameFromDataGrid(m_DataGridPtxList);
            DataCon.RequestResponse onReplyCallback = delegate(tcpByteBuffer resp)
            {
                bool success = resp.Read_bool();
                KeyframeEditorFormBase.FindAndCloseUnusedForms();
                if (success)
                {
                    ClearInterface();
                    DataCon.m_SDataCon.RequestLists();
                }
                else
                {
                    MessageBox.Show("Couldn't remove ptx rule " + ptxRuleName + ". It may be in use by another effect\n or you need to set the param rmptfxRemove in the commandline of the game",
                        "Error Removing Ptx Rule",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
            };

            KeyframeEditorFormBase.DisableAllForms();
            DataCon.m_SDataCon.RequestRemovePtxRule(ptxRuleName, onReplyCallback);
        }

        private void exitMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void m_DataGridEffects_SelectionChanged(object sender, EventArgs e)
        {
            if (m_DataGridEffects.Tag != null && m_DataGridEffects.Tag.ToString() == "true")
            {
                return;
            }

            String name = GetSelectedRuleNameFromDataGrid(m_DataGridEffects);
            RequestEffect(name);
        }

        private void SelectOnRightMouseButton_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                DataGridView grid = sender as DataGridView;
                grid.CurrentCell = grid.Rows[e.RowIndex].Cells[e.ColumnIndex];
            }
        }

        private void m_DataGridEmitters_SelectionChanged(object sender, EventArgs e)
        {
            String name = GetSelectedRuleNameFromDataGrid(m_DataGridEmitters);
            m_effectRuleName = name;
            RequestEmitter(name);
        }

        private void m_DataGridPtxList_SelectionChanged(object sender, EventArgs e)
        {
            String name = GetSelectedRuleNameFromDataGrid(m_DataGridPtxList);
            m_ptxRulName = name;
            RequestPtxRule(name);
        }

        private void m_DataGridEffects_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid == null) return;

            DataMod.dtaObject doo = GetDtaObject(grid, e.RowIndex);
            if (doo == null) return;

            String oldname = m_currentEffectObject == null ? "" : m_currentEffectObject.m_Name;
            String newname = "";

            if (grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                newname = grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

            if (newname.Length == 0)
            {
                grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = oldname;
                SelectEffect(""); //Need to do this to maintain selection (thanks to funky datagrid)
                RequestEffect(oldname);
                return;
            }
            //Reject duplicates
            DataMod.dtaObject dot = DataMod.m_SDataMod.GetObject(DataMod.m_SDataMod.m_EffectRuleList, newname);
            if (dot != null)
            {
                grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = oldname;
                if (oldname != newname)
                {
                    m_DataGridEffects.BeginInvoke(new UpdateVisualDelegate(UpdateEffectsVisual), oldname, newname);
                }
                return;
            }

            doo.Rename(newname);
            SelectEffect(""); //Need to do this to maintain selection (thanks to funky datagrid)
            DataCon.m_SDataCon.SendRenameEffectRule(oldname, newname);
            BuildEffectList();
            m_EffectWindow.m_CurrentEffectName = newname;
            RequestEffect(newname);
        }

        /// <summary>
        /// Update visuals delegate. Fixes a problem with the data grid throwing an exception. 
        /// See http://social.msdn.microsoft.com/forums/en-US/winformsdatacontrols/thread/f824fbbf-9d08-4191-98d6-14903801acfc/
        /// for details.
        /// </summary>
        /// <param name="oldName"></param>
        /// <param name="newName"></param>
        private delegate void UpdateVisualDelegate(string oldName, string newName);

        /// <summary>
        /// Update the effects visual data grid.
        /// </summary>
        /// <param name="oldName"></param>
        /// <param name="newName"></param>
        private void UpdateEffectsVisual(string oldName, string newName)
        {
            SelectEffect(""); //Need to do this to maintain selection (thanks to funky datagrid)
            DataCon.m_SDataCon.SendRenameEffectRule(oldName, newName);
            BuildEffectList();
            RequestEffect(newName);
        }

        /// <summary>
        /// Update the emitters visual data grid.
        /// </summary>
        /// <param name="oldName"></param>
        /// <param name="newName"></param>
        private void UpdateEmittersVisual(string oldName, string newName)
        {
            SelectEmitter(""); //Need to do this to maintain selection (thanks to funky datagrid)
            DataCon.m_SDataCon.SendRenameEmitterRule(oldName, newName);
            BuildEmitterList();
            RequestEmitter(newName);
        }

        /// <summary>
        /// Update the ptx visual data grid.
        /// </summary>
        /// <param name="oldName"></param>
        /// <param name="newName"></param>
        private void UpdatePtxVisual(string oldName, string newName)
        {
            SelectPtxRule(""); //Need to do this to maintain selection (thanks to funky datagrid)
            DataCon.m_SDataCon.SendRenamePtxRule(oldName, newName);
            BuildPtxList();
            RequestPtxRule(newName);
        }

        private void m_DataGridEmitters_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid == null) return;

            DataMod.dtaObject doo = GetDtaObject(grid, e.RowIndex);
            if (doo == null) return;

            String oldname = m_currentEffectObject == null ? "" : m_currentEffectObject.m_Name;
            String newname = "";

            if (grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                newname = grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

            if (newname.Length == 0)
            {
                grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = oldname;
                SelectEmitter(""); //Need to do this to maintain selection (thanks to funky datagrid)
                RequestEmitter(oldname);
                return;
            }
            //Reject duplicates
            DataMod.dtaObject dot = DataMod.m_SDataMod.GetObject(DataMod.m_SDataMod.m_EmitRuleList, newname);
            if (dot != null)
            {
                grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = oldname;
                if (oldname != newname)
                {
                    m_DataGridEmitters.BeginInvoke(new UpdateVisualDelegate(UpdateEmittersVisual), oldname, newname);
                }
                return;
            }

            doo.Rename(newname);
            SelectEmitter(""); //Need to do this to maintain selection (thanks to funky datagrid)
            DataCon.m_SDataCon.SendRenameEmitterRule(oldname, newname);
            BuildEmitterList();
            RequestEmitter(newname);

            ////Update the effect if we are currently looking at it
            DataMod.dtaEffectRule dfx = DataMod.GetEffectRule(m_EffectWindow.m_CurrentEffectName) as DataMod.dtaEffectRuleStd;
            if (dfx != null)
            {
                if (dfx.RenameEmitterRule(oldname, newname))
                    DisplayEffect(dfx.m_Name, false, false);

                if (dfx is DataMod.dtaEffectRuleStd)
                    DisplayHeirarchy(dfx as DataMod.dtaEffectRuleStd);
            }
        }

        private void m_DataGridPtxList_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid == null) return;
            DataMod.dtaObject doo = GetDtaObject(grid, e.RowIndex);
            if (doo == null) return;

            String oldname = m_currentEffectObject == null ? "" : m_currentEffectObject.m_Name;
            String newname = "";

            if (grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                newname = grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

            if (newname.Length == 0)
            {
                grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = oldname;
                SelectPtxRule(""); //Need to do this to maintain selection (thanks to funky datagrid)
                RequestPtxRule(oldname);
                return;
            }
            //Reject duplicates
            DataMod.dtaObject dot = DataMod.m_SDataMod.GetObject(DataMod.m_SDataMod.m_PtxRuleList, newname);
            if (dot != null)
            {
                grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = oldname;
                if (oldname != newname)
                {
                    m_DataGridPtxList.BeginInvoke(new UpdateVisualDelegate(UpdatePtxVisual), oldname, newname);
                }
                return;
            }

            doo.Rename(newname);
            SelectPtxRule(""); //Need to do this to maintain selection (thanks to funky datagrid)
            DataCon.m_SDataCon.SendRenamePtxRule(oldname, newname);
            BuildPtxList();
            RequestPtxRule(newname);

            ////Update the effect if we are currently looking at it
            DataMod.dtaEffectRule dfx = DataMod.GetEffectRule(m_EffectWindow.m_CurrentEffectName) as DataMod.dtaEffectRuleStd;
            if (dfx != null)
            {
                if (dfx.RenamePtxRule(oldname, newname))
                    DisplayEffect(dfx.m_Name, false, false);

                if (dfx is DataMod.dtaEffectRuleStd)
                    DisplayHeirarchy(dfx as DataMod.dtaEffectRuleStd);
            }
        }

        private void DataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            ////For some reason enter acts as arrow down ?
            //if (e.KeyCode == Keys.Enter)
            //    e.Handled = true;
            if (e.KeyCode == Keys.Space)
                e.Handled = true;
        }

        private void openEffectListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog diag = new OpenFileDialog();
            diag.InitialDirectory = m_LastEffectListDir;
            diag.Filter = "Effect List (*.fxlist)|*.fxlist";
            diag.CheckFileExists = true;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                String path = System.IO.Path.GetDirectoryName(diag.FileName);
                m_LastEffectListDir = path;

                if (path.EndsWith("\\fxlists"))
                    path = path.Replace("\\fxlists", "");

                string name = System.IO.Path.GetFileNameWithoutExtension(diag.FileName);
                DataCon.m_SDataCon.RequestLoadEffectList(path, name);
            }
        }

        private void saveEffectListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog diag = new SaveFileDialog();
            diag.InitialDirectory = m_LastEffectListDir;
            diag.Filter = "Effect List (*.fxlist)|*.fxlist";
            diag.CheckFileExists = false;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                String path = System.IO.Path.GetDirectoryName(diag.FileName);
                m_LastEffectListDir = path;

                if (path.EndsWith("\\fxlists"))
                    path = path.Replace("\\fxlists", "");

                string name = System.IO.Path.GetFileNameWithoutExtension(diag.FileName);
                DataCon.m_SDataCon.RequestSaveEffectList(path, name);
            }
        }

        private void saveRulesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.RequestSaveAll();
        }

        private void spriteRuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.SendCreateNewPtxRuleEvent(0,true);
        }

        private void modelRuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.SendCreateNewPtxRuleEvent(1,true);
        }

        private void behaviorRuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.SendCreateNewPtxRuleEvent(2, true);
        }

        private void newEmitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.SendCreateNewEmitRuleEvent(true);
        }

        private void newEffectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.SendCreateNewEffectRuleEvent(true);
        }

        private void TimeLine_Menu_NewEmitterEvent_Click(object sender, EventArgs e)
        {
            if (m_EffectWindow.m_CurrentEffectName.Length > 0)
                DataCon.m_SDataCon.SendCreateNewTimelineEvent(m_EffectWindow.m_CurrentEffectName, 0, true);
        }
        private void TimeLine_Menu_NewEffectEvent_Click(object sender, EventArgs e)
        {
            if (m_EffectWindow.m_CurrentEffectName.Length > 0)
                DataCon.m_SDataCon.SendCreateNewTimelineEvent(m_EffectWindow.m_CurrentEffectName, 1, true);
        }

        private void SaveEmitter_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.RequestSave(1, m_EmitterWindow.m_CurrentEmitterName, false);
        }

        private void SaveEffectAndChidlren_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.RequestSave(0, m_EffectWindow.m_CurrentEffectName, true);
        }

        private void SaveEffect_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.RequestSave(0, m_EffectWindow.m_CurrentEffectName, false);
        }

        private void SavePtxRule_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.RequestSave(2, m_PtxRuleWindow.m_CurrentPtxName, false);
        }

        private void m_TimeLine_OnSoloMuteChanged(object sender, EventArgs e)
        {
            bool isSoloing = false;
            foreach (ragPtxTimeLine.ptxTimeLine.ptxChannel channel in m_TimeLine.m_Channels)
            {
                if (channel.m_Solo)
                {
                    isSoloing = true;
                    break;
                }
            }

            // if soloing only solos should be active, unless those solos are also muted
            foreach(ragPtxTimeLine.ptxTimeLine.ptxChannel channel in m_TimeLine.m_Channels)
            {
                DataMod.dtaEvent evt = channel.m_Data as DataMod.dtaEvent;
                evt.m_ActiveToggle = !channel.m_Mute && (!isSoloing || channel.m_Solo); 
            }

            m_EffectWindow.ChangedEffect(m_EffectWindow.m_CurrentEffectName);
        }

        public void PropertyDataGrid_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid == null) return;
            if (grid.CurrentCell == null) return;
            DataGridViewCell cell = grid.CurrentCell;
            if (cell.ColumnIndex == 6)
            {
                grid.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }

        }

        public void PropertyDataGrid_CellValueChanged(object sender, DataMod.guiPropEventData guiEvt, DataGridViewCellEventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid == null) return;
            if (grid.CurrentCell == null) return;
            DataGridViewCell cell = grid.CurrentCell;
            DataMod.guiDataGridTagInfo info = grid.Tag as DataMod.guiDataGridTagInfo;
            if (info == null) return;
            if (cell.ColumnIndex == 6)
            {
                main.m_MainForm.SetEvoBlendDataFromGui(grid, info.m_EvoList);
                guiEvt.InfoChanged = true;
            }

        }

        private void clonePTXRuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.SendClonePtxRuleEvent(m_PtxRuleWindow.m_CurrentPtxName);
        }

        private void saveRulesSAFEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_SavingPopup.Start();
        }

        private Cursor mLastType = System.Windows.Forms.Cursors.No; 

        // this is here to force the Cursor to be visible... 
        private void m_UpdateTimer_Tick(object sender, EventArgs e)
        {
            Cursor.Show();

            if (m_ConnectionForm.RagConnected && !m_remoteConsole.IsConnected())
            {
                m_ConnectionForm.RagDisconnected();
            }
		}

        #region List filtration

        private void m_effectsFilterBox_Leave(object sender, EventArgs e)
        {
            FilterText(m_effectsFilterBox.Text, m_effectsData, m_DataGridEffects);
        }

        private void m_effectsFilterBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                FilterText(m_effectsFilterBox.Text, m_effectsData, m_DataGridEffects);
            }
        }

        private void m_emittersFilterBox_Leave(object sender, EventArgs e)
        {
            FilterText(m_emittersFilterBox.Text, m_emitterListData, m_DataGridEmitters);
        }

        private void m_emittersFilterBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                FilterText(m_emittersFilterBox.Text, m_emitterListData, m_DataGridEmitters);
            }
        }

        private void m_ptxFilterBox_Leave(object sender, EventArgs e)
        {
            FilterText(m_ptxFilterBox.Text, m_ptxData, m_DataGridPtxList);
        }

        private void m_ptxFilterBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                FilterText(m_ptxFilterBox.Text, m_ptxData, m_DataGridPtxList);
            }
        }

        /// <summary>
        /// When a filter text box is given focus, highlight all the text inside it.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void FilterBox_GotFocus(object sender, EventArgs e)
        {
            var tb = sender as TextBox;
            tb.SelectAll();
        }

        /// <summary>
        /// Filter out the entries in the list based upon the filter criteria.
        /// </summary>
        /// <param name="filter">Filter criteria.</param>
        /// <param name="data">Source data.</param>
        /// <param name="dgv">Visual representation of the data.</param>
        private void FilterText(string filter, DataTable data, DataGridView dgv)
        {
            filter = filter.Trim();
            if (String.IsNullOrEmpty(filter))
            {
                data.DefaultView.RowFilter = "";
            }
            else
            {
                data.DefaultView.RowFilter = "Name LIKE '%" + filter + "%'";
            }
        }

        #endregion

        private void m_DataGridEffects_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void DataGrid_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            var dataGrid = sender as DataGridView;

            DataMod.dtaObject currentObject = GetDtaObject(dataGrid, e.RowIndex);
            m_currentEffectObject = currentObject;
            DataMod.dtaObject newObject = new DataMod.dtaObject(e.Value.ToString(), currentObject);
            e.Value = newObject;

            if (dataGrid == m_DataGridEffects)
            {
                m_DataControl.m_DataModel.RemoveObject(m_DataControl.m_DataModel.m_EffectRuleList, currentObject.ToDisplayString());
                m_DataControl.m_DataModel.m_EffectRuleList.Add(newObject);
            }
            else if (dataGrid == m_DataGridEmitters)
            {
                m_DataControl.m_DataModel.RemoveObject(m_DataControl.m_DataModel.m_EmitRuleList, currentObject.ToDisplayString());
                m_DataControl.m_DataModel.m_EmitRuleList.Add(newObject);
            }
            else if (dataGrid == m_DataGridPtxList)
            {
                m_DataControl.m_DataModel.RemoveObject(m_DataControl.m_DataModel.m_PtxRuleList, currentObject.ToDisplayString());
                m_DataControl.m_DataModel.m_PtxRuleList.Add(newObject);
            }

            e.ParsingApplied = !String.IsNullOrWhiteSpace(e.Value.ToString());
        }

        private void DataGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = false;
        }

        private void m_DataGridEffects_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            m_DataGridEffects.BeginEdit(true);
        }
    }
}
