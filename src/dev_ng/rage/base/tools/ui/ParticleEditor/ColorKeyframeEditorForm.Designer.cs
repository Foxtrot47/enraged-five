namespace ParticleEditor
{
    partial class ColorKeyframeEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorGradientEditor1 = new ragKeyframeEditorControl.ColorGradientEditor();
            this.SuspendLayout();
            // 
            // colorGradientEditor1
            // 
            this.colorGradientEditor1.AllowEditAlpha = true;
            this.colorGradientEditor1.AllowEditColor = true;
            this.colorGradientEditor1.AllowHSBMode = false;
            this.colorGradientEditor1.LabelRes = 3;
            this.colorGradientEditor1.Location = new System.Drawing.Point(0, 0);
            this.colorGradientEditor1.MaxRange = 1F;
            this.colorGradientEditor1.MinRange = 0F;
            this.colorGradientEditor1.Name = "colorGradientEditor1";
            this.colorGradientEditor1.Size = new System.Drawing.Size(568, 344);
            this.colorGradientEditor1.TabIndex = 0;
            // 
            // ColorKeyframeEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 338);
            this.Controls.Add(this.colorGradientEditor1);
            this.Name = "ColorKeyframeEditorForm";
            this.Text = "ColorKeyframeEditorForm";
            this.ResumeLayout(false);

        }

        #endregion

        private ragKeyframeEditorControl.ColorGradientEditor colorGradientEditor1;
    }
}