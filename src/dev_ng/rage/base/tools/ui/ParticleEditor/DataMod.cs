using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.IO;

namespace ParticleEditor
{
    public struct Float3
    {
        public float x, y, z;
    };
    public struct Float2
    {
        public Float2(float xx, float yy)
        {
            x = xx; y = yy;
        }

        public float x, y;
    };

    //The internal representation of all the data
    public class DataMod
    {
        public static DataMod m_SDataMod = null;

        public DataMod()
        {
            m_SDataMod = this;
        }

        static void FindXmlFloatValue(XmlNode root, string name, ref float data)
        {
            foreach (XmlNode child in root.ChildNodes)
            {
                if (child.Name == name && child is XmlElement)
                {
                    XmlElement elt = child as XmlElement;
                    data = Single.Parse(elt.GetAttribute("value"));
                    return;
                }
            }
            throw new KeyNotFoundException();
        }

        static void FindXmlBoolValue(XmlNode root, string name, ref bool data)
        {
            foreach(XmlNode child in root.ChildNodes)
            {
                if (child.Name == name && child is XmlElement)
                {
                    XmlElement elt = child as XmlElement;
                    data = Boolean.Parse(elt.GetAttribute("value"));
                    return;
                }
            }
            throw new KeyNotFoundException();
        }

        static void FindXmlIntValue(XmlNode root, string name, ref uint data)
        {
            foreach (XmlNode child in root.ChildNodes)
            {
                if (child.Name == name && child is XmlElement)
                {
                    XmlElement elt = child as XmlElement;
					data = StringConversion.ToUint(elt.GetAttribute("value"));
                    return;
                }
            }
            throw new KeyNotFoundException();
        }

        static void FindXmlIntValue(XmlNode root, string name, ref int data)
        {
            foreach (XmlNode child in root.ChildNodes)
            {
                if (child.Name == name && child is XmlElement)
                {
                    XmlElement elt = child as XmlElement;
					data = StringConversion.ToInt(elt.GetAttribute("value"));
                    return;
                }
            }
            throw new KeyNotFoundException();
        }

        static void FindXmlByteValue(XmlNode root, string name, ref byte data)
        {
            foreach (XmlNode child in root.ChildNodes)
            {
                if (child.Name == name && child is XmlElement)
                {
                    XmlElement elt = child as XmlElement;
                    data = Byte.Parse(elt.GetAttribute("value"));
                    return;
                }
            }
            throw new KeyNotFoundException();
        }

        static void FindXmlStringValue(XmlNode root, string name, ref string data)
        {
            foreach (XmlNode child in root.ChildNodes)
            {
                if (child.Name == name && child is XmlElement)
                {
                    XmlElement elt = child as XmlElement;
                    data = elt.InnerText;
                    return;
                }
            }
            throw new KeyNotFoundException();
        }

        static XmlNode FindXmlNode(XmlNode root, string name)
        {
            foreach(XmlNode child in root.ChildNodes)
            {
                if (child.Name == name && child is XmlElement)
                {
                    return child;
                }
            }
            throw new KeyNotFoundException();
        }

        public static void LoadXmlStruct(XmlNode root, string name, IRageXmlSerializable structure)
        {
            XmlNode node = FindXmlNode(root, name);
            if (node != null)
            {
                structure.LoadFromXml(node);
            }
        }

        public static void LoadXmlStruct(tcpByteBuffer buff, IRageXmlSerializable structure)
        {
            string s = buff.Read_const_char();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(s);
            structure.LoadFromXml(doc.DocumentElement);
        }

        static void SaveXmlFloatValue(XmlDocument doc, XmlNode parent, string name, float data)
        {
            XmlElement elt = doc.CreateElement(name);
            elt.SetAttribute("value", data.ToString());
            parent.AppendChild(elt);
        }

        static void SaveXmlIntValue(XmlDocument doc, XmlNode parent, string name, int data)
        {
            XmlElement elt = doc.CreateElement(name);
            elt.SetAttribute("value", data.ToString());
            parent.AppendChild(elt);
        }

        static void SaveXmlByteValue(XmlDocument doc, XmlNode parent, string name, byte data)
        {
            XmlElement elt = doc.CreateElement(name);
            elt.SetAttribute("value", data.ToString());
            parent.AppendChild(elt);
        }

        static void SaveXmlBoolValue(XmlDocument doc, XmlNode parent, string name, bool data)
        {
            XmlElement elt = doc.CreateElement(name);
            elt.SetAttribute("value", data ? "true" : "false");
            parent.AppendChild(elt);
        }

        static void SaveXmlStringValue(XmlDocument doc, XmlNode parent, string name, string data)
        {
            XmlElement elt = doc.CreateElement(name);
            XmlText text = doc.CreateTextNode(data);
            elt.AppendChild(text);
            parent.AppendChild(elt);
        }

        public static void SaveXmlStruct(XmlDocument doc, XmlNode parent, string name, IRageXmlSerializable structure)
        {
            XmlElement elt = doc.CreateElement(name);
            parent.AppendChild(elt);
            structure.SaveToXml(doc, elt);
        }

        public static void SaveXmlStruct(tcpByteBuffer buff, string name, IRageXmlSerializable structure)
        {
            XmlDocument doc = new XmlDocument();
            XmlElement elt = doc.CreateElement(name);
            doc.AppendChild(elt);
            structure.SaveToXml(doc, elt);

            StringBuilder s = new StringBuilder();
            XmlWriter w = XmlWriter.Create(s);
            doc.WriteTo(w);
            w.Close();
       
            buff.Write_const_char(s.ToString());
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public interface IRageXmlSerializable
        {
            void LoadFromXml(XmlNode root);
            void SaveToXml(XmlDocument doc, XmlNode root);
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class KeyframeSpec : IRageXmlSerializable
        {
            public KeyframeSpec()
            {
            }

            public override string ToString()
            {
                return DefnName;
            }

            //public dtaPtxBehavior m_Behavior = null;
            public string DefnName;

			public string RuleName = "invalid";
			public int RuleType = -1;
			public uint PropertyId = 0;
			public int EventIdx = -1;
			public int EvoIdx = -1;


            public void LoadFromXml(XmlNode root)
            {
				FindXmlStringValue(root, "pDefnName", ref DefnName);
				FindXmlStringValue(root, "pRuleName", ref RuleName);
				FindXmlIntValue(root, "ruleType", ref RuleType);
				FindXmlIntValue(root, "propertyId", ref PropertyId);
            }
            public void SaveToXml(XmlDocument doc, XmlNode root)
            {
                throw new NotImplementedException();
            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class KeyframeSet : IRageXmlSerializable
        {
            public List<KeyframeSpec> Keyframes = new List<KeyframeSpec>();

            public void LoadFromXml(XmlNode root)
            {
                XmlNode keys = FindXmlNode(root, "Keyframes");
                foreach(XmlNode node in keys.ChildNodes)
                {
                    if (node is XmlElement && node.Name == "Item")
                    {
                        KeyframeSpec s = new KeyframeSpec();
                        s.LoadFromXml(node);
                        Keyframes.Add(s);
                    }
                }
            }
            public void SaveToXml(XmlDocument doc, XmlNode root)
            {
                throw new NotImplementedException();
            }
        };

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class KeyframeData : IRageXmlSerializable
        {
            public class Key {
                public float Time;
                public float[] Data;

                public override string ToString()
                {
                    StringBuilder s = new StringBuilder();
                    s.Append(Time);
                    s.Append(" ");
                    foreach(float d in Data)
                    {
                        s.Append(d);
                        s.Append(" ");
                    }
                    return s.ToString();
                }
            };
            public List<Key> m_KeyData = new List<Key>();
            public int m_Channels = 4;

            public void LoadFromXml(XmlNode root)
            {
                int keys = 0;

                XmlElement kfData = FindXmlNode(root, "keyData") as XmlElement;

                FindXmlIntValue(kfData, "numKeyEntries", ref keys);

                XmlText datanode = FindXmlNode(kfData, "keyEntryData").ChildNodes[0] as XmlText;
                if (datanode != null)
                {
                    string[] dataToks = datanode.Value.Split(null);
                    List<string> data = new List<string>();
                    foreach (string d in dataToks)
                    {
                        if (d != String.Empty)
                        {
                            data.Add(d);
                        }
                    }

                    int dataIndex = 0;
                    for (int keyIndex = 0; keyIndex < keys; keyIndex++)
                    {
                        Key k = new Key();
                        k.Time = Single.Parse(data[dataIndex++]);
                        k.Data = new float[m_Channels];
                        for (int channelIndex = 0; channelIndex < m_Channels; channelIndex++)
                        {
                            k.Data[channelIndex] = Single.Parse(data[dataIndex++]);
                        }
                        m_KeyData.Add(k);
                    }
                }
            }

            public void SaveToXml(XmlDocument doc, XmlNode root)
            {
                XmlElement kfData = doc.CreateElement("keyData");
                root.AppendChild(kfData);

                SaveXmlIntValue(doc, kfData, "numKeyEntries", m_KeyData.Count);

                StringBuilder s = new StringBuilder();
                foreach(Key key in m_KeyData)
                {
                    s.Append(key.ToString());
                    s.Append('\n');
                }

                string dataString = s.ToString();

                XmlText t = doc.CreateTextNode(dataString);
                XmlElement elt = doc.CreateElement("keyEntryData");
                elt.SetAttribute("content", "float_array");
                elt.AppendChild(t);

                kfData.AppendChild(elt);
            }

            public void PostLoad(KeyframeUiData uidata)
            {
                // if there is no key data we need to set the default value
                if (m_KeyData.Count == 0)
                {
                    Key k = new Key();
                    k.Time = 0.0f;
                    k.Data = new float[m_Channels];
                    k.Data[0] = uidata.DefaultValX;
                    k.Data[1] = uidata.DefaultValY;
                    k.Data[2] = uidata.DefaultValZ;
                    k.Data[3] = uidata.DefaultValW;
                    m_KeyData.Add(k);
                }
            }

            public void PreSave(KeyframeUiData uidata)
            {
                if (m_KeyData.Count == 1)
                {
                    if (m_KeyData[0].Time == 0.0f &&
                        m_KeyData[0].Data[0] == uidata.DefaultValX &&
                        m_KeyData[0].Data[1] == uidata.DefaultValY &&
                        m_KeyData[0].Data[2] == uidata.DefaultValZ &&
                        m_KeyData[0].Data[3] == uidata.DefaultValW)
                    {
                        m_KeyData.Remove(m_KeyData[0]);
                    }
                }
            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class KeyframeUiData : IRageXmlSerializable
        {
            public string[] Labels;
            public System.Drawing.Color[] Colors;
            public float XMin = 0.0f;
            public float XMax = 1.0f;
            public float XStep = 0.01f;
            public float YMin = 0.0f;
            public float YMax = 1.0f;
            public float YStep = 0.01f;
            public float DefaultValX = 0.0f;
            public float DefaultValY = 0.0f;
            public float DefaultValZ = 0.0f;
            public float DefaultValW = 0.0f;
            public bool ShowAsColorGradient = false;
			/*
			public void LoadFromByteBuffer(tcpByteBuffer buff)
			{
                string name = buff.Read_const_char();

				for (int i=0; i<4; i++)
				{
					Labels[i] = buff.Read_const_char();
				}

				for (int i=0; i<4; i++)
				{
					byte r = (byte)buff.Read_u8();
					byte g = (byte)buff.Read_u8();
					byte b = (byte)buff.Read_u8();
					byte a = (byte)buff.Read_u8();

                    Colors[i] = System.Drawing.Color.FromArgb(a, r, g, b);
				}

				YMin = buff.Read_float();
				YMax = buff.Read_float();
				
				XMin = buff.Read_float();
				XMax = buff.Read_float();
				
				int type = buff.Read_s32();
				ShowAsColorGradient = buff.Read_bool();
			}

			public void WriteToByteBuffer(tcpByteBuffer buff)
			{
			}
			*/
            public void LoadFromXml(XmlNode root)
            {
                XmlElement elt = null;
                XmlNodeList kids;

				elt = FindXmlNode(root, "vMinMaxDeltaY") as XmlElement;
                YMin = Single.Parse(elt.GetAttribute("x"));
                YMax = Single.Parse(elt.GetAttribute("y"));

				elt = FindXmlNode(root, "vMinMaxDeltaX") as XmlElement;
                XMin = Single.Parse(elt.GetAttribute("x"));
                XMax = Single.Parse(elt.GetAttribute("y"));

                elt = FindXmlNode(root, "vDefaultValue") as XmlElement;
                DefaultValX = Single.Parse(elt.GetAttribute("x"));
                DefaultValY = Single.Parse(elt.GetAttribute("y"));
                DefaultValZ = Single.Parse(elt.GetAttribute("z"));
                DefaultValW = Single.Parse(elt.GetAttribute("w"));

                Labels = new string[4];
                elt = FindXmlNode(root, "labels") as XmlElement;
                kids = elt.GetElementsByTagName("Item");
                Labels[0] = kids[0].InnerText;
                Labels[1] = kids[1].InnerText;
                Labels[2] = kids[2].InnerText;
                Labels[3] = kids[3].InnerText;

                Colors = new System.Drawing.Color[4];
                elt = FindXmlNode(root, "colours") as XmlElement;
                string[] rawItems = elt.InnerText.Split();
                List<string> items = new List<string>();
                foreach(string s in rawItems)
                {
                    if (s != String.Empty)
                    {
                        items.Add(s);
                    }
                }

                for(int i = 0; i < 4; i++)
                {
                    int color32 = StringConversion.ToInt(items[i]);
                    // color32 is ARGB
                    Colors[i] = System.Drawing.Color.FromArgb(
                        ((color32 >> 24) & 0xFF),
                        ((color32 >> 16) & 0xFF),
                        ((color32 >> 8) & 0xFF),
                        ((color32 ) & 0xFF)
                        );
                }

                FindXmlBoolValue(root, "useColourGradient", ref ShowAsColorGradient);
            }

            public void SaveToXml(XmlDocument doc, XmlNode root)
            {
                throw new NotImplementedException();
            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //This class is the generic container for the other objects
        //Makes is easier to allow objects to change type 
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaObject
        {
            public String m_DisplayName = "";  //Might use this to abstract list box string from the search key
            public String m_Name = "";
            public String m_ClassName = "";
            public int m_Hash = 0;
            //public int m_Type = -1;
            public int m_RefCount = 0;
            public Object m_Object = null;
            
//            public bool m_ActiveFlag = false;
//            public bool m_NeedsSave = false;
            public override string ToString() { return m_Name; }
            public string ToDisplayString()
            {
                String name = m_Name;
//                if (m_NeedsSave)
//                    name += "*";
//                if (m_ActiveFlag)
//                    name += "#";
//                if (m_RefCount > 1)
//                    name += String.Format(" ({0})", m_RefCount);
                return name;
            }

            public dtaObject()
            {

            }

            public dtaObject(string newName, dtaObject objectToClone)
            {
                m_DisplayName = newName;
                m_Name = newName;
                m_ClassName = objectToClone.m_ClassName;
                m_RefCount = 0;
                m_Object = objectToClone.m_Object;
            }
            
            public void Rename(String newname)
            {
                m_Name = newname;
                if(m_Object != null)
                {
                    if (m_Object is dtaEmitRule)
                        (m_Object as dtaEmitRule).m_Name = newname;
                    else
                        if (m_Object is dtaEffectRule)
                            (m_Object as dtaEffectRule).m_Name = newname;
                        else
                            if (m_Object is dtaPtxRule)
                                (m_Object as dtaPtxRule).m_Name = newname;
                }
            }
        };

        public dtaObject GetObject(ArrayList list, String name)
        {
            foreach (dtaObject obj in list)
                if (obj.ToString() == name)
                    return obj;
            return null;
        }

        public void RemoveObject(ArrayList list, String name)
        {
            dtaObject found = null;

            foreach (dtaObject obj in list)
            {
                if (obj.ToString() == name)
                {
                    found = obj;
                    break;
                }
            }

            if (found != null)
            {
                list.Remove(found);
            }
        }

        public int GetIndex(ArrayList list, String name)
        {
            for(int i = 0; i < list.Count; i++)
            {
                if (list[i].ToString() == name)
                {
                    return i;
                }
            }
            return -1;
        }
        public static dtaBehaviorDefinition GetUpdateBehaviorDef(String name)
        {
            if (m_SDataMod == null)
                return null;

            foreach (dtaBehaviorDefinition obj in m_SDataMod.m_UpdateBehaviorDefList)
                if (obj.m_BehaviorName == name)
                    return obj;
            return null;
        }

        public static dtaEffectRule GetEffectRule(String name)
        {
            if (m_SDataMod == null)
                return null;
            
            dtaObject doo = m_SDataMod.GetObject(m_SDataMod.m_EffectRuleList, name);
            if (doo == null)
                return null;
            return doo.m_Object as dtaEffectRule;
        }
        public static dtaStdEmitRule GetEmitRule(String name)
        {
            if (m_SDataMod == null)
                return null;

            dtaObject doo = m_SDataMod.GetObject(m_SDataMod.m_EmitRuleList, name);
            if (doo == null)
                return null;
            return doo.m_Object as dtaStdEmitRule;
        }
        public static dtaPtxRule GetPtxRule(String name)
        {
            if (m_SDataMod == null)
                return null;

            dtaObject doo = m_SDataMod.GetObject(m_SDataMod.m_PtxRuleList, name);
            if (doo == null)
                return null;
            return doo.m_Object as dtaPtxRule;
        }
		public static string GetDomainNameFromType(byte type)
		{
			return m_SDataMod.m_DomainTypes[type].ToString();
		}
        public static byte GetDomainTypeFromName(string name)
        {
            return (byte)m_SDataMod.GetIndex(m_SDataMod.m_DomainTypes, name);
        }
		public static byte GetEmitterDomainTypeFromName(string name)
		{
			return (byte)m_SDataMod.GetIndex(m_SDataMod.m_EmitterDomainTypes, name);
		}
		public static byte GetVelocityDomainTypeFromName(string name)
		{
			return (byte)m_SDataMod.GetIndex(m_SDataMod.m_VelocityDomainTypes, name);
		}
		public static byte GetAttractorDomainTypeFromName(string name)
		{
			return (byte)m_SDataMod.GetIndex(m_SDataMod.m_AttractorDomainTypes, name);
		}
        public static dtaBehaviorDefinition GetPtxRuleBehaviorDefinitionFromName(String name)
        {
            for(int i=0;i<m_SDataMod.m_UpdateBehaviorDefList.Count;i++)
            {
                dtaBehaviorDefinition def = m_SDataMod.m_UpdateBehaviorDefList[i] as dtaBehaviorDefinition;
                if (def.m_BehaviorName == name)
                    return def;
            }
            return null;
        }

        public class dtaEffectOverridable
        {
            public bool m_Duration;
            public float m_DurationMin, m_DurationMax;
            public bool m_PlaybackSpeed;
            public float m_PlaybackSpeedMin, m_PlaybackSpeedMax;
            public bool m_ColorTint;
            public uint m_ColorTintMin, m_ColorTintMax;
            public bool m_Zoom;
            public float m_ZoomMin, m_ZoomMax;
//            public bool m_SizeScale;
//            public float m_SizeScaleXMin, m_SizeScaleXMax;
//            public float m_SizeScaleYMin, m_SizeScaleYMax;
//            public float m_SizeScaleZMin, m_SizeScaleZMax;
//            public bool m_InheritDirection;
//            public bool m_FlipDirection;

            public virtual void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_Duration          = buff.Read_bool();
                m_DurationMin       = buff.Read_float();
                m_DurationMax       = buff.Read_float();
                m_PlaybackSpeed     = buff.Read_bool();
                m_PlaybackSpeedMin  = buff.Read_float();
                m_PlaybackSpeedMax  = buff.Read_float();
                m_ColorTint         = buff.Read_bool();
                m_ColorTintMin      = buff.Read_u32();
                m_ColorTintMax      = buff.Read_u32();
                m_Zoom              = buff.Read_bool();
                m_ZoomMin           = buff.Read_float();
                m_ZoomMax           = buff.Read_float();
//                m_SizeScale         = buff.Read_bool();
//                m_SizeScaleXMin     = buff.Read_float();
//                m_SizeScaleYMin = buff.Read_float();
//                m_SizeScaleZMin = buff.Read_float();
//                m_SizeScaleXMax = buff.Read_float();
//                m_SizeScaleYMax     = buff.Read_float();
//                m_SizeScaleZMax     = buff.Read_float();
//                m_InheritDirection = buff.Read_bool();
//                m_FlipDirection = buff.Read_bool();
            }

            public virtual void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_bool(    m_Duration           );
                buff.Write_float(   m_DurationMin        );
                buff.Write_float(   m_DurationMax        );
                buff.Write_bool(    m_PlaybackSpeed      );
                buff.Write_float(   m_PlaybackSpeedMin   );
                buff.Write_float(   m_PlaybackSpeedMax   );
                buff.Write_bool(    m_ColorTint          );
                buff.Write_u32(     m_ColorTintMin       );
                buff.Write_u32(     m_ColorTintMax       );
                buff.Write_bool(    m_Zoom               );
                buff.Write_float(   m_ZoomMin            );
                buff.Write_float(   m_ZoomMax            );
//                buff.Write_bool(    m_SizeScale          );
//                buff.Write_float(   m_SizeScaleXMin      );
//                buff.Write_float(m_SizeScaleYMin);
//                buff.Write_float(m_SizeScaleZMin);
//                buff.Write_float(m_SizeScaleXMax);
//                buff.Write_float(m_SizeScaleYMax);
//                buff.Write_float(   m_SizeScaleZMax      );
//                buff.Write_bool(m_InheritDirection);
//                buff.Write_bool(m_FlipDirection);
            }
        };

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing the ptxEffect
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaEffectRule : IRageXmlSerializable
        {
            public String m_Path = "";
            public String m_Name = "";
            public String m_ClassName = "";
            public float m_Version = 0.0f;
 //           public byte m_Type = 0;
            public int m_Hash = 0;
            public int m_ID = -1;

            public override string ToString()
            {
                return m_Name;
            }

            public virtual void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_ClassName = "ptxEffectRule";
                buff.SetReadPos(8);
                m_Name = buff.Read_const_char();
                m_Hash = (int)buff.Read_u32();
                m_Version = buff.Read_float();
                m_Name = buff.Read_const_char();  //yes, it is in here twice

            }
            public virtual void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_const_char(m_Name);
                buff.Write_u32((uint)m_Hash);
//                buff.Write_u8(m_Type);
                buff.Write_float(m_Version);
                buff.Write_const_char(m_Name);  //yes, it is in here twice
            }

            public virtual void LoadFromXml(XmlNode root)
            {
                FindXmlStringValue(root, "Path", ref m_Path);
                FindXmlStringValue(root, "Name", ref m_Name);
                FindXmlFloatValue(root, "Version", ref m_Version);
                FindXmlIntValue(root, "ID", ref m_ID);
            }

            public virtual void SaveToXml(XmlDocument doc, XmlNode root)
            {
                SaveXmlStringValue(doc, root, "Path", m_Path);
                SaveXmlStringValue(doc, root, "Name", m_Name);
                SaveXmlFloatValue(doc, root, "Version", m_Version);
                SaveXmlIntValue(doc, root, "ID", m_ID);
            }
            public virtual bool RenameEmitterRule(String oldname,String newname)
            {
                return false;
            }
            public virtual bool RenamePtxRule(String oldname,String newname)
            {
                return false;
            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing ptxStdEffect
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaEffectRuleStd : dtaEffectRule, IRageXmlSerializable
        {
            //<struct name="m_TimeLine" type="rage::ptxTimeLine"/>
            //<pointer name="m_PtxEvoGroup" type="rage::ptxEvolutionGroup" policy="owner"/>

			//Global Props
			public float m_PreUpdate = 0.0f;
            public float m_PreUpdateInterval = 0.25f;
			public int m_NumLoops    = 0;
            public float m_DurationMin   = 1.0f;
            public float m_DurationMax = 1.0f;
            public float m_TimeScalarMin = 1.0f;
            public float m_TimeScalarMax = 1.0f;
            public bool  m_UseRandomColorTint = false;
            public Float3 m_RndOffsetPos = new Float3();
            public byte m_DrawList = 0;

            //Culling
//            public bool m_ZoomCullDist = true;
//            public bool m_UseCullSphere = false;
//            public bool m_CullSpNoUpdate = false;
//            public bool m_CullSpNoEmit = false;
//            public bool m_CullSpNoDraw = true;
            public bool m_HasDataSphere = false;
            public byte m_DataObjectType = 0;
//            public Float3 m_CullSphere;	        //Cullsphere offset to emitter 
//            public float m_CullRadius = 1.0f;	//Cullsphere radius
//            public float m_CullDistance = -1.0f;	//Cullout distance from camera -1 for no culling
//            public float m_FadeDistance = -1.0f; // Start fading out at this dist (-1 for no fading)
//            public float m_LodNearDistance = -1.0f; // start LOD transition
//            public float m_LodFarDistance = -1.0f; // end LOD transition
            public ArrayList m_GameSpecificFlags = new ArrayList();

            //New Culling
            public byte m_CullViewportMode = 0;
            public bool m_CullViewportRender = true;
            public bool m_CullViewportUpdate = true;
            public bool m_CullViewportEmit = true;
            public float m_CullViewportRadius = 1.0f;
            public float m_CullViewportOffsetX = 0.0f;
            public float m_CullViewportOffsetY = 0.0f;
            public float m_CullViewportOffsetZ = 0.0f;
            public byte m_CullDistanceMode = 0;
            public bool m_CullDistanceRender = true;
            public bool m_CullDistanceUpdate = true;
            public bool m_CullDistanceEmit = true;
            public float m_CullDistanceFadeDist = 25.0f;
            public float m_CullDistanceCullDist = 50.0f;
            public float m_LODNearDist = -1.0f;
            public float m_LODFarDist = -1.0f;
            public byte m_ColnType = 0;
            public float m_ColnRange = 0.0f;
            public float m_ColnProbeDist = 0.0f;
			public bool m_ColnUseEntity = false;
			public bool m_ColnOnlyBVH = true;

            //Debug
            public bool m_ShowInstances = false;
            public bool m_ShowCullSpheres = false;
            public bool m_ShowDataSpheres = false;
            public float m_Scale = 100.0f;
            public bool m_ShowPhysics = false;
            public bool m_ShowPointQuads = false;

            //Keyframes
            public dtaPtxKeyframePropList m_KeyframeList = new dtaPtxKeyframePropList();

            //Evolution
            public dtaEvolutionList m_EvolutionList = new dtaEvolutionList();

            //Timeline
            public bool m_SortEvents = false;
            public dtaTimeLine m_TimeLine = new dtaTimeLine();

            public void RenameEvolution(String evoname, String newname)
            {
                foreach (dtaEvolution evo in m_EvolutionList.m_Evolutions)
                    if (evo.m_Name == evoname)
                        evo.m_Name = newname;
   
                m_TimeLine.RenameEvolution(evoname, newname);
            }

            public override void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                base.LoadFromByteBuffer(buff);

				// timeline and evolution data
				m_TimeLine.LoadFromByteBuffer(buff);
				m_EvolutionList.LoadFromByteBuffer(buff);

				// effect setup
				m_RndOffsetPos.x = buff.Read_float();
				m_RndOffsetPos.y = buff.Read_float();
				m_RndOffsetPos.z = buff.Read_float();
				m_NumLoops = buff.Read_s32();
				m_PreUpdate = buff.Read_float();
                m_PreUpdateInterval = buff.Read_float();
				m_DurationMin = buff.Read_float();
				m_DurationMax = buff.Read_float();
				m_TimeScalarMin = buff.Read_float();
				m_TimeScalarMax = buff.Read_float();
				m_SortEvents = buff.Read_bool();
				m_DrawList = buff.Read_u8();

				// culling
				m_CullViewportMode = buff.Read_u8();
				m_CullViewportRender = buff.Read_bool();
				m_CullViewportUpdate = buff.Read_bool();
				m_CullViewportEmit = buff.Read_bool();
				m_CullDistanceMode = buff.Read_u8();
				m_CullDistanceRender = buff.Read_bool();
				m_CullDistanceUpdate = buff.Read_bool();
				m_CullDistanceEmit = buff.Read_bool();
				m_CullViewportRadius = buff.Read_float();
				m_CullViewportOffsetX = buff.Read_float();
				m_CullViewportOffsetY = buff.Read_float();
				m_CullViewportOffsetZ = buff.Read_float();
				m_CullDistanceFadeDist = buff.Read_float();
				m_CullDistanceCullDist = buff.Read_float();

				// lod
				m_LODNearDist = buff.Read_float();
				m_LODFarDist = buff.Read_float();

				// collision
				m_ColnType = buff.Read_u8();
				m_ColnUseEntity = buff.Read_bool();
				m_ColnOnlyBVH = buff.Read_bool();
				m_ColnRange = buff.Read_float();
				m_ColnProbeDist = buff.Read_float();

				// game specific
				m_GameSpecificFlags.Clear();
				int cnt = buff.Read_s32();
				for (int i = 0; i < cnt; i++)
				{
					dtaGameSpecificDataBool dta = new dtaGameSpecificDataBool();
					dta.m_Name = m_SDataMod.m_GameSpecificDataEffectRuleBoolList[i].ToString();
					dta.m_State = buff.Read_bool();
					m_GameSpecificFlags.Add(dta);
				}

				// keyframes
				m_UseRandomColorTint = buff.Read_bool();
				m_HasDataSphere = buff.Read_bool();
				m_DataObjectType = buff.Read_u8();
				m_KeyframeList.LoadFromByteBuffer(buff);

				// zoom
				m_Scale = buff.Read_float();

				// info

				// ui data
				m_ShowInstances = buff.Read_bool();
				m_ShowCullSpheres = buff.Read_bool();
				m_ShowPhysics = buff.Read_bool();
				m_ShowDataSpheres = buff.Read_bool();
                m_ShowPointQuads = buff.Read_bool();
            }
            public override void WriteToByteBuffer(tcpByteBuffer buff)
            {
                base.WriteToByteBuffer(buff);

				// timeline and evolution data
				m_TimeLine.WriteToByteBuffer(buff);
				m_EvolutionList.WriteToByteBuffer(buff);

				// effect setup
				buff.Write_float(m_RndOffsetPos.x);
				buff.Write_float(m_RndOffsetPos.y);
				buff.Write_float(m_RndOffsetPos.z);
				buff.Write_s32(m_NumLoops);
				buff.Write_float(m_PreUpdate);
                buff.Write_float(m_PreUpdateInterval);
				buff.Write_float(m_DurationMin);
				buff.Write_float(m_DurationMax);
				buff.Write_float(m_TimeScalarMin);
				buff.Write_float(m_TimeScalarMax);
				buff.Write_bool(m_SortEvents);
				buff.Write_u8(m_DrawList);

				// culling	
				buff.Write_u8(m_CullViewportMode);
				buff.Write_bool(m_CullViewportRender);
				buff.Write_bool(m_CullViewportUpdate);
				buff.Write_bool(m_CullViewportEmit);
				buff.Write_u8(m_CullDistanceMode);
				buff.Write_bool(m_CullDistanceRender);
				buff.Write_bool(m_CullDistanceUpdate);
				buff.Write_bool(m_CullDistanceEmit);
				buff.Write_float(m_CullViewportRadius);
				buff.Write_float(m_CullViewportOffsetX);
				buff.Write_float(m_CullViewportOffsetY);
				buff.Write_float(m_CullViewportOffsetZ);
				buff.Write_float(m_CullDistanceFadeDist);
				buff.Write_float(m_CullDistanceCullDist);

				// lod
				buff.Write_float(m_LODNearDist);
				buff.Write_float(m_LODFarDist);

				// collision
				buff.Write_u8(m_ColnType);
				buff.Write_bool(m_ColnUseEntity);
				buff.Write_bool(m_ColnOnlyBVH);
				buff.Write_float(m_ColnRange);
				buff.Write_float(m_ColnProbeDist);

				// game specific
				buff.Write_s32(m_SDataMod.m_GameSpecificDataEffectRuleBoolList.Count);
				for (int i = 0; i < m_SDataMod.m_GameSpecificDataEffectRuleBoolList.Count; i++)
				{
					dtaGameSpecificDataBool dta = m_GameSpecificFlags[i] as dtaGameSpecificDataBool;
					buff.Write_bool(dta.m_State);
				}

				// keyframes
				buff.Write_bool(m_UseRandomColorTint);
				buff.Write_bool(m_HasDataSphere);
				buff.Write_u8(m_DataObjectType);

				// zoom
				buff.Write_float(m_Scale);

				// info

				// ui data
				buff.Write_bool(m_ShowInstances);
				buff.Write_bool(m_ShowCullSpheres);
				buff.Write_bool(m_ShowPhysics);
				buff.Write_bool(m_ShowDataSpheres);
                buff.Write_bool(m_ShowPointQuads);
            }

            public override void LoadFromXml(XmlNode root)
            {
                base.LoadFromXml(root);

                FindXmlFloatValue(root, "DurationMin", ref m_DurationMin);
                FindXmlFloatValue(root, "DurationMax", ref m_DurationMax);
                FindXmlFloatValue(root, "TimeScalarMin", ref m_TimeScalarMin);
                FindXmlFloatValue(root, "TimeScalarMax", ref m_TimeScalarMax);

//                FindXmlBoolValue(root, "ZoomCullDist", ref m_ZoomCullDist);
//                FindXmlBoolValue(root, "UseCullSphere", ref m_UseCullSphere);
//                FindXmlBoolValue(root, "CullNoUpdate", ref m_CullSpNoUpdate);
//                FindXmlBoolValue(root, "CullNoEmit", ref m_CullSpNoEmit);
//                FindXmlBoolValue(root, "CullNoDraw", ref m_CullSpNoDraw);

//                FindXmlFloatValue(root, "CullRadius", ref m_CullRadius);
//                FindXmlFloatValue(root, "CullDistance", ref m_CullDistance);
//                FindXmlFloatValue(root, "FadeDistance", ref m_FadeDistance);
//                FindXmlFloatValue(root, "LodNearDistance", ref m_FadeDistance);
//                FindXmlFloatValue(root, "LodFarDistance", ref m_FadeDistance);

                FindXmlByteValue(root, "CullViewportMode", ref m_CullViewportMode);
                FindXmlBoolValue(root, "CullViewportRender", ref m_CullViewportRender);
                FindXmlBoolValue(root, "CullViewportUpdate", ref m_CullViewportUpdate);
                FindXmlBoolValue(root, "CullViewportEmit", ref m_CullViewportEmit);
                FindXmlFloatValue(root, "CullViewportRadius", ref m_CullViewportRadius);
                FindXmlFloatValue(root, "CullViewportOffsetX", ref m_CullViewportOffsetX);
                FindXmlFloatValue(root, "CullViewportOffsetY", ref m_CullViewportOffsetY);
                FindXmlFloatValue(root, "CullViewportOffsetZ", ref m_CullViewportOffsetZ);
                FindXmlByteValue(root, "CullDistanceMode", ref m_CullDistanceMode);
                FindXmlBoolValue(root, "CullDistanceRender", ref m_CullDistanceRender);
                FindXmlBoolValue(root, "CullDistanceUpdate", ref m_CullDistanceUpdate);
                FindXmlBoolValue(root, "CullDistanceEmit", ref m_CullDistanceEmit);
                FindXmlFloatValue(root, "CullDistanceFadeDist", ref m_CullDistanceFadeDist);
                FindXmlFloatValue(root, "CullDistanceCullDist", ref m_CullDistanceCullDist);
                FindXmlFloatValue(root, "LODNearDist", ref m_LODNearDist);
                FindXmlFloatValue(root, "LODFarDist", ref m_LODFarDist);
                FindXmlByteValue(root, "CollisionType", ref m_ColnType);
                FindXmlFloatValue(root, "CollisionRange", ref m_ColnRange);
                FindXmlFloatValue(root, "CollisionProbeDist", ref m_ColnProbeDist);
                FindXmlBoolValue(root, "CollisionUseEntity", ref m_ColnUseEntity);
				FindXmlBoolValue(root, "CollisionOnlyBVH", ref m_ColnOnlyBVH);
            }

            public override void SaveToXml(XmlDocument doc, XmlNode root)
            {
                base.SaveToXml(doc, root);
                SaveXmlFloatValue(doc, root, "DurationMin",  m_DurationMin);
                SaveXmlFloatValue(doc, root, "DurationMax",  m_DurationMax);
                SaveXmlFloatValue(doc, root, "TimeScalarMin",  m_TimeScalarMin);
                SaveXmlFloatValue(doc, root, "TimeScalarMax",  m_TimeScalarMax);

//                SaveXmlBoolValue(doc, root, "ZoomCullDist",  m_ZoomCullDist);
//                SaveXmlBoolValue(doc, root, "UseCullSphere",  m_UseCullSphere);
//                SaveXmlBoolValue(doc, root, "CullNoUpdate",  m_CullSpNoUpdate);
//                SaveXmlBoolValue(doc, root, "CullNoEmit",  m_CullSpNoEmit);
//                SaveXmlBoolValue(doc, root, "CullNoDraw",  m_CullSpNoDraw);

//                SaveXmlFloatValue(doc, root, "CullRadius",  m_CullRadius);
//                SaveXmlFloatValue(doc, root, "CullDistance",  m_CullDistance);
//                SaveXmlFloatValue(doc, root, "FadeDistance", m_FadeDistance);
//                SaveXmlFloatValue(doc, root, "LodNearDistance", m_FadeDistance);
//                SaveXmlFloatValue(doc, root, "LodFarDistance", m_FadeDistance);

                SaveXmlByteValue(doc, root, "CullViewportMode", m_CullViewportMode);
                SaveXmlBoolValue(doc, root, "CullViewportRender", m_CullViewportRender);
                SaveXmlBoolValue(doc, root, "CullViewportUpdate", m_CullViewportUpdate);
                SaveXmlBoolValue(doc, root, "CullViewportEmit", m_CullViewportEmit);
                SaveXmlFloatValue(doc, root, "CullViewportRadius", m_CullViewportRadius);
                SaveXmlFloatValue(doc, root, "CullViewportOffsetX", m_CullViewportOffsetX);
                SaveXmlFloatValue(doc, root, "CullViewportOffsetY", m_CullViewportOffsetY);
                SaveXmlFloatValue(doc, root, "CullViewportOffsetZ", m_CullViewportOffsetZ);
                SaveXmlByteValue(doc, root, "CullDistanceMode", m_CullDistanceMode);
                SaveXmlBoolValue(doc, root, "CullDistanceRender", m_CullDistanceRender);
                SaveXmlBoolValue(doc, root, "CullDistanceUpdate", m_CullDistanceUpdate);
                SaveXmlBoolValue(doc, root, "CullDistanceEmit", m_CullDistanceEmit);
                SaveXmlFloatValue(doc, root, "CullDistanceFadeDist", m_CullDistanceFadeDist);
                SaveXmlFloatValue(doc, root, "CullDistanceCullDist", m_CullDistanceCullDist);
                SaveXmlFloatValue(doc, root, "LODNearDist", m_LODNearDist);
                SaveXmlFloatValue(doc, root, "LODFarDist", m_LODFarDist);
                SaveXmlByteValue(doc, root, "CollisionType", m_ColnType);
                SaveXmlFloatValue(doc, root, "CollisionRange", m_ColnRange);
                SaveXmlFloatValue(doc, root, "CollisionProbeDist", m_ColnProbeDist);
                SaveXmlBoolValue(doc, root, "CollisionUseEntity", m_ColnUseEntity);
				SaveXmlBoolValue(doc, root, "CollisionOnlyBVH", m_ColnOnlyBVH);
            }

            public override bool RenameEmitterRule(string oldname, string newname)
            {
                bool result = false;
                for(int i=0;i<m_TimeLine.m_Events.Count;i++)
                {
                    if (m_TimeLine.m_Events[i] is dtaEventEmitter)
                    {
                        dtaEventEmitter dve = m_TimeLine.m_Events[i] as dtaEventEmitter;
                        if (dve.m_EmitRuleName == oldname)
                        {
                            dve.m_EmitRuleName = newname;
                            result = true;
                        }
                    }
                }
                return result;
            }
            public override bool RenamePtxRule(string oldname, string newname)
            {
                bool result = false;
                for (int i = 0; i < m_TimeLine.m_Events.Count; i++)
                {
                    if (m_TimeLine.m_Events[i] is dtaEventEmitter)
                    {
                        dtaEventEmitter dve = m_TimeLine.m_Events[i] as dtaEventEmitter;
                        if (dve.m_PtxRuleName == oldname)
                        {
                            dve.m_PtxRuleName = newname;
                            result = true;
                        }
                    }
                }
                return result;
            }
        };

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaTimeLine : IRageXmlSerializable
        {
//            public float m_PreUpdate = 0.0f;
//          public int m_NumLoops    = 0;
            public ArrayList m_Events = new ArrayList();

            public void RenameEvolution(String evoname, String newname)
            {
                foreach (dtaEvent evt in m_Events)
                    evt.RenameEvolution(evoname, newname);
            }

            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_Events.Clear();
//                m_PreUpdate = buff.Read_float();
//                m_NumLoops = buff.Read_s32();
                int eventcnt = buff.Read_s32();
                for(int i=0;i<eventcnt;i++)
                {
                    dtaEvent evt = null;
                    //determine type
                    int type = buff.Read_s32();
                    switch(type)
                    {
                        case 0: 
                            evt = new dtaEventEmitter();
                            evt.LoadFromByteBuffer(buff);
                            break;
                        case 1:
                            evt = new dtaEventEffect();
                            evt.LoadFromByteBuffer(buff);
                            break;
                    }
                    m_Events.Add(evt);
                }
            }

            public void WriteToByteBuffer(tcpByteBuffer buff)
            {
//                buff.Write_float(m_PreUpdate);
//                buff.Write_s32(m_NumLoops);
                buff.Write_s32(m_Events.Count);
                foreach (dtaEvent evt in m_Events)
                    evt.WriteToByteBuffer(buff);
            }

            public void LoadFromXml(XmlNode root)
            {
//                FindXmlFloatValue(root, "PreUpdate", ref m_PreUpdate);
//                FindXmlIntValue(root, "NumLoops", ref m_NumLoops);
                // how do we load the events array?
            }

            public void SaveToXml(XmlDocument doc, XmlNode root)
            {
//                SaveXmlFloatValue(doc, root, "PreUpdate", m_PreUpdate);
//                SaveXmlIntValue(doc, root, "NumLoops", m_NumLoops);
                // how do we save the events array?
            }

        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing the ptxEvent
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaEvent
        {
            public int      m_Type = -1;
            public int      m_Index = -1;
 //           public float    m_TriggerTime = 0.0f;
			public float m_StartRatio = 0.0f;
			public float m_EndRatio = 0.0f;
 //           public int      m_TriggerCap    = -1;
            public bool     m_ActiveToggle = true;
            public Object   m_Data = null;  //Used to communicate back to the timeline
            public dtaEvolutionList m_EvoList = new dtaEvolutionList();

//            public virtual Float2 GetDuration()
//            {
//                return new Float2(0.0f,0.0f);
//            }
            public float GetStartRatio()
            {
				return m_StartRatio;
            }
			public virtual float GetEndRatio()
			{
				return m_EndRatio;
			}
            public virtual System.Drawing.Color GetColor()
            {
                return System.Drawing.Color.Black;
            }

            public override String ToString()
            {
                return "Undefined";
            }

            public void RenameEvolution(String evoname, String newname)
            {
                foreach (dtaEvolution evo in m_EvoList.m_Evolutions)
                    if (evo.m_Name == evoname)
                        evo.m_Name = newname;
            }

            public virtual void WriteToByteBuffer(tcpByteBuffer buff)
			{
				buff.Write_s32(m_Type);
				buff.Write_s32(m_Index);
 //               buff.Write_float(m_TriggerTime);
				buff.Write_float(m_StartRatio);
				buff.Write_float(m_EndRatio);

 //               buff.Write_s32(m_TriggerCap);

                //Evolution
                m_EvoList.WriteToByteBuffer(buff);

				buff.Write_bool(m_ActiveToggle);
            }
            public virtual void LoadFromByteBuffer(tcpByteBuffer buff)
			{
				m_Index = buff.Read_s32();
//                m_TriggerTime   = buff.Read_float();
				m_StartRatio = buff.Read_float();
				m_EndRatio = buff.Read_float();
 //               m_TriggerCap    = buff.Read_s32();
                
                //Evolution
                m_EvoList.LoadFromByteBuffer(buff);

				m_ActiveToggle = buff.Read_bool();
            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing the ptxEventEmitter
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaEventEmitter : dtaEvent
        {
            public String m_EmitRuleName = "";
            public String m_PtxRuleName = "";
			public bool m_isOneShot = false;
//            public float m_EmitterDuration = 0.0f;
//            public float m_DurationScalarMin = 0.0f;
//            public float m_DurationScalarMax = 0.0f;
            public float m_TimeScalarMin = 0.0f;
            public float m_TimeScalarMax = 0.0f;
            public float m_ZoomMin = 100.0f;
            public float m_ZoomMax = 100.0f;
            public System.Drawing.Color m_ColorTintMin;
            public System.Drawing.Color m_ColorTintMax;
            

            public dtaEventEmitter()
            {
                m_Type = 0;
            }

            public override string ToString()
            {
                return m_EmitRuleName;
            }

			public override float GetEndRatio()
			{
				dtaStdEmitRule doe = GetEmitRule(m_EmitRuleName) as dtaStdEmitRule;
                if (doe != null)
                {
					// we have an emitter rule - use it to determine the end ratio
					if (doe.m_OneShot)
					{
						return m_StartRatio;
					}

					return m_EndRatio;
                }

				// we don't have an emitter rule - use the initial value passed to the event to determine the end ratio
				if (m_isOneShot)
				{
					return m_StartRatio;
				}

				return m_EndRatio;
			}
//            public override Float2 GetDuration()
//            {
//				dtaStdEmitRule doe = GetEmitRule(m_EmitRuleName) as dtaStdEmitRule;
//                if (doe != null)
//                {
//					if (doe.m_OneShot)
//						return new Float2(0.0f, 0.0f);
//                }
//
//				return new Float2(0.1f, 0.2f);
//
//                dtaStdEmitRule doe = GetEmitRule(m_EmitRuleName) as dtaStdEmitRule;
//                if (doe != null)
//                {
//                    m_EmitterDuration = doe.m_Duration;
//                    if (doe.m_OneShot)
//						m_EmitterDuration = 0.0f;
//                }
//				float tsmin = 1.0f;// Math.Max(0.0001f, m_TimeScalarMin);
//				float tsmax = 1.0f;// Math.Max(0.0001f, m_TimeScalarMax);
//				return new Float2((m_EmitterDuration * m_DurationScalarMin) / tsmax, (m_EmitterDuration * m_DurationScalarMax) / tsmin);
//            }

            public override System.Drawing.Color GetColor()
            {
                if( (m_EmitRuleName == "") || (m_PtxRuleName==""))
                    return System.Drawing.Color.FromArgb(255, 255, 0, 0);
                return System.Drawing.Color.FromArgb(255, 127, 177, 255);
            }


            public override void WriteToByteBuffer(tcpByteBuffer buff)
            {
                base.WriteToByteBuffer(buff);
                buff.Write_const_char(m_EmitRuleName);
                buff.Write_const_char(m_PtxRuleName);
//                buff.Write_float(m_DurationScalarMin);
//				buff.Write_float(m_DurationScalarMax);
				buff.Write_float(m_TimeScalarMin);
				buff.Write_float(m_TimeScalarMax);
                buff.Write_float(m_ZoomMin);
                buff.Write_float(m_ZoomMax);
                buff.Write_u8(m_ColorTintMin.R);
                buff.Write_u8(m_ColorTintMin.G);
                buff.Write_u8(m_ColorTintMin.B);
                buff.Write_u8(m_ColorTintMin.A);
                buff.Write_u8(m_ColorTintMax.R);
                buff.Write_u8(m_ColorTintMax.G);
                buff.Write_u8(m_ColorTintMax.B);
                buff.Write_u8(m_ColorTintMax.A);

            }
            public override void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                byte r, g, b, a;
                base.LoadFromByteBuffer(buff);
                m_EmitRuleName      = buff.Read_const_char();
                m_PtxRuleName       = buff.Read_const_char();
				m_isOneShot = buff.Read_bool();
//                m_EmitterDuration   = buff.Read_float();
//                m_DurationScalarMin = buff.Read_float();
//				m_DurationScalarMax = buff.Read_float();
				m_TimeScalarMin = buff.Read_float();
				m_TimeScalarMax = buff.Read_float();
                m_ZoomMin           = buff.Read_float();
                m_ZoomMax           = buff.Read_float();
                r=buff.Read_u8();g=buff.Read_u8();b=buff.Read_u8();a=buff.Read_u8();
                m_ColorTintMin = System.Drawing.Color.FromArgb(a, r, g, b);
                r = buff.Read_u8(); g = buff.Read_u8(); b = buff.Read_u8(); a = buff.Read_u8();
                m_ColorTintMax = System.Drawing.Color.FromArgb(a, r, g, b);
            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing the ptxEventEffect
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaEventEffect : dtaEvent
        {
            public String       m_EffectName = "";
            public dtaDomain    m_TriggerDomain = new dtaDomain();
            public Float3       m_Orientation;
            public bool         m_ShowTriggerDomain = false;
            public bool         m_ShowOrientation = false;
            public int      m_TriggerCap    = -1;

            public dtaEffectOverridable m_Overridables = new dtaEffectOverridable();

            public dtaEventEffect()
            {
                m_Type = 1;
            }

            public override string ToString()
            {
                if (m_EffectName == null)
                    m_EffectName = "";
                return m_EffectName;
            }
//            public override Float2 GetDuration()
//            {
//                return new Float2(0.0f,0.0f);
//            }
            public override System.Drawing.Color GetColor()
            {
                if(m_EffectName == null || m_EffectName == "")
                    return System.Drawing.Color.FromArgb(255, 255, 0, 0);
                return System.Drawing.Color.FromArgb(255, 127, 255, 177);
            }

            public override void WriteToByteBuffer(tcpByteBuffer buff)
            {
                base.WriteToByteBuffer(buff);
                buff.Write_const_char(m_EffectName);
				buff.Write_s32(m_TriggerCap);
                m_TriggerDomain.WriteToByteBuffer(buff);

                buff.Write_float(m_Orientation.x);
                buff.Write_float(m_Orientation.y);
                buff.Write_float(m_Orientation.z);

				m_Overridables.WriteToByteBuffer(buff);

				buff.Write_bool(m_ShowTriggerDomain);
				buff.Write_bool(m_ShowOrientation);
            }
            public override void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                base.LoadFromByteBuffer(buff);
                m_EffectName = buff.Read_const_char();
				m_TriggerCap = buff.Read_s32();
                m_TriggerDomain.LoadFromByteBuffer(buff);

                m_Orientation.x = buff.Read_float();
                m_Orientation.y = buff.Read_float();
                m_Orientation.z = buff.Read_float();

				m_Overridables.LoadFromByteBuffer(buff);

				m_ShowTriggerDomain = buff.Read_bool();
				m_ShowOrientation = buff.Read_bool();
            }
        };


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing the ptxEmitterRule
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaEmitRule : IRageXmlSerializable
        {
            public string m_ClassName = "";
            public string m_Path = "";
            public string m_Name = "";
            public int m_Hash    = 0;
            public int m_ID      = -1;
            public float m_Version = 0.0f;
			

            public virtual void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_ClassName = "ptxEmitterRule";
                buff.SetReadPos(8);
                m_Name = buff.Read_const_char();
                m_Hash = (int)buff.Read_u32();
                m_Version = buff.Read_float();
                m_Name = buff.Read_const_char();  //yes, it is in here twice

            }
            public virtual void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_const_char(m_Name);
                buff.Write_u32((uint)m_Hash);
                buff.Write_float(m_Version);
                buff.Write_const_char(m_Name);  //yes, it is in here twice
            }

            public virtual void LoadFromXml(XmlNode root)
            {
                FindXmlStringValue(root, "Path", ref m_Path);
                FindXmlStringValue(root, "Name", ref m_Name);
                FindXmlIntValue(root, "ID", ref m_ID);
            }
            public virtual void SaveToXml(XmlDocument doc, XmlNode root)
            {
                SaveXmlStringValue(doc, root, "Path", m_Path);
                SaveXmlStringValue(doc, root, "Name", m_Name);
                SaveXmlIntValue(doc, root, "ID", m_ID);
            }

            
        };

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing the ptxStdEmitterRule
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaStdEmitRule : dtaEmitRule, IRageXmlSerializable
        {
//            public float m_Duration=1.0f;
            public bool m_OneShot = false;
            public bool m_ShowEmitterDomain = false;
			public bool m_ShowVelocityDomain = false;
			public bool m_ShowAttractorDomain = false;

            public dtaDomain m_EmitterDomain = new dtaDomain();
			public dtaDomain m_VelocityDomain = new dtaDomain();
			public dtaDomain m_AttractorDomain = new dtaDomain();

            public dtaPtxKeyframePropList m_KeyframeList = new dtaPtxKeyframePropList();

            public override void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                base.LoadFromByteBuffer(buff);
//                m_Duration = buff.Read_float();
                m_OneShot = buff.Read_bool();

                //Emitter Domain
                m_EmitterDomain.LoadFromByteBuffer(buff);
                //Velocity Domain
				m_VelocityDomain.LoadFromByteBuffer(buff);
				//Attractor Domain
				m_AttractorDomain.LoadFromByteBuffer(buff);
                //Keyframes
                m_KeyframeList.LoadFromByteBuffer(buff);

				m_ShowEmitterDomain = buff.Read_bool();
				m_ShowVelocityDomain = buff.Read_bool();
				m_ShowAttractorDomain = buff.Read_bool();
            }
            public override void WriteToByteBuffer(tcpByteBuffer buff)
            {
                base.WriteToByteBuffer(buff);
//                buff.Write_float(m_Duration);
                buff.Write_bool(m_OneShot);

                //Emitter Domain
                m_EmitterDomain.WriteToByteBuffer(buff);
                //Velocity Domain
				m_VelocityDomain.WriteToByteBuffer(buff);
				//Attractor Domain
				m_AttractorDomain.WriteToByteBuffer(buff);

				buff.Write_bool(m_ShowEmitterDomain);
				buff.Write_bool(m_ShowVelocityDomain);
				buff.Write_bool(m_ShowAttractorDomain);
            }

            public override void LoadFromXml(XmlNode root)
            {
                base.LoadFromXml(root);
//                FindXmlFloatValue(root, "Duration", ref m_Duration);
                FindXmlBoolValue(root, "OneShot", ref m_OneShot);
            }
            public override void SaveToXml(XmlDocument doc, XmlNode root)
            {
                base.SaveToXml(doc, root);
//                SaveXmlFloatValue(doc, root, "Duration", m_Duration);
                SaveXmlBoolValue(doc, root, "OneShot", m_OneShot);
            }
        };

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaDomain
        {
			public bool m_Exists;
            public int m_Type;
            public int m_Function;
            public bool m_WorldSpace;
            public bool m_PointRelative;
            public bool m_CreationRelative;
            public bool m_TargetRelative;

            //Keyframes
            public dtaPtxKeyframePropList m_KeyframeList = new dtaPtxKeyframePropList();
            
            public virtual void LoadFromByteBuffer(tcpByteBuffer buff)
            {
				m_Exists = buff.Read_bool();
				if (m_Exists)
				{
					m_Type = buff.Read_s32();
					m_Function = buff.Read_s32();
					m_WorldSpace = buff.Read_bool();
					m_PointRelative = buff.Read_bool();
                    m_CreationRelative = buff.Read_bool();
                    m_TargetRelative = buff.Read_bool();
					m_KeyframeList.LoadFromByteBuffer(buff);
				}
            }
            public virtual void WriteToByteBuffer(tcpByteBuffer buff)
            {
				buff.Write_bool(m_Exists);
				if (m_Exists)
				{
					buff.Write_s32(m_Type);
					buff.Write_s32(m_Function);
					buff.Write_bool(m_WorldSpace);
					buff.Write_bool(m_PointRelative);
                    buff.Write_bool(m_CreationRelative);
                    buff.Write_bool(m_TargetRelative);
				}
            }
        };

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing the ptxRenderState
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaPtxRenderState : IRageXmlSerializable
        {
            public int  m_BlendSet = 0;
            public int  m_LightingMode = 0;
            public bool m_DepthWrite = false;
            public bool m_DepthTest = true;
            public bool m_AlphaBlend = true;
            public int  m_CullMode = 0;
//            public float m_DepthBias = 0.0f;

            public virtual void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_u32((uint)m_BlendSet);
                buff.Write_u32((uint)m_LightingMode);
                buff.Write_bool(m_DepthWrite);
                buff.Write_bool(m_DepthTest);
                buff.Write_bool(m_AlphaBlend);
                buff.Write_u32((uint)m_CullMode);
//                buff.Write_float(m_DepthBias);
            }
            public virtual void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_BlendSet      = (int)buff.Read_u32();
                m_LightingMode  = (int)buff.Read_u32();
                m_DepthWrite    = buff.Read_bool();
                m_DepthTest     = buff.Read_bool();
                m_AlphaBlend    = buff.Read_bool();
                m_CullMode      = (int)buff.Read_u32();
//                m_DepthBias     = buff.Read_float();
            }
            

            public virtual void LoadFromXml(XmlNode root)
            {
                FindXmlIntValue(root, "BlendSet", ref m_BlendSet);
                FindXmlIntValue(root, "LightingMode", ref m_LightingMode);
                FindXmlBoolValue(root, "DepthWrite", ref m_DepthWrite);
                FindXmlBoolValue(root, "DepthTest", ref m_DepthTest);
                FindXmlBoolValue(root, "AlphaBlend", ref m_AlphaBlend);
                
            }
            public virtual void SaveToXml(XmlDocument doc, XmlNode root)
            {
                SaveXmlIntValue(doc, root, "BlendSet", m_BlendSet);
                SaveXmlIntValue(doc, root, "LightingMode", m_LightingMode);
                SaveXmlBoolValue(doc, root, "DepthWrite", m_DepthWrite);
                SaveXmlBoolValue(doc, root, "DepthTest", m_DepthTest);
                SaveXmlBoolValue(doc, root, "AlphaBlend", m_AlphaBlend);
            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing dtaPtxPropList
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaPtxKeyframePropList
        {
            public List<dtaPtxKeyframeProp> m_KeyframeProps = new List<dtaPtxKeyframeProp>();
            public dtaPtxKeyframeProp GetKeyframeProp(UInt32 propId)
            {
                for (int i = 0; i < m_KeyframeProps.Count; i++)
                    if (m_KeyframeProps[i].m_PropID == propId)
                        return m_KeyframeProps[i];
                return null;
            }

            public void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_s32(m_KeyframeProps.Count);
                for (int i = 0; i < m_KeyframeProps.Count; i++)
                    m_KeyframeProps[i].WriteToByteBuffer(buff);
            }
            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_KeyframeProps.Clear();
                int cnt = buff.Read_s32();
                for(int i=0;i<cnt;i++)
                {
                    dtaPtxKeyframeProp prop = new dtaPtxKeyframeProp();
                    prop.LoadFromByteBuffer(buff);
                    m_KeyframeProps.Add(prop);
                }
            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing dtaPtxKeyframeProp
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaPtxKeyframeProp
        {
            public int m_PropID;
            public bool m_InvertBiasLink;
            public KeyframeSpec m_KeyframeSpec;

            public void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_s32(m_PropID);
                buff.Write_bool(m_InvertBiasLink);
            }
            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_PropID = buff.Read_s32();
                m_InvertBiasLink = buff.Read_bool();
                m_KeyframeSpec = new KeyframeSpec();
                LoadXmlStruct(buff, m_KeyframeSpec);
            }
        };

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing dtaPtxBehavior
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaBehaviorVar
        {
            public String m_Name;
            public String m_Type;
            public dtaBehaviorDefinition m_Definition;
            //combined generic data for all types
            public dtaPtxKeyframeProp m_KeyframeProp = new dtaPtxKeyframeProp();
            public bool m_Bool;
            public int m_Int;
            public float m_Float;
            public float m_MinRange = -100000.0f;
            public float m_MaxRange = 100000.0f;
			public int m_ComboNumItems;
			public String[] m_ComboItemNames = new String[32];


            public void WriteToByteBuffer(tcpByteBuffer buff)
            {
                if (m_Type == "bool")
                {
                    buff.Write_bool(m_Bool);
                }
                if (m_Type == "int")
                {
                    buff.Write_s32(m_Int);
                }
                if (m_Type == "float")
                {
                    buff.Write_float(m_Float);
                }
                if (m_Type == "keyframe")
                {
                    m_KeyframeProp.WriteToByteBuffer(buff);
                }
				if (m_Type == "combo")
				{
					buff.Write_s32(m_Int);
				}
            }

            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                if (m_Type == "bool")
                {
                    m_Bool = buff.Read_bool();
                }
                if (m_Type == "int")
                {
                    m_Int = buff.Read_s32();
                }
                if (m_Type == "float")
                {
                    m_Float = buff.Read_float();
                }
                if (m_Type == "keyframe")
                {
                    m_KeyframeProp.LoadFromByteBuffer(buff);
				}
				if (m_Type == "combo")
				{
					m_Int = buff.Read_s32();
				}
            }

        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing dtaBehaviorDefinition
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaBehaviorDefinition
        {
            public String m_BehaviorName;   // This is the "decorative name"...
            public String m_BehaviorId;     // ...This is the "id" (these are sometimes very close, if not the same,
                                            //      but it's good to be able to change the decorative name freely from game-side.

            public String m_DrawType;
            
            public ArrayList m_Vars = null;
            public ArrayList m_Names = null;
            public bool m_CustomInterface = false;
            public String m_CustomInterfaceName = "";

            public void LoadDefinitionFromByteBuffer(tcpByteBuffer buff)
            {
                m_Vars = new ArrayList();
                m_Names = new ArrayList();
                m_BehaviorName = buff.Read_const_char();
                m_BehaviorId = buff.Read_const_char();
                m_DrawType = buff.Read_const_char();
                m_CustomInterface = buff.Read_bool();
                if (m_CustomInterface)
                    m_CustomInterfaceName = buff.Read_const_char();

                int cnt = (int)buff.Read_u8();
                for(int i=0;i<cnt;i++)
                {
                    dtaBehaviorVar var = new dtaBehaviorVar();
                    var.m_Type = buff.Read_const_char();
                    var.m_Name = buff.Read_const_char();
                    if(var.m_Type == "float")
                    {
                        var.m_MinRange = buff.Read_float();
                        var.m_MaxRange = buff.Read_float();
                    }
                    if(var.m_Type == "int")
                    {
                        var.m_MinRange = (float)buff.Read_s32();
                        var.m_MaxRange = (float)buff.Read_s32();
					}
					if (var.m_Type == "combo")
					{
						var.m_ComboNumItems = buff.Read_s32();
						for (int j=0; j<var.m_ComboNumItems; j++)
						{
							var.m_ComboItemNames[j] = buff.Read_const_char();
						}
					}
                    var.m_Definition = this;
                    m_Vars.Add(var);
                    m_Names.Add(var.m_Name);
                }
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing dtaPtxBehavior
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaPtxBehavior
        {
            public dtaBehaviorDefinition m_Definition = null;
            public dtaPtxRule m_PtxRule = null;
            public String m_BehaviorName;
            public String m_BehaviorId;
            public ArrayList m_Vars = new ArrayList();
            public dtaPtxKeyframePropList m_Keyframes = new dtaPtxKeyframePropList();
            public bool HasCustomInterface() 
            {
                if (m_Definition != null)
                    return m_Definition.m_CustomInterface;
                return false;
            }
            public String GetCustomInterfaceName()
            {
                if (m_Definition != null)
                    return m_Definition.m_CustomInterfaceName;
                return "";
            }

            public dtaBehaviorVar GetBehaviorVar(String name)
            {
                foreach (dtaBehaviorVar var in m_Vars)
                {
                    if (var.m_Name == name)
                        return var;
                }
                return null;
            }
            public override string ToString()
            {
                return m_BehaviorName;
            }
            public static int Compare(dtaPtxBehavior A, dtaPtxBehavior B)
            {
                return A.m_BehaviorName.CompareTo(B.m_BehaviorName);
            }

            public bool ContainsEvolution(int evoID,DataMod.dtaEvolutionList evoList)
            {
                //go thru the vars and see if we have a matching keyframe tag
               foreach (DataMod.dtaBehaviorVar var in m_Vars)
                {
                   foreach(dtaPtxKeyframeProp keyProp in m_Keyframes.m_KeyframeProps )
                   {
                        DataMod.ptxEvoPropBase propBase = evoList.GetPropBaseFromID((UInt32)keyProp.m_PropID);
                        if(propBase != null)
                            if(propBase.HasEvolution(evoID))
                                return true;
                   }
                }
                return false;
            }
            public void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_const_char(m_BehaviorId);
                dtaBehaviorDefinition def = GetUpdateBehaviorDef(m_BehaviorName);
                if (def != null)
                {
                    foreach (dtaBehaviorVar var in m_Vars)
                    {
                        if(var.m_Type == "keyframe")
                        {
                            dtaPtxKeyframeProp keyprop = m_Keyframes.GetKeyframeProp((UInt32)var.m_KeyframeProp.m_PropID);
                            if (keyprop != null)
                                var.m_KeyframeProp = keyprop;
                        }
                        var.WriteToByteBuffer(buff);
                    }
                }
            }
            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_BehaviorName = buff.Read_const_char();
                m_BehaviorId = buff.Read_const_char();

                dtaBehaviorDefinition def = GetUpdateBehaviorDef(m_BehaviorName);
                m_Definition = def;
                if (def != null)
                {
                    int idx = 0;
                    m_Vars = new ArrayList();
                    foreach (dtaBehaviorVar var in def.m_Vars)
                    {
                        dtaBehaviorVar newvar = new dtaBehaviorVar();
                        newvar = var;
                        newvar.LoadFromByteBuffer(buff);
                        newvar.m_Definition = def;
                        newvar.m_Name = def.m_Names[idx] as String;
                        
                        m_Vars.Add(newvar);
                        idx++;
                        if (newvar.m_Type == "keyframe")
                            m_Keyframes.m_KeyframeProps.Add(newvar.m_KeyframeProp);
                    }
                }
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing dtaPtxBehaviorList
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaPtxBehaviorList
        {
            public dtaPtxRuleBehavior m_PtxRule = null;
            public List<dtaPtxBehavior> m_Behaviors = new List<dtaPtxBehavior>();
            public int GetBehaviorIndex(String name)
            {
                for (int i = 0; i < m_Behaviors.Count; i++)
                    if (m_Behaviors[i].m_BehaviorName == name)
                        return i;
                return -1;
            }

            public void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_s32(m_Behaviors.Count);
                for (int i = 0; i < m_Behaviors.Count; i++)
                    m_Behaviors[i].WriteToByteBuffer(buff);
            }
            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_Behaviors.Clear();
                int cnt = buff.Read_s32();
                for (int i = 0; i < cnt; i++)
                {
                    dtaPtxBehavior behavior = new dtaPtxBehavior();
                    behavior.m_PtxRule = m_PtxRule;
                    behavior.LoadFromByteBuffer(buff);
                    m_Behaviors.Add(behavior);
                }
            }

        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing ptxBiasLink 
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaPropBiasLink
        {
            public String m_Name;
            public ArrayList m_Props =new ArrayList();
            public ArrayList m_PropIDs = new ArrayList();

            public bool ContainsID(int id)
            {
                foreach (dtaPtxKeyframeProp keyprop in m_Props)
                {
                    if (keyprop == null) continue;
                    if (keyprop.m_PropID == id)
                        return true;
                }
                return false;
            }

            public void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_const_char(m_Name);

                m_PropIDs.Clear();
                foreach (dtaPtxKeyframeProp keyprop in m_Props)
                    if (keyprop != null)
                        m_PropIDs.Add(keyprop.m_PropID);
                
                buff.Write_s32(m_PropIDs.Count);
                for(int i=0;i<m_PropIDs.Count;i++)
                {
                    buff.Write_s32((int)m_PropIDs[i]);
                }
            }
            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_PropIDs.Clear();
                m_Name = buff.Read_const_char();
                int cnt = buff.Read_s32();
                for (int i = 0; i < cnt; i++)
                {
                    int id = buff.Read_s32();
                    m_PropIDs.Add(id);
                }
            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing the ptxRule
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaPtxRule : IRageXmlSerializable
        {
            string m_Path = "";
            int m_ID = -1;
            public float m_Version = 0.0f;
            public dtaPtxKeyframePropList m_PropList;

            public bool m_Physical = false;
           
            public string m_Name = "";
            public string m_ClassName = "";
            public dtaPtxRenderState m_RenderState = new dtaPtxRenderState();

            public string m_SpawnEffectA;
			public bool m_SpawnEffectAInheritsLife;
			public bool m_SpawnEffectATracksPos;
			public bool m_SpawnEffectATracksDir;
			public bool m_SpawnEffectATracksNegDir;
            public float m_SpawnEffectATime = 0.0f;
            public dtaEffectOverridable m_SpawnEffectAOverridables = new dtaEffectOverridable();

            public string m_SpawnEffectB;
			public bool m_SpawnEffectBInheritsLife;
			public bool m_SpawnEffectBTracksPos;
			public bool m_SpawnEffectBTracksDir;
			public bool m_SpawnEffectBTracksNegDir;
            public float m_SpawnEffectBTime = 1.0f;
            public dtaEffectOverridable m_SpawnEffectBOverridables = new dtaEffectOverridable();

            public ArrayList m_BiasLinks = new ArrayList();

			public int m_TexFrameIdMin;
			public int m_TexFrameIdMax;
 //           public float m_PhysicalRange;
 //           public byte m_PercentPhysical;
 //           public byte m_PercentKill;
 //           public bool m_OnCollideKill;
 //           public bool m_ShowPhysics;
 //           public float m_StopVel;
 //           public bool m_AllowPositionOverride;
 //           public bool m_AllowColorOverride;
 //           public bool m_RestrictNoiseX;
 //           public bool m_RestrictNoiseY;
 //           public bool m_RestrictNoiseZ;

            public virtual void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_const_char(m_Name);
                buff.Write_const_char(m_ClassName);
                buff.Write_float(m_Version);
                m_RenderState.WriteToByteBuffer(buff);

				buff.Write_const_char(m_SpawnEffectA);
				buff.Write_float(m_SpawnEffectATime);
				buff.Write_bool(m_SpawnEffectAInheritsLife);
				buff.Write_bool(m_SpawnEffectATracksPos);
				buff.Write_bool(m_SpawnEffectATracksDir);
				buff.Write_bool(m_SpawnEffectATracksNegDir);
                m_SpawnEffectAOverridables.WriteToByteBuffer(buff);

				buff.Write_const_char(m_SpawnEffectB);
				buff.Write_float(m_SpawnEffectBTime);
				buff.Write_bool(m_SpawnEffectBInheritsLife);
				buff.Write_bool(m_SpawnEffectBTracksPos);
				buff.Write_bool(m_SpawnEffectBTracksDir);
				buff.Write_bool(m_SpawnEffectBTracksNegDir);
                m_SpawnEffectBOverridables.WriteToByteBuffer(buff);

				buff.Write_s32(m_TexFrameIdMin);
				buff.Write_s32(m_TexFrameIdMax);
 //               buff.Write_float(m_PhysicalRange);
 //               buff.Write_bool(m_OnCollideKill);
 //               buff.Write_bool(m_ShowPhysics);
 //               buff.Write_float(m_StopVel);
 //               buff.Write_u8(m_PercentPhysical);
 //               buff.Write_u8(m_PercentKill);
 //               buff.Write_bool(m_AllowPositionOverride);
 //               buff.Write_bool(m_AllowColorOverride);
 //               buff.Write_bool(m_RestrictNoiseX);
 //               buff.Write_bool(m_RestrictNoiseY);
 //               buff.Write_bool(m_RestrictNoiseZ);

            }
            public virtual void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                buff.SetReadPos(8);
                m_Name      = buff.Read_const_char();
                m_ClassName = buff.Read_const_char();
                m_Version   = buff.Read_float();

                m_RenderState.LoadFromByteBuffer(buff);

				m_SpawnEffectA = buff.Read_const_char();
				m_SpawnEffectATime = buff.Read_float();
				m_SpawnEffectAInheritsLife = buff.Read_bool();
				m_SpawnEffectATracksPos = buff.Read_bool();
				m_SpawnEffectATracksDir = buff.Read_bool();
				m_SpawnEffectATracksNegDir = buff.Read_bool();
                m_SpawnEffectAOverridables.LoadFromByteBuffer(buff);

				m_SpawnEffectB = buff.Read_const_char();
				m_SpawnEffectBTime = buff.Read_float();
				m_SpawnEffectBInheritsLife = buff.Read_bool();
				m_SpawnEffectBTracksPos = buff.Read_bool();
				m_SpawnEffectBTracksDir = buff.Read_bool();
				m_SpawnEffectBTracksNegDir = buff.Read_bool();
                m_SpawnEffectBOverridables.LoadFromByteBuffer(buff);

				m_TexFrameIdMin = buff.Read_s32();
				m_TexFrameIdMax = buff.Read_s32();
//                m_PhysicalRange = buff.Read_float();
//                m_OnCollideKill = buff.Read_bool();
//                m_ShowPhysics = buff.Read_bool();
//                m_StopVel = buff.Read_float();
//                m_PercentPhysical = buff.Read_u8();
//                m_PercentKill = buff.Read_u8();
//                m_AllowPositionOverride = buff.Read_bool();
//                m_AllowColorOverride = buff.Read_bool();
//                m_RestrictNoiseX = buff.Read_bool();
//                m_RestrictNoiseY = buff.Read_bool();
//                m_RestrictNoiseZ = buff.Read_bool();
            }
            
            public virtual void LoadFromXml(XmlNode root)
            {
                FindXmlStringValue(root, "Path", ref m_Path);
                FindXmlIntValue(root, "ID", ref m_ID);
                FindXmlStringValue(root, "Name", ref m_Name);
                FindXmlStringValue(root, "ClassName", ref m_ClassName);

                LoadXmlStruct(root, "RenderState", m_RenderState);
            }
            public virtual void SaveToXml(XmlDocument doc, XmlNode root)
            {

                SaveXmlStringValue(doc, root, "Path", m_Path);
                SaveXmlIntValue(doc, root, "ID", m_ID);
                SaveXmlStringValue(doc, root, "Name", m_Name);
                SaveXmlStringValue(doc, root, "ClassName", m_ClassName);

                SaveXmlStruct(doc, root, "RenderState", m_RenderState);
            }
        };

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing the ptxRuleBehavior
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaPtxRuleBehavior : dtaPtxRule, IRageXmlSerializable
        {
            public dtaPtxShader m_Shader = new dtaPtxShader();
            public ArrayList m_Drawables = new ArrayList();
            public dtaPtxBehaviorList m_BehaviorList;
            public string m_DrawType;
            public byte m_SortType;

            public override void WriteToByteBuffer(tcpByteBuffer buff)
            {
                base.WriteToByteBuffer(buff);
                buff.Write_const_char(m_DrawType);
                buff.Write_u8(m_SortType);
                m_Shader.WriteToByteBuffer(buff);
                m_BehaviorList.WriteToByteBuffer(buff);

                //Bias Links
                buff.Write_s32(m_BiasLinks.Count);
                for (int i = 0; i < m_BiasLinks.Count; i++)
                {
                    (m_BiasLinks[i] as dtaPropBiasLink).WriteToByteBuffer(buff);
                }
            }
            public override void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                base.LoadFromByteBuffer(buff);
                m_DrawType = buff.Read_const_char();
                m_SortType = buff.Read_u8();

                //Shaderstuff
                m_Shader.LoadFromByteBuffer(buff);

                //Drawable list
                if (m_DrawType == "mesh")
                {
                    int cnt = (int)buff.Read_u32();
                    m_Drawables.Clear();
                    for (int i = 0; i < cnt; i++)
                        m_Drawables.Add(buff.Read_const_char());
                }

                m_PropList = new dtaPtxKeyframePropList();
                m_BehaviorList = new dtaPtxBehaviorList();
                m_BehaviorList.m_PtxRule = this;
                m_BehaviorList.LoadFromByteBuffer(buff);

                //Bias Linking
                m_BiasLinks.Clear();
                int count = buff.Read_s32();
                for (int i = 0; i < count; i++)
                {
                    dtaPropBiasLink link = new dtaPropBiasLink();
                    link.LoadFromByteBuffer(buff);
                    link.m_Props.Clear();
                    foreach (int id in link.m_PropIDs)
                    {
                        link.m_Props.Add(GetKeyframePropFromID(id));
                    }
                    m_BiasLinks.Add(link);
                }
            }

            private dtaPtxKeyframeProp GetKeyframePropFromID(int id)
            {
                foreach (dtaPtxBehavior behav in m_BehaviorList.m_Behaviors)
                {
                    foreach (dtaBehaviorVar var in behav.m_Vars)
                    {
                        if (var.m_Type == "keyframe" && var.m_KeyframeProp.m_PropID == id)
                        {
                            return var.m_KeyframeProp;
                        }
                    }
                }
                return null;
            }

        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing the ptxSprite
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaPtxSprite : dtaPtxRule, IRageXmlSerializable
        {
            public bool m_DrawSorted = false;
            public bool m_SortRandom;

            public dtaPtxShader m_Shader = new dtaPtxShader();

            public override void WriteToByteBuffer(tcpByteBuffer buff)
            {
                base.WriteToByteBuffer(buff);
                buff.Write_bool(m_DrawSorted);
                buff.Write_bool(m_SortRandom);

                m_Shader.WriteToByteBuffer(buff);
                m_PropList.WriteToByteBuffer(buff);

                //Bias Links
                buff.Write_s32(m_BiasLinks.Count);
                for(int i=0;i<m_BiasLinks.Count;i++)
                {
                    (m_BiasLinks[i] as dtaPropBiasLink).WriteToByteBuffer(buff);
                }
            }

            public override void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                base.LoadFromByteBuffer(buff);
                m_DrawSorted = buff.Read_bool();
                m_SortRandom = buff.Read_bool();
                                
                //Shaderstuff
                m_Shader.LoadFromByteBuffer(buff);

                m_PropList = new dtaPtxKeyframePropList();
                m_PropList.LoadFromByteBuffer(buff);

                //Bias Linking
                m_BiasLinks.Clear();
                int count = buff.Read_s32();
                for (int i = 0; i < count; i++)
                {
                    dtaPropBiasLink link = new dtaPropBiasLink();
                    link.LoadFromByteBuffer(buff);
                    link.m_Props.Clear();
                    foreach (int id in link.m_PropIDs)
                    {
                        link.m_Props.Add(GetKeyframePropFromID(id));
                    }
                    m_BiasLinks.Add(link);
                }

            }

            private dtaPtxKeyframeProp GetKeyframePropFromID(int id)
            {
                foreach (dtaPtxKeyframeProp prop in m_PropList.m_KeyframeProps)
                {
                    if (prop.m_PropID == id)
                        return prop;
                }
                return null;
            }


            public override void LoadFromXml(XmlNode root)
            {
                base.LoadFromXml(root);
                FindXmlBoolValue(root, "DrawSorted", ref m_DrawSorted);
            }

            public override void SaveToXml(XmlDocument doc, XmlNode root)
            {
                base.SaveToXml(doc, root);
                SaveXmlBoolValue(doc, root, "DrawSorted", m_DrawSorted);
            }

        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing the ptxModel
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaPtxModel : dtaPtxRule, IRageXmlSerializable
        {
            //<array name="m_Drawables" type="atArray">
            //<struct type="rage::ptxDrawable"/>
            //</array>
            //Vector3 m_Rotation;
            //Vector3 m_RotationVar;
            //Vector2 m_RotationSpeedVar;
            public bool m_DrawSorted = false;
            public bool m_InitRotateToEffectMatrix;
            public bool m_ProportionalSize = true;
            public bool m_UseRandomColor = false;
	        public bool m_WorldSpaceDampening = true;
            public bool m_WorldSpaceAcceleration = true;
            public bool m_UseEffectWeight;
            public bool m_UseEmitterWeight;
            public bool m_ZoomAcceleration;


            public ArrayList m_Drawables = new ArrayList();

            //struct name= "m_SpawnEffectOnBirth" type="rage::ptxTriggerEvent"/>
            //<struct name= "m_SpawnEffectOnDeath" type="rage::ptxTriggerEvent"/>
            
            //<struct name= "m_Color" type="rage::ptxKeyframe"/>
			//<struct name= "m_SizeMin" type="rage::ptxKeyframe"/>
			//<struct name= "m_SizeMax" type="rage::ptxKeyframe"/>
			//<struct name= "m_Acceleration" type="rage::ptxKeyframe"/>
			//<struct name= "m_Dampening" type="rage::ptxKeyframe"/>
			//<struct name= "m_MatrixWeight" type="rage::ptxKeyframe"/>
			//<struct name= "m_InitialThetaMin" type="rage::ptxKeyframe"/>
			//<struct name= "m_InitialThetaMax" type="rage::ptxKeyframe"/>
			//<struct name= "m_ThetaMin" type="rage::ptxKeyframe"/>
			//<struct name= "m_ThetaMax" type="rage::ptxKeyframe"/>
			//<struct name= "m_InitialRotationMin" type="rage::ptxKeyframe"/>
			//<struct name= "m_InitialRotationMax" type="rage::ptxKeyframe"/>
			//<struct name= "m_RotationMin" type="rage::ptxKeyframe"/>
			//<struct name= "m_RotationMax" type="rage::ptxKeyframe"/>
			//<struct name= "m_InitRotationSpeed" type="rage::ptxKeyframe"/>

            public override void WriteToByteBuffer(tcpByteBuffer buff)
            {
                base.WriteToByteBuffer(buff);
                buff.Write_bool(m_DrawSorted);
                buff.Write_bool(m_InitRotateToEffectMatrix);
                buff.Write_bool(m_ProportionalSize);
                buff.Write_bool(m_UseRandomColor);
                buff.Write_bool(m_WorldSpaceDampening);
                buff.Write_bool(m_WorldSpaceAcceleration);
                buff.Write_bool(m_UseEffectWeight);
                buff.Write_bool(m_UseEmitterWeight);
                buff.Write_bool(m_ZoomAcceleration);
                
                //Proplist
                m_PropList.WriteToByteBuffer(buff);
                
                //Bias Links
                buff.Write_s32(m_BiasLinks.Count);
                for(int i=0;i<m_BiasLinks.Count;i++)
                {
                    (m_BiasLinks[i] as dtaPropBiasLink).WriteToByteBuffer(buff);
                }
            }

            public override void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                base.LoadFromByteBuffer(buff);
                m_DrawSorted                = buff.Read_bool();
                m_InitRotateToEffectMatrix  = buff.Read_bool();
                m_ProportionalSize          = buff.Read_bool();
                m_UseRandomColor            = buff.Read_bool();
	            m_WorldSpaceDampening       = buff.Read_bool();
                m_WorldSpaceAcceleration    = buff.Read_bool();
                m_UseEffectWeight           = buff.Read_bool();
                m_UseEmitterWeight          = buff.Read_bool();
                m_ZoomAcceleration          = buff.Read_bool();

                //Drawable list
                int cnt = (int)buff.Read_u32();
                m_Drawables.Clear();
                for(int i=0;i<cnt;i++)
                    m_Drawables.Add(buff.Read_const_char());

                //Spawn triggers

                //PropList
                m_PropList = new dtaPtxKeyframePropList();
                m_PropList.LoadFromByteBuffer(buff);

                //Bias Linking
                m_BiasLinks.Clear();
                int count = buff.Read_s32();
                for (int i = 0; i < count; i++)
                {
                    dtaPropBiasLink link = new dtaPropBiasLink();
                    link.LoadFromByteBuffer(buff);
                    link.m_Props.Clear();
                    foreach (int id in link.m_PropIDs)
                    {
                        link.m_Props.Add(GetKeyframePropFromID(id));
                    }
                    m_BiasLinks.Add(link);
                }
            }

            private dtaPtxKeyframeProp GetKeyframePropFromID(int id)
            {
                foreach (dtaPtxKeyframeProp prop in m_PropList.m_KeyframeProps)
                {
                    if (prop.m_PropID == id)
                        return prop;
                }
                return null;
            }

            public override void LoadFromXml(XmlNode root)
            {
                base.LoadFromXml(root);
                FindXmlBoolValue(root, "InitRotateToEffectMatrix", ref m_InitRotateToEffectMatrix);
                FindXmlBoolValue(root, "ProportionalSize", ref m_ProportionalSize);
            }

            public override void SaveToXml(XmlDocument doc, XmlNode root)
            {
                base.SaveToXml(doc, root);
                SaveXmlBoolValue(doc, root, "InitRotateToEffectMatrix", m_InitRotateToEffectMatrix);
                SaveXmlBoolValue(doc, root, "ProportionalSize", m_ProportionalSize);
            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing Shader Variable
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class ptxShaderVar : IRageXmlSerializable
        {
            public string m_Type = "";
            public string m_VarName = "";
            public float m_UIMin = 0.0f;
            public float m_UIMax = 0.0f;
            public float m_UIStep = 0.0f;

            public virtual string GetTypeName()
            {
                return "none";
            }

            public virtual void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_const_char(GetTypeName());
            }
            public virtual void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                String flag = buff.Read_const_char();
                Debug.Assert(flag == "uidata");
                m_VarName   = buff.Read_const_char();
                m_Type      = buff.Read_const_char();
                m_UIMin     = buff.Read_float();
                m_UIMax     = buff.Read_float();
                m_UIStep    = buff.Read_float();
            }

            public virtual void LoadFromXml(XmlNode root)
            {

            }
            public virtual void SaveToXml(XmlDocument doc, XmlNode root)
            {

            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class ptxShaderVar_Texture : ptxShaderVar, IRageXmlSerializable
        {
            public string m_TextureName = "";
            public string m_TexturePath = "";
            public override string GetTypeName()
            {
                return "texture";
            }
            public string FormatTextureName(String name)
            {
                int idx = name.IndexOf("pack:/");
                if (idx >= 0 )
                {
                    name = name.Remove(idx, 6);
                }
                return name;
            }

            public override void WriteToByteBuffer(tcpByteBuffer buff)
            {
                base.WriteToByteBuffer(buff);
                buff.Write_const_char(m_TexturePath);
                buff.Write_const_char(m_TextureName);
            }
            public override void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                base.LoadFromByteBuffer(buff);

                m_Type = buff.Read_const_char();
                Debug.Assert(m_Type == GetTypeName(), String.Format("Shader var '{0}' type is '{1}', expecting '{2}'", m_VarName, m_Type, GetTypeName()));
                m_TextureName = buff.Read_const_char();
                m_TextureName = FormatTextureName(m_TextureName);
            }
            public override void LoadFromXml(XmlNode root)
            {
                base.LoadFromXml(root);

            }
            public override void SaveToXml(XmlDocument doc, XmlNode root)
            {
                base.SaveToXml(doc, root);
            }

        };

        public class ptxShaderVar_Float : ptxShaderVar, IRageXmlSerializable
        {
            public float  m_Float= 0.0f;
            public override string GetTypeName()
            {
                return "float";
            }

            public override void WriteToByteBuffer(tcpByteBuffer buff)
            {
                base.WriteToByteBuffer(buff);
                buff.Write_float(m_Float);
                buff.Write_float(0.0f); // game always writes out 16 bytes and also expects to read 4 floats
                buff.Write_float(0.0f);
                buff.Write_float(0.0f);
            }
            public override void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                base.LoadFromByteBuffer(buff);

                m_Type = buff.Read_const_char();
                Debug.Assert(m_Type == GetTypeName(), String.Format("Shader var '{0}' type is '{1}', expecting '{2}'", m_VarName, m_Type, GetTypeName()));
                m_Float = buff.Read_float();
                buff.Read_float(); // game always writes out 16 bytes and also expects to read 4 floats
                buff.Read_float();
                buff.Read_float();
            }
            public override void LoadFromXml(XmlNode root)
            {
                base.LoadFromXml(root);

            }
            public override void SaveToXml(XmlDocument doc, XmlNode root)
            {
                base.SaveToXml(doc, root);
            }

        };
        public class ptxShaderVar_Float2 : ptxShaderVar, IRageXmlSerializable
        {
            public float m_FloatX = 0.0f;
            public float m_FloatY = 0.0f;
            public override string GetTypeName()
            {
                return "float2";
            }

            public override void WriteToByteBuffer(tcpByteBuffer buff)
            {
                base.WriteToByteBuffer(buff);
                buff.Write_float(m_FloatX);
                buff.Write_float(m_FloatY);
                buff.Write_float(0.0f); // game always writes out 16 bytes and also expects to read 4 floats
                buff.Write_float(0.0f);
            }
            public override void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                base.LoadFromByteBuffer(buff);

                m_Type = buff.Read_const_char();
                Debug.Assert(m_Type == GetTypeName(), String.Format("Shader var '{0}' type is '{1}', expecting '{2}'", m_VarName, m_Type, GetTypeName()));
                m_FloatX = buff.Read_float();
                m_FloatY = buff.Read_float();
                buff.Read_float();  // game always writes out 16 bytes and also expects to read 4 floats
                buff.Read_float();
            }
            public override void LoadFromXml(XmlNode root)
            {
                base.LoadFromXml(root);

            }
            public override void SaveToXml(XmlDocument doc, XmlNode root)
            {
                base.SaveToXml(doc, root);
            }

        };
        public class ptxShaderVar_Float3 : ptxShaderVar, IRageXmlSerializable
        {
            public float m_FloatX = 0.0f;
            public float m_FloatY = 0.0f;
            public float m_FloatZ = 0.0f;
            public override string GetTypeName()
            {
                return "float3";
            }

            public override void WriteToByteBuffer(tcpByteBuffer buff)
            {
                base.WriteToByteBuffer(buff);
                buff.Write_float(m_FloatX);
                buff.Write_float(m_FloatY);
                buff.Write_float(m_FloatZ);
                buff.Write_float(0.0f); // game always writes out 16 bytes and also expects to read 4 floats
            }
            public override void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                base.LoadFromByteBuffer(buff);

                m_Type = buff.Read_const_char();
                Debug.Assert(m_Type == GetTypeName(), String.Format("Shader var '{0}' type is '{1}', expecting '{2}'", m_VarName, m_Type, GetTypeName()));
                m_FloatX = buff.Read_float();
                m_FloatY = buff.Read_float();
                m_FloatZ = buff.Read_float();
                buff.Read_float(); // game always writes out 16 bytes and also expects to read 4 floats
            }
            public override void LoadFromXml(XmlNode root)
            {
                base.LoadFromXml(root);

            }
            public override void SaveToXml(XmlDocument doc, XmlNode root)
            {
                base.SaveToXml(doc, root);
            }

        };
        public class ptxShaderVar_Float4 : ptxShaderVar, IRageXmlSerializable
        {
            public float m_FloatX = 0.0f;
            public float m_FloatY = 0.0f;
            public float m_FloatZ = 0.0f;
            public float m_FloatW = 0.0f;
            public override string GetTypeName()
            {
                return "float4";
            }

            public override void WriteToByteBuffer(tcpByteBuffer buff)
            {
                base.WriteToByteBuffer(buff);
                buff.Write_float(m_FloatX);
                buff.Write_float(m_FloatY);
                buff.Write_float(m_FloatZ);
                buff.Write_float(m_FloatW);
            }
            public override void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                base.LoadFromByteBuffer(buff);

                m_Type = buff.Read_const_char();
                Debug.Assert(m_Type == GetTypeName(), String.Format("Shader var '{0}' type is '{1}', expecting '{2}'", m_VarName, m_Type, GetTypeName()));
                m_FloatX = buff.Read_float();
                m_FloatY = buff.Read_float();
                m_FloatZ = buff.Read_float();
                m_FloatW = buff.Read_float();
            }
            public override void LoadFromXml(XmlNode root)
            {
                base.LoadFromXml(root);

            }
            public override void SaveToXml(XmlDocument doc, XmlNode root)
            {
                base.SaveToXml(doc, root);
            }

        };

        public class ptxShaderVar_Keyframe : ptxShaderVar, IRageXmlSerializable
        {
			public string m_RuleName;
			public int m_RuleType;
			public uint m_PropertyId;

            public override string GetTypeName()
            {
                return "keyframe";
            }

            public override void WriteToByteBuffer(tcpByteBuffer buff)
            {
                base.WriteToByteBuffer(buff);

				// these should not be required
				buff.Write_const_char(m_RuleName);
				buff.Write_s32(m_RuleType);
				buff.Write_u32(m_PropertyId);
            }

            public override void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                base.LoadFromByteBuffer(buff);
                m_Type = buff.Read_const_char();
                Debug.Assert(m_Type == GetTypeName(), String.Format("Shader var '{0}' type is '{1}', expecting '{2}'", m_VarName, m_Type, GetTypeName()));

				m_RuleName = buff.Read_const_char();
				m_RuleType = buff.Read_s32();
				m_PropertyId = buff.Read_u32();
            }
        };

        public class dtaPtxShaderTechnique
        {
            public int m_TechID;
            public String m_Name;
            public override string ToString()
            {
                return m_Name;
            }

            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_TechID    = buff.Read_s32();
                m_Name = buff.Read_const_char();
            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Class representing the ptxShader
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaPtxShader : IRageXmlSerializable
        {
            public string m_ShaderName = "";
            public ArrayList m_ShaderVars = new ArrayList();
//            public ArrayList m_Techniques = new ArrayList();
//            public String m_SelectedTechnique = "draw";
			public bool m_isLit = true;
			public bool m_isSoft = true;
			public bool m_isScreenSpace = false;
			public bool m_isRefract = false;
			public bool m_isNormalSpec = false;
			public int m_diffuseMode = 0;
			public int m_projMode = 0;

            public virtual void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_const_char(m_ShaderName);
                buff.Write_u8((byte)m_ShaderVars.Count);

                foreach(ptxShaderVar var in m_ShaderVars)
                    var.WriteToByteBuffer(buff);

 //               buff.Write_const_char(m_SelectedTechnique);

				// apply technique desc rules
				bool updateView = false;
				if (m_isRefract)
				{
					// can't have lit or non rgb diffuse mode
					m_isLit = false;
					m_diffuseMode = 4;	// RGB
					m_projMode = 0;
					updateView = true;
				}

				if (m_isScreenSpace)
				{
					// can't be soft 
					m_isSoft = false;
					m_projMode = 0;
					updateView = true;
				}

				if (m_isNormalSpec)
				{
					m_isLit = true;
					m_projMode = 0;
					updateView = true;
				}

				if (updateView)
				{
					main.m_MainForm.m_PtxRuleWindow.UpdateTechniqueDescView(m_diffuseMode, m_projMode, m_isLit, m_isSoft, m_isScreenSpace, m_isRefract, m_isNormalSpec);
				}

				// technique desc
				buff.Write_s32(m_diffuseMode);
				buff.Write_s32(m_projMode);
				buff.Write_bool(m_isLit);
				buff.Write_bool(m_isSoft);
				buff.Write_bool(m_isScreenSpace);
				buff.Write_bool(m_isRefract);
				buff.Write_bool(m_isNormalSpec);
            }
            public virtual void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_ShaderName = buff.Read_const_char();
                
                m_ShaderVars.Clear();
                int cnt = (int)buff.Read_u8();

                for(int i=0;i<cnt;i++)
                {
                    int bp = buff.GetReadPos();
                    ptxShaderVar var = null; // new ptxShaderVar();  //Just get rid of the compile error
                    string flag = buff.Read_const_char();
                    Debug.Assert(flag =="uidata");
                    string name = buff.Read_const_char();
                    string type = buff.Read_const_char();

                    switch(type)
                    {
                        case "texture":
                            var = new ptxShaderVar_Texture(); break;
                        case "float":
                            var = new ptxShaderVar_Float(); break;
                        case "float2":
                            var = new ptxShaderVar_Float2(); break;
                        case "float3":
                            var = new ptxShaderVar_Float3(); break;
                        case "float4":
                            var = new ptxShaderVar_Float4(); break;
                        case "keyframe":
                            var = new ptxShaderVar_Keyframe(); break;
                        default:
                            Debug.Assert(false);
                            break;
                    }

                    if (var != null)
                    {
                        buff.SetReadPos(bp);
                        var.LoadFromByteBuffer(buff);
                        m_ShaderVars.Add(var);
                    }
                }

//               m_Techniques.Clear();
//              cnt = buff.Read_s32();
//              for(int i=0;i<cnt;i++)
//                {
//                    dtaPtxShaderTechnique tech = new dtaPtxShaderTechnique();
//                    tech.LoadFromByteBuffer(buff);
//                    m_Techniques.Add(tech);
//                }
//                m_SelectedTechnique = buff.Read_const_char();

				m_diffuseMode = buff.Read_s32();
				m_projMode = buff.Read_s32();
				m_isLit = buff.Read_bool();
				m_isSoft = buff.Read_bool();
				m_isScreenSpace = buff.Read_bool();
				m_isRefract = buff.Read_bool();
				m_isNormalSpec = buff.Read_bool();
            }

            public virtual void LoadFromXml(XmlNode root)
            {

            }
            public virtual void SaveToXml(XmlDocument doc, XmlNode root)
            {

            }
        };

        public class dtaProfilingGlobal
        {
            public int m_TotalPoints = 0;
            public int m_TotalActiveInstances = 0;
            public int m_TotalCulledInstances = 0;
            public float m_TotalCPUUpdateTime = 0.0f;
            public float m_TotalCPUDrawTime =0.0f;
            public float m_TotalGPUDrawTime = 0.0f;
            
            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_TotalPoints           = buff.Read_s32();
                m_TotalActiveInstances  = buff.Read_s32();
                m_TotalCulledInstances  = buff.Read_s32();
                m_TotalCPUUpdateTime    = buff.Read_float();
                m_TotalCPUDrawTime      = buff.Read_float();
                m_TotalGPUDrawTime      = buff.Read_float();
            }
        };

        public class dtaProfilingEffect
        {
            public int m_EffectTotalPoints = 0;
            public int m_EffectActiveInstances = 0;
            public int m_EffectCulledInstances = 0;
            public float m_EffectCPUUpdateTime = 0.0f;
            public float m_EffectCPUUDrawTime = 0.0f;
            public string m_EffectName;
            
            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_EffectName            = buff.Read_const_char();
                m_EffectTotalPoints     = buff.Read_s32();
                m_EffectActiveInstances = buff.Read_s32();
                m_EffectCulledInstances = buff.Read_s32();
                m_EffectCPUUpdateTime   = buff.Read_float();
                m_EffectCPUUDrawTime    = buff.Read_float();
            }
        };
        public class dtaProfilingEmitter
		{
			public string m_EmitterName;
            public float m_EmitterCPUUpdateTime = 0.0f;
            public float m_EmitterCPUDrawTime = 0.0f;
			public float m_EmitterCPUSortTime = 0.0f;
			public int m_EmitterTotalPoints = 0;
			public int m_EmitterActiveInstances = 0;

            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_EmitterName           = buff.Read_const_char();
                m_EmitterCPUUpdateTime  = buff.Read_float();
				m_EmitterCPUDrawTime	= buff.Read_float();
				m_EmitterCPUSortTime	= buff.Read_float();
				m_EmitterTotalPoints	= buff.Read_s32();
				m_EmitterActiveInstances = buff.Read_s32();
            }
        };
        public class dtaProfilingPtx
        {
//            public int m_PtxTotalPoints = 0;
//            public float m_PtxCPUUpdateTime = 0.0f;
//            public float m_PtxCPUDrawTime = 0.0f;
//            public float m_PtxCPUSortTime = 0.0f;
            public string m_PtxRuleName;

            public int m_PtxNumBehaviors;
            public List<string> m_PtxBehaviorNames = new List<string>();
            public List<float> m_PtxBehaviorCPUUpdateTimes = new List<float>();

            public int GetBehaviorIndexFromUIName(String name)
            {
                for (int i = 0; i < m_PtxBehaviorNames.Count; i++)
                    if (m_PtxBehaviorNames[i] == name)
                        return i;
                return -1;
            }
            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_PtxRuleName       = buff.Read_const_char();
//                m_PtxTotalPoints    = buff.Read_s32();
//                m_PtxCPUUpdateTime  = buff.Read_float();
//                m_PtxCPUDrawTime    = buff.Read_float();
//                m_PtxCPUSortTime    = buff.Read_float();
                m_PtxBehaviorNames.Clear();
                m_PtxBehaviorCPUUpdateTimes.Clear();

                // If we have a ptxRuleBehavior here, there will be per-behavior
                // profile info coming over too.
                dtaPtxRule doo = GetPtxRule(m_PtxRuleName);
                dtaPtxRuleBehavior dob = doo as dtaPtxRuleBehavior;
                if( dob != null )
                {
                    m_PtxNumBehaviors = buff.Read_s32();
                    for( int i = 0; i < m_PtxNumBehaviors; i++ )
                    {
                        // Get UiName.
                        string name = buff.Read_const_char();
                        m_PtxBehaviorNames.Add(name);
                        // Get timing.
                        float timing = buff.Read_float();
                        m_PtxBehaviorCPUUpdateTimes.Add(timing);
                    }
                }
            }
        };
        
        public class dtaEvolutionProperty
        {
            public KeyframeSpec m_Keyframe = new KeyframeSpec();
            
            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_Keyframe.DefnName = buff.Read_const_char();
                buff.Read_u32();
				m_Keyframe.PropertyId = buff.Read_u32();
            }
        };

        public class dtaEvolutionPropList
        {
            public List<dtaEvolutionProperty> m_Props = new List<dtaEvolutionProperty>();

            public bool HasProperty(int id)
            {
                foreach (dtaEvolutionProperty prop in m_Props)
					if (prop.m_Keyframe.PropertyId == (uint)id)
                        return true;
                return false;
            }

            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_Props.Clear();
                int cnt = buff.Read_s32();
                for(int i=0;i<cnt;i++)
                {
                    m_Props.Add(new dtaEvolutionProperty());
                    dtaEvolutionProperty prop = m_Props[m_Props.Count - 1];
                    prop.LoadFromByteBuffer(buff);
                }
            }
        };

        public class dtaEvolution
        {
            public String m_Name = "";
            public float  m_TestEvolution = 0.0f;
            public bool m_ProcOverride = false;
            public List<dtaEvolutionPropList> m_PropList = new List<dtaEvolutionPropList>();

            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_Name = buff.Read_const_char();
                m_ProcOverride = buff.Read_bool();
                m_TestEvolution = buff.Read_float();
            }
            public void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_const_char(m_Name);
                buff.Write_bool(m_ProcOverride);
                buff.Write_float(m_TestEvolution);
            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class ptxEvoPropChain
        {
            public KeyframeSpec m_Keyframe;
            public int m_EvoIndex = -1;

            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_EvoIndex = buff.Read_s32();
                m_Keyframe = new KeyframeSpec();
                m_Keyframe.DefnName = buff.Read_const_char();

				m_Keyframe.RuleName = buff.Read_const_char();
				m_Keyframe.RuleType = buff.Read_s32();
				m_Keyframe.PropertyId = buff.Read_u32();
				m_Keyframe.EventIdx = buff.Read_s32();
				m_Keyframe.EvoIdx = m_EvoIndex;
            }
            public void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_s32(m_EvoIndex);
                buff.Write_const_char(m_Keyframe.DefnName);

				buff.Write_const_char(m_Keyframe.RuleName);
				buff.Write_s32(m_Keyframe.RuleType);
				buff.Write_u32(m_Keyframe.PropertyId);
				buff.Write_s32(m_Keyframe.EventIdx);
            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class ptxEvoPropBase
        {
            public UInt32 m_PropHashID = 0;
            public UInt32 m_Blendmode = 0;
            public List<ptxEvoPropChain> m_PropChain = new List<ptxEvoPropChain>();
            public bool HasEvolution(int evoID)
            {
                for (int i = 0; i < m_PropChain.Count; i++)
                    if (m_PropChain[i].m_EvoIndex == evoID)
                        return true;
                return false;
            }
            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_PropChain.Clear();
                m_PropHashID = buff.Read_u32();
                m_Blendmode = buff.Read_u32();
                UInt32 chaincnt = buff.Read_u32();
                for (int i = 0; i < chaincnt; i++)
                {
                    ptxEvoPropChain evoChain = new ptxEvoPropChain();
                    evoChain.LoadFromByteBuffer(buff);
                    m_PropChain.Add(evoChain);
                }
            }
            public void WriteToByteBuffer(tcpByteBuffer buff)
            {
                buff.Write_u32(m_PropHashID);
                buff.Write_u32(m_Blendmode);
                buff.Write_u32((uint)m_PropChain.Count);
                for (int i = 0; i < m_PropChain.Count; i++)
                    m_PropChain[i].WriteToByteBuffer(buff);
            }
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaEvolutionList
        {
            public List<dtaEvolution> m_Evolutions = new List<dtaEvolution>();
            public List<ptxEvoPropBase> m_Props = new List<ptxEvoPropBase>();
            public ptxEvoPropBase GetPropBaseFromID(UInt32 propID)
            {
                if (m_Props.Count == 0)
                    return null;
                foreach (ptxEvoPropBase baseProp in m_Props)
                {
                    if (baseProp.m_PropHashID == propID)
                        return baseProp;
                }
                return null;
            }

            public ptxEvoPropBase GetPropBase(dtaPtxKeyframeProp prop)
            {
                if (prop == null) return null;
                return GetPropBaseFromID((UInt32)prop.m_PropID);
            }

            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_Evolutions.Clear();
                m_Props.Clear();

                if (!buff.Read_bool())
                    return;
                UInt32 evocnt = buff.Read_u32();
                for (int e = 0; e < evocnt; e++)
                {
                    dtaEvolution evo = new dtaEvolution();
                    evo.LoadFromByteBuffer(buff);
                    m_Evolutions.Add(evo);
                }
                UInt32 basepropcnt = buff.Read_u32();
                for (int i = 0; i < basepropcnt; i++)
                {
                    ptxEvoPropBase propBase = new ptxEvoPropBase();
                    propBase.LoadFromByteBuffer(buff);
                    m_Props.Add(propBase);
                }
            }
            public void WriteToByteBuffer(tcpByteBuffer buff)
            {
                if (m_Evolutions.Count == 0)
                {
                    buff.Write_bool(false);
                    return;
                }
                buff.Write_bool(true);
                buff.Write_u32((uint)m_Evolutions.Count);
                for (int e = 0; e < m_Evolutions.Count; e++)
                    m_Evolutions[e].WriteToByteBuffer(buff);

                buff.Write_u32((uint)m_Props.Count);
                for(int i=0;i<m_Props.Count;i++)
                    m_Props[i].WriteToByteBuffer(buff);
            }
        };

        public class dtaEvolutionGroup
        {
            public ArrayList m_Evolutions = new ArrayList();
            public ArrayList m_PropBase = new ArrayList();
            //TODO: move to per property
            public List<List<int>> m_EvoBlendModeList = new List<List<int>>();

            public bool ContainsEvoData(int proplist)
            {
                foreach (dtaEvolution evo in m_Evolutions)
                {
                    if (evo.m_PropList.Count > proplist)
                    {
                        dtaEvolutionPropList pl = evo.m_PropList[proplist] as dtaEvolutionPropList;
                        if (pl.m_Props.Count > 0)
                            return true;
                    }
                }
                return false;
            }
            public bool ContainsPtxRuleEvoData()
            {
                foreach(dtaEvolution evo in m_Evolutions)
                {
                    if (evo.m_PropList.Count > 3)
                        if (evo.m_PropList[3].m_Props.Count > 0)
                            return true;
                }
                return false;
            }
            public bool ContainsEffectRuleEvoData()
            {
                foreach (dtaEvolution evo in m_Evolutions)
                {
                    if (evo.m_PropList.Count > 0)
                    {
                        dtaEvolutionPropList pl = evo.m_PropList[0] as dtaEvolutionPropList;
                        if(pl.m_Props.Count>0)
                            return true;
                    }
                }
                return false;
            }
            public bool ContainsEmitRuleEvoData()
            {
                foreach (dtaEvolution evo in m_Evolutions)
                {
                    if (evo.m_PropList.Count > 0)
                    {
                        dtaEvolutionPropList pl = evo.m_PropList[0] as dtaEvolutionPropList;
                        if (pl.m_Props.Count > 0)
                            return true;
                    }
                }
                return false;
            }
//            public bool ContainsVDomainEvoData()
//            {
//                foreach (dtaEvolution evo in m_Evolutions)
//                {
//                    if (evo.m_PropList.Count >= 2)
//                    {
//                        dtaEvolutionPropList pl = evo.m_PropList[2] as dtaEvolutionPropList;
//                        if (pl.m_Props.Count > 0)
//                            return true;
//                    }
//                }
//                return false;
//            }
//            public bool ContainsEDomainEvoData()
//            {
//                foreach (dtaEvolution evo in m_Evolutions)
//                {
//                    if (evo.m_PropList.Count >= 1)
//                    {
//                        dtaEvolutionPropList pl = evo.m_PropList[1] as dtaEvolutionPropList;
//                        if (pl.m_Props.Count > 0)
//                            return true;
//                    }
//                }
//                return false;
//            }
//			public bool ContainsADomainEvoData()
//			{
//				foreach (dtaEvolution evo in m_Evolutions)
//				{
//					if (evo.m_PropList.Count >= 1)
//					{
//						dtaEvolutionPropList pl = evo.m_PropList[1] as dtaEvolutionPropList;
//						if (pl.m_Props.Count > 0)
//							return true;
//					}
//				}
//				return false;
//			}

            public void LoadFromByteBuffer(tcpByteBuffer buff)
            {
                m_Evolutions.Clear();
                m_EvoBlendModeList.Clear();

                if (!buff.Read_bool())
                    return;

                int listcnt = buff.Read_s32();
                for (int list = 0; list < listcnt; list++)
                {
                    List<int> blends = new List<int>();
                    int cnt = buff.Read_s32();
                    for (int i = 0; i < cnt; i++)
                        blends.Add((int)buff.Read_u8());
                    m_EvoBlendModeList.Add(blends);
                }
                int evocnt = buff.Read_s32();
                for (int i = 0; i < evocnt; i++)
                {
                    m_Evolutions.Add(new dtaEvolution());
                    dtaEvolution evo = m_Evolutions[i] as dtaEvolution;
                    evo.LoadFromByteBuffer(buff);
                }
            }
            public void WriteToByteBuffer(tcpByteBuffer buff)
            {
                if (m_Evolutions.Count == 0)
                {
                    buff.Write_bool(false);
                    return;
                }
                buff.Write_bool(true);
                buff.Write_s32(m_EvoBlendModeList.Count);
                for (int list = 0; list < m_EvoBlendModeList.Count; list++)
                {
                    buff.Write_s32(m_EvoBlendModeList[list].Count);
                    for (int i = 0; i < m_EvoBlendModeList[list].Count; i++)
                        buff.Write_u8(System.Convert.ToByte(m_EvoBlendModeList[list][i]));
                }
                buff.Write_s32(m_Evolutions.Count);
                foreach (dtaEvolution evo in m_Evolutions)
                    evo.WriteToByteBuffer(buff);
            }
        };

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class guiDataGridTagInfo
        {
            public dtaEvolutionList m_EvoList = null;
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class guiPropEventData
        {
            public bool InfoChanged = false;
        };
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class dtaGameSpecificDataBool
        {
            public String m_Name = "no name";
            public bool m_State = false;
        };
        /**** GUI-DATA **********************************************/
        public float m_InterfaceVersion = 3.40f; //If you change this you have to also change the rmptfx code (rage\suite\src\rmptfx\ptxmanager.h [RMPTFX_VERSION])
        public float m_SysVersion = 0.0f;
        public dtaProfilingGlobal   m_ProfileGlobal = new dtaProfilingGlobal();
        public dtaProfilingEffect   m_ProfileEffect = new dtaProfilingEffect();
        public dtaProfilingEmitter  m_ProfileEmitter = new dtaProfilingEmitter();
        public dtaProfilingPtx      m_ProfilePtx = new dtaProfilingPtx();

		public ArrayList m_DrawListNames = new ArrayList();
		public ArrayList m_DataVolumeTypeNames = new ArrayList();
        public ArrayList m_EffectRuleList = new ArrayList();
        public ArrayList m_EmitRuleList = new ArrayList();
        public ArrayList m_PtxRuleList = new ArrayList();
        public ArrayList m_DomainTypes = new ArrayList();
        public ArrayList m_EmitterDomainTypes = new ArrayList();
		public ArrayList m_VelocityDomainTypes = new ArrayList();
		public ArrayList m_AttractorDomainTypes = new ArrayList();
        public ArrayList m_BlendSetList = new ArrayList();
        public ArrayList m_ShaderList = new ArrayList();
        public ArrayList m_GameSpecificDataEffectRuleBoolList = new ArrayList();
        public ArrayList m_UpdateBehaviorDefList = new ArrayList();
        public ArrayList m_DrawTypeList = new ArrayList();

        public ArrayList m_ActiveEffects = new ArrayList();
        public ArrayList m_ActiveEmitters = new ArrayList();
        public ArrayList m_ActivePtxRules = new ArrayList();
    }
}
