namespace ParticleEditor
{
    partial class EventEffect
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label18 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.m_ComboEffectRule = new System.Windows.Forms.ComboBox();
            this.m_EvtStartTime = new ParticleEditor.ptxSlider();
            this.m_CheckEvtActive = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.m_DataGridEmitKeyframes = new System.Windows.Forms.DataGridView();
            this.m_ComboEmitShape = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.m_CheckEmitWorld = new System.Windows.Forms.CheckBox();
            this.m_CheckEmitShow = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_SliderTriggerCap = new ParticleEditor.ptxSlider();
            this.m_SliderRotationX = new ParticleEditor.ptxSlider();
            this.m_SliderRotationY = new ParticleEditor.ptxSlider();
            this.m_SliderRotationZ = new ParticleEditor.ptxSlider();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.m_CheckShowOrientation = new System.Windows.Forms.CheckBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDEvolution1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDEvolution2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDEvolution3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDEvolution4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDEvolution5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDEvolutionLOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Invis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridEmitKeyframes)).BeginInit();
            this.SuspendLayout();
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(227, 6);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 13);
            this.label18.TabIndex = 81;
            this.label18.Text = "Start Time:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(227, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 90;
            this.label1.Text = "EffectRule:";
            // 
            // m_ComboEffectRule
            // 
            this.m_ComboEffectRule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboEffectRule.FormattingEnabled = true;
            this.m_ComboEffectRule.Location = new System.Drawing.Point(230, 73);
            this.m_ComboEffectRule.Name = "m_ComboEffectRule";
            this.m_ComboEffectRule.Size = new System.Drawing.Size(191, 21);
            this.m_ComboEffectRule.Sorted = true;
            this.m_ComboEffectRule.TabIndex = 89;
            this.m_ComboEffectRule.SelectionChangeCommitted += new System.EventHandler(this.m_ComboEffectRule_SelectionChangeCommitted);
            this.m_ComboEffectRule.DropDown += new System.EventHandler(this.m_ComboEffectRule_DropDown);
            // 
            // m_EvtStartTime
            // 
            this.m_EvtStartTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_EvtStartTime.Decimals = 3;
            this.m_EvtStartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_EvtStartTime.Increment = 0.001F;
            this.m_EvtStartTime.Location = new System.Drawing.Point(362, 4);
            this.m_EvtStartTime.MaxValue = 10000F;
            this.m_EvtStartTime.MinValue = 0F;
            this.m_EvtStartTime.Multiline = true;
            this.m_EvtStartTime.Name = "m_EvtStartTime";
            this.m_EvtStartTime.SetValue = 0F;
            this.m_EvtStartTime.Size = new System.Drawing.Size(59, 20);
            this.m_EvtStartTime.TabIndex = 86;
            this.m_EvtStartTime.Text = "0.000";
            this.m_EvtStartTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_EvtStartTime.Value = 0F;
            this.m_EvtStartTime.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.Slider_OnValueChanged);
            // 
            // m_CheckEvtActive
            // 
            this.m_CheckEvtActive.AutoSize = true;
            this.m_CheckEvtActive.Checked = true;
            this.m_CheckEvtActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckEvtActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_CheckEvtActive.Location = new System.Drawing.Point(5, 5);
            this.m_CheckEvtActive.Name = "m_CheckEvtActive";
            this.m_CheckEvtActive.Size = new System.Drawing.Size(87, 17);
            this.m_CheckEvtActive.TabIndex = 78;
            this.m_CheckEvtActive.Text = "Event Active";
            this.m_CheckEvtActive.UseVisualStyleBackColor = true;
            this.m_CheckEvtActive.Click += new System.EventHandler(this.CheckBox_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.m_DataGridEmitKeyframes);
            this.panel2.Controls.Add(this.m_ComboEmitShape);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.m_CheckEmitWorld);
            this.panel2.Controls.Add(this.m_CheckEmitShow);
            this.panel2.Location = new System.Drawing.Point(3, 33);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(218, 132);
            this.panel2.TabIndex = 91;
            // 
            // m_DataGridEmitKeyframes
            // 
            this.m_DataGridEmitKeyframes.AllowUserToAddRows = false;
            this.m_DataGridEmitKeyframes.AllowUserToDeleteRows = false;
            this.m_DataGridEmitKeyframes.AllowUserToResizeColumns = false;
            this.m_DataGridEmitKeyframes.AllowUserToResizeRows = false;
            this.m_DataGridEmitKeyframes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.m_DataGridEmitKeyframes.BackgroundColor = System.Drawing.SystemColors.Window;
            this.m_DataGridEmitKeyframes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.m_DataGridEmitKeyframes.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.m_DataGridEmitKeyframes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridEmitKeyframes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.m_DataGridEmitKeyframes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.EDEvolution1,
            this.EDEvolution2,
            this.EDEvolution3,
            this.EDEvolution4,
            this.EDEvolution5,
            this.EDEvolutionLOD,
            this.Invis});
            this.m_DataGridEmitKeyframes.GridColor = System.Drawing.SystemColors.Window;
            this.m_DataGridEmitKeyframes.Location = new System.Drawing.Point(6, 58);
            this.m_DataGridEmitKeyframes.MultiSelect = false;
            this.m_DataGridEmitKeyframes.Name = "m_DataGridEmitKeyframes";
            this.m_DataGridEmitKeyframes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridEmitKeyframes.RowHeadersVisible = false;
            this.m_DataGridEmitKeyframes.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.m_DataGridEmitKeyframes.RowTemplate.Height = 15;
            this.m_DataGridEmitKeyframes.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridEmitKeyframes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_DataGridEmitKeyframes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.m_DataGridEmitKeyframes.ShowCellToolTips = false;
            this.m_DataGridEmitKeyframes.Size = new System.Drawing.Size(207, 69);
            this.m_DataGridEmitKeyframes.TabIndex = 45;
            this.m_DataGridEmitKeyframes.DoubleClick += new System.EventHandler(this.ShowKeyframe);
            this.m_DataGridEmitKeyframes.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SelectOnRightMouseButton_CellMouseDown);
            this.m_DataGridEmitKeyframes.SelectionChanged += new System.EventHandler(this.DataGridAutoSelectValidCell);
            // 
            // m_ComboEmitShape
            // 
            this.m_ComboEmitShape.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboEmitShape.FormattingEnabled = true;
            this.m_ComboEmitShape.Items.AddRange(new object[] {
            "Sphere",
            "Box",
            "Cylinder"});
            this.m_ComboEmitShape.Location = new System.Drawing.Point(94, 31);
            this.m_ComboEmitShape.Name = "m_ComboEmitShape";
            this.m_ComboEmitShape.Size = new System.Drawing.Size(119, 21);
            this.m_ComboEmitShape.TabIndex = 41;
            this.m_ComboEmitShape.SelectionChangeCommitted += new System.EventHandler(this.m_ComboEmitShape_SelectionChangeCommitted);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 40;
            this.label7.Text = "Domain Shape:";
            // 
            // m_CheckEmitWorld
            // 
            this.m_CheckEmitWorld.AutoSize = true;
            this.m_CheckEmitWorld.Checked = true;
            this.m_CheckEmitWorld.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckEmitWorld.Location = new System.Drawing.Point(105, 9);
            this.m_CheckEmitWorld.Name = "m_CheckEmitWorld";
            this.m_CheckEmitWorld.Size = new System.Drawing.Size(88, 17);
            this.m_CheckEmitWorld.TabIndex = 38;
            this.m_CheckEmitWorld.Text = "World Space";
            this.m_CheckEmitWorld.UseVisualStyleBackColor = true;
            this.m_CheckEmitWorld.Click += new System.EventHandler(this.CheckBox_Click);
            // 
            // m_CheckEmitShow
            // 
            this.m_CheckEmitShow.AutoSize = true;
            this.m_CheckEmitShow.Checked = true;
            this.m_CheckEmitShow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckEmitShow.Location = new System.Drawing.Point(9, 9);
            this.m_CheckEmitShow.Name = "m_CheckEmitShow";
            this.m_CheckEmitShow.Size = new System.Drawing.Size(92, 17);
            this.m_CheckEmitShow.TabIndex = 37;
            this.m_CheckEmitShow.Text = "Show Domain";
            this.m_CheckEmitShow.UseVisualStyleBackColor = true;
            this.m_CheckEmitShow.Click += new System.EventHandler(this.CheckBox_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 39;
            this.label4.Text = "Trigger Domain:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(227, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 92;
            this.label2.Text = "Trigger Cap:";
            // 
            // m_SliderTriggerCap
            // 
            this.m_SliderTriggerCap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SliderTriggerCap.Decimals = 0;
            this.m_SliderTriggerCap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderTriggerCap.Increment = 1F;
            this.m_SliderTriggerCap.Location = new System.Drawing.Point(362, 30);
            this.m_SliderTriggerCap.MaxValue = 10000F;
            this.m_SliderTriggerCap.MinValue = -1F;
            this.m_SliderTriggerCap.Multiline = true;
            this.m_SliderTriggerCap.Name = "m_SliderTriggerCap";
            this.m_SliderTriggerCap.SetValue = 0F;
            this.m_SliderTriggerCap.Size = new System.Drawing.Size(59, 20);
            this.m_SliderTriggerCap.TabIndex = 93;
            this.m_SliderTriggerCap.Text = "0";
            this.m_SliderTriggerCap.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_SliderTriggerCap.Value = 0F;
            this.m_SliderTriggerCap.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.Slider_OnValueChanged);
            // 
            // m_SliderRotationX
            // 
            this.m_SliderRotationX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SliderRotationX.Decimals = 2;
            this.m_SliderRotationX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderRotationX.Increment = 0.001F;
            this.m_SliderRotationX.Location = new System.Drawing.Point(246, 126);
            this.m_SliderRotationX.MaxValue = 10000F;
            this.m_SliderRotationX.MinValue = -10000F;
            this.m_SliderRotationX.Multiline = true;
            this.m_SliderRotationX.Name = "m_SliderRotationX";
            this.m_SliderRotationX.SetValue = 0F;
            this.m_SliderRotationX.Size = new System.Drawing.Size(43, 20);
            this.m_SliderRotationX.TabIndex = 94;
            this.m_SliderRotationX.Text = "0.00";
            this.m_SliderRotationX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_SliderRotationX.Value = 0F;
            this.m_SliderRotationX.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.Slider_OnValueChanged);
            // 
            // m_SliderRotationY
            // 
            this.m_SliderRotationY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SliderRotationY.Decimals = 2;
            this.m_SliderRotationY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderRotationY.Increment = 0.001F;
            this.m_SliderRotationY.Location = new System.Drawing.Point(311, 126);
            this.m_SliderRotationY.MaxValue = 10000F;
            this.m_SliderRotationY.MinValue = -10000F;
            this.m_SliderRotationY.Multiline = true;
            this.m_SliderRotationY.Name = "m_SliderRotationY";
            this.m_SliderRotationY.SetValue = 0F;
            this.m_SliderRotationY.Size = new System.Drawing.Size(43, 20);
            this.m_SliderRotationY.TabIndex = 95;
            this.m_SliderRotationY.Text = "0.00";
            this.m_SliderRotationY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_SliderRotationY.Value = 0F;
            this.m_SliderRotationY.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.Slider_OnValueChanged);
            // 
            // m_SliderRotationZ
            // 
            this.m_SliderRotationZ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SliderRotationZ.Decimals = 2;
            this.m_SliderRotationZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderRotationZ.Increment = 0.001F;
            this.m_SliderRotationZ.Location = new System.Drawing.Point(378, 126);
            this.m_SliderRotationZ.MaxValue = 10000F;
            this.m_SliderRotationZ.MinValue = -10000F;
            this.m_SliderRotationZ.Multiline = true;
            this.m_SliderRotationZ.Name = "m_SliderRotationZ";
            this.m_SliderRotationZ.SetValue = 0F;
            this.m_SliderRotationZ.Size = new System.Drawing.Size(43, 20);
            this.m_SliderRotationZ.TabIndex = 96;
            this.m_SliderRotationZ.Text = "0.00";
            this.m_SliderRotationZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_SliderRotationZ.Value = 0F;
            this.m_SliderRotationZ.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.Slider_OnValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(227, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 97;
            this.label3.Text = "Orientation:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(227, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 98;
            this.label5.Text = "X:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(292, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 13);
            this.label6.TabIndex = 99;
            this.label6.Text = "Y:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(359, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 13);
            this.label8.TabIndex = 100;
            this.label8.Text = "Z:";
            // 
            // m_CheckShowOrientation
            // 
            this.m_CheckShowOrientation.AutoSize = true;
            this.m_CheckShowOrientation.Checked = true;
            this.m_CheckShowOrientation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckShowOrientation.Location = new System.Drawing.Point(317, 109);
            this.m_CheckShowOrientation.Name = "m_CheckShowOrientation";
            this.m_CheckShowOrientation.Size = new System.Drawing.Size(107, 17);
            this.m_CheckShowOrientation.TabIndex = 46;
            this.m_CheckShowOrientation.Text = "Show Orientation";
            this.m_CheckShowOrientation.UseVisualStyleBackColor = true;
            this.m_CheckShowOrientation.Click += new System.EventHandler(this.CheckBox_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.dataGridViewTextBoxColumn1.HeaderText = "Property";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.Width = 5;
            // 
            // EDEvolution1
            // 
            this.EDEvolution1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Red;
            this.EDEvolution1.DefaultCellStyle = dataGridViewCellStyle1;
            this.EDEvolution1.HeaderText = "EDEvolution1";
            this.EDEvolution1.Name = "EDEvolution1";
            this.EDEvolution1.ReadOnly = true;
            this.EDEvolution1.Width = 5;
            // 
            // EDEvolution2
            // 
            this.EDEvolution2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Blue;
            this.EDEvolution2.DefaultCellStyle = dataGridViewCellStyle2;
            this.EDEvolution2.HeaderText = "EDEvolution2";
            this.EDEvolution2.Name = "EDEvolution2";
            this.EDEvolution2.ReadOnly = true;
            this.EDEvolution2.Width = 5;
            // 
            // EDEvolution3
            // 
            this.EDEvolution3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Green;
            this.EDEvolution3.DefaultCellStyle = dataGridViewCellStyle3;
            this.EDEvolution3.HeaderText = "EDEvolution3";
            this.EDEvolution3.Name = "EDEvolution3";
            this.EDEvolution3.ReadOnly = true;
            this.EDEvolution3.Width = 5;
            // 
            // EDEvolution4
            // 
            this.EDEvolution4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Purple;
            this.EDEvolution4.DefaultCellStyle = dataGridViewCellStyle4;
            this.EDEvolution4.HeaderText = "EDEvolution4";
            this.EDEvolution4.Name = "EDEvolution4";
            this.EDEvolution4.ReadOnly = true;
            this.EDEvolution4.Width = 5;
            // 
            // EDEvolution5
            // 
            this.EDEvolution5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.EDEvolution5.HeaderText = "EDEvolution5";
            this.EDEvolution5.MinimumWidth = 2;
            this.EDEvolution5.Name = "EDEvolution5";
            this.EDEvolution5.ReadOnly = true;
            this.EDEvolution5.Width = 2;
            // 
            // EDEvolutionLOD
            // 
            this.EDEvolutionLOD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.EDEvolutionLOD.HeaderText = "EDEvolutionLOD";
            this.EDEvolutionLOD.Name = "EDEvolutionLOD";
            this.EDEvolutionLOD.ReadOnly = true;
            this.EDEvolutionLOD.Width = 5;
            // 
            // Invis
            // 
            this.Invis.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Invis.HeaderText = "Invis";
            this.Invis.Name = "Invis";
            this.Invis.ReadOnly = true;
            // 
            // EventEffect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_CheckShowOrientation);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_SliderRotationZ);
            this.Controls.Add(this.m_SliderRotationY);
            this.Controls.Add(this.m_SliderRotationX);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_SliderTriggerCap);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_ComboEffectRule);
            this.Controls.Add(this.m_EvtStartTime);
            this.Controls.Add(this.m_CheckEvtActive);
            this.Name = "EventEffect";
            this.Size = new System.Drawing.Size(433, 169);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridEmitKeyframes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox m_ComboEffectRule;
        private System.Windows.Forms.CheckBox m_CheckEvtActive;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView m_DataGridEmitKeyframes;
        private System.Windows.Forms.ComboBox m_ComboEmitShape;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox m_CheckEmitWorld;
        private System.Windows.Forms.CheckBox m_CheckEmitShow;
        private System.Windows.Forms.Label label2;
        private ptxSlider m_SliderTriggerCap;
        private ptxSlider m_SliderRotationX;
        private ptxSlider m_SliderRotationY;
        private ptxSlider m_SliderRotationZ;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox m_CheckShowOrientation;
        public ptxSlider m_EvtStartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDEvolution1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDEvolution2;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDEvolution3;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDEvolution4;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDEvolution5;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDEvolutionLOD;
        private System.Windows.Forms.DataGridViewTextBoxColumn Invis;
    }
}
