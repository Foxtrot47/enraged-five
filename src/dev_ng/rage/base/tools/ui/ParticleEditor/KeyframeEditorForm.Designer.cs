namespace ParticleEditor
{
    partial class KeyframeEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KeyframeEditorForm));
            this.keyframeEditor1 = new ragKeyframeEditorControl.KeyframeEditor();
            this.SuspendLayout();
            // 
            // keyframeEditor1
            // 
            this.keyframeEditor1.AsColorGrad = false;
            this.keyframeEditor1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keyframeEditor1.Location = new System.Drawing.Point(0, 0);
            this.keyframeEditor1.Name = "keyframeEditor1";
            this.keyframeEditor1.Size = new System.Drawing.Size(741, 373);
            this.keyframeEditor1.TabIndex = 0;
            // 
            // KeyframeEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 373);
            this.Controls.Add(this.keyframeEditor1);
            this.Name = "KeyframeEditorForm";
            this.Text = "KeyframeEditorForm";
            this.ResumeLayout(false);

        }

        #endregion

        private ragKeyframeEditorControl.KeyframeEditor keyframeEditor1;

 
    }
}