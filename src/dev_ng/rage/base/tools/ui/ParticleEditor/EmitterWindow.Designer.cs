namespace ParticleEditor
{
    partial class EmitterWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_PanelHeader = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.m_PanelGroup = new System.Windows.Forms.Panel();
            this.m_PanelSection = new System.Windows.Forms.Panel();
            this.m_ContextMenuKeyProp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_ContextMenuEDKeyProp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_ContextMenuVDKeyProp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_ContextMenuADKeyProp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_LabelHeader = new System.Windows.Forms.Label();
            this.ptxSubSection2 = new ParticleEditor.ptxSubSection();
            this.m_DataGridKeyframeList = new System.Windows.Forms.DataGridView();
            this.m_ColumnTextRuleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evolution5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvolutionLOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvoBlend = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.InvisK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ptxSubSection1 = new ParticleEditor.ptxSubSection();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.m_DataGridEmitKeyframes = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDEvolution1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDEvolution2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDEvolution3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDEvolution4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDEvolution5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDEvolutionLOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDEvoBlend = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Invis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_ComboEmitShape = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.m_CheckEmitWorld = new System.Windows.Forms.CheckBox();
            this.m_CheckEmitShow = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.m_CheckVelCreationRel = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.m_DataGridVelKeyframes = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VDEvolution1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VDEvolution2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VDEvolution3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VDEvolution4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VDEvolution5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VDEvolutionLOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VDEvoBlend = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.InvisVD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_ComboVelShape = new System.Windows.Forms.ComboBox();
            this.m_CheckVelPointRel = new System.Windows.Forms.CheckBox();
            this.m_CheckVelWorld = new System.Windows.Forms.CheckBox();
            this.m_CheckVelShow = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.m_CheckAttrTargetRel = new System.Windows.Forms.CheckBox();
            this.m_CheckAttrCreationRel = new System.Windows.Forms.CheckBox();
            this.m_ButtonAttrEnableToggle = new System.Windows.Forms.Button();
            this.m_LabelAttrShape = new System.Windows.Forms.Label();
            this.m_DataGridAttrKeyframes = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_ComboAttrShape = new System.Windows.Forms.ComboBox();
            this.m_CheckAttrWorld = new System.Windows.Forms.CheckBox();
            this.m_CheckAttrShow = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.m_SubEvolutions = new ParticleEditor.ptxSubSection();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.m_CheckOneShot = new System.Windows.Forms.CheckBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.m_PanelHeader.SuspendLayout();
            this.m_PanelGroup.SuspendLayout();
            this.m_PanelSection.SuspendLayout();
            this.panel1.SuspendLayout();
            this.ptxSubSection2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridKeyframeList)).BeginInit();
            this.ptxSubSection1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridEmitKeyframes)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridVelKeyframes)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridAttrKeyframes)).BeginInit();
            this.m_SubEvolutions.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_PanelHeader
            // 
            this.m_PanelHeader.Controls.Add(this.label1);
            this.m_PanelHeader.Location = new System.Drawing.Point(6, -70);
            this.m_PanelHeader.Name = "m_PanelHeader";
            this.m_PanelHeader.Size = new System.Drawing.Size(348, 30);
            this.m_PanelHeader.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 22);
            this.label1.TabIndex = 2;
            this.label1.Text = "Effect:";
            // 
            // m_PanelGroup
            // 
            this.m_PanelGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.m_PanelGroup.AutoScroll = true;
            this.m_PanelGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_PanelGroup.Controls.Add(this.m_PanelSection);
            this.m_PanelGroup.Location = new System.Drawing.Point(14, 28);
            this.m_PanelGroup.Margin = new System.Windows.Forms.Padding(0);
            this.m_PanelGroup.Name = "m_PanelGroup";
            this.m_PanelGroup.Size = new System.Drawing.Size(587, 693);
            this.m_PanelGroup.TabIndex = 5;
            // 
            // m_PanelSection
            // 
            this.m_PanelSection.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_PanelSection.Controls.Add(this.ptxSubSection2);
            this.m_PanelSection.Controls.Add(this.ptxSubSection1);
            this.m_PanelSection.Controls.Add(this.m_SubEvolutions);
            this.m_PanelSection.Location = new System.Drawing.Point(0, 0);
            this.m_PanelSection.Margin = new System.Windows.Forms.Padding(0);
            this.m_PanelSection.MaximumSize = new System.Drawing.Size(567, 0);
            this.m_PanelSection.MinimumSize = new System.Drawing.Size(567, 713);
            this.m_PanelSection.Name = "m_PanelSection";
            this.m_PanelSection.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.m_PanelSection.Size = new System.Drawing.Size(567, 713);
            this.m_PanelSection.TabIndex = 1;
            this.m_PanelSection.Paint += new System.Windows.Forms.PaintEventHandler(this.m_PanelSection_Paint);
            // 
            // m_ContextMenuKeyProp
            // 
            this.m_ContextMenuKeyProp.Name = "m_ContextMenuKeyProp";
            this.m_ContextMenuKeyProp.Size = new System.Drawing.Size(61, 4);
            this.m_ContextMenuKeyProp.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContextMenuEmitKeyProps_Opening);
            // 
            // m_ContextMenuEDKeyProp
            // 
            this.m_ContextMenuEDKeyProp.Name = "m_ContextMenuEDKeyProp";
            this.m_ContextMenuEDKeyProp.Size = new System.Drawing.Size(61, 4);
            this.m_ContextMenuEDKeyProp.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContextMenuEDKeyProp_Opening);
            // 
            // m_ContextMenuVDKeyProp
            // 
            this.m_ContextMenuVDKeyProp.Name = "m_ContextMenuVDKeyProp";
            this.m_ContextMenuVDKeyProp.Size = new System.Drawing.Size(61, 4);
            this.m_ContextMenuVDKeyProp.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContextMenuVDKeyProp_Opening);
            // 
            // m_ContextMenuADKeyProp
            // 
            this.m_ContextMenuADKeyProp.Name = "m_ContextMenuVDKeyProp";
            this.m_ContextMenuADKeyProp.Size = new System.Drawing.Size(61, 4);
            this.m_ContextMenuADKeyProp.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContextMenuADKeyProp_Opening);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.m_LabelHeader);
            this.panel1.Location = new System.Drawing.Point(13, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(326, 26);
            this.panel1.TabIndex = 7;
            // 
            // m_LabelHeader
            // 
            this.m_LabelHeader.AutoSize = true;
            this.m_LabelHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelHeader.Location = new System.Drawing.Point(-4, 4);
            this.m_LabelHeader.Name = "m_LabelHeader";
            this.m_LabelHeader.Size = new System.Drawing.Size(71, 22);
            this.m_LabelHeader.TabIndex = 2;
            this.m_LabelHeader.Text = "Emitter:";
            // 
            // ptxSubSection2
            // 
            this.ptxSubSection2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptxSubSection2.Controls.Add(this.m_DataGridKeyframeList);
            this.ptxSubSection2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ptxSubSection2.Expanded = true;
            this.ptxSubSection2.HeaderText = "";
            this.ptxSubSection2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ptxSubSection2.Location = new System.Drawing.Point(0, 527);
            this.ptxSubSection2.Margin = new System.Windows.Forms.Padding(0);
            this.ptxSubSection2.Name = "ptxSubSection2";
            this.ptxSubSection2.SectionName = "Emitter Keyframing";
            this.ptxSubSection2.Size = new System.Drawing.Size(567, 232);
            this.ptxSubSection2.TabIndex = 2;
            // 
            // m_DataGridKeyframeList
            // 
            this.m_DataGridKeyframeList.AllowUserToAddRows = false;
            this.m_DataGridKeyframeList.AllowUserToDeleteRows = false;
            this.m_DataGridKeyframeList.AllowUserToResizeColumns = false;
            this.m_DataGridKeyframeList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AliceBlue;
            this.m_DataGridKeyframeList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.m_DataGridKeyframeList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.m_DataGridKeyframeList.BackgroundColor = System.Drawing.SystemColors.Window;
            this.m_DataGridKeyframeList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.m_DataGridKeyframeList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.m_DataGridKeyframeList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridKeyframeList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.m_DataGridKeyframeList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.m_ColumnTextRuleName,
            this.Evolution1,
            this.Evolution2,
            this.Evolution3,
            this.Evolution4,
            this.Evolution5,
            this.EvolutionLOD,
            this.EvoBlend,
            this.InvisK});
            this.m_DataGridKeyframeList.ContextMenuStrip = this.m_ContextMenuKeyProp;
            this.m_DataGridKeyframeList.GridColor = System.Drawing.SystemColors.Window;
            this.m_DataGridKeyframeList.Location = new System.Drawing.Point(8, 34);
            this.m_DataGridKeyframeList.MultiSelect = false;
            this.m_DataGridKeyframeList.Name = "m_DataGridKeyframeList";
            this.m_DataGridKeyframeList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridKeyframeList.RowHeadersVisible = false;
            this.m_DataGridKeyframeList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_DataGridKeyframeList.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.m_DataGridKeyframeList.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.m_DataGridKeyframeList.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_DataGridKeyframeList.RowTemplate.Height = 15;
            this.m_DataGridKeyframeList.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridKeyframeList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_DataGridKeyframeList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.m_DataGridKeyframeList.ShowCellToolTips = false;
            this.m_DataGridKeyframeList.Size = new System.Drawing.Size(553, 193);
            this.m_DataGridKeyframeList.TabIndex = 23;
            this.m_DataGridKeyframeList.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SelectOnRightMouseButton_CellMouseDown);
            this.m_DataGridKeyframeList.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_DataGridKeyframeList_CellValueChanged);
            this.m_DataGridKeyframeList.CurrentCellDirtyStateChanged += new System.EventHandler(this.m_DataGridKeyframeList_CurrentCellDirtyStateChanged);
            this.m_DataGridKeyframeList.SelectionChanged += new System.EventHandler(this.DataGridAutoSelectValidCell);
            this.m_DataGridKeyframeList.DoubleClick += new System.EventHandler(this.m_DataGridKeyframeList_DoubleClick);
            // 
            // m_ColumnTextRuleName
            // 
            this.m_ColumnTextRuleName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ColumnTextRuleName.DefaultCellStyle = dataGridViewCellStyle2;
            this.m_ColumnTextRuleName.HeaderText = "Property";
            this.m_ColumnTextRuleName.Name = "m_ColumnTextRuleName";
            this.m_ColumnTextRuleName.ReadOnly = true;
            this.m_ColumnTextRuleName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_ColumnTextRuleName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.m_ColumnTextRuleName.Width = 5;
            // 
            // Evolution1
            // 
            this.Evolution1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Red;
            this.Evolution1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Evolution1.HeaderText = "Evolution1";
            this.Evolution1.Name = "Evolution1";
            this.Evolution1.ReadOnly = true;
            this.Evolution1.Width = 5;
            // 
            // Evolution2
            // 
            this.Evolution2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Blue;
            this.Evolution2.DefaultCellStyle = dataGridViewCellStyle4;
            this.Evolution2.HeaderText = "Evolution2";
            this.Evolution2.Name = "Evolution2";
            this.Evolution2.ReadOnly = true;
            this.Evolution2.Width = 5;
            // 
            // Evolution3
            // 
            this.Evolution3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Green;
            this.Evolution3.DefaultCellStyle = dataGridViewCellStyle5;
            this.Evolution3.HeaderText = "Evolution3";
            this.Evolution3.Name = "Evolution3";
            this.Evolution3.ReadOnly = true;
            this.Evolution3.Width = 5;
            // 
            // Evolution4
            // 
            this.Evolution4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Purple;
            this.Evolution4.DefaultCellStyle = dataGridViewCellStyle6;
            this.Evolution4.HeaderText = "Evolution4";
            this.Evolution4.Name = "Evolution4";
            this.Evolution4.ReadOnly = true;
            this.Evolution4.Width = 5;
            // 
            // Evolution5
            // 
            this.Evolution5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.SaddleBrown;
            this.Evolution5.DefaultCellStyle = dataGridViewCellStyle7;
            this.Evolution5.HeaderText = "Evolution5";
            this.Evolution5.MinimumWidth = 2;
            this.Evolution5.Name = "Evolution5";
            this.Evolution5.ReadOnly = true;
            this.Evolution5.Width = 2;
            // 
            // EvolutionLOD
            // 
            this.EvolutionLOD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.EvolutionLOD.HeaderText = "EvolutionLOD";
            this.EvolutionLOD.Name = "EvolutionLOD";
            this.EvolutionLOD.ReadOnly = true;
            this.EvolutionLOD.Width = 5;
            // 
            // EvoBlend
            // 
            this.EvoBlend.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EvoBlend.HeaderText = "EvoBlend";
            this.EvoBlend.Items.AddRange(new object[] {
            "Active Avg",
            "Add",
            "Max",
            "Full Avg"});
            this.EvoBlend.Name = "EvoBlend";
            this.EvoBlend.Width = 80;
            // 
            // InvisK
            // 
            this.InvisK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.InvisK.HeaderText = "InvisK";
            this.InvisK.Name = "InvisK";
            this.InvisK.ReadOnly = true;
            // 
            // ptxSubSection1
            // 
            this.ptxSubSection1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptxSubSection1.Controls.Add(this.panel2);
            this.ptxSubSection1.Controls.Add(this.panel3);
            this.ptxSubSection1.Controls.Add(this.panel4);
            this.ptxSubSection1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ptxSubSection1.Expanded = true;
            this.ptxSubSection1.HeaderText = "";
            this.ptxSubSection1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ptxSubSection1.Location = new System.Drawing.Point(0, 96);
            this.ptxSubSection1.Margin = new System.Windows.Forms.Padding(0);
            this.ptxSubSection1.Name = "ptxSubSection1";
            this.ptxSubSection1.SectionName = "Domains";
            this.ptxSubSection1.Size = new System.Drawing.Size(567, 431);
            this.ptxSubSection1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.m_DataGridEmitKeyframes);
            this.panel2.Controls.Add(this.m_ComboEmitShape);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.m_CheckEmitWorld);
            this.panel2.Controls.Add(this.m_CheckEmitShow);
            this.panel2.Location = new System.Drawing.Point(3, 28);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(558, 110);
            this.panel2.TabIndex = 38;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(93, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 46;
            this.label7.Text = "Shape";
            // 
            // m_DataGridEmitKeyframes
            // 
            this.m_DataGridEmitKeyframes.AllowUserToAddRows = false;
            this.m_DataGridEmitKeyframes.AllowUserToDeleteRows = false;
            this.m_DataGridEmitKeyframes.AllowUserToResizeColumns = false;
            this.m_DataGridEmitKeyframes.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.AliceBlue;
            this.m_DataGridEmitKeyframes.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.m_DataGridEmitKeyframes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.m_DataGridEmitKeyframes.BackgroundColor = System.Drawing.SystemColors.Window;
            this.m_DataGridEmitKeyframes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.m_DataGridEmitKeyframes.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.m_DataGridEmitKeyframes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridEmitKeyframes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.m_DataGridEmitKeyframes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.EDEvolution1,
            this.EDEvolution2,
            this.EDEvolution3,
            this.EDEvolution4,
            this.EDEvolution5,
            this.EDEvolutionLOD,
            this.EDEvoBlend,
            this.Invis});
            this.m_DataGridEmitKeyframes.ContextMenuStrip = this.m_ContextMenuEDKeyProp;
            this.m_DataGridEmitKeyframes.GridColor = System.Drawing.SystemColors.Window;
            this.m_DataGridEmitKeyframes.Location = new System.Drawing.Point(8, 34);
            this.m_DataGridEmitKeyframes.MultiSelect = false;
            this.m_DataGridEmitKeyframes.Name = "m_DataGridEmitKeyframes";
            this.m_DataGridEmitKeyframes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridEmitKeyframes.RowHeadersVisible = false;
            this.m_DataGridEmitKeyframes.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.m_DataGridEmitKeyframes.RowTemplate.Height = 15;
            this.m_DataGridEmitKeyframes.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridEmitKeyframes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_DataGridEmitKeyframes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.m_DataGridEmitKeyframes.ShowCellToolTips = false;
            this.m_DataGridEmitKeyframes.Size = new System.Drawing.Size(537, 66);
            this.m_DataGridEmitKeyframes.TabIndex = 45;
            this.m_DataGridEmitKeyframes.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SelectOnRightMouseButton_CellMouseDown);
            this.m_DataGridEmitKeyframes.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_DataGridEmitKeyframes_CellValueChanged);
            this.m_DataGridEmitKeyframes.CurrentCellDirtyStateChanged += new System.EventHandler(this.m_DataGridEmitKeyframes_CurrentCellDirtyStateChanged);
            this.m_DataGridEmitKeyframes.SelectionChanged += new System.EventHandler(this.DataGridAutoSelectValidCell);
            this.m_DataGridEmitKeyframes.DoubleClick += new System.EventHandler(this.m_DataGridEmitKeyframes_DoubleClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.dataGridViewTextBoxColumn1.HeaderText = "Property";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 5;
            // 
            // EDEvolution1
            // 
            this.EDEvolution1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Red;
            this.EDEvolution1.DefaultCellStyle = dataGridViewCellStyle10;
            this.EDEvolution1.HeaderText = "EDEvolution1";
            this.EDEvolution1.Name = "EDEvolution1";
            this.EDEvolution1.ReadOnly = true;
            this.EDEvolution1.Width = 5;
            // 
            // EDEvolution2
            // 
            this.EDEvolution2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Blue;
            this.EDEvolution2.DefaultCellStyle = dataGridViewCellStyle11;
            this.EDEvolution2.HeaderText = "EDEvolution2";
            this.EDEvolution2.Name = "EDEvolution2";
            this.EDEvolution2.ReadOnly = true;
            this.EDEvolution2.Width = 5;
            // 
            // EDEvolution3
            // 
            this.EDEvolution3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Green;
            this.EDEvolution3.DefaultCellStyle = dataGridViewCellStyle12;
            this.EDEvolution3.HeaderText = "EDEvolution3";
            this.EDEvolution3.Name = "EDEvolution3";
            this.EDEvolution3.ReadOnly = true;
            this.EDEvolution3.Width = 5;
            // 
            // EDEvolution4
            // 
            this.EDEvolution4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Purple;
            this.EDEvolution4.DefaultCellStyle = dataGridViewCellStyle13;
            this.EDEvolution4.HeaderText = "EDEvolution4";
            this.EDEvolution4.Name = "EDEvolution4";
            this.EDEvolution4.ReadOnly = true;
            this.EDEvolution4.Width = 5;
            // 
            // EDEvolution5
            // 
            this.EDEvolution5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.SaddleBrown;
            this.EDEvolution5.DefaultCellStyle = dataGridViewCellStyle14;
            this.EDEvolution5.HeaderText = "EDEvolution5";
            this.EDEvolution5.MinimumWidth = 2;
            this.EDEvolution5.Name = "EDEvolution5";
            this.EDEvolution5.ReadOnly = true;
            this.EDEvolution5.Width = 2;
            // 
            // EDEvolutionLOD
            // 
            this.EDEvolutionLOD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.EDEvolutionLOD.HeaderText = "EDEvolutionLOD";
            this.EDEvolutionLOD.Name = "EDEvolutionLOD";
            this.EDEvolutionLOD.ReadOnly = true;
            this.EDEvolutionLOD.Width = 5;
            // 
            // EDEvoBlend
            // 
            this.EDEvoBlend.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EDEvoBlend.HeaderText = "EDEvoBlend";
            this.EDEvoBlend.Items.AddRange(new object[] {
            "Active Avg",
            "Add",
            "Max",
            "Full Avg"});
            this.EDEvoBlend.Name = "EDEvoBlend";
            this.EDEvoBlend.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EDEvoBlend.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.EDEvoBlend.Width = 80;
            // 
            // Invis
            // 
            this.Invis.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Invis.HeaderText = "Invis";
            this.Invis.Name = "Invis";
            this.Invis.ReadOnly = true;
            // 
            // m_ComboEmitShape
            // 
            this.m_ComboEmitShape.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboEmitShape.FormattingEnabled = true;
            this.m_ComboEmitShape.Location = new System.Drawing.Point(137, 7);
            this.m_ComboEmitShape.Name = "m_ComboEmitShape";
            this.m_ComboEmitShape.Size = new System.Drawing.Size(92, 21);
            this.m_ComboEmitShape.TabIndex = 41;
            this.m_ComboEmitShape.SelectionChangeCommitted += new System.EventHandler(this.m_Combo_SelectionChangeCommitted);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 39;
            this.label4.Text = "Creation";
            // 
            // m_CheckEmitWorld
            // 
            this.m_CheckEmitWorld.AutoSize = true;
            this.m_CheckEmitWorld.Checked = true;
            this.m_CheckEmitWorld.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckEmitWorld.Location = new System.Drawing.Point(457, 11);
            this.m_CheckEmitWorld.Name = "m_CheckEmitWorld";
            this.m_CheckEmitWorld.Size = new System.Drawing.Size(88, 17);
            this.m_CheckEmitWorld.TabIndex = 38;
            this.m_CheckEmitWorld.Text = "World Space";
            this.m_CheckEmitWorld.UseVisualStyleBackColor = true;
            this.m_CheckEmitWorld.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_CheckEmitShow
            // 
            this.m_CheckEmitShow.AutoSize = true;
            this.m_CheckEmitShow.Checked = true;
            this.m_CheckEmitShow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckEmitShow.Location = new System.Drawing.Point(248, 9);
            this.m_CheckEmitShow.Name = "m_CheckEmitShow";
            this.m_CheckEmitShow.Size = new System.Drawing.Size(92, 17);
            this.m_CheckEmitShow.TabIndex = 37;
            this.m_CheckEmitShow.Text = "Show Domain";
            this.m_CheckEmitShow.UseVisualStyleBackColor = true;
            this.m_CheckEmitShow.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.m_CheckVelCreationRel);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.m_DataGridVelKeyframes);
            this.panel3.Controls.Add(this.m_ComboVelShape);
            this.panel3.Controls.Add(this.m_CheckVelPointRel);
            this.panel3.Controls.Add(this.m_CheckVelWorld);
            this.panel3.Controls.Add(this.m_CheckVelShow);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Location = new System.Drawing.Point(3, 144);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(558, 135);
            this.panel3.TabIndex = 39;
            // 
            // m_CheckVelCreationRel
            // 
            this.m_CheckVelCreationRel.AutoSize = true;
            this.m_CheckVelCreationRel.Checked = true;
            this.m_CheckVelCreationRel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckVelCreationRel.Location = new System.Drawing.Point(248, 34);
            this.m_CheckVelCreationRel.Name = "m_CheckVelCreationRel";
            this.m_CheckVelCreationRel.Size = new System.Drawing.Size(146, 17);
            this.m_CheckVelCreationRel.TabIndex = 53;
            this.m_CheckVelCreationRel.Text = "Creation Domain Relative";
            this.m_CheckVelCreationRel.UseVisualStyleBackColor = true;
            this.m_CheckVelCreationRel.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(93, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 52;
            this.label8.Text = "Shape";
            // 
            // m_DataGridVelKeyframes
            // 
            this.m_DataGridVelKeyframes.AllowUserToAddRows = false;
            this.m_DataGridVelKeyframes.AllowUserToDeleteRows = false;
            this.m_DataGridVelKeyframes.AllowUserToResizeColumns = false;
            this.m_DataGridVelKeyframes.AllowUserToResizeRows = false;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.AliceBlue;
            this.m_DataGridVelKeyframes.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle15;
            this.m_DataGridVelKeyframes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.m_DataGridVelKeyframes.BackgroundColor = System.Drawing.SystemColors.Window;
            this.m_DataGridVelKeyframes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.m_DataGridVelKeyframes.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.m_DataGridVelKeyframes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridVelKeyframes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.m_DataGridVelKeyframes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.VDEvolution1,
            this.VDEvolution2,
            this.VDEvolution3,
            this.VDEvolution4,
            this.VDEvolution5,
            this.VDEvolutionLOD,
            this.VDEvoBlend,
            this.InvisVD});
            this.m_DataGridVelKeyframes.ContextMenuStrip = this.m_ContextMenuVDKeyProp;
            this.m_DataGridVelKeyframes.GridColor = System.Drawing.SystemColors.Window;
            this.m_DataGridVelKeyframes.Location = new System.Drawing.Point(9, 57);
            this.m_DataGridVelKeyframes.MultiSelect = false;
            this.m_DataGridVelKeyframes.Name = "m_DataGridVelKeyframes";
            this.m_DataGridVelKeyframes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridVelKeyframes.RowHeadersVisible = false;
            this.m_DataGridVelKeyframes.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.m_DataGridVelKeyframes.RowTemplate.Height = 15;
            this.m_DataGridVelKeyframes.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridVelKeyframes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_DataGridVelKeyframes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.m_DataGridVelKeyframes.ShowCellToolTips = false;
            this.m_DataGridVelKeyframes.Size = new System.Drawing.Size(536, 68);
            this.m_DataGridVelKeyframes.TabIndex = 46;
            this.m_DataGridVelKeyframes.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SelectOnRightMouseButton_CellMouseDown);
            this.m_DataGridVelKeyframes.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_DataGridVelKeyframes_CellValueChanged);
            this.m_DataGridVelKeyframes.CurrentCellDirtyStateChanged += new System.EventHandler(this.m_DataGridVelKeyframes_CurrentCellDirtyStateChanged);
            this.m_DataGridVelKeyframes.SelectionChanged += new System.EventHandler(this.DataGridAutoSelectValidCell);
            this.m_DataGridVelKeyframes.DoubleClick += new System.EventHandler(this.m_DataGridVelKeyframes_DoubleClick);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.dataGridViewTextBoxColumn2.HeaderText = "Property";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 5;
            // 
            // VDEvolution1
            // 
            this.VDEvolution1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Red;
            this.VDEvolution1.DefaultCellStyle = dataGridViewCellStyle16;
            this.VDEvolution1.HeaderText = "VDEvolution1";
            this.VDEvolution1.Name = "VDEvolution1";
            this.VDEvolution1.ReadOnly = true;
            this.VDEvolution1.Width = 5;
            // 
            // VDEvolution2
            // 
            this.VDEvolution2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Blue;
            this.VDEvolution2.DefaultCellStyle = dataGridViewCellStyle17;
            this.VDEvolution2.HeaderText = "VDEvolution2";
            this.VDEvolution2.Name = "VDEvolution2";
            this.VDEvolution2.ReadOnly = true;
            this.VDEvolution2.Width = 5;
            // 
            // VDEvolution3
            // 
            this.VDEvolution3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.Green;
            this.VDEvolution3.DefaultCellStyle = dataGridViewCellStyle18;
            this.VDEvolution3.HeaderText = "VDEvolution3";
            this.VDEvolution3.Name = "VDEvolution3";
            this.VDEvolution3.ReadOnly = true;
            this.VDEvolution3.Width = 5;
            // 
            // VDEvolution4
            // 
            this.VDEvolution4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.Purple;
            this.VDEvolution4.DefaultCellStyle = dataGridViewCellStyle19;
            this.VDEvolution4.HeaderText = "VDEvolution4";
            this.VDEvolution4.Name = "VDEvolution4";
            this.VDEvolution4.ReadOnly = true;
            this.VDEvolution4.Width = 5;
            // 
            // VDEvolution5
            // 
            this.VDEvolution5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.SaddleBrown;
            this.VDEvolution5.DefaultCellStyle = dataGridViewCellStyle20;
            this.VDEvolution5.HeaderText = "VDEvolution5";
            this.VDEvolution5.MinimumWidth = 2;
            this.VDEvolution5.Name = "VDEvolution5";
            this.VDEvolution5.ReadOnly = true;
            this.VDEvolution5.Width = 2;
            // 
            // VDEvolutionLOD
            // 
            this.VDEvolutionLOD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.VDEvolutionLOD.HeaderText = "VDEvolutionLOD";
            this.VDEvolutionLOD.Name = "VDEvolutionLOD";
            this.VDEvolutionLOD.ReadOnly = true;
            this.VDEvolutionLOD.Width = 5;
            // 
            // VDEvoBlend
            // 
            this.VDEvoBlend.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.VDEvoBlend.HeaderText = "VDEvoBlend";
            this.VDEvoBlend.Items.AddRange(new object[] {
            "Active Avg",
            "Add",
            "Max",
            "Full Avg"});
            this.VDEvoBlend.Name = "VDEvoBlend";
            this.VDEvoBlend.Width = 80;
            // 
            // InvisVD
            // 
            this.InvisVD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.InvisVD.HeaderText = "InvisVD";
            this.InvisVD.Name = "InvisVD";
            this.InvisVD.ReadOnly = true;
            // 
            // m_ComboVelShape
            // 
            this.m_ComboVelShape.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboVelShape.FormattingEnabled = true;
            this.m_ComboVelShape.Location = new System.Drawing.Point(137, 7);
            this.m_ComboVelShape.Name = "m_ComboVelShape";
            this.m_ComboVelShape.Size = new System.Drawing.Size(92, 21);
            this.m_ComboVelShape.TabIndex = 51;
            this.m_ComboVelShape.SelectionChangeCommitted += new System.EventHandler(this.m_Combo_SelectionChangeCommitted);
            // 
            // m_CheckVelPointRel
            // 
            this.m_CheckVelPointRel.AutoSize = true;
            this.m_CheckVelPointRel.Checked = true;
            this.m_CheckVelPointRel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckVelPointRel.Location = new System.Drawing.Point(453, 34);
            this.m_CheckVelPointRel.Name = "m_CheckVelPointRel";
            this.m_CheckVelPointRel.Size = new System.Drawing.Size(92, 17);
            this.m_CheckVelPointRel.TabIndex = 50;
            this.m_CheckVelPointRel.Text = "Point Relative";
            this.m_CheckVelPointRel.UseVisualStyleBackColor = true;
            this.m_CheckVelPointRel.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_CheckVelWorld
            // 
            this.m_CheckVelWorld.AutoSize = true;
            this.m_CheckVelWorld.Checked = true;
            this.m_CheckVelWorld.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckVelWorld.Location = new System.Drawing.Point(457, 9);
            this.m_CheckVelWorld.Name = "m_CheckVelWorld";
            this.m_CheckVelWorld.Size = new System.Drawing.Size(88, 17);
            this.m_CheckVelWorld.TabIndex = 49;
            this.m_CheckVelWorld.Text = "World Space";
            this.m_CheckVelWorld.UseVisualStyleBackColor = true;
            this.m_CheckVelWorld.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_CheckVelShow
            // 
            this.m_CheckVelShow.AutoSize = true;
            this.m_CheckVelShow.Checked = true;
            this.m_CheckVelShow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckVelShow.Location = new System.Drawing.Point(249, 9);
            this.m_CheckVelShow.Name = "m_CheckVelShow";
            this.m_CheckVelShow.Size = new System.Drawing.Size(92, 17);
            this.m_CheckVelShow.TabIndex = 48;
            this.m_CheckVelShow.Text = "Show Domain";
            this.m_CheckVelShow.UseVisualStyleBackColor = true;
            this.m_CheckVelShow.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(5, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 47;
            this.label6.Text = "Target";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.m_CheckAttrTargetRel);
            this.panel4.Controls.Add(this.m_CheckAttrCreationRel);
            this.panel4.Controls.Add(this.m_ButtonAttrEnableToggle);
            this.panel4.Controls.Add(this.m_LabelAttrShape);
            this.panel4.Controls.Add(this.m_DataGridAttrKeyframes);
            this.panel4.Controls.Add(this.m_ComboAttrShape);
            this.panel4.Controls.Add(this.m_CheckAttrWorld);
            this.panel4.Controls.Add(this.m_CheckAttrShow);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Location = new System.Drawing.Point(4, 285);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(558, 136);
            this.panel4.TabIndex = 40;
            // 
            // m_CheckAttrTargetRel
            // 
            this.m_CheckAttrTargetRel.AutoSize = true;
            this.m_CheckAttrTargetRel.Checked = true;
            this.m_CheckAttrTargetRel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckAttrTargetRel.Location = new System.Drawing.Point(407, 34);
            this.m_CheckAttrTargetRel.Name = "m_CheckAttrTargetRel";
            this.m_CheckAttrTargetRel.Size = new System.Drawing.Size(138, 17);
            this.m_CheckAttrTargetRel.TabIndex = 56;
            this.m_CheckAttrTargetRel.Text = "Target Domain Relative";
            this.m_CheckAttrTargetRel.UseVisualStyleBackColor = true;
            this.m_CheckAttrTargetRel.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_CheckAttrCreationRel
            // 
            this.m_CheckAttrCreationRel.AutoSize = true;
            this.m_CheckAttrCreationRel.Checked = true;
            this.m_CheckAttrCreationRel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckAttrCreationRel.Location = new System.Drawing.Point(247, 34);
            this.m_CheckAttrCreationRel.Name = "m_CheckAttrCreationRel";
            this.m_CheckAttrCreationRel.Size = new System.Drawing.Size(146, 17);
            this.m_CheckAttrCreationRel.TabIndex = 55;
            this.m_CheckAttrCreationRel.Text = "Creation Domain Relative";
            this.m_CheckAttrCreationRel.UseVisualStyleBackColor = true;
            this.m_CheckAttrCreationRel.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_ButtonAttrEnableToggle
            // 
            this.m_ButtonAttrEnableToggle.Location = new System.Drawing.Point(9, 26);
            this.m_ButtonAttrEnableToggle.Name = "m_ButtonAttrEnableToggle";
            this.m_ButtonAttrEnableToggle.Size = new System.Drawing.Size(75, 23);
            this.m_ButtonAttrEnableToggle.TabIndex = 54;
            this.m_ButtonAttrEnableToggle.Text = "Enable";
            this.m_ButtonAttrEnableToggle.UseVisualStyleBackColor = true;
            this.m_ButtonAttrEnableToggle.Click += new System.EventHandler(this.m_ButtonAttrEnableToggle_Click);
            // 
            // m_LabelAttrShape
            // 
            this.m_LabelAttrShape.AutoSize = true;
            this.m_LabelAttrShape.Location = new System.Drawing.Point(93, 10);
            this.m_LabelAttrShape.Name = "m_LabelAttrShape";
            this.m_LabelAttrShape.Size = new System.Drawing.Size(38, 13);
            this.m_LabelAttrShape.TabIndex = 53;
            this.m_LabelAttrShape.Text = "Shape";
            // 
            // m_DataGridAttrKeyframes
            // 
            this.m_DataGridAttrKeyframes.AllowUserToAddRows = false;
            this.m_DataGridAttrKeyframes.AllowUserToDeleteRows = false;
            this.m_DataGridAttrKeyframes.AllowUserToResizeColumns = false;
            this.m_DataGridAttrKeyframes.AllowUserToResizeRows = false;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.AliceBlue;
            this.m_DataGridAttrKeyframes.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle21;
            this.m_DataGridAttrKeyframes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.m_DataGridAttrKeyframes.BackgroundColor = System.Drawing.SystemColors.Window;
            this.m_DataGridAttrKeyframes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.m_DataGridAttrKeyframes.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.m_DataGridAttrKeyframes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridAttrKeyframes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.m_DataGridAttrKeyframes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewComboBoxColumn1,
            this.dataGridViewTextBoxColumn10});
            this.m_DataGridAttrKeyframes.ContextMenuStrip = this.m_ContextMenuADKeyProp;
            this.m_DataGridAttrKeyframes.GridColor = System.Drawing.SystemColors.Window;
            this.m_DataGridAttrKeyframes.Location = new System.Drawing.Point(9, 57);
            this.m_DataGridAttrKeyframes.MultiSelect = false;
            this.m_DataGridAttrKeyframes.Name = "m_DataGridAttrKeyframes";
            this.m_DataGridAttrKeyframes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridAttrKeyframes.RowHeadersVisible = false;
            this.m_DataGridAttrKeyframes.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.m_DataGridAttrKeyframes.RowTemplate.Height = 15;
            this.m_DataGridAttrKeyframes.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridAttrKeyframes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_DataGridAttrKeyframes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.m_DataGridAttrKeyframes.ShowCellToolTips = false;
            this.m_DataGridAttrKeyframes.Size = new System.Drawing.Size(536, 69);
            this.m_DataGridAttrKeyframes.TabIndex = 46;
            this.m_DataGridAttrKeyframes.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SelectOnRightMouseButton_CellMouseDown);
            this.m_DataGridAttrKeyframes.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_DataGridAttrKeyframes_CellValueChanged);
            this.m_DataGridAttrKeyframes.CurrentCellDirtyStateChanged += new System.EventHandler(this.m_DataGridAttrKeyframes_CurrentCellDirtyStateChanged);
            this.m_DataGridAttrKeyframes.SelectionChanged += new System.EventHandler(this.DataGridAutoSelectValidCell);
            this.m_DataGridAttrKeyframes.DoubleClick += new System.EventHandler(this.m_DataGridAttrKeyframes_DoubleClick);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.dataGridViewTextBoxColumn3.HeaderText = "Property";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 5;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.Red;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewTextBoxColumn4.HeaderText = "VDEvolution1";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 5;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Blue;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewTextBoxColumn5.HeaderText = "VDEvolution2";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 5;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Green;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewTextBoxColumn6.HeaderText = "VDEvolution3";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 5;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Purple;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridViewTextBoxColumn7.HeaderText = "VDEvolution4";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 5;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.SaddleBrown;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn8.HeaderText = "VDEvolution5";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 2;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 2;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.dataGridViewTextBoxColumn9.HeaderText = "VDEvolutionLOD";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 5;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewComboBoxColumn1.HeaderText = "VDEvoBlend";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "Active Avg",
            "Add",
            "Max",
            "Full Avg"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Width = 80;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn10.HeaderText = "InvisAD";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // m_ComboAttrShape
            // 
            this.m_ComboAttrShape.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboAttrShape.FormattingEnabled = true;
            this.m_ComboAttrShape.Location = new System.Drawing.Point(137, 7);
            this.m_ComboAttrShape.Name = "m_ComboAttrShape";
            this.m_ComboAttrShape.Size = new System.Drawing.Size(92, 21);
            this.m_ComboAttrShape.TabIndex = 51;
            this.m_ComboAttrShape.SelectionChangeCommitted += new System.EventHandler(this.m_Combo_SelectionChangeCommitted);
            // 
            // m_CheckAttrWorld
            // 
            this.m_CheckAttrWorld.AutoSize = true;
            this.m_CheckAttrWorld.Checked = true;
            this.m_CheckAttrWorld.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckAttrWorld.Location = new System.Drawing.Point(457, 11);
            this.m_CheckAttrWorld.Name = "m_CheckAttrWorld";
            this.m_CheckAttrWorld.Size = new System.Drawing.Size(88, 17);
            this.m_CheckAttrWorld.TabIndex = 49;
            this.m_CheckAttrWorld.Text = "World Space";
            this.m_CheckAttrWorld.UseVisualStyleBackColor = true;
            this.m_CheckAttrWorld.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // m_CheckAttrShow
            // 
            this.m_CheckAttrShow.AutoSize = true;
            this.m_CheckAttrShow.Checked = true;
            this.m_CheckAttrShow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckAttrShow.Location = new System.Drawing.Point(248, 11);
            this.m_CheckAttrShow.Name = "m_CheckAttrShow";
            this.m_CheckAttrShow.Size = new System.Drawing.Size(92, 17);
            this.m_CheckAttrShow.TabIndex = 48;
            this.m_CheckAttrShow.Text = "Show Domain";
            this.m_CheckAttrShow.UseVisualStyleBackColor = true;
            this.m_CheckAttrShow.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(5, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 47;
            this.label10.Text = "Attractor";
            // 
            // m_SubEvolutions
            // 
            this.m_SubEvolutions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SubEvolutions.Controls.Add(this.label3);
            this.m_SubEvolutions.Controls.Add(this.textBox2);
            this.m_SubEvolutions.Controls.Add(this.label2);
            this.m_SubEvolutions.Controls.Add(this.label5);
            this.m_SubEvolutions.Controls.Add(this.m_CheckOneShot);
            this.m_SubEvolutions.Controls.Add(this.textBox4);
            this.m_SubEvolutions.Controls.Add(this.textBox1);
            this.m_SubEvolutions.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_SubEvolutions.Expanded = true;
            this.m_SubEvolutions.HeaderText = "";
            this.m_SubEvolutions.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.m_SubEvolutions.Location = new System.Drawing.Point(0, 0);
            this.m_SubEvolutions.Margin = new System.Windows.Forms.Padding(0);
            this.m_SubEvolutions.Name = "m_SubEvolutions";
            this.m_SubEvolutions.SectionName = "Emitter Setup";
            this.m_SubEvolutions.Size = new System.Drawing.Size(567, 96);
            this.m_SubEvolutions.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(5, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Number of Loops";
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(109, 61);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(59, 20);
            this.textBox2.TabIndex = 27;
            this.textBox2.Text = "100.000";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(207, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Max Delay Between Loops:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(207, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(134, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Min Delay Between Loops:";
            // 
            // m_CheckOneShot
            // 
            this.m_CheckOneShot.AutoSize = true;
            this.m_CheckOneShot.Checked = true;
            this.m_CheckOneShot.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckOneShot.Location = new System.Drawing.Point(8, 36);
            this.m_CheckOneShot.Name = "m_CheckOneShot";
            this.m_CheckOneShot.Size = new System.Drawing.Size(71, 17);
            this.m_CheckOneShot.TabIndex = 20;
            this.m_CheckOneShot.Text = "One Shot";
            this.m_CheckOneShot.UseVisualStyleBackColor = true;
            this.m_CheckOneShot.Click += new System.EventHandler(this.m_CheckBox_Click);
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(347, 35);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(59, 20);
            this.textBox4.TabIndex = 21;
            this.textBox4.Text = "100.000";
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(347, 61);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(59, 20);
            this.textBox1.TabIndex = 25;
            this.textBox1.Text = "100.000";
            // 
            // EmitterWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.m_PanelHeader);
            this.Controls.Add(this.m_PanelGroup);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "EmitterWindow";
            this.Size = new System.Drawing.Size(600, 721);
            this.m_PanelHeader.ResumeLayout(false);
            this.m_PanelHeader.PerformLayout();
            this.m_PanelGroup.ResumeLayout(false);
            this.m_PanelSection.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ptxSubSection2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridKeyframeList)).EndInit();
            this.ptxSubSection1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridEmitKeyframes)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridVelKeyframes)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridAttrKeyframes)).EndInit();
            this.m_SubEvolutions.ResumeLayout(false);
            this.m_SubEvolutions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel m_PanelHeader;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel m_PanelGroup;
        private System.Windows.Forms.Panel m_PanelSection;
        private ptxSubSection m_SubEvolutions;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label m_LabelHeader;
        private System.Windows.Forms.CheckBox m_CheckOneShot;
        private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private ptxSubSection ptxSubSection1;
        private System.Windows.Forms.Label label3;
        private ptxSubSection ptxSubSection2;
		private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox m_ComboVelShape;
        private System.Windows.Forms.CheckBox m_CheckVelPointRel;
        private System.Windows.Forms.CheckBox m_CheckVelWorld;
        private System.Windows.Forms.CheckBox m_CheckVelShow;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.ComboBox m_ComboEmitShape;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox m_CheckEmitWorld;
		private System.Windows.Forms.CheckBox m_CheckEmitShow;
        private System.Windows.Forms.DataGridView m_DataGridKeyframeList;
        private System.Windows.Forms.DataGridView m_DataGridEmitKeyframes;
        private System.Windows.Forms.DataGridView m_DataGridVelKeyframes;
        private System.Windows.Forms.ContextMenuStrip m_ContextMenuKeyProp;
        private System.Windows.Forms.ContextMenuStrip m_ContextMenuEDKeyProp;
        private System.Windows.Forms.ContextMenuStrip m_ContextMenuVDKeyProp;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_ColumnTextRuleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evolution5;
        private System.Windows.Forms.DataGridViewTextBoxColumn EvolutionLOD;
        private System.Windows.Forms.DataGridViewComboBoxColumn EvoBlend;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvisK;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDEvolution1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDEvolution2;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDEvolution3;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDEvolution4;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDEvolution5;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDEvolutionLOD;
        private System.Windows.Forms.DataGridViewComboBoxColumn EDEvoBlend;
        private System.Windows.Forms.DataGridViewTextBoxColumn Invis;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn VDEvolution1;
        private System.Windows.Forms.DataGridViewTextBoxColumn VDEvolution2;
        private System.Windows.Forms.DataGridViewTextBoxColumn VDEvolution3;
        private System.Windows.Forms.DataGridViewTextBoxColumn VDEvolution4;
        private System.Windows.Forms.DataGridViewTextBoxColumn VDEvolution5;
        private System.Windows.Forms.DataGridViewTextBoxColumn VDEvolutionLOD;
        private System.Windows.Forms.DataGridViewComboBoxColumn VDEvoBlend;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvisVD;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.DataGridView m_DataGridAttrKeyframes;
		private System.Windows.Forms.ComboBox m_ComboAttrShape;
		private System.Windows.Forms.CheckBox m_CheckAttrWorld;
		private System.Windows.Forms.CheckBox m_CheckAttrShow;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.ContextMenuStrip m_ContextMenuADKeyProp;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label m_LabelAttrShape;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
		private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
		private System.Windows.Forms.Button m_ButtonAttrEnableToggle;
        private System.Windows.Forms.CheckBox m_CheckVelCreationRel;
        private System.Windows.Forms.CheckBox m_CheckAttrTargetRel;
        private System.Windows.Forms.CheckBox m_CheckAttrCreationRel;
    }
}
