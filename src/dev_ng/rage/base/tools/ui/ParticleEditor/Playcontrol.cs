using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RSG.Base.Network;

namespace ParticleEditor
{
    public partial class Playcontrol : ptxForm
    {
        public String m_CurEffectName = "";
        public int m_AnimateOrientation = 0;
        public Playcontrol()
        {
            m_Name = "PlayControl";
            m_WindowID = "PLAYCONTROL";
            InitializeComponent();
            LayoutDesign();
        }

        public void SetEffectRule(String effectName)
        {
            m_CheckAnimateEffect.Checked = false;
            LayoutDesign();

            if (m_CurEffectName != effectName)
                ChangedPlayControl();

            m_CurEffectName = effectName;
            m_LabelPlaybackOptions.Text = "Playback Options";
            m_ButtonPlay.Text = "Play";
            if (m_CurEffectName.Length > 0)
            {
                m_ButtonPlay.Text += " (" + m_CurEffectName + ")";
                //m_LabelPlaybackOptions.Text += " (" + m_CurEffectName + ")";
            }
        }

        private void LayoutDesign()
        {
            bool enab = m_CheckAnimateEffect.Checked;
            m_ComboAnimateType.Enabled = enab;
            m_SliderAnimateDist.Enabled = enab;
            m_SliderAnimateSpeed.Enabled = enab;

        }

        private void ChangedPlayControl()
        {
            DataCon.m_SDataCon.SendPlayControl();
        }

        private void Data_OnValueChanged(ptxSlider slider)
        {
            ChangedPlayControl();
        }

        private void m_Radio_CheckedChanged(object sender, EventArgs e)
        {
            m_WPosX.Enabled = !m_RadioSCam.Checked;
            m_WPosY.Enabled = !m_RadioSCam.Checked;
            m_WPosZ.Enabled = !m_RadioSCam.Checked;
            m_CamStartDist.Enabled = m_RadioSCam.Checked;
        }

        protected override void LoadConfigData()
        {
            m_RadioSCam.Checked = bool.Parse(m_ConfigFile.IniReadValueDefault(m_WindowID, "SPAWNCAM", "TRUE"));
            m_CamStartDist.SetValue = float.Parse(m_ConfigFile.IniReadValueDefault(m_WindowID, "SPAWNCAMDIST", "10.0"));
            m_WPosX.SetValue = float.Parse(m_ConfigFile.IniReadValueDefault(m_WindowID, "SPAWNWORLDPOSX", "0.0"));
            m_WPosY.SetValue = float.Parse(m_ConfigFile.IniReadValueDefault(m_WindowID, "SPAWNWORLDPOSY", "0.0"));
            m_WPosZ.SetValue = float.Parse(m_ConfigFile.IniReadValueDefault(m_WindowID, "SPAWNWORLDPOSZ", "0.0"));
            m_ComboAnimateType.Text= m_ConfigFile.IniReadValueDefault(m_WindowID, "ANIMATEORIENTATION", "X-Y-Plane");
            base.LoadConfigData();

            m_RadioSWorld.Checked = !m_RadioSCam.Checked;
            m_Radio_CheckedChanged(null, null);
        }

        protected override void SaveConfigData()
        {
            m_ConfigFile.IniWriteValue(m_WindowID, "SPAWNCAM", m_RadioSCam.Checked.ToString());
            m_ConfigFile.IniWriteValue(m_WindowID, "SPAWNCAMDIST", m_CamStartDist.Value.ToString());
            m_ConfigFile.IniWriteValue(m_WindowID, "SPAWNWORLDPOSX", m_WPosX.Value.ToString());
            m_ConfigFile.IniWriteValue(m_WindowID, "SPAWNWORLDPOSY", m_WPosY.Value.ToString());
            m_ConfigFile.IniWriteValue(m_WindowID, "SPAWNWORLDPOSZ", m_WPosZ.Value.ToString());
            m_ConfigFile.IniWriteValue(m_WindowID, "ANIMATEORIENTATION", m_ComboAnimateType.Text);
            base.SaveConfigData();
        }

        private void m_ButtonPlay_Click(object sender, EventArgs e)
        {
            if (m_TimeScalar.Value == 0.0f)
                m_TimeScalar.Value = 1.0f;
            else
            {
                DataCon.m_SDataCon.SendStartEffectEvent(main.m_MainForm.m_EffectWindow.m_CurrentEffectName);
                ChangedPlayControl();
            }
        }

        private void m_PauseButton_Click(object sender, EventArgs e)
        {
            if (m_TimeScalar.Value > 0.0f)
                m_TimeScalar.Value = 0.0f;  //Set value to cause on change event
            else
                m_TimeScalar.Value = 1.0f;
        }

        private void m_ButtonResetEffect_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.SendResetEvent(m_CurEffectName);
        }

        private void m_ButtonReset_Click(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.SendResetEvent();
        }

        private void m_CheckAnimateEffect_Click(object sender, EventArgs e)
        {
            LayoutDesign();
            ChangedPlayControl();
        }

        private void m_ComboAnimateType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            m_AnimateOrientation = 0;
            if (m_ComboAnimateType.SelectedIndex>=0)
                m_AnimateOrientation = m_ComboAnimateType.SelectedIndex;
            ChangedPlayControl();
        }

        private void checkBox1_Click(object sender, EventArgs e)
        {
            ChangedPlayControl();
        }

        private void ragSyncTimer_Tick(object sender, EventArgs e)
        {
            if (!syncFromRAGCheckBox.Checked)
            {
                return;
            }

            cRemoteConsole console = main.m_MainForm.m_remoteConsole;
            if (!console.IsConnected())
            {
                return;
            }

            String position = console.ReadStringWidget("Script/Script Debug Tools/Cursor Pos");
            if (!String.IsNullOrEmpty(position))
            {
                String[] components = position.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (components.Length == 3)
                {
                    float x, y, z;
                    if (Single.TryParse(components[0], out x) &&
                        Single.TryParse(components[1], out y) &&
                        Single.TryParse(components[2], out z))
                    {
                        m_WPosX.Value = x;
                        m_WPosY.Value = y;
                        m_WPosZ.Value = z;
                    }
                }
            }
        }

        private void syncFromRAGCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            cRemoteConsole console = main.m_MainForm.m_remoteConsole;

            if (syncFromRAGCheckBox.Checked)
            {
                // Make sure the correct banks are created and toggled on.
                if (!console.WidgetExists("Script/Script Debug Tools/Enable Debugging"))
                {
                    console.PressWidgetButton("Script/Toggle Script bank");
                    console.SendSyncCommand();
                }

                console.WriteBoolWidget("Script/Script Debug Tools/Enable Debugging", true);
                console.SendSyncCommand();

                console.AddUpdates("Script/Script Debug Tools");
                ragSyncTimer.Start();
            }
            else
            {
                console.RemoveUpdates("Script/Script Debug Tools");
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            if (syncFromRAGCheckBox.Checked)
            {
                cRemoteConsole console = main.m_MainForm.m_remoteConsole;
                console.RemoveUpdates("Script/Script Debug Tools");
            }
            base.OnClosed(e);
        }
    }
}