using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
    public partial class EventEffect : UserControl
    {
        public DataMod.dtaEventEffect m_CurrentEvent = null;
        EffectWindow m_EffectWindow = null;
        public EventEffect()
        {
            InitializeComponent();
            m_DataGridEmitKeyframes.ColumnHeadersVisible = false;
        }

        public void BuildDomainTypeList()
        {
            m_ComboEmitShape.SuspendLayout();
            m_ComboEmitShape.Items.Clear();
            for (int i = 0; i < DataMod.m_SDataMod.m_EmitterDomainTypes.Count; i++)
            {
                m_ComboEmitShape.Items.Add(DataMod.m_SDataMod.m_DomainTypes[i]);
            }

            m_ComboEmitShape.ResumeLayout();
        }
        public void BuildEffectRuleDropDown()
        {
            String curSel = m_ComboEffectRule.Text;
            m_ComboEffectRule.SuspendLayout();
            m_ComboEffectRule.Items.Clear();
            for (int i = 0; i < DataMod.m_SDataMod.m_EffectRuleList.Count; i++)
            {
                //Don't allow adding current effectrule
                if (DataMod.m_SDataMod.m_EffectRuleList[i].ToString() != m_EffectWindow.m_CurrentEffectName)
                    m_ComboEffectRule.Items.Add(DataMod.m_SDataMod.m_EffectRuleList[i]);
            }
            m_ComboEffectRule.Items.Add("");
            m_ComboEffectRule.ResumeLayout();
            m_ComboEffectRule.Text = curSel;
        }

        public void SetDataFromGui(DataMod.dtaEventEffect doe)
        {
            if (doe == null) return;

            doe.m_ActiveToggle = m_CheckEvtActive.Checked;
 //           doe.m_TriggerTime = m_EvtStartTime.Value;
            doe.m_TriggerCap = (int)m_SliderTriggerCap.Value;
            doe.m_ShowOrientation = m_CheckShowOrientation.Checked;
            doe.m_Orientation.x = m_SliderRotationX.Value;
            doe.m_Orientation.y = m_SliderRotationY.Value;
            doe.m_Orientation.z = m_SliderRotationZ.Value;

            //Trigger Domain
            doe.m_ShowTriggerDomain =m_CheckEmitShow.Checked;
            doe.m_TriggerDomain.m_WorldSpace = m_CheckEmitWorld.Checked;
            doe.m_TriggerDomain.m_Type = DataMod.GetDomainTypeFromName(m_ComboEmitShape.Items[m_ComboEmitShape.SelectedIndex].ToString());

            doe.m_EffectName = m_ComboEffectRule.Text;
        }

        public void DisplayEvent(DataMod.dtaEventEffect doe)
        {
            m_EffectWindow = main.m_MainForm.m_EffectWindow;
            m_CurrentEvent = doe;
            if(doe == null) return;

            m_CheckEvtActive.Checked = doe.m_ActiveToggle;
            m_CheckShowOrientation.Checked = doe.m_ShowOrientation;
 //           m_EvtStartTime.SetValue = doe.m_TriggerTime;
            m_SliderTriggerCap.SetValue = doe.m_TriggerCap;
            m_SliderRotationX.SetValue = doe.m_Orientation.x;
            m_SliderRotationY.SetValue = doe.m_Orientation.y;
            m_SliderRotationZ.SetValue = doe.m_Orientation.z;
            
            //Trigger Domain
            m_CheckEmitShow.Checked = doe.m_ShowTriggerDomain;
            
            m_CheckEmitWorld.Checked = doe.m_TriggerDomain.m_WorldSpace;
            m_ComboEmitShape.SelectedIndex = doe.m_TriggerDomain.m_Type;
            main.m_MainForm.BuildKeyframeGrid(m_DataGridEmitKeyframes, doe.m_TriggerDomain.m_KeyframeList);

            BuildEffectRuleDropDown();
            m_ComboEffectRule.Text = doe.m_EffectName;
        }

        private void ShowKeyframe(object sender, EventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid.CurrentCell == null) return;
            int cdx = grid.CurrentCell.ColumnIndex;

            String desc = "Trigger Domain (" + m_CurrentEvent.ToString() + ")";

            KeyframeEditorFormBase.ShowKeyframe(GetSelectedKeyframeCellFromDataGrid(sender as DataGridView), desc + ":");
        }
        public DataMod.KeyframeSpec GetSelectedKeyframeFromDataGrid(DataGridView grid)
        {
            if (grid.CurrentRow == null) return null;
            DataMod.KeyframeSpec key = grid.CurrentRow.Cells[0].Tag as DataMod.KeyframeSpec;
            return key;

        }
        public DataMod.KeyframeSpec GetSelectedKeyframeCellFromDataGrid(DataGridView grid)
        {
            if (grid.CurrentCell == null) return null;
            DataMod.KeyframeSpec key = grid.CurrentCell.Tag as DataMod.KeyframeSpec;
            return key;

        }

        public void SelectOnRightMouseButton_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                DataGridView grid = sender as DataGridView;
                grid.CurrentCell = grid.Rows[e.RowIndex].Cells[e.ColumnIndex];
            }

        }
        public void DataGridAutoSelectValidCell(object sender, EventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid.CurrentCell == null) return;
            if (grid.CurrentCell.Value == null)
                grid.CurrentCell = grid.Rows[grid.CurrentCell.RowIndex].Cells[0];
        }

        private void Slider_OnValueChanged(ptxSlider slider)
        {
            m_EffectWindow.m_Slider_OnValueChanged(slider);
        }

        private void CheckBox_Click(object sender, EventArgs e)
        {
            m_EffectWindow.m_CheckBox_Click(sender, e);
        }

        private void m_ComboEmitShape_SelectionChangeCommitted(object sender, EventArgs e)
        {
            m_EffectWindow.SetDataFromGui();
            m_EffectWindow.ChangedEffect(m_EffectWindow.m_CurrentEffectName);

        }

        private void m_ComboEffectRule_DropDown(object sender, EventArgs e)
        {
            BuildEffectRuleDropDown();
        }

        private void m_ComboEffectRule_SelectionChangeCommitted(object sender, EventArgs e)
        {
            m_EffectWindow.SetEventDataFromGui();
            m_EffectWindow.ChangedEffect(m_EffectWindow.m_CurrentEffectName);
            main.m_MainForm.RequestEffect(m_EffectWindow.m_CurrentEffectName);

        }
    }
}
