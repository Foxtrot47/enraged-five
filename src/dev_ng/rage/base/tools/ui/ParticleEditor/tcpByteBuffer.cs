using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;


namespace ParticleEditor
{
    public class tcpByteBuffer
    {
        [StructLayout(LayoutKind.Explicit)]
        public struct FloatToInt
        {
            [FieldOffset(0)]
            public float f;
            [FieldOffset(0)]
            public uint u;
        }
        public byte[] m_Storage;
        protected int m_Length = 0;
        protected int m_ReadPos = 0;
        
        //Notes:
        //Read functions cleanly handle "past bounds" case by returning 0 or ""

        public tcpByteBuffer()
        {

        }

        public tcpByteBuffer(int size)
        {
            m_Storage = new byte[size];
        }

        public bool IsReady()
        {
            if (m_Length == 0)
                return false;
            int rp = m_ReadPos;
            m_ReadPos = 0;
            int size = (int)Read_u32();
            m_ReadPos = rp;
            return (size == (m_Length-4));
        }

        public void BeginRead()
        {
            m_ReadPos = 0;
        }
        public void BeginWrite()
        {
            m_Length = 0;
        }

        public void BeginTCPBuffer()
        {
            BeginWrite();
            Write_u32(0); //Placeholder for size
        }

        public void EndTCPBuffer()
        {
            int l = m_Length;
            m_Length = 0;
            Write_u32((uint)l - 4);
            m_Length = l;
        }

        public void Clear()
        {
            m_Length = 0;
            m_ReadPos = 0;
        }

        public byte Read_u8()
        {
            Debug.Assert(m_ReadPos >= 0);
            if (m_ReadPos >= m_Storage.Length)
                return 0;
            return m_Storage[m_ReadPos++];
        }

        public sbyte Read_s8()
        {
            return (sbyte)Read_u8();
        }

        public bool Read_bool() { return Read_u8() != 0; }
        
        public ushort Read_u16()
        {
            Debug.Assert(m_ReadPos >= 0);
            if (m_ReadPos >= m_Storage.Length)
                return 0;

            ushort result = m_Storage[m_ReadPos++];
            return (ushort)(result | (m_Storage[m_ReadPos++] << 8));
        }
        
        public short Read_s16() { return (short)Read_u16(); }

        public uint Read_u32()
        {
            Debug.Assert(m_ReadPos >= 0);
            if (m_ReadPos >= m_Storage.Length)
                return 0;

            uint result = m_Storage[m_ReadPos++];
            result |= (uint)(m_Storage[m_ReadPos++] << 8);
            result |= (uint)(m_Storage[m_ReadPos++] << 16);
            return result | (uint)(m_Storage[m_ReadPos++] << 24);
        }

        public int Read_s32() { return (int)Read_u32(); }
        
        public float Read_float()
        {
            FloatToInt x;
            x.f = 0.0f;
            x.u = Read_u32();
            return x.f;
        }

        public String Read_const_char()
        {
            Debug.Assert(m_ReadPos >= 0);
            if (m_ReadPos >= m_Storage.Length)
                return "";

            uint sl = Read_u32();
            if (sl != 0)
            {
                char[] array = new char[sl - 1];
                for (uint i = 0; i < sl - 1; i++)
                    array[i] = Convert.ToChar(Read_u8());
                Read_u8(); //read null character
                String result = new String(array);
                return result;
            }
            else
            {
                return "";
            }
        }

        void CheckForBufferOverwrite()
        {
            Debug.Assert(m_Length <= m_Storage.Length);
        }

        public void Write_u8(byte value)
        {
            Debug.Assert(m_Length >= 0);
            m_Storage[m_Length++] = value;
            CheckForBufferOverwrite();
        }

        public void Write_s8(sbyte value) { Write_u8((byte)value); }

        public void Write_bool(bool value) { Write_u8((byte)((value == true) ? 1 : 0)); }

        public void Write_u16(ushort value)
        {
            Debug.Assert(m_Length >=0 );
            m_Storage[m_Length++] = (byte)(value);
            m_Storage[m_Length++] = (byte)(value >> 8);
            CheckForBufferOverwrite();
        }

        public void Write_s16(short value) { Write_u16((ushort)value); }

        public void Write_u32(uint value)
        {
            Debug.Assert(m_Length >= 0);
            m_Storage[m_Length++] = (byte)(value);
            m_Storage[m_Length++] = (byte)(value >> 8);
            m_Storage[m_Length++] = (byte)(value >> 16);
            m_Storage[m_Length++] = (byte)(value >> 24);
            CheckForBufferOverwrite();
        }

        public void Write_s32(int value) { Write_u32((uint)value); }

        public void Write_float(float value)
        {
            FloatToInt x;
            x.u = 0;
            x.f = value;
            Write_u32(x.u);
        }

        public void Write_const_char(String value)
        {
            Debug.Assert(m_Length >=0);
            if (value == null || value.Length == 0)
            {
                Write_u32(0);
            }
            else
            {
                int sl = value.Length;
                Write_u32((uint)sl);
                for (int i = 0; i < sl; i++)
                    m_Storage[m_Length++] = Convert.ToByte(value[i]);
                m_Storage[m_Length++] = 0x0; // null byte
            }
            CheckForBufferOverwrite();
        }

        public int GetLength()
        {
            return m_Length;
        }

        public void SetLength(int size)
        {
            m_Length = size;
        }

        public int GetReadPos()
        {
            return m_ReadPos;
        }
        public void SetReadPos(int pos)
        {
            m_ReadPos = pos;
        }

    }
}
