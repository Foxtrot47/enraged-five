using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ragKeyframeEditorControl;

namespace ParticleEditor
{
    public partial class KeyframeEditorForm : KeyframeEditorFormBase
    {

        public KeyframeEditorForm()
        {
            InitializeComponent();
            keyframeEditor1.ValueChanged += SendNewKeys;
            KeyframeCurveEditor.realtimeUpdateMode = true;
        }

        void SendNewKeys(object obj, EventArgs args)
        {
            List<ragKeyframeEditorControl.KeyframeCurveEditor.Float5> f5s = ragKeyframeEditorControl.KeyframeCurveEditor.ConvertRawCurvesToFloat5s(keyframeEditor1.Value);
            DataMod.KeyframeData data = new DataMod.KeyframeData();

            foreach (ragKeyframeEditorControl.KeyframeCurveEditor.Float5 f5 in f5s)
            {
                DataMod.KeyframeData.Key k = new DataMod.KeyframeData.Key();
                k.Time = f5.t;
                k.Data = new float[4];
                k.Data[0] = f5.x;
                k.Data[1] = f5.y;
                k.Data[2] = f5.z;
                k.Data[3] = f5.w;
                data.m_KeyData.Add(k);
            }

            Debug.Assert(data.m_KeyData.Count > 0);

            data.PreSave(m_uiData);

			DataCon.m_SDataCon.SendKeyframeData(m_RuleName, m_RuleType, m_PropertyId, m_EventIdx, m_EvoIdx, data);
        }

        public override void  ConfigureEditor(DataMod.KeyframeUiData uidata)
        {
            keyframeEditor1.ConfigureEditor(
                uidata.Labels,
                uidata.Colors,
                uidata.XMin,
                uidata.XMax,
                uidata.YMin,
                uidata.YMax,
                uidata.XStep,
                uidata.YStep);

        }

        public override void SetInitialData(DataMod.KeyframeData data)
        {
            List<ragKeyframeEditorControl.KeyframeCurveEditor.Float5> float5s = new List<ragKeyframeEditorControl.KeyframeCurveEditor.Float5>();
            foreach (DataMod.KeyframeData.Key k in data.m_KeyData)
            {
                ragKeyframeEditorControl.KeyframeCurveEditor.Float5 f5 = new ragKeyframeEditorControl.KeyframeCurveEditor.Float5();
                f5.t = k.Time;
                f5.x = k.Data[0];
                f5.y = k.Data[1];
                f5.z = k.Data[2];
                f5.w = k.Data[3];
                float5s.Add(f5);
            }

            keyframeEditor1.Value = ragKeyframeEditorControl.KeyframeCurveEditor.ConvertFloat5sToRawCurves(float5s);
            keyframeEditor1.DetermineAutoZoom();
        }
    }
}
