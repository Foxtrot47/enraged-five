using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace ParticleEditor
{
    public partial class tcpComServer :UserControl
    {

#region Variables
        String m_AddressString = "0.0.0.0";
        IPAddress m_Address = IPAddress.Any;
        Int32 m_PortNumber = 16000;
        TcpClient m_Client = null;
        static bool m_Connected = false;
#endregion
        
#region Properties
        [Description("Time in ms between updates"), Category("TCP")]
        public int UpdateInterval
        {
            get
            {
                return m_UpdateTimer.Interval;
            }
            set
            {
                m_UpdateTimer.Interval = value;
            }
        }

        [Description("IP Address"), Category("TCP")]
        public String Address
        {
            get
            {
                return m_Address.ToString();
            }
            set
            {
                IPAddress addy = new IPAddress(m_Address.GetAddressBytes());
                if (IPAddress.TryParse(value, out addy))
                {
                    m_AddressString = addy.ToString();
                    m_Address = addy;
                    return;
                }
            }
        }

        [Description("Port number"), Category("TCP")]
        public Int32 Port
        {
            get
            {
                return m_PortNumber;
            }
            set
            {
                m_PortNumber = value;
            }
        }
#endregion

        public tcpComServer()
        {
            InitializeComponent();
            Visible = false;
        }

        public void Connect()
        {
            if (m_Client != null)
                Disconnect();

            m_Client = new TcpClient();
            try
            {
                m_Client.BeginConnect(m_Address,m_PortNumber,new AsyncCallback(OnClientConnect), m_Client);
            }
            catch (SocketException /*e*/)
            {
                Disconnect();
            }
        }
        public void OnClientConnect(IAsyncResult ar)
        {
            TcpClient client = ar.AsyncState as TcpClient;
            m_Connected = client.Connected;
        }
        
        public void Disconnect()
        {
            if (m_Client.Client != null)
                m_Client.Client.Close();
            m_Client = null;
            m_Connected = false;
        }

        private void m_UpdateTimer_Tick(object sender, EventArgs e)
        {
            if (!m_Connected)
                return;
        }
    }
}
