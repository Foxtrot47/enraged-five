namespace ParticleEditor
{
    partial class EvolutionWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_LabelEffectName = new System.Windows.Forms.Label();
            this.m_PanelSliders = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // m_LabelEffectName
            // 
            this.m_LabelEffectName.AutoSize = true;
            this.m_LabelEffectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelEffectName.Location = new System.Drawing.Point(1, 2);
            this.m_LabelEffectName.Name = "m_LabelEffectName";
            this.m_LabelEffectName.Size = new System.Drawing.Size(81, 13);
            this.m_LabelEffectName.TabIndex = 0;
            this.m_LabelEffectName.Text = "Select Effect";
            // 
            // m_PanelSliders
            // 
            this.m_PanelSliders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_PanelSliders.Cursor = System.Windows.Forms.Cursors.Default;
            this.m_PanelSliders.Location = new System.Drawing.Point(0, 18);
            this.m_PanelSliders.Name = "m_PanelSliders";
            this.m_PanelSliders.Size = new System.Drawing.Size(238, 153);
            this.m_PanelSliders.TabIndex = 1;
            // 
            // EvolutionWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(238, 172);
            this.Controls.Add(this.m_PanelSliders);
            this.Controls.Add(this.m_LabelEffectName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EvolutionWindow";
            this.Text = "Evolution Preview";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_LabelEffectName;
        private System.Windows.Forms.Panel m_PanelSliders;

    }
}
