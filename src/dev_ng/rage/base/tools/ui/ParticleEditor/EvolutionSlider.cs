using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
    public partial class EvolutionSlider : UserControl
    {
        int   m_Index = 0;
        Color  m_Color = Color.Black;
        String m_Name = "";

        public Object m_DataObject;
       

#region Properties
        public int Index
        {
            get { return m_Index; }
            set { m_Index = value; UpdateDisplay(); }
        }

        public String SliderName
        {
            get { return m_Name; }
            set { m_Name = value; UpdateDisplay(); }
        }

        public Color Color
        {
            get { return m_Color; }
            set { m_Color = value; UpdateDisplay(); }
        }

        public bool Checked
        {
            get { return m_CheckOverride.Checked; }
            set { m_CheckOverride.Checked= value;}
        }

        public float Value
        {
            get { return m_SliderValue.Value; }
            set { m_SliderValue.SetValue = value;}
        }
        public delegate void evoSliderEventDelegate(EvolutionSlider slider);
        [Description("Occurs when user changes the value"), Category("Action")]
        public event evoSliderEventDelegate OnValueChanged = null;

#endregion

        public EvolutionSlider()
        {
            InitializeComponent();
            m_SliderValue.OnValueChanged +=new ptxSlider.ptxSliderEventDelegate(m_SliderValue_OnValueChanged);
        }

        public void UpdateDisplay()
        {
            m_LabelID.ForeColor = m_Color;
            m_LabelID.Text = "[" + m_Index.ToString() + "]";
            m_LabelName.Text = m_Name;
        }

        private void m_SliderValue_OnValueChanged(ptxSlider slider)
        {
            if (OnValueChanged != null)
                OnValueChanged(this);
            Checked = true;
        }

        private void m_CheckOverride_MouseClick(object sender, MouseEventArgs e)
        {
            if (OnValueChanged != null)
                OnValueChanged(this);
        }

        private void m_LabelName_Click(object sender, EventArgs e)
        {
            Checked = !Checked;
            if (OnValueChanged != null)
                OnValueChanged(this);
        }
    }
}

