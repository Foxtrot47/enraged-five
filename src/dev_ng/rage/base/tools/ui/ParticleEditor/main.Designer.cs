namespace ParticleEditor
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_EmitterContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newEmitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadEmitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeEmitter0ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveEmitter0ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_PtxContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newPTXRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spriteRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modelRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.behaviorRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadPTXRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removePTXRule0ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savePTXRule0ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clonePTXRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_EffectContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newEffectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadEffectToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.removeEffect0ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveEffectAndChildrenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveEffectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_UpdateTimer = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_effectsFilterBox = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.m_DataGridEffects = new System.Windows.Forms.DataGridView();
            this.m_value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_EffectWindow = new ParticleEditor.EffectWindow();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.m_ptxFilterBox = new System.Windows.Forms.TextBox();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.m_DataGridPtxList = new System.Windows.Forms.DataGridView();
            this.m_ptxValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_ptxName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_PtxRuleWindow = new ParticleEditor.PtxWindow();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.m_emittersFilterBox = new System.Windows.Forms.TextBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.m_DataGridEmitters = new System.Windows.Forms.DataGridView();
            this.m_emitValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_emitName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_EmitterWindow = new ParticleEditor.EmitterWindow();
            this.label3 = new System.Windows.Forms.Label();
            this.m_TimelineLabel = new System.Windows.Forms.Label();
            this.m_MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadEffectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openEffectListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.saveRulesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveEffectListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_WindowMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.m_TimeLine = new ragPtxTimeLine.ptxTimeLine();
            this.tcpComClient1 = new ParticleEditor.tcpComClient();
            this.m_EmitterContextMenu.SuspendLayout();
            this.m_PtxContextMenu.SuspendLayout();
            this.m_EffectContextMenu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridEffects)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridPtxList)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridEmitters)).BeginInit();
            this.m_MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_EmitterContextMenu
            // 
            this.m_EmitterContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newEmitterToolStripMenuItem,
            this.loadEmitterToolStripMenuItem,
            this.removeEmitter0ToolStripMenuItem,
            this.saveEmitter0ToolStripMenuItem});
            this.m_EmitterContextMenu.Name = "m_EmitterContextMenu";
            this.m_EmitterContextMenu.ShowImageMargin = false;
            this.m_EmitterContextMenu.Size = new System.Drawing.Size(160, 92);
            this.m_EmitterContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.m_EmitterContextMenu_Opening);
            // 
            // newEmitterToolStripMenuItem
            // 
            this.newEmitterToolStripMenuItem.Name = "newEmitterToolStripMenuItem";
            this.newEmitterToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.newEmitterToolStripMenuItem.Text = "New Emitter";
            this.newEmitterToolStripMenuItem.Click += new System.EventHandler(this.newEmitterToolStripMenuItem_Click);
            // 
            // loadEmitterToolStripMenuItem
            // 
            this.loadEmitterToolStripMenuItem.Name = "loadEmitterToolStripMenuItem";
            this.loadEmitterToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.loadEmitterToolStripMenuItem.Text = "Load Emitter...";
            this.loadEmitterToolStripMenuItem.Click += new System.EventHandler(this.LoadEmitRules_Click);
            // 
            // removeEmitter0ToolStripMenuItem
            // 
            this.removeEmitter0ToolStripMenuItem.Name = "removeEmitter0ToolStripMenuItem";
            this.removeEmitter0ToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.removeEmitter0ToolStripMenuItem.Text = "Remove Emitter {0}...";
            this.removeEmitter0ToolStripMenuItem.Click += new System.EventHandler(this.removeEmitter0ToolStripMenuItem_Click);
            // 
            // saveEmitter0ToolStripMenuItem
            // 
            this.saveEmitter0ToolStripMenuItem.Name = "saveEmitter0ToolStripMenuItem";
            this.saveEmitter0ToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.saveEmitter0ToolStripMenuItem.Text = "Save Emitter {0}";
            this.saveEmitter0ToolStripMenuItem.Click += new System.EventHandler(this.SaveEmitter_Click);
            // 
            // m_PtxContextMenu
            // 
            this.m_PtxContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newPTXRuleToolStripMenuItem,
            this.loadPTXRuleToolStripMenuItem,
            this.removePTXRule0ToolStripMenuItem,
            this.savePTXRule0ToolStripMenuItem,
            this.clonePTXRuleToolStripMenuItem});
            this.m_PtxContextMenu.Name = "m_PtxContextMenu";
            this.m_PtxContextMenu.ShowImageMargin = false;
            this.m_PtxContextMenu.Size = new System.Drawing.Size(160, 114);
            this.m_PtxContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.m_PtxContextMenu_Opening);
            // 
            // newPTXRuleToolStripMenuItem
            // 
            this.newPTXRuleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spriteRuleToolStripMenuItem,
            this.modelRuleToolStripMenuItem,
            this.behaviorRuleToolStripMenuItem});
            this.newPTXRuleToolStripMenuItem.Name = "newPTXRuleToolStripMenuItem";
            this.newPTXRuleToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.newPTXRuleToolStripMenuItem.Text = "New PTX Rule";
            // 
            // spriteRuleToolStripMenuItem
            // 
            this.spriteRuleToolStripMenuItem.Name = "spriteRuleToolStripMenuItem";
            this.spriteRuleToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.spriteRuleToolStripMenuItem.Text = "Sprite Rule";
            this.spriteRuleToolStripMenuItem.Click += new System.EventHandler(this.spriteRuleToolStripMenuItem_Click);
            // 
            // modelRuleToolStripMenuItem
            // 
            this.modelRuleToolStripMenuItem.Name = "modelRuleToolStripMenuItem";
            this.modelRuleToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.modelRuleToolStripMenuItem.Text = "Model Rule";
            this.modelRuleToolStripMenuItem.Click += new System.EventHandler(this.modelRuleToolStripMenuItem_Click);
            // 
            // behaviorRuleToolStripMenuItem
            // 
            this.behaviorRuleToolStripMenuItem.Name = "behaviorRuleToolStripMenuItem";
            this.behaviorRuleToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.behaviorRuleToolStripMenuItem.Text = "Behavior Rule";
            this.behaviorRuleToolStripMenuItem.Click += new System.EventHandler(this.behaviorRuleToolStripMenuItem_Click);
            // 
            // loadPTXRuleToolStripMenuItem
            // 
            this.loadPTXRuleToolStripMenuItem.Name = "loadPTXRuleToolStripMenuItem";
            this.loadPTXRuleToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.loadPTXRuleToolStripMenuItem.Text = "Load PTX Rule...";
            this.loadPTXRuleToolStripMenuItem.Click += new System.EventHandler(this.LoadPtxRules_Click);
            // 
            // removePTXRule0ToolStripMenuItem
            // 
            this.removePTXRule0ToolStripMenuItem.Name = "removePTXRule0ToolStripMenuItem";
            this.removePTXRule0ToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.removePTXRule0ToolStripMenuItem.Text = "Remove PTX Rule {0}";
            this.removePTXRule0ToolStripMenuItem.Click += new System.EventHandler(this.removePTXRule0ToolStripMenuItem_Click);
            // 
            // savePTXRule0ToolStripMenuItem
            // 
            this.savePTXRule0ToolStripMenuItem.Name = "savePTXRule0ToolStripMenuItem";
            this.savePTXRule0ToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.savePTXRule0ToolStripMenuItem.Text = "Save PTX Rule {0}";
            this.savePTXRule0ToolStripMenuItem.Click += new System.EventHandler(this.SavePtxRule_Click);
            // 
            // clonePTXRuleToolStripMenuItem
            // 
            this.clonePTXRuleToolStripMenuItem.Name = "clonePTXRuleToolStripMenuItem";
            this.clonePTXRuleToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.clonePTXRuleToolStripMenuItem.Text = "Clone PTX Rule";
            this.clonePTXRuleToolStripMenuItem.Click += new System.EventHandler(this.clonePTXRuleToolStripMenuItem_Click);
            // 
            // m_EffectContextMenu
            // 
            this.m_EffectContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newEffectToolStripMenuItem,
            this.loadEffectToolStripMenuItem1,
            this.removeEffect0ToolStripMenuItem,
            this.saveEffectAndChildrenToolStripMenuItem,
            this.saveEffectToolStripMenuItem});
            this.m_EffectContextMenu.Name = "m_EffectContextMenu";
            this.m_EffectContextMenu.ShowImageMargin = false;
            this.m_EffectContextMenu.Size = new System.Drawing.Size(251, 114);
            this.m_EffectContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.m_EffectContextMenu_Opening);
            // 
            // newEffectToolStripMenuItem
            // 
            this.newEffectToolStripMenuItem.Name = "newEffectToolStripMenuItem";
            this.newEffectToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.newEffectToolStripMenuItem.Text = "New Effect";
            this.newEffectToolStripMenuItem.Click += new System.EventHandler(this.newEffectToolStripMenuItem_Click);
            // 
            // loadEffectToolStripMenuItem1
            // 
            this.loadEffectToolStripMenuItem1.Name = "loadEffectToolStripMenuItem1";
            this.loadEffectToolStripMenuItem1.Size = new System.Drawing.Size(250, 22);
            this.loadEffectToolStripMenuItem1.Text = "Load Effect...";
            this.loadEffectToolStripMenuItem1.Click += new System.EventHandler(this.loadEffectToolStripMenuItem_Click);
            // 
            // removeEffect0ToolStripMenuItem
            // 
            this.removeEffect0ToolStripMenuItem.Name = "removeEffect0ToolStripMenuItem";
            this.removeEffect0ToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.removeEffect0ToolStripMenuItem.Text = "Remove Effect {0}";
            this.removeEffect0ToolStripMenuItem.Click += new System.EventHandler(this.removeEffect0ToolStripMenuItem_Click);
            // 
            // saveEffectAndChildrenToolStripMenuItem
            // 
            this.saveEffectAndChildrenToolStripMenuItem.Name = "saveEffectAndChildrenToolStripMenuItem";
            this.saveEffectAndChildrenToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.saveEffectAndChildrenToolStripMenuItem.Text = "Save Effect {0}, Emitters and PTX Rules";
            this.saveEffectAndChildrenToolStripMenuItem.Click += new System.EventHandler(this.SaveEffectAndChidlren_Click);
            // 
            // saveEffectToolStripMenuItem
            // 
            this.saveEffectToolStripMenuItem.Name = "saveEffectToolStripMenuItem";
            this.saveEffectToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.saveEffectToolStripMenuItem.Text = "Save Effect {0} only";
            this.saveEffectToolStripMenuItem.Click += new System.EventHandler(this.SaveEffect_Click);
            // 
            // m_UpdateTimer
            // 
            this.m_UpdateTimer.Tick += new System.EventHandler(this.m_UpdateTimer_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.m_effectsFilterBox);
            this.groupBox1.Controls.Add(this.splitContainer1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(10, 256);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(482, 593);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Effects";
            // 
            // m_effectsFilterBox
            // 
            this.m_effectsFilterBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_effectsFilterBox.Location = new System.Drawing.Point(41, 19);
            this.m_effectsFilterBox.Name = "m_effectsFilterBox";
            this.m_effectsFilterBox.Size = new System.Drawing.Size(435, 20);
            this.m_effectsFilterBox.TabIndex = 24;
            this.m_effectsFilterBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.m_effectsFilterBox_KeyPress);
            this.m_effectsFilterBox.Leave += new System.EventHandler(this.m_effectsFilterBox_Leave);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(6, 53);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.m_DataGridEffects);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.m_EffectWindow);
            this.splitContainer1.Size = new System.Drawing.Size(470, 534);
            this.splitContainer1.SplitterDistance = 193;
            this.splitContainer1.TabIndex = 27;
            // 
            // m_DataGridEffects
            // 
            this.m_DataGridEffects.AllowUserToAddRows = false;
            this.m_DataGridEffects.AllowUserToDeleteRows = false;
            this.m_DataGridEffects.AllowUserToResizeColumns = false;
            this.m_DataGridEffects.AllowUserToResizeRows = false;
            this.m_DataGridEffects.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.m_DataGridEffects.BackgroundColor = System.Drawing.SystemColors.Window;
            this.m_DataGridEffects.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.m_DataGridEffects.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.m_DataGridEffects.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.m_DataGridEffects.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.m_DataGridEffects.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.m_DataGridEffects.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.m_value,
            this.m_name});
            this.m_DataGridEffects.ContextMenuStrip = this.m_EffectContextMenu;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridEffects.DefaultCellStyle = dataGridViewCellStyle2;
            this.m_DataGridEffects.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_DataGridEffects.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.m_DataGridEffects.GridColor = System.Drawing.SystemColors.Window;
            this.m_DataGridEffects.Location = new System.Drawing.Point(0, 0);
            this.m_DataGridEffects.MultiSelect = false;
            this.m_DataGridEffects.Name = "m_DataGridEffects";
            this.m_DataGridEffects.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridEffects.RowHeadersVisible = false;
            this.m_DataGridEffects.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.m_DataGridEffects.RowTemplate.Height = 17;
            this.m_DataGridEffects.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridEffects.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_DataGridEffects.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.m_DataGridEffects.ShowCellToolTips = false;
            this.m_DataGridEffects.Size = new System.Drawing.Size(470, 193);
            this.m_DataGridEffects.TabIndex = 21;
            this.m_DataGridEffects.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_DataGridEffects_CellEndEdit);
            this.m_DataGridEffects.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.m_DataGridEffects_CellMouseDoubleClick);
            this.m_DataGridEffects.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SelectOnRightMouseButton_CellMouseDown);
            this.m_DataGridEffects.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.DataGrid_CellParsing);
            this.m_DataGridEffects.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.m_DataGridEffects_DataError);
            this.m_DataGridEffects.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGrid_KeyDown);
            // 
            // m_value
            // 
            this.m_value.DataPropertyName = "Value";
            this.m_value.HeaderText = "Value";
            this.m_value.Name = "m_value";
            this.m_value.Width = 59;
            // 
            // m_name
            // 
            this.m_name.DataPropertyName = "Name";
            this.m_name.HeaderText = "Name";
            this.m_name.Name = "m_name";
            this.m_name.ReadOnly = true;
            this.m_name.Visible = false;
            this.m_name.Width = 60;
            // 
            // m_EffectWindow
            // 
            this.m_EffectWindow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_EffectWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_EffectWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_EffectWindow.Location = new System.Drawing.Point(0, 0);
            this.m_EffectWindow.Margin = new System.Windows.Forms.Padding(0);
            this.m_EffectWindow.Name = "m_EffectWindow";
            this.m_EffectWindow.Size = new System.Drawing.Size(470, 337);
            this.m_EffectWindow.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Filter";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.m_ptxFilterBox);
            this.groupBox3.Controls.Add(this.splitContainer3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(1119, 256);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(490, 593);
            this.groupBox3.TabIndex = 30;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "PTX";
            // 
            // m_ptxFilterBox
            // 
            this.m_ptxFilterBox.Location = new System.Drawing.Point(53, 19);
            this.m_ptxFilterBox.Name = "m_ptxFilterBox";
            this.m_ptxFilterBox.Size = new System.Drawing.Size(428, 20);
            this.m_ptxFilterBox.TabIndex = 26;
            this.m_ptxFilterBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.m_ptxFilterBox_KeyPress);
            this.m_ptxFilterBox.Leave += new System.EventHandler(this.m_ptxFilterBox_Leave);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer3.Location = new System.Drawing.Point(6, 53);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.m_DataGridPtxList);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.m_PtxRuleWindow);
            this.splitContainer3.Size = new System.Drawing.Size(478, 534);
            this.splitContainer3.SplitterDistance = 192;
            this.splitContainer3.TabIndex = 27;
            // 
            // m_DataGridPtxList
            // 
            this.m_DataGridPtxList.AllowUserToAddRows = false;
            this.m_DataGridPtxList.AllowUserToDeleteRows = false;
            this.m_DataGridPtxList.AllowUserToResizeColumns = false;
            this.m_DataGridPtxList.AllowUserToResizeRows = false;
            this.m_DataGridPtxList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.m_DataGridPtxList.BackgroundColor = System.Drawing.SystemColors.Window;
            this.m_DataGridPtxList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.m_DataGridPtxList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.m_DataGridPtxList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.m_DataGridPtxList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.m_DataGridPtxList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.m_DataGridPtxList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.m_ptxValue,
            this.m_ptxName});
            this.m_DataGridPtxList.ContextMenuStrip = this.m_PtxContextMenu;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridPtxList.DefaultCellStyle = dataGridViewCellStyle4;
            this.m_DataGridPtxList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_DataGridPtxList.GridColor = System.Drawing.SystemColors.Window;
            this.m_DataGridPtxList.Location = new System.Drawing.Point(0, 0);
            this.m_DataGridPtxList.MultiSelect = false;
            this.m_DataGridPtxList.Name = "m_DataGridPtxList";
            this.m_DataGridPtxList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridPtxList.RowHeadersVisible = false;
            this.m_DataGridPtxList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.m_DataGridPtxList.RowTemplate.Height = 17;
            this.m_DataGridPtxList.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridPtxList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_DataGridPtxList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.m_DataGridPtxList.ShowCellToolTips = false;
            this.m_DataGridPtxList.Size = new System.Drawing.Size(478, 192);
            this.m_DataGridPtxList.TabIndex = 23;
            this.m_DataGridPtxList.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_DataGridPtxList_CellEndEdit);
            this.m_DataGridPtxList.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SelectOnRightMouseButton_CellMouseDown);
            this.m_DataGridPtxList.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.DataGrid_CellParsing);
            this.m_DataGridPtxList.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGrid_DataError);
            this.m_DataGridPtxList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGrid_KeyDown);
            // 
            // m_ptxValue
            // 
            this.m_ptxValue.DataPropertyName = "Value";
            this.m_ptxValue.HeaderText = "Value";
            this.m_ptxValue.Name = "m_ptxValue";
            this.m_ptxValue.Width = 59;
            // 
            // m_ptxName
            // 
            this.m_ptxName.DataPropertyName = "Name";
            this.m_ptxName.HeaderText = "Name";
            this.m_ptxName.Name = "m_ptxName";
            this.m_ptxName.ReadOnly = true;
            this.m_ptxName.Visible = false;
            this.m_ptxName.Width = 60;
            // 
            // m_PtxRuleWindow
            // 
            this.m_PtxRuleWindow.AutoScroll = true;
            this.m_PtxRuleWindow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_PtxRuleWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_PtxRuleWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_PtxRuleWindow.Location = new System.Drawing.Point(0, 0);
            this.m_PtxRuleWindow.Margin = new System.Windows.Forms.Padding(0);
            this.m_PtxRuleWindow.Name = "m_PtxRuleWindow";
            this.m_PtxRuleWindow.Size = new System.Drawing.Size(478, 338);
            this.m_PtxRuleWindow.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Filter";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.m_emittersFilterBox);
            this.groupBox2.Controls.Add(this.splitContainer2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(498, 256);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(615, 593);
            this.groupBox2.TabIndex = 29;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Emitters";
            // 
            // m_emittersFilterBox
            // 
            this.m_emittersFilterBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_emittersFilterBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_emittersFilterBox.Location = new System.Drawing.Point(45, 19);
            this.m_emittersFilterBox.Name = "m_emittersFilterBox";
            this.m_emittersFilterBox.Size = new System.Drawing.Size(563, 20);
            this.m_emittersFilterBox.TabIndex = 25;
            this.m_emittersFilterBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.m_emittersFilterBox_KeyPress);
            this.m_emittersFilterBox.Leave += new System.EventHandler(this.m_emittersFilterBox_Leave);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer2.Location = new System.Drawing.Point(6, 53);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.m_DataGridEmitters);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.m_EmitterWindow);
            this.splitContainer2.Size = new System.Drawing.Size(603, 534);
            this.splitContainer2.SplitterDistance = 193;
            this.splitContainer2.TabIndex = 26;
            // 
            // m_DataGridEmitters
            // 
            this.m_DataGridEmitters.AllowUserToAddRows = false;
            this.m_DataGridEmitters.AllowUserToDeleteRows = false;
            this.m_DataGridEmitters.AllowUserToResizeColumns = false;
            this.m_DataGridEmitters.AllowUserToResizeRows = false;
            this.m_DataGridEmitters.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.m_DataGridEmitters.BackgroundColor = System.Drawing.SystemColors.Window;
            this.m_DataGridEmitters.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.m_DataGridEmitters.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.m_DataGridEmitters.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.m_DataGridEmitters.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.m_DataGridEmitters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.m_DataGridEmitters.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.m_emitValue,
            this.m_emitName});
            this.m_DataGridEmitters.ContextMenuStrip = this.m_EmitterContextMenu;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridEmitters.DefaultCellStyle = dataGridViewCellStyle6;
            this.m_DataGridEmitters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_DataGridEmitters.GridColor = System.Drawing.SystemColors.Window;
            this.m_DataGridEmitters.Location = new System.Drawing.Point(0, 0);
            this.m_DataGridEmitters.MultiSelect = false;
            this.m_DataGridEmitters.Name = "m_DataGridEmitters";
            this.m_DataGridEmitters.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.m_DataGridEmitters.RowHeadersVisible = false;
            this.m_DataGridEmitters.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.m_DataGridEmitters.RowTemplate.Height = 17;
            this.m_DataGridEmitters.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridEmitters.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_DataGridEmitters.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.m_DataGridEmitters.ShowCellToolTips = false;
            this.m_DataGridEmitters.Size = new System.Drawing.Size(603, 193);
            this.m_DataGridEmitters.TabIndex = 22;
            this.m_DataGridEmitters.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_DataGridEmitters_CellEndEdit);
            this.m_DataGridEmitters.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SelectOnRightMouseButton_CellMouseDown);
            this.m_DataGridEmitters.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.DataGrid_CellParsing);
            this.m_DataGridEmitters.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGrid_DataError);
            this.m_DataGridEmitters.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGrid_KeyDown);
            // 
            // m_emitValue
            // 
            this.m_emitValue.DataPropertyName = "Value";
            this.m_emitValue.HeaderText = "Value";
            this.m_emitValue.Name = "m_emitValue";
            this.m_emitValue.Width = 59;
            // 
            // m_emitName
            // 
            this.m_emitName.DataPropertyName = "Name";
            this.m_emitName.HeaderText = "Name";
            this.m_emitName.Name = "m_emitName";
            this.m_emitName.ReadOnly = true;
            this.m_emitName.Visible = false;
            this.m_emitName.Width = 60;
            // 
            // m_EmitterWindow
            // 
            this.m_EmitterWindow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_EmitterWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_EmitterWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_EmitterWindow.Location = new System.Drawing.Point(0, 0);
            this.m_EmitterWindow.Margin = new System.Windows.Forms.Padding(0);
            this.m_EmitterWindow.Name = "m_EmitterWindow";
            this.m_EmitterWindow.Size = new System.Drawing.Size(603, 337);
            this.m_EmitterWindow.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Filter";
            // 
            // m_TimelineLabel
            // 
            this.m_TimelineLabel.AutoSize = true;
            this.m_TimelineLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_TimelineLabel.Location = new System.Drawing.Point(7, 32);
            this.m_TimelineLabel.Name = "m_TimelineLabel";
            this.m_TimelineLabel.Size = new System.Drawing.Size(115, 17);
            this.m_TimelineLabel.TabIndex = 20;
            this.m_TimelineLabel.Text = "Event Timeline";
            // 
            // m_MainMenu
            // 
            this.m_MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.m_WindowMenu});
            this.m_MainMenu.Location = new System.Drawing.Point(0, 0);
            this.m_MainMenu.Name = "m_MainMenu";
            this.m_MainMenu.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.m_MainMenu.Size = new System.Drawing.Size(1621, 24);
            this.m_MainMenu.TabIndex = 17;
            this.m_MainMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadEffectToolStripMenuItem,
            this.openEffectListToolStripMenuItem,
            this.toolStripSeparator2,
            this.saveRulesToolStripMenuItem,
            this.saveEffectListToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 24);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // loadEffectToolStripMenuItem
            // 
            this.loadEffectToolStripMenuItem.Name = "loadEffectToolStripMenuItem";
            this.loadEffectToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.loadEffectToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.loadEffectToolStripMenuItem.Text = "Load &Effect";
            this.loadEffectToolStripMenuItem.Click += new System.EventHandler(this.loadEffectToolStripMenuItem_Click);
            // 
            // openEffectListToolStripMenuItem
            // 
            this.openEffectListToolStripMenuItem.Name = "openEffectListToolStripMenuItem";
            this.openEffectListToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.O)));
            this.openEffectListToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.openEffectListToolStripMenuItem.Text = "&Open Effect List...";
            this.openEffectListToolStripMenuItem.Click += new System.EventHandler(this.openEffectListToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(238, 6);
            // 
            // saveRulesToolStripMenuItem
            // 
            this.saveRulesToolStripMenuItem.Name = "saveRulesToolStripMenuItem";
            this.saveRulesToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.saveRulesToolStripMenuItem.Text = "Save Rules...";
            this.saveRulesToolStripMenuItem.Click += new System.EventHandler(this.saveRulesToolStripMenuItem_Click);
            // 
            // saveEffectListToolStripMenuItem
            // 
            this.saveEffectListToolStripMenuItem.Name = "saveEffectListToolStripMenuItem";
            this.saveEffectListToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveEffectListToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.saveEffectListToolStripMenuItem.Text = "Save Effect List...";
            this.saveEffectListToolStripMenuItem.Click += new System.EventHandler(this.saveEffectListToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(238, 6);
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.ShortcutKeyDisplayString = "";
            this.exitMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitMenuItem.Size = new System.Drawing.Size(241, 22);
            this.exitMenuItem.Text = "E&xit";
            this.exitMenuItem.Click += new System.EventHandler(this.exitMenuItem_Click);
            // 
            // m_WindowMenu
            // 
            this.m_WindowMenu.Name = "m_WindowMenu";
            this.m_WindowMenu.Size = new System.Drawing.Size(63, 24);
            this.m_WindowMenu.Text = "&Window";
            this.m_WindowMenu.DropDownOpening += new System.EventHandler(this.m_WindowMenu_DropDownOpening);
            // 
            // m_TimeLine
            // 
            this.m_TimeLine.BackColor = System.Drawing.SystemColors.ControlLight;
            this.m_TimeLine.ChannelLabelWidth = 150;
            this.m_TimeLine.ChannelHeight = 20;
            this.m_TimeLine.Duration = 4F;
            this.m_TimeLine.Granularity = 4;
            this.m_TimeLine.Location = new System.Drawing.Point(-1, 32);
            this.m_TimeLine.MarkerPos = -1F;
            this.m_TimeLine.Name = "m_TimeLine";
            this.m_TimeLine.Right_Padding = 40;
            this.m_TimeLine.Size = new System.Drawing.Size(1610, 216);
            this.m_TimeLine.TabIndex = 19;
            this.m_TimeLine.OnSelectChannel += new ragPtxTimeLine.ptxTimeLine.ChannelEventDelegate(this.m_TimeLine_OnSelectChannel);
            this.m_TimeLine.OnDeleteChannel += new ragPtxTimeLine.ptxTimeLine.ChannelEventDelegate(this.m_TimeLine_OnDeleteChannel);
            this.m_TimeLine.OnChannelInfoChanged += new ragPtxTimeLine.ptxTimeLine.ChannelEventDelegate(this.m_TimeLine_OnChannelInfoChanged);
            this.m_TimeLine.OnReOrderChannels += new System.EventHandler(this.m_TimeLine_OnReOrderChannels);
            this.m_TimeLine.OnSoloMuteChanged += new System.EventHandler(this.m_TimeLine_OnSoloMuteChanged);
            // 
            // tcpComClient1
            // 
            this.tcpComClient1.Address = "10.0.23.33";
            this.tcpComClient1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tcpComClient1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tcpComClient1.Location = new System.Drawing.Point(58, 55);
            this.tcpComClient1.MaximumSize = new System.Drawing.Size(59, 24);
            this.tcpComClient1.MinimumSize = new System.Drawing.Size(59, 24);
            this.tcpComClient1.Name = "tcpComClient1";
            this.tcpComClient1.Port = 3333;
            this.tcpComClient1.Size = new System.Drawing.Size(59, 24);
            this.tcpComClient1.TabIndex = 4;
            this.tcpComClient1.UpdateInterval = 16;
            this.tcpComClient1.Visible = false;
            this.tcpComClient1.OnConnect += new ParticleEditor.tcpComClient.tcpComClientEventDelegate(this.tcpComClient1_OnConnect);
            this.tcpComClient1.OnDisconnect += new ParticleEditor.tcpComClient.tcpComClientEventDelegate(this.tcpComClient1_OnDisconnect);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1621, 861);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.m_TimelineLabel);
            this.Controls.Add(this.m_MainMenu);
            this.Controls.Add(this.tcpComClient1);
            this.Controls.Add(this.m_TimeLine);
            this.MainMenuStrip = this.m_MainMenu;
            this.Name = "main";
            this.Text = "RMPTFX Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.main_FormClosing);
            this.m_EmitterContextMenu.ResumeLayout(false);
            this.m_PtxContextMenu.ResumeLayout(false);
            this.m_EffectContextMenu.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridEffects)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridPtxList)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridEmitters)).EndInit();
            this.m_MainMenu.ResumeLayout(false);
            this.m_MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public tcpComClient tcpComClient1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MenuStrip m_MainMenu;
        private System.Windows.Forms.ToolStripMenuItem m_WindowMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadEffectToolStripMenuItem;
        public ragPtxTimeLine.ptxTimeLine m_TimeLine;
        private System.Windows.Forms.Label m_TimelineLabel;
        public EffectWindow m_EffectWindow;
        private System.Windows.Forms.ContextMenuStrip m_EffectContextMenu;
        private System.Windows.Forms.ToolStripMenuItem loadEffectToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem removeEffect0ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip m_EmitterContextMenu;
        private System.Windows.Forms.ToolStripMenuItem loadEmitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeEmitter0ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip m_PtxContextMenu;
        private System.Windows.Forms.ToolStripMenuItem loadPTXRuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removePTXRule0ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
        private System.Windows.Forms.DataGridView m_DataGridEffects;
        private System.Windows.Forms.DataGridView m_DataGridEmitters;
        private System.Windows.Forms.DataGridView m_DataGridPtxList;
        private System.Windows.Forms.ToolStripMenuItem openEffectListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveEffectListToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        public EmitterWindow m_EmitterWindow;
        private System.Windows.Forms.ToolStripMenuItem saveRulesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newPTXRuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spriteRuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modelRuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newEmitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newEffectToolStripMenuItem;
        public PtxWindow m_PtxRuleWindow;
        private System.Windows.Forms.ToolStripMenuItem saveEmitter0ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem savePTXRule0ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveEffectAndChildrenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveEffectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem behaviorRuleToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem clonePTXRuleToolStripMenuItem;
        private System.Windows.Forms.Timer m_UpdateTimer;
        private System.Windows.Forms.TextBox m_effectsFilterBox;
        private System.Windows.Forms.TextBox m_emittersFilterBox;
        private System.Windows.Forms.TextBox m_ptxFilterBox;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_value;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_emitValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_emitName;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_ptxValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_ptxName;



    }
}

