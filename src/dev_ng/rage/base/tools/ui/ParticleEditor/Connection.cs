using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.NetworkInformation;

namespace ParticleEditor
{
    public partial class Connection : ptxForm
    {
        private bool _ragConnected = false;

        public bool RagConnected
        {
            get { return _ragConnected; }
        }

        public Connection()
        {
            m_Name = "Connection";
            m_WindowID = "CONNECTIONWINDOW";
            InitializeComponent();

        }

        private void EstablishConnection(string ip)
        {
            button1.Enabled = false;
            button2.Enabled = false;
            main.m_MainForm.tcpComClient1.OnDisconnect += tcpComClient1_OnDisconnect;

            main.m_MainForm.tcpComClient1.Address = ip;
            main.m_MainForm.EstablishConnection();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            IPAddress addy;
            //First trying to parse it
            if(IPAddress.TryParse(m_IP.Text, out addy))
            {
                EstablishConnection(m_IP.Text);
            }
            else
            {
                try
                {
                    //see if it's using a hostname
                    IPAddress[] hostIPs;
                    hostIPs = Dns.GetHostAddresses(m_IP.Text);
                    if (hostIPs.Length > 0)
                    {
                        if (hostIPs.Length > 1)
                        {
                            String message = "More that one system has the same hostname, using IP: " + hostIPs[0].ToString();
                            System.Windows.Forms.MessageBox.Show(message, "RMPTFX Error: Multiple Systems/Same Hostname", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        EstablishConnection(hostIPs[0].ToString());
                    }
                }
                catch (Exception ex)
                {
                    //throw error if it cant find it
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Error: Invalid IP/Hostname", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void ragConnectionButton_Click(object sender, EventArgs e)
        {
            if (!_ragConnected)
            {
                if (main.m_MainForm.m_remoteConsole.Connect())
                {
                    ragConnectionButton.Text = "Disconnect";
                    _ragConnected = true;

                    main.m_MainForm.m_PlayControlForm.syncFromRAGCheckBox.Enabled = true;
                }
            }
            else
            {
                main.m_MainForm.m_PlayControlForm.syncFromRAGCheckBox.Enabled = false;
                main.m_MainForm.m_remoteConsole.DisconnectConsole();
                ragConnectionButton.Text = "Connect";
                _ragConnected = false;
            }
            
        }

        public void RagDisconnected()
        {
            ragConnectionButton.Text = "Connect";
            _ragConnected = false;
        }

        void tcpComClient1_OnDisconnect( tcpComClient obj )
        {
            button1.Enabled = true;
            button2.Enabled = true;
            main.m_MainForm.tcpComClient1.OnDisconnect -=   tcpComClient1_OnDisconnect;
        }

        protected override void LoadConfigData()
        {
            m_IP.Text = m_ConfigFile.IniReadValueDefault(m_WindowID, "IPADDRESS", "0.0.0.0");
            base.LoadConfigData();
        }
        protected override void SaveConfigData()
        {
            m_ConfigFile.IniWriteValue(m_WindowID, "IPADDRESS", m_IP.Text);
            base.SaveConfigData();
        }

        private IPAddress m_LocalHost;

        private void Connection_Load(object sender, EventArgs e)
        {
            IPAddress[] ips = System.Net.Dns.GetHostAddresses("");
            m_LocalHost = ips[0];
                      
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button2.Enabled = false;

            main.m_MainForm.tcpComClient1.OnDisconnect += tcpComClient1_OnDisconnect;
            main.m_MainForm.tcpComClient1.Address = m_LocalHost.ToString();
            System.Net.NetworkInformation.NetworkInterface[] nics = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();

            // walk over every NIC, looking for 'sane' values.
            foreach(NetworkInterface nic in nics)
            {
                // assume ethernet adapters, reject loopbacks.
                if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    // IPv4, since the game only uses this.
                    if (nic.Supports(NetworkInterfaceComponent.IPv4))
                    {
                        IPInterfaceProperties props = nic.GetIPProperties();

                        // look at the unicast addresses (p2p)
                        foreach (UnicastIPAddressInformation uca in props.UnicastAddresses)
                        {
                            // and 'regular' IPv4 here (not InterNetworkV6)
                            if (uca.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                            {
                                main.m_MainForm.tcpComClient1.Address = uca.Address.ToString();
                                // we found a good ethernet adapter here. Note that the game binds to a similar interface (ipv4, internet) so I doubt I need to worry about dialup or USB)
                                // multiple ethernet adapters will be weird, but it is likely to also confuse the game as well.
                                break;
                            }
                        }

                    }
                }
            }
            
            main.m_MainForm.EstablishConnection();

        }

        /// <summary>
        /// Make the default button the "Connect to console" button. Means that the user 
        /// just has to hit return to connect, rather than tabbing / clicking on the button.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void m_IP_Enter(object sender, EventArgs e)
        {
            AcceptButton = button1;
            button1.DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// Reset the accept button to null when the system caret leaves the text box.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void m_IP_Leave(object sender, EventArgs e)
        {
            AcceptButton = null;
        }
    }
}