using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
    public partial class PtxWindow : UserControl
    {
        public String m_CurrentPtxName = "";

        public PtxWindow()
        {
            InitializeComponent();

            m_DataGridPtxtKeyframeList.ColumnHeadersVisible = false;
            m_DataGridBiasProps.ColumnHeadersVisible = false;
            m_DataGridLinkSet.ColumnHeadersVisible = false;

			m_SpawnEffectB.Enabled = false;
			m_SpawnEffectBTime.Enabled = false;
			m_InheritLifeBCheck.Enabled = false;
			m_TrackPosBCheck.Enabled = false;
			m_TrackDirBCheck.Enabled = false;
			m_TrackNegDirBCheck.Enabled = false;
            m_SubSectionSpawnBOverrides.Visible = false;

			m_SpawnEffectA.Enabled = false;
			m_SpawnEffectATime.Enabled = false;
			m_InheritLifeACheck.Enabled = false;
			m_TrackPosACheck.Enabled = false;
			m_TrackDirACheck.Enabled = false;
			m_TrackNegDirACheck.Enabled = false;
            m_SubSectionSpawnAOverrides.Visible = false;
        }

        public void BuildBlendSetList()
        {
            m_ComboBlendMode.DataSource = DataMod.m_SDataMod.m_BlendSetList;
        }

        public void BuildDrawTypeList()
        {
            m_ComboDrawType.DataSource = DataMod.m_SDataMod.m_DrawTypeList;
        }

        public void BuildShaderList()
        {
            m_ComboShader.BeginUpdate();
            m_ComboShader.Items.Clear();
            foreach (String s in DataMod.m_SDataMod.m_ShaderList)
                m_ComboShader.Items.Add(s);
            m_ComboShader.EndUpdate();
            
            //Data source comes from the devil
            //m_ComboShader.DataSource = DataMod.m_SDataMod.m_ShaderList;
        }

        public void BuildSpawnEffectsList(ComboBox box)
        {
            if (box == null) return;
            String cursel = box.Text;

            box.SuspendLayout();
            box.Items.Clear();
            foreach (DataMod.dtaObject effect in DataMod.m_SDataMod.m_EffectRuleList)
                box.Items.Add(effect.m_Name);
            box.Text = cursel;
            box.ResumeLayout();
        }

        public void SetSpawnEffectA(string name, DataMod.dtaPtxRule dor)
        {
			if (name == null || name == String.Empty)
			{
				m_SpawnEffectACheck.Checked = false;
				m_SpawnEffectA.Text = String.Empty;
				m_SubSectionSpawnAOverrides.Visible = false;
				m_SpawnAOverrides.Overridable = null;
			}
			else
			{
				m_SpawnEffectACheck.Checked = true;
				m_SpawnEffectA.Text = name;
				m_SubSectionSpawnAOverrides.Visible = true;
				m_SpawnAOverrides.ValueChanged -= OnOverrideChanged; // remove any preexisting callback
				m_SpawnAOverrides.Overridable = dor.m_SpawnEffectAOverridables;
				m_SpawnAOverrides.ValueChanged += OnOverrideChanged; // add the new one
			}

            m_InheritLifeACheck.Checked = dor.m_SpawnEffectAInheritsLife;
			m_TrackPosACheck.Checked = dor.m_SpawnEffectATracksPos;
			m_TrackDirACheck.Checked = dor.m_SpawnEffectATracksDir;
			m_TrackNegDirACheck.Checked = dor.m_SpawnEffectATracksNegDir;
			m_SpawnEffectATime.SetValue = dor.m_SpawnEffectATime;
        }

        public void OnOverrideChanged(object sender, EventArgs e)
        {
            ChangedRule(m_CurrentPtxName);
        }

        public void SetSpawnEffectB(string name, DataMod.dtaPtxRule dor)
        {
			if (name == null || name == String.Empty)
			{
				m_SpawnEffectBCheck.Checked = false;
				m_SpawnEffectB.Text = String.Empty;
				m_SubSectionSpawnBOverrides.Visible = false;
				m_SpawnBOverrides.Overridable = null;
			}
			else
			{
				m_SpawnEffectBCheck.Checked = true;
				m_SpawnEffectB.Text = name;
				m_SubSectionSpawnBOverrides.Visible = true;
				m_SpawnBOverrides.ValueChanged -= OnOverrideChanged; // remove any preexisting callback
				m_SpawnBOverrides.Overridable = dor.m_SpawnEffectBOverridables;
				m_SpawnBOverrides.ValueChanged += OnOverrideChanged;
			}

			m_InheritLifeBCheck.Checked = dor.m_SpawnEffectBInheritsLife;
			m_TrackPosBCheck.Checked = dor.m_SpawnEffectBTracksPos;
			m_TrackDirBCheck.Checked = dor.m_SpawnEffectBTracksDir;
			m_TrackNegDirBCheck.Checked = dor.m_SpawnEffectBTracksNegDir;
			m_SpawnEffectBTime.SetValue = dor.m_SpawnEffectBTime;
        }

        private void DisplayBiasLinkSets()
        {
            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(m_CurrentPtxName);
            if (dor == null) return;
            m_DataGridLinkSet.SuspendLayout();
            String Selected_name = "";
            if (m_DataGridLinkSet.CurrentCell != null)
                Selected_name = m_DataGridLinkSet.CurrentCell.Value.ToString();

            m_DataGridLinkSet.Rows.Clear();
            foreach (DataMod.dtaPropBiasLink linkset in dor.m_BiasLinks)
            {
                int row = m_DataGridLinkSet.Rows.Add();
                m_DataGridLinkSet.Rows[row].Cells[0].Value = linkset.m_Name;
                m_DataGridLinkSet.Rows[row].Cells[0].Tag = linkset;
            }
            m_DataGridLinkSet.Sort(m_DataGridLinkSet.Columns[0], ListSortDirection.Ascending);
            m_DataGridLinkSet.ResumeLayout();

            if (Selected_name == "")
                if (m_DataGridLinkSet.Rows.Count > 0)
                    Selected_name = m_DataGridLinkSet.Rows[0].Cells[0].Value.ToString();

            SelectPropertyFromDataGrid(m_DataGridLinkSet, Selected_name);
            DisplayBiasLinkProperties(Selected_name);
        }
        private void DisplayBiasLinkProperties(String name)
        {
            m_DataGridBiasProps.SuspendLayout();
            String Selected_name = "";
            if (m_DataGridBiasProps.CurrentCell != null)
                Selected_name = m_DataGridBiasProps.CurrentCell.Value.ToString();

            m_DataGridBiasProps.Rows.Clear();
            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(m_CurrentPtxName);
            if (dor != null)
            {
                foreach (DataMod.dtaPropBiasLink linkset in dor.m_BiasLinks)
                {
                    if (linkset.m_Name == name)
                    {
                        foreach (DataMod.dtaPtxKeyframeProp keyprop in linkset.m_Props)
                        {
                            if (keyprop == null) continue;
                            int row = m_DataGridBiasProps.Rows.Add();
                            m_DataGridBiasProps.Rows[row].Cells[0].Value = keyprop.m_KeyframeSpec.DefnName;
                            m_DataGridBiasProps.Rows[row].Cells[1].Value = keyprop.m_InvertBiasLink;
                            m_DataGridBiasProps.Rows[row].Cells[0].Tag = keyprop;
                            m_DataGridBiasProps.Rows[row].Cells[1].Tag = keyprop;
                        }
                    }
                }
            }
            m_DataGridBiasProps.Sort(m_DataGridBiasProps.Columns[0], ListSortDirection.Ascending);
            m_DataGridBiasProps.ResumeLayout();
            if (Selected_name == "")
                if (m_DataGridBiasProps.Rows.Count > 0)
                    Selected_name = m_DataGridBiasProps.Rows[0].Cells[0].Value.ToString();
            SelectPropertyFromDataGrid(m_DataGridBiasProps, Selected_name);
        }

        public void SetDataFromGui()
        {
            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(m_CurrentPtxName);
            if (dor == null)
                return;

            //Renderstate info
            dor.m_RenderState.m_DepthTest = m_CheckDepthTest.Checked;
            dor.m_RenderState.m_DepthWrite = m_CheckDepthWrite.Checked;
            dor.m_RenderState.m_BlendSet = m_ComboBlendMode.SelectedIndex;
			dor.m_RenderState.m_CullMode = m_ComboCullMode.SelectedIndex;
//            dor.m_RenderState.m_DepthBias = m_DepthBias.Value;

            dor.m_SpawnEffectA = m_SpawnEffectACheck.Checked ? m_SpawnEffectA.SelectedItem as string : String.Empty;
            dor.m_SpawnEffectAInheritsLife = m_InheritLifeACheck.Checked;
			dor.m_SpawnEffectATracksPos = m_TrackPosACheck.Checked;
			dor.m_SpawnEffectATracksDir = m_TrackDirACheck.Checked;
			dor.m_SpawnEffectATracksNegDir = m_TrackNegDirACheck.Checked;
            dor.m_SpawnEffectATime = m_SpawnEffectATime.Value;

            dor.m_SpawnEffectB = m_SpawnEffectBCheck.Checked ? m_SpawnEffectB.SelectedItem as string : String.Empty;
			dor.m_SpawnEffectBInheritsLife = m_InheritLifeBCheck.Checked;
			dor.m_SpawnEffectBTracksPos = m_TrackPosBCheck.Checked;
			dor.m_SpawnEffectBTracksDir = m_TrackDirBCheck.Checked;
			dor.m_SpawnEffectBTracksNegDir = m_TrackNegDirBCheck.Checked;
            dor.m_SpawnEffectBTime = m_SpawnEffectBTime.Value;

			Int32 maxFrameId = PtxClipRegions.GetCurrTexNumFrames();
			if (m_TexFrameIdMax.Value > maxFrameId)
			{
				m_TexFrameIdMax.Value = maxFrameId;
			}

			if (m_TexFrameIdMin.Value > m_TexFrameIdMax.Value)
			{
				m_TexFrameIdMin.Value = m_TexFrameIdMax.Value;
			}
			dor.m_TexFrameIdMin = (int)m_TexFrameIdMin.Value;
			dor.m_TexFrameIdMax = (int)m_TexFrameIdMax.Value;

//            dor.m_PhysicalRange = m_PhysicalRange.Value;
//            dor.m_OnCollideKill = m_CheckOnCollideKill.Checked;
//            dor.m_ShowPhysics = m_CheckShowPhysics.Checked;
//            dor.m_StopVel = m_StopVel.Value;
//            dor.m_PercentPhysical = (byte)m_SlidePerPhysical.Value;
//            dor.m_PercentKill = (byte) m_SlidePerKill.Value;
//            dor.m_AllowPositionOverride = m_AllowPosOverride.Checked;
//            dor.m_AllowColorOverride = m_AllowColorOverride.Checked;
//            dor.m_RestrictNoiseX = m_RestrictNoiseX.Checked;
//            dor.m_RestrictNoiseY = m_RestrictNoiseY.Checked;
//            dor.m_RestrictNoiseZ = m_RestrictNoiseZ.Checked;
           
            //Determine type
//            if (dor.m_ClassName == "ptxsprite")
//                SetSpriteDataFromGui(dor as DataMod.dtaPtxSprite);
//            if (dor.m_ClassName == "ptxmodel")
//                SetModelDataFromGui(dor as DataMod.dtaPtxModel);
			if (dor.m_ClassName == "ptxparticlerule")
                SetBehaviorDataFromGui(dor as DataMod.dtaPtxRuleBehavior);

            //BiasLinks
        }

        private void OnBehaviorChanged(object sender, BehaviorWindowBase.BehaviorChangedEventArgs args)
        {
            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(args.Behavior.m_PtxRule.m_Name);
            if(dor != null)
            {
                if(dor is DataMod.dtaPtxRuleBehavior)
                {
                    ChangedRule(args.Behavior.m_PtxRule.m_Name);
                }
            }
        }
        private void OnBehaviorWindowClosed(object sender, BehaviorWindowBase.BehaviorChangedEventArgs args)
        {
            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(args.Behavior.m_PtxRule.m_Name);
            if (dor != null)
            {
                if (dor is DataMod.dtaPtxRuleBehavior)
                {
                    // Get effect rule name, needed for app for querying evolution data.
                    string effectRuleName = main.m_MainForm.m_EffectWindow.m_CurrentEffectName;

                    DataCon.m_SDataCon.SendValidateBehaviorEvent(args.Behavior, effectRuleName);
                    if (m_CurrentPtxName == args.Behavior.m_PtxRule.m_Name)
                        DataCon.m_SDataCon.RequestPtxRuleInfo(m_CurrentPtxName);
                }
            }
        }
        public void ChangedRule(String name)
        {
            if (name == "") return;
            DataCon.m_SDataCon.SendPtxRule(name);
        }

        public void EnableSubSections(bool b)
        {
            foreach (ptxSubSection sub in this.m_PanelSection.Controls)
                sub.Enabled = b;
        }

        public void DisplayRule(String name)
        {
            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(name);
            if (dor == null)
            {
                EnableSubSections(false);
                main.m_MainForm.ClearEvolutionData(m_DataGridPtxtKeyframeList);
                return;
            }

            EnableSubSections(true);
            m_CurrentPtxName = name;

            m_LabelHeader.Text = "PTX: " + name;

            //Renderstate info
            m_CheckDepthTest.Checked = dor.m_RenderState.m_DepthTest;
            m_CheckDepthWrite.Checked = dor.m_RenderState.m_DepthWrite;
            m_ComboBlendMode.SelectedIndex = dor.m_RenderState.m_BlendSet;
            m_ComboCullMode.SelectedIndex = dor.m_RenderState.m_CullMode;
//            m_DepthBias.SetValue = dor.m_RenderState.m_DepthBias;
			m_TexFrameIdMin.SetValue = dor.m_TexFrameIdMin;
			m_TexFrameIdMax.SetValue = dor.m_TexFrameIdMax;
 //           m_PhysicalRange.SetValue = dor.m_PhysicalRange;
 //           m_CheckOnCollideKill.Checked = dor.m_OnCollideKill;
 //           m_CheckShowPhysics.Checked = dor.m_ShowPhysics;
 //           m_StopVel.SetValue = dor.m_StopVel;
 //           m_SlidePerPhysical.SetValue = (float)dor.m_PercentPhysical;
 //           m_SlidePerKill.SetValue = (float)dor.m_PercentKill;
 //           m_AllowPosOverride.Checked = dor.m_AllowPositionOverride;
 //           m_AllowColorOverride.Checked = dor.m_AllowColorOverride;
 //           m_RestrictNoiseX.Checked = dor.m_RestrictNoiseX;
 //           m_RestrictNoiseY.Checked = dor.m_RestrictNoiseY;
 //           m_RestrictNoiseZ.Checked = dor.m_RestrictNoiseZ;

            m_SpawnEffectA.Items.Clear();
            m_SpawnEffectA.Items.Add(dor.m_SpawnEffectA);

            m_SpawnEffectB.Items.Clear();
            m_SpawnEffectB.Items.Add(dor.m_SpawnEffectB);

            // backwards order here so subsections are in correct order
            SetSpawnEffectB(dor.m_SpawnEffectB, dor);
            SetSpawnEffectA(dor.m_SpawnEffectA, dor);

            m_TimerProfile.Stop();
            //Keyframes
            m_DataGridPtxtKeyframeList.ContextMenuStrip = m_ContextMenuKeyProp;

            //Determine type
//            if (dor.m_ClassName == "ptxsprite")
//            {
//                main.m_MainForm.BuildKeyframeGrid(m_DataGridPtxtKeyframeList, dor.m_PropList);
//                DisplaySpriteRule(dor as DataMod.dtaPtxSprite);
//            }
//            if (dor.m_ClassName == "ptxmodel")
//            {
//                main.m_MainForm.BuildKeyframeGrid(m_DataGridPtxtKeyframeList, dor.m_PropList);
//                DisplayModelRule(dor as DataMod.dtaPtxModel);
//            }
            //this is the Ptxrule behavior type
			if (dor.m_ClassName == "ptxparticlerule")
                DisplayBehaviorRule(dor as DataMod.dtaPtxRuleBehavior);

            //Bias Links
            DisplayBiasLinkSets();

        }

        private void DisplayBehaviorRule(DataMod.dtaPtxRuleBehavior dob)
        {
            if (dob == null)
                return;
            m_TimerProfile.Start();
            m_SubSectionKeyframes.SectionName = "Behaviors";
            m_ComboDrawType.Visible = true;
            m_LabelDrawType.Visible = true;
            m_DataGridPtxtKeyframeList.ContextMenuStrip = m_ContextBehavior;
            m_ComboDrawType.SelectedItem = dob.m_DrawType as string;
            m_SubSectionPhysical.Visible = false;
            ptxSubSectionBiasLinks.Visible = true;

			UpdateTechniqueDescView(dob.m_Shader.m_diffuseMode, dob.m_Shader.m_projMode, dob.m_Shader.m_isLit, dob.m_Shader.m_isSoft, dob.m_Shader.m_isScreenSpace, dob.m_Shader.m_isRefract, dob.m_Shader.m_isNormalSpec);

//            groupBox4.Enabled = false;

            foreach (Control ctr in m_SubSectionSprite.Controls)
            {
//				ctr.Enabled = false;
				ctr.Enabled = true;
            }
//            m_ComboShader.Enabled = true;
//            m_ComboShaderTech.Enabled = true;
//            label2.Enabled = true;
//            label3.Enabled = true;

            foreach (Control ctr in m_SubSectionModel.Controls)
            {
                ctr.Enabled = false;
            }
            m_ListModels.Enabled = true;
            label7.Enabled = true;
            label6.Enabled = true;
            m_AllowColorOverride.Enabled = false;

 //           if (dob.m_SortType > 0)
 //           {
 //               m_CheckSort.Checked = true;
                m_ComboSortType.Enabled = true;
				m_ComboSortType.SelectedIndex = dob.m_SortType;// -1;
 //           }
 //           else
 //           {
 //               m_CheckSort.Checked = false;
 //               m_ComboSortType.Enabled = false;
 //           }

            //These should probably be encapsulated into some data driven type thing
            if (dob.m_DrawType == "mesh")
            {
                m_SubSectionSprite.Visible = false;
                m_SubSectionShader.Visible = false;
                m_SubSectionModel.Visible = true;

                // Get the model list
                m_ListModels.SuspendLayout();
                m_ListModels.Items.Clear();
                for (int i = 0; i < dob.m_Drawables.Count; i++)
                    m_ListModels.Items.Add(dob.m_Drawables[i]);
                m_ListModels.ResumeLayout();

            }
            else //if (dob.m_DrawType == "sprite")
            {
                m_SubSectionSprite.Visible = true;
                m_SubSectionShader.Visible = true;
                m_SubSectionModel.Visible = false;
            }

            //Behaviors
            String selkf = "";
            DataMod.dtaPtxBehavior behg = GetSelectedBehaviorFromDataGrid(m_DataGridPtxtKeyframeList);
            if (behg != null)
                selkf = behg.m_BehaviorName;
            m_DataGridPtxtKeyframeList.SuspendLayout();
            m_DataGridPtxtKeyframeList.Rows.Clear();
            int profileidx = m_DataGridPtxtKeyframeList.Columns.Count - 1;

            //Add all the properties and bold the ones included in this rule
            foreach (DataMod.dtaBehaviorDefinition def in DataMod.m_SDataMod.m_UpdateBehaviorDefList)
            {
                if(def.m_Vars.Count>0)
                    if ((def.m_DrawType == "all") || (def.m_DrawType == m_ComboDrawType.Text.ToLower()))
                    {
                        int idx = m_DataGridPtxtKeyframeList.Rows.Add();
                        m_DataGridPtxtKeyframeList.Rows[idx].Cells[0].Value = def.m_BehaviorName;

                        int behid = dob.m_BehaviorList.GetBehaviorIndex(def.m_BehaviorName);
                        if (behid >= 0)
                        {
                            DataMod.dtaPtxBehavior beh = dob.m_BehaviorList.m_Behaviors[behid];
                            m_DataGridPtxtKeyframeList.Rows[idx].Cells[0].Style.Font = new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Bold);
                            m_DataGridPtxtKeyframeList.Rows[idx].Cells[0].Tag = beh;
                            //This will dynamically update any behaviors which are currently visible
                            BehaviorWindowBase.UpdateBehavior(beh);
                        }
                        else
                        {
                            m_DataGridPtxtKeyframeList.Rows[idx].Cells[0].Style.Font = new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Regular);
                            m_DataGridPtxtKeyframeList.Rows[idx].Cells[0].Tag = def;
                        }
                        m_DataGridPtxtKeyframeList.Rows[idx].Cells[profileidx].Style.ForeColor = Color.Green;
                        m_DataGridPtxtKeyframeList.Rows[idx].Cells[profileidx].Value = "";
                    }
            }
            m_DataGridPtxtKeyframeList.Columns[7].Visible = false;
            m_DataGridPtxtKeyframeList.Sort(m_DataGridPtxtKeyframeList.Columns[0], ListSortDirection.Ascending);
            m_DataGridPtxtKeyframeList.ResumeLayout();
            SelectPropertyFromDataGrid(m_DataGridPtxtKeyframeList, selkf);

            //Shader data
            m_ComboShader.Text = dob.m_Shader.m_ShaderName;
            GenerateShaderInterface(dob.m_Shader);

            //Evolution
            DisplayEvolutionData(main.m_MainForm.m_EffectWindow.m_CurrentEvent);
        }

        public void UpdateProfiling()
        {
            DataMod.dtaPtxRuleBehavior dob = DataMod.GetPtxRule(m_CurrentPtxName) as DataMod.dtaPtxRuleBehavior;
            if (dob == null)
                return;
            DataMod.dtaProfilingPtx pro = DataMod.m_SDataMod.m_ProfilePtx;
            if (pro.m_PtxRuleName != dob.m_Name)
                return;

            //Total behavior update time
            float totalBehaviorUpdate = 0.0f;
            for (int i = 0; i < pro.m_PtxBehaviorCPUUpdateTimes.Count; i++)
                totalBehaviorUpdate += pro.m_PtxBehaviorCPUUpdateTimes[i];
            
            int profileIndx = m_DataGridPtxtKeyframeList.Columns.Count - 1;

            //Find the row
            for(int r=0;r<m_DataGridPtxtKeyframeList.Rows.Count;r++)
            {
                String behaviorUIName = m_DataGridPtxtKeyframeList.Rows[r].Cells[0].Value.ToString();
                int bpid = pro.GetBehaviorIndexFromUIName(behaviorUIName);
                if (bpid >= 0)
                {
                    //Color Lerps from blue to red based on %of total time
                    float percentTotalUpdateTime = ((totalBehaviorUpdate == 0.0f) ? 0.0f : (100.0f * pro.m_PtxBehaviorCPUUpdateTimes[bpid] / totalBehaviorUpdate));
                    byte redbyte = (byte)(((100.0f - percentTotalUpdateTime) / 100.0f) * (float)(Color.Blue.R - Color.Red.R));
                    byte greenbyte = (byte)(((100.0f - percentTotalUpdateTime) / 100.0f) * (float)(Color.Blue.G - Color.Red.G));
                    byte bluebyte = (byte)(((100.0f - percentTotalUpdateTime) / 100.0f) * (float)(Color.Blue.B - Color.Red.B));
                    Color textColor = Color.FromArgb(redbyte, greenbyte, bluebyte);

                    m_DataGridPtxtKeyframeList.Rows[r].Cells[profileIndx].Style.ForeColor = textColor;
                    m_DataGridPtxtKeyframeList.Rows[r].Cells[profileIndx].Value = pro.m_PtxBehaviorCPUUpdateTimes[bpid].ToString("0.00") + "ms" + " (" + percentTotalUpdateTime.ToString("0.") + "%)";
                }
                else
                {
                    m_DataGridPtxtKeyframeList.Rows[r].Cells[profileIndx].Style.ForeColor = Color.Green;
                    m_DataGridPtxtKeyframeList.Rows[r].Cells[profileIndx].Value = "";
                }
            }
            
        }

        private void SetBehaviorDataFromGui(DataMod.dtaPtxRuleBehavior dob)
        {
            dob.m_DrawType = m_ComboDrawType.SelectedItem as string;

            dob.m_SortType = 0;
//            if(m_CheckSort.Checked)
            {
                dob.m_SortType = (byte)(m_ComboSortType.SelectedIndex);
            }
            
//            if (m_ComboShaderTech.SelectedItem != null)
//                dob.m_Shader.m_SelectedTechnique = m_ComboShaderTech.SelectedItem.ToString();

			dob.m_Shader.m_isLit = m_checkBoxIsLit.Checked;
			dob.m_Shader.m_isSoft = m_checkBoxIsSoft.Checked;
			dob.m_Shader.m_isScreenSpace = m_checkBoxIsScreenSpace.Checked;
			dob.m_Shader.m_isRefract = m_checkBoxIsRefract.Checked;
			dob.m_Shader.m_isNormalSpec = m_checkBoxIsNormalSpec.Checked;
			dob.m_Shader.m_diffuseMode = m_comboDiffuseMode.SelectedIndex;
			dob.m_Shader.m_projMode = m_comboProjMode.SelectedIndex;
        }

        private void DisplaySpriteRule(DataMod.dtaPtxSprite dos)
        {
            if (dos == null)
                return;
            m_SubSectionKeyframes.SectionName = "Keyframes";
            m_ComboDrawType.Visible = false;
            m_LabelDrawType.Visible = false;
//            m_CheckSort.Checked = dos.m_DrawSorted;
//            m_CheckSort.Enabled = true;

            m_SubSectionModel.Visible = false;
            m_SubSectionShader.Visible = true;
            ptxSubSectionBiasLinks.Visible = false;

            m_SubSectionPhysical.Visible = true;

//            groupBox4.Enabled = true;
            m_SubSectionSprite.Visible = true;
            foreach (Control ctr in m_SubSectionSprite.Controls)
            {
                ctr.Enabled = true;
            }
            m_AllowColorOverride.Enabled = true;

            m_ComboSortType.SelectedIndex = 0;
            if (dos.m_SortRandom)
                m_ComboSortType.SelectedIndex = 1;
			m_ComboSortType.Enabled = true;// m_CheckSort.Checked;

            //Shader data
            m_ComboShader.Text = dos.m_Shader.m_ShaderName;
            GenerateShaderInterface(dos.m_Shader);

            //Evolution
            DisplayEvolutionData(main.m_MainForm.m_EffectWindow.m_CurrentEvent);
        }
//       private void SetSpriteDataFromGui(DataMod.dtaPtxSprite dos)
//       {
//           dos.m_DrawSorted = m_CheckSort.Checked;
//
//            dos.m_SortRandom = false;
//            if (m_ComboSortType.SelectedIndex == 1)
//                dos.m_SortRandom = true;
//
//            if(m_ComboShaderTech.SelectedItem != null)
//                dos.m_Shader.m_SelectedTechnique = m_ComboShaderTech.SelectedItem.ToString();
//        }

        private void DisplayModelRule(DataMod.dtaPtxModel dom)
        {
            if (dom == null)
                return;
            m_SubSectionKeyframes.SectionName = "Keyframes";
            m_ComboDrawType.Visible = false;
            m_LabelDrawType.Visible = false;

//            m_CheckSort.Checked = false;
//            m_CheckSort.Enabled = false;


            m_SubSectionSprite.Visible = false;
            m_SubSectionShader.Visible = false;
            m_SubSectionModel.Visible = true;
            m_SubSectionPhysical.Visible = true;
            ptxSubSectionBiasLinks.Visible = false;
//            groupBox4.Enabled = true;

            foreach (Control ctr in m_SubSectionModel.Controls)
            {
                ctr.Enabled = true;
            }

            m_AllowColorOverride.Enabled = true;

            m_ListModels.SuspendLayout();
            m_ListModels.Items.Clear();
            for (int i = 0; i < dom.m_Drawables.Count; i++)
                m_ListModels.Items.Add(dom.m_Drawables[i]);
            m_ListModels.ResumeLayout();


            ////Keyframes
            //m_DataGridPtxtKeyframeList.SuspendLayout();
            //m_DataGridPtxtKeyframeList.Rows.Clear();

            //foreach (DataMod.KeyframeSpec key in dom.m_Keyframes.Keyframes)
            //{
            //    int idx = m_DataGridPtxtKeyframeList.Rows.Add();
            //    m_DataGridPtxtKeyframeList.Rows[idx].Cells[0].Value = key.Name;
            //    m_DataGridPtxtKeyframeList.Rows[idx].Cells[0].Tag = key;
            //}
            //m_DataGridPtxtKeyframeList.Sort(m_DataGridPtxtKeyframeList.Columns[0], ListSortDirection.Ascending);
            //m_DataGridPtxtKeyframeList.ResumeLayout();
            //SelectPropertyFromDataGrid(m_DataGridPtxtKeyframeList, "");

            //Evolution
            DisplayEvolutionData(main.m_MainForm.m_EffectWindow.m_CurrentEvent);

        }
//        private void SetModelDataFromGui(DataMod.dtaPtxModel dom)
//        {
//            dom.m_DrawSorted = false;
//        }


        protected void DisplayEvolutionData(DataMod.dtaEvent doev)
        {
            if (doev == null) return;
            if (doev is DataMod.dtaEventEmitter)
                if ((doev as DataMod.dtaEventEmitter).m_PtxRuleName == m_CurrentPtxName)
                {
                    DataMod.dtaPtxRule dor = DataMod.GetPtxRule(m_CurrentPtxName);
                    if (dor == null) return;
                    if(dor is DataMod.dtaPtxRuleBehavior)
                        main.m_MainForm.DisplayEvolutionForBehaviors(m_DataGridPtxtKeyframeList, doev.m_EvoList);
                    else
                        main.m_MainForm.DisplayEvolutionData(m_DataGridPtxtKeyframeList, doev.m_EvoList);
                }
        }

        private DataMod.dtaBehaviorVar GetBehaviorKeyframeVarFromName(DataMod.dtaPtxRuleBehavior dor, String varname)
        {
            // For each behavior...
            foreach (DataMod.dtaPtxBehavior behav in dor.m_BehaviorList.m_Behaviors)
            {
                // For each behavior's variable...
                foreach( DataMod.dtaBehaviorVar var in behav.m_Vars )
                {
                    // If the variable is a "keyframe" type, and matches the name...
                    if (var.m_Type == "keyframe" && var.m_Name == varname )
                    {
                        // Return the var.
                        return var;
                    }
                }
            }
            return null;
        }

        private void AddBiasLinkSet(String name)
        {
            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(m_CurrentPtxName);
            if (dor == null) return;
            DataMod.dtaPropBiasLink link = new DataMod.dtaPropBiasLink();
            link.m_Name = name;
            dor.m_BiasLinks.Add(link);
        }
        private void RemoveBiasLinkSet(String name)
        {
            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(m_CurrentPtxName);
            foreach (DataMod.dtaPropBiasLink linkset in dor.m_BiasLinks)
            {
                if (linkset.m_Name == name)
                {
                    dor.m_BiasLinks.Remove(linkset);
                    DisplayBiasLinkSets();
                    return;
                }
            }
        }
        private void RemoveBiasProp(String setname,DataMod.dtaPtxKeyframeProp prop)
        {
            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(m_CurrentPtxName);
            foreach (DataMod.dtaPropBiasLink linkset in dor.m_BiasLinks)
            {
                if (linkset.m_Name == setname)
                {
                    foreach (DataMod.dtaPtxKeyframeProp keyProp in linkset.m_Props)
                        if (keyProp.m_PropID == prop.m_PropID)
                        {
                            linkset.m_Props.Remove(keyProp);
                            DisplayBiasLinkSets();
                            return;
                        }
                }
            }
        }
        private void AddBiasProp(String setname, DataMod.dtaPtxKeyframeProp prop)
        {
            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(m_CurrentPtxName);

            foreach (DataMod.dtaPropBiasLink linkset in dor.m_BiasLinks)
            {
                if (linkset.m_Name == setname)
                {
                    foreach (DataMod.dtaPtxKeyframeProp keyprop in linkset.m_Props)
                    {
                        if (keyprop == null) continue;
                        if (keyprop.m_PropID == prop.m_PropID)
                        {
                            return;
                        }
                    }
                    linkset.m_Props.Add(prop);
                    DisplayBiasLinkSets();
                    return;
                }
            }
        }

        private String GenerateUniqueBiasSetName()
        {
            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(m_CurrentPtxName);
            if (dor == null) return "";
            int cnt = 0;
            do
            {
                String name = "Bias Link Set_" + cnt.ToString("00");
                cnt++;
                bool unique=true;
                foreach(DataMod.dtaPropBiasLink linkset in dor.m_BiasLinks)
                {
                    if(linkset.m_Name == name)
                    {
                        unique = false;
                        break;
                    }
                }
                if (unique)
                    return name;
            }
            while (true);

        }

		public void UpdateTechniqueDescView(int diffuseMode, int projMode, bool isLit, bool isSoft, bool isScreenSpace, bool isRefract, bool isNormalSpec)
		{
			m_checkBoxIsLit.Checked = isLit;
			m_checkBoxIsSoft.Checked = isSoft;
			m_checkBoxIsScreenSpace.Checked = isScreenSpace;
			m_checkBoxIsRefract.Checked = isRefract;
			m_checkBoxIsNormalSpec.Checked = isNormalSpec;
			m_comboDiffuseMode.SelectedIndex = diffuseMode;
			m_comboProjMode.SelectedIndex = projMode;
		}

        private void GenerateShaderInterface(DataMod.dtaPtxShader shader)
        {
            //Fill out techniques
//            m_ComboShaderTech.SuspendLdaayout();
//            m_ComboShaderTech.Items.Clear();
//            for (int i = 0; i < shader.m_Techniques.Count; i++)
//                m_ComboShaderTech.Items.Add(shader.m_Techniques[i]);
//            m_ComboShaderTech.ResumeLayout();
//            m_ComboShaderTech.Text = shader.m_SelectedTechnique;

			m_checkBoxIsLit.Checked = shader.m_isLit;
			m_checkBoxIsSoft.Checked = shader.m_isSoft;
			m_checkBoxIsScreenSpace.Checked = shader.m_isScreenSpace;
			m_checkBoxIsRefract.Checked = shader.m_isRefract;
			m_checkBoxIsNormalSpec.Checked = shader.m_isNormalSpec;
			m_comboDiffuseMode.SelectedIndex = shader.m_diffuseMode;
			m_comboProjMode.SelectedIndex = shader.m_projMode;

            m_SubSectionShader.HeaderText = "("+shader.m_ShaderName+")";
            m_ShaderPanel.SuspendLayout();
            m_ShaderPanel.Controls.Clear();

            int yl = 0;
            foreach(DataMod.ptxShaderVar var in shader.m_ShaderVars)
            {
                UserControl ge = null;
                switch(var.GetTypeName())
                {
                    case "texture":
                    {
                        DataMod.ptxShaderVar_Texture tex = var as DataMod.ptxShaderVar_Texture;
                        ptxShaderVars.varTexture ui = new ptxShaderVars.varTexture();
                        ui.OnSelectFile += new ptxShaderVars.varTexture.varTextureEventDelegate(ShaderVarTexture_OnSelectFile);
                        ui.OnResetFile += new ptxShaderVars.varTexture.varTextureEventDelegate(ShaderVarTexture_OnSelectFile);
                        ui.m_DataObject = var;
                        ui.HeaderText = tex.m_VarName;
                        ui.FileName = tex.m_TextureName;
                        //ui.BrowseDirectory =
                        //ui.FilePath =
                        ge = ui;
						if (tex.m_VarName == "DiffuseTex2")
						{
							PtxClipRegions.SetCurrTexIndex(tex.m_TextureName);
						}
                        break;
                    }
                    case "float":
                    {
                        DataMod.ptxShaderVar_Float tex = var as DataMod.ptxShaderVar_Float;
                        ptxShaderVars.varFloat ui = new ptxShaderVars.varFloat();
                        ui.OnValueChanged += new ptxShaderVars.varFloat.varFloatEventDelegate(ShaderVarFloat_OnValueChanged);
                        ui.m_DataObject = var;
                        ui.HeaderText = tex.m_VarName;
                        ui.MinValue = var.m_UIMin;
                        ui.MaxValue = var.m_UIMax;
                        ui.Increment = var.m_UIStep;
                        ui.Value = tex.m_Float;
                        ge = ui;
                        break;
                    }
                    case "float2":
                    {
                        DataMod.ptxShaderVar_Float2 tex = var as DataMod.ptxShaderVar_Float2;
                        ptxShaderVars.varFloat2 ui = new ptxShaderVars.varFloat2();
                        ui.OnValueChanged += new ptxShaderVars.varFloat2.varFloat2EventDelegate(ShaderVarFloat2_OnValueChanged);
                        ui.m_DataObject = var;
                        ui.HeaderText = tex.m_VarName;
                        ui.MinValue = var.m_UIMin;
                        ui.MaxValue = var.m_UIMax;
                        ui.Increment = var.m_UIStep;
                        ui.Value = new ptxShaderVars.varFloat2.float2(tex.m_FloatX,tex.m_FloatY);

                        ge = ui;
                        break;
                    }
                    case "float3":
                    {
                        DataMod.ptxShaderVar_Float3 tex = var as DataMod.ptxShaderVar_Float3;
                        ptxShaderVars.varFloat3 ui = new ptxShaderVars.varFloat3();
                        ui.OnValueChanged += new ptxShaderVars.varFloat3.varFloat3EventDelegate(ShaderVarFloat3_OnValueChanged);
                        ui.m_DataObject = var;
                        ui.HeaderText = tex.m_VarName;
                        ui.MinValue = var.m_UIMin;
                        ui.MaxValue = var.m_UIMax;
                        ui.Increment = var.m_UIStep;
                        ui.Value = new ptxShaderVars.varFloat3.float3(tex.m_FloatX, tex.m_FloatY,tex.m_FloatZ);
                        ge = ui;
                        break;
                    }
                    case "float4":
                    {
                        DataMod.ptxShaderVar_Float4 tex = var as DataMod.ptxShaderVar_Float4;
                        ptxShaderVars.varFloat4 ui = new ptxShaderVars.varFloat4();
                        ui.OnValueChanged += new ptxShaderVars.varFloat4.varFloat4EventDelegate(ShaderVarFloat4_OnValueChanged);
                        ui.m_DataObject = var;
                        ui.HeaderText = tex.m_VarName;
                        ui.MinValue = var.m_UIMin;
                        ui.MaxValue = var.m_UIMax;
                        ui.Increment = var.m_UIStep;
                        ui.Value = new ptxShaderVars.varFloat4.float4(tex.m_FloatX, tex.m_FloatY, tex.m_FloatZ, tex.m_FloatW);
                        ge = ui;
                        break;
                    }    
                    case "keyframe":
                        {
                            DataMod.ptxShaderVar_Keyframe kf = var as DataMod.ptxShaderVar_Keyframe;
                            ptxShaderVars.varKeyframe ui = new ptxShaderVars.varKeyframe();
                            ui.m_DataObject = var;
                            ui.HeaderText = kf.m_VarName;
							ui.m_RuleName = kf.m_RuleName;
							ui.m_RuleType = kf.m_RuleType;
							ui.m_PropertyId = kf.m_PropertyId;
                            ui.ShowEditWindow += delegate
                            {
                                KeyframeEditorFormBase.ShowForm("", ui.HeaderText, ui.m_RuleName, ui.m_RuleType, ui.m_PropertyId, -1, -1);
                            };
                            ge = ui;
                            break;
                        }
                    default:
                        // do nothing?
                        break;
                }

                if (ge != null)
                {
                    ge.Left = 0;
                    ge.Top = yl;
                    m_ShaderPanel.Controls.Add(ge);
                    yl += ge.Height;
                    m_ShaderPanel.Height = yl;
                }
            }
            m_ShaderPanel.ResumeLayout();
            m_SubSectionShader.SetHeight(m_ShaderPanel.Height + m_ShaderPanel.Top);
			
			SetDataFromGui();
        }

        private void m_CheckBox_Click(object sender, EventArgs e)
        {
			m_ComboSortType.Enabled = true;// m_CheckSort.Checked;
            SetDataFromGui();
            ChangedRule(m_CurrentPtxName);
        }

        private void m_ComboBlendMode_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetDataFromGui();
            ChangedRule(m_CurrentPtxName);
        }

        private void m_Slider_OnValueChanged(ptxSlider slider)
        {
            SetDataFromGui();
            ChangedRule(m_CurrentPtxName);
        }

        private void m_CheckAnimTecture_Click(object sender, EventArgs e)
        {
        }

        private void m_ContextModelList_Opening(object sender, CancelEventArgs e)
        {
            m_AddModelMenuItem.Visible = true;
            m_DelModelMenuItem.Visible = false;

            if (m_ListModels.SelectedItem == null)
                m_AddModelMenuItem.Visible = true;
            else
            {
                m_DelModelMenuItem.Text = "Remove Model: '" + m_ListModels.SelectedItem.ToString()+"'";
                m_DelModelMenuItem.Visible = true;
            }
        }

        private void m_AddModelMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog diag = new OpenFileDialog();
            diag.InitialDirectory = main.m_MainForm.m_LastModelDir;
            diag.Filter = "Rage Models (*.type)|*.type";
            diag.Multiselect = false;
            diag.CheckFileExists = true;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                String path = System.IO.Path.GetDirectoryName(diag.FileNames[0]);
                String filename = System.IO.Path.GetFileNameWithoutExtension(diag.FileNames[0]);
                String foldername = System.IO.Path.GetDirectoryName(diag.FileNames[0]);
                int numPathCharsToStrip = foldername.LastIndexOf('\\') + 1;
                foldername = foldername.Substring(numPathCharsToStrip, foldername.Length - numPathCharsToStrip);

                //Get the parent directory
                int idx = path.IndexOf(filename);
                if (idx >= 0)
                    path = path.Remove(idx);

                main.m_MainForm.m_LastModelDir = path; 
                
                if (path.EndsWith("\\models\\"))
                    path = path.Replace("\\models\\", "");

                for (int i = 0; i < diag.FileNames.Length; i++)
                {
                    // Get file name.
                    filename = System.IO.Path.GetFileNameWithoutExtension(diag.FileNames[i]);

                    // Make one string: foldername/filename (".type" extension implied)
                    String folderPlusFile = foldername + '/' + filename;

                    DataCon.m_SDataCon.SendPtxRuleAddModelEvent(m_CurrentPtxName,path,folderPlusFile);
                }
            }
        }

        private void m_DelModelMenuItem_Click(object sender, EventArgs e)
        {
            if (m_ListModels.SelectedItem == null)
                return;
            DataCon.m_SDataCon.SendPtxRuleRemoveModelEvent(m_CurrentPtxName, m_ListModels.SelectedItem.ToString());
        }

        private void ShowKeyframe(object sender, EventArgs e)
        {
            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(m_CurrentPtxName);
            if(dor is DataMod.dtaPtxRuleBehavior)
            {
                BehaviorWindowBase.BehaviorWindowEvent evtChanged = new BehaviorWindowBase.BehaviorWindowEvent(this.OnBehaviorChanged);
                BehaviorWindowBase.BehaviorWindowEvent evtClosed = new BehaviorWindowBase.BehaviorWindowEvent(this.OnBehaviorWindowClosed);
                DataMod.dtaPtxBehavior beh = GetSelectedBehaviorFromDataGrid(sender as DataGridView);
                if (beh == null)
                {
                    DataMod.dtaBehaviorDefinition def = GetSelectedBehaviorDefinitionFromDataGrid(sender as DataGridView);
                    if(def == null)
                        return;

                    DataCon.RequestResponse onReplyCallback = delegate(tcpByteBuffer resp)
                    {
                        bool success = resp.Read_bool();

                        if (success)
                        {
                           SelectNameFromDataGrid(sender as DataGridView, def.m_BehaviorName);
                            ShowKeyframe(sender, e);
                            return;
                        }
                        else
                        {
                            return;
                        }
                    };
                    DataCon.m_SDataCon.SendAddBehaviorPtxRuleEvent(m_CurrentPtxName, def.m_BehaviorName, onReplyCallback);
                }
                else
                {
                    BehaviorWindowBase.ShowBehavior(beh, evtChanged,evtClosed);
                }
                return;
            }
            
            DataGridView grid = sender as DataGridView;
            if (grid.CurrentCell == null) return;
            int cdx = grid.CurrentCell.ColumnIndex;


            String desc = "PtxRule: " + m_CurrentPtxName;
            if ((cdx > 0) && (cdx <= main.m_MainForm.m_EffectWindow.m_CurrentEvent.m_EvoList.m_Evolutions.Count))
            {
                String evoname = (main.m_MainForm.m_EffectWindow.m_CurrentEvent.m_EvoList.m_Evolutions[cdx - 1] as DataMod.dtaEvolution).m_Name;
                desc += ": Evolution (" + evoname + ")";
            }
            KeyframeEditorFormBase.ShowKeyframe(GetSelectedKeyframeCellFromDataGrid(sender as DataGridView),desc+":");
        }

        private void ShaderVarTexture_OnSelectFile(ptxShaderVars.varTexture var)
        {
            DataMod.ptxShaderVar_Texture tex = var.m_DataObject as DataMod.ptxShaderVar_Texture;
            tex.m_TexturePath = var.FilePath;
            tex.m_TextureName = var.FileName;
            ChangedRule(m_CurrentPtxName);
			PtxClipRegions.SetCurrTexIndex(tex.m_TextureName);
			SetDataFromGui();
        }

        private DataMod.KeyframeSpec GetSelectedKeyframeFromDataGrid(DataGridView grid)
        {
            if (grid.CurrentRow == null) return null;
            DataMod.KeyframeSpec key = grid.CurrentRow.Cells[0].Tag as DataMod.KeyframeSpec;
            return key;

        }
        private DataMod.dtaPtxBehavior GetSelectedBehaviorFromDataGrid(DataGridView grid)
        {
            if (grid.CurrentCell == null) return null;
            DataMod.dtaPtxBehavior behavior = grid.CurrentCell.Tag as DataMod.dtaPtxBehavior;
            return behavior;
        }
        private DataMod.dtaBehaviorDefinition GetSelectedBehaviorDefinitionFromDataGrid(DataGridView grid)
        {
            if (grid.CurrentRow== null) return null;
            DataMod.dtaBehaviorDefinition def = grid.CurrentRow.Cells[0].Tag as DataMod.dtaBehaviorDefinition;
            return def;
        }

        private DataMod.KeyframeSpec GetSelectedKeyframeCellFromDataGrid(DataGridView grid)
        {
            if (grid.CurrentCell == null) return null;
            DataMod.KeyframeSpec key = grid.CurrentCell.Tag as DataMod.KeyframeSpec;
            return key;

        }

        private void SelectNameFromDataGrid(DataGridView grid, String propName)
        {
            if (propName == "")
            {
                grid.CurrentCell = null;
                return;
            }

            foreach (DataGridViewRow row in grid.Rows)
            {
                if (row.Cells[0].Value.ToString() == propName)
                {
                    grid.CurrentCell = row.Cells[0];
                    return;
                }
            }
            grid.CurrentCell = null;

        }
        private void SelectPropertyFromDataGrid(DataGridView grid, String propName)
        {
            if (propName == "")
            {
                grid.CurrentCell = null;
                return;
            }

            foreach (DataGridViewRow row in grid.Rows)
            {
                if (row.Cells[0].Value.ToString() == propName)
                {
                    grid.CurrentCell = row.Cells[0];
                    return;
                }
            }
            grid.CurrentCell = null;
        }
        private void ShaderVarFloat_OnValueChanged(ptxShaderVars.varFloat var)
        {
            DataMod.ptxShaderVar_Float tex = var.m_DataObject as DataMod.ptxShaderVar_Float;
            tex.m_Float = var.Value;
            ChangedRule(m_CurrentPtxName);
        }
        private void ShaderVarFloat2_OnValueChanged(ptxShaderVars.varFloat2 var)
        {
            DataMod.ptxShaderVar_Float2 tex = var.m_DataObject as DataMod.ptxShaderVar_Float2;
            tex.m_FloatX = var.Value.x;
            tex.m_FloatY = var.Value.y;
            ChangedRule(m_CurrentPtxName);
        }
        private void ShaderVarFloat3_OnValueChanged(ptxShaderVars.varFloat3 var)
        {
            DataMod.ptxShaderVar_Float3 tex = var.m_DataObject as DataMod.ptxShaderVar_Float3;
            tex.m_FloatX = var.Value.x;
            tex.m_FloatY = var.Value.y;
            tex.m_FloatZ = var.Value.z;
            ChangedRule(m_CurrentPtxName);
        }
        private void ShaderVarFloat4_OnValueChanged(ptxShaderVars.varFloat4 var)
        {
            DataMod.ptxShaderVar_Float4 tex = var.m_DataObject as DataMod.ptxShaderVar_Float4;
            tex.m_FloatX = var.Value.x;
            tex.m_FloatY = var.Value.y;
            tex.m_FloatZ = var.Value.z;
            tex.m_FloatW = var.Value.w;
            ChangedRule(m_CurrentPtxName);
        }

        private void m_ComboShader_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (m_ComboShader.SelectedItem == null)
                return;
            DataCon.m_SDataCon.SendShaderChangedEvent(m_CurrentPtxName, m_ComboShader.SelectedItem.ToString());
        }
        private void SelectOnRightMouseButton_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                DataGridView grid = sender as DataGridView;
                grid.CurrentCell = grid.Rows[e.RowIndex].Cells[e.ColumnIndex];
            }

        }

        private void m_ContextMenuKeyProps_Opening(object sender, CancelEventArgs e)
        {
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;
            if (doe == null) { e.Cancel = true; return; }
            
            DataMod.dtaPtxKeyframeProp keyProp = main.m_MainForm.GetSelectedKeyframePropFromDataGrid(m_DataGridPtxtKeyframeList);
            if (keyProp == null) { e.Cancel = true; return; }

            if (doe is DataMod.dtaEventEmitter)
            {
                if ((doe as DataMod.dtaEventEmitter).m_PtxRuleName == m_CurrentPtxName)
                    main.m_MainForm.GenerateKeyPropEvoMenu("PTX", m_ContextMenuKeyProp, doe.m_EvoList, keyProp, new EventHandler(KeyProp_Menu_AddEvo_Click), new EventHandler(KeyProp_Menu_DelEvo_Click));
                else
                    main.m_MainForm.GenerateKeyPropMenu("PTX", m_ContextMenuKeyProp,keyProp);
            }
                
            e.Cancel = false;
        }
        private void KeyProp_Menu_AddEvo_Click(object sender, EventArgs e)
        {
            String effectName = main.m_MainForm.m_EffectWindow.m_CurrentEffectName;
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;

            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            String evoname = item.Tag as String;
            DataMod.dtaPtxKeyframeProp keyprop = m_ContextMenuKeyProp.Tag as DataMod.dtaPtxKeyframeProp;
            if (keyprop == null) return;
            DataCon.m_SDataCon.SendAddPropEvolutionEvent(effectName, evoname,doe.m_Index, keyprop);
        }

        private void KeyProp_Menu_DelEvo_Click(object sender, EventArgs e)
        {
            String effectName = main.m_MainForm.m_EffectWindow.m_CurrentEffectName;
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;

            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            String evoname = item.Tag as String;
            DataMod.dtaPtxKeyframeProp keyprop = m_ContextMenuKeyProp.Tag as DataMod.dtaPtxKeyframeProp;
            if (keyprop == null) return;
            DataCon.m_SDataCon.SendDelPropEvolutionEvent(effectName, evoname,doe.m_Index, keyprop);
        }

        private void DataGridAutoSelectValidCell(object sender, EventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid.CurrentCell == null) return;
            if (grid.CurrentCell.Value == null)
                grid.CurrentCell = grid.Rows[grid.CurrentCell.RowIndex].Cells[0];
        }

        private void m_SpawnEffectA_CheckedChanged(object sender, EventArgs e)
        {
            m_SpawnEffectA.Enabled = m_SpawnEffectACheck.Checked;
            m_SpawnEffectATime.Enabled = m_SpawnEffectACheck.Checked;
			m_InheritLifeACheck.Enabled = m_SpawnEffectACheck.Checked;
			m_TrackPosACheck.Enabled = m_SpawnEffectACheck.Checked;
			m_TrackDirACheck.Enabled = m_SpawnEffectACheck.Checked;
			m_TrackNegDirACheck.Enabled = m_SpawnEffectACheck.Checked;
            m_SubSectionSpawnAOverrides.Visible = m_SpawnEffectACheck.Checked;
        }

        private void m_SpawnEffectB_CheckedChanged(object sender, EventArgs e)
        {
            m_SpawnEffectB.Enabled = m_SpawnEffectBCheck.Checked;
            m_SpawnEffectBTime.Enabled = m_SpawnEffectBCheck.Checked;
			m_InheritLifeBCheck.Enabled = m_SpawnEffectBCheck.Checked;
			m_TrackPosBCheck.Enabled = m_SpawnEffectBCheck.Checked;
			m_TrackDirBCheck.Enabled = m_SpawnEffectBCheck.Checked;
			m_TrackNegDirBCheck.Enabled = m_SpawnEffectBCheck.Checked;
            m_SubSectionSpawnBOverrides.Visible = m_SpawnEffectBCheck.Checked;
        }

        private void SpawnEffect_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetDataFromGui();

            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(m_CurrentPtxName);
            SetSpawnEffectB(dor.m_SpawnEffectB, dor);
            SetSpawnEffectA(dor.m_SpawnEffectA, dor);

            ChangedRule(m_CurrentPtxName);
        }

        private void BuildSpawnEffect_DropDown(object sender, EventArgs e)
        {
            BuildSpawnEffectsList(sender as ComboBox);
        }

        private void m_DataGridLinkSet_SelectionChanged(object sender, EventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid.CurrentCell == null) return;
            if (grid.CurrentCell.Value == null) return;
            DisplayBiasLinkProperties(grid.CurrentCell.Value.ToString());
        }

        private void m_ContexBiasSet_Opening(object sender, CancelEventArgs e)
        {
            m_ContexRemoveSet.Visible = false;
            if(m_DataGridLinkSet.CurrentCell != null)
            {
                m_ContexRemoveSet.Text = "Remove Set [" + m_DataGridLinkSet.CurrentCell.Value.ToString() + "]";
                m_ContexRemoveSet.Visible = true;
            }
        }

        private void m_ContexRemoveSet_Click(object sender, EventArgs e)
        {
            if (m_DataGridLinkSet.CurrentCell == null) return;
            String name = m_DataGridLinkSet.CurrentCell.Value.ToString();
            RemoveBiasLinkSet(name);
            SetDataFromGui();
            ChangedRule(m_CurrentPtxName);
        }

        private void m_ContextRemoveBiasProp_Click(object sender, EventArgs e)
        {
            if (m_DataGridBiasProps.CurrentCell == null) return;
            if (m_DataGridLinkSet.CurrentCell == null) return;

            DataMod.dtaPtxKeyframeProp keyprop = m_DataGridBiasProps.CurrentCell.Tag as DataMod.dtaPtxKeyframeProp;
            String setname = m_DataGridLinkSet.CurrentCell.Value.ToString();

            RemoveBiasProp(setname, keyprop);
            SetDataFromGui();
            ChangedRule(m_CurrentPtxName);
        }

        private void m_ContextBiasProp_Opening(object sender, CancelEventArgs e)
        {
            m_ContextRemoveBiasProp.Visible = false;
            if (m_DataGridBiasProps.CurrentCell != null)
            {
                m_ContextRemoveBiasProp.Text = "Remove Property [" + m_DataGridBiasProps.CurrentCell.Value.ToString() + "]";
                m_ContextRemoveBiasProp.Visible = true;
            }
        }

        private void m_ContextAddBiasProp_DropDownOpening(object sender, EventArgs e)
        {
            m_ContextAddBiasProp.DropDownItems.Clear();
            //Add any prop not in another set
            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(m_CurrentPtxName);

            // New rule type (covers ptxRuleBehavior).
            DataMod.dtaPtxRuleBehavior dorBehav = dor as DataMod.dtaPtxRuleBehavior;
            if (dorBehav != null)
            {
                dorBehav.m_BehaviorList.m_Behaviors.Sort(DataMod.dtaPtxBehavior.Compare);
                // For each behavior in the behavior list...
                foreach( DataMod.dtaPtxBehavior behav in dorBehav.m_BehaviorList.m_Behaviors )
                {
                    ToolStripMenuItem behaviorMenuItem = null;

                    // For each variable in the behavior...
                    foreach (DataMod.dtaBehaviorVar var in behav.m_Vars)
                    {
                        // If the variable is a keyframe...
                        if( var.m_Type == "keyframe")
                        {
                            // HACK: don't add "max" properties
                            if (var.m_Name.Contains(" Max"))
                                continue;

                            // Don't add properties to the drop-down if they are already included in the active bias link
                            bool add = true;
                            foreach (DataMod.dtaPropBiasLink link in dorBehav.m_BiasLinks)
                            {
                                if (link.ContainsID(var.m_KeyframeProp.m_PropID))
                                {
                                    add = false;
                                    break;
                                }
                            }

                            // Else, add the keyframe name to the bias link drop-down.
                            if (add)
                            {
                                if (behaviorMenuItem == null)
                                    behaviorMenuItem = m_ContextAddBiasProp.DropDownItems.Add(behav.m_BehaviorName) as ToolStripMenuItem;

                                ToolStripItem menuitem = behaviorMenuItem.DropDownItems.Add(var.m_KeyframeProp.m_KeyframeSpec.DefnName);
                                menuitem.Click += new EventHandler(BiasPropAdd_Click);
                                menuitem.Tag = var.m_KeyframeProp;
                            }
                        }
                    }
                }
            }
            // Old rule type (covers ptxSprite/ptxModel).
            else
            {
                foreach (DataMod.dtaPtxKeyframeProp prop in dor.m_PropList.m_KeyframeProps)
                {
                    // HACK: don't add "max" properties
                    if (prop.m_KeyframeSpec.DefnName.Contains(" Max"))
                        continue;
                    bool add = true;
                    // Don't add properties to the drop-down if they are already included in the active bias link
                    foreach (DataMod.dtaPropBiasLink link in dor.m_BiasLinks)
                    {
                        if (link.ContainsID(prop.m_PropID))
                        {
                            add = false;
                            break;
                        }
                    }
                    if (add)
                    {
                        ToolStripItem menuitem = m_ContextAddBiasProp.DropDownItems.Add(prop.m_KeyframeSpec.DefnName);
                        menuitem.Tag = prop;
                        menuitem.Click += new EventHandler(BiasPropAdd_Click);
                    }
                }
            }
        }
        
        private void BiasPropAdd_Click(object sender, EventArgs e)
        {
            if (m_DataGridLinkSet.CurrentCell == null) return;
            ToolStripItem item = sender as ToolStripMenuItem;
            DataMod.dtaPtxKeyframeProp keyProp = item.Tag as DataMod.dtaPtxKeyframeProp;
            if (keyProp == null) return;
            
            String linksetname = m_DataGridLinkSet.CurrentCell.Value.ToString();

            AddBiasProp(linksetname, keyProp);
            SetDataFromGui();
            ChangedRule(m_CurrentPtxName);
        }

        private void m_ContexAddSet_Click(object sender, EventArgs e)
        {
            String name = GenerateUniqueBiasSetName();
            AddBiasLinkSet(name);
            DisplayBiasLinkSets();
            SetDataFromGui();
            ChangedRule(m_CurrentPtxName);
        }

        private void m_DataGridLinkSet_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (m_DataGridLinkSet.CurrentCell == null) return;
            String newname = m_DataGridLinkSet.CurrentCell.Value.ToString();
            DataMod.dtaPropBiasLink LinkSetData = m_DataGridLinkSet.CurrentCell.Tag as DataMod.dtaPropBiasLink;
            String oldname = LinkSetData.m_Name;

            DataMod.dtaPtxRule dor = DataMod.GetPtxRule(m_CurrentPtxName);
            if (dor == null) return;
            
            //go thru other linksets and see if the name is unique
            foreach(DataMod.dtaPropBiasLink linkset in dor.m_BiasLinks)
                if(linkset.m_Name == newname)
                {
                    m_DataGridLinkSet.CurrentCell.Value = oldname;
                    return;
                }
            if (LinkSetData == null) return;
            LinkSetData.m_Name = newname;
            SetDataFromGui();
            ChangedRule(m_CurrentPtxName);
        }

        private void m_DataGridLinkSet_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (m_DataGridLinkSet.CurrentCell == null) return;
        }

        private void m_DataGridBiasProps_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (m_DataGridBiasProps.CurrentCell == null) return;
            DataGridViewCell cell = m_DataGridBiasProps.CurrentCell;
            DataMod.guiPropEventData guievt = new DataMod.guiPropEventData();
            if (cell.ColumnIndex == 1)
            {
                // Old ptxSprite/ptxModel way.
                DataMod.dtaPtxKeyframeProp prop = cell.Tag as DataMod.dtaPtxKeyframeProp;
                if (prop != null)
                {
                    DataGridViewCheckBoxCell chbox = cell as DataGridViewCheckBoxCell;
                    if (prop.m_InvertBiasLink.ToString() != (string)chbox.Value)
                    {
                        prop.m_InvertBiasLink = ((string)chbox.Value == "true");
                        SetDataFromGui();
                        ChangedRule(m_CurrentPtxName);
                    }
                }

                // New ptxRuleBehavior way.
                else
                {
                    DataMod.dtaBehaviorVar var = cell.Tag as DataMod.dtaBehaviorVar;
                    DataGridViewCheckBoxCell chbox = cell as DataGridViewCheckBoxCell;
                    if (var.m_KeyframeProp.m_InvertBiasLink.ToString() != (string)chbox.Value)
                    {
                        var.m_KeyframeProp.m_InvertBiasLink = ((string)chbox.Value == "true");
                        SetDataFromGui();
                        ChangedRule(m_CurrentPtxName);
                    }
                }
            }
            else
            {
                main.m_MainForm.PropertyDataGrid_CellValueChanged(sender, guievt, e);
                if (guievt.InfoChanged)
                    main.m_MainForm.m_EffectWindow.ChangedEffect(main.m_MainForm.m_EffectWindow.m_CurrentEffectName);
            }
        }

        private void m_DataGridBiasProps_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (m_DataGridBiasProps.CurrentCell == null) return;
            DataGridViewCell cell = m_DataGridBiasProps.CurrentCell;
            if (cell.ColumnIndex == 1)
            {
                m_DataGridBiasProps.CommitEdit(DataGridViewDataErrorContexts.Commit);
                return;
            }
            main.m_MainForm.PropertyDataGrid_CurrentCellDirtyStateChanged(sender, e);
        }

//        private void m_DepthBias_OnValueChanged(ptxSlider slider)
//        {
//            SetDataFromGui();
//            ChangedRule(m_CurrentPtxName);
//        }

        private void m_ComboShaderTech_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetDataFromGui();
            ChangedRule(m_CurrentPtxName);
        }

        private void m_DataGridPtxtKeyframeList_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            main.m_MainForm.PropertyDataGrid_CurrentCellDirtyStateChanged(sender, e);
        }

        private void m_DataGridPtxtKeyframeList_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataMod.guiPropEventData guievt = new DataMod.guiPropEventData();
            if (main.m_MainForm != null)
                main.m_MainForm.PropertyDataGrid_CellValueChanged(sender, guievt, e);
            if (guievt.InfoChanged)
            {
                if (main.m_MainForm != null)
                    main.m_MainForm.m_EffectWindow.ChangedEffect(main.m_MainForm.m_EffectWindow.m_CurrentEffectName);
            }
        }

        private void m_ComboSortType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetDataFromGui();
            ChangedRule(m_CurrentPtxName);
        }

        private void m_ComboDrawType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetDataFromGui();
            ChangedRule(m_CurrentPtxName);
            DataCon.m_SDataCon.RequestPtxRuleInfo(m_CurrentPtxName);
        }

        private void m_ContextBehavior_Opening(object sender, CancelEventArgs e)
        {
            DataMod.dtaPtxBehavior beh = GetSelectedBehaviorFromDataGrid(m_DataGridPtxtKeyframeList);
            if (beh == null)
            {
                e.Cancel = true;
                return;
            }
            m_MenuClearBehavior.Text = "Reset Behavior: " + beh.m_BehaviorName;
            m_MenuClearBehavior.Tag = beh;
            e.Cancel = false;
        }

        private void m_MenuClearBehavior_Click(object sender, EventArgs e)
        {
            if(sender is ToolStripMenuItem)
            {
                ToolStripMenuItem item = sender as ToolStripMenuItem;
                if (item.Tag == null)
                    return;
                DataMod.dtaPtxBehavior beh = item.Tag as DataMod.dtaPtxBehavior;
                if (beh == null)
                    return;

                //if the window is open then close it
                BehaviorWindowBase.CloseBehaviorWindow(beh);

                DataCon.m_SDataCon.SendDelBehaviorPtxRuleEvent(m_CurrentPtxName, beh.m_BehaviorName);
                item.Tag = null;
            }
        }

        private void m_TimerProfile_Tick(object sender, EventArgs e)
        {
            if (m_CurrentPtxName != "")
                DataCon.m_SDataCon.RequestProfilingPtx(m_CurrentPtxName);
            //Ensure it runs forever
            m_TimerProfile.Stop();
            m_TimerProfile.Start();
		}

		private void m_TexFrameIdMin_OnValueChanged(ptxSlider slider)
		{
			SetDataFromGui();
			ChangedRule(m_CurrentPtxName);
		}

		private void m_TexFrameIdMax_OnValueChanged(ptxSlider slider)
		{
			SetDataFromGui();
			ChangedRule(m_CurrentPtxName);
		}

		private void Technique_OnValueChanged(ptxSlider slider)
		{
			SetDataFromGui();
			ChangedRule(m_CurrentPtxName);
		}

		private void Technique_OnValueChanged(object sender, EventArgs e)
		{
			SetDataFromGui();
			ChangedRule(m_CurrentPtxName);
		}

		private void m_TrackDirBCheck_CheckedChanged(object sender, EventArgs e)
		{

		}

        private void m_checkBoxIsNormalSpec_CheckedChanged(object sender, EventArgs e)
        {
            bool isReset = false;
            //gui element on screen
            if (m_checkBoxIsNormalSpec.Checked == false)
            {
                foreach (UserControl var in m_ShaderPanel.Controls)
                {
                    if (var.Name == "varTexture")
                    {
                        ptxShaderVars.varTexture textureUI = (ptxShaderVars.varTexture)var;
                        if (textureUI.HeaderText == "NormalSpecMap")
                        {
                            textureUI.FileName = "";
                            isReset = true;
                        }
                    }
                }
            }

            if (isReset)
            {
                SetDataFromGui();
                ChangedRule(m_CurrentPtxName);
            }
        }

        private void m_checkBoxIsRefract_CheckedChanged(object sender, EventArgs e)
        {
            bool isReset = false;
            //gui element on screen
            if (m_checkBoxIsRefract.Checked == false)
            {
                foreach (UserControl var in m_ShaderPanel.Controls)
                {
                    if (var.Name == "varTexture")
                    {
                        ptxShaderVars.varTexture textureUI = (ptxShaderVars.varTexture)var;
                        if (textureUI.HeaderText == "RefractionMap")
                        {
                            textureUI.FileName = "";
                            isReset = true;
                        }
                    }
                }
            }

            if (isReset)
            {
                SetDataFromGui();
                ChangedRule(m_CurrentPtxName);
            }
        }

    }

	public class PtxClipRegionData
	{
		public PtxClipRegionData()
		{
			m_numTexCols = 0;
			m_numTexRows = 0;
		}

		public String m_textureName = "";
		public Int32 m_numTexCols;
		public Int32 m_numTexRows;
	}

	public class PtxClipRegions
	{
		public static void SetCurrTexIndex(String textureName)
		{
			for (Int32 i = 0; i < m_clipRegionData.Count; i++)
			{
				PtxClipRegionData clipRegionData = ((PtxClipRegionData)m_clipRegionData[i]);
				if (clipRegionData.m_textureName == textureName)
				{
					m_currTexIndex = i;
					break;
				}
			}
		}

		public static Int32 GetCurrTexNumFrames()
		{
			PtxClipRegionData clipRegionData = ((PtxClipRegionData)m_clipRegionData[m_currTexIndex]);
			return (clipRegionData.m_numTexCols * clipRegionData.m_numTexRows) - 1;
		}

		public static ArrayList m_clipRegionData = new ArrayList();
		public static Int32 m_currTexIndex = 0;
	}

}

