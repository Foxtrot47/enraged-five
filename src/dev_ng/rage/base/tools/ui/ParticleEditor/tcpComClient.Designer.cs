namespace ParticleEditor
{
    partial class tcpComClient
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_LabelType = new System.Windows.Forms.Label();
            this.m_UpdateTimer = new System.Windows.Forms.Timer(this.components);
            this.m_PingTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // m_LabelType
            // 
            this.m_LabelType.AutoSize = true;
            this.m_LabelType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelType.Location = new System.Drawing.Point(6, 5);
            this.m_LabelType.Name = "m_LabelType";
            this.m_LabelType.Size = new System.Drawing.Size(39, 13);
            this.m_LabelType.TabIndex = 0;
            this.m_LabelType.Text = "Client";
            // 
            // m_UpdateTimer
            // 
            this.m_UpdateTimer.Interval = 16;
            this.m_UpdateTimer.Tick += new System.EventHandler(this.Update);
            // 
            // m_PingTimer
            // 
            this.m_PingTimer.Interval = 1000;
            this.m_PingTimer.Tick += new System.EventHandler(this.m_PingTimer_Tick);
            // 
            // tcpComClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.m_LabelType);
            this.MaximumSize = new System.Drawing.Size(59, 24);
            this.MinimumSize = new System.Drawing.Size(59, 24);
            this.Name = "tcpComClient";
            this.Size = new System.Drawing.Size(57, 22);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_LabelType;
        private System.Windows.Forms.Timer m_UpdateTimer;
        private System.Windows.Forms.Timer m_PingTimer;
    }
}
