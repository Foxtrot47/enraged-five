namespace ParticleEditor
{
    partial class Playcontrol
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.m_ComboAnimateType = new System.Windows.Forms.ComboBox();
            this.m_SliderAnimateSpeed = new ParticleEditor.ptxSlider();
            this.m_CheckAnimateEffect = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.m_SliderAnimateDist = new ParticleEditor.ptxSlider();
            this.panel3 = new System.Windows.Forms.Panel();
            this.syncFromRAGCheckBox = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.m_WPosZ = new ParticleEditor.ptxSlider();
            this.m_WPosY = new ParticleEditor.ptxSlider();
            this.m_WPosX = new ParticleEditor.ptxSlider();
            this.m_RadioSWorld = new System.Windows.Forms.RadioButton();
            this.m_RadioSCam = new System.Windows.Forms.RadioButton();
            this.m_CamStartDist = new ParticleEditor.ptxSlider();
            this.label3 = new System.Windows.Forms.Label();
            this.m_CheckRepeat = new System.Windows.Forms.CheckBox();
            this.m_RepeatTime = new ParticleEditor.ptxSlider();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.m_ButtonResetEffect = new System.Windows.Forms.Button();
            this.m_ButtonPlay = new System.Windows.Forms.Button();
            this.m_ButtonReset = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.m_PauseButton = new System.Windows.Forms.Button();
            this.m_TimeScalar = new ParticleEditor.ptxSlider();
            this.m_LabelPlaybackOptions = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ragSyncTimer = new System.Windows.Forms.Timer(this.components);
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.m_ComboAnimateType);
            this.panel4.Controls.Add(this.m_SliderAnimateSpeed);
            this.panel4.Controls.Add(this.m_CheckAnimateEffect);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.m_SliderAnimateDist);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(670, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(229, 140);
            this.panel4.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(107, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Distance";
            // 
            // m_ComboAnimateType
            // 
            this.m_ComboAnimateType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboAnimateType.FormattingEnabled = true;
            this.m_ComboAnimateType.Items.AddRange(new object[] {
            "X-Y-Plane",
            "X-Z-Plane",
            "X-Y+Rotation",
            "X-Z+Rotation",
            "RotateZ",
            "RotateY",
            "PingPong",
            "PingPong2"});
            this.m_ComboAnimateType.Location = new System.Drawing.Point(110, 17);
            this.m_ComboAnimateType.Name = "m_ComboAnimateType";
            this.m_ComboAnimateType.Size = new System.Drawing.Size(104, 21);
            this.m_ComboAnimateType.TabIndex = 10;
            this.m_ComboAnimateType.SelectionChangeCommitted += new System.EventHandler(this.m_ComboAnimateType_SelectionChangeCommitted);
            // 
            // m_SliderAnimateSpeed
            // 
            this.m_SliderAnimateSpeed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SliderAnimateSpeed.Decimals = 3;
            this.m_SliderAnimateSpeed.Increment = 0.01F;
            this.m_SliderAnimateSpeed.Location = new System.Drawing.Point(46, 44);
            this.m_SliderAnimateSpeed.MaxValue = 10000F;
            this.m_SliderAnimateSpeed.MinValue = -10000F;
            this.m_SliderAnimateSpeed.Multiline = true;
            this.m_SliderAnimateSpeed.Name = "m_SliderAnimateSpeed";
            this.m_SliderAnimateSpeed.SetValue = 1F;
            this.m_SliderAnimateSpeed.Size = new System.Drawing.Size(50, 20);
            this.m_SliderAnimateSpeed.TabIndex = 7;
            this.m_SliderAnimateSpeed.Text = "1.000";
            this.m_SliderAnimateSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_SliderAnimateSpeed.Value = 1F;
            this.m_SliderAnimateSpeed.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.Data_OnValueChanged);
            // 
            // m_CheckAnimateEffect
            // 
            this.m_CheckAnimateEffect.AutoSize = true;
            this.m_CheckAnimateEffect.Location = new System.Drawing.Point(3, 21);
            this.m_CheckAnimateEffect.Name = "m_CheckAnimateEffect";
            this.m_CheckAnimateEffect.Size = new System.Drawing.Size(95, 17);
            this.m_CheckAnimateEffect.TabIndex = 6;
            this.m_CheckAnimateEffect.Text = "Animate Effect";
            this.m_CheckAnimateEffect.UseVisualStyleBackColor = true;
            this.m_CheckAnimateEffect.Click += new System.EventHandler(this.m_CheckAnimateEffect_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Speed";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(139, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Debug Effect Anim Info";
            // 
            // m_SliderAnimateDist
            // 
            this.m_SliderAnimateDist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_SliderAnimateDist.Decimals = 3;
            this.m_SliderAnimateDist.Increment = 0.01F;
            this.m_SliderAnimateDist.Location = new System.Drawing.Point(162, 44);
            this.m_SliderAnimateDist.MaxValue = 10000F;
            this.m_SliderAnimateDist.MinValue = 0F;
            this.m_SliderAnimateDist.Multiline = true;
            this.m_SliderAnimateDist.Name = "m_SliderAnimateDist";
            this.m_SliderAnimateDist.SetValue = 5F;
            this.m_SliderAnimateDist.Size = new System.Drawing.Size(50, 20);
            this.m_SliderAnimateDist.TabIndex = 8;
            this.m_SliderAnimateDist.Text = "5.000";
            this.m_SliderAnimateDist.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_SliderAnimateDist.Value = 5F;
            this.m_SliderAnimateDist.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.Data_OnValueChanged);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.syncFromRAGCheckBox);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.m_WPosZ);
            this.panel3.Controls.Add(this.m_WPosY);
            this.panel3.Controls.Add(this.m_WPosX);
            this.panel3.Controls.Add(this.m_RadioSWorld);
            this.panel3.Controls.Add(this.m_RadioSCam);
            this.panel3.Controls.Add(this.m_CamStartDist);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(366, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(304, 140);
            this.panel3.TabIndex = 3;
            // 
            // syncFromRAGCheckBox
            // 
            this.syncFromRAGCheckBox.AutoSize = true;
            this.syncFromRAGCheckBox.Enabled = false;
            this.syncFromRAGCheckBox.Location = new System.Drawing.Point(33, 110);
            this.syncFromRAGCheckBox.Name = "syncFromRAGCheckBox";
            this.syncFromRAGCheckBox.Size = new System.Drawing.Size(99, 17);
            this.syncFromRAGCheckBox.TabIndex = 14;
            this.syncFromRAGCheckBox.Text = "Sync from RAG";
            this.syncFromRAGCheckBox.UseVisualStyleBackColor = true;
            this.syncFromRAGCheckBox.CheckedChanged += new System.EventHandler(this.syncFromRAGCheckBox_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(205, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Z";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(30, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Distance";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(118, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Y";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(30, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "X";
            // 
            // m_WPosZ
            // 
            this.m_WPosZ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_WPosZ.Decimals = 3;
            this.m_WPosZ.Increment = 0.01F;
            this.m_WPosZ.Location = new System.Drawing.Point(224, 80);
            this.m_WPosZ.MaxValue = 100000F;
            this.m_WPosZ.MinValue = -100000F;
            this.m_WPosZ.Multiline = true;
            this.m_WPosZ.Name = "m_WPosZ";
            this.m_WPosZ.SetValue = 0F;
            this.m_WPosZ.Size = new System.Drawing.Size(70, 20);
            this.m_WPosZ.TabIndex = 9;
            this.m_WPosZ.Text = "0.000";
            this.m_WPosZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_WPosZ.Value = 0F;
            this.m_WPosZ.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.Data_OnValueChanged);
            // 
            // m_WPosY
            // 
            this.m_WPosY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_WPosY.Decimals = 3;
            this.m_WPosY.Increment = 0.01F;
            this.m_WPosY.Location = new System.Drawing.Point(134, 80);
            this.m_WPosY.MaxValue = 100000F;
            this.m_WPosY.MinValue = -100000F;
            this.m_WPosY.Multiline = true;
            this.m_WPosY.Name = "m_WPosY";
            this.m_WPosY.SetValue = 0F;
            this.m_WPosY.Size = new System.Drawing.Size(65, 20);
            this.m_WPosY.TabIndex = 8;
            this.m_WPosY.Text = "0.000";
            this.m_WPosY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_WPosY.Value = 0F;
            this.m_WPosY.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.Data_OnValueChanged);
            // 
            // m_WPosX
            // 
            this.m_WPosX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_WPosX.Decimals = 3;
            this.m_WPosX.Increment = 0.01F;
            this.m_WPosX.Location = new System.Drawing.Point(49, 80);
            this.m_WPosX.MaxValue = 100000F;
            this.m_WPosX.MinValue = -100000F;
            this.m_WPosX.Multiline = true;
            this.m_WPosX.Name = "m_WPosX";
            this.m_WPosX.SetValue = 0F;
            this.m_WPosX.Size = new System.Drawing.Size(65, 20);
            this.m_WPosX.TabIndex = 7;
            this.m_WPosX.Text = "0.000";
            this.m_WPosX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_WPosX.Value = 0F;
            this.m_WPosX.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.Data_OnValueChanged);
            // 
            // m_RadioSWorld
            // 
            this.m_RadioSWorld.AutoSize = true;
            this.m_RadioSWorld.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_RadioSWorld.Location = new System.Drawing.Point(12, 61);
            this.m_RadioSWorld.Name = "m_RadioSWorld";
            this.m_RadioSWorld.Size = new System.Drawing.Size(107, 17);
            this.m_RadioSWorld.TabIndex = 6;
            this.m_RadioSWorld.TabStop = true;
            this.m_RadioSWorld.Text = "World Position";
            this.m_RadioSWorld.UseVisualStyleBackColor = true;
            this.m_RadioSWorld.CheckedChanged += new System.EventHandler(this.m_Radio_CheckedChanged);
            // 
            // m_RadioSCam
            // 
            this.m_RadioSCam.AutoSize = true;
            this.m_RadioSCam.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_RadioSCam.Location = new System.Drawing.Point(12, 20);
            this.m_RadioSCam.Name = "m_RadioSCam";
            this.m_RadioSCam.Size = new System.Drawing.Size(123, 17);
            this.m_RadioSCam.TabIndex = 5;
            this.m_RadioSCam.TabStop = true;
            this.m_RadioSCam.Text = "Infront of Camera";
            this.m_RadioSCam.UseVisualStyleBackColor = true;
            this.m_RadioSCam.CheckedChanged += new System.EventHandler(this.m_Radio_CheckedChanged);
            // 
            // m_CamStartDist
            // 
            this.m_CamStartDist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_CamStartDist.Decimals = 3;
            this.m_CamStartDist.Increment = 0.01F;
            this.m_CamStartDist.Location = new System.Drawing.Point(121, 38);
            this.m_CamStartDist.MaxValue = 100000F;
            this.m_CamStartDist.MinValue = 0F;
            this.m_CamStartDist.Multiline = true;
            this.m_CamStartDist.Name = "m_CamStartDist";
            this.m_CamStartDist.SetValue = 10F;
            this.m_CamStartDist.Size = new System.Drawing.Size(50, 20);
            this.m_CamStartDist.TabIndex = 3;
            this.m_CamStartDist.Text = "10.000";
            this.m_CamStartDist.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_CamStartDist.Value = 10F;
            this.m_CamStartDist.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.Data_OnValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Debug Effect Spawn Info";
            // 
            // m_CheckRepeat
            // 
            this.m_CheckRepeat.AutoSize = true;
            this.m_CheckRepeat.Location = new System.Drawing.Point(11, 51);
            this.m_CheckRepeat.Name = "m_CheckRepeat";
            this.m_CheckRepeat.Size = new System.Drawing.Size(61, 17);
            this.m_CheckRepeat.TabIndex = 11;
            this.m_CheckRepeat.Text = "Repeat";
            this.m_CheckRepeat.UseVisualStyleBackColor = true;
            this.m_CheckRepeat.Click += new System.EventHandler(this.m_CheckAnimateEffect_Click);
            // 
            // m_RepeatTime
            // 
            this.m_RepeatTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_RepeatTime.Decimals = 2;
            this.m_RepeatTime.Increment = 0.1F;
            this.m_RepeatTime.Location = new System.Drawing.Point(90, 51);
            this.m_RepeatTime.MaxValue = 1000F;
            this.m_RepeatTime.MinValue = 0F;
            this.m_RepeatTime.Multiline = true;
            this.m_RepeatTime.Name = "m_RepeatTime";
            this.m_RepeatTime.SetValue = 5F;
            this.m_RepeatTime.Size = new System.Drawing.Size(77, 20);
            this.m_RepeatTime.TabIndex = 4;
            this.m_RepeatTime.Text = "5.00";
            this.m_RepeatTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_RepeatTime.Value = 5F;
            this.m_RepeatTime.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.Data_OnValueChanged);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.m_ButtonResetEffect);
            this.panel2.Controls.Add(this.m_RepeatTime);
            this.panel2.Controls.Add(this.m_CheckRepeat);
            this.panel2.Controls.Add(this.m_ButtonPlay);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(185, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(181, 140);
            this.panel2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Debug Effect Controls";
            // 
            // m_ButtonResetEffect
            // 
            this.m_ButtonResetEffect.Location = new System.Drawing.Point(92, 19);
            this.m_ButtonResetEffect.Name = "m_ButtonResetEffect";
            this.m_ButtonResetEffect.Size = new System.Drawing.Size(75, 23);
            this.m_ButtonResetEffect.TabIndex = 5;
            this.m_ButtonResetEffect.Text = "Reset";
            this.m_ButtonResetEffect.UseVisualStyleBackColor = true;
            this.m_ButtonResetEffect.Click += new System.EventHandler(this.m_ButtonResetEffect_Click);
            // 
            // m_ButtonPlay
            // 
            this.m_ButtonPlay.BackColor = System.Drawing.SystemColors.Control;
            this.m_ButtonPlay.Location = new System.Drawing.Point(11, 20);
            this.m_ButtonPlay.Name = "m_ButtonPlay";
            this.m_ButtonPlay.Size = new System.Drawing.Size(75, 23);
            this.m_ButtonPlay.TabIndex = 0;
            this.m_ButtonPlay.Text = "Play";
            this.m_ButtonPlay.UseVisualStyleBackColor = false;
            this.m_ButtonPlay.Click += new System.EventHandler(this.m_ButtonPlay_Click);
            // 
            // m_ButtonReset
            // 
            this.m_ButtonReset.Location = new System.Drawing.Point(90, 19);
            this.m_ButtonReset.Name = "m_ButtonReset";
            this.m_ButtonReset.Size = new System.Drawing.Size(75, 23);
            this.m_ButtonReset.TabIndex = 4;
            this.m_ButtonReset.Text = "Reset";
            this.m_ButtonReset.UseVisualStyleBackColor = true;
            this.m_ButtonReset.Click += new System.EventHandler(this.m_ButtonReset_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(30, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Time Scalar";
            // 
            // m_PauseButton
            // 
            this.m_PauseButton.Location = new System.Drawing.Point(9, 19);
            this.m_PauseButton.Name = "m_PauseButton";
            this.m_PauseButton.Size = new System.Drawing.Size(75, 23);
            this.m_PauseButton.TabIndex = 5;
            this.m_PauseButton.Text = "Pause";
            this.m_PauseButton.UseVisualStyleBackColor = true;
            this.m_PauseButton.Click += new System.EventHandler(this.m_PauseButton_Click);
            // 
            // m_TimeScalar
            // 
            this.m_TimeScalar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_TimeScalar.Decimals = 3;
            this.m_TimeScalar.Increment = 0.01F;
            this.m_TimeScalar.Location = new System.Drawing.Point(99, 48);
            this.m_TimeScalar.MaxValue = 10000F;
            this.m_TimeScalar.MinValue = 0F;
            this.m_TimeScalar.Multiline = true;
            this.m_TimeScalar.Name = "m_TimeScalar";
            this.m_TimeScalar.SetValue = 1F;
            this.m_TimeScalar.Size = new System.Drawing.Size(50, 20);
            this.m_TimeScalar.TabIndex = 2;
            this.m_TimeScalar.Text = "1.000";
            this.m_TimeScalar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_TimeScalar.Value = 1F;
            this.m_TimeScalar.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.Data_OnValueChanged);
            // 
            // m_LabelPlaybackOptions
            // 
            this.m_LabelPlaybackOptions.AutoSize = true;
            this.m_LabelPlaybackOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelPlaybackOptions.Location = new System.Drawing.Point(3, 0);
            this.m_LabelPlaybackOptions.Name = "m_LabelPlaybackOptions";
            this.m_LabelPlaybackOptions.Size = new System.Drawing.Size(93, 13);
            this.m_LabelPlaybackOptions.TabIndex = 1;
            this.m_LabelPlaybackOptions.Text = "Global Controls";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.m_ButtonReset);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.m_PauseButton);
            this.panel1.Controls.Add(this.m_TimeScalar);
            this.panel1.Controls.Add(this.m_LabelPlaybackOptions);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(185, 140);
            this.panel1.TabIndex = 1;
            // 
            // ragSyncTimer
            // 
            this.ragSyncTimer.Tick += new System.EventHandler(this.ragSyncTimer_Tick);
            // 
            // Playcontrol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 140);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Playcontrol";
            this.ShowInTaskbar = false;
            this.Text = "Playcontrol";
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button m_ButtonPlay;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label m_LabelPlaybackOptions;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.RadioButton m_RadioSCam;
        public ptxSlider m_CamStartDist;
        public System.Windows.Forms.RadioButton m_RadioSWorld;
        public ptxSlider m_WPosX;
        public ptxSlider m_WPosZ;
        public ptxSlider m_WPosY;
        public ptxSlider m_TimeScalar;
        public ptxSlider m_RepeatTime;
        private System.Windows.Forms.Button m_PauseButton;
        private System.Windows.Forms.Button m_ButtonReset;
        private System.Windows.Forms.Button m_ButtonResetEffect;
        public System.Windows.Forms.CheckBox m_CheckAnimateEffect;
        public ptxSlider m_SliderAnimateDist;
        public ptxSlider m_SliderAnimateSpeed;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.ComboBox m_ComboAnimateType;
        public System.Windows.Forms.CheckBox m_CheckRepeat;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.CheckBox syncFromRAGCheckBox;
        private System.Windows.Forms.Timer ragSyncTimer;


    }
}