using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
    public partial class ProfileWindow : ParticleEditor.ptxForm
    {
        String m_EffectName = "";
        String m_EmitterName = "";
        String m_PtxRuleName ="";
        List<System.Windows.Forms.Label>    m_BehaviorNames = new List<System.Windows.Forms.Label>();
        List<System.Windows.Forms.Label>    m_BehaviorTimes = new List<System.Windows.Forms.Label>();


        public ProfileWindow()
        {
            m_Name = "Profile";
            m_WindowID = "PROFILEWINDOW";

            InitializeComponent();
        }
    
        public void SetEffect(String name)
        {
            m_EffectName = name;
        }

        public void SetEmitter(String name)
        {
            m_EmitterName = name;
        }
        public void SetPtxRule(String name)
        {
            m_PtxRuleName = name;
        }

        public void StartProfiling()
        {
            m_ProfileTimer.Start();
        }

        public void StopProfiling()
        {
            m_ProfileTimer.Stop();
        }

        public void UpdateProfiling()
        {
            DataMod.dtaProfilingGlobal proG = DataMod.m_SDataMod.m_ProfileGlobal;
            m_LabelTotalPoints.Text = proG.m_TotalPoints.ToString();
            m_LabelTotalActiveInstances.Text = proG.m_TotalActiveInstances.ToString();
            m_LabelTotalCulledInstances.Text = proG.m_TotalCulledInstances.ToString();
            m_LabelTotalCPUUpdateTime.Text = proG.m_TotalCPUUpdateTime.ToString("0.000");
            m_LabelTotalCPUDrawTime.Text = proG.m_TotalCPUDrawTime.ToString("0.000");
            m_LabelTotalGPUDrawTime.Text = proG.m_TotalGPUDrawTime.ToString("0.000");

            DataMod.dtaProfilingEffect proEf = DataMod.m_SDataMod.m_ProfileEffect;
            m_LabelEffect.Text = "Effect (" + proEf.m_EffectName + ")";
            m_LabelEffectTotalPoints.Text = proEf.m_EffectTotalPoints.ToString();
            m_LabelEffectActiveInstances.Text = proEf.m_EffectActiveInstances.ToString();
            m_LabelEffectCulledInstances.Text = proEf.m_EffectCulledInstances.ToString();
            m_LabelEffectCUPUpdateTime.Text = proEf.m_EffectCPUUpdateTime.ToString("0.000");
            m_LabelEffectCPUDrawTime.Text = proEf.m_EffectCPUUDrawTime.ToString("0.000");

            DataMod.dtaProfilingEmitter proEm = DataMod.m_SDataMod.m_ProfileEmitter;
            m_LabelEmitter.Text = "Emitter (" + proEm.m_EmitterName + ")";
			m_LabelEmitterTotalPoints.Text = proEm.m_EmitterTotalPoints.ToString();
			m_LabelEmitterActiveInstances.Text = proEm.m_EmitterActiveInstances.ToString();
            m_LabelEmitterCPUUpdateTime.Text = proEm.m_EmitterCPUUpdateTime.ToString("0.000");
			m_LabelEmitterCPUDrawTime.Text = proEm.m_EmitterCPUDrawTime.ToString("0.000");
			m_LabelEmitterCPUSortTime.Text = proEm.m_EmitterCPUSortTime.ToString("0.000");

            DataMod.dtaProfilingPtx proPt = DataMod.m_SDataMod.m_ProfilePtx;
            m_LabelPtxRule.Text = "PtxRule (" + proPt.m_PtxRuleName + ")";
//            m_LabelPtxTotalPoints.Text = proPt.m_PtxTotalPoints.ToString();
//            m_LabelPtxCPUUpdateTime.Text = proPt.m_PtxCPUUpdateTime.ToString("0.000");
//            m_LabelPtxCPUDrawTime.Text = proPt.m_PtxCPUDrawTime.ToString("0.000");
//            m_LabelPtxSortTime.Text = proPt.m_PtxCPUSortTime.ToString("0.000");

            //Behaviors
            // Calc and show total behavior update time.
            float totalBehaviorUpdate = 0.0f;
            for( int i = 0; i < proPt.m_PtxBehaviorCPUUpdateTimes.Count; i++ )
                totalBehaviorUpdate += proPt.m_PtxBehaviorCPUUpdateTimes[i];
            m_LabelTotalbehaviorUpdateTime.Text = totalBehaviorUpdate.ToString("0.000");

            
        }

        private void m_ProfileTimer_Tick(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.RequestProfilingGlobal();
            if(m_EffectName != "")
                DataCon.m_SDataCon.RequestProfilingEffect(m_EffectName);
            if (m_EmitterName != "")
                DataCon.m_SDataCon.RequestProfilingEmitter(m_EmitterName);
            if (m_PtxRuleName != "")
                DataCon.m_SDataCon.RequestProfilingPtx(m_PtxRuleName);
        }

        private void ProfileWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopProfiling();
        }

        private void ProfileWindow_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                StopProfiling();
            else
                StartProfiling();
        }

        private void m_ShowEffectNames_CheckedChanged(object sender, EventArgs e)
        {
            DataCon.m_SDataCon.SendShowEffectNames(m_ShowEffectNames.Checked);
        }
    }
}

